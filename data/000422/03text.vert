<s>
SK	Sk	kA	Sk
Slavia	Slavia	k1gFnSc1	Slavia
Praha	Praha	k1gFnSc1	Praha
je	být	k5eAaImIp3nS	být
český	český	k2eAgInSc1d1	český
fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
klub	klub	k1gInSc1	klub
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
2	[number]	k4	2
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1892	[number]	k4	1892
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
jako	jako	k8xC	jako
cyklistický	cyklistický	k2eAgInSc1d1	cyklistický
odbor	odbor	k1gInSc1	odbor
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
vzešlý	vzešlý	k2eAgInSc1d1	vzešlý
z	z	k7c2	z
pražského	pražský	k2eAgNnSc2d1	Pražské
vlasteneckého	vlastenecký	k2eAgNnSc2d1	vlastenecké
studentského	studentský	k2eAgNnSc2d1	studentské
sdružení	sdružení	k1gNnSc2	sdružení
Literární	literární	k2eAgMnSc1d1	literární
a	a	k8xC	a
řečnický	řečnický	k2eAgInSc1d1	řečnický
spolek	spolek	k1gInSc1	spolek
Slavia	Slavia	k1gFnSc1	Slavia
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
ve	v	k7c6	v
Vodičkově	Vodičkův	k2eAgFnSc6d1	Vodičkova
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
počtu	počet	k1gInSc2	počet
získaných	získaný	k2eAgInPc2d1	získaný
ligových	ligový	k2eAgInPc2d1	ligový
titulů	titul	k1gInPc2	titul
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
historicky	historicky	k6eAd1	historicky
druhý	druhý	k4xOgInSc4	druhý
nejúspěšnější	úspěšný	k2eAgInSc4d3	nejúspěšnější
český	český	k2eAgInSc4d1	český
profesionální	profesionální	k2eAgInSc4d1	profesionální
fotbalový	fotbalový	k2eAgInSc4d1	fotbalový
klub	klub	k1gInSc4	klub
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
fotbalové	fotbalový	k2eAgFnSc2d1	fotbalová
historie	historie	k1gFnSc2	historie
patří	patřit	k5eAaImIp3nS	patřit
společně	společně	k6eAd1	společně
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgMnSc7	svůj
pražským	pražský	k2eAgMnSc7d1	pražský
rivalem	rival	k1gMnSc7	rival
<g/>
,	,	kIx,	,
klubem	klub	k1gInSc7	klub
AC	AC	kA	AC
Sparta	Sparta	k1gFnSc1	Sparta
Praha	Praha	k1gFnSc1	Praha
k	k	k7c3	k
nejpopulárnějším	populární	k2eAgInPc3d3	nejpopulárnější
klubům	klub	k1gInPc3	klub
českého	český	k2eAgInSc2d1	český
fotbalu	fotbal	k1gInSc2	fotbal
<g/>
.	.	kIx.	.
</s>
<s>
Slavia	Slavia	k1gFnSc1	Slavia
získala	získat	k5eAaPmAgFnS	získat
17	[number]	k4	17
ligových	ligový	k2eAgInPc2d1	ligový
titulů	titul	k1gInPc2	titul
(	(	kIx(	(
<g/>
naposledy	naposledy	k6eAd1	naposledy
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
a	a	k8xC	a
třikrát	třikrát	k6eAd1	třikrát
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
vítězem	vítěz	k1gMnSc7	vítěz
domácího	domácí	k2eAgInSc2d1	domácí
poháru	pohár	k1gInSc2	pohár
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
Středoevropský	středoevropský	k2eAgInSc4d1	středoevropský
pohár	pohár	k1gInSc4	pohár
(	(	kIx(	(
<g/>
Mitropa	Mitropa	k1gFnSc1	Mitropa
Cup	cup	k1gInSc1	cup
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
předchůdce	předchůdce	k1gMnSc1	předchůdce
současných	současný	k2eAgInPc2d1	současný
evropských	evropský	k2eAgInPc2d1	evropský
pohárů	pohár	k1gInPc2	pohár
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejslavnější	slavný	k2eAgMnPc4d3	nejslavnější
hráče	hráč	k1gMnPc4	hráč
klubu	klub	k1gInSc2	klub
patří	patřit	k5eAaImIp3nS	patřit
Jan	Jan	k1gMnSc1	Jan
Košek	Košek	k1gMnSc1	Košek
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Plánička	plánička	k1gFnSc1	plánička
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
Puč	puč	k1gInSc1	puč
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Svoboda	Svoboda	k1gMnSc1	Svoboda
(	(	kIx(	(
<g/>
poslední	poslední	k2eAgMnPc1d1	poslední
tři	tři	k4xCgMnPc1	tři
jmenovaní	jmenovaný	k2eAgMnPc1d1	jmenovaný
stříbrní	stříbrný	k2eAgMnPc1d1	stříbrný
medailisté	medailista	k1gMnPc1	medailista
mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
1934	[number]	k4	1934
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Bican	Bican	k1gMnSc1	Bican
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Lála	Lála	k1gMnSc1	Lála
(	(	kIx(	(
<g/>
stříbrný	stříbrný	k2eAgMnSc1d1	stříbrný
medailista	medailista	k1gMnSc1	medailista
mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Veselý	Veselý	k1gMnSc1	Veselý
(	(	kIx(	(
<g/>
mistr	mistr	k1gMnSc1	mistr
Evropy	Evropa	k1gFnSc2	Evropa
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Kuka	Kuka	k1gMnSc1	Kuka
a	a	k8xC	a
Vladimír	Vladimír	k1gMnSc1	Vladimír
Šmicer	Šmicer	k1gMnSc1	Šmicer
<g/>
.	.	kIx.	.
</s>
<s>
Domácí	domácí	k2eAgInPc1d1	domácí
zápasy	zápas	k1gInPc1	zápas
se	se	k3xPyFc4	se
od	od	k7c2	od
sezony	sezona	k1gFnSc2	sezona
2008	[number]	k4	2008
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
pořádají	pořádat	k5eAaImIp3nP	pořádat
na	na	k7c6	na
stadionu	stadion	k1gInSc6	stadion
v	v	k7c6	v
Edenu	Eden	k1gInSc6	Eden
s	s	k7c7	s
názvem	název	k1gInSc7	název
Eden	Eden	k1gInSc1	Eden
Aréna	aréna	k1gFnSc1	aréna
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
byl	být	k5eAaImAgInS	být
slavnostně	slavnostně	k6eAd1	slavnostně
otevřen	otevřít	k5eAaPmNgInS	otevřít
7	[number]	k4	7
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
zápasem	zápas	k1gInSc7	zápas
s	s	k7c7	s
týmem	tým	k1gInSc7	tým
Oxford	Oxford	k1gInSc1	Oxford
University	universita	k1gFnSc2	universita
AFC	AFC	kA	AFC
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
stadion	stadion	k1gInSc1	stadion
na	na	k7c6	na
Letné	Letná	k1gFnSc6	Letná
<g/>
,	,	kIx,	,
založený	založený	k2eAgInSc1d1	založený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1899	[number]	k4	1899
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
reprezentovaný	reprezentovaný	k2eAgInSc4d1	reprezentovaný
dokonce	dokonce	k9	dokonce
dvěma	dva	k4xCgInPc7	dva
stadiony	stadion	k1gInPc7	stadion
<g/>
,	,	kIx,	,
užíval	užívat	k5eAaImAgInS	užívat
klub	klub	k1gInSc1	klub
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgMnS	být
přinucen	přinucen	k2eAgMnSc1d1	přinucen
se	se	k3xPyFc4	se
přestěhovat	přestěhovat	k5eAaPmF	přestěhovat
do	do	k7c2	do
vršovického	vršovický	k2eAgInSc2d1	vršovický
Edenu	Eden	k1gInSc2	Eden
(	(	kIx(	(
<g/>
Stadion	stadion	k1gInSc1	stadion
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Václava	Václav	k1gMnSc2	Václav
Vacka	Vacek	k1gMnSc2	Vacek
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
Eden	Eden	k1gInSc1	Eden
Aréna	aréna	k1gFnSc1	aréna
<g/>
)	)	kIx)	)
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
vládního	vládní	k2eAgNnSc2d1	vládní
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
o	o	k7c6	o
stavbě	stavba	k1gFnSc6	stavba
Stalinova	Stalinův	k2eAgInSc2d1	Stalinův
pomníku	pomník	k1gInSc2	pomník
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgInSc3	jenž
prostor	prostor	k1gInSc4	prostor
stadionů	stadion	k1gInPc2	stadion
bránil	bránit	k5eAaImAgMnS	bránit
<g/>
.	.	kIx.	.
</s>
<s>
Klub	klub	k1gInSc1	klub
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
době	doba	k1gFnSc6	doba
rozmachu	rozmach	k1gInSc2	rozmach
českého	český	k2eAgNnSc2d1	české
národnostního	národnostní	k2eAgNnSc2d1	národnostní
cítění	cítění	k1gNnSc2	cítění
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
2	[number]	k4	2
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1892	[number]	k4	1892
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
v	v	k7c6	v
Kulichově	Kulichův	k2eAgInSc6d1	Kulichův
domě	dům	k1gInSc6	dům
na	na	k7c6	na
pražském	pražský	k2eAgNnSc6d1	Pražské
Karlově	Karlův	k2eAgNnSc6d1	Karlovo
náměstí	náměstí	k1gNnSc6	náměstí
valná	valný	k2eAgFnSc1d1	valná
hromada	hromada	k1gFnSc1	hromada
vysokoškolského	vysokoškolský	k2eAgInSc2d1	vysokoškolský
Literárního	literární	k2eAgInSc2d1	literární
a	a	k8xC	a
řečnického	řečnický	k2eAgInSc2d1	řečnický
spolku	spolek	k1gInSc2	spolek
Slavia	Slavia	k1gFnSc1	Slavia
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yIgInSc6	který
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
sportovní	sportovní	k2eAgInSc1d1	sportovní
odbor	odbor	k1gInSc1	odbor
ACOS	ACOS	kA	ACOS
(	(	kIx(	(
<g/>
Akademický	akademický	k2eAgInSc1d1	akademický
cyklistický	cyklistický	k2eAgInSc1d1	cyklistický
oddíl	oddíl	k1gInSc1	oddíl
Slavia	Slavia	k1gFnSc1	Slavia
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
23	[number]	k4	23
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1892	[number]	k4	1892
byl	být	k5eAaImAgInS	být
předsedou	předseda	k1gMnSc7	předseda
tohoto	tento	k3xDgInSc2	tento
odboru	odbor	k1gInSc2	odbor
zvolen	zvolen	k2eAgMnSc1d1	zvolen
lékař	lékař	k1gMnSc1	lékař
Václav	Václav	k1gMnSc1	Václav
Kubr	Kubr	k1gMnSc1	Kubr
<g/>
.	.	kIx.	.
</s>
<s>
Klub	klub	k1gInSc1	klub
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaPmAgInS	věnovat
v	v	k7c6	v
počátcích	počátek	k1gInPc6	počátek
cyklistice	cyklistika	k1gFnSc6	cyklistika
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
brzy	brzy	k6eAd1	brzy
po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
založení	založení	k1gNnSc6	založení
byl	být	k5eAaImAgInS	být
celý	celý	k2eAgInSc1d1	celý
spolek	spolek	k1gInSc1	spolek
Slavia	Slavium	k1gNnSc2	Slavium
pro	pro	k7c4	pro
údajnou	údajný	k2eAgFnSc4d1	údajná
protirakouskou	protirakouský	k2eAgFnSc4d1	protirakouská
činnost	činnost	k1gFnSc4	činnost
policejně	policejně	k6eAd1	policejně
rozpuštěn	rozpuštěn	k2eAgMnSc1d1	rozpuštěn
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
31	[number]	k4	31
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1895	[number]	k4	1895
byl	být	k5eAaImAgInS	být
z	z	k7c2	z
popudu	popud	k1gInSc2	popud
medika	medik	k1gMnSc2	medik
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Hausmana	Hausman	k1gMnSc2	Hausman
založen	založit	k5eAaPmNgInS	založit
"	"	kIx"	"
<g/>
Sportovní	sportovní	k2eAgInSc1d1	sportovní
klub	klub	k1gInSc1	klub
Slavia	Slavia	k1gFnSc1	Slavia
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
nástupcem	nástupce	k1gMnSc7	nástupce
ACOS	ACOS	kA	ACOS
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
prvním	první	k4xOgMnSc7	první
starostou	starosta	k1gMnSc7	starosta
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
pan	pan	k1gMnSc1	pan
Ankrt	Ankrta	k1gFnPc2	Ankrta
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
slovanské	slovanský	k2eAgFnPc1d1	Slovanská
barvy	barva	k1gFnPc1	barva
-	-	kIx~	-
červená	červený	k2eAgFnSc1d1	červená
a	a	k8xC	a
bílá	bílý	k2eAgFnSc1d1	bílá
-	-	kIx~	-
staly	stát	k5eAaPmAgFnP	stát
oficiálními	oficiální	k2eAgFnPc7d1	oficiální
barvami	barva	k1gFnPc7	barva
klubu	klub	k1gInSc2	klub
<g/>
,	,	kIx,	,
k	k	k7c3	k
nim	on	k3xPp3gInPc3	on
byla	být	k5eAaImAgFnS	být
přidána	přidán	k2eAgFnSc1d1	přidána
červená	červený	k2eAgFnSc1d1	červená
pěticípá	pěticípý	k2eAgFnSc1d1	pěticípá
hvězda	hvězda	k1gFnSc1	hvězda
směřující	směřující	k2eAgInSc4d1	směřující
hrotem	hrot	k1gInSc7	hrot
dolů	dol	k1gInPc2	dol
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
symbolizovat	symbolizovat	k5eAaImF	symbolizovat
naději	naděje	k1gFnSc4	naděje
i	i	k9	i
v	v	k7c6	v
čase	čas	k1gInSc6	čas
nezdaru	nezdar	k1gInSc2	nezdar
<g/>
.	.	kIx.	.
</s>
<s>
Oficiálním	oficiální	k2eAgInSc7d1	oficiální
dresem	dres	k1gInSc7	dres
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
tričko	tričko	k1gNnSc1	tričko
s	s	k7c7	s
červenou	červený	k2eAgFnSc7d1	červená
a	a	k8xC	a
bílou	bílý	k2eAgFnSc7d1	bílá
polovinou	polovina	k1gFnSc7	polovina
<g/>
,	,	kIx,	,
na	na	k7c6	na
polovině	polovina	k1gFnSc6	polovina
bílé	bílý	k2eAgFnSc6d1	bílá
s	s	k7c7	s
onou	onen	k3xDgFnSc7	onen
hvězdou	hvězda	k1gFnSc7	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Klub	klub	k1gInSc1	klub
se	se	k3xPyFc4	se
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
opětovném	opětovný	k2eAgInSc6d1	opětovný
vzniku	vznik	k1gInSc6	vznik
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1895	[number]	k4	1895
stal	stát	k5eAaPmAgInS	stát
dominantním	dominantní	k2eAgInSc7d1	dominantní
na	na	k7c6	na
české	český	k2eAgFnSc6d1	Česká
fotbalové	fotbalový	k2eAgFnSc6d1	fotbalová
scéně	scéna	k1gFnSc6	scéna
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1896	[number]	k4	1896
<g/>
-	-	kIx~	-
<g/>
1902	[number]	k4	1902
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
šest	šest	k4xCc1	šest
z	z	k7c2	z
devíti	devět	k4xCc2	devět
ročníků	ročník	k1gInPc2	ročník
neoficiálního	neoficiální	k2eAgNnSc2d1	neoficiální
Mistrovství	mistrovství	k1gNnSc2	mistrovství
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
klubů	klub	k1gInPc2	klub
z	z	k7c2	z
Čech	Čechy	k1gFnPc2	Čechy
porážel	porážet	k5eAaImAgInS	porážet
vysokým	vysoký	k2eAgInSc7d1	vysoký
rozdílem	rozdíl	k1gInSc7	rozdíl
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
jeho	jeho	k3xOp3gMnSc1	jeho
velký	velký	k2eAgMnSc1d1	velký
rival	rival	k1gMnSc1	rival
AC	AC	kA	AC
Sparta	Sparta	k1gFnSc1	Sparta
Praha	Praha	k1gFnSc1	Praha
dokázal	dokázat	k5eAaPmAgInS	dokázat
zvítězit	zvítězit	k5eAaPmF	zvítězit
až	až	k9	až
v	v	k7c6	v
7	[number]	k4	7
<g/>
.	.	kIx.	.
vzájemném	vzájemný	k2eAgNnSc6d1	vzájemné
utkání	utkání	k1gNnSc6	utkání
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
nazývaném	nazývaný	k2eAgMnSc6d1	nazývaný
Pražské	pražský	k2eAgNnSc1d1	Pražské
derby	derby	k1gNnSc1	derby
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1899	[number]	k4	1899
získala	získat	k5eAaPmAgFnS	získat
Slavia	Slavia	k1gFnSc1	Slavia
prostor	prostor	k1gInSc4	prostor
pro	pro	k7c4	pro
své	svůj	k3xOyFgInPc4	svůj
domácí	domácí	k2eAgInPc4d1	domácí
zápasy	zápas	k1gInPc4	zápas
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c6	na
Letné	Letná	k1gFnSc6	Letná
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
tentýž	týž	k3xTgInSc4	týž
rok	rok	k1gInSc4	rok
sehrála	sehrát	k5eAaPmAgFnS	sehrát
první	první	k4xOgInSc4	první
mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
zápas	zápas	k1gInSc4	zápas
s	s	k7c7	s
týmem	tým	k1gInSc7	tým
SC	SC	kA	SC
Berlin	berlina	k1gFnPc2	berlina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1903	[number]	k4	1903
přišel	přijít	k5eAaPmAgMnS	přijít
do	do	k7c2	do
Slavie	slavie	k1gFnSc2	slavie
kanonýr	kanonýr	k1gMnSc1	kanonýr
a	a	k8xC	a
velká	velký	k2eAgFnSc1d1	velká
ikona	ikona	k1gFnSc1	ikona
Jan	Jan	k1gMnSc1	Jan
Košek	Košek	k1gMnSc1	Košek
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zde	zde	k6eAd1	zde
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1913	[number]	k4	1913
nastřílel	nastřílet	k5eAaPmAgMnS	nastřílet
804	[number]	k4	804
gólů	gól	k1gInPc2	gól
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
sezónu	sezóna	k1gFnSc4	sezóna
1911	[number]	k4	1911
vstřelil	vstřelit	k5eAaPmAgMnS	vstřelit
tento	tento	k3xDgMnSc1	tento
fenomenální	fenomenální	k2eAgMnSc1d1	fenomenální
fotbalista	fotbalista	k1gMnSc1	fotbalista
135	[number]	k4	135
branek	branka	k1gFnPc2	branka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1909	[number]	k4	1909
prohrála	prohrát	k5eAaPmAgFnS	prohrát
Slavia	Slavia	k1gFnSc1	Slavia
s	s	k7c7	s
SK	Sk	kA	Sk
Smíchov	Smíchov	k1gInSc1	Smíchov
a	a	k8xC	a
po	po	k7c6	po
12	[number]	k4	12
letech	léto	k1gNnPc6	léto
byla	být	k5eAaImAgFnS	být
poražena	porazit	k5eAaPmNgFnS	porazit
domácím	domácí	k2eAgInSc7d1	domácí
klubem	klub	k1gInSc7	klub
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1905	[number]	k4	1905
přichází	přicházet	k5eAaImIp3nS	přicházet
do	do	k7c2	do
Slavie	slavie	k1gFnSc2	slavie
slavný	slavný	k2eAgMnSc1d1	slavný
skotský	skotský	k2eAgMnSc1d1	skotský
trenér	trenér	k1gMnSc1	trenér
John	John	k1gMnSc1	John
Madden	Maddna	k1gFnPc2	Maddna
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
roli	role	k1gFnSc6	role
u	u	k7c2	u
mužstva	mužstvo	k1gNnSc2	mužstvo
posléze	posléze	k6eAd1	posléze
působil	působit	k5eAaImAgInS	působit
čtvrt	čtvrt	k1gFnSc4	čtvrt
století	století	k1gNnSc2	století
a	a	k8xC	a
trénoval	trénovat	k5eAaImAgMnS	trénovat
i	i	k9	i
mužstvo	mužstvo	k1gNnSc4	mužstvo
Čech	Čechy	k1gFnPc2	Čechy
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1911	[number]	k4	1911
<g/>
,	,	kIx,	,
o	o	k7c4	o
jehož	jehož	k3xOyRp3gInSc4	jehož
triumf	triumf	k1gInSc4	triumf
na	na	k7c6	na
amatérském	amatérský	k2eAgNnSc6d1	amatérské
mistrovství	mistrovství	k1gNnSc6	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
se	se	k3xPyFc4	se
významnou	významný	k2eAgFnSc7d1	významná
měrou	míra	k1gFnSc7wR	míra
zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
<g/>
.	.	kIx.	.
</s>
<s>
Sestava	sestava	k1gFnSc1	sestava
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
doby	doba	k1gFnSc2	doba
<g/>
:	:	kIx,	:
Hejda	Hejda	k1gMnSc1	Hejda
<g/>
-	-	kIx~	-
<g/>
Krummer	Krummer	k1gMnSc1	Krummer
<g/>
,	,	kIx,	,
Veselý	Veselý	k1gMnSc1	Veselý
<g/>
-	-	kIx~	-
<g/>
E.	E.	kA	E.
Benda	Benda	k1gMnSc1	Benda
<g/>
,	,	kIx,	,
Hrabě	Hrabě	k1gMnSc1	Hrabě
<g/>
,	,	kIx,	,
Macoun	Macoun	k1gMnSc1	Macoun
<g/>
-	-	kIx~	-
<g/>
Baumruk	Baumruk	k1gMnSc1	Baumruk
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Benda	Benda	k1gMnSc1	Benda
<g/>
,	,	kIx,	,
Malý	Malý	k1gMnSc1	Malý
<g/>
,	,	kIx,	,
Košek	Košek	k1gMnSc1	Košek
a	a	k8xC	a
Vaněk	Vaněk	k1gMnSc1	Vaněk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1901	[number]	k4	1901
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
první	první	k4xOgFnSc1	první
tribuna	tribuna	k1gFnSc1	tribuna
klubového	klubový	k2eAgInSc2d1	klubový
stadionu	stadion	k1gInSc2	stadion
na	na	k7c6	na
Letné	Letná	k1gFnSc6	Letná
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
poté	poté	k6eAd1	poté
postupně	postupně	k6eAd1	postupně
rozšiřován	rozšiřován	k2eAgInSc1d1	rozšiřován
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
století	století	k1gNnSc2	století
poráží	porážet	k5eAaImIp3nS	porážet
Slavia	Slavia	k1gFnSc1	Slavia
i	i	k8xC	i
opravdové	opravdový	k2eAgFnPc1d1	opravdová
evropské	evropský	k2eAgFnPc1d1	Evropská
velkokluby	velkokluba	k1gFnPc1	velkokluba
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
anglický	anglický	k2eAgInSc1d1	anglický
Southampton	Southampton	k1gInSc1	Southampton
FC	FC	kA	FC
či	či	k8xC	či
španělská	španělský	k2eAgFnSc1d1	španělská
FC	FC	kA	FC
Barcelona	Barcelona	k1gFnSc1	Barcelona
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
domácí	domácí	k2eAgFnSc6d1	domácí
půdě	půda	k1gFnSc6	půda
slaví	slavit	k5eAaImIp3nP	slavit
úspěchy	úspěch	k1gInPc1	úspěch
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
Charity	charita	k1gFnSc2	charita
Cupu	cup	k1gInSc2	cup
-	-	kIx~	-
Poháru	pohár	k1gInSc2	pohár
dobročinnosti	dobročinnost	k1gFnSc2	dobročinnost
<g/>
,	,	kIx,	,
jejž	jenž	k3xRgInSc4	jenž
získala	získat	k5eAaPmAgFnS	získat
4	[number]	k4	4
<g/>
×	×	k?	×
či	či	k8xC	či
zisku	zisk	k1gInSc2	zisk
1	[number]	k4	1
<g/>
.	.	kIx.	.
mistrovského	mistrovský	k2eAgInSc2d1	mistrovský
titulu	titul	k1gInSc2	titul
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1913	[number]	k4	1913
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vyhrává	vyhrávat	k5eAaImIp3nS	vyhrávat
Mistrovství	mistrovství	k1gNnSc1	mistrovství
Českého	český	k2eAgInSc2d1	český
svazu	svaz	k1gInSc2	svaz
fotbalového	fotbalový	k2eAgInSc2d1	fotbalový
a	a	k8xC	a
po	po	k7c6	po
Spartě	Sparta	k1gFnSc6	Sparta
(	(	kIx(	(
<g/>
1912	[number]	k4	1912
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stává	stávat	k5eAaImIp3nS	stávat
druhým	druhý	k4xOgMnSc7	druhý
držitelem	držitel	k1gMnSc7	držitel
titulu	titul	k1gInSc2	titul
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
dekádě	dekáda	k1gFnSc6	dekáda
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
Slavia	Slavia	k1gFnSc1	Slavia
neoficiálně	oficiálně	k6eNd1	oficiálně
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgInPc2d3	nejlepší
fotbalových	fotbalový	k2eAgInPc2d1	fotbalový
klubů	klub	k1gInPc2	klub
kontinentální	kontinentální	k2eAgFnSc2d1	kontinentální
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
souvislosti	souvislost	k1gFnSc6	souvislost
stojí	stát	k5eAaImIp3nS	stát
za	za	k7c4	za
zmínku	zmínka	k1gFnSc4	zmínka
některé	některý	k3yIgInPc1	některý
z	z	k7c2	z
výsledků	výsledek	k1gInPc2	výsledek
jejích	její	k3xOp3gMnPc2	její
mezinárodních	mezinárodní	k2eAgMnPc2d1	mezinárodní
utkání	utkání	k1gNnSc3	utkání
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
<g/>
:	:	kIx,	:
1901	[number]	k4	1901
-	-	kIx~	-
Racing	Racing	k1gInSc1	Racing
Club	club	k1gInSc1	club
Paris	Paris	k1gMnSc1	Paris
8	[number]	k4	8
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
1903	[number]	k4	1903
-	-	kIx~	-
Budapesti	Budapest	k1gFnSc2	Budapest
TC	tc	k0	tc
12	[number]	k4	12
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
Rapid	rapid	k1gInSc1	rapid
Vídeň	Vídeň	k1gFnSc1	Vídeň
14	[number]	k4	14
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
1905	[number]	k4	1905
-	-	kIx~	-
Southampton	Southampton	k1gInSc1	Southampton
FC	FC	kA	FC
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
1906	[number]	k4	1906
-	-	kIx~	-
Celtic	Celtice	k1gFnPc2	Celtice
Glasgow	Glasgow	k1gNnPc2	Glasgow
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
Bayern	Bayern	k1gInSc1	Bayern
Mnichov	Mnichov	k1gInSc1	Mnichov
13	[number]	k4	13
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
1908	[number]	k4	1908
-	-	kIx~	-
Middlesborough	Middlesborough	k1gInSc1	Middlesborough
FC	FC	kA	FC
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
I.	I.	kA	I.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgInS	být
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
styk	styk	k1gInSc1	styk
přerušen	přerušit	k5eAaPmNgInS	přerušit
a	a	k8xC	a
na	na	k7c6	na
domácích	domácí	k2eAgNnPc6d1	domácí
hřištích	hřiště	k1gNnPc6	hřiště
většina	většina	k1gFnSc1	většina
družstev	družstvo	k1gNnPc2	družstvo
hraje	hrát	k5eAaImIp3nS	hrát
s	s	k7c7	s
náhradníky	náhradník	k1gMnPc7	náhradník
<g/>
.	.	kIx.	.
</s>
<s>
Slavia	Slavia	k1gFnSc1	Slavia
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1915	[number]	k4	1915
bez	bez	k7c2	bez
ztráty	ztráta	k1gFnSc2	ztráta
bodu	bod	k1gInSc2	bod
vyhrává	vyhrávat	k5eAaImIp3nS	vyhrávat
Válečné	válečný	k2eAgNnSc4d1	válečné
mistrovství	mistrovství	k1gNnSc4	mistrovství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
základní	základní	k2eAgFnSc1d1	základní
sestava	sestava	k1gFnSc1	sestava
<g/>
:	:	kIx,	:
Hlaváček	Hlaváček	k1gMnSc1	Hlaváček
<g/>
-	-	kIx~	-
<g/>
Janko	Janko	k1gMnSc1	Janko
<g/>
,	,	kIx,	,
Marway	Marway	k1gInPc1	Marway
<g/>
-	-	kIx~	-
<g/>
Loos	Loos	k1gInSc1	Loos
<g/>
,	,	kIx,	,
Zajíček	Zajíček	k1gMnSc1	Zajíček
<g/>
,	,	kIx,	,
Waldheger	Waldheger	k1gMnSc1	Waldheger
<g/>
-	-	kIx~	-
<g/>
Hájek	Hájek	k1gMnSc1	Hájek
<g/>
,	,	kIx,	,
Bělka	bělka	k1gFnSc1	bělka
<g/>
,	,	kIx,	,
Šubrt	Šubrt	k1gMnSc1	Šubrt
<g/>
,	,	kIx,	,
Prošek	Prošek	k1gMnSc1	Prošek
a	a	k8xC	a
Husák	Husák	k1gMnSc1	Husák
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
se	se	k3xPyFc4	se
odehrávala	odehrávat	k5eAaImAgFnS	odehrávat
pravidelná	pravidelný	k2eAgFnSc1d1	pravidelná
pohárová	pohárový	k2eAgFnSc1d1	pohárová
soutěž	soutěž	k1gFnSc1	soutěž
Středočeský	středočeský	k2eAgInSc1d1	středočeský
pohár	pohár	k1gInSc4	pohár
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
Slavia	Slavia	k1gFnSc1	Slavia
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
hrála	hrát	k5eAaImAgFnS	hrát
"	"	kIx"	"
<g/>
první	první	k4xOgFnPc1	první
housle	housle	k1gFnPc1	housle
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
dobu	doba	k1gFnSc4	doba
existence	existence	k1gFnSc2	existence
této	tento	k3xDgFnSc2	tento
soutěže	soutěž	k1gFnSc2	soutěž
ji	on	k3xPp3gFnSc4	on
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
celkem	celkem	k6eAd1	celkem
10	[number]	k4	10
<g/>
×	×	k?	×
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
nejvíce	nejvíce	k6eAd1	nejvíce
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1925	[number]	k4	1925
je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
profesionální	profesionální	k2eAgFnSc1d1	profesionální
ligová	ligový	k2eAgFnSc1d1	ligová
soutěž	soutěž	k1gFnSc1	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
zápase	zápas	k1gInSc6	zápas
poráží	porážet	k5eAaImIp3nS	porážet
Slavia	Slavia	k1gFnSc1	Slavia
SK	Sk	kA	Sk
Libeň	Libeň	k1gFnSc1	Libeň
9	[number]	k4	9
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
vítězí	vítězit	k5eAaImIp3nS	vítězit
i	i	k9	i
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
soutěži	soutěž	k1gFnSc6	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Stává	stávat	k5eAaImIp3nS	stávat
se	se	k3xPyFc4	se
tak	tak	k9	tak
historicky	historicky	k6eAd1	historicky
prvním	první	k4xOgMnSc7	první
mistrem	mistr	k1gMnSc7	mistr
ligy	liga	k1gFnSc2	liga
a	a	k8xC	a
získává	získávat	k5eAaImIp3nS	získávat
svůj	svůj	k3xOyFgMnSc1	svůj
2	[number]	k4	2
<g/>
.	.	kIx.	.
mistrovský	mistrovský	k2eAgInSc4d1	mistrovský
titul	titul	k1gInSc4	titul
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
už	už	k9	už
ve	v	k7c6	v
Slavii	slavie	k1gFnSc6	slavie
působí	působit	k5eAaImIp3nS	působit
možná	možná	k9	možná
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
gólman	gólman	k1gMnSc1	gólman
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
František	František	k1gMnSc1	František
Plánička	plánička	k1gFnSc1	plánička
(	(	kIx(	(
<g/>
od	od	k7c2	od
1923	[number]	k4	1923
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInPc4d1	následující
tři	tři	k4xCgInPc4	tři
soutěžní	soutěžní	k2eAgInPc4d1	soutěžní
ročníky	ročník	k1gInPc4	ročník
obsadí	obsadit	k5eAaPmIp3nS	obsadit
Slavia	Slavia	k1gFnSc1	Slavia
skvělé	skvělý	k2eAgNnSc4d1	skvělé
druhé	druhý	k4xOgNnSc4	druhý
místo	místo	k1gNnSc4	místo
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
vítězí	vítězit	k5eAaImIp3nS	vítězit
v	v	k7c6	v
posledním	poslední	k2eAgInSc6d1	poslední
ročníku	ročník	k1gInSc6	ročník
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
soutěž	soutěž	k1gFnSc1	soutěž
pojmenovaná	pojmenovaný	k2eAgFnSc1d1	pojmenovaná
Středočeská	středočeský	k2eAgFnSc1d1	Středočeská
1	[number]	k4	1
<g/>
.	.	kIx.	.
liga	liga	k1gFnSc1	liga
1928	[number]	k4	1928
<g/>
/	/	kIx~	/
<g/>
29	[number]	k4	29
<g/>
.	.	kIx.	.
</s>
<s>
Získává	získávat	k5eAaImIp3nS	získávat
tak	tak	k9	tak
svůj	svůj	k3xOyFgMnSc1	svůj
3	[number]	k4	3
<g/>
.	.	kIx.	.
mistrovský	mistrovský	k2eAgInSc4d1	mistrovský
titul	titul	k1gInSc4	titul
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
není	být	k5eNaImIp3nS	být
největším	veliký	k2eAgMnSc7d3	veliký
soupeřem	soupeř	k1gMnSc7	soupeř
Slavie	slavie	k1gFnSc2	slavie
AC	AC	kA	AC
Sparta	Sparta	k1gFnSc1	Sparta
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tehdy	tehdy	k6eAd1	tehdy
výborně	výborně	k6eAd1	výborně
hrající	hrající	k2eAgInSc1d1	hrající
klub	klub	k1gInSc1	klub
SK	Sk	kA	Sk
Viktoria	Viktoria	k1gFnSc1	Viktoria
Žižkov	Žižkov	k1gInSc1	Žižkov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezoně	sezona	k1gFnSc6	sezona
1929	[number]	k4	1929
<g/>
-	-	kIx~	-
<g/>
1930	[number]	k4	1930
končí	končit	k5eAaImIp3nS	končit
ve	v	k7c6	v
trenér	trenér	k1gMnSc1	trenér
John	John	k1gMnSc1	John
William	William	k1gInSc4	William
Madden	Maddna	k1gFnPc2	Maddna
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zde	zde	k6eAd1	zde
působil	působit	k5eAaImAgMnS	působit
od	od	k7c2	od
února	únor	k1gInSc2	únor
1905	[number]	k4	1905
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
neuvěřitelných	uvěřitelný	k2eNgNnPc2d1	neuvěřitelné
25	[number]	k4	25
let	léto	k1gNnPc2	léto
<g/>
!	!	kIx.	!
</s>
<s>
Loučení	loučení	k1gNnSc4	loučení
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
vskutku	vskutku	k9	vskutku
velkolepé	velkolepý	k2eAgNnSc1d1	velkolepé
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
hrála	hrát	k5eAaImAgFnS	hrát
dvoukolově	dvoukolově	k6eAd1	dvoukolově
dovedl	dovést	k5eAaPmAgMnS	dovést
svůj	svůj	k3xOyFgInSc4	svůj
tým	tým	k1gInSc4	tým
k	k	k7c3	k
jasnému	jasný	k2eAgNnSc3d1	jasné
prvenství	prvenství	k1gNnSc3	prvenství
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ztráty	ztráta	k1gFnSc2	ztráta
bodu	bod	k1gInSc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Připsal	připsat	k5eAaPmAgInS	připsat
si	se	k3xPyFc3	se
tak	tak	k6eAd1	tak
se	s	k7c7	s
"	"	kIx"	"
<g/>
sešívaným	sešívaný	k2eAgInSc7d1	sešívaný
<g/>
"	"	kIx"	"
týmem	tým	k1gInSc7	tým
svůj	svůj	k3xOyFgInSc4	svůj
celkem	celek	k1gInSc7	celek
4	[number]	k4	4
<g/>
.	.	kIx.	.
mistrovský	mistrovský	k2eAgInSc4d1	mistrovský
titul	titul	k1gInSc4	titul
<g/>
.	.	kIx.	.
</s>
<s>
Soutěž	soutěž	k1gFnSc1	soutěž
bez	bez	k7c2	bez
ztráty	ztráta	k1gFnSc2	ztráta
bodu	bůst	k5eAaImIp1nS	bůst
dosud	dosud	k6eAd1	dosud
žádný	žádný	k3yNgInSc1	žádný
jiný	jiný	k2eAgInSc1d1	jiný
tým	tým	k1gInSc1	tým
vyhrát	vyhrát	k5eAaPmF	vyhrát
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
Slavii	slavie	k1gFnSc3	slavie
povedlo	povést	k5eAaPmAgNnS	povést
získat	získat	k5eAaPmF	získat
jako	jako	k9	jako
prvnímu	první	k4xOgNnSc3	první
týmu	tým	k1gInSc3	tým
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
získat	získat	k5eAaPmF	získat
Mistrovský	mistrovský	k2eAgInSc4d1	mistrovský
hattrick	hattrick	k1gInSc4	hattrick
<g/>
.	.	kIx.	.
</s>
<s>
Ziskem	zisk	k1gInSc7	zisk
5	[number]	k4	5
<g/>
.	.	kIx.	.
mistrovského	mistrovský	k2eAgInSc2d1	mistrovský
titulu	titul	k1gInSc2	titul
se	se	k3xPyFc4	se
Slavia	Slavia	k1gFnSc1	Slavia
vyrovnala	vyrovnat	k5eAaPmAgFnS	vyrovnat
Spartě	Sparta	k1gFnSc3	Sparta
na	na	k7c6	na
čele	čelo	k1gNnSc6	čelo
pořadí	pořadí	k1gNnPc2	pořadí
titulů	titul	k1gInPc2	titul
<g/>
.	.	kIx.	.
</s>
<s>
Nutno	nutno	k6eAd1	nutno
dodat	dodat	k5eAaPmF	dodat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Sparta	Sparta	k1gFnSc1	Sparta
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
nejlepších	dobrý	k2eAgInPc2d3	nejlepší
klubů	klub	k1gInPc2	klub
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
ročníku	ročník	k1gInSc2	ročník
byl	být	k5eAaImAgMnS	být
přestup	přestup	k1gInSc4	přestup
Franciho	Franci	k1gMnSc2	Franci
Svobody	Svoboda	k1gMnSc2	Svoboda
do	do	k7c2	do
Sparty	Sparta	k1gFnSc2	Sparta
<g/>
.	.	kIx.	.
</s>
<s>
Hvězda	hvězda	k1gFnSc1	hvězda
sešívaných	sešívaní	k1gMnPc2	sešívaní
totiž	totiž	k9	totiž
požadovala	požadovat	k5eAaImAgFnS	požadovat
mnohem	mnohem	k6eAd1	mnohem
vyšší	vysoký	k2eAgInSc4d2	vyšší
plat	plat	k1gInSc4	plat
<g/>
,	,	kIx,	,
než	než	k8xS	než
byl	být	k5eAaImAgInS	být
výbor	výbor	k1gInSc1	výbor
Sportovního	sportovní	k2eAgInSc2d1	sportovní
klubu	klub	k1gInSc2	klub
ochoten	ochoten	k2eAgMnSc1d1	ochoten
vyplácet	vyplácet	k5eAaImF	vyplácet
<g/>
.	.	kIx.	.
</s>
<s>
Fanoušci	Fanoušek	k1gMnPc1	Fanoušek
klubu	klub	k1gInSc2	klub
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nechtěli	chtít	k5eNaImAgMnP	chtít
o	o	k7c4	o
kanonýra	kanonýr	k1gMnSc4	kanonýr
přijít	přijít	k5eAaPmF	přijít
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
uspořádali	uspořádat	k5eAaPmAgMnP	uspořádat
sbírku	sbírka	k1gFnSc4	sbírka
na	na	k7c4	na
100	[number]	k4	100
tisíc	tisíc	k4xCgInPc2	tisíc
Kčs	Kčs	kA	Kčs
a	a	k8xC	a
přestup	přestup	k1gInSc4	přestup
zvrátili	zvrátit	k5eAaPmAgMnP	zvrátit
<g/>
.	.	kIx.	.
</s>
<s>
Sestava	sestava	k1gFnSc1	sestava
mistrovského	mistrovský	k2eAgInSc2d1	mistrovský
hattricku	hattrick	k1gInSc2	hattrick
<g/>
:	:	kIx,	:
Plánička	plánička	k1gFnSc1	plánička
<g/>
,	,	kIx,	,
Ženíšek	Ženíšek	k1gMnSc1	Ženíšek
<g/>
,	,	kIx,	,
A.	A.	kA	A.
Novák	Novák	k1gMnSc1	Novák
<g/>
,	,	kIx,	,
Šimperský	Šimperský	k2eAgMnSc1d1	Šimperský
<g/>
,	,	kIx,	,
Černický	Černický	k2eAgMnSc1d1	Černický
<g/>
,	,	kIx,	,
A.	A.	kA	A.
Vodička	Vodička	k1gMnSc1	Vodička
<g/>
,	,	kIx,	,
Čambal	Čambal	k1gInSc1	Čambal
<g/>
,	,	kIx,	,
Joska	Joska	k1gFnSc1	Joska
<g/>
,	,	kIx,	,
F.	F.	kA	F.
Junek	Junek	k1gMnSc1	Junek
<g/>
,	,	kIx,	,
F.	F.	kA	F.
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
Fait	Fait	k1gMnSc1	Fait
<g/>
,	,	kIx,	,
Puč	puč	k1gInSc1	puč
<g/>
,	,	kIx,	,
Šoltys	šoltys	k1gMnSc1	šoltys
<g/>
,	,	kIx,	,
Bára	Bára	k1gFnSc1	Bára
<g/>
,	,	kIx,	,
Kloz	Kloz	k1gMnSc1	Kloz
<g/>
,	,	kIx,	,
Šubrt	Šubrt	k1gMnSc1	Šubrt
<g/>
,	,	kIx,	,
Smolka	Smolka	k1gMnSc1	Smolka
<g/>
,	,	kIx,	,
König	König	k1gMnSc1	König
<g/>
.	.	kIx.	.
</s>
<s>
Nejúspěšnější	úspěšný	k2eAgNnSc4d3	nejúspěšnější
období	období	k1gNnSc4	období
zažila	zažít	k5eAaPmAgFnS	zažít
Slavia	Slavia	k1gFnSc1	Slavia
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
získala	získat	k5eAaPmAgFnS	získat
14	[number]	k4	14
ze	z	k7c2	z
17	[number]	k4	17
uváděných	uváděný	k2eAgInPc2d1	uváděný
mistrovských	mistrovský	k2eAgInPc2d1	mistrovský
titulů	titul	k1gInPc2	titul
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1934	[number]	k4	1934
byl	být	k5eAaImAgInS	být
klub	klub	k1gInSc1	klub
hlavním	hlavní	k2eAgMnSc7d1	hlavní
dodavatelem	dodavatel	k1gMnSc7	dodavatel
stříbrné	stříbrný	k2eAgFnSc2d1	stříbrná
reprezentace	reprezentace	k1gFnSc2	reprezentace
z	z	k7c2	z
Mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
-	-	kIx~	-
ve	v	k7c6	v
finálové	finálový	k2eAgFnSc6d1	finálová
sestavě	sestava	k1gFnSc6	sestava
bylo	být	k5eAaImAgNnS	být
osm	osm	k4xCc1	osm
hráčů	hráč	k1gMnPc2	hráč
Slavie	slavie	k1gFnSc2	slavie
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
kapitána	kapitán	k1gMnSc2	kapitán
-	-	kIx~	-
legendárního	legendární	k2eAgMnSc4d1	legendární
brankáře	brankář	k1gMnSc4	brankář
Františka	František	k1gMnSc4	František
Pláničky	plánička	k1gFnSc2	plánička
a	a	k8xC	a
nejlepšího	dobrý	k2eAgMnSc2d3	nejlepší
reprezentačního	reprezentační	k2eAgMnSc2d1	reprezentační
střelce	střelec	k1gMnSc2	střelec
Československa	Československo	k1gNnSc2	Československo
(	(	kIx(	(
<g/>
do	do	k7c2	do
31	[number]	k4	31
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
Antonína	Antonín	k1gMnSc2	Antonín
Puče	puč	k1gInSc2	puč
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezoně	sezona	k1gFnSc6	sezona
končící	končící	k2eAgFnSc6d1	končící
rokem	rok	k1gInSc7	rok
1932	[number]	k4	1932
si	se	k3xPyFc3	se
Slavia	Slavia	k1gFnSc1	Slavia
dává	dávat	k5eAaImIp3nS	dávat
mistrovskou	mistrovský	k2eAgFnSc4d1	mistrovská
přestávku	přestávka	k1gFnSc4	přestávka
a	a	k8xC	a
po	po	k7c6	po
třech	tři	k4xCgNnPc6	tři
letech	léto	k1gNnPc6	léto
přenechává	přenechávat	k5eAaImIp3nS	přenechávat
titul	titul	k1gInSc4	titul
rivalovi	rival	k1gMnSc3	rival
z	z	k7c2	z
AC	AC	kA	AC
Sparta	Sparta	k1gFnSc1	Sparta
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
sezony	sezona	k1gFnSc2	sezona
následující	následující	k2eAgMnSc1d1	následující
to	ten	k3xDgNnSc1	ten
nevypadalo	vypadat	k5eNaImAgNnS	vypadat
na	na	k7c4	na
zisk	zisk	k1gInSc4	zisk
titulu	titul	k1gInSc2	titul
<g/>
,	,	kIx,	,
zvlášť	zvlášť	k6eAd1	zvlášť
když	když	k8xS	když
Slavia	Slavia	k1gFnSc1	Slavia
již	již	k6eAd1	již
v	v	k7c6	v
jarní	jarní	k2eAgFnSc6d1	jarní
části	část	k1gFnSc6	část
prohrála	prohrát	k5eAaPmAgFnS	prohrát
s	s	k7c7	s
outsiderem	outsider	k1gMnSc7	outsider
SK	Sk	kA	Sk
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
.	.	kIx.	.
</s>
<s>
Jenže	jenže	k8xC	jenže
nakonec	nakonec	k6eAd1	nakonec
titul	titul	k1gInSc4	titul
díky	díky	k7c3	díky
sérii	série	k1gFnSc3	série
výsledků	výsledek	k1gInPc2	výsledek
patřil	patřit	k5eAaImAgInS	patřit
sešívaným	sešívaný	k2eAgFnPc3d1	sešívaná
<g/>
.	.	kIx.	.
</s>
<s>
Ziskem	zisk	k1gInSc7	zisk
6	[number]	k4	6
<g/>
.	.	kIx.	.
mistrovského	mistrovský	k2eAgInSc2d1	mistrovský
titulu	titul	k1gInSc2	titul
napočala	napočal	k1gMnSc2	napočal
Slavia	Slavius	k1gMnSc2	Slavius
další	další	k2eAgInSc4d1	další
mistrovský	mistrovský	k2eAgInSc4d1	mistrovský
hattrick	hattrick	k1gInSc4	hattrick
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgFnSc4d1	následující
sezonu	sezona	k1gFnSc4	sezona
ovládla	ovládnout	k5eAaPmAgFnS	ovládnout
Slavia	Slavia	k1gFnSc1	Slavia
rozdílem	rozdíl	k1gInSc7	rozdíl
pěti	pět	k4xCc2	pět
bodů	bod	k1gInPc2	bod
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
titul	titul	k1gInSc4	titul
<g/>
)	)	kIx)	)
a	a	k8xC	a
ve	v	k7c6	v
značně	značně	k6eAd1	značně
změněné	změněný	k2eAgFnSc6d1	změněná
a	a	k8xC	a
rozšířené	rozšířený	k2eAgFnSc6d1	rozšířená
podobě	podoba	k1gFnSc6	podoba
ligy	liga	k1gFnSc2	liga
v	v	k7c6	v
sezoně	sezona	k1gFnSc6	sezona
1934	[number]	k4	1934
<g/>
/	/	kIx~	/
<g/>
35	[number]	k4	35
dokonala	dokonat	k5eAaPmAgFnS	dokonat
hattrick	hattrick	k1gInSc1	hattrick
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
titul	titul	k1gInSc1	titul
znamenal	znamenat	k5eAaImAgInS	znamenat
upevnění	upevnění	k1gNnSc4	upevnění
postu	post	k1gInSc2	post
na	na	k7c6	na
čele	čelo	k1gNnSc6	čelo
historického	historický	k2eAgInSc2d1	historický
žebříčku	žebříček	k1gInSc2	žebříček
před	před	k7c7	před
Spartou	Sparta	k1gFnSc7	Sparta
<g/>
,	,	kIx,	,
mající	mající	k2eAgInSc1d1	mající
titulů	titul	k1gInPc2	titul
šest	šest	k4xCc4	šest
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
historické	historický	k2eAgFnSc6d1	historická
periodě	perioda	k1gFnSc6	perioda
končící	končící	k2eAgFnSc7d1	končící
invazí	invaze	k1gFnSc7	invaze
nacistických	nacistický	k2eAgNnPc2d1	nacistické
vojsk	vojsko	k1gNnPc2	vojsko
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1939	[number]	k4	1939
získala	získat	k5eAaPmAgFnS	získat
Slavia	Slavia	k1gFnSc1	Slavia
ještě	ještě	k9	ještě
9	[number]	k4	9
<g/>
.	.	kIx.	.
titul	titul	k1gInSc1	titul
ze	z	k7c2	z
sezony	sezona	k1gFnSc2	sezona
1936	[number]	k4	1936
<g/>
/	/	kIx~	/
<g/>
37	[number]	k4	37
<g/>
.	.	kIx.	.
</s>
<s>
Tu	tu	k6eAd1	tu
Slavia	Slavia	k1gFnSc1	Slavia
ovládla	ovládnout	k5eAaPmAgFnS	ovládnout
rozdílem	rozdíl	k1gInSc7	rozdíl
sedmi	sedm	k4xCc2	sedm
bodů	bod	k1gInPc2	bod
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byla	být	k5eAaImAgFnS	být
velmi	velmi	k6eAd1	velmi
slušná	slušný	k2eAgFnSc1d1	slušná
porce	porce	k1gFnSc1	porce
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
také	také	k9	také
cestovalo	cestovat	k5eAaImAgNnS	cestovat
na	na	k7c4	na
zápas	zápas	k1gInSc4	zápas
ligy	liga	k1gFnSc2	liga
letadlem	letadlo	k1gNnSc7	letadlo
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
na	na	k7c4	na
zápas	zápas	k1gInSc4	zápas
s	s	k7c7	s
týmem	tým	k1gInSc7	tým
ŠK	ŠK	kA	ŠK
Rusj	Rusj	k1gInSc1	Rusj
Užhorod	Užhorod	k1gInSc1	Užhorod
<g/>
,	,	kIx,	,
reprezentující	reprezentující	k2eAgFnSc4d1	reprezentující
Podkarpatskou	podkarpatský	k2eAgFnSc4d1	Podkarpatská
Rus	Rus	k1gFnSc4	Rus
<g/>
.	.	kIx.	.
</s>
<s>
Titul	titul	k1gInSc1	titul
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
sezony	sezona	k1gFnSc2	sezona
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
vyhrát	vyhrát	k5eAaPmF	vyhrát
i	i	k9	i
čtyřem	čtyři	k4xCgMnPc3	čtyři
hráčům	hráč	k1gMnPc3	hráč
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
vyhráli	vyhrát	k5eAaPmAgMnP	vyhrát
již	již	k6eAd1	již
v	v	k7c6	v
sezoně	sezona	k1gFnSc6	sezona
1928	[number]	k4	1928
<g/>
/	/	kIx~	/
<g/>
29	[number]	k4	29
<g/>
.	.	kIx.	.
</s>
<s>
Byli	být	k5eAaImAgMnP	být
to	ten	k3xDgNnSc4	ten
František	František	k1gMnSc1	František
Plánička	plánička	k1gFnSc1	plánička
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
Puč	puč	k1gInSc1	puč
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Svoboda	Svoboda	k1gMnSc1	Svoboda
a	a	k8xC	a
Antonín	Antonín	k1gMnSc1	Antonín
Vodička	Vodička	k1gMnSc1	Vodička
<g/>
.	.	kIx.	.
</s>
<s>
Shodou	shoda	k1gFnSc7	shoda
okolností	okolnost	k1gFnPc2	okolnost
finalisté	finalista	k1gMnPc1	finalista
Mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
1934	[number]	k4	1934
<g/>
.	.	kIx.	.
</s>
<s>
Sedmnáct	sedmnáct	k4xCc1	sedmnáct
titulů	titul	k1gInPc2	titul
a	a	k8xC	a
nespočet	nespočet	k1gInSc1	nespočet
menších	malý	k2eAgFnPc2d2	menší
trofejí	trofej	k1gFnPc2	trofej
z	z	k7c2	z
domácích	domácí	k2eAgFnPc2d1	domácí
soutěží	soutěž	k1gFnPc2	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
je	být	k5eAaImIp3nS	být
možná	možná	k9	možná
největším	veliký	k2eAgInSc7d3	veliký
úspěchem	úspěch	k1gInSc7	úspěch
klubu	klub	k1gInSc2	klub
jediné	jediný	k2eAgNnSc4d1	jediné
vítězství	vítězství	k1gNnSc4	vítězství
v	v	k7c6	v
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
soutěži	soutěž	k1gFnSc6	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
Slavia	Slavia	k1gFnSc1	Slavia
získala	získat	k5eAaPmAgFnS	získat
Středoevropský	středoevropský	k2eAgInSc4d1	středoevropský
pohár	pohár	k1gInSc4	pohár
<g/>
.	.	kIx.	.
</s>
<s>
Tehdejší	tehdejší	k2eAgFnSc1d1	tehdejší
doba	doba	k1gFnSc1	doba
neznala	neznat	k5eAaImAgFnS	neznat
významnější	významný	k2eAgFnSc4d2	významnější
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
soutěž	soutěž	k1gFnSc4	soutěž
na	na	k7c6	na
území	území	k1gNnSc6	území
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
jejího	její	k3xOp3gMnSc4	její
nástupce	nástupce	k1gMnSc4	nástupce
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
Pohár	pohár	k1gInSc4	pohár
mistrů	mistr	k1gMnPc2	mistr
Evropských	evropský	k2eAgFnPc2d1	Evropská
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
soutěž	soutěž	k1gFnSc4	soutěž
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1955	[number]	k4	1955
odsunul	odsunout	k5eAaPmAgInS	odsunout
na	na	k7c6	na
"	"	kIx"	"
<g/>
druhou	druhý	k4xOgFnSc4	druhý
kolej	kolej	k1gFnSc4	kolej
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tohoto	tento	k3xDgInSc2	tento
ročníku	ročník	k1gInSc2	ročník
soutěže	soutěž	k1gFnSc2	soutěž
se	se	k3xPyFc4	se
účastnily	účastnit	k5eAaImAgInP	účastnit
týmy	tým	k1gInPc1	tým
z	z	k7c2	z
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
<g/>
,	,	kIx,	,
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
Rumunska	Rumunsko	k1gNnSc2	Rumunsko
a	a	k8xC	a
samozřejmě	samozřejmě	k6eAd1	samozřejmě
i	i	k9	i
čtyři	čtyři	k4xCgInPc1	čtyři
týmy	tým	k1gInPc1	tým
z	z	k7c2	z
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgNnSc6	první
kole	kolo	k1gNnSc6	kolo
se	se	k3xPyFc4	se
Slavia	Slavia	k1gFnSc1	Slavia
utkala	utkat	k5eAaPmAgFnS	utkat
se	s	k7c7	s
soupeřem	soupeř	k1gMnSc7	soupeř
z	z	k7c2	z
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
<g/>
,	,	kIx,	,
Beogradski	Beogradsk	k1gFnSc2	Beogradsk
SK	Sk	kA	Sk
(	(	kIx(	(
<g/>
dnešním	dnešní	k2eAgInSc6d1	dnešní
OFK	OFK	kA	OFK
Bělehrad	Bělehrad	k1gInSc1	Bělehrad
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
zápasy	zápas	k1gInPc1	zápas
patřily	patřit	k5eAaImAgInP	patřit
Slavii	slavie	k1gFnSc4	slavie
a	a	k8xC	a
výsledky	výsledek	k1gInPc7	výsledek
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
a	a	k8xC	a
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
ji	on	k3xPp3gFnSc4	on
posunuly	posunout	k5eAaPmAgFnP	posunout
do	do	k7c2	do
čtvrtfinále	čtvrtfinále	k1gNnSc2	čtvrtfinále
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
ji	on	k3xPp3gFnSc4	on
čekala	čekat	k5eAaImAgFnS	čekat
italská	italský	k2eAgFnSc1d1	italská
Ambrosiana	Ambrosiana	k1gFnSc1	Ambrosiana
Milán	Milán	k1gInSc1	Milán
<g/>
,	,	kIx,	,
nynější	nynější	k2eAgInSc1d1	nynější
Inter	Inter	k1gInSc1	Inter
Milán	Milán	k1gInSc1	Milán
<g/>
.	.	kIx.	.
</s>
<s>
Slavia	Slavia	k1gFnSc1	Slavia
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
zápase	zápas	k1gInSc6	zápas
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
snad	snad	k9	snad
až	až	k6eAd1	až
neuvěřitelným	uvěřitelný	k2eNgInSc7d1	neuvěřitelný
výsledkem	výsledek	k1gInSc7	výsledek
9	[number]	k4	9
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Odveta	odveta	k1gFnSc1	odveta
i	i	k9	i
přes	přes	k7c4	přes
prohru	prohra	k1gFnSc4	prohra
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
byla	být	k5eAaImAgFnS	být
jen	jen	k9	jen
formalitou	formalita	k1gFnSc7	formalita
<g/>
.	.	kIx.	.
</s>
<s>
Semifinále	semifinále	k1gNnSc1	semifinále
opět	opět	k6eAd1	opět
přineslo	přinést	k5eAaPmAgNnS	přinést
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Zápas	zápas	k1gInSc1	zápas
na	na	k7c6	na
hřišti	hřiště	k1gNnSc6	hřiště
FC	FC	kA	FC
Janov	Janov	k1gInSc1	Janov
znamenal	znamenat	k5eAaImAgInS	znamenat
i	i	k9	i
vinou	vinou	k7c2	vinou
několika	několik	k4yIc2	několik
zranění	zranění	k1gNnPc2	zranění
porážku	porážka	k1gFnSc4	porážka
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Odveta	odveta	k1gFnSc1	odveta
na	na	k7c6	na
hřišti	hřiště	k1gNnSc6	hřiště
Sparty	Sparta	k1gFnSc2	Sparta
se	se	k3xPyFc4	se
sešívaným	sešívaní	k1gMnPc3	sešívaní
velmi	velmi	k6eAd1	velmi
vydařila	vydařit	k5eAaPmAgNnP	vydařit
<g/>
.	.	kIx.	.
</s>
<s>
Zápas	zápas	k1gInSc1	zápas
byl	být	k5eAaImAgInS	být
sice	sice	k8xC	sice
několikrát	několikrát	k6eAd1	několikrát
přerušen	přerušen	k2eAgMnSc1d1	přerušen
kvůli	kvůli	k7c3	kvůli
výtržnostem	výtržnost	k1gFnPc3	výtržnost
Italů	Ital	k1gMnPc2	Ital
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
rozhodčí	rozhodčí	k1gMnSc1	rozhodčí
pohrozil	pohrozit	k5eAaPmAgMnS	pohrozit
i	i	k9	i
ukončením	ukončení	k1gNnSc7	ukončení
zápasu	zápas	k1gInSc2	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
nestalo	stát	k5eNaPmAgNnS	stát
a	a	k8xC	a
zápas	zápas	k1gInSc4	zápas
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
čtyřmi	čtyři	k4xCgNnPc7	čtyři
góly	gól	k1gInPc4	gól
Josef	Josef	k1gMnSc1	Josef
Bican	Bican	k1gMnSc1	Bican
<g/>
.	.	kIx.	.
</s>
<s>
Výhra	výhra	k1gFnSc1	výhra
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
domácí	domácí	k1gFnSc4	domácí
vytouženou	vytoužený	k2eAgFnSc7d1	vytoužená
vstupenkou	vstupenka	k1gFnSc7	vstupenka
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
<g/>
!	!	kIx.	!
</s>
<s>
Tam	tam	k6eAd1	tam
již	již	k6eAd1	již
čekal	čekat	k5eAaImAgInS	čekat
maďarský	maďarský	k2eAgInSc1d1	maďarský
Ferencváros	Ferencvárosa	k1gFnPc2	Ferencvárosa
Budapešť	Budapešť	k1gFnSc1	Budapešť
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
v	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
skolil	skolit	k5eAaImAgInS	skolit
celkovým	celkový	k2eAgNnSc7d1	celkové
skóre	skóre	k1gNnSc7	skóre
9	[number]	k4	9
<g/>
:	:	kIx,	:
<g/>
6	[number]	k4	6
jiný	jiný	k2eAgInSc1d1	jiný
italský	italský	k2eAgInSc1d1	italský
tým	tým	k1gInSc1	tým
Lazio	Lazio	k1gNnSc1	Lazio
Řím	Řím	k1gInSc1	Řím
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
finále	finále	k1gNnSc1	finále
na	na	k7c6	na
českém	český	k2eAgNnSc6d1	české
území	území	k1gNnSc6	území
se	se	k3xPyFc4	se
hrálo	hrát	k5eAaImAgNnS	hrát
netradičně	tradičně	k6eNd1	tradičně
na	na	k7c6	na
Strahově	Strahov	k1gInSc6	Strahov
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
byla	být	k5eAaImAgFnS	být
větší	veliký	k2eAgFnSc1d2	veliký
kapacita	kapacita	k1gFnSc1	kapacita
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
na	na	k7c4	na
zápas	zápas	k1gInSc4	zápas
povolila	povolit	k5eAaPmAgFnS	povolit
vstup	vstup	k1gInSc4	vstup
více	hodně	k6eAd2	hodně
45	[number]	k4	45
tisíc	tisíc	k4xCgInPc2	tisíc
diváků	divák	k1gMnPc2	divák
<g/>
.	.	kIx.	.
</s>
<s>
Zápas	zápas	k1gInSc1	zápas
to	ten	k3xDgNnSc1	ten
nebyl	být	k5eNaImAgInS	být
z	z	k7c2	z
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
<g/>
.	.	kIx.	.
</s>
<s>
Slavia	Slavia	k1gFnSc1	Slavia
nejdřív	dříve	k6eAd3	dříve
prohrávala	prohrávat	k5eAaImAgFnS	prohrávat
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
vedla	vést	k5eAaImAgFnS	vést
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
remizovala	remizovat	k5eAaPmAgFnS	remizovat
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
odvetu	odveta	k1gFnSc4	odveta
nepříliš	příliš	k6eNd1	příliš
pozitivní	pozitivní	k2eAgInSc4d1	pozitivní
výhled	výhled	k1gInSc4	výhled
<g/>
.	.	kIx.	.
</s>
<s>
Nabízela	nabízet	k5eAaImAgFnS	nabízet
se	se	k3xPyFc4	se
paralela	paralela	k1gFnSc1	paralela
s	s	k7c7	s
prvním	první	k4xOgNnSc7	první
finále	finále	k1gNnSc7	finále
před	před	k7c7	před
devíti	devět	k4xCc2	devět
lety	léto	k1gNnPc7	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
sešívaní	sešívaní	k1gMnPc1	sešívaní
podlehli	podlehnout	k5eAaPmAgMnP	podlehnout
jinému	jiný	k2eAgInSc3d1	jiný
maďarskému	maďarský	k2eAgInSc3d1	maďarský
celku	celek	k1gInSc3	celek
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
zápas	zápas	k1gInSc1	zápas
v	v	k7c6	v
metropoli	metropol	k1gFnSc6	metropol
Budapešti	Budapešť	k1gFnSc6	Budapešť
se	se	k3xPyFc4	se
hrál	hrát	k5eAaImAgMnS	hrát
v	v	k7c6	v
neděli	neděle	k1gFnSc6	neděle
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
stadionu	stadion	k1gInSc6	stadion
bylo	být	k5eAaImAgNnS	být
35	[number]	k4	35
tisíc	tisíc	k4xCgInPc2	tisíc
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
6	[number]	k4	6
tisíc	tisíc	k4xCgInPc2	tisíc
Čechů	Čech	k1gMnPc2	Čech
<g/>
.	.	kIx.	.
</s>
<s>
Slavia	Slavia	k1gFnSc1	Slavia
podala	podat	k5eAaPmAgFnS	podat
o	o	k7c4	o
mnoho	mnoho	k6eAd1	mnoho
lepší	dobrý	k2eAgInSc4d2	lepší
výkon	výkon	k1gInSc4	výkon
než	než	k8xS	než
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
zápase	zápas	k1gInSc6	zápas
a	a	k8xC	a
zaslouženě	zaslouženě	k6eAd1	zaslouženě
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Vysněnou	vysněný	k2eAgFnSc4d1	vysněná
trofej	trofej	k1gFnSc4	trofej
pak	pak	k6eAd1	pak
převzal	převzít	k5eAaPmAgMnS	převzít
kapitán	kapitán	k1gMnSc1	kapitán
červenobílých	červenobílý	k2eAgInPc6d1	červenobílý
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Daučík	Daučík	k1gInSc1	Daučík
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
byla	být	k5eAaImAgFnS	být
dominantním	dominantní	k2eAgInSc7d1	dominantní
klubem	klub	k1gInSc7	klub
v	v	k7c6	v
protektorátní	protektorátní	k2eAgFnSc6d1	protektorátní
lize	liga	k1gFnSc6	liga
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zářil	zářit	k5eAaImAgMnS	zářit
především	především	k9	především
kanonýr	kanonýr	k1gMnSc1	kanonýr
Josef	Josef	k1gMnSc1	Josef
Bican	Bican	k1gMnSc1	Bican
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
neměl	mít	k5eNaImAgMnS	mít
mezi	mezi	k7c7	mezi
fotbalovými	fotbalový	k2eAgInPc7d1	fotbalový
kluby	klub	k1gInPc7	klub
v	v	k7c6	v
tehdejším	tehdejší	k2eAgInSc6d1	tehdejší
Protektorátu	protektorát	k1gInSc6	protektorát
konkurenci	konkurence	k1gFnSc4	konkurence
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
však	však	k9	však
následoval	následovat	k5eAaImAgInS	následovat
ústup	ústup	k1gInSc1	ústup
ze	z	k7c2	z
slávy	sláva	k1gFnSc2	sláva
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
únorovém	únorový	k2eAgInSc6d1	únorový
převratu	převrat	k1gInSc6	převrat
a	a	k8xC	a
nucené	nucený	k2eAgFnSc6d1	nucená
transformaci	transformace	k1gFnSc6	transformace
české	český	k2eAgFnSc2d1	Česká
klubové	klubový	k2eAgFnSc2d1	klubová
kopané	kopaná	k1gFnSc2	kopaná
zažívala	zažívat	k5eAaImAgFnS	zažívat
Slavia	Slavia	k1gFnSc1	Slavia
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
a	a	k8xC	a
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	let	k1gInPc6	let
těžké	těžký	k2eAgInPc1d1	těžký
časy	čas	k1gInPc1	čas
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
sestupů	sestup	k1gInPc2	sestup
do	do	k7c2	do
2	[number]	k4	2
<g/>
.	.	kIx.	.
ligy	liga	k1gFnSc2	liga
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
prvoligového	prvoligový	k2eAgNnSc2d1	prvoligové
popředí	popředí	k1gNnSc2	popředí
se	se	k3xPyFc4	se
dostává	dostávat	k5eAaImIp3nS	dostávat
Slavia	Slavia	k1gFnSc1	Slavia
opět	opět	k6eAd1	opět
až	až	k6eAd1	až
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
klíčovými	klíčový	k2eAgMnPc7d1	klíčový
hráči	hráč	k1gMnPc1	hráč
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
jsou	být	k5eAaImIp3nP	být
zejména	zejména	k9	zejména
František	František	k1gMnSc1	František
Veselý	Veselý	k1gMnSc1	Veselý
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
hráči	hráč	k1gMnPc1	hráč
<g/>
,	,	kIx,	,
např.	např.	kA	např.
pozdější	pozdní	k2eAgMnSc1d2	pozdější
úspěšný	úspěšný	k2eAgMnSc1d1	úspěšný
trenér	trenér	k1gMnSc1	trenér
František	František	k1gMnSc1	František
Cipro	Cipro	k1gMnSc1	Cipro
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
titul	titul	k1gInSc4	titul
však	však	k9	však
Slavia	Slavia	k1gFnSc1	Slavia
nedosáhla	dosáhnout	k5eNaPmAgFnS	dosáhnout
a	a	k8xC	a
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
přišel	přijít	k5eAaPmAgMnS	přijít
opět	opět	k6eAd1	opět
jistý	jistý	k2eAgInSc4d1	jistý
výsledkový	výsledkový	k2eAgInSc4d1	výsledkový
ústup	ústup	k1gInSc4	ústup
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1989	[number]	k4	1989
se	se	k3xPyFc4	se
hráči	hráč	k1gMnPc1	hráč
Slavie	slavie	k1gFnSc2	slavie
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
podpořit	podpořit	k5eAaPmF	podpořit
Sametovou	sametový	k2eAgFnSc4d1	sametová
revoluci	revoluce	k1gFnSc4	revoluce
stávkou	stávka	k1gFnSc7	stávka
-	-	kIx~	-
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
nastoupit	nastoupit	k5eAaPmF	nastoupit
k	k	k7c3	k
utkání	utkání	k1gNnSc3	utkání
s	s	k7c7	s
Chebem	Cheb	k1gInSc7	Cheb
(	(	kIx(	(
<g/>
hráči	hráč	k1gMnPc1	hráč
Chebu	Cheb	k1gInSc2	Cheb
se	se	k3xPyFc4	se
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
ke	k	k7c3	k
stávce	stávka	k1gFnSc3	stávka
připojili	připojit	k5eAaPmAgMnP	připojit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pádu	pád	k1gInSc6	pád
komunistického	komunistický	k2eAgInSc2d1	komunistický
režimu	režim	k1gInSc2	režim
se	se	k3xPyFc4	se
Slavia	Slavia	k1gFnSc1	Slavia
(	(	kIx(	(
<g/>
přejmenovaná	přejmenovaný	k2eAgFnSc1d1	přejmenovaná
z	z	k7c2	z
TJ	tj	kA	tj
Slavia	Slavia	k1gFnSc1	Slavia
zpět	zpět	k6eAd1	zpět
na	na	k7c6	na
SK	Sk	kA	Sk
Slavia	Slavia	k1gFnSc1	Slavia
<g/>
)	)	kIx)	)
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
plnohodnotně	plnohodnotně	k6eAd1	plnohodnotně
vrátila	vrátit	k5eAaPmAgFnS	vrátit
na	na	k7c4	na
výsluní	výsluní	k1gNnSc4	výsluní
československého	československý	k2eAgNnSc2d1	Československé
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
českého	český	k2eAgInSc2d1	český
fotbalu	fotbal	k1gInSc2	fotbal
<g/>
.	.	kIx.	.
</s>
<s>
Pomohly	pomoct	k5eAaPmAgFnP	pomoct
jí	on	k3xPp3gFnSc3	on
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
zejména	zejména	k9	zejména
finance	finance	k1gFnPc1	finance
od	od	k7c2	od
česko-amerického	českomerický	k2eAgMnSc2d1	česko-americký
podnikatele	podnikatel	k1gMnSc2	podnikatel
Borise	Boris	k1gMnSc2	Boris
Korbela	Korbel	k1gMnSc2	Korbel
(	(	kIx(	(
<g/>
celkem	celkem	k6eAd1	celkem
asi	asi	k9	asi
180	[number]	k4	180
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Korbel	Korbel	k1gMnSc1	Korbel
přišel	přijít	k5eAaPmAgMnS	přijít
s	s	k7c7	s
úmyslem	úmysl	k1gInSc7	úmysl
vybudovat	vybudovat	k5eAaPmF	vybudovat
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
silný	silný	k2eAgInSc1d1	silný
fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
by	by	kYmCp3nS	by
konkuroval	konkurovat	k5eAaImAgMnS	konkurovat
evropským	evropský	k2eAgMnSc7d1	evropský
velkoklubům	velkokluba	k1gMnPc3	velkokluba
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
jeho	jeho	k3xOp3gFnSc4	jeho
nabídku	nabídka	k1gFnSc4	nabídka
odmítlo	odmítnout	k5eAaPmAgNnS	odmítnout
vedení	vedení	k1gNnSc1	vedení
Sparty	Sparta	k1gFnSc2	Sparta
<g/>
,	,	kIx,	,
obrátil	obrátit	k5eAaPmAgMnS	obrátit
se	se	k3xPyFc4	se
na	na	k7c4	na
Slavii	slavie	k1gFnSc4	slavie
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
návrh	návrh	k1gInSc1	návrh
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
jeho	jeho	k3xOp3gInPc3	jeho
penězům	peníze	k1gInPc3	peníze
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
Slavii	slavie	k1gFnSc3	slavie
poskládat	poskládat	k5eAaPmF	poskládat
silný	silný	k2eAgInSc4d1	silný
kádr	kádr	k1gInSc4	kádr
<g/>
;	;	kIx,	;
klub	klub	k1gInSc1	klub
tehdy	tehdy	k6eAd1	tehdy
posílil	posílit	k5eAaPmAgInS	posílit
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
sparťanský	sparťanský	k2eAgMnSc1d1	sparťanský
dorostenec	dorostenec	k1gMnSc1	dorostenec
Patrik	Patrik	k1gMnSc1	Patrik
Berger	Berger	k1gMnSc1	Berger
<g/>
,	,	kIx,	,
ostravský	ostravský	k2eAgMnSc1d1	ostravský
záložník	záložník	k1gMnSc1	záložník
Radim	Radim	k1gMnSc1	Radim
Nečas	Nečas	k1gMnSc1	Nečas
<g/>
,	,	kIx,	,
za	za	k7c4	za
kterého	který	k3yIgMnSc4	který
Slavia	Slavia	k1gFnSc1	Slavia
zaplatila	zaplatit	k5eAaPmAgFnS	zaplatit
na	na	k7c4	na
tehdejší	tehdejší	k2eAgFnSc4d1	tehdejší
dobu	doba	k1gFnSc4	doba
neuvěřitelných	uvěřitelný	k2eNgInPc2d1	neuvěřitelný
24	[number]	k4	24
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
<g/>
,	,	kIx,	,
vítěz	vítěz	k1gMnSc1	vítěz
Ligy	liga	k1gFnSc2	liga
mistrů	mistr	k1gMnPc2	mistr
Dragiša	Dragiša	k1gMnSc1	Dragiša
Binič	Binič	k1gMnSc1	Binič
<g/>
,	,	kIx,	,
a	a	k8xC	a
olympijský	olympijský	k2eAgMnSc1d1	olympijský
vítěz	vítěz	k1gMnSc1	vítěz
Vladimír	Vladimír	k1gMnSc1	Vladimír
Tatarčuk	Tatarčuk	k1gMnSc1	Tatarčuk
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
kádru	kádr	k1gInSc2	kádr
dospělých	dospělí	k1gMnPc2	dospělí
se	se	k3xPyFc4	se
prosadil	prosadit	k5eAaPmAgMnS	prosadit
mladý	mladý	k2eAgMnSc1d1	mladý
útočník	útočník	k1gMnSc1	útočník
Pavel	Pavel	k1gMnSc1	Pavel
Kuka	Kuka	k1gMnSc1	Kuka
<g/>
.	.	kIx.	.
</s>
<s>
Titul	titul	k1gInSc1	titul
se	se	k3xPyFc4	se
však	však	k9	však
tehdy	tehdy	k6eAd1	tehdy
získat	získat	k5eAaPmF	získat
nepodařilo	podařit	k5eNaPmAgNnS	podařit
<g/>
,	,	kIx,	,
a	a	k8xC	a
Slavia	Slavia	k1gFnSc1	Slavia
se	se	k3xPyFc4	se
neprosadila	prosadit	k5eNaPmAgFnS	prosadit
ani	ani	k9	ani
v	v	k7c6	v
pohárové	pohárový	k2eAgFnSc6d1	pohárová
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
sezóny	sezóna	k1gFnSc2	sezóna
1992	[number]	k4	1992
<g/>
/	/	kIx~	/
<g/>
93	[number]	k4	93
se	se	k3xPyFc4	se
Korbel	Korbel	k1gMnSc1	Korbel
vzdal	vzdát	k5eAaPmAgMnS	vzdát
funkce	funkce	k1gFnPc4	funkce
v	v	k7c6	v
klubu	klub	k1gInSc6	klub
a	a	k8xC	a
přestal	přestat	k5eAaPmAgInS	přestat
klub	klub	k1gInSc1	klub
finančně	finančně	k6eAd1	finančně
podporovat	podporovat	k5eAaImF	podporovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
navýšení	navýšení	k1gNnSc1	navýšení
vlastního	vlastní	k2eAgInSc2d1	vlastní
kapitálu	kapitál	k1gInSc2	kapitál
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yRgNnSc6	který
získali	získat	k5eAaPmAgMnP	získat
čtvrtinové	čtvrtinový	k2eAgInPc4d1	čtvrtinový
podíly	podíl	k1gInPc4	podíl
v	v	k7c6	v
klubu	klub	k1gInSc6	klub
investiční	investiční	k2eAgFnSc1d1	investiční
společnost	společnost	k1gFnSc1	společnost
PPF	PPF	kA	PPF
a	a	k8xC	a
podnikatel	podnikatel	k1gMnSc1	podnikatel
Milan	Milan	k1gMnSc1	Milan
Vinkler	Vinkler	k1gMnSc1	Vinkler
<g/>
.	.	kIx.	.
</s>
<s>
Vinkler	Vinkler	k1gMnSc1	Vinkler
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
na	na	k7c4	na
několik	několik	k4yIc4	několik
týdnů	týden	k1gInPc2	týden
jednatelem	jednatel	k1gMnSc7	jednatel
klubu	klub	k1gInSc2	klub
<g/>
,	,	kIx,	,
po	po	k7c6	po
vzetí	vzetí	k1gNnSc6	vzetí
do	do	k7c2	do
vazby	vazba	k1gFnSc2	vazba
kvůli	kvůli	k7c3	kvůli
údajnému	údajný	k2eAgNnSc3d1	údajné
falšování	falšování	k1gNnSc3	falšování
směnky	směnka	k1gFnSc2	směnka
vystavené	vystavená	k1gFnSc2	vystavená
Tarekem	Tarek	k1gInSc7	Tarek
Becharou	Bechara	k1gFnSc7	Bechara
ho	on	k3xPp3gMnSc4	on
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
nahradil	nahradit	k5eAaPmAgMnS	nahradit
Vladimír	Vladimír	k1gMnSc1	Vladimír
Leška	Leška	k1gMnSc1	Leška
<g/>
.	.	kIx.	.
</s>
<s>
Vinklerův	Vinklerův	k2eAgInSc4d1	Vinklerův
podíl	podíl	k1gInSc4	podíl
odkoupila	odkoupit	k5eAaPmAgFnS	odkoupit
společnost	společnost	k1gFnSc1	společnost
DR	dr	kA	dr
<g/>
.	.	kIx.	.
<g/>
AG	AG	kA	AG
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
vlastnili	vlastnit	k5eAaImAgMnP	vlastnit
Petr	Petr	k1gMnSc1	Petr
Kellner	Kellner	k1gMnSc1	Kellner
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Czaban	Czaban	k1gMnSc1	Czaban
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Jiskra	jiskra	k1gFnSc1	jiskra
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
Krejčí	Krejčí	k1gMnSc1	Krejčí
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
Bartoníček	Bartoníček	k1gMnSc1	Bartoníček
a	a	k8xC	a
PPF	PPF	kA	PPF
<g/>
.	.	kIx.	.
</s>
<s>
PPF	PPF	kA	PPF
následně	následně	k6eAd1	následně
podíly	podíl	k1gInPc4	podíl
těchto	tento	k3xDgMnPc2	tento
podnikatelů	podnikatel	k1gMnPc2	podnikatel
vykoupila	vykoupit	k5eAaPmAgFnS	vykoupit
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	s	k7c7	s
vlastníkem	vlastník	k1gMnSc7	vlastník
50	[number]	k4	50
%	%	kIx~	%
podílu	podíl	k1gInSc2	podíl
ve	v	k7c6	v
Slavii	slavie	k1gFnSc6	slavie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
samostatné	samostatný	k2eAgFnSc6d1	samostatná
české	český	k2eAgFnSc6d1	Česká
lize	liga	k1gFnSc6	liga
nedosáhla	dosáhnout	k5eNaPmAgFnS	dosáhnout
Slavia	Slavia	k1gFnSc1	Slavia
na	na	k7c4	na
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
příčku	příčka	k1gFnSc4	příčka
ani	ani	k8xC	ani
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
1993	[number]	k4	1993
<g/>
/	/	kIx~	/
<g/>
94	[number]	k4	94
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
o	o	k7c4	o
sezónu	sezóna	k1gFnSc4	sezóna
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
třebaže	třebaže	k8xS	třebaže
po	po	k7c6	po
podzimu	podzim	k1gInSc6	podzim
vedla	vést	k5eAaImAgFnS	vést
ligu	liga	k1gFnSc4	liga
o	o	k7c4	o
sedm	sedm	k4xCc4	sedm
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
v	v	k7c6	v
evropských	evropský	k2eAgInPc6d1	evropský
pohárech	pohár	k1gInPc6	pohár
se	se	k3xPyFc4	se
nepodařilo	podařit	k5eNaPmAgNnS	podařit
přejít	přejít	k5eAaPmF	přejít
přes	přes	k7c4	přes
první	první	k4xOgNnSc4	první
kolo	kolo	k1gNnSc4	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Zlom	zlom	k1gInSc1	zlom
přišel	přijít	k5eAaPmAgInS	přijít
až	až	k9	až
v	v	k7c6	v
památné	památný	k2eAgFnSc6d1	památná
sezóně	sezóna	k1gFnSc6	sezóna
1995	[number]	k4	1995
<g/>
/	/	kIx~	/
<g/>
96	[number]	k4	96
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Slavia	Slavia	k1gFnSc1	Slavia
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
trenéra	trenér	k1gMnSc2	trenér
Františka	František	k1gMnSc2	František
Cipra	Cipro	k1gMnSc2	Cipro
získala	získat	k5eAaPmAgFnS	získat
po	po	k7c6	po
49	[number]	k4	49
letech	let	k1gInPc6	let
mistrovský	mistrovský	k2eAgInSc4d1	mistrovský
titul	titul	k1gInSc4	titul
a	a	k8xC	a
došla	dojít	k5eAaPmAgFnS	dojít
až	až	k9	až
do	do	k7c2	do
semifinále	semifinále	k1gNnSc2	semifinále
Poháru	pohár	k1gInSc2	pohár
UEFA	UEFA	kA	UEFA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hráči	hráč	k1gMnPc1	hráč
Slavie	slavie	k1gFnSc2	slavie
(	(	kIx(	(
<g/>
Jan	Jan	k1gMnSc1	Jan
Suchopárek	Suchopárek	k1gMnSc1	Suchopárek
<g/>
,	,	kIx,	,
Radek	Radek	k1gMnSc1	Radek
Bejbl	Bejbl	k1gMnSc1	Bejbl
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Poborský	Poborský	k2eAgMnSc1d1	Poborský
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Šmicer	Šmicer	k1gMnSc1	Šmicer
<g/>
)	)	kIx)	)
také	také	k9	také
tvořili	tvořit	k5eAaImAgMnP	tvořit
jádro	jádro	k1gNnSc4	jádro
České	český	k2eAgFnSc2d1	Česká
reprezentace	reprezentace	k1gFnSc2	reprezentace
na	na	k7c6	na
Euru	euro	k1gNnSc6	euro
96	[number]	k4	96
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
této	tento	k3xDgFnSc6	tento
úspěšné	úspěšný	k2eAgFnSc6d1	úspěšná
sezóně	sezóna	k1gFnSc6	sezóna
však	však	k9	však
byli	být	k5eAaImAgMnP	být
tito	tento	k3xDgMnPc1	tento
hráči	hráč	k1gMnPc1	hráč
rozprodáni	rozprodán	k2eAgMnPc1d1	rozprodán
<g/>
,	,	kIx,	,
Slavia	Slavia	k1gFnSc1	Slavia
prohrála	prohrát	k5eAaPmAgFnS	prohrát
v	v	k7c6	v
kvalifikaci	kvalifikace	k1gFnSc6	kvalifikace
Ligy	liga	k1gFnSc2	liga
mistrů	mistr	k1gMnPc2	mistr
celkově	celkově	k6eAd1	celkově
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
6	[number]	k4	6
a	a	k8xC	a
brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
rozloučila	rozloučit	k5eAaPmAgFnS	rozloučit
i	i	k9	i
s	s	k7c7	s
pohárem	pohár	k1gInSc7	pohár
UEFA	UEFA	kA	UEFA
<g/>
.	.	kIx.	.
</s>
<s>
Mistrovský	mistrovský	k2eAgInSc1d1	mistrovský
titul	titul	k1gInSc1	titul
se	se	k3xPyFc4	se
obhájit	obhájit	k5eAaPmF	obhájit
nepodařilo	podařit	k5eNaPmAgNnS	podařit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Slavia	Slavia	k1gFnSc1	Slavia
alespoň	alespoň	k9	alespoň
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
domácí	domácí	k2eAgInSc4d1	domácí
pohár	pohár	k1gInSc4	pohár
<g/>
.	.	kIx.	.
</s>
<s>
Nepodařilo	podařit	k5eNaPmAgNnS	podařit
se	se	k3xPyFc4	se
také	také	k9	také
-	-	kIx~	-
i	i	k9	i
přes	přes	k7c4	přes
mnohá	mnohý	k2eAgNnPc4d1	mnohé
ujišťování	ujišťování	k1gNnPc4	ujišťování
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
vedení	vedení	k1gNnSc2	vedení
Slavie	slavie	k1gFnSc2	slavie
-	-	kIx~	-
zahájit	zahájit	k5eAaPmF	zahájit
výstavbu	výstavba	k1gFnSc4	výstavba
nového	nový	k2eAgInSc2d1	nový
stadionu	stadion	k1gInSc2	stadion
místo	místo	k7c2	místo
nevyhovujícího	vyhovující	k2eNgInSc2d1	nevyhovující
areálu	areál	k1gInSc2	areál
v	v	k7c6	v
Edenu	Eden	k1gInSc6	Eden
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1997	[number]	k4	1997
Slavii	slavie	k1gFnSc6	slavie
koupila	koupit	k5eAaPmAgFnS	koupit
britská	britský	k2eAgFnSc1d1	britská
investiční	investiční	k2eAgFnSc1d1	investiční
společnost	společnost	k1gFnSc1	společnost
ENIC	ENIC	kA	ENIC
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
zároveň	zároveň	k6eAd1	zároveň
koupila	koupit	k5eAaPmAgFnS	koupit
podíly	podíl	k1gInPc4	podíl
v	v	k7c6	v
několika	několik	k4yIc6	několik
dalších	další	k2eAgInPc6d1	další
významných	významný	k2eAgInPc6d1	významný
klubech	klub	k1gInPc6	klub
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Glasgow	Glasgow	k1gNnSc1	Glasgow
Rangers	Rangers	k1gInSc1	Rangers
<g/>
,	,	kIx,	,
Valencia	Valencia	k1gFnSc1	Valencia
CF	CF	kA	CF
<g/>
,	,	kIx,	,
Tottenham	Tottenham	k1gInSc1	Tottenham
Hotspur	Hotspura	k1gFnPc2	Hotspura
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
patrně	patrně	k6eAd1	patrně
s	s	k7c7	s
úmyslem	úmysl	k1gInSc7	úmysl
založit	založit	k5eAaPmF	založit
nadnárodní	nadnárodní	k2eAgFnSc4d1	nadnárodní
fotbalovou	fotbalový	k2eAgFnSc4d1	fotbalová
ligu	liga	k1gFnSc4	liga
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
by	by	kYmCp3nS	by
konkurovala	konkurovat	k5eAaImAgFnS	konkurovat
Lize	liga	k1gFnSc6	liga
mistrů	mistr	k1gMnPc2	mistr
UEFA	UEFA	kA	UEFA
<g/>
.	.	kIx.	.
</s>
<s>
Ředitelem	ředitel	k1gMnSc7	ředitel
klubu	klub	k1gInSc2	klub
zůstal	zůstat	k5eAaPmAgMnS	zůstat
Vladimír	Vladimír	k1gMnSc1	Vladimír
Leška	Leška	k1gMnSc1	Leška
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c4	pod
ENIC	ENIC	kA	ENIC
se	se	k3xPyFc4	se
Slavia	Slavia	k1gFnSc1	Slavia
držela	držet	k5eAaImAgFnS	držet
v	v	k7c6	v
popředí	popředí	k1gNnSc6	popředí
ligové	ligový	k2eAgFnSc2d1	ligová
tabulky	tabulka	k1gFnSc2	tabulka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nepodařilo	podařit	k5eNaPmAgNnS	podařit
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
získat	získat	k5eAaPmF	získat
český	český	k2eAgInSc4d1	český
ligový	ligový	k2eAgInSc4d1	ligový
titul	titul	k1gInSc4	titul
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1999	[number]	k4	1999
a	a	k8xC	a
2002	[number]	k4	2002
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
vítězem	vítěz	k1gMnSc7	vítěz
domácího	domácí	k2eAgInSc2d1	domácí
poháru	pohár	k1gInSc2	pohár
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
evropské	evropský	k2eAgFnSc6d1	Evropská
scéně	scéna	k1gFnSc6	scéna
Slavia	Slavia	k1gFnSc1	Slavia
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
několik	několik	k4yIc4	několik
úspěchů	úspěch	k1gInPc2	úspěch
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
postup	postup	k1gInSc1	postup
do	do	k7c2	do
čtvrtfinále	čtvrtfinále	k1gNnSc2	čtvrtfinále
Poháru	pohár	k1gInSc2	pohár
vítězů	vítěz	k1gMnPc2	vítěz
poháru	pohár	k1gInSc2	pohár
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
či	či	k8xC	či
postup	postup	k1gInSc4	postup
do	do	k7c2	do
čtvrtfinále	čtvrtfinále	k1gNnSc2	čtvrtfinále
poháru	pohár	k1gInSc2	pohár
UEFA	UEFA	kA	UEFA
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nikdy	nikdy	k6eAd1	nikdy
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
však	však	k9	však
nepovedlo	povést	k5eNaPmAgNnS	povést
postoupit	postoupit	k5eAaPmF	postoupit
do	do	k7c2	do
hlavní	hlavní	k2eAgFnSc2d1	hlavní
soutěže	soutěž	k1gFnSc2	soutěž
Ligy	liga	k1gFnSc2	liga
mistrů	mistr	k1gMnPc2	mistr
-	-	kIx~	-
nejblíže	blízce	k6eAd3	blízce
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
2000	[number]	k4	2000
<g/>
/	/	kIx~	/
<g/>
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
po	po	k7c6	po
výhře	výhra	k1gFnSc6	výhra
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
na	na	k7c6	na
hřišti	hřiště	k1gNnSc6	hřiště
Šachťoru	Šachťor	k1gInSc2	Šachťor
Doněck	Doněck	k1gInSc1	Doněck
zkolabovala	zkolabovat	k5eAaPmAgFnS	zkolabovat
v	v	k7c6	v
domácí	domácí	k2eAgFnSc6d1	domácí
odvetě	odveta	k1gFnSc6	odveta
v	v	k7c6	v
posledních	poslední	k2eAgFnPc6d1	poslední
vteřinách	vteřina	k1gFnPc6	vteřina
zápasu	zápas	k1gInSc2	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
dubna	duben	k1gInSc2	duben
2005	[number]	k4	2005
trénoval	trénovat	k5eAaImAgMnS	trénovat
Slavii	slavie	k1gFnSc4	slavie
Karel	Karel	k1gMnSc1	Karel
Jarolím	Jarolí	k1gNnPc3	Jarolí
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
sezóny	sezóna	k1gFnSc2	sezóna
2000	[number]	k4	2000
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
hrála	hrát	k5eAaImAgFnS	hrát
Slavia	Slavia	k1gFnSc1	Slavia
svá	svůj	k3xOyFgNnPc4	svůj
domácí	domácí	k2eAgNnPc4d1	domácí
utkání	utkání	k1gNnPc4	utkání
na	na	k7c6	na
pronajímaném	pronajímaný	k2eAgInSc6d1	pronajímaný
Stadionu	stadion	k1gInSc6	stadion
Evžena	Evžen	k1gMnSc2	Evžen
Rošického	Rošický	k2eAgMnSc2d1	Rošický
na	na	k7c6	na
pražském	pražský	k2eAgInSc6d1	pražský
Strahově	Strahov	k1gInSc6	Strahov
<g/>
.	.	kIx.	.
</s>
<s>
Stadion	stadion	k1gInSc1	stadion
vyhovoval	vyhovovat	k5eAaImAgInS	vyhovovat
moderním	moderní	k2eAgInPc3d1	moderní
předpisům	předpis	k1gInPc3	předpis
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
byl	být	k5eAaImAgInS	být
obtížně	obtížně	k6eAd1	obtížně
dostupný	dostupný	k2eAgInSc1d1	dostupný
a	a	k8xC	a
poskytoval	poskytovat	k5eAaImAgInS	poskytovat
minimální	minimální	k2eAgNnSc4d1	minimální
divácké	divácký	k2eAgNnSc4d1	divácké
pohodlí	pohodlí	k1gNnSc4	pohodlí
<g/>
;	;	kIx,	;
zápasy	zápas	k1gInPc1	zápas
Slavie	slavie	k1gFnSc2	slavie
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
často	často	k6eAd1	často
hrály	hrát	k5eAaImAgInP	hrát
před	před	k7c7	před
nízkou	nízký	k2eAgFnSc7d1	nízká
diváckou	divácký	k2eAgFnSc7d1	divácká
návštěvou	návštěva	k1gFnSc7	návštěva
<g/>
.	.	kIx.	.
</s>
<s>
Starý	starý	k2eAgInSc1d1	starý
stadion	stadion	k1gInSc1	stadion
byl	být	k5eAaImAgInS	být
zbourán	zbourat	k5eAaPmNgInS	zbourat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2003	[number]	k4	2003
až	až	k9	až
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c4	na
výstavbu	výstavba	k1gFnSc4	výstavba
nového	nový	k2eAgInSc2d1	nový
se	se	k3xPyFc4	se
nedařilo	dařit	k5eNaImAgNnS	dařit
sehnat	sehnat	k5eAaPmF	sehnat
finanční	finanční	k2eAgInPc4d1	finanční
prostředky	prostředek	k1gInPc4	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
stadion	stadion	k1gInSc1	stadion
v	v	k7c6	v
Edenu	Eden	k1gInSc6	Eden
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
stavět	stavět	k5eAaImF	stavět
až	až	k9	až
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
majoritními	majoritní	k2eAgMnPc7d1	majoritní
akcionáři	akcionář	k1gMnPc1	akcionář
klubu	klub	k1gInSc2	klub
stali	stát	k5eAaPmAgMnP	stát
členové	člen	k1gMnPc1	člen
představenstva	představenstvo	k1gNnSc2	představenstvo
Petr	Petr	k1gMnSc1	Petr
Doležal	Doležal	k1gMnSc1	Doležal
a	a	k8xC	a
Tomáš	Tomáš	k1gMnSc1	Tomáš
Rosen	rosen	k2eAgMnSc1d1	rosen
<g/>
.	.	kIx.	.
</s>
<s>
Sezónu	sezóna	k1gFnSc4	sezóna
2006	[number]	k4	2006
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
Slavia	Slavia	k1gFnSc1	Slavia
dokončila	dokončit	k5eAaPmAgFnS	dokončit
na	na	k7c4	na
2	[number]	k4	2
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
a	a	k8xC	a
kvalifikovala	kvalifikovat	k5eAaBmAgFnS	kvalifikovat
se	se	k3xPyFc4	se
tak	tak	k9	tak
do	do	k7c2	do
druhého	druhý	k4xOgNnSc2	druhý
předkola	předkolo	k1gNnSc2	předkolo
Ligy	liga	k1gFnSc2	liga
mistrů	mistr	k1gMnPc2	mistr
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgFnSc1d1	následující
sezóna	sezóna	k1gFnSc1	sezóna
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nejúspěšnější	úspěšný	k2eAgFnSc1d3	nejúspěšnější
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
Slavie	slavie	k1gFnSc2	slavie
-	-	kIx~	-
Slavia	Slavia	k1gFnSc1	Slavia
posílila	posílit	k5eAaPmAgFnS	posílit
kádr	kádr	k1gInSc4	kádr
a	a	k8xC	a
na	na	k7c4	na
šestý	šestý	k4xOgInSc4	šestý
pokus	pokus	k1gInSc4	pokus
prošla	projít	k5eAaPmAgFnS	projít
do	do	k7c2	do
hlavní	hlavní	k2eAgFnSc2d1	hlavní
soutěže	soutěž	k1gFnSc2	soutěž
Ligy	liga	k1gFnSc2	liga
mistrů	mistr	k1gMnPc2	mistr
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
obsadila	obsadit	k5eAaPmAgFnS	obsadit
třetí	třetí	k4xOgFnSc1	třetí
místo	místo	k7c2	místo
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
a	a	k8xC	a
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
pohárem	pohár	k1gInSc7	pohár
UEFA	UEFA	kA	UEFA
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
2008	[number]	k4	2008
byl	být	k5eAaImAgInS	být
slavnostně	slavnostně	k6eAd1	slavnostně
otevřen	otevřít	k5eAaPmNgInS	otevřít
nový	nový	k2eAgInSc1d1	nový
stadion	stadion	k1gInSc1	stadion
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
prvním	první	k4xOgNnSc6	první
soutěžním	soutěžní	k2eAgNnSc6d1	soutěžní
na	na	k7c6	na
něm	on	k3xPp3gNnSc6	on
utkání	utkání	k1gNnSc6	utkání
si	se	k3xPyFc3	se
Slavia	Slavia	k1gFnSc1	Slavia
zajistila	zajistit	k5eAaPmAgFnS	zajistit
po	po	k7c6	po
12	[number]	k4	12
letech	let	k1gInPc6	let
mistrovský	mistrovský	k2eAgInSc4d1	mistrovský
titul	titul	k1gInSc4	titul
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následující	následující	k2eAgFnSc6d1	následující
sezóně	sezóna	k1gFnSc6	sezóna
přesáhla	přesáhnout	k5eAaPmAgFnS	přesáhnout
průměrná	průměrný	k2eAgFnSc1d1	průměrná
návštěvnost	návštěvnost	k1gFnSc1	návštěvnost
na	na	k7c6	na
novém	nový	k2eAgInSc6d1	nový
stadionu	stadion	k1gInSc6	stadion
11	[number]	k4	11
000	[number]	k4	000
diváků	divák	k1gMnPc2	divák
<g/>
,	,	kIx,	,
a	a	k8xC	a
Slavia	Slavia	k1gFnSc1	Slavia
titul	titul	k1gInSc4	titul
obhájila	obhájit	k5eAaPmAgFnS	obhájit
(	(	kIx(	(
<g/>
podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
to	ten	k3xDgNnSc1	ten
poprvé	poprvé	k6eAd1	poprvé
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nepodařilo	podařit	k5eNaPmAgNnS	podařit
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
zopakovat	zopakovat	k5eAaPmF	zopakovat
účast	účast	k1gFnSc4	účast
v	v	k7c6	v
Lize	liga	k1gFnSc6	liga
mistrů	mistr	k1gMnPc2	mistr
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
poháru	pohár	k1gInSc6	pohár
UEFA	UEFA	kA	UEFA
Slavia	Slavia	k1gFnSc1	Slavia
skončila	skončit	k5eAaPmAgFnS	skončit
již	již	k6eAd1	již
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
skupině	skupina	k1gFnSc6	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
let	léto	k1gNnPc2	léto
1990	[number]	k4	1990
<g/>
-	-	kIx~	-
<g/>
2009	[number]	k4	2009
Slavia	Slavia	k1gFnSc1	Slavia
v	v	k7c6	v
evropských	evropský	k2eAgInPc6d1	evropský
pohárech	pohár	k1gInPc6	pohár
vyřadila	vyřadit	k5eAaPmAgFnS	vyřadit
několik	několik	k4yIc4	několik
známých	známý	k2eAgInPc2d1	známý
evropských	evropský	k2eAgInPc2d1	evropský
klubů	klub	k1gInPc2	klub
<g/>
:	:	kIx,	:
AS	as	k9	as
Řím	Řím	k1gInSc1	Řím
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Schalke	Schalke	k1gFnSc1	Schalke
04	[number]	k4	04
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Grasshoppers	Grasshoppers	k1gInSc1	Grasshoppers
Curych	Curych	k1gInSc1	Curych
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Steauu	Steau	k2eAgFnSc4d1	Steaua
Bukurešť	Bukurešť	k1gFnSc4	Bukurešť
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Partizan	Partizan	k1gInSc1	Partizan
Bělehrad	Bělehrad	k1gInSc1	Bělehrad
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
a	a	k8xC	a
Ajax	Ajax	k1gInSc1	Ajax
Amsterdam	Amsterdam	k1gInSc1	Amsterdam
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
však	však	k9	však
přišly	přijít	k5eAaPmAgFnP	přijít
nenadálé	nenadálý	k2eAgFnPc1d1	nenadálá
potíže	potíž	k1gFnPc1	potíž
<g/>
.	.	kIx.	.
</s>
<s>
Trenér	trenér	k1gMnSc1	trenér
Jarolím	Jarole	k1gFnPc3	Jarole
dělal	dělat	k5eAaImAgMnS	dělat
časté	častý	k2eAgFnPc4d1	častá
změny	změna	k1gFnPc4	změna
v	v	k7c6	v
sestavě	sestava	k1gFnSc6	sestava
<g/>
,	,	kIx,	,
média	médium	k1gNnSc2	médium
spekulovala	spekulovat	k5eAaImAgFnS	spekulovat
o	o	k7c6	o
jeho	jeho	k3xOp3gInPc6	jeho
sporech	spor	k1gInPc6	spor
s	s	k7c7	s
hráči	hráč	k1gMnPc7	hráč
<g/>
,	,	kIx,	,
vůči	vůči	k7c3	vůči
kterým	který	k3yIgMnPc3	který
uplatňoval	uplatňovat	k5eAaImAgInS	uplatňovat
příliš	příliš	k6eAd1	příliš
drsný	drsný	k2eAgInSc4d1	drsný
přístup	přístup	k1gInSc4	přístup
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2009	[number]	k4	2009
Jarolím	Jarolí	k1gNnSc7	Jarolí
ze	z	k7c2	z
Slavie	slavie	k1gFnSc2	slavie
překvapivě	překvapivě	k6eAd1	překvapivě
propustil	propustit	k5eAaPmAgInS	propustit
kapitána	kapitán	k1gMnSc4	kapitán
Ericha	Erich	k1gMnSc4	Erich
Brabce	Brabec	k1gMnSc4	Brabec
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
spory	spor	k1gInPc4	spor
s	s	k7c7	s
Jarolímem	Jarolím	k1gInSc7	Jarolím
se	se	k3xPyFc4	se
také	také	k9	také
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
klub	klub	k1gInSc4	klub
opustit	opustit	k5eAaPmF	opustit
Jarolímův	Jarolímův	k2eAgMnSc1d1	Jarolímův
synovec	synovec	k1gMnSc1	synovec
Marek	Marek	k1gMnSc1	Marek
Jarolím	Jarolí	k1gNnSc7	Jarolí
<g/>
.	.	kIx.	.
</s>
<s>
Slavia	Slavia	k1gFnSc1	Slavia
opět	opět	k6eAd1	opět
nepostoupila	postoupit	k5eNaPmAgFnS	postoupit
do	do	k7c2	do
Ligy	liga	k1gFnSc2	liga
mistrů	mistr	k1gMnPc2	mistr
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
Evropské	evropský	k2eAgFnSc6d1	Evropská
lize	liga	k1gFnSc6	liga
skončila	skončit	k5eAaPmAgFnS	skončit
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
skupině	skupina	k1gFnSc6	skupina
poslední	poslední	k2eAgFnSc6d1	poslední
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
na	na	k7c4	na
dalších	další	k2eAgNnPc2d1	další
sedm	sedm	k4xCc4	sedm
let	léto	k1gNnPc2	léto
poslední	poslední	k2eAgFnSc4d1	poslední
účast	účast	k1gFnSc4	účast
Slavie	slavie	k1gFnSc2	slavie
v	v	k7c6	v
evropských	evropský	k2eAgInPc6d1	evropský
pohárech	pohár	k1gInPc6	pohár
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
2009	[number]	k4	2009
<g/>
/	/	kIx~	/
<g/>
10	[number]	k4	10
přezimovala	přezimovat	k5eAaBmAgFnS	přezimovat
Slavia	Slavia	k1gFnSc1	Slavia
na	na	k7c6	na
sedmém	sedmý	k4xOgInSc6	sedmý
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
výkonnostní	výkonnostní	k2eAgNnSc1d1	výkonnostní
zlepšení	zlepšení	k1gNnSc1	zlepšení
nepřišlo	přijít	k5eNaPmAgNnS	přijít
<g/>
.	.	kIx.	.
</s>
<s>
Jarolím	Jarolit	k5eAaImIp1nS	Jarolit
dostal	dostat	k5eAaPmAgMnS	dostat
od	od	k7c2	od
vedení	vedení	k1gNnSc2	vedení
klubu	klub	k1gInSc2	klub
ultimátum	ultimátum	k1gNnSc1	ultimátum
(	(	kIx(	(
<g/>
neprohrát	prohrát	k5eNaPmF	prohrát
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
nesplnil	splnit	k5eNaPmAgInS	splnit
<g/>
,	,	kIx,	,
vedení	vedený	k2eAgMnPc1d1	vedený
Jarolíma	Jarolí	k2eAgFnPc7d1	Jarolí
ani	ani	k8xC	ani
tak	tak	k6eAd1	tak
neodvolalo	odvolat	k5eNaPmAgNnS	odvolat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Jarolím	Jarolí	k1gNnSc7	Jarolí
nakonec	nakonec	k6eAd1	nakonec
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
sám	sám	k3xTgMnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Sezónu	sezóna	k1gFnSc4	sezóna
dokončil	dokončit	k5eAaPmAgMnS	dokončit
František	František	k1gMnSc1	František
Cipro	Cipro	k1gMnSc1	Cipro
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
jeho	jeho	k3xOp3gNnSc1	jeho
již	již	k6eAd1	již
třetí	třetí	k4xOgNnSc1	třetí
trenérské	trenérský	k2eAgNnSc1d1	trenérské
angažmá	angažmá	k1gNnSc1	angažmá
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
klubu	klub	k1gInSc6	klub
<g/>
.	.	kIx.	.
</s>
<s>
Slavii	slavie	k1gFnSc3	slavie
dovedl	dovést	k5eAaPmAgMnS	dovést
k	k	k7c3	k
sedmému	sedmý	k4xOgInSc3	sedmý
místu	místo	k1gNnSc3	místo
v	v	k7c6	v
lize	liga	k1gFnSc6	liga
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
pro	pro	k7c4	pro
Slavii	slavie	k1gFnSc4	slavie
nejhorší	zlý	k2eAgNnSc1d3	nejhorší
umístění	umístění	k1gNnSc1	umístění
od	od	k7c2	od
sezóny	sezóna	k1gFnSc2	sezóna
1990	[number]	k4	1990
<g/>
/	/	kIx~	/
<g/>
91	[number]	k4	91
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
se	se	k3xPyFc4	se
nekvalifikovala	kvalifikovat	k5eNaBmAgFnS	kvalifikovat
do	do	k7c2	do
žádného	žádný	k3yNgInSc2	žádný
evropského	evropský	k2eAgInSc2d1	evropský
poháru	pohár	k1gInSc2	pohár
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
trenérem	trenér	k1gMnSc7	trenér
znovu	znovu	k6eAd1	znovu
Jarolím	Jarolí	k1gNnPc3	Jarolí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
devíti	devět	k4xCc6	devět
ligových	ligový	k2eAgNnPc6d1	ligové
kolech	kolo	k1gNnPc6	kolo
(	(	kIx(	(
<g/>
ve	v	k7c6	v
kterých	který	k3yQgFnPc6	který
Slavia	Slavia	k1gFnSc1	Slavia
získala	získat	k5eAaPmAgFnS	získat
10	[number]	k4	10
bodů	bod	k1gInPc2	bod
<g/>
)	)	kIx)	)
rezignoval	rezignovat	k5eAaBmAgInS	rezignovat
<g/>
.	.	kIx.	.
</s>
<s>
Přišly	přijít	k5eAaPmAgFnP	přijít
obrovské	obrovský	k2eAgInPc4d1	obrovský
ekonomické	ekonomický	k2eAgInPc4d1	ekonomický
problémy	problém	k1gInPc4	problém
<g/>
,	,	kIx,	,
dohady	dohad	k1gInPc4	dohad
kolem	kolem	k7c2	kolem
neznámého	známý	k2eNgMnSc2d1	neznámý
majitele	majitel	k1gMnSc2	majitel
klubu	klub	k1gInSc2	klub
<g/>
,	,	kIx,	,
spory	spor	k1gInPc1	spor
ve	v	k7c6	v
vedení	vedení	k1gNnSc6	vedení
klubu	klub	k1gInSc2	klub
<g/>
,	,	kIx,	,
stěhování	stěhování	k1gNnSc1	stěhování
klubu	klub	k1gInSc2	klub
Bohemians	Bohemians	k1gInSc1	Bohemians
1905	[number]	k4	1905
do	do	k7c2	do
Edenu	Eden	k1gInSc2	Eden
a	a	k8xC	a
bojkot	bojkot	k1gInSc4	bojkot
fanoušků	fanoušek	k1gMnPc2	fanoušek
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
přineslo	přinést	k5eAaPmAgNnS	přinést
výsledkovou	výsledkový	k2eAgFnSc4d1	výsledková
krizi	krize	k1gFnSc4	krize
a	a	k8xC	a
obavy	obava	k1gFnPc4	obava
o	o	k7c4	o
další	další	k2eAgInSc4d1	další
osud	osud	k1gInSc4	osud
klubu	klub	k1gInSc2	klub
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
2010	[number]	k4	2010
<g/>
/	/	kIx~	/
<g/>
2011	[number]	k4	2011
přezimoval	přezimovat	k5eAaBmAgInS	přezimovat
klub	klub	k1gInSc1	klub
na	na	k7c4	na
14	[number]	k4	14
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
ligové	ligový	k2eAgFnSc2d1	ligová
tabulky	tabulka	k1gFnSc2	tabulka
s	s	k7c7	s
náskokem	náskok	k1gInSc7	náskok
1	[number]	k4	1
bodu	bod	k1gInSc2	bod
na	na	k7c4	na
sestupovou	sestupový	k2eAgFnSc4d1	sestupová
příčku	příčka	k1gFnSc4	příčka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2011	[number]	k4	2011
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jejím	její	k3xOp3gMnSc7	její
majitelem	majitel	k1gMnSc7	majitel
a	a	k8xC	a
prezidentem	prezident	k1gMnSc7	prezident
klubu	klub	k1gInSc2	klub
podnikatel	podnikatel	k1gMnSc1	podnikatel
a	a	k8xC	a
bývalý	bývalý	k2eAgMnSc1d1	bývalý
politik	politik	k1gMnSc1	politik
ODS	ODS	kA	ODS
Aleš	Aleš	k1gMnSc1	Aleš
Řebíček	řebíček	k1gInSc1	řebíček
<g/>
.	.	kIx.	.
</s>
<s>
V	V	kA	V
online	onlinout	k5eAaPmIp3nS	onlinout
rozhovoru	rozhovor	k1gInSc6	rozhovor
s	s	k7c7	s
fanoušky	fanoušek	k1gMnPc7	fanoušek
Řebíček	řebíček	k1gInSc4	řebíček
formuloval	formulovat	k5eAaImAgInS	formulovat
hlavní	hlavní	k2eAgFnPc4d1	hlavní
priority	priorita	k1gFnPc4	priorita
Slavie	slavie	k1gFnSc2	slavie
pod	pod	k7c7	pod
svým	svůj	k3xOyFgNnSc7	svůj
vedením	vedení	k1gNnSc7	vedení
<g/>
:	:	kIx,	:
stabilizovat	stabilizovat	k5eAaBmF	stabilizovat
finanční	finanční	k2eAgFnSc4d1	finanční
situaci	situace	k1gFnSc4	situace
klubu	klub	k1gInSc2	klub
<g/>
,	,	kIx,	,
hrát	hrát	k5eAaImF	hrát
pravidelně	pravidelně	k6eAd1	pravidelně
o	o	k7c4	o
evropské	evropský	k2eAgInPc4d1	evropský
poháry	pohár	k1gInPc4	pohár
<g/>
,	,	kIx,	,
porážet	porážet	k5eAaImF	porážet
konkurenční	konkurenční	k2eAgFnSc4d1	konkurenční
Spartu	Sparta	k1gFnSc4	Sparta
a	a	k8xC	a
vychovávat	vychovávat	k5eAaImF	vychovávat
mladé	mladý	k2eAgMnPc4d1	mladý
hráče	hráč	k1gMnPc4	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Fanouškům	Fanoušek	k1gMnPc3	Fanoušek
též	též	k6eAd1	též
slíbil	slíbit	k5eAaPmAgMnS	slíbit
účast	účast	k1gFnSc4	účast
v	v	k7c6	v
Lize	liga	k1gFnSc6	liga
mistrů	mistr	k1gMnPc2	mistr
<g/>
.	.	kIx.	.
</s>
<s>
Řebíček	řebíček	k1gInSc1	řebíček
vystřídal	vystřídat	k5eAaPmAgInS	vystřídat
kouče	kouč	k1gMnSc4	kouč
Michala	Michal	k1gMnSc4	Michal
Petrouše	Petrouš	k1gMnSc4	Petrouš
Františkem	František	k1gMnSc7	František
Strakou	Straka	k1gMnSc7	Straka
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
profiloval	profilovat	k5eAaImAgMnS	profilovat
jako	jako	k9	jako
velký	velký	k2eAgMnSc1d1	velký
srdcař	srdcař	k1gMnSc1	srdcař
rivalské	rivalský	k2eAgFnSc2d1	rivalská
Sparty	Sparta	k1gFnSc2	Sparta
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
mezi	mezi	k7c7	mezi
příznivci	příznivec	k1gMnPc7	příznivec
obou	dva	k4xCgInPc2	dva
klubů	klub	k1gInPc2	klub
značné	značný	k2eAgInPc4d1	značný
protesty	protest	k1gInPc4	protest
<g/>
.	.	kIx.	.
</s>
<s>
Slávisté	slávista	k1gMnPc1	slávista
dorazili	dorazit	k5eAaPmAgMnP	dorazit
na	na	k7c4	na
Strakovu	Strakův	k2eAgFnSc4d1	Strakova
premiéru	premiéra	k1gFnSc4	premiéra
v	v	k7c6	v
černém	černé	k1gNnSc6	černé
a	a	k8xC	a
na	na	k7c6	na
tréninkovém	tréninkový	k2eAgNnSc6d1	tréninkové
hřišti	hřiště	k1gNnSc6	hřiště
vykopali	vykopat	k5eAaPmAgMnP	vykopat
Strakovi	Strakův	k2eAgMnPc1d1	Strakův
i	i	k9	i
členům	člen	k1gMnPc3	člen
slávistického	slávistický	k2eAgNnSc2d1	slávistické
vedení	vedení	k1gNnSc2	vedení
symbolické	symbolický	k2eAgInPc4d1	symbolický
hroby	hrob	k1gInPc4	hrob
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Kněževsi	Kněževes	k1gFnSc6	Kněževes
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Straka	Straka	k1gMnSc1	Straka
žije	žít	k5eAaImIp3nS	žít
<g/>
,	,	kIx,	,
namalovali	namalovat	k5eAaPmAgMnP	namalovat
na	na	k7c6	na
zdi	zeď	k1gFnSc6	zeď
nápisy	nápis	k1gInPc4	nápis
<g/>
,	,	kIx,	,
urážející	urážející	k2eAgFnSc4d1	urážející
Straku	straka	k1gFnSc4	straka
<g/>
.	.	kIx.	.
</s>
<s>
Sparťanští	sparťanský	k2eAgMnPc1d1	sparťanský
fanoušci	fanoušek	k1gMnPc1	fanoušek
pomalovali	pomalovat	k5eAaPmAgMnP	pomalovat
přímo	přímo	k6eAd1	přímo
Strakův	Strakův	k2eAgInSc4d1	Strakův
dům	dům	k1gInSc4	dům
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
nekončícím	končící	k2eNgInPc3d1	nekončící
protestům	protest	k1gInPc3	protest
se	se	k3xPyFc4	se
Straka	Straka	k1gMnSc1	Straka
nakonec	nakonec	k6eAd1	nakonec
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2012	[number]	k4	2012
rezignovat	rezignovat	k5eAaBmF	rezignovat
<g/>
.	.	kIx.	.
</s>
<s>
Sezónu	sezóna	k1gFnSc4	sezóna
dotrénoval	dotrénovat	k5eAaPmAgMnS	dotrénovat
Strakův	Strakův	k2eAgMnSc1d1	Strakův
asistent	asistent	k1gMnSc1	asistent
<g/>
,	,	kIx,	,
šestatřicetiletý	šestatřicetiletý	k2eAgMnSc1d1	šestatřicetiletý
Martin	Martin	k1gMnSc1	Martin
Poustka	poustka	k1gFnSc1	poustka
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
finanční	finanční	k2eAgInSc4d1	finanční
rok	rok	k1gInSc4	rok
končící	končící	k2eAgInSc4d1	končící
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2012	[number]	k4	2012
Slavia	Slavia	k1gFnSc1	Slavia
utržila	utržit	k5eAaPmAgFnS	utržit
47	[number]	k4	47
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
<g/>
,	,	kIx,	,
závazky	závazek	k1gInPc4	závazek
k	k	k7c3	k
majoritnímu	majoritní	k2eAgMnSc3d1	majoritní
akcionáři	akcionář	k1gMnSc3	akcionář
byly	být	k5eAaImAgInP	být
vyšší	vysoký	k2eAgFnSc1d2	vyšší
než	než	k8xS	než
účetní	účetní	k2eAgFnSc1d1	účetní
hodnota	hodnota	k1gFnSc1	hodnota
aktiv	aktivum	k1gNnPc2	aktivum
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
polovinu	polovina	k1gFnSc4	polovina
účetní	účetní	k2eAgFnSc2d1	účetní
hodnoty	hodnota	k1gFnSc2	hodnota
aktiv	aktivum	k1gNnPc2	aktivum
tvořilo	tvořit	k5eAaImAgNnS	tvořit
předplacené	předplacený	k2eAgNnSc1d1	předplacené
nájemné	nájemné	k1gNnSc1	nájemné
na	na	k7c6	na
základě	základ	k1gInSc6	základ
padesátileté	padesátiletý	k2eAgFnSc2d1	padesátiletá
smlouvy	smlouva	k1gFnSc2	smlouva
na	na	k7c4	na
pronájem	pronájem	k1gInSc4	pronájem
stadionu	stadion	k1gInSc2	stadion
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
sezóny	sezóna	k1gFnSc2	sezóna
2012	[number]	k4	2012
<g/>
/	/	kIx~	/
<g/>
13	[number]	k4	13
trénoval	trénovat	k5eAaImAgMnS	trénovat
Slavii	slavie	k1gFnSc4	slavie
Petr	Petr	k1gMnSc1	Petr
Rada	Rada	k1gMnSc1	Rada
<g/>
,	,	kIx,	,
sezónu	sezóna	k1gFnSc4	sezóna
ale	ale	k8xC	ale
nedokončil	dokončit	k5eNaPmAgMnS	dokončit
<g/>
,	,	kIx,	,
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2013	[number]	k4	2013
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
neuspokojivé	uspokojivý	k2eNgInPc4d1	neuspokojivý
výsledky	výsledek	k1gInPc4	výsledek
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
rezignovat	rezignovat	k5eAaBmF	rezignovat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
rezignaci	rezignace	k1gFnSc6	rezignace
byl	být	k5eAaImAgMnS	být
vedením	vedení	k1gNnSc7	vedení
týmu	tým	k1gInSc2	tým
do	do	k7c2	do
konce	konec	k1gInSc2	konec
sezóny	sezóna	k1gFnSc2	sezóna
pověřen	pověřit	k5eAaPmNgMnS	pověřit
podruhé	podruhé	k6eAd1	podruhé
Michal	Michal	k1gMnSc1	Michal
Petrouš	Petrouš	k1gMnSc1	Petrouš
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
vedením	vedení	k1gNnSc7	vedení
uhrála	uhrát	k5eAaPmAgFnS	uhrát
Slavia	Slavia	k1gFnSc1	Slavia
ve	v	k7c6	v
zbývajících	zbývající	k2eAgInPc6d1	zbývající
pěti	pět	k4xCc6	pět
kolech	kolo	k1gNnPc6	kolo
čtyři	čtyři	k4xCgFnPc4	čtyři
výhry	výhra	k1gFnPc4	výhra
při	při	k7c6	při
skóre	skóre	k1gNnSc6	skóre
13	[number]	k4	13
<g/>
:	:	kIx,	:
<g/>
4	[number]	k4	4
a	a	k8xC	a
sezónu	sezóna	k1gFnSc4	sezóna
2012	[number]	k4	2012
<g/>
/	/	kIx~	/
<g/>
13	[number]	k4	13
dokončila	dokončit	k5eAaPmAgFnS	dokončit
na	na	k7c4	na
7	[number]	k4	7
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Petrouš	Petrouš	k5eAaPmIp2nS	Petrouš
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
angažovaný	angažovaný	k2eAgMnSc1d1	angažovaný
jako	jako	k8xC	jako
dočasný	dočasný	k2eAgMnSc1d1	dočasný
trenér	trenér	k1gMnSc1	trenér
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
poté	poté	k6eAd1	poté
potvrzen	potvrdit	k5eAaPmNgMnS	potvrdit
jako	jako	k8xC	jako
hlavní	hlavní	k2eAgMnSc1d1	hlavní
trenér	trenér	k1gMnSc1	trenér
i	i	k9	i
pro	pro	k7c4	pro
nadcházející	nadcházející	k2eAgFnPc4d1	nadcházející
sezóny	sezóna	k1gFnPc4	sezóna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následující	následující	k2eAgFnSc6d1	následující
sezóně	sezóna	k1gFnSc6	sezóna
už	už	k6eAd1	už
se	se	k3xPyFc4	se
týmu	tým	k1gInSc2	tým
tolik	tolik	k6eAd1	tolik
nedařilo	dařit	k5eNaImAgNnS	dařit
-	-	kIx~	-
19	[number]	k4	19
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2013	[number]	k4	2013
dokonce	dokonce	k9	dokonce
prohrála	prohrát	k5eAaPmAgFnS	prohrát
na	na	k7c6	na
domácím	domácí	k2eAgNnSc6d1	domácí
hřišti	hřiště	k1gNnSc6	hřiště
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
7	[number]	k4	7
se	se	k3xPyFc4	se
Teplicemi	Teplice	k1gFnPc7	Teplice
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
pro	pro	k7c4	pro
Slavii	slavie	k1gFnSc4	slavie
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
porážka	porážka	k1gFnSc1	porážka
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
historii	historie	k1gFnSc6	historie
klubu	klub	k1gInSc2	klub
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
měsíc	měsíc	k1gInSc4	měsíc
později	pozdě	k6eAd2	pozdě
přišel	přijít	k5eAaPmAgInS	přijít
další	další	k2eAgInSc1d1	další
domácí	domácí	k2eAgInSc1d1	domácí
debakl	debakl	k1gInSc1	debakl
<g/>
,	,	kIx,	,
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
4	[number]	k4	4
s	s	k7c7	s
Mladou	mladá	k1gFnSc7	mladá
Boleslaví	Boleslavý	k2eAgMnPc1d1	Boleslavý
<g/>
,	,	kIx,	,
a	a	k8xC	a
trenér	trenér	k1gMnSc1	trenér
Petrouš	Petrouš	k1gMnSc1	Petrouš
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
osmi	osm	k4xCc6	osm
odehraných	odehraný	k2eAgNnPc6d1	odehrané
kolech	kolo	k1gNnPc6	kolo
byla	být	k5eAaImAgFnS	být
Slavia	Slavia	k1gFnSc1	Slavia
13	[number]	k4	13
<g/>
.	.	kIx.	.
se	s	k7c7	s
sedmi	sedm	k4xCc7	sedm
získanými	získaný	k2eAgInPc7d1	získaný
body	bod	k1gInPc7	bod
a	a	k8xC	a
skóre	skóre	k1gNnSc7	skóre
7	[number]	k4	7
<g/>
:	:	kIx,	:
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Petroušovi	Petrouš	k1gMnSc6	Petrouš
se	s	k7c7	s
trenérem	trenér	k1gMnSc7	trenér
stal	stát	k5eAaPmAgMnS	stát
Miroslav	Miroslav	k1gMnSc1	Miroslav
Koubek	Koubek	k1gMnSc1	Koubek
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zároveň	zároveň	k6eAd1	zároveň
trénoval	trénovat	k5eAaImAgMnS	trénovat
českou	český	k2eAgFnSc4d1	Česká
reprezentaci	reprezentace	k1gFnSc4	reprezentace
do	do	k7c2	do
19	[number]	k4	19
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Musel	muset	k5eAaImAgMnS	muset
tedy	tedy	k9	tedy
dělit	dělit	k5eAaImF	dělit
svůj	svůj	k3xOyFgInSc4	svůj
čas	čas	k1gInSc4	čas
a	a	k8xC	a
pozornost	pozornost	k1gFnSc4	pozornost
mezi	mezi	k7c4	mezi
tato	tento	k3xDgNnPc4	tento
dvě	dva	k4xCgNnPc4	dva
angažmá	angažmá	k1gNnPc4	angažmá
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2013	[number]	k4	2013
připadly	připadnout	k5eAaPmAgInP	připadnout
zápasy	zápas	k1gInPc1	zápas
Slavie	slavie	k1gFnSc2	slavie
a	a	k8xC	a
reprezentace	reprezentace	k1gFnSc2	reprezentace
na	na	k7c4	na
jediný	jediný	k2eAgInSc4d1	jediný
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
kouč	kouč	k1gMnSc1	kouč
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
zápasy	zápas	k1gInPc7	zápas
přesunul	přesunout	k5eAaPmAgMnS	přesunout
letadlem	letadlo	k1gNnSc7	letadlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2014	[number]	k4	2014
od	od	k7c2	od
reprezentace	reprezentace	k1gFnSc2	reprezentace
odešel	odejít	k5eAaPmAgMnS	odejít
a	a	k8xC	a
věnoval	věnovat	k5eAaPmAgMnS	věnovat
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
Slavii	slavie	k1gFnSc3	slavie
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
se	se	k3xPyFc4	se
v	v	k7c6	v
médiích	médium	k1gNnPc6	médium
objevovaly	objevovat	k5eAaImAgFnP	objevovat
informace	informace	k1gFnPc1	informace
<g/>
,	,	kIx,	,
že	že	k8xS	že
Řebíčka	Řebíček	k1gInSc2	Řebíček
financování	financování	k1gNnSc2	financování
klubu	klub	k1gInSc2	klub
finančně	finančně	k6eAd1	finančně
vyčerpává	vyčerpávat	k5eAaImIp3nS	vyčerpávat
a	a	k8xC	a
shání	shánět	k5eAaImIp3nS	shánět
pro	pro	k7c4	pro
Slavii	slavie	k1gFnSc4	slavie
kupce	kupec	k1gMnSc2	kupec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zimní	zimní	k2eAgFnSc6d1	zimní
přestávce	přestávka	k1gFnSc6	přestávka
sezony	sezona	k1gFnSc2	sezona
2013	[number]	k4	2013
<g/>
/	/	kIx~	/
<g/>
14	[number]	k4	14
projevil	projevit	k5eAaPmAgInS	projevit
zájem	zájem	k1gInSc1	zájem
Dmitrij	Dmitrij	k1gFnSc2	Dmitrij
Romaněnko	Romaněnka	k1gFnSc5	Romaněnka
z	z	k7c2	z
koncernu	koncern	k1gInSc2	koncern
Lukoil	Lukoil	k1gInSc1	Lukoil
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
do	do	k7c2	do
managementu	management	k1gInSc2	management
klubu	klub	k1gInSc2	klub
dosadil	dosadit	k5eAaPmAgMnS	dosadit
bývalého	bývalý	k2eAgMnSc4d1	bývalý
sportovního	sportovní	k2eAgMnSc4d1	sportovní
ředitele	ředitel	k1gMnSc4	ředitel
Zenitu	zenit	k1gInSc2	zenit
Petohrad	Petohrad	k1gInSc1	Petohrad
Igora	Igor	k1gMnSc2	Igor
Kornějeva	Kornějev	k1gMnSc2	Kornějev
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
do	do	k7c2	do
dokončení	dokončení	k1gNnSc2	dokončení
prodeje	prodej	k1gFnSc2	prodej
zastupoval	zastupovat	k5eAaImAgInS	zastupovat
nového	nový	k2eAgMnSc4d1	nový
investora	investor	k1gMnSc4	investor
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
prosadil	prosadit	k5eAaPmAgMnS	prosadit
překvapivé	překvapivý	k2eAgNnSc4d1	překvapivé
odvolání	odvolání	k1gNnSc4	odvolání
kouče	kouč	k1gMnSc2	kouč
Koubka	Koubek	k1gMnSc2	Koubek
již	již	k6eAd1	již
po	po	k7c6	po
dvou	dva	k4xCgNnPc6	dva
odehraných	odehraný	k2eAgNnPc6d1	odehrané
jarních	jarní	k2eAgNnPc6d1	jarní
kolech	kolo	k1gNnPc6	kolo
<g/>
,	,	kIx,	,
a	a	k8xC	a
jmenování	jmenování	k1gNnSc1	jmenování
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
málo	málo	k6eAd1	málo
známého	známý	k2eAgMnSc2d1	známý
Nizozemce	Nizozemec	k1gMnSc2	Nizozemec
Alexe	Alex	k1gMnSc5	Alex
Pastoora	Pastoor	k1gMnSc4	Pastoor
<g/>
.	.	kIx.	.
</s>
<s>
Sportovní	sportovní	k2eAgMnSc1d1	sportovní
manažer	manažer	k1gMnSc1	manažer
Jaromír	Jaromír	k1gMnSc1	Jaromír
Šeterle	Šeterle	k1gNnSc4	Šeterle
na	na	k7c4	na
protest	protest	k1gInSc4	protest
proti	proti	k7c3	proti
vyhazovu	vyhazov	k1gInSc3	vyhazov
Koubka	Koubek	k1gMnSc2	Koubek
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
na	na	k7c4	na
svoji	svůj	k3xOyFgFnSc4	svůj
funkci	funkce	k1gFnSc4	funkce
ve	v	k7c6	v
Slavii	slavie	k1gFnSc6	slavie
<g/>
.	.	kIx.	.
</s>
<s>
Pastoorovým	Pastoorův	k2eAgMnSc7d1	Pastoorův
asistentem	asistent	k1gMnSc7	asistent
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Vladimír	Vladimír	k1gMnSc1	Vladimír
Šmicer	Šmicer	k1gMnSc1	Šmicer
<g/>
.	.	kIx.	.
</s>
<s>
Sezona	sezona	k1gFnSc1	sezona
2013	[number]	k4	2013
<g/>
/	/	kIx~	/
<g/>
14	[number]	k4	14
byla	být	k5eAaImAgFnS	být
bídná	bídný	k2eAgFnSc1d1	bídná
herně	herna	k1gFnSc3	herna
i	i	k8xC	i
výsledkově	výsledkově	k6eAd1	výsledkově
<g/>
.	.	kIx.	.
</s>
<s>
Slavia	Slavia	k1gFnSc1	Slavia
v	v	k7c6	v
nejvyšší	vysoký	k2eAgFnSc6d3	nejvyšší
české	český	k2eAgFnSc6d1	Česká
lize	liga	k1gFnSc6	liga
obsadila	obsadit	k5eAaPmAgFnS	obsadit
13	[number]	k4	13
<g/>
.	.	kIx.	.
příčku	příčka	k1gFnSc4	příčka
a	a	k8xC	a
zachránila	zachránit	k5eAaPmAgFnS	zachránit
se	se	k3xPyFc4	se
v	v	k7c6	v
posledním	poslední	k2eAgNnSc6d1	poslední
kole	kolo	k1gNnSc6	kolo
jen	jen	k9	jen
díky	díky	k7c3	díky
bodovým	bodový	k2eAgFnPc3d1	bodová
ztrátám	ztráta	k1gFnPc3	ztráta
dalších	další	k2eAgMnPc2d1	další
adeptů	adept	k1gMnPc2	adept
na	na	k7c4	na
sestup	sestup	k1gInSc4	sestup
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
domácím	domácí	k2eAgInSc6d1	domácí
poháru	pohár	k1gInSc6	pohár
skončila	skončit	k5eAaPmAgFnS	skončit
Slavia	Slavia	k1gFnSc1	Slavia
ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
<g/>
.	.	kIx.	.
</s>
<s>
Trenér	trenér	k1gMnSc1	trenér
Alex	Alex	k1gMnSc1	Alex
Pastoor	Pastoor	k1gMnSc1	Pastoor
se	se	k3xPyFc4	se
již	již	k6eAd1	již
před	před	k7c7	před
koncem	konec	k1gInSc7	konec
sezóny	sezóna	k1gFnSc2	sezóna
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
v	v	k7c6	v
klubu	klub	k1gInSc6	klub
nepokračovat	pokračovat	k5eNaImF	pokračovat
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
skončil	skončit	k5eAaPmAgMnS	skončit
i	i	k9	i
Šmicer	Šmicer	k1gMnSc1	Šmicer
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
také	také	k9	také
jednání	jednání	k1gNnSc1	jednání
s	s	k7c7	s
ruským	ruský	k2eAgMnSc7d1	ruský
investorem	investor	k1gMnSc7	investor
nebyla	být	k5eNaImAgFnS	být
úspěšná	úspěšný	k2eAgNnPc1d1	úspěšné
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
odchodu	odchod	k1gInSc3	odchod
Kornějeva	Kornějeva	k1gFnSc1	Kornějeva
ze	z	k7c2	z
struktur	struktura	k1gFnPc2	struktura
klubu	klub	k1gInSc2	klub
<g/>
.	.	kIx.	.
</s>
<s>
Novým	nový	k2eAgMnSc7d1	nový
koučem	kouč	k1gMnSc7	kouč
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
již	již	k9	již
potřetí	potřetí	k4xO	potřetí
Miroslav	Miroslav	k1gMnSc1	Miroslav
Beránek	Beránek	k1gMnSc1	Beránek
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
plán	plán	k1gInSc1	plán
do	do	k7c2	do
tří	tři	k4xCgNnPc2	tři
let	léto	k1gNnPc2	léto
vrátit	vrátit	k5eAaPmF	vrátit
Slavii	slavie	k1gFnSc4	slavie
do	do	k7c2	do
evropských	evropský	k2eAgInPc2d1	evropský
pohárů	pohár	k1gInPc2	pohár
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvních	první	k4xOgInPc6	první
pěti	pět	k4xCc6	pět
kolech	kolo	k1gNnPc6	kolo
Slavia	Slavium	k1gNnSc2	Slavium
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
a	a	k8xC	a
vedla	vést	k5eAaImAgFnS	vést
ligu	liga	k1gFnSc4	liga
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
však	však	k9	však
přišlo	přijít	k5eAaPmAgNnS	přijít
období	období	k1gNnSc1	období
útlumu	útlum	k1gInSc2	útlum
a	a	k8xC	a
sezónu	sezóna	k1gFnSc4	sezóna
2014	[number]	k4	2014
<g/>
/	/	kIx~	/
<g/>
15	[number]	k4	15
skočila	skočit	k5eAaPmAgFnS	skočit
se	s	k7c7	s
ziskem	zisk	k1gInSc7	zisk
34	[number]	k4	34
bodů	bod	k1gInPc2	bod
na	na	k7c4	na
11	[number]	k4	11
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Trenér	trenér	k1gMnSc1	trenér
Beránek	Beránek	k1gMnSc1	Beránek
byl	být	k5eAaImAgMnS	být
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
sezóny	sezóna	k1gFnSc2	sezóna
odvolán	odvolat	k5eAaPmNgMnS	odvolat
<g/>
,	,	kIx,	,
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
místo	místo	k1gNnSc4	místo
přišel	přijít	k5eAaPmAgMnS	přijít
Dušan	Dušan	k1gMnSc1	Dušan
Uhrin	Uhrin	k1gMnSc1	Uhrin
mladší	mladý	k2eAgMnSc1d2	mladší
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
již	již	k6eAd1	již
v	v	k7c6	v
klubu	klub	k1gInSc6	klub
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1999	[number]	k4	1999
až	až	k9	až
2001	[number]	k4	2001
jako	jako	k8xS	jako
asistent	asistent	k1gMnSc1	asistent
trenérů	trenér	k1gMnPc2	trenér
Cipra	Cipro	k1gMnSc2	Cipro
a	a	k8xC	a
Jarolíma	Jarolí	k2eAgFnPc7d1	Jarolí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
2015	[number]	k4	2015
se	s	k7c7	s
prohloubily	prohloubit	k5eAaPmAgInP	prohloubit
finanční	finanční	k2eAgInPc4d1	finanční
problémy	problém	k1gInPc4	problém
Slavie	slavie	k1gFnSc2	slavie
<g/>
.	.	kIx.	.
</s>
<s>
Bývalý	bývalý	k2eAgMnSc1d1	bývalý
ředitel	ředitel	k1gMnSc1	ředitel
klubu	klub	k1gInSc2	klub
Zbyněk	Zbyněk	k1gMnSc1	Zbyněk
Kusý	kusý	k2eAgMnSc1d1	kusý
vymáhal	vymáhat	k5eAaImAgInS	vymáhat
po	po	k7c6	po
Slavii	slavie	k1gFnSc6	slavie
údajnou	údajný	k2eAgFnSc4d1	údajná
pohledávku	pohledávka	k1gFnSc4	pohledávka
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
19	[number]	k4	19
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2015	[number]	k4	2015
podal	podat	k5eAaPmAgMnS	podat
na	na	k7c4	na
klub	klub	k1gInSc4	klub
návrh	návrh	k1gInSc4	návrh
na	na	k7c4	na
insolvenční	insolvenční	k2eAgNnSc4d1	insolvenční
řízení	řízení	k1gNnSc4	řízení
pro	pro	k7c4	pro
neschopnost	neschopnost	k1gFnSc4	neschopnost
splácet	splácet	k5eAaImF	splácet
závazky	závazek	k1gInPc4	závazek
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
návrhu	návrh	k1gInSc3	návrh
se	se	k3xPyFc4	se
připojily	připojit	k5eAaPmAgFnP	připojit
i	i	k9	i
další	další	k2eAgInPc4d1	další
subjekty	subjekt	k1gInPc4	subjekt
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
společnost	společnost	k1gFnSc1	společnost
Gamma	Gamma	k1gFnSc1	Gamma
Enterprise	Enterprise	k1gFnSc1	Enterprise
<g/>
,	,	kIx,	,
zajišťující	zajišťující	k2eAgFnSc1d1	zajišťující
pro	pro	k7c4	pro
Slavii	slavie	k1gFnSc4	slavie
catering	catering	k1gInSc1	catering
<g/>
.	.	kIx.	.
</s>
<s>
Slavia	Slavia	k1gFnSc1	Slavia
údajně	údajně	k6eAd1	údajně
dlužila	dlužit	k5eAaImAgFnS	dlužit
i	i	k8xC	i
některým	některý	k3yIgMnPc3	některý
bývalým	bývalý	k2eAgMnPc3d1	bývalý
hráčům	hráč	k1gMnPc3	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Generální	generální	k2eAgMnSc1d1	generální
ředitel	ředitel	k1gMnSc1	ředitel
Slavie	slavie	k1gFnSc2	slavie
David	David	k1gMnSc1	David
Trunda	Trund	k1gMnSc4	Trund
zpochybnil	zpochybnit	k5eAaPmAgMnS	zpochybnit
Kusého	kusý	k2eAgInSc2d1	kusý
nároky	nárok	k1gInPc4	nárok
a	a	k8xC	a
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
Slavia	Slavia	k1gFnSc1	Slavia
nebyla	být	k5eNaImAgFnS	být
schopna	schopen	k2eAgFnSc1d1	schopna
platit	platit	k5eAaImF	platit
své	svůj	k3xOyFgInPc4	svůj
dluhy	dluh	k1gInPc4	dluh
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Trundy	Trunda	k1gFnSc2	Trunda
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
"	"	kIx"	"
<g/>
organizovaný	organizovaný	k2eAgInSc4d1	organizovaný
a	a	k8xC	a
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
připravovaný	připravovaný	k2eAgInSc4d1	připravovaný
útok	útok	k1gInSc4	útok
[	[	kIx(	[
<g/>
...	...	k?	...
<g/>
]	]	kIx)	]
promyšlený	promyšlený	k2eAgInSc4d1	promyšlený
postup	postup	k1gInSc4	postup
několika	několik	k4yIc2	několik
konkrétních	konkrétní	k2eAgFnPc2d1	konkrétní
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
ovládnout	ovládnout	k5eAaPmF	ovládnout
a	a	k8xC	a
zničit	zničit	k5eAaPmF	zničit
Slavii	slavie	k1gFnSc4	slavie
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Cíle	cíl	k1gInPc1	cíl
<g/>
,	,	kIx,	,
vytyčené	vytyčený	k2eAgNnSc1d1	vytyčené
Alešem	Aleš	k1gMnSc7	Aleš
Řebíčkem	řebíček	k1gInSc7	řebíček
při	při	k7c6	při
koupi	koupě	k1gFnSc6	koupě
klubu	klub	k1gInSc2	klub
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nepodařilo	podařit	k5eNaPmAgNnS	podařit
naplnit	naplnit	k5eAaPmF	naplnit
-	-	kIx~	-
v	v	k7c6	v
lize	liga	k1gFnSc6	liga
skončila	skončit	k5eAaPmAgFnS	skončit
Slavia	Slavia	k1gFnSc1	Slavia
na	na	k7c4	na
12	[number]	k4	12
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
13	[number]	k4	13
<g/>
.	.	kIx.	.
a	a	k8xC	a
11	[number]	k4	11
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
v	v	k7c6	v
Poháru	pohár	k1gInSc6	pohár
FAČR	FAČR	kA	FAČR
došla	dojít	k5eAaPmAgFnS	dojít
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
2013	[number]	k4	2013
<g/>
/	/	kIx~	/
<g/>
14	[number]	k4	14
do	do	k7c2	do
čtvrtfinále	čtvrtfinále	k1gNnSc2	čtvrtfinále
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
vždy	vždy	k6eAd1	vždy
vypadla	vypadnout	k5eAaPmAgFnS	vypadnout
ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
kole	kolo	k1gNnSc6	kolo
s	s	k7c7	s
týmem	tým	k1gInSc7	tým
z	z	k7c2	z
nižších	nízký	k2eAgFnPc2d2	nižší
soutěží	soutěž	k1gFnPc2	soutěž
<g/>
,	,	kIx,	,
do	do	k7c2	do
evropských	evropský	k2eAgInPc2d1	evropský
pohárů	pohár	k1gInPc2	pohár
se	se	k3xPyFc4	se
jí	jíst	k5eAaImIp3nS	jíst
tedy	tedy	k9	tedy
proniknout	proniknout	k5eAaPmF	proniknout
nepodařilo	podařit	k5eNaPmAgNnS	podařit
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
osmi	osm	k4xCc2	osm
zápasů	zápas	k1gInPc2	zápas
se	s	k7c7	s
Spartou	Sparta	k1gFnSc7	Sparta
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
podařilo	podařit	k5eAaPmAgNnS	podařit
vyhrát	vyhrát	k5eAaPmF	vyhrát
jediný	jediný	k2eAgMnSc1d1	jediný
<g/>
,	,	kIx,	,
a	a	k8xC	a
místo	místo	k7c2	místo
finanční	finanční	k2eAgFnSc2d1	finanční
stabilizace	stabilizace	k1gFnSc2	stabilizace
spadl	spadnout	k5eAaPmAgInS	spadnout
klub	klub	k1gInSc1	klub
do	do	k7c2	do
insolvence	insolvence	k1gFnSc2	insolvence
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2015	[number]	k4	2015
se	se	k3xPyFc4	se
majiteli	majitel	k1gMnSc3	majitel
staly	stát	k5eAaPmAgFnP	stát
klubu	klub	k1gInSc2	klub
čínská	čínský	k2eAgFnSc1d1	čínská
společnost	společnost	k1gFnSc1	společnost
China	China	k1gFnSc1	China
Energy	Energ	k1gInPc4	Energ
Company	Compana	k1gFnSc2	Compana
Limited	limited	k2eAgFnSc2d1	limited
(	(	kIx(	(
<g/>
CEFC	CEFC	kA	CEFC
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
Fly	Fly	k1gMnPc2	Fly
Sport	sport	k1gInSc1	sport
Investments	Investmentsa	k1gFnPc2	Investmentsa
podnikatele	podnikatel	k1gMnSc4	podnikatel
Jiřího	Jiří	k1gMnSc4	Jiří
Šimáně	Šimána	k1gFnSc6	Šimána
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
svých	svůj	k3xOyFgFnPc2	svůj
společností	společnost	k1gFnPc2	společnost
Slavii	slavie	k1gFnSc4	slavie
léta	léto	k1gNnSc2	léto
sponzoroval	sponzorovat	k5eAaImAgMnS	sponzorovat
<g/>
.	.	kIx.	.
</s>
<s>
Šimáně	Šimána	k1gFnSc3	Šimána
podle	podle	k7c2	podle
svých	svůj	k3xOyFgNnPc2	svůj
slov	slovo	k1gNnPc2	slovo
nikdy	nikdy	k6eAd1	nikdy
nechtěl	chtít	k5eNaImAgInS	chtít
Slavii	slavie	k1gFnSc4	slavie
vlastnit	vlastnit	k5eAaImF	vlastnit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
když	když	k8xS	když
byla	být	k5eAaImAgFnS	být
bezprostředně	bezprostředně	k6eAd1	bezprostředně
ohrožena	ohrozit	k5eAaPmNgFnS	ohrozit
zánikem	zánik	k1gInSc7	zánik
<g/>
,	,	kIx,	,
svůj	svůj	k3xOyFgInSc4	svůj
názor	názor	k1gInSc4	názor
změnil	změnit	k5eAaPmAgInS	změnit
<g/>
,	,	kIx,	,
menší	malý	k2eAgInSc4d2	menší
podíl	podíl	k1gInSc4	podíl
koupil	koupit	k5eAaPmAgMnS	koupit
a	a	k8xC	a
sehnal	sehnat	k5eAaPmAgMnS	sehnat
strategického	strategický	k2eAgMnSc4d1	strategický
partnera	partner	k1gMnSc4	partner
z	z	k7c2	z
Číny	Čína	k1gFnSc2	Čína
<g/>
.	.	kIx.	.
</s>
<s>
CEFC	CEFC	kA	CEFC
získala	získat	k5eAaPmAgFnS	získat
za	za	k7c4	za
27	[number]	k4	27
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
59,97	[number]	k4	59,97
%	%	kIx~	%
akcií	akcie	k1gFnPc2	akcie
klubu	klub	k1gInSc2	klub
(	(	kIx(	(
<g/>
také	také	k9	také
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
jeho	jeho	k3xOp3gMnSc7	jeho
generálním	generální	k2eAgMnSc7d1	generální
partnerem	partner	k1gMnSc7	partner
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
společnost	společnost	k1gFnSc1	společnost
Jiřího	Jiří	k1gMnSc2	Jiří
Šimáně	Šimána	k1gFnSc3	Šimána
měla	mít	k5eAaImAgFnS	mít
podíl	podíl	k1gInSc4	podíl
39,97	[number]	k4	39,97
%	%	kIx~	%
<g/>
,	,	kIx,	,
zbývající	zbývající	k2eAgInSc1d1	zbývající
podíl	podíl	k1gInSc1	podíl
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
vlastnictví	vlastnictví	k1gNnSc6	vlastnictví
drobných	drobný	k2eAgMnPc2d1	drobný
akcionářů	akcionář	k1gMnPc2	akcionář
<g/>
.	.	kIx.	.
</s>
<s>
Novým	nový	k2eAgMnSc7d1	nový
předsedou	předseda	k1gMnSc7	předseda
představenstva	představenstvo	k1gNnSc2	představenstvo
byl	být	k5eAaImAgMnS	být
jmenován	jmenován	k2eAgMnSc1d1	jmenován
Šimáně	Šimána	k1gFnSc3	Šimána
<g/>
,	,	kIx,	,
v	v	k7c6	v
dozorčí	dozorčí	k2eAgFnSc6d1	dozorčí
radě	rada	k1gFnSc6	rada
zasedli	zasednout	k5eAaPmAgMnP	zasednout
manažer	manažer	k1gMnSc1	manažer
CEFC	CEFC	kA	CEFC
Čchan	Čchan	k1gMnSc1	Čchan
Čchao-tuo	Čchaouo	k1gMnSc1	Čchao-tuo
<g/>
,	,	kIx,	,
manažer	manažer	k1gMnSc1	manažer
a	a	k8xC	a
bývalý	bývalý	k2eAgMnSc1d1	bývalý
politik	politik	k1gMnSc1	politik
ČSSD	ČSSD	kA	ČSSD
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Tvrdík	Tvrdík	k1gMnSc1	Tvrdík
<g/>
,	,	kIx,	,
a	a	k8xC	a
Vladimír	Vladimír	k1gMnSc1	Vladimír
Šmicer	Šmicer	k1gMnSc1	Šmicer
<g/>
,	,	kIx,	,
generálním	generální	k2eAgMnSc7d1	generální
ředitelem	ředitel	k1gMnSc7	ředitel
zůstal	zůstat	k5eAaPmAgMnS	zůstat
David	David	k1gMnSc1	David
Trunda	Trund	k1gMnSc2	Trund
<g/>
.	.	kIx.	.
</s>
<s>
Noví	nový	k2eAgMnPc1d1	nový
majitelé	majitel	k1gMnPc1	majitel
splatili	splatit	k5eAaPmAgMnP	splatit
všechny	všechen	k3xTgFnPc4	všechen
nesporné	sporný	k2eNgFnPc4d1	nesporná
pohledávky	pohledávka	k1gFnPc4	pohledávka
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
ukončili	ukončit	k5eAaPmAgMnP	ukončit
insolvenční	insolvenční	k2eAgNnSc4d1	insolvenční
řízení	řízení	k1gNnSc4	řízení
s	s	k7c7	s
klubem	klub	k1gInSc7	klub
<g/>
,	,	kIx,	,
a	a	k8xC	a
deklarovali	deklarovat	k5eAaBmAgMnP	deklarovat
záměr	záměr	k1gInSc4	záměr
vrátit	vrátit	k5eAaPmF	vrátit
Slavii	slavie	k1gFnSc4	slavie
do	do	k7c2	do
boje	boj	k1gInSc2	boj
o	o	k7c4	o
nejvyšší	vysoký	k2eAgFnPc4d3	nejvyšší
příčky	příčka	k1gFnPc4	příčka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zimní	zimní	k2eAgFnSc6d1	zimní
přestávce	přestávka	k1gFnSc6	přestávka
2015	[number]	k4	2015
<g/>
/	/	kIx~	/
<g/>
2016	[number]	k4	2016
přivedla	přivést	k5eAaPmAgFnS	přivést
Slavia	Slavia	k1gFnSc1	Slavia
osm	osm	k4xCc1	osm
nových	nový	k2eAgFnPc2d1	nová
posil	posila	k1gFnPc2	posila
<g/>
,	,	kIx,	,
za	za	k7c4	za
které	který	k3yQgFnPc4	který
utratila	utratit	k5eAaPmAgFnS	utratit
značné	značný	k2eAgFnPc4d1	značná
finanční	finanční	k2eAgFnPc4d1	finanční
částky	částka	k1gFnPc4	částka
<g/>
.	.	kIx.	.
</s>
<s>
Média	médium	k1gNnPc1	médium
přinesla	přinést	k5eAaPmAgFnS	přinést
zprávy	zpráva	k1gFnPc4	zpráva
o	o	k7c6	o
sporech	spor	k1gInPc6	spor
Tvrdíka	Tvrdík	k1gMnSc2	Tvrdík
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
v	v	k7c6	v
úmyslu	úmysl	k1gInSc6	úmysl
investovat	investovat	k5eAaBmF	investovat
ve	v	k7c6	v
Slavii	slavie	k1gFnSc6	slavie
velké	velký	k2eAgInPc4d1	velký
prostředky	prostředek	k1gInPc4	prostředek
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
dosažení	dosažení	k1gNnSc2	dosažení
okamžitého	okamžitý	k2eAgInSc2d1	okamžitý
úspěchu	úspěch	k1gInSc2	úspěch
<g/>
,	,	kIx,	,
s	s	k7c7	s
opatrnějším	opatrný	k2eAgMnSc7d2	opatrnější
Šimáněm	Šimáň	k1gMnSc7	Šimáň
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
preferoval	preferovat	k5eAaImAgInS	preferovat
cestu	cesta	k1gFnSc4	cesta
pozvolného	pozvolný	k2eAgNnSc2d1	pozvolné
zlepšování	zlepšování	k1gNnSc2	zlepšování
s	s	k7c7	s
menšími	malý	k2eAgInPc7d2	menší
náklady	náklad	k1gInPc7	náklad
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
rozpory	rozpora	k1gFnPc1	rozpora
vyvrcholily	vyvrcholit	k5eAaPmAgFnP	vyvrcholit
6	[number]	k4	6
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Šimáně	Šimána	k1gFnSc3	Šimána
rezignoval	rezignovat	k5eAaBmAgInS	rezignovat
na	na	k7c4	na
post	post	k1gInSc4	post
předsedy	předseda	k1gMnSc2	předseda
představenstva	představenstvo	k1gNnSc2	představenstvo
klubu	klub	k1gInSc2	klub
(	(	kIx(	(
<g/>
svůj	svůj	k3xOyFgInSc4	svůj
majetkový	majetkový	k2eAgInSc4d1	majetkový
podíl	podíl	k1gInSc4	podíl
si	se	k3xPyFc3	se
ponechal	ponechat	k5eAaPmAgMnS	ponechat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
důvody	důvod	k1gInPc4	důvod
rezignace	rezignace	k1gFnSc2	rezignace
uvedl	uvést	k5eAaPmAgInS	uvést
spory	spor	k1gInPc4	spor
s	s	k7c7	s
Tvrdíkem	Tvrdík	k1gMnSc7	Tvrdík
a	a	k8xC	a
odlišný	odlišný	k2eAgInSc1d1	odlišný
pohled	pohled	k1gInSc1	pohled
na	na	k7c4	na
řízení	řízení	k1gNnSc4	řízení
klubu	klub	k1gInSc2	klub
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
Tvrdík	Tvrdík	k1gMnSc1	Tvrdík
se	s	k7c7	s
Šimáněm	Šimáň	k1gMnSc7	Šimáň
usmířili	usmířit	k5eAaPmAgMnP	usmířit
<g/>
;	;	kIx,	;
Šimáně	Šimán	k1gInSc6	Šimán
byl	být	k5eAaImAgMnS	být
pověřen	pověřit	k5eAaPmNgMnS	pověřit
vybudováním	vybudování	k1gNnSc7	vybudování
mládežnické	mládežnický	k2eAgFnSc2d1	mládežnická
akademie	akademie	k1gFnSc2	akademie
klubu	klub	k1gInSc2	klub
a	a	k8xC	a
vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
do	do	k7c2	do
představenstva	představenstvo	k1gNnSc2	představenstvo
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc2	jehož
se	se	k3xPyFc4	se
Tvrdík	Tvrdík	k1gMnSc1	Tvrdík
stal	stát	k5eAaPmAgMnS	stát
předsedou	předseda	k1gMnSc7	předseda
<g/>
.	.	kIx.	.
</s>
<s>
Majetkový	majetkový	k2eAgInSc1d1	majetkový
podíl	podíl	k1gInSc1	podíl
CEFC	CEFC	kA	CEFC
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
z	z	k7c2	z
60	[number]	k4	60
na	na	k7c4	na
67	[number]	k4	67
%	%	kIx~	%
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Šimáněho	Šimáně	k1gMnSc4	Šimáně
klesl	klesnout	k5eAaPmAgMnS	klesnout
ze	z	k7c2	z
40	[number]	k4	40
na	na	k7c4	na
33	[number]	k4	33
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2016	[number]	k4	2016
navýšila	navýšit	k5eAaPmAgFnS	navýšit
CEFC	CEFC	kA	CEFC
svůj	svůj	k3xOyFgInSc4	svůj
podíl	podíl	k1gInSc4	podíl
na	na	k7c4	na
81	[number]	k4	81
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
2015	[number]	k4	2015
<g/>
/	/	kIx~	/
<g/>
16	[number]	k4	16
skočila	skočit	k5eAaPmAgFnS	skočit
Slavia	Slavia	k1gFnSc1	Slavia
v	v	k7c6	v
lize	liga	k1gFnSc6	liga
pátá	pátá	k1gFnSc1	pátá
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
po	po	k7c6	po
sedmi	sedm	k4xCc6	sedm
letech	léto	k1gNnPc6	léto
kvalifikovala	kvalifikovat	k5eAaBmAgFnS	kvalifikovat
do	do	k7c2	do
evropských	evropský	k2eAgInPc2d1	evropský
pohárů	pohár	k1gInPc2	pohár
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
do	do	k7c2	do
2	[number]	k4	2
<g/>
.	.	kIx.	.
předkola	předkolo	k1gNnSc2	předkolo
Evropské	evropský	k2eAgFnSc2d1	Evropská
ligy	liga	k1gFnSc2	liga
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
předkolech	předkolo	k1gNnPc6	předkolo
přešla	přejít	k5eAaPmAgFnS	přejít
přes	přes	k7c4	přes
estonskou	estonský	k2eAgFnSc4d1	Estonská
Levadii	Levadie	k1gFnSc4	Levadie
Tallinn	Tallinna	k1gFnPc2	Tallinna
a	a	k8xC	a
portugalské	portugalský	k2eAgFnSc2d1	portugalská
Rio	Rio	k1gFnSc2	Rio
Ave	ave	k1gNnSc2	ave
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
ji	on	k3xPp3gFnSc4	on
vyřadil	vyřadit	k5eAaPmAgMnS	vyřadit
RSC	RSC	kA	RSC
Anderlecht	Anderlecht	k1gMnSc1	Anderlecht
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtyřech	čtyři	k4xCgInPc6	čtyři
úvodních	úvodní	k2eAgInPc6d1	úvodní
zápasech	zápas	k1gInPc6	zápas
ligové	ligový	k2eAgFnSc2d1	ligová
sezóny	sezóna	k1gFnSc2	sezóna
2016	[number]	k4	2016
<g/>
/	/	kIx~	/
<g/>
17	[number]	k4	17
získala	získat	k5eAaPmAgFnS	získat
Slavia	Slavia	k1gFnSc1	Slavia
5	[number]	k4	5
bodů	bod	k1gInPc2	bod
z	z	k7c2	z
12	[number]	k4	12
možných	možný	k2eAgMnPc2d1	možný
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byl	být	k5eAaImAgInS	být
vedle	vedle	k7c2	vedle
neuspokojivého	uspokojivý	k2eNgInSc2d1	neuspokojivý
herního	herní	k2eAgInSc2d1	herní
projevu	projev	k1gInSc2	projev
mužstva	mužstvo	k1gNnSc2	mužstvo
důvod	důvod	k1gInSc1	důvod
k	k	k7c3	k
odvolání	odvolání	k1gNnSc3	odvolání
trenéra	trenér	k1gMnSc2	trenér
Uhrina	Uhrin	k1gMnSc2	Uhrin
a	a	k8xC	a
sportovního	sportovní	k2eAgMnSc2d1	sportovní
ředitele	ředitel	k1gMnSc2	ředitel
Josefa	Josef	k1gMnSc2	Josef
Jinocha	jinoch	k1gMnSc2	jinoch
<g/>
.	.	kIx.	.
</s>
<s>
Novým	nový	k2eAgMnSc7d1	nový
trenérem	trenér	k1gMnSc7	trenér
mužstva	mužstvo	k1gNnSc2	mužstvo
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Šilhavý	šilhavý	k2eAgMnSc1d1	šilhavý
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
v	v	k7c6	v
klubu	klub	k1gInSc6	klub
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1990	[number]	k4	1990
až	až	k9	až
1993	[number]	k4	1993
působil	působit	k5eAaImAgInS	působit
jako	jako	k9	jako
hráč	hráč	k1gMnSc1	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
jeho	jeho	k3xOp3gNnSc7	jeho
vedením	vedení	k1gNnSc7	vedení
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
ve	v	k7c6	v
zbývajících	zbývající	k2eAgInPc6d1	zbývající
14	[number]	k4	14
zápasech	zápas	k1gInPc6	zápas
v	v	k7c6	v
lize	liga	k1gFnSc6	liga
Slavia	Slavia	k1gFnSc1	Slavia
11	[number]	k4	11
<g/>
x	x	k?	x
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
<g/>
,	,	kIx,	,
třikrát	třikrát	k6eAd1	třikrát
remizovala	remizovat	k5eAaPmAgFnS	remizovat
a	a	k8xC	a
ani	ani	k8xC	ani
jednou	jeden	k4xCgFnSc7	jeden
neprohrála	prohrát	k5eNaPmAgFnS	prohrát
a	a	k8xC	a
vystoupala	vystoupat	k5eAaPmAgFnS	vystoupat
tak	tak	k9	tak
na	na	k7c4	na
druhé	druhý	k4xOgNnSc4	druhý
místo	místo	k1gNnSc4	místo
ligové	ligový	k2eAgFnSc2d1	ligová
tabulky	tabulka	k1gFnSc2	tabulka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2016	[number]	k4	2016
odkoupila	odkoupit	k5eAaPmAgFnS	odkoupit
CEFC	CEFC	kA	CEFC
zbývající	zbývající	k2eAgMnSc1d1	zbývající
Šimáněho	Šimáně	k1gMnSc4	Šimáně
akcie	akcie	k1gFnSc2	akcie
<g/>
,	,	kIx,	,
a	a	k8xC	a
oznámila	oznámit	k5eAaPmAgFnS	oznámit
svůj	svůj	k3xOyFgInSc4	svůj
úmysl	úmysl	k1gInSc4	úmysl
kapitalizovat	kapitalizovat	k5eAaBmF	kapitalizovat
své	svůj	k3xOyFgFnPc4	svůj
pohledávky	pohledávka	k1gFnPc4	pohledávka
vůči	vůči	k7c3	vůči
klubu	klub	k1gInSc2	klub
formou	forma	k1gFnSc7	forma
navýšení	navýšení	k1gNnSc2	navýšení
základního	základní	k2eAgNnSc2d1	základní
jmění	jmění	k1gNnSc2	jmění
<g/>
.	.	kIx.	.
</s>
<s>
Tradičními	tradiční	k2eAgFnPc7d1	tradiční
barvami	barva	k1gFnPc7	barva
klubu	klub	k1gInSc2	klub
jsou	být	k5eAaImIp3nP	být
červená	červený	k2eAgFnSc1d1	červená
a	a	k8xC	a
bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
doplněná	doplněný	k2eAgFnSc1d1	doplněná
o	o	k7c4	o
červenou	červený	k2eAgFnSc4d1	červená
pěticípou	pěticípý	k2eAgFnSc4d1	pěticípá
hvězdu	hvězda	k1gFnSc4	hvězda
obrácenou	obrácený	k2eAgFnSc4d1	obrácená
cípem	cíp	k1gInSc7	cíp
dolů	dol	k1gInPc2	dol
<g/>
.	.	kIx.	.
název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
Slavia	Slavia	k1gFnSc1	Slavia
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
latinský	latinský	k2eAgInSc1d1	latinský
termín	termín	k1gInSc1	termín
<g/>
,	,	kIx,	,
označující	označující	k2eAgInPc1d1	označující
ve	v	k7c6	v
starší	starý	k2eAgFnSc6d2	starší
středověké	středověký	k2eAgFnSc3d1	středověká
literatuře	literatura	k1gFnSc3	literatura
území	území	k1gNnSc2	území
obývané	obývaný	k2eAgInPc4d1	obývaný
Slovany	Slovan	k1gInPc4	Slovan
(	(	kIx(	(
<g/>
Slawi	Slaw	k1gFnPc4	Slaw
<g/>
,	,	kIx,	,
Slavi	Slaev	k1gFnSc6	Slaev
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
jejichž	jejichž	k3xOyRp3gNnSc1	jejichž
jméno	jméno	k1gNnSc1	jméno
je	být	k5eAaImIp3nS	být
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
od	od	k7c2	od
jména	jméno	k1gNnSc2	jméno
bohyně	bohyně	k1gFnSc2	bohyně
"	"	kIx"	"
<g/>
Slawa	Slawa	k1gMnSc1	Slawa
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
v	v	k7c6	v
latinsky	latinsky	k6eAd1	latinsky
psané	psaný	k2eAgFnSc6d1	psaná
literatuře	literatura	k1gFnSc6	literatura
rovněž	rovněž	k9	rovněž
transkribované	transkribovaný	k2eAgInPc1d1	transkribovaný
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
Slavia	Slavia	k1gFnSc1	Slavia
<g/>
"	"	kIx"	"
Na	na	k7c6	na
první	první	k4xOgFnSc6	první
výborové	výborový	k2eAgFnSc6d1	výborová
schůzi	schůze	k1gFnSc6	schůze
byly	být	k5eAaImAgFnP	být
odsouhlaseny	odsouhlasen	k2eAgFnPc1d1	odsouhlasena
klubové	klubový	k2eAgFnPc1d1	klubová
slovanské	slovanský	k2eAgFnPc1d1	Slovanská
barvy	barva	k1gFnPc1	barva
červená	červený	k2eAgFnSc1d1	červená
a	a	k8xC	a
bílá	bílý	k2eAgFnSc1d1	bílá
(	(	kIx(	(
<g/>
identické	identický	k2eAgInPc1d1	identický
s	s	k7c7	s
barvami	barva	k1gFnPc7	barva
zemské	zemský	k2eAgFnSc2d1	zemská
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
)	)	kIx)	)
s	s	k7c7	s
odůvodněním	odůvodnění	k1gNnSc7	odůvodnění
<g/>
,	,	kIx,	,
že	že	k8xS	že
<g/>
:	:	kIx,	:
Bílá	bílý	k2eAgFnSc1d1	bílá
barva	barva	k1gFnSc1	barva
je	být	k5eAaImIp3nS	být
čistota	čistota	k1gFnSc1	čistota
sportovní	sportovní	k2eAgFnSc2d1	sportovní
myšlenky	myšlenka	k1gFnSc2	myšlenka
a	a	k8xC	a
čestného	čestný	k2eAgInSc2d1	čestný
boje	boj	k1gInSc2	boj
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
protivník	protivník	k1gMnSc1	protivník
není	být	k5eNaImIp3nS	být
nepřítel	nepřítel	k1gMnSc1	nepřítel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
uznávaný	uznávaný	k2eAgMnSc1d1	uznávaný
soupeř	soupeř	k1gMnSc1	soupeř
<g/>
.	.	kIx.	.
</s>
<s>
Červená	červený	k2eAgFnSc1d1	červená
barva	barva	k1gFnSc1	barva
je	být	k5eAaImIp3nS	být
symbolem	symbol	k1gInSc7	symbol
srdce	srdce	k1gNnSc2	srdce
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
vkládáme	vkládat	k5eAaImIp1nP	vkládat
do	do	k7c2	do
svých	svůj	k3xOyFgInPc2	svůj
zápasů	zápas	k1gInPc2	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Hvězda	hvězda	k1gFnSc1	hvězda
v	v	k7c6	v
bílém	bílý	k2eAgNnSc6d1	bílé
poli	pole	k1gNnSc6	pole
představuje	představovat	k5eAaImIp3nS	představovat
stále	stále	k6eAd1	stále
novou	nový	k2eAgFnSc4d1	nová
naději	naděje	k1gFnSc4	naděje
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
povznáší	povznášet	k5eAaImIp3nS	povznášet
mysl	mysl	k1gFnSc4	mysl
a	a	k8xC	a
sílí	sílet	k5eAaImIp3nS	sílet
ducha	duch	k1gMnSc4	duch
i	i	k9	i
v	v	k7c6	v
chmurných	chmurný	k2eAgNnPc6d1	chmurné
obdobích	období	k1gNnPc6	období
nezdaru	nezdar	k1gInSc2	nezdar
a	a	k8xC	a
neúspěchu	neúspěch	k1gInSc2	neúspěch
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
odlišné	odlišný	k2eAgFnPc1d1	odlišná
sešívané	sešívaný	k2eAgFnPc1d1	sešívaná
poloviny	polovina	k1gFnPc1	polovina
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
žádný	žádný	k3yNgMnSc1	žádný
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
žádná	žádný	k3yNgFnSc1	žádný
věc	věc	k1gFnSc1	věc
nemají	mít	k5eNaImIp3nP	mít
jen	jen	k9	jen
jednu	jeden	k4xCgFnSc4	jeden
stránku	stránka	k1gFnSc4	stránka
a	a	k8xC	a
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
nalézt	nalézt	k5eAaPmF	nalézt
soulad	soulad	k1gInSc4	soulad
mezi	mezi	k7c7	mezi
vůlí	vůle	k1gFnSc7	vůle
a	a	k8xC	a
citem	cit	k1gInSc7	cit
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
silou	síla	k1gFnSc7	síla
a	a	k8xC	a
technickou	technický	k2eAgFnSc7d1	technická
jemností	jemnost	k1gFnSc7	jemnost
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nadšením	nadšení	k1gNnSc7	nadšení
a	a	k8xC	a
zklamáním	zklamání	k1gNnSc7	zklamání
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
sluncem	slunce	k1gNnSc7	slunce
a	a	k8xC	a
stínem	stín	k1gInSc7	stín
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
veselá	veselý	k2eAgFnSc1d1	veselá
nápadnost	nápadnost	k1gFnSc1	nápadnost
tohoto	tento	k3xDgNnSc2	tento
spojení	spojení	k1gNnSc2	spojení
svědčí	svědčit	k5eAaImIp3nS	svědčit
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
sport	sport	k1gInSc1	sport
musí	muset	k5eAaImIp3nS	muset
provozovat	provozovat	k5eAaImF	provozovat
s	s	k7c7	s
upřímnou	upřímný	k2eAgFnSc7d1	upřímná
radostí	radost	k1gFnSc7	radost
a	a	k8xC	a
že	že	k8xS	že
musí	muset	k5eAaImIp3nS	muset
všem	všecek	k3xTgMnPc3	všecek
na	na	k7c6	na
hřišti	hřiště	k1gNnSc6	hřiště
i	i	k9	i
za	za	k7c7	za
bariérami	bariéra	k1gFnPc7	bariéra
přinášet	přinášet	k5eAaImF	přinášet
radost	radost	k1gFnSc4	radost
a	a	k8xC	a
uspokojení	uspokojení	k1gNnSc4	uspokojení
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
kroji-dresu	krojires	k1gInSc2	kroji-dres
příslušela	příslušet	k5eAaImAgFnS	příslušet
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
i	i	k8xC	i
červená	červený	k2eAgFnSc1d1	červená
čapka	čapka	k1gFnSc1	čapka
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
dres	dres	k1gInSc1	dres
Slavie	slavie	k1gFnSc2	slavie
je	být	k5eAaImIp3nS	být
stejně	stejně	k6eAd1	stejně
starý	starý	k2eAgInSc1d1	starý
jako	jako	k8xC	jako
tým	tým	k1gInSc1	tým
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
jej	on	k3xPp3gMnSc4	on
nosí	nosit	k5eAaImIp3nS	nosit
<g/>
.	.	kIx.	.
</s>
<s>
Tým	tým	k1gInSc1	tým
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
totiž	totiž	k9	totiž
nastupuje	nastupovat	k5eAaImIp3nS	nastupovat
již	již	k6eAd1	již
od	od	k7c2	od
historicky	historicky	k6eAd1	historicky
prvního	první	k4xOgInSc2	první
zápasu	zápas	k1gInSc2	zápas
z	z	k7c2	z
25	[number]	k4	25
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1896	[number]	k4	1896
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
tým	tým	k1gInSc1	tým
včetně	včetně	k7c2	včetně
brankáře	brankář	k1gMnSc2	brankář
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
v	v	k7c6	v
sešívaném	sešívaný	k2eAgInSc6d1	sešívaný
dresu	dres	k1gInSc6	dres
z	z	k7c2	z
pravé	pravý	k2eAgFnSc2d1	pravá
poloviny	polovina	k1gFnSc2	polovina
červeném	červené	k1gNnSc6	červené
(	(	kIx(	(
<g/>
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
nositele	nositel	k1gMnSc2	nositel
<g/>
)	)	kIx)	)
a	a	k8xC	a
z	z	k7c2	z
levé	levý	k2eAgFnSc2d1	levá
poloviny	polovina	k1gFnSc2	polovina
bílém	bílý	k2eAgNnSc6d1	bílé
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
bílém	bílý	k2eAgNnSc6d1	bílé
poli	pole	k1gNnSc6	pole
byla	být	k5eAaImAgFnS	být
posazena	posazen	k2eAgFnSc1d1	posazena
velká	velký	k2eAgFnSc1d1	velká
červená	červený	k2eAgFnSc1d1	červená
pěticípá	pěticípý	k2eAgFnSc1d1	pěticípá
hvězda	hvězda	k1gFnSc1	hvězda
otočená	otočený	k2eAgFnSc1d1	otočená
vzhůru	vzhůru	k6eAd1	vzhůru
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
byl	být	k5eAaImAgInS	být
součástí	součást	k1gFnSc7	součást
úboru	úbor	k1gInSc2	úbor
dokonce	dokonce	k9	dokonce
i	i	k9	i
červený	červený	k2eAgInSc1d1	červený
baret	baret	k1gInSc1	baret
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
dres	dres	k1gInSc1	dres
se	se	k3xPyFc4	se
zachoval	zachovat	k5eAaPmAgInS	zachovat
až	až	k9	až
do	do	k7c2	do
dnešních	dnešní	k2eAgInPc2d1	dnešní
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
měnil	měnit	k5eAaImAgInS	měnit
pouze	pouze	k6eAd1	pouze
materiál	materiál	k1gInSc1	materiál
na	na	k7c4	na
modernější	moderní	k2eAgInPc4d2	modernější
a	a	k8xC	a
měnila	měnit	k5eAaImAgFnS	měnit
se	se	k3xPyFc4	se
i	i	k9	i
velikost	velikost	k1gFnSc1	velikost
červené	červený	k2eAgFnSc2d1	červená
hvězdy	hvězda	k1gFnSc2	hvězda
umístěné	umístěný	k2eAgFnSc2d1	umístěná
na	na	k7c6	na
srdci	srdce	k1gNnSc6	srdce
<g/>
.	.	kIx.	.
</s>
<s>
Slovanskou	slovanský	k2eAgFnSc4d1	Slovanská
kombinaci	kombinace	k1gFnSc4	kombinace
barev	barva	k1gFnPc2	barva
nezakázali	zakázat	k5eNaPmAgMnP	zakázat
ani	ani	k8xC	ani
Němci	Němec	k1gMnPc1	Němec
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
však	však	k9	však
jedno	jeden	k4xCgNnSc4	jeden
období	období	k1gNnSc4	období
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
dres	dres	k1gInSc4	dres
měnil	měnit	k5eAaImAgInS	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgMnS	být
součástí	součást	k1gFnSc7	součást
opatření	opatření	k1gNnSc2	opatření
komunistického	komunistický	k2eAgInSc2d1	komunistický
režimu	režim	k1gInSc2	režim
proti	proti	k7c3	proti
Slavii	slavie	k1gFnSc3	slavie
i	i	k8xC	i
zákaz	zákaz	k1gInSc4	zákaz
tradičního	tradiční	k2eAgInSc2d1	tradiční
dresu	dres	k1gInSc2	dres
<g/>
.	.	kIx.	.
</s>
<s>
Následovala	následovat	k5eAaImAgFnS	následovat
pak	pak	k6eAd1	pak
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
různých	různý	k2eAgFnPc2d1	různá
kombinací	kombinace	k1gFnPc2	kombinace
včetně	včetně	k7c2	včetně
modré	modrý	k2eAgFnSc2d1	modrá
<g/>
,	,	kIx,	,
bílé	bílý	k2eAgFnSc2d1	bílá
a	a	k8xC	a
žluté	žlutý	k2eAgFnSc2d1	žlutá
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Návrat	návrat	k1gInSc1	návrat
tradičního	tradiční	k2eAgInSc2d1	tradiční
dresu	dres	k1gInSc2	dres
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
za	za	k7c2	za
nevole	nevole	k1gFnSc2	nevole
režimu	režim	k1gInSc2	režim
2	[number]	k4	2
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1956	[number]	k4	1956
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
tohoto	tento	k3xDgNnSc2	tento
data	datum	k1gNnSc2	datum
je	být	k5eAaImIp3nS	být
dres	dres	k1gInSc4	dres
již	již	k6eAd1	již
pouze	pouze	k6eAd1	pouze
červeno-bílý	červenoílý	k2eAgInSc1d1	červeno-bílý
<g/>
.	.	kIx.	.
</s>
<s>
Domácí	domácí	k2eAgInSc1d1	domácí
dres	dres	k1gInSc1	dres
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
symbolem	symbol	k1gInSc7	symbol
klubu	klub	k1gInSc2	klub
<g/>
.	.	kIx.	.
</s>
<s>
Vzhled	vzhled	k1gInSc1	vzhled
venkovního	venkovní	k2eAgInSc2d1	venkovní
dresu	dres	k1gInSc2	dres
se	se	k3xPyFc4	se
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
často	často	k6eAd1	často
měnil	měnit	k5eAaImAgInS	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Níže	nízce	k6eAd2	nízce
jsou	být	k5eAaImIp3nP	být
uvedené	uvedený	k2eAgFnPc1d1	uvedená
některé	některý	k3yIgFnPc1	některý
použité	použitý	k2eAgFnPc1d1	použitá
kombinace	kombinace	k1gFnPc1	kombinace
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
československá	československý	k2eAgFnSc1d1	Československá
<g/>
/	/	kIx~	/
<g/>
česká	český	k2eAgFnSc1d1	Česká
ligová	ligový	k2eAgFnSc1d1	ligová
soutěž	soutěž	k1gFnSc1	soutěž
(	(	kIx(	(
<g/>
17	[number]	k4	17
<g/>
×	×	k?	×
<g/>
)	)	kIx)	)
Mistrovství	mistrovství	k1gNnSc1	mistrovství
ČSF	ČSF	kA	ČSF
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
×	×	k?	×
<g/>
)	)	kIx)	)
1913	[number]	k4	1913
Fotbalová	fotbalový	k2eAgFnSc1d1	fotbalová
liga	liga	k1gFnSc1	liga
Československa	Československo	k1gNnSc2	Československo
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
×	×	k?	×
<g/>
)	)	kIx)	)
1925	[number]	k4	1925
<g/>
,	,	kIx,	,
1928	[number]	k4	1928
<g/>
/	/	kIx~	/
<g/>
29	[number]	k4	29
<g/>
,	,	kIx,	,
1929	[number]	k4	1929
<g/>
/	/	kIx~	/
<g/>
30	[number]	k4	30
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
1930	[number]	k4	1930
<g/>
/	/	kIx~	/
<g/>
31	[number]	k4	31
<g/>
,	,	kIx,	,
1932	[number]	k4	1932
<g/>
/	/	kIx~	/
<g/>
33	[number]	k4	33
<g/>
,	,	kIx,	,
1933	[number]	k4	1933
<g/>
/	/	kIx~	/
<g/>
34	[number]	k4	34
<g/>
,	,	kIx,	,
1934	[number]	k4	1934
<g/>
/	/	kIx~	/
<g/>
35	[number]	k4	35
<g/>
,	,	kIx,	,
1936	[number]	k4	1936
<g/>
/	/	kIx~	/
<g/>
37	[number]	k4	37
<g/>
,	,	kIx,	,
1946	[number]	k4	1946
<g/>
/	/	kIx~	/
<g/>
47	[number]	k4	47
Mistrovství	mistrovství	k1gNnSc1	mistrovství
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
Moravy	Morava	k1gFnSc2	Morava
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
<g />
.	.	kIx.	.
</s>
<s>
×	×	k?	×
<g/>
)	)	kIx)	)
1939	[number]	k4	1939
<g/>
/	/	kIx~	/
<g/>
40	[number]	k4	40
<g/>
,	,	kIx,	,
1940	[number]	k4	1940
<g/>
/	/	kIx~	/
<g/>
41	[number]	k4	41
<g/>
,	,	kIx,	,
1941	[number]	k4	1941
<g/>
/	/	kIx~	/
<g/>
42	[number]	k4	42
<g/>
,	,	kIx,	,
1942	[number]	k4	1942
<g/>
/	/	kIx~	/
<g/>
43	[number]	k4	43
1	[number]	k4	1
<g/>
.	.	kIx.	.
česká	český	k2eAgFnSc1d1	Česká
fotbalová	fotbalový	k2eAgFnSc1d1	fotbalová
liga	liga	k1gFnSc1	liga
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
×	×	k?	×
<g/>
)	)	kIx)	)
1995	[number]	k4	1995
<g/>
/	/	kIx~	/
<g/>
96	[number]	k4	96
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g />
.	.	kIx.	.
</s>
<s>
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
Český	český	k2eAgInSc1d1	český
fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
pohár	pohár	k1gInSc1	pohár
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
×	×	k?	×
<g/>
)	)	kIx)	)
1940	[number]	k4	1940
<g/>
/	/	kIx~	/
<g/>
41	[number]	k4	41
<g/>
,	,	kIx,	,
1941	[number]	k4	1941
<g/>
/	/	kIx~	/
<g/>
42	[number]	k4	42
<g/>
,	,	kIx,	,
1944	[number]	k4	1944
<g/>
/	/	kIx~	/
<g/>
45	[number]	k4	45
<g/>
,	,	kIx,	,
1973	[number]	k4	1973
<g/>
/	/	kIx~	/
<g/>
<g />
.	.	kIx.	.
</s>
<s>
74	[number]	k4	74
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
/	/	kIx~	/
<g/>
97	[number]	k4	97
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
/	/	kIx~	/
<g/>
99	[number]	k4	99
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
Mistrovství	mistrovství	k1gNnPc2	mistrovství
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
Moravy	Morava	k1gFnSc2	Morava
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
×	×	k?	×
<g/>
)	)	kIx)	)
1897	[number]	k4	1897
jaro	jaro	k1gNnSc1	jaro
<g/>
,	,	kIx,	,
1897	[number]	k4	1897
podzim	podzim	k1gInSc1	podzim
<g/>
,	,	kIx,	,
1898	[number]	k4	1898
<g/>
,	,	kIx,	,
1899	[number]	k4	1899
<g/>
,	,	kIx,	,
1900	[number]	k4	1900
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
1901	[number]	k4	1901
Mistrovství	mistrovství	k1gNnSc2	mistrovství
ČSF	ČSF	kA	ČSF
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
×	×	k?	×
<g/>
)	)	kIx)	)
1915	[number]	k4	1915
Mistrovství	mistrovství	k1gNnSc1	mistrovství
Středočeské	středočeský	k2eAgFnSc2d1	Středočeská
župy	župa	k1gFnSc2	župa
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
×	×	k?	×
<g/>
)	)	kIx)	)
1918	[number]	k4	1918
<g/>
,	,	kIx,	,
1924	[number]	k4	1924
Pohár	pohár	k1gInSc1	pohár
dobročinnosti	dobročinnost	k1gFnSc2	dobročinnost
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
×	×	k?	×
<g/>
)	)	kIx)	)
1908	[number]	k4	1908
<g/>
,	,	kIx,	,
1910	[number]	k4	1910
<g/>
,	,	kIx,	,
1911	[number]	k4	1911
<g/>
,	,	kIx,	,
1912	[number]	k4	1912
Středočeský	středočeský	k2eAgInSc1d1	středočeský
pohár	pohár	k1gInSc1	pohár
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
10	[number]	k4	10
<g/>
×	×	k?	×
<g/>
)	)	kIx)	)
1922	[number]	k4	1922
<g/>
,	,	kIx,	,
1926	[number]	k4	1926
<g/>
,	,	kIx,	,
1927	[number]	k4	1927
<g/>
,	,	kIx,	,
1928	[number]	k4	1928
<g/>
,	,	kIx,	,
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
1932	[number]	k4	1932
<g/>
,	,	kIx,	,
1935	[number]	k4	1935
<g/>
,	,	kIx,	,
1941	[number]	k4	1941
<g/>
,	,	kIx,	,
1943	[number]	k4	1943
<g/>
,	,	kIx,	,
1944	[number]	k4	1944
Perleťový	perleťový	k2eAgInSc1d1	perleťový
pohár	pohár	k1gInSc1	pohár
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
×	×	k?	×
<g/>
)	)	kIx)	)
1966	[number]	k4	1966
<g/>
,	,	kIx,	,
1984	[number]	k4	1984
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
Středoevropský	středoevropský	k2eAgInSc1d1	středoevropský
pohár	pohár	k1gInSc1	pohár
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
×	×	k?	×
<g/>
)	)	kIx)	)
1938	[number]	k4	1938
Interpohár	Interpohár	k1gInSc1	Interpohár
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
×	×	k?	×
<g/>
)	)	kIx)	)
1970	[number]	k4	1970
<g/>
,	,	kIx,	,
1972	[number]	k4	1972
<g/>
,	,	kIx,	,
1977	[number]	k4	1977
<g/>
,	,	kIx,	,
1978	[number]	k4	1978
<g/>
,	,	kIx,	,
1986	[number]	k4	1986
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
1892	[number]	k4	1892
-	-	kIx~	-
SK	Sk	kA	Sk
ACOS	ACOS	kA	ACOS
Praha	Praha	k1gFnSc1	Praha
(	(	kIx(	(
<g/>
Sportovní	sportovní	k2eAgNnSc1d1	sportovní
<g />
.	.	kIx.	.
</s>
<s>
klub	klub	k1gInSc1	klub
Akadamický	Akadamický	k2eAgInSc1d1	Akadamický
cyklistický	cyklistický	k2eAgInSc1d1	cyklistický
odbor	odbor	k1gInSc1	odbor
Slavia	Slavia	k1gFnSc1	Slavia
<g/>
)	)	kIx)	)
1895	[number]	k4	1895
-	-	kIx~	-
S.	S.	kA	S.
<g/>
K.	K.	kA	K.
Slavia	Slavia	k1gFnSc1	Slavia
(	(	kIx(	(
<g/>
Sportovní	sportovní	k2eAgInSc1d1	sportovní
klub	klub	k1gInSc1	klub
Slavia	Slavia	k1gFnSc1	Slavia
<g/>
)	)	kIx)	)
1948	[number]	k4	1948
-	-	kIx~	-
Sokol	Sokol	k1gMnSc1	Sokol
Slavia	Slavia	k1gFnSc1	Slavia
Praha	Praha	k1gFnSc1	Praha
1949	[number]	k4	1949
-	-	kIx~	-
ZSJ	ZSJ	kA	ZSJ
Dynamo	dynamo	k1gNnSc1	dynamo
Slavia	Slavia	k1gFnSc1	Slavia
Praha	Praha	k1gFnSc1	Praha
(	(	kIx(	(
<g/>
Základní	základní	k2eAgFnSc1d1	základní
sportovní	sportovní	k2eAgFnSc1d1	sportovní
jednota	jednota	k1gFnSc1	jednota
Dynamo	dynamo	k1gNnSc1	dynamo
Slavia	Slavius	k1gMnSc2	Slavius
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
1953	[number]	k4	1953
-	-	kIx~	-
DSO	DSO	kA	DSO
Dynamo	dynamo	k1gNnSc1	dynamo
Praha	Praha	k1gFnSc1	Praha
(	(	kIx(	(
<g/>
Dobrovolná	Dobrovolná	k1gFnSc1	Dobrovolná
sportovní	sportovní	k2eAgFnSc2d1	sportovní
<g />
.	.	kIx.	.
</s>
<s>
organizace	organizace	k1gFnPc1	organizace
Dynamo	dynamo	k1gNnSc1	dynamo
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
1954	[number]	k4	1954
-	-	kIx~	-
TJ	tj	kA	tj
Dynamo	dynamo	k1gNnSc1	dynamo
Praha	Praha	k1gFnSc1	Praha
(	(	kIx(	(
<g/>
Tělovýchovná	tělovýchovný	k2eAgFnSc1d1	Tělovýchovná
jednota	jednota	k1gFnSc1	jednota
Dynamo	dynamo	k1gNnSc1	dynamo
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
1965	[number]	k4	1965
-	-	kIx~	-
SK	Sk	kA	Sk
Slavia	Slavia	k1gFnSc1	Slavia
Praha	Praha	k1gFnSc1	Praha
(	(	kIx(	(
<g/>
Sportovní	sportovní	k2eAgInSc1d1	sportovní
klub	klub	k1gInSc1	klub
Slavia	Slavia	k1gFnSc1	Slavia
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
1973	[number]	k4	1973
-	-	kIx~	-
TJ	tj	kA	tj
Slavia	Slavia	k1gFnSc1	Slavia
Praha	Praha	k1gFnSc1	Praha
(	(	kIx(	(
<g/>
Tělovýchovná	tělovýchovný	k2eAgFnSc1d1	Tělovýchovná
jednota	jednota	k1gFnSc1	jednota
Slavia	Slavia	k1gFnSc1	Slavia
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
1977	[number]	k4	1977
-	-	kIx~	-
TJ	tj	kA	tj
Slavia	Slavia	k1gFnSc1	Slavia
IPS	IPS	kA	IPS
Praha	Praha	k1gFnSc1	Praha
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
Tělovýchovná	tělovýchovný	k2eAgFnSc1d1	Tělovýchovná
jednota	jednota	k1gFnSc1	jednota
Slavia	Slavia	k1gFnSc1	Slavia
Inženýrské	inženýrský	k2eAgFnSc2d1	inženýrská
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
stavby	stavba	k1gFnSc2	stavba
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
1978	[number]	k4	1978
-	-	kIx~	-
SK	Sk	kA	Sk
Slavia	Slavia	k1gFnSc1	Slavia
IPS	IPS	kA	IPS
Praha	Praha	k1gFnSc1	Praha
(	(	kIx(	(
<g/>
Sportovní	sportovní	k2eAgInSc1d1	sportovní
klub	klub	k1gInSc1	klub
Slavia	Slavium	k1gNnSc2	Slavium
Inženýrské	inženýrský	k2eAgFnSc2d1	inženýrská
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
stavby	stavba	k1gFnSc2	stavba
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
1991	[number]	k4	1991
-	-	kIx~	-
SK	Sk	kA	Sk
Slavia	Slavia	k1gFnSc1	Slavia
Praha	Praha	k1gFnSc1	Praha
(	(	kIx(	(
<g/>
Sportovní	sportovní	k2eAgInSc1d1	sportovní
klub	klub	k1gInSc1	klub
Slavia	Slavia	k1gFnSc1	Slavia
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
Nejlepší	dobrý	k2eAgNnSc1d3	nejlepší
umístění	umístění	k1gNnSc1	umístění
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
17	[number]	k4	17
<g/>
×	×	k?	×
<g/>
)	)	kIx)	)
Nejhorší	zlý	k2eAgNnSc4d3	nejhorší
umístění	umístění	k1gNnSc4	umístění
<g/>
:	:	kIx,	:
14	[number]	k4	14
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
×	×	k?	×
-	-	kIx~	-
1960	[number]	k4	1960
<g/>
/	/	kIx~	/
<g/>
61	[number]	k4	61
<g/>
,	,	kIx,	,
1962	[number]	k4	1962
<g/>
/	/	kIx~	/
<g/>
63	[number]	k4	63
a	a	k8xC	a
1972	[number]	k4	1972
<g/>
/	/	kIx~	/
<g/>
73	[number]	k4	73
<g/>
)	)	kIx)	)
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
výhra	výhra	k1gFnSc1	výhra
<g/>
:	:	kIx,	:
15	[number]	k4	15
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
výhra	výhra	k1gFnSc1	výhra
<g />
.	.	kIx.	.
</s>
<s>
nad	nad	k7c4	nad
SK	Sk	kA	Sk
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
(	(	kIx(	(
<g/>
1947	[number]	k4	1947
<g/>
/	/	kIx~	/
<g/>
48	[number]	k4	48
<g/>
)	)	kIx)	)
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
prohra	prohra	k1gFnSc1	prohra
<g/>
:	:	kIx,	:
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
7	[number]	k4	7
domácí	domácí	k2eAgFnSc1d1	domácí
prohra	prohra	k1gFnSc1	prohra
s	s	k7c7	s
Teplicemi	Teplice	k1gFnPc7	Teplice
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
/	/	kIx~	/
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
Nejvíce	hodně	k6eAd3	hodně
vítězství	vítězství	k1gNnSc1	vítězství
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
<g/>
:	:	kIx,	:
23	[number]	k4	23
zápasů	zápas	k1gInPc2	zápas
(	(	kIx(	(
<g/>
28	[number]	k4	28
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1929	[number]	k4	1929
-	-	kIx~	-
9	[number]	k4	9
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
Nejdelší	dlouhý	k2eAgNnPc4d3	nejdelší
období	období	k1gNnPc4	období
bez	bez	k7c2	bez
porážky	porážka	k1gFnSc2	porážka
<g/>
:	:	kIx,	:
27	[number]	k4	27
zápasů	zápas	k1gInPc2	zápas
(	(	kIx(	(
<g/>
1928	[number]	k4	1928
<g/>
-	-	kIx~	-
<g/>
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
Nejdelší	dlouhý	k2eAgNnPc4d3	nejdelší
období	období	k1gNnPc4	období
bez	bez	k7c2	bez
výhry	výhra	k1gFnSc2	výhra
<g/>
:	:	kIx,	:
16	[number]	k4	16
zápasů	zápas	k1gInPc2	zápas
(	(	kIx(	(
<g/>
1956	[number]	k4	1956
<g/>
-	-	kIx~	-
<g/>
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
Nejdelší	dlouhý	k2eAgNnPc4d3	nejdelší
období	období	k1gNnPc4	období
bez	bez	k7c2	bez
porážky	porážka	k1gFnSc2	porážka
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
doma	doma	k6eAd1	doma
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
32	[number]	k4	32
zápasů	zápas	k1gInPc2	zápas
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
1998	[number]	k4	1998
-	-	kIx~	-
30	[number]	k4	30
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
Nejdelší	dlouhý	k2eAgNnPc4d3	nejdelší
období	období	k1gNnPc4	období
bez	bez	k7c2	bez
výhry	výhra	k1gFnSc2	výhra
(	(	kIx(	(
<g/>
venku	venku	k6eAd1	venku
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
22	[number]	k4	22
zápasů	zápas	k1gInPc2	zápas
(	(	kIx(	(
<g/>
1977	[number]	k4	1977
<g/>
-	-	kIx~	-
<g/>
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Nejdelší	dlouhý	k2eAgNnSc1d3	nejdelší
období	období	k1gNnSc1	období
bez	bez	k7c2	bez
obdrženého	obdržený	k2eAgInSc2d1	obdržený
gólu	gól	k1gInSc2	gól
(	(	kIx(	(
<g/>
samostatně	samostatně	k6eAd1	samostatně
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Jánoš	Jánoš	k1gMnSc1	Jánoš
-	-	kIx~	-
586	[number]	k4	586
min	mina	k1gFnPc2	mina
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
-	-	kIx~	-
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
Nejdelší	dlouhý	k2eAgNnSc1d3	nejdelší
období	období	k1gNnSc1	období
bez	bez	k7c2	bez
obdrženého	obdržený	k2eAgInSc2d1	obdržený
gólu	gól	k1gInSc2	gól
(	(	kIx(	(
<g/>
společně	společně	k6eAd1	společně
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Stejskal	Stejskal	k1gMnSc1	Stejskal
a	a	k8xC	a
Radek	Radek	k1gMnSc1	Radek
Černý	Černý	k1gMnSc1	Černý
600	[number]	k4	600
min	mina	k1gFnPc2	mina
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
Nejdelší	dlouhý	k2eAgNnSc1d3	nejdelší
období	období	k1gNnSc1	období
bez	bez	k7c2	bez
vstřeleného	vstřelený	k2eAgInSc2d1	vstřelený
gólu	gól	k1gInSc2	gól
<g/>
:	:	kIx,	:
505	[number]	k4	505
min	mina	k1gFnPc2	mina
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
-	-	kIx~	-
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
Nejvíce	nejvíce	k6eAd1	nejvíce
uhraných	uhraný	k2eAgInPc2d1	uhraný
bodů	bod	k1gInPc2	bod
<g/>
:	:	kIx,	:
70	[number]	k4	70
(	(	kIx(	(
<g/>
sezóna	sezóna	k1gFnSc1	sezóna
1995	[number]	k4	1995
<g/>
/	/	kIx~	/
<g/>
96	[number]	k4	96
<g/>
)	)	kIx)	)
Nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
střelec	střelec	k1gMnSc1	střelec
klubu	klub	k1gInSc2	klub
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
:	:	kIx,	:
Josef	Josef	k1gMnSc1	Josef
Bican	Bican	k1gMnSc1	Bican
-	-	kIx~	-
417	[number]	k4	417
gólů	gól	k1gInPc2	gól
<g />
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
gólů	gól	k1gInPc2	gól
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
sezóně	sezóna	k1gFnSc6	sezóna
(	(	kIx(	(
<g/>
jednotlivec	jednotlivec	k1gMnSc1	jednotlivec
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Josef	Josef	k1gMnSc1	Josef
Bican	Bican	k1gMnSc1	Bican
-	-	kIx~	-
57	[number]	k4	57
gólů	gól	k1gInPc2	gól
(	(	kIx(	(
<g/>
1943	[number]	k4	1943
<g/>
/	/	kIx~	/
<g/>
44	[number]	k4	44
<g/>
)	)	kIx)	)
Nejvíce	nejvíce	k6eAd1	nejvíce
gólů	gól	k1gInPc2	gól
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
sezóně	sezóna	k1gFnSc6	sezóna
(	(	kIx(	(
<g/>
mužstvo	mužstvo	k1gNnSc1	mužstvo
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
131	[number]	k4	131
gólů	gól	k1gInPc2	gól
(	(	kIx(	(
<g/>
1943	[number]	k4	1943
<g/>
/	/	kIx~	/
<g/>
44	[number]	k4	44
<g/>
)	)	kIx)	)
Nejvíce	hodně	k6eAd3	hodně
<g />
.	.	kIx.	.
</s>
<s>
gólů	gól	k1gInPc2	gól
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
zápase	zápas	k1gInSc6	zápas
(	(	kIx(	(
<g/>
jednotlivec	jednotlivec	k1gMnSc1	jednotlivec
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
7	[number]	k4	7
gólů	gól	k1gInPc2	gól
-	-	kIx~	-
Josef	Josef	k1gMnSc1	Josef
Bican	Bican	k1gMnSc1	Bican
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
×	×	k?	×
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Horák	Horák	k1gMnSc1	Horák
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
×	×	k?	×
<g/>
)	)	kIx)	)
Nejvíce	hodně	k6eAd3	hodně
ligových	ligový	k2eAgInPc2d1	ligový
startů	start	k1gInPc2	start
<g/>
:	:	kIx,	:
František	František	k1gMnSc1	František
Veselý	Veselý	k1gMnSc1	Veselý
-	-	kIx~	-
404	[number]	k4	404
Nejvíce	hodně	k6eAd3	hodně
ligových	ligový	k2eAgInPc2d1	ligový
startů	start	k1gInPc2	start
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
<g/>
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s>
František	František	k1gMnSc1	František
Veselý	Veselý	k1gMnSc1	Veselý
-	-	kIx~	-
133	[number]	k4	133
Nejčastější	častý	k2eAgMnSc1d3	nejčastější
klubový	klubový	k2eAgMnSc1d1	klubový
král	král	k1gMnSc1	král
střelců	střelec	k1gMnPc2	střelec
<g/>
:	:	kIx,	:
Josef	Josef	k1gMnSc1	Josef
Bican	Bican	k1gMnSc1	Bican
-	-	kIx~	-
13	[number]	k4	13
<g/>
×	×	k?	×
Absolutní	absolutní	k2eAgFnSc1d1	absolutní
<g/>
:	:	kIx,	:
50	[number]	k4	50
000	[number]	k4	000
diváků	divák	k1gMnPc2	divák
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1945	[number]	k4	1945
přátelské	přátelský	k2eAgNnSc4d1	přátelské
utkání	utkání	k1gNnSc4	utkání
s	s	k7c7	s
ŠK	ŠK	kA	ŠK
Bratislava	Bratislava	k1gFnSc1	Bratislava
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
-	-	kIx~	-
Strahov	Strahov	k1gInSc1	Strahov
V	v	k7c6	v
evropských	evropský	k2eAgInPc6d1	evropský
pohárech	pohár	k1gInPc6	pohár
:	:	kIx,	:
45	[number]	k4	45
000	[number]	k4	000
diváků	divák	k1gMnPc2	divák
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1938	[number]	k4	1938
finále	finále	k1gNnSc2	finále
STEP	step	k1gInSc1	step
s	s	k7c7	s
Ferencvárosi	Ferencváros	k1gMnPc7	Ferencváros
TC	tc	k0	tc
Budapest	Budapest	k1gInSc1	Budapest
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
-	-	kIx~	-
Strahov	Strahov	k1gInSc1	Strahov
1	[number]	k4	1
<g/>
.	.	kIx.	.
liga	liga	k1gFnSc1	liga
<g/>
:	:	kIx,	:
42	[number]	k4	42
000	[number]	k4	000
diváků	divák	k1gMnPc2	divák
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1966	[number]	k4	1966
se	s	k7c7	s
Spartou	Sparta	k1gFnSc7	Sparta
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
-	-	kIx~	-
Eden	Eden	k1gInSc1	Eden
2	[number]	k4	2
<g />
.	.	kIx.	.
</s>
<s>
<g/>
liga	liga	k1gFnSc1	liga
<g/>
:	:	kIx,	:
42	[number]	k4	42
000	[number]	k4	000
diváků	divák	k1gMnPc2	divák
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1965	[number]	k4	1965
s	s	k7c7	s
Plzní	Plzeň	k1gFnSc7	Plzeň
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
-	-	kIx~	-
Eden	Eden	k1gInSc1	Eden
1	[number]	k4	1
<g/>
.	.	kIx.	.
liga	liga	k1gFnSc1	liga
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
<g/>
:	:	kIx,	:
20	[number]	k4	20
698	[number]	k4	698
diváků	divák	k1gMnPc2	divák
(	(	kIx(	(
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2008	[number]	k4	2008
s	s	k7c7	s
Jabloncem	Jablonec	k1gInSc7	Jablonec
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s>
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
-	-	kIx~	-
Eden	Eden	k1gInSc1	Eden
Jan	Jan	k1gMnSc1	Jan
Vaník	Vaník	k1gMnSc1	Vaník
-	-	kIx~	-
1925	[number]	k4	1925
(	(	kIx(	(
<g/>
13	[number]	k4	13
gólů	gól	k1gInPc2	gól
<g/>
)	)	kIx)	)
Antonín	Antonín	k1gMnSc1	Antonín
Puč	puč	k1gInSc1	puč
-	-	kIx~	-
1927	[number]	k4	1927
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1928	[number]	k4	1928
<g/>
/	/	kIx~	/
<g/>
29	[number]	k4	29
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
)	)	kIx)	)
Jiří	Jiří	k1gMnSc1	Jiří
Sobotka	Sobotka	k1gMnSc1	Sobotka
-	-	kIx~	-
1933	[number]	k4	1933
<g/>
/	/	kIx~	/
<g/>
34	[number]	k4	34
(	(	kIx(	(
<g/>
18	[number]	k4	18
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
František	František	k1gMnSc1	František
Svoboda	Svoboda	k1gMnSc1	Svoboda
-	-	kIx~	-
1934	[number]	k4	1934
<g/>
/	/	kIx~	/
<g/>
35	[number]	k4	35
(	(	kIx(	(
<g/>
27	[number]	k4	27
<g/>
)	)	kIx)	)
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Bradáč	Bradáč	k1gMnSc1	Bradáč
-	-	kIx~	-
1935	[number]	k4	1935
<g/>
/	/	kIx~	/
<g/>
36	[number]	k4	36
(	(	kIx(	(
<g/>
42	[number]	k4	42
<g/>
)	)	kIx)	)
Josef	Josef	k1gMnSc1	Josef
Bican	Bican	k1gMnSc1	Bican
-	-	kIx~	-
1937	[number]	k4	1937
<g/>
/	/	kIx~	/
<g/>
38	[number]	k4	38
(	(	kIx(	(
<g/>
22	[number]	k4	22
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1938	[number]	k4	1938
<g/>
/	/	kIx~	/
<g/>
39	[number]	k4	39
(	(	kIx(	(
<g/>
29	[number]	k4	29
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1939	[number]	k4	1939
<g/>
/	/	kIx~	/
<g/>
40	[number]	k4	40
(	(	kIx(	(
<g/>
50	[number]	k4	50
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1940	[number]	k4	1940
<g/>
/	/	kIx~	/
<g/>
41	[number]	k4	41
(	(	kIx(	(
<g/>
38	[number]	k4	38
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1941	[number]	k4	1941
<g/>
/	/	kIx~	/
<g/>
42	[number]	k4	42
(	(	kIx(	(
<g/>
45	[number]	k4	45
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1942	[number]	k4	1942
<g/>
/	/	kIx~	/
<g/>
43	[number]	k4	43
(	(	kIx(	(
<g/>
39	[number]	k4	39
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
1943	[number]	k4	1943
<g/>
/	/	kIx~	/
<g/>
44	[number]	k4	44
(	(	kIx(	(
<g/>
57	[number]	k4	57
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1945	[number]	k4	1945
<g/>
/	/	kIx~	/
<g/>
46	[number]	k4	46
(	(	kIx(	(
<g/>
31	[number]	k4	31
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1946	[number]	k4	1946
<g/>
/	/	kIx~	/
<g/>
47	[number]	k4	47
(	(	kIx(	(
<g/>
43	[number]	k4	43
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1948	[number]	k4	1948
(	(	kIx(	(
<g/>
21	[number]	k4	21
<g/>
)	)	kIx)	)
Ladislav	Ladislav	k1gMnSc1	Ladislav
Hlaváček	Hlaváček	k1gMnSc1	Hlaváček
-	-	kIx~	-
1949	[number]	k4	1949
(	(	kIx(	(
<g/>
28	[number]	k4	28
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Peter	Peter	k1gMnSc1	Peter
Herda	Herda	k1gMnSc1	Herda
-	-	kIx~	-
1981	[number]	k4	1981
<g/>
/	/	kIx~	/
<g/>
82	[number]	k4	82
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
)	)	kIx)	)
Ivo	Ivo	k1gMnSc1	Ivo
Knoflíček	knoflíček	k1gInSc1	knoflíček
-	-	kIx~	-
1984	[number]	k4	1984
<g/>
/	/	kIx~	/
<g/>
85	[number]	k4	85
(	(	kIx(	(
<g/>
21	[number]	k4	21
<g/>
)	)	kIx)	)
Po	po	k7c6	po
podzimní	podzimní	k2eAgFnSc6d1	podzimní
části	část	k1gFnSc6	část
ligy	liga	k1gFnSc2	liga
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2007	[number]	k4	2007
a	a	k8xC	a
2008	[number]	k4	2008
byli	být	k5eAaImAgMnP	být
na	na	k7c6	na
prvním	první	k4xOgNnSc6	první
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
tabulce	tabulka	k1gFnSc6	tabulka
střelců	střelec	k1gMnPc2	střelec
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
Stanislav	Stanislav	k1gMnSc1	Stanislav
Vlček	Vlček	k1gMnSc1	Vlček
se	s	k7c7	s
<g />
.	.	kIx.	.
</s>
<s>
7	[number]	k4	7
góly	gól	k1gInPc4	gól
(	(	kIx(	(
<g/>
7	[number]	k4	7
gólů	gól	k1gInPc2	gól
vstřelil	vstřelit	k5eAaPmAgInS	vstřelit
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
i	i	k8xC	i
mostecký	mostecký	k2eAgInSc4d1	mostecký
Goce	Goce	k1gInSc4	Goce
Toleski	Tolesk	k1gFnSc2	Tolesk
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
pak	pak	k6eAd1	pak
do	do	k7c2	do
Slavie	slavie	k1gFnSc2	slavie
přestoupil	přestoupit	k5eAaPmAgMnS	přestoupit
jako	jako	k9	jako
možná	možná	k9	možná
náhrada	náhrada	k1gFnSc1	náhrada
za	za	k7c4	za
Vlčka	Vlček	k1gMnSc4	Vlček
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
poté	poté	k6eAd1	poté
přestoupil	přestoupit	k5eAaPmAgMnS	přestoupit
do	do	k7c2	do
belgického	belgický	k2eAgInSc2d1	belgický
klubu	klub	k1gInSc2	klub
Anderlecht	Anderlecht	k2eAgInSc4d1	Anderlecht
Brusel	Brusel	k1gInSc4	Brusel
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
Tomáš	Tomáš	k1gMnSc1	Tomáš
Necid	Necid	k1gInSc4	Necid
s	s	k7c7	s
11	[number]	k4	11
góly	gól	k1gInPc7	gól
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
přestoupil	přestoupit	k5eAaPmAgMnS	přestoupit
do	do	k7c2	do
CSKA	CSKA	kA	CSKA
Moskva	Moskva	k1gFnSc1	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
Vlčkových	Vlčkových	k2eAgInPc2d1	Vlčkových
7	[number]	k4	7
gólů	gól	k1gInPc2	gól
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
2008	[number]	k4	2008
pokořil	pokořit	k5eAaPmAgMnS	pokořit
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
později	pozdě	k6eAd2	pozdě
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
střelec	střelec	k1gMnSc1	střelec
ligy	liga	k1gFnSc2	liga
Václav	Václav	k1gMnSc1	Václav
Svěrkoš	Svěrkoš	k1gMnSc1	Svěrkoš
<g/>
,	,	kIx,	,
Necidových	Necidový	k2eAgInPc2d1	Necidový
11	[number]	k4	11
gólů	gól	k1gInPc2	gól
z	z	k7c2	z
podzimních	podzimní	k2eAgInPc2d1	podzimní
bojů	boj	k1gInPc2	boj
překonal	překonat	k5eAaPmAgMnS	překonat
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
liberecký	liberecký	k2eAgMnSc1d1	liberecký
Andrej	Andrej	k1gMnSc1	Andrej
Kerić	Kerić	k1gMnSc1	Kerić
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
dal	dát	k5eAaPmAgMnS	dát
do	do	k7c2	do
konce	konec	k1gInSc2	konec
ročníku	ročník	k1gInSc2	ročník
gólů	gól	k1gInPc2	gól
15	[number]	k4	15
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
října	říjen	k1gInSc2	říjen
1997	[number]	k4	1997
byla	být	k5eAaImAgFnS	být
majoritním	majoritní	k2eAgMnSc7d1	majoritní
vlastníkem	vlastník	k1gMnSc7	vlastník
klubu	klub	k1gInSc2	klub
britská	britský	k2eAgFnSc1d1	britská
investiční	investiční	k2eAgFnSc1d1	investiční
společnost	společnost	k1gFnSc1	společnost
ENIC	ENIC	kA	ENIC
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
původní	původní	k2eAgInSc1d1	původní
54	[number]	k4	54
<g/>
%	%	kIx~	%
akcionářský	akcionářský	k2eAgInSc1d1	akcionářský
podíl	podíl	k1gInSc1	podíl
postupně	postupně	k6eAd1	postupně
stoupl	stoupnout	k5eAaPmAgInS	stoupnout
až	až	k9	až
na	na	k7c4	na
96,7	[number]	k4	96,7
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2007	[number]	k4	2007
generální	generální	k2eAgMnSc1d1	generální
ředitel	ředitel	k1gMnSc1	ředitel
Slavie	slavie	k1gFnSc2	slavie
Petr	Petr	k1gMnSc1	Petr
Doležal	Doležal	k1gMnSc1	Doležal
pro	pro	k7c4	pro
server	server	k1gInSc4	server
Euro	euro	k1gNnSc4	euro
online	onlinout	k5eAaPmIp3nS	onlinout
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
ENIC	ENIC	kA	ENIC
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
ve	v	k7c6	v
Slavii	slavie	k1gFnSc6	slavie
pouze	pouze	k6eAd1	pouze
31	[number]	k4	31
%	%	kIx~	%
podíl	podíl	k1gInSc4	podíl
a	a	k8xC	a
majoritní	majoritní	k2eAgInSc4d1	majoritní
balík	balík	k1gInSc4	balík
61	[number]	k4	61
%	%	kIx~	%
akcií	akcie	k1gFnPc2	akcie
je	být	k5eAaImIp3nS	být
registrován	registrovat	k5eAaBmNgMnS	registrovat
na	na	k7c4	na
společnost	společnost	k1gFnSc4	společnost
Key	Key	k1gFnSc2	Key
Investments	Investments	k1gInSc1	Investments
<g/>
.	.	kIx.	.
31	[number]	k4	31
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2008	[number]	k4	2008
porušila	porušit	k5eAaPmAgFnS	porušit
KEY	KEY	kA	KEY
INVESTMENT	INVESTMENT	kA	INVESTMENT
smlouvu	smlouva	k1gFnSc4	smlouva
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
ENIC	ENIC	kA	ENIC
a	a	k8xC	a
navýšila	navýšit	k5eAaPmAgFnS	navýšit
základní	základní	k2eAgInSc4d1	základní
kapitál	kapitál	k1gInSc4	kapitál
akciové	akciový	k2eAgFnSc2d1	akciová
společnosti	společnost	k1gFnSc2	společnost
SK	Sk	kA	Sk
Slavia	Slavia	k1gFnSc1	Slavia
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
fotbal	fotbal	k1gInSc1	fotbal
na	na	k7c4	na
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvojnásobek	dvojnásobek	k1gInSc1	dvojnásobek
původní	původní	k2eAgFnSc2d1	původní
hodnoty	hodnota	k1gFnSc2	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Předmětem	předmět	k1gInSc7	předmět
vkladu	vklad	k1gInSc2	vklad
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
teoretická	teoretický	k2eAgFnSc1d1	teoretická
suma	suma	k1gFnSc1	suma
maximálního	maximální	k2eAgNnSc2d1	maximální
pojistného	pojistné	k1gNnSc2	pojistné
plnění	plnění	k1gNnSc2	plnění
z	z	k7c2	z
životních	životní	k2eAgFnPc2d1	životní
pojistek	pojistka	k1gFnPc2	pojistka
fotbalistů	fotbalista	k1gMnPc2	fotbalista
Slavie	slavie	k1gFnSc2	slavie
<g/>
.	.	kIx.	.
</s>
<s>
Znalec	znalec	k1gMnSc1	znalec
Otto	Otto	k1gMnSc1	Otto
Šmída	Šmíd	k1gMnSc2	Šmíd
od	od	k7c2	od
maximálního	maximální	k2eAgNnSc2d1	maximální
pojistného	pojistné	k1gNnSc2	pojistné
plnění	plnění	k1gNnSc2	plnění
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
294	[number]	k4	294
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
odečetl	odečíst	k5eAaPmAgInS	odečíst
pojistné	pojistné	k1gNnSc4	pojistné
5	[number]	k4	5
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
a	a	k8xC	a
po	po	k7c6	po
vynásobení	vynásobení	k1gNnSc6	vynásobení
koeficientem	koeficient	k1gInSc7	koeficient
1,164	[number]	k4	1,164
<g/>
25	[number]	k4	25
dospěl	dochvít	k5eAaPmAgMnS	dochvít
k	k	k7c3	k
částce	částka	k1gFnSc3	částka
618	[number]	k4	618
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2008	[number]	k4	2008
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
sedm	sedm	k4xCc1	sedm
hromadných	hromadný	k2eAgFnPc2d1	hromadná
akcií	akcie	k1gFnPc2	akcie
o	o	k7c6	o
celkové	celkový	k2eAgFnSc6d1	celková
nominální	nominální	k2eAgFnSc6d1	nominální
hodnotě	hodnota	k1gFnSc6	hodnota
618	[number]	k4	618
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
znějících	znějící	k2eAgInPc2d1	znějící
na	na	k7c4	na
jméno	jméno	k1gNnSc4	jméno
Dartmoor	Dartmoor	k1gInSc4	Dartmoor
Association	Association	k1gInSc1	Association
Ltd	ltd	kA	ltd
<g/>
.	.	kIx.	.
sídlící	sídlící	k2eAgFnSc1d1	sídlící
na	na	k7c6	na
karibském	karibský	k2eAgInSc6d1	karibský
ostrově	ostrov	k1gInSc6	ostrov
Nevis	viset	k5eNaImRp2nS	viset
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
března	březen	k1gInSc2	březen
2008	[number]	k4	2008
Slavia	Slavia	k1gFnSc1	Slavia
bez	bez	k7c2	bez
dalších	další	k2eAgFnPc2d1	další
podrobností	podrobnost	k1gFnPc2	podrobnost
oznámila	oznámit	k5eAaPmAgFnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
od	od	k7c2	od
května	květen	k1gInSc2	květen
2006	[number]	k4	2006
jsou	být	k5eAaImIp3nP	být
většinovými	většinový	k2eAgMnPc7d1	většinový
vlastníky	vlastník	k1gMnPc7	vlastník
fotbalové	fotbalový	k2eAgFnSc2d1	fotbalová
Slavie	slavie	k1gFnSc2	slavie
Tomáš	Tomáš	k1gMnSc1	Tomáš
Rosen	rosen	k2eAgMnSc1d1	rosen
a	a	k8xC	a
Petr	Petr	k1gMnSc1	Petr
Doležal	Doležal	k1gMnSc1	Doležal
<g/>
.	.	kIx.	.
23	[number]	k4	23
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2009	[number]	k4	2009
Dartmoor	Dartmoor	k1gInSc1	Dartmoor
Association	Association	k1gInSc4	Association
akcie	akcie	k1gFnSc2	akcie
prodala	prodat	k5eAaPmAgFnS	prodat
společnosti	společnost	k1gFnPc4	společnost
Falkonida	Falkonid	k1gMnSc2	Falkonid
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
za	za	k7c4	za
ně	on	k3xPp3gInPc4	on
nezaplatila	zaplatit	k5eNaPmAgFnS	zaplatit
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
převedla	převést	k5eAaPmAgFnS	převést
na	na	k7c4	na
společnost	společnost	k1gFnSc4	společnost
EGIDA	egida	k1gFnSc1	egida
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	on	k3xPp3gNnSc4	on
převedla	převést	k5eAaPmAgFnS	převést
na	na	k7c4	na
Antonína	Antonín	k1gMnSc4	Antonín
France	Franc	k1gMnSc4	Franc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
došlo	dojít	k5eAaPmAgNnS	dojít
ve	v	k7c4	v
Slavii	slavie	k1gFnSc4	slavie
k	k	k7c3	k
rozporům	rozpor	k1gInPc3	rozpor
<g/>
.	.	kIx.	.
</s>
<s>
ENIC	ENIC	kA	ENIC
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
vymoci	vymoct	k5eAaPmF	vymoct
více	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
<g/>
milionový	milionový	k2eAgInSc4d1	milionový
dluh	dluh	k1gInSc4	dluh
na	na	k7c6	na
Slavii	slavie	k1gFnSc6	slavie
<g/>
,	,	kIx,	,
nadále	nadále	k6eAd1	nadále
se	se	k3xPyFc4	se
hlásil	hlásit	k5eAaImAgMnS	hlásit
jako	jako	k9	jako
majoritní	majoritní	k2eAgMnSc1d1	majoritní
vlastník	vlastník	k1gMnSc1	vlastník
a	a	k8xC	a
snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
domoci	domoct	k5eAaPmF	domoct
svých	svůj	k3xOyFgNnPc2	svůj
práv	právo	k1gNnPc2	právo
soudní	soudní	k2eAgFnSc7d1	soudní
cestou	cesta	k1gFnSc7	cesta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
téže	týž	k3xTgFnSc6	týž
době	doba	k1gFnSc6	doba
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
že	že	k8xS	že
údajným	údajný	k2eAgMnSc7d1	údajný
pravým	pravý	k2eAgMnSc7d1	pravý
majitelem	majitel	k1gMnSc7	majitel
většinového	většinový	k2eAgInSc2d1	většinový
podílu	podíl	k1gInSc2	podíl
byl	být	k5eAaImAgMnS	být
podnikatel	podnikatel	k1gMnSc1	podnikatel
Petr	Petr	k1gMnSc1	Petr
Sisák	Sisák	k1gMnSc1	Sisák
<g/>
,	,	kIx,	,
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
odsouzený	odsouzený	k1gMnSc1	odsouzený
za	za	k7c2	za
tunelování	tunelování	k1gNnSc2	tunelování
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
řídit	řídit	k5eAaImF	řídit
Slavii	slavie	k1gFnSc4	slavie
přes	přes	k7c4	přes
Antonína	Antonín	k1gMnSc4	Antonín
France	Franc	k1gMnSc4	Franc
<g/>
.	.	kIx.	.
</s>
<s>
Slavia	Slavia	k1gFnSc1	Slavia
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
dluhové	dluhový	k2eAgFnSc2d1	dluhová
pasti	past	k1gFnSc2	past
a	a	k8xC	a
klubu	klub	k1gInSc2	klub
hrozilo	hrozit	k5eAaImAgNnS	hrozit
vyřazení	vyřazení	k1gNnSc1	vyřazení
z	z	k7c2	z
ligy	liga	k1gFnSc2	liga
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
nejasných	jasný	k2eNgFnPc2d1	nejasná
okolností	okolnost	k1gFnPc2	okolnost
byl	být	k5eAaImAgInS	být
většinový	většinový	k2eAgInSc1d1	většinový
podíl	podíl	k1gInSc1	podíl
přenesen	přenést	k5eAaPmNgInS	přenést
na	na	k7c4	na
firmu	firma	k1gFnSc4	firma
Natland	Natlanda	k1gFnPc2	Natlanda
a	a	k8xC	a
Tomáš	Tomáš	k1gMnSc1	Tomáš
Rosen	rosen	k2eAgMnSc1d1	rosen
pak	pak	k6eAd1	pak
svůj	svůj	k3xOyFgInSc4	svůj
podíl	podíl	k1gInSc4	podíl
prodal	prodat	k5eAaPmAgMnS	prodat
Aleši	Aleš	k1gMnSc3	Aleš
Řebíčkovi	Řebíček	k1gMnSc3	Řebíček
<g/>
.	.	kIx.	.
</s>
<s>
Noví	nový	k2eAgMnPc1d1	nový
vlastníci	vlastník	k1gMnPc1	vlastník
se	se	k3xPyFc4	se
vyrovnávali	vyrovnávat	k5eAaImAgMnP	vyrovnávat
s	s	k7c7	s
firmou	firma	k1gFnSc7	firma
Enic	Enic	k1gFnSc1	Enic
a	a	k8xC	a
následně	následně	k6eAd1	následně
pak	pak	k6eAd1	pak
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
odprodeji	odprodej	k1gInSc3	odprodej
akcií	akcie	k1gFnPc2	akcie
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
vlastnil	vlastnit	k5eAaImAgInS	vlastnit
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
Natland	Natlanda	k1gFnPc2	Natlanda
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
srpna	srpen	k1gInSc2	srpen
2011	[number]	k4	2011
pak	pak	k8xC	pak
Aleš	Aleš	k1gMnSc1	Aleš
Řebíček	řebíček	k1gInSc4	řebíček
vlastnil	vlastnit	k5eAaImAgMnS	vlastnit
98	[number]	k4	98
%	%	kIx~	%
akcií	akcie	k1gFnPc2	akcie
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2015	[number]	k4	2015
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
vlastníka	vlastník	k1gMnSc2	vlastník
akcií	akcie	k1gFnPc2	akcie
fotbalového	fotbalový	k2eAgInSc2d1	fotbalový
klubu	klub	k1gInSc2	klub
<g/>
.	.	kIx.	.
59.97	[number]	k4	59.97
%	%	kIx~	%
akcií	akcie	k1gFnPc2	akcie
od	od	k7c2	od
Aleše	Aleš	k1gMnSc2	Aleš
Řebíčka	Řebíček	k1gMnSc2	Řebíček
získala	získat	k5eAaPmAgFnS	získat
významná	významný	k2eAgFnSc1d1	významná
čínská	čínský	k2eAgFnSc1d1	čínská
energetická	energetický	k2eAgFnSc1d1	energetická
společnost	společnost	k1gFnSc1	společnost
CEFC	CEFC	kA	CEFC
China	China	k1gFnSc1	China
Energy	Energ	k1gInPc4	Energ
Company	Compana	k1gFnSc2	Compana
Limited	limited	k2eAgFnSc2d1	limited
a	a	k8xC	a
39,97	[number]	k4	39,97
%	%	kIx~	%
akcií	akcie	k1gFnPc2	akcie
převzal	převzít	k5eAaPmAgMnS	převzít
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
majitel	majitel	k1gMnSc1	majitel
letecké	letecký	k2eAgFnSc2d1	letecká
společnosti	společnost	k1gFnSc2	společnost
Travel	Travel	k1gMnSc1	Travel
Service	Service	k1gFnSc2	Service
Jiří	Jiří	k1gMnSc1	Jiří
Šimáně	Šimán	k1gInSc6	Šimán
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
své	svůj	k3xOyFgFnSc2	svůj
společnosti	společnost	k1gFnSc2	společnost
Fly	Fly	k1gMnPc2	Fly
Sport	sport	k1gInSc1	sport
Investments	Investmentsa	k1gFnPc2	Investmentsa
<g/>
.	.	kIx.	.
</s>
<s>
Zbývající	zbývající	k2eAgFnPc1d1	zbývající
akcie	akcie	k1gFnPc1	akcie
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
minoritních	minoritní	k2eAgMnPc2d1	minoritní
podílníků	podílník	k1gMnPc2	podílník
<g/>
.	.	kIx.	.
</s>
<s>
Příchodem	příchod	k1gInSc7	příchod
nových	nový	k2eAgMnPc2d1	nový
majitelů	majitel	k1gMnPc2	majitel
a	a	k8xC	a
splacením	splacení	k1gNnSc7	splacení
dluhů	dluh	k1gInPc2	dluh
skončilo	skončit	k5eAaPmAgNnS	skončit
insolvenční	insolvenční	k2eAgNnSc1d1	insolvenční
řízení	řízení	k1gNnSc1	řízení
s	s	k7c7	s
klubem	klub	k1gInSc7	klub
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2016	[number]	k4	2016
odkoupila	odkoupit	k5eAaPmAgFnS	odkoupit
CEFC	CEFC	kA	CEFC
zbývající	zbývající	k2eAgMnSc1d1	zbývající
Šimáněho	Šimáně	k1gMnSc4	Šimáně
akcie	akcie	k1gFnSc2	akcie
<g/>
,	,	kIx,	,
a	a	k8xC	a
navýšila	navýšit	k5eAaPmAgFnS	navýšit
svůj	svůj	k3xOyFgInSc4	svůj
majetkový	majetkový	k2eAgInSc4d1	majetkový
podíl	podíl	k1gInSc4	podíl
na	na	k7c4	na
99,96	[number]	k4	99,96
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Informace	informace	k1gFnPc1	informace
o	o	k7c6	o
změnách	změna	k1gFnPc6	změna
v	v	k7c6	v
kádru	kádr	k1gInSc6	kádr
a	a	k8xC	a
mnohé	mnohé	k1gNnSc1	mnohé
další	další	k2eAgFnSc1d1	další
z	z	k7c2	z
právě	právě	k6eAd1	právě
probíhající	probíhající	k2eAgFnSc2d1	probíhající
sezony	sezona	k1gFnSc2	sezona
2016	[number]	k4	2016
<g/>
/	/	kIx~	/
<g/>
17	[number]	k4	17
naleznete	naleznout	k5eAaPmIp2nP	naleznout
na	na	k7c6	na
stránce	stránka	k1gFnSc6	stránka
SK	Sk	kA	Sk
Slavia	Slavia	k1gFnSc1	Slavia
Praha	Praha	k1gFnSc1	Praha
2016	[number]	k4	2016
<g/>
/	/	kIx~	/
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
</s>
<s>
Informace	informace	k1gFnPc1	informace
o	o	k7c6	o
Juniorském	juniorský	k2eAgInSc6d1	juniorský
týmu	tým	k1gInSc6	tým
klubu	klub	k1gInSc2	klub
SK	Sk	kA	Sk
Slavia	Slavia	k1gFnSc1	Slavia
Praha	Praha	k1gFnSc1	Praha
naleznete	nalézt	k5eAaBmIp2nP	nalézt
na	na	k7c6	na
SK	Sk	kA	Sk
Slavia	Slavia	k1gFnSc1	Slavia
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
Juniorský	juniorský	k2eAgInSc1d1	juniorský
tým	tým	k1gInSc1	tým
Legenda	legenda	k1gFnSc1	legenda
<g/>
:	:	kIx,	:
Z	Z	kA	Z
-	-	kIx~	-
zápasy	zápas	k1gInPc1	zápas
<g/>
,	,	kIx,	,
V	v	k7c6	v
-	-	kIx~	-
výhry	výhra	k1gFnPc4	výhra
<g/>
,	,	kIx,	,
R	R	kA	R
-	-	kIx~	-
remízy	remíz	k1gInPc1	remíz
<g/>
,	,	kIx,	,
P	P	kA	P
-	-	kIx~	-
porážky	porážka	k1gFnSc2	porážka
<g/>
,	,	kIx,	,
VG	VG	kA	VG
-	-	kIx~	-
vstřelené	vstřelený	k2eAgInPc1d1	vstřelený
góly	gól	k1gInPc1	gól
<g/>
,	,	kIx,	,
OG	OG	kA	OG
-	-	kIx~	-
obdržené	obdržený	k2eAgInPc1d1	obdržený
góly	gól	k1gInPc1	gól
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
+	+	kIx~	+
<g/>
/	/	kIx~	/
<g/>
-	-	kIx~	-
-	-	kIx~	-
rozdíl	rozdíl	k1gInSc1	rozdíl
skóre	skóre	k1gNnSc2	skóre
<g/>
,	,	kIx,	,
B	B	kA	B
-	-	kIx~	-
body	bod	k1gInPc1	bod
<g/>
,	,	kIx,	,
červené	červený	k2eAgNnSc1d1	červené
podbarvení	podbarvení	k1gNnSc1	podbarvení
-	-	kIx~	-
sestup	sestup	k1gInSc1	sestup
<g/>
,	,	kIx,	,
zelené	zelený	k2eAgNnSc1d1	zelené
podbarvení	podbarvení	k1gNnSc1	podbarvení
-	-	kIx~	-
postup	postup	k1gInSc1	postup
<g/>
,	,	kIx,	,
světle	světle	k6eAd1	světle
fialové	fialový	k2eAgNnSc1d1	fialové
podbarvení	podbarvení	k1gNnSc1	podbarvení
-	-	kIx~	-
přesun	přesun	k1gInSc1	přesun
do	do	k7c2	do
jiné	jiný	k2eAgFnSc2d1	jiná
soutěže	soutěž	k1gFnSc2	soutěž
Poznámky	poznámka	k1gFnSc2	poznámka
<g/>
:	:	kIx,	:
1945	[number]	k4	1945
<g/>
/	/	kIx~	/
<g/>
46	[number]	k4	46
<g/>
:	:	kIx,	:
Slavia	Slavia	k1gFnSc1	Slavia
(	(	kIx(	(
<g/>
vítěz	vítěz	k1gMnSc1	vítěz
sk	sk	k?	sk
<g/>
.	.	kIx.	.
</s>
<s>
B	B	kA	B
<g/>
)	)	kIx)	)
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
ligového	ligový	k2eAgInSc2d1	ligový
ročníku	ročník	k1gInSc2	ročník
prohrála	prohrát	k5eAaPmAgFnS	prohrát
celkově	celkově	k6eAd1	celkově
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
9	[number]	k4	9
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
z	z	k7c2	z
-	-	kIx~	-
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
z	z	k7c2	z
-	-	kIx~	-
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
s	s	k7c7	s
pražskou	pražský	k2eAgFnSc7d1	Pražská
Spartou	Sparta	k1gFnSc7	Sparta
(	(	kIx(	(
<g/>
vítěz	vítěz	k1gMnSc1	vítěz
sk	sk	k?	sk
<g/>
.	.	kIx.	.
</s>
<s>
A	A	kA	A
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Výsledky	výsledek	k1gInPc4	výsledek
Slavie	slavie	k1gFnSc1	slavie
Praha	Praha	k1gFnSc1	Praha
v	v	k7c6	v
evropských	evropský	k2eAgInPc6d1	evropský
fotbalových	fotbalový	k2eAgInPc6d1	fotbalový
pohárech	pohár	k1gInPc6	pohár
<g/>
.	.	kIx.	.
1996	[number]	k4	1996
<g/>
/	/	kIx~	/
<g/>
97	[number]	k4	97
předkolo	předkolo	k1gNnSc1	předkolo
(	(	kIx(	(
<g/>
Grasshopper	Grasshopper	k1gInSc1	Grasshopper
Club	club	k1gInSc1	club
Zürich	Zürich	k1gInSc1	Zürich
<g/>
)	)	kIx)	)
2000	[number]	k4	2000
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
3	[number]	k4	3
<g/>
.	.	kIx.	.
předkolo	předkolo	k1gNnSc1	předkolo
(	(	kIx(	(
<g/>
FK	FK	kA	FK
Šachtar	Šachtar	k1gInSc1	Šachtar
Doněck	Doněck	k1gInSc1	Doněck
<g/>
)	)	kIx)	)
2001	[number]	k4	2001
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g />
.	.	kIx.	.
</s>
<s>
<g/>
2	[number]	k4	2
3	[number]	k4	3
<g/>
.	.	kIx.	.
předkolo	předkolo	k1gNnSc1	předkolo
(	(	kIx(	(
<g/>
Panathinaikos	Panathinaikos	k1gInSc1	Panathinaikos
FC	FC	kA	FC
<g/>
)	)	kIx)	)
2003	[number]	k4	2003
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
4	[number]	k4	4
3	[number]	k4	3
<g/>
.	.	kIx.	.
předkolo	předkolo	k1gNnSc1	předkolo
(	(	kIx(	(
<g/>
Celta	celta	k1gFnSc1	celta
de	de	k?	de
Vigo	Vigo	k1gMnSc1	Vigo
<g/>
)	)	kIx)	)
2005	[number]	k4	2005
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
3	[number]	k4	3
<g/>
.	.	kIx.	.
předkolo	předkolo	k1gNnSc1	předkolo
(	(	kIx(	(
<g/>
RSC	RSC	kA	RSC
Anderlecht	Anderlecht	k1gMnSc1	Anderlecht
<g/>
)	)	kIx)	)
2007	[number]	k4	2007
<g/>
/	/	kIx~	/
<g />
.	.	kIx.	.
</s>
<s>
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
Základní	základní	k2eAgFnSc1d1	základní
skupina	skupina	k1gFnSc1	skupina
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
za	za	k7c7	za
Sevilla	Sevilla	k1gFnSc1	Sevilla
FC	FC	kA	FC
a	a	k8xC	a
Arsenal	Arsenal	k1gMnSc1	Arsenal
FC	FC	kA	FC
<g/>
)	)	kIx)	)
2008	[number]	k4	2008
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
3	[number]	k4	3
<g/>
.	.	kIx.	.
předkolo	předkolo	k1gNnSc1	předkolo
(	(	kIx(	(
<g/>
ACF	ACF	kA	ACF
Fiorentina	Fiorentina	k1gFnSc1	Fiorentina
<g/>
)	)	kIx)	)
2009	[number]	k4	2009
<g/>
/	/	kIx~	/
<g/>
10	[number]	k4	10
3	[number]	k4	3
<g/>
.	.	kIx.	.
předkolo	předkolo	k1gNnSc1	předkolo
(	(	kIx(	(
<g/>
FC	FC	kA	FC
Sheriff	Sheriff	k1gMnSc1	Sheriff
<g />
.	.	kIx.	.
</s>
<s>
Tiraspol	Tiraspol	k1gInSc1	Tiraspol
<g/>
)	)	kIx)	)
1976	[number]	k4	1976
<g/>
/	/	kIx~	/
<g/>
77	[number]	k4	77
1	[number]	k4	1
<g/>
.	.	kIx.	.
kolo	kolo	k1gNnSc1	kolo
(	(	kIx(	(
<g/>
Akademik	akademik	k1gMnSc1	akademik
Sofia	Sofia	k1gFnSc1	Sofia
<g/>
)	)	kIx)	)
1977	[number]	k4	1977
<g/>
/	/	kIx~	/
<g/>
78	[number]	k4	78
1	[number]	k4	1
<g/>
.	.	kIx.	.
kolo	kolo	k1gNnSc1	kolo
(	(	kIx(	(
<g/>
Standard	standard	k1gInSc1	standard
Liè	Liè	k1gFnSc2	Liè
<g/>
)	)	kIx)	)
1985	[number]	k4	1985
<g/>
/	/	kIx~	/
<g/>
86	[number]	k4	86
1	[number]	k4	1
<g/>
.	.	kIx.	.
kolo	kolo	k1gNnSc1	kolo
(	(	kIx(	(
<g/>
St.	st.	kA	st.
Mirren	Mirrna	k1gFnPc2	Mirrna
FC	FC	kA	FC
<g/>
)	)	kIx)	)
1992	[number]	k4	1992
<g />
.	.	kIx.	.
</s>
<s>
<g/>
/	/	kIx~	/
<g/>
93	[number]	k4	93
1	[number]	k4	1
<g/>
.	.	kIx.	.
kolo	kolo	k1gNnSc1	kolo
(	(	kIx(	(
<g/>
Heart	Heart	k1gInSc1	Heart
of	of	k?	of
Midlothian	Midlothian	k1gInSc1	Midlothian
FC	FC	kA	FC
<g/>
)	)	kIx)	)
1993	[number]	k4	1993
<g/>
/	/	kIx~	/
<g/>
94	[number]	k4	94
1	[number]	k4	1
<g/>
.	.	kIx.	.
kolo	kolo	k1gNnSc1	kolo
(	(	kIx(	(
<g/>
OFI	OFI	kA	OFI
Kréta	Kréta	k1gFnSc1	Kréta
<g/>
)	)	kIx)	)
1994	[number]	k4	1994
<g/>
/	/	kIx~	/
<g/>
95	[number]	k4	95
1	[number]	k4	1
<g/>
.	.	kIx.	.
kolo	kolo	k1gNnSc1	kolo
(	(	kIx(	(
<g/>
AIK	AIK	kA	AIK
Stockholm	Stockholm	k1gInSc1	Stockholm
<g/>
)	)	kIx)	)
1995	[number]	k4	1995
<g/>
/	/	kIx~	/
<g/>
<g />
.	.	kIx.	.
</s>
<s>
96	[number]	k4	96
semifinále	semifinále	k1gNnSc1	semifinále
(	(	kIx(	(
<g/>
FC	FC	kA	FC
Girondins	Girondins	k1gInSc1	Girondins
de	de	k?	de
Bordeaux	Bordeaux	k1gNnSc1	Bordeaux
<g/>
)	)	kIx)	)
1996	[number]	k4	1996
<g/>
/	/	kIx~	/
<g/>
97	[number]	k4	97
2	[number]	k4	2
<g/>
.	.	kIx.	.
kolo	kolo	k1gNnSc1	kolo
(	(	kIx(	(
<g/>
Valencia	Valencia	k1gFnSc1	Valencia
CF	CF	kA	CF
<g/>
)	)	kIx)	)
1998	[number]	k4	1998
<g/>
/	/	kIx~	/
<g/>
99	[number]	k4	99
2	[number]	k4	2
<g/>
.	.	kIx.	.
kolo	kolo	k1gNnSc1	kolo
(	(	kIx(	(
<g/>
Bologna	Bologna	k1gFnSc1	Bologna
FC	FC	kA	FC
1909	[number]	k4	1909
<g/>
)	)	kIx)	)
1999	[number]	k4	1999
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
čtvrtfinále	čtvrtfinále	k1gNnPc2	čtvrtfinále
(	(	kIx(	(
<g/>
Leeds	Leeds	k1gInSc1	Leeds
United	United	k1gInSc1	United
FC	FC	kA	FC
<g/>
)	)	kIx)	)
2000	[number]	k4	2000
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
4	[number]	k4	4
<g/>
.	.	kIx.	.
kolo	kolo	k1gNnSc1	kolo
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
FC	FC	kA	FC
Kaiserslautern	Kaiserslautern	k1gMnSc1	Kaiserslautern
<g/>
)	)	kIx)	)
2001	[number]	k4	2001
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
1	[number]	k4	1
<g/>
.	.	kIx.	.
kolo	kolo	k1gNnSc1	kolo
(	(	kIx(	(
<g/>
Servette	Servett	k1gMnSc5	Servett
FC	FC	kA	FC
<g/>
)	)	kIx)	)
2002	[number]	k4	2002
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
4	[number]	k4	4
<g/>
.	.	kIx.	.
kolo	kolo	k1gNnSc1	kolo
(	(	kIx(	(
<g/>
Beşiktaş	Beşiktaş	k1gFnSc1	Beşiktaş
JK	JK	kA	JK
<g/>
)	)	kIx)	)
2003	[number]	k4	2003
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
4	[number]	k4	4
2	[number]	k4	2
<g/>
.	.	kIx.	.
kolo	kolo	k1gNnSc1	kolo
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
PFK	PFK	kA	PFK
Levski	Levski	k1gNnSc1	Levski
Sofia	Sofia	k1gFnSc1	Sofia
<g/>
)	)	kIx)	)
2004	[number]	k4	2004
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
2	[number]	k4	2
<g/>
.	.	kIx.	.
předkolo	předkolo	k1gNnSc1	předkolo
(	(	kIx(	(
<g/>
FC	FC	kA	FC
Dinamo	Dinama	k1gFnSc5	Dinama
Tbilisi	Tbilisi	k1gNnSc6	Tbilisi
<g/>
)	)	kIx)	)
2005	[number]	k4	2005
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
3	[number]	k4	3
<g/>
.	.	kIx.	.
kolo	kolo	k1gNnSc1	kolo
(	(	kIx(	(
<g/>
US	US	kA	US
Città	Città	k1gMnSc1	Città
di	di	k?	di
Palermo	Palermo	k1gNnSc4	Palermo
<g/>
)	)	kIx)	)
2006	[number]	k4	2006
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g />
.	.	kIx.	.
</s>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
kolo	kolo	k1gNnSc1	kolo
(	(	kIx(	(
<g/>
Tottenham	Tottenham	k1gInSc1	Tottenham
Hotspur	Hotspura	k1gFnPc2	Hotspura
FC	FC	kA	FC
<g/>
)	)	kIx)	)
2007	[number]	k4	2007
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
3	[number]	k4	3
<g/>
.	.	kIx.	.
kolo	kolo	k1gNnSc1	kolo
(	(	kIx(	(
<g/>
Tottenham	Tottenham	k1gInSc1	Tottenham
Hotspur	Hotspura	k1gFnPc2	Hotspura
FC	FC	kA	FC
<g/>
)	)	kIx)	)
2008	[number]	k4	2008
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
5	[number]	k4	5
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
skupině	skupina	k1gFnSc6	skupina
EL	Ela	k1gFnPc2	Ela
(	(	kIx(	(
<g/>
za	za	k7c4	za
Hamburger	hamburger	k1gInSc4	hamburger
SV	sv	kA	sv
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
AFC	AFC	kA	AFC
Ajax	Ajax	k1gInSc1	Ajax
<g/>
,	,	kIx,	,
Aston	Aston	k1gMnSc1	Aston
Villa	Villo	k1gNnSc2	Villo
FC	FC	kA	FC
<g/>
,	,	kIx,	,
MŠK	MŠK	kA	MŠK
Žilina	Žilina	k1gFnSc1	Žilina
<g/>
)	)	kIx)	)
2009	[number]	k4	2009
<g/>
/	/	kIx~	/
<g/>
10	[number]	k4	10
4	[number]	k4	4
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
skupině	skupina	k1gFnSc6	skupina
EL	Ela	k1gFnPc2	Ela
(	(	kIx(	(
<g/>
za	za	k7c2	za
Valencia	Valencium	k1gNnSc2	Valencium
CF	CF	kA	CF
<g/>
,	,	kIx,	,
Lille	Lille	k1gNnSc1	Lille
OSC	OSC	kA	OSC
<g/>
,	,	kIx,	,
Janov	Janov	k1gInSc1	Janov
CFC	CFC	kA	CFC
<g/>
)	)	kIx)	)
1967	[number]	k4	1967
<g/>
/	/	kIx~	/
<g/>
68	[number]	k4	68
1	[number]	k4	1
<g/>
.	.	kIx.	.
kolo	kolo	k1gNnSc1	kolo
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
FC	FC	kA	FC
Köln	Köln	k1gMnSc1	Köln
<g/>
)	)	kIx)	)
1968	[number]	k4	1968
<g/>
/	/	kIx~	/
<g/>
69	[number]	k4	69
2	[number]	k4	2
<g/>
.	.	kIx.	.
kolo	kolo	k1gNnSc1	kolo
(	(	kIx(	(
<g/>
Hamburger	hamburger	k1gInSc1	hamburger
SV	sv	kA	sv
<g/>
)	)	kIx)	)
1974	[number]	k4	1974
<g/>
/	/	kIx~	/
<g/>
75	[number]	k4	75
1	[number]	k4	1
<g/>
.	.	kIx.	.
kolo	kolo	k1gNnSc1	kolo
(	(	kIx(	(
<g/>
FC	FC	kA	FC
Carl	Carl	k1gInSc1	Carl
Zeiss	Zeiss	k1gInSc1	Zeiss
Jena	Jena	k1gFnSc1	Jena
<g/>
)	)	kIx)	)
1997	[number]	k4	1997
<g/>
/	/	kIx~	/
<g/>
98	[number]	k4	98
čtvrtfinále	čtvrtfinále	k1gNnSc2	čtvrtfinále
(	(	kIx(	(
<g/>
prohra	prohra	k1gFnSc1	prohra
s	s	k7c7	s
VfB	VfB	k1gFnPc2	VfB
Stuttgart	Stuttgart	k1gInSc1	Stuttgart
<g/>
)	)	kIx)	)
Podle	podle	k7c2	podle
knihy	kniha	k1gFnSc2	kniha
Věčná	věčný	k2eAgFnSc1d1	věčná
Slavia	Slavia	k1gFnSc1	Slavia
<g/>
:	:	kIx,	:
Zdroj	zdroj	k1gInSc1	zdroj
<g/>
:	:	kIx,	:
slavia	slavia	k1gFnSc1	slavia
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Zde	zde	k6eAd1	zde
jsou	být	k5eAaImIp3nP	být
uvedeni	uveden	k2eAgMnPc1d1	uveden
trenéři	trenér	k1gMnPc1	trenér
SK	Sk	kA	Sk
Slavia	Slavia	k1gFnSc1	Slavia
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
během	během	k7c2	během
svého	svůj	k3xOyFgNnSc2	svůj
působení	působení	k1gNnSc2	působení
vyhráli	vyhrát	k5eAaPmAgMnP	vyhrát
alespoň	alespoň	k9	alespoň
jednu	jeden	k4xCgFnSc4	jeden
trofej	trofej	k1gFnSc4	trofej
<g/>
.	.	kIx.	.
</s>
