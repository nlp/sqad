<s>
Transliterace	transliterace	k1gFnSc1	transliterace
je	být	k5eAaImIp3nS	být
věrný	věrný	k2eAgInSc4d1	věrný
přepis	přepis	k1gInSc4	přepis
psaného	psaný	k2eAgNnSc2d1	psané
slova	slovo	k1gNnSc2	slovo
(	(	kIx(	(
<g/>
textu	text	k1gInSc2	text
<g/>
,	,	kIx,	,
typicky	typicky	k6eAd1	typicky
zeměpisného	zeměpisný	k2eAgInSc2d1	zeměpisný
názvu	název	k1gInSc2	název
nebo	nebo	k8xC	nebo
jména	jméno	k1gNnSc2	jméno
osoby	osoba	k1gFnSc2	osoba
<g/>
)	)	kIx)	)
z	z	k7c2	z
jednoho	jeden	k4xCgNnSc2	jeden
písma	písmo	k1gNnSc2	písmo
do	do	k7c2	do
jiného	jiný	k2eAgNnSc2d1	jiné
<g/>
.	.	kIx.	.
</s>
<s>
Význam	význam	k1gInSc1	význam
má	mít	k5eAaImIp3nS	mít
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
odborné	odborný	k2eAgInPc4d1	odborný
lingvistické	lingvistický	k2eAgInPc4d1	lingvistický
účely	účel	k1gInPc4	účel
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gInSc7	její
cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
usnadnit	usnadnit	k5eAaPmF	usnadnit
přečtení	přečtení	k1gNnSc4	přečtení
daného	daný	k2eAgInSc2d1	daný
výrazu	výraz	k1gInSc2	výraz
poučenému	poučený	k2eAgMnSc3d1	poučený
čtenáři	čtenář	k1gMnSc3	čtenář
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
zdrojové	zdrojový	k2eAgNnSc1d1	zdrojové
písmo	písmo	k1gNnSc1	písmo
neovládá	ovládat	k5eNaImIp3nS	ovládat
<g/>
;	;	kIx,	;
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
transkripce	transkripce	k1gFnSc2	transkripce
se	se	k3xPyFc4	se
však	však	k9	však
snaží	snažit	k5eAaImIp3nS	snažit
neztratit	ztratit	k5eNaPmF	ztratit
žádnou	žádný	k3yNgFnSc4	žádný
informaci	informace	k1gFnSc4	informace
o	o	k7c6	o
původním	původní	k2eAgInSc6d1	původní
pravopisu	pravopis	k1gInSc6	pravopis
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
tedy	tedy	k9	tedy
zápisem	zápis	k1gInSc7	zápis
mluvené	mluvený	k2eAgFnSc2d1	mluvená
řeči	řeč	k1gFnSc2	řeč
jiným	jiný	k1gMnSc7	jiný
písmem	písmo	k1gNnSc7	písmo
jako	jako	k8xC	jako
transkripce	transkripce	k1gFnSc1	transkripce
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
přepisem	přepis	k1gInSc7	přepis
grafických	grafický	k2eAgInPc2d1	grafický
znaků	znak	k1gInPc2	znak
jinými	jiný	k2eAgInPc7d1	jiný
znaky	znak	k1gInPc7	znak
(	(	kIx(	(
<g/>
písmeny	písmeno	k1gNnPc7	písmeno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Transliterace	transliterace	k1gFnSc1	transliterace
tedy	tedy	k9	tedy
musí	muset	k5eAaImIp3nS	muset
zachytit	zachytit	k5eAaPmF	zachytit
i	i	k9	i
rozdíly	rozdíl	k1gInPc4	rozdíl
mezi	mezi	k7c7	mezi
znaky	znak	k1gInPc7	znak
či	či	k8xC	či
skupinami	skupina	k1gFnPc7	skupina
znaků	znak	k1gInPc2	znak
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
vyslovují	vyslovovat	k5eAaImIp3nP	vyslovovat
stejně	stejně	k6eAd1	stejně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
zdrojovém	zdrojový	k2eAgInSc6d1	zdrojový
jazyce	jazyk	k1gInSc6	jazyk
se	se	k3xPyFc4	se
z	z	k7c2	z
nějakých	nějaký	k3yIgInPc2	nějaký
důvodů	důvod	k1gInPc2	důvod
píší	psát	k5eAaImIp3nP	psát
odlišně	odlišně	k6eAd1	odlišně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mezích	mez	k1gFnPc6	mez
tohoto	tento	k3xDgInSc2	tento
principu	princip	k1gInSc2	princip
se	se	k3xPyFc4	se
transliterace	transliterace	k1gFnSc1	transliterace
nicméně	nicméně	k8xC	nicméně
snaží	snažit	k5eAaImIp3nS	snažit
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
transkripce	transkripce	k1gFnSc1	transkripce
<g/>
)	)	kIx)	)
o	o	k7c6	o
zachycení	zachycení	k1gNnSc6	zachycení
výslovnosti	výslovnost	k1gFnSc2	výslovnost
slova	slovo	k1gNnSc2	slovo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
obdobně	obdobně	k6eAd1	obdobně
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
v	v	k7c6	v
původním	původní	k2eAgNnSc6d1	původní
písmu	písmo	k1gNnSc6	písmo
<g/>
,	,	kIx,	,
a	a	k8xC	a
volí	volit	k5eAaImIp3nS	volit
pokud	pokud	k8xS	pokud
možno	možno	k6eAd1	možno
takové	takový	k3xDgInPc4	takový
znaky	znak	k1gInPc4	znak
cílového	cílový	k2eAgNnSc2d1	cílové
písma	písmo	k1gNnSc2	písmo
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
mají	mít	k5eAaImIp3nP	mít
(	(	kIx(	(
<g/>
v	v	k7c6	v
hlavních	hlavní	k2eAgInPc6d1	hlavní
jazycích	jazyk	k1gInPc6	jazyk
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc4	jenž
cílové	cílový	k2eAgNnSc4d1	cílové
písmo	písmo	k1gNnSc4	písmo
používají	používat	k5eAaImIp3nP	používat
<g/>
)	)	kIx)	)
podobnou	podobný	k2eAgFnSc4d1	podobná
či	či	k8xC	či
stejnou	stejný	k2eAgFnSc4d1	stejná
výslovnost	výslovnost	k1gFnSc4	výslovnost
jako	jako	k8xC	jako
text	text	k1gInSc4	text
ve	v	k7c6	v
zdrojovém	zdrojový	k2eAgNnSc6d1	zdrojové
písmu	písmo	k1gNnSc6	písmo
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
transliterace	transliterace	k1gFnSc1	transliterace
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
nejen	nejen	k6eAd1	nejen
písmy	písmo	k1gNnPc7	písmo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
konkrétními	konkrétní	k2eAgInPc7d1	konkrétní
jazyky	jazyk	k1gInPc7	jazyk
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
písmo	písmo	k1gNnSc4	písmo
používají	používat	k5eAaImIp3nP	používat
<g/>
.	.	kIx.	.
</s>
<s>
Vliv	vliv	k1gInSc1	vliv
zdrojového	zdrojový	k2eAgInSc2d1	zdrojový
jazyka	jazyk	k1gInSc2	jazyk
lze	lze	k6eAd1	lze
málokdy	málokdy	k6eAd1	málokdy
úplně	úplně	k6eAd1	úplně
odbourat	odbourat	k5eAaPmF	odbourat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
obtížné	obtížný	k2eAgNnSc1d1	obtížné
najít	najít	k5eAaPmF	najít
univerzální	univerzální	k2eAgFnSc4d1	univerzální
transkripci	transkripce	k1gFnSc4	transkripce
z	z	k7c2	z
jednoho	jeden	k4xCgNnSc2	jeden
zdrojového	zdrojový	k2eAgNnSc2d1	zdrojové
písma	písmo	k1gNnSc2	písmo
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
žádoucí	žádoucí	k2eAgMnSc1d1	žádoucí
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
transliterace	transliterace	k1gFnSc1	transliterace
pokud	pokud	k8xS	pokud
možno	možno	k6eAd1	možno
nepodléhala	podléhat	k5eNaImAgFnS	podléhat
jazyku	jazyk	k1gInSc3	jazyk
cílového	cílový	k2eAgMnSc2d1	cílový
uživatele	uživatel	k1gMnSc2	uživatel
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
řadu	řada	k1gFnSc4	řada
zdrojových	zdrojový	k2eAgInPc2d1	zdrojový
jazyků	jazyk	k1gInPc2	jazyk
proto	proto	k8xC	proto
existují	existovat	k5eAaImIp3nP	existovat
transliterační	transliterační	k2eAgInPc1d1	transliterační
úzy	úzus	k1gInPc1	úzus
pro	pro	k7c4	pro
účely	účel	k1gInPc4	účel
lingvistických	lingvistický	k2eAgFnPc2d1	lingvistická
publikací	publikace	k1gFnPc2	publikace
<g/>
;	;	kIx,	;
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
snahy	snaha	k1gFnSc2	snaha
sjednotit	sjednotit	k5eAaPmF	sjednotit
transliteraci	transliterace	k1gFnSc4	transliterace
zeměpisných	zeměpisný	k2eAgInPc2d1	zeměpisný
názvů	název	k1gInPc2	název
na	na	k7c6	na
mapách	mapa	k1gFnPc6	mapa
<g/>
.	.	kIx.	.
</s>
<s>
Ideální	ideální	k2eAgFnSc1d1	ideální
transliterace	transliterace	k1gFnSc1	transliterace
je	být	k5eAaImIp3nS	být
zobrazení	zobrazení	k1gNnSc4	zobrazení
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
jednomu	jeden	k4xCgNnSc3	jeden
zdrojovému	zdrojový	k2eAgInSc3d1	zdrojový
znaku	znak	k1gInSc3	znak
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
jeden	jeden	k4xCgInSc4	jeden
cílový	cílový	k2eAgInSc4d1	cílový
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
repertoáry	repertoár	k1gInPc1	repertoár
dvou	dva	k4xCgNnPc2	dva
písem	písmo	k1gNnPc2	písmo
jsou	být	k5eAaImIp3nP	být
typicky	typicky	k6eAd1	typicky
ovlivněny	ovlivnit	k5eAaPmNgInP	ovlivnit
dvěma	dva	k4xCgInPc7	dva
různými	různý	k2eAgInPc7d1	různý
jazyky	jazyk	k1gInPc7	jazyk
a	a	k8xC	a
vzájemně	vzájemně	k6eAd1	vzájemně
si	se	k3xPyFc3	se
neodpovídají	odpovídat	k5eNaImIp3nP	odpovídat
<g/>
,	,	kIx,	,
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
se	se	k3xPyFc4	se
v	v	k7c6	v
cílovém	cílový	k2eAgNnSc6d1	cílové
písmu	písmo	k1gNnSc6	písmo
nové	nový	k2eAgInPc4d1	nový
znaky	znak	k1gInPc4	znak
pomocí	pomocí	k7c2	pomocí
diakritických	diakritický	k2eAgNnPc2d1	diakritické
znamének	znaménko	k1gNnPc2	znaménko
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
vhodnější	vhodný	k2eAgInSc1d2	vhodnější
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
z	z	k7c2	z
technických	technický	k2eAgInPc2d1	technický
důvodů	důvod	k1gInPc2	důvod
nezbytné	zbytný	k2eNgNnSc1d1	nezbytné
<g/>
)	)	kIx)	)
použít	použít	k5eAaPmF	použít
raději	rád	k6eAd2	rád
v	v	k7c6	v
cílovém	cílový	k2eAgNnSc6d1	cílové
písmu	písmo	k1gNnSc6	písmo
spřežku	spřežka	k1gFnSc4	spřežka
(	(	kIx(	(
<g/>
dva	dva	k4xCgInPc4	dva
nebo	nebo	k8xC	nebo
tři	tři	k4xCgInPc4	tři
znaky	znak	k1gInPc4	znak
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
reprezentují	reprezentovat	k5eAaImIp3nP	reprezentovat
jeden	jeden	k4xCgInSc4	jeden
původní	původní	k2eAgInSc4d1	původní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
možno	možno	k6eAd1	možno
by	by	kYmCp3nS	by
ale	ale	k9	ale
mělo	mít	k5eAaImAgNnS	mít
platit	platit	k5eAaImF	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
znaky	znak	k1gInPc1	znak
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
se	se	k3xPyFc4	se
spřežka	spřežka	k1gFnSc1	spřežka
skládá	skládat	k5eAaImIp3nS	skládat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nemohou	moct	k5eNaImIp3nP	moct
vedle	vedle	k7c2	vedle
sebe	sebe	k3xPyFc4	sebe
v	v	k7c6	v
daném	daný	k2eAgNnSc6d1	dané
pořadí	pořadí	k1gNnSc6	pořadí
vyskytnout	vyskytnout	k5eAaPmF	vyskytnout
jako	jako	k9	jako
samostatné	samostatný	k2eAgInPc4d1	samostatný
přepisy	přepis	k1gInPc4	přepis
dvou	dva	k4xCgInPc2	dva
zdrojových	zdrojový	k2eAgInPc2d1	zdrojový
znaků	znak	k1gInPc2	znak
<g/>
.	.	kIx.	.
</s>
<s>
Případně	případně	k6eAd1	případně
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
tuto	tento	k3xDgFnSc4	tento
kolizi	kolize	k1gFnSc4	kolize
vhodně	vhodně	k6eAd1	vhodně
řešit	řešit	k5eAaImF	řešit
jinak	jinak	k6eAd1	jinak
<g/>
.	.	kIx.	.
</s>
<s>
Nejvhodnější	vhodný	k2eAgFnSc1d3	nejvhodnější
je	být	k5eAaImIp3nS	být
použít	použít	k5eAaPmF	použít
jako	jako	k9	jako
jeden	jeden	k4xCgInSc4	jeden
ze	z	k7c2	z
znaků	znak	k1gInPc2	znak
(	(	kIx(	(
<g/>
zpravidla	zpravidla	k6eAd1	zpravidla
druhý	druhý	k4xOgMnSc1	druhý
resp.	resp.	kA	resp.
poslední	poslední	k2eAgFnSc1d1	poslední
ve	v	k7c6	v
spřežce	spřežka	k1gFnSc6	spřežka
<g/>
)	)	kIx)	)
znak	znak	k1gInSc1	znak
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
nepoužívá	používat	k5eNaImIp3nS	používat
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
takový	takový	k3xDgMnSc1	takový
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
některých	některý	k3yIgNnPc2	některý
písem	písmo	k1gNnPc2	písmo
ani	ani	k8xC	ani
nelze	lze	k6eNd1	lze
opravdovou	opravdový	k2eAgFnSc4d1	opravdová
transliteraci	transliterace	k1gFnSc4	transliterace
provést	provést	k5eAaPmF	provést
<g/>
,	,	kIx,	,
např.	např.	kA	např.
repertoár	repertoár	k1gInSc1	repertoár
čínských	čínský	k2eAgInPc2d1	čínský
znaků	znak	k1gInPc2	znak
natolik	natolik	k6eAd1	natolik
převyšuje	převyšovat	k5eAaImIp3nS	převyšovat
repertoár	repertoár	k1gInSc1	repertoár
kteréhokoli	kterýkoli	k3yIgNnSc2	kterýkoli
hláskového	hláskový	k2eAgNnSc2d1	hláskové
písma	písmo	k1gNnSc2	písmo
<g/>
,	,	kIx,	,
že	že	k8xS	že
neexistuje	existovat	k5eNaImIp3nS	existovat
prakticky	prakticky	k6eAd1	prakticky
použitelné	použitelný	k2eAgNnSc1d1	použitelné
zobrazení	zobrazení	k1gNnSc1	zobrazení
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
by	by	kYmCp3nS	by
současně	současně	k6eAd1	současně
umožňovalo	umožňovat	k5eAaImAgNnS	umožňovat
rekonstrukci	rekonstrukce	k1gFnSc4	rekonstrukce
původního	původní	k2eAgInSc2d1	původní
textu	text	k1gInSc2	text
a	a	k8xC	a
současně	současně	k6eAd1	současně
bylo	být	k5eAaImAgNnS	být
čitelné	čitelný	k2eAgFnPc4d1	čitelná
pro	pro	k7c4	pro
poučeného	poučený	k2eAgMnSc4d1	poučený
čtenáře	čtenář	k1gMnSc4	čtenář
<g/>
.	.	kIx.	.
</s>
