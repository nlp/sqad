<s>
Hans	Hans	k1gMnSc1
Christoff	Christoff	k1gMnSc1
Königsmarck	Königsmarck	k1gMnSc1
</s>
<s>
Hans	Hans	k1gMnSc1
Christoff	Christoff	k1gMnSc1
Königsmarck	Königsmarck	k1gMnSc1
Hans	Hans	k1gMnSc1
Christoff	Christoff	k1gMnSc1
Königsmarck	Königsmarck	k1gMnSc1
Narození	narození	k1gNnPc2
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
prosinec	prosinec	k1gInSc1
1605	#num#	k4
Kötzlin	Kötzlina	k1gFnPc2
<g/>
,	,	kIx,
Altmark	Altmark	k1gInSc4
Úmrtí	úmrť	k1gFnPc2
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
únor	únor	k1gInSc1
1663	#num#	k4
<g/>
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
57	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Stockholm	Stockholm	k1gInSc1
Manžel	manžel	k1gMnSc1
<g/>
(	(	kIx(
<g/>
ka	ka	k?
<g/>
)	)	kIx)
</s>
<s>
Agathe	Agathat	k5eAaPmIp3nS
von	von	k1gInSc1
Leesten	Leesten	k2eAgInSc1d1
Děti	dítě	k1gFnPc1
</s>
<s>
Beata	Beata	k1gFnSc1
Elisabet	Elisabet	k1gInSc1
von	von	k1gInSc1
KönigsmarckKurt	KönigsmarckKurt	k1gInSc1
Christoph	Christoph	k1gInSc4
von	von	k1gInSc1
KönigsmarckOtto	KönigsmarckOtto	k1gNnSc1
Wilhelm	Wilhelm	k1gInSc1
Königsmarck	Königsmarck	k1gMnSc1
Příbuzní	příbuzný	k1gMnPc1
</s>
<s>
Johanna	Johanen	k2eAgFnSc1d1
Eleonora	Eleonora	k1gFnSc1
De	De	k?
la	la	k1gNnSc7
Gardie	Gardie	k1gFnSc2
a	a	k8xC
Ebba	Ebb	k2eAgFnSc1d1
Maria	Maria	k1gFnSc1
De	De	k?
la	la	k1gNnSc1
Gardie	Gardie	k1gFnSc1
(	(	kIx(
<g/>
vnoučata	vnouče	k1gNnPc1
<g/>
)	)	kIx)
Vojenská	vojenský	k2eAgFnSc1d1
kariéra	kariéra	k1gFnSc1
Hodnost	hodnost	k1gFnSc1
</s>
<s>
Polní	polní	k2eAgMnSc1d1
maršál	maršál	k1gMnSc1
Války	válka	k1gFnSc2
</s>
<s>
Třicetiletá	třicetiletý	k2eAgFnSc1d1
válka	válka	k1gFnSc1
Bitvy	bitva	k1gFnSc2
</s>
<s>
Švédské	švédský	k2eAgNnSc1d1
obléhání	obléhání	k1gNnSc1
Prahy	Praha	k1gFnSc2
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
</s>
<s>
Hans	Hans	k1gMnSc1
Christoff	Christoff	k1gMnSc1
Königsmarck	Königsmarck	k1gMnSc1
<g/>
,	,	kIx,
vévoda	vévoda	k1gMnSc1
Tjustský	Tjustský	k2eAgMnSc1d1
(	(	kIx(
<g/>
12	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1605	#num#	k4
–	–	k?
20	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1663	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
generál	generál	k1gMnSc1
švédských	švédský	k2eAgNnPc2d1
vojsk	vojsko	k1gNnPc2
za	za	k7c2
třicetileté	třicetiletý	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Čechách	Čechy	k1gFnPc6
proslul	proslout	k5eAaPmAgInS
především	především	k6eAd1
obléháním	obléhání	k1gNnSc7
Prahy	Praha	k1gFnSc2
na	na	k7c6
samotném	samotný	k2eAgInSc6d1
konci	konec	k1gInSc6
války	válka	k1gFnSc2
(	(	kIx(
<g/>
31	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
–	–	k?
1	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1648	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Životopis	životopis	k1gInSc1
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS
se	se	k3xPyFc4
v	v	k7c6
Kötzlinu	Kötzlin	k1gInSc6
(	(	kIx(
<g/>
nyní	nyní	k6eAd1
Kyritz	Kyritz	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Altmark	Altmark	k1gInSc1
na	na	k7c6
území	území	k1gNnSc6
Braniborska	Braniborsko	k1gNnSc2
jako	jako	k8xS,k8xC
syn	syn	k1gMnSc1
Conrada	Conrada	k1gFnSc1
von	von	k1gInSc1
Königsmarcka	Königsmarcka	k1gFnSc1
a	a	k8xC
Beatrix	Beatrix	k1gInSc1
von	von	k1gInSc4
Blumenthal	Blumenthal	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
třicetileté	třicetiletý	k2eAgFnSc2d1
války	válka	k1gFnSc2
proslul	proslout	k5eAaPmAgMnS
rychlými	rychlý	k2eAgInPc7d1
výpady	výpad	k1gInPc7
a	a	k8xC
drancováním	drancování	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1640	#num#	k4
dosáhl	dosáhnout	k5eAaPmAgInS
hodnosti	hodnost	k1gFnSc2
generála	generál	k1gMnSc2
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1645	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
guvernérem	guvernér	k1gMnSc7
Brém	Brémy	k1gFnPc2
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1645	#num#	k4
pak	pak	k6eAd1
člen	člen	k1gMnSc1
Švédské	švédský	k2eAgFnSc2d1
státní	státní	k2eAgFnSc2d1
rady	rada	k1gFnSc2
a	a	k8xC
roku	rok	k1gInSc2
1655	#num#	k4
dosáhl	dosáhnout	k5eAaPmAgInS
hodnosti	hodnost	k1gFnSc2
polního	polní	k2eAgMnSc4d1
maršála	maršál	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1655	#num#	k4
nechal	nechat	k5eAaPmAgMnS
postavit	postavit	k5eAaPmF
hrad	hrad	k1gInSc4
v	v	k7c4
Lieth	Lieth	k1gInSc4
a	a	k8xC
pojmenoval	pojmenovat	k5eAaPmAgMnS
ho	on	k3xPp3gMnSc4
po	po	k7c6
své	svůj	k3xOyFgFnSc6
ženě	žena	k1gFnSc6
Agathenburg	Agathenburg	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jméno	jméno	k1gNnSc1
hradu	hrad	k1gInSc2
pak	pak	k6eAd1
nahradilo	nahradit	k5eAaPmAgNnS
původní	původní	k2eAgInSc4d1
název	název	k1gInSc4
obce	obec	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Vojenská	vojenský	k2eAgFnSc1d1
kariéra	kariéra	k1gFnSc1
</s>
<s>
Vojenskou	vojenský	k2eAgFnSc4d1
kariéru	kariéra	k1gFnSc4
zahájil	zahájit	k5eAaPmAgMnS
v	v	k7c6
císařské	císařský	k2eAgFnSc6d1
armádě	armáda	k1gFnSc6
<g/>
,	,	kIx,
ze	z	k7c2
které	který	k3yQgFnSc2,k3yRgFnSc2,k3yIgFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1629	#num#	k4
odešel	odejít	k5eAaPmAgMnS
v	v	k7c6
hodnosti	hodnost	k1gFnSc6
kapitána	kapitán	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
vylodění	vylodění	k1gNnSc6
krále	král	k1gMnSc2
Gustava	Gustav	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Adolfa	Adolf	k1gMnSc2
v	v	k7c6
Německu	Německo	k1gNnSc6
roku	rok	k1gInSc2
1630	#num#	k4
vstoupil	vstoupit	k5eAaPmAgInS
do	do	k7c2
švédských	švédský	k2eAgFnPc2d1
služeb	služba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pod	pod	k7c7
velením	velení	k1gNnSc7
generálů	generál	k1gMnPc2
Lennarta	Lennart	k1gMnSc2
Torstensona	Torstenson	k1gMnSc2
a	a	k8xC
Carla	Carl	k1gMnSc2
Gustava	Gustav	k1gMnSc2
Wrangela	Wrangel	k1gMnSc2
mu	on	k3xPp3gMnSc3
bývalo	bývat	k5eAaImAgNnS
svěřováno	svěřovat	k5eAaImNgNnS
velení	velení	k1gNnSc1
samostatně	samostatně	k6eAd1
operujícího	operující	k2eAgInSc2d1
sboru	sbor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
ním	on	k3xPp3gMnSc7
pak	pak	k6eAd1
v	v	k7c6
Německu	Německo	k1gNnSc6
drancoval	drancovat	k5eAaImAgMnS
a	a	k8xC
verboval	verbovat	k5eAaImAgMnS
posily	posila	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
službách	služba	k1gFnPc6
generála	generál	k1gMnSc2
Torstenssona	Torstensson	k1gMnSc2
byl	být	k5eAaImAgMnS
neocenitelný	ocenitelný	k2eNgMnSc1d1
pomocník	pomocník	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
mimo	mimo	k7c4
jiné	jiné	k1gNnSc4
hrál	hrát	k5eAaImAgMnS
hlavní	hlavní	k2eAgFnSc4d1
roli	role	k1gFnSc4
ve	v	k7c6
vítězství	vítězství	k1gNnSc6
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Jüterbogu	Jüterbog	k1gInSc2
a	a	k8xC
dobytí	dobytí	k1gNnSc2
arcibiskupství	arcibiskupství	k1gNnSc2
v	v	k7c6
Brémách	Brémy	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
ustaven	ustavit	k5eAaPmNgMnS
do	do	k7c2
role	role	k1gFnSc2
guvernéra	guvernér	k1gMnSc2
Brém	Brémy	k1gFnPc2
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
zastával	zastávat	k5eAaImAgMnS
až	až	k9
do	do	k7c2
své	svůj	k3xOyFgFnSc2
smrti	smrt	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1641	#num#	k4
spolu	spolu	k6eAd1
s	s	k7c7
Wrangelem	Wrangel	k1gInSc7
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Wolfenbüttelu	Wolfenbüttel	k1gInSc2
porazili	porazit	k5eAaPmAgMnP
Rakušany	Rakušan	k1gMnPc7
vedené	vedený	k2eAgFnSc2d1
Leopoldem	Leopold	k1gMnSc7
Habsburským	habsburský	k2eAgMnPc3d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Königsmarck	Königsmarck	k1gInSc1
také	také	k9
přinutil	přinutit	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
1645	#num#	k4
Jana	Jan	k1gMnSc2
Jiřího	Jiří	k1gMnSc2
I.	I.	kA
Saského	saský	k2eAgInSc2d1
<g/>
,	,	kIx,
významného	významný	k2eAgMnSc2d1
císařského	císařský	k2eAgMnSc2d1
spojence	spojenec	k1gMnSc2
<g/>
,	,	kIx,
k	k	k7c3
půlročnímu	půlroční	k2eAgNnSc3d1
příměří	příměří	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
témže	týž	k3xTgInSc6
roce	rok	k1gInSc6
ale	ale	k8xC
vystřídal	vystřídat	k5eAaPmAgInS
Wrangel	Wrangel	k1gInSc1
ve	v	k7c6
velení	velení	k1gNnSc6
nemocného	nemocný	k1gMnSc2
Torstenssona	Torstensson	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Königsmarck	Königsmarck	k1gMnSc1
měl	mít	k5eAaImAgMnS
problémy	problém	k1gInPc4
podřídit	podřídit	k5eAaPmF
se	se	k3xPyFc4
velení	velení	k1gNnSc1
mladšího	mladý	k2eAgMnSc2d2
Wrangela	Wrangel	k1gMnSc2
a	a	k8xC
jednal	jednat	k5eAaImAgMnS
nezávisle	závisle	k6eNd1
na	na	k7c6
jeho	jeho	k3xOp3gInPc6
rozkazech	rozkaz	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1648	#num#	k4
se	se	k3xPyFc4
jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
vojska	vojsko	k1gNnSc2
rychle	rychle	k6eAd1
přemístila	přemístit	k5eAaPmAgFnS
k	k	k7c3
Praze	Praha	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
znalosti	znalost	k1gFnSc3
slabiny	slabina	k1gFnSc2
v	v	k7c6
opevnění	opevnění	k1gNnSc6
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
Königsmarck	Königsmarck	k1gMnSc1
získal	získat	k5eAaPmAgMnS
od	od	k7c2
císařského	císařský	k2eAgMnSc2d1
zběha	zběh	k1gMnSc2
podplukovníka	podplukovník	k1gMnSc2
Arnošta	Arnošt	k1gMnSc2
Ottowalského	Ottowalský	k2eAgMnSc2d1
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
obsadila	obsadit	k5eAaPmAgFnS
Malou	malý	k2eAgFnSc4d1
Stranu	strana	k1gFnSc4
a	a	k8xC
Pražský	pražský	k2eAgInSc4d1
hrad	hrad	k1gInSc4
a	a	k8xC
vyrabovala	vyrabovat	k5eAaPmAgFnS
jej	on	k3xPp3gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
přímou	přímý	k2eAgFnSc4d1
žádost	žádost	k1gFnSc4
královny	královna	k1gFnSc2
Kristýny	Kristýna	k1gFnSc2
nechal	nechat	k5eAaPmAgInS
Königsmarck	Königsmarck	k1gInSc1
zabavit	zabavit	k5eAaPmF
velmi	velmi	k6eAd1
cenné	cenný	k2eAgFnPc4d1
sbírky	sbírka	k1gFnPc4
císaře	císař	k1gMnSc2
Rudolfa	Rudolf	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
na	na	k7c6
Pražském	pražský	k2eAgInSc6d1
hradě	hrad	k1gInSc6
a	a	k8xC
nechal	nechat	k5eAaPmAgMnS
je	být	k5eAaImIp3nS
převézt	převézt	k5eAaPmF
do	do	k7c2
Švédska	Švédsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následovalo	následovat	k5eAaImAgNnS
dlouhé	dlouhý	k2eAgNnSc1d1
ostřelovaní	ostřelovaný	k2eAgMnPc1d1
opevněného	opevněný	k2eAgInSc2d1
zbytku	zbytek	k1gInSc2
Prahy	Praha	k1gFnSc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
obránci	obránce	k1gMnPc1
podnikali	podnikat	k5eAaImAgMnP
protiútoky	protiútok	k1gInPc4
<g/>
,	,	kIx,
ale	ale	k8xC
z	z	k7c2
důvodu	důvod	k1gInSc2
nedostatku	nedostatek	k1gInSc2
střelného	střelný	k2eAgInSc2d1
prachu	prach	k1gInSc2
začali	začít	k5eAaPmAgMnP
obránci	obránce	k1gMnPc1
Prahy	Praha	k1gFnSc2
vyjednávat	vyjednávat	k5eAaImF
o	o	k7c6
příměří	příměří	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Švédské	švédský	k2eAgInPc1d1
požadavky	požadavek	k1gInPc1
byly	být	k5eAaImAgInP
však	však	k9
natolik	natolik	k6eAd1
potupné	potupný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
je	on	k3xPp3gMnPc4
obránci	obránce	k1gMnPc1
odmítli	odmítnout	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
10	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
11	#num#	k4
<g/>
.	.	kIx.
říjnem	říjen	k1gInSc7
1648	#num#	k4
byl	být	k5eAaImAgInS
podepsán	podepsat	k5eAaPmNgInS
Vestfálský	vestfálský	k2eAgInSc1d1
mír	mír	k1gInSc1
<g/>
,	,	kIx,
ale	ale	k8xC
Švédové	Švéd	k1gMnPc1
v	v	k7c6
nevědomí	nevědomí	k1gNnSc6
dále	daleko	k6eAd2
podnikali	podnikat	k5eAaImAgMnP
útoky	útok	k1gInPc4
na	na	k7c4
Prahu	Praha	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
1	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
dostal	dostat	k5eAaPmAgMnS
následník	následník	k1gMnSc1
švédského	švédský	k2eAgInSc2d1
trůnu	trůn	k1gInSc2
Karel	Karel	k1gMnSc1
X.	X.	kA
Gustav	Gustav	k1gMnSc1
zprávu	zpráva	k1gFnSc4
o	o	k7c6
podepsání	podepsání	k1gNnSc6
míru	mír	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Königsmarck	Königsmarck	k1gInSc1
byl	být	k5eAaImAgInS
donucen	donucen	k2eAgMnSc1d1
ukončit	ukončit	k5eAaPmF
obléhání	obléhání	k1gNnSc4
a	a	k8xC
stáhnout	stáhnout	k5eAaPmF
se	se	k3xPyFc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
bitva	bitva	k1gFnSc1
byla	být	k5eAaImAgFnS
poslední	poslední	k2eAgFnSc7d1
velkou	velký	k2eAgFnSc7d1
bitvou	bitva	k1gFnSc7
třicetileté	třicetiletý	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Švédské	švédský	k2eAgNnSc1d1
obléhání	obléhání	k1gNnSc1
Prahy	Praha	k1gFnSc2
</s>
<s>
Švédská	švédský	k2eAgFnSc1d1
loupež	loupež	k1gFnSc1
uměleckých	umělecký	k2eAgNnPc2d1
děl	dělo	k1gNnPc2
v	v	k7c6
Praze	Praha	k1gFnSc6
</s>
<s>
Třicetiletá	třicetiletý	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
http://id.loc.gov/authorities/names/n85088555.html	http://id.loc.gov/authorities/names/n85088555.html	k1gInSc1
<g/>
↑	↑	k?
http://aleph.nkp.cz/F/4ATE3F6MP8XYC5JR61B35JHIRIFFPLDGS393NE5186SDNYRRCE-30186?func=find-word&	http://aleph.nkp.cz/F/4ATE3F6MP8XYC5JR61B35JHIRIFFPLDGS393NE5186SDNYRRCE-30186?func=find-word&	k1gInSc7
<g/>
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.national-geographic.cz	www.national-geographic.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
REBITSCH	REBITSCH	kA
<g/>
,	,	kIx,
Robert	Robert	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Matyáš	Matyáš	k1gMnSc1
Gallas	Gallas	k1gMnSc1
(	(	kIx(
<g/>
1588	#num#	k4
<g/>
–	–	k?
<g/>
1647	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Císařský	císařský	k2eAgMnSc1d1
generál	generál	k1gMnSc1
a	a	k8xC
Valdštejnův	Valdštejnův	k2eAgMnSc1d1
"	"	kIx"
<g/>
dědic	dědic	k1gMnSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Grada	Grada	k1gFnSc1
Publishing	Publishing	k1gInSc1
a.s.	a.s.	k?
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
247	#num#	k4
<g/>
-	-	kIx~
<g/>
4778	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
DOSTÁL	Dostál	k1gMnSc1
<g/>
,	,	kIx,
Alois	Alois	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Königsmark	Königsmark	k1gInSc1
<g/>
,	,	kIx,
vůdce	vůdce	k1gMnPc4
Švédů	Švéd	k1gMnPc2
před	před	k7c7
Prahou	Praha	k1gFnSc7
:	:	kIx,
dějepisný	dějepisný	k2eAgInSc1d1
obraz	obraz	k1gInSc1
roku	rok	k1gInSc2
1648	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Rohlíček	rohlíček	k1gInSc1
a	a	k8xC
Sievers	Sievers	k1gInSc1
<g/>
,	,	kIx,
1877	#num#	k4
<g/>
.	.	kIx.
103	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Hans	hansa	k1gFnPc2
Christoff	Christoff	k1gMnSc1
Königsmarck	Königsmarck	k1gMnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
1159073112	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
5487	#num#	k4
6279	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
85088555	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
8813627	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
85088555	#num#	k4
</s>
