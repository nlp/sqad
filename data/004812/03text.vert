<s>
Gainax	Gainax	k1gInSc1	Gainax
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
ガ	ガ	k?	ガ
<g/>
,	,	kIx,	,
Gainakkusu	Gainakkus	k1gInSc2	Gainakkus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
japonské	japonský	k2eAgNnSc4d1	Japonské
animační	animační	k2eAgNnSc4d1	animační
studio	studio	k1gNnSc4	studio
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
nejvíce	nejvíce	k6eAd1	nejvíce
proslavilo	proslavit	k5eAaPmAgNnS	proslavit
televizním	televizní	k2eAgInSc7d1	televizní
seriálem	seriál	k1gInSc7	seriál
Neon	neon	k1gInSc4	neon
Genesis	Genesis	k1gFnSc1	Genesis
Evangelion	Evangelion	k1gInSc1	Evangelion
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
NGE	NGE	kA	NGE
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Gainax	Gainax	k1gInSc1	Gainax
je	on	k3xPp3gMnPc4	on
znám	znát	k5eAaImIp1nS	znát
díky	díky	k7c3	díky
svým	svůj	k3xOyFgInPc3	svůj
ambiciozním	ambiciozní	k2eAgInPc3d1	ambiciozní
<g/>
,	,	kIx,	,
experimentálním	experimentální	k2eAgInPc3d1	experimentální
anime	anim	k1gInSc5	anim
a	a	k8xC	a
kontroverzním	kontroverzní	k2eAgInPc3d1	kontroverzní
koncům	konec	k1gInPc3	konec
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
své	svůj	k3xOyFgFnSc2	svůj
existence	existence	k1gFnSc2	existence
se	se	k3xPyFc4	se
studio	studio	k1gNnSc1	studio
Gainax	Gainax	k1gInSc1	Gainax
potýkalo	potýkat	k5eAaImAgNnS	potýkat
s	s	k7c7	s
rozpočtovými	rozpočtový	k2eAgInPc7d1	rozpočtový
problémy	problém	k1gInPc7	problém
a	a	k8xC	a
špatným	špatný	k2eAgInSc7d1	špatný
managementem	management	k1gInSc7	management
<g/>
.	.	kIx.	.
</s>
<s>
Těsně	těsně	k6eAd1	těsně
po	po	k7c6	po
úspěchu	úspěch	k1gInSc6	úspěch
NGE	NGE	kA	NGE
se	se	k3xPyFc4	se
Gainax	Gainax	k1gInSc4	Gainax
vyhýbalo	vyhýbat	k5eAaImAgNnS	vyhýbat
placení	placení	k1gNnSc1	placení
daní	daň	k1gFnPc2	daň
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
prezident	prezident	k1gMnSc1	prezident
<g/>
,	,	kIx,	,
Takeši	Takech	k1gMnPc1	Takech
Sawamura	Sawamur	k1gMnSc2	Sawamur
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
odsouzen	odsoudit	k5eAaPmNgMnS	odsoudit
za	za	k7c4	za
daňový	daňový	k2eAgInSc4d1	daňový
podvod	podvod	k1gInSc4	podvod
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
do	do	k7c2	do
uvedení	uvedení	k1gNnSc2	uvedení
Neon	neon	k1gInSc1	neon
Genesis	Genesis	k1gFnSc4	Genesis
Evangelion	Evangelion	k1gInSc1	Evangelion
pracoval	pracovat	k5eAaImAgInS	pracovat
Gainax	Gainax	k1gInSc4	Gainax
na	na	k7c6	na
příbězích	příběh	k1gInPc6	příběh
vytvořených	vytvořený	k2eAgInPc2d1	vytvořený
podomácku	podomácku	k6eAd1	podomácku
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
studio	studio	k1gNnSc1	studio
adaptovalo	adaptovat	k5eAaBmAgNnS	adaptovat
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
existujících	existující	k2eAgNnPc2d1	existující
mang	mango	k1gNnPc2	mango
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
Kareši	Kareš	k1gMnPc7	Kareš
kanodžo	kanodžo	k6eAd1	kanodžo
no	no	k9	no
džidžó	džidžó	k?	džidžó
a	a	k8xC	a
Mahoromatic	Mahoromatice	k1gFnPc2	Mahoromatice
<g/>
,	,	kIx,	,
do	do	k7c2	do
anime	animat	k5eAaPmIp3nS	animat
<g/>
.	.	kIx.	.
</s>
<s>
Dalo	dát	k5eAaPmAgNnS	dát
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
říct	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
Gainax	Gainax	k1gInSc1	Gainax
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
produkuje	produkovat	k5eAaImIp3nS	produkovat
dva	dva	k4xCgInPc4	dva
typy	typ	k1gInPc4	typ
anime	animat	k5eAaPmIp3nS	animat
<g/>
:	:	kIx,	:
komerční	komerční	k2eAgNnPc4d1	komerční
díla	dílo	k1gNnPc4	dílo
(	(	kIx(	(
<g/>
Mahoromatic	Mahoromatice	k1gFnPc2	Mahoromatice
<g/>
,	,	kIx,	,
Kore	Kore	k1gFnSc1	Kore
ga	ga	k?	ga
wataši	watasat	k5eAaPmIp1nSwK	watasat
no	no	k9	no
gošudžin-sama	gošudžinama	k1gFnSc1	gošudžin-sama
<g/>
)	)	kIx)	)
versus	versus	k7c1	versus
experimentálnější	experimentální	k2eAgNnSc4d2	experimentálnější
díla	dílo	k1gNnPc4	dílo
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
zvyklostem	zvyklost	k1gFnPc3	zvyklost
společnosti	společnost	k1gFnSc2	společnost
(	(	kIx(	(
<g/>
FLCL	FLCL	kA	FLCL
<g/>
,	,	kIx,	,
Gunbuster	Gunbuster	k1gMnSc1	Gunbuster
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
tvorby	tvorba	k1gFnSc2	tvorba
anime	animat	k5eAaPmIp3nS	animat
se	se	k3xPyFc4	se
Gainax	Gainax	k1gInSc1	Gainax
velmi	velmi	k6eAd1	velmi
silně	silně	k6eAd1	silně
spoléhá	spoléhat	k5eAaImIp3nS	spoléhat
také	také	k9	také
na	na	k7c4	na
reklamní	reklamní	k2eAgInSc4d1	reklamní
prodej	prodej	k1gInSc4	prodej
slavných	slavný	k2eAgFnPc2d1	slavná
rekvizit	rekvizita	k1gFnPc2	rekvizita
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
seriál	seriál	k1gInSc1	seriál
Neon	neon	k1gInSc1	neon
Genesis	Genesis	k1gFnSc4	Genesis
Evangelion	Evangelion	k1gInSc1	Evangelion
skončil	skončit	k5eAaPmAgInS	skončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
videohry	videohra	k1gFnPc1	videohra
<g/>
,	,	kIx,	,
třička	třička	k6eAd1	třička
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
doplňky	doplněk	k1gInPc4	doplněk
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
tematikou	tematika	k1gFnSc7	tematika
jsou	být	k5eAaImIp3nP	být
produkovány	produkovat	k5eAaImNgInP	produkovat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Studio	studio	k1gNnSc1	studio
Gainax	Gainax	k1gInSc4	Gainax
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
jako	jako	k9	jako
Daicon	Daicon	k1gInSc4	Daicon
Film	film	k1gInSc1	film
skupinkou	skupinka	k1gFnSc7	skupinka
vysokoškoláků	vysokoškolák	k1gMnPc2	vysokoškolák
Hideakim	Hideakim	k1gInSc1	Hideakim
Annem	Annum	k1gNnSc7	Annum
<g/>
,	,	kIx,	,
Jošijukim	Jošijuki	k1gNnSc7	Jošijuki
Sadamotem	Sadamot	k1gMnSc7	Sadamot
<g/>
,	,	kIx,	,
Takamim	Takamim	k1gMnSc1	Takamim
Akaim	Akaim	k1gMnSc1	Akaim
a	a	k8xC	a
Šindžim	Šindžim	k1gMnSc1	Šindžim
Higučim	Higučim	k1gMnSc1	Higučim
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
první	první	k4xOgInSc1	první
projekt	projekt	k1gInSc1	projekt
Daicon	Daicon	k1gInSc1	Daicon
III	III	kA	III
byl	být	k5eAaImAgInS	být
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
pro	pro	k7c4	pro
animované	animovaný	k2eAgInPc4d1	animovaný
spoty	spot	k1gInPc4	spot
20	[number]	k4	20
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc3	výročí
japonské	japonský	k2eAgFnSc2d1	japonská
národní	národní	k2eAgFnSc2d1	národní
SF	SF	kA	SF
konvence	konvence	k1gFnSc2	konvence
<g/>
,	,	kIx,	,
konající	konající	k2eAgNnSc4d1	konající
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1981	[number]	k4	1981
v	v	k7c6	v
japonské	japonský	k2eAgFnSc6d1	japonská
Ósace	Ósaka	k1gFnSc6	Ósaka
<g/>
.	.	kIx.	.
</s>
<s>
Spoty	spot	k1gInPc1	spot
byly	být	k5eAaImAgInP	být
o	o	k7c6	o
malé	malý	k2eAgFnSc6d1	malá
holčičce	holčička	k1gFnSc6	holčička
bojující	bojující	k2eAgFnSc6d1	bojující
proti	proti	k7c3	proti
všelijakým	všelijaký	k3yIgNnPc3	všelijaký
monstrům	monstrum	k1gNnPc3	monstrum
<g/>
,	,	kIx,	,
robotům	robot	k1gInPc3	robot
a	a	k8xC	a
vesmírným	vesmírný	k2eAgFnPc3d1	vesmírná
lodím	loď	k1gFnPc3	loď
z	z	k7c2	z
dřívějších	dřívější	k2eAgFnPc2d1	dřívější
sci-fi	scii	k1gFnPc2	sci-fi
televizních	televizní	k2eAgInPc2d1	televizní
pořadů	pořad	k1gInPc2	pořad
(	(	kIx(	(
<g/>
zahrnujících	zahrnující	k2eAgFnPc2d1	zahrnující
například	například	k6eAd1	například
Ultramana	Ultraman	k1gMnSc2	Ultraman
<g/>
,	,	kIx,	,
Učú	Učú	k1gFnSc2	Učú
senkan	senkany	k1gInPc2	senkany
Jamato	Jamat	k2eAgNnSc1d1	Jamato
<g/>
,	,	kIx,	,
Star	Star	kA	Star
Trek	Trek	k1gInSc1	Trek
<g/>
,	,	kIx,	,
Star	Star	kA	Star
Wars	Wars	k1gInSc4	Wars
nebo	nebo	k8xC	nebo
Godzillu	Godzilla	k1gFnSc4	Godzilla
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dívka	dívka	k1gFnSc1	dívka
nakonec	nakonec	k6eAd1	nakonec
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
pouštní	pouštní	k2eAgFnPc4d1	pouštní
pláně	pláň	k1gFnPc4	pláň
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vylije	vylít	k5eAaPmIp3nS	vylít
sklenici	sklenice	k1gFnSc4	sklenice
vody	voda	k1gFnSc2	voda
na	na	k7c4	na
vyschlou	vyschlý	k2eAgFnSc4d1	vyschlá
ředkev	ředkev	k1gFnSc4	ředkev
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
záhy	záhy	k6eAd1	záhy
promění	proměnit	k5eAaPmIp3nS	proměnit
v	v	k7c4	v
obrovskou	obrovský	k2eAgFnSc4d1	obrovská
vesmírnou	vesmírný	k2eAgFnSc4d1	vesmírná
loď	loď	k1gFnSc4	loď
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc2	jenž
dívka	dívka	k1gFnSc1	dívka
odletí	odletět	k5eAaPmIp3nS	odletět
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc1	tento
animovaný	animovaný	k2eAgInSc1d1	animovaný
spot	spot	k1gInSc1	spot
ambiciózní	ambiciózní	k2eAgInSc1d1	ambiciózní
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnPc1	jeho
animace	animace	k1gFnPc1	animace
byly	být	k5eAaImAgFnP	být
hrubé	hrubý	k2eAgFnPc1d1	hrubá
a	a	k8xC	a
nízké	nízký	k2eAgFnPc1d1	nízká
kvality	kvalita	k1gFnPc1	kvalita
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
internetu	internet	k1gInSc6	internet
se	se	k3xPyFc4	se
rozšířil	rozšířit	k5eAaPmAgMnS	rozšířit
výklad	výklad	k1gInSc4	výklad
tohoto	tento	k3xDgNnSc2	tento
díla	dílo	k1gNnSc2	dílo
tvrdící	tvrdící	k2eAgNnSc1d1	tvrdící
<g/>
,	,	kIx,	,
že	že	k8xS	že
voda	voda	k1gFnSc1	voda
představuje	představovat	k5eAaImIp3nS	představovat
kreativitu	kreativita	k1gFnSc4	kreativita
a	a	k8xC	a
obrazotvornost	obrazotvornost	k1gFnSc4	obrazotvornost
<g/>
,	,	kIx,	,
monstra	monstrum	k1gNnPc1	monstrum
a	a	k8xC	a
jiní	jiný	k2eAgMnPc1d1	jiný
protivníci	protivník	k1gMnPc1	protivník
stojící	stojící	k2eAgFnSc4d1	stojící
proti	proti	k7c3	proti
dívce	dívka	k1gFnSc3	dívka
pak	pak	k9	pak
ty	ten	k3xDgMnPc4	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
tvůrčí	tvůrčí	k2eAgFnSc3d1	tvůrčí
duši	duše	k1gFnSc3	duše
zničit	zničit	k5eAaPmF	zničit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
na	na	k7c4	na
22	[number]	k4	22
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc3	výročí
japonské	japonský	k2eAgFnSc2d1	japonská
národní	národní	k2eAgFnSc2d1	národní
SF	SF	kA	SF
konvenci	konvence	k1gFnSc4	konvence
již	již	k9	již
skupina	skupina	k1gFnSc1	skupina
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
větší	veliký	k2eAgInSc4d2	veliký
projekt	projekt	k1gInSc4	projekt
-	-	kIx~	-
Daicon	Daicon	k1gInSc4	Daicon
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Spot	spot	k1gInSc1	spot
začínal	začínat	k5eAaImAgInS	začínat
rekapitulací	rekapitulace	k1gFnSc7	rekapitulace
původního	původní	k2eAgInSc2d1	původní
šotu	šot	k1gInSc2	šot
s	s	k7c7	s
daleko	daleko	k6eAd1	daleko
vyšší	vysoký	k2eAgFnSc7d2	vyšší
kvalitou	kvalita	k1gFnSc7	kvalita
animací	animace	k1gFnPc2	animace
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
hlavní	hlavní	k2eAgFnSc1d1	hlavní
hrdinka	hrdinka	k1gFnSc1	hrdinka
<g/>
,	,	kIx,	,
v	v	k7c6	v
dospělém	dospělý	k2eAgInSc6d1	dospělý
věku	věk	k1gInSc6	věk
a	a	k8xC	a
oblečená	oblečený	k2eAgFnSc1d1	oblečená
do	do	k7c2	do
králičího	králičí	k2eAgInSc2d1	králičí
kostýmu	kostým	k1gInSc2	kostým
z	z	k7c2	z
Playboye	playboy	k1gMnSc2	playboy
<g/>
,	,	kIx,	,
létala	létat	k5eAaImAgFnS	létat
po	po	k7c6	po
obloze	obloha	k1gFnSc6	obloha
na	na	k7c6	na
meči	meč	k1gInSc6	meč
Stormbringer	Stormbringra	k1gFnPc2	Stormbringra
<g/>
,	,	kIx,	,
bojovala	bojovat	k5eAaImAgFnS	bojovat
proti	proti	k7c3	proti
bytostem	bytost	k1gFnPc3	bytost
známých	známý	k2eAgMnPc2d1	známý
ze	z	k7c2	z
sci-fi	scii	k1gFnSc2	sci-fi
a	a	k8xC	a
fantasy	fantas	k1gInPc4	fantas
filmů	film	k1gInPc2	film
nebo	nebo	k8xC	nebo
novel	novela	k1gFnPc2	novela
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
proti	proti	k7c3	proti
Darth	Darth	k1gInSc1	Darth
Vaderovi	Vader	k1gMnSc3	Vader
<g/>
,	,	kIx,	,
Vetřelci	vetřelec	k1gMnSc3	vetřelec
<g/>
,	,	kIx,	,
klingonskému	klingonský	k2eAgInSc3d1	klingonský
bitevnímu	bitevní	k2eAgInSc3d1	bitevní
křižníku	křižník	k1gInSc3	křižník
nebo	nebo	k8xC	nebo
Spidermanovi	Spidermanův	k2eAgMnPc1d1	Spidermanův
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
akce	akce	k1gFnSc1	akce
byla	být	k5eAaImAgFnS	být
doprovázená	doprovázený	k2eAgFnSc1d1	doprovázená
písní	píseň	k1gFnSc7	píseň
"	"	kIx"	"
<g/>
Twilight	Twilighta	k1gFnPc2	Twilighta
<g/>
"	"	kIx"	"
od	od	k7c2	od
hudební	hudební	k2eAgFnSc2d1	hudební
skupiny	skupina	k1gFnSc2	skupina
Electric	Electric	k1gMnSc1	Electric
Light	Light	k1gMnSc1	Light
Orchestra	orchestra	k1gFnSc1	orchestra
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
použití	použití	k1gNnSc1	použití
této	tento	k3xDgFnSc2	tento
písně	píseň	k1gFnSc2	píseň
nebylo	být	k5eNaImAgNnS	být
licencované	licencovaný	k2eAgNnSc1d1	licencované
<g/>
,	,	kIx,	,
spot	spot	k1gInSc1	spot
nemohl	moct	k5eNaImAgInS	moct
být	být	k5eAaImF	být
oficiálně	oficiálně	k6eAd1	oficiálně
vydán	vydat	k5eAaPmNgInS	vydat
na	na	k7c6	na
DVD	DVD	kA	DVD
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
později	pozdě	k6eAd2	pozdě
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
jako	jako	k8xS	jako
limitovaná	limitovaný	k2eAgFnSc1d1	limitovaná
edice	edice	k1gFnSc1	edice
spotů	spot	k1gInPc2	spot
studia	studio	k1gNnSc2	studio
Daicon	Daicona	k1gFnPc2	Daicona
<g/>
.	.	kIx.	.
</s>
<s>
Píseň	píseň	k1gFnSc1	píseň
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
také	také	k9	také
jako	jako	k9	jako
úvodní	úvodní	k2eAgFnSc1d1	úvodní
znělka	znělka	k1gFnSc1	znělka
hraného	hraný	k2eAgInSc2d1	hraný
seriálu	seriál	k1gInSc2	seriál
Denša	Denš	k1gInSc2	Denš
otoko	otoko	k6eAd1	otoko
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
již	již	k6eAd1	již
licencovaná	licencovaný	k2eAgFnSc1d1	licencovaná
<g/>
.	.	kIx.	.
</s>
<s>
Díky	dík	k1gInPc1	dík
Daicon	Daicona	k1gFnPc2	Daicona
IV	Iva	k1gFnPc2	Iva
začalo	začít	k5eAaPmAgNnS	začít
být	být	k5eAaImF	být
Daicon	Daicon	k1gNnSc1	Daicon
Film	film	k1gInSc4	film
bráno	brát	k5eAaImNgNnS	brát
jako	jako	k9	jako
talentované	talentovaný	k2eAgNnSc1d1	talentované
anime	animat	k5eAaPmIp3nS	animat
studio	studio	k1gNnSc1	studio
<g/>
.	.	kIx.	.
</s>
<s>
Studio	studio	k1gNnSc1	studio
změnilo	změnit	k5eAaPmAgNnS	změnit
svůj	svůj	k3xOyFgInSc4	svůj
název	název	k1gInSc4	název
na	na	k7c4	na
Gainax	Gainax	k1gInSc4	Gainax
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
</s>
<s>
Fušigi	Fušigi	k6eAd1	Fušigi
no	no	k9	no
umi	umi	k?	umi
no	no	k9	no
Nadia	Nadia	k1gFnSc1	Nadia
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
Neon	neon	k1gInSc4	neon
Genesis	Genesis	k1gFnSc1	Genesis
Evangelion	Evangelion	k1gInSc1	Evangelion
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
–	–	k?	–
v	v	k7c6	v
koprodukci	koprodukce	k1gFnSc6	koprodukce
s	s	k7c7	s
Tacunoko	Tacunoka	k1gFnSc5	Tacunoka
Production	Production	k1gInSc1	Production
Kareši	Kareš	k1gMnPc7	Kareš
kanodžo	kanodžo	k6eAd1	kanodžo
no	no	k9	no
džidžo	džidžo	k6eAd1	džidžo
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
známé	známý	k2eAgFnPc1d1	známá
také	také	k9	také
jako	jako	k9	jako
Kare	kar	k1gInSc5	kar
Kano	Kana	k1gFnSc5	Kana
Anime	Anim	k1gInSc5	Anim
ai	ai	k?	ai
no	no	k9	no
awa	awa	k?	awa
awa	awa	k?	awa
awá	awá	k?	awá
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Oručuban	Oručuban	k1gInSc1	Oručuban
Ebiču	Ebičus	k1gInSc2	Ebičus
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
FLCL	FLCL	kA	FLCL
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
Mahoromatic	Mahoromatice	k1gFnPc2	Mahoromatice
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
Abenobaši	Abenobaš	k1gInSc6	Abenobaš
mahó	mahó	k?	mahó
šótengai	šótenga	k1gFnSc2	šótenga
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
Puči	puč	k1gInSc6	puč
puri	pur	k1gFnSc2	pur
Yucie	Yucie	k1gFnSc2	Yucie
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
Bókjaku	Bókjak	k1gInSc2	Bókjak
no	no	k9	no
senricu	senricu	k6eAd1	senricu
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
Kono	Kono	k1gNnSc1	Kono
minikuku	minikuk	k1gInSc2	minikuk
mo	mo	k?	mo
ucukušii	ucukušie	k1gFnSc3	ucukušie
sekai	seka	k1gFnSc2	seka
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
známé	známý	k2eAgNnSc1d1	známé
také	také	k6eAd1	také
jako	jako	k8xS	jako
Konomini	Konomin	k2eAgMnPc1d1	Konomin
Kore	Kore	k1gInSc4	Kore
ga	ga	k?	ga
wataši	watasat	k5eAaPmIp1nSwK	watasat
no	no	k9	no
gošudžin-sama	gošudžinama	k1gFnSc1	gošudžin-sama
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
Tengen	Tengen	k1gInSc1	Tengen
toppa	toppa	k1gFnSc1	toppa
Gurren-Lagann	Gurren-Lagann	k1gMnSc1	Gurren-Lagann
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
Šikabane	Šikaban	k1gMnSc5	Šikaban
hime	himus	k1gMnSc5	himus
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
Hanamaru	Hanamar	k1gInSc2	Hanamar
jóčien	jóčien	k1gInSc1	jóčien
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
Panty	pant	k1gInPc1	pant
&	&	k?	&
Stocking	Stocking	k1gInSc1	Stocking
with	with	k1gMnSc1	with
Garterbelt	Garterbelt	k1gMnSc1	Garterbelt
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Dantalian	Dantalian	k1gInSc1	Dantalian
no	no	k9	no
šoka	šoka	k6eAd1	šoka
<g/>
'	'	kIx"	'
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Medaka	Medak	k1gMnSc2	Medak
Box	box	k1gInSc1	box
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
Medaka	Medak	k1gMnSc2	Medak
Box	box	k1gInSc1	box
Abnormal	Abnormal	k1gInSc1	Abnormal
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
Tokurei	Tokurei	k1gNnSc3	Tokurei
soči	soči	k1gNnSc3	soči
dantai	dantai	k6eAd1	dantai
Stella	Stella	k1gFnSc1	Stella
džo-gakuin	džoakuina	k1gFnPc2	džo-gakuina
kótó-ka	kótók	k1gInSc2	kótó-ek
C	C	kA	C
<g/>
3	[number]	k4	3
<g/>
-bu	u	k?	-bu
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
Mahó	Mahó	k1gFnSc1	Mahó
šódžo	šódžo	k1gMnSc1	šódžo
taisen	taisen	k1gInSc1	taisen
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
Hókago	Hókago	k6eAd1	Hókago
<g />
.	.	kIx.	.
</s>
<s>
no	no	k9	no
Pleiades	Pleiades	k1gInSc1	Pleiades
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
Óricu	Óricus	k1gInSc2	Óricus
učúgun	učúgun	k1gInSc1	učúgun
<g/>
:	:	kIx,	:
oneamisu	oneamis	k1gInSc2	oneamis
no	no	k9	no
cubasa	cubasa	k1gFnSc1	cubasa
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
Neon	neon	k1gInSc4	neon
Genesis	Genesis	k1gFnSc1	Genesis
Evangelion	Evangelion	k1gInSc1	Evangelion
<g/>
:	:	kIx,	:
Death	Death	k1gInSc1	Death
&	&	k?	&
Rebirth	Rebirth	k1gInSc1	Rebirth
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
End	End	k1gFnSc2	End
of	of	k?	of
Evangelion	Evangelion	k1gInSc1	Evangelion
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
Revival	revival	k1gInSc1	revival
of	of	k?	of
Evangelion	Evangelion	k1gInSc1	Evangelion
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
Cowboy	Cowboa	k1gFnSc2	Cowboa
<g />
.	.	kIx.	.
</s>
<s>
Bebop	bebop	k1gInSc1	bebop
the	the	k?	the
Movie	Movie	k1gFnSc1	Movie
<g/>
:	:	kIx,	:
Knockin	Knockin	k1gMnSc1	Knockin
<g/>
'	'	kIx"	'
on	on	k3xPp3gMnSc1	on
Heaven	Heaven	k2eAgMnSc1d1	Heaven
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Door	Doora	k1gFnPc2	Doora
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
Cutie	Cutie	k1gFnSc2	Cutie
Honey	Honea	k1gFnSc2	Honea
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
Gunbuster	Gunbuster	k1gInSc1	Gunbuster
&	&	k?	&
Diebuster	Diebuster	k1gInSc1	Diebuster
Movie	Movie	k1gFnSc1	Movie
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
Rebuild	Rebuild	k1gInSc1	Rebuild
of	of	k?	of
Evangelion	Evangelion	k1gInSc1	Evangelion
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
–	–	k?	–
dosud	dosud	k6eAd1	dosud
<g/>
)	)	kIx)	)
Tengen	Tengen	k1gInSc1	Tengen
toppa	toppa	k1gFnSc1	toppa
Gurren-Lagann	Gurren-Lagann	k1gMnSc1	Gurren-Lagann
<g/>
:	:	kIx,	:
Gurren-hen	Gurrenen	k1gInSc1	Gurren-hen
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
Tengen	Tengen	k1gInSc1	Tengen
toppa	toppa	k1gFnSc1	toppa
Gurren-Lagann	Gurren-Lagann	k1gMnSc1	Gurren-Lagann
<g/>
:	:	kIx,	:
Lagann-hen	Lagannen	k1gInSc1	Lagann-hen
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
Top	topit	k5eAaImRp2nS	topit
o	o	k7c6	o
Nerae	Nerae	k1gFnPc6	Nerae
<g/>
!	!	kIx.	!
</s>
<s>
Gunbuster	Gunbuster	k1gInSc1	Gunbuster
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
Otaku	Otak	k1gInSc2	Otak
no	no	k9	no
Video	video	k1gNnSc1	video
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
Re	re	k9	re
<g/>
:	:	kIx,	:
Cutie	Cutie	k1gFnSc1	Cutie
Honey	Honea	k1gFnSc2	Honea
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
Gainax	Gainax	k1gInSc1	Gainax
produkuje	produkovat	k5eAaImIp3nS	produkovat
také	také	k9	také
počítačové	počítačový	k2eAgFnPc4d1	počítačová
hry	hra	k1gFnPc4	hra
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
ně	on	k3xPp3gMnPc4	on
patří	patřit	k5eAaImIp3nS	patřit
svlékací	svlékací	k2eAgFnSc1d1	svlékací
hra	hra	k1gFnSc1	hra
mahjong	mahjonga	k1gFnPc2	mahjonga
s	s	k7c7	s
postavami	postava	k1gFnPc7	postava
z	z	k7c2	z
Evangelionu	Evangelion	k1gInSc2	Evangelion
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
slavná	slavný	k2eAgFnSc1d1	slavná
série	série	k1gFnSc1	série
Princess	Princessa	k1gFnPc2	Princessa
Maker	makro	k1gNnPc2	makro
<g/>
,	,	kIx,	,
vydávaná	vydávaný	k2eAgFnSc1d1	vydávaná
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
jako	jako	k9	jako
Daicon	Daicon	k1gMnSc1	Daicon
Films	Films	k1gInSc1	Films
<g/>
,	,	kIx,	,
Gainax	Gainax	k1gInSc1	Gainax
vytvářel	vytvářet	k5eAaImAgInS	vytvářet
série	série	k1gFnPc4	série
tzv.	tzv.	kA	tzv.
tokusacu	tokusaca	k1gFnSc4	tokusaca
fan	fana	k1gFnPc2	fana
filmů	film	k1gInPc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
krátké	krátký	k2eAgInPc1d1	krátký
filmy	film	k1gInPc1	film
<g/>
,	,	kIx,	,
vydávané	vydávaný	k2eAgNnSc1d1	vydávané
od	od	k7c2	od
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
parodovaly	parodovat	k5eAaImAgInP	parodovat
filmy	film	k1gInPc1	film
s	s	k7c7	s
monstry	monstrum	k1gNnPc7	monstrum
a	a	k8xC	a
superhrdiny	superhrdina	k1gFnSc2	superhrdina
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
ně	on	k3xPp3gInPc4	on
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
Patriotic	Patriotice	k1gFnPc2	Patriotice
Task	Taska	k1gFnPc2	Taska
Force	force	k1gFnSc2	force
Dai-Nippon	Dai-Nippon	k1gInSc1	Dai-Nippon
(	(	kIx(	(
<g/>
愛	愛	k?	愛
Aikoku	Aikok	k1gInSc2	Aikok
sentai	senta	k1gFnSc2	senta
dai-nippon	daiippona	k1gFnPc2	dai-nippona
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
–	–	k?	–
parodie	parodie	k1gFnSc2	parodie
oblíbených	oblíbený	k2eAgInPc2d1	oblíbený
Super	super	k1gInPc2	super
Sentai	Sentai	k1gNnSc2	Sentai
pořadů	pořad	k1gInPc2	pořad
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
byly	být	k5eAaImAgFnP	být
satirou	satira	k1gFnSc7	satira
rusko-japonské	ruskoaponský	k2eAgFnPc1d1	rusko-japonská
války	válka	k1gFnPc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
Hrdinové	Hrdina	k1gMnPc1	Hrdina
symbolizovali	symbolizovat	k5eAaImAgMnP	symbolizovat
Japonsko	Japonsko	k1gNnSc4	Japonsko
a	a	k8xC	a
zlé	zlý	k2eAgNnSc1d1	zlé
Impérium	impérium	k1gNnSc1	impérium
Rudého	rudý	k2eAgMnSc2d1	rudý
medvěda	medvěd	k1gMnSc2	medvěd
vedené	vedený	k2eAgFnSc2d1	vedená
"	"	kIx"	"
<g/>
Smrtí	smrtit	k5eAaImIp3nS	smrtit
Kremlu	Kreml	k1gInSc2	Kreml
<g/>
"	"	kIx"	"
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Return	Return	k1gMnSc1	Return
of	of	k?	of
Ultraman	Ultraman	k1gMnSc1	Ultraman
(	(	kIx(	(
<g/>
帰	帰	k?	帰
Kaettekita	Kaettekita	k1gMnSc1	Kaettekita
Urutoraman	Urutoraman	k1gMnSc1	Urutoraman
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
–	–	k?	–
stejnojmenná	stejnojmenný	k2eAgFnSc1d1	stejnojmenná
parodie	parodie	k1gFnSc1	parodie
filmu	film	k1gInSc3	film
Return	Return	k1gMnSc1	Return
of	of	k?	of
Ultraman	Ultraman	k1gMnSc1	Ultraman
s	s	k7c7	s
občasnými	občasný	k2eAgFnPc7d1	občasná
dobře	dobře	k6eAd1	dobře
provedenými	provedený	k2eAgInPc7d1	provedený
speciálními	speciální	k2eAgInPc7d1	speciální
efekty	efekt	k1gInPc7	efekt
i	i	k9	i
navzdory	navzdory	k7c3	navzdory
nízkému	nízký	k2eAgInSc3d1	nízký
rozpočtu	rozpočet	k1gInSc3	rozpočet
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Eight-Headed	Eight-Headed	k1gMnSc1	Eight-Headed
Giant	Giant	k1gMnSc1	Giant
Serpent	Serpent	k1gMnSc1	Serpent
Strikes	Strikes	k1gMnSc1	Strikes
Back	Back	k1gMnSc1	Back
(	(	kIx(	(
<g/>
八	八	k?	八
Jamata	Jamat	k2eAgFnSc1d1	Jamat
no	no	k9	no
oroči	oročit	k5eAaImRp2nS	oročit
no	no	k9	no
gjakušú	gjakušú	k?	gjakušú
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
–	–	k?	–
epická	epický	k2eAgFnSc1d1	epická
72	[number]	k4	72
<g/>
-minutová	inutový	k2eAgFnSc1d1	-minutový
parodie	parodie	k1gFnSc1	parodie
filmů	film	k1gInPc2	film
s	s	k7c7	s
obřími	obří	k2eAgFnPc7d1	obří
příšerami	příšera	k1gFnPc7	příšera
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
film	film	k1gInSc4	film
dělal	dělat	k5eAaImAgMnS	dělat
speciální	speciální	k2eAgInPc4d1	speciální
efekty	efekt	k1gInPc4	efekt
Šindži	Šindž	k1gFnSc3	Šindž
Higuči	Higuč	k1gFnSc3	Higuč
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
Kare	kar	k1gInSc5	kar
Kano	Kano	k1gMnSc1	Kano
animovaných	animovaný	k2eAgFnPc2d1	animovaná
scén	scéna	k1gFnPc2	scéna
a	a	k8xC	a
návrhů	návrh	k1gInPc2	návrh
oděvů	oděv	k1gInPc2	oděv
postav	postava	k1gFnPc2	postava
je	být	k5eAaImIp3nS	být
nápadně	nápadně	k6eAd1	nápadně
podobných	podobný	k2eAgFnPc2d1	podobná
Neon	neon	k1gInSc1	neon
Genesis	Genesis	k1gFnSc1	Genesis
Evangelionu	Evangelion	k1gInSc3	Evangelion
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
pár	pár	k4xCyI	pár
otevřených	otevřený	k2eAgFnPc2d1	otevřená
narážek	narážka	k1gFnPc2	narážka
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
scénka	scénka	k1gFnSc1	scénka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Mijazawa	Mijazawa	k1gFnSc1	Mijazawa
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
dílu	díl	k1gInSc6	díl
Kare	kar	k1gInSc5	kar
Kano	Kano	k1gNnSc4	Kano
ve	v	k7c6	v
žlutém	žlutý	k2eAgInSc6d1	žlutý
obleku	oblek	k1gInSc6	oblek
Asuky	Asuka	k1gFnSc2	Asuka
podlehne	podlehnout	k5eAaPmIp3nS	podlehnout
zběsilosti	zběsilost	k1gFnPc4	zběsilost
EVA-	EVA-	k1gFnSc2	EVA-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Fanoušci	Fanoušek	k1gMnPc1	Fanoušek
často	často	k6eAd1	často
upozorňují	upozorňovat	k5eAaImIp3nP	upozorňovat
na	na	k7c4	na
neobvykle	obvykle	k6eNd1	obvykle
silnou	silný	k2eAgFnSc4d1	silná
podobnost	podobnost	k1gFnSc4	podobnost
vzhledu	vzhled	k1gInSc2	vzhled
postav	postava	k1gFnPc2	postava
z	z	k7c2	z
Nadii	Nadie	k1gFnSc3	Nadie
a	a	k8xC	a
Evangelionu	Evangelion	k1gInSc3	Evangelion
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
fakt	fakt	k1gInSc1	fakt
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
ale	ale	k9	ale
odůvodnit	odůvodnit	k5eAaPmF	odůvodnit
stejným	stejný	k2eAgMnSc7d1	stejný
designérem	designér	k1gMnSc7	designér
Jošijukim	Jošijukim	k1gMnSc1	Jošijukim
Sadamotem	Sadamot	k1gMnSc7	Sadamot
<g/>
.	.	kIx.	.
</s>
<s>
Maskot	maskot	k1gInSc1	maskot
Gainaxu	Gainax	k1gInSc2	Gainax
je	být	k5eAaImIp3nS	být
superdeformací	superdeformací	k2eAgNnSc4d1	superdeformací
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Ricuky	Ricuk	k1gInPc1	Ricuk
Akagi	Akag	k1gFnSc2	Akag
z	z	k7c2	z
NGE	NGE	kA	NGE
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
epizodě	epizoda	k1gFnSc6	epizoda
He	he	k0	he
is	is	k?	is
My	my	k3xPp1nPc1	my
Master	master	k1gMnSc1	master
jsou	být	k5eAaImIp3nP	být
odkazy	odkaz	k1gInPc4	odkaz
na	na	k7c4	na
všechna	všechen	k3xTgNnPc4	všechen
díla	dílo	k1gNnPc4	dílo
Gainaxu	Gainax	k1gInSc2	Gainax
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
plyšáků	plyšáků	k?	plyšáků
<g/>
,	,	kIx,	,
oděvů	oděv	k1gInPc2	oděv
nebo	nebo	k8xC	nebo
chování	chování	k1gNnSc2	chování
postav	postava	k1gFnPc2	postava
<g/>
.	.	kIx.	.
</s>
<s>
Kontroverzní	kontroverzní	k2eAgInPc1d1	kontroverzní
konce	konec	k1gInPc1	konec
Gainaxu	Gainax	k1gInSc2	Gainax
jsou	být	k5eAaImIp3nP	být
přisuzovány	přisuzovat	k5eAaImNgFnP	přisuzovat
buď	buď	k8xC	buď
experimentálnímu	experimentální	k2eAgNnSc3d1	experimentální
vyprávění	vyprávění	k1gNnSc3	vyprávění
nebo	nebo	k8xC	nebo
nízkému	nízký	k2eAgInSc3d1	nízký
rozpočtu	rozpočet	k1gInSc3	rozpočet
<g/>
.	.	kIx.	.
</s>
<s>
Gainax	Gainax	k1gInSc1	Gainax
také	také	k9	také
pracoval	pracovat	k5eAaImAgInS	pracovat
na	na	k7c6	na
propagačním	propagační	k2eAgNnSc6d1	propagační
videu	video	k1gNnSc6	video
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
pro	pro	k7c4	pro
skladbu	skladba	k1gFnSc4	skladba
Marionette	Marionett	k1gInSc5	Marionett
japonské	japonský	k2eAgFnPc1d1	japonská
skupiny	skupina	k1gFnSc2	skupina
Boø	Boø	k1gFnSc2	Boø
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Gainax	Gainax	k1gInSc4	Gainax
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnSc2	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
GAINAX	GAINAX	kA	GAINAX
<g/>
.	.	kIx.	.
<g/>
NET	NET	kA	NET
–	–	k?	–
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
stránka	stránka	k1gFnSc1	stránka
studia	studio	k1gNnSc2	studio
Gainax	Gainax	k1gInSc4	Gainax
The	The	k1gFnSc2	The
Most	most	k1gInSc4	most
Holy	hola	k1gFnSc2	hola
Gainax	Gainax	k1gInSc1	Gainax
Cult	Cult	k1gMnSc1	Cult
–	–	k?	–
anglická	anglický	k2eAgNnPc4d1	anglické
fan	fana	k1gFnPc2	fana
stránka	stránka	k1gFnSc1	stránka
GAINAX	GAINAX	kA	GAINAX
<g/>
.	.	kIx.	.
<g/>
FR	fr	k0	fr
–	–	k?	–
francouzská	francouzský	k2eAgFnSc1d1	francouzská
fan	fana	k1gFnPc2	fana
stránka	stránka	k1gFnSc1	stránka
</s>
