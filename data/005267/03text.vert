<s>
Edikt	edikt	k1gInSc1	edikt
nantský	nantský	k2eAgInSc1d1	nantský
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc1	označení
ediktu	edikt	k1gInSc2	edikt
<g/>
,	,	kIx,	,
vydaného	vydaný	k2eAgNnSc2d1	vydané
13	[number]	k4	13
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1598	[number]	k4	1598
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgMnSc6	jenž
francouzský	francouzský	k2eAgMnSc1d1	francouzský
král	král	k1gMnSc1	král
Jindřich	Jindřich	k1gMnSc1	Jindřich
IV	IV	kA	IV
<g/>
.	.	kIx.	.
zaručoval	zaručovat	k5eAaImAgMnS	zaručovat
francouzským	francouzský	k2eAgMnSc7d1	francouzský
protestantům	protestant	k1gMnPc3	protestant
(	(	kIx(	(
<g/>
hugenotům	hugenot	k1gMnPc3	hugenot
<g/>
)	)	kIx)	)
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
stejná	stejný	k2eAgNnPc1d1	stejné
práva	právo	k1gNnPc1	právo
<g/>
,	,	kIx,	,
jakých	jaký	k3yRgInPc2	jaký
požívali	požívat	k5eAaImAgMnP	požívat
katolíci	katolík	k1gMnPc1	katolík
<g/>
.	.	kIx.	.
</s>
<s>
Edikt	edikt	k1gInSc1	edikt
měl	mít	k5eAaImAgInS	mít
především	především	k6eAd1	především
ukončit	ukončit	k5eAaPmF	ukončit
dlouhotrvající	dlouhotrvající	k2eAgFnSc2d1	dlouhotrvající
náboženské	náboženský	k2eAgFnSc2d1	náboženská
války	válka	k1gFnSc2	válka
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Jindřich	Jindřich	k1gMnSc1	Jindřich
IV	IV	kA	IV
<g/>
.	.	kIx.	.
měl	mít	k5eAaImAgMnS	mít
k	k	k7c3	k
vydání	vydání	k1gNnSc3	vydání
ediktu	edikt	k1gInSc2	edikt
i	i	k9	i
osobní	osobní	k2eAgInPc1d1	osobní
důvody	důvod	k1gInPc1	důvod
<g/>
;	;	kIx,	;
sám	sám	k3xTgInSc1	sám
byl	být	k5eAaImAgInS	být
totiž	totiž	k9	totiž
původním	původní	k2eAgNnSc7d1	původní
náboženstvím	náboženství	k1gNnSc7	náboženství
protestant	protestant	k1gMnSc1	protestant
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
zřekl	zřeknout	k5eAaPmAgMnS	zřeknout
své	svůj	k3xOyFgFnSc2	svůj
víry	víra	k1gFnSc2	víra
po	po	k7c6	po
bartolomějské	bartolomějský	k2eAgFnSc6d1	Bartolomějská
noci	noc	k1gFnSc6	noc
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
Karlem	Karel	k1gMnSc7	Karel
IX	IX	kA	IX
<g/>
.	.	kIx.	.
držen	držen	k2eAgMnSc1d1	držen
v	v	k7c6	v
Louvru	Louvre	k1gInSc6	Louvre
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1576	[number]	k4	1576
Jindřich	Jindřich	k1gMnSc1	Jindřich
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
ještě	ještě	k9	ještě
král	král	k1gMnSc1	král
navarrský	navarrský	k2eAgMnSc1d1	navarrský
<g/>
,	,	kIx,	,
uprchl	uprchnout	k5eAaPmAgMnS	uprchnout
z	z	k7c2	z
Paříže	Paříž	k1gFnSc2	Paříž
a	a	k8xC	a
vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
k	k	k7c3	k
protestantské	protestantský	k2eAgFnSc3d1	protestantská
víře	víra	k1gFnSc3	víra
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgMnS	moct
roku	rok	k1gInSc2	rok
1594	[number]	k4	1594
stát	stát	k5eAaPmF	stát
francouzským	francouzský	k2eAgMnSc7d1	francouzský
králem	král	k1gMnSc7	král
a	a	k8xC	a
přispět	přispět	k5eAaPmF	přispět
tak	tak	k6eAd1	tak
k	k	k7c3	k
ukončení	ukončení	k1gNnSc3	ukončení
náboženských	náboženský	k2eAgFnPc2d1	náboženská
válek	válka	k1gFnPc2	válka
<g/>
,	,	kIx,	,
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
změnit	změnit	k5eAaPmF	změnit
víru	víra	k1gFnSc4	víra
na	na	k7c4	na
katolickou	katolický	k2eAgFnSc4d1	katolická
a	a	k8xC	a
proslul	proslout	k5eAaPmAgMnS	proslout
výrokem	výrok	k1gInSc7	výrok
"	"	kIx"	"
<g/>
Paříž	Paříž	k1gFnSc1	Paříž
stojí	stát	k5eAaImIp3nS	stát
za	za	k7c4	za
mši	mše	k1gFnSc4	mše
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Edikt	edikt	k1gInSc1	edikt
nantský	nantský	k2eAgInSc1d1	nantský
pak	pak	k6eAd1	pak
po	po	k7c6	po
čtyřech	čtyři	k4xCgNnPc6	čtyři
letech	léto	k1gNnPc6	léto
skutečně	skutečně	k6eAd1	skutečně
nastolil	nastolit	k5eAaPmAgInS	nastolit
mír	mír	k1gInSc1	mír
a	a	k8xC	a
přispěl	přispět	k5eAaPmAgInS	přispět
k	k	k7c3	k
upevnění	upevnění	k1gNnSc3	upevnění
jednoty	jednota	k1gFnSc2	jednota
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
hugenotů	hugenot	k1gMnPc2	hugenot
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
ovšem	ovšem	k9	ovšem
spokojená	spokojený	k2eAgFnSc1d1	spokojená
nebyla	být	k5eNaImAgNnP	být
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jejich	jejich	k3xOp3gNnSc1	jejich
náboženství	náboženství	k1gNnSc1	náboženství
zůstávalo	zůstávat	k5eAaImAgNnS	zůstávat
oproti	oproti	k7c3	oproti
katolickému	katolický	k2eAgNnSc3d1	katolické
pořád	pořád	k6eAd1	pořád
méněcenné	méněcenný	k2eAgFnPc1d1	méněcenná
<g/>
.	.	kIx.	.
</s>
<s>
Protestantské	protestantský	k2eAgInPc1d1	protestantský
obřady	obřad	k1gInPc1	obřad
se	se	k3xPyFc4	se
mohly	moct	k5eAaImAgInP	moct
konat	konat	k5eAaImF	konat
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
určitých	určitý	k2eAgNnPc6d1	určité
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInPc1	jejich
chrámy	chrám	k1gInPc1	chrám
mohly	moct	k5eAaImAgInP	moct
být	být	k5eAaImF	být
stavěny	stavit	k5eAaImNgInP	stavit
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
předměstích	předměstí	k1gNnPc6	předměstí
katedrálních	katedrální	k2eAgNnPc2d1	katedrální
měst	město	k1gNnPc2	město
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
knihy	kniha	k1gFnPc1	kniha
byly	být	k5eAaImAgFnP	být
cenzurovány	cenzurovat	k5eAaImNgFnP	cenzurovat
<g/>
.	.	kIx.	.
</s>
<s>
Edikt	edikt	k1gInSc1	edikt
musely	muset	k5eAaImAgInP	muset
schválit	schválit	k5eAaPmF	schválit
regionální	regionální	k2eAgInPc1d1	regionální
parlamenty	parlament	k1gInPc1	parlament
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
trvalo	trvat	k5eAaImAgNnS	trvat
jedenáct	jedenáct	k4xCc4	jedenáct
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
jej	on	k3xPp3gMnSc4	on
schválil	schválit	k5eAaPmAgInS	schválit
roku	rok	k1gInSc2	rok
1609	[number]	k4	1609
parlament	parlament	k1gInSc1	parlament
v	v	k7c6	v
Rouenu	Rouen	k1gInSc6	Rouen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1685	[number]	k4	1685
Ludvík	Ludvík	k1gMnSc1	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
ediktem	edikt	k1gInSc7	edikt
z	z	k7c2	z
Fontainebleau	Fontainebleaus	k1gInSc2	Fontainebleaus
edikt	edikt	k1gInSc1	edikt
nantský	nantský	k2eAgInSc1d1	nantský
zrušil	zrušit	k5eAaPmAgInS	zrušit
a	a	k8xC	a
prohlásil	prohlásit	k5eAaPmAgInS	prohlásit
protestantské	protestantský	k2eAgNnSc4d1	protestantské
náboženství	náboženství	k1gNnSc4	náboženství
za	za	k7c4	za
ilegální	ilegální	k2eAgMnPc4d1	ilegální
<g/>
.	.	kIx.	.
</s>
<s>
Náboženské	náboženský	k2eAgFnPc1d1	náboženská
války	válka	k1gFnPc1	válka
proto	proto	k8xC	proto
sice	sice	k8xC	sice
nevzplanuly	vzplanout	k5eNaPmAgFnP	vzplanout
<g/>
,	,	kIx,	,
protestanti	protestant	k1gMnPc1	protestant
se	se	k3xPyFc4	se
však	však	k9	však
stali	stát	k5eAaPmAgMnP	stát
předmětem	předmět	k1gInSc7	předmět
perzekucí	perzekuce	k1gFnPc2	perzekuce
<g/>
,	,	kIx,	,
pronásledování	pronásledování	k1gNnSc2	pronásledování
a	a	k8xC	a
teroru	teror	k1gInSc2	teror
<g/>
;	;	kIx,	;
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
mnozí	mnohý	k2eAgMnPc1d1	mnohý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
Francii	Francie	k1gFnSc3	Francie
opustit	opustit	k5eAaPmF	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Odcházeli	odcházet	k5eAaImAgMnP	odcházet
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
Holandska	Holandsko	k1gNnSc2	Holandsko
i	i	k9	i
za	za	k7c4	za
moře	moře	k1gNnSc4	moře
<g/>
.	.	kIx.	.
</s>
<s>
Francie	Francie	k1gFnSc1	Francie
tak	tak	k6eAd1	tak
přišla	přijít	k5eAaPmAgFnS	přijít
o	o	k7c4	o
mnoho	mnoho	k4c4	mnoho
schopných	schopný	k2eAgMnPc2d1	schopný
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
Ludvík	Ludvík	k1gMnSc1	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
ideologickým	ideologický	k2eAgMnSc7d1	ideologický
nepřítelem	nepřítel	k1gMnSc7	nepřítel
protestantské	protestantský	k2eAgFnSc2d1	protestantská
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Edikt	edikt	k1gInSc1	edikt
se	se	k3xPyFc4	se
skládal	skládat	k5eAaImAgInS	skládat
ze	z	k7c2	z
čtyř	čtyři	k4xCgFnPc2	čtyři
listin	listina	k1gFnPc2	listina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
obsahovaly	obsahovat	k5eAaImAgFnP	obsahovat
92	[number]	k4	92
obecných	obecná	k1gFnPc2	obecná
a	a	k8xC	a
52	[number]	k4	52
tajných	tajný	k2eAgInPc2d1	tajný
článků	článek	k1gInPc2	článek
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
ediktu	edikt	k1gInSc2	edikt
mohli	moct	k5eAaImAgMnP	moct
hugenoti	hugenot	k1gMnPc1	hugenot
sloužit	sloužit	k5eAaImF	sloužit
své	svůj	k3xOyFgFnPc4	svůj
bohoslužby	bohoslužba	k1gFnPc4	bohoslužba
jednak	jednak	k8xC	jednak
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hugenotští	hugenotský	k2eAgMnPc1d1	hugenotský
šlechtici	šlechtic	k1gMnPc1	šlechtic
disponovali	disponovat	k5eAaBmAgMnP	disponovat
soudním	soudní	k2eAgNnSc7d1	soudní
právem	právo	k1gNnSc7	právo
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
lokalitách	lokalita	k1gFnPc6	lokalita
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
za	za	k7c7	za
tímto	tento	k3xDgInSc7	tento
účelem	účel	k1gInSc7	účel
vybrány	vybrat	k5eAaPmNgFnP	vybrat
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
rychtářství	rychtářství	k1gNnSc6	rychtářství
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
bailliage	bailliage	k6eAd1	bailliage
<g/>
)	)	kIx)	)
královskými	královský	k2eAgMnPc7d1	královský
komisaři	komisař	k1gMnPc7	komisař
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
všude	všude	k6eAd1	všude
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hugenoti	hugenot	k1gMnPc1	hugenot
vyznávali	vyznávat	k5eAaImAgMnP	vyznávat
svou	svůj	k3xOyFgFnSc4	svůj
víru	víra	k1gFnSc4	víra
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1597	[number]	k4	1597
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
za	za	k7c2	za
předpokladu	předpoklad	k1gInSc2	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
to	ten	k3xDgNnSc1	ten
podařilo	podařit	k5eAaPmAgNnS	podařit
dokázat	dokázat	k5eAaPmF	dokázat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
ediktu	edikt	k1gInSc2	edikt
mohli	moct	k5eAaImAgMnP	moct
hugenoti	hugenot	k1gMnPc1	hugenot
získat	získat	k5eAaPmF	získat
libovolný	libovolný	k2eAgInSc4d1	libovolný
státní	státní	k2eAgInSc4d1	státní
úřad	úřad	k1gInSc4	úřad
či	či	k8xC	či
povolání	povolání	k1gNnSc4	povolání
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
jim	on	k3xPp3gMnPc3	on
také	také	k9	také
umožněno	umožněn	k2eAgNnSc1d1	umožněno
studovat	studovat	k5eAaImF	studovat
na	na	k7c6	na
všech	všecek	k3xTgFnPc6	všecek
školách	škola	k1gFnPc6	škola
a	a	k8xC	a
univerzitách	univerzita	k1gFnPc6	univerzita
a	a	k8xC	a
léčit	léčit	k5eAaImF	léčit
se	se	k3xPyFc4	se
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
špitálech	špitál	k1gInPc6	špitál
<g/>
.	.	kIx.	.
</s>
<s>
Případné	případný	k2eAgInPc1d1	případný
právní	právní	k2eAgInPc1d1	právní
spory	spor	k1gInPc1	spor
mezi	mezi	k7c7	mezi
katolíky	katolík	k1gMnPc7	katolík
a	a	k8xC	a
protestanty	protestant	k1gMnPc7	protestant
měly	mít	k5eAaImAgFnP	mít
řešit	řešit	k5eAaImF	řešit
speciální	speciální	k2eAgInPc4d1	speciální
tribunály	tribunál	k1gInPc4	tribunál
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
by	by	kYmCp3nP	by
zasedali	zasedat	k5eAaImAgMnP	zasedat
jak	jak	k6eAd1	jak
hugenoti	hugenot	k1gMnPc1	hugenot
<g/>
,	,	kIx,	,
tak	tak	k9	tak
katolíci	katolík	k1gMnPc1	katolík
<g/>
.	.	kIx.	.
</s>
<s>
Edikt	edikt	k1gInSc4	edikt
také	také	k6eAd1	také
garantoval	garantovat	k5eAaBmAgMnS	garantovat
protestantům	protestant	k1gMnPc3	protestant
určitou	určitý	k2eAgFnSc4d1	určitá
míru	míra	k1gFnSc4	míra
politické	politický	k2eAgFnSc2d1	politická
a	a	k8xC	a
vojenské	vojenský	k2eAgFnSc2d1	vojenská
nezávislosti	nezávislost	k1gFnSc2	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
Mohli	moct	k5eAaImAgMnP	moct
udržovat	udržovat	k5eAaImF	udržovat
vojenské	vojenský	k2eAgFnPc4d1	vojenská
posádky	posádka	k1gFnPc4	posádka
přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
padesáti	padesát	k4xCc6	padesát
svých	svůj	k3xOyFgNnPc6	svůj
opevněných	opevněný	k2eAgNnPc6d1	opevněné
městech	město	k1gNnPc6	město
a	a	k8xC	a
230	[number]	k4	230
dalších	další	k2eAgFnPc6d1	další
pevnostech	pevnost	k1gFnPc6	pevnost
<g/>
.	.	kIx.	.
</s>
