<s>
Edikt	edikt	k1gInSc1	edikt
nantský	nantský	k2eAgInSc1d1	nantský
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc1	označení
ediktu	edikt	k1gInSc2	edikt
<g/>
,	,	kIx,	,
vydaného	vydaný	k2eAgNnSc2d1	vydané
13	[number]	k4	13
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1598	[number]	k4	1598
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgMnSc6	jenž
francouzský	francouzský	k2eAgMnSc1d1	francouzský
král	král	k1gMnSc1	král
Jindřich	Jindřich	k1gMnSc1	Jindřich
IV	IV	kA	IV
<g/>
.	.	kIx.	.
zaručoval	zaručovat	k5eAaImAgMnS	zaručovat
francouzským	francouzský	k2eAgMnSc7d1	francouzský
protestantům	protestant	k1gMnPc3	protestant
(	(	kIx(	(
<g/>
hugenotům	hugenot	k1gMnPc3	hugenot
<g/>
)	)	kIx)	)
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
stejná	stejný	k2eAgNnPc1d1	stejné
práva	právo	k1gNnPc1	právo
<g/>
,	,	kIx,	,
jakých	jaký	k3yIgInPc2	jaký
požívali	požívat	k5eAaImAgMnP	požívat
katolíci	katolík	k1gMnPc1	katolík
<g/>
.	.	kIx.	.
</s>
