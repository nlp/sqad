<s>
Holokaust	holokaust	k1gInSc1	holokaust
resp.	resp.	kA	resp.
holocaust	holocaust	k1gInSc4	holocaust
(	(	kIx(	(
<g/>
z	z	k7c2	z
řeckého	řecký	k2eAgInSc2d1	řecký
holokauston	holokauston	k1gInSc4	holokauston
<g/>
,	,	kIx,	,
celopal	celopat	k5eAaPmAgMnS	celopat
a	a	k8xC	a
anglického	anglický	k2eAgMnSc2d1	anglický
holocaust	holocaust	k1gInSc4	holocaust
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
nacistická	nacistický	k2eAgFnSc1d1	nacistická
politika	politika	k1gFnSc1	politika
systematického	systematický	k2eAgInSc2d1	systematický
<g/>
,	,	kIx,	,
státem	stát	k1gInSc7	stát
provozovaného	provozovaný	k2eAgNnSc2d1	provozované
pronásledování	pronásledování	k1gNnSc2	pronásledování
a	a	k8xC	a
hromadného	hromadný	k2eAgNnSc2d1	hromadné
vyvražďování	vyvražďování	k1gNnSc2	vyvražďování
<g/>
,	,	kIx,	,
především	především	k9	především
osob	osoba	k1gFnPc2	osoba
židovské	židovský	k2eAgFnSc2d1	židovská
národnosti	národnost	k1gFnSc2	národnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
širším	široký	k2eAgInSc6d2	širší
smyslu	smysl	k1gInSc6	smysl
je	být	k5eAaImIp3nS	být
slovem	slovem	k6eAd1	slovem
holokaust	holokaust	k1gInSc1	holokaust
označován	označovat	k5eAaImNgInS	označovat
i	i	k8xC	i
celý	celý	k2eAgInSc1d1	celý
nacistický	nacistický	k2eAgInSc1d1	nacistický
perzekuční	perzekuční	k2eAgInSc1d1	perzekuční
systém	systém	k1gInSc1	systém
věznic	věznice	k1gFnPc2	věznice
<g/>
,	,	kIx,	,
káznic	káznice	k1gFnPc2	káznice
a	a	k8xC	a
táborů	tábor	k1gInPc2	tábor
jako	jako	k8xC	jako
celek	celek	k1gInSc4	celek
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
nacistická	nacistický	k2eAgFnSc1d1	nacistická
perzekuce	perzekuce	k1gFnSc1	perzekuce
všech	všecek	k3xTgFnPc2	všecek
etnických	etnický	k2eAgFnPc2d1	etnická
<g/>
,	,	kIx,	,
náboženských	náboženský	k2eAgFnPc2d1	náboženská
a	a	k8xC	a
politických	politický	k2eAgFnPc2d1	politická
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Tj.	tj.	kA	tj.
(	(	kIx(	(
<g/>
chronologicky	chronologicky	k6eAd1	chronologicky
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
politických	politický	k2eAgMnPc2d1	politický
odpůrců	odpůrce	k1gMnPc2	odpůrce
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
komunistů	komunista	k1gMnPc2	komunista
a	a	k8xC	a
sociálních	sociální	k2eAgMnPc2d1	sociální
demokratů	demokrat	k1gMnPc2	demokrat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Svědků	svědek	k1gMnPc2	svědek
Jehovových	Jehovův	k2eAgInPc2d1	Jehovův
<g/>
,	,	kIx,	,
tělesně	tělesně	k6eAd1	tělesně
či	či	k8xC	či
mentálně	mentálně	k6eAd1	mentálně
postižených	postižený	k1gMnPc2	postižený
<g/>
,	,	kIx,	,
homosexuálů	homosexuál	k1gMnPc2	homosexuál
<g/>
,	,	kIx,	,
Židů	Žid	k1gMnPc2	Žid
<g/>
,	,	kIx,	,
Romů	Rom	k1gMnPc2	Rom
<g/>
,	,	kIx,	,
Poláků	Polák	k1gMnPc2	Polák
a	a	k8xC	a
občanů	občan	k1gMnPc2	občan
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
genocidy	genocida	k1gFnSc2	genocida
namířené	namířený	k2eAgFnPc1d1	namířená
na	na	k7c4	na
židovské	židovský	k2eAgNnSc4d1	Židovské
etnikum	etnikum	k1gNnSc4	etnikum
bylo	být	k5eAaImAgNnS	být
vyvražděno	vyvraždit	k5eAaPmNgNnS	vyvraždit
kolem	kolem	k7c2	kolem
6	[number]	k4	6
milionů	milion	k4xCgInPc2	milion
Židů	Žid	k1gMnPc2	Žid
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
pod	pod	k7c7	pod
pojmem	pojem	k1gInSc7	pojem
holokaust	holokaust	k1gInSc4	holokaust
chápeme	chápat	k5eAaImIp1nP	chápat
nacistickou	nacistický	k2eAgFnSc4d1	nacistická
perzekuci	perzekuce	k1gFnSc4	perzekuce
jako	jako	k8xS	jako
celek	celek	k1gInSc4	celek
<g/>
,	,	kIx,	,
hovoříme	hovořit	k5eAaImIp1nP	hovořit
o	o	k7c6	o
počtu	počet	k1gInSc6	počet
obětí	oběť	k1gFnPc2	oběť
holokaustu	holokaust	k1gInSc2	holokaust
mezi	mezi	k7c7	mezi
11	[number]	k4	11
až	až	k8xS	až
17	[number]	k4	17
miliony	milion	k4xCgInPc4	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českých	český	k2eAgInPc6d1	český
textech	text	k1gInPc6	text
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
setkat	setkat	k5eAaPmF	setkat
jak	jak	k8xS	jak
s	s	k7c7	s
variantou	varianta	k1gFnSc7	varianta
holokaust	holokaust	k1gInSc1	holokaust
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
s	s	k7c7	s
původní	původní	k2eAgFnSc7d1	původní
anglickou	anglický	k2eAgFnSc7d1	anglická
odvozeninou	odvozenina	k1gFnSc7	odvozenina
holocaust	holocaust	k1gInSc1	holocaust
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
varianty	varianta	k1gFnPc1	varianta
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
rovnocenné	rovnocenný	k2eAgInPc1d1	rovnocenný
<g/>
.	.	kIx.	.
</s>
<s>
Anglický	anglický	k2eAgInSc1d1	anglický
termín	termín	k1gInSc1	termín
holocaust	holocaust	k1gInSc1	holocaust
(	(	kIx(	(
<g/>
naprosté	naprostý	k2eAgNnSc1d1	naprosté
zničení	zničení	k1gNnSc1	zničení
<g/>
,	,	kIx,	,
úplná	úplný	k2eAgFnSc1d1	úplná
katastrofa	katastrofa	k1gFnSc1	katastrofa
<g/>
,	,	kIx,	,
masakr	masakr	k1gInSc1	masakr
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
z	z	k7c2	z
řeckého	řecký	k2eAgNnSc2d1	řecké
slova	slovo	k1gNnSc2	slovo
holokauston	holokauston	k1gInSc1	holokauston
(	(	kIx(	(
<g/>
zápalná	zápalný	k2eAgFnSc1d1	zápalná
oběť	oběť	k1gFnSc1	oběť
<g/>
,	,	kIx,	,
přeneseně	přeneseně	k6eAd1	přeneseně
zničení	zničení	k1gNnSc4	zničení
ohněm	oheň	k1gInSc7	oheň
nebo	nebo	k8xC	nebo
úplné	úplný	k2eAgNnSc1d1	úplné
zničení	zničení	k1gNnSc1	zničení
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
celopal	celopat	k5eAaImAgMnS	celopat
(	(	kIx(	(
<g/>
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
náboženskou	náboženský	k2eAgFnSc4d1	náboženská
obětinu	obětina	k1gFnSc4	obětina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
celá	celá	k1gFnSc1	celá
spálí	spálit	k5eAaPmIp3nS	spálit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgInSc1d1	moderní
anglický	anglický	k2eAgInSc1d1	anglický
termín	termín	k1gInSc1	termín
holocaust	holocaust	k1gInSc4	holocaust
však	však	k9	však
už	už	k6eAd1	už
(	(	kIx(	(
<g/>
po	po	k7c6	po
obsahové	obsahový	k2eAgFnSc6d1	obsahová
stránce	stránka	k1gFnSc6	stránka
<g/>
)	)	kIx)	)
nemá	mít	k5eNaImIp3nS	mít
s	s	k7c7	s
původním	původní	k2eAgInSc7d1	původní
náboženským	náboženský	k2eAgInSc7d1	náboženský
termínem	termín	k1gInSc7	termín
prakticky	prakticky	k6eAd1	prakticky
nic	nic	k3yNnSc1	nic
společného	společný	k2eAgNnSc2d1	společné
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
anglické	anglický	k2eAgNnSc1d1	anglické
slovo	slovo	k1gNnSc1	slovo
holocaust	holocaust	k1gInSc1	holocaust
v	v	k7c6	v
anglofonním	anglofonní	k2eAgNnSc6d1	anglofonní
prostředí	prostředí	k1gNnSc6	prostředí
používáno	používat	k5eAaImNgNnS	používat
pro	pro	k7c4	pro
označení	označení	k1gNnSc4	označení
prakticky	prakticky	k6eAd1	prakticky
jakékoli	jakýkoli	k3yIgFnSc2	jakýkoli
katastrofy	katastrofa	k1gFnSc2	katastrofa
<g/>
.	.	kIx.	.
</s>
<s>
Uvádí	uvádět	k5eAaImIp3nS	uvádět
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
patrně	patrně	k6eAd1	patrně
první	první	k4xOgMnSc1	první
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
použil	použít	k5eAaPmAgMnS	použít
slovo	slovo	k1gNnSc4	slovo
holocaust	holocaust	k1gInSc4	holocaust
pro	pro	k7c4	pro
nacistické	nacistický	k2eAgNnSc4d1	nacistické
vyhlazování	vyhlazování	k1gNnSc4	vyhlazování
Židů	Žid	k1gMnPc2	Žid
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
míru	míra	k1gFnSc4	míra
Elie	Elie	k1gFnSc1	Elie
Wiesel	Wiesel	k1gMnSc1	Wiesel
(	(	kIx(	(
<g/>
*	*	kIx~	*
1928	[number]	k4	1928
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
románu	román	k1gInSc6	román
Noc	noc	k1gFnSc1	noc
(	(	kIx(	(
<g/>
La	la	k1gNnSc1	la
Nuit	Nuita	k1gFnPc2	Nuita
<g/>
,	,	kIx,	,
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
"	"	kIx"	"
<g/>
zničení	zničení	k1gNnSc2	zničení
ohněm	oheň	k1gInSc7	oheň
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
až	až	k9	až
do	do	k7c2	do
vzniku	vznik	k1gInSc2	vznik
amerického	americký	k2eAgInSc2d1	americký
televizního	televizní	k2eAgInSc2d1	televizní
seriálu	seriál	k1gInSc2	seriál
Holocaust	holocaust	k1gInSc1	holocaust
(	(	kIx(	(
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
zůstával	zůstávat	k5eAaImAgInS	zůstávat
tento	tento	k3xDgInSc1	tento
termín	termín	k1gInSc1	termín
prakticky	prakticky	k6eAd1	prakticky
neznámý	známý	k2eNgMnSc1d1	neznámý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
je	být	k5eAaImIp3nS	být
situace	situace	k1gFnSc1	situace
taková	takový	k3xDgFnSc1	takový
<g/>
,	,	kIx,	,
že	že	k8xS	že
například	například	k6eAd1	například
izraelský	izraelský	k2eAgInSc1d1	izraelský
státní	státní	k2eAgInSc1d1	státní
ústav	ústav	k1gInSc1	ústav
Jad	Jad	k1gFnSc2	Jad
vašem	váš	k3xOp2gInSc6	váš
a	a	k8xC	a
řada	řada	k1gFnSc1	řada
židovských	židovský	k2eAgMnPc2d1	židovský
historiků	historik	k1gMnPc2	historik
vztahují	vztahovat	k5eAaImIp3nP	vztahovat
pojem	pojem	k1gInSc4	pojem
holocaust	holocaust	k1gInSc4	holocaust
především	především	k9	především
na	na	k7c4	na
vyvražďování	vyvražďování	k1gNnSc4	vyvražďování
Židů	Žid	k1gMnPc2	Žid
nebo	nebo	k8xC	nebo
jiných	jiný	k2eAgFnPc2d1	jiná
skupin	skupina	k1gFnPc2	skupina
označených	označený	k2eAgFnPc2d1	označená
jako	jako	k8xS	jako
rasově	rasově	k6eAd1	rasově
méněcenné	méněcenný	k2eAgNnSc1d1	méněcenné
(	(	kIx(	(
<g/>
Romové	Rom	k1gMnPc1	Rom
<g/>
)	)	kIx)	)
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Obecný	obecný	k2eAgInSc1d1	obecný
konsensus	konsensus	k1gInSc1	konsensus
nad	nad	k7c7	nad
rozsahem	rozsah	k1gInSc7	rozsah
pojmu	pojem	k1gInSc2	pojem
holokaust	holokaust	k1gInSc4	holokaust
však	však	k9	však
neexistuje	existovat	k5eNaImIp3nS	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
Židé	Žid	k1gMnPc1	Žid
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
důvodů	důvod	k1gInPc2	důvod
preferují	preferovat	k5eAaImIp3nP	preferovat
pro	pro	k7c4	pro
genocidu	genocida	k1gFnSc4	genocida
svého	svůj	k3xOyFgInSc2	svůj
národa	národ	k1gInSc2	národ
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
hebrejský	hebrejský	k2eAgInSc1d1	hebrejský
výraz	výraz	k1gInSc1	výraz
šoa	šoa	k?	šoa
(	(	kIx(	(
<g/>
hebrejsky	hebrejsky	k6eAd1	hebrejsky
ה	ה	k?	ה
zničení	zničení	k1gNnSc1	zničení
<g/>
,	,	kIx,	,
záhuba	záhuba	k1gFnSc1	záhuba
<g/>
,	,	kIx,	,
zmar	zmar	k1gInSc1	zmar
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
nejasné	jasný	k2eNgNnSc4d1	nejasné
vymezení	vymezení	k1gNnSc4	vymezení
tohoto	tento	k3xDgInSc2	tento
pojmu	pojem	k1gInSc2	pojem
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
setkat	setkat	k5eAaPmF	setkat
i	i	k9	i
s	s	k7c7	s
řadou	řada	k1gFnSc7	řada
jeho	jeho	k3xOp3gFnPc2	jeho
dalších	další	k2eAgFnPc2d1	další
aplikací	aplikace	k1gFnPc2	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
užíván	užíván	k2eAgInSc1d1	užíván
k	k	k7c3	k
označení	označení	k1gNnSc3	označení
pro	pro	k7c4	pro
jakoukoliv	jakýkoliv	k3yIgFnSc4	jakýkoliv
genocidu	genocida	k1gFnSc4	genocida
či	či	k8xC	či
masakr	masakr	k1gInSc4	masakr
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
lokalitu	lokalita	k1gFnSc4	lokalita
a	a	k8xC	a
konkrétní	konkrétní	k2eAgFnSc4d1	konkrétní
událost	událost	k1gFnSc4	událost
–	–	k?	–
například	například	k6eAd1	například
pro	pro	k7c4	pro
etnické	etnický	k2eAgFnPc4d1	etnická
čistky	čistka	k1gFnPc4	čistka
ve	v	k7c6	v
Rwandě	Rwanda	k1gFnSc6	Rwanda
či	či	k8xC	či
genocidu	genocida	k1gFnSc4	genocida
Arménů	Armén	k1gMnPc2	Armén
v	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
po	po	k7c4	po
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Fidel	Fidel	k1gMnSc1	Fidel
Castro	Castro	k1gNnSc4	Castro
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
rasový	rasový	k2eAgInSc4d1	rasový
holocaust	holocaust	k1gInSc4	holocaust
vyhošťování	vyhošťování	k1gNnSc2	vyhošťování
Romů	Rom	k1gMnPc2	Rom
bez	bez	k7c2	bez
povolení	povolení	k1gNnSc2	povolení
k	k	k7c3	k
pobytu	pobyt	k1gInSc3	pobyt
z	z	k7c2	z
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Vyvražďování	vyvražďování	k1gNnSc1	vyvražďování
Romů	Rom	k1gMnPc2	Rom
během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
zainteresovaní	zainteresovaný	k2eAgMnPc1d1	zainteresovaný
badatelé	badatel	k1gMnPc1	badatel
někdy	někdy	k6eAd1	někdy
nazývají	nazývat	k5eAaImIp3nP	nazývat
slovem	slovo	k1gNnSc7	slovo
porajmos	porajmos	k1gInSc1	porajmos
<g/>
,	,	kIx,	,
doslova	doslova	k6eAd1	doslova
pohlcení	pohlcený	k2eAgMnPc1d1	pohlcený
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
obětí	oběť	k1gFnSc7	oběť
holokaustu	holokaust	k1gInSc2	holokaust
byli	být	k5eAaImAgMnP	být
Židé	Žid	k1gMnPc1	Žid
<g/>
.	.	kIx.	.
</s>
<s>
Holokaust	holokaust	k1gInSc1	holokaust
byl	být	k5eAaImAgInS	být
praktickou	praktický	k2eAgFnSc7d1	praktická
aplikací	aplikace	k1gFnSc7	aplikace
"	"	kIx"	"
<g/>
konečného	konečný	k2eAgNnSc2d1	konečné
řešení	řešení	k1gNnSc2	řešení
židovské	židovský	k2eAgFnSc2d1	židovská
otázky	otázka	k1gFnSc2	otázka
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Endlösung	Endlösung	k1gInSc1	Endlösung
der	drát	k5eAaImRp2nS	drát
Judenfrage	Judenfragus	k1gMnSc5	Judenfragus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterým	který	k3yIgMnPc3	který
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
úplné	úplný	k2eAgNnSc1d1	úplné
vyvraždění	vyvraždění	k1gNnSc1	vyvraždění
židovské	židovský	k2eAgFnSc2d1	židovská
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
udávaný	udávaný	k2eAgInSc1d1	udávaný
počet	počet	k1gInSc1	počet
židovských	židovský	k2eAgFnPc2d1	židovská
obětí	oběť	k1gFnPc2	oběť
je	být	k5eAaImIp3nS	být
6	[number]	k4	6
miliónů	milión	k4xCgInPc2	milión
<g/>
,	,	kIx,	,
Raul	Raul	k1gMnSc1	Raul
Hilberg	Hilberg	k1gMnSc1	Hilberg
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
The	The	k1gFnSc2	The
Destruction	Destruction	k1gInSc1	Destruction
of	of	k?	of
European	European	k1gInSc1	European
Jews	Jewsa	k1gFnPc2	Jewsa
uvádí	uvádět	k5eAaImIp3nS	uvádět
číslo	číslo	k1gNnSc1	číslo
5,2	[number]	k4	5,2
miliónu	milión	k4xCgInSc2	milión
<g/>
.	.	kIx.	.
</s>
<s>
Vyvražděny	vyvražděn	k2eAgFnPc1d1	vyvražděna
byly	být	k5eAaImAgFnP	být
zhruba	zhruba	k6eAd1	zhruba
dvě	dva	k4xCgFnPc4	dva
třetiny	třetina	k1gFnPc1	třetina
evropských	evropský	k2eAgMnPc2d1	evropský
Židů	Žid	k1gMnPc2	Žid
<g/>
,	,	kIx,	,
většinu	většina	k1gFnSc4	většina
obětí	oběť	k1gFnPc2	oběť
tvořili	tvořit	k5eAaImAgMnP	tvořit
Židé	Žid	k1gMnPc1	Žid
z	z	k7c2	z
Polska	Polsko	k1gNnSc2	Polsko
(	(	kIx(	(
<g/>
3	[number]	k4	3
milióny	milión	k4xCgInPc1	milión
mrtvých	mrtvý	k1gMnPc2	mrtvý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Údaje	údaj	k1gInPc1	údaj
jsou	být	k5eAaImIp3nP	být
neustále	neustále	k6eAd1	neustále
zpřesňovány	zpřesňován	k2eAgFnPc1d1	zpřesňována
a	a	k8xC	a
doplňovány	doplňován	k2eAgFnPc1d1	doplňována
<g/>
,	,	kIx,	,
také	také	k9	také
díky	díky	k7c3	díky
informacím	informace	k1gFnPc3	informace
odtajněným	odtajněný	k2eAgFnPc3d1	odtajněná
a	a	k8xC	a
zpřístupněným	zpřístupněný	k2eAgFnPc3d1	zpřístupněná
až	až	k8xS	až
po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
SSSR	SSSR	kA	SSSR
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Generalplan	Generalplan	k1gInSc4	Generalplan
Ost	Ost	k1gFnPc2	Ost
a	a	k8xC	a
Německé	německý	k2eAgInPc4d1	německý
zločiny	zločin	k1gInPc4	zločin
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
Hitlerových	Hitlerových	k2eAgInPc2d1	Hitlerových
plánů	plán	k1gInPc2	plán
bylo	být	k5eAaImAgNnS	být
vytvoření	vytvoření	k1gNnSc1	vytvoření
německého	německý	k2eAgInSc2d1	německý
životního	životní	k2eAgInSc2d1	životní
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
plán	plán	k1gInSc1	plán
počítal	počítat	k5eAaImAgInS	počítat
v	v	k7c6	v
řádu	řád	k1gInSc6	řád
20	[number]	k4	20
až	až	k9	až
30	[number]	k4	30
let	léto	k1gNnPc2	léto
s	s	k7c7	s
odsunutím	odsunutí	k1gNnSc7	odsunutí
<g/>
,	,	kIx,	,
vyvražděním	vyvraždění	k1gNnSc7	vyvraždění
a	a	k8xC	a
zotročením	zotročení	k1gNnSc7	zotročení
většiny	většina	k1gFnSc2	většina
východních	východní	k2eAgInPc2d1	východní
a	a	k8xC	a
západních	západní	k2eAgInPc2d1	západní
Slovanů	Slovan	k1gInPc2	Slovan
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
němečtí	německý	k2eAgMnPc1d1	německý
nacisté	nacista	k1gMnPc1	nacista
považovali	považovat	k5eAaImAgMnP	považovat
za	za	k7c4	za
podlidi	podčlověk	k1gMnPc4	podčlověk
<g/>
.	.	kIx.	.
</s>
<s>
Nacistický	nacistický	k2eAgInSc1d1	nacistický
plán	plán	k1gInSc1	plán
genocidy	genocida	k1gFnSc2	genocida
Generalplan	Generalplan	k1gMnSc1	Generalplan
Ost	Ost	k1gMnSc1	Ost
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	s	k7c7	s
souhlasem	souhlas	k1gInSc7	souhlas
Hitlera	Hitler	k1gMnSc2	Hitler
vypracoval	vypracovat	k5eAaPmAgMnS	vypracovat
říšský	říšský	k2eAgMnSc1d1	říšský
vedoucí	vedoucí	k1gMnSc1	vedoucí
SS	SS	kA	SS
Heinrich	Heinrich	k1gMnSc1	Heinrich
Himmler	Himmler	k1gMnSc1	Himmler
<g/>
,	,	kIx,	,
počítal	počítat	k5eAaImAgInS	počítat
po	po	k7c6	po
vítězné	vítězný	k2eAgFnSc6d1	vítězná
válce	válka	k1gFnSc6	válka
proti	proti	k7c3	proti
SSSR	SSSR	kA	SSSR
s	s	k7c7	s
vystěhováním	vystěhování	k1gNnSc7	vystěhování
na	na	k7c4	na
východ	východ	k1gInSc4	východ
za	za	k7c4	za
Ural	Ural	k1gInSc4	Ural
nebo	nebo	k8xC	nebo
s	s	k7c7	s
fyzickou	fyzický	k2eAgFnSc7d1	fyzická
likvidací	likvidace	k1gFnSc7	likvidace
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
Poláků	Polák	k1gMnPc2	Polák
<g/>
,	,	kIx,	,
Čechů	Čech	k1gMnPc2	Čech
<g/>
,	,	kIx,	,
Ukrajinců	Ukrajinec	k1gMnPc2	Ukrajinec
<g/>
,	,	kIx,	,
Bělorusů	Bělorus	k1gMnPc2	Bělorus
a	a	k8xC	a
Rusů	Rus	k1gMnPc2	Rus
z	z	k7c2	z
evropské	evropský	k2eAgFnSc2d1	Evropská
části	část	k1gFnSc2	část
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Slované	Slovan	k1gMnPc1	Slovan
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
by	by	kYmCp3nP	by
zůstali	zůstat	k5eAaPmAgMnP	zůstat
v	v	k7c6	v
"	"	kIx"	"
<g/>
německém	německý	k2eAgInSc6d1	německý
životním	životní	k2eAgInSc6d1	životní
prostoru	prostor	k1gInSc6	prostor
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
by	by	kYmCp3nP	by
podle	podle	k7c2	podle
nacistických	nacistický	k2eAgInPc2d1	nacistický
plánů	plán	k1gInPc2	plán
sloužili	sloužit	k5eAaImAgMnP	sloužit
jako	jako	k9	jako
otroci	otrok	k1gMnPc1	otrok
německým	německý	k2eAgMnPc3d1	německý
pánům	pan	k1gMnPc3	pan
a	a	k8xC	a
u	u	k7c2	u
části	část	k1gFnSc2	část
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
rasově	rasově	k6eAd1	rasově
vhodných	vhodný	k2eAgMnPc2d1	vhodný
<g/>
"	"	kIx"	"
Slovanů	Slovan	k1gMnPc2	Slovan
mělo	mít	k5eAaImAgNnS	mít
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
poněmčení	poněmčení	k1gNnSc3	poněmčení
<g/>
.	.	kIx.	.
</s>
<s>
Generalplan	Generalplan	k1gInSc1	Generalplan
Ost	Ost	k1gFnSc2	Ost
byl	být	k5eAaImAgInS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
operace	operace	k1gFnSc1	operace
Tannenberg	Tannenberg	k1gInSc1	Tannenberg
popraveno	popravit	k5eAaPmNgNnS	popravit
přes	přes	k7c4	přes
20	[number]	k4	20
tisíc	tisíc	k4xCgInPc2	tisíc
příslušníků	příslušník	k1gMnPc2	příslušník
polské	polský	k2eAgFnSc2d1	polská
elity	elita	k1gFnSc2	elita
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
následovala	následovat	k5eAaImAgFnS	následovat
akce	akce	k1gFnSc1	akce
"	"	kIx"	"
<g/>
Inteligence	inteligence	k1gFnSc1	inteligence
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
bylo	být	k5eAaImAgNnS	být
zabito	zabít	k5eAaPmNgNnS	zabít
přes	přes	k7c4	přes
40	[number]	k4	40
tisíc	tisíc	k4xCgInPc2	tisíc
polských	polský	k2eAgMnPc2d1	polský
intelektuálů	intelektuál	k1gMnPc2	intelektuál
<g/>
.	.	kIx.	.
</s>
<s>
Německá	německý	k2eAgFnSc1d1	německá
okupační	okupační	k2eAgFnSc1d1	okupační
moc	moc	k1gFnSc1	moc
zavedla	zavést	k5eAaPmAgFnS	zavést
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
v	v	k7c6	v
Generálním	generální	k2eAgInSc6d1	generální
gouvernementu	gouvernement	k1gInSc6	gouvernement
a	a	k8xC	a
na	na	k7c6	na
okupovaných	okupovaný	k2eAgNnPc6d1	okupované
polských	polský	k2eAgNnPc6d1	polské
územích	území	k1gNnPc6	území
politiku	politika	k1gFnSc4	politika
teroru	teror	k1gInSc2	teror
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
prováděla	provádět	k5eAaImAgFnS	provádět
hromadné	hromadný	k2eAgFnPc4d1	hromadná
veřejné	veřejný	k2eAgFnPc4d1	veřejná
popravy	poprava	k1gFnPc4	poprava
rolníků	rolník	k1gMnPc2	rolník
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
měly	mít	k5eAaImAgInP	mít
zastrašovat	zastrašovat	k5eAaImF	zastrašovat
ostatní	ostatní	k2eAgNnSc4d1	ostatní
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
vysídlování	vysídlování	k1gNnSc3	vysídlování
z	z	k7c2	z
oblastí	oblast	k1gFnPc2	oblast
určených	určený	k2eAgFnPc2d1	určená
k	k	k7c3	k
další	další	k2eAgFnSc3d1	další
germanizaci	germanizace	k1gFnSc3	germanizace
a	a	k8xC	a
násilnému	násilný	k2eAgNnSc3d1	násilné
zabírání	zabírání	k1gNnSc3	zabírání
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
půdy	půda	k1gFnSc2	půda
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
ze	z	k7c2	z
svých	svůj	k3xOyFgInPc2	svůj
domovů	domov	k1gInPc2	domov
bylo	být	k5eAaImAgNnS	být
vyhnáno	vyhnat	k5eAaPmNgNnS	vyhnat
několik	několik	k4yIc1	několik
stovek	stovka	k1gFnPc2	stovka
tisíc	tisíc	k4xCgInPc2	tisíc
Poláků	Polák	k1gMnPc2	Polák
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
celkem	celkem	k6eAd1	celkem
2	[number]	k4	2
milióny	milión	k4xCgInPc1	milión
etnických	etnický	k2eAgMnPc2d1	etnický
Poláků	Polák	k1gMnPc2	Polák
nežidovského	židovský	k2eNgInSc2d1	nežidovský
původu	původ	k1gInSc2	původ
zemřely	zemřít	k5eAaPmAgFnP	zemřít
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
nacistické	nacistický	k2eAgFnSc2d1	nacistická
okupace	okupace	k1gFnSc2	okupace
a	a	k8xC	a
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Občané	občan	k1gMnPc1	občan
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
byli	být	k5eAaImAgMnP	být
jednak	jednak	k8xC	jednak
jako	jako	k8xS	jako
civilisté	civilista	k1gMnPc1	civilista
vystaveni	vystavit	k5eAaPmNgMnP	vystavit
německým	německý	k2eAgInPc3d1	německý
válečným	válečný	k2eAgInPc3d1	válečný
zločinům	zločin	k1gInPc3	zločin
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
Einsatzgruppen	Einsatzgruppen	k2eAgMnSc1d1	Einsatzgruppen
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc7	jejichž
obětí	oběť	k1gFnSc7	oběť
bylo	být	k5eAaImAgNnS	být
kolem	kolem	k7c2	kolem
2	[number]	k4	2
milionů	milion	k4xCgInPc2	milion
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
1,3	[number]	k4	1,3
milionů	milion	k4xCgInPc2	milion
sovětských	sovětský	k2eAgMnPc2d1	sovětský
Židů	Žid	k1gMnPc2	Žid
<g/>
.	.	kIx.	.
</s>
<s>
Byli	být	k5eAaImAgMnP	být
jako	jako	k9	jako
váleční	váleční	k2eAgMnPc1d1	váleční
zajatci	zajatec	k1gMnPc1	zajatec
popravováni	popravovat	k5eAaImNgMnP	popravovat
<g/>
,	,	kIx,	,
nasazováni	nasazovat	k5eAaImNgMnP	nasazovat
na	na	k7c4	na
nucené	nucený	k2eAgFnPc4d1	nucená
práce	práce	k1gFnPc4	práce
a	a	k8xC	a
vystaveni	vystavit	k5eAaPmNgMnP	vystavit
krutému	krutý	k2eAgNnSc3d1	kruté
zacházení	zacházení	k1gNnSc3	zacházení
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
odhadováno	odhadován	k2eAgNnSc1d1	odhadováno
<g/>
,	,	kIx,	,
že	že	k8xS	že
tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
byly	být	k5eAaImAgFnP	být
zavražděny	zavražděn	k2eAgFnPc1d1	zavražděna
až	až	k9	až
3	[number]	k4	3
miliony	milion	k4xCgInPc1	milion
sovětských	sovětský	k2eAgMnPc2d1	sovětský
válečných	válečný	k2eAgMnPc2d1	válečný
zajatců	zajatec	k1gMnPc2	zajatec
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
Němci	Němec	k1gMnPc7	Němec
okupovaném	okupovaný	k2eAgNnSc6d1	okupované
území	území	k1gNnSc6	území
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
13,7	[number]	k4	13,7
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
sovětských	sovětský	k2eAgMnPc2d1	sovětský
Židů	Žid	k1gMnPc2	Žid
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
20	[number]	k4	20
%	%	kIx~	%
všech	všecek	k3xTgMnPc2	všecek
tamních	tamní	k2eAgMnPc2d1	tamní
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Říšský	říšský	k2eAgMnSc1d1	říšský
ministr	ministr	k1gMnSc1	ministr
pro	pro	k7c4	pro
výživu	výživa	k1gFnSc4	výživa
Herbert	Herbert	k1gMnSc1	Herbert
Backe	Back	k1gInSc2	Back
prováděl	provádět	k5eAaImAgMnS	provádět
politiku	politika	k1gFnSc4	politika
vyhladovění	vyhladovění	k1gNnSc3	vyhladovění
východních	východní	k2eAgInPc2d1	východní
<g/>
,	,	kIx,	,
Německem	Německo	k1gNnSc7	Německo
okupovaných	okupovaný	k2eAgNnPc2d1	okupované
území	území	k1gNnPc2	území
(	(	kIx(	(
<g/>
Der	drát	k5eAaImRp2nS	drát
Hungerplan	Hungerplan	k1gInSc1	Hungerplan
také	také	k9	také
Der	drát	k5eAaImRp2nS	drát
Backe-Plan	Backe-Plan	k1gInSc1	Backe-Plan
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hladový	hladový	k2eAgInSc1d1	hladový
plán	plán	k1gInSc1	plán
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
předehrou	předehra	k1gFnSc7	předehra
k	k	k7c3	k
akci	akce	k1gFnSc3	akce
Generalplan	Generalplany	k1gInPc2	Generalplany
Ost	Ost	k1gMnPc2	Ost
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
historika	historik	k1gMnSc2	historik
Timothy	Timotha	k1gMnSc2	Timotha
Snydera	Snyder	k1gMnSc2	Snyder
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
hladového	hladový	k2eAgInSc2d1	hladový
plánu	plán	k1gInSc2	plán
"	"	kIx"	"
<g/>
4.2	[number]	k4	4.2
miliónu	milión	k4xCgInSc2	milión
sovětských	sovětský	k2eAgMnPc2d1	sovětský
občanů	občan	k1gMnPc2	občan
(	(	kIx(	(
<g/>
převážně	převážně	k6eAd1	převážně
Rusů	Rus	k1gMnPc2	Rus
<g/>
,	,	kIx,	,
Bělorusů	Bělorus	k1gMnPc2	Bělorus
a	a	k8xC	a
Ukrajinců	Ukrajinec	k1gMnPc2	Ukrajinec
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
letech	let	k1gInPc6	let
1941	[number]	k4	1941
<g/>
–	–	k?	–
<g/>
1944	[number]	k4	1944
vyhladověno	vyhladověn	k2eAgNnSc1d1	vyhladověno
německými	německý	k2eAgMnPc7d1	německý
okupanty	okupant	k1gMnPc7	okupant
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Politika	politika	k1gFnSc1	politika
úmyslného	úmyslný	k2eAgNnSc2d1	úmyslné
vyhladovění	vyhladovění	k1gNnSc2	vyhladovění
se	se	k3xPyFc4	se
v	v	k7c6	v
letech	let	k1gInPc6	let
1941	[number]	k4	1941
<g/>
–	–	k?	–
<g/>
1942	[number]	k4	1942
uplatňovala	uplatňovat	k5eAaImAgFnS	uplatňovat
i	i	k9	i
vůči	vůči	k7c3	vůči
sovětským	sovětský	k2eAgMnPc3d1	sovětský
válečným	válečný	k2eAgMnPc3d1	válečný
zajatcům	zajatec	k1gMnPc3	zajatec
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
státy	stát	k1gInPc7	stát
s	s	k7c7	s
vysokým	vysoký	k2eAgInSc7d1	vysoký
počtem	počet	k1gInSc7	počet
civilních	civilní	k2eAgFnPc2d1	civilní
obětí	oběť	k1gFnPc2	oběť
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
Jugoslávie	Jugoslávie	k1gFnSc1	Jugoslávie
s	s	k7c7	s
půl	půl	k1xP	půl
miliónem	milión	k4xCgInSc7	milión
mrtvých	mrtvý	k1gMnPc6	mrtvý
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
Srbů	Srb	k1gMnPc2	Srb
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
povraždil	povraždit	k5eAaPmAgInS	povraždit
pronacistický	pronacistický	k2eAgInSc1d1	pronacistický
Ustašovský	ustašovský	k2eAgInSc1d1	ustašovský
režim	režim	k1gInSc1	režim
<g/>
,	,	kIx,	,
mnohé	mnohý	k2eAgInPc1d1	mnohý
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
v	v	k7c6	v
táborovém	táborový	k2eAgInSc6d1	táborový
komlexu	komlex	k1gInSc6	komlex
Jasenovac	Jasenovac	k1gFnSc4	Jasenovac
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
k	k	k7c3	k
metodám	metoda	k1gFnPc3	metoda
vraždění	vraždění	k1gNnSc2	vraždění
patřilo	patřit	k5eAaImAgNnS	patřit
například	například	k6eAd1	například
upalování	upalování	k1gNnSc2	upalování
zaživa	zaživa	k6eAd1	zaživa
ve	v	k7c6	v
velkokapacitních	velkokapacitní	k2eAgFnPc6d1	velkokapacitní
pecích	pec	k1gFnPc6	pec
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
byli	být	k5eAaImAgMnP	být
lidé	člověk	k1gMnPc1	člověk
naháněni	naháněn	k2eAgMnPc1d1	naháněn
po	po	k7c6	po
desítkách	desítka	k1gFnPc6	desítka
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Porajmos	Porajmos	k1gInSc1	Porajmos
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
terčem	terč	k1gInSc7	terč
vyvražďování	vyvražďování	k1gNnSc2	vyvražďování
byli	být	k5eAaImAgMnP	být
Romové	Rom	k1gMnPc1	Rom
a	a	k8xC	a
romští	romský	k2eAgMnPc1d1	romský
míšenci	míšenec	k1gMnPc1	míšenec
<g/>
,	,	kIx,	,
s	s	k7c7	s
odhadovaným	odhadovaný	k2eAgInSc7d1	odhadovaný
počtem	počet	k1gInSc7	počet
220	[number]	k4	220
tisíc	tisíc	k4xCgInPc2	tisíc
obětí	oběť	k1gFnPc2	oběť
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
odhady	odhad	k1gInPc1	odhad
uvádějí	uvádět	k5eAaImIp3nP	uvádět
až	až	k9	až
800	[number]	k4	800
tisíc	tisíc	k4xCgInPc2	tisíc
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
činí	činit	k5eAaImIp3nS	činit
asi	asi	k9	asi
čtvrtinu	čtvrtina	k1gFnSc4	čtvrtina
až	až	k8xS	až
polovinu	polovina	k1gFnSc4	polovina
jejich	jejich	k3xOp3gFnSc2	jejich
předválečné	předválečný	k2eAgFnSc2d1	předválečná
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Roku	rok	k1gInSc2	rok
1935	[number]	k4	1935
Norimberské	norimberský	k2eAgInPc1d1	norimberský
zákony	zákon	k1gInPc1	zákon
označily	označit	k5eAaPmAgInP	označit
Romy	Rom	k1gMnPc4	Rom
a	a	k8xC	a
Židy	Žid	k1gMnPc4	Žid
za	za	k7c4	za
rasově	rasově	k6eAd1	rasově
méněcenné	méněcenný	k2eAgFnPc4d1	méněcenná
<g/>
.	.	kIx.	.
</s>
<s>
Sňatky	sňatek	k1gInPc1	sňatek
mezi	mezi	k7c7	mezi
příslušníky	příslušník	k1gMnPc7	příslušník
árijské	árijský	k2eAgFnSc2d1	árijská
a	a	k8xC	a
neárijské	árijský	k2eNgFnSc2d1	neárijská
rasy	rasa	k1gFnSc2	rasa
byly	být	k5eAaImAgFnP	být
zakázány	zakázat	k5eAaPmNgFnP	zakázat
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
14	[number]	k4	14
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1937	[number]	k4	1937
vydal	vydat	k5eAaPmAgMnS	vydat
říšský	říšský	k2eAgMnSc1d1	říšský
vůdce	vůdce	k1gMnSc1	vůdce
SS	SS	kA	SS
a	a	k8xC	a
šéf	šéf	k1gMnSc1	šéf
německé	německý	k2eAgFnSc2d1	německá
policie	policie	k1gFnSc2	policie
Heinrich	Heinrich	k1gMnSc1	Heinrich
Himmler	Himmler	k1gMnSc1	Himmler
výnos	výnos	k1gInSc4	výnos
o	o	k7c6	o
preventivním	preventivní	k2eAgNnSc6d1	preventivní
potírání	potírání	k1gNnSc6	potírání
zločinnosti	zločinnost	k1gFnSc2	zločinnost
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
mezi	mezi	k7c4	mezi
asociály	asociál	k1gMnPc4	asociál
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
cikány	cikán	k1gMnPc4	cikán
<g/>
,	,	kIx,	,
cikánské	cikánský	k2eAgMnPc4d1	cikánský
míšence	míšenec	k1gMnPc4	míšenec
a	a	k8xC	a
cikánským	cikánský	k2eAgInSc7d1	cikánský
způsobem	způsob	k1gInSc7	způsob
kočující	kočující	k2eAgFnSc2d1	kočující
osoby	osoba	k1gFnSc2	osoba
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
řádně	řádně	k6eAd1	řádně
nepracovaly	pracovat	k5eNaImAgInP	pracovat
a	a	k8xC	a
prováděly	provádět	k5eAaImAgInP	provádět
trestnou	trestný	k2eAgFnSc4d1	trestná
činnost	činnost	k1gFnSc4	činnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1938	[number]	k4	1938
začali	začít	k5eAaPmAgMnP	začít
být	být	k5eAaImF	být
cikáni	cikán	k1gMnPc1	cikán
v	v	k7c6	v
Říši	říš	k1gFnSc6	říš
hromadně	hromadně	k6eAd1	hromadně
vězněni	věznit	k5eAaImNgMnP	věznit
a	a	k8xC	a
deportování	deportování	k1gNnSc1	deportování
do	do	k7c2	do
koncentračních	koncentrační	k2eAgInPc2d1	koncentrační
táborů	tábor	k1gInPc2	tábor
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
8	[number]	k4	8
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1938	[number]	k4	1938
vydal	vydat	k5eAaPmAgInS	vydat
Himmler	Himmler	k1gInSc1	Himmler
"	"	kIx"	"
<g/>
Výnos	výnos	k1gInSc1	výnos
o	o	k7c6	o
potírání	potírání	k1gNnSc6	potírání
cikánského	cikánský	k2eAgInSc2d1	cikánský
zlořádu	zlořád	k1gInSc2	zlořád
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
nařídil	nařídit	k5eAaPmAgMnS	nařídit
podchycení	podchycení	k1gNnSc4	podchycení
všech	všecek	k3xTgMnPc2	všecek
cikánů	cikán	k1gMnPc2	cikán
<g/>
,	,	kIx,	,
cikánských	cikánský	k2eAgMnPc2d1	cikánský
míšenců	míšenec	k1gMnPc2	míšenec
a	a	k8xC	a
cikánským	cikánský	k2eAgInSc7d1	cikánský
způsobem	způsob	k1gInSc7	způsob
kočujících	kočující	k2eAgFnPc2d1	kočující
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1939	[number]	k4	1939
provedla	provést	k5eAaPmAgFnS	provést
říšská	říšský	k2eAgFnSc1d1	říšská
kriminální	kriminální	k2eAgFnSc1d1	kriminální
policie	policie	k1gFnSc1	policie
soupis	soupis	k1gInSc1	soupis
všech	všecek	k3xTgMnPc2	všecek
cikánů	cikán	k1gMnPc2	cikán
<g/>
,	,	kIx,	,
cikánských	cikánský	k2eAgMnPc2d1	cikánský
míšenců	míšenec	k1gMnPc2	míšenec
a	a	k8xC	a
cikánským	cikánský	k2eAgInSc7d1	cikánský
způsobem	způsob	k1gInSc7	způsob
kočujících	kočující	k2eAgFnPc2d1	kočující
osob	osoba	k1gFnPc2	osoba
starších	starší	k1gMnPc2	starší
6	[number]	k4	6
let	léto	k1gNnPc2	léto
a	a	k8xC	a
zakázala	zakázat	k5eAaPmAgFnS	zakázat
jim	on	k3xPp3gMnPc3	on
volný	volný	k2eAgInSc1d1	volný
pohyb	pohyb	k1gInSc1	pohyb
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1942	[number]	k4	1942
vydal	vydat	k5eAaPmAgInS	vydat
obdobné	obdobný	k2eAgNnSc4d1	obdobné
nařízení	nařízení	k1gNnSc4	nařízení
o	o	k7c6	o
soupisu	soupis	k1gInSc6	soupis
cikánů	cikán	k1gMnPc2	cikán
i	i	k9	i
velitel	velitel	k1gMnSc1	velitel
protektorátní	protektorátní	k2eAgFnSc2d1	protektorátní
neuniformované	uniformovaný	k2eNgFnSc2d1	neuniformovaná
policie	policie	k1gFnSc2	policie
<g/>
.	.	kIx.	.
</s>
<s>
Dosavadní	dosavadní	k2eAgInPc1d1	dosavadní
kárné	kárný	k2eAgInPc1d1	kárný
pracovní	pracovní	k2eAgInPc1d1	pracovní
tábory	tábor	k1gInPc1	tábor
byly	být	k5eAaImAgInP	být
změněny	změnit	k5eAaPmNgInP	změnit
na	na	k7c4	na
cikánské	cikánský	k2eAgInPc4d1	cikánský
sběrné	sběrný	k2eAgInPc4d1	sběrný
tábory	tábor	k1gInPc4	tábor
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
1942	[number]	k4	1942
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
prováděcí	prováděcí	k2eAgNnSc1d1	prováděcí
nařízení	nařízení	k1gNnSc1	nařízení
o	o	k7c6	o
deportacích	deportace	k1gFnPc6	deportace
romského	romský	k2eAgNnSc2d1	romské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
do	do	k7c2	do
cikánského	cikánský	k2eAgInSc2d1	cikánský
tábora	tábor	k1gInSc2	tábor
v	v	k7c6	v
Auschwitz-Birkenau	Auschwitz-Birkenaus	k1gInSc6	Auschwitz-Birkenaus
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1944	[number]	k4	1944
byl	být	k5eAaImAgInS	být
cikánský	cikánský	k2eAgInSc1d1	cikánský
tábor	tábor	k1gInSc1	tábor
nacisty	nacista	k1gMnSc2	nacista
zničen	zničen	k2eAgMnSc1d1	zničen
a	a	k8xC	a
asi	asi	k9	asi
tři	tři	k4xCgInPc1	tři
tisíce	tisíc	k4xCgInPc1	tisíc
Romů	Rom	k1gMnPc2	Rom
byly	být	k5eAaImAgInP	být
usmrceny	usmrtit	k5eAaPmNgInP	usmrtit
v	v	k7c6	v
plynových	plynový	k2eAgFnPc6d1	plynová
komorách	komora	k1gFnPc6	komora
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
1700	[number]	k4	1700
práceschopných	práceschopný	k2eAgMnPc2d1	práceschopný
Romů	Rom	k1gMnPc2	Rom
bylo	být	k5eAaImAgNnS	být
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
přemístěno	přemístit	k5eAaPmNgNnS	přemístit
do	do	k7c2	do
jiných	jiný	k2eAgInPc2d1	jiný
koncentračních	koncentrační	k2eAgInPc2d1	koncentrační
táborů	tábor	k1gInPc2	tábor
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Akce	akce	k1gFnSc2	akce
T	T	kA	T
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Systematické	systematický	k2eAgNnSc1d1	systematické
vyvražďování	vyvražďování	k1gNnSc1	vyvražďování
postižených	postižený	k1gMnPc2	postižený
bylo	být	k5eAaImAgNnS	být
zahájeno	zahájit	k5eAaPmNgNnS	zahájit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
oficiálnímu	oficiální	k2eAgNnSc3d1	oficiální
ukončení	ukončení	k1gNnSc3	ukončení
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
po	po	k7c6	po
protestech	protest	k1gInPc6	protest
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
iniciovali	iniciovat	k5eAaBmAgMnP	iniciovat
představitelé	představitel	k1gMnPc1	představitel
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
program	program	k1gInSc1	program
ovšem	ovšem	k9	ovšem
skrytě	skrytě	k6eAd1	skrytě
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
dál	daleko	k6eAd2	daleko
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
mnohem	mnohem	k6eAd1	mnohem
pomaleji	pomale	k6eAd2	pomale
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
až	až	k9	až
do	do	k7c2	do
konečné	konečný	k2eAgFnSc2d1	konečná
porážky	porážka	k1gFnSc2	porážka
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
letech	let	k1gInPc6	let
1939	[number]	k4	1939
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
povražděno	povraždit	k5eAaPmNgNnS	povraždit
asi	asi	k9	asi
200	[number]	k4	200
<g/>
–	–	k?	–
<g/>
250	[number]	k4	250
tisíc	tisíc	k4xCgInPc2	tisíc
postižených	postižený	k1gMnPc2	postižený
<g/>
.	.	kIx.	.
</s>
<s>
Neexistují	existovat	k5eNaImIp3nP	existovat
oficiální	oficiální	k2eAgInPc1d1	oficiální
záznamy	záznam	k1gInPc1	záznam
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
kolik	kolik	k4yQc1	kolik
homosexuálů	homosexuál	k1gMnPc2	homosexuál
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
v	v	k7c6	v
koncentračních	koncentrační	k2eAgInPc6d1	koncentrační
táborech	tábor	k1gInPc6	tábor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1933	[number]	k4	1933
až	až	k9	až
1945	[number]	k4	1945
bylo	být	k5eAaImAgNnS	být
100	[number]	k4	100
tisíc	tisíc	k4xCgInPc2	tisíc
homosexuálů	homosexuál	k1gMnPc2	homosexuál
zatčeno	zatknout	k5eAaPmNgNnS	zatknout
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
50	[number]	k4	50
tisíc	tisíc	k4xCgInPc2	tisíc
odsouzeno	odsoudit	k5eAaPmNgNnS	odsoudit
a	a	k8xC	a
uvězněno	uvěznit	k5eAaPmNgNnS	uvěznit
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yQnSc2	což
odhadem	odhad	k1gInSc7	odhad
5	[number]	k4	5
až	až	k8xS	až
15	[number]	k4	15
tisíc	tisíc	k4xCgInPc2	tisíc
bylo	být	k5eAaImAgNnS	být
posláno	poslat	k5eAaPmNgNnS	poslat
do	do	k7c2	do
koncentračních	koncentrační	k2eAgInPc2d1	koncentrační
táborů	tábor	k1gInPc2	tábor
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
německého	německý	k2eAgMnSc2d1	německý
badatele	badatel	k1gMnSc2	badatel
Rüdigera	Rüdiger	k1gMnSc2	Rüdiger
Lautmanna	Lautmann	k1gMnSc2	Lautmann
přibližně	přibližně	k6eAd1	přibližně
60	[number]	k4	60
%	%	kIx~	%
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
v	v	k7c6	v
táborech	tábor	k1gInPc6	tábor
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Homosexuálové	homosexuál	k1gMnPc1	homosexuál
museli	muset	k5eAaImAgMnP	muset
nosit	nosit	k5eAaImF	nosit
růžový	růžový	k2eAgInSc4d1	růžový
trojúhelník	trojúhelník	k1gInSc4	trojúhelník
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c7	mezi
symboly	symbol	k1gInPc7	symbol
homosexuálů	homosexuál	k1gMnPc2	homosexuál
bojujících	bojující	k2eAgMnPc2d1	bojující
za	za	k7c4	za
svoje	svůj	k3xOyFgNnPc4	svůj
práva	právo	k1gNnPc4	právo
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
oběti	oběť	k1gFnPc1	oběť
nacismu	nacismus	k1gInSc2	nacismus
se	se	k3xPyFc4	se
nedočkaly	dočkat	k5eNaPmAgFnP	dočkat
odškodnění	odškodnění	k1gNnSc4	odškodnění
<g/>
.	.	kIx.	.
</s>
<s>
Oběti	oběť	k1gFnPc1	oběť
holocaustu	holocaust	k1gInSc2	holocaust
si	se	k3xPyFc3	se
mnoho	mnoho	k6eAd1	mnoho
států	stát	k1gInPc2	stát
světa	svět	k1gInSc2	svět
připomíná	připomínat	k5eAaImIp3nS	připomínat
27	[number]	k4	27
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
<g/>
,	,	kIx,	,
den	den	k1gInSc1	den
osvobození	osvobození	k1gNnSc2	osvobození
vyhlazovacího	vyhlazovací	k2eAgInSc2d1	vyhlazovací
tábora	tábor	k1gInSc2	tábor
Auschwitz	Auschwitza	k1gFnPc2	Auschwitza
Rudou	rudý	k2eAgFnSc7d1	rudá
armádou	armáda	k1gFnSc7	armáda
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
se	se	k3xPyFc4	se
s	s	k7c7	s
tímto	tento	k3xDgNnSc7	tento
datem	datum	k1gNnSc7	datum
pojí	pojit	k5eAaImIp3nS	pojit
různé	různý	k2eAgFnPc4d1	různá
vzpomínkové	vzpomínkový	k2eAgFnPc4d1	vzpomínková
akce	akce	k1gFnPc4	akce
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
české	český	k2eAgFnPc1d1	Česká
oběti	oběť	k1gFnPc1	oběť
holocaustu	holocaust	k1gInSc2	holocaust
připomínány	připomínat	k5eAaImNgFnP	připomínat
na	na	k7c4	na
výročí	výročí	k1gNnSc4	výročí
vyvraždění	vyvraždění	k1gNnSc2	vyvraždění
terezínského	terezínský	k2eAgInSc2d1	terezínský
rodinného	rodinný	k2eAgInSc2d1	rodinný
tábora	tábor	k1gInSc2	tábor
v	v	k7c4	v
Auschwitz	Auschwitz	k1gInSc4	Auschwitz
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
z	z	k7c2	z
8	[number]	k4	8
<g/>
.	.	kIx.	.
na	na	k7c4	na
9	[number]	k4	9
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1944	[number]	k4	1944
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
specificky	specificky	k6eAd1	specificky
významné	významný	k2eAgNnSc1d1	významné
pro	pro	k7c4	pro
Česko	Česko	k1gNnSc4	Česko
jako	jako	k8xC	jako
největší	veliký	k2eAgFnSc1d3	veliký
masová	masový	k2eAgFnSc1d1	masová
vražda	vražda	k1gFnSc1	vražda
československých	československý	k2eAgMnPc2d1	československý
občanů	občan	k1gMnPc2	občan
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
výročí	výročí	k1gNnSc4	výročí
této	tento	k3xDgFnSc2	tento
události	událost	k1gFnSc2	událost
se	se	k3xPyFc4	se
například	například	k6eAd1	například
koná	konat	k5eAaImIp3nS	konat
tryzna	tryzna	k1gFnSc1	tryzna
v	v	k7c6	v
pražském	pražský	k2eAgInSc6d1	pražský
Památníku	památník	k1gInSc6	památník
českých	český	k2eAgFnPc2d1	Česká
a	a	k8xC	a
moravských	moravský	k2eAgFnPc2d1	Moravská
obětí	oběť	k1gFnPc2	oběť
šoa	šoa	k?	šoa
v	v	k7c6	v
Pinkasově	Pinkasův	k2eAgFnSc6d1	Pinkasova
synagoze	synagoga	k1gFnSc6	synagoga
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Stolpersteine	Stolperstein	k1gInSc5	Stolperstein
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
90	[number]	k4	90
<g/>
tých	týcha	k1gFnPc2	týcha
let	léto	k1gNnPc2	léto
započala	započnout	k5eAaPmAgFnS	započnout
z	z	k7c2	z
iniciativy	iniciativa	k1gFnSc2	iniciativa
německého	německý	k2eAgMnSc2d1	německý
umělce	umělec	k1gMnSc2	umělec
Guntera	Gunter	k1gMnSc2	Gunter
Demniga	Demnig	k1gMnSc2	Demnig
akce	akce	k1gFnSc2	akce
tzv.	tzv.	kA	tzv.
stolpersteine	stolpersteinout	k5eAaPmIp3nS	stolpersteinout
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
pokládání	pokládání	k1gNnSc1	pokládání
pamětních	pamětní	k2eAgInPc2d1	pamětní
kamenů	kámen	k1gInPc2	kámen
před	před	k7c7	před
domy	dům	k1gInPc7	dům
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
naposled	naposled	k6eAd1	naposled
žili	žít	k5eAaImAgMnP	žít
oběti	oběť	k1gFnPc4	oběť
holokaustu	holokaust	k1gInSc2	holokaust
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
byly	být	k5eAaImAgInP	být
první	první	k4xOgInPc1	první
kameny	kámen	k1gInPc1	kámen
položeny	položen	k2eAgInPc1d1	položen
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
bylo	být	k5eAaImAgNnS	být
napočítáno	napočítat	k5eAaPmNgNnS	napočítat
již	již	k9	již
přes	přes	k7c4	přes
60	[number]	k4	60
000	[number]	k4	000
kamenů	kámen	k1gInPc2	kámen
na	na	k7c4	na
zhruba	zhruba	k6eAd1	zhruba
1800	[number]	k4	1800
místech	místo	k1gNnPc6	místo
v	v	k7c6	v
celkem	celkem	k6eAd1	celkem
21	[number]	k4	21
evropských	evropský	k2eAgFnPc6d1	Evropská
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
pak	pak	k9	pak
několik	několik	k4yIc4	několik
set	set	k1gInSc4	set
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
ve	v	k7c6	v
více	hodně	k6eAd2	hodně
než	než	k8xS	než
35	[number]	k4	35
lokalitách	lokalita	k1gFnPc6	lokalita
<g/>
.	.	kIx.	.
</s>
<s>
Oběti	oběť	k1gFnPc1	oběť
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
Židů	Žid	k1gMnPc2	Žid
mohly	moct	k5eAaImAgInP	moct
být	být	k5eAaImF	být
zabity	zabít	k5eAaPmNgInP	zabít
při	při	k7c6	při
pogromech	pogrom	k1gInPc6	pogrom
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
byly	být	k5eAaImAgInP	být
povražděny	povraždit	k5eAaPmNgInP	povraždit
polovojenskými	polovojenský	k2eAgFnPc7d1	polovojenská
a	a	k8xC	a
protižidovskými	protižidovský	k2eAgNnPc7d1	protižidovské
komandy	komando	k1gNnPc7	komando
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
zrodem	zrod	k1gInSc7	zrod
Konečného	Konečného	k2eAgInSc7d1	Konečného
řešení	řešení	k1gNnSc4	řešení
židovské	židovský	k2eAgFnSc2d1	židovská
otázky	otázka	k1gFnSc2	otázka
byli	být	k5eAaImAgMnP	být
Židé	Žid	k1gMnPc1	Žid
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ostatními	ostatní	k2eAgFnPc7d1	ostatní
skupinami	skupina	k1gFnPc7	skupina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tvořící	tvořící	k2eAgFnSc1d1	tvořící
největší	veliký	k2eAgFnSc1d3	veliký
část	část	k1gFnSc1	část
<g/>
)	)	kIx)	)
koncentrováni	koncentrován	k2eAgMnPc1d1	koncentrován
v	v	k7c6	v
ghettech	ghetto	k1gNnPc6	ghetto
<g/>
,	,	kIx,	,
koncentračních	koncentrační	k2eAgInPc6d1	koncentrační
táborech	tábor	k1gInPc6	tábor
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
posíláni	posílat	k5eAaImNgMnP	posílat
do	do	k7c2	do
vyhlazovacích	vyhlazovací	k2eAgInPc2d1	vyhlazovací
táborů	tábor	k1gInPc2	tábor
na	na	k7c6	na
území	území	k1gNnSc6	území
dobytém	dobytý	k2eAgNnSc6d1	dobyté
Třetí	třetí	k4xOgFnSc7	třetí
říší	říš	k1gFnSc7	říš
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
představovaly	představovat	k5eAaImAgInP	představovat
"	"	kIx"	"
<g/>
továrny	továrna	k1gFnSc2	továrna
na	na	k7c4	na
smrt	smrt	k1gFnSc4	smrt
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
mající	mající	k2eAgInSc1d1	mající
za	za	k7c4	za
účel	účel	k1gInSc4	účel
co	co	k9	co
nejefektivněji	efektivně	k6eAd3	efektivně
zabíjet	zabíjet	k5eAaImF	zabíjet
a	a	k8xC	a
likvidovat	likvidovat	k5eAaBmF	likvidovat
mrtvá	mrtvý	k2eAgNnPc4d1	mrtvé
těla	tělo	k1gNnPc4	tělo
lidí	člověk	k1gMnPc2	člověk
dopravovaných	dopravovaný	k2eAgMnPc2d1	dopravovaný
za	za	k7c4	za
jejich	jejich	k3xOp3gFnPc4	jejich
zdi	zeď	k1gFnPc4	zeď
<g/>
.	.	kIx.	.
</s>
<s>
Nezanedbatelná	zanedbatelný	k2eNgFnSc1d1	nezanedbatelná
část	část	k1gFnSc1	část
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
do	do	k7c2	do
nich	on	k3xPp3gMnPc2	on
byli	být	k5eAaImAgMnP	být
posláni	poslán	k2eAgMnPc1d1	poslán
<g/>
,	,	kIx,	,
zahynula	zahynout	k5eAaPmAgFnS	zahynout
již	již	k6eAd1	již
při	při	k7c6	při
transportu	transport	k1gInSc6	transport
za	za	k7c2	za
nelidských	lidský	k2eNgFnPc2d1	nelidská
podmínek	podmínka	k1gFnPc2	podmínka
(	(	kIx(	(
<g/>
namačkáni	namačkán	k2eAgMnPc1d1	namačkán
v	v	k7c6	v
dobytčích	dobytčí	k2eAgInPc6d1	dobytčí
vagónech	vagón	k1gInPc6	vagón
<g/>
,	,	kIx,	,
přepravováni	přepravovat	k5eAaImNgMnP	přepravovat
často	často	k6eAd1	často
i	i	k8xC	i
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
jídla	jídlo	k1gNnSc2	jídlo
a	a	k8xC	a
vody	voda	k1gFnSc2	voda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
příjezdu	příjezd	k1gInSc6	příjezd
do	do	k7c2	do
tábora	tábor	k1gInSc2	tábor
následovala	následovat	k5eAaImAgFnS	následovat
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
od	od	k7c2	od
léta	léto	k1gNnSc2	léto
1942	[number]	k4	1942
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
vyhlazovacích	vyhlazovací	k2eAgInPc2d1	vyhlazovací
táborů	tábor	k1gInPc2	tábor
vybudována	vybudován	k2eAgNnPc4d1	vybudováno
krematoria	krematorium	k1gNnPc4	krematorium
<g/>
)	)	kIx)	)
vstupní	vstupní	k2eAgFnSc2d1	vstupní
selekce	selekce	k1gFnSc2	selekce
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterých	který	k3yIgFnPc6	který
táborový	táborový	k2eAgMnSc1d1	táborový
lékař	lékař	k1gMnSc1	lékař
vybíral	vybírat	k5eAaImAgMnS	vybírat
část	část	k1gFnSc4	část
nejzdatnějších	zdatný	k2eAgMnPc2d3	nejzdatnější
vězňů	vězeň	k1gMnPc2	vězeň
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
byli	být	k5eAaImAgMnP	být
určeni	určit	k5eAaPmNgMnP	určit
pro	pro	k7c4	pro
práci	práce	k1gFnSc4	práce
(	(	kIx(	(
<g/>
buď	buď	k8xC	buď
v	v	k7c6	v
pomocných	pomocný	k2eAgInPc6d1	pomocný
táborech	tábor	k1gInPc6	tábor
nebo	nebo	k8xC	nebo
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
širším	široký	k2eAgInSc6d2	širší
komplexu	komplex	k1gInSc6	komplex
tábora	tábor	k1gInSc2	tábor
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
pro	pro	k7c4	pro
další	další	k2eAgNnSc4d1	další
budování	budování	k1gNnSc4	budování
tábora	tábor	k1gInSc2	tábor
<g/>
,	,	kIx,	,
stavbu	stavba	k1gFnSc4	stavba
nových	nový	k2eAgFnPc2d1	nová
budov	budova	k1gFnPc2	budova
apod.	apod.	kA	apod.
Lidi	člověk	k1gMnPc4	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
vstupní	vstupní	k2eAgFnSc7d1	vstupní
selekcí	selekce	k1gFnSc7	selekce
neprošli	projít	k5eNaPmAgMnP	projít
<g/>
,	,	kIx,	,
čekala	čekat	k5eAaImAgFnS	čekat
likvidace	likvidace	k1gFnSc1	likvidace
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
podle	podle	k7c2	podle
svědectví	svědectví	k1gNnSc2	svědectví
Rudolfa	Rudolf	k1gMnSc2	Rudolf
Vrby	Vrba	k1gMnSc2	Vrba
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
utekl	utéct	k5eAaPmAgMnS	utéct
z	z	k7c2	z
Auschwitzu	Auschwitz	k1gInSc2	Auschwitz
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
v	v	k7c6	v
letech	let	k1gInPc6	let
1942	[number]	k4	1942
<g/>
–	–	k?	–
<g/>
1944	[number]	k4	1944
počet	počet	k1gInSc4	počet
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
neprošli	projít	k5eNaPmAgMnP	projít
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
táboře	tábor	k1gInSc6	tábor
vstupní	vstupní	k2eAgFnSc1d1	vstupní
selekcí	selekce	k1gFnSc7	selekce
<g/>
,	,	kIx,	,
pohyboval	pohybovat	k5eAaImAgMnS	pohybovat
typicky	typicky	k6eAd1	typicky
a	a	k8xC	a
zhruba	zhruba	k6eAd1	zhruba
okolo	okolo	k7c2	okolo
75	[number]	k4	75
až	až	k9	až
90	[number]	k4	90
%	%	kIx~	%
<g/>
,	,	kIx,	,
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
některé	některý	k3yIgInPc1	některý
transporty	transport	k1gInPc1	transport
byly	být	k5eAaImAgInP	být
zlikvidovány	zlikvidovat	k5eAaPmNgInP	zlikvidovat
stoprocentně	stoprocentně	k6eAd1	stoprocentně
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgMnPc4	ten
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
prošli	projít	k5eAaPmAgMnP	projít
vstupní	vstupní	k2eAgFnSc7d1	vstupní
selekcí	selekce	k1gFnSc7	selekce
<g/>
,	,	kIx,	,
čekala	čekat	k5eAaImAgFnS	čekat
každodenní	každodenní	k2eAgFnSc1d1	každodenní
tvrdá	tvrdý	k2eAgFnSc1d1	tvrdá
práce	práce	k1gFnSc1	práce
v	v	k7c6	v
drsných	drsný	k2eAgFnPc6d1	drsná
podmínkách	podmínka	k1gFnPc6	podmínka
a	a	k8xC	a
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
počasí	počasí	k1gNnSc6	počasí
<g/>
,	,	kIx,	,
a	a	k8xC	a
za	za	k7c4	za
ni	on	k3xPp3gFnSc4	on
malé	malý	k2eAgMnPc4d1	malý
příděly	příděl	k1gInPc4	příděl
velmi	velmi	k6eAd1	velmi
nekvalitního	kvalitní	k2eNgNnSc2d1	nekvalitní
jídla	jídlo	k1gNnSc2	jídlo
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
zcela	zcela	k6eAd1	zcela
nedostačovaly	dostačovat	k5eNaImAgFnP	dostačovat
jejich	jejich	k3xOp3gInSc3	jejich
energetickému	energetický	k2eAgInSc3d1	energetický
výdeji	výdej	k1gInSc3	výdej
<g/>
.	.	kIx.	.
</s>
<s>
Přeživší	přeživší	k2eAgMnPc1d1	přeživší
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
táborů	tábor	k1gInPc2	tábor
se	se	k3xPyFc4	se
shodují	shodovat	k5eAaImIp3nP	shodovat
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
vedoucí	vedoucí	k1gMnPc1	vedoucí
z	z	k7c2	z
podniků	podnik	k1gInPc2	podnik
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgFnPc6	který
vězni	vězeň	k1gMnPc1	vězeň
pracovali	pracovat	k5eAaImAgMnP	pracovat
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
mohli	moct	k5eAaImAgMnP	moct
dovolit	dovolit	k5eAaPmF	dovolit
je	on	k3xPp3gInPc4	on
pracovně	pracovně	k6eAd1	pracovně
přetěžovat	přetěžovat	k5eAaImF	přetěžovat
nad	nad	k7c4	nad
mez	mez	k1gFnSc4	mez
trvalé	trvalý	k2eAgFnSc2d1	trvalá
udržitelnosti	udržitelnost	k1gFnSc2	udržitelnost
kvality	kvalita	k1gFnSc2	kvalita
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
museli	muset	k5eAaImAgMnP	muset
bát	bát	k5eAaImF	bát
o	o	k7c4	o
přísun	přísun	k1gInSc4	přísun
nové	nový	k2eAgFnSc2d1	nová
pracovní	pracovní	k2eAgFnSc2d1	pracovní
síly	síla	k1gFnSc2	síla
–	–	k?	–
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
úhlu	úhel	k1gInSc2	úhel
pohledu	pohled	k1gInSc2	pohled
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
nahodilou	nahodilý	k2eAgFnSc4d1	nahodilá
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
záměrnou	záměrná	k1gFnSc4	záměrná
<g/>
,	,	kIx,	,
pomalou	pomalý	k2eAgFnSc4d1	pomalá
smrt	smrt	k1gFnSc4	smrt
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
kombinace	kombinace	k1gFnSc2	kombinace
podvýživy	podvýživa	k1gFnSc2	podvýživa
a	a	k8xC	a
vyčerpání	vyčerpání	k1gNnSc2	vyčerpání
<g/>
.	.	kIx.	.
</s>
<s>
Selekce	selekce	k1gFnSc1	selekce
lidí	člověk	k1gMnPc2	člověk
určených	určený	k2eAgFnPc2d1	určená
pro	pro	k7c4	pro
likvidaci	likvidace	k1gFnSc4	likvidace
probíhaly	probíhat	k5eAaImAgFnP	probíhat
nejenom	nejenom	k6eAd1	nejenom
při	při	k7c6	při
vstupu	vstup	k1gInSc6	vstup
do	do	k7c2	do
vyhlazovacích	vyhlazovací	k2eAgInPc2d1	vyhlazovací
táborů	tábor	k1gInPc2	tábor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gMnPc4	jejich
obyvatele	obyvatel	k1gMnPc4	obyvatel
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
korigován	korigován	k2eAgInSc1d1	korigován
počet	počet	k1gInSc1	počet
vězňů	vězeň	k1gMnPc2	vězeň
mezi	mezi	k7c7	mezi
jejich	jejich	k3xOp3gFnPc7	jejich
zdmi	zeď	k1gFnPc7	zeď
<g/>
,	,	kIx,	,
zvyšovaný	zvyšovaný	k2eAgInSc1d1	zvyšovaný
o	o	k7c4	o
pravidelně	pravidelně	k6eAd1	pravidelně
přijíždějící	přijíždějící	k2eAgFnPc4d1	přijíždějící
transporty	transporta	k1gFnPc4	transporta
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
pracovali	pracovat	k5eAaImAgMnP	pracovat
nejdéle	dlouho	k6eAd3	dlouho
a	a	k8xC	a
na	na	k7c6	na
jejichž	jejichž	k3xOyRp3gNnSc6	jejichž
zdraví	zdraví	k1gNnSc6	zdraví
se	se	k3xPyFc4	se
nesnesitelné	snesitelný	k2eNgFnPc1d1	nesnesitelná
pracovní	pracovní	k2eAgFnPc1d1	pracovní
podmínky	podmínka	k1gFnPc1	podmínka
a	a	k8xC	a
nedostačující	dostačující	k2eNgFnSc1d1	nedostačující
výživa	výživa	k1gFnSc1	výživa
podepsaly	podepsat	k5eAaPmAgFnP	podepsat
nejvíce	nejvíce	k6eAd1	nejvíce
<g/>
,	,	kIx,	,
zeslábli	zeslábnout	k5eAaPmAgMnP	zeslábnout
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
některé	některý	k3yIgFnSc6	některý
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
selekcí	selekce	k1gFnPc2	selekce
byli	být	k5eAaImAgMnP	být
shledáni	shledat	k5eAaPmNgMnP	shledat
neschopnými	schopný	k2eNgMnPc7d1	neschopný
další	další	k2eAgMnPc4d1	další
práce	práce	k1gFnSc2	práce
a	a	k8xC	a
následně	následně	k6eAd1	následně
zlikvidováni	zlikvidován	k2eAgMnPc1d1	zlikvidován
<g/>
;	;	kIx,	;
jejich	jejich	k3xOp3gInSc4	jejich
zesláblý	zesláblý	k2eAgInSc4d1	zesláblý
organismus	organismus	k1gInSc4	organismus
též	též	k9	též
mohla	moct	k5eAaImAgFnS	moct
skolit	skolit	k5eAaPmF	skolit
(	(	kIx(	(
<g/>
běžně	běžně	k6eAd1	běžně
<g />
.	.	kIx.	.
</s>
<s>
nesmrtelná	smrtelný	k2eNgFnSc1d1	nesmrtelná
<g/>
)	)	kIx)	)
nemoc	nemoc	k1gFnSc1	nemoc
či	či	k8xC	či
infekce	infekce	k1gFnSc1	infekce
ze	z	k7c2	z
zranění	zranění	k1gNnSc2	zranění
(	(	kIx(	(
<g/>
popř.	popř.	kA	popř.
kombinace	kombinace	k1gFnSc1	kombinace
více	hodně	k6eAd2	hodně
těchto	tento	k3xDgMnPc2	tento
faktorů	faktor	k1gMnPc2	faktor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
taktéž	taktéž	k?	taktéž
podchlazení	podchlazení	k1gNnSc4	podchlazení
vlivem	vlivem	k7c2	vlivem
sychravého	sychravý	k2eAgNnSc2d1	sychravé
a	a	k8xC	a
chladného	chladný	k2eAgNnSc2d1	chladné
počasí	počasí	k1gNnSc2	počasí
(	(	kIx(	(
<g/>
baráky	barák	k1gInPc1	barák
v	v	k7c6	v
táborech	tábor	k1gInPc6	tábor
nebyly	být	k5eNaImAgFnP	být
nijak	nijak	k6eAd1	nijak
vyhřívány	vyhříván	k2eAgFnPc1d1	vyhřívána
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
mohli	moct	k5eAaImAgMnP	moct
nalézt	nalézt	k5eAaBmF	nalézt
smrt	smrt	k1gFnSc4	smrt
přímo	přímo	k6eAd1	přímo
při	při	k7c6	při
práci	práce	k1gFnSc6	práce
<g/>
,	,	kIx,	,
když	když	k8xS	když
již	již	k6eAd1	již
nebyli	být	k5eNaImAgMnP	být
schopni	schopen	k2eAgMnPc1d1	schopen
plnit	plnit	k5eAaImF	plnit
kvóty	kvóta	k1gFnPc4	kvóta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
několika	několik	k4yIc6	několik
případech	případ	k1gInPc6	případ
navíc	navíc	k6eAd1	navíc
v	v	k7c6	v
táborech	tábor	k1gInPc6	tábor
vypukly	vypuknout	k5eAaPmAgFnP	vypuknout
epidemie	epidemie	k1gFnPc1	epidemie
smrtelných	smrtelný	k2eAgFnPc2d1	smrtelná
nemocí	nemoc	k1gFnPc2	nemoc
–	–	k?	–
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nejvážnější	vážní	k2eAgFnSc1d3	nejvážnější
byla	být	k5eAaImAgFnS	být
epidemie	epidemie	k1gFnSc1	epidemie
tyfu	tyf	k1gInSc2	tyf
v	v	k7c6	v
Auschwitz-Birkenau	Auschwitz-Birkenaus	k1gInSc6	Auschwitz-Birkenaus
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
trvala	trvat	k5eAaImAgFnS	trvat
od	od	k7c2	od
července	červenec	k1gInSc2	červenec
do	do	k7c2	do
září	září	k1gNnSc2	září
1942	[number]	k4	1942
<g/>
,	,	kIx,	,
vyžádala	vyžádat	k5eAaPmAgFnS	vyžádat
si	se	k3xPyFc3	se
odhadem	odhad	k1gInSc7	odhad
15	[number]	k4	15
<g/>
–	–	k?	–
<g/>
20	[number]	k4	20
tisíc	tisíc	k4xCgInPc2	tisíc
životů	život	k1gInPc2	život
a	a	k8xC	a
způsobila	způsobit	k5eAaPmAgFnS	způsobit
dočasné	dočasný	k2eAgNnSc4d1	dočasné
přerušení	přerušení	k1gNnSc4	přerušení
selekcí	selekce	k1gFnPc2	selekce
<g/>
.	.	kIx.	.
</s>
<s>
Časté	častý	k2eAgFnPc1d1	častá
byly	být	k5eAaImAgFnP	být
též	též	k9	též
zažívací	zažívací	k2eAgFnPc1d1	zažívací
potíže	potíž	k1gFnPc1	potíž
a	a	k8xC	a
"	"	kIx"	"
<g/>
zdánlivě	zdánlivě	k6eAd1	zdánlivě
nevyléčitelné	vyléčitelný	k2eNgFnPc4d1	nevyléčitelná
choroby	choroba	k1gFnPc4	choroba
nohou	noha	k1gFnPc2	noha
–	–	k?	–
otékaly	otékat	k5eAaImAgFnP	otékat
do	do	k7c2	do
takové	takový	k3xDgFnSc2	takový
míry	míra	k1gFnSc2	míra
<g/>
,	,	kIx,	,
že	že	k8xS	že
lidé	člověk	k1gMnPc1	člověk
nemohli	moct	k5eNaImAgMnP	moct
chodit	chodit	k5eAaImF	chodit
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
případech	případ	k1gInPc6	případ
mohli	moct	k5eAaImAgMnP	moct
být	být	k5eAaImF	být
obyvatelé	obyvatel	k1gMnPc1	obyvatel
vyhlazovacích	vyhlazovací	k2eAgFnPc2d1	vyhlazovací
tábora	tábor	k1gInSc2	tábor
zabiti	zabít	k5eAaPmNgMnP	zabít
svými	svůj	k3xOyFgMnPc7	svůj
spoluvězni	spoluvězeň	k1gMnPc7	spoluvězeň
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
těch	ten	k3xDgMnPc2	ten
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
kriminálních	kriminální	k2eAgMnPc2d1	kriminální
zločinců	zločinec	k1gMnPc2	zločinec
nebo	nebo	k8xC	nebo
kápů	kápo	k1gMnPc2	kápo
<g/>
;	;	kIx,	;
občas	občas	k6eAd1	občas
i	i	k9	i
bez	bez	k7c2	bez
zřejmého	zřejmý	k2eAgInSc2d1	zřejmý
důvodu	důvod	k1gInSc2	důvod
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
za	za	k7c4	za
drobný	drobný	k2eAgInSc4d1	drobný
prohřešek	prohřešek	k1gInSc4	prohřešek
jako	jako	k8xS	jako
např.	např.	kA	např.
nesundání	nesundání	k?	nesundání
čepice	čepice	k1gFnPc4	čepice
při	při	k7c6	při
setkání	setkání	k1gNnSc6	setkání
s	s	k7c7	s
esesmanem	esesman	k1gMnSc7	esesman
apod.	apod.	kA	apod.
Řádově	řádově	k6eAd1	řádově
stovky	stovka	k1gFnPc1	stovka
<g/>
,	,	kIx,	,
možná	možná	k9	možná
několik	několik	k4yIc1	několik
málo	málo	k6eAd1	málo
<g />
.	.	kIx.	.
</s>
<s>
tisíc	tisíc	k4xCgInSc1	tisíc
<g/>
,	,	kIx,	,
nedobrovolných	dobrovolný	k2eNgMnPc2d1	nedobrovolný
obyvatel	obyvatel	k1gMnPc2	obyvatel
koncentračních	koncentrační	k2eAgInPc2d1	koncentrační
a	a	k8xC	a
vyhlazovacích	vyhlazovací	k2eAgInPc2d1	vyhlazovací
táborů	tábor	k1gInPc2	tábor
se	se	k3xPyFc4	se
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
svého	svůj	k3xOyFgNnSc2	svůj
věznění	věznění	k1gNnSc2	věznění
pokusilo	pokusit	k5eAaPmAgNnS	pokusit
o	o	k7c4	o
útěk	útěk	k1gInSc4	útěk
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
během	během	k7c2	během
něj	on	k3xPp3gInSc2	on
zastřeleno	zastřelen	k2eAgNnSc1d1	zastřeleno
nebo	nebo	k8xC	nebo
oběšeno	oběšen	k2eAgNnSc1d1	oběšeno
před	před	k7c4	před
zraky	zrak	k1gInPc4	zrak
ostatních	ostatní	k1gNnPc2	ostatní
po	po	k7c6	po
jejich	jejich	k3xOp3gNnSc6	jejich
dopadení	dopadení	k1gNnSc6	dopadení
<g/>
;	;	kIx,	;
neznámý	známý	k2eNgMnSc1d1	neznámý
<g/>
,	,	kIx,	,
možná	možná	k9	možná
stejný	stejný	k2eAgInSc4d1	stejný
počet	počet	k1gInSc4	počet
vězňů	vězeň	k1gMnPc2	vězeň
nevydržel	vydržet	k5eNaPmAgMnS	vydržet
utrpení	utrpení	k1gNnSc4	utrpení
po	po	k7c6	po
stránce	stránka	k1gFnSc6	stránka
psychické	psychický	k2eAgFnSc6d1	psychická
a	a	k8xC	a
zvolil	zvolit	k5eAaPmAgMnS	zvolit
dobrovolný	dobrovolný	k2eAgInSc4d1	dobrovolný
odchod	odchod	k1gInSc4	odchod
ze	z	k7c2	z
života	život	k1gInSc2	život
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
vběhnutí	vběhnutí	k1gNnSc1	vběhnutí
do	do	k7c2	do
elektrického	elektrický	k2eAgNnSc2d1	elektrické
oplocení	oplocení	k1gNnSc2	oplocení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
desítek	desítka	k1gFnPc2	desítka
vězňů	vězeň	k1gMnPc2	vězeň
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
oběťmi	oběť	k1gFnPc7	oběť
pokusů	pokus	k1gInPc2	pokus
na	na	k7c6	na
lidech	člověk	k1gMnPc6	člověk
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
ve	v	k7c6	v
vybraných	vybraný	k2eAgInPc6d1	vybraný
táborech	tábor	k1gInPc6	tábor
probíhaly	probíhat	k5eAaImAgFnP	probíhat
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Vyhlazovací	vyhlazovací	k2eAgInPc4d1	vyhlazovací
tábory	tábor	k1gInPc4	tábor
nacistického	nacistický	k2eAgNnSc2d1	nacistické
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Nejúčinnějším	účinný	k2eAgInSc7d3	nejúčinnější
a	a	k8xC	a
nejhrůznějším	hrůzný	k2eAgInSc7d3	nejhrůznější
prostředkem	prostředek	k1gInSc7	prostředek
německého	německý	k2eAgInSc2d1	německý
holocaustu	holocaust	k1gInSc2	holocaust
byly	být	k5eAaImAgInP	být
vyhlazovací	vyhlazovací	k2eAgInPc1d1	vyhlazovací
tábory	tábor	k1gInPc1	tábor
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
hromadně	hromadně	k6eAd1	hromadně
popravovalo	popravovat	k5eAaImAgNnS	popravovat
(	(	kIx(	(
<g/>
v	v	k7c6	v
prvních	první	k4xOgFnPc6	první
fázích	fáze	k1gFnPc6	fáze
<g/>
)	)	kIx)	)
zastřelením	zastřelení	k1gNnSc7	zastřelení
<g/>
,	,	kIx,	,
oběšením	oběšení	k1gNnSc7	oběšení
a	a	k8xC	a
jedovatým	jedovatý	k2eAgInSc7d1	jedovatý
plynem	plyn	k1gInSc7	plyn
(	(	kIx(	(
<g/>
nejpoužívanější	používaný	k2eAgInSc1d3	nejpoužívanější
byl	být	k5eAaImAgInS	být
Cyklon	cyklon	k1gInSc1	cyklon
B	B	kA	B
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mrtvoly	mrtvola	k1gFnPc1	mrtvola
byly	být	k5eAaImAgFnP	být
poté	poté	k6eAd1	poté
hromadně	hromadně	k6eAd1	hromadně
pohřbívány	pohřbíván	k2eAgFnPc1d1	pohřbívána
či	či	k8xC	či
spalovány	spalován	k2eAgFnPc1d1	spalována
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
přepadení	přepadení	k1gNnSc6	přepadení
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
22	[number]	k4	22
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1941	[number]	k4	1941
(	(	kIx(	(
<g/>
Operace	operace	k1gFnSc1	operace
Barbarossa	Barbarossa	k1gMnSc1	Barbarossa
<g/>
)	)	kIx)	)
nechal	nechat	k5eAaPmAgMnS	nechat
Adolf	Adolf	k1gMnSc1	Adolf
Hitler	Hitler	k1gMnSc1	Hitler
na	na	k7c6	na
bývalém	bývalý	k2eAgNnSc6d1	bývalé
území	území	k1gNnSc6	území
Polska	Polsko	k1gNnSc2	Polsko
zřídit	zřídit	k5eAaPmF	zřídit
šest	šest	k4xCc4	šest
vyhlazovacích	vyhlazovací	k2eAgInPc2d1	vyhlazovací
táborů	tábor	k1gInPc2	tábor
<g/>
:	:	kIx,	:
Chełmno	Chełmno	k1gNnSc1	Chełmno
(	(	kIx(	(
<g/>
Kulmhof	Kulmhof	k1gInSc1	Kulmhof
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1941	[number]	k4	1941
–	–	k?	–
17	[number]	k4	17
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
přibližně	přibližně	k6eAd1	přibližně
320	[number]	k4	320
000	[number]	k4	000
obětí	oběť	k1gFnPc2	oběť
<g/>
)	)	kIx)	)
Auschwitz	Auschwitz	k1gInSc1	Auschwitz
–	–	k?	–
Výstavba	výstavba	k1gFnSc1	výstavba
původního	původní	k2eAgInSc2d1	původní
tábora	tábor	k1gInSc2	tábor
u	u	k7c2	u
města	město	k1gNnSc2	město
Osvětim	Osvětim	k1gFnSc4	Osvětim
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1940	[number]	k4	1940
<g/>
.	.	kIx.	.
</s>
<s>
Tábor	Tábor	k1gInSc1	Tábor
existoval	existovat	k5eAaImAgInS	existovat
do	do	k7c2	do
ledna	leden	k1gInSc2	leden
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
velitelem	velitel	k1gMnSc7	velitel
byl	být	k5eAaImAgMnS	být
Rudolf	Rudolf	k1gMnSc1	Rudolf
Höss	Hössa	k1gFnPc2	Hössa
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
<g/>
–	–	k?	–
<g/>
42	[number]	k4	42
se	se	k3xPyFc4	se
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
o	o	k7c4	o
Auschwitz	Auschwitz	k1gInSc4	Auschwitz
II	II	kA	II
–	–	k?	–
Birkenau	Birkenaus	k1gInSc2	Birkenaus
v	v	k7c6	v
nedaleké	daleký	k2eNgFnSc6d1	nedaleká
Březince	březinka	k1gFnSc6	březinka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1942	[number]	k4	1942
následoval	následovat	k5eAaImAgInS	následovat
Auschwitz	Auschwitz	k1gInSc1	Auschwitz
III	III	kA	III
v	v	k7c6	v
Monowicích	Monowik	k1gInPc6	Monowik
<g/>
.	.	kIx.	.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
počet	počet	k1gInSc1	počet
lidí	člověk	k1gMnPc2	člověk
zavražděných	zavražděný	k2eAgMnPc2d1	zavražděný
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
vyhlazovacím	vyhlazovací	k2eAgInSc6d1	vyhlazovací
táboře	tábor	k1gInSc6	tábor
nelze	lze	k6eNd1	lze
přesně	přesně	k6eAd1	přesně
určit	určit	k5eAaPmF	určit
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
počet	počet	k1gInSc1	počet
lidí	člověk	k1gMnPc2	člověk
usmrcených	usmrcený	k2eAgMnPc2d1	usmrcený
v	v	k7c6	v
plynových	plynový	k2eAgFnPc6d1	plynová
komorách	komora	k1gFnPc6	komora
nebyl	být	k5eNaImAgInS	být
registrován	registrovat	k5eAaBmNgInS	registrovat
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
údajů	údaj	k1gInPc2	údaj
Hösse	Hösse	k1gFnSc2	Hösse
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
osvětimském	osvětimský	k2eAgInSc6d1	osvětimský
vyhlazovacím	vyhlazovací	k2eAgInSc6d1	vyhlazovací
táboře	tábor	k1gInSc6	tábor
zplynováno	zplynovat	k5eAaPmNgNnS	zplynovat
1	[number]	k4	1
135	[number]	k4	135
000	[number]	k4	000
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Tábory	Tábor	k1gInPc1	Tábor
v	v	k7c6	v
Osvětimi	Osvětim	k1gFnSc6	Osvětim
a	a	k8xC	a
Březince	březinka	k1gFnSc6	březinka
jsou	být	k5eAaImIp3nP	být
dnes	dnes	k6eAd1	dnes
přístupné	přístupný	k2eAgFnPc4d1	přístupná
veřejnosti	veřejnost	k1gFnPc4	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Majdanek	Majdanka	k1gFnPc2	Majdanka
(	(	kIx(	(
<g/>
červenec	červenec	k1gInSc1	červenec
1941	[number]	k4	1941
–	–	k?	–
červenec	červenec	k1gInSc1	červenec
1944	[number]	k4	1944
<g/>
,	,	kIx,	,
78	[number]	k4	78
000	[number]	k4	000
až	až	k9	až
360	[number]	k4	360
000	[number]	k4	000
obětí	oběť	k1gFnPc2	oběť
<g/>
)	)	kIx)	)
Bełżec	Bełżec	k1gInSc1	Bełżec
(	(	kIx(	(
<g/>
březen	březen	k1gInSc1	březen
1942	[number]	k4	1942
–	–	k?	–
červen	červen	k1gInSc1	červen
1943	[number]	k4	1943
<g/>
,	,	kIx,	,
600	[number]	k4	600
000	[number]	k4	000
obětí	oběť	k1gFnPc2	oběť
<g/>
)	)	kIx)	)
Sobibor	Sobibor	k1gInSc1	Sobibor
(	(	kIx(	(
<g/>
květen	květen	k1gInSc1	květen
1942	[number]	k4	1942
–	–	k?	–
říjen	říjen	k1gInSc1	říjen
1943	[number]	k4	1943
<g/>
,	,	kIx,	,
250	[number]	k4	250
000	[number]	k4	000
obětí	oběť	k1gFnPc2	oběť
<g/>
)	)	kIx)	)
Treblinka	Treblinka	k1gFnSc1	Treblinka
(	(	kIx(	(
<g/>
červenec	červenec	k1gInSc1	červenec
1942	[number]	k4	1942
–	–	k?	–
říjen	říjen	k1gInSc1	říjen
1943	[number]	k4	1943
<g/>
,	,	kIx,	,
nejméně	málo	k6eAd3	málo
800	[number]	k4	800
000	[number]	k4	000
obětí	oběť	k1gFnPc2	oběť
<g/>
)	)	kIx)	)
Představa	představa	k1gFnSc1	představa
<g/>
,	,	kIx,	,
že	že	k8xS	že
holokaust	holokaust	k1gInSc1	holokaust
rovná	rovnat	k5eAaImIp3nS	rovnat
se	se	k3xPyFc4	se
koncentrační	koncentrační	k2eAgInPc1d1	koncentrační
tábory	tábor	k1gInPc1	tábor
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
však	však	k9	však
zjednodušující	zjednodušující	k2eAgMnSc1d1	zjednodušující
<g/>
.	.	kIx.	.
</s>
<s>
Holokaust	holokaust	k1gInSc1	holokaust
začal	začít	k5eAaPmAgInS	začít
masovými	masový	k2eAgFnPc7d1	masová
popravami	poprava	k1gFnPc7	poprava
střelnými	střelný	k2eAgFnPc7d1	střelná
zbraněmi	zbraň	k1gFnPc7	zbraň
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
na	na	k7c6	na
dobytých	dobytý	k2eAgNnPc6d1	dobyté
územích	území	k1gNnPc6	území
Polska	Polsko	k1gNnSc2	Polsko
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Generální	generální	k2eAgNnSc1d1	generální
gouvernement	gouvernement	k1gNnSc1	gouvernement
<g/>
)	)	kIx)	)
a	a	k8xC	a
dobytých	dobytý	k2eAgNnPc6d1	dobyté
územích	území	k1gNnPc6	území
SSSR	SSSR	kA	SSSR
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Říšský	říšský	k2eAgInSc1d1	říšský
komisariát	komisariát	k1gInSc1	komisariát
Ostland	Ostland	k1gInSc1	Ostland
a	a	k8xC	a
Říšský	říšský	k2eAgInSc1d1	říšský
komisariát	komisariát	k1gInSc1	komisariát
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
<g/>
)	)	kIx)	)
začaly	začít	k5eAaPmAgFnP	začít
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1941	[number]	k4	1941
provádět	provádět	k5eAaImF	provádět
zejména	zejména	k9	zejména
speciální	speciální	k2eAgFnPc4d1	speciální
jednotky	jednotka	k1gFnPc4	jednotka
Einsatzgruppen	Einsatzgruppen	k2eAgMnSc1d1	Einsatzgruppen
<g/>
.	.	kIx.	.
</s>
<s>
Uvádí	uvádět	k5eAaImIp3nS	uvádět
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
bylo	být	k5eAaImAgNnS	být
zlikvidováno	zlikvidovat	k5eAaPmNgNnS	zlikvidovat
1,25	[number]	k4	1,25
či	či	k9wB	či
1,5	[number]	k4	1,5
milionů	milion	k4xCgInPc2	milion
Židů	Žid	k1gMnPc2	Žid
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
téměř	téměř	k6eAd1	téměř
jedna	jeden	k4xCgFnSc1	jeden
čtvrtina	čtvrtina	k1gFnSc1	čtvrtina
všech	všecek	k3xTgFnPc2	všecek
obětí	oběť	k1gFnPc2	oběť
holokaustu	holokaust	k1gInSc2	holokaust
<g/>
.	.	kIx.	.
</s>
<s>
Typický	typický	k2eAgInSc1d1	typický
scénář	scénář	k1gInSc1	scénář
těchto	tento	k3xDgFnPc2	tento
masových	masový	k2eAgFnPc2d1	masová
poprav	poprava	k1gFnPc2	poprava
spočíval	spočívat	k5eAaImAgInS	spočívat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
oběti	oběť	k1gFnPc1	oběť
vykopaly	vykopat	k5eAaPmAgFnP	vykopat
rokle	rokle	k1gFnPc4	rokle
<g/>
,	,	kIx,	,
do	do	k7c2	do
nichž	jenž	k3xRgFnPc2	jenž
pak	pak	k6eAd1	pak
postupně	postupně	k6eAd1	postupně
po	po	k7c6	po
řadách	řada	k1gFnPc6	řada
vstupovaly	vstupovat	k5eAaImAgFnP	vstupovat
a	a	k8xC	a
zde	zde	k6eAd1	zde
byly	být	k5eAaImAgInP	být
stříleny	střílet	k5eAaImNgInP	střílet
do	do	k7c2	do
hlavy	hlava	k1gFnSc2	hlava
–	–	k?	–
je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
zdokumentováno	zdokumentován	k2eAgNnSc1d1	zdokumentováno
fotograficky	fotograficky	k6eAd1	fotograficky
i	i	k9	i
na	na	k7c6	na
kinofilmu	kinofilm	k1gInSc6	kinofilm
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgInSc7d3	veliký
jednotlivým	jednotlivý	k2eAgInSc7d1	jednotlivý
masakrem	masakr	k1gInSc7	masakr
byla	být	k5eAaImAgFnS	být
poprava	poprava	k1gFnSc1	poprava
více	hodně	k6eAd2	hodně
než	než	k8xS	než
30	[number]	k4	30
tisíc	tisíc	k4xCgInPc2	tisíc
kyjevských	kyjevský	k2eAgMnPc2d1	kyjevský
Židů	Žid	k1gMnPc2	Žid
v	v	k7c6	v
Babím	babí	k2eAgInSc6d1	babí
Jaru	jar	k1gInSc6	jar
na	na	k7c6	na
konci	konec	k1gInSc6	konec
září	zářit	k5eAaImIp3nS	zářit
1941	[number]	k4	1941
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
k	k	k7c3	k
plynovým	plynový	k2eAgFnPc3d1	plynová
komorám	komora	k1gFnPc3	komora
se	se	k3xPyFc4	se
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
raných	raný	k2eAgInPc6d1	raný
fázích	fág	k1gInPc6	fág
konečného	konečný	k2eAgNnSc2d1	konečné
řešení	řešení	k1gNnSc2	řešení
teprve	teprve	k6eAd1	teprve
objevovala	objevovat	k5eAaImAgFnS	objevovat
<g/>
.	.	kIx.	.
</s>
<s>
Nacisté	nacista	k1gMnPc1	nacista
experimentovali	experimentovat	k5eAaImAgMnP	experimentovat
a	a	k8xC	a
hledali	hledat	k5eAaImAgMnP	hledat
co	co	k9	co
nejefektivnější	efektivní	k2eAgInSc4d3	nejefektivnější
způsob	způsob	k1gInSc4	způsob
vraždění	vraždění	k1gNnSc2	vraždění
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
by	by	kYmCp3nS	by
zároveň	zároveň	k6eAd1	zároveň
nepůsobil	působit	k5eNaImAgInS	působit
tak	tak	k9	tak
silně	silně	k6eAd1	silně
na	na	k7c4	na
psychiku	psychika	k1gFnSc4	psychika
vrahů	vrah	k1gMnPc2	vrah
jako	jako	k8xS	jako
hromadné	hromadný	k2eAgNnSc1d1	hromadné
střílení	střílení	k1gNnSc1	střílení
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
prováděly	provádět	k5eAaImAgFnP	provádět
Einsatzgruppen	Einsatzgruppen	k2eAgInSc4d1	Einsatzgruppen
na	na	k7c6	na
dobytých	dobytý	k2eAgNnPc6d1	dobyté
územích	území	k1gNnPc6	území
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k6eAd1	právě
Einsatzgruppen	Einsatzgruppen	k2eAgInSc1d1	Einsatzgruppen
začaly	začít	k5eAaPmAgFnP	začít
od	od	k7c2	od
listopadu	listopad	k1gInSc2	listopad
1941	[number]	k4	1941
využívat	využívat	k5eAaImF	využívat
nový	nový	k2eAgInSc4d1	nový
způsob	způsob	k1gInSc4	způsob
vraždění	vraždění	k1gNnSc2	vraždění
–	–	k?	–
mobilní	mobilní	k2eAgFnSc2d1	mobilní
plynové	plynový	k2eAgFnSc2d1	plynová
komory	komora	k1gFnSc2	komora
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
byly	být	k5eAaImAgFnP	být
oběti	oběť	k1gFnPc4	oběť
zabíjeny	zabíjen	k2eAgFnPc4d1	zabíjena
výfukovým	výfukový	k2eAgInSc7d1	výfukový
plynem	plyn	k1gInSc7	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
roku	rok	k1gInSc2	rok
1942	[number]	k4	1942
vznikala	vznikat	k5eAaImAgNnP	vznikat
postupně	postupně	k6eAd1	postupně
vyhlazovací	vyhlazovací	k2eAgNnPc1d1	vyhlazovací
centra	centrum	k1gNnPc1	centrum
vybavená	vybavený	k2eAgNnPc1d1	vybavené
stálými	stálý	k2eAgFnPc7d1	stálá
plynovými	plynový	k2eAgFnPc7d1	plynová
komorami	komora	k1gFnPc7	komora
<g/>
.	.	kIx.	.
</s>
<s>
Nacistické	nacistický	k2eAgNnSc1d1	nacistické
Německo	Německo	k1gNnSc1	Německo
se	se	k3xPyFc4	se
při	při	k7c6	při
likvidaci	likvidace	k1gFnSc6	likvidace
Židů	Žid	k1gMnPc2	Žid
soustředilo	soustředit	k5eAaPmAgNnS	soustředit
nejen	nejen	k6eAd1	nejen
na	na	k7c4	na
Židy	Žid	k1gMnPc4	Žid
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
nacházeli	nacházet	k5eAaImAgMnP	nacházet
na	na	k7c6	na
"	"	kIx"	"
<g/>
předválečném	předválečný	k2eAgNnSc6d1	předválečné
<g/>
"	"	kIx"	"
německém	německý	k2eAgNnSc6d1	německé
území	území	k1gNnSc6	území
nebo	nebo	k8xC	nebo
na	na	k7c6	na
území	území	k1gNnSc6	území
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
nímž	jenž	k3xRgInSc7	jenž
Německo	Německo	k1gNnSc1	Německo
získalo	získat	k5eAaPmAgNnS	získat
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
války	válka	k1gFnSc2	válka
kontrolu	kontrola	k1gFnSc4	kontrola
<g/>
.	.	kIx.	.
</s>
<s>
Vyžadovalo	vyžadovat	k5eAaImAgNnS	vyžadovat
též	též	k9	též
vydání	vydání	k1gNnSc1	vydání
Židů	Žid	k1gMnPc2	Žid
po	po	k7c6	po
svých	svůj	k3xOyFgMnPc6	svůj
spojencích	spojenec	k1gMnPc6	spojenec
a	a	k8xC	a
satelitech	satelit	k1gMnPc6	satelit
<g/>
.	.	kIx.	.
</s>
<s>
Různé	různý	k2eAgInPc1d1	různý
státy	stát	k1gInPc1	stát
se	se	k3xPyFc4	se
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
otázce	otázka	k1gFnSc3	otázka
postavily	postavit	k5eAaPmAgInP	postavit
různě	různě	k6eAd1	různě
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nejvstřícnějších	vstřícný	k2eAgInPc2d3	nejvstřícnější
států	stát	k1gInPc2	stát
lze	lze	k6eAd1	lze
označit	označit	k5eAaPmF	označit
Slovenský	slovenský	k2eAgInSc1d1	slovenský
štát	štát	k1gInSc1	štát
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
nejenže	nejenže	k6eAd1	nejenže
Židy	Žid	k1gMnPc4	Žid
vydával	vydávat	k5eAaPmAgInS	vydávat
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
z	z	k7c2	z
vlastní	vlastní	k2eAgFnSc2d1	vlastní
iniciativy	iniciativa	k1gFnSc2	iniciativa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
navíc	navíc	k6eAd1	navíc
za	za	k7c4	za
jejich	jejich	k3xOp3gInSc4	jejich
odvoz	odvoz	k1gInSc4	odvoz
nacistickému	nacistický	k2eAgNnSc3d1	nacistické
Německu	Německo	k1gNnSc3	Německo
z	z	k7c2	z
vlastní	vlastní	k2eAgFnSc2d1	vlastní
iniciativy	iniciativa	k1gFnSc2	iniciativa
platil	platit	k5eAaImAgInS	platit
(	(	kIx(	(
<g/>
nutno	nutno	k6eAd1	nutno
ovšem	ovšem	k9	ovšem
dodat	dodat	k5eAaPmF	dodat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vydávání	vydávání	k1gNnSc1	vydávání
bylo	být	k5eAaImAgNnS	být
několikráte	několikráte	k6eAd1	několikráte
přerušeno	přerušit	k5eAaPmNgNnS	přerušit
a	a	k8xC	a
dokončeno	dokončit	k5eAaPmNgNnS	dokončit
bylo	být	k5eAaImAgNnS	být
až	až	k6eAd1	až
po	po	k7c6	po
propuknutí	propuknutí	k1gNnSc6	propuknutí
slovenského	slovenský	k2eAgNnSc2d1	slovenské
národního	národní	k2eAgNnSc2d1	národní
povstání	povstání	k1gNnSc2	povstání
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
řadě	řada	k1gFnSc6	řada
dalších	další	k2eAgFnPc2d1	další
zemí	zem	k1gFnPc2	zem
bylo	být	k5eAaImAgNnS	být
vydávání	vydávání	k1gNnSc4	vydávání
ve	v	k7c6	v
větší	veliký	k2eAgFnSc6d2	veliký
míře	míra	k1gFnSc6	míra
uskutečněno	uskutečnit	k5eAaPmNgNnS	uskutečnit
až	až	k9	až
na	na	k7c4	na
nátlak	nátlak	k1gInSc4	nátlak
nacistů	nacista	k1gMnPc2	nacista
podpořený	podpořený	k2eAgInSc1d1	podpořený
růstem	růst	k1gInSc7	růst
moci	moc	k1gFnSc2	moc
pronacistických	pronacistický	k2eAgFnPc2d1	pronacistická
bojůvek	bojůvka	k1gFnPc2	bojůvka
a	a	k8xC	a
radikálním	radikální	k2eAgNnSc6d1	radikální
oslabení	oslabení	k1gNnSc6	oslabení
centrální	centrální	k2eAgFnSc2d1	centrální
vlády	vláda	k1gFnSc2	vláda
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
vlažný	vlažný	k2eAgInSc4d1	vlažný
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
pronásledování	pronásledování	k1gNnSc3	pronásledování
Židů	Žid	k1gMnPc2	Žid
měla	mít	k5eAaImAgFnS	mít
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
i	i	k9	i
ona	onen	k3xDgFnSc1	onen
přijala	přijmout	k5eAaPmAgFnS	přijmout
protižidovské	protižidovský	k2eAgInPc4d1	protižidovský
zákony	zákon	k1gInPc4	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Zcela	zcela	k6eAd1	zcela
odmítavě	odmítavě	k6eAd1	odmítavě
se	se	k3xPyFc4	se
k	k	k7c3	k
celé	celý	k2eAgFnSc3d1	celá
záležitosti	záležitost	k1gFnSc3	záležitost
postavilo	postavit	k5eAaPmAgNnS	postavit
Finsko	Finsko	k1gNnSc1	Finsko
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
žádné	žádný	k3yNgNnSc1	žádný
protižidovské	protižidovský	k2eAgInPc4d1	protižidovský
zákony	zákon	k1gInPc4	zákon
nepřijalo	přijmout	k5eNaPmAgNnS	přijmout
a	a	k8xC	a
přes	přes	k7c4	přes
silný	silný	k2eAgInSc4d1	silný
nátlak	nátlak	k1gInSc4	nátlak
striktně	striktně	k6eAd1	striktně
odmítlo	odmítnout	k5eAaPmAgNnS	odmítnout
vydat	vydat	k5eAaPmF	vydat
nejen	nejen	k6eAd1	nejen
své	svůj	k3xOyFgMnPc4	svůj
občany	občan	k1gMnPc4	občan
židovského	židovský	k2eAgInSc2d1	židovský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
drtivou	drtivý	k2eAgFnSc4d1	drtivá
většinu	většina	k1gFnSc4	většina
Židů	Žid	k1gMnPc2	Žid
uprchlých	uprchlý	k1gMnPc2	uprchlý
ze	z	k7c2	z
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
nacistické	nacistický	k2eAgNnSc1d1	nacistické
Německo	Německo	k1gNnSc1	Německo
ovládlo	ovládnout	k5eAaPmAgNnS	ovládnout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
řadě	řada	k1gFnSc6	řada
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
své	svůj	k3xOyFgMnPc4	svůj
židovské	židovský	k2eAgMnPc4d1	židovský
obyvatele	obyvatel	k1gMnPc4	obyvatel
a	a	k8xC	a
uprchlíky	uprchlík	k1gMnPc4	uprchlík
vydávaly	vydávat	k5eAaPmAgFnP	vydávat
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
deportace	deportace	k1gFnPc1	deportace
přerušovány	přerušovat	k5eAaImNgFnP	přerušovat
a	a	k8xC	a
pozastavovány	pozastavovat	k5eAaImNgFnP	pozastavovat
díky	díky	k7c3	díky
diplomatickému	diplomatický	k2eAgNnSc3d1	diplomatické
úsilí	úsilí	k1gNnSc3	úsilí
neutrálních	neutrální	k2eAgFnPc2d1	neutrální
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Významná	významný	k2eAgFnSc1d1	významná
byla	být	k5eAaImAgFnS	být
snaha	snaha	k1gFnSc1	snaha
zejména	zejména	k9	zejména
Vatikánu	Vatikán	k1gInSc2	Vatikán
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
opakovaně	opakovaně	k6eAd1	opakovaně
protestoval	protestovat	k5eAaBmAgMnS	protestovat
proti	proti	k7c3	proti
deportacím	deportace	k1gFnPc3	deportace
především	především	k9	především
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
,	,	kIx,	,
v	v	k7c6	v
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
a	a	k8xC	a
Chorvatsku	Chorvatsko	k1gNnSc6	Chorvatsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
jeho	jeho	k3xOp3gInSc1	jeho
diplomatický	diplomatický	k2eAgInSc1d1	diplomatický
vliv	vliv	k1gInSc1	vliv
nejsilnější	silný	k2eAgFnSc4d3	nejsilnější
a	a	k8xC	a
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
podle	podle	k7c2	podle
jeho	jeho	k3xOp3gNnSc2	jeho
vedení	vedení	k1gNnSc2	vedení
naděje	nadát	k5eAaBmIp3nS	nadát
na	na	k7c4	na
pozitivní	pozitivní	k2eAgInSc4d1	pozitivní
účinek	účinek	k1gInSc4	účinek
protestů	protest	k1gInPc2	protest
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1944	[number]	k4	1944
vedl	vést	k5eAaImAgMnS	vést
papežský	papežský	k2eAgMnSc1d1	papežský
nuncius	nuncius	k1gMnSc1	nuncius
Angelo	Angela	k1gFnSc5	Angela
Rotta	Rott	k1gMnSc4	Rott
společnou	společný	k2eAgFnSc4d1	společná
delegaci	delegace	k1gFnSc4	delegace
pěti	pět	k4xCc2	pět
šéfů	šéf	k1gMnPc2	šéf
diplomatických	diplomatický	k2eAgNnPc2d1	diplomatické
zastoupení	zastoupení	k1gNnPc2	zastoupení
neutrálních	neutrální	k2eAgFnPc2d1	neutrální
zemí	zem	k1gFnPc2	zem
protestujících	protestující	k2eAgFnPc2d1	protestující
proti	proti	k7c3	proti
vydávání	vydávání	k1gNnSc6	vydávání
Židů	Žid	k1gMnPc2	Žid
do	do	k7c2	do
Německa	Německo	k1gNnSc2	Německo
(	(	kIx(	(
<g/>
protest	protest	k1gInSc4	protest
připojilo	připojit	k5eAaPmAgNnS	připojit
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
<g/>
,	,	kIx,	,
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
,	,	kIx,	,
Švédsko	Švédsko	k1gNnSc1	Švédsko
a	a	k8xC	a
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
světě	svět	k1gInSc6	svět
byl	být	k5eAaImAgInS	být
27	[number]	k4	27
<g/>
.	.	kIx.	.
leden	leden	k1gInSc1	leden
zvolen	zvolit	k5eAaPmNgInS	zvolit
jako	jako	k8xC	jako
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
den	den	k1gInSc1	den
památky	památka	k1gFnSc2	památka
obětí	oběť	k1gFnPc2	oběť
holocaustu	holocaust	k1gInSc2	holocaust
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Židé	Žid	k1gMnPc1	Žid
v	v	k7c6	v
Protektorátu	protektorát	k1gInSc6	protektorát
Čechy	Čechy	k1gFnPc1	Čechy
a	a	k8xC	a
Morava	Morava	k1gFnSc1	Morava
a	a	k8xC	a
Porajmos	Porajmos	k1gMnSc1	Porajmos
<g/>
#	#	kIx~	#
<g/>
Porajmos	Porajmos	k1gMnSc1	Porajmos
v	v	k7c6	v
Protektorátu	protektorát	k1gInSc6	protektorát
Čechy	Čechy	k1gFnPc1	Čechy
a	a	k8xC	a
Morava	Morava	k1gFnSc1	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Holokaust	holokaust	k1gInSc1	holokaust
v	v	k7c6	v
protektorátu	protektorát	k1gInSc6	protektorát
se	se	k3xPyFc4	se
odehrával	odehrávat	k5eAaImAgInS	odehrávat
pod	pod	k7c7	pod
taktovkou	taktovka	k1gFnSc7	taktovka
okupačních	okupační	k2eAgInPc2d1	okupační
úřadů	úřad	k1gInPc2	úřad
a	a	k8xC	a
až	až	k9	až
na	na	k7c4	na
výjimky	výjimka	k1gFnPc4	výjimka
bez	bez	k7c2	bez
odporu	odpor	k1gInSc2	odpor
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
za	za	k7c2	za
spolupráce	spolupráce	k1gFnSc2	spolupráce
českých	český	k2eAgInPc2d1	český
úřadů	úřad	k1gInPc2	úřad
a	a	k8xC	a
úředníků	úředník	k1gMnPc2	úředník
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
druhou	druhý	k4xOgFnSc7	druhý
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
žilo	žít	k5eAaImAgNnS	žít
na	na	k7c6	na
dnešním	dnešní	k2eAgNnSc6d1	dnešní
území	území	k1gNnSc6	území
ČR	ČR	kA	ČR
více	hodně	k6eAd2	hodně
než	než	k8xS	než
118	[number]	k4	118
tisíc	tisíc	k4xCgInSc4	tisíc
Židů	Žid	k1gMnPc2	Žid
<g/>
;	;	kIx,	;
26	[number]	k4	26
tisícům	tisíc	k4xCgInPc3	tisíc
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1941	[number]	k4	1941
emigrovat	emigrovat	k5eAaBmF	emigrovat
<g/>
,	,	kIx,	,
potom	potom	k6eAd1	potom
bylo	být	k5eAaImAgNnS	být
protektorátními	protektorátní	k2eAgInPc7d1	protektorátní
úřady	úřad	k1gInPc7	úřad
vystěhování	vystěhování	k1gNnSc2	vystěhování
zakázáno	zakázat	k5eAaPmNgNnS	zakázat
a	a	k8xC	a
současně	současně	k6eAd1	současně
začaly	začít	k5eAaPmAgInP	začít
transporty	transport	k1gInPc1	transport
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
80	[number]	k4	80
tisíc	tisíc	k4xCgInPc2	tisíc
osob	osoba	k1gFnPc2	osoba
bylo	být	k5eAaImAgNnS	být
během	během	k7c2	během
holokaustu	holokaust	k1gInSc2	holokaust
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
vyvražděno	vyvražděn	k2eAgNnSc1d1	vyvražděno
<g/>
.	.	kIx.	.
</s>
<s>
Silnou	silný	k2eAgFnSc4d1	silná
kritiku	kritika	k1gFnSc4	kritika
si	se	k3xPyFc3	se
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
době	doba	k1gFnSc6	doba
vysloužilo	vysloužit	k5eAaPmAgNnS	vysloužit
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
Československa	Československo	k1gNnSc2	Československo
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zakázalo	zakázat	k5eAaPmAgNnS	zakázat
rakouským	rakouský	k2eAgMnPc3d1	rakouský
Židům	Žid	k1gMnPc3	Žid
vstup	vstup	k1gInSc4	vstup
na	na	k7c4	na
své	svůj	k3xOyFgNnSc4	svůj
území	území	k1gNnSc4	území
a	a	k8xC	a
vrátilo	vrátit	k5eAaPmAgNnS	vrátit
do	do	k7c2	do
Rakouska	Rakousko	k1gNnSc2	Rakousko
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
nacistů	nacista	k1gMnPc2	nacista
tisíce	tisíc	k4xCgInPc1	tisíc
židovských	židovský	k2eAgMnPc2d1	židovský
uprchlíků	uprchlík	k1gMnPc2	uprchlík
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Zpochybňování	zpochybňování	k1gNnSc2	zpochybňování
holokaustu	holokaust	k1gInSc2	holokaust
<g/>
.	.	kIx.	.
</s>
<s>
Holokaust	holokaust	k1gInSc1	holokaust
je	být	k5eAaImIp3nS	být
či	či	k8xC	či
některé	některý	k3yIgFnPc4	některý
z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
částí	část	k1gFnPc2	část
jsou	být	k5eAaImIp3nP	být
občas	občas	k6eAd1	občas
terčem	terč	k1gInSc7	terč
zpochybňování	zpochybňování	k1gNnSc2	zpochybňování
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
v	v	k7c6	v
drtivé	drtivý	k2eAgFnSc6d1	drtivá
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
není	být	k5eNaImIp3nS	být
zpochybňována	zpochybňován	k2eAgFnSc1d1	zpochybňována
perzekuce	perzekuce	k1gFnSc1	perzekuce
Židů	Žid	k1gMnPc2	Žid
jako	jako	k9	jako
celek	celek	k1gInSc4	celek
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
fenomén	fenomén	k1gInSc4	fenomén
vžité	vžitý	k2eAgFnSc2d1	vžitá
taktéž	taktéž	k?	taktéž
označení	označení	k1gNnSc1	označení
popírání	popírání	k1gNnPc2	popírání
holokaustu	holokaust	k1gInSc2	holokaust
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
státech	stát	k1gInPc6	stát
světa	svět	k1gInSc2	svět
včetně	včetně	k7c2	včetně
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
je	být	k5eAaImIp3nS	být
zpochybňování	zpochybňování	k1gNnSc1	zpochybňování
holokaustu	holokaust	k1gInSc2	holokaust
<g/>
,	,	kIx,	,
též	též	k9	též
označované	označovaný	k2eAgFnPc1d1	označovaná
jako	jako	k8xS	jako
hlásání	hlásání	k1gNnSc1	hlásání
Osvětimské	osvětimský	k2eAgFnSc2d1	Osvětimská
lži	lež	k1gFnSc2	lež
<g/>
,	,	kIx,	,
trestným	trestný	k2eAgInSc7d1	trestný
činem	čin	k1gInSc7	čin
<g/>
.	.	kIx.	.
</s>
<s>
Zpochybňování	zpochybňování	k1gNnSc1	zpochybňování
holokaustu	holokaust	k1gInSc2	holokaust
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
širšího	široký	k2eAgInSc2d2	širší
směru	směr	k1gInSc2	směr
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
historického	historický	k2eAgInSc2d1	historický
revizionismu	revizionismus	k1gInSc2	revizionismus
<g/>
.	.	kIx.	.
</s>
<s>
Zpochybňování	zpochybňování	k1gNnSc1	zpochybňování
holokaustu	holokaust	k1gInSc2	holokaust
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
vědeckými	vědecký	k2eAgFnPc7d1	vědecká
metodami	metoda	k1gFnPc7	metoda
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nových	nový	k2eAgInPc2d1	nový
proudů	proud	k1gInPc2	proud
tohoto	tento	k3xDgNnSc2	tento
původně	původně	k6eAd1	původně
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
seriózního	seriózní	k2eAgInSc2d1	seriózní
historického	historický	k2eAgInSc2d1	historický
proudu	proud	k1gInSc2	proud
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
tak	tak	k6eAd1	tak
značně	značně	k6eAd1	značně
zdiskreditovalo	zdiskreditovat	k5eAaPmAgNnS	zdiskreditovat
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
americké	americký	k2eAgFnSc2d1	americká
historičky	historička	k1gFnSc2	historička
Debory	Debora	k1gFnSc2	Debora
Lipstadt	Lipstadta	k1gFnPc2	Lipstadta
seriózní	seriózní	k2eAgMnPc1d1	seriózní
historikové	historik	k1gMnPc1	historik
tento	tento	k3xDgInSc4	tento
směr	směr	k1gInSc4	směr
odmítají	odmítat	k5eAaImIp3nP	odmítat
a	a	k8xC	a
vyčítají	vyčítat	k5eAaImIp3nP	vyčítat
mu	on	k3xPp3gNnSc3	on
pseudovědecké	pseudovědecký	k2eAgFnPc1d1	pseudovědecká
metody	metoda	k1gFnPc1	metoda
práce	práce	k1gFnSc2	práce
a	a	k8xC	a
neseriózní	seriózní	k2eNgFnSc4d1	neseriózní
práci	práce	k1gFnSc4	práce
s	s	k7c7	s
prameny	pramen	k1gInPc7	pramen
a	a	k8xC	a
citacemi	citace	k1gFnPc7	citace
s	s	k7c7	s
účelem	účel	k1gInSc7	účel
falšovat	falšovat	k5eAaImF	falšovat
historii	historie	k1gFnSc4	historie
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgFnSc1d2	veliký
část	část	k1gFnSc1	část
revizionistů	revizionista	k1gMnPc2	revizionista
nezpochybňuje	zpochybňovat	k5eNaImIp3nS	zpochybňovat
holokaust	holokaust	k1gInSc4	holokaust
jako	jako	k8xS	jako
celek	celek	k1gInSc4	celek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gFnSc4	jeho
plánovanost	plánovanost	k1gFnSc4	plánovanost
či	či	k8xC	či
rozsah	rozsah	k1gInSc4	rozsah
a	a	k8xC	a
počet	počet	k1gInSc4	počet
obětí	oběť	k1gFnPc2	oběť
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
plného	plný	k2eAgNnSc2d1	plné
uznání	uznání	k1gNnSc2	uznání
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
a	a	k8xC	a
celkového	celkový	k2eAgNnSc2d1	celkové
nebo	nebo	k8xC	nebo
částečného	částečný	k2eAgNnSc2d1	částečné
zpochybnění	zpochybnění	k1gNnSc2	zpochybnění
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
druhé	druhý	k4xOgFnSc6	druhý
existuje	existovat	k5eAaImIp3nS	existovat
k	k	k7c3	k
holokaustu	holokaust	k1gInSc3	holokaust
i	i	k9	i
jakýsi	jakýsi	k3yIgInSc1	jakýsi
"	"	kIx"	"
<g/>
třetí	třetí	k4xOgInSc4	třetí
<g/>
"	"	kIx"	"
postoj	postoj	k1gInSc4	postoj
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
tento	tento	k3xDgInSc1	tento
je	být	k5eAaImIp3nS	být
minoritně	minoritně	k6eAd1	minoritně
zastoupený	zastoupený	k2eAgInSc1d1	zastoupený
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
holokaust	holokaust	k1gInSc1	holokaust
jako	jako	k8xC	jako
takový	takový	k3xDgInSc4	takový
nezpochybňuje	zpochybňovat	k5eNaImIp3nS	zpochybňovat
–	–	k?	–
nezaobírá	zaobírat	k5eNaImIp3nS	zaobírat
se	se	k3xPyFc4	se
ani	ani	k8xC	ani
tak	tak	k9	tak
jím	on	k3xPp3gMnSc7	on
samotným	samotný	k2eAgMnSc7d1	samotný
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
spíš	spíš	k9	spíš
jeho	jeho	k3xOp3gInSc7	jeho
odkazem	odkaz	k1gInSc7	odkaz
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
památka	památka	k1gFnSc1	památka
vnímána	vnímán	k2eAgFnSc1d1	vnímána
a	a	k8xC	a
jaký	jaký	k3yQgInSc1	jaký
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
vliv	vliv	k1gInSc4	vliv
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
zdali	zdali	k8xS	zdali
nebyl	být	k5eNaImAgInS	být
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
zneužit	zneužít	k5eAaPmNgMnS	zneužít
pro	pro	k7c4	pro
ideologické	ideologický	k2eAgInPc4d1	ideologický
<g/>
,	,	kIx,	,
politické	politický	k2eAgInPc4d1	politický
či	či	k8xC	či
jiné	jiný	k2eAgInPc4d1	jiný
cíle	cíl	k1gInPc4	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgMnSc7d1	hlavní
představitelem	představitel	k1gMnSc7	představitel
je	být	k5eAaImIp3nS	být
Norman	Norman	k1gMnSc1	Norman
Finkelstein	Finkelstein	k1gMnSc1	Finkelstein
(	(	kIx(	(
<g/>
sám	sám	k3xTgMnSc1	sám
židovského	židovský	k2eAgMnSc4d1	židovský
původu	původa	k1gMnSc4	původa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
téma	téma	k1gNnSc4	téma
napsal	napsat	k5eAaBmAgInS	napsat
knihu	kniha	k1gFnSc4	kniha
Průmysl	průmysl	k1gInSc1	průmysl
holocaustu	holocaust	k1gInSc2	holocaust
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
přeložena	přeložit	k5eAaPmNgFnS	přeložit
do	do	k7c2	do
16	[number]	k4	16
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Zavádí	zavádět	k5eAaImIp3nS	zavádět
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
pojem	pojem	k1gInSc4	pojem
průmysl	průmysl	k1gInSc1	průmysl
holokaustu	holokaust	k1gInSc2	holokaust
a	a	k8xC	a
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
několik	několik	k4yIc1	několik
otázek	otázka	k1gFnPc2	otázka
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
<g/>
:	:	kIx,	:
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
holokaust	holokaust	k1gInSc4	holokaust
obhajuje	obhajovat	k5eAaImIp3nS	obhajovat
sám	sám	k3xTgMnSc1	sám
sebe	sebe	k3xPyFc4	sebe
a	a	k8xC	a
jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
staví	stavit	k5eAaPmIp3nS	stavit
nad	nad	k7c4	nad
podobné	podobný	k2eAgFnPc4d1	podobná
tragické	tragický	k2eAgFnPc4d1	tragická
události	událost	k1gFnPc4	událost
dějin	dějiny	k1gFnPc2	dějiny
<g/>
;	;	kIx,	;
propagace	propagace	k1gFnSc1	propagace
holokaustu	holokaust	k1gInSc2	holokaust
z	z	k7c2	z
finančního	finanční	k2eAgNnSc2d1	finanční
hlediska	hledisko	k1gNnSc2	hledisko
(	(	kIx(	(
<g/>
jeho	jeho	k3xOp3gFnSc1	jeho
kapitalizace	kapitalizace	k1gFnSc1	kapitalizace
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
zveličování	zveličování	k1gNnSc1	zveličování
<g/>
,	,	kIx,	,
přikrášlování	přikrášlování	k1gNnSc1	přikrášlování
a	a	k8xC	a
zneužívání	zneužívání	k1gNnSc1	zneužívání
utrpení	utrpení	k1gNnSc2	utrpení
Židů	Žid	k1gMnPc2	Žid
jako	jako	k8xS	jako
pózy	póza	k1gFnSc2	póza
ublíženectví	ublíženectví	k1gNnSc2	ublíženectví
<g/>
;	;	kIx,	;
zneužití	zneužití	k1gNnSc1	zneužití
holokaustu	holokaust	k1gInSc2	holokaust
pro	pro	k7c4	pro
politické	politický	k2eAgInPc4d1	politický
cíle	cíl	k1gInPc4	cíl
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
prosazování	prosazování	k1gNnSc6	prosazování
politických	politický	k2eAgInPc2d1	politický
zájmů	zájem	k1gInPc2	zájem
Izraele	Izrael	k1gInSc2	Izrael
<g/>
;	;	kIx,	;
argumentace	argumentace	k1gFnSc1	argumentace
holokaustem	holokaust	k1gInSc7	holokaust
těmi	ten	k3xDgMnPc7	ten
<g/>
,	,	kIx,	,
jichž	jenž	k3xRgInPc2	jenž
se	se	k3xPyFc4	se
holokaust	holokaust	k1gInSc1	holokaust
fakticky	fakticky	k6eAd1	fakticky
nedotkl	dotknout	k5eNaPmAgInS	dotknout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
aspektech	aspekt	k1gInPc6	aspekt
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
těch	ten	k3xDgFnPc2	ten
politických	politický	k2eAgFnPc2d1	politická
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc4	tento
postoj	postoj	k1gInSc4	postoj
překrývá	překrývat	k5eAaImIp3nS	překrývat
s	s	k7c7	s
argumenty	argument	k1gInPc7	argument
antisionismu	antisionismus	k1gInSc2	antisionismus
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
