<s>
Uvádí	uvádět	k5eAaImIp3nS	uvádět
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
patrně	patrně	k6eAd1	patrně
první	první	k4xOgMnSc1	první
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
použil	použít	k5eAaPmAgMnS	použít
slovo	slovo	k1gNnSc4	slovo
holocaust	holocaust	k1gInSc4	holocaust
pro	pro	k7c4	pro
nacistické	nacistický	k2eAgNnSc4d1	nacistické
vyhlazování	vyhlazování	k1gNnSc4	vyhlazování
Židů	Žid	k1gMnPc2	Žid
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
míru	míra	k1gFnSc4	míra
Elie	Elie	k1gFnSc1	Elie
Wiesel	Wiesel	k1gMnSc1	Wiesel
(	(	kIx(	(
<g/>
*	*	kIx~	*
1928	[number]	k4	1928
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
románu	román	k1gInSc6	román
Noc	noc	k1gFnSc1	noc
(	(	kIx(	(
<g/>
La	la	k1gNnSc1	la
Nuit	Nuita	k1gFnPc2	Nuita
<g/>
,	,	kIx,	,
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
"	"	kIx"	"
<g/>
zničení	zničení	k1gNnSc2	zničení
ohněm	oheň	k1gInSc7	oheň
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
