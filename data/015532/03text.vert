<s>
Tramvajová	tramvajový	k2eAgFnSc1d1
trať	trať	k1gFnSc1
Liberec	Liberec	k1gInSc1
<g/>
–	–	k?
<g/>
Rochlice	Rochlice	k1gFnSc2
</s>
<s>
Tramvajová	tramvajový	k2eAgFnSc1d1
trať	trať	k1gFnSc1
Liberec	Liberec	k1gInSc1
–	–	k?
Rochlice	Rochlice	k1gFnSc2
byla	být	k5eAaImAgFnS
v	v	k7c6
provozu	provoz	k1gInSc6
v	v	k7c6
letech	let	k1gInPc6
1899	#num#	k4
<g/>
–	–	k?
<g/>
1960	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
plánována	plánovat	k5eAaImNgFnS
výstavba	výstavba	k1gFnSc1
nové	nový	k2eAgFnSc2d1
trati	trať	k1gFnSc2
v	v	k7c6
podobné	podobný	k2eAgFnSc6d1
relaci	relace	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Původní	původní	k2eAgFnSc1d1
trať	trať	k1gFnSc1
</s>
<s>
Trať	trať	k1gFnSc1
z	z	k7c2
dolního	dolní	k2eAgNnSc2d1
libereckého	liberecký	k2eAgNnSc2d1
náměstí	náměstí	k1gNnSc2
(	(	kIx(
<g/>
později	pozdě	k6eAd2
Gottwaldovo	Gottwaldův	k2eAgNnSc4d1
náměstí	náměstí	k1gNnSc4
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
Soukenné	soukenný	k2eAgNnSc4d1
náměstí	náměstí	k1gNnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
byla	být	k5eAaImAgFnS
napojena	napojit	k5eAaPmNgFnS
na	na	k7c4
hlavní	hlavní	k2eAgFnPc4d1
městské	městský	k2eAgFnPc4d1
tratě	trať	k1gFnPc4
<g/>
)	)	kIx)
jižním	jižní	k2eAgInSc7d1
směrem	směr	k1gInSc7
do	do	k7c2
obce	obec	k1gFnSc2
<g/>
,	,	kIx,
později	pozdě	k6eAd2
liberecké	liberecký	k2eAgFnSc2d1
části	část	k1gFnSc2
Rochlice	Rochlice	k1gFnSc2
na	na	k7c4
Poštovní	poštovní	k2eAgNnSc4d1
náměstí	náměstí	k1gNnSc4
byla	být	k5eAaImAgFnS
uvedena	uveden	k2eAgFnSc1d1
do	do	k7c2
provozu	provoz	k1gInSc2
na	na	k7c6
konci	konec	k1gInSc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Povolení	povolení	k1gNnPc1
ke	k	k7c3
stavbě	stavba	k1gFnSc3
trati	trať	k1gFnSc2
nádraží	nádraží	k1gNnSc2
–	–	k?
Rochlice	Rochlice	k1gFnSc2
bylo	být	k5eAaImAgNnS
uděleno	udělit	k5eAaPmNgNnS
v	v	k7c6
roce	rok	k1gInSc6
1897	#num#	k4
<g/>
,	,	kIx,
stavba	stavba	k1gFnSc1
trati	trať	k1gFnSc2
v	v	k7c6
trase	trasa	k1gFnSc6
z	z	k7c2
náměstí	náměstí	k1gNnSc2
byla	být	k5eAaImAgFnS
zahájena	zahájen	k2eAgFnSc1d1
3	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1899	#num#	k4
a	a	k8xC
14	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1899	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
podle	podle	k7c2
webu	web	k1gInSc2
SPVD	SPVD	kA
v	v	k7c6
září	září	k1gNnSc6
1898	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
na	na	k7c6
trati	trať	k1gFnSc6
zahájen	zahájit	k5eAaPmNgInS
provoz	provoz	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zpravidla	zpravidla	k6eAd1
po	po	k7c6
této	tento	k3xDgFnSc6
trati	trať	k1gFnSc6
jezdila	jezdit	k5eAaImAgFnS
linka	linka	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
pak	pak	k6eAd1
pokračovala	pokračovat	k5eAaImAgFnS
po	po	k7c6
trati	trať	k1gFnSc6
do	do	k7c2
Růžodolu	růžodol	k1gInSc2
<g/>
,	,	kIx,
dostavěné	dostavěný	k2eAgFnPc1d1
později	pozdě	k6eAd2
<g/>
,	,	kIx,
například	například	k6eAd1
při	při	k7c6
zavedení	zavedení	k1gNnSc6
čísel	číslo	k1gNnPc2
linek	linka	k1gFnPc2
v	v	k7c6
roce	rok	k1gInSc6
1924	#num#	k4
jezdila	jezdit	k5eAaImAgFnS
v	v	k7c6
takové	takový	k3xDgFnSc6
trase	trasa	k1gFnSc6
linka	linka	k1gFnSc1
č.	č.	k?
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1944	#num#	k4
byla	být	k5eAaImAgFnS
v	v	k7c6
Rochlici	Rochlice	k1gFnSc6
zprovozněna	zprovozněn	k2eAgFnSc1d1
nová	nový	k2eAgFnSc1d1
měnírna	měnírna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
byla	být	k5eAaImAgFnS
na	na	k7c6
náměstí	náměstí	k1gNnSc6
v	v	k7c6
Liberci	Liberec	k1gInSc6
uvedena	uveden	k2eAgFnSc1d1
do	do	k7c2
provozu	provoz	k1gInSc2
elektromagnetická	elektromagnetický	k2eAgFnSc1d1
výhybka	výhybka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Trať	trať	k1gFnSc1
byla	být	k5eAaImAgFnS
zrušena	zrušit	k5eAaPmNgFnS
společně	společně	k6eAd1
s	s	k7c7
tratí	trať	k1gFnSc7
do	do	k7c2
Růžodolu	růžodol	k1gInSc2
I	I	kA
<g/>
,	,	kIx,
naposledy	naposledy	k6eAd1
byly	být	k5eAaImAgInP
v	v	k7c6
provozu	provoz	k1gInSc6
31	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1960	#num#	k4
a	a	k8xC
do	do	k7c2
tří	tři	k4xCgNnPc2
let	léto	k1gNnPc2
bylo	být	k5eAaImAgNnS
traťové	traťový	k2eAgNnSc1d1
zařízení	zařízení	k1gNnSc1
odstraněno	odstranit	k5eAaPmNgNnS
<g/>
.	.	kIx.
</s>
<s>
Provozní	provozní	k2eAgFnSc1d1
délka	délka	k1gFnSc1
tratě	trať	k1gFnSc2
činila	činit	k5eAaImAgFnS
2,826	#num#	k4
km	km	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nová	nový	k2eAgFnSc1d1
trať	trať	k1gFnSc1
</s>
<s>
Obnovení	obnovení	k1gNnSc1
tramvajové	tramvajový	k2eAgFnSc2d1
trati	trať	k1gFnSc2
do	do	k7c2
Rochlice	Rochlice	k1gFnSc2
je	být	k5eAaImIp3nS
stále	stále	k6eAd1
aktivně	aktivně	k6eAd1
plánováno	plánovat	k5eAaImNgNnS
<g/>
,	,	kIx,
zejména	zejména	k9
kvůli	kvůli	k7c3
vytíženosti	vytíženost	k1gFnSc3
linek	linka	k1gFnPc2
hromadné	hromadný	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
na	na	k7c6
rochlickému	rochlický	k2eAgNnSc3d1
sídlišti	sídliště	k1gNnSc3
s	s	k7c7
asi	asi	k9
18	#num#	k4
000	#num#	k4
obyvateli	obyvatel	k1gMnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Již	již	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
měly	mít	k5eAaImAgFnP
tento	tento	k3xDgInSc4
projekt	projekt	k1gInSc4
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
volebním	volební	k2eAgInSc6d1
programu	program	k1gInSc6
pro	pro	k7c4
komunální	komunální	k2eAgFnPc4d1
volby	volba	k1gFnPc4
všechny	všechen	k3xTgFnPc4
významné	významný	k2eAgFnPc4d1
politické	politický	k2eAgFnPc4d1
strany	strana	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přestože	přestože	k8xS
zvítězivší	zvítězivší	k2eAgFnSc1d1
ODS	ODS	kA
dala	dát	k5eAaPmAgFnS
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
přednost	přednost	k1gFnSc4
jiným	jiný	k2eAgInPc3d1
projektům	projekt	k1gInPc3
<g/>
,	,	kIx,
zejména	zejména	k9
sportovním	sportovní	k2eAgInPc3d1
areálům	areál	k1gInPc3
pro	pro	k7c4
lyžařské	lyžařský	k2eAgInPc4d1
MS	MS	kA
v	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
<g/>
,	,	kIx,
zastupitelstvo	zastupitelstvo	k1gNnSc1
odsouhlasilo	odsouhlasit	k5eAaPmAgNnS
první	první	k4xOgFnSc4
variantu	varianta	k1gFnSc4
podoby	podoba	k1gFnSc2
trati	trať	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgNnPc1
by	by	kYmCp3nP
se	se	k3xPyFc4
u	u	k7c2
zastávky	zastávka	k1gFnSc2
U	u	k7c2
Lomu	lom	k1gInSc2
oddělovala	oddělovat	k5eAaImAgFnS
od	od	k7c2
modernizované	modernizovaný	k2eAgFnSc2d1
liberecko	liberecko	k6eAd1
<g/>
–	–	k?
<g/>
jablonecké	jablonecký	k2eAgFnPc4d1
trati	trať	k1gFnPc4
a	a	k8xC
vedla	vést	k5eAaImAgFnS
by	by	kYmCp3nS
kolem	kolem	k6eAd1
sídliště	sídliště	k1gNnSc4
Broumovská	broumovský	k2eAgFnSc1d1
ulicemi	ulice	k1gFnPc7
Broumovskou	broumovský	k2eAgFnSc7d1
a	a	k8xC
Krejčího	Krejčího	k2eAgFnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cena	cena	k1gFnSc1
se	se	k3xPyFc4
odhadovala	odhadovat	k5eAaImAgFnS
asi	asi	k9
na	na	k7c4
700	#num#	k4
milionů	milion	k4xCgInPc2
Kč	Kč	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
témže	týž	k3xTgInSc6
roce	rok	k1gInSc6
primátor	primátor	k1gMnSc1
doufal	doufat	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
mohla	moct	k5eAaImAgFnS
být	být	k5eAaImF
nová	nový	k2eAgFnSc1d1
trať	trať	k1gFnSc1
zprovozněna	zprovozněn	k2eAgFnSc1d1
do	do	k7c2
roku	rok	k1gInSc2
2011	#num#	k4
<g/>
,	,	kIx,
o	o	k7c6
podobných	podobný	k2eAgInPc6d1
termínech	termín	k1gInPc6
se	se	k3xPyFc4
psalo	psát	k5eAaImAgNnS
i	i	k9
v	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Projednávána	projednáván	k2eAgFnSc1d1
byla	být	k5eAaImAgFnS
také	také	k9
druhá	druhý	k4xOgFnSc1
varianta	varianta	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
by	by	kYmCp3nS
kopírovala	kopírovat	k5eAaImAgFnS
trasu	trasa	k1gFnSc4
autobusové	autobusový	k2eAgFnSc2d1
linky	linka	k1gFnSc2
č.	č.	k?
12	#num#	k4
od	od	k7c2
stanice	stanice	k1gFnSc2
Rybníček	Rybníček	k1gMnSc1
kolem	kolem	k7c2
krajského	krajský	k2eAgInSc2d1
úřadu	úřad	k1gInSc2
<g/>
,	,	kIx,
přemostěním	přemostění	k1gNnSc7
Nisy	Nisa	k1gFnSc2
a	a	k8xC
podjezdem	podjezd	k1gInSc7
křižovatky	křižovatka	k1gFnSc2
na	na	k7c4
třídu	třída	k1gFnSc4
Dr	dr	kA
<g/>
.	.	kIx.
Milady	Milada	k1gFnSc2
Horákové	Horáková	k1gFnSc2
(	(	kIx(
<g/>
zastávka	zastávka	k1gFnSc1
Košická	košický	k2eAgFnSc1d1
<g/>
)	)	kIx)
a	a	k8xC
dále	daleko	k6eAd2
po	po	k7c6
její	její	k3xOp3gFnSc6
levé	levý	k2eAgFnSc6d1
straně	strana	k1gFnSc6
náspem	násep	k1gInSc7
k	k	k7c3
zastávce	zastávka	k1gFnSc3
U	u	k7c2
Močálu	močál	k1gInSc2
<g/>
,	,	kIx,
od	od	k7c2
zastávky	zastávka	k1gFnSc2
Dobiášova	Dobiášův	k2eAgInSc2d1
pak	pak	k6eAd1
samostatným	samostatný	k2eAgInSc7d1
dopravním	dopravní	k2eAgInSc7d1
pásem	pás	k1gInSc7
po	po	k7c6
pravé	pravý	k2eAgFnSc6d1
straně	strana	k1gFnSc6
třídy	třída	k1gFnSc2
Dobiášova	Dobiášův	k2eAgNnSc2d1
až	až	k9
ke	k	k7c3
smyčce	smyčka	k1gFnSc3
Zelené	Zelené	k2eAgNnSc2d1
údolí	údolí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tuto	tento	k3xDgFnSc4
variantu	varianta	k1gFnSc4
zastupitelstvo	zastupitelstvo	k1gNnSc1
města	město	k1gNnSc2
bez	bez	k7c2
provedení	provedení	k1gNnSc2
srovnávací	srovnávací	k2eAgFnSc2d1
studie	studie	k1gFnSc2
odmítlo	odmítnout	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s>
Dopravní	dopravní	k2eAgInSc1d1
podnik	podnik	k1gInSc1
nechal	nechat	k5eAaPmAgInS
vypracovat	vypracovat	k5eAaPmF
i	i	k9
studii	studie	k1gFnSc4
tříkilometrového	tříkilometrový	k2eAgNnSc2d1
pokračování	pokračování	k1gNnSc2
tratě	trať	k1gFnSc2
po	po	k7c6
mostě	most	k1gInSc6
přes	přes	k7c4
údolí	údolí	k1gNnSc4
Nisy	Nisa	k1gFnSc2
a	a	k8xC
silnici	silnice	k1gFnSc4
I	I	kA
<g/>
/	/	kIx~
<g/>
14	#num#	k4
ulicí	ulice	k1gFnSc7
Českou	český	k2eAgFnSc7d1
až	až	k6eAd1
k	k	k7c3
Makru	makro	k1gNnSc3
ve	v	k7c6
Vesci	Vesce	k1gFnSc6
<g/>
,	,	kIx,
výhledově	výhledově	k6eAd1
do	do	k7c2
Doubí	doubí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Původně	původně	k6eAd1
se	se	k3xPyFc4
počítalo	počítat	k5eAaImAgNnS
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
na	na	k7c6
ploše	plocha	k1gFnSc6
bývalého	bývalý	k2eAgInSc2d1
podniku	podnik	k1gInSc2
Textilana	textilana	k1gFnSc1
vybuduje	vybudovat	k5eAaPmIp3nS
soukromý	soukromý	k2eAgMnSc1d1
investor	investor	k1gMnSc1
obchodně-zábavní	obchodně-zábavní	k2eAgNnSc4d1
centrum	centrum	k1gNnSc4
Promenáda	promenáda	k1gFnSc1
a	a	k8xC
v	v	k7c6
jeho	jeho	k3xOp3gInSc6
rámci	rámec	k1gInSc6
zaplatí	zaplatit	k5eAaPmIp3nS
i	i	k9
přeložku	přeložka	k1gFnSc4
tramvajové	tramvajový	k2eAgFnSc2d1
trati	trať	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
projektu	projekt	k1gInSc2
však	však	k9
v	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
ustoupil	ustoupit	k5eAaPmAgInS
a	a	k8xC
přeložku	přeložka	k1gFnSc4
tak	tak	k6eAd1
muselo	muset	k5eAaImAgNnS
město	město	k1gNnSc4
budovat	budovat	k5eAaImF
na	na	k7c4
své	svůj	k3xOyFgInPc4
náklady	náklad	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
byla	být	k5eAaImAgFnS
v	v	k7c6
rámci	rámec	k1gInSc6
tzv.	tzv.	kA
nulté	nultý	k4xOgFnSc2
etapy	etapa	k1gFnSc2
za	za	k7c4
40	#num#	k4
milionů	milion	k4xCgInPc2
Kč	Kč	kA
přestavěna	přestavěn	k2eAgFnSc1d1
tramvajová	tramvajový	k2eAgFnSc1d1
smyčka	smyčka	k1gFnSc1
v	v	k7c6
terminálu	terminál	k1gInSc6
MHD	MHD	kA
Fügnerova	Fügnerův	k2eAgFnSc1d1
<g/>
,	,	kIx,
k	k	k7c3
čemuž	což	k3yRnSc3,k3yQnSc3
bylo	být	k5eAaImAgNnS
město	město	k1gNnSc1
motivováno	motivovat	k5eAaBmNgNnS
pořádáním	pořádání	k1gNnSc7
MS	MS	kA
v	v	k7c6
lyžování	lyžování	k1gNnSc6
v	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dubnu	duben	k1gInSc6
2008	#num#	k4
vedení	vedení	k1gNnSc2
města	město	k1gNnSc2
uvažovalo	uvažovat	k5eAaImAgNnS
o	o	k7c6
nabídce	nabídka	k1gFnSc6
společnosti	společnost	k1gFnSc2
Involar	Involar	k1gInSc4
na	na	k7c4
údajně	údajně	k6eAd1
výhodný	výhodný	k2eAgInSc4d1
úvěr	úvěr	k1gInSc4
od	od	k7c2
nebankovní	bankovní	k2eNgFnSc2d1
společnosti	společnost	k1gFnSc2
IBC	IBC	kA
Valor	valor	k1gInSc1
GmbH	GmbH	k1gFnSc1
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
ve	v	k7c6
výši	výše	k1gFnSc6,k1gFnSc6wB
odpovídající	odpovídající	k2eAgInPc4d1
asi	asi	k9
312	#num#	k4
milionů	milion	k4xCgInPc2
Kč	Kč	kA
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
pojistce	pojistce	k1gMnSc1
pro	pro	k7c4
případ	případ	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
město	město	k1gNnSc1
nedostalo	dostat	k5eNaPmAgNnS
evropskou	evropský	k2eAgFnSc4d1
ani	ani	k8xC
státní	státní	k2eAgFnSc4d1
dotaci	dotace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
červnu	červen	k1gInSc6
2009	#num#	k4
začala	začít	k5eAaPmAgFnS
rekonstrukce	rekonstrukce	k1gFnSc1
stávající	stávající	k2eAgFnSc2d1
meziměstské	meziměstský	k2eAgFnSc2d1
trati	trať	k1gFnSc2
v	v	k7c6
úseku	úsek	k1gInSc6
Fügnerova	Fügnerův	k2eAgFnSc1d1
–	–	k?
Mlýnská	mlýnský	k2eAgFnSc1d1
–	–	k?
Klicperova	Klicperův	k2eAgFnSc1d1
–	–	k?
Textilana	textilana	k1gFnSc1
–	–	k?
Jablonecká	jablonecký	k2eAgFnSc1d1
–	–	k?
U	u	k7c2
Lomu	lom	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
zahrnovala	zahrnovat	k5eAaImAgFnS
zdvojkolejnění	zdvojkolejnění	k1gNnSc3
a	a	k8xC
zdvojrozchodnění	zdvojrozchodnění	k1gNnSc3
stávající	stávající	k2eAgFnSc2d1
jednokolejné	jednokolejný	k2eAgFnSc2d1
trati	trať	k1gFnSc2
s	s	k7c7
rozchodem	rozchod	k1gInSc7
1000	#num#	k4
mm	mm	kA
(	(	kIx(
<g/>
nově	nově	k6eAd1
má	mít	k5eAaImIp3nS
i	i	k9
normální	normální	k2eAgInSc4d1
rozchod	rozchod	k1gInSc4
1435	#num#	k4
mm	mm	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
finančních	finanční	k2eAgInPc2d1
důvodů	důvod	k1gInPc2
však	však	k9
ve	v	k7c6
výstavbě	výstavba	k1gFnSc6
nové	nový	k2eAgFnSc2d1
trati	trať	k1gFnSc2
ze	z	k7c2
zastávky	zastávka	k1gFnSc2
U	u	k7c2
Lomu	lom	k1gInSc2
pokračováno	pokračován	k2eAgNnSc4d1
nebylo	být	k5eNaImAgNnS
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
komunálních	komunální	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
v	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
se	se	k3xPyFc4
toto	tento	k3xDgNnSc1
téma	téma	k1gNnSc1
také	také	k9
objevilo	objevit	k5eAaPmAgNnS
v	v	k7c6
programech	program	k1gInPc6
některých	některý	k3yIgFnPc2
stran	strana	k1gFnPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
po	po	k7c6
volbách	volba	k1gFnPc6
nové	nový	k2eAgNnSc1d1
složení	složení	k1gNnSc1
zastupitelstva	zastupitelstvo	k1gNnSc2
pokračování	pokračování	k1gNnSc2
v	v	k7c6
projektu	projekt	k1gInSc6
schválilo	schválit	k5eAaPmAgNnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
plánu	plán	k1gInSc6
však	však	k9
nyní	nyní	k6eAd1
byla	být	k5eAaImAgFnS
druhá	druhý	k4xOgFnSc1
varianta	varianta	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
vyhýbá	vyhýbat	k5eAaImIp3nS
terminálu	terminál	k1gInSc2
Fügnerova	Fügnerův	k2eAgInSc2d1
a	a	k8xC
zároveň	zároveň	k6eAd1
umožňuje	umožňovat	k5eAaImIp3nS
návaznost	návaznost	k1gFnSc4
linek	linka	k1gFnPc2
od	od	k7c2
nádraží	nádraží	k1gNnSc2
nebo	nebo	k8xC
od	od	k7c2
Vratislavic	Vratislavice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cena	cena	k1gFnSc1
se	se	k3xPyFc4
odhadovala	odhadovat	k5eAaImAgFnS
mezi	mezi	k7c7
650	#num#	k4
a	a	k8xC
800	#num#	k4
miliony	milion	k4xCgInPc4
Kč	Kč	kA
<g/>
,	,	kIx,
někteří	některý	k3yIgMnPc1
zastupitelé	zastupitel	k1gMnPc1
však	však	k9
vyjádřili	vyjádřit	k5eAaPmAgMnP
obavy	obava	k1gFnPc4
z	z	k7c2
finančního	finanční	k2eAgNnSc2d1
zabezpečení	zabezpečení	k1gNnSc2
projektu	projekt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
Nová	nový	k2eAgFnSc1d1
trať	trať	k1gFnSc1
do	do	k7c2
Rochlice	Rochlice	k1gFnSc2
má	mít	k5eAaImIp3nS
stát	stát	k5eAaImF,k5eAaPmF
stamiliony	stamiliony	k1gInPc4
<g/>
,	,	kIx,
v	v	k7c6
plánu	plán	k1gInSc6
je	být	k5eAaImIp3nS
i	i	k9
tunel	tunel	k1gInSc1
u	u	k7c2
Babylonu	babylon	k1gInSc2
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-10-30	2018-10-30	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
DVORSKÝ	Dvorský	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
;	;	kIx,
GRISA	GRISA	kA
<g/>
,	,	kIx,
Ivan	Ivan	k1gMnSc1
<g/>
;	;	kIx,
JÄKL	JÄKL	kA
<g/>
,	,	kIx,
Gisbert	Gisbert	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úzkorozchodné	úzkorozchodný	k2eAgInPc4d1
tramvajové	tramvajový	k2eAgInPc4d1
provozy	provoz	k1gInPc4
–	–	k?
Liberec	Liberec	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Corona	Corona	k1gFnSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86116	#num#	k4
<g/>
-	-	kIx~
<g/>
49	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
101	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Liberecké	liberecký	k2eAgNnSc1d1
ANO	ano	k9
láká	lákat	k5eAaImIp3nS
na	na	k7c4
tramvaj	tramvaj	k1gFnSc4
do	do	k7c2
Rochlice	Rochlice	k1gFnSc2
a	a	k8xC
tunel	tunel	k1gInSc4
<g/>
,	,	kIx,
Ťok	Ťok	k1gFnPc4
však	však	k8xC
peníze	peníz	k1gInPc4
neslíbil	slíbit	k5eNaPmAgInS
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-09-12	2018-09-12	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Tramvajová	tramvajový	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
v	v	k7c6
Liberci	Liberec	k1gInSc6
</s>
<s>
Tramvajová	tramvajový	k2eAgFnSc1d1
trať	trať	k1gFnSc1
Lidové	lidový	k2eAgFnSc2d1
sady	sada	k1gFnSc2
–	–	k?
Horní	horní	k2eAgInSc1d1
Hanychov	Hanychov	k1gInSc1
</s>
<s>
Tramvajová	tramvajový	k2eAgFnSc1d1
trať	trať	k1gFnSc1
Liberec	Liberec	k1gInSc1
<g/>
,	,	kIx,
radnice	radnice	k1gFnSc1
–	–	k?
Růžodol	růžodol	k1gInSc1
I	i	k9
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Historie	historie	k1gFnSc1
tramvají	tramvaj	k1gFnPc2
v	v	k7c6
Liberci	Liberec	k1gInSc6
<g/>
,	,	kIx,
czechtrams	czechtrams	k6eAd1
<g/>
.	.	kIx.
<g/>
wz	wz	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
Hoidekr	Hoidekr	k1gMnSc1
<g/>
,	,	kIx,
27	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
,	,	kIx,
podle	podle	k7c2
<g/>
:	:	kIx,
L.	L.	kA
Kysela	Kysela	k1gMnSc1
<g/>
,	,	kIx,
G.	G.	kA
Jäkl	Jäkl	k1gInSc1
<g/>
:	:	kIx,
Liberecké	liberecký	k2eAgFnPc1d1
tramvaje	tramvaj	k1gFnPc1
</s>
<s>
Liberecké	liberecký	k2eAgFnPc1d1
tramvaje	tramvaj	k1gFnPc1
<g/>
,	,	kIx,
Společnost	společnost	k1gFnSc1
pro	pro	k7c4
veřejnou	veřejný	k2eAgFnSc4d1
dopravu	doprava	k1gFnSc4
</s>
<s>
Jan	Jan	k1gMnSc1
Široký	Široký	k1gMnSc1
<g/>
:	:	kIx,
S	s	k7c7
vybudováním	vybudování	k1gNnSc7
tramvaje	tramvaj	k1gFnSc2
má	mít	k5eAaImIp3nS
městu	město	k1gNnSc3
pomoct	pomoct	k5eAaPmF
úvěr	úvěr	k1gInSc4
ze	z	k7c2
soukromé	soukromý	k2eAgFnSc2d1
sféry	sféra	k1gFnSc2
<g/>
,	,	kIx,
Liberecký	liberecký	k2eAgInSc1d1
deník	deník	k1gInSc1
<g/>
,	,	kIx,
18	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
2008	#num#	k4
</s>
