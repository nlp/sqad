<p>
<s>
Třebíč	Třebíč	k1gFnSc1	Třebíč
(	(	kIx(	(
Třebíč	Třebíč	k1gFnSc1	Třebíč
[	[	kIx(	[
<g/>
ˈ	ˈ	k?	ˈ
<g/>
̝	̝	k?	̝
<g/>
̊	̊	k?	̊
<g/>
ɛ	ɛ	k?	ɛ
<g/>
]	]	kIx)	]
IPA	IPA	kA	IPA
<g/>
,	,	kIx,	,
rod	rod	k1gInSc4	rod
ženský	ženský	k2eAgInSc4d1	ženský
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
původně	původně	k6eAd1	původně
mužský	mužský	k2eAgInSc1d1	mužský
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Trebitsch	Trebitsch	k1gInSc1	Trebitsch
<g/>
,	,	kIx,	,
v	v	k7c6	v
němčině	němčina	k1gFnSc6	němčina
užíváno	užíván	k2eAgNnSc1d1	užíváno
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1850	[number]	k4	1850
do	do	k7c2	do
1924	[number]	k4	1924
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1940	[number]	k4	1940
a	a	k8xC	a
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
v	v	k7c6	v
jidiš	jidiš	k1gNnSc6	jidiš
ט	ט	k?	ט
(	(	kIx(	(
<g/>
Trejbitš	Trejbitš	k1gMnSc1	Trejbitš
<g/>
))	))	k?	))
<g/>
,	,	kIx,	,
město	město	k1gNnSc1	město
s	s	k7c7	s
rozšířenou	rozšířený	k2eAgFnSc7d1	rozšířená
působností	působnost	k1gFnSc7	působnost
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
západě	západ	k1gInSc6	západ
Moravy	Morava	k1gFnSc2	Morava
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
části	část	k1gFnSc6	část
Kraje	kraj	k1gInSc2	kraj
Vysočina	vysočina	k1gFnSc1	vysočina
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
Jihlavě	Jihlava	k1gFnSc6	Jihlava
jeho	jeho	k3xOp3gFnSc2	jeho
druhým	druhý	k4xOgNnSc7	druhý
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Jihlavě	Jihlava	k1gFnSc6	Jihlava
30	[number]	k4	30
km	km	kA	km
jihovýchodně	jihovýchodně	k6eAd1	jihovýchodně
od	od	k7c2	od
krajského	krajský	k2eAgNnSc2d1	krajské
města	město	k1gNnSc2	město
Jihlavy	Jihlava	k1gFnSc2	Jihlava
a	a	k8xC	a
56	[number]	k4	56
km	km	kA	km
západně	západně	k6eAd1	západně
od	od	k7c2	od
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Nadmořská	nadmořský	k2eAgFnSc1d1	nadmořská
výška	výška	k1gFnSc1	výška
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c4	mezi
392	[number]	k4	392
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
(	(	kIx(	(
<g/>
u	u	k7c2	u
Palečkova	Palečkův	k2eAgInSc2d1	Palečkův
mlýna	mlýn	k1gInSc2	mlýn
při	při	k7c6	při
řece	řeka	k1gFnSc6	řeka
Jihlavě	Jihlava	k1gFnSc6	Jihlava
<g/>
)	)	kIx)	)
a	a	k8xC	a
503	[number]	k4	503
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
(	(	kIx(	(
<g/>
Strážná	strážný	k2eAgFnSc1d1	Strážná
hora	hora	k1gFnSc1	hora
s	s	k7c7	s
kaplí	kaple	k1gFnSc7	kaple
svatého	svatý	k2eAgMnSc2d1	svatý
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
přibližně	přibližně	k6eAd1	přibližně
36	[number]	k4	36
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Počátky	počátek	k1gInPc1	počátek
města	město	k1gNnSc2	město
souvisí	souviset	k5eAaImIp3nP	souviset
se	s	k7c7	s
založením	založení	k1gNnSc7	založení
benediktinského	benediktinský	k2eAgInSc2d1	benediktinský
kláštera	klášter	k1gInSc2	klášter
nad	nad	k7c7	nad
řekou	řeka	k1gFnSc7	řeka
Jihlavou	Jihlava	k1gFnSc7	Jihlava
roku	rok	k1gInSc2	rok
1101	[number]	k4	1101
<g/>
,	,	kIx,	,
na	na	k7c6	na
jehož	jehož	k3xOyRp3gNnSc6	jehož
místě	místo	k1gNnSc6	místo
dnes	dnes	k6eAd1	dnes
stojí	stát	k5eAaImIp3nS	stát
zámek	zámek	k1gInSc1	zámek
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
listina	listina	k1gFnSc1	listina
opata	opat	k1gMnSc2	opat
Martina	Martin	k1gMnSc2	Martin
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1277	[number]	k4	1277
<g/>
.	.	kIx.	.
</s>
<s>
Městská	městský	k2eAgNnPc1d1	Městské
práva	právo	k1gNnPc1	právo
obdrželo	obdržet	k5eAaPmAgNnS	obdržet
město	město	k1gNnSc1	město
od	od	k7c2	od
markraběte	markrabě	k1gMnSc2	markrabě
Karla	Karel	k1gMnSc2	Karel
roku	rok	k1gInSc2	rok
1335	[number]	k4	1335
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
získalo	získat	k5eAaPmAgNnS	získat
i	i	k9	i
právo	právo	k1gNnSc4	právo
opevnit	opevnit	k5eAaPmF	opevnit
se	s	k7c7	s
hradbami	hradba	k1gFnPc7	hradba
a	a	k8xC	a
příkopy	příkop	k1gInPc7	příkop
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dobách	doba	k1gFnPc6	doba
největšího	veliký	k2eAgInSc2d3	veliký
rozmachu	rozmach	k1gInSc2	rozmach
byla	být	k5eAaImAgFnS	být
Třebíč	Třebíč	k1gFnSc1	Třebíč
po	po	k7c6	po
Olomouci	Olomouc	k1gFnSc6	Olomouc
a	a	k8xC	a
Brně	Brno	k1gNnSc6	Brno
nejvýznamnějším	významný	k2eAgNnSc7d3	nejvýznamnější
střediskem	středisko	k1gNnSc7	středisko
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
,	,	kIx,	,
statky	statek	k1gInPc1	statek
zdejšího	zdejší	k2eAgInSc2d1	zdejší
kláštera	klášter	k1gInSc2	klášter
dosahovaly	dosahovat	k5eAaImAgInP	dosahovat
téměř	téměř	k6eAd1	téměř
až	až	k9	až
k	k	k7c3	k
Opavě	Opava	k1gFnSc3	Opava
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
začal	začít	k5eAaPmAgInS	začít
vzrůstat	vzrůstat	k5eAaImF	vzrůstat
až	až	k9	až
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
výstavbou	výstavba	k1gFnSc7	výstavba
rodinných	rodinný	k2eAgInPc2d1	rodinný
domů	dům	k1gInPc2	dům
a	a	k8xC	a
následně	následně	k6eAd1	následně
velkých	velký	k2eAgNnPc2d1	velké
panelových	panelový	k2eAgNnPc2d1	panelové
sídlišť	sídliště	k1gNnPc2	sídliště
s	s	k7c7	s
byty	byt	k1gInPc7	byt
především	především	k9	především
pro	pro	k7c4	pro
zaměstnance	zaměstnanec	k1gMnPc4	zaměstnanec
nově	nově	k6eAd1	nově
budované	budovaný	k2eAgFnPc1d1	budovaná
elektrárny	elektrárna	k1gFnPc1	elektrárna
v	v	k7c6	v
Dukovanech	Dukovany	k1gInPc6	Dukovany
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
Třebíč	Třebíč	k1gFnSc4	Třebíč
důležitým	důležitý	k2eAgInSc7d1	důležitý
centrem	centr	k1gInSc7	centr
západní	západní	k2eAgFnSc2d1	západní
Moravy	Morava	k1gFnSc2	Morava
a	a	k8xC	a
okresu	okres	k1gInSc2	okres
Třebíč	Třebíč	k1gFnSc1	Třebíč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozloha	rozloha	k1gFnSc1	rozloha
Třebíče	Třebíč	k1gFnSc2	Třebíč
činí	činit	k5eAaImIp3nS	činit
57,6	[number]	k4	57,6
km	km	kA	km
<g/>
2	[number]	k4	2
a	a	k8xC	a
město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
17	[number]	k4	17
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
10	[number]	k4	10
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
městské	městský	k2eAgFnSc6d1	městská
zástavbě	zástavba	k1gFnSc6	zástavba
a	a	k8xC	a
zbylých	zbylý	k2eAgFnPc2d1	zbylá
7	[number]	k4	7
v	v	k7c6	v
blízkém	blízký	k2eAgNnSc6d1	blízké
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
jsou	být	k5eAaImIp3nP	být
významné	významný	k2eAgFnPc1d1	významná
památky	památka	k1gFnPc1	památka
<g/>
,	,	kIx,	,
Třebíčská	třebíčský	k2eAgFnSc1d1	Třebíčská
židovská	židovský	k2eAgFnSc1d1	židovská
čtvrť	čtvrť	k1gFnSc1	čtvrť
a	a	k8xC	a
bazilika	bazilika	k1gFnSc1	bazilika
svatého	svatý	k2eAgMnSc2d1	svatý
Prokopa	Prokop	k1gMnSc2	Prokop
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
zapsány	zapsat	k5eAaPmNgInP	zapsat
do	do	k7c2	do
seznamu	seznam	k1gInSc2	seznam
světového	světový	k2eAgNnSc2d1	světové
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
Karlovo	Karlův	k2eAgNnSc4d1	Karlovo
náměstí	náměstí	k1gNnSc4	náměstí
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgInPc2d3	veliký
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
bylo	být	k5eAaImAgNnS	být
zveřejněno	zveřejnit	k5eAaPmNgNnS	zveřejnit
usnesení	usnesení	k1gNnSc1	usnesení
rady	rada	k1gFnSc2	rada
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
že	že	k8xS	že
město	město	k1gNnSc4	město
Třebíč	Třebíč	k1gFnSc1	Třebíč
hodlá	hodlat	k5eAaImIp3nS	hodlat
usilovat	usilovat	k5eAaImF	usilovat
o	o	k7c4	o
dosažení	dosažení	k1gNnSc4	dosažení
titulu	titul	k1gInSc2	titul
statutárního	statutární	k2eAgNnSc2d1	statutární
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
5	[number]	k4	5
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1921	[number]	k4	1921
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
založen	založit	k5eAaPmNgInS	založit
Klub	klub	k1gInSc1	klub
Třebíčanů	Třebíčan	k1gMnPc2	Třebíčan
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
v	v	k7c6	v
Karlíně	Karlín	k1gInSc6	Karlín
probíhaly	probíhat	k5eAaImAgFnP	probíhat
přednášky	přednáška	k1gFnPc1	přednáška
<g/>
,	,	kIx,	,
konalo	konat	k5eAaImAgNnS	konat
se	se	k3xPyFc4	se
fotografické	fotografický	k2eAgNnSc1d1	fotografické
promítání	promítání	k1gNnSc1	promítání
a	a	k8xC	a
další	další	k2eAgFnSc1d1	další
akce	akce	k1gFnSc1	akce
<g/>
.	.	kIx.	.
<g/>
Dne	den	k1gInSc2	den
30	[number]	k4	30
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1987	[number]	k4	1987
byl	být	k5eAaImAgMnS	být
městu	město	k1gNnSc3	město
Třebíč	Třebíč	k1gFnSc4	Třebíč
propůjčen	propůjčen	k2eAgInSc1d1	propůjčen
Řád	řád	k1gInSc1	řád
práce	práce	k1gFnSc2	práce
za	za	k7c4	za
významný	významný	k2eAgInSc4d1	významný
přínos	přínos	k1gInSc4	přínos
při	při	k7c6	při
rozvoji	rozvoj	k1gInSc6	rozvoj
československé	československý	k2eAgFnSc2d1	Československá
jaderné	jaderný	k2eAgFnSc2d1	jaderná
energetiky	energetika	k1gFnSc2	energetika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
město	město	k1gNnSc1	město
získalo	získat	k5eAaPmAgNnS	získat
ocenění	ocenění	k1gNnSc4	ocenění
Cena	cena	k1gFnSc1	cena
hejtmana	hejtman	k1gMnSc2	hejtman
Kraje	kraj	k1gInSc2	kraj
Vysočina	vysočina	k1gFnSc1	vysočina
za	za	k7c4	za
společenskou	společenský	k2eAgFnSc4d1	společenská
odpovědnost	odpovědnost	k1gFnSc4	odpovědnost
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
veřejný	veřejný	k2eAgInSc4d1	veřejný
sektor	sektor	k1gInSc4	sektor
–	–	k?	–
obce	obec	k1gFnSc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
byla	být	k5eAaImAgFnS	být
městu	město	k1gNnSc3	město
udělena	udělen	k2eAgFnSc1d1	udělena
Cena	cena	k1gFnSc1	cena
hejtmana	hejtman	k1gMnSc2	hejtman
Kraje	kraj	k1gInSc2	kraj
Vysočina	vysočina	k1gFnSc1	vysočina
za	za	k7c4	za
společenskou	společenský	k2eAgFnSc4d1	společenská
odpovědnost	odpovědnost	k1gFnSc4	odpovědnost
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
veřejný	veřejný	k2eAgInSc4d1	veřejný
sektor	sektor	k1gInSc4	sektor
–	–	k?	–
obce	obec	k1gFnSc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2019	[number]	k4	2019
byla	být	k5eAaImAgFnS	být
městu	město	k1gNnSc3	město
udělena	udělen	k2eAgFnSc1d1	udělena
Cena	cena	k1gFnSc1	cena
hejtmana	hejtman	k1gMnSc2	hejtman
Kraje	kraj	k1gInSc2	kraj
Vysočina	vysočina	k1gFnSc1	vysočina
za	za	k7c4	za
společenskou	společenský	k2eAgFnSc4d1	společenská
odpovědnost	odpovědnost	k1gFnSc4	odpovědnost
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
veřejný	veřejný	k2eAgInSc4d1	veřejný
sektor	sektor	k1gInSc4	sektor
–	–	k?	–
obce	obec	k1gFnSc2	obec
<g/>
.	.	kIx.	.
<g/>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
město	město	k1gNnSc1	město
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Radou	rada	k1gFnSc7	rada
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
mládeže	mládež	k1gFnSc2	mládež
kraje	kraj	k1gInSc2	kraj
Vysočina	vysočina	k1gFnSc1	vysočina
udílí	udílet	k5eAaImIp3nS	udílet
cenu	cena	k1gFnSc4	cena
Třebíčské	třebíčský	k2eAgNnSc1d1	třebíčské
srdíčko	srdíčko	k1gNnSc1	srdíčko
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
oceňuje	oceňovat	k5eAaImIp3nS	oceňovat
dobrovolníky	dobrovolník	k1gMnPc4	dobrovolník
pracující	pracující	k1gFnSc1	pracující
s	s	k7c7	s
dětmi	dítě	k1gFnPc7	dítě
a	a	k8xC	a
mládeží	mládež	k1gFnSc7	mládež
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2019	[number]	k4	2019
se	se	k3xPyFc4	se
Třebíč	Třebíč	k1gFnSc1	Třebíč
stala	stát	k5eAaPmAgFnS	stát
členem	člen	k1gMnSc7	člen
Společnosti	společnost	k1gFnSc2	společnost
pro	pro	k7c4	pro
zahradní	zahradní	k2eAgFnSc4d1	zahradní
a	a	k8xC	a
krajinářskou	krajinářský	k2eAgFnSc4d1	krajinářská
tvorbu	tvorba	k1gFnSc4	tvorba
<g/>
.	.	kIx.	.
<g/>
Sousedními	sousední	k2eAgFnPc7d1	sousední
obcemi	obec	k1gFnPc7	obec
sídla	sídlo	k1gNnSc2	sídlo
jsou	být	k5eAaImIp3nP	být
Okřešice	Okřešice	k1gFnPc1	Okřešice
<g/>
,	,	kIx,	,
Vladislav	Vladislav	k1gMnSc1	Vladislav
<g/>
,	,	kIx,	,
Trnava	Trnava	k1gFnSc1	Trnava
<g/>
,	,	kIx,	,
Kožichovice	Kožichovice	k1gFnSc1	Kožichovice
<g/>
,	,	kIx,	,
Střítež	Střítež	k1gFnSc1	Střítež
<g/>
,	,	kIx,	,
Petrůvky	Petrůvka	k1gFnPc1	Petrůvka
<g/>
,	,	kIx,	,
Výčapy	Výčap	k1gInPc1	Výčap
<g/>
,	,	kIx,	,
Mikulovice	Mikulovice	k1gFnPc1	Mikulovice
<g/>
,	,	kIx,	,
Mastník	Mastník	k1gInSc1	Mastník
<g/>
,	,	kIx,	,
Petrovice	Petrovice	k1gFnSc1	Petrovice
<g/>
,	,	kIx,	,
Krahulov	Krahulov	k1gInSc1	Krahulov
<g/>
,	,	kIx,	,
Nová	nový	k2eAgFnSc1d1	nová
Ves	ves	k1gFnSc1	ves
<g/>
,	,	kIx,	,
Číhalín	Číhalín	k1gInSc1	Číhalín
<g/>
,	,	kIx,	,
Horní	horní	k2eAgFnSc1d1	horní
Vilémovice	Vilémovice	k1gFnSc1	Vilémovice
a	a	k8xC	a
Stařeč	Stařeč	k1gInSc1	Stařeč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Etymologie	etymologie	k1gFnSc2	etymologie
==	==	k?	==
</s>
</p>
<p>
<s>
Název	název	k1gInSc1	název
města	město	k1gNnSc2	město
s	s	k7c7	s
největší	veliký	k2eAgFnSc7d3	veliký
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
místního	místní	k2eAgInSc2d1	místní
názvu	název	k1gInSc2	název
les	les	k1gInSc1	les
třebečský	třebečský	k2eAgInSc1d1	třebečský
(	(	kIx(	(
<g/>
Třebečský	Třebečský	k2eAgInSc1d1	Třebečský
les	les	k1gInSc1	les
<g/>
)	)	kIx)	)
zmíněného	zmíněný	k2eAgNnSc2d1	zmíněné
v	v	k7c6	v
třebíčském	třebíčský	k2eAgInSc6d1	třebíčský
opisu	opis	k1gInSc6	opis
Kosmovy	Kosmův	k2eAgFnSc2d1	Kosmova
kroniky	kronika	k1gFnSc2	kronika
<g/>
,	,	kIx,	,
sepsaném	sepsaný	k2eAgMnSc6d1	sepsaný
v	v	k7c6	v
třebíčském	třebíčský	k2eAgInSc6d1	třebíčský
klášteře	klášter	k1gInSc6	klášter
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
in	in	k?	in
silva	silva	k1gFnSc1	silva
trebecensis	trebecensis	k1gFnSc2	trebecensis
–	–	k?	–
v	v	k7c6	v
lese	les	k1gInSc6	les
třebečském	třebečský	k2eAgInSc6d1	třebečský
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pojmenování	pojmenování	k1gNnSc4	pojmenování
Třebečský	Třebečský	k2eAgInSc1d1	Třebečský
les	les	k1gInSc1	les
pak	pak	k6eAd1	pak
může	moct	k5eAaImIp3nS	moct
pocházet	pocházet	k5eAaImF	pocházet
z	z	k7c2	z
vlastního	vlastní	k2eAgNnSc2d1	vlastní
jména	jméno	k1gNnSc2	jméno
Třebek	Třebka	k1gFnPc2	Třebka
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
ze	z	k7c2	z
slova	slovo	k1gNnSc2	slovo
třeby	třeba	k1gFnSc2	třeba
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byly	být	k5eAaImAgFnP	být
obětiště	obětiště	k1gNnPc1	obětiště
pohanským	pohanský	k2eAgMnSc7d1	pohanský
bohům	bůh	k1gMnPc3	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
může	moct	k5eAaImIp3nS	moct
také	také	k9	také
pocházet	pocházet	k5eAaImF	pocházet
ze	z	k7c2	z
slova	slovo	k1gNnSc2	slovo
třebiti	třebit	k5eAaPmF	třebit
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
má	mít	k5eAaImIp3nS	mít
význam	význam	k1gInSc4	význam
ničení	ničení	k1gNnSc4	ničení
lesa	les	k1gInSc2	les
nebo	nebo	k8xC	nebo
též	též	k9	též
kácení	kácení	k1gNnSc1	kácení
lesa	les	k1gInSc2	les
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
usuzuje	usuzovat	k5eAaImIp3nS	usuzovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
území	území	k1gNnSc6	území
města	město	k1gNnSc2	město
byly	být	k5eAaImAgInP	být
hluboké	hluboký	k2eAgInPc1d1	hluboký
lesy	les	k1gInPc1	les
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tato	tento	k3xDgFnSc1	tento
možnost	možnost	k1gFnSc1	možnost
je	být	k5eAaImIp3nS	být
méně	málo	k6eAd2	málo
pravděpodobná	pravděpodobný	k2eAgFnSc1d1	pravděpodobná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dle	dle	k7c2	dle
rozboru	rozbor	k1gInSc2	rozbor
všech	všecek	k3xTgInPc2	všecek
místopisných	místopisný	k2eAgInPc2d1	místopisný
názvů	název	k1gInPc2	název
na	na	k7c6	na
Třebíčsku	Třebíčsko	k1gNnSc6	Třebíčsko
provedeného	provedený	k2eAgInSc2d1	provedený
Rudolfem	Rudolf	k1gMnSc7	Rudolf
Šrámkem	šrámek	k1gInSc7	šrámek
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
vyvozeno	vyvodit	k5eAaPmNgNnS	vyvodit
několik	několik	k4yIc1	několik
závěrů	závěr	k1gInPc2	závěr
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
obecně	obecně	k6eAd1	obecně
nejpravděpodobnější	pravděpodobný	k2eAgFnSc4d3	nejpravděpodobnější
<g/>
,	,	kIx,	,
co	co	k8xS	co
se	se	k3xPyFc4	se
původu	původ	k1gInSc2	původ
názvu	název	k1gInSc2	název
města	město	k1gNnSc2	město
Třebíče	Třebíč	k1gFnSc2	Třebíč
týče	týkat	k5eAaImIp3nS	týkat
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Šrámka	Šrámek	k1gMnSc2	Šrámek
zapadá	zapadat	k5eAaPmIp3nS	zapadat
jméno	jméno	k1gNnSc4	jméno
Třebíč	Třebíč	k1gFnSc1	Třebíč
do	do	k7c2	do
osy	osa	k1gFnSc2	osa
Vladislav	Vladislav	k1gMnSc1	Vladislav
–	–	k?	–
Stařeč	Stařeč	k1gMnSc1	Stařeč
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
názvem	název	k1gInSc7	název
obce	obec	k1gFnSc2	obec
Vladislav	Vladislava	k1gFnPc2	Vladislava
má	mít	k5eAaImIp3nS	mít
společné	společný	k2eAgNnSc1d1	společné
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejspíše	nejspíše	k9	nejspíše
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
osobního	osobní	k2eAgNnSc2d1	osobní
jména	jméno	k1gNnSc2	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
jako	jako	k9	jako
název	název	k1gInSc1	název
Stařeč	Stařeč	k1gInSc1	Stařeč
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
pocházet	pocházet	k5eAaImF	pocházet
z	z	k7c2	z
osobního	osobní	k2eAgNnSc2d1	osobní
jména	jméno	k1gNnSc2	jméno
Starek	starka	k1gFnPc2	starka
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
osobním	osobní	k2eAgNnSc7d1	osobní
jménem	jméno	k1gNnSc7	jméno
bývá	bývat	k5eAaImIp3nS	bývat
u	u	k7c2	u
Třebíče	Třebíč	k1gFnSc2	Třebíč
považováno	považován	k2eAgNnSc1d1	považováno
jméno	jméno	k1gNnSc1	jméno
Třebek	Třebka	k1gFnPc2	Třebka
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
obec	obec	k1gFnSc1	obec
Trzebiesláw	Trzebiesláw	k1gFnSc2	Trzebiesláw
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
by	by	kYmCp3nS	by
pak	pak	k9	pak
označení	označení	k1gNnSc1	označení
Třebeč	Třebeč	k1gMnSc1	Třebeč
bylo	být	k5eAaImAgNnS	být
označením	označení	k1gNnSc7	označení
pro	pro	k7c4	pro
Třebkův	Třebkův	k2eAgInSc4d1	Třebkův
majetek	majetek	k1gInSc4	majetek
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Třebek	Třebek	k1gMnSc1	Třebek
žil	žít	k5eAaImAgMnS	žít
a	a	k8xC	a
patrně	patrně	k6eAd1	patrně
vlastnil	vlastnit	k5eAaImAgMnS	vlastnit
určitou	určitý	k2eAgFnSc4d1	určitá
půdu	půda	k1gFnSc4	půda
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
skutečnosti	skutečnost	k1gFnSc3	skutečnost
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
název	název	k1gInSc4	název
Třebečský	Třebečský	k2eAgInSc1d1	Třebečský
les	les	k1gInSc1	les
zmiňovaný	zmiňovaný	k2eAgInSc1d1	zmiňovaný
v	v	k7c6	v
třebíčském	třebíčský	k2eAgInSc6d1	třebíčský
opisu	opis	k1gInSc6	opis
Kosmovy	Kosmův	k2eAgFnSc2d1	Kosmova
kroniky	kronika	k1gFnSc2	kronika
<g/>
,	,	kIx,	,
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
územím	území	k1gNnSc7	území
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
buď	buď	k8xC	buď
nacházelo	nacházet	k5eAaImAgNnS	nacházet
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
Třebkova	Třebkův	k2eAgNnSc2d1	Třebkův
sídla	sídlo	k1gNnSc2	sídlo
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
bylo	být	k5eAaImAgNnS	být
přímo	přímo	k6eAd1	přímo
pod	pod	k7c7	pod
jeho	jeho	k3xOp3gFnSc7	jeho
správou	správa	k1gFnSc7	správa
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yInSc1	co
se	se	k3xPyFc4	se
změny	změna	k1gFnPc1	změna
z	z	k7c2	z
pojmenování	pojmenování	k1gNnSc2	pojmenování
Třebeč	Třebeč	k1gInSc4	Třebeč
na	na	k7c4	na
Třebíč	Třebíč	k1gFnSc4	Třebíč
týče	týkat	k5eAaImIp3nS	týkat
<g/>
,	,	kIx,	,
souvisí	souviset	k5eAaImIp3nS	souviset
jméno	jméno	k1gNnSc1	jméno
města	město	k1gNnSc2	město
s	s	k7c7	s
již	již	k6eAd1	již
zmíněným	zmíněný	k2eAgNnSc7d1	zmíněné
pojmenováním	pojmenování	k1gNnSc7	pojmenování
nedaleké	daleký	k2eNgFnSc2d1	nedaleká
obce	obec	k1gFnSc2	obec
Stařeč	Stařeč	k1gMnSc1	Stařeč
<g/>
.	.	kIx.	.
</s>
<s>
Tvary	tvar	k1gInPc1	tvar
Třebeč	Třebeč	k1gInSc1	Třebeč
a	a	k8xC	a
Stařeč	Stařeč	k1gMnSc1	Stařeč
jsou	být	k5eAaImIp3nP	být
si	se	k3xPyFc3	se
velice	velice	k6eAd1	velice
podobné	podobný	k2eAgInPc1d1	podobný
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
i	i	k9	i
stejně	stejně	k6eAd1	stejně
skloňují	skloňovat	k5eAaImIp3nP	skloňovat
<g/>
.	.	kIx.	.
</s>
<s>
Genitiv	genitiv	k1gInSc1	genitiv
u	u	k7c2	u
obou	dva	k4xCgNnPc2	dva
pojmenování	pojmenování	k1gNnPc2	pojmenování
je	být	k5eAaImIp3nS	být
tudíž	tudíž	k8xC	tudíž
podobný	podobný	k2eAgInSc1d1	podobný
<g/>
:	:	kIx,	:
bez	bez	k7c2	bez
Třebče	Třebč	k1gFnSc2	Třebč
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
Stařčě	Stařčě	k1gMnSc2	Stařčě
<g/>
.	.	kIx.	.
</s>
<s>
Tvar	tvar	k1gInSc1	tvar
bez	bez	k7c2	bez
Stařče	Stařč	k1gInSc2	Stařč
byl	být	k5eAaImAgInS	být
časem	časem	k6eAd1	časem
pozměněn	pozměnit	k5eAaPmNgInS	pozměnit
na	na	k7c4	na
bez	bez	k1gInSc4	bez
Starče	stařec	k1gMnSc5	stařec
a	a	k8xC	a
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
tak	tak	k6eAd1	tak
verze	verze	k1gFnSc1	verze
tohoto	tento	k3xDgNnSc2	tento
místního	místní	k2eAgNnSc2d1	místní
pojmenování	pojmenování	k1gNnSc2	pojmenování
Starč	Starč	k1gFnSc1	Starč
<g/>
,	,	kIx,	,
užívaná	užívaný	k2eAgFnSc1d1	užívaná
ještě	ještě	k9	ještě
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
se	se	k3xPyFc4	se
změnilo	změnit	k5eAaPmAgNnS	změnit
i	i	k9	i
jméno	jméno	k1gNnSc4	jméno
Teleč	Teleč	k1gInSc4	Teleč
na	na	k7c4	na
Telč	Telč	k1gFnSc4	Telč
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
vytvořením	vytvoření	k1gNnSc7	vytvoření
nového	nový	k2eAgInSc2d1	nový
nominativu	nominativ	k1gInSc2	nominativ
ke	k	k7c3	k
genitivu	genitiv	k1gInSc2	genitiv
a	a	k8xC	a
ostatním	ostatní	k2eAgInPc3d1	ostatní
pádům	pád	k1gInPc3	pád
mohlo	moct	k5eAaImAgNnS	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
pojmenování	pojmenování	k1gNnSc1	pojmenování
Třebč	Třebč	k1gInSc4	Třebč
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
i	i	k9	i
Třebíč	Třebíč	k1gFnSc1	Třebíč
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
bylo	být	k5eAaImAgNnS	být
užíváno	užívat	k5eAaImNgNnS	užívat
i	i	k9	i
pojmenování	pojmenování	k1gNnSc1	pojmenování
Staříč	Staříč	k1gMnSc1	Staříč
<g/>
.	.	kIx.	.
<g/>
Šrámek	Šrámek	k1gMnSc1	Šrámek
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
rozboru	rozbor	k1gInSc6	rozbor
také	také	k9	také
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
prakticky	prakticky	k6eAd1	prakticky
vyloučil	vyloučit	k5eAaPmAgMnS	vyloučit
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
název	název	k1gInSc1	název
Třebíč	Třebíč	k1gFnSc4	Třebíč
původ	původ	k1gInSc4	původ
ve	v	k7c6	v
slovese	sloveso	k1gNnSc6	sloveso
třebit	třebit	k5eAaImF	třebit
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
vyklučit	vyklučit	k5eAaPmF	vyklučit
<g/>
,	,	kIx,	,
vypálit	vypálit	k5eAaPmF	vypálit
či	či	k8xC	či
vyčistit	vyčistit	k5eAaPmF	vyčistit
dané	daný	k2eAgNnSc4d1	dané
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
především	především	k9	především
od	od	k7c2	od
zalesnění	zalesnění	k1gNnSc2	zalesnění
<g/>
.	.	kIx.	.
</s>
<s>
Odůvodnění	odůvodnění	k1gNnSc1	odůvodnění
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
slově	slovo	k1gNnSc6	slovo
třebež	třebež	k6eAd1	třebež
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
staročeský	staročeský	k2eAgInSc1d1	staročeský
pojem	pojem	k1gInSc1	pojem
pro	pro	k7c4	pro
vytříbené	vytříbený	k2eAgNnSc4d1	vytříbené
<g/>
,	,	kIx,	,
vyklučené	vyklučený	k2eAgNnSc4d1	vyklučený
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
třebilo	třebit	k5eAaImAgNnS	třebit
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
název	název	k1gInSc1	název
místa	místo	k1gNnSc2	místo
a	a	k8xC	a
města	město	k1gNnSc2	město
by	by	kYmCp3nS	by
totiž	totiž	k9	totiž
pak	pak	k6eAd1	pak
musel	muset	k5eAaImAgMnS	muset
být	být	k5eAaImF	být
Třebež	Třebež	k1gFnSc4	Třebež
a	a	k8xC	a
takový	takový	k3xDgInSc1	takový
zápis	zápis	k1gInSc1	zápis
nikdy	nikdy	k6eAd1	nikdy
nebyl	být	k5eNaImAgInS	být
nalezen	nalézt	k5eAaBmNgInS	nalézt
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
fakt	fakt	k1gInSc1	fakt
je	být	k5eAaImIp3nS	být
ještě	ještě	k6eAd1	ještě
více	hodně	k6eAd2	hodně
upevněn	upevnit	k5eAaPmNgMnS	upevnit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
patrně	patrně	k6eAd1	patrně
nedošlo	dojít	k5eNaPmAgNnS	dojít
k	k	k7c3	k
tak	tak	k9	tak
velké	velký	k2eAgFnSc3d1	velká
změně	změna	k1gFnSc3	změna
a	a	k8xC	a
k	k	k7c3	k
potlačení	potlačení	k1gNnSc3	potlačení
původního	původní	k2eAgInSc2d1	původní
významu	význam	k1gInSc2	význam
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
by	by	kYmCp3nS	by
při	při	k7c6	při
změně	změna	k1gFnSc6	změna
z	z	k7c2	z
Třebež	Třebež	k1gFnSc4	Třebež
na	na	k7c4	na
Třebíč	Třebíč	k1gFnSc4	Třebíč
došlo	dojít	k5eAaPmAgNnS	dojít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Název	název	k1gInSc1	název
se	se	k3xPyFc4	se
původně	původně	k6eAd1	původně
užíval	užívat	k5eAaImAgInS	užívat
v	v	k7c6	v
mužském	mužský	k2eAgInSc6d1	mužský
rodě	rod	k1gInSc6	rod
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
po	po	k7c6	po
vzoru	vzor	k1gInSc6	vzor
ta	ten	k3xDgFnSc1	ten
obec	obec	k1gFnSc1	obec
začala	začít	k5eAaPmAgFnS	začít
používat	používat	k5eAaImF	používat
v	v	k7c6	v
ženském	ženský	k2eAgInSc6d1	ženský
rodě	rod	k1gInSc6	rod
–	–	k?	–
ta	ten	k3xDgFnSc1	ten
Třebíč	Třebíč	k1gFnSc1	Třebíč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Heraldika	heraldika	k1gFnSc1	heraldika
==	==	k?	==
</s>
</p>
<p>
<s>
Městský	městský	k2eAgInSc1d1	městský
znak	znak	k1gInSc1	znak
tvoří	tvořit	k5eAaImIp3nS	tvořit
štít	štít	k1gInSc1	štít
rozdělený	rozdělený	k2eAgInSc1d1	rozdělený
na	na	k7c4	na
tři	tři	k4xCgNnPc4	tři
vodorovná	vodorovný	k2eAgNnPc4d1	vodorovné
pole	pole	k1gNnPc4	pole
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
horní	horní	k2eAgNnPc1d1	horní
a	a	k8xC	a
dolní	dolní	k2eAgNnPc1d1	dolní
jsou	být	k5eAaImIp3nP	být
červená	červený	k2eAgFnSc1d1	červená
a	a	k8xC	a
ve	v	k7c6	v
středním	střední	k2eAgNnSc6d1	střední
<g/>
,	,	kIx,	,
bílém	bílé	k1gNnSc6	bílé
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
tři	tři	k4xCgFnPc4	tři
černé	černý	k2eAgFnPc4d1	černá
benediktinské	benediktinský	k2eAgFnPc4d1	benediktinská
kápě	kápě	k1gFnPc4	kápě
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
byla	být	k5eAaImAgFnS	být
červená	červený	k2eAgFnSc1d1	červená
pole	pole	k1gFnSc1	pole
dekorována	dekorován	k2eAgFnSc1d1	dekorována
stříbrnými	stříbrný	k2eAgFnPc7d1	stříbrná
arabeskami	arabeska	k1gFnPc7	arabeska
a	a	k8xC	a
počet	počet	k1gInSc1	počet
kápí	kápě	k1gFnPc2	kápě
vysvětlován	vysvětlován	k2eAgMnSc1d1	vysvětlován
zastoupením	zastoupení	k1gNnSc7	zastoupení
opatství	opatství	k1gNnSc2	opatství
třebíčského	třebíčský	k2eAgNnSc2d1	třebíčské
<g/>
,	,	kIx,	,
proboštství	proboštství	k1gNnSc2	proboštství
komárovského	komárovský	k2eAgNnSc2d1	komárovské
a	a	k8xC	a
měřínského	měřínský	k2eAgNnSc2d1	měřínské
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
uvádí	uvádět	k5eAaImIp3nS	uvádět
městský	městský	k2eAgMnSc1d1	městský
historik	historik	k1gMnSc1	historik
Vilém	Vilém	k1gMnSc1	Vilém
Nikodém	Nikodém	k1gMnSc1	Nikodém
v	v	k7c6	v
textu	text	k1gInSc6	text
pro	pro	k7c4	pro
Ottův	Ottův	k2eAgInSc4d1	Ottův
slovník	slovník	k1gInSc4	slovník
naučný	naučný	k2eAgInSc4d1	naučný
<g/>
.	.	kIx.	.
<g/>
Znak	znak	k1gInSc4	znak
města	město	k1gNnSc2	město
pocházející	pocházející	k2eAgNnSc4d1	pocházející
z	z	k7c2	z
pečeti	pečeť	k1gFnSc2	pečeť
je	být	k5eAaImIp3nS	být
středověkého	středověký	k2eAgInSc2d1	středověký
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
vlajka	vlajka	k1gFnSc1	vlajka
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
odvozená	odvozený	k2eAgFnSc1d1	odvozená
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
schválena	schválit	k5eAaPmNgFnS	schválit
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
číslo	číslo	k1gNnSc1	číslo
10	[number]	k4	10
ze	z	k7c2	z
dne	den	k1gInSc2	den
15	[number]	k4	15
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dějiny	dějiny	k1gFnPc1	dějiny
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
Třebíč	Třebíč	k1gFnSc1	Třebíč
založena	založit	k5eAaPmNgFnS	založit
<g/>
,	,	kIx,	,
neexistovalo	existovat	k5eNaImAgNnS	existovat
žádné	žádný	k3yNgNnSc1	žádný
starší	starý	k2eAgNnSc1d2	starší
osídlení	osídlení	k1gNnSc1	osídlení
<g/>
.	.	kIx.	.
</s>
<s>
Jediné	jediný	k2eAgInPc1d1	jediný
nálezy	nález	k1gInPc1	nález
byly	být	k5eAaImAgInP	být
doloženy	doložit	k5eAaPmNgInP	doložit
na	na	k7c6	na
Karlově	Karlův	k2eAgNnSc6d1	Karlovo
náměstí	náměstí	k1gNnSc6	náměstí
a	a	k8xC	a
v	v	k7c6	v
části	část	k1gFnSc6	část
Stařečky	stařeček	k1gMnPc4	stařeček
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgNnPc6	tento
místech	místo	k1gNnPc6	místo
byly	být	k5eAaImAgFnP	být
nalezeny	nalézt	k5eAaBmNgInP	nalézt
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
základů	základ	k1gInPc2	základ
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgInP	moct
být	být	k5eAaImF	být
starší	starý	k2eAgInPc1d2	starší
než	než	k8xS	než
stavby	stavba	k1gFnPc4	stavba
samotného	samotný	k2eAgNnSc2d1	samotné
města	město	k1gNnSc2	město
a	a	k8xC	a
podle	podle	k7c2	podle
Fišera	Fišer	k1gMnSc2	Fišer
by	by	kYmCp3nS	by
mohly	moct	k5eAaImAgFnP	moct
být	být	k5eAaImF	být
i	i	k9	i
starší	starý	k2eAgMnSc1d2	starší
než	než	k8xS	než
třebíčský	třebíčský	k2eAgInSc1d1	třebíčský
klášter	klášter	k1gInSc1	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
kroniky	kronika	k1gFnSc2	kronika
Eliáše	Eliáš	k1gMnSc2	Eliáš
Střelky	střelka	k1gFnSc2	střelka
Náchodského	náchodský	k2eAgInSc2d1	náchodský
se	se	k3xPyFc4	se
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
kláštera	klášter	k1gInSc2	klášter
mohl	moct	k5eAaImAgMnS	moct
nacházet	nacházet	k5eAaImF	nacházet
hrad	hrad	k1gInSc4	hrad
Dřevín	Dřevína	k1gFnPc2	Dřevína
<g/>
,	,	kIx,	,
či	či	k8xC	či
Dřevíč	Dřevíč	k1gFnSc1	Dřevíč
<g/>
,	,	kIx,	,
tato	tento	k3xDgFnSc1	tento
možnost	možnost	k1gFnSc1	možnost
však	však	k9	však
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
novějších	nový	k2eAgInPc6d2	novější
výzkumech	výzkum	k1gInPc6	výzkum
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Fišera	Fišer	k1gMnSc2	Fišer
již	již	k6eAd1	již
téměř	téměř	k6eAd1	téměř
vyvrácena	vyvrátit	k5eAaPmNgFnS	vyvrátit
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nacházel	nacházet	k5eAaImAgMnS	nacházet
opevněný	opevněný	k2eAgInSc4d1	opevněný
dvorec	dvorec	k1gInSc4	dvorec
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
mohl	moct	k5eAaImAgInS	moct
patřit	patřit	k5eAaImF	patřit
jistému	jistý	k2eAgMnSc3d1	jistý
Třebkovi	Třebek	k1gMnSc3	Třebek
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
jméno	jméno	k1gNnSc1	jméno
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
spojeno	spojit	k5eAaPmNgNnS	spojit
s	s	k7c7	s
místním	místní	k2eAgInSc7d1	místní
názvem	název	k1gInSc7	název
Třebečský	Třebečský	k2eAgInSc1d1	Třebečský
les	les	k1gInSc1	les
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
opevněný	opevněný	k2eAgInSc1d1	opevněný
dvorec	dvorec	k1gInSc1	dvorec
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
nacházet	nacházet	k5eAaImF	nacházet
na	na	k7c6	na
pahorku	pahorek	k1gInSc6	pahorek
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
jménem	jméno	k1gNnSc7	jméno
Horka	horko	k1gNnSc2	horko
<g/>
.	.	kIx.	.
<g/>
První	první	k4xOgFnPc1	první
zmínky	zmínka	k1gFnPc1	zmínka
o	o	k7c6	o
Třebíči	Třebíč	k1gFnSc6	Třebíč
sahají	sahat	k5eAaImIp3nP	sahat
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1101	[number]	k4	1101
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
na	na	k7c6	na
území	území	k1gNnSc6	území
pod	pod	k7c7	pod
Strážnou	strážný	k2eAgFnSc7d1	Strážná
horou	hora	k1gFnSc7	hora
a	a	k8xC	a
při	při	k7c6	při
řece	řeka	k1gFnSc6	řeka
Jihlavě	Jihlava	k1gFnSc6	Jihlava
moravská	moravský	k2eAgFnSc1d1	Moravská
údělná	údělný	k2eAgFnSc1d1	údělná
knížata	kníže	k1gMnPc1wR	kníže
Oldřich	Oldřich	k1gMnSc1	Oldřich
Brněnský	brněnský	k2eAgMnSc1d1	brněnský
a	a	k8xC	a
Litold	Litold	k1gInSc1	Litold
Znojemský	znojemský	k2eAgInSc1d1	znojemský
založila	založit	k5eAaPmAgFnS	založit
benediktinský	benediktinský	k2eAgInSc4d1	benediktinský
klášter	klášter	k1gInSc4	klášter
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
stavbou	stavba	k1gFnSc7	stavba
tohoto	tento	k3xDgInSc2	tento
kláštera	klášter	k1gInSc2	klášter
byl	být	k5eAaImAgInS	být
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Benedikta	Benedikt	k1gMnSc2	Benedikt
vysvěcený	vysvěcený	k2eAgInSc1d1	vysvěcený
roku	rok	k1gInSc2	rok
1104	[number]	k4	1104
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
přistavěna	přistavěn	k2eAgFnSc1d1	přistavěna
nová	nový	k2eAgFnSc1d1	nová
klášterní	klášterní	k2eAgFnSc1d1	klášterní
budova	budova	k1gFnSc1	budova
a	a	k8xC	a
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1260	[number]	k4	1260
bazilika	bazilika	k1gFnSc1	bazilika
svatého	svatý	k2eAgMnSc2d1	svatý
Prokopa	Prokop	k1gMnSc2	Prokop
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc1d3	nejstarší
listina	listina	k1gFnSc1	listina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
Třebíč	Třebíč	k1gFnSc1	Třebíč
zmíněna	zmínit	k5eAaPmNgFnS	zmínit
jako	jako	k8xC	jako
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
napsána	napsat	k5eAaPmNgFnS	napsat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1277	[number]	k4	1277
<g/>
.	.	kIx.	.
</s>
<s>
Povolení	povolení	k1gNnSc4	povolení
opevnit	opevnit	k5eAaPmF	opevnit
se	s	k7c7	s
hradbami	hradba	k1gFnPc7	hradba
a	a	k8xC	a
řídit	řídit	k5eAaImF	řídit
se	se	k3xPyFc4	se
městským	městský	k2eAgNnSc7d1	Městské
právem	právo	k1gNnSc7	právo
byla	být	k5eAaImAgFnS	být
městu	město	k1gNnSc3	město
udělena	udělen	k2eAgFnSc1d1	udělena
roku	rok	k1gInSc2	rok
1335	[number]	k4	1335
Karlem	Karel	k1gMnSc7	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
a	a	k8xC	a
Janem	Jan	k1gMnSc7	Jan
Lucemburským	lucemburský	k2eAgMnSc7d1	lucemburský
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1468	[number]	k4	1468
při	při	k7c6	při
válkách	válka	k1gFnPc6	válka
mezi	mezi	k7c7	mezi
Jiřím	Jiří	k1gMnSc7	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
a	a	k8xC	a
Matyášem	Matyáš	k1gMnSc7	Matyáš
Korvínem	Korvín	k1gMnSc7	Korvín
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
velmi	velmi	k6eAd1	velmi
poničeno	poničen	k2eAgNnSc1d1	poničeno
a	a	k8xC	a
na	na	k7c4	na
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
přestalo	přestat	k5eAaPmAgNnS	přestat
dokonce	dokonce	k9	dokonce
existovat	existovat	k5eAaImF	existovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1492	[number]	k4	1492
město	město	k1gNnSc1	město
mnohokrát	mnohokrát	k6eAd1	mnohokrát
vyhořelo	vyhořet	k5eAaPmAgNnS	vyhořet
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
tak	tak	k9	tak
několikrát	několikrát	k6eAd1	několikrát
přebudováno	přebudován	k2eAgNnSc1d1	přebudováno
a	a	k8xC	a
až	až	k9	až
na	na	k7c4	na
některé	některý	k3yIgInPc4	některý
domy	dům	k1gInPc4	dům
se	se	k3xPyFc4	se
nezachovala	zachovat	k5eNaPmAgFnS	zachovat
jeho	jeho	k3xOp3gFnSc1	jeho
renesanční	renesanční	k2eAgFnSc1d1	renesanční
podoba	podoba	k1gFnSc1	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Třebíč	Třebíč	k1gFnSc1	Třebíč
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
majetkem	majetek	k1gInSc7	majetek
Pernštejnů	Pernštejn	k1gInPc2	Pernštejn
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1525	[number]	k4	1525
byl	být	k5eAaImAgInS	být
násilně	násilně	k6eAd1	násilně
zrušen	zrušit	k5eAaPmNgInS	zrušit
i	i	k9	i
klášter	klášter	k1gInSc1	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1527	[number]	k4	1527
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
stalo	stát	k5eAaPmAgNnS	stát
majetkem	majetek	k1gInSc7	majetek
Osovských	Osovský	k2eAgFnPc2d1	Osovská
z	z	k7c2	z
Doubravice	Doubravice	k1gFnSc2	Doubravice
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
rod	rod	k1gInSc1	rod
se	se	k3xPyFc4	se
zasloužil	zasloužit	k5eAaPmAgInS	zasloužit
o	o	k7c4	o
rozvoj	rozvoj	k1gInSc4	rozvoj
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
získání	získání	k1gNnSc6	získání
práva	právo	k1gNnSc2	právo
pečetního	pečetní	k2eAgNnSc2d1	pečetní
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1582	[number]	k4	1582
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
stalo	stát	k5eAaPmAgNnS	stát
majetkem	majetek	k1gInSc7	majetek
Valdštejnů	Valdštejn	k1gInPc2	Valdštejn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
fara	fara	k1gFnSc1	fara
sv.	sv.	kA	sv.
Martina	Martina	k1gFnSc1	Martina
dočkala	dočkat	k5eAaPmAgFnS	dočkat
povýšení	povýšení	k1gNnSc4	povýšení
na	na	k7c4	na
děkanství	děkanství	k1gNnSc4	děkanství
<g/>
,	,	kIx,	,
tato	tento	k3xDgFnSc1	tento
událost	událost	k1gFnSc1	událost
velmi	velmi	k6eAd1	velmi
výrazně	výrazně	k6eAd1	výrazně
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
rekatolizaci	rekatolizace	k1gFnSc4	rekatolizace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1765	[number]	k4	1765
až	až	k9	až
1769	[number]	k4	1769
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
celá	celý	k2eAgFnSc1d1	celá
Morava	Morava	k1gFnSc1	Morava
<g/>
,	,	kIx,	,
trpělo	trpět	k5eAaImAgNnS	trpět
hladomorem	hladomor	k1gInSc7	hladomor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1821	[number]	k4	1821
a	a	k8xC	a
1822	[number]	k4	1822
město	město	k1gNnSc4	město
poničily	poničit	k5eAaPmAgInP	poničit
velké	velký	k2eAgInPc1d1	velký
požáry	požár	k1gInPc1	požár
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
byly	být	k5eAaImAgFnP	být
odplatou	odplata	k1gFnSc7	odplata
za	za	k7c4	za
protirobotní	protirobotní	k2eAgFnPc4d1	protirobotní
vzpoury	vzpoura	k1gFnPc4	vzpoura
<g/>
,	,	kIx,	,
po	po	k7c6	po
okolních	okolní	k2eAgFnPc6d1	okolní
vesnicích	vesnice	k1gFnPc6	vesnice
byly	být	k5eAaImAgFnP	být
rozdávány	rozdáván	k2eAgInPc4d1	rozdáván
paličské	paličský	k2eAgInPc4d1	paličský
listy	list	k1gInPc4	list
s	s	k7c7	s
hrozbami	hrozba	k1gFnPc7	hrozba
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
od	od	k7c2	od
května	květen	k1gInSc2	květen
do	do	k7c2	do
července	červenec	k1gInSc2	červenec
roku	rok	k1gInSc2	rok
1821	[number]	k4	1821
plněny	plněn	k2eAgInPc1d1	plněn
<g/>
.	.	kIx.	.
</s>
<s>
Shořelo	shořet	k5eAaPmAgNnS	shořet
mnoho	mnoho	k4c1	mnoho
domů	dům	k1gInPc2	dům
v	v	k7c6	v
Židech	Žid	k1gMnPc6	Žid
<g/>
,	,	kIx,	,
v	v	k7c6	v
Týně	Týn	k1gInSc6	Týn
a	a	k8xC	a
na	na	k7c6	na
Jejkově	Jejkův	k2eAgNnSc6d1	Jejkův
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
požár	požár	k1gInSc1	požár
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
domě	dům	k1gInSc6	dům
U	u	k7c2	u
zlatého	zlatý	k2eAgInSc2d1	zlatý
lva	lev	k1gInSc2	lev
a	a	k8xC	a
shořelo	shořet	k5eAaPmAgNnS	shořet
290	[number]	k4	290
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
kostel	kostel	k1gInSc1	kostel
a	a	k8xC	a
kostelní	kostelní	k2eAgFnSc1d1	kostelní
věž	věž	k1gFnSc1	věž
<g/>
.	.	kIx.	.
</s>
<s>
Škoda	škoda	k1gFnSc1	škoda
byla	být	k5eAaImAgFnS	být
odhadnuta	odhadnout	k5eAaPmNgFnS	odhadnout
na	na	k7c4	na
milion	milion	k4xCgInSc4	milion
zlatých	zlatá	k1gFnPc2	zlatá
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1832	[number]	k4	1832
<g/>
,	,	kIx,	,
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
napadat	napadat	k5eAaImF	napadat
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
cholera	cholera	k1gFnSc1	cholera
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1830	[number]	k4	1830
městem	město	k1gNnSc7	město
prohnala	prohnat	k5eAaPmAgFnS	prohnat
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
větších	veliký	k2eAgFnPc2d2	veliký
povodní	povodeň	k1gFnPc2	povodeň
<g/>
,	,	kIx,	,
voda	voda	k1gFnSc1	voda
zaplavila	zaplavit	k5eAaPmAgFnS	zaplavit
celé	celý	k2eAgNnSc4d1	celé
Karlovo	Karlův	k2eAgNnSc4d1	Karlovo
náměstí	náměstí	k1gNnSc4	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1850	[number]	k4	1850
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
městě	město	k1gNnSc6	město
postupně	postupně	k6eAd1	postupně
založena	založen	k2eAgFnSc1d1	založena
spořitelna	spořitelna	k1gFnSc1	spořitelna
(	(	kIx(	(
<g/>
1863	[number]	k4	1863
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Národní	národní	k2eAgInSc1d1	národní
dům	dům	k1gInSc1	dům
a	a	k8xC	a
gymnázium	gymnázium	k1gNnSc1	gymnázium
(	(	kIx(	(
<g/>
obojí	oboj	k1gFnPc2	oboj
1871	[number]	k4	1871
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1885	[number]	k4	1885
bylo	být	k5eAaImAgNnS	být
vztyčeno	vztyčit	k5eAaPmNgNnS	vztyčit
i	i	k8xC	i
sousoší	sousoší	k1gNnSc1	sousoší
sv.	sv.	kA	sv.
Cyrila	Cyril	k1gMnSc2	Cyril
a	a	k8xC	a
Metoděje	Metoděj	k1gMnSc2	Metoděj
na	na	k7c6	na
Karlově	Karlův	k2eAgNnSc6d1	Karlovo
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
,	,	kIx,	,
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
připojeno	připojen	k2eAgNnSc1d1	připojeno
k	k	k7c3	k
železnici	železnice	k1gFnSc3	železnice
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
také	také	k9	také
dnes	dnes	k6eAd1	dnes
významné	významný	k2eAgInPc1d1	významný
podniky	podnik	k1gInPc1	podnik
ZON	ZON	kA	ZON
(	(	kIx(	(
<g/>
zal	zal	k?	zal
<g/>
.	.	kIx.	.
roku	rok	k1gInSc2	rok
1879	[number]	k4	1879
<g/>
)	)	kIx)	)
a	a	k8xC	a
BOPO	BOPO	kA	BOPO
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Carl	Carl	k1gMnSc1	Carl
Budischowski	Budischowsk	k1gFnSc2	Budischowsk
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
neexistuje	existovat	k5eNaImIp3nS	existovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
doktorem	doktor	k1gMnSc7	doktor
Jaroslavem	Jaroslav	k1gMnSc7	Jaroslav
Bakešem	Bakeš	k1gMnSc7	Bakeš
založena	založen	k2eAgFnSc1d1	založena
nemocnice	nemocnice	k1gFnSc1	nemocnice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
třicátých	třicátý	k4xOgNnPc6	třicátý
letech	léto	k1gNnPc6	léto
byly	být	k5eAaImAgFnP	být
zakoupeny	zakoupen	k2eAgInPc4d1	zakoupen
koželužské	koželužský	k2eAgInPc4d1	koželužský
závody	závod	k1gInPc4	závod
Budischowski	Budischowsk	k1gFnSc2	Budischowsk
společností	společnost	k1gFnSc7	společnost
Tomáše	Tomáš	k1gMnSc2	Tomáš
Bati	Baťa	k1gMnSc2	Baťa
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
začal	začít	k5eAaPmAgMnS	začít
v	v	k7c6	v
Borovině	borovina	k1gFnSc6	borovina
stavět	stavět	k5eAaImF	stavět
dělnické	dělnický	k2eAgFnPc4d1	Dělnická
kolonie	kolonie	k1gFnPc4	kolonie
<g/>
,	,	kIx,	,
známé	známý	k2eAgInPc4d1	známý
cihlové	cihlový	k2eAgInPc4d1	cihlový
dvojdomky	dvojdomek	k1gInPc4	dvojdomek
<g/>
,	,	kIx,	,
a	a	k8xC	a
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
tak	tak	k6eAd1	tak
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
dnešní	dnešní	k2eAgFnSc2d1	dnešní
místní	místní	k2eAgFnSc2d1	místní
části	část	k1gFnSc2	část
Borovina	borovina	k1gFnSc1	borovina
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dvacátých	dvacátý	k4xOgNnPc6	dvacátý
a	a	k8xC	a
třicátých	třicátý	k4xOgNnPc6	třicátý
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgMnS	být
starostou	starosta	k1gMnSc7	starosta
města	město	k1gNnSc2	město
významný	významný	k2eAgMnSc1d1	významný
vlastenec	vlastenec	k1gMnSc1	vlastenec
Josef	Josef	k1gMnSc1	Josef
Vaněk	Vaněk	k1gMnSc1	Vaněk
<g/>
,	,	kIx,	,
zažilo	zažít	k5eAaPmAgNnS	zažít
město	město	k1gNnSc4	město
velký	velký	k2eAgInSc1d1	velký
rozmach	rozmach	k1gInSc1	rozmach
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
také	také	k6eAd1	také
uvítal	uvítat	k5eAaPmAgInS	uvítat
13	[number]	k4	13
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1928	[number]	k4	1928
na	na	k7c6	na
oficiální	oficiální	k2eAgFnSc6d1	oficiální
návštěvě	návštěva	k1gFnSc6	návštěva
města	město	k1gNnSc2	město
prezidenta	prezident	k1gMnSc2	prezident
T.	T.	kA	T.
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
si	se	k3xPyFc3	se
prohlédl	prohlédnout	k5eAaPmAgMnS	prohlédnout
i	i	k9	i
románsko-gotickou	románskootický	k2eAgFnSc4d1	románsko-gotická
baziliku	bazilika	k1gFnSc4	bazilika
sv.	sv.	kA	sv.
Prokopa	Prokop	k1gMnSc2	Prokop
<g/>
,	,	kIx,	,
na	na	k7c4	na
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
opravu	oprava	k1gFnSc4	oprava
věnoval	věnovat	k5eAaImAgInS	věnovat
osobní	osobní	k2eAgInSc1d1	osobní
dar	dar	k1gInSc1	dar
(	(	kIx(	(
<g/>
15	[number]	k4	15
000	[number]	k4	000
Kč	Kč	kA	Kč
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
probíhaly	probíhat	k5eAaImAgFnP	probíhat
velkolepé	velkolepý	k2eAgFnPc1d1	velkolepá
oslavy	oslava	k1gFnPc1	oslava
Třebíč	Třebíč	k1gFnSc1	Třebíč
600	[number]	k4	600
let	let	k1gInSc1	let
městem	město	k1gNnSc7	město
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yIgFnPc4	který
byly	být	k5eAaImAgFnP	být
pozvány	pozvat	k5eAaPmNgFnP	pozvat
i	i	k9	i
významné	významný	k2eAgFnPc1d1	významná
osobnosti	osobnost	k1gFnPc1	osobnost
z	z	k7c2	z
celé	celý	k2eAgFnSc2d1	celá
Československé	československý	k2eAgFnSc2d1	Československá
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
<g/>
Za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgInS	být
rozvoj	rozvoj	k1gInSc1	rozvoj
města	město	k1gNnSc2	město
zpomalen	zpomalit	k5eAaPmNgMnS	zpomalit
<g/>
,	,	kIx,	,
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
příčin	příčina	k1gFnPc2	příčina
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
vyhnání	vyhnání	k1gNnSc4	vyhnání
židů	žid	k1gMnPc2	žid
z	z	k7c2	z
Třebíče	Třebíč	k1gFnSc2	Třebíč
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc1	jejich
transporty	transport	k1gInPc1	transport
do	do	k7c2	do
koncentračních	koncentrační	k2eAgInPc2d1	koncentrační
táborů	tábor	k1gInPc2	tábor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
také	také	k9	také
sídlila	sídlit	k5eAaImAgFnS	sídlit
německá	německý	k2eAgFnSc1d1	německá
posádka	posádka	k1gFnSc1	posádka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
rostla	růst	k5eAaImAgFnS	růst
síla	síla	k1gFnSc1	síla
komunistů	komunista	k1gMnPc2	komunista
<g/>
,	,	kIx,	,
mohlo	moct	k5eAaImAgNnS	moct
tomu	ten	k3xDgMnSc3	ten
být	být	k5eAaImF	být
i	i	k9	i
kvůli	kvůli	k7c3	kvůli
vysokému	vysoký	k2eAgInSc3d1	vysoký
počtu	počet	k1gInSc3	počet
dělníků	dělník	k1gMnPc2	dělník
a	a	k8xC	a
kvůli	kvůli	k7c3	kvůli
některým	některý	k3yIgMnPc3	některý
tehdejším	tehdejší	k2eAgMnPc3d1	tehdejší
politikům	politik	k1gMnPc3	politik
nebo	nebo	k8xC	nebo
armádním	armádní	k2eAgMnPc3d1	armádní
činitelům	činitel	k1gMnPc3	činitel
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
z	z	k7c2	z
Třebíče	Třebíč	k1gFnSc2	Třebíč
pocházeli	pocházet	k5eAaImAgMnP	pocházet
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tzv.	tzv.	kA	tzv.
Únoru	únor	k1gInSc6	únor
1948	[number]	k4	1948
byly	být	k5eAaImAgFnP	být
zestátněny	zestátnit	k5eAaPmNgFnP	zestátnit
velké	velký	k2eAgFnPc1d1	velká
třebíčské	třebíčský	k2eAgFnPc1d1	Třebíčská
společnosti	společnost	k1gFnPc1	společnost
a	a	k8xC	a
město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
rozvíjeno	rozvíjet	k5eAaImNgNnS	rozvíjet
podle	podle	k7c2	podle
centrálních	centrální	k2eAgInPc2d1	centrální
plánů	plán	k1gInPc2	plán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
budovat	budovat	k5eAaImF	budovat
nová	nový	k2eAgNnPc4d1	nové
sídliště	sídliště	k1gNnPc4	sídliště
<g/>
,	,	kIx,	,
školy	škola	k1gFnPc4	škola
a	a	k8xC	a
další	další	k2eAgNnPc4d1	další
zařízení	zařízení	k1gNnPc4	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
byla	být	k5eAaImAgFnS	být
stavba	stavba	k1gFnSc1	stavba
a	a	k8xC	a
provoz	provoz	k1gInSc1	provoz
jaderné	jaderný	k2eAgFnSc2d1	jaderná
elektrárny	elektrárna	k1gFnSc2	elektrárna
Dukovany	Dukovany	k1gInPc1	Dukovany
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
80	[number]	k4	80
<g/>
.	.	kIx.	.
a	a	k8xC	a
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
bylo	být	k5eAaImAgNnS	být
také	také	k6eAd1	také
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
zrychlení	zrychlení	k1gNnSc3	zrychlení
hromadné	hromadný	k2eAgFnSc2d1	hromadná
dopravy	doprava	k1gFnSc2	doprava
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Třebíče	Třebíč	k1gFnSc2	Třebíč
a	a	k8xC	a
okolí	okolí	k1gNnSc4	okolí
<g/>
,	,	kIx,	,
plánováno	plánovat	k5eAaImNgNnS	plánovat
zavedení	zavedení	k1gNnSc1	zavedení
trolejbusů	trolejbus	k1gInPc2	trolejbus
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
nikdy	nikdy	k6eAd1	nikdy
nebylo	být	k5eNaImAgNnS	být
realizováno	realizovat	k5eAaBmNgNnS	realizovat
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
se	se	k3xPyFc4	se
městem	město	k1gNnSc7	město
prohnala	prohnat	k5eAaPmAgFnS	prohnat
další	další	k2eAgFnSc1d1	další
z	z	k7c2	z
velkých	velký	k2eAgFnPc2d1	velká
povodní	povodeň	k1gFnPc2	povodeň
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
povodně	povodeň	k1gFnPc1	povodeň
vrátily	vrátit	k5eAaPmAgFnP	vrátit
ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2005	[number]	k4	2005
a	a	k8xC	a
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
sametové	sametový	k2eAgFnSc6d1	sametová
revoluci	revoluce	k1gFnSc6	revoluce
Třebíč	Třebíč	k1gFnSc1	Třebíč
zažila	zažít	k5eAaPmAgFnS	zažít
velký	velký	k2eAgInSc4d1	velký
rozvoj	rozvoj	k1gInSc4	rozvoj
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2003	[number]	k4	2003
se	se	k3xPyFc4	se
židovské	židovský	k2eAgNnSc1d1	Židovské
ghetto	ghetto	k1gNnSc1	ghetto
a	a	k8xC	a
bazilika	bazilika	k1gFnSc1	bazilika
staly	stát	k5eAaPmAgFnP	stát
součástí	součást	k1gFnSc7	součást
seznamu	seznam	k1gInSc2	seznam
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
podporován	podporován	k2eAgInSc4d1	podporován
regionální	regionální	k2eAgInSc4d1	regionální
rozvoj	rozvoj	k1gInSc4	rozvoj
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
otevřena	otevřít	k5eAaPmNgFnS	otevřít
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
zóna	zóna	k1gFnSc1	zóna
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
otevírány	otevírán	k2eAgInPc4d1	otevírán
nové	nový	k2eAgInPc4d1	nový
obchody	obchod	k1gInPc4	obchod
(	(	kIx(	(
<g/>
byl	být	k5eAaImAgMnS	být
zde	zde	k6eAd1	zde
mj.	mj.	kA	mj.
otevřen	otevřít	k5eAaPmNgInS	otevřít
první	první	k4xOgInSc1	první
supermarket	supermarket	k1gInSc1	supermarket
Spar	Spar	k?	Spar
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
)	)	kIx)	)
a	a	k8xC	a
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
podnikání	podnikání	k1gNnSc4	podnikání
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
roky	rok	k1gInPc4	rok
2009	[number]	k4	2009
až	až	k8xS	až
2013	[number]	k4	2013
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
několika	několik	k4yIc6	několik
etapách	etapa	k1gFnPc6	etapa
plánována	plánován	k2eAgFnSc1d1	plánována
modernizace	modernizace	k1gFnSc1	modernizace
a	a	k8xC	a
elektrifikace	elektrifikace	k1gFnSc1	elektrifikace
železniční	železniční	k2eAgFnSc2d1	železniční
tratě	trať	k1gFnSc2	trať
č.	č.	k?	č.
240	[number]	k4	240
vedoucí	vedoucí	k1gFnSc1	vedoucí
přes	přes	k7c4	přes
Třebíč	Třebíč	k1gFnSc4	Třebíč
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
zveřejněny	zveřejnit	k5eAaPmNgInP	zveřejnit
i	i	k9	i
návrhy	návrh	k1gInPc4	návrh
tras	trasa	k1gFnPc2	trasa
pro	pro	k7c4	pro
výstavbu	výstavba	k1gFnSc4	výstavba
velkého	velký	k2eAgInSc2d1	velký
městského	městský	k2eAgInSc2d1	městský
okruhu	okruh	k1gInSc2	okruh
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
budově	budova	k1gFnSc6	budova
radnice	radnice	k1gFnSc2	radnice
odhalena	odhalen	k2eAgFnSc1d1	odhalena
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
letcům	letec	k1gMnPc3	letec
RAF	raf	k0	raf
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
Eva	Eva	k1gFnSc1	Eva
Pokorná	Pokorná	k1gFnSc1	Pokorná
vydala	vydat	k5eAaPmAgFnS	vydat
publikaci	publikace	k1gFnSc4	publikace
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
právě	právě	k6eAd1	právě
šesti	šest	k4xCc7	šest
třebíčskými	třebíčský	k2eAgMnPc7d1	třebíčský
letci	letec	k1gMnPc7	letec
ve	v	k7c6	v
službách	služba	k1gFnPc6	služba
RAF	raf	k0	raf
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
Třebíč	Třebíč	k1gFnSc1	Třebíč
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
části	část	k1gFnSc6	část
okresu	okres	k1gInSc2	okres
Třebíč	Třebíč	k1gFnSc1	Třebíč
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
části	část	k1gFnSc6	část
Kraje	kraj	k1gInSc2	kraj
Vysočina	vysočina	k1gFnSc1	vysočina
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
krajského	krajský	k2eAgNnSc2d1	krajské
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
Třebíč	Třebíč	k1gFnSc1	Třebíč
vzdálena	vzdálen	k2eAgFnSc1d1	vzdálena
35	[number]	k4	35
km	km	kA	km
jihovýchodním	jihovýchodní	k2eAgInSc7d1	jihovýchodní
směrem	směr	k1gInSc7	směr
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
Jihlavě	Jihlava	k1gFnSc6	Jihlava
druhým	druhý	k4xOgNnSc7	druhý
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
v	v	k7c6	v
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgInSc7	druhý
blízkým	blízký	k2eAgNnSc7d1	blízké
velkým	velký	k2eAgNnSc7d1	velké
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
bývalé	bývalý	k2eAgNnSc1d1	bývalé
zemské	zemský	k2eAgNnSc1d1	zemské
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
vzdálené	vzdálený	k2eAgNnSc1d1	vzdálené
od	od	k7c2	od
Třebíče	Třebíč	k1gFnSc2	Třebíč
56	[number]	k4	56
km	km	kA	km
východním	východní	k2eAgInSc7d1	východní
směrem	směr	k1gInSc7	směr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Centrum	centrum	k1gNnSc1	centrum
města	město	k1gNnSc2	město
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
řeky	řeka	k1gFnSc2	řeka
Jihlavy	Jihlava	k1gFnSc2	Jihlava
<g/>
,	,	kIx,	,
hlubokém	hluboký	k2eAgNnSc6d1	hluboké
šedesát	šedesát	k4xCc1	šedesát
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Zbytek	zbytek	k1gInSc1	zbytek
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
na	na	k7c6	na
návrších	návrš	k1gFnPc6	návrš
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
místy	místy	k6eAd1	místy
střídají	střídat	k5eAaImIp3nP	střídat
s	s	k7c7	s
údolími	údolí	k1gNnPc7	údolí
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgInPc7	jenž
do	do	k7c2	do
řeky	řeka	k1gFnSc2	řeka
tečou	téct	k5eAaImIp3nP	téct
potoky	potok	k1gInPc1	potok
<g/>
.	.	kIx.	.
</s>
<s>
Krajina	Krajina	k1gFnSc1	Krajina
severně	severně	k6eAd1	severně
od	od	k7c2	od
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
rovinatá	rovinatý	k2eAgFnSc1d1	rovinatá
s	s	k7c7	s
typickými	typický	k2eAgInPc7d1	typický
remízky	remízek	k1gInPc7	remízek
a	a	k8xC	a
velkými	velký	k2eAgInPc7d1	velký
syenitovými	syenitový	k2eAgInPc7d1	syenitový
balvany	balvan	k1gInPc7	balvan
<g/>
,	,	kIx,	,
doplněná	doplněný	k2eAgNnPc4d1	doplněné
soustavami	soustava	k1gFnPc7	soustava
rybníků	rybník	k1gInPc2	rybník
<g/>
.	.	kIx.	.
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1	jižní
krajina	krajina	k1gFnSc1	krajina
je	být	k5eAaImIp3nS	být
jiného	jiný	k2eAgInSc2d1	jiný
charakteru	charakter	k1gInSc2	charakter
a	a	k8xC	a
nabízí	nabízet	k5eAaImIp3nS	nabízet
mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgFnPc4d2	veliký
možnosti	možnost	k1gFnPc4	možnost
využití	využití	k1gNnSc2	využití
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
tři	tři	k4xCgInPc4	tři
kilometry	kilometr	k1gInPc4	kilometr
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
se	se	k3xPyFc4	se
od	od	k7c2	od
města	město	k1gNnSc2	město
zvedá	zvedat	k5eAaImIp3nS	zvedat
sopečný	sopečný	k2eAgInSc1d1	sopečný
masiv	masiv	k1gInSc1	masiv
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
Stařečské	Stařečský	k2eAgFnSc2d1	Stařečský
a	a	k8xC	a
Jevišovické	Jevišovický	k2eAgFnSc2d1	Jevišovická
pahorkatiny	pahorkatina	k1gFnSc2	pahorkatina
<g/>
,	,	kIx,	,
táhnoucí	táhnoucí	k2eAgInPc4d1	táhnoucí
se	se	k3xPyFc4	se
od	od	k7c2	od
jihovýchodu	jihovýchod	k1gInSc2	jihovýchod
až	až	k9	až
na	na	k7c4	na
západ	západ	k1gInSc4	západ
<g/>
.	.	kIx.	.
</s>
<s>
Výraznou	výrazný	k2eAgFnSc7d1	výrazná
dominantou	dominanta	k1gFnSc7	dominanta
okolí	okolí	k1gNnSc2	okolí
je	být	k5eAaImIp3nS	být
Klučovská	Klučovský	k2eAgFnSc1d1	Klučovská
hora	hora	k1gFnSc1	hora
s	s	k7c7	s
televizním	televizní	k2eAgInSc7d1	televizní
vysílačem	vysílač	k1gInSc7	vysílač
pro	pro	k7c4	pro
Třebíč	Třebíč	k1gFnSc4	Třebíč
a	a	k8xC	a
okolí	okolí	k1gNnSc4	okolí
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
Jevišovické	Jevišovický	k2eAgFnSc3d1	Jevišovická
pahorkatině	pahorkatina	k1gFnSc3	pahorkatina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jejím	její	k3xOp3gNnSc6	její
sousedství	sousedství	k1gNnSc6	sousedství
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
pahorek	pahorek	k1gInSc1	pahorek
Hošťanka	Hošťanka	k1gFnSc1	Hošťanka
s	s	k7c7	s
přírodní	přírodní	k2eAgFnSc7d1	přírodní
rezervací	rezervace	k1gFnSc7	rezervace
<g/>
.	.	kIx.	.
</s>
<s>
Významnými	významný	k2eAgInPc7d1	významný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
od	od	k7c2	od
města	město	k1gNnSc2	město
již	již	k6eAd1	již
vzdálenějšími	vzdálený	k2eAgInPc7d2	vzdálenější
vrcholy	vrchol	k1gInPc7	vrchol
masivu	masiv	k1gInSc2	masiv
jsou	být	k5eAaImIp3nP	být
Pekelný	pekelný	k2eAgInSc4d1	pekelný
kopec	kopec	k1gInSc4	kopec
<g/>
,	,	kIx,	,
oblíbený	oblíbený	k2eAgInSc4d1	oblíbený
cíl	cíl	k1gInSc4	cíl
turistů	turist	k1gMnPc2	turist
<g/>
,	,	kIx,	,
a	a	k8xC	a
Zadní	zadní	k2eAgFnSc1d1	zadní
hora	hora	k1gFnSc1	hora
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
nachází	nacházet	k5eAaImIp3nS	nacházet
radiotelevizní	radiotelevizní	k2eAgInSc1d1	radiotelevizní
vysílač	vysílač	k1gInSc1	vysílač
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Třebíč	Třebíč	k1gFnSc1	Třebíč
byla	být	k5eAaImAgFnS	být
územně	územně	k6eAd1	územně
příslušná	příslušný	k2eAgFnSc1d1	příslušná
k	k	k7c3	k
několika	několik	k4yIc3	několik
krajům	kraj	k1gInPc3	kraj
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1849	[number]	k4	1849
ke	k	k7c3	k
kraji	kraj	k1gInSc3	kraj
Jihlavskému	jihlavský	k2eAgInSc3d1	jihlavský
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1850	[number]	k4	1850
k	k	k7c3	k
Brněnskému	brněnský	k2eAgMnSc3d1	brněnský
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1854	[number]	k4	1854
opět	opět	k6eAd1	opět
Jihlavskému	jihlavský	k2eAgMnSc3d1	jihlavský
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něm	on	k3xPp3gInSc6	on
zůstala	zůstat	k5eAaPmAgNnP	zůstat
až	až	k9	až
do	do	k7c2	do
ukončení	ukončení	k1gNnSc2	ukončení
krajské	krajský	k2eAgFnSc2d1	krajská
samosprávy	samospráva	k1gFnSc2	samospráva
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1868	[number]	k4	1868
<g/>
.	.	kIx.	.
</s>
<s>
Posléze	posléze	k6eAd1	posléze
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
stala	stát	k5eAaPmAgFnS	stát
na	na	k7c4	na
osm	osm	k4xCc4	osm
let	léto	k1gNnPc2	léto
součástí	součást	k1gFnPc2	součást
"	"	kIx"	"
<g/>
jihlavské	jihlavský	k2eAgFnSc2d1	Jihlavská
<g/>
"	"	kIx"	"
župy	župa	k1gFnSc2	župa
X.	X.	kA	X.
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
patřila	patřit	k5eAaImAgFnS	patřit
do	do	k7c2	do
Jihlavského	jihlavský	k2eAgInSc2d1	jihlavský
kraje	kraj	k1gInSc2	kraj
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
do	do	k7c2	do
Jihomoravského	jihomoravský	k2eAgInSc2d1	jihomoravský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ekologie	ekologie	k1gFnSc1	ekologie
===	===	k?	===
</s>
</p>
<p>
<s>
Třebíč	Třebíč	k1gFnSc1	Třebíč
je	být	k5eAaImIp3nS	být
druhé	druhý	k4xOgNnSc1	druhý
město	město	k1gNnSc1	město
s	s	k7c7	s
nejvyšším	vysoký	k2eAgNnSc7d3	nejvyšší
procentem	procento	k1gNnSc7	procento
zateplených	zateplený	k2eAgInPc2d1	zateplený
domů	dům	k1gInPc2	dům
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Domácnosti	domácnost	k1gFnPc1	domácnost
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
jsou	být	k5eAaImIp3nP	být
rovněž	rovněž	k9	rovněž
vytápěny	vytápět	k5eAaImNgInP	vytápět
biomasou	biomasa	k1gFnSc7	biomasa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
byla	být	k5eAaImAgFnS	být
otevřena	otevřít	k5eAaPmNgFnS	otevřít
již	již	k9	již
třetí	třetí	k4xOgFnSc1	třetí
teplárna	teplárna	k1gFnSc1	teplárna
na	na	k7c4	na
biomasu	biomasa	k1gFnSc4	biomasa
<g/>
.	.	kIx.	.
</s>
<s>
Radnice	radnice	k1gFnSc1	radnice
města	město	k1gNnSc2	město
umístila	umístit	k5eAaPmAgFnS	umístit
na	na	k7c6	na
vybraných	vybraný	k2eAgNnPc6d1	vybrané
místech	místo	k1gNnPc6	místo
cedulky	cedulka	k1gFnSc2	cedulka
s	s	k7c7	s
informacemi	informace	k1gFnPc7	informace
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
občané	občan	k1gMnPc1	občan
města	město	k1gNnSc2	město
měli	mít	k5eAaImAgMnP	mít
uklízet	uklízet	k5eAaImF	uklízet
po	po	k7c6	po
svých	svůj	k3xOyFgMnPc6	svůj
psech	pes	k1gMnPc6	pes
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
uspořádala	uspořádat	k5eAaPmAgFnS	uspořádat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
letákovou	letákový	k2eAgFnSc4d1	letáková
kampaň	kampaň	k1gFnSc4	kampaň
na	na	k7c4	na
stejné	stejný	k2eAgNnSc4d1	stejné
téma	téma	k1gNnSc4	téma
<g/>
.	.	kIx.	.
</s>
<s>
Správce	správce	k1gMnSc1	správce
městské	městský	k2eAgFnSc2d1	městská
zeleně	zeleň	k1gFnSc2	zeleň
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
netěší	těšit	k5eNaImIp3nS	těšit
ubývání	ubývání	k1gNnSc1	ubývání
městské	městský	k2eAgFnSc2d1	městská
zeleně	zeleň	k1gFnSc2	zeleň
<g/>
.	.	kIx.	.
</s>
<s>
Poblíž	poblíž	k7c2	poblíž
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
také	také	k9	také
nachází	nacházet	k5eAaImIp3nS	nacházet
více	hodně	k6eAd2	hodně
společností	společnost	k1gFnPc2	společnost
a	a	k8xC	a
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
chráněných	chráněný	k2eAgFnPc2d1	chráněná
oblastí	oblast	k1gFnPc2	oblast
znečišťují	znečišťovat	k5eAaImIp3nP	znečišťovat
životní	životní	k2eAgNnPc4d1	životní
prostředí	prostředí	k1gNnPc4	prostředí
<g/>
;	;	kIx,	;
velkou	velký	k2eAgFnSc7d1	velká
ekologickou	ekologický	k2eAgFnSc7d1	ekologická
zátěží	zátěž	k1gFnSc7	zátěž
byla	být	k5eAaImAgFnS	být
např.	např.	kA	např.
blízká	blízký	k2eAgFnSc1d1	blízká
skládka	skládka	k1gFnSc1	skládka
Pozďátky	Pozďátka	k1gFnSc2	Pozďátka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
nový	nový	k2eAgInSc1d1	nový
návrh	návrh	k1gInSc1	návrh
územní	územní	k2eAgFnSc2d1	územní
studie	studie	k1gFnSc2	studie
krajiny	krajina	k1gFnSc2	krajina
<g/>
,	,	kIx,	,
do	do	k7c2	do
jehož	jehož	k3xOyRp3gFnSc2	jehož
tvorby	tvorba	k1gFnSc2	tvorba
se	se	k3xPyFc4	se
mohli	moct	k5eAaImAgMnP	moct
zapojit	zapojit	k5eAaPmF	zapojit
i	i	k9	i
občané	občan	k1gMnPc1	občan
pomocí	pomocí	k7c2	pomocí
interaktivní	interaktivní	k2eAgFnSc2d1	interaktivní
mapy	mapa	k1gFnSc2	mapa
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2019	[number]	k4	2019
bylo	být	k5eAaImAgNnS	být
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
město	město	k1gNnSc1	město
omezí	omezit	k5eAaPmIp3nS	omezit
sečení	sečení	k1gNnSc4	sečení
městských	městský	k2eAgInPc2d1	městský
trávníků	trávník	k1gInPc2	trávník
<g/>
,	,	kIx,	,
budou	být	k5eAaImBp3nP	být
také	také	k6eAd1	také
založeny	založit	k5eAaPmNgFnP	založit
květné	květný	k2eAgFnPc1d1	Květná
louky	louka	k1gFnPc1	louka
<g/>
.	.	kIx.	.
</s>
<s>
Prvními	první	k4xOgFnPc7	první
květnými	květný	k2eAgFnPc7d1	Květná
loukami	louka	k1gFnPc7	louka
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgInP	mít
být	být	k5eAaImF	být
svahy	svah	k1gInPc1	svah
Strážné	strážný	k2eAgFnSc2d1	Strážná
hory	hora	k1gFnSc2	hora
<g/>
,	,	kIx,	,
místa	místo	k1gNnSc2	místo
kolem	kolem	k7c2	kolem
ulice	ulice	k1gFnSc2	ulice
Rafaelova	Rafaelův	k2eAgMnSc2d1	Rafaelův
a	a	k8xC	a
plochy	plocha	k1gFnPc4	plocha
na	na	k7c6	na
sídlišti	sídliště	k1gNnSc6	sídliště
Obránců	obránce	k1gMnPc2	obránce
míru	mír	k1gInSc2	mír
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Klima	klima	k1gNnSc1	klima
===	===	k?	===
</s>
</p>
<p>
<s>
Třebíč	Třebíč	k1gFnSc1	Třebíč
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
mírně	mírně	k6eAd1	mírně
teplé	teplý	k2eAgFnSc6d1	teplá
oblasti	oblast	k1gFnSc6	oblast
s	s	k7c7	s
občasnými	občasný	k2eAgInPc7d1	občasný
dešti	dešť	k1gInPc7	dešť
<g/>
.	.	kIx.	.
</s>
<s>
Převládá	převládat	k5eAaImIp3nS	převládat
zde	zde	k6eAd1	zde
severozápadní	severozápadní	k2eAgNnSc1d1	severozápadní
a	a	k8xC	a
západní	západní	k2eAgNnSc1d1	západní
proudění	proudění	k1gNnSc1	proudění
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
východu	východ	k1gInSc2	východ
sem	sem	k6eAd1	sem
pak	pak	k6eAd1	pak
proudí	proudit	k5eAaPmIp3nS	proudit
především	především	k9	především
chladný	chladný	k2eAgInSc1d1	chladný
vzduch	vzduch	k1gInSc1	vzduch
a	a	k8xC	a
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
větší	veliký	k2eAgNnSc4d2	veliký
množství	množství	k1gNnSc4	množství
sněhových	sněhový	k2eAgFnPc2d1	sněhová
srážek	srážka	k1gFnPc2	srážka
<g/>
.	.	kIx.	.
</s>
<s>
Ročně	ročně	k6eAd1	ročně
spadne	spadnout	k5eAaPmIp3nS	spadnout
asi	asi	k9	asi
560	[number]	k4	560
mm	mm	kA	mm
srážek	srážka	k1gFnPc2	srážka
(	(	kIx(	(
<g/>
na	na	k7c4	na
vegetační	vegetační	k2eAgNnSc4d1	vegetační
období	období	k1gNnSc4	období
připadá	připadat	k5eAaPmIp3nS	připadat
asi	asi	k9	asi
350	[number]	k4	350
<g/>
–	–	k?	–
<g/>
400	[number]	k4	400
mm	mm	kA	mm
<g/>
,	,	kIx,	,
na	na	k7c4	na
zimní	zimní	k2eAgNnSc4d1	zimní
období	období	k1gNnSc4	období
pak	pak	k6eAd1	pak
200	[number]	k4	200
<g/>
–	–	k?	–
<g/>
250	[number]	k4	250
mm	mm	kA	mm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
roční	roční	k2eAgFnSc1d1	roční
teplota	teplota	k1gFnSc1	teplota
činí	činit	k5eAaImIp3nS	činit
7,5	[number]	k4	7,5
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
pak	pak	k6eAd1	pak
18,5	[number]	k4	18,5
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
−	−	k?	−
°	°	k?	°
<g/>
C.	C.	kA	C.
</s>
</p>
<p>
<s>
===	===	k?	===
Okolí	okolí	k1gNnSc1	okolí
města	město	k1gNnSc2	město
===	===	k?	===
</s>
</p>
<p>
<s>
Poblíž	poblíž	k7c2	poblíž
Třebíče	Třebíč	k1gFnSc2	Třebíč
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
zachovalá	zachovalý	k2eAgFnSc1d1	zachovalá
příroda	příroda	k1gFnSc1	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
byla	být	k5eAaImAgFnS	být
původní	původní	k2eAgFnSc1d1	původní
vegetace	vegetace	k1gFnSc1	vegetace
rozrušena	rozrušen	k2eAgFnSc1d1	rozrušena
velkými	velký	k2eAgNnPc7d1	velké
poli	pole	k1gNnPc7	pole
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
vznikla	vzniknout	k5eAaPmAgNnP	vzniknout
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
výskytu	výskyt	k1gInSc3	výskyt
mnoha	mnoho	k4c2	mnoho
vzácných	vzácný	k2eAgInPc2d1	vzácný
druhů	druh	k1gInPc2	druh
rostlin	rostlina	k1gFnPc2	rostlina
a	a	k8xC	a
živočichů	živočich	k1gMnPc2	živočich
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Třebíče	Třebíč	k1gFnSc2	Třebíč
existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc1	několik
přírodních	přírodní	k2eAgInPc2d1	přírodní
parků	park	k1gInPc2	park
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Přírodní	přírodní	k2eAgInSc1d1	přírodní
park	park	k1gInSc1	park
Třebíčsko	Třebíčsko	k1gNnSc1	Třebíčsko
</s>
</p>
<p>
<s>
Přírodní	přírodní	k2eAgInSc1d1	přírodní
park	park	k1gInSc1	park
Pojihlaví	Pojihlavý	k2eAgMnPc5d1	Pojihlavý
</s>
</p>
<p>
<s>
Přírodní	přírodní	k2eAgInSc1d1	přírodní
park	park	k1gInSc1	park
RokytnáDále	RokytnáDála	k1gFnSc3	RokytnáDála
se	se	k3xPyFc4	se
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Třebíče	Třebíč	k1gFnSc2	Třebíč
nachází	nacházet	k5eAaImIp3nS	nacházet
několik	několik	k4yIc4	několik
památek	památka	k1gFnPc2	památka
a	a	k8xC	a
přírodních	přírodní	k2eAgFnPc2d1	přírodní
rezervací	rezervace	k1gFnPc2	rezervace
a	a	k8xC	a
přibližně	přibližně	k6eAd1	přibližně
100	[number]	k4	100
památných	památný	k2eAgInPc2d1	památný
stromů	strom	k1gInPc2	strom
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geologie	geologie	k1gFnSc2	geologie
==	==	k?	==
</s>
</p>
<p>
<s>
Z	z	k7c2	z
geologického	geologický	k2eAgNnSc2d1	geologické
hlediska	hledisko	k1gNnSc2	hledisko
se	se	k3xPyFc4	se
prakticky	prakticky	k6eAd1	prakticky
celá	celý	k2eAgFnSc1d1	celá
zastavěná	zastavěný	k2eAgFnSc1d1	zastavěná
část	část	k1gFnSc1	část
města	město	k1gNnSc2	město
Třebíče	Třebíč	k1gFnSc2	Třebíč
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
tzv.	tzv.	kA	tzv.
třebíčském	třebíčský	k2eAgInSc6d1	třebíčský
masivu	masiv	k1gInSc6	masiv
(	(	kIx(	(
<g/>
ve	v	k7c6	v
starší	starý	k2eAgFnSc6d2	starší
literatuře	literatura	k1gFnSc6	literatura
též	též	k9	též
třebíčsko-velkomeziříčský	třebíčskoelkomeziříčský	k2eAgInSc4d1	třebíčsko-velkomeziříčský
masiv	masiv	k1gInSc4	masiv
<g/>
)	)	kIx)	)
někdy	někdy	k6eAd1	někdy
označovaném	označovaný	k2eAgNnSc6d1	označované
také	také	k9	také
jako	jako	k8xS	jako
třebíčská	třebíčský	k2eAgFnSc1d1	Třebíčská
plošina	plošina	k1gFnSc1	plošina
či	či	k8xC	či
třebíčský	třebíčský	k2eAgInSc1d1	třebíčský
trojúhelník	trojúhelník	k1gInSc1	trojúhelník
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
magmatické	magmatický	k2eAgNnSc4d1	magmatické
těleso	těleso	k1gNnSc4	těleso
o	o	k7c6	o
ploše	plocha	k1gFnSc6	plocha
cca	cca	kA	cca
500	[number]	k4	500
km	km	kA	km
<g/>
2	[number]	k4	2
rozkládající	rozkládající	k2eAgMnSc1d1	rozkládající
se	se	k3xPyFc4	se
přibližně	přibližně	k6eAd1	přibližně
mezi	mezi	k7c7	mezi
městy	město	k1gNnPc7	město
Jaroměřice	Jaroměřice	k1gFnPc1	Jaroměřice
nad	nad	k7c7	nad
Rokytnou	Rokytný	k2eAgFnSc7d1	Rokytná
<g/>
,	,	kIx,	,
Polná	Polný	k2eAgFnSc1d1	Polná
a	a	k8xC	a
Velká	velký	k2eAgFnSc1d1	velká
Bíteš	Bíteš	k1gFnSc1	Bíteš
<g/>
.	.	kIx.	.
</s>
<s>
Stáří	stáří	k1gNnSc1	stáří
hornin	hornina	k1gFnPc2	hornina
činí	činit	k5eAaImIp3nS	činit
podle	podle	k7c2	podle
izotopického	izotopický	k2eAgNnSc2d1	izotopické
datování	datování	k1gNnSc2	datování
přibližně	přibližně	k6eAd1	přibližně
340	[number]	k4	340
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Složením	složení	k1gNnSc7	složení
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
horniny	hornina	k1gFnPc1	hornina
třebíčského	třebíčský	k2eAgInSc2d1	třebíčský
masivu	masiv	k1gInSc2	masiv
nejčastěji	často	k6eAd3	často
žule	žula	k1gFnSc3	žula
a	a	k8xC	a
v	v	k7c6	v
případě	případ	k1gInSc6	případ
nižšího	nízký	k2eAgNnSc2d2	nižší
zastoupení	zastoupení	k1gNnSc2	zastoupení
křemene	křemen	k1gInSc2	křemen
i	i	k8xC	i
syenitu	syenit	k1gInSc2	syenit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hornině	hornina	k1gFnSc6	hornina
jsou	být	k5eAaImIp3nP	být
poměrně	poměrně	k6eAd1	poměrně
bohatě	bohatě	k6eAd1	bohatě
zastoupeny	zastoupit	k5eAaPmNgInP	zastoupit
tmavé	tmavý	k2eAgInPc1d1	tmavý
minerály	minerál	k1gInPc1	minerál
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
biotit	biotit	k1gInSc1	biotit
a	a	k8xC	a
amfibol	amfibol	k1gInSc1	amfibol
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
jí	jíst	k5eAaImIp3nS	jíst
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
velkými	velký	k2eAgNnPc7d1	velké
zrny	zrno	k1gNnPc7	zrno
živce	živec	k1gInSc2	živec
dávají	dávat	k5eAaImIp3nP	dávat
charakteristický	charakteristický	k2eAgInSc4d1	charakteristický
vzhled	vzhled	k1gInSc4	vzhled
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zajímavá	zajímavý	k2eAgFnSc1d1	zajímavá
z	z	k7c2	z
chemického	chemický	k2eAgNnSc2d1	chemické
hlediska	hledisko	k1gNnSc2	hledisko
<g/>
:	:	kIx,	:
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
totiž	totiž	k9	totiž
relativně	relativně	k6eAd1	relativně
vysoký	vysoký	k2eAgInSc4d1	vysoký
obsah	obsah	k1gInSc4	obsah
draslíku	draslík	k1gInSc2	draslík
kombinovaný	kombinovaný	k2eAgInSc4d1	kombinovaný
neobvykle	obvykle	k6eNd1	obvykle
se	s	k7c7	s
zvýšeným	zvýšený	k2eAgInSc7d1	zvýšený
obsahem	obsah	k1gInSc7	obsah
hořčíku	hořčík	k1gInSc2	hořčík
<g/>
.	.	kIx.	.
</s>
<s>
Horniny	hornina	k1gFnPc1	hornina
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
byly	být	k5eAaImAgInP	být
poprvé	poprvé	k6eAd1	poprvé
popsány	popsat	k5eAaPmNgInP	popsat
v	v	k7c6	v
německém	německý	k2eAgInSc6d1	německý
Schwarzwaldu	Schwarzwald	k1gInSc6	Schwarzwald
na	na	k7c6	na
lokalitě	lokalita	k1gFnSc6	lokalita
Durbach	Durbacha	k1gFnPc2	Durbacha
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
vděčí	vděčit	k5eAaImIp3nS	vděčit
hornina	hornina	k1gFnSc1	hornina
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
název	název	k1gInSc4	název
–	–	k?	–
durbachit	durbachit	k1gInSc1	durbachit
<g/>
.	.	kIx.	.
</s>
<s>
Třebíčský	třebíčský	k2eAgInSc1d1	třebíčský
masív	masív	k1gInSc1	masív
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
znám	znám	k2eAgInSc1d1	znám
především	především	k9	především
nálezy	nález	k1gInPc1	nález
ametystů	ametyst	k1gInPc2	ametyst
a	a	k8xC	a
záhněd	záhněda	k1gFnPc2	záhněda
u	u	k7c2	u
Hostákova	Hostákův	k2eAgNnSc2d1	Hostákův
a	a	k8xC	a
Bochovic	Bochovice	k1gFnPc2	Bochovice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
horninu	hornina	k1gFnSc4	hornina
jsou	být	k5eAaImIp3nP	být
dále	daleko	k6eAd2	daleko
charakteristické	charakteristický	k2eAgInPc1d1	charakteristický
zvýšené	zvýšený	k2eAgInPc1d1	zvýšený
obsahy	obsah	k1gInPc1	obsah
uranu	uran	k1gInSc2	uran
a	a	k8xC	a
thoria	thorium	k1gNnSc2	thorium
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
společně	společně	k6eAd1	společně
s	s	k7c7	s
radioaktivním	radioaktivní	k2eAgInSc7d1	radioaktivní
izotopem	izotop	k1gInSc7	izotop
draslíku	draslík	k1gInSc2	draslík
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
relativně	relativně	k6eAd1	relativně
vysokou	vysoký	k2eAgFnSc4d1	vysoká
radioaktivitu	radioaktivita	k1gFnSc4	radioaktivita
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
mimo	mimo	k6eAd1	mimo
jiné	jiný	k2eAgFnSc2d1	jiná
příčinou	příčina	k1gFnSc7	příčina
zčernání	zčernání	k1gNnSc2	zčernání
křemene	křemen	k1gInSc2	křemen
–	–	k?	–
hrádeckého	hrádecký	k2eAgInSc2d1	hrádecký
morionu	morion	k1gInSc2	morion
<g/>
.	.	kIx.	.
</s>
<s>
Vlivem	vliv	k1gInSc7	vliv
radioaktivního	radioaktivní	k2eAgInSc2d1	radioaktivní
rozpadu	rozpad	k1gInSc2	rozpad
uranu	uran	k1gInSc2	uran
a	a	k8xC	a
thoria	thorium	k1gNnSc2	thorium
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
radioaktivního	radioaktivní	k2eAgInSc2d1	radioaktivní
plynu	plyn	k1gInSc2	plyn
radonu	radon	k1gInSc2	radon
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
přítomnost	přítomnost	k1gFnSc1	přítomnost
v	v	k7c6	v
podložních	podložní	k2eAgFnPc6d1	podložní
horninách	hornina	k1gFnPc6	hornina
s	s	k7c7	s
sebou	se	k3xPyFc7	se
přináší	přinášet	k5eAaImIp3nS	přinášet
nutnost	nutnost	k1gFnSc4	nutnost
uplatnění	uplatnění	k1gNnSc2	uplatnění
opatření	opatření	k1gNnSc2	opatření
proti	proti	k7c3	proti
jeho	jeho	k3xOp3gNnSc3	jeho
pronikání	pronikání	k1gNnSc1	pronikání
do	do	k7c2	do
budov	budova	k1gFnPc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
uvolňování	uvolňování	k1gNnSc3	uvolňování
radonu	radon	k1gInSc2	radon
dochází	docházet	k5eAaImIp3nS	docházet
především	především	k9	především
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
překážkou	překážka	k1gFnSc7	překážka
při	při	k7c6	při
rozšiřování	rozšiřování	k1gNnSc3	rozšiřování
města	město	k1gNnSc2	město
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
sever	sever	k1gInSc4	sever
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Durbachity	Durbachita	k1gFnPc1	Durbachita
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
na	na	k7c6	na
území	území	k1gNnSc6	území
města	město	k1gNnSc2	město
velmi	velmi	k6eAd1	velmi
rozsáhlé	rozsáhlý	k2eAgInPc4d1	rozsáhlý
výchozy	výchoz	k1gInPc4	výchoz
táhnoucí	táhnoucí	k2eAgInPc4d1	táhnoucí
se	se	k3xPyFc4	se
po	po	k7c6	po
severním	severní	k2eAgInSc6d1	severní
obvodu	obvod	k1gInSc6	obvod
Židovské	židovský	k2eAgFnSc2d1	židovská
čtvrti	čtvrt	k1gFnSc2	čtvrt
podél	podél	k7c2	podél
řeky	řeka	k1gFnSc2	řeka
až	až	k9	až
do	do	k7c2	do
Kočičiny	kočičina	k1gFnSc2	kočičina
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
k	k	k7c3	k
východu	východ	k1gInSc3	východ
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
však	však	k9	však
odkryty	odkryt	k2eAgFnPc1d1	odkryta
i	i	k9	i
na	na	k7c6	na
celé	celý	k2eAgFnSc6d1	celá
řadě	řada	k1gFnSc6	řada
jiných	jiný	k2eAgNnPc2d1	jiné
míst	místo	k1gNnPc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
hojně	hojně	k6eAd1	hojně
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
horniny	hornina	k1gFnPc1	hornina
žilného	žilný	k2eAgInSc2d1	žilný
doprovodu	doprovod	k1gInSc2	doprovod
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
světlé	světlý	k2eAgFnPc4d1	světlá
aplitové	aplitový	k2eAgFnPc4d1	aplitová
žíly	žíla	k1gFnPc4	žíla
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc4	jejichž
mocnost	mocnost	k1gFnSc1	mocnost
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
maximálně	maximálně	k6eAd1	maximálně
několika	několik	k4yIc2	několik
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Nejnápadnější	nápadní	k2eAgInSc1d3	nápadní
výchoz	výchoz	k1gInSc1	výchoz
těchto	tento	k3xDgFnPc2	tento
hornin	hornina	k1gFnPc2	hornina
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
svahu	svah	k1gInSc6	svah
na	na	k7c6	na
Hrádku	Hrádok	k1gInSc6	Hrádok
pod	pod	k7c7	pod
Žižkovou	Žižkův	k2eAgFnSc7d1	Žižkova
mohylou	mohyla	k1gFnSc7	mohyla
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
žíly	žíla	k1gFnPc4	žíla
díky	díky	k7c3	díky
světlé	světlý	k2eAgFnSc3d1	světlá
barvě	barva	k1gFnSc3	barva
patrné	patrný	k2eAgFnSc2d1	patrná
na	na	k7c4	na
velkou	velký	k2eAgFnSc4d1	velká
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejzápadnější	západní	k2eAgFnSc1d3	nejzápadnější
část	část	k1gFnSc1	část
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
do	do	k7c2	do
níž	jenž	k3xRgFnSc2	jenž
spadá	spadat	k5eAaImIp3nS	spadat
především	především	k9	především
místní	místní	k2eAgFnSc1d1	místní
část	část	k1gFnSc1	část
Borovina	borovina	k1gFnSc1	borovina
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
leží	ležet	k5eAaImIp3nS	ležet
mimo	mimo	k7c4	mimo
třebíčský	třebíčský	k2eAgInSc4d1	třebíčský
masiv	masiv	k1gInSc4	masiv
a	a	k8xC	a
podloží	podloží	k1gNnSc4	podloží
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
tvořeno	tvořit	k5eAaImNgNnS	tvořit
migmatitzovanými	migmatitzovaný	k2eAgInPc7d1	migmatitzovaný
(	(	kIx(	(
<g/>
natavenými	natavený	k2eAgInPc7d1	natavený
<g/>
)	)	kIx)	)
sillimanit	sillimanit	k1gInSc1	sillimanit
biotitickými	biotitický	k2eAgFnPc7d1	biotitická
pararulami	pararula	k1gFnPc7	pararula
obsahujícími	obsahující	k2eAgFnPc7d1	obsahující
místy	místy	k6eAd1	místy
cordierit	cordierit	k1gInSc4	cordierit
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
těmito	tento	k3xDgFnPc7	tento
horninami	hornina	k1gFnPc7	hornina
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
se	se	k3xPyFc4	se
setkat	setkat	k5eAaPmF	setkat
např.	např.	kA	např.
ve	v	k7c6	v
výchozech	výchoz	k1gInPc6	výchoz
mezi	mezi	k7c7	mezi
Polankou	polanka	k1gFnSc7	polanka
a	a	k8xC	a
Poušovem	Poušov	k1gInSc7	Poušov
<g/>
.	.	kIx.	.
</s>
<s>
Bývají	bývat	k5eAaImIp3nP	bývat
zpravidla	zpravidla	k6eAd1	zpravidla
páskované	páskovaný	k2eAgFnPc1d1	páskovaná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tato	tento	k3xDgFnSc1	tento
stavba	stavba	k1gFnSc1	stavba
je	být	k5eAaImIp3nS	být
místy	místy	k6eAd1	místy
setřena	setřen	k2eAgFnSc1d1	setřena
díky	díky	k7c3	díky
jejich	jejich	k3xOp3gNnSc3	jejich
natavení	natavení	k1gNnSc3	natavení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
horninách	hornina	k1gFnPc6	hornina
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
vložky	vložka	k1gFnPc1	vložka
vápenato-silikátových	vápenatoilikátův	k2eAgFnPc2d1	vápenato-silikátův
hornin	hornina	k1gFnPc2	hornina
erlanů	erlan	k1gMnPc2	erlan
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
horniny	hornina	k1gFnPc1	hornina
podléhaly	podléhat	k5eAaImAgFnP	podléhat
v	v	k7c6	v
třetihorním	třetihorní	k2eAgNnSc6d1	třetihorní
tropickém	tropický	k2eAgNnSc6d1	tropické
klimatu	klima	k1gNnSc6	klima
intenzivnímu	intenzivní	k2eAgNnSc3d1	intenzivní
chemickému	chemický	k2eAgNnSc3d1	chemické
zvětrávání	zvětrávání	k1gNnSc3	zvětrávání
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
dalo	dát	k5eAaPmAgNnS	dát
vzniknout	vzniknout	k5eAaPmF	vzniknout
známým	známý	k2eAgInPc3d1	známý
pestrobarevným	pestrobarevný	k2eAgInPc3d1	pestrobarevný
borovinským	borovinský	k2eAgInPc3d1	borovinský
a	a	k8xC	a
řípovským	řípovský	k2eAgInPc3d1	řípovský
opálům	opál	k1gInPc3	opál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
stavebních	stavební	k2eAgFnPc2d1	stavební
úprav	úprava	k1gFnPc2	úprava
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
byly	být	k5eAaImAgInP	být
znovu	znovu	k6eAd1	znovu
odkryty	odkryt	k2eAgInPc1d1	odkryt
opály	opál	k1gInPc1	opál
různých	různý	k2eAgFnPc2d1	různá
barev	barva	k1gFnPc2	barva
<g/>
,	,	kIx,	,
nejvíce	hodně	k6eAd3	hodně
zelených	zelená	k1gFnPc2	zelená
<g/>
,	,	kIx,	,
hnědých	hnědý	k2eAgFnPc2d1	hnědá
a	a	k8xC	a
červených	červený	k2eAgFnPc2d1	červená
<g/>
,	,	kIx,	,
nejméně	málo	k6eAd3	málo
pak	pak	k6eAd1	pak
bílých	bílý	k2eAgFnPc6d1	bílá
a	a	k8xC	a
žlutých	žlutý	k2eAgFnPc6d1	žlutá
<g/>
.	.	kIx.	.
</s>
<s>
Opály	opál	k1gInPc1	opál
se	se	k3xPyFc4	se
již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
objevovaly	objevovat	k5eAaImAgInP	objevovat
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
bývalé	bývalý	k2eAgFnSc2d1	bývalá
borovinské	borovinský	k2eAgFnSc2d1	borovinský
cihelny	cihelna	k1gFnSc2	cihelna
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
později	pozdě	k6eAd2	pozdě
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
a	a	k8xC	a
k	k	k7c3	k
nálezům	nález	k1gInPc3	nález
docházelo	docházet	k5eAaImAgNnS	docházet
pouze	pouze	k6eAd1	pouze
sporadicky	sporadicky	k6eAd1	sporadicky
<g/>
.	.	kIx.	.
<g/>
Zajímavé	zajímavý	k2eAgInPc1d1	zajímavý
nerosty	nerost	k1gInPc1	nerost
se	se	k3xPyFc4	se
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
nalézají	nalézat	k5eAaImIp3nP	nalézat
již	již	k6eAd1	již
jen	jen	k9	jen
příležitostně	příležitostně	k6eAd1	příležitostně
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
při	při	k7c6	při
stavebních	stavební	k2eAgFnPc6d1	stavební
pracích	práce	k1gFnPc6	práce
a	a	k8xC	a
při	při	k7c6	při
výkopech	výkop	k1gInPc6	výkop
<g/>
.	.	kIx.	.
<g/>
Při	při	k7c6	při
výkopech	výkop	k1gInPc6	výkop
na	na	k7c6	na
Karlově	Karlův	k2eAgNnSc6d1	Karlovo
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
kdysi	kdysi	k6eAd1	kdysi
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
protékala	protékat	k5eAaImAgFnS	protékat
řeka	řeka	k1gFnSc1	řeka
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
setkat	setkat	k5eAaPmF	setkat
s	s	k7c7	s
valounky	valounek	k1gInPc7	valounek
křišťálu	křišťál	k1gInSc2	křišťál
a	a	k8xC	a
záhnědy	záhněda	k1gFnSc2	záhněda
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
ně	on	k3xPp3gNnSc4	on
však	však	k9	však
i	i	k9	i
s	s	k7c7	s
vivianitem	vivianit	k1gInSc7	vivianit
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
vznik	vznik	k1gInSc1	vznik
v	v	k7c6	v
naší	náš	k3xOp1gFnSc6	náš
lokalitě	lokalita	k1gFnSc6	lokalita
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
hromaděním	hromadění	k1gNnSc7	hromadění
organických	organický	k2eAgFnPc2d1	organická
látek	látka	k1gFnPc2	látka
ve	v	k7c6	v
středověkých	středověký	k2eAgFnPc6d1	středověká
stokách	stoka	k1gFnPc6	stoka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Stařečce	Stařečka	k1gFnSc6	Stařečka
byl	být	k5eAaImAgInS	být
při	při	k7c6	při
výstavbě	výstavba	k1gFnSc6	výstavba
autobusového	autobusový	k2eAgNnSc2d1	autobusové
nádraží	nádraží	k1gNnSc2	nádraží
nalezen	nalézt	k5eAaBmNgInS	nalézt
allanit	allanit	k1gInSc1	allanit
<g/>
.	.	kIx.	.
</s>
<s>
Výskyt	výskyt	k1gInSc1	výskyt
vzácného	vzácný	k2eAgInSc2d1	vzácný
nerostu	nerost	k1gInSc2	nerost
prioritu	priorita	k1gFnSc4	priorita
je	být	k5eAaImIp3nS	být
doložen	doložen	k2eAgInSc1d1	doložen
z	z	k7c2	z
Týna	Týn	k1gInSc2	Týn
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
žulosyenitovém	žulosyenitový	k2eAgInSc6d1	žulosyenitový
Hrádku	Hrádok	k1gInSc6	Hrádok
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
setkat	setkat	k5eAaPmF	setkat
s	s	k7c7	s
bílou	bílý	k2eAgFnSc7d1	bílá
horninou	hornina	k1gFnSc7	hornina
-	-	kIx~	-
aplitem	aplit	k1gInSc7	aplit
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
lze	lze	k6eAd1	lze
připomenout	připomenout	k5eAaPmF	připomenout
i	i	k9	i
turmalín	turmalín	k1gInSc4	turmalín
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
tzv.	tzv.	kA	tzv.
turmalínových	turmalínový	k2eAgNnPc2d1	turmalínový
sluncí	slunce	k1gNnPc2	slunce
<g/>
:	:	kIx,	:
křementurmalínové	křementurmalínový	k2eAgInPc4d1	křementurmalínový
srůsty	srůst	k1gInPc4	srůst
lemované	lemovaný	k2eAgInPc4d1	lemovaný
bílým	bílý	k2eAgInSc7d1	bílý
bezslídným	bezslídný	k2eAgInSc7d1	bezslídný
aplitem	aplit	k1gInSc7	aplit
a	a	k8xC	a
v	v	k7c6	v
tmavším	tmavý	k2eAgInSc6d2	tmavší
biotitovém	biotitový	k2eAgInSc6d1	biotitový
aplitu	aplit	k1gInSc6	aplit
<g/>
.	.	kIx.	.
<g/>
Třebíč	Třebíč	k1gFnSc1	Třebíč
je	být	k5eAaImIp3nS	být
též	též	k9	též
známá	známý	k2eAgFnSc1d1	známá
velkým	velký	k2eAgNnSc7d1	velké
nalezištěm	naleziště	k1gNnSc7	naleziště
vltavínů	vltavín	k1gInPc2	vltavín
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
města	město	k1gNnSc2	město
ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
Horka-Domky	Horka-Domka	k1gFnSc2	Horka-Domka
poblíž	poblíž	k7c2	poblíž
polikliniky	poliklinika	k1gFnSc2	poliklinika
ve	v	k7c6	v
Vltavínské	Vltavínský	k2eAgFnSc6d1	Vltavínská
ulici	ulice	k1gFnSc6	ulice
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
nich	on	k3xPp3gFnPc2	on
nazvána	nazván	k2eAgFnSc1d1	nazvána
<g/>
.	.	kIx.	.
</s>
<s>
Krásné	krásný	k2eAgInPc1d1	krásný
nerosty	nerost	k1gInPc1	nerost
byly	být	k5eAaImAgInP	být
nalezeny	naleznout	k5eAaPmNgInP	naleznout
též	též	k9	též
u	u	k7c2	u
tzv.	tzv.	kA	tzv.
Kostelíčka	kostelíček	k1gInSc2	kostelíček
<g/>
:	:	kIx,	:
černý	černý	k2eAgInSc1d1	černý
turmalín	turmalín	k1gInSc1	turmalín
<g/>
,	,	kIx,	,
hnědá	hnědý	k2eAgFnSc1d1	hnědá
záhněda	záhněda	k1gFnSc1	záhněda
<g/>
,	,	kIx,	,
šedozelený	šedozelený	k2eAgInSc1d1	šedozelený
chlorit	chlorit	k1gInSc1	chlorit
a	a	k8xC	a
zrna	zrno	k1gNnPc1	zrno
a	a	k8xC	a
osmistěnové	osmistěnový	k2eAgInPc1d1	osmistěnový
krystaly	krystal	k1gInPc1	krystal
fialového	fialový	k2eAgMnSc2d1	fialový
a	a	k8xC	a
zeleného	zelený	k2eAgInSc2d1	zelený
fluoritu	fluorit	k1gInSc2	fluorit
nebo	nebo	k8xC	nebo
zrna	zrno	k1gNnPc4	zrno
fialově	fialově	k6eAd1	fialově
červeného	červený	k2eAgInSc2d1	červený
granátu	granát	k1gInSc2	granát
–	–	k?	–
almandinu	almandin	k1gInSc2	almandin
<g/>
.	.	kIx.	.
</s>
<s>
Bílý	bílý	k2eAgInSc1d1	bílý
křemen	křemen	k1gInSc1	křemen
býval	bývat	k5eAaImAgInS	bývat
hojný	hojný	k2eAgMnSc1d1	hojný
na	na	k7c6	na
Jejkově	Jejkův	k2eAgMnSc6d1	Jejkův
a	a	k8xC	a
na	na	k7c6	na
starém	starý	k2eAgInSc6d1	starý
hřbitově	hřbitov	k1gInSc6	hřbitov
v	v	k7c6	v
Domcích	domek	k1gInPc6	domek
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
západě	západ	k1gInSc6	západ
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
dříve	dříve	k6eAd2	dříve
také	také	k9	také
těžila	těžit	k5eAaImAgFnS	těžit
cihlářská	cihlářský	k2eAgFnSc1d1	Cihlářská
hlína	hlína	k1gFnSc1	hlína
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Členění	členění	k1gNnSc1	členění
a	a	k8xC	a
samospráva	samospráva	k1gFnSc1	samospráva
==	==	k?	==
</s>
</p>
<p>
<s>
Město	město	k1gNnSc1	město
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
56	[number]	k4	56
km	km	kA	km
<g/>
2	[number]	k4	2
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
10	[number]	k4	10
katastrálních	katastrální	k2eAgNnPc2d1	katastrální
území	území	k1gNnPc2	území
a	a	k8xC	a
17	[number]	k4	17
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
10	[number]	k4	10
tvoří	tvořit	k5eAaImIp3nP	tvořit
městskou	městský	k2eAgFnSc4d1	městská
zástavbu	zástavba	k1gFnSc4	zástavba
a	a	k8xC	a
zbylých	zbylý	k2eAgNnPc2d1	zbylé
7	[number]	k4	7
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
blízkém	blízký	k2eAgNnSc6d1	blízké
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
žilo	žít	k5eAaImAgNnS	žít
39	[number]	k4	39
021	[number]	k4	021
obyvatel	obyvatel	k1gMnPc2	obyvatel
v	v	k7c6	v
4	[number]	k4	4
895	[number]	k4	895
domech	dům	k1gInPc6	dům
a	a	k8xC	a
podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
jen	jen	k9	jen
36	[number]	k4	36
998	[number]	k4	998
obyvatel	obyvatel	k1gMnPc2	obyvatel
v	v	k7c6	v
5	[number]	k4	5
304	[number]	k4	304
domech	dům	k1gInPc6	dům
<g/>
.	.	kIx.	.
</s>
<s>
Nejrozsáhlejší	rozsáhlý	k2eAgFnSc7d3	nejrozsáhlejší
místní	místní	k2eAgFnSc7d1	místní
částí	část	k1gFnSc7	část
jsou	být	k5eAaImIp3nP	být
Slavice	slavice	k1gFnPc1	slavice
<g/>
;	;	kIx,	;
nejmenší	malý	k2eAgFnSc1d3	nejmenší
Zámostí	Zámost	k1gFnSc7	Zámost
<g/>
.	.	kIx.	.
</s>
<s>
Nejlidnatější	lidnatý	k2eAgInPc1d3	nejlidnatější
jsou	být	k5eAaImIp3nP	být
Nové	Nové	k2eAgInPc1d1	Nové
Dvory	Dvůr	k1gInPc1	Dvůr
a	a	k8xC	a
nejméně	málo	k6eAd3	málo
obyvatel	obyvatel	k1gMnPc2	obyvatel
má	mít	k5eAaImIp3nS	mít
Řípov	Řípov	k1gInSc1	Řípov
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
po	po	k7c6	po
revoluci	revoluce	k1gFnSc6	revoluce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1848	[number]	k4	1848
a	a	k8xC	a
podle	podle	k7c2	podle
březnové	březnový	k2eAgFnSc2d1	březnová
ústavy	ústava	k1gFnSc2	ústava
přestal	přestat	k5eAaPmAgInS	přestat
fungovat	fungovat	k5eAaImF	fungovat
třebíčský	třebíčský	k2eAgInSc1d1	třebíčský
magistrát	magistrát	k1gInSc1	magistrát
<g/>
,	,	kIx,	,
úředníci	úředník	k1gMnPc1	úředník
přešli	přejít	k5eAaPmAgMnP	přejít
do	do	k7c2	do
služeb	služba	k1gFnPc2	služba
státu	stát	k1gInSc2	stát
a	a	k8xC	a
prvním	první	k4xOgInSc7	první
starostou	starosta	k1gMnSc7	starosta
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
Martin	Martin	k1gMnSc1	Martin
Hassek	Hassek	k1gMnSc1	Hassek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
se	se	k3xPyFc4	se
několik	několik	k4yIc1	několik
bývalých	bývalý	k2eAgFnPc2d1	bývalá
místních	místní	k2eAgFnPc2d1	místní
částí	část	k1gFnPc2	část
osamostatnilo	osamostatnit	k5eAaPmAgNnS	osamostatnit
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
to	ten	k3xDgNnSc1	ten
byly	být	k5eAaImAgInP	být
Krahulov	Krahulovo	k1gNnPc2	Krahulovo
(	(	kIx(	(
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
<g/>
,	,	kIx,	,
s	s	k7c7	s
platností	platnost	k1gFnSc7	platnost
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
Kožichovice	Kožichovice	k1gFnSc1	Kožichovice
a	a	k8xC	a
Okřešice	Okřešice	k1gFnSc1	Okřešice
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc7d1	poslední
odpojenou	odpojený	k2eAgFnSc7d1	odpojená
částí	část	k1gFnSc7	část
byla	být	k5eAaImAgFnS	být
Střítež	Střítež	k1gFnSc1	Střítež
<g/>
,	,	kIx,	,
samostatná	samostatný	k2eAgFnSc1d1	samostatná
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
<g/>
Městský	městský	k2eAgInSc1d1	městský
úřad	úřad	k1gInSc1	úřad
města	město	k1gNnSc2	město
Třebíče	Třebíč	k1gFnSc2	Třebíč
tvoří	tvořit	k5eAaImIp3nS	tvořit
starosta	starosta	k1gMnSc1	starosta
<g/>
,	,	kIx,	,
místostarostové	místostarosta	k1gMnPc1	místostarosta
<g/>
,	,	kIx,	,
tajemník	tajemník	k1gMnSc1	tajemník
úřadu	úřad	k1gInSc2	úřad
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
zaměstnanci	zaměstnanec	k1gMnPc1	zaměstnanec
<g/>
.	.	kIx.	.
</s>
<s>
Starostou	Starosta	k1gMnSc7	Starosta
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
do	do	k7c2	do
20	[number]	k4	20
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2014	[number]	k4	2014
Pavel	Pavel	k1gMnSc1	Pavel
Heřman	Heřman	k1gMnSc1	Heřman
<g/>
,	,	kIx,	,
od	od	k7c2	od
podzimu	podzim	k1gInSc2	podzim
2014	[number]	k4	2014
do	do	k7c2	do
20	[number]	k4	20
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2018	[number]	k4	2018
jím	on	k3xPp3gInSc7	on
byl	být	k5eAaImAgMnS	být
Pavel	Pavel	k1gMnSc1	Pavel
Janata	Janata	k1gMnSc1	Janata
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
20	[number]	k4	20
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2018	[number]	k4	2018
zastává	zastávat	k5eAaImIp3nS	zastávat
funkci	funkce	k1gFnSc4	funkce
starosty	starosta	k1gMnSc2	starosta
Pavel	Pavel	k1gMnSc1	Pavel
Pacal	Pacal	k1gMnSc1	Pacal
<g/>
.	.	kIx.	.
</s>
<s>
Zastupitelstvo	zastupitelstvo	k1gNnSc1	zastupitelstvo
města	město	k1gNnSc2	město
tvoří	tvořit	k5eAaImIp3nS	tvořit
27	[number]	k4	27
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
působí	působit	k5eAaImIp3nS	působit
i	i	k9	i
městská	městský	k2eAgFnSc1d1	městská
rada	rada	k1gFnSc1	rada
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
bylo	být	k5eAaImAgNnS	být
vedení	vedení	k1gNnSc1	vedení
města	město	k1gNnSc2	město
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
národního	národní	k2eAgInSc2d1	národní
výboru	výbor	k1gInSc2	výbor
a	a	k8xC	a
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
města	město	k1gNnSc2	město
stál	stát	k5eAaImAgMnS	stát
"	"	kIx"	"
<g/>
předseda	předseda	k1gMnSc1	předseda
národního	národní	k2eAgInSc2d1	národní
výboru	výbor	k1gInSc2	výbor
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Třebíčskem	Třebíčsko	k1gNnSc7	Třebíčsko
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2007	[number]	k4	2007
a	a	k8xC	a
2008	[number]	k4	2008
otřásaly	otřásat	k5eAaImAgInP	otřásat
protesty	protest	k1gInPc1	protest
vůči	vůči	k7c3	vůči
vedení	vedení	k1gNnSc3	vedení
města	město	k1gNnSc2	město
kvůli	kvůli	k7c3	kvůli
tzv.	tzv.	kA	tzv.
kauze	kauza	k1gFnSc6	kauza
"	"	kIx"	"
<g/>
Vídeňský	vídeňský	k2eAgInSc1d1	vídeňský
rybník	rybník	k1gInSc1	rybník
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
prodej	prodej	k1gInSc4	prodej
pozemků	pozemek	k1gInPc2	pozemek
firmě	firma	k1gFnSc3	firma
Lima	Lima	k1gFnSc1	Lima
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
dostat	dostat	k5eAaPmF	dostat
pozemky	pozemek	k1gInPc4	pozemek
za	za	k7c4	za
nižší	nízký	k2eAgFnSc4d2	nižší
cenu	cena	k1gFnSc4	cena
než	než	k8xS	než
jiné	jiný	k2eAgFnPc4d1	jiná
firmy	firma	k1gFnPc4	firma
v	v	k7c6	v
konkurzu	konkurz	k1gInSc6	konkurz
<g/>
.	.	kIx.	.
</s>
<s>
Starosta	Starosta	k1gMnSc1	Starosta
nakonec	nakonec	k6eAd1	nakonec
odvolán	odvolán	k2eAgMnSc1d1	odvolán
nebyl	být	k5eNaImAgInS	být
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Územní	územní	k2eAgInSc4d1	územní
vývoj	vývoj	k1gInSc4	vývoj
===	===	k?	===
</s>
</p>
<p>
<s>
Město	město	k1gNnSc1	město
Třebíč	Třebíč	k1gFnSc1	Třebíč
se	se	k3xPyFc4	se
půdorysem	půdorys	k1gInSc7	půdorys
řadí	řadit	k5eAaImIp3nS	řadit
k	k	k7c3	k
podunajskému	podunajský	k2eAgInSc3d1	podunajský
typu	typ	k1gInSc2	typ
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
hlavní	hlavní	k2eAgNnSc1d1	hlavní
náměstí	náměstí	k1gNnSc1	náměstí
má	mít	k5eAaImIp3nS	mít
ulicový	ulicový	k2eAgInSc4d1	ulicový
charakter	charakter	k1gInSc4	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
jednostranným	jednostranný	k2eAgNnSc7d1	jednostranné
rozšířením	rozšíření	k1gNnSc7	rozšíření
staré	starý	k2eAgFnSc2d1	stará
komunikace	komunikace	k1gFnSc2	komunikace
a	a	k8xC	a
kopíruje	kopírovat	k5eAaImIp3nS	kopírovat
pravý	pravý	k2eAgInSc4d1	pravý
břeh	břeh	k1gInSc4	břeh
oblouku	oblouk	k1gInSc2	oblouk
řeky	řeka	k1gFnSc2	řeka
Jihlavy	Jihlava	k1gFnSc2	Jihlava
<g/>
.	.	kIx.	.
</s>
<s>
Vedlejší	vedlejší	k2eAgNnSc1d1	vedlejší
prostranství	prostranství	k1gNnSc1	prostranství
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
při	při	k7c6	při
farním	farní	k2eAgInSc6d1	farní
kostele	kostel	k1gInSc6	kostel
sv.	sv.	kA	sv.
Martina	Martin	k1gMnSc4	Martin
z	z	k7c2	z
Tours	Toursa	k1gFnPc2	Toursa
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
jádro	jádro	k1gNnSc1	jádro
města	město	k1gNnSc2	město
uzavřely	uzavřít	k5eAaPmAgFnP	uzavřít
hradby	hradba	k1gFnPc1	hradba
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
vcházelo	vcházet	k5eAaImAgNnS	vcházet
třemi	tři	k4xCgFnPc7	tři
branami	brána	k1gFnPc7	brána
<g/>
:	:	kIx,	:
jihlavskou	jihlavský	k2eAgFnSc4d1	Jihlavská
(	(	kIx(	(
<g/>
podklášterskou	podklášterský	k2eAgFnSc4d1	podklášterský
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vídeňskou	vídeňský	k2eAgFnSc7d1	Vídeňská
(	(	kIx(	(
<g/>
znojemskou	znojemský	k2eAgFnSc7d1	Znojemská
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
jejkovskou	jejkovský	k2eAgFnSc7d1	jejkovský
(	(	kIx(	(
<g/>
brněnskou	brněnský	k2eAgFnSc7d1	brněnská
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jako	jako	k8xS	jako
předměstí	předměstí	k1gNnSc1	předměstí
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
v	v	k7c6	v
době	doba	k1gFnSc6	doba
renesance	renesance	k1gFnSc2	renesance
Stařečka	stařeček	k1gMnSc2	stařeček
za	za	k7c7	za
jihlavskou	jihlavský	k2eAgFnSc7d1	Jihlavská
branou	brána	k1gFnSc7	brána
a	a	k8xC	a
Jejkov	Jejkov	k1gInSc1	Jejkov
za	za	k7c7	za
brněnskou	brněnský	k2eAgFnSc7d1	brněnská
branou	brána	k1gFnSc7	brána
<g/>
.	.	kIx.	.
</s>
<s>
Románské	románský	k2eAgNnSc1d1	románské
podhradí	podhradí	k1gNnSc1	podhradí
kláštera	klášter	k1gInSc2	klášter
(	(	kIx(	(
<g/>
Podklášteří	podklášteří	k1gNnSc2	podklášteří
<g/>
)	)	kIx)	)
stavebně	stavebně	k6eAd1	stavebně
přešlo	přejít	k5eAaPmAgNnS	přejít
do	do	k7c2	do
sousedícího	sousedící	k2eAgNnSc2d1	sousedící
Židovského	židovský	k2eAgNnSc2d1	Židovské
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
obnově	obnova	k1gFnSc6	obnova
města	město	k1gNnSc2	město
v	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
město	město	k1gNnSc4	město
rozšiřovalo	rozšiřovat	k5eAaImAgNnS	rozšiřovat
ulice	ulice	k1gFnSc2	ulice
i	i	k9	i
náměstí	náměstí	k1gNnSc4	náměstí
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
posílilo	posílit	k5eAaPmAgNnS	posílit
jejich	jejich	k3xOp3gFnSc4	jejich
tržní	tržní	k2eAgFnSc4d1	tržní
funkci	funkce	k1gFnSc4	funkce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poté	poté	k6eAd1	poté
byly	být	k5eAaImAgInP	být
položeny	položen	k2eAgInPc1d1	položen
základy	základ	k1gInPc1	základ
pozdějších	pozdní	k2eAgFnPc2d2	pozdější
částí	část	k1gFnPc2	část
Nových	Nových	k2eAgInPc2d1	Nových
Dvorů	Dvůr	k1gInPc2	Dvůr
a	a	k8xC	a
Nového	Nového	k2eAgNnSc2d1	Nového
Města	město	k1gNnSc2	město
na	na	k7c6	na
levém	levý	k2eAgInSc6d1	levý
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
2	[number]	k4	2
<g/>
.	.	kIx.	.
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
začalo	začít	k5eAaPmAgNnS	začít
rozpínat	rozpínat	k5eAaImF	rozpínat
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
jih	jih	k1gInSc4	jih
od	od	k7c2	od
Horky	horka	k1gFnSc2	horka
do	do	k7c2	do
nové	nový	k2eAgFnSc2d1	nová
části	část	k1gFnSc2	část
Domky	domek	k1gInPc1	domek
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
přes	přes	k7c4	přes
Stařečku	stařeček	k1gMnSc3	stařeček
do	do	k7c2	do
Boroviny	borovina	k1gFnSc2	borovina
<g/>
,	,	kIx,	,
rychlým	rychlý	k2eAgNnSc7d1	rychlé
tempem	tempo	k1gNnSc7	tempo
pak	pak	k6eAd1	pak
rostlo	růst	k5eAaImAgNnS	růst
začátkem	začátkem	k7c2	začátkem
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
dál	daleko	k6eAd2	daleko
k	k	k7c3	k
severu	sever	k1gInSc2	sever
rozšířily	rozšířit	k5eAaPmAgInP	rozšířit
Nové	Nové	k2eAgInPc1d1	Nové
Dvory	Dvůr	k1gInPc1	Dvůr
<g/>
,	,	kIx,	,
na	na	k7c4	na
jih	jih	k1gInSc4	jih
Domky	domek	k1gInPc4	domek
a	a	k8xC	a
při	při	k7c6	při
borovinské	borovinský	k2eAgFnSc6d1	borovinský
továrně	továrna	k1gFnSc6	továrna
sestavěly	sestavět	k5eAaPmAgInP	sestavět
dělnické	dělnický	k2eAgInPc1d1	dělnický
domky	domek	k1gInPc1	domek
<g/>
.	.	kIx.	.
</s>
<s>
Pozdější	pozdní	k2eAgFnSc1d2	pozdější
víceméně	víceméně	k9	víceméně
pozvolný	pozvolný	k2eAgInSc4d1	pozvolný
růst	růst	k1gInSc4	růst
města	město	k1gNnSc2	město
na	na	k7c4	na
všechny	všechen	k3xTgFnPc4	všechen
strany	strana	k1gFnPc4	strana
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
výstavby	výstavba	k1gFnSc2	výstavba
mnoha	mnoho	k4c2	mnoho
cihlových	cihlový	k2eAgInPc2d1	cihlový
činžovních	činžovní	k2eAgInPc2d1	činžovní
domů	dům	k1gInPc2	dům
v	v	k7c6	v
Domcích	domek	k1gInPc6	domek
a	a	k8xC	a
Borovině	borovina	k1gFnSc6	borovina
a	a	k8xC	a
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
zástavby	zástavba	k1gFnSc2	zástavba
v	v	k7c6	v
Domcích	domek	k1gInPc6	domek
a	a	k8xC	a
na	na	k7c6	na
Jejkově	Jejkův	k2eAgFnSc6d1	Jejkův
<g/>
,	,	kIx,	,
nabral	nabrat	k5eAaPmAgInS	nabrat
překotné	překotný	k2eAgNnSc4d1	překotné
tempo	tempo	k1gNnSc4	tempo
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
se	s	k7c7	s
záměrem	záměr	k1gInSc7	záměr
a	a	k8xC	a
začátkem	začátek	k1gInSc7	začátek
realizace	realizace	k1gFnSc2	realizace
výstavby	výstavba	k1gFnSc2	výstavba
dukovanské	dukovanský	k2eAgFnSc2d1	Dukovanská
elektrárny	elektrárna	k1gFnSc2	elektrárna
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
tak	tak	k6eAd1	tak
byla	být	k5eAaImAgNnP	být
po	po	k7c6	po
okrajích	okraj	k1gInPc6	okraj
města	město	k1gNnSc2	město
zakládána	zakládat	k5eAaImNgNnP	zakládat
sídliště	sídliště	k1gNnPc1	sídliště
s	s	k7c7	s
panelovými	panelový	k2eAgInPc7d1	panelový
domy	dům	k1gInPc7	dům
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInPc4d3	veliký
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
vyrostlo	vyrůst	k5eAaPmAgNnS	vyrůst
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
70	[number]	k4	70
<g/>
.	.	kIx.	.
a	a	k8xC	a
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
na	na	k7c6	na
Nových	Nových	k2eAgInPc6d1	Nových
Dvorech	Dvůr	k1gInPc6	Dvůr
a	a	k8xC	a
v	v	k7c6	v
Borovině	borovina	k1gFnSc6	borovina
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
panelová	panelový	k2eAgFnSc1d1	panelová
výstavba	výstavba	k1gFnSc1	výstavba
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
v	v	k7c6	v
mezitím	mezitím	k6eAd1	mezitím
připojené	připojený	k2eAgFnSc6d1	připojená
části	část	k1gFnSc6	část
Týn	Týn	k1gInSc4	Týn
a	a	k8xC	a
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
80	[number]	k4	80
<g/>
.	.	kIx.	.
a	a	k8xC	a
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
též	též	k9	též
na	na	k7c6	na
Novém	nový	k2eAgNnSc6d1	nové
Městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Natolik	natolik	k6eAd1	natolik
zásadnímu	zásadní	k2eAgNnSc3d1	zásadní
zvýšení	zvýšení	k1gNnSc3	zvýšení
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
musela	muset	k5eAaImAgFnS	muset
přizpůsobit	přizpůsobit	k5eAaPmF	přizpůsobit
i	i	k9	i
dopravní	dopravní	k2eAgFnSc1d1	dopravní
infrastruktura	infrastruktura	k1gFnSc1	infrastruktura
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
mj.	mj.	kA	mj.
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
asanování	asanování	k1gNnSc3	asanování
takřka	takřka	k6eAd1	takřka
celé	celý	k2eAgFnSc2d1	celá
Horky	horka	k1gFnSc2	horka
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
staré	starý	k2eAgFnPc1d1	stará
zástavby	zástavba	k1gFnPc1	zástavba
Nového	Nového	k2eAgNnSc2d1	Nového
Města	město	k1gNnSc2	město
a	a	k8xC	a
podstatné	podstatný	k2eAgFnSc2d1	podstatná
části	část	k1gFnSc2	část
Stařečky	stařeček	k1gMnPc7	stařeček
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
o	o	k7c6	o
přípravě	příprava	k1gFnSc6	příprava
60	[number]	k4	60
parcel	parcela	k1gFnPc2	parcela
k	k	k7c3	k
bydlení	bydlení	k1gNnSc3	bydlení
na	na	k7c6	na
jížním	jížní	k2eAgInSc6d1	jížní
okraji	okraj	k1gInSc6	okraj
města	město	k1gNnSc2	město
nedaleko	nedaleko	k7c2	nedaleko
Polikliniky	poliklinika	k1gFnSc2	poliklinika
Vltavínská	Vltavínský	k2eAgFnSc1d1	Vltavínská
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
budou	být	k5eAaImBp3nP	být
stát	stát	k5eAaImF	stát
samostatné	samostatný	k2eAgInPc4d1	samostatný
rodinné	rodinný	k2eAgInPc4d1	rodinný
domy	dům	k1gInPc4	dům
a	a	k8xC	a
bytový	bytový	k2eAgInSc4d1	bytový
dům	dům	k1gInSc4	dům
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severním	severní	k2eAgInSc6d1	severní
okraji	okraj	k1gInSc6	okraj
města	město	k1gNnSc2	město
budou	být	k5eAaImBp3nP	být
postaveny	postaven	k2eAgInPc1d1	postaven
domy	dům	k1gInPc1	dům
za	za	k7c7	za
ulicí	ulice	k1gFnSc7	ulice
Manželů	manžel	k1gMnPc2	manžel
Curieových	Curieův	k2eAgMnPc2d1	Curieův
<g/>
,	,	kIx,	,
v	v	k7c6	v
lokalitě	lokalita	k1gFnSc6	lokalita
na	na	k7c6	na
Kopcích	kopec	k1gInPc6	kopec
budou	být	k5eAaImBp3nP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
dokončeny	dokončen	k2eAgFnPc1d1	dokončena
parcely	parcela	k1gFnPc1	parcela
pro	pro	k7c4	pro
řadové	řadový	k2eAgInPc4d1	řadový
domy	dům	k1gInPc4	dům
a	a	k8xC	a
dva	dva	k4xCgInPc4	dva
bytové	bytový	k2eAgInPc4d1	bytový
domy	dům	k1gInPc4	dům
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
město	město	k1gNnSc1	město
Třebíč	Třebíč	k1gFnSc1	Třebíč
vybralo	vybrat	k5eAaPmAgNnS	vybrat
po	po	k7c6	po
třech	tři	k4xCgNnPc6	tři
letech	léto	k1gNnPc6	léto
opět	opět	k6eAd1	opět
městského	městský	k2eAgMnSc2d1	městský
architekta	architekt	k1gMnSc2	architekt
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
jím	on	k3xPp3gInSc7	on
byl	být	k5eAaImAgMnS	být
Lubor	Lubor	k1gMnSc1	Lubor
Herzán	Herzán	k2eAgMnSc1d1	Herzán
<g/>
,	,	kIx,	,
od	od	k7c2	od
dubna	duben	k1gInSc2	duben
2017	[number]	k4	2017
je	být	k5eAaImIp3nS	být
novým	nový	k2eAgMnSc7d1	nový
architektem	architekt	k1gMnSc7	architekt
Jaroslav	Jaroslava	k1gFnPc2	Jaroslava
Hulín	Hulín	k1gInSc1	Hulín
<g/>
,	,	kIx,	,
třebíčský	třebíčský	k2eAgMnSc1d1	třebíčský
rodák	rodák	k1gMnSc1	rodák
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
žije	žít	k5eAaImIp3nS	žít
a	a	k8xC	a
pracuje	pracovat	k5eAaImIp3nS	pracovat
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
roku	rok	k1gInSc2	rok
2019	[number]	k4	2019
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
lokalitě	lokalita	k1gFnSc6	lokalita
Brněnská	brněnský	k2eAgFnSc1d1	brněnská
západ	západ	k1gInSc1	západ
připraveny	připraven	k2eAgInPc1d1	připraven
pozemky	pozemek	k1gInPc1	pozemek
s	s	k7c7	s
projekty	projekt	k1gInPc7	projekt
pro	pro	k7c4	pro
výstavbu	výstavba	k1gFnSc4	výstavba
rodinných	rodinný	k2eAgInPc2d1	rodinný
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
prodány	prodán	k2eAgInPc1d1	prodán
mají	mít	k5eAaImIp3nP	mít
být	být	k5eAaImF	být
v	v	k7c6	v
aukci	aukce	k1gFnSc6	aukce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Demografie	demografie	k1gFnSc2	demografie
===	===	k?	===
</s>
</p>
<p>
<s>
Třebíč	Třebíč	k1gFnSc1	Třebíč
má	mít	k5eAaImIp3nS	mít
17	[number]	k4	17
místních	místní	k2eAgFnPc2d1	místní
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
nejvíce	nejvíce	k6eAd1	nejvíce
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
16	[number]	k4	16
657	[number]	k4	657
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
části	část	k1gFnSc6	část
Nové	Nové	k2eAgInPc1d1	Nové
Dvory	Dvůr	k1gInPc1	Dvůr
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
údajů	údaj	k1gInPc2	údaj
českého	český	k2eAgInSc2d1	český
statistické	statistický	k2eAgFnSc3d1	statistická
úřadu	úřad	k1gInSc3	úřad
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
části	část	k1gFnSc6	část
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
pouze	pouze	k6eAd1	pouze
7	[number]	k4	7
929	[number]	k4	929
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c4	za
11	[number]	k4	11
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
zdvojnásobil	zdvojnásobit	k5eAaPmAgInS	zdvojnásobit
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
jev	jev	k1gInSc1	jev
se	se	k3xPyFc4	se
přičítá	přičítat	k5eAaImIp3nS	přičítat
stavbě	stavba	k1gFnSc3	stavba
Jaderné	jaderný	k2eAgFnSc2d1	jaderná
elektrárny	elektrárna	k1gFnSc2	elektrárna
Dukovany	Dukovany	k1gInPc1	Dukovany
<g/>
.	.	kIx.	.
</s>
<s>
Nejméně	málo	k6eAd3	málo
obyvatel	obyvatel	k1gMnPc2	obyvatel
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
Řípově	Řípův	k2eAgNnSc6d1	Řípův
<g/>
,	,	kIx,	,
pouhých	pouhý	k2eAgInPc2d1	pouhý
68	[number]	k4	68
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
pouhých	pouhý	k2eAgMnPc2d1	pouhý
88	[number]	k4	88
obyvatel	obyvatel	k1gMnPc2	obyvatel
žilo	žít	k5eAaImAgNnS	žít
i	i	k9	i
v	v	k7c6	v
místní	místní	k2eAgFnSc6d1	místní
části	část	k1gFnSc6	část
Sokolí	sokolí	k2eAgNnSc1d1	sokolí
<g/>
.	.	kIx.	.
<g/>
Mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1850	[number]	k4	1850
<g/>
–	–	k?	–
<g/>
1920	[number]	k4	1920
byl	být	k5eAaImAgInS	být
průměrný	průměrný	k2eAgInSc1d1	průměrný
přírůstek	přírůstek	k1gInSc1	přírůstek
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
1532	[number]	k4	1532
obyvatel	obyvatel	k1gMnPc2	obyvatel
za	za	k7c4	za
10	[number]	k4	10
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1930	[number]	k4	1930
je	být	k5eAaImIp3nS	být
patrná	patrný	k2eAgFnSc1d1	patrná
míra	míra	k1gFnSc1	míra
populační	populační	k2eAgFnSc2d1	populační
expanze	expanze	k1gFnSc2	expanze
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1970	[number]	k4	1970
a	a	k8xC	a
1991	[number]	k4	1991
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
velice	velice	k6eAd1	velice
patrný	patrný	k2eAgInSc1d1	patrný
nárůst	nárůst	k1gInSc1	nárůst
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
kvůli	kvůli	k7c3	kvůli
výstavbě	výstavba	k1gFnSc3	výstavba
Jaderné	jaderný	k2eAgFnSc2d1	jaderná
elektrárny	elektrárna	k1gFnSc2	elektrárna
Dukovany	Dukovany	k1gInPc1	Dukovany
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
narostl	narůst	k5eAaPmAgInS	narůst
počet	počet	k1gInSc1	počet
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
o	o	k7c4	o
15	[number]	k4	15
800	[number]	k4	800
obyvatel	obyvatel	k1gMnPc2	obyvatel
za	za	k7c4	za
21	[number]	k4	21
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
nárůst	nárůst	k1gInSc1	nárůst
je	být	k5eAaImIp3nS	být
také	také	k6eAd1	také
spojen	spojit	k5eAaPmNgInS	spojit
s	s	k7c7	s
výstavbou	výstavba	k1gFnSc7	výstavba
velkých	velký	k2eAgNnPc2d1	velké
sídlišť	sídliště	k1gNnPc2	sídliště
na	na	k7c6	na
severu	sever	k1gInSc6	sever
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
přirozený	přirozený	k2eAgInSc1d1	přirozený
přírůstek	přírůstek	k1gInSc1	přírůstek
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
byl	být	k5eAaImAgInS	být
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1981	[number]	k4	1981
a	a	k8xC	a
1982	[number]	k4	1982
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
projevují	projevovat	k5eAaImIp3nP	projevovat
tzv.	tzv.	kA	tzv.
silné	silný	k2eAgInPc1d1	silný
ročníky	ročník	k1gInPc1	ročník
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
bylo	být	k5eAaImAgNnS	být
zaznamenané	zaznamenaný	k2eAgNnSc1d1	zaznamenané
největší	veliký	k2eAgNnSc1d3	veliký
snížení	snížení	k1gNnSc1	snížení
přirozeného	přirozený	k2eAgInSc2d1	přirozený
přírůstku	přírůstek	k1gInSc2	přírůstek
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
další	další	k2eAgInPc1d1	další
roky	rok	k1gInPc1	rok
se	se	k3xPyFc4	se
přirozený	přirozený	k2eAgInSc1d1	přirozený
přírůstek	přírůstek	k1gInSc1	přírůstek
snižuje	snižovat	k5eAaImIp3nS	snižovat
pouze	pouze	k6eAd1	pouze
pozvolně	pozvolně	k6eAd1	pozvolně
<g/>
.	.	kIx.	.
<g/>
Město	město	k1gNnSc1	město
Třebíč	Třebíč	k1gFnSc1	Třebíč
je	být	k5eAaImIp3nS	být
známé	známý	k2eAgNnSc1d1	známé
svým	svůj	k3xOyFgNnSc7	svůj
Židovským	židovský	k2eAgNnSc7d1	Židovské
městem	město	k1gNnSc7	město
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
Fišera	Fišer	k1gMnSc2	Fišer
se	se	k3xPyFc4	se
cizí	cizit	k5eAaImIp3nP	cizit
národní	národní	k2eAgMnPc1d1	národní
příslušníci	příslušník	k1gMnPc1	příslušník
začali	začít	k5eAaPmAgMnP	začít
do	do	k7c2	do
města	město	k1gNnSc2	město
Třebíče	Třebíč	k1gFnSc2	Třebíč
stěhovat	stěhovat	k5eAaImF	stěhovat
již	již	k6eAd1	již
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Pernštějnů	Pernštějn	k1gInPc2	Pernštějn
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
však	však	k9	však
až	až	k9	až
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Smila	Smil	k1gMnSc2	Smil
Osovského	Osovský	k2eAgMnSc2d1	Osovský
z	z	k7c2	z
Doubravice	Doubravice	k1gFnSc2	Doubravice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
v	v	k7c6	v
purkrechtních	purkrechtní	k2eAgFnPc6d1	purkrechtní
knihách	kniha	k1gFnPc6	kniha
města	město	k1gNnSc2	město
objevují	objevovat	k5eAaImIp3nP	objevovat
záznamy	záznam	k1gInPc1	záznam
o	o	k7c4	o
koupi	koupě	k1gFnSc4	koupě
12	[number]	k4	12
domů	dům	k1gInPc2	dům
ve	v	k7c6	v
Vnitřním	vnitřní	k2eAgNnSc6d1	vnitřní
městě	město	k1gNnSc6	město
a	a	k8xC	a
o	o	k7c6	o
koupi	koupě	k1gFnSc6	koupě
10	[number]	k4	10
domů	dům	k1gInPc2	dům
na	na	k7c6	na
Stařečce	Stařečka	k1gFnSc6	Stařečka
Němci	Němec	k1gMnPc1	Němec
<g/>
.	.	kIx.	.
</s>
<s>
Malovaný	malovaný	k2eAgInSc1d1	malovaný
dům	dům	k1gInSc1	dům
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
zvaný	zvaný	k2eAgInSc1d1	zvaný
Františkovský	Františkovský	k2eAgInSc1d1	Františkovský
<g/>
,	,	kIx,	,
patřil	patřit	k5eAaImAgMnS	patřit
nejzámožnějšímu	zámožný	k2eAgMnSc3d3	nejzámožnější
italskému	italský	k2eAgMnSc3d1	italský
občanu	občan	k1gMnSc3	občan
<g/>
,	,	kIx,	,
Františkovi	František	k1gMnSc3	František
Kaligardovi	Kaligard	k1gMnSc3	Kaligard
(	(	kIx(	(
<g/>
it.	it.	k?	it.
Calligardo	Calligardo	k1gNnSc4	Calligardo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
třebíčských	třebíčský	k2eAgFnPc6d1	Třebíčská
čtvrtích	čtvrt	k1gFnPc6	čtvrt
žilo	žít	k5eAaImAgNnS	žít
dalších	další	k2eAgMnPc2d1	další
10	[number]	k4	10
Italů	Ital	k1gMnPc2	Ital
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
usadilo	usadit	k5eAaPmAgNnS	usadit
také	také	k9	také
asi	asi	k9	asi
5	[number]	k4	5
Slováků	Slovák	k1gMnPc2	Slovák
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
kupec	kupec	k1gMnSc1	kupec
Janek	Janek	k1gMnSc1	Janek
Strážnický	strážnický	k2eAgMnSc1d1	strážnický
a	a	k8xC	a
Januš	Januš	k1gMnSc1	Januš
Slovák	Slovák	k1gMnSc1	Slovák
<g/>
,	,	kIx,	,
tito	tento	k3xDgMnPc1	tento
vlastnili	vlastnit	k5eAaImAgMnP	vlastnit
domy	dům	k1gInPc4	dům
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
také	také	k9	také
začalo	začít	k5eAaPmAgNnS	začít
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
židovské	židovský	k2eAgNnSc4d1	Židovské
město	město	k1gNnSc4	město
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1601	[number]	k4	1601
se	se	k3xPyFc4	se
také	také	k9	také
objevují	objevovat	k5eAaImIp3nP	objevovat
zmínky	zmínka	k1gFnPc1	zmínka
o	o	k7c6	o
židovské	židovský	k2eAgFnSc6d1	židovská
škole	škola	k1gFnSc6	škola
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
existovala	existovat	k5eAaImAgFnS	existovat
pouze	pouze	k6eAd1	pouze
židovská	židovský	k2eAgFnSc1d1	židovská
ulice	ulice	k1gFnSc1	ulice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1656	[number]	k4	1656
a	a	k8xC	a
1657	[number]	k4	1657
do	do	k7c2	do
Třebíče	Třebíč	k1gFnSc2	Třebíč
přichází	přicházet	k5eAaImIp3nS	přicházet
hodně	hodně	k6eAd1	hodně
Čechů	Čech	k1gMnPc2	Čech
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
mohou	moct	k5eAaImIp3nP	moct
uchytit	uchytit	k5eAaPmF	uchytit
jako	jako	k9	jako
nekatolíci	nekatolík	k1gMnPc1	nekatolík
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
knih	kniha	k1gFnPc2	kniha
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1657	[number]	k4	1657
až	až	k8xS	až
1660	[number]	k4	1660
emigrovalo	emigrovat	k5eAaBmAgNnS	emigrovat
asi	asi	k9	asi
20-30	[number]	k4	20-30
rodin	rodina	k1gFnPc2	rodina
do	do	k7c2	do
Uher	Uhry	k1gFnPc2	Uhry
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Sčítání	sčítání	k1gNnSc3	sčítání
lidu	lid	k1gInSc2	lid
2001	[number]	k4	2001
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
okrese	okres	k1gInSc6	okres
Třebíč	Třebíč	k1gFnSc1	Třebíč
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
celkem	celkem	k6eAd1	celkem
117	[number]	k4	117
367	[number]	k4	367
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
bylo	být	k5eAaImAgNnS	být
nejvíce	nejvíce	k6eAd1	nejvíce
obyvatel	obyvatel	k1gMnPc2	obyvatel
s	s	k7c7	s
českou	český	k2eAgFnSc7d1	Česká
a	a	k8xC	a
moravskou	moravský	k2eAgFnSc7d1	Moravská
národností	národnost	k1gFnSc7	národnost
<g/>
,	,	kIx,	,
českou	český	k2eAgFnSc4d1	Česká
národnost	národnost	k1gFnSc4	národnost
při	při	k7c6	při
sčítání	sčítání	k1gNnSc6	sčítání
lidu	lid	k1gInSc2	lid
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
přiznalo	přiznat	k5eAaPmAgNnS	přiznat
100	[number]	k4	100
126	[number]	k4	126
<g/>
,	,	kIx,	,
moravskou	moravský	k2eAgFnSc4d1	Moravská
přiznalo	přiznat	k5eAaPmAgNnS	přiznat
12	[number]	k4	12
793	[number]	k4	793
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
okrese	okres	k1gInSc6	okres
Třebíč	Třebíč	k1gFnSc4	Třebíč
dále	daleko	k6eAd2	daleko
žilo	žít	k5eAaImAgNnS	žít
21	[number]	k4	21
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	s	k7c7	s
slezskou	slezský	k2eAgFnSc7d1	Slezská
národností	národnost	k1gFnSc7	národnost
<g/>
,	,	kIx,	,
940	[number]	k4	940
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	s	k7c7	s
slovenskou	slovenský	k2eAgFnSc7d1	slovenská
<g/>
,	,	kIx,	,
78	[number]	k4	78
obyvatel	obyvatel	k1gMnPc2	obyvatel
romskou	romský	k2eAgFnSc4d1	romská
<g/>
,	,	kIx,	,
52	[number]	k4	52
obyvatel	obyvatel	k1gMnPc2	obyvatel
s	s	k7c7	s
polskou	polský	k2eAgFnSc7d1	polská
<g/>
,	,	kIx,	,
31	[number]	k4	31
obyvatel	obyvatel	k1gMnPc2	obyvatel
s	s	k7c7	s
německou	německý	k2eAgFnSc7d1	německá
<g/>
,	,	kIx,	,
47	[number]	k4	47
obyvatel	obyvatel	k1gMnPc2	obyvatel
s	s	k7c7	s
ruskou	ruský	k2eAgFnSc7d1	ruská
národností	národnost	k1gFnSc7	národnost
<g/>
,	,	kIx,	,
139	[number]	k4	139
obyvatel	obyvatel	k1gMnPc2	obyvatel
s	s	k7c7	s
ukrajinskou	ukrajinský	k2eAgFnSc7d1	ukrajinská
národností	národnost	k1gFnSc7	národnost
a	a	k8xC	a
88	[number]	k4	88
obyvatel	obyvatel	k1gMnPc2	obyvatel
s	s	k7c7	s
vietnamskou	vietnamský	k2eAgFnSc7d1	vietnamská
národností	národnost	k1gFnSc7	národnost
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatel	obyvatel	k1gMnSc1	obyvatel
s	s	k7c7	s
ostatními	ostatní	k2eAgFnPc7d1	ostatní
zjištěnými	zjištěný	k2eAgFnPc7d1	zjištěná
národnostmi	národnost	k1gFnPc7	národnost
bylo	být	k5eAaImAgNnS	být
338	[number]	k4	338
a	a	k8xC	a
obyvatel	obyvatel	k1gMnSc1	obyvatel
s	s	k7c7	s
nezjištěnou	zjištěný	k2eNgFnSc7d1	nezjištěná
národností	národnost	k1gFnSc7	národnost
2	[number]	k4	2
714	[number]	k4	714
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
2015	[number]	k4	2015
a	a	k8xC	a
2016	[number]	k4	2016
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
meziročnímu	meziroční	k2eAgInSc3d1	meziroční
poklesu	pokles	k1gInSc3	pokles
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
v	v	k7c6	v
městě	město	k1gNnSc6	město
Třebíči	Třebíč	k1gFnSc6	Třebíč
o	o	k7c4	o
239	[number]	k4	239
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
odstěhování	odstěhování	k1gNnSc1	odstěhování
do	do	k7c2	do
jiných	jiný	k2eAgNnPc2d1	jiné
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
bylo	být	k5eAaImAgNnS	být
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
celkem	celkem	k6eAd1	celkem
k	k	k7c3	k
1194	[number]	k4	1194
trestným	trestný	k2eAgInPc3d1	trestný
činům	čin	k1gInPc3	čin
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
objasněno	objasnit	k5eAaPmNgNnS	objasnit
bylo	být	k5eAaImAgNnS	být
766	[number]	k4	766
činů	čin	k1gInPc2	čin
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
je	být	k5eAaImIp3nS	být
objasněnost	objasněnost	k1gFnSc4	objasněnost
64,15	[number]	k4	64,15
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
120	[number]	k4	120
trestných	trestný	k2eAgInPc2d1	trestný
činů	čin	k1gInPc2	čin
násilného	násilný	k2eAgInSc2d1	násilný
charakteru	charakter	k1gInSc2	charakter
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
objasněnost	objasněnost	k1gFnSc1	objasněnost
byla	být	k5eAaImAgFnS	být
75,83	[number]	k4	75,83
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Sčítání	sčítání	k1gNnSc3	sčítání
lidu	lid	k1gInSc2	lid
2011	[number]	k4	2011
====	====	k?	====
</s>
</p>
<p>
<s>
Dle	dle	k7c2	dle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
2011	[number]	k4	2011
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Třebíč	Třebíč	k1gFnSc1	Třebíč
žilo	žít	k5eAaImAgNnS	žít
celkem	celek	k1gInSc7	celek
36	[number]	k4	36
998	[number]	k4	998
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
bylo	být	k5eAaImAgNnS	být
nejvíce	nejvíce	k6eAd1	nejvíce
obyvatel	obyvatel	k1gMnPc2	obyvatel
s	s	k7c7	s
českou	český	k2eAgFnSc7d1	Česká
a	a	k8xC	a
moravskou	moravský	k2eAgFnSc7d1	Moravská
národností	národnost	k1gFnSc7	národnost
<g/>
,	,	kIx,	,
českou	český	k2eAgFnSc4d1	Česká
národnost	národnost	k1gFnSc4	národnost
při	při	k7c6	při
sčítání	sčítání	k1gNnSc6	sčítání
lidu	lid	k1gInSc2	lid
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
přiznalo	přiznat	k5eAaPmAgNnS	přiznat
21	[number]	k4	21
245	[number]	k4	245
<g/>
,	,	kIx,	,
moravskou	moravský	k2eAgFnSc4d1	Moravská
přiznalo	přiznat	k5eAaPmAgNnS	přiznat
5	[number]	k4	5
266	[number]	k4	266
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
dále	daleko	k6eAd2	daleko
žili	žít	k5eAaImAgMnP	žít
2	[number]	k4	2
obyvatelé	obyvatel	k1gMnPc1	obyvatel
se	s	k7c7	s
slezskou	slezský	k2eAgFnSc7d1	Slezská
národností	národnost	k1gFnSc7	národnost
<g/>
,	,	kIx,	,
250	[number]	k4	250
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	s	k7c7	s
slovenskou	slovenský	k2eAgFnSc7d1	slovenská
<g/>
,	,	kIx,	,
16	[number]	k4	16
obyvatel	obyvatel	k1gMnPc2	obyvatel
s	s	k7c7	s
romskou	romský	k2eAgFnSc7d1	romská
<g/>
,	,	kIx,	,
15	[number]	k4	15
obyvatel	obyvatel	k1gMnPc2	obyvatel
s	s	k7c7	s
polskou	polský	k2eAgFnSc7d1	polská
<g/>
,	,	kIx,	,
7	[number]	k4	7
obyvatel	obyvatel	k1gMnPc2	obyvatel
s	s	k7c7	s
německou	německý	k2eAgFnSc7d1	německá
<g/>
,	,	kIx,	,
74	[number]	k4	74
obyvatel	obyvatel	k1gMnPc2	obyvatel
s	s	k7c7	s
ukrajinskou	ukrajinský	k2eAgFnSc7d1	ukrajinská
národností	národnost	k1gFnSc7	národnost
a	a	k8xC	a
68	[number]	k4	68
obyvatel	obyvatel	k1gMnPc2	obyvatel
s	s	k7c7	s
vietnamskou	vietnamský	k2eAgFnSc7d1	vietnamská
národností	národnost	k1gFnSc7	národnost
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatel	obyvatel	k1gMnSc1	obyvatel
s	s	k7c7	s
nezjištěnou	zjištěný	k2eNgFnSc7d1	nezjištěná
národností	národnost	k1gFnSc7	národnost
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
8	[number]	k4	8
779	[number]	k4	779
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Náboženství	náboženství	k1gNnSc2	náboženství
===	===	k?	===
</s>
</p>
<p>
<s>
Město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
bývalého	bývalý	k2eAgInSc2d1	bývalý
kláštera	klášter	k1gInSc2	klášter
<g/>
,	,	kIx,	,
během	během	k7c2	během
staletí	staletí	k1gNnSc2	staletí
se	se	k3xPyFc4	se
v	v	k7c6	v
kultuře	kultura	k1gFnSc6	kultura
města	město	k1gNnSc2	město
projevovalo	projevovat	k5eAaImAgNnS	projevovat
více	hodně	k6eAd2	hodně
vlivů	vliv	k1gInPc2	vliv
<g/>
,	,	kIx,	,
původní	původní	k2eAgInSc1d1	původní
klášter	klášter	k1gInSc1	klášter
byl	být	k5eAaImAgInS	být
benediktinský	benediktinský	k2eAgMnSc1d1	benediktinský
<g/>
,	,	kIx,	,
kapuce	kapuce	k1gFnSc1	kapuce
benediktinů	benediktin	k1gMnPc2	benediktin
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
ve	v	k7c6	v
městském	městský	k2eAgInSc6d1	městský
znaku	znak	k1gInSc6	znak
a	a	k8xC	a
na	na	k7c6	na
městské	městský	k2eAgFnSc6d1	městská
vlajce	vlajka	k1gFnSc6	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
nynějšího	nynější	k2eAgInSc2d1	nynější
kostela	kostel	k1gInSc2	kostel
na	na	k7c6	na
Jejkově	Jejkův	k2eAgFnSc6d1	Jejkův
a	a	k8xC	a
katolického	katolický	k2eAgNnSc2d1	katolické
gymnázia	gymnázium	k1gNnSc2	gymnázium
nacházel	nacházet	k5eAaImAgMnS	nacházet
klášter	klášter	k1gInSc4	klášter
kapucínský	kapucínský	k2eAgInSc4d1	kapucínský
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
ve	v	k7c6	v
městě	město	k1gNnSc6	město
hojně	hojně	k6eAd1	hojně
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
protestantské	protestantský	k2eAgNnSc1d1	protestantské
vyznání	vyznání	k1gNnSc1	vyznání
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
objevili	objevit	k5eAaPmAgMnP	objevit
také	také	k9	také
židé	žid	k1gMnPc1	žid
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
jich	on	k3xPp3gInPc2	on
byla	být	k5eAaImAgFnS	být
nucena	nucen	k2eAgFnSc1d1	nucena
žít	žít	k5eAaImF	žít
odděleně	odděleně	k6eAd1	odděleně
od	od	k7c2	od
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
v	v	k7c6	v
nynější	nynější	k2eAgFnSc6d1	nynější
čtvrti	čtvrt	k1gFnSc6	čtvrt
Zámostí	Zámost	k1gFnPc2	Zámost
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
ale	ale	k8xC	ale
pronikali	pronikat	k5eAaImAgMnP	pronikat
i	i	k9	i
do	do	k7c2	do
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
části	část	k1gFnSc2	část
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
měli	mít	k5eAaImAgMnP	mít
své	svůj	k3xOyFgFnPc4	svůj
živnosti	živnost	k1gFnPc4	živnost
<g/>
.	.	kIx.	.
</s>
<s>
Židé	Žid	k1gMnPc1	Žid
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
založili	založit	k5eAaPmAgMnP	založit
dvě	dva	k4xCgFnPc4	dva
synagogy	synagoga	k1gFnPc4	synagoga
<g/>
.	.	kIx.	.
<g/>
Při	při	k7c6	při
sčítání	sčítání	k1gNnSc6	sčítání
lidu	lid	k1gInSc2	lid
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
se	s	k7c7	s
22	[number]	k4	22
246	[number]	k4	246
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
57,01	[number]	k4	57,01
%	%	kIx~	%
<g/>
)	)	kIx)	)
obyvatel	obyvatel	k1gMnPc2	obyvatel
zapsalo	zapsat	k5eAaPmAgNnS	zapsat
jako	jako	k8xC	jako
bez	bez	k7c2	bez
vyznání	vyznání	k1gNnSc2	vyznání
<g/>
,	,	kIx,	,
12	[number]	k4	12
266	[number]	k4	266
(	(	kIx(	(
<g/>
31,43	[number]	k4	31,43
%	%	kIx~	%
<g/>
)	)	kIx)	)
jako	jako	k9	jako
věřící	věřící	k2eAgMnSc1d1	věřící
a	a	k8xC	a
4	[number]	k4	4
509	[number]	k4	509
(	(	kIx(	(
<g/>
11,56	[number]	k4	11,56
%	%	kIx~	%
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
k	k	k7c3	k
otázce	otázka	k1gFnSc3	otázka
nevyjádřilo	vyjádřit	k5eNaPmAgNnS	vyjádřit
<g/>
.	.	kIx.	.
10	[number]	k4	10
691	[number]	k4	691
věřících	věřící	k1gMnPc2	věřící
se	se	k3xPyFc4	se
přihlásilo	přihlásit	k5eAaPmAgNnS	přihlásit
k	k	k7c3	k
římskokatolické	římskokatolický	k2eAgFnSc3d1	Římskokatolická
církvi	církev	k1gFnSc3	církev
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
ve	v	k7c6	v
městě	město	k1gNnSc6	město
několik	několik	k4yIc4	několik
kostelů	kostel	k1gInPc2	kostel
<g/>
,	,	kIx,	,
sídlí	sídlet	k5eAaImIp3nS	sídlet
zde	zde	k6eAd1	zde
děkanát	děkanát	k1gInSc4	děkanát
Třebíč	Třebíč	k1gFnSc1	Třebíč
a	a	k8xC	a
Katolické	katolický	k2eAgNnSc1d1	katolické
gymnázium	gymnázium	k1gNnSc1	gymnázium
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc7	druhý
nejpočetnější	početní	k2eAgFnSc7d3	nejpočetnější
církví	církev	k1gFnSc7	církev
je	být	k5eAaImIp3nS	být
Českobratrská	českobratrský	k2eAgFnSc1d1	Českobratrská
církev	církev	k1gFnSc1	církev
evangelická	evangelický	k2eAgFnSc1d1	evangelická
<g/>
,	,	kIx,	,
k	k	k7c3	k
níž	jenž	k3xRgFnSc3	jenž
se	se	k3xPyFc4	se
přihlásilo	přihlásit	k5eAaPmAgNnS	přihlásit
388	[number]	k4	388
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
ve	v	k7c6	v
městě	město	k1gNnSc6	město
sídlí	sídlet	k5eAaImIp3nS	sídlet
její	její	k3xOp3gInSc4	její
sbor	sbor	k1gInSc4	sbor
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
následuje	následovat	k5eAaImIp3nS	následovat
Církev	církev	k1gFnSc1	církev
československá	československý	k2eAgFnSc1d1	Československá
husitská	husitský	k2eAgFnSc1d1	husitská
se	s	k7c7	s
166	[number]	k4	166
věřícími	věřící	k1gFnPc7	věřící
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnPc4	její
modlitebna	modlitebna	k1gFnSc1	modlitebna
je	být	k5eAaImIp3nS	být
bývalou	bývalý	k2eAgFnSc7d1	bývalá
synagogou	synagoga	k1gFnSc7	synagoga
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
následují	následovat	k5eAaImIp3nP	následovat
Svědkové	svědek	k1gMnPc1	svědek
Jehovovi	Jehovův	k2eAgMnPc1d1	Jehovův
(	(	kIx(	(
<g/>
110	[number]	k4	110
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
a	a	k8xC	a
Pravoslavná	pravoslavný	k2eAgFnSc1d1	pravoslavná
církev	církev	k1gFnSc1	církev
v	v	k7c6	v
Českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
a	a	k8xC	a
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
(	(	kIx(	(
<g/>
67	[number]	k4	67
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
také	také	k9	také
malý	malý	k2eAgInSc1d1	malý
kostel	kostel	k1gInSc1	kostel
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
obsluhován	obsluhovat	k5eAaImNgInS	obsluhovat
popem	pop	k1gInSc7	pop
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
další	další	k2eAgNnSc1d1	další
sčítání	sčítání	k1gNnSc1	sčítání
lidu	lid	k1gInSc2	lid
<g/>
,	,	kIx,	,
celkem	celkem	k6eAd1	celkem
12	[number]	k4	12
562	[number]	k4	562
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
33,95	[number]	k4	33,95
%	%	kIx~	%
<g/>
)	)	kIx)	)
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
zapsalo	zapsat	k5eAaPmAgNnS	zapsat
jako	jako	k9	jako
bez	bez	k7c2	bez
vyznání	vyznání	k1gNnSc2	vyznání
<g/>
,	,	kIx,	,
8	[number]	k4	8
947	[number]	k4	947
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
24,18	[number]	k4	24,18
%	%	kIx~	%
<g/>
)	)	kIx)	)
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
zapsalo	zapsat	k5eAaPmAgNnS	zapsat
jako	jako	k9	jako
věřící	věřící	k2eAgNnSc1d1	věřící
a	a	k8xC	a
celkem	celkem	k6eAd1	celkem
15485	[number]	k4	15485
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
41,85	[number]	k4	41,85
%	%	kIx~	%
<g/>
)	)	kIx)	)
obyvatel	obyvatel	k1gMnSc1	obyvatel
se	se	k3xPyFc4	se
k	k	k7c3	k
otázce	otázka	k1gFnSc3	otázka
víry	víra	k1gFnSc2	víra
nevyjádřilo	vyjádřit	k5eNaPmAgNnS	vyjádřit
<g/>
.	.	kIx.	.
</s>
<s>
Nejpočetnější	početní	k2eAgFnSc7d3	nejpočetnější
církví	církev	k1gFnSc7	církev
zůstala	zůstat	k5eAaPmAgFnS	zůstat
římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
církev	církev	k1gFnSc1	církev
-	-	kIx~	-
5	[number]	k4	5
013	[number]	k4	013
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
druhou	druhý	k4xOgFnSc4	druhý
nejpočetnější	početní	k2eAgFnSc4d3	nejpočetnější
zůstala	zůstat	k5eAaPmAgFnS	zůstat
Českobratrská	českobratrský	k2eAgFnSc1d1	Českobratrská
církev	církev	k1gFnSc1	církev
evangelická	evangelický	k2eAgFnSc1d1	evangelická
<g/>
,	,	kIx,	,
k	k	k7c3	k
níž	jenž	k3xRgFnSc3	jenž
se	se	k3xPyFc4	se
přihlásilo	přihlásit	k5eAaPmAgNnS	přihlásit
233	[number]	k4	233
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
třetím	třetí	k4xOgNnSc6	třetí
nejčastějším	častý	k2eAgNnSc6d3	nejčastější
vyznání	vyznání	k1gNnSc6	vyznání
zůstala	zůstat	k5eAaPmAgFnS	zůstat
Církev	církev	k1gFnSc1	církev
československá	československý	k2eAgFnSc1d1	Československá
husitská	husitský	k2eAgFnSc1d1	husitská
se	se	k3xPyFc4	se
104	[number]	k4	104
přihlášenými	přihlášený	k2eAgMnPc7d1	přihlášený
<g/>
.	.	kIx.	.
<g/>
Významná	významný	k2eAgFnSc1d1	významná
je	být	k5eAaImIp3nS	být
i	i	k9	i
přítomnost	přítomnost	k1gFnSc1	přítomnost
církve	církev	k1gFnSc2	církev
bratrské	bratrský	k2eAgFnSc2d1	bratrská
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
sice	sice	k8xC	sice
nemá	mít	k5eNaImIp3nS	mít
<g />
.	.	kIx.	.
</s>
<s>
příliš	příliš	k6eAd1	příliš
mnoho	mnoho	k4c1	mnoho
věřících	věřící	k1gMnPc2	věřící
přímo	přímo	k6eAd1	přímo
mezi	mezi	k7c7	mezi
obyvateli	obyvatel	k1gMnPc7	obyvatel
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
sídlí	sídlet	k5eAaImIp3nS	sídlet
zde	zde	k6eAd1	zde
její	její	k3xOp3gInSc1	její
sbor	sbor	k1gInSc1	sbor
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
věřícím	věřící	k1gFnPc3	věřící
z	z	k7c2	z
celého	celý	k2eAgNnSc2d1	celé
Třebíčska	Třebíčsko	k1gNnSc2	Třebíčsko
a	a	k8xC	a
přilehlého	přilehlý	k2eAgNnSc2d1	přilehlé
okolí	okolí	k1gNnSc2	okolí
<g/>
.	.	kIx.	.
<g/>
Velký	velký	k2eAgInSc4d1	velký
význam	význam	k1gInSc4	význam
zde	zde	k6eAd1	zde
mělo	mít	k5eAaImAgNnS	mít
i	i	k9	i
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
židovského	židovský	k2eAgNnSc2d1	Židovské
vyznání	vyznání	k1gNnSc2	vyznání
<g/>
,	,	kIx,	,
v	v	k7c6	v
části	část	k1gFnSc6	část
Zámostí	Zámost	k1gFnPc2	Zámost
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
pod	pod	k7c7	pod
správou	správa	k1gFnSc7	správa
Podklášteří	podklášteří	k1gNnSc2	podklášteří
založili	založit	k5eAaPmAgMnP	založit
vlastní	vlastní	k2eAgFnSc4d1	vlastní
obec	obec	k1gFnSc4	obec
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byly	být	k5eAaImAgFnP	být
postaveny	postaven	k2eAgInPc1d1	postaven
dvě	dva	k4xCgFnPc1	dva
synagogy	synagoga	k1gFnPc1	synagoga
<g/>
.	.	kIx.	.
</s>
<s>
Starší	starý	k2eAgFnSc1d2	starší
byla	být	k5eAaImAgFnS	být
postavena	postavit	k5eAaPmNgFnS	postavit
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1639	[number]	k4	1639
a	a	k8xC	a
1641	[number]	k4	1641
<g/>
,	,	kIx,	,
novější	nový	k2eAgMnSc1d2	novější
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
byl	být	k5eAaImAgMnS	být
i	i	k9	i
fungující	fungující	k2eAgInSc4d1	fungující
židovský	židovský	k2eAgInSc4d1	židovský
hřbitov	hřbitov	k1gInSc4	hřbitov
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc1	ten
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
svahu	svah	k1gInSc6	svah
Týnského	týnský	k2eAgNnSc2d1	týnské
údolí	údolí	k1gNnSc2	údolí
<g/>
,	,	kIx,	,
starší	starý	k2eAgInPc1d2	starší
pohřby	pohřeb	k1gInPc1	pohřeb
ovšem	ovšem	k9	ovšem
byly	být	k5eAaImAgInP	být
vykonávány	vykonávat	k5eAaImNgInP	vykonávat
u	u	k7c2	u
klášterních	klášterní	k2eAgFnPc2d1	klášterní
hradeb	hradba	k1gFnPc2	hradba
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byly	být	k5eAaImAgFnP	být
objeveny	objeven	k2eAgFnPc1d1	objevena
kostry	kostra	k1gFnPc1	kostra
se	se	k3xPyFc4	se
střípky	střípek	k1gInPc7	střípek
na	na	k7c6	na
očích	oko	k1gNnPc6	oko
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
byly	být	k5eAaImAgInP	být
obráceny	obrátit	k5eAaPmNgInP	obrátit
k	k	k7c3	k
východu	východ	k1gInSc3	východ
<g/>
.	.	kIx.	.
</s>
<s>
Nejdelší	dlouhý	k2eAgFnSc4d3	nejdelší
dobu	doba	k1gFnSc4	doba
rabínský	rabínský	k2eAgInSc4d1	rabínský
úřad	úřad	k1gInSc4	úřad
zastával	zastávat	k5eAaImAgMnS	zastávat
třebíčský	třebíčský	k2eAgMnSc1d1	třebíčský
rodák	rodák	k1gMnSc1	rodák
a	a	k8xC	a
gymnazijní	gymnazijní	k2eAgMnSc1d1	gymnazijní
profesor	profesor	k1gMnSc1	profesor
Joachim	Joachim	k1gMnSc1	Joachim
Pollak	Pollak	k1gMnSc1	Pollak
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
tomu	ten	k3xDgNnSc3	ten
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1828	[number]	k4	1828
a	a	k8xC	a
1879	[number]	k4	1879
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Politika	politikum	k1gNnSc2	politikum
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
prvních	první	k4xOgFnPc6	první
porevolučních	porevoluční	k2eAgFnPc6d1	porevoluční
svobodných	svobodný	k2eAgFnPc6d1	svobodná
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
České	český	k2eAgFnSc2d1	Česká
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
<g/>
,	,	kIx,	,
Sněmovny	sněmovna	k1gFnPc1	sněmovna
lidu	lid	k1gInSc2	lid
Federálního	federální	k2eAgNnSc2d1	federální
shromáždění	shromáždění	k1gNnSc2	shromáždění
a	a	k8xC	a
Sněmovny	sněmovna	k1gFnSc2	sněmovna
národů	národ	k1gInPc2	národ
Federálního	federální	k2eAgNnSc2d1	federální
shromáždění	shromáždění	k1gNnSc2	shromáždění
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
voleno	volit	k5eAaImNgNnS	volit
8	[number]	k4	8
<g/>
.	.	kIx.	.
a	a	k8xC	a
9	[number]	k4	9
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
zasedla	zasednout	k5eAaPmAgFnS	zasednout
okresní	okresní	k2eAgFnSc1d1	okresní
volební	volební	k2eAgFnSc1d1	volební
komise	komise	k1gFnSc1	komise
ONV	ONV	kA	ONV
a	a	k8xC	a
losem	los	k1gInSc7	los
vybrala	vybrat	k5eAaPmAgFnS	vybrat
předsedu	předseda	k1gMnSc4	předseda
a	a	k8xC	a
místopředsedu	místopředseda	k1gMnSc4	místopředseda
volební	volební	k2eAgFnSc2d1	volební
komise	komise	k1gFnSc2	komise
<g/>
,	,	kIx,	,
stali	stát	k5eAaPmAgMnP	stát
se	se	k3xPyFc4	se
jimi	on	k3xPp3gMnPc7	on
Karel	Karel	k1gMnSc1	Karel
Svoboda	Svoboda	k1gMnSc1	Svoboda
(	(	kIx(	(
<g/>
ČSL	ČSL	kA	ČSL
<g/>
)	)	kIx)	)
a	a	k8xC	a
Ladislav	Ladislav	k1gMnSc1	Ladislav
Fiala	Fiala	k1gMnSc1	Fiala
(	(	kIx(	(
<g/>
OF	OF	kA	OF
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
ČNR	ČNR	kA	ČNR
byli	být	k5eAaImAgMnP	být
zvoleni	zvolit	k5eAaPmNgMnP	zvolit
Jiří	Jiří	k1gMnSc1	Jiří
Karas	Karas	k1gMnSc1	Karas
a	a	k8xC	a
Miroslav	Miroslav	k1gMnSc1	Miroslav
Wolf	Wolf	k1gMnSc1	Wolf
<g/>
,	,	kIx,	,
do	do	k7c2	do
Sněmovny	sněmovna	k1gFnSc2	sněmovna
lidu	lid	k1gInSc2	lid
byli	být	k5eAaImAgMnP	být
zvoleni	zvolit	k5eAaPmNgMnP	zvolit
Pavel	Pavel	k1gMnSc1	Pavel
Zahrádka	Zahrádka	k1gMnSc1	Zahrádka
a	a	k8xC	a
Ivo	Ivo	k1gMnSc1	Ivo
Novotný	Novotný	k1gMnSc1	Novotný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Památky	památka	k1gFnPc4	památka
a	a	k8xC	a
zajímavosti	zajímavost	k1gFnPc4	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
mnoho	mnoho	k4c1	mnoho
památek	památka	k1gFnPc2	památka
a	a	k8xC	a
významných	významný	k2eAgNnPc2d1	významné
míst	místo	k1gNnPc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
starobylé	starobylý	k2eAgNnSc1d1	starobylé
židovské	židovský	k2eAgNnSc1d1	Židovské
město	město	k1gNnSc1	město
a	a	k8xC	a
bazilika	bazilika	k1gFnSc1	bazilika
svatého	svatý	k2eAgMnSc2d1	svatý
Prokopa	Prokop	k1gMnSc2	Prokop
<g/>
,	,	kIx,	,
obě	dva	k4xCgFnPc1	dva
památky	památka	k1gFnPc1	památka
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
zapsány	zapsat	k5eAaPmNgInP	zapsat
do	do	k7c2	do
seznamu	seznam	k1gInSc2	seznam
světového	světový	k2eAgNnSc2d1	světové
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Karlovo	Karlův	k2eAgNnSc4d1	Karlovo
náměstí	náměstí	k1gNnSc4	náměstí
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgNnPc2d3	veliký
náměstí	náměstí	k1gNnPc2	náměstí
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
levém	levý	k2eAgInSc6d1	levý
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
Jihlavy	Jihlava	k1gFnSc2	Jihlava
<g/>
,	,	kIx,	,
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
zvaném	zvaný	k2eAgNnSc6d1	zvané
Židy	Žid	k1gMnPc7	Žid
či	či	k8xC	či
Zámostí	Zámost	k1gFnSc7	Zámost
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
rozprostírá	rozprostírat	k5eAaImIp3nS	rozprostírat
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
židovských	židovský	k2eAgFnPc2d1	židovská
čtvrtí	čtvrt	k1gFnPc2	čtvrt
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Třebíčská	třebíčský	k2eAgFnSc1d1	Třebíčská
židovská	židovský	k2eAgFnSc1d1	židovská
čtvrť	čtvrť	k1gFnSc1	čtvrť
je	být	k5eAaImIp3nS	být
jedinou	jediný	k2eAgFnSc7d1	jediná
židovskou	židovský	k2eAgFnSc7d1	židovská
památkou	památka	k1gFnSc7	památka
mimo	mimo	k7c4	mimo
území	území	k1gNnSc4	území
Izraele	Izrael	k1gInSc2	Izrael
<g/>
,	,	kIx,	,
samostatně	samostatně	k6eAd1	samostatně
zapsanou	zapsaný	k2eAgFnSc4d1	zapsaná
v	v	k7c6	v
seznamu	seznam	k1gInSc6	seznam
UNESCO	Unesco	k1gNnSc1	Unesco
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
něj	on	k3xPp3gNnSc2	on
byla	být	k5eAaImAgFnS	být
zapsána	zapsán	k2eAgFnSc1d1	zapsána
3	[number]	k4	3
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
křivolaké	křivolaký	k2eAgFnPc1d1	křivolaká
uličky	ulička	k1gFnPc1	ulička
<g/>
,	,	kIx,	,
původní	původní	k2eAgInPc1d1	původní
domy	dům	k1gInPc1	dům
<g/>
,	,	kIx,	,
klenuté	klenutý	k2eAgInPc1d1	klenutý
průchody	průchod	k1gInPc1	průchod
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgInPc2d1	další
typických	typický	k2eAgInPc2d1	typický
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Radnice	radnice	k1gFnSc1	radnice
<g/>
,	,	kIx,	,
rabinát	rabinát	k1gInSc1	rabinát
<g/>
,	,	kIx,	,
nemocnice	nemocnice	k1gFnSc1	nemocnice
ani	ani	k8xC	ani
většina	většina	k1gFnSc1	většina
ostatních	ostatní	k2eAgFnPc2d1	ostatní
památek	památka	k1gFnPc2	památka
dnes	dnes	k6eAd1	dnes
neslouží	sloužit	k5eNaImIp3nS	sloužit
svému	svůj	k3xOyFgInSc3	svůj
původnímu	původní	k2eAgInSc3d1	původní
účelu	účel	k1gInSc3	účel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Podklášteří	podklášteří	k1gNnSc6	podklášteří
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Bazilika	bazilika	k1gFnSc1	bazilika
svatého	svatý	k2eAgMnSc2d1	svatý
Prokopa	Prokop	k1gMnSc2	Prokop
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
společně	společně	k6eAd1	společně
s	s	k7c7	s
židovskou	židovský	k2eAgFnSc7d1	židovská
čtvrtí	čtvrt	k1gFnSc7	čtvrt
zapsána	zapsán	k2eAgFnSc1d1	zapsána
na	na	k7c6	na
seznamu	seznam	k1gInSc6	seznam
UNESCO	Unesco	k1gNnSc1	Unesco
<g/>
.	.	kIx.	.
</s>
<s>
Kdysi	kdysi	k6eAd1	kdysi
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nacházelo	nacházet	k5eAaImAgNnS	nacházet
benediktinské	benediktinský	k2eAgNnSc1d1	benediktinské
opatství	opatství	k1gNnSc1	opatství
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1101	[number]	k4	1101
<g/>
.	.	kIx.	.
</s>
<s>
Klášter	klášter	k1gInSc1	klášter
se	se	k3xPyFc4	se
postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
stal	stát	k5eAaPmAgInS	stát
náboženským	náboženský	k2eAgNnSc7d1	náboženské
centrem	centrum	k1gNnSc7	centrum
i	i	k8xC	i
vojenskou	vojenský	k2eAgFnSc7d1	vojenská
pevností	pevnost	k1gFnSc7	pevnost
<g/>
,	,	kIx,	,
tuto	tento	k3xDgFnSc4	tento
pozici	pozice	k1gFnSc4	pozice
si	se	k3xPyFc3	se
udržel	udržet	k5eAaPmAgInS	udržet
až	až	k9	až
do	do	k7c2	do
husitských	husitský	k2eAgFnPc2d1	husitská
válek	válka	k1gFnPc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1468	[number]	k4	1468
byl	být	k5eAaImAgInS	být
při	při	k7c6	při
tzv.	tzv.	kA	tzv.
bitvě	bitva	k1gFnSc6	bitva
o	o	k7c4	o
Třebíč	Třebíč	k1gFnSc4	Třebíč
dobyt	dobyt	k2eAgMnSc1d1	dobyt
uherskými	uherský	k2eAgMnPc7d1	uherský
vojáky	voják	k1gMnPc7	voják
<g/>
.	.	kIx.	.
</s>
<s>
Pozdější	pozdní	k2eAgFnSc1d2	pozdější
světská	světský	k2eAgFnSc1d1	světská
vrchnost	vrchnost	k1gFnSc1	vrchnost
klášter	klášter	k1gInSc1	klášter
přestavěla	přestavět	k5eAaPmAgFnS	přestavět
na	na	k7c4	na
zámek	zámek	k1gInSc4	zámek
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
zde	zde	k6eAd1	zde
sídlí	sídlet	k5eAaImIp3nS	sídlet
Muzeum	muzeum	k1gNnSc4	muzeum
Vysočiny	vysočina	k1gFnSc2	vysočina
Třebíč	Třebíč	k1gFnSc1	Třebíč
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
rekonstruováno	rekonstruovat	k5eAaBmNgNnS	rekonstruovat
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
prostory	prostor	k1gInPc7	prostor
zámku	zámek	k1gInSc2	zámek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přes	přes	k7c4	přes
řeku	řeka	k1gFnSc4	řeka
naproti	naproti	k7c3	naproti
židovskému	židovský	k2eAgNnSc3d1	Židovské
městu	město	k1gNnSc3	město
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
centrum	centrum	k1gNnSc1	centrum
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
tvořeno	tvořit	k5eAaImNgNnS	tvořit
Karlovým	Karlův	k2eAgNnSc7d1	Karlovo
náměstím	náměstí	k1gNnSc7	náměstí
<g/>
,	,	kIx,	,
lidově	lidově	k6eAd1	lidově
zvaným	zvaný	k2eAgInSc7d1	zvaný
také	také	k9	také
rynek	rynek	k1gInSc1	rynek
<g/>
,	,	kIx,	,
vytyčeným	vytyčený	k2eAgInPc3d1	vytyčený
při	při	k7c6	při
založení	založení	k1gNnSc6	založení
města	město	k1gNnSc2	město
jako	jako	k8xS	jako
tržiště	tržiště	k1gNnSc2	tržiště
s	s	k7c7	s
plochou	plocha	k1gFnSc7	plocha
22	[number]	k4	22
000	[number]	k4	000
m2	m2	k4	m2
<g/>
.	.	kIx.	.
</s>
<s>
Těmito	tento	k3xDgInPc7	tento
rozměry	rozměr	k1gInPc7	rozměr
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
o	o	k7c4	o
pár	pár	k4xCyI	pár
metrů	metr	k1gInPc2	metr
čtverečných	čtverečný	k2eAgInPc2d1	čtverečný
menší	malý	k2eAgNnSc4d2	menší
než	než	k8xS	než
Václavské	václavský	k2eAgNnSc4d1	Václavské
náměstí	náměstí	k1gNnSc4	náměstí
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
mohlo	moct	k5eAaImAgNnS	moct
velikostí	velikost	k1gFnSc7	velikost
náměstí	náměstí	k1gNnSc2	náměstí
rovnat	rovnat	k5eAaImF	rovnat
městům	město	k1gNnPc3	město
královským	královský	k2eAgNnPc3d1	královské
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
mělo	mít	k5eAaImAgNnS	mít
náměstí	náměstí	k1gNnSc4	náměstí
ještě	ještě	k6eAd1	ještě
renesanční	renesanční	k2eAgFnSc4d1	renesanční
a	a	k8xC	a
barokní	barokní	k2eAgFnSc4d1	barokní
podobu	podoba	k1gFnSc4	podoba
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
zničil	zničit	k5eAaPmAgInS	zničit
požár	požár	k1gInSc1	požár
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1822	[number]	k4	1822
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
se	se	k3xPyFc4	se
na	na	k7c6	na
Karlově	Karlův	k2eAgNnSc6d1	Karlovo
náměstí	náměstí	k1gNnSc6	náměstí
konají	konat	k5eAaImIp3nP	konat
dopolední	dopolední	k2eAgInPc1d1	dopolední
trhy	trh	k1gInPc1	trh
<g/>
,	,	kIx,	,
kulturní	kulturní	k2eAgFnPc1d1	kulturní
akce	akce	k1gFnPc1	akce
jako	jako	k8xS	jako
Třebíčské	třebíčský	k2eAgFnPc1d1	Třebíčská
folklórní	folklórní	k2eAgFnPc1d1	folklórní
slavnosti	slavnost	k1gFnPc1	slavnost
a	a	k8xC	a
oslavy	oslava	k1gFnPc1	oslava
vstupu	vstup	k1gInSc2	vstup
města	město	k1gNnSc2	město
do	do	k7c2	do
Seznamu	seznam	k1gInSc2	seznam
světového	světový	k2eAgNnSc2d1	světové
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
kopci	kopec	k1gInSc6	kopec
ve	v	k7c6	v
Vnitřním	vnitřní	k2eAgNnSc6d1	vnitřní
Městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
75	[number]	k4	75
metrů	metr	k1gInPc2	metr
vysoká	vysoký	k2eAgFnSc1d1	vysoká
městská	městský	k2eAgFnSc1d1	městská
věž	věž	k1gFnSc1	věž
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
náleží	náležet	k5eAaImIp3nS	náležet
ke	k	k7c3	k
kostelu	kostel	k1gInSc3	kostel
sv.	sv.	kA	sv.
Martina	Martin	k1gMnSc4	Martin
z	z	k7c2	z
Tours	Toursa	k1gFnPc2	Toursa
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
ním	on	k3xPp3gInSc7	on
však	však	k9	však
dříve	dříve	k6eAd2	dříve
nebyla	být	k5eNaImAgFnS	být
spojena	spojit	k5eAaPmNgFnS	spojit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
sloužila	sloužit	k5eAaImAgFnS	sloužit
jako	jako	k9	jako
součást	součást	k1gFnSc1	součást
opevnění	opevnění	k1gNnSc2	opevnění
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
zde	zde	k6eAd1	zde
leží	ležet	k5eAaImIp3nS	ležet
kapucínský	kapucínský	k2eAgInSc4d1	kapucínský
klášter	klášter	k1gInSc4	klášter
<g/>
,	,	kIx,	,
pravoslavný	pravoslavný	k2eAgInSc4d1	pravoslavný
a	a	k8xC	a
evangelický	evangelický	k2eAgInSc4d1	evangelický
kostel	kostel	k1gInSc4	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Karlově	Karlův	k2eAgNnSc6d1	Karlovo
náměstí	náměstí	k1gNnSc6	náměstí
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
i	i	k9	i
Malovaný	malovaný	k2eAgInSc1d1	malovaný
dům	dům	k1gInSc1	dům
s	s	k7c7	s
galerií	galerie	k1gFnSc7	galerie
a	a	k8xC	a
informačním	informační	k2eAgInSc7d1	informační
centrem	centr	k1gInSc7	centr
<g/>
,	,	kIx,	,
Černý	Černý	k1gMnSc1	Černý
dům	dům	k1gInSc1	dům
a	a	k8xC	a
sousoší	sousoší	k1gNnSc1	sousoší
Cyrila	Cyril	k1gMnSc2	Cyril
a	a	k8xC	a
Metoděje	Metoděj	k1gMnSc2	Metoděj
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
na	na	k7c6	na
Karlově	Karlův	k2eAgNnSc6d1	Karlovo
náměstí	náměstí	k1gNnSc6	náměstí
nachází	nacházet	k5eAaImIp3nS	nacházet
Národní	národní	k2eAgInSc1d1	národní
dům	dům	k1gInSc1	dům
se	s	k7c7	s
stálou	stálý	k2eAgFnSc7d1	stálá
expozicí	expozice	k1gFnSc7	expozice
třebíčského	třebíčský	k2eAgMnSc4d1	třebíčský
rodáka	rodák	k1gMnSc4	rodák
Františka	František	k1gMnSc4	František
Mertla	Mertl	k1gMnSc4	Mertl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
židovském	židovský	k2eAgNnSc6d1	Židovské
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Galerie	galerie	k1gFnSc1	galerie
Ladislava	Ladislav	k1gMnSc2	Ladislav
Nováka	Novák	k1gMnSc2	Novák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Východní	východní	k2eAgFnPc1d1	východní
oblasti	oblast	k1gFnPc1	oblast
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
směrem	směr	k1gInSc7	směr
do	do	k7c2	do
čtvrti	čtvrt	k1gFnSc2	čtvrt
Borovina	borovina	k1gFnSc1	borovina
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
na	na	k7c6	na
Stařečce	Stařečka	k1gFnSc6	Stařečka
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
kaple	kaple	k1gFnSc1	kaple
Povýšení	povýšení	k1gNnSc2	povýšení
sv.	sv.	kA	sv.
Kříže	kříž	k1gInSc2	kříž
<g/>
,	,	kIx,	,
na	na	k7c6	na
nejvyšším	vysoký	k2eAgNnSc6d3	nejvyšší
místě	místo	k1gNnSc6	místo
Třebíče	Třebíč	k1gFnSc2	Třebíč
<g/>
,	,	kIx,	,
nedaleko	daleko	k6eNd1	daleko
od	od	k7c2	od
nedostavěné	dostavěný	k2eNgFnSc2d1	nedostavěná
hvězdárny	hvězdárna	k1gFnSc2	hvězdárna
a	a	k8xC	a
kluboven	klubovna	k1gFnPc2	klubovna
skautských	skautský	k2eAgInPc2d1	skautský
oddílů	oddíl	k1gInPc2	oddíl
<g/>
,	,	kIx,	,
zvaném	zvaný	k2eAgNnSc6d1	zvané
Kostelíček	kostelíček	k1gInSc1	kostelíček
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
kaple	kaple	k1gFnSc1	kaple
má	mít	k5eAaImIp3nS	mít
půdorys	půdorys	k1gInSc4	půdorys
kříže	kříž	k1gInSc2	kříž
a	a	k8xC	a
jejím	její	k3xOp3gMnSc7	její
stavitelem	stavitel	k1gMnSc7	stavitel
byl	být	k5eAaImAgMnS	být
architektonicky	architektonicky	k6eAd1	architektonicky
nevzdělaný	vzdělaný	k2eNgMnSc1d1	nevzdělaný
stavitel	stavitel	k1gMnSc1	stavitel
Jan	Jan	k1gMnSc1	Jan
Fulík	Fulík	k1gMnSc1	Fulík
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
postavena	postavit	k5eAaPmNgFnS	postavit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1644	[number]	k4	1644
<g/>
.	.	kIx.	.
</s>
<s>
Nedaleko	nedaleko	k7c2	nedaleko
kaple	kaple	k1gFnSc2	kaple
stojí	stát	k5eAaImIp3nS	stát
i	i	k9	i
původní	původní	k2eAgFnSc1d1	původní
vodárna	vodárna	k1gFnSc1	vodárna
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yQgFnSc2	který
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
vytvořena	vytvořen	k2eAgFnSc1d1	vytvořena
rozhledna	rozhledna	k1gFnSc1	rozhledna
s	s	k7c7	s
expozicí	expozice	k1gFnSc7	expozice
vodárenství	vodárenství	k1gNnPc2	vodárenství
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
<g/>
.	.	kIx.	.
<g/>
U	u	k7c2	u
řeky	řeka	k1gFnSc2	řeka
Jihlavy	Jihlava	k1gFnSc2	Jihlava
leží	ležet	k5eAaImIp3nS	ležet
říční	říční	k2eAgFnSc2d1	říční
lázně	lázeň	k1gFnSc2	lázeň
Polanka	polanka	k1gFnSc1	polanka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1933	[number]	k4	1933
<g/>
.	.	kIx.	.
</s>
<s>
Dřevěné	dřevěný	k2eAgFnPc4d1	dřevěná
šatny	šatna	k1gFnPc4	šatna
z	z	k7c2	z
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
jsou	být	k5eAaImIp3nP	být
stále	stále	k6eAd1	stále
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
a	a	k8xC	a
stále	stále	k6eAd1	stále
jsou	být	k5eAaImIp3nP	být
předmětem	předmět	k1gInSc7	předmět
obdivu	obdiv	k1gInSc2	obdiv
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
návrhu	návrh	k1gInSc2	návrh
byl	být	k5eAaImAgMnS	být
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Fuchs	Fuchs	k1gMnSc1	Fuchs
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
lázních	lázeň	k1gFnPc6	lázeň
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
konaly	konat	k5eAaImAgFnP	konat
Hry	hra	k1gFnPc1	hra
bez	bez	k7c2	bez
hranic	hranice	k1gFnPc2	hranice
<g/>
,	,	kIx,	,
třebíčský	třebíčský	k2eAgInSc1d1	třebíčský
tým	tým	k1gInSc1	tým
nakonec	nakonec	k6eAd1	nakonec
zvítězil	zvítězit	k5eAaPmAgInS	zvítězit
i	i	k9	i
ve	v	k7c4	v
finále	finále	k1gNnSc4	finále
tohoto	tento	k3xDgInSc2	tento
ročníku	ročník	k1gInSc2	ročník
na	na	k7c6	na
Azorských	azorský	k2eAgInPc6d1	azorský
ostrovech	ostrov	k1gInPc6	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tzv.	tzv.	kA	tzv.
Kanciborku	Kanciborek	k1gInSc6	Kanciborek
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Větrný	větrný	k2eAgInSc1d1	větrný
mlýn	mlýn	k1gInSc1	mlýn
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
sloužil	sloužit	k5eAaImAgInS	sloužit
na	na	k7c4	na
úpravu	úprava	k1gFnSc4	úprava
třísla	tříslo	k1gNnSc2	tříslo
pro	pro	k7c4	pro
koželuhy	koželuh	k1gMnPc4	koželuh
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
není	být	k5eNaImIp3nS	být
funkční	funkční	k2eAgNnSc1d1	funkční
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
také	také	k9	také
několik	několik	k4yIc4	několik
městských	městský	k2eAgInPc2d1	městský
parků	park	k1gInPc2	park
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
rozloženy	rozložit	k5eAaPmNgInP	rozložit
poměrně	poměrně	k6eAd1	poměrně
rovnoměrně	rovnoměrně	k6eAd1	rovnoměrně
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
místní	místní	k2eAgFnSc6d1	místní
části	část	k1gFnSc6	část
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
alespoň	alespoň	k9	alespoň
jeden	jeden	k4xCgMnSc1	jeden
<g/>
.	.	kIx.	.
</s>
<s>
Tvorba	tvorba	k1gFnSc1	tvorba
parků	park	k1gInPc2	park
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
i	i	k9	i
svou	svůj	k3xOyFgFnSc4	svůj
tradici	tradice	k1gFnSc4	tradice
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Lorenzovy	Lorenzův	k2eAgFnPc1d1	Lorenzova
sady	sada	k1gFnPc1	sada
mají	mít	k5eAaImIp3nP	mít
svůj	svůj	k3xOyFgInSc4	svůj
počátek	počátek	k1gInSc4	počátek
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1899	[number]	k4	1899
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
mnoho	mnoho	k6eAd1	mnoho
z	z	k7c2	z
parků	park	k1gInPc2	park
městem	město	k1gNnSc7	město
upravováno	upravován	k2eAgNnSc4d1	upravováno
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
nákladů	náklad	k1gInPc2	náklad
spojených	spojený	k2eAgInPc2d1	spojený
s	s	k7c7	s
těmito	tento	k3xDgFnPc7	tento
úpravami	úprava	k1gFnPc7	úprava
je	být	k5eAaImIp3nS	být
hrazena	hradit	k5eAaImNgFnS	hradit
z	z	k7c2	z
evropských	evropský	k2eAgInPc2d1	evropský
fondů	fond	k1gInPc2	fond
<g/>
.	.	kIx.	.
<g/>
Památky	památka	k1gFnPc1	památka
zapsané	zapsaný	k2eAgFnPc1d1	zapsaná
do	do	k7c2	do
seznamu	seznam	k1gInSc2	seznam
památek	památka	k1gFnPc2	památka
UNESCO	UNESCO	kA	UNESCO
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
získaly	získat	k5eAaPmAgInP	získat
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
města	město	k1gNnSc2	město
grant	grant	k1gInSc4	grant
Kraje	kraj	k1gInSc2	kraj
Vysočina	vysočina	k1gFnSc1	vysočina
vázaný	vázaný	k2eAgMnSc1d1	vázaný
na	na	k7c4	na
začlenění	začlenění	k1gNnSc4	začlenění
kamerových	kamerový	k2eAgInPc2d1	kamerový
záznamů	záznam	k1gInPc2	záznam
a	a	k8xC	a
přenosů	přenos	k1gInPc2	přenos
památek	památka	k1gFnPc2	památka
do	do	k7c2	do
pořadu	pořad	k1gInSc2	pořad
Panorama	panorama	k1gNnSc1	panorama
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
městem	město	k1gNnSc7	město
Třebíčí	Třebíč	k1gFnPc2	Třebíč
získala	získat	k5eAaPmAgFnS	získat
tentýž	týž	k3xTgInSc4	týž
grant	grant	k1gInSc4	grant
i	i	k8xC	i
města	město	k1gNnSc2	město
Telč	Telč	k1gFnSc1	Telč
a	a	k8xC	a
Žďár	Žďár	k1gInSc1	Žďár
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
<g/>
.	.	kIx.	.
</s>
<s>
Vysílání	vysílání	k1gNnSc1	vysílání
probíhalo	probíhat	k5eAaImAgNnS	probíhat
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2012	[number]	k4	2012
do	do	k7c2	do
konce	konec	k1gInSc2	konec
října	říjen	k1gInSc2	říjen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Záznamy	záznam	k1gInPc1	záznam
byly	být	k5eAaImAgInP	být
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
na	na	k7c6	na
webových	webový	k2eAgFnPc6d1	webová
stránkách	stránka	k1gFnPc6	stránka
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
kraje	kraj	k1gInSc2	kraj
<g/>
,	,	kIx,	,
organizace	organizace	k1gFnPc1	organizace
UNESCO	UNESCO	kA	UNESCO
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
organizací	organizace	k1gFnPc2	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Pořad	pořad	k1gInSc4	pořad
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
Toulavá	toulavý	k2eAgFnSc1d1	Toulavá
kamera	kamera	k1gFnSc1	kamera
natočil	natočit	k5eAaBmAgInS	natočit
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2016	[number]	k4	2016
reportáž	reportáž	k1gFnSc1	reportáž
o	o	k7c6	o
přeměně	přeměna	k1gFnSc6	přeměna
areálu	areál	k1gInSc2	areál
bývalých	bývalý	k2eAgFnPc2d1	bývalá
Baťových	Baťová	k1gFnPc2	Baťová
závodů	závod	k1gInPc2	závod
a	a	k8xC	a
science	science	k1gFnSc2	science
centru	centr	k1gInSc2	centr
Alternátor	alternátor	k1gInSc1	alternátor
<g/>
,	,	kIx,	,
třebíčský	třebíčský	k2eAgMnSc1d1	třebíčský
rodák	rodák	k1gMnSc1	rodák
reportér	reportér	k1gMnSc1	reportér
Pavel	Pavel	k1gMnSc1	Pavel
Horký	Horký	k1gMnSc1	Horký
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
pořad	pořad	k1gInSc1	pořad
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
vysílat	vysílat	k5eAaImF	vysílat
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
března	březen	k1gInSc2	březen
a	a	k8xC	a
dubna	duben	k1gInSc2	duben
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgFnP	konat
oslavy	oslava	k1gFnPc1	oslava
15	[number]	k4	15
let	léto	k1gNnPc2	léto
od	od	k7c2	od
vstupu	vstup	k1gInSc2	vstup
památek	památka	k1gFnPc2	památka
ve	v	k7c6	v
městě	město	k1gNnSc6	město
do	do	k7c2	do
seznamu	seznam	k1gInSc2	seznam
UNESCO	Unesco	k1gNnSc1	Unesco
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
uspořádáná	uspořádáný	k2eAgFnSc1d1	uspořádáná
odborná	odborný	k2eAgFnSc1d1	odborná
konference	konference	k1gFnSc1	konference
a	a	k8xC	a
navázala	navázat	k5eAaPmAgFnS	navázat
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
výstava	výstava	k1gFnSc1	výstava
a	a	k8xC	a
koncert	koncert	k1gInSc1	koncert
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2019	[number]	k4	2019
byly	být	k5eAaImAgFnP	být
obnoveny	obnoven	k2eAgFnPc1d1	obnovena
tabulky	tabulka	k1gFnPc1	tabulka
na	na	k7c6	na
památkách	památka	k1gFnPc6	památka
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc7	jejich
součástí	součást	k1gFnSc7	součást
jsou	být	k5eAaImIp3nP	být
QR	QR	kA	QR
kódy	kód	k1gInPc7	kód
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Doprava	doprava	k1gFnSc1	doprava
==	==	k?	==
</s>
</p>
<p>
<s>
Již	již	k6eAd1	již
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
vedly	vést	k5eAaImAgInP	vést
v	v	k7c6	v
blízkém	blízký	k2eAgNnSc6d1	blízké
okolí	okolí	k1gNnSc6	okolí
Třebíče	Třebíč	k1gFnSc2	Třebíč
významné	významný	k2eAgFnSc2d1	významná
obchodní	obchodní	k2eAgFnSc2d1	obchodní
stezky	stezka	k1gFnSc2	stezka
<g/>
:	:	kIx,	:
Haberská	Haberský	k2eAgFnSc1d1	Haberská
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vedla	vést	k5eAaImAgFnS	vést
přes	přes	k7c4	přes
Čáslavice	Čáslavice	k1gFnPc4	Čáslavice
<g/>
,	,	kIx,	,
Kojetice	Kojetika	k1gFnSc6	Kojetika
<g/>
,	,	kIx,	,
Stařeč	Stařeč	k1gInSc1	Stařeč
<g/>
,	,	kIx,	,
Přibyslavice	Přibyslavice	k1gFnSc1	Přibyslavice
a	a	k8xC	a
Brtnici	Brtnice	k1gFnSc3	Brtnice
<g/>
,	,	kIx,	,
v	v	k7c6	v
Brtnici	Brtnice	k1gFnSc6	Brtnice
se	se	k3xPyFc4	se
křížila	křížit	k5eAaImAgFnS	křížit
se	s	k7c7	s
stezkou	stezka	k1gFnSc7	stezka
Lovětínskou	Lovětínský	k2eAgFnSc7d1	Lovětínská
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
zvanou	zvaný	k2eAgFnSc4d1	zvaná
i	i	k8xC	i
Třebíčská	třebíčský	k2eAgFnSc1d1	Třebíčská
stezka	stezka	k1gFnSc1	stezka
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
vedla	vést	k5eAaImAgFnS	vést
od	od	k7c2	od
starého	starý	k2eAgNnSc2d1	staré
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
Zastávku	zastávka	k1gFnSc4	zastávka
u	u	k7c2	u
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
Vladislav	Vladislav	k1gMnSc1	Vladislav
<g/>
,	,	kIx,	,
Trnavu	Trnava	k1gFnSc4	Trnava
a	a	k8xC	a
končila	končit	k5eAaImAgFnS	končit
v	v	k7c6	v
Lovětíně	Lovětína	k1gFnSc6	Lovětína
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
důležitou	důležitý	k2eAgFnSc7d1	důležitá
křižovatkou	křižovatka	k1gFnSc7	křižovatka
stezek	stezka	k1gFnPc2	stezka
byl	být	k5eAaImAgInS	být
tehdejší	tehdejší	k2eAgInSc1d1	tehdejší
Brod	Brod	k1gInSc1	Brod
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
nacházel	nacházet	k5eAaImAgMnS	nacházet
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
dnešní	dnešní	k2eAgMnSc1d1	dnešní
Vladislav	Vladislav	k1gMnSc1	Vladislav
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
křížily	křížit	k5eAaImAgFnP	křížit
stezky	stezka	k1gFnPc1	stezka
Libická	Libická	k1gFnSc1	Libická
a	a	k8xC	a
Lovětínská	Lovětínský	k2eAgFnSc1d1	Lovětínská
<g/>
.	.	kIx.	.
<g/>
Nyní	nyní	k6eAd1	nyní
je	být	k5eAaImIp3nS	být
Třebíč	Třebíč	k1gFnSc4	Třebíč
dopravním	dopravní	k2eAgInSc7d1	dopravní
uzlem	uzel	k1gInSc7	uzel
okresního	okresní	k2eAgInSc2d1	okresní
významu	význam	k1gInSc2	význam
<g/>
.	.	kIx.	.
</s>
<s>
Městem	město	k1gNnSc7	město
prochází	procházet	k5eAaImIp3nS	procházet
silnice	silnice	k1gFnSc1	silnice
první	první	k4xOgFnSc2	první
třídy	třída	k1gFnSc2	třída
č.	č.	k?	č.
23	[number]	k4	23
(	(	kIx(	(
<g/>
Písek	Písek	k1gInSc1	Písek
–	–	k?	–
Brno	Brno	k1gNnSc1	Brno
<g/>
)	)	kIx)	)
a	a	k8xC	a
krajská	krajský	k2eAgFnSc1d1	krajská
páteřní	páteřní	k2eAgFnSc1d1	páteřní
komunikace	komunikace	k1gFnSc1	komunikace
druhé	druhý	k4xOgFnSc2	druhý
třídy	třída	k1gFnSc2	třída
č.	č.	k?	č.
360	[number]	k4	360
spojující	spojující	k2eAgInSc4d1	spojující
město	město	k1gNnSc1	město
s	s	k7c7	s
dálnici	dálnice	k1gFnSc3	dálnice
D1	D1	k1gFnPc7	D1
u	u	k7c2	u
Velkého	velký	k2eAgNnSc2d1	velké
Meziříčí	Meziříčí	k1gNnSc2	Meziříčí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těsné	těsný	k2eAgFnSc6d1	těsná
blízkosti	blízkost	k1gFnSc6	blízkost
města	město	k1gNnSc2	město
odbočuje	odbočovat	k5eAaImIp3nS	odbočovat
ze	z	k7c2	z
silnice	silnice	k1gFnSc2	silnice
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
23	[number]	k4	23
silnice	silnice	k1gFnPc1	silnice
druhé	druhý	k4xOgFnSc2	druhý
třídy	třída	k1gFnSc2	třída
č.	č.	k?	č.
405	[number]	k4	405
spojující	spojující	k2eAgFnSc1d1	spojující
Třebíč	Třebíč	k1gFnSc1	Třebíč
s	s	k7c7	s
krajským	krajský	k2eAgNnSc7d1	krajské
městem	město	k1gNnSc7	město
Jihlavou	Jihlava	k1gFnSc7	Jihlava
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
prošly	projít	k5eAaPmAgFnP	projít
obě	dva	k4xCgFnPc4	dva
krajské	krajský	k2eAgFnPc4d1	krajská
komunikace	komunikace	k1gFnPc4	komunikace
výraznou	výrazný	k2eAgFnSc7d1	výrazná
rekonstrukcí	rekonstrukce	k1gFnSc7	rekonstrukce
<g/>
.	.	kIx.	.
<g/>
Železniční	železniční	k2eAgFnSc1d1	železniční
síť	síť	k1gFnSc1	síť
Třebíčí	Třebíč	k1gFnPc2	Třebíč
vede	vést	k5eAaImIp3nS	vést
ve	v	k7c6	v
východozápadním	východozápadní	k2eAgInSc6d1	východozápadní
směru	směr	k1gInSc6	směr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Třebíče	Třebíč	k1gFnSc2	Třebíč
je	být	k5eAaImIp3nS	být
nejvýznamnější	významný	k2eAgFnSc1d3	nejvýznamnější
železnice	železnice	k1gFnSc1	železnice
č.	č.	k?	č.
240	[number]	k4	240
z	z	k7c2	z
Jihlavy	Jihlava	k1gFnSc2	Jihlava
do	do	k7c2	do
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
železniční	železniční	k2eAgFnSc1d1	železniční
stanice	stanice	k1gFnSc1	stanice
Třebíč	Třebíč	k1gFnSc1	Třebíč
a	a	k8xC	a
zastávka	zastávka	k1gFnSc1	zastávka
Třebíč-Borovina	Třebíč-Borovina	k1gFnSc1	Třebíč-Borovina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
přestupní	přestupní	k2eAgInSc1d1	přestupní
terminál	terminál	k1gInSc1	terminál
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
uveden	uvést	k5eAaPmNgInS	uvést
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Poblíž	poblíž	k7c2	poblíž
města	město	k1gNnSc2	město
také	také	k9	také
vede	vést	k5eAaImIp3nS	vést
trať	trať	k1gFnSc1	trať
č.	č.	k?	č.
241	[number]	k4	241
z	z	k7c2	z
Okříšek	Okříšek	k1gMnSc1	Okříšek
do	do	k7c2	do
Znojma	Znojmo	k1gNnSc2	Znojmo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
sportovní	sportovní	k2eAgNnSc1d1	sportovní
letiště	letiště	k1gNnSc1	letiště
Západomoravského	západomoravský	k2eAgInSc2d1	západomoravský
aeroklubu	aeroklub	k1gInSc2	aeroklub
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
funguje	fungovat	k5eAaImIp3nS	fungovat
městská	městský	k2eAgFnSc1d1	městská
hromadná	hromadný	k2eAgFnSc1d1	hromadná
doprava	doprava	k1gFnSc1	doprava
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
městě	město	k1gNnSc6	město
zajišťována	zajišťovat	k5eAaImNgFnS	zajišťovat
devíti	devět	k4xCc7	devět
linkami	linka	k1gFnPc7	linka
autobusů	autobus	k1gInPc2	autobus
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
také	také	k6eAd1	také
plánovalo	plánovat	k5eAaImAgNnS	plánovat
zavedení	zavedení	k1gNnSc4	zavedení
trolejbusů	trolejbus	k1gInPc2	trolejbus
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
ekologického	ekologický	k2eAgNnSc2d1	ekologické
hlediska	hledisko	k1gNnSc2	hledisko
ji	on	k3xPp3gFnSc4	on
dnes	dnes	k6eAd1	dnes
nahrazují	nahrazovat	k5eAaImIp3nP	nahrazovat
plynové	plynový	k2eAgInPc1d1	plynový
autobusy	autobus	k1gInPc1	autobus
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
je	být	k5eAaImIp3nS	být
postaveno	postaven	k2eAgNnSc4d1	postaveno
velké	velký	k2eAgNnSc4d1	velké
autobusové	autobusový	k2eAgNnSc4d1	autobusové
nádraží	nádraží	k1gNnSc4	nádraží
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
modernizováno	modernizovat	k5eAaBmNgNnS	modernizovat
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
a	a	k8xC	a
okolí	okolí	k1gNnSc6	okolí
existují	existovat	k5eAaImIp3nP	existovat
cyklostezky	cyklostezka	k1gFnPc4	cyklostezka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
bylo	být	k5eAaImAgNnS	být
veřejnou	veřejný	k2eAgFnSc7d1	veřejná
vyhláškou	vyhláška	k1gFnSc7	vyhláška
oznámeno	oznámen	k2eAgNnSc4d1	oznámeno
zahájení	zahájení	k1gNnSc4	zahájení
stavebního	stavební	k2eAgNnSc2d1	stavební
řízení	řízení	k1gNnSc2	řízení
cyklostezky	cyklostezka	k1gFnSc2	cyklostezka
Jihlava	Jihlava	k1gFnSc1	Jihlava
–	–	k?	–
Třebíč	Třebíč	k1gFnSc1	Třebíč
–	–	k?	–
Raabs	Raabs	k1gInSc1	Raabs
an	an	k?	an
der	drát	k5eAaImRp2nS	drát
Thaya	Thay	k1gInSc2	Thay
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
stavbu	stavba	k1gFnSc4	stavba
provázely	provázet	k5eAaImAgInP	provázet
problémy	problém	k1gInPc1	problém
s	s	k7c7	s
odkupem	odkup	k1gInSc7	odkup
pozemků	pozemek	k1gInPc2	pozemek
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
cyklostezka	cyklostezka	k1gFnSc1	cyklostezka
nese	nést	k5eAaImIp3nS	nést
název	název	k1gInSc4	název
Třebíčský	třebíčský	k2eAgInSc4d1	třebíčský
okruh	okruh	k1gInSc4	okruh
a	a	k8xC	a
začíná	začínat	k5eAaImIp3nS	začínat
i	i	k8xC	i
končí	končit	k5eAaImIp3nS	končit
na	na	k7c6	na
tzv.	tzv.	kA	tzv.
Polance	polanka	k1gFnSc6	polanka
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
cyklostezka	cyklostezka	k1gFnSc1	cyklostezka
je	být	k5eAaImIp3nS	být
výhledově	výhledově	k6eAd1	výhledově
naplánovaná	naplánovaný	k2eAgFnSc1d1	naplánovaná
na	na	k7c4	na
trasu	trasa	k1gFnSc4	trasa
mezi	mezi	k7c7	mezi
Třebíčí	Třebíč	k1gFnSc7	Třebíč
a	a	k8xC	a
Dukovany	Dukovany	k1gInPc1	Dukovany
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
vést	vést	k5eAaImF	vést
souběžně	souběžně	k6eAd1	souběžně
s	s	k7c7	s
hlavní	hlavní	k2eAgFnSc7d1	hlavní
silnicí	silnice	k1gFnSc7	silnice
kolem	kolem	k7c2	kolem
Jaderné	jaderný	k2eAgFnSc2d1	jaderná
elektrárny	elektrárna	k1gFnSc2	elektrárna
Dukovany	Dukovany	k1gInPc1	Dukovany
<g/>
.	.	kIx.	.
<g/>
Třebíčí	Třebíč	k1gFnPc2	Třebíč
protéká	protékat	k5eAaImIp3nS	protékat
řeka	řeka	k1gFnSc1	řeka
Jihlava	Jihlava	k1gFnSc1	Jihlava
a	a	k8xC	a
současně	současně	k6eAd1	současně
i	i	k8xC	i
několik	několik	k4yIc4	několik
jejích	její	k3xOp3gInPc2	její
marginálních	marginální	k2eAgInPc2d1	marginální
přítoků	přítok	k1gInPc2	přítok
<g/>
,	,	kIx,	,
řeka	řeka	k1gFnSc1	řeka
byla	být	k5eAaImAgFnS	být
několikrát	několikrát	k6eAd1	několikrát
regulována	regulován	k2eAgFnSc1d1	regulována
<g/>
,	,	kIx,	,
naposledy	naposledy	k6eAd1	naposledy
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
<g/>
–	–	k?	–
<g/>
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
přistavěny	přistavěn	k2eAgFnPc4d1	přistavěna
protipovodňové	protipovodňový	k2eAgFnPc4d1	protipovodňová
hráze	hráz	k1gFnPc4	hráz
a	a	k8xC	a
další	další	k2eAgNnSc4d1	další
vybavení	vybavení	k1gNnSc4	vybavení
<g/>
.	.	kIx.	.
</s>
<s>
Vodní	vodní	k2eAgFnSc1d1	vodní
doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
neexistuje	existovat	k5eNaImIp3nS	existovat
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
době	doba	k1gFnSc6	doba
mezi	mezi	k7c7	mezi
válkami	válka	k1gFnPc7	válka
fungovala	fungovat	k5eAaImAgFnS	fungovat
říční	říční	k2eAgFnSc1d1	říční
výletní	výletní	k2eAgFnSc1d1	výletní
doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
u	u	k7c2	u
říčních	říční	k2eAgFnPc2d1	říční
lázní	lázeň	k1gFnPc2	lázeň
Polanka	polanka	k1gFnSc1	polanka
<g/>
.	.	kIx.	.
</s>
<s>
Antonín	Antonín	k1gMnSc1	Antonín
Uhlíř	Uhlíř	k1gMnSc1	Uhlíř
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc4	jenž
provozoval	provozovat	k5eAaImAgInS	provozovat
půjčovnu	půjčovna	k1gFnSc4	půjčovna
loděk	loďka	k1gFnPc2	loďka
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
postavil	postavit	k5eAaPmAgInS	postavit
prám	prám	k1gInSc1	prám
s	s	k7c7	s
motorem	motor	k1gInSc7	motor
o	o	k7c6	o
nosnosti	nosnost	k1gFnSc6	nosnost
25	[number]	k4	25
osob	osoba	k1gFnPc2	osoba
a	a	k8xC	a
provozoval	provozovat	k5eAaImAgMnS	provozovat
tak	tak	k6eAd1	tak
říční	říční	k2eAgFnSc4d1	říční
dopravu	doprava	k1gFnSc4	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
zakoupeny	zakoupit	k5eAaPmNgFnP	zakoupit
i	i	k9	i
mobilní	mobilní	k2eAgFnPc1d1	mobilní
protipovodňové	protipovodňový	k2eAgFnPc1d1	protipovodňová
zábrany	zábrana	k1gFnPc1	zábrana
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
budou	být	k5eAaImBp3nP	být
instalovány	instalovat	k5eAaBmNgFnP	instalovat
na	na	k7c6	na
nově	nově	k6eAd1	nově
opravené	opravený	k2eAgFnSc3d1	opravená
opěrné	opěrný	k2eAgFnSc3d1	opěrná
zdi	zeď	k1gFnSc3	zeď
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
Jihlavy	Jihlava	k1gFnSc2	Jihlava
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
hrazení	hrazení	k1gNnSc4	hrazení
nedaleko	nedaleko	k7c2	nedaleko
tzv.	tzv.	kA	tzv.
Novodvorského	novodvorský	k2eAgInSc2d1	novodvorský
mostu	most	k1gInSc2	most
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
vysoké	vysoký	k2eAgNnSc1d1	vysoké
4,5	[number]	k4	4,5
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
asi	asi	k9	asi
150	[number]	k4	150
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
součástí	součást	k1gFnSc7	součást
hrazení	hrazení	k1gNnSc2	hrazení
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
protipovodňové	protipovodňový	k2eAgFnPc1d1	protipovodňová
dveře	dveře	k1gFnPc1	dveře
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
kanalizace	kanalizace	k1gFnSc2	kanalizace
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zapříčinila	zapříčinit	k5eAaPmAgFnS	zapříčinit
vyplavování	vyplavování	k1gNnSc3	vyplavování
židovské	židovský	k2eAgFnSc2d1	židovská
čtvrti	čtvrt	k1gFnSc2	čtvrt
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
nainstalovány	nainstalován	k2eAgFnPc1d1	nainstalována
zpětné	zpětný	k2eAgFnPc1d1	zpětná
klapky	klapka	k1gFnPc1	klapka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
tomuto	tento	k3xDgInSc3	tento
zabránit	zabránit	k5eAaPmF	zabránit
<g/>
.	.	kIx.	.
<g/>
Dle	dle	k7c2	dle
ČP	ČP	kA	ČP
Indexu	index	k1gInSc2	index
byla	být	k5eAaImAgFnS	být
Třebíč	Třebíč	k1gFnSc1	Třebíč
za	za	k7c4	za
první	první	k4xOgNnSc4	první
pololetí	pololetí	k1gNnSc4	pololetí
roku	rok	k1gInSc2	rok
2018	[number]	k4	2018
mezi	mezi	k7c7	mezi
okresními	okresní	k2eAgNnPc7d1	okresní
městy	město	k1gNnPc7	město
na	na	k7c6	na
prvním	první	k4xOgNnSc6	první
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
bezpečnosti	bezpečnost	k1gFnSc6	bezpečnost
silničního	silniční	k2eAgInSc2d1	silniční
provozu	provoz	k1gInSc2	provoz
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
se	se	k3xPyFc4	se
umístilo	umístit	k5eAaPmAgNnS	umístit
dalších	další	k2eAgNnPc2d1	další
16	[number]	k4	16
okresních	okresní	k2eAgNnPc2d1	okresní
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
znamená	znamenat	k5eAaImIp3nS	znamenat
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
během	během	k7c2	během
prvního	první	k4xOgNnSc2	první
čtvrtletí	čtvrtletí	k1gNnSc2	čtvrtletí
se	se	k3xPyFc4	se
na	na	k7c6	na
území	území	k1gNnSc6	území
města	město	k1gNnSc2	město
nestala	stát	k5eNaPmAgFnS	stát
žádná	žádný	k3yNgFnSc1	žádný
dopravní	dopravní	k2eAgFnSc1d1	dopravní
nehoda	nehoda	k1gFnSc1	nehoda
se	s	k7c7	s
smrtelným	smrtelný	k2eAgNnSc7d1	smrtelné
nebo	nebo	k8xC	nebo
těžkým	těžký	k2eAgNnSc7d1	těžké
zraněním	zranění	k1gNnSc7	zranění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hospodářství	hospodářství	k1gNnSc1	hospodářství
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Historie	historie	k1gFnSc1	historie
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Cechy	cech	k1gInPc4	cech
====	====	k?	====
</s>
</p>
<p>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
silně	silně	k6eAd1	silně
rozvíjely	rozvíjet	k5eAaImAgInP	rozvíjet
cechy	cech	k1gInPc1	cech
<g/>
,	,	kIx,	,
v	v	k7c6	v
přehledu	přehled	k1gInSc6	přehled
cechů	cech	k1gInPc2	cech
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1763	[number]	k4	1763
se	se	k3xPyFc4	se
již	již	k6eAd1	již
začíná	začínat	k5eAaImIp3nS	začínat
projevovat	projevovat	k5eAaImF	projevovat
větší	veliký	k2eAgInSc4d2	veliký
počet	počet	k1gInSc4	počet
zástupců	zástupce	k1gMnPc2	zástupce
soukenického	soukenický	k2eAgInSc2d1	soukenický
cechu	cech	k1gInSc2	cech
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
Karel	Karel	k1gMnSc1	Karel
Josef	Josef	k1gMnSc1	Josef
z	z	k7c2	z
Valdštejna	Valdštejno	k1gNnSc2	Valdštejno
založí	založit	k5eAaPmIp3nP	založit
manufakturní	manufakturní	k2eAgFnSc4d1	manufakturní
výrobu	výroba	k1gFnSc4	výroba
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
nakonec	nakonec	k6eAd1	nakonec
nedošlo	dojít	k5eNaPmAgNnS	dojít
a	a	k8xC	a
místní	místní	k2eAgMnPc1d1	místní
soukeníci	soukeník	k1gMnPc1	soukeník
nedokázali	dokázat	k5eNaPmAgMnP	dokázat
konkurovat	konkurovat	k5eAaImF	konkurovat
větším	veliký	k2eAgFnPc3d2	veliký
dílnám	dílna	k1gFnPc3	dílna
s	s	k7c7	s
manufakturním	manufakturní	k2eAgInSc7d1	manufakturní
provozem	provoz	k1gInSc7	provoz
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
Liberci	Liberec	k1gInSc6	Liberec
nebo	nebo	k8xC	nebo
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
již	již	k6eAd1	již
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
několik	několik	k4yIc4	několik
větších	veliký	k2eAgFnPc2d2	veliký
dílen	dílna	k1gFnPc2	dílna
koželužnického	koželužnický	k2eAgInSc2d1	koželužnický
cechu	cech	k1gInSc2	cech
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
to	ten	k3xDgNnSc1	ten
dílny	dílna	k1gFnPc1	dílna
Karla	Karel	k1gMnSc2	Karel
Budischowského	Budischowský	k1gMnSc2	Budischowský
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc4	jeho
bratra	bratr	k1gMnSc4	bratr
Františka	František	k1gMnSc4	František
Budischowského	Budischowský	k1gMnSc4	Budischowský
<g/>
,	,	kIx,	,
dílna	dílna	k1gFnSc1	dílna
Martina	Martina	k1gFnSc1	Martina
Hasska	Hasska	k1gFnSc1	Hasska
a	a	k8xC	a
Subakova	Subakův	k2eAgFnSc1d1	Subakova
továrna	továrna	k1gFnSc1	továrna
<g/>
,	,	kIx,	,
tyto	tento	k3xDgInPc1	tento
měly	mít	k5eAaImAgInP	mít
podle	podle	k7c2	podle
srovnání	srovnání	k1gNnSc2	srovnání
Jindřicha	Jindřich	k1gMnSc2	Jindřich
Chylíka	Chylík	k1gMnSc2	Chylík
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1841	[number]	k4	1841
vyrábět	vyrábět	k5eAaImF	vyrábět
dohromady	dohromady	k6eAd1	dohromady
90	[number]	k4	90
000	[number]	k4	000
kůží	kůže	k1gFnPc2	kůže
ročně	ročně	k6eAd1	ročně
<g/>
,	,	kIx,	,
brněnská	brněnský	k2eAgFnSc1d1	brněnská
největší	veliký	k2eAgFnSc1d3	veliký
továrna	továrna	k1gFnSc1	továrna
<g/>
,	,	kIx,	,
továrna	továrna	k1gFnSc1	továrna
Karla	Karel	k1gMnSc2	Karel
Ignáce	Ignác	k1gMnSc2	Ignác
Lettmayera	Lettmayer	k1gMnSc2	Lettmayer
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
srovnání	srovnání	k1gNnSc6	srovnání
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
rok	rok	k1gInSc4	rok
v	v	k7c4	v
tuto	tento	k3xDgFnSc4	tento
dobu	doba	k1gFnSc4	doba
vyrobit	vyrobit	k5eAaPmF	vyrobit
12	[number]	k4	12
000	[number]	k4	000
kůží	kůže	k1gFnPc2	kůže
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1906	[number]	k4	1906
prosperovala	prosperovat	k5eAaImAgFnS	prosperovat
i	i	k9	i
obuvnická	obuvnický	k2eAgFnSc1d1	obuvnická
továrna	továrna	k1gFnSc1	továrna
Cinkajzlova	Cinkajzlův	k2eAgFnSc1d1	Cinkajzlův
(	(	kIx(	(
<g/>
v	v	k7c6	v
matrikách	matrika	k1gFnPc6	matrika
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
i	i	k9	i
Zinkeiselova	Zinkeiselův	k2eAgFnSc1d1	Zinkeiselův
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Papírníctví	Papírníctví	k1gNnPc2	Papírníctví
a	a	k8xC	a
lihovarnictví	lihovarnictví	k1gNnPc2	lihovarnictví
====	====	k?	====
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
Vlastivědy	vlastivěda	k1gFnSc2	vlastivěda
moravské	moravský	k2eAgFnSc2d1	Moravská
v	v	k7c6	v
Novém	nový	k2eAgNnSc6d1	nové
městě	město	k1gNnSc6	město
stávala	stávat	k5eAaImAgFnS	stávat
papírna	papírna	k1gFnSc1	papírna
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
při	při	k7c6	při
povodni	povodeň	k1gFnSc6	povodeň
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1774	[number]	k4	1774
byla	být	k5eAaImAgFnS	být
úplně	úplně	k6eAd1	úplně
zničena	zničit	k5eAaPmNgFnS	zničit
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgInPc4d1	další
průmyslové	průmyslový	k2eAgInPc4d1	průmyslový
závody	závod	k1gInPc4	závod
měli	mít	k5eAaImAgMnP	mít
patřit	patřit	k5eAaImF	patřit
i	i	k9	i
lihovary	lihovar	k1gInPc1	lihovar
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
měly	mít	k5eAaImAgInP	mít
k	k	k7c3	k
datu	datum	k1gNnSc3	datum
vydání	vydání	k1gNnSc2	vydání
publikace	publikace	k1gFnSc2	publikace
(	(	kIx(	(
<g/>
rok	rok	k1gInSc1	rok
1906	[number]	k4	1906
<g/>
)	)	kIx)	)
být	být	k5eAaImF	být
příslušny	příslušny	k?	příslušny
ke	k	k7c3	k
každému	každý	k3xTgInSc3	každý
panskému	panský	k2eAgInSc3d1	panský
dvoru	dvůr	k1gInSc3	dvůr
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc4d1	další
odvětví	odvětví	k1gNnSc4	odvětví
byla	být	k5eAaImAgFnS	být
zastoupena	zastoupit	k5eAaPmNgFnS	zastoupit
těžením	těžení	k1gNnSc7	těžení
grafitové	grafitový	k2eAgFnSc2d1	grafitová
tuhy	tuha	k1gFnSc2	tuha
u	u	k7c2	u
Římova	Římov	k1gInSc2	Římov
v	v	k7c6	v
lokalitě	lokalita	k1gFnSc6	lokalita
Vísky	víska	k1gFnSc2	víska
<g/>
,	,	kIx,	,
důl	důl	k1gInSc1	důl
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
jmenovat	jmenovat	k5eAaBmF	jmenovat
Černý	černý	k2eAgInSc1d1	černý
důl	důl	k1gInSc1	důl
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
velké	velký	k2eAgInPc4d1	velký
závody	závod	k1gInPc4	závod
patřila	patřit	k5eAaImAgFnS	patřit
i	i	k9	i
velká	velký	k2eAgFnSc1d1	velká
papírna	papírna	k1gFnSc1	papírna
u	u	k7c2	u
Přibyslavic	Přibyslavice	k1gFnPc2	Přibyslavice
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejímž	jejíž	k3xOyRp3gInSc6	jejíž
místě	místo	k1gNnSc6	místo
měl	mít	k5eAaImAgInS	mít
stávat	stávat	k5eAaImF	stávat
železný	železný	k2eAgInSc1d1	železný
hamr	hamr	k1gInSc1	hamr
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc4	ten
měli	mít	k5eAaImAgMnP	mít
zakoupit	zakoupit	k5eAaPmF	zakoupit
a	a	k8xC	a
proměnit	proměnit	k5eAaPmF	proměnit
v	v	k7c4	v
papírnu	papírna	k1gFnSc4	papírna
manželé	manžel	k1gMnPc1	manžel
Schneiderovi	Schneiderův	k2eAgMnPc1d1	Schneiderův
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1846	[number]	k4	1846
měl	mít	k5eAaImAgMnS	mít
papírnu	papírna	k1gFnSc4	papírna
koupit	koupit	k5eAaPmF	koupit
Karel	Karel	k1gMnSc1	Karel
Fundulus	Fundulus	k1gMnSc1	Fundulus
z	z	k7c2	z
Třebíče	Třebíč	k1gFnSc2	Třebíč
<g/>
,	,	kIx,	,
v	v	k7c6	v
osmdesátých	osmdesátý	k4xOgNnPc6	osmdesátý
letech	léto	k1gNnPc6	léto
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
papírna	papírna	k1gFnSc1	papírna
rozšířena	rozšířen	k2eAgFnSc1d1	rozšířena
a	a	k8xC	a
papír	papír	k1gInSc1	papír
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
vyrábět	vyrábět	k5eAaImF	vyrábět
na	na	k7c6	na
moderních	moderní	k2eAgInPc6d1	moderní
strojích	stroj	k1gInPc6	stroj
ze	z	k7c2	z
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
mleto	mlít	k5eAaImNgNnS	mlít
v	v	k7c6	v
mlýnech	mlýn	k1gInPc6	mlýn
u	u	k7c2	u
Rokštejna	Rokštejn	k1gInSc2	Rokštejn
a	a	k8xC	a
u	u	k7c2	u
Sokolí	sokolí	k2eAgFnSc2d1	sokolí
<g/>
.	.	kIx.	.
</s>
<s>
Papírna	papírna	k1gFnSc1	papírna
v	v	k7c6	v
těch	ten	k3xDgInPc6	ten
letech	let	k1gInPc6	let
zaměstnávala	zaměstnávat	k5eAaImAgFnS	zaměstnávat
asi	asi	k9	asi
150	[number]	k4	150
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
velkým	velký	k2eAgInSc7d1	velký
závodem	závod	k1gInSc7	závod
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
přádelna	přádelna	k1gFnSc1	přádelna
u	u	k7c2	u
Vladislavi	Vladislaev	k1gFnSc6	Vladislaev
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yIgFnSc2	který
se	se	k3xPyFc4	se
postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
stala	stát	k5eAaPmAgFnS	stát
továrna	továrna	k1gFnSc1	továrna
na	na	k7c4	na
čokoládu	čokoláda	k1gFnSc4	čokoláda
a	a	k8xC	a
k	k	k7c3	k
roce	rok	k1gInSc6	rok
vydání	vydání	k1gNnSc2	vydání
publikace	publikace	k1gFnSc2	publikace
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
továrnou	továrna	k1gFnSc7	továrna
na	na	k7c4	na
umělá	umělý	k2eAgNnPc4d1	umělé
hnojiva	hnojivo	k1gNnPc4	hnojivo
a	a	k8xC	a
na	na	k7c4	na
klíh	klíh	k1gInSc4	klíh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Okříškách	Okříšek	k1gInPc6	Okříšek
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1892	[number]	k4	1892
Františkem	František	k1gMnSc7	František
Engelmannem	Engelmann	k1gMnSc7	Engelmann
z	z	k7c2	z
Brna	Brno	k1gNnSc2	Brno
postavena	postaven	k2eAgFnSc1d1	postavena
parní	parní	k2eAgFnSc1d1	parní
pila	pila	k1gFnSc1	pila
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
posléze	posléze	k6eAd1	posléze
přejít	přejít	k5eAaPmF	přejít
do	do	k7c2	do
majetku	majetek	k1gInSc2	majetek
velkostatku	velkostatek	k1gInSc2	velkostatek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
majetku	majetek	k1gInSc6	majetek
byl	být	k5eAaImAgInS	být
i	i	k9	i
lihovar	lihovar	k1gInSc1	lihovar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Rozvoj	rozvoj	k1gInSc1	rozvoj
průmyslu	průmysl	k1gInSc2	průmysl
====	====	k?	====
</s>
</p>
<p>
<s>
Průmyslu	průmysl	k1gInSc3	průmysl
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
dařit	dařit	k5eAaImF	dařit
až	až	k9	až
po	po	k7c6	po
dovedení	dovedení	k1gNnSc6	dovedení
železnice	železnice	k1gFnSc2	železnice
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1885	[number]	k4	1885
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
i	i	k9	i
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
různé	různý	k2eAgInPc4d1	různý
cechy	cech	k1gInPc4	cech
<g/>
,	,	kIx,	,
hlavně	hlavně	k6eAd1	hlavně
soukenický	soukenický	k2eAgMnSc1d1	soukenický
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
spojené	spojený	k2eAgFnPc1d1	spojená
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
cechem	cech	k1gInSc7	cech
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
se	se	k3xPyFc4	se
nacházelo	nacházet	k5eAaImAgNnS	nacházet
několik	několik	k4yIc1	několik
mlýnů	mlýn	k1gInPc2	mlýn
<g/>
,	,	kIx,	,
na	na	k7c6	na
Stařečském	Stařečský	k2eAgInSc6d1	Stařečský
potoce	potok	k1gInSc6	potok
ležely	ležet	k5eAaImAgFnP	ležet
Borovský	borovský	k2eAgInSc4d1	borovský
<g/>
,	,	kIx,	,
Janův	Janův	k2eAgInSc4d1	Janův
a	a	k8xC	a
Hluchý	hluchý	k2eAgInSc4d1	hluchý
<g/>
,	,	kIx,	,
na	na	k7c6	na
Jihlavce	Jihlavka	k1gFnSc6	Jihlavka
ležely	ležet	k5eAaImAgFnP	ležet
Poušův	Poušův	k2eAgInSc4d1	Poušův
<g/>
,	,	kIx,	,
Krajíčkův	Krajíčkův	k2eAgInSc4d1	Krajíčkův
<g/>
,	,	kIx,	,
Churavý	churavý	k2eAgInSc4d1	churavý
a	a	k8xC	a
Táborský	táborský	k2eAgInSc4d1	táborský
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
počátcích	počátek	k1gInPc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
byl	být	k5eAaImAgInS	být
i	i	k9	i
parní	parní	k2eAgInSc1d1	parní
pivovar	pivovar	k1gInSc1	pivovar
<g/>
,	,	kIx,	,
sladovka	sladovka	k1gFnSc1	sladovka
<g/>
,	,	kIx,	,
elektrárna	elektrárna	k1gFnSc1	elektrárna
<g/>
,	,	kIx,	,
lihovar	lihovar	k1gInSc1	lihovar
<g/>
,	,	kIx,	,
sodovkárny	sodovkárna	k1gFnPc1	sodovkárna
ZON	ZON	kA	ZON
<g/>
,	,	kIx,	,
octárna	octárna	k1gFnSc1	octárna
<g/>
,	,	kIx,	,
2	[number]	k4	2
likérky	likérek	k1gInPc1	likérek
<g/>
,	,	kIx,	,
2	[number]	k4	2
cihelny	cihelna	k1gFnPc4	cihelna
<g/>
,	,	kIx,	,
slévárna	slévárna	k1gFnSc1	slévárna
<g/>
,	,	kIx,	,
2	[number]	k4	2
strojírny	strojírna	k1gFnSc2	strojírna
<g/>
,	,	kIx,	,
2	[number]	k4	2
barvírny	barvírna	k1gFnSc2	barvírna
a	a	k8xC	a
knihtiskařství	knihtiskařství	k1gNnSc2	knihtiskařství
<g/>
,	,	kIx,	,
stavitel	stavitel	k1gMnSc1	stavitel
<g/>
,	,	kIx,	,
řezbář	řezbář	k1gMnSc1	řezbář
<g/>
,	,	kIx,	,
2	[number]	k4	2
sochaři	sochař	k1gMnPc7	sochař
<g/>
,	,	kIx,	,
2	[number]	k4	2
fotografové	fotograf	k1gMnPc5	fotograf
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
byly	být	k5eAaImAgInP	být
2	[number]	k4	2
velkoobchody	velkoobchod	k1gInPc1	velkoobchod
<g/>
,	,	kIx,	,
20	[number]	k4	20
kupců	kupec	k1gMnPc2	kupec
a	a	k8xC	a
2	[number]	k4	2
knihkupectví	knihkupectví	k1gNnPc2	knihkupectví
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
také	také	k6eAd1	také
operovali	operovat	k5eAaImAgMnP	operovat
bankovní	bankovní	k2eAgInPc4d1	bankovní
ústavy	ústav	k1gInPc4	ústav
<g/>
:	:	kIx,	:
Obecní	obecní	k2eAgFnSc1d1	obecní
spořitelna	spořitelna	k1gFnSc1	spořitelna
města	město	k1gNnSc2	město
Třebíče	Třebíč	k1gFnSc2	Třebíč
(	(	kIx(	(
<g/>
založená	založený	k2eAgFnSc1d1	založená
roku	rok	k1gInSc2	rok
1861	[number]	k4	1861
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
První	první	k4xOgFnSc1	první
občanská	občanský	k2eAgFnSc1d1	občanská
záložna	záložna	k1gFnSc1	záložna
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
(	(	kIx(	(
<g/>
založená	založený	k2eAgFnSc1d1	založená
roku	rok	k1gInSc2	rok
1874	[number]	k4	1874
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Trebitscher	Trebitschra	k1gFnPc2	Trebitschra
Vorschussverein	Vorschussvereina	k1gFnPc2	Vorschussvereina
(	(	kIx(	(
<g/>
založená	založený	k2eAgFnSc1d1	založená
roku	rok	k1gInSc2	rok
1871	[number]	k4	1871
<g/>
)	)	kIx)	)
a	a	k8xC	a
Rolnická	rolnický	k2eAgFnSc1d1	rolnická
záložna	záložna	k1gFnSc1	záložna
(	(	kIx(	(
<g/>
založená	založený	k2eAgFnSc1d1	založená
roku	rok	k1gInSc2	rok
1899	[number]	k4	1899
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výroční	výroční	k2eAgInPc1d1	výroční
trhy	trh	k1gInPc1	trh
se	se	k3xPyFc4	se
ve	v	k7c6	v
městě	město	k1gNnSc6	město
konaly	konat	k5eAaImAgFnP	konat
pouze	pouze	k6eAd1	pouze
při	při	k7c6	při
svátku	svátek	k1gInSc6	svátek
sv.	sv.	kA	sv.
Jiří	Jiří	k1gMnSc1	Jiří
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
a	a	k8xC	a
při	při	k7c6	při
svátku	svátek	k1gInSc6	svátek
sv.	sv.	kA	sv.
Bartoloměje	Bartoloměj	k1gMnSc2	Bartoloměj
<g/>
,	,	kIx,	,
dobytčí	dobytčí	k2eAgInPc1d1	dobytčí
trhy	trh	k1gInPc1	trh
se	se	k3xPyFc4	se
konali	konat	k5eAaImAgMnP	konat
každé	každý	k3xTgNnSc4	každý
třetí	třetí	k4xOgNnSc4	třetí
pondělí	pondělí	k1gNnSc4	pondělí
a	a	k8xC	a
městské	městský	k2eAgInPc4d1	městský
trhy	trh	k1gInPc4	trh
byly	být	k5eAaImAgFnP	být
každé	každý	k3xTgNnSc4	každý
pondělí	pondělí	k1gNnSc4	pondělí
a	a	k8xC	a
pátek	pátek	k1gInSc4	pátek
<g/>
.	.	kIx.	.
<g/>
Později	pozdě	k6eAd2	pozdě
svoji	svůj	k3xOyFgFnSc4	svůj
činnost	činnost	k1gFnSc4	činnost
ukončily	ukončit	k5eAaPmAgInP	ukončit
jak	jak	k8xS	jak
Subakova	Subakův	k2eAgFnSc1d1	Subakova
továrna	továrna	k1gFnSc1	továrna
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
továrna	továrna	k1gFnSc1	továrna
Martina	Martin	k1gMnSc2	Martin
Hasska	Hassek	k1gMnSc2	Hassek
<g/>
,	,	kIx,	,
i	i	k9	i
během	během	k7c2	během
první	první	k4xOgFnSc2	první
republiky	republika	k1gFnSc2	republika
se	se	k3xPyFc4	se
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
rozvíjel	rozvíjet	k5eAaImAgInS	rozvíjet
závod	závod	k1gInSc4	závod
Budischowských	Budischowský	k1gMnPc2	Budischowský
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
zakoupen	zakoupit	k5eAaPmNgInS	zakoupit
podnikatelem	podnikatel	k1gMnSc7	podnikatel
Tomášem	Tomáš	k1gMnSc7	Tomáš
Baťou	Baťa	k1gMnSc7	Baťa
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
rozšiřovat	rozšiřovat	k5eAaImF	rozšiřovat
závod	závod	k1gInSc4	závod
v	v	k7c6	v
Borovině	borovina	k1gFnSc6	borovina
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
budovy	budova	k1gFnPc1	budova
byly	být	k5eAaImAgFnP	být
postaveny	postavit	k5eAaPmNgFnP	postavit
podle	podle	k7c2	podle
návrhů	návrh	k1gInPc2	návrh
architektů	architekt	k1gMnPc2	architekt
Františka	František	k1gMnSc4	František
Gahury	Gahura	k1gFnSc2	Gahura
<g/>
,	,	kIx,	,
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Karfíka	Karfík	k1gMnSc2	Karfík
a	a	k8xC	a
Miroslava	Miroslav	k1gMnSc2	Miroslav
Lorence	Lorenc	k1gMnSc2	Lorenc
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
byly	být	k5eAaImAgInP	být
závody	závod	k1gInPc1	závod
zestátněny	zestátnit	k5eAaPmNgInP	zestátnit
a	a	k8xC	a
přejmenovány	přejmenovat	k5eAaPmNgInP	přejmenovat
na	na	k7c4	na
Závody	závod	k1gInPc4	závod
Gustava	Gustav	k1gMnSc2	Gustav
Klimenta	Kliment	k1gMnSc2	Kliment
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
na	na	k7c6	na
BOPO	BOPO	kA	BOPO
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
patřila	patřit	k5eAaImAgFnS	patřit
mezi	mezi	k7c4	mezi
největší	veliký	k2eAgInPc4d3	veliký
obuvnické	obuvnický	k2eAgInPc4d1	obuvnický
a	a	k8xC	a
punčochárenské	punčochárenský	k2eAgInPc4d1	punčochárenský
podniky	podnik	k1gInPc4	podnik
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
firma	firma	k1gFnSc1	firma
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
platební	platební	k2eAgFnSc2d1	platební
neschopnosti	neschopnost	k1gFnSc2	neschopnost
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
bylo	být	k5eAaImAgNnS	být
uvaleno	uvalit	k5eAaPmNgNnS	uvalit
konkursní	konkursní	k2eAgNnSc1d1	konkursní
řízení	řízení	k1gNnSc1	řízení
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
byla	být	k5eAaImAgFnS	být
firma	firma	k1gFnSc1	firma
uvedena	uvést	k5eAaPmNgFnS	uvést
do	do	k7c2	do
likvidace	likvidace	k1gFnSc2	likvidace
a	a	k8xC	a
majetek	majetek	k1gInSc1	majetek
je	být	k5eAaImIp3nS	být
prodáván	prodávat	k5eAaImNgInS	prodávat
na	na	k7c6	na
aukcích	aukce	k1gFnPc6	aukce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
zde	zde	k6eAd1	zde
boty	bota	k1gFnSc2	bota
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
Selva	Selva	k1gFnSc1	Selva
Shoes	Shoesa	k1gFnPc2	Shoesa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tradici	tradice	k1gFnSc6	tradice
výroby	výroba	k1gFnSc2	výroba
punčochového	punčochový	k2eAgNnSc2d1	punčochové
zboží	zboží	k1gNnSc2	zboží
navázala	navázat	k5eAaPmAgFnS	navázat
firma	firma	k1gFnSc1	firma
TREPON	TREPON	kA	TREPON
<g/>
,	,	kIx,	,
jediná	jediný	k2eAgFnSc1d1	jediná
ještě	ještě	k6eAd1	ještě
existující	existující	k2eAgFnSc1d1	existující
dceřiná	dceřiný	k2eAgFnSc1d1	dceřiná
společnost	společnost	k1gFnSc1	společnost
zaniklé	zaniklý	k2eAgFnSc2d1	zaniklá
firmy	firma	k1gFnSc2	firma
BOPO	BOPO	kA	BOPO
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
si	se	k3xPyFc3	se
František	František	k1gMnSc1	František
Noha	noh	k1gMnSc4	noh
otevřel	otevřít	k5eAaPmAgMnS	otevřít
v	v	k7c6	v
Nádražní	nádražní	k2eAgFnSc6d1	nádražní
ulici	ulice	k1gFnSc6	ulice
(	(	kIx(	(
<g/>
nedaleko	nedaleko	k7c2	nedaleko
nemocnice	nemocnice	k1gFnSc2	nemocnice
<g/>
)	)	kIx)	)
zámečnickou	zámečnický	k2eAgFnSc4d1	zámečnická
výrobu	výroba	k1gFnSc4	výroba
ocelového	ocelový	k2eAgInSc2d1	ocelový
<g/>
,	,	kIx,	,
chromovaného	chromovaný	k2eAgInSc2d1	chromovaný
<g/>
,	,	kIx,	,
niklovaného	niklovaný	k2eAgInSc2d1	niklovaný
a	a	k8xC	a
lakovaného	lakovaný	k2eAgInSc2d1	lakovaný
nábytku	nábytek	k1gInSc2	nábytek
(	(	kIx(	(
<g/>
bytového	bytový	k2eAgMnSc2d1	bytový
<g/>
,	,	kIx,	,
kancelářského	kancelářský	k2eAgInSc2d1	kancelářský
<g/>
,	,	kIx,	,
školního	školní	k2eAgInSc2d1	školní
<g/>
,	,	kIx,	,
zahradního	zahradní	k2eAgInSc2d1	zahradní
i	i	k8xC	i
sanitárního	sanitární	k2eAgInSc2d1	sanitární
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
vyráběl	vyrábět	k5eAaImAgInS	vyrábět
i	i	k9	i
dětské	dětský	k2eAgInPc4d1	dětský
vozíky	vozík	k1gInPc4	vozík
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zřízení	zřízení	k1gNnSc6	zřízení
protektorátu	protektorát	k1gInSc2	protektorát
zámečnická	zámečnický	k2eAgFnSc1d1	zámečnická
dílna	dílna	k1gFnSc1	dílna
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
přestěhoval	přestěhovat	k5eAaPmAgInS	přestěhovat
na	na	k7c6	na
tř	tř	kA	tř
<g/>
.	.	kIx.	.
Bráfova	Bráfův	k2eAgInSc2d1	Bráfův
čp.	čp.	k?	čp.
595	[number]	k4	595
(	(	kIx(	(
<g/>
přejmenovanou	přejmenovaný	k2eAgFnSc7d1	přejmenovaná
r.	r.	kA	r.
1946	[number]	k4	1946
na	na	k7c4	na
třídu	třída	k1gFnSc4	třída
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
E.	E.	kA	E.
Beneše	Beneš	k1gMnSc2	Beneš
<g/>
,	,	kIx,	,
r.	r.	kA	r.
1948	[number]	k4	1948
třída	třída	k1gFnSc1	třída
Julia	Julius	k1gMnSc2	Julius
Fučíka	Fučík	k1gMnSc2	Fučík
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
výroba	výroba	k1gFnSc1	výroba
dětských	dětský	k2eAgInPc2d1	dětský
kočárků	kočárek	k1gInPc2	kočárek
byla	být	k5eAaImAgFnS	být
důvodem	důvod	k1gInSc7	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
byl	být	k5eAaImAgInS	být
jeho	jeho	k3xOp3gInSc4	jeho
závod	závod	k1gInSc4	závod
po	po	k7c6	po
znárodnění	znárodnění	k1gNnSc6	znárodnění
zařazen	zařazen	k2eAgInSc1d1	zařazen
pod	pod	k7c4	pod
národní	národní	k2eAgInSc4d1	národní
podnik	podnik	k1gInSc4	podnik
Radovan	Radovana	k1gFnPc2	Radovana
v	v	k7c6	v
Mělníku	Mělník	k1gInSc6	Mělník
a	a	k8xC	a
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
pod	pod	k7c4	pod
národní	národní	k2eAgInSc4d1	národní
podnik	podnik	k1gInSc4	podnik
Liberta	Liberta	k1gFnSc1	Liberta
Mělník	Mělník	k1gInSc1	Mělník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
–	–	k?	–
<g/>
Národní	národní	k2eAgFnSc1d1	národní
obroda	obroda	k1gFnSc1	obroda
<g/>
,	,	kIx,	,
18.08	[number]	k4	18.08
<g/>
.1946	.1946	k4	.1946
</s>
</p>
<p>
<s>
====	====	k?	====
Strojírenství	strojírenství	k1gNnSc1	strojírenství
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
průmyslové	průmyslový	k2eAgFnSc6d1	průmyslová
zóně	zóna	k1gFnSc6	zóna
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
města	město	k1gNnSc2	město
působí	působit	k5eAaImIp3nP	působit
další	další	k2eAgFnPc1d1	další
společnosti	společnost	k1gFnPc1	společnost
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgFnPc4d1	jiná
tehdejší	tehdejší	k2eAgFnPc4d1	tehdejší
Západomoravské	západomoravský	k2eAgFnPc4d1	Západomoravská
strojírny	strojírna	k1gFnPc4	strojírna
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vyráběly	vyrábět	k5eAaImAgFnP	vyrábět
pletací	pletací	k2eAgInPc4d1	pletací
stroje	stroj	k1gInPc4	stroj
<g/>
,	,	kIx,	,
tato	tento	k3xDgFnSc1	tento
společnost	společnost	k1gFnSc1	společnost
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
transformací	transformace	k1gFnSc7	transformace
z	z	k7c2	z
tehdejších	tehdejší	k2eAgInPc2d1	tehdejší
Závodů	závod	k1gInPc2	závod
Gustava	Gustav	k1gMnSc2	Gustav
Klimenta	Kliment	k1gMnSc2	Kliment
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
Borovině	borovina	k1gFnSc6	borovina
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
byla	být	k5eAaImAgFnS	být
registrována	registrován	k2eAgFnSc1d1	registrována
ochranná	ochranný	k2eAgFnSc1d1	ochranná
známka	známka	k1gFnSc1	známka
UNIPLET	UNIPLET	kA	UNIPLET
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
společnost	společnost	k1gFnSc1	společnost
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
zařazena	zařadit	k5eAaPmNgFnS	zařadit
do	do	k7c2	do
společnosti	společnost	k1gFnSc2	společnost
Elitex	Elitex	k1gInSc1	Elitex
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
touto	tento	k3xDgFnSc7	tento
značkou	značka	k1gFnSc7	značka
společnost	společnost	k1gFnSc1	společnost
fungovala	fungovat	k5eAaImAgFnS	fungovat
a	a	k8xC	a
exportovala	exportovat	k5eAaBmAgFnS	exportovat
svoje	svůj	k3xOyFgInPc4	svůj
pletařské	pletařský	k2eAgInPc4d1	pletařský
a	a	k8xC	a
punčochářské	punčochářský	k2eAgInPc4d1	punčochářský
stroje	stroj	k1gInPc4	stroj
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
firma	firma	k1gFnSc1	firma
transformována	transformovat	k5eAaBmNgFnS	transformovat
na	na	k7c4	na
akciovou	akciový	k2eAgFnSc4d1	akciová
společnost	společnost	k1gFnSc4	společnost
UNIPLET	UNIPLET	kA	UNIPLET
a.	a.	k?	a.
s.	s.	k?	s.
a	a	k8xC	a
fungovala	fungovat	k5eAaImAgFnS	fungovat
dále	daleko	k6eAd2	daleko
<g/>
,	,	kIx,	,
po	po	k7c6	po
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
byla	být	k5eAaImAgFnS	být
prodána	prodat	k5eAaPmNgFnS	prodat
britské	britský	k2eAgMnPc4d1	britský
společnosti	společnost	k1gFnSc2	společnost
Monarch	Monarcha	k1gFnPc2	Monarcha
Knitting	Knitting	k1gInSc1	Knitting
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
společnost	společnost	k1gFnSc1	společnost
UNIPLET	UNIPLET	kA	UNIPLET
usilovala	usilovat	k5eAaImAgFnS	usilovat
o	o	k7c4	o
prodej	prodej	k1gInSc4	prodej
svých	svůj	k3xOyFgInPc2	svůj
pozemků	pozemek	k1gInPc2	pozemek
na	na	k7c6	na
Jejkově	Jejkův	k2eAgFnSc6d1	Jejkův
městu	město	k1gNnSc3	město
Třebíč	Třebíč	k1gFnSc1	Třebíč
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
územního	územní	k2eAgInSc2d1	územní
plánu	plán	k1gInSc2	plán
měl	mít	k5eAaImAgInS	mít
přes	přes	k7c4	přes
tyto	tento	k3xDgInPc4	tento
pozemky	pozemek	k1gInPc4	pozemek
vést	vést	k5eAaImF	vést
obchvat	obchvat	k1gInSc4	obchvat
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
prodej	prodej	k1gInSc1	prodej
se	se	k3xPyFc4	se
neuskutečnil	uskutečnit	k5eNaPmAgInS	uskutečnit
a	a	k8xC	a
společnost	společnost	k1gFnSc4	společnost
se	se	k3xPyFc4	se
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
žalovat	žalovat	k5eAaImF	žalovat
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
energetická	energetický	k2eAgFnSc1d1	energetická
společnost	společnost	k1gFnSc1	společnost
E.	E.	kA	E.
<g/>
On	on	k3xPp3gMnSc1	on
odpojila	odpojit	k5eAaPmAgFnS	odpojit
areál	areál	k1gInSc4	areál
společnosti	společnost	k1gFnSc2	společnost
od	od	k7c2	od
zdrojů	zdroj	k1gInPc2	zdroj
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
a	a	k8xC	a
způsobila	způsobit	k5eAaPmAgFnS	způsobit
tak	tak	k8xS	tak
škody	škoda	k1gFnSc2	škoda
firmám	firma	k1gFnPc3	firma
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
sídlí	sídlet	k5eAaImIp3nP	sídlet
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
na	na	k7c6	na
Jejkově	Jejkův	k2eAgNnSc6d1	Jejkův
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průmyslové	průmyslový	k2eAgFnSc6d1	průmyslová
zóně	zóna	k1gFnSc6	zóna
na	na	k7c6	na
Jejkově	Jejkův	k2eAgMnSc6d1	Jejkův
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
i	i	k9	i
akciová	akciový	k2eAgFnSc1d1	akciová
společnost	společnost	k1gFnSc1	společnost
PBS	PBS	kA	PBS
INDUSTRY	INDUSTRY	kA	INDUSTRY
<g/>
,	,	kIx,	,
její	její	k3xOp3gInPc1	její
počátky	počátek	k1gInPc1	počátek
sahají	sahat	k5eAaImIp3nP	sahat
do	do	k7c2	do
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byly	být	k5eAaImAgFnP	být
založeny	založen	k2eAgFnPc1d1	založena
společnosti	společnost	k1gFnPc1	společnost
Wallig	Walliga	k1gFnPc2	Walliga
a	a	k8xC	a
Benz	Benza	k1gFnPc2	Benza
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
vyráběly	vyrábět	k5eAaImAgFnP	vyrábět
kotle	kotel	k1gInPc4	kotel
<g/>
,	,	kIx,	,
motory	motor	k1gInPc4	motor
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
průmyslové	průmyslový	k2eAgInPc4d1	průmyslový
stroje	stroj	k1gInPc4	stroj
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
byly	být	k5eAaImAgFnP	být
tyto	tento	k3xDgFnPc1	tento
společnosti	společnost	k1gFnPc1	společnost
začleněny	začleněn	k2eAgFnPc1d1	začleněna
do	do	k7c2	do
koncernu	koncern	k1gInSc2	koncern
První	první	k4xOgFnSc2	první
brněnské	brněnský	k2eAgFnSc2d1	brněnská
strojírny	strojírna	k1gFnSc2	strojírna
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
sametové	sametový	k2eAgFnSc6d1	sametová
revoluci	revoluce	k1gFnSc6	revoluce
byla	být	k5eAaImAgFnS	být
firma	firma	k1gFnSc1	firma
převedena	převést	k5eAaPmNgFnS	převést
na	na	k7c4	na
akciovou	akciový	k2eAgFnSc4d1	akciová
společnost	společnost	k1gFnSc4	společnost
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
sametové	sametový	k2eAgFnSc6d1	sametová
revoluci	revoluce	k1gFnSc6	revoluce
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
otevřela	otevřít	k5eAaPmAgFnS	otevřít
svoji	svůj	k3xOyFgFnSc4	svůj
centrálu	centrála	k1gFnSc4	centrála
kanadská	kanadský	k2eAgFnSc1d1	kanadská
firma	firma	k1gFnSc1	firma
Computer	computer	k1gInSc1	computer
Accounting	Accounting	k1gInSc1	Accounting
Systems	Systems	k1gInSc4	Systems
<g/>
,	,	kIx,	,
otevření	otevření	k1gNnSc4	otevření
se	se	k3xPyFc4	se
účastnila	účastnit	k5eAaImAgFnS	účastnit
manželka	manželka	k1gFnSc1	manželka
majitele	majitel	k1gMnSc2	majitel
firmy	firma	k1gFnSc2	firma
a	a	k8xC	a
třebíčská	třebíčský	k2eAgFnSc1d1	Třebíčská
rodačka	rodačka	k1gFnSc1	rodačka
Martha	Marth	k1gMnSc2	Marth
Lacko	Lacko	k1gNnSc4	Lacko
<g/>
,	,	kIx,	,
starosta	starosta	k1gMnSc1	starosta
Windsoru	Windsor	k1gInSc2	Windsor
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
John	John	k1gMnSc1	John
Milson	Milson	k1gMnSc1	Milson
<g/>
,	,	kIx,	,
obchodní	obchodní	k2eAgFnSc1d1	obchodní
rada	rada	k1gFnSc1	rada
kanadského	kanadský	k2eAgNnSc2d1	kanadské
velvyslanectví	velvyslanectví	k1gNnSc2	velvyslanectví
Ross	Rossa	k1gFnPc2	Rossa
Miller	Miller	k1gMnSc1	Miller
<g/>
,	,	kIx,	,
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
starosta	starosta	k1gMnSc1	starosta
Třebíče	Třebíč	k1gFnSc2	Třebíč
Lubomír	Lubomír	k1gMnSc1	Lubomír
Vostal	Vostal	k1gMnSc1	Vostal
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
hosté	host	k1gMnPc1	host
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
na	na	k7c6	na
českém	český	k2eAgInSc6d1	český
trhu	trh	k1gInSc6	trh
prodávala	prodávat	k5eAaImAgFnS	prodávat
a	a	k8xC	a
spravovala	spravovat	k5eAaImAgFnS	spravovat
produkty	produkt	k1gInPc4	produkt
kanadské	kanadský	k2eAgFnSc2d1	kanadská
firmy	firma	k1gFnSc2	firma
Trillium	Trillium	k1gNnSc1	Trillium
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
byl	být	k5eAaImAgInS	být
schválen	schválit	k5eAaPmNgInS	schválit
prodej	prodej	k1gInSc1	prodej
pozemků	pozemek	k1gInPc2	pozemek
v	v	k7c6	v
Rafaelově	Rafaelův	k2eAgFnSc6d1	Rafaelova
ulici	ulice	k1gFnSc6	ulice
na	na	k7c6	na
severním	severní	k2eAgInSc6d1	severní
okraji	okraj	k1gInSc6	okraj
města	město	k1gNnSc2	město
společnosti	společnost	k1gFnSc2	společnost
Wera	Wera	k1gMnSc1	Wera
Werk	Werk	k1gMnSc1	Werk
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
postavit	postavit	k5eAaPmF	postavit
továranu	továrana	k1gFnSc4	továrana
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
nářadí	nářadí	k1gNnSc2	nářadí
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
by	by	kYmCp3nS	by
přinést	přinést	k5eAaPmF	přinést
celkem	celkem	k6eAd1	celkem
350	[number]	k4	350
pracovních	pracovní	k2eAgNnPc2d1	pracovní
míst	místo	k1gNnPc2	místo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
zóna	zóna	k1gFnSc1	zóna
Hrotovická	hrotovický	k2eAgFnSc1d1	Hrotovická
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
územním	územní	k2eAgInSc6d1	územní
plánu	plán	k1gInSc6	plán
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
o	o	k7c6	o
výstavbě	výstavba	k1gFnSc6	výstavba
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
zóny	zóna	k1gFnSc2	zóna
Hrotovická	hrotovický	k2eAgFnSc1d1	Hrotovická
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
výstavba	výstavba	k1gFnSc1	výstavba
inženýrských	inženýrský	k2eAgFnPc2d1	inženýrská
sítí	síť	k1gFnPc2	síť
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
byly	být	k5eAaImAgFnP	být
tyto	tento	k3xDgFnPc1	tento
úpravy	úprava	k1gFnPc1	úprava
dokončeny	dokončit	k5eAaPmNgFnP	dokončit
<g/>
.	.	kIx.	.
</s>
<s>
Celkové	celkový	k2eAgInPc1d1	celkový
náklady	náklad	k1gInPc1	náklad
na	na	k7c4	na
výstavbu	výstavba	k1gFnSc4	výstavba
samotné	samotný	k2eAgFnSc2d1	samotná
zóny	zóna	k1gFnSc2	zóna
přesáhly	přesáhnout	k5eAaPmAgInP	přesáhnout
75	[number]	k4	75
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
z	z	k7c2	z
velké	velká	k1gFnSc2	velká
části	část	k1gFnPc4	část
dotovány	dotován	k2eAgFnPc4d1	dotována
Ministerstvem	ministerstvo	k1gNnSc7	ministerstvo
pro	pro	k7c4	pro
místní	místní	k2eAgInSc4d1	místní
rozvoj	rozvoj	k1gInSc4	rozvoj
a	a	k8xC	a
Ministerstvem	ministerstvo	k1gNnSc7	ministerstvo
práce	práce	k1gFnSc2	práce
a	a	k8xC	a
obchodu	obchod	k1gInSc2	obchod
<g/>
,	,	kIx,	,
necelých	celý	k2eNgInPc2d1	necelý
23	[number]	k4	23
milionů	milion	k4xCgInPc2	milion
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
částky	částka	k1gFnSc2	částka
bylo	být	k5eAaImAgNnS	být
samotnou	samotný	k2eAgFnSc7d1	samotná
investicí	investice	k1gFnSc7	investice
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
průmyslové	průmyslový	k2eAgFnSc6d1	průmyslová
zóně	zóna	k1gFnSc6	zóna
investovalo	investovat	k5eAaBmAgNnS	investovat
sedm	sedm	k4xCc1	sedm
firem	firma	k1gFnPc2	firma
celkem	celek	k1gInSc7	celek
300	[number]	k4	300
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Celkovým	celkový	k2eAgMnSc7d1	celkový
počet	počet	k1gInSc4	počet
nových	nový	k2eAgNnPc2d1	nové
pracovních	pracovní	k2eAgNnPc2d1	pracovní
míst	místo	k1gNnPc2	místo
k	k	k7c3	k
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosinci	prosinec	k1gInSc3	prosinec
2007	[number]	k4	2007
byl	být	k5eAaImAgInS	být
77	[number]	k4	77
<g/>
,	,	kIx,	,
plánovaný	plánovaný	k2eAgInSc4d1	plánovaný
počet	počet	k1gInSc4	počet
nových	nový	k2eAgNnPc2d1	nové
pracovních	pracovní	k2eAgNnPc2d1	pracovní
míst	místo	k1gNnPc2	místo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
je	být	k5eAaImIp3nS	být
412	[number]	k4	412
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgMnSc7d3	veliký
investorem	investor	k1gMnSc7	investor
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
zóně	zóna	k1gFnSc6	zóna
byla	být	k5eAaImAgFnS	být
společnost	společnost	k1gFnSc1	společnost
TEDOM	TEDOM	kA	TEDOM
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
investovala	investovat	k5eAaBmAgFnS	investovat
200	[number]	k4	200
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
do	do	k7c2	do
závodu	závod	k1gInSc2	závod
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
motorových	motorový	k2eAgNnPc2d1	motorové
vozidel	vozidlo	k1gNnPc2	vozidlo
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
měla	mít	k5eAaImAgFnS	mít
nabídnout	nabídnout	k5eAaPmF	nabídnout
210	[number]	k4	210
nových	nový	k2eAgNnPc2d1	nové
pracovních	pracovní	k2eAgNnPc2d1	pracovní
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
druhým	druhý	k4xOgMnSc7	druhý
největším	veliký	k2eAgMnSc7d3	veliký
investorem	investor	k1gMnSc7	investor
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
zóně	zóna	k1gFnSc6	zóna
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
společnost	společnost	k1gFnSc1	společnost
S.	S.	kA	S.
<g/>
O.K.	O.K.	k1gFnSc1	O.K.
stavební	stavební	k2eAgFnSc1d1	stavební
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
investovala	investovat	k5eAaBmAgFnS	investovat
40	[number]	k4	40
milionů	milion	k4xCgInPc2	milion
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
měla	mít	k5eAaImAgFnS	mít
nabídnout	nabídnout	k5eAaPmF	nabídnout
110	[number]	k4	110
nových	nový	k2eAgNnPc2d1	nové
pracovních	pracovní	k2eAgNnPc2d1	pracovní
míst	místo	k1gNnPc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgMnPc7d1	další
investory	investor	k1gMnPc7	investor
jsou	být	k5eAaImIp3nP	být
(	(	kIx(	(
<g/>
řazení	řazení	k1gNnSc1	řazení
podle	podle	k7c2	podle
výše	výše	k1gFnSc2	výše
investice	investice	k1gFnSc1	investice
<g/>
)	)	kIx)	)
Envinet	Envinet	k1gInSc1	Envinet
<g/>
,	,	kIx,	,
DOMY	dům	k1gInPc1	dům
D.	D.	kA	D.
<g/>
N.	N.	kA	N.
<g/>
E.S	E.S	k1gMnSc1	E.S
<g/>
,	,	kIx,	,
UNICODE	UNICODE	kA	UNICODE
SYSTEMS	SYSTEMS	kA	SYSTEMS
<g/>
,	,	kIx,	,
SKAML	SKAML	kA	SKAML
a	a	k8xC	a
KOVO	kovo	k1gNnSc1	kovo
UNI	UNI	kA	UNI
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Současnost	současnost	k1gFnSc4	současnost
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
zájmové	zájmový	k2eAgNnSc1d1	zájmové
sdružení	sdružení	k1gNnSc1	sdružení
Rozvoj	rozvoj	k1gInSc1	rozvoj
Třebíčska	Třebíčsko	k1gNnSc2	Třebíčsko
<g/>
,	,	kIx,	,
města	město	k1gNnPc1	město
Třebíč	Třebíč	k1gFnSc1	Třebíč
<g/>
,	,	kIx,	,
Náměšť	Náměšť	k1gFnSc1	Náměšť
nad	nad	k7c7	nad
Oslavou	oslava	k1gFnSc7	oslava
a	a	k8xC	a
Jemnice	Jemnice	k1gFnSc1	Jemnice
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Západomoravskou	západomoravský	k2eAgFnSc7d1	Západomoravská
vysokou	vysoký	k2eAgFnSc7d1	vysoká
školou	škola	k1gFnSc7	škola
spustily	spustit	k5eAaPmAgFnP	spustit
projekt	projekt	k1gInSc1	projekt
Podnikatelských	podnikatelský	k2eAgInPc2d1	podnikatelský
inkubátorů	inkubátor	k1gInPc2	inkubátor
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
dalších	další	k2eAgNnPc2d1	další
let	léto	k1gNnPc2	léto
byli	být	k5eAaImAgMnP	být
zajišťováni	zajišťován	k2eAgMnPc1d1	zajišťován
investoři	investor	k1gMnPc1	investor
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
fondy	fond	k1gInPc7	fond
PHARE	PHARE	kA	PHARE
<g/>
,	,	kIx,	,
dalšími	další	k2eAgMnPc7d1	další
investory	investor	k1gMnPc4	investor
byla	být	k5eAaImAgNnP	být
města	město	k1gNnPc1	město
ze	z	k7c2	z
sdružení	sdružení	k1gNnPc2	sdružení
<g/>
,	,	kIx,	,
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
pro	pro	k7c4	pro
místní	místní	k2eAgInSc4d1	místní
rozvoj	rozvoj	k1gInSc4	rozvoj
a	a	k8xC	a
další	další	k2eAgInSc4d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Stavby	stavba	k1gFnPc1	stavba
osmi	osm	k4xCc2	osm
inkubátorů	inkubátor	k1gInPc2	inkubátor
byly	být	k5eAaImAgFnP	být
zahájeny	zahájit	k5eAaPmNgFnP	zahájit
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
stavby	stavba	k1gFnPc1	stavba
byly	být	k5eAaImAgFnP	být
dokončeny	dokončit	k5eAaPmNgFnP	dokončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
slavnostní	slavnostní	k2eAgNnSc4d1	slavnostní
otevření	otevření	k1gNnSc4	otevření
třebíčského	třebíčský	k2eAgInSc2d1	třebíčský
objektu	objekt	k1gInSc2	objekt
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
17	[number]	k4	17
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
existují	existovat	k5eAaImIp3nP	existovat
dva	dva	k4xCgInPc4	dva
objekty	objekt	k1gInPc4	objekt
tohoto	tento	k3xDgInSc2	tento
projektu	projekt	k1gInSc2	projekt
<g/>
,	,	kIx,	,
první	první	k4xOgInSc1	první
podnikatelský	podnikatelský	k2eAgInSc1d1	podnikatelský
inkubátor	inkubátor	k1gInSc1	inkubátor
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
v	v	k7c6	v
prostorách	prostora	k1gFnPc6	prostora
bývalého	bývalý	k2eAgInSc2d1	bývalý
zámeckého	zámecký	k2eAgInSc2d1	zámecký
velkostatku	velkostatek	k1gInSc2	velkostatek
třebíčského	třebíčský	k2eAgInSc2d1	třebíčský
zámku	zámek	k1gInSc2	zámek
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc7	jeho
součástí	součást	k1gFnSc7	součást
jsou	být	k5eAaImIp3nP	být
skladovací	skladovací	k2eAgInPc1d1	skladovací
prostory	prostor	k1gInPc1	prostor
a	a	k8xC	a
dvacet	dvacet	k4xCc1	dvacet
buněk	buňka	k1gFnPc2	buňka
pro	pro	k7c4	pro
podnikatelskou	podnikatelský	k2eAgFnSc4d1	podnikatelská
činnost	činnost	k1gFnSc4	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgInSc7	druhý
objektem	objekt	k1gInSc7	objekt
je	být	k5eAaImIp3nS	být
podnikatelské	podnikatelský	k2eAgNnSc1d1	podnikatelské
a	a	k8xC	a
výzkumné	výzkumný	k2eAgNnSc1d1	výzkumné
centrum	centrum	k1gNnSc1	centrum
s	s	k7c7	s
přednáškovými	přednáškový	k2eAgInPc7d1	přednáškový
sály	sál	k1gInPc7	sál
<g/>
,	,	kIx,	,
konferenčními	konferenční	k2eAgFnPc7d1	konferenční
místnostmi	místnost	k1gFnPc7	místnost
a	a	k8xC	a
dalšími	další	k2eAgInPc7d1	další
prostory	prostor	k1gInPc7	prostor
v	v	k7c6	v
části	část	k1gFnSc6	část
Borovina	borovina	k1gFnSc1	borovina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgFnPc4d1	další
třebíčské	třebíčský	k2eAgFnPc4d1	Třebíčská
společnosti	společnost	k1gFnPc4	společnost
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
koncern	koncern	k1gInSc1	koncern
TIPA	TIPA	kA	TIPA
<g/>
,	,	kIx,	,
pod	pod	k7c4	pod
tento	tento	k3xDgInSc4	tento
koncern	koncern	k1gInSc1	koncern
patří	patřit	k5eAaImIp3nS	patřit
společnosti	společnost	k1gFnSc3	společnost
potravinářské	potravinářský	k2eAgFnSc3d1	potravinářská
<g/>
,	,	kIx,	,
stavební	stavební	k2eAgFnSc3d1	stavební
a	a	k8xC	a
další	další	k2eAgFnSc3d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
podle	podle	k7c2	podle
ústavu	ústav	k1gInSc2	ústav
územního	územní	k2eAgInSc2d1	územní
rozvoje	rozvoj	k1gInSc2	rozvoj
byla	být	k5eAaImAgFnS	být
firma	firma	k1gFnSc1	firma
TIPA	TIPA	kA	TIPA
pátým	pátý	k4xOgNnSc7	pátý
nejvyšším	vysoký	k2eAgMnSc7d3	nejvyšší
zaměstnavatelem	zaměstnavatel	k1gMnSc7	zaměstnavatel
okresu	okres	k1gInSc2	okres
Třebíč	Třebíč	k1gFnSc1	Třebíč
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
větším	veliký	k2eAgMnPc3d2	veliký
zaměstnavatelům	zaměstnavatel	k1gMnPc3	zaměstnavatel
i	i	k8xC	i
její	její	k3xOp3gFnSc1	její
divize	divize	k1gFnSc1	divize
TIPA	TIPA	kA	TIPA
Frost	Frost	k1gFnSc1	Frost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
nad	nad	k7c7	nad
třebíčským	třebíčský	k2eAgInSc7d1	třebíčský
zámkem	zámek	k1gInSc7	zámek
vyrábí	vyrábět	k5eAaImIp3nP	vyrábět
mražené	mražený	k2eAgInPc1d1	mražený
výrobky	výrobek	k1gInPc1	výrobek
<g/>
.	.	kIx.	.
</s>
<s>
Potravinářský	potravinářský	k2eAgInSc1d1	potravinářský
průmysl	průmysl	k1gInSc1	průmysl
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
existuje	existovat	k5eAaImIp3nS	existovat
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1879	[number]	k4	1879
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
František	František	k1gMnSc1	František
Kubeš	Kubeš	k1gMnSc1	Kubeš
založil	založit	k5eAaPmAgMnS	založit
sodovkárnu	sodovkárna	k1gFnSc4	sodovkárna
ZON	ZON	kA	ZON
<g/>
,	,	kIx,	,
tato	tento	k3xDgFnSc1	tento
společnost	společnost	k1gFnSc1	společnost
sídlí	sídlet	k5eAaImIp3nS	sídlet
od	od	k7c2	od
svého	svůj	k3xOyFgInSc2	svůj
počátku	počátek	k1gInSc2	počátek
v	v	k7c6	v
objektech	objekt	k1gInPc6	objekt
na	na	k7c6	na
Stařečce	Stařečka	k1gFnSc6	Stařečka
<g/>
,	,	kIx,	,
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
byla	být	k5eAaImAgFnS	být
znárodněna	znárodnit	k5eAaPmNgFnS	znárodnit
a	a	k8xC	a
po	po	k7c6	po
sametové	sametový	k2eAgFnSc6d1	sametová
revoluci	revoluce	k1gFnSc6	revoluce
byla	být	k5eAaImAgFnS	být
vrácena	vrátit	k5eAaPmNgFnS	vrátit
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
Františka	František	k1gMnSc2	František
Kubeše	Kubeš	k1gMnSc2	Kubeš
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1978	[number]	k4	1978
na	na	k7c6	na
severu	sever	k1gInSc6	sever
města	město	k1gNnSc2	město
funguje	fungovat	k5eAaImIp3nS	fungovat
pekárna	pekárna	k1gFnSc1	pekárna
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
později	pozdě	k6eAd2	pozdě
přešla	přejít	k5eAaPmAgFnS	přejít
do	do	k7c2	do
majetku	majetek	k1gInSc2	majetek
holdingu	holding	k1gInSc2	holding
Agrofert	Agrofert	k1gInSc4	Agrofert
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
se	se	k3xPyFc4	se
spekuluje	spekulovat	k5eAaImIp3nS	spekulovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
uzavřena	uzavřít	k5eAaPmNgFnS	uzavřít
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
postavení	postavení	k1gNnSc2	postavení
nové	nový	k2eAgFnSc2d1	nová
pekárny	pekárna	k1gFnSc2	pekárna
v	v	k7c6	v
Rosicích	Rosice	k1gFnPc6	Rosice
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
bylo	být	k5eAaImAgNnS	být
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgMnS	mít
začít	začít	k5eAaPmF	začít
postupný	postupný	k2eAgInSc4d1	postupný
výkup	výkup	k1gInSc4	výkup
pozemků	pozemek	k1gInPc2	pozemek
na	na	k7c6	na
severu	sever	k1gInSc6	sever
města	město	k1gNnSc2	město
a	a	k8xC	a
transformace	transformace	k1gFnSc1	transformace
tohoto	tento	k3xDgInSc2	tento
prostoru	prostor	k1gInSc2	prostor
na	na	k7c4	na
další	další	k2eAgFnSc4d1	další
průmyslovou	průmyslový	k2eAgFnSc4d1	průmyslová
zónu	zóna	k1gFnSc4	zóna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
průmyslové	průmyslový	k2eAgFnSc6d1	průmyslová
zóně	zóna	k1gFnSc6	zóna
by	by	kYmCp3nS	by
měly	mít	k5eAaImAgFnP	mít
minimálně	minimálně	k6eAd1	minimálně
30	[number]	k4	30
pracovních	pracovní	k2eAgNnPc2d1	pracovní
míst	místo	k1gNnPc2	místo
otevřít	otevřít	k5eAaPmF	otevřít
firmy	firma	k1gFnPc4	firma
Autocentrum	autocentrum	k1gNnSc4	autocentrum
P	P	kA	P
<g/>
+	+	kIx~	+
<g/>
P	P	kA	P
servis	servis	k1gInSc1	servis
a	a	k8xC	a
Evotech	Evoto	k1gNnPc6	Evoto
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
také	také	k9	také
bylo	být	k5eAaImAgNnS	být
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
výrobní	výrobní	k2eAgFnSc4d1	výrobní
halu	hala	k1gFnSc4	hala
v	v	k7c6	v
průmyslové	průmyslový	k2eAgFnSc6d1	průmyslová
zóně	zóna	k1gFnSc6	zóna
Hrotovická	hrotovický	k2eAgFnSc1d1	Hrotovická
postaví	postavit	k5eAaPmIp3nS	postavit
společnost	společnost	k1gFnSc1	společnost
Jopp	Jopp	k1gInSc1	Jopp
Automotive	Automotiv	k1gInSc5	Automotiv
<g/>
,	,	kIx,	,
nabídne	nabídnout	k5eAaPmIp3nS	nabídnout
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
5	[number]	k4	5
let	léto	k1gNnPc2	léto
pracovní	pracovní	k2eAgNnPc1d1	pracovní
místa	místo	k1gNnPc1	místo
pro	pro	k7c4	pro
celkem	celkem	k6eAd1	celkem
200	[number]	k4	200
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
<g/>
Aktuálně	aktuálně	k6eAd1	aktuálně
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgFnPc4d3	nejvýznamnější
firmy	firma	k1gFnPc4	firma
na	na	k7c6	na
Třebíčsku	Třebíčsko	k1gNnSc6	Třebíčsko
MANN	Mann	k1gMnSc1	Mann
<g/>
+	+	kIx~	+
<g/>
HUMMEL	HUMMEL	kA	HUMMEL
Group	Group	k1gInSc1	Group
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
MANN	Mann	k1gMnSc1	Mann
<g/>
+	+	kIx~	+
<g/>
HUMMEL	HUMMEL	kA	HUMMEL
Group	Group	k1gMnSc1	Group
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
v	v	k7c6	v
německém	německý	k2eAgInSc6d1	německý
Ludwigsburgu	Ludwigsburg	k1gInSc6	Ludwigsburg
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
předním	přední	k2eAgMnSc7d1	přední
světovým	světový	k2eAgMnSc7d1	světový
expertem	expert	k1gMnSc7	expert
na	na	k7c4	na
filtrační	filtrační	k2eAgInPc4d1	filtrační
systémy	systém	k1gInPc4	systém
<g/>
.	.	kIx.	.
</s>
<s>
Dodává	dodávat	k5eAaImIp3nS	dodávat
originální	originální	k2eAgInPc4d1	originální
sériové	sériový	k2eAgInPc4d1	sériový
výrobky	výrobek	k1gInPc4	výrobek
a	a	k8xC	a
řešení	řešení	k1gNnSc4	řešení
pro	pro	k7c4	pro
mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
automobilový	automobilový	k2eAgInSc4d1	automobilový
a	a	k8xC	a
strojírenský	strojírenský	k2eAgInSc4d1	strojírenský
průmysl	průmysl	k1gInSc4	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
zastupují	zastupovat	k5eAaImIp3nP	zastupovat
MANN	Mann	k1gMnSc1	Mann
<g/>
+	+	kIx~	+
<g/>
HUMMEL	HUMMEL	kA	HUMMEL
Group	Group	k1gMnSc1	Group
tři	tři	k4xCgFnPc4	tři
společnosti	společnost	k1gFnPc1	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
Nové	Nové	k2eAgFnSc6d1	Nové
Vsi	ves	k1gFnSc6	ves
u	u	k7c2	u
Třebíče	Třebíč	k1gFnSc2	Třebíč
<g/>
:	:	kIx,	:
MANN	Mann	k1gMnSc1	Mann
<g/>
+	+	kIx~	+
<g/>
HUMMEL	HUMMEL	kA	HUMMEL
(	(	kIx(	(
<g/>
CZ	CZ	kA	CZ
<g/>
)	)	kIx)	)
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
a	a	k8xC	a
MANN	Mann	k1gMnSc1	Mann
<g/>
+	+	kIx~	+
<g/>
HUMMEL	HUMMEL	kA	HUMMEL
Service	Service	k1gFnSc2	Service
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
a	a	k8xC	a
zaměstnávají	zaměstnávat	k5eAaImIp3nP	zaměstnávat
dohromady	dohromady	k6eAd1	dohromady
přes	přes	k7c4	přes
1200	[number]	k4	1200
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
část	část	k1gFnSc1	část
společnosti	společnost	k1gFnSc2	společnost
MANN	Mann	k1gMnSc1	Mann
<g/>
+	+	kIx~	+
<g/>
HUMMEL	HUMMEL	kA	HUMMEL
Innenraumfilter	Innenraumfiltrum	k1gNnPc2	Innenraumfiltrum
CZ	CZ	kA	CZ
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
Uherském	uherský	k2eAgInSc6d1	uherský
Brodě	Brod	k1gInSc6	Brod
a	a	k8xC	a
zaměstnává	zaměstnávat	k5eAaImIp3nS	zaměstnávat
téměř	téměř	k6eAd1	téměř
300	[number]	k4	300
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
výrobním	výrobní	k2eAgInSc6d1	výrobní
závodě	závod	k1gInSc6	závod
MANN	Mann	k1gMnSc1	Mann
<g/>
+	+	kIx~	+
<g/>
HUMMEL	HUMMEL	kA	HUMMEL
(	(	kIx(	(
<g/>
CZ	CZ	kA	CZ
<g/>
)	)	kIx)	)
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
se	se	k3xPyFc4	se
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
kapalinové	kapalinový	k2eAgInPc1d1	kapalinový
a	a	k8xC	a
vzduchové	vzduchový	k2eAgInPc4d1	vzduchový
filtry	filtr	k1gInPc4	filtr
i	i	k8xC	i
další	další	k2eAgInPc4d1	další
komponenty	komponent	k1gInPc4	komponent
pro	pro	k7c4	pro
strojírenský	strojírenský	k2eAgInSc4d1	strojírenský
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
automobilový	automobilový	k2eAgInSc4d1	automobilový
průmysl	průmysl	k1gInSc4	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
MANN-FILTER	MANN-FILTER	k?	MANN-FILTER
je	být	k5eAaImIp3nS	být
nejsilnější	silný	k2eAgFnSc7d3	nejsilnější
zahraniční	zahraniční	k2eAgFnSc7d1	zahraniční
značkou	značka	k1gFnSc7	značka
filtrů	filtr	k1gInPc2	filtr
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
a	a	k8xC	a
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
a	a	k8xC	a
pod	pod	k7c7	pod
její	její	k3xOp3gFnSc7	její
značkou	značka	k1gFnSc7	značka
se	se	k3xPyFc4	se
v	v	k7c6	v
Nové	Nové	k2eAgFnSc6d1	Nové
Vsi	ves	k1gFnSc6	ves
vyrobí	vyrobit	k5eAaPmIp3nS	vyrobit
přibližně	přibližně	k6eAd1	přibližně
20	[number]	k4	20
milionů	milion	k4xCgInPc2	milion
filtrů	filtr	k1gInPc2	filtr
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Centrum	centrum	k1gNnSc1	centrum
sdílených	sdílený	k2eAgFnPc2d1	sdílená
služeb	služba	k1gFnPc2	služba
MANN	Mann	k1gMnSc1	Mann
<g/>
+	+	kIx~	+
<g/>
HUMMEL	HUMMEL	kA	HUMMEL
Service	Service	k1gFnSc2	Service
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
servisní	servisní	k2eAgFnPc4d1	servisní
služby	služba	k1gFnPc4	služba
pro	pro	k7c4	pro
ostatní	ostatní	k1gNnSc4	ostatní
pobočky	pobočka	k1gFnSc2	pobočka
MANN	Mann	k1gMnSc1	Mann
<g/>
+	+	kIx~	+
<g/>
HUMMEL	HUMMEL	kA	HUMMEL
Group	Group	k1gInSc1	Group
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
portfolia	portfolio	k1gNnSc2	portfolio
firmy	firma	k1gFnSc2	firma
patří	patřit	k5eAaImIp3nS	patřit
služby	služba	k1gFnPc4	služba
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
účetnictví	účetnictví	k1gNnSc2	účetnictví
a	a	k8xC	a
financí	finance	k1gFnPc2	finance
<g/>
,	,	kIx,	,
výzkumu	výzkum	k1gInSc2	výzkum
a	a	k8xC	a
vývoje	vývoj	k1gInSc2	vývoj
<g/>
,	,	kIx,	,
IT	IT	kA	IT
&	&	k?	&
SAP	sapa	k1gFnPc2	sapa
<g/>
,	,	kIx,	,
kvality	kvalita	k1gFnSc2	kvalita
<g/>
,	,	kIx,	,
lidských	lidský	k2eAgInPc2d1	lidský
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
,	,	kIx,	,
managementu	management	k1gInSc2	management
projektů	projekt	k1gInPc2	projekt
<g/>
,	,	kIx,	,
nákupu	nákup	k1gInSc2	nákup
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
společnosti	společnost	k1gFnSc2	společnost
sídlí	sídlet	k5eAaImIp3nS	sídlet
také	také	k9	také
v	v	k7c6	v
Technologickém	technologický	k2eAgInSc6d1	technologický
parku	park	k1gInSc6	park
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
bylo	být	k5eAaImAgNnS	být
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
energetická	energetický	k2eAgFnSc1d1	energetická
společnost	společnost	k1gFnSc1	společnost
ČEZ	ČEZ	kA	ČEZ
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
otevře	otevřít	k5eAaPmIp3nS	otevřít
call	call	k1gMnSc1	call
centrum	centrum	k1gNnSc4	centrum
a	a	k8xC	a
zaměstná	zaměstnat	k5eAaPmIp3nS	zaměstnat
60	[number]	k4	60
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Call	Call	k1gInSc4	Call
centrum	centrum	k1gNnSc1	centrum
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
v	v	k7c6	v
budovách	budova	k1gFnPc6	budova
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
bývalé	bývalý	k2eAgFnSc2d1	bývalá
továrny	továrna	k1gFnSc2	továrna
BOPO	BOPO	kA	BOPO
v	v	k7c6	v
Borovině	borovina	k1gFnSc6	borovina
a	a	k8xC	a
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
bylo	být	k5eAaImAgNnS	být
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
na	na	k7c4	na
minimální	minimální	k2eAgFnSc4d1	minimální
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
společnost	společnost	k1gFnSc1	společnost
S.	S.	kA	S.
<g/>
O.K.	O.K.	k1gFnSc1	O.K.
stavební	stavební	k2eAgFnSc1d1	stavební
nesplnila	splnit	k5eNaPmAgFnS	splnit
závazek	závazek	k1gInSc4	závazek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
dala	dát	k5eAaPmAgFnS	dát
městu	město	k1gNnSc3	město
-	-	kIx~	-
nebyla	být	k5eNaImAgFnS	být
schopna	schopen	k2eAgFnSc1d1	schopna
naplnit	naplnit	k5eAaPmF	naplnit
všech	všecek	k3xTgInPc2	všecek
115	[number]	k4	115
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
nově	nově	k6eAd1	nově
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
při	při	k7c6	při
stavbě	stavba	k1gFnSc6	stavba
budovy	budova	k1gFnSc2	budova
v	v	k7c6	v
průmyslové	průmyslový	k2eAgFnSc6d1	průmyslová
zóně	zóna	k1gFnSc6	zóna
Hrotovická	hrotovický	k2eAgFnSc1d1	Hrotovická
<g/>
.	.	kIx.	.
</s>
<s>
Požádala	požádat	k5eAaPmAgFnS	požádat
město	město	k1gNnSc4	město
o	o	k7c4	o
odložení	odložení	k1gNnSc4	odložení
lhůty	lhůta	k1gFnSc2	lhůta
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
bylo	být	k5eAaImAgNnS	být
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
bude	být	k5eAaImBp3nS	být
vybudováno	vybudovat	k5eAaPmNgNnS	vybudovat
soukromé	soukromý	k2eAgNnSc1d1	soukromé
zdravotní	zdravotní	k2eAgNnSc1d1	zdravotní
zařízení	zařízení	k1gNnSc1	zařízení
pro	pro	k7c4	pro
pacienty	pacient	k1gMnPc4	pacient
postižené	postižený	k2eAgMnPc4d1	postižený
Alzhemerovou	Alzhemerův	k2eAgFnSc7d1	Alzhemerův
nemocí	nemoc	k1gFnPc2	nemoc
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
investorem	investor	k1gMnSc7	investor
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
společnost	společnost	k1gFnSc4	společnost
Delta	delta	k1gNnPc2	delta
Capital	Capital	k1gMnSc1	Capital
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
za	za	k7c4	za
tento	tento	k3xDgInSc4	tento
projekt	projekt	k1gInSc4	projekt
před	před	k7c7	před
zastupitelstvem	zastupitelstvo	k1gNnSc7	zastupitelstvo
loboval	lobovat	k5eAaBmAgMnS	lobovat
přímo	přímo	k6eAd1	přímo
majitel	majitel	k1gMnSc1	majitel
investiční	investiční	k2eAgFnSc2d1	investiční
společnosti	společnost	k1gFnSc2	společnost
Michael	Michael	k1gMnSc1	Michael
Broda	Broda	k1gMnSc1	Broda
<g/>
.	.	kIx.	.
</s>
<s>
Rekonstruována	rekonstruován	k2eAgFnSc1d1	rekonstruována
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
<g/>
,	,	kIx,	,
budou	být	k5eAaImBp3nP	být
tam	tam	k6eAd1	tam
působit	působit	k5eAaImF	působit
i	i	k9	i
lékaři	lékař	k1gMnPc1	lékař
celostní	celostní	k2eAgFnSc2d1	celostní
medicíny	medicína	k1gFnSc2	medicína
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
právě	právě	k9	právě
s	s	k7c7	s
lékařem	lékař	k1gMnSc7	lékař
celostní	celostní	k2eAgFnSc2d1	celostní
mediciny	medicin	k2eAgFnSc2d1	medicina
Jiřím	Jiří	k1gMnSc6	Jiří
Kucharským	Kucharský	k2eAgFnPc3d1	Kucharská
projekt	projekt	k1gInSc1	projekt
bude	být	k5eAaImBp3nS	být
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
<g/>
.	.	kIx.	.
<g/>
Nedaleko	nedaleko	k7c2	nedaleko
Červené	Červená	k1gFnSc2	Červená
Hospody	Hospody	k?	Hospody
západně	západně	k6eAd1	západně
od	od	k7c2	od
města	město	k1gNnSc2	město
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
bylo	být	k5eAaImAgNnS	být
postaveno	postavit	k5eAaPmNgNnS	postavit
překladiště	překladiště	k1gNnSc1	překladiště
společnosti	společnost	k1gFnSc2	společnost
TOP	topit	k5eAaImRp2nS	topit
TRANS.	TRANS.	k1gFnSc2	TRANS.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
<g />
.	.	kIx.	.
</s>
<s>
2018	[number]	k4	2018
byla	být	k5eAaImAgFnS	být
dokončena	dokončen	k2eAgFnSc1d1	dokončena
přestavba	přestavba	k1gFnSc1	přestavba
budovy	budova	k1gFnSc2	budova
bývalé	bývalý	k2eAgFnSc2d1	bývalá
spořitelny	spořitelna	k1gFnSc2	spořitelna
na	na	k7c6	na
Soukopově	Soukopův	k2eAgFnSc6d1	Soukopův
ulici	ulice	k1gFnSc6	ulice
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
byla	být	k5eAaImAgFnS	být
budova	budova	k1gFnSc1	budova
postavena	postaven	k2eAgFnSc1d1	postavena
za	za	k7c2	za
socialismu	socialismus	k1gInSc2	socialismus
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
sídlem	sídlo	k1gNnSc7	sídlo
České	český	k2eAgFnSc2d1	Česká
spořitelny	spořitelna	k1gFnSc2	spořitelna
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
budova	budova	k1gFnSc1	budova
opuštěna	opustit	k5eAaPmNgFnS	opustit
a	a	k8xC	a
nabídnuta	nabídnout	k5eAaPmNgFnS	nabídnout
k	k	k7c3	k
prodeji	prodej	k1gInSc3	prodej
<g/>
,	,	kIx,	,
spelukovalo	spelukovat	k5eAaBmAgNnS	spelukovat
se	se	k3xPyFc4	se
o	o	k7c6	o
prodeji	prodej	k1gInSc6	prodej
městu	město	k1gNnSc3	město
a	a	k8xC	a
zřízení	zřízení	k1gNnSc3	zřízení
sociálních	sociální	k2eAgInPc2d1	sociální
bytů	byt	k1gInPc2	byt
nebo	nebo	k8xC	nebo
bytů	byt	k1gInPc2	byt
pro	pro	k7c4	pro
charitu	charita	k1gFnSc4	charita
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
však	však	k9	však
budovu	budova	k1gFnSc4	budova
zakoupila	zakoupit	k5eAaPmAgFnS	zakoupit
olomoucká	olomoucký	k2eAgFnSc1d1	olomoucká
společnost	společnost	k1gFnSc1	společnost
a	a	k8xC	a
předbudovala	předbudovat	k5eAaPmAgFnS	předbudovat
ji	on	k3xPp3gFnSc4	on
na	na	k7c4	na
budobu	budoba	k1gFnSc4	budoba
s	s	k7c7	s
29	[number]	k4	29
byty	byt	k1gInPc7	byt
a	a	k8xC	a
někoika	někoika	k1gFnSc1	někoika
prodejnami	prodejna	k1gFnPc7	prodejna
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
založena	založen	k2eAgFnSc1d1	založena
destilérka	destilérka	k1gFnSc1	destilérka
Trebitsch	Trebitsch	k1gMnSc1	Trebitsch
old	old	k?	old
town	town	k1gMnSc1	town
distillery	distiller	k1gInPc4	distiller
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
na	na	k7c4	na
produkci	produkce	k1gFnSc4	produkce
whisky	whisky	k1gFnSc2	whisky
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2020	[number]	k4	2020
chce	chtít	k5eAaImIp3nS	chtít
destilérka	destilérka	k1gFnSc1	destilérka
vyprodukovat	vyprodukovat	k5eAaPmF	vyprodukovat
60	[number]	k4	60
tisíc	tisíc	k4xCgInPc2	tisíc
lahví	lahev	k1gFnPc2	lahev
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
2018	[number]	k4	2018
byl	být	k5eAaImAgInS	být
otevřen	otevřít	k5eAaPmNgInS	otevřít
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
značkový	značkový	k2eAgInSc4d1	značkový
whisky	whisky	k1gFnSc2	whisky
bar	bar	k1gInSc1	bar
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roku	rok	k1gInSc6	rok
2018	[number]	k4	2018
bylo	být	k5eAaImAgNnS	být
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
třebíčská	třebíčský	k2eAgFnSc1d1	Třebíčská
společnost	společnost	k1gFnSc1	společnost
Nuvia	Nuvia	k1gFnSc1	Nuvia
získala	získat	k5eAaPmAgFnS	získat
zakázky	zakázka	k1gFnPc4	zakázka
pro	pro	k7c4	pro
modernizaci	modernizace	k1gFnSc4	modernizace
jaderných	jaderný	k2eAgFnPc2d1	jaderná
elektráren	elektrárna	k1gFnPc2	elektrárna
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
v	v	k7c6	v
hodnotě	hodnota	k1gFnSc6	hodnota
1,7	[number]	k4	1,7
miliardy	miliarda	k4xCgFnSc2	miliarda
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2019	[number]	k4	2019
by	by	kYmCp3nP	by
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
dokončena	dokončen	k2eAgFnSc1d1	dokončena
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
zóna	zóna	k1gFnSc1	zóna
Rafaelova	Rafaelův	k2eAgFnSc1d1	Rafaelova
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
investic	investice	k1gFnPc2	investice
v	v	k7c6	v
rozpočtu	rozpočet	k1gInSc6	rozpočet
pro	pro	k7c4	pro
rok	rok	k1gInSc4	rok
2019	[number]	k4	2019
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
pozemky	pozemek	k1gInPc4	pozemek
projevilo	projevit	k5eAaPmAgNnS	projevit
zájem	zájem	k1gInSc4	zájem
několik	několik	k4yIc1	několik
společností	společnost	k1gFnPc2	společnost
<g/>
,	,	kIx,	,
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2019	[number]	k4	2019
byly	být	k5eAaImAgInP	být
prodány	prodán	k2eAgInPc1d1	prodán
poslední	poslední	k2eAgInPc1d1	poslední
pozemky	pozemek	k1gInPc1	pozemek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průmyslové	průmyslový	k2eAgFnSc6d1	průmyslová
zóně	zóna	k1gFnSc6	zóna
budou	být	k5eAaImBp3nP	být
působit	působit	k5eAaImF	působit
společnosti	společnost	k1gFnPc1	společnost
Werk	Werk	k1gMnSc1	Werk
Werk	Werk	k1gMnSc1	Werk
<g/>
,	,	kIx,	,
Altreva	Altreva	k1gFnSc1	Altreva
a	a	k8xC	a
DNK	DNK	kA	DNK
systém	systém	k1gInSc1	systém
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2019	[number]	k4	2019
budou	být	k5eAaImBp3nP	být
instalovány	instalovat	k5eAaBmNgFnP	instalovat
tzv.	tzv.	kA	tzv.
mluvící	mluvící	k2eAgFnPc4d1	mluvící
sirény	siréna	k1gFnPc4	siréna
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
nárůstu	nárůst	k1gInSc3	nárůst
počtu	počet	k1gInSc2	počet
poplachových	poplachový	k2eAgFnPc2d1	poplachová
sirén	siréna	k1gFnPc2	siréna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Hospodaření	hospodaření	k1gNnSc1	hospodaření
města	město	k1gNnSc2	město
===	===	k?	===
</s>
</p>
<p>
<s>
Město	město	k1gNnSc1	město
hospodaří	hospodařit	k5eAaImIp3nS	hospodařit
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
s	s	k7c7	s
téměř	téměř	k6eAd1	téměř
vyrovnaným	vyrovnaný	k2eAgInSc7d1	vyrovnaný
rozpočtem	rozpočet	k1gInSc7	rozpočet
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2007	[number]	k4	2007
a	a	k8xC	a
2008	[number]	k4	2008
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
vyšších	vysoký	k2eAgInPc2d2	vyšší
přebytků	přebytek	k1gInPc2	přebytek
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
vyšší	vysoký	k2eAgInPc4d2	vyšší
schodky	schodek	k1gInPc4	schodek
město	město	k1gNnSc1	město
vykázalo	vykázat	k5eAaPmAgNnS	vykázat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
a	a	k8xC	a
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
hospodaří	hospodařit	k5eAaImIp3nS	hospodařit
s	s	k7c7	s
daňovými	daňový	k2eAgFnPc7d1	daňová
<g/>
,	,	kIx,	,
nedaňovými	daňový	k2eNgInPc7d1	nedaňový
<g/>
,	,	kIx,	,
kapitálovými	kapitálový	k2eAgInPc7d1	kapitálový
a	a	k8xC	a
dotačními	dotační	k2eAgInPc7d1	dotační
příjmy	příjem	k1gInPc7	příjem
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
svých	svůj	k3xOyFgMnPc2	svůj
výdajů	výdaj	k1gInPc2	výdaj
vynakládá	vynakládat	k5eAaImIp3nS	vynakládat
vyšší	vysoký	k2eAgFnSc1d2	vyšší
část	část	k1gFnSc1	část
svých	svůj	k3xOyFgInPc2	svůj
prostředků	prostředek	k1gInPc2	prostředek
na	na	k7c4	na
vzdělávání	vzdělávání	k1gNnSc4	vzdělávání
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
v	v	k7c6	v
majetku	majetek	k1gInSc6	majetek
má	mít	k5eAaImIp3nS	mít
základní	základní	k2eAgFnPc4d1	základní
školy	škola	k1gFnPc4	škola
a	a	k8xC	a
mateřské	mateřský	k2eAgFnPc4d1	mateřská
školy	škola	k1gFnPc4	škola
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
je	být	k5eAaImIp3nS	být
městem	město	k1gNnSc7	město
provozována	provozován	k2eAgFnSc1d1	provozována
i	i	k8xC	i
městská	městský	k2eAgFnSc1d1	městská
knihovna	knihovna	k1gFnSc1	knihovna
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
vynakládá	vynakládat	k5eAaImIp3nS	vynakládat
prostředky	prostředek	k1gInPc4	prostředek
i	i	k9	i
na	na	k7c4	na
kulturu	kultura	k1gFnSc4	kultura
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
provozuje	provozovat	k5eAaImIp3nS	provozovat
příspěvkovou	příspěvkový	k2eAgFnSc4d1	příspěvková
organizaci	organizace	k1gFnSc4	organizace
města	město	k1gNnSc2	město
Městské	městský	k2eAgNnSc4d1	Městské
kulturní	kulturní	k2eAgNnSc4d1	kulturní
středisko	středisko	k1gNnSc4	středisko
a	a	k8xC	a
další	další	k2eAgFnSc4d1	další
činnost	činnost	k1gFnSc4	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Sportovním	sportovní	k2eAgInPc3d1	sportovní
klubům	klub	k1gInPc3	klub
a	a	k8xC	a
sportovním	sportovní	k2eAgFnPc3d1	sportovní
zařízením	zařízení	k1gNnPc3	zařízení
město	město	k1gNnSc1	město
také	také	k9	také
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
financovat	financovat	k5eAaBmF	financovat
provoz	provoz	k1gInSc4	provoz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
opětovnému	opětovný	k2eAgNnSc3d1	opětovné
odložení	odložení	k1gNnSc3	odložení
účinnosti	účinnost	k1gFnSc2	účinnost
vyhlášky	vyhláška	k1gFnSc2	vyhláška
o	o	k7c6	o
zákazu	zákaz	k1gInSc6	zákaz
hazardních	hazardní	k2eAgInPc2d1	hazardní
přístrojů	přístroj	k1gInPc2	přístroj
na	na	k7c6	na
území	území	k1gNnSc6	území
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
tato	tento	k3xDgFnSc1	tento
vyhláška	vyhláška	k1gFnSc1	vyhláška
byla	být	k5eAaImAgFnS	být
schválena	schválit	k5eAaPmNgFnS	schválit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
byla	být	k5eAaImAgFnS	být
odložena	odložit	k5eAaPmNgFnS	odložit
její	její	k3xOp3gFnSc1	její
účinnost	účinnost	k1gFnSc1	účinnost
<g/>
.	.	kIx.	.
</s>
<s>
Kdyby	kdyby	kYmCp3nP	kdyby
vyhláška	vyhláška	k1gFnSc1	vyhláška
byla	být	k5eAaImAgFnS	být
schválena	schválit	k5eAaPmNgFnS	schválit
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
by	by	kYmCp3nS	by
zákaz	zákaz	k1gInSc1	zákaz
hazardu	hazard	k1gInSc2	hazard
na	na	k7c6	na
území	území	k1gNnSc6	území
města	město	k1gNnSc2	město
platil	platit	k5eAaImAgInS	platit
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
i	i	k9	i
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
rozpočet	rozpočet	k1gInSc1	rozpočet
města	město	k1gNnSc2	město
počítal	počítat	k5eAaImAgInS	počítat
s	s	k7c7	s
příjmem	příjem	k1gInSc7	příjem
12,5	[number]	k4	12,5
milionu	milion	k4xCgInSc2	milion
z	z	k7c2	z
"	"	kIx"	"
<g/>
hazardu	hazard	k1gInSc2	hazard
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
začali	začít	k5eAaPmAgMnP	začít
ve	v	k7c6	v
městě	město	k1gNnSc6	město
pod	pod	k7c7	pod
městskou	městský	k2eAgFnSc7d1	městská
policií	policie	k1gFnSc7	policie
působit	působit	k5eAaImF	působit
dva	dva	k4xCgInPc4	dva
tzv.	tzv.	kA	tzv.
asistenti	asistent	k1gMnPc1	asistent
prevence	prevence	k1gFnSc2	prevence
kriminality	kriminalita	k1gFnSc2	kriminalita
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
působit	působit	k5eAaImF	působit
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
prevence	prevence	k1gFnSc2	prevence
u	u	k7c2	u
škol	škola	k1gFnPc2	škola
či	či	k8xC	či
úřadu	úřad	k1gInSc2	úřad
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c4	na
rok	rok	k1gInSc4	rok
2017	[number]	k4	2017
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
schválen	schválen	k2eAgInSc1d1	schválen
nevyrovnaný	vyrovnaný	k2eNgInSc1d1	nevyrovnaný
rozpočet	rozpočet	k1gInSc1	rozpočet
se	s	k7c7	s
schodkem	schodek	k1gInSc7	schodek
59	[number]	k4	59
milionů	milion	k4xCgInPc2	milion
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
příjmy	příjem	k1gInPc1	příjem
jsou	být	k5eAaImIp3nP	být
plánovány	plánovat	k5eAaImNgInP	plánovat
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
562	[number]	k4	562
milionů	milion	k4xCgInPc2	milion
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
výdaje	výdaj	k1gInPc1	výdaj
pak	pak	k6eAd1	pak
na	na	k7c4	na
621	[number]	k4	621
milionů	milion	k4xCgInPc2	milion
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
<s>
Schodek	schodek	k1gInSc1	schodek
bude	být	k5eAaImBp3nS	být
pokryt	pokrýt	k5eAaPmNgInS	pokrýt
z	z	k7c2	z
výsledku	výsledek	k1gInSc2	výsledek
hospodaření	hospodaření	k1gNnSc2	hospodaření
v	v	k7c6	v
předchozích	předchozí	k2eAgNnPc6d1	předchozí
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
měla	mít	k5eAaImAgFnS	mít
stát	stát	k1gInSc4	stát
revitalizace	revitalizace	k1gFnSc2	revitalizace
ulic	ulice	k1gFnPc2	ulice
v	v	k7c6	v
městské	městský	k2eAgFnSc6d1	městská
části	část	k1gFnSc6	část
Borovina	borovina	k1gFnSc1	borovina
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
20	[number]	k4	20
milionů	milion	k4xCgInPc2	milion
Kč	Kč	kA	Kč
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2019	[number]	k4	2019
byl	být	k5eAaImAgInS	být
schválen	schválit	k5eAaPmNgInS	schválit
městská	městský	k2eAgFnSc1d1	městská
rozpočet	rozpočet	k1gInSc4	rozpočet
ve	v	k7c4	v
výši	výše	k1gFnSc4	výše
670	[number]	k4	670
milionů	milion	k4xCgInPc2	milion
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
celkem	celkem	k6eAd1	celkem
je	být	k5eAaImIp3nS	být
vyhrazeno	vyhradit	k5eAaPmNgNnS	vyhradit
200	[number]	k4	200
milionů	milion	k4xCgInPc2	milion
Kč	Kč	kA	Kč
na	na	k7c4	na
investice	investice	k1gFnPc4	investice
<g/>
.	.	kIx.	.
</s>
<s>
Plánována	plánován	k2eAgFnSc1d1	plánována
je	být	k5eAaImIp3nS	být
dostavba	dostavba	k1gFnSc1	dostavba
komunitního	komunitní	k2eAgNnSc2d1	komunitní
centra	centrum	k1gNnSc2	centrum
v	v	k7c6	v
bývalém	bývalý	k2eAgNnSc6d1	bývalé
kině	kino	k1gNnSc6	kino
Moravia	Moravium	k1gNnSc2	Moravium
<g/>
,	,	kIx,	,
zasíťování	zasíťování	k1gNnSc2	zasíťování
nové	nový	k2eAgFnSc2d1	nová
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
zóny	zóna	k1gFnSc2	zóna
Rafaelova	Rafaelův	k2eAgFnSc1d1	Rafaelova
<g/>
,	,	kIx,	,
vybudování	vybudování	k1gNnSc1	vybudování
kruhové	kruhový	k2eAgFnSc2d1	kruhová
křižovatky	křižovatka	k1gFnSc2	křižovatka
na	na	k7c6	na
Cyrilometodějské	cyrilometodějský	k2eAgFnSc6d1	Cyrilometodějská
ulici	ulice	k1gFnSc6	ulice
a	a	k8xC	a
vybudování	vybudování	k1gNnSc6	vybudování
semaforů	semafor	k1gInPc2	semafor
na	na	k7c6	na
několika	několik	k4yIc6	několik
křižovatkách	křižovatka	k1gFnPc6	křižovatka
<g/>
.	.	kIx.	.
</s>
<s>
Budou	být	k5eAaImBp3nP	být
se	se	k3xPyFc4	se
také	také	k9	také
rekonstruovat	rekonstruovat	k5eAaBmF	rekonstruovat
ulice	ulice	k1gFnSc1	ulice
Chelčického	Chelčický	k2eAgInSc2d1	Chelčický
a	a	k8xC	a
Křižíkova	Křižíkův	k2eAgInSc2d1	Křižíkův
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
rozpočtu	rozpočet	k1gInSc2	rozpočet
je	být	k5eAaImIp3nS	být
vyčleněno	vyčlenit	k5eAaPmNgNnS	vyčlenit
3,5	[number]	k4	3,5
milionu	milion	k4xCgInSc2	milion
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterých	který	k3yRgInPc6	který
budou	být	k5eAaImBp3nP	být
rozhodovat	rozhodovat	k5eAaImF	rozhodovat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
participativního	participativní	k2eAgInSc2d1	participativní
rozpočtu	rozpočet	k1gInSc2	rozpočet
třebíčští	třebíčský	k2eAgMnPc1d1	třebíčský
občané	občan	k1gMnPc1	občan
<g/>
.	.	kIx.	.
<g/>
Ratingová	ratingový	k2eAgFnSc1d1	ratingová
agentura	agentura	k1gFnSc1	agentura
Moody	Mooda	k1gFnSc2	Mooda
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Investors	Investorsa	k1gFnPc2	Investorsa
Service	Service	k1gFnSc2	Service
provedla	provést	k5eAaPmAgFnS	provést
ratingové	ratingový	k2eAgNnSc4d1	ratingové
šetření	šetření	k1gNnSc4	šetření
města	město	k1gNnSc2	město
s	s	k7c7	s
výsledkem	výsledek	k1gInSc7	výsledek
A	a	k9	a
<g/>
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
Aa	Aa	k1gFnSc1	Aa
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
město	město	k1gNnSc1	město
hospodaří	hospodařit	k5eAaImIp3nS	hospodařit
stabilně	stabilně	k6eAd1	stabilně
s	s	k7c7	s
přebytky	přebytek	k1gInPc7	přebytek
<g/>
.	.	kIx.	.
</s>
<s>
Silnou	silný	k2eAgFnSc7d1	silná
stránkou	stránka	k1gFnSc7	stránka
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
hospodaření	hospodaření	k1gNnSc1	hospodaření
s	s	k7c7	s
přebytkem	přebytek	k1gInSc7	přebytek
a	a	k8xC	a
mírná	mírný	k2eAgFnSc1d1	mírná
zadluženost	zadluženost	k1gFnSc1	zadluženost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
pod	pod	k7c7	pod
průměrem	průměr	k1gInSc7	průměr
měst	město	k1gNnPc2	město
Česka	Česko	k1gNnSc2	Česko
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
město	město	k1gNnSc1	město
rozhodovalo	rozhodovat	k5eAaImAgNnS	rozhodovat
o	o	k7c6	o
hazardu	hazard	k1gInSc6	hazard
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
plánovalo	plánovat	k5eAaImAgNnS	plánovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2018	[number]	k4	2018
budou	být	k5eAaImBp3nP	být
muset	muset	k5eAaImF	muset
být	být	k5eAaImF	být
herny	herna	k1gFnPc4	herna
plošně	plošně	k6eAd1	plošně
zrušeny	zrušit	k5eAaPmNgFnP	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Herny	herna	k1gFnPc1	herna
nakonec	nakonec	k6eAd1	nakonec
nebyly	být	k5eNaImAgFnP	být
plošně	plošně	k6eAd1	plošně
zakázány	zakázat	k5eAaPmNgFnP	zakázat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
budou	být	k5eAaImBp3nP	být
regulovány	regulován	k2eAgFnPc1d1	regulována
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc1	jejich
provoz	provoz	k1gInSc1	provoz
je	být	k5eAaImIp3nS	být
povolen	povolit	k5eAaPmNgInS	povolit
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
získá	získat	k5eAaPmIp3nS	získat
z	z	k7c2	z
provozu	provoz	k1gInSc2	provoz
heren	herna	k1gFnPc2	herna
ve	v	k7c6	v
městě	město	k1gNnSc6	město
20	[number]	k4	20
milionů	milion	k4xCgInPc2	milion
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
<s>
Členka	členka	k1gFnSc1	členka
sociální	sociální	k2eAgFnSc2d1	sociální
komise	komise	k1gFnSc2	komise
Blanka	Blanka	k1gFnSc1	Blanka
Kutinová	kutinový	k2eAgFnSc1d1	Kutinová
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
dle	dle	k7c2	dle
Národního	národní	k2eAgNnSc2d1	národní
monitorovacího	monitorovací	k2eAgNnSc2d1	monitorovací
střediska	středisko	k1gNnSc2	středisko
pro	pro	k7c4	pro
drogy	droga	k1gFnPc4	droga
a	a	k8xC	a
závislosti	závislost	k1gFnPc4	závislost
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
narůstá	narůstat	k5eAaImIp3nS	narůstat
počet	počet	k1gInSc1	počet
závislých	závislý	k2eAgMnPc2d1	závislý
mladistvých	mladistvý	k1gMnPc2	mladistvý
a	a	k8xC	a
narůstá	narůstat	k5eAaImIp3nS	narůstat
i	i	k9	i
počet	počet	k1gInSc1	počet
heren	herna	k1gFnPc2	herna
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Třebíč	Třebíč	k1gFnSc1	Třebíč
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
posledních	poslední	k2eAgNnPc2d1	poslední
měst	město	k1gNnPc2	město
na	na	k7c6	na
Vysočině	vysočina	k1gFnSc6	vysočina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
povolen	povolen	k2eAgInSc4d1	povolen
jejich	jejich	k3xOp3gInSc4	jejich
provoz	provoz	k1gInSc4	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
Pacal	Pacal	k1gMnSc1	Pacal
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
Jihlavě	Jihlava	k1gFnSc6	Jihlava
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
hazard	hazard	k1gInSc4	hazard
na	na	k7c4	na
území	území	k1gNnSc4	území
města	město	k1gNnSc2	město
zakázala	zakázat	k5eAaPmAgFnS	zakázat
probíhá	probíhat	k5eAaImIp3nS	probíhat
boj	boj	k1gInSc1	boj
s	s	k7c7	s
nelegálními	legální	k2eNgFnPc7d1	nelegální
hernami	herna	k1gFnPc7	herna
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
v	v	k7c6	v
případě	případ	k1gInSc6	případ
plošného	plošný	k2eAgInSc2d1	plošný
zákazu	zákaz	k1gInSc2	zákaz
heren	herna	k1gFnPc2	herna
jejich	jejich	k3xOp3gMnPc1	jejich
provozovatelé	provozovatel	k1gMnPc1	provozovatel
je	on	k3xPp3gNnSc4	on
otevřeli	otevřít	k5eAaPmAgMnP	otevřít
v	v	k7c6	v
obchodním	obchodní	k2eAgNnSc6d1	obchodní
centru	centrum	k1gNnSc6	centrum
StopShop	StopShop	k1gInSc1	StopShop
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
funguje	fungovat	k5eAaImIp3nS	fungovat
na	na	k7c6	na
území	území	k1gNnSc6	území
Stříteže	Střítež	k1gFnSc2	Střítež
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
nové	nový	k2eAgFnSc2d1	nová
vyhlášky	vyhláška	k1gFnSc2	vyhláška
je	být	k5eAaImIp3nS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
14	[number]	k4	14
adres	adresa	k1gFnPc2	adresa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mohou	moct	k5eAaImIp3nP	moct
herny	herna	k1gFnPc1	herna
pod	pod	k7c7	pod
regulací	regulace	k1gFnSc7	regulace
nadále	nadále	k6eAd1	nadále
působit	působit	k5eAaImF	působit
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
mít	mít	k5eAaImF	mít
například	například	k6eAd1	například
kamery	kamera	k1gFnSc2	kamera
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
omezena	omezit	k5eAaPmNgFnS	omezit
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
sázka	sázka	k1gFnSc1	sázka
<g/>
.	.	kIx.	.
</s>
<s>
Herny	herna	k1gFnPc4	herna
nesmí	smět	k5eNaImIp3nS	smět
být	být	k5eAaImF	být
na	na	k7c6	na
území	území	k1gNnSc6	území
pod	pod	k7c7	pod
ochranou	ochrana	k1gFnSc7	ochrana
UNESCO	UNESCO	kA	UNESCO
a	a	k8xC	a
do	do	k7c2	do
100	[number]	k4	100
metrů	metr	k1gInPc2	metr
od	od	k7c2	od
škol	škola	k1gFnPc2	škola
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Obchod	obchod	k1gInSc1	obchod
===	===	k?	===
</s>
</p>
<p>
<s>
Již	již	k9	již
dříve	dříve	k6eAd2	dříve
ovšem	ovšem	k9	ovšem
byly	být	k5eAaImAgInP	být
otevřeny	otevřen	k2eAgInPc1d1	otevřen
jiné	jiný	k2eAgInPc1d1	jiný
supermarkety	supermarket	k1gInPc1	supermarket
či	či	k8xC	či
prodejny	prodejna	k1gFnPc1	prodejna
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
byl	být	k5eAaImAgInS	být
otevřen	otevřít	k5eAaPmNgInS	otevřít
provoz	provoz	k1gInSc1	provoz
prodejny	prodejna	k1gFnSc2	prodejna
Potravin	potravina	k1gFnPc2	potravina
Brno	Brno	k1gNnSc1	Brno
–	–	k?	–
závod	závod	k1gInSc4	závod
Třebíč	Třebíč	k1gFnSc1	Třebíč
na	na	k7c6	na
Račerovické	Račerovický	k2eAgFnSc6d1	Račerovická
ulici	ulice	k1gFnSc6	ulice
<g/>
,	,	kIx,	,
budovy	budova	k1gFnPc1	budova
byly	být	k5eAaImAgFnP	být
postaveny	postavit	k5eAaPmNgFnP	postavit
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
akce	akce	k1gFnSc2	akce
Z	Z	kA	Z
a	a	k8xC	a
celkové	celkový	k2eAgInPc1d1	celkový
náklady	náklad	k1gInPc1	náklad
na	na	k7c4	na
stavbu	stavba	k1gFnSc4	stavba
dosáhly	dosáhnout	k5eAaPmAgFnP	dosáhnout
více	hodně	k6eAd2	hodně
než	než	k8xS	než
5	[number]	k4	5
milionů	milion	k4xCgInPc2	milion
Kčs	Kčs	kA	Kčs
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
rokem	rok	k1gInSc7	rok
bylo	být	k5eAaImAgNnS	být
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
obchodní	obchodní	k2eAgNnSc1d1	obchodní
středisko	středisko	k1gNnSc1	středisko
Domácích	domácí	k2eAgFnPc2d1	domácí
potřeb	potřeba	k1gFnPc2	potřeba
<g/>
,	,	kIx,	,
vystavěno	vystavěn	k2eAgNnSc1d1	vystavěno
bylo	být	k5eAaImAgNnS	být
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1983	[number]	k4	1983
a	a	k8xC	a
1986	[number]	k4	1986
s	s	k7c7	s
celkovými	celkový	k2eAgInPc7d1	celkový
náklady	náklad	k1gInPc7	náklad
téměř	téměř	k6eAd1	téměř
30	[number]	k4	30
milionů	milion	k4xCgInPc2	milion
Kčs	Kčs	kA	Kčs
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
situováno	situovat	k5eAaBmNgNnS	situovat
na	na	k7c6	na
Komenského	Komenského	k2eAgNnSc6d1	Komenského
náměstí	náměstí	k1gNnSc6	náměstí
a	a	k8xC	a
celkem	celkem	k6eAd1	celkem
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
nalezlo	naleznout	k5eAaPmAgNnS	naleznout
zaměstnání	zaměstnání	k1gNnSc1	zaměstnání
76	[number]	k4	76
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
prvními	první	k4xOgInPc7	první
porevolučními	porevoluční	k2eAgInPc7d1	porevoluční
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
Mana	mana	k1gFnSc1	mana
na	na	k7c6	na
Karlově	Karlův	k2eAgNnSc6d1	Karlovo
náměstí	náměstí	k1gNnSc6	náměstí
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
supermarket	supermarket	k1gInSc1	supermarket
Delvita	Delvita	k1gFnSc1	Delvita
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
otevřen	otevřít	k5eAaPmNgInS	otevřít
v	v	k7c6	v
bývalé	bývalý	k2eAgFnSc6d1	bývalá
proluce	proluka	k1gFnSc6	proluka
u	u	k7c2	u
řeky	řeka	k1gFnSc2	řeka
Jihlavy	Jihlava	k1gFnSc2	Jihlava
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Smila	Smilo	k1gNnSc2	Smilo
Osovského	Osovský	k2eAgNnSc2d1	Osovský
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
supermarkety	supermarket	k1gInPc4	supermarket
Delvita	Delvita	k1gFnSc1	Delvita
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
převzaty	převzít	k5eAaPmNgInP	převzít
společností	společnost	k1gFnSc7	společnost
Rewe	Rew	k1gFnSc2	Rew
Group	Group	k1gInSc1	Group
a	a	k8xC	a
transformovány	transformován	k2eAgInPc1d1	transformován
na	na	k7c4	na
supermarkety	supermarket	k1gInPc4	supermarket
Billa	Bill	k1gMnSc2	Bill
<g/>
.	.	kIx.	.
</s>
<s>
Supermarket	supermarket	k1gInSc1	supermarket
Billa	Bill	k1gMnSc2	Bill
byl	být	k5eAaImAgInS	být
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2015	[number]	k4	2015
pro	pro	k7c4	pro
špatný	špatný	k2eAgInSc4d1	špatný
technický	technický	k2eAgInSc4d1	technický
stav	stav	k1gInSc4	stav
budovy	budova	k1gFnSc2	budova
zavřen	zavřen	k2eAgInSc1d1	zavřen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
na	na	k7c6	na
Táborské	Táborské	k2eAgFnSc6d1	Táborské
ulici	ulice	k1gFnSc6	ulice
Billa	Bill	k1gMnSc2	Bill
<g/>
,	,	kIx,	,
v	v	k7c4	v
tu	ten	k3xDgFnSc4	ten
dobu	doba	k1gFnSc4	doba
(	(	kIx(	(
<g/>
rok	rok	k1gInSc4	rok
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
otevřen	otevřít	k5eAaPmNgInS	otevřít
i	i	k9	i
supermarket	supermarket	k1gInSc1	supermarket
Prima	primo	k1gNnSc2	primo
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
Hypernova	Hypernův	k2eAgFnSc1d1	Hypernova
<g/>
,	,	kIx,	,
po	po	k7c6	po
přestěhování	přestěhování	k1gNnSc6	přestěhování
do	do	k7c2	do
nové	nový	k2eAgFnSc2d1	nová
budovy	budova	k1gFnSc2	budova
Hyper	hyper	k2eAgFnPc2d1	hyper
Albert	Alberta	k1gFnPc2	Alberta
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
Albert	Alberta	k1gFnPc2	Alberta
Hypermarket	hypermarket	k1gInSc1	hypermarket
<g/>
)	)	kIx)	)
na	na	k7c6	na
jižním	jižní	k2eAgInSc6d1	jižní
okraji	okraj	k1gInSc6	okraj
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
Brněnské	brněnský	k2eAgFnSc6d1	brněnská
ulici	ulice	k1gFnSc6	ulice
postaven	postavit	k5eAaPmNgInS	postavit
supermarket	supermarket	k1gInSc1	supermarket
Kaufland	Kauflanda	k1gFnPc2	Kauflanda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
po	po	k7c6	po
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
budovat	budovat	k5eAaImF	budovat
levné	levný	k2eAgInPc4d1	levný
supermarkety	supermarket	k1gInPc4	supermarket
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
Penny	penny	k1gFnSc4	penny
Market	market	k1gInSc1	market
<g/>
,	,	kIx,	,
Albert	Albert	k1gMnSc1	Albert
(	(	kIx(	(
<g/>
bývalý	bývalý	k2eAgInSc1d1	bývalý
Sezam	sezam	k1gInSc1	sezam
<g/>
)	)	kIx)	)
či	či	k8xC	či
Jednota	jednota	k1gFnSc1	jednota
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Kpt.	Kpt.	k1gMnSc2	Kpt.
Jaroše	Jaroš	k1gMnSc2	Jaroš
otevřen	otevřen	k2eAgInSc4d1	otevřen
historicky	historicky	k6eAd1	historicky
první	první	k4xOgInSc1	první
supermarket	supermarket	k1gInSc1	supermarket
SPAR	SPAR	k?	SPAR
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgInPc1	všechen
ostatní	ostatní	k2eAgInPc1d1	ostatní
hypermarkety	hypermarket	k1gInPc1	hypermarket
firmy	firma	k1gFnSc2	firma
Spar	Spar	k?	Spar
měly	mít	k5eAaImAgFnP	mít
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
značku	značka	k1gFnSc4	značka
Interspar	Interspar	k1gMnSc1	Interspar
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
31	[number]	k4	31
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2007	[number]	k4	2007
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
lokalitě	lokalita	k1gFnSc6	lokalita
Vídeňský	vídeňský	k2eAgInSc4d1	vídeňský
rybník	rybník	k1gInSc4	rybník
otevřeny	otevřen	k2eAgFnPc1d1	otevřena
supermarket	supermarket	k1gInSc4	supermarket
Interspar	Interspara	k1gFnPc2	Interspara
a	a	k8xC	a
supermarket	supermarket	k1gInSc1	supermarket
Lidl	Lidla	k1gFnPc2	Lidla
<g/>
,	,	kIx,	,
otevření	otevření	k1gNnSc4	otevření
provázely	provázet	k5eAaImAgInP	provázet
problémy	problém	k1gInPc1	problém
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jeden	jeden	k4xCgInSc4	jeden
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
příjezdů	příjezd	k1gInPc2	příjezd
nebyl	být	k5eNaImAgInS	být
zkolaudován	zkolaudovat	k5eAaPmNgInS	zkolaudovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
byl	být	k5eAaImAgInS	být
otevřen	otevřít	k5eAaPmNgInS	otevřít
první	první	k4xOgInSc1	první
supermarket	supermarket	k1gInSc1	supermarket
v	v	k7c6	v
místní	místní	k2eAgFnSc6d1	místní
části	část	k1gFnSc6	část
Borovina	borovina	k1gFnSc1	borovina
<g/>
,	,	kIx,	,
Plus	plus	k1gInSc1	plus
<g/>
.	.	kIx.	.
</s>
<s>
Nejnovějším	nový	k2eAgInSc7d3	nejnovější
nákupním	nákupní	k2eAgInSc7d1	nákupní
centrem	centr	k1gInSc7	centr
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
je	být	k5eAaImIp3nS	být
OC	OC	kA	OC
Stop	stop	k1gInSc1	stop
Shop	shop	k1gInSc1	shop
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgFnSc1d1	ležící
na	na	k7c6	na
Znojemské	znojemský	k2eAgFnSc6d1	Znojemská
ulici	ulice	k1gFnSc6	ulice
naproti	naproti	k7c3	naproti
hypermarketu	hypermarket	k1gInSc3	hypermarket
Interspar	Interspara	k1gFnPc2	Interspara
a	a	k8xC	a
bývalé	bývalý	k2eAgFnSc6d1	bývalá
prodejně	prodejna	k1gFnSc6	prodejna
Hypernova	Hypernov	k1gInSc2	Hypernov
<g/>
,	,	kIx,	,
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
objektu	objekt	k1gInSc2	objekt
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
hala	hala	k1gFnSc1	hala
s	s	k7c7	s
neurčitým	určitý	k2eNgInSc7d1	neurčitý
účelem	účel	k1gInSc7	účel
<g/>
.	.	kIx.	.
</s>
<s>
Budova	budova	k1gFnSc1	budova
byla	být	k5eAaImAgFnS	být
uzavřena	uzavřít	k5eAaPmNgFnS	uzavřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
byla	být	k5eAaImAgFnS	být
zakoupena	zakoupen	k2eAgFnSc1d1	zakoupena
Robertem	Robert	k1gMnSc7	Robert
Fialou	Fiala	k1gMnSc7	Fiala
a	a	k8xC	a
měla	mít	k5eAaImAgFnS	mít
by	by	kYmCp3nS	by
být	být	k5eAaImF	být
proměněna	proměnit	k5eAaPmNgFnS	proměnit
v	v	k7c4	v
obchodní	obchodní	k2eAgNnSc4d1	obchodní
centrum	centrum	k1gNnSc4	centrum
<g/>
.	.	kIx.	.
<g/>
Součástí	součást	k1gFnSc7	součást
OC	OC	kA	OC
Stop	stop	k2eAgInSc1d1	stop
Shop	shop	k1gInSc1	shop
je	být	k5eAaImIp3nS	být
první	první	k4xOgInSc1	první
supermarket	supermarket	k1gInSc1	supermarket
Hyper	hyper	k2eAgMnSc1d1	hyper
Albert	Albert	k1gMnSc1	Albert
otevřený	otevřený	k2eAgMnSc1d1	otevřený
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Hypernova	Hypernův	k2eAgFnSc1d1	Hypernova
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dalšími	další	k2eAgFnPc7d1	další
většími	veliký	k2eAgFnPc7d2	veliký
provozovnami	provozovna	k1gFnPc7	provozovna
v	v	k7c6	v
OC	OC	kA	OC
jsou	být	k5eAaImIp3nP	být
prodejny	prodejna	k1gFnSc2	prodejna
elektroniky	elektronika	k1gFnSc2	elektronika
Okay	Okaa	k1gFnSc2	Okaa
a	a	k8xC	a
Expert	expert	k1gMnSc1	expert
elektro	elektro	k6eAd1	elektro
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2009	[number]	k4	2009
byl	být	k5eAaImAgInS	být
hypermarket	hypermarket	k1gInSc4	hypermarket
Hyper	hyper	k2eAgMnSc1d1	hyper
Albert	Albert	k1gMnSc1	Albert
přejmenován	přejmenován	k2eAgMnSc1d1	přejmenován
na	na	k7c4	na
Albert	Albert	k1gMnSc1	Albert
Hypermarket	hypermarket	k1gInSc1	hypermarket
a	a	k8xC	a
diskont	diskont	k1gInSc1	diskont
Plus	plus	k1gNnSc2	plus
na	na	k7c4	na
obchod	obchod	k1gInSc4	obchod
Penny	penny	k1gInSc2	penny
Market	market	k1gInSc1	market
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
budově	budova	k1gFnSc6	budova
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dříve	dříve	k6eAd2	dříve
sídlil	sídlit	k5eAaImAgInS	sídlit
supermarket	supermarket	k1gInSc1	supermarket
společnosti	společnost	k1gFnSc2	společnost
Prima	prima	k2eAgFnSc2d1	prima
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
Hypernova	Hypernův	k2eAgFnSc1d1	Hypernova
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgFnP	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
umístěny	umístit	k5eAaPmNgInP	umístit
celkem	celkem	k6eAd1	celkem
tři	tři	k4xCgFnPc4	tři
prodejny	prodejna	k1gFnPc4	prodejna
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
budova	budova	k1gFnSc1	budova
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
a	a	k8xC	a
2017	[number]	k4	2017
byla	být	k5eAaImAgFnS	být
novým	nový	k2eAgMnSc7d1	nový
majitelem	majitel	k1gMnSc7	majitel
Robertem	Robert	k1gMnSc7	Robert
Fialou	fiala	k1gFnSc7	fiala
rekonstruována	rekonstruovat	k5eAaBmNgFnS	rekonstruovat
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
prodejny	prodejna	k1gFnSc2	prodejna
Global	globat	k5eAaImAgMnS	globat
Elektro	Elektro	k1gNnSc4	Elektro
<g/>
,	,	kIx,	,
Mountfield	Mountfield	k1gMnSc1	Mountfield
a	a	k8xC	a
Jena	Jena	k1gMnSc1	Jena
Nábytek	nábytek	k1gInSc1	nábytek
<g/>
.	.	kIx.	.
</s>
<s>
Plánuje	plánovat	k5eAaImIp3nS	plánovat
se	se	k3xPyFc4	se
také	také	k9	také
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
budovy	budova	k1gFnSc2	budova
bývalé	bývalý	k2eAgFnSc2d1	bývalá
prodejny	prodejna	k1gFnSc2	prodejna
společnosti	společnost	k1gFnSc2	společnost
Billa	Bill	k1gMnSc2	Bill
v	v	k7c6	v
Jejkovské	Jejkovský	k2eAgFnSc6d1	Jejkovská
bráně	brána	k1gFnSc6	brána
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2017	[number]	k4	2017
byla	být	k5eAaImAgFnS	být
otevřena	otevřít	k5eAaPmNgFnS	otevřít
v	v	k7c6	v
bývalé	bývalý	k2eAgFnSc6d1	bývalá
prodejně	prodejna	k1gFnSc6	prodejna
společnosti	společnost	k1gFnSc2	společnost
prima	prima	k6eAd1	prima
otevřena	otevřen	k2eAgFnSc1d1	otevřena
nová	nový	k2eAgFnSc1d1	nová
prodejna	prodejna	k1gFnSc1	prodejna
společnosti	společnost	k1gFnSc2	společnost
Mountfield	Mountfielda	k1gFnPc2	Mountfielda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
1360	[number]	k4	1360
metrů	metr	k1gInPc2	metr
čtverečních	čtvereční	k2eAgInPc2d1	čtvereční
prodejní	prodejní	k2eAgFnSc4d1	prodejní
plochy	plocha	k1gFnPc4	plocha
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
příslušno	příslušno	k6eAd1	příslušno
60	[number]	k4	60
parkovacích	parkovací	k2eAgNnPc2d1	parkovací
míst	místo	k1gNnPc2	místo
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
výstavba	výstavba	k1gFnSc1	výstavba
nové	nový	k2eAgFnSc2d1	nová
obchodní	obchodní	k2eAgFnSc2d1	obchodní
zóny	zóna	k1gFnSc2	zóna
v	v	k7c6	v
sousedství	sousedství	k1gNnSc6	sousedství
stávajících	stávající	k2eAgFnPc2d1	stávající
budov	budova	k1gFnPc2	budova
OC	OC	kA	OC
Stop	stop	k1gInSc1	stop
Shop	shop	k1gInSc1	shop
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
drive-in	driven	k1gInSc1	drive-in
restaurace	restaurace	k1gFnSc2	restaurace
a	a	k8xC	a
obchody	obchod	k1gInPc1	obchod
s	s	k7c7	s
módou	móda	k1gFnSc7	móda
<g/>
,	,	kIx,	,
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
také	také	k9	také
stojí	stát	k5eAaImIp3nS	stát
samostatná	samostatný	k2eAgFnSc1d1	samostatná
budova	budova	k1gFnSc1	budova
prodejny	prodejna	k1gFnSc2	prodejna
sportovního	sportovní	k2eAgNnSc2d1	sportovní
zboží	zboží	k1gNnSc2	zboží
Decathlon	Decathlon	k1gInSc1	Decathlon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
pěti	pět	k4xCc6	pět
letech	léto	k1gNnPc6	léto
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
hypermarket	hypermarket	k1gInSc1	hypermarket
Kaufland	Kauflanda	k1gFnPc2	Kauflanda
<g/>
.	.	kIx.	.
</s>
<s>
Bude	být	k5eAaImBp3nS	být
také	také	k9	také
upravena	upravit	k5eAaPmNgFnS	upravit
silnice	silnice	k1gFnSc1	silnice
v	v	k7c6	v
prostorách	prostora	k1gFnPc6	prostora
parkoviště	parkoviště	k1gNnSc2	parkoviště
<g/>
,	,	kIx,	,
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
nový	nový	k2eAgInSc4d1	nový
kruhový	kruhový	k2eAgInSc4d1	kruhový
objezd	objezd	k1gInSc4	objezd
<g/>
,	,	kIx,	,
město	město	k1gNnSc1	město
pak	pak	k6eAd1	pak
neplánuje	plánovat	k5eNaImIp3nS	plánovat
přivedení	přivedení	k1gNnSc4	přivedení
chodníku	chodník	k1gInSc2	chodník
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
plocha	plocha	k1gFnSc1	plocha
OC	OC	kA	OC
Stop	stop	k2eAgInSc1d1	stop
Shop	shop	k1gInSc1	shop
je	být	k5eAaImIp3nS	být
96	[number]	k4	96
000	[number]	k4	000
m	m	kA	m
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
celkem	celkem	k6eAd1	celkem
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
767	[number]	k4	767
parkovacích	parkovací	k2eAgNnPc2d1	parkovací
míst	místo	k1gNnPc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Drive-in	Driven	k1gInSc1	Drive-in
restaurací	restaurace	k1gFnPc2	restaurace
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
bude	být	k5eAaImBp3nS	být
zbudována	zbudovat	k5eAaPmNgFnS	zbudovat
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
obchodního	obchodní	k2eAgNnSc2d1	obchodní
centra	centrum	k1gNnSc2	centrum
je	být	k5eAaImIp3nS	být
KFC	KFC	kA	KFC
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Decathlonem	Decathlon	k1gInSc7	Decathlon
byly	být	k5eAaImAgFnP	být
otevřeny	otevřen	k2eAgFnPc1d1	otevřena
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
roku	rok	k1gInSc2	rok
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
roku	rok	k1gInSc2	rok
2019	[number]	k4	2019
bude	být	k5eAaImBp3nS	být
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
otevřena	otevřen	k2eAgFnSc1d1	otevřena
provozovna	provozovna	k1gFnSc1	provozovna
drogerie	drogerie	k1gFnSc2	drogerie
dm	dm	kA	dm
a	a	k8xC	a
obchody	obchod	k1gInPc1	obchod
značek	značka	k1gFnPc2	značka
CCC	CCC	kA	CCC
a	a	k8xC	a
New	New	k1gMnSc1	New
Yorker	Yorker	k1gMnSc1	Yorker
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
mají	mít	k5eAaImIp3nP	mít
být	být	k5eAaImF	být
otevřeny	otevřen	k2eAgFnPc4d1	otevřena
provozovny	provozovna	k1gFnPc4	provozovna
Cropp	Cropp	k1gInSc4	Cropp
<g/>
,	,	kIx,	,
House	house	k1gNnSc4	house
a	a	k8xC	a
Sinsay	Sinsaa	k1gFnPc4	Sinsaa
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
pak	pak	k6eAd1	pak
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
upraveno	upravit	k5eAaPmNgNnS	upravit
zázemí	zázemí	k1gNnSc2	zázemí
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
otevřeno	otevřen	k2eAgNnSc4d1	otevřeno
dětské	dětský	k2eAgNnSc4d1	dětské
hřiště	hřiště	k1gNnSc4	hřiště
<g/>
.	.	kIx.	.
<g/>
Roku	rok	k1gInSc2	rok
2018	[number]	k4	2018
projevil	projevit	k5eAaPmAgInS	projevit
zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
proluku	proluka	k1gFnSc4	proluka
po	po	k7c6	po
bývalých	bývalý	k2eAgInPc6d1	bývalý
rukavičkářských	rukavičkářský	k2eAgInPc6d1	rukavičkářský
závodech	závod	k1gInPc6	závod
na	na	k7c6	na
Brněnské	brněnský	k2eAgFnSc6d1	brněnská
ulici	ulice	k1gFnSc6	ulice
majitel	majitel	k1gMnSc1	majitel
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
provozuje	provozovat	k5eAaImIp3nS	provozovat
prodejny	prodejna	k1gFnPc4	prodejna
LIDL	LIDL	kA	LIDL
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
by	by	kYmCp3nS	by
ráda	rád	k2eAgFnSc1d1	ráda
postavila	postavit	k5eAaPmAgFnS	postavit
obchod	obchod	k1gInSc4	obchod
s	s	k7c7	s
velkým	velký	k2eAgNnSc7d1	velké
parkovištěm	parkoviště	k1gNnSc7	parkoviště
<g/>
.	.	kIx.	.
</s>
<s>
Budova	budova	k1gFnSc1	budova
obchodu	obchod	k1gInSc2	obchod
bude	být	k5eAaImBp3nS	být
spojena	spojen	k2eAgFnSc1d1	spojena
s	s	k7c7	s
obchodní	obchodní	k2eAgFnSc7d1	obchodní
zónou	zóna	k1gFnSc7	zóna
pro	pro	k7c4	pro
menší	malý	k2eAgInPc4d2	menší
obchody	obchod	k1gInPc4	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
budova	budova	k1gFnSc1	budova
byla	být	k5eAaImAgFnS	být
otevřena	otevřít	k5eAaPmNgFnS	otevřít
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2018	[number]	k4	2018
<g/>
,	,	kIx,	,
součástí	součást	k1gFnSc7	součást
budovy	budova	k1gFnSc2	budova
jsou	být	k5eAaImIp3nP	být
širší	široký	k2eAgFnPc1d2	širší
uličky	ulička	k1gFnPc1	ulička
<g/>
,	,	kIx,	,
než	než	k8xS	než
v	v	k7c6	v
původních	původní	k2eAgInPc6d1	původní
návrzích	návrh	k1gInPc6	návrh
objektů	objekt	k1gInPc2	objekt
společnosti	společnost	k1gFnSc2	společnost
LIDL	LIDL	kA	LIDL
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
stěn	stěna	k1gFnPc2	stěna
budovy	budova	k1gFnSc2	budova
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
prosklená	prosklený	k2eAgFnSc1d1	prosklená
<g/>
.	.	kIx.	.
</s>
<s>
Parkoviště	parkoviště	k1gNnSc1	parkoviště
má	mít	k5eAaImIp3nS	mít
mít	mít	k5eAaImF	mít
širší	široký	k2eAgInSc1d2	širší
parkovací	parkovací	k2eAgInSc1d1	parkovací
místa	místo	k1gNnSc2	místo
a	a	k8xC	a
součástí	součást	k1gFnSc7	součást
objektu	objekt	k1gInSc2	objekt
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
pekárna	pekárna	k1gFnSc1	pekárna
<g/>
.	.	kIx.	.
</s>
<s>
Objekt	objekt	k1gInSc1	objekt
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
vytápět	vytápět	k5eAaImF	vytápět
odpadním	odpadní	k2eAgNnSc7d1	odpadní
teplem	teplo	k1gNnSc7	teplo
a	a	k8xC	a
budou	být	k5eAaImBp3nP	být
využity	využít	k5eAaPmNgFnP	využít
LED	LED	kA	LED
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Provozovna	provozovna	k1gFnSc1	provozovna
byla	být	k5eAaImAgFnS	být
otevřena	otevřít	k5eAaPmNgFnS	otevřít
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
(	(	kIx(	(
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
<g/>
)	)	kIx)	)
roku	rok	k1gInSc2	rok
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
dubna	duben	k1gInSc2	duben
roku	rok	k1gInSc2	rok
2019	[number]	k4	2019
byla	být	k5eAaImAgFnS	být
přechodně	přechodně	k6eAd1	přechodně
uzavřena	uzavřen	k2eAgFnSc1d1	uzavřena
prodejna	prodejna	k1gFnSc1	prodejna
společnosti	společnost	k1gFnSc2	společnost
Lidl	Lidla	k1gFnPc2	Lidla
ve	v	k7c6	v
Znojemské	znojemský	k2eAgFnSc6d1	Znojemská
ulici	ulice	k1gFnSc6	ulice
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
znovu	znovu	k6eAd1	znovu
otevřena	otevřen	k2eAgFnSc1d1	otevřena
<g/>
.	.	kIx.	.
<g/>
Zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
výstavbu	výstavba	k1gFnSc4	výstavba
hypermarketu	hypermarket	k1gInSc2	hypermarket
v	v	k7c6	v
Brněnské	brněnský	k2eAgFnSc6d1	brněnská
ulici	ulice	k1gFnSc6	ulice
projevila	projevit	k5eAaPmAgFnS	projevit
společnost	společnost	k1gFnSc1	společnost
Tesco	Tesco	k6eAd1	Tesco
<g/>
.	.	kIx.	.
</s>
<s>
Zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
supermarket	supermarket	k1gInSc4	supermarket
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Borovina	borovina	k1gFnSc1	borovina
<g/>
,	,	kIx,	,
v	v	k7c6	v
bývalé	bývalý	k2eAgFnSc6d1	bývalá
továrně	továrna	k1gFnSc6	továrna
BOPO	BOPO	kA	BOPO
projevila	projevit	k5eAaPmAgFnS	projevit
i	i	k9	i
společnost	společnost	k1gFnSc1	společnost
BILLA	Bill	k1gMnSc2	Bill
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Turismus	turismus	k1gInSc4	turismus
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
výstavbu	výstavba	k1gFnSc4	výstavba
obchodních	obchodní	k2eAgInPc2d1	obchodní
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
zvláště	zvláště	k6eAd1	zvláště
turismus	turismus	k1gInSc4	turismus
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
tuto	tento	k3xDgFnSc4	tento
dobu	doba	k1gFnSc4	doba
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
tři	tři	k4xCgNnPc1	tři
informační	informační	k2eAgNnPc1d1	informační
centra	centrum	k1gNnPc1	centrum
(	(	kIx(	(
<g/>
jsou	být	k5eAaImIp3nP	být
provozována	provozovat	k5eAaImNgFnS	provozovat
Městským	městský	k2eAgNnSc7d1	Městské
kulturním	kulturní	k2eAgNnSc7d1	kulturní
střediskem	středisko	k1gNnSc7	středisko
<g/>
)	)	kIx)	)
–	–	k?	–
v	v	k7c6	v
Malovaném	malovaný	k2eAgInSc6d1	malovaný
domě	dům	k1gInSc6	dům
na	na	k7c6	na
Karlově	Karlův	k2eAgNnSc6d1	Karlovo
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
,	,	kIx,	,
u	u	k7c2	u
Zadní	zadní	k2eAgFnSc2d1	zadní
synagogy	synagoga	k1gFnSc2	synagoga
a	a	k8xC	a
na	na	k7c6	na
Zámku	zámek	k1gInSc6	zámek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velký	velký	k2eAgMnSc1d1	velký
krokem	krokem	k6eAd1	krokem
kupředu	kupředu	k6eAd1	kupředu
pro	pro	k7c4	pro
rozvoj	rozvoj	k1gInSc4	rozvoj
turismu	turismus	k1gInSc2	turismus
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
bylo	být	k5eAaImAgNnS	být
zapsání	zapsání	k1gNnSc1	zapsání
Baziliky	bazilika	k1gFnSc2	bazilika
sv.	sv.	kA	sv.
Prokopa	Prokop	k1gMnSc2	Prokop
a	a	k8xC	a
Židovského	židovský	k2eAgNnSc2d1	Židovské
města	město	k1gNnSc2	město
s	s	k7c7	s
hřbitovem	hřbitov	k1gInSc7	hřbitov
na	na	k7c4	na
seznam	seznam	k1gInSc4	seznam
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
naučná	naučný	k2eAgFnSc1d1	naučná
stezka	stezka	k1gFnSc1	stezka
po	po	k7c6	po
památkách	památka	k1gFnPc6	památka
s	s	k7c7	s
audio	audio	k2eAgMnPc7d1	audio
průvodci	průvodce	k1gMnPc7	průvodce
a	a	k8xC	a
dvě	dva	k4xCgNnPc4	dva
z	z	k7c2	z
již	již	k6eAd1	již
zmíněných	zmíněný	k2eAgNnPc2d1	zmíněné
informačních	informační	k2eAgNnPc2d1	informační
center	centrum	k1gNnPc2	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Rozvoj	rozvoj	k1gInSc1	rozvoj
turistického	turistický	k2eAgInSc2d1	turistický
ruchu	ruch	k1gInSc2	ruch
prospívá	prospívat	k5eAaImIp3nS	prospívat
také	také	k9	také
obchodu	obchod	k1gInSc2	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
přílivu	příliv	k1gInSc3	příliv
turistů	turist	k1gMnPc2	turist
mohlo	moct	k5eAaImAgNnS	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
mnoho	mnoho	k4c4	mnoho
nových	nový	k2eAgFnPc2d1	nová
restaurací	restaurace	k1gFnPc2	restaurace
v	v	k7c6	v
Židovském	židovský	k2eAgNnSc6d1	Židovské
městě	město	k1gNnSc6	město
a	a	k8xC	a
také	také	k9	také
další	další	k2eAgNnPc4d1	další
zařízení	zařízení	k1gNnPc4	zařízení
pro	pro	k7c4	pro
ubytování	ubytování	k1gNnSc4	ubytování
turistu	turista	k1gMnSc4	turista
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
menší	malý	k2eAgInPc4d2	menší
penziony	penzion	k1gInPc4	penzion
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
O	o	k7c6	o
zápisu	zápis	k1gInSc6	zápis
památek	památka	k1gFnPc2	památka
na	na	k7c4	na
seznam	seznam	k1gInSc4	seznam
UNESCO	UNESCO	kA	UNESCO
se	se	k3xPyFc4	se
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
také	také	k9	také
začalo	začít	k5eAaPmAgNnS	začít
objevovat	objevovat	k5eAaImF	objevovat
větší	veliký	k2eAgNnSc4d2	veliký
množství	množství	k1gNnSc4	množství
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
turistů	turist	k1gMnPc2	turist
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
pocházejí	pocházet	k5eAaImIp3nP	pocházet
zpravidla	zpravidla	k6eAd1	zpravidla
z	z	k7c2	z
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
také	také	k9	také
podmíněno	podmínit	k5eAaPmNgNnS	podmínit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
státech	stát	k1gInPc6	stát
má	mít	k5eAaImIp3nS	mít
Třebíč	Třebíč	k1gFnSc1	Třebíč
svá	svůj	k3xOyFgNnPc4	svůj
partnerská	partnerský	k2eAgNnPc4d1	partnerské
města	město	k1gNnPc4	město
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yIgFnPc7	který
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
i	i	k9	i
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
turistiky	turistika	k1gFnSc2	turistika
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
potvrzuje	potvrzovat	k5eAaImIp3nS	potvrzovat
například	například	k6eAd1	například
umístění	umístění	k1gNnSc1	umístění
směrovky	směrovka	k1gFnSc2	směrovka
mířící	mířící	k2eAgFnSc2d1	mířící
na	na	k7c4	na
Třebíč	Třebíč	k1gFnSc4	Třebíč
na	na	k7c4	na
horu	hora	k1gFnSc4	hora
Muckenkogel	Muckenkogela	k1gFnPc2	Muckenkogela
ležící	ležící	k2eAgFnSc1d1	ležící
nad	nad	k7c7	nad
partnerským	partnerský	k2eAgNnSc7d1	partnerské
městem	město	k1gNnSc7	město
Lilienfeld	Lilienfelda	k1gFnPc2	Lilienfelda
<g/>
.	.	kIx.	.
<g/>
O	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Třebíč	Třebíč	k1gFnSc1	Třebíč
je	být	k5eAaImIp3nS	být
turisticky	turisticky	k6eAd1	turisticky
význačná	význačný	k2eAgFnSc1d1	význačná
i	i	k9	i
v	v	k7c6	v
evropském	evropský	k2eAgInSc6d1	evropský
pohledu	pohled	k1gInSc6	pohled
svědčí	svědčit	k5eAaImIp3nS	svědčit
také	také	k9	také
výrok	výrok	k1gInSc1	výrok
předsedy	předseda	k1gMnSc2	předseda
Evropské	evropský	k2eAgFnSc2d1	Evropská
komise	komise	k1gFnSc2	komise
J.	J.	kA	J.
M.	M.	kA	M.
Barrosa	Barrosa	k1gFnSc1	Barrosa
ve	v	k7c6	v
speciálním	speciální	k2eAgNnSc6d1	speciální
vydání	vydání	k1gNnSc6	vydání
pořadu	pořad	k1gInSc2	pořad
ČT	ČT	kA	ČT
Otázky	otázka	k1gFnPc1	otázka
Václava	Václav	k1gMnSc2	Václav
Moravce	Moravec	k1gMnSc2	Moravec
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2009	[number]	k4	2009
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Chci	chtít	k5eAaImIp1nS	chtít
ocenit	ocenit	k5eAaPmF	ocenit
premiéra	premiér	k1gMnSc4	premiér
Mirka	Mirek	k1gMnSc4	Mirek
Topolánka	Topolánek	k1gMnSc4	Topolánek
<g/>
,	,	kIx,	,
pochopil	pochopit	k5eAaPmAgMnS	pochopit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Evropa	Evropa	k1gFnSc1	Evropa
není	být	k5eNaImIp3nS	být
jen	jen	k9	jen
Brusel	Brusel	k1gInSc1	Brusel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
Londýn	Londýn	k1gInSc1	Londýn
<g/>
,	,	kIx,	,
Madrid	Madrid	k1gInSc1	Madrid
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
nebo	nebo	k8xC	nebo
Třebíč	Třebíč	k1gFnSc1	Třebíč
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
poprvé	poprvé	k6eAd1	poprvé
do	do	k7c2	do
Třebíče	Třebíč	k1gFnSc2	Třebíč
přicestovalo	přicestovat	k5eAaPmAgNnS	přicestovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
000	[number]	k4	000
turistů	turist	k1gMnPc2	turist
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
počet	počet	k1gInSc4	počet
přijíždějících	přijíždějící	k2eAgMnPc2d1	přijíždějící
turistů	turist	k1gMnPc2	turist
klesal	klesat	k5eAaImAgInS	klesat
<g/>
,	,	kIx,	,
růst	růst	k1gInSc1	růst
nastal	nastat	k5eAaPmAgInS	nastat
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
a	a	k8xC	a
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
i	i	k9	i
v	v	k7c6	v
roce	rok	k1gInSc6	rok
následujícím	následující	k2eAgInSc6d1	následující
<g/>
.	.	kIx.	.
</s>
<s>
MKS	MKS	kA	MKS
Třebíč	Třebíč	k1gFnSc1	Třebíč
<g/>
,	,	kIx,	,
příspěvková	příspěvkový	k2eAgFnSc1d1	příspěvková
organizace	organizace	k1gFnSc1	organizace
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
stará	starat	k5eAaImIp3nS	starat
o	o	k7c4	o
turistickou	turistický	k2eAgFnSc4d1	turistická
propagaci	propagace	k1gFnSc4	propagace
města	město	k1gNnSc2	město
tento	tento	k3xDgInSc1	tento
růst	růst	k1gInSc4	růst
přičítá	přičítat	k5eAaImIp3nS	přičítat
systematické	systematický	k2eAgFnSc3d1	systematická
propagaci	propagace	k1gFnSc3	propagace
na	na	k7c6	na
turistických	turistický	k2eAgInPc6d1	turistický
veletrzích	veletrh	k1gInPc6	veletrh
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yIgFnPc6	který
se	se	k3xPyFc4	se
organizace	organizace	k1gFnSc1	organizace
zaměřila	zaměřit	k5eAaPmAgFnS	zaměřit
na	na	k7c4	na
sousední	sousední	k2eAgFnPc4d1	sousední
země	zem	k1gFnPc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Významný	významný	k2eAgInSc1d1	významný
podíl	podíl	k1gInSc1	podíl
turistů	turist	k1gMnPc2	turist
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
cykloturistice	cykloturistika	k1gFnSc3	cykloturistika
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
zprovoznění	zprovoznění	k1gNnSc4	zprovoznění
cyklostezky	cyklostezka	k1gFnSc2	cyklostezka
Jihlava	Jihlava	k1gFnSc1	Jihlava
–	–	k?	–
Třebíč	Třebíč	k1gFnSc1	Třebíč
–	–	k?	–
Raabs	Raabs	k1gInSc1	Raabs
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2015	[number]	k4	2015
bylo	být	k5eAaImAgNnS	být
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
největší	veliký	k2eAgInSc1d3	veliký
počet	počet	k1gInSc1	počet
turistů	turist	k1gMnPc2	turist
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
projdou	projít	k5eAaPmIp3nP	projít
informačními	informační	k2eAgNnPc7d1	informační
centry	centrum	k1gNnPc7	centrum
ve	v	k7c6	v
městě	město	k1gNnSc6	město
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
USA	USA	kA	USA
<g/>
,	,	kIx,	,
další	další	k2eAgInSc1d1	další
signifikantní	signifikantní	k2eAgInSc1d1	signifikantní
podíl	podíl	k1gInSc1	podíl
patří	patřit	k5eAaImIp3nS	patřit
turistům	turist	k1gMnPc3	turist
z	z	k7c2	z
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
Rakouska	Rakousko	k1gNnSc2	Rakousko
a	a	k8xC	a
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
se	se	k3xPyFc4	se
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
počty	počet	k1gInPc1	počet
turistů	turist	k1gMnPc2	turist
z	z	k7c2	z
Izraele	Izrael	k1gInSc2	Izrael
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
propagace	propagace	k1gFnSc2	propagace
město	město	k1gNnSc1	město
pracuje	pracovat	k5eAaImIp3nS	pracovat
s	s	k7c7	s
tour	tour	k1gInSc4	tour
operátory	operátor	k1gInPc4	operátor
či	či	k8xC	či
novináři	novinář	k1gMnPc1	novinář
se	se	k3xPyFc4	se
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
září	září	k1gNnSc4	září
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
otevření	otevření	k1gNnSc3	otevření
rozhledny	rozhledna	k1gFnSc2	rozhledna
s	s	k7c7	s
expozicemi	expozice	k1gFnPc7	expozice
vodárenství	vodárenství	k1gNnSc2	vodárenství
v	v	k7c6	v
objektu	objekt	k1gInSc6	objekt
bývalého	bývalý	k2eAgInSc2d1	bývalý
vodojemu	vodojem	k1gInSc2	vodojem
na	na	k7c6	na
vrchu	vrch	k1gInSc6	vrch
Strážné	strážný	k2eAgFnSc2d1	Strážná
hory	hora	k1gFnSc2	hora
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
bylo	být	k5eAaImAgNnS	být
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
velmi	velmi	k6eAd1	velmi
oblíbeným	oblíbený	k2eAgInSc7d1	oblíbený
suvenýrem	suvenýr	k1gInSc7	suvenýr
pro	pro	k7c4	pro
turisty	turist	k1gMnPc4	turist
přijíždějící	přijíždějící	k2eAgMnPc4d1	přijíždějící
do	do	k7c2	do
Třebíče	Třebíč	k1gFnSc2	Třebíč
jsou	být	k5eAaImIp3nP	být
čokoládové	čokoládový	k2eAgFnPc4d1	čokoládová
pralinky	pralinka	k1gFnPc4	pralinka
s	s	k7c7	s
motivy	motiv	k1gInPc7	motiv
památek	památka	k1gFnPc2	památka
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
byl	být	k5eAaImAgMnS	být
ve	v	k7c6	v
městě	město	k1gNnSc6	město
otevřen	otevřen	k2eAgInSc1d1	otevřen
hotel	hotel	k1gInSc1	hotel
Alfa	alfa	k1gFnSc1	alfa
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
byl	být	k5eAaImAgInS	být
otevřen	otevřít	k5eAaPmNgInS	otevřít
hotel	hotel	k1gInSc1	hotel
Slavia	Slavium	k1gNnSc2	Slavium
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
byla	být	k5eAaImAgFnS	být
dokončena	dokončen	k2eAgFnSc1d1	dokončena
<g />
.	.	kIx.	.
</s>
<s>
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
Hotelu	hotel	k1gInSc2	hotel
Zlatý	zlatý	k2eAgInSc1d1	zlatý
Kříž	kříž	k1gInSc1	kříž
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
jsou	být	k5eAaImIp3nP	být
zastoupeny	zastoupit	k5eAaPmNgFnP	zastoupit
ubytovací	ubytovací	k2eAgFnPc1d1	ubytovací
zařízení	zařízení	k1gNnSc2	zařízení
několika	několik	k4yIc2	několik
úrovní	úroveň	k1gFnPc2	úroveň
<g/>
,	,	kIx,	,
malé	malý	k2eAgInPc4d1	malý
penziony	penzion	k1gInPc4	penzion
i	i	k8xC	i
větší	veliký	k2eAgInPc4d2	veliký
hotely	hotel	k1gInPc4	hotel
<g/>
,	,	kIx,	,
ve	v	k7c6	v
městě	město	k1gNnSc6	město
není	být	k5eNaImIp3nS	být
žádný	žádný	k3yNgInSc1	žádný
hotel	hotel	k1gInSc1	hotel
většího	veliký	k2eAgInSc2d2	veliký
významu	význam	k1gInSc2	význam
<g/>
,	,	kIx,	,
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
byl	být	k5eAaImAgInS	být
dobudován	dobudován	k2eAgInSc1d1	dobudován
Hotel	hotel	k1gInSc1	hotel
Joseph	Joseph	k1gInSc1	Joseph
1699	[number]	k4	1699
v	v	k7c6	v
bývalém	bývalý	k2eAgInSc6d1	bývalý
chudobinci	chudobinec	k1gInSc6	chudobinec
v	v	k7c6	v
židovské	židovský	k2eAgFnSc6d1	židovská
čtvrti	čtvrt	k1gFnSc6	čtvrt
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
společnou	společný	k2eAgFnSc7d1	společná
investicí	investice	k1gFnSc7	investice
společností	společnost	k1gFnPc2	společnost
EuroAgentur	EuroAgentura	k1gFnPc2	EuroAgentura
a	a	k8xC	a
soukromého	soukromý	k2eAgMnSc2d1	soukromý
investora	investor	k1gMnSc2	investor
<g/>
,	,	kIx,	,
hotel	hotel	k1gInSc1	hotel
byl	být	k5eAaImAgInS	být
spolufinancován	spolufinancovat	k5eAaImNgInS	spolufinancovat
Evropským	evropský	k2eAgInSc7d1	evropský
fondem	fond	k1gInSc7	fond
pro	pro	k7c4	pro
regionální	regionální	k2eAgInSc4d1	regionální
rozvoj	rozvoj	k1gInSc4	rozvoj
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
tento	tento	k3xDgInSc4	tento
hotel	hotel	k1gInSc4	hotel
se	se	k3xPyFc4	se
v	v	k7c6	v
prostorách	prostora	k1gFnPc6	prostora
židovské	židovský	k2eAgFnSc2d1	židovská
čtvrti	čtvrt	k1gFnSc2	čtvrt
nacházel	nacházet	k5eAaImAgMnS	nacházet
i	i	k9	i
hostel	hostel	k1gMnSc1	hostel
řetězce	řetězec	k1gInSc2	řetězec
Travellers	Travellers	k1gInSc1	Travellers
<g/>
'	'	kIx"	'
Hostel	Hostel	k1gInSc1	Hostel
a	a	k8xC	a
penzion	penzion	k1gInSc1	penzion
U	u	k7c2	u
Synagogy	synagoga	k1gFnSc2	synagoga
<g/>
,	,	kIx,	,
další	další	k2eAgFnPc4d1	další
ubytovací	ubytovací	k2eAgFnPc4d1	ubytovací
kapacity	kapacita	k1gFnPc4	kapacita
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
hotely	hotel	k1gInPc1	hotel
Grand	grand	k1gMnSc1	grand
(	(	kIx(	(
<g/>
kapacita	kapacita	k1gFnSc1	kapacita
210	[number]	k4	210
lůžek	lůžko	k1gNnPc2	lůžko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Zlatý	zlatý	k2eAgInSc1d1	zlatý
kříž	kříž	k1gInSc1	kříž
(	(	kIx(	(
<g/>
77	[number]	k4	77
lůžek	lůžko	k1gNnPc2	lůžko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Atom	atom	k1gInSc1	atom
(	(	kIx(	(
<g/>
72	[number]	k4	72
lůžek	lůžko	k1gNnPc2	lůžko
<g/>
)	)	kIx)	)
a	a	k8xC	a
další	další	k2eAgNnPc1d1	další
marginální	marginální	k2eAgNnPc1d1	marginální
ubytovací	ubytovací	k2eAgNnPc1d1	ubytovací
zařízení	zařízení	k1gNnPc1	zařízení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Okresní	okresní	k2eAgFnSc1d1	okresní
hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
komora	komora	k1gFnSc1	komora
při	při	k7c6	při
své	svůj	k3xOyFgFnSc6	svůj
konferenci	konference	k1gFnSc6	konference
oznámila	oznámit	k5eAaPmAgFnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
pro	pro	k7c4	pro
rok	rok	k1gInSc4	rok
2017	[number]	k4	2017
chtěla	chtít	k5eAaImAgFnS	chtít
do	do	k7c2	do
Třebíče	Třebíč	k1gFnSc2	Třebíč
zavést	zavést	k5eAaPmF	zavést
tzv.	tzv.	kA	tzv.
turistický	turistický	k2eAgInSc4d1	turistický
vláček	vláček	k1gInSc4	vláček
<g/>
,	,	kIx,	,
půjčovnu	půjčovna	k1gFnSc4	půjčovna
elektrokol	elektrokola	k1gFnPc2	elektrokola
a	a	k8xC	a
elektromobilů	elektromobil	k1gInPc2	elektromobil
a	a	k8xC	a
další	další	k2eAgNnSc4d1	další
vydávání	vydávání	k1gNnSc4	vydávání
Výletních	výletní	k2eAgFnPc2d1	výletní
novin	novina	k1gFnPc2	novina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
plánu	plán	k1gInSc6	plán
je	být	k5eAaImIp3nS	být
i	i	k9	i
splavnění	splavnění	k1gNnSc1	splavnění
řeky	řeka	k1gFnSc2	řeka
Jihlavy	Jihlava	k1gFnSc2	Jihlava
pro	pro	k7c4	pro
turistické	turistický	k2eAgInPc4d1	turistický
účely	účel	k1gInPc4	účel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
modeláře	modelář	k1gMnSc2	modelář
Stanislava	Stanislav	k1gMnSc2	Stanislav
Vršky	vršek	k1gInPc1	vršek
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
manželky	manželka	k1gFnSc2	manželka
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
další	další	k2eAgFnSc1d1	další
část	část	k1gFnSc1	část
modelu	model	k1gInSc2	model
historické	historický	k2eAgFnSc2d1	historická
Třebíče	Třebíč	k1gFnSc2	Třebíč
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
100	[number]	k4	100
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
stavbě	stavba	k1gFnSc6	stavba
část	část	k1gFnSc4	část
města	město	k1gNnSc2	město
Stařečka	stařeček	k1gMnSc2	stařeček
<g/>
.	.	kIx.	.
</s>
<s>
Model	model	k1gInSc1	model
pak	pak	k6eAd1	pak
byl	být	k5eAaImAgInS	být
dokončen	dokončit	k5eAaPmNgInS	dokončit
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
a	a	k8xC	a
od	od	k7c2	od
května	květen	k1gInSc2	květen
2017	[number]	k4	2017
je	být	k5eAaImIp3nS	být
vystaven	vystavit	k5eAaPmNgInS	vystavit
v	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
Pasáž	pasáž	k1gFnSc1	pasáž
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
modelu	model	k1gInSc2	model
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
3	[number]	k4	3
<g/>
x	x	k?	x
<g/>
3	[number]	k4	3
metry	metr	k1gInPc4	metr
<g/>
,	,	kIx,	,
umístění	umístění	k1gNnSc4	umístění
v	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
dočasné	dočasný	k2eAgNnSc1d1	dočasné
<g/>
,	,	kIx,	,
posléze	posléze	k6eAd1	posléze
bude	být	k5eAaImBp3nS	být
přemístěn	přemístit	k5eAaPmNgInS	přemístit
na	na	k7c4	na
vhodnější	vhodný	k2eAgNnSc4d2	vhodnější
a	a	k8xC	a
přístupnější	přístupný	k2eAgNnSc4d2	přístupnější
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Majitelem	majitel	k1gMnSc7	majitel
modelu	model	k1gInSc2	model
je	být	k5eAaImIp3nS	být
společnost	společnost	k1gFnSc1	společnost
Kapucín	kapucín	k1gMnSc1	kapucín
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
jej	on	k3xPp3gMnSc4	on
městu	město	k1gNnSc3	město
zapůjčila	zapůjčit	k5eAaPmAgFnS	zapůjčit
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
chtělo	chtít	k5eAaImAgNnS	chtít
model	model	k1gInSc4	model
zakoupit	zakoupit	k5eAaPmF	zakoupit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zastupitelé	zastupitel	k1gMnPc1	zastupitel
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
vyjádřili	vyjádřit	k5eAaPmAgMnP	vyjádřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nákup	nákup	k1gInSc1	nákup
nebude	být	k5eNaImBp3nS	být
proveden	provést	k5eAaPmNgInS	provést
a	a	k8xC	a
majitelem	majitel	k1gMnSc7	majitel
je	být	k5eAaImIp3nS	být
nadále	nadále	k6eAd1	nadále
společnost	společnost	k1gFnSc4	společnost
Kapucín	kapucín	k1gMnSc1	kapucín
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
však	však	k9	však
město	město	k1gNnSc1	město
rozhodlo	rozhodnout	k5eAaPmAgNnS	rozhodnout
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
model	model	k1gInSc1	model
za	za	k7c4	za
1,2	[number]	k4	1,2
milionu	milion	k4xCgInSc2	milion
Kč	Kč	kA	Kč
zakoupí	zakoupit	k5eAaPmIp3nS	zakoupit
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
bylo	být	k5eAaImAgNnS	být
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
město	město	k1gNnSc1	město
zažádá	zažádat	k5eAaPmIp3nS	zažádat
o	o	k7c4	o
dotaci	dotace	k1gFnSc4	dotace
na	na	k7c6	na
pořízení	pořízení	k1gNnSc6	pořízení
16	[number]	k4	16
koloběžek	koloběžka	k1gFnPc2	koloběžka
s	s	k7c7	s
elektrickým	elektrický	k2eAgInSc7d1	elektrický
pohonem	pohon	k1gInSc7	pohon
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
budou	být	k5eAaImBp3nP	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
turistům	turist	k1gMnPc3	turist
na	na	k7c6	na
čtyřech	čtyři	k4xCgNnPc6	čtyři
nabíjecích	nabíjecí	k2eAgNnPc6d1	nabíjecí
místech	místo	k1gNnPc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Dobíjecí	dobíjecí	k2eAgFnPc1d1	dobíjecí
stanice	stanice	k1gFnPc1	stanice
mají	mít	k5eAaImIp3nP	mít
být	být	k5eAaImF	být
umístěny	umístit	k5eAaPmNgInP	umístit
u	u	k7c2	u
Baziliky	bazilika	k1gFnSc2	bazilika
svatého	svatý	k2eAgMnSc2d1	svatý
Prokopa	Prokop	k1gMnSc2	Prokop
<g/>
,	,	kIx,	,
Zadní	zadní	k2eAgMnPc1d1	zadní
synagogy	synagoga	k1gFnSc2	synagoga
<g/>
,	,	kIx,	,
na	na	k7c6	na
Karlově	Karlův	k2eAgNnSc6d1	Karlovo
náměstí	náměstí	k1gNnSc6	náměstí
a	a	k8xC	a
u	u	k7c2	u
ekocentra	ekocentrum	k1gNnSc2	ekocentrum
Alternátor	alternátor	k1gInSc1	alternátor
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
město	město	k1gNnSc1	město
navštívilo	navštívit	k5eAaPmAgNnS	navštívit
celkem	celkem	k6eAd1	celkem
o	o	k7c4	o
12	[number]	k4	12
%	%	kIx~	%
turistů	turist	k1gMnPc2	turist
více	hodně	k6eAd2	hodně
než	než	k8xS	než
o	o	k7c4	o
rok	rok	k1gInSc4	rok
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kultura	kultura	k1gFnSc1	kultura
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
se	se	k3xPyFc4	se
o	o	k7c6	o
uspokojení	uspokojení	k1gNnSc6	uspokojení
potřeb	potřeba	k1gFnPc2	potřeba
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
kulturního	kulturní	k2eAgInSc2d1	kulturní
a	a	k8xC	a
společenského	společenský	k2eAgInSc2d1	společenský
života	život	k1gInSc2	život
stará	starat	k5eAaImIp3nS	starat
příspěvková	příspěvkový	k2eAgFnSc1d1	příspěvková
organizace	organizace	k1gFnSc1	organizace
města	město	k1gNnSc2	město
Třebíče	Třebíč	k1gFnSc2	Třebíč
Městské	městský	k2eAgNnSc1d1	Městské
kulturní	kulturní	k2eAgNnSc1d1	kulturní
středisko	středisko	k1gNnSc1	středisko
(	(	kIx(	(
<g/>
jinak	jinak	k6eAd1	jinak
také	také	k9	také
MKS	MKS	kA	MKS
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Městské	městský	k2eAgNnSc1d1	Městské
kulturní	kulturní	k2eAgNnSc1d1	kulturní
středisko	středisko	k1gNnSc1	středisko
provozuje	provozovat	k5eAaImIp3nS	provozovat
některé	některý	k3yIgFnSc3	některý
z	z	k7c2	z
kulturních	kulturní	k2eAgFnPc2d1	kulturní
organizací	organizace	k1gFnPc2	organizace
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
např.	např.	kA	např.
kino	kino	k1gNnSc1	kino
Pasáž	pasáž	k1gFnSc1	pasáž
<g/>
,	,	kIx,	,
Národní	národní	k2eAgInSc1d1	národní
dům	dům	k1gInSc1	dům
s	s	k7c7	s
Galerií	galerie	k1gFnSc7	galerie
Franta	Franta	k1gMnSc1	Franta
<g/>
,	,	kIx,	,
Zadní	zadní	k2eAgFnSc4d1	zadní
synagogu	synagoga	k1gFnSc4	synagoga
<g/>
,	,	kIx,	,
Fórum	fórum	k1gNnSc1	fórum
<g/>
,	,	kIx,	,
Klub	klub	k1gInSc1	klub
Hájek	hájek	k1gInSc1	hájek
<g/>
,	,	kIx,	,
Divadlo	divadlo	k1gNnSc1	divadlo
Pasáž	pasáž	k1gFnSc1	pasáž
<g/>
,	,	kIx,	,
Malovaný	malovaný	k2eAgInSc4d1	malovaný
dům	dům	k1gInSc4	dům
<g/>
,	,	kIx,	,
Galerii	galerie	k1gFnSc4	galerie
Ladislava	Ladislav	k1gMnSc2	Ladislav
Nováka	Novák	k1gMnSc2	Novák
a	a	k8xC	a
Městskou	městský	k2eAgFnSc4d1	městská
věž	věž	k1gFnSc4	věž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Divadelní	divadelní	k2eAgFnSc1d1	divadelní
a	a	k8xC	a
hudební	hudební	k2eAgFnSc1d1	hudební
produkce	produkce	k1gFnSc1	produkce
===	===	k?	===
</s>
</p>
<p>
<s>
Kulturní	kulturní	k2eAgNnSc1d1	kulturní
vyžití	vyžití	k1gNnSc1	vyžití
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
Divadlo	divadlo	k1gNnSc1	divadlo
Pasáž	pasáž	k1gFnSc1	pasáž
na	na	k7c6	na
Masarykově	Masarykův	k2eAgNnSc6d1	Masarykovo
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
,	,	kIx,	,
kino	kino	k1gNnSc4	kino
Pasáž	pasáž	k1gFnSc1	pasáž
a	a	k8xC	a
Národní	národní	k2eAgInSc1d1	národní
dům	dům	k1gInSc1	dům
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
součástí	součást	k1gFnSc7	součást
kongresového	kongresový	k2eAgNnSc2d1	Kongresové
centra	centrum	k1gNnSc2	centrum
Pasáž	pasáž	k1gFnSc1	pasáž
je	být	k5eAaImIp3nS	být
i	i	k9	i
divadelní	divadelní	k2eAgFnSc1d1	divadelní
infrastruktura	infrastruktura	k1gFnSc1	infrastruktura
<g/>
,	,	kIx,	,
stálou	stálý	k2eAgFnSc4d1	stálá
profesionální	profesionální	k2eAgFnSc4d1	profesionální
scénu	scéna	k1gFnSc4	scéna
Třebíč	Třebíč	k1gFnSc4	Třebíč
nemá	mít	k5eNaImIp3nS	mít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
1940	[number]	k4	1940
založena	založen	k2eAgFnSc1d1	založena
a	a	k8xC	a
zahájila	zahájit	k5eAaPmAgFnS	zahájit
činnost	činnost	k1gFnSc4	činnost
profesionální	profesionální	k2eAgFnSc1d1	profesionální
scéna	scéna	k1gFnSc1	scéna
Horáckého	horácký	k2eAgNnSc2d1	Horácké
divadla	divadlo	k1gNnSc2	divadlo
<g/>
,	,	kIx,	,
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
přesunutého	přesunutý	k2eAgInSc2d1	přesunutý
do	do	k7c2	do
Jihlavy	Jihlava	k1gFnSc2	Jihlava
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
příspěvkové	příspěvkový	k2eAgFnPc1d1	příspěvková
organizace	organizace	k1gFnPc1	organizace
Kraje	kraj	k1gInSc2	kraj
Vysočina	vysočina	k1gFnSc1	vysočina
<g/>
.	.	kIx.	.
<g/>
Hudební	hudební	k2eAgFnSc1d1	hudební
produkce	produkce	k1gFnSc1	produkce
probíhají	probíhat	k5eAaImIp3nP	probíhat
v	v	k7c6	v
prostorách	prostora	k1gFnPc6	prostora
Národního	národní	k2eAgInSc2d1	národní
domu	dům	k1gInSc2	dům
na	na	k7c6	na
Karlově	Karlův	k2eAgNnSc6d1	Karlovo
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
v	v	k7c6	v
sálech	sál	k1gInPc6	sál
kulturního	kulturní	k2eAgNnSc2d1	kulturní
zařízení	zařízení	k1gNnSc2	zařízení
Fórum	fórum	k1gNnSc1	fórum
<g/>
,	,	kIx,	,
na	na	k7c6	na
Karlově	Karlův	k2eAgNnSc6d1	Karlovo
náměstí	náměstí	k1gNnSc6	náměstí
a	a	k8xC	a
v	v	k7c6	v
několika	několik	k4yIc6	několik
hudebních	hudební	k2eAgInPc6d1	hudební
klubech	klub	k1gInPc6	klub
jako	jako	k8xC	jako
Béčko	béčko	k1gNnSc1	béčko
<g/>
,	,	kIx,	,
Paradise	Paradise	k1gFnSc1	Paradise
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Ponorka	ponorka	k1gFnSc1	ponorka
<g/>
,	,	kIx,	,
Florida	Florida	k1gFnSc1	Florida
<g/>
,	,	kIx,	,
Black	Black	k1gInSc1	Black
<g/>
&	&	k?	&
<g/>
White	Whit	k1gMnSc5	Whit
<g/>
,	,	kIx,	,
Golem	Golem	k1gMnSc1	Golem
<g/>
,	,	kIx,	,
Level	level	k1gInSc1	level
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Club-R	Club-R	k1gFnSc1	Club-R
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Roxy	Roxa	k1gMnSc2	Roxa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Znojemka	Znojemka	k1gFnSc1	Znojemka
či	či	k8xC	či
v	v	k7c6	v
nedaleké	daleký	k2eNgFnSc6d1	nedaleká
cihelně	cihelna	k1gFnSc6	cihelna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
klub	klub	k1gInSc1	klub
Cihelna	cihelna	k1gFnSc1	cihelna
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
menších	malý	k2eAgInPc6d2	menší
klubech	klub	k1gInPc6	klub
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
byl	být	k5eAaImAgInS	být
slavnostně	slavnostně	k6eAd1	slavnostně
ukončen	ukončit	k5eAaPmNgInS	ukončit
provoz	provoz	k1gInSc1	provoz
klubu	klub	k1gInSc2	klub
B	B	kA	B
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
Béčka	béčko	k1gNnSc2	béčko
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
z	z	k7c2	z
klubu	klub	k1gInSc2	klub
měl	mít	k5eAaImAgInS	mít
stát	stát	k5eAaPmF	stát
putovní	putovní	k2eAgInSc1d1	putovní
klub	klub	k1gInSc1	klub
bez	bez	k7c2	bez
stálých	stálý	k2eAgFnPc2d1	stálá
prostor	prostora	k1gFnPc2	prostora
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kulturní	kulturní	k2eAgInPc4d1	kulturní
festivaly	festival	k1gInPc4	festival
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
se	se	k3xPyFc4	se
pořádají	pořádat	k5eAaImIp3nP	pořádat
různé	různý	k2eAgFnPc4d1	různá
tradiční	tradiční	k2eAgFnPc4d1	tradiční
i	i	k8xC	i
mimořádné	mimořádný	k2eAgFnPc4d1	mimořádná
kulturní	kulturní	k2eAgFnPc4d1	kulturní
akce	akce	k1gFnPc4	akce
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
každoročně	každoročně	k6eAd1	každoročně
konaný	konaný	k2eAgInSc1d1	konaný
hudební	hudební	k2eAgInSc1d1	hudební
festival	festival	k1gInSc1	festival
Zámostí	Zámost	k1gFnPc2	Zámost
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc4	jenž
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
na	na	k7c6	na
říční	říční	k2eAgFnSc6d1	říční
Nivě	niva	k1gFnSc6	niva
<g/>
,	,	kIx,	,
první	první	k4xOgInSc1	první
ročník	ročník	k1gInSc1	ročník
festivalu	festival	k1gInSc2	festival
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
odehrával	odehrávat	k5eAaImAgInS	odehrávat
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
Subakovy	Subakův	k2eAgFnSc2d1	Subakova
koželužny	koželužna	k1gFnSc2	koželužna
v	v	k7c6	v
židovském	židovský	k2eAgNnSc6d1	Židovské
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
navazování	navazování	k1gNnSc2	navazování
na	na	k7c4	na
židovskou	židovský	k2eAgFnSc4d1	židovská
tradici	tradice	k1gFnSc4	tradice
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
konal	konat	k5eAaImAgInS	konat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
pod	pod	k7c7	pod
patronací	patronace	k1gFnSc7	patronace
Arnošta	Arnošt	k1gMnSc2	Arnošt
Lustiga	Lustig	k1gMnSc2	Lustig
festival	festival	k1gInSc1	festival
židovských	židovský	k2eAgFnPc2d1	židovská
tradic	tradice	k1gFnPc2	tradice
Šamajim	Šamajima	k1gFnPc2	Šamajima
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
přejmenování	přejmenování	k1gNnSc3	přejmenování
na	na	k7c4	na
Třebíčský	třebíčský	k2eAgInSc4d1	třebíčský
židovský	židovský	k2eAgInSc4d1	židovský
festival	festival	k1gInSc4	festival
<g/>
,	,	kIx,	,
současně	současně	k6eAd1	současně
s	s	k7c7	s
přejmenováním	přejmenování	k1gNnSc7	přejmenování
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
i	i	k9	i
změna	změna	k1gFnSc1	změna
patrona	patron	k1gMnSc2	patron
<g/>
,	,	kIx,	,
kterým	který	k3yQgMnSc7	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Arnošt	Arnošt	k1gMnSc1	Arnošt
Goldflam	Goldflam	k1gInSc4	Goldflam
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
se	se	k3xPyFc4	se
před	před	k7c7	před
letními	letní	k2eAgFnPc7d1	letní
prázdninami	prázdniny	k1gFnPc7	prázdniny
koná	konat	k5eAaImIp3nS	konat
i	i	k9	i
festival	festival	k1gInSc1	festival
Country	country	k2eAgFnSc2d1	country
hudby	hudba	k1gFnSc2	hudba
a	a	k8xC	a
tanců	tanec	k1gInPc2	tanec
–	–	k?	–
Country	country	k2eAgFnPc1d1	country
fest	fest	k6eAd1	fest
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
se	se	k3xPyFc4	se
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
pořádal	pořádat	k5eAaImAgInS	pořádat
i	i	k9	i
festival	festival	k1gInSc1	festival
etnické	etnický	k2eAgFnSc2d1	etnická
hudby	hudba	k1gFnSc2	hudba
a	a	k8xC	a
kultury	kultura	k1gFnSc2	kultura
Třebíčský	třebíčský	k2eAgInSc1d1	třebíčský
Zvonek	zvonek	k1gInSc1	zvonek
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
i	i	k8xC	i
folklorní	folklorní	k2eAgFnPc1d1	folklorní
slavnosti	slavnost	k1gFnPc1	slavnost
s	s	k7c7	s
názvem	název	k1gInSc7	název
Třebíčské	třebíčský	k2eAgNnSc1d1	třebíčské
Bramborobraní	Bramborobraní	k1gNnSc1	Bramborobraní
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
název	název	k1gInSc1	název
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
změnil	změnit	k5eAaPmAgMnS	změnit
na	na	k7c6	na
Folklórní	folklórní	k2eAgFnSc6d1	folklórní
slavnosti	slavnost	k1gFnSc6	slavnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
probíhá	probíhat	k5eAaImIp3nS	probíhat
divadelní	divadelní	k2eAgInSc4d1	divadelní
festival	festival	k1gInSc4	festival
malých	malý	k2eAgInPc2d1	malý
souborů	soubor	k1gInPc2	soubor
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Festival	festival	k1gInSc4	festival
divadla	divadlo	k1gNnSc2	divadlo
2-3-4	[number]	k4	2-3-4
herců	herec	k1gMnPc2	herec
<g/>
,	,	kIx,	,
Třebíč	Třebíč	k1gFnSc1	Třebíč
je	být	k5eAaImIp3nS	být
také	také	k9	také
městem	město	k1gNnSc7	město
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
část	část	k1gFnSc1	část
festivalu	festival	k1gInSc2	festival
klasické	klasický	k2eAgFnSc2d1	klasická
hudby	hudba	k1gFnSc2	hudba
Concentus	Concentus	k1gMnSc1	Concentus
Moraviae	Moraviae	k1gFnSc1	Moraviae
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
každoročně	každoročně	k6eAd1	každoročně
Třebíčský	třebíčský	k2eAgInSc1d1	třebíčský
operní	operní	k2eAgInSc1d1	operní
festival	festival	k1gInSc1	festival
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
se	s	k7c7	s
zařazením	zařazení	k1gNnSc7	zařazení
židovských	židovský	k2eAgFnPc2d1	židovská
památek	památka	k1gFnPc2	památka
a	a	k8xC	a
baziliky	bazilika	k1gFnSc2	bazilika
se	se	k3xPyFc4	se
ve	v	k7c6	v
městě	město	k1gNnSc6	město
pořádají	pořádat	k5eAaImIp3nP	pořádat
středověké	středověký	k2eAgFnPc1d1	středověká
slavnosti	slavnost	k1gFnPc1	slavnost
s	s	k7c7	s
průvodem	průvod	k1gInSc7	průvod
historických	historický	k2eAgFnPc2d1	historická
postav	postava	k1gFnPc2	postava
a	a	k8xC	a
středověkými	středověký	k2eAgFnPc7d1	středověká
slavnostmi	slavnost	k1gFnPc7	slavnost
<g/>
,	,	kIx,	,
po	po	k7c6	po
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
byly	být	k5eAaImAgFnP	být
nazvány	nazván	k2eAgFnPc1d1	nazvána
Slavnosti	slavnost	k1gFnPc1	slavnost
tří	tři	k4xCgFnPc2	tři
kápí	kápě	k1gFnPc2	kápě
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
se	se	k3xPyFc4	se
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
říčních	říční	k2eAgFnPc2d1	říční
lázní	lázeň	k1gFnPc2	lázeň
na	na	k7c6	na
tzv.	tzv.	kA	tzv.
Polance	polanka	k1gFnSc6	polanka
koná	konat	k5eAaImIp3nS	konat
festival	festival	k1gInSc1	festival
hudby	hudba	k1gFnSc2	hudba
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Polanka	polanka	k1gFnSc1	polanka
Fest	fest	k6eAd1	fest
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Muzejnictví	muzejnictví	k1gNnSc2	muzejnictví
===	===	k?	===
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
i	i	k9	i
Muzeum	muzeum	k1gNnSc4	muzeum
Vysočiny	vysočina	k1gFnSc2	vysočina
Třebíč	Třebíč	k1gFnSc1	Třebíč
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
prostorách	prostora	k1gFnPc6	prostora
bývalého	bývalý	k2eAgInSc2d1	bývalý
zámku	zámek	k1gInSc2	zámek
<g/>
;	;	kIx,	;
Galerie	galerie	k1gFnSc2	galerie
Malovaný	malovaný	k2eAgInSc1d1	malovaný
dům	dům	k1gInSc1	dům
v	v	k7c6	v
Malovaném	malovaný	k2eAgInSc6d1	malovaný
domě	dům	k1gInSc6	dům
a	a	k8xC	a
galerie	galerie	k1gFnSc2	galerie
v	v	k7c6	v
Zadní	zadní	k2eAgFnSc6d1	zadní
synagoze	synagoga	k1gFnSc6	synagoga
<g/>
;	;	kIx,	;
výstavy	výstava	k1gFnPc1	výstava
se	se	k3xPyFc4	se
konají	konat	k5eAaImIp3nP	konat
i	i	k9	i
v	v	k7c6	v
kulturním	kulturní	k2eAgNnSc6d1	kulturní
centru	centrum	k1gNnSc6	centrum
náležícím	náležící	k2eAgNnSc6d1	náležící
k	k	k7c3	k
divadlu	divadlo	k1gNnSc3	divadlo
Pasáž	pasáž	k1gFnSc1	pasáž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Zámostí	Zámost	k1gFnPc2	Zámost
otevřena	otevřít	k5eAaPmNgFnS	otevřít
stálá	stálý	k2eAgFnSc1d1	stálá
výstava	výstava	k1gFnSc1	výstava
věnovaná	věnovaný	k2eAgFnSc1d1	věnovaná
třebíčskému	třebíčský	k2eAgMnSc3d1	třebíčský
spisovateli	spisovatel	k1gMnSc3	spisovatel
Ladislavu	Ladislav	k1gMnSc3	Ladislav
Novákovi	Novák	k1gMnSc3	Novák
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
transformovala	transformovat	k5eAaBmAgFnS	transformovat
do	do	k7c2	do
Galerie	galerie	k1gFnSc2	galerie
Ladislava	Ladislav	k1gMnSc2	Ladislav
Nováka	Novák	k1gMnSc2	Novák
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
jeho	jeho	k3xOp3gNnPc1	jeho
díla	dílo	k1gNnPc1	dílo
jsou	být	k5eAaImIp3nP	být
vystavena	vystaven	k2eAgNnPc1d1	vystaveno
i	i	k9	i
ve	v	k7c6	v
vinárně	vinárna	k1gFnSc6	vinárna
Ráchel	Ráchel	k1gFnSc2	Ráchel
v	v	k7c6	v
židovské	židovský	k2eAgFnSc6d1	židovská
čtvrti	čtvrt	k1gFnSc6	čtvrt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
byla	být	k5eAaImAgFnS	být
slavnostně	slavnostně	k6eAd1	slavnostně
otevřena	otevřen	k2eAgFnSc1d1	otevřena
výstava	výstava	k1gFnSc1	výstava
třebíčského	třebíčský	k2eAgMnSc2d1	třebíčský
rodáka	rodák	k1gMnSc2	rodák
Františka	František	k1gMnSc2	František
Mertla	Mertl	k1gMnSc2	Mertl
<g/>
,	,	kIx,	,
řečeného	řečený	k2eAgMnSc2d1	řečený
Franty	Franta	k1gMnSc2	Franta
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
školy	škola	k1gFnSc2	škola
je	být	k5eAaImIp3nS	být
i	i	k9	i
Muzeum	muzeum	k1gNnSc1	muzeum
staré	starý	k2eAgFnSc2d1	stará
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
techniky	technika	k1gFnSc2	technika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
ve	v	k7c6	v
městě	město	k1gNnSc6	město
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
bývalé	bývalý	k2eAgFnSc2d1	bývalá
mazutové	mazutový	k2eAgFnSc2d1	mazutová
kotelny	kotelna	k1gFnSc2	kotelna
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
bývalé	bývalý	k2eAgFnSc2d1	bývalá
obuvnické	obuvnický	k2eAgFnSc2d1	obuvnická
továrny	továrna	k1gFnSc2	továrna
BOPO	BOPO	kA	BOPO
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
ekotechnické	ekotechnický	k2eAgNnSc1d1	ekotechnické
centrum	centrum	k1gNnSc1	centrum
Alternátor	alternátor	k1gInSc1	alternátor
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
několik	několik	k4yIc1	několik
expozic	expozice	k1gFnPc2	expozice
o	o	k7c6	o
technice	technika	k1gFnSc6	technika
<g/>
,	,	kIx,	,
ekologii	ekologie	k1gFnSc6	ekologie
a	a	k8xC	a
energetice	energetika	k1gFnSc6	energetika
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
centra	centrum	k1gNnSc2	centrum
je	být	k5eAaImIp3nS	být
i	i	k9	i
tzv.	tzv.	kA	tzv.
projekční	projekční	k2eAgFnSc1d1	projekční
koule	koule	k1gFnSc1	koule
a	a	k8xC	a
expozice	expozice	k1gFnSc1	expozice
o	o	k7c6	o
historii	historie	k1gFnSc6	historie
průmyslu	průmysl	k1gInSc2	průmysl
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
otevření	otevření	k1gNnSc6	otevření
centra	centrum	k1gNnSc2	centrum
29	[number]	k4	29
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
bylo	být	k5eAaImAgNnS	být
na	na	k7c4	na
budovu	budova	k1gFnSc4	budova
centra	centrum	k1gNnSc2	centrum
promítáno	promítat	k5eAaImNgNnS	promítat
videomappingem	videomapping	k1gInSc7	videomapping
úvodní	úvodní	k2eAgNnSc4d1	úvodní
video	video	k1gNnSc4	video
<g/>
.	.	kIx.	.
</s>
<s>
Kraj	kraj	k1gInSc1	kraj
Vysočina	vysočina	k1gFnSc1	vysočina
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
někdejšího	někdejší	k2eAgInSc2d1	někdejší
Šmeralova	Šmeralův	k2eAgInSc2d1	Šmeralův
domu	dům	k1gInSc2	dům
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
Centrum	centrum	k1gNnSc1	centrum
tradiční	tradiční	k2eAgFnSc2d1	tradiční
lidové	lidový	k2eAgFnSc2d1	lidová
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Muzeum	muzeum	k1gNnSc1	muzeum
Vysočiny	vysočina	k1gFnSc2	vysočina
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
zřizovatelem	zřizovatel	k1gMnSc7	zřizovatel
tohoto	tento	k3xDgNnSc2	tento
centra	centrum	k1gNnSc2	centrum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
bude	být	k5eAaImBp3nS	být
v	v	k7c6	v
Galerii	galerie	k1gFnSc6	galerie
Předsálí	předsálí	k1gNnSc2	předsálí
v	v	k7c6	v
Národním	národní	k2eAgInSc6d1	národní
domě	dům	k1gInSc6	dům
probíhat	probíhat	k5eAaImF	probíhat
výstava	výstava	k1gFnSc1	výstava
o	o	k7c6	o
událostech	událost	k1gFnPc6	událost
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kulturní	kulturní	k2eAgInPc1d1	kulturní
spolky	spolek	k1gInPc1	spolek
===	===	k?	===
</s>
</p>
<p>
<s>
Nejstarším	starý	k2eAgInSc7d3	nejstarší
českým	český	k2eAgInSc7d1	český
spolkem	spolek	k1gInSc7	spolek
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
byla	být	k5eAaImAgFnS	být
Měšťanská	měšťanský	k2eAgFnSc1d1	měšťanská
beseda	beseda	k1gFnSc1	beseda
<g/>
,	,	kIx,	,
založená	založený	k2eAgFnSc1d1	založená
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Janem	Jan	k1gMnSc7	Jan
Miloslavem	Miloslav	k1gMnSc7	Miloslav
Hanělem	Haněl	k1gMnSc7	Haněl
roku	rok	k1gInSc2	rok
1848	[number]	k4	1848
a	a	k8xC	a
následně	následně	k6eAd1	následně
obnovená	obnovený	k2eAgFnSc1d1	obnovená
roku	rok	k1gInSc2	rok
1860	[number]	k4	1860
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozdějších	pozdní	k2eAgFnPc6d2	pozdější
dobách	doba	k1gFnPc6	doba
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
roku	rok	k1gInSc2	rok
1866	[number]	k4	1866
Spolek	spolek	k1gInSc1	spolek
kat	kat	k1gInSc4	kat
<g/>
.	.	kIx.	.
tovaryšů	tovaryš	k1gMnPc2	tovaryš
</s>
</p>
<p>
<s>
roku	rok	k1gInSc2	rok
1869	[number]	k4	1869
Dobrovolný	dobrovolný	k2eAgInSc1d1	dobrovolný
hasičský	hasičský	k2eAgInSc1d1	hasičský
sbor	sbor	k1gInSc1	sbor
</s>
</p>
<p>
<s>
roku	rok	k1gInSc2	rok
1873	[number]	k4	1873
Politický	politický	k2eAgInSc1d1	politický
spolek	spolek	k1gInSc1	spolek
</s>
</p>
<p>
<s>
roku	rok	k1gInSc2	rok
1875	[number]	k4	1875
Dívčí	dívčí	k2eAgFnSc1d1	dívčí
pěvecká	pěvecký	k2eAgFnSc1d1	pěvecká
jednota	jednota	k1gFnSc1	jednota
"	"	kIx"	"
<g/>
Vesna	Vesna	k1gFnSc1	Vesna
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
roku	rok	k1gInSc2	rok
1880	[number]	k4	1880
Řemeslnická	řemeslnický	k2eAgFnSc1d1	řemeslnická
beseda	beseda	k1gFnSc1	beseda
<g/>
,	,	kIx,	,
Tělocvičná	tělocvičný	k2eAgFnSc1d1	Tělocvičná
hasičská	hasičský	k2eAgFnSc1d1	hasičská
jednota	jednota	k1gFnSc1	jednota
<g/>
,	,	kIx,	,
Hospodářský	hospodářský	k2eAgInSc1d1	hospodářský
spolek	spolek	k1gInSc1	spolek
</s>
</p>
<p>
<s>
roku	rok	k1gInSc2	rok
1886	[number]	k4	1886
Spolek	spolek	k1gInSc1	spolek
pro	pro	k7c4	pro
zakládání	zakládání	k1gNnSc4	zakládání
knihoven	knihovna	k1gFnPc2	knihovna
</s>
</p>
<p>
<s>
roku	rok	k1gInSc2	rok
1887	[number]	k4	1887
Dělnická	dělnický	k2eAgFnSc1d1	Dělnická
beseda	beseda	k1gFnSc1	beseda
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1890	[number]	k4	1890
byly	být	k5eAaImAgFnP	být
ještě	ještě	k6eAd1	ještě
založeny	založit	k5eAaPmNgFnP	založit
<g/>
:	:	kIx,	:
Vzdělávací	vzdělávací	k2eAgFnSc1d1	vzdělávací
beseda	beseda	k1gFnSc1	beseda
"	"	kIx"	"
<g/>
Palacký	Palacký	k1gMnSc1	Palacký
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
politický	politický	k2eAgInSc1d1	politický
spolek	spolek	k1gInSc1	spolek
Pokrok	pokrok	k1gInSc1	pokrok
<g/>
,	,	kIx,	,
Sportovní	sportovní	k2eAgInSc1d1	sportovní
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
Bruslařský	bruslařský	k2eAgInSc1d1	bruslařský
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
Spolek	spolek	k1gInSc1	spolek
soukromých	soukromý	k2eAgMnPc2d1	soukromý
úředníků	úředník	k1gMnPc2	úředník
<g/>
,	,	kIx,	,
Československá	československý	k2eAgFnSc1d1	Československá
obchodní	obchodní	k2eAgFnSc1d1	obchodní
beseda	beseda	k1gFnSc1	beseda
<g/>
,	,	kIx,	,
zpěvácký	zpěvácký	k2eAgInSc1d1	zpěvácký
spolek	spolek	k1gInSc1	spolek
Lumír	Lumír	k1gMnSc1	Lumír
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Musejní	musejní	k2eAgInSc1d1	musejní
spolek	spolek	k1gInSc1	spolek
(	(	kIx(	(
<g/>
1898	[number]	k4	1898
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Průmyslové	průmyslový	k2eAgNnSc1d1	průmyslové
museum	museum	k1gNnSc1	museum
<g/>
,	,	kIx,	,
Spolek	spolek	k1gInSc1	spolek
katolických	katolický	k2eAgMnPc2d1	katolický
dělníků	dělník	k1gMnPc2	dělník
<g/>
,	,	kIx,	,
Katolicko-politická	katolickoolitický	k2eAgFnSc1d1	katolicko-politický
jednota	jednota	k1gFnSc1	jednota
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgInPc2d1	další
spolků	spolek	k1gInPc2	spolek
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
loterních	loterní	k2eAgFnPc6d1	loterní
a	a	k8xC	a
podporovacích	podporovací	k2eAgFnPc6d1	podporovací
<g/>
.	.	kIx.	.
<g/>
První	první	k4xOgFnSc4	první
knihovnu	knihovna	k1gFnSc4	knihovna
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
založil	založit	k5eAaPmAgInS	založit
Spolek	spolek	k1gInSc1	spolek
pro	pro	k7c4	pro
zakládání	zakládání	k1gNnSc4	zakládání
knihoven	knihovna	k1gFnPc2	knihovna
dne	den	k1gInSc2	den
6	[number]	k4	6
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1885	[number]	k4	1885
<g/>
,	,	kIx,	,
zakladateli	zakladatel	k1gMnSc6	zakladatel
byli	být	k5eAaImAgMnP	být
Alois	Alois	k1gMnSc1	Alois
Grimmich	Grimmich	k1gMnSc1	Grimmich
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Pochop	Pochop	k1gMnSc1	Pochop
a	a	k8xC	a
František	František	k1gMnSc1	František
Doležel	Doležel	k1gMnSc1	Doležel
<g/>
.	.	kIx.	.
</s>
<s>
Knihovna	knihovna	k1gFnSc1	knihovna
působila	působit	k5eAaImAgFnS	působit
nejprve	nejprve	k6eAd1	nejprve
v	v	k7c6	v
malé	malý	k2eAgFnSc6d1	malá
místnosti	místnost	k1gFnSc6	místnost
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
německé	německý	k2eAgFnSc2d1	německá
školy	škola	k1gFnSc2	škola
na	na	k7c6	na
Hasskově	Hasskův	k2eAgFnSc6d1	Hasskova
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
se	se	k3xPyFc4	se
jmenovala	jmenovat	k5eAaBmAgFnS	jmenovat
Knihovna	knihovna	k1gFnSc1	knihovna
pro	pro	k7c4	pro
lid	lid	k1gInSc4	lid
<g/>
,	,	kIx,	,
po	po	k7c6	po
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
byla	být	k5eAaImAgNnP	být
předána	předat	k5eAaPmNgNnP	předat
z	z	k7c2	z
rukou	ruka	k1gFnPc2	ruka
spolku	spolek	k1gInSc2	spolek
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
města	město	k1gNnSc2	město
a	a	k8xC	a
přejmenována	přejmenován	k2eAgFnSc1d1	přejmenována
na	na	k7c4	na
Husovu	Husův	k2eAgFnSc4d1	Husova
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
knihovnu	knihovna	k1gFnSc4	knihovna
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
začala	začít	k5eAaPmAgFnS	začít
vydávat	vydávat	k5eAaImF	vydávat
časopis	časopis	k1gInSc4	časopis
Čtenář	čtenář	k1gMnSc1	čtenář
a	a	k8xC	a
po	po	k7c6	po
sametové	sametový	k2eAgFnSc6d1	sametová
revoluci	revoluce	k1gFnSc6	revoluce
byla	být	k5eAaImAgFnS	být
přejmenována	přejmenovat	k5eAaPmNgFnS	přejmenovat
na	na	k7c4	na
Městskou	městský	k2eAgFnSc4d1	městská
knihovnu	knihovna	k1gFnSc4	knihovna
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Skauting	skauting	k1gInSc4	skauting
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
===	===	k?	===
</s>
</p>
<p>
<s>
Kulturu	kultura	k1gFnSc4	kultura
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
podporují	podporovat	k5eAaImIp3nP	podporovat
též	též	k9	též
třebíčští	třebíčský	k2eAgMnPc1d1	třebíčský
skauti	skaut	k1gMnPc1	skaut
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
zde	zde	k6eAd1	zde
působí	působit	k5eAaImIp3nP	působit
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1923	[number]	k4	1923
a	a	k8xC	a
u	u	k7c2	u
jejichž	jejichž	k3xOyRp3gInSc2	jejichž
zrodu	zrod	k1gInSc2	zrod
stálo	stát	k5eAaImAgNnS	stát
několik	několik	k4yIc1	několik
významných	významný	k2eAgFnPc2d1	významná
osobností	osobnost	k1gFnPc2	osobnost
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
funguje	fungovat	k5eAaImIp3nS	fungovat
dohromady	dohromady	k6eAd1	dohromady
10	[number]	k4	10
oddílů	oddíl	k1gInPc2	oddíl
skautů	skaut	k1gMnPc2	skaut
vodních	vodní	k2eAgMnPc2d1	vodní
i	i	k8xC	i
pozemních	pozemní	k2eAgMnPc2d1	pozemní
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
činnosti	činnost	k1gFnSc3	činnost
využívají	využívat	k5eAaImIp3nP	využívat
několik	několik	k4yIc4	několik
budov	budova	k1gFnPc2	budova
rozmístěných	rozmístěný	k2eAgFnPc2d1	rozmístěná
po	po	k7c6	po
celém	celý	k2eAgNnSc6d1	celé
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
sdruženy	sdružit	k5eAaPmNgFnP	sdružit
ve	v	k7c6	v
dvou	dva	k4xCgNnPc6	dva
střediscích	středisko	k1gNnPc6	středisko
<g/>
:	:	kIx,	:
pozemní	pozemní	k2eAgMnPc1d1	pozemní
skauti	skaut	k1gMnPc1	skaut
fungují	fungovat	k5eAaImIp3nP	fungovat
pod	pod	k7c7	pod
střediskem	středisko	k1gNnSc7	středisko
s	s	k7c7	s
názvem	název	k1gInSc7	název
Srdíčko	srdíčko	k1gNnSc1	srdíčko
a	a	k8xC	a
vodní	vodní	k2eAgFnSc1d1	vodní
pod	pod	k7c7	pod
střediskem	středisko	k1gNnSc7	středisko
nazvaným	nazvaný	k2eAgNnSc7d1	nazvané
Žlutá	žlutý	k2eAgFnSc1d1	žlutá
ponorka	ponorka	k1gFnSc1	ponorka
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgNnPc1	dva
střediska	středisko	k1gNnPc1	středisko
mají	mít	k5eAaImIp3nP	mít
delší	dlouhý	k2eAgFnSc4d2	delší
historii	historie	k1gFnSc4	historie
a	a	k8xC	a
spoustu	spousta	k1gFnSc4	spousta
tradičních	tradiční	k2eAgFnPc2d1	tradiční
akcí	akce	k1gFnPc2	akce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
O	o	k7c6	o
tradici	tradice	k1gFnSc6	tradice
Junáka	junák	k1gMnSc2	junák
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
svědčí	svědčit	k5eAaImIp3nS	svědčit
i	i	k9	i
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
otevření	otevření	k1gNnSc2	otevření
nového	nový	k2eAgNnSc2d1	nové
divadla	divadlo	k1gNnSc2	divadlo
Pasáž	pasáž	k1gFnSc1	pasáž
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
byl	být	k5eAaImAgMnS	být
jako	jako	k9	jako
první	první	k4xOgFnSc1	první
velká	velký	k2eAgFnSc1d1	velká
akce	akce	k1gFnSc1	akce
ve	v	k7c6	v
dnech	den	k1gInPc6	den
18	[number]	k4	18
<g/>
.	.	kIx.	.
až	až	k9	až
20	[number]	k4	20
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2005	[number]	k4	2005
prezentován	prezentován	k2eAgMnSc1d1	prezentován
XI	XI	kA	XI
<g/>
.	.	kIx.	.
valný	valný	k2eAgInSc1d1	valný
sněm	sněm	k1gInSc1	sněm
Junáka	junák	k1gMnSc2	junák
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
několika	několik	k4yIc6	několik
sálech	sál	k1gInPc6	sál
divadla	divadlo	k1gNnSc2	divadlo
<g/>
,	,	kIx,	,
ve	v	k7c6	v
foyer	foyer	k1gNnSc6	foyer
i	i	k8xC	i
v	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
místech	místo	k1gNnPc6	místo
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c6	o
budoucnosti	budoucnost	k1gFnSc6	budoucnost
skautingu	skauting	k1gInSc2	skauting
a	a	k8xC	a
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
o	o	k7c6	o
tzv.	tzv.	kA	tzv.
Chartě	charta	k1gFnSc6	charta
českého	český	k2eAgInSc2d1	český
skautingu	skauting	k1gInSc2	skauting
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Útulek	útulek	k1gInSc1	útulek
pro	pro	k7c4	pro
psy	pes	k1gMnPc4	pes
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
do	do	k7c2	do
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2015	[number]	k4	2015
fungoval	fungovat	k5eAaImAgInS	fungovat
útulek	útulek	k1gInSc1	útulek
pro	pro	k7c4	pro
psy	pes	k1gMnPc4	pes
pod	pod	k7c7	pod
patronací	patronace	k1gFnSc7	patronace
Ligy	liga	k1gFnSc2	liga
pro	pro	k7c4	pro
ochranu	ochrana	k1gFnSc4	ochrana
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
je	být	k5eAaImIp3nS	být
provozován	provozovat	k5eAaImNgInS	provozovat
městskou	městský	k2eAgFnSc7d1	městská
policií	policie	k1gFnSc7	policie
<g/>
,	,	kIx,	,
nejpozději	pozdě	k6eAd3	pozdě
od	od	k7c2	od
15	[number]	k4	15
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2017	[number]	k4	2017
jej	on	k3xPp3gInSc4	on
musí	muset	k5eAaImIp3nS	muset
provozovat	provozovat	k5eAaImF	provozovat
nový	nový	k2eAgMnSc1d1	nový
provozovatel	provozovatel	k1gMnSc1	provozovatel
či	či	k8xC	či
město	město	k1gNnSc1	město
v	v	k7c6	v
nových	nový	k2eAgInPc6d1	nový
prostorech	prostor	k1gInPc6	prostor
na	na	k7c6	na
Hrotovické	hrotovický	k2eAgFnSc6d1	Hrotovická
ulici	ulice	k1gFnSc6	ulice
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
původní	původní	k2eAgFnPc4d1	původní
prostory	prostora	k1gFnPc4	prostora
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
vyklidit	vyklidit	k5eAaPmF	vyklidit
a	a	k8xC	a
předat	předat	k5eAaPmF	předat
novému	nový	k2eAgMnSc3d1	nový
majiteli	majitel	k1gMnSc3	majitel
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
nového	nový	k2eAgInSc2d1	nový
útulku	útulek	k1gInSc2	útulek
začala	začít	k5eAaPmAgFnS	začít
15	[number]	k4	15
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
Útulek	útulek	k1gInSc1	útulek
skončil	skončit	k5eAaPmAgInS	skončit
kvůli	kvůli	k7c3	kvůli
původnímu	původní	k2eAgMnSc3d1	původní
provozovateli	provozovatel	k1gMnSc3	provozovatel
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
podezřelý	podezřelý	k2eAgMnSc1d1	podezřelý
pro	pro	k7c4	pro
zpronevěru	zpronevěra	k1gFnSc4	zpronevěra
<g/>
.	.	kIx.	.
</s>
<s>
Policie	policie	k1gFnSc1	policie
však	však	k9	však
případ	případ	k1gInSc4	případ
odložila	odložit	k5eAaPmAgFnS	odložit
<g/>
.	.	kIx.	.
</s>
<s>
Útulek	útulek	k1gInSc1	útulek
byl	být	k5eAaImAgInS	být
otevřen	otevřít	k5eAaPmNgInS	otevřít
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
června	červen	k1gInSc2	červen
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Sport	sport	k1gInSc1	sport
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
existuje	existovat	k5eAaImIp3nS	existovat
více	hodně	k6eAd2	hodně
možností	možnost	k1gFnSc7	možnost
sportovního	sportovní	k2eAgNnSc2d1	sportovní
vyžití	vyžití	k1gNnSc2	vyžití
<g/>
,	,	kIx,	,
ve	v	k7c6	v
městě	město	k1gNnSc6	město
jsou	být	k5eAaImIp3nP	být
dva	dva	k4xCgInPc1	dva
atletické	atletický	k2eAgInPc1d1	atletický
stadiony	stadion	k1gInPc1	stadion
<g/>
,	,	kIx,	,
dvě	dva	k4xCgNnPc1	dva
fotbalová	fotbalový	k2eAgNnPc1d1	fotbalové
hřiště	hřiště	k1gNnPc1	hřiště
<g/>
,	,	kIx,	,
plavecký	plavecký	k2eAgInSc1d1	plavecký
bazén	bazén	k1gInSc1	bazén
<g/>
,	,	kIx,	,
zimní	zimní	k2eAgInSc1d1	zimní
stadion	stadion	k1gInSc1	stadion
s	s	k7c7	s
letní	letní	k2eAgFnSc7d1	letní
umělou	umělý	k2eAgFnSc7d1	umělá
plochou	plocha	k1gFnSc7	plocha
<g/>
,	,	kIx,	,
baseballové	baseballový	k2eAgNnSc4d1	baseballové
hřiště	hřiště	k1gNnSc4	hřiště
<g/>
,	,	kIx,	,
kryté	krytý	k2eAgNnSc4d1	kryté
i	i	k8xC	i
venkovní	venkovní	k2eAgNnSc4d1	venkovní
minigolfové	minigolfový	k2eAgNnSc4d1	minigolfové
hřiště	hřiště	k1gNnSc4	hřiště
<g/>
,	,	kIx,	,
několik	několik	k4yIc4	několik
tenisových	tenisový	k2eAgInPc2d1	tenisový
kurtů	kurt	k1gInPc2	kurt
<g/>
,	,	kIx,	,
hřiště	hřiště	k1gNnSc2	hřiště
na	na	k7c4	na
pétanque	pétanqu	k1gInPc4	pétanqu
budované	budovaný	k2eAgInPc4d1	budovaný
z	z	k7c2	z
prostředků	prostředek	k1gInPc2	prostředek
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
hokejbalové	hokejbalový	k2eAgNnSc1d1	hokejbalové
hřiště	hřiště	k1gNnSc1	hřiště
<g/>
,	,	kIx,	,
basketbalová	basketbalový	k2eAgNnPc4d1	basketbalové
hřiště	hřiště	k1gNnPc4	hřiště
a	a	k8xC	a
další	další	k2eAgNnPc4d1	další
veřejná	veřejný	k2eAgNnPc4d1	veřejné
sportoviště	sportoviště	k1gNnPc4	sportoviště
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hokejový	hokejový	k2eAgInSc4d1	hokejový
klub	klub	k1gInSc4	klub
SK	Sk	kA	Sk
Horácká	horácký	k2eAgFnSc1d1	Horácká
Slavia	Slavia	k1gFnSc1	Slavia
Třebíč	Třebíč	k1gFnSc1	Třebíč
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
účastní	účastnit	k5eAaImIp3nS	účastnit
1	[number]	k4	1
<g/>
.	.	kIx.	.
ligy	liga	k1gFnSc2	liga
ledního	lední	k2eAgInSc2d1	lední
hokeje	hokej	k1gInSc2	hokej
<g/>
,	,	kIx,	,
fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
klub	klub	k1gInSc1	klub
HFK	HFK	kA	HFK
Třebíč	Třebíč	k1gFnSc1	Třebíč
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2018	[number]	k4	2018
Krajský	krajský	k2eAgInSc1d1	krajský
přebor	přebor	k1gInSc1	přebor
Vysočiny	vysočina	k1gFnSc2	vysočina
<g/>
,	,	kIx,	,
baseballový	baseballový	k2eAgInSc4d1	baseballový
klub	klub	k1gInSc4	klub
Třebíč	Třebíč	k1gFnSc1	Třebíč
Nuclears	Nuclearsa	k1gFnPc2	Nuclearsa
hraje	hrát	k5eAaImIp3nS	hrát
Českomoravskou	českomoravský	k2eAgFnSc4d1	Českomoravská
ligu	liga	k1gFnSc4	liga
<g/>
,	,	kIx,	,
basketbalový	basketbalový	k2eAgInSc1d1	basketbalový
klub	klub	k1gInSc1	klub
TJ	tj	kA	tj
Třebíč	Třebíč	k1gFnSc1	Třebíč
2	[number]	k4	2
<g/>
.	.	kIx.	.
ligu	liga	k1gFnSc4	liga
<g/>
,	,	kIx,	,
tenisový	tenisový	k2eAgInSc1d1	tenisový
klub	klub	k1gInSc1	klub
HTK	HTK	kA	HTK
Třebíč	Třebíč	k1gFnSc1	Třebíč
také	také	k9	také
2	[number]	k4	2
<g/>
.	.	kIx.	.
ligu	liga	k1gFnSc4	liga
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgMnPc7d1	další
úspěšnými	úspěšný	k2eAgMnPc7d1	úspěšný
třebíčskými	třebíčský	k2eAgMnPc7d1	třebíčský
sportovci	sportovec	k1gMnPc7	sportovec
jsou	být	k5eAaImIp3nP	být
kuželkářka	kuželkářka	k1gFnSc1	kuželkářka
Naděžda	Naděžda	k1gFnSc1	Naděžda
Dobešová	Dobešová	k1gFnSc1	Dobešová
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
obdržela	obdržet	k5eAaPmAgFnS	obdržet
ocenění	ocenění	k1gNnSc4	ocenění
Kuželkářka	kuželkářka	k1gFnSc1	kuželkářka
století	století	k1gNnSc2	století
a	a	k8xC	a
několikrát	několikrát	k6eAd1	několikrát
byla	být	k5eAaImAgFnS	být
oceněna	ocenit	k5eAaPmNgFnS	ocenit
jako	jako	k9	jako
Kuželkářka	kuželkářka	k1gFnSc1	kuželkářka
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
cyklista	cyklista	k1gMnSc1	cyklista
Pavel	Pavel	k1gMnSc1	Pavel
Padrnos	Padrnos	k1gMnSc1	Padrnos
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
závodí	závodit	k5eAaImIp3nS	závodit
v	v	k7c4	v
Discovery	Discover	k1gInPc4	Discover
Channel	Channela	k1gFnPc2	Channela
Pro	pro	k7c4	pro
Cycling	Cycling	k1gInSc4	Cycling
Teamu	team	k1gInSc2	team
<g/>
,	,	kIx,	,
fotbalista	fotbalista	k1gMnSc1	fotbalista
Filip	Filip	k1gMnSc1	Filip
Trojan	Trojan	k1gMnSc1	Trojan
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
hrál	hrát	k5eAaImAgMnS	hrát
v	v	k7c6	v
týmech	tým	k1gInPc6	tým
VfL	VfL	k1gFnSc1	VfL
Bochum	Bochum	k1gInSc1	Bochum
a	a	k8xC	a
FC	FC	kA	FC
Schalke	Schalke	k1gFnSc1	Schalke
04	[number]	k4	04
a	a	k8xC	a
hokejisté	hokejista	k1gMnPc1	hokejista
Patrik	Patrik	k1gMnSc1	Patrik
Eliáš	Eliáš	k1gMnSc1	Eliáš
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
Erat	Erat	k1gMnSc1	Erat
a	a	k8xC	a
Marek	Marek	k1gMnSc1	Marek
Laš	Laš	k1gMnSc1	Laš
<g/>
.	.	kIx.	.
</s>
<s>
Každoročně	každoročně	k6eAd1	každoročně
se	se	k3xPyFc4	se
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
a	a	k8xC	a
okolí	okolí	k1gNnSc6	okolí
konají	konat	k5eAaImIp3nP	konat
FIA	FIA	kA	FIA
rally	ralnout	k5eAaPmAgInP	ralnout
závody	závod	k1gInPc1	závod
s	s	k7c7	s
názvem	název	k1gInSc7	název
Horácká	horácký	k2eAgFnSc1d1	Horácká
rally	rall	k1gInPc7	rall
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
bylo	být	k5eAaImAgNnS	být
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
areálu	areál	k1gInSc3	areál
krytého	krytý	k2eAgInSc2d1	krytý
bazénu	bazén	k1gInSc2	bazén
Laguna	laguna	k1gFnSc1	laguna
bude	být	k5eAaImBp3nS	být
přistavěn	přistavěn	k2eAgInSc1d1	přistavěn
aquapark	aquapark	k1gInSc1	aquapark
s	s	k7c7	s
atrakcemi	atrakce	k1gFnPc7	atrakce
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc1	ten
nakonec	nakonec	k6eAd1	nakonec
byl	být	k5eAaImAgInS	být
otevřen	otevřít	k5eAaPmNgInS	otevřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
lokalitě	lokalita	k1gFnSc6	lokalita
Na	na	k7c6	na
Hvězdě	hvězda	k1gFnSc6	hvězda
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
nové	nový	k2eAgNnSc1d1	nové
sportoviště	sportoviště	k1gNnSc1	sportoviště
–	–	k?	–
baseballové	baseballový	k2eAgNnSc4d1	baseballové
hřiště	hřiště	k1gNnSc4	hřiště
<g/>
.	.	kIx.	.
</s>
<s>
Hrálo	hrát	k5eAaImAgNnS	hrát
se	se	k3xPyFc4	se
na	na	k7c6	na
něm	on	k3xPp3gNnSc6	on
Mistrovství	mistrovství	k1gNnSc1	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
v	v	k7c6	v
baseballu	baseball	k1gInSc6	baseball
<g/>
.	.	kIx.	.
</s>
<s>
Hřiště	hřiště	k1gNnSc1	hřiště
je	být	k5eAaImIp3nS	být
domovským	domovský	k2eAgNnSc7d1	domovské
sportovištěm	sportoviště	k1gNnSc7	sportoviště
baseballového	baseballový	k2eAgInSc2d1	baseballový
klubu	klub	k1gInSc2	klub
Nuclears	Nuclearsa	k1gFnPc2	Nuclearsa
Třebíč	Třebíč	k1gFnSc1	Třebíč
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
by	by	kYmCp3nP	by
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
otevřena	otevřen	k2eAgFnSc1d1	otevřena
uzavřená	uzavřený	k2eAgFnSc1d1	uzavřená
dráha	dráha	k1gFnSc1	dráha
pro	pro	k7c4	pro
cyklisty	cyklista	k1gMnPc4	cyklista
a	a	k8xC	a
in-line	inin	k1gInSc5	in-lin
bruslaře	bruslař	k1gMnPc4	bruslař
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
by	by	kYmCp3nS	by
vést	vést	k5eAaImF	vést
od	od	k7c2	od
rybníka	rybník	k1gInSc2	rybník
Zámiš	zámiš	k1gInSc4	zámiš
až	až	k9	až
do	do	k7c2	do
tzv.	tzv.	kA	tzv.
Bažantnice	bažantnice	k1gFnSc2	bažantnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Školství	školství	k1gNnSc2	školství
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Historie	historie	k1gFnSc1	historie
====	====	k?	====
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
Vlastivědy	vlastivěda	k1gFnSc2	vlastivěda
moravské	moravský	k2eAgFnSc6d1	Moravská
byli	být	k5eAaImAgMnP	být
první	první	k4xOgMnPc1	první
vzdělanci	vzdělanec	k1gMnPc1	vzdělanec
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
učeni	učen	k2eAgMnPc1d1	učen
již	již	k6eAd1	již
v	v	k7c6	v
klášteře	klášter	k1gInSc6	klášter
benediktinů	benediktin	k1gMnPc2	benediktin
<g/>
,	,	kIx,	,
prof.	prof.	kA	prof.
Dvorský	Dvorský	k1gMnSc1	Dvorský
se	se	k3xPyFc4	se
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
autora	autor	k1gMnSc4	autor
latinského	latinský	k2eAgInSc2d1	latinský
formuláře	formulář	k1gInSc2	formulář
Liber	libra	k1gFnPc2	libra
a	a	k8xC	a
missionibus	missionibus	k1gInSc1	missionibus
regum	regum	k1gInSc1	regum
(	(	kIx(	(
<g/>
což	což	k3yRnSc1	což
byl	být	k5eAaImAgMnS	být
vlastně	vlastně	k9	vlastně
návod	návod	k1gInSc4	návod
k	k	k7c3	k
psaní	psaní	k1gNnSc3	psaní
královských	královský	k2eAgInPc2d1	královský
dopisů	dopis	k1gInPc2	dopis
<g/>
)	)	kIx)	)
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1292	[number]	k4	1292
<g/>
,	,	kIx,	,
na	na	k7c4	na
Zdeňka	Zdeněk	k1gMnSc4	Zdeněk
z	z	k7c2	z
Třebíče	Třebíč	k1gFnSc2	Třebíč
(	(	kIx(	(
<g/>
Zdenko	Zdenka	k1gFnSc5	Zdenka
<g />
.	.	kIx.	.
</s>
<s>
de	de	k?	de
Trebecz	Trebecz	k1gInSc1	Trebecz
<g/>
)	)	kIx)	)
a	a	k8xC	a
také	také	k9	také
na	na	k7c4	na
císařského	císařský	k2eAgMnSc4d1	císařský
notáře	notář	k1gMnSc4	notář
Adama	Adam	k1gMnSc4	Adam
<g/>
,	,	kIx,	,
syna	syn	k1gMnSc4	syn
Petra	Petr	k1gMnSc4	Petr
z	z	k7c2	z
Třebíče	Třebíč	k1gFnSc2	Třebíč
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc1	ten
měl	mít	k5eAaImAgInS	mít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1406	[number]	k4	1406
potvrdit	potvrdit	k5eAaPmF	potvrdit
listiny	listina	k1gFnSc2	listina
třebíčského	třebíčský	k2eAgMnSc2d1	třebíčský
rychtáře	rychtář	k1gMnSc2	rychtář
Martina	Martin	k1gMnSc2	Martin
<g/>
.	.	kIx.	.
<g/>
První	první	k4xOgFnSc1	první
městská	městský	k2eAgFnSc1d1	městská
škola	škola	k1gFnSc1	škola
<g/>
,	,	kIx,	,
zřízená	zřízený	k2eAgFnSc1d1	zřízená
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
stála	stát	k5eAaImAgFnS	stát
v	v	k7c6	v
tehdejší	tehdejší	k2eAgFnSc6d1	tehdejší
Masařské	masařský	k2eAgFnSc6d1	masařský
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
urbáře	urbář	k1gInSc2	urbář
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1556	[number]	k4	1556
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
školou	škola	k1gFnSc7	škola
poblíž	poblíž	k7c2	poblíž
Martinské	martinský	k2eAgFnSc2d1	Martinská
brány	brána	k1gFnSc2	brána
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
rektory	rektor	k1gMnPc7	rektor
školy	škola	k1gFnSc2	škola
patřili	patřit	k5eAaImAgMnP	patřit
např.	např.	kA	např.
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
Rožmberský	rožmberský	k2eAgMnSc1d1	rožmberský
nebo	nebo	k8xC	nebo
Eliáš	Eliáš	k1gMnSc1	Eliáš
Histurgius	Histurgius	k1gMnSc1	Histurgius
Bílský	Bílský	k1gMnSc1	Bílský
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
školou	škola	k1gFnSc7	škola
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
zmiňována	zmiňovat	k5eAaImNgFnS	zmiňovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1557	[number]	k4	1557
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
škola	škola	k1gFnSc1	škola
bratrského	bratrský	k2eAgInSc2d1	bratrský
sboru	sbor	k1gInSc2	sbor
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
udání	udání	k1gNnSc4	udání
rektora	rektor	k1gMnSc2	rektor
městské	městský	k2eAgFnSc2d1	městská
školy	škola	k1gFnSc2	škola
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1557	[number]	k4	1557
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
Burianem	Burian	k1gMnSc7	Burian
Osovským	Osovský	k2eAgFnPc3d1	Osovská
zrušena	zrušit	k5eAaPmNgFnS	zrušit
<g/>
,	,	kIx,	,
stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
tomu	ten	k3xDgNnSc3	ten
až	až	k9	až
po	po	k7c6	po
druhém	druhý	k4xOgInSc6	druhý
příkazu	příkaz	k1gInSc6	příkaz
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
I.	I.	kA	I.
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1575	[number]	k4	1575
byla	být	k5eAaImAgFnS	být
škola	škola	k1gFnSc1	škola
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
školou	škola	k1gFnSc7	škola
v	v	k7c6	v
Ivančicích	Ivančice	k1gFnPc6	Ivančice
obnovena	obnovit	k5eAaPmNgFnS	obnovit
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
zakoupena	zakoupen	k2eAgFnSc1d1	zakoupena
budova	budova	k1gFnSc1	budova
na	na	k7c6	na
Jejkově	Jejkův	k2eAgMnSc6d1	Jejkův
<g/>
.	.	kIx.	.
</s>
<s>
Prof.	prof.	kA	prof.
Dvorský	Dvorský	k1gMnSc1	Dvorský
uvádí	uvádět	k5eAaImIp3nS	uvádět
několik	několik	k4yIc4	několik
absolventů	absolvent	k1gMnPc2	absolvent
třebíčské	třebíčský	k2eAgFnSc2d1	Třebíčská
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
ve	v	k7c6	v
studiích	studio	k1gNnPc6	studio
na	na	k7c6	na
různých	různý	k2eAgFnPc6d1	různá
vyšších	vysoký	k2eAgFnPc6d2	vyšší
školách	škola	k1gFnPc6	škola
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
třicetileté	třicetiletý	k2eAgFnSc6d1	třicetiletá
válce	válka	k1gFnSc6	válka
měla	mít	k5eAaImAgFnS	mít
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
zůstat	zůstat	k5eAaPmF	zůstat
pouze	pouze	k6eAd1	pouze
škola	škola	k1gFnSc1	škola
městská	městský	k2eAgFnSc1d1	městská
a	a	k8xC	a
škola	škola	k1gFnSc1	škola
prvního	první	k4xOgInSc2	první
stupně	stupeň	k1gInSc2	stupeň
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
pouze	pouze	k6eAd1	pouze
chlapecká	chlapecký	k2eAgFnSc1d1	chlapecká
a	a	k8xC	a
podle	podle	k7c2	podle
zprávy	zpráva	k1gFnSc2	zpráva
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1671	[number]	k4	1671
<g/>
,	,	kIx,	,
tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
vyučovat	vyučovat	k5eAaImF	vyučovat
matematika	matematika	k1gFnSc1	matematika
<g/>
,	,	kIx,	,
psaní	psaní	k1gNnSc1	psaní
<g/>
,	,	kIx,	,
hudba	hudba	k1gFnSc1	hudba
<g/>
,	,	kIx,	,
němčina	němčina	k1gFnSc1	němčina
<g/>
,	,	kIx,	,
čeština	čeština	k1gFnSc1	čeština
a	a	k8xC	a
náboženství	náboženství	k1gNnSc1	náboženství
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
jazycích	jazyk	k1gInPc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1787	[number]	k4	1787
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
o	o	k7c6	o
stavbě	stavba	k1gFnSc6	stavba
školy	škola	k1gFnSc2	škola
na	na	k7c6	na
Jejkově	Jejkův	k2eAgFnSc6d1	Jejkův
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgNnP	být
postavena	postavit	k5eAaPmNgNnP	postavit
v	v	k7c6	v
zahradě	zahrada	k1gFnSc6	zahrada
sousedského	sousedský	k2eAgInSc2d1	sousedský
špitálu	špitál	k1gInSc2	špitál
<g/>
,	,	kIx,	,
tam	tam	k6eAd1	tam
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1847	[number]	k4	1847
i	i	k9	i
vyhořela	vyhořet	k5eAaPmAgFnS	vyhořet
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
znovu	znovu	k6eAd1	znovu
vybudována	vybudovat	k5eAaPmNgFnS	vybudovat
a	a	k8xC	a
na	na	k7c6	na
tomtéž	týž	k3xTgNnSc6	týž
místě	místo	k1gNnSc6	místo
zůstala	zůstat	k5eAaPmAgFnS	zůstat
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1888	[number]	k4	1888
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
reorganizaci	reorganizace	k1gFnSc3	reorganizace
školských	školský	k2eAgInPc2d1	školský
okresů	okres	k1gInPc2	okres
a	a	k8xC	a
tato	tento	k3xDgFnSc1	tento
budova	budova	k1gFnSc1	budova
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
městským	městský	k2eAgInSc7d1	městský
sirotčincem	sirotčinec	k1gInSc7	sirotčinec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
přesněji	přesně	k6eAd2	přesně
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1817	[number]	k4	1817
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
zřízena	zřídit	k5eAaPmNgFnS	zřídit
i	i	k9	i
městská	městský	k2eAgFnSc1d1	městská
dívčí	dívčí	k2eAgFnSc1d1	dívčí
škola	škola	k1gFnSc1	škola
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1874	[number]	k4	1874
bylo	být	k5eAaImAgNnS	být
Němci	Němec	k1gMnPc7	Němec
požádáno	požádán	k2eAgNnSc1d1	požádáno
o	o	k7c4	o
zřízení	zřízení	k1gNnSc4	zřízení
dívčí	dívčí	k2eAgFnSc2d1	dívčí
německé	německý	k2eAgFnSc2d1	německá
měšťanské	měšťanský	k2eAgFnSc2d1	měšťanská
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
chlapecká	chlapecký	k2eAgFnSc1d1	chlapecká
německá	německý	k2eAgFnSc1d1	německá
škola	škola	k1gFnSc1	škola
a	a	k8xC	a
po	po	k7c6	po
počeštění	počeštění	k1gNnSc6	počeštění
gymnázia	gymnázium	k1gNnSc2	gymnázium
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1888	[number]	k4	1888
i	i	k8xC	i
chlapecká	chlapecký	k2eAgFnSc1d1	chlapecká
měšťanská	měšťanský	k2eAgFnSc1d1	měšťanská
škola	škola	k1gFnSc1	škola
s	s	k7c7	s
německou	německý	k2eAgFnSc7d1	německá
výukou	výuka	k1gFnSc7	výuka
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
česká	český	k2eAgFnSc1d1	Česká
strana	strana	k1gFnSc1	strana
ovšem	ovšem	k9	ovšem
na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
nechala	nechat	k5eAaPmAgFnS	nechat
založit	založit	k5eAaPmF	založit
chlapeckou	chlapecký	k2eAgFnSc4d1	chlapecká
i	i	k8xC	i
dívčí	dívčí	k2eAgFnSc4d1	dívčí
měšťanskou	měšťanský	k2eAgFnSc4d1	měšťanská
školu	škola	k1gFnSc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1906	[number]	k4	1906
tak	tak	k6eAd1	tak
studovalo	studovat	k5eAaImAgNnS	studovat
na	na	k7c6	na
třebíčských	třebíčský	k2eAgFnPc6d1	Třebíčská
měšťanských	měšťanský	k2eAgFnPc6d1	měšťanská
a	a	k8xC	a
obecných	obecný	k2eAgFnPc6d1	obecná
školách	škola	k1gFnPc6	škola
944	[number]	k4	944
žáků	žák	k1gMnPc2	žák
v	v	k7c6	v
chlapeckých	chlapecký	k2eAgFnPc6d1	chlapecká
školách	škola	k1gFnPc6	škola
<g/>
,	,	kIx,	,
1014	[number]	k4	1014
žákyň	žákyně	k1gFnPc2	žákyně
v	v	k7c6	v
dívčích	dívčí	k2eAgFnPc6d1	dívčí
školách	škola	k1gFnPc6	škola
a	a	k8xC	a
225	[number]	k4	225
žáků	žák	k1gMnPc2	žák
a	a	k8xC	a
žákyň	žákyně	k1gFnPc2	žákyně
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
obecné	obecný	k2eAgFnSc6d1	obecná
škole	škola	k1gFnSc6	škola
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgMnPc4	tento
žáky	žák	k1gMnPc4	žák
(	(	kIx(	(
<g/>
celkem	celkem	k6eAd1	celkem
2183	[number]	k4	2183
žáků	žák	k1gMnPc2	žák
<g/>
)	)	kIx)	)
vychovávalo	vychovávat	k5eAaImAgNnS	vychovávat
v	v	k7c6	v
tomtéž	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
54	[number]	k4	54
kantorů	kantor	k1gMnPc2	kantor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vyšší	vysoký	k2eAgNnSc1d2	vyšší
školství	školství	k1gNnSc1	školství
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
mělo	mít	k5eAaImAgNnS	mít
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
také	také	k9	také
zastoupení	zastoupení	k1gNnSc1	zastoupení
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1871	[number]	k4	1871
bylo	být	k5eAaImAgNnS	být
založeno	založen	k2eAgNnSc1d1	založeno
gymnázium	gymnázium	k1gNnSc1	gymnázium
Třebíč	Třebíč	k1gFnSc1	Třebíč
<g/>
,	,	kIx,	,
prvním	první	k4xOgMnSc7	první
ředitelem	ředitel	k1gMnSc7	ředitel
byl	být	k5eAaImAgInS	být
ustanoven	ustanoven	k2eAgInSc1d1	ustanoven
prof.	prof.	kA	prof.
Alois	Alois	k1gMnSc1	Alois
Vaníček	Vaníček	k1gMnSc1	Vaníček
<g/>
,	,	kIx,	,
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1873	[number]	k4	1873
probíhala	probíhat	k5eAaImAgFnS	probíhat
výuka	výuka	k1gFnSc1	výuka
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1888	[number]	k4	1888
probíhala	probíhat	k5eAaImAgFnS	probíhat
výuka	výuka	k1gFnSc1	výuka
některých	některý	k3yIgFnPc2	některý
tříd	třída	k1gFnPc2	třída
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
německém	německý	k2eAgInSc6d1	německý
jazyce	jazyk	k1gInSc6	jazyk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1880	[number]	k4	1880
byla	být	k5eAaImAgFnS	být
znovuobnovena	znovuobnoven	k2eAgFnSc1d1	znovuobnoven
výuka	výuka	k1gFnSc1	výuka
3	[number]	k4	3
<g/>
.	.	kIx.	.
třídy	třída	k1gFnSc2	třída
v	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
českém	český	k2eAgInSc6d1	český
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
i	i	k9	i
třídy	třída	k1gFnSc2	třída
čtvrté	čtvrtá	k1gFnSc2	čtvrtá
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
učily	učit	k5eAaImAgFnP	učit
česky	česky	k6eAd1	česky
pouze	pouze	k6eAd1	pouze
první	první	k4xOgFnPc1	první
dvě	dva	k4xCgFnPc1	dva
třídy	třída	k1gFnPc1	třída
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1886	[number]	k4	1886
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
pokračovací	pokračovací	k2eAgFnSc1d1	pokračovací
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
škola	škola	k1gFnSc1	škola
a	a	k8xC	a
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
i	i	k8xC	i
odborný	odborný	k2eAgInSc1d1	odborný
kurs	kurs	k1gInSc1	kurs
obuvnický	obuvnický	k2eAgInSc1d1	obuvnický
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1901	[number]	k4	1901
tu	tu	k6eAd1	tu
otevřeli	otevřít	k5eAaPmAgMnP	otevřít
českou	český	k2eAgFnSc4d1	Česká
dvoutřídní	dvoutřídní	k2eAgFnSc4d1	dvoutřídní
obchodní	obchodní	k2eAgFnSc4d1	obchodní
školu	škola	k1gFnSc4	škola
<g/>
,	,	kIx,	,
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
i	i	k8xC	i
jednoletý	jednoletý	k2eAgInSc1d1	jednoletý
kurs	kurs	k1gInSc1	kurs
pro	pro	k7c4	pro
dívky	dívka	k1gFnPc4	dívka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
téže	tenže	k3xDgFnSc6	tenže
škole	škola	k1gFnSc6	škola
se	se	k3xPyFc4	se
učili	učít	k5eAaPmAgMnP	učít
i	i	k9	i
učňové	učeň	k1gMnPc1	učeň
pokračovací	pokračovací	k2eAgFnSc2d1	pokračovací
obchodnické	obchodnický	k2eAgFnSc2d1	obchodnická
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
dvoutřídní	dvoutřídní	k2eAgFnSc1d1	dvoutřídní
Zimní	zimní	k2eAgFnSc1d1	zimní
hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
škola	škola	k1gFnSc1	škola
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
učila	učit	k5eAaImAgFnS	učit
děti	dítě	k1gFnPc1	dítě
zemědělců	zemědělec	k1gMnPc2	zemědělec
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
zimním	zimní	k2eAgNnSc6d1	zimní
období	období	k1gNnSc6	období
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Současnost	současnost	k1gFnSc4	současnost
====	====	k?	====
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
jedna	jeden	k4xCgFnSc1	jeden
soukromá	soukromý	k2eAgFnSc1d1	soukromá
vysoká	vysoký	k2eAgFnSc1d1	vysoká
škola	škola	k1gFnSc1	škola
<g/>
</s>
<s>
,	,	kIx,	,
založená	založený	k2eAgNnPc1d1	založené
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
Západomoravská	západomoravský	k2eAgFnSc1d1	Západomoravská
vysoká	vysoký	k2eAgFnSc1d1	vysoká
škola	škola	k1gFnSc1	škola
Třebíč	Třebíč	k1gFnSc1	Třebíč
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
městě	město	k1gNnSc6	město
devět	devět	k4xCc1	devět
středních	střední	k2eAgFnPc2d1	střední
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
dvě	dva	k4xCgNnPc1	dva
gymnázia	gymnázium	k1gNnPc1	gymnázium
(	(	kIx(	(
<g/>
jedno	jeden	k4xCgNnSc1	jeden
klasické	klasický	k2eAgFnPc4d1	klasická
<g/>
,	,	kIx,	,
jedno	jeden	k4xCgNnSc1	jeden
katolické	katolický	k2eAgNnSc1d1	katolické
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čtyři	čtyři	k4xCgFnPc1	čtyři
odborné	odborný	k2eAgFnPc1d1	odborná
školy	škola	k1gFnPc1	škola
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
hotelová	hotelový	k2eAgFnSc1d1	hotelová
a	a	k8xC	a
jedna	jeden	k4xCgFnSc1	jeden
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
škola	škola	k1gFnSc1	škola
<g/>
,	,	kIx,	,
další	další	k2eAgFnSc1d1	další
střední	střední	k2eAgFnSc7d1	střední
školou	škola	k1gFnSc7	škola
je	být	k5eAaImIp3nS	být
soukromá	soukromý	k2eAgFnSc1d1	soukromá
střední	střední	k2eAgFnSc1d1	střední
škola	škola	k1gFnSc1	škola
řemesel	řemeslo	k1gNnPc2	řemeslo
a	a	k8xC	a
podnikání	podnikání	k1gNnSc2	podnikání
<g/>
.	.	kIx.	.
</s>
<s>
Základních	základní	k2eAgFnPc2d1	základní
škol	škola	k1gFnPc2	škola
se	se	k3xPyFc4	se
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
nachází	nacházet	k5eAaImIp3nS	nacházet
osm	osm	k4xCc1	osm
<g/>
,	,	kIx,	,
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
se	se	k3xPyFc4	se
jedna	jeden	k4xCgFnSc1	jeden
věnuje	věnovat	k5eAaImIp3nS	věnovat
dětem	dítě	k1gFnPc3	dítě
s	s	k7c7	s
různými	různý	k2eAgNnPc7d1	různé
postiženími	postižení	k1gNnPc7	postižení
<g/>
,	,	kIx,	,
další	další	k2eAgFnSc7d1	další
školou	škola	k1gFnSc7	škola
je	být	k5eAaImIp3nS	být
Základní	základní	k2eAgFnSc1d1	základní
umělecká	umělecký	k2eAgFnSc1d1	umělecká
škola	škola	k1gFnSc1	škola
Třebíč	Třebíč	k1gFnSc1	Třebíč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
se	se	k3xPyFc4	se
provozují	provozovat	k5eAaImIp3nP	provozovat
i	i	k9	i
jiné	jiný	k2eAgFnPc1d1	jiná
výchovné	výchovný	k2eAgFnPc1d1	výchovná
a	a	k8xC	a
zájmové	zájmový	k2eAgFnPc1d1	zájmová
činnosti	činnost	k1gFnPc1	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
zde	zde	k6eAd1	zde
skautské	skautský	k2eAgInPc1d1	skautský
a	a	k8xC	a
sokolské	sokolský	k2eAgInPc1d1	sokolský
oddíly	oddíl	k1gInPc1	oddíl
<g/>
,	,	kIx,	,
podprahový	podprahový	k2eAgInSc1d1	podprahový
klub	klub	k1gInSc1	klub
Zámek	zámek	k1gInSc1	zámek
<g/>
,	,	kIx,	,
studentský	studentský	k2eAgInSc1d1	studentský
klub	klub	k1gInSc1	klub
Halahoj	Halahoj	k1gInSc1	Halahoj
<g/>
,	,	kIx,	,
dům	dům	k1gInSc1	dům
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
mládeže	mládež	k1gFnSc2	mládež
Hrádek	hrádek	k1gInSc1	hrádek
(	(	kIx(	(
<g/>
jenž	jenž	k3xRgMnSc1	jenž
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
přejmenován	přejmenovat	k5eAaPmNgMnS	přejmenovat
na	na	k7c4	na
Dům	dům	k1gInSc4	dům
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
mládeže	mládež	k1gFnSc2	mládež
Třebíč	Třebíč	k1gFnSc1	Třebíč
a	a	k8xC	a
přestěhován	přestěhován	k2eAgMnSc1d1	přestěhován
do	do	k7c2	do
nové	nový	k2eAgFnSc2d1	nová
budovy	budova	k1gFnSc2	budova
v	v	k7c6	v
bývalém	bývalý	k2eAgInSc6d1	bývalý
areálu	areál	k1gInSc6	areál
borovinských	borovinský	k2eAgInPc2d1	borovinský
závodů	závod	k1gInPc2	závod
<g/>
)	)	kIx)	)
pořádající	pořádající	k2eAgFnPc1d1	pořádající
zájmové	zájmový	k2eAgFnPc1d1	zájmová
činnosti	činnost	k1gFnPc1	činnost
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
skupiny	skupina	k1gFnPc1	skupina
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
a	a	k8xC	a
mládež	mládež	k1gFnSc4	mládež
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
třebíčské	třebíčský	k2eAgFnSc6d1	Třebíčská
radnici	radnice	k1gFnSc6	radnice
otevřen	otevřít	k5eAaPmNgInS	otevřít
koutek	koutek	k1gInSc1	koutek
pro	pro	k7c4	pro
matky	matka	k1gFnPc4	matka
s	s	k7c7	s
dětmi	dítě	k1gFnPc7	dítě
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
matky	matka	k1gFnSc2	matka
mohou	moct	k5eAaImIp3nP	moct
využít	využít	k5eAaPmF	využít
při	při	k7c6	při
jednání	jednání	k1gNnSc6	jednání
na	na	k7c6	na
úřadech	úřad	k1gInPc6	úřad
či	či	k8xC	či
případně	případně	k6eAd1	případně
také	také	k9	také
při	při	k7c6	při
nakupování	nakupování	k1gNnSc6	nakupování
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Hvězdárna	hvězdárna	k1gFnSc1	hvězdárna
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1957	[number]	k4	1957
nachází	nacházet	k5eAaImIp3nS	nacházet
hvězdárna	hvězdárna	k1gFnSc1	hvězdárna
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
v	v	k7c6	v
části	část	k1gFnSc6	část
Horka-Domky	Horka-Domka	k1gFnSc2	Horka-Domka
na	na	k7c4	na
ul	ul	kA	ul
<g/>
.	.	kIx.	.
Švabinského	Švabinský	k2eAgMnSc2d1	Švabinský
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byla	být	k5eAaImAgFnS	být
učitelem	učitel	k1gMnSc7	učitel
Františkem	František	k1gMnSc7	František
Havlíkem	Havlík	k1gMnSc7	Havlík
založena	založen	k2eAgFnSc1d1	založena
pobočka	pobočka	k1gFnSc1	pobočka
České	český	k2eAgFnSc2d1	Česká
astronomické	astronomický	k2eAgFnSc2d1	astronomická
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
si	se	k3xPyFc3	se
dala	dát	k5eAaPmAgFnS	dát
postavit	postavit	k5eAaPmF	postavit
lidovou	lidový	k2eAgFnSc4d1	lidová
hvězdárnu	hvězdárna	k1gFnSc4	hvězdárna
<g/>
,	,	kIx,	,
její	její	k3xOp3gInSc4	její
návrh	návrh	k1gInSc4	návrh
nakreslil	nakreslit	k5eAaPmAgMnS	nakreslit
architekt	architekt	k1gMnSc1	architekt
Herzán	Herzán	k2eAgMnSc1d1	Herzán
<g/>
.	.	kIx.	.
</s>
<s>
Měla	mít	k5eAaImAgFnS	mít
původně	původně	k6eAd1	původně
mít	mít	k5eAaImF	mít
3	[number]	k4	3
kupole	kupole	k1gFnPc4	kupole
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
pozorování	pozorování	k1gNnSc1	pozorování
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
při	při	k7c6	při
průletu	průlet	k1gInSc6	průlet
komety	kometa	k1gFnSc2	kometa
Arend-Roland	Arend-Rolanda	k1gFnPc2	Arend-Rolanda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zničení	zničení	k1gNnSc6	zničení
dalekohledu	dalekohled	k1gInSc2	dalekohled
se	se	k3xPyFc4	se
pobočka	pobočka	k1gFnSc1	pobočka
na	na	k7c4	na
20	[number]	k4	20
let	léto	k1gNnPc2	léto
rozpadla	rozpadnout	k5eAaPmAgFnS	rozpadnout
a	a	k8xC	a
pozorování	pozorování	k1gNnPc1	pozorování
probíhala	probíhat	k5eAaImAgNnP	probíhat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
amatérských	amatérský	k2eAgFnPc6d1	amatérská
podmínkách	podmínka	k1gFnPc6	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
začalo	začít	k5eAaPmAgNnS	začít
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
obnově	obnova	k1gFnSc6	obnova
hvězdárny	hvězdárna	k1gFnSc2	hvězdárna
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
povedlo	povést	k5eAaPmAgNnS	povést
a	a	k8xC	a
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1980	[number]	k4	1980
byla	být	k5eAaImAgFnS	být
hvězdárna	hvězdárna	k1gFnSc1	hvězdárna
znovu	znovu	k6eAd1	znovu
otevřena	otevřít	k5eAaPmNgFnS	otevřít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
astronomové	astronom	k1gMnPc1	astronom
získali	získat	k5eAaPmAgMnP	získat
budovu	budova	k1gFnSc4	budova
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
od	od	k7c2	od
tohoto	tento	k3xDgInSc2	tento
roku	rok	k1gInSc2	rok
jako	jako	k8xC	jako
zázemí	zázemí	k1gNnSc2	zázemí
hvězdárny	hvězdárna	k1gFnSc2	hvězdárna
<g/>
;	;	kIx,	;
je	být	k5eAaImIp3nS	být
jí	on	k3xPp3gFnSc3	on
původní	původní	k2eAgFnSc1d1	původní
autobusové	autobusový	k2eAgNnSc4d1	autobusové
nádraží	nádraží	k1gNnSc4	nádraží
z	z	k7c2	z
Březinovy	Březinův	k2eAgFnSc2d1	Březinova
ulice	ulice	k1gFnSc2	ulice
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc4	ten
byla	být	k5eAaImAgFnS	být
přeneseno	přenést	k5eAaPmNgNnS	přenést
do	do	k7c2	do
prostor	prostora	k1gFnPc2	prostora
Švabinského	Švabinský	k2eAgInSc2d1	Švabinský
ulice	ulice	k1gFnPc1	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Dřevěná	dřevěný	k2eAgFnSc1d1	dřevěná
pozorovatelna	pozorovatelna	k1gFnSc1	pozorovatelna
byla	být	k5eAaImAgFnS	být
později	pozdě	k6eAd2	pozdě
přebudována	přebudovat	k5eAaPmNgFnS	přebudovat
na	na	k7c4	na
observatoř	observatoř	k1gFnSc4	observatoř
železnou	železný	k2eAgFnSc4d1	železná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
první	první	k4xOgNnSc1	první
pozorování	pozorování	k1gNnSc1	pozorování
s	s	k7c7	s
novým	nový	k2eAgInSc7d1	nový
dalekohledem	dalekohled	k1gInSc7	dalekohled
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
byla	být	k5eAaImAgFnS	být
rozestavěna	rozestavěn	k2eAgFnSc1d1	rozestavěna
nová	nový	k2eAgFnSc1d1	nová
budova	budova	k1gFnSc1	budova
hvězdárny	hvězdárna	k1gFnSc2	hvězdárna
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
tzv.	tzv.	kA	tzv.
Kostelíčka	kostelíček	k1gInSc2	kostelíček
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
na	na	k7c6	na
severním	severní	k2eAgInSc6d1	severní
svahu	svah	k1gInSc6	svah
Strážné	strážný	k2eAgFnSc2d1	Strážná
hory	hora	k1gFnSc2	hora
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
instalace	instalace	k1gFnSc1	instalace
astronomických	astronomický	k2eAgFnPc2d1	astronomická
hodin	hodina	k1gFnPc2	hodina
na	na	k7c6	na
pozemku	pozemek	k1gInSc6	pozemek
hvězdárny	hvězdárna	k1gFnSc2	hvězdárna
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
mezi	mezi	k7c7	mezi
1	[number]	k4	1
<g/>
.	.	kIx.	.
květnem	květen	k1gInSc7	květen
a	a	k8xC	a
25	[number]	k4	25
<g/>
.	.	kIx.	.
listopadem	listopad	k1gInSc7	listopad
<g/>
.	.	kIx.	.
</s>
<s>
Ukazují	ukazovat	k5eAaImIp3nP	ukazovat
čas	čas	k1gInSc4	čas
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
<g/>
,	,	kIx,	,
New	New	k1gFnSc6	New
Yorku	York	k1gInSc6	York
<g/>
,	,	kIx,	,
Tokiu	Tokio	k1gNnSc6	Tokio
<g/>
,	,	kIx,	,
Moskvě	Moskva	k1gFnSc6	Moskva
a	a	k8xC	a
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
;	;	kIx,	;
rovněž	rovněž	k9	rovněž
fázi	fáze	k1gFnSc4	fáze
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
čas	čas	k1gInSc4	čas
východu	východ	k1gInSc2	východ
a	a	k8xC	a
západu	západ	k1gInSc2	západ
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
datum	datum	k1gNnSc4	datum
<g/>
,	,	kIx,	,
měsíc	měsíc	k1gInSc4	měsíc
a	a	k8xC	a
den	den	k1gInSc4	den
v	v	k7c6	v
týdnu	týden	k1gInSc6	týden
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
hodin	hodina	k1gFnPc2	hodina
je	být	k5eAaImIp3nS	být
i	i	k9	i
statické	statický	k2eAgNnSc1d1	statické
tellurium	tellurium	k1gNnSc1	tellurium
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
řízeny	řízen	k2eAgFnPc1d1	řízena
počítačem	počítač	k1gInSc7	počítač
a	a	k8xC	a
synchronizované	synchronizovaný	k2eAgInPc1d1	synchronizovaný
se	se	k3xPyFc4	se
servery	server	k1gInPc7	server
řízení	řízení	k1gNnSc3	řízení
času	čas	k1gInSc2	čas
ve	v	k7c6	v
Frankfurtu	Frankfurt	k1gInSc6	Frankfurt
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2019	[number]	k4	2019
bylo	být	k5eAaImAgNnS	být
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
DDM	DDM	kA	DDM
Třebíč	Třebíč	k1gFnSc1	Třebíč
už	už	k9	už
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
usiluje	usilovat	k5eAaImIp3nS	usilovat
o	o	k7c4	o
postavení	postavení	k1gNnSc4	postavení
planetária	planetárium	k1gNnSc2	planetárium
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
<g/>
,	,	kIx,	,
DDM	DDM	kA	DDM
Třebíč	Třebíč	k1gFnSc1	Třebíč
chce	chtít	k5eAaImIp3nS	chtít
ke	k	k7c3	k
stávající	stávající	k2eAgFnSc3d1	stávající
budově	budova	k1gFnSc3	budova
přistavět	přistavět	k5eAaPmF	přistavět
další	další	k2eAgFnSc4d1	další
budovu	budova	k1gFnSc4	budova
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yIgMnPc4	který
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgInP	mít
být	být	k5eAaImF	být
dva	dva	k4xCgInPc1	dva
sály	sál	k1gInPc1	sál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
přednáškový	přednáškový	k2eAgInSc1d1	přednáškový
sál	sál	k1gInSc1	sál
a	a	k8xC	a
v	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgNnP	mít
být	být	k5eAaImF	být
promítací	promítací	k2eAgFnPc4d1	promítací
kupole	kupole	k1gFnPc4	kupole
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
DDM	DDM	kA	DDM
snažilo	snažit	k5eAaImAgNnS	snažit
získat	získat	k5eAaPmF	získat
peníze	peníz	k1gInPc4	peníz
pro	pro	k7c4	pro
rozšíření	rozšíření	k1gNnSc4	rozšíření
z	z	k7c2	z
dotací	dotace	k1gFnPc2	dotace
regionálního	regionální	k2eAgInSc2d1	regionální
dotačního	dotační	k2eAgInSc2d1	dotační
programu	program	k1gInSc2	program
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
však	však	k9	však
nepovedlo	povést	k5eNaPmAgNnS	povést
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2019	[number]	k4	2019
si	se	k3xPyFc3	se
toto	tento	k3xDgNnSc4	tento
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
vzalo	vzít	k5eAaPmAgNnS	vzít
město	město	k1gNnSc1	město
Třebíč	Třebíč	k1gFnSc1	Třebíč
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
zřizuje	zřizovat	k5eAaImIp3nS	zřizovat
DDM	DDM	kA	DDM
Třebíč	Třebíč	k1gFnSc1	Třebíč
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
oznámilo	oznámit	k5eAaPmAgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
nepodaří	podařit	k5eNaPmIp3nS	podařit
získat	získat	k5eAaPmF	získat
peníze	peníz	k1gInPc4	peníz
z	z	k7c2	z
dotačního	dotační	k2eAgInSc2d1	dotační
programu	program	k1gInSc2	program
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
přestavbu	přestavba	k1gFnSc4	přestavba
zainvestuje	zainvestovat	k5eAaPmIp3nS	zainvestovat
samo	sám	k3xTgNnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Vedoucí	vedoucí	k1gMnSc1	vedoucí
hvězdárny	hvězdárna	k1gFnSc2	hvězdárna
je	být	k5eAaImIp3nS	být
Jana	Jana	k1gFnSc1	Jana
Švíhálková	Švíhálková	k1gFnSc1	Švíhálková
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
jejímž	jejíž	k3xOyRp3gNnSc7	jejíž
vedením	vedení	k1gNnSc7	vedení
se	se	k3xPyFc4	se
hvězdárna	hvězdárna	k1gFnSc1	hvězdárna
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Média	médium	k1gNnPc1	médium
===	===	k?	===
</s>
</p>
<p>
<s>
Prvním	první	k4xOgInSc7	první
listem	list	k1gInSc7	list
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Třebíče	Třebíč	k1gFnSc2	Třebíč
byly	být	k5eAaImAgInP	být
Listy	list	k1gInPc1	list
západní	západní	k2eAgFnSc2d1	západní
Moravy	Morava	k1gFnSc2	Morava
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
vydával	vydávat	k5eAaPmAgMnS	vydávat
třebíčský	třebíčský	k2eAgMnSc1d1	třebíčský
starosta	starosta	k1gMnSc1	starosta
Jan	Jan	k1gMnSc1	Jan
František	František	k1gMnSc1	František
Kubeš	Kubeš	k1gMnSc1	Kubeš
<g/>
.	.	kIx.	.
</s>
<s>
Vycházely	vycházet	k5eAaImAgFnP	vycházet
od	od	k7c2	od
5	[number]	k4	5
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1884	[number]	k4	1884
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1898	[number]	k4	1898
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
na	na	k7c4	na
ně	on	k3xPp3gInPc4	on
navázal	navázat	k5eAaPmAgInS	navázat
list	list	k1gInSc1	list
s	s	k7c7	s
názvem	název	k1gInSc7	název
Hlasy	hlas	k1gInPc4	hlas
ze	z	k7c2	z
západní	západní	k2eAgFnSc2d1	západní
Moravy	Morava	k1gFnSc2	Morava
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
redigoval	redigovat	k5eAaImAgMnS	redigovat
J.	J.	kA	J.
Šlesinger	Šlesinger	k1gMnSc1	Šlesinger
<g/>
.	.	kIx.	.
</s>
<s>
Hlasy	hlas	k1gInPc1	hlas
vycházely	vycházet	k5eAaImAgFnP	vycházet
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1915	[number]	k4	1915
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1899	[number]	k4	1899
začal	začít	k5eAaPmAgMnS	začít
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
vycházet	vycházet	k5eAaImF	vycházet
katolický	katolický	k2eAgInSc4d1	katolický
časopis	časopis	k1gInSc4	časopis
Stráž	stráž	k1gFnSc1	stráž
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1911	[number]	k4	1911
třebíčští	třebíčský	k2eAgMnPc1d1	třebíčský
socialisté	socialist	k1gMnPc1	socialist
vydávali	vydávat	k5eAaImAgMnP	vydávat
časopis	časopis	k1gInSc4	časopis
Horácko	Horácko	k1gNnSc1	Horácko
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
začal	začít	k5eAaPmAgMnS	začít
vycházet	vycházet	k5eAaImF	vycházet
národnědemokratický	národnědemokratický	k2eAgInSc4d1	národnědemokratický
list	list	k1gInSc4	list
Demokrat	demokrat	k1gMnSc1	demokrat
a	a	k8xC	a
republikánský	republikánský	k2eAgInSc4d1	republikánský
list	list	k1gInSc4	list
Zájmy	zájem	k1gInPc1	zájem
venkova	venkov	k1gInSc2	venkov
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
vycházela	vycházet	k5eAaImAgFnS	vycházet
původně	původně	k6eAd1	původně
sociálnědemokratická	sociálnědemokratický	k2eAgFnSc1d1	sociálnědemokratická
Jiskra	jiskra	k1gFnSc1	jiskra
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
komunistická	komunistický	k2eAgFnSc1d1	komunistická
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
21	[number]	k4	21
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1968	[number]	k4	1968
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
běžné	běžný	k2eAgNnSc1d1	běžné
číslo	číslo	k1gNnSc1	číslo
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
Jiskry	jiskra	k1gFnSc2	jiskra
<g/>
,	,	kIx,	,
další	další	k2eAgInSc4d1	další
týden	týden	k1gInSc4	týden
již	již	k6eAd1	již
byla	být	k5eAaImAgFnS	být
tiskárna	tiskárna	k1gFnSc1	tiskárna
v	v	k7c6	v
Jihlavě	Jihlava	k1gFnSc6	Jihlava
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
list	list	k1gInSc1	list
tištěn	tisknout	k5eAaImNgInS	tisknout
<g/>
,	,	kIx,	,
uzavřena	uzavřen	k2eAgFnSc1d1	uzavřena
a	a	k8xC	a
další	další	k2eAgNnSc1d1	další
číslo	číslo	k1gNnSc1	číslo
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
menším	malý	k2eAgInSc7d2	menší
nákladem	náklad	k1gInSc7	náklad
tiskem	tisk	k1gInSc7	tisk
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
dvojstránkové	dvojstránkový	k2eAgNnSc1d1	dvojstránkový
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnPc4d1	další
tři	tři	k4xCgNnPc4	tři
čísla	číslo	k1gNnPc4	číslo
byla	být	k5eAaImAgFnS	být
ještě	ještě	k6eAd1	ještě
mimořádná	mimořádný	k2eAgFnSc1d1	mimořádná
a	a	k8xC	a
po	po	k7c6	po
těchto	tento	k3xDgInPc6	tento
problémech	problém	k1gInPc6	problém
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
dvojčíslo	dvojčíslo	k1gNnSc1	dvojčíslo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
shrnovalo	shrnovat	k5eAaImAgNnS	shrnovat
nastalé	nastalý	k2eAgFnPc4d1	nastalá
události	událost	k1gFnPc4	událost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
vychází	vycházet	k5eAaImIp3nS	vycházet
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
radniční	radniční	k2eAgInSc1d1	radniční
měsíčník	měsíčník	k1gInSc1	měsíčník
Třebíčský	třebíčský	k2eAgInSc1d1	třebíčský
zpravodaj	zpravodaj	k1gInSc1	zpravodaj
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1977	[number]	k4	1977
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Zpravodaj	zpravodaj	k1gInSc1	zpravodaj
města	město	k1gNnSc2	město
Třebíče	Třebíč	k1gFnSc2	Třebíč
jako	jako	k8xS	jako
sborník	sborník	k1gInSc1	sborník
informací	informace	k1gFnPc2	informace
formátu	formát	k1gInSc2	formát
A	A	kA	A
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
s	s	k7c7	s
názvem	název	k1gInSc7	název
Třebíčský	třebíčský	k2eAgInSc1d1	třebíčský
zpravodaj	zpravodaj	k1gInSc1	zpravodaj
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
barevný	barevný	k2eAgInSc4d1	barevný
magazín	magazín	k1gInSc4	magazín
velikosti	velikost	k1gFnSc2	velikost
A	a	k9	a
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
vydávání	vydávání	k1gNnSc1	vydávání
je	být	k5eAaImIp3nS	být
plně	plně	k6eAd1	plně
v	v	k7c6	v
režii	režie	k1gFnSc6	režie
třebíčské	třebíčský	k2eAgFnSc2d1	Třebíčská
radnice	radnice	k1gFnSc2	radnice
<g/>
,	,	kIx,	,
odpovědným	odpovědný	k2eAgMnSc7d1	odpovědný
redaktorem	redaktor	k1gMnSc7	redaktor
je	být	k5eAaImIp3nS	být
Milan	Milan	k1gMnSc1	Milan
Krčmář	Krčmář	k1gMnSc1	Krčmář
<g/>
,	,	kIx,	,
tisk	tisk	k1gInSc4	tisk
a	a	k8xC	a
distribuci	distribuce	k1gFnSc4	distribuce
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
společnost	společnost	k1gFnSc1	společnost
Yashica	Yashica	k1gFnSc1	Yashica
<g/>
.	.	kIx.	.
</s>
<s>
Třebíčský	třebíčský	k2eAgInSc1d1	třebíčský
zpravodaj	zpravodaj	k1gInSc1	zpravodaj
je	být	k5eAaImIp3nS	být
vydáván	vydávat	k5eAaImNgInS	vydávat
11	[number]	k4	11
<g/>
krát	krát	k6eAd1	krát
ročně	ročně	k6eAd1	ročně
v	v	k7c6	v
nákladu	náklad	k1gInSc6	náklad
cca	cca	kA	cca
17	[number]	k4	17
500	[number]	k4	500
výtisků	výtisk	k1gInPc2	výtisk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
byl	být	k5eAaImAgInS	být
distribuován	distribuovat	k5eAaBmNgInS	distribuovat
Českou	český	k2eAgFnSc7d1	Česká
poštou	pošta	k1gFnSc7	pošta
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2019	[number]	k4	2019
je	být	k5eAaImIp3nS	být
distribuován	distribuovat	k5eAaBmNgInS	distribuovat
společností	společnost	k1gFnSc7	společnost
Mediaservis	Mediaservis	k1gFnPc2	Mediaservis
<g/>
.	.	kIx.	.
</s>
<s>
Náklady	náklad	k1gInPc1	náklad
na	na	k7c4	na
tisk	tisk	k1gInSc4	tisk
každého	každý	k3xTgNnSc2	každý
čísla	číslo	k1gNnSc2	číslo
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
23	[number]	k4	23
tisíc	tisíc	k4xCgInPc2	tisíc
Kč	Kč	kA	Kč
za	za	k7c4	za
distribuci	distribuce	k1gFnSc4	distribuce
a	a	k8xC	a
70	[number]	k4	70
tisíc	tisíc	k4xCgInPc2	tisíc
Kč	Kč	kA	Kč
za	za	k7c4	za
tisk	tisk	k1gInSc4	tisk
<g/>
,	,	kIx,	,
příjmy	příjem	k1gInPc4	příjem
z	z	k7c2	z
inzerce	inzerce	k1gFnSc2	inzerce
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
přibližně	přibližně	k6eAd1	přibližně
44	[number]	k4	44
tisíc	tisíc	k4xCgInPc2	tisíc
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
<g/>
Dalšími	další	k2eAgMnPc7d1	další
tištěnými	tištěný	k2eAgFnPc7d1	tištěná
médii	médium	k1gNnPc7	médium
vycházejícími	vycházející	k2eAgFnPc7d1	vycházející
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
jsou	být	k5eAaImIp3nP	být
Horácké	horácký	k2eAgFnPc1d1	Horácká
noviny	novina	k1gFnPc1	novina
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
noviny	novina	k1gFnPc4	novina
Jiskra	jiskra	k1gFnSc1	jiskra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
číslo	číslo	k1gNnSc1	číslo
Horáckých	horácký	k2eAgFnPc2d1	Horácká
novin	novina	k1gFnPc2	novina
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
25	[number]	k4	25
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
šéfredaktorem	šéfredaktor	k1gMnSc7	šéfredaktor
byl	být	k5eAaImAgMnS	být
Richard	Richard	k1gMnSc1	Richard
Paleček	Paleček	k1gMnSc1	Paleček
(	(	kIx(	(
<g/>
narozen	narozen	k2eAgMnSc1d1	narozen
26	[number]	k4	26
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
zůstal	zůstat	k5eAaPmAgInS	zůstat
redaktorem	redaktor	k1gMnSc7	redaktor
novin	novina	k1gFnPc2	novina
Jiskra	jiskra	k1gFnSc1	jiskra
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
moderátorem	moderátor	k1gMnSc7	moderátor
revolučního	revoluční	k2eAgInSc2d1	revoluční
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
vycházely	vycházet	k5eAaImAgFnP	vycházet
Třebíčské	třebíčský	k2eAgFnPc1d1	Třebíčská
noviny	novina	k1gFnPc1	novina
v	v	k7c6	v
původním	původní	k2eAgNnSc6d1	původní
složení	složení	k1gNnSc6	složení
redakce	redakce	k1gFnSc2	redakce
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
v	v	k7c6	v
září	září	k1gNnSc6	září
1995	[number]	k4	1995
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
první	první	k4xOgNnSc1	první
vydání	vydání	k1gNnSc1	vydání
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Miloslava	Miloslav	k1gMnSc2	Miloslav
Zemana	Zeman	k1gMnSc2	Zeman
(	(	kIx(	(
<g/>
1933	[number]	k4	1933
<g/>
–	–	k?	–
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
a	a	k8xC	a
Otta	Otta	k1gMnSc1	Otta
Zejdy	Zejda	k1gMnSc2	Zejda
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1928	[number]	k4	1928
–	–	k?	–
28	[number]	k4	28
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
se	se	k3xPyFc4	se
vydavatelkou	vydavatelka	k1gFnSc7	vydavatelka
a	a	k8xC	a
redaktorkou	redaktorka	k1gFnSc7	redaktorka
stala	stát	k5eAaPmAgFnS	stát
Anna	Anna	k1gFnSc1	Anna
Kočová	Kočová	k1gFnSc1	Kočová
<g/>
,	,	kIx,	,
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
se	se	k3xPyFc4	se
redaktorem	redaktor	k1gMnSc7	redaktor
stal	stát	k5eAaPmAgMnS	stát
Luboš	Luboš	k1gMnSc1	Luboš
Janoušek	Janoušek	k1gMnSc1	Janoušek
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
noviny	novina	k1gFnPc4	novina
vede	vést	k5eAaImIp3nS	vést
Antonín	Antonín	k1gMnSc1	Antonín
Zvěřina	Zvěřina	k1gMnSc1	Zvěřina
<g/>
.	.	kIx.	.
</s>
<s>
Vltava-Press	Vltava-Press	k6eAd1	Vltava-Press
vydává	vydávat	k5eAaImIp3nS	vydávat
i	i	k9	i
regionální	regionální	k2eAgInSc4d1	regionální
deník	deník	k1gInSc4	deník
<g/>
,	,	kIx,	,
Třebíčský	třebíčský	k2eAgInSc4d1	třebíčský
deník	deník	k1gInSc4	deník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2008	[number]	k4	2008
město	město	k1gNnSc1	město
Třebíč	Třebíč	k1gFnSc4	Třebíč
navázalo	navázat	k5eAaPmAgNnS	navázat
spolupráci	spolupráce	k1gFnSc4	spolupráce
s	s	k7c7	s
TV	TV	kA	TV
Prima	prima	k6eAd1	prima
a	a	k8xC	a
v	v	k7c6	v
pořadu	pořad	k1gInSc6	pořad
Minuty	minuta	k1gFnSc2	minuta
regionu	region	k1gInSc2	region
–	–	k?	–
Zrcadlo	zrcadlo	k1gNnSc1	zrcadlo
vašeho	váš	k3xOp2gInSc2	váš
kraje	kraj	k1gInSc2	kraj
jsou	být	k5eAaImIp3nP	být
vysílány	vysílán	k2eAgFnPc1d1	vysílána
zprávy	zpráva	k1gFnPc1	zpráva
třebíčské	třebíčský	k2eAgFnSc2d1	Třebíčská
radnice	radnice	k1gFnSc2	radnice
<g/>
.	.	kIx.	.
</s>
<s>
ZŠ	ZŠ	kA	ZŠ
Na	na	k7c6	na
Kopcích	kopec	k1gInPc6	kopec
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
navázala	navázat	k5eAaPmAgFnS	navázat
spolupráci	spolupráce	k1gFnSc4	spolupráce
s	s	k7c7	s
ČT24	ČT24	k1gFnSc7	ČT24
a	a	k8xC	a
jejím	její	k3xOp3gInSc7	její
projektem	projekt	k1gInSc7	projekt
Digitální	digitální	k2eAgFnSc1d1	digitální
vesnice	vesnice	k1gFnSc1	vesnice
<g/>
,	,	kIx,	,
příspěvky	příspěvek	k1gInPc1	příspěvek
jejích	její	k3xOp3gMnPc2	její
žáků	žák	k1gMnPc2	žák
jsou	být	k5eAaImIp3nP	být
tak	tak	k6eAd1	tak
vysílány	vysílat	k5eAaImNgInP	vysílat
na	na	k7c4	na
ČT	ČT	kA	ČT
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
</s>
<s>
Rozhlasové	rozhlasový	k2eAgNnSc1d1	rozhlasové
vysílání	vysílání	k1gNnSc1	vysílání
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
regionální	regionální	k2eAgFnSc1d1	regionální
<g/>
,	,	kIx,	,
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
není	být	k5eNaImIp3nS	být
žádná	žádný	k3yNgFnSc1	žádný
stanice	stanice	k1gFnSc1	stanice
<g/>
,	,	kIx,	,
nejbližší	blízký	k2eAgFnSc1d3	nejbližší
regionální	regionální	k2eAgFnSc1d1	regionální
stanice	stanice	k1gFnSc1	stanice
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
Jihlavě	Jihlava	k1gFnSc6	Jihlava
(	(	kIx(	(
<g/>
Hitrádio	Hitrádio	k6eAd1	Hitrádio
Vysočina	vysočina	k1gFnSc1	vysočina
<g/>
,	,	kIx,	,
Český	český	k2eAgInSc1d1	český
rozhlas	rozhlas	k1gInSc1	rozhlas
Region	region	k1gInSc1	region
a	a	k8xC	a
Radio	radio	k1gNnSc1	radio
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2017	[number]	k4	2017
rádio	rádio	k1gNnSc4	rádio
Hey	Hey	k1gFnSc2	Hey
rádio	rádio	k1gNnSc1	rádio
spustilo	spustit	k5eAaPmAgNnS	spustit
vysílač	vysílač	k1gInSc4	vysílač
na	na	k7c4	na
frekvenci	frekvence	k1gFnSc4	frekvence
97,6	[number]	k4	97,6
FM	FM	kA	FM
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
<g/>
.	.	kIx.	.
<g/>
Město	město	k1gNnSc1	město
Třebíč	Třebíč	k1gFnSc4	Třebíč
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
používá	používat	k5eAaImIp3nS	používat
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
propagaci	propagace	k1gFnSc3	propagace
moderní	moderní	k2eAgFnSc4d1	moderní
komunikační	komunikační	k2eAgFnSc4d1	komunikační
média	médium	k1gNnPc4	médium
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
i	i	k9	i
server	server	k1gInSc4	server
pro	pro	k7c4	pro
sdílení	sdílení	k1gNnSc4	sdílení
videozáznamů	videozáznam	k1gInPc2	videozáznam
YouTube	YouTub	k1gInSc5	YouTub
a	a	k8xC	a
systém	systém	k1gInSc4	systém
Twitter	Twittra	k1gFnPc2	Twittra
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
nově	nově	k6eAd1	nově
komunikuje	komunikovat	k5eAaImIp3nS	komunikovat
s	s	k7c7	s
občany	občan	k1gMnPc7	občan
také	také	k6eAd1	také
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
služby	služba	k1gFnSc2	služba
tzv.	tzv.	kA	tzv.
Mobilního	mobilní	k2eAgInSc2d1	mobilní
rozhlasu	rozhlas	k1gInSc2	rozhlas
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
SMS	SMS	kA	SMS
zpráv	zpráva	k1gFnPc2	zpráva
či	či	k8xC	či
aplikace	aplikace	k1gFnSc2	aplikace
pro	pro	k7c4	pro
mobilní	mobilní	k2eAgInPc4d1	mobilní
telefony	telefon	k1gInPc4	telefon
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
Třebíč	Třebíč	k1gFnSc1	Třebíč
zmíněno	zmínit	k5eAaPmNgNnS	zmínit
jako	jako	k8xS	jako
součást	součást	k1gFnSc1	součást
děje	dít	k5eAaImIp3nS	dít
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Zejtra	Zejtra	k?	Zejtra
napořád	napořád	k6eAd1	napořád
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
natáčen	natáčen	k2eAgInSc4d1	natáčen
seriál	seriál	k1gInSc4	seriál
Četníci	četník	k1gMnPc1	četník
z	z	k7c2	z
Luhačovic	Luhačovice	k1gFnPc2	Luhačovice
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
i	i	k8xC	i
film	film	k1gInSc4	film
Zádušní	zádušní	k2eAgFnSc4d1	zádušní
oběť	oběť	k1gFnSc4	oběť
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
měl	mít	k5eAaImAgMnS	mít
premiéru	premiér	k1gMnSc3	premiér
film	film	k1gInSc4	film
s	s	k7c7	s
názvem	název	k1gInSc7	název
Planeta	planeta	k1gFnSc1	planeta
Třebíč	Třebíč	k1gFnSc1	Třebíč
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
autorem	autor	k1gMnSc7	autor
je	být	k5eAaImIp3nS	být
Rakušan	Rakušan	k1gMnSc1	Rakušan
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Fassold	Fassold	k1gMnSc1	Fassold
<g/>
,	,	kIx,	,
film	film	k1gInSc1	film
má	mít	k5eAaImIp3nS	mít
na	na	k7c4	na
Třebíč	Třebíč	k1gFnSc4	Třebíč
nahlížet	nahlížet	k5eAaImF	nahlížet
pohledem	pohled	k1gInSc7	pohled
mimozemšťana	mimozemšťan	k1gMnSc2	mimozemšťan
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
dokument	dokument	k1gInSc4	dokument
o	o	k7c6	o
Třebíči	Třebíč	k1gFnSc6	Třebíč
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc4	její
historii	historie	k1gFnSc4	historie
<g/>
,	,	kIx,	,
lidech	člověk	k1gMnPc6	člověk
a	a	k8xC	a
cizincích	cizinec	k1gMnPc6	cizinec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
zde	zde	k6eAd1	zde
žijí	žít	k5eAaImIp3nP	žít
<g/>
.	.	kIx.	.
</s>
<s>
Očekává	očekávat	k5eAaImIp3nS	očekávat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
odvysílán	odvysílán	k2eAgMnSc1d1	odvysílán
na	na	k7c6	na
stanicích	stanice	k1gFnPc6	stanice
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
či	či	k8xC	či
Izraeli	Izrael	k1gInSc6	Izrael
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
vyšla	vyjít	k5eAaPmAgFnS	vyjít
kniha	kniha	k1gFnSc1	kniha
s	s	k7c7	s
názvem	název	k1gInSc7	název
Příběh	příběh	k1gInSc1	příběh
chytrého	chytrý	k2eAgNnSc2d1	chytré
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
na	na	k7c6	na
recepci	recepce	k1gFnSc6	recepce
Alternátoru	alternátor	k1gInSc2	alternátor
<g/>
,	,	kIx,	,
autorem	autor	k1gMnSc7	autor
je	být	k5eAaImIp3nS	být
Petr	Petr	k1gMnSc1	Petr
Mrázek	Mrázek	k1gMnSc1	Mrázek
a	a	k8xC	a
zabývá	zabývat	k5eAaImIp3nS	zabývat
se	se	k3xPyFc4	se
příběhem	příběh	k1gInSc7	příběh
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
chytrého	chytrý	k2eAgNnSc2d1	chytré
města	město	k1gNnSc2	město
Třebíče	Třebíč	k1gFnSc2	Třebíč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Slavní	slavný	k2eAgMnPc1d1	slavný
rodáci	rodák	k1gMnPc1	rodák
a	a	k8xC	a
čestní	čestný	k2eAgMnPc1d1	čestný
občané	občan	k1gMnPc1	občan
==	==	k?	==
</s>
</p>
<p>
<s>
Rodáky	rodák	k1gMnPc4	rodák
a	a	k8xC	a
významnými	významný	k2eAgMnPc7d1	významný
obyvateli	obyvatel	k1gMnPc7	obyvatel
města	město	k1gNnSc2	město
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
sportovci	sportovec	k1gMnPc1	sportovec
(	(	kIx(	(
<g/>
Patrik	Patrik	k1gMnSc1	Patrik
Eliáš	Eliáš	k1gMnSc1	Eliáš
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
Erat	Erat	k1gMnSc1	Erat
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Sobotka	Sobotka	k1gMnSc1	Sobotka
<g/>
,	,	kIx,	,
Ondřej	Ondřej	k1gMnSc1	Ondřej
Němec	Němec	k1gMnSc1	Němec
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Bouzek	Bouzek	k1gInSc1	Bouzek
<g/>
,	,	kIx,	,
Theodor	Theodor	k1gMnSc1	Theodor
Gebre	Gebr	k1gInSc5	Gebr
Selassie	Selassie	k1gFnPc1	Selassie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
umělci	umělec	k1gMnPc1	umělec
(	(	kIx(	(
<g/>
Míla	Míla	k1gFnSc1	Míla
Myslíková	Myslíková	k1gFnSc1	Myslíková
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
Novák	Novák	k1gMnSc1	Novák
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
Donutil	donutit	k5eAaPmAgMnS	donutit
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
politici	politik	k1gMnPc1	politik
(	(	kIx(	(
<g/>
armádní	armádní	k2eAgMnSc1d1	armádní
generál	generál	k1gMnSc1	generál
Jan	Jan	k1gMnSc1	Jan
Syrový	syrový	k2eAgMnSc1d1	syrový
<g/>
,	,	kIx,	,
MUDr.	MUDr.	kA	MUDr.
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Zvěřina	Zvěřina	k1gMnSc1	Zvěřina
<g/>
,	,	kIx,	,
generál	generál	k1gMnSc1	generál
Ludvík	Ludvík	k1gMnSc1	Ludvík
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
Bohumír	Bohumír	k1gMnSc1	Bohumír
Šmeral	Šmeral	k1gMnSc1	Šmeral
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
Kalina	Kalina	k1gMnSc1	Kalina
<g/>
,	,	kIx,	,
zachránce	zachránce	k1gMnSc2	zachránce
Židů	Žid	k1gMnPc2	Žid
před	před	k7c7	před
holocaustem	holocaust	k1gInSc7	holocaust
<g/>
,	,	kIx,	,
rabín	rabín	k1gMnSc1	rabín
Adolf	Adolf	k1gMnSc1	Adolf
Kurrein	Kurrein	k1gMnSc1	Kurrein
<g/>
,	,	kIx,	,
důstojník	důstojník	k1gMnSc1	důstojník
a	a	k8xC	a
účastník	účastník	k1gMnSc1	účastník
odboje	odboj	k1gInSc2	odboj
Vladimír	Vladimír	k1gMnSc1	Vladimír
Šoffr	Šoffr	k1gMnSc1	Šoffr
<g/>
,	,	kIx,	,
zahraniční	zahraniční	k2eAgMnSc1d1	zahraniční
televizní	televizní	k2eAgMnSc1d1	televizní
reportér	reportér	k1gMnSc1	reportér
Bohumil	Bohumil	k1gMnSc1	Bohumil
Vostal	Vostal	k1gMnSc1	Vostal
<g/>
,	,	kIx,	,
meteorolog	meteorolog	k1gMnSc1	meteorolog
Michal	Michal	k1gMnSc1	Michal
Žák	Žák	k1gMnSc1	Žák
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mnohým	mnohé	k1gNnSc7	mnohé
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
postav	postava	k1gFnPc2	postava
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
dalším	další	k2eAgMnSc7d1	další
z	z	k7c2	z
celé	celý	k2eAgFnSc2d1	celá
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
uděleno	udělit	k5eAaPmNgNnS	udělit
čestné	čestný	k2eAgNnSc1d1	čestné
občanství	občanství	k1gNnSc1	občanství
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Udělováno	udělován	k2eAgNnSc1d1	udělováno
je	být	k5eAaImIp3nS	být
významným	významný	k2eAgFnPc3d1	významná
osobám	osoba	k1gFnPc3	osoba
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
zasloužily	zasloužit	k5eAaPmAgFnP	zasloužit
o	o	k7c4	o
Třebíč	Třebíč	k1gFnSc4	Třebíč
nebo	nebo	k8xC	nebo
o	o	k7c4	o
české	český	k2eAgFnPc4d1	Česká
země	zem	k1gFnPc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Kontroverzními	kontroverzní	k2eAgFnPc7d1	kontroverzní
osobami	osoba	k1gFnPc7	osoba
v	v	k7c6	v
seznamu	seznam	k1gInSc6	seznam
jsou	být	k5eAaImIp3nP	být
prezidenti	prezident	k1gMnPc1	prezident
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
před	před	k7c7	před
sametovou	sametový	k2eAgFnSc7d1	sametová
revolucí	revoluce	k1gFnSc7	revoluce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Partnerská	partnerský	k2eAgNnPc1d1	partnerské
města	město	k1gNnPc1	město
==	==	k?	==
</s>
</p>
<p>
<s>
Město	město	k1gNnSc1	město
má	mít	k5eAaImIp3nS	mít
celkem	celkem	k6eAd1	celkem
čtyři	čtyři	k4xCgNnPc4	čtyři
partnerská	partnerský	k2eAgNnPc4d1	partnerské
města	město	k1gNnPc4	město
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
</s>
<s>
městy	město	k1gNnPc7	město
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2018	[number]	k4	2018
se	se	k3xPyFc4	se
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
spolupráce	spolupráce	k1gFnSc1	spolupráce
s	s	k7c7	s
městem	město	k1gNnSc7	město
Ulsan	Ulsana	k1gFnPc2	Ulsana
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Koreji	Korea	k1gFnSc6	Korea
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
jaderných	jaderný	k2eAgFnPc2d1	jaderná
technologií	technologie	k1gFnPc2	technologie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oschatz	Oschatz	k1gInSc1	Oschatz
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
</s>
</p>
<p>
<s>
Lilienfeld	Lilienfeld	k1gInSc1	Lilienfeld
<g/>
,	,	kIx,	,
Rakousko	Rakousko	k1gNnSc1	Rakousko
</s>
</p>
<p>
<s>
Humenné	Humenné	k1gNnSc1	Humenné
<g/>
,	,	kIx,	,
Slovensko	Slovensko	k1gNnSc1	Slovensko
</s>
</p>
<p>
<s>
Rachov	Rachov	k1gInSc1	Rachov
<g/>
,	,	kIx,	,
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
stádiu	stádium	k1gNnSc6	stádium
jednání	jednání	k1gNnSc2	jednání
o	o	k7c4	o
partnerství	partnerství	k1gNnSc4	partnerství
<g/>
,	,	kIx,	,
dojednáno	dojednán	k2eAgNnSc4d1	dojednáno
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2017	[number]	k4	2017
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
starosty	starosta	k1gMnSc2	starosta
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
představitelů	představitel	k1gMnPc2	představitel
města	město	k1gNnSc2	město
do	do	k7c2	do
Zakarpatské	zakarpatský	k2eAgFnSc2d1	Zakarpatská
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
25	[number]	k4	25
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
podepsali	podepsat	k5eAaPmAgMnP	podepsat
představitelé	představitel	k1gMnPc1	představitel
města	město	k1gNnSc2	město
Rachov	Rachov	k1gInSc1	Rachov
smlouvu	smlouva	k1gFnSc4	smlouva
o	o	k7c6	o
partnerství	partnerství	k1gNnSc6	partnerství
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
<g/>
,	,	kIx,	,
město	město	k1gNnSc1	město
Třebíč	Třebíč	k1gFnSc1	Třebíč
schválilo	schválit	k5eAaPmAgNnS	schválit
partnerství	partnerství	k1gNnSc1	partnerství
21	[number]	k4	21
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
I-čchang	I-čchang	k1gInSc1	I-čchang
<g/>
,	,	kIx,	,
Čína	Čína	k1gFnSc1	Čína
<g/>
,	,	kIx,	,
původní	původní	k2eAgFnSc1d1	původní
myšlenka	myšlenka	k1gFnSc1	myšlenka
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
pri	pri	k?	pri
návštěvě	návštěva	k1gFnSc3	návštěva
delegace	delegace	k1gFnSc2	delegace
Kraje	kraj	k1gInSc2	kraj
Vysočina	vysočina	k1gFnSc1	vysočina
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byly	být	k5eAaImAgFnP	být
zástupci	zástupce	k1gMnPc7	zástupce
kraje	kraj	k1gInSc2	kraj
osloveni	oslovit	k5eAaPmNgMnP	oslovit
k	k	k7c3	k
vytipování	vytipování	k1gNnSc3	vytipování
partnerského	partnerský	k2eAgNnSc2d1	partnerské
města	město	k1gNnSc2	město
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
HEDBÁVNÝ	hedbávný	k2eAgMnSc1d1	hedbávný
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
<g/>
.	.	kIx.	.
</s>
<s>
Třebíčské	třebíčský	k2eAgFnSc2d1	Třebíčská
pověsti	pověst	k1gFnSc2	pověst
a	a	k8xC	a
zajímavosti	zajímavost	k1gFnSc2	zajímavost
<g/>
.	.	kIx.	.
</s>
<s>
Ilustrace	ilustrace	k1gFnSc1	ilustrace
Josef	Josef	k1gMnSc1	Josef
Dvořák	Dvořák	k1gMnSc1	Dvořák
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
rozšířené	rozšířený	k2eAgFnPc4d1	rozšířená
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Třebíč	Třebíč	k1gFnSc1	Třebíč
<g/>
:	:	kIx,	:
Akcent	akcent	k1gInSc1	akcent
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
198	[number]	k4	198
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
8072683403	[number]	k4	8072683403
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KLENOVSKÝ	KLENOVSKÝ	kA	KLENOVSKÝ
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Židovské	židovský	k2eAgFnPc1d1	židovská
památky	památka	k1gFnPc1	památka
Třebíče	Třebíč	k1gFnSc2	Třebíč
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
doplněné	doplněný	k2eAgFnPc4d1	doplněná
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
CERM	CERM	kA	CERM
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
68	[number]	k4	68
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
8072042858	[number]	k4	8072042858
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
FIŠER	Fišer	k1gMnSc1	Fišer
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
<g/>
.	.	kIx.	.
</s>
<s>
Třebíč	Třebíč	k1gFnSc1	Třebíč
-	-	kIx~	-
Z	z	k7c2	z
historie	historie	k1gFnSc2	historie
benediktinského	benediktinský	k2eAgNnSc2d1	benediktinské
opatství	opatství	k1gNnSc2	opatství
<g/>
.	.	kIx.	.
</s>
<s>
Foto	foto	k1gNnSc1	foto
František	František	k1gMnSc1	František
Fiala	Fiala	k1gMnSc1	Fiala
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Třebíč	Třebíč	k1gFnSc1	Třebíč
<g/>
:	:	kIx,	:
Fibox	Fibox	k1gInSc1	Fibox
Třebíč	Třebíč	k1gFnSc1	Třebíč
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
183	[number]	k4	183
s.	s.	k?	s.
(	(	kIx(	(
<g/>
Edice	edice	k1gFnSc1	edice
Vysočiny	vysočina	k1gFnSc2	vysočina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
8085571161	[number]	k4	8085571161
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
kolektiv	kolektiv	k1gInSc1	kolektiv
autorů	autor	k1gMnPc2	autor
<g/>
.	.	kIx.	.
</s>
<s>
Třebíč	Třebíč	k1gFnSc1	Třebíč
-	-	kIx~	-
Historie	historie	k1gFnSc1	historie
a	a	k8xC	a
památky	památka	k1gFnPc1	památka
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Třebíč	Třebíč	k1gFnSc1	Třebíč
<g/>
:	:	kIx,	:
Město	město	k1gNnSc1	město
Třebíč	Třebíč	k1gFnSc1	Třebíč
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
87	[number]	k4	87
s.	s.	k?	s.
</s>
</p>
<p>
<s>
NAVRKAL	Navrkal	k1gMnSc1	Navrkal
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
<g/>
.	.	kIx.	.
</s>
<s>
Hráli	hrát	k5eAaImAgMnP	hrát
pro	pro	k7c4	pro
slávu	sláva	k1gFnSc4	sláva
Třebíče	Třebíč	k1gFnSc2	Třebíč
<g/>
.	.	kIx.	.
</s>
<s>
Ilustrace	ilustrace	k1gFnSc1	ilustrace
Vlastimil	Vlastimil	k1gMnSc1	Vlastimil
Toman	Toman	k1gMnSc1	Toman
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Třebíč	Třebíč	k1gFnSc1	Třebíč
<g/>
:	:	kIx,	:
Akcent	akcent	k1gInSc1	akcent
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
318	[number]	k4	318
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
807268339	[number]	k4	807268339
<g/>
X.	X.	kA	X.
</s>
</p>
<p>
<s>
neznámý	známý	k2eNgMnSc1d1	neznámý
<g/>
.	.	kIx.	.
</s>
<s>
Jubilejní	jubilejní	k2eAgFnSc1d1	jubilejní
ročenka	ročenka	k1gFnSc1	ročenka
Třebíče	Třebíč	k1gFnSc2	Třebíč
:	:	kIx,	:
K	K	kA	K
600	[number]	k4	600
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc3	výročí
města	město	k1gNnSc2	město
Třebíče	Třebíč	k1gFnSc2	Třebíč
<g/>
.	.	kIx.	.
</s>
<s>
Třebíč	Třebíč	k1gFnSc1	Třebíč
<g/>
:	:	kIx,	:
Českobratrský	českobratrský	k2eAgInSc1d1	českobratrský
evangelický	evangelický	k2eAgInSc1d1	evangelický
farní	farní	k2eAgInSc1d1	farní
sbor	sbor	k1gInSc1	sbor
<g/>
,	,	kIx,	,
1935	[number]	k4	1935
<g/>
.	.	kIx.	.
31	[number]	k4	31
s.	s.	k?	s.
</s>
</p>
<p>
<s>
BEČKOVÁ	Bečková	k1gFnSc1	Bečková
<g/>
,	,	kIx,	,
Jana	Jana	k1gFnSc1	Jana
<g/>
.	.	kIx.	.
</s>
<s>
Kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
pivo	pivo	k1gNnSc1	pivo
vaří	vařit	k5eAaImIp3nS	vařit
<g/>
...	...	k?	...
:	:	kIx,	:
Povídání	povídání	k1gNnSc1	povídání
nejenom	nejenom	k6eAd1	nejenom
o	o	k7c6	o
třebíčském	třebíčský	k2eAgNnSc6d1	třebíčské
vaření	vaření	k1gNnSc6	vaření
piva	pivo	k1gNnSc2	pivo
<g/>
.	.	kIx.	.
</s>
<s>
Ilustrace	ilustrace	k1gFnSc1	ilustrace
Josef	Josef	k1gMnSc1	Josef
Kremláček	Kremláček	k1gMnSc1	Kremláček
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Třebíč	Třebíč	k1gFnSc1	Třebíč
<g/>
:	:	kIx,	:
s.	s.	k?	s.
<g/>
n.	n.	k?	n.
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
38	[number]	k4	38
s.	s.	k?	s.
</s>
</p>
<p>
<s>
FIŠER	Fišer	k1gMnSc1	Fišer
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
<g/>
.	.	kIx.	.
</s>
<s>
Klášter	klášter	k1gInSc1	klášter
uprostřed	uprostřed	k7c2	uprostřed
lesa	les	k1gInSc2	les
:	:	kIx,	:
dvě	dva	k4xCgFnPc1	dva
studie	studie	k1gFnPc1	studie
o	o	k7c6	o
třebíčském	třebíčský	k2eAgNnSc6d1	třebíčské
benediktinském	benediktinský	k2eAgNnSc6d1	benediktinské
opatství	opatství	k1gNnSc6	opatství
<g/>
.	.	kIx.	.
</s>
<s>
Foto	foto	k1gNnSc1	foto
Josef	Josef	k1gMnSc1	Josef
Němec	Němec	k1gMnSc1	Němec
<g/>
,	,	kIx,	,
Libor	Libor	k1gMnSc1	Libor
Teplý	Teplý	k1gMnSc1	Teplý
<g/>
,	,	kIx,	,
ZMM	ZMM	kA	ZMM
Třebíč	Třebíč	k1gFnSc1	Třebíč
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Matice	matice	k1gFnSc1	matice
moravská	moravský	k2eAgFnSc1d1	Moravská
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
158	[number]	k4	158
s.	s.	k?	s.
<g/>
,	,	kIx,	,
ilustrace	ilustrace	k1gFnPc1	ilustrace
<g/>
,	,	kIx,	,
volné	volný	k2eAgFnPc1d1	volná
přílohy	příloha	k1gFnPc1	příloha
:	:	kIx,	:
3	[number]	k4	3
mapy	mapa	k1gFnSc2	mapa
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
8086488020	[number]	k4	8086488020
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HUDCOVÁ	Hudcová	k1gFnSc1	Hudcová
<g/>
,	,	kIx,	,
Šárka	Šárka	k1gFnSc1	Šárka
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
Třebíč	Třebíč	k1gFnSc1	Třebíč
a	a	k8xC	a
okolí	okolí	k1gNnSc1	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Fotografové	fotograf	k1gMnPc1	fotograf
V.	V.	kA	V.
Hyhlík	Hyhlík	k1gMnSc1	Hyhlík
....	....	k?	....
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Třebíč	Třebíč	k1gFnSc1	Třebíč
<g/>
:	:	kIx,	:
Západomoravské	západomoravský	k2eAgNnSc1d1	Západomoravské
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
;	;	kIx,	;
Město	město	k1gNnSc1	město
Třebíč	Třebíč	k1gFnSc1	Třebíč
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
41	[number]	k4	41
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
8023867970	[number]	k4	8023867970
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Městský	městský	k2eAgInSc1d1	městský
úřad	úřad	k1gInSc1	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Modrá	modrý	k2eAgFnSc1d1	modrá
kniha	kniha	k1gFnSc1	kniha
města	město	k1gNnSc2	město
Třebíče	Třebíč	k1gFnSc2	Třebíč
:	:	kIx,	:
poznámky	poznámka	k1gFnPc1	poznámka
ke	k	k7c3	k
stavu	stav	k1gInSc3	stav
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Třebíč	Třebíč	k1gFnSc1	Třebíč
<g/>
:	:	kIx,	:
Městský	městský	k2eAgInSc1d1	městský
úřad	úřad	k1gInSc1	úřad
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
55	[number]	k4	55
s.	s.	k?	s.
</s>
</p>
<p>
<s>
ZEJDA	Zejda	k1gMnSc1	Zejda
<g/>
,	,	kIx,	,
Radovan	Radovan	k1gMnSc1	Radovan
<g/>
.	.	kIx.	.
</s>
<s>
Osobnosti	osobnost	k1gFnPc1	osobnost
Třebíčska	Třebíčsko	k1gNnSc2	Třebíčsko
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Třebíč	Třebíč	k1gFnSc1	Třebíč
<g/>
:	:	kIx,	:
Akcent	akcent	k1gInSc1	akcent
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
227	[number]	k4	227
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
8072681044	[number]	k4	8072681044
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
BECKOVÁ	BECKOVÁ	kA	BECKOVÁ
<g/>
,	,	kIx,	,
Hana	Hana	k1gFnSc1	Hana
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
historie	historie	k1gFnSc2	historie
veřejné	veřejný	k2eAgFnSc2d1	veřejná
knihovny	knihovna	k1gFnSc2	knihovna
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Třebíč	Třebíč	k1gFnSc1	Třebíč
<g/>
:	:	kIx,	:
Okresní	okresní	k2eAgFnSc1d1	okresní
knihovna	knihovna	k1gFnSc1	knihovna
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
18	[number]	k4	18
s.	s.	k?	s.
</s>
</p>
<p>
<s>
NOVÁK	Novák	k1gMnSc1	Novák
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
<g/>
.	.	kIx.	.
</s>
<s>
Zámostí	Zámostit	k5eAaPmIp3nS	Zámostit
<g/>
.	.	kIx.	.
</s>
<s>
Fotografie	fotografie	k1gFnSc1	fotografie
Pavel	Pavel	k1gMnSc1	Pavel
Heřman	Heřman	k1gMnSc1	Heřman
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Třebíč	Třebíč	k1gFnSc1	Třebíč
<g/>
:	:	kIx,	:
Arca	Arca	k1gFnSc1	Arca
Jimfa	Jimf	k1gMnSc2	Jimf
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
36	[number]	k4	36
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
8085766361	[number]	k4	8085766361
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HANELOVÁ	HANELOVÁ	kA	HANELOVÁ
<g/>
,	,	kIx,	,
Nela	Nela	k1gFnSc1	Nela
<g/>
;	;	kIx,	;
HEŘMAN	Heřman	k1gMnSc1	Heřman
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
<g/>
.	.	kIx.	.
</s>
<s>
Zámostí	Zámostit	k5eAaPmIp3nS	Zámostit
v	v	k7c6	v
nás	my	k3xPp1nPc6	my
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Třebíč	Třebíč	k1gFnSc1	Třebíč
<g/>
:	:	kIx,	:
KVIZ	kviz	k1gInSc1	kviz
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
xii	xii	k?	xii
s.	s.	k?	s.
</s>
</p>
<p>
<s>
SMEJKAL	Smejkal	k1gMnSc1	Smejkal
<g/>
,	,	kIx,	,
Jindřich	Jindřich	k1gMnSc1	Jindřich
<g/>
.	.	kIx.	.
</s>
<s>
Zaplavené	zaplavený	k2eAgFnPc1d1	zaplavená
vzpomínky	vzpomínka	k1gFnPc1	vzpomínka
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Třebíč	Třebíč	k1gFnSc1	Třebíč
<g/>
:	:	kIx,	:
J.	J.	kA	J.
Smejkal	Smejkal	k1gMnSc1	Smejkal
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
135	[number]	k4	135
s.	s.	k?	s.
</s>
</p>
<p>
<s>
ONDRÁČKOVÁ	Ondráčková	k1gFnSc1	Ondráčková
<g/>
,	,	kIx,	,
S.	S.	kA	S.
Třebíč	Třebíč	k1gFnSc1	Třebíč
<g/>
.	.	kIx.	.
</s>
<s>
Příroda	příroda	k1gFnSc1	příroda
Třebíčska	Třebíčsko	k1gNnSc2	Třebíčsko
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Blok	blok	k1gInSc1	blok
<g/>
,	,	kIx,	,
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
93	[number]	k4	93
s.	s.	k?	s.
</s>
</p>
<p>
<s>
kolektiv	kolektiv	k1gInSc1	kolektiv
autorů	autor	k1gMnPc2	autor
<g/>
.	.	kIx.	.
</s>
<s>
Pravěk	pravěk	k1gInSc1	pravěk
Třebíčska	Třebíčsko	k1gNnSc2	Třebíčsko
<g/>
.	.	kIx.	.
</s>
<s>
Ilustrace	ilustrace	k1gFnSc1	ilustrace
P.	P.	kA	P.
Šindelář	Šindelář	k1gMnSc1	Šindelář
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Muzejní	muzejní	k2eAgFnSc1d1	muzejní
a	a	k8xC	a
vlastivědná	vlastivědný	k2eAgFnSc1d1	vlastivědná
společnost	společnost	k1gFnSc1	společnost
<g/>
,	,	kIx,	,
1986	[number]	k4	1986
<g/>
.	.	kIx.	.
280	[number]	k4	280
s.	s.	k?	s.
</s>
</p>
<p>
<s>
JOURA	JOURA	kA	JOURA
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Procházky	procházka	k1gFnSc2	procházka
starou	starý	k2eAgFnSc7d1	stará
Třebíčí	Třebíč	k1gFnSc7	Třebíč
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Třebíč	Třebíč	k1gFnSc1	Třebíč
<g/>
:	:	kIx,	:
Amaprint	Amaprint	k1gMnSc1	Amaprint
Kerndl	Kerndl	k1gMnSc1	Kerndl
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
193	[number]	k4	193
s.	s.	k?	s.
</s>
</p>
<p>
<s>
JOURA	JOURA	kA	JOURA
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Procházky	Procházka	k1gMnPc7	Procházka
starou	stará	k1gFnSc4	stará
Třebíčí	Třebíč	k1gFnPc2	Třebíč
podruhé	podruhé	k6eAd1	podruhé
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Třebíč	Třebíč	k1gFnSc1	Třebíč
<g/>
:	:	kIx,	:
Amaprint	Amaprint	k1gMnSc1	Amaprint
Kerndl	Kerndl	k1gMnSc1	Kerndl
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
202	[number]	k4	202
s.	s.	k?	s.
</s>
</p>
<p>
<s>
KUBEŠ	Kubeš	k1gMnSc1	Kubeš
<g/>
,	,	kIx,	,
Adolf	Adolf	k1gMnSc1	Adolf
<g/>
.	.	kIx.	.
</s>
<s>
Dějepis	dějepis	k1gInSc1	dějepis
města	město	k1gNnSc2	město
Třebíče	Třebíč	k1gFnSc2	Třebíč
<g/>
.	.	kIx.	.
</s>
<s>
Třebíč	Třebíč	k1gFnSc1	Třebíč
<g/>
:	:	kIx,	:
J.	J.	kA	J.
F.	F.	kA	F.
Kubeš	Kubeš	k1gMnSc1	Kubeš
<g/>
,	,	kIx,	,
1874	[number]	k4	1874
<g/>
.	.	kIx.	.
173	[number]	k4	173
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
DVORSKÝ	Dvorský	k1gMnSc1	Dvorský
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
.	.	kIx.	.
</s>
<s>
Vlastivěda	vlastivěda	k1gFnSc1	vlastivěda
moravská	moravský	k2eAgFnSc1d1	Moravská
<g/>
.	.	kIx.	.
</s>
<s>
Redakce	redakce	k1gFnSc1	redakce
František	František	k1gMnSc1	František
Dvorský	Dvorský	k1gMnSc1	Dvorský
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Kameníček	kameníček	k1gInSc1	kameníček
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Hladík	Hladík	k1gMnSc1	Hladík
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Rypáček	rypáček	k1gInSc1	rypáček
<g/>
,	,	kIx,	,
Tomáš	Tomáš	k1gMnSc1	Tomáš
Šílený	šílený	k2eAgMnSc1d1	šílený
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Šujan	Šujan	k1gMnSc1	Šujan
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Muzejní	muzejní	k2eAgInSc1d1	muzejní
spolek	spolek	k1gInSc1	spolek
<g/>
,	,	kIx,	,
1906	[number]	k4	1906
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
Třebíč	Třebíč	k1gFnSc1	Třebíč
<g/>
,	,	kIx,	,
školství	školství	k1gNnSc1	školství
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ZEJDA	Zejda	k1gMnSc1	Zejda
<g/>
,	,	kIx,	,
Radovan	Radovan	k1gMnSc1	Radovan
<g/>
.	.	kIx.	.
</s>
<s>
Třebíčsko	Třebíčsko	k1gNnSc1	Třebíčsko
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Třebíč	Třebíč	k1gFnSc1	Třebíč
<g/>
:	:	kIx,	:
SURSUM	SURSUM	kA	SURSUM
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
206	[number]	k4	206
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
8085799839	[number]	k4	8085799839
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
BARTUŠEK	Bartušek	k1gMnSc1	Bartušek
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
<g/>
.	.	kIx.	.
</s>
<s>
Umělecké	umělecký	k2eAgFnPc1d1	umělecká
památky	památka	k1gFnPc1	památka
Třebíče	Třebíč	k1gFnSc2	Třebíč
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Blok	blok	k1gInSc1	blok
<g/>
,	,	kIx,	,
1969	[number]	k4	1969
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Okres	okres	k1gInSc1	okres
Třebíč	Třebíč	k1gFnSc1	Třebíč
</s>
</p>
<p>
<s>
Děkanát	děkanát	k1gInSc1	děkanát
Třebíč	Třebíč	k1gFnSc1	Třebíč
</s>
</p>
<p>
<s>
Okresní	okresní	k2eAgInSc1d1	okresní
soud	soud	k1gInSc1	soud
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
</s>
</p>
<p>
<s>
SK	Sk	kA	Sk
Horácká	horácký	k2eAgFnSc1d1	Horácká
Slavia	Slavia	k1gFnSc1	Slavia
Třebíč	Třebíč	k1gFnSc1	Třebíč
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
kamenů	kámen	k1gInPc2	kámen
zmizelých	zmizelý	k2eAgInPc2d1	zmizelý
v	v	k7c6	v
Kraji	kraj	k1gInSc6	kraj
Vysočina	vysočina	k1gFnSc1	vysočina
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Třebíč	Třebíč	k1gFnSc1	Třebíč
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Průvodce	průvodce	k1gMnSc1	průvodce
Třebíč	Třebíč	k1gFnSc1	Třebíč
ve	v	k7c6	v
Wikicestách	Wikicesta	k1gFnPc6	Wikicesta
</s>
</p>
<p>
<s>
Třebíč	Třebíč	k1gFnSc1	Třebíč
ve	v	k7c6	v
Vlastenském	vlastenský	k2eAgInSc6d1	vlastenský
slovníku	slovník	k1gInSc6	slovník
historickém	historický	k2eAgInSc6d1	historický
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc7	jejichž
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Třebíč	Třebíč	k1gFnSc1	Třebíč
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Třebíč	Třebíč	k1gFnSc1	Třebíč
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Třebíč	Třebíč	k1gFnSc1	Třebíč
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Zpráva	zpráva	k1gFnSc1	zpráva
Portál	portál	k1gInSc1	portál
<g/>
:	:	kIx,	:
<g/>
Třebíč	Třebíč	k1gFnSc1	Třebíč
ve	v	k7c6	v
Wikizprávách	Wikizpráva	k1gFnPc6	Wikizpráva
</s>
</p>
<p>
<s>
Téma	téma	k1gNnSc1	téma
Třebíč	Třebíč	k1gFnSc1	Třebíč
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Územně	územně	k6eAd1	územně
identifikační	identifikační	k2eAgInSc1d1	identifikační
registr	registr	k1gInSc1	registr
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc1	obec
Třebíč	Třebíč	k1gFnSc1	Třebíč
v	v	k7c6	v
Územně	územně	k6eAd1	územně
identifikačním	identifikační	k2eAgInSc6d1	identifikační
registru	registr	k1gInSc6	registr
ČR	ČR	kA	ČR
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
města	město	k1gNnSc2	město
</s>
</p>
<p>
<s>
SK	Sk	kA	Sk
Horácká	horácký	k2eAgFnSc1d1	Horácká
Slavia	Slavia	k1gFnSc1	Slavia
Třebíč	Třebíč	k1gFnSc1	Třebíč
</s>
</p>
<p>
<s>
UNESCO	Unesco	k1gNnSc1	Unesco
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Třebíč	Třebíč	k1gFnSc1	Třebíč
–	–	k?	–
jedinečnost	jedinečnost	k1gFnSc1	jedinečnost
mnohosti	mnohost	k1gFnSc2	mnohost
–	–	k?	–
video	video	k1gNnSc1	video
z	z	k7c2	z
archivu	archiv	k1gInSc2	archiv
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
v	v	k7c6	v
pořadu	pořad	k1gInSc6	pořad
Národní	národní	k2eAgInPc4d1	národní
klenoty	klenot	k1gInPc4	klenot
</s>
</p>
