<s>
Norijuki	Norijuki	k1gNnSc1	Norijuki
Abe	Abe	k1gFnSc2	Abe
(	(	kIx(	(
<g/>
阿	阿	k?	阿
記	記	k?	記
Abe	Abe	k1gFnSc2	Abe
Noriyuki	Noriyuk	k1gFnSc2	Noriyuk
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgInS	narodit
19	[number]	k4	19
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1961	[number]	k4	1961
v	v	k7c6	v
Kjótu	Kjóto	k1gNnSc6	Kjóto
<g/>
.	.	kIx.	.
</s>
<s>
Živí	živit	k5eAaImIp3nS	živit
se	se	k3xPyFc4	se
jako	jako	k9	jako
anime	animat	k5eAaPmIp3nS	animat
režisér	režisér	k1gMnSc1	režisér
<g/>
,	,	kIx,	,
scenárista	scenárista	k1gMnSc1	scenárista
a	a	k8xC	a
zvukový	zvukový	k2eAgMnSc1d1	zvukový
režisér	režisér	k1gMnSc1	režisér
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
známý	známý	k2eAgMnSc1d1	známý
díky	díky	k7c3	díky
spolupráci	spolupráce	k1gFnSc3	spolupráce
se	s	k7c7	s
Studiem	studio	k1gNnSc7	studio
Pierrot	Pierrota	k1gFnPc2	Pierrota
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
režíroval	režírovat	k5eAaImAgMnS	režírovat
několik	několik	k4yIc4	několik
známých	známý	k1gMnPc2	známý
anime	animat	k5eAaPmIp3nS	animat
hitů	hit	k1gInPc2	hit
typu	typ	k1gInSc2	typ
Flame	Flam	k1gInSc5	Flam
of	of	k?	of
Recca	Recca	k1gMnSc1	Recca
<g/>
,	,	kIx,	,
Bleach	Bleach	k1gMnSc1	Bleach
<g/>
,	,	kIx,	,
Yu	Yu	k1gMnSc1	Yu
Yu	Yu	k1gMnSc2	Yu
Hakusho	Hakus	k1gMnSc2	Hakus
<g/>
.	.	kIx.	.
</s>
<s>
Noriyuki	Noriyuki	k6eAd1	Noriyuki
Abe	Abe	k1gMnSc1	Abe
dopomohl	dopomoct	k5eAaPmAgMnS	dopomoct
Yu	Yu	k1gMnSc4	Yu
Yu	Yu	k1gMnSc4	Yu
Hakushu	Hakusha	k1gMnSc4	Hakusha
vyhrát	vyhrát	k5eAaPmF	vyhrát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
a	a	k8xC	a
1994	[number]	k4	1994
cenu	cena	k1gFnSc4	cena
Animage	Animag	k1gInSc2	Animag
Anime	Anim	k1gInSc5	Anim
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc1	Prix
<g/>
.	.	kIx.	.
</s>
<s>
Bleach	Bleach	k1gMnSc1	Bleach
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
DiamondDust	DiamondDust	k1gMnSc1	DiamondDust
Rebellion	Rebellion	k?	Rebellion
:	:	kIx,	:
Režisér	režisér	k1gMnSc1	režisér
Bleach	Bleach	k1gMnSc1	Bleach
<g/>
:	:	kIx,	:
Memories	Memories	k1gMnSc1	Memories
of	of	k?	of
Nobody	Noboda	k1gFnSc2	Noboda
<g/>
:	:	kIx,	:
Režisér	režisér	k1gMnSc1	režisér
Bleach	Bleach	k1gMnSc1	Bleach
(	(	kIx(	(
<g/>
TV	TV	kA	TV
<g/>
)	)	kIx)	)
:	:	kIx,	:
Režisér	režisér	k1gMnSc1	režisér
<g/>
,	,	kIx,	,
Storyboard	Storyboard	k1gMnSc1	Storyboard
<g/>
,	,	kIx,	,
Technický	technický	k2eAgMnSc1d1	technický
ředitel	ředitel	k1gMnSc1	ředitel
Čiisana	Čiisan	k1gMnSc2	Čiisan
kjódžin	kjódžin	k1gMnSc1	kjódžin
Microman	Microman	k1gMnSc1	Microman
(	(	kIx(	(
<g/>
TV	TV	kA	TV
<g/>
)	)	kIx)	)
:	:	kIx,	:
Režisér	režisér	k1gMnSc1	režisér
[	[	kIx(	[
<g/>
seriál	seriál	k1gInSc1	seriál
<g/>
]	]	kIx)	]
Detective	Detectiv	k1gInSc5	Detectiv
School	School	k1gInSc4	School
Q	Q	kA	Q
<g />
.	.	kIx.	.
</s>
<s>
:	:	kIx,	:
Režisér	režisér	k1gMnSc1	režisér
<g/>
,	,	kIx,	,
Storyboard	Storyboard	k1gMnSc1	Storyboard
<g/>
,	,	kIx,	,
Režisér	režisér	k1gMnSc1	režisér
[	[	kIx(	[
<g/>
epizod	epizoda	k1gFnPc2	epizoda
<g/>
]	]	kIx)	]
Flame	Flam	k1gInSc5	Flam
of	of	k?	of
Recca	Recca	k1gMnSc1	Recca
(	(	kIx(	(
<g/>
TV	TV	kA	TV
<g/>
)	)	kIx)	)
:	:	kIx,	:
Režisér	režisér	k1gMnSc1	režisér
<g/>
,	,	kIx,	,
Režisér	režisér	k1gMnSc1	režisér
[	[	kIx(	[
<g/>
epizod	epizoda	k1gFnPc2	epizoda
<g/>
]	]	kIx)	]
Strašidelné	strašidelný	k2eAgFnSc2d1	strašidelná
historky	historka	k1gFnSc2	historka
(	(	kIx(	(
<g/>
TV	TV	kA	TV
<g/>
)	)	kIx)	)
:	:	kIx,	:
Režisér	režisér	k1gMnSc1	režisér
<g/>
,	,	kIx,	,
Zvukový	zvukový	k2eAgMnSc1d1	zvukový
režisér	režisér	k1gMnSc1	režisér
Great	Great	k2eAgMnSc1d1	Great
Teacher	Teachra	k1gFnPc2	Teachra
Onizuka	Onizuk	k1gMnSc2	Onizuk
(	(	kIx(	(
<g/>
TV	TV	kA	TV
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
:	:	kIx,	:
Režisér	režisér	k1gMnSc1	režisér
<g/>
,	,	kIx,	,
Storyboard	Storyboard	k1gMnSc1	Storyboard
Midori	Midor	k1gFnSc2	Midor
no	no	k9	no
makibao	makibao	k1gMnSc1	makibao
(	(	kIx(	(
<g/>
TV	TV	kA	TV
<g/>
)	)	kIx)	)
:	:	kIx,	:
Režisér	režisér	k1gMnSc1	režisér
<g/>
,	,	kIx,	,
Režisér	režisér	k1gMnSc1	režisér
[	[	kIx(	[
<g/>
epizod	epizoda	k1gFnPc2	epizoda
<g/>
]	]	kIx)	]
Ninku	Ninka	k1gFnSc4	Ninka
(	(	kIx(	(
<g/>
TV	TV	kA	TV
<g/>
)	)	kIx)	)
:	:	kIx,	:
Režisér	režisér	k1gMnSc1	režisér
<g/>
,	,	kIx,	,
Storyboard	Storyboard	k1gMnSc1	Storyboard
Ninku	Nink	k1gInSc2	Nink
the	the	k?	the
Movie	Movie	k1gFnSc2	Movie
:	:	kIx,	:
Režisér	režisér	k1gMnSc1	režisér
<g/>
,	,	kIx,	,
Storyboard	Storyboard	k1gMnSc1	Storyboard
Norakuro-kun	Norakuroun	k1gMnSc1	Norakuro-kun
(	(	kIx(	(
<g/>
TV	TV	kA	TV
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
:	:	kIx,	:
Režisér	režisér	k1gMnSc1	režisér
[	[	kIx(	[
<g/>
epizod	epizoda	k1gFnPc2	epizoda
<g/>
]	]	kIx)	]
Ore	Ore	k1gFnSc1	Ore
wa	wa	k?	wa
čokkaku	čokkak	k1gInSc2	čokkak
(	(	kIx(	(
<g/>
TV	TV	kA	TV
<g/>
)	)	kIx)	)
:	:	kIx,	:
Režisér	režisér	k1gMnSc1	režisér
[	[	kIx(	[
<g/>
epizod	epizoda	k1gFnPc2	epizoda
<g/>
]	]	kIx)	]
Saber	Saber	k1gInSc1	Saber
Marionette	Marionett	k1gInSc5	Marionett
J	J	kA	J
to	ten	k3xDgNnSc4	ten
X	X	kA	X
(	(	kIx(	(
<g/>
TV	TV	kA	TV
<g/>
)	)	kIx)	)
:	:	kIx,	:
Storyboard	Storyboard	k1gInSc1	Storyboard
(	(	kIx(	(
<g/>
ep	ep	k?	ep
16	[number]	k4	16
<g/>
)	)	kIx)	)
Seikai	Seikai	k1gNnPc7	Seikai
no	no	k9	no
Senki	Senki	k1gNnSc1	Senki
(	(	kIx(	(
<g/>
seriál	seriál	k1gInSc1	seriál
<g/>
)	)	kIx)	)
:	:	kIx,	:
Storyboard	Storyboard	k1gMnSc1	Storyboard
Tokyo	Tokyo	k1gMnSc1	Tokyo
Mew	Mew	k1gMnSc1	Mew
Mew	Mew	k1gMnSc1	Mew
(	(	kIx(	(
<g/>
TV	TV	kA	TV
<g/>
)	)	kIx)	)
:	:	kIx,	:
Režisér	režisér	k1gMnSc1	režisér
Yu	Yu	k1gMnSc2	Yu
Yu	Yu	k1gMnSc2	Yu
Hakusho	Hakus	k1gMnSc2	Hakus
<g/>
:	:	kIx,	:
Poltergeist	Poltergeist	k1gInSc1	Poltergeist
Report	report	k1gInSc1	report
:	:	kIx,	:
Supervize	supervize	k1gFnSc1	supervize
Yu	Yu	k1gMnSc2	Yu
Yu	Yu	k1gMnSc2	Yu
Hakusho	Hakus	k1gMnSc2	Hakus
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Movie	Movie	k1gFnSc1	Movie
:	:	kIx,	:
Režisér	režisér	k1gMnSc1	režisér
Velká	velká	k1gFnSc1	velká
Zkouška	zkouška	k1gFnSc1	zkouška
(	(	kIx(	(
<g/>
TV	TV	kA	TV
<g/>
)	)	kIx)	)
:	:	kIx,	:
Režisér	režisér	k1gMnSc1	režisér
<g/>
,	,	kIx,	,
Storyboard	Storyboard	k1gMnSc1	Storyboard
<g/>
,	,	kIx,	,
režisér	režisér	k1gMnSc1	režisér
[	[	kIx(	[
<g/>
animace	animace	k1gFnSc1	animace
<g/>
]	]	kIx)	]
</s>
