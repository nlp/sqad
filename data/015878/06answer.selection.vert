<s>
Žíly	žíla	k1gFnPc1
v	v	k7c6
loketní	loketní	k2eAgFnSc6d1
jamce	jamka	k1gFnSc6
(	(	kIx(
<g/>
vena	ven	k2eAgFnSc1d1
cephalica	cephalica	k1gFnSc1
<g/>
)	)	kIx)
slouží	sloužit	k5eAaImIp3nS
většinou	většina	k1gFnSc7
k	k	k7c3
odebírání	odebírání	k1gNnSc3
vzorků	vzorek	k1gInPc2
krve	krev	k1gFnSc2
nebo	nebo	k8xC
jsou	být	k5eAaImIp3nP
používány	používat	k5eAaImNgFnP
při	při	k7c6
intravenózním	intravenózní	k2eAgInSc6d1
(	(	kIx(
<g/>
nitrožilním	nitrožilní	k2eAgInSc6d1
<g/>
)	)	kIx)
podávání	podávání	k1gNnSc3
léků	lék	k1gInPc2
<g/>
.	.	kIx.
</s>