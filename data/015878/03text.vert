<s>
Loket	loket	k1gInSc1
</s>
<s>
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
Loket	loket	k1gInSc1
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Loket	loket	k1gInSc1
</s>
<s>
Loket	loket	k1gInSc1
</s>
<s>
Loket	loket	k1gInSc1
</s>
<s>
Latinsky	latinsky	k6eAd1
</s>
<s>
articulatio	articulatio	k6eAd1
cubiti	cubit	k5eAaPmF,k5eAaBmF,k5eAaImF
</s>
<s>
MeSH	MeSH	k?
</s>
<s>
A	a	k9
<g/>
02.835.583.290	02.835.583.290	k4
</s>
<s>
Gray	Graa	k1gFnPc1
</s>
<s>
321	#num#	k4
</s>
<s>
Lidský	lidský	k2eAgInSc1d1
loket	loket	k1gInSc1
(	(	kIx(
<g/>
articulatio	articulatio	k6eAd1
cubiti	cubit	k5eAaPmF,k5eAaBmF,k5eAaImF
<g/>
,	,	kIx,
cubitus	cubitus	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
oblast	oblast	k1gFnSc1
horní	horní	k2eAgFnSc2d1
končetiny	končetina	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
spojují	spojovat	k5eAaImIp3nP
tři	tři	k4xCgFnPc1
kosti	kost	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kost	kost	k1gFnSc1
pažní	pažní	k2eAgFnSc1d1
(	(	kIx(
<g/>
humerus	humerus	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
kosti	kost	k1gFnPc1
předloktí	předloktí	k1gNnSc2
–	–	k?
kost	kost	k1gFnSc1
vřetenní	vřetenní	k2eAgFnSc1d1
(	(	kIx(
<g/>
radius	radius	k1gInSc1
<g/>
,	,	kIx,
os	osa	k1gFnPc2
radialis	radialis	k1gFnPc2
<g/>
)	)	kIx)
s	s	k7c7
kostí	kost	k1gFnSc7
loketní	loketní	k2eAgInSc4d1
(	(	kIx(
<g/>
ulna	ulna	k1gFnSc1
<g/>
,	,	kIx,
os	osa	k1gFnPc2
ulnaris	ulnaris	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Svaly	sval	k1gInPc1
a	a	k8xC
šlachy	šlacha	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
kryjí	krýt	k5eAaImIp3nP
loketní	loketní	k2eAgInSc4d1
kloub	kloub	k1gInSc4
<g/>
,	,	kIx,
mu	on	k3xPp3gNnSc3
neumožňují	umožňovat	k5eNaImIp3nP
příliš	příliš	k6eAd1
velký	velký	k2eAgInSc4d1
pohyb	pohyb	k1gInSc4
–	–	k?
pouze	pouze	k6eAd1
natažení	natažení	k1gNnSc1
(	(	kIx(
<g/>
extenze	extenze	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
ohnutí	ohnutí	k1gNnSc1
(	(	kIx(
<g/>
flexe	flexe	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Přetížení	přetížení	k1gNnSc1
těchto	tento	k3xDgInPc2
vazů	vaz	k1gInPc2
může	moct	k5eAaImIp3nS
vést	vést	k5eAaImF
k	k	k7c3
velmi	velmi	k6eAd1
známému	známý	k2eAgInSc3d1
a	a	k8xC
často	často	k6eAd1
objevujícímu	objevující	k2eAgNnSc3d1
se	se	k3xPyFc4
onemocnění	onemocnění	k1gNnPc2
–	–	k?
tenisový	tenisový	k2eAgInSc4d1
loket	loket	k1gInSc4
(	(	kIx(
<g/>
entezopatie	entezopatie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Žíly	žíla	k1gFnPc1
v	v	k7c6
loketní	loketní	k2eAgFnSc6d1
jamce	jamka	k1gFnSc6
(	(	kIx(
<g/>
vena	ven	k2eAgFnSc1d1
cephalica	cephalica	k1gFnSc1
<g/>
)	)	kIx)
slouží	sloužit	k5eAaImIp3nS
většinou	většina	k1gFnSc7
k	k	k7c3
odebírání	odebírání	k1gNnSc3
vzorků	vzorek	k1gInPc2
krve	krev	k1gFnSc2
nebo	nebo	k8xC
jsou	být	k5eAaImIp3nP
používány	používat	k5eAaImNgFnP
při	při	k7c6
intravenózním	intravenózní	k2eAgInSc6d1
(	(	kIx(
<g/>
nitrožilním	nitrožilní	k2eAgInSc6d1
<g/>
)	)	kIx)
podávání	podávání	k1gNnSc3
léků	lék	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
kloubní	kloubní	k2eAgFnSc6d1
jamce	jamka	k1gFnSc6
se	se	k3xPyFc4
také	také	k9
přikládá	přikládat	k5eAaImIp3nS
stetoskop	stetoskop	k1gInSc1
při	při	k7c6
měření	měření	k1gNnSc6
krevního	krevní	k2eAgInSc2d1
tlaku	tlak	k1gInSc2
–	–	k?
dochází	docházet	k5eAaImIp3nS
zde	zde	k6eAd1
ke	k	k7c3
zdvojení	zdvojení	k1gNnSc3
tepen	tepna	k1gFnPc2
–	–	k?
bifurkaci	bifurkace	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Velmi	velmi	k6eAd1
často	často	k6eAd1
také	také	k9
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
vyvrknutí	vyvrknutý	k2eAgMnPc1d1
(	(	kIx(
<g/>
distorse	distorse	k1gFnSc2
<g/>
)	)	kIx)
nebo	nebo	k8xC
vykloubení	vykloubení	k1gNnSc1
(	(	kIx(
<g/>
luxace	luxace	k1gFnSc1
<g/>
)	)	kIx)
loketního	loketní	k2eAgInSc2d1
kloubu	kloub	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
luxaci	luxace	k1gFnSc6
se	se	k3xPyFc4
následně	následně	k6eAd1
musí	muset	k5eAaImIp3nS
provést	provést	k5eAaPmF
napravení	napravení	k1gNnSc1
–	–	k?
vrácení	vrácení	k1gNnSc1
kloubu	kloub	k1gInSc2
do	do	k7c2
kloubní	kloubní	k2eAgFnSc2d1
jamky	jamka	k1gFnSc2
(	(	kIx(
<g/>
repozice	repozice	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Soustava	soustava	k1gFnSc1
kostí	kost	k1gFnPc2
kloubu	kloub	k1gInSc2
loketního	loketní	k2eAgInSc2d1
(	(	kIx(
<g/>
articulatio	articulatio	k6eAd1
cubiti	cubit	k5eAaPmF,k5eAaBmF,k5eAaImF
<g/>
)	)	kIx)
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
lze	lze	k6eAd1
také	také	k9
nazvat	nazvat	k5eAaPmF,k5eAaBmF
jako	jako	k8xC,k8xS
(	(	kIx(
<g/>
articulatio	articulatio	k1gNnSc1
humeroradialis	humeroradialis	k1gFnSc2
<g/>
,	,	kIx,
humeroulnaris	humeroulnaris	k1gFnSc2
a	a	k8xC
radioulnaris	radioulnaris	k1gFnSc2
proximalis	proximalis	k1gFnSc2
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Loketní	loketní	k2eAgInSc4d1
kloub	kloub	k1gInSc4
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Brňavka	brňavka	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
loket	loket	k1gInSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Lidské	lidský	k2eAgInPc1d1
anatomické	anatomický	k2eAgInPc1d1
rysy	rys	k1gInPc1
Hlava	hlava	k1gFnSc1
</s>
<s>
lebka	lebka	k1gFnSc1
•	•	k?
obličej	obličej	k1gInSc1
•	•	k?
čelo	čelo	k1gNnSc1
•	•	k?
oko	oko	k1gNnSc1
•	•	k?
ucho	ucho	k1gNnSc4
•	•	k?
nos	nos	k1gInSc1
•	•	k?
ústa	ústa	k1gNnPc4
•	•	k?
jazyk	jazyk	k1gInSc4
•	•	k?
zuby	zub	k1gInPc4
•	•	k?
čelist	čelist	k1gFnSc1
•	•	k?
tvář	tvář	k1gFnSc1
•	•	k?
brada	brada	k1gFnSc1
Krk	krk	k1gInSc1
</s>
<s>
ohryzek	ohryzek	k1gInSc1
Trup	trupa	k1gFnPc2
</s>
<s>
rameno	rameno	k1gNnSc4
•	•	k?
páteř	páteř	k1gFnSc1
•	•	k?
prs	prs	k1gInSc1
(	(	kIx(
<g/>
bradavka	bradavka	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
hrudník	hrudník	k1gInSc1
•	•	k?
hrudní	hrudní	k2eAgInSc1d1
koš	koš	k1gInSc1
•	•	k?
břicho	břicho	k1gNnSc4
•	•	k?
pupek	pupek	k1gInSc1
•	•	k?
záda	záda	k1gNnPc4
•	•	k?
boky	bok	k1gInPc4
</s>
<s>
pohlavní	pohlavní	k2eAgInPc1d1
orgány	orgán	k1gInPc1
(	(	kIx(
<g/>
klitoris	klitoris	k1gFnSc1
•	•	k?
pochva	pochva	k1gFnSc1
•	•	k?
stydké	stydký	k2eAgInPc1d1
pysky	pysk	k1gInPc1
•	•	k?
penis	penis	k1gInSc1
•	•	k?
šourek	šourek	k1gInSc1
•	•	k?
varle	varle	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
kyčelní	kyčelní	k2eAgInSc4d1
kloub	kloub	k1gInSc4
•	•	k?
konečník	konečník	k1gInSc1
•	•	k?
hýždě	hýždě	k1gFnSc2
Končetiny	končetina	k1gFnSc2
</s>
<s>
Horní	horní	k2eAgFnSc1d1
končetina	končetina	k1gFnSc1
</s>
<s>
paže	paže	k1gFnSc1
•	•	k?
loket	loket	k1gInSc4
•	•	k?
předloktí	předloktí	k1gNnSc2
•	•	k?
zápěstí	zápěstí	k1gNnSc2
•	•	k?
ruka	ruka	k1gFnSc1
•	•	k?
prst	prst	k1gInSc1
(	(	kIx(
<g/>
palec	palec	k1gInSc1
•	•	k?
ukazovák	ukazovák	k1gInSc1
•	•	k?
prostředník	prostředník	k1gInSc1
•	•	k?
prsteník	prsteník	k1gInSc1
•	•	k?
malíček	malíček	k1gInSc1
<g/>
)	)	kIx)
Dolní	dolní	k2eAgFnSc1d1
končetina	končetina	k1gFnSc1
</s>
<s>
noha	noha	k1gFnSc1
•	•	k?
stehno	stehno	k1gNnSc1
•	•	k?
koleno	koleno	k1gNnSc1
•	•	k?
lýtko	lýtko	k1gNnSc1
•	•	k?
pata	pata	k1gFnSc1
•	•	k?
kotník	kotník	k1gInSc4
•	•	k?
chodidlo	chodidlo	k1gNnSc4
•	•	k?
prstec	prstec	k1gInSc1
</s>
<s>
Kůže	kůže	k1gFnSc1
</s>
<s>
vlasy	vlas	k1gInPc1
<g/>
,	,	kIx,
ochlupení	ochlupení	k1gNnPc1
<g/>
,	,	kIx,
nehty	nehet	k1gInPc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4014462-8	4014462-8	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85041533	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85041533	#num#	k4
</s>
<s>
↑	↑	k?
FRYDRÝŠEK	FRYDRÝŠEK	kA
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Biomechanika	biomechanika	k1gFnSc1
1	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostrava	Ostrava	k1gFnSc1
<g/>
:	:	kIx,
VSB	VSB	kA
–	–	k?
Technical	Technical	k1gMnSc1
University	universita	k1gFnSc2
of	of	k?
Ostrava	Ostrava	k1gFnSc1
<g/>
,	,	kIx,
Faculty	Facult	k1gInPc1
of	of	k?
Mechanical	Mechanical	k1gFnPc2
Engineering	Engineering	k1gInSc1
<g/>
,	,	kIx,
Department	department	k1gInSc1
of	of	k?
Applied	Applied	k1gInSc1
Mechanics	Mechanicsa	k1gFnPc2
<g/>
,	,	kIx,
2019	#num#	k4
<g/>
.	.	kIx.
461	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
248	#num#	k4
<g/>
-	-	kIx~
<g/>
4263	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
