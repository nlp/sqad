<s>
Yttrium	yttrium	k1gNnSc1
</s>
<s>
Yttrium	yttrium	k1gNnSc1
</s>
<s>
[	[	kIx(
<g/>
Kr	Kr	k1gMnSc6
<g/>
]	]	kIx)
4	#num#	k4
<g/>
d	d	k?
<g/>
1	#num#	k4
5	#num#	k4
<g/>
s	s	k7c7
<g/>
2	#num#	k4
</s>
<s>
89	#num#	k4
</s>
<s>
Y	Y	kA
</s>
<s>
39	#num#	k4
</s>
<s>
↓	↓	k?
Periodická	periodický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
↓	↓	k?
</s>
<s>
Obecné	obecný	k2eAgNnSc1d1
</s>
<s>
Název	název	k1gInSc1
<g/>
,	,	kIx,
značka	značka	k1gFnSc1
<g/>
,	,	kIx,
číslo	číslo	k1gNnSc1
</s>
<s>
Yttrium	yttrium	k1gNnSc1
<g/>
,	,	kIx,
Y	Y	kA
<g/>
,	,	kIx,
39	#num#	k4
</s>
<s>
Cizojazyčné	cizojazyčný	k2eAgInPc1d1
názvy	název	k1gInPc1
</s>
<s>
lat.	lat.	k?
Yttrium	yttrium	k1gNnSc1
</s>
<s>
Skupina	skupina	k1gFnSc1
<g/>
,	,	kIx,
perioda	perioda	k1gFnSc1
<g/>
,	,	kIx,
blok	blok	k1gInSc1
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
skupina	skupina	k1gFnSc1
<g/>
,	,	kIx,
5	#num#	k4
<g/>
.	.	kIx.
perioda	perioda	k1gFnSc1
<g/>
,	,	kIx,
blok	blok	k1gInSc1
d	d	k?
</s>
<s>
Chemická	chemický	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
</s>
<s>
Přechodné	přechodný	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Koncentrace	koncentrace	k1gFnPc1
v	v	k7c6
zemské	zemský	k2eAgFnSc6d1
kůře	kůra	k1gFnSc6
</s>
<s>
28,1	28,1	k4
až	až	k6eAd1
34	#num#	k4
ppm	ppm	k?
</s>
<s>
Koncentrace	koncentrace	k1gFnPc1
v	v	k7c6
mořské	mořský	k2eAgFnSc6d1
vodě	voda	k1gFnSc6
</s>
<s>
0,0003	0,0003	k4
mg	mg	kA
<g/>
/	/	kIx~
<g/>
l	l	kA
</s>
<s>
Vzhled	vzhled	k1gInSc1
</s>
<s>
Šedý	Šedý	k1gMnSc1
až	až	k9
stříbřitě	stříbřitě	k6eAd1
bílý	bílý	k2eAgInSc1d1
přechodný	přechodný	k2eAgInSc1d1
kov	kov	k1gInSc1
</s>
<s>
Identifikace	identifikace	k1gFnSc1
</s>
<s>
Registrační	registrační	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
CAS	CAS	kA
</s>
<s>
7440-65-6	7440-65-6	k4
</s>
<s>
Atomové	atomový	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Relativní	relativní	k2eAgFnSc1d1
atomová	atomový	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
</s>
<s>
88,905	88,905	k4
<g/>
85	#num#	k4
</s>
<s>
Atomový	atomový	k2eAgInSc4d1
poloměr	poloměr	k1gInSc4
</s>
<s>
180	#num#	k4
pm	pm	k?
</s>
<s>
Kovalentní	kovalentní	k2eAgInSc4d1
poloměr	poloměr	k1gInSc4
</s>
<s>
190	#num#	k4
pm	pm	k?
</s>
<s>
Iontový	iontový	k2eAgInSc4d1
poloměr	poloměr	k1gInSc4
</s>
<s>
92	#num#	k4
pm	pm	k?
</s>
<s>
Elektronová	elektronový	k2eAgFnSc1d1
konfigurace	konfigurace	k1gFnSc1
</s>
<s>
[	[	kIx(
<g/>
Kr	Kr	k1gMnSc6
<g/>
]	]	kIx)
4	#num#	k4
<g/>
d	d	k?
<g/>
1	#num#	k4
5	#num#	k4
<g/>
s	s	k7c7
<g/>
2	#num#	k4
</s>
<s>
Oxidační	oxidační	k2eAgNnPc1d1
čísla	číslo	k1gNnPc1
</s>
<s>
I	I	kA
<g/>
,	,	kIx,
II	II	kA
<g/>
,	,	kIx,
III	III	kA
</s>
<s>
Elektronegativita	Elektronegativita	k1gFnSc1
(	(	kIx(
<g/>
Paulingova	Paulingův	k2eAgFnSc1d1
stupnice	stupnice	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
1,22	1,22	k4
</s>
<s>
Ionizační	ionizační	k2eAgFnSc1d1
energie	energie	k1gFnSc1
</s>
<s>
První	první	k4xOgFnSc1
</s>
<s>
600	#num#	k4
KJ	KJ	kA
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
</s>
<s>
Druhá	druhý	k4xOgFnSc1
</s>
<s>
1180	#num#	k4
KJ	KJ	kA
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
</s>
<s>
Třetí	třetí	k4xOgFnSc1
</s>
<s>
1980	#num#	k4
KJ	KJ	kA
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
</s>
<s>
Látkové	látkový	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Krystalografická	krystalografický	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
</s>
<s>
Šesterečná	šesterečný	k2eAgFnSc1d1
</s>
<s>
Molární	molární	k2eAgInSc1d1
objem	objem	k1gInSc1
</s>
<s>
19,88	19,88	k4
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
6	#num#	k4
m	m	kA
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
mol	mol	k1gInSc1
</s>
<s>
Mechanické	mechanický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Hustota	hustota	k1gFnSc1
</s>
<s>
4,472	4,472	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
cm	cm	kA
<g/>
3	#num#	k4
</s>
<s>
Skupenství	skupenství	k1gNnSc1
</s>
<s>
Pevné	pevný	k2eAgNnSc1d1
</s>
<s>
Tlak	tlak	k1gInSc1
syté	sytý	k2eAgFnSc2d1
páry	pára	k1gFnSc2
</s>
<s>
100	#num#	k4
Pa	Pa	kA
při	při	k7c6
2320K	2320K	k4
</s>
<s>
Rychlost	rychlost	k1gFnSc1
zvuku	zvuk	k1gInSc2
</s>
<s>
3300	#num#	k4
m	m	kA
<g/>
/	/	kIx~
<g/>
s	s	k7c7
</s>
<s>
Termické	termický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Tepelná	tepelný	k2eAgFnSc1d1
vodivost	vodivost	k1gFnSc1
</s>
<s>
17,2	17,2	k4
W	W	kA
<g/>
⋅	⋅	k?
<g/>
m	m	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
<g/>
⋅	⋅	k?
<g/>
K	K	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
</s>
<s>
Termodynamické	termodynamický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Teplota	teplota	k1gFnSc1
tání	tání	k1gNnSc2
</s>
<s>
1525,85	1525,85	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
1	#num#	k4
799	#num#	k4
K	k	k7c3
<g/>
)	)	kIx)
</s>
<s>
Teplota	teplota	k1gFnSc1
varu	var	k1gInSc2
</s>
<s>
3335,85	3335,85	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
3	#num#	k4
609	#num#	k4
K	k	k7c3
<g/>
)	)	kIx)
</s>
<s>
Skupenské	skupenský	k2eAgNnSc1d1
teplo	teplo	k1gNnSc1
tání	tání	k1gNnSc2
</s>
<s>
11,42	11,42	k4
KJ	KJ	kA
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
</s>
<s>
Skupenské	skupenský	k2eAgNnSc1d1
teplo	teplo	k1gNnSc1
varu	var	k1gInSc2
</s>
<s>
365	#num#	k4
KJ	KJ	kA
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
</s>
<s>
Měrná	měrný	k2eAgFnSc1d1
tepelná	tepelný	k2eAgFnSc1d1
kapacita	kapacita	k1gFnSc1
</s>
<s>
26,53	26,53	k4
Jmol	Jmol	k1gInSc1
<g/>
−	−	k?
<g/>
1	#num#	k4
<g/>
K	K	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
</s>
<s>
Elektromagnetické	elektromagnetický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Elektrická	elektrický	k2eAgFnSc1d1
vodivost	vodivost	k1gFnSc1
</s>
<s>
1,66	1,66	k4
<g/>
×	×	k?
<g/>
106	#num#	k4
S	s	k7c7
<g/>
/	/	kIx~
<g/>
m	m	kA
</s>
<s>
Měrný	měrný	k2eAgInSc4d1
elektrický	elektrický	k2eAgInSc4d1
odpor	odpor	k1gInSc4
</s>
<s>
596	#num#	k4
nΩ	nΩ	k?
<g/>
·	·	k?
<g/>
m	m	kA
</s>
<s>
Standardní	standardní	k2eAgInSc1d1
elektrodový	elektrodový	k2eAgInSc1d1
potenciál	potenciál	k1gInSc1
</s>
<s>
-2,37	-2,37	k4
V	v	k7c6
</s>
<s>
Magnetické	magnetický	k2eAgNnSc1d1
chování	chování	k1gNnSc1
</s>
<s>
Paramagnetický	paramagnetický	k2eAgInSc1d1
</s>
<s>
Bezpečnost	bezpečnost	k1gFnSc1
</s>
<s>
R-věty	R-věta	k1gFnPc1
</s>
<s>
R11	R11	k4
</s>
<s>
S-věty	S-věta	k1gFnPc1
</s>
<s>
S	s	k7c7
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
9	#num#	k4
<g/>
,	,	kIx,
S	s	k7c7
<g/>
16	#num#	k4
<g/>
,	,	kIx,
S33	S33	k1gFnSc1
</s>
<s>
Izotopy	izotop	k1gInPc1
</s>
<s>
I	i	k9
</s>
<s>
V	v	k7c6
(	(	kIx(
<g/>
%	%	kIx~
<g/>
)	)	kIx)
</s>
<s>
S	s	k7c7
</s>
<s>
T	T	kA
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
E	E	kA
(	(	kIx(
<g/>
MeV	MeV	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
P	P	kA
</s>
<s>
87Y	87Y	k4
</s>
<s>
umělý	umělý	k2eAgInSc4d1
</s>
<s>
3,35	3,35	k4
dne	den	k1gInSc2
</s>
<s>
ε	ε	k?
</s>
<s>
-	-	kIx~
</s>
<s>
87	#num#	k4
<g/>
Sr	Sr	k1gFnSc1
</s>
<s>
γ	γ	k?
</s>
<s>
0,48	0,48	k4
</s>
<s>
87	#num#	k4
<g/>
Sr	Sr	k1gFnSc1
</s>
<s>
88Y	88Y	k4
</s>
<s>
umělý	umělý	k2eAgInSc4d1
</s>
<s>
106,6	106,6	k4
dne	den	k1gInSc2
</s>
<s>
ε	ε	k?
</s>
<s>
-	-	kIx~
</s>
<s>
88	#num#	k4
<g/>
Sr	Sr	k1gFnSc1
</s>
<s>
γ	γ	k?
</s>
<s>
1,83	1,83	k4
</s>
<s>
88	#num#	k4
<g/>
Sr	Sr	k1gFnSc1
</s>
<s>
89Y	89Y	k4
</s>
<s>
100	#num#	k4
<g/>
%	%	kIx~
</s>
<s>
je	být	k5eAaImIp3nS
stabilní	stabilní	k2eAgMnSc1d1
s	s	k7c7
50	#num#	k4
neutrony	neutron	k1gInPc7
</s>
<s>
90Y	90Y	k4
</s>
<s>
umělý	umělý	k2eAgInSc4d1
</s>
<s>
2,67	2,67	k4
dne	den	k1gInSc2
</s>
<s>
β	β	k?
<g/>
−	−	k?
</s>
<s>
2,28	2,28	k4
</s>
<s>
90	#num#	k4
<g/>
Zr	Zr	k1gFnSc1
</s>
<s>
γ	γ	k?
</s>
<s>
2,18	2,18	k4
</s>
<s>
90	#num#	k4
<g/>
Zr	Zr	k1gFnSc1
</s>
<s>
91Y	91Y	k4
</s>
<s>
umělý	umělý	k2eAgInSc4d1
</s>
<s>
58,5	58,5	k4
dne	den	k1gInSc2
</s>
<s>
β	β	k?
<g/>
−	−	k?
</s>
<s>
1,54	1,54	k4
</s>
<s>
91	#num#	k4
<g/>
Zr	Zr	k1gFnSc1
</s>
<s>
γ	γ	k?
</s>
<s>
1,20	1,20	k4
</s>
<s>
91	#num#	k4
<g/>
Zr	Zr	k1gFnSc1
</s>
<s>
Není	být	k5eNaImIp3nS
<g/>
-li	-li	k?
uvedeno	uvést	k5eAaPmNgNnS
jinak	jinak	k6eAd1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
použityjednotky	použityjednotka	k1gFnPc1
SI	si	k1gNnSc2
a	a	k8xC
STP	STP	kA
(	(	kIx(
<g/>
25	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
100	#num#	k4
kPa	kPa	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Sc	Sc	k?
<g/>
⋏	⋏	k?
</s>
<s>
Stroncium	stroncium	k1gNnSc1
≺	≺	k?
<g/>
Y	Y	kA
<g/>
≻	≻	k?
Zirkonium	zirkonium	k1gNnSc1
</s>
<s>
⋎	⋎	k?
<g/>
La	la	k1gNnSc1
</s>
<s>
Yttrium	yttrium	k1gNnSc1
(	(	kIx(
<g/>
chemická	chemický	k2eAgFnSc1d1
značka	značka	k1gFnSc1
Y	Y	kA
<g/>
,	,	kIx,
latinsky	latinsky	k6eAd1
Yttrium	yttrium	k1gNnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
šedý	šedý	k2eAgInSc1d1
až	až	k9
stříbřitě	stříbřitě	k6eAd1
bílý	bílý	k2eAgInSc1d1
<g/>
,	,	kIx,
přechodný	přechodný	k2eAgInSc1d1
kovový	kovový	k2eAgInSc1d1
prvek	prvek	k1gInSc1
<g/>
,	,	kIx,
chemicky	chemicky	k6eAd1
silně	silně	k6eAd1
příbuzný	příbuzný	k2eAgMnSc1d1
prvkům	prvek	k1gInPc3
skupiny	skupina	k1gFnSc2
lanthanoidů	lanthanoid	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgInSc4d1
uplatnění	uplatnění	k1gNnSc1
nalézá	nalézat	k5eAaImIp3nS
ve	v	k7c6
výrobě	výroba	k1gFnSc6
barevných	barevný	k2eAgFnPc2d1
televizních	televizní	k2eAgFnPc2d1
obrazovek	obrazovka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Základní	základní	k2eAgFnPc1d1
fyzikálně-chemické	fyzikálně-chemický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Yttrium	yttrium	k1gNnSc1
je	být	k5eAaImIp3nS
stříbřitě	stříbřitě	k6eAd1
bílý	bílý	k2eAgInSc1d1
<g/>
,	,	kIx,
středně	středně	k6eAd1
tvrdý	tvrdý	k2eAgMnSc1d1
<g/>
,	,	kIx,
poměrně	poměrně	k6eAd1
vzácný	vzácný	k2eAgInSc4d1
přechodný	přechodný	k2eAgInSc4d1
kov	kov	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Vůči	vůči	k7c3
působení	působení	k1gNnSc3
vzdušného	vzdušný	k2eAgInSc2d1
kyslíku	kyslík	k1gInSc2
je	být	k5eAaImIp3nS
poměrně	poměrně	k6eAd1
stálé	stálý	k2eAgNnSc1d1
<g/>
,	,	kIx,
pouze	pouze	k6eAd1
v	v	k7c6
práškovité	práškovitý	k2eAgFnSc6d1
formě	forma	k1gFnSc6
podléhá	podléhat	k5eAaImIp3nS
za	za	k7c2
vyšších	vysoký	k2eAgFnPc2d2
teplot	teplota	k1gFnPc2
spontánní	spontánní	k2eAgFnSc4d1
oxidaci	oxidace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odolává	odolávat	k5eAaImIp3nS
i	i	k9
působení	působení	k1gNnSc1
vody	voda	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
snadno	snadno	k6eAd1
se	se	k3xPyFc4
rozpouští	rozpouštět	k5eAaImIp3nS
ve	v	k7c6
zředěných	zředěný	k2eAgFnPc6d1
minerálních	minerální	k2eAgFnPc6d1
kyselinách	kyselina	k1gFnPc6
<g/>
,	,	kIx,
především	především	k6eAd1
v	v	k7c6
kyselině	kyselina	k1gFnSc6
chlorovodíkové	chlorovodíkový	k2eAgFnSc6d1
(	(	kIx(
<g/>
HCl	HCl	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
sloučeninách	sloučenina	k1gFnPc6
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
prakticky	prakticky	k6eAd1
pouze	pouze	k6eAd1
v	v	k7c6
mocenství	mocenství	k1gNnSc6
Y	Y	kA
<g/>
3	#num#	k4
<g/>
+	+	kIx~
<g/>
.	.	kIx.
</s>
<s>
Bylo	být	k5eAaImAgNnS
objeveno	objevit	k5eAaPmNgNnS
v	v	k7c6
roce	rok	k1gInSc6
1794	#num#	k4
finským	finský	k2eAgMnSc7d1
chemikem	chemik	k1gMnSc7
Johanem	Johan	k1gMnSc7
Gadolinem	Gadolin	k1gInSc7
a	a	k8xC
poprvé	poprvé	k6eAd1
bylo	být	k5eAaImAgNnS
v	v	k7c6
čisté	čistý	k2eAgFnSc6d1
formě	forma	k1gFnSc6
izolováno	izolován	k2eAgNnSc1d1
Friedrichem	Friedrich	k1gMnSc7
Wöhlerem	Wöhler	k1gMnSc7
roku	rok	k1gInSc2
1828	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Název	název	k1gInSc1
získalo	získat	k5eAaPmAgNnS
podle	podle	k7c2
obce	obec	k1gFnSc2
Ytterby	Ytterba	k1gFnSc2
u	u	k7c2
Stockholmu	Stockholm	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
geolog	geolog	k1gMnSc1
Carl	Carl	k1gMnSc1
Axel	Axel	k1gMnSc1
Arrhennius	Arrhennius	k1gMnSc1
nalezl	nalézt	k5eAaBmAgMnS,k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1787	#num#	k4
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
neznámý	známý	k2eNgInSc4d1
nerost	nerost	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
dal	dát	k5eAaPmAgMnS
Gadolinovi	Gadolin	k1gMnSc3
k	k	k7c3
prozkoumání	prozkoumání	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obdobně	obdobně	k6eAd1
dostalo	dostat	k5eAaPmAgNnS
název	název	k1gInSc4
i	i	k9
ytterbium	ytterbium	k1gNnSc4
<g/>
,	,	kIx,
terbium	terbium	k1gNnSc4
a	a	k8xC
erbium	erbium	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Výskyt	výskyt	k1gInSc1
a	a	k8xC
výroba	výroba	k1gFnSc1
</s>
<s>
Yttrium	yttrium	k1gNnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
zemské	zemský	k2eAgFnSc6d1
kůře	kůra	k1gFnSc6
obsaženo	obsáhnout	k5eAaPmNgNnS
v	v	k7c6
množství	množství	k1gNnSc6
přibližně	přibližně	k6eAd1
28	#num#	k4
<g/>
–	–	k?
<g/>
40	#num#	k4
mg	mg	kA
<g/>
/	/	kIx~
<g/>
kg	kg	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
mořské	mořský	k2eAgFnSc6d1
vodě	voda	k1gFnSc6
je	být	k5eAaImIp3nS
jeho	jeho	k3xOp3gFnSc1
koncentrace	koncentrace	k1gFnSc1
kolem	kolem	k7c2
0,000	0,000	k4
3	#num#	k4
mg	mg	kA
<g/>
/	/	kIx~
<g/>
l.	l.	k?
Ve	v	k7c6
vesmíru	vesmír	k1gInSc6
připadá	připadat	k5eAaPmIp3nS,k5eAaImIp3nS
jeden	jeden	k4xCgInSc4
atom	atom	k1gInSc4
yttria	yttrium	k1gNnSc2
na	na	k7c4
10	#num#	k4
miliard	miliarda	k4xCgFnPc2
atomů	atom	k1gInPc2
vodíku	vodík	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
zemské	zemský	k2eAgFnSc6d1
kůře	kůra	k1gFnSc6
se	se	k3xPyFc4
čisté	čistý	k2eAgNnSc1d1
yttrium	yttrium	k1gNnSc1
nenachází	nacházet	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyskytuje	vyskytovat	k5eAaImIp3nS
se	se	k3xPyFc4
pouze	pouze	k6eAd1
ve	v	k7c6
formě	forma	k1gFnSc6
sloučenin	sloučenina	k1gFnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
vždy	vždy	k6eAd1
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
směsné	směsný	k2eAgInPc4d1
minerály	minerál	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
obsahují	obsahovat	k5eAaImIp3nP
lanthanoidy	lanthanoida	k1gFnPc4
a	a	k8xC
v	v	k7c6
některých	některý	k3yIgInPc6
případech	případ	k1gInPc6
je	být	k5eAaImIp3nS
yttrium	yttrium	k1gNnSc1
přítomno	přítomen	k2eAgNnSc1d1
v	v	k7c6
uranových	uranový	k2eAgFnPc6d1
rudách	ruda	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejznámějšími	známý	k2eAgFnPc7d3
průmyslově	průmyslově	k6eAd1
využívanými	využívaný	k2eAgFnPc7d1
surovinami	surovina	k1gFnPc7
jsou	být	k5eAaImIp3nP
monazitové	monazitový	k2eAgInPc4d1
písky	písek	k1gInPc4
<g/>
,	,	kIx,
v	v	k7c6
nichž	jenž	k3xRgFnPc6
převládají	převládat	k5eAaImIp3nP
fosforečnany	fosforečnan	k1gInPc1
ceru	cer	k1gInSc2
a	a	k8xC
lanthanu	lanthan	k1gInSc2
a	a	k8xC
bastnäsity	bastnäsit	k1gInPc1
–	–	k?
směsné	směsný	k2eAgInPc1d1
fluorouhličitany	fluorouhličitan	k1gInPc1
prvků	prvek	k1gInPc2
vzácných	vzácný	k2eAgFnPc2d1
zemin	zemina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Velká	velký	k2eAgNnPc1d1
ložiska	ložisko	k1gNnPc1
těchto	tento	k3xDgFnPc2
rud	ruda	k1gFnPc2
se	se	k3xPyFc4
nalézají	nalézat	k5eAaImIp3nP
v	v	k7c6
USA	USA	kA
<g/>
,	,	kIx,
Číně	Čína	k1gFnSc6
a	a	k8xC
Vietnamu	Vietnam	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Významným	významný	k2eAgInSc7d1
zdrojem	zdroj	k1gInSc7
jsou	být	k5eAaImIp3nP
i	i	k9
fosfátové	fosfátový	k2eAgFnPc1d1
suroviny	surovina	k1gFnPc1
–	–	k?
apatity	apatit	k1gInPc1
z	z	k7c2
poloostrova	poloostrov	k1gInSc2
Kola	kolo	k1gNnSc2
v	v	k7c6
Rusku	Rusko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
byl	být	k5eAaImAgInS
ohlášen	ohlásit	k5eAaPmNgInS,k5eAaImNgInS
nález	nález	k1gInSc1
ložiska	ložisko	k1gNnSc2
bohatého	bohatý	k2eAgNnSc2d1
na	na	k7c6
yttrium	yttrium	k1gNnSc4
<g/>
,	,	kIx,
dysprosium	dysprosium	k1gNnSc4
<g/>
,	,	kIx,
europium	europium	k1gNnSc4
a	a	k8xC
terbium	terbium	k1gNnSc4
poblíž	poblíž	k7c2
japonského	japonský	k2eAgInSc2d1
ostrůvku	ostrůvek	k1gInSc2
Minami	mina	k1gFnPc7
Torišima	Torišim	k1gMnSc4
(	(	kIx(
<g/>
asi	asi	k9
1	#num#	k4
850	#num#	k4
km	km	kA
jihovýchodně	jihovýchodně	k6eAd1
od	od	k7c2
Tokia	Tokio	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vzhledem	vzhledem	k7c3
k	k	k7c3
omezené	omezený	k2eAgFnSc3d1
dostupnosti	dostupnost	k1gFnSc3
hrozil	hrozit	k5eAaImAgInS
v	v	k7c6
nejbližších	blízký	k2eAgInPc6d3
letech	let	k1gInPc6
kritický	kritický	k2eAgInSc1d1
nedostatek	nedostatek	k1gInSc1
zdrojů	zdroj	k1gInPc2
prvku	prvek	k1gInSc2
pro	pro	k7c4
technologické	technologický	k2eAgNnSc4d1
využití	využití	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Výše	vysoce	k6eAd2
uvedený	uvedený	k2eAgInSc1d1
nález	nález	k1gInSc1
by	by	kYmCp3nS
mohl	moct	k5eAaImAgInS
tuto	tento	k3xDgFnSc4
situaci	situace	k1gFnSc4
změnit	změnit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Průmyslová	průmyslový	k2eAgFnSc1d1
výroba	výroba	k1gFnSc1
yttria	yttrium	k1gNnSc2
vychází	vycházet	k5eAaImIp3nS
obvykle	obvykle	k6eAd1
z	z	k7c2
lanthanoidových	lanthanoidový	k2eAgFnPc2d1
rud	ruda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hornina	hornina	k1gFnSc1
se	se	k3xPyFc4
louží	loužit	k5eAaImIp3nS
směsí	směs	k1gFnSc7
kyseliny	kyselina	k1gFnSc2
sírové	sírový	k2eAgFnSc2d1
a	a	k8xC
chlorovodíkové	chlorovodíkový	k2eAgFnSc2d1
a	a	k8xC
ze	z	k7c2
vzniklého	vzniklý	k2eAgInSc2d1
roztoku	roztok	k1gInSc2
solí	solit	k5eAaImIp3nS
se	s	k7c7
přídavkem	přídavek	k1gInSc7
hydroxidu	hydroxid	k1gInSc2
sodného	sodný	k2eAgInSc2d1
vysráží	vysrážet	k5eAaPmIp3nP
hydroxidy	hydroxid	k1gInPc7
yttria	yttrium	k1gNnSc2
a	a	k8xC
lanthanoidů	lanthanoid	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Separace	separace	k1gFnSc1
jednotlivých	jednotlivý	k2eAgInPc2d1
prvků	prvek	k1gInPc2
se	se	k3xPyFc4
provádí	provádět	k5eAaImIp3nS
řadou	řada	k1gFnSc7
různých	různý	k2eAgInPc2d1
postupů	postup	k1gInPc2
–	–	k?
kapalinovou	kapalinový	k2eAgFnSc7d1
extrakcí	extrakce	k1gFnSc7
komplexních	komplexní	k2eAgFnPc2d1
solí	sůl	k1gFnPc2
<g/>
,	,	kIx,
ionexovou	ionexový	k2eAgFnSc7d1
chromatografií	chromatografie	k1gFnSc7
nebo	nebo	k8xC
selektivním	selektivní	k2eAgNnSc7d1
srážením	srážení	k1gNnSc7
nerozpustných	rozpustný	k2eNgFnPc2d1
komplexních	komplexní	k2eAgFnPc2d1
solí	sůl	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Příprava	příprava	k1gFnSc1
čistého	čistý	k2eAgInSc2d1
kovu	kov	k1gInSc2
obvykle	obvykle	k6eAd1
probíhá	probíhat	k5eAaImIp3nS
redukcí	redukce	k1gFnSc7
solí	sůl	k1gFnPc2
yttria	yttrium	k1gNnPc1
vápníkem	vápník	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Redukci	redukce	k1gFnSc4
fluoridu	fluorid	k1gInSc2
yttritého	yttritý	k2eAgInSc2d1
popisuje	popisovat	k5eAaImIp3nS
rovnice	rovnice	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
2	#num#	k4
YF3	YF3	k1gFnSc1
+	+	kIx~
3	#num#	k4
Ca	ca	kA
→	→	k?
2	#num#	k4
Y	Y	kA
+	+	kIx~
3	#num#	k4
CaF	CaF	k1gFnPc2
<g/>
2	#num#	k4
</s>
<s>
Použití	použití	k1gNnSc1
a	a	k8xC
sloučeniny	sloučenina	k1gFnPc1
</s>
<s>
Většina	většina	k1gFnSc1
světové	světový	k2eAgFnSc2d1
produkce	produkce	k1gFnSc2
yttria	yttrium	k1gNnSc2
slouží	sloužit	k5eAaImIp3nS
v	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
jako	jako	k9
základní	základní	k2eAgInSc4d1
materiál	materiál	k1gInSc4
při	při	k7c6
syntéze	syntéza	k1gFnSc6
luminoforů	luminofor	k1gInPc2
pro	pro	k7c4
výrobu	výroba	k1gFnSc4
vakuových	vakuový	k2eAgFnPc2d1
obrazovek	obrazovka	k1gFnPc2
barevných	barevný	k2eAgInPc2d1
televizorů	televizor	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Společně	společně	k6eAd1
s	s	k7c7
oxidy	oxid	k1gInPc7
europia	europium	k1gNnSc2
se	s	k7c7
sloučeniny	sloučenina	k1gFnPc1
yttria	yttrium	k1gNnSc2
nanášejí	nanášet	k5eAaImIp3nP
na	na	k7c4
vnitřní	vnitřní	k2eAgFnSc4d1
stranu	strana	k1gFnSc4
televizní	televizní	k2eAgFnSc2d1
obrazovky	obrazovka	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
po	po	k7c6
dopadu	dopad	k1gInSc6
urychleného	urychlený	k2eAgInSc2d1
elektronu	elektron	k1gInSc2
vydávají	vydávat	k5eAaPmIp3nP,k5eAaImIp3nP
červené	červený	k2eAgNnSc4d1
luminiscenční	luminiscenční	k2eAgNnSc4d1
záření	záření	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Oxidy	oxid	k1gInPc1
železa	železo	k1gNnSc2
<g/>
,	,	kIx,
hliníku	hliník	k1gInSc2
a	a	k8xC
yttria	yttrium	k1gNnSc2
(	(	kIx(
<g/>
granáty	granát	k1gInPc4
<g/>
)	)	kIx)
Y	Y	kA
<g/>
3	#num#	k4
<g/>
Fe	Fe	k1gFnSc2
<g/>
5	#num#	k4
<g/>
O	o	k7c4
<g/>
12	#num#	k4
a	a	k8xC
Y	Y	kA
<g/>
3	#num#	k4
<g/>
Al	ala	k1gFnPc2
<g/>
5	#num#	k4
<g/>
O	O	kA
<g/>
12	#num#	k4
mají	mít	k5eAaImIp3nP
tvrdost	tvrdost	k1gFnSc4
až	až	k9
8,5	8,5	k4
Mohsovy	Mohsův	k2eAgFnSc2d1
stupnice	stupnice	k1gFnSc2
a	a	k8xC
používají	používat	k5eAaImIp3nP
se	se	k3xPyFc4
při	při	k7c6
výrobě	výroba	k1gFnSc6
šperků	šperk	k1gInPc2
jako	jako	k8xS,k8xC
levná	levný	k2eAgFnSc1d1
náhrada	náhrada	k1gFnSc1
diamantu	diamant	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nacházejí	nacházet	k5eAaImIp3nP
uplatnění	uplatnění	k1gNnSc4
i	i	k9
jako	jako	k9
snímací	snímací	k2eAgMnPc4d1
členy	člen	k1gMnPc4
akustické	akustický	k2eAgFnSc2d1
energie	energie	k1gFnSc2
a	a	k8xC
při	při	k7c6
výrobě	výroba	k1gFnSc6
infračervených	infračervený	k2eAgInPc2d1
laserů	laser	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
metalurgii	metalurgie	k1gFnSc6
se	s	k7c7
přídavky	přídavek	k1gInPc7
malého	malý	k2eAgNnSc2d1
množství	množství	k1gNnSc2
yttria	yttrium	k1gNnSc2
do	do	k7c2
slitin	slitina	k1gFnPc2
hliníku	hliník	k1gInSc2
a	a	k8xC
hořčíku	hořčík	k1gInSc2
(	(	kIx(
<g/>
duralů	dural	k1gInPc2
<g/>
)	)	kIx)
značně	značně	k6eAd1
zvyšuje	zvyšovat	k5eAaImIp3nS
jejich	jejich	k3xOp3gFnSc4
pevnost	pevnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
slitinách	slitina	k1gFnPc6
hliníku	hliník	k1gInSc2
navíc	navíc	k6eAd1
zvyšuje	zvyšovat	k5eAaImIp3nS
vodivost	vodivost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
slitina	slitina	k1gFnSc1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
se	se	k3xPyFc4
do	do	k7c2
drátů	drát	k1gInPc2
vysokého	vysoký	k2eAgNnSc2d1
napětí	napětí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Litina	litina	k1gFnSc1
s	s	k7c7
obsahem	obsah	k1gInSc7
yttria	yttrium	k1gNnSc2
získává	získávat	k5eAaImIp3nS
značně	značně	k6eAd1
vyšší	vysoký	k2eAgFnSc1d2
tvárnost	tvárnost	k1gFnSc1
a	a	k8xC
kujnost	kujnost	k1gFnSc1
–	–	k?
tzv.	tzv.	kA
kujná	kujný	k2eAgFnSc1d1
litina	litina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
výrobě	výroba	k1gFnSc6
vanadu	vanad	k1gInSc2
a	a	k8xC
některých	některý	k3yIgInPc2
dalších	další	k2eAgInPc2d1
neželezných	železný	k2eNgInPc2d1
kovů	kov	k1gInPc2
slouží	sloužit	k5eAaImIp3nP
yttrium	yttrium	k1gNnSc4
k	k	k7c3
odstraňování	odstraňování	k1gNnSc3
kyslíku	kyslík	k1gInSc2
–	–	k?
deoxidaci	deoxidace	k1gFnSc6
vyráběného	vyráběný	k2eAgInSc2d1
kovu	kov	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Při	při	k7c6
výrobě	výroba	k1gFnSc6
skla	sklo	k1gNnSc2
a	a	k8xC
keramiky	keramika	k1gFnSc2
působí	působit	k5eAaImIp3nP
přídavky	přídavek	k1gInPc4
oxidu	oxid	k1gInSc2
yttritého	yttritý	k2eAgNnSc2d1
zvýšení	zvýšení	k1gNnSc2
bodu	bod	k1gInSc2
tání	tání	k1gNnSc2
<g/>
,	,	kIx,
zlepšují	zlepšovat	k5eAaImIp3nP
odolnost	odolnost	k1gFnSc4
proti	proti	k7c3
tepelnému	tepelný	k2eAgInSc3d1
šoku	šok	k1gInSc3
a	a	k8xC
snižují	snižovat	k5eAaImIp3nP
tepelnou	tepelný	k2eAgFnSc4d1
roztažnost	roztažnost	k1gFnSc4
produktu	produkt	k1gInSc2
.	.	kIx.
</s>
<s>
Sloučenina	sloučenina	k1gFnSc1
(	(	kIx(
<g/>
Y	Y	kA
<g/>
1,2	1,2	k4
<g/>
Ba	ba	k9
<g/>
0,8	0,8	k4
<g/>
CuO	CuO	k1gFnPc2
<g/>
4	#num#	k4
<g/>
)	)	kIx)
vykazuje	vykazovat	k5eAaImIp3nS
supravodivé	supravodivý	k2eAgFnPc4d1
vlastnosti	vlastnost	k1gFnPc4
i	i	k9
při	při	k7c6
teplotách	teplota	k1gFnPc6
kolem	kolem	k7c2
90	#num#	k4
K	K	kA
<g/>
,	,	kIx,
tedy	tedy	k9
nad	nad	k7c7
bodem	bod	k1gInSc7
varu	var	k1gInSc2
kapalného	kapalný	k2eAgInSc2d1
dusíku	dusík	k1gInSc2
a	a	k8xC
je	být	k5eAaImIp3nS
proto	proto	k8xC
perspektivním	perspektivní	k2eAgInSc7d1
materiálem	materiál	k1gInSc7
pro	pro	k7c4
výrobu	výroba	k1gFnSc4
prakticky	prakticky	k6eAd1
využitelných	využitelný	k2eAgInPc2d1
supravodivých	supravodivý	k2eAgInPc2d1
materiálů	materiál	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
The	The	k1gMnSc1
tremendous	tremendous	k1gMnSc1
potential	potential	k1gMnSc1
of	of	k?
deep-sea	deep-sea	k1gMnSc1
mud	mud	k?
as	as	k1gInSc1
a	a	k8xC
source	source	k1gMnSc1
of	of	k?
rare-earth	rare-earth	k1gInSc1
elements	elementsa	k1gFnPc2
<g/>
,	,	kIx,
10	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
2018	#num#	k4
<g/>
↑	↑	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Energy	Energ	k1gInPc4
Department	department	k1gInSc1
Releases	Releases	k1gMnSc1
New	New	k1gMnSc1
Critical	Critical	k1gMnSc1
Materials	Materials	k1gInSc4
Strategy	Stratega	k1gFnSc2
<g/>
,	,	kIx,
15	#num#	k4
<g/>
.	.	kIx.
prosinec	prosinec	k1gInSc1
2010	#num#	k4
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Cotton	Cotton	k1gInSc1
F.	F.	kA
<g/>
A.	A.	kA
<g/>
,	,	kIx,
Wilkinson	Wilkinson	k1gMnSc1
J.	J.	kA
<g/>
:	:	kIx,
<g/>
Anorganická	anorganický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
<g/>
,	,	kIx,
souborné	souborný	k2eAgNnSc1d1
zpracování	zpracování	k1gNnSc1
pro	pro	k7c4
pokročilé	pokročilý	k1gMnPc4
<g/>
,	,	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1973	#num#	k4
</s>
<s>
Holzbecher	Holzbechra	k1gFnPc2
Z.	Z.	kA
<g/>
:	:	kIx,
<g/>
Analytická	analytický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
<g/>
,	,	kIx,
SNTL	SNTL	kA
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1974	#num#	k4
</s>
<s>
Dr	dr	kA
<g/>
.	.	kIx.
Heinrich	Heinrich	k1gMnSc1
Remy	remy	k1gNnSc2
<g/>
,	,	kIx,
Anorganická	anorganický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
1	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc4
1961	#num#	k4
</s>
<s>
N.	N.	kA
N.	N.	kA
Greenwood	Greenwood	k1gInSc1
–	–	k?
A.	A.	kA
Earnshaw	Earnshaw	k1gFnSc2
<g/>
,	,	kIx,
Chemie	chemie	k1gFnSc1
prvků	prvek	k1gInPc2
1	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc2
1993	#num#	k4
ISBN	ISBN	kA
80-85427-38-9	80-85427-38-9	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
yttrium	yttrium	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
yttrium	yttrium	k1gNnSc4
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Periodická	periodický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
prvků	prvek	k1gInPc2
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
15	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
17	#num#	k4
</s>
<s>
18	#num#	k4
</s>
<s>
H	H	kA
</s>
<s>
He	he	k0
</s>
<s>
Li	li	k8xS
</s>
<s>
Be	Be	k?
</s>
<s>
B	B	kA
</s>
<s>
C	C	kA
</s>
<s>
N	N	kA
</s>
<s>
O	o	k7c6
</s>
<s>
F	F	kA
</s>
<s>
Ne	ne	k9
</s>
<s>
Na	na	k7c6
</s>
<s>
Mg	mg	kA
</s>
<s>
Al	ala	k1gFnPc2
</s>
<s>
Si	se	k3xPyFc3
</s>
<s>
P	P	kA
</s>
<s>
S	s	k7c7
</s>
<s>
Cl	Cl	k?
</s>
<s>
Ar	ar	k1gInSc1
</s>
<s>
K	k	k7c3
</s>
<s>
Ca	ca	kA
</s>
<s>
Sc	Sc	k?
</s>
<s>
Ti	ten	k3xDgMnPc1
</s>
<s>
V	v	k7c6
</s>
<s>
Cr	cr	k0
</s>
<s>
Mn	Mn	k?
</s>
<s>
Fe	Fe	k?
</s>
<s>
Co	co	k3yQnSc1,k3yRnSc1,k3yInSc1
</s>
<s>
Ni	on	k3xPp3gFnSc4
</s>
<s>
Cu	Cu	k?
</s>
<s>
Zn	zn	kA
</s>
<s>
Ga	Ga	k?
</s>
<s>
Ge	Ge	k?
</s>
<s>
As	as	k9
</s>
<s>
Se	s	k7c7
</s>
<s>
Br	br	k0
</s>
<s>
Kr	Kr	k?
</s>
<s>
Rb	Rb	k?
</s>
<s>
Sr	Sr	k?
</s>
<s>
Y	Y	kA
</s>
<s>
Zr	Zr	k?
</s>
<s>
Nb	Nb	k?
</s>
<s>
Mo	Mo	k?
</s>
<s>
Tc	tc	k0
</s>
<s>
Ru	Ru	k?
</s>
<s>
Rh	Rh	k?
</s>
<s>
Pd	Pd	k?
</s>
<s>
Ag	Ag	k?
</s>
<s>
Cd	cd	kA
</s>
<s>
In	In	k?
</s>
<s>
Sn	Sn	k?
</s>
<s>
Sb	sb	kA
</s>
<s>
Te	Te	k?
</s>
<s>
I	i	k9
</s>
<s>
Xe	Xe	k?
</s>
<s>
Cs	Cs	k?
</s>
<s>
Ba	ba	k9
</s>
<s>
La	la	k1gNnSc1
</s>
<s>
Ce	Ce	k?
</s>
<s>
Pr	pr	k0
</s>
<s>
Nd	Nd	k?
</s>
<s>
Pm	Pm	k?
</s>
<s>
Sm	Sm	k?
</s>
<s>
Eu	Eu	k?
</s>
<s>
Gd	Gd	k?
</s>
<s>
Tb	Tb	k?
</s>
<s>
Dy	Dy	k?
</s>
<s>
Ho	on	k3xPp3gNnSc4
</s>
<s>
Er	Er	k?
</s>
<s>
Tm	Tm	k?
</s>
<s>
Yb	Yb	k?
</s>
<s>
Lu	Lu	k?
</s>
<s>
Hf	Hf	k?
</s>
<s>
Ta	ten	k3xDgFnSc1
</s>
<s>
W	W	kA
</s>
<s>
Re	re	k9
</s>
<s>
Os	osa	k1gFnPc2
</s>
<s>
Ir	Ir	k1gMnSc1
</s>
<s>
Pt	Pt	k?
</s>
<s>
Au	au	k0
</s>
<s>
Hg	Hg	k?
</s>
<s>
Tl	Tl	k?
</s>
<s>
Pb	Pb	k?
</s>
<s>
Bi	Bi	k?
</s>
<s>
Po	po	k7c6
</s>
<s>
At	At	k?
</s>
<s>
Rn	Rn	k?
</s>
<s>
Fr	fr	k0
</s>
<s>
Ra	ra	k0
</s>
<s>
Ac	Ac	k?
</s>
<s>
Th	Th	k?
</s>
<s>
Pa	Pa	kA
</s>
<s>
U	u	k7c2
</s>
<s>
Np	Np	k?
</s>
<s>
Pu	Pu	k?
</s>
<s>
Am	Am	k?
</s>
<s>
Cm	cm	kA
</s>
<s>
Bk	Bk	k?
</s>
<s>
Cf	Cf	k?
</s>
<s>
Es	es	k1gNnSc1
</s>
<s>
Fm	Fm	k?
</s>
<s>
Md	Md	k?
</s>
<s>
No	no	k9
</s>
<s>
Lr	Lr	k?
</s>
<s>
Rf	Rf	k?
</s>
<s>
Db	db	kA
</s>
<s>
Sg	Sg	k?
</s>
<s>
Bh	Bh	k?
</s>
<s>
Hs	Hs	k?
</s>
<s>
Mt	Mt	k?
</s>
<s>
Ds	Ds	k?
</s>
<s>
Rg	Rg	k?
</s>
<s>
Cn	Cn	k?
</s>
<s>
Nh	Nh	k?
</s>
<s>
Fl	Fl	k?
</s>
<s>
Mc	Mc	k?
</s>
<s>
Lv	Lv	k?
</s>
<s>
Ts	ts	k0
</s>
<s>
Og	Og	k?
</s>
<s>
Alkalické	alkalický	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Kovy	kov	k1gInPc1
alkalických	alkalický	k2eAgFnPc2d1
zemin	zemina	k1gFnPc2
</s>
<s>
Lanthanoidy	Lanthanoida	k1gFnPc1
</s>
<s>
Aktinoidy	Aktinoida	k1gFnPc1
</s>
<s>
Přechodné	přechodný	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Nepřechodné	přechodný	k2eNgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Polokovy	Polokův	k2eAgFnPc1d1
</s>
<s>
Nekovy	nekov	k1gInPc1
</s>
<s>
Halogeny	halogen	k1gInPc1
</s>
<s>
Vzácné	vzácný	k2eAgInPc1d1
plyny	plyn	k1gInPc1
</s>
<s>
neznámé	známý	k2eNgNnSc1d1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4067222-0	4067222-0	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
5757	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85149410	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85149410	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Chemie	chemie	k1gFnSc1
</s>
