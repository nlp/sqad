<p>
<s>
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
rybka	rybka	k1gFnSc1	rybka
je	být	k5eAaImIp3nS	být
pohádkové	pohádkový	k2eAgNnSc1d1	pohádkové
<g/>
,	,	kIx,	,
kouzelné	kouzelný	k2eAgNnSc1d1	kouzelné
zvíře	zvíře	k1gNnSc1	zvíře
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
obyčejně	obyčejně	k6eAd1	obyčejně
splní	splnit	k5eAaPmIp3nS	splnit
tři	tři	k4xCgNnPc4	tři
přání	přání	k1gNnPc4	přání
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
ji	on	k3xPp3gFnSc4	on
pustí	pustit	k5eAaPmIp3nS	pustit
na	na	k7c4	na
svobodu	svoboda	k1gFnSc4	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
vešla	vejít	k5eAaPmAgFnS	vejít
ve	v	k7c4	v
známost	známost	k1gFnSc4	známost
především	především	k9	především
díky	díky	k7c3	díky
pohádce	pohádka	k1gFnSc3	pohádka
Alexandra	Alexandr	k1gMnSc2	Alexandr
Sergejeviče	Sergejevič	k1gMnSc2	Sergejevič
Puškina	Puškin	k1gMnSc2	Puškin
<g/>
,	,	kIx,	,
Pohádce	pohádka	k1gFnSc6	pohádka
o	o	k7c6	o
rybáři	rybář	k1gMnSc6	rybář
a	a	k8xC	a
rybce	rybka	k1gFnSc6	rybka
(	(	kIx(	(
<g/>
С	С	k?	С
о	о	k?	о
р	р	k?	р
и	и	k?	и
р	р	k?	р
<g/>
)	)	kIx)	)
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1833	[number]	k4	1833
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vychází	vycházet	k5eAaImIp3nS	vycházet
ze	z	k7c2	z
staré	starý	k2eAgFnSc2d1	stará
ruské	ruský	k2eAgFnSc2d1	ruská
lidové	lidový	k2eAgFnSc2d1	lidová
slovesnosti	slovesnost	k1gFnSc2	slovesnost
<g/>
.	.	kIx.	.
<g/>
Pohádka	pohádka	k1gFnSc1	pohádka
o	o	k7c6	o
rybáři	rybář	k1gMnSc6	rybář
a	a	k8xC	a
rybce	rybka	k1gFnSc6	rybka
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
svárlivé	svárlivý	k2eAgFnSc6d1	svárlivá
stařeně	stařena	k1gFnSc6	stařena
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
neuspokojí	uspokojit	k5eNaPmIp3nS	uspokojit
žádný	žádný	k3yNgInSc4	žádný
z	z	k7c2	z
ohromných	ohromný	k2eAgInPc2d1	ohromný
darů	dar	k1gInPc2	dar
zlaté	zlatý	k2eAgFnSc2d1	zlatá
rybky	rybka	k1gFnSc2	rybka
<g/>
,	,	kIx,	,
královny	královna	k1gFnSc2	královna
moří	moře	k1gNnPc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
touze	touha	k1gFnSc6	touha
po	po	k7c6	po
stále	stále	k6eAd1	stále
větším	veliký	k2eAgNnSc6d2	veliký
bohatství	bohatství	k1gNnSc6	bohatství
a	a	k8xC	a
moci	moc	k1gFnSc6	moc
je	být	k5eAaImIp3nS	být
nakonec	nakonec	k6eAd1	nakonec
potrestána	potrestán	k2eAgFnSc1d1	potrestána
a	a	k8xC	a
stane	stanout	k5eAaPmIp3nS	stanout
se	se	k3xPyFc4	se
zas	zas	k6eAd1	zas
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
čím	co	k3yInSc7	co
byla	být	k5eAaImAgFnS	být
<g/>
:	:	kIx,	:
chudou	chudý	k2eAgFnSc7d1	chudá
ženou	žena	k1gFnSc7	žena
rybáře	rybář	k1gMnSc2	rybář
v	v	k7c6	v
rozbité	rozbitý	k2eAgFnSc6d1	rozbitá
hliněné	hliněný	k2eAgFnSc3d1	hliněná
chatrči	chatrč	k1gFnSc3	chatrč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
rybka	rybka	k1gFnSc1	rybka
se	se	k3xPyFc4	se
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
stala	stát	k5eAaPmAgFnS	stát
námětem	námět	k1gInSc7	námět
mnoha	mnoho	k4c2	mnoho
adaptací	adaptace	k1gFnPc2	adaptace
příběhu	příběh	k1gInSc2	příběh
o	o	k7c6	o
zlaté	zlatý	k2eAgFnSc6d1	zlatá
rybce	rybka	k1gFnSc6	rybka
<g/>
,	,	kIx,	,
knižních	knižní	k2eAgInPc2d1	knižní
<g/>
,	,	kIx,	,
hudebních	hudební	k2eAgInPc2d1	hudební
<g/>
,	,	kIx,	,
televizních	televizní	k2eAgInPc2d1	televizní
<g/>
,	,	kIx,	,
filmových	filmový	k2eAgNnPc2d1	filmové
i	i	k8xC	i
divadelních	divadelní	k2eAgNnPc2d1	divadelní
<g/>
.	.	kIx.	.
</s>
<s>
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
rybka	rybka	k1gFnSc1	rybka
je	být	k5eAaImIp3nS	být
též	též	k9	též
ústředním	ústřední	k2eAgInSc7d1	ústřední
motivem	motiv	k1gInSc7	motiv
mnoha	mnoho	k4c2	mnoho
vtipů	vtip	k1gInPc2	vtip
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c4	za
opravdovou	opravdový	k2eAgFnSc4d1	opravdová
zlatou	zlatý	k2eAgFnSc4d1	zlatá
rybku	rybka	k1gFnSc4	rybka
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
považován	považován	k2eAgMnSc1d1	považován
karas	karas	k1gMnSc1	karas
zlatý	zlatý	k2eAgMnSc1d1	zlatý
(	(	kIx(	(
<g/>
Carassius	Carassius	k1gMnSc1	Carassius
auratus	auratus	k1gMnSc1	auratus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
tomu	ten	k3xDgNnSc3	ten
v	v	k7c6	v
asijských	asijský	k2eAgFnPc6d1	asijská
zemích	zem	k1gFnPc6	zem
zlatá	zlatý	k2eAgFnSc1d1	zlatá
ryba	ryba	k1gFnSc1	ryba
sice	sice	k8xC	sice
neplní	plnit	k5eNaImIp3nS	plnit
přání	přání	k1gNnSc4	přání
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
například	například	k6eAd1	například
baramundi	baramund	k1gMnPc1	baramund
malajský	malajský	k2eAgInSc1d1	malajský
(	(	kIx(	(
<g/>
Scleropages	Scleropages	k1gInSc1	Scleropages
formosus	formosus	k1gInSc1	formosus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
rybu	ryba	k1gFnSc4	ryba
symbolizující	symbolizující	k2eAgNnSc1d1	symbolizující
štěstí	štěstí	k1gNnSc1	štěstí
<g/>
,	,	kIx,	,
zdraví	zdraví	k1gNnSc2	zdraví
<g/>
,	,	kIx,	,
prosperitu	prosperita	k1gFnSc4	prosperita
a	a	k8xC	a
sílu	síla	k1gFnSc4	síla
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
