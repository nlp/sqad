<s>
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
rybka	rybka	k1gFnSc1	rybka
je	být	k5eAaImIp3nS	být
pohádkové	pohádkový	k2eAgNnSc1d1	pohádkové
<g/>
,	,	kIx,	,
kouzelné	kouzelný	k2eAgNnSc1d1	kouzelné
zvíře	zvíře	k1gNnSc1	zvíře
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
obyčejně	obyčejně	k6eAd1	obyčejně
splní	splnit	k5eAaPmIp3nS	splnit
tři	tři	k4xCgNnPc4	tři
přání	přání	k1gNnPc4	přání
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
ji	on	k3xPp3gFnSc4	on
pustí	pustit	k5eAaPmIp3nS	pustit
na	na	k7c4	na
svobodu	svoboda	k1gFnSc4	svoboda
<g/>
.	.	kIx.	.
</s>
