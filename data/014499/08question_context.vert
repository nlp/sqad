<s>
Druhá	druhý	k4xOgFnSc1
světová	světový	k2eAgFnSc1d1
válka	válka	k1gFnSc1
byl	být	k5eAaImAgInS
globální	globální	k2eAgInSc1d1
vojenský	vojenský	k2eAgInSc1d1
konflikt	konflikt	k1gInSc1
v	v	k7c6
letech	let	k1gInPc6
1939	#num#	k4
<g/>
–	–	k?
<g/>
1945	#num#	k4
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc1
se	se	k3xPyFc4
zúčastnila	zúčastnit	k5eAaPmAgFnS
většina	většina	k1gFnSc1
států	stát	k1gInPc2
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
