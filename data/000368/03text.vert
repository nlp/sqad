<s>
Vodík	vodík	k1gInSc1	vodík
(	(	kIx(	(
<g/>
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
H	H	kA	H
latinsky	latinsky	k6eAd1	latinsky
Hydrogenium	hydrogenium	k1gNnSc1	hydrogenium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejlehčí	lehký	k2eAgMnSc1d3	nejlehčí
a	a	k8xC	a
nejjednodušší	jednoduchý	k2eAgInSc1d3	nejjednodušší
plynný	plynný	k2eAgInSc1d1	plynný
chemický	chemický	k2eAgInSc1d1	chemický
prvek	prvek	k1gInSc1	prvek
<g/>
,	,	kIx,	,
tvořící	tvořící	k2eAgFnSc4d1	tvořící
převážnou	převážný	k2eAgFnSc4d1	převážná
část	část	k1gFnSc4	část
hmoty	hmota	k1gFnSc2	hmota
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
široké	široký	k2eAgNnSc1d1	široké
praktické	praktický	k2eAgNnSc1d1	praktické
využití	využití	k1gNnSc1	využití
jako	jako	k8xS	jako
zdroj	zdroj	k1gInSc1	zdroj
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
redukční	redukční	k2eAgNnSc1d1	redukční
činidlo	činidlo	k1gNnSc1	činidlo
v	v	k7c6	v
chemické	chemický	k2eAgFnSc6d1	chemická
syntéze	syntéza	k1gFnSc6	syntéza
nebo	nebo	k8xC	nebo
metalurgii	metalurgie	k1gFnSc6	metalurgie
a	a	k8xC	a
také	také	k9	také
jako	jako	k9	jako
náplň	náplň	k1gFnSc1	náplň
meteorologických	meteorologický	k2eAgInPc2d1	meteorologický
a	a	k8xC	a
pouťových	pouťový	k2eAgInPc2d1	pouťový
balonů	balon	k1gInPc2	balon
a	a	k8xC	a
do	do	k7c2	do
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
i	i	k8xC	i
vzducholodí	vzducholoď	k1gFnPc2	vzducholoď
<g/>
.	.	kIx.	.
</s>
<s>
Vodík	vodík	k1gInSc1	vodík
je	být	k5eAaImIp3nS	být
bezbarvý	bezbarvý	k2eAgInSc1d1	bezbarvý
<g/>
,	,	kIx,	,
lehký	lehký	k2eAgInSc1d1	lehký
plyn	plyn	k1gInSc1	plyn
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
chuti	chuť	k1gFnSc2	chuť
a	a	k8xC	a
zápachu	zápach	k1gInSc2	zápach
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
hořlavý	hořlavý	k2eAgMnSc1d1	hořlavý
<g/>
,	,	kIx,	,
hoří	hořet	k5eAaImIp3nS	hořet
namodralým	namodralý	k2eAgInSc7d1	namodralý
plamenem	plamen	k1gInSc7	plamen
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hoření	hoření	k1gNnSc1	hoření
nepodporuje	podporovat	k5eNaImIp3nS	podporovat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
14,38	[number]	k4	14,38
<g/>
krát	krát	k6eAd1	krát
lehčí	lehký	k2eAgFnSc4d2	lehčí
než	než	k8xS	než
vzduch	vzduch	k1gInSc4	vzduch
a	a	k8xC	a
vede	vést	k5eAaImIp3nS	vést
teplo	teplo	k1gNnSc1	teplo
7	[number]	k4	7
<g/>
krát	krát	k6eAd1	krát
lépe	dobře	k6eAd2	dobře
než	než	k8xS	než
vzduch	vzduch	k1gInSc4	vzduch
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Vodík	vodík	k1gInSc1	vodík
je	být	k5eAaImIp3nS	být
za	za	k7c4	za
normální	normální	k2eAgFnPc4d1	normální
teploty	teplota	k1gFnPc4	teplota
stabilní	stabilní	k2eAgFnSc1d1	stabilní
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
fluorem	fluor	k1gInSc7	fluor
se	se	k3xPyFc4	se
slučuje	slučovat	k5eAaImIp3nS	slučovat
za	za	k7c4	za
pokojové	pokojový	k2eAgFnPc4d1	pokojová
teploty	teplota	k1gFnPc4	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
reaktivnější	reaktivní	k2eAgFnSc1d2	reaktivnější
při	při	k7c6	při
zahřátí	zahřátí	k1gNnSc6	zahřátí
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
s	s	k7c7	s
kyslíkem	kyslík	k1gInSc7	kyslík
a	a	k8xC	a
halogeny	halogen	k1gInPc7	halogen
se	se	k3xPyFc4	se
slučuje	slučovat	k5eAaImIp3nS	slučovat
velmi	velmi	k6eAd1	velmi
bouřlivě	bouřlivě	k6eAd1	bouřlivě
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
pro	pro	k7c4	pro
spuštění	spuštění	k1gNnSc4	spuštění
této	tento	k3xDgFnSc2	tento
reakce	reakce	k1gFnSc2	reakce
je	být	k5eAaImIp3nS	být
nutná	nutný	k2eAgFnSc1d1	nutná
inicializace	inicializace	k1gFnSc1	inicializace
(	(	kIx(	(
<g/>
např.	např.	kA	např.
jiskra	jiskra	k1gFnSc1	jiskra
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zapálí	zapálit	k5eAaPmIp3nS	zapálit
kyslíko-vodíkový	kyslíkoodíkový	k2eAgInSc4d1	kyslíko-vodíkový
plamen	plamen	k1gInSc4	plamen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vodík	vodík	k1gInSc1	vodík
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
málo	málo	k6eAd1	málo
rozpustný	rozpustný	k2eAgMnSc1d1	rozpustný
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
některé	některý	k3yIgInPc1	některý
kovy	kov	k1gInPc1	kov
ho	on	k3xPp3gMnSc4	on
pohlcují	pohlcovat	k5eAaImIp3nP	pohlcovat
(	(	kIx(	(
<g/>
nejlépe	dobře	k6eAd3	dobře
palladium	palladium	k1gNnSc1	palladium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vodík	vodík	k1gInSc1	vodík
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
sloučeniny	sloučenina	k1gFnPc1	sloučenina
se	s	k7c7	s
všemi	všecek	k3xTgInPc7	všecek
prvky	prvek	k1gInPc7	prvek
periodické	periodický	k2eAgFnSc2d1	periodická
tabulky	tabulka	k1gFnSc2	tabulka
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
vzácných	vzácný	k2eAgInPc2d1	vzácný
plynů	plyn	k1gInPc2	plyn
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pak	pak	k6eAd1	pak
s	s	k7c7	s
uhlíkem	uhlík	k1gInSc7	uhlík
<g/>
,	,	kIx,	,
kyslíkem	kyslík	k1gInSc7	kyslík
<g/>
,	,	kIx,	,
sírou	síra	k1gFnSc7	síra
a	a	k8xC	a
dusíkem	dusík	k1gInSc7	dusík
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
základní	základní	k2eAgFnPc4d1	základní
stavební	stavební	k2eAgFnPc4d1	stavební
jednotky	jednotka	k1gFnPc4	jednotka
života	život	k1gInSc2	život
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Vodík	vodík	k1gInSc1	vodík
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgInSc1d1	schopen
tvořit	tvořit	k5eAaImF	tvořit
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
typ	typ	k1gInSc4	typ
chemické	chemický	k2eAgFnSc2d1	chemická
vazby	vazba	k1gFnSc2	vazba
<g/>
,	,	kIx,	,
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
vodíková	vodíkový	k2eAgFnSc1d1	vodíková
vazba	vazba	k1gFnSc1	vazba
nebo	nebo	k8xC	nebo
také	také	k9	také
vodíkový	vodíkový	k2eAgInSc4d1	vodíkový
můstek	můstek	k1gInSc4	můstek
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vázaný	vázaný	k2eAgInSc1d1	vázaný
atom	atom	k1gInSc1	atom
vodíku	vodík	k1gInSc2	vodík
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
afinitu	afinita	k1gFnSc4	afinita
i	i	k9	i
k	k	k7c3	k
dalším	další	k2eAgInPc3d1	další
atomům	atom	k1gInPc3	atom
<g/>
,	,	kIx,	,
s	s	k7c7	s
nimiž	jenž	k3xRgInPc7	jenž
není	být	k5eNaImIp3nS	být
poután	poutat	k5eAaImNgInS	poutat
klasickou	klasický	k2eAgFnSc7d1	klasická
chemickou	chemický	k2eAgFnSc7d1	chemická
vazbou	vazba	k1gFnSc7	vazba
<g/>
.	.	kIx.	.
</s>
<s>
Mimořádně	mimořádně	k6eAd1	mimořádně
silná	silný	k2eAgFnSc1d1	silná
je	být	k5eAaImIp3nS	být
vodíková	vodíkový	k2eAgFnSc1d1	vodíková
vazba	vazba	k1gFnSc1	vazba
s	s	k7c7	s
atomy	atom	k1gInPc7	atom
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
anomální	anomální	k2eAgFnPc4d1	anomální
fyzikální	fyzikální	k2eAgFnPc4d1	fyzikální
vlastnosti	vlastnost	k1gFnPc4	vlastnost
vody	voda	k1gFnSc2	voda
(	(	kIx(	(
<g/>
vysoký	vysoký	k2eAgInSc1d1	vysoký
bod	bod	k1gInSc1	bod
varu	var	k1gInSc2	var
a	a	k8xC	a
tání	tání	k1gNnSc2	tání
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavou	zajímavý	k2eAgFnSc7d1	zajímavá
vlastností	vlastnost	k1gFnSc7	vlastnost
vodíku	vodík	k1gInSc2	vodík
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
schopnost	schopnost	k1gFnSc1	schopnost
"	"	kIx"	"
<g/>
rozpouštět	rozpouštět	k5eAaImF	rozpouštět
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
kovech	kov	k1gInPc6	kov
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
palladiu	palladion	k1gNnSc6	palladion
nebo	nebo	k8xC	nebo
platině	platina	k1gFnSc6	platina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
poté	poté	k6eAd1	poté
fungují	fungovat	k5eAaImIp3nP	fungovat
jako	jako	k9	jako
katalyzátory	katalyzátor	k1gInPc1	katalyzátor
chemických	chemický	k2eAgFnPc2d1	chemická
reakcí	reakce	k1gFnPc2	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
vodík	vodík	k1gInSc4	vodík
velmi	velmi	k6eAd1	velmi
malé	malý	k2eAgFnPc1d1	malá
molekuly	molekula	k1gFnPc1	molekula
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
schopny	schopen	k2eAgFnPc1d1	schopna
procházet	procházet	k5eAaImF	procházet
různými	různý	k2eAgInPc7d1	různý
materiály	materiál	k1gInPc7	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Vodík	vodík	k1gInSc1	vodík
objevil	objevit	k5eAaPmAgInS	objevit
roku	rok	k1gInSc2	rok
1766	[number]	k4	1766
Angličan	Angličan	k1gMnSc1	Angličan
Henry	Henry	k1gMnSc1	Henry
Cavendish	Cavendish	k1gMnSc1	Cavendish
<g/>
,	,	kIx,	,
když	když	k8xS	když
si	se	k3xPyFc3	se
všiml	všimnout	k5eAaPmAgMnS	všimnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
rozpouštění	rozpouštění	k1gNnSc6	rozpouštění
neušlechtilých	ušlechtilý	k2eNgInPc2d1	neušlechtilý
kovů	kov	k1gInPc2	kov
v	v	k7c6	v
kyselině	kyselina	k1gFnSc6	kyselina
vzniká	vznikat	k5eAaImIp3nS	vznikat
bezbarvý	bezbarvý	k2eAgInSc1d1	bezbarvý
<g/>
,	,	kIx,	,
hořlavý	hořlavý	k2eAgInSc1d1	hořlavý
plyn	plyn	k1gInSc1	plyn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1781	[number]	k4	1781
poznal	poznat	k5eAaPmAgMnS	poznat
také	také	k9	také
jako	jako	k9	jako
první	první	k4xOgFnSc1	první
<g/>
,	,	kIx,	,
že	že	k8xS	že
voda	voda	k1gFnSc1	voda
je	být	k5eAaImIp3nS	být
sloučeninou	sloučenina	k1gFnSc7	sloučenina
kyslíku	kyslík	k1gInSc2	kyslík
a	a	k8xC	a
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
Robert	Robert	k1gMnSc1	Robert
Boyle	Boyle	k1gFnSc2	Boyle
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
také	také	k9	také
Boyleův-Marriotův	Boyleův-Marriotův	k2eAgInSc1d1	Boyleův-Marriotův
zákon	zákon	k1gInSc1	zákon
<g/>
)	)	kIx)	)
pozoroval	pozorovat	k5eAaImAgMnS	pozorovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1671	[number]	k4	1671
reakci	reakce	k1gFnSc3	reakce
železa	železo	k1gNnSc2	železo
se	s	k7c7	s
zředěnými	zředěný	k2eAgFnPc7d1	zředěná
kyselinami	kyselina	k1gFnPc7	kyselina
(	(	kIx(	(
<g/>
sírovou	sírový	k2eAgFnSc7d1	sírová
a	a	k8xC	a
chlorovodíkovou	chlorovodíkový	k2eAgFnSc4d1	chlorovodíková
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
tvorbě	tvorba	k1gFnSc3	tvorba
plynného	plynný	k2eAgInSc2d1	plynný
vodíku	vodík	k1gInSc2	vodík
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nepoznal	poznat	k5eNaPmAgMnS	poznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
takto	takto	k6eAd1	takto
uvolněný	uvolněný	k2eAgInSc4d1	uvolněný
plyn	plyn	k1gInSc4	plyn
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1766	[number]	k4	1766
si	se	k3xPyFc3	se
Henry	Henry	k1gMnSc1	Henry
Cavendish	Cavendish	k1gMnSc1	Cavendish
uvědomil	uvědomit	k5eAaPmAgMnS	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc4	tento
plyn	plyn	k1gInSc4	plyn
je	být	k5eAaImIp3nS	být
chemické	chemický	k2eAgNnSc1d1	chemické
individuum	individuum	k1gNnSc1	individuum
a	a	k8xC	a
nazval	nazvat	k5eAaPmAgMnS	nazvat
jej	on	k3xPp3gMnSc4	on
"	"	kIx"	"
<g/>
hořlavým	hořlavý	k2eAgInSc7d1	hořlavý
vzduchem	vzduch	k1gInSc7	vzduch
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Cavendish	Cavendish	k1gInSc1	Cavendish
je	být	k5eAaImIp3nS	být
takto	takto	k6eAd1	takto
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
objevitele	objevitel	k1gMnSc4	objevitel
vodíku	vodík	k1gInSc2	vodík
jako	jako	k8xS	jako
chemického	chemický	k2eAgInSc2d1	chemický
prvku	prvek	k1gInSc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Cavendish	Cavendish	k1gMnSc1	Cavendish
ovšem	ovšem	k9	ovšem
spekuloval	spekulovat	k5eAaImAgMnS	spekulovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
hořlavý	hořlavý	k2eAgInSc1d1	hořlavý
vzduch	vzduch	k1gInSc1	vzduch
je	být	k5eAaImIp3nS	být
vlastně	vlastně	k9	vlastně
hledaná	hledaný	k2eAgFnSc1d1	hledaná
hypotetická	hypotetický	k2eAgFnSc1d1	hypotetická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
flogiston	flogiston	k1gInSc1	flogiston
a	a	k8xC	a
své	svůj	k3xOyFgFnSc2	svůj
flogistonové	flogistonový	k2eAgFnSc2d1	flogistonová
teorie	teorie	k1gFnSc2	teorie
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
nevzdal	vzdát	k5eNaPmAgMnS	vzdát
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1781	[number]	k4	1781
zjistil	zjistit	k5eAaPmAgInS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
látka	látka	k1gFnSc1	látka
tvoří	tvořit	k5eAaImIp3nS	tvořit
při	při	k7c6	při
hoření	hoření	k1gNnSc6	hoření
vodu	voda	k1gFnSc4	voda
což	což	k3yQnSc1	což
otevřelo	otevřít	k5eAaPmAgNnS	otevřít
cestu	cesta	k1gFnSc4	cesta
k	k	k7c3	k
objevu	objev	k1gInSc3	objev
vodíku	vodík	k1gInSc2	vodík
jako	jako	k8xC	jako
chemického	chemický	k2eAgInSc2d1	chemický
prvku	prvek	k1gInSc2	prvek
(	(	kIx(	(
<g/>
elementu	element	k1gInSc2	element
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1783	[number]	k4	1783
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
Antoine	Antoin	k1gInSc5	Antoin
Lavoisier	Lavoisier	k1gInSc1	Lavoisier
tento	tento	k3xDgInSc4	tento
prvek	prvek	k1gInSc4	prvek
"	"	kIx"	"
<g/>
hydrogen	hydrogen	k1gInSc1	hydrogen
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
z	z	k7c2	z
řecký	řecký	k2eAgMnSc1d1	řecký
slov	slovo	k1gNnPc2	slovo
hydro	hydra	k1gFnSc5	hydra
(	(	kIx(	(
<g/>
voda	voda	k1gFnSc1	voda
<g/>
)	)	kIx)	)
a	a	k8xC	a
genes	genes	k1gMnSc1	genes
(	(	kIx(	(
<g/>
tvořící	tvořící	k2eAgFnPc1d1	tvořící
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lavoisier	Lavoisier	k1gMnSc1	Lavoisier
syntetizoval	syntetizovat	k5eAaImAgMnS	syntetizovat
vodu	voda	k1gFnSc4	voda
hořením	hoření	k1gNnSc7	hoření
vodíku	vodík	k1gInSc2	vodík
v	v	k7c6	v
proudu	proud	k1gInSc6	proud
kyslíku	kyslík	k1gInSc2	kyslík
a	a	k8xC	a
kvantitativní	kvantitativní	k2eAgInPc1d1	kvantitativní
výsledky	výsledek	k1gInPc1	výsledek
potvrdily	potvrdit	k5eAaPmAgInP	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
voda	voda	k1gFnSc1	voda
není	být	k5eNaImIp3nS	být
element	element	k1gInSc4	element
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
po	po	k7c4	po
2000	[number]	k4	2000
<g/>
let	léto	k1gNnPc2	léto
považovalo	považovat	k5eAaImAgNnS	považovat
za	za	k7c4	za
zřejmé	zřejmý	k2eAgInPc4d1	zřejmý
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
sloučenina	sloučenina	k1gFnSc1	sloučenina
těchto	tento	k3xDgInPc2	tento
dvou	dva	k4xCgInPc2	dva
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Lavoisier	Lavoisier	k1gInSc1	Lavoisier
provedl	provést	k5eAaPmAgInS	provést
rovněž	rovněž	k9	rovněž
rozklad	rozklad	k1gInSc1	rozklad
vody	voda	k1gFnSc2	voda
rozžhaveným	rozžhavený	k2eAgNnSc7d1	rozžhavené
železem	železo	k1gNnSc7	železo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1783	[number]	k4	1783
<g/>
.	.	kIx.	.
</s>
<s>
Rozklad	rozklad	k1gInSc4	rozklad
vody	voda	k1gFnSc2	voda
elektrickým	elektrický	k2eAgInSc7d1	elektrický
proudem	proud	k1gInSc7	proud
na	na	k7c4	na
jedntotlivé	jedntotlivý	k2eAgFnPc4d1	jedntotlivý
složky	složka	k1gFnPc4	složka
<g/>
,	,	kIx,	,
kyslík	kyslík	k1gInSc4	kyslík
a	a	k8xC	a
vodík	vodík	k1gInSc4	vodík
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
provedli	provést	k5eAaPmAgMnP	provést
Jan	Jan	k1gMnSc1	Jan
Rudolph	Rudolph	k1gMnSc1	Rudolph
Deiman	Deiman	k1gMnSc1	Deiman
and	and	k?	and
Adriaan	Adriaan	k1gInSc1	Adriaan
Paets	Paets	k1gInSc1	Paets
van	van	k1gInSc1	van
Troostwijk	Troostwijk	k1gInSc1	Troostwijk
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1789	[number]	k4	1789
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
název	název	k1gInSc1	název
pro	pro	k7c4	pro
hydrogen	hydrogen	k1gInSc4	hydrogen
(	(	kIx(	(
<g/>
vodík	vodík	k1gInSc4	vodík
<g/>
)	)	kIx)	)
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
Jana	Jan	k1gMnSc2	Jan
Svatopluka	Svatopluk	k1gMnSc2	Svatopluk
Presla	Presla	k1gMnSc2	Presla
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Karlem	Karel	k1gMnSc7	Karel
Slavojem	Slavoj	k1gMnSc7	Slavoj
Amerlingem	Amerling	k1gInSc7	Amerling
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
české	český	k2eAgInPc1d1	český
názvy	název	k1gInPc1	název
pro	pro	k7c4	pro
tehdy	tehdy	k6eAd1	tehdy
známé	známý	k2eAgInPc4d1	známý
prvky	prvek	k1gInPc4	prvek
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
než	než	k8xS	než
Mendělejev	Mendělejev	k1gMnSc1	Mendělejev
publikoval	publikovat	k5eAaBmAgMnS	publikovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1869	[number]	k4	1869
svůj	svůj	k3xOyFgInSc4	svůj
objev	objev	k1gInSc4	objev
periodické	periodický	k2eAgFnSc2d1	periodická
tabulky	tabulka	k1gFnSc2	tabulka
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Vodík	vodík	k1gInSc1	vodík
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
deseti	deset	k4xCc2	deset
tzv.	tzv.	kA	tzv.
archaických	archaický	k2eAgInPc2d1	archaický
názvů	název	k1gInPc2	název
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
používají	používat	k5eAaImIp3nP	používat
dodnes	dodnes	k6eAd1	dodnes
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgInPc1d1	ostatní
názvy	název	k1gInPc1	název
se	se	k3xPyFc4	se
neujaly	ujmout	k5eNaPmAgInP	ujmout
<g/>
.	.	kIx.	.
</s>
<s>
Vodík	vodík	k1gInSc1	vodík
je	být	k5eAaImIp3nS	být
jediný	jediný	k2eAgInSc1d1	jediný
prvek	prvek	k1gInSc1	prvek
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
isotopy	isotop	k1gInPc1	isotop
mají	mít	k5eAaImIp3nP	mít
vlastní	vlastní	k2eAgInPc4d1	vlastní
chemické	chemický	k2eAgInPc4d1	chemický
názvy	název	k1gInPc4	název
a	a	k8xC	a
značky	značka	k1gFnPc4	značka
<g/>
.	.	kIx.	.
</s>
<s>
Deuterium	deuterium	k1gNnSc1	deuterium
(	(	kIx(	(
<g/>
D	D	kA	D
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
H	H	kA	H
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
isotop	isotop	k1gInSc1	isotop
tvořený	tvořený	k2eAgInSc1d1	tvořený
jedním	jeden	k4xCgInSc7	jeden
protonem	proton	k1gInSc7	proton
a	a	k8xC	a
jedním	jeden	k4xCgInSc7	jeden
neutronem	neutron	k1gInSc7	neutron
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
atomu	atom	k1gInSc2	atom
<g/>
,	,	kIx,	,
Tritium	tritium	k1gNnSc1	tritium
(	(	kIx(	(
<g/>
T	T	kA	T
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
H	H	kA	H
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
isotop	isotop	k1gInSc1	isotop
tvořený	tvořený	k2eAgInSc1d1	tvořený
jedním	jeden	k4xCgInSc7	jeden
protonem	proton	k1gInSc7	proton
a	a	k8xC	a
dvěma	dva	k4xCgInPc7	dva
neutrony	neutron	k1gInPc7	neutron
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
atomu	atom	k1gInSc2	atom
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
samotný	samotný	k2eAgInSc4d1	samotný
vodík	vodík	k1gInSc4	vodík
je	být	k5eAaImIp3nS	být
vyhrazen	vyhrazen	k2eAgInSc4d1	vyhrazen
název	název	k1gInSc4	název
protium	protium	k1gNnSc4	protium
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
značka	značka	k1gFnSc1	značka
P	P	kA	P
se	se	k3xPyFc4	se
nepoužívá	používat	k5eNaImIp3nS	používat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
obsazena	obsadit	k5eAaPmNgFnS	obsadit
značkou	značka	k1gFnSc7	značka
fosforu	fosfor	k1gInSc2	fosfor
(	(	kIx(	(
<g/>
P	P	kA	P
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
IUPAC	IUPAC	kA	IUPAC
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
používání	používání	k1gNnSc1	používání
značek	značka	k1gFnPc2	značka
D	D	kA	D
<g/>
,	,	kIx,	,
T	T	kA	T
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
preferuje	preferovat	k5eAaImIp3nS	preferovat
2	[number]	k4	2
<g/>
H	H	kA	H
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
H.	H.	kA	H.
Plynný	plynný	k2eAgInSc4d1	plynný
vodík	vodík	k1gInSc4	vodík
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
ve	v	k7c6	v
dvouatomové	dvouatomový	k2eAgFnSc6d1	dvouatomová
molekule	molekula	k1gFnSc6	molekula
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
stabilnější	stabilní	k2eAgMnSc1d2	stabilnější
než	než	k8xS	než
atomární	atomární	k2eAgInSc1d1	atomární
vodík	vodík	k1gInSc1	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
normální	normální	k2eAgFnSc2d1	normální
teploty	teplota	k1gFnSc2	teplota
se	se	k3xPyFc4	se
vodík	vodík	k1gInSc1	vodík
chová	chovat	k5eAaImIp3nS	chovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
směsí	směs	k1gFnSc7	směs
tří	tři	k4xCgInPc2	tři
objemů	objem	k1gInPc2	objem
ortho-	ortho-	k?	ortho-
a	a	k8xC	a
jednoho	jeden	k4xCgInSc2	jeden
objemu	objem	k1gInSc2	objem
para-	para-	k?	para-
<g/>
.	.	kIx.	.
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
Při	při	k7c6	při
skladování	skladování	k1gNnSc6	skladování
kapalného	kapalný	k2eAgInSc2d1	kapalný
vodíku	vodík	k1gInSc2	vodík
probíhá	probíhat	k5eAaImIp3nS	probíhat
přeměna	přeměna	k1gFnSc1	přeměna
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
ortho	ortze	k6eAd1	ortze
--	--	k?	--
<g/>
>	>	kIx)	>
para	para	k1gFnSc1	para
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
exotermická	exotermický	k2eAgFnSc1d1	exotermická
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
ke	k	k7c3	k
ztrátě	ztráta	k1gFnSc3	ztráta
až	až	k9	až
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
původní	původní	k2eAgFnSc2d1	původní
kapaliny	kapalina	k1gFnSc2	kapalina
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
nutno	nutno	k6eAd1	nutno
vyvinout	vyvinout	k5eAaPmF	vyvinout
postup	postup	k1gInSc4	postup
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
při	při	k7c6	při
zkapalňování	zkapalňování	k1gNnSc6	zkapalňování
vodíku	vodík	k1gInSc2	vodík
zároveň	zároveň	k6eAd1	zároveň
provádí	provádět	k5eAaImIp3nS	provádět
přeměna	přeměna	k1gFnSc1	přeměna
na	na	k7c4	na
para-H	para-H	k?	para-H
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Deuterium	deuterium	k1gNnSc1	deuterium
bylo	být	k5eAaImAgNnS	být
spektroskopicky	spektroskopicky	k6eAd1	spektroskopicky
pozorováno	pozorovat	k5eAaImNgNnS	pozorovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1931	[number]	k4	1931
<g/>
,	,	kIx,	,
příprava	příprava	k1gFnSc1	příprava
téměř	téměř	k6eAd1	téměř
čisté	čistý	k2eAgFnSc2d1	čistá
sloučeniny	sloučenina	k1gFnSc2	sloučenina
D2O	D2O	k1gMnSc1	D2O
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
těžká	těžký	k2eAgFnSc1d1	těžká
voda	voda	k1gFnSc1	voda
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
provedena	provést	k5eAaPmNgFnS	provést
elektrolyticky	elektrolyticky	k6eAd1	elektrolyticky
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1933	[number]	k4	1933
<g/>
.	.	kIx.	.
</s>
<s>
Tritium	tritium	k1gNnSc1	tritium
bylo	být	k5eAaImAgNnS	být
poprvé	poprvé	k6eAd1	poprvé
připraveno	připravit	k5eAaPmNgNnS	připravit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1934	[number]	k4	1934
bombardováním	bombardování	k1gNnSc7	bombardování
sloučenin	sloučenina	k1gFnPc2	sloučenina
deuteria	deuterium	k1gNnSc2	deuterium
deuterony	deuteron	k1gInPc4	deuteron
<g/>
,	,	kIx,	,
schematicky	schematicky	k6eAd1	schematicky
2	[number]	k4	2
<g/>
H	H	kA	H
<g/>
+	+	kIx~	+
<g/>
2	[number]	k4	2
<g/>
H	H	kA	H
--	--	k?	--
<g/>
>	>	kIx)	>
3H	[number]	k4	3H
+	+	kIx~	+
<g/>
H.	H.	kA	H.
Elementární	elementární	k2eAgInSc1d1	elementární
vodík	vodík	k1gInSc1	vodík
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
přítomen	přítomno	k1gNnPc2	přítomno
jen	jen	k9	jen
vzácně	vzácně	k6eAd1	vzácně
<g/>
,	,	kIx,	,
nejvíce	hodně	k6eAd3	hodně
elementárního	elementární	k2eAgInSc2d1	elementární
vodíku	vodík	k1gInSc2	vodík
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
sopek	sopka	k1gFnPc2	sopka
v	v	k7c6	v
sopečných	sopečný	k2eAgInPc6d1	sopečný
plynech	plyn	k1gInPc6	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Plynný	plynný	k2eAgInSc1d1	plynný
vodík	vodík	k1gInSc1	vodík
se	se	k3xPyFc4	se
v	v	k7c6	v
našem	náš	k3xOp1gInSc6	náš
prostředí	prostředí	k1gNnSc6	prostředí
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
dvouatomových	dvouatomový	k2eAgFnPc2d1	dvouatomová
molekul	molekula	k1gFnPc2	molekula
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
však	však	k9	však
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
mezihvězdném	mezihvězdný	k2eAgInSc6d1	mezihvězdný
prostoru	prostor	k1gInSc6	prostor
je	být	k5eAaImIp3nS	být
přítomen	přítomen	k2eAgMnSc1d1	přítomen
z	z	k7c2	z
převážné	převážný	k2eAgFnSc2d1	převážná
části	část	k1gFnSc2	část
jako	jako	k8xS	jako
atomární	atomární	k2eAgInSc1d1	atomární
vodík	vodík	k1gInSc1	vodík
H.	H.	kA	H.
V	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
atmosféře	atmosféra	k1gFnSc6	atmosféra
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
jen	jen	k9	jen
ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
vrstvách	vrstva	k1gFnPc6	vrstva
a	a	k8xC	a
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
mimořádně	mimořádně	k6eAd1	mimořádně
nízké	nízký	k2eAgFnSc3d1	nízká
hmotnosti	hmotnost	k1gFnSc3	hmotnost
postupně	postupně	k6eAd1	postupně
z	z	k7c2	z
atmosféry	atmosféra	k1gFnSc2	atmosféra
vyprchává	vyprchávat	k5eAaImIp3nS	vyprchávat
<g/>
.	.	kIx.	.
</s>
<s>
Elementární	elementární	k2eAgInSc1d1	elementární
vodík	vodík	k1gInSc1	vodík
je	být	k5eAaImIp3nS	být
však	však	k9	však
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
podstatných	podstatný	k2eAgFnPc2d1	podstatná
složek	složka	k1gFnPc2	složka
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
<g/>
,	,	kIx,	,
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
i	i	k9	i
v	v	k7c6	v
ložiscích	ložisko	k1gNnPc6	ložisko
uhlí	uhlí	k1gNnSc2	uhlí
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
sloučenin	sloučenina	k1gFnPc2	sloučenina
je	být	k5eAaImIp3nS	být
nejvíce	nejvíce	k6eAd1	nejvíce
zastoupena	zastoupen	k2eAgFnSc1d1	zastoupena
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
jako	jako	k9	jako
moře	moře	k1gNnSc1	moře
a	a	k8xC	a
oceány	oceán	k1gInPc1	oceán
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
zemského	zemský	k2eAgInSc2d1	zemský
povrchu	povrch	k1gInSc2	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
vypočteno	vypočten	k2eAgNnSc1d1	vypočteno
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
vodík	vodík	k1gInSc1	vodík
podílí	podílet	k5eAaImIp3nS	podílet
na	na	k7c4	na
složení	složení	k1gNnSc4	složení
zemské	zemský	k2eAgFnSc2d1	zemská
kůry	kůra	k1gFnSc2	kůra
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
atmosféry	atmosféra	k1gFnSc2	atmosféra
a	a	k8xC	a
hydrosféry	hydrosféra	k1gFnSc2	hydrosféra
<g/>
)	)	kIx)	)
0,88	[number]	k4	0,88
hmotnostními	hmotnostní	k2eAgNnPc7d1	hmotnostní
procenty	procento	k1gNnPc7	procento
a	a	k8xC	a
15,5	[number]	k4	15,5
atomárních	atomární	k2eAgNnPc2d1	atomární
procent	procento	k1gNnPc2	procento
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc4d1	další
významný	významný	k2eAgInSc4d1	významný
zdroj	zdroj	k1gInSc4	zdroj
vodíku	vodík	k1gInSc2	vodík
představují	představovat	k5eAaImIp3nP	představovat
organické	organický	k2eAgFnPc1d1	organická
sloučeniny	sloučenina	k1gFnPc1	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Vodík	vodík	k1gInSc1	vodík
patří	patřit	k5eAaImIp3nS	patřit
společně	společně	k6eAd1	společně
s	s	k7c7	s
uhlíkem	uhlík	k1gInSc7	uhlík
<g/>
,	,	kIx,	,
kyslíkem	kyslík	k1gInSc7	kyslík
a	a	k8xC	a
dusíkem	dusík	k1gInSc7	dusík
mezi	mezi	k7c4	mezi
biogenní	biogenní	k2eAgInPc4d1	biogenní
prvky	prvek	k1gInPc4	prvek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
základní	základní	k2eAgInPc1d1	základní
stavební	stavební	k2eAgInPc1d1	stavební
kameny	kámen	k1gInPc1	kámen
všech	všecek	k3xTgInPc2	všecek
živých	živý	k2eAgInPc2d1	živý
organizmů	organizmus	k1gInPc2	organizmus
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
vodík	vodík	k1gInSc1	vodík
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
prakticky	prakticky	k6eAd1	prakticky
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
sloučeninách	sloučenina	k1gFnPc6	sloučenina
tvořících	tvořící	k2eAgMnPc2d1	tvořící
nejvýznamnější	významný	k2eAgFnSc4d3	nejvýznamnější
surovinu	surovina	k1gFnSc4	surovina
současné	současný	k2eAgFnSc2d1	současná
energetiky	energetika	k1gFnSc2	energetika
a	a	k8xC	a
organické	organický	k2eAgFnSc2d1	organická
chemie	chemie	k1gFnSc2	chemie
-	-	kIx~	-
ropu	ropa	k1gFnSc4	ropa
<g/>
.	.	kIx.	.
</s>
<s>
Vodík	vodík	k1gInSc1	vodík
je	být	k5eAaImIp3nS	být
základním	základní	k2eAgInSc7d1	základní
stavebním	stavební	k2eAgInSc7d1	stavební
prvkem	prvek	k1gInSc7	prvek
celého	celý	k2eAgInSc2d1	celý
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
,	,	kIx,	,
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
jak	jak	k6eAd1	jak
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
svítících	svítící	k2eAgFnPc6d1	svítící
hvězdách	hvězda	k1gFnPc6	hvězda
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
v	v	k7c6	v
mezigalaktickém	mezigalaktický	k2eAgInSc6d1	mezigalaktický
prostoru	prostor	k1gInSc6	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
současných	současný	k2eAgNnPc2d1	současné
měření	měření	k1gNnPc2	měření
se	se	k3xPyFc4	se
podílí	podílet	k5eAaImIp3nS	podílet
ze	z	k7c2	z
75	[number]	k4	75
%	%	kIx~	%
na	na	k7c6	na
hmotě	hmota	k1gFnSc6	hmota
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
z	z	k7c2	z
90	[number]	k4	90
%	%	kIx~	%
na	na	k7c6	na
počtu	počet	k1gInSc6	počet
atomů	atom	k1gInPc2	atom
přítomných	přítomný	k2eAgInPc2d1	přítomný
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Vodík	vodík	k1gInSc1	vodík
se	se	k3xPyFc4	se
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
tvoří	tvořit	k5eAaImIp3nP	tvořit
při	při	k7c6	při
rozkladu	rozklad	k1gInSc6	rozklad
organických	organický	k2eAgFnPc2d1	organická
látek	látka	k1gFnPc2	látka
některými	některý	k3yIgFnPc7	některý
bakteriemi	bakterie	k1gFnPc7	bakterie
<g/>
.	.	kIx.	.
</s>
<s>
Genetické	genetický	k2eAgNnSc1d1	genetické
inženýrství	inženýrství	k1gNnSc1	inženýrství
usiluje	usilovat	k5eAaImIp3nS	usilovat
o	o	k7c4	o
zdokonalení	zdokonalení	k1gNnSc4	zdokonalení
tohoto	tento	k3xDgInSc2	tento
procesu	proces	k1gInSc2	proces
do	do	k7c2	do
míry	míra	k1gFnSc2	míra
průmyslově	průmyslově	k6eAd1	průmyslově
využitelné	využitelný	k2eAgFnSc2d1	využitelná
k	k	k7c3	k
produkci	produkce	k1gFnSc3	produkce
vodíku	vodík	k1gInSc2	vodík
pro	pro	k7c4	pro
vodíkové	vodíkový	k2eAgInPc4d1	vodíkový
motory	motor	k1gInPc4	motor
<g/>
.	.	kIx.	.
</s>
<s>
Vodík	vodík	k1gInSc1	vodík
se	se	k3xPyFc4	se
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
při	při	k7c6	při
koksování	koksování	k1gNnSc6	koksování
uhlí	uhlí	k1gNnSc2	uhlí
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
ve	v	k7c6	v
svítiplynu	svítiplyn	k1gInSc6	svítiplyn
a	a	k8xC	a
koksárenském	koksárenský	k2eAgInSc6d1	koksárenský
plynu	plyn	k1gInSc6	plyn
tvoří	tvořit	k5eAaImIp3nS	tvořit
okolo	okolo	k7c2	okolo
50	[number]	k4	50
%	%	kIx~	%
obj	obj	k?	obj
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
dříve	dříve	k6eAd2	dříve
využívalo	využívat	k5eAaImAgNnS	využívat
při	při	k7c6	při
průmyslové	průmyslový	k2eAgFnSc6d1	průmyslová
výrobě	výroba	k1gFnSc6	výroba
vodíku	vodík	k1gInSc2	vodík
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tyto	tento	k3xDgInPc1	tento
plyny	plyn	k1gInPc1	plyn
zkapalnily	zkapalnit	k5eAaPmAgInP	zkapalnit
a	a	k8xC	a
vodík	vodík	k1gInSc1	vodík
se	se	k3xPyFc4	se
oddestiloval	oddestilovat	k5eAaPmAgInS	oddestilovat
<g/>
.	.	kIx.	.
</s>
<s>
Vodík	vodík	k1gInSc1	vodík
se	se	k3xPyFc4	se
ve	v	k7c6	v
velkém	velký	k2eAgNnSc6d1	velké
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
termickým	termický	k2eAgInSc7d1	termický
rozkladem	rozklad	k1gInSc7	rozklad
methanu	methan	k1gInSc2	methan
(	(	kIx(	(
<g/>
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
<g/>
)	)	kIx)	)
při	při	k7c6	při
1000	[number]	k4	1000
°	°	k?	°
<g/>
C.	C.	kA	C.
CH4	CH4	k1gFnSc2	CH4
→	→	k?	→
C	C	kA	C
+	+	kIx~	+
2	[number]	k4	2
H2	H2	k1gFnSc4	H2
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
využívaných	využívaný	k2eAgFnPc2d1	využívaná
příprav	příprava	k1gFnPc2	příprava
vodíku	vodík	k1gInSc2	vodík
je	být	k5eAaImIp3nS	být
katalytické	katalytický	k2eAgNnSc1d1	katalytické
štěpení	štěpení	k1gNnSc1	štěpení
methanolu	methanol	k1gInSc2	methanol
vodní	vodní	k2eAgFnSc7d1	vodní
parou	para	k1gFnSc7	para
při	při	k7c6	při
250	[number]	k4	250
°	°	k?	°
<g/>
C.	C.	kA	C.
CH3OH	CH3OH	k1gMnSc1	CH3OH
+	+	kIx~	+
H2O	H2O	k1gMnSc1	H2O
→	→	k?	→
CO2	CO2	k1gMnSc1	CO2
+	+	kIx~	+
3	[number]	k4	3
H2	H2	k1gFnPc2	H2
Další	další	k2eAgNnSc4d1	další
málo	málo	k1gNnSc4	málo
využívaná	využívaný	k2eAgFnSc1d1	využívaná
příprava	příprava	k1gFnSc1	příprava
je	být	k5eAaImIp3nS	být
katalytický	katalytický	k2eAgInSc4d1	katalytický
rozklad	rozklad	k1gInSc4	rozklad
amoniaku	amoniak	k1gInSc2	amoniak
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
okolo	okolo	k7c2	okolo
1000	[number]	k4	1000
°	°	k?	°
<g/>
C	C	kA	C
2	[number]	k4	2
NH3	NH3	k1gFnSc2	NH3
→	→	k?	→
N2	N2	k1gMnSc1	N2
+	+	kIx~	+
3	[number]	k4	3
H2	H2	k1gFnSc7	H2
Rozpouštění	rozpouštění	k1gNnSc2	rozpouštění
neušlechtilých	ušlechtilý	k2eNgInPc2d1	neušlechtilý
kovů	kov	k1gInPc2	kov
v	v	k7c6	v
kyselinách	kyselina	k1gFnPc6	kyselina
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
k	k	k7c3	k
přípravě	příprava	k1gFnSc3	příprava
vodíku	vodík	k1gInSc2	vodík
v	v	k7c6	v
laboratoři	laboratoř	k1gFnSc6	laboratoř
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
využívá	využívat	k5eAaImIp3nS	využívat
reakce	reakce	k1gFnSc1	reakce
zinku	zinek	k1gInSc2	zinek
s	s	k7c7	s
kyselinou	kyselina	k1gFnSc7	kyselina
chlorovodíkovou	chlorovodíkový	k2eAgFnSc7d1	chlorovodíková
<g/>
.	.	kIx.	.
</s>
<s>
Zn	zn	kA	zn
+	+	kIx~	+
2	[number]	k4	2
HCl	HCl	k1gFnSc2	HCl
→	→	k?	→
ZnCl	ZnCl	k1gInSc1	ZnCl
<g/>
2	[number]	k4	2
+	+	kIx~	+
H2	H2	k1gFnSc1	H2
Reakcí	reakce	k1gFnPc2	reakce
amfoterních	amfoterní	k2eAgInPc2d1	amfoterní
kovů	kov	k1gInPc2	kov
s	s	k7c7	s
roztoky	roztok	k1gInPc7	roztok
hydroxidů	hydroxid	k1gInPc2	hydroxid
vznikají	vznikat	k5eAaImIp3nP	vznikat
rozpustné	rozpustný	k2eAgInPc1d1	rozpustný
hydroxokomplexy	hydroxokomplex	k1gInPc1	hydroxokomplex
a	a	k8xC	a
vodík	vodík	k1gInSc1	vodík
<g/>
,	,	kIx,	,
nejtypičtější	typický	k2eAgFnSc1d3	nejtypičtější
je	být	k5eAaImIp3nS	být
reakce	reakce	k1gFnSc1	reakce
hliníku	hliník	k1gInSc2	hliník
s	s	k7c7	s
roztokem	roztok	k1gInSc7	roztok
hydroxidu	hydroxid	k1gInSc2	hydroxid
sodného	sodný	k2eAgInSc2d1	sodný
<g/>
.	.	kIx.	.
</s>
<s>
Popřípadě	popřípadě	k6eAd1	popřípadě
lze	lze	k6eAd1	lze
využít	využít	k5eAaPmF	využít
reakce	reakce	k1gFnPc4	reakce
křemíku	křemík	k1gInSc2	křemík
s	s	k7c7	s
roztokem	roztok	k1gInSc7	roztok
hydroxidu	hydroxid	k1gInSc2	hydroxid
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
hydroxidu	hydroxid	k1gInSc2	hydroxid
sodného	sodný	k2eAgMnSc2d1	sodný
nebo	nebo	k8xC	nebo
směsi	směs	k1gFnPc4	směs
hydroxidu	hydroxid	k1gInSc2	hydroxid
sodného	sodný	k2eAgInSc2d1	sodný
a	a	k8xC	a
hydroxidu	hydroxid	k1gInSc2	hydroxid
vápenatého	vápenatý	k2eAgInSc2d1	vápenatý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
2	[number]	k4	2
Al	ala	k1gFnPc2	ala
+	+	kIx~	+
2	[number]	k4	2
NaOH	NaOH	k1gFnPc2	NaOH
+	+	kIx~	+
6	[number]	k4	6
H2O	H2O	k1gFnSc2	H2O
→	→	k?	→
2	[number]	k4	2
Na	na	k7c4	na
<g/>
[	[	kIx(	[
<g/>
Al	ala	k1gFnPc2	ala
<g/>
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
<g/>
4	[number]	k4	4
<g/>
]	]	kIx)	]
+	+	kIx~	+
<g />
.	.	kIx.	.
</s>
<s>
3	[number]	k4	3
H2	H2	k1gFnSc1	H2
Si	se	k3xPyFc3	se
+	+	kIx~	+
4	[number]	k4	4
NaOH	NaOH	k1gFnSc2	NaOH
→	→	k?	→
Na	na	k7c6	na
<g/>
4	[number]	k4	4
<g/>
SiO	SiO	k1gFnSc6	SiO
<g/>
4	[number]	k4	4
+	+	kIx~	+
2	[number]	k4	2
H2	H2	k1gFnPc4	H2
Si	se	k3xPyFc3	se
+	+	kIx~	+
Ca	ca	kA	ca
<g/>
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
+	+	kIx~	+
2	[number]	k4	2
NaOH	NaOH	k1gFnSc2	NaOH
→	→	k?	→
Na	na	k7c6	na
<g/>
2	[number]	k4	2
<g/>
SiO	SiO	k1gFnSc6	SiO
<g/>
3	[number]	k4	3
+	+	kIx~	+
CaO	CaO	k1gFnSc2	CaO
+	+	kIx~	+
2	[number]	k4	2
H2	H2	k1gFnPc2	H2
Vodík	vodík	k1gInSc4	vodík
vzniká	vznikat	k5eAaImIp3nS	vznikat
také	také	k9	také
jako	jako	k9	jako
odpadní	odpadní	k2eAgInSc4d1	odpadní
produkt	produkt	k1gInSc4	produkt
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
hydroxidů	hydroxid	k1gInPc2	hydroxid
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
sodík	sodík	k1gInSc1	sodík
reaguje	reagovat	k5eAaBmIp3nS	reagovat
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
hydroxidu	hydroxid	k1gInSc2	hydroxid
sodného	sodný	k2eAgInSc2d1	sodný
a	a	k8xC	a
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
2	[number]	k4	2
Na	na	k7c4	na
+	+	kIx~	+
2	[number]	k4	2
H2O	H2O	k1gFnSc2	H2O
→	→	k?	→
2	[number]	k4	2
NaOH	NaOH	k1gFnSc1	NaOH
+	+	kIx~	+
H2	H2	k1gFnSc1	H2
Reakce	reakce	k1gFnSc2	reakce
hydridu	hydrid	k1gInSc2	hydrid
vápenatého	vápenatý	k2eAgMnSc4d1	vápenatý
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
vzniká	vznikat	k5eAaImIp3nS	vznikat
hydroxid	hydroxid	k1gInSc1	hydroxid
vápenatý	vápenatý	k2eAgInSc1d1	vápenatý
a	a	k8xC	a
vodík	vodík	k1gInSc1	vodík
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
reakce	reakce	k1gFnSc1	reakce
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
praktické	praktický	k2eAgNnSc4d1	praktické
použití	použití	k1gNnSc4	použití
nevyužitelná	využitelný	k2eNgFnSc1d1	nevyužitelná
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
hydrid	hydrid	k1gInSc1	hydrid
vápenatý	vápenatý	k2eAgInSc1d1	vápenatý
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
drahý	drahý	k2eAgInSc1d1	drahý
<g/>
.	.	kIx.	.
</s>
<s>
CaH	CaH	k?	CaH
<g/>
2	[number]	k4	2
+	+	kIx~	+
2	[number]	k4	2
H2O	H2O	k1gFnSc2	H2O
→	→	k?	→
Ca	ca	kA	ca
<g/>
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
+	+	kIx~	+
2	[number]	k4	2
H2	H2	k1gMnSc1	H2
Vedením	vedení	k1gNnSc7	vedení
vodní	vodní	k2eAgFnSc2d1	vodní
páry	pára	k1gFnSc2	pára
přes	přes	k7c4	přes
rozžhavené	rozžhavený	k2eAgNnSc4d1	rozžhavené
železo	železo	k1gNnSc4	železo
vzniká	vznikat	k5eAaImIp3nS	vznikat
oxid	oxid	k1gInSc1	oxid
železnato-železitý	železnato-železitý	k2eAgInSc1d1	železnato-železitý
a	a	k8xC	a
vodík	vodík	k1gInSc1	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
oxid	oxid	k1gInSc1	oxid
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
využít	využít	k5eAaPmF	využít
k	k	k7c3	k
tvorbě	tvorba	k1gFnSc3	tvorba
permanentních	permanentní	k2eAgInPc2d1	permanentní
magnetů	magnet	k1gInPc2	magnet
<g/>
.	.	kIx.	.
3	[number]	k4	3
Fe	Fe	k1gFnPc2	Fe
+	+	kIx~	+
4	[number]	k4	4
H2O	H2O	k1gFnSc2	H2O
→	→	k?	→
Fe	Fe	k1gFnSc2	Fe
<g/>
3	[number]	k4	3
<g/>
O	o	k7c4	o
<g/>
4	[number]	k4	4
+	+	kIx~	+
4	[number]	k4	4
H2	H2	k1gFnSc6	H2
Dřívější	dřívější	k2eAgFnSc1d1	dřívější
velmi	velmi	k6eAd1	velmi
využívaná	využívaný	k2eAgFnSc1d1	využívaná
příprava	příprava	k1gFnSc1	příprava
vodíku	vodík	k1gInSc2	vodík
byla	být	k5eAaImAgFnS	být
reakce	reakce	k1gFnSc1	reakce
koksu	koks	k1gInSc2	koks
s	s	k7c7	s
vodní	vodní	k2eAgFnSc7d1	vodní
párou	pára	k1gFnSc7	pára
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
vzniká	vznikat	k5eAaImIp3nS	vznikat
hlavně	hlavně	k9	hlavně
vodní	vodní	k2eAgInSc4d1	vodní
plyn	plyn	k1gInSc4	plyn
<g/>
.	.	kIx.	.
</s>
<s>
H2O	H2O	k4	H2O
+	+	kIx~	+
C	C	kA	C
→	→	k?	→
CO	co	k8xS	co
+	+	kIx~	+
H2	H2	k1gFnSc1	H2
...	...	k?	...
reakce	reakce	k1gFnSc1	reakce
probíhá	probíhat	k5eAaImIp3nS	probíhat
dále	daleko	k6eAd2	daleko
...	...	k?	...
CO	co	k3yRnSc1	co
+	+	kIx~	+
H2O	H2O	k1gMnPc1	H2O
→	→	k?	→
CO2	CO2	k1gFnSc1	CO2
+	+	kIx~	+
H2	H2	k1gFnSc1	H2
Další	další	k2eAgFnSc1d1	další
z	z	k7c2	z
možností	možnost	k1gFnPc2	možnost
je	být	k5eAaImIp3nS	být
reakce	reakce	k1gFnSc1	reakce
methanu	methan	k1gInSc2	methan
s	s	k7c7	s
vodní	vodní	k2eAgFnSc7d1	vodní
párou	pára	k1gFnSc7	pára
<g/>
.	.	kIx.	.
</s>
<s>
Popřípadě	popřípadě	k6eAd1	popřípadě
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
k	k	k7c3	k
methanu	methan	k1gInSc3	methan
a	a	k8xC	a
vodní	vodní	k2eAgFnSc3d1	vodní
páře	pára	k1gFnSc3	pára
přidat	přidat	k5eAaPmF	přidat
kyslík	kyslík	k1gInSc4	kyslík
a	a	k8xC	a
reakce	reakce	k1gFnSc1	reakce
probíhá	probíhat	k5eAaImIp3nS	probíhat
za	za	k7c2	za
velmi	velmi	k6eAd1	velmi
velkého	velký	k2eAgInSc2d1	velký
zisku	zisk	k1gInSc2	zisk
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
CH4	CH4	k4	CH4
+	+	kIx~	+
H2O	H2O	k1gFnSc1	H2O
→	→	k?	→
CO	co	k6eAd1	co
+	+	kIx~	+
3	[number]	k4	3
H2	H2	k1gFnPc2	H2
12	[number]	k4	12
CH4	CH4	k1gFnPc2	CH4
+	+	kIx~	+
5	[number]	k4	5
H2O	H2O	k1gFnPc2	H2O
+	+	kIx~	+
5	[number]	k4	5
O2	O2	k1gFnSc2	O2
→	→	k?	→
29	[number]	k4	29
H2	H2	k1gFnPc2	H2
+	+	kIx~	+
9	[number]	k4	9
CO	co	k6eAd1	co
+	+	kIx~	+
3	[number]	k4	3
CO2	CO2	k1gFnSc7	CO2
Poslední	poslední	k2eAgFnSc7d1	poslední
z	z	k7c2	z
alespoň	alespoň	k9	alespoň
trochu	trochu	k6eAd1	trochu
běžných	běžný	k2eAgFnPc2d1	běžná
příprav	příprava	k1gFnPc2	příprava
vodíku	vodík	k1gInSc2	vodík
je	být	k5eAaImIp3nS	být
reakce	reakce	k1gFnSc1	reakce
fosforu	fosfor	k1gInSc2	fosfor
s	s	k7c7	s
vodní	vodní	k2eAgFnSc7d1	vodní
párou	pára	k1gFnSc7	pára
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
kyseliny	kyselina	k1gFnSc2	kyselina
fosforečné	fosforečný	k2eAgFnSc2d1	fosforečná
a	a	k8xC	a
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
2	[number]	k4	2
P	P	kA	P
+	+	kIx~	+
8	[number]	k4	8
<g />
.	.	kIx.	.
</s>
<s>
H2O	H2O	k4	H2O
→	→	k?	→
2	[number]	k4	2
H3PO4	H3PO4	k1gFnPc2	H3PO4
+	+	kIx~	+
5	[number]	k4	5
H2	H2	k1gFnPc2	H2
Průmyslově	průmyslově	k6eAd1	průmyslově
se	se	k3xPyFc4	se
vodík	vodík	k1gInSc1	vodík
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
elektrolýzou	elektrolýza	k1gFnSc7	elektrolýza
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
2	[number]	k4	2
H2O	H2O	k1gFnSc2	H2O
→	→	k?	→
2	[number]	k4	2
H2	H2	k1gFnSc1	H2
+	+	kIx~	+
O2	O2	k1gFnSc1	O2
Do	do	k7c2	do
budoucna	budoucno	k1gNnSc2	budoucno
se	se	k3xPyFc4	se
počítá	počítat	k5eAaImIp3nS	počítat
s	s	k7c7	s
výrobou	výroba	k1gFnSc7	výroba
vodíku	vodík	k1gInSc2	vodík
pomocí	pomocí	k7c2	pomocí
jaderné	jaderný	k2eAgFnSc2d1	jaderná
energie	energie	k1gFnSc2	energie
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
buď	buď	k8xC	buď
termochemicky	termochemicky	k6eAd1	termochemicky
(	(	kIx(	(
<g/>
vysokými	vysoký	k2eAgFnPc7d1	vysoká
teplotami	teplota	k1gFnPc7	teplota
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
elektrického	elektrický	k2eAgInSc2d1	elektrický
proudu	proud	k1gInSc2	proud
(	(	kIx(	(
<g/>
jaderné	jaderný	k2eAgFnPc1d1	jaderná
elektrárny	elektrárna	k1gFnPc1	elektrárna
by	by	kYmCp3nP	by
tak	tak	k9	tak
mohly	moct	k5eAaImAgInP	moct
být	být	k5eAaImF	být
využívány	využívat	k5eAaImNgInP	využívat
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
pro	pro	k7c4	pro
vyráběný	vyráběný	k2eAgInSc4d1	vyráběný
proud	proud	k1gInSc4	proud
není	být	k5eNaImIp3nS	být
odběr	odběr	k1gInSc1	odběr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
využití	využití	k1gNnSc1	využití
elementárního	elementární	k2eAgInSc2d1	elementární
vodíku	vodík	k1gInSc2	vodík
<g/>
:	:	kIx,	:
V	v	k7c6	v
chemickém	chemický	k2eAgInSc6d1	chemický
průmyslu	průmysl	k1gInSc6	průmysl
je	být	k5eAaImIp3nS	být
vodík	vodík	k1gInSc1	vodík
výborným	výborný	k2eAgNnSc7d1	výborné
redukčním	redukční	k2eAgNnSc7d1	redukční
činidlem	činidlo	k1gNnSc7	činidlo
<g/>
,	,	kIx,	,
sloužícím	sloužící	k1gMnPc3	sloužící
k	k	k7c3	k
sycení	sycení	k1gNnSc3	sycení
násobných	násobný	k2eAgFnPc2d1	násobná
vazeb	vazba	k1gFnPc2	vazba
organických	organický	k2eAgFnPc2d1	organická
molekul	molekula	k1gFnPc2	molekula
<g/>
,	,	kIx,	,
např.	např.	kA	např.
při	při	k7c6	při
ztužování	ztužování	k1gNnSc6	ztužování
rostlinných	rostlinný	k2eAgInPc2d1	rostlinný
olejů	olej	k1gInPc2	olej
<g/>
.	.	kIx.	.
</s>
<s>
Redukčních	redukční	k2eAgFnPc2d1	redukční
vlastností	vlastnost	k1gFnPc2	vlastnost
plynného	plynný	k2eAgInSc2d1	plynný
vodíku	vodík	k1gInSc2	vodík
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
využívá	využívat	k5eAaImIp3nS	využívat
v	v	k7c6	v
metalurgii	metalurgie	k1gFnSc6	metalurgie
k	k	k7c3	k
získávání	získávání	k1gNnSc3	získávání
kovů	kov	k1gInPc2	kov
z	z	k7c2	z
jejich	jejich	k3xOp3gFnPc2	jejich
rud	ruda	k1gFnPc2	ruda
(	(	kIx(	(
<g/>
wolfram	wolfram	k1gInSc1	wolfram
<g/>
,	,	kIx,	,
molybden	molybden	k1gInSc1	molybden
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
proces	proces	k1gInSc1	proces
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
nasazován	nasazovat	k5eAaImNgInS	nasazovat
pouze	pouze	k6eAd1	pouze
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nelze	lze	k6eNd1	lze
využít	využít	k5eAaPmF	využít
běžnější	běžný	k2eAgNnPc4d2	běžnější
redukční	redukční	k2eAgNnPc4d1	redukční
činidla	činidlo	k1gNnPc4	činidlo
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
koks	koks	k1gInSc4	koks
nebo	nebo	k8xC	nebo
dřevěné	dřevěný	k2eAgNnSc4d1	dřevěné
uhlí	uhlí	k1gNnSc4	uhlí
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jednak	jednak	k8xC	jednak
kvůli	kvůli	k7c3	kvůli
poměrně	poměrně	k6eAd1	poměrně
vysoké	vysoký	k2eAgFnSc3d1	vysoká
ceně	cena	k1gFnSc3	cena
vodíku	vodík	k1gInSc2	vodík
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
především	především	k9	především
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
riziko	riziko	k1gNnSc4	riziko
možného	možný	k2eAgInSc2d1	možný
výbuchu	výbuch	k1gInSc2	výbuch
vodíku	vodík	k1gInSc2	vodík
při	při	k7c6	při
kontaminaci	kontaminace	k1gFnSc6	kontaminace
prostředí	prostředí	k1gNnSc2	prostředí
kyslíkem	kyslík	k1gInSc7	kyslík
nebo	nebo	k8xC	nebo
vzduchem	vzduch	k1gInSc7	vzduch
za	za	k7c4	za
vysoké	vysoký	k2eAgFnPc4d1	vysoká
teploty	teplota	k1gFnPc4	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Vodík	vodík	k1gInSc1	vodík
jako	jako	k8xC	jako
zdroj	zdroj	k1gInSc1	zdroj
energie	energie	k1gFnSc2	energie
představuje	představovat	k5eAaImIp3nS	představovat
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
budoucnost	budoucnost	k1gFnSc4	budoucnost
energetiky	energetika	k1gFnSc2	energetika
i	i	k8xC	i
dopravy	doprava	k1gFnSc2	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
spalování	spalování	k1gNnSc6	spalování
vodíku	vodík	k1gInSc2	vodík
vzniká	vznikat	k5eAaImIp3nS	vznikat
vedle	vedle	k7c2	vedle
značného	značný	k2eAgInSc2d1	značný
energetického	energetický	k2eAgInSc2d1	energetický
zisku	zisk	k1gInSc2	zisk
(	(	kIx(	(
<g/>
96	[number]	k4	96
-	-	kIx~	-
120	[number]	k4	120
MJ	mj	kA	mj
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
vodíku	vodík	k1gInSc2	vodík
<g/>
)	)	kIx)	)
pouze	pouze	k6eAd1	pouze
ekologicky	ekologicky	k6eAd1	ekologicky
naprosto	naprosto	k6eAd1	naprosto
nezávadná	závadný	k2eNgFnSc1d1	nezávadná
voda	voda	k1gFnSc1	voda
<g/>
.	.	kIx.	.
</s>
<s>
Automobilové	automobilový	k2eAgInPc1d1	automobilový
motory	motor	k1gInPc1	motor
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
spalování	spalování	k1gNnSc2	spalování
plynného	plynný	k2eAgInSc2d1	plynný
vodíku	vodík	k1gInSc2	vodík
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
předmětem	předmět	k1gInSc7	předmět
intenzivního	intenzivní	k2eAgInSc2d1	intenzivní
výzkumu	výzkum	k1gInSc2	výzkum
předních	přední	k2eAgMnPc2d1	přední
světových	světový	k2eAgMnPc2d1	světový
výrobců	výrobce	k1gMnPc2	výrobce
motorů	motor	k1gInPc2	motor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
však	však	k9	však
většina	většina	k1gFnSc1	většina
vodíku	vodík	k1gInSc2	vodík
získávána	získávat	k5eAaImNgFnS	získávat
z	z	k7c2	z
fosilních	fosilní	k2eAgNnPc2d1	fosilní
paliv	palivo	k1gNnPc2	palivo
<g/>
,	,	kIx,	,
a	a	k8xC	a
vodík	vodík	k1gInSc1	vodík
jako	jako	k8xS	jako
mezistupeň	mezistupeň	k1gInSc1	mezistupeň
snižuje	snižovat	k5eAaImIp3nS	snižovat
účinnost	účinnost	k1gFnSc4	účinnost
jejich	jejich	k3xOp3gNnSc2	jejich
využití	využití	k1gNnSc2	využití
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
výhodný	výhodný	k2eAgInSc4d1	výhodný
poměr	poměr	k1gInSc4	poměr
chemická	chemický	k2eAgFnSc1d1	chemická
energie	energie	k1gFnSc1	energie
<g/>
/	/	kIx~	/
<g/>
hmotnost	hmotnost	k1gFnSc1	hmotnost
je	být	k5eAaImIp3nS	být
vodík	vodík	k1gInSc1	vodík
používán	používat	k5eAaImNgInS	používat
jako	jako	k8xC	jako
raketové	raketový	k2eAgNnSc1d1	raketové
palivo	palivo	k1gNnSc1	palivo
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
pro	pro	k7c4	pro
raketoplán	raketoplán	k1gInSc4	raketoplán
<g/>
)	)	kIx)	)
Zdokonalení	zdokonalení	k1gNnSc1	zdokonalení
a	a	k8xC	a
zlevnění	zlevnění	k1gNnSc1	zlevnění
palivového	palivový	k2eAgInSc2d1	palivový
článku	článek	k1gInSc2	článek
postupně	postupně	k6eAd1	postupně
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
jeho	jeho	k3xOp3gNnSc4	jeho
širší	široký	k2eAgNnSc4d2	širší
nasazení	nasazení	k1gNnSc4	nasazení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
energetickém	energetický	k2eAgNnSc6d1	energetické
zařízení	zařízení	k1gNnSc6	zařízení
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
přímé	přímý	k2eAgFnSc3d1	přímá
přeměně	přeměna	k1gFnSc3	přeměna
energie	energie	k1gFnSc2	energie
chemické	chemický	k2eAgFnSc2d1	chemická
reakce	reakce	k1gFnSc2	reakce
vodíku	vodík	k1gInSc2	vodík
s	s	k7c7	s
kyslíkem	kyslík	k1gInSc7	kyslík
na	na	k7c4	na
elektrickou	elektrický	k2eAgFnSc4d1	elektrická
energii	energie	k1gFnSc4	energie
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
paliva	palivo	k1gNnPc1	palivo
se	se	k3xPyFc4	se
přitom	přitom	k6eAd1	přitom
používá	používat	k5eAaImIp3nS	používat
plynného	plynný	k2eAgInSc2d1	plynný
vodíku	vodík	k1gInSc2	vodík
<g/>
,	,	kIx,	,
kyslík	kyslík	k1gInSc1	kyslík
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
článků	článek	k1gInPc2	článek
dodáván	dodávat	k5eAaImNgInS	dodávat
z	z	k7c2	z
atmosféry	atmosféra	k1gFnSc2	atmosféra
jako	jako	k8xC	jako
při	při	k7c6	při
normálním	normální	k2eAgNnSc6d1	normální
hoření	hoření	k1gNnSc6	hoření
<g/>
.	.	kIx.	.
</s>
<s>
Účinnost	účinnost	k1gFnSc1	účinnost
tohoto	tento	k3xDgInSc2	tento
procesu	proces	k1gInSc2	proces
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
hodnoty	hodnota	k1gFnSc2	hodnota
60	[number]	k4	60
%	%	kIx~	%
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
podstatně	podstatně	k6eAd1	podstatně
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
než	než	k8xS	než
při	při	k7c6	při
spalování	spalování	k1gNnSc6	spalování
vodíku	vodík	k1gInSc2	vodík
a	a	k8xC	a
následným	následný	k2eAgNnSc7d1	následné
využitím	využití	k1gNnSc7	využití
vzniklého	vzniklý	k2eAgNnSc2d1	vzniklé
tepla	teplo	k1gNnSc2	teplo
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
současných	současný	k2eAgInPc2d1	současný
palivových	palivový	k2eAgInPc2d1	palivový
článků	článek	k1gInPc2	článek
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
ještě	ještě	k9	ještě
jejich	jejich	k3xOp3gFnSc1	jejich
vysoká	vysoký	k2eAgFnSc1d1	vysoká
cena	cena	k1gFnSc1	cena
a	a	k8xC	a
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
proces	proces	k1gInSc1	proces
je	být	k5eAaImIp3nS	být
doposud	doposud	k6eAd1	doposud
značně	značně	k6eAd1	značně
citlivý	citlivý	k2eAgMnSc1d1	citlivý
vůči	vůči	k7c3	vůči
katalytickým	katalytický	k2eAgInPc3d1	katalytický
jedům	jed	k1gInPc3	jed
a	a	k8xC	a
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
proto	proto	k8xC	proto
použití	použití	k1gNnSc1	použití
velmi	velmi	k6eAd1	velmi
čistých	čistý	k2eAgFnPc2d1	čistá
chemikálií	chemikálie	k1gFnPc2	chemikálie
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
palivové	palivový	k2eAgInPc1d1	palivový
články	článek	k1gInPc1	článek
od	od	k7c2	od
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
využívají	využívat	k5eAaImIp3nP	využívat
především	především	k9	především
v	v	k7c6	v
kosmických	kosmický	k2eAgFnPc6d1	kosmická
technologiích	technologie	k1gFnPc6	technologie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
uvedené	uvedený	k2eAgFnPc4d1	uvedená
nevýhody	nevýhoda	k1gFnPc4	nevýhoda
nejsou	být	k5eNaImIp3nP	být
příliš	příliš	k6eAd1	příliš
významné	významný	k2eAgFnPc1d1	významná
<g/>
.	.	kIx.	.
</s>
<s>
Perspektivně	perspektivně	k6eAd1	perspektivně
jsou	být	k5eAaImIp3nP	být
izotopy	izotop	k1gInPc1	izotop
vodíku	vodík	k1gInSc2	vodík
pokládány	pokládán	k2eAgInPc1d1	pokládán
za	za	k7c4	za
hlavní	hlavní	k2eAgInSc4d1	hlavní
energetický	energetický	k2eAgInSc4d1	energetický
zdroj	zdroj	k1gInSc4	zdroj
při	při	k7c6	při
využití	využití	k1gNnSc6	využití
řízené	řízený	k2eAgFnSc2d1	řízená
termonukleární	termonukleární	k2eAgFnSc2d1	termonukleární
fúze	fúze	k1gFnSc2	fúze
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
lze	lze	k6eAd1	lze
slučováním	slučování	k1gNnSc7	slučování
lehkých	lehký	k2eAgNnPc2d1	lehké
atomových	atomový	k2eAgNnPc2d1	atomové
jader	jádro	k1gNnPc2	jádro
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
významného	významný	k2eAgInSc2d1	významný
energetického	energetický	k2eAgInSc2d1	energetický
zisku	zisk	k1gInSc2	zisk
<g/>
.	.	kIx.	.
</s>
<s>
Jaderná	jaderný	k2eAgFnSc1d1	jaderná
fúze	fúze	k1gFnSc1	fúze
však	však	k9	však
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
ve	v	k7c6	v
stádiu	stádium	k1gNnSc6	stádium
experimentálních	experimentální	k2eAgInPc2d1	experimentální
prototypů	prototyp	k1gInPc2	prototyp
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc4	jejich
zavedení	zavedení	k1gNnSc4	zavedení
do	do	k7c2	do
praxe	praxe	k1gFnSc2	praxe
lze	lze	k6eAd1	lze
očekávat	očekávat	k5eAaImF	očekávat
v	v	k7c6	v
horizontu	horizont	k1gInSc6	horizont
několika	několik	k4yIc2	několik
desítek	desítka	k1gFnPc2	desítka
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
začala	začít	k5eAaPmAgFnS	začít
probíhat	probíhat	k5eAaImF	probíhat
výstavba	výstavba	k1gFnSc1	výstavba
termonukleárního	termonukleární	k2eAgInSc2d1	termonukleární
reaktoru	reaktor	k1gInSc2	reaktor
ITER	ITER	kA	ITER
ve	v	k7c6	v
francouzském	francouzský	k2eAgNnSc6d1	francouzské
Cadarache	Cadarache	k1gNnSc6	Cadarache
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Praktické	praktický	k2eAgNnSc1d1	praktické
využití	využití	k1gNnSc1	využití
jaderné	jaderný	k2eAgFnSc2d1	jaderná
fúze	fúze	k1gFnSc2	fúze
se	se	k3xPyFc4	se
doposud	doposud	k6eAd1	doposud
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
pouze	pouze	k6eAd1	pouze
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
termonukleární	termonukleární	k2eAgFnSc2d1	termonukleární
bomby	bomba	k1gFnSc2	bomba
<g/>
.	.	kIx.	.
</s>
<s>
Hoření	hoření	k1gNnSc1	hoření
vodíku	vodík	k1gInSc2	vodík
s	s	k7c7	s
kyslíkem	kyslík	k1gInSc7	kyslík
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
exotermní	exotermní	k2eAgFnSc1d1	exotermní
a	a	k8xC	a
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
teploty	teplota	k1gFnPc4	teplota
přes	přes	k7c4	přes
3	[number]	k4	3
000	[number]	k4	000
°	°	k?	°
<g/>
C.	C.	kA	C.
Toho	ten	k3xDgMnSc2	ten
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
využívá	využívat	k5eAaPmIp3nS	využívat
při	při	k7c6	při
svařování	svařování	k1gNnSc1	svařování
nebo	nebo	k8xC	nebo
řezání	řezání	k1gNnSc1	řezání
kyslíko-vodíkovým	kyslíkoodíkův	k2eAgInSc7d1	kyslíko-vodíkův
plamenem	plamen	k1gInSc7	plamen
nebo	nebo	k8xC	nebo
v	v	k7c6	v
metalurgii	metalurgie	k1gFnSc6	metalurgie
při	při	k7c6	při
zpracování	zpracování	k1gNnSc6	zpracování
těžko	těžko	k6eAd1	těžko
tavitelných	tavitelný	k2eAgInPc2d1	tavitelný
kovů	kov	k1gInPc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
Vodík	vodík	k1gInSc1	vodík
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
chladivo	chladivo	k1gNnSc1	chladivo
alternátorů	alternátor	k1gInPc2	alternátor
v	v	k7c6	v
elektrárnách	elektrárna	k1gFnPc6	elektrárna
Mimořádně	mimořádně	k6eAd1	mimořádně
nízké	nízký	k2eAgFnPc1d1	nízká
hustoty	hustota	k1gFnPc1	hustota
plynného	plynný	k2eAgInSc2d1	plynný
vodíku	vodík	k1gInSc2	vodík
se	se	k3xPyFc4	se
využívalo	využívat	k5eAaPmAgNnS	využívat
v	v	k7c6	v
počátcích	počátek	k1gInPc6	počátek
letectví	letectví	k1gNnSc2	letectví
k	k	k7c3	k
plnění	plnění	k1gNnSc3	plnění
vzducholodí	vzducholoď	k1gFnPc2	vzducholoď
a	a	k8xC	a
balónů	balón	k1gInPc2	balón
<g/>
.	.	kIx.	.
</s>
<s>
Náhrada	náhrada	k1gFnSc1	náhrada
výbušného	výbušný	k2eAgInSc2d1	výbušný
vodíku	vodík	k1gInSc2	vodík
inertním	inertní	k2eAgNnSc7d1	inertní
heliem	helium	k1gNnSc7	helium
byla	být	k5eAaImAgFnS	být
prakticky	prakticky	k6eAd1	prakticky
využitelná	využitelný	k2eAgFnSc1d1	využitelná
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
s	s	k7c7	s
přírodními	přírodní	k2eAgInPc7d1	přírodní
zdroji	zdroj	k1gInPc7	zdroj
podzemního	podzemní	k2eAgNnSc2d1	podzemní
helia	helium	k1gNnSc2	helium
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
bylo	být	k5eAaImAgNnS	být
helium	helium	k1gNnSc1	helium
embargováno	embargovat	k5eAaImNgNnS	embargovat
pro	pro	k7c4	pro
vývoz	vývoz	k1gInSc4	vývoz
do	do	k7c2	do
nacistického	nacistický	k2eAgNnSc2d1	nacistické
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1937	[number]	k4	1937
vzducholoď	vzducholoď	k1gFnSc1	vzducholoď
Hindenburg	Hindenburg	k1gInSc1	Hindenburg
shořela	shořet	k5eAaPmAgFnS	shořet
při	při	k7c6	při
přistání	přistání	k1gNnSc6	přistání
s	s	k7c7	s
několika	několik	k4yIc7	několik
desítkami	desítka	k1gFnPc7	desítka
obětí	oběť	k1gFnPc2	oběť
<g/>
,	,	kIx,	,
éru	éra	k1gFnSc4	éra
vodíkem	vodík	k1gInSc7	vodík
plněných	plněný	k2eAgInPc2d1	plněný
dopravních	dopravní	k2eAgInPc2d1	dopravní
prostředků	prostředek	k1gInPc2	prostředek
lehčích	lehký	k2eAgNnPc2d2	lehčí
než	než	k8xS	než
vzduch	vzduch	k1gInSc4	vzduch
definitivně	definitivně	k6eAd1	definitivně
ukončila	ukončit	k5eAaPmAgFnS	ukončit
<g/>
.	.	kIx.	.
</s>
<s>
Příčinou	příčina	k1gFnSc7	příčina
exploze	exploze	k1gFnSc2	exploze
vzducholodě	vzducholoď	k1gFnSc2	vzducholoď
Hindenburg	Hindenburg	k1gInSc1	Hindenburg
byla	být	k5eAaImAgFnS	být
elektrická	elektrický	k2eAgFnSc1d1	elektrická
jiskra	jiskra	k1gFnSc1	jiskra
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k8xC	jak
vzducholoď	vzducholoď	k1gFnSc1	vzducholoď
"	"	kIx"	"
<g/>
pluje	plout	k5eAaImIp3nS	plout
<g/>
"	"	kIx"	"
ve	v	k7c6	v
vzduchu	vzduch	k1gInSc6	vzduch
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
plášť	plášť	k1gInSc1	plášť
vzducholodě	vzducholoď	k1gFnSc2	vzducholoď
tře	třít	k5eAaImIp3nS	třít
o	o	k7c4	o
okolní	okolní	k2eAgInSc4d1	okolní
vzduch	vzduch	k1gInSc4	vzduch
a	a	k8xC	a
dochází	docházet	k5eAaImIp3nS	docházet
takto	takto	k6eAd1	takto
ke	k	k7c3	k
elektrostatickému	elektrostatický	k2eAgNnSc3d1	elektrostatické
nabití	nabití	k1gNnSc3	nabití
balonu	balon	k1gInSc2	balon
vzducholodě	vzducholoď	k1gFnSc2	vzducholoď
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
historickém	historický	k2eAgInSc6d1	historický
případě	případ	k1gInSc6	případ
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
kombinaci	kombinace	k1gFnSc4	kombinace
počasí	počasí	k1gNnSc2	počasí
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
přistání	přistání	k1gNnSc2	přistání
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
před	před	k7c7	před
bouřkou	bouřka	k1gFnSc7	bouřka
<g/>
,	,	kIx,	,
a	a	k8xC	a
přetření	přetření	k1gNnSc1	přetření
povrchu	povrch	k1gInSc2	povrch
vzducholodi	vzducholoď	k1gFnSc2	vzducholoď
nevhodným	vhodný	k2eNgInSc7d1	nevhodný
nátěrem	nátěr	k1gInSc7	nátěr
zvyšujícím	zvyšující	k2eAgInSc7d1	zvyšující
akumulaci	akumulace	k1gFnSc4	akumulace
elektrostatického	elektrostatický	k2eAgInSc2d1	elektrostatický
náboje	náboj	k1gInSc2	náboj
<g/>
.	.	kIx.	.
</s>
<s>
Stačila	stačit	k5eAaBmAgFnS	stačit
pak	pak	k6eAd1	pak
jediná	jediný	k2eAgFnSc1d1	jediná
jiskra	jiskra	k1gFnSc1	jiskra
<g/>
,	,	kIx,	,
obal	obal	k1gInSc1	obal
se	se	k3xPyFc4	se
vzňal	vznít	k5eAaPmAgInS	vznít
<g/>
,	,	kIx,	,
od	od	k7c2	od
obalu	obal	k1gInSc2	obal
se	se	k3xPyFc4	se
propálily	propálit	k5eAaPmAgInP	propálit
vnitřní	vnitřní	k2eAgInPc1d1	vnitřní
balony	balon	k1gInPc1	balon
s	s	k7c7	s
vodíkem	vodík	k1gInSc7	vodík
<g/>
,	,	kIx,	,
a	a	k8xC	a
katastrofa	katastrofa	k1gFnSc1	katastrofa
propukla	propuknout	k5eAaPmAgFnS	propuknout
naplno	naplno	k6eAd1	naplno
<g/>
.	.	kIx.	.
</s>
<s>
Nízké	nízký	k2eAgFnPc1d1	nízká
hustoty	hustota	k1gFnPc1	hustota
a	a	k8xC	a
nízké	nízký	k2eAgFnPc1d1	nízká
viskozity	viskozita	k1gFnPc1	viskozita
vodíku	vodík	k1gInSc2	vodík
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
pro	pro	k7c4	pro
snížení	snížení	k1gNnSc4	snížení
tření	tření	k1gNnSc2	tření
ve	v	k7c6	v
strojích	stroj	k1gInPc6	stroj
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
rychle	rychle	k6eAd1	rychle
proudící	proudící	k2eAgNnSc4d1	proudící
plynné	plynný	k2eAgNnSc4d1	plynné
médium	médium	k1gNnSc4	médium
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
jsou	být	k5eAaImIp3nP	být
elektrické	elektrický	k2eAgInPc1d1	elektrický
generátory	generátor	k1gInPc1	generátor
(	(	kIx(	(
<g/>
kde	kde	k6eAd1	kde
může	moct	k5eAaImIp3nS	moct
tvořit	tvořit	k5eAaImF	tvořit
náplň	náplň	k1gFnSc4	náplň
skříně	skříň	k1gFnSc2	skříň
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Stirlingův	Stirlingův	k2eAgInSc1d1	Stirlingův
motor	motor	k1gInSc1	motor
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
pracovní	pracovní	k2eAgNnSc1d1	pracovní
médium	médium	k1gNnSc1	médium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vodíku	vodík	k1gInSc3	vodík
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
využívá	využívat	k5eAaImIp3nS	využívat
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
amoniaku	amoniak	k1gInSc2	amoniak
z	z	k7c2	z
prvků	prvek	k1gInPc2	prvek
-	-	kIx~	-
dusíku	dusík	k1gInSc2	dusík
a	a	k8xC	a
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Reakce	reakce	k1gFnSc1	reakce
probíhá	probíhat	k5eAaImIp3nS	probíhat
za	za	k7c4	za
teploty	teplota	k1gFnPc4	teplota
okolo	okolo	k7c2	okolo
500	[number]	k4	500
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
tlaku	tlak	k1gInSc2	tlak
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
100	[number]	k4	100
MPa	MPa	k1gFnPc2	MPa
a	a	k8xC	a
katalyzátoru	katalyzátor	k1gInSc2	katalyzátor
aktivovaného	aktivovaný	k2eAgNnSc2d1	aktivované
železa	železo	k1gNnSc2	železo
(	(	kIx(	(
<g/>
železo	železo	k1gNnSc1	železo
je	být	k5eAaImIp3nS	být
aktivované	aktivovaný	k2eAgNnSc1d1	aktivované
oxidem	oxid	k1gInSc7	oxid
hlinitým	hlinitý	k2eAgFnPc3d1	hlinitá
Al	ala	k1gFnPc2	ala
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
3	[number]	k4	3
nebo	nebo	k8xC	nebo
oxidem	oxid	k1gInSc7	oxid
draselným	draselný	k2eAgInSc7d1	draselný
K	k	k7c3	k
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Amoniak	amoniak	k1gInSc1	amoniak
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
nejpoužívanějším	používaný	k2eAgNnSc7d3	nejpoužívanější
rostlinným	rostlinný	k2eAgNnSc7d1	rostlinné
hnojivem	hnojivo	k1gNnSc7	hnojivo
<g/>
.	.	kIx.	.
</s>
<s>
N2	N2	k4	N2
+	+	kIx~	+
3	[number]	k4	3
H2	H2	k1gFnSc2	H2
→	→	k?	→
2	[number]	k4	2
NH3	NH3	k1gFnSc1	NH3
Reakcí	reakce	k1gFnPc2	reakce
vodíku	vodík	k1gInSc2	vodík
s	s	k7c7	s
chlorem	chlor	k1gInSc7	chlor
vzniká	vznikat	k5eAaImIp3nS	vznikat
chlorovodík	chlorovodík	k1gInSc1	chlorovodík
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
pak	pak	k6eAd1	pak
zavádíme	zavádět	k5eAaImIp1nP	zavádět
do	do	k7c2	do
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
vzniká	vznikat	k5eAaImIp3nS	vznikat
kyselina	kyselina	k1gFnSc1	kyselina
chlorovodíková	chlorovodíkový	k2eAgFnSc1d1	chlorovodíková
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
průmyslu	průmysl	k1gInSc6	průmysl
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
mnoha	mnoho	k4c3	mnoho
reakcím	reakce	k1gFnPc3	reakce
a	a	k8xC	a
syntézám	syntéza	k1gFnPc3	syntéza
<g/>
.	.	kIx.	.
</s>
<s>
H2	H2	k4	H2
+	+	kIx~	+
Cl	Cl	k1gFnSc1	Cl
<g/>
2	[number]	k4	2
→	→	k?	→
2	[number]	k4	2
HCl	HCl	k1gFnPc2	HCl
Experimentálně	experimentálně	k6eAd1	experimentálně
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
jako	jako	k9	jako
fyziologicky	fyziologicky	k6eAd1	fyziologicky
inertní	inertní	k2eAgInSc1d1	inertní
dýchací	dýchací	k2eAgInSc1d1	dýchací
plyn	plyn	k1gInSc1	plyn
ve	v	k7c6	v
směsích	směs	k1gFnPc6	směs
pro	pro	k7c4	pro
extrémní	extrémní	k2eAgFnPc4d1	extrémní
hloubkové	hloubkový	k2eAgFnPc4d1	hloubková
potápění	potápění	k1gNnPc4	potápění
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
výhodou	výhoda	k1gFnSc7	výhoda
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
nízká	nízký	k2eAgFnSc1d1	nízká
hustota	hustota	k1gFnSc1	hustota
a	a	k8xC	a
absence	absence	k1gFnSc1	absence
HPNS	HPNS	kA	HPNS
(	(	kIx(	(
<g/>
nervový	nervový	k2eAgInSc4d1	nervový
syndrom	syndrom	k1gInSc4	syndrom
vysokého	vysoký	k2eAgInSc2d1	vysoký
tlaku	tlak	k1gInSc2	tlak
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
vysoké	vysoký	k2eAgFnSc3d1	vysoká
reaktivitě	reaktivita	k1gFnSc3	reaktivita
vodíku	vodík	k1gInSc2	vodík
s	s	k7c7	s
kyslíkem	kyslík	k1gInSc7	kyslík
jsou	být	k5eAaImIp3nP	být
při	při	k7c6	při
potápění	potápění	k1gNnSc2	potápění
používány	používán	k2eAgFnPc4d1	používána
směsi	směs	k1gFnPc4	směs
s	s	k7c7	s
maximálním	maximální	k2eAgInSc7d1	maximální
obsahem	obsah	k1gInSc7	obsah
kyslíku	kyslík	k1gInSc2	kyslík
4	[number]	k4	4
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
je	být	k5eAaImIp3nS	být
směs	směs	k1gFnSc1	směs
bezpečně	bezpečně	k6eAd1	bezpečně
dýchatelná	dýchatelný	k2eAgFnSc1d1	dýchatelná
teprve	teprve	k6eAd1	teprve
od	od	k7c2	od
hloubky	hloubka	k1gFnSc2	hloubka
30	[number]	k4	30
m.	m.	k?	m.
Jako	jako	k8xS	jako
první	první	k4xOgInSc1	první
použil	použít	k5eAaPmAgInS	použít
vodík	vodík	k1gInSc4	vodík
v	v	k7c6	v
dýchací	dýchací	k2eAgFnSc6d1	dýchací
směsi	směs	k1gFnSc6	směs
Švéd	Švéd	k1gMnSc1	Švéd
Arne	Arne	k1gMnSc1	Arne
Zetterström	Zetterström	k1gMnSc1	Zetterström
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
bylo	být	k5eAaImAgNnS	být
provedeno	provést	k5eAaPmNgNnS	provést
několik	několik	k4yIc4	několik
výzkumných	výzkumný	k2eAgInPc2d1	výzkumný
projektů	projekt	k1gInPc2	projekt
(	(	kIx(	(
<g/>
např.	např.	kA	např.
HYDRA	hydra	k1gFnSc1	hydra
5	[number]	k4	5
<g/>
,	,	kIx,	,
HYDRA	hydra	k1gFnSc1	hydra
8	[number]	k4	8
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dokazujících	dokazující	k2eAgFnPc2d1	dokazující
použitelnost	použitelnost	k1gFnSc4	použitelnost
vodíkových	vodíkový	k2eAgFnPc2d1	vodíková
směsí	směs	k1gFnPc2	směs
v	v	k7c6	v
hloubkách	hloubka	k1gFnPc6	hloubka
400	[number]	k4	400
<g/>
-	-	kIx~	-
<g/>
600	[number]	k4	600
m.	m.	k?	m.
Dýchací	dýchací	k2eAgFnSc1d1	dýchací
směs	směs	k1gFnSc1	směs
vodíku	vodík	k1gInSc2	vodík
a	a	k8xC	a
kyslíku	kyslík	k1gInSc2	kyslík
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
HYDROX	HYDROX	kA	HYDROX
a	a	k8xC	a
směs	směs	k1gFnSc1	směs
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
,	,	kIx,	,
vodíku	vodík	k1gInSc2	vodík
a	a	k8xC	a
helia	helium	k1gNnSc2	helium
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
HYDRELIOX	HYDRELIOX	kA	HYDRELIOX
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
využití	využití	k1gNnSc1	využití
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
strojích	stroj	k1gInPc6	stroj
TOKAMAK	TOKAMAK	kA	TOKAMAK
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
napodobit	napodobit	k5eAaPmF	napodobit
reakci	reakce	k1gFnSc4	reakce
probíhající	probíhající	k2eAgFnSc4d1	probíhající
ve	v	k7c6	v
Slunci	slunce	k1gNnSc6	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Hydridy	hydrid	k1gInPc1	hydrid
jsou	být	k5eAaImIp3nP	být
obecně	obecně	k6eAd1	obecně
všechny	všechen	k3xTgFnPc1	všechen
dvouprvkové	dvouprvkový	k2eAgFnPc1d1	dvouprvková
sloučeniny	sloučenina	k1gFnPc1	sloučenina
vodíku	vodík	k1gInSc2	vodík
s	s	k7c7	s
prvky	prvek	k1gInPc7	prvek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
užším	úzký	k2eAgInSc6d2	užší
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
se	se	k3xPyFc4	se
jako	jako	k9	jako
hydridy	hydrid	k1gInPc1	hydrid
označují	označovat	k5eAaImIp3nP	označovat
pouze	pouze	k6eAd1	pouze
dvouprvkové	dvouprvkový	k2eAgFnPc1d1	dvouprvková
sloučeniny	sloučenina	k1gFnPc1	sloučenina
vodíku	vodík	k1gInSc2	vodík
s	s	k7c7	s
alkalickými	alkalický	k2eAgInPc7d1	alkalický
kovy	kov	k1gInPc7	kov
a	a	k8xC	a
kovy	kov	k1gInPc1	kov
alkalických	alkalický	k2eAgFnPc2d1	alkalická
zemin	zemina	k1gFnPc2	zemina
<g/>
.	.	kIx.	.
</s>
<s>
Hydridy	hydrid	k1gInPc1	hydrid
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c4	na
iontové	iontový	k2eAgFnPc4d1	iontová
<g/>
,	,	kIx,	,
kovalentní	kovalentní	k2eAgFnPc4d1	kovalentní
a	a	k8xC	a
kovové	kovový	k2eAgFnPc4d1	kovová
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
nemají	mít	k5eNaImIp3nP	mít
vždy	vždy	k6eAd1	vždy
pravidelnou	pravidelný	k2eAgFnSc4d1	pravidelná
strukturu	struktura	k1gFnSc4	struktura
a	a	k8xC	a
pevné	pevný	k2eAgNnSc4d1	pevné
stechiometrické	stechiometrický	k2eAgNnSc4d1	stechiometrické
složení	složení	k1gNnSc4	složení
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
ještě	ještě	k6eAd1	ještě
kovové	kovový	k2eAgInPc1d1	kovový
hydridy	hydrid	k1gInPc1	hydrid
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c4	na
kovové	kovový	k2eAgFnPc4d1	kovová
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
pravidelnou	pravidelný	k2eAgFnSc4d1	pravidelná
strukturu	struktura	k1gFnSc4	struktura
a	a	k8xC	a
stechiometrické	stechiometrický	k2eAgNnSc4d1	stechiometrické
složení	složení	k1gNnSc4	složení
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
to	ten	k3xDgNnSc4	ten
jsou	být	k5eAaImIp3nP	být
práškovité	práškovitý	k2eAgFnPc1d1	práškovitá
látky	látka	k1gFnPc1	látka
s	s	k7c7	s
černou	černý	k2eAgFnSc7d1	černá
barvou	barva	k1gFnSc7	barva
<g/>
,	,	kIx,	,
a	a	k8xC	a
hydridy	hydrid	k1gInPc4	hydrid
přechodného	přechodný	k2eAgInSc2d1	přechodný
typu	typ	k1gInSc2	typ
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
mají	mít	k5eAaImIp3nP	mít
proměnlivé	proměnlivý	k2eAgNnSc4d1	proměnlivé
složení	složení	k1gNnSc4	složení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
tlaku	tlak	k1gInSc6	tlak
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Kovalentní	kovalentní	k2eAgInPc1d1	kovalentní
hydridy	hydrid	k1gInPc1	hydrid
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c6	na
molekulové	molekulový	k2eAgFnSc6d1	molekulová
a	a	k8xC	a
polymerní	polymerní	k2eAgFnSc6d1	polymerní
<g/>
.	.	kIx.	.
</s>
<s>
Hydrid	hydrid	k1gInSc1	hydrid
sodný	sodný	k2eAgInSc1d1	sodný
NaH	naho	k1gNnPc2	naho
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
teplotou	teplota	k1gFnSc7	teplota
tání	tání	k1gNnSc2	tání
a	a	k8xC	a
varu	var	k1gInSc2	var
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
využít	využít	k5eAaPmF	využít
jako	jako	k9	jako
velmi	velmi	k6eAd1	velmi
silné	silný	k2eAgNnSc4d1	silné
redukční	redukční	k2eAgNnSc4d1	redukční
činidlo	činidlo	k1gNnSc4	činidlo
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
zahříváním	zahřívání	k1gNnSc7	zahřívání
sodíku	sodík	k1gInSc2	sodík
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
vodou	voda	k1gFnSc7	voda
reaguje	reagovat	k5eAaBmIp3nS	reagovat
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
hydroxidu	hydroxid	k1gInSc2	hydroxid
sodného	sodný	k2eAgInSc2d1	sodný
a	a	k8xC	a
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
iontový	iontový	k2eAgInSc1d1	iontový
hydrid	hydrid	k1gInSc1	hydrid
<g/>
.	.	kIx.	.
</s>
<s>
Hydrid	hydrid	k1gInSc4	hydrid
vápenatý	vápenatý	k2eAgInSc4d1	vápenatý
CaH	CaH	k1gFnSc7	CaH
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
teplotou	teplota	k1gFnSc7	teplota
tání	tání	k1gNnSc2	tání
a	a	k8xC	a
varu	var	k1gInSc2	var
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
využít	využít	k5eAaPmF	využít
jako	jako	k9	jako
velmi	velmi	k6eAd1	velmi
silné	silný	k2eAgNnSc4d1	silné
redukční	redukční	k2eAgNnSc4d1	redukční
činidlo	činidlo	k1gNnSc4	činidlo
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
zahříváním	zahřívání	k1gNnSc7	zahřívání
vápníku	vápník	k1gInSc2	vápník
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
vodou	voda	k1gFnSc7	voda
reaguje	reagovat	k5eAaBmIp3nS	reagovat
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
hydroxidu	hydroxid	k1gInSc2	hydroxid
vápenatého	vápenatý	k2eAgInSc2d1	vápenatý
a	a	k8xC	a
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
iontový	iontový	k2eAgInSc1d1	iontový
hydrid	hydrid	k1gInSc1	hydrid
<g/>
.	.	kIx.	.
</s>
<s>
Amoniak	amoniak	k1gInSc1	amoniak
<g/>
,	,	kIx,	,
čpavek	čpavek	k1gInSc1	čpavek
neboli	neboli	k8xC	neboli
azan	azan	k1gInSc1	azan
NH3	NH3	k1gFnSc2	NH3
je	být	k5eAaImIp3nS	být
bezbarvý	bezbarvý	k2eAgInSc1d1	bezbarvý
plyn	plyn	k1gInSc1	plyn
nepříjemné	příjemný	k2eNgFnSc2d1	nepříjemná
chuti	chuť	k1gFnSc2	chuť
a	a	k8xC	a
čpavého	čpavý	k2eAgInSc2d1	čpavý
zápachu	zápach	k1gInSc2	zápach
<g/>
.	.	kIx.	.
</s>
<s>
Dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
lehce	lehko	k6eAd1	lehko
zkapalnit	zkapalnit	k5eAaPmF	zkapalnit
a	a	k8xC	a
v	v	k7c6	v
laboratoři	laboratoř	k1gFnSc6	laboratoř
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
polární	polární	k2eAgNnSc1d1	polární
rozpouštědlo	rozpouštědlo	k1gNnSc1	rozpouštědlo
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
reakcí	reakce	k1gFnSc7	reakce
dusíku	dusík	k1gInSc2	dusík
s	s	k7c7	s
vodíkem	vodík	k1gInSc7	vodík
za	za	k7c2	za
vyšší	vysoký	k2eAgFnSc2d2	vyšší
teploty	teplota	k1gFnSc2	teplota
a	a	k8xC	a
vysokého	vysoký	k2eAgInSc2d1	vysoký
tlaku	tlak	k1gInSc2	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
kovalentní	kovalentní	k2eAgInSc1d1	kovalentní
hydrid	hydrid	k1gInSc1	hydrid
<g/>
.	.	kIx.	.
</s>
<s>
Hydrazin	hydrazin	k1gInSc1	hydrazin
je	být	k5eAaImIp3nS	být
další	další	k2eAgFnSc7d1	další
sloučeninou	sloučenina	k1gFnSc7	sloučenina
vodíku	vodík	k1gInSc2	vodík
s	s	k7c7	s
dusíkem	dusík	k1gInSc7	dusík
N	N	kA	N
<g/>
2	[number]	k4	2
<g/>
H	H	kA	H
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Voda	voda	k1gFnSc1	voda
(	(	kIx(	(
<g/>
v	v	k7c6	v
systematickém	systematický	k2eAgNnSc6d1	systematické
názvosloví	názvosloví	k1gNnSc6	názvosloví
oxidan	oxidana	k1gFnPc2	oxidana
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
oxan	oxan	k1gInSc1	oxan
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
systematický	systematický	k2eAgInSc1d1	systematický
název	název	k1gInSc1	název
pro	pro	k7c4	pro
tetrahydropyran	tetrahydropyran	k1gInSc4	tetrahydropyran
<g/>
)	)	kIx)	)
H2O	H2O	k1gFnSc1	H2O
je	být	k5eAaImIp3nS	být
bezbarvá	bezbarvý	k2eAgFnSc1d1	bezbarvá
kapalina	kapalina	k1gFnSc1	kapalina
bez	bez	k7c2	bez
chuti	chuť	k1gFnSc2	chuť
a	a	k8xC	a
vůně	vůně	k1gFnSc2	vůně
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nejběžnější	běžný	k2eAgNnSc1d3	nejběžnější
polární	polární	k2eAgNnSc1d1	polární
rozpouštědlo	rozpouštědlo	k1gNnSc1	rozpouštědlo
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
reakcí	reakce	k1gFnSc7	reakce
vodíku	vodík	k1gInSc2	vodík
s	s	k7c7	s
kyslíkem	kyslík	k1gInSc7	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
kovalentní	kovalentní	k2eAgInSc1d1	kovalentní
hydrid	hydrid	k1gInSc1	hydrid
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
sloučeninou	sloučenina	k1gFnSc7	sloučenina
těchto	tento	k3xDgInPc2	tento
prvků	prvek	k1gInPc2	prvek
je	být	k5eAaImIp3nS	být
peroxid	peroxid	k1gInSc1	peroxid
vodíku	vodík	k1gInSc2	vodík
<g/>
,	,	kIx,	,
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
látka	látka	k1gFnSc1	látka
se	s	k7c7	s
silnými	silný	k2eAgInPc7d1	silný
oxidačními	oxidační	k2eAgInPc7d1	oxidační
účinky	účinek	k1gInPc7	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Sulfan	Sulfan	k1gMnSc1	Sulfan
H2S	H2S	k1gMnSc1	H2S
je	být	k5eAaImIp3nS	být
bezbarvý	bezbarvý	k2eAgInSc4d1	bezbarvý
plyn	plyn	k1gInSc4	plyn
s	s	k7c7	s
nakyslou	nakyslý	k2eAgFnSc7d1	nakyslá
chutí	chuť	k1gFnSc7	chuť
a	a	k8xC	a
vůní	vůně	k1gFnSc7	vůně
po	po	k7c6	po
zkažených	zkažený	k2eAgNnPc6d1	zkažené
vejcích	vejce	k1gNnPc6	vejce
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
extrémně	extrémně	k6eAd1	extrémně
jedovatý	jedovatý	k2eAgMnSc1d1	jedovatý
-	-	kIx~	-
0,015	[number]	k4	0,015
<g/>
%	%	kIx~	%
ve	v	k7c6	v
vzduchu	vzduch	k1gInSc6	vzduch
dokáže	dokázat	k5eAaPmIp3nS	dokázat
usmrtit	usmrtit	k5eAaPmF	usmrtit
člověka	člověk	k1gMnSc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
lehce	lehko	k6eAd1	lehko
rozpustný	rozpustný	k2eAgMnSc1d1	rozpustný
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
slabě	slabě	k6eAd1	slabě
kyselého	kyselý	k2eAgNnSc2d1	kyselé
prostředí	prostředí	k1gNnSc2	prostředí
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
vodný	vodný	k2eAgInSc1d1	vodný
roztok	roztok	k1gInSc1	roztok
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
analytické	analytický	k2eAgFnSc6d1	analytická
chemii	chemie	k1gFnSc6	chemie
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
sirovodíková	sirovodíkový	k2eAgFnSc1d1	sirovodíková
voda	voda	k1gFnSc1	voda
jako	jako	k8xC	jako
zkoumadlo	zkoumadlo	k1gNnSc1	zkoumadlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
vzniká	vznikat	k5eAaImIp3nS	vznikat
tlením	tlení	k1gNnSc7	tlení
bílkovinných	bílkovinný	k2eAgInPc2d1	bílkovinný
organismů	organismus	k1gInPc2	organismus
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
síry	síra	k1gFnSc2	síra
<g/>
.	.	kIx.	.
</s>
<s>
Průmyslově	průmyslově	k6eAd1	průmyslově
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
vytěsňováním	vytěsňování	k1gNnSc7	vytěsňování
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
solí	sůl	k1gFnPc2	sůl
silnější	silný	k2eAgFnSc7d2	silnější
kyselinou	kyselina	k1gFnSc7	kyselina
<g/>
.	.	kIx.	.
</s>
<s>
Fluorovodík	fluorovodík	k1gInSc1	fluorovodík
neboli	neboli	k8xC	neboli
fluoran	fluoran	k1gInSc1	fluoran
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
HF	HF	kA	HF
je	být	k5eAaImIp3nS	být
plyn	plyn	k1gInSc1	plyn
bez	bez	k7c2	bez
barvy	barva	k1gFnSc2	barva
<g/>
,	,	kIx,	,
s	s	k7c7	s
leptavou	leptavý	k2eAgFnSc7d1	leptavá
chutí	chuť	k1gFnSc7	chuť
a	a	k8xC	a
nepříjemným	příjemný	k2eNgInSc7d1	nepříjemný
zápachem	zápach	k1gInSc7	zápach
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
se	se	k3xPyFc4	se
chová	chovat	k5eAaImIp3nS	chovat
jako	jako	k9	jako
středně	středně	k6eAd1	středně
silná	silný	k2eAgFnSc1d1	silná
kyselina	kyselina	k1gFnSc1	kyselina
a	a	k8xC	a
z	z	k7c2	z
halogenovodíků	halogenovodík	k1gMnPc2	halogenovodík
je	být	k5eAaImIp3nS	být
nejslabší	slabý	k2eAgFnSc1d3	nejslabší
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
k	k	k7c3	k
uměleckému	umělecký	k2eAgNnSc3d1	umělecké
leptání	leptání	k1gNnSc3	leptání
skla	sklo	k1gNnSc2	sklo
a	a	k8xC	a
jako	jako	k9	jako
velmi	velmi	k6eAd1	velmi
silné	silný	k2eAgNnSc4d1	silné
oxidační	oxidační	k2eAgNnSc4d1	oxidační
činidlo	činidlo	k1gNnSc4	činidlo
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
reakcí	reakce	k1gFnSc7	reakce
vodíku	vodík	k1gInSc2	vodík
s	s	k7c7	s
fluorem	fluor	k1gInSc7	fluor
nebo	nebo	k8xC	nebo
vytěsněním	vytěsnění	k1gNnSc7	vytěsnění
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
soli	sůl	k1gFnSc2	sůl
<g/>
.	.	kIx.	.
</s>
<s>
Chlorovodík	chlorovodík	k1gInSc1	chlorovodík
neboli	neboli	k8xC	neboli
chloran	chloran	k1gInSc1	chloran
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
HCl	HCl	k1gFnSc3	HCl
je	být	k5eAaImIp3nS	být
plyn	plyn	k1gInSc1	plyn
bez	bez	k7c2	bez
barvy	barva	k1gFnSc2	barva
<g/>
,	,	kIx,	,
s	s	k7c7	s
leptavou	leptavý	k2eAgFnSc7d1	leptavá
chutí	chuť	k1gFnSc7	chuť
a	a	k8xC	a
nepříjemným	příjemný	k2eNgInSc7d1	nepříjemný
zápachem	zápach	k1gInSc7	zápach
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
se	se	k3xPyFc4	se
chová	chovat	k5eAaImIp3nS	chovat
jako	jako	k9	jako
silná	silný	k2eAgFnSc1d1	silná
kyselina	kyselina	k1gFnSc1	kyselina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
silnější	silný	k2eAgFnSc1d2	silnější
než	než	k8xS	než
fluorovodík	fluorovodík	k1gInSc1	fluorovodík
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
slabší	slabý	k2eAgMnSc1d2	slabší
než	než	k8xS	než
bromovodík	bromovodík	k1gInSc1	bromovodík
a	a	k8xC	a
jodovodík	jodovodík	k1gInSc1	jodovodík
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
chloridů	chlorid	k1gInPc2	chlorid
<g/>
.	.	kIx.	.
</s>
<s>
Vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
reakcí	reakce	k1gFnSc7	reakce
vodíku	vodík	k1gInSc2	vodík
s	s	k7c7	s
chlorem	chlor	k1gInSc7	chlor
nebo	nebo	k8xC	nebo
vytěsněním	vytěsnění	k1gNnSc7	vytěsnění
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
soli	sůl	k1gFnSc2	sůl
<g/>
.	.	kIx.	.
</s>
<s>
Bromovodík	bromovodík	k1gInSc1	bromovodík
neboli	neboli	k8xC	neboli
broman	broman	k1gMnSc1	broman
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
HBr	HBr	k1gFnSc3	HBr
je	být	k5eAaImIp3nS	být
plyn	plyn	k1gInSc1	plyn
bez	bez	k7c2	bez
barvy	barva	k1gFnSc2	barva
<g/>
,	,	kIx,	,
s	s	k7c7	s
leptavou	leptavý	k2eAgFnSc7d1	leptavá
chutí	chuť	k1gFnSc7	chuť
a	a	k8xC	a
nepříjemným	příjemný	k2eNgInSc7d1	nepříjemný
zápachem	zápach	k1gInSc7	zápach
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
se	se	k3xPyFc4	se
chová	chovat	k5eAaImIp3nS	chovat
jako	jako	k9	jako
silná	silný	k2eAgFnSc1d1	silná
kyselina	kyselina	k1gFnSc1	kyselina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
silnější	silný	k2eAgFnSc1d2	silnější
než	než	k8xS	než
chlorovodík	chlorovodík	k1gInSc1	chlorovodík
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
slabší	slabý	k2eAgMnSc1d2	slabší
než	než	k8xS	než
jodovodík	jodovodík	k1gInSc1	jodovodík
<g/>
.	.	kIx.	.
</s>
<s>
Nemá	mít	k5eNaImIp3nS	mít
významné	významný	k2eAgNnSc1d1	významné
praktické	praktický	k2eAgNnSc1d1	praktické
použití	použití	k1gNnSc1	použití
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
lze	lze	k6eAd1	lze
jej	on	k3xPp3gMnSc4	on
použít	použít	k5eAaPmF	použít
jako	jako	k9	jako
slabé	slabý	k2eAgNnSc4d1	slabé
redukční	redukční	k2eAgNnSc4d1	redukční
činidlo	činidlo	k1gNnSc4	činidlo
<g/>
.	.	kIx.	.
</s>
<s>
Vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
reakcí	reakce	k1gFnSc7	reakce
bromu	brom	k1gInSc2	brom
s	s	k7c7	s
vodíkem	vodík	k1gInSc7	vodík
<g/>
,	,	kIx,	,
nelze	lze	k6eNd1	lze
jej	on	k3xPp3gInSc4	on
vytěsnit	vytěsnit	k5eAaPmF	vytěsnit
z	z	k7c2	z
jeho	jeho	k3xOp3gFnSc2	jeho
soli	sůl	k1gFnSc2	sůl
<g/>
.	.	kIx.	.
</s>
<s>
Jodovodík	jodovodík	k1gInSc1	jodovodík
neboli	neboli	k8xC	neboli
jodan	jodan	k1gInSc1	jodan
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
HI	hi	k0	hi
je	on	k3xPp3gInPc4	on
plyn	plyn	k1gInSc4	plyn
bez	bez	k7c2	bez
barvy	barva	k1gFnSc2	barva
<g/>
,	,	kIx,	,
s	s	k7c7	s
leptavou	leptavý	k2eAgFnSc7d1	leptavá
chutí	chuť	k1gFnSc7	chuť
a	a	k8xC	a
nepříjemným	příjemný	k2eNgInSc7d1	nepříjemný
zápachem	zápach	k1gInSc7	zápach
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
se	se	k3xPyFc4	se
chová	chovat	k5eAaImIp3nS	chovat
jako	jako	k9	jako
velmi	velmi	k6eAd1	velmi
silná	silný	k2eAgFnSc1d1	silná
kyselina	kyselina	k1gFnSc1	kyselina
a	a	k8xC	a
z	z	k7c2	z
halogenovodíků	halogenovodík	k1gMnPc2	halogenovodík
je	být	k5eAaImIp3nS	být
nejsilnější	silný	k2eAgFnSc1d3	nejsilnější
<g/>
.	.	kIx.	.
</s>
<s>
Nemá	mít	k5eNaImIp3nS	mít
významné	významný	k2eAgNnSc1d1	významné
praktické	praktický	k2eAgNnSc1d1	praktické
využití	využití	k1gNnSc1	využití
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
lze	lze	k6eAd1	lze
jej	on	k3xPp3gMnSc4	on
použít	použít	k5eAaPmF	použít
jako	jako	k9	jako
silnější	silný	k2eAgNnSc4d2	silnější
redukční	redukční	k2eAgNnSc4d1	redukční
činidlo	činidlo	k1gNnSc4	činidlo
<g/>
.	.	kIx.	.
</s>
<s>
Vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
reakcí	reakce	k1gFnSc7	reakce
vodíku	vodík	k1gInSc2	vodík
s	s	k7c7	s
jodem	jod	k1gInSc7	jod
<g/>
,	,	kIx,	,
nelze	lze	k6eNd1	lze
jej	on	k3xPp3gInSc4	on
vytěsnit	vytěsnit	k5eAaPmF	vytěsnit
z	z	k7c2	z
jeho	jeho	k3xOp3gFnSc2	jeho
soli	sůl	k1gFnSc2	sůl
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1	ostatní
hydridy	hydrid	k1gInPc1	hydrid
nejsou	být	k5eNaImIp3nP	být
nijak	nijak	k6eAd1	nijak
významné	významný	k2eAgFnPc1d1	významná
a	a	k8xC	a
běžně	běžně	k6eAd1	běžně
se	se	k3xPyFc4	se
nevyskytují	vyskytovat	k5eNaImIp3nP	vyskytovat
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
iontové	iontový	k2eAgInPc1d1	iontový
hydridy	hydrid	k1gInPc1	hydrid
jsou	být	k5eAaImIp3nP	být
hydrid	hydrid	k1gInSc1	hydrid
lithný	lithný	k2eAgInSc1d1	lithný
LiH	LiH	k1gFnSc3	LiH
<g/>
,	,	kIx,	,
hydrid	hydrid	k1gInSc1	hydrid
draselný	draselný	k2eAgInSc1d1	draselný
KH	kh	k0	kh
<g/>
,	,	kIx,	,
hydrid	hydrid	k1gInSc1	hydrid
rubidný	rubidný	k2eAgInSc1d1	rubidný
RbH	RbH	k1gFnSc3	RbH
<g/>
,	,	kIx,	,
hydrid	hydrid	k1gInSc1	hydrid
cesný	cesný	k1gMnSc1	cesný
CsH	CsH	k1gMnSc1	CsH
<g/>
,	,	kIx,	,
hydrid	hydrid	k1gInSc1	hydrid
berylnatý	berylnatý	k2eAgInSc1d1	berylnatý
(	(	kIx(	(
<g/>
polymerní	polymerní	k2eAgFnSc1d1	polymerní
struktura	struktura	k1gFnSc1	struktura
<g/>
)	)	kIx)	)
BeH	BeH	k1gFnSc1	BeH
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
hydrid	hydrid	k1gInSc1	hydrid
hořečnatý	hořečnatý	k2eAgInSc1d1	hořečnatý
(	(	kIx(	(
<g/>
polymerní	polymerní	k2eAgFnSc1d1	polymerní
struktura	struktura	k1gFnSc1	struktura
<g/>
)	)	kIx)	)
MgH	MgH	k1gFnSc1	MgH
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
hydrid	hydrid	k1gInSc1	hydrid
strontnatý	strontnatý	k2eAgInSc1d1	strontnatý
SrH	srha	k1gFnPc2	srha
<g/>
2	[number]	k4	2
a	a	k8xC	a
hydrid	hydrid	k1gInSc1	hydrid
barnatý	barnatý	k2eAgInSc1d1	barnatý
BaH	bah	k0	bah
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
kovalentní	kovalentní	k2eAgInPc1d1	kovalentní
hydridy	hydrid	k1gInPc1	hydrid
jsou	být	k5eAaImIp3nP	být
boran	boran	k1gInSc4	boran
(	(	kIx(	(
<g/>
polymerní	polymerní	k2eAgFnSc1d1	polymerní
struktura	struktura	k1gFnSc1	struktura
<g/>
)	)	kIx)	)
BH	BH	kA	BH
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
alan	alan	k1gInSc1	alan
(	(	kIx(	(
<g/>
polymerní	polymerní	k2eAgFnSc1d1	polymerní
struktura	struktura	k1gFnSc1	struktura
<g/>
)	)	kIx)	)
AlH	AlH	k1gFnSc1	AlH
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
gallan	gallan	k1gInSc1	gallan
GaH	GaH	k1gFnSc1	GaH
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
indan	indan	k1gInSc1	indan
(	(	kIx(	(
<g/>
polymerní	polymerní	k2eAgFnSc1d1	polymerní
struktura	struktura	k1gFnSc1	struktura
<g/>
)	)	kIx)	)
InH	InH	k1gFnSc1	InH
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
thalan	thalan	k1gInSc1	thalan
TlH	TlH	k1gFnSc1	TlH
<g/>
3	[number]	k4	3
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
methan	methan	k1gInSc1	methan
(	(	kIx(	(
<g/>
systematicky	systematicky	k6eAd1	systematicky
karban	karban	k1gInSc1	karban
<g/>
)	)	kIx)	)
CH4	CH4	k1gFnSc1	CH4
(	(	kIx(	(
<g/>
organická	organický	k2eAgFnSc1d1	organická
sloučenina	sloučenina	k1gFnSc1	sloučenina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
silan	silan	k1gInSc1	silan
SiH	SiH	k1gFnSc1	SiH
<g/>
4	[number]	k4	4
(	(	kIx(	(
<g/>
organická	organický	k2eAgFnSc1d1	organická
sloučenina	sloučenina	k1gFnSc1	sloučenina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
german	german	k1gMnSc1	german
GeH	GeH	k1gFnSc2	GeH
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
stannan	stannan	k1gInSc1	stannan
SnH	SnH	k1gFnSc1	SnH
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
plumban	plumban	k1gInSc1	plumban
PbH	PbH	k1gFnSc1	PbH
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
fosfan	fosfan	k1gInSc1	fosfan
PH	Ph	kA	Ph
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
arsan	arsan	k1gInSc1	arsan
AsH	AsH	k1gFnSc1	AsH
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
stiban	stiban	k1gInSc1	stiban
SbH	SbH	k1gFnSc1	SbH
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
bismutan	bismutan	k1gInSc1	bismutan
BiH	BiH	k1gFnSc1	BiH
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
selan	selan	k1gInSc1	selan
SeH	SeH	k1gFnSc1	SeH
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
telan	telan	k1gInSc1	telan
TeH	TeH	k1gFnSc1	TeH
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
polan	polana	k1gFnPc2	polana
PoH	PoH	k1gFnSc4	PoH
<g/>
2	[number]	k4	2
a	a	k8xC	a
astatan	astatan	k1gInSc1	astatan
AtH	AtH	k1gFnSc2	AtH
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalším	další	k2eAgFnPc3d1	další
sloučeninám	sloučenina	k1gFnPc3	sloučenina
vodíku	vodík	k1gInSc2	vodík
patří	patřit	k5eAaImIp3nP	patřit
kyslíkaté	kyslíkatý	k2eAgFnPc1d1	kyslíkatá
kyseliny	kyselina	k1gFnPc1	kyselina
<g/>
,	,	kIx,	,
hydroxidy	hydroxid	k1gInPc1	hydroxid
a	a	k8xC	a
hydráty	hydrát	k1gInPc1	hydrát
solí	sůl	k1gFnPc2	sůl
<g/>
.	.	kIx.	.
</s>
<s>
Obecný	obecný	k2eAgInSc1d1	obecný
vzorec	vzorec	k1gInSc1	vzorec
kyslíkaté	kyslíkatý	k2eAgFnSc2d1	kyslíkatá
kyseliny	kyselina	k1gFnSc2	kyselina
je	být	k5eAaImIp3nS	být
HaAbOc	HaAbOc	k1gFnSc1	HaAbOc
<g/>
,	,	kIx,	,
a	a	k8xC	a
<g/>
,	,	kIx,	,
<g/>
b	b	k?	b
<g/>
,	,	kIx,	,
<g/>
c	c	k0	c
jsou	být	k5eAaImIp3nP	být
stechiometrické	stechiometrický	k2eAgInPc4d1	stechiometrický
koeficienty	koeficient	k1gInPc4	koeficient
kyseliny	kyselina	k1gFnSc2	kyselina
a	a	k8xC	a
A	a	k9	a
je	být	k5eAaImIp3nS	být
kyselinotvorný	kyselinotvorný	k2eAgInSc4d1	kyselinotvorný
prvek	prvek	k1gInSc4	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Atom	atom	k1gInSc1	atom
vodíku	vodík	k1gInSc2	vodík
je	být	k5eAaImIp3nS	být
složkou	složka	k1gFnSc7	složka
každé	každý	k3xTgFnSc2	každý
kyseliny	kyselina	k1gFnSc2	kyselina
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
odštěpuje	odštěpovat	k5eAaImIp3nS	odštěpovat
kyselina	kyselina	k1gFnSc1	kyselina
ion	ion	k1gInSc1	ion
H	H	kA	H
<g/>
+	+	kIx~	+
a	a	k8xC	a
následně	následně	k6eAd1	následně
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
s	s	k7c7	s
molekulou	molekula	k1gFnSc7	molekula
vody	voda	k1gFnSc2	voda
oxoniový	oxoniový	k2eAgInSc4d1	oxoniový
kation	kation	k1gInSc4	kation
H	H	kA	H
<g/>
3	[number]	k4	3
<g/>
O	O	kA	O
<g/>
+	+	kIx~	+
<g/>
.	.	kIx.	.
</s>
<s>
Kyseliny	kyselina	k1gFnPc1	kyselina
v	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
mají	mít	k5eAaImIp3nP	mít
pH	ph	kA	ph
menší	malý	k2eAgInPc1d2	menší
než	než	k8xS	než
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Obecný	obecný	k2eAgInSc1d1	obecný
vzorec	vzorec	k1gInSc1	vzorec
hydroxidu	hydroxid	k1gInSc2	hydroxid
je	být	k5eAaImIp3nS	být
M	M	kA	M
<g/>
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
<g/>
n	n	k0	n
<g/>
,	,	kIx,	,
n	n	k0	n
je	být	k5eAaImIp3nS	být
počet	počet	k1gInSc1	počet
molekul	molekula	k1gFnPc2	molekula
OH	OH	kA	OH
a	a	k8xC	a
M	M	kA	M
je	být	k5eAaImIp3nS	být
zásadotvorný	zásadotvorný	k2eAgInSc1d1	zásadotvorný
kov	kov	k1gInSc1	kov
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
hydroxidy	hydroxid	k1gInPc1	hydroxid
odštěpují	odštěpovat	k5eAaImIp3nP	odštěpovat
anion	anion	k1gInSc4	anion
OH	OH	kA	OH
<g/>
-	-	kIx~	-
a	a	k8xC	a
v	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
mají	mít	k5eAaImIp3nP	mít
pH	ph	kA	ph
větší	veliký	k2eAgFnSc4d2	veliký
než	než	k8xS	než
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Hydráty	hydrát	k1gInPc1	hydrát
solí	sůl	k1gFnPc2	sůl
jsou	být	k5eAaImIp3nP	být
látky	látka	k1gFnPc1	látka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
struktuře	struktura	k1gFnSc6	struktura
vázané	vázaný	k2eAgFnSc6d1	vázaná
(	(	kIx(	(
<g/>
komplexně	komplexně	k6eAd1	komplexně
i	i	k9	i
hydratačně	hydratačně	k6eAd1	hydratačně
<g/>
)	)	kIx)	)
molekuly	molekula	k1gFnPc1	molekula
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Zahříváním	zahřívání	k1gNnSc7	zahřívání
se	se	k3xPyFc4	se
tyto	tento	k3xDgFnPc1	tento
molekuly	molekula	k1gFnPc1	molekula
vody	voda	k1gFnSc2	voda
odštěpují	odštěpovat	k5eAaImIp3nP	odštěpovat
a	a	k8xC	a
rozpouštěním	rozpouštění	k1gNnSc7	rozpouštění
látek	látka	k1gFnPc2	látka
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
opět	opět	k6eAd1	opět
poutají	poutat	k5eAaImIp3nP	poutat
molekuly	molekula	k1gFnPc1	molekula
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
poutání	poutání	k1gNnSc6	poutání
molekuly	molekula	k1gFnSc2	molekula
vody	voda	k1gFnSc2	voda
-	-	kIx~	-
hydrataci	hydratace	k1gFnSc4	hydratace
-	-	kIx~	-
se	se	k3xPyFc4	se
u	u	k7c2	u
některých	některý	k3yIgFnPc2	některý
sloučenin	sloučenina	k1gFnPc2	sloučenina
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
tzv.	tzv.	kA	tzv.
hydratační	hydratační	k2eAgNnSc1d1	hydratační
teplo	teplo	k1gNnSc1	teplo
-	-	kIx~	-
např.	např.	kA	např.
CaCl	CaCl	k1gInSc4	CaCl
<g/>
2	[number]	k4	2
+	+	kIx~	+
2	[number]	k4	2
H2O	H2O	k1gMnSc1	H2O
→	→	k?	→
CaCl	CaCl	k1gMnSc1	CaCl
<g/>
2.2	[number]	k4	2.2
H2O	H2O	k1gFnPc2	H2O
+	+	kIx~	+
teplo	teplo	k1gNnSc1	teplo
<g/>
.	.	kIx.	.
</s>
<s>
Nejtypičtější	typický	k2eAgInSc1d3	nejtypičtější
hydrát	hydrát	k1gInSc1	hydrát
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
vodu	voda	k1gFnSc4	voda
vázanou	vázaný	k2eAgFnSc4d1	vázaná
komplexně	komplexně	k6eAd1	komplexně
i	i	k8xC	i
hydratačně	hydratačně	k6eAd1	hydratačně
je	být	k5eAaImIp3nS	být
modrá	modrý	k2eAgFnSc1d1	modrá
skalice	skalice	k1gFnSc1	skalice
[	[	kIx(	[
<g/>
Cu	Cu	k1gMnSc1	Cu
<g/>
(	(	kIx(	(
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
)	)	kIx)	)
<g/>
4	[number]	k4	4
<g/>
]	]	kIx)	]
<g/>
SO	So	kA	So
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
monohydrát	monohydrát	k1gInSc4	monohydrát
síranu	síran	k1gInSc2	síran
tetraaqua-měďnatého	tetraaquaěďnatý	k2eAgInSc2d1	tetraaqua-měďnatý
<g/>
.	.	kIx.	.
</s>
<s>
Komplexně	komplexně	k6eAd1	komplexně
vázaná	vázaný	k2eAgFnSc1d1	vázaná
voda	voda	k1gFnSc1	voda
se	se	k3xPyFc4	se
z	z	k7c2	z
látek	látka	k1gFnPc2	látka
odstraňuje	odstraňovat	k5eAaImIp3nS	odstraňovat
hůře	zle	k6eAd2	zle
než	než	k8xS	než
voda	voda	k1gFnSc1	voda
vázaná	vázaný	k2eAgFnSc1d1	vázaná
hydratačně	hydratačně	k6eAd1	hydratačně
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
základních	základní	k2eAgInPc2d1	základní
kamenů	kámen	k1gInPc2	kámen
všech	všecek	k3xTgFnPc2	všecek
organických	organický	k2eAgFnPc2d1	organická
molekul	molekula	k1gFnPc2	molekula
je	být	k5eAaImIp3nS	být
vodík	vodík	k1gInSc1	vodík
přítomný	přítomný	k2eAgInSc1d1	přítomný
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
tkáních	tkáň	k1gFnPc6	tkáň
živých	živý	k2eAgMnPc2d1	živý
organizmů	organizmus	k1gInPc2	organizmus
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
organické	organický	k2eAgFnPc4d1	organická
sloučeniny	sloučenina	k1gFnPc4	sloučenina
patří	patřit	k5eAaImIp3nP	patřit
sloučeniny	sloučenina	k1gFnPc1	sloučenina
uhlíku	uhlík	k1gInSc2	uhlík
s	s	k7c7	s
vodíkem	vodík	k1gInSc7	vodík
a	a	k8xC	a
křemíku	křemík	k1gInSc2	křemík
s	s	k7c7	s
vodíkem	vodík	k1gInSc7	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Uhlík	uhlík	k1gInSc1	uhlík
a	a	k8xC	a
vodík	vodík	k1gInSc1	vodík
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
uhlovodících	uhlovodík	k1gInPc6	uhlovodík
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
všech	všecek	k3xTgMnPc2	všecek
jejich	jejich	k3xOp3gInSc6	jejich
derivátech	derivát	k1gInPc6	derivát
<g/>
.	.	kIx.	.
</s>
<s>
Křemík	křemík	k1gInSc1	křemík
a	a	k8xC	a
vodík	vodík	k1gInSc1	vodík
je	být	k5eAaImIp3nS	být
obsažen	obsáhnout	k5eAaPmNgInS	obsáhnout
v	v	k7c6	v
silanech	silan	k1gInPc6	silan
a	a	k8xC	a
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
jejich	jejich	k3xOp3gInPc2	jejich
derivátů	derivát	k1gInPc2	derivát
<g/>
.	.	kIx.	.
</s>
<s>
Vodík	vodík	k1gInSc1	vodík
má	mít	k5eAaImIp3nS	mít
3	[number]	k4	3
izotopy	izotop	k1gInPc4	izotop
<g/>
:	:	kIx,	:
Klasický	klasický	k2eAgInSc4d1	klasický
atom	atom	k1gInSc4	atom
vodíku	vodík	k1gInSc2	vodík
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
protium	protium	k1gNnSc4	protium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tvořený	tvořený	k2eAgInSc1d1	tvořený
jedním	jeden	k4xCgInSc7	jeden
protonem	proton	k1gInSc7	proton
a	a	k8xC	a
jedním	jeden	k4xCgInSc7	jeden
elektronem	elektron	k1gInSc7	elektron
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
izotop	izotop	k1gInSc1	izotop
je	být	k5eAaImIp3nS	být
nejjednodušší	jednoduchý	k2eAgInSc4d3	nejjednodušší
atom	atom	k1gInSc4	atom
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Deuterium	deuterium	k1gNnSc1	deuterium
<g/>
.	.	kIx.	.
</s>
<s>
Atom	atom	k1gInSc1	atom
s	s	k7c7	s
jádrem	jádro	k1gNnSc7	jádro
2	[number]	k4	2
<g/>
H	H	kA	H
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
jeden	jeden	k4xCgInSc4	jeden
proton	proton	k1gInSc4	proton
a	a	k8xC	a
jeden	jeden	k4xCgInSc4	jeden
neutron	neutron	k1gInSc4	neutron
a	a	k8xC	a
od	od	k7c2	od
běžného	běžný	k2eAgInSc2d1	běžný
vodíku	vodík	k1gInSc2	vodík
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
především	především	k9	především
atomovou	atomový	k2eAgFnSc7d1	atomová
hmotností	hmotnost	k1gFnSc7	hmotnost
2,013	[number]	k4	2,013
<g/>
63	[number]	k4	63
u	u	k7c2	u
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
deuterium	deuterium	k1gNnSc1	deuterium
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
mu	on	k3xPp3gMnSc3	on
bývá	bývat	k5eAaImIp3nS	bývat
přiřazována	přiřazován	k2eAgFnSc1d1	přiřazován
i	i	k8xC	i
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
D	D	kA	D
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
nejedná	jednat	k5eNaImIp3nS	jednat
o	o	k7c4	o
jiný	jiný	k2eAgInSc4d1	jiný
prvek	prvek	k1gInSc4	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Deuterium	deuterium	k1gNnSc1	deuterium
je	být	k5eAaImIp3nS	být
stabilní	stabilní	k2eAgInSc4d1	stabilní
izotop	izotop	k1gInSc4	izotop
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
nepodléhá	podléhat	k5eNaImIp3nS	podléhat
radioaktivní	radioaktivní	k2eAgFnSc3d1	radioaktivní
přeměně	přeměna	k1gFnSc3	přeměna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
vedle	vedle	k7c2	vedle
lehkého	lehký	k2eAgInSc2d1	lehký
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
připadá	připadat	k5eAaPmIp3nS	připadat
jeden	jeden	k4xCgInSc4	jeden
atom	atom	k1gInSc4	atom
deuteria	deuterium	k1gNnSc2	deuterium
na	na	k7c4	na
7	[number]	k4	7
000	[number]	k4	000
atomů	atom	k1gInPc2	atom
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
s	s	k7c7	s
kyslíkem	kyslík	k1gInSc7	kyslík
tvoří	tvořit	k5eAaImIp3nS	tvořit
deuterium	deuterium	k1gNnSc1	deuterium
těžkou	těžký	k2eAgFnSc4d1	těžká
vodu	voda	k1gFnSc4	voda
<g/>
,	,	kIx,	,
D	D	kA	D
<g/>
2	[number]	k4	2
<g/>
O.	O.	kA	O.
Tato	tento	k3xDgFnSc1	tento
sloučenina	sloučenina	k1gFnSc1	sloučenina
má	mít	k5eAaImIp3nS	mít
významné	významný	k2eAgNnSc4d1	významné
využití	využití	k1gNnSc4	využití
v	v	k7c6	v
jaderném	jaderný	k2eAgInSc6d1	jaderný
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
účinným	účinný	k2eAgInSc7d1	účinný
moderátorem	moderátor	k1gInSc7	moderátor
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
látkou	látka	k1gFnSc7	látka
zpomalující	zpomalující	k2eAgFnSc1d1	zpomalující
rychlost	rychlost	k1gFnSc1	rychlost
neutronů	neutron	k1gInPc2	neutron
<g/>
.	.	kIx.	.
</s>
<s>
Této	tento	k3xDgFnSc3	tento
vlastnosti	vlastnost	k1gFnSc3	vlastnost
se	se	k3xPyFc4	se
již	již	k6eAd1	již
od	od	k7c2	od
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
využívá	využívat	k5eAaPmIp3nS	využívat
v	v	k7c6	v
určitém	určitý	k2eAgInSc6d1	určitý
typu	typ	k1gInSc6	typ
jaderných	jaderný	k2eAgInPc2d1	jaderný
reaktorů	reaktor	k1gInPc2	reaktor
k	k	k7c3	k
přípravě	příprava	k1gFnSc3	příprava
plutonia	plutonium	k1gNnSc2	plutonium
z	z	k7c2	z
uranu	uran	k1gInSc2	uran
<g/>
.	.	kIx.	.
</s>
<s>
Těžká	těžký	k2eAgFnSc1d1	těžká
voda	voda	k1gFnSc1	voda
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
elektrolýzou	elektrolýza	k1gFnSc7	elektrolýza
vody	voda	k1gFnSc2	voda
<g/>
:	:	kIx,	:
ta	ten	k3xDgFnSc1	ten
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
H2O	H2O	k1gMnSc7	H2O
i	i	k8xC	i
D	D	kA	D
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
,	,	kIx,	,
těžká	těžký	k2eAgFnSc1d1	těžká
voda	voda	k1gFnSc1	voda
se	se	k3xPyFc4	se
ale	ale	k9	ale
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
pomaleji	pomale	k6eAd2	pomale
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
při	při	k7c6	při
mnohonásobném	mnohonásobný	k2eAgNnSc6d1	mnohonásobné
opakování	opakování	k1gNnSc6	opakování
elektrolýzy	elektrolýza	k1gFnSc2	elektrolýza
lze	lze	k6eAd1	lze
získat	získat	k5eAaPmF	získat
velmi	velmi	k6eAd1	velmi
čistou	čistý	k2eAgFnSc4d1	čistá
těžkou	těžký	k2eAgFnSc4d1	těžká
vodu	voda	k1gFnSc4	voda
-	-	kIx~	-
až	až	k9	až
99,9	[number]	k4	99,9
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Německá	německý	k2eAgFnSc1d1	německá
armáda	armáda	k1gFnSc1	armáda
se	se	k3xPyFc4	se
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
snažila	snažit	k5eAaImAgFnS	snažit
vyvinout	vyvinout	k5eAaPmF	vyvinout
jadernou	jaderný	k2eAgFnSc4d1	jaderná
bombu	bomba	k1gFnSc4	bomba
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
plutonia	plutonium	k1gNnSc2	plutonium
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
norském	norský	k2eAgInSc6d1	norský
Rjukanu	Rjukan	k1gInSc6	Rjukan
existoval	existovat	k5eAaImAgMnS	existovat
průmyslový	průmyslový	k2eAgInSc4d1	průmyslový
komplex	komplex	k1gInSc4	komplex
společnosti	společnost	k1gFnSc2	společnost
Norsk	Norsk	k1gInSc1	Norsk
Hydro	hydra	k1gFnSc5	hydra
<g/>
,	,	kIx,	,
vyrábějící	vyrábějící	k2eAgFnSc4d1	vyrábějící
těžkou	těžký	k2eAgFnSc4d1	těžká
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
</s>
<s>
Spojenci	spojenec	k1gMnPc1	spojenec
tento	tento	k3xDgInSc4	tento
komplex	komplex	k1gInSc4	komplex
zničili	zničit	k5eAaPmAgMnP	zničit
operací	operace	k1gFnSc7	operace
zvláštních	zvláštní	k2eAgFnPc2d1	zvláštní
jednotek	jednotka	k1gFnPc2	jednotka
(	(	kIx(	(
<g/>
bombardování	bombardování	k1gNnSc1	bombardování
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
opravě	oprava	k1gFnSc6	oprava
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
těžké	těžký	k2eAgFnPc4d1	těžká
ztráty	ztráta	k1gFnPc4	ztráta
na	na	k7c6	na
životech	život	k1gInPc6	život
místních	místní	k2eAgMnPc2d1	místní
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
továrnu	továrna	k1gFnSc4	továrna
poškodilo	poškodit	k5eAaPmAgNnS	poškodit
jen	jen	k9	jen
mírně	mírně	k6eAd1	mírně
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
však	však	k9	však
nacistům	nacista	k1gMnPc3	nacista
podařilo	podařit	k5eAaPmAgNnS	podařit
vyrobit	vyrobit	k5eAaPmF	vyrobit
dostatečné	dostatečný	k2eAgNnSc4d1	dostatečné
množství	množství	k1gNnSc4	množství
těžké	těžký	k2eAgFnSc2d1	těžká
vody	voda	k1gFnSc2	voda
pro	pro	k7c4	pro
další	další	k2eAgInPc4d1	další
experimenty	experiment	k1gInPc4	experiment
s	s	k7c7	s
jadernou	jaderný	k2eAgFnSc7d1	jaderná
zbraní	zbraň	k1gFnSc7	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Těžká	těžký	k2eAgFnSc1d1	těžká
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
přepravit	přepravit	k5eAaPmF	přepravit
z	z	k7c2	z
Norska	Norsko	k1gNnSc2	Norsko
do	do	k7c2	do
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
potopena	potopen	k2eAgFnSc1d1	potopena
při	při	k7c6	při
převozu	převoz	k1gInSc6	převoz
přes	přes	k7c4	přes
jedno	jeden	k4xCgNnSc4	jeden
z	z	k7c2	z
norských	norský	k2eAgNnPc2d1	norské
jezer	jezero	k1gNnPc2	jezero
díky	díky	k7c3	díky
partyzánské	partyzánský	k2eAgFnSc3d1	Partyzánská
akci	akce	k1gFnSc3	akce
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
sudy	sud	k1gInPc1	sud
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
nebyly	být	k5eNaImAgInP	být
zcela	zcela	k6eAd1	zcela
naplněny	naplněn	k2eAgInPc1d1	naplněn
a	a	k8xC	a
po	po	k7c6	po
výbuchu	výbuch	k1gInSc6	výbuch
trajektu	trajekt	k1gInSc2	trajekt
plavaly	plavat	k5eAaImAgFnP	plavat
na	na	k7c6	na
hladině	hladina	k1gFnSc6	hladina
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
však	však	k9	však
dostaly	dostat	k5eAaPmAgFnP	dostat
do	do	k7c2	do
Berlína	Berlín	k1gInSc2	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
deuterium	deuterium	k1gNnSc1	deuterium
využíváno	využívat	k5eAaImNgNnS	využívat
také	také	k6eAd1	také
jako	jako	k8xC	jako
účinný	účinný	k2eAgInSc1d1	účinný
stopovač	stopovač	k1gInSc1	stopovač
biochemických	biochemický	k2eAgFnPc2d1	biochemická
reakcí	reakce	k1gFnPc2	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
výzkumu	výzkum	k1gInSc2	výzkum
distribuce	distribuce	k1gFnSc2	distribuce
určité	určitý	k2eAgFnSc2d1	určitá
sloučeniny	sloučenina	k1gFnSc2	sloučenina
v	v	k7c6	v
organizmu	organizmus	k1gInSc6	organizmus
použita	použít	k5eAaPmNgFnS	použít
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
atomy	atom	k1gInPc4	atom
vodíku	vodík	k1gInSc2	vodík
nahrazeny	nahradit	k5eAaPmNgInP	nahradit
deuteriem	deuterium	k1gNnSc7	deuterium
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
vysledovat	vysledovat	k5eAaPmF	vysledovat
její	její	k3xOp3gFnSc4	její
cestu	cesta	k1gFnSc4	cesta
biochemickou	biochemický	k2eAgFnSc7d1	biochemická
přeměnou	přeměna	k1gFnSc7	přeměna
analýzou	analýza	k1gFnSc7	analýza
všech	všecek	k3xTgInPc2	všecek
možných	možný	k2eAgInPc2d1	možný
vzniklých	vzniklý	k2eAgInPc2d1	vzniklý
produktů	produkt	k1gInPc2	produkt
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Tritium	tritium	k1gNnSc1	tritium
<g/>
.	.	kIx.	.
</s>
<s>
Tritium	tritium	k1gNnSc1	tritium
(	(	kIx(	(
<g/>
čti	číst	k5eAaImRp2nS	číst
trícium	trícium	k1gNnSc4	trícium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
izotop	izotop	k1gInSc4	izotop
3	[number]	k4	3
<g/>
H	H	kA	H
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
jádro	jádro	k1gNnSc4	jádro
složeno	složen	k2eAgNnSc4d1	složeno
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
protonu	proton	k1gInSc2	proton
a	a	k8xC	a
2	[number]	k4	2
neutronů	neutron	k1gInPc2	neutron
a	a	k8xC	a
bývá	bývat	k5eAaImIp3nS	bývat
někdy	někdy	k6eAd1	někdy
označován	označovat	k5eAaImNgInS	označovat
chemickou	chemický	k2eAgFnSc7d1	chemická
značkou	značka	k1gFnSc7	značka
T.	T.	kA	T.
Jeho	jeho	k3xOp3gFnSc1	jeho
atomová	atomový	k2eAgFnSc1d1	atomová
hmotnost	hmotnost	k1gFnSc1	hmotnost
má	mít	k5eAaImIp3nS	mít
hodnotu	hodnota	k1gFnSc4	hodnota
3,016	[number]	k4	3,016
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
u.	u.	k?	u.
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
deuteria	deuterium	k1gNnSc2	deuterium
je	být	k5eAaImIp3nS	být
jádro	jádro	k1gNnSc4	jádro
tritia	tritium	k1gNnSc2	tritium
nestabilní	stabilní	k2eNgNnSc1d1	nestabilní
a	a	k8xC	a
rozpadá	rozpadat	k5eAaPmIp3nS	rozpadat
se	se	k3xPyFc4	se
s	s	k7c7	s
poločasem	poločas	k1gInSc7	poločas
rozpadu	rozpad	k1gInSc2	rozpad
12,33	[number]	k4	12,33
roku	rok	k1gInSc2	rok
za	za	k7c4	za
vyzáření	vyzáření	k1gNnSc4	vyzáření
pouze	pouze	k6eAd1	pouze
málo	málo	k6eAd1	málo
energetického	energetický	k2eAgNnSc2d1	energetické
záření	záření	k1gNnSc2	záření
beta	beta	k1gNnSc1	beta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodních	přírodní	k2eAgFnPc6d1	přírodní
podmínkách	podmínka	k1gFnPc6	podmínka
vzniká	vznikat	k5eAaImIp3nS	vznikat
tritium	tritium	k1gNnSc1	tritium
především	především	k9	především
v	v	k7c6	v
horních	horní	k2eAgFnPc6d1	horní
vrstvách	vrstva	k1gFnPc6	vrstva
atmosféry	atmosféra	k1gFnSc2	atmosféra
při	při	k7c6	při
kolizi	kolize	k1gFnSc6	kolize
kosmického	kosmický	k2eAgNnSc2d1	kosmické
záření	záření	k1gNnSc2	záření
s	s	k7c7	s
jádrem	jádro	k1gNnSc7	jádro
atomu	atom	k1gInSc2	atom
deuteria	deuterium	k1gNnSc2	deuterium
<g/>
.	.	kIx.	.
</s>
<s>
Uměle	uměle	k6eAd1	uměle
je	být	k5eAaImIp3nS	být
tritium	tritium	k1gNnSc1	tritium
získáváno	získávat	k5eAaImNgNnS	získávat
v	v	k7c6	v
těžkovodních	těžkovodní	k2eAgInPc6d1	těžkovodní
jaderných	jaderný	k2eAgInPc6d1	jaderný
reaktorech	reaktor	k1gInPc6	reaktor
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
plutonia	plutonium	k1gNnSc2	plutonium
z	z	k7c2	z
přírodního	přírodní	k2eAgInSc2d1	přírodní
uranu	uran	k1gInSc2	uran
<g/>
.	.	kIx.	.
</s>
<s>
Tritium	tritium	k1gNnSc1	tritium
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
jedna	jeden	k4xCgFnSc1	jeden
složka	složka	k1gFnSc1	složka
náplně	náplň	k1gFnSc2	náplň
termonukleární	termonukleární	k2eAgFnSc2d1	termonukleární
bomby	bomba	k1gFnSc2	bomba
<g/>
,	,	kIx,	,
nejničivější	ničivý	k2eAgFnPc4d3	nejničivější
lidmi	člověk	k1gMnPc7	člověk
vyrobené	vyrobený	k2eAgFnSc2d1	vyrobená
zbraně	zbraň	k1gFnSc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Tritium	tritium	k1gNnSc1	tritium
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
základních	základní	k2eAgInPc2d1	základní
meziproduktů	meziprodukt	k1gInPc2	meziprodukt
termojaderné	termojaderný	k2eAgFnSc2d1	termojaderná
fúze	fúze	k1gFnSc2	fúze
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
energetickým	energetický	k2eAgInSc7d1	energetický
zdrojem	zdroj	k1gInSc7	zdroj
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Tritium	tritium	k1gNnSc1	tritium
se	se	k3xPyFc4	se
též	též	k9	též
někdy	někdy	k6eAd1	někdy
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
svítících	svítící	k2eAgFnPc2d1	svítící
ručiček	ručička	k1gFnPc2	ručička
a	a	k8xC	a
indexů	index	k1gInPc2	index
hodinek	hodinka	k1gFnPc2	hodinka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
září	zářit	k5eAaImIp3nP	zářit
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
byly	být	k5eAaImAgFnP	být
předtím	předtím	k6eAd1	předtím
vystaveny	vystavit	k5eAaPmNgFnP	vystavit
světlu	světlo	k1gNnSc3	světlo
<g/>
:	:	kIx,	:
tritium	tritium	k1gNnSc1	tritium
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
zářič	zářič	k1gInSc1	zářič
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
budí	budit	k5eAaImIp3nS	budit
některou	některý	k3yIgFnSc4	některý
luminiscenční	luminiscenční	k2eAgFnSc4d1	luminiscenční
látku	látka	k1gFnSc4	látka
ke	k	k7c3	k
světélkování	světélkování	k1gNnSc3	světélkování
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
poločas	poločas	k1gInSc4	poločas
rozpadu	rozpad	k1gInSc2	rozpad
tritia	tritium	k1gNnSc2	tritium
je	být	k5eAaImIp3nS	být
životnost	životnost	k1gFnSc1	životnost
takové	takový	k3xDgFnSc2	takový
světélkující	světélkující	k2eAgFnSc2d1	světélkující
barvy	barva	k1gFnSc2	barva
několik	několik	k4yIc4	několik
desítek	desítka	k1gFnPc2	desítka
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Zdravotní	zdravotní	k2eAgNnPc1d1	zdravotní
rizika	riziko	k1gNnPc1	riziko
jsou	být	k5eAaImIp3nP	být
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
luminiscenčních	luminiscenční	k2eAgFnPc2d1	luminiscenční
barev	barva	k1gFnPc2	barva
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yRgFnPc2	který
se	se	k3xPyFc4	se
používalo	používat	k5eAaImAgNnS	používat
radium	radium	k1gNnSc1	radium
<g/>
,	,	kIx,	,
nulové	nulový	k2eAgNnSc1d1	nulové
<g/>
.	.	kIx.	.
</s>
<s>
Tritium	tritium	k1gNnSc1	tritium
však	však	k9	však
je	být	k5eAaImIp3nS	být
používáno	používán	k2eAgNnSc1d1	používáno
jen	jen	k9	jen
několika	několik	k4yIc7	několik
výrobci	výrobce	k1gMnPc7	výrobce
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
výroba	výroba	k1gFnSc1	výroba
je	být	k5eAaImIp3nS	být
nákladná	nákladný	k2eAgFnSc1d1	nákladná
<g/>
.	.	kIx.	.
</s>
<s>
Tritium	tritium	k1gNnSc1	tritium
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
vázáno	vázat	k5eAaImNgNnS	vázat
jako	jako	k8xC	jako
plyn	plyn	k1gInSc1	plyn
do	do	k7c2	do
mikrogranulí	mikrogranulý	k2eAgMnPc1d1	mikrogranulý
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
obsaženo	obsáhnout	k5eAaPmNgNnS	obsáhnout
ve	v	k7c6	v
skleněných	skleněný	k2eAgFnPc6d1	skleněná
mikrotrubičkách	mikrotrubička	k1gFnPc6	mikrotrubička
<g/>
.	.	kIx.	.
</s>
<s>
Obojí	obojí	k4xRgMnSc1	obojí
je	být	k5eAaImIp3nS	být
technologicky	technologicky	k6eAd1	technologicky
náročné	náročný	k2eAgNnSc1d1	náročné
<g/>
.	.	kIx.	.
</s>
<s>
Cotton	Cotton	k1gInSc1	Cotton
F.	F.	kA	F.
<g/>
A.	A.	kA	A.
<g/>
,	,	kIx,	,
Wilkinson	Wilkinson	k1gMnSc1	Wilkinson
J.	J.	kA	J.
<g/>
:	:	kIx,	:
<g/>
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
souborné	souborný	k2eAgNnSc1d1	souborné
zpracování	zpracování	k1gNnSc1	zpracování
pro	pro	k7c4	pro
pokročilé	pokročilý	k1gMnPc4	pokročilý
<g/>
,	,	kIx,	,
ACADEMIA	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1973	[number]	k4	1973
Holzbecher	Holzbechra	k1gFnPc2	Holzbechra
Z.	Z.	kA	Z.
<g/>
:	:	kIx,	:
<g/>
Analytická	analytický	k2eAgFnSc1d1	analytická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
SNTL	SNTL	kA	SNTL
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1974	[number]	k4	1974
Jursík	Jursík	k1gInSc1	Jursík
F.	F.	kA	F.
<g/>
:	:	kIx,	:
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
nekovů	nekov	k1gInPc2	nekov
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-7080-504-8	[number]	k4	80-7080-504-8
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Heinrich	Heinrich	k1gMnSc1	Heinrich
Remy	remy	k1gNnSc2	remy
<g/>
,	,	kIx,	,
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1961	[number]	k4	1961
N.	N.	kA	N.
N.	N.	kA	N.
Greenwood	Greenwooda	k1gFnPc2	Greenwooda
-	-	kIx~	-
A.	A.	kA	A.
Earnshaw	Earnshaw	k1gFnSc1	Earnshaw
<g/>
,	,	kIx,	,
Chemie	chemie	k1gFnSc1	chemie
prvků	prvek	k1gInPc2	prvek
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1993	[number]	k4	1993
ISBN	ISBN	kA	ISBN
80-85427-38-9	[number]	k4	80-85427-38-9
Z.	Z.	kA	Z.
Ibler	Iblra	k1gFnPc2	Iblra
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Technický	technický	k2eAgMnSc1d1	technický
průvodce	průvodce	k1gMnSc1	průvodce
energetika	energetika	k1gFnSc1	energetika
<g/>
,	,	kIx,	,
BEN	Ben	k1gInSc1	Ben
-	-	kIx~	-
technická	technický	k2eAgFnSc1d1	technická
literatura	literatura	k1gFnSc1	literatura
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7300-026-1	[number]	k4	80-7300-026-1
VOHLÍDAL	VOHLÍDAL	kA	VOHLÍDAL
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
;	;	kIx,	;
ŠTULÍK	štulík	k1gInSc1	štulík
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
;	;	kIx,	;
JULÁK	JULÁK	kA	JULÁK
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
<g/>
.	.	kIx.	.
</s>
<s>
Chemické	chemický	k2eAgFnPc1d1	chemická
a	a	k8xC	a
analytické	analytický	k2eAgFnPc1d1	analytická
tabulky	tabulka	k1gFnPc1	tabulka
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Grada	Grada	k1gFnSc1	Grada
Publishing	Publishing	k1gInSc1	Publishing
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7169	[number]	k4	7169
<g/>
-	-	kIx~	-
<g/>
855	[number]	k4	855
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Kovový	kovový	k2eAgInSc4d1	kovový
vodík	vodík	k1gInSc4	vodík
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
vodík	vodík	k1gInSc1	vodík
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
vodík	vodík	k1gInSc1	vodík
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Basic	Basic	kA	Basic
Hydrogen	Hydrogen	k1gInSc1	Hydrogen
Calculations	Calculations	k1gInSc1	Calculations
of	of	k?	of
Quantum	Quantum	k1gNnSc1	Quantum
Mechanics	Mechanics	k1gInSc4	Mechanics
Hydrogen	Hydrogen	k1gInSc1	Hydrogen
(	(	kIx(	(
<g/>
University	universita	k1gFnPc1	universita
of	of	k?	of
Nottingham	Nottingham	k1gInSc1	Nottingham
<g/>
)	)	kIx)	)
High	High	k1gInSc1	High
temperature	temperatur	k1gMnSc5	temperatur
hydrogen	hydrogen	k1gInSc4	hydrogen
phase	phase	k1gFnSc1	phase
diagram	diagram	k1gInSc1	diagram
Wavefunction	Wavefunction	k1gInSc1	Wavefunction
of	of	k?	of
hydrogen	hydrogen	k1gInSc1	hydrogen
</s>
