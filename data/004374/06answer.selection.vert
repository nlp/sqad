<s>
Jeho	jeho	k3xOp3gInSc1	jeho
pobyt	pobyt	k1gInSc1	pobyt
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
v	v	k7c6	v
letech	let	k1gInPc6	let
1892	[number]	k4	1892
<g/>
–	–	k?	–
<g/>
1895	[number]	k4	1895
mu	on	k3xPp3gMnSc3	on
přinesl	přinést	k5eAaPmAgInS	přinést
další	další	k2eAgFnPc4d1	další
pocty	pocta	k1gFnPc4	pocta
a	a	k8xC	a
definitivně	definitivně	k6eAd1	definitivně
i	i	k9	i
světovou	světový	k2eAgFnSc4d1	světová
proslulost	proslulost	k1gFnSc4	proslulost
<g/>
.	.	kIx.	.
</s>
