<s>
Medvěd	medvěd	k1gMnSc1	medvěd
hnědý	hnědý	k2eAgMnSc1d1	hnědý
(	(	kIx(	(
<g/>
Ursus	Ursus	k1gMnSc1	Ursus
arctos	arctos	k1gMnSc1	arctos
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
medvědovitá	medvědovitý	k2eAgFnSc1d1	medvědovitá
šelma	šelma	k1gFnSc1	šelma
a	a	k8xC	a
jediný	jediný	k2eAgMnSc1d1	jediný
zástupce	zástupce	k1gMnSc1	zástupce
své	svůj	k3xOyFgFnSc2	svůj
čeledi	čeleď	k1gFnSc2	čeleď
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
vyskytoval	vyskytovat	k5eAaImAgInS	vyskytovat
a	a	k8xC	a
vzácně	vzácně	k6eAd1	vzácně
i	i	k9	i
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
na	na	k7c6	na
území	území	k1gNnSc6	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
největší	veliký	k2eAgFnSc4d3	veliký
evropskou	evropský	k2eAgFnSc4d1	Evropská
šelmu	šelma	k1gFnSc4	šelma
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
medvědem	medvěd	k1gMnSc7	medvěd
ledním	lední	k2eAgMnSc7d1	lední
o	o	k7c4	o
vůbec	vůbec	k9	vůbec
největší	veliký	k2eAgInSc4d3	veliký
recentní	recentní	k2eAgInSc4d1	recentní
druh	druh	k1gInSc4	druh
suchozemského	suchozemský	k2eAgMnSc2d1	suchozemský
predátora	predátor	k1gMnSc2	predátor
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
Asii	Asie	k1gFnSc6	Asie
a	a	k8xC	a
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
těla	tělo	k1gNnSc2	tělo
<g/>
:	:	kIx,	:
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
m	m	kA	m
Délka	délka	k1gFnSc1	délka
ocasu	ocas	k1gInSc2	ocas
<g/>
:	:	kIx,	:
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
20	[number]	k4	20
cm	cm	kA	cm
Hmotnost	hmotnost	k1gFnSc1	hmotnost
<g/>
:	:	kIx,	:
100	[number]	k4	100
<g/>
-	-	kIx~	-
<g/>
800	[number]	k4	800
kg	kg	kA	kg
Výška	výška	k1gFnSc1	výška
v	v	k7c6	v
kohoutku	kohoutek	k1gInSc6	kohoutek
<g/>
:	:	kIx,	:
87	[number]	k4	87
<g/>
-	-	kIx~	-
<g/>
126	[number]	k4	126
cm	cm	kA	cm
Délka	délka	k1gFnSc1	délka
života	život	k1gInSc2	život
<g/>
:	:	kIx,	:
20	[number]	k4	20
<g/>
-	-	kIx~	-
<g/>
30	[number]	k4	30
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
se	se	k3xPyFc4	se
výjimečně	výjimečně	k6eAd1	výjimečně
mohou	moct	k5eAaImIp3nP	moct
dožít	dožít	k5eAaPmF	dožít
až	až	k9	až
přes	přes	k7c4	přes
40	[number]	k4	40
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Medvěd	medvěd	k1gMnSc1	medvěd
hnědý	hnědý	k2eAgMnSc1d1	hnědý
je	být	k5eAaImIp3nS	být
mohutná	mohutný	k2eAgFnSc1d1	mohutná
šelma	šelma	k1gFnSc1	šelma
se	s	k7c7	s
silnými	silný	k2eAgFnPc7d1	silná
končetinami	končetina	k1gFnPc7	končetina
s	s	k7c7	s
velkými	velký	k2eAgInPc7d1	velký
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
15	[number]	k4	15
cm	cm	kA	cm
dlouhými	dlouhý	k2eAgInPc7d1	dlouhý
drápy	dráp	k1gInPc7	dráp
<g/>
,	,	kIx,	,
dlouhou	dlouhý	k2eAgFnSc7d1	dlouhá
srstí	srst	k1gFnSc7	srst
a	a	k8xC	a
velkou	velký	k2eAgFnSc7d1	velká
kulatou	kulatý	k2eAgFnSc7d1	kulatá
hlavou	hlava	k1gFnSc7	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Zbarvení	zbarvení	k1gNnSc1	zbarvení
srsti	srst	k1gFnSc2	srst
se	se	k3xPyFc4	se
značně	značně	k6eAd1	značně
liší	lišit	k5eAaImIp3nS	lišit
podle	podle	k7c2	podle
několika	několik	k4yIc2	několik
poddruhů	poddruh	k1gInPc2	poddruh
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
všeobecně	všeobecně	k6eAd1	všeobecně
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
od	od	k7c2	od
žlutavě	žlutavě	k6eAd1	žlutavě
plavé	plavý	k2eAgFnSc2d1	plavá
až	až	k9	až
po	po	k7c4	po
tmavě	tmavě	k6eAd1	tmavě
černou	černý	k2eAgFnSc4d1	černá
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
jedinci	jedinec	k1gMnPc1	jedinec
mají	mít	k5eAaImIp3nP	mít
navíc	navíc	k6eAd1	navíc
bílý	bílý	k2eAgInSc4d1	bílý
nebo	nebo	k8xC	nebo
stříbřitý	stříbřitý	k2eAgInSc4d1	stříbřitý
odstín	odstín	k1gInSc4	odstín
srsti	srst	k1gFnSc2	srst
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
prošedivělý	prošedivělý	k2eAgInSc4d1	prošedivělý
vzhled	vzhled	k1gInSc4	vzhled
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
velikost	velikost	k1gFnSc1	velikost
není	být	k5eNaImIp3nS	být
pevně	pevně	k6eAd1	pevně
stanovena	stanovit	k5eAaPmNgFnS	stanovit
a	a	k8xC	a
kolísá	kolísat	k5eAaImIp3nS	kolísat
u	u	k7c2	u
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
populací	populace	k1gFnPc2	populace
podle	podle	k7c2	podle
množství	množství	k1gNnSc2	množství
dostupné	dostupný	k2eAgFnSc2d1	dostupná
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Nejmenším	malý	k2eAgInSc7d3	nejmenší
poddruhem	poddruh	k1gInSc7	poddruh
je	být	k5eAaImIp3nS	být
přitom	přitom	k6eAd1	přitom
medvěd	medvěd	k1gMnSc1	medvěd
syrský	syrský	k2eAgMnSc1d1	syrský
a	a	k8xC	a
největším	veliký	k2eAgMnSc7d3	veliký
medvěd	medvěd	k1gMnSc1	medvěd
kodiak	kodiak	k1gInSc1	kodiak
<g/>
.	.	kIx.	.
</s>
<s>
Přítomnost	přítomnost	k1gFnSc1	přítomnost
medvěda	medvěd	k1gMnSc2	medvěd
hnědého	hnědý	k2eAgInSc2d1	hnědý
můžeme	moct	k5eAaImIp1nP	moct
zaznamenat	zaznamenat	k5eAaPmF	zaznamenat
i	i	k9	i
podle	podle	k7c2	podle
jeho	jeho	k3xOp3gFnPc2	jeho
charakteristických	charakteristický	k2eAgFnPc2d1	charakteristická
stop	stopa	k1gFnPc2	stopa
-	-	kIx~	-
přední	přední	k2eAgMnSc1d1	přední
je	být	k5eAaImIp3nS	být
širší	široký	k2eAgInSc1d2	širší
<g/>
,	,	kIx,	,
zadní	zadní	k2eAgInSc1d1	zadní
připomíná	připomínat	k5eAaImIp3nS	připomínat
otisk	otisk	k1gInSc1	otisk
lidské	lidský	k2eAgFnSc2d1	lidská
nohy	noha	k1gFnSc2	noha
<g/>
,	,	kIx,	,
akorát	akorát	k6eAd1	akorát
má	mít	k5eAaImIp3nS	mít
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
ní	on	k3xPp3gFnSc2	on
viditelné	viditelný	k2eAgNnSc1d1	viditelné
silné	silný	k2eAgInPc1d1	silný
drápy	dráp	k1gInPc1	dráp
<g/>
.	.	kIx.	.
</s>
<s>
Pohlaví	pohlaví	k1gNnSc1	pohlaví
se	se	k3xPyFc4	se
od	od	k7c2	od
sebe	se	k3xPyFc2	se
liší	lišit	k5eAaImIp3nP	lišit
pouze	pouze	k6eAd1	pouze
velikostí	velikost	k1gFnPc2	velikost
(	(	kIx(	(
<g/>
samec	samec	k1gMnSc1	samec
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
38	[number]	k4	38
<g/>
-	-	kIx~	-
<g/>
50	[number]	k4	50
%	%	kIx~	%
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
samice	samice	k1gFnSc1	samice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mladí	mladý	k2eAgMnPc1d1	mladý
jedinci	jedinec	k1gMnPc1	jedinec
mají	mít	k5eAaImIp3nP	mít
do	do	k7c2	do
druhého	druhý	k4xOgInSc2	druhý
roku	rok	k1gInSc2	rok
života	život	k1gInSc2	život
bílý	bílý	k2eAgInSc1d1	bílý
pruh	pruh	k1gInSc1	pruh
na	na	k7c6	na
hrdle	hrdla	k1gFnSc6	hrdla
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
počet	počet	k1gInSc1	počet
volně	volně	k6eAd1	volně
žijících	žijící	k2eAgMnPc2d1	žijící
medvědů	medvěd	k1gMnPc2	medvěd
hnědých	hnědý	k2eAgMnPc2d1	hnědý
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
na	na	k7c4	na
200	[number]	k4	200
000	[number]	k4	000
jedinců	jedinec	k1gMnPc2	jedinec
a	a	k8xC	a
dle	dle	k7c2	dle
Červeného	Červeného	k2eAgInSc2d1	Červeného
seznamu	seznam	k1gInSc2	seznam
IUCN	IUCN	kA	IUCN
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
málo	málo	k6eAd1	málo
dotčený	dotčený	k2eAgInSc4d1	dotčený
druh	druh	k1gInSc4	druh
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnPc1d3	veliký
populace	populace	k1gFnPc1	populace
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
(	(	kIx(	(
<g/>
120	[number]	k4	120
000	[number]	k4	000
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
(	(	kIx(	(
<g/>
32	[number]	k4	32
500	[number]	k4	500
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
)	)	kIx)	)
a	a	k8xC	a
Kanadě	Kanada	k1gFnSc6	Kanada
(	(	kIx(	(
<g/>
21	[number]	k4	21
750	[number]	k4	750
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
žije	žít	k5eAaImIp3nS	žít
zhruba	zhruba	k6eAd1	zhruba
14	[number]	k4	14
000	[number]	k4	000
jedinců	jedinec	k1gMnPc2	jedinec
v	v	k7c6	v
desíti	deset	k4xCc6	deset
oddělených	oddělený	k2eAgFnPc6d1	oddělená
populacích	populace	k1gFnPc6	populace
<g/>
,	,	kIx,	,
od	od	k7c2	od
západního	západní	k2eAgNnSc2d1	západní
Španělska	Španělsko	k1gNnSc2	Španělsko
po	po	k7c4	po
východní	východní	k2eAgNnSc4d1	východní
Rusko	Rusko	k1gNnSc4	Rusko
a	a	k8xC	a
od	od	k7c2	od
severní	severní	k2eAgFnSc2d1	severní
Skandinávie	Skandinávie	k1gFnSc2	Skandinávie
po	po	k7c4	po
jižní	jižní	k2eAgNnSc4d1	jižní
Rumunsko	Rumunsko	k1gNnSc4	Rumunsko
a	a	k8xC	a
Bulharsko	Bulharsko	k1gNnSc4	Bulharsko
<g/>
.	.	kIx.	.
</s>
<s>
Nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
na	na	k7c6	na
Britských	britský	k2eAgInPc6d1	britský
ostrovech	ostrov	k1gInPc6	ostrov
a	a	k8xC	a
ve	v	k7c6	v
Skandinávii	Skandinávie	k1gFnSc6	Skandinávie
a	a	k8xC	a
Francii	Francie	k1gFnSc6	Francie
je	být	k5eAaImIp3nS	být
kriticky	kriticky	k6eAd1	kriticky
ohrožený	ohrožený	k2eAgMnSc1d1	ohrožený
<g/>
,	,	kIx,	,
velice	velice	k6eAd1	velice
nízký	nízký	k2eAgInSc1d1	nízký
počet	počet	k1gInSc1	počet
volně	volně	k6eAd1	volně
žijících	žijící	k2eAgMnPc2d1	žijící
jedinců	jedinec	k1gMnPc2	jedinec
byl	být	k5eAaImAgInS	být
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Karpatské	karpatský	k2eAgFnSc2d1	Karpatská
populace	populace	k1gFnSc2	populace
medvěda	medvěd	k1gMnSc2	medvěd
hnědého	hnědý	k2eAgMnSc2d1	hnědý
jsou	být	k5eAaImIp3nP	být
nejpočetnější	početní	k2eAgInPc1d3	nejpočetnější
v	v	k7c6	v
evropské	evropský	k2eAgFnSc6d1	Evropská
části	část	k1gFnSc6	část
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žije	žít	k5eAaImIp3nS	žít
4	[number]	k4	4
500	[number]	k4	500
až	až	k9	až
5	[number]	k4	5
000	[number]	k4	000
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Prehistorické	prehistorický	k2eAgInPc1d1	prehistorický
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
potvrzují	potvrzovat	k5eAaImIp3nP	potvrzovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
medvěd	medvěd	k1gMnSc1	medvěd
hnědý	hnědý	k2eAgMnSc1d1	hnědý
dříve	dříve	k6eAd2	dříve
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
a	a	k8xC	a
v	v	k7c6	v
pohoří	pohoří	k1gNnSc6	pohoří
Atlas	Atlas	k1gInSc1	Atlas
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnohých	mnohý	k2eAgFnPc6d1	mnohá
oblastech	oblast	k1gFnPc6	oblast
původního	původní	k2eAgInSc2d1	původní
areálu	areál	k1gInSc2	areál
rozšíření	rozšíření	k1gNnSc2	rozšíření
je	být	k5eAaImIp3nS	být
však	však	k9	však
nyní	nyní	k6eAd1	nyní
zaniklý	zaniklý	k2eAgMnSc1d1	zaniklý
nebo	nebo	k8xC	nebo
kriticky	kriticky	k6eAd1	kriticky
ohrožený	ohrožený	k2eAgMnSc1d1	ohrožený
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
poddruh	poddruh	k1gInSc1	poddruh
medvěda	medvěd	k1gMnSc2	medvěd
hnědého	hnědý	k2eAgMnSc2d1	hnědý
zvaný	zvaný	k2eAgInSc4d1	zvaný
medvěd	medvěd	k1gMnSc1	medvěd
grizzly	grizzly	k1gMnSc1	grizzly
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
Aljašce	Aljaška	k1gFnSc6	Aljaška
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
velice	velice	k6eAd1	velice
hojní	hojnit	k5eAaImIp3nP	hojnit
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
na	na	k7c6	na
východě	východ	k1gInSc6	východ
od	od	k7c2	od
Yukonu	Yukon	k1gInSc2	Yukon
po	po	k7c4	po
Severozápadní	severozápadní	k2eAgNnPc4d1	severozápadní
teritoria	teritorium	k1gNnPc4	teritorium
a	a	k8xC	a
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
od	od	k7c2	od
Britské	britský	k2eAgFnSc2d1	britská
Kolumbie	Kolumbie	k1gFnSc2	Kolumbie
po	po	k7c4	po
západní	západní	k2eAgMnPc4d1	západní
Alberty	Albert	k1gMnPc4	Albert
<g/>
.	.	kIx.	.
</s>
<s>
Izolované	izolovaný	k2eAgFnPc1d1	izolovaná
populace	populace	k1gFnPc1	populace
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
severozápadním	severozápadní	k2eAgInSc6d1	severozápadní
Washingtonu	Washington	k1gInSc6	Washington
<g/>
,	,	kIx,	,
severním	severní	k2eAgInSc6d1	severní
Idahu	Idah	k1gInSc6	Idah
<g/>
,	,	kIx,	,
západní	západní	k2eAgFnSc6d1	západní
Montaně	Montana	k1gFnSc6	Montana
a	a	k8xC	a
severozápadním	severozápadní	k2eAgInSc6d1	severozápadní
Wyomingu	Wyoming	k1gInSc6	Wyoming
<g/>
.	.	kIx.	.
</s>
<s>
Populace	populace	k1gFnSc1	populace
medvědů	medvěd	k1gMnPc2	medvěd
hnědých	hnědý	k2eAgMnPc2d1	hnědý
v	v	k7c6	v
Pyrenejích	Pyreneje	k1gFnPc6	Pyreneje
mezi	mezi	k7c7	mezi
Francií	Francie	k1gFnSc7	Francie
a	a	k8xC	a
Španělskem	Španělsko	k1gNnSc7	Španělsko
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
nízká	nízký	k2eAgFnSc1d1	nízká
a	a	k8xC	a
čítá	čítat	k5eAaImIp3nS	čítat
odhadem	odhad	k1gInSc7	odhad
čtrnáct	čtrnáct	k4xCc4	čtrnáct
až	až	k9	až
osmnáct	osmnáct	k4xCc4	osmnáct
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
navíc	navíc	k6eAd1	navíc
nedostatek	nedostatek	k1gInSc1	nedostatek
samic	samice	k1gFnPc2	samice
schopných	schopný	k2eAgFnPc2d1	schopná
rozmnožování	rozmnožování	k1gNnPc2	rozmnožování
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
bývá	bývat	k5eAaImIp3nS	bývat
díky	díky	k7c3	díky
globálnímu	globální	k2eAgNnSc3d1	globální
oteplování	oteplování	k1gNnSc3	oteplování
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
mu	on	k3xPp3gMnSc3	on
dovoluje	dovolovat	k5eAaImIp3nS	dovolovat
dostávat	dostávat	k5eAaImF	dostávat
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
sever	sever	k1gInSc4	sever
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
hojnější	hojný	k2eAgMnSc1d2	hojnější
i	i	k9	i
v	v	k7c6	v
arktických	arktický	k2eAgFnPc6d1	arktická
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
medvědi	medvěd	k1gMnPc1	medvěd
preferují	preferovat	k5eAaImIp3nP	preferovat
otevřené	otevřený	k2eAgFnPc4d1	otevřená
krajiny	krajina	k1gFnPc4	krajina
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
Eurasii	Eurasie	k1gFnSc6	Eurasie
se	se	k3xPyFc4	se
zdržují	zdržovat	k5eAaImIp3nP	zdržovat
spíše	spíše	k9	spíše
skryti	skryt	k2eAgMnPc1d1	skryt
v	v	k7c6	v
horách	hora	k1gFnPc6	hora
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
vlhkých	vlhký	k2eAgInPc6d1	vlhký
lesních	lesní	k2eAgInPc6d1	lesní
komplexech	komplex	k1gInPc6	komplex
a	a	k8xC	a
odlehlých	odlehlý	k2eAgInPc6d1	odlehlý
krajích	kraj	k1gInPc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
pro	pro	k7c4	pro
ně	on	k3xPp3gFnPc4	on
představuje	představovat	k5eAaImIp3nS	představovat
největší	veliký	k2eAgFnSc4d3	veliký
hrozbu	hrozba	k1gFnSc4	hrozba
ztráta	ztráta	k1gFnSc1	ztráta
přirozeného	přirozený	k2eAgInSc2d1	přirozený
biomu	biom	k1gInSc2	biom
a	a	k8xC	a
lov	lov	k1gInSc4	lov
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
byl	být	k5eAaImAgMnS	být
medvěd	medvěd	k1gMnSc1	medvěd
hnědý	hnědý	k2eAgInSc1d1	hnědý
považovaný	považovaný	k2eAgInSc1d1	považovaný
za	za	k7c2	za
plně	plně	k6eAd1	plně
vyhynulého	vyhynulý	k2eAgInSc2d1	vyhynulý
už	už	k6eAd1	už
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
;	;	kIx,	;
poslední	poslední	k2eAgInSc1d1	poslední
zaznamenaný	zaznamenaný	k2eAgInSc1d1	zaznamenaný
úlovek	úlovek	k1gInSc1	úlovek
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1885	[number]	k4	1885
v	v	k7c6	v
Beskydech	Beskyd	k1gInPc6	Beskyd
(	(	kIx(	(
<g/>
jeho	jeho	k3xOp3gFnSc1	jeho
lebka	lebka	k1gFnSc1	lebka
s	s	k7c7	s
původním	původní	k2eAgInSc7d1	původní
popisem	popis	k1gInSc7	popis
a	a	k8xC	a
datem	datum	k1gNnSc7	datum
ulovení	ulovení	k1gNnSc2	ulovení
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
sbírek	sbírka	k1gFnPc2	sbírka
Muzea	muzeum	k1gNnSc2	muzeum
regionu	region	k1gInSc3	region
Valašsko	Valašsko	k1gNnSc4	Valašsko
v	v	k7c6	v
zámku	zámek	k1gInSc6	zámek
Kinských	Kinská	k1gFnPc2	Kinská
ve	v	k7c6	v
Valašském	valašský	k2eAgNnSc6d1	Valašské
Meziříčí	Meziříčí	k1gNnSc6	Meziříčí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
a	a	k8xC	a
Slezsku	Slezsko	k1gNnSc6	Slezsko
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
stále	stále	k6eAd1	stále
a	a	k8xC	a
občas	občas	k6eAd1	občas
se	se	k3xPyFc4	se
odsud	odsud	k6eAd1	odsud
toulá	toulat	k5eAaImIp3nS	toulat
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
západ	západ	k1gInSc4	západ
<g/>
,	,	kIx,	,
jih	jih	k1gInSc4	jih
nebo	nebo	k8xC	nebo
jihozápad	jihozápad	k1gInSc4	jihozápad
<g/>
.	.	kIx.	.
</s>
<s>
Nejhojnější	hojný	k2eAgFnSc1d3	nejhojnější
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
moravské	moravský	k2eAgFnSc6d1	Moravská
straně	strana	k1gFnSc6	strana
Karpat	Karpaty	k1gInPc2	Karpaty
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
Javorníkách	Javorníky	k1gInPc6	Javorníky
nebo	nebo	k8xC	nebo
Moravskoslezských	moravskoslezský	k2eAgInPc6d1	moravskoslezský
Beskydech	Beskyd	k1gInPc6	Beskyd
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
proniká	pronikat	k5eAaImIp3nS	pronikat
ze	z	k7c2	z
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
významně	významně	k6eAd1	významně
hojnější	hojný	k2eAgMnSc1d2	hojnější
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
vyhlášky	vyhláška	k1gFnSc2	vyhláška
395	[number]	k4	395
<g/>
/	/	kIx~	/
<g/>
1992	[number]	k4	1992
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
je	být	k5eAaImIp3nS	být
medvěd	medvěd	k1gMnSc1	medvěd
hnědý	hnědý	k2eAgMnSc1d1	hnědý
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
kriticky	kriticky	k6eAd1	kriticky
ohrožený	ohrožený	k2eAgMnSc1d1	ohrožený
a	a	k8xC	a
přísně	přísně	k6eAd1	přísně
chráněný	chráněný	k2eAgInSc4d1	chráněný
druh	druh	k1gInSc4	druh
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
není	být	k5eNaImIp3nS	být
medvěd	medvěd	k1gMnSc1	medvěd
hnědý	hnědý	k2eAgMnSc1d1	hnědý
rušen	rušit	k5eAaImNgInS	rušit
člověkem	člověk	k1gMnSc7	člověk
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
aktivní	aktivní	k2eAgMnSc1d1	aktivní
především	především	k6eAd1	především
ve	v	k7c6	v
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pokud	pokud	k8xS	pokud
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
přešel	přejít	k5eAaPmAgInS	přejít
k	k	k7c3	k
nočnímu	noční	k2eAgInSc3d1	noční
způsobu	způsob	k1gInSc3	způsob
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
většinu	většina	k1gFnSc4	většina
roku	rok	k1gInSc2	rok
žije	žít	k5eAaImIp3nS	žít
samotářsky	samotářsky	k6eAd1	samotářsky
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
již	již	k6eAd1	již
několikrát	několikrát	k6eAd1	několikrát
zpozorovány	zpozorován	k2eAgFnPc4d1	zpozorována
skupiny	skupina	k1gFnPc4	skupina
medvědů	medvěd	k1gMnPc2	medvěd
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
vládne	vládnout	k5eAaImIp3nS	vládnout
přísná	přísný	k2eAgFnSc1d1	přísná
hierarchie	hierarchie	k1gFnSc1	hierarchie
založená	založený	k2eAgFnSc1d1	založená
na	na	k7c6	na
věku	věk	k1gInSc6	věk
a	a	k8xC	a
velikosti	velikost	k1gFnSc6	velikost
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
jedinec	jedinec	k1gMnSc1	jedinec
si	se	k3xPyFc3	se
brání	bránit	k5eAaImIp3nS	bránit
teritorium	teritorium	k1gNnSc4	teritorium
velké	velká	k1gFnSc2	velká
několik	několik	k4yIc4	několik
desítek	desítka	k1gFnPc2	desítka
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
zimu	zima	k1gFnSc4	zima
upadá	upadat	k5eAaPmIp3nS	upadat
do	do	k7c2	do
hibernace	hibernace	k1gFnSc2	hibernace
(	(	kIx(	(
<g/>
nepravého	pravý	k2eNgInSc2d1	nepravý
zimního	zimní	k2eAgInSc2d1	zimní
spánku	spánek	k1gInSc2	spánek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
před	před	k7c7	před
kterým	který	k3yIgInSc7	který
musí	muset	k5eAaImIp3nP	muset
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
hmotnosti	hmotnost	k1gFnSc2	hmotnost
i	i	k9	i
vyšší	vysoký	k2eAgFnSc4d2	vyšší
než	než	k8xS	než
900	[number]	k4	900
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
listopadu	listopad	k1gInSc2	listopad
až	až	k8xS	až
prosince	prosinec	k1gInSc2	prosinec
ulehává	ulehávat	k5eAaImIp3nS	ulehávat
na	na	k7c6	na
klidném	klidný	k2eAgMnSc6d1	klidný
a	a	k8xC	a
suchém	suchý	k2eAgNnSc6d1	suché
místě	místo	k1gNnSc6	místo
(	(	kIx(	(
<g/>
např.	např.	kA	např.
ve	v	k7c6	v
skalních	skalní	k2eAgFnPc6d1	skalní
dutinách	dutina	k1gFnPc6	dutina
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
vlastnoručně	vlastnoručně	k6eAd1	vlastnoručně
vyhrabané	vyhrabaný	k2eAgFnSc6d1	vyhrabaná
jámě	jáma	k1gFnSc6	jáma
<g/>
)	)	kIx)	)
a	a	k8xC	a
během	během	k7c2	během
hibernace	hibernace	k1gFnSc2	hibernace
<g/>
,	,	kIx,	,
z	z	k7c2	z
které	který	k3yRgFnSc2	který
se	se	k3xPyFc4	se
probouzí	probouzet	k5eAaImIp3nS	probouzet
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
až	až	k8xS	až
dubnu	duben	k1gInSc6	duben
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
opouští	opouštět	k5eAaImIp3nS	opouštět
svůj	svůj	k3xOyFgInSc4	svůj
brloh	brloh	k1gInSc4	brloh
a	a	k8xC	a
vydává	vydávat	k5eAaPmIp3nS	vydávat
se	se	k3xPyFc4	se
za	za	k7c7	za
potravou	potrava	k1gFnSc7	potrava
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
svůj	svůj	k3xOyFgInSc4	svůj
mohutný	mohutný	k2eAgInSc4d1	mohutný
vzhled	vzhled	k1gInSc4	vzhled
je	být	k5eAaImIp3nS	být
medvěd	medvěd	k1gMnSc1	medvěd
hnědý	hnědý	k2eAgMnSc1d1	hnědý
výborný	výborný	k2eAgMnSc1d1	výborný
běžec	běžec	k1gMnSc1	běžec
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
dokáže	dokázat	k5eAaPmIp3nS	dokázat
běžet	běžet	k5eAaImF	běžet
i	i	k9	i
vyšší	vysoký	k2eAgFnSc7d2	vyšší
rychlostí	rychlost	k1gFnSc7	rychlost
než	než	k8xS	než
45	[number]	k4	45
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
</s>
<s>
Medvěd	medvěd	k1gMnSc1	medvěd
hnědý	hnědý	k2eAgMnSc1d1	hnědý
je	být	k5eAaImIp3nS	být
všežravec	všežravec	k1gMnSc1	všežravec
<g/>
.	.	kIx.	.
</s>
<s>
Živí	živit	k5eAaImIp3nS	živit
se	se	k3xPyFc4	se
širokou	široký	k2eAgFnSc7d1	široká
paletou	paleta	k1gFnSc7	paleta
rostlinné	rostlinný	k2eAgInPc4d1	rostlinný
(	(	kIx(	(
<g/>
lesní	lesní	k2eAgInPc4d1	lesní
plody	plod	k1gInPc4	plod
<g/>
,	,	kIx,	,
kořínky	kořínek	k1gInPc4	kořínek
<g/>
,	,	kIx,	,
zemědělské	zemědělský	k2eAgFnPc4d1	zemědělská
plodiny	plodina	k1gFnPc4	plodina
<g/>
,	,	kIx,	,
houby	houba	k1gFnPc4	houba
<g/>
)	)	kIx)	)
i	i	k9	i
živočišné	živočišný	k2eAgFnSc2d1	živočišná
stravy	strava	k1gFnSc2	strava
(	(	kIx(	(
<g/>
nejčastěji	často	k6eAd3	často
ryby	ryba	k1gFnPc4	ryba
<g/>
,	,	kIx,	,
hmyz	hmyz	k1gInSc4	hmyz
nebo	nebo	k8xC	nebo
malí	malý	k2eAgMnPc1d1	malý
až	až	k8xS	až
středně	středně	k6eAd1	středně
velcí	velký	k2eAgMnPc1d1	velký
savci	savec	k1gMnPc1	savec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
svou	svůj	k3xOyFgFnSc4	svůj
zabijáckou	zabijácký	k2eAgFnSc4d1	zabijácká
pověst	pověst	k1gFnSc4	pověst
tvoří	tvořit	k5eAaImIp3nS	tvořit
až	až	k9	až
90	[number]	k4	90
%	%	kIx~	%
potravy	potrava	k1gFnSc2	potrava
medvěda	medvěd	k1gMnSc2	medvěd
hnědého	hnědý	k2eAgMnSc2d1	hnědý
rostlinná	rostlinný	k2eAgFnSc1d1	rostlinná
strava	strava	k1gFnSc1	strava
<g/>
.	.	kIx.	.
</s>
<s>
Obsah	obsah	k1gInSc1	obsah
jeho	jeho	k3xOp3gFnSc2	jeho
potravy	potrava	k1gFnSc2	potrava
se	se	k3xPyFc4	se
značně	značně	k6eAd1	značně
mění	měnit	k5eAaImIp3nS	měnit
podle	podle	k7c2	podle
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
lokalit	lokalita	k1gFnPc2	lokalita
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
medvědi	medvěd	k1gMnPc1	medvěd
ze	z	k7c2	z
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
spořádají	spořádat	k5eAaPmIp3nP	spořádat
enormní	enormní	k2eAgInSc4d1	enormní
počet	počet	k1gInSc4	počet
hmyzu	hmyz	k1gInSc2	hmyz
během	během	k7c2	během
jednoho	jeden	k4xCgNnSc2	jeden
léta	léto	k1gNnSc2	léto
<g/>
,	,	kIx,	,
mnohdy	mnohdy	k6eAd1	mnohdy
i	i	k9	i
více	hodně	k6eAd2	hodně
jak	jak	k8xS	jak
40	[number]	k4	40
000	[number]	k4	000
jedinců	jedinec	k1gMnPc2	jedinec
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
u	u	k7c2	u
nich	on	k3xPp3gInPc2	on
může	moct	k5eAaImIp3nS	moct
tento	tento	k3xDgInSc4	tento
hmyz	hmyz	k1gInSc4	hmyz
zaplnit	zaplnit	k5eAaPmF	zaplnit
až	až	k9	až
jednu	jeden	k4xCgFnSc4	jeden
třetinu	třetina	k1gFnSc4	třetina
energie	energie	k1gFnSc2	energie
získávané	získávaný	k2eAgFnSc2d1	získávaná
z	z	k7c2	z
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
a	a	k8xC	a
na	na	k7c6	na
Aljašce	Aljaška	k1gFnSc6	Aljaška
tvoří	tvořit	k5eAaImIp3nP	tvořit
hlavní	hlavní	k2eAgFnSc4d1	hlavní
složku	složka	k1gFnSc4	složka
jejich	jejich	k3xOp3gFnSc2	jejich
potravy	potrava	k1gFnSc2	potrava
ryby	ryba	k1gFnSc2	ryba
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
pak	pak	k6eAd1	pak
lososi	losos	k1gMnPc1	losos
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nP	živit
medem	med	k1gInSc7	med
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
bere	brát	k5eAaImIp3nS	brát
lesním	lesní	k2eAgFnPc3d1	lesní
včelám	včela	k1gFnPc3	včela
<g/>
.	.	kIx.	.
</s>
<s>
Medvěd	medvěd	k1gMnSc1	medvěd
hnědý	hnědý	k2eAgMnSc1d1	hnědý
však	však	k9	však
útočí	útočit	k5eAaImIp3nS	útočit
i	i	k9	i
na	na	k7c4	na
mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgNnPc4d2	veliký
zvířata	zvíře	k1gNnPc4	zvíře
<g/>
,	,	kIx,	,
např.	např.	kA	např.
mladé	mladý	k2eAgFnPc4d1	mladá
jeleny	jelena	k1gFnPc4	jelena
<g/>
,	,	kIx,	,
daňky	daněk	k1gMnPc4	daněk
<g/>
,	,	kIx,	,
srnce	srnec	k1gMnPc4	srnec
<g/>
,	,	kIx,	,
jelence	jelenka	k1gFnSc6	jelenka
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
bizony	bizon	k1gMnPc4	bizon
<g/>
,	,	kIx,	,
občas	občas	k6eAd1	občas
zabíjí	zabíjet	k5eAaImIp3nS	zabíjet
i	i	k9	i
hospodářská	hospodářský	k2eAgNnPc4d1	hospodářské
zvířata	zvíře	k1gNnPc4	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
lovu	lov	k1gInSc2	lov
využívá	využívat	k5eAaPmIp3nS	využívat
své	svůj	k3xOyFgInPc4	svůj
ostré	ostrý	k2eAgInPc4d1	ostrý
zuby	zub	k1gInPc4	zub
na	na	k7c4	na
zabití	zabití	k1gNnSc4	zabití
a	a	k8xC	a
rozporcování	rozporcování	k1gNnSc4	rozporcování
kořisti	kořist	k1gFnSc2	kořist
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
vyhledává	vyhledávat	k5eAaImIp3nS	vyhledávat
i	i	k9	i
mršiny	mršina	k1gFnPc4	mršina
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yIgInPc2	který
využívá	využívat	k5eAaPmIp3nS	využívat
svých	svůj	k3xOyFgMnPc2	svůj
ostrých	ostrý	k2eAgMnPc2d1	ostrý
drápů	dráp	k1gInPc2	dráp
pro	pro	k7c4	pro
zastrašení	zastrašení	k1gNnSc4	zastrašení
nebo	nebo	k8xC	nebo
odehnání	odehnání	k1gNnSc4	odehnání
konkurentů	konkurent	k1gMnPc2	konkurent
<g/>
,	,	kIx,	,
např.	např.	kA	např.
vlků	vlk	k1gMnPc2	vlk
<g/>
,	,	kIx,	,
pum	puma	k1gFnPc2	puma
<g/>
,	,	kIx,	,
menších	malý	k2eAgMnPc2d2	menší
medvědů	medvěd	k1gMnPc2	medvěd
nebo	nebo	k8xC	nebo
tygrů	tygr	k1gMnPc2	tygr
<g/>
.	.	kIx.	.
</s>
<s>
Medvěd	medvěd	k1gMnSc1	medvěd
hnědý	hnědý	k2eAgMnSc1d1	hnědý
se	se	k3xPyFc4	se
páří	pářit	k5eAaImIp3nS	pářit
od	od	k7c2	od
května	květen	k1gInSc2	květen
do	do	k7c2	do
července	červenec	k1gInSc2	červenec
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
druhem	druh	k1gInSc7	druh
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
několika	několik	k4yIc2	několik
dnů	den	k1gInPc2	den
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k8xC	i
týdnů	týden	k1gInPc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnSc1	samice
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
pohlavní	pohlavní	k2eAgFnSc2d1	pohlavní
dospělosti	dospělost	k1gFnSc2	dospělost
v	v	k7c6	v
5	[number]	k4	5
<g/>
.	.	kIx.	.
až	až	k9	až
7	[number]	k4	7
<g/>
.	.	kIx.	.
roce	rok	k1gInSc6	rok
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
samci	samec	k1gMnPc1	samec
se	se	k3xPyFc4	se
páří	pářit	k5eAaImIp3nP	pářit
až	až	k9	až
o	o	k7c4	o
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
až	až	k9	až
když	když	k8xS	když
jsou	být	k5eAaImIp3nP	být
mohutní	mohutný	k2eAgMnPc1d1	mohutný
a	a	k8xC	a
silní	silný	k2eAgMnPc1d1	silný
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
důležitý	důležitý	k2eAgInSc1d1	důležitý
předpoklad	předpoklad	k1gInSc1	předpoklad
při	při	k7c6	při
soubojích	souboj	k1gInPc6	souboj
o	o	k7c4	o
samice	samice	k1gFnPc4	samice
a	a	k8xC	a
území	území	k1gNnPc4	území
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnSc1	samice
má	mít	k5eAaImIp3nS	mít
utajenou	utajený	k2eAgFnSc4d1	utajená
březost	březost	k1gFnSc4	březost
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
časné	časný	k2eAgNnSc1d1	časné
zárodečné	zárodečný	k2eAgNnSc1d1	zárodečné
stadium	stadium	k1gNnSc1	stadium
nevyvíjí	vyvíjet	k5eNaImIp3nS	vyvíjet
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
se	se	k3xPyFc4	se
rodí	rodit	k5eAaImIp3nP	rodit
po	po	k7c6	po
6-8	[number]	k4	6-8
měsíční	měsíční	k2eAgFnSc6d1	měsíční
březosti	březost	k1gFnSc6	březost
při	při	k7c6	při
zimním	zimní	k2eAgInSc6d1	zimní
spánku	spánek	k1gInSc6	spánek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případech	případ	k1gInPc6	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nemá	mít	k5eNaImIp3nS	mít
matka	matka	k1gFnSc1	matka
dostatečné	dostatečný	k2eAgFnPc4d1	dostatečná
tukové	tukový	k2eAgFnPc4d1	tuková
zásoby	zásoba	k1gFnPc4	zásoba
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
často	často	k6eAd1	často
mláďata	mládě	k1gNnPc1	mládě
rodí	rodit	k5eAaImIp3nP	rodit
mrtvá	mrtvý	k2eAgNnPc1d1	mrtvé
nebo	nebo	k8xC	nebo
umírají	umírat	k5eAaImIp3nP	umírat
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
porodu	porod	k1gInSc6	porod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
vrhu	vrh	k1gInSc6	vrh
bývá	bývat	k5eAaImIp3nS	bývat
průměrně	průměrně	k6eAd1	průměrně
jedno	jeden	k4xCgNnSc1	jeden
až	až	k9	až
čtyři	čtyři	k4xCgNnPc4	čtyři
mláďata	mládě	k1gNnPc4	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
rodí	rodit	k5eAaImIp3nS	rodit
dvě	dva	k4xCgFnPc4	dva
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
byly	být	k5eAaImAgInP	být
zaznamenány	zaznamenat	k5eAaPmNgInP	zaznamenat
i	i	k9	i
případy	případ	k1gInPc1	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
měla	mít	k5eAaImAgFnS	mít
samice	samice	k1gFnSc1	samice
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
vrhu	vrh	k1gInSc6	vrh
pět	pět	k4xCc4	pět
mláďat	mládě	k1gNnPc2	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
mláďat	mládě	k1gNnPc2	mládě
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
věku	věk	k1gInSc6	věk
matky	matka	k1gFnSc2	matka
<g/>
,	,	kIx,	,
lokalitě	lokalita	k1gFnSc6	lokalita
a	a	k8xC	a
množství	množství	k1gNnSc6	množství
dostupné	dostupný	k2eAgFnSc2d1	dostupná
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Starší	starý	k2eAgFnPc1d2	starší
samice	samice	k1gFnPc1	samice
rodí	rodit	k5eAaImIp3nP	rodit
zpravidla	zpravidla	k6eAd1	zpravidla
větší	veliký	k2eAgNnPc1d2	veliký
mláďata	mládě	k1gNnPc1	mládě
než	než	k8xS	než
samice	samice	k1gFnPc1	samice
mladší	mladý	k2eAgFnPc1d2	mladší
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
jsou	být	k5eAaImIp3nP	být
po	po	k7c6	po
narození	narození	k1gNnSc6	narození
slepá	slepý	k2eAgFnSc1d1	slepá
<g/>
,	,	kIx,	,
bezzubá	bezzubý	k2eAgFnSc1d1	bezzubá
<g/>
,	,	kIx,	,
neosrstěná	osrstěný	k2eNgFnSc1d1	neosrstěná
a	a	k8xC	a
váží	vážit	k5eAaImIp3nS	vážit
zhruba	zhruba	k6eAd1	zhruba
500	[number]	k4	500
g.	g.	k?	g.
Svou	svůj	k3xOyFgFnSc4	svůj
matku	matka	k1gFnSc4	matka
následují	následovat	k5eAaImIp3nP	následovat
a	a	k8xC	a
tuhou	tuhý	k2eAgFnSc4d1	tuhá
potravu	potrava	k1gFnSc4	potrava
si	se	k3xPyFc3	se
začnou	začít	k5eAaPmIp3nP	začít
shánět	shánět	k5eAaImF	shánět
až	až	k9	až
po	po	k7c6	po
čtyřech	čtyři	k4xCgInPc6	čtyři
měsících	měsíc	k1gInPc6	měsíc
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
matky	matka	k1gFnSc2	matka
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
dva	dva	k4xCgInPc1	dva
až	až	k9	až
čtyři	čtyři	k4xCgInPc1	čtyři
roky	rok	k1gInPc1	rok
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
u	u	k7c2	u
jiných	jiný	k2eAgMnPc2d1	jiný
savců	savec	k1gMnPc2	savec
se	se	k3xPyFc4	se
od	od	k7c2	od
matky	matka	k1gFnSc2	matka
učí	učit	k5eAaImIp3nP	učit
pozorováním	pozorování	k1gNnSc7	pozorování
a	a	k8xC	a
napodobováním	napodobování	k1gNnSc7	napodobování
<g/>
.	.	kIx.	.
</s>
<s>
Velice	velice	k6eAd1	velice
často	často	k6eAd1	často
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
dospělý	dospělý	k2eAgMnSc1d1	dospělý
samec	samec	k1gMnSc1	samec
zabije	zabít	k5eAaPmIp3nS	zabít
dospívající	dospívající	k2eAgNnPc4d1	dospívající
mláďata	mládě	k1gNnPc4	mládě
od	od	k7c2	od
jiného	jiné	k1gNnSc2	jiné
samce	samec	k1gMnSc4	samec
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
zdůrazňuje	zdůrazňovat	k5eAaImIp3nS	zdůrazňovat
svou	svůj	k3xOyFgFnSc4	svůj
osobu	osoba	k1gFnSc4	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Starší	starý	k2eAgNnPc1d2	starší
mláďata	mládě	k1gNnPc1	mládě
v	v	k7c6	v
případě	případ	k1gInSc6	případ
nebezpečí	nebezpečí	k1gNnSc2	nebezpečí
nejčastěji	často	k6eAd3	často
prchají	prchat	k5eAaImIp3nP	prchat
na	na	k7c4	na
stromy	strom	k1gInPc4	strom
<g/>
,	,	kIx,	,
po	po	k7c6	po
kterých	který	k3yQgFnPc6	který
umí	umět	k5eAaImIp3nS	umět
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
dospělců	dospělec	k1gMnPc2	dospělec
dobře	dobře	k6eAd1	dobře
šplhat	šplhat	k5eAaImF	šplhat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
dožívá	dožívat	k5eAaImIp3nS	dožívat
průměrně	průměrně	k6eAd1	průměrně
25	[number]	k4	25
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
až	až	k9	až
dvakrát	dvakrát	k6eAd1	dvakrát
tolik	tolik	k6eAd1	tolik
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
sílu	síla	k1gFnSc4	síla
<g/>
,	,	kIx,	,
velikost	velikost	k1gFnSc4	velikost
a	a	k8xC	a
schopnost	schopnost	k1gFnSc4	schopnost
"	"	kIx"	"
<g/>
zázračně	zázračně	k6eAd1	zázračně
obživnout	obživnout	k5eAaPmF	obživnout
<g/>
"	"	kIx"	"
ze	z	k7c2	z
zimního	zimní	k2eAgInSc2d1	zimní
spánku	spánek	k1gInSc2	spánek
byl	být	k5eAaImAgMnS	být
medvěd	medvěd	k1gMnSc1	medvěd
hnědý	hnědý	k2eAgMnSc1d1	hnědý
uctíván	uctívat	k5eAaImNgInS	uctívat
přírodními	přírodní	k2eAgInPc7d1	přírodní
národy	národ	k1gInPc7	národ
téměř	téměř	k6eAd1	téměř
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Eurasii	Eurasie	k1gFnSc6	Eurasie
a	a	k8xC	a
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Nálezy	nález	k1gInPc1	nález
ozdobených	ozdobený	k2eAgFnPc2d1	ozdobená
medvědích	medvědí	k2eAgFnPc2d1	medvědí
lebek	lebka	k1gFnPc2	lebka
<g/>
,	,	kIx,	,
pohřbených	pohřbený	k2eAgMnPc2d1	pohřbený
společně	společně	k6eAd1	společně
s	s	k7c7	s
lidskými	lidský	k2eAgInPc7d1	lidský
ostatky	ostatek	k1gInPc7	ostatek
<g/>
,	,	kIx,	,
pocházejí	pocházet	k5eAaImIp3nP	pocházet
již	již	k6eAd1	již
z	z	k7c2	z
období	období	k1gNnSc2	období
paleolitu	paleolit	k1gInSc2	paleolit
před	před	k7c7	před
více	hodně	k6eAd2	hodně
než	než	k8xS	než
30	[number]	k4	30
000	[number]	k4	000
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Sámy	Sámo	k1gMnPc4	Sámo
<g/>
,	,	kIx,	,
staré	starý	k2eAgMnPc4d1	starý
Finy	Fin	k1gMnPc4	Fin
<g/>
,	,	kIx,	,
Balty	Balt	k1gMnPc4	Balt
a	a	k8xC	a
sibiřské	sibiřský	k2eAgMnPc4d1	sibiřský
domorodce	domorodec	k1gMnPc4	domorodec
byl	být	k5eAaImAgMnS	být
medvěd	medvěd	k1gMnSc1	medvěd
uctívaným	uctívaný	k2eAgMnSc7d1	uctívaný
tvorem	tvor	k1gMnSc7	tvor
<g/>
.	.	kIx.	.
</s>
<s>
Věřilo	věřit	k5eAaImAgNnS	věřit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
pánem	pán	k1gMnSc7	pán
a	a	k8xC	a
hospodářem	hospodář	k1gMnSc7	hospodář
lesa	les	k1gInSc2	les
<g/>
,	,	kIx,	,
příbuzným	příbuzný	k1gMnSc7	příbuzný
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
rozumí	rozumět	k5eAaImIp3nS	rozumět
lidské	lidský	k2eAgFnSc3d1	lidská
řeči	řeč	k1gFnSc3	řeč
<g/>
.	.	kIx.	.
</s>
<s>
Vyslovení	vyslovení	k1gNnSc4	vyslovení
jeho	jeho	k3xOp3gNnSc2	jeho
pravého	pravý	k2eAgNnSc2d1	pravé
jména	jméno	k1gNnSc2	jméno
bylo	být	k5eAaImAgNnS	být
tabu	tabu	k1gNnSc1	tabu
<g/>
,	,	kIx,	,
dostal	dostat	k5eAaPmAgMnS	dostat
proto	proto	k8xC	proto
většinou	většinou	k6eAd1	většinou
opisné	opisný	k2eAgNnSc4d1	opisné
jméno	jméno	k1gNnSc4	jméno
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
i	i	k9	i
případ	případ	k1gInSc1	případ
českého	český	k2eAgNnSc2d1	české
slova	slovo	k1gNnSc2	slovo
medvěd	medvěd	k1gMnSc1	medvěd
(	(	kIx(	(
<g/>
s	s	k7c7	s
pův	pův	k?	pův
<g/>
.	.	kIx.	.
významem	význam	k1gInSc7	význam
"	"	kIx"	"
<g/>
med-vyjed	medyjed	k1gInSc1	med-vyjed
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jedlík	jedlík	k1gMnSc1	jedlík
medu	med	k1gInSc2	med
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
lesních	lesní	k2eAgInPc2d1	lesní
národů	národ	k1gInPc2	národ
jsou	být	k5eAaImIp3nP	být
jeho	jeho	k3xOp3gNnSc1	jeho
tabu	tabu	k2eAgNnSc1d1	tabu
pojmenování	pojmenování	k1gNnSc1	pojmenování
obdivnější	obdivný	k2eAgFnSc2d2	obdivný
a	a	k8xC	a
vznešenější	vznešený	k2eAgFnSc2d2	vznešenější
<g/>
:	:	kIx,	:
např.	např.	kA	např.
sibiřští	sibiřský	k2eAgMnPc1d1	sibiřský
Mansové	Mans	k1gMnPc1	Mans
mu	on	k3xPp3gMnSc3	on
říkají	říkat	k5eAaImIp3nP	říkat
torel	torel	k1gInSc4	torel
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
vznešený	vznešený	k2eAgMnSc1d1	vznešený
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Eveni	Eveeň	k1gFnSc6	Eveeň
akbaka	akbak	k1gMnSc4	akbak
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
stařec	stařec	k1gMnSc1	stařec
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
Jakuti	Jakut	k1gMnPc1	Jakut
ehe	ehe	k0	ehe
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
dědeček	dědeček	k1gMnSc1	dědeček
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
také	také	k9	také
na	na	k7c6	na
Zakarpatské	zakarpatský	k2eAgFnSc6d1	Zakarpatská
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
či	či	k8xC	či
v	v	k7c6	v
Bělorusku	Bělorusko	k1gNnSc6	Bělorusko
mu	on	k3xPp3gMnSc3	on
vesničané	vesničan	k1gMnPc1	vesničan
dříve	dříve	k6eAd2	dříve
lichotivě	lichotivě	k6eAd1	lichotivě
říkali	říkat	k5eAaImAgMnP	říkat
ďadko	ďadko	k6eAd1	ďadko
<g/>
.	.	kIx.	.
</s>
<s>
Postoj	postoj	k1gInSc1	postoj
starých	starý	k2eAgMnPc2d1	starý
Finů	Fin	k1gMnPc2	Fin
k	k	k7c3	k
medvědovi	medvěd	k1gMnSc3	medvěd
je	být	k5eAaImIp3nS	být
popsán	popsán	k2eAgInSc1d1	popsán
v	v	k7c6	v
eposu	epos	k1gInSc6	epos
Kalevala	Kalevala	k1gFnSc2	Kalevala
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c4	v
jeho	jeho	k3xOp3gNnPc2	jeho
46	[number]	k4	46
<g/>
.	.	kIx.	.
runě	runa	k1gFnSc6	runa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
čarodějný	čarodějný	k2eAgMnSc1d1	čarodějný
pěvec	pěvec	k1gMnSc1	pěvec
Väinämöinen	Väinämöinna	k1gFnPc2	Väinämöinna
loví	lovit	k5eAaImIp3nS	lovit
medvěda	medvěd	k1gMnSc4	medvěd
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
medvěd	medvěd	k1gMnSc1	medvěd
nazýván	nazývat	k5eAaImNgInS	nazývat
lichotivými	lichotivý	k2eAgNnPc7d1	lichotivé
jmény	jméno	k1gNnPc7	jméno
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Otso	Otso	k1gNnSc1	Otso
s	s	k7c7	s
medovou	medový	k2eAgFnSc7d1	medová
tlapou	tlapa	k1gFnSc7	tlapa
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
označen	označit	k5eAaPmNgMnS	označit
z	z	k7c2	z
potomka	potomek	k1gMnSc2	potomek
bohyně	bohyně	k1gFnSc2	bohyně
lesa	les	k1gInSc2	les
Mielikki	Mielikk	k1gFnSc2	Mielikk
<g/>
.	.	kIx.	.
</s>
<s>
Ulovení	ulovení	k1gNnSc1	ulovení
medvěda	medvěd	k1gMnSc2	medvěd
u	u	k7c2	u
sibiřských	sibiřský	k2eAgInPc2d1	sibiřský
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
u	u	k7c2	u
severoamerických	severoamerický	k2eAgMnPc2d1	severoamerický
Indiánů	Indián	k1gMnPc2	Indián
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Kríů	Krí	k1gInPc2	Krí
<g/>
,	,	kIx,	,
Čipevajanů	Čipevajan	k1gInPc2	Čipevajan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dodnes	dodnes	k6eAd1	dodnes
doprovázejí	doprovázet	k5eAaImIp3nP	doprovázet
složité	složitý	k2eAgInPc4d1	složitý
usmiřovací	usmiřovací	k2eAgInPc4d1	usmiřovací
obřady	obřad	k1gInPc4	obřad
<g/>
,	,	kIx,	,
při	při	k7c6	při
nichž	jenž	k3xRgInPc6	jenž
se	se	k3xPyFc4	se
lovci	lovec	k1gMnSc3	lovec
zabitému	zabitý	k1gMnSc3	zabitý
medvědovi	medvěd	k1gMnSc3	medvěd
omlouvají	omlouvat	k5eAaImIp3nP	omlouvat
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
i	i	k9	i
svalují	svalovat	k5eAaImIp3nP	svalovat
vinu	vina	k1gFnSc4	vina
na	na	k7c4	na
někoho	někdo	k3yInSc4	někdo
jiného	jiný	k2eAgMnSc4d1	jiný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
severní	severní	k2eAgFnSc2d1	severní
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
u	u	k7c2	u
Ainuů	Ainu	k1gMnPc2	Ainu
<g/>
,	,	kIx,	,
Nivchů	Nivcha	k1gMnPc2	Nivcha
a	a	k8xC	a
Ketů	Ket	k1gMnPc2	Ket
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
zvykem	zvyk	k1gInSc7	zvyk
chovat	chovat	k5eAaImF	chovat
medvíďata	medvídě	k1gNnPc4	medvídě
ve	v	k7c6	v
vesnici	vesnice	k1gFnSc6	vesnice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
některé	některý	k3yIgFnPc4	některý
ženy	žena	k1gFnPc4	žena
kojily	kojit	k5eAaImAgFnP	kojit
společně	společně	k6eAd1	společně
s	s	k7c7	s
dětmi	dítě	k1gFnPc7	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Medvíďata	medvídě	k1gNnPc1	medvídě
si	se	k3xPyFc3	se
hrála	hrát	k5eAaImAgFnS	hrát
s	s	k7c7	s
dětmi	dítě	k1gFnPc7	dítě
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
pečlivě	pečlivě	k6eAd1	pečlivě
ošetřována	ošetřován	k2eAgFnSc1d1	ošetřována
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
dorostla	dorůst	k5eAaPmAgFnS	dorůst
<g/>
,	,	kIx,	,
vesničané	vesničan	k1gMnPc1	vesničan
je	on	k3xPp3gInPc4	on
obvykle	obvykle	k6eAd1	obvykle
obětovali	obětovat	k5eAaBmAgMnP	obětovat
a	a	k8xC	a
snědli	snědnout	k5eAaImAgMnP	snědnout
<g/>
.	.	kIx.	.
</s>
<s>
Ainuové	Ainuus	k1gMnPc1	Ainuus
věřili	věřit	k5eAaImAgMnP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
duch	duch	k1gMnSc1	duch
obětovaného	obětovaný	k2eAgMnSc2d1	obětovaný
medvěda	medvěd	k1gMnSc2	medvěd
putuje	putovat	k5eAaImIp3nS	putovat
přímo	přímo	k6eAd1	přímo
do	do	k7c2	do
nebe	nebe	k1gNnSc2	nebe
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
sdělí	sdělit	k5eAaPmIp3nS	sdělit
bohům	bůh	k1gMnPc3	bůh
všechna	všechen	k3xTgNnPc4	všechen
přání	přání	k1gNnPc4	přání
a	a	k8xC	a
prosby	prosba	k1gFnPc4	prosba
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Nivchové	Nivch	k1gMnPc1	Nivch
a	a	k8xC	a
Ketové	Ketus	k1gMnPc1	Ketus
někdy	někdy	k6eAd1	někdy
medvědy	medvěd	k1gMnPc7	medvěd
místo	místo	k7c2	místo
obětování	obětování	k1gNnSc2	obětování
vypustili	vypustit	k5eAaPmAgMnP	vypustit
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
přírody	příroda	k1gFnSc2	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Staří	starý	k2eAgMnPc1d1	starý
Izraelité	izraelita	k1gMnPc1	izraelita
pokládali	pokládat	k5eAaImAgMnP	pokládat
medvěda	medvěd	k1gMnSc4	medvěd
za	za	k7c4	za
zlé	zlá	k1gFnPc4	zlá
a	a	k8xC	a
nebezpečné	bezpečný	k2eNgNnSc4d1	nebezpečné
zvíře	zvíře	k1gNnSc4	zvíře
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
starozákonní	starozákonní	k2eAgFnSc2d1	starozákonní
legendy	legenda	k1gFnSc2	legenda
přivolal	přivolat	k5eAaPmAgMnS	přivolat
prorok	prorok	k1gMnSc1	prorok
Elíša	Elíša	k1gMnSc1	Elíša
<g/>
,	,	kIx,	,
uražený	uražený	k2eAgMnSc1d1	uražený
posměšky	posměšek	k1gInPc7	posměšek
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
dvě	dva	k4xCgFnPc1	dva
medvědice	medvědice	k1gFnPc1	medvědice
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
posměváčky	posměváček	k1gMnPc4	posměváček
napadly	napadnout	k5eAaPmAgInP	napadnout
<g/>
.	.	kIx.	.
</s>
<s>
Medvěd	medvěd	k1gMnSc1	medvěd
měl	mít	k5eAaImAgMnS	mít
své	svůj	k3xOyFgNnSc4	svůj
místo	místo	k1gNnSc4	místo
i	i	k9	i
v	v	k7c6	v
řecké	řecký	k2eAgFnSc6d1	řecká
mytologii	mytologie	k1gFnSc6	mytologie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
spojován	spojovat	k5eAaImNgMnS	spojovat
s	s	k7c7	s
bohyní	bohyně	k1gFnSc7	bohyně
Artemis	Artemis	k1gFnSc1	Artemis
<g/>
.	.	kIx.	.
</s>
<s>
Známý	známý	k2eAgMnSc1d1	známý
je	být	k5eAaImIp3nS	být
příběh	příběh	k1gInSc1	příběh
o	o	k7c4	o
lovkyni	lovkyně	k1gFnSc4	lovkyně
Kallistó	Kallistó	k1gFnSc2	Kallistó
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
následkem	následkem	k7c2	následkem
kletby	kletba	k1gFnSc2	kletba
proměněna	proměnit	k5eAaPmNgFnS	proměnit
v	v	k7c6	v
medvědici	medvědice	k1gFnSc6	medvědice
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
souhvězdím	souhvězdí	k1gNnSc7	souhvězdí
Velké	velký	k2eAgFnSc2d1	velká
medvědice	medvědice	k1gFnSc2	medvědice
<g/>
.	.	kIx.	.
</s>
<s>
Římané	Říman	k1gMnPc1	Říman
medvědy	medvěd	k1gMnPc4	medvěd
zneužívali	zneužívat	k5eAaImAgMnP	zneužívat
k	k	k7c3	k
hrám	hra	k1gFnPc3	hra
v	v	k7c6	v
cirku	cirk	k1gInSc6	cirk
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	on	k3xPp3gNnPc4	on
nutili	nutit	k5eAaImAgMnP	nutit
zápasit	zápasit	k5eAaImF	zápasit
s	s	k7c7	s
gladiátory	gladiátor	k1gMnPc7	gladiátor
nebo	nebo	k8xC	nebo
jinými	jiný	k2eAgNnPc7d1	jiné
zvířaty	zvíře	k1gNnPc7	zvíře
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
jim	on	k3xPp3gMnPc3	on
předhazovali	předhazovat	k5eAaImAgMnP	předhazovat
odsouzené	odsouzený	k2eAgMnPc4d1	odsouzený
zločince	zločinec	k1gMnPc4	zločinec
<g/>
.	.	kIx.	.
</s>
<s>
Přiváželi	přivážet	k5eAaImAgMnP	přivážet
je	on	k3xPp3gNnSc4	on
ve	v	k7c6	v
velkém	velký	k2eAgNnSc6d1	velké
ze	z	k7c2	z
Sýrie	Sýrie	k1gFnSc2	Sýrie
a	a	k8xC	a
z	z	k7c2	z
Germánie	Germánie	k1gFnSc2	Germánie
<g/>
,	,	kIx,	,
např.	např.	kA	např.
císař	císař	k1gMnSc1	císař
Gordianus	Gordianus	k1gMnSc1	Gordianus
III	III	kA	III
<g/>
.	.	kIx.	.
dal	dát	k5eAaPmAgMnS	dát
vypustit	vypustit	k5eAaPmF	vypustit
do	do	k7c2	do
arény	aréna	k1gFnSc2	aréna
300	[number]	k4	300
medvědů	medvěd	k1gMnPc2	medvěd
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
pobavení	pobavení	k1gNnSc4	pobavení
diváků	divák	k1gMnPc2	divák
navzájem	navzájem	k6eAd1	navzájem
rozsápali	rozsápat	k5eAaPmAgMnP	rozsápat
<g/>
.	.	kIx.	.
</s>
<s>
Medvědi	medvěd	k1gMnPc1	medvěd
měli	mít	k5eAaImAgMnP	mít
význam	význam	k1gInSc4	význam
i	i	k9	i
v	v	k7c6	v
kultuře	kultura	k1gFnSc6	kultura
starověké	starověký	k2eAgFnSc2d1	starověká
Indie	Indie	k1gFnSc2	Indie
<g/>
,	,	kIx,	,
Číny	Čína	k1gFnSc2	Čína
<g/>
,	,	kIx,	,
Korey	Korea	k1gFnSc2	Korea
a	a	k8xC	a
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
se	se	k3xPyFc4	se
však	však	k9	však
nejednalo	jednat	k5eNaImAgNnS	jednat
o	o	k7c4	o
medvěda	medvěd	k1gMnSc4	medvěd
hnědého	hnědý	k2eAgMnSc4d1	hnědý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jiné	jiný	k2eAgInPc1d1	jiný
<g/>
,	,	kIx,	,
menší	malý	k2eAgInPc1d2	menší
druhy	druh	k1gInPc1	druh
<g/>
,	,	kIx,	,
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgMnS	být
medvěd	medvěd	k1gMnSc1	medvěd
pyskatý	pyskatý	k2eAgMnSc1d1	pyskatý
<g/>
,	,	kIx,	,
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Asii	Asie	k1gFnSc6	Asie
medvěd	medvěd	k1gMnSc1	medvěd
ušatý	ušatý	k2eAgInSc1d1	ušatý
<g/>
.	.	kIx.	.
</s>
<s>
Korejský	korejský	k2eAgMnSc1d1	korejský
kulturní	kulturní	k2eAgMnSc1d1	kulturní
hrdina	hrdina	k1gMnSc1	hrdina
a	a	k8xC	a
první	první	k4xOgMnSc1	první
panovník	panovník	k1gMnSc1	panovník
Tangun	Tangun	k1gMnSc1	Tangun
byl	být	k5eAaImAgMnS	být
podle	podle	k7c2	podle
mýtů	mýtus	k1gInPc2	mýtus
potomkem	potomek	k1gMnSc7	potomek
nebešťana	nebešťan	k1gMnSc2	nebešťan
jménem	jméno	k1gNnSc7	jméno
Hwanung	Hwanung	k1gInSc1	Hwanung
a	a	k8xC	a
medvědice	medvědice	k1gFnSc1	medvědice
<g/>
.	.	kIx.	.
</s>
<s>
Medvěda	medvěd	k1gMnSc4	medvěd
si	se	k3xPyFc3	se
vážili	vážit	k5eAaImAgMnP	vážit
i	i	k9	i
staří	starý	k2eAgMnPc1d1	starý
Keltové	Kelt	k1gMnPc1	Kelt
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
ho	on	k3xPp3gMnSc4	on
pokládali	pokládat	k5eAaImAgMnP	pokládat
za	za	k7c4	za
symbol	symbol	k1gInSc4	symbol
síly	síla	k1gFnSc2	síla
a	a	k8xC	a
statečnosti	statečnost	k1gFnSc2	statečnost
<g/>
.	.	kIx.	.
</s>
<s>
Jména	jméno	k1gNnPc1	jméno
keltských	keltský	k2eAgMnPc2d1	keltský
králů	král	k1gMnPc2	král
Artuše	Artuš	k1gMnSc2	Artuš
a	a	k8xC	a
Cormaca	Cormacus	k1gMnSc2	Cormacus
Mac	Mac	kA	Mac
Arta	Arta	k1gMnSc1	Arta
jsou	být	k5eAaImIp3nP	být
odvozena	odvodit	k5eAaPmNgFnS	odvodit
z	z	k7c2	z
keltského	keltský	k2eAgInSc2d1	keltský
názvu	název	k1gInSc2	název
medvěda	medvěd	k1gMnSc2	medvěd
(	(	kIx(	(
<g/>
art	art	k?	art
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Medvěda	medvěd	k1gMnSc4	medvěd
si	se	k3xPyFc3	se
vážili	vážit	k5eAaImAgMnP	vážit
rovněž	rovněž	k9	rovněž
Germáni	Germán	k1gMnPc1	Germán
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
hrdiny	hrdina	k1gMnSc2	hrdina
Beowulfa	Beowulf	k1gMnSc2	Beowulf
znamená	znamenat	k5eAaImIp3nS	znamenat
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
Vlk	Vlk	k1gMnSc1	Vlk
včel	včela	k1gFnPc2	včela
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
opět	opět	k6eAd1	opět
opisné	opisný	k2eAgNnSc1d1	opisné
označení	označení	k1gNnSc1	označení
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
kenning	kenning	k1gInSc1	kenning
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
medvěda	medvěd	k1gMnSc4	medvěd
<g/>
.	.	kIx.	.
</s>
<s>
Vikinští	vikinský	k2eAgMnPc1d1	vikinský
berserkové	berserkové	k?	berserkové
byli	být	k5eAaImAgMnP	být
elitní	elitní	k2eAgMnPc1d1	elitní
bojovníci	bojovník	k1gMnPc1	bojovník
nadaní	nadaný	k2eAgMnPc1d1	nadaný
medvědí	medvědí	k2eAgMnPc1d1	medvědí
silou	síla	k1gFnSc7	síla
a	a	k8xC	a
zuřivostí	zuřivost	k1gFnSc7	zuřivost
<g/>
,	,	kIx,	,
také	také	k6eAd1	také
jejich	jejich	k3xOp3gInSc1	jejich
název	název	k1gInSc1	název
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
medvěda	medvěd	k1gMnSc2	medvěd
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
motiv	motiv	k1gInSc4	motiv
později	pozdě	k6eAd2	pozdě
využil	využít	k5eAaPmAgMnS	využít
J.	J.	kA	J.
R.	R.	kA	R.
R.	R.	kA	R.
Tolkien	Tolkino	k1gNnPc2	Tolkino
pro	pro	k7c4	pro
postavu	postava	k1gFnSc4	postava
Medděda	Medděd	k1gMnSc2	Medděd
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
románu	román	k1gInSc6	román
Hobit	hobit	k1gMnSc1	hobit
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
germánského	germánský	k2eAgInSc2d1	germánský
názvu	název	k1gInSc2	název
medvěda	medvěd	k1gMnSc4	medvěd
jsou	být	k5eAaImIp3nP	být
odvozena	odvozen	k2eAgNnPc4d1	odvozeno
vlastní	vlastní	k2eAgNnPc4d1	vlastní
jména	jméno	k1gNnPc4	jméno
Björn	Björna	k1gFnPc2	Björna
<g/>
,	,	kIx,	,
Berthold	Berthold	k1gMnSc1	Berthold
<g/>
,	,	kIx,	,
Berengarie	Berengarie	k1gFnSc1	Berengarie
či	či	k8xC	či
Bernard	Bernard	k1gMnSc1	Bernard
<g/>
,	,	kIx,	,
z	z	k7c2	z
keltského	keltský	k2eAgInSc2d1	keltský
jméno	jméno	k1gNnSc4	jméno
Artur	Artur	k1gMnSc1	Artur
a	a	k8xC	a
z	z	k7c2	z
latinského	latinský	k2eAgNnSc2d1	latinské
jména	jméno	k1gNnSc2	jméno
Uršula	Uršula	k1gFnSc1	Uršula
a	a	k8xC	a
Voršila	Voršila	k1gFnSc1	Voršila
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
mnoho	mnoho	k4c1	mnoho
pohádek	pohádka	k1gFnPc2	pohádka
o	o	k7c6	o
medvědech	medvěd	k1gMnPc6	medvěd
<g/>
,	,	kIx,	,
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
např.	např.	kA	např.
původně	původně	k6eAd1	původně
anglická	anglický	k2eAgFnSc1d1	anglická
pohádka	pohádka	k1gFnSc1	pohádka
o	o	k7c6	o
třech	tři	k4xCgMnPc6	tři
medvědech	medvěd	k1gMnPc6	medvěd
<g/>
,	,	kIx,	,
známá	známý	k2eAgFnSc1d1	známá
je	být	k5eAaImIp3nS	být
i	i	k9	i
skotská	skotský	k2eAgFnSc1d1	skotská
pohádka	pohádka	k1gFnSc1	pohádka
o	o	k7c6	o
hnědém	hnědý	k2eAgMnSc6d1	hnědý
medvědu	medvěd	k1gMnSc6	medvěd
ze	z	k7c2	z
Zeleného	zelené	k1gNnSc2	zelené
údolí	údolí	k1gNnSc2	údolí
(	(	kIx(	(
<g/>
či	či	k8xC	či
Hnědém	hnědý	k2eAgMnSc6d1	hnědý
medvědu	medvěd	k1gMnSc6	medvěd
norském	norský	k2eAgMnSc6d1	norský
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
medvěd	medvěd	k1gMnSc1	medvěd
zakletým	zakletý	k2eAgMnSc7d1	zakletý
princem	princ	k1gMnSc7	princ
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
častý	častý	k2eAgInSc1d1	častý
je	být	k5eAaImIp3nS	být
motiv	motiv	k1gInSc1	motiv
únosu	únos	k1gInSc2	únos
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
manželství	manželství	k1gNnSc1	manželství
<g/>
"	"	kIx"	"
medvěda	medvěd	k1gMnSc4	medvěd
a	a	k8xC	a
dívky	dívka	k1gFnPc4	dívka
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
v	v	k7c6	v
ruské	ruský	k2eAgFnSc6d1	ruská
pohádce	pohádka	k1gFnSc6	pohádka
unesl	unést	k5eAaPmAgMnS	unést
medvěd	medvěd	k1gMnSc1	medvěd
dívku	dívka	k1gFnSc4	dívka
Mášenku	Mášenka	k1gFnSc4	Mášenka
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
ho	on	k3xPp3gMnSc4	on
však	však	k9	však
přelstila	přelstít	k5eAaPmAgFnS	přelstít
a	a	k8xC	a
vrátila	vrátit	k5eAaPmAgFnS	vrátit
se	se	k3xPyFc4	se
k	k	k7c3	k
rodičům	rodič	k1gMnPc3	rodič
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jiné	jiný	k2eAgFnSc6d1	jiná
ruské	ruský	k2eAgFnSc6d1	ruská
pohádce	pohádka	k1gFnSc6	pohádka
se	se	k3xPyFc4	se
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
bohatýru	bohatýr	k1gMnSc6	bohatýr
Ivanu	Ivan	k1gMnSc6	Ivan
Medvídkovi	medvídek	k1gMnSc6	medvídek
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
synem	syn	k1gMnSc7	syn
ženy	žena	k1gFnSc2	žena
a	a	k8xC	a
medvěda	medvěd	k1gMnSc2	medvěd
a	a	k8xC	a
měl	mít	k5eAaImAgInS	mít
medvědí	medvědí	k2eAgFnSc4d1	medvědí
sílu	síla	k1gFnSc4	síla
<g/>
.	.	kIx.	.
</s>
<s>
Podobného	podobný	k2eAgInSc2d1	podobný
původu	původ	k1gInSc2	původ
byl	být	k5eAaImAgInS	být
i	i	k9	i
lotyšský	lotyšský	k2eAgMnSc1d1	lotyšský
bohatýr	bohatýr	k1gMnSc1	bohatýr
Láčplésis	Láčplésis	k1gFnSc2	Láčplésis
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
navíc	navíc	k6eAd1	navíc
medvědí	medvědí	k2eAgNnPc4d1	medvědí
uši	ucho	k1gNnPc4	ucho
<g/>
.	.	kIx.	.
</s>
<s>
Sibiřští	sibiřský	k2eAgMnPc1d1	sibiřský
Evenkové	Evenek	k1gMnPc1	Evenek
si	se	k3xPyFc3	se
vyprávějí	vyprávět	k5eAaImIp3nP	vyprávět
o	o	k7c6	o
ženě	žena	k1gFnSc6	žena
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zabloudila	zabloudit	k5eAaPmAgFnS	zabloudit
v	v	k7c6	v
tajze	tajga	k1gFnSc6	tajga
a	a	k8xC	a
přezimovala	přezimovat	k5eAaBmAgFnS	přezimovat
v	v	k7c6	v
medvědím	medvědí	k2eAgInSc6d1	medvědí
brlohu	brloh	k1gInSc6	brloh
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
medvědem	medvěd	k1gMnSc7	medvěd
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
pak	pak	k9	pak
medvědovi	medvěd	k1gMnSc3	medvěd
porodila	porodit	k5eAaPmAgFnS	porodit
děti	dítě	k1gFnPc1	dítě
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
vyrostli	vyrůst	k5eAaPmAgMnP	vyrůst
siláci	silák	k1gMnPc1	silák
a	a	k8xC	a
šamani	šaman	k1gMnPc1	šaman
<g/>
.	.	kIx.	.
</s>
<s>
Motiv	motiv	k1gInSc1	motiv
ženy	žena	k1gFnSc2	žena
unesené	unesený	k2eAgFnSc2d1	unesená
medvědem	medvěd	k1gMnSc7	medvěd
je	být	k5eAaImIp3nS	být
častý	častý	k2eAgInSc1d1	častý
i	i	k9	i
v	v	k7c6	v
pohádkách	pohádka	k1gFnPc6	pohádka
amerických	americký	k2eAgMnPc2d1	americký
indiánů	indián	k1gMnPc2	indián
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Haidů	Haid	k1gMnPc2	Haid
nebo	nebo	k8xC	nebo
Kríů	Krí	k1gMnPc2	Krí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
u	u	k7c2	u
indiánů	indián	k1gMnPc2	indián
jihoamerických	jihoamerický	k2eAgMnPc2d1	jihoamerický
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
And	Anda	k1gFnPc2	Anda
<g/>
,	,	kIx,	,
tam	tam	k6eAd1	tam
je	být	k5eAaImIp3nS	být
však	však	k9	však
únoscem	únosce	k1gMnSc7	únosce
místní	místní	k2eAgMnSc1d1	místní
druh	druh	k1gMnSc1	druh
-	-	kIx~	-
medvěd	medvěd	k1gMnSc1	medvěd
brýlatý	brýlatý	k2eAgMnSc1d1	brýlatý
<g/>
.	.	kIx.	.
</s>
<s>
Indiáni	Indián	k1gMnPc1	Indián
medvědy	medvěd	k1gMnPc4	medvěd
zobrazovali	zobrazovat	k5eAaImAgMnP	zobrazovat
na	na	k7c6	na
totemových	totemový	k2eAgInPc6d1	totemový
sloupech	sloup	k1gInPc6	sloup
a	a	k8xC	a
uctívali	uctívat	k5eAaImAgMnP	uctívat
je	on	k3xPp3gMnPc4	on
jako	jako	k8xC	jako
své	svůj	k3xOyFgMnPc4	svůj
předky	předek	k1gMnPc4	předek
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgInPc1d1	mnohý
kmeny	kmen	k1gInPc1	kmen
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
Čerokíjové	Čerokíj	k1gMnPc1	Čerokíj
<g/>
,	,	kIx,	,
Kvakiutlové	Kvakiutl	k1gMnPc1	Kvakiutl
nebo	nebo	k8xC	nebo
Arikarové	Arikar	k1gMnPc1	Arikar
tančili	tančit	k5eAaImAgMnP	tančit
medvědí	medvědí	k2eAgInPc4d1	medvědí
tance	tanec	k1gInPc4	tanec
<g/>
,	,	kIx,	,
Tlingitové	Tlingit	k1gMnPc1	Tlingit
nosili	nosit	k5eAaImAgMnP	nosit
medvědí	medvědí	k2eAgMnPc1d1	medvědí
masky	maska	k1gFnSc2	maska
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
medvěd	medvěd	k1gMnSc1	medvěd
bylo	být	k5eAaImAgNnS	být
součástí	součást	k1gFnPc2	součást
mnoha	mnoho	k4c2	mnoho
indiánských	indiánský	k2eAgNnPc2d1	indiánské
jmen	jméno	k1gNnPc2	jméno
<g/>
,	,	kIx,	,
stačí	stačit	k5eAaBmIp3nS	stačit
vzpomenout	vzpomenout	k5eAaPmF	vzpomenout
náčelníky	náčelník	k1gInPc4	náčelník
Kiowů	Kiow	k1gMnPc2	Kiow
Stojícího	stojící	k2eAgMnSc2d1	stojící
medvěda	medvěd	k1gMnSc2	medvěd
a	a	k8xC	a
Bílého	bílý	k1gMnSc2	bílý
medvěda	medvěd	k1gMnSc2	medvěd
<g/>
,	,	kIx,	,
náčelníka	náčelník	k1gMnSc2	náčelník
Kríů	Krí	k1gInPc2	Krí
Velkého	velký	k2eAgMnSc2d1	velký
medvěda	medvěd	k1gMnSc2	medvěd
<g/>
,	,	kIx,	,
medicinmana	medicinman	k1gMnSc2	medicinman
Arikarů	Arikar	k1gInPc2	Arikar
Medvědí	medvědí	k2eAgNnSc4d1	medvědí
břicho	břicho	k1gNnSc4	břicho
a	a	k8xC	a
mnohé	mnohé	k1gNnSc4	mnohé
další	další	k2eAgNnSc4d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středověké	středověký	k2eAgFnSc6d1	středověká
Evropě	Evropa	k1gFnSc6	Evropa
medvěd	medvěd	k1gMnSc1	medvěd
své	svůj	k3xOyFgNnSc4	svůj
výsadní	výsadní	k2eAgNnSc4d1	výsadní
postavení	postavení	k1gNnSc4	postavení
mezi	mezi	k7c7	mezi
zvířaty	zvíře	k1gNnPc7	zvíře
ztratil	ztratit	k5eAaPmAgMnS	ztratit
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
lva	lev	k1gMnSc2	lev
<g/>
.	.	kIx.	.
</s>
<s>
Nadále	nadále	k6eAd1	nadále
však	však	k9	však
zůstal	zůstat	k5eAaPmAgMnS	zůstat
oblíbenou	oblíbený	k2eAgFnSc7d1	oblíbená
lovnou	lovný	k2eAgFnSc7d1	lovná
zvěří	zvěř	k1gFnSc7	zvěř
a	a	k8xC	a
erbovními	erbovní	k2eAgFnPc7d1	erbovní
zvířetem	zvíře	k1gNnSc7	zvíře
šlechtických	šlechtický	k2eAgInPc2d1	šlechtický
rodů	rod	k1gInPc2	rod
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Orsiniové	Orsinius	k1gMnPc1	Orsinius
<g/>
,	,	kIx,	,
vévoda	vévoda	k1gMnSc1	vévoda
Albrecht	Albrecht	k1gMnSc1	Albrecht
Medvěd	medvěd	k1gMnSc1	medvěd
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
měst	město	k1gNnPc2	město
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Berlín	Berlín	k1gInSc1	Berlín
<g/>
,	,	kIx,	,
Bern	Bern	k1gInSc1	Bern
<g/>
,	,	kIx,	,
Madrid	Madrid	k1gInSc1	Madrid
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
státních	státní	k2eAgInPc2d1	státní
a	a	k8xC	a
územních	územní	k2eAgInPc2d1	územní
celků	celek	k1gInPc2	celek
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Appenzelského	Appenzelský	k2eAgInSc2d1	Appenzelský
kantonu	kanton	k1gInSc2	kanton
<g/>
,	,	kIx,	,
východního	východní	k2eAgNnSc2d1	východní
Fríska	Frísko	k1gNnSc2	Frísko
<g/>
,	,	kIx,	,
Saska-Anhaltska	Saska-Anhaltsko	k1gNnSc2	Saska-Anhaltsko
<g/>
,	,	kIx,	,
republiky	republika	k1gFnSc2	republika
Karélie	Karélie	k1gFnSc2	Karélie
<g/>
)	)	kIx)	)
či	či	k8xC	či
emblematickým	emblematický	k2eAgNnSc7d1	emblematické
zvířetem	zvíře	k1gNnSc7	zvíře
(	(	kIx(	(
<g/>
Finska	Finsko	k1gNnSc2	Finsko
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
19	[number]	k4	19
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
Ruský	ruský	k2eAgMnSc1d1	ruský
medvěd	medvěd	k1gMnSc1	medvěd
<g/>
"	"	kIx"	"
neoficiálním	oficiální	k2eNgInSc7d1	neoficiální
symbolem	symbol	k1gInSc7	symbol
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
používaným	používaný	k2eAgInSc7d1	používaný
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
karikaturách	karikatura	k1gFnPc6	karikatura
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
plyšový	plyšový	k2eAgMnSc1d1	plyšový
medvídek	medvídek	k1gMnSc1	medvídek
(	(	kIx(	(
<g/>
teddy	teddy	k6eAd1	teddy
bear	bear	k1gInSc1	bear
<g/>
)	)	kIx)	)
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejoblíbenějších	oblíbený	k2eAgFnPc2d3	nejoblíbenější
dětských	dětský	k2eAgFnPc2d1	dětská
hraček	hračka	k1gFnPc2	hračka
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
jeho	jeho	k3xOp3gInSc7	jeho
vzorem	vzor	k1gInSc7	vzor
mezi	mezi	k7c7	mezi
živými	živý	k2eAgNnPc7d1	živé
zvířaty	zvíře	k1gNnPc7	zvíře
není	být	k5eNaImIp3nS	být
žádný	žádný	k3yNgInSc4	žádný
druh	druh	k1gInSc4	druh
medvěda	medvěd	k1gMnSc2	medvěd
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
australský	australský	k2eAgInSc1d1	australský
koala	koala	k1gFnSc1	koala
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
slavným	slavný	k2eAgInSc7d1	slavný
symbolem	symbol	k1gInSc7	symbol
a	a	k8xC	a
maskotem	maskot	k1gInSc7	maskot
polského	polský	k2eAgInSc2d1	polský
odboje	odboj	k1gInSc2	odboj
medvěd	medvěd	k1gMnSc1	medvěd
Wojtek	Wojtek	k1gMnSc1	Wojtek
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgMnSc1	tento
medvěd	medvěd	k1gMnSc1	medvěd
pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
Íránu	Írán	k1gInSc2	Írán
a	a	k8xC	a
patřil	patřit	k5eAaImAgMnS	patřit
generálu	generál	k1gMnSc3	generál
Wladyslawu	Wladyslaw	k1gMnSc3	Wladyslaw
Andersovi	Anders	k1gMnSc3	Anders
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1944	[number]	k4	1944
se	se	k3xPyFc4	se
Wojtek	Wojtek	k1gInSc1	Wojtek
účastnil	účastnit	k5eAaImAgInS	účastnit
bitvy	bitva	k1gFnSc2	bitva
o	o	k7c6	o
Monte	Mont	k1gInSc5	Mont
Cassino	Cassina	k1gFnSc5	Cassina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pomáhal	pomáhat	k5eAaImAgInS	pomáhat
nosit	nosit	k5eAaImF	nosit
dělostřeleckou	dělostřelecký	k2eAgFnSc4d1	dělostřelecká
munici	munice	k1gFnSc4	munice
<g/>
.	.	kIx.	.
</s>
<s>
Bitvu	bitva	k1gFnSc4	bitva
přežil	přežít	k5eAaPmAgMnS	přežít
a	a	k8xC	a
za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
statečnost	statečnost	k1gFnSc4	statečnost
byl	být	k5eAaImAgInS	být
vyznamenán	vyznamenat	k5eAaPmNgInS	vyznamenat
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
získal	získat	k5eAaPmAgMnS	získat
v	v	k7c6	v
polské	polský	k2eAgFnSc6d1	polská
armádě	armáda	k1gFnSc6	armáda
hodnost	hodnost	k1gFnSc1	hodnost
desátníka	desátník	k1gMnSc2	desátník
<g/>
.	.	kIx.	.
</s>
<s>
Válku	válka	k1gFnSc4	válka
přežil	přežít	k5eAaPmAgMnS	přežít
a	a	k8xC	a
zemřel	zemřít	k5eAaPmAgMnS	zemřít
roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
<g/>
.	.	kIx.	.
</s>
<s>
Medvěd	medvěd	k1gMnSc1	medvěd
je	být	k5eAaImIp3nS	být
také	také	k9	také
maskotem	maskot	k1gInSc7	maskot
fotbalového	fotbalový	k2eAgInSc2d1	fotbalový
klubu	klub	k1gInSc2	klub
Bayern	Bayern	k1gInSc1	Bayern
Mnichov	Mnichov	k1gInSc1	Mnichov
nebo	nebo	k8xC	nebo
univerzity	univerzita	k1gFnSc2	univerzita
Berkeley	Berkelea	k1gFnSc2	Berkelea
<g/>
.	.	kIx.	.
</s>
<s>
Medvědi	medvěd	k1gMnPc1	medvěd
byli	být	k5eAaImAgMnP	být
často	často	k6eAd1	často
chováni	chován	k2eAgMnPc1d1	chován
v	v	k7c6	v
hradních	hradní	k2eAgInPc6d1	hradní
příkopech	příkop	k1gInPc6	příkop
nebo	nebo	k8xC	nebo
cvičeni	cvičit	k5eAaImNgMnP	cvičit
k	k	k7c3	k
tanci	tanec	k1gInSc3	tanec
a	a	k8xC	a
předváděni	předvádět	k5eAaImNgMnP	předvádět
jako	jako	k8xC	jako
pouťová	pouťový	k2eAgFnSc1d1	pouťová
atrakce	atrakce	k1gFnSc1	atrakce
<g/>
.	.	kIx.	.
</s>
<s>
Medvědář	medvědář	k1gMnSc1	medvědář
s	s	k7c7	s
ochočeným	ochočený	k2eAgMnSc7d1	ochočený
medvědem	medvěd	k1gMnSc7	medvěd
putoval	putovat	k5eAaImAgMnS	putovat
od	od	k7c2	od
vesnice	vesnice	k1gFnSc2	vesnice
k	k	k7c3	k
vesnici	vesnice	k1gFnSc3	vesnice
a	a	k8xC	a
vydělával	vydělávat	k5eAaImAgMnS	vydělávat
si	se	k3xPyFc3	se
jídlo	jídlo	k1gNnSc4	jídlo
předváděním	předvádění	k1gNnSc7	předvádění
různých	různý	k2eAgInPc2d1	různý
kousků	kousek	k1gInPc2	kousek
<g/>
.	.	kIx.	.
</s>
<s>
Metody	metoda	k1gFnPc1	metoda
výcviku	výcvik	k1gInSc2	výcvik
medvědů	medvěd	k1gMnPc2	medvěd
však	však	k9	však
byly	být	k5eAaImAgInP	být
velmi	velmi	k6eAd1	velmi
tvrdé	tvrdý	k2eAgInPc1d1	tvrdý
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
za	za	k7c7	za
hranicemi	hranice	k1gFnPc7	hranice
týrání	týrání	k1gNnSc2	týrání
<g/>
,	,	kIx,	,
cvičení	cvičený	k2eAgMnPc1d1	cvičený
medvědi	medvěd	k1gMnPc1	medvěd
byli	být	k5eAaImAgMnP	být
ve	v	k7c6	v
špatném	špatný	k2eAgInSc6d1	špatný
zdravotním	zdravotní	k2eAgInSc6d1	zdravotní
stavu	stav	k1gInSc6	stav
následkem	následkem	k7c2	následkem
stresu	stres	k1gInSc2	stres
<g/>
,	,	kIx,	,
podvýživy	podvýživa	k1gFnSc2	podvýživa
a	a	k8xC	a
nemocí	nemoc	k1gFnPc2	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
se	se	k3xPyFc4	se
s	s	k7c7	s
medvědáři	medvědář	k1gMnPc7	medvědář
můžeme	moct	k5eAaImIp1nP	moct
setkat	setkat	k5eAaPmF	setkat
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
především	především	k6eAd1	především
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Indii	Indie	k1gFnSc6	Indie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
používají	používat	k5eAaImIp3nP	používat
medvěda	medvěd	k1gMnSc4	medvěd
pyskatého	pyskatý	k2eAgMnSc4d1	pyskatý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
byly	být	k5eAaImAgFnP	být
oblíbenou	oblíbený	k2eAgFnSc7d1	oblíbená
zábavou	zábava	k1gFnSc7	zábava
šlechty	šlechta	k1gFnSc2	šlechta
také	také	k9	také
zápasy	zápas	k1gInPc4	zápas
medvědů	medvěd	k1gMnPc2	medvěd
s	s	k7c7	s
velkými	velký	k2eAgMnPc7d1	velký
psy	pes	k1gMnPc7	pes
nebo	nebo	k8xC	nebo
s	s	k7c7	s
býky	býk	k1gMnPc7	býk
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
antických	antický	k2eAgInPc2d1	antický
zápasů	zápas	k1gInPc2	zápas
však	však	k9	však
jejich	jejich	k3xOp3gInSc7	jejich
cílem	cíl	k1gInSc7	cíl
nebylo	být	k5eNaImAgNnS	být
vzájemné	vzájemný	k2eAgNnSc1d1	vzájemné
usmrcení	usmrcení	k1gNnSc1	usmrcení
zápasníků	zápasník	k1gMnPc2	zápasník
<g/>
.	.	kIx.	.
</s>
<s>
Milovníkem	milovník	k1gMnSc7	milovník
této	tento	k3xDgFnSc2	tento
zábavy	zábava	k1gFnSc2	zábava
byli	být	k5eAaImAgMnP	být
např.	např.	kA	např.
ruský	ruský	k2eAgMnSc1d1	ruský
car	car	k1gMnSc1	car
Fjodor	Fjodor	k1gMnSc1	Fjodor
II	II	kA	II
<g/>
.	.	kIx.	.
nebo	nebo	k8xC	nebo
saský	saský	k2eAgMnSc1d1	saský
kurfiřt	kurfiřt	k1gMnSc1	kurfiřt
August	August	k1gMnSc1	August
Silný	silný	k2eAgInSc1d1	silný
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
zobrazení	zobrazení	k1gNnSc1	zobrazení
se	se	k3xPyFc4	se
dochovalo	dochovat	k5eAaPmAgNnS	dochovat
např.	např.	kA	např.
na	na	k7c6	na
olejomalbách	olejomalba	k1gFnPc6	olejomalba
anglického	anglický	k2eAgMnSc2d1	anglický
barokního	barokní	k2eAgMnSc2d1	barokní
malíře	malíř	k1gMnSc2	malíř
George	Georg	k1gMnSc2	Georg
Hamiltona	Hamilton	k1gMnSc2	Hamilton
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
19	[number]	k4	19
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
jsou	být	k5eAaImIp3nP	být
cvičení	cvičený	k2eAgMnPc1d1	cvičený
medvědi	medvěd	k1gMnPc1	medvěd
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
atrakcí	atrakce	k1gFnPc2	atrakce
cirkusů	cirkus	k1gInPc2	cirkus
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
také	také	k9	také
mezi	mezi	k7c4	mezi
oblíbené	oblíbený	k2eAgFnPc4d1	oblíbená
a	a	k8xC	a
poměrně	poměrně	k6eAd1	poměrně
nenáročné	náročný	k2eNgFnSc3d1	nenáročná
chovance	chovanka	k1gFnSc3	chovanka
zoologických	zoologický	k2eAgFnPc2d1	zoologická
zahrad	zahrada	k1gFnPc2	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
je	on	k3xPp3gInPc4	on
chovají	chovat	k5eAaImIp3nP	chovat
nejen	nejen	k6eAd1	nejen
Zoo	zoo	k1gFnSc2	zoo
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
Zoo	zoo	k1gFnSc1	zoo
Děčín	Děčín	k1gInSc1	Děčín
<g/>
,	,	kIx,	,
Zoo	zoo	k1gFnSc1	zoo
Dvorec	dvorec	k1gInSc1	dvorec
<g/>
,	,	kIx,	,
Zoo	zoo	k1gFnSc1	zoo
Chomutov	Chomutov	k1gInSc1	Chomutov
<g/>
,	,	kIx,	,
Zoo	zoo	k1gFnSc1	zoo
Plzeň	Plzeň	k1gFnSc1	Plzeň
a	a	k8xC	a
Zoo	zoo	k1gFnSc1	zoo
Tábor	Tábor	k1gInSc1	Tábor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
město	město	k1gNnSc4	město
Beroun	Beroun	k1gInSc1	Beroun
nebo	nebo	k8xC	nebo
některé	některý	k3yIgInPc1	některý
hrady	hrad	k1gInPc1	hrad
a	a	k8xC	a
zámky	zámek	k1gInPc1	zámek
<g/>
.	.	kIx.	.
</s>
<s>
Medvědi	medvěd	k1gMnPc1	medvěd
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
oblíbeni	oblíben	k2eAgMnPc1d1	oblíben
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
učenlivost	učenlivost	k1gFnSc4	učenlivost
<g/>
,	,	kIx,	,
komické	komický	k2eAgInPc4d1	komický
pohyby	pohyb	k1gInPc4	pohyb
a	a	k8xC	a
zdánlivě	zdánlivě	k6eAd1	zdánlivě
dobromyslný	dobromyslný	k2eAgInSc4d1	dobromyslný
vzhled	vzhled	k1gInSc4	vzhled
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
jsou	být	k5eAaImIp3nP	být
ale	ale	k9	ale
značně	značně	k6eAd1	značně
nebezpeční	bezpečný	k2eNgMnPc1d1	nebezpečný
a	a	k8xC	a
způsobili	způsobit	k5eAaPmAgMnP	způsobit
v	v	k7c6	v
ZOO	zoo	k1gFnSc6	zoo
i	i	k8xC	i
v	v	k7c6	v
cirkusech	cirkus	k1gInPc6	cirkus
mnohem	mnohem	k6eAd1	mnohem
více	hodně	k6eAd2	hodně
úrazů	úraz	k1gInPc2	úraz
než	než	k8xS	než
např.	např.	kA	např.
lvi	lev	k1gMnPc1	lev
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
kočkovité	kočkovitý	k2eAgFnPc1d1	kočkovitá
šelmy	šelma	k1gFnPc1	šelma
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
důvod	důvod	k1gInSc1	důvod
je	být	k5eAaImIp3nS	být
ten	ten	k3xDgInSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
medvědi	medvěd	k1gMnPc1	medvěd
mají	mít	k5eAaImIp3nP	mít
slabě	slabě	k6eAd1	slabě
vyvinuté	vyvinutý	k2eAgInPc1d1	vyvinutý
obličejové	obličejový	k2eAgInPc1d1	obličejový
svaly	sval	k1gInPc1	sval
a	a	k8xC	a
tedy	tedy	k9	tedy
i	i	k9	i
málo	málo	k6eAd1	málo
výraznou	výrazný	k2eAgFnSc4d1	výrazná
mimiku	mimika	k1gFnSc4	mimika
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
medvědovi	medvěd	k1gMnSc6	medvěd
tedy	tedy	k8xC	tedy
nelze	lze	k6eNd1	lze
poznat	poznat	k5eAaPmF	poznat
změny	změna	k1gFnPc4	změna
nálady	nálada	k1gFnSc2	nálada
dřív	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
když	když	k8xS	když
se	se	k3xPyFc4	se
bezprostředně	bezprostředně	k6eAd1	bezprostředně
chystá	chystat	k5eAaImIp3nS	chystat
k	k	k7c3	k
útoku	útok	k1gInSc3	útok
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
lovili	lovit	k5eAaImAgMnP	lovit
medvědy	medvěd	k1gMnPc4	medvěd
hnědé	hnědý	k2eAgMnPc4d1	hnědý
i	i	k8xC	i
jeskynní	jeskynní	k2eAgMnPc4d1	jeskynní
už	už	k6eAd1	už
v	v	k7c6	v
paleolitu	paleolit	k1gInSc6	paleolit
<g/>
,	,	kIx,	,
nejen	nejen	k6eAd1	nejen
jako	jako	k8xS	jako
zdroj	zdroj	k1gInSc4	zdroj
masa	maso	k1gNnSc2	maso
a	a	k8xC	a
kůže	kůže	k1gFnSc2	kůže
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
jako	jako	k9	jako
konkurenty	konkurent	k1gMnPc4	konkurent
a	a	k8xC	a
případný	případný	k2eAgInSc1d1	případný
zdroj	zdroj	k1gInSc1	zdroj
nebezpečí	nebezpečí	k1gNnSc2	nebezpečí
<g/>
.	.	kIx.	.
</s>
<s>
Medvědi	medvěd	k1gMnPc1	medvěd
byli	být	k5eAaImAgMnP	být
často	často	k6eAd1	často
ubíjeni	ubíjet	k5eAaImNgMnP	ubíjet
během	během	k7c2	během
zimního	zimní	k2eAgInSc2d1	zimní
spánku	spánek	k1gInSc2	spánek
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
loveni	lovit	k5eAaImNgMnP	lovit
pomocí	pomocí	k7c2	pomocí
různých	různý	k2eAgFnPc2d1	různá
léček	léčka	k1gFnPc2	léčka
a	a	k8xC	a
pastí	past	k1gFnPc2	past
<g/>
.	.	kIx.	.
osobní	osobní	k2eAgInSc1d1	osobní
souboj	souboj	k1gInSc1	souboj
s	s	k7c7	s
medvědem	medvěd	k1gMnSc7	medvěd
byl	být	k5eAaImAgMnS	být
odedávna	odedávna	k6eAd1	odedávna
důkazem	důkaz	k1gInSc7	důkaz
mužnosti	mužnost	k1gFnSc2	mužnost
a	a	k8xC	a
odvahy	odvaha	k1gFnSc2	odvaha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
byly	být	k5eAaImAgFnP	být
k	k	k7c3	k
lovu	lov	k1gInSc3	lov
používány	používat	k5eAaImNgInP	používat
těžké	těžký	k2eAgInPc1d1	těžký
oštěpy	oštěp	k1gInPc1	oštěp
či	či	k8xC	či
kopí	kopí	k1gNnSc1	kopí
<g/>
,	,	kIx,	,
od	od	k7c2	od
19	[number]	k4	19
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
se	se	k3xPyFc4	se
medvědi	medvěd	k1gMnPc1	medvěd
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
loví	lovit	k5eAaImIp3nP	lovit
odstřelem	odstřel	k1gInSc7	odstřel
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
na	na	k7c6	na
čekané	čekaná	k1gFnSc6	čekaná
<g/>
.	.	kIx.	.
</s>
<s>
Obvyklou	obvyklý	k2eAgFnSc7d1	obvyklá
metodou	metoda	k1gFnSc7	metoda
je	být	k5eAaImIp3nS	být
lov	lov	k1gInSc1	lov
na	na	k7c4	na
újedi	újed	k1gMnPc1	újed
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
myslí	myslet	k5eAaImIp3nS	myslet
čekání	čekání	k1gNnSc1	čekání
u	u	k7c2	u
návnady	návnada	k1gFnSc2	návnada
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
mrtvého	mrtvý	k2eAgNnSc2d1	mrtvé
zvířete	zvíře	k1gNnSc2	zvíře
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
USA	USA	kA	USA
a	a	k8xC	a
Kanadě	Kanada	k1gFnSc6	Kanada
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
lov	lov	k1gInSc1	lov
medvěda	medvěd	k1gMnSc4	medvěd
hnědého	hnědý	k2eAgMnSc4d1	hnědý
a	a	k8xC	a
baribala	baribal	k1gMnSc4	baribal
pomocí	pomocí	k7c2	pomocí
psů	pes	k1gMnPc2	pes
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
medvěda	medvěd	k1gMnSc4	medvěd
vyplaší	vyplašit	k5eAaPmIp3nP	vyplašit
a	a	k8xC	a
přinutí	přinutit	k5eAaPmIp3nP	přinutit
k	k	k7c3	k
obraně	obrana	k1gFnSc3	obrana
nebo	nebo	k8xC	nebo
k	k	k7c3	k
útěku	útěk	k1gInSc3	útěk
na	na	k7c4	na
strom	strom	k1gInSc4	strom
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
pro	pro	k7c4	pro
lovce	lovec	k1gMnPc4	lovec
snadným	snadný	k2eAgInSc7d1	snadný
cílem	cíl	k1gInSc7	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
povoleno	povolen	k2eAgNnSc1d1	povoleno
lovit	lovit	k5eAaImF	lovit
pouze	pouze	k6eAd1	pouze
medvědí	medvědí	k2eAgInPc4d1	medvědí
samce	samec	k1gInPc4	samec
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Trofejí	trofej	k1gFnSc7	trofej
z	z	k7c2	z
medvěda	medvěd	k1gMnSc2	medvěd
je	být	k5eAaImIp3nS	být
lebka	lebka	k1gFnSc1	lebka
<g/>
,	,	kIx,	,
kožešina	kožešina	k1gFnSc1	kožešina
a	a	k8xC	a
pyjová	pyjový	k2eAgFnSc1d1	pyjový
kost	kost	k1gFnSc1	kost
<g/>
.	.	kIx.	.
</s>
<s>
Medvědí	medvědí	k2eAgNnSc1d1	medvědí
maso	maso	k1gNnSc1	maso
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
jíst	jíst	k5eAaImF	jíst
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
tmavou	tmavý	k2eAgFnSc4d1	tmavá
barvu	barva	k1gFnSc4	barva
a	a	k8xC	a
chutí	chuť	k1gFnSc7	chuť
se	se	k3xPyFc4	se
poněkud	poněkud	k6eAd1	poněkud
podobá	podobat	k5eAaImIp3nS	podobat
vepřovému	vepřové	k1gNnSc3	vepřové
nebo	nebo	k8xC	nebo
kančí	kančí	k2eAgFnSc3d1	kančí
zvěřině	zvěřina	k1gFnSc3	zvěřina
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
pochoutky	pochoutka	k1gFnSc2	pochoutka
se	se	k3xPyFc4	se
pokládala	pokládat	k5eAaImAgFnS	pokládat
medvědí	medvědí	k2eAgFnSc1d1	medvědí
hlava	hlava	k1gFnSc1	hlava
a	a	k8xC	a
především	především	k9	především
tlapy	tlapa	k1gFnSc2	tlapa
<g/>
.	.	kIx.	.
</s>
<s>
Různým	různý	k2eAgFnPc3d1	různá
částem	část	k1gFnPc3	část
medvědího	medvědí	k2eAgNnSc2d1	medvědí
těla	tělo	k1gNnSc2	tělo
bylo	být	k5eAaImAgNnS	být
připisována	připisován	k2eAgFnSc1d1	připisována
léčivé	léčivý	k2eAgFnSc3d1	léčivá
nebo	nebo	k8xC	nebo
magická	magický	k2eAgFnSc1d1	magická
moc	moc	k1gFnSc1	moc
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
platí	platit	k5eAaImIp3nS	platit
např.	např.	kA	např.
o	o	k7c6	o
medvědím	medvědí	k2eAgInSc6d1	medvědí
tuku	tuk	k1gInSc6	tuk
<g/>
,	,	kIx,	,
a	a	k8xC	a
zejména	zejména	k9	zejména
žluči	žluč	k1gFnSc2	žluč
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
dosud	dosud	k6eAd1	dosud
využívá	využívat	k5eAaPmIp3nS	využívat
tradiční	tradiční	k2eAgFnSc1d1	tradiční
čínská	čínský	k2eAgFnSc1d1	čínská
medicína	medicína	k1gFnSc1	medicína
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
žluči	žluč	k1gFnSc3	žluč
jsou	být	k5eAaImIp3nP	být
dodnes	dodnes	k6eAd1	dodnes
medvědí	medvědí	k2eAgMnPc1d1	medvědí
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
vybíjeni	vybíjen	k2eAgMnPc1d1	vybíjen
pytláky	pytlák	k1gMnPc4	pytlák
nebo	nebo	k8xC	nebo
drženi	držen	k2eAgMnPc1d1	držen
v	v	k7c6	v
nelidských	lidský	k2eNgFnPc6d1	nelidská
podmínkách	podmínka	k1gFnPc6	podmínka
na	na	k7c6	na
farmách	farma	k1gFnPc6	farma
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
žluč	žluč	k1gFnSc4	žluč
odebírá	odebírat	k5eAaImIp3nS	odebírat
pomocí	pomocí	k7c2	pomocí
kanyly	kanyla	k1gFnSc2	kanyla
<g/>
.	.	kIx.	.
</s>
<s>
Léčivé	léčivý	k2eAgInPc1d1	léčivý
účinky	účinek	k1gInPc1	účinek
medvědí	medvědí	k2eAgFnSc2d1	medvědí
žluči	žluč	k1gFnSc2	žluč
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
sporné	sporný	k2eAgInPc1d1	sporný
<g/>
.	.	kIx.	.
</s>
<s>
Méně	málo	k6eAd2	málo
se	se	k3xPyFc4	se
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
se	se	k3xPyFc4	se
medvědím	medvědí	k2eAgNnSc7d1	medvědí
sádlem	sádlo	k1gNnSc7	sádlo
a	a	k8xC	a
žlučí	žluč	k1gFnSc7	žluč
léčilo	léčit	k5eAaImAgNnS	léčit
i	i	k9	i
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Kronikář	kronikář	k1gMnSc1	kronikář
Václav	Václav	k1gMnSc1	Václav
Březan	Březan	k1gMnSc1	Březan
zaznamenal	zaznamenat	k5eAaPmAgMnS	zaznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
medvědí	medvědí	k2eAgFnSc7d1	medvědí
žlučí	žluč	k1gFnSc7	žluč
léčil	léčit	k5eAaImAgMnS	léčit
Vilém	Vilém	k1gMnSc1	Vilém
z	z	k7c2	z
Rožmberka	Rožmberk	k1gInSc2	Rožmberk
<g/>
.	.	kIx.	.
</s>
<s>
Medvěd	medvěd	k1gMnSc1	medvěd
hnědý	hnědý	k2eAgMnSc1d1	hnědý
útočí	útočit	k5eAaImIp3nS	útočit
na	na	k7c4	na
člověka	člověk	k1gMnSc4	člověk
jen	jen	k9	jen
velice	velice	k6eAd1	velice
zřídkakdy	zřídkakdy	k6eAd1	zřídkakdy
a	a	k8xC	a
nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
tak	tak	k9	tak
stává	stávat	k5eAaImIp3nS	stávat
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
ho	on	k3xPp3gMnSc4	on
vyrušíme	vyrušit	k5eAaPmIp1nP	vyrušit
při	při	k7c6	při
konzumaci	konzumace	k1gFnSc6	konzumace
potravy	potrava	k1gFnSc2	potrava
<g/>
,	,	kIx,	,
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
raněný	raněný	k2eAgMnSc1d1	raněný
<g/>
,	,	kIx,	,
nemá	mít	k5eNaImIp3nS	mít
možnost	možnost	k1gFnSc4	možnost
úniku	únik	k1gInSc2	únik
nebo	nebo	k8xC	nebo
pokud	pokud	k8xS	pokud
narazíme	narazit	k5eAaPmIp1nP	narazit
na	na	k7c4	na
samici	samice	k1gFnSc4	samice
s	s	k7c7	s
mláďaty	mládě	k1gNnPc7	mládě
či	či	k8xC	či
na	na	k7c4	na
samce	samec	k1gMnSc4	samec
v	v	k7c6	v
období	období	k1gNnSc6	období
páření	páření	k1gNnSc2	páření
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
problém	problém	k1gInSc1	problém
existuje	existovat	k5eAaImIp3nS	existovat
např.	např.	kA	např.
v	v	k7c6	v
Yellowstonském	Yellowstonský	k2eAgInSc6d1	Yellowstonský
národním	národní	k2eAgInSc6d1	národní
parku	park	k1gInSc6	park
<g/>
,	,	kIx,	,
Rusku	Rusko	k1gNnSc6	Rusko
nebo	nebo	k8xC	nebo
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
v	v	k7c6	v
posledních	poslední	k2eAgInPc6d1	poslední
letech	let	k1gInPc6	let
počet	počet	k1gInSc4	počet
medvědů	medvěd	k1gMnPc2	medvěd
hnědých	hnědý	k2eAgFnPc2d1	hnědá
výrazně	výrazně	k6eAd1	výrazně
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
<g/>
.	.	kIx.	.
</s>
<s>
Medvěd	medvěd	k1gMnSc1	medvěd
hnědý	hnědý	k2eAgMnSc1d1	hnědý
byl	být	k5eAaImAgInS	být
donedávna	donedávna	k6eAd1	donedávna
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
krvelačného	krvelačný	k2eAgMnSc4d1	krvelačný
zabijáka	zabiják	k1gMnSc4	zabiják
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
útočícího	útočící	k2eAgMnSc4d1	útočící
na	na	k7c4	na
domácí	domácí	k2eAgNnSc4d1	domácí
zvířata	zvíře	k1gNnPc4	zvíře
až	až	k6eAd1	až
do	do	k7c2	do
velikosti	velikost	k1gFnSc2	velikost
krávy	kráva	k1gFnSc2	kráva
a	a	k8xC	a
koně	kůň	k1gMnSc4	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Škody	škoda	k1gFnPc1	škoda
na	na	k7c6	na
dobytku	dobytek	k1gInSc6	dobytek
byly	být	k5eAaImAgInP	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
pronásledování	pronásledování	k1gNnPc2	pronásledování
a	a	k8xC	a
vybíjení	vybíjení	k1gNnPc2	vybíjení
medvědů	medvěd	k1gMnPc2	medvěd
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
ve	v	k7c6	v
Skandinávii	Skandinávie	k1gFnSc6	Skandinávie
<g/>
,	,	kIx,	,
Rumunsku	Rumunsko	k1gNnSc6	Rumunsko
či	či	k8xC	či
Pyrenejích	Pyreneje	k1gFnPc6	Pyreneje
přibylo	přibýt	k5eAaPmAgNnS	přibýt
útoků	útok	k1gInPc2	útok
medvědů	medvěd	k1gMnPc2	medvěd
na	na	k7c4	na
dobytek	dobytek	k1gInSc4	dobytek
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nastal	nastat	k5eAaPmAgInS	nastat
ústup	ústup	k1gInSc1	ústup
chovu	chov	k1gInSc2	chov
velkých	velký	k2eAgMnPc2d1	velký
pasteveckých	pastevecký	k2eAgMnPc2d1	pastevecký
psů	pes	k1gMnPc2	pes
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Skandinávii	Skandinávie	k1gFnSc6	Skandinávie
a	a	k8xC	a
Rusku	Rusko	k1gNnSc6	Rusko
někdy	někdy	k6eAd1	někdy
proniká	pronikat	k5eAaImIp3nS	pronikat
i	i	k9	i
do	do	k7c2	do
chlévů	chlév	k1gInPc2	chlév
<g/>
.	.	kIx.	.
kromě	kromě	k7c2	kromě
samotných	samotný	k2eAgNnPc2d1	samotné
zvířat	zvíře	k1gNnPc2	zvíře
zde	zde	k6eAd1	zde
často	často	k6eAd1	často
sežere	sežrat	k5eAaPmIp3nS	sežrat
i	i	k9	i
jejich	jejich	k3xOp3gNnSc1	jejich
krmení	krmení	k1gNnSc1	krmení
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
prasata	prase	k1gNnPc4	prase
<g/>
.	.	kIx.	.
</s>
<s>
Zvířata	zvíře	k1gNnPc1	zvíře
usmrcuje	usmrcovat	k5eAaImIp3nS	usmrcovat
zlomením	zlomení	k1gNnSc7	zlomení
vazu	vaz	k1gInSc2	vaz
a	a	k8xC	a
většinou	většinou	k6eAd1	většinou
je	on	k3xPp3gMnPc4	on
nesežere	sežrat	k5eNaPmIp3nS	sežrat
celá	celý	k2eAgFnSc1d1	celá
<g/>
,	,	kIx,	,
přednost	přednost	k1gFnSc1	přednost
dává	dávat	k5eAaImIp3nS	dávat
vnitřnostem	vnitřnost	k1gFnPc3	vnitřnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
lokalitách	lokalita	k1gFnPc6	lokalita
medvědi	medvěd	k1gMnPc1	medvěd
(	(	kIx(	(
<g/>
nejen	nejen	k6eAd1	nejen
hnědí	hnědit	k5eAaImIp3nS	hnědit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
např.	např.	kA	např.
i	i	k9	i
lední	lední	k2eAgMnPc1d1	lední
a	a	k8xC	a
baribalové	baribal	k1gMnPc1	baribal
<g/>
)	)	kIx)	)
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
navštěvují	navštěvovat	k5eAaImIp3nP	navštěvovat
skládky	skládka	k1gFnPc4	skládka
odpadků	odpadek	k1gInPc2	odpadek
i	i	k8xC	i
ulice	ulice	k1gFnSc2	ulice
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
prohledávají	prohledávat	k5eAaImIp3nP	prohledávat
kontejnery	kontejner	k1gInPc1	kontejner
a	a	k8xC	a
popelnice	popelnice	k1gFnPc1	popelnice
a	a	k8xC	a
hledají	hledat	k5eAaImIp3nP	hledat
poživatelné	poživatelný	k2eAgInPc4d1	poživatelný
odpadky	odpadek	k1gInPc4	odpadek
<g/>
.	.	kIx.	.
</s>
<s>
Poddruh	poddruh	k1gInSc1	poddruh
medvěd	medvěd	k1gMnSc1	medvěd
brtník	brtník	k1gMnSc1	brtník
dostal	dostat	k5eAaPmAgMnS	dostat
své	svůj	k3xOyFgNnSc4	svůj
jméno	jméno	k1gNnSc4	jméno
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
vybírá	vybírat	k5eAaImIp3nS	vybírat
dutiny	dutina	k1gFnPc4	dutina
stromů	strom	k1gInPc2	strom
s	s	k7c7	s
hnízdy	hnízdo	k1gNnPc7	hnízdo
lesních	lesní	k2eAgFnPc2d1	lesní
včel	včela	k1gFnPc2	včela
<g/>
.	.	kIx.	.
</s>
<s>
Brť	brť	k1gFnSc1	brť
je	být	k5eAaImIp3nS	být
totiž	totiž	k9	totiž
staročeské	staročeský	k2eAgNnSc4d1	staročeské
slovo	slovo	k1gNnSc4	slovo
<g/>
,	,	kIx,	,
označující	označující	k2eAgFnSc4d1	označující
dutinu	dutina	k1gFnSc4	dutina
stromu	strom	k1gInSc2	strom
<g/>
.	.	kIx.	.
</s>
<s>
Medvěd	medvěd	k1gMnSc1	medvěd
hnědý	hnědý	k2eAgMnSc1d1	hnědý
byl	být	k5eAaImAgMnS	být
popsán	popsat	k5eAaPmNgMnS	popsat
švédským	švédský	k2eAgMnSc7d1	švédský
přírodovědcem	přírodovědec	k1gMnSc7	přírodovědec
Carlem	Carl	k1gMnSc7	Carl
Linném	Linné	k1gNnSc6	Linné
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
díle	dílo	k1gNnSc6	dílo
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1758	[number]	k4	1758
Systema	Systema	k1gFnSc1	Systema
naturae	naturae	k1gFnSc1	naturae
(	(	kIx(	(
<g/>
Soustava	soustava	k1gFnSc1	soustava
přírody	příroda	k1gFnSc2	příroda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
latinským	latinský	k2eAgInSc7d1	latinský
názvem	název	k1gInSc7	název
Ursus	Ursus	k1gMnSc1	Ursus
arctos	arctos	k1gMnSc1	arctos
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
jméno	jméno	k1gNnSc1	jméno
je	být	k5eAaImIp3nS	být
složeno	složit	k5eAaPmNgNnS	složit
z	z	k7c2	z
latinského	latinský	k2eAgNnSc2d1	latinské
a	a	k8xC	a
řeckého	řecký	k2eAgNnSc2d1	řecké
slova	slovo	k1gNnSc2	slovo
pro	pro	k7c4	pro
medvěda	medvěd	k1gMnSc4	medvěd
<g/>
.	.	kIx.	.
</s>
<s>
Předkem	předek	k1gInSc7	předek
medvěda	medvěd	k1gMnSc2	medvěd
hnědého	hnědý	k2eAgMnSc2d1	hnědý
byl	být	k5eAaImAgInS	být
znatelně	znatelně	k6eAd1	znatelně
větší	veliký	k2eAgInSc1d2	veliký
<g/>
,	,	kIx,	,
až	až	k9	až
4	[number]	k4	4
m	m	kA	m
velký	velký	k2eAgMnSc1d1	velký
medvěd	medvěd	k1gMnSc1	medvěd
jeskynní	jeskynní	k2eAgMnSc1d1	jeskynní
(	(	kIx(	(
<g/>
Ursus	Ursus	k1gMnSc1	Ursus
spelaeus	spelaeus	k1gMnSc1	spelaeus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
loven	loven	k2eAgInSc1d1	loven
našimi	náš	k3xOp1gInPc7	náš
předky	předek	k1gInPc7	předek
<g/>
,	,	kIx,	,
neandrtálci	neandrtálec	k1gMnSc3	neandrtálec
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
maso	maso	k1gNnSc4	maso
a	a	k8xC	a
srst	srst	k1gFnSc4	srst
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kteří	který	k3yRgMnPc1	který
jej	on	k3xPp3gNnSc4	on
též	též	k9	též
používali	používat	k5eAaImAgMnP	používat
jako	jako	k8xC	jako
námět	námět	k1gInSc4	námět
pro	pro	k7c4	pro
sošky	soška	k1gFnPc4	soška
určené	určený	k2eAgNnSc4d1	určené
k	k	k7c3	k
náboženským	náboženský	k2eAgInPc3d1	náboženský
ceremoniálům	ceremoniál	k1gInPc3	ceremoniál
<g/>
.	.	kIx.	.
</s>
<s>
Medvěd	medvěd	k1gMnSc1	medvěd
jeskynní	jeskynní	k2eAgMnSc1d1	jeskynní
<g/>
,	,	kIx,	,
obávaný	obávaný	k2eAgMnSc1d1	obávaný
živočich	živočich	k1gMnSc1	živočich
i	i	k9	i
pro	pro	k7c4	pro
jiné	jiný	k2eAgFnPc4d1	jiná
šelmy	šelma	k1gFnPc4	šelma
<g/>
,	,	kIx,	,
např.	např.	kA	např.
lvy	lev	k1gInPc1	lev
jeskynní	jeskynní	k2eAgInPc1d1	jeskynní
<g/>
,	,	kIx,	,
vyhynul	vyhynout	k5eAaPmAgMnS	vyhynout
zhruba	zhruba	k6eAd1	zhruba
před	před	k7c7	před
10	[number]	k4	10
000	[number]	k4	000
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Medvěd	medvěd	k1gMnSc1	medvěd
kodiak	kodiak	k1gMnSc1	kodiak
(	(	kIx(	(
<g/>
U.	U.	kA	U.
arctos	arctos	k1gInSc1	arctos
middendorffi	middendorffi	k1gNnSc1	middendorffi
<g/>
)	)	kIx)	)
Medvěd	medvěd	k1gMnSc1	medvěd
grizzly	grizzly	k1gMnSc1	grizzly
(	(	kIx(	(
<g/>
U.	U.	kA	U.
arctos	arctos	k1gInSc1	arctos
horribillis	horribillis	k1gFnSc1	horribillis
<g/>
)	)	kIx)	)
Medvěd	medvěd	k1gMnSc1	medvěd
eurasijský	eurasijský	k2eAgMnSc1d1	eurasijský
(	(	kIx(	(
<g/>
U.	U.	kA	U.
arctos	arctos	k1gMnSc1	arctos
arctos	arctos	k1gMnSc1	arctos
<g/>
)	)	kIx)	)
Medvěd	medvěd	k1gMnSc1	medvěd
kamčatský	kamčatský	k2eAgMnSc1d1	kamčatský
(	(	kIx(	(
<g/>
U.	U.	kA	U.
a.	a.	k?	a.
beringianus	beringianus	k1gMnSc1	beringianus
<g/>
)	)	kIx)	)
Medvěd	medvěd	k1gMnSc1	medvěd
kalifornský	kalifornský	k2eAgMnSc1d1	kalifornský
(	(	kIx(	(
<g/>
U.	U.	kA	U.
a.	a.	k?	a.
californicus	californicus	k1gMnSc1	californicus
<g/>
)	)	kIx)	)
Medvěd	medvěd	k1gMnSc1	medvěd
aljašský	aljašský	k2eAgMnSc1d1	aljašský
(	(	kIx(	(
<g/>
U.	U.	kA	U.
<g />
.	.	kIx.	.
</s>
<s>
a.	a.	k?	a.
gyas	gyas	k1gInSc1	gyas
<g/>
)	)	kIx)	)
Medvěd	medvěd	k1gMnSc1	medvěd
plavý	plavý	k2eAgMnSc1d1	plavý
(	(	kIx(	(
<g/>
U.	U.	kA	U.
a.	a.	k?	a.
isabellinus	isabellinus	k1gMnSc1	isabellinus
<g/>
)	)	kIx)	)
Medvěd	medvěd	k1gMnSc1	medvěd
východosibiřský	východosibiřský	k2eAgMnSc1d1	východosibiřský
(	(	kIx(	(
<g/>
U.	U.	kA	U.
a.	a.	k?	a.
jeniseensis	jeniseensis	k1gInSc1	jeniseensis
<g/>
)	)	kIx)	)
Medvěd	medvěd	k1gMnSc1	medvěd
ussurijský	ussurijský	k2eAgMnSc1d1	ussurijský
(	(	kIx(	(
<g/>
U.	U.	kA	U.
a.	a.	k?	a.
lasiotus	lasiotus	k1gMnSc1	lasiotus
<g/>
)	)	kIx)	)
Medvěd	medvěd	k1gMnSc1	medvěd
kavkazský	kavkazský	k2eAgMnSc1d1	kavkazský
(	(	kIx(	(
<g/>
U.	U.	kA	U.
a.	a.	k?	a.
meridionalis	meridionalis	k1gInSc1	meridionalis
<g/>
)	)	kIx)	)
Medvěd	medvěd	k1gMnSc1	medvěd
západokanadský	západokanadský	k2eAgMnSc1d1	západokanadský
(	(	kIx(	(
<g/>
U.	U.	kA	U.
a.	a.	k?	a.
pervagor	pervagor	k1gMnSc1	pervagor
<g/>
)	)	kIx)	)
Medvěd	medvěd	k1gMnSc1	medvěd
tibetský	tibetský	k2eAgMnSc1d1	tibetský
(	(	kIx(	(
<g/>
U.	U.	kA	U.
a.	a.	k?	a.
pruinonsus	pruinonsus	k1gMnSc1	pruinonsus
<g/>
)	)	kIx)	)
Medvěd	medvěd	k1gMnSc1	medvěd
syrský	syrský	k2eAgMnSc1d1	syrský
(	(	kIx(	(
<g/>
U.	U.	kA	U.
a.	a.	k?	a.
syriacus	syriacus	k1gMnSc1	syriacus
<g/>
)	)	kIx)	)
Medvěd	medvěd	k1gMnSc1	medvěd
atlaský	atlaský	k1gMnSc1	atlaský
†	†	k?	†
(	(	kIx(	(
<g/>
U.	U.	kA	U.
a.	a.	k?	a.
crowtheri	crowther	k1gFnSc2	crowther
<g/>
)	)	kIx)	)
Medvěd	medvěd	k1gMnSc1	medvěd
mexický	mexický	k2eAgMnSc1d1	mexický
†	†	k?	†
(	(	kIx(	(
<g/>
U.	U.	kA	U.
a.	a.	k?	a.
nelsoni	nelsoň	k1gFnSc6	nelsoň
<g/>
)	)	kIx)	)
</s>
