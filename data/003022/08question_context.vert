<s>
Medvěd	medvěd	k1gMnSc1	medvěd
hnědý	hnědý	k2eAgMnSc1d1	hnědý
(	(	kIx(	(
<g/>
Ursus	Ursus	k1gMnSc1	Ursus
arctos	arctos	k1gMnSc1	arctos
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
medvědovitá	medvědovitý	k2eAgFnSc1d1	medvědovitá
šelma	šelma	k1gFnSc1	šelma
a	a	k8xC	a
jediný	jediný	k2eAgMnSc1d1	jediný
zástupce	zástupce	k1gMnSc1	zástupce
své	svůj	k3xOyFgFnSc2	svůj
čeledi	čeleď	k1gFnSc2	čeleď
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
vyskytoval	vyskytovat	k5eAaImAgInS	vyskytovat
a	a	k8xC	a
vzácně	vzácně	k6eAd1	vzácně
i	i	k9	i
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
na	na	k7c6	na
území	území	k1gNnSc6	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
největší	veliký	k2eAgFnSc4d3	veliký
evropskou	evropský	k2eAgFnSc4d1	Evropská
šelmu	šelma	k1gFnSc4	šelma
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
medvědem	medvěd	k1gMnSc7	medvěd
ledním	lední	k2eAgMnSc7d1	lední
o	o	k7c4	o
vůbec	vůbec	k9	vůbec
největší	veliký	k2eAgInSc4d3	veliký
recentní	recentní	k2eAgInSc4d1	recentní
druh	druh	k1gInSc4	druh
suchozemského	suchozemský	k2eAgMnSc2d1	suchozemský
predátora	predátor	k1gMnSc2	predátor
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
Asii	Asie	k1gFnSc6	Asie
a	a	k8xC	a
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
těla	tělo	k1gNnSc2	tělo
<g/>
:	:	kIx,	:
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
m	m	kA	m
Délka	délka	k1gFnSc1	délka
ocasu	ocas	k1gInSc2	ocas
<g/>
:	:	kIx,	:
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
20	[number]	k4	20
cm	cm	kA	cm
Hmotnost	hmotnost	k1gFnSc1	hmotnost
<g/>
:	:	kIx,	:
100	[number]	k4	100
<g/>
-	-	kIx~	-
<g/>
800	[number]	k4	800
kg	kg	kA	kg
Výška	výška	k1gFnSc1	výška
v	v	k7c6	v
kohoutku	kohoutek	k1gInSc6	kohoutek
<g/>
:	:	kIx,	:
87	[number]	k4	87
<g/>
-	-	kIx~	-
<g/>
126	[number]	k4	126
cm	cm	kA	cm
Délka	délka	k1gFnSc1	délka
života	život	k1gInSc2	život
<g/>
:	:	kIx,	:
20	[number]	k4	20
<g/>
-	-	kIx~	-
<g/>
30	[number]	k4	30
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
se	se	k3xPyFc4	se
výjimečně	výjimečně	k6eAd1	výjimečně
mohou	moct	k5eAaImIp3nP	moct
dožít	dožít	k5eAaPmF	dožít
až	až	k9	až
přes	přes	k7c4	přes
40	[number]	k4	40
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Medvěd	medvěd	k1gMnSc1	medvěd
hnědý	hnědý	k2eAgMnSc1d1	hnědý
je	být	k5eAaImIp3nS	být
mohutná	mohutný	k2eAgFnSc1d1	mohutná
šelma	šelma	k1gFnSc1	šelma
se	s	k7c7	s
silnými	silný	k2eAgFnPc7d1	silná
končetinami	končetina	k1gFnPc7	končetina
s	s	k7c7	s
velkými	velký	k2eAgInPc7d1	velký
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
15	[number]	k4	15
cm	cm	kA	cm
dlouhými	dlouhý	k2eAgInPc7d1	dlouhý
drápy	dráp	k1gInPc7	dráp
<g/>
,	,	kIx,	,
dlouhou	dlouhý	k2eAgFnSc7d1	dlouhá
srstí	srst	k1gFnSc7	srst
a	a	k8xC	a
velkou	velký	k2eAgFnSc7d1	velká
kulatou	kulatý	k2eAgFnSc7d1	kulatá
hlavou	hlava	k1gFnSc7	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Zbarvení	zbarvení	k1gNnSc1	zbarvení
srsti	srst	k1gFnSc2	srst
se	se	k3xPyFc4	se
značně	značně	k6eAd1	značně
liší	lišit	k5eAaImIp3nS	lišit
podle	podle	k7c2	podle
několika	několik	k4yIc2	několik
poddruhů	poddruh	k1gInPc2	poddruh
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
všeobecně	všeobecně	k6eAd1	všeobecně
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
od	od	k7c2	od
žlutavě	žlutavě	k6eAd1	žlutavě
plavé	plavý	k2eAgFnSc2d1	plavá
až	až	k9	až
po	po	k7c4	po
tmavě	tmavě	k6eAd1	tmavě
černou	černý	k2eAgFnSc4d1	černá
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
jedinci	jedinec	k1gMnPc1	jedinec
mají	mít	k5eAaImIp3nP	mít
navíc	navíc	k6eAd1	navíc
bílý	bílý	k2eAgInSc4d1	bílý
nebo	nebo	k8xC	nebo
stříbřitý	stříbřitý	k2eAgInSc4d1	stříbřitý
odstín	odstín	k1gInSc4	odstín
srsti	srst	k1gFnSc2	srst
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
prošedivělý	prošedivělý	k2eAgInSc4d1	prošedivělý
vzhled	vzhled	k1gInSc4	vzhled
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
velikost	velikost	k1gFnSc1	velikost
není	být	k5eNaImIp3nS	být
pevně	pevně	k6eAd1	pevně
stanovena	stanovit	k5eAaPmNgFnS	stanovit
a	a	k8xC	a
kolísá	kolísat	k5eAaImIp3nS	kolísat
u	u	k7c2	u
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
populací	populace	k1gFnPc2	populace
podle	podle	k7c2	podle
množství	množství	k1gNnSc2	množství
dostupné	dostupný	k2eAgFnSc2d1	dostupná
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Nejmenším	malý	k2eAgInSc7d3	nejmenší
poddruhem	poddruh	k1gInSc7	poddruh
je	být	k5eAaImIp3nS	být
přitom	přitom	k6eAd1	přitom
medvěd	medvěd	k1gMnSc1	medvěd
syrský	syrský	k2eAgMnSc1d1	syrský
a	a	k8xC	a
největším	veliký	k2eAgMnSc7d3	veliký
medvěd	medvěd	k1gMnSc1	medvěd
kodiak	kodiak	k1gInSc1	kodiak
<g/>
.	.	kIx.	.
</s>
<s>
Přítomnost	přítomnost	k1gFnSc1	přítomnost
medvěda	medvěd	k1gMnSc2	medvěd
hnědého	hnědý	k2eAgInSc2d1	hnědý
můžeme	moct	k5eAaImIp1nP	moct
zaznamenat	zaznamenat	k5eAaPmF	zaznamenat
i	i	k9	i
podle	podle	k7c2	podle
jeho	jeho	k3xOp3gFnPc2	jeho
charakteristických	charakteristický	k2eAgFnPc2d1	charakteristická
stop	stopa	k1gFnPc2	stopa
-	-	kIx~	-
přední	přední	k2eAgMnSc1d1	přední
je	být	k5eAaImIp3nS	být
širší	široký	k2eAgInSc1d2	širší
<g/>
,	,	kIx,	,
zadní	zadní	k2eAgInSc1d1	zadní
připomíná	připomínat	k5eAaImIp3nS	připomínat
otisk	otisk	k1gInSc1	otisk
lidské	lidský	k2eAgFnSc2d1	lidská
nohy	noha	k1gFnSc2	noha
<g/>
,	,	kIx,	,
akorát	akorát	k6eAd1	akorát
má	mít	k5eAaImIp3nS	mít
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
ní	on	k3xPp3gFnSc2	on
viditelné	viditelný	k2eAgNnSc1d1	viditelné
silné	silný	k2eAgInPc1d1	silný
drápy	dráp	k1gInPc1	dráp
<g/>
.	.	kIx.	.
</s>
<s>
Pohlaví	pohlaví	k1gNnSc1	pohlaví
se	se	k3xPyFc4	se
od	od	k7c2	od
sebe	se	k3xPyFc2	se
liší	lišit	k5eAaImIp3nP	lišit
pouze	pouze	k6eAd1	pouze
velikostí	velikost	k1gFnPc2	velikost
(	(	kIx(	(
<g/>
samec	samec	k1gMnSc1	samec
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
38	[number]	k4	38
<g/>
-	-	kIx~	-
<g/>
50	[number]	k4	50
%	%	kIx~	%
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
samice	samice	k1gFnSc1	samice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mladí	mladý	k2eAgMnPc1d1	mladý
jedinci	jedinec	k1gMnPc1	jedinec
mají	mít	k5eAaImIp3nP	mít
do	do	k7c2	do
druhého	druhý	k4xOgInSc2	druhý
roku	rok	k1gInSc2	rok
života	život	k1gInSc2	život
bílý	bílý	k2eAgInSc1d1	bílý
pruh	pruh	k1gInSc1	pruh
na	na	k7c6	na
hrdle	hrdla	k1gFnSc6	hrdla
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
počet	počet	k1gInSc1	počet
volně	volně	k6eAd1	volně
žijících	žijící	k2eAgMnPc2d1	žijící
medvědů	medvěd	k1gMnPc2	medvěd
hnědých	hnědý	k2eAgMnPc2d1	hnědý
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
na	na	k7c4	na
200	[number]	k4	200
000	[number]	k4	000
jedinců	jedinec	k1gMnPc2	jedinec
a	a	k8xC	a
dle	dle	k7c2	dle
Červeného	Červeného	k2eAgInSc2d1	Červeného
seznamu	seznam	k1gInSc2	seznam
IUCN	IUCN	kA	IUCN
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
málo	málo	k6eAd1	málo
dotčený	dotčený	k2eAgInSc4d1	dotčený
druh	druh	k1gInSc4	druh
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnPc1d3	veliký
populace	populace	k1gFnPc1	populace
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
(	(	kIx(	(
<g/>
120	[number]	k4	120
000	[number]	k4	000
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
(	(	kIx(	(
<g/>
32	[number]	k4	32
500	[number]	k4	500
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
)	)	kIx)	)
a	a	k8xC	a
Kanadě	Kanada	k1gFnSc6	Kanada
(	(	kIx(	(
<g/>
21	[number]	k4	21
750	[number]	k4	750
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
