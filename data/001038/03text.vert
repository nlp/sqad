<s>
Adamov	Adamov	k1gInSc1	Adamov
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
:	:	kIx,	:
Adamsthal	Adamsthal	k1gMnSc1	Adamsthal
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Blansko	Blansko	k1gNnSc4	Blansko
v	v	k7c6	v
Jihomoravském	jihomoravský	k2eAgInSc6d1	jihomoravský
kraji	kraj	k1gInSc6	kraj
<g/>
,	,	kIx,	,
12	[number]	k4	12
km	km	kA	km
severně	severně	k6eAd1	severně
od	od	k7c2	od
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
na	na	k7c6	na
soutoku	soutok	k1gInSc6	soutok
Svitavy	Svitava	k1gFnSc2	Svitava
a	a	k8xC	a
Křtinského	Křtinský	k2eAgInSc2d1	Křtinský
potoka	potok	k1gInSc2	potok
v	v	k7c6	v
lesnatém	lesnatý	k2eAgNnSc6d1	lesnaté
údolí	údolí	k1gNnSc6	údolí
<g/>
.	.	kIx.	.
</s>
<s>
Nadmořská	nadmořský	k2eAgFnSc1d1	nadmořská
výška	výška	k1gFnSc1	výška
dna	dno	k1gNnSc2	dno
údolí	údolí	k1gNnSc2	údolí
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
těchto	tento	k3xDgNnPc6	tento
místech	místo	k1gNnPc6	místo
kolem	kolem	k7c2	kolem
230	[number]	k4	230
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
městská	městský	k2eAgFnSc1d1	městská
obytná	obytný	k2eAgFnSc1d1	obytná
zástavba	zástavba	k1gFnSc1	zástavba
se	se	k3xPyFc4	se
však	však	k9	však
nachází	nacházet	k5eAaImIp3nS	nacházet
i	i	k9	i
ve	v	k7c6	v
výškách	výška	k1gFnPc6	výška
nad	nad	k7c4	nad
300	[number]	k4	300
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Východně	východně	k6eAd1	východně
od	od	k7c2	od
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
chráněná	chráněný	k2eAgFnSc1d1	chráněná
krajinná	krajinný	k2eAgFnSc1d1	krajinná
oblast	oblast	k1gFnSc1	oblast
Moravský	moravský	k2eAgInSc1d1	moravský
kras	kras	k1gInSc1	kras
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
přes	přes	k7c4	přes
4	[number]	k4	4
500	[number]	k4	500
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Počátek	počátek	k1gInSc1	počátek
osídlování	osídlování	k1gNnSc2	osídlování
Adamovska	Adamovsko	k1gNnSc2	Adamovsko
spadá	spadat	k5eAaPmIp3nS	spadat
do	do	k7c2	do
období	období	k1gNnSc2	období
mladého	mladý	k2eAgInSc2d1	mladý
paleolitu	paleolit	k1gInSc2	paleolit
(	(	kIx(	(
<g/>
17	[number]	k4	17
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
tisíc	tisíc	k4xCgInSc4	tisíc
let	léto	k1gNnPc2	léto
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
území	území	k1gNnSc4	území
přišli	přijít	k5eAaPmAgMnP	přijít
lovci	lovec	k1gMnPc1	lovec
mamutů	mamut	k1gMnPc2	mamut
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
se	se	k3xPyFc4	se
usídlovali	usídlovat	k5eAaImAgMnP	usídlovat
v	v	k7c6	v
jeskyních	jeskyně	k1gFnPc6	jeskyně
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
něž	jenž	k3xRgFnPc4	jenž
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
Býčí	býčí	k2eAgFnSc1d1	býčí
skála	skála	k1gFnSc1	skála
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
cca	cca	kA	cca
5	[number]	k4	5
km	km	kA	km
východně	východně	k6eAd1	východně
od	od	k7c2	od
Adamova	Adamov	k1gInSc2	Adamov
v	v	k7c6	v
Křtinském	Křtinský	k2eAgNnSc6d1	Křtinské
údolí	údolí	k1gNnSc6	údolí
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1869	[number]	k4	1869
tu	tu	k6eAd1	tu
byl	být	k5eAaImAgMnS	být
nalezen	nalezen	k2eAgMnSc1d1	nalezen
bronzový	bronzový	k2eAgMnSc1d1	bronzový
býček	býček	k1gMnSc1	býček
a	a	k8xC	a
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
pak	pak	k6eAd1	pak
při	při	k7c6	při
větších	veliký	k2eAgInPc6d2	veliký
archeologických	archeologický	k2eAgInPc6d1	archeologický
průzkumech	průzkum	k1gInPc6	průzkum
vedených	vedený	k2eAgInPc2d1	vedený
Jindřichem	Jindřich	k1gMnSc7	Jindřich
Wankelem	Wankel	k1gMnSc7	Wankel
soubor	soubor	k1gInSc4	soubor
archeologických	archeologický	k2eAgInPc2d1	archeologický
předmětů	předmět	k1gInPc2	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Nálezy	nález	k1gInPc1	nález
z	z	k7c2	z
Býčí	býčí	k2eAgFnSc2d1	býčí
skály	skála	k1gFnSc2	skála
jsou	být	k5eAaImIp3nP	být
vystaveny	vystavit	k5eAaPmNgFnP	vystavit
v	v	k7c6	v
Přírodovědném	přírodovědný	k2eAgNnSc6d1	Přírodovědné
muzeu	muzeum	k1gNnSc6	muzeum
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Historie	historie	k1gFnSc1	historie
Adamova	Adamov	k1gInSc2	Adamov
byla	být	k5eAaImAgFnS	být
od	od	k7c2	od
prvopočátku	prvopočátek	k1gInSc2	prvopočátek
spojena	spojen	k2eAgFnSc1d1	spojena
se	s	k7c7	s
železářskou	železářský	k2eAgFnSc7d1	železářská
výrobou	výroba	k1gFnSc7	výroba
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
kořeny	kořen	k1gInPc4	kořen
zde	zde	k6eAd1	zde
sahají	sahat	k5eAaImIp3nP	sahat
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1360	[number]	k4	1360
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
tuto	tento	k3xDgFnSc4	tento
oblast	oblast	k1gFnSc4	oblast
vlastnil	vlastnit	k5eAaImAgMnS	vlastnit
majitel	majitel	k1gMnSc1	majitel
hradu	hrad	k1gInSc2	hrad
Ronova	Ronův	k2eAgNnSc2d1	Ronovo
Čeněk	Čeněk	k1gMnSc1	Čeněk
Krušina	krušina	k1gFnSc1	krušina
z	z	k7c2	z
Lichtenburka	Lichtenburek	k1gMnSc2	Lichtenburek
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
však	však	k9	však
zemřel	zemřít	k5eAaPmAgMnS	zemřít
bezdětný	bezdětný	k2eAgMnSc1d1	bezdětný
<g/>
,	,	kIx,	,
a	a	k8xC	a
panství	panství	k1gNnSc1	panství
poté	poté	k6eAd1	poté
připadlo	připadnout	k5eAaPmAgNnS	připadnout
moravskému	moravský	k2eAgMnSc3d1	moravský
markraběti	markrabě	k1gMnSc3	markrabě
Joštovi	Jošt	k1gMnSc3	Jošt
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
jako	jako	k9	jako
centrum	centrum	k1gNnSc1	centrum
panství	panství	k1gNnPc2	panství
zde	zde	k6eAd1	zde
postavil	postavit	k5eAaPmAgInS	postavit
Nový	nový	k2eAgInSc1d1	nový
hrad	hrad	k1gInSc1	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnSc1d1	původní
osada	osada	k1gFnSc1	osada
se	se	k3xPyFc4	se
jmenovala	jmenovat	k5eAaImAgFnS	jmenovat
Hamry	Hamry	k1gInPc4	Hamry
či	či	k8xC	či
Staré	Staré	k2eAgInPc1d1	Staré
Hamry	Hamry	k1gInPc1	Hamry
podle	podle	k7c2	podle
hamrů	hamr	k1gInPc2	hamr
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
zde	zde	k6eAd1	zde
stály	stát	k5eAaImAgInP	stát
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
markraběte	markrabě	k1gMnSc2	markrabě
Jošta	Jošt	k1gMnSc2	Jošt
přešlo	přejít	k5eAaPmAgNnS	přejít
zdejší	zdejší	k2eAgNnSc4d1	zdejší
území	území	k1gNnSc4	území
do	do	k7c2	do
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
českých	český	k2eAgMnPc2d1	český
králů	král	k1gMnPc2	král
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
ho	on	k3xPp3gMnSc4	on
pronajímali	pronajímat	k5eAaImAgMnP	pronajímat
šlechtickým	šlechtický	k2eAgInPc3d1	šlechtický
rodům	rod	k1gInPc3	rod
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
k	k	k7c3	k
nejvýznamnějším	významný	k2eAgMnPc3d3	nejvýznamnější
patřili	patřit	k5eAaImAgMnP	patřit
Boskovicové	Boskovicový	k2eAgFnPc1d1	Boskovicový
a	a	k8xC	a
páni	pan	k1gMnPc1	pan
z	z	k7c2	z
Kunštátu	Kunštát	k1gInSc2	Kunštát
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1597	[number]	k4	1597
zemřel	zemřít	k5eAaPmAgMnS	zemřít
poslední	poslední	k2eAgMnSc1d1	poslední
mužský	mužský	k2eAgMnSc1d1	mužský
potomek	potomek	k1gMnSc1	potomek
Černohorských	Černohorská	k1gFnPc2	Černohorská
z	z	k7c2	z
Boskovic	Boskovice	k1gInPc2	Boskovice
Jan	Jan	k1gMnSc1	Jan
Šembera	Šembera	k1gMnSc1	Šembera
z	z	k7c2	z
Boskovic	Boskovice	k1gInPc2	Boskovice
<g/>
,	,	kIx,	,
panství	panství	k1gNnSc2	panství
vyženil	vyženit	k5eAaPmAgInS	vyženit
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
dcerou	dcera	k1gFnSc7	dcera
Kateřinou	Kateřina	k1gFnSc7	Kateřina
Maxmilián	Maxmiliána	k1gFnPc2	Maxmiliána
z	z	k7c2	z
Lichtenštejna	Lichtenštejn	k1gInSc2	Lichtenštejn
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
roku	rok	k1gInSc2	rok
1643	[number]	k4	1643
zemřel	zemřít	k5eAaPmAgMnS	zemřít
bezdětný	bezdětný	k2eAgMnSc1d1	bezdětný
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
majetek	majetek	k1gInSc4	majetek
zdědil	zdědit	k5eAaPmAgMnS	zdědit
synovec	synovec	k1gMnSc1	synovec
Karel	Karel	k1gMnSc1	Karel
Eusebius	Eusebius	k1gMnSc1	Eusebius
z	z	k7c2	z
Lichtenštejna	Lichtenštejn	k1gInSc2	Lichtenštejn
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
Lichtenštejnů	Lichtenštejn	k1gMnPc2	Lichtenštejn
začalo	začít	k5eAaPmAgNnS	začít
docházet	docházet	k5eAaImF	docházet
k	k	k7c3	k
postupnému	postupný	k2eAgInSc3d1	postupný
rozvoji	rozvoj	k1gInSc3	rozvoj
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Patrně	patrně	k6eAd1	patrně
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1732	[number]	k4	1732
nese	nést	k5eAaImIp3nS	nést
osada	osada	k1gFnSc1	osada
jméno	jméno	k1gNnSc4	jméno
Adamov	Adamov	k1gInSc1	Adamov
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
podle	podle	k7c2	podle
Adama	Adam	k1gMnSc2	Adam
Josefa	Josef	k1gMnSc2	Josef
z	z	k7c2	z
Lichtenštejna	Lichtenštejn	k1gMnSc2	Lichtenštejn
<g/>
,	,	kIx,	,
majitele	majitel	k1gMnSc2	majitel
zdejších	zdejší	k2eAgFnPc2d1	zdejší
železáren	železárna	k1gFnPc2	železárna
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1832	[number]	k4	1832
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
Adamov	Adamov	k1gInSc1	Adamov
stal	stát	k5eAaPmAgInS	stát
samostatnou	samostatný	k2eAgFnSc7d1	samostatná
obcí	obec	k1gFnSc7	obec
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jeho	jeho	k3xOp3gInSc3	jeho
rozvoji	rozvoj	k1gInSc3	rozvoj
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
i	i	k8xC	i
rozkvětu	rozkvět	k1gInSc6	rozkvět
místních	místní	k2eAgInPc2d1	místní
průmyslových	průmyslový	k2eAgInPc2d1	průmyslový
podniků	podnik	k1gInPc2	podnik
došlo	dojít	k5eAaPmAgNnS	dojít
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1849	[number]	k4	1849
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
dobudována	dobudován	k2eAgFnSc1d1	dobudována
železniční	železniční	k2eAgFnSc1d1	železniční
trať	trať	k1gFnSc1	trať
Brno	Brno	k1gNnSc4	Brno
-	-	kIx~	-
Česká	český	k2eAgFnSc1d1	Česká
Třebová	Třebová	k1gFnSc1	Třebová
<g/>
,	,	kIx,	,
dnešní	dnešní	k2eAgFnSc1d1	dnešní
<g/>
[	[	kIx(	[
<g/>
kdy	kdy	k6eAd1	kdy
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
součást	součást	k1gFnSc1	součást
1	[number]	k4	1
<g/>
.	.	kIx.	.
železničního	železniční	k2eAgInSc2d1	železniční
koridoru	koridor	k1gInSc2	koridor
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
15	[number]	k4	15
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1850	[number]	k4	1850
byl	být	k5eAaImAgInS	být
zřízen	zřídit	k5eAaPmNgInS	zřídit
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
poštovní	poštovní	k2eAgInSc4d1	poštovní
úřad	úřad	k1gInSc4	úřad
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
disponoval	disponovat	k5eAaBmAgInS	disponovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1880	[number]	k4	1880
telegrafním	telegrafní	k2eAgInPc3d1	telegrafní
a	a	k8xC	a
1905	[number]	k4	1905
telefonním	telefonní	k2eAgNnSc7d1	telefonní
spojením	spojení	k1gNnSc7	spojení
<g/>
.	.	kIx.	.
</s>
<s>
Alois	Alois	k1gMnSc1	Alois
II	II	kA	II
<g/>
.	.	kIx.	.
z	z	k7c2	z
Lichtenštejna	Lichtenštejn	k1gInSc2	Lichtenštejn
nechal	nechat	k5eAaPmAgMnS	nechat
zbudovat	zbudovat	k5eAaPmF	zbudovat
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
obce	obec	k1gFnSc2	obec
čtyři	čtyři	k4xCgMnPc1	čtyři
pro	pro	k7c4	pro
Adamov	Adamov	k1gInSc4	Adamov
významné	významný	k2eAgFnSc2d1	významná
stavby	stavba	k1gFnSc2	stavba
<g/>
,	,	kIx,	,
a	a	k8xC	a
sice	sice	k8xC	sice
kostel	kostel	k1gInSc1	kostel
svaté	svatý	k2eAgFnSc2d1	svatá
Barbory	Barbora	k1gFnSc2	Barbora
<g/>
,	,	kIx,	,
postavený	postavený	k2eAgInSc1d1	postavený
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1855	[number]	k4	1855
až	až	k9	až
1857	[number]	k4	1857
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgMnSc3	jenž
věnoval	věnovat	k5eAaImAgInS	věnovat
tzv.	tzv.	kA	tzv.
Světelský	Světelský	k2eAgInSc4d1	Světelský
oltář	oltář	k1gInSc4	oltář
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
stavbami	stavba	k1gFnPc7	stavba
jsou	být	k5eAaImIp3nP	být
hřbitov	hřbitov	k1gInSc4	hřbitov
<g/>
,	,	kIx,	,
fara	fara	k1gFnSc1	fara
a	a	k8xC	a
budova	budova	k1gFnSc1	budova
pro	pro	k7c4	pro
potřeby	potřeba	k1gFnPc4	potřeba
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
objekty	objekt	k1gInPc1	objekt
byly	být	k5eAaImAgInP	být
postaveny	postavit	k5eAaPmNgInP	postavit
ve	v	k7c6	v
shodném	shodný	k2eAgInSc6d1	shodný
novogotickém	novogotický	k2eAgInSc6d1	novogotický
slohu	sloh	k1gInSc6	sloh
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1880	[number]	k4	1880
byly	být	k5eAaImAgFnP	být
adamovské	adamovský	k2eAgFnPc1d1	adamovská
železárny	železárna	k1gFnPc1	železárna
pronajaty	pronajat	k2eAgFnPc1d1	pronajata
na	na	k7c4	na
25	[number]	k4	25
let	léto	k1gNnPc2	léto
firmě	firma	k1gFnSc3	firma
Märky	Märka	k1gFnSc2	Märka
<g/>
,	,	kIx,	,
Bromovský	Bromovský	k1gMnSc1	Bromovský
a	a	k8xC	a
Schulz	Schulz	k1gMnSc1	Schulz
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
uplynutí	uplynutí	k1gNnSc6	uplynutí
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1905	[number]	k4	1905
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
o	o	k7c6	o
jejich	jejich	k3xOp3gInSc6	jejich
prodeji	prodej	k1gInSc6	prodej
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
podnik	podnik	k1gInSc1	podnik
<g/>
,	,	kIx,	,
čítající	čítající	k2eAgInSc1d1	čítající
takřka	takřka	k6eAd1	takřka
třicet	třicet	k4xCc4	třicet
výrobních	výrobní	k2eAgInPc2d1	výrobní
a	a	k8xC	a
pomocných	pomocný	k2eAgInPc2d1	pomocný
objektů	objekt	k1gInPc2	objekt
<g/>
,	,	kIx,	,
koupila	koupit	k5eAaPmAgFnS	koupit
za	za	k7c4	za
480	[number]	k4	480
000	[number]	k4	000
rakouských	rakouský	k2eAgFnPc2d1	rakouská
korun	koruna	k1gFnPc2	koruna
tatáž	týž	k3xTgFnSc1	týž
firma	firma	k1gFnSc1	firma
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
jej	on	k3xPp3gInSc4	on
doposavad	doposavad	k6eAd1	doposavad
měla	mít	k5eAaImAgFnS	mít
v	v	k7c6	v
nájmu	nájem	k1gInSc6	nájem
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
mezitím	mezitím	k6eAd1	mezitím
přejmenovala	přejmenovat	k5eAaPmAgFnS	přejmenovat
na	na	k7c6	na
Bromovský	Bromovský	k1gMnSc1	Bromovský
<g/>
,	,	kIx,	,
Schulz	Schulz	k1gMnSc1	Schulz
a	a	k8xC	a
Sohr	Sohr	k1gMnSc1	Sohr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1889	[number]	k4	1889
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
Adamově	Adamov	k1gInSc6	Adamov
vyroben	vyrobit	k5eAaPmNgInS	vyrobit
Marcusův	Marcusův	k2eAgInSc1d1	Marcusův
automobil	automobil	k1gInSc1	automobil
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
prvním	první	k4xOgInSc7	první
automobilem	automobil	k1gInSc7	automobil
s	s	k7c7	s
benzínovým	benzínový	k2eAgInSc7d1	benzínový
čtyřtaktním	čtyřtaktní	k2eAgInSc7d1	čtyřtaktní
motorem	motor	k1gInSc7	motor
na	na	k7c6	na
území	území	k1gNnSc6	území
Rakouska-Uherska	Rakouska-Uhersk	k1gInSc2	Rakouska-Uhersk
(	(	kIx(	(
<g/>
kopřivnický	kopřivnický	k2eAgMnSc1d1	kopřivnický
Präsident	Präsident	k1gMnSc1	Präsident
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
o	o	k7c4	o
osm	osm	k4xCc4	osm
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Adamově	Adamov	k1gInSc6	Adamov
vybudována	vybudovat	k5eAaPmNgFnS	vybudovat
řada	řada	k1gFnSc1	řada
letních	letní	k2eAgNnPc2d1	letní
sídel	sídlo	k1gNnPc2	sídlo
<g/>
,	,	kIx,	,
především	především	k9	především
zámožnějších	zámožný	k2eAgMnPc2d2	zámožnější
obyvatel	obyvatel	k1gMnPc2	obyvatel
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
první	první	k4xOgFnSc1	první
Československá	československý	k2eAgFnSc1d1	Československá
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
převzaly	převzít	k5eAaPmAgInP	převzít
adamovské	adamovský	k2eAgInPc1d1	adamovský
průmyslové	průmyslový	k2eAgInPc1d1	průmyslový
podniky	podnik	k1gInPc1	podnik
Škodovy	Škodův	k2eAgInPc1d1	Škodův
závody	závod	k1gInPc1	závod
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
třicátých	třicátý	k4xOgNnPc6	třicátý
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
zde	zde	k6eAd1	zde
probíhala	probíhat	k5eAaImAgFnS	probíhat
mohutná	mohutný	k2eAgFnSc1d1	mohutná
zbrojní	zbrojní	k2eAgFnSc1d1	zbrojní
výroba	výroba	k1gFnSc1	výroba
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
jednu	jeden	k4xCgFnSc4	jeden
dobu	doba	k1gFnSc4	doba
pracovalo	pracovat	k5eAaImAgNnS	pracovat
na	na	k7c4	na
8000	[number]	k4	8000
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
Adamov	Adamov	k1gInSc1	Adamov
samotný	samotný	k2eAgInSc1d1	samotný
měl	mít	k5eAaImAgInS	mít
něco	něco	k6eAd1	něco
kolem	kolem	k7c2	kolem
jednoho	jeden	k4xCgInSc2	jeden
tisíce	tisíc	k4xCgInSc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
německé	německý	k2eAgNnSc1d1	německé
vojsko	vojsko	k1gNnSc1	vojsko
ustupovalo	ustupovat	k5eAaImAgNnS	ustupovat
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
o	o	k7c4	o
zničení	zničení	k1gNnSc4	zničení
adamovského	adamovský	k2eAgInSc2d1	adamovský
závodu	závod	k1gInSc2	závod
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
rozbíjení	rozbíjení	k1gNnSc3	rozbíjení
všeho	všecek	k3xTgNnSc2	všecek
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
nešlo	jít	k5eNaImAgNnS	jít
evakuovat	evakuovat	k5eAaBmF	evakuovat
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
oběť	oběť	k1gFnSc4	oběť
tomu	ten	k3xDgMnSc3	ten
tak	tak	k9	tak
padla	padnout	k5eAaPmAgFnS	padnout
většina	většina	k1gFnSc1	většina
strojů	stroj	k1gInPc2	stroj
<g/>
,	,	kIx,	,
budov	budova	k1gFnPc2	budova
a	a	k8xC	a
několik	několik	k4yIc4	několik
objektů	objekt	k1gInPc2	objekt
vyletělo	vyletět	k5eAaPmAgNnS	vyletět
do	do	k7c2	do
povětří	povětří	k1gNnSc2	povětří
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
byla	být	k5eAaImAgFnS	být
vypálena	vypálen	k2eAgFnSc1d1	vypálena
i	i	k8xC	i
administrativní	administrativní	k2eAgFnSc1d1	administrativní
budova	budova	k1gFnSc1	budova
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
škoda	škoda	k1gFnSc1	škoda
překročila	překročit	k5eAaPmAgFnS	překročit
částku	částka	k1gFnSc4	částka
164	[number]	k4	164
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
znárodnění	znárodnění	k1gNnSc3	znárodnění
zdejších	zdejší	k2eAgInPc2d1	zdejší
Škodových	Škodových	k2eAgInPc2d1	Škodových
závodů	závod	k1gInPc2	závod
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
byly	být	k5eAaImAgFnP	být
přejmenovány	přejmenovat	k5eAaPmNgFnP	přejmenovat
na	na	k7c4	na
Adamovské	Adamovské	k2eAgFnPc4d1	Adamovské
strojírny	strojírna	k1gFnPc4	strojírna
(	(	kIx(	(
<g/>
Adast	Adast	k1gMnSc1	Adast
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poválečném	poválečný	k2eAgNnSc6d1	poválečné
období	období	k1gNnSc6	období
se	se	k3xPyFc4	se
s	s	k7c7	s
růstem	růst	k1gInSc7	růst
strojíren	strojírna	k1gFnPc2	strojírna
postupně	postupně	k6eAd1	postupně
zvyšoval	zvyšovat	k5eAaImAgInS	zvyšovat
i	i	k9	i
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
Adamova	Adamov	k1gInSc2	Adamov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
se	se	k3xPyFc4	se
započalo	započnout	k5eAaPmAgNnS	započnout
s	s	k7c7	s
budováním	budování	k1gNnSc7	budování
nové	nový	k2eAgFnSc2d1	nová
adamovské	adamovský	k2eAgFnSc2d1	adamovská
čtvrti	čtvrt	k1gFnSc2	čtvrt
tzv.	tzv.	kA	tzv.
Na	na	k7c6	na
kolonii	kolonie	k1gFnSc6	kolonie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
nacházela	nacházet	k5eAaImAgFnS	nacházet
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
obce	obec	k1gFnSc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
touto	tento	k3xDgFnSc7	tento
čtvrtí	čtvrt	k1gFnSc7	čtvrt
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1951	[number]	k4	1951
zahájilo	zahájit	k5eAaPmAgNnS	zahájit
s	s	k7c7	s
výstavbou	výstavba	k1gFnSc7	výstavba
čtvrti	čtvrt	k1gFnSc2	čtvrt
další	další	k2eAgFnSc2d1	další
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Ptačiny	ptačina	k1gFnSc2	ptačina
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
skládala	skládat	k5eAaImAgFnS	skládat
ze	z	k7c2	z
třiceti	třicet	k4xCc2	třicet
dřevěných	dřevěný	k2eAgInPc2d1	dřevěný
domků	domek	k1gInPc2	domek
typu	typ	k1gInSc2	typ
Hartl	Hartl	k1gMnSc1	Hartl
a	a	k8xC	a
čtyřiadvaceti	čtyřiadvacet	k4xCc2	čtyřiadvacet
cihlových	cihlový	k2eAgFnPc2d1	cihlová
bytovek	bytovka	k1gFnPc2	bytovka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
letech	let	k1gInPc6	let
byla	být	k5eAaImAgFnS	být
také	také	k9	také
v	v	k7c6	v
Adamově	Adamov	k1gInSc6	Adamov
zavedena	zavést	k5eAaPmNgFnS	zavést
výroba	výroba	k1gFnSc1	výroba
polygrafických	polygrafický	k2eAgInPc2d1	polygrafický
strojů	stroj	k1gInPc2	stroj
a	a	k8xC	a
čerpací	čerpací	k2eAgFnSc2d1	čerpací
a	a	k8xC	a
měrné	měrný	k2eAgFnSc2d1	měrná
techniky	technika	k1gFnSc2	technika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
byl	být	k5eAaImAgInS	být
Adamov	Adamov	k1gInSc1	Adamov
začleněn	začleněn	k2eAgInSc1d1	začleněn
do	do	k7c2	do
okresu	okres	k1gInSc2	okres
Blansko	Blansko	k1gNnSc1	Blansko
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
byl	být	k5eAaImAgMnS	být
pak	pak	k6eAd1	pak
povýšen	povýšit	k5eAaPmNgMnS	povýšit
na	na	k7c4	na
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
letech	let	k1gInPc6	let
se	se	k3xPyFc4	se
zvyšoval	zvyšovat	k5eAaImAgInS	zvyšovat
export	export	k1gInSc1	export
polygrafických	polygrafický	k2eAgInPc2d1	polygrafický
strojů	stroj	k1gInPc2	stroj
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	on	k3xPp3gInSc4	on
další	další	k2eAgInSc4d1	další
rozvoj	rozvoj	k1gInSc4	rozvoj
brzdila	brzdit	k5eAaImAgFnS	brzdit
intenzivní	intenzivní	k2eAgFnSc1d1	intenzivní
tajná	tajný	k2eAgFnSc1d1	tajná
vojenská	vojenský	k2eAgFnSc1d1	vojenská
výroba	výroba	k1gFnSc1	výroba
<g/>
,	,	kIx,	,
určená	určený	k2eAgFnSc1d1	určená
především	především	k9	především
pro	pro	k7c4	pro
vývoz	vývoz	k1gInSc4	vývoz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
a	a	k8xC	a
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
začala	začít	k5eAaPmAgFnS	začít
výstavba	výstavba	k1gFnSc1	výstavba
dalších	další	k2eAgInPc2d1	další
obytných	obytný	k2eAgInPc2d1	obytný
panelových	panelový	k2eAgInPc2d1	panelový
bloků	blok	k1gInPc2	blok
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
proto	proto	k8xC	proto
ekonomika	ekonomika	k1gFnSc1	ekonomika
města	město	k1gNnSc2	město
výrazně	výrazně	k6eAd1	výrazně
postižena	postihnout	k5eAaPmNgFnS	postihnout
konverzí	konverze	k1gFnSc7	konverze
zbrojního	zbrojní	k2eAgInSc2d1	zbrojní
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Adamovské	Adamovské	k2eAgFnPc1d1	Adamovské
strojírny	strojírna	k1gFnPc1	strojírna
vyhlásily	vyhlásit	k5eAaPmAgFnP	vyhlásit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
bankrot	bankrot	k1gInSc1	bankrot
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
roce	rok	k1gInSc6	rok
byly	být	k5eAaImAgFnP	být
celé	celý	k2eAgFnPc1d1	celá
strojírny	strojírna	k1gFnPc1	strojírna
odkoupeny	odkoupen	k2eAgFnPc1d1	odkoupena
za	za	k7c4	za
85	[number]	k4	85
milionů	milion	k4xCgInPc2	milion
slovenskou	slovenský	k2eAgFnSc7d1	slovenská
firmou	firma	k1gFnSc7	firma
Penta	Pent	k1gInSc2	Pent
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
byly	být	k5eAaImAgInP	být
opět	opět	k6eAd1	opět
prodány	prodán	k2eAgInPc1d1	prodán
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
pražské	pražský	k2eAgFnSc2d1	Pražská
společnosti	společnost	k1gFnSc2	společnost
J	J	kA	J
23	[number]	k4	23
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgMnS	být
na	na	k7c4	na
strojírny	strojírna	k1gFnPc4	strojírna
podán	podán	k2eAgInSc4d1	podán
insolventní	insolventní	k2eAgInSc4d1	insolventní
návrh	návrh	k1gInSc4	návrh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
podnik	podnik	k1gInSc1	podnik
do	do	k7c2	do
likvidace	likvidace	k1gFnSc2	likvidace
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
měl	mít	k5eAaImAgInS	mít
Adamov	Adamov	k1gInSc1	Adamov
společnou	společný	k2eAgFnSc4d1	společná
pečeť	pečeť	k1gFnSc4	pečeť
s	s	k7c7	s
osadou	osada	k1gFnSc7	osada
Jezerky	Jezerka	k1gFnSc2	Jezerka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jejím	její	k3xOp3gNnSc6	její
pečetním	pečetní	k2eAgNnSc6d1	pečetní
poli	pole	k1gNnSc6	pole
se	se	k3xPyFc4	se
nacházel	nacházet	k5eAaImAgMnS	nacházet
Josef	Josef	k1gMnSc1	Josef
Jan	Jan	k1gMnSc1	Jan
Adam	Adam	k1gMnSc1	Adam
z	z	k7c2	z
Lichtenštejna	Lichtenštejn	k1gInSc2	Lichtenštejn
s	s	k7c7	s
pozdviženou	pozdvižený	k2eAgFnSc7d1	pozdvižená
pravicí	pravice	k1gFnSc7	pravice
<g/>
.	.	kIx.	.
</s>
<s>
Nápis	nápis	k1gInSc1	nápis
na	na	k7c6	na
pečeti	pečeť	k1gFnSc6	pečeť
byl	být	k5eAaImAgMnS	být
Peczet	Peczet	k1gMnSc1	Peczet
obczy	obcza	k1gFnSc2	obcza
Jezerka	Jezerka	k1gFnSc1	Jezerka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1749	[number]	k4	1749
Adamov	Adamov	k1gInSc1	Adamov
použil	použít	k5eAaPmAgInS	použít
svou	svůj	k3xOyFgFnSc4	svůj
vlastní	vlastní	k2eAgFnSc4d1	vlastní
pečeť	pečeť	k1gFnSc4	pečeť
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
pečeť	pečeť	k1gFnSc1	pečeť
měla	mít	k5eAaImAgFnS	mít
tvar	tvar	k1gInSc4	tvar
oválu	ovál	k1gInSc2	ovál
o	o	k7c6	o
rozměrech	rozměr	k1gInPc6	rozměr
28	[number]	k4	28
mm	mm	kA	mm
na	na	k7c4	na
výšku	výška	k1gFnSc4	výška
a	a	k8xC	a
25	[number]	k4	25
mm	mm	kA	mm
na	na	k7c4	na
šířku	šířka	k1gFnSc4	šířka
<g/>
.	.	kIx.	.
</s>
<s>
Pečeť	pečeť	k1gFnSc1	pečeť
se	se	k3xPyFc4	se
skládala	skládat	k5eAaImAgFnS	skládat
z	z	k7c2	z
oválného	oválný	k2eAgInSc2d1	oválný
barokního	barokní	k2eAgInSc2d1	barokní
štítu	štít	k1gInSc2	štít
zdobeného	zdobený	k2eAgInSc2d1	zdobený
na	na	k7c6	na
bocích	bok	k1gInPc6	bok
palmovými	palmový	k2eAgInPc7d1	palmový
listy	list	k1gInPc7	list
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc4	jejichž
stopky	stopka	k1gFnPc1	stopka
se	se	k3xPyFc4	se
stýkají	stýkat	k5eAaImIp3nP	stýkat
pod	pod	k7c7	pod
štítem	štít	k1gInSc7	štít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
štítu	štít	k1gInSc2	štít
bylo	být	k5eAaImAgNnS	být
umístěno	umístit	k5eAaPmNgNnS	umístit
břevno	břevno	k1gNnSc1	břevno
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
jej	on	k3xPp3gMnSc4	on
dělilo	dělit	k5eAaImAgNnS	dělit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
poloviny	polovina	k1gFnPc4	polovina
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
břevnem	břevno	k1gNnSc7	břevno
bylo	být	k5eAaImAgNnS	být
umístěno	umístěn	k2eAgNnSc1d1	umístěno
kladivo	kladivo	k1gNnSc1	kladivo
zkřížené	zkřížený	k2eAgNnSc1d1	zkřížené
s	s	k7c7	s
kosou	kosa	k1gFnSc7	kosa
<g/>
,	,	kIx,	,
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
zkřížení	zkřížení	k1gNnSc2	zkřížení
byly	být	k5eAaImAgInP	být
kolmo	kolmo	k6eAd1	kolmo
umístěné	umístěný	k2eAgFnPc1d1	umístěná
hrábě	hrábě	k1gFnPc1	hrábě
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dolní	dolní	k2eAgFnSc6d1	dolní
půlce	půlka	k1gFnSc6	půlka
štítu	štít	k1gInSc2	štít
se	se	k3xPyFc4	se
nacházela	nacházet	k5eAaImAgFnS	nacházet
kovadlina	kovadlina	k1gFnSc1	kovadlina
<g/>
,	,	kIx,	,
na	na	k7c4	na
niž	jenž	k3xRgFnSc4	jenž
byly	být	k5eAaImAgFnP	být
umístěny	umístěn	k2eAgFnPc1d1	umístěna
kleště	kleště	k1gFnPc1	kleště
a	a	k8xC	a
hamerské	hamerský	k2eAgNnSc1d1	Hamerské
kladivo	kladivo	k1gNnSc1	kladivo
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
štítem	štít	k1gInSc7	štít
se	se	k3xPyFc4	se
nacházel	nacházet	k5eAaImAgMnS	nacházet
znak	znak	k1gInSc4	znak
převzatý	převzatý	k2eAgInSc4d1	převzatý
od	od	k7c2	od
Josefa	Josef	k1gMnSc2	Josef
Adama	Adam	k1gMnSc2	Adam
-	-	kIx~	-
koruna	koruna	k1gFnSc1	koruna
s	s	k7c7	s
pěti	pět	k4xCc7	pět
lístky	lístek	k1gInPc7	lístek
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
pečeť	pečeť	k1gFnSc1	pečeť
se	se	k3xPyFc4	se
používala	používat	k5eAaImAgFnS	používat
až	až	k9	až
do	do	k7c2	do
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
byla	být	k5eAaImAgFnS	být
vypsána	vypsat	k5eAaPmNgFnS	vypsat
městem	město	k1gNnSc7	město
Adamov	Adamov	k1gInSc4	Adamov
soutěž	soutěž	k1gFnSc4	soutěž
na	na	k7c4	na
návrh	návrh	k1gInSc4	návrh
městského	městský	k2eAgInSc2d1	městský
znaku	znak	k1gInSc2	znak
<g/>
.	.	kIx.	.
</s>
<s>
Vítězný	vítězný	k2eAgInSc1d1	vítězný
návrh	návrh	k1gInSc1	návrh
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
J.	J.	kA	J.
Kosteleckého	Kosteleckého	k2eAgFnSc1d1	Kosteleckého
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
17	[number]	k4	17
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1972	[number]	k4	1972
<g/>
.	.	kIx.	.
</s>
<s>
Znak	znak	k1gInSc1	znak
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
stříbrného	stříbrný	k2eAgInSc2d1	stříbrný
gotického	gotický	k2eAgInSc2d1	gotický
štítu	štít	k1gInSc2	štít
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
je	být	k5eAaImIp3nS	být
vyobrazena	vyobrazen	k2eAgFnSc1d1	vyobrazena
železářská	železářský	k2eAgFnSc1d1	železářská
pec	pec	k1gFnSc1	pec
naznačující	naznačující	k2eAgFnSc4d1	naznačující
železářskou	železářský	k2eAgFnSc4d1	železářská
tradici	tradice	k1gFnSc4	tradice
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
kterou	který	k3yRgFnSc7	který
se	se	k3xPyFc4	se
rozbíhají	rozbíhat	k5eAaImIp3nP	rozbíhat
do	do	k7c2	do
stran	strana	k1gFnPc2	strana
plameny	plamen	k1gInPc1	plamen
symbolizují	symbolizovat	k5eAaImIp3nP	symbolizovat
dělnou	dělný	k2eAgFnSc4d1	dělná
tradici	tradice	k1gFnSc4	tradice
minulosti	minulost	k1gFnSc2	minulost
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
pecí	pec	k1gFnSc7	pec
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
nachází	nacházet	k5eAaImIp3nS	nacházet
kolo	kolo	k1gNnSc1	kolo
s	s	k7c7	s
polovinou	polovina	k1gFnSc7	polovina
ozubení	ozubení	k1gNnSc2	ozubení
stříbrné	stříbrný	k2eAgFnSc2d1	stříbrná
barvy	barva	k1gFnSc2	barva
vyznačující	vyznačující	k2eAgInSc4d1	vyznačující
novodobý	novodobý	k2eAgInSc4d1	novodobý
strojírenský	strojírenský	k2eAgInSc4d1	strojírenský
rozvoj	rozvoj	k1gInSc4	rozvoj
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
pecí	pec	k1gFnSc7	pec
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
hnědá	hnědý	k2eAgFnSc1d1	hnědá
ostrev	ostrev	k?	ostrev
se	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
suky	suk	k1gInPc7	suk
<g/>
,	,	kIx,	,
představující	představující	k2eAgFnPc4d1	představující
přírodní	přírodní	k2eAgNnPc4d1	přírodní
bohatství	bohatství	k1gNnPc4	bohatství
okolních	okolní	k2eAgInPc2d1	okolní
lesů	les	k1gInPc2	les
<g/>
.	.	kIx.	.
</s>
<s>
Drahanská	Drahanský	k2eAgFnSc1d1	Drahanská
vrchovina	vrchovina	k1gFnSc1	vrchovina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
geologický	geologický	k2eAgInSc4d1	geologický
profil	profil	k1gInSc4	profil
širokého	široký	k2eAgNnSc2d1	široké
okolí	okolí	k1gNnSc2	okolí
Adamova	Adamov	k1gInSc2	Adamov
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
geomorfologickým	geomorfologický	k2eAgInSc7d1	geomorfologický
celkem	celek	k1gInSc7	celek
spadajícím	spadající	k2eAgInSc7d1	spadající
pod	pod	k7c4	pod
Brněnskou	brněnský	k2eAgFnSc4d1	brněnská
vrchovinu	vrchovina	k1gFnSc4	vrchovina
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
tři	tři	k4xCgFnPc4	tři
části	část	k1gFnPc4	část
-	-	kIx~	-
Moravský	moravský	k2eAgInSc4d1	moravský
kras	kras	k1gInSc4	kras
<g/>
,	,	kIx,	,
Konickou	konický	k2eAgFnSc4d1	konická
vrchovinu	vrchovina	k1gFnSc4	vrchovina
a	a	k8xC	a
Adamovskou	Adamovská	k1gFnSc4	Adamovská
vrchovinu	vrchovina	k1gFnSc4	vrchovina
<g/>
.	.	kIx.	.
</s>
<s>
Východní	východní	k2eAgInSc4d1	východní
okraj	okraj	k1gInSc4	okraj
Adamovské	Adamovské	k2eAgFnSc2d1	Adamovské
vrchoviny	vrchovina	k1gFnSc2	vrchovina
prořezává	prořezávat	k5eAaImIp3nS	prořezávat
průlomové	průlomový	k2eAgNnSc1d1	průlomové
údolí	údolí	k1gNnSc1	údolí
řeky	řeka	k1gFnSc2	řeka
Svitavy	Svitava	k1gFnSc2	Svitava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
údolí	údolí	k1gNnSc6	údolí
se	se	k3xPyFc4	se
Adamov	Adamov	k1gInSc1	Adamov
nachází	nacházet	k5eAaImIp3nS	nacházet
<g/>
.	.	kIx.	.
</s>
<s>
Centrum	centrum	k1gNnSc1	centrum
města	město	k1gNnSc2	město
pak	pak	k6eAd1	pak
u	u	k7c2	u
soutoku	soutok	k1gInSc2	soutok
řeky	řeka	k1gFnSc2	řeka
Svitavy	Svitava	k1gFnSc2	Svitava
a	a	k8xC	a
Křtinského	Křtinský	k2eAgInSc2d1	Křtinský
potoka	potok	k1gInSc2	potok
<g/>
.	.	kIx.	.
</s>
<s>
Nadmořská	nadmořský	k2eAgFnSc1d1	nadmořská
výška	výška	k1gFnSc1	výška
dna	dno	k1gNnSc2	dno
údolí	údolí	k1gNnSc2	údolí
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k7c2	okolo
230	[number]	k4	230
metrů	metr	k1gInPc2	metr
nad	nad	k7c7	nad
mořem	moře	k1gNnSc7	moře
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
lesnatým	lesnatý	k2eAgMnSc7d1	lesnatý
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
hornatým	hornatý	k2eAgInSc7d1	hornatý
charakterem	charakter	k1gInSc7	charakter
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
kterému	který	k3yIgInSc3	který
se	se	k3xPyFc4	se
obydlená	obydlený	k2eAgFnSc1d1	obydlená
zástavba	zástavba	k1gFnSc1	zástavba
zvedá	zvedat	k5eAaImIp3nS	zvedat
až	až	k9	až
do	do	k7c2	do
výše	výše	k1gFnSc2	výše
nad	nad	k7c4	nad
300	[number]	k4	300
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
blízká	blízký	k2eAgFnSc1d1	blízká
Alexandrova	Alexandrův	k2eAgFnSc1d1	Alexandrova
rozhledna	rozhledna	k1gFnSc1	rozhledna
(	(	kIx(	(
<g/>
televizní	televizní	k2eAgMnSc1d1	televizní
převaděč	převaděč	k1gMnSc1	převaděč
pro	pro	k7c4	pro
Adamov	Adamov	k1gInSc4	Adamov
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
tyčí	tyčit	k5eAaImIp3nP	tyčit
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
527	[number]	k4	527
metrů	metr	k1gInPc2	metr
nad	nad	k7c7	nad
mořem	moře	k1gNnSc7	moře
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
doložitelný	doložitelný	k2eAgInSc4d1	doložitelný
počátek	počátek	k1gInSc4	počátek
rozvoje	rozvoj	k1gInSc2	rozvoj
kultury	kultura	k1gFnSc2	kultura
v	v	k7c6	v
Adamově	Adamov	k1gInSc6	Adamov
lze	lze	k6eAd1	lze
označit	označit	k5eAaPmF	označit
rok	rok	k1gInSc4	rok
1863	[number]	k4	1863
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
Čtenářský	čtenářský	k2eAgInSc1d1	čtenářský
spolek	spolek	k1gInSc1	spolek
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
spolek	spolek	k1gInSc1	spolek
pěstoval	pěstovat	k5eAaImAgInS	pěstovat
mezi	mezi	k7c7	mezi
svými	svůj	k3xOyFgMnPc7	svůj
členy	člen	k1gMnPc7	člen
především	především	k6eAd1	především
čtení	čtení	k1gNnSc4	čtení
české	český	k2eAgFnSc2d1	Česká
literatury	literatura	k1gFnSc2	literatura
a	a	k8xC	a
novin	novina	k1gFnPc2	novina
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
šířil	šířit	k5eAaImAgMnS	šířit
české	český	k2eAgNnSc4d1	české
uvědomění	uvědomění	k1gNnSc4	uvědomění
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgInPc4d1	další
spolky	spolek	k1gInPc4	spolek
patřil	patřit	k5eAaImAgMnS	patřit
například	například	k6eAd1	například
Sokol	Sokol	k1gMnSc1	Sokol
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1890	[number]	k4	1890
<g/>
,	,	kIx,	,
či	či	k8xC	či
Dělnická	dělnický	k2eAgFnSc1d1	Dělnická
tělocvičná	tělocvičný	k2eAgFnSc1d1	Tělocvičná
jednota	jednota	k1gFnSc1	jednota
založená	založený	k2eAgFnSc1d1	založená
roku	rok	k1gInSc2	rok
1905	[number]	k4	1905
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
spolky	spolek	k1gInPc1	spolek
započaly	započnout	k5eAaPmAgInP	započnout
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
kromě	kromě	k7c2	kromě
sportů	sport	k1gInPc2	sport
také	také	k6eAd1	také
amatérské	amatérský	k2eAgNnSc4d1	amatérské
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1931	[number]	k4	1931
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Adamově	Adamov	k1gInSc6	Adamov
založeno	založen	k2eAgNnSc1d1	založeno
Pěvecké	pěvecký	k2eAgNnSc1d1	pěvecké
sdružení	sdružení	k1gNnSc1	sdružení
Hlahol	hlahol	k1gInSc1	hlahol
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
navázalo	navázat	k5eAaPmAgNnS	navázat
na	na	k7c4	na
volné	volný	k2eAgNnSc4d1	volné
sdružení	sdružení	k1gNnPc4	sdružení
Pěvecký	pěvecký	k2eAgInSc4d1	pěvecký
kroužek	kroužek	k1gInSc4	kroužek
Moravan	Moravan	k1gMnSc1	Moravan
<g/>
.	.	kIx.	.
</s>
<s>
Masivní	masivní	k2eAgInSc1d1	masivní
rozvoj	rozvoj	k1gInSc1	rozvoj
kultury	kultura	k1gFnSc2	kultura
nastal	nastat	k5eAaPmAgInS	nastat
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Adamovských	Adamovských	k2eAgFnPc6d1	Adamovských
strojírnách	strojírna	k1gFnPc6	strojírna
založen	založen	k2eAgInSc1d1	založen
Závodní	závodní	k2eAgInSc1d1	závodní
klub	klub	k1gInSc1	klub
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
jeho	jeho	k3xOp3gFnSc7	jeho
patronací	patronace	k1gFnSc7	patronace
začaly	začít	k5eAaPmAgInP	začít
vznikat	vznikat	k5eAaImF	vznikat
různé	různý	k2eAgInPc1d1	různý
kroužky	kroužek	k1gInPc1	kroužek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
podílely	podílet	k5eAaImAgInP	podílet
na	na	k7c6	na
rozvoji	rozvoj	k1gInSc6	rozvoj
kulturního	kulturní	k2eAgInSc2d1	kulturní
života	život	k1gInSc2	život
v	v	k7c6	v
Adamově	Adamov	k1gInSc6	Adamov
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejaktivnější	aktivní	k2eAgFnPc4d3	nejaktivnější
patřil	patřit	k5eAaImAgInS	patřit
historicko-vlastivědný	historickolastivědný	k2eAgInSc4d1	historicko-vlastivědný
kroužek	kroužek	k1gInSc4	kroužek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vydával	vydávat	k5eAaPmAgInS	vydávat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1957	[number]	k4	1957
<g/>
-	-	kIx~	-
<g/>
1972	[number]	k4	1972
vlastní	vlastní	k2eAgInSc1d1	vlastní
časopis	časopis	k1gInSc1	časopis
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
filmový	filmový	k2eAgInSc1d1	filmový
<g/>
,	,	kIx,	,
estrádní	estrádní	k2eAgInSc1d1	estrádní
apod.	apod.	kA	apod.
K	k	k7c3	k
utlumení	utlumení	k1gNnSc3	utlumení
kulturní	kulturní	k2eAgFnSc2d1	kulturní
činnosti	činnost	k1gFnSc2	činnost
došlo	dojít	k5eAaPmAgNnS	dojít
po	po	k7c6	po
zániku	zánik	k1gInSc6	zánik
Závodního	závodní	k2eAgInSc2d1	závodní
klubu	klub	k1gInSc2	klub
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
<g/>
[	[	kIx(	[
<g/>
kdy	kdy	k6eAd1	kdy
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
v	v	k7c6	v
Adamově	Adamov	k1gInSc6	Adamov
působí	působit	k5eAaImIp3nS	působit
příspěvková	příspěvkový	k2eAgFnSc1d1	příspěvková
organizace	organizace	k1gFnSc1	organizace
Městské	městský	k2eAgNnSc1d1	Městské
kulturní	kulturní	k2eAgNnSc1d1	kulturní
středisko	středisko	k1gNnSc1	středisko
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
kterým	který	k3yQgInSc7	který
se	se	k3xPyFc4	se
koná	konat	k5eAaImIp3nS	konat
převážná	převážný	k2eAgFnSc1d1	převážná
část	část	k1gFnSc1	část
kulturních	kulturní	k2eAgFnPc2d1	kulturní
akcí	akce	k1gFnPc2	akce
v	v	k7c6	v
Adamově	Adamov	k1gInSc6	Adamov
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
jí	on	k3xPp3gFnSc3	on
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
několik	několik	k4yIc4	několik
dalších	další	k2eAgFnPc6d1	další
organizacích	organizace	k1gFnPc6	organizace
jako	jako	k8xC	jako
například	například	k6eAd1	například
Městský	městský	k2eAgInSc1d1	městský
klub	klub	k1gInSc1	klub
mládeže	mládež	k1gFnSc2	mládež
či	či	k8xC	či
různé	různý	k2eAgInPc1d1	různý
městské	městský	k2eAgInPc1d1	městský
výbory	výbor	k1gInPc1	výbor
<g/>
,	,	kIx,	,
spolky	spolek	k1gInPc1	spolek
a	a	k8xC	a
kroužky	kroužek	k1gInPc1	kroužek
<g/>
.	.	kIx.	.
</s>
<s>
Historie	historie	k1gFnSc1	historie
organizované	organizovaný	k2eAgFnSc2d1	organizovaná
tělovýchovy	tělovýchova	k1gFnSc2	tělovýchova
a	a	k8xC	a
sportu	sport	k1gInSc2	sport
se	se	k3xPyFc4	se
v	v	k7c6	v
Adamově	Adamov	k1gInSc6	Adamov
začala	začít	k5eAaPmAgFnS	začít
psát	psát	k5eAaImF	psát
po	po	k7c4	po
založení	založení	k1gNnSc4	založení
Sokola	Sokol	k1gMnSc2	Sokol
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1890	[number]	k4	1890
a	a	k8xC	a
Dělnické	dělnický	k2eAgFnSc2d1	Dělnická
tělocvičné	tělocvičný	k2eAgFnSc2d1	Tělocvičná
jednoty	jednota	k1gFnSc2	jednota
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1905	[number]	k4	1905
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
dva	dva	k4xCgInPc1	dva
spolky	spolek	k1gInPc1	spolek
především	především	k9	především
pěstovaly	pěstovat	k5eAaImAgInP	pěstovat
cvičení	cvičení	k1gNnSc4	cvičení
<g/>
,	,	kIx,	,
výlety	výlet	k1gInPc4	výlet
do	do	k7c2	do
okolí	okolí	k1gNnSc2	okolí
a	a	k8xC	a
různé	různý	k2eAgInPc4d1	různý
sporty	sport	k1gInPc4	sport
jako	jako	k8xS	jako
běh	běh	k1gInSc4	běh
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
<g/>
,	,	kIx,	,
volejbal	volejbal	k1gInSc1	volejbal
apod.	apod.	kA	apod.
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
klub	klub	k1gInSc1	klub
SK	Sk	kA	Sk
Adamov	Adamov	k1gInSc1	Adamov
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
klub	klub	k1gInSc1	klub
existuje	existovat	k5eAaImIp3nS	existovat
se	s	k7c7	s
střídavými	střídavý	k2eAgInPc7d1	střídavý
výsledky	výsledek	k1gInPc7	výsledek
až	až	k9	až
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
adamovském	adamovský	k2eAgNnSc6d1	adamovský
fotbalovém	fotbalový	k2eAgNnSc6d1	fotbalové
hřišti	hřiště	k1gNnSc6	hřiště
s	s	k7c7	s
fotbalem	fotbal	k1gInSc7	fotbal
začínal	začínat	k5eAaImAgInS	začínat
také	také	k9	také
na	na	k7c6	na
konci	konec	k1gInSc6	konec
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
český	český	k2eAgMnSc1d1	český
fotbalový	fotbalový	k2eAgMnSc1d1	fotbalový
reprezentant	reprezentant	k1gMnSc1	reprezentant
Jindřich	Jindřich	k1gMnSc1	Jindřich
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
střelec	střelec	k1gMnSc1	střelec
zlatého	zlatý	k2eAgInSc2d1	zlatý
gólu	gól	k1gInSc2	gól
na	na	k7c6	na
Olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
</s>
<s>
Místní	místní	k2eAgInSc1d1	místní
stadion	stadion	k1gInSc1	stadion
po	po	k7c6	po
něm	on	k3xPp3gNnSc6	on
nese	nést	k5eAaImIp3nS	nést
název	název	k1gInSc1	název
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
uděleno	udělit	k5eAaPmNgNnS	udělit
čestné	čestný	k2eAgNnSc1d1	čestné
občanství	občanství	k1gNnSc1	občanství
Adamova	Adamov	k1gInSc2	Adamov
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
sport	sport	k1gInSc1	sport
sdružoval	sdružovat	k5eAaImAgInS	sdružovat
pod	pod	k7c7	pod
hlavičkou	hlavička	k1gFnSc7	hlavička
Tělovýchovné	tělovýchovný	k2eAgFnSc2d1	Tělovýchovná
jednoty	jednota	k1gFnSc2	jednota
(	(	kIx(	(
<g/>
TJ	tj	kA	tj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
působily	působit	k5eAaImAgInP	působit
kluby	klub	k1gInPc1	klub
ledního	lední	k2eAgInSc2d1	lední
hokeje	hokej	k1gInSc2	hokej
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
snad	snad	k9	snad
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
40	[number]	k4	40
<g/>
.	.	kIx.	.
a	a	k8xC	a
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
házené	házená	k1gFnPc4	házená
působící	působící	k2eAgFnPc4d1	působící
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1957	[number]	k4	1957
<g/>
,	,	kIx,	,
lyžařský	lyžařský	k2eAgInSc4d1	lyžařský
od	od	k7c2	od
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
a	a	k8xC	a
vytrvalostního	vytrvalostní	k2eAgInSc2d1	vytrvalostní
běhu	běh	k1gInSc2	běh
vzniklého	vzniklý	k2eAgInSc2d1	vzniklý
v	v	k7c4	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Zmíněné	zmíněný	k2eAgInPc1d1	zmíněný
kluby	klub	k1gInPc1	klub
postupem	postupem	k7c2	postupem
času	čas	k1gInSc2	čas
zanikly	zaniknout	k5eAaPmAgFnP	zaniknout
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
dodnes	dodnes	k6eAd1	dodnes
působí	působit	k5eAaImIp3nP	působit
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
kluby	klub	k1gInPc4	klub
fotbalový	fotbalový	k2eAgMnSc1d1	fotbalový
(	(	kIx(	(
<g/>
TJ	tj	kA	tj
Spartak	Spartak	k1gInSc1	Spartak
Adamov	Adamov	k1gInSc1	Adamov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
orientačního	orientační	k2eAgInSc2d1	orientační
běhu	běh	k1gInSc2	běh
(	(	kIx(	(
<g/>
založen	založit	k5eAaPmNgInS	založit
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stolního	stolní	k2eAgInSc2d1	stolní
tenisu	tenis	k1gInSc2	tenis
(	(	kIx(	(
<g/>
počátky	počátek	k1gInPc1	počátek
v	v	k7c4	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
šachů	šach	k1gInPc2	šach
(	(	kIx(	(
<g/>
založen	založit	k5eAaPmNgInS	založit
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
a	a	k8xC	a
tenisový	tenisový	k2eAgMnSc1d1	tenisový
(	(	kIx(	(
<g/>
založen	založit	k5eAaPmNgInS	založit
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nově	nově	k6eAd1	nově
vzniklé	vzniklý	k2eAgInPc4d1	vzniklý
kluby	klub	k1gInPc4	klub
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
florbalový	florbalový	k2eAgInSc1d1	florbalový
<g/>
,	,	kIx,	,
cyklisticko-všestranný	cyklistickošestranný	k2eAgInSc1d1	cyklisticko-všestranný
<g/>
,	,	kIx,	,
oddíl	oddíl	k1gInSc1	oddíl
juda	judo	k1gNnSc2	judo
<g/>
,	,	kIx,	,
šipkový	šipkový	k2eAgInSc1d1	šipkový
klub	klub	k1gInSc1	klub
a	a	k8xC	a
oddíl	oddíl	k1gInSc1	oddíl
petanque	petanquat	k5eAaPmIp3nS	petanquat
<g/>
.	.	kIx.	.
</s>
<s>
Počátky	počátek	k1gInPc4	počátek
školství	školství	k1gNnSc2	školství
v	v	k7c6	v
Adamově	Adamov	k1gInSc6	Adamov
spadají	spadat	k5eAaPmIp3nP	spadat
do	do	k7c2	do
2	[number]	k4	2
<g/>
.	.	kIx.	.
poloviny	polovina	k1gFnSc2	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1780	[number]	k4	1780
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
vyučovat	vyučovat	k5eAaImF	vyučovat
čtení	čtení	k1gNnSc1	čtení
<g/>
,	,	kIx,	,
psaní	psaní	k1gNnSc1	psaní
a	a	k8xC	a
počítání	počítání	k1gNnSc1	počítání
v	v	k7c6	v
domě	dům	k1gInSc6	dům
číslo	číslo	k1gNnSc1	číslo
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
známým	známý	k2eAgMnSc7d1	známý
učitelem	učitel	k1gMnSc7	učitel
byl	být	k5eAaImAgMnS	být
Jan	Jan	k1gMnSc1	Jan
Procházka	Procházka	k1gMnSc1	Procházka
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1814	[number]	k4	1814
<g/>
-	-	kIx~	-
<g/>
1850	[number]	k4	1850
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1857	[number]	k4	1857
byla	být	k5eAaImAgFnS	být
otevřena	otevřít	k5eAaPmNgFnS	otevřít
nová	nový	k2eAgFnSc1d1	nová
budova	budova	k1gFnSc1	budova
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
nechal	nechat	k5eAaPmAgMnS	nechat
společně	společně	k6eAd1	společně
s	s	k7c7	s
kostelem	kostel	k1gInSc7	kostel
svaté	svatý	k2eAgFnSc2d1	svatá
Barbory	Barbora	k1gFnSc2	Barbora
<g/>
,	,	kIx,	,
farou	fara	k1gFnSc7	fara
a	a	k8xC	a
hřbitovem	hřbitov	k1gInSc7	hřbitov
postavit	postavit	k5eAaPmF	postavit
majitel	majitel	k1gMnSc1	majitel
místních	místní	k2eAgFnPc2d1	místní
železáren	železárna	k1gFnPc2	železárna
Alois	Alois	k1gMnSc1	Alois
II	II	kA	II
<g/>
.	.	kIx.	.
z	z	k7c2	z
Liechtenštejna	Liechtenštejn	k1gInSc2	Liechtenštejn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
zaznamenalo	zaznamenat	k5eAaPmAgNnS	zaznamenat
místní	místní	k2eAgNnSc4d1	místní
školství	školství	k1gNnSc4	školství
vlnu	vlna	k1gFnSc4	vlna
germanizačních	germanizační	k2eAgFnPc2d1	germanizační
snah	snaha	k1gFnPc2	snaha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
novém	nový	k2eAgInSc6d1	nový
školním	školní	k2eAgInSc6d1	školní
roce	rok	k1gInSc6	rok
1872	[number]	k4	1872
<g/>
/	/	kIx~	/
<g/>
1873	[number]	k4	1873
bylo	být	k5eAaImAgNnS	být
započato	započat	k2eAgNnSc1d1	započato
s	s	k7c7	s
výukou	výuka	k1gFnSc7	výuka
německého	německý	k2eAgInSc2d1	německý
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
tu	tu	k6eAd1	tu
vydržela	vydržet	k5eAaPmAgFnS	vydržet
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1882	[number]	k4	1882
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgNnP	být
zrušena	zrušit	k5eAaPmNgNnP	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
se	se	k3xPyFc4	se
vyučovalo	vyučovat	k5eAaImAgNnS	vyučovat
taktéž	taktéž	k?	taktéž
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
bývalé	bývalý	k2eAgFnSc2d1	bývalá
vily	vila	k1gFnSc2	vila
brněnského	brněnský	k2eAgMnSc2d1	brněnský
advokáta	advokát	k1gMnSc2	advokát
německého	německý	k2eAgInSc2d1	německý
původu	původ	k1gInSc2	původ
Friedricha	Friedrich	k1gMnSc4	Friedrich
Kloba	Klob	k1gMnSc4	Klob
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
3	[number]	k4	3
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1926	[number]	k4	1926
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
slavnostnímu	slavnostní	k2eAgNnSc3d1	slavnostní
otevření	otevření	k1gNnSc3	otevření
nově	nově	k6eAd1	nově
postavené	postavený	k2eAgFnPc4d1	postavená
budovy	budova	k1gFnPc4	budova
měšťanské	měšťanský	k2eAgFnSc2d1	měšťanská
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stala	stát	k5eAaPmAgFnS	stát
ozdobou	ozdoba	k1gFnSc7	ozdoba
obce	obec	k1gFnPc1	obec
<g/>
.	.	kIx.	.
</s>
<s>
Konec	konec	k1gInSc4	konec
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
přinesl	přinést	k5eAaPmAgMnS	přinést
přerušení	přerušení	k1gNnPc4	přerušení
školní	školní	k2eAgFnSc2d1	školní
výuky	výuka	k1gFnSc2	výuka
a	a	k8xC	a
značné	značný	k2eAgNnSc1d1	značné
poškození	poškození	k1gNnSc1	poškození
školy	škola	k1gFnSc2	škola
a	a	k8xC	a
jejího	její	k3xOp3gNnSc2	její
vybavení	vybavení	k1gNnSc2	vybavení
od	od	k7c2	od
německých	německý	k2eAgMnPc2d1	německý
vojáků	voják	k1gMnPc2	voják
a	a	k8xC	a
sovětského	sovětský	k2eAgNnSc2d1	sovětské
bombardování	bombardování	k1gNnSc2	bombardování
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
17	[number]	k4	17
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1956	[number]	k4	1956
se	se	k3xPyFc4	se
započalo	započnout	k5eAaPmAgNnS	započnout
vyučovat	vyučovat	k5eAaImF	vyučovat
v	v	k7c6	v
nové	nový	k2eAgFnSc6d1	nová
škole	škola	k1gFnSc6	škola
ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
Klementa	Klement	k1gMnSc2	Klement
Gottwalda	Gottwald	k1gMnSc2	Gottwald
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
dne	den	k1gInSc2	den
2	[number]	k4	2
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
otevřena	otevřít	k5eAaPmNgFnS	otevřít
další	další	k2eAgFnSc1d1	další
budova	budova	k1gFnSc1	budova
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
na	na	k7c6	na
Horce	horka	k1gFnSc6	horka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
budova	budova	k1gFnSc1	budova
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
o	o	k7c4	o
přístavbu	přístavba	k1gFnSc4	přístavba
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
vyučování	vyučování	k1gNnSc1	vyučování
omezilo	omezit	k5eAaPmAgNnS	omezit
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
tyto	tento	k3xDgFnPc4	tento
dvě	dva	k4xCgFnPc4	dva
budovy	budova	k1gFnPc4	budova
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgFnPc6	jenž
se	se	k3xPyFc4	se
vyučuje	vyučovat	k5eAaImIp3nS	vyučovat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
základních	základní	k2eAgFnPc2d1	základní
škol	škola	k1gFnPc2	škola
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
Adamově	Adamov	k1gInSc6	Adamov
zřízeny	zřídit	k5eAaPmNgFnP	zřídit
učňovské	učňovský	k2eAgFnPc1d1	učňovská
obory	obora	k1gFnPc1	obora
pod	pod	k7c7	pod
záštitou	záštita	k1gFnSc7	záštita
Adamovských	Adamovských	k2eAgFnPc2d1	Adamovských
strojíren	strojírna	k1gFnPc2	strojírna
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
SOU	sou	k1gInSc7	sou
strojírenské	strojírenský	k2eAgInPc4d1	strojírenský
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
zanikly	zaniknout	k5eAaPmAgInP	zaniknout
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
Soukromá	soukromý	k2eAgFnSc1d1	soukromá
střední	střední	k2eAgFnSc1d1	střední
škola	škola	k1gFnSc1	škola
obchodu	obchod	k1gInSc2	obchod
<g/>
,	,	kIx,	,
služeb	služba	k1gFnPc2	služba
a	a	k8xC	a
provozu	provoz	k1gInSc2	provoz
hotelů	hotel	k1gInPc2	hotel
<g/>
,	,	kIx,	,
taktéž	taktéž	k?	taktéž
zaniklá	zaniklý	k2eAgFnSc1d1	zaniklá
<g/>
,	,	kIx,	,
a	a	k8xC	a
Základní	základní	k2eAgFnSc1d1	základní
umělecká	umělecký	k2eAgFnSc1d1	umělecká
škola	škola	k1gFnSc1	škola
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
a	a	k8xC	a
mateřská	mateřský	k2eAgFnSc1d1	mateřská
škola	škola	k1gFnSc1	škola
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
<g/>
[	[	kIx(	[
<g/>
kdy	kdy	k6eAd1	kdy
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
působí	působit	k5eAaImIp3nS	působit
jako	jako	k9	jako
jedna	jeden	k4xCgFnSc1	jeden
příspěvková	příspěvkový	k2eAgFnSc1d1	příspěvková
organizace	organizace	k1gFnSc1	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
byl	být	k5eAaImAgInS	být
Adamov	Adamov	k1gInSc1	Adamov
ustanoven	ustanovit	k5eAaPmNgInS	ustanovit
jako	jako	k8xC	jako
samostatná	samostatný	k2eAgFnSc1d1	samostatná
obec	obec	k1gFnSc1	obec
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1732	[number]	k4	1732
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
ještě	ještě	k6eAd1	ještě
jako	jako	k8xC	jako
Hamry	Hamry	k1gInPc1	Hamry
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
první	první	k4xOgNnSc4	první
sčítání	sčítání	k1gNnSc4	sčítání
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
pramenech	pramen	k1gInPc6	pramen
objevuje	objevovat	k5eAaImIp3nS	objevovat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1754	[number]	k4	1754
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nacházelo	nacházet	k5eAaImAgNnS	nacházet
21	[number]	k4	21
obytných	obytný	k2eAgInPc2d1	obytný
domů	dům	k1gInPc2	dům
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
hamrů	hamr	k1gInPc2	hamr
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
zde	zde	k6eAd1	zde
bydlelo	bydlet	k5eAaImAgNnS	bydlet
150	[number]	k4	150
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
postupným	postupný	k2eAgInSc7d1	postupný
rozvojem	rozvoj	k1gInSc7	rozvoj
zdejších	zdejší	k2eAgFnPc2d1	zdejší
železáren	železárna	k1gFnPc2	železárna
resp.	resp.	kA	resp.
strojíren	strojírna	k1gFnPc2	strojírna
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
zaměstnávaly	zaměstnávat	k5eAaImAgFnP	zaměstnávat
většinu	většina	k1gFnSc4	většina
místních	místní	k2eAgInPc2d1	místní
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zvyšoval	zvyšovat	k5eAaImAgInS	zvyšovat
i	i	k9	i
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
například	například	k6eAd1	například
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1771	[number]	k4	1771
uvádí	uvádět	k5eAaImIp3nS	uvádět
221	[number]	k4	221
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1857	[number]	k4	1857
634	[number]	k4	634
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1880	[number]	k4	1880
953	[number]	k4	953
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1900	[number]	k4	1900
792	[number]	k4	792
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
překročil	překročit	k5eAaPmAgInS	překročit
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
hranici	hranice	k1gFnSc6	hranice
jednoho	jeden	k4xCgInSc2	jeden
tisíce	tisíc	k4xCgInSc2	tisíc
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
je	být	k5eAaImIp3nS	být
uváděno	uvádět	k5eAaImNgNnS	uvádět
již	již	k6eAd1	již
2	[number]	k4	2
512	[number]	k4	512
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poválečné	poválečný	k2eAgFnSc6d1	poválečná
době	doba	k1gFnSc6	doba
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
výstavbě	výstavba	k1gFnSc3	výstavba
nových	nový	k2eAgNnPc2d1	nové
sídlišť	sídliště	k1gNnPc2	sídliště
pro	pro	k7c4	pro
stovky	stovka	k1gFnPc4	stovka
nových	nový	k2eAgMnPc2d1	nový
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
strojíren	strojírna	k1gFnPc2	strojírna
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
přehoupl	přehoupnout	k5eAaPmAgMnS	přehoupnout
třítisícovou	třítisícový	k2eAgFnSc4d1	třítisícová
hranici	hranice	k1gFnSc4	hranice
<g/>
,	,	kIx,	,
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
pak	pak	k6eAd1	pak
hranici	hranice	k1gFnSc4	hranice
čtyř	čtyři	k4xCgInPc2	čtyři
i	i	k8xC	i
pětitisícovou	pětitisícový	k2eAgFnSc7d1	pětitisícová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
dosažena	dosáhnout	k5eAaPmNgFnS	dosáhnout
horní	horní	k2eAgFnSc1d1	horní
hranice	hranice	k1gFnSc1	hranice
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
-	-	kIx~	-
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
to	ten	k3xDgNnSc1	ten
tvořilo	tvořit	k5eAaImAgNnS	tvořit
5	[number]	k4	5
146	[number]	k4	146
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgNnPc6	tento
letech	léto	k1gNnPc6	léto
zaznamenávají	zaznamenávat	k5eAaImIp3nP	zaznamenávat
Adamovské	Adamovské	k2eAgFnPc1d1	Adamovské
strojírny	strojírna	k1gFnPc1	strojírna
finanční	finanční	k2eAgInPc4d1	finanční
problémy	problém	k1gInPc4	problém
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
mají	mít	k5eAaImIp3nP	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
propouštění	propouštění	k1gNnSc2	propouštění
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
nepravidelných	pravidelný	k2eNgFnPc6d1	nepravidelná
vlnách	vlna	k1gFnPc6	vlna
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
firma	firma	k1gFnSc1	firma
(	(	kIx(	(
<g/>
mezitím	mezitím	k6eAd1	mezitím
přejmenovaná	přejmenovaný	k2eAgFnSc1d1	přejmenovaná
na	na	k7c4	na
Adast	Adast	k1gInSc4	Adast
<g/>
)	)	kIx)	)
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
do	do	k7c2	do
likvidace	likvidace	k1gFnSc2	likvidace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
Adamov	Adamov	k1gInSc1	Adamov
zaznamenává	zaznamenávat	k5eAaImIp3nS	zaznamenávat
pozvolné	pozvolný	k2eAgNnSc4d1	pozvolné
snižování	snižování	k1gNnSc4	snižování
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
byl	být	k5eAaImAgInS	být
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
v	v	k7c6	v
Adamově	Adamov	k1gInSc6	Adamov
celkem	celkem	k6eAd1	celkem
4	[number]	k4	4
591	[number]	k4	591
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
roce	rok	k1gInSc6	rok
byl	být	k5eAaImAgInS	být
úbytek	úbytek	k1gInSc1	úbytek
i	i	k8xC	i
přírůstek	přírůstek	k1gInSc1	přírůstek
obyvatel	obyvatel	k1gMnPc2	obyvatel
zhruba	zhruba	k6eAd1	zhruba
na	na	k7c6	na
stejné	stejný	k2eAgFnSc6d1	stejná
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1849	[number]	k4	1849
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
otevření	otevření	k1gNnSc3	otevření
železniční	železniční	k2eAgFnSc2d1	železniční
tratě	trať	k1gFnSc2	trať
spojující	spojující	k2eAgNnSc1d1	spojující
Brno	Brno	k1gNnSc1	Brno
s	s	k7c7	s
Českou	český	k2eAgFnSc7d1	Česká
Třebovou	Třebová	k1gFnSc7	Třebová
s	s	k7c7	s
nádražím	nádraží	k1gNnSc7	nádraží
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
Adamova	Adamov	k1gInSc2	Adamov
a	a	k8xC	a
1869	[number]	k4	1869
pak	pak	k6eAd1	pak
k	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
zdvoukolejnění	zdvoukolejnění	k1gNnSc3	zdvoukolejnění
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
byly	být	k5eAaImAgFnP	být
dokončeny	dokončit	k5eAaPmNgFnP	dokončit
práce	práce	k1gFnPc1	práce
na	na	k7c4	na
její	její	k3xOp3gFnSc4	její
elektrifikaci	elektrifikace	k1gFnSc4	elektrifikace
a	a	k8xC	a
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
<g/>
[	[	kIx(	[
<g/>
kdy	kdy	k6eAd1	kdy
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
je	být	k5eAaImIp3nS	být
trať	trať	k1gFnSc1	trať
součástí	součást	k1gFnSc7	součást
Prvního	první	k4xOgInSc2	první
železničního	železniční	k2eAgInSc2d1	železniční
koridoru	koridor	k1gInSc2	koridor
<g/>
.	.	kIx.	.
</s>
<s>
Železnice	železnice	k1gFnSc1	železnice
hrála	hrát	k5eAaImAgFnS	hrát
v	v	k7c6	v
rozvoji	rozvoj	k1gInSc6	rozvoj
místních	místní	k2eAgFnPc2d1	místní
železáren	železárna	k1gFnPc2	železárna
velkou	velký	k2eAgFnSc4d1	velká
roli	role	k1gFnSc4	role
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
jejímu	její	k3xOp3gNnSc3	její
dobudování	dobudování	k1gNnSc3	dobudování
nastal	nastat	k5eAaPmAgInS	nastat
rozkvět	rozkvět	k1gInSc1	rozkvět
místních	místní	k2eAgMnPc2d1	místní
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Kvalitního	kvalitní	k2eAgNnSc2d1	kvalitní
spojení	spojení	k1gNnSc2	spojení
s	s	k7c7	s
okolím	okolí	k1gNnSc7	okolí
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
asfaltové	asfaltový	k2eAgFnSc2d1	asfaltová
silnice	silnice	k1gFnSc2	silnice
se	se	k3xPyFc4	se
Adamov	Adamov	k1gInSc1	Adamov
dočkal	dočkat	k5eAaPmAgInS	dočkat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1937	[number]	k4	1937
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
vybudovaná	vybudovaný	k2eAgFnSc1d1	vybudovaná
po	po	k7c6	po
několikaleté	několikaletý	k2eAgFnSc6d1	několikaletá
urgenci	urgence	k1gFnSc6	urgence
adamovských	adamovský	k2eAgMnPc2d1	adamovský
zastupitelů	zastupitel	k1gMnPc2	zastupitel
na	na	k7c6	na
různých	různý	k2eAgNnPc6d1	různé
odpovědných	odpovědný	k2eAgNnPc6d1	odpovědné
místech	místo	k1gNnPc6	místo
silnice	silnice	k1gFnSc2	silnice
z	z	k7c2	z
Brna	Brno	k1gNnSc2	Brno
kolem	kolem	k7c2	kolem
řeky	řeka	k1gFnSc2	řeka
Svitavy	Svitava	k1gFnSc2	Svitava
přes	přes	k7c4	přes
Bílovice	Bílovice	k1gInPc4	Bílovice
nad	nad	k7c7	nad
Svitavou	Svitava	k1gFnSc7	Svitava
do	do	k7c2	do
Adamova	Adamov	k1gInSc2	Adamov
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Adamově	Adamov	k1gInSc6	Adamov
zřízena	zřídit	k5eAaPmNgFnS	zřídit
městská	městský	k2eAgFnSc1d1	městská
autobusová	autobusový	k2eAgFnSc1d1	autobusová
doprava	doprava	k1gFnSc1	doprava
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
<g/>
[	[	kIx(	[
<g/>
kdy	kdy	k6eAd1	kdy
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
obsluhována	obsluhovat	k5eAaImNgFnS	obsluhovat
třemi	tři	k4xCgInPc7	tři
autobusy	autobus	k1gInPc7	autobus
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gMnSc7	její
provozovatelem	provozovatel	k1gMnSc7	provozovatel
je	být	k5eAaImIp3nS	být
ČAD	Čad	k1gInSc1	Čad
Blansko	Blansko	k1gNnSc1	Blansko
<g/>
.	.	kIx.	.
</s>
<s>
MHD	MHD	kA	MHD
je	být	k5eAaImIp3nS	být
zahrnuta	zahrnut	k2eAgFnSc1d1	zahrnuta
v	v	k7c6	v
Integrovaném	integrovaný	k2eAgInSc6d1	integrovaný
dopravním	dopravní	k2eAgInSc6d1	dopravní
systému	systém	k1gInSc6	systém
Jihomoravského	jihomoravský	k2eAgInSc2d1	jihomoravský
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volebním	volební	k2eAgNnSc6d1	volební
období	období	k1gNnSc6	období
2010	[number]	k4	2010
až	až	k9	až
2014	[number]	k4	2014
stál	stát	k5eAaImAgInS	stát
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
města	město	k1gNnSc2	město
starosta	starosta	k1gMnSc1	starosta
Roman	Roman	k1gMnSc1	Roman
Pilát	Pilát	k1gMnSc1	Pilát
<g/>
,	,	kIx,	,
místostarostou	místostarosta	k1gMnSc7	místostarosta
pak	pak	k6eAd1	pak
byl	být	k5eAaImAgMnS	být
Jiří	Jiří	k1gMnSc1	Jiří
Němec	Němec	k1gMnSc1	Němec
(	(	kIx(	(
<g/>
ODS	ODS	kA	ODS
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stejné	stejný	k2eAgNnSc1d1	stejné
vedení	vedení	k1gNnSc1	vedení
Adamova	Adamov	k1gInSc2	Adamov
je	být	k5eAaImIp3nS	být
také	také	k9	také
od	od	k7c2	od
ustavujícího	ustavující	k2eAgNnSc2d1	ustavující
zasedání	zasedání	k1gNnSc2	zasedání
zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
4	[number]	k4	4
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejstarší	starý	k2eAgFnPc4d3	nejstarší
stavby	stavba	k1gFnPc4	stavba
ve	v	k7c6	v
městě	město	k1gNnSc6	město
patří	patřit	k5eAaImIp3nS	patřit
zámek	zámek	k1gInSc1	zámek
<g/>
,	,	kIx,	,
dokončený	dokončený	k2eAgInSc1d1	dokončený
roku	rok	k1gInSc2	rok
1809	[number]	k4	1809
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
si	se	k3xPyFc3	se
zde	zde	k6eAd1	zde
nechal	nechat	k5eAaPmAgMnS	nechat
zbudovat	zbudovat	k5eAaPmF	zbudovat
lichtenštejnský	lichtenštejnský	k2eAgMnSc1d1	lichtenštejnský
kníže	kníže	k1gMnSc1	kníže
Jan	Jan	k1gMnSc1	Jan
I.	I.	kA	I.
z	z	k7c2	z
Lichtenštejna	Lichtenštejn	k1gInSc2	Lichtenštejn
knížecím	knížecí	k2eAgMnSc7d1	knížecí
stavitelem	stavitel	k1gMnSc7	stavitel
Josefem	Josef	k1gMnSc7	Josef
Hardtmuthem	Hardtmuth	k1gInSc7	Hardtmuth
<g/>
.	.	kIx.	.
</s>
<s>
Zámek	zámek	k1gInSc1	zámek
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
<g/>
[	[	kIx(	[
<g/>
kdy	kdy	k6eAd1	kdy
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
obklopují	obklopovat	k5eAaImIp3nP	obklopovat
výrobní	výrobní	k2eAgFnPc4d1	výrobní
haly	hala	k1gFnPc4	hala
bývalé	bývalý	k2eAgFnSc2d1	bývalá
adamovské	adamovský	k2eAgFnSc2d1	adamovská
strojírny	strojírna	k1gFnSc2	strojírna
a	a	k8xC	a
samotná	samotný	k2eAgFnSc1d1	samotná
stavba	stavba	k1gFnSc1	stavba
je	být	k5eAaImIp3nS	být
využívána	využívat	k5eAaPmNgFnS	využívat
jako	jako	k8xC	jako
průmyslový	průmyslový	k2eAgInSc4d1	průmyslový
objekt	objekt	k1gInSc4	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Alois	Alois	k1gMnSc1	Alois
II	II	kA	II
<g/>
.	.	kIx.	.
z	z	k7c2	z
Lichtenštejna	Lichtenštejno	k1gNnSc2	Lichtenštejno
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
zmíněného	zmíněný	k2eAgInSc2d1	zmíněný
Johanna	Johann	k1gInSc2	Johann
<g/>
,	,	kIx,	,
v	v	k7c6	v
Adamově	Adamov	k1gInSc6	Adamov
roku	rok	k1gInSc2	rok
1857	[number]	k4	1857
nechal	nechat	k5eAaPmAgMnS	nechat
zbudovat	zbudovat	k5eAaPmF	zbudovat
kostel	kostel	k1gInSc4	kostel
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
zasvěcen	zasvěcen	k2eAgInSc1d1	zasvěcen
svaté	svatý	k2eAgFnSc3d1	svatá
Barboře	Barbora	k1gFnSc3	Barbora
<g/>
,	,	kIx,	,
patronce	patronka	k1gFnSc3	patronka
horníků	horník	k1gMnPc2	horník
<g/>
.	.	kIx.	.
</s>
<s>
Kostelu	kostel	k1gInSc3	kostel
taktéž	taktéž	k?	taktéž
věnoval	věnovat	k5eAaPmAgInS	věnovat
zakoupený	zakoupený	k2eAgInSc1d1	zakoupený
oltář	oltář	k1gInSc1	oltář
<g/>
,	,	kIx,	,
pocházející	pocházející	k2eAgInSc1d1	pocházející
ze	z	k7c2	z
Zwettlu	Zwettl	k1gInSc2	Zwettl
(	(	kIx(	(
<g/>
Dolní	dolní	k2eAgNnSc1d1	dolní
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
unikátní	unikátní	k2eAgInSc4d1	unikátní
pozdně	pozdně	k6eAd1	pozdně
gotický	gotický	k2eAgInSc4d1	gotický
soubor	soubor	k1gInSc4	soubor
figurálních	figurální	k2eAgFnPc2d1	figurální
plastik	plastika	k1gFnPc2	plastika
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
kostela	kostel	k1gInSc2	kostel
byla	být	k5eAaImAgFnS	být
také	také	k9	také
zbudována	zbudován	k2eAgFnSc1d1	zbudována
fara	fara	k1gFnSc1	fara
<g/>
,	,	kIx,	,
hřbitov	hřbitov	k1gInSc4	hřbitov
(	(	kIx(	(
<g/>
tyto	tento	k3xDgFnPc1	tento
společně	společně	k6eAd1	společně
s	s	k7c7	s
kostelem	kostel	k1gInSc7	kostel
vyhlášeny	vyhlásit	k5eAaPmNgFnP	vyhlásit
jako	jako	k9	jako
kulturní	kulturní	k2eAgFnPc1d1	kulturní
památky	památka	k1gFnPc1	památka
<g/>
)	)	kIx)	)
a	a	k8xC	a
budova	budova	k1gFnSc1	budova
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
století	století	k1gNnSc4	století
taktéž	taktéž	k?	taktéž
pochází	pocházet	k5eAaImIp3nS	pocházet
tzv.	tzv.	kA	tzv.
Skalní	skalní	k2eAgInSc1d1	skalní
sklep	sklep	k1gInSc1	sklep
<g/>
,	,	kIx,	,
lidově	lidově	k6eAd1	lidově
Skalák	Skalák	k1gMnSc1	Skalák
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Felsenkeller	Felsenkeller	k1gInSc1	Felsenkeller
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Budova	budova	k1gFnSc1	budova
vyrostla	vyrůst	k5eAaPmAgFnS	vyrůst
na	na	k7c6	na
původním	původní	k2eAgNnSc6d1	původní
místě	místo	k1gNnSc6	místo
bývalé	bývalý	k2eAgFnSc2d1	bývalá
hospody	hospody	k?	hospody
stejného	stejný	k2eAgInSc2d1	stejný
názvu	název	k1gInSc2	název
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
první	první	k4xOgFnSc2	první
republiky	republika	k1gFnSc2	republika
fungoval	fungovat	k5eAaImAgMnS	fungovat
jako	jako	k9	jako
restaurace	restaurace	k1gFnPc4	restaurace
a	a	k8xC	a
hotel	hotel	k1gInSc4	hotel
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
hojně	hojně	k6eAd1	hojně
využívali	využívat	k5eAaPmAgMnP	využívat
úředníci	úředník	k1gMnPc1	úředník
ze	z	k7c2	z
strojíren	strojírna	k1gFnPc2	strojírna
k	k	k7c3	k
ubytování	ubytování	k1gNnSc3	ubytování
<g/>
.	.	kIx.	.
</s>
<s>
Nájemci	nájemce	k1gMnPc1	nájemce
byli	být	k5eAaImAgMnP	být
bratři	bratr	k1gMnPc1	bratr
Ducháčkové	Ducháčková	k1gFnSc2	Ducháčková
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
<g/>
[	[	kIx(	[
<g/>
kdy	kdy	k6eAd1	kdy
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
probíhá	probíhat	k5eAaImIp3nS	probíhat
jeho	jeho	k3xOp3gNnSc1	jeho
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
také	také	k9	také
nachází	nacházet	k5eAaImIp3nS	nacházet
tři	tři	k4xCgFnPc1	tři
busty	busta	k1gFnPc1	busta
významných	významný	k2eAgFnPc2d1	významná
osobností	osobnost	k1gFnPc2	osobnost
<g/>
.	.	kIx.	.
</s>
<s>
Tou	ten	k3xDgFnSc7	ten
nejstarší	starý	k2eAgFnSc7d3	nejstarší
je	být	k5eAaImIp3nS	být
busta	busta	k1gFnSc1	busta
Bedřicha	Bedřich	k1gMnSc2	Bedřich
Smetany	smetana	k1gFnSc2	smetana
od	od	k7c2	od
akademického	akademický	k2eAgMnSc2d1	akademický
sochaře	sochař	k1gMnSc2	sochař
Jana	Jan	k1gMnSc2	Jan
Třísky	Tříska	k1gMnSc2	Tříska
na	na	k7c6	na
Smetanově	Smetanův	k2eAgNnSc6d1	Smetanovo
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
byla	být	k5eAaImAgFnS	být
slavnostně	slavnostně	k6eAd1	slavnostně
odhalena	odhalen	k2eAgFnSc1d1	odhalena
23	[number]	k4	23
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1937	[number]	k4	1937
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
odhalena	odhalit	k5eAaPmNgFnS	odhalit
bez	bez	k7c2	bez
slavnostního	slavnostní	k2eAgInSc2d1	slavnostní
aktu	akt	k1gInSc2	akt
busta	busta	k1gFnSc1	busta
Tomáše	Tomáš	k1gMnSc4	Tomáš
Garrigua	Garriguus	k1gMnSc4	Garriguus
Masaryka	Masaryk	k1gMnSc4	Masaryk
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
umístěna	umístit	k5eAaPmNgFnS	umístit
před	před	k7c7	před
základní	základní	k2eAgFnSc7d1	základní
školou	škola	k1gFnSc7	škola
na	na	k7c6	na
ulici	ulice	k1gFnSc6	ulice
Komenského	Komenský	k1gMnSc2	Komenský
<g/>
.	.	kIx.	.
</s>
<s>
Busta	busta	k1gFnSc1	busta
Petera	Peter	k1gMnSc2	Peter
Jilemnického	jilemnický	k2eAgMnSc2d1	jilemnický
před	před	k7c7	před
základní	základní	k2eAgFnSc7d1	základní
školou	škola	k1gFnSc7	škola
na	na	k7c6	na
ulici	ulice	k1gFnSc6	ulice
Ronovská	Ronovská	k1gFnSc1	Ronovská
byla	být	k5eAaImAgFnS	být
slavnostně	slavnostně	k6eAd1	slavnostně
odhalena	odhalen	k2eAgFnSc1d1	odhalena
10	[number]	k4	10
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1963	[number]	k4	1963
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zátiší	zátiší	k1gNnSc6	zátiší
nedaleko	nedaleko	k7c2	nedaleko
kostela	kostel	k1gInSc2	kostel
svaté	svatý	k2eAgFnSc2d1	svatá
Barbory	Barbora	k1gFnSc2	Barbora
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
socha	socha	k1gFnSc1	socha
Rudoarmějce	rudoarmějec	k1gMnSc2	rudoarmějec
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
Adamově	Adamov	k1gInSc6	Adamov
slavnostně	slavnostně	k6eAd1	slavnostně
odhalen	odhalit	k5eAaPmNgInS	odhalit
10	[number]	k4	10
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1951	[number]	k4	1951
<g/>
.	.	kIx.	.
</s>
<s>
Zátiší	zátiší	k1gNnSc1	zátiší
dotváří	dotvářet	k5eAaImIp3nS	dotvářet
památník	památník	k1gInSc1	památník
padlých	padlý	k2eAgMnPc2d1	padlý
adamovských	adamovský	k2eAgMnPc2d1	adamovský
občanů	občan	k1gMnPc2	občan
ve	v	k7c6	v
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
a	a	k8xC	a
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
strojírny	strojírna	k1gFnSc2	strojírna
popravených	popravený	k2eAgMnPc2d1	popravený
v	v	k7c6	v
období	období	k1gNnSc6	období
nacistické	nacistický	k2eAgFnSc2d1	nacistická
okupace	okupace	k1gFnSc2	okupace
<g/>
.	.	kIx.	.
</s>
<s>
Adamov	Adamov	k1gInSc1	Adamov
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
hranici	hranice	k1gFnSc6	hranice
CHKO	CHKO	kA	CHKO
Moravský	moravský	k2eAgInSc4d1	moravský
kras	kras	k1gInSc4	kras
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
tedy	tedy	k9	tedy
může	moct	k5eAaImIp3nS	moct
působit	působit	k5eAaImF	působit
jako	jako	k9	jako
výchozí	výchozí	k2eAgInSc4d1	výchozí
bod	bod	k1gInSc4	bod
střední	střední	k2eAgFnSc2d1	střední
části	část	k1gFnSc2	část
této	tento	k3xDgFnSc2	tento
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
krasovému	krasový	k2eAgInSc3d1	krasový
charakteru	charakter	k1gInSc3	charakter
Moravského	moravský	k2eAgInSc2d1	moravský
krasu	kras	k1gInSc2	kras
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
okolí	okolí	k1gNnSc1	okolí
města	město	k1gNnSc2	město
pochlubit	pochlubit	k5eAaPmF	pochlubit
s	s	k7c7	s
celou	celý	k2eAgFnSc7d1	celá
řadou	řada	k1gFnSc7	řada
jeskyní	jeskyně	k1gFnPc2	jeskyně
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgNnSc4d3	nejznámější
pak	pak	k6eAd1	pak
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
Býčí	býčí	k2eAgFnSc1d1	býčí
skála	skála	k1gFnSc1	skála
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1872	[number]	k4	1872
archeolog	archeolog	k1gMnSc1	archeolog
Jindřich	Jindřich	k1gMnSc1	Jindřich
Wankel	Wankel	k1gMnSc1	Wankel
nalezl	nalézt	k5eAaBmAgMnS	nalézt
známou	známý	k2eAgFnSc4d1	známá
plastiku	plastika	k1gFnSc4	plastika
bronzového	bronzový	k2eAgMnSc2d1	bronzový
býčka	býček	k1gMnSc2	býček
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
Jáchymka	Jáchymka	k1gFnSc1	Jáchymka
<g/>
,	,	kIx,	,
Sobolova	Sobolův	k2eAgFnSc1d1	Sobolova
jeskyně	jeskyně	k1gFnSc1	jeskyně
<g/>
,	,	kIx,	,
Kostelík	kostelík	k1gInSc1	kostelík
<g/>
,	,	kIx,	,
Výpustek	výpustek	k1gInSc1	výpustek
<g/>
,	,	kIx,	,
Jestřábka	jestřábek	k1gMnSc2	jestřábek
<g/>
,	,	kIx,	,
Drátenická	drátenický	k2eAgFnSc1d1	Drátenická
jeskyně	jeskyně	k1gFnSc1	jeskyně
<g/>
,	,	kIx,	,
jeskyně	jeskyně	k1gFnSc1	jeskyně
Vokounka	Vokounka	k1gFnSc1	Vokounka
a	a	k8xC	a
Nad	nad	k7c7	nad
Švýcárnou	Švýcárna	k1gFnSc7	Švýcárna
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
celém	celý	k2eAgNnSc6d1	celé
území	území	k1gNnSc6	území
Moravského	moravský	k2eAgInSc2d1	moravský
krasu	kras	k1gInSc2	kras
se	se	k3xPyFc4	se
celkem	celkem	k6eAd1	celkem
nachází	nacházet	k5eAaImIp3nS	nacházet
přes	přes	k7c4	přes
1	[number]	k4	1
600	[number]	k4	600
jeskyní	jeskyně	k1gFnPc2	jeskyně
<g/>
.	.	kIx.	.
</s>
<s>
Hrady	hrad	k1gInPc1	hrad
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
hustých	hustý	k2eAgInPc6d1	hustý
adamovských	adamovský	k2eAgInPc6d1	adamovský
lesích	les	k1gInPc6	les
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
Nový	nový	k2eAgInSc4d1	nový
hrad	hrad	k1gInSc4	hrad
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
se	se	k3xPyFc4	se
zbytky	zbytek	k1gInPc1	zbytek
původní	původní	k2eAgFnSc2d1	původní
fortifikace	fortifikace	k1gFnSc2	fortifikace
nazývané	nazývaný	k2eAgFnSc2d1	nazývaná
Starý	starý	k2eAgInSc4d1	starý
hrad	hrad	k1gInSc4	hrad
<g/>
,	,	kIx,	,
Ronov	Ronov	k1gInSc4	Ronov
zaniklý	zaniklý	k2eAgInSc4d1	zaniklý
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
Hrádek	hrádek	k1gInSc1	hrádek
u	u	k7c2	u
Babic	Babice	k1gFnPc2	Babice
v	v	k7c6	v
Křtinském	Křtinský	k2eAgNnSc6d1	Křtinské
údolí	údolí	k1gNnSc6	údolí
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
pozůstatky	pozůstatek	k1gInPc7	pozůstatek
po	po	k7c6	po
těžbě	těžba	k1gFnSc6	těžba
železné	železný	k2eAgFnSc2d1	železná
rudy	ruda	k1gFnSc2	ruda
v	v	k7c6	v
okolních	okolní	k2eAgInPc6d1	okolní
lesích	les	k1gInPc6	les
patří	patřit	k5eAaImIp3nS	patřit
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
několika	několik	k4yIc2	několik
pozůstatků	pozůstatek	k1gInPc2	pozůstatek
po	po	k7c6	po
důlních	důlní	k2eAgFnPc6d1	důlní
mírách	míra	k1gFnPc6	míra
<g/>
,	,	kIx,	,
také	také	k9	také
tzv.	tzv.	kA	tzv.
Stará	starý	k2eAgFnSc1d1	stará
huť	huť	k1gFnSc1	huť
z	z	k7c2	z
poloviny	polovina	k1gFnSc2	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
1971	[number]	k4	1971
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
kulturní	kulturní	k2eAgFnSc7d1	kulturní
památkou	památka	k1gFnSc7	památka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nedalekém	daleký	k2eNgInSc6d1	nedaleký
Vranově	Vranov	k1gInSc6	Vranov
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
poutní	poutní	k2eAgInSc1d1	poutní
kostel	kostel	k1gInSc1	kostel
Narození	narození	k1gNnSc2	narození
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
s	s	k7c7	s
hrobkou	hrobka	k1gFnSc7	hrobka
rodu	rod	k1gInSc2	rod
Lichtenštejnů	Lichtenštejn	k1gInPc2	Lichtenštejn
a	a	k8xC	a
ve	v	k7c6	v
Křtinách	křtiny	k1gFnPc6	křtiny
kostel	kostel	k1gInSc1	kostel
Jména	jméno	k1gNnSc2	jméno
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
postavená	postavený	k2eAgFnSc1d1	postavená
kamenná	kamenný	k2eAgFnSc1d1	kamenná
Alexandrova	Alexandrův	k2eAgFnSc1d1	Alexandrova
rozhledna	rozhledna	k1gFnSc1	rozhledna
tyčící	tyčící	k2eAgFnSc2d1	tyčící
se	se	k3xPyFc4	se
nad	nad	k7c7	nad
svitavským	svitavský	k2eAgNnSc7d1	svitavské
údolím	údolí	k1gNnSc7	údolí
je	být	k5eAaImIp3nS	být
dominantou	dominanta	k1gFnSc7	dominanta
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
lesích	les	k1gInPc6	les
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
katastru	katastr	k1gInSc6	katastr
sousední	sousední	k2eAgFnSc2d1	sousední
obce	obec	k1gFnSc2	obec
Babice	babice	k1gFnSc2	babice
nad	nad	k7c7	nad
Svitavou	Svitava	k1gFnSc7	Svitava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
adamovských	adamovský	k2eAgInPc6d1	adamovský
lesích	les	k1gInPc6	les
tvoří	tvořit	k5eAaImIp3nS	tvořit
unikátní	unikátní	k2eAgFnSc1d1	unikátní
řada	řada	k1gFnSc1	řada
pamětních	pamětní	k2eAgFnPc2d1	pamětní
desek	deska	k1gFnPc2	deska
i	i	k8xC	i
větších	veliký	k2eAgInPc2d2	veliký
pomníků	pomník	k1gInPc2	pomník
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Máchův	Máchův	k2eAgInSc1d1	Máchův
památník	památník	k1gInSc1	památník
<g/>
)	)	kIx)	)
a	a	k8xC	a
kamenných	kamenný	k2eAgInPc2d1	kamenný
portálů	portál	k1gInPc2	portál
studánek	studánka	k1gFnPc2	studánka
tzv.	tzv.	kA	tzv.
Lesnický	lesnický	k2eAgInSc4d1	lesnický
Slavín	Slavín	k1gInSc4	Slavín
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
již	již	k6eAd1	již
vzniká	vznikat	k5eAaImIp3nS	vznikat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
založení	založení	k1gNnSc3	založení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
okolních	okolní	k2eAgInPc6d1	okolní
lesích	les	k1gInPc6	les
je	být	k5eAaImIp3nS	být
také	také	k6eAd1	také
vyhlášeno	vyhlásit	k5eAaPmNgNnS	vyhlásit
několik	několik	k4yIc1	několik
přírodních	přírodní	k2eAgFnPc2d1	přírodní
rezervací	rezervace	k1gFnPc2	rezervace
-	-	kIx~	-
Dřínová	dřínový	k2eAgFnSc1d1	Dřínová
<g/>
,	,	kIx,	,
Jelení	jelení	k2eAgInSc1d1	jelení
skok	skok	k1gInSc1	skok
<g/>
,	,	kIx,	,
Coufavá	Coufavý	k2eAgFnSc1d1	Coufavá
-	-	kIx~	-
a	a	k8xC	a
národní	národní	k2eAgFnSc2d1	národní
přírodní	přírodní	k2eAgFnPc4d1	přírodní
rezervace	rezervace	k1gFnSc2	rezervace
Býčí	býčí	k2eAgFnSc1d1	býčí
skála	skála	k1gFnSc1	skála
<g/>
.	.	kIx.	.
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Bernášek	Bernášek	k1gMnSc1	Bernášek
(	(	kIx(	(
<g/>
1951	[number]	k4	1951
<g/>
)	)	kIx)	)
-	-	kIx~	-
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
poslanec	poslanec	k1gMnSc1	poslanec
a	a	k8xC	a
adamovský	adamovský	k1gMnSc1	adamovský
starosta	starosta	k1gMnSc1	starosta
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Fibich	Fibich	k1gMnSc1	Fibich
(	(	kIx(	(
<g/>
1819	[number]	k4	1819
<g/>
-	-	kIx~	-
<g/>
1882	[number]	k4	1882
<g/>
)	)	kIx)	)
-	-	kIx~	-
otec	otec	k1gMnSc1	otec
hudebního	hudební	k2eAgMnSc2d1	hudební
skladatele	skladatel	k1gMnSc2	skladatel
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Fibicha	Fibich	k1gMnSc2	Fibich
<g/>
,	,	kIx,	,
v	v	k7c6	v
Adamově	Adamov	k1gInSc6	Adamov
žil	žít	k5eAaImAgMnS	žít
na	na	k7c6	na
odpočinku	odpočinek	k1gInSc6	odpočinek
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
taktéž	taktéž	k?	taktéž
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
<g/>
.	.	kIx.	.
</s>
<s>
Anna	Anna	k1gFnSc1	Anna
Hübnerová	Hübnerová	k1gFnSc1	Hübnerová
(	(	kIx(	(
<g/>
1860	[number]	k4	1860
<g/>
-	-	kIx~	-
<g/>
1891	[number]	k4	1891
<g/>
)	)	kIx)	)
-	-	kIx~	-
populární	populární	k2eAgFnSc1d1	populární
herečka	herečka	k1gFnSc1	herečka
prvního	první	k4xOgNnSc2	první
českého	český	k2eAgNnSc2d1	české
divadla	divadlo	k1gNnSc2	divadlo
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pochována	pochovat	k5eAaPmNgFnS	pochovat
v	v	k7c6	v
Adamově	Adamov	k1gInSc6	Adamov
<g/>
.	.	kIx.	.
</s>
<s>
Vatroslav	Vatroslav	k1gMnSc1	Vatroslav
Jagić	Jagić	k1gMnSc1	Jagić
(	(	kIx(	(
<g/>
1838	[number]	k4	1838
<g/>
-	-	kIx~	-
<g/>
1923	[number]	k4	1923
<g/>
)	)	kIx)	)
-	-	kIx~	-
chorvatský	chorvatský	k2eAgMnSc1d1	chorvatský
slavista	slavista	k1gMnSc1	slavista
<g/>
,	,	kIx,	,
nějakou	nějaký	k3yIgFnSc4	nějaký
dobu	doba	k1gFnSc4	doba
strávil	strávit	k5eAaPmAgMnS	strávit
v	v	k7c6	v
Adamově	Adamov	k1gInSc6	Adamov
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1897	[number]	k4	1897
zde	zde	k6eAd1	zde
započal	započnout	k5eAaPmAgInS	započnout
psát	psát	k5eAaImF	psát
svoje	svůj	k3xOyFgFnPc4	svůj
paměti	paměť	k1gFnPc4	paměť
<g/>
.	.	kIx.	.
</s>
<s>
Peter	Peter	k1gMnSc1	Peter
Jilemnický	jilemnický	k2eAgMnSc1d1	jilemnický
(	(	kIx(	(
<g/>
1901	[number]	k4	1901
<g/>
-	-	kIx~	-
<g/>
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
-	-	kIx~	-
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
v	v	k7c6	v
Adamově	Adamov	k1gInSc6	Adamov
pobýval	pobývat	k5eAaImAgMnS	pobývat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1928	[number]	k4	1928
<g/>
-	-	kIx~	-
<g/>
1929	[number]	k4	1929
společně	společně	k6eAd1	společně
se	s	k7c7	s
svojí	svůj	k3xOyFgFnSc7	svůj
manželkou	manželka	k1gFnSc7	manželka
Růženou	Růžena	k1gFnSc7	Růžena
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Adamově	Adamov	k1gInSc6	Adamov
byla	být	k5eAaImAgFnS	být
vystavěna	vystavěn	k2eAgFnSc1d1	vystavěna
jeho	jeho	k3xOp3gFnSc1	jeho
busta	busta	k1gFnSc1	busta
a	a	k8xC	a
pojmenovaná	pojmenovaný	k2eAgFnSc1d1	pojmenovaná
ulice	ulice	k1gFnSc1	ulice
v	v	k7c6	v
nové	nový	k2eAgFnSc6d1	nová
výstavbě	výstavba	k1gFnSc6	výstavba
<g/>
.	.	kIx.	.
</s>
<s>
Johann	Johann	k1gMnSc1	Johann
Victor	Victor	k1gMnSc1	Victor
Krämer	Krämer	k1gMnSc1	Krämer
(	(	kIx(	(
<g/>
1861	[number]	k4	1861
<g/>
-	-	kIx~	-
<g/>
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
-	-	kIx~	-
malíř	malíř	k1gMnSc1	malíř
<g/>
.	.	kIx.	.
</s>
<s>
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Kroupa	Kroupa	k1gMnSc1	Kroupa
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
-	-	kIx~	-
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
-	-	kIx~	-
operní	operní	k2eAgMnSc1d1	operní
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
,	,	kIx,	,
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
Čestný	čestný	k2eAgMnSc1d1	čestný
občan	občan	k1gMnSc1	občan
Adamova	Adamov	k1gInSc2	Adamov
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
Štěpán	Štěpán	k1gMnSc1	Štěpán
Maleček	Maleček	k1gMnSc1	Maleček
(	(	kIx(	(
<g/>
1890	[number]	k4	1890
<g/>
-	-	kIx~	-
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
-	-	kIx~	-
akademický	akademický	k2eAgMnSc1d1	akademický
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
svou	svůj	k3xOyFgFnSc7	svůj
tvorbou	tvorba	k1gFnSc7	tvorba
se	se	k3xPyFc4	se
vracel	vracet	k5eAaImAgInS	vracet
do	do	k7c2	do
Adamova	Adamov	k1gInSc2	Adamov
<g/>
,	,	kIx,	,
Moravského	moravský	k2eAgInSc2d1	moravský
krasu	kras	k1gInSc2	kras
a	a	k8xC	a
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
</s>
<s>
František	František	k1gMnSc1	František
Xaver	Xaver	k1gMnSc1	Xaver
Naske	Naske	k1gFnSc1	Naske
(	(	kIx(	(
<g/>
1884	[number]	k4	1884
<g/>
-	-	kIx~	-
<g/>
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
-	-	kIx~	-
akademický	akademický	k2eAgMnSc1d1	akademický
malíř	malíř	k1gMnSc1	malíř
a	a	k8xC	a
grafik	grafik	k1gMnSc1	grafik
<g/>
,	,	kIx,	,
manžel	manžel	k1gMnSc1	manžel
herečky	herečka	k1gFnSc2	herečka
Růženy	Růžena	k1gFnSc2	Růžena
Naskové	Nasková	k1gFnSc2	Nasková
<g/>
,	,	kIx,	,
svá	svůj	k3xOyFgNnPc4	svůj
školní	školní	k2eAgNnPc4d1	školní
léta	léto	k1gNnPc4	léto
prožil	prožít	k5eAaPmAgMnS	prožít
v	v	k7c6	v
Adamově	Adamov	k1gInSc6	Adamov
<g/>
.	.	kIx.	.
</s>
<s>
František	František	k1gMnSc1	František
Pokorný	Pokorný	k1gMnSc1	Pokorný
(	(	kIx(	(
<g/>
1902	[number]	k4	1902
<g/>
-	-	kIx~	-
<g/>
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
-	-	kIx~	-
výtvarník	výtvarník	k1gMnSc1	výtvarník
<g/>
.	.	kIx.	.
</s>
<s>
Leonid	Leonid	k1gInSc1	Leonid
Pribytkov	Pribytkov	k1gInSc1	Pribytkov
(	(	kIx(	(
<g/>
1895	[number]	k4	1895
<g/>
-	-	kIx~	-
<g/>
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
-	-	kIx~	-
operní	operní	k2eAgMnSc1d1	operní
pěvec	pěvec	k1gMnSc1	pěvec
ruského	ruský	k2eAgInSc2d1	ruský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1925	[number]	k4	1925
<g/>
-	-	kIx~	-
<g/>
1952	[number]	k4	1952
člen	člen	k1gInSc1	člen
opery	opera	k1gFnSc2	opera
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
Brno	Brno	k1gNnSc1	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
též	též	k6eAd1	též
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
<g/>
.	.	kIx.	.
</s>
<s>
Robert	Robert	k1gMnSc1	Robert
Schoeller	Schoeller	k1gMnSc1	Schoeller
(	(	kIx(	(
<g/>
1873	[number]	k4	1873
<g/>
-	-	kIx~	-
<g/>
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
-	-	kIx~	-
obchodník	obchodník	k1gMnSc1	obchodník
v	v	k7c6	v
cukrárenském	cukrárenský	k2eAgInSc6d1	cukrárenský
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Antonín	Antonín	k1gMnSc1	Antonín
Schumann	Schumann	k1gMnSc1	Schumann
(	(	kIx(	(
<g/>
1868	[number]	k4	1868
<g/>
-	-	kIx~	-
<g/>
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
-	-	kIx~	-
malíř	malíř	k1gMnSc1	malíř
samouk	samouk	k1gMnSc1	samouk
<g/>
,	,	kIx,	,
zachytil	zachytit	k5eAaPmAgMnS	zachytit
na	na	k7c6	na
svých	svůj	k3xOyFgInPc6	svůj
obrazech	obraz	k1gInPc6	obraz
Adamov	Adamov	k1gInSc1	Adamov
takový	takový	k3xDgInSc1	takový
<g/>
,	,	kIx,	,
jaký	jaký	k3yRgInSc1	jaký
tehdy	tehdy	k6eAd1	tehdy
byl	být	k5eAaImAgInS	být
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Adamově	Adamov	k1gInSc6	Adamov
působil	působit	k5eAaImAgMnS	působit
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1910	[number]	k4	1910
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1936	[number]	k4	1936
<g/>
.	.	kIx.	.
</s>
<s>
Theodor	Theodor	k1gMnSc1	Theodor
Slouka	Slouk	k1gMnSc2	Slouk
(	(	kIx(	(
<g/>
1884	[number]	k4	1884
<g/>
-	-	kIx~	-
<g/>
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
-	-	kIx~	-
učitel	učitel	k1gMnSc1	učitel
<g/>
,	,	kIx,	,
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
odboji	odboj	k1gInSc6	odboj
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
umučen	umučit	k5eAaPmNgMnS	umučit
v	v	k7c6	v
káznici	káznice	k1gFnSc6	káznice
<g/>
.	.	kIx.	.
</s>
<s>
Jindřich	Jindřich	k1gMnSc1	Jindřich
Svoboda	Svoboda	k1gMnSc1	Svoboda
(	(	kIx(	(
<g/>
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
-	-	kIx~	-
fotbalista	fotbalista	k1gMnSc1	fotbalista
<g/>
,	,	kIx,	,
olympijský	olympijský	k2eAgMnSc1d1	olympijský
vítěz	vítěz	k1gMnSc1	vítěz
1980	[number]	k4	1980
a	a	k8xC	a
mistr	mistr	k1gMnSc1	mistr
republiky	republika	k1gFnSc2	republika
1978	[number]	k4	1978
<g/>
.	.	kIx.	.
</s>
