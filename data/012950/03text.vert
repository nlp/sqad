<p>
<s>
Reuss	Reuss	k6eAd1	Reuss
je	být	k5eAaImIp3nS	být
řeka	řeka	k1gFnSc1	řeka
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
(	(	kIx(	(
<g/>
kantony	kanton	k1gInPc4	kanton
Uri	Uri	k1gFnPc2	Uri
<g/>
,	,	kIx,	,
Schwyz	Schwyza	k1gFnPc2	Schwyza
<g/>
,	,	kIx,	,
Nidwalden	Nidwaldna	k1gFnPc2	Nidwaldna
<g/>
,	,	kIx,	,
Obwalden	Obwaldna	k1gFnPc2	Obwaldna
<g/>
,	,	kIx,	,
Lucern	Lucerna	k1gFnPc2	Lucerna
a	a	k8xC	a
Aargau	Aargaus	k1gInSc2	Aargaus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pravým	pravý	k2eAgInSc7d1	pravý
přítokem	přítok	k1gInSc7	přítok
Aary	Aara	k1gFnSc2	Aara
(	(	kIx(	(
<g/>
povodí	povodit	k5eAaPmIp3nS	povodit
Rýnu	rýna	k1gFnSc4	rýna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
159	[number]	k4	159
km	km	kA	km
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
<g/>
.	.	kIx.	.
</s>
<s>
Povodí	povodí	k1gNnSc1	povodí
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
3425	[number]	k4	3425
km2	km2	k4	km2
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yRnSc2	což
134	[number]	k4	134
km2	km2	k4	km2
zaujímají	zaujímat	k5eAaImIp3nP	zaujímat
ledovce	ledovec	k1gInPc1	ledovec
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Rýnu	Rýn	k1gInSc6	Rýn
<g/>
,	,	kIx,	,
Aaře	Aaře	k1gFnSc6	Aaře
a	a	k8xC	a
Rhôně	Rhôna	k1gFnSc6	Rhôna
je	být	k5eAaImIp3nS	být
čtvrtou	čtvrtý	k4xOgFnSc7	čtvrtý
největší	veliký	k2eAgFnSc7d3	veliký
švýcarskou	švýcarský	k2eAgFnSc7d1	švýcarská
řekou	řeka	k1gFnSc7	řeka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Průběh	průběh	k1gInSc1	průběh
toku	tok	k1gInSc2	tok
==	==	k?	==
</s>
</p>
<p>
<s>
Pramení	pramenit	k5eAaImIp3nS	pramenit
v	v	k7c6	v
Gotthardském	Gotthardský	k2eAgInSc6d1	Gotthardský
masivu	masiv	k1gInSc6	masiv
na	na	k7c6	na
severních	severní	k2eAgInPc6d1	severní
svazích	svah	k1gInPc6	svah
Lepontinských	Lepontinský	k2eAgFnPc2d1	Lepontinský
Alp	Alpy	k1gFnPc2	Alpy
<g/>
.	.	kIx.	.
</s>
<s>
Vlastní	vlastní	k2eAgInSc1d1	vlastní
Reuss	Reuss	k1gInSc1	Reuss
vzniká	vznikat	k5eAaImIp3nS	vznikat
soutokem	soutok	k1gInSc7	soutok
zdrojnic	zdrojnice	k1gFnPc2	zdrojnice
Gotthardreuss	Gotthardreussa	k1gFnPc2	Gotthardreussa
(	(	kIx(	(
<g/>
pramení	pramenit	k5eAaImIp3nS	pramenit
v	v	k7c6	v
Gotthardském	Gotthardský	k2eAgNnSc6d1	Gotthardský
sedle	sedlo	k1gNnSc6	sedlo
<g/>
)	)	kIx)	)
a	a	k8xC	a
Furkareuss	Furkareuss	k1gInSc1	Furkareuss
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
pramení	pramenit	k5eAaImIp3nS	pramenit
ve	v	k7c6	v
Furka-sedle	Furkaedlo	k1gNnSc6	Furka-sedlo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
stékají	stékat	k5eAaImIp3nP	stékat
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
Urseren-tal	Urserenal	k1gMnSc1	Urseren-tal
<g/>
.	.	kIx.	.
</s>
<s>
Řeka	řeka	k1gFnSc1	řeka
protéká	protékat	k5eAaImIp3nS	protékat
ledovcovým	ledovcový	k2eAgNnSc7d1	ledovcové
údolím	údolí	k1gNnSc7	údolí
a	a	k8xC	a
poblíž	poblíž	k6eAd1	poblíž
Andermattu	Andermatt	k1gInSc2	Andermatt
se	se	k3xPyFc4	se
stáčí	stáčet	k5eAaImIp3nS	stáčet
na	na	k7c4	na
sever	sever	k1gInSc4	sever
a	a	k8xC	a
protéká	protékat	k5eAaImIp3nS	protékat
divokou	divoký	k2eAgFnSc7d1	divoká
soutěskou	soutěska	k1gFnSc7	soutěska
Schöllenen-schlucht	Schöllenenchluchta	k1gFnPc2	Schöllenen-schluchta
<g/>
.	.	kIx.	.
</s>
<s>
Neschůdná	schůdný	k2eNgFnSc1d1	neschůdná
Schöllenen-schlucht	Schöllenenchlucht	k2eAgMnSc1d1	Schöllenen-schlucht
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
strmými	strmý	k2eAgMnPc7d1	strmý
<g/>
,	,	kIx,	,
stovky	stovka	k1gFnPc4	stovka
metrů	metr	k1gInPc2	metr
vysokými	vysoký	k2eAgFnPc7d1	vysoká
žulovými	žulový	k2eAgFnPc7d1	Žulová
stěnami	stěna	k1gFnPc7	stěna
byla	být	k5eAaImAgFnS	být
odedávna	odedávna	k6eAd1	odedávna
překážkou	překážka	k1gFnSc7	překážka
pro	pro	k7c4	pro
zdolání	zdolání	k1gNnSc4	zdolání
Gotthardského	Gotthardský	k2eAgNnSc2d1	Gotthardský
sedla	sedlo	k1gNnSc2	sedlo
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
jen	jen	k9	jen
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
obtížných	obtížný	k2eAgFnPc2d1	obtížná
staveb	stavba	k1gFnPc2	stavba
<g/>
,	,	kIx,	,
jakými	jaký	k3yIgFnPc7	jaký
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
most	most	k1gInSc1	most
Teufelsbrücke	Teufelsbrücke	k1gInSc1	Teufelsbrücke
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
klene	klenout	k5eAaImIp3nS	klenout
přes	přes	k7c4	přes
propast	propast	k1gFnSc4	propast
Urnerloch	Urnerloch	k1gMnSc1	Urnerloch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
soutěsce	soutěska	k1gFnSc6	soutěska
se	se	k3xPyFc4	se
také	také	k9	také
nachází	nacházet	k5eAaImIp3nS	nacházet
pomník	pomník	k1gInSc4	pomník
věnovaný	věnovaný	k2eAgInSc4d1	věnovaný
generálovi	generál	k1gMnSc6	generál
Suvorovovi	Suvorova	k1gMnSc6	Suvorova
<g/>
.	.	kIx.	.
</s>
<s>
Pomník	pomník	k1gInSc1	pomník
byl	být	k5eAaImAgInS	být
vystavěn	vystavět	k5eAaPmNgInS	vystavět
na	na	k7c4	na
počet	počet	k1gInSc4	počet
tažení	tažení	k1gNnSc2	tažení
generála	generál	k1gMnSc4	generál
Suvorova	Suvorův	k2eAgMnSc4d1	Suvorův
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1799	[number]	k4	1799
proti	proti	k7c3	proti
Napoleonovi	Napoleon	k1gMnSc3	Napoleon
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
do	do	k7c2	do
osady	osada	k1gFnSc2	osada
Erstfeld	Erstfeld	k1gInSc1	Erstfeld
následují	následovat	k5eAaImIp3nP	následovat
další	další	k2eAgFnPc4d1	další
soutěsky	soutěska	k1gFnPc4	soutěska
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
teče	téct	k5eAaImIp3nS	téct
řeka	řeka	k1gFnSc1	řeka
širokým	široký	k2eAgNnSc7d1	široké
údolím	údolí	k1gNnSc7	údolí
až	až	k9	až
do	do	k7c2	do
města	město	k1gNnSc2	město
Flüelen	Flüelna	k1gFnPc2	Flüelna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
vlévá	vlévat	k5eAaImIp3nS	vlévat
do	do	k7c2	do
Lucernského	Lucernský	k2eAgNnSc2d1	Lucernský
jezera	jezero	k1gNnSc2	jezero
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
Luzernu	Luzerna	k1gFnSc4	Luzerna
opouští	opouštět	k5eAaImIp3nS	opouštět
řeka	řeka	k1gFnSc1	řeka
<g/>
,	,	kIx,	,
v	v	k7c4	v
tu	ten	k3xDgFnSc4	ten
dobu	doba	k1gFnSc4	doba
již	již	k6eAd1	již
široká	široký	k2eAgFnSc1d1	široká
<g/>
,	,	kIx,	,
jezero	jezero	k1gNnSc1	jezero
a	a	k8xC	a
teče	téct	k5eAaImIp3nS	téct
v	v	k7c6	v
meandrech	meandr	k1gInPc6	meandr
nejdříve	dříve	k6eAd3	dříve
na	na	k7c4	na
východ	východ	k1gInSc4	východ
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
na	na	k7c4	na
sever	sever	k1gInSc4	sever
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
protíná	protínat	k5eAaImIp3nS	protínat
Švýcarské	švýcarský	k2eAgNnSc4d1	švýcarské
předalpí	předalpí	k1gNnSc4	předalpí
<g/>
.	.	kIx.	.
</s>
<s>
Řeka	řeka	k1gFnSc1	řeka
se	se	k3xPyFc4	se
vlévá	vlévat	k5eAaImIp3nS	vlévat
zprava	zprava	k6eAd1	zprava
do	do	k7c2	do
řeky	řeka	k1gFnSc2	řeka
Aary	Aara	k1gFnSc2	Aara
<g/>
.	.	kIx.	.
</s>
<s>
Reuss	Reuss	k6eAd1	Reuss
také	také	k9	také
tvoří	tvořit	k5eAaImIp3nP	tvořit
západní	západní	k2eAgFnSc4d1	západní
hranici	hranice	k1gFnSc4	hranice
Glarnským	Glarnský	k2eAgFnPc3d1	Glarnský
Alpám	Alpy	k1gFnPc3	Alpy
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vodní	vodní	k2eAgInSc1d1	vodní
stav	stav	k1gInSc1	stav
==	==	k?	==
</s>
</p>
<p>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
průtok	průtok	k1gInSc1	průtok
vody	voda	k1gFnSc2	voda
činí	činit	k5eAaImIp3nS	činit
140	[number]	k4	140
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Maxima	Maxima	k1gFnSc1	Maxima
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
povodí	povodí	k1gNnSc6	povodí
řeky	řeka	k1gFnSc2	řeka
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
několik	několik	k4yIc1	několik
vodních	vodní	k2eAgFnPc2d1	vodní
elektráren	elektrárna	k1gFnPc2	elektrárna
(	(	kIx(	(
<g/>
Göschenen	Göschenen	k1gInSc1	Göschenen
<g/>
,	,	kIx,	,
Amsteg	Amsteg	k1gInSc1	Amsteg
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Osídlení	osídlení	k1gNnSc2	osídlení
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
leží	ležet	k5eAaImIp3nS	ležet
města	město	k1gNnSc2	město
Erstfel	Erstfela	k1gFnPc2	Erstfela
<g/>
,	,	kIx,	,
Flüelen	Flüelna	k1gFnPc2	Flüelna
<g/>
,	,	kIx,	,
Lucern	Lucerna	k1gFnPc2	Lucerna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgFnP	být
použity	použit	k2eAgFnPc1d1	použita
informace	informace	k1gFnPc1	informace
z	z	k7c2	z
Velké	velký	k2eAgFnSc2d1	velká
sovětské	sovětský	k2eAgFnSc2d1	sovětská
encyklopedie	encyklopedie	k1gFnSc2	encyklopedie
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
"	"	kIx"	"
<g/>
Р	Р	k?	Р
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Reuss	Reussa	k1gFnPc2	Reussa
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
