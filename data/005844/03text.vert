<s>
Medúzovci	Medúzovec	k1gMnPc1	Medúzovec
(	(	kIx(	(
<g/>
Scyphozoa	Scyphozoa	k1gMnSc1	Scyphozoa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
třída	třída	k1gFnSc1	třída
mořských	mořský	k2eAgMnPc2d1	mořský
živočichů	živočich	k1gMnPc2	živočich
z	z	k7c2	z
kmene	kmen	k1gInSc2	kmen
žahavců	žahavec	k1gMnPc2	žahavec
<g/>
,	,	kIx,	,
do	do	k7c2	do
níž	jenž	k3xRgFnSc2	jenž
je	být	k5eAaImIp3nS	být
řazeno	řadit	k5eAaImNgNnS	řadit
přibližně	přibližně	k6eAd1	přibližně
220	[number]	k4	220
druhů	druh	k1gInPc2	druh
v	v	k7c6	v
71	[number]	k4	71
rodech	rod	k1gInPc6	rod
<g/>
.	.	kIx.	.
</s>
<s>
Medúzovci	Medúzovec	k1gMnPc1	Medúzovec
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
mořích	moře	k1gNnPc6	moře
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
od	od	k7c2	od
jižního	jižní	k2eAgInSc2d1	jižní
pólu	pól	k1gInSc2	pól
k	k	k7c3	k
severnímu	severní	k2eAgNnSc3d1	severní
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
v	v	k7c6	v
pobřežních	pobřežní	k2eAgInPc6d1	pobřežní
šelfech	šelf	k1gInPc6	šelf
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgInPc1d1	znám
i	i	k8xC	i
hlubinné	hlubinný	k2eAgInPc1d1	hlubinný
druhy	druh	k1gInPc1	druh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
životním	životní	k2eAgInSc6d1	životní
cyklu	cyklus	k1gInSc6	cyklus
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
stadium	stadium	k1gNnSc1	stadium
"	"	kIx"	"
<g/>
medúza	medúza	k1gFnSc1	medúza
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
volně	volně	k6eAd1	volně
plovoucí	plovoucí	k2eAgFnSc1d1	plovoucí
forma	forma	k1gFnSc1	forma
se	s	k7c7	s
zvonem	zvon	k1gInSc7	zvon
a	a	k8xC	a
žahavými	žahavý	k2eAgNnPc7d1	žahavé
rameny	rameno	k1gNnPc7	rameno
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
v	v	k7c6	v
případě	případ	k1gInSc6	případ
medúzovců	medúzovec	k1gInPc2	medúzovec
chybí	chybit	k5eAaPmIp3nS	chybit
plachetka	plachetka	k1gFnSc1	plachetka
(	(	kIx(	(
<g/>
velum	velum	k1gInSc1	velum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
druhů	druh	k1gInPc2	druh
má	mít	k5eAaImIp3nS	mít
dále	daleko	k6eAd2	daleko
rovněž	rovněž	k9	rovněž
přisedlé	přisedlý	k2eAgNnSc1d1	přisedlé
stádium	stádium	k1gNnSc1	stádium
známé	známý	k2eAgFnSc2d1	známá
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
polyp	polyp	k1gInSc1	polyp
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
na	na	k7c6	na
jehož	jehož	k3xOyRp3gInSc6	jehož
ústním	ústní	k2eAgInSc6d1	ústní
konci	konec	k1gInSc6	konec
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
strobilací	strobilace	k1gFnSc7	strobilace
odštěpují	odštěpovat	k5eAaImIp3nP	odštěpovat
nové	nový	k2eAgFnPc1d1	nová
mladé	mladý	k2eAgFnPc1d1	mladá
medúzy	medúza	k1gFnPc1	medúza
<g/>
.	.	kIx.	.
</s>
<s>
Ramena	rameno	k1gNnPc1	rameno
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
jedovaté	jedovatý	k2eAgFnPc1d1	jedovatá
látky	látka	k1gFnPc1	látka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mohou	moct	k5eAaImIp3nP	moct
při	při	k7c6	při
požahání	požahání	k1gNnSc6	požahání
vyvolat	vyvolat	k5eAaPmF	vyvolat
bolest	bolest	k1gFnSc4	bolest
a	a	k8xC	a
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
dokonce	dokonce	k9	dokonce
životně	životně	k6eAd1	životně
nebezpečné	bezpečný	k2eNgInPc1d1	nebezpečný
(	(	kIx(	(
<g/>
např.	např.	kA	např.
kořenoústka	kořenoústka	k1gFnSc1	kořenoústka
Lobonema	Lobonema	k1gFnSc1	Lobonema
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Polypy	polyp	k1gInPc1	polyp
jsou	být	k5eAaImIp3nP	být
zpravidla	zpravidla	k6eAd1	zpravidla
malé	malý	k2eAgFnPc1d1	malá
a	a	k8xC	a
nenápadné	nápadný	k2eNgFnPc1d1	nenápadná
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
koloniální	koloniální	k2eAgMnPc1d1	koloniální
nebo	nebo	k8xC	nebo
žijí	žít	k5eAaImIp3nP	žít
jednotlivě	jednotlivě	k6eAd1	jednotlivě
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
jeden	jeden	k4xCgMnSc1	jeden
tělní	tělní	k2eAgInSc1d1	tělní
otvor	otvor	k1gInSc1	otvor
a	a	k8xC	a
uvnitř	uvnitř	k7c2	uvnitř
těla	tělo	k1gNnSc2	tělo
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
relativně	relativně	k6eAd1	relativně
tenká	tenký	k2eAgFnSc1d1	tenká
vrstva	vrstva	k1gFnSc1	vrstva
gelovité	gelovitý	k2eAgFnSc2d1	gelovitá
tkáně	tkáň	k1gFnSc2	tkáň
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
mezoglea	mezoglea	k1gFnSc1	mezoglea
<g/>
.	.	kIx.	.
</s>
<s>
Medúzy	Medúza	k1gFnPc1	Medúza
představují	představovat	k5eAaImIp3nP	představovat
čtyřstranně	čtyřstranně	k6eAd1	čtyřstranně
souměrné	souměrný	k2eAgFnPc1d1	souměrná
<g/>
,	,	kIx,	,
volně	volně	k6eAd1	volně
plovoucí	plovoucí	k2eAgFnSc1d1	plovoucí
a	a	k8xC	a
samostatně	samostatně	k6eAd1	samostatně
žijící	žijící	k2eAgNnSc4d1	žijící
stadium	stadium	k1gNnSc4	stadium
<g/>
.	.	kIx.	.
</s>
<s>
Tělo	tělo	k1gNnSc1	tělo
<g/>
,	,	kIx,	,
složené	složený	k2eAgNnSc1d1	složené
z	z	k7c2	z
97	[number]	k4	97
%	%	kIx~	%
z	z	k7c2	z
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
zvonovité	zvonovitý	k2eAgNnSc1d1	zvonovité
a	a	k8xC	a
poloprůhledné	poloprůhledný	k2eAgNnSc1d1	poloprůhledné
či	či	k8xC	či
mléčného	mléčný	k2eAgInSc2d1	mléčný
vzhledu	vzhled	k1gInSc2	vzhled
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
pokožka	pokožka	k1gFnSc1	pokožka
(	(	kIx(	(
<g/>
epidermis	epidermis	k1gFnSc1	epidermis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
ní	on	k3xPp3gFnSc7	on
mezoglea	mezogle	k2eAgFnSc1d1	mezogle
(	(	kIx(	(
<g/>
tlustá	tlustý	k2eAgFnSc1d1	tlustá
želatinózní	želatinózní	k2eAgFnSc1d1	želatinózní
vrstva	vrstva	k1gFnSc1	vrstva
<g/>
)	)	kIx)	)
a	a	k8xC	a
zcela	zcela	k6eAd1	zcela
uvnitř	uvnitř	k6eAd1	uvnitř
je	být	k5eAaImIp3nS	být
gastrovaskulární	gastrovaskulární	k2eAgFnSc1d1	gastrovaskulární
dutina	dutina	k1gFnSc1	dutina
(	(	kIx(	(
<g/>
trávicí	trávicí	k2eAgFnSc2d1	trávicí
a	a	k8xC	a
"	"	kIx"	"
<g/>
cévní	cévní	k2eAgNnSc1d1	cévní
<g/>
"	"	kIx"	"
soustava	soustava	k1gFnSc1	soustava
<g/>
)	)	kIx)	)
s	s	k7c7	s
obalem	obal	k1gInSc7	obal
zvaným	zvaný	k2eAgInSc7d1	zvaný
gastrodermis	gastrodermis	k1gInSc4	gastrodermis
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
okrajích	okraj	k1gInPc6	okraj
zvonu	zvon	k1gInSc2	zvon
se	se	k3xPyFc4	se
tyto	tento	k3xDgFnPc1	tento
tři	tři	k4xCgFnPc1	tři
vrstvy	vrstva	k1gFnPc1	vrstva
spojují	spojovat	k5eAaImIp3nP	spojovat
<g/>
.	.	kIx.	.
</s>
<s>
Naspodu	naspodu	k7c2	naspodu
zvonu	zvon	k1gInSc2	zvon
(	(	kIx(	(
<g/>
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
zvaném	zvaný	k2eAgMnSc6d1	zvaný
subumbrella	subumbrella	k6eAd1	subumbrella
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ústní	ústní	k2eAgInSc4d1	ústní
otvor	otvor	k1gInSc4	otvor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mezogleální	mezogleální	k2eAgFnSc6d1	mezogleální
vrstvě	vrstva	k1gFnSc6	vrstva
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
svalovina	svalovina	k1gFnSc1	svalovina
umožňující	umožňující	k2eAgFnSc1d1	umožňující
pohyb	pohyb	k1gInSc1	pohyb
(	(	kIx(	(
<g/>
vypuzováním	vypuzování	k1gNnSc7	vypuzování
vody	voda	k1gFnSc2	voda
pod	pod	k7c7	pod
zvonem	zvon	k1gInSc7	zvon
pomocí	pomocí	k7c2	pomocí
svaloviny	svalovina	k1gFnSc2	svalovina
po	po	k7c6	po
obvodě	obvod	k1gInSc6	obvod
zvonu	zvon	k1gInSc2	zvon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nervová	nervový	k2eAgFnSc1d1	nervová
soustava	soustava	k1gFnSc1	soustava
má	mít	k5eAaImIp3nS	mít
síťovitou	síťovitý	k2eAgFnSc4d1	síťovitá
stavbu	stavba	k1gFnSc4	stavba
<g/>
,	,	kIx,	,
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
nervový	nervový	k2eAgInSc1d1	nervový
kruh	kruh	k1gInSc1	kruh
kolem	kolem	k6eAd1	kolem
dokola	dokola	k6eAd1	dokola
zvonu	zvon	k1gInSc2	zvon
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
důležitým	důležitý	k2eAgInPc3d1	důležitý
smyslovým	smyslový	k2eAgInPc3d1	smyslový
orgánům	orgán	k1gInPc3	orgán
patří	patřit	k5eAaImIp3nS	patřit
statocysta	statocysta	k1gFnSc1	statocysta
(	(	kIx(	(
<g/>
orgán	orgán	k1gInSc1	orgán
sloužící	sloužící	k1gFnSc2	sloužící
k	k	k7c3	k
určování	určování	k1gNnSc3	určování
polohy	poloha	k1gFnSc2	poloha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
i	i	k9	i
jednoduchá	jednoduchý	k2eAgNnPc1d1	jednoduché
očka	očko	k1gNnPc1	očko
umožňující	umožňující	k2eAgNnPc1d1	umožňující
vnímání	vnímání	k1gNnPc1	vnímání
světla	světlo	k1gNnSc2	světlo
a	a	k8xC	a
občas	občas	k6eAd1	občas
také	také	k9	také
speciální	speciální	k2eAgInPc1d1	speciální
orgány	orgán	k1gInPc1	orgán
hmatu	hmat	k1gInSc2	hmat
<g/>
.	.	kIx.	.
</s>
<s>
Koncentrují	koncentrovat	k5eAaBmIp3nP	koncentrovat
se	se	k3xPyFc4	se
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
na	na	k7c6	na
obvodu	obvod	k1gInSc6	obvod
zvonu	zvon	k1gInSc2	zvon
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
ropáliích	ropálie	k1gFnPc6	ropálie
<g/>
.	.	kIx.	.
</s>
<s>
Životní	životní	k2eAgInSc1d1	životní
cyklus	cyklus	k1gInSc1	cyklus
je	být	k5eAaImIp3nS	být
vlastně	vlastně	k9	vlastně
rodozměna	rodozměna	k1gFnSc1	rodozměna
(	(	kIx(	(
<g/>
metageneze	metageneze	k1gFnSc1	metageneze
<g/>
)	)	kIx)	)
ve	v	k7c6	v
zvláštní	zvláštní	k2eAgFnSc6d1	zvláštní
podobě	podoba	k1gFnSc6	podoba
–	–	k?	–
dochází	docházet	k5eAaImIp3nS	docházet
při	při	k7c6	při
ní	on	k3xPp3gFnSc6	on
k	k	k7c3	k
přechodu	přechod	k1gInSc3	přechod
od	od	k7c2	od
přisedlé	přisedlý	k2eAgFnSc2d1	přisedlá
formy	forma	k1gFnSc2	forma
(	(	kIx(	(
<g/>
vzniklé	vzniklý	k2eAgNnSc1d1	vzniklé
pohlavní	pohlavní	k2eAgFnSc7d1	pohlavní
cestou	cesta	k1gFnSc7	cesta
<g/>
)	)	kIx)	)
k	k	k7c3	k
volně	volně	k6eAd1	volně
se	se	k3xPyFc4	se
pohybující	pohybující	k2eAgMnPc1d1	pohybující
(	(	kIx(	(
<g/>
vzniklé	vzniklý	k2eAgNnSc1d1	vzniklé
nepohlavně	pohlavně	k6eNd1	pohlavně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
dospělém	dospělý	k2eAgInSc6d1	dospělý
polypu	polyp	k1gInSc6	polyp
se	se	k3xPyFc4	se
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
nový	nový	k2eAgMnSc1d1	nový
jedinec	jedinec	k1gMnSc1	jedinec
(	(	kIx(	(
<g/>
efyra	efyra	k1gMnSc1	efyra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
příčně	příčně	k6eAd1	příčně
odškrtí	odškrtit	k5eAaPmIp3nS	odškrtit
(	(	kIx(	(
<g/>
strobiluje	strobilovat	k5eAaBmIp3nS	strobilovat
<g/>
)	)	kIx)	)
a	a	k8xC	a
doroste	dorůst	k5eAaPmIp3nS	dorůst
do	do	k7c2	do
formy	forma	k1gFnSc2	forma
medúzy	medúza	k1gFnSc2	medúza
<g/>
,	,	kIx,	,
zvané	zvaný	k2eAgFnPc1d1	zvaná
scyfomedúza	scyfomedúza	k1gFnSc1	scyfomedúza
(	(	kIx(	(
<g/>
nepohlavní	pohlavní	k2eNgFnSc1d1	nepohlavní
část	část	k1gFnSc1	část
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
z	z	k7c2	z
gonád	gonáda	k1gFnPc2	gonáda
uvolní	uvolnit	k5eAaPmIp3nS	uvolnit
do	do	k7c2	do
gastrovaskulární	gastrovaskulární	k2eAgFnSc2d1	gastrovaskulární
soustavy	soustava	k1gFnSc2	soustava
pohlavní	pohlavní	k2eAgFnSc2d1	pohlavní
buňky	buňka	k1gFnSc2	buňka
a	a	k8xC	a
ústním	ústní	k2eAgInSc7d1	ústní
otvorem	otvor	k1gInSc7	otvor
vypustí	vypustit	k5eAaPmIp3nS	vypustit
do	do	k7c2	do
vody	voda	k1gFnSc2	voda
vajíčka	vajíčko	k1gNnSc2	vajíčko
nebo	nebo	k8xC	nebo
spermie	spermie	k1gFnSc2	spermie
(	(	kIx(	(
<g/>
pohlavní	pohlavní	k2eAgFnSc4d1	pohlavní
část	část	k1gFnSc4	část
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
<g/>
,	,	kIx,	,
medúzovci	medúzovec	k1gMnPc1	medúzovec
jsou	být	k5eAaImIp3nP	být
odděleného	oddělený	k2eAgNnSc2d1	oddělené
pohlaví	pohlaví	k1gNnSc2	pohlaví
–	–	k?	–
gonochoristé	gonochorista	k1gMnPc1	gonochorista
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
oplození	oplození	k1gNnSc3	oplození
dojde	dojít	k5eAaPmIp3nS	dojít
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
a	a	k8xC	a
rýhováním	rýhování	k1gNnSc7	rýhování
vzniká	vznikat	k5eAaImIp3nS	vznikat
larva	larva	k1gFnSc1	larva
(	(	kIx(	(
<g/>
planula	planout	k5eAaImAgFnS	planout
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
přisedne	přisednout	k5eAaPmIp3nS	přisednout
a	a	k8xC	a
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
se	se	k3xPyFc4	se
nový	nový	k2eAgInSc1d1	nový
polyp	polyp	k1gInSc1	polyp
(	(	kIx(	(
<g/>
scyfopolyp	scyfopolyp	k1gInSc1	scyfopolyp
<g/>
,	,	kIx,	,
scyphistoma	scyphistoma	k1gFnSc1	scyphistoma
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
řád	řád	k1gInSc1	řád
<g/>
:	:	kIx,	:
Kalichovky	kalichovka	k1gFnPc1	kalichovka
(	(	kIx(	(
<g/>
Stauromedusidae	Stauromedusida	k1gFnPc1	Stauromedusida
<g/>
)	)	kIx)	)
Medúzy	Medúza	k1gFnPc1	Medúza
jsou	být	k5eAaImIp3nP	být
přisedlé	přisedlý	k2eAgInPc1d1	přisedlý
hřbetní	hřbetní	k2eAgFnSc7d1	hřbetní
stranou	strana	k1gFnSc7	strana
k	k	k7c3	k
podkladu	podklad	k1gInSc3	podklad
<g/>
,	,	kIx,	,
tvarem	tvar	k1gInSc7	tvar
a	a	k8xC	a
způsobem	způsob	k1gInSc7	způsob
života	život	k1gInSc2	život
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
podobají	podobat	k5eAaImIp3nP	podobat
polypu	polyp	k1gInSc3	polyp
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
vývoj	vývoj	k1gInSc1	vývoj
není	být	k5eNaImIp3nS	být
dostatečně	dostatečně	k6eAd1	dostatečně
známý	známý	k2eAgMnSc1d1	známý
<g/>
,	,	kIx,	,
nevytvářejí	vytvářet	k5eNaImIp3nP	vytvářet
typickou	typický	k2eAgFnSc4d1	typická
planulu	planula	k1gFnSc4	planula
ani	ani	k8xC	ani
typického	typický	k2eAgMnSc4d1	typický
polypa	polyp	k1gMnSc4	polyp
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
chladných	chladný	k2eAgNnPc6d1	chladné
mořích	moře	k1gNnPc6	moře
<g/>
,	,	kIx,	,
cirkumpolárně	cirkumpolárně	k6eAd1	cirkumpolárně
<g/>
.	.	kIx.	.
kalichovka	kalichovka	k1gFnSc1	kalichovka
(	(	kIx(	(
<g/>
Lucernaria	Lucernarium	k1gNnSc2	Lucernarium
<g/>
)	)	kIx)	)
obývá	obývat	k5eAaImIp3nS	obývat
chladná	chladný	k2eAgNnPc4d1	chladné
moře	moře	k1gNnPc4	moře
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
polokouli	polokoule	k1gFnSc6	polokoule
řád	řád	k1gInSc1	řád
<g/>
:	:	kIx,	:
Korunovky	Korunovka	k1gFnPc1	Korunovka
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
<g/>
.	.	kIx.	.
</s>
<s>
Kruhovky	kruhovka	k1gFnPc1	kruhovka
(	(	kIx(	(
<g/>
Coronata	Coronat	k2eAgFnSc1d1	Coronat
<g/>
)	)	kIx)	)
Exumbrella	Exumbrella	k1gFnSc1	Exumbrella
vybíhá	vybíhat	k5eAaImIp3nS	vybíhat
dolů	dolů	k6eAd1	dolů
v	v	k7c4	v
pás	pás	k1gInSc4	pás
oddělený	oddělený	k2eAgInSc4d1	oddělený
zářezem	zářez	k1gInSc7	zářez
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
medúza	medúza	k1gFnSc1	medúza
pak	pak	k6eAd1	pak
může	moct	k5eAaImIp3nS	moct
tvarem	tvar	k1gInSc7	tvar
připomínat	připomínat	k5eAaImF	připomínat
přilbici	přilbice	k1gFnSc4	přilbice
nebo	nebo	k8xC	nebo
korunu	koruna	k1gFnSc4	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
hloubkách	hloubka	k1gFnPc6	hloubka
téměř	téměř	k6eAd1	téměř
všech	všecek	k3xTgNnPc2	všecek
světových	světový	k2eAgNnPc2d1	světové
moří	moře	k1gNnPc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Korunovka	Korunovka	k1gFnSc1	Korunovka
purpurová	purpurový	k2eAgFnSc1d1	purpurová
(	(	kIx(	(
<g/>
Periphylla	Periphylla	k1gFnSc1	Periphylla
periphylla	periphylla	k1gFnSc1	periphylla
<g/>
)	)	kIx)	)
řád	řád	k1gInSc1	řád
<g/>
:	:	kIx,	:
Talířovky	Talířovka	k1gFnPc1	Talířovka
(	(	kIx(	(
<g/>
Semaeostomea	Semaeostomea	k1gFnSc1	Semaeostomea
<g/>
)	)	kIx)	)
Medúzy	Medúza	k1gFnPc1	Medúza
mívají	mívat	k5eAaImIp3nP	mívat
obvykle	obvykle	k6eAd1	obvykle
málo	málo	k6eAd1	málo
vyklenuté	vyklenutý	k2eAgNnSc4d1	vyklenuté
tělo	tělo	k1gNnSc4	tělo
a	a	k8xC	a
dorůstají	dorůstat	k5eAaImIp3nP	dorůstat
větších	veliký	k2eAgInPc2d2	veliký
rozměrů	rozměr	k1gInPc2	rozměr
<g/>
.	.	kIx.	.
</s>
<s>
Chobotovité	chobotovitý	k2eAgNnSc1d1	chobotovitý
manubrium	manubrium	k1gNnSc1	manubrium
(	(	kIx(	(
<g/>
příústní	příústní	k2eAgInSc1d1	příústní
val	val	k1gInSc1	val
<g/>
)	)	kIx)	)
bývá	bývat	k5eAaImIp3nS	bývat
prodlouženo	prodloužit	k5eAaPmNgNnS	prodloužit
ve	v	k7c4	v
čtyři	čtyři	k4xCgInPc4	čtyři
výrazné	výrazný	k2eAgInPc4d1	výrazný
cípy	cíp	k1gInPc4	cíp
sloužící	sloužící	k1gFnSc2	sloužící
také	také	k9	také
k	k	k7c3	k
lovu	lov	k1gInSc3	lov
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Talířovka	Talířovka	k1gFnSc1	Talířovka
ušatá	ušatý	k2eAgFnSc1d1	ušatá
(	(	kIx(	(
<g/>
Aurelia	Aurelia	k1gFnSc1	Aurelia
aurita	aurita	k1gFnSc1	aurita
<g/>
)	)	kIx)	)
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
evropských	evropský	k2eAgNnPc6d1	Evropské
mořích	moře	k1gNnPc6	moře
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
40	[number]	k4	40
cm	cm	kA	cm
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
<g/>
,	,	kIx,	,
průměr	průměr	k1gInSc1	průměr
zvonu	zvon	k1gInSc2	zvon
ca	ca	kA	ca
15	[number]	k4	15
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
exumbrellu	exumbrella	k1gFnSc4	exumbrella
prosvítají	prosvítat	k5eAaImIp3nP	prosvítat
čtyři	čtyři	k4xCgFnPc4	čtyři
modré	modrý	k2eAgFnPc4d1	modrá
podkovovité	podkovovitý	k2eAgFnPc4d1	podkovovitá
(	(	kIx(	(
<g/>
ouškaté	ouškatý	k2eAgFnPc4d1	ouškatá
<g/>
)	)	kIx)	)
pohlavní	pohlavní	k2eAgFnPc4d1	pohlavní
žlázy	žláza	k1gFnPc4	žláza
<g/>
.	.	kIx.	.
</s>
<s>
Ramena	rameno	k1gNnPc1	rameno
manubria	manubrium	k1gNnPc1	manubrium
jsou	být	k5eAaImIp3nP	být
dlouhá	dlouhý	k2eAgNnPc1d1	dlouhé
<g/>
,	,	kIx,	,
početná	početný	k2eAgNnPc1d1	početné
chapadla	chapadlo	k1gNnPc1	chapadlo
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
zvonce	zvonec	k1gInPc4	zvonec
jsou	být	k5eAaImIp3nP	být
krátká	krátký	k2eAgNnPc1d1	krátké
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
živit	živit	k5eAaImF	živit
jako	jako	k9	jako
mikrofág	mikrofág	k1gInSc4	mikrofág
pomocí	pomocí	k7c2	pomocí
řasinkového	řasinkový	k2eAgInSc2d1	řasinkový
epitelu	epitel	k1gInSc2	epitel
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
transportuje	transportovat	k5eAaBmIp3nS	transportovat
jemné	jemný	k2eAgFnPc4d1	jemná
částečky	částečka	k1gFnPc4	částečka
potravy	potrava	k1gFnSc2	potrava
do	do	k7c2	do
rýhy	rýha	k1gFnSc2	rýha
na	na	k7c6	na
obvodu	obvod	k1gInSc6	obvod
zvonce	zvonec	k1gInSc2	zvonec
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
vychytávány	vychytáván	k2eAgFnPc1d1	vychytáván
rameny	rameno	k1gNnPc7	rameno
manubria	manubrium	k1gNnSc2	manubrium
<g/>
.	.	kIx.	.
</s>
<s>
Talířovka	Talířovka	k1gFnSc1	Talířovka
svítivá	svítivý	k2eAgFnSc1d1	svítivá
(	(	kIx(	(
<g/>
Pelagia	Pelagia	k1gFnSc1	Pelagia
noctiluca	noctiluca	k1gFnSc1	noctiluca
<g/>
)	)	kIx)	)
Má	mít	k5eAaImIp3nS	mít
kromě	kromě	k7c2	kromě
dlouhých	dlouhý	k2eAgNnPc2d1	dlouhé
ramen	rameno	k1gNnPc2	rameno
manubria	manubrium	k1gNnSc2	manubrium
také	také	k6eAd1	také
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
málo	málo	k6eAd1	málo
početná	početný	k2eAgNnPc1d1	početné
chapadla	chapadlo	k1gNnPc1	chapadlo
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
zvonce	zvonec	k1gInSc2	zvonec
<g/>
.	.	kIx.	.
</s>
<s>
Nevytváří	vytvářet	k5eNaImIp3nS	vytvářet
stádium	stádium	k1gNnSc4	stádium
polypa	polyp	k1gMnSc2	polyp
<g/>
.	.	kIx.	.
</s>
<s>
Tělo	tělo	k1gNnSc1	tělo
bíle	bíle	k6eAd1	bíle
světélkuje	světélkovat	k5eAaImIp3nS	světélkovat
díky	díky	k7c3	díky
symbiotickým	symbiotický	k2eAgFnPc3d1	symbiotická
řasám	řasa	k1gFnPc3	řasa
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
teplých	teplý	k2eAgNnPc6d1	teplé
mořích	moře	k1gNnPc6	moře
do	do	k7c2	do
50	[number]	k4	50
<g/>
°	°	k?	°
z.	z.	k?	z.
š.	š.	k?	š.
<g/>
,	,	kIx,	,
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
místě	místo	k1gNnSc6	místo
se	se	k3xPyFc4	se
většinou	většina	k1gFnSc7	většina
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
více	hodně	k6eAd2	hodně
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Talířovka	Talířovka	k1gFnSc1	Talířovka
obrovská	obrovský	k2eAgFnSc1d1	obrovská
(	(	kIx(	(
<g/>
Cyanea	Cyane	k2eAgNnPc1d1	Cyane
capillata	capille	k1gNnPc1	capille
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
<g/>
.	.	kIx.	.
</s>
<s>
C.	C.	kA	C.
arctica	arctica	k1gFnSc1	arctica
<g/>
)	)	kIx)	)
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
největší	veliký	k2eAgFnSc1d3	veliký
medúza	medúza	k1gFnSc1	medúza
na	na	k7c6	na
světě	svět	k1gInSc6	svět
(	(	kIx(	(
<g/>
a	a	k8xC	a
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
také	také	k9	také
nejdelší	dlouhý	k2eAgMnSc1d3	nejdelší
živočich	živočich	k1gMnSc1	živočich
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
velmi	velmi	k6eAd1	velmi
početná	početný	k2eAgNnPc4d1	početné
chapadla	chapadlo	k1gNnPc4	chapadlo
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
až	až	k9	až
40	[number]	k4	40
m	m	kA	m
a	a	k8xC	a
zvon	zvon	k1gInSc4	zvon
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
až	až	k9	až
2	[number]	k4	2
m.	m.	k?	m.
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
severních	severní	k2eAgNnPc6d1	severní
chladných	chladný	k2eAgNnPc6d1	chladné
mořích	moře	k1gNnPc6	moře
<g/>
.	.	kIx.	.
řád	řád	k1gInSc1	řád
<g/>
:	:	kIx,	:
Kořenoústky	Kořenoústek	k1gInPc1	Kořenoústek
(	(	kIx(	(
<g/>
Rhizostomea	Rhizostomea	k1gFnSc1	Rhizostomea
<g/>
)	)	kIx)	)
Medúzy	Medúza	k1gFnPc1	Medúza
nemají	mít	k5eNaImIp3nP	mít
chapadla	chapadlo	k1gNnPc1	chapadlo
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
složitou	složitý	k2eAgFnSc7d1	složitá
stavbou	stavba	k1gFnSc7	stavba
velkého	velký	k2eAgNnSc2d1	velké
manubria	manubrium	k1gNnSc2	manubrium
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
původní	původní	k2eAgInSc1d1	původní
přijímací	přijímací	k2eAgInSc1d1	přijímací
otvor	otvor	k1gInSc1	otvor
je	být	k5eAaImIp3nS	být
zarostlý	zarostlý	k2eAgInSc1d1	zarostlý
<g/>
,	,	kIx,	,
sekundárně	sekundárně	k6eAd1	sekundárně
je	být	k5eAaImIp3nS	být
vytvořena	vytvořen	k2eAgFnSc1d1	vytvořena
hustá	hustý	k2eAgFnSc1d1	hustá
síť	síť	k1gFnSc1	síť
kanálků	kanálek	k1gInPc2	kanálek
zakončená	zakončený	k2eAgFnSc1d1	zakončená
savými	savý	k2eAgFnPc7d1	savá
rourkami	rourka	k1gFnPc7	rourka
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
manubria	manubrium	k1gNnSc2	manubrium
<g/>
.	.	kIx.	.
</s>
<s>
Trávící	trávící	k2eAgInPc1d1	trávící
enzymy	enzym	k1gInPc1	enzym
jsou	být	k5eAaImIp3nP	být
tímto	tento	k3xDgInSc7	tento
systémem	systém	k1gInSc7	systém
dopravovány	dopravován	k2eAgInPc4d1	dopravován
až	až	k9	až
na	na	k7c4	na
manubriem	manubrium	k1gNnSc7	manubrium
zachycenou	zachycený	k2eAgFnSc7d1	zachycená
potravu	potrava	k1gFnSc4	potrava
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
mimotělně	mimotělně	k6eAd1	mimotělně
trávena	tráven	k2eAgFnSc1d1	trávena
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
druhů	druh	k1gInPc2	druh
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nS	živit
jako	jako	k9	jako
mikrofágové	mikrofágový	k2eAgNnSc1d1	mikrofágový
<g/>
.	.	kIx.	.
</s>
<s>
Kořenoústka	Kořenoústka	k1gFnSc1	Kořenoústka
plicnatá	plicnatý	k2eAgFnSc1d1	plicnatá
(	(	kIx(	(
<g/>
Rhizostoma	Rhizostoma	k1gFnSc1	Rhizostoma
pulmo	pulmo	k6eAd1	pulmo
<g/>
)	)	kIx)	)
Průměr	průměr	k1gInSc1	průměr
zvonu	zvon	k1gInSc2	zvon
je	být	k5eAaImIp3nS	být
ca	ca	kA	ca
20	[number]	k4	20
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Pomalé	pomalý	k2eAgNnSc4d1	pomalé
stahování	stahování	k1gNnSc4	stahování
těla	tělo	k1gNnSc2	tělo
při	při	k7c6	při
plavání	plavání	k1gNnSc6	plavání
připomíná	připomínat	k5eAaImIp3nS	připomínat
dýchací	dýchací	k2eAgInPc4d1	dýchací
pohyby	pohyb	k1gInPc4	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
druhem	druh	k1gInSc7	druh
evropských	evropský	k2eAgNnPc2d1	Evropské
moří	moře	k1gNnPc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Kořenoústka	Kořenoústka	k1gFnSc1	Kořenoústka
hrbolatá	hrbolatý	k2eAgFnSc1d1	hrbolatá
(	(	kIx(	(
<g/>
Cotylorhiza	Cotylorhiza	k1gFnSc1	Cotylorhiza
tuberculata	tubercule	k1gNnPc1	tubercule
<g/>
)	)	kIx)	)
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
ochranu	ochrana	k1gFnSc4	ochrana
malým	malý	k2eAgFnPc3d1	malá
treskám	treska	k1gFnPc3	treska
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
