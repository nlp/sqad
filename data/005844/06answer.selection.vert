<s>
V	v	k7c6	v
životním	životní	k2eAgInSc6d1	životní
cyklu	cyklus	k1gInSc6	cyklus
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
stadium	stadium	k1gNnSc1	stadium
"	"	kIx"	"
<g/>
medúza	medúza	k1gFnSc1	medúza
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
volně	volně	k6eAd1	volně
plovoucí	plovoucí	k2eAgFnSc1d1	plovoucí
forma	forma	k1gFnSc1	forma
se	s	k7c7	s
zvonem	zvon	k1gInSc7	zvon
a	a	k8xC	a
žahavými	žahavý	k2eAgNnPc7d1	žahavé
rameny	rameno	k1gNnPc7	rameno
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
v	v	k7c6	v
případě	případ	k1gInSc6	případ
medúzovců	medúzovec	k1gInPc2	medúzovec
chybí	chybit	k5eAaPmIp3nS	chybit
plachetka	plachetka	k1gFnSc1	plachetka
(	(	kIx(	(
<g/>
velum	velum	k1gInSc1	velum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
