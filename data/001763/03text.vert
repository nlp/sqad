<s>
Obraz	obraz	k1gInSc1	obraz
Doriana	Dorian	k1gMnSc2	Dorian
Graye	Gray	k1gMnSc2	Gray
(	(	kIx(	(
<g/>
The	The	k1gMnSc5	The
Picture	Pictur	k1gMnSc5	Pictur
of	of	k?	of
Dorian	Dorian	k1gInSc4	Dorian
Gray	Graa	k1gFnSc2	Graa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
román	román	k1gInSc4	román
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
napsal	napsat	k5eAaPmAgInS	napsat
britský	britský	k2eAgMnSc1d1	britský
literát	literát	k1gMnSc1	literát
Oscar	Oscar	k1gMnSc1	Oscar
Wilde	Wild	k1gInSc5	Wild
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
byl	být	k5eAaImAgInS	být
publikován	publikovat	k5eAaBmNgInS	publikovat
jako	jako	k8xS	jako
úvodní	úvodní	k2eAgInSc1d1	úvodní
příběh	příběh	k1gInSc1	příběh
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Lippincott	Lippincotta	k1gFnPc2	Lippincotta
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Monthly	Monthly	k1gFnSc7	Monthly
Magazine	Magazin	k1gInSc5	Magazin
dne	den	k1gInSc2	den
20	[number]	k4	20
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1890	[number]	k4	1890
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
jej	on	k3xPp3gMnSc4	on
Wilde	Wild	k1gInSc5	Wild
přepsal	přepsat	k5eAaPmAgInS	přepsat
<g/>
,	,	kIx,	,
několikrát	několikrát	k6eAd1	několikrát
pozměnil	pozměnit	k5eAaPmAgMnS	pozměnit
a	a	k8xC	a
přidal	přidat	k5eAaPmAgMnS	přidat
několik	několik	k4yIc4	několik
kapitol	kapitola	k1gFnPc2	kapitola
<g/>
.	.	kIx.	.
</s>
<s>
Novou	nový	k2eAgFnSc4d1	nová
verzi	verze	k1gFnSc4	verze
poté	poté	k6eAd1	poté
vydalo	vydat	k5eAaPmAgNnS	vydat
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
vydavatelství	vydavatelství	k1gNnSc1	vydavatelství
"	"	kIx"	"
<g/>
Ward	Ward	k1gMnSc1	Ward
<g/>
,	,	kIx,	,
Lock	Lock	k1gMnSc1	Lock
<g/>
,	,	kIx,	,
and	and	k?	and
Co	co	k9	co
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Jiří	Jiří	k1gMnSc1	Jiří
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Novák	Novák	k1gMnSc1	Novák
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc4	román
v	v	k7c6	v
sobě	sebe	k3xPyFc6	sebe
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
hororové	hororový	k2eAgInPc4d1	hororový
prvky	prvek	k1gInPc4	prvek
(	(	kIx(	(
<g/>
s	s	k7c7	s
gotickým	gotický	k2eAgInSc7d1	gotický
nádechem	nádech	k1gInSc7	nádech
<g/>
)	)	kIx)	)
a	a	k8xC	a
faustovské	faustovský	k2eAgNnSc4d1	Faustovské
téma	téma	k1gNnSc4	téma
<g/>
.	.	kIx.	.
</s>
<s>
Zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
umělecké	umělecký	k2eAgFnPc4d1	umělecká
pohnutky	pohnutka	k1gFnPc4	pohnutka
dekadence	dekadence	k1gFnSc2	dekadence
i	i	k8xC	i
homosexuality	homosexualita	k1gFnSc2	homosexualita
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
byla	být	k5eAaImAgFnS	být
kniha	kniha	k1gFnSc1	kniha
po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
vydání	vydání	k1gNnSc6	vydání
mnohými	mnohý	k2eAgMnPc7d1	mnohý
odsuzována	odsuzován	k2eAgNnPc1d1	odsuzováno
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
pilířů	pilíř	k1gInPc2	pilíř
moderní	moderní	k2eAgFnSc2d1	moderní
literatury	literatura	k1gFnSc2	literatura
<g/>
,	,	kIx,	,
je	on	k3xPp3gFnPc4	on
jí	jíst	k5eAaImIp3nS	jíst
ovlivněno	ovlivnit	k5eAaPmNgNnS	ovlivnit
mnoho	mnoho	k6eAd1	mnoho
dalších	další	k2eAgNnPc2d1	další
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
byla	být	k5eAaImAgFnS	být
zfilmována	zfilmovat	k5eAaPmNgFnS	zfilmovat
americkým	americký	k2eAgMnSc7d1	americký
režisérem	režisér	k1gMnSc7	režisér
Davidem	David	k1gMnSc7	David
Rosenbaumem	Rosenbaum	k1gInSc7	Rosenbaum
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
a	a	k8xC	a
britským	britský	k2eAgMnSc7d1	britský
režisérem	režisér	k1gMnSc7	režisér
Oliverem	Oliver	k1gMnSc7	Oliver
Parkerem	Parker	k1gMnSc7	Parker
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
knihy	kniha	k1gFnSc2	kniha
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
původní	původní	k2eAgInSc1d1	původní
český	český	k2eAgInSc1d1	český
muzikál	muzikál	k1gInSc1	muzikál
Obraz	obraz	k1gInSc1	obraz
Doriana	Dorian	k1gMnSc2	Dorian
Graye	Gray	k1gMnSc2	Gray
<g/>
.	.	kIx.	.
</s>
<s>
Dorian	Dorian	k1gInSc1	Dorian
Gray	Graa	k1gFnSc2	Graa
-	-	kIx~	-
pohledný	pohledný	k2eAgMnSc1d1	pohledný
mladý	mladý	k2eAgMnSc1d1	mladý
aristokrat	aristokrat	k1gMnSc1	aristokrat
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	s	k7c7	s
vlivem	vliv	k1gInSc7	vliv
lorda	lord	k1gMnSc2	lord
Henryho	Henry	k1gMnSc2	Henry
mění	měnit	k5eAaImIp3nS	měnit
k	k	k7c3	k
špatnému	špatný	k2eAgNnSc3d1	špatné
lord	lord	k1gMnSc1	lord
Henry	henry	k1gInSc2	henry
Wotton	Wotton	k1gInSc1	Wotton
-	-	kIx~	-
dekadentní	dekadentní	k2eAgMnSc1d1	dekadentní
aristokrat	aristokrat	k1gMnSc1	aristokrat
s	s	k7c7	s
pokřiveným	pokřivený	k2eAgInSc7d1	pokřivený
světonázorem	světonázor	k1gInSc7	světonázor
<g/>
,	,	kIx,	,
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
je	být	k5eAaImIp3nS	být
však	však	k9	však
oblíbenou	oblíbený	k2eAgFnSc7d1	oblíbená
hvězdou	hvězda	k1gFnSc7	hvězda
Basil	Basil	k1gMnSc1	Basil
Hallward	Hallward	k1gMnSc1	Hallward
-	-	kIx~	-
utopicky	utopicky	k6eAd1	utopicky
smýšlející	smýšlející	k2eAgMnSc1d1	smýšlející
malíř	malíř	k1gMnSc1	malíř
věřící	věřící	k1gMnSc1	věřící
v	v	k7c4	v
dobro	dobro	k1gNnSc4	dobro
<g/>
,	,	kIx,	,
okouzlený	okouzlený	k2eAgInSc1d1	okouzlený
Dorianem	Dorian	k1gInSc7	Dorian
Sibyla	Sibyla	k1gFnSc1	Sibyla
Vaneová	Vaneová	k1gFnSc1	Vaneová
-	-	kIx~	-
mladičká	mladičký	k2eAgFnSc1d1	mladičká
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
nadaná	nadaný	k2eAgFnSc1d1	nadaná
herečka	herečka	k1gFnSc1	herečka
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
naivní	naivní	k2eAgFnSc1d1	naivní
a	a	k8xC	a
plná	plný	k2eAgFnSc1d1	plná
romantických	romantický	k2eAgInPc2d1	romantický
ideálů	ideál	k1gInPc2	ideál
James	James	k1gMnSc1	James
Vane	vanout	k5eAaImIp3nS	vanout
-	-	kIx~	-
bratr	bratr	k1gMnSc1	bratr
Sibyly	Sibyla	k1gFnSc2	Sibyla
<g/>
,	,	kIx,	,
lehce	lehko	k6eAd1	lehko
neotesaný	otesaný	k2eNgMnSc1d1	neotesaný
dobrodruh	dobrodruh	k1gMnSc1	dobrodruh
<g/>
,	,	kIx,	,
námořník	námořník	k1gMnSc1	námořník
pan	pan	k1gMnSc1	pan
Issacs	Issacs	k1gInSc1	Issacs
-	-	kIx~	-
ředitel	ředitel	k1gMnSc1	ředitel
druhořadého	druhořadý	k2eAgNnSc2d1	druhořadé
divadla	divadlo	k1gNnSc2	divadlo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hraje	hrát	k5eAaImIp3nS	hrát
Sibyla	Sibyla	k1gFnSc1	Sibyla
<g/>
;	;	kIx,	;
kdysi	kdysi	k6eAd1	kdysi
finančně	finančně	k6eAd1	finančně
pomohl	pomoct	k5eAaPmAgMnS	pomoct
rodině	rodina	k1gFnSc3	rodina
Vaneových	Vaneův	k2eAgInPc2d1	Vaneův
a	a	k8xC	a
žádá	žádat	k5eAaImIp3nS	žádat
si	se	k3xPyFc3	se
za	za	k7c4	za
to	ten	k3xDgNnSc1	ten
Sibylu	Sibyla	k1gFnSc4	Sibyla
jako	jako	k8xC	jako
herečku	herečka	k1gFnSc4	herečka
Alan	Alan	k1gMnSc1	Alan
Campbell	Campbell	k1gMnSc1	Campbell
-	-	kIx~	-
bývalý	bývalý	k2eAgMnSc1d1	bývalý
Dorianův	Dorianův	k2eAgMnSc1d1	Dorianův
nerozlučný	rozlučný	k2eNgMnSc1d1	nerozlučný
přítel	přítel	k1gMnSc1	přítel
<g/>
,	,	kIx,	,
výborný	výborný	k2eAgMnSc1d1	výborný
chemik	chemik	k1gMnSc1	chemik
<g/>
,	,	kIx,	,
biolog	biolog	k1gMnSc1	biolog
a	a	k8xC	a
hudebník	hudebník	k1gMnSc1	hudebník
sir	sir	k1gMnSc1	sir
Geoffrey	Geoffrea	k1gFnSc2	Geoffrea
Clouston	Clouston	k1gInSc1	Clouston
Příběh	příběh	k1gInSc1	příběh
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
mladém	mladý	k2eAgMnSc6d1	mladý
Dorianu	Dorian	k1gMnSc6	Dorian
Grayovi	Graya	k1gMnSc6	Graya
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
svou	svůj	k3xOyFgFnSc7	svůj
krásou	krása	k1gFnSc7	krása
učaruje	učarovat	k5eAaPmIp3nS	učarovat
malíři	malíř	k1gMnSc3	malíř
Basilu	Basil	k1gMnSc3	Basil
Hallwardovi	Hallward	k1gMnSc3	Hallward
<g/>
.	.	kIx.	.
</s>
<s>
Stává	stávat	k5eAaImIp3nS	stávat
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gInSc7	jeho
modelem	model	k1gInSc7	model
a	a	k8xC	a
múzou	múza	k1gFnSc7	múza
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jednou	k6eAd1	jednou
<g/>
,	,	kIx,	,
když	když	k8xS	když
Dorian	Dorian	k1gMnSc1	Dorian
stojí	stát	k5eAaImIp3nS	stát
opět	opět	k5eAaPmF	opět
Basilovi	Basil	k1gMnSc3	Basil
modelem	model	k1gInSc7	model
<g/>
,	,	kIx,	,
dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
do	do	k7c2	do
řeči	řeč	k1gFnSc2	řeč
s	s	k7c7	s
malířovým	malířův	k2eAgMnSc7d1	malířův
přítelem	přítel	k1gMnSc7	přítel
<g/>
,	,	kIx,	,
lordem	lord	k1gMnSc7	lord
Henrym	Henry	k1gMnSc7	Henry
<g/>
,	,	kIx,	,
a	a	k8xC	a
nechá	nechat	k5eAaPmIp3nS	nechat
se	se	k3xPyFc4	se
oslnit	oslnit	k5eAaPmF	oslnit
jeho	jeho	k3xOp3gInPc4	jeho
názory	názor	k1gInPc4	názor
o	o	k7c4	o
fungování	fungování	k1gNnSc4	fungování
světa	svět	k1gInSc2	svět
-	-	kIx~	-
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
hýbe	hýbat	k5eAaImIp3nS	hýbat
světem	svět	k1gInSc7	svět
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
prý	prý	k9	prý
krása	krása	k1gFnSc1	krása
a	a	k8xC	a
mládí	mládí	k1gNnSc1	mládí
<g/>
.	.	kIx.	.
</s>
<s>
Dorian	Dorian	k1gMnSc1	Dorian
si	se	k3xPyFc3	se
uvědomí	uvědomit	k5eAaPmIp3nS	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
portrét	portrét	k1gInSc1	portrét
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
mu	on	k3xPp3gMnSc3	on
maluje	malovat	k5eAaImIp3nS	malovat
Basil	Basil	k1gInSc4	Basil
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
za	za	k7c4	za
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
nemilosrdně	milosrdně	k6eNd1	milosrdně
připomínat	připomínat	k5eAaImF	připomínat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
byl	být	k5eAaImAgInS	být
krásný	krásný	k2eAgInSc1d1	krásný
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
již	již	k6eAd1	již
bude	být	k5eAaImBp3nS	být
zchátralý	zchátralý	k2eAgInSc1d1	zchátralý
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
vysloví	vyslovit	k5eAaPmIp3nS	vyslovit
přání	přání	k1gNnSc4	přání
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
známky	známka	k1gFnPc4	známka
úpadku	úpadek	k1gInSc2	úpadek
nenesl	nést	k5eNaImAgMnS	nést
on	on	k3xPp3gMnSc1	on
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ten	ten	k3xDgInSc4	ten
portrét	portrét	k1gInSc4	portrét
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
přání	přání	k1gNnSc1	přání
je	být	k5eAaImIp3nS	být
naplněno	naplnit	k5eAaPmNgNnS	naplnit
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
portrétu	portrét	k1gInSc2	portrét
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
zrcadlo	zrcadlo	k1gNnSc1	zrcadlo
Dorianovy	Dorianův	k2eAgFnSc2d1	Dorianova
duše	duše	k1gFnSc2	duše
<g/>
,	,	kIx,	,
obraz	obraz	k1gInSc1	obraz
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
a	a	k8xC	a
odráží	odrážet	k5eAaImIp3nS	odrážet
hříchy	hřích	k1gInPc4	hřích
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
Dorian	Dorian	k1gMnSc1	Dorian
Gray	Graa	k1gFnSc2	Graa
páchá	páchat	k5eAaImIp3nS	páchat
<g/>
.	.	kIx.	.
</s>
<s>
Dorianova	Dorianův	k2eAgFnSc1d1	Dorianova
tvář	tvář	k1gFnSc1	tvář
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
beze	beze	k7c2	beze
změny	změna	k1gFnSc2	změna
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
mladá	mladý	k2eAgFnSc1d1	mladá
a	a	k8xC	a
krásná	krásný	k2eAgFnSc1d1	krásná
<g/>
...	...	k?	...
Děj	děj	k1gInSc1	děj
tohoto	tento	k3xDgNnSc2	tento
díla	dílo	k1gNnSc2	dílo
začíná	začínat	k5eAaImIp3nS	začínat
v	v	k7c6	v
rozkvetlé	rozkvetlý	k2eAgFnSc6d1	rozkvetlá
zahradě	zahrada	k1gFnSc6	zahrada
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
sešli	sejít	k5eAaPmAgMnP	sejít
dva	dva	k4xCgMnPc1	dva
přátelé	přítel	k1gMnPc1	přítel
z	z	k7c2	z
dobrých	dobrý	k2eAgInPc2d1	dobrý
kruhů	kruh	k1gInPc2	kruh
<g/>
.	.	kIx.	.
</s>
<s>
Malíř	malíř	k1gMnSc1	malíř
plný	plný	k2eAgInSc4d1	plný
ideálů	ideál	k1gInPc2	ideál
a	a	k8xC	a
ryzí	ryzí	k2eAgMnSc1d1	ryzí
muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
Basil	Basil	k1gMnSc1	Basil
Hallward	Hallward	k1gMnSc1	Hallward
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
maluje	malovat	k5eAaImIp3nS	malovat
portrét	portrét	k1gInSc4	portrét
<g/>
.	.	kIx.	.
</s>
<s>
Cynický	cynický	k2eAgMnSc1d1	cynický
lord	lord	k1gMnSc1	lord
Henry	Henry	k1gMnSc1	Henry
<g/>
,	,	kIx,	,
povahově	povahově	k6eAd1	povahově
naprostý	naprostý	k2eAgInSc4d1	naprostý
opak	opak	k1gInSc4	opak
malíře	malíř	k1gMnSc2	malíř
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
portrét	portrét	k1gInSc1	portrét
označí	označit	k5eAaPmIp3nS	označit
za	za	k7c4	za
nejlepší	dobrý	k2eAgNnSc4d3	nejlepší
Basilovo	Basilův	k2eAgNnSc4d1	Basilův
dílo	dílo	k1gNnSc4	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
však	však	k9	však
navrhne	navrhnout	k5eAaPmIp3nS	navrhnout
malíři	malíř	k1gMnSc3	malíř
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gInSc1	jeho
obraz	obraz	k1gInSc1	obraz
pošle	poslat	k5eAaPmIp3nS	poslat
na	na	k7c4	na
prestižní	prestižní	k2eAgFnSc4d1	prestižní
výstavu	výstava	k1gFnSc4	výstava
jako	jako	k8xC	jako
hlavní	hlavní	k2eAgInSc1d1	hlavní
exponát	exponát	k1gInSc1	exponát
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
odmítne	odmítnout	k5eAaPmIp3nS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Prý	prý	k9	prý
do	do	k7c2	do
něj	on	k3xPp3gInSc2	on
vložil	vložit	k5eAaPmAgMnS	vložit
příliš	příliš	k6eAd1	příliš
mnoho	mnoho	k6eAd1	mnoho
sebe	sebe	k3xPyFc4	sebe
sama	sám	k3xTgMnSc4	sám
<g/>
.	.	kIx.	.
</s>
<s>
Obrazem	obraz	k1gInSc7	obraz
totiž	totiž	k9	totiž
vzdává	vzdávat	k5eAaImIp3nS	vzdávat
přílišný	přílišný	k2eAgInSc4d1	přílišný
obdiv	obdiv	k1gInSc4	obdiv
nezletilému	zletilý	k2eNgMnSc3d1	nezletilý
a	a	k8xC	a
krásnému	krásný	k2eAgMnSc3d1	krásný
mládenci	mládenec	k1gMnSc3	mládenec
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
nedávno	nedávno	k6eAd1	nedávno
poznal	poznat	k5eAaPmAgMnS	poznat
-	-	kIx~	-
Dorianu	Dorian	k1gMnSc3	Dorian
Grayovi	Graya	k1gMnSc3	Graya
<g/>
.	.	kIx.	.
</s>
<s>
Dorian	Dorian	k1gInSc1	Dorian
-	-	kIx~	-
mladý	mladý	k2eAgMnSc1d1	mladý
aristokrat	aristokrat	k1gMnSc1	aristokrat
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
umělce	umělec	k1gMnPc4	umělec
jakousi	jakýsi	k3yIgFnSc7	jakýsi
múzou	múza	k1gFnSc7	múza
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
svěžest	svěžest	k1gFnSc1	svěžest
a	a	k8xC	a
krása	krása	k1gFnSc1	krása
těla	tělo	k1gNnSc2	tělo
i	i	k8xC	i
ducha	duch	k1gMnSc2	duch
se	se	k3xPyFc4	se
promítá	promítat	k5eAaImIp3nS	promítat
do	do	k7c2	do
celé	celý	k2eAgFnSc2d1	celá
malířovy	malířův	k2eAgFnSc2d1	malířova
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Lord	lord	k1gMnSc1	lord
Henry	Henry	k1gMnSc1	Henry
se	se	k3xPyFc4	se
malířovým	malířův	k2eAgFnPc3d1	malířova
řečím	řečit	k5eAaImIp1nS	řečit
vysměje	vysmát	k5eAaPmIp3nS	vysmát
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
zahrady	zahrada	k1gFnSc2	zahrada
přichází	přicházet	k5eAaImIp3nS	přicházet
sluha	sluha	k1gMnSc1	sluha
a	a	k8xC	a
oznamuje	oznamovat	k5eAaImIp3nS	oznamovat
příchod	příchod	k1gInSc1	příchod
Doriana	Dorian	k1gMnSc2	Dorian
Graye	Gray	k1gMnSc2	Gray
<g/>
.	.	kIx.	.
</s>
<s>
Basil	Basil	k1gInSc1	Basil
Doriana	Doriana	k1gFnSc1	Doriana
představí	představit	k5eAaPmIp3nS	představit
svému	svůj	k1gMnSc3	svůj
příteli	přítel	k1gMnSc3	přítel
a	a	k8xC	a
odcházejí	odcházet	k5eAaImIp3nP	odcházet
spolu	spolu	k6eAd1	spolu
do	do	k7c2	do
ateliérů	ateliér	k1gInPc2	ateliér
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
má	mít	k5eAaImIp3nS	mít
Dorian	Dorian	k1gInSc4	Dorian
stát	stát	k5eAaImF	stát
modelem	model	k1gInSc7	model
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
malby	malba	k1gFnSc2	malba
si	se	k3xPyFc3	se
všichni	všechen	k3xTgMnPc1	všechen
povídají	povídat	k5eAaImIp3nP	povídat
<g/>
.	.	kIx.	.
</s>
<s>
Doriana	Doriana	k1gFnSc1	Doriana
očaruje	očarovat	k5eAaPmIp3nS	očarovat
Henryho	Henry	k1gMnSc4	Henry
nemilosrdný	milosrdný	k2eNgInSc1d1	nemilosrdný
přístup	přístup	k1gInSc1	přístup
k	k	k7c3	k
životu	život	k1gInSc3	život
a	a	k8xC	a
svéráznost	svéráznost	k1gFnSc4	svéráznost
některých	některý	k3yIgInPc2	některý
jeho	jeho	k3xOp3gInPc2	jeho
názorů	názor	k1gInPc2	názor
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Léčení	léčení	k1gNnSc1	léčení
duše	duše	k1gFnSc2	duše
skrze	skrze	k?	skrze
smysly	smysl	k1gInPc1	smysl
<g/>
,	,	kIx,	,
léčení	léčení	k1gNnSc1	léčení
smyslů	smysl	k1gInPc2	smysl
skrze	skrze	k?	skrze
duši	duše	k1gFnSc3	duše
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
na	na	k7c4	na
okamžik	okamžik	k1gInSc4	okamžik
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
jdou	jít	k5eAaImIp3nP	jít
projít	projít	k5eAaPmF	projít
do	do	k7c2	do
zahrady	zahrada	k1gFnSc2	zahrada
a	a	k8xC	a
stanou	stanout	k5eAaPmIp3nP	stanout
se	se	k3xPyFc4	se
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
přátelé	přítel	k1gMnPc1	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Henry	Henry	k1gMnSc1	Henry
lichotí	lichotit	k5eAaImIp3nS	lichotit
Dorianovi	Dorian	k1gMnSc3	Dorian
<g/>
,	,	kIx,	,
vyzdvihuje	vyzdvihovat	k5eAaImIp3nS	vyzdvihovat
jeho	jeho	k3xOp3gFnSc4	jeho
krásu	krása	k1gFnSc4	krása
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
mu	on	k3xPp3gMnSc3	on
vystavuje	vystavovat	k5eAaImIp3nS	vystavovat
její	její	k3xOp3gFnSc4	její
pomíjivost	pomíjivost	k1gFnSc4	pomíjivost
<g/>
.	.	kIx.	.
</s>
<s>
Tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
právě	právě	k9	právě
pro	pro	k7c4	pro
pomíjivost	pomíjivost	k1gFnSc4	pomíjivost
mládí	mládí	k1gNnSc2	mládí
a	a	k8xC	a
krásy	krása	k1gFnPc1	krása
by	by	kYmCp3nP	by
si	se	k3xPyFc3	se
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
je	být	k5eAaImIp3nS	být
mlád	mlád	k2eAgMnSc1d1	mlád
<g/>
,	,	kIx,	,
neměl	mít	k5eNaImAgMnS	mít
cokoli	cokoli	k3yInSc4	cokoli
odpírat	odpírat	k5eAaImF	odpírat
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
oba	dva	k4xCgInPc1	dva
vrátí	vrátit	k5eAaPmIp3nP	vrátit
do	do	k7c2	do
ateliéru	ateliér	k1gInSc2	ateliér
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mohli	moct	k5eAaImAgMnP	moct
pokračovat	pokračovat	k5eAaImF	pokračovat
v	v	k7c6	v
malbě	malba	k1gFnSc6	malba
<g/>
,	,	kIx,	,
Dorian	Dorian	k1gMnSc1	Dorian
vysloví	vyslovit	k5eAaPmIp3nS	vyslovit
přání	přání	k1gNnSc4	přání
-	-	kIx~	-
chce	chtít	k5eAaImIp3nS	chtít
aby	aby	k9	aby
všechny	všechen	k3xTgInPc4	všechen
znaky	znak	k1gInPc4	znak
stárnutí	stárnutí	k1gNnSc2	stárnutí
a	a	k8xC	a
úpadku	úpadek	k1gInSc2	úpadek
nenesl	nést	k5eNaImAgMnS	nést
on	on	k3xPp3gMnSc1	on
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gInSc4	jeho
portrét	portrét	k1gInSc4	portrét
<g/>
.	.	kIx.	.
</s>
<s>
Basil	Basil	k1gMnSc1	Basil
vidí	vidět	k5eAaImIp3nS	vidět
<g/>
,	,	kIx,	,
že	že	k8xS	že
Henryho	Henry	k1gMnSc2	Henry
zvrácená	zvrácený	k2eAgFnSc1d1	zvrácená
morálka	morálka	k1gFnSc1	morálka
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
na	na	k7c4	na
Doriana	Dorian	k1gMnSc4	Dorian
zničující	zničující	k2eAgInSc1d1	zničující
vliv	vliv	k1gInSc1	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Jakékoli	jakýkoli	k3yIgFnPc4	jakýkoli
jeho	jeho	k3xOp3gFnPc4	jeho
rady	rada	k1gFnPc4	rada
nebrat	brat	k5eNaPmF	brat
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
lord	lord	k1gMnSc1	lord
říká	říkat	k5eAaImIp3nS	říkat
vážně	vážně	k6eAd1	vážně
však	však	k9	však
Dorian	Dorian	k1gMnSc1	Dorian
přehlíží	přehlížet	k5eAaImIp3nS	přehlížet
<g/>
.	.	kIx.	.
</s>
<s>
Zakázané	zakázaný	k2eAgNnSc1d1	zakázané
ovoce	ovoce	k1gNnSc1	ovoce
nejlépe	dobře	k6eAd3	dobře
chutná	chutnat	k5eAaImIp3nS	chutnat
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
oba	dva	k4xCgMnPc1	dva
dva	dva	k4xCgMnPc1	dva
vídají	vídat	k5eAaImIp3nP	vídat
a	a	k8xC	a
Basil	Basil	k1gInSc4	Basil
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
Doriana	Dorian	k1gMnSc2	Dorian
téměř	téměř	k6eAd1	téměř
zamilován	zamilován	k2eAgInSc1d1	zamilován
<g/>
,	,	kIx,	,
ustupuje	ustupovat	k5eAaImIp3nS	ustupovat
do	do	k7c2	do
pozadí	pozadí	k1gNnSc2	pozadí
<g/>
.	.	kIx.	.
</s>
<s>
Jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
čeká	čekat	k5eAaImIp3nS	čekat
na	na	k7c4	na
lorda	lord	k1gMnSc4	lord
Henryho	Henry	k1gMnSc4	Henry
a	a	k8xC	a
na	na	k7c4	na
malíře	malíř	k1gMnSc4	malíř
Basila	Basil	k1gMnSc2	Basil
překvapení	překvapení	k1gNnSc2	překvapení
<g/>
.	.	kIx.	.
</s>
<s>
Dorian	Dorian	k1gMnSc1	Dorian
si	se	k3xPyFc3	se
naplánoval	naplánovat	k5eAaBmAgMnS	naplánovat
svatbu	svatba	k1gFnSc4	svatba
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
chotí	choť	k1gFnSc7	choť
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
stát	stát	k5eAaPmF	stát
mladičká	mladičký	k2eAgFnSc1d1	mladičká
herečka	herečka	k1gFnSc1	herečka
druhořadého	druhořadý	k2eAgNnSc2d1	druhořadé
divadla	divadlo	k1gNnSc2	divadlo
<g/>
,	,	kIx,	,
vedeného	vedený	k2eAgMnSc2d1	vedený
neotesaným	otesaný	k2eNgMnSc7d1	neotesaný
Židem	Žid	k1gMnSc7	Žid
<g/>
,	,	kIx,	,
panem	pan	k1gMnSc7	pan
Issacsem	Issacs	k1gMnSc7	Issacs
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
jméno	jméno	k1gNnSc1	jméno
je	být	k5eAaImIp3nS	být
Sibyla	Sibyla	k1gFnSc1	Sibyla
Vaneová	Vaneová	k1gFnSc1	Vaneová
<g/>
.	.	kIx.	.
</s>
<s>
Doriana	Doriana	k1gFnSc1	Doriana
však	však	k9	však
spíše	spíše	k9	spíše
uchvátila	uchvátit	k5eAaPmAgFnS	uchvátit
pro	pro	k7c4	pro
role	role	k1gFnPc4	role
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
představovala	představovat	k5eAaImAgFnS	představovat
v	v	k7c6	v
divadelních	divadelní	k2eAgFnPc6d1	divadelní
hrách	hra	k1gFnPc6	hra
<g/>
,	,	kIx,	,
než	než	k8xS	než
pro	pro	k7c4	pro
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
kým	kdo	k3yInSc7	kdo
byla	být	k5eAaImAgFnS	být
doopravdy	doopravdy	k9	doopravdy
<g/>
.	.	kIx.	.
</s>
<s>
Sibyla	Sibyla	k1gFnSc1	Sibyla
a	a	k8xC	a
nikdo	nikdo	k3yNnSc1	nikdo
z	z	k7c2	z
její	její	k3xOp3gFnSc2	její
rodiny	rodina	k1gFnSc2	rodina
před	před	k7c7	před
svatbou	svatba	k1gFnSc7	svatba
neznal	znát	k5eNaImAgMnS	znát
ženichovo	ženichův	k2eAgNnSc1d1	ženichovo
jméno	jméno	k1gNnSc1	jméno
(	(	kIx(	(
<g/>
Sibyla	Sibyla	k1gFnSc1	Sibyla
mu	on	k3xPp3gMnSc3	on
říká	říkat	k5eAaImIp3nS	říkat
prostě	prostě	k9	prostě
jen	jen	k9	jen
"	"	kIx"	"
<g/>
Princ	princ	k1gMnSc1	princ
z	z	k7c2	z
pohádky	pohádka	k1gFnSc2	pohádka
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
matka	matka	k1gFnSc1	matka
není	být	k5eNaImIp3nS	být
z	z	k7c2	z
domluvené	domluvený	k2eAgFnSc2d1	domluvená
svatby	svatba	k1gFnSc2	svatba
nadšena	nadchnout	k5eAaPmNgFnS	nadchnout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vzpomíná	vzpomínat	k5eAaImIp3nS	vzpomínat
na	na	k7c4	na
své	svůj	k3xOyFgNnSc4	svůj
mládí	mládí	k1gNnSc4	mládí
<g/>
,	,	kIx,	,
když	když	k8xS	když
byla	být	k5eAaImAgFnS	být
také	také	k9	také
zamilovaná	zamilovaný	k2eAgFnSc1d1	zamilovaná
<g/>
,	,	kIx,	,
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
Dorian	Dorian	k1gInSc1	Dorian
je	být	k5eAaImIp3nS	být
bohatý	bohatý	k2eAgInSc1d1	bohatý
<g/>
.	.	kIx.	.
</s>
<s>
Sibylin	Sibylin	k2eAgMnSc1d1	Sibylin
bratr	bratr	k1gMnSc1	bratr
<g/>
,	,	kIx,	,
James	James	k1gMnSc1	James
Vane	vanout	k5eAaImIp3nS	vanout
<g/>
,	,	kIx,	,
však	však	k9	však
tuší	tušit	k5eAaImIp3nS	tušit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jí	jíst	k5eAaImIp3nS	jíst
neznámý	známý	k2eNgMnSc1d1	neznámý
cizinec	cizinec	k1gMnSc1	cizinec
jistě	jistě	k6eAd1	jistě
ublíží	ublížit	k5eAaPmIp3nS	ublížit
a	a	k8xC	a
přísahá	přísahat	k5eAaImIp3nS	přísahat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jestli	jestli	k8xS	jestli
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stane	stanout	k5eAaPmIp3nS	stanout
<g/>
,	,	kIx,	,
zabije	zabít	k5eAaPmIp3nS	zabít
ho	on	k3xPp3gInSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Dorian	Dorian	k1gInSc1	Dorian
pozve	pozvat	k5eAaPmIp3nS	pozvat
na	na	k7c4	na
divadelní	divadelní	k2eAgNnSc4d1	divadelní
představení	představení	k1gNnSc4	představení
oba	dva	k4xCgInPc1	dva
své	svůj	k3xOyFgMnPc4	svůj
přátele	přítel	k1gMnPc4	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Sibyla	Sibyla	k1gFnSc1	Sibyla
však	však	k9	však
ten	ten	k3xDgInSc4	ten
večer	večer	k1gInSc4	večer
hraje	hrát	k5eAaImIp3nS	hrát
příšerně	příšerně	k6eAd1	příšerně
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
neznala	znát	k5eNaImAgFnS	znát
nic	nic	k3yNnSc1	nic
než	než	k8xS	než
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Teď	teď	k6eAd1	teď
je	být	k5eAaImIp3nS	být
zamilovaná	zamilovaná	k1gFnSc1	zamilovaná
doopravdy	doopravdy	k6eAd1	doopravdy
<g/>
,	,	kIx,	,
milostné	milostný	k2eAgFnPc1d1	milostná
scény	scéna	k1gFnPc1	scéna
v	v	k7c6	v
Romeovi	Romeo	k1gMnSc6	Romeo
a	a	k8xC	a
Jůlii	Jůlie	k1gFnSc6	Jůlie
hraje	hrát	k5eAaImIp3nS	hrát
neprocítěně	procítěně	k6eNd1	procítěně
<g/>
.	.	kIx.	.
</s>
<s>
Vidí	vidět	k5eAaImIp3nS	vidět
<g/>
,	,	kIx,	,
že	že	k8xS	že
Romeo	Romeo	k1gMnSc1	Romeo
je	být	k5eAaImIp3nS	být
ošklivý	ošklivý	k2eAgMnSc1d1	ošklivý
a	a	k8xC	a
tlustý	tlustý	k2eAgMnSc1d1	tlustý
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
její	její	k3xOp3gInSc1	její
skutečný	skutečný	k2eAgInSc1d1	skutečný
nastávající	nastávající	k2eAgInSc1d1	nastávající
-	-	kIx~	-
Dorian	Dorian	k1gInSc1	Dorian
Gray	Graa	k1gFnSc2	Graa
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
obdivuhodně	obdivuhodně	k6eAd1	obdivuhodně
krásný	krásný	k2eAgInSc1d1	krásný
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
diváků	divák	k1gMnPc2	divák
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
hry	hra	k1gFnSc2	hra
odchází	odcházet	k5eAaImIp3nS	odcházet
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
představení	představení	k1gNnSc6	představení
přichází	přicházet	k5eAaImIp3nS	přicházet
za	za	k7c7	za
Sibylou	Sibyla	k1gFnSc7	Sibyla
Dorian	Doriana	k1gFnPc2	Doriana
<g/>
.	.	kIx.	.
</s>
<s>
Děvče	děvče	k1gNnSc1	děvče
je	být	k5eAaImIp3nS	být
rádo	rád	k2eAgNnSc1d1	rádo
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gMnSc4	on
vidí	vidět	k5eAaImIp3nS	vidět
<g/>
,	,	kIx,	,
směje	smát	k5eAaImIp3nS	smát
se	se	k3xPyFc4	se
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
a	a	k8xC	a
přímo	přímo	k6eAd1	přímo
hýří	hýřit	k5eAaImIp3nS	hýřit
láskou	láska	k1gFnSc7	láska
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
ji	on	k3xPp3gFnSc4	on
však	však	k9	však
pohrdavě	pohrdavě	k6eAd1	pohrdavě
a	a	k8xC	a
krutě	krutě	k6eAd1	krutě
odmítne	odmítnout	k5eAaPmIp3nS	odmítnout
se	s	k7c7	s
slovy	slovo	k1gNnPc7	slovo
<g/>
,	,	kIx,	,
že	že	k8xS	že
svým	svůj	k3xOyFgInSc7	svůj
příšerným	příšerný	k2eAgInSc7d1	příšerný
výkonem	výkon	k1gInSc7	výkon
zabila	zabít	k5eAaPmAgFnS	zabít
jeho	jeho	k3xOp3gFnSc4	jeho
lásku	láska	k1gFnSc4	láska
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
přijde	přijít	k5eAaPmIp3nS	přijít
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
podívá	podívat	k5eAaImIp3nS	podívat
se	se	k3xPyFc4	se
mimoděk	mimoděk	k6eAd1	mimoděk
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
portrét	portrét	k1gInSc4	portrét
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
onehda	onehda	k?	onehda
namaloval	namalovat	k5eAaPmAgMnS	namalovat
Basil	Basil	k1gMnSc1	Basil
<g/>
.	.	kIx.	.
</s>
<s>
Tvář	tvář	k1gFnSc1	tvář
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
jakoby	jakoby	k8xS	jakoby
jiná	jiný	k2eAgFnSc1d1	jiná
-	-	kIx~	-
stále	stále	k6eAd1	stále
krásná	krásný	k2eAgFnSc1d1	krásná
<g/>
,	,	kIx,	,
úsměv	úsměv	k1gInSc1	úsměv
působí	působit	k5eAaImIp3nS	působit
spíše	spíše	k9	spíše
jako	jako	k9	jako
krutý	krutý	k2eAgInSc1d1	krutý
škleb	škleb	k1gInSc1	škleb
<g/>
.	.	kIx.	.
</s>
<s>
Dorian	Dorian	k1gInSc1	Dorian
se	se	k3xPyFc4	se
zalekne	zaleknout	k5eAaPmIp3nS	zaleknout
a	a	k8xC	a
přemýšlí	přemýšlet	k5eAaImIp3nS	přemýšlet
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
vlastně	vlastně	k9	vlastně
se	se	k3xPyFc4	se
k	k	k7c3	k
Sibyle	Sibyla	k1gFnSc3	Sibyla
tak	tak	k6eAd1	tak
zachoval	zachovat	k5eAaPmAgInS	zachovat
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
si	se	k3xPyFc3	se
uvědomí	uvědomit	k5eAaPmIp3nS	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gNnSc1	jeho
chování	chování	k1gNnSc1	chování
bylo	být	k5eAaImAgNnS	být
špatné	špatný	k2eAgNnSc1d1	špatné
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
už	už	k6eAd1	už
se	se	k3xPyFc4	se
chystá	chystat	k5eAaImIp3nS	chystat
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
židovského	židovský	k2eAgNnSc2d1	Židovské
divadla	divadlo	k1gNnSc2	divadlo
prosit	prosit	k5eAaImF	prosit
o	o	k7c6	o
odpuštění	odpuštění	k1gNnSc6	odpuštění
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
vstoupí	vstoupit	k5eAaPmIp3nP	vstoupit
do	do	k7c2	do
místnosti	místnost	k1gFnSc2	místnost
lord	lord	k1gMnSc1	lord
Henry	Henry	k1gMnSc1	Henry
<g/>
.	.	kIx.	.
</s>
<s>
Říká	říkat	k5eAaImIp3nS	říkat
Dorianovi	Dorian	k1gMnSc3	Dorian
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
vším	všecek	k3xTgNnSc7	všecek
netrápil	trápit	k5eNaImAgMnS	trápit
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
to	ten	k3xDgNnSc1	ten
není	být	k5eNaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
chyba	chyba	k1gFnSc1	chyba
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dorian	Dorian	k1gInSc1	Dorian
mu	on	k3xPp3gMnSc3	on
odpoví	odpovědět	k5eAaPmIp3nS	odpovědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
vše	všechen	k3xTgNnSc1	všechen
odčiní	odčinit	k5eAaPmIp3nS	odčinit
a	a	k8xC	a
Sibylu	Sibyla	k1gFnSc4	Sibyla
si	se	k3xPyFc3	se
vezme	vzít	k5eAaPmIp3nS	vzít
<g/>
.	.	kIx.	.
</s>
<s>
Henry	Henry	k1gMnSc1	Henry
nechápe	chápat	k5eNaImIp3nS	chápat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
Dorian	Dorian	k1gInSc1	Dorian
ještě	ještě	k6eAd1	ještě
nečetl	číst	k5eNaImAgInS	číst
jeho	on	k3xPp3gInSc4	on
dopis	dopis	k1gInSc4	dopis
<g/>
?	?	kIx.	?
</s>
<s>
Sibyla	Sibyla	k1gFnSc1	Sibyla
Vaneová	Vaneová	k1gFnSc1	Vaneová
je	být	k5eAaImIp3nS	být
mrtvá	mrtvý	k2eAgFnSc1d1	mrtvá
<g/>
.	.	kIx.	.
</s>
<s>
Spáchala	spáchat	k5eAaPmAgFnS	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
Dorian	Dorian	k1gMnSc1	Dorian
od	od	k7c2	od
Henryho	Henry	k1gMnSc2	Henry
tuto	tento	k3xDgFnSc4	tento
zprávu	zpráva	k1gFnSc4	zpráva
dozví	dozvědět	k5eAaPmIp3nS	dozvědět
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zlomen	zlomen	k2eAgMnSc1d1	zlomen
<g/>
.	.	kIx.	.
</s>
<s>
Působením	působení	k1gNnSc7	působení
lordových	lordův	k2eAgInPc2d1	lordův
psychologických	psychologický	k2eAgInPc2d1	psychologický
postupů	postup	k1gInPc2	postup
plných	plný	k2eAgFnPc2d1	plná
pokřiveností	pokřivenost	k1gFnPc2	pokřivenost
<g/>
,	,	kIx,	,
však	však	k9	však
odhodí	odhodit	k5eAaPmIp3nS	odhodit
všechny	všechen	k3xTgFnPc4	všechen
své	svůj	k3xOyFgFnPc4	svůj
city	city	k1gFnPc4	city
a	a	k8xC	a
jde	jít	k5eAaImIp3nS	jít
do	do	k7c2	do
opery	opera	k1gFnSc2	opera
a	a	k8xC	a
do	do	k7c2	do
klubu	klub	k1gInSc2	klub
šlechticů	šlechtic	k1gMnPc2	šlechtic
<g/>
.	.	kIx.	.
</s>
<s>
Smrt	smrt	k1gFnSc1	smrt
Sibyly	Sibyla	k1gFnSc2	Sibyla
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
vlastně	vlastně	k9	vlastně
krásná	krásný	k2eAgFnSc1d1	krásná
-	-	kIx~	-
zemřela	zemřít	k5eAaPmAgFnS	zemřít
pro	pro	k7c4	pro
lásku	láska	k1gFnSc4	láska
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
přece	přece	k9	přece
romantické	romantický	k2eAgNnSc1d1	romantické
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
navíc	navíc	k6eAd1	navíc
již	jenž	k3xRgFnSc4	jenž
minulost	minulost	k1gFnSc4	minulost
<g/>
!	!	kIx.	!
</s>
<s>
Když	když	k8xS	když
za	za	k7c7	za
ním	on	k3xPp3gMnSc7	on
druhý	druhý	k4xOgInSc4	druhý
den	den	k1gInSc4	den
přijde	přijít	k5eAaPmIp3nS	přijít
Basil	Basil	k1gInSc1	Basil
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ho	on	k3xPp3gInSc4	on
utěšil	utěšit	k5eAaPmAgInS	utěšit
<g/>
,	,	kIx,	,
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
Dorianovu	Dorianův	k2eAgInSc3d1	Dorianův
cynismu	cynismus	k1gInSc3	cynismus
uvěřit	uvěřit	k5eAaPmF	uvěřit
<g/>
.	.	kIx.	.
</s>
<s>
Dorian	Dorian	k1gMnSc1	Dorian
si	se	k3xPyFc3	se
začíná	začínat	k5eAaImIp3nS	začínat
uvědomovat	uvědomovat	k5eAaImF	uvědomovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gNnSc1	jeho
přání	přání	k1gNnSc1	přání
prohozené	prohozený	k2eAgNnSc1d1	prohozené
de	de	k?	de
facto	facto	k1gNnSc1	facto
mimoděk	mimoděk	k6eAd1	mimoděk
tehdy	tehdy	k6eAd1	tehdy
v	v	k7c6	v
ateliéru	ateliér	k1gInSc6	ateliér
se	se	k3xPyFc4	se
vyplňuje	vyplňovat	k5eAaImIp3nS	vyplňovat
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
hříchy	hřích	k1gInPc4	hřích
nenese	nést	k5eNaImIp3nS	nést
on	on	k3xPp3gMnSc1	on
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gInSc4	jeho
portrét	portrét	k1gInSc4	portrét
<g/>
.	.	kIx.	.
</s>
<s>
Ukrýt	ukrýt	k5eAaPmF	ukrýt
jej	on	k3xPp3gNnSc4	on
-	-	kIx~	-
to	ten	k3xDgNnSc1	ten
musí	muset	k5eAaImIp3nS	muset
udělat	udělat	k5eAaPmF	udělat
<g/>
.	.	kIx.	.
</s>
<s>
Nikdo	nikdo	k3yNnSc1	nikdo
nesmí	smět	k5eNaImIp3nS	smět
tento	tento	k3xDgInSc4	tento
obraz	obraz	k1gInSc4	obraz
nikdy	nikdy	k6eAd1	nikdy
spatřit	spatřit	k5eAaPmF	spatřit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nebyla	být	k5eNaImAgFnS	být
odhalena	odhalit	k5eAaPmNgFnS	odhalit
pravá	pravý	k2eAgFnSc1d1	pravá
podstata	podstata	k1gFnSc1	podstata
Dorianovy	Dorianův	k2eAgFnSc2d1	Dorianova
duše	duše	k1gFnSc2	duše
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
Basil	Basil	k1gMnSc1	Basil
-	-	kIx~	-
sám	sám	k3xTgMnSc1	sám
autor	autor	k1gMnSc1	autor
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
nedávno	nedávno	k6eAd1	nedávno
vyznal	vyznat	k5eAaBmAgMnS	vyznat
k	k	k7c3	k
citům	cit	k1gInPc3	cit
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
do	do	k7c2	do
obrazu	obraz	k1gInSc2	obraz
zachytil	zachytit	k5eAaPmAgMnS	zachytit
<g/>
,	,	kIx,	,
jej	on	k3xPp3gInSc4	on
nesmí	smět	k5eNaImIp3nS	smět
vidět	vidět	k5eAaImF	vidět
<g/>
!	!	kIx.	!
</s>
<s>
Zabalí	zabalit	k5eAaPmIp3nP	zabalit
jej	on	k3xPp3gMnSc4	on
proto	proto	k8xC	proto
do	do	k7c2	do
roucha	roucho	k1gNnSc2	roucho
a	a	k8xC	a
s	s	k7c7	s
pomocí	pomoc	k1gFnPc2	pomoc
dvou	dva	k4xCgInPc2	dva
mužů	muž	k1gMnPc2	muž
ho	on	k3xPp3gInSc2	on
vynese	vynést	k5eAaPmIp3nS	vynést
do	do	k7c2	do
staré	starý	k2eAgFnSc2d1	stará
nepoužívané	používaný	k2eNgFnSc2d1	nepoužívaná
studovny	studovna	k1gFnSc2	studovna
v	v	k7c6	v
horních	horní	k2eAgNnPc6d1	horní
patrech	patro	k1gNnPc6	patro
jeho	jeho	k3xOp3gInSc2	jeho
domu	dům	k1gInSc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
žije	žít	k5eAaImIp3nS	žít
Dorian	Dorian	k1gInSc4	Dorian
na	na	k7c6	na
výsluní	výsluní	k1gNnSc6	výsluní
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
krása	krása	k1gFnSc1	krása
a	a	k8xC	a
šarm	šarm	k1gInSc1	šarm
jsou	být	k5eAaImIp3nP	být
mu	on	k3xPp3gMnSc3	on
branou	braný	k2eAgFnSc7d1	braná
do	do	k7c2	do
nejužší	úzký	k2eAgFnSc2d3	nejužší
smetánky	smetánka	k1gFnSc2	smetánka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
oblíbený	oblíbený	k2eAgMnSc1d1	oblíbený
a	a	k8xC	a
obdivovaný	obdivovaný	k2eAgMnSc1d1	obdivovaný
lidmi	člověk	k1gMnPc7	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
ho	on	k3xPp3gMnSc4	on
málo	málo	k6eAd1	málo
znají	znát	k5eAaImIp3nP	znát
a	a	k8xC	a
všichni	všechen	k3xTgMnPc1	všechen
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
ho	on	k3xPp3gMnSc4	on
poznají	poznat	k5eAaPmIp3nP	poznat
<g/>
,	,	kIx,	,
skončí	skončit	k5eAaPmIp3nP	skončit
zničeni	zničen	k2eAgMnPc1d1	zničen
<g/>
.	.	kIx.	.
</s>
<s>
Nic	nic	k3yNnSc1	nic
mu	on	k3xPp3gMnSc3	on
není	být	k5eNaImIp3nS	být
odepřeno	odepřen	k2eAgNnSc1d1	odepřeno
<g/>
.	.	kIx.	.
</s>
<s>
Dorian	Dorian	k1gInSc1	Dorian
se	se	k3xPyFc4	se
oddává	oddávat	k5eAaImIp3nS	oddávat
umění	umění	k1gNnSc1	umění
<g/>
,	,	kIx,	,
stává	stávat	k5eAaImIp3nS	stávat
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gMnSc4	on
ctěný	ctěný	k2eAgMnSc1d1	ctěný
sběratel	sběratel	k1gMnSc1	sběratel
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
večerech	večer	k1gInPc6	večer
se	se	k3xPyFc4	se
se	s	k7c7	s
zrcadlem	zrcadlo	k1gNnSc7	zrcadlo
v	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
kochá	kochat	k5eAaImIp3nS	kochat
ohyzdností	ohyzdnost	k1gFnSc7	ohyzdnost
svého	svůj	k3xOyFgInSc2	svůj
portrétu	portrét	k1gInSc2	portrét
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
společnosti	společnost	k1gFnSc2	společnost
již	již	k6eAd1	již
utrpěla	utrpět	k5eAaPmAgFnS	utrpět
Dorianovým	Dorianův	k2eAgInSc7d1	Dorianův
vlivem	vliv	k1gInSc7	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Dámy	dáma	k1gFnPc1	dáma
dříve	dříve	k6eAd2	dříve
ctěné	ctěný	k2eAgFnPc1d1	ctěná
jsou	být	k5eAaImIp3nP	být
nyní	nyní	k6eAd1	nyní
všemi	všecek	k3xTgFnPc7	všecek
odmítány	odmítat	k5eAaImNgInP	odmítat
<g/>
,	,	kIx,	,
vážení	vážený	k2eAgMnPc1d1	vážený
lordové	lord	k1gMnPc1	lord
odchází	odcházet	k5eAaImIp3nP	odcházet
z	z	k7c2	z
místnosti	místnost	k1gFnSc2	místnost
<g/>
,	,	kIx,	,
jakmile	jakmile	k8xS	jakmile
Dorian	Dorian	k1gMnSc1	Dorian
přijde	přijít	k5eAaPmIp3nS	přijít
<g/>
,	,	kIx,	,
úsměvy	úsměv	k1gInPc4	úsměv
mnohých	mnohé	k1gNnPc2	mnohé
jsou	být	k5eAaImIp3nP	být
přetvářka	přetvářka	k1gFnSc1	přetvářka
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
však	však	k9	však
je	být	k5eAaImIp3nS	být
Dorian	Dorian	k1gInSc1	Dorian
-	-	kIx~	-
i	i	k9	i
díky	díky	k7c3	díky
svému	svůj	k3xOyFgInSc3	svůj
přenádhernému	přenádherný	k2eAgInSc3d1	přenádherný
vzhledu	vzhled	k1gInSc3	vzhled
-	-	kIx~	-
stále	stále	k6eAd1	stále
mnohými	mnohý	k2eAgMnPc7d1	mnohý
ctěn	ctít	k5eAaImNgMnS	ctít
<g/>
.	.	kIx.	.
</s>
<s>
Dorian	Dorian	k1gMnSc1	Dorian
si	se	k3xPyFc3	se
ničeho	nic	k3yNnSc2	nic
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
mluví	mluvit	k5eAaImIp3nS	mluvit
proti	proti	k7c3	proti
němu	on	k3xPp3gMnSc3	on
<g/>
,	,	kIx,	,
nevšímá	všímat	k5eNaImIp3nS	všímat
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgMnPc4	svůj
kritiky	kritik	k1gMnPc4	kritik
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c2	za
mělké	mělký	k2eAgFnSc2d1	mělká
duchem	duch	k1gMnSc7	duch
a	a	k8xC	a
milenkami	milenka	k1gFnPc7	milenka
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
ho	on	k3xPp3gInSc4	on
již	již	k6eAd1	již
omrzely	omrzet	k5eAaPmAgInP	omrzet
<g/>
,	,	kIx,	,
opovrhuje	opovrhovat	k5eAaImIp3nS	opovrhovat
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jednou	k6eAd1	jednou
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
Dorian	Dorian	k1gMnSc1	Dorian
vracel	vracet	k5eAaImAgMnS	vracet
od	od	k7c2	od
lorda	lord	k1gMnSc2	lord
Henryho	Henry	k1gMnSc2	Henry
<g/>
,	,	kIx,	,
potkal	potkat	k5eAaPmAgInS	potkat
se	se	k3xPyFc4	se
s	s	k7c7	s
Basilem	Basil	k1gInSc7	Basil
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
mu	on	k3xPp3gMnSc3	on
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
odjíždí	odjíždět	k5eAaImIp3nS	odjíždět
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
a	a	k8xC	a
že	že	k8xS	že
by	by	kYmCp3nS	by
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
rád	rád	k6eAd1	rád
naposledy	naposledy	k6eAd1	naposledy
mluvil	mluvit	k5eAaImAgMnS	mluvit
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
dorazí	dorazit	k5eAaPmIp3nP	dorazit
k	k	k7c3	k
Dorianovi	Dorian	k1gMnSc3	Dorian
domů	domů	k6eAd1	domů
<g/>
,	,	kIx,	,
Basil	Basil	k1gInSc1	Basil
sdělí	sdělit	k5eAaPmIp3nS	sdělit
svému	svůj	k3xOyFgMnSc3	svůj
příteli	přítel	k1gMnSc3	přítel
<g/>
,	,	kIx,	,
co	co	k9	co
ho	on	k3xPp3gMnSc4	on
tíží	tížit	k5eAaImIp3nS	tížit
<g/>
.	.	kIx.	.
</s>
<s>
Nemůže	moct	k5eNaImIp3nS	moct
uvěřit	uvěřit	k5eAaPmF	uvěřit
zvěstem	zvěst	k1gFnPc3	zvěst
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
o	o	k7c6	o
Dorianovi	Dorianův	k2eAgMnPc1d1	Dorianův
kolují	kolovat	k5eAaImIp3nP	kolovat
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
ho	on	k3xPp3gNnSc4	on
vždy	vždy	k6eAd1	vždy
nesmírně	smírně	k6eNd1	smírně
rád	rád	k2eAgMnSc1d1	rád
<g/>
,	,	kIx,	,
dával	dávat	k5eAaImAgMnS	dávat
mu	on	k3xPp3gMnSc3	on
veškerý	veškerý	k3xTgInSc4	veškerý
svůj	svůj	k3xOyFgInSc4	svůj
obdiv	obdiv	k1gInSc4	obdiv
<g/>
,	,	kIx,	,
zrcadlil	zrcadlit	k5eAaImAgInS	zrcadlit
ho	on	k3xPp3gNnSc4	on
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
umění	umění	k1gNnSc6	umění
<g/>
.	.	kIx.	.
</s>
<s>
Teď	teď	k6eAd1	teď
je	být	k5eAaImIp3nS	být
vystavován	vystavovat	k5eAaImNgInS	vystavovat
názorům	názor	k1gInPc3	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
Dorian	Dorian	k1gInSc1	Dorian
stojí	stát	k5eAaImIp3nS	stát
za	za	k7c7	za
zkázou	zkáza	k1gFnSc7	zkáza
a	a	k8xC	a
smrtí	smrt	k1gFnSc7	smrt
mnoha	mnoho	k4c2	mnoho
lidí	člověk	k1gMnPc2	člověk
<g/>
...	...	k?	...
Dorian	Dorian	k1gMnSc1	Dorian
tyto	tento	k3xDgFnPc4	tento
řeči	řeč	k1gFnPc4	řeč
odmítá	odmítat	k5eAaImIp3nS	odmítat
<g/>
.	.	kIx.	.
</s>
<s>
Nic	nic	k3yNnSc1	nic
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
o	o	k7c6	o
čem	co	k3yInSc6	co
Basil	Basila	k1gFnPc2	Basila
mluví	mluvit	k5eAaImIp3nS	mluvit
prý	prý	k9	prý
nemá	mít	k5eNaImIp3nS	mít
na	na	k7c6	na
svědomí	svědomí	k1gNnSc6	svědomí
-	-	kIx~	-
sebevražda	sebevražda	k1gFnSc1	sebevražda
a	a	k8xC	a
neřesti	neřest	k1gFnSc2	neřest
jsou	být	k5eAaImIp3nP	být
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
je	on	k3xPp3gNnSc4	on
páchají	páchat	k5eAaImIp3nP	páchat
<g/>
.	.	kIx.	.
</s>
<s>
Basil	Basil	k1gMnSc1	Basil
mu	on	k3xPp3gNnSc3	on
věří	věřit	k5eAaImIp3nS	věřit
-	-	kIx~	-
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
hřích	hřích	k1gInSc4	hřích
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
člověku	člověk	k1gMnSc3	člověk
vepsal	vepsat	k5eAaPmAgInS	vepsat
do	do	k7c2	do
tváře	tvář	k1gFnSc2	tvář
<g/>
.	.	kIx.	.
</s>
<s>
Lituje	litovat	k5eAaImIp3nS	litovat
však	však	k9	však
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemůže	moct	k5eNaImIp3nS	moct
pohlédnout	pohlédnout	k5eAaPmF	pohlédnout
přímo	přímo	k6eAd1	přímo
na	na	k7c4	na
Dorianovu	Dorianův	k2eAgFnSc4d1	Dorianova
duši	duše	k1gFnSc4	duše
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
překvapen	překvapit	k5eAaPmNgMnS	překvapit
<g/>
,	,	kIx,	,
když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
Dorian	Dorian	k1gInSc1	Dorian
nabídne	nabídnout	k5eAaPmIp3nS	nabídnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
svou	svůj	k3xOyFgFnSc4	svůj
duši	duše	k1gFnSc6	duše
odhalí	odhalit	k5eAaPmIp3nP	odhalit
v	v	k7c6	v
celé	celá	k1gFnSc6	celá
její	její	k3xOp3gFnSc6	její
podobě	podoba	k1gFnSc6	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Malíř	malíř	k1gMnSc1	malíř
nechápe	chápat	k5eNaImIp3nS	chápat
<g/>
.	.	kIx.	.
</s>
<s>
Jdou	jít	k5eAaImIp3nP	jít
spolu	spolu	k6eAd1	spolu
po	po	k7c6	po
schodech	schod	k1gInPc6	schod
do	do	k7c2	do
staré	starý	k2eAgFnSc2d1	stará
místnosti	místnost	k1gFnSc2	místnost
<g/>
.	.	kIx.	.
</s>
<s>
Dorian	Dorian	k1gMnSc1	Dorian
se	se	k3xPyFc4	se
naposledy	naposledy	k6eAd1	naposledy
táže	tázat	k5eAaImIp3nS	tázat
malíře	malíř	k1gMnSc2	malíř
<g/>
,	,	kIx,	,
chce	chtít	k5eAaImIp3nS	chtít
<g/>
-li	i	k?	-li
vidět	vidět	k5eAaImF	vidět
jeho	jeho	k3xOp3gFnSc4	jeho
duši	duše	k1gFnSc4	duše
a	a	k8xC	a
strhává	strhávat	k5eAaImIp3nS	strhávat
závěs	závěs	k1gInSc1	závěs
ze	z	k7c2	z
svého	svůj	k3xOyFgInSc2	svůj
portrétu	portrét	k1gInSc2	portrét
<g/>
.	.	kIx.	.
</s>
<s>
Basil	Basil	k1gMnSc1	Basil
zděšením	zděšení	k1gNnPc3	zděšení
vykřikne	vykřiknout	k5eAaPmIp3nS	vykřiknout
<g/>
.	.	kIx.	.
</s>
<s>
Nemůže	moct	k5eNaImIp3nS	moct
věřit	věřit	k5eAaImF	věřit
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
vidí	vidět	k5eAaImIp3nS	vidět
<g/>
.	.	kIx.	.
</s>
<s>
Odporné	odporný	k2eAgInPc1d1	odporný
rysy	rys	k1gInPc1	rys
ve	v	k7c6	v
tváři	tvář	k1gFnSc6	tvář
-	-	kIx~	-
ne	ne	k9	ne
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
nějaká	nějaký	k3yIgFnSc1	nějaký
nevkusná	vkusný	k2eNgFnSc1d1	nevkusná
karikatura	karikatura	k1gFnSc1	karikatura
<g/>
!	!	kIx.	!
</s>
<s>
Vždyť	vždyť	k9	vždyť
tento	tento	k3xDgInSc1	tento
portrét	portrét	k1gInSc1	portrét
nemohl	moct	k5eNaImAgInS	moct
malovat	malovat	k5eAaImF	malovat
on	on	k3xPp3gMnSc1	on
<g/>
!	!	kIx.	!
</s>
<s>
Ale	ale	k9	ale
dole	dole	k6eAd1	dole
v	v	k7c6	v
rohu	roh	k1gInSc6	roh
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gNnSc4	jeho
jméno	jméno	k1gNnSc4	jméno
<g/>
...	...	k?	...
Basil	Basil	k1gInSc1	Basil
se	se	k3xPyFc4	se
začne	začít	k5eAaPmIp3nS	začít
za	za	k7c4	za
Doriana	Dorian	k1gMnSc4	Dorian
modlit	modlit	k5eAaImF	modlit
<g/>
,	,	kIx,	,
z	z	k7c2	z
posledních	poslední	k2eAgFnPc2d1	poslední
sil	síla	k1gFnPc2	síla
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
toho	ten	k3xDgMnSc4	ten
krásného	krásný	k2eAgMnSc4d1	krásný
chlapce	chlapec	k1gMnSc4	chlapec
zachránit	zachránit	k5eAaPmF	zachránit
<g/>
.	.	kIx.	.
</s>
<s>
Dorian	Dorian	k1gMnSc1	Dorian
s	s	k7c7	s
uslzenýma	uslzený	k2eAgNnPc7d1	uslzené
očima	oko	k1gNnPc7	oko
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
pozdě	pozdě	k6eAd1	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Basil	Basil	k1gInSc1	Basil
se	se	k3xPyFc4	se
nevzdává	vzdávat	k5eNaImIp3nS	vzdávat
<g/>
,	,	kIx,	,
vyzývá	vyzývat	k5eAaImIp3nS	vyzývat
Doriana	Doriana	k1gFnSc1	Doriana
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
nevzdal	vzdát	k5eNaPmAgMnS	vzdát
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
nepoddal	poddat	k5eNaPmAgMnS	poddat
té	ten	k3xDgFnSc3	ten
stvůře	stvůra	k1gFnSc3	stvůra
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
po	po	k7c6	po
něm	on	k3xPp3gNnSc6	on
zlověstně	zlověstně	k6eAd1	zlověstně
pokukuje	pokukovat	k5eAaImIp3nS	pokukovat
z	z	k7c2	z
rámu	rám	k1gInSc2	rám
obrazu	obraz	k1gInSc2	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Doriana	Doriana	k1gFnSc1	Doriana
Graye	Gray	k1gInSc2	Gray
náhle	náhle	k6eAd1	náhle
pohltí	pohltit	k5eAaPmIp3nP	pohltit
nenávist	nenávist	k1gFnSc4	nenávist
k	k	k7c3	k
Basilovi	Basil	k1gMnSc3	Basil
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
přece	přece	k9	přece
ten	ten	k3xDgMnSc1	ten
portrét	portrét	k1gInSc4	portrét
namaloval	namalovat	k5eAaPmAgMnS	namalovat
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
to	ten	k3xDgNnSc4	ten
byl	být	k5eAaImAgMnS	být
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
od	od	k7c2	od
základů	základ	k1gInPc2	základ
změnil	změnit	k5eAaPmAgInS	změnit
jeho	jeho	k3xOp3gInSc4	jeho
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Popadl	popadnout	k5eAaPmAgInS	popadnout
nůž	nůž	k1gInSc1	nůž
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zde	zde	k6eAd1	zde
v	v	k7c6	v
místnosti	místnost	k1gFnSc6	místnost
týž	týž	k3xTgInSc4	týž
den	den	k1gInSc4	den
zanechal	zanechat	k5eAaPmAgMnS	zanechat
a	a	k8xC	a
bodl	bodnout	k5eAaPmAgMnS	bodnout
několikrát	několikrát	k6eAd1	několikrát
Basila	Basila	k1gFnSc1	Basila
do	do	k7c2	do
krku	krk	k1gInSc2	krk
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc4	druhý
den	den	k1gInSc4	den
ráno	ráno	k6eAd1	ráno
napsal	napsat	k5eAaBmAgMnS	napsat
Dorian	Dorian	k1gMnSc1	Dorian
dopis	dopis	k1gInSc4	dopis
svému	svůj	k3xOyFgMnSc3	svůj
známému	známý	k1gMnSc3	známý
<g/>
,	,	kIx,	,
bývalému	bývalý	k2eAgMnSc3d1	bývalý
nerozlučnému	rozlučný	k2eNgMnSc3d1	nerozlučný
příteli	přítel	k1gMnSc3	přítel
<g/>
,	,	kIx,	,
kterému	který	k3yIgMnSc3	který
už	už	k9	už
ale	ale	k9	ale
také	také	k9	také
stihl	stihnout	k5eAaPmAgMnS	stihnout
zničit	zničit	k5eAaPmF	zničit
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
Alanu	Alan	k1gMnSc3	Alan
Campbellovi	Campbell	k1gMnSc3	Campbell
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
dostavil	dostavit	k5eAaPmAgMnS	dostavit
v	v	k7c6	v
otázce	otázka	k1gFnSc6	otázka
života	život	k1gInSc2	život
a	a	k8xC	a
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Alan	Alan	k1gMnSc1	Alan
je	být	k5eAaImIp3nS	být
vynikající	vynikající	k2eAgMnSc1d1	vynikající
chemik	chemik	k1gMnSc1	chemik
<g/>
,	,	kIx,	,
biolog	biolog	k1gMnSc1	biolog
a	a	k8xC	a
hudebník	hudebník	k1gMnSc1	hudebník
<g/>
.	.	kIx.	.
</s>
<s>
Alan	Alan	k1gMnSc1	Alan
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
Dorianovi	Dorian	k1gMnSc3	Dorian
velmi	velmi	k6eAd1	velmi
chladný	chladný	k2eAgInSc1d1	chladný
<g/>
.	.	kIx.	.
</s>
<s>
Nikdy	nikdy	k6eAd1	nikdy
již	již	k6eAd1	již
nechtěl	chtít	k5eNaImAgMnS	chtít
vkročit	vkročit	k5eAaPmF	vkročit
do	do	k7c2	do
jeho	jeho	k3xOp3gInSc2	jeho
domu	dům	k1gInSc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
přemáháním	přemáhání	k1gNnSc7	přemáhání
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
Doriana	Doriana	k1gFnSc1	Doriana
vyslechnout	vyslechnout	k5eAaPmF	vyslechnout
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
mu	on	k3xPp3gMnSc3	on
oznámí	oznámit	k5eAaPmIp3nS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nahoře	nahoře	k6eAd1	nahoře
v	v	k7c6	v
pokoji	pokoj	k1gInSc6	pokoj
je	být	k5eAaImIp3nS	být
mrtvý	mrtvý	k2eAgMnSc1d1	mrtvý
muž	muž	k1gMnSc1	muž
-	-	kIx~	-
a	a	k8xC	a
na	na	k7c6	na
Alanovi	Alan	k1gMnSc6	Alan
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
využil	využít	k5eAaPmAgMnS	využít
svých	svůj	k3xOyFgFnPc2	svůj
znalostí	znalost	k1gFnPc2	znalost
chemie	chemie	k1gFnSc2	chemie
a	a	k8xC	a
biologie	biologie	k1gFnSc2	biologie
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
po	po	k7c6	po
těle	tělo	k1gNnSc6	tělo
nezůstalo	zůstat	k5eNaPmAgNnS	zůstat
absolutně	absolutně	k6eAd1	absolutně
vůbec	vůbec	k9	vůbec
nic	nic	k3yNnSc1	nic
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
jak	jak	k6eAd1	jak
dotyčný	dotyčný	k2eAgMnSc1d1	dotyčný
zemřel	zemřít	k5eAaPmAgMnS	zemřít
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
nemusí	muset	k5eNaImIp3nS	muset
týkat	týkat	k5eAaImF	týkat
<g/>
,	,	kIx,	,
Dorianovi	Dorianův	k2eAgMnPc1d1	Dorianův
je	být	k5eAaImIp3nS	být
zase	zase	k9	zase
jedno	jeden	k4xCgNnSc1	jeden
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
těla	tělo	k1gNnSc2	tělo
Alan	alan	k1gInSc1	alan
zbaví	zbavit	k5eAaPmIp3nP	zbavit
<g/>
.	.	kIx.	.
</s>
<s>
Alan	Alan	k1gMnSc1	Alan
je	být	k5eAaImIp3nS	být
zděšen	zděsit	k5eAaPmNgMnS	zděsit
<g/>
,	,	kIx,	,
vstane	vstát	k5eAaPmIp3nS	vstát
a	a	k8xC	a
chystá	chystat	k5eAaImIp3nS	chystat
se	se	k3xPyFc4	se
odejít	odejít	k5eAaPmF	odejít
<g/>
.	.	kIx.	.
</s>
<s>
Dorian	Dorian	k1gInSc1	Dorian
ho	on	k3xPp3gMnSc4	on
zdržuje	zdržovat	k5eAaImIp3nS	zdržovat
-	-	kIx~	-
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
muž	muž	k1gMnSc1	muž
spáchal	spáchat	k5eAaPmAgMnS	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Alan	Alan	k1gMnSc1	Alan
namítá	namítat	k5eAaImIp3nS	namítat
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c4	za
ni	on	k3xPp3gFnSc4	on
je	být	k5eAaImIp3nS	být
jistě	jistě	k9	jistě
stejně	stejně	k6eAd1	stejně
zodpovědný	zodpovědný	k2eAgMnSc1d1	zodpovědný
přímo	přímo	k6eAd1	přímo
Dorian	Dorian	k1gInSc4	Dorian
<g/>
,	,	kIx,	,
chce	chtít	k5eAaImIp3nS	chtít
odejít	odejít	k5eAaPmF	odejít
a	a	k8xC	a
být	být	k5eAaImF	být
pryč	pryč	k6eAd1	pryč
od	od	k7c2	od
toho	ten	k3xDgMnSc2	ten
podivína	podivín	k1gMnSc2	podivín
<g/>
.	.	kIx.	.
</s>
<s>
Dorian	Dorian	k1gInSc1	Dorian
se	se	k3xPyFc4	se
dozná	doznat	k5eAaPmIp3nS	doznat
-	-	kIx~	-
byl	být	k5eAaImAgMnS	být
to	ten	k3xDgNnSc4	ten
on	on	k3xPp3gMnSc1	on
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
rukou	ruka	k1gFnSc7	ruka
zabil	zabít	k5eAaPmAgMnS	zabít
toho	ten	k3xDgMnSc4	ten
muže	muž	k1gMnSc4	muž
<g/>
.	.	kIx.	.
</s>
<s>
Campbel	Campbel	k1gMnSc1	Campbel
je	být	k5eAaImIp3nS	být
zděšen	zděsit	k5eAaPmNgMnS	zděsit
<g/>
.	.	kIx.	.
</s>
<s>
Nemůže	moct	k5eNaImIp3nS	moct
uvěřit	uvěřit	k5eAaPmF	uvěřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Dorian	Dorian	k1gMnSc1	Dorian
zašel	zajít	k5eAaPmAgMnS	zajít
až	až	k9	až
tak	tak	k6eAd1	tak
daleko	daleko	k6eAd1	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Slíbí	slíbit	k5eAaPmIp3nS	slíbit
<g/>
,	,	kIx,	,
že	že	k8xS	že
neprozradí	prozradit	k5eNaPmIp3nP	prozradit
nic	nic	k3yNnSc4	nic
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
slyšel	slyšet	k5eAaImAgMnS	slyšet
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
že	že	k9	že
Dorianovi	Dorianův	k2eAgMnPc1d1	Dorianův
s	s	k7c7	s
ničím	ničí	k3xOyNgNnSc7	ničí
nepomůže	pomoct	k5eNaPmIp3nS	pomoct
<g/>
.	.	kIx.	.
</s>
<s>
Dorian	Dorian	k1gInSc1	Dorian
zdvihne	zdvihnout	k5eAaPmIp3nS	zdvihnout
kousek	kousek	k1gInSc4	kousek
papíru	papír	k1gInSc2	papír
<g/>
,	,	kIx,	,
cosi	cosi	k3yInSc1	cosi
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
napíše	napsat	k5eAaBmIp3nS	napsat
a	a	k8xC	a
podá	podat	k5eAaPmIp3nS	podat
jej	on	k3xPp3gMnSc4	on
Campbelovi	Campbelovi	k1gRnPc1	Campbelovi
<g/>
.	.	kIx.	.
</s>
<s>
Campbelovi	Campbelův	k2eAgMnPc1d1	Campbelův
smrtelně	smrtelně	k6eAd1	smrtelně
zblednou	zblednout	k5eAaPmIp3nP	zblednout
tváře	tvář	k1gFnPc1	tvář
<g/>
,	,	kIx,	,
těžce	těžce	k6eAd1	těžce
se	se	k3xPyFc4	se
usadí	usadit	k5eAaPmIp3nP	usadit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dvou	dva	k4xCgFnPc6	dva
až	až	k6eAd1	až
třech	tři	k4xCgFnPc6	tři
minutách	minuta	k1gFnPc6	minuta
mu	on	k3xPp3gMnSc3	on
Dorian	Dorian	k1gInSc1	Dorian
oznámí	oznámit	k5eAaPmIp3nS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
odešle	odeslat	k5eAaPmIp3nS	odeslat
kompromitující	kompromitující	k2eAgInSc4d1	kompromitující
dopis	dopis	k1gInSc4	dopis
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
mu	on	k3xPp3gMnSc3	on
nepomůže	pomoct	k5eNaPmIp3nS	pomoct
<g/>
.	.	kIx.	.
</s>
<s>
Alan	Alan	k1gMnSc1	Alan
musí	muset	k5eAaImIp3nS	muset
nakonec	nakonec	k6eAd1	nakonec
souhlasit	souhlasit	k5eAaImF	souhlasit
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
to	ten	k3xDgNnSc4	ten
vše	všechen	k3xTgNnSc4	všechen
se	se	k3xPyFc4	se
Dorian	Dorian	k1gInSc1	Dorian
ještě	ještě	k9	ještě
téhož	týž	k3xTgInSc2	týž
večera	večer	k1gInSc2	večer
vypraví	vypravit	k5eAaPmIp3nS	vypravit
do	do	k7c2	do
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
dnech	den	k1gInPc6	den
pak	pak	k6eAd1	pak
žije	žít	k5eAaImIp3nS	žít
navenek	navenek	k6eAd1	navenek
stejně	stejně	k6eAd1	stejně
bujarý	bujarý	k2eAgInSc4d1	bujarý
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Trápí	trápit	k5eAaImIp3nS	trápit
se	se	k3xPyFc4	se
však	však	k9	však
víc	hodně	k6eAd2	hodně
a	a	k8xC	a
víc	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
<s>
Duši	duše	k1gFnSc4	duše
léčit	léčit	k5eAaImF	léčit
skrze	skrze	k?	skrze
smysly	smysl	k1gInPc4	smysl
-	-	kIx~	-
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
ono	onen	k3xDgNnSc1	onen
<g/>
!	!	kIx.	!
</s>
<s>
Vydá	vydat	k5eAaPmIp3nS	vydat
se	se	k3xPyFc4	se
na	na	k7c4	na
výlet	výlet	k1gInSc4	výlet
na	na	k7c6	na
předměstí	předměstí	k1gNnSc6	předměstí
<g/>
,	,	kIx,	,
chce	chtít	k5eAaImIp3nS	chtít
zkusit	zkusit	k5eAaPmF	zkusit
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
klubů	klub	k1gInPc2	klub
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
podává	podávat	k5eAaImIp3nS	podávat
opium	opium	k1gNnSc1	opium
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
však	však	k9	však
v	v	k7c6	v
klubu	klub	k1gInSc6	klub
potká	potkat	k5eAaPmIp3nS	potkat
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kterému	který	k3yRgMnSc3	který
zničil	zničit	k5eAaPmAgInS	zničit
život	život	k1gInSc1	život
<g/>
,	,	kIx,	,
a	a	k8xC	a
vidí	vidět	k5eAaImIp3nS	vidět
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
udělal	udělat	k5eAaPmAgMnS	udělat
<g/>
,	,	kIx,	,
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
z	z	k7c2	z
klubu	klub	k1gInSc2	klub
odejít	odejít	k5eAaPmF	odejít
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
místních	místní	k2eAgFnPc2d1	místní
žen	žena	k1gFnPc2	žena
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
pokouší	pokoušet	k5eAaImIp3nS	pokoušet
koketovat	koketovat	k5eAaImF	koketovat
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
ho	on	k3xPp3gMnSc4	on
osloví	oslovit	k5eAaPmIp3nS	oslovit
jako	jako	k8xC	jako
ďáblova	ďáblův	k2eAgMnSc4d1	ďáblův
zaprodance	zaprodanec	k1gMnSc4	zaprodanec
<g/>
,	,	kIx,	,
Dorian	Dorian	k1gInSc1	Dorian
ji	on	k3xPp3gFnSc4	on
okřikne	okřiknout	k5eAaPmIp3nS	okřiknout
<g/>
.	.	kIx.	.
</s>
<s>
Odpovědí	odpověď	k1gFnSc7	odpověď
od	od	k7c2	od
děvy	děva	k1gFnSc2	děva
mu	on	k3xPp3gMnSc3	on
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
zajisté	zajisté	k9	zajisté
raději	rád	k6eAd2	rád
nechává	nechávat	k5eAaImIp3nS	nechávat
říkat	říkat	k5eAaImF	říkat
"	"	kIx"	"
<g/>
Princ	princ	k1gMnSc1	princ
z	z	k7c2	z
pohádky	pohádka	k1gFnSc2	pohádka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
zaslechne	zaslechnout	k5eAaPmIp3nS	zaslechnout
v	v	k7c6	v
rohu	roh	k1gInSc6	roh
místnosti	místnost	k1gFnSc2	místnost
jeden	jeden	k4xCgMnSc1	jeden
námořník	námořník	k1gMnSc1	námořník
<g/>
.	.	kIx.	.
</s>
<s>
Vydá	vydat	k5eAaPmIp3nS	vydat
se	se	k3xPyFc4	se
za	za	k7c7	za
Dorianem	Dorian	k1gInSc7	Dorian
<g/>
.	.	kIx.	.
</s>
<s>
Ano	ano	k9	ano
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
James	James	k1gMnSc1	James
Vane	vanout	k5eAaImIp3nS	vanout
<g/>
,	,	kIx,	,
bratr	bratr	k1gMnSc1	bratr
Sibyly	Sibyla	k1gFnSc2	Sibyla
Vaneové	Vaneová	k1gFnSc2	Vaneová
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Doriana	Doriana	k1gFnSc1	Doriana
dostihne	dostihnout	k5eAaPmIp3nS	dostihnout
<g/>
,	,	kIx,	,
chytí	chytit	k5eAaPmIp3nS	chytit
ho	on	k3xPp3gMnSc4	on
za	za	k7c4	za
krk	krk	k1gInSc4	krk
a	a	k8xC	a
namíří	namířit	k5eAaPmIp3nS	namířit
na	na	k7c4	na
něj	on	k3xPp3gInSc4	on
revolver	revolver	k1gInSc4	revolver
<g/>
.	.	kIx.	.
</s>
<s>
Dá	dát	k5eAaPmIp3nS	dát
mu	on	k3xPp3gMnSc3	on
minutu	minuta	k1gFnSc4	minuta
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
smířil	smířit	k5eAaPmAgInS	smířit
s	s	k7c7	s
bohem	bůh	k1gMnSc7	bůh
<g/>
,	,	kIx,	,
potom	potom	k6eAd1	potom
ho	on	k3xPp3gMnSc4	on
prý	prý	k9	prý
zabije	zabít	k5eAaPmIp3nS	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Dorian	Dorian	k1gMnSc1	Dorian
předstírá	předstírat	k5eAaImIp3nS	předstírat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vůbec	vůbec	k9	vůbec
neví	vědět	k5eNaImIp3nS	vědět
<g/>
,	,	kIx,	,
o	o	k7c6	o
čem	co	k3yQnSc6	co
je	být	k5eAaImIp3nS	být
řeč	řeč	k1gFnSc1	řeč
<g/>
.	.	kIx.	.
</s>
<s>
Zeptá	zeptat	k5eAaPmIp3nS	zeptat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
celá	celý	k2eAgFnSc1d1	celá
záležitost	záležitost	k1gFnSc1	záležitost
<g/>
,	,	kIx,	,
o	o	k7c4	o
které	který	k3yQgFnPc4	který
James	James	k1gMnSc1	James
mluví	mluvit	k5eAaImIp3nS	mluvit
<g/>
,	,	kIx,	,
stala	stát	k5eAaPmAgFnS	stát
<g/>
.	.	kIx.	.
</s>
<s>
Odpověď	odpověď	k1gFnSc1	odpověď
<g/>
:	:	kIx,	:
Před	před	k7c7	před
osmnácti	osmnáct	k4xCc7	osmnáct
lety	let	k1gInPc7	let
<g/>
...	...	k?	...
Dorian	Dorian	k1gInSc1	Dorian
zajásá	zajásat	k5eAaPmIp3nS	zajásat
<g/>
.	.	kIx.	.
</s>
<s>
Požádá	požádat	k5eAaPmIp3nS	požádat
námořníka	námořník	k1gMnSc4	námořník
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
ho	on	k3xPp3gNnSc4	on
důkladně	důkladně	k6eAd1	důkladně
prohlédl	prohlédnout	k5eAaPmAgMnS	prohlédnout
<g/>
.	.	kIx.	.
</s>
<s>
Vskutku	vskutku	k9	vskutku
-	-	kIx~	-
James	James	k1gMnSc1	James
vidí	vidět	k5eAaImIp3nS	vidět
<g/>
,	,	kIx,	,
že	že	k8xS	že
tomuto	tento	k3xDgMnSc3	tento
chlapci	chlapec	k1gMnSc3	chlapec
nebylo	být	k5eNaImAgNnS	být
ještě	ještě	k6eAd1	ještě
dvacet	dvacet	k4xCc1	dvacet
<g/>
.	.	kIx.	.
</s>
<s>
Dorian	Dorian	k1gInSc1	Dorian
je	být	k5eAaImIp3nS	být
opět	opět	k6eAd1	opět
zachráněn	zachráněn	k2eAgInSc1d1	zachráněn
podivným	podivný	k2eAgNnSc7d1	podivné
kouzlem	kouzlo	k1gNnSc7	kouzlo
jeho	on	k3xPp3gInSc2	on
portrétu	portrét	k1gInSc2	portrét
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Dorian	Dorian	k1gInSc1	Dorian
odejde	odejít	k5eAaPmIp3nS	odejít
pryč	pryč	k6eAd1	pryč
<g/>
,	,	kIx,	,
přijde	přijít	k5eAaPmIp3nS	přijít
k	k	k7c3	k
Jamesi	Jamese	k1gFnSc3	Jamese
Vaneovi	Vaneův	k2eAgMnPc1d1	Vaneův
ta	ten	k3xDgFnSc1	ten
žena	žena	k1gFnSc1	žena
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
Doriana	Doriana	k1gFnSc1	Doriana
oslovila	oslovit	k5eAaPmAgFnS	oslovit
jako	jako	k9	jako
"	"	kIx"	"
<g/>
Prince	princ	k1gMnSc4	princ
z	z	k7c2	z
pohádky	pohádka	k1gFnSc2	pohádka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ptá	ptat	k5eAaImIp3nS	ptat
se	se	k3xPyFc4	se
ho	on	k3xPp3gNnSc2	on
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
Doriana	Dorian	k1gMnSc4	Dorian
nezabil	zabít	k5eNaPmAgMnS	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
jí	on	k3xPp3gFnSc3	on
Vane	vanout	k5eAaImIp3nS	vanout
řekne	říct	k5eAaPmIp3nS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
nemohl	moct	k5eNaImAgMnS	moct
být	být	k5eAaImF	být
muž	muž	k1gMnSc1	muž
zodpovědný	zodpovědný	k2eAgMnSc1d1	zodpovědný
za	za	k7c4	za
smrt	smrt	k1gFnSc4	smrt
jeho	jeho	k3xOp3gFnSc2	jeho
sestry	sestra	k1gFnSc2	sestra
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
byl	být	k5eAaImAgMnS	být
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
příliš	příliš	k6eAd1	příliš
mlád	mlád	k2eAgMnSc1d1	mlád
<g/>
,	,	kIx,	,
žena	žena	k1gFnSc1	žena
se	se	k3xPyFc4	se
usměje	usmát	k5eAaPmIp3nS	usmát
<g/>
.	.	kIx.	.
</s>
<s>
Přísahala	přísahat	k5eAaImAgFnS	přísahat
Jamesovi	James	k1gMnSc3	James
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
skoro	skoro	k6eAd1	skoro
18	[number]	k4	18
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
co	co	k9	co
tohoto	tento	k3xDgMnSc4	tento
muže	muž	k1gMnSc4	muž
-	-	kIx~	-
Doriana	Doriana	k1gFnSc1	Doriana
Graye	Graye	k1gFnSc1	Graye
-	-	kIx~	-
poznala	poznat	k5eAaPmAgFnS	poznat
<g/>
.	.	kIx.	.
</s>
<s>
Že	že	k8xS	že
právě	právě	k9	právě
on	on	k3xPp3gMnSc1	on
ji	on	k3xPp3gFnSc4	on
dostal	dostat	k5eAaPmAgMnS	dostat
na	na	k7c4	na
pokraj	pokraj	k1gInSc4	pokraj
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
sám	sám	k3xTgMnSc1	sám
zaprodal	zaprodat	k5eAaPmAgMnS	zaprodat
svou	svůj	k3xOyFgFnSc4	svůj
duši	duše	k1gFnSc4	duše
ďáblu	ďábel	k1gMnSc3	ďábel
za	za	k7c4	za
pěknou	pěkná	k1gFnSc4	pěkná
a	a	k8xC	a
věčně	věčně	k6eAd1	věčně
mladou	mladý	k2eAgFnSc4d1	mladá
tvář	tvář	k1gFnSc4	tvář
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
týden	týden	k1gInSc4	týden
později	pozdě	k6eAd2	pozdě
sedí	sedit	k5eAaImIp3nS	sedit
Dorian	Dorian	k1gInSc1	Dorian
s	s	k7c7	s
Henrym	Henry	k1gMnSc7	Henry
a	a	k8xC	a
další	další	k2eAgFnSc7d1	další
smetánkou	smetánka	k1gFnSc7	smetánka
v	v	k7c6	v
zimní	zimní	k2eAgFnSc6d1	zimní
zahradě	zahrada	k1gFnSc6	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Vedou	vést	k5eAaImIp3nP	vést
spolu	spolu	k6eAd1	spolu
klasické	klasický	k2eAgFnPc1d1	klasická
lehce	lehko	k6eAd1	lehko
dekadentní	dekadentní	k2eAgFnPc1d1	dekadentní
řeči	řeč	k1gFnPc1	řeč
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
šlechty	šlechta	k1gFnSc2	šlechta
<g/>
.	.	kIx.	.
</s>
<s>
Dorian	Dorian	k1gInSc1	Dorian
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
v	v	k7c6	v
šoku	šok	k1gInSc6	šok
ze	z	k7c2	z
setkání	setkání	k1gNnSc2	setkání
s	s	k7c7	s
Jamesem	James	k1gMnSc7	James
Vanem	van	k1gInSc7	van
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
již	již	k6eAd1	již
tak	tak	k6eAd1	tak
blízko	blízko	k7c2	blízko
smrti	smrt	k1gFnSc2	smrt
<g/>
!	!	kIx.	!
</s>
<s>
Nebyl	být	k5eNaImAgMnS	být
toho	ten	k3xDgInSc2	ten
dne	den	k1gInSc2	den
příliš	příliš	k6eAd1	příliš
zábavným	zábavný	k2eAgMnSc7d1	zábavný
společníkem	společník	k1gMnSc7	společník
<g/>
.	.	kIx.	.
</s>
<s>
Omluvil	omluvit	k5eAaPmAgMnS	omluvit
se	se	k3xPyFc4	se
se	s	k7c7	s
slovy	slovo	k1gNnPc7	slovo
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
natrhat	natrhat	k5eAaBmF	natrhat
orchideje	orchidea	k1gFnPc4	orchidea
pro	pro	k7c4	pro
vévodkyni	vévodkyně	k1gFnSc4	vévodkyně
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
a	a	k8xC	a
s	s	k7c7	s
Henrym	Henry	k1gMnSc7	Henry
zrovna	zrovna	k9	zrovna
bavila	bavit	k5eAaImAgFnS	bavit
<g/>
.	.	kIx.	.
</s>
<s>
Dorian	Dorian	k1gMnSc1	Dorian
se	se	k3xPyFc4	se
dlouho	dlouho	k6eAd1	dlouho
nevrací	vracet	k5eNaImIp3nS	vracet
<g/>
.	.	kIx.	.
</s>
<s>
Náhle	náhle	k6eAd1	náhle
se	se	k3xPyFc4	se
ozval	ozvat	k5eAaPmAgMnS	ozvat
ze	z	k7c2	z
zahrady	zahrada	k1gFnSc2	zahrada
dušený	dušený	k2eAgInSc4d1	dušený
sten	sten	k1gInSc4	sten
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
se	se	k3xPyFc4	se
seběhli	seběhnout	k5eAaPmAgMnP	seběhnout
<g/>
.	.	kIx.	.
</s>
<s>
Dorian	Dorian	k1gMnSc1	Dorian
omdlel	omdlet	k5eAaPmAgMnS	omdlet
<g/>
.	.	kIx.	.
</s>
<s>
Odnesli	odnést	k5eAaPmAgMnP	odnést
jej	on	k3xPp3gInSc4	on
do	do	k7c2	do
modrého	modré	k1gNnSc2	modré
salónku	salónek	k1gInSc2	salónek
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
Dorian	Dorian	k1gInSc1	Dorian
probral	probrat	k5eAaPmAgInS	probrat
<g/>
,	,	kIx,	,
vzpomněl	vzpomnít	k5eAaPmAgInS	vzpomnít
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
spatřil	spatřit	k5eAaPmAgMnS	spatřit
za	za	k7c7	za
sklem	sklo	k1gNnSc7	sklo
skleníku	skleník	k1gInSc2	skleník
tvář	tvář	k1gFnSc1	tvář
Jamese	Jamese	k1gFnSc1	Jamese
Vanea	Vanea	k1gFnSc1	Vanea
<g/>
.	.	kIx.	.
</s>
<s>
Příštího	příští	k2eAgInSc2d1	příští
dne	den	k1gInSc2	den
nevyšel	vyjít	k5eNaPmAgInS	vyjít
z	z	k7c2	z
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
ba	ba	k9	ba
téměř	téměř	k6eAd1	téměř
ani	ani	k8xC	ani
z	z	k7c2	z
pokoje	pokoj	k1gInSc2	pokoj
ne	ne	k9	ne
<g/>
.	.	kIx.	.
</s>
<s>
Viděl	vidět	k5eAaImAgMnS	vidět
Jamese	Jamese	k1gFnPc4	Jamese
doopravdy	doopravdy	k6eAd1	doopravdy
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
ho	on	k3xPp3gMnSc4	on
zachvátilo	zachvátit	k5eAaPmAgNnS	zachvátit
šílenství	šílenství	k1gNnSc1	šílenství
<g/>
?	?	kIx.	?
</s>
<s>
Možná	možná	k9	možná
za	za	k7c4	za
vše	všechen	k3xTgNnSc4	všechen
může	moct	k5eAaImIp3nS	moct
jeho	jeho	k3xOp3gFnSc4	jeho
představivost	představivost	k1gFnSc4	představivost
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
co	co	k3yInSc1	co
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
by	by	kYmCp3nS	by
vlastně	vlastně	k9	vlastně
bylo	být	k5eAaImAgNnS	být
horší	zlý	k2eAgNnSc1d2	horší
<g/>
?	?	kIx.	?
</s>
<s>
Třetího	třetí	k4xOgInSc2	třetí
dne	den	k1gInSc2	den
se	se	k3xPyFc4	se
Dorian	Dorian	k1gMnSc1	Dorian
odvážil	odvážit	k5eAaPmAgMnS	odvážit
ven	ven	k6eAd1	ven
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
vévodkyní	vévodkyně	k1gFnSc7	vévodkyně
se	se	k3xPyFc4	se
vydali	vydat	k5eAaPmAgMnP	vydat
na	na	k7c4	na
lov	lov	k1gInSc4	lov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lese	les	k1gInSc6	les
se	se	k3xPyFc4	se
Dorian	Dorian	k1gMnSc1	Dorian
připojil	připojit	k5eAaPmAgMnS	připojit
ke	k	k7c3	k
svému	svůj	k3xOyFgMnSc3	svůj
příteli	přítel	k1gMnSc3	přítel
Geoffreymu	Geoffreym	k1gInSc2	Geoffreym
Cloustonovi	Clouston	k1gMnSc3	Clouston
<g/>
.	.	kIx.	.
</s>
<s>
Šli	jít	k5eAaImAgMnP	jít
spolu	spolu	k6eAd1	spolu
lesem	les	k1gInSc7	les
a	a	k8xC	a
vypadalo	vypadat	k5eAaPmAgNnS	vypadat
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
úlovek	úlovek	k1gInSc1	úlovek
bude	být	k5eAaImBp3nS	být
nevalný	valný	k2eNgInSc1d1	nevalný
<g/>
.	.	kIx.	.
</s>
<s>
Vtom	vtom	k6eAd1	vtom
však	však	k9	však
vyběhl	vyběhnout	k5eAaPmAgMnS	vyběhnout
před	před	k7c4	před
ně	on	k3xPp3gMnPc4	on
zajíc	zajíc	k1gMnSc1	zajíc
<g/>
.	.	kIx.	.
</s>
<s>
Geoffrey	Geoffre	k1gMnPc4	Geoffre
zamířil	zamířit	k5eAaPmAgInS	zamířit
<g/>
.	.	kIx.	.
</s>
<s>
Dorian	Dorian	k1gInSc1	Dorian
vykřikl	vykřiknout	k5eAaPmAgInS	vykřiknout
<g/>
.	.	kIx.	.
</s>
<s>
Neuměl	umět	k5eNaImAgMnS	umět
to	ten	k3xDgNnSc4	ten
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
něco	něco	k3yInSc1	něco
v	v	k7c6	v
ladných	ladný	k2eAgInPc6d1	ladný
pohybech	pohyb	k1gInPc6	pohyb
zvířete	zvíře	k1gNnSc2	zvíře
mu	on	k3xPp3gInSc3	on
učarovalo	učarovat	k5eAaPmAgNnS	učarovat
-	-	kIx~	-
chtěl	chtít	k5eAaImAgInS	chtít
zajíce	zajíc	k1gMnPc4	zajíc
ušetřit	ušetřit	k5eAaPmF	ušetřit
<g/>
.	.	kIx.	.
</s>
<s>
Zajíc	Zajíc	k1gMnSc1	Zajíc
vběhl	vběhnout	k5eAaPmAgMnS	vběhnout
do	do	k7c2	do
houští	houští	k1gNnSc2	houští
<g/>
.	.	kIx.	.
</s>
<s>
Sir	sir	k1gMnSc1	sir
Geoffrey	Geoffrea	k1gFnSc2	Geoffrea
vystřelil	vystřelit	k5eAaPmAgMnS	vystřelit
a	a	k8xC	a
-	-	kIx~	-
ozval	ozvat	k5eAaPmAgMnS	ozvat
se	se	k3xPyFc4	se
dvojí	dvojí	k4xRgInSc1	dvojí
skřek	skřek	k1gInSc1	skřek
<g/>
.	.	kIx.	.
</s>
<s>
Skřek	skřek	k1gInSc1	skřek
zajíce	zajíc	k1gMnSc2	zajíc
a	a	k8xC	a
sten	sten	k1gInSc4	sten
umírajícího	umírající	k2eAgMnSc2d1	umírající
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Lov	lov	k1gInSc1	lov
byl	být	k5eAaImAgInS	být
ukončen	ukončit	k5eAaPmNgInS	ukončit
<g/>
.	.	kIx.	.
</s>
<s>
Mrtvé	mrtvý	k2eAgNnSc1d1	mrtvé
tělo	tělo	k1gNnSc1	tělo
bylo	být	k5eAaImAgNnS	být
odneseno	odnést	k5eAaPmNgNnS	odnést
pryč	pryč	k6eAd1	pryč
<g/>
.	.	kIx.	.
</s>
<s>
Dorian	Dorian	k1gInSc1	Dorian
se	se	k3xPyFc4	se
cítí	cítit	k5eAaImIp3nS	cítit
neskutečně	skutečně	k6eNd1	skutečně
zle	zle	k6eAd1	zle
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
teď	teď	k6eAd1	teď
mělo	mít	k5eAaImAgNnS	mít
stát	stát	k5eAaImF	stát
i	i	k8xC	i
jemu	on	k3xPp3gMnSc3	on
něco	něco	k3yInSc1	něco
hrozného	hrozný	k2eAgNnSc2d1	hrozné
<g/>
.	.	kIx.	.
</s>
<s>
Smrt	smrt	k1gFnSc1	smrt
toho	ten	k3xDgMnSc4	ten
člověka	člověk	k1gMnSc4	člověk
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
zlým	zlý	k2eAgNnSc7d1	zlé
znamením	znamení	k1gNnSc7	znamení
<g/>
.	.	kIx.	.
</s>
<s>
Lord	lord	k1gMnSc1	lord
Henry	Henry	k1gMnSc1	Henry
se	se	k3xPyFc4	se
těmto	tento	k3xDgNnPc3	tento
jeho	jeho	k3xOp3gNnSc4	jeho
obavám	obava	k1gFnPc3	obava
cynicky	cynicky	k6eAd1	cynicky
vysměje	vysmát	k5eAaPmIp3nS	vysmát
<g/>
.	.	kIx.	.
</s>
<s>
Dorian	Dorian	k1gMnSc1	Dorian
se	se	k3xPyFc4	se
nějakou	nějaký	k3yIgFnSc4	nějaký
dobu	doba	k1gFnSc4	doba
trápí	trápit	k5eAaImIp3nS	trápit
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
je	být	k5eAaImIp3nS	být
tok	tok	k1gInSc1	tok
jeho	jeho	k3xOp3gFnPc2	jeho
myšlenek	myšlenka	k1gFnPc2	myšlenka
přerušen	přerušit	k5eAaPmNgInS	přerušit
<g/>
.	.	kIx.	.
</s>
<s>
Totožnost	totožnost	k1gFnSc1	totožnost
mrtvého	mrtvý	k2eAgMnSc2d1	mrtvý
muže	muž	k1gMnSc2	muž
je	být	k5eAaImIp3nS	být
totiž	totiž	k9	totiž
neznámá	známý	k2eNgFnSc1d1	neznámá
<g/>
.	.	kIx.	.
</s>
<s>
Nikdo	nikdo	k3yNnSc1	nikdo
ho	on	k3xPp3gNnSc4	on
tu	tu	k6eAd1	tu
nikdy	nikdy	k6eAd1	nikdy
neviděl	vidět	k5eNaImAgMnS	vidět
<g/>
.	.	kIx.	.
</s>
<s>
Dorian	Dorian	k1gInSc1	Dorian
spěchá	spěchat	k5eAaImIp3nS	spěchat
do	do	k7c2	do
hospodářských	hospodářský	k2eAgFnPc2d1	hospodářská
budov	budova	k1gFnPc2	budova
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
uloženo	uložit	k5eAaPmNgNnS	uložit
tělo	tělo	k1gNnSc1	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
James	James	k1gInSc4	James
Vane	vanout	k5eAaImIp3nS	vanout
<g/>
!	!	kIx.	!
</s>
<s>
Nalezl	naleznout	k5eAaPmAgInS	naleznout
se	se	k3xPyFc4	se
u	u	k7c2	u
něj	on	k3xPp3gMnSc2	on
šestiranný	šestiranný	k2eAgInSc1d1	šestiranný
revolver	revolver	k1gInSc1	revolver
<g/>
...	...	k?	...
Doriana	Doriana	k1gFnSc1	Doriana
zaplaví	zaplavit	k5eAaPmIp3nS	zaplavit
nová	nový	k2eAgFnSc1d1	nová
vlna	vlna	k1gFnSc1	vlna
radosti	radost	k1gFnSc2	radost
<g/>
.	.	kIx.	.
</s>
<s>
Strach	strach	k1gInSc1	strach
ze	z	k7c2	z
smrti	smrt	k1gFnSc2	smrt
rukou	ruka	k1gFnSc7	ruka
bratra	bratr	k1gMnSc2	bratr
jeho	jeho	k3xOp3gFnSc2	jeho
dávné	dávný	k2eAgFnSc2d1	dávná
lásky	láska	k1gFnSc2	láska
pominul	pominout	k5eAaPmAgMnS	pominout
<g/>
.	.	kIx.	.
</s>
<s>
Věděl	vědět	k5eAaImAgMnS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
zachráněn	zachráněn	k2eAgMnSc1d1	zachráněn
<g/>
.	.	kIx.	.
</s>
<s>
Dorian	Dorian	k1gInSc1	Dorian
se	se	k3xPyFc4	se
chce	chtít	k5eAaImIp3nS	chtít
polepšit	polepšit	k5eAaPmF	polepšit
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
to	ten	k3xDgNnSc1	ten
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mu	on	k3xPp3gMnSc3	on
již	již	k9	již
nebylo	být	k5eNaImAgNnS	být
pomoci	pomoct	k5eAaPmF	pomoct
<g/>
.	.	kIx.	.
</s>
<s>
Opustí	opustit	k5eAaPmIp3nS	opustit
venkovskou	venkovský	k2eAgFnSc4d1	venkovská
dívku	dívka	k1gFnSc4	dívka
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
ji	on	k3xPp3gFnSc4	on
zneuctil	zneuctít	k5eAaPmAgMnS	zneuctít
<g/>
.	.	kIx.	.
</s>
<s>
Provedl	provést	k5eAaPmAgMnS	provést
něco	něco	k3yInSc1	něco
<g/>
,	,	kIx,	,
o	o	k7c6	o
čem	co	k3yRnSc6	co
si	se	k3xPyFc3	se
myslí	myslet	k5eAaImIp3nS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
po	po	k7c6	po
letech	léto	k1gNnPc6	léto
první	první	k4xOgMnSc1	první
čestný	čestný	k2eAgInSc1d1	čestný
skutek	skutek	k1gInSc1	skutek
<g/>
.	.	kIx.	.
</s>
<s>
Vede	vést	k5eAaImIp3nS	vést
rozhovor	rozhovor	k1gInSc1	rozhovor
s	s	k7c7	s
Henrym	Henry	k1gMnSc7	Henry
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
při	při	k7c6	při
tom	ten	k3xDgNnSc6	ten
na	na	k7c4	na
klavír	klavír	k1gInSc4	klavír
<g/>
.	.	kIx.	.
</s>
<s>
Mluví	mluvit	k5eAaImIp3nS	mluvit
spolu	spolu	k6eAd1	spolu
i	i	k9	i
o	o	k7c6	o
Basilovi	Basil	k1gMnSc6	Basil
<g/>
.	.	kIx.	.
</s>
<s>
Domýšlí	domýšlet	k5eAaImIp3nS	domýšlet
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
s	s	k7c7	s
Basilem	Basil	k1gMnSc7	Basil
asi	asi	k9	asi
stalo	stát	k5eAaPmAgNnS	stát
<g/>
,	,	kIx,	,
doznávají	doznávat	k5eAaImIp3nP	doznávat
se	se	k3xPyFc4	se
ke	k	k7c3	k
vztahu	vztah	k1gInSc3	vztah
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
<g/>
.	.	kIx.	.
</s>
<s>
Dorian	Dorian	k1gMnSc1	Dorian
se	se	k3xPyFc4	se
zeptal	zeptat	k5eAaPmAgMnS	zeptat
Henryho	Henry	k1gMnSc2	Henry
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
by	by	kYmCp3nS	by
řekl	říct	k5eAaPmAgMnS	říct
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Basila	Basil	k1gMnSc4	Basil
zabil	zabít	k5eAaPmAgMnS	zabít
právě	právě	k6eAd1	právě
on	on	k3xPp3gMnSc1	on
<g/>
.	.	kIx.	.
</s>
<s>
Henry	Henry	k1gMnSc1	Henry
se	se	k3xPyFc4	se
vysměje	vysmát	k5eAaPmIp3nS	vysmát
<g/>
.	.	kIx.	.
</s>
<s>
Zločin	zločin	k1gInSc1	zločin
podle	podle	k7c2	podle
něj	on	k3xPp3gNnSc2	on
není	být	k5eNaImIp3nS	být
pro	pro	k7c4	pro
lidi	člověk	k1gMnPc4	člověk
z	z	k7c2	z
vysoké	vysoký	k2eAgFnSc2d1	vysoká
vrstvy	vrstva	k1gFnSc2	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nP	patřit
spodině	spodina	k1gFnSc3	spodina
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
zeptá	zeptat	k5eAaPmIp3nS	zeptat
Doriana	Doriana	k1gFnSc1	Doriana
na	na	k7c4	na
něco	něco	k3yInSc4	něco
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
přinutí	přinutit	k5eAaPmIp3nS	přinutit
muže	muž	k1gMnSc4	muž
u	u	k7c2	u
klavíru	klavír	k1gInSc2	klavír
přestat	přestat	k5eAaPmF	přestat
hrát	hrát	k5eAaImF	hrát
a	a	k8xC	a
zakončit	zakončit	k5eAaPmF	zakončit
vystoupení	vystoupení	k1gNnSc4	vystoupení
oním	onen	k3xDgInSc7	onen
neharmonickým	harmonický	k2eNgInSc7d1	neharmonický
tónem	tón	k1gInSc7	tón
<g/>
.	.	kIx.	.
</s>
<s>
Citace	citace	k1gFnPc1	citace
z	z	k7c2	z
knihy	kniha	k1gFnSc2	kniha
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jak	jak	k8xC	jak
to	ten	k3xDgNnSc1	ten
vlastně	vlastně	k9	vlastně
prospěje	prospět	k5eAaPmIp3nS	prospět
člověku	člověk	k1gMnSc3	člověk
<g/>
,	,	kIx,	,
když	když	k8xS	když
celý	celý	k2eAgInSc1d1	celý
svět	svět	k1gInSc1	svět
získá	získat	k5eAaPmIp3nS	získat
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c6	na
duši	duše	k1gFnSc6	duše
škodu	škoda	k1gFnSc4	škoda
utrpí	utrpět	k5eAaPmIp3nS	utrpět
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
Henry	Henry	k1gMnSc1	Henry
se	se	k3xPyFc4	se
ptá	ptat	k5eAaImIp3nS	ptat
spíše	spíše	k9	spíše
ironicky	ironicky	k6eAd1	ironicky
-	-	kIx~	-
sám	sám	k3xTgMnSc1	sám
v	v	k7c6	v
existenci	existence	k1gFnSc6	existence
duše	duše	k1gFnSc2	duše
nevěří	věřit	k5eNaImIp3nP	věřit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Dorian	Dorian	k1gMnSc1	Dorian
o	o	k7c6	o
ní	on	k3xPp3gFnSc6	on
ví	vědět	k5eAaImIp3nS	vědět
vše	všechen	k3xTgNnSc4	všechen
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nocích	noc	k1gFnPc6	noc
se	se	k3xPyFc4	se
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
dříve	dříve	k6eAd2	dříve
dívával	dívávat	k5eAaImAgMnS	dívávat
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
již	již	k6eAd1	již
neměl	mít	k5eNaImAgMnS	mít
odvahy	odvaha	k1gFnSc2	odvaha
<g/>
.	.	kIx.	.
</s>
<s>
Henry	henry	k1gInSc1	henry
Doriana	Dorian	k1gMnSc2	Dorian
povzbuzuje	povzbuzovat	k5eAaImIp3nS	povzbuzovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zůstal	zůstat	k5eAaPmAgInS	zůstat
takový	takový	k3xDgInSc1	takový
<g/>
,	,	kIx,	,
jaký	jaký	k3yRgInSc1	jaký
je	být	k5eAaImIp3nS	být
-	-	kIx~	-
symbolem	symbol	k1gInSc7	symbol
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
doba	doba	k1gFnSc1	doba
hledá	hledat	k5eAaImIp3nS	hledat
a	a	k8xC	a
toho	ten	k3xDgMnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
našla	najít	k5eAaPmAgFnS	najít
<g/>
.	.	kIx.	.
</s>
<s>
Dorian	Dorian	k1gMnSc1	Dorian
si	se	k3xPyFc3	se
uvědomuje	uvědomovat	k5eAaImIp3nS	uvědomovat
<g/>
,	,	kIx,	,
jaká	jaký	k3yRgFnSc1	jaký
stvůra	stvůra	k1gFnSc1	stvůra
se	se	k3xPyFc4	se
z	z	k7c2	z
něho	on	k3xPp3gMnSc2	on
stává	stávat	k5eAaImIp3nS	stávat
<g/>
.	.	kIx.	.
</s>
<s>
Chce	chtít	k5eAaImIp3nS	chtít
se	se	k3xPyFc4	se
vymanit	vymanit	k5eAaPmF	vymanit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vliv	vliv	k1gInSc1	vliv
lorda	lord	k1gMnSc2	lord
Henryho	Henry	k1gMnSc2	Henry
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc4	ten
nedovoluje	dovolovat	k5eNaImIp3nS	dovolovat
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
obraz	obraz	k1gInSc1	obraz
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc4	ten
nedovoluje	dovolovat	k5eNaImIp3nS	dovolovat
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
duše	duše	k1gFnSc1	duše
je	být	k5eAaImIp3nS	být
otrávená	otrávený	k2eAgFnSc1d1	otrávená
<g/>
.	.	kIx.	.
</s>
<s>
Jakýkoli	jakýkoli	k3yIgInSc1	jakýkoli
pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
morálku	morálka	k1gFnSc4	morálka
je	být	k5eAaImIp3nS	být
lordem	lord	k1gMnSc7	lord
Henrym	Henry	k1gMnSc7	Henry
odmítnut	odmítnout	k5eAaPmNgInS	odmítnout
a	a	k8xC	a
zesměšněn	zesměšnit	k5eAaPmNgInS	zesměšnit
<g/>
.	.	kIx.	.
</s>
<s>
Dorian	Dorian	k1gInSc1	Dorian
odchází	odcházet	k5eAaImIp3nS	odcházet
z	z	k7c2	z
místnosti	místnost	k1gFnSc2	místnost
<g/>
.	.	kIx.	.
</s>
<s>
Oznamuje	oznamovat	k5eAaImIp3nS	oznamovat
Henrymu	Henry	k1gMnSc3	Henry
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
unaven	unavit	k5eAaPmNgMnS	unavit
<g/>
.	.	kIx.	.
</s>
<s>
Henry	henry	k1gInSc1	henry
jej	on	k3xPp3gMnSc4	on
požádá	požádat	k5eAaPmIp3nS	požádat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
u	u	k7c2	u
něj	on	k3xPp3gMnSc2	on
stavil	stavit	k5eAaBmAgMnS	stavit
dalšího	další	k2eAgInSc2d1	další
dne	den	k1gInSc2	den
v	v	k7c4	v
jedenáct	jedenáct	k4xCc4	jedenáct
hodin	hodina	k1gFnPc2	hodina
a	a	k8xC	a
Dorian	Doriany	k1gInPc2	Doriany
s	s	k7c7	s
těžkým	těžký	k2eAgNnSc7d1	těžké
srdcem	srdce	k1gNnSc7	srdce
přijímá	přijímat	k5eAaImIp3nS	přijímat
<g/>
.	.	kIx.	.
</s>
<s>
Dorian	Dorian	k1gInSc1	Dorian
kráčí	kráčet	k5eAaImIp3nS	kráčet
domů	domů	k6eAd1	domů
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
příjemný	příjemný	k2eAgInSc4d1	příjemný
večer	večer	k1gInSc4	večer
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgMnPc1	dva
lidé	člověk	k1gMnPc1	člověk
si	se	k3xPyFc3	se
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
-	-	kIx~	-
jak	jak	k8xC	jak
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc1	ten
dříve	dříve	k6eAd2	dříve
bývalo	bývat	k5eAaImAgNnS	bývat
příjemné	příjemný	k2eAgNnSc1d1	příjemné
<g/>
...	...	k?	...
Dorazil	dorazit	k5eAaPmAgInS	dorazit
<g/>
.	.	kIx.	.
</s>
<s>
Opravdu	opravdu	k6eAd1	opravdu
se	se	k3xPyFc4	se
nemůže	moct	k5eNaImIp3nS	moct
změnit	změnit	k5eAaPmF	změnit
<g/>
?	?	kIx.	?
</s>
<s>
Pošpinil	pošpinit	k5eAaPmAgInS	pošpinit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
přesytil	přesytit	k5eAaPmAgInS	přesytit
se	se	k3xPyFc4	se
zkažeností	zkaženost	k1gFnSc7	zkaženost
a	a	k8xC	a
měl	mít	k5eAaImAgInS	mít
špatný	špatný	k2eAgInSc4d1	špatný
vliv	vliv	k1gInSc4	vliv
právě	právě	k9	právě
na	na	k7c4	na
ty	ten	k3xDgMnPc4	ten
nejlepší	dobrý	k2eAgMnPc4d3	nejlepší
a	a	k8xC	a
nejčistší	čistý	k2eAgMnPc4d3	nejčistší
lidi	člověk	k1gMnPc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Proč	proč	k6eAd1	proč
jen	jen	k9	jen
tehdy	tehdy	k6eAd1	tehdy
pronesl	pronést	k5eAaPmAgMnS	pronést
tu	ten	k3xDgFnSc4	ten
modlitbu	modlitba	k1gFnSc4	modlitba
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jeho	jeho	k3xOp3gInPc4	jeho
hříchy	hřích	k1gInPc4	hřích
nesl	nést	k5eAaImAgInS	nést
portrét	portrét	k1gInSc1	portrét
<g/>
?	?	kIx.	?
</s>
<s>
Lépe	dobře	k6eAd2	dobře
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nS	kdyby
mu	on	k3xPp3gMnSc3	on
každý	každý	k3xTgInSc4	každý
hřích	hřích	k1gInSc4	hřích
vynesl	vynést	k5eAaPmAgMnS	vynést
odplatu	odplata	k1gFnSc4	odplata
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
trestu	trest	k1gInSc6	trest
je	být	k5eAaImIp3nS	být
očista	očista	k1gFnSc1	očista
<g/>
.	.	kIx.	.
</s>
<s>
Dorian	Dorian	k1gMnSc1	Dorian
se	se	k3xPyFc4	se
podíval	podívat	k5eAaPmAgMnS	podívat
do	do	k7c2	do
zrcadla	zrcadlo	k1gNnSc2	zrcadlo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
mu	on	k3xPp3gMnSc3	on
kdysi	kdysi	k6eAd1	kdysi
dal	dát	k5eAaPmAgMnS	dát
Henry	Henry	k1gMnSc1	Henry
<g/>
.	.	kIx.	.
</s>
<s>
Rozbil	rozbít	k5eAaPmAgMnS	rozbít
ho	on	k3xPp3gMnSc4	on
a	a	k8xC	a
rozšlapal	rozšlapat	k5eAaPmAgInS	rozšlapat
<g/>
.	.	kIx.	.
</s>
<s>
Nechtěl	chtít	k5eNaImAgMnS	chtít
myslet	myslet	k5eAaImF	myslet
na	na	k7c4	na
minulost	minulost	k1gFnSc4	minulost
<g/>
.	.	kIx.	.
</s>
<s>
James	James	k1gMnSc1	James
Vane	vanout	k5eAaImIp3nS	vanout
je	on	k3xPp3gNnSc4	on
pohřben	pohřben	k2eAgMnSc1d1	pohřben
<g/>
,	,	kIx,	,
Alan	Alan	k1gMnSc1	Alan
Campbell	Campbell	k1gMnSc1	Campbell
se	se	k3xPyFc4	se
jedné	jeden	k4xCgFnSc6	jeden
noci	noc	k1gFnSc6	noc
zastřelil	zastřelit	k5eAaPmAgMnS	zastřelit
v	v	k7c6	v
laboratoři	laboratoř	k1gFnSc6	laboratoř
<g/>
,	,	kIx,	,
smrt	smrt	k1gFnSc1	smrt
Basila	Basila	k1gFnSc1	Basila
jej	on	k3xPp3gMnSc4	on
nemůže	moct	k5eNaImIp3nS	moct
tížit	tížit	k5eAaImF	tížit
-	-	kIx~	-
vždyť	vždyť	k9	vždyť
to	ten	k3xDgNnSc1	ten
on	on	k3xPp3gMnSc1	on
namaloval	namalovat	k5eAaPmAgMnS	namalovat
ten	ten	k3xDgInSc4	ten
proklatý	proklatý	k2eAgInSc4d1	proklatý
portrét	portrét	k1gInSc4	portrét
<g/>
.	.	kIx.	.
</s>
<s>
Začne	začít	k5eAaPmIp3nS	začít
nový	nový	k2eAgInSc4d1	nový
život	život	k1gInSc4	život
<g/>
!	!	kIx.	!
</s>
<s>
přece	přece	k9	přece
se	se	k3xPyFc4	se
zachoval	zachovat	k5eAaPmAgInS	zachovat
šetrně	šetrně	k6eAd1	šetrně
k	k	k7c3	k
té	ten	k3xDgFnSc3	ten
vesnické	vesnický	k2eAgFnSc3d1	vesnická
dívence	dívenka	k1gFnSc3	dívenka
<g/>
.	.	kIx.	.
</s>
<s>
Bude	být	k5eAaImBp3nS	být
už	už	k6eAd1	už
dobrý	dobrý	k2eAgInSc1d1	dobrý
<g/>
!	!	kIx.	!
</s>
<s>
Ale	ale	k9	ale
počkat	počkat	k5eAaPmF	počkat
-	-	kIx~	-
změnil	změnit	k5eAaPmAgInS	změnit
se	se	k3xPyFc4	se
portrét	portrét	k1gInSc1	portrét
provedením	provedení	k1gNnSc7	provedení
toho	ten	k3xDgInSc2	ten
dobrého	dobrý	k2eAgInSc2d1	dobrý
skutku	skutek	k1gInSc2	skutek
<g/>
?	?	kIx.	?
</s>
<s>
Už	už	k6eAd1	už
přece	přece	k9	přece
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
hrozný	hrozný	k2eAgInSc1d1	hrozný
jako	jako	k9	jako
dřív	dříve	k6eAd2	dříve
<g/>
!	!	kIx.	!
</s>
<s>
Kdyby	kdyby	kYmCp3nS	kdyby
žil	žít	k5eAaImAgMnS	žít
odteď	odtedit	k5eAaPmRp2nS	odtedit
mravně	mravně	k6eAd1	mravně
<g/>
,	,	kIx,	,
třeba	třeba	k6eAd1	třeba
by	by	kYmCp3nS	by
vypudil	vypudit	k5eAaPmAgMnS	vypudit
z	z	k7c2	z
tváře	tvář	k1gFnSc2	tvář
všechny	všechen	k3xTgInPc4	všechen
zbytky	zbytek	k1gInPc4	zbytek
zlých	zlý	k2eAgFnPc2d1	zlá
vášní	vášeň	k1gFnPc2	vášeň
<g/>
...	...	k?	...
Plížil	plížil	k1gMnSc1	plížil
se	se	k3xPyFc4	se
nahoru	nahoru	k6eAd1	nahoru
do	do	k7c2	do
studovny	studovna	k1gFnSc2	studovna
<g/>
.	.	kIx.	.
</s>
<s>
Vešel	vejít	k5eAaPmAgMnS	vejít
<g/>
,	,	kIx,	,
zamkl	zamknout	k5eAaPmAgMnS	zamknout
za	za	k7c7	za
sebou	se	k3xPyFc7	se
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
měl	mít	k5eAaImAgMnS	mít
ve	v	k7c6	v
zvyku	zvyk	k1gInSc6	zvyk
<g/>
.	.	kIx.	.
</s>
<s>
Odkryl	odkrýt	k5eAaPmAgInS	odkrýt
závěs	závěs	k1gInSc1	závěs
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hrdla	hrdlo	k1gNnSc2	hrdlo
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
vydral	vydrat	k5eAaPmAgInS	vydrat
výkřik	výkřik	k1gInSc1	výkřik
děsu	děs	k1gInSc2	děs
<g/>
.	.	kIx.	.
</s>
<s>
Portrét	portrét	k1gInSc1	portrét
se	se	k3xPyFc4	se
k	k	k7c3	k
lepšímu	lepší	k1gNnSc3	lepší
nezměnil	změnit	k5eNaPmAgInS	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
<g/>
.	.	kIx.	.
</s>
<s>
Oči	oko	k1gNnPc1	oko
se	se	k3xPyFc4	se
dívaly	dívat	k5eAaImAgInP	dívat
zchytrale	zchytrale	k6eAd1	zchytrale
a	a	k8xC	a
kolem	kolem	k7c2	kolem
úst	ústa	k1gNnPc2	ústa
měl	mít	k5eAaImAgMnS	mít
vrásky	vráska	k1gFnPc4	vráska
pokrytectví	pokrytectví	k1gNnSc2	pokrytectví
<g/>
.	.	kIx.	.
</s>
<s>
Uvědomil	uvědomit	k5eAaPmAgInS	uvědomit
si	se	k3xPyFc3	se
pravdu	pravda	k1gFnSc4	pravda
<g/>
.	.	kIx.	.
</s>
<s>
Dívku	dívka	k1gFnSc4	dívka
ušetřil	ušetřit	k5eAaPmAgMnS	ušetřit
jen	jen	k9	jen
z	z	k7c2	z
ješitnosti	ješitnost	k1gFnSc2	ješitnost
<g/>
,	,	kIx,	,
jen	jen	k9	jen
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
poznal	poznat	k5eAaPmAgMnS	poznat
nové	nový	k2eAgNnSc4d1	nové
vzrušení	vzrušení	k1gNnSc4	vzrušení
<g/>
.	.	kIx.	.
</s>
<s>
Portrét	portrét	k1gInSc1	portrét
byl	být	k5eAaImAgInS	být
stále	stále	k6eAd1	stále
potřísněn	potřísnit	k5eAaPmNgInS	potřísnit
krví	krev	k1gFnSc7	krev
-	-	kIx~	-
navíc	navíc	k6eAd1	navíc
zřetelněji	zřetelně	k6eAd2	zřetelně
než	než	k8xS	než
kdy	kdy	k6eAd1	kdy
předtím	předtím	k6eAd1	předtím
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
se	se	k3xPyFc4	se
snad	snad	k9	snad
ke	k	k7c3	k
všemu	všecek	k3xTgNnSc3	všecek
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
spáchal	spáchat	k5eAaPmAgMnS	spáchat
<g/>
,	,	kIx,	,
přiznat	přiznat	k5eAaPmF	přiznat
<g/>
?	?	kIx.	?
</s>
<s>
Nikdo	nikdo	k3yNnSc1	nikdo
by	by	kYmCp3nS	by
mu	on	k3xPp3gMnSc3	on
neuvěřil	uvěřit	k5eNaPmAgMnS	uvěřit
<g/>
.	.	kIx.	.
</s>
<s>
Basilovo	Basilův	k2eAgNnSc1d1	Basilův
tělo	tělo	k1gNnSc1	tělo
je	být	k5eAaImIp3nS	být
zničeno	zničen	k2eAgNnSc1d1	zničeno
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
jeho	jeho	k3xOp3gFnPc1	jeho
věci	věc	k1gFnPc1	věc
<g/>
...	...	k?	...
Jediné	jediný	k2eAgNnSc1d1	jediné
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
proti	proti	k7c3	proti
němu	on	k3xPp3gNnSc3	on
svědčí	svědčit	k5eAaImIp3nS	svědčit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jeho	on	k3xPp3gInSc4	on
portrét	portrét	k1gInSc4	portrét
<g/>
.	.	kIx.	.
</s>
<s>
Ano	ano	k9	ano
-	-	kIx~	-
proč	proč	k6eAd1	proč
se	se	k3xPyFc4	se
i	i	k9	i
jeho	on	k3xPp3gInSc2	on
nezbavit	zbavit	k5eNaPmF	zbavit
<g/>
?	?	kIx.	?
</s>
<s>
Portrét	portrét	k1gInSc1	portrét
je	být	k5eAaImIp3nS	být
obraz	obraz	k1gInSc4	obraz
jeho	jeho	k3xOp3gNnSc2	jeho
svědomí	svědomí	k1gNnSc2	svědomí
-	-	kIx~	-
zničí	zničit	k5eAaPmIp3nS	zničit
ho	on	k3xPp3gMnSc4	on
tedy	tedy	k9	tedy
<g/>
!	!	kIx.	!
</s>
<s>
Vzal	vzít	k5eAaPmAgInS	vzít
nůž	nůž	k1gInSc1	nůž
kterým	který	k3yIgMnPc3	který
probodl	probodnout	k5eAaPmAgInS	probodnout
Basila	Basil	k1gMnSc4	Basil
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
čistý	čistý	k2eAgInSc1d1	čistý
<g/>
,	,	kIx,	,
několikrát	několikrát	k6eAd1	několikrát
ho	on	k3xPp3gMnSc4	on
očistil	očistit	k5eAaPmAgMnS	očistit
<g/>
.	.	kIx.	.
</s>
<s>
Probodl	probodnout	k5eAaPmAgMnS	probodnout
jím	jíst	k5eAaImIp1nS	jíst
obraz	obraz	k1gInSc4	obraz
<g/>
...	...	k?	...
Bylo	být	k5eAaImAgNnS	být
slyšet	slyšet	k5eAaImF	slyšet
výkřik	výkřik	k1gInSc4	výkřik
a	a	k8xC	a
zadunění	zadunění	k1gNnSc4	zadunění
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
výkřiku	výkřik	k1gInSc6	výkřik
zazněl	zaznět	k5eAaImAgInS	zaznět
takový	takový	k3xDgInSc1	takový
děs	děs	k1gInSc1	děs
ze	z	k7c2	z
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
že	že	k8xS	že
sluhové	sluha	k1gMnPc1	sluha
se	se	k3xPyFc4	se
s	s	k7c7	s
leknutím	leknutí	k1gNnSc7	leknutí
probudili	probudit	k5eAaPmAgMnP	probudit
a	a	k8xC	a
vyklouzli	vyklouznout	k5eAaPmAgMnP	vyklouznout
ze	z	k7c2	z
svých	svůj	k3xOyFgInPc2	svůj
pokojů	pokoj	k1gInPc2	pokoj
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgMnPc1	dva
páni	pan	k1gMnPc1	pan
<g/>
,	,	kIx,	,
kráčející	kráčející	k2eAgMnPc1d1	kráčející
dole	dole	k6eAd1	dole
po	po	k7c6	po
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zastavili	zastavit	k5eAaPmAgMnP	zastavit
a	a	k8xC	a
zvedli	zvednout	k5eAaPmAgMnP	zvednout
oči	oko	k1gNnPc4	oko
k	k	k7c3	k
tomu	ten	k3xDgInSc3	ten
velkému	velký	k2eAgInSc3d1	velký
domu	dům	k1gInSc3	dům
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
šli	jít	k5eAaImAgMnP	jít
dál	daleko	k6eAd2	daleko
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
nepotkali	potkat	k5eNaPmAgMnP	potkat
policistu	policista	k1gMnSc4	policista
<g/>
,	,	kIx,	,
a	a	k8xC	a
vrátili	vrátit	k5eAaPmAgMnP	vrátit
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Policista	policista	k1gMnSc1	policista
několikrát	několikrát	k6eAd1	několikrát
zazvonil	zazvonit	k5eAaPmAgMnS	zazvonit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nikdo	nikdo	k3yNnSc1	nikdo
nepřišel	přijít	k5eNaPmAgMnS	přijít
otevřít	otevřít	k5eAaPmF	otevřít
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
na	na	k7c4	na
světlo	světlo	k1gNnSc4	světlo
v	v	k7c6	v
jednom	jeden	k4xCgNnSc6	jeden
z	z	k7c2	z
nejhořejších	horní	k2eAgNnPc2d3	nejhořejší
oken	okno	k1gNnPc2	okno
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
domě	dům	k1gInSc6	dům
tma	tma	k6eAd1	tma
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
chvíli	chvíle	k1gFnSc6	chvíle
strážník	strážník	k1gMnSc1	strážník
odešel	odejít	k5eAaPmAgMnS	odejít
<g/>
,	,	kIx,	,
postavil	postavit	k5eAaPmAgMnS	postavit
se	se	k3xPyFc4	se
do	do	k7c2	do
sousedního	sousední	k2eAgInSc2d1	sousední
portálu	portál	k1gInSc2	portál
a	a	k8xC	a
vyčkával	vyčkávat	k5eAaImAgMnS	vyčkávat
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Čí	čí	k3xOyRgNnSc1	čí
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
dům	dům	k1gInSc1	dům
<g/>
,	,	kIx,	,
konstáble	konstábl	k1gMnSc5	konstábl
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
ptal	ptat	k5eAaImAgMnS	ptat
se	se	k3xPyFc4	se
starší	starší	k1gMnSc1	starší
z	z	k7c2	z
pánů	pan	k1gMnPc2	pan
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Pana	Pan	k1gMnSc2	Pan
Doriana	Dorian	k1gMnSc2	Dorian
Graye	Gray	k1gMnSc2	Gray
<g/>
,	,	kIx,	,
pane	pan	k1gMnSc5	pan
<g/>
"	"	kIx"	"
odvětil	odvětit	k5eAaPmAgMnS	odvětit
policista	policista	k1gMnSc1	policista
<g/>
.	.	kIx.	.
</s>
<s>
Páni	pan	k1gMnPc1	pan
se	se	k3xPyFc4	se
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
podívali	podívat	k5eAaPmAgMnP	podívat
<g/>
,	,	kIx,	,
ušklíbli	ušklíbnout	k5eAaPmAgMnP	ušklíbnout
se	se	k3xPyFc4	se
a	a	k8xC	a
vykročili	vykročit	k5eAaPmAgMnP	vykročit
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
byl	být	k5eAaImAgMnS	být
strýc	strýc	k1gMnSc1	strýc
sira	sir	k1gMnSc2	sir
Henryho	Henry	k1gMnSc2	Henry
Ashtona	Ashton	k1gMnSc2	Ashton
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k6eAd1	uvnitř
v	v	k7c6	v
domě	dům	k1gInSc6	dům
<g/>
,	,	kIx,	,
v	v	k7c6	v
křídle	křídlo	k1gNnSc6	křídlo
pro	pro	k7c4	pro
personál	personál	k1gInSc4	personál
<g/>
,	,	kIx,	,
hovořili	hovořit	k5eAaImAgMnP	hovořit
spolu	spolu	k6eAd1	spolu
tichým	tichý	k2eAgInSc7d1	tichý
šepotem	šepot	k1gInSc7	šepot
polooblečení	polooblečený	k2eAgMnPc1d1	polooblečený
sluhové	sluha	k1gMnPc1	sluha
<g/>
.	.	kIx.	.
</s>
<s>
Stará	starý	k2eAgFnSc1d1	stará
paní	paní	k1gFnSc1	paní
Leafová	Leafový	k2eAgFnSc1d1	Leafový
plakala	plakat	k5eAaImAgFnS	plakat
a	a	k8xC	a
lomila	lomit	k5eAaImAgFnS	lomit
rukama	ruka	k1gFnPc7	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Francis	Francis	k1gInSc1	Francis
byl	být	k5eAaImAgInS	být
smrtelně	smrtelně	k6eAd1	smrtelně
bledý	bledý	k2eAgInSc1d1	bledý
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
po	po	k7c6	po
čtvrthodině	čtvrthodina	k1gFnSc6	čtvrthodina
si	se	k3xPyFc3	se
přibrali	přibrat	k5eAaPmAgMnP	přibrat
kočího	kočí	k1gMnSc4	kočí
a	a	k8xC	a
jednoho	jeden	k4xCgMnSc2	jeden
lokaje	lokaj	k1gMnSc2	lokaj
a	a	k8xC	a
kradli	krást	k5eAaImAgMnP	krást
se	se	k3xPyFc4	se
nahoru	nahoru	k6eAd1	nahoru
<g/>
.	.	kIx.	.
</s>
<s>
Zaklepali	zaklepat	k5eAaPmAgMnP	zaklepat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neozvala	ozvat	k5eNaPmAgFnS	ozvat
se	se	k3xPyFc4	se
žádná	žádný	k3yNgFnSc1	žádný
odpověď	odpověď	k1gFnSc1	odpověď
<g/>
.	.	kIx.	.
</s>
<s>
Zavolali	zavolat	k5eAaPmAgMnP	zavolat
<g/>
.	.	kIx.	.
</s>
<s>
Všude	všude	k6eAd1	všude
bylo	být	k5eAaImAgNnS	být
ticho	ticho	k1gNnSc1	ticho
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
<g/>
,	,	kIx,	,
po	po	k7c6	po
marných	marný	k2eAgInPc6d1	marný
pokusech	pokus	k1gInPc6	pokus
vypáčit	vypáčit	k5eAaPmF	vypáčit
dveře	dveře	k1gFnPc4	dveře
<g/>
,	,	kIx,	,
vylezli	vylézt	k5eAaPmAgMnP	vylézt
na	na	k7c4	na
střechu	střecha	k1gFnSc4	střecha
a	a	k8xC	a
spustili	spustit	k5eAaPmAgMnP	spustit
se	se	k3xPyFc4	se
na	na	k7c4	na
balkón	balkón	k1gInSc4	balkón
<g/>
.	.	kIx.	.
</s>
<s>
Dveře	dveře	k1gFnPc1	dveře
lehce	lehko	k6eAd1	lehko
povolily	povolit	k5eAaPmAgFnP	povolit
<g/>
;	;	kIx,	;
zástrčky	zástrčka	k1gFnPc1	zástrčka
na	na	k7c6	na
nich	on	k3xPp3gInPc6	on
byly	být	k5eAaImAgFnP	být
už	už	k6eAd1	už
staré	starý	k2eAgFnPc1d1	stará
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
vstoupili	vstoupit	k5eAaPmAgMnP	vstoupit
do	do	k7c2	do
místnosti	místnost	k1gFnSc2	místnost
<g/>
,	,	kIx,	,
spatřili	spatřit	k5eAaPmAgMnP	spatřit
na	na	k7c6	na
stěně	stěna	k1gFnSc6	stěna
skvostný	skvostný	k2eAgInSc4d1	skvostný
portrét	portrét	k1gInSc4	portrét
svého	svůj	k3xOyFgMnSc2	svůj
pána	pán	k1gMnSc2	pán
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
ho	on	k3xPp3gMnSc4	on
naposledy	naposledy	k6eAd1	naposledy
viděli	vidět	k5eAaImAgMnP	vidět
<g/>
,	,	kIx,	,
se	s	k7c7	s
vší	všecek	k3xTgFnSc7	všecek
tou	ten	k3xDgFnSc7	ten
podivuhodnou	podivuhodný	k2eAgFnSc7d1	podivuhodná
nádherou	nádhera	k1gFnSc7	nádhera
jeho	jeho	k3xOp3gFnSc2	jeho
mladosti	mladost	k1gFnSc2	mladost
a	a	k8xC	a
krásy	krása	k1gFnSc2	krása
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
podlaze	podlaha	k1gFnSc6	podlaha
ležel	ležet	k5eAaImAgMnS	ležet
mrtvý	mrtvý	k2eAgMnSc1d1	mrtvý
muž	muž	k1gMnSc1	muž
ve	v	k7c6	v
večerním	večerní	k2eAgInSc6d1	večerní
úboru	úbor	k1gInSc6	úbor
a	a	k8xC	a
s	s	k7c7	s
nožem	nůž	k1gInSc7	nůž
v	v	k7c6	v
srdci	srdce	k1gNnSc6	srdce
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
povadlý	povadlý	k2eAgInSc1d1	povadlý
<g/>
,	,	kIx,	,
vrásčitý	vrásčitý	k2eAgInSc1d1	vrásčitý
a	a	k8xC	a
měl	mít	k5eAaImAgInS	mít
odporný	odporný	k2eAgInSc4d1	odporný
obličej	obličej	k1gInSc4	obličej
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
<g/>
,	,	kIx,	,
když	když	k8xS	když
si	se	k3xPyFc3	se
prohlédli	prohlédnout	k5eAaPmAgMnP	prohlédnout
jeho	jeho	k3xOp3gInPc4	jeho
prsteny	prsten	k1gInPc4	prsten
<g/>
,	,	kIx,	,
poznali	poznat	k5eAaPmAgMnP	poznat
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
to	ten	k3xDgNnSc4	ten
je	být	k5eAaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
románu	román	k1gInSc2	román
bylo	být	k5eAaImAgNnS	být
natočeno	natočit	k5eAaBmNgNnS	natočit
několik	několik	k4yIc1	několik
filmových	filmový	k2eAgFnPc2d1	filmová
adaptací	adaptace	k1gFnPc2	adaptace
a	a	k8xC	a
napsán	napsán	k2eAgInSc4d1	napsán
muzikál	muzikál	k1gInSc4	muzikál
<g/>
.	.	kIx.	.
</s>
<s>
Portrét	portrét	k1gInSc1	portrét
Doriana	Dorian	k1gMnSc2	Dorian
Graye	Gray	k1gMnSc2	Gray
-	-	kIx~	-
československé	československý	k2eAgNnSc1d1	Československé
drama	drama	k1gNnSc1	drama
režiséra	režisér	k1gMnSc2	režisér
Pavola	Pavola	k1gFnSc1	Pavola
Haspra	Haspra	k1gFnSc1	Haspra
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
<g/>
.	.	kIx.	.
</s>
<s>
Portrét	portrét	k1gInSc1	portrét
Doriana	Dorian	k1gMnSc2	Dorian
Graye	Gray	k1gMnSc2	Gray
-	-	kIx~	-
československé	československý	k2eAgNnSc1d1	Československé
drama	drama	k1gNnSc1	drama
režiséra	režisér	k1gMnSc2	režisér
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Strniska	strnisko	k1gNnSc2	strnisko
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1988	[number]	k4	1988
<g/>
.	.	kIx.	.
</s>
<s>
Obraz	obraz	k1gInSc1	obraz
Doriana	Dorian	k1gMnSc2	Dorian
Graye	Gray	k1gMnSc2	Gray
(	(	kIx(	(
<g/>
orig	orig	k1gMnSc1	orig
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Picture	Pictur	k1gMnSc5	Pictur
of	of	k?	of
Dorian	Dorian	k1gInSc4	Dorian
Gray	Graa	k1gFnSc2	Graa
<g/>
)	)	kIx)	)
-	-	kIx~	-
americké	americký	k2eAgNnSc1d1	americké
drama	drama	k1gNnSc1	drama
Davida	David	k1gMnSc2	David
Rosenbauma	Rosenbaum	k1gMnSc2	Rosenbaum
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Dorian	Dorian	k1gInSc1	Dorian
Gray	Graa	k1gFnSc2	Graa
-	-	kIx~	-
britské	britský	k2eAgNnSc1d1	Britské
drama	drama	k1gNnSc1	drama
režiséra	režisér	k1gMnSc2	režisér
Olivera	Oliver	k1gMnSc2	Oliver
Parkera	Parker	k1gMnSc2	Parker
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Obraz	obraz	k1gInSc1	obraz
Doriana	Dorian	k1gMnSc2	Dorian
Graye	Gray	k1gMnSc2	Gray
-	-	kIx~	-
původní	původní	k2eAgInSc1d1	původní
český	český	k2eAgInSc1d1	český
muzikál	muzikál	k1gInSc1	muzikál
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
napsaný	napsaný	k2eAgInSc4d1	napsaný
na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
knihy	kniha	k1gFnSc2	kniha
<g/>
.	.	kIx.	.
</s>
