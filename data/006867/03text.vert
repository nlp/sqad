<s>
Kanton	Kanton	k1gInSc1	Kanton
Yerville	Yerville	k1gFnSc2	Yerville
(	(	kIx(	(
<g/>
fr.	fr.	k?	fr.
Canton	Canton	k1gInSc1	Canton
de	de	k?	de
Yerville	Yerville	k1gInSc1	Yerville
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
francouzský	francouzský	k2eAgInSc4d1	francouzský
kanton	kanton	k1gInSc4	kanton
v	v	k7c6	v
departementu	departement	k1gInSc6	departement
Seine-Maritime	Seine-Maritim	k1gInSc5	Seine-Maritim
v	v	k7c6	v
regionu	region	k1gInSc6	region
Horní	horní	k2eAgFnSc2d1	horní
Normandie	Normandie	k1gFnSc2	Normandie
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
18	[number]	k4	18
obcí	obec	k1gFnPc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
Ancretiéville-Saint-Victor	Ancretiéville-Saint-Victor	k1gMnSc1	Ancretiéville-Saint-Victor
Auzouville-l	Auzouville	k1gMnSc1	Auzouville-l
<g/>
'	'	kIx"	'
<g/>
Esneval	Esneval	k1gFnSc1	Esneval
Bourdainville	Bourdainvill	k1gMnSc2	Bourdainvill
Cideville	Cidevill	k1gMnSc2	Cidevill
Criquetot-sur-Ouville	Criquetotur-Ouvill	k1gMnSc2	Criquetot-sur-Ouvill
Ectot-l	Ectot	k1gMnSc1	Ectot-l
<g/>
'	'	kIx"	'
<g/>
Auber	Auber	k1gMnSc1	Auber
Ectot-lè	Ectotè	k1gFnSc2	Ectot-lè
Étoutteville	Étoutteville	k1gFnSc1	Étoutteville
Flamanville	Flamanville	k1gFnSc1	Flamanville
Grémonville	Grémonville	k1gFnSc1	Grémonville
Hugleville-en-Caux	Huglevillen-Caux	k1gInSc4	Hugleville-en-Caux
Lindebeuf	Lindebeuf	k1gInSc1	Lindebeuf
Motteville	Motteville	k1gFnSc1	Motteville
Ouville-l	Ouville	k1gMnSc1	Ouville-l
<g/>
'	'	kIx"	'
<g/>
Abbaye	Abbay	k1gMnSc2	Abbay
Saint-Martin-aux-Arbres	Saint-Martinux-Arbres	k1gMnSc1	Saint-Martin-aux-Arbres
Saussay	Saussaa	k1gMnSc2	Saussaa
Vibeuf	Vibeuf	k1gMnSc1	Vibeuf
Yerville	Yervill	k1gMnSc2	Yervill
</s>
