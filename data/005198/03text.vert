<s>
Eragon	Eragon	k1gMnSc1	Eragon
je	být	k5eAaImIp3nS	být
fantasy	fantas	k1gInPc4	fantas
román	román	k1gInSc4	román
Christophera	Christopher	k1gMnSc4	Christopher
Paoliniho	Paolini	k1gMnSc4	Paolini
<g/>
,	,	kIx,	,
první	první	k4xOgInSc4	první
díl	díl	k1gInSc4	díl
tetralogie	tetralogie	k1gFnSc1	tetralogie
Odkaz	odkaz	k1gInSc1	odkaz
Dračích	dračí	k2eAgMnPc2d1	dračí
jezdců	jezdec	k1gMnPc2	jezdec
<g/>
.	.	kIx.	.
</s>
<s>
Vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
mladíkovi	mladík	k1gMnSc6	mladík
Eragonovi	Eragon	k1gMnSc6	Eragon
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
nalezne	nalézt	k5eAaBmIp3nS	nalézt
kouzelný	kouzelný	k2eAgInSc4d1	kouzelný
kámen	kámen	k1gInSc4	kámen
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
vyklube	vyklubat	k5eAaPmIp3nS	vyklubat
dračí	dračí	k2eAgNnSc1d1	dračí
mládě	mládě	k1gNnSc1	mládě
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
obyčejný	obyčejný	k2eAgInSc1d1	obyčejný
život	život	k1gInSc1	život
se	se	k3xPyFc4	se
převrátí	převrátit	k5eAaPmIp3nS	převrátit
naruby	naruby	k6eAd1	naruby
ze	z	k7c2	z
dne	den	k1gInSc2	den
na	na	k7c4	na
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nucen	nutit	k5eAaImNgMnS	nutit
převzít	převzít	k5eAaPmF	převzít
odpovědnost	odpovědnost	k1gFnSc4	odpovědnost
za	za	k7c4	za
osud	osud	k1gInSc4	osud
království	království	k1gNnSc2	království
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
vládne	vládnout	k5eAaImIp3nS	vládnout
zlý	zlý	k2eAgInSc1d1	zlý
a	a	k8xC	a
krutý	krutý	k2eAgMnSc1d1	krutý
král	král	k1gMnSc1	král
Galbatorix	Galbatorix	k1gInSc1	Galbatorix
<g/>
.	.	kIx.	.
</s>
<s>
Vypravuje	vypravovat	k5eAaImIp3nS	vypravovat
se	se	k3xPyFc4	se
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
učitelem	učitel	k1gMnSc7	učitel
Bromem	brom	k1gInSc7	brom
a	a	k8xC	a
zažívá	zažívat	k5eAaImIp3nS	zažívat
spoustu	spousta	k1gFnSc4	spousta
dobrodružství	dobrodružství	k1gNnSc2	dobrodružství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvé	prvý	k4xOgFnSc6	prvý
řadě	řada	k1gFnSc6	řada
hodlá	hodlat	k5eAaImIp3nS	hodlat
vystopovat	vystopovat	k5eAaPmF	vystopovat
Ra	ra	k0	ra
<g/>
'	'	kIx"	'
<g/>
zaky	zak	k2eAgMnPc4d1	zak
(	(	kIx(	(
<g/>
královy	králův	k2eAgMnPc4d1	králův
posluhovače	posluhovač	k1gMnPc4	posluhovač
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
zabili	zabít	k5eAaPmAgMnP	zabít
jeho	jeho	k3xOp3gMnSc4	jeho
strýce	strýc	k1gMnSc4	strýc
–	–	k?	–
vychovatele	vychovatel	k1gMnSc4	vychovatel
–	–	k?	–
a	a	k8xC	a
v	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
ochránit	ochránit	k5eAaPmF	ochránit
obyvatele	obyvatel	k1gMnPc4	obyvatel
tolik	tolik	k6eAd1	tolik
sužované	sužovaný	k2eAgFnPc1d1	sužovaná
země	zem	k1gFnPc1	zem
<g/>
.	.	kIx.	.
</s>
<s>
Eragon	Eragon	k1gInSc1	Eragon
má	mít	k5eAaImIp3nS	mít
však	však	k9	však
před	před	k7c7	před
sebou	se	k3xPyFc7	se
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
cestu	cesta	k1gFnSc4	cesta
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jeho	jeho	k3xOp3gFnPc1	jeho
dosavadní	dosavadní	k2eAgFnPc1d1	dosavadní
schopnosti	schopnost	k1gFnPc1	schopnost
nejsou	být	k5eNaImIp3nP	být
ještě	ještě	k9	ještě
tak	tak	k6eAd1	tak
velké	velký	k2eAgNnSc1d1	velké
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgInS	moct
vzdorovat	vzdorovat	k5eAaImF	vzdorovat
takto	takto	k6eAd1	takto
silným	silný	k2eAgMnPc3d1	silný
nepřátelům	nepřítel	k1gMnPc3	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
autor	autor	k1gMnSc1	autor
nepřinesl	přinést	k5eNaPmAgMnS	přinést
do	do	k7c2	do
žánru	žánr	k1gInSc2	žánr
fantasy	fantas	k1gInPc4	fantas
žádný	žádný	k3yNgInSc4	žádný
nový	nový	k2eAgInSc4d1	nový
zásadní	zásadní	k2eAgInSc4d1	zásadní
prvek	prvek	k1gInSc4	prvek
a	a	k8xC	a
pro	pro	k7c4	pro
dějová	dějový	k2eAgNnPc4d1	dějové
schémata	schéma	k1gNnPc4	schéma
i	i	k8xC	i
postavy	postava	k1gFnPc4	postava
a	a	k8xC	a
rasy	rasa	k1gFnSc2	rasa
našel	najít	k5eAaPmAgMnS	najít
inspiraci	inspirace	k1gFnSc4	inspirace
u	u	k7c2	u
jiných	jiný	k2eAgMnPc2d1	jiný
autorů	autor	k1gMnPc2	autor
(	(	kIx(	(
<g/>
Tolkienův	Tolkienův	k2eAgMnSc1d1	Tolkienův
"	"	kIx"	"
<g/>
Pán	pán	k1gMnSc1	pán
prstenů	prsten	k1gInPc2	prsten
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
války	válka	k1gFnSc2	válka
<g/>
"	"	kIx"	"
G.	G.	kA	G.
Lucase	Lucasa	k1gFnSc6	Lucasa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
dílo	dílo	k1gNnSc4	dílo
pozoruhodně	pozoruhodně	k6eAd1	pozoruhodně
propracované	propracovaný	k2eAgFnPc1d1	propracovaná
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
s	s	k7c7	s
přihlédnutím	přihlédnutí	k1gNnSc7	přihlédnutí
k	k	k7c3	k
věku	věk	k1gInSc3	věk
autora	autor	k1gMnSc2	autor
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
Eragona	Eragon	k1gMnSc4	Eragon
začal	začít	k5eAaPmAgMnS	začít
psát	psát	k5eAaImF	psát
již	již	k6eAd1	již
v	v	k7c6	v
15	[number]	k4	15
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Eragon	Eragon	k1gMnSc1	Eragon
<g/>
,	,	kIx,	,
mladý	mladý	k2eAgMnSc1d1	mladý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zdatný	zdatný	k2eAgMnSc1d1	zdatný
farmář	farmář	k1gMnSc1	farmář
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgNnSc4	jenž
jméno	jméno	k1gNnSc4	jméno
nesli	nést	k5eAaImAgMnP	nést
pouze	pouze	k6eAd1	pouze
dva	dva	k4xCgMnPc1	dva
lidé	člověk	k1gMnPc1	člověk
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
,	,	kIx,	,
první	první	k4xOgInSc1	první
Dračí	dračí	k2eAgMnSc1d1	dračí
jezdec	jezdec	k1gMnSc1	jezdec
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgMnS	vydat
na	na	k7c4	na
lov	lov	k1gInSc4	lov
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
on	on	k3xPp3gMnSc1	on
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc1	jeho
strýc	strýc	k1gMnSc1	strýc
Gero	Gero	k1gMnSc1	Gero
a	a	k8xC	a
bratranec	bratranec	k1gMnSc1	bratranec
Roran	Roran	k1gMnSc1	Roran
nemají	mít	k5eNaImIp3nP	mít
dostatek	dostatek	k1gInSc4	dostatek
peněz	peníze	k1gInPc2	peníze
na	na	k7c4	na
jídlo	jídlo	k1gNnSc4	jídlo
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
vyráží	vyrážet	k5eAaImIp3nS	vyrážet
do	do	k7c2	do
Dračích	dračí	k2eAgFnPc2d1	dračí
hor	hora	k1gFnPc2	hora
ulovit	ulovit	k5eAaPmF	ulovit
nějaké	nějaký	k3yIgNnSc4	nějaký
zvíře	zvíře	k1gNnSc4	zvíře
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
netrpěli	trpět	k5eNaImAgMnP	trpět
hlady	hlady	k6eAd1	hlady
<g/>
.	.	kIx.	.
</s>
<s>
Najednou	najednou	k6eAd1	najednou
narazí	narazit	k5eAaPmIp3nS	narazit
na	na	k7c4	na
stádo	stádo	k1gNnSc4	stádo
srn	srna	k1gFnPc2	srna
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
je	on	k3xPp3gNnSc4	on
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
stopovat	stopovat	k5eAaImF	stopovat
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
je	on	k3xPp3gNnSc4	on
nalezne	naleznout	k5eAaPmIp3nS	naleznout
<g/>
,	,	kIx,	,
namíří	namířit	k5eAaPmIp3nS	namířit
lukem	luk	k1gInSc7	luk
na	na	k7c6	na
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
a	a	k8xC	a
vystřelí	vystřelit	k5eAaPmIp3nP	vystřelit
šíp	šíp	k1gInSc4	šíp
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
však	však	k9	však
svůj	svůj	k3xOyFgInSc4	svůj
cíl	cíl	k1gInSc4	cíl
nenašel	najít	k5eNaPmAgMnS	najít
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
stádo	stádo	k1gNnSc4	stádo
vyplašil	vyplašit	k5eAaPmAgInS	vyplašit
zvláštní	zvláštní	k2eAgInSc1d1	zvláštní
záblesk	záblesk	k1gInSc1	záblesk
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
šel	jít	k5eAaImAgMnS	jít
Eragon	Eragon	k1gMnSc1	Eragon
podívat	podívat	k5eAaImF	podívat
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
zač	zač	k6eAd1	zač
<g/>
,	,	kIx,	,
uviděl	uvidět	k5eAaPmAgMnS	uvidět
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
velký	velký	k2eAgInSc1d1	velký
modrý	modrý	k2eAgInSc1d1	modrý
kámen	kámen	k1gInSc1	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
si	se	k3xPyFc3	se
jej	on	k3xPp3gNnSc4	on
vzal	vzít	k5eAaPmAgMnS	vzít
sebou	se	k3xPyFc7	se
<g/>
,	,	kIx,	,
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
rodné	rodný	k2eAgFnSc2d1	rodná
vesnice	vesnice	k1gFnSc2	vesnice
–	–	k?	–
Carvahallu	Carvahalla	k1gFnSc4	Carvahalla
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
jej	on	k3xPp3gMnSc4	on
zkouší	zkoušet	k5eAaImIp3nP	zkoušet
vyměnit	vyměnit	k5eAaPmF	vyměnit
za	za	k7c4	za
maso	maso	k1gNnSc4	maso
u	u	k7c2	u
řezníka	řezník	k1gMnSc2	řezník
Slouna	Slouna	k1gFnSc1	Slouna
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
když	když	k8xS	když
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
ten	ten	k3xDgInSc4	ten
kámen	kámen	k1gInSc4	kámen
z	z	k7c2	z
Dračích	dračí	k2eAgFnPc2d1	dračí
hor	hora	k1gFnPc2	hora
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
Eragona	Eragona	k1gFnSc1	Eragona
vyžene	vyhnat	k5eAaPmIp3nS	vyhnat
ze	z	k7c2	z
svého	svůj	k3xOyFgInSc2	svůj
obchodu	obchod	k1gInSc2	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
Eragon	Eragon	k1gInSc1	Eragon
vrátil	vrátit	k5eAaPmAgInS	vrátit
domů	domů	k6eAd1	domů
(	(	kIx(	(
<g/>
pár	pár	k4xCyI	pár
mil	míle	k1gFnPc2	míle
od	od	k7c2	od
Carvahallu	Carvahall	k1gInSc2	Carvahall
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
divili	divit	k5eAaImAgMnP	divit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nic	nic	k3yNnSc1	nic
neulovil	ulovit	k5eNaPmAgMnS	ulovit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
věděli	vědět	k5eAaImAgMnP	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
Eragon	Eragon	k1gInSc4	Eragon
výborný	výborný	k2eAgMnSc1d1	výborný
lovec	lovec	k1gMnSc1	lovec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
noci	noc	k1gFnSc6	noc
se	se	k3xPyFc4	se
Eragon	Eragon	k1gInSc1	Eragon
probudil	probudit	k5eAaPmAgInS	probudit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
uslyšel	uslyšet	k5eAaPmAgInS	uslyšet
podivné	podivný	k2eAgInPc4d1	podivný
zvuky	zvuk	k1gInPc4	zvuk
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
pokoji	pokoj	k1gInSc6	pokoj
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
probral	probrat	k5eAaPmAgInS	probrat
nemohl	moct	k5eNaImAgInS	moct
uvěřit	uvěřit	k5eAaPmF	uvěřit
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
viděl	vidět	k5eAaImAgInS	vidět
<g/>
:	:	kIx,	:
modrý	modrý	k2eAgInSc1d1	modrý
kámen	kámen	k1gInSc1	kámen
se	se	k3xPyFc4	se
kýval	kývat	k5eAaImAgInS	kývat
a	a	k8xC	a
kutálel	kutálet	k5eAaImAgInS	kutálet
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
chvíli	chvíle	k1gFnSc6	chvíle
praskl	prasknout	k5eAaPmAgMnS	prasknout
a	a	k8xC	a
Eragon	Eragon	k1gMnSc1	Eragon
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
není	být	k5eNaImIp3nS	být
kámen	kámen	k1gInSc1	kámen
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dračí	dračí	k2eAgNnSc1d1	dračí
vejce	vejce	k1gNnSc1	vejce
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
malý	malý	k2eAgMnSc1d1	malý
dráček	dráček	k1gMnSc1	dráček
vyklubal	vyklubat	k5eAaPmAgMnS	vyklubat
<g/>
,	,	kIx,	,
zkoumal	zkoumat	k5eAaImAgInS	zkoumat
chvilku	chvilka	k1gFnSc4	chvilka
okolí	okolí	k1gNnSc2	okolí
a	a	k8xC	a
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
zarazil	zarazit	k5eAaPmAgMnS	zarazit
<g/>
,	,	kIx,	,
když	když	k8xS	když
uviděl	uvidět	k5eAaPmAgMnS	uvidět
Eragona	Eragon	k1gMnSc4	Eragon
<g/>
.	.	kIx.	.
</s>
<s>
Přiblížil	přiblížit	k5eAaPmAgMnS	přiblížit
se	se	k3xPyFc4	se
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
a	a	k8xC	a
dotkl	dotknout	k5eAaPmAgMnS	dotknout
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
čumákem	čumák	k1gInSc7	čumák
Eragonovy	Eragonův	k2eAgFnSc2d1	Eragonova
ruky	ruka	k1gFnSc2	ruka
<g/>
,	,	kIx,	,
když	když	k8xS	když
to	ten	k3xDgNnSc4	ten
udělal	udělat	k5eAaPmAgMnS	udělat
<g/>
,	,	kIx,	,
Eragon	Eragon	k1gMnSc1	Eragon
se	se	k3xPyFc4	se
svalil	svalit	k5eAaPmAgMnS	svalit
bolestí	bolest	k1gFnSc7	bolest
na	na	k7c4	na
postel	postel	k1gFnSc4	postel
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
bolest	bolest	k1gFnSc1	bolest
po	po	k7c6	po
pár	pár	k4xCyI	pár
minutách	minuta	k1gFnPc6	minuta
ustala	ustat	k5eAaPmAgFnS	ustat
<g/>
,	,	kIx,	,
podíval	podívat	k5eAaImAgMnS	podívat
se	se	k3xPyFc4	se
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
ruku	ruka	k1gFnSc4	ruka
a	a	k8xC	a
uviděl	uvidět	k5eAaPmAgInS	uvidět
stříbrnou	stříbrný	k2eAgFnSc4d1	stříbrná
značku	značka	k1gFnSc4	značka
<g/>
.	.	kIx.	.
</s>
<s>
Ráno	ráno	k6eAd1	ráno
Eragon	Eragon	k1gMnSc1	Eragon
uviděl	uvidět	k5eAaPmAgMnS	uvidět
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
dráček	dráček	k1gMnSc1	dráček
dívá	dívat	k5eAaImIp3nS	dívat
z	z	k7c2	z
okna	okno	k1gNnSc2	okno
ven	ven	k6eAd1	ven
<g/>
.	.	kIx.	.
</s>
<s>
Řekl	říct	k5eAaPmAgInS	říct
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gInSc4	on
doma	doma	k6eAd1	doma
nechat	nechat	k5eAaPmF	nechat
nemůže	moct	k5eNaImIp3nS	moct
<g/>
;	;	kIx,	;
co	co	k3yQnSc4	co
by	by	kYmCp3nP	by
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
řekl	říct	k5eAaPmAgMnS	říct
Roran	Roran	k1gMnSc1	Roran
s	s	k7c7	s
Gerem	Gero	k1gMnSc7	Gero
<g/>
?	?	kIx.	?
</s>
<s>
Proto	proto	k8xC	proto
vzal	vzít	k5eAaPmAgMnS	vzít
draka	drak	k1gMnSc4	drak
do	do	k7c2	do
lesa	les	k1gInSc2	les
a	a	k8xC	a
tam	tam	k6eAd1	tam
mu	on	k3xPp3gMnSc3	on
postavil	postavit	k5eAaPmAgMnS	postavit
domeček	domeček	k1gInSc4	domeček
na	na	k7c6	na
stromě	strom	k1gInSc6	strom
a	a	k8xC	a
dovnitř	dovnitř	k6eAd1	dovnitř
mu	on	k3xPp3gMnSc3	on
dal	dát	k5eAaPmAgMnS	dát
nějaké	nějaký	k3yIgNnSc4	nějaký
jídlo	jídlo	k1gNnSc4	jídlo
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
za	za	k7c7	za
drakem	drak	k1gInSc7	drak
chodil	chodit	k5eAaImAgMnS	chodit
do	do	k7c2	do
blízkého	blízký	k2eAgInSc2d1	blízký
lesa	les	k1gInSc2	les
a	a	k8xC	a
pozoroval	pozorovat	k5eAaImAgMnS	pozorovat
jej	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
roste	růst	k5eAaImIp3nS	růst
<g/>
.	.	kIx.	.
</s>
<s>
Jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
může	moct	k5eAaImIp3nS	moct
slyšet	slyšet	k5eAaImF	slyšet
drakovy	drakův	k2eAgFnPc4d1	Drakova
myšlenky	myšlenka	k1gFnPc4	myšlenka
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
spolu	spolu	k6eAd1	spolu
začali	začít	k5eAaPmAgMnP	začít
komunikovat	komunikovat	k5eAaImF	komunikovat
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
také	také	k9	také
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
není	být	k5eNaImIp3nS	být
drak	drak	k1gInSc1	drak
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dračice	dračice	k1gFnSc1	dračice
<g/>
.	.	kIx.	.
</s>
<s>
Eragon	Eragon	k1gInSc1	Eragon
jí	on	k3xPp3gFnSc7	on
vybral	vybrat	k5eAaPmAgMnS	vybrat
jméno	jméno	k1gNnSc4	jméno
Safira	Safiro	k1gNnSc2	Safiro
(	(	kIx(	(
<g/>
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
jmen	jméno	k1gNnPc2	jméno
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
mu	on	k3xPp3gMnSc3	on
pověděl	povědět	k5eAaPmAgMnS	povědět
místní	místní	k2eAgMnSc1d1	místní
vypravěč	vypravěč	k1gMnSc1	vypravěč
Brom	brom	k1gInSc1	brom
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
Galbatorix	Galbatorix	k1gInSc4	Galbatorix
pošle	poslat	k5eAaPmIp3nS	poslat
ra	ra	k0	ra
<g/>
'	'	kIx"	'
<g/>
zaky	zak	k1gMnPc4	zak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
získali	získat	k5eAaPmAgMnP	získat
dračí	dračí	k2eAgNnSc4d1	dračí
vejce	vejce	k1gNnSc4	vejce
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
na	na	k7c4	na
farmu	farma	k1gFnSc4	farma
dorazili	dorazit	k5eAaPmAgMnP	dorazit
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
Eragon	Eragon	k1gInSc1	Eragon
naštěstí	naštěstí	k6eAd1	naštěstí
pryč	pryč	k6eAd1	pryč
a	a	k8xC	a
Roran	Roran	k1gMnSc1	Roran
také	také	k9	také
nebyl	být	k5eNaImAgMnS	být
doma	doma	k6eAd1	doma
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgMnS	vydat
do	do	k7c2	do
světa	svět	k1gInSc2	svět
vydělat	vydělat	k5eAaPmF	vydělat
peníze	peníz	k1gInPc1	peníz
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
mohl	moct	k5eAaImAgMnS	moct
vzít	vzít	k5eAaPmF	vzít
řezníkovu	řezníkův	k2eAgFnSc4d1	řezníkova
dceru	dcera	k1gFnSc4	dcera
Katrinu	Katrin	k1gInSc2	Katrin
<g/>
.	.	kIx.	.
</s>
<s>
Doma	doma	k6eAd1	doma
byl	být	k5eAaImAgMnS	být
jenom	jenom	k9	jenom
Gero	Gero	k1gMnSc1	Gero
<g/>
,	,	kIx,	,
strýc	strýc	k1gMnSc1	strýc
Eragona	Eragona	k1gFnSc1	Eragona
<g/>
.	.	kIx.	.
</s>
<s>
Ra	ra	k0	ra
<g/>
'	'	kIx"	'
<g/>
zakové	zakové	k2eAgFnSc4d1	zakové
farmu	farma	k1gFnSc4	farma
zapálili	zapálit	k5eAaPmAgMnP	zapálit
a	a	k8xC	a
Gera	Gera	k1gFnSc1	Gera
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
nechali	nechat	k5eAaPmAgMnP	nechat
zraněného	zraněný	k1gMnSc4	zraněný
<g/>
.	.	kIx.	.
</s>
<s>
Gero	Gero	k1gMnSc1	Gero
poté	poté	k6eAd1	poté
na	na	k7c6	na
zranění	zranění	k1gNnSc6	zranění
umírá	umírat	k5eAaImIp3nS	umírat
u	u	k7c2	u
léčitelky	léčitelka	k1gFnSc2	léčitelka
Gertrudy	Gertruda	k1gFnSc2	Gertruda
<g/>
.	.	kIx.	.
</s>
<s>
Eragon	Eragon	k1gInSc1	Eragon
se	se	k3xPyFc4	se
musel	muset	k5eAaImAgInS	muset
vydat	vydat	k5eAaPmF	vydat
pryč	pryč	k6eAd1	pryč
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
pomstil	pomstit	k5eAaImAgMnS	pomstit
ra	ra	k0	ra
<g/>
'	'	kIx"	'
<g/>
zakům	zakum	k1gNnPc3	zakum
a	a	k8xC	a
odlákal	odlákat	k5eAaPmAgInS	odlákat
královu	králův	k2eAgFnSc4d1	králova
pozornost	pozornost	k1gFnSc4	pozornost
od	od	k7c2	od
Carvahallu	Carvahall	k1gInSc2	Carvahall
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
se	se	k3xPyFc4	se
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
vydává	vydávat	k5eAaPmIp3nS	vydávat
i	i	k9	i
vypravěč	vypravěč	k1gMnSc1	vypravěč
Brom	brom	k1gInSc1	brom
(	(	kIx(	(
<g/>
Eragon	Eragon	k1gInSc1	Eragon
později	pozdě	k6eAd2	pozdě
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
také	také	k9	také
býval	bývat	k5eAaImAgMnS	bývat
jezdec	jezdec	k1gMnSc1	jezdec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
cestě	cesta	k1gFnSc6	cesta
naučí	naučit	k5eAaPmIp3nP	naučit
Brom	brom	k1gInSc4	brom
Eragona	Eragon	k1gMnSc4	Eragon
bojovat	bojovat	k5eAaImF	bojovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
správně	správně	k6eAd1	správně
používat	používat	k5eAaImF	používat
kouzla	kouzlo	k1gNnPc4	kouzlo
a	a	k8xC	a
starat	starat	k5eAaImF	starat
se	se	k3xPyFc4	se
o	o	k7c4	o
draka	drak	k1gMnSc4	drak
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
už	už	k6eAd1	už
to	ten	k3xDgNnSc1	ten
vypadá	vypadat	k5eAaPmIp3nS	vypadat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ra	ra	k0	ra
<g/>
́	́	k?	́
<g/>
zaky	zaka	k1gFnPc1	zaka
dopadnou	dopadnout	k5eAaPmIp3nP	dopadnout
v	v	k7c6	v
Dras-Leoně	Dras-Leon	k1gInSc6	Dras-Leon
<g/>
,	,	kIx,	,
ra	ra	k0	ra
<g/>
́	́	k?	́
<g/>
zakové	zaková	k1gFnSc6	zaková
je	být	k5eAaImIp3nS	být
místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
překvapí	překvapit	k5eAaPmIp3nP	překvapit
a	a	k8xC	a
zajmou	zajmout	k5eAaPmIp3nP	zajmout
je	on	k3xPp3gMnPc4	on
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
noci	noc	k1gFnSc6	noc
Eragona	Eragona	k1gFnSc1	Eragona
zachrání	zachránit	k5eAaPmIp3nS	zachránit
Murtagh	Murtagh	k1gInSc4	Murtagh
<g/>
,	,	kIx,	,
ra	ra	k0	ra
<g/>
'	'	kIx"	'
<g/>
zakové	zakové	k2eAgMnPc1d1	zakové
ale	ale	k8xC	ale
zabijí	zabít	k5eAaPmIp3nP	zabít
Broma	Brom	k1gMnSc4	Brom
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
obětoval	obětovat	k5eAaBmAgMnS	obětovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Eragon	Eragon	k1gMnSc1	Eragon
přežil	přežít	k5eAaPmAgMnS	přežít
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
chtějí	chtít	k5eAaImIp3nP	chtít
uprchnout	uprchnout	k5eAaPmF	uprchnout
z	z	k7c2	z
království	království	k1gNnSc2	království
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
Gil	Gil	k1gFnSc6	Gil
<g/>
́	́	k?	́
<g/>
eadu	eada	k1gMnSc4	eada
Eragona	Eragon	k1gMnSc4	Eragon
zajmou	zajmout	k5eAaPmIp3nP	zajmout
<g/>
.	.	kIx.	.
</s>
<s>
Opět	opět	k6eAd1	opět
ho	on	k3xPp3gMnSc4	on
zachrání	zachránit	k5eAaPmIp3nS	zachránit
Murtagh	Murtagh	k1gInSc1	Murtagh
se	s	k7c7	s
Safirou	Safira	k1gFnSc7	Safira
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
zachrání	zachránit	k5eAaPmIp3nP	zachránit
také	také	k9	také
elfku	elfku	k6eAd1	elfku
Aryu	Arya	k1gFnSc4	Arya
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
vězněna	věznit	k5eAaImNgFnS	věznit
v	v	k7c6	v
tamním	tamní	k2eAgInSc6d1	tamní
žaláři	žalář	k1gInSc6	žalář
a	a	k8xC	a
mučena	mučen	k2eAgFnSc1d1	mučena
Stínem	stín	k1gInSc7	stín
(	(	kIx(	(
<g/>
temný	temný	k2eAgMnSc1d1	temný
čaroděj	čaroděj	k1gMnSc1	čaroděj
Durza	Durz	k1gMnSc2	Durz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stín	stín	k1gInSc1	stín
však	však	k9	však
Aryu	Arya	k1gFnSc4	Arya
otrávil	otrávit	k5eAaPmAgInS	otrávit
<g/>
,	,	kIx,	,
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
mysli	mysl	k1gFnSc2	mysl
sdělila	sdělit	k5eAaPmAgFnS	sdělit
Eragonovi	Eragon	k1gMnSc3	Eragon
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
dostat	dostat	k5eAaPmF	dostat
do	do	k7c2	do
Beorských	Beorský	k2eAgFnPc2d1	Beorský
hor	hora	k1gFnPc2	hora
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
zde	zde	k6eAd1	zde
našli	najít	k5eAaPmAgMnP	najít
skupinu	skupina	k1gFnSc4	skupina
odboje	odboj	k1gInSc2	odboj
-	-	kIx~	-
Vardeny	Vardena	k1gFnSc2	Vardena
<g/>
.	.	kIx.	.
</s>
<s>
Překonají	překonat	k5eAaPmIp3nP	překonat
poušť	poušť	k1gFnSc1	poušť
Hadarak	Hadarak	k1gInSc4	Hadarak
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
nevyhnou	vyhnout	k5eNaPmIp3nP	vyhnout
dalším	další	k2eAgFnPc3d1	další
potížím	potíž	k1gFnPc3	potíž
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
ale	ale	k9	ale
přece	přece	k9	přece
jenom	jenom	k9	jenom
dostanou	dostat	k5eAaPmIp3nP	dostat
k	k	k7c3	k
Vardenům	Varden	k1gMnPc3	Varden
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
nečeká	čekat	k5eNaImIp3nS	čekat
příliš	příliš	k6eAd1	příliš
vřelé	vřelý	k2eAgNnSc1d1	vřelé
uvítání	uvítání	k1gNnSc1	uvítání
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
setkají	setkat	k5eAaPmIp3nP	setkat
s	s	k7c7	s
dvěma	dva	k4xCgMnPc7	dva
čaroději	čaroděj	k1gMnPc7	čaroděj
(	(	kIx(	(
<g/>
Dvojčaty	dvojče	k1gNnPc7	dvojče
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
Eragona	Eragon	k1gMnSc4	Eragon
přinutí	přinutit	k5eAaPmIp3nP	přinutit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
podrobil	podrobit	k5eAaPmAgMnS	podrobit
zkoušce	zkouška	k1gFnSc6	zkouška
mysli	mysl	k1gFnSc6	mysl
<g/>
.	.	kIx.	.
</s>
<s>
Murtagh	Murtagh	k1gMnSc1	Murtagh
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zkoušku	zkouška	k1gFnSc4	zkouška
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
ocitne	ocitnout	k5eAaPmIp3nS	ocitnout
v	v	k7c6	v
cele	cela	k1gFnSc6	cela
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
vůdce	vůdce	k1gMnSc1	vůdce
Vardenů	Varden	k1gMnPc2	Varden
pozná	poznat	k5eAaPmIp3nS	poznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
syn	syn	k1gMnSc1	syn
prvního	první	k4xOgMnSc2	první
a	a	k8xC	a
nejhoršího	zlý	k2eAgInSc2d3	Nejhorší
z	z	k7c2	z
křivopřísežníků	křivopřísežník	k1gMnPc2	křivopřísežník
-	-	kIx~	-
Morzana	Morzan	k1gMnSc2	Morzan
<g/>
.	.	kIx.	.
</s>
<s>
Murtagh	Murtagh	k1gInSc1	Murtagh
prokáže	prokázat	k5eAaPmIp3nS	prokázat
své	svůj	k3xOyFgInPc4	svůj
dobré	dobrý	k2eAgInPc4d1	dobrý
úmysly	úmysl	k1gInPc4	úmysl
a	a	k8xC	a
snahu	snaha	k1gFnSc4	snaha
očistit	očistit	k5eAaPmF	očistit
své	svůj	k3xOyFgNnSc4	svůj
jméno	jméno	k1gNnSc4	jméno
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
je	být	k5eAaImIp3nS	být
opět	opět	k6eAd1	opět
propuštěn	propuštěn	k2eAgInSc1d1	propuštěn
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
o	o	k7c4	o
měsíc	měsíc	k1gInSc4	měsíc
později	pozdě	k6eAd2	pozdě
začne	začít	k5eAaPmIp3nS	začít
velká	velký	k2eAgFnSc1d1	velká
bitva	bitva	k1gFnSc1	bitva
-	-	kIx~	-
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
nastoupili	nastoupit	k5eAaPmAgMnP	nastoupit
Vardenové	Varden	k1gMnPc1	Varden
a	a	k8xC	a
vojsko	vojsko	k1gNnSc1	vojsko
Urgalů	Urgal	k1gInPc2	Urgal
(	(	kIx(	(
<g/>
netvorové	netvorové	k?	netvorové
s	s	k7c7	s
rohy	roh	k1gInPc7	roh
<g/>
)	)	kIx)	)
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Durzou	Durza	k1gFnSc7	Durza
(	(	kIx(	(
<g/>
Stínem	stín	k1gInSc7	stín
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vardenové	Vardenové	k2eAgFnSc4d1	Vardenové
bitvu	bitva	k1gFnSc4	bitva
vyhráli	vyhrát	k5eAaPmAgMnP	vyhrát
<g/>
,	,	kIx,	,
Eragon	Eragon	k1gInSc4	Eragon
šťastnou	šťastný	k2eAgFnSc4d1	šťastná
náhodou	náhodou	k6eAd1	náhodou
zabil	zabít	k5eAaPmAgMnS	zabít
Durzu	Durza	k1gFnSc4	Durza
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byl	být	k5eAaImAgInS	být
přitom	přitom	k6eAd1	přitom
vážně	vážně	k6eAd1	vážně
zraněn	zranit	k5eAaPmNgMnS	zranit
na	na	k7c6	na
zádech	záda	k1gNnPc6	záda
<g/>
.	.	kIx.	.
</s>
<s>
Naštěstí	naštěstí	k6eAd1	naštěstí
to	ten	k3xDgNnSc4	ten
přežil	přežít	k5eAaPmAgMnS	přežít
<g/>
.	.	kIx.	.
</s>
<s>
Eragonovi	Eragonův	k2eAgMnPc1d1	Eragonův
začali	začít	k5eAaPmAgMnP	začít
všichni	všechen	k3xTgMnPc1	všechen
říkat	říkat	k5eAaImF	říkat
Stínovrah	Stínovrah	k1gInSc1	Stínovrah
<g/>
.	.	kIx.	.
</s>
<s>
Eragon	Eragon	k1gInSc1	Eragon
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
<g/>
)	)	kIx)	)
Odkaz	odkaz	k1gInSc1	odkaz
Dračích	dračí	k2eAgMnPc2d1	dračí
jezdců	jezdec	k1gMnPc2	jezdec
</s>
