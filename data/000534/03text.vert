<s>
Čínská	čínský	k2eAgFnSc1d1	čínská
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
čínsky	čínsky	k6eAd1	čínsky
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
přepisu	přepis	k1gInSc6	přepis
Čung-chua	Čunghua	k1gFnSc1	Čung-chua
žen-min	ženin	k1gInSc1	žen-min
kung-che-kuo	kungheuo	k6eAd1	kung-che-kuo
<g/>
,	,	kIx,	,
pchin-jinem	pchinin	k1gInSc7	pchin-jin
Zhō	Zhō	k1gFnSc2	Zhō
rénmín	rénmín	k1gInSc1	rénmín
gò	gò	k?	gò
<g/>
,	,	kIx,	,
znaky	znak	k1gInPc4	znak
zjednodušené	zjednodušený	k2eAgFnSc2d1	zjednodušená
中	中	k?	中
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
ČLR	ČLR	kA	ČLR
či	či	k8xC	či
Čína	Čína	k1gFnSc1	Čína
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
stát	stát	k1gInSc4	stát
ležící	ležící	k2eAgInSc4d1	ležící
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Asii	Asie	k1gFnSc6	Asie
a	a	k8xC	a
nejlidnatější	lidnatý	k2eAgFnSc1d3	nejlidnatější
země	země	k1gFnSc1	země
světa	svět	k1gInSc2	svět
<g/>
;	;	kIx,	;
to	ten	k3xDgNnSc4	ten
s	s	k7c7	s
rychle	rychle	k6eAd1	rychle
rostoucí	rostoucí	k2eAgFnSc7d1	rostoucí
ekonomikou	ekonomika	k1gFnSc7	ekonomika
a	a	k8xC	a
vojenskou	vojenský	k2eAgFnSc7d1	vojenská
silou	síla	k1gFnSc7	síla
dává	dávat	k5eAaImIp3nS	dávat
vzrůst	vzrůst	k1gInSc1	vzrůst
nové	nový	k2eAgFnSc2d1	nová
supervelmoci	supervelmoc	k1gFnSc2	supervelmoc
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
vedena	vést	k5eAaImNgFnS	vést
Komunistickou	komunistický	k2eAgFnSc7d1	komunistická
stranou	strana	k1gFnSc7	strana
Číny	Čína	k1gFnSc2	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Čínská	čínský	k2eAgFnSc1d1	čínská
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
si	se	k3xPyFc3	se
dělá	dělat	k5eAaImIp3nS	dělat
nárok	nárok	k1gInSc4	nárok
i	i	k9	i
na	na	k7c4	na
ostrov	ostrov	k1gInSc4	ostrov
Tchaj-wan	Tchajan	k1gInSc1	Tchaj-wan
a	a	k8xC	a
několik	několik	k4yIc1	několik
dalších	další	k2eAgNnPc2d1	další
menších	malý	k2eAgNnPc2d2	menší
území	území	k1gNnPc2	území
<g/>
,	,	kIx,	,
tyto	tento	k3xDgFnPc1	tento
oblasti	oblast	k1gFnPc1	oblast
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
spravovány	spravovat	k5eAaImNgInP	spravovat
vládou	vláda	k1gFnSc7	vláda
Čínské	čínský	k2eAgFnPc1d1	čínská
republiky	republika	k1gFnPc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc1	dějiny
Čínské	čínský	k2eAgFnSc2d1	čínská
lidové	lidový	k2eAgFnSc2d1	lidová
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
znovu	znovu	k6eAd1	znovu
propukla	propuknout	k5eAaPmAgFnS	propuknout
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
mezi	mezi	k7c7	mezi
Komunistickou	komunistický	k2eAgFnSc7d1	komunistická
stranou	strana	k1gFnSc7	strana
Číny	Čína	k1gFnSc2	Čína
(	(	kIx(	(
<g/>
Kungčchantang	Kungčchantang	k1gInSc1	Kungčchantang
<g/>
)	)	kIx)	)
a	a	k8xC	a
Nacionalisty	nacionalista	k1gMnSc2	nacionalista
(	(	kIx(	(
<g/>
Kuomintang	Kuomintang	k1gInSc1	Kuomintang
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
skončila	skončit	k5eAaPmAgFnS	skončit
roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
komunisté	komunista	k1gMnPc1	komunista
vedení	vedení	k1gNnSc2	vedení
Mao	Mao	k1gFnPc2	Mao
Ce-tungem	Ceung	k1gInSc7	Ce-tung
převzali	převzít	k5eAaPmAgMnP	převzít
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
pevninskou	pevninský	k2eAgFnSc7d1	pevninská
Čínou	Čína	k1gFnSc7	Čína
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
ostrova	ostrov	k1gInSc2	ostrov
Chaj-nan	Chajan	k1gMnSc1	Chaj-nan
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
a	a	k8xC	a
příslušníci	příslušník	k1gMnPc1	příslušník
Kuomintangu	Kuomintang	k1gInSc2	Kuomintang
se	se	k3xPyFc4	se
stáhli	stáhnout	k5eAaPmAgMnP	stáhnout
na	na	k7c4	na
ostrov	ostrov	k1gInSc4	ostrov
Tchaj-wan	Tchajan	k1gInSc1	Tchaj-wan
a	a	k8xC	a
několik	několik	k4yIc1	několik
menších	malý	k2eAgInPc2d2	menší
přilehlých	přilehlý	k2eAgInPc2d1	přilehlý
ostrovů	ostrov	k1gInPc2	ostrov
(	(	kIx(	(
<g/>
území	území	k1gNnSc2	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Čínské	čínský	k2eAgFnSc2d1	čínská
republiky	republika	k1gFnSc2	republika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1949	[number]	k4	1949
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
Mao	Mao	k1gMnSc1	Mao
Ce-tung	Ceung	k1gMnSc1	Ce-tung
na	na	k7c6	na
pekingském	pekingský	k2eAgNnSc6d1	pekingské
náměstí	náměstí	k1gNnSc6	náměstí
Nebeského	nebeský	k2eAgInSc2d1	nebeský
klidu	klid	k1gInSc2	klid
slovy	slovo	k1gNnPc7	slovo
"	"	kIx"	"
<g/>
Číňané	Číňan	k1gMnPc1	Číňan
povstali	povstat	k5eAaPmAgMnP	povstat
<g/>
"	"	kIx"	"
Čínskou	čínský	k2eAgFnSc4d1	čínská
lidovou	lidový	k2eAgFnSc4d1	lidová
republiku	republika	k1gFnSc4	republika
<g/>
.	.	kIx.	.
</s>
<s>
Období	období	k1gNnSc1	období
vlády	vláda	k1gFnSc2	vláda
během	během	k7c2	během
života	život	k1gInSc2	život
Mao	Mao	k1gMnSc2	Mao
Ce-tunga	Ceung	k1gMnSc2	Ce-tung
(	(	kIx(	(
<g/>
1949	[number]	k4	1949
<g/>
-	-	kIx~	-
<g/>
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
dodnes	dodnes	k6eAd1	dodnes
vzbuzuje	vzbuzovat	k5eAaImIp3nS	vzbuzovat
řadu	řada	k1gFnSc4	řada
kontroverzí	kontroverze	k1gFnPc2	kontroverze
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
kvůli	kvůli	k7c3	kvůli
nesmiřitelné	smiřitelný	k2eNgFnSc3d1	nesmiřitelná
politice	politika	k1gFnSc3	politika
vůči	vůči	k7c3	vůči
odpůrcům	odpůrce	k1gMnPc3	odpůrce
<g/>
,	,	kIx,	,
mnohým	mnohý	k2eAgMnPc3d1	mnohý
omylům	omyl	k1gInPc3	omyl
a	a	k8xC	a
až	až	k9	až
zločinným	zločinný	k2eAgInPc3d1	zločinný
důsledkům	důsledek	k1gInPc3	důsledek
na	na	k7c6	na
obyvatelstvu	obyvatelstvo	k1gNnSc6	obyvatelstvo
způsobených	způsobený	k2eAgFnPc2d1	způsobená
mj.	mj.	kA	mj.
fanatickým	fanatický	k2eAgInSc7d1	fanatický
vykládáním	vykládání	k1gNnSc7	vykládání
komunistické	komunistický	k2eAgFnSc2d1	komunistická
ideologie	ideologie	k1gFnSc2	ideologie
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
vláda	vláda	k1gFnSc1	vláda
okamžitě	okamžitě	k6eAd1	okamžitě
uzákonila	uzákonit	k5eAaPmAgFnS	uzákonit
pozemkovou	pozemkový	k2eAgFnSc4d1	pozemková
reformu	reforma	k1gFnSc4	reforma
<g/>
,	,	kIx,	,
znárodnila	znárodnit	k5eAaPmAgFnS	znárodnit
průmyslovou	průmyslový	k2eAgFnSc4d1	průmyslová
výrobu	výroba	k1gFnSc4	výroba
a	a	k8xC	a
započala	započnout	k5eAaPmAgFnS	započnout
s	s	k7c7	s
vyhlašováním	vyhlašování	k1gNnSc7	vyhlašování
kampaní	kampaň	k1gFnPc2	kampaň
(	(	kIx(	(
<g/>
programy	program	k1gInPc1	program
gramotnosti	gramotnost	k1gFnSc2	gramotnost
<g/>
,	,	kIx,	,
očkování	očkování	k1gNnSc1	očkování
<g/>
,	,	kIx,	,
zvýšení	zvýšení	k1gNnSc1	zvýšení
kvality	kvalita	k1gFnSc2	kvalita
zemědělství	zemědělství	k1gNnSc2	zemědělství
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
nového	nový	k2eAgInSc2d1	nový
státu	stát	k1gInSc2	stát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
ČLR	ČLR	kA	ČLR
také	také	k9	také
formálně	formálně	k6eAd1	formálně
anektovala	anektovat	k5eAaBmAgFnS	anektovat
Východní	východní	k2eAgInSc4d1	východní
Turkestán	Turkestán	k1gInSc4	Turkestán
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgFnSc1d1	dnešní
autonomní	autonomní	k2eAgFnSc1d1	autonomní
oblast	oblast	k1gFnSc1	oblast
Sin-ťiang	Sin-ťianga	k1gFnPc2	Sin-ťianga
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
i	i	k9	i
Tibet	Tibet	k1gInSc1	Tibet
(	(	kIx(	(
<g/>
oficiálně	oficiálně	k6eAd1	oficiálně
až	až	k6eAd1	až
roku	rok	k1gInSc2	rok
1951	[number]	k4	1951
<g/>
,	,	kIx,	,
dnešní	dnešní	k2eAgFnSc1d1	dnešní
Tibetská	tibetský	k2eAgFnSc1d1	tibetská
autonomní	autonomní	k2eAgFnSc1d1	autonomní
oblast	oblast	k1gFnSc1	oblast
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
dokončila	dokončit	k5eAaPmAgFnS	dokončit
opětovné	opětovný	k2eAgNnSc4d1	opětovné
sjednocení	sjednocení	k1gNnSc4	sjednocení
území	území	k1gNnSc2	území
připojením	připojení	k1gNnSc7	připojení
všech	všecek	k3xTgInPc2	všecek
separatistických	separatistický	k2eAgInPc2d1	separatistický
států	stát	k1gInPc2	stát
vzniklých	vzniklý	k2eAgInPc2d1	vzniklý
po	po	k7c6	po
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
republiky	republika	k1gFnSc2	republika
roku	rok	k1gInSc2	rok
1912	[number]	k4	1912
<g/>
.	.	kIx.	.
</s>
<s>
Komunistická	komunistický	k2eAgFnSc1d1	komunistická
ČLR	ČLR	kA	ČLR
se	se	k3xPyFc4	se
také	také	k9	také
zapojila	zapojit	k5eAaPmAgNnP	zapojit
do	do	k7c2	do
korejské	korejský	k2eAgFnSc2d1	Korejská
války	válka	k1gFnSc2	válka
(	(	kIx(	(
<g/>
1950	[number]	k4	1950
<g/>
-	-	kIx~	-
<g/>
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
komunistického	komunistický	k2eAgInSc2d1	komunistický
bloku	blok	k1gInSc2	blok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
pod	pod	k7c7	pod
heslem	heslo	k1gNnSc7	heslo
"	"	kIx"	"
<g/>
Ať	ať	k8xS	ať
rozkvétá	rozkvétat	k5eAaImIp3nS	rozkvétat
sto	sto	k4xCgNnSc4	sto
květů	květ	k1gInPc2	květ
umění	umění	k1gNnSc1	umění
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
soupeří	soupeřit	k5eAaImIp3nS	soupeřit
sto	sto	k4xCgNnSc1	sto
škol	škola	k1gFnPc2	škola
učení	učení	k1gNnSc2	učení
<g/>
"	"	kIx"	"
Kampaň	kampaň	k1gFnSc1	kampaň
Sta	sto	k4xCgNnPc1	sto
květů	květ	k1gInPc2	květ
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejímž	jejíž	k3xOyRp3gInSc6	jejíž
rámci	rámec	k1gInSc6	rámec
byli	být	k5eAaImAgMnP	být
intelektuálové	intelektuál	k1gMnPc1	intelektuál
vyzváni	vyzvat	k5eAaPmNgMnP	vyzvat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
dali	dát	k5eAaPmAgMnP	dát
kritikou	kritika	k1gFnSc7	kritika
podněty	podnět	k1gInPc4	podnět
pro	pro	k7c4	pro
další	další	k2eAgNnSc4d1	další
zlepšení	zlepšení	k1gNnSc4	zlepšení
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
ale	ale	k9	ale
kampaň	kampaň	k1gFnSc1	kampaň
postupně	postupně	k6eAd1	postupně
zvrhla	zvrhnout	k5eAaPmAgFnS	zvrhnout
v	v	k7c4	v
kritiku	kritika	k1gFnSc4	kritika
vládního	vládní	k2eAgInSc2d1	vládní
režimu	režim	k1gInSc2	režim
<g/>
,	,	kIx,	,
následovala	následovat	k5eAaImAgFnS	následovat
kampaň	kampaň	k1gFnSc4	kampaň
proti	proti	k7c3	proti
pravičákům	pravičák	k1gMnPc3	pravičák
(	(	kIx(	(
<g/>
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
odsoudila	odsoudit	k5eAaPmAgFnS	odsoudit
statisíce	statisíce	k1gInPc4	statisíce
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
intelektuálů	intelektuál	k1gMnPc2	intelektuál
<g/>
,	,	kIx,	,
k	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
nebo	nebo	k8xC	nebo
k	k	k7c3	k
převýchově	převýchova	k1gFnSc3	převýchova
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pokusu	pokus	k1gInSc6	pokus
dohnat	dohnat	k5eAaPmF	dohnat
a	a	k8xC	a
předehnat	předehnat	k5eAaPmF	předehnat
Západ	západ	k1gInSc4	západ
byl	být	k5eAaImAgInS	být
vyhlášen	vyhlášen	k2eAgInSc4d1	vyhlášen
tzv.	tzv.	kA	tzv.
Velký	velký	k2eAgInSc4d1	velký
skok	skok	k1gInSc4	skok
vpřed	vpřed	k6eAd1	vpřed
(	(	kIx(	(
<g/>
1958	[number]	k4	1958
<g/>
-	-	kIx~	-
<g/>
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
ale	ale	k9	ale
skončil	skončit	k5eAaPmAgMnS	skončit
katastrofou	katastrofa	k1gFnSc7	katastrofa
-	-	kIx~	-
na	na	k7c4	na
hladomor	hladomor	k1gInSc4	hladomor
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
podle	podle	k7c2	podle
odhadů	odhad	k1gInPc2	odhad
dokonce	dokonce	k9	dokonce
více	hodně	k6eAd2	hodně
než	než	k8xS	než
30	[number]	k4	30
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
z	z	k7c2	z
čela	čelo	k1gNnSc2	čelo
KS	ks	kA	ks
Číny	Čína	k1gFnSc2	Čína
odstoupil	odstoupit	k5eAaPmAgInS	odstoupit
Mao	Mao	k1gFnSc4	Mao
Ce-tung	Ceunga	k1gFnPc2	Ce-tunga
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
si	se	k3xPyFc3	se
i	i	k9	i
nadále	nadále	k6eAd1	nadále
ve	v	k7c6	v
straně	strana	k1gFnSc6	strana
udržoval	udržovat	k5eAaImAgInS	udržovat
velký	velký	k2eAgInSc1d1	velký
vliv	vliv	k1gInSc1	vliv
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
místo	místo	k1gNnSc4	místo
byl	být	k5eAaImAgMnS	být
zvolen	zvolen	k2eAgMnSc1d1	zvolen
Liou	Lioa	k1gMnSc4	Lioa
Šao-čchi	Šao-čch	k1gFnSc2	Šao-čch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
ideologické	ideologický	k2eAgFnSc2d1	ideologická
roztržky	roztržka	k1gFnSc2	roztržka
se	s	k7c7	s
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
byla	být	k5eAaImAgFnS	být
navíc	navíc	k6eAd1	navíc
ČLR	ČLR	kA	ČLR
odkázána	odkázat	k5eAaPmNgFnS	odkázat
pouze	pouze	k6eAd1	pouze
sama	sám	k3xTgFnSc1	sám
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1964	[number]	k4	1964
byla	být	k5eAaImAgFnS	být
provedena	provést	k5eAaPmNgFnS	provést
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
jaderná	jaderný	k2eAgFnSc1d1	jaderná
zkouška	zkouška	k1gFnSc1	zkouška
a	a	k8xC	a
ČLR	ČLR	kA	ČLR
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
zařadila	zařadit	k5eAaPmAgFnS	zařadit
mezi	mezi	k7c4	mezi
jaderné	jaderný	k2eAgFnPc4d1	jaderná
mocnosti	mocnost	k1gFnPc4	mocnost
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Kulturní	kulturní	k2eAgFnSc2d1	kulturní
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Mao	Mao	k?	Mao
Ce-tung	Ceung	k1gInSc1	Ce-tung
upevnil	upevnit	k5eAaPmAgInS	upevnit
svou	svůj	k3xOyFgFnSc4	svůj
moc	moc	k1gFnSc4	moc
mezi	mezi	k7c7	mezi
rolníky	rolník	k1gMnPc7	rolník
přes	přes	k7c4	přes
pozemkovou	pozemkový	k2eAgFnSc4d1	pozemková
reformu	reforma	k1gFnSc4	reforma
a	a	k8xC	a
také	také	k9	také
díky	díky	k7c3	díky
popravám	poprava	k1gFnPc3	poprava
mezi	mezi	k7c7	mezi
1	[number]	k4	1
a	a	k8xC	a
2	[number]	k4	2
miliony	milion	k4xCgInPc1	milion
zemědělských	zemědělský	k2eAgMnPc2d1	zemědělský
hospodářů	hospodář	k1gMnPc2	hospodář
<g/>
.	.	kIx.	.
</s>
<s>
Čínská	čínský	k2eAgFnSc1d1	čínská
populace	populace	k1gFnSc1	populace
se	se	k3xPyFc4	se
z	z	k7c2	z
550	[number]	k4	550
milionů	milion	k4xCgInPc2	milion
téměř	téměř	k6eAd1	téměř
zdvojnásobila	zdvojnásobit	k5eAaPmAgFnS	zdvojnásobit
na	na	k7c4	na
více	hodně	k6eAd2	hodně
než	než	k8xS	než
900	[number]	k4	900
milionů	milion	k4xCgInPc2	milion
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
Mao	Mao	k1gFnSc7	Mao
Ce-tungův	Ceungův	k2eAgInSc1d1	Ce-tungův
rozsáhlý	rozsáhlý	k2eAgInSc1d1	rozsáhlý
projekt	projekt	k1gInSc1	projekt
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
a	a	k8xC	a
sociální	sociální	k2eAgFnSc2d1	sociální
reformy	reforma	k1gFnSc2	reforma
tzv.	tzv.	kA	tzv.
Velký	velký	k2eAgInSc1d1	velký
skok	skok	k1gInSc1	skok
vpřed	vpřed	k6eAd1	vpřed
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1958	[number]	k4	1958
a	a	k8xC	a
1961	[number]	k4	1961
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
okolo	okolo	k7c2	okolo
45	[number]	k4	45
milionů	milion	k4xCgInPc2	milion
úmrtí	úmrtí	k1gNnPc2	úmrtí
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
následkem	následkem	k7c2	následkem
hladomoru	hladomor	k1gInSc2	hladomor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
Mao	Mao	k1gMnSc1	Mao
Ce-tung	Ceung	k1gMnSc1	Ce-tung
inicioval	iniciovat	k5eAaBmAgMnS	iniciovat
Velkou	velký	k2eAgFnSc4d1	velká
proletářskou	proletářský	k2eAgFnSc4d1	proletářská
kulturní	kulturní	k2eAgFnSc4d1	kulturní
revoluci	revoluce	k1gFnSc4	revoluce
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
postavena	postavit	k5eAaPmNgFnS	postavit
na	na	k7c6	na
myšlence	myšlenka	k1gFnSc6	myšlenka
nepřetržité	přetržitý	k2eNgFnSc2d1	nepřetržitá
revoluce	revoluce	k1gFnSc2	revoluce
a	a	k8xC	a
masové	masový	k2eAgFnSc2d1	masová
demokracie	demokracie	k1gFnSc2	demokracie
<g/>
,	,	kIx,	,
a	a	k8xC	a
měla	mít	k5eAaImAgFnS	mít
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
vymýtit	vymýtit	k5eAaPmF	vymýtit
kapitalismus	kapitalismus	k1gInSc4	kapitalismus
uvnitř	uvnitř	k7c2	uvnitř
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
Maovým	Maový	k2eAgInSc7d1	Maový
důvodem	důvod	k1gInSc7	důvod
pro	pro	k7c4	pro
uskutečnění	uskutečnění	k1gNnSc4	uskutečnění
revoluce	revoluce	k1gFnSc2	revoluce
však	však	k9	však
patrně	patrně	k6eAd1	patrně
bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1959	[number]	k4	1959
postupně	postupně	k6eAd1	postupně
ztrácel	ztrácet	k5eAaImAgMnS	ztrácet
vliv	vliv	k1gInSc4	vliv
ve	v	k7c6	v
vedení	vedení	k1gNnSc6	vedení
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
se	se	k3xPyFc4	se
chtěl	chtít	k5eAaImAgInS	chtít
zbavit	zbavit	k5eAaPmF	zbavit
svých	svůj	k3xOyFgMnPc2	svůj
ideologických	ideologický	k2eAgMnPc2d1	ideologický
odpůrců	odpůrce	k1gMnPc2	odpůrce
a	a	k8xC	a
osobních	osobní	k2eAgMnPc2d1	osobní
nepřátel	nepřítel	k1gMnPc2	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Kampaň	kampaň	k1gFnSc1	kampaň
byla	být	k5eAaImAgFnS	být
zaměřena	zaměřit	k5eAaPmNgFnS	zaměřit
proti	proti	k7c3	proti
čtyřem	čtyři	k4xCgInPc3	čtyři
přežitkům	přežitek	k1gInPc3	přežitek
-	-	kIx~	-
starému	starý	k2eAgNnSc3d1	staré
myšlení	myšlení	k1gNnSc3	myšlení
<g/>
,	,	kIx,	,
kultuře	kultura	k1gFnSc3	kultura
<g/>
,	,	kIx,	,
obyčejům	obyčej	k1gInPc3	obyčej
a	a	k8xC	a
návykům	návyk	k1gInPc3	návyk
<g/>
.	.	kIx.	.
</s>
<s>
Hnací	hnací	k2eAgFnSc7d1	hnací
silou	síla	k1gFnSc7	síla
Maovy	Maova	k1gFnSc2	Maova
revoluce	revoluce	k1gFnSc2	revoluce
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
mládež	mládež	k1gFnSc1	mládež
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vytvářela	vytvářet	k5eAaImAgFnS	vytvářet
tzv.	tzv.	kA	tzv.
rudé	rudý	k2eAgFnSc2d1	rudá
gardy	garda	k1gFnSc2	garda
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgFnPc7d1	hlavní
oběťmi	oběť	k1gFnPc7	oběť
kampaně	kampaň	k1gFnSc2	kampaň
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
Liou	Lioum	k1gNnSc3	Lioum
Šao-čchi	Šao-čchi	k1gNnSc2	Šao-čchi
a	a	k8xC	a
Teng	Teng	k1gMnSc1	Teng
Siao-pching	Siaoching	k1gInSc1	Siao-pching
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
zahraniční	zahraniční	k2eAgMnPc1d1	zahraniční
diplomaté	diplomat	k1gMnPc1	diplomat
a	a	k8xC	a
veškeré	veškerý	k3xTgFnSc2	veškerý
autority	autorita	k1gFnSc2	autorita
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
obrovským	obrovský	k2eAgFnPc3d1	obrovská
škodám	škoda	k1gFnPc3	škoda
na	na	k7c6	na
kulturním	kulturní	k2eAgNnSc6d1	kulturní
dědictví	dědictví	k1gNnSc6	dědictví
(	(	kIx(	(
<g/>
byly	být	k5eAaImAgFnP	být
páleny	pálen	k2eAgFnPc1d1	pálena
knihy	kniha	k1gFnPc1	kniha
<g/>
,	,	kIx,	,
vypleněna	vypleněn	k2eAgNnPc1d1	vypleněno
muzea	muzeum	k1gNnPc1	muzeum
a	a	k8xC	a
ničeny	ničen	k2eAgFnPc1d1	ničena
historické	historický	k2eAgFnPc1d1	historická
budovy	budova	k1gFnPc1	budova
<g/>
)	)	kIx)	)
a	a	k8xC	a
země	země	k1gFnSc1	země
se	se	k3xPyFc4	se
zmítala	zmítat	k5eAaImAgFnS	zmítat
v	v	k7c6	v
chaosu	chaos	k1gInSc6	chaos
<g/>
.	.	kIx.	.
</s>
<s>
Kampaň	kampaň	k1gFnSc1	kampaň
trvala	trvat	k5eAaImAgFnS	trvat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
její	její	k3xOp3gInPc1	její
výsledky	výsledek	k1gInPc1	výsledek
přetrvaly	přetrvat	k5eAaPmAgInP	přetrvat
do	do	k7c2	do
Maovy	Maova	k1gFnSc2	Maova
smrti	smrt	k1gFnSc2	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1984	[number]	k4	1984
<g/>
,	,	kIx,	,
po	po	k7c6	po
31	[number]	k4	31
měsících	měsíc	k1gInPc6	měsíc
intenzivního	intenzivní	k2eAgNnSc2d1	intenzivní
vyšetřování	vyšetřování	k1gNnSc2	vyšetřování
<g/>
,	,	kIx,	,
ověřování	ověřování	k1gNnSc2	ověřování
a	a	k8xC	a
přehodnocování	přehodnocování	k1gNnSc2	přehodnocování
Ústředním	ústřední	k2eAgInSc7d1	ústřední
výborem	výbor	k1gInSc7	výbor
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
počty	počet	k1gInPc4	počet
vztahující	vztahující	k2eAgInPc4d1	vztahující
se	se	k3xPyFc4	se
k	k	k7c3	k
Velké	velký	k2eAgFnSc3d1	velká
kulturní	kulturní	k2eAgFnSc3d1	kulturní
revoluci	revoluce	k1gFnSc3	revoluce
tyto	tento	k3xDgMnPc4	tento
<g/>
:	:	kIx,	:
4,2	[number]	k4	4,2
milionu	milion	k4xCgInSc2	milion
lidí	člověk	k1gMnPc2	člověk
bylo	být	k5eAaImAgNnS	být
zatčeno	zatknout	k5eAaPmNgNnS	zatknout
a	a	k8xC	a
vyšetřováno	vyšetřovat	k5eAaImNgNnS	vyšetřovat
<g/>
,	,	kIx,	,
1,7	[number]	k4	1,7
milionu	milion	k4xCgInSc2	milion
lidí	člověk	k1gMnPc2	člověk
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
nepřirozenou	přirozený	k2eNgFnSc7d1	nepřirozená
smrtí	smrt	k1gFnSc7	smrt
<g/>
,	,	kIx,	,
135	[number]	k4	135
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
bylo	být	k5eAaImAgNnS	být
označeno	označit	k5eAaPmNgNnS	označit
za	za	k7c4	za
kontrarevolucionáře	kontrarevolucionář	k1gMnPc4	kontrarevolucionář
a	a	k8xC	a
popraveno	popraven	k2eAgNnSc4d1	popraveno
<g/>
,	,	kIx,	,
237	[number]	k4	237
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
bylo	být	k5eAaImAgNnS	být
zabito	zabít	k5eAaPmNgNnS	zabít
<g/>
,	,	kIx,	,
7,03	[number]	k4	7,03
milionu	milion	k4xCgInSc2	milion
lidí	člověk	k1gMnPc2	člověk
bylo	být	k5eAaImAgNnS	být
zraněno	zranit	k5eAaPmNgNnS	zranit
nebo	nebo	k8xC	nebo
zmrzačeno	zmrzačit	k5eAaPmNgNnS	zmrzačit
při	při	k7c6	při
ozbrojených	ozbrojený	k2eAgInPc6d1	ozbrojený
útocích	útok	k1gInPc6	útok
a	a	k8xC	a
71	[number]	k4	71
200	[number]	k4	200
rodin	rodina	k1gFnPc2	rodina
bylo	být	k5eAaImAgNnS	být
rozbito	rozbít	k5eAaPmNgNnS	rozbít
<g/>
.	.	kIx.	.
</s>
<s>
Statistiky	statistika	k1gFnPc1	statistika
sestavené	sestavený	k2eAgFnPc1d1	sestavená
ze	z	k7c2	z
zdrojů	zdroj	k1gInPc2	zdroj
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
krajů	kraj	k1gInPc2	kraj
uvádějí	uvádět	k5eAaImIp3nP	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
revoluce	revoluce	k1gFnSc2	revoluce
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
7,73	[number]	k4	7,73
milionu	milion	k4xCgInSc2	milion
lidí	člověk	k1gMnPc2	člověk
nepřirozenou	přirozený	k2eNgFnSc7d1	nepřirozená
smrtí	smrt	k1gFnSc7	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Mao	Mao	k1gMnSc1	Mao
Ce-tung	Ceung	k1gMnSc1	Ce-tung
je	být	k5eAaImIp3nS	být
zodpovědný	zodpovědný	k2eAgMnSc1d1	zodpovědný
za	za	k7c4	za
smrt	smrt	k1gFnSc4	smrt
40	[number]	k4	40
až	až	k9	až
70	[number]	k4	70
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
hladu	hlad	k1gInSc2	hlad
<g/>
,	,	kIx,	,
nucených	nucený	k2eAgFnPc2d1	nucená
prací	práce	k1gFnPc2	práce
a	a	k8xC	a
poprav	poprava	k1gFnPc2	poprava
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
byl	být	k5eAaImAgInS	být
prakticky	prakticky	k6eAd1	prakticky
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
čínské	čínský	k2eAgFnSc6d1	čínská
vesnici	vesnice	k1gFnSc6	vesnice
určen	určit	k5eAaPmNgMnS	určit
pro	pro	k7c4	pro
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
popravu	poprava	k1gFnSc4	poprava
alespoň	alespoň	k9	alespoň
jeden	jeden	k4xCgMnSc1	jeden
statkář	statkář	k1gMnSc1	statkář
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
obvykle	obvykle	k6eAd1	obvykle
několik	několik	k4yIc1	několik
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
si	se	k3xPyFc3	se
vyžádalo	vyžádat	k5eAaPmAgNnS	vyžádat
odhadem	odhad	k1gInSc7	odhad
2	[number]	k4	2
až	až	k8xS	až
5	[number]	k4	5
milionů	milion	k4xCgInPc2	milion
obětí	oběť	k1gFnPc2	oběť
<g/>
.	.	kIx.	.
</s>
<s>
Kulturní	kulturní	k2eAgFnSc1d1	kulturní
revoluce	revoluce	k1gFnSc1	revoluce
skončila	skončit	k5eAaPmAgFnS	skončit
až	až	k9	až
smrtí	smrt	k1gFnSc7	smrt
Mao	Mao	k1gMnSc2	Mao
Ce-tunga	Ceung	k1gMnSc2	Ce-tung
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následném	následný	k2eAgInSc6d1	následný
boji	boj	k1gInSc6	boj
o	o	k7c4	o
moc	moc	k1gFnSc4	moc
neuspěla	uspět	k5eNaPmAgFnS	uspět
tzv.	tzv.	kA	tzv.
banda	banda	k1gFnSc1	banda
čtyř	čtyři	k4xCgMnPc2	čtyři
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
měla	mít	k5eAaImAgFnS	mít
velký	velký	k2eAgInSc4d1	velký
podíl	podíl	k1gInSc4	podíl
na	na	k7c4	na
kulturní	kulturní	k2eAgFnSc4d1	kulturní
revoluci	revoluce	k1gFnSc4	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
obviněna	obvinit	k5eAaPmNgFnS	obvinit
z	z	k7c2	z
uvedení	uvedení	k1gNnSc2	uvedení
země	zem	k1gFnSc2	zem
do	do	k7c2	do
chaosu	chaos	k1gInSc2	chaos
a	a	k8xC	a
všichni	všechen	k3xTgMnPc1	všechen
členové	člen	k1gMnPc1	člen
odsouzeni	odsouzet	k5eAaImNgMnP	odsouzet
k	k	k7c3	k
vysokým	vysoký	k2eAgInPc3d1	vysoký
trestům	trest	k1gInPc3	trest
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
státu	stát	k1gInSc2	stát
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
proreformní	proreformní	k2eAgNnSc1d1	proreformní
křídlo	křídlo	k1gNnSc1	křídlo
KS	ks	kA	ks
Číny	Čína	k1gFnSc2	Čína
reprezentované	reprezentovaný	k2eAgFnSc2d1	reprezentovaná
Teng	Teng	k1gMnSc1	Teng
Siao-pchingem	Siaoching	k1gInSc7	Siao-pching
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
postupně	postupně	k6eAd1	postupně
převzal	převzít	k5eAaPmAgInS	převzít
veškerou	veškerý	k3xTgFnSc4	veškerý
moc	moc	k1gFnSc4	moc
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
oteplování	oteplování	k1gNnSc3	oteplování
vztahů	vztah	k1gInPc2	vztah
se	s	k7c7	s
Západem	západ	k1gInSc7	západ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
letech	let	k1gInPc6	let
proběhly	proběhnout	k5eAaPmAgFnP	proběhnout
mnohé	mnohý	k2eAgFnPc1d1	mnohá
reformy	reforma	k1gFnPc1	reforma
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
nímž	jenž	k3xRgMnSc7	jenž
se	se	k3xPyFc4	se
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
ekonomické	ekonomický	k2eAgFnSc6d1	ekonomická
oblasti	oblast	k1gFnSc6	oblast
země	zem	k1gFnSc2	zem
postupně	postupně	k6eAd1	postupně
přiblížila	přiblížit	k5eAaPmAgFnS	přiblížit
více	hodně	k6eAd2	hodně
Západu	západ	k1gInSc2	západ
<g/>
.	.	kIx.	.
</s>
<s>
Zřízeny	zřízen	k2eAgFnPc1d1	zřízena
byly	být	k5eAaImAgFnP	být
například	například	k6eAd1	například
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
Zvláštní	zvláštní	k2eAgFnPc1d1	zvláštní
ekonomické	ekonomický	k2eAgFnPc1d1	ekonomická
zóny	zóna	k1gFnPc1	zóna
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
umožněno	umožnit	k5eAaPmNgNnS	umožnit
podnikání	podnikání	k1gNnSc1	podnikání
západním	západní	k2eAgMnSc7d1	západní
investorům	investor	k1gMnPc3	investor
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
ale	ale	k8xC	ale
naopak	naopak	k6eAd1	naopak
zavedena	zaveden	k2eAgFnSc1d1	zavedena
politika	politika	k1gFnSc1	politika
jednoho	jeden	k4xCgNnSc2	jeden
dítěte	dítě	k1gNnSc2	dítě
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ve	v	k7c6	v
výsledku	výsledek	k1gInSc6	výsledek
přinesla	přinést	k5eAaPmAgFnS	přinést
mnohé	mnohý	k2eAgInPc4d1	mnohý
negativní	negativní	k2eAgInPc4d1	negativní
vlivy	vliv	k1gInPc4	vliv
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
nepoměru	nepoměr	k1gInSc2	nepoměr
mezi	mezi	k7c7	mezi
pohlavími	pohlaví	k1gNnPc7	pohlaví
<g/>
,	,	kIx,	,
stárnutí	stárnutí	k1gNnSc1	stárnutí
populace	populace	k1gFnSc2	populace
a	a	k8xC	a
úbytku	úbytek	k1gInSc3	úbytek
pracovní	pracovní	k2eAgFnSc2d1	pracovní
síly	síla	k1gFnSc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
došlo	dojít	k5eAaPmAgNnS	dojít
po	po	k7c6	po
narůstající	narůstající	k2eAgFnSc6d1	narůstající
nespokojenosti	nespokojenost	k1gFnSc6	nespokojenost
k	k	k7c3	k
protestům	protest	k1gInPc3	protest
studentů	student	k1gMnPc2	student
proti	proti	k7c3	proti
vládě	vláda	k1gFnSc3	vláda
KS	ks	kA	ks
Číny	Čína	k1gFnSc2	Čína
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
Nebeského	nebeský	k2eAgInSc2d1	nebeský
klidu	klid	k1gInSc2	klid
v	v	k7c6	v
metropoli	metropol	k1gFnSc6	metropol
Pekingu	Peking	k1gInSc2	Peking
<g/>
,	,	kIx,	,
ty	ten	k3xDgFnPc1	ten
však	však	k9	však
byly	být	k5eAaImAgFnP	být
tvrdě	tvrdě	k6eAd1	tvrdě
potlačeny	potlačit	k5eAaPmNgFnP	potlačit
zásahem	zásah	k1gInSc7	zásah
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
uvrhly	uvrhnout	k5eAaPmAgFnP	uvrhnout
ČLR	ČLR	kA	ČLR
do	do	k7c2	do
přechodné	přechodný	k2eAgFnSc2d1	přechodná
izolace	izolace	k1gFnSc2	izolace
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
demokratizaci	demokratizace	k1gFnSc3	demokratizace
a	a	k8xC	a
zavedení	zavedení	k1gNnSc3	zavedení
pluralitního	pluralitní	k2eAgInSc2d1	pluralitní
systému	systém	k1gInSc2	systém
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
,	,	kIx,	,
komunistické	komunistický	k2eAgNnSc1d1	komunistické
vedení	vedení	k1gNnSc1	vedení
země	zem	k1gFnSc2	zem
se	se	k3xPyFc4	se
však	však	k9	však
generačně	generačně	k6eAd1	generačně
obměnilo	obměnit	k5eAaPmAgNnS	obměnit
a	a	k8xC	a
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
nadále	nadále	k6eAd1	nadále
v	v	k7c6	v
hospodářských	hospodářský	k2eAgFnPc6d1	hospodářská
reformách	reforma	k1gFnPc6	reforma
<g/>
.	.	kIx.	.
</s>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1997	[number]	k4	1997
byl	být	k5eAaImAgMnS	být
k	k	k7c3	k
Čínské	čínský	k2eAgFnSc3d1	čínská
lidové	lidový	k2eAgFnSc3d1	lidová
republice	republika	k1gFnSc3	republika
připojen	připojen	k2eAgInSc1d1	připojen
Hong	Hong	k1gInSc1	Hong
Kong	Kongo	k1gNnPc2	Kongo
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
pod	pod	k7c7	pod
správou	správa	k1gFnSc7	správa
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
podmínkou	podmínka	k1gFnSc7	podmínka
"	"	kIx"	"
<g/>
jeden	jeden	k4xCgInSc1	jeden
stát	stát	k1gInSc1	stát
<g/>
,	,	kIx,	,
dva	dva	k4xCgInPc1	dva
systémy	systém	k1gInPc1	systém
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
20	[number]	k4	20
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1999	[number]	k4	1999
bylo	být	k5eAaImAgNnS	být
pod	pod	k7c7	pod
stejnými	stejný	k2eAgFnPc7d1	stejná
podmínkami	podmínka	k1gFnPc7	podmínka
připojeno	připojit	k5eAaPmNgNnS	připojit
Macao	Macao	k1gNnSc1	Macao
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
pod	pod	k7c7	pod
správou	správa	k1gFnSc7	správa
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
výdajích	výdaj	k1gInPc6	výdaj
na	na	k7c4	na
ozbrojené	ozbrojený	k2eAgFnPc4d1	ozbrojená
síly	síla	k1gFnPc4	síla
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
Čína	Čína	k1gFnSc1	Čína
na	na	k7c4	na
druhé	druhý	k4xOgNnSc4	druhý
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
hned	hned	k6eAd1	hned
za	za	k7c4	za
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
<g/>
,	,	kIx,	,
a	a	k8xC	a
výrazně	výrazně	k6eAd1	výrazně
začala	začít	k5eAaPmAgFnS	začít
modernizovat	modernizovat	k5eAaBmF	modernizovat
všechny	všechen	k3xTgFnPc4	všechen
složky	složka	k1gFnPc4	složka
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
současné	současný	k2eAgFnSc2d1	současná
administrativy	administrativa	k1gFnSc2	administrativa
Chu	Chu	k1gMnSc1	Chu
Ťin-tchaa	Ťinchaa	k1gMnSc1	Ťin-tchaa
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vládne	vládnout	k5eAaImIp3nS	vládnout
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
začaly	začít	k5eAaPmAgFnP	začít
být	být	k5eAaImF	být
řešeny	řešen	k2eAgInPc4d1	řešen
problémy	problém	k1gInPc4	problém
zejména	zejména	k9	zejména
v	v	k7c6	v
sociální	sociální	k2eAgFnSc6d1	sociální
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
koncept	koncept	k1gInSc4	koncept
s	s	k7c7	s
názvem	název	k1gInSc7	název
harmonická	harmonický	k2eAgFnSc1d1	harmonická
společnost	společnost	k1gFnSc1	společnost
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
slibuje	slibovat	k5eAaImIp3nS	slibovat
fúzi	fúze	k1gFnSc4	fúze
socialismu	socialismus	k1gInSc2	socialismus
a	a	k8xC	a
demokracie	demokracie	k1gFnSc2	demokracie
<g/>
,	,	kIx,	,
podporu	podpora	k1gFnSc4	podpora
střední	střední	k2eAgFnSc2d1	střední
třídy	třída	k1gFnSc2	třída
<g/>
,	,	kIx,	,
vládu	vláda	k1gFnSc4	vláda
práva	právo	k1gNnSc2	právo
<g/>
,	,	kIx,	,
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
prvky	prvek	k1gInPc4	prvek
nového	nový	k2eAgNnSc2d1	nové
konfuciánství	konfuciánství	k1gNnSc2	konfuciánství
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
administrativa	administrativa	k1gFnSc1	administrativa
byla	být	k5eAaImAgFnS	být
vůbec	vůbec	k9	vůbec
první	první	k4xOgFnSc7	první
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
ČLR	ČLR	kA	ČLR
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
začala	začít	k5eAaPmAgFnS	začít
otevřeně	otevřeně	k6eAd1	otevřeně
podporovat	podporovat	k5eAaImF	podporovat
náboženství	náboženství	k1gNnSc4	náboženství
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
buddhismus	buddhismus	k1gInSc1	buddhismus
a	a	k8xC	a
taoismus	taoismus	k1gInSc1	taoismus
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vidí	vidět	k5eAaImIp3nS	vidět
jako	jako	k9	jako
tradiční	tradiční	k2eAgFnSc4d1	tradiční
součást	součást	k1gFnSc4	součást
čínské	čínský	k2eAgFnSc2d1	čínská
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
zahraniční	zahraniční	k2eAgMnPc1d1	zahraniční
pozorovatelé	pozorovatel	k1gMnPc1	pozorovatel
ovšem	ovšem	k9	ovšem
označují	označovat	k5eAaImIp3nP	označovat
aktivitu	aktivita	k1gFnSc4	aktivita
čínské	čínský	k2eAgFnSc2d1	čínská
vlády	vláda	k1gFnSc2	vláda
za	za	k7c4	za
další	další	k2eAgInSc4d1	další
pokus	pokus	k1gInSc4	pokus
o	o	k7c4	o
namalování	namalování	k1gNnSc4	namalování
falešného	falešný	k2eAgInSc2d1	falešný
obrazu	obraz	k1gInSc2	obraz
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
neustále	neustále	k6eAd1	neustále
probíhají	probíhat	k5eAaImIp3nP	probíhat
masové	masový	k2eAgFnPc4d1	masová
represe	represe	k1gFnPc4	represe
<g/>
,	,	kIx,	,
cenzura	cenzura	k1gFnSc1	cenzura
informací	informace	k1gFnPc2	informace
a	a	k8xC	a
zabíjení	zabíjení	k1gNnSc2	zabíjení
jako	jako	k9	jako
v	v	k7c6	v
předchozích	předchozí	k2eAgNnPc6d1	předchozí
obdobích	období	k1gNnPc6	období
<g/>
.	.	kIx.	.
</s>
<s>
Pokračují	pokračovat	k5eAaImIp3nP	pokračovat
také	také	k9	také
restrikce	restrikce	k1gFnPc1	restrikce
proti	proti	k7c3	proti
určitým	určitý	k2eAgFnPc3d1	určitá
náboženským	náboženský	k2eAgFnPc3d1	náboženská
skupinám	skupina	k1gFnPc3	skupina
proti	proti	k7c3	proti
nimž	jenž	k3xRgMnPc3	jenž
trvá	trvat	k5eAaImIp3nS	trvat
takzvaný	takzvaný	k2eAgInSc4d1	takzvaný
"	"	kIx"	"
<g/>
boj	boj	k1gInSc4	boj
proti	proti	k7c3	proti
zlým	zlý	k2eAgInPc3d1	zlý
kultům	kult	k1gInPc3	kult
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
Strana	strana	k1gFnSc1	strana
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
zasažena	zasažen	k2eAgNnPc1d1	zasaženo
jsou	být	k5eAaImIp3nP	být
veškerá	veškerý	k3xTgNnPc1	veškerý
náboženství	náboženství	k1gNnPc1	náboženství
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
se	se	k3xPyFc4	se
nepodřizují	podřizovat	k5eNaImIp3nP	podřizovat
Straně	strana	k1gFnSc3	strana
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
křesťané	křesťan	k1gMnPc1	křesťan
loajální	loajální	k2eAgFnSc2d1	loajální
papeži	papež	k1gMnSc3	papež
<g/>
,	,	kIx,	,
buddhisté	buddhista	k1gMnPc1	buddhista
loajální	loajální	k2eAgFnSc2d1	loajální
dalajlámovi	dalajláma	k1gMnSc3	dalajláma
<g/>
,	,	kIx,	,
Falun	Falun	k1gInSc1	Falun
Gong	gong	k1gInSc1	gong
loajální	loajální	k2eAgInPc1d1	loajální
k	k	k7c3	k
zakladateli	zakladatel	k1gMnSc6	zakladatel
Li	li	k8xS	li
Chung-č	Chung-č	k1gInSc4	Chung-č
<g/>
'	'	kIx"	'
<g/>
ovi	ovi	k?	ovi
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
,	,	kIx,	,
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
také	také	k9	také
sporná	sporný	k2eAgFnSc1d1	sporná
otázka	otázka	k1gFnSc1	otázka
Tibetu	Tibet	k1gInSc2	Tibet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
Tibetu	Tibet	k1gInSc6	Tibet
krvavě	krvavě	k6eAd1	krvavě
potlačeny	potlačen	k2eAgFnPc1d1	potlačena
údajné	údajný	k2eAgFnPc1d1	údajná
protičínské	protičínský	k2eAgFnPc1d1	protičínská
demonstrace	demonstrace	k1gFnPc1	demonstrace
<g/>
,	,	kIx,	,
různé	různý	k2eAgInPc1d1	různý
zdroje	zdroj	k1gInPc1	zdroj
pokazují	pokazovat	k5eAaImIp3nP	pokazovat
na	na	k7c4	na
angažování	angažování	k1gNnSc4	angažování
čínské	čínský	k2eAgFnSc2d1	čínská
vlády	vláda	k1gFnSc2	vláda
ve	v	k7c6	v
vyprovokování	vyprovokování	k1gNnSc6	vyprovokování
a	a	k8xC	a
řízení	řízení	k1gNnSc6	řízení
nepokojů	nepokoj	k1gInPc2	nepokoj
<g/>
.	.	kIx.	.
</s>
<s>
Exilový	exilový	k2eAgMnSc1d1	exilový
tibetský	tibetský	k2eAgMnSc1d1	tibetský
duchovní	duchovní	k1gMnSc1	duchovní
vůdce	vůdce	k1gMnSc1	vůdce
dalajláma	dalajláma	k1gMnSc1	dalajláma
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vzpoura	vzpoura	k1gFnSc1	vzpoura
ve	v	k7c6	v
Lhase	Lhasa	k1gFnSc6	Lhasa
byla	být	k5eAaImAgFnS	být
zinscenována	zinscenovat	k5eAaPmNgFnS	zinscenovat
čínskou	čínský	k2eAgFnSc7d1	čínská
policií	policie	k1gFnSc7	policie
<g/>
..	..	k?	..
Režimem	režim	k1gInSc7	režim
propagovaná	propagovaný	k2eAgFnSc1d1	propagovaná
"	"	kIx"	"
<g/>
harmonická	harmonický	k2eAgFnSc1d1	harmonická
společnost	společnost	k1gFnSc1	společnost
<g/>
"	"	kIx"	"
a	a	k8xC	a
vláda	vláda	k1gFnSc1	vláda
práva	právo	k1gNnSc2	právo
je	být	k5eAaImIp3nS	být
vzhledem	vzhled	k1gInSc7	vzhled
k	k	k7c3	k
skutečným	skutečný	k2eAgFnPc3d1	skutečná
událostem	událost	k1gFnPc3	událost
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
chápána	chápat	k5eAaImNgFnS	chápat
rovněž	rovněž	k6eAd1	rovněž
jako	jako	k8xS	jako
pouhý	pouhý	k2eAgInSc4d1	pouhý
politický	politický	k2eAgInSc4d1	politický
manévr	manévr	k1gInSc4	manévr
na	na	k7c4	na
okrášlení	okrášlení	k1gNnSc4	okrášlení
obrazu	obraz	k1gInSc2	obraz
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Čínský	čínský	k2eAgMnSc1d1	čínský
právník	právník	k1gMnSc1	právník
Kao	Kao	k1gFnSc2	Kao
Č	Č	kA	Č
<g/>
'	'	kIx"	'
<g/>
-šeng	-šeng	k1gMnSc1	-šeng
<g/>
..	..	k?	..
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
nezvěstný	zvěstný	k2eNgMnSc1d1	nezvěstný
<g/>
,	,	kIx,	,
prodělal	prodělat	k5eAaPmAgMnS	prodělat
mučení	mučení	k1gNnSc4	mučení
čínskou	čínský	k2eAgFnSc7d1	čínská
tajnou	tajný	k2eAgFnSc7d1	tajná
policií	policie	k1gFnSc7	policie
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterém	který	k3yQgInSc6	který
napsal	napsat	k5eAaBmAgMnS	napsat
do	do	k7c2	do
sněmovny	sněmovna	k1gFnSc2	sněmovna
USA	USA	kA	USA
svoji	svůj	k3xOyFgFnSc4	svůj
výpověď	výpověď	k1gFnSc4	výpověď
<g/>
.	.	kIx.	.
</s>
<s>
Právníci	právník	k1gMnPc1	právník
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
hájí	hájit	k5eAaImIp3nP	hájit
vládou	vláda	k1gFnSc7	vláda
pronásledované	pronásledovaný	k2eAgFnSc2d1	pronásledovaná
duchovní	duchovní	k2eAgFnSc2d1	duchovní
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
Falun	Faluna	k1gFnPc2	Faluna
Gongu	gong	k1gInSc2	gong
jsou	být	k5eAaImIp3nP	být
pronásledováni	pronásledovat	k5eAaImNgMnP	pronásledovat
a	a	k8xC	a
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
propukávají	propukávat	k5eAaImIp3nP	propukávat
deseti	deset	k4xCc7	deset
tisíce	tisíc	k4xCgInSc2	tisíc
nepokojů	nepokoj	k1gInPc2	nepokoj
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
i	i	k9	i
ekonomický	ekonomický	k2eAgInSc1d1	ekonomický
růst	růst	k1gInSc1	růst
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
znalců	znalec	k1gMnPc2	znalec
vykoupen	vykoupen	k2eAgMnSc1d1	vykoupen
devastací	devastace	k1gFnSc7	devastace
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
a	a	k8xC	a
pošlapáváním	pošlapávání	k1gNnSc7	pošlapávání
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Skutečná	skutečný	k2eAgFnSc1d1	skutečná
demokracie	demokracie	k1gFnSc1	demokracie
a	a	k8xC	a
vláda	vláda	k1gFnSc1	vláda
práva	právo	k1gNnSc2	právo
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
svoboda	svoboda	k1gFnSc1	svoboda
projevu	projev	k1gInSc2	projev
<g/>
,	,	kIx,	,
svoboda	svoboda	k1gFnSc1	svoboda
víry	víra	k1gFnSc2	víra
a	a	k8xC	a
šíření	šíření	k1gNnSc2	šíření
informací	informace	k1gFnPc2	informace
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
fakticky	fakticky	k6eAd1	fakticky
povolována	povolován	k2eAgFnSc1d1	povolována
a	a	k8xC	a
ani	ani	k8xC	ani
umožněna	umožněn	k2eAgFnSc1d1	umožněna
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
čínských	čínský	k2eAgInPc6d1	čínský
zákonech	zákon	k1gInPc6	zákon
ustavena	ustavit	k5eAaPmNgFnS	ustavit
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Geografie	geografie	k1gFnSc2	geografie
Číny	Čína	k1gFnSc2	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Čínská	čínský	k2eAgFnSc1d1	čínská
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Asii	Asie	k1gFnSc6	Asie
při	při	k7c6	při
západním	západní	k2eAgNnSc6d1	západní
pobřeží	pobřeží	k1gNnSc6	pobřeží
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
<g/>
Od	od	k7c2	od
severu	sever	k1gInSc2	sever
k	k	k7c3	k
jihu	jih	k1gInSc3	jih
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
od	od	k7c2	od
řeky	řeka	k1gFnSc2	řeka
Chej-lung-ťiang	Chejung-ťiang	k1gInSc1	Chej-lung-ťiang
(	(	kIx(	(
<g/>
Amur	Amur	k1gInSc1	Amur
<g/>
)	)	kIx)	)
na	na	k7c4	na
53	[number]	k4	53
<g/>
o	o	k7c6	o
s.	s.	k?	s.
<g/>
š.	š.	k?	š.
k	k	k7c3	k
nejjižnějšímu	jižní	k2eAgInSc3d3	nejjižnější
cípu	cíp	k1gInSc3	cíp
ostrovů	ostrov	k1gInPc2	ostrov
Nan-ša	Nan-š	k1gInSc2	Nan-š
<g/>
/	/	kIx~	/
<g/>
Spratlyovy	Spratlyův	k2eAgInPc4d1	Spratlyův
ostrovy	ostrov	k1gInPc4	ostrov
na	na	k7c4	na
18	[number]	k4	18
<g/>
o	o	k7c6	o
s.	s.	k?	s.
<g/>
š.	š.	k?	š.
Spojnice	spojnice	k1gFnSc1	spojnice
měří	měřit	k5eAaImIp3nS	měřit
kolem	kolem	k7c2	kolem
5	[number]	k4	5
500	[number]	k4	500
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Pamíru	Pamír	k1gInSc2	Pamír
na	na	k7c6	na
západě	západ	k1gInSc6	západ
(	(	kIx(	(
<g/>
71	[number]	k4	71
<g/>
o	o	k7c6	o
v.d.	v.d.	k?	v.d.
<g/>
)	)	kIx)	)
po	po	k7c4	po
soutok	soutok	k1gInSc4	soutok
řek	řeka	k1gFnPc2	řeka
Chej-lung-ťiang	Chejung-ťianga	k1gFnPc2	Chej-lung-ťianga
a	a	k8xC	a
Ussuri	Ussuri	k1gNnPc2	Ussuri
(	(	kIx(	(
<g/>
135	[number]	k4	135
<g/>
o	o	k7c6	o
v.d.	v.d.	k?	v.d.
<g/>
)	)	kIx)	)
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
spojnice	spojnice	k1gFnSc1	spojnice
5	[number]	k4	5
200	[number]	k4	200
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Časový	časový	k2eAgInSc1d1	časový
rozdíl	rozdíl	k1gInSc1	rozdíl
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
než	než	k8xS	než
čtyři	čtyři	k4xCgFnPc4	čtyři
hodiny	hodina	k1gFnPc4	hodina
<g/>
,	,	kIx,	,
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
státě	stát	k1gInSc6	stát
se	se	k3xPyFc4	se
však	však	k9	však
používá	používat	k5eAaImIp3nS	používat
jednotný	jednotný	k2eAgInSc1d1	jednotný
čas	čas	k1gInSc1	čas
(	(	kIx(	(
<g/>
UTC	UTC	kA	UTC
<g/>
+	+	kIx~	+
<g/>
8	[number]	k4	8
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ČLR	ČLR	kA	ČLR
přímo	přímo	k6eAd1	přímo
sousedí	sousedit	k5eAaImIp3nS	sousedit
se	s	k7c7	s
14	[number]	k4	14
zeměmi	zem	k1gFnPc7	zem
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
suchozemskou	suchozemský	k2eAgFnSc4d1	suchozemská
hranici	hranice	k1gFnSc4	hranice
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
22	[number]	k4	22
800	[number]	k4	800
km	km	kA	km
a	a	k8xC	a
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
mnohem	mnohem	k6eAd1	mnohem
větším	veliký	k2eAgNnSc7d2	veliký
Ruskem	Rusko	k1gNnSc7	Rusko
<g/>
)	)	kIx)	)
nejvíce	nejvíce	k6eAd1	nejvíce
sousedů	soused	k1gMnPc2	soused
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
států	stát	k1gInPc2	stát
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
směru	směr	k1gInSc6	směr
hodinových	hodinový	k2eAgFnPc2d1	hodinová
ručiček	ručička	k1gFnPc2	ručička
jimi	on	k3xPp3gFnPc7	on
jsou	být	k5eAaImIp3nP	být
Indie	Indie	k1gFnPc1	Indie
(	(	kIx(	(
<g/>
délka	délka	k1gFnSc1	délka
hranice	hranice	k1gFnSc1	hranice
3	[number]	k4	3
380	[number]	k4	380
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Pákistán	Pákistán	k1gInSc1	Pákistán
(	(	kIx(	(
<g/>
523	[number]	k4	523
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Afghánistán	Afghánistán	k1gInSc1	Afghánistán
(	(	kIx(	(
<g/>
76	[number]	k4	76
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tádžikistán	Tádžikistán	k1gInSc1	Tádžikistán
(	(	kIx(	(
<g/>
414	[number]	k4	414
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kyrgyzstán	Kyrgyzstán	k1gInSc1	Kyrgyzstán
(	(	kIx(	(
<g/>
858	[number]	k4	858
km	km	kA	km
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kazachstán	Kazachstán	k1gInSc1	Kazachstán
(	(	kIx(	(
<g/>
1	[number]	k4	1
533	[number]	k4	533
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
(	(	kIx(	(
<g/>
3	[number]	k4	3
645	[number]	k4	645
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mongolsko	Mongolsko	k1gNnSc1	Mongolsko
(	(	kIx(	(
<g/>
4	[number]	k4	4
677	[number]	k4	677
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Severní	severní	k2eAgFnSc1d1	severní
Korea	Korea	k1gFnSc1	Korea
(	(	kIx(	(
<g/>
1	[number]	k4	1
416	[number]	k4	416
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vietnam	Vietnam	k1gInSc1	Vietnam
(	(	kIx(	(
<g/>
1	[number]	k4	1
281	[number]	k4	281
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Laos	Laos	k1gInSc1	Laos
(	(	kIx(	(
<g/>
423	[number]	k4	423
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Myanmar	Myanmar	k1gMnSc1	Myanmar
(	(	kIx(	(
<g/>
2	[number]	k4	2
185	[number]	k4	185
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bhútán	Bhútán	k1gInSc1	Bhútán
(	(	kIx(	(
<g/>
470	[number]	k4	470
km	km	kA	km
<g/>
)	)	kIx)	)
a	a	k8xC	a
Nepál	Nepál	k1gInSc1	Nepál
(	(	kIx(	(
<g/>
1	[number]	k4	1
236	[number]	k4	236
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
moře	moře	k1gNnSc1	moře
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
Čínu	Čína	k1gFnSc4	Čína
od	od	k7c2	od
Jižní	jižní	k2eAgFnSc2d1	jižní
Koreje	Korea	k1gFnSc2	Korea
<g/>
,	,	kIx,	,
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
,	,	kIx,	,
Čínské	čínský	k2eAgFnPc1d1	čínská
republiky	republika	k1gFnPc1	republika
na	na	k7c6	na
Tchaj-wanu	Tchajan	k1gInSc6	Tchaj-wan
<g/>
,	,	kIx,	,
Filipín	Filipíny	k1gFnPc2	Filipíny
<g/>
,	,	kIx,	,
Bruneje	Brunej	k1gInSc2	Brunej
<g/>
,	,	kIx,	,
Malajsie	Malajsie	k1gFnSc2	Malajsie
a	a	k8xC	a
Indonésie	Indonésie	k1gFnSc2	Indonésie
<g/>
.	.	kIx.	.
</s>
<s>
ČLR	ČLR	kA	ČLR
vede	vést	k5eAaImIp3nS	vést
spor	spor	k1gInSc1	spor
o	o	k7c4	o
pozemní	pozemní	k2eAgFnSc4d1	pozemní
hranici	hranice	k1gFnSc4	hranice
s	s	k7c7	s
Indií	Indie	k1gFnSc7	Indie
<g/>
,	,	kIx,	,
spor	spor	k1gInSc1	spor
o	o	k7c6	o
hranici	hranice	k1gFnSc6	hranice
s	s	k7c7	s
Tádžikistánem	Tádžikistán	k1gInSc7	Tádžikistán
<g/>
,	,	kIx,	,
krátký	krátký	k2eAgInSc4d1	krátký
úsek	úsek	k1gInSc4	úsek
hranice	hranice	k1gFnSc2	hranice
s	s	k7c7	s
KLDR	KLDR	kA	KLDR
není	být	k5eNaImIp3nS	být
určen	určit	k5eAaPmNgInS	určit
<g/>
.	.	kIx.	.
</s>
<s>
Pohraniční	pohraniční	k2eAgInPc1d1	pohraniční
spory	spor	k1gInPc1	spor
s	s	k7c7	s
Ruskem	Rusko	k1gNnSc7	Rusko
byly	být	k5eAaImAgFnP	být
urovnány	urovnán	k2eAgFnPc1d1	urovnána
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
východě	východ	k1gInSc6	východ
a	a	k8xC	a
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
omývají	omývat	k5eAaImIp3nP	omývat
čínské	čínský	k2eAgNnSc1d1	čínské
území	území	k1gNnSc1	území
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
směru	směr	k1gInSc6	směr
hodinových	hodinový	k2eAgFnPc2d1	hodinová
ručiček	ručička	k1gFnPc2	ručička
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
Pochajský	Pochajský	k2eAgInSc1d1	Pochajský
záliv	záliv	k1gInSc1	záliv
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
čínské	čínský	k2eAgNnSc4d1	čínské
kontinentální	kontinentální	k2eAgNnSc4d1	kontinentální
moře	moře	k1gNnSc4	moře
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
Žluté	žlutý	k2eAgNnSc4d1	žluté
<g/>
,	,	kIx,	,
Východočínské	východočínský	k2eAgNnSc4d1	Východočínské
a	a	k8xC	a
Jihočínské	jihočínský	k2eAgNnSc4d1	Jihočínské
moře	moře	k1gNnSc4	moře
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc4	jenž
jsou	být	k5eAaImIp3nP	být
okrajovými	okrajový	k2eAgFnPc7d1	okrajová
moři	moře	k1gNnSc6	moře
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
teritoriálních	teritoriální	k2eAgFnPc6d1	teritoriální
vodách	voda	k1gFnPc6	voda
Číny	Čína	k1gFnSc2	Čína
o	o	k7c6	o
celkové	celkový	k2eAgFnSc6d1	celková
rozloze	rozloha	k1gFnSc6	rozloha
4,73	[number]	k4	4,73
milionu	milion	k4xCgInSc2	milion
km2	km2	k4	km2
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
na	na	k7c4	na
5	[number]	k4	5
400	[number]	k4	400
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgMnSc1d3	veliký
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
36	[number]	k4	36
000	[number]	k4	000
km2	km2	k4	km2
je	být	k5eAaImIp3nS	být
Tchaj-wan	Tchajan	k1gInSc1	Tchaj-wan
(	(	kIx(	(
<g/>
ovládán	ovládán	k2eAgInSc1d1	ovládán
je	být	k5eAaImIp3nS	být
však	však	k9	však
Čínskou	čínský	k2eAgFnSc7d1	čínská
republikou	republika	k1gFnSc7	republika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
následovaný	následovaný	k2eAgInSc1d1	následovaný
ostrovem	ostrov	k1gInSc7	ostrov
Chaj-nan	Chajan	k1gMnSc1	Chaj-nan
(	(	kIx(	(
<g/>
34	[number]	k4	34
000	[number]	k4	000
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýchodnějšími	východní	k2eAgInPc7d3	nejvýchodnější
čínskými	čínský	k2eAgInPc7d1	čínský
ostrovy	ostrov	k1gInPc7	ostrov
je	být	k5eAaImIp3nS	být
souostroví	souostroví	k1gNnSc1	souostroví
Tiao-jü	Tiaoü	k1gFnSc2	Tiao-jü
nacházející	nacházející	k2eAgFnSc2d1	nacházející
se	se	k3xPyFc4	se
severně	severně	k6eAd1	severně
od	od	k7c2	od
Tchaj-wanu	Tchajan	k1gInSc2	Tchaj-wan
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
nejvýchodněji	východně	k6eAd3	východně
leží	ležet	k5eAaImIp3nS	ležet
ostrov	ostrov	k1gInSc4	ostrov
Čch	Čch	k1gFnSc2	Čch
<g/>
'	'	kIx"	'
<g/>
-wej	ej	k1gInSc1	-wej
<g/>
;	;	kIx,	;
ostrovy	ostrov	k1gInPc1	ostrov
jsou	být	k5eAaImIp3nP	být
ovšem	ovšem	k9	ovšem
pod	pod	k7c7	pod
námořní	námořní	k2eAgFnSc7d1	námořní
kontrolou	kontrola	k1gFnSc7	kontrola
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
,	,	kIx,	,
ostrůvků	ostrůvek	k1gInPc2	ostrůvek
a	a	k8xC	a
skalních	skalní	k2eAgInPc2d1	skalní
útesů	útes	k1gInPc2	útes
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
nalézají	nalézat	k5eAaImIp3nP	nalézat
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
označovány	označován	k2eAgInPc1d1	označován
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
ostrovy	ostrov	k1gInPc1	ostrov
Jihočínského	jihočínský	k2eAgNnSc2d1	Jihočínské
moře	moře	k1gNnSc2	moře
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
patří	patřit	k5eAaImIp3nS	patřit
hlavně	hlavně	k6eAd1	hlavně
do	do	k7c2	do
čtyř	čtyři	k4xCgNnPc2	čtyři
větších	veliký	k2eAgNnPc2d2	veliký
souostroví	souostroví	k1gNnPc2	souostroví
<g/>
:	:	kIx,	:
Tung-ša	Tung-ša	k1gFnSc1	Tung-ša
(	(	kIx(	(
<g/>
Východní	východní	k2eAgInPc1d1	východní
písky	písek	k1gInPc1	písek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Si-ša	Si-ša	k1gFnSc1	Si-ša
(	(	kIx(	(
<g/>
Západní	západní	k2eAgInPc1d1	západní
písky	písek	k1gInPc1	písek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Čung-ša	Čung-ša	k1gFnSc1	Čung-ša
(	(	kIx(	(
<g/>
Střední	střední	k2eAgInPc1d1	střední
písky	písek	k1gInPc1	písek
<g/>
)	)	kIx)	)
a	a	k8xC	a
Nan-ša	Nan-ša	k1gFnSc1	Nan-ša
(	(	kIx(	(
<g/>
Jižní	jižní	k2eAgInPc1d1	jižní
písky	písek	k1gInPc1	písek
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
o	o	k7c4	o
oblast	oblast	k1gFnSc4	oblast
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
vedou	vést	k5eAaImIp3nP	vést
rozsáhlé	rozsáhlý	k2eAgInPc1d1	rozsáhlý
územní	územní	k2eAgInPc1d1	územní
spory	spor	k1gInPc1	spor
<g/>
.	.	kIx.	.
</s>
<s>
ČLR	ČLR	kA	ČLR
je	být	k5eAaImIp3nS	být
zaangažována	zaangažován	k2eAgFnSc1d1	zaangažován
ve	v	k7c6	v
spletitém	spletitý	k2eAgInSc6d1	spletitý
sporu	spor	k1gInSc6	spor
o	o	k7c4	o
ostrovy	ostrov	k1gInPc4	ostrov
Nan-ša	Nan-šum	k1gNnSc2	Nan-šum
<g/>
/	/	kIx~	/
<g/>
Spratlyovy	Spratlyův	k2eAgInPc1d1	Spratlyův
ostrovy	ostrov	k1gInPc1	ostrov
s	s	k7c7	s
Malajsií	Malajsie	k1gFnSc7	Malajsie
<g/>
,	,	kIx,	,
Filipínami	Filipíny	k1gFnPc7	Filipíny
<g/>
,	,	kIx,	,
Čínskou	čínský	k2eAgFnSc7d1	čínská
republikou	republika	k1gFnSc7	republika
<g/>
,	,	kIx,	,
Vietnamem	Vietnam	k1gInSc7	Vietnam
a	a	k8xC	a
Brunejí	Brunej	k1gFnSc7	Brunej
a	a	k8xC	a
ve	v	k7c6	v
sporu	spor	k1gInSc6	spor
o	o	k7c6	o
souostroví	souostroví	k1gNnSc6	souostroví
Senkaku	Senkak	k1gInSc2	Senkak
<g/>
/	/	kIx~	/
<g/>
Tiao-jü	Tiaoü	k1gFnSc1	Tiao-jü
s	s	k7c7	s
Japonskem	Japonsko	k1gNnSc7	Japonsko
a	a	k8xC	a
s	s	k7c7	s
Čínskou	čínský	k2eAgFnSc7d1	čínská
republikou	republika	k1gFnSc7	republika
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
vede	vést	k5eAaImIp3nS	vést
spor	spor	k1gInSc1	spor
o	o	k7c4	o
námořní	námořní	k2eAgFnSc4d1	námořní
hranici	hranice	k1gFnSc4	hranice
s	s	k7c7	s
Vietnamem	Vietnam	k1gInSc7	Vietnam
v	v	k7c6	v
Tonkinském	tonkinský	k2eAgInSc6d1	tonkinský
zálivu	záliv	k1gInSc6	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Ostrovy	ostrov	k1gInPc1	ostrov
Si-ša	Si-šum	k1gNnSc2	Si-šum
<g/>
/	/	kIx~	/
<g/>
Paracelské	Paracelský	k2eAgInPc1d1	Paracelský
ostrovy	ostrov	k1gInPc1	ostrov
jsou	být	k5eAaImIp3nP	být
obsazené	obsazený	k2eAgInPc1d1	obsazený
Čínskou	čínský	k2eAgFnSc7d1	čínská
lidovou	lidový	k2eAgFnSc7d1	lidová
republikou	republika	k1gFnSc7	republika
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nárokovány	nárokován	k2eAgFnPc1d1	nárokován
Vietnamem	Vietnam	k1gInSc7	Vietnam
a	a	k8xC	a
Čínskou	čínský	k2eAgFnSc7d1	čínská
republikou	republika	k1gFnSc7	republika
na	na	k7c6	na
Tchaj-wanu	Tchajan	k1gInSc6	Tchaj-wan
<g/>
.	.	kIx.	.
</s>
<s>
Podobná	podobný	k2eAgFnSc1d1	podobná
situace	situace	k1gFnSc1	situace
je	být	k5eAaImIp3nS	být
i	i	k9	i
okolo	okolo	k7c2	okolo
souostroví	souostroví	k1gNnSc2	souostroví
Tung-ša	Tung-š	k1gInSc2	Tung-š
<g/>
/	/	kIx~	/
<g/>
Prataské	Prataský	k2eAgInPc1d1	Prataský
ostrovy	ostrov	k1gInPc1	ostrov
a	a	k8xC	a
Čung-ša	Čung-ša	k1gFnSc1	Čung-ša
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
severovýchodní	severovýchodní	k2eAgFnSc6d1	severovýchodní
Číně	Čína	k1gFnSc6	Čína
bývají	bývat	k5eAaImIp3nP	bývat
studené	studený	k2eAgFnPc1d1	studená
zimy	zima	k1gFnPc1	zima
a	a	k8xC	a
horká	horký	k2eAgNnPc4d1	horké
léta	léto	k1gNnPc4	léto
s	s	k7c7	s
přiměřeným	přiměřený	k2eAgNnSc7d1	přiměřené
množstvím	množství	k1gNnSc7	množství
srážek	srážka	k1gFnPc2	srážka
<g/>
.	.	kIx.	.
</s>
<s>
Šanghaj	Šanghaj	k1gFnSc1	Šanghaj
leží	ležet	k5eAaImIp3nS	ležet
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
oblasti	oblast	k1gFnSc6	oblast
střední	střední	k2eAgFnSc2d1	střední
Číny	Čína	k1gFnSc2	Čína
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
mírné	mírný	k2eAgFnPc4d1	mírná
zimy	zima	k1gFnPc4	zima
a	a	k8xC	a
vysoké	vysoký	k2eAgNnSc4d1	vysoké
množství	množství	k1gNnSc4	množství
srážek	srážka	k1gFnPc2	srážka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
je	být	k5eAaImIp3nS	být
vlhké	vlhký	k2eAgNnSc4d1	vlhké
subtropické	subtropický	k2eAgNnSc4d1	subtropické
podnebí	podnebí	k1gNnSc4	podnebí
<g/>
,	,	kIx,	,
na	na	k7c6	na
západě	západ	k1gInSc6	západ
je	být	k5eAaImIp3nS	být
podnebí	podnebí	k1gNnSc1	podnebí
velmi	velmi	k6eAd1	velmi
drsné	drsný	k2eAgNnSc1d1	drsné
<g/>
.	.	kIx.	.
</s>
<s>
Lhasa	Lhasa	k1gFnSc1	Lhasa
má	mít	k5eAaImIp3nS	mít
velmi	velmi	k6eAd1	velmi
studené	studený	k2eAgFnSc2d1	studená
zimy	zima	k1gFnSc2	zima
a	a	k8xC	a
málo	málo	k4c4	málo
srážek	srážka	k1gFnPc2	srážka
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Administrativní	administrativní	k2eAgNnSc1d1	administrativní
členění	členění	k1gNnSc1	členění
Čínské	čínský	k2eAgFnSc2d1	čínská
lidové	lidový	k2eAgFnSc2d1	lidová
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Čínská	čínský	k2eAgFnSc1d1	čínská
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
má	mít	k5eAaImIp3nS	mít
vícestupňový	vícestupňový	k2eAgInSc4d1	vícestupňový
správní	správní	k2eAgInSc4d1	správní
systém	systém	k1gInSc4	systém
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
nejvyšší	vysoký	k2eAgFnSc6d3	nejvyšší
úrovní	úroveň	k1gFnSc7	úroveň
stojí	stát	k5eAaImIp3nS	stát
provincie	provincie	k1gFnSc1	provincie
(	(	kIx(	(
<g/>
省	省	k?	省
<g/>
,	,	kIx,	,
shěng	shěng	k1gMnSc1	shěng
<g/>
,	,	kIx,	,
šeng	šeng	k1gMnSc1	šeng
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
ČLR	ČLR	kA	ČLR
jich	on	k3xPp3gMnPc2	on
kontroluje	kontrolovat	k5eAaImIp3nS	kontrolovat
22	[number]	k4	22
<g/>
,	,	kIx,	,
ostrov	ostrov	k1gInSc1	ostrov
Tchaj-wan	Tchajan	k1gInSc1	Tchaj-wan
je	být	k5eAaImIp3nS	být
vládou	vláda	k1gFnSc7	vláda
ČLR	ČLR	kA	ČLR
sice	sice	k8xC	sice
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
další	další	k2eAgFnSc4d1	další
provincii	provincie	k1gFnSc4	provincie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
fakticky	fakticky	k6eAd1	fakticky
nad	nad	k7c7	nad
ním	on	k3xPp3gMnSc7	on
nemá	mít	k5eNaImIp3nS	mít
žádnou	žádný	k3yNgFnSc4	žádný
moc	moc	k1gFnSc4	moc
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k6eAd1	mimo
provincií	provincie	k1gFnPc2	provincie
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
ČLR	ČLR	kA	ČLR
5	[number]	k4	5
autonomních	autonomní	k2eAgFnPc2d1	autonomní
oblastí	oblast	k1gFnPc2	oblast
(	(	kIx(	(
<g/>
自	自	k?	自
<g/>
,	,	kIx,	,
zì	zì	k?	zì
<g/>
,	,	kIx,	,
c	c	k0	c
<g/>
'	'	kIx"	'
<g/>
-č	-č	k?	-č
<g/>
'	'	kIx"	'
<g/>
-čchü	-čchü	k?	-čchü
<g/>
)	)	kIx)	)
zahrnujících	zahrnující	k2eAgNnPc2d1	zahrnující
území	území	k1gNnPc2	území
obydlené	obydlený	k2eAgFnPc1d1	obydlená
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
příslušníky	příslušník	k1gMnPc7	příslušník
národnostních	národnostní	k2eAgFnPc2d1	národnostní
menšin	menšina	k1gFnPc2	menšina
<g/>
,	,	kIx,	,
4	[number]	k4	4
přímo	přímo	k6eAd1	přímo
(	(	kIx(	(
<g/>
=	=	kIx~	=
ústřední	ústřední	k2eAgFnSc7d1	ústřední
vládou	vláda	k1gFnSc7	vláda
<g/>
)	)	kIx)	)
spravovaná	spravovaný	k2eAgNnPc1d1	spravované
města	město	k1gNnPc1	město
(	(	kIx(	(
<g/>
直	直	k?	直
<g/>
,	,	kIx,	,
zhíxiáshì	zhíxiáshì	k?	zhíxiáshì
<g/>
,	,	kIx,	,
č	č	k0	č
<g/>
'	'	kIx"	'
<g/>
-sia-š	ia-š	k1gInSc4	-sia-š
<g/>
'	'	kIx"	'
<g/>
)	)	kIx)	)
a	a	k8xC	a
2	[number]	k4	2
zvláštní	zvláštní	k2eAgFnSc2d1	zvláštní
správní	správní	k2eAgFnSc2d1	správní
oblasti	oblast	k1gFnSc2	oblast
(	(	kIx(	(
<g/>
特	特	k?	特
<g/>
,	,	kIx,	,
tè	tè	k?	tè
xíngzhè	xíngzhè	k?	xíngzhè
<g/>
,	,	kIx,	,
tche-pie	tcheie	k1gFnSc1	tche-pie
sing-čeng-čchü	sing-čeng-čchü	k?	sing-čeng-čchü
<g/>
)	)	kIx)	)
těšící	těšící	k2eAgFnSc3d1	těšící
se	se	k3xPyFc4	se
velké	velký	k2eAgFnSc3d1	velká
míře	míra	k1gFnSc3	míra
autonomie	autonomie	k1gFnSc2	autonomie
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
jsou	být	k5eAaImIp3nP	být
provincie	provincie	k1gFnPc1	provincie
a	a	k8xC	a
autonomní	autonomní	k2eAgFnPc1d1	autonomní
oblasti	oblast	k1gFnPc1	oblast
rozděleny	rozdělit	k5eAaPmNgFnP	rozdělit
na	na	k7c4	na
autonomní	autonomní	k2eAgFnPc4d1	autonomní
prefektury	prefektura	k1gFnPc4	prefektura
<g/>
,	,	kIx,	,
okresy	okres	k1gInPc1	okres
<g/>
,	,	kIx,	,
autonomní	autonomní	k2eAgInPc1d1	autonomní
okresy	okres	k1gInPc1	okres
a	a	k8xC	a
města	město	k1gNnPc1	město
a	a	k8xC	a
ty	ten	k3xDgMnPc4	ten
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
města	město	k1gNnPc4	město
a	a	k8xC	a
obce	obec	k1gFnPc4	obec
včetně	včetně	k7c2	včetně
měst	město	k1gNnPc2	město
a	a	k8xC	a
obcí	obec	k1gFnPc2	obec
etnických	etnický	k2eAgFnPc2d1	etnická
menšin	menšina	k1gFnPc2	menšina
<g/>
.	.	kIx.	.
</s>
<s>
Přehled	přehled	k1gInSc1	přehled
provincií	provincie	k1gFnPc2	provincie
ČLR	ČLR	kA	ČLR
<g/>
:	:	kIx,	:
Poznámka	poznámka	k1gFnSc1	poznámka
<g/>
:	:	kIx,	:
ČLR	ČLR	kA	ČLR
považuje	považovat	k5eAaImIp3nS	považovat
Tchaj-wan	Tchajan	k1gInSc1	Tchaj-wan
za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
23	[number]	k4	23
<g/>
.	.	kIx.	.
provincii	provincie	k1gFnSc6	provincie
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
jej	on	k3xPp3gMnSc4	on
fakticky	fakticky	k6eAd1	fakticky
neovládá	ovládat	k5eNaImIp3nS	ovládat
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Politický	politický	k2eAgInSc1d1	politický
systém	systém	k1gInSc1	systém
Číny	Čína	k1gFnSc2	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Čínská	čínský	k2eAgFnSc1d1	čínská
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
je	být	k5eAaImIp3nS	být
totalitní	totalitní	k2eAgInSc4d1	totalitní
režim	režim	k1gInSc4	režim
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
Číny	Čína	k1gFnSc2	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
se	se	k3xPyFc4	se
v	v	k7c6	v
jejím	její	k3xOp3gNnSc6	její
čele	čelo	k1gNnSc6	čelo
vystřídaly	vystřídat	k5eAaPmAgInP	vystřídat
čtyři	čtyři	k4xCgNnPc1	čtyři
"	"	kIx"	"
<g/>
generace	generace	k1gFnSc1	generace
vůdců	vůdce	k1gMnPc2	vůdce
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
válečná	válečný	k2eAgFnSc1d1	válečná
generace	generace	k1gFnSc1	generace
Mao	Mao	k1gMnSc1	Mao
Ce-tung	Ceung	k1gMnSc1	Ce-tung
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
strany	strana	k1gFnSc2	strana
1945	[number]	k4	1945
<g/>
-	-	kIx~	-
<g/>
1976	[number]	k4	1976
<g/>
,	,	kIx,	,
prezident	prezident	k1gMnSc1	prezident
1954-1959	[number]	k4	1954-1959
Čou	Čou	k1gFnSc2	Čou
En-laj	Enaj	k1gFnSc1	En-laj
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
1949-1976	[number]	k4	1949-1976
Ču	Ču	k1gMnSc1	Ču
Te	Te	k1gMnSc1	Te
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
stálého	stálý	k2eAgInSc2d1	stálý
výboru	výbor	k1gInSc2	výbor
Všečínského	všečínský	k2eAgNnSc2d1	Všečínské
shromáždění	shromáždění	k1gNnSc2	shromáždění
lidových	lidový	k2eAgMnPc2d1	lidový
zástupců	zástupce	k1gMnPc2	zástupce
1959-1976	[number]	k4	1959-1976
(	(	kIx(	(
<g/>
hlava	hlava	k1gFnSc1	hlava
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
1975	[number]	k4	1975
<g/>
-	-	kIx~	-
<g/>
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Liou	Lio	k2eAgFnSc4d1	Lio
Šao-čchi	Šao-čche	k1gFnSc4	Šao-čche
<g/>
,	,	kIx,	,
prezident	prezident	k1gMnSc1	prezident
1959-1968	[number]	k4	1959-1968
Druhá	druhý	k4xOgFnSc1	druhý
válečná	válečný	k2eAgFnSc1d1	válečná
generace	generace	k1gFnSc1	generace
Teng	Teng	k1gMnSc1	Teng
Siao-pching	Siaoching	k1gInSc1	Siao-pching
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
vojenské	vojenský	k2eAgFnSc2d1	vojenská
komise	komise	k1gFnSc2	komise
1981	[number]	k4	1981
<g/>
-	-	kIx~	-
<g/>
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
Maův	Maův	k1gMnSc1	Maův
nástupce	nástupce	k1gMnSc1	nástupce
<g/>
,	,	kIx,	,
provedl	provést	k5eAaPmAgMnS	provést
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
reformy	reforma	k1gFnSc2	reforma
a	a	k8xC	a
otevřel	otevřít	k5eAaPmAgMnS	otevřít
Čínu	Čína	k1gFnSc4	Čína
zahraničnímu	zahraniční	k2eAgInSc3d1	zahraniční
kapitálu	kapitál	k1gInSc3	kapitál
Chua	Chua	k1gMnSc1	Chua
Kuo-feng	Kuoeng	k1gMnSc1	Kuo-feng
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
strany	strana	k1gFnSc2	strana
1976	[number]	k4	1976
<g/>
-	-	kIx~	-
<g/>
1981	[number]	k4	1981
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
<g />
.	.	kIx.	.
</s>
<s>
1976-1980	[number]	k4	1976-1980
Chu	Chu	k1gMnSc1	Chu
Jao-Pang	Jao-Pang	k1gMnSc1	Jao-Pang
<g/>
,	,	kIx,	,
generální	generální	k2eAgMnSc1d1	generální
tajemník	tajemník	k1gMnSc1	tajemník
strany	strana	k1gFnSc2	strana
1980-1987	[number]	k4	1980-1987
Třetí	třetí	k4xOgFnSc2	třetí
generace	generace	k1gFnSc2	generace
(	(	kIx(	(
<g/>
vzešlá	vzešlý	k2eAgFnSc1d1	vzešlá
z	z	k7c2	z
ředitelů	ředitel	k1gMnPc2	ředitel
státních	státní	k2eAgInPc2d1	státní
podniků	podnik	k1gInPc2	podnik
vzdělaných	vzdělaný	k2eAgInPc2d1	vzdělaný
v	v	k7c6	v
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
<g/>
)	)	kIx)	)
Čao	čao	k0	čao
C	C	kA	C
<g/>
'	'	kIx"	'
<g/>
-jang	ang	k1gMnSc1	-jang
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
1983	[number]	k4	1983
<g/>
-	-	kIx~	-
<g/>
1987	[number]	k4	1987
<g/>
,	,	kIx,	,
generální	generální	k2eAgMnSc1d1	generální
tajemník	tajemník	k1gMnSc1	tajemník
strany	strana	k1gFnSc2	strana
1987-1989	[number]	k4	1987-1989
Ťiang	Ťiang	k1gInSc1	Ťiang
Ce-min	Cein	k2eAgInSc1d1	Ce-min
<g/>
,	,	kIx,	,
generální	generální	k2eAgMnSc1d1	generální
<g />
.	.	kIx.	.
</s>
<s>
tajemník	tajemník	k1gMnSc1	tajemník
strany	strana	k1gFnSc2	strana
1989	[number]	k4	1989
<g/>
-	-	kIx~	-
<g/>
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
prezident	prezident	k1gMnSc1	prezident
1993-2003	[number]	k4	1993-2003
Li	li	k8xS	li
Pcheng	Pcheng	k1gMnSc1	Pcheng
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
1988-1998	[number]	k4	1988-1998
Ču	Ču	k1gFnSc1	Ču
Žung-ťi	Žung-ťi	k1gNnSc2	Žung-ťi
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
1998-2003	[number]	k4	1998-2003
Čtvrtá	čtvrtá	k1gFnSc1	čtvrtá
generace	generace	k1gFnSc1	generace
<g/>
,	,	kIx,	,
technokratická	technokratický	k2eAgFnSc1d1	technokratická
Chu	Chu	k1gFnSc1	Chu
Ťin-tchao	Ťinchao	k1gMnSc1	Ťin-tchao
<g/>
,	,	kIx,	,
generální	generální	k2eAgMnSc1d1	generální
tajemník	tajemník	k1gMnSc1	tajemník
strany	strana	k1gFnSc2	strana
od	od	k7c2	od
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
prezident	prezident	k1gMnSc1	prezident
od	od	k7c2	od
2003	[number]	k4	2003
Wen	Wen	k1gMnPc2	Wen
Ťia-pao	Ťiaao	k1gMnSc1	Ťia-pao
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
od	od	k7c2	od
2003	[number]	k4	2003
Pátá	pátá	k1gFnSc1	pátá
generace	generace	k1gFnSc1	generace
Si	se	k3xPyFc3	se
Ťin-pching	Ťinching	k1gInSc4	Ťin-pching
<g/>
,	,	kIx,	,
generální	generální	k2eAgMnSc1d1	generální
tajemník	tajemník	k1gMnSc1	tajemník
strany	strana	k1gFnSc2	strana
od	od	k7c2	od
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
prezident	prezident	k1gMnSc1	prezident
od	od	k7c2	od
2013	[number]	k4	2013
Li	li	k8xS	li
Kche-čchiang	Kche-čchiang	k1gMnSc1	Kche-čchiang
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
od	od	k7c2	od
2013	[number]	k4	2013
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Ekonomika	ekonomika	k1gFnSc1	ekonomika
Čínské	čínský	k2eAgFnSc2d1	čínská
lidové	lidový	k2eAgFnSc2d1	lidová
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
Velký	velký	k2eAgInSc4d1	velký
skok	skok	k1gInSc4	skok
vpřed	vpřed	k6eAd1	vpřed
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
ekonomickým	ekonomický	k2eAgFnPc3d1	ekonomická
reformám	reforma	k1gFnPc3	reforma
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
začal	začít	k5eAaPmAgInS	začít
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1976	[number]	k4	1976
prosazovat	prosazovat	k5eAaImF	prosazovat
Teng	Teng	k1gInSc4	Teng
Siao-pching	Siaoching	k1gInSc1	Siao-pching
a	a	k8xC	a
na	na	k7c4	na
které	který	k3yIgInPc4	který
navázali	navázat	k5eAaPmAgMnP	navázat
jeho	jeho	k3xOp3gMnPc1	jeho
nástupci	nástupce	k1gMnPc1	nástupce
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
ČLR	ČLR	kA	ČLR
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
nastartován	nastartován	k2eAgInSc4d1	nastartován
rychlý	rychlý	k2eAgInSc4d1	rychlý
hospodářský	hospodářský	k2eAgInSc4d1	hospodářský
růst	růst	k1gInSc4	růst
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
trvá	trvat	k5eAaImIp3nS	trvat
dodnes	dodnes	k6eAd1	dodnes
a	a	k8xC	a
rychle	rychle	k6eAd1	rychle
stimuluje	stimulovat	k5eAaImIp3nS	stimulovat
všechny	všechen	k3xTgFnPc4	všechen
oblasti	oblast	k1gFnPc4	oblast
národního	národní	k2eAgNnSc2d1	národní
hospodářství	hospodářství	k1gNnSc2	hospodářství
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomika	ekonomika	k1gFnSc1	ekonomika
země	zem	k1gFnSc2	zem
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
rostla	růst	k5eAaImAgFnS	růst
tempem	tempo	k1gNnSc7	tempo
11,9	[number]	k4	11,9
%	%	kIx~	%
<g/>
,	,	kIx,	,
nejvyšším	vysoký	k2eAgMnSc6d3	nejvyšší
za	za	k7c4	za
13	[number]	k4	13
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Hrubý	hrubý	k2eAgInSc1d1	hrubý
domácí	domácí	k2eAgInSc1d1	domácí
produkt	produkt	k1gInSc1	produkt
tak	tak	k6eAd1	tak
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
cifry	cifra	k1gFnSc2	cifra
24,953	[number]	k4	24,953
biliónů	bilión	k4xCgInPc2	bilión
CNY	CNY	kA	CNY
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
3,428	[number]	k4	3,428
biliónů	bilión	k4xCgInPc2	bilión
USD	USD	kA	USD
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
dlouhodobý	dlouhodobý	k2eAgInSc1d1	dlouhodobý
dvouciferný	dvouciferný	k2eAgInSc1d1	dvouciferný
růst	růst	k1gInSc1	růst
způsobil	způsobit	k5eAaPmAgInS	způsobit
zvýšení	zvýšení	k1gNnSc4	zvýšení
inflace	inflace	k1gFnSc2	inflace
na	na	k7c4	na
4,8	[number]	k4	4,8
%	%	kIx~	%
<g/>
,	,	kIx,	,
oproti	oproti	k7c3	oproti
necelým	celý	k2eNgInPc3d1	necelý
2	[number]	k4	2
%	%	kIx~	%
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2008	[number]	k4	2008
inflace	inflace	k1gFnSc1	inflace
dokonce	dokonce	k9	dokonce
vyšplhala	vyšplhat	k5eAaPmAgFnS	vyšplhat
až	až	k9	až
na	na	k7c4	na
8,7	[number]	k4	8,7
%	%	kIx~	%
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
jedenáctileté	jedenáctiletý	k2eAgNnSc4d1	jedenáctileté
maximum	maximum	k1gNnSc4	maximum
<g/>
.	.	kIx.	.
</s>
<s>
Čínský	čínský	k2eAgInSc1d1	čínský
vývoz	vývoz	k1gInSc1	vývoz
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
o	o	k7c4	o
27,2	[number]	k4	27,2
%	%	kIx~	%
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
dovoz	dovoz	k1gInSc1	dovoz
ve	v	k7c6	v
stejném	stejný	k2eAgNnSc6d1	stejné
období	období	k1gNnSc6	období
narostl	narůst	k5eAaPmAgInS	narůst
o	o	k7c4	o
20	[number]	k4	20
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Saldo	saldo	k1gNnSc1	saldo
obchodní	obchodní	k2eAgFnSc2d1	obchodní
bilance	bilance	k1gFnSc2	bilance
tak	tak	k6eAd1	tak
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
přebytku	přebytek	k1gInSc2	přebytek
177,5	[number]	k4	177,5
miliard	miliarda	k4xCgFnPc2	miliarda
USD	USD	kA	USD
(	(	kIx(	(
<g/>
oproti	oproti	k7c3	oproti
100	[number]	k4	100
mld.	mld.	k?	mld.
USD	USD	kA	USD
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Čínská	čínský	k2eAgFnSc1d1	čínská
ekonomika	ekonomika	k1gFnSc1	ekonomika
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
předstihla	předstihnout	k5eAaPmAgFnS	předstihnout
britskou	britský	k2eAgFnSc7d1	britská
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
německou	německý	k2eAgFnSc4d1	německá
ekonomiku	ekonomika	k1gFnSc4	ekonomika
a	a	k8xC	a
s	s	k7c7	s
objemem	objem	k1gInSc7	objem
HDP	HDP	kA	HDP
2,49	[number]	k4	2,49
bilionu	bilion	k4xCgInSc2	bilion
€	€	k?	€
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
tehdy	tehdy	k6eAd1	tehdy
na	na	k7c4	na
třetí	třetí	k4xOgNnSc4	třetí
místo	místo	k1gNnSc4	místo
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
předstihla	předstihnout	k5eAaPmAgFnS	předstihnout
Japonsko	Japonsko	k1gNnSc4	Japonsko
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	s	k7c7	s
druhou	druhý	k4xOgFnSc7	druhý
největší	veliký	k2eAgFnSc7d3	veliký
státní	státní	k2eAgFnSc7d1	státní
ekonomikou	ekonomika	k1gFnSc7	ekonomika
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
předstihla	předstihnout	k5eAaPmAgFnS	předstihnout
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
americké	americký	k2eAgInPc4d1	americký
co	co	k9	co
do	do	k7c2	do
objemu	objem	k1gInSc2	objem
obchodovaného	obchodovaný	k2eAgNnSc2d1	obchodované
zboží	zboží	k1gNnSc2	zboží
a	a	k8xC	a
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2014	[number]	k4	2014
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
měnový	měnový	k2eAgInSc4d1	měnový
fond	fond	k1gInSc4	fond
ekonomiku	ekonomika	k1gFnSc4	ekonomika
Číny	Čína	k1gFnSc2	Čína
za	za	k7c4	za
nejsilnější	silný	k2eAgNnSc4d3	nejsilnější
na	na	k7c6	na
světě	svět	k1gInSc6	svět
v	v	k7c6	v
ukazateli	ukazatel	k1gInSc6	ukazatel
HDP	HDP	kA	HDP
dle	dle	k7c2	dle
parity	parita	k1gFnSc2	parita
kupní	kupní	k2eAgFnSc2d1	kupní
síly	síla	k1gFnSc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Čínská	čínský	k2eAgFnSc1d1	čínská
energetika	energetika	k1gFnSc1	energetika
je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
všech	všecek	k3xTgInPc6	všecek
typech	typ	k1gInPc6	typ
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Elektrická	elektrický	k2eAgFnSc1d1	elektrická
energie	energie	k1gFnSc1	energie
je	být	k5eAaImIp3nS	být
zatím	zatím	k6eAd1	zatím
vyráběna	vyrábět	k5eAaImNgFnS	vyrábět
především	především	k9	především
v	v	k7c6	v
tepelných	tepelný	k2eAgFnPc6d1	tepelná
elektrárnách	elektrárna	k1gFnPc6	elektrárna
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
vodních	vodní	k2eAgFnPc2d1	vodní
elektráren	elektrárna	k1gFnPc2	elektrárna
je	být	k5eAaImIp3nS	být
nejvýznamnější	významný	k2eAgFnSc1d3	nejvýznamnější
elektrárna	elektrárna	k1gFnSc1	elektrárna
Tři	tři	k4xCgInPc4	tři
soutěsky	soutěsk	k1gInPc1	soutěsk
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ze	z	k7c2	z
zároveň	zároveň	k6eAd1	zároveň
největší	veliký	k2eAgFnSc2d3	veliký
elektrárnou	elektrárna	k1gFnSc7	elektrárna
(	(	kIx(	(
<g/>
nejen	nejen	k6eAd1	nejen
vodní	vodní	k2eAgInPc1d1	vodní
<g/>
)	)	kIx)	)
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Čína	Čína	k1gFnSc1	Čína
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
velmi	velmi	k6eAd1	velmi
ambiciozní	ambiciozní	k2eAgInSc1d1	ambiciozní
projekt	projekt	k1gInSc1	projekt
výstavby	výstavba	k1gFnSc2	výstavba
jaderných	jaderný	k2eAgFnPc2d1	jaderná
elektráren	elektrárna	k1gFnPc2	elektrárna
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
výkon	výkon	k1gInSc4	výkon
zařízení	zařízení	k1gNnSc2	zařízení
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
dvojnásobný	dvojnásobný	k2eAgInSc1d1	dvojnásobný
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
výstavbě	výstavba	k1gFnSc6	výstavba
jaderných	jaderný	k2eAgInPc2d1	jaderný
reaktorů	reaktor	k1gInPc2	reaktor
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
pracují	pracovat	k5eAaImIp3nP	pracovat
jak	jak	k6eAd1	jak
francouzské	francouzský	k2eAgFnPc1d1	francouzská
<g/>
,	,	kIx,	,
tak	tak	k9	tak
americké	americký	k2eAgFnSc2d1	americká
a	a	k8xC	a
ruské	ruský	k2eAgFnSc2d1	ruská
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
havárii	havárie	k1gFnSc6	havárie
v	v	k7c6	v
japonské	japonský	k2eAgFnSc6d1	japonská
Fukušimě	Fukušima	k1gFnSc6	Fukušima
bylo	být	k5eAaImAgNnS	být
sice	sice	k8xC	sice
dočasně	dočasně	k6eAd1	dočasně
pozastaveno	pozastaven	k2eAgNnSc4d1	pozastaveno
vydávání	vydávání	k1gNnSc4	vydávání
povolení	povolení	k1gNnSc2	povolení
ke	k	k7c3	k
stavbě	stavba	k1gFnSc3	stavba
nových	nový	k2eAgFnPc2d1	nová
jaderných	jaderný	k2eAgFnPc2d1	jaderná
elektráren	elektrárna	k1gFnPc2	elektrárna
a	a	k8xC	a
provedeny	proveden	k2eAgFnPc1d1	provedena
prověrky	prověrka	k1gFnPc1	prověrka
v	v	k7c6	v
těch	ten	k3xDgFnPc6	ten
stávajících	stávající	k2eAgFnPc6d1	stávající
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
Čína	Čína	k1gFnSc1	Čína
hodlá	hodlat	k5eAaImIp3nS	hodlat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
projaderné	projaderný	k2eAgFnSc6d1	projaderná
politice	politika	k1gFnSc6	politika
beze	beze	k7c2	beze
změny	změna	k1gFnSc2	změna
pokračovat	pokračovat	k5eAaImF	pokračovat
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
politice	politika	k1gFnSc3	politika
jednoho	jeden	k4xCgNnSc2	jeden
dítěte	dítě	k1gNnSc2	dítě
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
ČLR	ČLR	kA	ČLR
zavedla	zavést	k5eAaPmAgFnS	zavést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
Číny	Čína	k1gFnSc2	Čína
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
z	z	k7c2	z
přibližně	přibližně	k6eAd1	přibližně
550	[number]	k4	550
milionů	milion	k4xCgInPc2	milion
v	v	k7c6	v
době	doba	k1gFnSc6	doba
vzniku	vznik	k1gInSc2	vznik
ČLR	ČLR	kA	ČLR
na	na	k7c4	na
1.4	[number]	k4	1.4
miliardy	miliarda	k4xCgFnSc2	miliarda
dnes	dnes	k6eAd1	dnes
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Zdroj	zdroj	k1gInSc1	zdroj
<g/>
:	:	kIx,	:
tisková	tiskový	k2eAgFnSc1d1	tisková
agentura	agentura	k1gFnSc1	agentura
Xinhua	Xinhua	k1gFnSc1	Xinhua
<g/>
)	)	kIx)	)
Došlo	dojít	k5eAaPmAgNnS	dojít
mj.	mj.	kA	mj.
ke	k	k7c3	k
značnému	značný	k2eAgNnSc3d1	značné
znečištění	znečištění	k1gNnSc3	znečištění
zemědělských	zemědělský	k2eAgFnPc2d1	zemědělská
půd	půda	k1gFnPc2	půda
a	a	k8xC	a
povodí	povodí	k1gNnSc2	povodí
řeky	řeka	k1gFnSc2	řeka
Jang-c	Jang	k1gInSc1	Jang-c
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
hlavních	hlavní	k2eAgFnPc2d1	hlavní
oblastí	oblast	k1gFnPc2	oblast
produkce	produkce	k1gFnSc2	produkce
potravin	potravina	k1gFnPc2	potravina
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
zamoření	zamoření	k1gNnSc1	zamoření
půd	půda	k1gFnPc2	půda
těžkými	těžký	k2eAgInPc7d1	těžký
kovy	kov	k1gInPc7	kov
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
znehodnocení	znehodnocení	k1gNnSc1	znehodnocení
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
produkce	produkce	k1gFnSc2	produkce
v	v	k7c6	v
hodnotě	hodnota	k1gFnSc6	hodnota
20	[number]	k4	20
miliard	miliarda	k4xCgFnPc2	miliarda
jüanů	jüan	k1gInPc2	jüan
(	(	kIx(	(
<g/>
cca	cca	kA	cca
54	[number]	k4	54
miliard	miliarda	k4xCgFnPc2	miliarda
Kč	Kč	kA	Kč
<g/>
)	)	kIx)	)
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
problémem	problém	k1gInSc7	problém
intenzifikovaného	intenzifikovaný	k2eAgNnSc2d1	intenzifikovaný
zemědělství	zemědělství	k1gNnSc2	zemědělství
(	(	kIx(	(
<g/>
ČLR	ČLR	kA	ČLR
je	být	k5eAaImIp3nS	být
země	zem	k1gFnPc4	zem
s	s	k7c7	s
9	[number]	k4	9
<g/>
.	.	kIx.	.
největším	veliký	k2eAgInSc7d3	veliký
růstem	růst	k1gInSc7	růst
zemědělství	zemědělství	k1gNnSc2	zemědělství
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
eroze	eroze	k1gFnSc1	eroze
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
<s>
Čína	Čína	k1gFnSc1	Čína
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
oblastí	oblast	k1gFnPc2	oblast
s	s	k7c7	s
nejmenšími	malý	k2eAgFnPc7d3	nejmenší
zásobami	zásoba	k1gFnPc7	zásoba
vody	voda	k1gFnSc2	voda
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Sever	sever	k1gInSc1	sever
Číny	Čína	k1gFnSc2	Čína
má	mít	k5eAaImIp3nS	mít
kritický	kritický	k2eAgInSc1d1	kritický
nedostatek	nedostatek	k1gInSc1	nedostatek
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
nedostatkem	nedostatek	k1gInSc7	nedostatek
pitné	pitný	k2eAgFnSc2d1	pitná
vody	voda	k1gFnSc2	voda
trpí	trpět	k5eAaImIp3nS	trpět
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
čínských	čínský	k2eAgNnPc2d1	čínské
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Ekologickým	ekologický	k2eAgInSc7d1	ekologický
problémem	problém	k1gInSc7	problém
je	být	k5eAaImIp3nS	být
kontaminace	kontaminace	k1gFnSc1	kontaminace
valné	valný	k2eAgFnSc2d1	valná
většiny	většina	k1gFnSc2	většina
řek	řeka	k1gFnPc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Kvalita	kvalita	k1gFnSc1	kvalita
vody	voda	k1gFnSc2	voda
je	být	k5eAaImIp3nS	být
také	také	k6eAd1	také
ohrožována	ohrožovat	k5eAaImNgFnS	ohrožovat
rizikovými	rizikový	k2eAgInPc7d1	rizikový
průmyslovými	průmyslový	k2eAgInPc7d1	průmyslový
provozy	provoz	k1gInPc7	provoz
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
zdrojů	zdroj	k1gInPc2	zdroj
pitné	pitný	k2eAgFnSc2d1	pitná
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
např.	např.	kA	např.
13	[number]	k4	13
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2005	[number]	k4	2005
způsobila	způsobit	k5eAaPmAgFnS	způsobit
exploze	exploze	k1gFnSc1	exploze
chemičky	chemička	k1gFnSc2	chemička
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Ťi-lin	Ťiina	k1gFnPc2	Ťi-lina
kontaminaci	kontaminace	k1gFnSc4	kontaminace
řeky	řeka	k1gFnSc2	řeka
Sung-chua	Sunghua	k1gMnSc1	Sung-chua
benzenem	benzen	k1gInSc7	benzen
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
i	i	k9	i
ohrožení	ohrožení	k1gNnSc1	ohrožení
a	a	k8xC	a
znečištění	znečištění	k1gNnSc1	znečištění
pitné	pitný	k2eAgFnSc2d1	pitná
vody	voda	k1gFnSc2	voda
ve	v	k7c6	v
městech	město	k1gNnPc6	město
po	po	k7c6	po
proudu	proud	k1gInSc6	proud
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Charbinu	Charbina	k1gFnSc4	Charbina
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
východního	východní	k2eAgNnSc2d1	východní
pobřeží	pobřeží	k1gNnSc2	pobřeží
je	být	k5eAaImIp3nS	být
území	území	k1gNnSc4	území
s	s	k7c7	s
největším	veliký	k2eAgNnSc7d3	veliký
znečištěním	znečištění	k1gNnSc7	znečištění
ovzduší	ovzduší	k1gNnSc2	ovzduší
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
je	být	k5eAaImIp3nS	být
využívání	využívání	k1gNnSc1	využívání
černého	černý	k2eAgNnSc2d1	černé
uhlí	uhlí	k1gNnSc2	uhlí
jako	jako	k8xC	jako
topiva	topivo	k1gNnSc2	topivo
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
severu	sever	k1gInSc2	sever
země	zem	k1gFnSc2	zem
ohrožuje	ohrožovat	k5eAaImIp3nS	ohrožovat
desertifikace	desertifikace	k1gFnSc1	desertifikace
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
postupné	postupný	k2eAgNnSc4d1	postupné
rozšiřování	rozšiřování	k1gNnSc4	rozšiřování
pouště	poušť	k1gFnSc2	poušť
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
tomuto	tento	k3xDgInSc3	tento
postupu	postup	k1gInSc3	postup
byl	být	k5eAaImAgInS	být
započat	započat	k2eAgInSc1d1	započat
plán	plán	k1gInSc1	plán
nazvaný	nazvaný	k2eAgInSc1d1	nazvaný
"	"	kIx"	"
<g/>
Velká	velký	k2eAgFnSc1d1	velká
zelená	zelený	k2eAgFnSc1d1	zelená
zeď	zeď	k1gFnSc1	zeď
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
počítá	počítat	k5eAaImIp3nS	počítat
s	s	k7c7	s
masivním	masivní	k2eAgNnSc7d1	masivní
zalesňováním	zalesňování	k1gNnSc7	zalesňování
ohrožených	ohrožený	k2eAgFnPc2d1	ohrožená
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
setbou	setba	k1gFnSc7	setba
vyšlechtěných	vyšlechtěný	k2eAgInPc2d1	vyšlechtěný
<g/>
,	,	kIx,	,
odolných	odolný	k2eAgInPc2d1	odolný
druhů	druh	k1gInPc2	druh
dřevin	dřevina	k1gFnPc2	dřevina
ze	z	k7c2	z
vzduchu	vzduch	k1gInSc2	vzduch
a	a	k8xC	a
dalšími	další	k2eAgNnPc7d1	další
opatřeními	opatření	k1gNnPc7	opatření
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
splnění	splnění	k1gNnSc1	splnění
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
na	na	k7c4	na
desítky	desítka	k1gFnPc4	desítka
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
83,3	[number]	k4	83,3
%	%	kIx~	%
čínských	čínský	k2eAgFnPc2d1	čínská
žen	žena	k1gFnPc2	žena
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
15	[number]	k4	15
až	až	k9	až
49	[number]	k4	49
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
vdaných	vdaný	k2eAgFnPc2d1	vdaná
či	či	k8xC	či
zadaných	zadaný	k2eAgFnPc2d1	zadaná
<g/>
,	,	kIx,	,
používalo	používat	k5eAaImAgNnS	používat
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
moderní	moderní	k2eAgFnSc2d1	moderní
metody	metoda	k1gFnSc2	metoda
antikoncepce	antikoncepce	k1gFnSc2	antikoncepce
a	a	k8xC	a
tímto	tento	k3xDgInSc7	tento
podílem	podíl	k1gInSc7	podíl
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
Čína	Čína	k1gFnSc1	Čína
na	na	k7c4	na
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
většina	většina	k1gFnSc1	většina
nálepek	nálepka	k1gFnPc2	nálepka
s	s	k7c7	s
nápisem	nápis	k1gInSc7	nápis
Made	Made	k1gFnSc1	Made
in	in	k?	in
China	China	k1gFnSc1	China
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
</s>
