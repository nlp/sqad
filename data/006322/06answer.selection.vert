<s>
Seriál	seriál	k1gInSc1	seriál
satiricky	satiricky	k6eAd1	satiricky
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c6	o
životě	život	k1gInSc6	život
americké	americký	k2eAgFnSc2d1	americká
střední	střední	k2eAgFnSc2d1	střední
třídy	třída	k1gFnSc2	třída
představované	představovaný	k2eAgFnSc2d1	představovaná
rodinkou	rodinka	k1gFnSc7	rodinka
Simpsonů	Simpson	k1gInPc2	Simpson
<g/>
,	,	kIx,	,
zahrnující	zahrnující	k2eAgFnSc1d1	zahrnující
Homera	Homera	k1gFnSc1	Homera
<g/>
,	,	kIx,	,
Marge	Marge	k1gFnSc1	Marge
<g/>
,	,	kIx,	,
Barta	Barta	k1gFnSc1	Barta
<g/>
,	,	kIx,	,
Lízu	Líza	k1gFnSc4	Líza
<g/>
,	,	kIx,	,
Maggie	Maggie	k1gFnSc1	Maggie
<g/>
,	,	kIx,	,
dědu	děda	k1gMnSc4	děda
Simpsona	Simpson	k1gMnSc4	Simpson
<g/>
,	,	kIx,	,
psa	pes	k1gMnSc4	pes
Spasitele	spasitel	k1gMnSc4	spasitel
a	a	k8xC	a
kočku	kočka	k1gFnSc4	kočka
Sněhulku	Sněhulka	k1gFnSc4	Sněhulka
<g/>
.	.	kIx.	.
</s>
