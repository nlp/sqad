<s>
Do	do	k7c2	do
Arménie	Arménie	k1gFnSc2	Arménie
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
horské	horský	k2eAgNnSc4d1	horské
pásmo	pásmo	k1gNnSc4	pásmo
Malého	Malého	k2eAgInSc2d1	Malého
Kavkazu	Kavkaz	k1gInSc2	Kavkaz
a	a	k8xC	a
vysokohorské	vysokohorský	k2eAgNnSc4d1	vysokohorské
pohoří	pohoří	k1gNnSc4	pohoří
Ararat	Ararat	k1gMnSc1	Ararat
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
také	také	k9	také
nachází	nacházet	k5eAaImIp3nS	nacházet
oficiální	oficiální	k2eAgFnSc1d1	oficiální
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
Arménie	Arménie	k1gFnSc2	Arménie
(	(	kIx(	(
<g/>
Aragac	Aragac	k1gInSc1	Aragac
<g/>
,	,	kIx,	,
4090	[number]	k4	4090
m	m	kA	m
n	n	k0	n
<g/>
.	.	kIx.	.
m	m	kA	m
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
