<s>
Voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
sumárním	sumární	k2eAgInSc7d1	sumární
vzorcem	vzorec	k1gInSc7	vzorec
(	(	kIx(	(
<g/>
zároveň	zároveň	k6eAd1	zároveň
však	však	k9	však
racionálním	racionální	k2eAgInSc7d1	racionální
<g/>
)	)	kIx)	)
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
,	,	kIx,	,
systematický	systematický	k2eAgInSc1d1	systematický
název	název	k1gInSc1	název
oxidan	oxidana	k1gFnPc2	oxidana
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
chemická	chemický	k2eAgFnSc1d1	chemická
sloučenina	sloučenina	k1gFnSc1	sloučenina
vodíku	vodík	k1gInSc2	vodík
a	a	k8xC	a
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
.	.	kIx.	.
</s>
