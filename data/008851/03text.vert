<p>
<s>
Obilka	obilka	k1gFnSc1	obilka
(	(	kIx(	(
<g/>
caryopsis	caryopsis	k1gInSc1	caryopsis
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
suchý	suchý	k2eAgInSc4d1	suchý
nepukavý	pukavý	k2eNgInSc4d1	nepukavý
plod	plod	k1gInSc4	plod
lipnicovitých	lipnicovitý	k2eAgFnPc2d1	lipnicovitý
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
složena	složit	k5eAaPmNgFnS	složit
z	z	k7c2	z
obalových	obalový	k2eAgFnPc2d1	obalová
vrstev	vrstva	k1gFnPc2	vrstva
(	(	kIx(	(
<g/>
ektosperm	ektosperm	k1gInSc1	ektosperm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
endospermu	endosperm	k1gInSc2	endosperm
(	(	kIx(	(
<g/>
bílek	bílek	k1gInSc1	bílek
<g/>
)	)	kIx)	)
a	a	k8xC	a
embrya	embryo	k1gNnPc1	embryo
(	(	kIx(	(
<g/>
klíček	klíček	k1gInSc1	klíček
<g/>
,	,	kIx,	,
zárodek	zárodek	k1gInSc1	zárodek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vnější	vnější	k2eAgFnSc6d1	vnější
straně	strana	k1gFnSc6	strana
endospermu	endosperm	k1gInSc2	endosperm
je	být	k5eAaImIp3nS	být
aleuronová	aleuronový	k2eAgFnSc1d1	aleuronový
vrstva	vrstva	k1gFnSc1	vrstva
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
vysycháním	vysychání	k1gNnSc7	vysychání
vakuol	vakuola	k1gFnPc2	vakuola
s	s	k7c7	s
bilkovinnými	bilkovinný	k2eAgFnPc7d1	bilkovinný
látkami	látka	k1gFnPc7	látka
<g/>
.	.	kIx.	.
</s>
<s>
Embryo	embryo	k1gNnSc1	embryo
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
položeno	položit	k5eAaPmNgNnS	položit
šikmo	šikmo	k6eAd1	šikmo
a	a	k8xC	a
od	od	k7c2	od
endospermu	endosperm	k1gInSc2	endosperm
odděleno	oddělit	k5eAaPmNgNnS	oddělit
štítkem	štítek	k1gInSc7	štítek
<g/>
.	.	kIx.	.
</s>
<s>
Obal	obal	k1gInSc4	obal
tvoří	tvořit	k5eAaImIp3nP	tvořit
dvě	dva	k4xCgFnPc4	dva
vrstvy	vrstva	k1gFnPc4	vrstva
–	–	k?	–
oplodí	oplodí	k1gNnSc1	oplodí
(	(	kIx(	(
<g/>
pericarp	pericarp	k1gInSc1	pericarp
<g/>
)	)	kIx)	)
a	a	k8xC	a
osemení	osemení	k1gNnSc1	osemení
(	(	kIx(	(
<g/>
testa	testa	k1gFnSc1	testa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
je	být	k5eAaImIp3nS	být
obilka	obilka	k1gFnSc1	obilka
chráněna	chráněn	k2eAgFnSc1d1	chráněna
pluchou	plucha	k1gFnSc7	plucha
a	a	k8xC	a
pluškou	pluška	k1gFnSc7	pluška
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pluchy	plucha	k1gFnPc1	plucha
jsou	být	k5eAaImIp3nP	být
buď	buď	k8xC	buď
přirostlé	přirostlý	k2eAgFnPc1d1	přirostlá
k	k	k7c3	k
obilce	obilka	k1gFnSc3	obilka
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
pluchaté	pluchatý	k2eAgFnPc4d1	pluchatá
obilky	obilka	k1gFnPc4	obilka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
nebo	nebo	k8xC	nebo
nejsou	být	k5eNaImIp3nP	být
přirostlé	přirostlý	k2eAgFnPc1d1	přirostlá
a	a	k8xC	a
lze	lze	k6eAd1	lze
je	být	k5eAaImIp3nS	být
odstranit	odstranit	k5eAaPmF	odstranit
(	(	kIx(	(
<g/>
nahé	nahý	k2eAgFnPc4d1	nahá
obilky	obilka	k1gFnPc4	obilka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nahé	nahý	k2eAgFnPc4d1	nahá
obilky	obilka	k1gFnPc4	obilka
má	mít	k5eAaImIp3nS	mít
pšenice	pšenice	k1gFnSc1	pšenice
<g/>
,	,	kIx,	,
žito	žit	k2eAgNnSc1d1	žito
<g/>
,	,	kIx,	,
tritikale	tritikal	k1gMnSc5	tritikal
<g/>
,	,	kIx,	,
kukuřice	kukuřice	k1gFnSc2	kukuřice
<g/>
,	,	kIx,	,
nahý	nahý	k2eAgInSc4d1	nahý
oves	oves	k1gInSc4	oves
a	a	k8xC	a
nahý	nahý	k2eAgInSc4d1	nahý
ječmen	ječmen	k1gInSc4	ječmen
<g/>
.	.	kIx.	.
</s>
<s>
Pluchaté	pluchatý	k2eAgFnPc4d1	pluchatá
obilky	obilka	k1gFnPc4	obilka
má	mít	k5eAaImIp3nS	mít
obvykle	obvykle	k6eAd1	obvykle
ječmen	ječmen	k1gInSc4	ječmen
<g/>
,	,	kIx,	,
oves	oves	k1gInSc4	oves
<g/>
,	,	kIx,	,
rýže	rýže	k1gFnPc4	rýže
<g/>
,	,	kIx,	,
proso	proso	k1gNnSc4	proso
a	a	k8xC	a
některé	některý	k3yIgInPc4	některý
čiroky	čirok	k1gInPc4	čirok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
obilek	obilka	k1gFnPc2	obilka
obilnin	obilnina	k1gFnPc2	obilnina
se	se	k3xPyFc4	se
mele	mlít	k5eAaImIp3nS	mlít
mouka	mouka	k1gFnSc1	mouka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Cereálie	cereálie	k1gFnPc1	cereálie
</s>
</p>
<p>
<s>
Lipnicovité	Lipnicovitý	k2eAgNnSc1d1	Lipnicovitý
</s>
</p>
<p>
<s>
Mouka	mouka	k1gFnSc1	mouka
</s>
</p>
