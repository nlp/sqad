<s>
Pokožka	pokožka	k1gFnSc1	pokožka
(	(	kIx(	(
<g/>
epidermis	epidermis	k1gFnSc1	epidermis
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejsvrchnější	svrchní	k2eAgFnSc1d3	nejsvrchnější
vrstva	vrstva	k1gFnSc1	vrstva
kůže	kůže	k1gFnSc2	kůže
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
vodotěsný	vodotěsný	k2eAgInSc4d1	vodotěsný
ochranný	ochranný	k2eAgInSc4d1	ochranný
obal	obal	k1gInSc4	obal
kolem	kolem	k7c2	kolem
povrchu	povrch	k1gInSc2	povrch
těla	tělo	k1gNnSc2	tělo
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
jednak	jednak	k8xC	jednak
tenkým	tenký	k2eAgInSc7d1	tenký
vrstevnatým	vrstevnatý	k2eAgInSc7d1	vrstevnatý
epitelem	epitel	k1gInSc7	epitel
z	z	k7c2	z
dlaždicovitých	dlaždicovitý	k2eAgFnPc2d1	dlaždicovitý
buněk	buňka	k1gFnPc2	buňka
a	a	k8xC	a
jednak	jednak	k8xC	jednak
pod	pod	k7c7	pod
nimi	on	k3xPp3gInPc7	on
je	být	k5eAaImIp3nS	být
bazální	bazální	k2eAgFnSc1d1	bazální
membrána	membrána	k1gFnSc1	membrána
<g/>
.	.	kIx.	.
</s>
<s>
Pokožka	pokožka	k1gFnSc1	pokožka
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
žádné	žádný	k3yNgFnPc4	žádný
cévy	céva	k1gFnPc4	céva
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
vyživována	vyživovat	k5eAaImNgFnS	vyživovat
pouze	pouze	k6eAd1	pouze
difuzí	difuze	k1gFnSc7	difuze
ze	z	k7c2	z
škáry	škára	k1gFnSc2	škára
(	(	kIx(	(
<g/>
dermis	dermis	k1gInSc1	dermis
<g/>
)	)	kIx)	)
a	a	k8xC	a
kyslík	kyslík	k1gInSc4	kyslík
si	se	k3xPyFc3	se
bere	brát	k5eAaImIp3nS	brát
přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
okolního	okolní	k2eAgInSc2d1	okolní
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Čtyři	čtyři	k4xCgInPc1	čtyři
hlavní	hlavní	k2eAgInPc1d1	hlavní
druhy	druh	k1gInPc1	druh
buněk	buňka	k1gFnPc2	buňka
pokožky	pokožka	k1gFnSc2	pokožka
jsou	být	k5eAaImIp3nP	být
keratinocyty	keratinocyt	k1gInPc4	keratinocyt
-	-	kIx~	-
převážná	převážný	k2eAgFnSc1d1	převážná
většina	většina	k1gFnSc1	většina
buněk	buňka	k1gFnPc2	buňka
pokožky	pokožka	k1gFnSc2	pokožka
<g/>
,	,	kIx,	,
melanocyty	melanocyt	k1gInPc1	melanocyt
-	-	kIx~	-
pigmentové	pigmentový	k2eAgFnPc1d1	pigmentová
buňky	buňka	k1gFnPc1	buňka
Langerhansovy	Langerhansův	k2eAgFnSc2d1	Langerhansův
buňky	buňka	k1gFnSc2	buňka
-	-	kIx~	-
signální	signální	k2eAgFnSc1d1	signální
součást	součást	k1gFnSc1	součást
imunitního	imunitní	k2eAgInSc2d1	imunitní
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
Merkelovy	Merkelův	k2eAgFnPc1d1	Merkelova
buňky	buňka	k1gFnPc1	buňka
-	-	kIx~	-
čidla	čidlo	k1gNnPc1	čidlo
hmatu	hmat	k1gInSc2	hmat
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
keratinocyty	keratinocyt	k1gInPc1	keratinocyt
představují	představovat	k5eAaImIp3nP	představovat
až	až	k9	až
95	[number]	k4	95
<g/>
%	%	kIx~	%
z	z	k7c2	z
celkového	celkový	k2eAgInSc2d1	celkový
objemu	objem	k1gInSc2	objem
buněk	buňka	k1gFnPc2	buňka
v	v	k7c6	v
pokožce	pokožka	k1gFnSc6	pokožka
<g/>
.	.	kIx.	.
</s>
<s>
Epidermis	epidermis	k1gFnSc1	epidermis
je	být	k5eAaImIp3nS	být
rozdělená	rozdělený	k2eAgFnSc1d1	rozdělená
do	do	k7c2	do
mnoha	mnoho	k4c2	mnoho
vrstev	vrstva	k1gFnPc2	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
se	s	k7c7	s
<g/>
:	:	kIx,	:
Stratum	Stratum	k1gNnSc1	Stratum
disiunctum	disiunctum	k1gNnSc1	disiunctum
–	–	k?	–
v	v	k7c6	v
histologické	histologický	k2eAgFnSc6d1	histologická
terminologii	terminologie	k1gFnSc6	terminologie
se	se	k3xPyFc4	se
takto	takto	k6eAd1	takto
označují	označovat	k5eAaImIp3nP	označovat
zrohovatělé	zrohovatělý	k2eAgFnPc4d1	zrohovatělá
buňky	buňka	k1gFnPc4	buňka
oddělené	oddělený	k2eAgFnPc4d1	oddělená
od	od	k7c2	od
stratum	stratum	k1gNnSc1	stratum
corneum	corneum	k1gNnSc1	corneum
(	(	kIx(	(
<g/>
artefakt	artefakt	k1gInSc1	artefakt
způsobený	způsobený	k2eAgInSc1d1	způsobený
zpracováním	zpracování	k1gNnSc7	zpracování
materiálu	materiál	k1gInSc2	materiál
<g/>
)	)	kIx)	)
Stratum	Stratum	k1gNnSc1	Stratum
corneum	corneum	k1gNnSc1	corneum
(	(	kIx(	(
<g/>
tloušťka	tloušťka	k1gFnSc1	tloušťka
0,01	[number]	k4	0,01
až	až	k9	až
0,04	[number]	k4	0,04
mm	mm	kA	mm
<g/>
)	)	kIx)	)
–	–	k?	–
Vrstva	vrstva	k1gFnSc1	vrstva
odumřelých	odumřelý	k2eAgInPc2d1	odumřelý
a	a	k8xC	a
plně	plně	k6eAd1	plně
keratinisovaných	keratinisovaný	k2eAgFnPc2d1	keratinisovaný
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
vlastní	vlastní	k2eAgFnSc1d1	vlastní
kožní	kožní	k2eAgFnSc1d1	kožní
bariéra	bariéra	k1gFnSc1	bariéra
<g/>
.	.	kIx.	.
</s>
<s>
Stratum	Stratum	k1gNnSc1	Stratum
lucidum	lucidum	k1gInSc1	lucidum
–	–	k?	–
tenká	tenký	k2eAgFnSc1d1	tenká
eosinofilní	eosinofilní	k2eAgFnSc1d1	eosinofilní
vrstva	vrstva	k1gFnSc1	vrstva
<g/>
;	;	kIx,	;
dobře	dobře	k6eAd1	dobře
zřetelná	zřetelný	k2eAgFnSc1d1	zřetelná
v	v	k7c6	v
tlustém	tlusté	k1gNnSc6	tlusté
typu	typ	k1gInSc2	typ
kůže	kůže	k1gFnSc2	kůže
(	(	kIx(	(
<g/>
dlaně	dlaň	k1gFnSc2	dlaň
<g/>
,	,	kIx,	,
chodidla	chodidlo	k1gNnSc2	chodidlo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
špatně	špatně	k6eAd1	špatně
zřetelná	zřetelný	k2eAgFnSc1d1	zřetelná
až	až	k8xS	až
nezřetelná	zřetelný	k2eNgFnSc1d1	nezřetelná
v	v	k7c6	v
tenkém	tenký	k2eAgInSc6d1	tenký
typu	typ	k1gInSc6	typ
kůže	kůže	k1gFnSc2	kůže
(	(	kIx(	(
<g/>
rty	ret	k1gInPc1	ret
<g/>
,	,	kIx,	,
víčka	víčko	k1gNnPc1	víčko
<g/>
,	,	kIx,	,
podpaží	podpaží	k1gNnSc1	podpaží
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stratum	Stratum	k1gNnSc1	Stratum
granulosum	granulosum	k1gInSc1	granulosum
–	–	k?	–
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
vrstev	vrstva	k1gFnPc2	vrstva
plochých	plochý	k2eAgFnPc2d1	plochá
buněk	buňka	k1gFnPc2	buňka
se	s	k7c7	s
zrny	zrno	k1gNnPc7	zrno
keratohyalinu	keratohyalin	k1gInSc2	keratohyalin
<g/>
;	;	kIx,	;
již	již	k6eAd1	již
nejsou	být	k5eNaImIp3nP	být
mitoticky	mitoticky	k6eAd1	mitoticky
aktivní	aktivní	k2eAgMnPc1d1	aktivní
<g/>
.	.	kIx.	.
</s>
<s>
Stratum	Stratum	k1gNnSc1	Stratum
spinosum	spinosum	k1gInSc1	spinosum
–	–	k?	–
několik	několik	k4yIc4	několik
vrstev	vrstva	k1gFnPc2	vrstva
buněk	buňka	k1gFnPc2	buňka
s	s	k7c7	s
vysokým	vysoký	k2eAgInSc7d1	vysoký
stupněm	stupeň	k1gInSc7	stupeň
proteosyntézy	proteosyntéza	k1gFnSc2	proteosyntéza
<g/>
;	;	kIx,	;
buňky	buňka	k1gFnPc1	buňka
jsou	být	k5eAaImIp3nP	být
stále	stále	k6eAd1	stále
mitoticky	mitoticky	k6eAd1	mitoticky
aktivní	aktivní	k2eAgMnPc1d1	aktivní
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
vyvinuté	vyvinutý	k2eAgInPc1d1	vyvinutý
spojovací	spojovací	k2eAgInPc1d1	spojovací
komplexy	komplex	k1gInPc1	komplex
<g/>
.	.	kIx.	.
</s>
<s>
Stratum	Stratum	k1gNnSc4	Stratum
basale	basale	k6eAd1	basale
–	–	k?	–
1	[number]	k4	1
vrstva	vrstva	k1gFnSc1	vrstva
nízkých	nízký	k2eAgFnPc2d1	nízká
cylindrických	cylindrický	k2eAgFnPc2d1	cylindrická
buněk	buňka	k1gFnPc2	buňka
<g/>
;	;	kIx,	;
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
kmenové	kmenový	k2eAgFnPc4d1	kmenová
buňky	buňka	k1gFnPc4	buňka
kůže	kůže	k1gFnSc2	kůže
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
mitoticky	mitoticky	k6eAd1	mitoticky
velmi	velmi	k6eAd1	velmi
aktivní	aktivní	k2eAgMnSc1d1	aktivní
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Stratum	Stratum	k1gNnSc1	Stratum
basale	basale	k6eAd1	basale
a	a	k8xC	a
spinosum	spinosum	k1gInSc1	spinosum
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
mitotickou	mitotický	k2eAgFnSc4d1	mitotická
aktivitu	aktivita	k1gFnSc4	aktivita
nazývají	nazývat	k5eAaImIp3nP	nazývat
stratum	stratum	k1gNnSc4	stratum
germinativum	germinativum	k1gNnSc1	germinativum
<g/>
)	)	kIx)	)
Nové	Nové	k2eAgFnPc1d1	Nové
buňky	buňka	k1gFnPc1	buňka
vznikají	vznikat	k5eAaImIp3nP	vznikat
dělením	dělení	k1gNnSc7	dělení
buněk	buňka	k1gFnPc2	buňka
(	(	kIx(	(
<g/>
mitózou	mitóza	k1gFnSc7	mitóza
<g/>
)	)	kIx)	)
ve	v	k7c6	v
vnitřních	vnitřní	k2eAgFnPc6d1	vnitřní
vrstvách	vrstva	k1gFnPc6	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
buňky	buňka	k1gFnPc1	buňka
dostávají	dostávat	k5eAaImIp3nP	dostávat
do	do	k7c2	do
svrchních	svrchní	k2eAgFnPc2d1	svrchní
vrstev	vrstva	k1gFnPc2	vrstva
<g/>
,	,	kIx,	,
mění	měnit	k5eAaImIp3nS	měnit
tvar	tvar	k1gInSc4	tvar
a	a	k8xC	a
diferencují	diferencovat	k5eAaImIp3nP	diferencovat
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
plní	plnit	k5eAaImIp3nP	plnit
proteinem	protein	k1gInSc7	protein
keratinem	keratin	k1gInSc7	keratin
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
buňky	buňka	k1gFnPc1	buňka
dostanou	dostat	k5eAaPmIp3nP	dostat
do	do	k7c2	do
nejsvrchnější	svrchní	k2eAgFnSc2d3	nejsvrchnější
vrstvy	vrstva	k1gFnSc2	vrstva
<g/>
,	,	kIx,	,
zvané	zvaný	k2eAgNnSc1d1	zvané
stratum	stratum	k1gNnSc1	stratum
corneum	corneum	k1gInSc1	corneum
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
buď	buď	k8xC	buď
svléknuty	svléknout	k5eAaPmNgFnP	svléknout
(	(	kIx(	(
<g/>
známé	známý	k2eAgNnSc1d1	známé
svlékání	svlékání	k1gNnSc1	svlékání
kůže	kůže	k1gFnSc2	kůže
<g/>
,	,	kIx,	,
např.	např.	kA	např.
u	u	k7c2	u
hadů	had	k1gMnPc2	had
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
odloupnou	odloupnout	k5eAaPmIp3nP	odloupnout
(	(	kIx(	(
<g/>
běžné	běžný	k2eAgFnPc1d1	běžná
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svrchní	svrchní	k2eAgFnSc6d1	svrchní
části	část	k1gFnSc6	část
pokožky	pokožka	k1gFnSc2	pokožka
<g/>
,	,	kIx,	,
stratu	stratus	k1gInSc2	stratus
corneu	corneu	k6eAd1	corneu
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
15-20	[number]	k4	15-20
vrstev	vrstva	k1gFnPc2	vrstva
mrtvých	mrtvý	k2eAgFnPc2d1	mrtvá
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
odlupují	odlupovat	k5eAaImIp3nP	odlupovat
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
proces	proces	k1gInSc1	proces
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
cyklus	cyklus	k1gInSc1	cyklus
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
pokožkových	pokožkový	k2eAgFnPc2d1	pokožková
buněk	buňka	k1gFnPc2	buňka
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
keratinizace	keratinizace	k1gFnSc1	keratinizace
<g/>
.	.	kIx.	.
</s>
