<s>
Pokožka	pokožka	k1gFnSc1	pokožka
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
žádné	žádný	k3yNgFnPc4	žádný
cévy	céva	k1gFnPc4	céva
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
vyživována	vyživovat	k5eAaImNgFnS	vyživovat
pouze	pouze	k6eAd1	pouze
difuzí	difuze	k1gFnSc7	difuze
ze	z	k7c2	z
škáry	škára	k1gFnSc2	škára
(	(	kIx(	(
<g/>
dermis	dermis	k1gInSc1	dermis
<g/>
)	)	kIx)	)
a	a	k8xC	a
kyslík	kyslík	k1gInSc4	kyslík
si	se	k3xPyFc3	se
bere	brát	k5eAaImIp3nS	brát
přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
okolního	okolní	k2eAgInSc2d1	okolní
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
