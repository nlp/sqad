<p>
<s>
Bajkal	Bajkal	k1gInSc1	Bajkal
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
Б	Б	k?	Б
<g/>
,	,	kIx,	,
Bajkal	Bajkal	k1gInSc1	Bajkal
<g/>
,	,	kIx,	,
burjatsky	burjatsky	k6eAd1	burjatsky
Б	Б	k?	Б
<g/>
,	,	kIx,	,
Bajgal	Bajgal	k1gInSc1	Bajgal
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jezero	jezero	k1gNnSc1	jezero
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
východní	východní	k2eAgFnSc2d1	východní
Sibiře	Sibiř	k1gFnSc2	Sibiř
<g/>
,	,	kIx,	,
na	na	k7c6	na
rozhraní	rozhraní	k1gNnSc6	rozhraní
Burjatské	burjatský	k2eAgFnSc2d1	Burjatská
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
Irkutské	irkutský	k2eAgFnSc2d1	Irkutská
oblasti	oblast	k1gFnSc2	oblast
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nejstarší	starý	k2eAgNnSc1d3	nejstarší
a	a	k8xC	a
nejhlubší	hluboký	k2eAgNnSc1d3	nejhlubší
jezero	jezero	k1gNnSc1	jezero
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
stáří	stáří	k1gNnSc1	stáří
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
na	na	k7c4	na
25	[number]	k4	25
až	až	k9	až
30	[number]	k4	30
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
a	a	k8xC	a
maximální	maximální	k2eAgFnSc1d1	maximální
hloubka	hloubka	k1gFnSc1	hloubka
je	být	k5eAaImIp3nS	být
1642	[number]	k4	1642
m.	m.	k?	m.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
Kaspickém	kaspický	k2eAgNnSc6d1	Kaspické
moři	moře	k1gNnSc6	moře
jde	jít	k5eAaImIp3nS	jít
také	také	k9	také
o	o	k7c4	o
druhé	druhý	k4xOgNnSc4	druhý
největší	veliký	k2eAgNnSc4d3	veliký
jezero	jezero	k1gNnSc4	jezero
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
místní	místní	k2eAgMnPc1d1	místní
obyvatelé	obyvatel	k1gMnPc1	obyvatel
o	o	k7c6	o
něm	on	k3xPp3gInSc6	on
obvykle	obvykle	k6eAd1	obvykle
hovoří	hovořit	k5eAaImIp3nS	hovořit
jako	jako	k9	jako
o	o	k7c6	o
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jezeře	jezero	k1gNnSc6	jezero
žije	žít	k5eAaImIp3nS	žít
1	[number]	k4	1
200	[number]	k4	200
druhů	druh	k1gInPc2	druh
živočichů	živočich	k1gMnPc2	živočich
<g/>
,	,	kIx,	,
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
tři	tři	k4xCgFnPc1	tři
čtvrtiny	čtvrtina	k1gFnPc1	čtvrtina
jsou	být	k5eAaImIp3nP	být
endemity	endemit	k1gInPc4	endemit
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
zde	zde	k6eAd1	zde
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Bajkalu	Bajkal	k1gInSc6	Bajkal
je	být	k5eAaImIp3nS	být
soustředěna	soustředit	k5eAaPmNgFnS	soustředit
jedna	jeden	k4xCgFnSc1	jeden
pětina	pětina	k1gFnSc1	pětina
světových	světový	k2eAgFnPc2d1	světová
zásob	zásoba	k1gFnPc2	zásoba
povrchové	povrchový	k2eAgFnSc2d1	povrchová
sladké	sladký	k2eAgFnSc2d1	sladká
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Popis	popis	k1gInSc4	popis
===	===	k?	===
</s>
</p>
<p>
<s>
Bajkal	Bajkal	k1gInSc1	Bajkal
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
hlubokou	hluboký	k2eAgFnSc7d1	hluboká
příkopovou	příkopový	k2eAgFnSc7d1	příkopová
propadlinou	propadlina	k1gFnSc7	propadlina
na	na	k7c4	na
rozhraní	rozhraní	k1gNnSc4	rozhraní
dvou	dva	k4xCgFnPc2	dva
tektonických	tektonický	k2eAgFnPc2d1	tektonická
desek	deska	k1gFnPc2	deska
(	(	kIx(	(
<g/>
eurasijské	eurasijský	k2eAgFnPc1d1	eurasijská
a	a	k8xC	a
amurské	amurský	k2eAgFnPc1d1	Amurská
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
každým	každý	k3xTgInSc7	každý
rokem	rok	k1gInSc7	rok
rozestupují	rozestupovat	k5eAaImIp3nP	rozestupovat
zhruba	zhruba	k6eAd1	zhruba
o	o	k7c4	o
2	[number]	k4	2
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
31	[number]	k4	31
500	[number]	k4	500
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
636	[number]	k4	636
km	km	kA	km
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
a	a	k8xC	a
šířka	šířka	k1gFnSc1	šířka
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
od	od	k7c2	od
27	[number]	k4	27
do	do	k7c2	do
79,4	[number]	k4	79,4
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Rozloha	rozloha	k1gFnSc1	rozloha
povodí	povodí	k1gNnSc2	povodí
je	být	k5eAaImIp3nS	být
557	[number]	k4	557
000	[number]	k4	000
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
objem	objem	k1gInSc1	objem
vody	voda	k1gFnSc2	voda
je	být	k5eAaImIp3nS	být
23	[number]	k4	23
000	[number]	k4	000
km3	km3	k4	km3
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
nadmořská	nadmořský	k2eAgFnSc1d1	nadmořská
výška	výška	k1gFnSc1	výška
hladiny	hladina	k1gFnSc2	hladina
jezera	jezero	k1gNnSc2	jezero
je	být	k5eAaImIp3nS	být
456	[number]	k4	456
m.	m.	k?	m.
</s>
</p>
<p>
<s>
===	===	k?	===
Pobřeží	pobřeží	k1gNnSc2	pobřeží
===	===	k?	===
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
obklopený	obklopený	k2eAgInSc1d1	obklopený
horskými	horský	k2eAgInPc7d1	horský
hřbety	hřbet	k1gInPc7	hřbet
vysokými	vysoký	k2eAgInPc7d1	vysoký
až	až	k6eAd1	až
2000	[number]	k4	2000
m	m	kA	m
nad	nad	k7c7	nad
jeho	jeho	k3xOp3gFnSc7	jeho
hladinou	hladina	k1gFnSc7	hladina
<g/>
.	.	kIx.	.
</s>
<s>
Pobřeží	pobřeží	k1gNnSc1	pobřeží
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
mírně	mírně	k6eAd1	mírně
členité	členitý	k2eAgNnSc1d1	členité
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
2100	[number]	k4	2100
km	km	kA	km
dlouhé	dlouhý	k2eAgNnSc1d1	dlouhé
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
největší	veliký	k2eAgFnPc1d3	veliký
záhyby	záhyba	k1gFnPc1	záhyba
jsou	být	k5eAaImIp3nP	být
zálivy	záliv	k1gInPc4	záliv
Barguzinský	Barguzinský	k2eAgInSc1d1	Barguzinský
<g/>
,	,	kIx,	,
Čivyrkujský	Čivyrkujský	k2eAgInSc1d1	Čivyrkujský
a	a	k8xC	a
Velký	velký	k2eAgInSc1d1	velký
Proval	provalit	k5eAaPmRp2nS	provalit
<g/>
,	,	kIx,	,
zátoky	zátoka	k1gFnPc1	zátoka
Ajaja	Ajajus	k1gMnSc2	Ajajus
a	a	k8xC	a
Frolicha	Frolich	k1gMnSc2	Frolich
a	a	k8xC	a
poloostrov	poloostrov	k1gInSc4	poloostrov
Svatý	svatý	k2eAgInSc4d1	svatý
nos	nos	k1gInSc4	nos
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Dno	dno	k1gNnSc1	dno
===	===	k?	===
</s>
</p>
<p>
<s>
Kotlina	kotlina	k1gFnSc1	kotlina
jezera	jezero	k1gNnSc2	jezero
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
přímo	přímo	k6eAd1	přímo
uprostřed	uprostřed	k7c2	uprostřed
Bajkalské	bajkalský	k2eAgFnSc2d1	Bajkalská
riftové	riftový	k2eAgFnSc2d1	riftový
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
geofyzikálního	geofyzikální	k2eAgInSc2d1	geofyzikální
výzkumu	výzkum	k1gInSc2	výzkum
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
vodní	vodní	k2eAgFnSc7d1	vodní
masou	masa	k1gFnSc7	masa
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
částech	část	k1gFnPc6	část
kotliny	kotlina	k1gFnSc2	kotlina
nacházejí	nacházet	k5eAaImIp3nP	nacházet
usazené	usazený	k2eAgFnPc1d1	usazená
vrstvy	vrstva	k1gFnPc1	vrstva
o	o	k7c6	o
tloušťce	tloušťka	k1gFnSc6	tloušťka
až	až	k9	až
6000	[number]	k4	6000
m.	m.	k?	m.
Za	za	k7c4	za
začátek	začátek	k1gInSc4	začátek
vytváření	vytváření	k1gNnSc2	vytváření
bajkalské	bajkalský	k2eAgFnSc2d1	Bajkalská
propadliny	propadlina	k1gFnSc2	propadlina
a	a	k8xC	a
jejího	její	k3xOp3gNnSc2	její
zaplňování	zaplňování	k1gNnSc2	zaplňování
vodou	voda	k1gFnSc7	voda
považuje	považovat	k5eAaImIp3nS	považovat
většina	většina	k1gFnSc1	většina
vědců	vědec	k1gMnPc2	vědec
konec	konec	k1gInSc4	konec
paleocénu	paleocén	k1gInSc2	paleocén
a	a	k8xC	a
raný	raný	k2eAgInSc1d1	raný
neogén	neogén	k1gInSc1	neogén
(	(	kIx(	(
<g/>
tzn.	tzn.	kA	tzn.
celkový	celkový	k2eAgInSc1d1	celkový
věk	věk	k1gInSc1	věk
jezera	jezero	k1gNnSc2	jezero
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
25	[number]	k4	25
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tektonické	tektonický	k2eAgInPc1d1	tektonický
pohyby	pohyb	k1gInPc1	pohyb
pokračují	pokračovat	k5eAaImIp3nP	pokračovat
i	i	k9	i
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
o	o	k7c6	o
čemž	což	k3yQnSc6	což
svědčí	svědčit	k5eAaImIp3nP	svědčit
mnohá	mnohý	k2eAgNnPc1d1	mnohé
zemětřesení	zemětřesení	k1gNnPc1	zemětřesení
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
jsou	být	k5eAaImIp3nP	být
někdy	někdy	k6eAd1	někdy
doprovázena	doprovázet	k5eAaImNgFnS	doprovázet
viditelným	viditelný	k2eAgInSc7d1	viditelný
propadem	propad	k1gInSc7	propad
částí	část	k1gFnPc2	část
břehů	břeh	k1gInPc2	břeh
a	a	k8xC	a
dna	dno	k1gNnSc2	dno
jezera	jezero	k1gNnSc2	jezero
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1862	[number]	k4	1862
se	se	k3xPyFc4	se
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
delty	delta	k1gFnSc2	delta
Selengy	Seleng	k1gInPc4	Seleng
propadl	propadnout	k5eAaPmAgMnS	propadnout
do	do	k7c2	do
hloubky	hloubka	k1gFnSc2	hloubka
10	[number]	k4	10
m	m	kA	m
pod	pod	k7c4	pod
vodu	voda	k1gFnSc4	voda
kus	kus	k6eAd1	kus
pobřežní	pobřežní	k2eAgFnSc2d1	pobřežní
souše	souš	k1gFnSc2	souš
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
200	[number]	k4	200
km2	km2	k4	km2
a	a	k8xC	a
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
místě	místo	k1gNnSc6	místo
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
záliv	záliv	k1gInSc1	záliv
Velký	velký	k2eAgInSc1d1	velký
Proval	provalit	k5eAaPmRp2nS	provalit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kotlina	kotlina	k1gFnSc1	kotlina
Bajkalu	Bajkal	k1gInSc2	Bajkal
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
nesouměrností	nesouměrnost	k1gFnSc7	nesouměrnost
příčného	příčný	k2eAgInSc2d1	příčný
řezu	řez	k1gInSc2	řez
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
prudkost	prudkost	k1gFnSc1	prudkost
pobřežních	pobřežní	k2eAgInPc2d1	pobřežní
a	a	k8xC	a
podvodních	podvodní	k2eAgInPc2d1	podvodní
svahů	svah	k1gInPc2	svah
na	na	k7c6	na
západním	západní	k2eAgInSc6d1	západní
břehu	břeh	k1gInSc6	břeh
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
kontrastu	kontrast	k1gInSc6	kontrast
s	s	k7c7	s
mírnějšími	mírný	k2eAgInPc7d2	mírnější
východními	východní	k2eAgInPc7d1	východní
břehy	břeh	k1gInPc7	břeh
s	s	k7c7	s
nevelkými	velký	k2eNgFnPc7d1	nevelká
plochami	plocha	k1gFnPc7	plocha
mělčin	mělčina	k1gFnPc2	mělčina
<g/>
.	.	kIx.	.
</s>
<s>
Pobřežní	pobřežní	k2eAgFnPc1d1	pobřežní
části	část	k1gFnPc1	část
s	s	k7c7	s
hloubkou	hloubka	k1gFnSc7	hloubka
do	do	k7c2	do
50	[number]	k4	50
m	m	kA	m
tvoří	tvořit	k5eAaImIp3nP	tvořit
pouze	pouze	k6eAd1	pouze
okolo	okolo	k7c2	okolo
8	[number]	k4	8
%	%	kIx~	%
rozlohy	rozloh	k1gInPc1	rozloh
dna	dno	k1gNnSc2	dno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ostrovy	ostrov	k1gInPc1	ostrov
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
jezeře	jezero	k1gNnSc6	jezero
je	být	k5eAaImIp3nS	být
27	[number]	k4	27
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
22	[number]	k4	22
stálých	stálý	k2eAgFnPc2d1	stálá
a	a	k8xC	a
5	[number]	k4	5
periodicky	periodicky	k6eAd1	periodicky
zaplavovaných	zaplavovaný	k2eAgMnPc2d1	zaplavovaný
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInPc1d3	veliký
jsou	být	k5eAaImIp3nP	být
ostrovy	ostrov	k1gInPc1	ostrov
Olchon	Olchona	k1gFnPc2	Olchona
(	(	kIx(	(
<g/>
730	[number]	k4	730
km2	km2	k4	km2
<g/>
)	)	kIx)	)
a	a	k8xC	a
Velký	velký	k2eAgMnSc1d1	velký
Uškanij	Uškanij	k1gMnSc1	Uškanij
(	(	kIx(	(
<g/>
9,4	[number]	k4	9,4
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Vodní	vodní	k2eAgInSc1d1	vodní
režim	režim	k1gInSc1	režim
==	==	k?	==
</s>
</p>
<p>
<s>
Do	do	k7c2	do
Bajkalu	Bajkal	k1gInSc2	Bajkal
ústí	ústit	k5eAaImIp3nS	ústit
336	[number]	k4	336
řek	řeka	k1gFnPc2	řeka
a	a	k8xC	a
říček	říčka	k1gFnPc2	říčka
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
největší	veliký	k2eAgFnSc1d3	veliký
jsou	být	k5eAaImIp3nP	být
Selenga	Seleng	k1gMnSc4	Seleng
<g/>
,	,	kIx,	,
Barguzin	Barguzin	k1gMnSc1	Barguzin
<g/>
,	,	kIx,	,
Horní	horní	k2eAgFnSc1d1	horní
Angara	Angara	k1gFnSc1	Angara
<g/>
,	,	kIx,	,
Turka	Turek	k1gMnSc2	Turek
a	a	k8xC	a
Sněžnaja	Sněžnajus	k1gMnSc2	Sněžnajus
<g/>
.	.	kIx.	.
</s>
<s>
Odtéká	odtékat	k5eAaImIp3nS	odtékat
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
Angara	Angar	k1gMnSc2	Angar
(	(	kIx(	(
<g/>
Dolní	dolní	k2eAgFnSc1d1	dolní
Angara	Angara	k1gFnSc1	Angara
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ústí	ústit	k5eAaImIp3nS	ústit
do	do	k7c2	do
Jeniseje	Jenisej	k1gInSc2	Jenisej
<g/>
.	.	kIx.	.
</s>
<s>
Roční	roční	k2eAgNnSc1d1	roční
množství	množství	k1gNnSc1	množství
srážek	srážka	k1gFnPc2	srážka
uprostřed	uprostřed	k7c2	uprostřed
jezera	jezero	k1gNnSc2	jezero
a	a	k8xC	a
na	na	k7c6	na
severu	sever	k1gInSc6	sever
je	být	k5eAaImIp3nS	být
200	[number]	k4	200
až	až	k9	až
350	[number]	k4	350
mm	mm	kA	mm
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
500	[number]	k4	500
až	až	k9	až
900	[number]	k4	900
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
mnoho	mnoho	k4c1	mnoho
horkých	horký	k2eAgInPc2d1	horký
pramenů	pramen	k1gInPc2	pramen
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
některé	některý	k3yIgFnPc1	některý
se	se	k3xPyFc4	se
využívají	využívat	k5eAaImIp3nP	využívat
(	(	kIx(	(
<g/>
Gorjačinsk	Gorjačinsk	k1gInSc1	Gorjačinsk
<g/>
,	,	kIx,	,
Chakusy	Chakus	k1gInPc1	Chakus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
ve	v	k7c6	v
vodní	vodní	k2eAgFnSc6d1	vodní
bilanci	bilance	k1gFnSc6	bilance
hrají	hrát	k5eAaImIp3nP	hrát
přítoky	přítok	k1gInPc1	přítok
říčních	říční	k2eAgFnPc2d1	říční
vod	voda	k1gFnPc2	voda
(	(	kIx(	(
<g/>
58,24	[number]	k4	58,24
km3	km3	k4	km3
<g/>
)	)	kIx)	)
a	a	k8xC	a
srážky	srážka	k1gFnPc1	srážka
(	(	kIx(	(
<g/>
9,26	[number]	k4	9,26
km3	km3	k4	km3
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
odtok	odtok	k1gInSc4	odtok
přes	přes	k7c4	přes
Angaru	Angara	k1gFnSc4	Angara
(	(	kIx(	(
<g/>
60,07	[number]	k4	60,07
km3	km3	k4	km3
<g/>
)	)	kIx)	)
a	a	k8xC	a
odpařování	odpařování	k1gNnSc1	odpařování
(	(	kIx(	(
<g/>
9,45	[number]	k4	9,45
km3	km3	k4	km3
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíl	rozdíl	k1gInSc1	rozdíl
připadá	připadat	k5eAaPmIp3nS	připadat
na	na	k7c4	na
přítok	přítok	k1gInSc4	přítok
podzemních	podzemní	k2eAgFnPc2d1	podzemní
vod	voda	k1gFnPc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
50	[number]	k4	50
%	%	kIx~	%
ročního	roční	k2eAgInSc2d1	roční
přítoku	přítok	k1gInSc2	přítok
připadá	připadat	k5eAaPmIp3nS	připadat
na	na	k7c6	na
Selengu	Seleng	k1gInSc6	Seleng
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
úroveň	úroveň	k1gFnSc1	úroveň
hladiny	hladina	k1gFnSc2	hladina
jezero	jezero	k1gNnSc1	jezero
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
a	a	k8xC	a
září	září	k1gNnSc6	září
a	a	k8xC	a
nejnižší	nízký	k2eAgFnSc4d3	nejnižší
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
a	a	k8xC	a
dubnu	duben	k1gInSc6	duben
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
roční	roční	k2eAgFnSc1d1	roční
hodnota	hodnota	k1gFnSc1	hodnota
kolísání	kolísání	k1gNnSc2	kolísání
hladiny	hladina	k1gFnSc2	hladina
činí	činit	k5eAaImIp3nS	činit
0,8	[number]	k4	0,8
m	m	kA	m
<g/>
,	,	kIx,	,
stoletá	stoletý	k2eAgFnSc1d1	stoletá
3	[number]	k4	3
m.	m.	k?	m.
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
dlouhodobá	dlouhodobý	k2eAgFnSc1d1	dlouhodobá
úroveň	úroveň	k1gFnSc1	úroveň
hladiny	hladina	k1gFnSc2	hladina
jezera	jezero	k1gNnSc2	jezero
stoupla	stoupnout	k5eAaPmAgFnS	stoupnout
přibližně	přibližně	k6eAd1	přibližně
o	o	k7c4	o
1	[number]	k4	1
m	m	kA	m
po	po	k7c6	po
postavení	postavení	k1gNnSc6	postavení
Irkutské	irkutský	k2eAgFnSc2d1	Irkutská
přehrady	přehrada	k1gFnSc2	přehrada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kotlina	kotlina	k1gFnSc1	kotlina
jezera	jezero	k1gNnSc2	jezero
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
ryze	ryze	k6eAd1	ryze
kontinentální	kontinentální	k2eAgNnSc1d1	kontinentální
klima	klima	k1gNnSc1	klima
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
objem	objem	k1gInSc1	objem
jezera	jezero	k1gNnSc2	jezero
má	mít	k5eAaImIp3nS	mít
zmírňující	zmírňující	k2eAgInSc4d1	zmírňující
dopad	dopad	k1gInSc4	dopad
na	na	k7c4	na
klima	klima	k1gNnSc4	klima
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Sezónní	sezónní	k2eAgNnSc1d1	sezónní
a	a	k8xC	a
denní	denní	k2eAgNnSc1d1	denní
kolísání	kolísání	k1gNnSc1	kolísání
teploty	teplota	k1gFnSc2	teplota
a	a	k8xC	a
vlhkosti	vlhkost	k1gFnSc2	vlhkost
vzduchu	vzduch	k1gInSc2	vzduch
je	být	k5eAaImIp3nS	být
mírně	mírně	k6eAd1	mírně
sníženo	snížen	k2eAgNnSc1d1	sníženo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
okolními	okolní	k2eAgFnPc7d1	okolní
oblastmi	oblast	k1gFnPc7	oblast
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
Bajkalu	Bajkal	k1gInSc2	Bajkal
mírnější	mírný	k2eAgFnSc1d2	mírnější
zima	zima	k1gFnSc1	zima
a	a	k8xC	a
chladnější	chladný	k2eAgNnSc1d2	chladnější
léto	léto	k1gNnSc1	léto
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
teplota	teplota	k1gFnSc1	teplota
vzduchu	vzduch	k1gInSc2	vzduch
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
a	a	k8xC	a
únoru	únor	k1gInSc6	únor
je	být	k5eAaImIp3nS	být
okolo	okolo	k7c2	okolo
-	-	kIx~	-
19	[number]	k4	19
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
okolo	okolo	k7c2	okolo
11	[number]	k4	11
°	°	k?	°
<g/>
C.	C.	kA	C.
</s>
</p>
<p>
<s>
Oblast	oblast	k1gFnSc1	oblast
Bajkalu	Bajkal	k1gInSc2	Bajkal
je	být	k5eAaImIp3nS	být
charakteristická	charakteristický	k2eAgNnPc4d1	charakteristické
složitým	složitý	k2eAgInSc7d1	složitý
systémem	systém	k1gInSc7	systém
větrů	vítr	k1gInPc2	vítr
<g/>
.	.	kIx.	.
</s>
<s>
Převládají	převládat	k5eAaImIp3nP	převládat
západní	západní	k2eAgInPc1d1	západní
a	a	k8xC	a
severozápadní	severozápadní	k2eAgInPc1d1	severozápadní
větry	vítr	k1gInPc1	vítr
z	z	k7c2	z
břehu	břeh	k1gInSc2	břeh
na	na	k7c4	na
jezero	jezero	k1gNnSc4	jezero
(	(	kIx(	(
<g/>
gornaja	gornaja	k6eAd1	gornaja
a	a	k8xC	a
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
ostrova	ostrov	k1gInSc2	ostrov
Olchon	Olchon	k1gInSc4	Olchon
sarma	sarmum	k1gNnSc2	sarmum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
a	a	k8xC	a
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
zimy	zima	k1gFnSc2	zima
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
větry	vítr	k1gInPc1	vítr
rychlosti	rychlost	k1gFnSc2	rychlost
uragánu	uragán	k1gInSc2	uragán
30	[number]	k4	30
až	až	k9	až
40	[number]	k4	40
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Jižní	jižní	k2eAgInPc1d1	jižní
a	a	k8xC	a
jihovýchodní	jihovýchodní	k2eAgInPc1d1	jihovýchodní
větry	vítr	k1gInPc1	vítr
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
šelonnik	šelonnik	k1gInSc1	šelonnik
<g/>
,	,	kIx,	,
podélné	podélný	k2eAgInPc1d1	podélný
větry	vítr	k1gInPc1	vítr
ze	z	k7c2	z
severovýchodu	severovýchod	k1gInSc2	severovýchod
věrchovik	věrchovika	k1gFnPc2	věrchovika
a	a	k8xC	a
barguzin	barguzina	k1gFnPc2	barguzina
<g/>
,	,	kIx,	,
z	z	k7c2	z
jihozápadu	jihozápad	k1gInSc2	jihozápad
kultuk	kultuka	k1gFnPc2	kultuka
<g/>
.	.	kIx.	.
</s>
<s>
Větrné	větrný	k2eAgFnPc1d1	větrná
vlny	vlna	k1gFnPc1	vlna
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
většinou	většina	k1gFnSc7	většina
výšky	výška	k1gFnSc2	výška
1	[number]	k4	1
až	až	k9	až
3	[number]	k4	3
m	m	kA	m
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
někdy	někdy	k6eAd1	někdy
4	[number]	k4	4
až	až	k9	až
5	[number]	k4	5
i	i	k9	i
více	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
<s>
Konvektivní	Konvektivní	k2eAgNnSc1d1	Konvektivní
a	a	k8xC	a
turbulentní	turbulentní	k2eAgNnSc1d1	turbulentní
promíchávání	promíchávání	k1gNnSc1	promíchávání
zachvacuje	zachvacovat	k5eAaImIp3nS	zachvacovat
horní	horní	k2eAgFnPc4d1	horní
vrstvy	vrstva	k1gFnPc4	vrstva
vody	voda	k1gFnSc2	voda
až	až	k9	až
do	do	k7c2	do
hloubky	hloubka	k1gFnSc2	hloubka
250	[number]	k4	250
m	m	kA	m
<g/>
,	,	kIx,	,
hlouběji	hluboko	k6eAd2	hluboko
promíchávání	promíchávání	k1gNnSc1	promíchávání
slábne	slábnout	k5eAaImIp3nS	slábnout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
větrů	vítr	k1gInPc2	vítr
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
pohybu	pohyb	k1gInSc3	pohyb
povrchové	povrchový	k2eAgFnSc2d1	povrchová
teplé	teplý	k2eAgFnSc2d1	teplá
vody	voda	k1gFnSc2	voda
k	k	k7c3	k
protilehlému	protilehlý	k2eAgInSc3d1	protilehlý
břehu	břeh	k1gInSc3	břeh
a	a	k8xC	a
ke	k	k7c3	k
vzestupu	vzestup	k1gInSc3	vzestup
hlubinné	hlubinný	k2eAgFnSc2d1	hlubinná
chladné	chladný	k2eAgFnSc2d1	chladná
vody	voda	k1gFnSc2	voda
u	u	k7c2	u
břehů	břeh	k1gInPc2	břeh
<g/>
,	,	kIx,	,
od	od	k7c2	od
kterých	který	k3yQgInPc2	který
fouká	foukat	k5eAaImIp3nS	foukat
vítr	vítr	k1gInSc1	vítr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
vody	voda	k1gFnSc2	voda
==	==	k?	==
</s>
</p>
<p>
<s>
Teplota	teplota	k1gFnSc1	teplota
vody	voda	k1gFnSc2	voda
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
na	na	k7c6	na
odkryté	odkrytý	k2eAgFnSc6d1	odkrytá
části	část	k1gFnSc6	část
9	[number]	k4	9
až	až	k9	až
12	[number]	k4	12
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
u	u	k7c2	u
břehu	břeh	k1gInSc2	břeh
někdy	někdy	k6eAd1	někdy
až	až	k9	až
20	[number]	k4	20
°	°	k?	°
<g/>
C.	C.	kA	C.
Sezónní	sezónní	k2eAgNnSc1d1	sezónní
kolísání	kolísání	k1gNnSc1	kolísání
teploty	teplota	k1gFnSc2	teplota
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
do	do	k7c2	do
hloubky	hloubka	k1gFnSc2	hloubka
250	[number]	k4	250
až	až	k9	až
300	[number]	k4	300
m.	m.	k?	m.
Hlouběji	hluboko	k6eAd2	hluboko
je	být	k5eAaImIp3nS	být
teplota	teplota	k1gFnSc1	teplota
stálá	stálý	k2eAgFnSc1d1	stálá
3,2	[number]	k4	3,2
až	až	k9	až
3,5	[number]	k4	3,5
°	°	k?	°
<g/>
С	С	k?	С
Ve	v	k7c6	v
vrstvách	vrstva	k1gFnPc6	vrstva
u	u	k7c2	u
dna	dno	k1gNnSc2	dno
<g/>
,	,	kIx,	,
ve	v	k7c6	v
velkých	velký	k2eAgFnPc6d1	velká
hloubkách	hloubka	k1gFnPc6	hloubka
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
vzestup	vzestup	k1gInSc1	vzestup
teploty	teplota	k1gFnSc2	teplota
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
ohřevu	ohřev	k1gInSc2	ohřev
vnitřním	vnitřní	k2eAgNnSc7d1	vnitřní
teplem	teplo	k1gNnSc7	teplo
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Bajkal	Bajkal	k1gInSc1	Bajkal
zamrzá	zamrzat	k5eAaImIp3nS	zamrzat
většinou	většina	k1gFnSc7	většina
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
a	a	k8xC	a
rozmrzá	rozmrzat	k5eAaImIp3nS	rozmrzat
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
<g/>
.	.	kIx.	.
</s>
<s>
Tloušťka	tloušťka	k1gFnSc1	tloušťka
ledu	led	k1gInSc2	led
je	být	k5eAaImIp3nS	být
70	[number]	k4	70
až	až	k9	až
115	[number]	k4	115
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Voda	voda	k1gFnSc1	voda
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
průzračná	průzračný	k2eAgFnSc1d1	průzračná
(	(	kIx(	(
<g/>
až	až	k9	až
40	[number]	k4	40
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
málo	málo	k6eAd1	málo
mineralizovaná	mineralizovaný	k2eAgFnSc1d1	mineralizovaná
a	a	k8xC	a
mírně	mírně	k6eAd1	mírně
alkalická	alkalický	k2eAgFnSc1d1	alkalická
<g/>
.	.	kIx.	.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
obsah	obsah	k1gInSc1	obsah
soli	sůl	k1gFnSc2	sůl
nepřevyšuje	převyšovat	k5eNaImIp3nS	převyšovat
150	[number]	k4	150
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
l.	l.	k?	l.
Iontové	iontový	k2eAgNnSc4d1	iontové
složení	složení	k1gNnSc4	složení
je	být	k5eAaImIp3nS	být
obdobné	obdobný	k2eAgNnSc4d1	obdobné
jako	jako	k8xC	jako
v	v	k7c6	v
přítocích	přítok	k1gInPc6	přítok
<g/>
.	.	kIx.	.
</s>
<s>
Voda	voda	k1gFnSc1	voda
je	být	k5eAaImIp3nS	být
bohatá	bohatý	k2eAgFnSc1d1	bohatá
na	na	k7c4	na
kyslík	kyslík	k1gInSc4	kyslík
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
vrstvách	vrstva	k1gFnPc6	vrstva
až	až	k6eAd1	až
do	do	k7c2	do
nejhlubších	hluboký	k2eAgNnPc2d3	nejhlubší
míst	místo	k1gNnPc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
obsah	obsah	k1gInSc1	obsah
neklesá	klesat	k5eNaImIp3nS	klesat
pod	pod	k7c7	pod
9	[number]	k4	9
až	až	k9	až
10	[number]	k4	10
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
l.	l.	k?	l.
</s>
</p>
<p>
<s>
Chemické	chemický	k2eAgFnPc1d1	chemická
látky	látka	k1gFnPc1	látka
obsažené	obsažený	k2eAgFnPc1d1	obsažená
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
Bajkalu	Bajkal	k1gInSc2	Bajkal
<g/>
'	'	kIx"	'
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc2	jeho
přítoků	přítok	k1gInPc2	přítok
(	(	kIx(	(
<g/>
několikaletý	několikaletý	k2eAgInSc1d1	několikaletý
průměr	průměr	k1gInSc1	průměr
v	v	k7c6	v
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
l	l	kA	l
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
hydrogenuhličitany	hydrogenuhličitan	k1gInPc4	hydrogenuhličitan
(	(	kIx(	(
<g/>
66,5	[number]	k4	66,5
<g/>
/	/	kIx~	/
<g/>
79,3	[number]	k4	79,3
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sírany	síran	k1gInPc1	síran
(	(	kIx(	(
<g/>
5,2	[number]	k4	5,2
<g/>
/	/	kIx~	/
<g/>
6,7	[number]	k4	6,7
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
chloridy	chlorid	k1gInPc1	chlorid
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
0,5	[number]	k4	0,5
<g/>
/	/	kIx~	/
<g/>
0,8	[number]	k4	0,8
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vápník	vápník	k1gInSc1	vápník
(	(	kIx(	(
<g/>
15,2	[number]	k4	15,2
<g/>
/	/	kIx~	/
<g/>
20	[number]	k4	20
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hořčík	hořčík	k1gInSc1	hořčík
(	(	kIx(	(
<g/>
3,1	[number]	k4	3,1
<g/>
/	/	kIx~	/
<g/>
4,3	[number]	k4	4,3
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sodík	sodík	k1gInSc1	sodík
(	(	kIx(	(
<g/>
3,8	[number]	k4	3,8
<g/>
/	/	kIx~	/
<g/>
3,4	[number]	k4	3,4
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
draslík	draslík	k1gInSc1	draslík
(	(	kIx(	(
<g/>
2,0	[number]	k4	2,0
<g/>
/	/	kIx~	/
<g/>
1,7	[number]	k4	1,7
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
křemík	křemík	k1gInSc1	křemík
(	(	kIx(	(
<g/>
1,1	[number]	k4	1,1
<g/>
/	/	kIx~	/
<g/>
4,2	[number]	k4	4,2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
železo	železo	k1gNnSc1	železo
(	(	kIx(	(
<g/>
0,02	[number]	k4	0,02
<g/>
/	/	kIx~	/
<g/>
0,16	[number]	k4	0,16
<g/>
)	)	kIx)	)
a	a	k8xC	a
organické	organický	k2eAgFnPc1d1	organická
látky	látka	k1gFnPc1	látka
(	(	kIx(	(
<g/>
3,2	[number]	k4	3,2
<g/>
/	/	kIx~	/
<g/>
10,3	[number]	k4	10,3
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fauna	fauna	k1gFnSc1	fauna
a	a	k8xC	a
flóra	flóra	k1gFnSc1	flóra
==	==	k?	==
</s>
</p>
<p>
<s>
Rostlinný	rostlinný	k2eAgInSc1d1	rostlinný
a	a	k8xC	a
živočišný	živočišný	k2eAgInSc1d1	živočišný
svět	svět	k1gInSc1	svět
jezera	jezero	k1gNnSc2	jezero
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
bohatý	bohatý	k2eAgMnSc1d1	bohatý
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
hladiny	hladina	k1gFnSc2	hladina
až	až	k9	až
po	po	k7c6	po
nejhlubší	hluboký	k2eAgFnSc6d3	nejhlubší
místa	místo	k1gNnSc2	místo
ho	on	k3xPp3gMnSc4	on
obývá	obývat	k5eAaImIp3nS	obývat
na	na	k7c4	na
600	[number]	k4	600
druhů	druh	k1gInPc2	druh
rostlin	rostlina	k1gFnPc2	rostlina
a	a	k8xC	a
1	[number]	k4	1
200	[number]	k4	200
druhů	druh	k1gInPc2	druh
živočichů	živočich	k1gMnPc2	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgFnPc1	tři
čtvrtiny	čtvrtina	k1gFnPc1	čtvrtina
druhů	druh	k1gInPc2	druh
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
pouze	pouze	k6eAd1	pouze
zde	zde	k6eAd1	zde
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
rostlin	rostlina	k1gFnPc2	rostlina
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
především	především	k9	především
mnoho	mnoho	k4c4	mnoho
druhů	druh	k1gInPc2	druh
rozsivek	rozsivka	k1gFnPc2	rozsivka
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
prvoků	prvok	k1gMnPc2	prvok
zde	zde	k6eAd1	zde
žije	žít	k5eAaImIp3nS	žít
mnoho	mnoho	k4c1	mnoho
druhů	druh	k1gInPc2	druh
nálevníků	nálevník	k1gMnPc2	nálevník
(	(	kIx(	(
<g/>
okolo	okolo	k7c2	okolo
300	[number]	k4	300
druhů	druh	k1gInPc2	druh
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Svérázní	svérázný	k2eAgMnPc1d1	svérázný
jsou	být	k5eAaImIp3nP	být
bajkalští	bajkalský	k2eAgMnPc1d1	bajkalský
houbovci	houbovec	k1gInSc3	houbovec
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
zde	zde	k6eAd1	zde
žije	žít	k5eAaImIp3nS	žít
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
červů	červ	k1gMnPc2	červ
<g/>
,	,	kIx,	,
měkkýšů	měkkýš	k1gMnPc2	měkkýš
<g/>
,	,	kIx,	,
chrostíků	chrostík	k1gMnPc2	chrostík
a	a	k8xC	a
především	především	k9	především
korýšů	korýš	k1gMnPc2	korýš
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
korýši	korýš	k1gMnPc1	korýš
jsou	být	k5eAaImIp3nP	být
významnou	významný	k2eAgFnSc7d1	významná
složkou	složka	k1gFnSc7	složka
jídelníčku	jídelníček	k1gInSc2	jídelníček
ryb	ryba	k1gFnPc2	ryba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
jezeře	jezero	k1gNnSc6	jezero
žije	žít	k5eAaImIp3nS	žít
50	[number]	k4	50
druhů	druh	k1gInPc2	druh
ryb	ryba	k1gFnPc2	ryba
ze	z	k7c2	z
7	[number]	k4	7
řádů	řád	k1gInPc2	řád
<g/>
.	.	kIx.	.
</s>
<s>
Nejpočetněji	početně	k6eAd3	početně
je	být	k5eAaImIp3nS	být
zastoupena	zastoupit	k5eAaPmNgFnS	zastoupit
skupina	skupina	k1gFnSc1	skupina
hlaváčovitých	hlaváčovitý	k2eAgMnPc2d1	hlaváčovitý
(	(	kIx(	(
<g/>
25	[number]	k4	25
druhů	druh	k1gInPc2	druh
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
průmyslový	průmyslový	k2eAgInSc1d1	průmyslový
význam	význam	k1gInSc1	význam
z	z	k7c2	z
lososovitých	lososovití	k1gMnPc2	lososovití
má	mít	k5eAaImIp3nS	mít
omul	omul	k1gInSc1	omul
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
představuje	představovat	k5eAaImIp3nS	představovat
2⁄	2⁄	k?	2⁄
celkového	celkový	k2eAgInSc2d1	celkový
ročního	roční	k2eAgInSc2d1	roční
výlovu	výlov	k1gInSc2	výlov
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
následují	následovat	k5eAaImIp3nP	následovat
lipani	lipan	k1gMnPc1	lipan
a	a	k8xC	a
sízy	síza	k1gFnPc1	síza
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgMnSc1d3	veliký
představitel	představitel	k1gMnSc1	představitel
vodní	vodní	k2eAgFnSc2d1	vodní
fauny	fauna	k1gFnSc2	fauna
je	být	k5eAaImIp3nS	být
jeseter	jeseter	k1gMnSc1	jeseter
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
délky	délka	k1gFnSc2	délka
až	až	k9	až
180	[number]	k4	180
cm	cm	kA	cm
a	a	k8xC	a
váží	vážit	k5eAaImIp3nP	vážit
100	[number]	k4	100
až	až	k9	až
120	[number]	k4	120
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
ryb	ryba	k1gFnPc2	ryba
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
průmyslově	průmyslově	k6eAd1	průmyslově
neloví	lovit	k5eNaImIp3nS	lovit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nejznámější	známý	k2eAgFnSc1d3	nejznámější
holměnka	holměnka	k1gFnSc1	holměnka
velká	velký	k2eAgFnSc1d1	velká
<g/>
,	,	kIx,	,
živorodá	živorodý	k2eAgFnSc1d1	živorodá
ryba	ryba	k1gFnSc1	ryba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
endemickému	endemický	k2eAgInSc3d1	endemický
řádu	řád	k1gInSc3	řád
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
savců	savec	k1gMnPc2	savec
je	být	k5eAaImIp3nS	být
nejznámější	známý	k2eAgFnSc1d3	nejznámější
něrpa	něrpa	k1gFnSc1	něrpa
–	–	k?	–
tuleň	tuleň	k1gMnSc1	tuleň
bajkalský	bajkalský	k2eAgMnSc1d1	bajkalský
<g/>
,	,	kIx,	,
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
kriticky	kriticky	k6eAd1	kriticky
ohrožený	ohrožený	k2eAgInSc1d1	ohrožený
nezákonným	zákonný	k2eNgInSc7d1	nezákonný
lovem	lov	k1gInSc7	lov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Doprava	doprava	k1gFnSc1	doprava
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
jezeře	jezero	k1gNnSc6	jezero
je	být	k5eAaImIp3nS	být
rozvinutá	rozvinutý	k2eAgFnSc1d1	rozvinutá
místní	místní	k2eAgFnSc1d1	místní
lodní	lodní	k2eAgFnSc1d1	lodní
doprava	doprava	k1gFnSc1	doprava
a	a	k8xC	a
splavování	splavování	k1gNnSc1	splavování
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
Bajkalu	Bajkal	k1gInSc2	Bajkal
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
možností	možnost	k1gFnPc2	možnost
jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
dostat	dostat	k5eAaPmF	dostat
ze	z	k7c2	z
západního	západní	k2eAgInSc2d1	západní
břehu	břeh	k1gInSc2	břeh
na	na	k7c4	na
východní	východní	k2eAgFnSc4d1	východní
právě	právě	k9	právě
cesta	cesta	k1gFnSc1	cesta
trajektem	trajekt	k1gInSc7	trajekt
<g/>
.	.	kIx.	.
</s>
<s>
Vodní	vodní	k2eAgFnSc1d1	vodní
cesta	cesta	k1gFnSc1	cesta
Severobajkalsk	Severobajkalsk	k1gInSc1	Severobajkalsk
-	-	kIx~	-
Chakusy	Chakus	k1gInPc1	Chakus
nabízí	nabízet	k5eAaImIp3nP	nabízet
nevšední	všední	k2eNgInSc4d1	nevšední
cca	cca	kA	cca
2,5	[number]	k4	2,5
<g/>
hodinový	hodinový	k2eAgInSc4d1	hodinový
zážitek	zážitek	k1gInSc4	zážitek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
Chakusy	Chakus	k1gMnPc4	Chakus
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
oblíbené	oblíbený	k2eAgNnSc1d1	oblíbené
rekreační	rekreační	k2eAgNnSc1d1	rekreační
centrum	centrum	k1gNnSc1	centrum
s	s	k7c7	s
termálními	termální	k2eAgInPc7d1	termální
prameny	pramen	k1gInPc7	pramen
a	a	k8xC	a
možností	možnost	k1gFnSc7	možnost
mnoha	mnoho	k4c2	mnoho
výletů	výlet	k1gInPc2	výlet
či	či	k8xC	či
prostě	prostě	k9	prostě
jen	jen	k9	jen
krásné	krásný	k2eAgNnSc4d1	krásné
místo	místo	k1gNnSc4	místo
k	k	k7c3	k
odpočinku	odpočinek	k1gInSc3	odpočinek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Osídlení	osídlení	k1gNnSc3	osídlení
pobřeží	pobřeží	k1gNnSc3	pobřeží
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
leží	ležet	k5eAaImIp3nP	ležet
města	město	k1gNnPc1	město
Sljuďanka	Sljuďanka	k1gFnSc1	Sljuďanka
<g/>
,	,	kIx,	,
Bajkalsk	Bajkalsk	k1gInSc1	Bajkalsk
a	a	k8xC	a
Babuškin	Babuškin	k1gInSc1	Babuškin
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
přístavy	přístav	k1gInPc1	přístav
a	a	k8xC	a
obydlená	obydlený	k2eAgNnPc1d1	obydlené
místa	místo	k1gNnPc1	místo
Bajkal	Bajkal	k1gInSc1	Bajkal
<g/>
,	,	kIx,	,
Tanchoj	Tanchoj	k1gInSc1	Tanchoj
<g/>
,	,	kIx,	,
Vydrino	Vydrino	k1gNnSc1	Vydrino
<g/>
,	,	kIx,	,
Usť-Barguzin	Usť-Barguzin	k1gInSc1	Usť-Barguzin
<g/>
,	,	kIx,	,
Nižněangarsk	Nižněangarsk	k1gInSc1	Nižněangarsk
<g/>
,	,	kIx,	,
Chužir	Chužir	k1gInSc1	Chužir
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
jižním	jižní	k2eAgInSc6d1	jižní
a	a	k8xC	a
jihovýchodním	jihovýchodní	k2eAgInSc6d1	jihovýchodní
břehu	břeh	k1gInSc6	břeh
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vesnici	vesnice	k1gFnSc6	vesnice
Listvjanka	Listvjanka	k1gFnSc1	Listvjanka
je	být	k5eAaImIp3nS	být
limnologický	limnologický	k2eAgInSc4d1	limnologický
ústav	ústav	k1gInSc4	ústav
a	a	k8xC	a
ve	v	k7c6	v
vesnici	vesnice	k1gFnSc6	vesnice
Velké	velký	k2eAgInPc4d1	velký
Koty	kot	k1gInPc4	kot
hydrobiologická	hydrobiologický	k2eAgFnSc1d1	hydrobiologická
stanice	stanice	k1gFnSc1	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
nacházejí	nacházet	k5eAaImIp3nP	nacházet
turistické	turistický	k2eAgFnPc1d1	turistická
chaty	chata	k1gFnPc1	chata
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Bajkalu	Bajkal	k1gInSc2	Bajkal
je	být	k5eAaImIp3nS	být
rozvinutý	rozvinutý	k2eAgInSc1d1	rozvinutý
těžební	těžební	k2eAgInSc1d1	těžební
a	a	k8xC	a
papírenský	papírenský	k2eAgInSc1d1	papírenský
průmysl	průmysl	k1gInSc1	průmysl
<g/>
,	,	kIx,	,
výstavba	výstavba	k1gFnSc1	výstavba
lodí	loď	k1gFnPc2	loď
<g/>
,	,	kIx,	,
zpracovávání	zpracovávání	k1gNnSc1	zpracovávání
ryb	ryba	k1gFnPc2	ryba
a	a	k8xC	a
rybolov	rybolov	k1gInSc1	rybolov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Sport	sport	k1gInSc1	sport
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Dálkoví	dálkový	k2eAgMnPc1d1	dálkový
plavci	plavec	k1gMnPc1	plavec
na	na	k7c6	na
Bajkalu	Bajkal	k1gInSc6	Bajkal
====	====	k?	====
</s>
</p>
<p>
<s>
1990	[number]	k4	1990
–	–	k?	–
československý	československý	k2eAgMnSc1d1	československý
dálkový	dálkový	k2eAgMnSc1d1	dálkový
plavec	plavec	k1gMnSc1	plavec
Ján	Ján	k1gMnSc1	Ján
Novák	Novák	k1gMnSc1	Novák
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
stal	stát	k5eAaPmAgMnS	stát
prvním	první	k4xOgMnSc7	první
člověkem	člověk	k1gMnSc7	člověk
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
přeplaval	přeplavat	k5eAaPmAgMnS	přeplavat
Bajkal	Bajkal	k1gInSc4	Bajkal
</s>
</p>
<p>
<s>
====	====	k?	====
Zimní	zimní	k2eAgInPc1d1	zimní
přechody	přechod	k1gInPc1	přechod
jezera	jezero	k1gNnSc2	jezero
Bajkal	Bajkal	k1gInSc4	Bajkal
====	====	k?	====
</s>
</p>
<p>
<s>
Únor	únor	k1gInSc1	únor
<g/>
/	/	kIx~	/
<g/>
březen	březen	k1gInSc1	březen
2003	[number]	k4	2003
-	-	kIx~	-
Stefan	Stefan	k1gMnSc1	Stefan
Wackerhagen	Wackerhagen	k1gInSc1	Wackerhagen
a	a	k8xC	a
Stephan	Stephan	k1gMnSc1	Stephan
Reichelt	Reichelt	k1gMnSc1	Reichelt
z	z	k7c2	z
Německa	Německo	k1gNnSc2	Německo
uskutečnili	uskutečnit	k5eAaPmAgMnP	uskutečnit
první	první	k4xOgInSc4	první
přechod	přechod	k1gInSc4	přechod
jezera	jezero	k1gNnSc2	jezero
Bajkal	Bajkal	k1gInSc1	Bajkal
na	na	k7c4	na
délku	délka	k1gFnSc4	délka
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
přechod	přechod	k1gInSc1	přechod
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
unassisted	unassisted	k1gInSc1	unassisted
a	a	k8xC	a
přechod	přechod	k1gInSc1	přechod
trval	trvat	k5eAaImAgInS	trvat
32	[number]	k4	32
dnů	den	k1gInPc2	den
<g/>
.2007	.2007	k4	.2007
-	-	kIx~	-
Conrad	Conrada	k1gFnPc2	Conrada
Dickenson	Dickenson	k1gInSc1	Dickenson
<g/>
,	,	kIx,	,
Hilary	Hilara	k1gFnPc1	Hilara
Dickenson	Dickenson	k1gInSc1	Dickenson
<g/>
,	,	kIx,	,
Alistair	Alistair	k1gInSc1	Alistair
Guthrie	Guthrie	k1gFnSc1	Guthrie
and	and	k?	and
Antony	anton	k1gInPc1	anton
Baird	Baird	k1gInSc4	Baird
překonali	překonat	k5eAaPmAgMnP	překonat
délku	délka	k1gFnSc4	délka
jezera	jezero	k1gNnSc2	jezero
Bajkal	Bajkal	k1gInSc1	Bajkal
za	za	k7c4	za
21	[number]	k4	21
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Expedice	expedice	k1gFnSc1	expedice
s	s	k7c7	s
sebou	se	k3xPyFc7	se
měla	mít	k5eAaImAgFnS	mít
tažné	tažný	k2eAgInPc4d1	tažný
padáky	padák	k1gInPc4	padák
a	a	k8xC	a
použila	použít	k5eAaPmAgFnS	použít
je	být	k5eAaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jejich	jejich	k3xOp3gFnPc2	jejich
zpráv	zpráva	k1gFnPc2	zpráva
jim	on	k3xPp3gMnPc3	on
ale	ale	k8xC	ale
vhodný	vhodný	k2eAgInSc1d1	vhodný
vítr	vítr	k1gInSc1	vítr
foukal	foukat	k5eAaImAgInS	foukat
za	za	k7c4	za
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
2,5	[number]	k4	2,5
hodiny	hodina	k1gFnSc2	hodina
<g/>
.	.	kIx.	.
<g/>
Únor	únor	k1gInSc1	únor
<g/>
/	/	kIx~	/
<g/>
březen	březen	k1gInSc1	březen
2008	[number]	k4	2008
-	-	kIx~	-
Hardy	Hard	k1gInPc1	Hard
Brandstötter	Brandstöttrum	k1gNnPc2	Brandstöttrum
z	z	k7c2	z
Rakouska	Rakousko	k1gNnSc2	Rakousko
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
sólo	sólo	k2eAgInSc1d1	sólo
přechod	přechod	k1gInSc1	přechod
jezera	jezero	k1gNnSc2	jezero
Bajkal	Bajkal	k1gInSc1	Bajkal
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
přechodu	přechod	k1gInSc3	přechod
používal	používat	k5eAaImAgInS	používat
tažný	tažný	k2eAgInSc1d1	tažný
padák	padák	k1gInSc1	padák
<g/>
,	,	kIx,	,
kolik	kolik	k9	kolik
přesně	přesně	k6eAd1	přesně
dnů	den	k1gInPc2	den
na	na	k7c6	na
ledě	led	k1gInSc6	led
strávil	strávit	k5eAaPmAgMnS	strávit
<g/>
,	,	kIx,	,
neuvádí	uvádět	k5eNaImIp3nS	uvádět
<g/>
.	.	kIx.	.
<g/>
Únor	únor	k1gInSc1	únor
<g/>
/	/	kIx~	/
<g/>
březen	březen	k1gInSc1	březen
2010	[number]	k4	2010
-	-	kIx~	-
Václav	Václav	k1gMnSc1	Václav
Sůra	Sůra	k1gMnSc1	Sůra
a	a	k8xC	a
Pavel	Pavel	k1gMnSc1	Pavel
Blažek	Blažek	k1gMnSc1	Blažek
z	z	k7c2	z
Česka	Česko	k1gNnSc2	Česko
uskutečnili	uskutečnit	k5eAaPmAgMnP	uskutečnit
první	první	k4xOgInSc4	první
český	český	k2eAgInSc4d1	český
přechod	přechod	k1gInSc4	přechod
jezera	jezero	k1gNnSc2	jezero
Bajkal	Bajkal	k1gInSc1	Bajkal
(	(	kIx(	(
<g/>
Expedice	expedice	k1gFnSc1	expedice
Bergans	Bergans	k1gInSc4	Bergans
Bajkal	Bajkal	k1gInSc1	Bajkal
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přechod	přechod	k1gInSc1	přechod
trval	trvat	k5eAaImAgInS	trvat
24	[number]	k4	24
dnů	den	k1gInPc2	den
a	a	k8xC	a
6	[number]	k4	6
hodin	hodina	k1gFnPc2	hodina
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
druhým	druhý	k4xOgInSc7	druhý
nejrychlejším	rychlý	k2eAgInSc7d3	nejrychlejší
přechodem	přechod	k1gInSc7	přechod
jezera	jezero	k1gNnSc2	jezero
na	na	k7c6	na
světě	svět	k1gInSc6	svět
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
unassisted	unassisted	k1gInSc1	unassisted
<g/>
.	.	kIx.	.
<g/>
Březen	březen	k1gInSc1	březen
2010	[number]	k4	2010
-	-	kIx~	-
Ray	Ray	k1gMnSc1	Ray
Zahab	Zahab	k1gMnSc1	Zahab
a	a	k8xC	a
Kevin	Kevin	k1gMnSc1	Kevin
Vallely	Vallely	k1gMnSc1	Vallely
z	z	k7c2	z
Kanady	Kanada	k1gFnSc2	Kanada
uskutečnili	uskutečnit	k5eAaPmAgMnP	uskutečnit
rychlostní	rychlostní	k2eAgInSc4d1	rychlostní
přechod	přechod	k1gInSc4	přechod
jezera	jezero	k1gNnSc2	jezero
Baikal	Baikal	k1gMnSc1	Baikal
<g/>
.	.	kIx.	.
</s>
<s>
Expedice	expedice	k1gFnSc1	expedice
probíhala	probíhat	k5eAaImAgFnS	probíhat
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
unassisted	unassisted	k1gInSc4	unassisted
a	a	k8xC	a
s	s	k7c7	s
13	[number]	k4	13
dny	dno	k1gNnPc7	dno
a	a	k8xC	a
16	[number]	k4	16
hodinami	hodina	k1gFnPc7	hodina
strávenými	strávený	k2eAgInPc7d1	strávený
na	na	k7c6	na
ledě	led	k1gInSc6	led
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
nejrychlejší	rychlý	k2eAgFnSc7d3	nejrychlejší
expedicí	expedice	k1gFnSc7	expedice
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
kdy	kdy	k6eAd1	kdy
jezero	jezero	k1gNnSc1	jezero
přešla	přejít	k5eAaPmAgFnS	přejít
<g/>
.	.	kIx.	.
<g/>
Únor	únor	k1gInSc4	únor
2016	[number]	k4	2016
-	-	kIx~	-
David	David	k1gMnSc1	David
Šelicha	Šelich	k1gMnSc2	Šelich
a	a	k8xC	a
Filip	Filip	k1gMnSc1	Filip
Zloch	zloch	k1gMnSc1	zloch
z	z	k7c2	z
Česka	Česko	k1gNnSc2	Česko
uskutečnili	uskutečnit	k5eAaPmAgMnP	uskutečnit
první	první	k4xOgMnSc1	první
známý	známý	k1gMnSc1	známý
unassisted	unassisted	k1gMnSc1	unassisted
přechod	přechod	k1gInSc4	přechod
jezera	jezero	k1gNnSc2	jezero
Bajkal	Bajkal	k1gInSc1	Bajkal
ze	z	k7c2	z
Severu	sever	k1gInSc2	sever
na	na	k7c4	na
Jih	jih	k1gInSc4	jih
<g/>
.	.	kIx.	.
</s>
<s>
Přechod	přechod	k1gInSc1	přechod
přerušen	přerušen	k2eAgInSc1d1	přerušen
11	[number]	k4	11
<g/>
.	.	kIx.	.
den	den	k1gInSc4	den
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
ostrova	ostrov	k1gInSc2	ostrov
Olchon	Olchona	k1gFnPc2	Olchona
<g/>
,	,	kIx,	,
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
nebezpečného	bezpečný	k2eNgNnSc2d1	nebezpečné
tání	tání	k1gNnSc2	tání
jižního	jižní	k2eAgInSc2d1	jižní
úseku	úsek	k1gInSc2	úsek
plánované	plánovaný	k2eAgFnSc2d1	plánovaná
trasy	trasa	k1gFnSc2	trasa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Únor	únor	k1gInSc1	únor
2019	[number]	k4	2019
-	-	kIx~	-
Daniel	Daniel	k1gMnSc1	Daniel
Born	Born	k1gMnSc1	Born
a	a	k8xC	a
Ladislav	Ladislav	k1gMnSc1	Ladislav
Smrž	Smrž	k1gMnSc1	Smrž
z	z	k7c2	z
Česka	Česko	k1gNnSc2	Česko
uskutečnili	uskutečnit	k5eAaPmAgMnP	uskutečnit
přechod	přechod	k1gInSc4	přechod
jezera	jezero	k1gNnSc2	jezero
Bajkal	Bajkal	k1gInSc1	Bajkal
z	z	k7c2	z
jihu	jih	k1gInSc2	jih
na	na	k7c4	na
sever	sever	k1gInSc4	sever
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
unsupported	unsupported	k1gInSc4	unsupported
a	a	k8xC	a
unassisted	unassisted	k1gInSc4	unassisted
<g/>
.	.	kIx.	.
</s>
<s>
Přechod	přechod	k1gInSc1	přechod
trval	trvat	k5eAaImAgInS	trvat
16	[number]	k4	16
dní	den	k1gInPc2	den
a	a	k8xC	a
6	[number]	k4	6
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Born	Born	k1gMnSc1	Born
se	se	k3xPyFc4	se
tak	tak	k9	tak
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
22	[number]	k4	22
letech	léto	k1gNnPc6	léto
stal	stát	k5eAaPmAgMnS	stát
nejmladším	mladý	k2eAgMnSc7d3	nejmladší
člověkem	člověk	k1gMnSc7	člověk
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
přechod	přechod	k1gInSc4	přechod
absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ekologie	ekologie	k1gFnSc1	ekologie
==	==	k?	==
</s>
</p>
<p>
<s>
Jezero	jezero	k1gNnSc1	jezero
Bajkal	Bajkal	k1gInSc1	Bajkal
je	být	k5eAaImIp3nS	být
unikátním	unikátní	k2eAgInSc7d1	unikátní
ekologickým	ekologický	k2eAgInSc7d1	ekologický
systémem	systém	k1gInSc7	systém
<g/>
,	,	kIx,	,
chráněným	chráněný	k2eAgNnSc7d1	chráněné
na	na	k7c6	na
základě	základ	k1gInSc6	základ
federálního	federální	k2eAgInSc2d1	federální
zákona	zákon	k1gInSc2	zákon
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
"	"	kIx"	"
<g/>
O	o	k7c6	o
ochraně	ochrana	k1gFnSc6	ochrana
jezera	jezero	k1gNnSc2	jezero
Bajkal	Bajkal	k1gInSc4	Bajkal
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
tohoto	tento	k3xDgInSc2	tento
zákona	zákon	k1gInSc2	zákon
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
Bajkalském	bajkalský	k2eAgNnSc6d1	Bajkalské
přírodním	přírodní	k2eAgNnSc6d1	přírodní
teritoriu	teritorium	k1gNnSc6	teritorium
(	(	kIx(	(
<g/>
široká	široký	k2eAgFnSc1d1	široká
oblast	oblast	k1gFnSc1	oblast
okolo	okolo	k7c2	okolo
jezera	jezero	k1gNnSc2	jezero
<g/>
,	,	kIx,	,
sahající	sahající	k2eAgFnSc1d1	sahající
až	až	k9	až
k	k	k7c3	k
mongolským	mongolský	k2eAgFnPc3d1	mongolská
hranicím	hranice	k1gFnPc3	hranice
<g/>
)	)	kIx)	)
zaveden	zaveden	k2eAgInSc1d1	zaveden
speciální	speciální	k2eAgInSc1d1	speciální
režim	režim	k1gInSc1	režim
pro	pro	k7c4	pro
hospodářské	hospodářský	k2eAgFnPc4d1	hospodářská
a	a	k8xC	a
jiné	jiný	k2eAgFnPc4d1	jiná
činnosti	činnost	k1gFnPc4	činnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lidská	lidský	k2eAgFnSc1d1	lidská
činnost	činnost	k1gFnSc1	činnost
má	mít	k5eAaImIp3nS	mít
na	na	k7c4	na
ekosystém	ekosystém	k1gInSc4	ekosystém
jezera	jezero	k1gNnSc2	jezero
samozřejmě	samozřejmě	k6eAd1	samozřejmě
negativní	negativní	k2eAgInSc1d1	negativní
vliv	vliv	k1gInSc1	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
byl	být	k5eAaImAgInS	být
velkým	velký	k2eAgInSc7d1	velký
problémem	problém	k1gInSc7	problém
provoz	provoz	k1gInSc1	provoz
papírny	papírna	k1gFnSc2	papírna
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Bajkalsk	Bajkalsk	k1gInSc4	Bajkalsk
<g/>
,	,	kIx,	,
tu	tu	k6eAd1	tu
se	se	k3xPyFc4	se
ale	ale	k9	ale
již	již	k6eAd1	již
podařilo	podařit	k5eAaPmAgNnS	podařit
uzavřít	uzavřít	k5eAaPmF	uzavřít
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
však	však	k9	však
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
velkým	velký	k2eAgInSc7d1	velký
zdrojem	zdroj	k1gInSc7	zdroj
znečištění	znečištění	k1gNnSc2	znečištění
řeka	řeka	k1gFnSc1	řeka
Selenga	Selenga	k1gFnSc1	Selenga
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejčastějšímu	častý	k2eAgNnSc3d3	nejčastější
porušování	porušování	k1gNnSc3	porušování
zákonů	zákon	k1gInPc2	zákon
o	o	k7c6	o
ochraně	ochrana	k1gFnSc6	ochrana
přírody	příroda	k1gFnSc2	příroda
docházelo	docházet	k5eAaImAgNnS	docházet
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
na	na	k7c6	na
Bajkale	Bajkal	k1gInSc6	Bajkal
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
nelegálním	legální	k2eNgNnSc7d1	nelegální
kácením	kácení	k1gNnSc7	kácení
stromů	strom	k1gInPc2	strom
a	a	k8xC	a
odstraňováním	odstraňování	k1gNnSc7	odstraňování
křovin	křovina	k1gFnPc2	křovina
(	(	kIx(	(
<g/>
78,5	[number]	k4	78,5
%	%	kIx~	%
zjištěných	zjištěný	k2eAgInPc2d1	zjištěný
případů	případ	k1gInPc2	případ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
nezákonný	zákonný	k2eNgInSc4d1	nezákonný
rybolov	rybolov	k1gInSc4	rybolov
(	(	kIx(	(
<g/>
12,4	[number]	k4	12,4
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ničení	ničení	k1gNnSc4	ničení
a	a	k8xC	a
poškozování	poškozování	k1gNnSc4	poškozování
lesů	les	k1gInPc2	les
(	(	kIx(	(
<g/>
8	[number]	k4	8
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pytláctví	pytláctví	k1gNnSc4	pytláctví
<g/>
,	,	kIx,	,
znečišťování	znečišťování	k1gNnSc4	znečišťování
vod	voda	k1gFnPc2	voda
<g/>
,	,	kIx,	,
porušování	porušování	k1gNnSc1	porušování
veterinárních	veterinární	k2eAgNnPc2d1	veterinární
pravidel	pravidlo	k1gNnPc2	pravidlo
a	a	k8xC	a
pravidel	pravidlo	k1gNnPc2	pravidlo
boje	boj	k1gInPc4	boj
proti	proti	k7c3	proti
onemocnění	onemocnění	k1gNnSc3	onemocnění
rostlin	rostlina	k1gFnPc2	rostlina
(	(	kIx(	(
<g/>
0,9	[number]	k4	0,9
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
takovýchto	takovýto	k3xDgInPc2	takovýto
činů	čin	k1gInPc2	čin
bylo	být	k5eAaImAgNnS	být
zaregistrováno	zaregistrovat	k5eAaPmNgNnS	zaregistrovat
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
v	v	k7c6	v
předešlých	předešlý	k2eAgNnPc6d1	předešlé
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
v	v	k7c6	v
Burjatské	burjatský	k2eAgFnSc6d1	Burjatská
republice	republika	k1gFnSc6	republika
(	(	kIx(	(
<g/>
42,7	[number]	k4	42,7
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Hlavní	hlavní	k2eAgInPc1d1	hlavní
zdroje	zdroj	k1gInPc1	zdroj
znečištění	znečištění	k1gNnSc2	znečištění
===	===	k?	===
</s>
</p>
<p>
<s>
Řeka	řeka	k1gFnSc1	řeka
Selenga	Selenga	k1gFnSc1	Selenga
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgInSc7d3	veliký
přítokem	přítok	k1gInSc7	přítok
jezera	jezero	k1gNnSc2	jezero
<g/>
,	,	kIx,	,
ročně	ročně	k6eAd1	ročně
přinese	přinést	k5eAaPmIp3nS	přinést
30	[number]	k4	30
km	km	kA	km
<g/>
3	[number]	k4	3
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
představuje	představovat	k5eAaImIp3nS	představovat
polovinu	polovina	k1gFnSc4	polovina
všech	všecek	k3xTgInPc2	všecek
přítoků	přítok	k1gInPc2	přítok
do	do	k7c2	do
Bajkalu	Bajkal	k1gInSc2	Bajkal
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlavní	hlavní	k2eAgInPc1d1	hlavní
zdroje	zdroj	k1gInPc1	zdroj
znečištění	znečištění	k1gNnSc2	znečištění
Selengy	Seleng	k1gInPc4	Seleng
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
Mongolsku	Mongolsko	k1gNnSc6	Mongolsko
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
velkou	velký	k2eAgFnSc4d1	velká
roli	role	k1gFnSc4	role
zde	zde	k6eAd1	zde
hrají	hrát	k5eAaImIp3nP	hrát
přítoky	přítok	k1gInPc1	přítok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
povodí	povodí	k1gNnSc6	povodí
Selengy	Seleng	k1gInPc1	Seleng
totiž	totiž	k9	totiž
leží	ležet	k5eAaImIp3nP	ležet
například	například	k6eAd1	například
mongolské	mongolský	k2eAgNnSc4d1	mongolské
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Ulanbátar	Ulanbátara	k1gFnPc2	Ulanbátara
<g/>
,	,	kIx,	,
zlaté	zlatý	k2eAgInPc4d1	zlatý
doly	dol	k1gInPc4	dol
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Zaamar	Zaamar	k1gInSc4	Zaamar
či	či	k8xC	či
různé	různý	k2eAgInPc4d1	různý
těžební	těžební	k2eAgInPc4d1	těžební
podniky	podnik	k1gInPc4	podnik
na	na	k7c6	na
horním	horní	k2eAgInSc6d1	horní
toku	tok	k1gInSc6	tok
řek	řeka	k1gFnPc2	řeka
Orchon	Orchon	k1gInSc4	Orchon
gol	gol	k?	gol
a	a	k8xC	a
Charaa	Charaa	k1gMnSc1	Charaa
gol	gol	k?	gol
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
velkým	velký	k2eAgInSc7d1	velký
zdrojem	zdroj	k1gInSc7	zdroj
znečištění	znečištění	k1gNnSc2	znečištění
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
odpadní	odpadní	k2eAgFnSc2d1	odpadní
vody	voda	k1gFnSc2	voda
z	z	k7c2	z
města	město	k1gNnSc2	město
Darchan	Darchany	k1gInPc2	Darchany
<g/>
,	,	kIx,	,
velkého	velký	k2eAgNnSc2d1	velké
průmyslového	průmyslový	k2eAgNnSc2d1	průmyslové
centra	centrum	k1gNnSc2	centrum
v	v	k7c6	v
severním	severní	k2eAgNnSc6d1	severní
Mongolsku	Mongolsko	k1gNnSc6	Mongolsko
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
fungují	fungovat	k5eAaImIp3nP	fungovat
strojírenské	strojírenský	k2eAgInPc4d1	strojírenský
kombináty	kombinát	k1gInPc4	kombinát
<g/>
,	,	kIx,	,
továrna	továrna	k1gFnSc1	továrna
na	na	k7c4	na
zpracování	zpracování	k1gNnSc4	zpracování
kůží	kůže	k1gFnPc2	kůže
<g/>
,	,	kIx,	,
ocelárna	ocelárna	k1gFnSc1	ocelárna
nebo	nebo	k8xC	nebo
různé	různý	k2eAgInPc1d1	různý
potravinářské	potravinářský	k2eAgInPc1d1	potravinářský
závody	závod	k1gInPc1	závod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
Ruska	Rusko	k1gNnSc2	Rusko
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgMnSc7d3	veliký
znečišťovatelem	znečišťovatel	k1gMnSc7	znečišťovatel
Selengy	Seleng	k1gInPc1	Seleng
město	město	k1gNnSc1	město
Ulan-Ude	Ulan-Ud	k1gMnSc5	Ulan-Ud
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnPc1	jehož
čističky	čistička	k1gFnPc1	čistička
nejsou	být	k5eNaImIp3nP	být
schopny	schopen	k2eAgFnPc1d1	schopna
upravit	upravit	k5eAaPmF	upravit
odpadní	odpadní	k2eAgFnPc1d1	odpadní
vody	voda	k1gFnPc1	voda
z	z	k7c2	z
průmyslových	průmyslový	k2eAgInPc2d1	průmyslový
podniků	podnik	k1gInPc2	podnik
a	a	k8xC	a
domácností	domácnost	k1gFnPc2	domácnost
na	na	k7c4	na
požadovanou	požadovaný	k2eAgFnSc4d1	požadovaná
kvalitu	kvalita	k1gFnSc4	kvalita
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
většina	většina	k1gFnSc1	většina
čističek	čistička	k1gFnPc2	čistička
odpadních	odpadní	k2eAgFnPc2d1	odpadní
vod	voda	k1gFnPc2	voda
v	v	k7c6	v
povodí	povodí	k1gNnSc6	povodí
Bajkalu	Bajkal	k1gInSc2	Bajkal
v	v	k7c6	v
Burjatské	burjatský	k2eAgFnSc6d1	Burjatská
republice	republika	k1gFnSc6	republika
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
havarijním	havarijní	k2eAgInSc6d1	havarijní
stavu	stav	k1gInSc6	stav
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
v	v	k7c6	v
mnohých	mnohý	k2eAgNnPc6d1	mnohé
venkovských	venkovský	k2eAgNnPc6d1	venkovské
sídlech	sídlo	k1gNnPc6	sídlo
se	se	k3xPyFc4	se
vůbec	vůbec	k9	vůbec
žádná	žádný	k3yNgFnSc1	žádný
kanalizace	kanalizace	k1gFnSc1	kanalizace
ani	ani	k8xC	ani
čističky	čistička	k1gFnPc1	čistička
odpadních	odpadní	k2eAgFnPc2d1	odpadní
vod	voda	k1gFnPc2	voda
nenacházejí	nacházet	k5eNaImIp3nP	nacházet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
neřešeným	řešený	k2eNgInSc7d1	neřešený
problémem	problém	k1gInSc7	problém
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
únik	únik	k1gInSc1	únik
ropných	ropný	k2eAgInPc2d1	ropný
produktů	produkt	k1gInPc2	produkt
do	do	k7c2	do
řeky	řeka	k1gFnSc2	řeka
Selengy	Seleng	k1gInPc1	Seleng
v	v	k7c6	v
katastru	katastr	k1gInSc6	katastr
obce	obec	k1gFnSc2	obec
Stěklozavod	Stěklozavoda	k1gFnPc2	Stěklozavoda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
znečištění	znečištění	k1gNnSc4	znečištění
vod	voda	k1gFnPc2	voda
Bajkalu	Bajkal	k1gInSc2	Bajkal
mají	mít	k5eAaImIp3nP	mít
podíl	podíl	k1gInSc1	podíl
i	i	k8xC	i
hlavní	hlavní	k2eAgInPc1d1	hlavní
přítoky	přítok	k1gInPc1	přítok
Selengy	Seleng	k1gInPc1	Seleng
<g/>
,	,	kIx,	,
řeky	řeka	k1gFnPc1	řeka
Čikoj	Čikoj	k1gInSc1	Čikoj
a	a	k8xC	a
Chilok	Chilok	k1gInSc1	Chilok
<g/>
,	,	kIx,	,
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	se	k3xPyFc4	se
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
Zabajkalském	zabajkalský	k2eAgInSc6d1	zabajkalský
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vážné	vážný	k2eAgNnSc4d1	vážné
ohrožení	ohrožení	k1gNnSc4	ohrožení
ekosystému	ekosystém	k1gInSc2	ekosystém
Bajkalu	Bajkal	k1gInSc2	Bajkal
představuje	představovat	k5eAaImIp3nS	představovat
průmyslový	průmyslový	k2eAgInSc4d1	průmyslový
a	a	k8xC	a
komunální	komunální	k2eAgInSc4d1	komunální
odpad	odpad	k1gInSc4	odpad
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
vážného	vážný	k2eAgNnSc2d1	vážné
ohrožení	ohrožení	k1gNnSc2	ohrožení
průmyslovým	průmyslový	k2eAgInSc7d1	průmyslový
odpadem	odpad	k1gInSc7	odpad
je	být	k5eAaImIp3nS	být
Džidinský	Džidinský	k2eAgInSc1d1	Džidinský
wolframo-molybdenový	wolframoolybdenový	k2eAgInSc1d1	wolframo-molybdenový
kombinát	kombinát	k1gInSc1	kombinát
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
fungoval	fungovat	k5eAaImAgInS	fungovat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1934	[number]	k4	1934
<g/>
–	–	k?	–
<g/>
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
zamořil	zamořit	k5eAaPmAgInS	zamořit
asi	asi	k9	asi
třetinu	třetina	k1gFnSc4	třetina
území	území	k1gNnSc2	území
města	město	k1gNnSc2	město
Zakamensk	Zakamensk	k1gInSc1	Zakamensk
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
se	se	k3xPyFc4	se
odtud	odtud	k6eAd1	odtud
dostávají	dostávat	k5eAaImIp3nP	dostávat
do	do	k7c2	do
Bajkalu	Bajkal	k1gInSc2	Bajkal
vysoce	vysoce	k6eAd1	vysoce
nebezpečné	bezpečný	k2eNgFnPc1d1	nebezpečná
chemické	chemický	k2eAgFnPc1d1	chemická
látky	látka	k1gFnPc1	látka
<g/>
.	.	kIx.	.
</s>
<s>
Říčka	říčka	k1gFnSc1	říčka
Mondokul	Mondokula	k1gFnPc2	Mondokula
<g/>
,	,	kIx,	,
protékající	protékající	k2eAgFnSc4d1	protékající
Zakamenskem	Zakamensko	k1gNnSc7	Zakamensko
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nejzamořenějším	zamořený	k2eAgInSc7d3	zamořený
vodním	vodní	k2eAgInSc7d1	vodní
tokem	tok	k1gInSc7	tok
Bajkalského	bajkalský	k2eAgNnSc2d1	Bajkalské
přírodního	přírodní	k2eAgNnSc2d1	přírodní
teritoria	teritorium	k1gNnSc2	teritorium
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Bajkalská	bajkalský	k2eAgFnSc1d1	Bajkalská
papírna	papírna	k1gFnSc1	papírna
===	===	k?	===
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
byla	být	k5eAaImAgFnS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
Bajkalská	bajkalský	k2eAgFnSc1d1	Bajkalská
papírna	papírna	k1gFnSc1	papírna
(	(	kIx(	(
<g/>
Б	Б	k?	Б
ц	ц	k?	ц
к	к	k?	к
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
velkým	velký	k2eAgMnSc7d1	velký
znečišťovatelem	znečišťovatel	k1gMnSc7	znečišťovatel
okolního	okolní	k2eAgNnSc2d1	okolní
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Zplodiny	zplodina	k1gFnPc1	zplodina
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
továrny	továrna	k1gFnSc2	továrna
způsobovaly	způsobovat	k5eAaImAgInP	způsobovat
usychání	usychání	k1gNnSc4	usychání
okolních	okolní	k2eAgInPc2d1	okolní
lesů	les	k1gInPc2	les
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2008	[number]	k4	2008
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
továrně	továrna	k1gFnSc6	továrna
zaveden	zavést	k5eAaPmNgInS	zavést
systém	systém	k1gInSc1	systém
uzavřené	uzavřený	k2eAgFnSc2d1	uzavřená
cirkulace	cirkulace	k1gFnSc2	cirkulace
vod	voda	k1gFnPc2	voda
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
zamezeno	zamezen	k2eAgNnSc1d1	zamezeno
úniku	únik	k1gInSc2	únik
nečistot	nečistota	k1gFnPc2	nečistota
do	do	k7c2	do
jezera	jezero	k1gNnSc2	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
systém	systém	k1gInSc1	systém
se	se	k3xPyFc4	se
však	však	k9	však
záhy	záhy	k6eAd1	záhy
ukázal	ukázat	k5eAaPmAgInS	ukázat
jako	jako	k9	jako
nefunkční	funkční	k2eNgFnSc4d1	nefunkční
a	a	k8xC	a
papírnu	papírna	k1gFnSc4	papírna
bylo	být	k5eAaImAgNnS	být
nutno	nutno	k6eAd1	nutno
již	již	k6eAd1	již
za	za	k7c4	za
měsíc	měsíc	k1gInSc4	měsíc
odstavit	odstavit	k5eAaPmF	odstavit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
13	[number]	k4	13
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2010	[number]	k4	2010
vydala	vydat	k5eAaPmAgFnS	vydat
ruská	ruský	k2eAgFnSc1d1	ruská
vláda	vláda	k1gFnSc1	vláda
nařízení	nařízení	k1gNnSc2	nařízení
o	o	k7c6	o
okamžitém	okamžitý	k2eAgInSc6d1	okamžitý
zákazu	zákaz	k1gInSc6	zákaz
výroby	výroba	k1gFnSc2	výroba
celulózy	celulóza	k1gFnSc2	celulóza
<g/>
,	,	kIx,	,
papíru	papír	k1gInSc2	papír
a	a	k8xC	a
kartonu	karton	k1gInSc2	karton
bez	bez	k7c2	bez
uzavřeného	uzavřený	k2eAgInSc2d1	uzavřený
systému	systém	k1gInSc2	systém
provozních	provozní	k2eAgFnPc2d1	provozní
vod	voda	k1gFnPc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
nařízení	nařízení	k1gNnSc1	nařízení
také	také	k9	také
nově	nově	k6eAd1	nově
upravovalo	upravovat	k5eAaImAgNnS	upravovat
způsob	způsob	k1gInSc4	způsob
skladování	skladování	k1gNnSc2	skladování
<g/>
,	,	kIx,	,
ukládání	ukládání	k1gNnSc1	ukládání
a	a	k8xC	a
spalování	spalování	k1gNnSc1	spalování
odpadu	odpad	k1gInSc2	odpad
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
jezera	jezero	k1gNnSc2	jezero
Bajkal	Bajkal	k1gInSc1	Bajkal
<g/>
,	,	kIx,	,
zapsaného	zapsaný	k2eAgMnSc4d1	zapsaný
na	na	k7c4	na
seznam	seznam	k1gInSc4	seznam
světového	světový	k2eAgNnSc2d1	světové
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Navzdory	navzdory	k7c3	navzdory
četným	četný	k2eAgInPc3d1	četný
protestům	protest	k1gInPc3	protest
aktivistů	aktivista	k1gMnPc2	aktivista
začala	začít	k5eAaPmAgFnS	začít
papírna	papírna	k1gFnSc1	papírna
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2010	[number]	k4	2010
v	v	k7c6	v
průmyslovém	průmyslový	k2eAgNnSc6d1	průmyslové
měřítku	měřítko	k1gNnSc6	měřítko
opět	opět	k6eAd1	opět
produkovat	produkovat	k5eAaImF	produkovat
bělenou	bělený	k2eAgFnSc4d1	bělená
celulózu	celulóza	k1gFnSc4	celulóza
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vzorky	vzorek	k1gInPc1	vzorek
odebrané	odebraný	k2eAgInPc1d1	odebraný
výzkumnou	výzkumný	k2eAgFnSc7d1	výzkumná
ponorkou	ponorka	k1gFnSc7	ponorka
Mir	Mira	k1gFnPc2	Mira
však	však	k9	však
ukázaly	ukázat	k5eAaPmAgInP	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
voda	voda	k1gFnSc1	voda
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
závodu	závod	k1gInSc2	závod
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
vysokou	vysoký	k2eAgFnSc4d1	vysoká
koncentraci	koncentrace	k1gFnSc4	koncentrace
dioxinů	dioxin	k1gInPc2	dioxin
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
nebezpečné	bezpečný	k2eNgFnPc1d1	nebezpečná
pro	pro	k7c4	pro
jakékoli	jakýkoli	k3yIgInPc4	jakýkoli
živé	živý	k2eAgInPc4d1	živý
organismy	organismus	k1gInPc4	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Znečištění	znečištěný	k2eAgMnPc1d1	znečištěný
dioxiny	dioxin	k1gInPc4	dioxin
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
papírny	papírna	k1gFnSc2	papírna
bylo	být	k5eAaImAgNnS	být
asi	asi	k9	asi
40	[number]	k4	40
–	–	k?	–
50	[number]	k4	50
krát	krát	k6eAd1	krát
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
ve	v	k7c6	v
středních	střední	k2eAgFnPc6d1	střední
částech	část	k1gFnPc6	část
jezera	jezero	k1gNnSc2	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Vzorky	vzorek	k1gInPc1	vzorek
byly	být	k5eAaImAgInP	být
odebírány	odebírat	k5eAaImNgInP	odebírat
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
hloubkách	hloubka	k1gFnPc6	hloubka
a	a	k8xC	a
v	v	k7c6	v
absolutní	absolutní	k2eAgFnSc3d1	absolutní
většině	většina	k1gFnSc3	většina
případů	případ	k1gInPc2	případ
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
překročení	překročení	k1gNnSc3	překročení
limitů	limit	k1gInPc2	limit
pro	pro	k7c4	pro
dioxiny	dioxin	k1gInPc4	dioxin
3-8	[number]	k4	3-8
krát	krát	k6eAd1	krát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2013	[number]	k4	2013
přijala	přijmout	k5eAaPmAgFnS	přijmout
vláda	vláda	k1gFnSc1	vláda
Ruské	ruský	k2eAgFnSc2d1	ruská
federace	federace	k1gFnSc2	federace
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
o	o	k7c4	o
uzavření	uzavření	k1gNnSc4	uzavření
Bajkalské	bajkalský	k2eAgFnSc2d1	Bajkalská
papírny	papírna	k1gFnSc2	papírna
<g/>
,	,	kIx,	,
v	v	k7c6	v
září	září	k1gNnSc6	září
2013	[number]	k4	2013
byl	být	k5eAaImAgInS	být
provoz	provoz	k1gInSc1	provoz
ukončen	ukončen	k2eAgInSc1d1	ukončen
a	a	k8xC	a
zaměstnanci	zaměstnanec	k1gMnPc1	zaměstnanec
podniku	podnik	k1gInSc2	podnik
byli	být	k5eAaImAgMnP	být
propuštěni	propustit	k5eAaPmNgMnP	propustit
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
se	se	k3xPyFc4	se
během	během	k7c2	během
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
ještě	ještě	k6eAd1	ještě
objevovaly	objevovat	k5eAaImAgFnP	objevovat
zprávy	zpráva	k1gFnPc1	zpráva
o	o	k7c6	o
možném	možný	k2eAgNnSc6d1	možné
znovuotevření	znovuotevření	k1gNnSc6	znovuotevření
továrny	továrna	k1gFnSc2	továrna
<g/>
,	,	kIx,	,
28	[number]	k4	28
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2013	[number]	k4	2013
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
ruská	ruský	k2eAgFnSc1d1	ruská
vláda	vláda	k1gFnSc1	vláda
o	o	k7c6	o
likvidaci	likvidace	k1gFnSc6	likvidace
zařízení	zařízení	k1gNnSc2	zařízení
papírny	papírna	k1gFnSc2	papírna
a	a	k8xC	a
ekologických	ekologický	k2eAgFnPc2d1	ekologická
škod	škoda	k1gFnPc2	škoda
s	s	k7c7	s
ní	on	k3xPp3gFnSc6	on
souvisejících	související	k2eAgInPc6d1	související
a	a	k8xC	a
o	o	k7c6	o
zřízení	zřízení	k1gNnSc6	zřízení
ekologického	ekologický	k2eAgNnSc2d1	ekologické
centra	centrum	k1gNnSc2	centrum
Chráněná	chráněný	k2eAgFnSc1d1	chráněná
území	území	k1gNnSc6	území
Ruska	Rusko	k1gNnSc2	Rusko
(	(	kIx(	(
<g/>
З	З	k?	З
Р	Р	k?	Р
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Irkutská	irkutský	k2eAgFnSc1d1	Irkutská
přehradní	přehradní	k2eAgFnSc1d1	přehradní
nádrž	nádrž	k1gFnSc1	nádrž
===	===	k?	===
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1956	[number]	k4	1956
je	být	k5eAaImIp3nS	být
jezero	jezero	k1gNnSc1	jezero
využíváno	využívat	k5eAaImNgNnS	využívat
v	v	k7c6	v
energetice	energetika	k1gFnSc6	energetika
–	–	k?	–
stalo	stát	k5eAaPmAgNnS	stát
se	s	k7c7	s
součástí	součást	k1gFnSc7	součást
Irkutské	irkutský	k2eAgFnSc2d1	Irkutská
přehradní	přehradní	k2eAgFnSc2d1	přehradní
nádrže	nádrž	k1gFnSc2	nádrž
<g/>
,	,	kIx,	,
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
čehož	což	k3yQnSc2	což
stoupla	stoupnout	k5eAaPmAgFnS	stoupnout
vodní	vodní	k2eAgFnSc1d1	vodní
hladina	hladina	k1gFnSc1	hladina
Bajkalu	Bajkal	k1gInSc2	Bajkal
téměř	téměř	k6eAd1	téměř
o	o	k7c4	o
1	[number]	k4	1
metr	metr	k1gInSc4	metr
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
přehrady	přehrada	k1gFnSc2	přehrada
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
zatím	zatím	k6eAd1	zatím
největší	veliký	k2eAgInSc4d3	veliký
lidský	lidský	k2eAgInSc4d1	lidský
zásah	zásah	k1gInSc4	zásah
do	do	k7c2	do
ekosystému	ekosystém	k1gInSc2	ekosystém
jezera	jezero	k1gNnSc2	jezero
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
výstavby	výstavba	k1gFnSc2	výstavba
dokonce	dokonce	k9	dokonce
existoval	existovat	k5eAaImAgInS	existovat
plán	plán	k1gInSc1	plán
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterého	který	k3yRgInSc2	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
urychlilo	urychlit	k5eAaPmAgNnS	urychlit
napouštění	napouštění	k1gNnSc1	napouštění
přehrady	přehrada	k1gFnSc2	přehrada
za	za	k7c4	za
cenu	cena	k1gFnSc4	cena
dočasného	dočasný	k2eAgNnSc2d1	dočasné
snížení	snížení	k1gNnSc2	snížení
hladiny	hladina	k1gFnSc2	hladina
jezera	jezero	k1gNnSc2	jezero
o	o	k7c4	o
5	[number]	k4	5
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
tlakem	tlak	k1gInSc7	tlak
veřejnosti	veřejnost	k1gFnSc2	veřejnost
bylo	být	k5eAaImAgNnS	být
ovšem	ovšem	k9	ovšem
od	od	k7c2	od
tohoto	tento	k3xDgInSc2	tento
nápadu	nápad	k1gInSc2	nápad
upuštěno	upustit	k5eAaPmNgNnS	upustit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1959	[number]	k4	1959
<g/>
–	–	k?	–
<g/>
1962	[number]	k4	1962
stoupla	stoupnout	k5eAaPmAgFnS	stoupnout
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
dokončením	dokončení	k1gNnSc7	dokončení
elektrárny	elektrárna	k1gFnSc2	elektrárna
hladina	hladina	k1gFnSc1	hladina
jezera	jezero	k1gNnSc2	jezero
o	o	k7c6	o
1,2	[number]	k4	1,2
metru	metro	k1gNnSc6	metro
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
umělým	umělý	k2eAgInPc3d1	umělý
výkyvům	výkyv	k1gInPc3	výkyv
výšky	výška	k1gFnSc2	výška
hladiny	hladina	k1gFnSc2	hladina
až	až	k9	až
1,32	[number]	k4	1,32
m	m	kA	m
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vše	všechen	k3xTgNnSc1	všechen
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
bylo	být	k5eAaImAgNnS	být
zatopeno	zatopen	k2eAgNnSc1d1	zatopeno
asi	asi	k9	asi
500	[number]	k4	500
km	km	kA	km
<g/>
2	[number]	k4	2
souše	souš	k1gFnSc2	souš
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
zničení	zničení	k1gNnSc3	zničení
řady	řada	k1gFnSc2	řada
pobřežních	pobřežní	k2eAgInPc2d1	pobřežní
lesů	les	k1gInPc2	les
a	a	k8xC	a
jiných	jiný	k2eAgNnPc2d1	jiné
rostlinných	rostlinný	k2eAgNnPc2d1	rostlinné
společenstev	společenstvo	k1gNnPc2	společenstvo
<g/>
,	,	kIx,	,
nadto	nadto	k6eAd1	nadto
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
zvýšení	zvýšení	k1gNnSc4	zvýšení
hladiny	hladina	k1gFnSc2	hladina
prudkou	prudký	k2eAgFnSc4d1	prudká
erozi	eroze	k1gFnSc4	eroze
prakticky	prakticky	k6eAd1	prakticky
po	po	k7c6	po
celém	celý	k2eAgNnSc6d1	celé
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
břehy	břeh	k1gInPc1	břeh
ustoupily	ustoupit	k5eAaPmAgInP	ustoupit
o	o	k7c4	o
desítky	desítka	k1gFnPc4	desítka
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Jarki	Jarki	k1gNnSc2	Jarki
šlo	jít	k5eAaImAgNnS	jít
dokonce	dokonce	k9	dokonce
o	o	k7c4	o
100-350	[number]	k4	100-350
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
ostrov	ostrov	k1gInSc1	ostrov
samotný	samotný	k2eAgInSc1d1	samotný
byl	být	k5eAaImAgInS	být
vodou	voda	k1gFnSc7	voda
rozdělen	rozdělit	k5eAaPmNgMnS	rozdělit
na	na	k7c4	na
několik	několik	k4yIc4	několik
částí	část	k1gFnPc2	část
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
však	však	k9	však
výčet	výčet	k1gInSc1	výčet
negativních	negativní	k2eAgInPc2d1	negativní
důsledků	důsledek	k1gInPc2	důsledek
navýšení	navýšení	k1gNnSc4	navýšení
hladiny	hladina	k1gFnSc2	hladina
jezera	jezero	k1gNnSc2	jezero
nekončí	končit	k5eNaImIp3nS	končit
<g/>
:	:	kIx,	:
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
odplavování	odplavování	k1gNnSc3	odplavování
a	a	k8xC	a
zmenšování	zmenšování	k1gNnSc3	zmenšování
pláží	pláž	k1gFnPc2	pláž
<g/>
,	,	kIx,	,
rozvoji	rozvoj	k1gInPc7	rozvoj
větrné	větrný	k2eAgFnSc2d1	větrná
eroze	eroze	k1gFnSc2	eroze
<g/>
,	,	kIx,	,
při	při	k7c6	při
snížení	snížení	k1gNnSc6	snížení
stavu	stav	k1gInSc6	stav
vody	voda	k1gFnSc2	voda
vznikaly	vznikat	k5eAaImAgInP	vznikat
mělké	mělký	k2eAgInPc1d1	mělký
zálivy	záliv	k1gInPc1	záliv
špinavé	špinavý	k2eAgFnSc2d1	špinavá
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
degradovala	degradovat	k5eAaBmAgFnS	degradovat
místa	místo	k1gNnPc1	místo
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
ryb	ryba	k1gFnPc2	ryba
a	a	k8xC	a
ptačí	ptačí	k2eAgNnPc4d1	ptačí
hnízdiště	hnízdiště	k1gNnPc4	hnízdiště
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
výše	výše	k1gFnSc2	výše
uvedených	uvedený	k2eAgInPc2d1	uvedený
důvodů	důvod	k1gInPc2	důvod
bylo	být	k5eAaImAgNnS	být
iniciováno	iniciován	k2eAgNnSc1d1	iniciováno
nařízení	nařízení	k1gNnSc1	nařízení
vlády	vláda	k1gFnSc2	vláda
Ruské	ruský	k2eAgFnSc2d1	ruská
federace	federace	k1gFnSc2	federace
č.	č.	k?	č.
234	[number]	k4	234
z	z	k7c2	z
26	[number]	k4	26
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
stanovuje	stanovovat	k5eAaImIp3nS	stanovovat
maximální	maximální	k2eAgNnSc4d1	maximální
rozmezí	rozmezí	k1gNnSc4	rozmezí
hladiny	hladina	k1gFnSc2	hladina
Bajkalu	Bajkal	k1gInSc2	Bajkal
od	od	k7c2	od
456	[number]	k4	456
do	do	k7c2	do
457	[number]	k4	457
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
</s>
</p>
<p>
<s>
===	===	k?	===
Ropovod	ropovod	k1gInSc1	ropovod
Východní	východní	k2eAgFnSc1d1	východní
Sibiř	Sibiř	k1gFnSc1	Sibiř
–	–	k?	–
Tichý	tichý	k2eAgInSc1d1	tichý
oceán	oceán	k1gInSc1	oceán
===	===	k?	===
</s>
</p>
<p>
<s>
Firma	firma	k1gFnSc1	firma
Transněft	Transněfta	k1gFnPc2	Transněfta
realizovala	realizovat	k5eAaBmAgFnS	realizovat
stavbu	stavba	k1gFnSc4	stavba
ropovodu	ropovod	k1gInSc2	ropovod
Východní	východní	k2eAgFnSc1d1	východní
Sibiř	Sibiř	k1gFnSc1	Sibiř
–	–	k?	–
Tichý	tichý	k2eAgInSc1d1	tichý
oceán	oceán	k1gInSc1	oceán
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vede	vést	k5eAaImIp3nS	vést
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
Bajkalu	Bajkal	k1gInSc2	Bajkal
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ropovod	ropovod	k1gInSc1	ropovod
povede	povést	k5eAaPmIp3nS	povést
v	v	k7c6	v
těsné	těsný	k2eAgFnSc6d1	těsná
blízkosti	blízkost	k1gFnSc6	blízkost
jezera	jezero	k1gNnSc2	jezero
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
by	by	kYmCp3nS	by
však	však	k9	však
znamenalo	znamenat	k5eAaImAgNnS	znamenat
v	v	k7c6	v
případě	případ	k1gInSc6	případ
potenciální	potenciální	k2eAgFnSc2d1	potenciální
havárie	havárie	k1gFnSc2	havárie
ekologickou	ekologický	k2eAgFnSc4d1	ekologická
katastrofu	katastrofa	k1gFnSc4	katastrofa
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
plány	plán	k1gInPc1	plán
pochopitelně	pochopitelně	k6eAd1	pochopitelně
vyvolaly	vyvolat	k5eAaPmAgInP	vyvolat
velkou	velký	k2eAgFnSc4d1	velká
vlnu	vlna	k1gFnSc4	vlna
odporu	odpor	k1gInSc2	odpor
a	a	k8xC	a
protestů	protest	k1gInPc2	protest
<g/>
,	,	kIx,	,
při	při	k7c6	při
největším	veliký	k2eAgMnSc6d3	veliký
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
se	se	k3xPyFc4	se
sešel	sejít	k5eAaPmAgMnS	sejít
18	[number]	k4	18
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2006	[number]	k4	2006
v	v	k7c6	v
Irkutsku	Irkutsk	k1gInSc6	Irkutsk
mnohatisícový	mnohatisícový	k2eAgInSc1d1	mnohatisícový
dav	dav	k1gInSc1	dav
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jednání	jednání	k1gNnSc6	jednání
se	s	k7c7	s
sibiřskými	sibiřský	k2eAgMnPc7d1	sibiřský
gubernátory	gubernátor	k1gMnPc7	gubernátor
26	[number]	k4	26
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2006	[number]	k4	2006
v	v	k7c6	v
Tomsku	Tomsek	k1gInSc6	Tomsek
oznámil	oznámit	k5eAaPmAgInS	oznámit
Vladimir	Vladimir	k1gInSc1	Vladimir
Putin	putin	k2eAgInSc1d1	putin
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
projekt	projekt	k1gInSc4	projekt
přepracovat	přepracovat	k5eAaPmF	přepracovat
a	a	k8xC	a
nevést	vést	k5eNaImF	vést
ropovod	ropovod	k1gInSc4	ropovod
blíže	blízce	k6eAd2	blízce
než	než	k8xS	než
40	[number]	k4	40
kilometrů	kilometr	k1gInPc2	kilometr
od	od	k7c2	od
severního	severní	k2eAgInSc2d1	severní
břehu	břeh	k1gInSc2	břeh
jezera	jezero	k1gNnSc2	jezero
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
Transněft	Transněft	k2eAgInSc4d1	Transněft
projekt	projekt	k1gInSc4	projekt
zásadně	zásadně	k6eAd1	zásadně
pozměnil	pozměnit	k5eAaPmAgMnS	pozměnit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ropovod	ropovod	k1gInSc1	ropovod
nevedl	vést	k5eNaImAgInS	vést
blíže	blízce	k6eAd2	blízce
než	než	k8xS	než
350-400	[number]	k4	350-400
kilometrů	kilometr	k1gInPc2	kilometr
od	od	k7c2	od
břehů	břeh	k1gInPc2	břeh
Bajkalu	Bajkal	k1gInSc2	Bajkal
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Pytláctví	pytláctví	k1gNnSc2	pytláctví
===	===	k?	===
</s>
</p>
<p>
<s>
Dalším	další	k2eAgInSc7d1	další
problémem	problém	k1gInSc7	problém
ohrožujícím	ohrožující	k2eAgInSc7d1	ohrožující
faunu	fauna	k1gFnSc4	fauna
i	i	k8xC	i
floru	flora	k1gFnSc4	flora
jezera	jezero	k1gNnSc2	jezero
je	být	k5eAaImIp3nS	být
pytláctví	pytláctví	k1gNnSc4	pytláctví
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
objektů	objekt	k1gInPc2	objekt
nezákonného	zákonný	k2eNgInSc2d1	nezákonný
lovu	lov	k1gInSc2	lov
je	být	k5eAaImIp3nS	být
tuleň	tuleň	k1gMnSc1	tuleň
bajkalský	bajkalský	k2eAgMnSc1d1	bajkalský
(	(	kIx(	(
<g/>
něrpa	něrp	k1gMnSc4	něrp
<g/>
)	)	kIx)	)
a	a	k8xC	a
zejména	zejména	k9	zejména
jeho	jeho	k3xOp3gNnPc4	jeho
mláďata	mládě	k1gNnPc4	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	k9	aby
problematika	problematika	k1gFnSc1	problematika
úbytku	úbytek	k1gInSc2	úbytek
populace	populace	k1gFnSc2	populace
tuleně	tuleň	k1gMnSc2	tuleň
bajkalského	bajkalský	k2eAgMnSc2d1	bajkalský
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
hlouběji	hluboko	k6eAd2	hluboko
do	do	k7c2	do
obecného	obecný	k2eAgNnSc2d1	obecné
povědomí	povědomí	k1gNnSc2	povědomí
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
zaveden	zavést	k5eAaPmNgMnS	zavést
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
Deň	Deň	k1gFnSc1	Deň
něrpjonka	něrpjonka	k1gFnSc1	něrpjonka
(	(	kIx(	(
<g/>
Den	den	k1gInSc1	den
tuleního	tulení	k2eAgNnSc2d1	tulení
mláděte	mládě	k1gNnSc2	mládě
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Problémem	problém	k1gInSc7	problém
je	být	k5eAaImIp3nS	být
také	také	k9	také
nelegální	legální	k2eNgInSc4d1	nelegální
lov	lov	k1gInSc4	lov
omula	omulo	k1gNnSc2	omulo
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
až	až	k9	až
polovinu	polovina	k1gFnSc4	polovina
úlovků	úlovek	k1gInPc2	úlovek
této	tento	k3xDgFnSc2	tento
ryby	ryba	k1gFnSc2	ryba
mají	mít	k5eAaImIp3nP	mít
na	na	k7c6	na
svědomí	svědomí	k1gNnSc6	svědomí
pytláci	pytlák	k1gMnPc1	pytlák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Organizace	organizace	k1gFnSc2	organizace
<g/>
,	,	kIx,	,
zabývající	zabývající	k2eAgFnSc2d1	zabývající
se	se	k3xPyFc4	se
ochranou	ochrana	k1gFnSc7	ochrana
Bajkalu	Bajkal	k1gInSc2	Bajkal
===	===	k?	===
</s>
</p>
<p>
<s>
Fond	fond	k1gInSc1	fond
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
při	při	k7c6	při
ochraně	ochrana	k1gFnSc6	ochrana
Bajkalu	Bajkal	k1gInSc2	Bajkal
</s>
</p>
<p>
<s>
Bajkalské	bajkalský	k2eAgNnSc1d1	Bajkalské
ekologické	ekologický	k2eAgNnSc1d1	ekologické
hnutí	hnutí	k1gNnSc1	hnutí
</s>
</p>
<p>
<s>
Světový	světový	k2eAgInSc1d1	světový
fond	fond	k1gInSc1	fond
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
přírody	příroda	k1gFnSc2	příroda
</s>
</p>
<p>
<s>
Ruský	ruský	k2eAgMnSc1d1	ruský
Greenpeace	Greenpeace	k1gFnSc2	Greenpeace
</s>
</p>
<p>
<s>
Ekologický	ekologický	k2eAgInSc1d1	ekologický
fond	fond	k1gInSc1	fond
"	"	kIx"	"
<g/>
Čistý	čistý	k2eAgInSc1d1	čistý
Bajkal	Bajkal	k1gInSc1	Bajkal
<g/>
"	"	kIx"	"
Nezisková	ziskový	k2eNgFnSc1d1	nezisková
organizace	organizace	k1gFnSc1	organizace
"	"	kIx"	"
<g/>
Společně	společně	k6eAd1	společně
za	za	k7c4	za
obranu	obrana	k1gFnSc4	obrana
Bajkalu	Bajkal	k1gInSc2	Bajkal
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
==	==	k?	==
Turismus	turismus	k1gInSc4	turismus
==	==	k?	==
</s>
</p>
<p>
<s>
Nejčastějším	častý	k2eAgInSc7d3	nejčastější
výchozím	výchozí	k2eAgInSc7d1	výchozí
bodem	bod	k1gInSc7	bod
pro	pro	k7c4	pro
cestu	cesta	k1gFnSc4	cesta
na	na	k7c4	na
Bajkal	Bajkal	k1gInSc4	Bajkal
jsou	být	k5eAaImIp3nP	být
města	město	k1gNnSc2	město
Irkutsk	Irkutsk	k1gInSc1	Irkutsk
a	a	k8xC	a
Ulan-Ude	Ulan-Ud	k1gInSc5	Ulan-Ud
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yRgFnPc2	který
k	k	k7c3	k
jezeru	jezero	k1gNnSc3	jezero
vyjíždí	vyjíždět	k5eAaImIp3nP	vyjíždět
četné	četný	k2eAgInPc1d1	četný
maršrutky	maršrutek	k1gInPc1	maršrutek
i	i	k8xC	i
vlaky	vlak	k1gInPc1	vlak
<g/>
.	.	kIx.	.
</s>
<s>
Pokochat	pokochat	k5eAaPmF	pokochat
se	se	k3xPyFc4	se
pohledem	pohled	k1gInSc7	pohled
na	na	k7c4	na
jezero	jezero	k1gNnSc4	jezero
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
i	i	k9	i
přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
vlaku	vlak	k1gInSc2	vlak
při	při	k7c6	při
cestě	cesta	k1gFnSc6	cesta
po	po	k7c6	po
Transsibiřské	transsibiřský	k2eAgFnSc6d1	Transsibiřská
magistrále	magistrála	k1gFnSc6	magistrála
úsekem	úsek	k1gInSc7	úsek
Irkutsk	Irkutsk	k1gInSc1	Irkutsk
-	-	kIx~	-
Ulan-Ude	Ulan-Ud	k1gInSc5	Ulan-Ud
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
65	[number]	k4	65
km	km	kA	km
od	od	k7c2	od
Irkutsku	Irkutsk	k1gInSc2	Irkutsk
u	u	k7c2	u
výtoku	výtok	k1gInSc2	výtok
Angary	Angara	k1gFnSc2	Angara
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
vesnice	vesnice	k1gFnSc1	vesnice
Listvjanka	Listvjanka	k1gFnSc1	Listvjanka
–	–	k?	–
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejpopulárnějších	populární	k2eAgNnPc2d3	nejpopulárnější
turistických	turistický	k2eAgNnPc2d1	turistické
míst	místo	k1gNnPc2	místo
Bajkalu	Bajkal	k1gInSc2	Bajkal
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Irkutsku	Irkutsk	k1gInSc2	Irkutsk
se	se	k3xPyFc4	se
sem	sem	k6eAd1	sem
lze	lze	k6eAd1	lze
dostat	dostat	k5eAaPmF	dostat
asi	asi	k9	asi
za	za	k7c4	za
hodinu	hodina	k1gFnSc4	hodina
autobusem	autobus	k1gInSc7	autobus
či	či	k8xC	či
parníkem	parník	k1gInSc7	parník
(	(	kIx(	(
<g/>
po	po	k7c6	po
Angaře	Angara	k1gFnSc6	Angara
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Listvjanka	Listvjanka	k1gFnSc1	Listvjanka
nabízí	nabízet	k5eAaImIp3nS	nabízet
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
organizovaných	organizovaný	k2eAgInPc2d1	organizovaný
výletů	výlet	k1gInPc2	výlet
a	a	k8xC	a
jiných	jiný	k2eAgFnPc2d1	jiná
aktivit	aktivita	k1gFnPc2	aktivita
<g/>
,	,	kIx,	,
právě	právě	k6eAd1	právě
odtud	odtud	k6eAd1	odtud
je	být	k5eAaImIp3nS	být
odpravována	odpravován	k2eAgFnSc1d1	odpravován
většina	většina	k1gFnSc1	většina
vyhlídkových	vyhlídkový	k2eAgFnPc2d1	vyhlídková
plaveb	plavba	k1gFnPc2	plavba
po	po	k7c6	po
jezeře	jezero	k1gNnSc6	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
populární	populární	k2eAgFnPc1d1	populární
jsou	být	k5eAaImIp3nP	být
plavby	plavba	k1gFnPc1	plavba
na	na	k7c4	na
ostrov	ostrov	k1gInSc4	ostrov
Olchon	Olchona	k1gFnPc2	Olchona
či	či	k8xC	či
na	na	k7c4	na
poloostrov	poloostrov	k1gInSc4	poloostrov
Svatý	svatý	k2eAgInSc4d1	svatý
nos	nos	k1gInSc4	nos
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
Bajkalu	Bajkal	k1gInSc2	Bajkal
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
města	město	k1gNnSc2	město
Sljuďanka	Sljuďanka	k1gFnSc1	Sljuďanka
a	a	k8xC	a
Bajkalsk	Bajkalsk	k1gInSc1	Bajkalsk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Sljuďance	Sljuďanka	k1gFnSc6	Sljuďanka
stojí	stát	k5eAaImIp3nS	stát
železniční	železniční	k2eAgNnSc1d1	železniční
nádraží	nádraží	k1gNnSc1	nádraží
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
celé	celá	k1gFnSc2	celá
postaveno	postavit	k5eAaPmNgNnS	postavit
z	z	k7c2	z
mramoru	mramor	k1gInSc2	mramor
<g/>
.	.	kIx.	.
</s>
<s>
Bajkalsk	Bajkalsk	k1gInSc1	Bajkalsk
nabízí	nabízet	k5eAaImIp3nS	nabízet
lyžařský	lyžařský	k2eAgInSc4d1	lyžařský
areál	areál	k1gInSc4	areál
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
lanovka	lanovka	k1gFnSc1	lanovka
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
i	i	k8xC	i
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
dobrého	dobrý	k2eAgNnSc2d1	dobré
počasí	počasí	k1gNnSc2	počasí
je	být	k5eAaImIp3nS	být
shora	shora	k6eAd1	shora
možno	možno	k6eAd1	možno
vidět	vidět	k5eAaImF	vidět
až	až	k9	až
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
jezera	jezero	k1gNnSc2	jezero
a	a	k8xC	a
na	na	k7c4	na
vrcholky	vrcholek	k1gInPc4	vrcholek
Bajkalského	bajkalský	k2eAgNnSc2d1	Bajkalské
pohoří	pohoří	k1gNnSc2	pohoří
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
východním	východní	k2eAgInSc6d1	východní
břehu	břeh	k1gInSc6	břeh
se	se	k3xPyFc4	se
těší	těšit	k5eAaImIp3nS	těšit
pozornosti	pozornost	k1gFnSc2	pozornost
turistů	turist	k1gMnPc2	turist
Barguzinský	Barguzinský	k2eAgInSc4d1	Barguzinský
záliv	záliv	k1gInSc4	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Nedaleko	daleko	k6eNd1	daleko
od	od	k7c2	od
něj	on	k3xPp3gMnSc2	on
probíhá	probíhat	k5eAaImIp3nS	probíhat
výstavba	výstavba	k1gFnSc1	výstavba
turisticko-rekreační	turistickoekreační	k2eAgFnSc2d1	turisticko-rekreační
zóny	zóna	k1gFnSc2	zóna
Bajkalský	bajkalský	k2eAgInSc1d1	bajkalský
přístav	přístav	k1gInSc1	přístav
(	(	kIx(	(
<g/>
Б	Б	k?	Б
г	г	k?	г
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
zvláštní	zvláštní	k2eAgFnSc1d1	zvláštní
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
zóna	zóna	k1gFnSc1	zóna
byla	být	k5eAaImAgFnS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
na	na	k7c6	na
základě	základ	k1gInSc6	základ
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
vlády	vláda	k1gFnSc2	vláda
RF	RF	kA	RF
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
rozvoje	rozvoj	k1gInSc2	rozvoj
turismu	turismus	k1gInSc2	turismus
na	na	k7c6	na
Bajkale	Bajkal	k1gInSc6	Bajkal
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
chce	chtít	k5eAaImIp3nS	chtít
investovat	investovat	k5eAaBmF	investovat
do	do	k7c2	do
infrastruktury	infrastruktura	k1gFnSc2	infrastruktura
Bajkalského	bajkalský	k2eAgInSc2d1	bajkalský
přístavu	přístav	k1gInSc2	přístav
36	[number]	k4	36
miliard	miliarda	k4xCgFnPc2	miliarda
rublů	rubl	k1gInPc2	rubl
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
soukromé	soukromý	k2eAgInPc1d1	soukromý
subjekty	subjekt	k1gInPc1	subjekt
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
zde	zde	k6eAd1	zde
investují	investovat	k5eAaBmIp3nP	investovat
<g/>
,	,	kIx,	,
budou	být	k5eAaImBp3nP	být
na	na	k7c4	na
dobu	doba	k1gFnSc4	doba
5-10	[number]	k4	5-10
let	léto	k1gNnPc2	léto
osvobozeny	osvobodit	k5eAaPmNgFnP	osvobodit
od	od	k7c2	od
daní	daň	k1gFnPc2	daň
<g/>
.	.	kIx.	.
</s>
<s>
Počítá	počítat	k5eAaImIp3nS	počítat
se	se	k3xPyFc4	se
s	s	k7c7	s
výstavbou	výstavba	k1gFnSc7	výstavba
zázemí	zázemí	k1gNnSc2	zázemí
jak	jak	k8xS	jak
pro	pro	k7c4	pro
letní	letní	k2eAgFnSc4d1	letní
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
pro	pro	k7c4	pro
zimní	zimní	k2eAgInPc4d1	zimní
sporty	sport	k1gInPc4	sport
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c7	mezi
vesnicemi	vesnice	k1gFnPc7	vesnice
Novyj	Novyj	k1gFnSc1	Novyj
Encheluk	Encheluk	k1gInSc1	Encheluk
a	a	k8xC	a
Suchaja	Suchaja	k1gFnSc1	Suchaja
se	se	k3xPyFc4	se
poblíž	poblíž	k7c2	poblíž
řeky	řeka	k1gFnSc2	řeka
Zagza	Zagz	k1gMnSc2	Zagz
nachází	nacházet	k5eAaImIp3nS	nacházet
stejnojmenný	stejnojmenný	k2eAgInSc4d1	stejnojmenný
termální	termální	k2eAgInSc4d1	termální
pramen	pramen	k1gInSc4	pramen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nejkrásnějších	krásný	k2eAgInPc2d3	nejkrásnější
koutů	kout	k1gInPc2	kout
jezera	jezero	k1gNnSc2	jezero
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
Čivyrkujskij	Čivyrkujskij	k1gMnSc1	Čivyrkujskij
záliv	záliv	k1gInSc4	záliv
<g/>
,	,	kIx,	,
plný	plný	k2eAgInSc4d1	plný
malebných	malebný	k2eAgFnPc2d1	malebná
zátok	zátoka	k1gFnPc2	zátoka
<g/>
,	,	kIx,	,
tajemných	tajemný	k2eAgInPc2d1	tajemný
ostrovů	ostrov	k1gInPc2	ostrov
a	a	k8xC	a
léčivých	léčivý	k2eAgInPc2d1	léčivý
pramenů	pramen	k1gInPc2	pramen
<g/>
.	.	kIx.	.
</s>
<s>
Pěkný	pěkný	k2eAgInSc4d1	pěkný
výhled	výhled	k1gInSc4	výhled
na	na	k7c4	na
záliv	záliv	k1gInSc4	záliv
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
vrcholků	vrcholek	k1gInPc2	vrcholek
poloostrova	poloostrov	k1gInSc2	poloostrov
Svatý	svatý	k2eAgInSc4d1	svatý
nos	nos	k1gInSc4	nos
<g/>
,	,	kIx,	,
na	na	k7c4	na
který	který	k3yIgInSc4	který
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
snadno	snadno	k6eAd1	snadno
dostat	dostat	k5eAaPmF	dostat
z	z	k7c2	z
vesnice	vesnice	k1gFnSc2	vesnice
Usť-Barguzin	Usť-Barguzina	k1gFnPc2	Usť-Barguzina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Třicet	třicet	k4xCc1	třicet
kilometrů	kilometr	k1gInPc2	kilometr
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
ústí	ústí	k1gNnSc2	ústí
řeky	řeka	k1gFnSc2	řeka
Selengy	Seleng	k1gInPc4	Seleng
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
záliv	záliv	k1gInSc1	záliv
Posolskij	Posolskij	k1gFnSc2	Posolskij
sor	sor	k?	sor
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
funguje	fungovat	k5eAaImIp3nS	fungovat
rekreační	rekreační	k2eAgFnSc1d1	rekreační
zóna	zóna	k1gFnSc1	zóna
Bajkalskij	Bajkalskij	k1gFnSc2	Bajkalskij
Priboj	Priboj	k1gInSc1	Priboj
–	–	k?	–
Kultušnaja	Kultušnaja	k1gFnSc1	Kultušnaja
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
Bajkalu	Bajkal	k1gInSc2	Bajkal
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
vodoléčebné	vodoléčebný	k2eAgFnSc2d1	vodoléčebná
termální	termální	k2eAgFnSc2d1	termální
lázně	lázeň	k1gFnSc2	lázeň
Chakusy	Chakus	k1gInPc1	Chakus
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterých	který	k3yQgFnPc2	který
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
dostat	dostat	k5eAaPmF	dostat
pouze	pouze	k6eAd1	pouze
parníkem	parník	k1gInSc7	parník
z	z	k7c2	z
Nižněangarsku	Nižněangarsek	k1gInSc2	Nižněangarsek
či	či	k8xC	či
Severobajkalsku	Severobajkalsek	k1gInSc2	Severobajkalsek
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
po	po	k7c6	po
ledu	led	k1gInSc6	led
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Okolo	okolo	k7c2	okolo
jezera	jezero	k1gNnSc2	jezero
budují	budovat	k5eAaImIp3nP	budovat
dobrovolníci	dobrovolník	k1gMnPc1	dobrovolník
postupně	postupně	k6eAd1	postupně
systém	systém	k1gInSc4	systém
ekologických	ekologický	k2eAgFnPc2d1	ekologická
stezek	stezka	k1gFnPc2	stezka
Velká	velký	k2eAgFnSc1d1	velká
bajkalská	bajkalský	k2eAgFnSc1d1	Bajkalská
stezka	stezka	k1gFnSc1	stezka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
po	po	k7c6	po
vzoru	vzor	k1gInSc6	vzor
amerických	americký	k2eAgFnPc2d1	americká
dálkových	dálkový	k2eAgFnPc2d1	dálková
pěších	pěší	k2eAgFnPc2d1	pěší
tras	trasa	k1gFnPc2	trasa
usnadnit	usnadnit	k5eAaPmF	usnadnit
pohyb	pohyb	k1gInSc4	pohyb
turistů	turist	k1gMnPc2	turist
okolo	okolo	k7c2	okolo
Bajkalu	Bajkal	k1gInSc2	Bajkal
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Turistické	turistický	k2eAgFnSc2d1	turistická
zajímavosti	zajímavost	k1gFnSc2	zajímavost
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Jako	jako	k8xC	jako
první	první	k4xOgFnSc1	první
se	se	k3xPyFc4	se
o	o	k7c6	o
jezeře	jezero	k1gNnSc6	jezero
jménem	jméno	k1gNnSc7	jméno
"	"	kIx"	"
<g/>
Bejchaj	Bejchaj	k1gFnSc4	Bejchaj
<g/>
"	"	kIx"	"
zmiňují	zmiňovat	k5eAaImIp3nP	zmiňovat
čínské	čínský	k2eAgInPc4d1	čínský
písemné	písemný	k2eAgInPc4d1	písemný
prameny	pramen	k1gInPc4	pramen
už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
110	[number]	k4	110
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Čínské	čínský	k2eAgInPc1d1	čínský
prameny	pramen	k1gInPc1	pramen
také	také	k9	také
dokládají	dokládat	k5eAaImIp3nP	dokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
u	u	k7c2	u
jezera	jezero	k1gNnSc2	jezero
žily	žít	k5eAaImAgInP	žít
tehdy	tehdy	k6eAd1	tehdy
národy	národ	k1gInPc1	národ
Kurykanů	Kurykan	k1gMnPc2	Kurykan
a	a	k8xC	a
Bargů	Barg	k1gMnPc2	Barg
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ty	k3xPp2nSc1	ty
později	pozdě	k6eAd2	pozdě
vytlačili	vytlačit	k5eAaPmAgMnP	vytlačit
Burjati	Burjat	k1gMnPc1	Burjat
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
postupně	postupně	k6eAd1	postupně
osídlili	osídlit	k5eAaPmAgMnP	osídlit
pobřeží	pobřeží	k1gNnSc4	pobřeží
jezera	jezero	k1gNnSc2	jezero
i	i	k9	i
Zabajkalsko	Zabajkalsko	k1gNnSc1	Zabajkalsko
<g/>
.	.	kIx.	.
</s>
<s>
Obvyklé	obvyklý	k2eAgNnSc1d1	obvyklé
vysvětlení	vysvětlení	k1gNnSc1	vysvětlení
původu	původ	k1gInSc2	původ
jména	jméno	k1gNnSc2	jméno
Bajkal	Bajkal	k1gInSc1	Bajkal
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
turkického	turkický	k2eAgMnSc2d1	turkický
Б	Б	k?	Б
-	-	kIx~	-
bohaté	bohatý	k2eAgInPc4d1	bohatý
na	na	k7c4	na
ryby	ryba	k1gFnPc4	ryba
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
však	však	k9	však
vědecky	vědecky	k6eAd1	vědecky
doloženo	doložen	k2eAgNnSc1d1	doloženo
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
slovo	slovo	k1gNnSc4	slovo
mongolského	mongolský	k2eAgInSc2d1	mongolský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c4	za
prvního	první	k4xOgMnSc4	první
ruského	ruský	k2eAgMnSc4d1	ruský
objevitele	objevitel	k1gMnSc4	objevitel
Bajkalu	Bajkal	k1gInSc2	Bajkal
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
kozák	kozák	k1gMnSc1	kozák
Kurbat	Kurbat	k1gFnSc2	Kurbat
Afanasjevič	Afanasjevič	k1gMnSc1	Afanasjevič
Ivanov	Ivanov	k1gInSc1	Ivanov
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
výpravou	výprava	k1gFnSc7	výprava
stanul	stanout	k5eAaPmAgInS	stanout
na	na	k7c6	na
západním	západní	k2eAgInSc6d1	západní
břehu	břeh	k1gInSc6	břeh
jezera	jezero	k1gNnSc2	jezero
roku	rok	k1gInSc2	rok
1643	[number]	k4	1643
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
bylo	být	k5eAaImAgNnS	být
také	také	k9	také
nedaleko	nedaleko	k7c2	nedaleko
Bajkalu	Bajkal	k1gInSc2	Bajkal
založeno	založen	k2eAgNnSc1d1	založeno
významné	významný	k2eAgNnSc1d1	významné
kupecké	kupecký	k2eAgNnSc1d1	kupecké
město	město	k1gNnSc1	město
Irkutsk	Irkutsk	k1gInSc4	Irkutsk
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
jezera	jezero	k1gNnSc2	jezero
natrvalo	natrvalo	k6eAd1	natrvalo
usadili	usadit	k5eAaPmAgMnP	usadit
Rusové	Rus	k1gMnPc1	Rus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgFnPc4	první
vědomosti	vědomost	k1gFnPc4	vědomost
o	o	k7c6	o
jezeře	jezero	k1gNnSc6	jezero
byly	být	k5eAaImAgFnP	být
získány	získat	k5eAaPmNgFnP	získat
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
Kurbat	Kurbat	k1gFnSc1	Kurbat
Ivanov	Ivanov	k1gInSc1	Ivanov
<g/>
,	,	kIx,	,
Vasilij	Vasilij	k1gFnSc1	Vasilij
Kolesnikov	Kolesnikov	k1gInSc1	Kolesnikov
<g/>
,	,	kIx,	,
Ivan	Ivan	k1gMnSc1	Ivan
Pochabov	Pochabov	k1gInSc1	Pochabov
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
bylo	být	k5eAaImAgNnS	být
jezero	jezero	k1gNnSc1	jezero
poprvé	poprvé	k6eAd1	poprvé
geograficky	geograficky	k6eAd1	geograficky
popsáno	popsat	k5eAaPmNgNnS	popsat
a	a	k8xC	a
byly	být	k5eAaImAgInP	být
učiněny	učiněn	k2eAgInPc1d1	učiněn
první	první	k4xOgInPc1	první
pokusy	pokus	k1gInPc1	pokus
o	o	k7c4	o
vytvoření	vytvoření	k1gNnSc4	vytvoření
mapy	mapa	k1gFnSc2	mapa
(	(	kIx(	(
<g/>
Nicolae	Nicolae	k1gInSc1	Nicolae
Milescu	Milescus	k1gInSc2	Milescus
<g/>
,	,	kIx,	,
Fjodor	Fjodor	k1gInSc1	Fjodor
Golovin	Golovina	k1gFnPc2	Golovina
<g/>
,	,	kIx,	,
Semjon	Semjon	k1gInSc1	Semjon
Remezov	Remezov	k1gInSc1	Remezov
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1771	[number]	k4	1771
až	až	k9	až
1772	[number]	k4	1772
provedli	provést	k5eAaPmAgMnP	provést
účastníci	účastník	k1gMnPc1	účastník
expedice	expedice	k1gFnSc2	expedice
Carské	carský	k2eAgFnSc2d1	carská
akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
I.	I.	kA	I.
Georgi	Georg	k1gFnSc2	Georg
a	a	k8xC	a
Aleksander	Aleksander	k1gMnSc1	Aleksander
Puškarjov	Puškarjov	k1gInSc4	Puškarjov
vyměření	vyměření	k1gNnSc2	vyměření
jezera	jezero	k1gNnSc2	jezero
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1773	[number]	k4	1773
sestavili	sestavit	k5eAaPmAgMnP	sestavit
mapu	mapa	k1gFnSc4	mapa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velkou	velký	k2eAgFnSc4d1	velká
roli	role	k1gFnSc4	role
pro	pro	k7c4	pro
výzkum	výzkum	k1gInSc4	výzkum
jezera	jezero	k1gNnSc2	jezero
sehrálo	sehrát	k5eAaPmAgNnS	sehrát
založení	založení	k1gNnSc4	založení
Východosibiřského	východosibiřský	k2eAgNnSc2d1	Východosibiřské
oddělení	oddělení	k1gNnSc2	oddělení
Ruského	ruský	k2eAgInSc2d1	ruský
geografického	geografický	k2eAgInSc2d1	geografický
ústavu	ústav	k1gInSc2	ústav
(	(	kIx(	(
<g/>
1851	[number]	k4	1851
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
stála	stát	k5eAaImAgFnS	stát
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
výzkumu	výzkum	k1gInSc2	výzkum
Sibiře	Sibiř	k1gFnSc2	Sibiř
a	a	k8xC	a
Přibajkalí	Přibajkalý	k2eAgMnPc1d1	Přibajkalý
<g/>
.	.	kIx.	.
</s>
<s>
Známé	známý	k2eAgFnPc1d1	známá
jsou	být	k5eAaImIp3nP	být
práce	práce	k1gFnPc1	práce
V.	V.	kA	V.
Dybovského	Dybovský	k1gMnSc4	Dybovský
<g/>
,	,	kIx,	,
V.	V.	kA	V.
Godlevského	Godlevský	k2eAgInSc2d1	Godlevský
<g/>
,	,	kIx,	,
Jana	Jan	k1gMnSc4	Jan
Czerského	Czerský	k2eAgInSc2d1	Czerský
aj.	aj.	kA	aj.
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1896	[number]	k4	1896
až	až	k9	až
1902	[number]	k4	1902
sestavila	sestavit	k5eAaPmAgFnS	sestavit
hydrografická	hydrografický	k2eAgFnSc1d1	hydrografická
expedice	expedice	k1gFnSc1	expedice
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
F.	F.	kA	F.
K.	K.	kA	K.
Driženka	Driženka	k1gFnSc1	Driženka
atlas	atlas	k1gInSc1	atlas
a	a	k8xC	a
lodní	lodní	k2eAgFnSc4d1	lodní
mapu	mapa	k1gFnSc4	mapa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1916	[number]	k4	1916
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
popud	popud	k1gInSc4	popud
Zenona	Zenon	k1gMnSc2	Zenon
Franceviče	Francevič	k1gMnSc2	Francevič
Svatoše	Svatoš	k1gMnSc2	Svatoš
<g/>
,	,	kIx,	,
zoologa	zoolog	k1gMnSc2	zoolog
českého	český	k2eAgInSc2d1	český
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
založena	založen	k2eAgFnSc1d1	založena
Barguzínská	Barguzínský	k2eAgFnSc1d1	Barguzínský
sobolí	sobolí	k2eAgFnSc1d1	sobolí
rezervace	rezervace	k1gFnSc1	rezervace
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
cílem	cíl	k1gInSc7	cíl
byla	být	k5eAaImAgFnS	být
záchrana	záchrana	k1gFnSc1	záchrana
tehdy	tehdy	k6eAd1	tehdy
ohrožené	ohrožený	k2eAgFnSc2d1	ohrožená
populace	populace	k1gFnSc2	populace
sobola	sobol	k1gMnSc2	sobol
asijského	asijský	k2eAgMnSc2d1	asijský
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
byla	být	k5eAaImAgFnS	být
sestavena	sestavit	k5eAaPmNgFnS	sestavit
trvalá	trvalý	k2eAgFnSc1d1	trvalá
komise	komise	k1gFnSc1	komise
pro	pro	k7c4	pro
výzkum	výzkum	k1gInSc4	výzkum
Bajkalu	Bajkal	k1gInSc2	Bajkal
při	při	k7c6	při
akademii	akademie	k1gFnSc6	akademie
věd	věda	k1gFnPc2	věda
(	(	kIx(	(
<g/>
N.	N.	kA	N.
V.	V.	kA	V.
Nasonov	Nasonov	k1gInSc1	Nasonov
<g/>
,	,	kIx,	,
V.	V.	kA	V.
Č.	Č.	kA	Č.
Dorogostajskij	Dorogostajskij	k1gMnSc1	Dorogostajskij
<g/>
,	,	kIx,	,
I.	I.	kA	I.
I.	I.	kA	I.
Mesjacev	Mesjacva	k1gFnPc2	Mesjacva
<g/>
,	,	kIx,	,
G.	G.	kA	G.
J.	J.	kA	J.
Vereščagin	Vereščagin	k1gMnSc1	Vereščagin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Systematický	systematický	k2eAgInSc1d1	systematický
celkový	celkový	k2eAgInSc1d1	celkový
průzkum	průzkum	k1gInSc1	průzkum
byl	být	k5eAaImAgInS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1925	[number]	k4	1925
stacionářem	stacionář	k1gInSc7	stacionář
bajkalského	bajkalský	k2eAgNnSc2d1	Bajkalské
oddělení	oddělení	k1gNnSc2	oddělení
Akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1928	[number]	k4	1928
reorganizováno	reorganizovat	k5eAaBmNgNnS	reorganizovat
na	na	k7c4	na
limnologickou	limnologický	k2eAgFnSc4d1	Limnologická
stanici	stanice	k1gFnSc4	stanice
v	v	k7c6	v
Listvjance	Listvjanka	k1gFnSc6	Listvjanka
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1961	[number]	k4	1961
změnila	změnit	k5eAaPmAgFnS	změnit
na	na	k7c4	na
Limnologický	limnologický	k2eAgInSc4d1	limnologický
ústav	ústav	k1gInSc4	ústav
Sibiřského	sibiřský	k2eAgNnSc2d1	sibiřské
oddělení	oddělení	k1gNnSc2	oddělení
Akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1899	[number]	k4	1899
byla	být	k5eAaImAgFnS	být
k	k	k7c3	k
jezeru	jezero	k1gNnSc3	jezero
přivedena	přiveden	k2eAgFnSc1d1	přivedena
železnice	železnice	k1gFnSc1	železnice
<g/>
,	,	kIx,	,
Krugobajkalská	Krugobajkalský	k2eAgFnSc1d1	Krugobajkalská
železnice	železnice	k1gFnSc1	železnice
však	však	k9	však
byla	být	k5eAaImAgFnS	být
dokončena	dokončit	k5eAaPmNgFnS	dokončit
až	až	k6eAd1	až
r.	r.	kA	r.
1905	[number]	k4	1905
<g/>
,	,	kIx,	,
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
museli	muset	k5eAaImAgMnP	muset
cestující	cestující	k1gMnPc1	cestující
i	i	k8xC	i
náklad	náklad	k1gInSc1	náklad
putující	putující	k2eAgMnSc1d1	putující
po	po	k7c6	po
Transsibiřské	transsibiřský	k2eAgFnSc6d1	Transsibiřská
magistrále	magistrála	k1gFnSc6	magistrála
překonávat	překonávat	k5eAaImF	překonávat
jezero	jezero	k1gNnSc4	jezero
na	na	k7c6	na
lodi	loď	k1gFnSc6	loď
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
16	[number]	k4	16
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1918	[number]	k4	1918
došlo	dojít	k5eAaPmAgNnS	dojít
na	na	k7c6	na
Bajkale	Bajkal	k1gInSc6	Bajkal
k	k	k7c3	k
boji	boj	k1gInSc3	boj
mezi	mezi	k7c7	mezi
československými	československý	k2eAgMnPc7d1	československý
legionáři	legionář	k1gMnPc7	legionář
a	a	k8xC	a
rudoarmějci	rudoarmějec	k1gMnPc7	rudoarmějec
<g/>
.	.	kIx.	.
</s>
<s>
Čechoslováci	Čechoslovák	k1gMnPc1	Čechoslovák
se	se	k3xPyFc4	se
nalodili	nalodit	k5eAaPmAgMnP	nalodit
v	v	k7c6	v
Listvjance	Listvjanka	k1gFnSc6	Listvjanka
a	a	k8xC	a
dopluli	doplout	k5eAaPmAgMnP	doplout
do	do	k7c2	do
přístavu	přístav	k1gInSc2	přístav
Posolská	Posolský	k2eAgFnSc1d1	Posolský
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
přemohli	přemoct	k5eAaPmAgMnP	přemoct
skupinu	skupina	k1gFnSc4	skupina
rudoarmějců	rudoarmějec	k1gMnPc2	rudoarmějec
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
zneškodnili	zneškodnit	k5eAaPmAgMnP	zneškodnit
i	i	k9	i
ozbrojený	ozbrojený	k2eAgMnSc1d1	ozbrojený
ledoborec	ledoborec	k1gMnSc1	ledoborec
Bajkal	Bajkal	k1gInSc1	Bajkal
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
incidentu	incident	k1gInSc6	incident
padlo	padnout	k5eAaImAgNnS	padnout
19	[number]	k4	19
Čechoslováků	Čechoslovák	k1gMnPc2	Čechoslovák
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1950-1958	[number]	k4	1950-1958
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
Irkutská	irkutský	k2eAgFnSc1d1	Irkutská
přehradní	přehradní	k2eAgFnSc1d1	přehradní
nádrž	nádrž	k1gFnSc1	nádrž
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
výrazně	výrazně	k6eAd1	výrazně
zvedla	zvednout	k5eAaPmAgFnS	zvednout
hladinu	hladina	k1gFnSc4	hladina
jezera	jezero	k1gNnSc2	jezero
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
začali	začít	k5eAaPmAgMnP	začít
komsomolští	komsomolský	k2eAgMnPc1d1	komsomolský
úderníci	úderník	k1gMnPc1	úderník
se	se	k3xPyFc4	se
stavbou	stavba	k1gFnSc7	stavba
města	město	k1gNnSc2	město
Bajkalsk	Bajkalsk	k1gInSc1	Bajkalsk
a	a	k8xC	a
Bajkalské	bajkalský	k2eAgFnPc1d1	Bajkalská
papírny	papírna	k1gFnPc1	papírna
<g/>
,	,	kIx,	,
donedávna	donedávna	k6eAd1	donedávna
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
největších	veliký	k2eAgMnPc2d3	veliký
znečišťovatelů	znečišťovatel	k1gMnPc2	znečišťovatel
jezera	jezero	k1gNnSc2	jezero
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hloubka	hloubka	k1gFnSc1	hloubka
jezera	jezero	k1gNnSc2	jezero
byla	být	k5eAaImAgFnS	být
poprvé	poprvé	k6eAd1	poprvé
změřena	změřen	k2eAgFnSc1d1	změřena
roku	rok	k1gInSc2	rok
1959	[number]	k4	1959
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
byla	být	k5eAaImAgFnS	být
stanovena	stanovit	k5eAaPmNgFnS	stanovit
na	na	k7c4	na
1620	[number]	k4	1620
m.	m.	k?	m.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
bylo	být	k5eAaImAgNnS	být
jezero	jezero	k1gNnSc1	jezero
Bajkal	Bajkal	k1gInSc1	Bajkal
zapsáno	zapsat	k5eAaPmNgNnS	zapsat
na	na	k7c4	na
Seznam	seznam	k1gInSc4	seznam
světového	světový	k2eAgNnSc2d1	světové
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Legenda	legenda	k1gFnSc1	legenda
==	==	k?	==
</s>
</p>
<p>
<s>
Do	do	k7c2	do
Bajkalu	Bajkal	k1gInSc2	Bajkal
přivádí	přivádět	k5eAaImIp3nS	přivádět
své	svůj	k3xOyFgFnPc4	svůj
vody	voda	k1gFnPc4	voda
336	[number]	k4	336
řek	řeka	k1gFnPc2	řeka
a	a	k8xC	a
říček	říčka	k1gFnPc2	říčka
<g/>
,	,	kIx,	,
vytéká	vytékat	k5eAaImIp3nS	vytékat
z	z	k7c2	z
něj	on	k3xPp3gNnSc2	on
však	však	k9	však
jen	jen	k9	jen
jediná	jediný	k2eAgFnSc1d1	jediná
-	-	kIx~	-
Angara	Angara	k1gFnSc1	Angara
<g/>
.	.	kIx.	.
</s>
<s>
Sibiřská	sibiřský	k2eAgFnSc1d1	sibiřská
legenda	legenda	k1gFnSc1	legenda
popisuje	popisovat	k5eAaImIp3nS	popisovat
romantický	romantický	k2eAgInSc1d1	romantický
příběh	příběh	k1gInSc1	příběh
o	o	k7c6	o
Bajkalu	Bajkal	k1gInSc6	Bajkal
<g/>
,	,	kIx,	,
otci	otec	k1gMnSc3	otec
všech	všecek	k3xTgFnPc2	všecek
těchto	tento	k3xDgFnPc2	tento
řek	řeka	k1gFnPc2	řeka
<g/>
,	,	kIx,	,
Angaře	Angara	k1gFnSc6	Angara
a	a	k8xC	a
Jeniseji	Jenisej	k1gInSc6	Jenisej
<g/>
.	.	kIx.	.
</s>
<s>
Angara	Angara	k1gFnSc1	Angara
se	se	k3xPyFc4	se
zamilovala	zamilovat	k5eAaPmAgFnS	zamilovat
do	do	k7c2	do
bohatýra	bohatýr	k1gMnSc2	bohatýr
Jeniseje	Jenisej	k1gInSc2	Jenisej
<g/>
,	,	kIx,	,
Bajkal	Bajkal	k1gInSc1	Bajkal
ji	on	k3xPp3gFnSc4	on
však	však	k9	však
nechtěl	chtít	k5eNaImAgMnS	chtít
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
pustit	pustit	k5eAaPmF	pustit
<g/>
.	.	kIx.	.
</s>
<s>
Angara	Angara	k1gFnSc1	Angara
tedy	tedy	k9	tedy
za	za	k7c7	za
Jenisejem	Jenisej	k1gInSc7	Jenisej
od	od	k7c2	od
otce	otec	k1gMnSc2	otec
utekla	utéct	k5eAaPmAgFnS	utéct
<g/>
;	;	kIx,	;
podle	podle	k7c2	podle
legendy	legenda	k1gFnSc2	legenda
Šaman-kameň	Šamanameň	k1gFnSc2	Šaman-kameň
<g/>
,	,	kIx,	,
nacházející	nacházející	k2eAgFnSc2d1	nacházející
se	se	k3xPyFc4	se
uprostřed	uprostřed	k7c2	uprostřed
výtoku	výtok	k1gInSc2	výtok
Angary	Angara	k1gFnSc2	Angara
z	z	k7c2	z
Bajkalu	Bajkal	k1gInSc2	Bajkal
u	u	k7c2	u
vesnice	vesnice	k1gFnSc2	vesnice
Listvjanka	Listvjanka	k1gFnSc1	Listvjanka
(	(	kIx(	(
<g/>
pozor	pozor	k1gInSc1	pozor
na	na	k7c4	na
záměnu	záměna	k1gFnSc4	záměna
se	s	k7c7	s
skálou	skála	k1gFnSc7	skála
Šamankou	šamanka	k1gFnSc7	šamanka
na	na	k7c6	na
Olchonu	Olchon	k1gInSc6	Olchon
<g/>
,	,	kIx,	,
největším	veliký	k2eAgInSc6d3	veliký
ostrově	ostrov	k1gInSc6	ostrov
Bajkalu	Bajkal	k1gInSc2	Bajkal
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hodil	hodit	k5eAaImAgMnS	hodit
rozezlený	rozezlený	k2eAgMnSc1d1	rozezlený
otec	otec	k1gMnSc1	otec
Bajkal	Bajkal	k1gInSc4	Bajkal
za	za	k7c7	za
neposlušnou	poslušný	k2eNgFnSc7d1	neposlušná
dcerou	dcera	k1gFnSc7	dcera
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ji	on	k3xPp3gFnSc4	on
zastavil	zastavit	k5eAaPmAgMnS	zastavit
<g/>
.	.	kIx.	.
</s>
<s>
Angaře	Angara	k1gFnSc3	Angara
se	se	k3xPyFc4	se
však	však	k9	však
přesto	přesto	k8xC	přesto
podařilo	podařit	k5eAaPmAgNnS	podařit
utéci	utéct	k5eAaPmF	utéct
a	a	k8xC	a
splynout	splynout	k5eAaPmF	splynout
s	s	k7c7	s
Jenisejem	Jenisej	k1gInSc7	Jenisej
ve	v	k7c6	v
věčném	věčný	k2eAgNnSc6d1	věčné
objetí	objetí	k1gNnSc6	objetí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgInP	být
použity	použit	k2eAgInPc1d1	použit
překlady	překlad	k1gInPc1	překlad
textů	text	k1gInPc2	text
z	z	k7c2	z
článků	článek	k1gInPc2	článek
Б	Б	k?	Б
na	na	k7c6	na
ruské	ruský	k2eAgFnSc6d1	ruská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
a	a	k8xC	a
Х	Х	k?	Х
о	о	k?	о
Б	Б	k?	Б
na	na	k7c6	na
ruské	ruský	k2eAgFnSc6d1	ruská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgFnP	být
použity	použit	k2eAgFnPc1d1	použita
informace	informace	k1gFnPc1	informace
z	z	k7c2	z
Velké	velký	k2eAgFnSc2d1	velká
sovětské	sovětský	k2eAgFnSc2d1	sovětská
encyklopedie	encyklopedie	k1gFnSc2	encyklopedie
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
"	"	kIx"	"
<g/>
Б	Б	k?	Б
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Bajkal	Bajkal	k1gInSc1	Bajkal
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
Jezero	jezero	k1gNnSc1	jezero
Bajkal	Bajkal	k1gInSc1	Bajkal
-	-	kIx~	-
Irkutsk	Irkutsk	k1gInSc1	Irkutsk
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
Jezero	jezero	k1gNnSc1	jezero
Bajkal	Bajkal	k1gInSc4	Bajkal
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
Mapa	mapa	k1gFnSc1	mapa
jezera	jezero	k1gNnSc2	jezero
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
Ledovce	ledovec	k1gInPc1	ledovec
a	a	k8xC	a
tající	tající	k2eAgFnSc1d1	tající
voda	voda	k1gFnSc1	voda
směřující	směřující	k2eAgFnSc1d1	směřující
k	k	k7c3	k
Bajkalu	Bajkal	k1gInSc3	Bajkal
během	během	k7c2	během
poslední	poslední	k2eAgFnSc2d1	poslední
doby	doba	k1gFnSc2	doba
ledové	ledový	k2eAgFnSc2d1	ledová
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Jezero	jezero	k1gNnSc4	jezero
Bajkal	Bajkal	k1gInSc1	Bajkal
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
UNESCO	UNESCO	kA	UNESCO
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Bajkal	Bajkal	k1gInSc1	Bajkal
-	-	kIx~	-
obrázky	obrázek	k1gInPc1	obrázek
a	a	k8xC	a
informace	informace	k1gFnPc1	informace
(	(	kIx(	(
<g/>
fotogalerie	fotogalerie	k1gFnPc1	fotogalerie
<g/>
)	)	kIx)	)
</s>
</p>
