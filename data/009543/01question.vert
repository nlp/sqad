<s>
Jaké	jaký	k3yRgNnSc1	jaký
jezero	jezero	k1gNnSc1	jezero
se	se	k3xPyFc4	se
rozprostíra	rozprostír	k1gMnSc2	rozprostír
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
východní	východní	k2eAgFnSc2d1	východní
Sibiře	Sibiř	k1gFnSc2	Sibiř
<g/>
,	,	kIx,	,
na	na	k7c6	na
rozhraní	rozhraní	k1gNnSc6	rozhraní
Burjatské	burjatský	k2eAgFnSc2d1	Burjatská
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
Irkutské	irkutský	k2eAgFnSc2d1	Irkutská
oblasti	oblast	k1gFnSc2	oblast
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
?	?	kIx.	?
</s>
