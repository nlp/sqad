<p>
<s>
Albinismus	albinismus	k1gInSc1	albinismus
je	být	k5eAaImIp3nS	být
barevná	barevný	k2eAgFnSc1d1	barevná
odchylka	odchylka	k1gFnSc1	odchylka
živých	živý	k2eAgInPc2d1	živý
organismů	organismus	k1gInPc2	organismus
<g/>
,	,	kIx,	,
způsobená	způsobený	k2eAgFnSc1d1	způsobená
poruchou	porucha	k1gFnSc7	porucha
tvorby	tvorba	k1gFnSc2	tvorba
barviva	barvivo	k1gNnSc2	barvivo
melaninu	melanin	k1gInSc2	melanin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příčiny	příčina	k1gFnPc4	příčina
==	==	k?	==
</s>
</p>
<p>
<s>
Podstatou	podstata	k1gFnSc7	podstata
albinismu	albinismus	k1gInSc2	albinismus
je	být	k5eAaImIp3nS	být
vrozená	vrozený	k2eAgFnSc1d1	vrozená
absence	absence	k1gFnSc1	absence
enzymu	enzym	k1gInSc2	enzym
tyrozinázy	tyrozináza	k1gFnSc2	tyrozináza
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
nezbytný	zbytný	k2eNgInSc1d1	zbytný
při	při	k7c6	při
tvorbě	tvorba	k1gFnSc6	tvorba
melaninu	melanin	k1gInSc2	melanin
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
albinotických	albinotický	k2eAgMnPc2d1	albinotický
jedinců	jedinec	k1gMnPc2	jedinec
tak	tak	k6eAd1	tak
nedochází	docházet	k5eNaImIp3nS	docházet
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
tvorbě	tvorba	k1gFnSc3	tvorba
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
na	na	k7c6	na
zbarvení	zbarvení	k1gNnSc6	zbarvení
částí	část	k1gFnPc2	část
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
za	za	k7c4	za
něž	jenž	k3xRgInPc4	jenž
je	být	k5eAaImIp3nS	být
melanin	melanin	k1gInSc1	melanin
zodpovědný	zodpovědný	k2eAgInSc1d1	zodpovědný
<g/>
.	.	kIx.	.
</s>
<s>
Charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
je	být	k5eAaImIp3nS	být
bezbarvá	bezbarvý	k2eAgFnSc1d1	bezbarvá
kůže	kůže	k1gFnSc1	kůže
a	a	k8xC	a
červeně	červeň	k1gFnPc1	červeň
zbarvené	zbarvený	k2eAgFnPc1d1	zbarvená
oči	oko	k1gNnPc4	oko
(	(	kIx(	(
<g/>
zbarvení	zbarvení	k1gNnPc2	zbarvení
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
prosvítající	prosvítající	k2eAgFnSc1d1	prosvítající
krev	krev	k1gFnSc1	krev
v	v	k7c6	v
kapilárách	kapilára	k1gFnPc6	kapilára
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Absence	absence	k1gFnSc1	absence
pigmentu	pigment	k1gInSc2	pigment
v	v	k7c6	v
očích	oko	k1gNnPc6	oko
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
světloplachost	světloplachost	k1gFnSc1	světloplachost
a	a	k8xC	a
problémy	problém	k1gInPc1	problém
s	s	k7c7	s
viděním	vidění	k1gNnSc7	vidění
<g/>
,	,	kIx,	,
albinotičtí	albinotický	k2eAgMnPc1d1	albinotický
jedinci	jedinec	k1gMnPc1	jedinec
se	se	k3xPyFc4	se
tak	tak	k9	tak
stávají	stávat	k5eAaImIp3nP	stávat
snadnou	snadný	k2eAgFnSc7d1	snadná
kořistí	kořist	k1gFnSc7	kořist
predátorů	predátor	k1gMnPc2	predátor
nebo	nebo	k8xC	nebo
zahynou	zahynout	k5eAaPmIp3nP	zahynout
při	při	k7c6	při
srážce	srážka	k1gFnSc6	srážka
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
objekty	objekt	k1gInPc7	objekt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nepřesnost	nepřesnost	k1gFnSc1	nepřesnost
používání	používání	k1gNnSc2	používání
termínu	termín	k1gInSc2	termín
==	==	k?	==
</s>
</p>
<p>
<s>
Termín	termín	k1gInSc1	termín
"	"	kIx"	"
<g/>
albinismus	albinismus	k1gInSc1	albinismus
<g/>
"	"	kIx"	"
bývá	bývat	k5eAaImIp3nS	bývat
často	často	k6eAd1	často
používán	používat	k5eAaImNgInS	používat
nepřesně	přesně	k6eNd1	přesně
kvůli	kvůli	k7c3	kvůli
nepochopení	nepochopení	k1gNnSc3	nepochopení
jeho	jeho	k3xOp3gFnSc2	jeho
podstaty	podstata	k1gFnSc2	podstata
<g/>
.	.	kIx.	.
</s>
<s>
Albinismus	albinismus	k1gInSc1	albinismus
je	být	k5eAaImIp3nS	být
poruchou	porucha	k1gFnSc7	porucha
tvorby	tvorba	k1gFnSc2	tvorba
melaninu	melanin	k1gInSc2	melanin
–	–	k?	–
neovlivňuje	ovlivňovat	k5eNaImIp3nS	ovlivňovat
tedy	tedy	k9	tedy
tvorbu	tvorba	k1gFnSc4	tvorba
jiných	jiný	k2eAgInPc2d1	jiný
pigmentů	pigment	k1gInPc2	pigment
<g/>
,	,	kIx,	,
např.	např.	kA	např.
karotenů	karoten	k1gInPc2	karoten
<g/>
.	.	kIx.	.
</s>
<s>
Albinotičtí	albinotický	k2eAgMnPc1d1	albinotický
jedinci	jedinec	k1gMnPc1	jedinec
tedy	tedy	k9	tedy
nemusí	muset	k5eNaImIp3nP	muset
být	být	k5eAaImF	být
nezbytně	zbytně	k6eNd1	zbytně
bílí	bílý	k2eAgMnPc1d1	bílý
<g/>
,	,	kIx,	,
části	část	k1gFnSc3	část
těla	tělo	k1gNnSc2	tělo
obarvené	obarvený	k2eAgInPc4d1	obarvený
jinými	jiný	k2eAgNnPc7d1	jiné
pigmenty	pigment	k1gInPc1	pigment
si	se	k3xPyFc3	se
zachovávají	zachovávat	k5eAaImIp3nP	zachovávat
původní	původní	k2eAgFnSc4d1	původní
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
používán	používán	k2eAgInSc1d1	používán
termín	termín	k1gInSc1	termín
částečný	částečný	k2eAgInSc4d1	částečný
albín	albín	k1gInSc4	albín
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
při	při	k7c6	při
popisu	popis	k1gInSc6	popis
jedinců	jedinec	k1gMnPc2	jedinec
s	s	k7c7	s
poruchou	porucha	k1gFnSc7	porucha
distribuce	distribuce	k1gFnSc2	distribuce
melaninu	melanin	k1gInSc2	melanin
<g/>
,	,	kIx,	,
u	u	k7c2	u
nichž	jenž	k3xRgInPc2	jenž
jsou	být	k5eAaImIp3nP	být
části	část	k1gFnPc1	část
těla	tělo	k1gNnSc2	tělo
bílé	bílý	k2eAgFnPc1d1	bílá
a	a	k8xC	a
části	část	k1gFnPc1	část
normálně	normálně	k6eAd1	normálně
zbarvené	zbarvený	k2eAgFnPc1d1	zbarvená
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
definice	definice	k1gFnSc2	definice
albinismu	albinismus	k1gInSc2	albinismus
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nic	nic	k3yNnSc1	nic
jako	jako	k9	jako
"	"	kIx"	"
<g/>
částečný	částečný	k2eAgInSc1d1	částečný
albinismus	albinismus	k1gInSc1	albinismus
<g/>
"	"	kIx"	"
existovat	existovat	k5eAaImF	existovat
nemůže	moct	k5eNaImIp3nS	moct
-	-	kIx~	-
při	při	k7c6	při
poruše	porucha	k1gFnSc6	porucha
tvorby	tvorba	k1gFnSc2	tvorba
melaninu	melanin	k1gInSc2	melanin
žádný	žádný	k3yNgInSc4	žádný
melanin	melanin	k1gInSc4	melanin
nevzniká	vznikat	k5eNaImIp3nS	vznikat
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
ne	ne	k9	ne
všechna	všechen	k3xTgNnPc4	všechen
celkově	celkově	k6eAd1	celkově
bílá	bílý	k2eAgNnPc4d1	bílé
zvířata	zvíře	k1gNnPc4	zvíře
jsou	být	k5eAaImIp3nP	být
albíny	albín	k1gInPc1	albín
-	-	kIx~	-
pokud	pokud	k8xS	pokud
nemají	mít	k5eNaImIp3nP	mít
červené	červený	k2eAgFnPc1d1	červená
oči	oko	k1gNnPc4	oko
a	a	k8xC	a
světlou	světlý	k2eAgFnSc4d1	světlá
(	(	kIx(	(
<g/>
růžovou	růžový	k2eAgFnSc4d1	růžová
<g/>
)	)	kIx)	)
kůži	kůže	k1gFnSc4	kůže
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obou	dva	k4xCgInPc6	dva
těchto	tento	k3xDgInPc6	tento
případech	případ	k1gInPc6	případ
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
jinou	jiný	k2eAgFnSc4d1	jiná
barevnou	barevný	k2eAgFnSc4d1	barevná
odchylku	odchylka	k1gFnSc4	odchylka
–	–	k?	–
leucismus	leucismus	k1gInSc1	leucismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Albinismus	albinismus	k1gInSc1	albinismus
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
==	==	k?	==
</s>
</p>
<p>
<s>
Charakteristickými	charakteristický	k2eAgInPc7d1	charakteristický
znaky	znak	k1gInPc7	znak
lidí	člověk	k1gMnPc2	člověk
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
albínů	albín	k1gInPc2	albín
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
kůže	kůže	k1gFnSc1	kůže
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
zdá	zdát	k5eAaImIp3nS	zdát
až	až	k9	až
průhledná	průhledný	k2eAgFnSc1d1	průhledná
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
světlé	světlý	k2eAgInPc1d1	světlý
vlasy	vlas	k1gInPc1	vlas
i	i	k9	i
ochlupení	ochlupení	k1gNnSc1	ochlupení
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
i	i	k9	i
nebarevnost	nebarevnost	k1gFnSc4	nebarevnost
duhovky	duhovka	k1gFnSc2	duhovka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
se	se	k3xPyFc4	se
zdá	zdát	k5eAaImIp3nS	zdát
být	být	k5eAaImF	být
narůžovělá	narůžovělý	k2eAgFnSc1d1	narůžovělá
(	(	kIx(	(
<g/>
kapiláry	kapilára	k1gFnPc1	kapilára
prosvítají	prosvítat	k5eAaImIp3nP	prosvítat
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
oka	oko	k1gNnSc2	oko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Albinismus	albinismus	k1gInSc1	albinismus
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
vzácný	vzácný	k2eAgMnSc1d1	vzácný
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
však	však	k9	však
projevit	projevit	k5eAaPmF	projevit
u	u	k7c2	u
kteréhokoliv	kterýkoliv	k3yIgInSc2	kterýkoliv
živočišného	živočišný	k2eAgInSc2d1	živočišný
druhu	druh	k1gInSc2	druh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
albinismus	albinismus	k1gInSc1	albinismus
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
albinismus	albinismus	k1gInSc1	albinismus
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
albinismus	albinismus	k1gInSc4	albinismus
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
