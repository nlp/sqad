<s>
Albinismus	albinismus	k1gInSc1	albinismus
je	být	k5eAaImIp3nS	být
barevná	barevný	k2eAgFnSc1d1	barevná
odchylka	odchylka	k1gFnSc1	odchylka
živých	živý	k2eAgInPc2d1	živý
organismů	organismus	k1gInPc2	organismus
<g/>
,	,	kIx,	,
způsobená	způsobený	k2eAgFnSc1d1	způsobená
poruchou	porucha	k1gFnSc7	porucha
tvorby	tvorba	k1gFnSc2	tvorba
barviva	barvivo	k1gNnSc2	barvivo
melaninu	melanin	k1gInSc2	melanin
<g/>
.	.	kIx.	.
</s>
