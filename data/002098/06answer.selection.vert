<s>
Původně	původně	k6eAd1	původně
římský	římský	k2eAgInSc1d1	římský
jazyk	jazyk	k1gInSc1	jazyk
latina	latina	k1gFnSc1	latina
je	být	k5eAaImIp3nS	být
základem	základ	k1gInSc7	základ
mnoha	mnoho	k4c2	mnoho
evropských	evropský	k2eAgInPc2d1	evropský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
římské	římský	k2eAgNnSc1d1	římské
právo	právo	k1gNnSc1	právo
vzorem	vzor	k1gInSc7	vzor
pro	pro	k7c4	pro
mnoho	mnoho	k4c4	mnoho
právních	právní	k2eAgInPc2d1	právní
a	a	k8xC	a
politických	politický	k2eAgInPc2d1	politický
systémů	systém	k1gInPc2	systém
a	a	k8xC	a
původně	původně	k6eAd1	původně
římské	římský	k2eAgNnSc4d1	římské
písmo	písmo	k1gNnSc4	písmo
se	se	k3xPyFc4	se
jako	jako	k9	jako
latinka	latinka	k1gFnSc1	latinka
stalo	stát	k5eAaPmAgNnS	stát
nejrozšířenějším	rozšířený	k2eAgNnSc7d3	nejrozšířenější
písmem	písmo	k1gNnSc7	písmo
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
