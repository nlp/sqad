<p>
<s>
Štěstí	štěstit	k5eAaImIp3nS	štěstit
a	a	k8xC	a
šťastný	šťastný	k2eAgMnSc1d1	šťastný
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
odlišných	odlišný	k2eAgInPc6d1	odlišný
významech	význam	k1gInPc6	význam
či	či	k8xC	či
souvislostech	souvislost	k1gFnPc6	souvislost
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
jako	jako	k8xC	jako
vnější	vnější	k2eAgFnSc1d1	vnější
příznivá	příznivý	k2eAgFnSc1d1	příznivá
událost	událost	k1gFnSc1	událost
nebo	nebo	k8xC	nebo
okolnost	okolnost	k1gFnSc1	okolnost
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
nahodilá	nahodilý	k2eAgFnSc1d1	nahodilá
a	a	k8xC	a
nezasloužená	zasloužený	k2eNgFnSc1d1	nezasloužená
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
mít	mít	k5eAaImF	mít
štěstí	štěstí	k1gNnSc4	štěstí
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
udělat	udělat	k5eAaPmF	udělat
štěstí	štěstí	k1gNnSc4	štěstí
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
jako	jako	k9	jako
blaženost	blaženost	k1gFnSc1	blaženost
<g/>
,	,	kIx,	,
vnitřní	vnitřní	k2eAgInSc1d1	vnitřní
pocit	pocit	k1gInSc1	pocit
radostného	radostný	k2eAgMnSc2d1	radostný
a	a	k8xC	a
více	hodně	k6eAd2	hodně
méně	málo	k6eAd2	málo
trvalého	trvalý	k2eAgNnSc2d1	trvalé
vyplnění	vyplnění	k1gNnSc2	vyplnění
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
být	být	k5eAaImF	být
šťastný	šťastný	k2eAgMnSc1d1	šťastný
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
mít	mít	k5eAaImF	mít
šťastnou	šťastný	k2eAgFnSc4d1	šťastná
povahu	povaha	k1gFnSc4	povaha
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Původ	původ	k1gInSc1	původ
slova	slovo	k1gNnSc2	slovo
==	==	k?	==
</s>
</p>
<p>
<s>
Obdobná	obdobný	k2eAgNnPc1d1	obdobné
slova	slovo	k1gNnPc1	slovo
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
slovanských	slovanský	k2eAgInPc6d1	slovanský
jazycích	jazyk	k1gInPc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
V.	V.	kA	V.
Machka	Machek	k1gInSc2	Machek
se	se	k3xPyFc4	se
skládají	skládat	k5eAaImIp3nP	skládat
z	z	k7c2	z
indoevropské	indoevropský	k2eAgFnSc2d1	indoevropská
předpony	předpona	k1gFnSc2	předpona
su-	su-	k?	su-
<g/>
,	,	kIx,	,
dobrý	dobrý	k2eAgMnSc1d1	dobrý
<g/>
,	,	kIx,	,
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
<g/>
,	,	kIx,	,
a	a	k8xC	a
slovanského	slovanský	k2eAgNnSc2d1	slovanské
časť	časť	k1gFnSc1	časť
<g/>
,	,	kIx,	,
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
úděl	úděl	k1gInSc1	úděl
<g/>
.	.	kIx.	.
</s>
<s>
Výše	vysoce	k6eAd2	vysoce
naznačené	naznačený	k2eAgInPc4d1	naznačený
rozlišení	rozlišení	k1gNnSc3	rozlišení
dvojího	dvojí	k4xRgInSc2	dvojí
významu	význam	k1gInSc2	význam
štěstí	štěstí	k1gNnSc2	štěstí
a	a	k8xC	a
blaženosti	blaženost	k1gFnSc2	blaženost
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
i	i	k9	i
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
jiných	jiný	k2eAgInPc6d1	jiný
jazycích	jazyk	k1gInPc6	jazyk
<g/>
:	:	kIx,	:
v	v	k7c6	v
řečtině	řečtina	k1gFnSc6	řečtina
eutychia	eutychium	k1gNnSc2	eutychium
a	a	k8xC	a
eudaimonia	eudaimonium	k1gNnSc2	eudaimonium
<g/>
,	,	kIx,	,
v	v	k7c6	v
latině	latina	k1gFnSc6	latina
fortuna	fortuna	k1gFnSc1	fortuna
a	a	k8xC	a
beatitudo	beatitudo	k1gNnSc1	beatitudo
<g/>
,	,	kIx,	,
ve	v	k7c6	v
francouzštině	francouzština	k1gFnSc6	francouzština
bonne	bonnout	k5eAaImIp3nS	bonnout
chance	chanec	k1gInSc2	chanec
a	a	k8xC	a
bonheur	bonheura	k1gFnPc2	bonheura
<g/>
,	,	kIx,	,
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
luck	lucka	k1gFnPc2	lucka
a	a	k8xC	a
happiness	happinessa	k1gFnPc2	happinessa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Štěstí	štěstí	k1gNnSc1	štěstí
ve	v	k7c6	v
filosofii	filosofie	k1gFnSc6	filosofie
==	==	k?	==
</s>
</p>
<p>
<s>
Sókratés	Sókratés	k6eAd1	Sókratés
v	v	k7c6	v
dialogu	dialog	k1gInSc6	dialog
"	"	kIx"	"
<g/>
Gorgias	Gorgias	k1gInSc1	Gorgias
<g/>
"	"	kIx"	"
polemizuje	polemizovat	k5eAaImIp3nS	polemizovat
s	s	k7c7	s
Kalliklem	Kallikl	k1gInSc7	Kallikl
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
něhož	jenž	k3xRgNnSc4	jenž
štěstí	štěstí	k1gNnSc4	štěstí
je	být	k5eAaImIp3nS	být
blahobyt	blahobyt	k1gInSc1	blahobyt
a	a	k8xC	a
nezkrocená	zkrocený	k2eNgFnSc1d1	nezkrocená
svoboda	svoboda	k1gFnSc1	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Aristotelés	Aristotelés	k6eAd1	Aristotelés
v	v	k7c6	v
"	"	kIx"	"
<g/>
Etice	etika	k1gFnSc6	etika
Nikomachově	Nikomachův	k2eAgFnSc6d1	Nikomachova
<g/>
"	"	kIx"	"
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
člověk	člověk	k1gMnSc1	člověk
nemůže	moct	k5eNaImIp3nS	moct
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
skutečného	skutečný	k2eAgNnSc2d1	skutečné
štěstí	štěstí	k1gNnSc2	štěstí
(	(	kIx(	(
<g/>
eudaimonia	eudaimonium	k1gNnSc2	eudaimonium
<g/>
)	)	kIx)	)
ani	ani	k8xC	ani
sám	sám	k3xTgMnSc1	sám
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jen	jen	k9	jen
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
svobodných	svobodný	k2eAgFnPc2d1	svobodná
<g/>
.	.	kIx.	.
</s>
<s>
Štěstí	štěstí	k1gNnSc1	štěstí
není	být	k5eNaImIp3nS	být
nahodilý	nahodilý	k2eAgInSc4d1	nahodilý
pocit	pocit	k1gInSc4	pocit
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
výsledek	výsledek	k1gInSc4	výsledek
rozumně	rozumně	k6eAd1	rozumně
vedeného	vedený	k2eAgInSc2d1	vedený
dobrého	dobrý	k2eAgInSc2d1	dobrý
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
řecké	řecký	k2eAgFnSc2d1	řecká
polis	polis	k1gFnSc2	polis
však	však	k9	však
i	i	k8xC	i
filosofové	filosof	k1gMnPc1	filosof
hledají	hledat	k5eAaImIp3nP	hledat
individuální	individuální	k2eAgNnSc1d1	individuální
štěstí	štěstí	k1gNnSc4	štěstí
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
buď	buď	k8xC	buď
jako	jako	k9	jako
rozumové	rozumový	k2eAgNnSc1d1	rozumové
ovládání	ovládání	k1gNnSc1	ovládání
vášní	vášeň	k1gFnPc2	vášeň
a	a	k8xC	a
odolnost	odolnost	k1gFnSc4	odolnost
proti	proti	k7c3	proti
vnějším	vnější	k2eAgInPc3d1	vnější
dojmům	dojem	k1gInPc3	dojem
a	a	k8xC	a
vlivům	vliv	k1gInPc3	vliv
(	(	kIx(	(
<g/>
kynismus	kynismus	k1gInSc1	kynismus
<g/>
,	,	kIx,	,
stoicismus	stoicismus	k1gInSc1	stoicismus
–	–	k?	–
ataraxia	ataraxia	k1gFnSc1	ataraxia
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
jako	jako	k9	jako
vyhledávání	vyhledávání	k1gNnSc1	vyhledávání
a	a	k8xC	a
užívání	užívání	k1gNnSc1	užívání
příjemností	příjemnost	k1gFnPc2	příjemnost
života	život	k1gInSc2	život
(	(	kIx(	(
<g/>
epikureismus	epikureismus	k1gInSc1	epikureismus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kautilja	Kautilja	k6eAd1	Kautilja
vyvozuje	vyvozovat	k5eAaImIp3nS	vyvozovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
štěstí	štěstí	k1gNnSc1	štěstí
(	(	kIx(	(
<g/>
sukham	sukham	k1gInSc1	sukham
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
výsledkem	výsledek	k1gInSc7	výsledek
sebeovládání	sebeovládání	k1gNnSc2	sebeovládání
(	(	kIx(	(
<g/>
džitátmá	džitátmat	k5eAaPmIp3nS	džitátmat
<g/>
,	,	kIx,	,
doslova	doslova	k6eAd1	doslova
vítězství	vítězství	k1gNnSc4	vítězství
nad	nad	k7c7	nad
sebou	se	k3xPyFc7	se
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
touhami	touha	k1gFnPc7	touha
po	po	k7c6	po
hmotném	hmotný	k2eAgInSc6d1	hmotný
požitku	požitek	k1gInSc6	požitek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Totéž	týž	k3xTgNnSc1	týž
popisuje	popisovat	k5eAaImIp3nS	popisovat
Bhagavadgítá	Bhagavadgítá	k1gFnSc1	Bhagavadgítá
5.16	[number]	k4	5.16
<g/>
-	-	kIx~	-
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Štěstí	štěstí	k1gNnSc1	štěstí
jako	jako	k8xC	jako
soustavné	soustavný	k2eAgNnSc1d1	soustavné
vyhýbání	vyhýbání	k1gNnSc1	vyhýbání
se	se	k3xPyFc4	se
utrpení	utrpení	k1gNnSc1	utrpení
a	a	k8xC	a
potlačování	potlačování	k1gNnSc1	potlačování
žádosti	žádost	k1gFnSc2	žádost
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
vůdčích	vůdčí	k2eAgFnPc2d1	vůdčí
myšlenek	myšlenka	k1gFnPc2	myšlenka
buddhismu	buddhismus	k1gInSc2	buddhismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
křesťanství	křesťanství	k1gNnSc6	křesťanství
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
objevuje	objevovat	k5eAaImIp3nS	objevovat
i	i	k9	i
stoické	stoický	k2eAgNnSc1d1	stoické
dědictví	dědictví	k1gNnSc1	dědictví
ukázněného	ukázněný	k2eAgInSc2d1	ukázněný
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
cílem	cíl	k1gInSc7	cíl
života	život	k1gInSc2	život
je	být	k5eAaImIp3nS	být
spása	spása	k1gFnSc1	spása
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
štěstí	štěstí	k1gNnSc1	štěstí
zde	zde	k6eAd1	zde
nehraje	hrát	k5eNaImIp3nS	hrát
velkou	velký	k2eAgFnSc4d1	velká
roli	role	k1gFnSc4	role
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
tradici	tradice	k1gFnSc6	tradice
je	být	k5eAaImIp3nS	být
nejisté	jistý	k2eNgNnSc1d1	nejisté
a	a	k8xC	a
nespolehlivé	spolehlivý	k2eNgNnSc1d1	nespolehlivé
štěstí	štěstí	k1gNnSc1	štěstí
spíše	spíše	k9	spíše
iluzí	iluze	k1gFnSc7	iluze
také	také	k9	také
pro	pro	k7c4	pro
Kanta	Kant	k1gMnSc4	Kant
nebo	nebo	k8xC	nebo
Schopenhauera	Schopenhauer	k1gMnSc4	Schopenhauer
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgInSc3	ten
angličtí	anglický	k2eAgMnPc1d1	anglický
autoři	autor	k1gMnPc1	autor
(	(	kIx(	(
<g/>
Jeremy	Jerema	k1gFnSc2	Jerema
Bentham	Bentham	k1gInSc1	Bentham
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Stuart	Stuarta	k1gFnPc2	Stuarta
Mill	Mill	k1gMnSc1	Mill
<g/>
,	,	kIx,	,
utilitarismus	utilitarismus	k1gInSc1	utilitarismus
<g/>
)	)	kIx)	)
začínají	začínat	k5eAaImIp3nP	začínat
uvažovat	uvažovat	k5eAaImF	uvažovat
o	o	k7c6	o
objektivním	objektivní	k2eAgNnSc6d1	objektivní
a	a	k8xC	a
měřitelném	měřitelný	k2eAgNnSc6d1	měřitelné
"	"	kIx"	"
<g/>
štěstí	štěstí	k1gNnSc6	štěstí
<g/>
"	"	kIx"	"
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
blahobytu	blahobyt	k1gInSc2	blahobyt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
moderních	moderní	k2eAgFnPc6d1	moderní
masových	masový	k2eAgFnPc6d1	masová
společnostech	společnost	k1gFnPc6	společnost
přibývá	přibývat	k5eAaImIp3nS	přibývat
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
po	po	k7c6	po
štěstí	štěstí	k1gNnSc6	štěstí
touží	toužit	k5eAaImIp3nP	toužit
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
však	však	k9	však
ztratili	ztratit	k5eAaPmAgMnP	ztratit
naději	naděje	k1gFnSc4	naděje
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
ho	on	k3xPp3gMnSc4	on
mohli	moct	k5eAaImAgMnP	moct
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
si	se	k3xPyFc3	se
štěstí	štěstí	k1gNnSc4	štěstí
zároveň	zároveň	k6eAd1	zároveň
představují	představovat	k5eAaImIp3nP	představovat
jako	jako	k8xS	jako
něco	něco	k3yInSc4	něco
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
by	by	kYmCp3nP	by
měli	mít	k5eAaImAgMnP	mít
dostat	dostat	k5eAaPmF	dostat
zvenčí	zvenčí	k6eAd1	zvenčí
<g/>
,	,	kIx,	,
propadají	propadat	k5eAaImIp3nP	propadat
často	často	k6eAd1	často
hráčské	hráčský	k2eAgFnSc3d1	hráčská
vášni	vášeň	k1gFnSc3	vášeň
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
štěstí	štěstí	k1gNnSc2	štěstí
nějak	nějak	k6eAd1	nějak
vynutit	vynutit	k5eAaPmF	vynutit
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgMnSc4	ten
rovněž	rovněž	k9	rovněž
masově	masově	k6eAd1	masově
využívají	využívat	k5eAaImIp3nP	využívat
různé	různý	k2eAgFnPc1d1	různá
sázky	sázka	k1gFnPc1	sázka
<g/>
,	,	kIx,	,
loterie	loterie	k1gFnPc1	loterie
a	a	k8xC	a
hazardní	hazardní	k2eAgFnPc1d1	hazardní
hry	hra	k1gFnPc1	hra
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
zdánlivě	zdánlivě	k6eAd1	zdánlivě
nabízejí	nabízet	k5eAaImIp3nP	nabízet
"	"	kIx"	"
<g/>
štěstí	štěstí	k1gNnSc4	štěstí
<g/>
"	"	kIx"	"
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
velké	velký	k2eAgFnSc2d1	velká
výhry	výhra	k1gFnSc2	výhra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Novější	nový	k2eAgNnSc1d2	novější
myšlení	myšlení	k1gNnSc1	myšlení
sice	sice	k8xC	sice
uznává	uznávat	k5eAaImIp3nS	uznávat
nečekanou	čekaný	k2eNgFnSc4d1	nečekaná
a	a	k8xC	a
nezaslouženou	zasloužený	k2eNgFnSc4d1	nezasloužená
povahu	povaha	k1gFnSc4	povaha
štěstí	štěstí	k1gNnSc2	štěstí
<g/>
,	,	kIx,	,
varuje	varovat	k5eAaImIp3nS	varovat
však	však	k9	však
před	před	k7c7	před
představou	představa	k1gFnSc7	představa
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
dalo	dát	k5eAaPmAgNnS	dát
vynutit	vynutit	k5eAaPmF	vynutit
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Francouzský	francouzský	k2eAgMnSc1d1	francouzský
spisovatel	spisovatel	k1gMnSc1	spisovatel
André	André	k1gMnSc1	André
Gide	Gid	k1gMnSc4	Gid
si	se	k3xPyFc3	se
v	v	k7c6	v
"	"	kIx"	"
<g/>
Deníku	deník	k1gInSc6	deník
<g/>
"	"	kIx"	"
poznamenal	poznamenat	k5eAaPmAgMnS	poznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
==	==	k?	==
Štěstí	štěstí	k1gNnSc1	štěstí
jako	jako	k8xS	jako
předmět	předmět	k1gInSc1	předmět
vědeckého	vědecký	k2eAgNnSc2d1	vědecké
zkoumání	zkoumání	k1gNnSc2	zkoumání
==	==	k?	==
</s>
</p>
<p>
<s>
Vědecký	vědecký	k2eAgInSc1d1	vědecký
výzkum	výzkum	k1gInSc1	výzkum
štěstí	štěstí	k1gNnSc2	štěstí
se	se	k3xPyFc4	se
vede	vést	k5eAaImIp3nS	vést
zejména	zejména	k9	zejména
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
liniích	linie	k1gFnPc6	linie
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Srovnávací	srovnávací	k2eAgInPc1d1	srovnávací
psychologické	psychologický	k2eAgInPc1d1	psychologický
výzkumy	výzkum	k1gInPc1	výzkum
pocitu	pocit	k1gInSc2	pocit
štěstí	štěstí	k1gNnSc1	štěstí
a	a	k8xC	a
hledání	hledání	k1gNnSc1	hledání
korelací	korelace	k1gFnPc2	korelace
s	s	k7c7	s
jinými	jiný	k2eAgFnPc7d1	jiná
veličinami	veličina	k1gFnPc7	veličina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Neurobiologický	Neurobiologický	k2eAgInSc1d1	Neurobiologický
výzkum	výzkum	k1gInSc1	výzkum
fyziologických	fyziologický	k2eAgFnPc2d1	fyziologická
příčin	příčina	k1gFnPc2	příčina
a	a	k8xC	a
podmínek	podmínka	k1gFnPc2	podmínka
štěstí	štěstit	k5eAaImIp3nP	štěstit
<g/>
.	.	kIx.	.
<g/>
Jakkoli	jakkoli	k8xS	jakkoli
výsledky	výsledek	k1gInPc1	výsledek
nejsou	být	k5eNaImIp3nP	být
zatím	zatím	k6eAd1	zatím
zdaleka	zdaleka	k6eAd1	zdaleka
jednoznačné	jednoznačný	k2eAgNnSc1d1	jednoznačné
<g/>
,	,	kIx,	,
aspoň	aspoň	k9	aspoň
některé	některý	k3yIgFnPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
vysokou	vysoký	k2eAgFnSc4d1	vysoká
korelaci	korelace	k1gFnSc4	korelace
štěstí	štěstí	k1gNnSc2	štěstí
s	s	k7c7	s
náboženskou	náboženský	k2eAgFnSc7d1	náboženská
vírou	víra	k1gFnSc7	víra
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
v	v	k7c6	v
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
kdežto	kdežto	k8xS	kdežto
podle	podle	k7c2	podle
neurobiologických	urobiologický	k2eNgNnPc2d1	urobiologický
zkoumání	zkoumání	k1gNnPc2	zkoumání
je	být	k5eAaImIp3nS	být
šance	šance	k1gFnPc4	šance
na	na	k7c4	na
štěstí	štěstí	k1gNnSc4	štěstí
z	z	k7c2	z
poloviny	polovina	k1gFnSc2	polovina
vrozená	vrozený	k2eAgFnSc1d1	vrozená
<g/>
,	,	kIx,	,
asi	asi	k9	asi
z	z	k7c2	z
15	[number]	k4	15
%	%	kIx~	%
vysvětlitelná	vysvětlitelný	k2eAgFnSc1d1	vysvětlitelná
vnějšími	vnější	k2eAgFnPc7d1	vnější
okolnostmi	okolnost	k1gFnPc7	okolnost
(	(	kIx(	(
<g/>
zdraví	zdraví	k1gNnSc4	zdraví
<g/>
,	,	kIx,	,
blahobyt	blahobyt	k1gInSc4	blahobyt
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
a	a	k8xC	a
zbytek	zbytek	k1gInSc1	zbytek
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
individuální	individuální	k2eAgNnSc1d1	individuální
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Magnetická	magnetický	k2eAgFnSc1d1	magnetická
rezonance	rezonance	k1gFnSc1	rezonance
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
na	na	k7c4	na
souvislost	souvislost	k1gFnSc4	souvislost
pocitu	pocit	k1gInSc2	pocit
štěstí	štěstit	k5eAaImIp3nS	štěstit
s	s	k7c7	s
oblastí	oblast	k1gFnSc7	oblast
mozku	mozek	k1gInSc2	mozek
zvané	zvaný	k2eAgInPc4d1	zvaný
precuneus	precuneus	k1gInSc4	precuneus
(	(	kIx(	(
<g/>
v	v	k7c6	v
temenním	temenní	k2eAgInSc6d1	temenní
laloku	lalok	k1gInSc6	lalok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
E.	E.	kA	E.
Fink	Fink	k1gMnSc1	Fink
<g/>
,	,	kIx,	,
Oáza	oáza	k1gFnSc1	oáza
štěstí	štěstí	k1gNnSc2	štěstí
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
1992	[number]	k4	1992
</s>
</p>
<p>
<s>
R.	R.	kA	R.
Spaemann	Spaemann	k1gNnSc1	Spaemann
<g/>
,	,	kIx,	,
Štěstí	štěstí	k1gNnSc1	štěstí
a	a	k8xC	a
vůle	vůle	k1gFnSc1	vůle
k	k	k7c3	k
dobru	dobro	k1gNnSc3	dobro
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Oikúmené	Oikúmený	k2eAgFnSc6d1	Oikúmený
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86005	[number]	k4	86005
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Talisman	talisman	k1gInSc1	talisman
</s>
</p>
<p>
<s>
Flow	Flow	k?	Flow
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
štěstí	štěstí	k1gNnSc2	štěstí
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
štěstí	štěstí	k1gNnSc2	štěstí
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Téma	téma	k1gNnSc1	téma
Štěstí	štěstit	k5eAaImIp3nS	štěstit
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
<g/>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
History	Histor	k1gInPc1	Histor
of	of	k?	of
Happiness	Happiness	k1gInSc1	Happiness
–	–	k?	–
přehled	přehled	k1gInSc1	přehled
významných	významný	k2eAgFnPc2d1	významná
teorií	teorie	k1gFnPc2	teorie
štěstí	štěstí	k1gNnSc2	štěstí
</s>
</p>
<p>
<s>
The	The	k?	The
World	World	k1gMnSc1	World
Database	Databasa	k1gFnSc3	Databasa
of	of	k?	of
Happiness	Happiness	k1gInSc1	Happiness
–	–	k?	–
přehled	přehled	k1gInSc1	přehled
výzkumů	výzkum	k1gInPc2	výzkum
o	o	k7c6	o
subjektivním	subjektivní	k2eAgInSc6d1	subjektivní
pocitu	pocit	k1gInSc6	pocit
štěstí	štěstí	k1gNnSc2	štěstí
</s>
</p>
<p>
<s>
Stanford	Stanford	k1gMnSc1	Stanford
encyclopedia	encyclopedium	k1gNnSc2	encyclopedium
of	of	k?	of
philosophy	philosoph	k1gInPc1	philosoph
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
Pleasure	Pleasur	k1gMnSc5	Pleasur
</s>
</p>
<p>
<s>
Coaching	Coaching	k1gInSc1	Coaching
to	ten	k3xDgNnSc4	ten
happiness	happiness	k6eAd1	happiness
<g/>
,	,	kIx,	,
Happiness	Happiness	k1gInSc1	Happiness
inventory	inventor	k1gInPc4	inventor
</s>
</p>
