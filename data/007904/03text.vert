<s>
Božena	Božena	k1gFnSc1	Božena
je	být	k5eAaImIp3nS	být
ženské	ženský	k2eAgNnSc4d1	ženské
křestní	křestní	k2eAgNnSc4d1	křestní
jméno	jméno	k1gNnSc4	jméno
slovanského	slovanský	k2eAgInSc2d1	slovanský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
českého	český	k2eAgInSc2d1	český
kalendáře	kalendář	k1gInSc2	kalendář
má	mít	k5eAaImIp3nS	mít
svátek	svátek	k1gInSc1	svátek
11	[number]	k4	11
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
česká	český	k2eAgFnSc1d1	Česká
varianta	varianta	k1gFnSc1	varianta
jmen	jméno	k1gNnPc2	jméno
Benedikta	Benedikt	k1gMnSc4	Benedikt
<g/>
,	,	kIx,	,
Teodora	Teodor	k1gMnSc4	Teodor
<g/>
,	,	kIx,	,
Beatrice	Beatrice	k1gFnSc1	Beatrice
<g/>
,	,	kIx,	,
Beáta	Beáta	k1gFnSc1	Beáta
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
"	"	kIx"	"
<g/>
blahoslavená	blahoslavený	k2eAgFnSc1d1	blahoslavená
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
bohem	bůh	k1gMnSc7	bůh
obdařená	obdařený	k2eAgNnPc1d1	obdařené
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgFnSc1d1	následující
tabulka	tabulka	k1gFnSc1	tabulka
uvádí	uvádět	k5eAaImIp3nS	uvádět
četnost	četnost	k1gFnSc4	četnost
jména	jméno	k1gNnSc2	jméno
v	v	k7c6	v
ČR	ČR	kA	ČR
a	a	k8xC	a
pořadí	pořadí	k1gNnSc2	pořadí
mezi	mezi	k7c7	mezi
ženskými	ženský	k2eAgNnPc7d1	ženské
jmény	jméno	k1gNnPc7	jméno
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
dvou	dva	k4xCgInPc2	dva
roků	rok	k1gInPc2	rok
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yRgInPc4	který
jsou	být	k5eAaImIp3nP	být
dostupné	dostupný	k2eAgInPc1d1	dostupný
údaje	údaj	k1gInPc1	údaj
MV	MV	kA	MV
ČR	ČR	kA	ČR
–	–	k?	–
lze	lze	k6eAd1	lze
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
tedy	tedy	k9	tedy
vysledovat	vysledovat	k5eAaPmF	vysledovat
trend	trend	k1gInSc4	trend
v	v	k7c6	v
užívání	užívání	k1gNnSc6	užívání
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
<g/>
:	:	kIx,	:
Změna	změna	k1gFnSc1	změna
procentního	procentní	k2eAgNnSc2d1	procentní
zastoupení	zastoupení	k1gNnSc2	zastoupení
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
mezi	mezi	k7c7	mezi
žijícími	žijící	k2eAgFnPc7d1	žijící
ženami	žena	k1gFnPc7	žena
v	v	k7c6	v
ČR	ČR	kA	ČR
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
procentní	procentní	k2eAgFnSc1d1	procentní
změna	změna	k1gFnSc1	změna
se	se	k3xPyFc4	se
započítáním	započítání	k1gNnSc7	započítání
celkového	celkový	k2eAgInSc2d1	celkový
úbytku	úbytek	k1gInSc2	úbytek
žen	žena	k1gFnPc2	žena
v	v	k7c6	v
ČR	ČR	kA	ČR
za	za	k7c4	za
sledované	sledovaný	k2eAgInPc4d1	sledovaný
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
1999	[number]	k4	1999
<g/>
-	-	kIx~	-
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
-5,9	-5,9	k4	-5,9
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
svědčí	svědčit	k5eAaImIp3nS	svědčit
o	o	k7c6	o
poměrně	poměrně	k6eAd1	poměrně
značném	značný	k2eAgInSc6d1	značný
poklesu	pokles	k1gInSc6	pokles
obliby	obliba	k1gFnSc2	obliba
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Božena	Božena	k1gFnSc1	Božena
(	(	kIx(	(
<g/>
kněžna	kněžna	k1gFnSc1	kněžna
<g/>
)	)	kIx)	)
–	–	k?	–
druhá	druhý	k4xOgFnSc1	druhý
manželka	manželka	k1gFnSc1	manželka
knížete	kníže	k1gMnSc2	kníže
Oldřicha	Oldřich	k1gMnSc2	Oldřich
Božena	Božena	k1gFnSc1	Božena
Benešová	Benešová	k1gFnSc1	Benešová
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
básnířka	básnířka	k1gFnSc1	básnířka
a	a	k8xC	a
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
Božena	Božena	k1gFnSc1	Božena
Bobková	Bobková	k1gFnSc1	Bobková
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
a	a	k8xC	a
československá	československý	k2eAgFnSc1d1	Československá
politička	politička	k1gFnSc1	politička
Božena	Božena	k1gFnSc1	Božena
Brodská	Brodská	k1gFnSc1	Brodská
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
tanečnice	tanečnice	k1gFnSc1	tanečnice
<g/>
,	,	kIx,	,
historička	historička	k1gFnSc1	historička
baletu	balet	k1gInSc2	balet
a	a	k8xC	a
pedagožka	pedagožka	k1gFnSc1	pedagožka
dějin	dějiny	k1gFnPc2	dějiny
baletu	balet	k1gInSc2	balet
na	na	k7c6	na
AMU	AMU	kA	AMU
Božena	Božena	k1gFnSc1	Božena
Česká	český	k2eAgFnSc1d1	Česká
–	–	k?	–
dcera	dcera	k1gFnSc1	dcera
Václava	Václava	k1gFnSc1	Václava
I.	I.	kA	I.
Božena	Božena	k1gFnSc1	Božena
Fialová	Fialová	k1gFnSc1	Fialová
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
československá	československý	k2eAgFnSc1d1	Československá
politička	politička	k1gFnSc1	politička
Božena	Božena	k1gFnSc1	Božena
Folkmanová	Folkmanová	k1gFnSc1	Folkmanová
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
zooložka	zooložka	k1gFnSc1	zooložka
a	a	k8xC	a
parazitoložka	parazitoložka	k1gFnSc1	parazitoložka
Božena	Božena	k1gFnSc1	Božena
Fuchsová	Fuchsová	k1gFnSc1	Fuchsová
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
a	a	k8xC	a
československá	československý	k2eAgFnSc1d1	Československá
politička	politička	k1gFnSc1	politička
Božena	Božena	k1gFnSc1	Božena
Fuková	Fuková	k1gFnSc1	Fuková
–	–	k?	–
slovenská	slovenský	k2eAgFnSc1d1	slovenská
a	a	k8xC	a
československá	československý	k2eAgFnSc1d1	Československá
ekonomka	ekonomka	k1gFnSc1	ekonomka
Božena	Božena	k1gFnSc1	Božena
Hašplová	Hašplová	k1gFnSc1	Hašplová
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
a	a	k8xC	a
československá	československý	k2eAgFnSc1d1	Československá
politička	politička	k1gFnSc1	politička
Božena	Božena	k1gFnSc1	Božena
Havlová	Havlová	k1gFnSc1	Havlová
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
výtvarnice	výtvarnice	k1gFnSc1	výtvarnice
a	a	k8xC	a
módní	módní	k2eAgFnSc1d1	módní
návrhářka	návrhářka	k1gFnSc1	návrhářka
<g/>
,	,	kIx,	,
matka	matka	k1gFnSc1	matka
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
Božena	Božena	k1gFnSc1	Božena
Holečková	Holečková	k1gFnSc1	Holečková
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
a	a	k8xC	a
československá	československý	k2eAgFnSc1d1	Československá
<g />
.	.	kIx.	.
</s>
<s>
politička	politička	k1gFnSc1	politička
Božena	Božena	k1gFnSc1	Božena
Hollerová	Hollerová	k1gFnSc1	Hollerová
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
a	a	k8xC	a
československá	československý	k2eAgFnSc1d1	Československá
politička	politička	k1gFnSc1	politička
Božena	Božena	k1gFnSc1	Božena
Jelínková-Jirásková	Jelínková-Jirásková	k1gFnSc1	Jelínková-Jirásková
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
malířka	malířka	k1gFnSc1	malířka
Božena	Božena	k1gFnSc1	Božena
Kacerovská	Kacerovský	k2eAgFnSc1d1	Kacerovská
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
operní	operní	k2eAgFnSc1d1	operní
pěvkyně	pěvkyně	k1gFnSc1	pěvkyně
a	a	k8xC	a
hudební	hudební	k2eAgFnSc1d1	hudební
pedagožka	pedagožka	k1gFnSc1	pedagožka
Božena	Božena	k1gFnSc1	Božena
Kamenická	Kamenická	k1gFnSc1	Kamenická
–	–	k?	–
lidová	lidový	k2eAgFnSc1d1	lidová
léčitelka	léčitelka	k1gFnSc1	léčitelka
a	a	k8xC	a
bylinkářka	bylinkářka	k1gFnSc1	bylinkářka
Božena	Božena	k1gFnSc1	Božena
Kocinová	Kocinová	k1gFnSc1	Kocinová
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
a	a	k8xC	a
československá	československý	k2eAgFnSc1d1	Československá
politička	politička	k1gFnSc1	politička
Božena	Božena	k1gFnSc1	Božena
Komárková	Komárková	k1gFnSc1	Komárková
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
filozofka	filozofka	k1gFnSc1	filozofka
<g/>
,	,	kIx,	,
pedagožka	pedagožka	k1gFnSc1	pedagožka
a	a	k8xC	a
teoložka	teoložka	k1gFnSc1	teoložka
Božena	Božena	k1gFnSc1	Božena
Kremláčková	Kremláčková	k1gFnSc1	Kremláčková
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
<g />
.	.	kIx.	.
</s>
<s>
veterinářka	veterinářka	k1gFnSc1	veterinářka
a	a	k8xC	a
filozofka	filozofka	k1gFnSc1	filozofka
Božena	Božena	k1gFnSc1	Božena
Kudláčková	Kudláčková	k1gFnSc1	Kudláčková
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
a	a	k8xC	a
československá	československý	k2eAgFnSc1d1	Československá
politička	politička	k1gFnSc1	politička
Božena	Božena	k1gFnSc1	Božena
Kuklová-Jíšová	Kuklová-Jíšová	k1gFnSc1	Kuklová-Jíšová
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
<g/>
,	,	kIx,	,
básnířka	básnířka	k1gFnSc1	básnířka
a	a	k8xC	a
politická	politický	k2eAgFnSc1d1	politická
vězenkyně	vězenkyně	k1gFnSc1	vězenkyně
Božena	Božena	k1gFnSc1	Božena
Kuklová-Štúrová	Kuklová-Štúrová	k1gFnSc1	Kuklová-Štúrová
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
lékařka	lékařka	k1gFnSc1	lékařka
Božena	Božena	k1gFnSc1	Božena
Laglerová	Laglerová	k1gFnSc1	Laglerová
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
pilotka	pilotka	k1gFnSc1	pilotka
Božena	Božena	k1gFnSc1	Božena
Lyčková	Lyčková	k1gFnSc1	Lyčková
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
a	a	k8xC	a
československá	československý	k2eAgFnSc1d1	Československá
politička	politička	k1gFnSc1	politička
Božena	Božena	k1gFnSc1	Božena
Machačová-Dostálová	Machačová-Dostálový	k2eAgFnSc1d1	Machačová-Dostálová
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
a	a	k8xC	a
československá	československý	k2eAgFnSc1d1	Československá
politička	politička	k1gFnSc1	politička
Božena	Božena	k1gFnSc1	Božena
Marečková	Marečková	k1gFnSc1	Marečková
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
československá	československý	k2eAgFnSc1d1	Československá
politička	politička	k1gFnSc1	politička
Božena	Božena	k1gFnSc1	Božena
Miklošovičová	Miklošovičová	k1gFnSc1	Miklošovičová
–	–	k?	–
československá	československý	k2eAgFnSc1d1	Československá
baskatbalistka	baskatbalistka	k1gFnSc1	baskatbalistka
Božena	Božena	k1gFnSc1	Božena
Němcová	Němcová	k1gFnSc1	Němcová
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
Božena	Božena	k1gFnSc1	Božena
Novotná	Novotná	k1gFnSc1	Novotná
–	–	k?	–
manželka	manželka	k1gFnSc1	manželka
československého	československý	k2eAgMnSc2d1	československý
prezidenta	prezident	k1gMnSc2	prezident
Antonína	Antonín	k1gMnSc2	Antonín
Novotného	Novotný	k1gMnSc2	Novotný
Božena	Božena	k1gFnSc1	Božena
Pacáková-Hošťálková	Pacáková-Hošťálková	k1gFnSc1	Pacáková-Hošťálková
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
odbornice	odbornice	k1gFnSc1	odbornice
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
zahradní	zahradní	k2eAgFnSc2d1	zahradní
architektury	architektura	k1gFnSc2	architektura
a	a	k8xC	a
publicistka	publicistka	k1gFnSc1	publicistka
Božena	Božena	k1gFnSc1	Božena
Pátková	Pátková	k1gFnSc1	Pátková
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
a	a	k8xC	a
československá	československý	k2eAgFnSc1d1	Československá
politička	politička	k1gFnSc1	politička
Božena	Božena	k1gFnSc1	Božena
Portová	Portová	k1gFnSc1	Portová
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Božena	Božena	k1gFnSc1	Božena
Procházková	procházkový	k2eAgFnSc1d1	Procházková
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
a	a	k8xC	a
československá	československý	k2eAgFnSc1d1	Československá
politička	politička	k1gFnSc1	politička
<g />
.	.	kIx.	.
</s>
<s>
Božena	Božena	k1gFnSc1	Božena
Pýchová	Pýchový	k2eAgFnSc1d1	Pýchová
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
textilní	textilní	k2eAgFnSc1d1	textilní
výtvarnice	výtvarnice	k1gFnSc1	výtvarnice
Božena	Božena	k1gFnSc1	Božena
Sekaninová	Sekaninová	k1gFnSc1	Sekaninová
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
politička	politička	k1gFnSc1	politička
Božena	Božena	k1gFnSc1	Božena
Slabejová	Slabejový	k2eAgFnSc1d1	Slabejová
–	–	k?	–
slovenská	slovenský	k2eAgFnSc1d1	slovenská
herečka	herečka	k1gFnSc1	herečka
Božena	Božena	k1gFnSc1	Božena
Slančíková-Timrava	Slančíková-Timrava	k1gFnSc1	Slančíková-Timrava
–	–	k?	–
slovenská	slovenský	k2eAgFnSc1d1	slovenská
spisovatelka-prozaička	spisovatelkarozaička	k1gFnSc1	spisovatelka-prozaička
a	a	k8xC	a
dramatička	dramatička	k1gFnSc1	dramatička
Božena	Božena	k1gFnSc1	Božena
Správcová	správcová	k1gFnSc1	správcová
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
Božena	Božena	k1gFnSc1	Božena
Srncová	Srncová	k1gFnSc1	Srncová
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
sportovní	sportovní	k2eAgFnSc1d1	sportovní
gymnastka	gymnastka	k1gFnSc1	gymnastka
Božena	Božena	k1gFnSc1	Božena
Steinerová	Steinerová	k1gFnSc1	Steinerová
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
klavíristka	klavíristka	k1gFnSc1	klavíristka
a	a	k8xC	a
hudební	hudební	k2eAgFnSc1d1	hudební
pedagožka	pedagožka	k1gFnSc1	pedagožka
Božena	Božena	k1gFnSc1	Božena
Studničková	Studničková	k1gFnSc1	Studničková
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
a	a	k8xC	a
básnířka	básnířka	k1gFnSc1	básnířka
Božena	Božena	k1gFnSc1	Božena
<g />
.	.	kIx.	.
</s>
<s>
Šebestovská	Šebestovský	k2eAgFnSc1d1	Šebestovský
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
lidových	lidový	k2eAgFnPc2d1	lidová
písní	píseň	k1gFnPc2	píseň
Božena	Božena	k1gFnSc1	Božena
Šimečková	Šimečková	k1gFnSc1	Šimečková
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
a	a	k8xC	a
československá	československý	k2eAgFnSc1d1	Československá
politička	politička	k1gFnSc1	politička
Božena	Božena	k1gFnSc1	Božena
Šimková	Šimková	k1gFnSc1	Šimková
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
scénáristka	scénáristka	k1gFnSc1	scénáristka
a	a	k8xC	a
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
Božena	Božena	k1gFnSc1	Božena
Šustrová	Šustrová	k1gFnSc1	Šustrová
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
herečka	herečka	k1gFnSc1	herečka
Božena	Božena	k1gFnSc1	Božena
Viková-Kunětická	Viková-Kunětický	k2eAgFnSc1d1	Viková-Kunětická
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
nacionalistická	nacionalistický	k2eAgFnSc1d1	nacionalistická
politička	politička	k1gFnSc1	politička
a	a	k8xC	a
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
Božena	Božena	k1gFnSc1	Božena
Weleková	Weleková	k1gFnSc1	Weleková
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
herečka	herečka	k1gFnSc1	herečka
a	a	k8xC	a
loutkoherečka	loutkoherečka	k1gFnSc1	loutkoherečka
Božena	Božena	k1gFnSc1	Božena
Zapletalová	Zapletalová	k1gFnSc1	Zapletalová
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
a	a	k8xC	a
československá	československý	k2eAgFnSc1d1	Československá
politička	politička	k1gFnSc1	politička
Bozena	Bozen	k2eAgFnSc1d1	Bozena
–	–	k?	–
opereta	opereta	k1gFnSc1	opereta
Oscara	Oscara	k1gFnSc1	Oscara
Strause	Strause	k1gFnSc1	Strause
(	(	kIx(	(
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1952	[number]	k4	1952
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
<g/>
)	)	kIx)	)
Božena	Božena	k1gFnSc1	Božena
–	–	k?	–
hrdinka	hrdinka	k1gFnSc1	hrdinka
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
novely	novela	k1gFnSc2	novela
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1876	[number]	k4	1876
od	od	k7c2	od
Marie	Maria	k1gFnSc2	Maria
von	von	k1gInSc1	von
Ebner-Eschenbachové	Ebner-Eschenbachové	k2eAgInSc1d1	Ebner-Eschenbachové
Seznam	seznam	k1gInSc1	seznam
článků	článek	k1gInPc2	článek
začínajících	začínající	k2eAgInPc2d1	začínající
na	na	k7c4	na
"	"	kIx"	"
<g/>
Božena	Božena	k1gFnSc1	Božena
<g/>
"	"	kIx"	"
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Božena	Božena	k1gFnSc1	Božena
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
