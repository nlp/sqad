<p>
<s>
Dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
lov	lov	k1gInSc1	lov
(	(	kIx(	(
<g/>
1966	[number]	k4	1966
<g/>
,	,	kIx,	,
La	la	k1gNnSc1	la
longue	longu	k1gInSc2	longu
chasse	chasse	k1gFnSc2	chasse
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
dobrodružný	dobrodružný	k2eAgInSc4d1	dobrodružný
westernový	westernový	k2eAgInSc4d1	westernový
román	román	k1gInSc4	román
pro	pro	k7c4	pro
mládež	mládež	k1gFnSc4	mládež
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
napsal	napsat	k5eAaPmAgMnS	napsat
francouzský	francouzský	k2eAgMnSc1d1	francouzský
spisovatel	spisovatel	k1gMnSc1	spisovatel
Pierre	Pierr	k1gInSc5	Pierr
Pelot	pelota	k1gFnPc2	pelota
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obsah	obsah	k1gInSc1	obsah
románu	román	k1gInSc2	román
==	==	k?	==
</s>
</p>
<p>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
postavou	postava	k1gFnSc7	postava
příběhu	příběh	k1gInSc2	příběh
<g/>
,	,	kIx,	,
odehrávajícího	odehrávající	k2eAgMnSc2d1	odehrávající
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1829	[number]	k4	1829
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dřevorubec	dřevorubec	k1gMnSc1	dřevorubec
Zakyr	Zakyr	k1gMnSc1	Zakyr
Olk	Olk	k1gMnSc1	Olk
<g/>
,	,	kIx,	,
žijící	žijící	k2eAgFnSc1d1	žijící
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
rodinou	rodina	k1gFnSc7	rodina
ve	v	k7c6	v
srubu	srub	k1gInSc6	srub
v	v	k7c6	v
osadě	osada	k1gFnSc6	osada
South	South	k1gMnSc1	South
Easter	Easter	k1gMnSc1	Easter
v	v	k7c6	v
Ohiu	Ohio	k1gNnSc6	Ohio
<g/>
.	.	kIx.	.
</s>
<s>
Jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
však	však	k9	však
obrovitý	obrovitý	k2eAgMnSc1d1	obrovitý
medvěd	medvěd	k1gMnSc1	medvěd
grizzly	grizzly	k1gMnSc1	grizzly
<g/>
,	,	kIx,	,
přezdívaný	přezdívaný	k2eAgMnSc1d1	přezdívaný
Devilpaw	Devilpaw	k1gMnSc1	Devilpaw
(	(	kIx(	(
<g/>
Ďáblova	ďáblův	k2eAgFnSc1d1	Ďáblova
tlapa	tlapa	k1gFnSc1	tlapa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zabije	zabít	k5eAaPmIp3nS	zabít
jeho	jeho	k3xOp3gFnSc4	jeho
ženu	žena	k1gFnSc4	žena
Tiu	Tiu	k1gMnSc2	Tiu
i	i	k8xC	i
jeho	jeho	k3xOp3gMnSc2	jeho
otce	otec	k1gMnSc2	otec
Jedediáše	Jedediáš	k1gMnSc2	Jedediáš
a	a	k8xC	a
samotnému	samotný	k2eAgMnSc3d1	samotný
Zakyrovi	Zakyr	k1gMnSc3	Zakyr
přerazí	přerazit	k5eAaPmIp3nP	přerazit
nohu	noha	k1gFnSc4	noha
<g/>
.	.	kIx.	.
</s>
<s>
Zakyr	Zakyr	k1gInSc1	Zakyr
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
chvíle	chvíle	k1gFnSc2	chvíle
žije	žít	k5eAaImIp3nS	žít
jen	jen	k9	jen
pomstou	pomsta	k1gFnSc7	pomsta
<g/>
.	.	kIx.	.
</s>
<s>
Chce	chtít	k5eAaImIp3nS	chtít
medvěda	medvěd	k1gMnSc4	medvěd
najít	najít	k5eAaPmF	najít
a	a	k8xC	a
zabít	zabít	k5eAaPmF	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Odejde	odejít	k5eAaPmIp3nS	odejít
proto	proto	k8xC	proto
do	do	k7c2	do
nedaleké	daleký	k2eNgFnSc2d1	nedaleká
osady	osada	k1gFnSc2	osada
Ohio	Ohio	k1gNnSc1	Ohio
Spring	Spring	k1gInSc4	Spring
a	a	k8xC	a
pokouší	pokoušet	k5eAaImIp3nS	pokoušet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
sehnat	sehnat	k5eAaPmF	sehnat
společníky	společník	k1gMnPc4	společník
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
velký	velký	k2eAgInSc4d1	velký
lov	lov	k1gInSc4	lov
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
nabízí	nabízet	k5eAaImIp3nS	nabízet
za	za	k7c4	za
zabití	zabití	k1gNnSc4	zabití
medvěda	medvěd	k1gMnSc2	medvěd
tisíc	tisíc	k4xCgInSc4	tisíc
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Nikdo	nikdo	k3yNnSc1	nikdo
se	se	k3xPyFc4	se
však	však	k9	však
nechce	chtít	k5eNaImIp3nS	chtít
lovu	lov	k1gInSc2	lov
zúčastnit	zúčastnit	k5eAaPmF	zúčastnit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
o	o	k7c6	o
síle	síla	k1gFnSc6	síla
Devilpawa	Devilpaw	k1gInSc2	Devilpaw
<g/>
,	,	kIx,	,
kterého	který	k3yQgInSc2	který
se	se	k3xPyFc4	se
bojí	bát	k5eAaImIp3nP	bát
i	i	k9	i
Indiáni	Indián	k1gMnPc1	Indián
,	,	kIx,	,
kolují	kolovat	k5eAaImIp3nP	kolovat
neuvěřitelné	uvěřitelný	k2eNgFnPc1d1	neuvěřitelná
pověsti	pověst	k1gFnPc1	pověst
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
přihlásí	přihlásit	k5eAaPmIp3nP	přihlásit
čtyři	čtyři	k4xCgMnPc1	čtyři
muži	muž	k1gMnPc1	muž
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
každý	každý	k3xTgMnSc1	každý
se	se	k3xPyFc4	se
na	na	k7c4	na
výpravu	výprava	k1gFnSc4	výprava
vydává	vydávat	k5eAaImIp3nS	vydávat
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
vlastních	vlastní	k2eAgFnPc2d1	vlastní
osobních	osobní	k2eAgFnPc2d1	osobní
pohnutek	pohnutka	k1gFnPc2	pohnutka
<g/>
.	.	kIx.	.
</s>
<s>
Corrado	Corrada	k1gFnSc5	Corrada
Rocknel	Rocknel	k1gMnSc1	Rocknel
<g/>
,	,	kIx,	,
profesionální	profesionální	k2eAgMnSc1d1	profesionální
hráč	hráč	k1gMnSc1	hráč
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
po	po	k7c6	po
Zakyrových	Zakyrův	k2eAgInPc6d1	Zakyrův
penězích	peníze	k1gInPc6	peníze
<g/>
,	,	kIx,	,
traper	traper	k1gMnSc1	traper
Malkija	Malkija	k1gMnSc1	Malkija
<g/>
,	,	kIx,	,
zvaný	zvaný	k2eAgMnSc1d1	zvaný
Starý	Starý	k1gMnSc1	Starý
řečník	řečník	k1gMnSc1	řečník
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
využít	využít	k5eAaPmF	využít
Zakyrova	Zakyrův	k2eAgInSc2d1	Zakyrův
hněvu	hněv	k1gInSc2	hněv
a	a	k8xC	a
dopadnout	dopadnout	k5eAaPmF	dopadnout
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
medvěda	medvěd	k1gMnSc2	medvěd
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yRgInSc7	který
jíž	jenž	k3xRgFnSc3	jenž
dlouho	dlouho	k6eAd1	dlouho
měří	měřit	k5eAaImIp3nS	měřit
své	svůj	k3xOyFgFnPc4	svůj
síly	síla	k1gFnPc4	síla
<g/>
.	.	kIx.	.
</s>
<s>
Doprovází	doprovázet	k5eAaImIp3nS	doprovázet
je	on	k3xPp3gMnPc4	on
dva	dva	k4xCgMnPc1	dva
traperovi	traperův	k2eAgMnPc1d1	traperův
indiánští	indiánský	k2eAgMnPc1d1	indiánský
přátelé	přítel	k1gMnPc1	přítel
<g/>
,	,	kIx,	,
němý	němý	k2eAgInSc1d1	němý
Algonkin	Algonkin	k1gInSc1	Algonkin
Miši	Miš	k1gFnSc2	Miš
a	a	k8xC	a
Ah-ža-di-Atsi-Laku	Ah-žai-Atsi-Lak	k1gInSc2	Ah-ža-di-Atsi-Lak
(	(	kIx(	(
<g/>
Zabíječ	zabíječ	k1gMnSc1	zabíječ
ryb	ryba	k1gFnPc2	ryba
<g/>
)	)	kIx)	)
z	z	k7c2	z
kmene	kmen	k1gInSc2	kmen
Čerokíů	Čerokí	k1gInPc2	Čerokí
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k9	především
Zabíječ	zabíječ	k1gMnSc1	zabíječ
ryb	ryba	k1gFnPc2	ryba
Zakyra	Zakyra	k1gMnSc1	Zakyra
nenávidí	návidět	k5eNaImIp3nS	návidět
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
patřil	patřit	k5eAaImAgMnS	patřit
k	k	k7c3	k
mužům	muž	k1gMnPc3	muž
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
po	po	k7c6	po
krvavých	krvavý	k2eAgNnPc6d1	krvavé
střetnutích	střetnutí	k1gNnPc6	střetnutí
vyhnali	vyhnat	k5eAaPmAgMnP	vyhnat
indiánovy	indiánův	k2eAgMnPc4d1	indiánův
předky	předek	k1gMnPc4	předek
z	z	k7c2	z
jejich	jejich	k3xOp3gInPc2	jejich
lesů	les	k1gInPc2	les
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
ušetřili	ušetřit	k5eAaPmAgMnP	ušetřit
jen	jen	k9	jen
ženy	žena	k1gFnPc4	žena
a	a	k8xC	a
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Když	když	k8xS	když
je	on	k3xPp3gMnPc4	on
přepadne	přepadnout	k5eAaPmIp3nS	přepadnout
vlčí	vlčí	k2eAgFnSc1d1	vlčí
smečka	smečka	k1gFnSc1	smečka
<g/>
,	,	kIx,	,
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
se	se	k3xPyFc4	se
Corrado	Corrada	k1gFnSc5	Corrada
využít	využít	k5eAaPmF	využít
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
ostatní	ostatní	k2eAgMnPc1d1	ostatní
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
vlky	vlk	k1gMnPc7	vlk
odrazit	odrazit	k5eAaPmF	odrazit
<g/>
,	,	kIx,	,
a	a	k8xC	a
chce	chtít	k5eAaImIp3nS	chtít
krást	krást	k5eAaImF	krást
Zakyrovi	Zakyr	k1gMnSc3	Zakyr
peníze	peníz	k1gInPc4	peníz
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
naivně	naivně	k6eAd1	naivně
domnívá	domnívat	k5eAaImIp3nS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
má	můj	k3xOp1gFnSc1	můj
sebou	se	k3xPyFc7	se
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
přestřelce	přestřelka	k1gFnSc3	přestřelka
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yIgNnSc1	který
Zakirovi	Zakir	k1gMnSc3	Zakir
zachrání	zachránit	k5eAaPmIp3nS	zachránit
život	život	k1gInSc4	život
muž	muž	k1gMnSc1	muž
jménem	jméno	k1gNnSc7	jméno
Zach	Zach	k1gMnSc1	Zach
Čibuk	čibuk	k1gInSc1	čibuk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
jede	jet	k5eAaImIp3nS	jet
za	za	k7c7	za
výpravou	výprava	k1gFnSc7	výprava
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
připojil	připojit	k5eAaPmAgMnS	připojit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
chce	chtít	k5eAaImIp3nS	chtít
lovem	lov	k1gInSc7	lov
na	na	k7c4	na
medvěda	medvěd	k1gMnSc4	medvěd
překonat	překonat	k5eAaPmF	překonat
svůj	svůj	k3xOyFgInSc4	svůj
strach	strach	k1gInSc4	strach
z	z	k7c2	z
divokých	divoký	k2eAgNnPc2d1	divoké
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
mu	on	k3xPp3gMnSc3	on
zabránil	zabránit	k5eAaPmAgInS	zabránit
pomoci	pomoct	k5eAaPmF	pomoct
příteli	přítel	k1gMnSc3	přítel
<g/>
,	,	kIx,	,
když	když	k8xS	když
byl	být	k5eAaImAgInS	být
jimi	on	k3xPp3gMnPc7	on
napaden	napadnout	k5eAaPmNgMnS	napadnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Corradovi	Corrada	k1gMnSc3	Corrada
se	se	k3xPyFc4	se
podaří	podařit	k5eAaPmIp3nP	podařit
uprchnou	uprchnout	k5eAaPmIp3nP	uprchnout
a	a	k8xC	a
přitom	přitom	k6eAd1	přitom
zastřelí	zastřelit	k5eAaPmIp3nS	zastřelit
Mišiho	Miši	k1gMnSc4	Miši
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
však	však	k9	však
vystopován	vystopovat	k5eAaPmNgInS	vystopovat
skupinou	skupina	k1gFnSc7	skupina
Čipevajů	Čipevaj	k1gInPc2	Čipevaj
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
jej	on	k3xPp3gMnSc4	on
zabijí	zabít	k5eAaPmIp3nP	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Zakyrovi	Zakyrův	k2eAgMnPc1d1	Zakyrův
a	a	k8xC	a
ostatním	ostatní	k2eAgMnPc3d1	ostatní
se	se	k3xPyFc4	se
podaří	podařit	k5eAaPmIp3nS	podařit
Čipevajům	Čipevaj	k1gMnPc3	Čipevaj
zmizet	zmizet	k5eAaPmF	zmizet
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
pak	pak	k6eAd1	pak
Devilapawa	Devilapawum	k1gNnPc1	Devilapawum
vystopují	vystopovat	k5eAaPmIp3nP	vystopovat
<g/>
,	,	kIx,	,
Zach	Zach	k1gMnSc1	Zach
se	se	k3xPyFc4	se
na	na	k7c4	na
něho	on	k3xPp3gMnSc4	on
vrhne	vrhnout	k5eAaImIp3nS	vrhnout
s	s	k7c7	s
dýkou	dýka	k1gFnSc7	dýka
<g/>
.	.	kIx.	.
</s>
<s>
Překoná	překonat	k5eAaPmIp3nS	překonat
svůj	svůj	k3xOyFgInSc4	svůj
strach	strach	k1gInSc4	strach
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
medvěd	medvěd	k1gMnSc1	medvěd
jej	on	k3xPp3gNnSc4	on
usmrtí	usmrtit	k5eAaPmIp3nS	usmrtit
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
zraní	zranit	k5eAaPmIp3nS	zranit
Zabíječe	zabíječ	k1gMnSc4	zabíječ
ryb	ryba	k1gFnPc2	ryba
i	i	k9	i
Malkiju	Malkiju	k1gMnPc2	Malkiju
<g/>
.	.	kIx.	.
</s>
<s>
Zakyrovi	Zakyr	k1gMnSc3	Zakyr
se	se	k3xPyFc4	se
však	však	k9	však
podaří	podařit	k5eAaPmIp3nS	podařit
medvěda	medvěd	k1gMnSc4	medvěd
sekerou	sekera	k1gFnSc7	sekera
zabít	zabít	k5eAaPmF	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Zabíječ	zabíječ	k1gMnSc1	zabíječ
ryb	ryba	k1gFnPc2	ryba
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
rozhodnut	rozhodnut	k2eAgMnSc1d1	rozhodnut
Zakira	Zakira	k1gMnSc1	Zakira
zabít	zabít	k5eAaPmF	zabít
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
neudělá	udělat	k5eNaPmIp3nS	udělat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Zakyr	Zakyr	k1gMnSc1	Zakyr
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
nad	nad	k7c7	nad
Devilpawem	Devilpaw	k1gMnSc7	Devilpaw
a	a	k8xC	a
zachránil	zachránit	k5eAaPmAgMnS	zachránit
Malkiju	Malkiju	k1gMnSc1	Malkiju
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Česká	český	k2eAgNnPc1d1	české
vydání	vydání	k1gNnPc1	vydání
==	==	k?	==
</s>
</p>
<p>
<s>
Dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
lov	lov	k1gInSc1	lov
<g/>
,	,	kIx,	,
Albatros	albatros	k1gMnSc1	albatros
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Gustav	Gustav	k1gMnSc1	Gustav
Francl	Francl	k1gMnSc1	Francl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
http://www.kodovky.cz/kniha/185	[url]	k4	http://www.kodovky.cz/kniha/185
</s>
</p>
