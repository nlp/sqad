<s>
Svátek	svátek	k1gInSc4	svátek
svatého	svatý	k2eAgMnSc2d1	svatý
Valentýna	Valentýn	k1gMnSc2	Valentýn
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
také	také	k9	také
Valentýn	Valentýn	k1gMnSc1	Valentýn
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
slaví	slavit	k5eAaImIp3nP	slavit
v	v	k7c6	v
anglosaských	anglosaský	k2eAgFnPc6d1	anglosaská
zemích	zem	k1gFnPc6	zem
každoročně	každoročně	k6eAd1	každoročně
14	[number]	k4	14
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
jako	jako	k8xS	jako
svátek	svátek	k1gInSc4	svátek
lásky	láska	k1gFnSc2	láska
a	a	k8xC	a
náklonnosti	náklonnost	k1gFnSc2	náklonnost
mezi	mezi	k7c7	mezi
intimními	intimní	k2eAgMnPc7d1	intimní
partnery	partner	k1gMnPc7	partner
<g/>
.	.	kIx.	.
</s>
