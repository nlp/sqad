<s>
Palestinská	palestinský	k2eAgFnSc1d1
autonomie	autonomie	k1gFnSc1
(	(	kIx(
<g/>
arabsky	arabsky	k6eAd1
<g/>
:	:	kIx,
ا	ا	k?
ا	ا	k?
ا	ا	k?
<g/>
,	,	kIx,
As-Sulta	As-Sulta	k1gMnSc1
Al-Wataníja	Al-Wataníja	k1gMnSc1
Al-Filastíníja	Al-Filastíníja	k1gMnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
částečně	částečně	k6eAd1
samostatný	samostatný	k2eAgInSc1d1
státní	státní	k2eAgInSc1d1
útvar	útvar	k1gInSc1
skládající	skládající	k2eAgFnSc2d1
se	se	k3xPyFc4
ze	z	k7c2
Západního	západní	k2eAgInSc2d1
břehu	břeh	k1gInSc2
Jordánu	Jordán	k1gInSc2
<g/>
,	,	kIx,
označovaného	označovaný	k2eAgInSc2d1
také	také	k6eAd1
jako	jako	k9
Judea	Judea	k1gFnSc1
a	a	k8xC
Samaří	Samaří	k1gNnSc1
<g/>
,	,	kIx,
a	a	k8xC
Pásma	pásmo	k1gNnPc4
Gazy	Gaz	k2eAgFnPc4d1
<g/>
.	.	kIx.
</s>