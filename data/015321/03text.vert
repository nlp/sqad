<s>
Palestinská	palestinský	k2eAgFnSc1d1
autonomie	autonomie	k1gFnSc1
</s>
<s>
Palestinská	palestinský	k2eAgFnSc1d1
autonomie	autonomie	k1gFnSc1
د	د	k?
ف	ف	k?
<g/>
(	(	kIx(
<g/>
Dawlah	Dawlah	k1gMnSc1
Filasṭ	Filasṭ	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
vlajka	vlajka	k1gFnSc1
</s>
<s>
znak	znak	k1gInSc1
</s>
<s>
HymnaBilā	HymnaBilā	k?
(	(	kIx(
<g/>
ب	ب	k?
<g/>
)	)	kIx)
Geografie	geografie	k1gFnSc1
</s>
<s>
Poloha	poloha	k1gFnSc1
Palestiny	Palestina	k1gFnSc2
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
</s>
<s>
Ramalláh	Ramalláh	k1gInSc1
(	(	kIx(
<g/>
ر	ر	k?
ا	ا	k?
<g/>
,	,	kIx,
Rā	Rā	k1gMnSc1
Allā	Allā	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zamýšleným	zamýšlený	k2eAgNnSc7d1
hlavním	hlavní	k2eAgNnSc7d1
městem	město	k1gNnSc7
je	být	k5eAaImIp3nS
Východní	východní	k2eAgInSc1d1
Jeruzalém	Jeruzalém	k1gInSc1
Rozloha	rozloha	k1gFnSc1
</s>
<s>
6	#num#	k4
220	#num#	k4
km²	km²	k?
(	(	kIx(
<g/>
162	#num#	k4
<g/>
.	.	kIx.
na	na	k7c6
světě	svět	k1gInSc6
<g/>
)	)	kIx)
z	z	k7c2
toho	ten	k3xDgNnSc2
zanedbatelné	zanedbatelný	k2eAgFnSc2d1
%	%	kIx~
vodní	vodní	k2eAgFnSc2d1
plochy	plocha	k1gFnSc2
Nejvyšší	vysoký	k2eAgInSc1d3
bod	bod	k1gInSc1
</s>
<s>
Tal	Tal	k?
Ásúr	Ásúr	k1gInSc1
(	(	kIx(
<g/>
ت	ت	k?
ع	ع	k?
<g/>
,	,	kIx,
Tal	Tal	k1gMnSc1
'	'	kIx"
<g/>
Ā	Ā	k1gMnSc1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
1022	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
Časové	časový	k2eAgNnSc1d1
pásmo	pásmo	k1gNnSc1
</s>
<s>
+2	+2	k4
Poloha	poloha	k1gFnSc1
</s>
<s>
31	#num#	k4
<g/>
°	°	k?
<g/>
53	#num#	k4
<g/>
′	′	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
35	#num#	k4
<g/>
°	°	k?
<g/>
12	#num#	k4
<g/>
′	′	k?
v.	v.	k?
d.	d.	k?
Obyvatelstvo	obyvatelstvo	k1gNnSc1
Počet	počet	k1gInSc4
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
3	#num#	k4
889	#num#	k4
249	#num#	k4
(	(	kIx(
<g/>
123	#num#	k4
<g/>
.	.	kIx.
na	na	k7c6
světě	svět	k1gInSc6
<g/>
,	,	kIx,
odhad	odhad	k1gInSc1
2006	#num#	k4
<g/>
)	)	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
625	#num#	k4
ob.	ob.	k?
/	/	kIx~
km²	km²	k?
(	(	kIx(
<g/>
17	#num#	k4
<g/>
.	.	kIx.
na	na	k7c6
světě	svět	k1gInSc6
<g/>
)	)	kIx)
HDI	HDI	kA
</s>
<s>
▲	▲	k?
0,731	0,731	k4
(	(	kIx(
<g/>
střední	střední	k2eAgFnSc2d1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
106	#num#	k4
<g/>
.	.	kIx.
na	na	k7c6
světě	svět	k1gInSc6
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
)	)	kIx)
Jazyk	jazyk	k1gInSc1
</s>
<s>
arabština	arabština	k1gFnSc1
(	(	kIx(
<g/>
úřední	úřední	k2eAgNnSc1d1
<g/>
)	)	kIx)
Náboženství	náboženství	k1gNnSc1
</s>
<s>
islám	islám	k1gInSc1
<g/>
,	,	kIx,
křesťanství	křesťanství	k1gNnSc4
Státní	státní	k2eAgInSc1d1
útvar	útvar	k1gInSc1
Státní	státní	k2eAgInSc1d1
zřízení	zřízení	k1gNnSc4
</s>
<s>
republika	republika	k1gFnSc1
Vznik	vznik	k1gInSc1
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1994	#num#	k4
(	(	kIx(
<g/>
na	na	k7c6
základě	základ	k1gInSc6
mírových	mírový	k2eAgFnPc2d1
dohod	dohoda	k1gFnPc2
z	z	k7c2
Osla	Oslo	k1gNnSc2
<g/>
)	)	kIx)
Prezident	prezident	k1gMnSc1
</s>
<s>
Mahmúd	Mahmúd	k1gInSc1
Abbás	Abbás	k1gInSc1
(	(	kIx(
<g/>
م	م	k?
ع	ع	k?
<g/>
,	,	kIx,
Maḥ	Maḥ	k1gInSc1
'	'	kIx"
<g/>
Abā	Abā	k1gInSc1
<g/>
)	)	kIx)
Předseda	předseda	k1gMnSc1
vlády	vláda	k1gFnSc2
</s>
<s>
Ramí	Ramí	k2eAgInSc1d1
Hamdalláh	Hamdalláh	k1gInSc1
Měna	měna	k1gFnSc1
</s>
<s>
izraelský	izraelský	k2eAgInSc1d1
šekel	šekel	k1gInSc1
<g/>
,	,	kIx,
jordánský	jordánský	k2eAgInSc1d1
dinár	dinár	k1gInSc1
(	(	kIx(
<g/>
ILS	ILS	kA
<g/>
,	,	kIx,
JOD	jod	k1gInSc1
<g/>
)	)	kIx)
Mezinárodní	mezinárodní	k2eAgFnSc1d1
identifikace	identifikace	k1gFnSc1
ISO	ISO	kA
3166-1	3166-1	k4
</s>
<s>
275	#num#	k4
PSE	pes	k1gMnSc5
PS	PS	kA
MPZ	MPZ	kA
</s>
<s>
PS	PS	kA
<g/>
,	,	kIx,
IL	IL	kA
Telefonní	telefonní	k2eAgFnSc1d1
předvolba	předvolba	k1gFnSc1
</s>
<s>
+970	+970	k4
<g/>
,	,	kIx,
+972	+972	k4
Národní	národní	k2eAgInSc1d1
TLD	TLD	kA
</s>
<s>
<g/>
ps	ps	k0
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Stát	stát	k5eAaPmF,k5eAaImF
Palestina	Palestina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Palestinská	palestinský	k2eAgFnSc1d1
autonomie	autonomie	k1gFnSc1
(	(	kIx(
<g/>
arabsky	arabsky	k6eAd1
<g/>
:	:	kIx,
ا	ا	k?
ا	ا	k?
ا	ا	k?
<g/>
,	,	kIx,
As-Sulta	As-Sulta	k1gMnSc1
Al-Wataníja	Al-Wataníja	k1gMnSc1
Al-Filastíníja	Al-Filastíníja	k1gMnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
částečně	částečně	k6eAd1
samostatný	samostatný	k2eAgInSc1d1
státní	státní	k2eAgInSc1d1
útvar	útvar	k1gInSc1
skládající	skládající	k2eAgFnSc2d1
se	se	k3xPyFc4
ze	z	k7c2
Západního	západní	k2eAgInSc2d1
břehu	břeh	k1gInSc2
Jordánu	Jordán	k1gInSc2
<g/>
,	,	kIx,
označovaného	označovaný	k2eAgInSc2d1
také	také	k6eAd1
jako	jako	k9
Judea	Judea	k1gFnSc1
a	a	k8xC
Samaří	Samaří	k1gNnSc1
<g/>
,	,	kIx,
a	a	k8xC
Pásma	pásmo	k1gNnPc4
Gazy	Gaz	k2eAgFnPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historicky	historicky	k6eAd1
pojem	pojem	k1gInSc4
Palestina	Palestina	k1gFnSc1
označoval	označovat	k5eAaImAgInS
celé	celý	k2eAgNnSc4d1
území	území	k1gNnSc4
mezi	mezi	k7c7
řekou	řeka	k1gFnSc7
Jordán	Jordán	k1gInSc1
a	a	k8xC
Středozemním	středozemní	k2eAgNnSc7d1
mořem	moře	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
většině	většina	k1gFnSc6
tohoto	tento	k3xDgNnSc2
území	území	k1gNnSc2
o	o	k7c6
rozloze	rozloha	k1gFnSc6
asi	asi	k9
15	#num#	k4
000	#num#	k4
km²	km²	k?
dnes	dnes	k6eAd1
leží	ležet	k5eAaImIp3nS
stát	stát	k5eAaImF,k5eAaPmF
Izrael	Izrael	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
zároveň	zároveň	k6eAd1
ekonomicky	ekonomicky	k6eAd1
i	i	k8xC
vojensky	vojensky	k6eAd1
kontroluje	kontrolovat	k5eAaImIp3nS
formálně	formálně	k6eAd1
autonomní	autonomní	k2eAgFnSc4d1
Palestinskou	palestinský	k2eAgFnSc4d1
autonomii	autonomie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalšími	další	k2eAgMnPc7d1
sousedy	soused	k1gMnPc7
Palestinské	palestinský	k2eAgFnSc2d1
autonomie	autonomie	k1gFnSc2
jsou	být	k5eAaImIp3nP
Egypt	Egypt	k1gInSc1
a	a	k8xC
Jordánsko	Jordánsko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většinu	většina	k1gFnSc4
ze	z	k7c2
4	#num#	k4
milionů	milion	k4xCgInPc2
obyvatel	obyvatel	k1gMnPc2
tvoří	tvořit	k5eAaImIp3nP
muslimové	muslim	k1gMnPc1
<g/>
,	,	kIx,
ale	ale	k8xC
jsou	být	k5eAaImIp3nP
zde	zde	k6eAd1
také	také	k9
významné	významný	k2eAgFnSc2d1
křesťanské	křesťanský	k2eAgFnSc2d1
komunity	komunita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úředním	úřední	k2eAgInSc7d1
jazykem	jazyk	k1gInSc7
je	být	k5eAaImIp3nS
arabština	arabština	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sídlem	sídlo	k1gNnSc7
palestinské	palestinský	k2eAgFnSc2d1
administrativy	administrativa	k1gFnSc2
je	být	k5eAaImIp3nS
Ramalláh	Ramalláh	k1gInSc1
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
Palestinci	Palestinec	k1gMnPc1
si	se	k3xPyFc3
nárokují	nárokovat	k5eAaImIp3nP
východní	východní	k2eAgInSc4d1
Jeruzalém	Jeruzalém	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vztahy	vztah	k1gInPc4
mezi	mezi	k7c7
Palestinskou	palestinský	k2eAgFnSc7d1
autonomií	autonomie	k1gFnSc7
a	a	k8xC
Izraelem	Izrael	k1gInSc7
jsou	být	k5eAaImIp3nP
poznamenány	poznamenat	k5eAaPmNgInP
vleklým	vleklý	k2eAgInSc7d1
oboustranným	oboustranný	k2eAgInSc7d1
konfliktem	konflikt	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
31	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2011	#num#	k4
je	být	k5eAaImIp3nS
členem	člen	k1gInSc7
organizace	organizace	k1gFnSc2
UNESCO	UNESCO	kA
<g/>
.	.	kIx.
29	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2012	#num#	k4
ji	on	k3xPp3gFnSc4
Valné	valný	k2eAgNnSc1d1
shromáždění	shromáždění	k1gNnSc1
OSN	OSN	kA
uznalo	uznat	k5eAaPmAgNnS
jako	jako	k8xC,k8xS
nečlenský	členský	k2eNgInSc1d1
pozorovatelský	pozorovatelský	k2eAgInSc1d1
stát	stát	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
oficiálně	oficiálně	k6eAd1
změnila	změnit	k5eAaPmAgFnS
svůj	svůj	k3xOyFgInSc4
název	název	k1gInSc4
na	na	k7c4
Stát	stát	k1gInSc4
Palestina	Palestina	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Etymologie	etymologie	k1gFnSc1
</s>
<s>
Název	název	k1gInSc1
Palestina	Palestina	k1gFnSc1
odkazuje	odkazovat	k5eAaImIp3nS
na	na	k7c4
indoevropský	indoevropský	k2eAgInSc4d1
pelištejský	pelištejský	k2eAgInSc4d1
národ	národ	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
sídlil	sídlit	k5eAaImAgInS
v	v	k7c6
jihozápadní	jihozápadní	k2eAgFnSc6d1
části	část	k1gFnSc6
biblického	biblický	k2eAgInSc2d1
Kanaánu	Kanaán	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Patrně	patrně	k6eAd1
jako	jako	k8xS,k8xC
součást	součást	k1gFnSc1
tzv.	tzv.	kA
mořských	mořský	k2eAgInPc2d1
národů	národ	k1gInPc2
sem	sem	k6eAd1
přišli	přijít	k5eAaPmAgMnP
od	od	k7c2
Středozemního	středozemní	k2eAgNnSc2d1
moře	moře	k1gNnSc2
(	(	kIx(
<g/>
nejspíše	nejspíše	k9
z	z	k7c2
Kréty	Kréta	k1gFnSc2
<g/>
)	)	kIx)
v	v	k7c6
13	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
a	a	k8xC
usadil	usadit	k5eAaPmAgMnS
se	se	k3xPyFc4
zde	zde	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pelištejci	Pelištejec	k1gMnPc1
s	s	k7c7
sebou	se	k3xPyFc7
přinesli	přinést	k5eAaPmAgMnP
mimo	mimo	k7c4
jiné	jiné	k1gNnSc4
znalost	znalost	k1gFnSc1
zpracování	zpracování	k1gNnSc2
železa	železo	k1gNnSc2
<g/>
,	,	kIx,
díky	díky	k7c3
čemuž	což	k3yRnSc3,k3yQnSc3
po	po	k7c4
dlouhou	dlouhý	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
v	v	k7c6
oblasti	oblast	k1gFnSc6
dominovali	dominovat	k5eAaImAgMnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Od	od	k7c2
jejich	jejich	k3xOp3gInSc2
příchodu	příchod	k1gInSc2
se	se	k3xPyFc4
datuje	datovat	k5eAaImIp3nS
začátek	začátek	k1gInSc1
doby	doba	k1gFnSc2
železné	železný	k2eAgFnSc2d1
v	v	k7c6
Kanaánu	Kanaán	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
termín	termín	k1gInSc4
Peliša	Pelišum	k1gNnSc2
(	(	kIx(
<g/>
Palestina	Palestina	k1gFnSc1
<g/>
)	)	kIx)
náleží	náležet	k5eAaImIp3nS
v	v	k7c6
přesném	přesný	k2eAgInSc6d1
slova	slovo	k1gNnSc2
smyslu	smysl	k1gInSc2
jen	jen	k9
pelištejskému	pelištejský	k2eAgNnSc3d1
pětiměstí	pětiměstí	k1gNnSc3
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
širší	široký	k2eAgFnSc1d2
oblast	oblast	k1gFnSc1
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
Kanaán	Kanaán	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
Palestinu	Palestina	k1gFnSc4
označuje	označovat	k5eAaImIp3nS
celou	celý	k2eAgFnSc4d1
„	„	k?
<g/>
jižní	jižní	k2eAgFnSc4d1
Sýrii	Sýrie	k1gFnSc4
<g/>
“	“	k?
(	(	kIx(
<g/>
tj.	tj.	kA
dnešní	dnešní	k2eAgInSc1d1
Izrael	Izrael	k1gInSc1
<g/>
,	,	kIx,
Palestinu	Palestina	k1gFnSc4
<g/>
,	,	kIx,
Libanon	Libanon	k1gInSc4
<g/>
,	,	kIx,
příp	příp	kA
<g/>
.	.	kIx.
Jordánsko	Jordánsko	k1gNnSc1
<g/>
)	)	kIx)
poprvé	poprvé	k6eAd1
Hérodotos	Hérodotos	k1gMnSc1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
vyhnul	vyhnout	k5eAaPmAgMnS
jiným	jiný	k2eAgMnSc7d1
<g/>
,	,	kIx,
dvojznačným	dvojznačný	k2eAgNnSc7d1
termínům	termín	k1gInPc3
<g/>
.	.	kIx.
</s>
<s>
Oficiálně	oficiálně	k6eAd1
se	se	k3xPyFc4
latinský	latinský	k2eAgInSc1d1
termín	termín	k1gInSc1
Palaestina	Palaestin	k1gMnSc2
začal	začít	k5eAaPmAgInS
používat	používat	k5eAaImF
po	po	k7c6
potlačení	potlačení	k1gNnSc1
druhého	druhý	k4xOgNnSc2
židovského	židovský	k2eAgNnSc2d1
protiřímského	protiřímský	k2eAgNnSc2d1
povstání	povstání	k1gNnSc2
v	v	k7c6
roce	rok	k1gInSc6
135	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
termín	termín	k1gInSc4
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
výraz	výraz	k1gInSc1
dominance	dominance	k1gFnSc2
a	a	k8xC
ponížení	ponížení	k1gNnSc2
nahradil	nahradit	k5eAaPmAgInS
dřívější	dřívější	k2eAgInSc1d1
název	název	k1gInSc1
Judaea	Judae	k1gInSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
vymazala	vymazat	k5eAaPmAgFnS
i	i	k9
ve	v	k7c6
jménu	jméno	k1gNnSc6
jakákoli	jakýkoli	k3yIgFnSc1
vzpomínka	vzpomínka	k1gFnSc1
na	na	k7c4
židovský	židovský	k2eAgInSc4d1
národ	národ	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
během	během	k7c2
necelého	celý	k2eNgInSc2d1
jednoho	jeden	k4xCgInSc2
století	století	k1gNnPc2
proti	proti	k7c3
Římu	Řím	k1gInSc3
dvakrát	dvakrát	k6eAd1
povstal	povstat	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Název	název	k1gInSc1
Palestina	Palestina	k1gFnSc1
se	se	k3xPyFc4
užívá	užívat	k5eAaImIp3nS
jako	jako	k9
zeměpisné	zeměpisný	k2eAgNnSc1d1
označení	označení	k1gNnSc1
území	území	k1gNnSc2
mezi	mezi	k7c7
Libanonem	Libanon	k1gInSc7
a	a	k8xC
Egyptem	Egypt	k1gInSc7
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
se	se	k3xPyFc4
na	na	k7c6
jeho	jeho	k3xOp3gNnSc6
území	území	k1gNnSc6
již	již	k9
nenachází	nacházet	k5eNaImIp3nS
jen	jen	k9
geneticky	geneticky	k6eAd1
původní	původní	k2eAgNnSc4d1
pelištejské	pelištejský	k2eAgNnSc4d1
obyvatelstvo	obyvatelstvo	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Pravěká	pravěký	k2eAgFnSc1d1
Palestina	Palestina	k1gFnSc1
</s>
<s>
Na	na	k7c6
území	území	k1gNnSc6
Palestiny	Palestina	k1gFnSc2
se	se	k3xPyFc4
objevují	objevovat	k5eAaImIp3nP
přibližně	přibližně	k6eAd1
10	#num#	k4
tisíc	tisíc	k4xCgInSc4
let	léto	k1gNnPc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
první	první	k4xOgMnPc1
protozemědělci	protozemědělec	k1gMnPc1
a	a	k8xC
lovci	lovec	k1gMnPc1
gazel	gazela	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
8	#num#	k4
tisíciletí	tisíciletí	k1gNnSc2
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
rychlému	rychlý	k2eAgInSc3d1
přechodu	přechod	k1gInSc3
k	k	k7c3
usedlému	usedlý	k2eAgNnSc3d1
zemědělství	zemědělství	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vystřídá	vystřídat	k5eAaPmIp3nS
se	se	k3xPyFc4
několik	několik	k4yIc4
málo	málo	k4c1
nám	my	k3xPp1nPc3
známých	známý	k2eAgFnPc2d1
zemědělských	zemědělský	k2eAgFnPc2d1
společností	společnost	k1gFnPc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgMnPc2
poslední	poslední	k2eAgMnPc1d1
byli	být	k5eAaImAgMnP
Kananejci	Kananejec	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s>
Starověká	starověký	k2eAgFnSc1d1
Palestina	Palestina	k1gFnSc1
</s>
<s>
Od	od	k7c2
cca	cca	kA
12	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
na	na	k7c6
území	území	k1gNnSc6
Palestiny	Palestina	k1gFnSc2
(	(	kIx(
<g/>
v	v	k7c6
obecném	obecný	k2eAgInSc6d1
zeměpisném	zeměpisný	k2eAgInSc6d1
smyslu	smysl	k1gInSc6
<g/>
)	)	kIx)
sídlily	sídlit	k5eAaImAgInP
především	především	k6eAd1
izraelské	izraelský	k2eAgInPc1d1
kmeny	kmen	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
vytvořily	vytvořit	k5eAaPmAgFnP
Izraelské	izraelský	k2eAgInPc1d1
a	a	k8xC
Judské	judský	k2eAgNnSc1d1
království	království	k1gNnSc1
<g/>
,	,	kIx,
dále	daleko	k6eAd2
již	již	k6eAd1
zmínění	zmíněný	k2eAgMnPc1d1
Pelištejci	Pelištejec	k1gMnPc1
<g/>
,	,	kIx,
Aramejské	aramejský	k2eAgNnSc1d1
království	království	k1gNnSc1
či	či	k8xC
Moab	Moab	k1gMnSc1
<g/>
,	,	kIx,
Amon	Amon	k1gMnSc1
a	a	k8xC
Edom	Edom	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Střídavě	střídavě	k6eAd1
se	se	k3xPyFc4
území	území	k1gNnSc1
dostávalo	dostávat	k5eAaImAgNnS
do	do	k7c2
vlivu	vliv	k1gInSc2
velkých	velký	k2eAgFnPc2d1
mocností	mocnost	k1gFnPc2
Asýrie	Asýrie	k1gFnSc2
<g/>
,	,	kIx,
Babylonie	Babylonie	k1gFnSc2
<g/>
,	,	kIx,
Makedonie	Makedonie	k1gFnSc2
a	a	k8xC
Egypta	Egypt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
se	se	k3xPyFc4
země	zem	k1gFnPc1
po	po	k7c6
krátkém	krátký	k2eAgNnSc6d1
období	období	k1gNnSc6
samostatného	samostatný	k2eAgInSc2d1
židovského	židovský	k2eAgInSc2d1
státu	stát	k1gInSc2
pod	pod	k7c7
vládou	vláda	k1gFnSc7
Makabejců	Makabejec	k1gMnPc2
dostala	dostat	k5eAaPmAgFnS
do	do	k7c2
moci	moc	k1gFnSc2
Říma	Řím	k1gInSc2
a	a	k8xC
později	pozdě	k6eAd2
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
římskou	římska	k1gFnSc7
provincií	provincie	k1gFnPc2
Judea	Judea	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k8xC
různé	různý	k2eAgFnPc1d1
části	část	k1gFnPc1
území	území	k1gNnSc2
byly	být	k5eAaImAgFnP
spravovány	spravován	k2eAgFnPc1d1
různými	různý	k2eAgMnPc7d1
králi	král	k1gMnPc7
<g/>
,	,	kIx,
etnarchy	etnarch	k1gMnPc7
či	či	k8xC
jinými	jiný	k2eAgFnPc7d1
pověřenými	pověřený	k2eAgFnPc7d1
osobami	osoba	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
Palestina	Palestina	k1gFnSc1
pod	pod	k7c7
vlivem	vliv	k1gInSc7
islámu	islám	k1gInSc2
</s>
<s>
Po	po	k7c6
rozpadu	rozpad	k1gInSc6
Římské	římský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
se	se	k3xPyFc4
Palestina	Palestina	k1gFnSc1
stala	stát	k5eAaPmAgFnS
součástí	součást	k1gFnSc7
Byzantské	byzantský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
<g/>
,	,	kIx,
až	až	k9
v	v	k7c6
roce	rok	k1gInSc6
636	#num#	k4
vstoupili	vstoupit	k5eAaPmAgMnP
do	do	k7c2
Jeruzaléma	Jeruzalém	k1gInSc2
muslimové	muslim	k1gMnPc1
a	a	k8xC
palestinské	palestinský	k2eAgNnSc1d1
území	území	k1gNnSc1
se	se	k3xPyFc4
dostalo	dostat	k5eAaPmAgNnS
pod	pod	k7c4
vliv	vliv	k1gInSc4
islámu	islám	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
11	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
12	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
sem	sem	k6eAd1
vedlo	vést	k5eAaImAgNnS
několik	několik	k4yIc1
křížových	křížový	k2eAgFnPc2d1
výprav	výprava	k1gFnPc2
<g/>
,	,	kIx,
křesťané	křesťan	k1gMnPc1
dobyli	dobýt	k5eAaPmAgMnP
Jeruzalém	Jeruzalém	k1gInSc4
a	a	k8xC
vyhlásili	vyhlásit	k5eAaPmAgMnP
zde	zde	k6eAd1
Jeruzalémské	jeruzalémský	k2eAgNnSc4d1
království	království	k1gNnSc4
<g/>
,	,	kIx,
z	z	k7c2
této	tento	k3xDgFnSc2
doby	doba	k1gFnSc2
také	také	k9
pocházejí	pocházet	k5eAaImIp3nP
všechny	všechen	k3xTgInPc1
izraelské	izraelský	k2eAgInPc1d1
historické	historický	k2eAgInPc1d1
hrady	hrad	k1gInPc1
stavěné	stavěný	k2eAgInPc1d1
v	v	k7c6
evropském	evropský	k2eAgInSc6d1
stylu	styl	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
Palestina	Palestina	k1gFnSc1
připadla	připadnout	k5eAaPmAgFnS
Osmanské	osmanský	k2eAgFnPc4d1
říši	říše	k1gFnSc4
a	a	k8xC
jako	jako	k9
její	její	k3xOp3gFnSc1
součást	součást	k1gFnSc1
setrvala	setrvat	k5eAaPmAgFnS
až	až	k9
do	do	k7c2
jejího	její	k3xOp3gInSc2
pádu	pád	k1gInSc2
na	na	k7c6
konci	konec	k1gInSc6
1	#num#	k4
<g/>
.	.	kIx.
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
80	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
začali	začít	k5eAaPmAgMnP
ve	v	k7c6
větším	veliký	k2eAgNnSc6d2
množství	množství	k1gNnSc6
přicházet	přicházet	k5eAaImF
Židé	Žid	k1gMnPc1
z	z	k7c2
celé	celý	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
porážce	porážka	k1gFnSc6
osmanského	osmanský	k2eAgNnSc2d1
Turecka	Turecko	k1gNnSc2
byla	být	k5eAaImAgFnS
Palestina	Palestina	k1gFnSc1
britským	britský	k2eAgInSc7d1
protektorátem	protektorát	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Moderní	moderní	k2eAgFnSc1d1
Palestina	Palestina	k1gFnSc1
a	a	k8xC
vznik	vznik	k1gInSc1
Izraele	Izrael	k1gInSc2
</s>
<s>
Od	od	k7c2
30	#num#	k4
<g/>
.	.	kIx.
let	let	k1gInSc4
minulého	minulý	k2eAgNnSc2d1
století	století	k1gNnSc2
začaly	začít	k5eAaPmAgInP
sílit	sílit	k5eAaImF
požadavky	požadavek	k1gInPc4
Židů	Žid	k1gMnPc2
na	na	k7c4
vlastní	vlastní	k2eAgInSc4d1
stát	stát	k1gInSc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
což	což	k3yQnSc1,k3yRnSc1
vedlo	vést	k5eAaImAgNnS
k	k	k7c3
arabskému	arabský	k2eAgNnSc3d1
povstání	povstání	k1gNnSc3
(	(	kIx(
<g/>
1936	#num#	k4
<g/>
-	-	kIx~
<g/>
39	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
kolem	kolem	k7c2
poloviny	polovina	k1gFnSc2
40	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
probíhaly	probíhat	k5eAaImAgFnP
série	série	k1gFnPc4
teroristických	teroristický	k2eAgInPc2d1
útoků	útok	k1gInPc2
radikálních	radikální	k2eAgFnPc2d1
sionistických	sionistický	k2eAgFnPc2d1
skupin	skupina	k1gFnPc2
proti	proti	k7c3
britské	britský	k2eAgFnSc3d1
správě	správa	k1gFnSc3
<g/>
.	.	kIx.
14	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1948	#num#	k4
byl	být	k5eAaImAgInS
vyhlášen	vyhlásit	k5eAaPmNgInS
stát	stát	k1gInSc1
Izrael	Izrael	k1gInSc1
<g/>
,	,	kIx,
podle	podle	k7c2
rozhodnutí	rozhodnutí	k1gNnSc2
Valného	valný	k2eAgNnSc2d1
shromáždění	shromáždění	k1gNnSc2
OSN	OSN	kA
z	z	k7c2
29	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1947	#num#	k4
o	o	k7c6
rozdělení	rozdělení	k1gNnSc6
mandátní	mandátní	k2eAgFnSc2d1
Palestiny	Palestina	k1gFnSc2
na	na	k7c4
dva	dva	k4xCgInPc4
státy	stát	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měl	mít	k5eAaImAgMnS
být	být	k5eAaImF
vyhlášen	vyhlásit	k5eAaPmNgMnS
i	i	k9
palestinský	palestinský	k2eAgMnSc1d1
stát	stát	k5eAaImF,k5eAaPmF
<g/>
,	,	kIx,
jenže	jenže	k8xC
na	na	k7c6
arabské	arabský	k2eAgFnSc6d1
části	část	k1gFnSc6
palestinského	palestinský	k2eAgNnSc2d1
území	území	k1gNnSc2
politická	politický	k2eAgFnSc1d1
reprezentace	reprezentace	k1gFnSc1
(	(	kIx(
<g/>
Amín	Amín	k1gMnSc1
al-Husajní	al-Husajný	k2eAgMnPc1d1
<g/>
)	)	kIx)
odmítla	odmítnout	k5eAaPmAgFnS
plán	plán	k1gInSc4
rozdělení	rozdělení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hned	hned	k6eAd1
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yInSc4,k3yQnSc4,k3yRnSc4
Izrael	Izrael	k1gInSc1
vyhlásil	vyhlásit	k5eAaPmAgInS
nezávislost	nezávislost	k1gFnSc4
<g/>
,	,	kIx,
na	na	k7c4
něj	on	k3xPp3gMnSc4
zaútočilo	zaútočit	k5eAaPmAgNnS
přibližně	přibližně	k6eAd1
1	#num#	k4
000	#num#	k4
libanonských	libanonský	k2eAgMnPc2d1
<g/>
,	,	kIx,
6	#num#	k4
000	#num#	k4
syrských	syrský	k2eAgMnPc2d1
<g/>
,	,	kIx,
4	#num#	k4
500	#num#	k4
iráckých	irácký	k2eAgMnPc2d1
<g/>
,	,	kIx,
5	#num#	k4
500	#num#	k4
egyptských	egyptský	k2eAgMnPc2d1
<g/>
,	,	kIx,
6	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
–	–	k?
<g/>
9	#num#	k4
000	#num#	k4
jordánských	jordánský	k2eAgMnPc2d1
a	a	k8xC
neznámý	známý	k2eNgInSc1d1
počet	počet	k1gInSc1
saúdskoarabských	saúdskoarabský	k2eAgFnPc2d1
a	a	k8xC
jemenských	jemenský	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Izraelská	izraelský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
<g/>
,	,	kIx,
podporovaná	podporovaný	k2eAgFnSc1d1
mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
Československem	Československo	k1gNnSc7
a	a	k8xC
Sovětským	sovětský	k2eAgInSc7d1
svazem	svaz	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
doufal	doufat	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
vznikem	vznik	k1gInSc7
Izraele	Izrael	k1gInSc2
posílí	posílit	k5eAaPmIp3nS
své	svůj	k3xOyFgInPc4
zájmy	zájem	k1gInPc4
na	na	k7c6
Blízkém	blízký	k2eAgInSc6d1
východě	východ	k1gInSc6
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
ovšem	ovšem	k9
silnější	silný	k2eAgInPc4d2
a	a	k8xC
arabské	arabský	k2eAgMnPc4d1
spojence	spojenec	k1gMnPc4
porazila	porazit	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současné	současný	k2eAgFnPc4d1
hranice	hranice	k1gFnPc4
mezi	mezi	k7c7
Izraelem	Izrael	k1gInSc7
a	a	k8xC
Palestinou	Palestina	k1gFnSc7
<g/>
,	,	kIx,
tzn.	tzn.	kA
Západní	západní	k2eAgInSc1d1
břeh	břeh	k1gInSc1
Jordánu	Jordán	k1gInSc2
a	a	k8xC
pásmo	pásmo	k1gNnSc4
Gazy	Gaza	k1gFnSc2
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
na	na	k7c6
linii	linie	k1gFnSc6
příměří	příměří	k1gNnSc2
(	(	kIx(
<g/>
tzv.	tzv.	kA
zelená	zelený	k2eAgFnSc1d1
linie	linie	k1gFnSc1
<g/>
)	)	kIx)
z	z	k7c2
roku	rok	k1gInSc2
1949	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Palestina	Palestina	k1gFnSc1
jako	jako	k8xS,k8xC
nezávislé	závislý	k2eNgNnSc1d1
území	území	k1gNnSc1
</s>
<s>
Nezávislá	závislý	k2eNgFnSc1d1
Palestina	Palestina	k1gFnSc1
je	být	k5eAaImIp3nS
formálně	formálně	k6eAd1
od	od	k7c2
roku	rok	k1gInSc2
1989	#num#	k4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
kdy	kdy	k6eAd1
se	se	k3xPyFc4
území	území	k1gNnSc1
Předjordánska	Předjordánsko	k1gNnSc2
formálně	formálně	k6eAd1
zřeklo	zřeknout	k5eAaPmAgNnS
Jordánsko	Jordánsko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
začátku	začátek	k1gInSc6
90	#num#	k4
<g/>
.	.	kIx.
let	let	k1gInSc1
zde	zde	k6eAd1
probíhal	probíhat	k5eAaImAgInS
mírový	mírový	k2eAgInSc4d1
proces	proces	k1gInSc4
<g/>
,	,	kIx,
při	při	k7c6
kterém	který	k3yRgInSc6,k3yQgInSc6,k3yIgInSc6
byla	být	k5eAaImAgFnS
pod	pod	k7c7
vedením	vedení	k1gNnSc7
Izraele	Izrael	k1gInSc2
a	a	k8xC
Jásira	Jásir	k1gMnSc2
Arafata	Arafat	k1gMnSc2
vytvářena	vytvářen	k2eAgFnSc1d1
palestinská	palestinský	k2eAgFnSc1d1
státní	státní	k2eAgFnSc1d1
správa	správa	k1gFnSc1
a	a	k8xC
vznikala	vznikat	k5eAaImAgFnS
a	a	k8xC
byla	být	k5eAaImAgFnS
vyzbrojována	vyzbrojován	k2eAgFnSc1d1
palestinská	palestinský	k2eAgFnSc1d1
policie	policie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nekončící	končící	k2eNgFnSc1d1
vojenská	vojenský	k2eAgFnSc1d1
i	i	k8xC
ekonomická	ekonomický	k2eAgFnSc1d1
kontrola	kontrola	k1gFnSc1
palestinských	palestinský	k2eAgNnPc2d1
území	území	k1gNnPc2
vedla	vést	k5eAaImAgFnS
k	k	k7c3
vystupňování	vystupňování	k1gNnSc3
teroru	teror	k1gInSc2
během	během	k7c2
druhé	druhý	k4xOgFnSc2
intifády	intifáda	k1gFnSc2
z	z	k7c2
roku	rok	k1gInSc2
2000	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
palestinští	palestinský	k2eAgMnPc1d1
teroristé	terorista	k1gMnPc1
útočili	útočit	k5eAaImAgMnP
na	na	k7c4
objekty	objekt	k1gInPc4
a	a	k8xC
obyvatele	obyvatel	k1gMnPc4
v	v	k7c6
Izraeli	Izrael	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Izraelská	izraelský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
poté	poté	k6eAd1
obsadila	obsadit	k5eAaPmAgFnS
významná	významný	k2eAgNnPc4d1
města	město	k1gNnPc4
a	a	k8xC
mírový	mírový	k2eAgInSc4d1
proces	proces	k1gInSc4
fakticky	fakticky	k6eAd1
skončil	skončit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
smrti	smrt	k1gFnSc6
Jásira	Jásir	k1gMnSc2
Arafata	Arafat	k1gMnSc2
v	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
byl	být	k5eAaImAgMnS
zvolen	zvolit	k5eAaPmNgMnS
prezidentem	prezident	k1gMnSc7
palestinské	palestinský	k2eAgFnSc2d1
samosprávy	samospráva	k1gFnSc2
Mahmúd	Mahmúd	k1gMnSc1
Abbás	Abbás	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Stát	stát	k1gInSc1
Palestina	Palestina	k1gFnSc1
</s>
<s>
Stát	stát	k1gInSc1
Palestina	Palestina	k1gFnSc1
vyhlásil	vyhlásit	k5eAaPmAgMnS
Jásir	Jásir	k1gMnSc1
Arafat	Arafat	k1gMnSc1
15	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1988	#num#	k4
v	v	k7c6
exilu	exil	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Okamžitě	okamžitě	k6eAd1
ho	on	k3xPp3gMnSc4
uznalo	uznat	k5eAaPmAgNnS
114	#num#	k4
zemí	zem	k1gFnPc2
(	(	kIx(
<g/>
hlavně	hlavně	k6eAd1
muslimských	muslimský	k2eAgMnPc2d1
a	a	k8xC
komunistických	komunistický	k2eAgMnPc2d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgMnPc2
93	#num#	k4
udržovalo	udržovat	k5eAaImAgNnS
nějakou	nějaký	k3yIgFnSc4
formu	forma	k1gFnSc4
diplomatických	diplomatický	k2eAgInPc2d1
styků	styk	k1gInPc2
s	s	k7c7
Izraelem	Izrael	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samotný	samotný	k2eAgInSc1d1
Izrael	Izrael	k1gInSc1
ovšem	ovšem	k9
takový	takový	k3xDgInSc4
stát	stát	k1gInSc4
neuznal	uznat	k5eNaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
OSN	OSN	kA
mu	on	k3xPp3gMnSc3
přiznala	přiznat	k5eAaPmAgFnS
status	status	k1gInSc4
pozorovatele	pozorovatel	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
a	a	k8xC
Izrael	Izrael	k1gInSc1
byly	být	k5eAaImAgFnP
proti	proti	k7c3
a	a	k8xC
44	#num#	k4
států	stát	k1gInPc2
se	se	k3xPyFc4
zdrželo	zdržet	k5eAaPmAgNnS
hlasování	hlasování	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navzdory	navzdory	k7c3
uznání	uznání	k1gNnSc3
státu	stát	k1gInSc3
Palestina	Palestina	k1gFnSc1
jen	jen	k9
29	#num#	k4
států	stát	k1gInPc2
v	v	k7c6
roce	rok	k1gInSc6
1995	#num#	k4
uznávalo	uznávat	k5eAaImAgNnS
„	„	k?
<g/>
palestinský	palestinský	k2eAgInSc4d1
pas	pas	k1gInSc4
<g/>
.	.	kIx.
<g/>
“	“	k?
</s>
<s>
ČSSR	ČSSR	kA
uznala	uznat	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1988	#num#	k4
vyhlášení	vyhlášení	k1gNnSc2
Státu	stát	k1gInSc2
Palestina	Palestina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
v	v	k7c6
ČSSR	ČSSR	kA
existovalo	existovat	k5eAaImAgNnS
od	od	k7c2
roku	rok	k1gInSc2
1976	#num#	k4
zastoupení	zastoupení	k1gNnSc4
Organizace	organizace	k1gFnSc2
pro	pro	k7c4
osvobození	osvobození	k1gNnSc4
Palestiny	Palestina	k1gFnSc2
(	(	kIx(
<g/>
OOP	OOP	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
1983	#num#	k4
s	s	k7c7
diplomatickým	diplomatický	k2eAgInSc7d1
statusem	status	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zastoupení	zastoupení	k1gNnSc1
Palestinské	palestinský	k2eAgFnSc2d1
národní	národní	k2eAgFnSc2d1
správy	správa	k1gFnSc2
(	(	kIx(
<g/>
PNS	PNS	kA
<g/>
)	)	kIx)
v	v	k7c6
Česku	Česko	k1gNnSc6
nese	nést	k5eAaImIp3nS
označení	označení	k1gNnSc1
Velvyslanectví	velvyslanectví	k1gNnSc2
Státu	stát	k1gInSc2
Palestina	Palestina	k1gFnSc1
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
od	od	k7c2
února	únor	k1gInSc2
roku	rok	k1gInSc2
1989	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČR	ČR	kA
uznala	uznat	k5eAaPmAgFnS
všechny	všechen	k3xTgInPc4
státy	stát	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
ke	k	k7c3
dni	den	k1gInSc3
31	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
1992	#num#	k4
uznávalo	uznávat	k5eAaImAgNnS
Československo	Československo	k1gNnSc1
a	a	k8xC
to	ten	k3xDgNnSc4
čl	čl	kA
<g/>
.	.	kIx.
5	#num#	k4
odst	odsta	k1gFnPc2
<g/>
.	.	kIx.
1	#num#	k4
ústavního	ústavní	k2eAgInSc2d1
zákona	zákon	k1gInSc2
č.	č.	k?
4	#num#	k4
<g/>
/	/	kIx~
<g/>
1993	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
opatřeních	opatření	k1gNnPc6
souvisejících	související	k2eAgFnPc2d1
se	s	k7c7
zánikem	zánik	k1gInSc7
ČSFR	ČSFR	kA
<g/>
,	,	kIx,
tedy	tedy	k8xC
i	i	k8xC
Stát	stát	k1gInSc1
Palestina	Palestina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
toleruje	tolerovat	k5eAaImIp3nS
status	status	k1gInSc4
quo	quo	k?
palestinského	palestinský	k2eAgNnSc2d1
zastoupení	zastoupení	k1gNnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
přesto	přesto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
vláda	vláda	k1gFnSc1
reciproční	reciproční	k2eAgInPc1d1
vztahy	vztah	k1gInPc1
se	s	k7c7
Státem	stát	k1gInSc7
Palestina	Palestina	k1gFnSc1
neudržuje	udržovat	k5eNaImIp3nS
<g/>
,	,	kIx,
ale	ale	k8xC
jedná	jednat	k5eAaImIp3nS
jen	jen	k9
s	s	k7c7
palestinskou	palestinský	k2eAgFnSc7d1
samosprávou	samospráva	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mírové	mírový	k2eAgFnPc4d1
dohody	dohoda	k1gFnPc4
z	z	k7c2
Osla	Oslo	k1gNnSc2
z	z	k7c2
roku	rok	k1gInSc2
1993	#num#	k4
zahraniční	zahraniční	k2eAgNnSc4d1
zastoupení	zastoupení	k1gNnSc4
PNS	PNS	kA
sice	sice	k8xC
formálně	formálně	k6eAd1
nepřipouštějí	připouštět	k5eNaImIp3nP
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
kodifikují	kodifikovat	k5eAaBmIp3nP
zachování	zachování	k1gNnSc4
úrovně	úroveň	k1gFnSc2
již	již	k6eAd1
existujících	existující	k2eAgFnPc2d1
diplomatických	diplomatický	k2eAgFnPc2d1
misí	mise	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
postoj	postoj	k1gInSc4
je	být	k5eAaImIp3nS
v	v	k7c6
souladu	soulad	k1gInSc6
s	s	k7c7
politikou	politika	k1gFnSc7
Evropské	evropský	k2eAgFnSc2d1
unie	unie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Palestina	Palestina	k1gFnSc1
dnes	dnes	k6eAd1
</s>
<s>
Palestina	Palestina	k1gFnSc1
dnes	dnes	k6eAd1
označuje	označovat	k5eAaImIp3nS
Západní	západní	k2eAgInSc4d1
břeh	břeh	k1gInSc4
Jordánu	Jordán	k1gInSc2
a	a	k8xC
Pásmo	pásmo	k1gNnSc4
Gazy	Gaza	k1gFnSc2
o	o	k7c6
rozloze	rozloha	k1gFnSc6
asi	asi	k9
6600	#num#	k4
km²	km²	k?
a	a	k8xC
3	#num#	k4
mil	míle	k1gFnPc2
<g/>
.	.	kIx.
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oficiálně	oficiálně	k6eAd1
tato	tento	k3xDgFnSc1
území	území	k1gNnSc4
spravuje	spravovat	k5eAaImIp3nS
Palestinská	palestinský	k2eAgFnSc1d1
samospráva	samospráva	k1gFnSc1
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc1
předsedou	předseda	k1gMnSc7
je	on	k3xPp3gNnPc4
od	od	k7c2
roku	rok	k1gInSc2
2005	#num#	k4
Mahmúd	Mahmúd	k1gInSc1
Abbás	Abbás	k1gInSc4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1989	#num#	k4
<g/>
–	–	k?
<g/>
2004	#num#	k4
Jásir	Jásir	k1gInSc1
Arafat	Arafat	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Území	území	k1gNnSc3
Gazy	Gaza	k1gFnSc2
je	být	k5eAaImIp3nS
pod	pod	k7c7
kontrolou	kontrola	k1gFnSc7
hnutí	hnutí	k1gNnPc2
Hamas	Hamas	k1gInSc4
a	a	k8xC
území	území	k1gNnSc4
Západního	západní	k2eAgInSc2d1
břehu	břeh	k1gInSc2
pod	pod	k7c7
kontrolou	kontrola	k1gFnSc7
Fatahu	Fatah	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Organizace	organizace	k1gFnSc1
pro	pro	k7c4
osvobození	osvobození	k1gNnSc4
Palestiny	Palestina	k1gFnSc2
je	být	k5eAaImIp3nS
hlavní	hlavní	k2eAgFnSc7d1
zastřešující	zastřešující	k2eAgFnSc7d1
<g/>
,	,	kIx,
reprezentativní	reprezentativní	k2eAgFnSc7d1
a	a	k8xC
výkonnou	výkonný	k2eAgFnSc7d1
silou	síla	k1gFnSc7
samosprávného	samosprávný	k2eAgNnSc2d1
území	území	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obyvatelé	obyvatel	k1gMnPc1
jsou	být	k5eAaImIp3nP
většinou	většina	k1gFnSc7
muslimové	muslim	k1gMnPc1
<g/>
,	,	kIx,
ale	ale	k8xC
existuje	existovat	k5eAaImIp3nS
tu	tu	k6eAd1
i	i	k9
početná	početný	k2eAgFnSc1d1
křesťanská	křesťanský	k2eAgFnSc1d1
menšina	menšina	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Problémy	problém	k1gInPc1
palestinské	palestinský	k2eAgFnSc2d1
samosprávy	samospráva	k1gFnSc2
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1
problémem	problém	k1gInSc7
současnosti	současnost	k1gFnSc2
je	být	k5eAaImIp3nS
nízký	nízký	k2eAgInSc1d1
životní	životní	k2eAgInSc1d1
standard	standard	k1gInSc1
a	a	k8xC
nezaměstnanost	nezaměstnanost	k1gFnSc1
obyvatelstva	obyvatelstvo	k1gNnSc2
(	(	kIx(
<g/>
zejména	zejména	k9
v	v	k7c6
pásmu	pásmo	k1gNnSc6
Gazy	Gaza	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Špatná	špatný	k2eAgFnSc1d1
sociální	sociální	k2eAgFnSc1d1
situace	situace	k1gFnSc1
a	a	k8xC
dlouho	dlouho	k6eAd1
trvající	trvající	k2eAgFnSc1d1
okupace	okupace	k1gFnSc1
je	být	k5eAaImIp3nS
vhodným	vhodný	k2eAgNnSc7d1
prostředím	prostředí	k1gNnSc7
k	k	k7c3
růstu	růst	k1gInSc3
náboženského	náboženský	k2eAgInSc2d1
fundamentalismu	fundamentalismus	k1gInSc2
a	a	k8xC
podhoubím	podhoubí	k1gNnSc7
terorismu	terorismus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
území	území	k1gNnSc6
palestinské	palestinský	k2eAgFnSc2d1
samosprávy	samospráva	k1gFnSc2
se	se	k3xPyFc4
nacházejí	nacházet	k5eAaImIp3nP
uprchlické	uprchlický	k2eAgInPc1d1
tábory	tábor	k1gInPc1
(	(	kIx(
<g/>
Tulkarm	Tulkarm	k1gInSc1
a	a	k8xC
Džanín	Džanín	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
z	z	k7c2
pohledu	pohled	k1gInSc2
mezinárodního	mezinárodní	k2eAgNnSc2d1
práva	právo	k1gNnSc2
nelegální	legální	k2eNgFnSc2d1
osady	osada	k1gFnSc2
v	v	k7c6
Judsku	Judsko	k1gNnSc6
a	a	k8xC
Samaří	Samaří	k1gNnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
(	(	kIx(
<g/>
mimo	mimo	k7c4
Jeruzalém	Jeruzalém	k1gInSc4
<g/>
)	)	kIx)
žije	žít	k5eAaImIp3nS
okolo	okolo	k7c2
300	#num#	k4
tisíc	tisíc	k4xCgInPc2
Izraelců	Izraelec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Palestinské	palestinský	k2eAgNnSc4d1
území	území	k1gNnPc4
navíc	navíc	k6eAd1
na	na	k7c6
mnoha	mnoho	k4c6
místech	místo	k1gNnPc6
protíná	protínat	k5eAaImIp3nS
tzv.	tzv.	kA
bezpečnostní	bezpečnostní	k2eAgFnSc1d1
bariéra	bariéra	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
vedena	vést	k5eAaImNgFnS
za	za	k7c7
"	"	kIx"
<g/>
Zelenou	zelený	k2eAgFnSc7d1
linií	linie	k1gFnSc7
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
červnu	červen	k1gInSc6
2005	#num#	k4
mezinárodní	mezinárodní	k2eAgInSc4d1
soud	soud	k1gInSc4
v	v	k7c6
Haagu	Haag	k1gInSc6
rozhodl	rozhodnout	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
bezpečnostní	bezpečnostní	k2eAgFnSc1d1
bariéra	bariéra	k1gFnSc1
porušuje	porušovat	k5eAaImIp3nS
mezinárodní	mezinárodní	k2eAgNnSc4d1
právo	právo	k1gNnSc4
<g/>
,	,	kIx,
protože	protože	k8xS
nestojí	stát	k5eNaImIp3nS
na	na	k7c6
izraelském	izraelský	k2eAgNnSc6d1
území	území	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
mezinárodního	mezinárodní	k2eAgNnSc2d1
práva	právo	k1gNnSc2
je	být	k5eAaImIp3nS
rovněž	rovněž	k9
zakázáno	zakázán	k2eAgNnSc1d1
usídlovat	usídlovat	k5eAaImF
obyvatelstvo	obyvatelstvo	k1gNnSc4
okupujícího	okupující	k2eAgInSc2d1
státu	stát	k1gInSc2
na	na	k7c6
okupovaném	okupovaný	k2eAgNnSc6d1
území	území	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Podle	podle	k7c2
reprezentantů	reprezentant	k1gMnPc2
státu	stát	k1gInSc2
Izrael	Izrael	k1gMnSc1
osady	osada	k1gFnSc2
neporušují	porušovat	k5eNaImIp3nP
Ženevskou	ženevský	k2eAgFnSc4d1
konvenci	konvence	k1gFnSc4
<g/>
,	,	kIx,
protože	protože	k8xS
tam	tam	k6eAd1
jejich	jejich	k3xOp3gMnPc1
obyvatelé	obyvatel	k1gMnPc1
odešli	odejít	k5eAaPmAgMnP
z	z	k7c2
vlastní	vlastní	k2eAgFnSc2d1
vůle	vůle	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Izrael	Izrael	k1gInSc4
ovšem	ovšem	k9
osadám	osada	k1gFnPc3
zajišťuje	zajišťovat	k5eAaImIp3nS
ochranu	ochrana	k1gFnSc4
vojenskou	vojenský	k2eAgFnSc7d1
silou	síla	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bezpečnostní	bezpečnostní	k2eAgFnSc1d1
bariéra	bariéra	k1gFnSc1
je	být	k5eAaImIp3nS
podle	podle	k7c2
izraelské	izraelský	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
nezbytná	zbytný	k2eNgFnSc1d1,k2eAgFnSc1d1
pro	pro	k7c4
zabránění	zabránění	k1gNnSc4
teroristickým	teroristický	k2eAgInPc3d1
útokům	útok	k1gInPc3
<g/>
,	,	kIx,
k	k	k7c3
jejichž	jejichž	k3xOyRp3gInSc3
extrémnímu	extrémní	k2eAgInSc3d1
nárůstu	nárůst	k1gInSc3
a	a	k8xC
vystupňování	vystupňování	k1gNnSc6
brutality	brutalita	k1gFnSc2
došlo	dojít	k5eAaPmAgNnS
v	v	k7c6
době	doba	k1gFnSc6
druhé	druhý	k4xOgFnSc2
intifády	intifáda	k1gFnSc2
v	v	k7c6
l.	l.	k?
2000	#num#	k4
<g/>
–	–	k?
<g/>
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Administrativní	administrativní	k2eAgNnSc1d1
dělení	dělení	k1gNnSc1
</s>
<s>
Podle	podle	k7c2
Dohody	dohoda	k1gFnSc2
z	z	k7c2
Osla	Oslo	k1gNnSc2
jsou	být	k5eAaImIp3nP
Západní	západní	k2eAgInSc4d1
břeh	břeh	k1gInSc4
a	a	k8xC
Pásmo	pásmo	k1gNnSc4
Gazy	Gaza	k1gFnSc2
rozděleny	rozdělit	k5eAaPmNgInP
do	do	k7c2
oblastí	oblast	k1gFnPc2
(	(	kIx(
<g/>
A	A	kA
<g/>
,	,	kIx,
B	B	kA
a	a	k8xC
C	C	kA
<g/>
)	)	kIx)
stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
16	#num#	k4
guvernorátů	guvernorát	k1gInPc2
<g/>
:	:	kIx,
</s>
<s>
Oblast	oblast	k1gFnSc1
A	a	k9
je	být	k5eAaImIp3nS
pod	pod	k7c7
bezpečnostní	bezpečnostní	k2eAgFnSc7d1
a	a	k8xC
civilní	civilní	k2eAgFnSc7d1
kontrolou	kontrola	k1gFnSc7
Palestinské	palestinský	k2eAgFnSc2d1
samosprávy	samospráva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Oblast	oblast	k1gFnSc1
B	B	kA
je	být	k5eAaImIp3nS
pod	pod	k7c7
civilní	civilní	k2eAgFnSc7d1
kontrolou	kontrola	k1gFnSc7
Palestinské	palestinský	k2eAgFnSc2d1
samosprávy	samospráva	k1gFnSc2
a	a	k8xC
bezpečnostní	bezpečnostní	k2eAgFnSc2d1
kontrolou	kontrola	k1gFnSc7
Izraelských	izraelský	k2eAgFnPc2d1
obranných	obranný	k2eAgFnPc2d1
sil	síla	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Oblast	oblast	k1gFnSc1
C	C	kA
je	být	k5eAaImIp3nS
pod	pod	k7c7
plnou	plný	k2eAgFnSc7d1
izraelskou	izraelský	k2eAgFnSc7d1
kontrolou	kontrola	k1gFnSc7
(	(	kIx(
<g/>
například	například	k6eAd1
<g/>
:	:	kIx,
Osady	osada	k1gFnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
převzal	převzít	k5eAaPmAgMnS
kontrolu	kontrola	k1gFnSc4
nad	nad	k7c7
Pásmem	pásmo	k1gNnSc7
Gazy	Gaza	k1gFnSc2
Hamas	Hamasa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Palestinská	palestinský	k2eAgFnSc1d1
správa	správa	k1gFnSc1
již	již	k6eAd1
nekontroluje	nekontrolovat	k5eAaImIp3nS,k5eNaImIp3nS
Pásmo	pásmo	k1gNnSc4
Gazy	Gaza	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Mapa	mapa	k1gFnSc1
ukazuje	ukazovat	k5eAaImIp3nS
guvernoráty	guvernorát	k1gInPc4
a	a	k8xC
oblasti	oblast	k1gFnPc4
pod	pod	k7c7
formální	formální	k2eAgFnSc7d1
Palestinskou	palestinský	k2eAgFnSc7d1
kontrolou	kontrola	k1gFnSc7
(	(	kIx(
<g/>
Oblasti	oblast	k1gFnSc2
A	A	kA
a	a	k8xC
B	B	kA
tmavě	tmavě	k6eAd1
zeleně	zeleně	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Guvernoraty	Guvernorat	k1gInPc1
Palestinské	palestinský	k2eAgFnSc2d1
správy	správa	k1gFnSc2
</s>
<s>
Palestinskou	palestinský	k2eAgFnSc4d1
autonomii	autonomie	k1gFnSc4
v	v	k7c6
současnosti	současnost	k1gFnSc6
tvoří	tvořit	k5eAaImIp3nS
11	#num#	k4
guvernorátů	guvernorát	k1gInPc2
<g/>
:	:	kIx,
</s>
<s>
Guvernorát	Guvernorát	k1gInSc1
Dženín	Dženína	k1gFnPc2
</s>
<s>
Guvernorát	Guvernorát	k1gInSc1
Túbás	Túbása	k1gFnPc2
</s>
<s>
Guvernorát	Guvernorát	k1gInSc1
Nábulus	Nábulus	k1gInSc1
</s>
<s>
Guvernorát	Guvernorát	k1gInSc1
Tulkarm	Tulkarm	k1gInSc1
</s>
<s>
Guvernorát	Guvernorát	k1gInSc1
Salfit	Salfita	k1gFnPc2
</s>
<s>
Guvernorát	Guvernorát	k1gInSc1
Kalkílija	Kalkílij	k1gInSc2
</s>
<s>
Guvernorát	Guvernorát	k1gInSc1
Ramalláh	Ramalláha	k1gFnPc2
</s>
<s>
Guvernorát	Guvernorát	k1gInSc1
Jericho	Jericho	k1gNnSc1
</s>
<s>
Guvernorát	Guvernorát	k1gInSc1
Jeruzalém	Jeruzalém	k1gInSc1
</s>
<s>
Guvernorát	Guvernorát	k1gInSc1
Betlém	Betlém	k1gInSc1
</s>
<s>
Guvernorát	Guvernorát	k1gInSc1
Hebron	Hebron	k1gInSc1
</s>
<s>
Guvernoráty	Guvernorát	k1gInPc1
v	v	k7c6
Gaze	Gaze	k1gFnPc6
(	(	kIx(
<g/>
do	do	k7c2
2007	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Pásmo	pásmo	k1gNnSc1
Gazy	Gaza	k1gFnSc2
není	být	k5eNaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
2007	#num#	k4
pod	pod	k7c7
kontrolou	kontrola	k1gFnSc7
Palestinské	palestinský	k2eAgFnSc2d1
samosprávy	samospráva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současnosti	současnost	k1gFnSc6
správu	správa	k1gFnSc4
nad	nad	k7c7
oblastí	oblast	k1gFnSc7
vykonává	vykonávat	k5eAaImIp3nS
Hamás	Hamás	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Guvernorát	Guvernorát	k1gInSc1
Dajr	Dajr	k1gMnSc1
al-Balah	al-Balah	k1gMnSc1
</s>
<s>
Guvernorát	Guvernorát	k1gInSc1
Chán	chán	k1gMnSc1
Júnis	Júnis	k1gFnPc2
</s>
<s>
Guvernorát	Guvernorát	k1gInSc1
Rafah	Rafaha	k1gFnPc2
</s>
<s>
Guvernorát	Guvernorát	k1gInSc1
Severní	severní	k2eAgFnSc1d1
Gaza	Gaza	k1gFnSc1
</s>
<s>
Guvernorát	Guvernorát	k1gInSc1
Gaza	Gaz	k1gInSc2
</s>
<s>
Města	město	k1gNnSc2
</s>
<s>
Na	na	k7c6
Západním	západní	k2eAgInSc6d1
břehu	břeh	k1gInSc6
</s>
<s>
Ramalláh	Ramalláh	k1gInSc1
(	(	kIx(
<g/>
Sídlo	sídlo	k1gNnSc1
samosprávy	samospráva	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Východní	východní	k2eAgInSc1d1
Jeruzalém	Jeruzalém	k1gInSc1
(	(	kIx(
<g/>
kontrolovaný	kontrolovaný	k2eAgInSc1d1
Izraelem	Izrael	k1gMnSc7
<g/>
)	)	kIx)
</s>
<s>
Hebron	Hebron	k1gMnSc1
</s>
<s>
Náblus	Náblus	k1gMnSc1
</s>
<s>
Dženín	Dženín	k1gInSc1
(	(	kIx(
<g/>
uprchlický	uprchlický	k2eAgInSc1d1
tábor	tábor	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Jericho	Jericho	k1gNnSc1
</s>
<s>
Betlém	Betlém	k1gInSc1
</s>
<s>
Tulkarm	Tulkarm	k1gInSc1
(	(	kIx(
<g/>
uprchlický	uprchlický	k2eAgInSc1d1
tábor	tábor	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Rawábí	Rawábit	k5eAaPmIp3nS
</s>
<s>
V	v	k7c6
Pásmu	pásmo	k1gNnSc6
Gazy	Gaza	k1gFnSc2
</s>
<s>
Gaza	Gaza	k6eAd1
</s>
<s>
Chán	chán	k1gMnSc1
Júnis	Júnis	k1gFnSc2
(	(	kIx(
<g/>
uprchlický	uprchlický	k2eAgInSc1d1
tábor	tábor	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Džabalíja	Džabalíja	k1gFnSc1
(	(	kIx(
<g/>
uprchlický	uprchlický	k2eAgInSc1d1
tábor	tábor	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Rafáh	Rafáh	k1gMnSc1
</s>
<s>
Dajr	Dajr	k1gMnSc1
al-Balah	al-Balah	k1gMnSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
OSN	OSN	kA
uznala	uznat	k5eAaPmAgFnS
nezávislý	závislý	k2eNgInSc4d1
stát	stát	k1gInSc4
Palestina	Palestina	k1gFnSc1
<g/>
↑	↑	k?
Palestina	Palestina	k1gFnSc1
posílila	posílit	k5eAaPmAgFnS
v	v	k7c6
OSN	OSN	kA
<g/>
,	,	kIx,
stala	stát	k5eAaPmAgFnS
se	se	k3xPyFc4
pozorovatelským	pozorovatelský	k2eAgInSc7d1
státem	stát	k1gInSc7
<g/>
↑	↑	k?
Valné	valný	k2eAgNnSc1d1
shromáždění	shromáždění	k1gNnSc1
OSN	OSN	kA
rozhodlo	rozhodnout	k5eAaPmAgNnS
drtivou	drtivý	k2eAgFnSc7d1
většinou	většina	k1gFnSc7
o	o	k7c6
nečlenském	členský	k2eNgInSc6d1
státu	stát	k1gInSc6
Palestina	Palestina	k1gFnSc1
<g/>
↑	↑	k?
Oficiální	oficiální	k2eAgInSc4d1
název	název	k1gInSc4
palestinského	palestinský	k2eAgNnSc2d1
autonomního	autonomní	k2eAgNnSc2d1
území	území	k1gNnSc2
bude	být	k5eAaImBp3nS
Stát	stát	k5eAaImF,k5eAaPmF
Palestina	Palestina	k1gFnSc1
<g/>
↑	↑	k?
http://www.palestine.cz	http://www.palestine.cz	k1gInSc4
Archivováno	archivovat	k5eAaBmNgNnS
27	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
2019	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
Velvyslanectví	velvyslanectví	k1gNnSc1
Státu	stát	k1gInSc2
Palestina	Palestina	k1gFnSc1
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
↑	↑	k?
IMEU	IMEU	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Are	ar	k1gInSc5
all	all	k?
Palestinians	Palestiniansa	k1gFnPc2
Muslim	muslim	k1gMnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
|	|	kIx~
IMEU	IMEU	kA
<g/>
.	.	kIx.
imeu	ime	k2eAgFnSc4d1
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
FARSOUN	FARSOUN	kA
<g/>
,	,	kIx,
Samih	Samih	k1gMnSc1
K.	K.	kA
Culture	Cultur	k1gMnSc5
and	and	k?
Customs	Customs	k1gInSc1
of	of	k?
the	the	k?
Palestinians	Palestinians	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Greenwood	Greenwood	k1gInSc1
192	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9780313320514	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
English	English	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Palestina	Palestina	k1gFnSc1
</s>
<s>
Pásmo	pásmo	k1gNnSc1
Gazy	Gaza	k1gFnSc2
</s>
<s>
Západní	západní	k2eAgInSc1d1
břeh	břeh	k1gInSc1
Jordánu	Jordán	k1gInSc2
</s>
<s>
Judea	Judea	k1gFnSc1
a	a	k8xC
Samaří	Samaří	k1gNnSc1
</s>
<s>
Hamás	Hamás	k6eAd1
</s>
<s>
Fatáh	Fatáh	k1gMnSc1
</s>
<s>
Izraelsko-palestinský	izraelsko-palestinský	k2eAgInSc1d1
konflikt	konflikt	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Palestinská	palestinský	k2eAgFnSc1d1
autonomie	autonomie	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
http://www.milost.tv/m/4p2A9Dlnn	http://www.milost.tv/m/4p2A9Dlnn	k1gMnSc1
</s>
<s>
ISM	ISM	kA
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
Velvyslanectví	velvyslanectví	k1gNnSc1
Státu	stát	k1gInSc2
Palestina	Palestina	k1gFnSc1
v	v	k7c6
Praze	Praha	k1gFnSc6
</s>
<s>
Autonomní	autonomní	k2eAgNnSc4d1
území	území	k1gNnSc4
pod	pod	k7c7
Palestinskou	palestinský	k2eAgFnSc7d1
národní	národní	k2eAgFnSc7d1
správou	správa	k1gFnSc7
v	v	k7c6
Encyklopedii	encyklopedie	k1gFnSc6
států	stát	k1gInPc2
a	a	k8xC
území	území	k1gNnSc2
světa	svět	k1gInSc2
MZV	MZV	kA
ČR	ČR	kA
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Geografie	geografie	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
kn	kn	k?
<g/>
20050429039	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2230	#num#	k4
8442	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
95043722	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
155583515	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
95043722	#num#	k4
</s>
