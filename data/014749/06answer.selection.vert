<s desamb="1">
Tento	tento	k3xDgInSc1
status	status	k1gInSc4
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
Falklandy	Falkland	k1gInPc1
nejsou	být	k5eNaImIp3nP
součástí	součást	k1gFnSc7
Spojeného	spojený	k2eAgNnSc2d1
království	království	k1gNnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
patří	patřit	k5eAaImIp3nS
pod	pod	k7c4
jeho	jeho	k3xOp3gFnSc4
suverenitu	suverenita	k1gFnSc4
<g/>
.	.	kIx.
<g/>
ISO	ISO	kA
4217	#num#	k4
kód	kód	k1gInSc4
falklandské	falklandský	k2eAgFnSc2d1
libry	libra	k1gFnSc2
je	být	k5eAaImIp3nS
FKP	FKP	kA
<g/>
.	.	kIx.
</s>