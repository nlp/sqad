<s>
Falklandská	Falklandský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
</s>
<s>
Falklandská	Falklandský	k2eAgFnSc1d1
libraFalkland	libraFalkland	k1gInSc1
Islands	Islands	k1gInSc1
pound	pound	k1gInSc4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
Země	země	k1gFnSc1
</s>
<s>
Falklandy	Falkland	k1gInPc1
Falklandy	Falklanda	k1gFnSc2
ISO	ISO	kA
4217	#num#	k4
</s>
<s>
FKP	FKP	kA
Symbol	symbol	k1gInSc1
</s>
<s>
£	£	k?
Dílčí	dílčí	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
</s>
<s>
penny	penny	k1gFnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
100	#num#	k4
<g/>
)	)	kIx)
Mince	mince	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
p	p	k?
<g/>
,	,	kIx,
2	#num#	k4
<g/>
p	p	k?
<g/>
,	,	kIx,
5	#num#	k4
<g/>
p	p	k?
<g/>
,	,	kIx,
10	#num#	k4
<g/>
p	p	k?
<g/>
,	,	kIx,
20	#num#	k4
<g/>
p	p	k?
<g/>
,	,	kIx,
50	#num#	k4
<g/>
p	p	k?
<g/>
,	,	kIx,
£	£	k?
<g/>
1	#num#	k4
<g/>
,	,	kIx,
£	£	k?
<g/>
2	#num#	k4
Bankovky	bankovka	k1gFnSc2
</s>
<s>
£	£	k?
<g/>
5	#num#	k4
<g/>
,	,	kIx,
£	£	k?
<g/>
10	#num#	k4
<g/>
,	,	kIx,
£	£	k?
<g/>
20	#num#	k4
<g/>
,	,	kIx,
£	£	k?
<g/>
50	#num#	k4
</s>
<s>
Falklandská	Falklandský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
je	být	k5eAaImIp3nS
zákonným	zákonný	k2eAgNnSc7d1
platidlem	platidlo	k1gNnSc7
britského	britský	k2eAgNnSc2d1
zámořského	zámořský	k2eAgNnSc2d1
teritoria	teritorium	k1gNnSc2
Falklandy	Falklanda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
status	status	k1gInSc4
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
Falklandy	Falkland	k1gInPc1
nejsou	být	k5eNaImIp3nP
součástí	součást	k1gFnSc7
Spojeného	spojený	k2eAgNnSc2d1
království	království	k1gNnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
patří	patřit	k5eAaImIp3nS
pod	pod	k7c4
jeho	jeho	k3xOp3gFnSc4
suverenitu	suverenita	k1gFnSc4
<g/>
.	.	kIx.
<g/>
ISO	ISO	kA
4217	#num#	k4
kód	kód	k1gInSc4
falklandské	falklandský	k2eAgFnSc2d1
libry	libra	k1gFnSc2
je	být	k5eAaImIp3nS
FKP	FKP	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Falklandská	Falklandský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
je	být	k5eAaImIp3nS
pevně	pevně	k6eAd1
navázána	navázán	k2eAgFnSc1d1
na	na	k7c4
měnu	měna	k1gFnSc4
svého	svůj	k3xOyFgInSc2
nadřazeného	nadřazený	k2eAgInSc2d1
politického	politický	k2eAgInSc2d1
celku	celek	k1gInSc2
-	-	kIx~
libru	libra	k1gFnSc4
šterlinků	šterlink	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kurs	kurs	k1gInSc1
je	být	k5eAaImIp3nS
stanoven	stanovit	k5eAaPmNgInS
na	na	k7c4
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
Falklandách	Falklanda	k1gFnPc6
se	se	k3xPyFc4
dá	dát	k5eAaPmIp3nS
platit	platit	k5eAaImF
jak	jak	k6eAd1
britskou	britský	k2eAgFnSc4d1
<g/>
,	,	kIx,
tak	tak	k9
i	i	k9
falklandskou	falklandský	k2eAgFnSc7d1
librou	libra	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naopak	naopak	k6eAd1
toto	tento	k3xDgNnSc1
ovšem	ovšem	k9
neplatí	platit	k5eNaImIp3nS
-	-	kIx~
ve	v	k7c6
Spojeném	spojený	k2eAgNnSc6d1
království	království	k1gNnSc6
falklandskou	falklandský	k2eAgFnSc4d1
libru	libra	k1gFnSc4
použít	použít	k5eAaPmF
nelze	lze	k6eNd1
<g/>
.	.	kIx.
</s>
<s>
Falklandy	Falkland	k1gInPc1
tisknou	tisknout	k5eAaImIp3nP
vlastní	vlastní	k2eAgFnPc4d1
bankovky	bankovka	k1gFnPc4
už	už	k6eAd1
od	od	k7c2
roku	rok	k1gInSc2
1921	#num#	k4
<g/>
,	,	kIx,
mince	mince	k1gFnSc1
razí	razit	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
1974	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Bankovky	bankovka	k1gFnPc1
a	a	k8xC
mince	mince	k1gFnSc1
falklandské	falklandský	k2eAgFnSc2d1
libry	libra	k1gFnSc2
</s>
<s>
Mince	mince	k1gFnSc1
falklandské	falklandský	k2eAgFnSc2d1
libry	libra	k1gFnSc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
