<s>
Puma	puma	k1gFnSc1	puma
AG	AG	kA	AG
Rudolf	Rudolf	k1gMnSc1	Rudolf
Dassler	Dassler	k1gMnSc1	Dassler
Sport	sport	k1gInSc1	sport
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
Puma	puma	k1gFnSc1	puma
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
německá	německý	k2eAgFnSc1d1	německá
společnost	společnost	k1gFnSc1	společnost
specializující	specializující	k2eAgFnSc1d1	specializující
se	se	k3xPyFc4	se
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
sportovního	sportovní	k2eAgNnSc2d1	sportovní
oblečení	oblečení	k1gNnSc2	oblečení
<g/>
,	,	kIx,	,
vybavení	vybavení	k1gNnSc2	vybavení
a	a	k8xC	a
obuvi	obuv	k1gFnSc2	obuv
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
Rudolfem	Rudolf	k1gMnSc7	Rudolf
Dasslerem	Dassler	k1gMnSc7	Dassler
roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
registrována	registrovat	k5eAaBmNgFnS	registrovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
<s>
Puma	puma	k1gFnSc1	puma
je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xS	jako
výrobce	výrobce	k1gMnPc4	výrobce
fotbalové	fotbalový	k2eAgFnSc2d1	fotbalová
obuvi	obuv	k1gFnSc2	obuv
a	a	k8xC	a
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
historii	historie	k1gFnSc6	historie
sponzorovala	sponzorovat	k5eAaImAgFnS	sponzorovat
mnoho	mnoho	k4c4	mnoho
významných	významný	k2eAgMnPc2d1	významný
fotbalistů	fotbalista	k1gMnPc2	fotbalista
(	(	kIx(	(
<g/>
Pelé	Pelé	k1gNnSc1	Pelé
<g/>
,	,	kIx,	,
Eusébio	Eusébio	k1gMnSc1	Eusébio
<g/>
,	,	kIx,	,
Johan	Johan	k1gMnSc1	Johan
Cruijff	Cruijff	k1gMnSc1	Cruijff
<g/>
,	,	kIx,	,
Enzo	Enzo	k1gMnSc1	Enzo
Francescoli	Francescole	k1gFnSc4	Francescole
<g/>
,	,	kIx,	,
Diego	Diego	k6eAd1	Diego
Maradona	Maradona	k1gFnSc1	Maradona
<g/>
,	,	kIx,	,
Lothar	Lothar	k1gMnSc1	Lothar
Matthäus	Matthäus	k1gMnSc1	Matthäus
<g/>
,	,	kIx,	,
Kenny	Kenna	k1gFnPc1	Kenna
Dalglish	Dalglish	k1gMnSc1	Dalglish
<g/>
,	,	kIx,	,
Didier	Didier	k1gMnSc1	Didier
Deschamps	Deschamps	k1gInSc1	Deschamps
či	či	k8xC	či
Gianluigi	Gianluigi	k1gNnSc1	Gianluigi
Buffon	Buffon	k1gInSc1	Buffon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sponzoruje	sponzorovat	k5eAaImIp3nS	sponzorovat
rovněž	rovněž	k9	rovněž
některé	některý	k3yIgInPc4	některý
národní	národní	k2eAgInPc4d1	národní
fotbalové	fotbalový	k2eAgInPc4d1	fotbalový
týmy	tým	k1gInPc4	tým
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
např.	např.	kA	např.
<g/>
:	:	kIx,	:
<g/>
Italskou	italský	k2eAgFnSc4d1	italská
fotbalovou	fotbalový	k2eAgFnSc4d1	fotbalová
reprezentaci	reprezentace	k1gFnSc4	reprezentace
nebo	nebo	k8xC	nebo
Českou	český	k2eAgFnSc4d1	Česká
fotbalovou	fotbalový	k2eAgFnSc4d1	fotbalová
reprezentaci	reprezentace	k1gFnSc4	reprezentace
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tenisovými	tenisový	k2eAgFnPc7d1	tenisová
raketami	raketa	k1gFnPc7	raketa
hráli	hrát	k5eAaImAgMnP	hrát
například	například	k6eAd1	například
Boris	Boris	k1gMnSc1	Boris
Becker	Becker	k1gMnSc1	Becker
nebo	nebo	k8xC	nebo
Tim	Tim	k?	Tim
Henman	Henman	k1gMnSc1	Henman
<g/>
.	.	kIx.	.
</s>
<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
Dassler	Dassler	k1gMnSc1	Dassler
společně	společně	k6eAd1	společně
s	s	k7c7	s
bratrem	bratr	k1gMnSc7	bratr
Adolfem	Adolf	k1gMnSc7	Adolf
založili	založit	k5eAaPmAgMnP	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
firmu	firma	k1gFnSc4	firma
na	na	k7c6	na
výrobu	výrob	k1gInSc6	výrob
sportovní	sportovní	k2eAgFnSc2d1	sportovní
obuvi	obuv	k1gFnSc2	obuv
Gebrüder	Gebrüder	k1gMnSc1	Gebrüder
Dassler	Dassler	k1gMnSc1	Dassler
Schuhfabrik	Schuhfabrik	k1gMnSc1	Schuhfabrik
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
se	se	k3xPyFc4	se
rozešli	rozejít	k5eAaPmAgMnP	rozejít
<g/>
.	.	kIx.	.
</s>
<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
založil	založit	k5eAaPmAgMnS	založit
opět	opět	k6eAd1	opět
firmu	firma	k1gFnSc4	firma
vyrábějící	vyrábějící	k2eAgFnSc4d1	vyrábějící
sportovní	sportovní	k2eAgFnSc4d1	sportovní
obuv	obuv	k1gFnSc4	obuv
a	a	k8xC	a
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
ji	on	k3xPp3gFnSc4	on
podle	podle	k7c2	podle
prvních	první	k4xOgFnPc2	první
dvou	dva	k4xCgFnPc2	dva
písmen	písmeno	k1gNnPc2	písmeno
ze	z	k7c2	z
svého	svůj	k3xOyFgNnSc2	svůj
jména	jméno	k1gNnSc2	jméno
Ruda	ruda	k1gFnSc1	ruda
(	(	kIx(	(
<g/>
RUdolf	Rudolf	k1gMnSc1	Rudolf
DAssler	DAssler	k1gMnSc1	DAssler
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
však	však	k9	však
firmu	firma	k1gFnSc4	firma
přejmenoval	přejmenovat	k5eAaPmAgInS	přejmenovat
na	na	k7c4	na
Puma	puma	k1gFnSc1	puma
Schuhfabrik	Schuhfabrik	k1gMnSc1	Schuhfabrik
Rudolf	Rudolf	k1gMnSc1	Rudolf
Dassler	Dassler	k1gMnSc1	Dassler
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
postupoval	postupovat	k5eAaImAgMnS	postupovat
i	i	k9	i
Adolf	Adolf	k1gMnSc1	Adolf
(	(	kIx(	(
<g/>
Adi	Adi	k1gMnSc1	Adi
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
do	do	k7c2	do
názvu	název	k1gInSc2	název
použil	použít	k5eAaPmAgMnS	použít
první	první	k4xOgNnPc4	první
tři	tři	k4xCgNnPc4	tři
písmena	písmeno	k1gNnPc4	písmeno
a	a	k8xC	a
založil	založit	k5eAaPmAgMnS	založit
firmu	firma	k1gFnSc4	firma
Adidas	Adidas	k1gMnSc1	Adidas
<g/>
.	.	kIx.	.
</s>
