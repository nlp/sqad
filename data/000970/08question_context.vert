<s>
Puma	puma	k1gFnSc1	puma
AG	AG	kA	AG
Rudolf	Rudolf	k1gMnSc1	Rudolf
Dassler	Dassler	k1gMnSc1	Dassler
Sport	sport	k1gInSc1	sport
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
Puma	puma	k1gFnSc1	puma
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
německá	německý	k2eAgFnSc1d1	německá
společnost	společnost	k1gFnSc1	společnost
specializující	specializující	k2eAgFnSc1d1	specializující
se	se	k3xPyFc4	se
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
sportovního	sportovní	k2eAgNnSc2d1	sportovní
oblečení	oblečení	k1gNnSc2	oblečení
<g/>
,	,	kIx,	,
vybavení	vybavení	k1gNnSc2	vybavení
a	a	k8xC	a
obuvi	obuv	k1gFnSc2	obuv
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
Rudolfem	Rudolf	k1gMnSc7	Rudolf
Dasslerem	Dassler	k1gMnSc7	Dassler
roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
registrována	registrovat	k5eAaBmNgFnS	registrovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>

