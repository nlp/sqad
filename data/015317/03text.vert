<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Lužické	lužický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
</s>
<s>
Zdroje	zdroj	k1gInPc1
k	k	k7c3
infoboxuChráněná	infoboxuChráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgNnPc1d1
oblastLužické	oblastLužický	k2eAgFnPc4d1
horyIUCN	horyIUCN	k?
kategorie	kategorie	k1gFnPc4
V	V	kA
(	(	kIx(
<g/>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
<g/>
)	)	kIx)
Likvidace	likvidace	k1gFnSc1
stromů	strom	k1gInPc2
<g/>
,	,	kIx,
napadených	napadený	k2eAgFnPc2d1
kůrovcem	kůrovec	k1gMnSc7
poblíž	poblíž	k6eAd1
železniční	železniční	k2eAgFnSc1d1
stanice	stanice	k1gFnSc1
Jedlová	jedlový	k2eAgFnSc1d1
(	(	kIx(
<g/>
rok	rok	k1gInSc1
2020	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
pozadí	pozadí	k1gNnSc6
hora	hora	k1gFnSc1
Jedlová	jedlový	k2eAgFnSc1d1
(	(	kIx(
<g/>
774	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
Základní	základní	k2eAgFnSc1d1
informace	informace	k1gFnSc1
Vyhlášení	vyhlášení	k1gNnSc2
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1976	#num#	k4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
270,72	270,72	k4
km²	km²	k?
Správa	správa	k1gFnSc1
</s>
<s>
Správa	správa	k1gFnSc1
CHKO	CHKO	kA
Lužické	lužický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
Poloha	poloha	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Kraj	kraj	k7c2
</s>
<s>
Ústecký	ústecký	k2eAgInSc1d1
kraj	kraj	k1gInSc1
Okres	okres	k1gInSc1
</s>
<s>
Děčín	Děčín	k1gInSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
50	#num#	k4
<g/>
′	′	k?
<g/>
18	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
14	#num#	k4
<g/>
°	°	k?
<g/>
36	#num#	k4
<g/>
′	′	k?
<g/>
19	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Geodata	Geodat	k1gMnSc2
(	(	kIx(
<g/>
OSM	osm	k4xCc1
<g/>
)	)	kIx)
</s>
<s>
OSM	osm	k4xCc1
<g/>
,	,	kIx,
WMF	WMF	kA
</s>
<s>
Lužické	lužický	k2eAgFnPc1d1
hory	hora	k1gFnPc1
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Web	web	k1gInSc1
</s>
<s>
www.luzickehory.ochranaprirody.cz	www.luzickehory.ochranaprirody.cz	k1gMnSc1
</s>
<s>
Obrázky	obrázek	k1gInPc4
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc4
či	či	k8xC
videa	video	k1gNnPc4
v	v	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Tento	tento	k3xDgInSc1
box	box	k1gInSc1
<g/>
:	:	kIx,
zobrazit	zobrazit	k5eAaPmF
•	•	k?
diskuse	diskuse	k1gFnSc2
</s>
<s>
Chráněné	chráněný	k2eAgFnPc1d1
krajinné	krajinný	k2eAgFnPc1d1
oblasti	oblast	k1gFnPc1
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Lužické	lužický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
severních	severní	k2eAgFnPc6d1
Čechách	Čechy	k1gFnPc6
na	na	k7c6
rozhraní	rozhraní	k1gNnSc6
Ústeckého	ústecký	k2eAgInSc2d1
a	a	k8xC
Libereckého	liberecký	k2eAgInSc2d1
kraje	kraj	k1gInSc2
v	v	k7c6
okresech	okres	k1gInPc6
Děčín	Děčín	k1gInSc1
<g/>
,	,	kIx,
Česká	český	k2eAgFnSc1d1
Lípa	lípa	k1gFnSc1
a	a	k8xC
Liberec	Liberec	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozloha	rozloha	k1gFnSc1
chráněného	chráněný	k2eAgNnSc2d1
území	území	k1gNnSc2
je	být	k5eAaImIp3nS
270,72	270,72	k4
km²	km²	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Předmětem	předmět	k1gInSc7
ochrany	ochrana	k1gFnSc2
je	být	k5eAaImIp3nS
rozmanitá	rozmanitý	k2eAgFnSc1d1
krajina	krajina	k1gFnSc1
pískovcových	pískovcový	k2eAgNnPc2d1
skalních	skalní	k2eAgNnPc2d1
měst	město	k1gNnPc2
a	a	k8xC
znělcových	znělcový	k2eAgInPc2d1
<g/>
,	,	kIx,
trachytových	trachytový	k2eAgInPc2d1
a	a	k8xC
čedičových	čedičový	k2eAgInPc2d1
kuželů	kužel	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Tyto	tento	k3xDgInPc1
význačné	význačný	k2eAgInPc1d1
geomorfologické	geomorfologický	k2eAgInPc1d1
útvary	útvar	k1gInPc1
jsou	být	k5eAaImIp3nP
doprovázeny	doprovázen	k2eAgInPc1d1
zbytky	zbytek	k1gInPc1
přirozených	přirozený	k2eAgInPc2d1
lesních	lesní	k2eAgInPc2d1
porostů	porost	k1gInPc2
ve	v	k7c6
vrcholových	vrcholový	k2eAgFnPc6d1
partiích	partie	k1gFnPc6
(	(	kIx(
<g/>
buk	buk	k1gInSc4
<g/>
,	,	kIx,
jedle	jedle	k1gFnPc1
<g/>
,	,	kIx,
javor	javor	k1gInSc1
<g/>
,	,	kIx,
jilm	jilm	k1gInSc1
a	a	k8xC
doubrava	doubrava	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vlhkými	vlhký	k2eAgFnPc7d1
horskými	horský	k2eAgFnPc7d1
a	a	k8xC
podhorskými	podhorský	k2eAgFnPc7d1
loukami	louka	k1gFnPc7
s	s	k7c7
výskytem	výskyt	k1gInSc7
vzácných	vzácný	k2eAgInPc2d1
druhů	druh	k1gInPc2
rostlin	rostlina	k1gFnPc2
a	a	k8xC
nivami	niva	k1gFnPc7
potoků	potok	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Území	území	k1gNnSc1
Chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
Lužické	lužický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
kromě	kromě	k7c2
vlastních	vlastní	k2eAgFnPc2d1
Lužických	lužický	k2eAgFnPc2d1
hor	hora	k1gFnPc2
zasahuje	zasahovat	k5eAaImIp3nS
také	také	k9
do	do	k7c2
okolních	okolní	k2eAgFnPc2d1
geomorfologických	geomorfologický	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
–	–	k?
do	do	k7c2
Ještědsko-kozákovského	Ještědsko-kozákovský	k2eAgInSc2d1
hřbetu	hřbet	k1gInSc2
<g/>
,	,	kIx,
Ralské	Ralský	k2eAgFnSc2d1
pahorkatiny	pahorkatina	k1gFnSc2
a	a	k8xC
Českého	český	k2eAgNnSc2d1
středohoří	středohoří	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chráněné	chráněný	k2eAgFnPc4d1
krajinná	krajinný	k2eAgNnPc1d1
oblast	oblast	k1gFnSc1
Lužické	lužický	k2eAgFnPc1d1
hory	hora	k1gFnPc1
sousedí	sousedit	k5eAaImIp3nP
s	s	k7c7
Národním	národní	k2eAgInSc7d1
parkem	park	k1gInSc7
České	český	k2eAgNnSc1d1
Švýcarsko	Švýcarsko	k1gNnSc1
<g/>
,	,	kIx,
Chráněnou	chráněný	k2eAgFnSc7d1
krajinnou	krajinný	k2eAgFnSc7d1
oblastí	oblast	k1gFnSc7
České	český	k2eAgNnSc1d1
středohoří	středohoří	k1gNnSc1
a	a	k8xC
Chráněnou	chráněný	k2eAgFnSc7d1
krajinnou	krajinný	k2eAgFnSc7d1
oblastí	oblast	k1gFnSc7
Labské	labský	k2eAgInPc1d1
pískovce	pískovec	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
německé	německý	k2eAgFnSc2d1
strany	strana	k1gFnSc2
navazují	navazovat	k5eAaImIp3nP
na	na	k7c4
Lužické	lužický	k2eAgFnPc4d1
hory	hora	k1gFnPc4
Žitavské	žitavský	k2eAgFnSc2d1
hory	hora	k1gFnSc2
obdobného	obdobný	k2eAgInSc2d1
charakteru	charakter	k1gInSc2
<g/>
,	,	kIx,
přírodního	přírodní	k2eAgInSc2d1
i	i	k8xC
kulturního	kulturní	k2eAgInSc2d1
vývoje	vývoj	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
se	se	k3xPyFc4
liší	lišit	k5eAaImIp3nP
jen	jen	k9
pojmenováním	pojmenování	k1gNnSc7
a	a	k8xC
státní	státní	k2eAgFnSc7d1
hranicí	hranice	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Správa	správa	k1gFnSc1
Chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
v	v	k7c6
Jablonném	Jablonné	k1gNnSc6
v	v	k7c6
Podještědí	Podještědí	k1gNnSc6
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1956	#num#	k4
bych	by	kYmCp1nS
schválen	schválen	k2eAgInSc1d1
zákon	zákon	k1gInSc1
číslo	číslo	k1gNnSc1
40	#num#	k4
/	/	kIx~
1956	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
uložil	uložit	k5eAaPmAgInS
založení	založení	k1gNnSc4
odborných	odborný	k2eAgNnPc2d1
pracovišť	pracoviště	k1gNnPc2
zaměřených	zaměřený	k2eAgFnPc2d1
na	na	k7c4
státní	státní	k2eAgFnSc4d1
ochranu	ochrana	k1gFnSc4
přírody	příroda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tehdejším	tehdejší	k2eAgInSc6d1
Libereckém	liberecký	k2eAgInSc6d1
kraji	kraj	k1gInSc6
ochranu	ochrana	k1gFnSc4
památek	památka	k1gFnPc2
i	i	k8xC
přírody	příroda	k1gFnSc2
zajišťoval	zajišťovat	k5eAaImAgInS
Vlastivědný	vlastivědný	k2eAgInSc1d1
ústav	ústav	k1gInSc1
se	s	k7c7
sídlem	sídlo	k1gNnSc7
v	v	k7c6
Liberci	Liberec	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
přijetí	přijetí	k1gNnSc6
citovaného	citovaný	k2eAgInSc2d1
zákona	zákon	k1gInSc2
na	na	k7c6
území	území	k1gNnSc6
Ústeckého	ústecký	k2eAgInSc2d1
kraje	kraj	k1gInSc2
byl	být	k5eAaImAgInS
úkol	úkol	k1gInSc1
svěřen	svěřen	k2eAgInSc1d1
samostatnému	samostatný	k2eAgInSc3d1
referátu	referát	k1gInSc3
Krajskému	krajský	k2eAgInSc3d1
národnímu	národní	k2eAgInSc3d1
výboru	výbor	k1gInSc3
v	v	k7c6
Ústí	ústí	k1gNnSc6
nad	nad	k7c7
Labem	Labe	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rada	rada	k1gFnSc1
KNV	KNV	kA
zřídila	zřídit	k5eAaPmAgFnS
roku	rok	k1gInSc2
1958	#num#	k4
krajské	krajský	k2eAgFnSc2d1
středisko	středisko	k1gNnSc1
památkové	památkový	k2eAgFnSc2d1
péče	péče	k1gFnSc2
a	a	k8xC
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
vzniku	vznik	k1gInSc6
Severočeského	severočeský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
červenci	červenec	k1gInSc3
1960	#num#	k4
byl	být	k5eAaImAgInS
zřízen	zřídit	k5eAaPmNgInS
samostatný	samostatný	k2eAgInSc1d1
referát	referát	k1gInSc1
zde	zde	k6eAd1
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
podřízen	podřídit	k5eAaPmNgMnS
odboru	odbor	k1gInSc2
kultury	kultura	k1gFnSc2
KNV	KNV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
středisko	středisko	k1gNnSc1
připravilo	připravit	k5eAaPmAgNnS
návrhy	návrh	k1gInPc4
na	na	k7c4
zřízení	zřízení	k1gNnSc4
několika	několik	k4yIc2
CHKO	CHKO	kA
na	na	k7c6
území	území	k1gNnSc6
kraje	kraj	k1gInSc2
včetně	včetně	k7c2
CHKO	CHKO	kA
Lužické	lužický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Založení	založení	k1gNnSc1
</s>
<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Lužické	lužický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
byla	být	k5eAaImAgFnS
založena	založen	k2eAgFnSc1d1
19	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1976	#num#	k4
výnosem	výnos	k1gInSc7
Ministerstva	ministerstvo	k1gNnSc2
kultury	kultura	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
založení	založení	k1gNnSc6
měla	mít	k5eAaImAgFnS
na	na	k7c4
starosti	starost	k1gFnPc4
území	území	k1gNnSc2
v	v	k7c6
rozsahu	rozsah	k1gInSc6
265	#num#	k4
km²	km²	k?
v	v	k7c6
okresech	okres	k1gInPc6
Děčín	Děčín	k1gInSc1
<g/>
,	,	kIx,
Česká	český	k2eAgFnSc1d1
Lípa	lípa	k1gFnSc1
a	a	k8xC
Liberec	Liberec	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
své	svůj	k3xOyFgFnSc6
péči	péče	k1gFnSc6
měla	mít	k5eAaImAgFnS
pět	pět	k4xCc4
státních	státní	k2eAgFnPc2d1
přírodních	přírodní	k2eAgFnPc2d1
rezervací	rezervace	k1gFnPc2
<g/>
,	,	kIx,
jedno	jeden	k4xCgNnSc1
chráněné	chráněný	k2eAgNnSc1d1
naleziště	naleziště	k1gNnSc1
a	a	k8xC
tři	tři	k4xCgInPc4
chráněné	chráněný	k2eAgInPc4d1
přírodní	přírodní	k2eAgInPc4d1
výtvory	výtvor	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prvním	první	k4xOgNnSc7
sídlem	sídlo	k1gNnSc7
správy	správa	k1gFnSc2
chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
byl	být	k5eAaImAgInS
v	v	k7c6
roce	rok	k1gInSc6
1976	#num#	k4
Děčín	Děčín	k1gInSc4
a	a	k8xC
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1979	#num#	k4
sídlí	sídlet	k5eAaImIp3nS
v	v	k7c6
Jablonném	Jablonné	k1gNnSc6
v	v	k7c6
Podještědí	Podještědí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
zrušení	zrušení	k1gNnSc6
krajských	krajský	k2eAgInPc2d1
národních	národní	k2eAgInPc2d1
výborů	výbor	k1gInPc2
a	a	k8xC
jejich	jejich	k3xOp3gNnPc2
středisek	středisko	k1gNnPc2
byly	být	k5eAaImAgFnP
správy	správa	k1gFnSc2
všech	všecek	k3xTgFnPc2
chráněných	chráněný	k2eAgFnPc2d1
krajinných	krajinný	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
lednu	leden	k1gInSc3
1991	#num#	k4
předány	předat	k5eAaPmNgFnP
pod	pod	k7c4
Český	český	k2eAgInSc4d1
ústav	ústav	k1gInSc4
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
řízené	řízený	k2eAgNnSc1d1
Ministerstvem	ministerstvo	k1gNnSc7
kultury	kultura	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brzy	brzy	k6eAd1
poté	poté	k6eAd1
vzniklo	vzniknout	k5eAaPmAgNnS
Ministerstvo	ministerstvo	k1gNnSc1
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
ČR	ČR	kA
<g/>
,	,	kIx,
podřídilo	podřídit	k5eAaPmAgNnS
si	se	k3xPyFc3
všech	všecek	k3xTgNnPc2
23	#num#	k4
správ	správa	k1gFnPc2
chráněných	chráněný	k2eAgFnPc2d1
krajinných	krajinný	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
a	a	k8xC
začlenilo	začlenit	k5eAaPmAgNnS
je	on	k3xPp3gInPc4
do	do	k7c2
samostatného	samostatný	k2eAgInSc2d1
útvaru	útvar	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
březnu	březen	k1gInSc6
1995	#num#	k4
ministr	ministr	k1gMnSc1
zrušil	zrušit	k5eAaPmAgMnS
Český	český	k2eAgInSc4d1
ústav	ústav	k1gInSc4
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
a	a	k8xC
zřídil	zřídit	k5eAaPmAgMnS
dvě	dva	k4xCgFnPc4
nové	nový	k2eAgFnPc4d1
organizace	organizace	k1gFnPc4
–	–	k?
Agenturu	agentura	k1gFnSc4
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
a	a	k8xC
krajiny	krajina	k1gFnSc2
ČR	ČR	kA
a	a	k8xC
Správu	správa	k1gFnSc4
chráněných	chráněný	k2eAgFnPc2d1
krajinných	krajinný	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obě	dva	k4xCgFnPc1
organizace	organizace	k1gFnPc1
měly	mít	k5eAaImAgFnP
sídlo	sídlo	k1gNnSc4
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
po	po	k7c6
republice	republika	k1gFnSc6
mají	mít	k5eAaImIp3nP
detašovaná	detašovaný	k2eAgNnPc1d1
pracoviště	pracoviště	k1gNnPc1
<g/>
.	.	kIx.
</s>
<s>
Své	svůj	k3xOyFgNnSc4
sídlo	sídlo	k1gNnSc4
Správa	správa	k1gFnSc1
CHKO	CHKO	kA
Lužické	lužický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
musela	muset	k5eAaImAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1991	#num#	k4
opustit	opustit	k5eAaPmF
<g/>
,	,	kIx,
a	a	k8xC
přestěhovala	přestěhovat	k5eAaPmAgFnS
se	se	k3xPyFc4
v	v	k7c6
říjnu	říjen	k1gInSc6
do	do	k7c2
budovy	budova	k1gFnSc2
Městského	městský	k2eAgInSc2d1
úřadu	úřad	k1gInSc2
v	v	k7c6
Novém	nový	k2eAgInSc6d1
Boru	bor	k1gInSc6
na	na	k7c6
náměstí	náměstí	k1gNnSc6
Míru	mír	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tehdy	tehdy	k6eAd1
měla	mít	k5eAaImAgFnS
osm	osm	k4xCc4
pracovníků	pracovník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brzo	brzo	k6eAd1
se	se	k3xPyFc4
ukázaly	ukázat	k5eAaPmAgFnP
nové	nový	k2eAgFnPc1d1
prostory	prostora	k1gFnPc1
jako	jako	k8xC,k8xS
nedostatečné	dostatečný	k2eNgFnPc1d1
<g/>
,	,	kIx,
protože	protože	k8xS
se	se	k3xPyFc4
přistěhovaly	přistěhovat	k5eAaPmAgFnP
do	do	k7c2
budovy	budova	k1gFnSc2
další	další	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
době	doba	k1gFnSc6
svého	svůj	k3xOyFgNnSc2
založení	založení	k1gNnSc2
(	(	kIx(
<g/>
rok	rok	k1gInSc1
1976	#num#	k4
<g/>
)	)	kIx)
měla	mít	k5eAaImAgFnS
CHKO	CHKO	kA
tyto	tento	k3xDgFnPc4
chráněné	chráněný	k2eAgFnPc4d1
lokality	lokalita	k1gFnPc4
<g/>
:	:	kIx,
</s>
<s>
Státní	státní	k2eAgFnPc1d1
přírodní	přírodní	k2eAgFnPc1d1
rezervace	rezervace	k1gFnPc1
<g/>
:	:	kIx,
Vápenka	vápenka	k1gFnSc1
<g/>
,	,	kIx,
Studený	studený	k2eAgInSc1d1
vrch	vrch	k1gInSc1
<g/>
,	,	kIx,
Klíč	klíč	k1gInSc1
a	a	k8xC
Jezevčí	jezevčí	k2eAgInSc1d1
vrch	vrch	k1gInSc1
</s>
<s>
Chráněná	chráněný	k2eAgNnPc1d1
naleziště	naleziště	k1gNnPc1
<g/>
:	:	kIx,
Kytlice	kytlice	k1gFnSc1
</s>
<s>
Chráněný	chráněný	k2eAgInSc1d1
přírodní	přírodní	k2eAgInSc1d1
výtvor	výtvor	k1gInSc1
<g/>
:	:	kIx,
Pustý	pustý	k2eAgInSc1d1
zámek	zámek	k1gInSc1
<g/>
,	,	kIx,
Naděje	naděje	k1gFnPc1
a	a	k8xC
Bílé	bílý	k2eAgInPc1d1
kameny	kámen	k1gInPc1
</s>
<s>
Maloplošná	Maloplošný	k2eAgNnPc1d1
chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
</s>
<s>
Letecký	letecký	k2eAgInSc1d1
pohled	pohled	k1gInSc1
na	na	k7c4
znělcovou	znělcový	k2eAgFnSc4d1
horu	hora	k1gFnSc4
Klíč	klíč	k1gInSc1
</s>
<s>
Pískovcové	pískovcový	k2eAgInPc1d1
Sloní	sloní	k2eAgInPc1d1
kameny	kámen	k1gInPc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2020	#num#	k4
se	se	k3xPyFc4
v	v	k7c6
chráněné	chráněný	k2eAgFnSc6d1
krajinné	krajinný	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
nacházelo	nacházet	k5eAaImAgNnS
osmnáct	osmnáct	k4xCc1
maloplošných	maloplošný	k2eAgInPc2d1
zvláště	zvláště	k6eAd1
chráněných	chráněný	k2eAgNnPc2d1
území	území	k1gNnPc2
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Národní	národní	k2eAgInSc1d1
přírodní	přírodní	k2eAgInSc1d1
rezervaceJezevčí	rezervaceJezevčí	k2eAgInSc1d1
vrch	vrch	k1gInSc1
</s>
<s>
Národní	národní	k2eAgInSc1d1
přírodní	přírodní	k2eAgInSc1d1
památkaZlatý	památkaZlatý	k2eAgInSc1d1
vrch	vrch	k1gInSc1
</s>
<s>
Přírodní	přírodní	k2eAgFnSc1d1
rezervaceVápenka	rezervaceVápenka	k1gFnSc1
<g/>
,	,	kIx,
Marschnerova	Marschnerův	k2eAgFnSc1d1
louka	louka	k1gFnSc1
<g/>
,	,	kIx,
Studený	studený	k2eAgInSc1d1
vrch	vrch	k1gInSc1
<g/>
,	,	kIx,
Klíč	klíč	k1gInSc1
<g/>
,	,	kIx,
Spravedlnost	spravedlnost	k1gFnSc1
<g/>
,	,	kIx,
Luž	Luž	k1gFnSc1
</s>
<s>
Přírodní	přírodní	k2eAgFnSc1d1
památkyLouka	památkyLouk	k1gMnSc4
u	u	k7c2
Brodských	Brodská	k1gFnPc2
<g/>
,	,	kIx,
Líska	líska	k1gFnSc1
<g/>
,	,	kIx,
Noldenteich	Noldenteich	k1gInSc1
<g/>
,	,	kIx,
Pustý	pustý	k2eAgInSc1d1
zámek	zámek	k1gInSc1
<g/>
,	,	kIx,
Kytlice	kytlice	k1gFnSc1
<g/>
,	,	kIx,
Brazilka	Brazilka	k1gFnSc1
<g/>
,	,	kIx,
Naděje	naděje	k1gFnSc1
<g/>
,	,	kIx,
Rašeliniště	rašeliniště	k1gNnSc1
Mařeničky	Mařenička	k1gFnSc2
<g/>
,	,	kIx,
Bílé	bílý	k2eAgInPc1d1
kameny	kámen	k1gInPc1
<g/>
,	,	kIx,
U	u	k7c2
Rozmoklé	rozmoklý	k2eAgFnSc2d1
žáby	žába	k1gFnSc2
</s>
<s>
Naučné	naučný	k2eAgFnPc1d1
stezky	stezka	k1gFnPc1
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1
naučná	naučný	k2eAgFnSc1d1
stezka	stezka	k1gFnSc1
Lužické	lužický	k2eAgFnSc2d1
a	a	k8xC
Žitavské	žitavský	k2eAgFnSc2d1
hory	hora	k1gFnSc2
–	–	k?
začíná	začínat	k5eAaImIp3nS
poblíž	poblíž	k6eAd1
spojovací	spojovací	k2eAgFnPc4d1
silnice	silnice	k1gFnPc4
mezi	mezi	k7c7
Jablonným	Jablonné	k1gNnSc7
v	v	k7c6
Podještědí	Podještědí	k1gNnSc6
a	a	k8xC
Chrastavou	chrastavý	k2eAgFnSc4d1
ve	v	k7c6
východní	východní	k2eAgFnSc6d1
části	část	k1gFnSc6
hor	hora	k1gFnPc2
a	a	k8xC
má	mít	k5eAaImIp3nS
27	#num#	k4
zastavení	zastavení	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Panely	panel	k1gInPc4
s	s	k7c7
informacemi	informace	k1gFnPc7
jsou	být	k5eAaImIp3nP
věnovány	věnovat	k5eAaPmNgFnP,k5eAaImNgFnP
Lužickým	lužický	k2eAgFnPc3d1
a	a	k8xC
navazujícím	navazující	k2eAgFnPc3d1
Žitavským	žitavský	k2eAgFnPc3d1
horám	hora	k1gFnPc3
<g/>
,	,	kIx,
geologii	geologie	k1gFnSc3
i	i	k8xC
přírodě	příroda	k1gFnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Naučná	naučný	k2eAgFnSc1d1
stezka	stezka	k1gFnSc1
Okolím	okolí	k1gNnSc7
Studence	studenka	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Brazilka	Brazilka	k1gFnSc1
u	u	k7c2
stejnojmenné	stejnojmenný	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Lesnická	lesnický	k2eAgFnSc1d1
naučná	naučný	k2eAgFnSc1d1
stezka	stezka	k1gFnSc1
Sokol	Sokol	k1gMnSc1
</s>
<s>
Köglerova	Köglerův	k2eAgFnSc1d1
naučná	naučný	k2eAgFnSc1d1
stezka	stezka	k1gFnSc1
<g/>
,	,	kIx,
nejstarší	starý	k2eAgFnSc1d3
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
Hornická	hornický	k2eAgFnSc1d1
naučná	naučný	k2eAgFnSc1d1
stezka	stezka	k1gFnSc1
Údolí	údolí	k1gNnSc2
Milířky	Milířka	k1gFnSc2
</s>
<s>
Naučná	naučný	k2eAgFnSc1d1
stezka	stezka	k1gFnSc1
Jánské	Jánská	k1gFnPc1
kameny	kámen	k1gInPc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Lužické	lužický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Agentura	agentura	k1gFnSc1
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
a	a	k8xC
krajiny	krajina	k1gFnSc2
ČR	ČR	kA
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
http://www.luzickehory.ochranaprirody.cz/	http://www.luzickehory.ochranaprirody.cz/	k?
<g/>
↑	↑	k?
MODRÝ	modrý	k2eAgMnSc1d1
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
<g/>
;	;	kIx,
SÝKOROVÁ	Sýkorová	k1gFnSc1
<g/>
,	,	kIx,
Jarmila	Jarmila	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Maloplošná	Maloplošný	k2eAgFnSc1d1
chráněná	chráněný	k2eAgFnSc1d1
území	území	k1gNnSc1
Libereckého	liberecký	k2eAgInSc2d1
kraje	kraj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Liberec	Liberec	k1gInSc1
<g/>
:	:	kIx,
Liberecký	liberecký	k2eAgInSc1d1
kraj	kraj	k1gInSc1
<g/>
,	,	kIx,
referát	referát	k1gInSc1
ŽP	ŽP	kA
a	a	k8xC
zemědělství	zemědělství	k1gNnSc1
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
33	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
KUNCOVÁ	Kuncová	k1gFnSc1
<g/>
,	,	kIx,
Jaromíra	Jaromíra	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
CHKO	CHKO	kA
Lužické	lužický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
dvacetiletá	dvacetiletý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bezděz	Bezděz	k1gInSc1
<g/>
,	,	kIx,
vlastivědný	vlastivědný	k2eAgInSc1d1
sborník	sborník	k1gInSc1
Českolipska	Českolipsko	k1gNnSc2
<g/>
.	.	kIx.
1996	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
4	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1211	#num#	k4
<g/>
-	-	kIx~
<g/>
9172	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
AUTORSKÝ	autorský	k2eAgInSc4d1
KOLEKTIV	kolektiv	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chráněná	chráněný	k2eAgNnPc4d1
území	území	k1gNnSc4
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
3	#num#	k4
<g/>
:	:	kIx,
Informatorium	Informatorium	k1gNnSc1
<g/>
,	,	kIx,
1991	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85368	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
CHKO	CHKO	kA
Lužické	lužický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
67	#num#	k4
<g/>
-	-	kIx~
<g/>
69	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Bezděz	Bezděz	k1gInSc1
1996	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
13	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
PODHORSKÝ	Podhorský	k1gMnSc1
<g/>
,	,	kIx,
Marek	Marek	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Liberecký	liberecký	k2eAgInSc1d1
kraj	kraj	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
freytag	freytag	k1gMnSc1
&	&	k?
berndt	berndt	k1gMnSc1
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7316	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
32	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Českolipsko	Českolipsko	k1gNnSc1
<g/>
,	,	kIx,
s.	s.	k?
23	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
HOLEČEK	Holeček	k1gMnSc1
<g/>
,	,	kIx,
Milan	Milan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lužické	lužický	k2eAgFnPc4d1
hory	hora	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Olympia	Olympia	k1gFnSc1
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7033	#num#	k4
<g/>
-	-	kIx~
<g/>
832	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Naučné	naučný	k2eAgFnSc2d1
stezky	stezka	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
58	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Lužické	lužický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
na	na	k7c4
OpenStreetMap	OpenStreetMap	k1gInSc4
</s>
<s>
Správa	správa	k1gFnSc1
CHKO	CHKO	kA
Lužické	lužický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
v	v	k7c6
okrese	okres	k1gInSc6
Česká	český	k2eAgFnSc1d1
Lípa	lípa	k1gFnSc1
Chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
</s>
<s>
České	český	k2eAgNnSc1d1
středohoří	středohoří	k1gNnSc1
•	•	k?
Kokořínsko	Kokořínsko	k1gNnSc1
–	–	k?
Máchův	Máchův	k2eAgMnSc1d1
kraj	kraj	k7c2
•	•	k?
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Lužické	lužický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Břehyně	Břehyně	k1gFnSc1
–	–	k?
Pecopala	Pecopal	k1gMnSc2
•	•	k?
Jezevčí	jezevčí	k2eAgInSc1d1
vrch	vrch	k1gInSc1
•	•	k?
Novozámecký	Novozámecký	k2eAgInSc1d1
rybník	rybník	k1gInSc1
•	•	k?
Velký	velký	k2eAgInSc1d1
a	a	k8xC
Malý	malý	k2eAgInSc1d1
Bezděz	Bezděz	k1gInSc1
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Jestřebské	Jestřebský	k2eAgFnPc1d1
slatiny	slatina	k1gFnPc1
•	•	k?
Panská	panská	k1gFnSc1
skála	skála	k1gFnSc1
•	•	k?
Peklo	peklo	k1gNnSc1
•	•	k?
Swamp	Swamp	k1gMnSc1
Přírodní	přírodní	k2eAgMnSc1d1
rezervace	rezervace	k1gFnPc4
</s>
<s>
Hradčanské	hradčanský	k2eAgInPc1d1
rybníky	rybník	k1gInPc1
•	•	k?
Jílovka	jílovka	k1gFnSc1
•	•	k?
Klíč	klíč	k1gInSc1
•	•	k?
Kokořínský	Kokořínský	k2eAgInSc4d1
důl	důl	k1gInSc4
•	•	k?
Kostelecké	Kostelecké	k2eAgInPc4d1
bory	bor	k1gInPc4
•	•	k?
Luž	Luž	k1gFnSc1
•	•	k?
Mokřady	mokřad	k1gInPc1
horní	horní	k2eAgFnSc2d1
Liběchovky	Liběchovka	k1gFnSc2
•	•	k?
Ralsko	Ralsko	k1gNnSc1
•	•	k?
Vlhošť	Vlhošť	k1gFnSc1
Přírodní	přírodní	k2eAgFnPc1d1
památky	památka	k1gFnPc1
</s>
<s>
Bobří	bobří	k2eAgFnSc1d1
soutěska	soutěska	k1gFnSc1
•	•	k?
Brazilka	Brazilka	k1gFnSc1
•	•	k?
Cihelenské	Cihelenský	k2eAgInPc1d1
rybníky	rybník	k1gInPc1
•	•	k?
Černý	černý	k2eAgInSc1d1
důl	důl	k1gInSc1
•	•	k?
Červený	červený	k2eAgInSc1d1
rybník	rybník	k1gInSc1
•	•	k?
Česká	český	k2eAgFnSc1d1
Lípa	lípa	k1gFnSc1
–	–	k?
mokřad	mokřad	k1gInSc4
v	v	k7c6
nivě	niva	k1gFnSc6
Šporky	Šporka	k1gMnSc2
•	•	k?
Deštenské	Deštenský	k2eAgFnPc1d1
pastviny	pastvina	k1gFnPc1
•	•	k?
Děvín	Děvín	k1gInSc1
a	a	k8xC
Ostrý	ostrý	k2eAgMnSc1d1
•	•	k?
Divadlo	divadlo	k1gNnSc1
•	•	k?
Dutý	dutý	k2eAgInSc1d1
kámen	kámen	k1gInSc1
•	•	k?
Farská	farský	k2eAgFnSc1d1
louka	louka	k1gFnSc1
•	•	k?
Husa	Hus	k1gMnSc2
•	•	k?
Jelení	jelení	k2eAgInPc4d1
vrchy	vrch	k1gInPc4
•	•	k?
Kamenný	kamenný	k2eAgInSc4d1
vrch	vrch	k1gInSc4
u	u	k7c2
Křenova	Křenov	k1gInSc2
•	•	k?
Kaňon	kaňon	k1gInSc1
potoka	potok	k1gInSc2
Kolné	Kolná	k1gFnSc2
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Manušické	Manušický	k2eAgInPc1d1
rybníky	rybník	k1gInPc1
•	•	k?
Martinské	martinský	k2eAgFnSc2d1
stěny	stěna	k1gFnSc2
•	•	k?
Meandry	meandr	k1gInPc1
Ploučnice	Ploučnice	k1gFnSc2
u	u	k7c2
Mimoně	Mimoň	k1gFnSc2
•	•	k?
Naděje	naděje	k1gFnSc1
•	•	k?
Niva	niva	k1gFnSc1
Ploučnice	Ploučnice	k1gFnSc1
u	u	k7c2
Žizníkova	Žizníkův	k2eAgInSc2d1
•	•	k?
Okřešické	Okřešický	k2eAgFnPc1d1
louky	louka	k1gFnPc1
•	•	k?
Osinalické	Osinalický	k2eAgFnPc1d1
bučiny	bučina	k1gFnPc1
•	•	k?
Pískovna	pískovna	k1gFnSc1
Žizníkov	Žizníkov	k1gInSc1
•	•	k?
Pod	pod	k7c7
Hvězdou	hvězda	k1gFnSc7
•	•	k?
Prameny	pramen	k1gInPc1
Pšovky	Pšovka	k1gFnSc2
•	•	k?
Provodínské	Provodínský	k2eAgInPc1d1
kameny	kámen	k1gInPc1
•	•	k?
Pustý	pustý	k2eAgInSc1d1
zámek	zámek	k1gInSc1
•	•	k?
Rašeliniště	rašeliniště	k1gNnSc2
Černého	Černý	k1gMnSc2
rybníka	rybník	k1gInSc2
•	•	k?
Rašeliniště	rašeliniště	k1gNnSc1
Mařeničky	Mařenička	k1gFnSc2
•	•	k?
Ronov	Ronov	k1gInSc1
•	•	k?
Skalice	Skalice	k1gFnSc2
u	u	k7c2
České	český	k2eAgFnSc2d1
Lípy	lípa	k1gFnSc2
•	•	k?
Stohánek	Stohánek	k1gMnSc1
•	•	k?
Stružnické	Stružnický	k2eAgInPc1d1
rybníky	rybník	k1gInPc1
•	•	k?
Stříbrný	stříbrný	k1gInSc1
vrch	vrch	k1gInSc1
•	•	k?
Široký	široký	k2eAgInSc1d1
kámen	kámen	k1gInSc1
•	•	k?
U	u	k7c2
Rozmoklé	rozmoklý	k2eAgFnSc2d1
žáby	žába	k1gFnSc2
•	•	k?
Vranovské	vranovský	k2eAgFnSc2d1
skály	skála	k1gFnSc2
•	•	k?
Zahrádky	zahrádka	k1gFnSc2
u	u	k7c2
České	český	k2eAgFnSc2d1
Lípy	lípa	k1gFnSc2
</s>
<s>
Chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
v	v	k7c6
okrese	okres	k1gInSc6
Děčín	Děčín	k1gInSc1
Národní	národní	k2eAgInPc1d1
parky	park	k1gInPc1
</s>
<s>
České	český	k2eAgNnSc1d1
Švýcarsko	Švýcarsko	k1gNnSc1
Chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
</s>
<s>
České	český	k2eAgNnSc1d1
středohoří	středohoří	k1gNnSc1
•	•	k?
Labské	labský	k2eAgInPc1d1
pískovce	pískovec	k1gInPc1
•	•	k?
Lužické	lužický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Kaňon	kaňon	k1gInSc1
Labe	Labe	k1gNnSc2
•	•	k?
Růžovský	Růžovský	k2eAgInSc4d1
vrch	vrch	k1gInSc4
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Březinské	Březinský	k2eAgInPc4d1
tisy	tis	k1gInPc4
•	•	k?
Pravčická	Pravčická	k1gFnSc1
brána	brána	k1gFnSc1
•	•	k?
Zlatý	zlatý	k2eAgInSc4d1
vrch	vrch	k1gInSc4
Přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Arba	arba	k1gFnSc1
•	•	k?
Bohyňská	Bohyňský	k2eAgFnSc1d1
lada	lado	k1gNnSc2
•	•	k?
Čabel	Čabel	k1gInSc1
•	•	k?
Holý	holý	k2eAgInSc1d1
vrch	vrch	k1gInSc1
u	u	k7c2
Jílového	Jílové	k1gNnSc2
•	•	k?
Kamenná	kamenný	k2eAgFnSc1d1
hůra	hůra	k1gFnSc1
•	•	k?
Maiberg	Maiberg	k1gInSc1
•	•	k?
Marschnerova	Marschnerův	k2eAgFnSc1d1
louka	louka	k1gFnSc1
•	•	k?
Pavlínino	Pavlínin	k2eAgNnSc4d1
údolí	údolí	k1gNnSc4
•	•	k?
Pekelský	pekelský	k2eAgInSc1d1
důl	důl	k1gInSc1
•	•	k?
Spravedlnost	spravedlnost	k1gFnSc1
•	•	k?
Studený	studený	k2eAgInSc1d1
vrch	vrch	k1gInSc1
•	•	k?
Světlík	světlík	k1gInSc1
•	•	k?
Vápenka	vápenka	k1gFnSc1
•	•	k?
Velký	velký	k2eAgInSc1d1
rybník	rybník	k1gInSc1
•	•	k?
Vrabinec	Vrabinec	k1gInSc1
•	•	k?
Za	za	k7c7
pilou	pila	k1gFnSc7
Přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Bobří	bobří	k2eAgFnSc1d1
soutěska	soutěska	k1gFnSc1
•	•	k?
Hofberg	Hofberg	k1gInSc1
•	•	k?
Jeskyně	jeskyně	k1gFnSc1
pod	pod	k7c7
Sněžníkem	Sněžník	k1gInSc7
•	•	k?
Jílovské	jílovský	k2eAgInPc4d1
tisy	tis	k1gInPc4
•	•	k?
Kytlice	kytlice	k1gFnSc2
•	•	k?
Líska	líska	k1gFnSc1
•	•	k?
Louka	louka	k1gFnSc1
u	u	k7c2
Brodských	Brodská	k1gFnPc2
•	•	k?
Meandry	meandr	k1gInPc1
Chřibské	chřibský	k2eAgFnSc2d1
Kamenice	Kamenice	k1gFnSc2
•	•	k?
Nebočadský	Nebočadský	k2eAgInSc1d1
luh	luh	k1gInSc1
•	•	k?
Noldenteich	Noldenteich	k1gInSc1
•	•	k?
Pod	pod	k7c7
lesem	les	k1gInSc7
•	•	k?
Rybník	rybník	k1gInSc1
u	u	k7c2
Králova	Králův	k2eAgInSc2d1
mlýna	mlýn	k1gInSc2
•	•	k?
Sojčí	sojčí	k2eAgFnSc1d1
rokle	rokle	k1gFnSc1
•	•	k?
Stará	starat	k5eAaImIp3nS
Oleška	Olešek	k1gMnSc2
•	•	k?
Stříbrný	stříbrný	k2eAgInSc4d1
roh	roh	k1gInSc4
</s>
<s>
Chráněné	chráněný	k2eAgFnPc1d1
krajinné	krajinný	k2eAgFnPc1d1
oblasti	oblast	k1gFnPc1
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
Beskydy	Beskyd	k1gInPc1
•	•	k?
Bílé	bílý	k2eAgInPc1d1
Karpaty	Karpaty	k1gInPc1
•	•	k?
Blaník	Blaník	k1gInSc1
•	•	k?
Blanský	blanský	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Brdy	Brdy	k1gInPc1
•	•	k?
Broumovsko	Broumovsko	k1gNnSc4
•	•	k?
České	český	k2eAgNnSc1d1
středohoří	středohoří	k1gNnSc1
•	•	k?
Český	český	k2eAgInSc1d1
kras	kras	k1gInSc1
•	•	k?
Český	český	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Český	český	k2eAgInSc1d1
ráj	ráj	k1gInSc1
•	•	k?
Jeseníky	Jeseník	k1gInPc1
•	•	k?
Jizerské	jizerský	k2eAgFnSc2d1
hory	hora	k1gFnSc2
•	•	k?
Kokořínsko	Kokořínsko	k1gNnSc1
–	–	k?
Máchův	Máchův	k2eAgInSc1d1
kraj	kraj	k1gInSc1
•	•	k?
Křivoklátsko	Křivoklátsko	k1gNnSc1
•	•	k?
Labské	labský	k2eAgInPc1d1
pískovce	pískovec	k1gInPc1
•	•	k?
Litovelské	litovelský	k2eAgNnSc1d1
Pomoraví	Pomoraví	k1gNnSc1
•	•	k?
Lužické	lužický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
•	•	k?
Moravský	moravský	k2eAgInSc4d1
kras	kras	k1gInSc4
•	•	k?
Orlické	orlický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
•	•	k?
Pálava	Pálava	k1gFnSc1
•	•	k?
Poodří	Poodří	k1gNnPc2
•	•	k?
Slavkovský	slavkovský	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Šumava	Šumava	k1gFnSc1
•	•	k?
Třeboňsko	Třeboňsko	k1gNnSc1
•	•	k?
Žďárské	Žďárské	k2eAgInPc1d1
vrchy	vrch	k1gInPc1
•	•	k?
Železné	železný	k2eAgFnSc2d1
hory	hora	k1gFnSc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
138025	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Šluknovský	šluknovský	k2eAgInSc1d1
výběžek	výběžek	k1gInSc1
</s>
