<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Lužické	lužický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
severních	severní	k2eAgFnPc6d1
Čechách	Čechy	k1gFnPc6
na	na	k7c6
rozhraní	rozhraní	k1gNnSc6
Ústeckého	ústecký	k2eAgInSc2d1
a	a	k8xC
Libereckého	liberecký	k2eAgInSc2d1
kraje	kraj	k1gInSc2
v	v	k7c6
okresech	okres	k1gInPc6
Děčín	Děčín	k1gInSc1
<g/>
,	,	kIx,
Česká	český	k2eAgFnSc1d1
Lípa	lípa	k1gFnSc1
a	a	k8xC
Liberec	Liberec	k1gInSc1
<g/>
.	.	kIx.
</s>