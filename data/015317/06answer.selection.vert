<s desamb="1">
Rozloha	rozloha	k1gFnSc1
chráněného	chráněný	k2eAgNnSc2d1
území	území	k1gNnSc2
je	být	k5eAaImIp3nS
270,72	270,72	k4
km²	km²	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Předmětem	předmět	k1gInSc7
ochrany	ochrana	k1gFnSc2
je	být	k5eAaImIp3nS
rozmanitá	rozmanitý	k2eAgFnSc1d1
krajina	krajina	k1gFnSc1
pískovcových	pískovcový	k2eAgNnPc2d1
skalních	skalní	k2eAgNnPc2d1
měst	město	k1gNnPc2
a	a	k8xC
znělcových	znělcový	k2eAgInPc2d1
<g/>
,	,	kIx,
trachytových	trachytový	k2eAgInPc2d1
a	a	k8xC
čedičových	čedičový	k2eAgInPc2d1
kuželů	kužel	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Tyto	tento	k3xDgInPc1
význačné	význačný	k2eAgInPc1d1
geomorfologické	geomorfologický	k2eAgInPc1d1
útvary	útvar	k1gInPc1
jsou	být	k5eAaImIp3nP
doprovázeny	doprovázen	k2eAgInPc1d1
zbytky	zbytek	k1gInPc1
přirozených	přirozený	k2eAgInPc2d1
lesních	lesní	k2eAgInPc2d1
porostů	porost	k1gInPc2
ve	v	k7c6
vrcholových	vrcholový	k2eAgFnPc6d1
partiích	partie	k1gFnPc6
(	(	kIx(
<g/>
buk	buk	k1gInSc4
<g/>
,	,	kIx,
jedle	jedle	k1gFnPc1
<g/>
,	,	kIx,
javor	javor	k1gInSc1
<g/>
,	,	kIx,
jilm	jilm	k1gInSc1
a	a	k8xC
doubrava	doubrava	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vlhkými	vlhký	k2eAgFnPc7d1
horskými	horský	k2eAgFnPc7d1
a	a	k8xC
podhorskými	podhorský	k2eAgFnPc7d1
loukami	louka	k1gFnPc7
s	s	k7c7
výskytem	výskyt	k1gInSc7
vzácných	vzácný	k2eAgInPc2d1
druhů	druh	k1gInPc2
rostlin	rostlina	k1gFnPc2
a	a	k8xC
nivami	niva	k1gFnPc7
potoků	potok	k1gInPc2
<g/>
.	.	kIx.
</s>