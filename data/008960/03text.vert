<p>
<s>
Americká	americký	k2eAgFnSc1d1	americká
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
(	(	kIx(	(
<g/>
1861	[number]	k4	1861
<g/>
–	–	k?	–
<g/>
1865	[number]	k4	1865
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
ozbrojený	ozbrojený	k2eAgInSc4d1	ozbrojený
konflikt	konflikt	k1gInSc4	konflikt
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
probíhal	probíhat	k5eAaImAgInS	probíhat
na	na	k7c6	na
severoamerickém	severoamerický	k2eAgInSc6d1	severoamerický
kontinentu	kontinent	k1gInSc6	kontinent
mezi	mezi	k7c7	mezi
státy	stát	k1gInPc7	stát
Unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
zakladatelskými	zakladatelský	k2eAgInPc7d1	zakladatelský
státy	stát	k1gInPc7	stát
USA	USA	kA	USA
<g/>
,	,	kIx,	,
a	a	k8xC	a
Státy	stát	k1gInPc1	stát
konfederace	konfederace	k1gFnSc2	konfederace
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byla	být	k5eAaImAgFnS	být
koalice	koalice	k1gFnSc1	koalice
jedenácti	jedenáct	k4xCc2	jedenáct
amerických	americký	k2eAgInPc2d1	americký
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
chtěly	chtít	k5eAaImAgInP	chtít
odtrhnout	odtrhnout	k5eAaPmF	odtrhnout
od	od	k7c2	od
Unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
státy	stát	k1gInPc1	stát
Konfederace	konfederace	k1gFnSc2	konfederace
byly	být	k5eAaImAgInP	být
soustředěny	soustředit	k5eAaPmNgInP	soustředit
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
části	část	k1gFnSc6	část
Unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
nazývá	nazývat	k5eAaImIp3nS	nazývat
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
válka	válka	k1gFnSc1	válka
také	také	k9	také
válkou	válka	k1gFnSc7	válka
Severu	sever	k1gInSc2	sever
proti	proti	k7c3	proti
Jihu	jih	k1gInSc3	jih
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
příčina	příčina	k1gFnSc1	příčina
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
často	často	k6eAd1	často
uvádí	uvádět	k5eAaImIp3nS	uvádět
spor	spor	k1gInSc4	spor
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
povoleno	povolen	k2eAgNnSc1d1	povoleno
otroctví	otroctví	k1gNnSc1	otroctví
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
tento	tento	k3xDgInSc1	tento
výklad	výklad	k1gInSc1	výklad
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
zjednodušený	zjednodušený	k2eAgInSc1d1	zjednodušený
<g/>
,	,	kIx,	,
důvodů	důvod	k1gInPc2	důvod
byla	být	k5eAaImAgFnS	být
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
<g/>
.	.	kIx.	.
</s>
<s>
Podstatou	podstata	k1gFnSc7	podstata
většiny	většina	k1gFnSc2	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Jih	jih	k1gInSc1	jih
usiloval	usilovat	k5eAaImAgInS	usilovat
o	o	k7c4	o
mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgFnSc4d2	veliký
autonomii	autonomie	k1gFnSc4	autonomie
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
států	stát	k1gInPc2	stát
v	v	k7c6	v
Unii	unie	k1gFnSc6	unie
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yIgFnSc7	který
Sever	sever	k1gInSc1	sever
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
prosazoval	prosazovat	k5eAaImAgInS	prosazovat
silnější	silný	k2eAgFnSc4d2	silnější
centrální	centrální	k2eAgFnSc4d1	centrální
vládu	vláda	k1gFnSc4	vláda
<g/>
,	,	kIx,	,
nesouhlasil	souhlasit	k5eNaImAgMnS	souhlasit
<g/>
.	.	kIx.	.
</s>
<s>
Bezprostřední	bezprostřední	k2eAgFnSc1d1	bezprostřední
otázka	otázka	k1gFnSc1	otázka
zrušení	zrušení	k1gNnSc2	zrušení
otroctví	otroctví	k1gNnSc2	otroctví
se	se	k3xPyFc4	se
často	často	k6eAd1	často
přeceňuje	přeceňovat	k5eAaImIp3nS	přeceňovat
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
se	se	k3xPyFc4	se
Jih	jih	k1gInSc1	jih
obával	obávat	k5eAaImAgInS	obávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gNnSc1	jeho
zrušení	zrušení	k1gNnSc1	zrušení
bude	být	k5eAaImBp3nS	být
logickým	logický	k2eAgInSc7d1	logický
důsledkem	důsledek	k1gInSc7	důsledek
růstu	růst	k1gInSc2	růst
moci	moc	k1gFnSc2	moc
vlády	vláda	k1gFnSc2	vláda
ve	v	k7c6	v
Washingtonu	Washington	k1gInSc6	Washington
<g/>
,	,	kIx,	,
otroctví	otroctví	k1gNnSc1	otroctví
ovšem	ovšem	k9	ovšem
stálo	stát	k5eAaImAgNnS	stát
(	(	kIx(	(
<g/>
ne	ne	k9	ne
vždy	vždy	k6eAd1	vždy
zcela	zcela	k6eAd1	zcela
zřetelně	zřetelně	k6eAd1	zřetelně
<g/>
)	)	kIx)	)
v	v	k7c6	v
pozadí	pozadí	k1gNnSc6	pozadí
i	i	k8xC	i
ostatních	ostatní	k2eAgInPc2d1	ostatní
rozporů	rozpor	k1gInPc2	rozpor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Válka	válka	k1gFnSc1	válka
přinesla	přinést	k5eAaPmAgFnS	přinést
asi	asi	k9	asi
970	[number]	k4	970
000	[number]	k4	000
obětí	oběť	k1gFnPc2	oběť
(	(	kIx(	(
<g/>
přes	přes	k7c4	přes
3	[number]	k4	3
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
kolem	kolem	k7c2	kolem
600	[number]	k4	600
000	[number]	k4	000
mrtvých	mrtvý	k1gMnPc2	mrtvý
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
převyšuje	převyšovat	k5eAaImIp3nS	převyšovat
ztráty	ztráta	k1gFnPc4	ztráta
amerických	americký	k2eAgNnPc2d1	americké
vojsk	vojsko	k1gNnPc2	vojsko
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
ostatních	ostatní	k2eAgInPc6d1	ostatní
konfliktech	konflikt	k1gInPc6	konflikt
dohromady	dohromady	k6eAd1	dohromady
<g/>
.	.	kIx.	.
</s>
<s>
Občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
přinesla	přinést	k5eAaPmAgFnS	přinést
první	první	k4xOgNnPc4	první
masivní	masivní	k2eAgNnPc4d1	masivní
využití	využití	k1gNnPc4	využití
telegrafu	telegraf	k1gInSc2	telegraf
<g/>
,	,	kIx,	,
železnice	železnice	k1gFnSc2	železnice
a	a	k8xC	a
kulometů	kulomet	k1gInPc2	kulomet
v	v	k7c6	v
boji	boj	k1gInSc6	boj
a	a	k8xC	a
skončila	skončit	k5eAaPmAgFnS	skončit
porážkou	porážka	k1gFnSc7	porážka
populačně	populačně	k6eAd1	populačně
i	i	k9	i
průmyslově	průmyslově	k6eAd1	průmyslově
slabšího	slabý	k2eAgInSc2d2	slabší
Jihu	jih	k1gInSc2	jih
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gInSc7	její
důsledkem	důsledek	k1gInSc7	důsledek
bylo	být	k5eAaImAgNnS	být
posílení	posílení	k1gNnSc1	posílení
centrální	centrální	k2eAgFnSc2d1	centrální
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
zrušení	zrušení	k1gNnSc1	zrušení
otroctví	otroctví	k1gNnSc1	otroctví
a	a	k8xC	a
zbídačení	zbídačení	k1gNnSc1	zbídačení
Jihu	jih	k1gInSc2	jih
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
rozšíření	rozšíření	k1gNnSc3	rozšíření
občanských	občanský	k2eAgFnPc2d1	občanská
svobod	svoboda	k1gFnPc2	svoboda
<g/>
,	,	kIx,	,
další	další	k2eAgFnSc2d1	další
územní	územní	k2eAgFnSc2d1	územní
expanze	expanze	k1gFnSc2	expanze
USA	USA	kA	USA
a	a	k8xC	a
mohutný	mohutný	k2eAgInSc1d1	mohutný
hospodářský	hospodářský	k2eAgInSc1d1	hospodářský
rozmach	rozmach	k1gInSc1	rozmach
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
Jihu	jih	k1gInSc2	jih
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Předcházející	předcházející	k2eAgInSc4d1	předcházející
vývoj	vývoj	k1gInSc4	vývoj
==	==	k?	==
</s>
</p>
<p>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1776	[number]	k4	1776
vyhlášena	vyhlášen	k2eAgFnSc1d1	vyhlášena
nezávislost	nezávislost	k1gFnSc1	nezávislost
USA	USA	kA	USA
a	a	k8xC	a
vyřešeny	vyřešen	k2eAgInPc1d1	vyřešen
vnitřní	vnitřní	k2eAgInPc1d1	vnitřní
problémy	problém	k1gInPc1	problém
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Amerika	Amerika	k1gFnSc1	Amerika
pustila	pustit	k5eAaPmAgFnS	pustit
do	do	k7c2	do
obchodního	obchodní	k2eAgInSc2d1	obchodní
a	a	k8xC	a
územního	územní	k2eAgInSc2d1	územní
výpadu	výpad	k1gInSc2	výpad
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
francouzské	francouzský	k2eAgFnSc2d1	francouzská
revoluce	revoluce	k1gFnSc2	revoluce
a	a	k8xC	a
za	za	k7c2	za
napoleonských	napoleonský	k2eAgFnPc2d1	napoleonská
válek	válka	k1gFnPc2	válka
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
bylo	být	k5eAaImAgNnS	být
využíváno	využívat	k5eAaImNgNnS	využívat
neutrality	neutralita	k1gFnPc4	neutralita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1812	[number]	k4	1812
<g/>
–	–	k?	–
<g/>
1814	[number]	k4	1814
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
nerozhodná	rozhodný	k2eNgFnSc1d1	nerozhodná
válka	válka	k1gFnSc1	válka
s	s	k7c7	s
Velkou	velký	k2eAgFnSc7d1	velká
Británií	Británie	k1gFnSc7	Británie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
Spojeným	spojený	k2eAgInPc3d1	spojený
státům	stát	k1gInPc3	stát
mnoho	mnoho	k6eAd1	mnoho
nepřinesla	přinést	k5eNaPmAgNnP	přinést
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1803	[number]	k4	1803
ale	ale	k8xC	ale
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
od	od	k7c2	od
Napoleona	Napoleon	k1gMnSc2	Napoleon
odkoupily	odkoupit	k5eAaPmAgFnP	odkoupit
Louisianu	Louisiana	k1gFnSc4	Louisiana
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
zdvojnásobily	zdvojnásobit	k5eAaPmAgFnP	zdvojnásobit
své	svůj	k3xOyFgNnSc4	svůj
území	území	k1gNnSc4	území
<g/>
.	.	kIx.	.
</s>
<s>
Nadále	nadále	k6eAd1	nadále
také	také	k9	také
pokračovaly	pokračovat	k5eAaImAgInP	pokračovat
problémy	problém	k1gInPc1	problém
s	s	k7c7	s
Indiány	Indián	k1gMnPc7	Indián
<g/>
,	,	kIx,	,
jež	jenž	k3xRgMnPc4	jenž
často	často	k6eAd1	často
končily	končit	k5eAaImAgInP	končit
jejich	jejich	k3xOp3gFnSc4	jejich
masakrem	masakr	k1gInSc7	masakr
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1819	[number]	k4	1819
získaly	získat	k5eAaPmAgFnP	získat
USA	USA	kA	USA
od	od	k7c2	od
Španělů	Španěl	k1gMnPc2	Španěl
poloostrov	poloostrov	k1gInSc1	poloostrov
Floridu	Florida	k1gFnSc4	Florida
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1823	[number]	k4	1823
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
prezident	prezident	k1gMnSc1	prezident
James	James	k1gMnSc1	James
Monroe	Monro	k1gFnSc2	Monro
Monroeovu	Monroeův	k2eAgFnSc4d1	Monroeova
doktrínu	doktrína	k1gFnSc4	doktrína
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
deklarovala	deklarovat	k5eAaBmAgFnS	deklarovat
jako	jako	k9	jako
cíl	cíl	k1gInSc4	cíl
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
politiky	politika	k1gFnSc2	politika
USA	USA	kA	USA
vzájemné	vzájemný	k2eAgNnSc1d1	vzájemné
nevměšování	nevměšování	k1gNnSc1	nevměšování
se	se	k3xPyFc4	se
evropských	evropský	k2eAgFnPc2d1	Evropská
a	a	k8xC	a
amerických	americký	k2eAgFnPc2d1	americká
zemí	zem	k1gFnPc2	zem
do	do	k7c2	do
záležitostí	záležitost	k1gFnPc2	záležitost
druhého	druhý	k4xOgInSc2	druhý
kontinentu	kontinent	k1gInSc2	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Reagovala	reagovat	k5eAaBmAgFnS	reagovat
na	na	k7c4	na
zásahy	zásah	k1gInPc4	zásah
některých	některý	k3yIgInPc2	některý
evropských	evropský	k2eAgInPc2d1	evropský
států	stát	k1gInPc2	stát
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Dočasně	dočasně	k6eAd1	dočasně
byla	být	k5eAaImAgFnS	být
opuštěna	opustit	k5eAaPmNgFnS	opustit
během	během	k7c2	během
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
a	a	k8xC	a
definitivně	definitivně	k6eAd1	definitivně
při	při	k7c6	při
druhé	druhý	k4xOgFnSc6	druhý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1845	[number]	k4	1845
USA	USA	kA	USA
obsadily	obsadit	k5eAaPmAgFnP	obsadit
Texas	Texas	k1gInSc4	Texas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1846	[number]	k4	1846
až	až	k9	až
1848	[number]	k4	1848
vedly	vést	k5eAaImAgFnP	vést
válku	válka	k1gFnSc4	válka
s	s	k7c7	s
Mexikem	Mexiko	k1gNnSc7	Mexiko
a	a	k8xC	a
získaly	získat	k5eAaPmAgFnP	získat
<g/>
:	:	kIx,	:
Kalifornii	Kalifornie	k1gFnSc4	Kalifornie
<g/>
,	,	kIx,	,
Arizonu	Arizona	k1gFnSc4	Arizona
<g/>
,	,	kIx,	,
Nové	Nové	k2eAgNnSc4d1	Nové
Mexiko	Mexiko	k1gNnSc4	Mexiko
a	a	k8xC	a
Oregon	Oregon	k1gNnSc4	Oregon
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
víc	hodně	k6eAd2	hodně
se	se	k3xPyFc4	se
také	také	k9	také
vyhrocovaly	vyhrocovat	k5eAaImAgInP	vyhrocovat
spory	spor	k1gInPc1	spor
mezi	mezi	k7c7	mezi
jižní	jižní	k2eAgFnSc7d1	jižní
a	a	k8xC	a
severní	severní	k2eAgFnSc7d1	severní
částí	část	k1gFnSc7	část
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
převažovalo	převažovat	k5eAaImAgNnS	převažovat
pěstování	pěstování	k1gNnSc4	pěstování
plodin	plodina	k1gFnPc2	plodina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
vyváženy	vyvážet	k5eAaImNgFnP	vyvážet
<g/>
,	,	kIx,	,
sever	sever	k1gInSc1	sever
USA	USA	kA	USA
byl	být	k5eAaImAgInS	být
průmyslovější	průmyslový	k2eAgInSc1d2	průmyslový
a	a	k8xC	a
dovozový	dovozový	k2eAgInSc1d1	dovozový
<g/>
.	.	kIx.	.
</s>
<s>
Nejzásadnějším	zásadní	k2eAgInSc7d3	nejzásadnější
sporem	spor	k1gInSc7	spor
vedoucím	vedoucí	k2eAgInSc7d1	vedoucí
až	až	k6eAd1	až
k	k	k7c3	k
občanské	občanský	k2eAgFnSc3d1	občanská
válce	válka	k1gFnSc3	válka
byla	být	k5eAaImAgFnS	být
výše	výše	k1gFnSc1	výše
cel	clo	k1gNnPc2	clo
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
chtěl	chtít	k5eAaImAgInS	chtít
mít	mít	k5eAaImF	mít
Jih	jih	k1gInSc1	jih
co	co	k9	co
nejmenší	malý	k2eAgMnSc1d3	nejmenší
a	a	k8xC	a
sever	sever	k1gInSc1	sever
naopak	naopak	k6eAd1	naopak
co	co	k3yRnSc4	co
nejvyšší	vysoký	k2eAgMnSc1d3	nejvyšší
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příčiny	příčina	k1gFnPc4	příčina
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Otroctví	otroctví	k1gNnSc1	otroctví
a	a	k8xC	a
clo	clo	k1gNnSc1	clo
===	===	k?	===
</s>
</p>
<p>
<s>
Příčinou	příčina	k1gFnSc7	příčina
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgInS	být
dlouhodobý	dlouhodobý	k2eAgInSc1d1	dlouhodobý
odlišně	odlišně	k6eAd1	odlišně
probíhající	probíhající	k2eAgInSc1d1	probíhající
vývoj	vývoj	k1gInSc1	vývoj
Severu	sever	k1gInSc2	sever
a	a	k8xC	a
Jihu	jih	k1gInSc2	jih
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Jihu	jih	k1gInSc6	jih
si	se	k3xPyFc3	se
bohatí	bohatý	k2eAgMnPc1d1	bohatý
plantážníci	plantážník	k1gMnPc1	plantážník
uvědomili	uvědomit	k5eAaPmAgMnP	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
sotva	sotva	k6eAd1	sotva
seženou	sehnat	k5eAaPmIp3nP	sehnat
dostatek	dostatek	k1gInSc4	dostatek
bílých	bílý	k2eAgMnPc2d1	bílý
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
budou	být	k5eAaImBp3nP	být
ochotni	ochoten	k2eAgMnPc1d1	ochoten
pracovat	pracovat	k5eAaImF	pracovat
v	v	k7c6	v
tamním	tamní	k2eAgNnSc6d1	tamní
horkém	horký	k2eAgNnSc6d1	horké
podnebí	podnebí	k1gNnSc6	podnebí
<g/>
.	.	kIx.	.
</s>
<s>
Indiáni	Indián	k1gMnPc1	Indián
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
brzy	brzy	k6eAd1	brzy
ukázali	ukázat	k5eAaPmAgMnP	ukázat
jako	jako	k8xS	jako
málo	málo	k6eAd1	málo
odolní	odolný	k2eAgMnPc1d1	odolný
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
představa	představa	k1gFnSc1	představa
černochů	černoch	k1gMnPc2	černoch
<g/>
,	,	kIx,	,
zvyklých	zvyklý	k2eAgMnPc2d1	zvyklý
na	na	k7c4	na
horké	horký	k2eAgNnSc4d1	horké
podnebí	podnebí	k1gNnSc4	podnebí
<g/>
,	,	kIx,	,
zdála	zdát	k5eAaImAgFnS	zdát
jako	jako	k9	jako
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
si	se	k3xPyFc3	se
už	už	k6eAd1	už
žádný	žádný	k3yNgMnSc1	žádný
Jižan	Jižan	k1gMnSc1	Jižan
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
představit	představit	k5eAaPmF	představit
život	život	k1gInSc4	život
bez	bez	k7c2	bez
otroků	otrok	k1gMnPc2	otrok
<g/>
.	.	kIx.	.
</s>
<s>
Největšími	veliký	k2eAgMnPc7d3	veliký
vlastníky	vlastník	k1gMnPc7	vlastník
byli	být	k5eAaImAgMnP	být
plantážníci	plantážník	k1gMnPc1	plantážník
pěstující	pěstující	k2eAgFnSc4d1	pěstující
převážně	převážně	k6eAd1	převážně
bavlnu	bavlna	k1gFnSc4	bavlna
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
byli	být	k5eAaImAgMnP	být
privilegovanou	privilegovaný	k2eAgFnSc7d1	privilegovaná
vrstvou	vrstva	k1gFnSc7	vrstva
jižanského	jižanský	k2eAgNnSc2d1	jižanské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnPc1	jejich
plantáže	plantáž	k1gFnPc1	plantáž
produkovaly	produkovat	k5eAaImAgFnP	produkovat
většinu	většina	k1gFnSc4	většina
zemědělských	zemědělský	k2eAgInPc2d1	zemědělský
výrobků	výrobek	k1gInPc2	výrobek
celých	celý	k2eAgNnPc2d1	celé
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1860	[number]	k4	1860
se	se	k3xPyFc4	se
vyvezlo	vyvézt	k5eAaPmAgNnS	vyvézt
z	z	k7c2	z
USA	USA	kA	USA
57	[number]	k4	57
%	%	kIx~	%
sklizně	sklizeň	k1gFnSc2	sklizeň
bavlny	bavlna	k1gFnSc2	bavlna
do	do	k7c2	do
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
do	do	k7c2	do
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
bavlně	bavlna	k1gFnSc6	bavlna
velká	velký	k2eAgFnSc1d1	velká
poptávka	poptávka	k1gFnSc1	poptávka
<g/>
.	.	kIx.	.
</s>
<s>
Americká	americký	k2eAgFnSc1d1	americká
bavlna	bavlna	k1gFnSc1	bavlna
byla	být	k5eAaImAgFnS	být
nejkvalitnější	kvalitní	k2eAgFnSc1d3	nejkvalitnější
a	a	k8xC	a
díky	díky	k7c3	díky
otrokářskému	otrokářský	k2eAgInSc3d1	otrokářský
systému	systém	k1gInSc3	systém
také	také	k6eAd1	také
nejlevnější	levný	k2eAgFnSc1d3	nejlevnější
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1860	[number]	k4	1860
se	se	k3xPyFc4	se
nacházely	nacházet	k5eAaImAgFnP	nacházet
v	v	k7c6	v
USA	USA	kA	USA
4	[number]	k4	4
miliony	milion	k4xCgInPc1	milion
otroků	otrok	k1gMnPc2	otrok
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
plantážníci	plantážník	k1gMnPc1	plantážník
a	a	k8xC	a
otroci	otrok	k1gMnPc1	otrok
nebyli	být	k5eNaImAgMnP	být
jedinými	jediný	k2eAgMnPc7d1	jediný
obyvateli	obyvatel	k1gMnPc7	obyvatel
Jihu	jih	k1gInSc2	jih
<g/>
.	.	kIx.	.
</s>
<s>
Žila	žít	k5eAaImAgFnS	žít
zde	zde	k6eAd1	zde
i	i	k9	i
takzvaná	takzvaný	k2eAgFnSc1d1	takzvaná
střední	střední	k2eAgFnSc1d1	střední
vrstva	vrstva	k1gFnSc1	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Byli	být	k5eAaImAgMnP	být
to	ten	k3xDgNnSc1	ten
drobní	drobný	k2eAgMnPc1d1	drobný
zemědělci	zemědělec	k1gMnPc1	zemědělec
pracující	pracující	k1gMnPc1	pracující
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
půdě	půda	k1gFnSc6	půda
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
vlastnili	vlastnit	k5eAaImAgMnP	vlastnit
dva	dva	k4xCgMnPc1	dva
až	až	k6eAd1	až
tři	tři	k4xCgMnPc4	tři
otroky	otrok	k1gMnPc4	otrok
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
skupinou	skupina	k1gFnSc7	skupina
obyvatel	obyvatel	k1gMnPc2	obyvatel
byla	být	k5eAaImAgNnP	být
tzv.	tzv.	kA	tzv.
bílá	bílý	k2eAgFnSc1d1	bílá
chátra	chátra	k1gFnSc1	chátra
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byli	být	k5eAaImAgMnP	být
nemajetní	majetný	k2eNgMnPc1d1	nemajetný
bílí	bílý	k2eAgMnPc1d1	bílý
obyvatelé	obyvatel	k1gMnPc1	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Ne	ne	k9	ne
všichni	všechen	k3xTgMnPc1	všechen
bílí	bílý	k2eAgMnPc1d1	bílý
Jižané	Jižan	k1gMnPc1	Jižan
vlastnili	vlastnit	k5eAaImAgMnP	vlastnit
otroky	otrok	k1gMnPc4	otrok
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1860	[number]	k4	1860
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
celkem	celkem	k6eAd1	celkem
393,975	[number]	k4	393,975
majitelů	majitel	k1gMnPc2	majitel
otroků	otrok	k1gMnPc2	otrok
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
reprezentovali	reprezentovat	k5eAaImAgMnP	reprezentovat
8	[number]	k4	8
<g/>
%	%	kIx~	%
amerických	americký	k2eAgFnPc2d1	americká
rodin	rodina	k1gFnPc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Jihu	jih	k1gInSc6	jih
vlastnilo	vlastnit	k5eAaImAgNnS	vlastnit
otroky	otrok	k1gMnPc7	otrok
celkem	celkem	k6eAd1	celkem
33	[number]	k4	33
<g/>
%	%	kIx~	%
všech	všecek	k3xTgFnPc2	všecek
rodin	rodina	k1gFnPc2	rodina
<g/>
.	.	kIx.	.
<g/>
O	o	k7c6	o
starém	starý	k2eAgInSc6d1	starý
Jihu	jih	k1gInSc6	jih
existuje	existovat	k5eAaImIp3nS	existovat
spousta	spousta	k1gFnSc1	spousta
mýtů	mýtus	k1gInPc2	mýtus
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
mluví	mluvit	k5eAaImIp3nS	mluvit
o	o	k7c6	o
pyšných	pyšný	k2eAgFnPc6d1	pyšná
<g/>
,	,	kIx,	,
vzdělaných	vzdělaný	k2eAgFnPc6d1	vzdělaná
mužích	muž	k1gMnPc6	muž
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
brali	brát	k5eAaImAgMnP	brát
otroctví	otroctví	k1gNnSc4	otroctví
jako	jako	k8xS	jako
nutnost	nutnost	k1gFnSc4	nutnost
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
které	který	k3yIgFnSc2	který
by	by	kYmCp3nP	by
jejich	jejich	k3xOp3gFnPc1	jejich
plantáže	plantáž	k1gFnPc1	plantáž
nepřežily	přežít	k5eNaPmAgFnP	přežít
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgMnSc1	druhý
mluví	mluvit	k5eAaImIp3nS	mluvit
o	o	k7c6	o
despotech	despot	k1gMnPc6	despot
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
bičují	bičovat	k5eAaImIp3nP	bičovat
své	svůj	k3xOyFgMnPc4	svůj
otroky	otrok	k1gMnPc4	otrok
<g/>
,	,	kIx,	,
chovají	chovat	k5eAaImIp3nP	chovat
se	se	k3xPyFc4	se
nelidsky	lidsky	k6eNd1	lidsky
a	a	k8xC	a
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
krutosti	krutost	k1gFnSc6	krutost
si	se	k3xPyFc3	se
libují	libovat	k5eAaImIp3nP	libovat
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
není	být	k5eNaImIp3nS	být
ten	ten	k3xDgInSc4	ten
pravý	pravý	k2eAgInSc4d1	pravý
<g/>
.	.	kIx.	.
</s>
<s>
Pravdu	pravda	k1gFnSc4	pravda
musíme	muset	k5eAaImIp1nP	muset
hledat	hledat	k5eAaImF	hledat
někde	někde	k6eAd1	někde
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
USA	USA	kA	USA
se	se	k3xPyFc4	se
rozvíjel	rozvíjet	k5eAaImAgInS	rozvíjet
průmysl	průmysl	k1gInSc1	průmysl
a	a	k8xC	a
rozdílný	rozdílný	k2eAgInSc1d1	rozdílný
pohled	pohled	k1gInSc1	pohled
na	na	k7c6	na
otroctví	otroctví	k1gNnSc6	otroctví
byl	být	k5eAaImAgInS	být
nejvýraznějším	výrazný	k2eAgInSc7d3	nejvýraznější
rysem	rys	k1gInSc7	rys
odlišující	odlišující	k2eAgFnSc4d1	odlišující
Unii	unie	k1gFnSc4	unie
a	a	k8xC	a
pozdější	pozdní	k2eAgFnSc4d2	pozdější
Konfederaci	konfederace	k1gFnSc4	konfederace
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
průmyslovém	průmyslový	k2eAgInSc6d1	průmyslový
Severu	sever	k1gInSc6	sever
se	se	k3xPyFc4	se
nacházelo	nacházet	k5eAaImAgNnS	nacházet
obrovské	obrovský	k2eAgNnSc4d1	obrovské
přírodní	přírodní	k2eAgNnSc4d1	přírodní
bohatství	bohatství	k1gNnSc4	bohatství
<g/>
,	,	kIx,	,
neustále	neustále	k6eAd1	neustále
přicházeli	přicházet	k5eAaImAgMnP	přicházet
další	další	k2eAgMnPc1d1	další
přistěhovalci	přistěhovalec	k1gMnPc1	přistěhovalec
z	z	k7c2	z
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
dynamicky	dynamicky	k6eAd1	dynamicky
rostl	růst	k5eAaImAgInS	růst
vnitřní	vnitřní	k2eAgInSc4d1	vnitřní
trh	trh	k1gInSc4	trh
<g/>
,	,	kIx,	,
k	k	k7c3	k
jehož	jehož	k3xOyRp3gInSc3	jehož
rozmachu	rozmach	k1gInSc3	rozmach
vedlo	vést	k5eAaImAgNnS	vést
další	další	k2eAgNnSc1d1	další
dobývání	dobývání	k1gNnSc4	dobývání
a	a	k8xC	a
osídlování	osídlování	k1gNnSc4	osídlování
Západu	západ	k1gInSc2	západ
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Severu	sever	k1gInSc6	sever
se	se	k3xPyFc4	se
budovaly	budovat	k5eAaImAgFnP	budovat
železniční	železniční	k2eAgFnPc1d1	železniční
cesty	cesta	k1gFnPc1	cesta
a	a	k8xC	a
v	v	k7c6	v
rychlém	rychlý	k2eAgNnSc6d1	rychlé
tempu	tempo	k1gNnSc6	tempo
se	se	k3xPyFc4	se
rozvíjela	rozvíjet	k5eAaImAgFnS	rozvíjet
oblast	oblast	k1gFnSc1	oblast
bankovnictví	bankovnictví	k1gNnSc2	bankovnictví
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1850	[number]	k4	1850
činila	činit	k5eAaImAgFnS	činit
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
produkce	produkce	k1gFnSc1	produkce
Jihu	jih	k1gInSc2	jih
168	[number]	k4	168
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
na	na	k7c6	na
Severu	sever	k1gInSc6	sever
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
845	[number]	k4	845
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1860	[number]	k4	1860
se	se	k3xPyFc4	se
Sever	sever	k1gInSc1	sever
podílel	podílet	k5eAaImAgInS	podílet
na	na	k7c6	na
celkovém	celkový	k2eAgInSc6d1	celkový
ročním	roční	k2eAgInSc6d1	roční
produktu	produkt	k1gInSc6	produkt
USA	USA	kA	USA
92,5	[number]	k4	92,5
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
vypuknutím	vypuknutí	k1gNnSc7	vypuknutí
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgInS	být
poměr	poměr	k1gInSc1	poměr
sil	síla	k1gFnPc2	síla
jednoznačný	jednoznačný	k2eAgInSc1d1	jednoznačný
–	–	k?	–
v	v	k7c6	v
ozbrojených	ozbrojený	k2eAgFnPc6d1	ozbrojená
silách	síla	k1gFnPc6	síla
měl	mít	k5eAaImAgInS	mít
Sever	sever	k1gInSc1	sever
převahu	převaha	k1gFnSc4	převaha
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
a	a	k8xC	a
ve	v	k7c6	v
výrobě	výroba	k1gFnSc6	výroba
zbraní	zbraň	k1gFnPc2	zbraň
30	[number]	k4	30
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Vše	všechen	k3xTgNnSc1	všechen
tedy	tedy	k9	tedy
nasvědčovalo	nasvědčovat	k5eAaImAgNnS	nasvědčovat
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
ozbrojeném	ozbrojený	k2eAgInSc6d1	ozbrojený
konfliktu	konflikt	k1gInSc6	konflikt
rychle	rychle	k6eAd1	rychle
a	a	k8xC	a
jednoznačně	jednoznačně	k6eAd1	jednoznačně
zvítězí	zvítězit	k5eAaPmIp3nS	zvítězit
Sever	sever	k1gInSc1	sever
<g/>
.	.	kIx.	.
</s>
<s>
Jižané	Jižan	k1gMnPc1	Jižan
však	však	k9	však
měli	mít	k5eAaImAgMnP	mít
výhodu	výhoda	k1gFnSc4	výhoda
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
jejich	jejich	k3xOp3gFnPc6	jejich
řadách	řada	k1gFnPc6	řada
panovalo	panovat	k5eAaImAgNnS	panovat
nadšení	nadšení	k1gNnSc1	nadšení
pro	pro	k7c4	pro
válku	válka	k1gFnSc4	válka
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
o	o	k7c6	o
Severu	sever	k1gInSc6	sever
říct	říct	k5eAaPmF	říct
nedalo	dát	k5eNaPmAgNnS	dát
<g/>
.	.	kIx.	.
</s>
<s>
Diplomaté	diplomat	k1gMnPc1	diplomat
Jihu	jih	k1gInSc2	jih
navíc	navíc	k6eAd1	navíc
spoléhali	spoléhat	k5eAaImAgMnP	spoléhat
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
do	do	k7c2	do
konfliktu	konflikt	k1gInSc2	konflikt
zapojí	zapojit	k5eAaPmIp3nS	zapojit
i	i	k9	i
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
a	a	k8xC	a
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
Jihu	jih	k1gInSc6	jih
pomohou	pomoct	k5eAaPmIp3nP	pomoct
a	a	k8xC	a
společnými	společný	k2eAgFnPc7d1	společná
silami	síla	k1gFnPc7	síla
pak	pak	k6eAd1	pak
zničí	zničit	k5eAaPmIp3nP	zničit
Sever	sever	k1gInSc4	sever
USA	USA	kA	USA
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
tyto	tento	k3xDgFnPc4	tento
dvě	dva	k4xCgFnPc4	dva
evropské	evropský	k2eAgFnPc4d1	Evropská
mocnosti	mocnost	k1gFnPc4	mocnost
nebezpečným	bezpečný	k2eNgMnSc7d1	nebezpečný
hospodářským	hospodářský	k2eAgMnSc7d1	hospodářský
konkurentem	konkurent	k1gMnSc7	konkurent
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgInPc1d1	další
rozpory	rozpor	k1gInPc1	rozpor
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
také	také	k9	také
kvůli	kvůli	k7c3	kvůli
clům	clo	k1gNnPc3	clo
vůči	vůči	k7c3	vůči
Britům	Brit	k1gMnPc3	Brit
<g/>
.	.	kIx.	.
</s>
<s>
Jih	jih	k1gInSc1	jih
chtěl	chtít	k5eAaImAgInS	chtít
cla	clo	k1gNnPc4	clo
co	co	k8xS	co
nejmenší	malý	k2eAgNnPc4d3	nejmenší
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
průmyslníci	průmyslník	k1gMnPc1	průmyslník
<g/>
,	,	kIx,	,
obchodníci	obchodník	k1gMnPc1	obchodník
a	a	k8xC	a
bankéři	bankéř	k1gMnPc1	bankéř
ze	z	k7c2	z
Severu	sever	k1gInSc2	sever
usilovali	usilovat	k5eAaImAgMnP	usilovat
o	o	k7c4	o
pevnou	pevný	k2eAgFnSc4d1	pevná
měnu	měna	k1gFnSc4	měna
a	a	k8xC	a
celní	celní	k2eAgFnSc4d1	celní
ochranu	ochrana	k1gFnSc4	ochrana
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
společenské	společenský	k2eAgInPc4d1	společenský
rozdíly	rozdíl	k1gInPc4	rozdíl
byly	být	k5eAaImAgFnP	být
ale	ale	k8xC	ale
dlouho	dlouho	k6eAd1	dlouho
snahy	snaha	k1gFnPc1	snaha
o	o	k7c4	o
kompromisní	kompromisní	k2eAgNnSc4d1	kompromisní
řešení	řešení	k1gNnSc4	řešení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Missouriský	Missouriský	k2eAgInSc1d1	Missouriský
kompromis	kompromis	k1gInSc1	kompromis
a	a	k8xC	a
Kansaská	kansaský	k2eAgFnSc1d1	Kansaská
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1818	[number]	k4	1818
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
do	do	k7c2	do
Unie	unie	k1gFnSc2	unie
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
později	pozdě	k6eAd2	pozdě
neotrokářská	otrokářský	k2eNgFnSc1d1	otrokářský
část	část	k1gFnSc1	část
USA	USA	kA	USA
<g/>
)	)	kIx)	)
přijata	přijat	k2eAgFnSc1d1	přijata
Missouri	Missouri	k1gFnSc1	Missouri
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
by	by	kYmCp3nS	by
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
převahu	převaha	k1gFnSc4	převaha
otrokářských	otrokářský	k2eAgInPc2d1	otrokářský
států	stát	k1gInPc2	stát
v	v	k7c6	v
Senátu	senát	k1gInSc6	senát
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
byl	být	k5eAaImAgMnS	být
Missourský	missourský	k2eAgInSc4d1	missourský
kompromis	kompromis	k1gInSc4	kompromis
z	z	k7c2	z
20	[number]	k4	20
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1820	[number]	k4	1820
<g/>
,	,	kIx,	,
na	na	k7c6	na
základě	základ	k1gInSc6	základ
kterého	který	k3yQgInSc2	který
byl	být	k5eAaImAgInS	být
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Missouri	Missouri	k1gNnSc7	Missouri
přijat	přijmout	k5eAaPmNgMnS	přijmout
i	i	k9	i
svobodný	svobodný	k2eAgMnSc1d1	svobodný
stát	stát	k5eAaImF	stát
Maine	Main	k1gInSc5	Main
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
stanovena	stanovit	k5eAaPmNgFnS	stanovit
hranice	hranice	k1gFnSc1	hranice
36	[number]	k4	36
<g/>
°	°	k?	°
a	a	k8xC	a
30	[number]	k4	30
<g/>
'	'	kIx"	'
severní	severní	k2eAgFnSc2d1	severní
šířky	šířka	k1gFnSc2	šířka
<g/>
,	,	kIx,	,
za	za	k7c4	za
kterou	který	k3yIgFnSc4	který
se	se	k3xPyFc4	se
nesmělo	smět	k5eNaImAgNnS	smět
otrokářství	otrokářství	k1gNnSc1	otrokářství
šířit	šířit	k5eAaImF	šířit
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
kompromis	kompromis	k1gInSc1	kompromis
byl	být	k5eAaImAgInS	být
zrušen	zrušen	k2eAgInSc1d1	zrušen
dne	den	k1gInSc2	den
30	[number]	k4	30
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1854	[number]	k4	1854
zákonem	zákon	k1gInSc7	zákon
o	o	k7c6	o
Kansasu	Kansas	k1gInSc6	Kansas
a	a	k8xC	a
Nebrasce	Nebraska	k1gFnSc6	Nebraska
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterého	který	k3yQgNnSc2	který
se	se	k3xPyFc4	se
občané	občan	k1gMnPc1	občan
těchto	tento	k3xDgInPc6	tento
teritorií	teritorium	k1gNnPc2	teritorium
měli	mít	k5eAaImAgMnP	mít
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
bude	být	k5eAaImBp3nS	být
povoleno	povolen	k2eAgNnSc1d1	povoleno
otroctví	otroctví	k1gNnSc1	otroctví
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
byla	být	k5eAaImAgFnS	být
kritika	kritika	k1gFnSc1	kritika
z	z	k7c2	z
obou	dva	k4xCgFnPc2	dva
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
Kansasu	Kansas	k1gInSc2	Kansas
přicházeli	přicházet	k5eAaImAgMnP	přicházet
jak	jak	k6eAd1	jak
odpůrci	odpůrce	k1gMnPc1	odpůrce
otrokářství	otrokářství	k1gNnPc2	otrokářství
tak	tak	k8xC	tak
jeho	jeho	k3xOp3gMnPc1	jeho
zastánci	zastánce	k1gMnPc1	zastánce
<g/>
,	,	kIx,	,
vedlo	vést	k5eAaImAgNnS	vést
následně	následně	k6eAd1	následně
k	k	k7c3	k
občanské	občanský	k2eAgFnSc3d1	občanská
válce	válka	k1gFnSc3	válka
v	v	k7c6	v
Kansasu	Kansas	k1gInSc6	Kansas
(	(	kIx(	(
<g/>
1855	[number]	k4	1855
<g/>
-	-	kIx~	-
<g/>
1858	[number]	k4	1858
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
ukončil	ukončit	k5eAaPmAgMnS	ukončit
až	až	k9	až
zásah	zásah	k1gInSc4	zásah
federálních	federální	k2eAgNnPc2d1	federální
vojsk	vojsko	k1gNnPc2	vojsko
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Krvácející	krvácející	k2eAgInSc1d1	krvácející
Kansas	Kansas	k1gInSc1	Kansas
<g/>
"	"	kIx"	"
značně	značně	k6eAd1	značně
přispěl	přispět	k5eAaPmAgMnS	přispět
k	k	k7c3	k
vyhrocení	vyhrocení	k1gNnSc3	vyhrocení
situace	situace	k1gFnSc2	situace
mezi	mezi	k7c7	mezi
Severem	sever	k1gInSc7	sever
a	a	k8xC	a
Jihem	jih	k1gInSc7	jih
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
hlavní	hlavní	k2eAgFnSc1d1	hlavní
příčina	příčina	k1gFnSc1	příčina
pozdějších	pozdní	k2eAgFnPc2d2	pozdější
událostí	událost	k1gFnPc2	událost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Spory	spor	k1gInPc1	spor
v	v	k7c6	v
Kansasu	Kansas	k1gInSc6	Kansas
rozdělily	rozdělit	k5eAaPmAgFnP	rozdělit
politické	politický	k2eAgFnPc1d1	politická
strany	strana	k1gFnPc1	strana
Unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
vládnoucí	vládnoucí	k2eAgFnSc3d1	vládnoucí
Demokratické	demokratický	k2eAgFnSc3d1	demokratická
straně	strana	k1gFnSc3	strana
<g/>
,	,	kIx,	,
hájící	hájící	k2eAgInPc1d1	hájící
zájmy	zájem	k1gInPc1	zájem
jižanských	jižanský	k2eAgMnPc2d1	jižanský
plantážníků	plantážník	k1gMnPc2	plantážník
a	a	k8xC	a
lidí	člověk	k1gMnPc2	člověk
spojených	spojený	k2eAgFnPc2d1	spojená
s	s	k7c7	s
obchodováním	obchodování	k1gNnSc7	obchodování
s	s	k7c7	s
bavlnou	bavlna	k1gFnSc7	bavlna
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
roku	rok	k1gInSc2	rok
1854	[number]	k4	1854
Republikánská	republikánský	k2eAgFnSc1d1	republikánská
strana	strana	k1gFnSc1	strana
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Hájila	hájit	k5eAaImAgFnS	hájit
zájmy	zájem	k1gInPc4	zájem
farmářů	farmář	k1gMnPc2	farmář
<g/>
,	,	kIx,	,
dělníků	dělník	k1gMnPc2	dělník
a	a	k8xC	a
průmyslníků	průmyslník	k1gMnPc2	průmyslník
ze	z	k7c2	z
Severu	sever	k1gInSc2	sever
<g/>
.	.	kIx.	.
</s>
<s>
Zásadně	zásadně	k6eAd1	zásadně
byla	být	k5eAaImAgFnS	být
proti	proti	k7c3	proti
otroctví	otroctví	k1gNnSc3	otroctví
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
otroctví	otroctví	k1gNnSc3	otroctví
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
dva	dva	k4xCgInPc1	dva
proudy	proud	k1gInPc1	proud
<g/>
.	.	kIx.	.
</s>
<s>
Abolicionisté	abolicionista	k1gMnPc1	abolicionista
byli	být	k5eAaImAgMnP	být
pro	pro	k7c4	pro
úplné	úplný	k2eAgNnSc4d1	úplné
zrušení	zrušení	k1gNnSc4	zrušení
otroctví	otroctví	k1gNnSc2	otroctví
<g/>
.	.	kIx.	.
</s>
<s>
Pomáhali	pomáhat	k5eAaImAgMnP	pomáhat
utíkat	utíkat	k5eAaImF	utíkat
otrokům	otrok	k1gMnPc3	otrok
do	do	k7c2	do
Kanady	Kanada	k1gFnSc2	Kanada
a	a	k8xC	a
měli	mít	k5eAaImAgMnP	mít
značný	značný	k2eAgInSc4d1	značný
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
veřejné	veřejný	k2eAgNnSc4d1	veřejné
mínění	mínění	k1gNnSc4	mínění
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
již	již	k9	již
zmiňovaná	zmiňovaný	k2eAgFnSc1d1	zmiňovaná
Republikánská	republikánský	k2eAgFnSc1d1	republikánská
strana	strana	k1gFnSc1	strana
byla	být	k5eAaImAgFnS	být
z	z	k7c2	z
pragmatických	pragmatický	k2eAgInPc2d1	pragmatický
důvodů	důvod	k1gInPc2	důvod
jen	jen	k9	jen
pro	pro	k7c4	pro
zamezení	zamezení	k1gNnSc4	zamezení
šíření	šíření	k1gNnSc2	šíření
otroctví	otroctví	k1gNnSc2	otroctví
do	do	k7c2	do
nových	nový	k2eAgInPc2d1	nový
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Povstání	povstání	k1gNnSc4	povstání
Johna	John	k1gMnSc2	John
Browna	Brown	k1gMnSc2	Brown
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1859	[number]	k4	1859
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
povstání	povstání	k1gNnSc3	povstání
Johna	John	k1gMnSc2	John
Browna	Brown	k1gMnSc2	Brown
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yQgInSc6	který
bylo	být	k5eAaImAgNnS	být
obsazeno	obsadit	k5eAaPmNgNnS	obsadit
skladiště	skladiště	k1gNnSc4	skladiště
zbraní	zbraň	k1gFnPc2	zbraň
v	v	k7c4	v
Harpers	Harpers	k1gInSc4	Harpers
Ferry	Ferro	k1gNnPc7	Ferro
ve	v	k7c6	v
Virginii	Virginie	k1gFnSc6	Virginie
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
povstání	povstání	k1gNnSc1	povstání
bylo	být	k5eAaImAgNnS	být
potlačeno	potlačit	k5eAaPmNgNnS	potlačit
oddíly	oddíl	k1gInPc1	oddíl
Virginie	Virginie	k1gFnSc2	Virginie
a	a	k8xC	a
Marylandu	Maryland	k1gInSc2	Maryland
a	a	k8xC	a
John	John	k1gMnSc1	John
Brown	Brown	k1gMnSc1	Brown
byl	být	k5eAaImAgMnS	být
popraven	popravit	k5eAaPmNgMnS	popravit
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
to	ten	k3xDgNnSc4	ten
jediný	jediný	k2eAgMnSc1d1	jediný
povstalec	povstalec	k1gMnSc1	povstalec
na	na	k7c6	na
Jihu	jih	k1gInSc6	jih
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zvolení	zvolení	k1gNnSc4	zvolení
Abrahama	Abraham	k1gMnSc2	Abraham
Lincolna	Lincoln	k1gMnSc2	Lincoln
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1860	[number]	k4	1860
v	v	k7c6	v
prezidentských	prezidentský	k2eAgFnPc6d1	prezidentská
volbách	volba	k1gFnPc6	volba
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
Abraham	Abraham	k1gMnSc1	Abraham
Lincoln	Lincoln	k1gMnSc1	Lincoln
<g/>
,	,	kIx,	,
kandidát	kandidát	k1gMnSc1	kandidát
Republikánské	republikánský	k2eAgFnSc2d1	republikánská
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
proti	proti	k7c3	proti
otrokářství	otrokářství	k1gNnSc3	otrokářství
a	a	k8xC	a
plně	plně	k6eAd1	plně
si	se	k3xPyFc3	se
věřila	věřit	k5eAaImAgFnS	věřit
v	v	k7c6	v
zastavení	zastavení	k1gNnSc6	zastavení
jižanských	jižanský	k2eAgInPc2d1	jižanský
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
Lincoln	Lincoln	k1gMnSc1	Lincoln
neplánoval	plánovat	k5eNaImAgMnS	plánovat
zrušení	zrušení	k1gNnSc4	zrušení
otroctví	otroctví	k1gNnSc2	otroctví
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
již	již	k6eAd1	již
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
a	a	k8xC	a
zachování	zachování	k1gNnSc4	zachování
jednoty	jednota	k1gFnSc2	jednota
Unie	unie	k1gFnSc2	unie
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
bylo	být	k5eAaImAgNnS	být
prvořadé	prvořadý	k2eAgNnSc1d1	prvořadé
<g/>
,	,	kIx,	,
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
zvolení	zvolení	k1gNnSc4	zvolení
zareagovala	zareagovat	k5eAaPmAgFnS	zareagovat
Jižní	jižní	k2eAgFnSc1d1	jižní
Karolína	Karolína	k1gFnSc1	Karolína
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	s	k7c7	s
20	[number]	k4	20
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
vystoupit	vystoupit	k5eAaPmF	vystoupit
z	z	k7c2	z
Unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Následovaly	následovat	k5eAaImAgFnP	následovat
ji	on	k3xPp3gFnSc4	on
Mississippi	Mississippi	k1gFnSc4	Mississippi
<g/>
,	,	kIx,	,
Florida	Florida	k1gFnSc1	Florida
<g/>
,	,	kIx,	,
Alabama	Alabama	k1gFnSc1	Alabama
<g/>
,	,	kIx,	,
Georgia	Georgia	k1gFnSc1	Georgia
<g/>
,	,	kIx,	,
Louisiana	Louisiana	k1gFnSc1	Louisiana
a	a	k8xC	a
Texas	Texas	k1gInSc1	Texas
<g/>
.	.	kIx.	.
</s>
<s>
Vznikly	vzniknout	k5eAaPmAgInP	vzniknout
tak	tak	k6eAd1	tak
Konfederované	konfederovaný	k2eAgInPc1d1	konfederovaný
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
s	s	k7c7	s
vlastní	vlastní	k2eAgFnSc7d1	vlastní
ústavou	ústava	k1gFnSc7	ústava
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
prezidentem	prezident	k1gMnSc7	prezident
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
Jefferson	Jefferson	k1gMnSc1	Jefferson
Davis	Davis	k1gFnSc2	Davis
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Konfederace	konfederace	k1gFnSc2	konfederace
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
město	město	k1gNnSc1	město
Montgomery	Montgomera	k1gFnSc2	Montgomera
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Alabama	Alabamum	k1gNnSc2	Alabamum
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
Richmond	Richmond	k1gInSc4	Richmond
ve	v	k7c6	v
Virginii	Virginie	k1gFnSc6	Virginie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Konec	konec	k1gInSc1	konec
nadějí	naděje	k1gFnPc2	naděje
na	na	k7c4	na
smírné	smírný	k2eAgNnSc4d1	smírné
řešení	řešení	k1gNnSc4	řešení
znamenal	znamenat	k5eAaImAgInS	znamenat
útok	útok	k1gInSc1	útok
jižanských	jižanský	k2eAgFnPc2d1	jižanská
jednotek	jednotka	k1gFnPc2	jednotka
na	na	k7c4	na
federální	federální	k2eAgFnSc4d1	federální
pevnost	pevnost	k1gFnSc4	pevnost
Fort	Fort	k?	Fort
Sumter	Sumtra	k1gFnPc2	Sumtra
u	u	k7c2	u
Charlestonu	charleston	k1gInSc2	charleston
12	[number]	k4	12
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1861	[number]	k4	1861
<g/>
.	.	kIx.	.
</s>
<s>
Oficiálním	oficiální	k2eAgInSc7d1	oficiální
začátkem	začátek	k1gInSc7	začátek
války	válka	k1gFnSc2	válka
je	být	k5eAaImIp3nS	být
15	[number]	k4	15
<g/>
.	.	kIx.	.
duben	duben	k1gInSc1	duben
1861	[number]	k4	1861
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Lincoln	Lincoln	k1gMnSc1	Lincoln
povolal	povolat	k5eAaPmAgMnS	povolat
75	[number]	k4	75
000	[number]	k4	000
dobrovolníků	dobrovolník	k1gMnPc2	dobrovolník
(	(	kIx(	(
<g/>
přihlásilo	přihlásit	k5eAaPmAgNnS	přihlásit
se	se	k3xPyFc4	se
500	[number]	k4	500
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
19	[number]	k4	19
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1861	[number]	k4	1861
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
blokádu	blokáda	k1gFnSc4	blokáda
jižanských	jižanský	k2eAgInPc2d1	jižanský
přístavů	přístav	k1gInPc2	přístav
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
Konfederaci	konfederace	k1gFnSc3	konfederace
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
přidaly	přidat	k5eAaPmAgInP	přidat
další	další	k2eAgInPc1d1	další
čtyři	čtyři	k4xCgInPc1	čtyři
státy	stát	k1gInPc1	stát
<g/>
:	:	kIx,	:
Virginie	Virginie	k1gFnSc1	Virginie
<g/>
,	,	kIx,	,
Arkansas	Arkansas	k1gInSc1	Arkansas
<g/>
,	,	kIx,	,
Tennessee	Tennessee	k1gFnSc1	Tennessee
a	a	k8xC	a
Severní	severní	k2eAgFnSc1d1	severní
Karolína	Karolína	k1gFnSc1	Karolína
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Unii	unie	k1gFnSc6	unie
však	však	k9	však
nadále	nadále	k6eAd1	nadále
zůstávaly	zůstávat	k5eAaImAgInP	zůstávat
otrokářské	otrokářský	k2eAgInPc1d1	otrokářský
státy	stát	k1gInPc1	stát
Maryland	Marylanda	k1gFnPc2	Marylanda
<g/>
,	,	kIx,	,
Kentucky	Kentucka	k1gFnSc2	Kentucka
<g/>
,	,	kIx,	,
Delaware	Delawar	k1gMnSc5	Delawar
a	a	k8xC	a
Missouri	Missouri	k1gNnPc6	Missouri
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
války	válka	k1gFnSc2	válka
to	ten	k3xDgNnSc1	ten
vypadalo	vypadat	k5eAaImAgNnS	vypadat
<g/>
,	,	kIx,	,
že	že	k8xS	že
výhodu	výhoda	k1gFnSc4	výhoda
má	mít	k5eAaImIp3nS	mít
Jih	jih	k1gInSc1	jih
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
Sever	sever	k1gInSc1	sever
byl	být	k5eAaImAgInS	být
průmyslově	průmyslově	k6eAd1	průmyslově
rozvinutější	rozvinutý	k2eAgInSc1d2	rozvinutější
a	a	k8xC	a
měl	mít	k5eAaImAgInS	mít
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
větší	veliký	k2eAgInSc4d2	veliký
kapitál	kapitál	k1gInSc4	kapitál
<g/>
,	,	kIx,	,
suroviny	surovina	k1gFnPc4	surovina
a	a	k8xC	a
zbraně	zbraň	k1gFnPc4	zbraň
<g/>
,	,	kIx,	,
nedostávalo	dostávat	k5eNaImAgNnS	dostávat
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
zkušených	zkušený	k2eAgMnPc2d1	zkušený
vojevůdců	vojevůdce	k1gMnPc2	vojevůdce
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
většina	většina	k1gFnSc1	většina
důstojníků	důstojník	k1gMnPc2	důstojník
pocházela	pocházet	k5eAaImAgFnS	pocházet
z	z	k7c2	z
Jihu	jih	k1gInSc2	jih
a	a	k8xC	a
odešla	odejít	k5eAaPmAgFnS	odejít
do	do	k7c2	do
konfederačních	konfederační	k2eAgFnPc2d1	konfederační
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
Sever	sever	k1gInSc1	sever
musel	muset	k5eAaImAgInS	muset
útočit	útočit	k5eAaImF	útočit
<g/>
.	.	kIx.	.
</s>
<s>
Jihu	jih	k1gInSc3	jih
stačilo	stačit	k5eAaBmAgNnS	stačit
bránit	bránit	k5eAaImF	bránit
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Průběh	průběh	k1gInSc4	průběh
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Důstojníci	důstojník	k1gMnPc1	důstojník
z	z	k7c2	z
Evropy	Evropa	k1gFnSc2	Evropa
===	===	k?	===
</s>
</p>
<p>
<s>
Většina	většina	k1gFnSc1	většina
důstojníků	důstojník	k1gMnPc2	důstojník
armády	armáda	k1gFnSc2	armáda
USA	USA	kA	USA
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
konfliktu	konflikt	k1gInSc2	konflikt
do	do	k7c2	do
služeb	služba	k1gFnPc2	služba
Konfederace	konfederace	k1gFnSc2	konfederace
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
v	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
Unie	unie	k1gFnSc2	unie
projevilo	projevit	k5eAaPmAgNnS	projevit
nedostatkem	nedostatek	k1gInSc7	nedostatek
kvalitních	kvalitní	k2eAgMnPc2d1	kvalitní
velitelů	velitel	k1gMnPc2	velitel
<g/>
.	.	kIx.	.
</s>
<s>
Agenti	agent	k1gMnPc1	agent
Unie	unie	k1gFnSc2	unie
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
snažili	snažit	k5eAaImAgMnP	snažit
naverbovat	naverbovat	k5eAaPmF	naverbovat
důstojníky	důstojník	k1gMnPc4	důstojník
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
tak	tak	k8xC	tak
Unie	unie	k1gFnSc1	unie
získala	získat	k5eAaPmAgFnS	získat
do	do	k7c2	do
svých	svůj	k3xOyFgNnPc2	svůj
služeb	služba	k1gFnPc2	služba
hraběte	hrabě	k1gMnSc4	hrabě
Gustava	Gustav	k1gMnSc2	Gustav
Alba	album	k1gNnSc2	album
<g/>
,	,	kIx,	,
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
57	[number]	k4	57
vyšších	vysoký	k2eAgMnPc2d2	vyšší
důstojníků	důstojník	k1gMnPc2	důstojník
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
baronem	baron	k1gMnSc7	baron
Ottou	Otta	k1gMnSc7	Otta
von	von	k1gInSc4	von
Steubenem	Steuben	k1gInSc7	Steuben
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
Charlese	Charles	k1gMnSc2	Charles
A.	A.	kA	A.
Strohlbranda	Strohlbrando	k1gNnSc2	Strohlbrando
nebo	nebo	k8xC	nebo
Ludwiga	Ludwig	k1gMnSc2	Ludwig
von	von	k1gInSc1	von
Holstein	Holstein	k2eAgInSc1d1	Holstein
<g/>
,	,	kIx,	,
v	v	k7c4	v
Itálii	Itálie	k1gFnSc4	Itálie
Luigiho	Luigi	k1gMnSc4	Luigi
Palmu	palma	k1gFnSc4	palma
de	de	k?	de
Cesnolu	Cesnol	k1gInSc2	Cesnol
<g/>
,	,	kIx,	,
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
Ivana	Ivan	k1gMnSc2	Ivan
Vasiljeviče	Vasiljevič	k1gMnSc2	Vasiljevič
Turchanina	Turchanin	k2eAgMnSc2d1	Turchanin
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnPc1d1	další
důstojníci	důstojník	k1gMnPc1	důstojník
byli	být	k5eAaImAgMnP	být
naverbováni	naverbovat	k5eAaPmNgMnP	naverbovat
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
a	a	k8xC	a
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Unie	unie	k1gFnSc1	unie
stála	stát	k5eAaImAgFnS	stát
také	také	k9	také
třeba	třeba	k6eAd1	třeba
o	o	k7c4	o
Giuseppa	Giusepp	k1gMnSc4	Giusepp
Garibaldiho	Garibaldi	k1gMnSc4	Garibaldi
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ten	ten	k3xDgMnSc1	ten
nabídku	nabídka	k1gFnSc4	nabídka
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důstojnickém	důstojnický	k2eAgInSc6d1	důstojnický
sboru	sbor	k1gInSc6	sbor
armády	armáda	k1gFnSc2	armáda
Unie	unie	k1gFnSc2	unie
sloužila	sloužit	k5eAaImAgFnS	sloužit
dále	daleko	k6eAd2	daleko
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
emigrantů	emigrant	k1gMnPc2	emigrant
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
museli	muset	k5eAaImAgMnP	muset
opustit	opustit	k5eAaPmF	opustit
Evropu	Evropa	k1gFnSc4	Evropa
po	po	k7c6	po
revolučním	revoluční	k2eAgInSc6d1	revoluční
roce	rok	k1gInSc6	rok
1848	[number]	k4	1848
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Prvotní	prvotní	k2eAgInPc1d1	prvotní
úspěchy	úspěch	k1gInPc1	úspěch
Konfederace	konfederace	k1gFnSc2	konfederace
===	===	k?	===
</s>
</p>
<p>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
docházelo	docházet	k5eAaImAgNnS	docházet
pouze	pouze	k6eAd1	pouze
k	k	k7c3	k
menším	malý	k2eAgInPc3d2	menší
ozbrojeným	ozbrojený	k2eAgInPc3d1	ozbrojený
střetům	střet	k1gInPc3	střet
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
velká	velký	k2eAgFnSc1d1	velká
bitva	bitva	k1gFnSc1	bitva
přišla	přijít	k5eAaPmAgFnS	přijít
velice	velice	k6eAd1	velice
rychle	rychle	k6eAd1	rychle
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
velká	velký	k2eAgFnSc1d1	velká
bitva	bitva	k1gFnSc1	bitva
byla	být	k5eAaImAgFnS	být
svedena	sveden	k2eAgFnSc1d1	svedena
21	[number]	k4	21
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1861	[number]	k4	1861
u	u	k7c2	u
Bull	bulla	k1gFnPc2	bulla
Runu	run	k1gInSc2	run
(	(	kIx(	(
<g/>
též	též	k9	též
Manassas	Manassas	k1gInSc1	Manassas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
bitva	bitva	k1gFnSc1	bitva
skončila	skončit	k5eAaPmAgFnS	skončit
nerozhodně	rozhodně	k6eNd1	rozhodně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
armáda	armáda	k1gFnSc1	armáda
Unie	unie	k1gFnSc1	unie
se	se	k3xPyFc4	se
dala	dát	k5eAaPmAgFnS	dát
při	při	k7c6	při
ústupu	ústup	k1gInSc6	ústup
na	na	k7c4	na
útěk	útěk	k1gInSc4	útěk
i	i	k9	i
s	s	k7c7	s
početným	početný	k2eAgInSc7d1	početný
davem	dav	k1gInSc7	dav
civilistů	civilista	k1gMnPc2	civilista
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
bitvě	bitva	k1gFnSc3	bitva
přihlížel	přihlížet	k5eAaImAgMnS	přihlížet
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
rovněž	rovněž	k9	rovněž
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
půjde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
delší	dlouhý	k2eAgInSc4d2	delší
konflikt	konflikt	k1gInSc4	konflikt
než	než	k8xS	než
obě	dva	k4xCgFnPc1	dva
strany	strana	k1gFnPc1	strana
očekávaly	očekávat	k5eAaImAgFnP	očekávat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1862	[number]	k4	1862
zavedla	zavést	k5eAaPmAgFnS	zavést
Konfederace	konfederace	k1gFnSc1	konfederace
povinné	povinný	k2eAgInPc1d1	povinný
odvody	odvod	k1gInPc1	odvod
do	do	k7c2	do
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1863	[number]	k4	1863
je	být	k5eAaImIp3nS	být
zavedla	zavést	k5eAaPmAgFnS	zavést
i	i	k9	i
Unie	unie	k1gFnSc1	unie
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
počáteční	počáteční	k2eAgFnSc2d1	počáteční
porážky	porážka	k1gFnSc2	porážka
Unie	unie	k1gFnSc2	unie
byly	být	k5eAaImAgInP	být
zapříčiněny	zapříčinit	k5eAaPmNgInP	zapříčinit
velice	velice	k6eAd1	velice
malými	malý	k2eAgFnPc7d1	malá
zkušenostmi	zkušenost	k1gFnPc7	zkušenost
vojáků	voják	k1gMnPc2	voják
v	v	k7c6	v
boji	boj	k1gInSc6	boj
<g/>
.	.	kIx.	.
</s>
<s>
Rebelové	rebel	k1gMnPc1	rebel
(	(	kIx(	(
<g/>
Konfederace	konfederace	k1gFnSc1	konfederace
<g/>
)	)	kIx)	)
měli	mít	k5eAaImAgMnP	mít
naopak	naopak	k6eAd1	naopak
zkušeností	zkušenost	k1gFnPc2	zkušenost
dostatek	dostatek	k1gInSc4	dostatek
a	a	k8xC	a
byli	být	k5eAaImAgMnP	být
vedeni	vést	k5eAaImNgMnP	vést
dobrými	dobrý	k2eAgMnPc7d1	dobrý
generály	generál	k1gMnPc7	generál
(	(	kIx(	(
<g/>
Jackson	Jackson	k1gMnSc1	Jackson
<g/>
,	,	kIx,	,
Lee	Lea	k1gFnSc3	Lea
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
Unii	unie	k1gFnSc6	unie
to	ten	k3xDgNnSc1	ten
nestačilo	stačit	k5eNaBmAgNnS	stačit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Únos	únos	k1gInSc1	únos
vlaku	vlak	k1gInSc2	vlak
===	===	k?	===
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
12	[number]	k4	12
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1862	[number]	k4	1862
se	se	k3xPyFc4	se
odehrála	odehrát	k5eAaPmAgFnS	odehrát
jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
známých	známý	k2eAgFnPc2d1	známá
epizod	epizoda	k1gFnPc2	epizoda
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xC	jako
velká	velký	k2eAgFnSc1d1	velká
lokomotivní	lokomotivní	k2eAgFnSc1d1	lokomotivní
honička	honička	k1gFnSc1	honička
<g/>
.	.	kIx.	.
</s>
<s>
Seveřan	Seveřan	k1gMnSc1	Seveřan
Andrews	Andrews	k1gInSc4	Andrews
s	s	k7c7	s
dobrovolníky	dobrovolník	k1gMnPc7	dobrovolník
z	z	k7c2	z
armády	armáda	k1gFnSc2	armáda
unesli	unést	k5eAaPmAgMnP	unést
jižanský	jižanský	k2eAgInSc4d1	jižanský
vlak	vlak	k1gInSc4	vlak
směřující	směřující	k2eAgInSc4d1	směřující
z	z	k7c2	z
Atlanty	Atlanta	k1gFnSc2	Atlanta
do	do	k7c2	do
Chattanoogy	Chattanooga	k1gFnSc2	Chattanooga
<g/>
.	.	kIx.	.
</s>
<s>
Únos	únos	k1gInSc1	únos
se	se	k3xPyFc4	se
však	však	k9	však
nezdařil	zdařit	k5eNaPmAgMnS	zdařit
<g/>
,	,	kIx,	,
lokomotivě	lokomotiva	k1gFnSc6	lokomotiva
došlo	dojít	k5eAaPmAgNnS	dojít
palivo	palivo	k1gNnSc1	palivo
<g/>
,	,	kIx,	,
únosci	únosce	k1gMnPc1	únosce
se	se	k3xPyFc4	se
rozprchli	rozprchnout	k5eAaPmAgMnP	rozprchnout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byli	být	k5eAaImAgMnP	být
polapeni	polapen	k2eAgMnPc1d1	polapen
a	a	k8xC	a
část	část	k1gFnSc1	část
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Andrewse	Andrewse	k1gFnSc2	Andrewse
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
popravena	popraven	k2eAgFnSc1d1	popravena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Námořní	námořní	k2eAgFnPc4d1	námořní
akce	akce	k1gFnPc4	akce
===	===	k?	===
</s>
</p>
<p>
<s>
Výhodou	výhoda	k1gFnSc7	výhoda
Unie	unie	k1gFnSc2	unie
bylo	být	k5eAaImAgNnS	být
silnější	silný	k2eAgNnSc1d2	silnější
námořnictvo	námořnictvo	k1gNnSc1	námořnictvo
rekrutované	rekrutovaný	k2eAgNnSc1d1	rekrutované
zejména	zejména	k9	zejména
z	z	k7c2	z
Nové	Nové	k2eAgFnSc2d1	Nové
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
mělo	mít	k5eAaImAgNnS	mít
v	v	k7c6	v
Atlantiku	Atlantik	k1gInSc6	Atlantik
výraznou	výrazný	k2eAgFnSc4d1	výrazná
převahu	převaha	k1gFnSc4	převaha
<g/>
.	.	kIx.	.
</s>
<s>
Několikrát	několikrát	k6eAd1	několikrát
se	se	k3xPyFc4	se
podílelo	podílet	k5eAaImAgNnS	podílet
na	na	k7c6	na
přepravě	přeprava	k1gFnSc6	přeprava
jednotek	jednotka	k1gFnPc2	jednotka
do	do	k7c2	do
týla	týl	k1gInSc2	týl
nepřítele	nepřítel	k1gMnSc2	nepřítel
a	a	k8xC	a
uzavřelo	uzavřít	k5eAaPmAgNnS	uzavřít
jižní	jižní	k2eAgInPc4d1	jižní
státy	stát	k1gInPc4	stát
do	do	k7c2	do
námořní	námořní	k2eAgFnSc2d1	námořní
blokády	blokáda	k1gFnSc2	blokáda
<g/>
,	,	kIx,	,
širšího	široký	k2eAgInSc2d2	širší
úspěchu	úspěch	k1gInSc2	úspěch
ovšem	ovšem	k9	ovšem
nezaznamenalo	zaznamenat	k5eNaPmAgNnS	zaznamenat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
9	[number]	k4	9
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1862	[number]	k4	1862
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
zátoce	zátoka	k1gFnSc6	zátoka
Chesapeake	Chesapeak	k1gFnSc2	Chesapeak
k	k	k7c3	k
námořní	námořní	k2eAgFnSc3d1	námořní
bitvě	bitva	k1gFnSc3	bitva
pancéřovaných	pancéřovaný	k2eAgFnPc2d1	pancéřovaná
lodí	loď	k1gFnPc2	loď
Konfederace	konfederace	k1gFnSc2	konfederace
CSS	CSS	kA	CSS
Virginia	Virginium	k1gNnSc2	Virginium
(	(	kIx(	(
<g/>
známé	známý	k2eAgNnSc1d1	známé
též	též	k9	též
jako	jako	k8xC	jako
Merrimac	Merrimac	k1gFnSc1	Merrimac
<g/>
)	)	kIx)	)
a	a	k8xC	a
Unie	unie	k1gFnSc1	unie
–	–	k?	–
USS	USS	kA	USS
Monitor	monitor	k1gInSc1	monitor
<g/>
.	.	kIx.	.
</s>
<s>
Virginie	Virginie	k1gFnSc1	Virginie
zničila	zničit	k5eAaPmAgFnS	zničit
tři	tři	k4xCgFnPc4	tři
lodě	loď	k1gFnPc4	loď
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
když	když	k8xS	když
se	se	k3xPyFc4	se
na	na	k7c6	na
bojišti	bojiště	k1gNnSc6	bojiště
objevilo	objevit	k5eAaPmAgNnS	objevit
menší	malý	k2eAgNnSc1d2	menší
plavidlo	plavidlo	k1gNnSc1	plavidlo
Monitor	monitor	k1gInSc1	monitor
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
dlouhém	dlouhý	k2eAgInSc6d1	dlouhý
boji	boj	k1gInSc6	boj
nucena	nutit	k5eAaImNgFnS	nutit
ustoupit	ustoupit	k5eAaPmF	ustoupit
<g/>
.	.	kIx.	.
</s>
<s>
Zanedlouho	zanedlouho	k6eAd1	zanedlouho
při	při	k7c6	při
ústupu	ústup	k1gInSc6	ústup
byla	být	k5eAaImAgFnS	být
vojáky	voják	k1gMnPc4	voják
Konfederace	konfederace	k1gFnSc1	konfederace
zapálena	zapálit	k5eAaPmNgFnS	zapálit
a	a	k8xC	a
zničena	zničit	k5eAaPmNgFnS	zničit
v	v	k7c6	v
kotvišti	kotviště	k1gNnSc6	kotviště
Norfolk	Norfolka	k1gFnPc2	Norfolka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nepadla	padnout	k5eNaPmAgFnS	padnout
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
jednotkám	jednotka	k1gFnPc3	jednotka
Unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
17	[number]	k4	17
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1864	[number]	k4	1864
potopila	potopit	k5eAaPmAgFnS	potopit
ponorka	ponorka	k1gFnSc1	ponorka
Konfederace	konfederace	k1gFnSc2	konfederace
Hunley	Hunlea	k1gFnSc2	Hunlea
jako	jako	k9	jako
vůbec	vůbec	k9	vůbec
první	první	k4xOgFnSc1	první
ponorka	ponorka	k1gFnSc1	ponorka
nepřátelskou	přátelský	k2eNgFnSc4d1	nepřátelská
válečnou	válečná	k1gFnSc4	válečná
loď	loď	k1gFnSc1	loď
–	–	k?	–
USS	USS	kA	USS
Housatonic	Housatonice	k1gFnPc2	Housatonice
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
chystala	chystat	k5eAaImAgFnS	chystat
prorazit	prorazit	k5eAaPmF	prorazit
námořní	námořní	k2eAgFnSc4d1	námořní
blokádu	blokáda	k1gFnSc4	blokáda
Charlestownu	Charlestown	k1gInSc2	Charlestown
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgInSc1d1	dnešní
Charleston	charleston	k1gInSc1	charleston
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Jižní	jižní	k2eAgFnSc1d1	jižní
Karolína	Karolína	k1gFnSc1	Karolína
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Akci	akce	k1gFnSc4	akce
provedla	provést	k5eAaPmAgFnS	provést
v	v	k7c6	v
pozdních	pozdní	k2eAgFnPc6d1	pozdní
nočních	noční	k2eAgFnPc6d1	noční
hodinách	hodina	k1gFnPc6	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Výbušninu	výbušnina	k1gFnSc4	výbušnina
</s>
</p>
<p>
<s>
upevněnou	upevněný	k2eAgFnSc7d1	upevněná
na	na	k7c6	na
ráhně	ráhno	k1gNnSc6	ráhno
ponorka	ponorka	k1gFnSc1	ponorka
zabodla	zabodnout	k5eAaPmAgFnS	zabodnout
do	do	k7c2	do
trupu	trup	k1gInSc2	trup
lodě	loď	k1gFnSc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
couvání	couvání	k1gNnSc6	couvání
se	se	k3xPyFc4	se
lanem	lano	k1gNnSc7	lano
spustila	spustit	k5eAaPmAgFnS	spustit
exploze	exploze	k1gFnSc1	exploze
<g/>
.	.	kIx.	.
</s>
<s>
Bitevní	bitevní	k2eAgFnSc1d1	bitevní
loď	loď	k1gFnSc1	loď
Housatonic	Housatonice	k1gFnPc2	Housatonice
se	se	k3xPyFc4	se
potopila	potopit	k5eAaPmAgFnS	potopit
během	během	k7c2	během
5	[number]	k4	5
minut	minuta	k1gFnPc2	minuta
a	a	k8xC	a
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
při	při	k7c6	při
tom	ten	k3xDgNnSc6	ten
5	[number]	k4	5
členů	člen	k1gInPc2	člen
posádky	posádka	k1gFnSc2	posádka
lodi	loď	k1gFnSc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
Ponorka	ponorka	k1gFnSc1	ponorka
však	však	k9	však
byla	být	k5eAaImAgFnS	být
též	též	k9	též
poškozena	poškodit	k5eAaPmNgFnS	poškodit
a	a	k8xC	a
krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
při	při	k7c6	při
návratu	návrat	k1gInSc6	návrat
k	k	k7c3	k
pobřeží	pobřeží	k1gNnSc3	pobřeží
potopila	potopit	k5eAaPmAgFnS	potopit
–	–	k?	–
žádný	žádný	k3yNgInSc4	žádný
z	z	k7c2	z
osmi	osm	k4xCc2	osm
členů	člen	k1gMnPc2	člen
její	její	k3xOp3gFnSc2	její
posádky	posádka	k1gFnSc2	posádka
nepřežil	přežít	k5eNaPmAgMnS	přežít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
západním	západní	k2eAgNnSc6d1	západní
bojišti	bojiště	k1gNnSc6	bojiště
se	se	k3xPyFc4	se
dlouho	dlouho	k6eAd1	dlouho
bojovalo	bojovat	k5eAaImAgNnS	bojovat
o	o	k7c4	o
tok	tok	k1gInSc4	tok
Mississippi	Mississippi	k1gFnSc2	Mississippi
jako	jako	k9	jako
o	o	k7c4	o
důležitou	důležitý	k2eAgFnSc4d1	důležitá
dopravní	dopravní	k2eAgFnSc4d1	dopravní
tepnu	tepna	k1gFnSc4	tepna
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
postupovala	postupovat	k5eAaImAgFnS	postupovat
silná	silný	k2eAgFnSc1d1	silná
říční	říční	k2eAgFnSc1d1	říční
flotila	flotila	k1gFnSc1	flotila
a	a	k8xC	a
z	z	k7c2	z
jihu	jih	k1gInSc2	jih
námořní	námořní	k2eAgFnSc2d1	námořní
lodě	loď	k1gFnSc2	loď
<g/>
,	,	kIx,	,
provoz	provoz	k1gInSc4	provoz
lodí	loď	k1gFnPc2	loď
Unie	unie	k1gFnSc2	unie
na	na	k7c4	na
Mississippi	Mississippi	k1gFnSc4	Mississippi
nebyl	být	k5eNaImAgInS	být
až	až	k6eAd1	až
do	do	k7c2	do
posledního	poslední	k2eAgInSc2d1	poslední
roku	rok	k1gInSc2	rok
války	válka	k1gFnSc2	válka
možný	možný	k2eAgInSc1d1	možný
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
trasu	trasa	k1gFnSc4	trasa
ohrožovala	ohrožovat	k5eAaImAgFnS	ohrožovat
děla	dělo	k1gNnPc1	dělo
konfederačních	konfederační	k2eAgFnPc2d1	konfederační
pevností	pevnost	k1gFnPc2	pevnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zrušení	zrušení	k1gNnSc1	zrušení
otroctví	otroctví	k1gNnSc2	otroctví
v	v	k7c6	v
Unii	unie	k1gFnSc6	unie
===	===	k?	===
</s>
</p>
<p>
<s>
6	[number]	k4	6
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1862	[number]	k4	1862
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
vojska	vojsko	k1gNnSc2	vojsko
Unie	unie	k1gFnSc2	unie
vítězství	vítězství	k1gNnSc2	vítězství
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
generála	generál	k1gMnSc2	generál
Ulyssese	Ulyssese	k1gFnSc2	Ulyssese
S.	S.	kA	S.
Granta	Grant	k1gMnSc2	Grant
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Shilohu	Shiloh	k1gInSc2	Shiloh
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
ovšem	ovšem	k9	ovšem
draze	draha	k1gFnSc3	draha
zaplacené	zaplacený	k2eAgNnSc1d1	zaplacené
vítězství	vítězství	k1gNnSc1	vítězství
(	(	kIx(	(
<g/>
počet	počet	k1gInSc1	počet
padlých	padlý	k1gMnPc2	padlý
na	na	k7c6	na
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
činil	činit	k5eAaImAgInS	činit
25	[number]	k4	25
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
vítězné	vítězný	k2eAgFnSc6d1	vítězná
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Antietamu	Antietam	k1gInSc2	Antietam
přednesl	přednést	k5eAaPmAgMnS	přednést
Abraham	Abraham	k1gMnSc1	Abraham
Lincoln	Lincoln	k1gMnSc1	Lincoln
22	[number]	k4	22
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1862	[number]	k4	1862
předběžné	předběžný	k2eAgNnSc1d1	předběžné
vyhlášení	vyhlášení	k1gNnSc3	vyhlášení
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterého	který	k3yQgMnSc4	který
byli	být	k5eAaImAgMnP	být
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1863	[number]	k4	1863
všichni	všechen	k3xTgMnPc1	všechen
otroci	otrok	k1gMnPc1	otrok
na	na	k7c6	na
územích	území	k1gNnPc6	území
Unie	unie	k1gFnSc2	unie
svobodní	svobodný	k2eAgMnPc1d1	svobodný
(	(	kIx(	(
<g/>
definitivní	definitivní	k2eAgNnSc1d1	definitivní
osvobození	osvobození	k1gNnSc1	osvobození
otroků	otrok	k1gMnPc2	otrok
znamenal	znamenat	k5eAaImAgInS	znamenat
až	až	k9	až
13	[number]	k4	13
<g/>
.	.	kIx.	.
dodatek	dodatek	k1gInSc1	dodatek
Ústavy	ústava	k1gFnSc2	ústava
USA	USA	kA	USA
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1865	[number]	k4	1865
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tímto	tento	k3xDgNnSc7	tento
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
bylo	být	k5eAaImAgNnS	být
úmyslně	úmyslně	k6eAd1	úmyslně
vyčkáváno	vyčkáván	k2eAgNnSc1d1	vyčkáváno
na	na	k7c4	na
jakékoliv	jakýkoliv	k3yIgFnPc4	jakýkoliv
byť	byť	k8xS	byť
i	i	k9	i
menší	malý	k2eAgNnSc4d2	menší
vítězství	vítězství	k1gNnSc4	vítězství
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
zamaskována	zamaskován	k2eAgFnSc1d1	zamaskována
podstata	podstata	k1gFnSc1	podstata
potřeby	potřeba	k1gFnSc2	potřeba
více	hodně	k6eAd2	hodně
vojáků	voják	k1gMnPc2	voják
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Lincoln	Lincoln	k1gMnSc1	Lincoln
potřeboval	potřebovat	k5eAaImAgMnS	potřebovat
po	po	k7c6	po
mnoha	mnoho	k4c6	mnoho
porážkách	porážka	k1gFnPc6	porážka
rychle	rychle	k6eAd1	rychle
podporu	podpora	k1gFnSc4	podpora
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
musel	muset	k5eAaImAgMnS	muset
zabránit	zabránit	k5eAaPmF	zabránit
vstupu	vstup	k1gInSc2	vstup
do	do	k7c2	do
války	válka	k1gFnSc2	válka
dalším	další	k2eAgMnPc3d1	další
evropským	evropský	k2eAgMnPc3d1	evropský
státům	stát	k1gInPc3	stát
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
odsuzovaly	odsuzovat	k5eAaImAgFnP	odsuzovat
násilné	násilný	k2eAgNnSc4d1	násilné
potlačování	potlačování	k1gNnSc4	potlačování
svobody	svoboda	k1gFnSc2	svoboda
Konfederace	konfederace	k1gFnSc2	konfederace
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1863	[number]	k4	1863
začaly	začít	k5eAaPmAgInP	začít
vznikat	vznikat	k5eAaImF	vznikat
první	první	k4xOgInPc1	první
oddíly	oddíl	k1gInPc1	oddíl
černošských	černošský	k2eAgMnPc2d1	černošský
dobrovolníků	dobrovolník	k1gMnPc2	dobrovolník
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
výrazně	výrazně	k6eAd1	výrazně
pomohly	pomoct	k5eAaPmAgInP	pomoct
v	v	k7c6	v
bitvách	bitva	k1gFnPc6	bitva
proti	proti	k7c3	proti
Konfederaci	konfederace	k1gFnSc3	konfederace
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
prohlášení	prohlášení	k1gNnSc1	prohlášení
rovněž	rovněž	k9	rovněž
částečně	částečně	k6eAd1	částečně
eliminovalo	eliminovat	k5eAaBmAgNnS	eliminovat
hrozbu	hrozba	k1gFnSc4	hrozba
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
Španělsko	Španělsko	k1gNnSc1	Španělsko
a	a	k8xC	a
Velká	velká	k1gFnSc1	velká
Británie	Británie	k1gFnSc2	Británie
pomohly	pomoct	k5eAaPmAgInP	pomoct
Konfederaci	konfederace	k1gFnSc4	konfederace
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
znamenalo	znamenat	k5eAaImAgNnS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
otevřeně	otevřeně	k6eAd1	otevřeně
podporují	podporovat	k5eAaImIp3nP	podporovat
otroctví	otroctví	k1gNnSc4	otroctví
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Unie	unie	k1gFnSc2	unie
v	v	k7c6	v
krizi	krize	k1gFnSc6	krize
===	===	k?	===
</s>
</p>
<p>
<s>
Koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1862	[number]	k4	1862
sílila	sílit	k5eAaImAgFnS	sílit
v	v	k7c6	v
Unii	unie	k1gFnSc6	unie
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
poraženecká	poraženecký	k2eAgFnSc1d1	poraženecká
nálada	nálada	k1gFnSc1	nálada
<g/>
.	.	kIx.	.
</s>
<s>
Odpor	odpor	k1gInSc1	odpor
vůči	vůči	k7c3	vůči
válce	válka	k1gFnSc3	válka
a	a	k8xC	a
celková	celkový	k2eAgFnSc1d1	celková
skepse	skepse	k1gFnSc1	skepse
z	z	k7c2	z
dosavadního	dosavadní	k2eAgInSc2d1	dosavadní
vývoje	vývoj	k1gInSc2	vývoj
dosáhly	dosáhnout	k5eAaPmAgInP	dosáhnout
takové	takový	k3xDgFnPc4	takový
míry	míra	k1gFnPc4	míra
<g/>
,	,	kIx,	,
že	že	k8xS	že
prezident	prezident	k1gMnSc1	prezident
Lincoln	Lincoln	k1gMnSc1	Lincoln
musel	muset	k5eAaImAgMnS	muset
dočasně	dočasně	k6eAd1	dočasně
omezit	omezit	k5eAaPmF	omezit
některá	některý	k3yIgNnPc4	některý
demokratická	demokratický	k2eAgNnPc4d1	demokratické
práva	právo	k1gNnPc4	právo
občanů	občan	k1gMnPc2	občan
a	a	k8xC	a
zavést	zavést	k5eAaPmF	zavést
jistý	jistý	k2eAgInSc4d1	jistý
druh	druh	k1gInSc4	druh
výjimečného	výjimečný	k2eAgInSc2d1	výjimečný
stavu	stav	k1gInSc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
Lincoln	Lincoln	k1gMnSc1	Lincoln
pozastavil	pozastavit	k5eAaPmAgInS	pozastavit
platnost	platnost	k1gFnSc4	platnost
práva	právo	k1gNnSc2	právo
habeas	habeasa	k1gFnPc2	habeasa
corpus	corpus	k1gInSc7	corpus
a	a	k8xC	a
vydal	vydat	k5eAaPmAgMnS	vydat
příkazy	příkaz	k1gInPc4	příkaz
<g/>
,	,	kIx,	,
že	že	k8xS	že
povstalci	povstalec	k1gMnPc1	povstalec
a	a	k8xC	a
všichni	všechen	k3xTgMnPc1	všechen
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
páchají	páchat	k5eAaImIp3nP	páchat
trestné	trestný	k2eAgInPc4d1	trestný
činy	čin	k1gInPc4	čin
proti	proti	k7c3	proti
Spojeným	spojený	k2eAgInPc3d1	spojený
státům	stát	k1gInPc3	stát
<g/>
,	,	kIx,	,
budou	být	k5eAaImBp3nP	být
postaveni	postavit	k5eAaPmNgMnP	postavit
před	před	k7c4	před
vojenský	vojenský	k2eAgInSc4d1	vojenský
tribunál	tribunál	k1gInSc4	tribunál
<g/>
.	.	kIx.	.
</s>
<s>
Opoziční	opoziční	k2eAgMnPc1d1	opoziční
poslanci	poslanec	k1gMnPc1	poslanec
Demokratické	demokratický	k2eAgFnSc2d1	demokratická
strany	strana	k1gFnSc2	strana
označili	označit	k5eAaPmAgMnP	označit
tato	tento	k3xDgNnPc4	tento
opatření	opatření	k1gNnPc4	opatření
za	za	k7c4	za
diktátorská	diktátorský	k2eAgNnPc4d1	diktátorské
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejhlasitějších	hlasitý	k2eAgMnPc2d3	nejhlasitější
kritiků	kritik	k1gMnPc2	kritik
těchto	tento	k3xDgNnPc2	tento
Lincolnových	Lincolnových	k2eAgNnPc2d1	Lincolnových
opatření	opatření	k1gNnPc2	opatření
byl	být	k5eAaImAgMnS	být
politik	politik	k1gMnSc1	politik
Clement	Clement	k1gMnSc1	Clement
Laure	laur	k1gInSc5	laur
Vallandigham	Vallandigham	k1gInSc1	Vallandigham
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
za	za	k7c4	za
své	svůj	k3xOyFgInPc4	svůj
výroky	výrok	k1gInPc4	výrok
zatčen	zatknout	k5eAaPmNgMnS	zatknout
a	a	k8xC	a
odsouzen	odsouzet	k5eAaImNgMnS	odsouzet
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
do	do	k7c2	do
vězení	vězení	k1gNnSc2	vězení
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Lincolnův	Lincolnův	k2eAgInSc4d1	Lincolnův
pokyn	pokyn	k1gInSc4	pokyn
byl	být	k5eAaImAgInS	být
nakonec	nakonec	k6eAd1	nakonec
vyhoštěn	vyhostit	k5eAaPmNgInS	vyhostit
do	do	k7c2	do
státu	stát	k1gInSc2	stát
Tennessee	Tennesse	k1gFnSc2	Tennesse
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
1863	[number]	k4	1863
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
státech	stát	k1gInPc6	stát
Unie	unie	k1gFnSc1	unie
zavedena	zaveden	k2eAgFnSc1d1	zavedena
povinná	povinný	k2eAgFnSc1d1	povinná
vojenská	vojenský	k2eAgFnSc1d1	vojenská
služba	služba	k1gFnSc1	služba
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
další	další	k2eAgNnSc1d1	další
napětí	napětí	k1gNnSc1	napětí
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
bohatí	bohatý	k2eAgMnPc1d1	bohatý
občané	občan	k1gMnPc1	občan
se	se	k3xPyFc4	se
mohli	moct	k5eAaImAgMnP	moct
z	z	k7c2	z
vojenské	vojenský	k2eAgFnSc2d1	vojenská
povinnosti	povinnost	k1gFnSc2	povinnost
vykoupit	vykoupit	k5eAaPmF	vykoupit
a	a	k8xC	a
poslat	poslat	k5eAaPmF	poslat
za	za	k7c4	za
sebe	sebe	k3xPyFc4	sebe
náhradníky	náhradník	k1gMnPc4	náhradník
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
velkých	velký	k2eAgNnPc6d1	velké
městech	město	k1gNnPc6	město
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
násilným	násilný	k2eAgInPc3d1	násilný
nepokojům	nepokoj	k1gInPc3	nepokoj
a	a	k8xC	a
ve	v	k7c6	v
dnech	den	k1gInPc6	den
13	[number]	k4	13
<g/>
.	.	kIx.	.
až	až	k9	až
16	[number]	k4	16
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1863	[number]	k4	1863
vyvrcholila	vyvrcholit	k5eAaPmAgFnS	vyvrcholit
nespokojenost	nespokojenost	k1gFnSc1	nespokojenost
některých	některý	k3yIgMnPc2	některý
občanů	občan	k1gMnPc2	občan
(	(	kIx(	(
<g/>
převážně	převážně	k6eAd1	převážně
irských	irský	k2eAgMnPc2d1	irský
přistěhovalců	přistěhovalec	k1gMnPc2	přistěhovalec
<g/>
)	)	kIx)	)
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
Lincoln	Lincoln	k1gMnSc1	Lincoln
byl	být	k5eAaImAgMnS	být
nucen	nutit	k5eAaImNgMnS	nutit
vyslat	vyslat	k5eAaPmF	vyslat
proti	proti	k7c3	proti
násilníkům	násilník	k1gMnPc3	násilník
tři	tři	k4xCgInPc1	tři
armádní	armádní	k2eAgInPc1d1	armádní
pluky	pluk	k1gInPc1	pluk
a	a	k8xC	a
jeden	jeden	k4xCgInSc1	jeden
pluk	pluk	k1gInSc1	pluk
milice	milice	k1gFnSc2	milice
<g/>
.	.	kIx.	.
</s>
<s>
Zásah	zásah	k1gInSc4	zásah
vojska	vojsko	k1gNnSc2	vojsko
si	se	k3xPyFc3	se
vyžádal	vyžádat	k5eAaPmAgInS	vyžádat
100	[number]	k4	100
zabitých	zabitý	k2eAgFnPc2d1	zabitá
a	a	k8xC	a
300	[number]	k4	300
zraněných	zraněný	k2eAgMnPc2d1	zraněný
civilistů	civilista	k1gMnPc2	civilista
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Pomoc	pomoc	k1gFnSc1	pomoc
Ruska	Rusko	k1gNnSc2	Rusko
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1863	[number]	k4	1863
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
diplomatickému	diplomatický	k2eAgNnSc3d1	diplomatické
sblížení	sblížení	k1gNnSc3	sblížení
Unie	unie	k1gFnSc2	unie
s	s	k7c7	s
Ruskem	Rusko	k1gNnSc7	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
byly	být	k5eAaImAgInP	být
zostřené	zostřený	k2eAgInPc1d1	zostřený
vztahy	vztah	k1gInPc1	vztah
mezi	mezi	k7c7	mezi
Ruskem	Rusko	k1gNnSc7	Rusko
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
a	a	k8xC	a
Velkou	velký	k2eAgFnSc7d1	velká
Británií	Británie	k1gFnSc7	Británie
a	a	k8xC	a
Francií	Francie	k1gFnSc7	Francie
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
druhé	druhý	k4xOgFnSc6	druhý
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
protiruskému	protiruský	k2eAgNnSc3d1	protiruské
povstání	povstání	k1gNnSc3	povstání
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
a	a	k8xC	a
Francie	Francie	k1gFnPc1	Francie
sympatizovaly	sympatizovat	k5eAaImAgFnP	sympatizovat
s	s	k7c7	s
Konfederací	konfederace	k1gFnSc7	konfederace
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
Rusové	Rus	k1gMnPc1	Rus
projevili	projevit	k5eAaPmAgMnP	projevit
svůj	svůj	k3xOyFgInSc4	svůj
kladný	kladný	k2eAgInSc4d1	kladný
vztah	vztah	k1gInSc4	vztah
vůči	vůči	k7c3	vůči
Unii	unie	k1gFnSc4	unie
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
1863	[number]	k4	1863
vyslali	vyslat	k5eAaPmAgMnP	vyslat
jednu	jeden	k4xCgFnSc4	jeden
eskadru	eskadra	k1gFnSc4	eskadra
do	do	k7c2	do
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
a	a	k8xC	a
druhou	druhý	k4xOgFnSc4	druhý
do	do	k7c2	do
San	San	k1gFnPc2	San
Francisca	Francisc	k1gInSc2	Francisc
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
bojům	boj	k1gInPc3	boj
sice	sice	k8xC	sice
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouhá	pouhý	k2eAgFnSc1d1	pouhá
přítomnost	přítomnost	k1gFnSc1	přítomnost
ruských	ruský	k2eAgFnPc2d1	ruská
válečných	válečný	k2eAgFnPc2d1	válečná
lodí	loď	k1gFnPc2	loď
ochránila	ochránit	k5eAaPmAgFnS	ochránit
San	San	k1gFnSc1	San
Francisco	Francisco	k6eAd1	Francisco
před	před	k7c7	před
případným	případný	k2eAgInSc7d1	případný
útokem	útok	k1gInSc7	útok
Konfederace	konfederace	k1gFnSc2	konfederace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zvrat	zvrat	k1gInSc4	zvrat
===	===	k?	===
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1863	[number]	k4	1863
zuřila	zuřit	k5eAaImAgFnS	zuřit
bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Gettysburgu	Gettysburg	k1gInSc2	Gettysburg
<g/>
.	.	kIx.	.
</s>
<s>
Vrchní	vrchní	k2eAgMnSc1d1	vrchní
velitel	velitel	k1gMnSc1	velitel
Konfederace	konfederace	k1gFnSc2	konfederace
generál	generál	k1gMnSc1	generál
Robert	Robert	k1gMnSc1	Robert
Edward	Edward	k1gMnSc1	Edward
Lee	Lea	k1gFnSc6	Lea
utrpěl	utrpět	k5eAaPmAgMnS	utrpět
drtivou	drtivý	k2eAgFnSc4d1	drtivá
porážku	porážka	k1gFnSc4	porážka
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
nucen	nutit	k5eAaImNgMnS	nutit
se	se	k3xPyFc4	se
stáhnout	stáhnout	k5eAaPmF	stáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Gettysburgu	Gettysburg	k1gInSc2	Gettysburg
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
zásadním	zásadní	k2eAgInSc7d1	zásadní
přelomem	přelom	k1gInSc7	přelom
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Prakticky	prakticky	k6eAd1	prakticky
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Unie	unie	k1gFnSc1	unie
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
u	u	k7c2	u
Gettysburgu	Gettysburg	k1gInSc2	Gettysburg
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
i	i	k9	i
k	k	k7c3	k
pádu	pád	k1gInSc3	pád
dalších	další	k2eAgInPc2d1	další
opěrných	opěrný	k2eAgInPc2d1	opěrný
bodů	bod	k1gInPc2	bod
jižanů	jižan	k1gMnPc2	jižan
–	–	k?	–
Vicksburgu	Vicksburg	k1gInSc2	Vicksburg
a	a	k8xC	a
Port	port	k1gInSc4	port
Hudsonu	Hudson	k1gInSc2	Hudson
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byly	být	k5eAaImAgFnP	být
poslední	poslední	k2eAgInPc1d1	poslední
dvě	dva	k4xCgFnPc1	dva
bašty	bašta	k1gFnPc1	bašta
Konfederace	konfederace	k1gFnSc2	konfederace
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Mississippi	Mississippi	k1gFnSc2	Mississippi
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
1864	[number]	k4	1864
byl	být	k5eAaImAgMnS	být
generálporučík	generálporučík	k1gMnSc1	generálporučík
Ulysses	Ulysses	k1gMnSc1	Ulysses
Simpson	Simpson	k1gMnSc1	Simpson
Grant	grant	k1gInSc4	grant
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
vrchním	vrchní	k2eAgMnSc7d1	vrchní
velitelem	velitel	k1gMnSc7	velitel
všech	všecek	k3xTgFnPc2	všecek
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
Unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
dalším	další	k2eAgInSc6d1	další
průběhu	průběh	k1gInSc6	průběh
války	válka	k1gFnSc2	válka
si	se	k3xPyFc3	se
vzal	vzít	k5eAaPmAgMnS	vzít
generál	generál	k1gMnSc1	generál
Grant	grant	k1gInSc4	grant
na	na	k7c6	na
starosti	starost	k1gFnSc6	starost
operace	operace	k1gFnSc2	operace
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
frontě	fronta	k1gFnSc6	fronta
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
jeho	jeho	k3xOp3gMnSc1	jeho
soupeřem	soupeř	k1gMnSc7	soupeř
generál	generál	k1gMnSc1	generál
Lee	Lea	k1gFnSc3	Lea
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
generál	generál	k1gMnSc1	generál
William	William	k1gInSc4	William
Tecumseh	Tecumseh	k1gMnSc1	Tecumseh
Sherman	Sherman	k1gMnSc1	Sherman
se	se	k3xPyFc4	se
na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
frontě	fronta	k1gFnSc6	fronta
pustil	pustit	k5eAaPmAgMnS	pustit
do	do	k7c2	do
souboje	souboj	k1gInSc2	souboj
s	s	k7c7	s
armádou	armáda	k1gFnSc7	armáda
generála	generál	k1gMnSc2	generál
Josepha	Joseph	k1gMnSc2	Joseph
E.	E.	kA	E.
Johnstona	Johnston	k1gMnSc2	Johnston
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
vojska	vojsko	k1gNnSc2	vojsko
Unie	unie	k1gFnSc2	unie
měla	mít	k5eAaImAgFnS	mít
na	na	k7c6	na
obou	dva	k4xCgFnPc6	dva
frontách	fronta	k1gFnPc6	fronta
početní	početní	k2eAgInSc1d1	početní
i	i	k9	i
materiální	materiální	k2eAgFnSc4d1	materiální
převahu	převaha	k1gFnSc4	převaha
<g/>
.	.	kIx.	.
</s>
<s>
Generál	generál	k1gMnSc1	generál
Lee	Lea	k1gFnSc3	Lea
se	se	k3xPyFc4	se
až	až	k9	až
do	do	k7c2	do
listopadu	listopad	k1gInSc2	listopad
1864	[number]	k4	1864
usilovně	usilovně	k6eAd1	usilovně
snažil	snažit	k5eAaImAgMnS	snažit
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
zvratu	zvrat	k1gInSc2	zvrat
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
Konfederace	konfederace	k1gFnSc2	konfederace
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
chtěl	chtít	k5eAaImAgMnS	chtít
poškodit	poškodit	k5eAaPmF	poškodit
pověst	pověst	k1gFnSc4	pověst
Abrahama	Abraham	k1gMnSc2	Abraham
Lincolna	Lincoln	k1gMnSc2	Lincoln
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
prezidentskými	prezidentský	k2eAgFnPc7d1	prezidentská
volbami	volba	k1gFnPc7	volba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
nezvolení	nezvolení	k1gNnSc2	nezvolení
Lincolna	Lincoln	k1gMnSc2	Lincoln
spoléhali	spoléhat	k5eAaImAgMnP	spoléhat
Jižané	Jižan	k1gMnPc1	Jižan
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
boje	boj	k1gInPc1	boj
budou	být	k5eAaImBp3nP	být
nakonec	nakonec	k6eAd1	nakonec
ukončeny	ukončit	k5eAaPmNgFnP	ukončit
s	s	k7c7	s
neutrálním	neutrální	k2eAgInSc7d1	neutrální
výsledkem	výsledek	k1gInSc7	výsledek
a	a	k8xC	a
na	na	k7c6	na
území	území	k1gNnSc6	území
bývalých	bývalý	k2eAgFnPc2d1	bývalá
USA	USA	kA	USA
budou	být	k5eAaImBp3nP	být
dále	daleko	k6eAd2	daleko
fungovat	fungovat	k5eAaImF	fungovat
dva	dva	k4xCgInPc4	dva
nezávislé	závislý	k2eNgInPc4d1	nezávislý
státy	stát	k1gInPc4	stát
<g/>
.	.	kIx.	.
</s>
<s>
Jižanský	jižanský	k2eAgMnSc1d1	jižanský
generál	generál	k1gMnSc1	generál
Jubal	Jubal	k1gMnSc1	Jubal
Anderson	Anderson	k1gMnSc1	Anderson
Early	earl	k1gMnPc4	earl
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
dle	dle	k7c2	dle
plánu	plán	k1gInSc2	plán
vydal	vydat	k5eAaPmAgInS	vydat
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
Washington	Washington	k1gInSc4	Washington
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gInSc1	jeho
útok	útok	k1gInSc1	útok
byl	být	k5eAaImAgInS	být
odražen	odražen	k2eAgMnSc1d1	odražen
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
se	se	k3xPyFc4	se
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
armádou	armáda	k1gFnSc7	armáda
stáhl	stáhnout	k5eAaPmAgInS	stáhnout
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Virginie	Virginie	k1gFnSc2	Virginie
<g/>
.	.	kIx.	.
19	[number]	k4	19
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
1864	[number]	k4	1864
byla	být	k5eAaImAgFnS	být
armáda	armáda	k1gFnSc1	armáda
generála	generál	k1gMnSc2	generál
Earlyho	Early	k1gMnSc2	Early
definitivně	definitivně	k6eAd1	definitivně
poražena	porazit	k5eAaPmNgNnP	porazit
u	u	k7c2	u
říčky	říčka	k1gFnSc2	říčka
Cedar	Cedar	k1gMnSc1	Cedar
Creek	Creek	k1gMnSc1	Creek
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
necelé	celý	k2eNgInPc4d1	necelý
tři	tři	k4xCgInPc4	tři
týdny	týden	k1gInPc4	týden
po	po	k7c6	po
této	tento	k3xDgFnSc6	tento
bitvě	bitva	k1gFnSc6	bitva
se	se	k3xPyFc4	se
v	v	k7c6	v
Unii	unie	k1gFnSc6	unie
konaly	konat	k5eAaImAgFnP	konat
prezidentské	prezidentský	k2eAgFnPc1d1	prezidentská
volby	volba	k1gFnPc1	volba
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
za	za	k7c4	za
Republikánskou	republikánský	k2eAgFnSc4d1	republikánská
stranu	strana	k1gFnSc4	strana
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
dosavadní	dosavadní	k2eAgMnSc1d1	dosavadní
prezident	prezident	k1gMnSc1	prezident
Abraham	Abraham	k1gMnSc1	Abraham
Lincoln	Lincoln	k1gMnSc1	Lincoln
a	a	k8xC	a
za	za	k7c4	za
Demokratickou	demokratický	k2eAgFnSc4d1	demokratická
stranu	strana	k1gFnSc4	strana
generál	generál	k1gMnSc1	generál
B.	B.	kA	B.
McClellan	McClellan	k1gMnSc1	McClellan
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
1864	[number]	k4	1864
byl	být	k5eAaImAgInS	být
prezidentem	prezident	k1gMnSc7	prezident
znovu	znovu	k6eAd1	znovu
zvolen	zvolen	k2eAgMnSc1d1	zvolen
Abraham	Abraham	k1gMnSc1	Abraham
Lincoln	Lincoln	k1gMnSc1	Lincoln
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Obsazení	obsazení	k1gNnSc1	obsazení
Atlanty	Atlanta	k1gFnSc2	Atlanta
a	a	k8xC	a
Savannah	Savannaha	k1gFnPc2	Savannaha
===	===	k?	===
</s>
</p>
<p>
<s>
Generál	generál	k1gMnSc1	generál
Sherman	Sherman	k1gMnSc1	Sherman
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
bojové	bojový	k2eAgFnSc6d1	bojová
frontě	fronta	k1gFnSc6	fronta
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
1864	[number]	k4	1864
například	například	k6eAd1	například
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
před	před	k7c7	před
jeho	jeho	k3xOp3gNnSc7	jeho
vojskem	vojsko	k1gNnSc7	vojsko
1	[number]	k4	1
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
1864	[number]	k4	1864
kapitulovala	kapitulovat	k5eAaBmAgFnS	kapitulovat
Atlanta	Atlanta	k1gFnSc1	Atlanta
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
značně	značně	k6eAd1	značně
posílilo	posílit	k5eAaPmAgNnS	posílit
optimismus	optimismus	k1gInSc4	optimismus
na	na	k7c6	na
Severu	sever	k1gInSc6	sever
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Konfederaci	konfederace	k1gFnSc4	konfederace
to	ten	k3xDgNnSc1	ten
znamenalo	znamenat	k5eAaImAgNnS	znamenat
ztrátu	ztráta	k1gFnSc4	ztráta
významného	významný	k2eAgNnSc2d1	významné
železničního	železniční	k2eAgNnSc2d1	železniční
centra	centrum	k1gNnSc2	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Generál	generál	k1gMnSc1	generál
Sherman	Sherman	k1gMnSc1	Sherman
vydal	vydat	k5eAaPmAgMnS	vydat
příkaz	příkaz	k1gInSc4	příkaz
k	k	k7c3	k
vystěhování	vystěhování	k1gNnSc3	vystěhování
všech	všecek	k3xTgMnPc2	všecek
obyvatel	obyvatel	k1gMnPc2	obyvatel
Atlanty	Atlanta	k1gFnSc2	Atlanta
a	a	k8xC	a
následně	následně	k6eAd1	následně
nechal	nechat	k5eAaPmAgMnS	nechat
toto	tento	k3xDgNnSc4	tento
město	město	k1gNnSc4	město
11	[number]	k4	11
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
1864	[number]	k4	1864
vypálit	vypálit	k5eAaPmF	vypálit
do	do	k7c2	do
základů	základ	k1gInPc2	základ
<g/>
.	.	kIx.	.
</s>
<s>
Ušetřeny	ušetřen	k2eAgFnPc1d1	ušetřena
byly	být	k5eAaImAgFnP	být
jen	jen	k9	jen
nemocnice	nemocnice	k1gFnPc1	nemocnice
a	a	k8xC	a
kostely	kostel	k1gInPc1	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Atlanty	Atlanta	k1gFnSc2	Atlanta
pak	pak	k6eAd1	pak
vojsko	vojsko	k1gNnSc1	vojsko
táhlo	táhnout	k5eAaImAgNnS	táhnout
dál	daleko	k6eAd2	daleko
státem	stát	k1gInSc7	stát
Georgie	Georgie	k1gFnSc2	Georgie
a	a	k8xC	a
důsledně	důsledně	k6eAd1	důsledně
ničilo	ničit	k5eAaImAgNnS	ničit
a	a	k8xC	a
vypalovalo	vypalovat	k5eAaImAgNnS	vypalovat
všechny	všechen	k3xTgFnPc4	všechen
obce	obec	k1gFnPc4	obec
a	a	k8xC	a
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
mu	on	k3xPp3gMnSc3	on
stála	stát	k5eAaImAgFnS	stát
v	v	k7c6	v
cestě	cesta	k1gFnSc6	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
vánočními	vánoční	k2eAgInPc7d1	vánoční
svátky	svátek	k1gInPc7	svátek
1864	[number]	k4	1864
vstoupily	vstoupit	k5eAaPmAgInP	vstoupit
Shermanovy	Shermanův	k2eAgInPc1d1	Shermanův
oddíly	oddíl	k1gInPc1	oddíl
do	do	k7c2	do
města	město	k1gNnSc2	město
Savannah	Savannaha	k1gFnPc2	Savannaha
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
pak	pak	k6eAd1	pak
postupovaly	postupovat	k5eAaImAgFnP	postupovat
na	na	k7c4	na
sever	sever	k1gInSc4	sever
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
připravovalo	připravovat	k5eAaImAgNnS	připravovat
setkání	setkání	k1gNnSc1	setkání
s	s	k7c7	s
Grantovou	grantový	k2eAgFnSc7d1	Grantová
armádou	armáda	k1gFnSc7	armáda
a	a	k8xC	a
konečný	konečný	k2eAgInSc1d1	konečný
útok	útok	k1gInSc1	útok
na	na	k7c4	na
armády	armáda	k1gFnPc4	armáda
generála	generál	k1gMnSc2	generál
Leea	Leeus	k1gMnSc2	Leeus
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
postupu	postup	k1gInSc2	postup
na	na	k7c4	na
sever	sever	k1gInSc4	sever
Sherman	Sherman	k1gMnSc1	Sherman
nadále	nadále	k6eAd1	nadále
prosazoval	prosazovat	k5eAaImAgMnS	prosazovat
taktiku	taktika	k1gFnSc4	taktika
spálené	spálený	k2eAgFnSc2d1	spálená
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
chtěl	chtít	k5eAaImAgMnS	chtít
zničit	zničit	k5eAaPmF	zničit
jižanskou	jižanský	k2eAgFnSc4d1	jižanská
ekonomiku	ekonomika	k1gFnSc4	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
Touto	tento	k3xDgFnSc7	tento
plánovanou	plánovaný	k2eAgFnSc7d1	plánovaná
devastací	devastace	k1gFnSc7	devastace
byly	být	k5eAaImAgFnP	být
kromě	kromě	k7c2	kromě
Georgie	Georgie	k1gFnSc2	Georgie
silně	silně	k6eAd1	silně
poškozeny	poškodit	k5eAaPmNgFnP	poškodit
ještě	ještě	k9	ještě
státy	stát	k1gInPc4	stát
Jižní	jižní	k2eAgFnSc1d1	jižní
a	a	k8xC	a
Severní	severní	k2eAgFnSc1d1	severní
Karolína	Karolína	k1gFnSc1	Karolína
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dnech	den	k1gInPc6	den
27	[number]	k4	27
<g/>
.	.	kIx.	.
a	a	k8xC	a
28	[number]	k4	28
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
1865	[number]	k4	1865
se	se	k3xPyFc4	se
v	v	k7c6	v
malém	malý	k2eAgInSc6d1	malý
přístavu	přístav	k1gInSc6	přístav
City	City	k1gFnSc2	City
Point	pointa	k1gFnPc2	pointa
<g/>
,	,	kIx,	,
na	na	k7c6	na
říčním	říční	k2eAgNnSc6d1	říční
plavidle	plavidlo	k1gNnSc6	plavidlo
River	Rivra	k1gFnPc2	Rivra
Queen	Queno	k1gNnPc2	Queno
<g/>
,	,	kIx,	,
konala	konat	k5eAaImAgFnS	konat
závěrečná	závěrečný	k2eAgFnSc1d1	závěrečná
konference	konference	k1gFnSc1	konference
vojenských	vojenský	k2eAgMnPc2d1	vojenský
velitelů	velitel	k1gMnPc2	velitel
Unie	unie	k1gFnSc2	unie
–	–	k?	–
sešel	sejít	k5eAaPmAgMnS	sejít
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
generál	generál	k1gMnSc1	generál
Grant	grant	k1gInSc1	grant
<g/>
,	,	kIx,	,
generál	generál	k1gMnSc1	generál
Sherman	Sherman	k1gMnSc1	Sherman
<g/>
,	,	kIx,	,
kontradmirál	kontradmirál	k1gMnSc1	kontradmirál
David	David	k1gMnSc1	David
D.	D.	kA	D.
Porter	porter	k1gInSc1	porter
a	a	k8xC	a
prezident	prezident	k1gMnSc1	prezident
Lincoln	Lincoln	k1gMnSc1	Lincoln
<g/>
.	.	kIx.	.
</s>
<s>
Programem	program	k1gInSc7	program
porady	porada	k1gFnSc2	porada
byla	být	k5eAaImAgFnS	být
koordinace	koordinace	k1gFnSc1	koordinace
postupu	postup	k1gInSc2	postup
všech	všecek	k3xTgFnPc2	všecek
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
složek	složka	k1gFnPc2	složka
k	k	k7c3	k
brzkému	brzký	k2eAgNnSc3d1	brzké
ukončení	ukončení	k1gNnSc3	ukončení
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tomto	tento	k3xDgNnSc6	tento
setkání	setkání	k1gNnSc6	setkání
zahájil	zahájit	k5eAaPmAgMnS	zahájit
generál	generál	k1gMnSc1	generál
Grant	grant	k1gInSc4	grant
tažení	tažení	k1gNnSc2	tažení
<g/>
,	,	kIx,	,
od	od	k7c2	od
něhož	jenž	k3xRgInSc2	jenž
očekával	očekávat	k5eAaImAgMnS	očekávat
co	co	k9	co
nejrychlejší	rychlý	k2eAgNnSc4d3	nejrychlejší
ukončení	ukončení	k1gNnSc4	ukončení
bojů	boj	k1gInPc2	boj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Konec	konec	k1gInSc1	konec
bojů	boj	k1gInPc2	boj
===	===	k?	===
</s>
</p>
<p>
<s>
Závěrečné	závěrečný	k2eAgInPc1d1	závěrečný
boje	boj	k1gInPc1	boj
proběhly	proběhnout	k5eAaPmAgInP	proběhnout
ve	v	k7c6	v
Virginii	Virginie	k1gFnSc6	Virginie
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Appomattox	Appomattox	k1gInSc4	Appomattox
<g/>
.	.	kIx.	.
</s>
<s>
Výsledek	výsledek	k1gInSc4	výsledek
byl	být	k5eAaImAgMnS	být
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	s	k7c7	s
9	[number]	k4	9
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
1865	[number]	k4	1865
ve	v	k7c6	v
zděném	zděný	k2eAgInSc6d1	zděný
domku	domek	k1gInSc6	domek
farmářské	farmářský	k2eAgFnSc2d1	farmářská
rodiny	rodina	k1gFnSc2	rodina
McCleanových	McCleanová	k1gFnPc2	McCleanová
u	u	k7c2	u
Appomattox	Appomattox	k1gInSc4	Appomattox
Court	Court	k1gInSc1	Court
House	house	k1gNnSc1	house
sešel	sejít	k5eAaPmAgInS	sejít
generál	generál	k1gMnSc1	generál
Ulysses	Ulyssesa	k1gFnPc2	Ulyssesa
Simpson	Simpsona	k1gFnPc2	Simpsona
Grant	grant	k1gInSc1	grant
a	a	k8xC	a
poražený	poražený	k2eAgMnSc1d1	poražený
generál	generál	k1gMnSc1	generál
Robert	Robert	k1gMnSc1	Robert
Edward	Edward	k1gMnSc1	Edward
Lee	Lea	k1gFnSc3	Lea
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
tu	tu	k6eAd1	tu
podepsali	podepsat	k5eAaPmAgMnP	podepsat
kapitulační	kapitulační	k2eAgFnSc4d1	kapitulační
listinu	listina	k1gFnSc4	listina
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
1865	[number]	k4	1865
Severovirginská	Severovirginský	k2eAgFnSc1d1	Severovirginský
armáda	armáda	k1gFnSc1	armáda
generála	generál	k1gMnSc2	generál
Leea	Leeus	k1gMnSc2	Leeus
složila	složit	k5eAaPmAgFnS	složit
své	svůj	k3xOyFgFnPc4	svůj
zbraně	zbraň	k1gFnPc4	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
14	[number]	k4	14
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
1865	[number]	k4	1865
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
Fordově	Fordův	k2eAgNnSc6d1	Fordovo
divadle	divadlo	k1gNnSc6	divadlo
spáchán	spáchán	k2eAgInSc4d1	spáchán
atentát	atentát	k1gInSc4	atentát
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
Abrahama	Abraham	k1gMnSc4	Abraham
Lincolna	Lincoln	k1gMnSc4	Lincoln
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
střelnému	střelný	k2eAgNnSc3d1	střelné
poranění	poranění	k1gNnSc3	poranění
o	o	k7c4	o
den	den	k1gInSc4	den
později	pozdě	k6eAd2	pozdě
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
<g/>
.	.	kIx.	.
</s>
<s>
Vrahem	vrah	k1gMnSc7	vrah
byl	být	k5eAaImAgMnS	být
bývalý	bývalý	k2eAgMnSc1d1	bývalý
herec	herec	k1gMnSc1	herec
John	John	k1gMnSc1	John
Wilkes	Wilkes	k1gMnSc1	Wilkes
Booth	Booth	k1gMnSc1	Booth
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
spiklenecké	spiklenecký	k2eAgFnSc2d1	spiklenecká
skupiny	skupina	k1gFnSc2	skupina
sympatizující	sympatizující	k2eAgFnPc4d1	sympatizující
s	s	k7c7	s
Jihem	jih	k1gInSc7	jih
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
prezidenta	prezident	k1gMnSc2	prezident
Lincolna	Lincoln	k1gMnSc2	Lincoln
měli	mít	k5eAaImAgMnP	mít
spiklenci	spiklenec	k1gMnPc1	spiklenec
v	v	k7c6	v
plánu	plán	k1gInSc6	plán
zavraždit	zavraždit	k5eAaPmF	zavraždit
také	také	k9	také
generála	generál	k1gMnSc4	generál
Granta	Grant	k1gMnSc4	Grant
<g/>
,	,	kIx,	,
viceprezidenta	viceprezident	k1gMnSc2	viceprezident
Andrewa	Andrewus	k1gMnSc2	Andrewus
Johnsona	Johnson	k1gMnSc2	Johnson
a	a	k8xC	a
ministra	ministr	k1gMnSc2	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
Williama	William	k1gMnSc2	William
H.	H.	kA	H.
Sewarda	Seward	k1gMnSc2	Seward
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
nepodařilo	podařit	k5eNaPmAgNnS	podařit
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
ministr	ministr	k1gMnSc1	ministr
Seward	Seward	k1gMnSc1	Seward
unikl	uniknout	k5eAaPmAgMnS	uniknout
zavraždění	zavraždění	k1gNnSc3	zavraždění
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
domě	dům	k1gInSc6	dům
jen	jen	k9	jen
náhodou	náhodou	k6eAd1	náhodou
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
pouze	pouze	k6eAd1	pouze
zraněn	zranit	k5eAaPmNgMnS	zranit
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
definitivní	definitivní	k2eAgFnSc3d1	definitivní
kapitulaci	kapitulace	k1gFnSc3	kapitulace
všech	všecek	k3xTgNnPc2	všecek
jižanských	jižanský	k2eAgNnPc2d1	jižanské
vojsk	vojsko	k1gNnPc2	vojsko
došlo	dojít	k5eAaPmAgNnS	dojít
až	až	k6eAd1	až
26	[number]	k4	26
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
1865	[number]	k4	1865
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Trans-Mississippi	Trans-Mississipp	k1gFnSc6	Trans-Mississipp
uzavřena	uzavřen	k2eAgFnSc1d1	uzavřena
dohoda	dohoda	k1gFnSc1	dohoda
o	o	k7c6	o
definitivním	definitivní	k2eAgNnSc6d1	definitivní
ukončení	ukončení	k1gNnSc6	ukončení
organizovaného	organizovaný	k2eAgInSc2d1	organizovaný
vojenského	vojenský	k2eAgInSc2d1	vojenský
odporu	odpor	k1gInSc2	odpor
s	s	k7c7	s
účinností	účinnost	k1gFnSc7	účinnost
od	od	k7c2	od
2	[number]	k4	2
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
1865	[number]	k4	1865
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Informování	informování	k1gNnPc4	informování
veřejnosti	veřejnost	k1gFnSc2	veřejnost
===	===	k?	===
</s>
</p>
<p>
<s>
Válka	válka	k1gFnSc1	válka
Severu	sever	k1gInSc2	sever
proti	proti	k7c3	proti
Jihu	jih	k1gInSc3	jih
je	být	k5eAaImIp3nS	být
historicky	historicky	k6eAd1	historicky
prvním	první	k4xOgInSc7	první
konfliktem	konflikt	k1gInSc7	konflikt
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byli	být	k5eAaImAgMnP	být
novináři	novinář	k1gMnPc1	novinář
odděleni	oddělit	k5eAaPmNgMnP	oddělit
od	od	k7c2	od
vojska	vojsko	k1gNnSc2	vojsko
a	a	k8xC	a
fungovali	fungovat	k5eAaImAgMnP	fungovat
čistě	čistě	k6eAd1	čistě
jako	jako	k8xS	jako
informátoři	informátor	k1gMnPc1	informátor
veřejnosti	veřejnost	k1gFnSc2	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
v	v	k7c4	v
tuto	tento	k3xDgFnSc4	tento
dobu	doba	k1gFnSc4	doba
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
žánr	žánr	k1gInSc4	žánr
reportáže	reportáž	k1gFnSc2	reportáž
a	a	k8xC	a
začali	začít	k5eAaPmAgMnP	začít
se	se	k3xPyFc4	se
objevovat	objevovat	k5eAaImF	objevovat
první	první	k4xOgMnPc1	první
váleční	váleční	k2eAgMnPc1d1	váleční
korespondenti	korespondent	k1gMnPc1	korespondent
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Konec	konec	k1gInSc1	konec
války	válka	k1gFnSc2	válka
a	a	k8xC	a
její	její	k3xOp3gInPc4	její
důsledky	důsledek	k1gInPc4	důsledek
==	==	k?	==
</s>
</p>
<p>
<s>
Zavražděním	zavraždění	k1gNnSc7	zavraždění
prezidenta	prezident	k1gMnSc2	prezident
Abrahama	Abraham	k1gMnSc2	Abraham
Lincolna	Lincoln	k1gMnSc2	Lincoln
14	[number]	k4	14
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1865	[number]	k4	1865
atentátníci	atentátník	k1gMnPc1	atentátník
Jihu	jih	k1gInSc2	jih
spíše	spíše	k9	spíše
uškodili	uškodit	k5eAaPmAgMnP	uškodit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
Jihu	jih	k1gInSc2	jih
se	se	k3xPyFc4	se
namísto	namísto	k7c2	namísto
Lincolna	Lincoln	k1gMnSc2	Lincoln
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
by	by	kYmCp3nS	by
díky	díky	k7c3	díky
svému	svůj	k3xOyFgNnSc3	svůj
vlivu	vliv	k1gInSc2	vliv
dokázal	dokázat	k5eAaPmAgMnS	dokázat
prosadit	prosadit	k5eAaPmF	prosadit
smířlivý	smířlivý	k2eAgInSc4d1	smířlivý
přístup	přístup	k1gInSc4	přístup
<g/>
,	,	kIx,	,
ujal	ujmout	k5eAaPmAgMnS	ujmout
Andrew	Andrew	k1gMnSc1	Andrew
Johnson	Johnson	k1gMnSc1	Johnson
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgMnSc1	tento
Lincolnův	Lincolnův	k2eAgMnSc1d1	Lincolnův
nástupce	nástupce	k1gMnSc1	nástupce
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
prezidenta	prezident	k1gMnSc2	prezident
svěřil	svěřit	k5eAaPmAgMnS	svěřit
znovuobnovení	znovuobnovení	k1gNnSc2	znovuobnovení
Jihu	jih	k1gInSc2	jih
radikálům	radikál	k1gMnPc3	radikál
toužícím	toužící	k2eAgInSc6d1	toužící
po	po	k7c6	po
pomstě	pomsta	k1gFnSc6	pomsta
na	na	k7c6	na
Jihu	jih	k1gInSc6	jih
za	za	k7c2	za
válečné	válečný	k2eAgFnSc2d1	válečná
útrapy	útrapa	k1gFnSc2	útrapa
–	–	k?	–
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
Scalawagy	Scalawaga	k1gFnPc4	Scalawaga
a	a	k8xC	a
Carpetbaggery	Carpetbaggera	k1gFnPc4	Carpetbaggera
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
tzv.	tzv.	kA	tzv.
taškáře	taškář	k1gMnSc4	taškář
a	a	k8xC	a
darebáky	darebák	k1gMnPc4	darebák
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tzv.	tzv.	kA	tzv.
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
jihu	jih	k1gInSc2	jih
probíhala	probíhat	k5eAaImAgFnS	probíhat
v	v	k7c6	v
letech	let	k1gInPc6	let
1865	[number]	k4	1865
<g/>
–	–	k?	–
<g/>
1877	[number]	k4	1877
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
byla	být	k5eAaImAgFnS	být
vytvořena	vytvořen	k2eAgFnSc1d1	vytvořena
vojenská	vojenský	k2eAgFnSc1d1	vojenská
správa	správa	k1gFnSc1	správa
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
ihned	ihned	k6eAd1	ihned
vynucováno	vynucován	k2eAgNnSc1d1	vynucováno
dodržování	dodržování	k1gNnSc1	dodržování
13	[number]	k4	13
<g/>
.	.	kIx.	.
a	a	k8xC	a
14	[number]	k4	14
<g/>
.	.	kIx.	.
dodatku	dodatek	k1gInSc2	dodatek
ústavy	ústava	k1gFnSc2	ústava
USA	USA	kA	USA
a	a	k8xC	a
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
negramotní	gramotný	k2eNgMnPc1d1	negramotný
bývalí	bývalý	k2eAgMnPc1d1	bývalý
otroci	otrok	k1gMnPc1	otrok
byli	být	k5eAaImAgMnP	být
dosazováni	dosazovat	k5eAaImNgMnP	dosazovat
do	do	k7c2	do
úřadů	úřad	k1gInPc2	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
také	také	k9	také
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
rasistická	rasistický	k2eAgFnSc1d1	rasistická
organizace	organizace	k1gFnSc1	organizace
Ku-Klux-Klan	Kuluxlan	k1gInSc1	Ku-klux-klan
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
rostoucí	rostoucí	k2eAgFnSc4d1	rostoucí
vlnu	vlna	k1gFnSc4	vlna
odporu	odpor	k1gInSc2	odpor
tak	tak	k6eAd1	tak
byla	být	k5eAaImAgFnS	být
vojenská	vojenský	k2eAgFnSc1d1	vojenská
okupace	okupace	k1gFnSc1	okupace
Jihu	jih	k1gInSc2	jih
nakonec	nakonec	k6eAd1	nakonec
ukončena	ukončit	k5eAaPmNgFnS	ukončit
a	a	k8xC	a
správa	správa	k1gFnSc1	správa
svěřena	svěřit	k5eAaPmNgFnS	svěřit
místním	místní	k2eAgFnPc3d1	místní
vládám	vláda	k1gFnPc3	vláda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
byla	být	k5eAaImAgFnS	být
vedle	vedle	k7c2	vedle
Indiánských	indiánský	k2eAgFnPc2d1	indiánská
válek	válka	k1gFnPc2	válka
proti	proti	k7c3	proti
domorodým	domorodý	k2eAgInPc3d1	domorodý
kmenům	kmen	k1gInPc3	kmen
nejkrvavějším	krvavý	k2eAgMnSc7d3	nejkrvavější
konfliktem	konflikt	k1gInSc7	konflikt
na	na	k7c6	na
území	území	k1gNnSc6	území
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
asi	asi	k9	asi
10	[number]	k4	10
<g/>
%	%	kIx~	%
všech	všecek	k3xTgMnPc2	všecek
mužů	muž	k1gMnPc2	muž
ze	z	k7c2	z
Severu	sever	k1gInSc2	sever
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
20-45	[number]	k4	20-45
let	léto	k1gNnPc2	léto
a	a	k8xC	a
okolo	okolo	k7c2	okolo
30	[number]	k4	30
<g/>
%	%	kIx~	%
všech	všecek	k3xTgMnPc2	všecek
bílých	bílý	k2eAgMnPc2d1	bílý
mužů	muž	k1gMnPc2	muž
z	z	k7c2	z
Jihu	jih	k1gInSc2	jih
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
18-40	[number]	k4	18-40
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k8xC	a
přestože	přestože	k8xS	přestože
byli	být	k5eAaImAgMnP	být
černí	černý	k2eAgMnPc1d1	černý
otroci	otrok	k1gMnPc1	otrok
podle	podle	k7c2	podle
zákona	zákon	k1gInSc2	zákon
svobodní	svobodný	k2eAgMnPc1d1	svobodný
<g/>
,	,	kIx,	,
neznamenalo	znamenat	k5eNaImAgNnS	znamenat
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
plnoprávní	plnoprávní	k2eAgMnPc1d1	plnoprávní
občané	občan	k1gMnPc1	občan
<g/>
.	.	kIx.	.
</s>
<s>
Pět	pět	k4xCc1	pět
let	léto	k1gNnPc2	léto
po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
1870	[number]	k4	1870
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
americké	americký	k2eAgFnSc2d1	americká
sněmovny	sněmovna	k1gFnSc2	sněmovna
reprezentantů	reprezentant	k1gMnPc2	reprezentant
zvolen	zvolen	k2eAgMnSc1d1	zvolen
první	první	k4xOgMnSc1	první
Afroameričan	Afroameričan	k1gMnSc1	Afroameričan
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
jím	jíst	k5eAaImIp1nS	jíst
syn	syn	k1gMnSc1	syn
otroků	otrok	k1gMnPc2	otrok
<g/>
,	,	kIx,	,
holič	holič	k1gMnSc1	holič
Joseph	Joseph	k1gMnSc1	Joseph
Rainey	Rainea	k1gFnSc2	Rainea
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ekonomické	ekonomický	k2eAgInPc4d1	ekonomický
dopady	dopad	k1gInPc4	dopad
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
==	==	k?	==
</s>
</p>
<p>
<s>
Občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
samozřejmě	samozřejmě	k6eAd1	samozřejmě
přinesla	přinést	k5eAaPmAgFnS	přinést
oběma	dva	k4xCgFnPc3	dva
znepřáteleným	znepřátelený	k2eAgFnPc3d1	znepřátelená
stranám	strana	k1gFnPc3	strana
nesmírné	smírný	k2eNgFnSc2d1	nesmírná
hospodářské	hospodářský	k2eAgFnPc4d1	hospodářská
a	a	k8xC	a
ekonomické	ekonomický	k2eAgFnPc4d1	ekonomická
ztráty	ztráta	k1gFnPc4	ztráta
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
došlo	dojít	k5eAaPmAgNnS	dojít
také	také	k9	také
k	k	k7c3	k
dalšímu	další	k2eAgNnSc3d1	další
prohloubení	prohloubení	k1gNnSc3	prohloubení
už	už	k6eAd1	už
existujících	existující	k2eAgInPc2d1	existující
rozdílů	rozdíl	k1gInPc2	rozdíl
v	v	k7c6	v
ekonomikách	ekonomika	k1gFnPc6	ekonomika
obou	dva	k4xCgInPc2	dva
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Slabší	slabý	k2eAgInSc1d2	slabší
Jih	jih	k1gInSc1	jih
byl	být	k5eAaImAgInS	být
válkou	válka	k1gFnSc7	válka
zcela	zcela	k6eAd1	zcela
zdevastován	zdevastován	k2eAgMnSc1d1	zdevastován
a	a	k8xC	a
poslední	poslední	k2eAgFnSc7d1	poslední
ranou	rána	k1gFnSc7	rána
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
taktika	taktika	k1gFnSc1	taktika
spálené	spálený	k2eAgFnSc2d1	spálená
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
při	při	k7c6	při
obsazování	obsazování	k1gNnSc6	obsazování
Jihu	jih	k1gInSc2	jih
nekompromisně	kompromisně	k6eNd1	kompromisně
prosazoval	prosazovat	k5eAaImAgMnS	prosazovat
generál	generál	k1gMnSc1	generál
William	William	k1gInSc4	William
Tecumseh	Tecumseh	k1gMnSc1	Tecumseh
Sherman	Sherman	k1gMnSc1	Sherman
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
Sever	sever	k1gInSc1	sever
se	se	k3xPyFc4	se
během	během	k7c2	během
války	válka	k1gFnSc2	válka
změnil	změnit	k5eAaPmAgInS	změnit
z	z	k7c2	z
agrárně-průmyslové	agrárněrůmyslový	k2eAgFnSc2d1	agrárně-průmyslový
země	zem	k1gFnSc2	zem
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
s	s	k7c7	s
industriálním	industriální	k2eAgInSc7d1	industriální
systémem	systém	k1gInSc7	systém
a	a	k8xC	a
jednotným	jednotný	k2eAgInSc7d1	jednotný
trhem	trh	k1gInSc7	trh
<g/>
.	.	kIx.	.
</s>
<s>
Průmysl	průmysl	k1gInSc1	průmysl
zde	zde	k6eAd1	zde
zažil	zažít	k5eAaPmAgInS	zažít
obrovský	obrovský	k2eAgInSc4d1	obrovský
růst	růst	k1gInSc4	růst
díky	díky	k7c3	díky
vládním	vládní	k2eAgFnPc3d1	vládní
zakázkám	zakázka	k1gFnPc3	zakázka
–	–	k?	–
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
zvýšení	zvýšení	k1gNnSc3	zvýšení
výroby	výroba	k1gFnSc2	výroba
železa	železo	k1gNnSc2	železo
<g/>
,	,	kIx,	,
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
se	se	k3xPyFc4	se
těžba	těžba	k1gFnSc1	těžba
uhlí	uhlí	k1gNnSc2	uhlí
<g/>
,	,	kIx,	,
rostl	růst	k5eAaImAgInS	růst
zbrojní	zbrojní	k2eAgMnSc1d1	zbrojní
<g/>
,	,	kIx,	,
strojírenský	strojírenský	k2eAgInSc1d1	strojírenský
a	a	k8xC	a
textilní	textilní	k2eAgInSc1d1	textilní
průmysl	průmysl	k1gInSc1	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Válečné	válečný	k2eAgNnSc1d1	válečné
úsilí	úsilí	k1gNnSc1	úsilí
vyžadovalo	vyžadovat	k5eAaImAgNnS	vyžadovat
neprodlené	prodlený	k2eNgNnSc1d1	neprodlené
zavádění	zavádění	k1gNnSc1	zavádění
nových	nový	k2eAgInPc2d1	nový
vynálezů	vynález	k1gInPc2	vynález
do	do	k7c2	do
výroby	výroba	k1gFnSc2	výroba
a	a	k8xC	a
také	také	k9	také
do	do	k7c2	do
zemědělství	zemědělství	k1gNnSc2	zemědělství
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
chyběla	chybět	k5eAaImAgFnS	chybět
pracovní	pracovní	k2eAgFnSc1d1	pracovní
síla	síla	k1gFnSc1	síla
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
farmách	farma	k1gFnPc6	farma
v	v	k7c6	v
Unii	unie	k1gFnSc6	unie
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
začaly	začít	k5eAaPmAgFnP	začít
objevovat	objevovat	k5eAaImF	objevovat
zcela	zcela	k6eAd1	zcela
nové	nový	k2eAgInPc4d1	nový
stroje	stroj	k1gInPc4	stroj
jako	jako	k8xS	jako
mlátičky	mlátička	k1gFnPc4	mlátička
obilí	obilí	k1gNnSc2	obilí
a	a	k8xC	a
otočné	otočný	k2eAgInPc4d1	otočný
pluhy	pluh	k1gInPc4	pluh
<g/>
.	.	kIx.	.
</s>
<s>
Válka	válka	k1gFnSc1	válka
však	však	k9	však
také	také	k9	také
odpuzovala	odpuzovat	k5eAaImAgFnS	odpuzovat
další	další	k2eAgFnPc4d1	další
vlny	vlna	k1gFnPc4	vlna
přistěhovalců	přistěhovalec	k1gMnPc2	přistěhovalec
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
Sever	sever	k1gInSc1	sever
přicházel	přicházet	k5eAaImAgInS	přicházet
o	o	k7c4	o
levnou	levný	k2eAgFnSc4d1	levná
pracovní	pracovní	k2eAgFnSc4d1	pracovní
sílu	síla	k1gFnSc4	síla
<g/>
.	.	kIx.	.
</s>
<s>
Oběma	dva	k4xCgFnPc3	dva
válčícím	válčící	k2eAgFnPc3d1	válčící
stranám	strana	k1gFnPc3	strana
odčerpaly	odčerpat	k5eAaPmAgInP	odčerpat
boje	boj	k1gInPc1	boj
značný	značný	k2eAgInSc1d1	značný
finanční	finanční	k2eAgInSc4d1	finanční
kapitál	kapitál	k1gInSc4	kapitál
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
nadměrnému	nadměrný	k2eAgNnSc3d1	nadměrné
rozšíření	rozšíření	k1gNnSc3	rozšíření
papírových	papírový	k2eAgInPc2d1	papírový
peněz	peníze	k1gInPc2	peníze
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
následně	následně	k6eAd1	následně
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
inflaci	inflace	k1gFnSc4	inflace
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
děsivá	děsivý	k2eAgFnSc1d1	děsivá
především	především	k9	především
na	na	k7c6	na
Jihu	jih	k1gInSc6	jih
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
války	válka	k1gFnSc2	válka
dosahovala	dosahovat	k5eAaImAgFnS	dosahovat
neuvěřitelných	uvěřitelný	k2eNgNnPc2d1	neuvěřitelné
9	[number]	k4	9
000	[number]	k4	000
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Americká	americký	k2eAgFnSc1d1	americká
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
také	také	k9	také
přinesla	přinést	k5eAaPmAgFnS	přinést
velké	velký	k2eAgFnPc4d1	velká
šance	šance	k1gFnPc4	šance
nejen	nejen	k6eAd1	nejen
pro	pro	k7c4	pro
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
finančních	finanční	k2eAgMnPc2d1	finanční
dobrodruhů	dobrodruh	k1gMnPc2	dobrodruh
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
pro	pro	k7c4	pro
skutečně	skutečně	k6eAd1	skutečně
odvážné	odvážný	k2eAgMnPc4d1	odvážný
a	a	k8xC	a
schopné	schopný	k2eAgMnPc4d1	schopný
podnikatele	podnikatel	k1gMnPc4	podnikatel
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
v	v	k7c6	v
době	doba	k1gFnSc6	doba
této	tento	k3xDgFnSc2	tento
války	válka	k1gFnSc2	válka
vznikaly	vznikat	k5eAaImAgInP	vznikat
základy	základ	k1gInPc1	základ
kapitálu	kapitál	k1gInSc3	kapitál
mnohých	mnohý	k2eAgFnPc2d1	mnohá
rodinných	rodinný	k2eAgFnPc2d1	rodinná
dynastií	dynastie	k1gFnPc2	dynastie
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
následně	následně	k6eAd1	následně
ovládaly	ovládat	k5eAaImAgFnP	ovládat
ekonomiku	ekonomika	k1gFnSc4	ekonomika
USA	USA	kA	USA
–	–	k?	–
mezi	mezi	k7c4	mezi
tyto	tento	k3xDgMnPc4	tento
podnikatele	podnikatel	k1gMnPc4	podnikatel
patřil	patřit	k5eAaImAgInS	patřit
např.	např.	kA	např.
Andrew	Andrew	k1gFnSc2	Andrew
Carnegie	Carnegie	k1gFnSc2	Carnegie
<g/>
,	,	kIx,	,
Cornelius	Cornelius	k1gMnSc1	Cornelius
Vanderbilt	Vanderbilt	k1gMnSc1	Vanderbilt
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Pierpont	Pierpont	k1gMnSc1	Pierpont
Morgan	morgan	k1gMnSc1	morgan
nebo	nebo	k8xC	nebo
John	John	k1gMnSc1	John
Davidson	Davidson	k1gMnSc1	Davidson
Rockefeller	Rockefeller	k1gMnSc1	Rockefeller
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Čechoameričané	Čechoameričan	k1gMnPc1	Čechoameričan
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
oficiální	oficiální	k2eAgFnSc2d1	oficiální
statistiky	statistika	k1gFnSc2	statistika
(	(	kIx(	(
<g/>
byla	být	k5eAaImAgFnS	být
oproti	oproti	k7c3	oproti
realitě	realita	k1gFnSc3	realita
značně	značně	k6eAd1	značně
podhodnocená	podhodnocený	k2eAgFnSc1d1	podhodnocená
<g/>
)	)	kIx)	)
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1860	[number]	k4	1860
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
na	na	k7c4	na
23	[number]	k4	23
000	[number]	k4	000
českých	český	k2eAgMnPc2d1	český
rodáků	rodák	k1gMnPc2	rodák
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc4	jejich
početní	početní	k2eAgInSc4d1	početní
nárůst	nárůst	k1gInSc4	nárůst
nezastavila	zastavit	k5eNaPmAgFnS	zastavit
ani	ani	k8xC	ani
následná	následný	k2eAgFnSc1d1	následná
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Za	za	k7c4	za
Unii	unie	k1gFnSc4	unie
===	===	k?	===
</s>
</p>
<p>
<s>
Většina	většina	k1gFnSc1	většina
Čechů	Čech	k1gMnPc2	Čech
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
přišli	přijít	k5eAaPmAgMnP	přijít
do	do	k7c2	do
USA	USA	kA	USA
před	před	k7c7	před
válkou	válka	k1gFnSc7	válka
<g/>
,	,	kIx,	,
sloužila	sloužit	k5eAaImAgFnS	sloužit
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
ozbrojených	ozbrojený	k2eAgFnPc6d1	ozbrojená
silách	síla	k1gFnPc6	síla
Unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vycházelo	vycházet	k5eAaImAgNnS	vycházet
už	už	k9	už
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
majorita	majorita	k1gFnSc1	majorita
českých	český	k2eAgMnPc2d1	český
přistěhovalců	přistěhovalec	k1gMnPc2	přistěhovalec
žila	žít	k5eAaImAgFnS	žít
právě	právě	k9	právě
v	v	k7c6	v
severních	severní	k2eAgInPc6d1	severní
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
vysokému	vysoký	k2eAgInSc3d1	vysoký
počtu	počet	k1gInSc3	počet
Čechů	Čech	k1gMnPc2	Čech
nevznikla	vzniknout	k5eNaPmAgFnS	vzniknout
žádná	žádný	k3yNgFnSc1	žádný
ucelená	ucelený	k2eAgFnSc1d1	ucelená
česká	český	k2eAgFnSc1d1	Česká
jednotka	jednotka	k1gFnSc1	jednotka
<g/>
,	,	kIx,	,
s	s	k7c7	s
jakými	jaký	k3yIgInPc7	jaký
se	se	k3xPyFc4	se
setkáváme	setkávat	k5eAaImIp1nP	setkávat
třeba	třeba	k6eAd1	třeba
u	u	k7c2	u
Němců	Němec	k1gMnPc2	Němec
<g/>
,	,	kIx,	,
Irů	Ir	k1gMnPc2	Ir
a	a	k8xC	a
dalších	další	k2eAgNnPc2d1	další
etnik	etnikum	k1gNnPc2	etnikum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
existovala	existovat	k5eAaImAgFnS	existovat
Slovanská	slovanský	k2eAgFnSc1d1	Slovanská
setnina	setnina	k1gFnSc1	setnina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
vůbec	vůbec	k9	vůbec
první	první	k4xOgFnSc1	první
jednotkou	jednotka	k1gFnSc7	jednotka
dobrovolníků	dobrovolník	k1gMnPc2	dobrovolník
vyslanou	vyslaný	k2eAgFnSc4d1	vyslaná
na	na	k7c6	na
frontu	front	k1gInSc6	front
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
Lincoln	Lincoln	k1gMnSc1	Lincoln
také	také	k9	také
vydal	vydat	k5eAaPmAgMnS	vydat
souhlas	souhlas	k1gInSc4	souhlas
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
Slovanská	slovanský	k2eAgFnSc1d1	Slovanská
setnina	setnina	k1gFnSc1	setnina
z	z	k7c2	z
Chicaga	Chicago	k1gNnSc2	Chicago
přejmenovala	přejmenovat	k5eAaPmAgFnS	přejmenovat
na	na	k7c4	na
Slovanskou	slovanský	k2eAgFnSc4d1	Slovanská
Lincolnovu	Lincolnův	k2eAgFnSc4d1	Lincolnova
střeleckou	střelecký	k2eAgFnSc4d1	střelecká
rotu	rota	k1gFnSc4	rota
(	(	kIx(	(
<g/>
Lincoln	Lincoln	k1gMnSc1	Lincoln
Riflemen	Riflemen	k1gInSc1	Riflemen
of	of	k?	of
Slavonic	Slavonice	k1gFnPc2	Slavonice
Origin	Origin	k1gMnSc1	Origin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
do	do	k7c2	do
frontové	frontový	k2eAgFnSc2d1	frontová
linie	linie	k1gFnSc2	linie
nasazena	nasazen	k2eAgFnSc1d1	nasazena
21	[number]	k4	21
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
1861	[number]	k4	1861
<g/>
.	.	kIx.	.
</s>
<s>
Jednotka	jednotka	k1gFnSc1	jednotka
se	se	k3xPyFc4	se
však	však	k9	však
rozpadla	rozpadnout	k5eAaPmAgFnS	rozpadnout
už	už	k6eAd1	už
v	v	k7c6	v
prvních	první	k4xOgInPc6	první
měsících	měsíc	k1gInPc6	měsíc
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Češi	Čech	k1gMnPc1	Čech
tak	tak	k6eAd1	tak
sloužili	sloužit	k5eAaImAgMnP	sloužit
roztroušení	roztroušení	k1gNnSc4	roztroušení
po	po	k7c6	po
mnoha	mnoho	k4c6	mnoho
jednotkách	jednotka	k1gFnPc6	jednotka
z	z	k7c2	z
řady	řada	k1gFnSc2	řada
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Nejpočetnější	početní	k2eAgFnSc1d3	nejpočetnější
skupina	skupina	k1gFnSc1	skupina
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
36	[number]	k4	36
mužů	muž	k1gMnPc2	muž
<g/>
)	)	kIx)	)
bojovala	bojovat	k5eAaImAgFnS	bojovat
v	v	k7c6	v
řadách	řada	k1gFnPc6	řada
26	[number]	k4	26
<g/>
.	.	kIx.	.
wisconsinského	wisconsinský	k2eAgInSc2d1	wisconsinský
dobrovolnického	dobrovolnický	k2eAgInSc2d1	dobrovolnický
pěšího	pěší	k2eAgInSc2d1	pěší
pluku	pluk	k1gInSc2	pluk
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnPc1d1	další
sloužili	sloužit	k5eAaImAgMnP	sloužit
v	v	k7c6	v
22	[number]	k4	22
<g/>
.	.	kIx.	.
iowském	iowské	k1gNnSc6	iowské
pluku	pluk	k1gInSc2	pluk
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
.	.	kIx.	.
missourském	missourský	k2eAgInSc6d1	missourský
pluku	pluk	k1gInSc6	pluk
a	a	k8xC	a
jiných	jiný	k2eAgFnPc6d1	jiná
<g/>
.	.	kIx.	.
</s>
<s>
Čechoameričané	Čechoameričan	k1gMnPc1	Čechoameričan
pochopitelně	pochopitelně	k6eAd1	pochopitelně
nechyběli	chybět	k5eNaImAgMnP	chybět
u	u	k7c2	u
většiny	většina	k1gFnSc2	většina
významných	významný	k2eAgFnPc2d1	významná
bitev	bitva	k1gFnPc2	bitva
–	–	k?	–
např.	např.	kA	např.
bitvy	bitva	k1gFnSc2	bitva
u	u	k7c2	u
Gettysburgu	Gettysburg	k1gInSc2	Gettysburg
nebo	nebo	k8xC	nebo
bitvy	bitva	k1gFnSc2	bitva
u	u	k7c2	u
Fredericksburgu	Fredericksburg	k1gInSc2	Fredericksburg
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
důstojnických	důstojnický	k2eAgInPc2d1	důstojnický
hodností	hodnost	k1gFnSc7	hodnost
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Adolf	Adolf	k1gMnSc1	Adolf
Chládek	Chládek	k1gMnSc1	Chládek
<g/>
,	,	kIx,	,
původem	původ	k1gInSc7	původ
z	z	k7c2	z
Rokycan	Rokycany	k1gInPc2	Rokycany
<g/>
,	,	kIx,	,
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
u	u	k7c2	u
9	[number]	k4	9
<g/>
.	.	kIx.	.
pluku	pluk	k1gInSc2	pluk
z	z	k7c2	z
Wisconsinu	Wisconsina	k1gFnSc4	Wisconsina
hodnosti	hodnost	k1gFnSc3	hodnost
poručíka	poručík	k1gMnSc2	poručík
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
hodnosti	hodnost	k1gFnSc2	hodnost
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
jindřichohradecký	jindřichohradecký	k2eAgMnSc1d1	jindřichohradecký
rodák	rodák	k1gMnSc1	rodák
Antonín	Antonín	k1gMnSc1	Antonín
Pokorný	Pokorný	k1gMnSc1	Pokorný
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
získal	získat	k5eAaPmAgInS	získat
hodnost	hodnost	k1gFnSc4	hodnost
podplukovníka	podplukovník	k1gMnSc2	podplukovník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Za	za	k7c4	za
Konfederaci	konfederace	k1gFnSc4	konfederace
===	===	k?	===
</s>
</p>
<p>
<s>
Jediným	jediný	k2eAgInSc7d1	jediný
jižanským	jižanský	k2eAgInSc7d1	jižanský
státem	stát	k1gInSc7	stát
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
během	během	k7c2	během
války	válka	k1gFnSc2	válka
žilo	žít	k5eAaImAgNnS	žít
větší	veliký	k2eAgNnSc1d2	veliký
množství	množství	k1gNnSc1	množství
českých	český	k2eAgMnPc2d1	český
přistěhovalců	přistěhovalec	k1gMnPc2	přistěhovalec
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
Texas	Texas	k1gInSc1	Texas
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
malé	malý	k2eAgNnSc1d1	malé
množství	množství	k1gNnSc1	množství
texaských	texaský	k2eAgMnPc2d1	texaský
Čechů	Čech	k1gMnPc2	Čech
vstoupilo	vstoupit	k5eAaPmAgNnS	vstoupit
do	do	k7c2	do
armády	armáda	k1gFnSc2	armáda
Konfederace	konfederace	k1gFnSc2	konfederace
z	z	k7c2	z
vlastní	vlastní	k2eAgFnSc2d1	vlastní
vůle	vůle	k1gFnSc2	vůle
<g/>
.	.	kIx.	.
</s>
<s>
Válečné	válečný	k2eAgInPc1d1	válečný
cíle	cíl	k1gInPc1	cíl
Konfederace	konfederace	k1gFnSc2	konfederace
byly	být	k5eAaImAgInP	být
většině	většina	k1gFnSc3	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
cizí	cizí	k2eAgMnPc1d1	cizí
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
u	u	k7c2	u
nich	on	k3xPp3gMnPc2	on
setkáváme	setkávat	k5eAaImIp1nP	setkávat
se	s	k7c7	s
snahou	snaha	k1gFnSc7	snaha
vyhýbat	vyhýbat	k5eAaImF	vyhýbat
se	se	k3xPyFc4	se
odvodům	odvod	k1gInPc3	odvod
a	a	k8xC	a
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
dezercí	dezerce	k1gFnSc7	dezerce
<g/>
.	.	kIx.	.
</s>
<s>
Něco	něco	k3yInSc1	něco
napovídá	napovídat	k5eAaBmIp3nS	napovídat
už	už	k9	už
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
mezi	mezi	k7c7	mezi
tamními	tamní	k2eAgMnPc7d1	tamní
krajany	krajan	k1gMnPc7	krajan
je	být	k5eAaImIp3nS	být
znám	znát	k5eAaImIp1nS	znát
pouze	pouze	k6eAd1	pouze
jediný	jediný	k2eAgMnSc1d1	jediný
vlastník	vlastník	k1gMnSc1	vlastník
otroka	otrok	k1gMnSc2	otrok
<g/>
,	,	kIx,	,
obchodník	obchodník	k1gMnSc1	obchodník
Reimershoffer	Reimershoffer	k1gMnSc1	Reimershoffer
(	(	kIx(	(
<g/>
koupil	koupit	k5eAaPmAgMnS	koupit
si	se	k3xPyFc3	se
devítileté	devítiletý	k2eAgNnSc4d1	devítileté
děvčátko	děvčátko	k1gNnSc4	děvčátko
<g/>
,	,	kIx,	,
prý	prý	k9	prý
aby	aby	kYmCp3nS	aby
v	v	k7c6	v
očích	oko	k1gNnPc6	oko
podezřívavých	podezřívavý	k2eAgMnPc2d1	podezřívavý
amerických	americký	k2eAgMnPc2d1	americký
sousedů	soused	k1gMnPc2	soused
utvrdil	utvrdit	k5eAaPmAgInS	utvrdit
svoji	svůj	k3xOyFgFnSc4	svůj
loajalitu	loajalita	k1gFnSc4	loajalita
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
Čechů	Čech	k1gMnPc2	Čech
však	však	k9	však
chovala	chovat	k5eAaImAgFnS	chovat
opačné	opačný	k2eAgInPc4d1	opačný
postoje	postoj	k1gInPc4	postoj
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Josef	Josef	k1gMnSc1	Josef
Lidumil	lidumil	k1gMnSc1	lidumil
Lešikar	Lešikar	k1gMnSc1	Lešikar
<g/>
,	,	kIx,	,
přítel	přítel	k1gMnSc1	přítel
Boženy	Božena	k1gFnSc2	Božena
Němcové	Němcová	k1gFnSc2	Němcová
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
pomáhal	pomáhat	k5eAaImAgInS	pomáhat
otrokům	otrok	k1gMnPc3	otrok
prchat	prchat	k5eAaImF	prchat
do	do	k7c2	do
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Někteří	některý	k3yIgMnPc1	některý
texaští	texaský	k2eAgMnPc1d1	texaský
krajané	krajan	k1gMnPc1	krajan
zaplatili	zaplatit	k5eAaPmAgMnP	zaplatit
za	za	k7c4	za
odpor	odpor	k1gInSc4	odpor
ke	k	k7c3	k
Konfederaci	konfederace	k1gFnSc3	konfederace
léty	léto	k1gNnPc7	léto
skrývání	skrývání	k1gNnSc2	skrývání
<g/>
,	,	kIx,	,
útěkem	útěk	k1gInSc7	útěk
do	do	k7c2	do
Mexika	Mexiko	k1gNnSc2	Mexiko
a	a	k8xC	a
přinejhorším	přinejhorším	k6eAd1	přinejhorším
i	i	k9	i
životem	život	k1gInSc7	život
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
dobrovolník	dobrovolník	k1gMnSc1	dobrovolník
prošel	projít	k5eAaPmAgMnS	projít
službou	služba	k1gFnSc7	služba
také	také	k6eAd1	také
mladý	mladý	k2eAgMnSc1d1	mladý
Augustin	Augustin	k1gMnSc1	Augustin
Hajdušek	Hajduška	k1gFnPc2	Hajduška
<g/>
,	,	kIx,	,
zajímavá	zajímavý	k2eAgFnSc1d1	zajímavá
postava	postava	k1gFnSc1	postava
texaských	texaský	k2eAgFnPc2d1	texaská
dějin	dějiny	k1gFnPc2	dějiny
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
všeobecně	všeobecně	k6eAd1	všeobecně
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
soudce	soudce	k1gMnSc1	soudce
Hajdušek	Hajduška	k1gFnPc2	Hajduška
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
skupina	skupina	k1gFnSc1	skupina
texaských	texaský	k2eAgMnPc2d1	texaský
Čechů	Čech	k1gMnPc2	Čech
(	(	kIx(	(
<g/>
14	[number]	k4	14
mužů	muž	k1gMnPc2	muž
<g/>
)	)	kIx)	)
sloužila	sloužit	k5eAaImAgFnS	sloužit
ve	v	k7c6	v
Waulově	Waulův	k2eAgFnSc6d1	Waulův
legii	legie	k1gFnSc6	legie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
CHABR	CHABR	kA	CHABR
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nejtěžších	těžký	k2eAgFnPc6d3	nejtěžší
chvílích	chvíle	k1gFnPc6	chvíle
unie	unie	k1gFnSc2	unie
(	(	kIx(	(
<g/>
Abraham	Abraham	k1gMnSc1	Abraham
Lincoln	Lincoln	k1gMnSc1	Lincoln
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Vesmír	vesmír	k1gInSc1	vesmír
<g/>
,	,	kIx,	,
1926	[number]	k4	1926
<g/>
.	.	kIx.	.
221	[number]	k4	221
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
český	český	k2eAgMnSc1d1	český
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Leonid	Leonid	k1gInSc1	Leonid
Křížek	křížek	k1gInSc1	křížek
a	a	k8xC	a
kolektiv	kolektiv	k1gInSc1	kolektiv
<g/>
:	:	kIx,	:
Americká	americký	k2eAgFnSc1d1	americká
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
X-Egem	X-Eg	k1gInSc7	X-Eg
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-85395-54-1	[number]	k4	80-85395-54-1
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Opatrný	opatrný	k2eAgMnSc1d1	opatrný
<g/>
:	:	kIx,	:
Válka	válka	k1gFnSc1	válka
Severu	sever	k1gInSc2	sever
proti	proti	k7c3	proti
Jihu	jih	k1gInSc3	jih
<g/>
,	,	kIx,	,
Libri	Libr	k1gInSc3	Libr
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-85983-37-0	[number]	k4	80-85983-37-0
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Hutečka	Hutečko	k1gNnSc2	Hutečko
<g/>
:	:	kIx,	:
Země	země	k1gFnSc1	země
krví	krvit	k5eAaImIp3nS	krvit
zbrocená	zbrocený	k2eAgFnSc1d1	zbrocená
<g/>
,	,	kIx,	,
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-7106-955-3	[number]	k4	978-80-7106-955-3
</s>
</p>
<p>
<s>
Marek	Marek	k1gMnSc1	Marek
Vlha	vlha	k1gFnSc1	vlha
<g/>
:	:	kIx,	:
Dopisy	dopis	k1gInPc1	dopis
z	z	k7c2	z
války	válka	k1gFnSc2	válka
Severu	sever	k1gInSc2	sever
proti	proti	k7c3	proti
Jihu	jih	k1gInSc3	jih
<g/>
,	,	kIx,	,
Matice	matice	k1gFnSc1	matice
moravská	moravský	k2eAgFnSc1d1	Moravská
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-86488-72-1	[number]	k4	978-80-86488-72-1
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
intervence	intervence	k1gFnSc1	intervence
v	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
americká	americký	k2eAgFnSc1d1	americká
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc4	Commons
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
americká	americký	k2eAgFnSc1d1	americká
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Česká	český	k2eAgFnSc1d1	Česká
asociace	asociace	k1gFnSc1	asociace
americké	americký	k2eAgFnSc2d1	americká
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
</s>
</p>
