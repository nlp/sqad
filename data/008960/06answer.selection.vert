<s>
Přestože	přestože	k8xS	přestože
Sever	sever	k1gInSc1	sever
byl	být	k5eAaImAgInS	být
průmyslově	průmyslově	k6eAd1	průmyslově
rozvinutější	rozvinutý	k2eAgInSc1d2	rozvinutější
a	a	k8xC	a
měl	mít	k5eAaImAgInS	mít
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
větší	veliký	k2eAgInSc4d2	veliký
kapitál	kapitál	k1gInSc4	kapitál
<g/>
,	,	kIx,	,
suroviny	surovina	k1gFnPc4	surovina
a	a	k8xC	a
zbraně	zbraň	k1gFnPc4	zbraň
<g/>
,	,	kIx,	,
nedostávalo	dostávat	k5eNaImAgNnS	dostávat
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
zkušených	zkušený	k2eAgMnPc2d1	zkušený
vojevůdců	vojevůdce	k1gMnPc2	vojevůdce
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
většina	většina	k1gFnSc1	většina
důstojníků	důstojník	k1gMnPc2	důstojník
pocházela	pocházet	k5eAaImAgFnS	pocházet
z	z	k7c2	z
Jihu	jih	k1gInSc2	jih
a	a	k8xC	a
odešla	odejít	k5eAaPmAgFnS	odejít
do	do	k7c2	do
konfederačních	konfederační	k2eAgFnPc2d1	konfederační
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
