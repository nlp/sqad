<s>
Jaroslav	Jaroslav	k1gMnSc1
Alois	Alois	k1gMnSc1
z	z	k7c2
Lobkowicz	Lobkowicz	k1gMnSc1
<g/>
,	,	kIx,
též	též	k9
z	z	k7c2
Lobkovic	Lobkovice	k1gInPc2
(	(	kIx(
<g/>
26	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1877	#num#	k4
Konopiště	Konopiště	k1gNnSc4
–	–	k?
24	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1953	#num#	k4
Křimice	Křimika	k1gFnSc3
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
Plzeň	Plzeň	k1gFnSc1
5	#num#	k4
<g/>
-Křimice	-Křimika	k1gFnSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
celým	celý	k2eAgNnSc7d1
jménem	jméno	k1gNnSc7
Jaroslav	Jaroslav	k1gMnSc1
Alois	Alois	k1gMnSc1
František	František	k1gMnSc1
Kunigund	Kunigund	k1gMnSc1
Emanuel	Emanuel	k1gMnSc1
Maria	Mario	k1gMnSc2
kníže	kníže	k1gMnSc1
z	z	k7c2
Lobkowicz	Lobkowicz	k1gMnSc1
byl	být	k5eAaImAgMnS
český	český	k2eAgMnSc1d1
šlechtic	šlechtic	k1gMnSc1
z	z	k7c2
(	(	kIx(
<g/>
konopišťsko-	konopišťsko-	k?
<g/>
)	)	kIx)
<g/>
křimické	křimický	k2eAgFnPc1d1
linie	linie	k1gFnPc1
šlechtického	šlechtický	k2eAgInSc2d1
rodu	rod	k1gInSc2
Lobkowiczů	Lobkowicz	k1gMnPc2
a	a	k8xC
majitel	majitel	k1gMnSc1
statku	statek	k1gInSc2
Křimice	Křimika	k1gFnSc3
<g/>
.	.	kIx.
</s>