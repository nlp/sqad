<s>
Jaroslav	Jaroslav	k1gMnSc1
Lobkowicz	Lobkowicz	k1gMnSc1
(	(	kIx(
<g/>
1877	#num#	k4
<g/>
–	–	k?
<g/>
1953	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
JUDr.	JUDr.	kA
Jaroslav	Jaroslav	k1gMnSc1
Lobkowicz	Lobkowicz	k1gMnSc1
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
kníže	kníže	k1gMnSc1
z	z	k7c2
Lobkowicz	Lobkowicz	k1gMnSc1
<g/>
,	,	kIx,
vladař	vladař	k1gMnSc1
domu	dům	k1gInSc2
lobkowiczkého	lobkowiczký	k2eAgNnSc2d1
a	a	k8xC
5	#num#	k4
<g/>
.	.	kIx.
vévoda	vévoda	k1gMnSc1
roudnický	roudnický	k2eAgMnSc1d1
</s>
<s>
Ve	v	k7c6
funkci	funkce	k1gFnSc6
<g/>
:	:	kIx,
<g/>
22	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1938	#num#	k4
–	–	k?
24	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1953	#num#	k4
(	(	kIx(
<g/>
podle	podle	k7c2
zákona	zákon	k1gInSc2
z	z	k7c2
10	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1918	#num#	k4
české	český	k2eAgFnSc2d1
republikánské	republikánský	k2eAgFnSc2d1
zřízení	zřízení	k1gNnSc6
tyto	tento	k3xDgInPc4
tituly	titul	k1gInPc4
neuznává	uznávat	k5eNaImIp3nS
<g/>
)	)	kIx)
Předchůdce	předchůdce	k1gMnSc1
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1
Zdeněk	Zdeněk	k1gMnSc1
z	z	k7c2
Lobkowicz	Lobkowicz	k1gMnSc1
Nástupce	nástupce	k1gMnSc2
</s>
<s>
Bedřich	Bedřich	k1gMnSc1
z	z	k7c2
Lobkowicz	Lobkowicz	k1gMnSc1
</s>
<s>
C.	C.	kA
k.	k.	k?
komoří	komoří	k1gMnSc1
</s>
<s>
Ve	v	k7c6
funkci	funkce	k1gFnSc6
<g/>
:	:	kIx,
<g/>
1906	#num#	k4
–	–	k?
1918	#num#	k4
Panovník	panovník	k1gMnSc1
</s>
<s>
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
I.	I.	kA
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
I.	I.	kA
</s>
<s>
Narození	narození	k1gNnSc1
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1877	#num#	k4
Konopiště	Konopiště	k1gNnSc1
Rakousko-Uhersko	Rakousko-Uherska	k1gFnSc5
Rakousko-Uhersko	Rakousko-Uherska	k1gFnSc5
Úmrtí	úmrtí	k1gNnPc1
</s>
<s>
24	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1953	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
76	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Křimice	Křimika	k1gFnSc6
Československo	Československo	k1gNnSc4
Československo	Československo	k1gNnSc1
Choť	choť	k1gFnSc1
</s>
<s>
(	(	kIx(
<g/>
1905	#num#	k4
<g/>
)	)	kIx)
Marie	Maria	k1gFnSc2
Terezie	Terezie	k1gFnSc2
z	z	k7c2
Beaufort-Spontinu	Beaufort-Spontin	k1gInSc2
(	(	kIx(
<g/>
1885	#num#	k4
<g/>
–	–	k?
<g/>
1942	#num#	k4
<g/>
)	)	kIx)
Rodiče	rodič	k1gMnPc1
</s>
<s>
Františk	Františk	k1gMnSc1
Evžen	Evžen	k1gMnSc1
z	z	k7c2
Lobkowicz	Lobkowicz	k1gMnSc1
(	(	kIx(
<g/>
1839	#num#	k4
<g/>
–	–	k?
<g/>
1898	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Kunhuta	Kunhuta	k1gFnSc1
ze	z	k7c2
Sternbergu	Sternberg	k1gInSc2
(	(	kIx(
<g/>
1847	#num#	k4
<g/>
–	–	k?
<g/>
1916	#num#	k4
<g/>
)	)	kIx)
Děti	dítě	k1gFnPc1
</s>
<s>
Bedřich	Bedřich	k1gMnSc1
(	(	kIx(
<g/>
1907	#num#	k4
<g/>
–	–	k?
<g/>
1954	#num#	k4
<g/>
)	)	kIx)
Jaroslav	Jaroslav	k1gMnSc1
(	(	kIx(
<g/>
1910	#num#	k4
<g/>
–	–	k?
<g/>
1985	#num#	k4
<g/>
)	)	kIx)
Ladislav	Ladislav	k1gMnSc1
(	(	kIx(
<g/>
1925	#num#	k4
<g/>
–	–	k?
<g/>
1985	#num#	k4
<g/>
)	)	kIx)
Příbuzní	příbuzný	k1gMnPc1
</s>
<s>
vnučka	vnučka	k1gFnSc1
<g/>
:	:	kIx,
Polyxena	Polyxena	k1gFnSc1
<g/>
,	,	kIx,
provd	provd	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Czerninová	Czerninový	k2eAgFnSc1d1
(	(	kIx(
<g/>
*	*	kIx~
1941	#num#	k4
<g/>
)	)	kIx)
vnuk	vnuk	k1gMnSc1
<g/>
:	:	kIx,
Jaroslav	Jaroslav	k1gMnSc1
Lobkowicz	Lobkowicz	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
1942	#num#	k4
<g/>
)	)	kIx)
vnučka	vnučka	k1gFnSc1
<g/>
:	:	kIx,
Marie	Marie	k1gFnSc1
Leopoldina	Leopoldina	k1gFnSc1
<g/>
,	,	kIx,
provd	provd	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sternbergová	Sternbergový	k2eAgFnSc1d1
(	(	kIx(
<g/>
1943	#num#	k4
<g/>
–	–	k?
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
vnuk	vnuk	k1gMnSc1
<g/>
:	:	kIx,
František	František	k1gMnSc1
Lobkowicz	Lobkowicz	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
1948	#num#	k4
<g/>
)	)	kIx)
vnuk	vnuk	k1gMnSc1
<g/>
:	:	kIx,
Filip	Filip	k1gMnSc1
(	(	kIx(
<g/>
Zdeněk	Zdeněk	k1gMnSc1
<g/>
)	)	kIx)
Lobkowicz	Lobkowicz	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
1954	#num#	k4
<g/>
)	)	kIx)
děd	děd	k1gMnSc1
<g/>
:	:	kIx,
Jan	Jan	k1gMnSc1
Nepomuk	Nepomuk	k1gMnSc1
Karel	Karel	k1gMnSc1
z	z	k7c2
Lobkowicz	Lobkowicz	k1gMnSc1
(	(	kIx(
<g/>
1799	#num#	k4
<g/>
–	–	k?
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
1878	#num#	k4
<g/>
)	)	kIx)
babička	babička	k1gFnSc1
<g/>
:	:	kIx,
Karolína	Karolína	k1gFnSc1
Marie	Maria	k1gFnSc2
Bruntálská	bruntálský	k2eAgFnSc1d1
z	z	k7c2
Vrbna	Vrbn	k1gInSc2
(	(	kIx(
<g/>
1815	#num#	k4
<g/>
–	–	k?
<g/>
1843	#num#	k4
<g/>
)	)	kIx)
děd	děd	k1gMnSc1
<g/>
:	:	kIx,
Zdeněk	Zdeněk	k1gMnSc1
ze	z	k7c2
Sternbergu	Sternberg	k1gInSc2
(	(	kIx(
<g/>
1813	#num#	k4
<g/>
–	–	k?
<g/>
1873	#num#	k4
<g/>
)	)	kIx)
babička	babička	k1gFnSc1
<g/>
:	:	kIx,
Marie	Marie	k1gFnSc1
Žofie	Žofie	k1gFnSc2
Terezie	Terezie	k1gFnSc2
ze	z	k7c2
Stadion-Thannhausenu	Stadion-Thannhausen	k2eAgFnSc4d1
(	(	kIx(
<g/>
1819	#num#	k4
<g/>
–	–	k?
<g/>
1873	#num#	k4
<g/>
)	)	kIx)
Sídlo	sídlo	k1gNnSc4
</s>
<s>
Křimice	Křimika	k1gFnSc3
Náboženství	náboženství	k1gNnSc2
</s>
<s>
římskokatolické	římskokatolický	k2eAgNnSc1d1
Ocenění	ocenění	k1gNnSc1
</s>
<s>
důstojník	důstojník	k1gMnSc1
řádu	řád	k1gInSc2
Františka	František	k1gMnSc4
Josefa	Josef	k1gMnSc4
(	(	kIx(
<g/>
1916	#num#	k4
<g/>
)	)	kIx)
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybit	k5eAaPmIp3nS,k5eAaImIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1
Alois	Alois	k1gMnSc1
z	z	k7c2
Lobkowicz	Lobkowicz	k1gMnSc1
<g/>
,	,	kIx,
též	též	k9
z	z	k7c2
Lobkovic	Lobkovice	k1gInPc2
(	(	kIx(
<g/>
26	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1877	#num#	k4
Konopiště	Konopiště	k1gNnSc4
–	–	k?
24	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1953	#num#	k4
Křimice	Křimika	k1gFnSc3
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
Plzeň	Plzeň	k1gFnSc1
5	#num#	k4
<g/>
-Křimice	-Křimika	k1gFnSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
celým	celý	k2eAgNnSc7d1
jménem	jméno	k1gNnSc7
Jaroslav	Jaroslav	k1gMnSc1
Alois	Alois	k1gMnSc1
František	František	k1gMnSc1
Kunigund	Kunigund	k1gMnSc1
Emanuel	Emanuel	k1gMnSc1
Maria	Mario	k1gMnSc2
kníže	kníže	k1gMnSc1
z	z	k7c2
Lobkowicz	Lobkowicz	k1gMnSc1
byl	být	k5eAaImAgMnS
český	český	k2eAgMnSc1d1
šlechtic	šlechtic	k1gMnSc1
z	z	k7c2
(	(	kIx(
<g/>
konopišťsko-	konopišťsko-	k?
<g/>
)	)	kIx)
<g/>
křimické	křimický	k2eAgFnPc1d1
linie	linie	k1gFnPc1
šlechtického	šlechtický	k2eAgInSc2d1
rodu	rod	k1gInSc2
Lobkowiczů	Lobkowicz	k1gMnPc2
a	a	k8xC
majitel	majitel	k1gMnSc1
statku	statek	k1gInSc2
Křimice	Křimika	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1938	#num#	k4
byl	být	k5eAaImAgInS
11	#num#	k4
<g/>
.	.	kIx.
knížetem	kníže	k1gNnSc7wR
z	z	k7c2
Lobkowicz	Lobkowicz	k1gMnSc1
a	a	k8xC
vladařem	vladař	k1gMnSc7
domu	dům	k1gInSc2
lobkowiczkého	lobkowiczký	k2eAgInSc2d1
<g/>
,	,	kIx,
5	#num#	k4
<g/>
.	.	kIx.
vévodou	vévoda	k1gMnSc7
z	z	k7c2
Roudnice	Roudnice	k1gFnSc2
a	a	k8xC
okněžněným	okněžněný	k2eAgMnSc7d1
hrabětem	hrabě	k1gMnSc7
ze	z	k7c2
Sternsteinu	Sternstein	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
signatářem	signatář	k1gMnSc7
Národnostního	národnostní	k2eAgNnSc2d1
prohlášení	prohlášení	k1gNnSc2
české	český	k2eAgFnSc2d1
šlechty	šlechta	k1gFnSc2
ze	z	k7c2
7	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
1939	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Původ	původ	k1gInSc1
a	a	k8xC
kariéra	kariéra	k1gFnSc1
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS
se	se	k3xPyFc4
jako	jako	k9
čtvrtý	čtvrtý	k4xOgMnSc1
syn	syn	k1gMnSc1
a	a	k8xC
páté	pátý	k4xOgNnSc1
dítě	dítě	k1gNnSc1
Františka	František	k1gMnSc2
Evžena	Evžen	k1gMnSc2
z	z	k7c2
Lobkowicz	Lobkowicz	k1gMnSc1
(	(	kIx(
<g/>
1839	#num#	k4
<g/>
–	–	k?
<g/>
1898	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Kunhuty	Kunhuta	k1gFnPc1
ze	z	k7c2
Sternbergu	Sternberg	k1gInSc2
(	(	kIx(
<g/>
1847	#num#	k4
<g/>
–	–	k?
<g/>
1916	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Vystudoval	vystudovat	k5eAaPmAgMnS
práva	právo	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1906	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
c.	c.	k?
k.	k.	k?
komořím	komoří	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
také	také	k9
čestným	čestný	k2eAgMnSc7d1
rytířem	rytíř	k1gMnSc7
Maltézského	maltézský	k2eAgInSc2d1
řádu	řád	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1916	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
důstojníkem	důstojník	k1gMnSc7
řádu	řád	k1gInSc2
Františka	František	k1gMnSc4
Josefa	Josef	k1gMnSc4
s	s	k7c7
válečnou	válečný	k2eAgFnSc7d1
dekorací	dekorace	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Kromě	kromě	k7c2
toho	ten	k3xDgInSc2
byl	být	k5eAaImAgMnS
také	také	k6eAd1
papežským	papežský	k2eAgMnSc7d1
tajným	tajný	k2eAgMnSc7d1
komorníkem	komorník	k1gMnSc7
a	a	k8xC
velkokomturem	velkokomtur	k1gMnSc7
bavorského	bavorský	k2eAgInSc2d1
Řádu	řád	k1gInSc2
sv.	sv.	kA
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
smrti	smrt	k1gFnSc6
Ferdinanda	Ferdinand	k1gMnSc2
Zdeňka	Zdeněk	k1gMnSc2
z	z	k7c2
Lobkowicz	Lobkowicz	k1gMnSc1
(	(	kIx(
<g/>
1858	#num#	k4
<g/>
–	–	k?
<g/>
1938	#num#	k4
<g/>
)	)	kIx)
z	z	k7c2
roudnické	roudnický	k2eAgFnSc2d1
primogenitury	primogenitura	k1gFnSc2
přešel	přejít	k5eAaPmAgInS
na	na	k7c4
Jaroslava	Jaroslav	k1gMnSc4
titul	titul	k1gInSc4
panujícího	panující	k2eAgMnSc2d1
knížete	kníže	k1gMnSc2
z	z	k7c2
Lobkowicz	Lobkowicz	k1gMnSc1
<g/>
,	,	kIx,
protože	protože	k8xS
Ferdinandův	Ferdinandův	k2eAgMnSc1d1
první	první	k4xOgMnSc1
syn	syn	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
Josef	Josef	k1gMnSc1
(	(	kIx(
<g/>
1885	#num#	k4
<g/>
–	–	k?
<g/>
1953	#num#	k4
<g/>
)	)	kIx)
se	se	k3xPyFc4
kvůli	kvůli	k7c3
nerovnému	rovný	k2eNgInSc3d1
sňatku	sňatek	k1gInSc3
musel	muset	k5eAaImAgInS
vzdát	vzdát	k5eAaPmF
nástupnických	nástupnický	k2eAgNnPc2d1
práv	právo	k1gNnPc2
a	a	k8xC
druhý	druhý	k4xOgMnSc1
syn	syn	k1gMnSc1
<g/>
,	,	kIx,
dědic	dědic	k1gMnSc1
Maxmilián	Maxmilián	k1gMnSc1
Ervín	Ervín	k1gMnSc1
(	(	kIx(
<g/>
1888	#num#	k4
<g/>
–	–	k?
<g/>
1967	#num#	k4
<g/>
)	)	kIx)
se	se	k3xPyFc4
oženil	oženit	k5eAaPmAgMnS
s	s	k7c7
rozvedenou	rozvedený	k2eAgFnSc7d1
šlechtičnou	šlechtična	k1gFnSc7
anglikánské	anglikánský	k2eAgFnSc2d1
víry	víra	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k9
oba	dva	k4xCgMnPc1
porušili	porušit	k5eAaPmAgMnP
rodinná	rodinný	k2eAgNnPc4d1
pravidla	pravidlo	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s>
Majetek	majetek	k1gInSc1
</s>
<s>
Zámek	zámek	k1gInSc1
Křimice	Křimika	k1gFnSc3
</s>
<s>
Po	po	k7c6
otci	otec	k1gMnSc6
zdědil	zdědit	k5eAaPmAgInS
zámek	zámek	k1gInSc1
a	a	k8xC
velkostatek	velkostatek	k1gInSc1
Křimice	Křimika	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
první	první	k4xOgFnSc2
republiky	republika	k1gFnSc2
ho	on	k3xPp3gNnSc4
pozemková	pozemkový	k2eAgFnSc1d1
reforma	reforma	k1gFnSc1
připravila	připravit	k5eAaPmAgFnS
o	o	k7c4
téměř	téměř	k6eAd1
30	#num#	k4
%	%	kIx~
jeho	jeho	k3xOp3gFnSc2
rozlohy	rozloha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
německé	německý	k2eAgFnSc2d1
okupace	okupace	k1gFnSc2
fungovala	fungovat	k5eAaImAgFnS
na	na	k7c6
statcích	statek	k1gInPc6
vnucená	vnucený	k2eAgFnSc1d1
správa	správa	k1gFnSc1
(	(	kIx(
<g/>
do	do	k7c2
roku	rok	k1gInSc2
1945	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1948	#num#	k4
mu	on	k3xPp3gMnSc3
byla	být	k5eAaImAgFnS
vyvlastněna	vyvlastněn	k2eAgFnSc1d1
půda	půda	k1gFnSc1
nad	nad	k7c7
5	#num#	k4
hektarů	hektar	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
2	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1950	#num#	k4
byla	být	k5eAaImAgFnS
uvalena	uvalen	k2eAgFnSc1d1
národní	národní	k2eAgFnSc1d1
správa	správa	k1gFnSc1
i	i	k9
na	na	k7c4
ostatní	ostatní	k2eAgInPc4d1
majetek	majetek	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
zámku	zámek	k1gInSc6
směl	smět	k5eAaImAgMnS
dožít	dožít	k5eAaPmF
<g/>
,	,	kIx,
ale	ale	k8xC
už	už	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1951	#num#	k4
byl	být	k5eAaImAgInS
zámek	zámek	k1gInSc1
přidělen	přidělit	k5eAaPmNgInS
Leninovým	Leninův	k2eAgMnPc3d1
závodům	závod	k1gInPc3
v	v	k7c6
Plzni	Plzeň	k1gFnSc6
(	(	kIx(
<g/>
Škoda	škoda	k1gFnSc1
Plzeň	Plzeň	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
rodina	rodina	k1gFnSc1
se	se	k3xPyFc4
musela	muset	k5eAaImAgFnS
přestěhovat	přestěhovat	k5eAaPmF
do	do	k7c2
budovy	budova	k1gFnSc2
zámeckého	zámecký	k2eAgInSc2d1
pivovaru	pivovar	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pohled	pohled	k1gInSc1
na	na	k7c4
Zámeček	zámeček	k1gInSc4
z	z	k7c2
údolí	údolí	k1gNnSc2
řeky	řeka	k1gFnSc2
Mže	Mže	k1gFnSc2
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1908	#num#	k4
si	se	k3xPyFc3
nedaleko	nedaleko	k7c2
Křimic	Křimice	k1gFnPc2
na	na	k7c6
skalním	skalní	k2eAgInSc6d1
masívu	masív	k1gInSc6
Čertova	čertův	k2eAgFnSc1d1
kazatelna	kazatelna	k1gFnSc1
nad	nad	k7c7
údolím	údolí	k1gNnSc7
řeky	řeka	k1gFnSc2
Mže	Mže	k1gFnSc2
nechal	nechat	k5eAaPmAgInS
postavit	postavit	k5eAaPmF
patrovou	patrový	k2eAgFnSc4d1
vilu	vila	k1gFnSc4
na	na	k7c6
téměř	téměř	k6eAd1
čtvercovém	čtvercový	k2eAgInSc6d1
půdoryse	půdorys	k1gInSc6
s	s	k7c7
novogotickými	novogotický	k2eAgInPc7d1
prvky	prvek	k1gInPc7
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
označovala	označovat	k5eAaImAgFnS
jako	jako	k9
Zámeček	zámeček	k1gInSc4
u	u	k7c2
Plzně	Plzeň	k1gFnSc2
(	(	kIx(
<g/>
dnes	dnes	k6eAd1
Pod	pod	k7c7
Vinicemi	vinice	k1gFnPc7
889	#num#	k4
<g/>
/	/	kIx~
<g/>
82	#num#	k4
<g/>
,	,	kIx,
Plzeň	Plzeň	k1gFnSc1
1	#num#	k4
-	-	kIx~
Severní	severní	k2eAgNnPc1d1
předměstí	předměstí	k1gNnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Staviteli	stavitel	k1gMnPc7
byli	být	k5eAaImAgMnP
Josef	Josef	k1gMnSc1
a	a	k8xC
Václav	Václav	k1gMnSc1
Páskovi	pásek	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předlohou	předloha	k1gFnSc7
Zámečku	zámeček	k1gInSc2
s	s	k7c7
válcovými	válcový	k2eAgInPc7d1
nárožními	nárožní	k2eAgInPc7d1
věžicemi	věžicemi	k?
byl	být	k5eAaImAgInS
zámek	zámek	k1gInSc1
Konopiště	Konopiště	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
Jaroslav	Jaroslav	k1gMnSc1
narodil	narodit	k5eAaPmAgMnS
a	a	k8xC
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
jeho	jeho	k3xOp3gMnSc1
otec	otec	k1gMnSc1
v	v	k7c6
roce	rok	k1gInSc6
1887	#num#	k4
prodal	prodat	k5eAaPmAgMnS
Františku	František	k1gMnSc3
Ferdinandovi	Ferdinand	k1gMnSc3
d	d	k?
<g/>
'	'	kIx"
<g/>
Este	Est	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
pohodlném	pohodlný	k2eAgInSc6d1
Zámečku	zámeček	k1gInSc6
přišlo	přijít	k5eAaPmAgNnS
na	na	k7c4
svět	svět	k1gInSc4
několik	několik	k4yIc4
Jaroslavových	Jaroslavův	k2eAgFnPc2d1
dětí	dítě	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rodina	rodina	k1gFnSc1
</s>
<s>
Oženil	oženit	k5eAaPmAgMnS
se	s	k7c7
11	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1905	#num#	k4
v	v	k7c6
Bečově	Bečov	k1gInSc6
nad	nad	k7c7
Teplou	Teplá	k1gFnSc7
s	s	k7c7
Marií	Maria	k1gFnSc7
Terezií	Terezie	k1gFnSc7
z	z	k7c2
Beaufort-Spontinu	Beaufort-Spontin	k1gInSc2
(	(	kIx(
<g/>
6	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1885	#num#	k4
Breuilpont	Breuilpont	k1gInSc1
–	–	k?
22	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1942	#num#	k4
Křimice	Křimika	k1gFnSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dcerou	dcera	k1gFnSc7
Bedřicha	Bedřich	k1gMnSc2
z	z	k7c2
Beaufort-Spontinu	Beaufort-Spontin	k1gInSc2
(	(	kIx(
<g/>
1843	#num#	k4
<g/>
–	–	k?
<g/>
1916	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
jeho	jeho	k3xOp3gFnPc1
manželky	manželka	k1gFnPc1
Marie	Maria	k1gFnSc2
Melanie	Melanie	k1gFnSc2
z	z	k7c2
Ligne	Lign	k1gInSc5
(	(	kIx(
<g/>
1855	#num#	k4
<g/>
–	–	k?
<g/>
1931	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Narodilo	narodit	k5eAaPmAgNnS
se	se	k3xPyFc4
jim	on	k3xPp3gMnPc3
jedenáct	jedenáct	k4xCc1
dětí	dítě	k1gFnPc2
<g/>
,	,	kIx,
ze	z	k7c2
sedmi	sedm	k4xCc2
synů	syn	k1gMnPc2
se	se	k3xPyFc4
jich	on	k3xPp3gMnPc2
šest	šest	k4xCc1
dožilo	dožít	k5eAaPmAgNnS
dospělosti	dospělost	k1gFnSc2
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Marie	Marie	k1gFnSc1
Kunhuta	Kunhuta	k1gFnSc1
(	(	kIx(
<g/>
11	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
1906	#num#	k4
Křimice	Křimika	k1gFnSc6
–	–	k?
11	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
2005	#num#	k4
Woluwe-Saint-Lambert	Woluwe-Saint-Lambert	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
∞	∞	k?
(	(	kIx(
<g/>
13	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
1932	#num#	k4
Křimice	Křimika	k1gFnSc6
<g/>
)	)	kIx)
Karel	Karel	k1gMnSc1
z	z	k7c2
Limburg-Stirum	Limburg-Stirum	k1gNnSc4
(	(	kIx(
<g/>
15	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
1906	#num#	k4
Huldenberg	Huldenberg	k1gInSc1
–	–	k?
14	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
1989	#num#	k4
Uccle	Uccle	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bedřich	Bedřich	k1gMnSc1
(	(	kIx(
<g/>
28	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
1907	#num#	k4
Křimice	Křimika	k1gFnSc6
–	–	k?
25	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
1954	#num#	k4
Pacy-sur-Eure	Pacy-sur-Eur	k1gMnSc5
<g/>
)	)	kIx)
<g/>
,	,	kIx,
12	#num#	k4
<g/>
.	.	kIx.
kníže	kníže	k1gMnSc1
z	z	k7c2
Lobkowicz	Lobkowicz	k1gMnSc1
<g/>
,	,	kIx,
vévoda	vévoda	k1gMnSc1
roudnický	roudnický	k2eAgMnSc1d1
<g/>
,	,	kIx,
emigroval	emigrovat	k5eAaBmAgMnS
do	do	k7c2
Francie	Francie	k1gFnSc2
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Beatrice	Beatrice	k1gFnSc1
(	(	kIx(
<g/>
20	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
1909	#num#	k4
Zámeček	zámeček	k1gInSc1
u	u	k7c2
Plzně	Plzeň	k1gFnSc2
–	–	k?
5	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
1999	#num#	k4
Moravské	moravský	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
<g/>
,	,	kIx,
pohřbena	pohřben	k2eAgNnPc1d1
v	v	k7c6
Křimicích	Křimice	k1gFnPc6
<g/>
)	)	kIx)
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jaroslav	Jaroslav	k1gMnSc1
(	(	kIx(
<g/>
18	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
1910	#num#	k4
Zámeček	zámeček	k1gInSc1
u	u	k7c2
Plzně	Plzeň	k1gFnSc2
–	–	k?
7	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
1985	#num#	k4
Plzeň-Křimice	Plzeň-Křimika	k1gFnSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
13	#num#	k4
<g/>
.	.	kIx.
kníže	kníže	k1gMnSc1
z	z	k7c2
Lobkowicz	Lobkowicz	k1gMnSc1
<g/>
,	,	kIx,
vévoda	vévoda	k1gMnSc1
roudnický	roudnický	k2eAgMnSc1d1
</s>
<s>
∞	∞	k?
(	(	kIx(
<g/>
11	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
1940	#num#	k4
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
Gabriela	Gabriela	k1gFnSc1
z	z	k7c2
Korff-Schmising-Kerssenbrocku	Korff-Schmising-Kerssenbrock	k1gInSc2
(	(	kIx(
<g/>
29	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
1917	#num#	k4
Klatovy	Klatovy	k1gInPc7
–	–	k?
30	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
2008	#num#	k4
Dymokury	Dymokur	k1gInPc4
<g/>
)	)	kIx)
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jan	Jan	k1gMnSc1
(	(	kIx(
<g/>
10	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
1912	#num#	k4
Křimice	Křimika	k1gFnSc6
–	–	k?
10	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
1912	#num#	k4
Křimice	Křimika	k1gFnSc6
<g/>
)	)	kIx)
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Markéta	Markéta	k1gFnSc1
(	(	kIx(
<g/>
4	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
1913	#num#	k4
Zámeček	zámeček	k1gInSc1
u	u	k7c2
Plzně	Plzeň	k1gFnSc2
–	–	k?
13	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
1946	#num#	k4
Sünching	Sünching	k1gInSc1
<g/>
,	,	kIx,
Německo	Německo	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
∞	∞	k?
(	(	kIx(
<g/>
16	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
1934	#num#	k4
Křimice	Křimika	k1gFnSc6
<g/>
)	)	kIx)
Zdenko	Zdenka	k1gFnSc5
z	z	k7c2
Hoenning	Hoenning	k1gInSc1
O	O	kA
<g/>
'	'	kIx"
<g/>
Carroll	Carroll	k1gMnSc1
(	(	kIx(
<g/>
6	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
1906	#num#	k4
Sünching	Sünching	k1gInSc1
–	–	k?
8	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
1996	#num#	k4
Sünching	Sünching	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Eleonora	Eleonora	k1gFnSc1
(	(	kIx(
<g/>
6	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
1916	#num#	k4
Sünching	Sünching	k1gInSc1
–	–	k?
15	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
2000	#num#	k4
Isareck	Isarecko	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
∞	∞	k?
(	(	kIx(
<g/>
církevně	církevně	k6eAd1
20	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
1943	#num#	k4
Křimice	Křimika	k1gFnSc3
<g/>
,	,	kIx,
civilně	civilně	k6eAd1
18	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
1943	#num#	k4
Plzeň	Plzeň	k1gFnSc1
<g/>
)	)	kIx)
František	František	k1gMnSc1
Xaver	Xavera	k1gFnPc2
Basselet	Basselet	k1gInSc4
de	de	k?
la	la	k1gNnSc2
Rosée	Rosée	k1gFnPc2
(	(	kIx(
<g/>
6	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
1906	#num#	k4
Neuburg	Neuburg	k1gMnSc1
an	an	k?
der	drát	k5eAaImRp2nS
Kammel	Kammel	k1gInSc1
–	–	k?
5	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
1984	#num#	k4
Freising	Freising	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Anna	Anna	k1gFnSc1
(	(	kIx(
<g/>
5	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
1917	#num#	k4
Křimice	Křimika	k1gFnSc6
–	–	k?
16	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
1919	#num#	k4
Křimice	Křimika	k1gFnSc6
<g/>
)	)	kIx)
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gabriela	Gabriela	k1gFnSc1
(	(	kIx(
<g/>
8	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
1919	#num#	k4
Křimice	Křimika	k1gFnSc6
–	–	k?
7	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
2010	#num#	k4
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
sestra	sestra	k1gFnSc1
III	III	kA
<g/>
.	.	kIx.
řádu	řád	k1gInSc2
sv.	sv.	kA
Františka	František	k1gMnSc2
<g/>
,	,	kIx,
řádové	řádový	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
Beata	Beat	k1gInSc2
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jan	Jan	k1gMnSc1
Nepomuk	Nepomuk	k1gMnSc1
(	(	kIx(
<g/>
25	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
1920	#num#	k4
Křimice	Křimika	k1gFnSc6
–	–	k?
10	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
2000	#num#	k4
Breuilpont	Breuilponto	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
∞	∞	k?
1	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
civilně	civilně	k6eAd1
Breuilpont	Breuilpont	k1gInSc1
25	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
1949	#num#	k4
<g/>
,	,	kIx,
církevně	církevně	k6eAd1
29	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
1949	#num#	k4
Paříž	Paříž	k1gFnSc1
<g/>
)	)	kIx)
Marie	Marie	k1gFnSc1
Tereza	Tereza	k1gFnSc1
z	z	k7c2
Belcredi	Belcred	k1gMnPc1
(	(	kIx(
<g/>
23	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
1922	#num#	k4
Líšeň	Líšeň	k1gFnSc1
–	–	k?
3	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
1978	#num#	k4
Breuilpont	Breuilponto	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
∞	∞	k?
2	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
19	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
1981	#num#	k4
Breuilpont	Breuilponto	k1gNnPc2
<g/>
)	)	kIx)
Pauline	Paulin	k1gInSc5
d	d	k?
<g/>
'	'	kIx"
<g/>
Ursel	Ursel	k1gMnSc1
(	(	kIx(
<g/>
7	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
1926	#num#	k4
Berlin	berlina	k1gFnPc2
–	–	k?
?	?	kIx.
</s>
<s desamb="1">
<g/>
)	)	kIx)
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Karel	Karel	k1gMnSc1
(	(	kIx(
<g/>
22	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
1922	#num#	k4
Křimice	Křimika	k1gFnSc6
–	–	k?
28	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
1990	#num#	k4
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
pohřben	pohřben	k2eAgInSc1d1
ve	v	k7c6
Štěchovicích	Štěchovice	k1gFnPc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
katolický	katolický	k2eAgMnSc1d1
kněz	kněz	k1gMnSc1
<g/>
,	,	kIx,
farář	farář	k1gMnSc1
ve	v	k7c6
Štěchovicích	Štěchovice	k1gFnPc6
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kristián	Kristián	k1gMnSc1
(	(	kIx(
<g/>
12.6	12.6	k4
<g/>
.1924	.1924	k4
Křimice	Křimika	k1gFnSc6
–	–	k?
2	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
2001	#num#	k4
Louvain	Louvaina	k1gFnPc2
<g/>
,	,	kIx,
Belgie	Belgie	k1gFnSc1
<g/>
,	,	kIx,
pohřben	pohřben	k2eAgInSc1d1
ve	v	k7c6
Štěchovicích	Štěchovice	k1gFnPc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
rytíř	rytíř	k1gMnSc1
Maltézského	maltézský	k2eAgInSc2d1
řádu	řád	k1gInSc2
</s>
<s>
∞	∞	k?
(	(	kIx(
<g/>
26	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
1954	#num#	k4
Kremsmünster	Kremsmünstrum	k1gNnPc2
<g/>
)	)	kIx)
Marie	Maria	k1gFnSc2
Terezie	Terezie	k1gFnSc2
z	z	k7c2
Trauttmansdorff-Weinsbergu	Trauttmansdorff-Weinsberg	k1gInSc2
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
1922	#num#	k4
Praha	Praha	k1gFnSc1
–	–	k?
?	?	kIx.
</s>
<s desamb="1">
<g/>
)	)	kIx)
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ladislav	Ladislav	k1gMnSc1
(	(	kIx(
<g/>
24	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
1925	#num#	k4
Křimice	Křimika	k1gFnSc6
–	–	k?
28	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
1985	#num#	k4
Uccle	Uccle	k1gNnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nabyl	nabýt	k5eAaPmAgInS
belgický	belgický	k2eAgInSc1d1
titul	titul	k1gInSc1
Prince	princ	k1gMnSc2
de	de	k?
Lobkowicz	Lobkowicz	k1gMnSc1
31	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1958	#num#	k4
</s>
<s>
∞	∞	k?
(	(	kIx(
<g/>
28	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
1954	#num#	k4
Malè	Malè	k1gFnPc2
<g/>
,	,	kIx,
Belgie	Belgie	k1gFnSc1
<g/>
)	)	kIx)
Therese	Therese	k1gFnSc1
Cornet	Cornet	k1gInSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
Elzius	Elzius	k1gMnSc1
du	du	k?
Chenoy	Chenoa	k1gFnSc2
(	(	kIx(
<g/>
23	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
1932	#num#	k4
Brusel	Brusel	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Marie	Marie	k1gFnSc1
Melanie	Melanie	k1gFnSc1
(	(	kIx(
<g/>
28	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
1928	#num#	k4
Křimice	Křimika	k1gFnSc6
–	–	k?
?	?	kIx.
</s>
<s desamb="1">
<g/>
)	)	kIx)
</s>
<s>
∞	∞	k?
(	(	kIx(
<g/>
4	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
1961	#num#	k4
Breuilpont	Breuilpont	k1gInSc4
<g/>
)	)	kIx)
Pierre	Pierr	k1gMnSc5
Bazinet	Bazinet	k1gMnSc1
(	(	kIx(
<g/>
13	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
1925	#num#	k4
Angoulè	Angoulè	k1gInSc5
–	–	k?
?	?	kIx.
</s>
<s desamb="1">
<g/>
)	)	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
POUZAR	POUZAR	kA
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
;	;	kIx,
MAŠEK	Mašek	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
;	;	kIx,
MENSDORFF-POUILLY	MENSDORFF-POUILLY	k1gMnSc1
<g/>
,	,	kIx,
Hugo	Hugo	k1gMnSc1
<g/>
;	;	kIx,
POKORNÝ	Pokorný	k1gMnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
R.	R.	kA
Almanach	almanach	k1gInSc1
českých	český	k2eAgInPc2d1
šlechtických	šlechtický	k2eAgInPc2d1
rodů	rod	k1gInPc2
2017	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Brandýs	Brandýs	k1gInSc1
nad	nad	k7c7
Labem	Labe	k1gNnSc7
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Martin	Martin	k1gMnSc1
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
.	.	kIx.
512	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85955	#num#	k4
<g/>
-	-	kIx~
<g/>
43	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
236	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
KASÍK	KASÍK	kA
<g/>
,	,	kIx,
Stanislav	Stanislav	k1gMnSc1
<g/>
;	;	kIx,
MAŠEK	Mašek	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
;	;	kIx,
MŽYKOVÁ	MŽYKOVÁ	kA
<g/>
,	,	kIx,
Marie	Marie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lobkowiczové	Lobkowicz	k1gMnPc1
<g/>
,	,	kIx,
dějiny	dějiny	k1gFnPc1
a	a	k8xC
genealogie	genealogie	k1gFnPc1
rodu	rod	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
<g/>
:	:	kIx,
Bohumír	Bohumír	k1gMnSc1
Němec	Němec	k1gMnSc1
–	–	k?
Veduta	veduta	k1gFnSc1
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
240	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
903040	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
192	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
jen	jen	k9
Lobkowiczové	Lobkowicz	k1gMnPc1
<g/>
.	.	kIx.
↑	↑	k?
Lobkowiczové	Lobkowicz	k1gMnPc1
<g/>
,	,	kIx,
s.	s.	k?
55	#num#	k4
<g/>
,	,	kIx,
192	#num#	k4
<g/>
↑	↑	k?
JUŘÍK	Juřík	k1gMnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lobkowiczové	Lobkowicz	k1gMnPc1
<g/>
:	:	kIx,
Popel	popel	k1gInSc1
jsem	být	k5eAaImIp1nS
a	a	k8xC
popel	popel	k1gInSc1
budu	být	k5eAaImBp1nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Euromedia	Euromedium	k1gNnSc2
Group	Group	k1gInSc1
–	–	k?
Knižní	knižní	k2eAgInSc1d1
klub	klub	k1gInSc1
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
.	.	kIx.
160	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
242	#num#	k4
<g/>
-	-	kIx~
<g/>
5429	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
75	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
jen	jen	k9
Lobkowiczové	Lobkowicz	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Popel	popel	k1gInSc1
jsem	být	k5eAaImIp1nS
a	a	k8xC
popel	popel	k1gInSc1
budu	být	k5eAaImBp1nS
<g/>
.	.	kIx.
↑	↑	k?
Zámeček	zámeček	k1gInSc1
Radčice	Radčice	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Informační	informační	k2eAgInSc1d1
portál	portál	k1gInSc1
obce	obec	k1gFnSc2
Křimice	Křimika	k1gFnSc3
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
MAREK	Marek	k1gMnSc1
<g/>
,	,	kIx,
Miroslav	Miroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rodokmen	rodokmen	k1gInSc4
Lobkowiczů	Lobkowicz	k1gMnPc2
4	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
genealogy	genealog	k1gMnPc7
<g/>
.	.	kIx.
<g/>
euweb	euwba	k1gFnPc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2009-01-30	2009-01-30	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
JUŘÍK	Juřík	k1gMnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lobkowiczové	Lobkowicz	k1gMnPc1
<g/>
:	:	kIx,
Popel	popel	k1gInSc1
jsem	být	k5eAaImIp1nS
a	a	k8xC
popel	popel	k1gInSc1
budu	být	k5eAaImBp1nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Euromedia	Euromedium	k1gNnSc2
Group	Group	k1gInSc1
–	–	k?
Knižní	knižní	k2eAgInSc1d1
klub	klub	k1gInSc1
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
.	.	kIx.
160	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
242	#num#	k4
<g/>
-	-	kIx~
<g/>
5429	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
75	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
KASÍK	KASÍK	kA
<g/>
,	,	kIx,
Stanislav	Stanislav	k1gMnSc1
<g/>
;	;	kIx,
MAŠEK	Mašek	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
;	;	kIx,
MŽYKOVÁ	MŽYKOVÁ	kA
<g/>
,	,	kIx,
Marie	Marie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lobkowiczové	Lobkowicz	k1gMnPc1
<g/>
,	,	kIx,
dějiny	dějiny	k1gFnPc1
a	a	k8xC
genealogie	genealogie	k1gFnPc1
rodu	rod	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
<g/>
:	:	kIx,
Bohumír	Bohumír	k1gMnSc1
Němec	Němec	k1gMnSc1
–	–	k?
Veduta	veduta	k1gFnSc1
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
240	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
903040	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
192	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
zdroje	zdroj	k1gInPc1
</s>
<s>
Rodokmen	rodokmen	k1gInSc1
Lobkowiczů	Lobkowicz	k1gMnPc2
na	na	k7c6
stránkách	stránka	k1gFnPc6
genealogy	genealog	k1gMnPc4
<g/>
.	.	kIx.
<g/>
euweb	euwba	k1gFnPc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
(	(	kIx(
<g/>
Miroslav	Miroslav	k1gMnSc1
Marek	Marek	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Rodokmen	rodokmen	k1gInSc1
s	s	k7c7
erby	erb	k1gInPc7
na	na	k7c6
stránkách	stránka	k1gFnPc6
www.roskildehistorie.dk	www.roskildehistorie.dk	k6eAd1
</s>
<s>
Rodokmen	rodokmen	k1gInSc1
Lobkowiczů	Lobkowicz	k1gMnPc2
jako	jako	k8xS,k8xC
potomků	potomek	k1gMnPc2
Karla	Karel	k1gMnSc2
IV	IV	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Jan	Jan	k1gMnSc1
Drocár	Drocár	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Česko	Česko	k1gNnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
</s>
