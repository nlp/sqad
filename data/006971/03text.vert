<s>
Mstitel	mstitel	k1gMnSc1	mstitel
je	být	k5eAaImIp3nS	být
české	český	k2eAgNnSc4d1	české
filmové	filmový	k2eAgNnSc4d1	filmové
drama	drama	k1gNnSc4	drama
režiséra	režisér	k1gMnSc2	režisér
Karla	Karel	k1gMnSc2	Karel
Steklého	Steklý	k1gMnSc2	Steklý
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1959	[number]	k4	1959
<g/>
.	.	kIx.	.
</s>
<s>
Námět	námět	k1gInSc1	námět
<g/>
:	:	kIx,	:
Karel	Karel	k1gMnSc1	Karel
Matěj	Matěj	k1gMnSc1	Matěj
Čapek-Chod	Čapek-Choda	k1gFnPc2	Čapek-Choda
román	román	k1gInSc1	román
Kašpar	Kašpar	k1gMnSc1	Kašpar
Lén	Léna	k1gFnPc2	Léna
mstitel	mstitel	k1gMnSc1	mstitel
Scénář	scénář	k1gInSc1	scénář
<g/>
:	:	kIx,	:
Karel	Karel	k1gMnSc1	Karel
Steklý	Steklý	k1gMnSc1	Steklý
Hudba	hudba	k1gFnSc1	hudba
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Seidel	Seidel	k1gMnSc1	Seidel
Zvuk	zvuk	k1gInSc1	zvuk
<g/>
:	:	kIx,	:
Ladislav	Ladislav	k1gMnSc1	Ladislav
Hausdorf	Hausdorf	k1gMnSc1	Hausdorf
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Svoboda	Svoboda	k1gMnSc1	Svoboda
Kamera	kamera	k1gFnSc1	kamera
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Stallich	Stallich	k1gMnSc1	Stallich
Střih	střih	k1gInSc1	střih
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Kohout	Kohout	k1gMnSc1	Kohout
<g/>
,	,	kIx,	,
Jarmila	Jarmila	k1gFnSc1	Jarmila
Müllerová	Müllerová	k1gFnSc1	Müllerová
Režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Karel	Karel	k1gMnSc1	Karel
Steklý	Steklý	k1gMnSc1	Steklý
Pomocná	pomocný	k2eAgFnSc1d1	pomocná
režie	režie	k1gFnSc2	režie
<g/>
:	:	kIx,	:
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Pour	Pour	k1gMnSc1	Pour
Mstitel	mstitel	k1gMnSc1	mstitel
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
http://www.kinobox.cz/cfn/film/13005-mstitel	[url]	k1gMnSc1	http://www.kinobox.cz/cfn/film/13005-mstitel
</s>
