<p>
<s>
Baxtrix	Baxtrix	k1gInSc1	Baxtrix
<g/>
,	,	kIx,	,
občanským	občanský	k2eAgNnSc7d1	občanské
jménem	jméno	k1gNnSc7	jméno
Štěpán	Štěpán	k1gMnSc1	Štěpán
Buchta	Buchta	k1gMnSc1	Buchta
(	(	kIx(	(
<g/>
*	*	kIx~	*
28	[number]	k4	28
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
český	český	k2eAgInSc1d1	český
youtuber	youtuber	k1gInSc1	youtuber
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vyrůstal	vyrůstat	k5eAaImAgInS	vyrůstat
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
obsah	obsah	k1gInSc4	obsah
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
a	a	k8xC	a
angličtině	angličtina	k1gFnSc6	angličtina
<g/>
.	.	kIx.	.
</s>
<s>
Zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
se	se	k3xPyFc4	se
na	na	k7c4	na
videa	video	k1gNnPc4	video
ze	z	k7c2	z
svého	svůj	k3xOyFgNnSc2	svůj
hraní	hraní	k1gNnSc2	hraní
her	hra	k1gFnPc2	hra
(	(	kIx(	(
<g/>
let	léto	k1gNnPc2	léto
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
play	play	k0	play
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
hráč	hráč	k1gMnSc1	hráč
počítačových	počítačový	k2eAgFnPc2d1	počítačová
her	hra	k1gFnPc2	hra
je	být	k5eAaImIp3nS	být
mistrem	mistr	k1gMnSc7	mistr
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
League	League	k1gFnSc6	League
of	of	k?	of
Legends	Legends	k1gInSc4	Legends
2013	[number]	k4	2013
a	a	k8xC	a
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Titul	titul	k1gInSc1	titul
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
získal	získat	k5eAaPmAgMnS	získat
v	v	k7c6	v
týmu	tým	k1gInSc6	tým
eSuba	eSub	k1gMnSc2	eSub
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
členem	člen	k1gMnSc7	člen
byl	být	k5eAaImAgMnS	být
s	s	k7c7	s
přestávkou	přestávka	k1gFnSc7	přestávka
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
<g/>
Účastní	účastnit	k5eAaImIp3nS	účastnit
se	se	k3xPyFc4	se
také	také	k9	také
různých	různý	k2eAgFnPc2d1	různá
herních	herní	k2eAgFnPc2d1	herní
soutěží	soutěž	k1gFnPc2	soutěž
či	či	k8xC	či
youtuberských	youtuberský	k2eAgInPc2d1	youtuberský
srazů	sraz	k1gInPc2	sraz
a	a	k8xC	a
festivalů	festival	k1gInPc2	festival
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2013	[number]	k4	2013
i	i	k9	i
2014	[number]	k4	2014
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
vítězném	vítězný	k2eAgInSc6d1	vítězný
týmu	tým	k1gInSc6	tým
na	na	k7c6	na
oficiálním	oficiální	k2eAgNnSc6d1	oficiální
mistrovství	mistrovství	k1gNnSc6	mistrovství
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
hraní	hraní	k1gNnSc6	hraní
počítačových	počítačový	k2eAgFnPc2d1	počítačová
her	hra	k1gFnPc2	hra
(	(	kIx(	(
<g/>
konkrétně	konkrétně	k6eAd1	konkrétně
v	v	k7c6	v
League	League	k1gFnSc6	League
of	of	k?	of
Legends	Legendsa	k1gFnPc2	Legendsa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
festivalu	festival	k1gInSc2	festival
ForGames	ForGames	k1gMnSc1	ForGames
na	na	k7c6	na
pražském	pražský	k2eAgNnSc6d1	Pražské
výstavišti	výstaviště	k1gNnSc6	výstaviště
v	v	k7c6	v
Letňanech	Letňan	k1gMnPc6	Letňan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2018	[number]	k4	2018
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
festivalu	festival	k1gInSc6	festival
Utubering	Utubering	k1gInSc1	Utubering
na	na	k7c6	na
brněnském	brněnský	k2eAgNnSc6d1	brněnské
výstavišti	výstaviště	k1gNnSc6	výstaviště
<g/>
.	.	kIx.	.
<g/>
Časopis	časopis	k1gInSc1	časopis
Forbes	forbes	k1gInSc1	forbes
jej	on	k3xPp3gNnSc4	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
analytiky	analytik	k1gMnPc7	analytik
ze	z	k7c2	z
startupu	startup	k1gInSc2	startup
Socialbakers	Socialbakers	k1gInSc1	Socialbakers
umístil	umístit	k5eAaPmAgMnS	umístit
na	na	k7c6	na
13	[number]	k4	13
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
českých	český	k2eAgMnPc2d1	český
youtuberů	youtuber	k1gMnPc2	youtuber
a	a	k8xC	a
na	na	k7c4	na
36	[number]	k4	36
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
celkovém	celkový	k2eAgInSc6d1	celkový
žebříčku	žebříček	k1gInSc6	žebříček
nejvlivnějších	vlivný	k2eAgMnPc2d3	nejvlivnější
Čechů	Čech	k1gMnPc2	Čech
na	na	k7c6	na
sociálních	sociální	k2eAgFnPc6d1	sociální
sítích	síť	k1gFnPc6	síť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Poznámky	poznámka	k1gFnSc2	poznámka
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Baxtrix	Baxtrix	k1gInSc1	Baxtrix
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Baxtrix	Baxtrix	k1gInSc1	Baxtrix
na	na	k7c4	na
YouTube	YouTub	k1gInSc5	YouTub
</s>
</p>
<p>
<s>
Baxtrix	Baxtrix	k1gInSc1	Baxtrix
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
na	na	k7c6	na
YouTube	YouTub	k1gInSc5	YouTub
</s>
</p>
<p>
<s>
Baxtrix	Baxtrix	k1gInSc1	Baxtrix
na	na	k7c6	na
Facebooku	Facebook	k1gInSc6	Facebook
</s>
</p>
<p>
<s>
Baxtrix	Baxtrix	k1gInSc1	Baxtrix
na	na	k7c6	na
Instagramu	Instagram	k1gInSc6	Instagram
</s>
</p>
<p>
<s>
Baxtrix	Baxtrix	k1gInSc1	Baxtrix
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
