<s>
DAM	dáma	k1gFnPc2	dáma
architekti	architekt	k1gMnPc1	architekt
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
(	(	kIx(	(
<g/>
do	do	k7c2	do
17	[number]	k4	17
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2015	[number]	k4	2015
DaM	dáma	k1gFnPc2	dáma
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
s	s	k7c7	s
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
architektonická	architektonický	k2eAgFnSc1d1	architektonická
kancelář	kancelář	k1gFnSc1	kancelář
sídlící	sídlící	k2eAgFnSc1d1	sídlící
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
Richardem	Richard	k1gMnSc7	Richard
Doležalem	Doležal	k1gMnSc7	Doležal
a	a	k8xC	a
Petrem	Petr	k1gMnSc7	Petr
Malinským	Malinský	k2eAgMnSc7d1	Malinský
<g/>
.	.	kIx.	.
</s>
