<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
česká	český	k2eAgFnSc1d1	Česká
architektonická	architektonický	k2eAgFnSc1d1	architektonická
kancelář	kancelář	k1gFnSc1	kancelář
sídlící	sídlící	k2eAgFnSc1d1	sídlící
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
Richardem	Richard	k1gMnSc7	Richard
Doležalem	Doležal	k1gMnSc7	Doležal
a	a	k8xC	a
Petrem	Petr	k1gMnSc7	Petr
Malinským	Malinský	k2eAgMnSc7d1	Malinský
<g/>
?	?	kIx.	?
</s>
