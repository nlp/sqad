<p>
<s>
DAM	dáma	k1gFnPc2	dáma
architekti	architekt	k1gMnPc1	architekt
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
(	(	kIx(	(
<g/>
do	do	k7c2	do
17	[number]	k4	17
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2015	[number]	k4	2015
DaM	dáma	k1gFnPc2	dáma
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
s	s	k7c7	s
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
architektonická	architektonický	k2eAgFnSc1d1	architektonická
kancelář	kancelář	k1gFnSc1	kancelář
sídlící	sídlící	k2eAgFnSc1d1	sídlící
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
Richardem	Richard	k1gMnSc7	Richard
Doležalem	Doležal	k1gMnSc7	Doležal
a	a	k8xC	a
Petrem	Petr	k1gMnSc7	Petr
Malinským	Malinský	k2eAgMnSc7d1	Malinský
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgMnPc7d1	další
společníky	společník	k1gMnPc7	společník
jsou	být	k5eAaImIp3nP	být
Petr	Petr	k1gMnSc1	Petr
Burian	Burian	k1gMnSc1	Burian
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Havrda	Havrda	k1gMnSc1	Havrda
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Hejda	Hejda	k1gMnSc1	Hejda
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
Holna	Holn	k1gInSc2	Holn
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
ateliér	ateliér	k1gInSc4	ateliér
s	s	k7c7	s
řadou	řada	k1gFnSc7	řada
realizací	realizace	k1gFnPc2	realizace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výběr	výběr	k1gInSc1	výběr
z	z	k7c2	z
realizací	realizace	k1gFnPc2	realizace
==	==	k?	==
</s>
</p>
<p>
<s>
1992	[number]	k4	1992
–	–	k?	–
Produkční	produkční	k2eAgFnSc2d1	produkční
a	a	k8xC	a
správní	správní	k2eAgFnSc2d1	správní
budovy	budova	k1gFnSc2	budova
firmy	firma	k1gFnSc2	firma
Rolex	Rolex	k1gInSc1	Rolex
ve	v	k7c6	v
švýcarském	švýcarský	k2eAgNnSc6d1	švýcarské
Bielu	Bielo	k1gNnSc6	Bielo
</s>
</p>
<p>
<s>
1993	[number]	k4	1993
–	–	k?	–
Lesnická	lesnický	k2eAgFnSc1d1	lesnická
škola	škola	k1gFnSc1	škola
ve	v	k7c6	v
švýcarském	švýcarský	k2eAgInSc6d1	švýcarský
Lyssu	Lyss	k1gInSc6	Lyss
</s>
</p>
<p>
<s>
2002	[number]	k4	2002
–	–	k?	–
Palác	palác	k1gInSc1	palác
Euro	euro	k1gNnSc1	euro
na	na	k7c6	na
Václavském	václavský	k2eAgNnSc6d1	Václavské
náměstí	náměstí	k1gNnSc6	náměstí
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
</s>
</p>
<p>
<s>
2004	[number]	k4	2004
–	–	k?	–
Ústav	ústav	k1gInSc1	ústav
šlechtičen	šlechtična	k1gFnPc2	šlechtična
na	na	k7c6	na
Pražském	pražský	k2eAgInSc6d1	pražský
hradě	hrad	k1gInSc6	hrad
(	(	kIx(	(
<g/>
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2010	[number]	k4	2010
–	–	k?	–
Filadelfie	Filadelfie	k1gFnSc1	Filadelfie
<g/>
,	,	kIx,	,
výšková	výškový	k2eAgFnSc1d1	výšková
kancelářská	kancelářský	k2eAgFnSc1d1	kancelářská
budova	budova	k1gFnSc1	budova
v	v	k7c4	v
business	business	k1gInSc4	business
centru	centr	k1gInSc2	centr
BB	BB	kA	BB
Centrum	centrum	k1gNnSc1	centrum
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
</s>
</p>
<p>
<s>
2011	[number]	k4	2011
–	–	k?	–
Bytový	bytový	k2eAgInSc4d1	bytový
dům	dům	k1gInSc4	dům
s	s	k7c7	s
tělocvičnou	tělocvična	k1gFnSc7	tělocvična
na	na	k7c6	na
pražském	pražský	k2eAgNnSc6d1	Pražské
Petrském	petrský	k2eAgNnSc6d1	Petrské
náměstí	náměstí	k1gNnSc6	náměstí
(	(	kIx(	(
<g/>
oceněn	oceněn	k2eAgMnSc1d1	oceněn
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc1	Prix
architektů	architekt	k1gMnPc2	architekt
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2012	[number]	k4	2012
–	–	k?	–
Main	Maina	k1gFnPc2	Maina
Point	pointa	k1gFnPc2	pointa
Karlin	Karlin	k2eAgInSc1d1	Karlin
–	–	k?	–
ocenění	ocenění	k1gNnPc2	ocenění
"	"	kIx"	"
<g/>
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
kancelářská	kancelářský	k2eAgFnSc1d1	kancelářská
budova	budova	k1gFnSc1	budova
světa	svět	k1gInSc2	svět
<g/>
"	"	kIx"	"
na	na	k7c6	na
mezinárodním	mezinárodní	k2eAgInSc6d1	mezinárodní
veletrhu	veletrh	k1gInSc6	veletrh
realit	realita	k1gFnPc2	realita
MIPIM	MIPIM	kA	MIPIM
v	v	k7c6	v
Cannes	Cannes	k1gNnSc6	Cannes
<g/>
,	,	kIx,	,
první	první	k4xOgInSc4	první
certifikát	certifikát	k1gInSc4	certifikát
LEED	LEED	kA	LEED
Platinum	Platinum	k1gInSc1	Platinum
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
</s>
</p>
<p>
<s>
2018	[number]	k4	2018
–	–	k?	–
Main	Main	k1gInSc1	Main
Point	pointa	k1gFnPc2	pointa
Pankrác	Pankrác	k1gMnSc1	Pankrác
<g/>
,	,	kIx,	,
kancelářská	kancelářský	k2eAgFnSc1d1	kancelářská
budova	budova	k1gFnSc1	budova
na	na	k7c6	na
Pankráci	Pankrác	k1gMnSc6	Pankrác
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
</s>
</p>
<p>
<s>
2019	[number]	k4	2019
–	–	k?	–
AFI	AFI	kA	AFI
Vokovice	Vokovice	k1gFnSc2	Vokovice
<g/>
,	,	kIx,	,
dvě	dva	k4xCgFnPc1	dva
kancelářské	kancelářská	k1gFnPc1	kancelářská
budovy	budova	k1gFnSc2	budova
ve	v	k7c6	v
Vokovicích	Vokovice	k1gFnPc6	Vokovice
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
</s>
</p>
<p>
<s>
==	==	k?	==
Kritika	kritika	k1gFnSc1	kritika
==	==	k?	==
</s>
</p>
<p>
<s>
Ateliér	ateliér	k1gInSc4	ateliér
je	být	k5eAaImIp3nS	být
kritizován	kritizovat	k5eAaImNgMnS	kritizovat
za	za	k7c4	za
možný	možný	k2eAgInSc4d1	možný
střet	střet	k1gInSc4	střet
zájmů	zájem	k1gInPc2	zájem
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
jeho	jeho	k3xOp3gFnPc2	jeho
realizací	realizace	k1gFnPc2	realizace
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
vyžadováno	vyžadován	k2eAgNnSc1d1	vyžadováno
schválení	schválení	k1gNnSc1	schválení
Národním	národní	k2eAgInSc7d1	národní
památkovým	památkový	k2eAgInSc7d1	památkový
ústavem	ústav	k1gInSc7	ústav
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
je	být	k5eAaImIp3nS	být
však	však	k9	však
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
ústavu	ústav	k1gInSc2	ústav
často	často	k6eAd1	často
změněno	změnit	k5eAaPmNgNnS	změnit
stanoviskem	stanovisko	k1gNnSc7	stanovisko
Sboru	sbor	k1gInSc2	sbor
expertů	expert	k1gMnPc2	expert
pro	pro	k7c4	pro
památkovou	památkový	k2eAgFnSc4d1	památková
péči	péče	k1gFnSc4	péče
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
poradní	poradní	k2eAgInSc1d1	poradní
orgán	orgán	k1gInSc1	orgán
památkového	památkový	k2eAgInSc2d1	památkový
odboru	odbor	k1gInSc2	odbor
pražského	pražský	k2eAgInSc2d1	pražský
magistrátu	magistrát	k1gInSc2	magistrát
<g/>
.	.	kIx.	.
</s>
<s>
Sboru	sbor	k1gInSc3	sbor
expertů	expert	k1gMnPc2	expert
však	však	k9	však
předsedá	předsedat	k5eAaImIp3nS	předsedat
Petr	Petr	k1gMnSc1	Petr
Malinský	Malinský	k2eAgMnSc1d1	Malinský
z	z	k7c2	z
DaM	dáma	k1gFnPc2	dáma
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
soutěž	soutěž	k1gFnSc4	soutěž
na	na	k7c4	na
úpravu	úprava	k1gFnSc4	úprava
Mariánského	mariánský	k2eAgNnSc2d1	Mariánské
náměstí	náměstí	k1gNnSc2	náměstí
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
jeho	jeho	k3xOp3gInSc1	jeho
tým	tým	k1gInSc1	tým
(	(	kIx(	(
<g/>
po	po	k7c6	po
schválení	schválení	k1gNnSc6	schválení
návrhu	návrh	k1gInSc2	návrh
Sborem	sborem	k6eAd1	sborem
expertů	expert	k1gMnPc2	expert
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Petr	Petr	k1gMnSc1	Petr
Malinský	Malinský	k2eAgMnSc1d1	Malinský
i	i	k8xC	i
odsouzen	odsoudit	k5eAaPmNgInS	odsoudit
Stavovským	stavovský	k2eAgInSc7d1	stavovský
soudem	soud	k1gInSc7	soud
komory	komora	k1gFnSc2	komora
architektů	architekt	k1gMnPc2	architekt
k	k	k7c3	k
pokutě	pokuta	k1gFnSc3	pokuta
25	[number]	k4	25
tisíc	tisíc	k4xCgInPc2	tisíc
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
www.dam.cz	www.dam.cz	k1gInSc1	www.dam.cz
–	–	k?	–
oficiální	oficiální	k2eAgFnPc4d1	oficiální
stránky	stránka	k1gFnPc4	stránka
</s>
</p>
