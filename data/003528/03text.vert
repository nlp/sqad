<s>
Evropa	Evropa	k1gFnSc1	Evropa
je	být	k5eAaImIp3nS	být
území	území	k1gNnSc4	území
brané	braný	k2eAgNnSc4d1	brané
buďto	buďto	k8xC	buďto
jako	jako	k9	jako
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
šesti	šest	k4xCc2	šest
světadílů	světadíl	k1gInPc2	světadíl
v	v	k7c6	v
jejich	jejich	k3xOp3gNnPc6	jejich
tradičních	tradiční	k2eAgNnPc6d1	tradiční
pojetích	pojetí	k1gNnPc6	pojetí
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
západní	západní	k2eAgFnSc4d1	západní
část	část	k1gFnSc4	část
Eurasie	Eurasie	k1gFnSc2	Eurasie
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
severu	sever	k1gInSc2	sever
jej	on	k3xPp3gMnSc4	on
ohraničuje	ohraničovat	k5eAaImIp3nS	ohraničovat
Severní	severní	k2eAgInSc1d1	severní
ledový	ledový	k2eAgInSc1d1	ledový
oceán	oceán	k1gInSc1	oceán
<g/>
,	,	kIx,	,
ze	z	k7c2	z
západu	západ	k1gInSc2	západ
Atlantský	atlantský	k2eAgInSc1d1	atlantský
oceán	oceán	k1gInSc1	oceán
<g/>
,	,	kIx,	,
z	z	k7c2	z
jihu	jih	k1gInSc2	jih
Středozemní	středozemní	k2eAgInPc4d1	středozemní
a	a	k8xC	a
Černé	Černé	k2eAgNnSc4d1	Černé
moře	moře	k1gNnSc4	moře
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
vodními	vodní	k2eAgFnPc7d1	vodní
cestami	cesta	k1gFnPc7	cesta
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
je	on	k3xPp3gMnPc4	on
spojují	spojovat	k5eAaImIp3nP	spojovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
z	z	k7c2	z
východu	východ	k1gInSc2	východ
Asie	Asie	k1gFnSc2	Asie
(	(	kIx(	(
<g/>
o	o	k7c6	o
přesném	přesný	k2eAgInSc6d1	přesný
průběhu	průběh	k1gInSc6	průběh
této	tento	k3xDgFnSc2	tento
části	část	k1gFnSc2	část
hranice	hranice	k1gFnSc2	hranice
nepanuje	panovat	k5eNaImIp3nS	panovat
konsenzus	konsenzus	k1gInSc1	konsenzus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
druhý	druhý	k4xOgInSc4	druhý
nejmenší	malý	k2eAgInSc4d3	nejmenší
světadíl	světadíl	k1gInSc4	světadíl
mající	mající	k2eAgFnSc4d1	mající
rozlohu	rozloha	k1gFnSc4	rozloha
asi	asi	k9	asi
10	[number]	k4	10
058	[number]	k4	058
912	[number]	k4	912
km2	km2	k4	km2
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
asi	asi	k9	asi
7	[number]	k4	7
%	%	kIx~	%
zemského	zemský	k2eAgInSc2d1	zemský
povrchu	povrch	k1gInSc2	povrch
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
však	však	k9	však
zároveň	zároveň	k6eAd1	zároveň
druhý	druhý	k4xOgMnSc1	druhý
nejhustěji	husto	k6eAd3	husto
zalidněný	zalidněný	k2eAgMnSc1d1	zalidněný
(	(	kIx(	(
<g/>
asi	asi	k9	asi
72	[number]	k4	72
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
/	/	kIx~	/
<g/>
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
asi	asi	k9	asi
742	[number]	k4	742
500	[number]	k4	500
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
Evropy	Evropa	k1gFnSc2	Evropa
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
představuje	představovat	k5eAaImIp3nS	představovat
přibližně	přibližně	k6eAd1	přibližně
9,6	[number]	k4	9,6
<g/>
%	%	kIx~	%
podíl	podíl	k1gInSc1	podíl
na	na	k7c6	na
světové	světový	k2eAgFnSc6d1	světová
populaci	populace	k1gFnSc6	populace
(	(	kIx(	(
<g/>
údaje	údaj	k1gInPc1	údaj
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Evropa	Evropa	k1gFnSc1	Evropa
je	být	k5eAaImIp3nS	být
kolébkou	kolébka	k1gFnSc7	kolébka
tzv.	tzv.	kA	tzv.
západní	západní	k2eAgFnSc2d1	západní
civilizace	civilizace	k1gFnSc2	civilizace
<g/>
.	.	kIx.	.
</s>
<s>
Evropské	evropský	k2eAgInPc1d1	evropský
národy	národ	k1gInPc1	národ
hrály	hrát	k5eAaImAgInP	hrát
dominantní	dominantní	k2eAgFnSc4d1	dominantní
roli	role	k1gFnSc4	role
ve	v	k7c6	v
světovém	světový	k2eAgNnSc6d1	světové
dění	dění	k1gNnSc6	dění
cca	cca	kA	cca
od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
až	až	k9	až
po	po	k7c4	po
počátek	počátek	k1gInSc4	počátek
století	století	k1gNnSc2	století
20	[number]	k4	20
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
byly	být	k5eAaImAgInP	být
zatlačeny	zatlačit	k5eAaPmNgInP	zatlačit
do	do	k7c2	do
pozadí	pozadí	k1gNnSc2	pozadí
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
americkými	americký	k2eAgInPc7d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
byla	být	k5eAaImAgFnS	být
zejména	zejména	k9	zejména
politická	politický	k2eAgFnSc1d1	politická
a	a	k8xC	a
národnostní	národnostní	k2eAgFnSc1d1	národnostní
roztříštěnost	roztříštěnost	k1gFnSc1	roztříštěnost
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejímž	jejíž	k3xOyRp3gNnSc6	jejíž
území	území	k1gNnSc6	území
se	se	k3xPyFc4	se
odehrály	odehrát	k5eAaPmAgInP	odehrát
hlavní	hlavní	k2eAgInPc1d1	hlavní
boje	boj	k1gInPc1	boj
obou	dva	k4xCgFnPc2	dva
světových	světový	k2eAgFnPc2d1	světová
válek	válka	k1gFnPc2	válka
<g/>
,	,	kIx,	,
a	a	k8xC	a
kterou	který	k3yQgFnSc4	který
po	po	k7c6	po
té	ten	k3xDgFnSc6	ten
druhé	druhý	k4xOgFnSc6	druhý
na	na	k7c4	na
čtyřicet	čtyřicet	k4xCc4	čtyřicet
let	léto	k1gNnPc2	léto
rozdělila	rozdělit	k5eAaPmAgFnS	rozdělit
železná	železný	k2eAgFnSc1d1	železná
opona	opona	k1gFnSc1	opona
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
přijetí	přijetí	k1gNnSc4	přijetí
amerického	americký	k2eAgInSc2d1	americký
dolaru	dolar	k1gInSc2	dolar
za	za	k7c4	za
základní	základní	k2eAgNnSc4d1	základní
mezinárodní	mezinárodní	k2eAgNnSc4d1	mezinárodní
platidlo	platidlo	k1gNnSc4	platidlo
(	(	kIx(	(
<g/>
tvorba	tvorba	k1gFnSc1	tvorba
měnových	měnový	k2eAgFnPc2d1	měnová
rezerv	rezerva	k1gFnPc2	rezerva
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
států	stát	k1gInPc2	stát
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
politická	politický	k2eAgFnSc1d1	politická
a	a	k8xC	a
jazyková	jazykový	k2eAgFnSc1d1	jazyková
roztříštěnost	roztříštěnost	k1gFnSc1	roztříštěnost
komplikují	komplikovat	k5eAaBmIp3nP	komplikovat
integraci	integrace	k1gFnSc4	integrace
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
u	u	k7c2	u
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Mluví	mluvit	k5eAaImIp3nS	mluvit
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
218	[number]	k4	218
různými	různý	k2eAgInPc7d1	různý
jazyky	jazyk	k1gInPc7	jazyk
z	z	k7c2	z
nejméně	málo	k6eAd3	málo
6	[number]	k4	6
jazykových	jazykový	k2eAgFnPc2d1	jazyková
rodin	rodina	k1gFnPc2	rodina
(	(	kIx(	(
<g/>
indoevropská	indoevropský	k2eAgFnSc1d1	indoevropská
<g/>
,	,	kIx,	,
uralská	uralský	k2eAgFnSc1d1	Uralská
<g/>
,	,	kIx,	,
altajská	altajský	k2eAgFnSc1d1	Altajská
<g/>
,	,	kIx,	,
afroasijská	afroasijský	k2eAgFnSc1d1	afroasijský
<g/>
,	,	kIx,	,
severokavkazská	severokavkazský	k2eAgFnSc1d1	severokavkazská
a	a	k8xC	a
baskičtina	baskičtina	k1gFnSc1	baskičtina
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
naprostá	naprostý	k2eAgFnSc1d1	naprostá
většina	většina	k1gFnSc1	většina
mluvčích	mluvčí	k1gMnPc2	mluvčí
však	však	k9	však
připadá	připadat	k5eAaPmIp3nS	připadat
na	na	k7c4	na
jazyky	jazyk	k1gInPc4	jazyk
indoevropské	indoevropský	k2eAgInPc4d1	indoevropský
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
státy	stát	k1gInPc1	stát
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
založeny	založit	k5eAaPmNgFnP	založit
na	na	k7c6	na
národních	národní	k2eAgInPc6d1	národní
základech	základ	k1gInPc6	základ
a	a	k8xC	a
existují	existovat	k5eAaImIp3nP	existovat
zde	zde	k6eAd1	zde
oblasti	oblast	k1gFnPc4	oblast
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
jsou	být	k5eAaImIp3nP	být
zdrojem	zdroj	k1gInSc7	zdroj
závažných	závažný	k2eAgInPc2d1	závažný
národnostních	národnostní	k2eAgInPc2d1	národnostní
konfliktů	konflikt	k1gInPc2	konflikt
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
Balkán	Balkán	k1gInSc1	Balkán
<g/>
,	,	kIx,	,
země	zem	k1gFnPc1	zem
bývalého	bývalý	k2eAgInSc2d1	bývalý
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
,	,	kIx,	,
severní	severní	k2eAgNnSc1d1	severní
Španělsko	Španělsko	k1gNnSc1	Španělsko
a	a	k8xC	a
Severní	severní	k2eAgNnSc1d1	severní
Irsko	Irsko	k1gNnSc1	Irsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Geografie	geografie	k1gFnSc2	geografie
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Evropa	Evropa	k1gFnSc1	Evropa
se	se	k3xPyFc4	se
za	za	k7c4	za
samostatný	samostatný	k2eAgInSc4d1	samostatný
světadíl	světadíl	k1gInSc4	světadíl
považuje	považovat	k5eAaImIp3nS	považovat
spíše	spíše	k9	spíše
z	z	k7c2	z
historických	historický	k2eAgInPc2d1	historický
<g/>
,	,	kIx,	,
kulturních	kulturní	k2eAgInPc2d1	kulturní
a	a	k8xC	a
politických	politický	k2eAgInPc2d1	politický
důvodů	důvod	k1gInPc2	důvod
než	než	k8xS	než
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
fyzickogeografických	fyzickogeografický	k2eAgInPc2d1	fyzickogeografický
<g/>
.	.	kIx.	.
</s>
<s>
Hranice	hranice	k1gFnSc1	hranice
mezi	mezi	k7c7	mezi
Evropou	Evropa	k1gFnSc7	Evropa
a	a	k8xC	a
Asií	Asie	k1gFnSc7	Asie
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
zjevná	zjevný	k2eAgFnSc1d1	zjevná
a	a	k8xC	a
zeměpisci	zeměpisec	k1gMnPc1	zeměpisec
se	se	k3xPyFc4	se
odedávna	odedávna	k6eAd1	odedávna
přou	přít	k5eAaImIp3nP	přít
<g/>
,	,	kIx,	,
kudy	kudy	k6eAd1	kudy
má	mít	k5eAaImIp3nS	mít
přesně	přesně	k6eAd1	přesně
vést	vést	k5eAaImF	vést
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vedení	vedení	k1gNnSc6	vedení
hranice	hranice	k1gFnSc2	hranice
ovšem	ovšem	k9	ovšem
závisí	záviset	k5eAaImIp3nS	záviset
řada	řada	k1gFnSc1	řada
charakteristik	charakteristika	k1gFnPc2	charakteristika
kontinentu	kontinent	k1gInSc2	kontinent
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc7	jeho
rozlohou	rozloha	k1gFnSc7	rozloha
počínaje	počínaje	k7c7	počínaje
a	a	k8xC	a
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
horou	hora	k1gFnSc7	hora
(	(	kIx(	(
<g/>
Mont	Mont	k2eAgMnSc1d1	Mont
Blanc	Blanc	k1gMnSc1	Blanc
vs	vs	k?	vs
<g/>
.	.	kIx.	.
</s>
<s>
Elbrus	Elbrus	k1gInSc1	Elbrus
<g/>
)	)	kIx)	)
konče	konče	k7c7	konče
<g/>
.	.	kIx.	.
</s>
<s>
Méně	málo	k6eAd2	málo
problematická	problematický	k2eAgFnSc1d1	problematická
je	být	k5eAaImIp3nS	být
severní	severní	k2eAgFnSc1d1	severní
část	část	k1gFnSc1	část
hranice	hranice	k1gFnSc1	hranice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
všeobecně	všeobecně	k6eAd1	všeobecně
přijímá	přijímat	k5eAaImIp3nS	přijímat
role	role	k1gFnSc1	role
pohoří	pohoří	k1gNnSc2	pohoří
Ural	Ural	k1gInSc4	Ural
(	(	kIx(	(
<g/>
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
v	v	k7c6	v
permu	perm	k1gInSc6	perm
nárazem	náraz	k1gInSc7	náraz
tehdejšího	tehdejší	k2eAgInSc2d1	tehdejší
kontinentu	kontinent	k1gInSc2	kontinent
Sibiř	Sibiř	k1gFnSc4	Sibiř
do	do	k7c2	do
Pangey	Pangea	k1gFnSc2	Pangea
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
někdejší	někdejší	k2eAgInPc1d1	někdejší
Baltiky	Baltik	k1gInPc1	Baltik
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
rozhraní	rozhraní	k1gNnSc4	rozhraní
kontinentů	kontinent	k1gInPc2	kontinent
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
nepovažuje	považovat	k5eNaImIp3nS	považovat
hlavní	hlavní	k2eAgInSc1d1	hlavní
hřeben	hřeben	k1gInSc1	hřeben
pohoří	pohořet	k5eAaPmIp3nS	pohořet
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gNnPc4	jeho
východní	východní	k2eAgNnPc4d1	východní
úpatí	úpatí	k1gNnPc4	úpatí
(	(	kIx(	(
<g/>
hranice	hranice	k1gFnSc1	hranice
začíná	začínat	k5eAaImIp3nS	začínat
v	v	k7c6	v
Bajdarackém	Bajdaracký	k2eAgInSc6d1	Bajdaracký
zálivu	záliv	k1gInSc6	záliv
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ural	Ural	k1gInSc1	Ural
tedy	tedy	k9	tedy
leží	ležet	k5eAaImIp3nS	ležet
celý	celý	k2eAgInSc1d1	celý
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Rozšířená	rozšířený	k2eAgFnSc1d1	rozšířená
a	a	k8xC	a
na	na	k7c6	na
českých	český	k2eAgFnPc6d1	Česká
školách	škola	k1gFnPc6	škola
vyučovaná	vyučovaný	k2eAgFnSc1d1	vyučovaná
verze	verze	k1gFnSc1	verze
dalšího	další	k2eAgInSc2d1	další
průběhu	průběh	k1gInSc2	průběh
hranice	hranice	k1gFnSc2	hranice
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
řece	řeka	k1gFnSc6	řeka
Embě	Embě	k1gMnSc2	Embě
do	do	k7c2	do
Kaspického	kaspický	k2eAgNnSc2d1	Kaspické
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
odtud	odtud	k6eAd1	odtud
Kumomanyčskou	Kumomanyčský	k2eAgFnSc7d1	Kumomanyčský
sníženinou	sníženina	k1gFnSc7	sníženina
podél	podél	k7c2	podél
řek	řeka	k1gFnPc2	řeka
Kuma	kuma	k1gFnSc1	kuma
a	a	k8xC	a
Manyč	Manyč	k1gInSc1	Manyč
až	až	k6eAd1	až
po	po	k7c6	po
ústí	ústí	k1gNnSc6	ústí
Manyče	Manyč	k1gInSc2	Manyč
do	do	k7c2	do
Donu	Don	k1gInSc2	Don
a	a	k8xC	a
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
do	do	k7c2	do
Azovského	azovský	k2eAgNnSc2d1	Azovské
moře	moře	k1gNnSc2	moře
(	(	kIx(	(
<g/>
na	na	k7c6	na
mapce	mapka	k1gFnSc6	mapka
vpravo	vpravo	k6eAd1	vpravo
označena	označit	k5eAaPmNgNnP	označit
písmenem	písmeno	k1gNnSc7	písmeno
A	A	kA	A
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Alternativ	alternativa	k1gFnPc2	alternativa
k	k	k7c3	k
takto	takto	k6eAd1	takto
stanovené	stanovený	k2eAgFnSc3d1	stanovená
hranici	hranice	k1gFnSc3	hranice
Evropy	Evropa	k1gFnSc2	Evropa
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
<g/>
.	.	kIx.	.
</s>
<s>
Relativně	relativně	k6eAd1	relativně
menší	malý	k2eAgFnSc1d2	menší
(	(	kIx(	(
<g/>
s	s	k7c7	s
menšími	malý	k2eAgInPc7d2	menší
důsledky	důsledek	k1gInPc7	důsledek
<g/>
)	)	kIx)	)
odchylkou	odchylka	k1gFnSc7	odchylka
je	být	k5eAaImIp3nS	být
starší	starý	k2eAgInSc4d2	starší
názor	názor	k1gInSc4	názor
na	na	k7c4	na
vedení	vedení	k1gNnSc4	vedení
hranice	hranice	k1gFnSc2	hranice
po	po	k7c6	po
hřebeni	hřeben	k1gInSc6	hřeben
Uralu	Ural	k1gInSc2	Ural
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
po	po	k7c6	po
řece	řeka	k1gFnSc6	řeka
Ural	Ural	k1gInSc1	Ural
(	(	kIx(	(
<g/>
teče	téct	k5eAaImIp3nS	téct
západněji	západně	k6eAd2	západně
než	než	k8xS	než
Emba	Emba	k1gFnSc1	Emba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
část	část	k1gFnSc1	část
Kazachstánu	Kazachstán	k1gInSc2	Kazachstán
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
Kaspickým	kaspický	k2eAgNnSc7d1	Kaspické
a	a	k8xC	a
Černým	černý	k2eAgNnSc7d1	černé
mořem	moře	k1gNnSc7	moře
pak	pak	k6eAd1	pak
nikoli	nikoli	k9	nikoli
korytem	koryto	k1gNnSc7	koryto
řek	řeka	k1gFnPc2	řeka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
až	až	k9	až
po	po	k7c6	po
severním	severní	k2eAgNnSc6d1	severní
úpatí	úpatí	k1gNnSc6	úpatí
Kavkazu	Kavkaz	k1gInSc2	Kavkaz
(	(	kIx(	(
<g/>
na	na	k7c6	na
mapce	mapka	k1gFnSc6	mapka
vpravo	vpravo	k6eAd1	vpravo
kombinace	kombinace	k1gFnSc1	kombinace
linií	linie	k1gFnPc2	linie
B	B	kA	B
a	a	k8xC	a
E	E	kA	E
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
posun	posun	k1gInSc1	posun
hranice	hranice	k1gFnSc2	hranice
z	z	k7c2	z
úpatí	úpatí	k1gNnSc2	úpatí
Kavkazu	Kavkaz	k1gInSc2	Kavkaz
na	na	k7c4	na
jeho	on	k3xPp3gInSc4	on
hlavní	hlavní	k2eAgInSc4d1	hlavní
hřeben	hřeben	k1gInSc4	hřeben
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
by	by	kYmCp3nS	by
připravil	připravit	k5eAaPmAgInS	připravit
o	o	k7c4	o
post	post	k1gInSc4	post
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
evropské	evropský	k2eAgFnSc2d1	Evropská
hory	hora	k1gFnSc2	hora
západoevropský	západoevropský	k2eAgMnSc1d1	západoevropský
Mont	Mont	k1gMnSc1	Mont
Blanc	Blanc	k1gMnSc1	Blanc
a	a	k8xC	a
přidělil	přidělit	k5eAaPmAgInS	přidělit
jej	on	k3xPp3gNnSc4	on
nejvyšší	vysoký	k2eAgNnSc4d3	nejvyšší
ruské	ruský	k2eAgNnSc4d1	ruské
hoře	hoře	k1gNnSc4	hoře
Elbrusu	Elbrus	k1gInSc2	Elbrus
<g/>
,	,	kIx,	,
navrhli	navrhnout	k5eAaPmAgMnP	navrhnout
američtí	americký	k2eAgMnPc1d1	americký
geografové	geograf	k1gMnPc1	geograf
(	(	kIx(	(
<g/>
na	na	k7c6	na
mapce	mapka	k1gFnSc6	mapka
vpravo	vpravo	k6eAd1	vpravo
označeno	označit	k5eAaPmNgNnS	označit
písmenem	písmeno	k1gNnSc7	písmeno
F	F	kA	F
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hranice	hranice	k1gFnSc1	hranice
vedená	vedený	k2eAgFnSc1d1	vedená
přesně	přesně	k6eAd1	přesně
po	po	k7c6	po
rozvodí	rozvodí	k1gNnSc6	rozvodí
by	by	kYmCp3nS	by
také	také	k9	také
znamenala	znamenat	k5eAaImAgFnS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
se	se	k3xPyFc4	se
dostanou	dostat	k5eAaPmIp3nP	dostat
malé	malý	k2eAgFnPc1d1	malá
části	část	k1gFnPc1	část
jinak	jinak	k6eAd1	jinak
převážně	převážně	k6eAd1	převážně
asijských	asijský	k2eAgInPc2d1	asijský
států	stát	k1gInPc2	stát
Gruzie	Gruzie	k1gFnSc2	Gruzie
a	a	k8xC	a
Ázerbájdžánu	Ázerbájdžán	k1gInSc2	Ázerbájdžán
<g/>
.	.	kIx.	.
</s>
<s>
Řeky	řeka	k1gFnPc1	řeka
Těrek	těrka	k1gFnPc2	těrka
(	(	kIx(	(
<g/>
asi	asi	k9	asi
50	[number]	k4	50
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Argun	Argun	k1gInSc1	Argun
(	(	kIx(	(
<g/>
asi	asi	k9	asi
18	[number]	k4	18
km	km	kA	km
<g/>
)	)	kIx)	)
a	a	k8xC	a
Andijské	Andijský	k2eAgFnSc2d1	Andijský
Kojsu	Kojs	k1gMnSc3	Kojs
pramení	pramenit	k5eAaImIp3nP	pramenit
na	na	k7c6	na
území	území	k1gNnSc6	území
Gruzie	Gruzie	k1gFnSc2	Gruzie
a	a	k8xC	a
odtékají	odtékat	k5eAaImIp3nP	odtékat
na	na	k7c4	na
sever	sever	k1gInSc4	sever
do	do	k7c2	do
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
ázerbájdžánské	ázerbájdžánský	k2eAgFnSc2d1	Ázerbájdžánská
hory	hora	k1gFnSc2	hora
Bazardüzü	Bazardüzü	k1gFnPc2	Bazardüzü
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
ázerbájdžánsko-dagestánská	ázerbájdžánskoagestánský	k2eAgFnSc1d1	ázerbájdžánsko-dagestánský
hranice	hranice	k1gFnSc1	hranice
odklání	odklánět	k5eAaImIp3nS	odklánět
k	k	k7c3	k
severovýchodu	severovýchod	k1gInSc3	severovýchod
a	a	k8xC	a
hlavní	hlavní	k2eAgInSc1d1	hlavní
hřeben	hřeben	k1gInSc1	hřeben
klesá	klesat	k5eAaImIp3nS	klesat
zbývajících	zbývající	k2eAgNnPc2d1	zbývající
asi	asi	k9	asi
230	[number]	k4	230
km	km	kA	km
ázerbájdžánským	ázerbájdžánský	k2eAgNnSc7d1	ázerbájdžánské
vnitrozemím	vnitrozemí	k1gNnSc7	vnitrozemí
k	k	k7c3	k
Baku	Baku	k1gNnSc3	Baku
<g/>
.	.	kIx.	.
</s>
<s>
Nejextrémnějším	extrémní	k2eAgMnSc7d3	nejextrémnější
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
směru	směr	k1gInSc6	směr
pohled	pohled	k1gInSc1	pohled
některých	některý	k3yIgMnPc2	některý
britských	britský	k2eAgMnPc2d1	britský
geografů	geograf	k1gMnPc2	geograf
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
posouvá	posouvat	k5eAaImIp3nS	posouvat
evropské	evropský	k2eAgFnPc4d1	Evropská
hranice	hranice	k1gFnPc4	hranice
až	až	k9	až
na	na	k7c4	na
severní	severní	k2eAgFnPc4d1	severní
hranice	hranice	k1gFnPc4	hranice
Turecka	Turecko	k1gNnSc2	Turecko
a	a	k8xC	a
Íránu	Írán	k1gInSc2	Írán
(	(	kIx(	(
<g/>
na	na	k7c6	na
mapce	mapka	k1gFnSc6	mapka
vpravo	vpravo	k6eAd1	vpravo
označeno	označit	k5eAaPmNgNnS	označit
písmenem	písmeno	k1gNnSc7	písmeno
J	J	kA	J
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
by	by	kYmCp3nS	by
evropskou	evropský	k2eAgFnSc7d1	Evropská
zemí	zem	k1gFnSc7	zem
byla	být	k5eAaImAgFnS	být
i	i	k9	i
Arménie	Arménie	k1gFnSc1	Arménie
a	a	k8xC	a
celé	celý	k2eAgNnSc1d1	celé
území	území	k1gNnSc1	území
Gruzie	Gruzie	k1gFnSc2	Gruzie
a	a	k8xC	a
Ázerbájdžánu	Ázerbájdžán	k1gInSc2	Ázerbájdžán
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Střed	středa	k1gFnPc2	středa
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
světadílu	světadíl	k1gInSc2	světadíl
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
akkadského	akkadský	k2eAgNnSc2d1	akkadské
slova	slovo	k1gNnSc2	slovo
ereb	ereba	k1gFnPc2	ereba
či	či	k8xC	či
irib	iriba	k1gFnPc2	iriba
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
západ	západ	k1gInSc1	západ
Slunce	slunce	k1gNnSc2	slunce
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jinou	jiný	k2eAgFnSc7d1	jiná
verzí	verze	k1gFnSc7	verze
je	být	k5eAaImIp3nS	být
pojmenování	pojmenování	k1gNnSc1	pojmenování
podle	podle	k7c2	podle
města	město	k1gNnSc2	město
Evropa	Evropa	k1gFnSc1	Evropa
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
nacházelo	nacházet	k5eAaImAgNnS	nacházet
v	v	k7c6	v
Malé	Malé	k2eAgFnSc6d1	Malé
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Evropa	Evropa	k1gFnSc1	Evropa
tvoří	tvořit	k5eAaImIp3nS	tvořit
vlastně	vlastně	k9	vlastně
obrovský	obrovský	k2eAgInSc1d1	obrovský
poloostrov	poloostrov	k1gInSc1	poloostrov
dvojkontinentu	dvojkontinent	k1gInSc2	dvojkontinent
Eurasie	Eurasie	k1gFnSc2	Eurasie
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
Starého	Starého	k2eAgInSc2d1	Starého
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Krajní	krajní	k2eAgInPc1d1	krajní
body	bod	k1gInPc1	bod
na	na	k7c6	na
pevnině	pevnina	k1gFnSc6	pevnina
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
na	na	k7c6	na
severu	sever	k1gInSc6	sever
mys	mys	k1gInSc1	mys
Nordkinn	Nordkinn	k1gInSc1	Nordkinn
(	(	kIx(	(
<g/>
Norsko	Norsko	k1gNnSc1	Norsko
<g/>
)	)	kIx)	)
–	–	k?	–
71	[number]	k4	71
<g/>
°	°	k?	°
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
'	'	kIx"	'
s.	s.	k?	s.
š.	š.	k?	š.
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
mys	mys	k1gInSc1	mys
Punta	punto	k1gNnSc2	punto
Marroqui	Marroqu	k1gFnSc2	Marroqu
známý	známý	k2eAgMnSc1d1	známý
též	též	k6eAd1	též
jako	jako	k8xC	jako
Punta	punto	k1gNnSc2	punto
de	de	k?	de
Tarifa	tarifa	k1gFnSc1	tarifa
(	(	kIx(	(
<g/>
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
)	)	kIx)	)
–	–	k?	–
35	[number]	k4	35
<g/>
°	°	k?	°
<g/>
59	[number]	k4	59
<g />
.	.	kIx.	.
</s>
<s>
<g/>
'	'	kIx"	'
s.	s.	k?	s.
š.	š.	k?	š.
na	na	k7c6	na
západě	západ	k1gInSc6	západ
mys	mys	k1gInSc1	mys
Cabo	Cabo	k6eAd1	Cabo
da	da	k?	da
Roca	Roca	k1gFnSc1	Roca
(	(	kIx(	(
<g/>
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
<g/>
)	)	kIx)	)
–	–	k?	–
9	[number]	k4	9
<g/>
°	°	k?	°
<g/>
29	[number]	k4	29
<g/>
'	'	kIx"	'
z.	z.	k?	z.
d.	d.	k?	d.
na	na	k7c6	na
východě	východ	k1gInSc6	východ
ústí	ústí	k1gNnSc2	ústí
řeky	řeka	k1gFnSc2	řeka
Bajdaraty	Bajdarat	k1gInPc4	Bajdarat
do	do	k7c2	do
Bajdaratského	Bajdaratský	k2eAgInSc2d1	Bajdaratský
zálivu	záliv	k1gInSc2	záliv
Karského	karský	k2eAgNnSc2d1	Karské
moře	moře	k1gNnSc2	moře
(	(	kIx(	(
<g/>
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
)	)	kIx)	)
–	–	k?	–
68	[number]	k4	68
<g/>
°	°	k?	°
<g/>
14	[number]	k4	14
<g/>
'	'	kIx"	'
v.	v.	k?	v.
d.	d.	k?	d.
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
srovnání	srovnání	k1gNnSc4	srovnání
<g/>
:	:	kIx,	:
tento	tento	k3xDgInSc1	tento
poledník	poledník	k1gInSc1	poledník
prochází	procházet	k5eAaImIp3nS	procházet
např.	např.	kA	např.
i	i	k8xC	i
Afghánistánem	Afghánistán	k1gInSc7	Afghánistán
<g/>
)	)	kIx)	)
Absolutní	absolutní	k2eAgInPc1d1	absolutní
krajní	krajní	k2eAgInPc1d1	krajní
body	bod	k1gInPc1	bod
Evropy	Evropa	k1gFnSc2	Evropa
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
na	na	k7c6	na
severu	sever	k1gInSc6	sever
mys	mys	k1gInSc1	mys
Fligely	Fligela	k1gFnSc2	Fligela
na	na	k7c6	na
Rudolfově	Rudolfův	k2eAgInSc6d1	Rudolfův
ostrově	ostrov	k1gInSc6	ostrov
v	v	k7c6	v
Zemi	zem	k1gFnSc6	zem
Františka	František	k1gMnSc2	František
Josefa	Josef	k1gMnSc2	Josef
(	(	kIx(	(
<g/>
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
)	)	kIx)	)
–	–	k?	–
81	[number]	k4	81
<g/>
°	°	k?	°
<g/>
51	[number]	k4	51
<g/>
<g />
.	.	kIx.	.
</s>
<s>
'	'	kIx"	'
s.	s.	k?	s.
š.	š.	k?	š.
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
ostrov	ostrov	k1gInSc1	ostrov
Gaudos	Gaudos	k1gInSc1	Gaudos
<g/>
,	,	kIx,	,
při	při	k7c6	při
jižním	jižní	k2eAgNnSc6d1	jižní
pobřeží	pobřeží	k1gNnSc6	pobřeží
Kréty	Kréta	k1gFnSc2	Kréta
(	(	kIx(	(
<g/>
Řecko	Řecko	k1gNnSc1	Řecko
<g/>
)	)	kIx)	)
–	–	k?	–
34	[number]	k4	34
<g/>
°	°	k?	°
<g/>
48	[number]	k4	48
<g/>
'	'	kIx"	'
s.	s.	k?	s.
š.	š.	k?	š.
na	na	k7c6	na
západě	západ	k1gInSc6	západ
mys	mys	k1gInSc1	mys
Bjargtangar	Bjargtangar	k1gInSc1	Bjargtangar
(	(	kIx(	(
<g/>
Island	Island	k1gInSc1	Island
<g/>
)	)	kIx)	)
–	–	k?	–
24	[number]	k4	24
<g/>
°	°	k?	°
<g/>
32	[number]	k4	32
<g/>
'	'	kIx"	'
z.	z.	k?	z.
d.	d.	k?	d.
<g/>
)	)	kIx)	)
na	na	k7c6	na
východě	východ	k1gInSc6	východ
ústí	ústí	k1gNnSc2	ústí
řeky	řeka	k1gFnSc2	řeka
Bajdaraty	Bajdarat	k1gInPc4	Bajdarat
do	do	k7c2	do
Bajdaratského	Bajdaratský	k2eAgInSc2d1	Bajdaratský
zálivu	záliv	k1gInSc2	záliv
Karského	karský	k2eAgNnSc2d1	Karské
moře	moře	k1gNnSc2	moře
(	(	kIx(	(
<g/>
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
)	)	kIx)	)
–	–	k?	–
68	[number]	k4	68
<g/>
°	°	k?	°
<g/>
14	[number]	k4	14
<g/>
'	'	kIx"	'
v.	v.	k?	v.
d.	d.	k?	d.
Rozloha	rozloha	k1gFnSc1	rozloha
Evropy	Evropa	k1gFnSc2	Evropa
činí	činit	k5eAaImIp3nS	činit
10,5	[number]	k4	10,5
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
km2	km2	k4	km2
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
7	[number]	k4	7
%	%	kIx~	%
světové	světový	k2eAgFnSc2d1	světová
souše	souš	k1gFnSc2	souš
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
druhý	druhý	k4xOgInSc1	druhý
nejmenší	malý	k2eAgInSc1d3	nejmenší
světadíl	světadíl	k1gInSc1	světadíl
po	po	k7c6	po
Austrálii	Austrálie	k1gFnSc6	Austrálie
a	a	k8xC	a
Oceánii	Oceánie	k1gFnSc6	Oceánie
<g/>
.	.	kIx.	.
</s>
<s>
Evropa	Evropa	k1gFnSc1	Evropa
má	mít	k5eAaImIp3nS	mít
velmi	velmi	k6eAd1	velmi
členité	členitý	k2eAgNnSc4d1	členité
pobřeží	pobřeží	k1gNnSc4	pobřeží
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
nejčlenitější	členitý	k2eAgInSc1d3	nejčlenitější
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
světadílů	světadíl	k1gInPc2	světadíl
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
pobřeží	pobřeží	k1gNnSc2	pobřeží
činí	činit	k5eAaImIp3nS	činit
37	[number]	k4	37
000	[number]	k4	000
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInPc1d3	veliký
poloostrovy	poloostrov	k1gInPc1	poloostrov
jsou	být	k5eAaImIp3nP	být
Poloostrov	poloostrov	k1gInSc1	poloostrov
Kola	kolo	k1gNnSc2	kolo
<g/>
,	,	kIx,	,
Skandinávský	skandinávský	k2eAgInSc4d1	skandinávský
<g/>
,	,	kIx,	,
Jutský	jutský	k2eAgInSc4d1	jutský
<g/>
,	,	kIx,	,
Pyrenejský	pyrenejský	k2eAgInSc4d1	pyrenejský
<g/>
,	,	kIx,	,
Apeninský	apeninský	k2eAgInSc4d1	apeninský
<g/>
,	,	kIx,	,
Balkánský	balkánský	k2eAgInSc4d1	balkánský
poloostrov	poloostrov	k1gInSc4	poloostrov
<g/>
,	,	kIx,	,
Peloponés	Peloponés	k1gInSc4	Peloponés
a	a	k8xC	a
Krym	Krym	k1gInSc4	Krym
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInPc1d3	veliký
ostrovy	ostrov	k1gInPc1	ostrov
Evropy	Evropa	k1gFnSc2	Evropa
jsou	být	k5eAaImIp3nP	být
soustředěny	soustředit	k5eAaPmNgFnP	soustředit
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
<g/>
:	:	kIx,	:
klikatobřehá	klikatobřehý	k2eAgFnSc1d1	klikatobřehý
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
,	,	kIx,	,
Island	Island	k1gInSc1	Island
a	a	k8xC	a
Irsko	Irsko	k1gNnSc1	Irsko
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
ostrovy	ostrov	k1gInPc1	ostrov
leží	ležet	k5eAaImIp3nP	ležet
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
:	:	kIx,	:
Nová	nový	k2eAgFnSc1d1	nová
země	země	k1gFnSc1	země
a	a	k8xC	a
Špicberky	Špicberky	k1gFnPc1	Špicberky
<g/>
,	,	kIx,	,
Gotland	Gotland	k1gInSc1	Gotland
<g/>
,	,	kIx,	,
Öland	Öland	k1gInSc1	Öland
<g/>
,	,	kIx,	,
Sjæ	Sjæ	k1gFnSc1	Sjæ
a	a	k8xC	a
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
:	:	kIx,	:
Mallorca	Mallorca	k1gFnSc1	Mallorca
<g/>
,	,	kIx,	,
Sicílie	Sicílie	k1gFnSc1	Sicílie
<g/>
,	,	kIx,	,
Sardinie	Sardinie	k1gFnSc1	Sardinie
<g/>
,	,	kIx,	,
Korsika	Korsika	k1gFnSc1	Korsika
<g/>
,	,	kIx,	,
Kréta	Kréta	k1gFnSc1	Kréta
a	a	k8xC	a
Rhodos	Rhodos	k1gInSc1	Rhodos
<g/>
.	.	kIx.	.
</s>
<s>
Evropa	Evropa	k1gFnSc1	Evropa
má	mít	k5eAaImIp3nS	mít
pestrý	pestrý	k2eAgInSc4d1	pestrý
reliéf	reliéf	k1gInSc4	reliéf
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
nadmořská	nadmořský	k2eAgFnSc1d1	nadmořská
výška	výška	k1gFnSc1	výška
činí	činit	k5eAaImIp3nS	činit
290	[number]	k4	290
m	m	kA	m
n.	n.	k?	n.
<g/>
m.	m.	k?	m.
(	(	kIx(	(
<g/>
nejméně	málo	k6eAd3	málo
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
světadílů	světadíl	k1gInPc2	světadíl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
území	území	k1gNnSc2	území
zaujímají	zaujímat	k5eAaImIp3nP	zaujímat
nížiny	nížina	k1gFnPc1	nížina
(	(	kIx(	(
<g/>
60	[number]	k4	60
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rozložení	rozložení	k1gNnSc1	rozložení
nížin	nížina	k1gFnPc2	nížina
a	a	k8xC	a
vysočin	vysočina	k1gFnPc2	vysočina
plyne	plynout	k5eAaImIp3nS	plynout
z	z	k7c2	z
geologické	geologický	k2eAgFnSc2d1	geologická
stavby	stavba	k1gFnSc2	stavba
a	a	k8xC	a
geomorfologického	geomorfologický	k2eAgInSc2d1	geomorfologický
vývoje	vývoj	k1gInSc2	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc1d3	nejstarší
část	část	k1gFnSc1	část
Evropy	Evropa	k1gFnSc2	Evropa
je	být	k5eAaImIp3nS	být
východoevropská	východoevropský	k2eAgFnSc1d1	východoevropská
nížinná	nížinný	k2eAgFnSc1d1	nížinná
Fennosarmatia	Fennosarmatia	k1gFnSc1	Fennosarmatia
s	s	k7c7	s
Východoevropskou	východoevropský	k2eAgFnSc7d1	východoevropská
rovinou	rovina	k1gFnSc7	rovina
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
leží	ležet	k5eAaImIp3nS	ležet
i	i	k9	i
nejnižší	nízký	k2eAgNnSc1d3	nejnižší
místo	místo	k1gNnSc1	místo
Evropy	Evropa	k1gFnSc2	Evropa
v	v	k7c6	v
proláklině	proláklina	k1gFnSc6	proláklina
Kaspické	kaspický	k2eAgFnSc2d1	kaspická
nížiny	nížina	k1gFnSc2	nížina
(	(	kIx(	(
<g/>
−	−	k?	−
<g/>
28	[number]	k4	28
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
východě	východ	k1gInSc6	východ
je	být	k5eAaImIp3nS	být
Východoevropská	východoevropský	k2eAgFnSc1d1	východoevropská
rovina	rovina	k1gFnSc1	rovina
lemována	lemovat	k5eAaImNgFnS	lemovat
protáhlým	protáhlý	k2eAgNnSc7d1	protáhlé
pohořím	pohoří	k1gNnSc7	pohoří
Ural	Ural	k1gInSc1	Ural
(	(	kIx(	(
<g/>
1894	[number]	k4	1894
m	m	kA	m
<g/>
)	)	kIx)	)
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
2500	[number]	k4	2500
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Skandinávské	skandinávský	k2eAgNnSc4d1	skandinávské
pohoří	pohoří	k1gNnSc4	pohoří
(	(	kIx(	(
<g/>
2469	[number]	k4	2469
m	m	kA	m
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
kaledonského	kaledonský	k2eAgNnSc2d1	kaledonské
stáří	stáří	k1gNnSc2	stáří
<g/>
.	.	kIx.	.
</s>
<s>
Ledovce	ledovec	k1gInPc1	ledovec
zde	zde	k6eAd1	zde
vyhloubily	vyhloubit	k5eAaPmAgInP	vyhloubit
typické	typický	k2eAgInPc1d1	typický
fjordy	fjord	k1gInPc1	fjord
<g/>
.	.	kIx.	.
</s>
<s>
Ledovec	ledovec	k1gInSc1	ledovec
poznamenal	poznamenat	k5eAaPmAgInS	poznamenat
i	i	k9	i
Severoněmeckou	severoněmecký	k2eAgFnSc4d1	Severoněmecká
a	a	k8xC	a
Středopolské	Středopolský	k2eAgFnSc2d1	Středopolský
nížiny	nížina	k1gFnSc2	nížina
<g/>
.	.	kIx.	.
</s>
<s>
Hercynským	hercynský	k2eAgNnSc7d1	hercynské
vrásněním	vrásnění	k1gNnSc7	vrásnění
vytvořilo	vytvořit	k5eAaPmAgNnS	vytvořit
Středoněmeckou	středoněmecký	k2eAgFnSc4d1	Středoněmecká
vysočinu	vysočina	k1gFnSc4	vysočina
<g/>
,	,	kIx,	,
Český	český	k2eAgInSc1d1	český
masiv	masiv	k1gInSc1	masiv
(	(	kIx(	(
<g/>
1602	[number]	k4	1602
m	m	kA	m
<g/>
)	)	kIx)	)
a	a	k8xC	a
Francouzské	francouzský	k2eAgNnSc1d1	francouzské
středohoří	středohoří	k1gNnSc1	středohoří
(	(	kIx(	(
<g/>
1886	[number]	k4	1886
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
třetihorního	třetihorní	k2eAgNnSc2d1	třetihorní
alpínského	alpínský	k2eAgNnSc2d1	alpínské
vrásnění	vrásnění	k1gNnSc2	vrásnění
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
Alp	Alpy	k1gFnPc2	Alpy
(	(	kIx(	(
<g/>
Mont	Mont	k1gMnSc1	Mont
Blanc	Blanc	k1gMnSc1	Blanc
<g/>
,	,	kIx,	,
4807	[number]	k4	4807
m	m	kA	m
<g/>
)	)	kIx)	)
a	a	k8xC	a
dalších	další	k2eAgNnPc2d1	další
evropských	evropský	k2eAgNnPc2d1	Evropské
pohoří	pohoří	k1gNnPc2	pohoří
(	(	kIx(	(
<g/>
Karpaty	Karpaty	k1gInPc7	Karpaty
(	(	kIx(	(
<g/>
2655	[number]	k4	2655
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Dinárské	dinárský	k2eAgFnPc1d1	Dinárská
hory	hora	k1gFnPc1	hora
(	(	kIx(	(
<g/>
2751	[number]	k4	2751
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Pyreneje	Pyreneje	k1gFnPc1	Pyreneje
(	(	kIx(	(
<g/>
3404	[number]	k4	3404
m	m	kA	m
<g/>
)	)	kIx)	)
a	a	k8xC	a
Apeniny	Apeniny	k1gFnPc1	Apeniny
(	(	kIx(	(
<g/>
2914	[number]	k4	2914
m	m	kA	m
<g/>
))	))	k?	))
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Evropě	Evropa	k1gFnSc6	Evropa
se	se	k3xPyFc4	se
setkáváme	setkávat	k5eAaImIp1nP	setkávat
i	i	k9	i
s	s	k7c7	s
vulkanismem	vulkanismus	k1gInSc7	vulkanismus
a	a	k8xC	a
zemětřesením	zemětřesení	k1gNnSc7	zemětřesení
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Sicílii	Sicílie	k1gFnSc6	Sicílie
se	se	k3xPyFc4	se
tyčí	tyčit	k5eAaImIp3nS	tyčit
i	i	k9	i
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
činná	činný	k2eAgFnSc1d1	činná
sopka	sopka	k1gFnSc1	sopka
Evropy	Evropa	k1gFnSc2	Evropa
Etna	Etna	k1gFnSc1	Etna
(	(	kIx(	(
<g/>
3340	[number]	k4	3340
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgInPc7d1	další
vulkány	vulkán	k1gInPc7	vulkán
jsou	být	k5eAaImIp3nP	být
Stromboli	Strombole	k1gFnSc4	Strombole
<g/>
,	,	kIx,	,
Monte	Mont	k1gMnSc5	Mont
Epomeo	Epomea	k1gMnSc5	Epomea
a	a	k8xC	a
Vesuv	Vesuv	k1gInSc4	Vesuv
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
s	s	k7c7	s
poměrně	poměrně	k6eAd1	poměrně
vlhkým	vlhký	k2eAgNnSc7d1	vlhké
podnebím	podnebí	k1gNnSc7	podnebí
je	být	k5eAaImIp3nS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
poměrně	poměrně	k6eAd1	poměrně
hustá	hustý	k2eAgFnSc1d1	hustá
říční	říční	k2eAgFnSc1d1	říční
síť	síť	k1gFnSc1	síť
<g/>
.	.	kIx.	.
</s>
<s>
80	[number]	k4	80
%	%	kIx~	%
Evropy	Evropa	k1gFnSc2	Evropa
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
úmoří	úmoří	k1gNnSc6	úmoří
Atlantského	atlantský	k2eAgInSc2d1	atlantský
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
20	[number]	k4	20
%	%	kIx~	%
Evropy	Evropa	k1gFnSc2	Evropa
je	být	k5eAaImIp3nS	být
odvodňováno	odvodňován	k2eAgNnSc1d1	odvodňován
do	do	k7c2	do
bezodtokého	bezodtoký	k2eAgNnSc2d1	bezodtoké
Kaspického	kaspický	k2eAgNnSc2d1	Kaspické
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
do	do	k7c2	do
něhož	jenž	k3xRgInSc2	jenž
ústí	ústit	k5eAaImIp3nS	ústit
nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
evropská	evropský	k2eAgFnSc1d1	Evropská
řeka	řeka	k1gFnSc1	řeka
Volha	Volha	k1gFnSc1	Volha
(	(	kIx(	(
<g/>
3531	[number]	k4	3531
km	km	kA	km
<g/>
,	,	kIx,	,
povodí	povodit	k5eAaPmIp3nS	povodit
1	[number]	k4	1
360	[number]	k4	360
000	[number]	k4	000
km2	km2	k4	km2
<g/>
,	,	kIx,	,
s	s	k7c7	s
průměrným	průměrný	k2eAgInSc7d1	průměrný
ročním	roční	k2eAgInSc7d1	roční
průtokem	průtok	k1gInSc7	průtok
8220	[number]	k4	8220
m3	m3	k4	m3
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
velkými	velký	k2eAgFnPc7d1	velká
řekami	řeka	k1gFnPc7	řeka
jsou	být	k5eAaImIp3nP	být
Don	Don	k1gInSc4	Don
<g/>
,	,	kIx,	,
Dněpr	Dněpr	k1gInSc1	Dněpr
<g/>
,	,	kIx,	,
Dunaj	Dunaj	k1gInSc1	Dunaj
<g/>
,	,	kIx,	,
Pád	Pád	k1gInSc1	Pád
<g/>
,	,	kIx,	,
Rhôna	Rhôna	k1gFnSc1	Rhôna
<g/>
,	,	kIx,	,
Ebro	Ebro	k1gNnSc1	Ebro
<g/>
,	,	kIx,	,
Seina	Seina	k1gFnSc1	Seina
<g/>
,	,	kIx,	,
Temže	Temže	k1gFnSc1	Temže
<g/>
,	,	kIx,	,
Rýn	Rýn	k1gInSc1	Rýn
<g/>
,	,	kIx,	,
Labe	Labe	k1gNnSc1	Labe
<g/>
,	,	kIx,	,
Odra	Odra	k1gFnSc1	Odra
a	a	k8xC	a
Visla	Visla	k1gFnSc1	Visla
<g/>
.	.	kIx.	.
</s>
<s>
Maxima	Maxima	k1gFnSc1	Maxima
vodních	vodní	k2eAgInPc2d1	vodní
stavů	stav	k1gInPc2	stav
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Evropě	Evropa	k1gFnSc6	Evropa
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
a	a	k8xC	a
počátkem	počátkem	k7c2	počátkem
léta	léto	k1gNnSc2	léto
<g/>
,	,	kIx,	,
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
a	a	k8xC	a
severní	severní	k2eAgFnSc6d1	severní
Evropě	Evropa	k1gFnSc6	Evropa
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
<g/>
,	,	kIx,	,
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
a	a	k8xC	a
jižní	jižní	k2eAgFnSc6d1	jižní
Evropě	Evropa	k1gFnSc6	Evropa
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
a	a	k8xC	a
ve	v	k7c6	v
velehorách	velehora	k1gFnPc6	velehora
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
evropských	evropský	k2eAgNnPc2d1	Evropské
jezer	jezero	k1gNnPc2	jezero
je	být	k5eAaImIp3nS	být
ledovcového	ledovcový	k2eAgInSc2d1	ledovcový
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Největšími	veliký	k2eAgNnPc7d3	veliký
jezery	jezero	k1gNnPc7	jezero
jsou	být	k5eAaImIp3nP	být
Ladožské	ladožský	k2eAgNnSc1d1	Ladožské
(	(	kIx(	(
<g/>
18	[number]	k4	18
360	[number]	k4	360
km2	km2	k4	km2
<g/>
)	)	kIx)	)
a	a	k8xC	a
Oněžské	oněžský	k2eAgNnSc1d1	Oněžské
jezero	jezero	k1gNnSc1	jezero
(	(	kIx(	(
<g/>
9700	[number]	k4	9700
km2	km2	k4	km2
<g/>
)	)	kIx)	)
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
Evropa	Evropa	k1gFnSc1	Evropa
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
tří	tři	k4xCgInPc2	tři
podnebných	podnebný	k2eAgInPc2d1	podnebný
pásů	pás	k1gInPc2	pás
severní	severní	k2eAgFnSc2d1	severní
polokoule	polokoule	k1gFnSc2	polokoule
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
nejzazším	zadní	k2eAgInSc6d3	nejzazší
severu	sever	k1gInSc6	sever
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
studený	studený	k2eAgInSc1d1	studený
pás	pás	k1gInSc1	pás
se	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
oblastmi	oblast	k1gFnPc7	oblast
<g/>
,	,	kIx,	,
arktickou	arktický	k2eAgFnSc4d1	arktická
a	a	k8xC	a
subarktickou	subarktický	k2eAgFnSc4d1	subarktická
<g/>
,	,	kIx,	,
na	na	k7c6	na
Špicberkách	Špicberky	k1gInPc6	Špicberky
<g/>
,	,	kIx,	,
Nové	Nové	k2eAgFnSc6d1	Nové
zemi	zem	k1gFnSc6	zem
a	a	k8xC	a
severním	severní	k2eAgNnSc6d1	severní
pobřeží	pobřeží	k1gNnSc6	pobřeží
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
leží	ležet	k5eAaImIp3nS	ležet
i	i	k9	i
absolutně	absolutně	k6eAd1	absolutně
nejchladnější	chladný	k2eAgNnSc4d3	nejchladnější
místo	místo	k1gNnSc4	místo
Evropy	Evropa	k1gFnSc2	Evropa
Usť-Cilma	Usť-Cilmum	k1gNnSc2	Usť-Cilmum
(	(	kIx(	(
<g/>
−	−	k?	−
<g/>
69	[number]	k4	69
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
Evropy	Evropa	k1gFnSc2	Evropa
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
severním	severní	k2eAgInSc6d1	severní
mírném	mírný	k2eAgInSc6d1	mírný
pásu	pás	k1gInSc6	pás
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Evropě	Evropa	k1gFnSc6	Evropa
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
západopřímořská	západopřímořský	k2eAgFnSc1d1	západopřímořský
oblast	oblast	k1gFnSc1	oblast
s	s	k7c7	s
převládajícími	převládající	k2eAgInPc7d1	převládající
západními	západní	k2eAgInPc7d1	západní
větry	vítr	k1gInPc7	vítr
a	a	k8xC	a
s	s	k7c7	s
mírnou	mírný	k2eAgFnSc7d1	mírná
zimou	zima	k1gFnSc7	zima
i	i	k8xC	i
létem	léto	k1gNnSc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
je	být	k5eAaImIp3nS	být
oblast	oblast	k1gFnSc1	oblast
přechodná	přechodný	k2eAgFnSc1d1	přechodná
<g/>
,	,	kIx,	,
teplé	teplý	k2eAgNnSc1d1	teplé
léto	léto	k1gNnSc1	léto
se	s	k7c7	s
srážkami	srážka	k1gFnPc7	srážka
se	se	k3xPyFc4	se
střídá	střídat	k5eAaImIp3nS	střídat
se	s	k7c7	s
zimou	zima	k1gFnSc7	zima
s	s	k7c7	s
trvalou	trvalý	k2eAgFnSc7d1	trvalá
sněhovou	sněhový	k2eAgFnSc7d1	sněhová
pokrývkou	pokrývka	k1gFnSc7	pokrývka
<g/>
.	.	kIx.	.
</s>
<s>
Východní	východní	k2eAgFnSc1d1	východní
Evropa	Evropa	k1gFnSc1	Evropa
leží	ležet	k5eAaImIp3nS	ležet
ve	v	k7c6	v
vnitrozemské	vnitrozemský	k2eAgFnSc6d1	vnitrozemská
(	(	kIx(	(
<g/>
kontinentální	kontinentální	k2eAgFnSc6d1	kontinentální
<g/>
)	)	kIx)	)
oblasti	oblast	k1gFnSc3	oblast
s	s	k7c7	s
dlouhými	dlouhý	k2eAgFnPc7d1	dlouhá
<g/>
,	,	kIx,	,
studenými	studený	k2eAgFnPc7d1	studená
zimami	zima	k1gFnPc7	zima
a	a	k8xC	a
horkými	horký	k2eAgNnPc7d1	horké
léty	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
leží	ležet	k5eAaImIp3nS	ležet
nejsušší	suchý	k2eAgNnSc4d3	nejsušší
místo	místo	k1gNnSc4	místo
Evropy	Evropa	k1gFnSc2	Evropa
Astrachaň	Astrachaň	k1gFnSc1	Astrachaň
(	(	kIx(	(
<g/>
180	[number]	k4	180
mm	mm	kA	mm
ročně	ročně	k6eAd1	ročně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
absolutní	absolutní	k2eAgFnSc1d1	absolutní
teplota	teplota	k1gFnSc1	teplota
byla	být	k5eAaImAgFnS	být
naměřena	naměřit	k5eAaBmNgFnS	naměřit
v	v	k7c6	v
močálu	močál	k1gInSc6	močál
Pantano	Pantana	k1gFnSc5	Pantana
de	de	k?	de
Guadalmelatto	Guadalmelatt	k2eAgNnSc4d1	Guadalmelatt
u	u	k7c2	u
Córdoby	Córdoba	k1gFnSc2	Córdoba
(	(	kIx(	(
<g/>
52	[number]	k4	52
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
ve	v	k7c6	v
středomořské	středomořský	k2eAgFnSc6d1	středomořská
oblasti	oblast	k1gFnSc6	oblast
subtropického	subtropický	k2eAgInSc2d1	subtropický
pásu	pás	k1gInSc2	pás
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
pásu	pás	k1gInSc6	pás
leží	ležet	k5eAaImIp3nS	ležet
i	i	k9	i
nejdeštivější	deštivý	k2eAgNnSc4d3	nejdeštivější
místo	místo	k1gNnSc4	místo
Evropy	Evropa	k1gFnSc2	Evropa
Crkvica	Crkvic	k1gInSc2	Crkvic
(	(	kIx(	(
<g/>
4624	[number]	k4	4624
mm	mm	kA	mm
ročně	ročně	k6eAd1	ročně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vegetační	vegetační	k2eAgInPc1d1	vegetační
pásy	pás	k1gInPc1	pás
jsou	být	k5eAaImIp3nP	být
závislé	závislý	k2eAgInPc1d1	závislý
na	na	k7c6	na
podnebných	podnebný	k2eAgInPc6d1	podnebný
poměrech	poměr	k1gInPc6	poměr
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
jich	on	k3xPp3gMnPc2	on
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
sedm	sedm	k4xCc1	sedm
<g/>
:	:	kIx,	:
mrazové	mrazový	k2eAgFnSc2d1	mrazová
pustiny	pustina	k1gFnSc2	pustina
<g/>
,	,	kIx,	,
tundry	tundra	k1gFnSc2	tundra
a	a	k8xC	a
lesotundry	lesotundra	k1gFnSc2	lesotundra
s	s	k7c7	s
mechy	mech	k1gInPc7	mech
a	a	k8xC	a
lišejníky	lišejník	k1gInPc1	lišejník
<g/>
,	,	kIx,	,
tajga	tajga	k1gFnSc1	tajga
s	s	k7c7	s
jehličnatými	jehličnatý	k2eAgInPc7d1	jehličnatý
lesy	les	k1gInPc7	les
<g/>
,	,	kIx,	,
smíšené	smíšený	k2eAgInPc1d1	smíšený
a	a	k8xC	a
listnaté	listnatý	k2eAgInPc1d1	listnatý
lesy	les	k1gInPc1	les
<g/>
,	,	kIx,	,
stepi	step	k1gFnPc1	step
a	a	k8xC	a
lesostepi	lesostep	k1gFnPc1	lesostep
se	s	k7c7	s
suchomilnými	suchomilný	k2eAgFnPc7d1	suchomilná
trávami	tráva	k1gFnPc7	tráva
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
proměněná	proměněný	k2eAgFnSc1d1	proměněná
v	v	k7c4	v
kulturní	kulturní	k2eAgFnSc4d1	kulturní
step	step	k1gFnSc4	step
s	s	k7c7	s
úrodnou	úrodný	k2eAgFnSc7d1	úrodná
půdou	půda	k1gFnSc7	půda
<g/>
,	,	kIx,	,
subtropické	subtropický	k2eAgInPc1d1	subtropický
listnaté	listnatý	k2eAgInPc1d1	listnatý
opadavé	opadavý	k2eAgInPc1d1	opadavý
a	a	k8xC	a
vždyzelené	vždyzelený	k2eAgInPc1d1	vždyzelený
suchomilné	suchomilný	k2eAgInPc1d1	suchomilný
lesy	les	k1gInPc1	les
a	a	k8xC	a
trnité	trnitý	k2eAgFnPc1d1	trnitá
křoviny	křovina	k1gFnPc1	křovina
macchie	macchie	k1gFnSc1	macchie
a	a	k8xC	a
pouště	poušť	k1gFnPc1	poušť
a	a	k8xC	a
polopouště	polopoušť	k1gFnPc1	polopoušť
v	v	k7c6	v
kaspické	kaspický	k2eAgFnSc6d1	kaspická
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
horách	hora	k1gFnPc6	hora
je	být	k5eAaImIp3nS	být
vyvinuta	vyvinout	k5eAaPmNgFnS	vyvinout
výšková	výškový	k2eAgFnSc1d1	výšková
stupňovitost	stupňovitost	k1gFnSc1	stupňovitost
s	s	k7c7	s
alpínskou	alpínský	k2eAgFnSc7d1	alpínská
květenou	květena	k1gFnSc7	květena
<g/>
.	.	kIx.	.
</s>
<s>
Zvířena	zvířena	k1gFnSc1	zvířena
má	mít	k5eAaImIp3nS	mít
typické	typický	k2eAgMnPc4d1	typický
zástupce	zástupce	k1gMnPc4	zástupce
<g/>
:	:	kIx,	:
soba	soba	k1gFnSc1	soba
v	v	k7c6	v
tundrách	tundra	k1gFnPc6	tundra
<g/>
,	,	kIx,	,
losa	los	k1gMnSc2	los
<g/>
,	,	kIx,	,
vlka	vlk	k1gMnSc2	vlk
<g/>
,	,	kIx,	,
medvěda	medvěd	k1gMnSc2	medvěd
hnědého	hnědý	k2eAgMnSc2d1	hnědý
<g/>
,	,	kIx,	,
rysa	rys	k1gMnSc2	rys
<g/>
,	,	kIx,	,
jelena	jelen	k1gMnSc2	jelen
a	a	k8xC	a
rosomáka	rosomák	k1gMnSc2	rosomák
v	v	k7c6	v
tajze	tajga	k1gFnSc6	tajga
<g/>
,	,	kIx,	,
lišku	liška	k1gFnSc4	liška
<g/>
,	,	kIx,	,
vydru	vydra	k1gFnSc4	vydra
<g/>
,	,	kIx,	,
zajíce	zajíc	k1gMnSc2	zajíc
a	a	k8xC	a
ježka	ježek	k1gMnSc2	ježek
ve	v	k7c6	v
smíšených	smíšený	k2eAgInPc6d1	smíšený
a	a	k8xC	a
listnatých	listnatý	k2eAgInPc6d1	listnatý
lesích	les	k1gInPc6	les
<g/>
,	,	kIx,	,
kamzíka	kamzík	k1gMnSc2	kamzík
v	v	k7c6	v
horských	horský	k2eAgFnPc6d1	horská
oblastech	oblast	k1gFnPc6	oblast
a	a	k8xC	a
různé	různý	k2eAgInPc1d1	různý
druhy	druh	k1gInPc1	druh
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
plazů	plaz	k1gMnPc2	plaz
<g/>
,	,	kIx,	,
obojživelníků	obojživelník	k1gMnPc2	obojživelník
<g/>
,	,	kIx,	,
ryb	ryba	k1gFnPc2	ryba
a	a	k8xC	a
hmyzu	hmyz	k1gInSc2	hmyz
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgNnSc1d1	původní
přírodní	přírodní	k2eAgNnSc1d1	přírodní
prostředí	prostředí	k1gNnSc1	prostředí
je	být	k5eAaImIp3nS	být
chráněno	chránit	k5eAaImNgNnS	chránit
k	k	k7c3	k
přírodních	přírodní	k2eAgFnPc6d1	přírodní
rezervacích	rezervace	k1gFnPc6	rezervace
a	a	k8xC	a
národních	národní	k2eAgInPc6d1	národní
parcích	park	k1gInPc6	park
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnPc1d3	nejvyšší
hory	hora	k1gFnPc1	hora
Související	související	k2eAgFnPc4d1	související
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
hory	hora	k1gFnSc2	hora
evropských	evropský	k2eAgFnPc2d1	Evropská
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Elbrus	Elbrus	k1gInSc1	Elbrus
(	(	kIx(	(
<g/>
Э	Э	k?	Э
<g/>
)	)	kIx)	)
5642	[number]	k4	5642
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
,	,	kIx,	,
Kavkaz	Kavkaz	k1gInSc4	Kavkaz
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
spor	spor	k1gInSc1	spor
o	o	k7c4	o
geografické	geografický	k2eAgNnSc4d1	geografické
vymezení	vymezení	k1gNnSc4	vymezení
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
)	)	kIx)	)
Mont	Mont	k1gMnSc1	Mont
Blanc	Blanc	k1gMnSc1	Blanc
(	(	kIx(	(
<g/>
Monte	Mont	k1gInSc5	Mont
Bianco	bianco	k2eAgInPc6d1	bianco
<g/>
)	)	kIx)	)
4810	[number]	k4	4810
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
,	,	kIx,	,
Savojské	savojský	k2eAgFnPc1d1	Savojská
Alpy	Alpy	k1gFnPc1	Alpy
Monte	Mont	k1gInSc5	Mont
Rosa	Rosa	k1gFnSc1	Rosa
(	(	kIx(	(
<g/>
Pic	pic	k0	pic
Dufour	Dufour	k1gMnSc1	Dufour
<g/>
)	)	kIx)	)
4634	[number]	k4	4634
m	m	kA	m
n.	n.	k?	n.
<g />
.	.	kIx.	.
</s>
<s>
m.	m.	k?	m.
<g/>
,	,	kIx,	,
Penninské	Penninský	k2eAgFnSc2d1	Penninský
Alpy	alpa	k1gFnSc2	alpa
Dom	Dom	k?	Dom
4545	[number]	k4	4545
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
,	,	kIx,	,
Valaiské	Valaiský	k2eAgFnPc1d1	Valaiský
Alpy	Alpy	k1gFnPc1	Alpy
Weisshorn	Weisshorn	k1gNnSc1	Weisshorn
4505	[number]	k4	4505
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
,	,	kIx,	,
Valaiské	Valaiský	k2eAgFnPc1d1	Valaiský
Alpy	Alpy	k1gFnPc1	Alpy
Matterhorn	Matterhorna	k1gFnPc2	Matterhorna
(	(	kIx(	(
<g/>
Monte	Mont	k1gInSc5	Mont
Cervino	Cervin	k2eAgNnSc1d1	Cervino
<g/>
)	)	kIx)	)
4478	[number]	k4	4478
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
,	,	kIx,	,
Penninské	Penninský	k2eAgFnPc1d1	Penninský
Alpy	Alpy	k1gFnPc1	Alpy
Dent	Dent	k1gInSc1	Dent
Blanche	Blanch	k1gFnSc2	Blanch
4357	[number]	k4	4357
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
,	,	kIx,	,
Valaiské	Valaiský	k2eAgFnSc2d1	Valaiský
Alpy	alpa	k1gFnSc2	alpa
Grand	grand	k1gMnSc1	grand
Combin	Combina	k1gFnPc2	Combina
<g />
.	.	kIx.	.
</s>
<s>
4314	[number]	k4	4314
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
,	,	kIx,	,
Valaiské	Valaiský	k2eAgFnPc1d1	Valaiský
Alpy	Alpy	k1gFnPc1	Alpy
Finsteraarhorn	Finsteraarhorn	k1gNnSc1	Finsteraarhorn
4274	[number]	k4	4274
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
,	,	kIx,	,
Bernské	bernský	k2eAgFnPc1d1	Bernská
Alpy	Alpy	k1gFnPc1	Alpy
Aletschhorn	Aletschhorn	k1gNnSc1	Aletschhorn
4195	[number]	k4	4195
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
,	,	kIx,	,
Bernské	bernský	k2eAgFnPc1d1	Bernská
Alpy	Alpy	k1gFnPc1	Alpy
Nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
činné	činný	k2eAgFnSc2d1	činná
sopky	sopka	k1gFnSc2	sopka
Etna	Etna	k1gFnSc1	Etna
3329	[number]	k4	3329
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
,	,	kIx,	,
Sicílie	Sicílie	k1gFnSc1	Sicílie
Beerenberg	Beerenberg	k1gInSc1	Beerenberg
2277	[number]	k4	2277
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Mayen	Mayen	k2eAgMnSc1d1	Mayen
Askja	Askja	k1gMnSc1	Askja
1510	[number]	k4	1510
m	m	kA	m
n.	n.	k?	n.
<g />
.	.	kIx.	.
</s>
<s>
m.	m.	k?	m.
<g/>
,	,	kIx,	,
Island	Island	k1gInSc4	Island
Hekla	heknout	k5eAaPmAgFnS	heknout
1491	[number]	k4	1491
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
,	,	kIx,	,
Island	Island	k1gInSc1	Island
Vesuv	Vesuv	k1gInSc1	Vesuv
1277	[number]	k4	1277
m	m	kA	m
n.	n.	k?	n.
<g/>
m.	m.	k?	m.
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
Stromboli	Strombole	k1gFnSc4	Strombole
926	[number]	k4	926
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
,	,	kIx,	,
Liparské	Liparský	k2eAgInPc1d1	Liparský
ostrovy	ostrov	k1gInPc1	ostrov
Théra	Théra	k1gFnSc1	Théra
584	[number]	k4	584
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
,	,	kIx,	,
Řecko	Řecko	k1gNnSc1	Řecko
Nejdelší	dlouhý	k2eAgFnSc2d3	nejdelší
řeky	řeka	k1gFnSc2	řeka
Volha	Volha	k1gFnSc1	Volha
(	(	kIx(	(
<g/>
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
)	)	kIx)	)
3534	[number]	k4	3534
km	km	kA	km
Dunaj	Dunaj	k1gInSc1	Dunaj
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
,	,	kIx,	,
Slovensko	Slovensko	k1gNnSc1	Slovensko
<g/>
,	,	kIx,	,
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
<g/>
,	,	kIx,	,
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
<g/>
,	,	kIx,	,
Srbsko	Srbsko	k1gNnSc1	Srbsko
<g/>
,	,	kIx,	,
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
<g/>
,	,	kIx,	,
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
<g/>
,	,	kIx,	,
Moldavsko	Moldavsko	k1gNnSc1	Moldavsko
<g/>
,	,	kIx,	,
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
<g/>
)	)	kIx)	)
2857	[number]	k4	2857
km	km	kA	km
Ural	Ural	k1gInSc1	Ural
(	(	kIx(	(
<g/>
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
,	,	kIx,	,
Kazachstán	Kazachstán	k1gInSc1	Kazachstán
<g/>
)	)	kIx)	)
2428	[number]	k4	2428
km	km	kA	km
Dněpr	Dněpr	k1gInSc1	Dněpr
(	(	kIx(	(
<g/>
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Bělorusko	Bělorusko	k1gNnSc1	Bělorusko
<g/>
,	,	kIx,	,
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
<g/>
)	)	kIx)	)
2200	[number]	k4	2200
km	km	kA	km
Don	dona	k1gFnPc2	dona
(	(	kIx(	(
<g/>
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
)	)	kIx)	)
1950	[number]	k4	1950
km	km	kA	km
Kama	Kama	k1gFnSc1	Kama
(	(	kIx(	(
<g/>
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
)	)	kIx)	)
1805	[number]	k4	1805
km	km	kA	km
Největší	veliký	k2eAgInSc4d3	veliký
jezera	jezero	k1gNnSc2	jezero
Ladožské	ladožský	k2eAgNnSc1d1	Ladožské
jezero	jezero	k1gNnSc1	jezero
(	(	kIx(	(
<g/>
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
)	)	kIx)	)
18	[number]	k4	18
130	[number]	k4	130
km2	km2	k4	km2
Oněžské	oněžský	k2eAgNnSc1d1	Oněžské
jezero	jezero	k1gNnSc1	jezero
(	(	kIx(	(
<g/>
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
)	)	kIx)	)
9616	[number]	k4	9616
km2	km2	k4	km2
Vänern	Vänerna	k1gFnPc2	Vänerna
(	(	kIx(	(
<g/>
Švédsko	Švédsko	k1gNnSc1	Švédsko
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
5650	[number]	k4	5650
km2	km2	k4	km2
Saimaa	Saimaa	k1gFnSc1	Saimaa
(	(	kIx(	(
<g/>
Finsko	Finsko	k1gNnSc1	Finsko
<g/>
)	)	kIx)	)
4377	[number]	k4	4377
km2	km2	k4	km2
Další	další	k2eAgInPc4d1	další
rekordy	rekord	k1gInPc4	rekord
největší	veliký	k2eAgInSc1d3	veliký
ostrov	ostrov	k1gInSc1	ostrov
–	–	k?	–
Velká	velká	k1gFnSc1	velká
Británie	Británie	k1gFnSc2	Británie
největší	veliký	k2eAgNnSc4d3	veliký
město	město	k1gNnSc4	město
–	–	k?	–
Istanbul	Istanbul	k1gInSc1	Istanbul
(	(	kIx(	(
<g/>
celý	celý	k2eAgInSc1d1	celý
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
asijské	asijský	k2eAgFnSc2d1	asijská
části	část	k1gFnSc2	část
<g/>
)	)	kIx)	)
resp.	resp.	kA	resp.
Moskva	Moskva	k1gFnSc1	Moskva
největší	veliký	k2eAgFnSc1d3	veliký
ledovec	ledovec	k1gInSc1	ledovec
–	–	k?	–
Jostedalsbreen	Jostedalsbreen	k2eAgInSc1d1	Jostedalsbreen
největší	veliký	k2eAgInSc1d3	veliký
stát	stát	k1gInSc1	stát
podle	podle	k7c2	podle
rozlohy	rozloha	k1gFnSc2	rozloha
–	–	k?	–
Rusko	Rusko	k1gNnSc4	Rusko
nejhustší	hustý	k2eAgFnSc4d3	nejhustší
železniční	železniční	k2eAgFnSc4d1	železniční
síť	síť	k1gFnSc4	síť
–	–	k?	–
Česko	Česko	k1gNnSc4	Česko
nejhustší	hustý	k2eAgFnSc2d3	nejhustší
<g />
.	.	kIx.	.
</s>
<s>
silniční	silniční	k2eAgFnSc1d1	silniční
síť	síť	k1gFnSc1	síť
–	–	k?	–
Belgie	Belgie	k1gFnSc1	Belgie
nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
silniční	silniční	k2eAgFnSc1d1	silniční
tunel	tunel	k1gInSc1	tunel
–	–	k?	–
Læ	Læ	k1gFnSc1	Læ
(	(	kIx(	(
<g/>
Norsko	Norsko	k1gNnSc1	Norsko
<g/>
)	)	kIx)	)
nejdelší	dlouhý	k2eAgInSc1d3	nejdelší
železniční	železniční	k2eAgInSc1d1	železniční
tunel	tunel	k1gInSc1	tunel
–	–	k?	–
Gotthardský	Gotthardský	k2eAgInSc1d1	Gotthardský
úpatní	úpatní	k2eAgInSc1d1	úpatní
tunel	tunel	k1gInSc1	tunel
(	(	kIx(	(
<g/>
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
)	)	kIx)	)
nejrychlejší	rychlý	k2eAgInSc1d3	nejrychlejší
vlak	vlak	k1gInSc1	vlak
–	–	k?	–
TGV	TGV	kA	TGV
(	(	kIx(	(
<g/>
Francie	Francie	k1gFnSc1	Francie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Frecciarossa	Frecciarossa	k1gFnSc1	Frecciarossa
(	(	kIx(	(
<g/>
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
)	)	kIx)	)
nejdelší	dlouhý	k2eAgInSc1d3	nejdelší
most	most	k1gInSc1	most
-	-	kIx~	-
Most	most	k1gInSc1	most
Vasco	Vasco	k6eAd1	Vasco
da	da	k?	da
Gama	gama	k1gNnSc1	gama
(	(	kIx(	(
<g/>
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
<g/>
)	)	kIx)	)
Podrobnější	podrobný	k2eAgFnPc1d2	podrobnější
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc4	dějiny
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
civilizace	civilizace	k1gFnSc1	civilizace
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
Evropy	Evropa	k1gFnSc2	Evropa
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Egejského	egejský	k2eAgNnSc2d1	Egejské
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Krétě	Kréta	k1gFnSc6	Kréta
se	se	k3xPyFc4	se
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
2700	[number]	k4	2700
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
zrodila	zrodit	k5eAaPmAgFnS	zrodit
Egyptem	Egypt	k1gInSc7	Egypt
ovlivněná	ovlivněný	k2eAgFnSc1d1	ovlivněná
Mínojská	Mínojský	k2eAgFnSc1d1	Mínojská
civilizace	civilizace	k1gFnSc1	civilizace
a	a	k8xC	a
následovná	následovný	k2eAgFnSc1d1	následovná
civilizací	civilizace	k1gFnSc7	civilizace
mykénskou	mykénský	k2eAgFnSc7d1	mykénská
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1200	[number]	k4	1200
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jejich	jejich	k3xOp3gFnPc6	jejich
troskách	troska	k1gFnPc6	troska
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
rozvíjela	rozvíjet	k5eAaImAgFnS	rozvíjet
v	v	k7c6	v
řeckých	řecký	k2eAgInPc6d1	řecký
městských	městský	k2eAgInPc6d1	městský
státech	stát	k1gInPc6	stát
od	od	k7c2	od
roku	rok	k1gInSc2	rok
800	[number]	k4	800
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
antická	antický	k2eAgFnSc1d1	antická
civilizace	civilizace	k1gFnSc1	civilizace
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
počátek	počátek	k1gInSc4	počátek
historie	historie	k1gFnSc2	historie
starověkého	starověký	k2eAgNnSc2d1	starověké
Řecka	Řecko	k1gNnSc2	Řecko
se	se	k3xPyFc4	se
tradičně	tradičně	k6eAd1	tradičně
považuje	považovat	k5eAaImIp3nS	považovat
rok	rok	k1gInSc4	rok
776	[number]	k4	776
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgFnP	konat
první	první	k4xOgFnPc1	první
antické	antický	k2eAgFnPc1d1	antická
olympijské	olympijský	k2eAgFnPc1d1	olympijská
hry	hra	k1gFnPc1	hra
<g/>
.	.	kIx.	.
</s>
<s>
Rozkvět	rozkvět	k1gInSc1	rozkvět
těchto	tento	k3xDgInPc2	tento
států	stát	k1gInPc2	stát
byl	být	k5eAaImAgInS	být
spojen	spojit	k5eAaPmNgInS	spojit
také	také	k6eAd1	také
s	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
.	.	kIx.	.
</s>
<s>
Období	období	k1gNnSc1	období
největšího	veliký	k2eAgNnSc2d3	veliký
rozšíření	rozšíření	k1gNnSc2	rozšíření
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
řecká	řecký	k2eAgFnSc1d1	řecká
kultura	kultura	k1gFnSc1	kultura
v	v	k7c6	v
období	období	k1gNnSc6	období
helénismu	helénismus	k1gInSc2	helénismus
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc4	který
následovalo	následovat	k5eAaImAgNnS	následovat
po	po	k7c6	po
vládě	vláda	k1gFnSc6	vláda
Alexandra	Alexandr	k1gMnSc2	Alexandr
Velikého	veliký	k2eAgMnSc2d1	veliký
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
řecká	řecký	k2eAgFnSc1d1	řecká
kultura	kultura	k1gFnSc1	kultura
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
po	po	k7c6	po
celém	celý	k2eAgNnSc6d1	celé
středomoří	středomoří	k1gNnSc6	středomoří
a	a	k8xC	a
blízkém	blízký	k2eAgInSc6d1	blízký
východě	východ	k1gInSc6	východ
<g/>
.	.	kIx.	.
</s>
<s>
Politickým	politický	k2eAgMnSc7d1	politický
nástupcem	nástupce	k1gMnSc7	nástupce
říše	říš	k1gFnSc2	říš
Alexandra	Alexandra	k1gFnSc1	Alexandra
Velikého	veliký	k2eAgInSc2d1	veliký
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
Římské	římský	k2eAgNnSc1d1	římské
císařství	císařství	k1gNnSc1	císařství
<g/>
,	,	kIx,	,
založené	založený	k2eAgNnSc1d1	založené
roku	rok	k1gInSc2	rok
27	[number]	k4	27
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
císařem	císař	k1gMnSc7	císař
Augustem	August	k1gMnSc7	August
za	za	k7c2	za
jehož	jehož	k3xOyRp3gFnSc2	jehož
vlády	vláda	k1gFnSc2	vláda
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
a	a	k8xC	a
také	také	k9	také
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
šířit	šířit	k5eAaImF	šířit
křesťanství	křesťanství	k1gNnSc4	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Římská	římský	k2eAgFnSc1d1	římská
říše	říše	k1gFnSc1	říše
ovládala	ovládat	k5eAaImAgFnS	ovládat
celé	celý	k2eAgNnSc4d1	celé
středomoří	středomoří	k1gNnSc4	středomoří
a	a	k8xC	a
západní	západní	k2eAgFnSc4d1	západní
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Latina	latina	k1gFnSc1	latina
úřední	úřední	k2eAgFnSc1d1	úřední
řeč	řeč	k1gFnSc1	řeč
Římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
základem	základ	k1gInSc7	základ
rodiny	rodina	k1gFnSc2	rodina
románských	románský	k2eAgInPc2d1	románský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
římské	římský	k2eAgNnSc1d1	římské
právo	právo	k1gNnSc1	právo
položilo	položit	k5eAaPmAgNnS	položit
základ	základ	k1gInSc4	základ
státovědy	státověda	k1gFnSc2	státověda
či	či	k8xC	či
evropského	evropský	k2eAgNnSc2d1	Evropské
občanského	občanský	k2eAgNnSc2d1	občanské
práva	právo	k1gNnSc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Římská	římský	k2eAgFnSc1d1	římská
říše	říše	k1gFnSc1	říše
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
roku	rok	k1gInSc2	rok
476	[number]	k4	476
během	během	k7c2	během
stěhování	stěhování	k1gNnSc2	stěhování
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
troskách	troska	k1gFnPc6	troska
antické	antický	k2eAgFnSc2d1	antická
civilizace	civilizace	k1gFnSc2	civilizace
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
převzala	převzít	k5eAaPmAgFnS	převzít
především	především	k6eAd1	především
řeckou	řecký	k2eAgFnSc4d1	řecká
filosofii	filosofie	k1gFnSc4	filosofie
<g/>
,	,	kIx,	,
římské	římský	k2eAgNnSc4d1	římské
právo	právo	k1gNnSc4	právo
a	a	k8xC	a
křesťanství	křesťanství	k1gNnSc4	křesťanství
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
postupně	postupně	k6eAd1	postupně
vyvíjet	vyvíjet	k5eAaImF	vyvíjet
naše	náš	k3xOp1gFnSc1	náš
současná	současný	k2eAgFnSc1d1	současná
západní	západní	k2eAgFnSc1d1	západní
civilizace	civilizace	k1gFnSc1	civilizace
<g/>
,	,	kIx,	,
živořící	živořící	k2eAgFnSc1d1	živořící
prvních	první	k4xOgNnPc6	první
1000	[number]	k4	1000
let	léto	k1gNnPc2	léto
na	na	k7c6	na
periferii	periferie	k1gFnSc6	periferie
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
byly	být	k5eAaImAgFnP	být
vedle	vedle	k7c2	vedle
zbytkové	zbytkový	k2eAgFnSc2d1	zbytková
Východořímské	východořímský	k2eAgFnSc2d1	Východořímská
později	pozdě	k6eAd2	pozdě
Byzantské	byzantský	k2eAgFnSc2d1	byzantská
říše	říš	k1gFnSc2	říš
zakládány	zakládán	k2eAgInPc4d1	zakládán
krátkodobé	krátkodobý	k2eAgInPc4d1	krátkodobý
barbarské	barbarský	k2eAgInPc4d1	barbarský
státy	stát	k1gInPc4	stát
<g/>
:	:	kIx,	:
Franská	franský	k2eAgFnSc1d1	Franská
říše	říše	k1gFnSc1	říše
v	v	k7c6	v
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
anglosaské	anglosaský	k2eAgInPc1d1	anglosaský
státy	stát	k1gInPc1	stát
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
v	v	k7c6	v
5	[number]	k4	5
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
nebo	nebo	k8xC	nebo
Dánsko	Dánsko	k1gNnSc1	Dánsko
roku	rok	k1gInSc2	rok
800	[number]	k4	800
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
přistěhování	přistěhování	k1gNnSc6	přistěhování
Slovanů	Slovan	k1gMnPc2	Slovan
do	do	k7c2	do
střední	střední	k2eAgFnSc2d1	střední
a	a	k8xC	a
jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
Evropy	Evropa	k1gFnSc2	Evropa
vznikaly	vznikat	k5eAaImAgInP	vznikat
i	i	k9	i
slovanské	slovanský	k2eAgInPc1d1	slovanský
státy	stát	k1gInPc1	stát
<g/>
:	:	kIx,	:
Velkomoravská	velkomoravský	k2eAgFnSc1d1	Velkomoravská
říše	říše	k1gFnSc1	říše
roku	rok	k1gInSc2	rok
833	[number]	k4	833
<g/>
,	,	kIx,	,
Bulharská	bulharský	k2eAgFnSc1d1	bulharská
říše	říše	k1gFnSc1	říše
v	v	k7c6	v
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
Kyjevská	kyjevský	k2eAgFnSc1d1	Kyjevská
Rus	Rus	k1gFnSc1	Rus
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
<g/>
,	,	kIx,	,
Srbsko	Srbsko	k1gNnSc1	Srbsko
<g/>
,	,	kIx,	,
České	český	k2eAgNnSc1d1	české
knížectví	knížectví	k1gNnSc1	knížectví
v	v	k7c6	v
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
a	a	k8xC	a
Polsko	Polsko	k1gNnSc1	Polsko
roku	rok	k1gInSc2	rok
960	[number]	k4	960
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgNnSc1d1	české
knížectví	knížectví	k1gNnSc1	knížectví
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1004	[number]	k4	1004
začlenilo	začlenit	k5eAaPmAgNnS	začlenit
do	do	k7c2	do
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1198	[number]	k4	1198
bylo	být	k5eAaImAgNnS	být
povýšeno	povýšit	k5eAaPmNgNnS	povýšit
na	na	k7c4	na
České	český	k2eAgNnSc4d1	české
království	království	k1gNnSc4	království
<g/>
.	.	kIx.	.
</s>
<s>
Rozmach	rozmach	k1gInSc1	rozmach
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
zděděné	zděděný	k2eAgFnSc2d1	zděděná
po	po	k7c6	po
antice	antika	k1gFnSc6	antika
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
charakterizován	charakterizovat	k5eAaBmNgInS	charakterizovat
vznikem	vznik	k1gInSc7	vznik
papežského	papežský	k2eAgInSc2d1	papežský
státu	stát	k1gInSc2	stát
v	v	k7c6	v
8	[number]	k4	8
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Dočasný	dočasný	k2eAgInSc1d1	dočasný
propad	propad	k1gInSc1	propad
raně	raně	k6eAd1	raně
středověké	středověký	k2eAgFnSc2d1	středověká
Evropy	Evropa	k1gFnSc2	Evropa
byl	být	k5eAaImAgInS	být
způsoben	způsobit	k5eAaPmNgInS	způsobit
pronikáním	pronikání	k1gNnSc7	pronikání
cizorodých	cizorodý	k2eAgInPc2d1	cizorodý
prvků	prvek	k1gInPc2	prvek
<g/>
:	:	kIx,	:
Arabů	Arab	k1gMnPc2	Arab
na	na	k7c4	na
Pyrenejský	pyrenejský	k2eAgInSc4d1	pyrenejský
poloostrov	poloostrov	k1gInSc4	poloostrov
od	od	k7c2	od
roku	rok	k1gInSc2	rok
711	[number]	k4	711
a	a	k8xC	a
Maďarů	Maďar	k1gMnPc2	Maďar
do	do	k7c2	do
uherských	uherský	k2eAgFnPc2d1	uherská
nížin	nížina	k1gFnPc2	nížina
v	v	k7c6	v
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
udrželi	udržet	k5eAaPmAgMnP	udržet
a	a	k8xC	a
stali	stát	k5eAaPmAgMnP	stát
se	se	k3xPyFc4	se
nedílnou	dílný	k2eNgFnSc7d1	nedílná
součástí	součást	k1gFnSc7	součást
evropské	evropský	k2eAgFnSc2d1	Evropská
civilizace	civilizace	k1gFnSc2	civilizace
především	především	k9	především
díky	díky	k7c3	díky
včasnému	včasný	k2eAgNnSc3d1	včasné
přijetí	přijetí	k1gNnSc3	přijetí
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
nastal	nastat	k5eAaPmAgInS	nastat
trvalý	trvalý	k2eAgInSc1d1	trvalý
tisíciletý	tisíciletý	k2eAgInSc1d1	tisíciletý
demografický	demografický	k2eAgInSc1d1	demografický
<g/>
,	,	kIx,	,
hospodářský	hospodářský	k2eAgInSc1d1	hospodářský
a	a	k8xC	a
kulturní	kulturní	k2eAgInSc1d1	kulturní
vzestup	vzestup	k1gInSc1	vzestup
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
Evropě	Evropa	k1gFnSc3	Evropa
přinesl	přinést	k5eAaPmAgInS	přinést
nakonec	nakonec	k6eAd1	nakonec
400	[number]	k4	400
let	léto	k1gNnPc2	léto
trvající	trvající	k2eAgFnSc4d1	trvající
vládu	vláda	k1gFnSc4	vláda
nad	nad	k7c7	nad
planetou	planeta	k1gFnSc7	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Vytlačením	vytlačení	k1gNnSc7	vytlačení
Arabů	Arab	k1gMnPc2	Arab
z	z	k7c2	z
Pyrenejského	pyrenejský	k2eAgInSc2d1	pyrenejský
poloostrova	poloostrov	k1gInSc2	poloostrov
bylo	být	k5eAaImAgNnS	být
umožněno	umožnit	k5eAaPmNgNnS	umožnit
vytvořit	vytvořit	k5eAaPmF	vytvořit
Portugalsko	Portugalsko	k1gNnSc4	Portugalsko
roku	rok	k1gInSc2	rok
1139	[number]	k4	1139
a	a	k8xC	a
Španělsko	Španělsko	k1gNnSc1	Španělsko
sjednocením	sjednocení	k1gNnSc7	sjednocení
Aragonie	Aragonie	k1gFnSc2	Aragonie
a	a	k8xC	a
Kastilie	Kastilie	k1gFnSc2	Kastilie
roku	rok	k1gInSc2	rok
1479	[number]	k4	1479
<g/>
.	.	kIx.	.
</s>
<s>
Dobytím	dobytí	k1gNnSc7	dobytí
arabské	arabský	k2eAgFnSc2d1	arabská
Granady	Granada	k1gFnSc2	Granada
roku	rok	k1gInSc2	rok
1492	[number]	k4	1492
bylo	být	k5eAaImAgNnS	být
vytlačení	vytlačení	k1gNnSc1	vytlačení
Arabů	Arab	k1gMnPc2	Arab
z	z	k7c2	z
Evropy	Evropa	k1gFnSc2	Evropa
ukončeno	ukončen	k2eAgNnSc1d1	ukončeno
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
Franské	franský	k2eAgFnSc2d1	Franská
říše	říš	k1gFnSc2	říš
roku	rok	k1gInSc2	rok
843	[number]	k4	843
byly	být	k5eAaImAgInP	být
položeny	položen	k2eAgInPc1d1	položen
základy	základ	k1gInPc1	základ
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
vznikaly	vznikat	k5eAaImAgInP	vznikat
mezitím	mezitím	k6eAd1	mezitím
od	od	k7c2	od
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
městské	městský	k2eAgInPc1d1	městský
státy	stát	k1gInPc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
Postupným	postupný	k2eAgInSc7d1	postupný
rozpadem	rozpad	k1gInSc7	rozpad
německé	německý	k2eAgFnSc2d1	německá
části	část	k1gFnSc2	část
Franské	franský	k2eAgFnSc2d1	Franská
říše	říš	k1gFnSc2	říš
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
roku	rok	k1gInSc2	rok
1156	[number]	k4	1156
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1291	[number]	k4	1291
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
do	do	k7c2	do
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
Prusko	Prusko	k1gNnSc1	Prusko
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
odporu	odpor	k1gInSc2	odpor
proti	proti	k7c3	proti
vnějšímu	vnější	k2eAgNnSc3d1	vnější
nepříteli	nepřítel	k1gMnSc3	nepřítel
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
velké	velký	k2eAgFnPc1d1	velká
mezistátní	mezistátní	k2eAgFnPc1d1	mezistátní
unie	unie	k1gFnPc1	unie
<g/>
:	:	kIx,	:
polsko-litevská	polskoitevský	k2eAgFnSc1d1	polsko-litevská
unie	unie	k1gFnSc1	unie
roku	rok	k1gInSc2	rok
1386	[number]	k4	1386
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
vlastními	vlastní	k2eAgInPc7d1	vlastní
nedostatky	nedostatek	k1gInPc7	nedostatek
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1772	[number]	k4	1772
<g/>
–	–	k?	–
<g/>
1795	[number]	k4	1795
<g/>
,	,	kIx,	,
a	a	k8xC	a
kalmarská	kalmarský	k2eAgFnSc1d1	kalmarský
v	v	k7c6	v
letech	let	k1gInPc6	let
1397	[number]	k4	1397
<g/>
–	–	k?	–
<g/>
1523	[number]	k4	1523
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
začali	začít	k5eAaPmAgMnP	začít
pronikat	pronikat	k5eAaImF	pronikat
na	na	k7c4	na
Balkán	Balkán	k1gInSc4	Balkán
Osmanští	osmanský	k2eAgMnPc1d1	osmanský
Turci	Turek	k1gMnPc1	Turek
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1389	[number]	k4	1389
porazili	porazit	k5eAaPmAgMnP	porazit
Srby	Srb	k1gMnPc7	Srb
na	na	k7c6	na
Kosově	Kosův	k2eAgNnSc6d1	Kosovo
poli	pole	k1gNnSc6	pole
a	a	k8xC	a
roku	rok	k1gInSc6	rok
1453	[number]	k4	1453
vyvrátili	vyvrátit	k5eAaPmAgMnP	vyvrátit
Byzantskou	byzantský	k2eAgFnSc4d1	byzantská
říši	říše	k1gFnSc4	říše
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1415	[number]	k4	1415
zahájili	zahájit	k5eAaPmAgMnP	zahájit
Portugalci	Portugalec	k1gMnPc1	Portugalec
dobytím	dobytí	k1gNnSc7	dobytí
africké	africký	k2eAgFnSc2d1	africká
Ceuty	Ceuta	k1gFnSc2	Ceuta
evropskou	evropský	k2eAgFnSc4d1	Evropská
expanzi	expanze	k1gFnSc4	expanze
do	do	k7c2	do
zámoří	zámoří	k1gNnSc2	zámoří
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
expanze	expanze	k1gFnSc1	expanze
zahajuje	zahajovat	k5eAaImIp3nS	zahajovat
novověk	novověk	k1gInSc4	novověk
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1492	[number]	k4	1492
byla	být	k5eAaImAgFnS	být
objevena	objeven	k2eAgFnSc1d1	objevena
Amerika	Amerika	k1gFnSc1	Amerika
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1498	[number]	k4	1498
přistáli	přistát	k5eAaPmAgMnP	přistát
Portugalci	Portugalec	k1gMnPc1	Portugalec
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Planeta	planeta	k1gFnSc1	planeta
byla	být	k5eAaImAgFnS	být
poprvé	poprvé	k6eAd1	poprvé
obepluta	obeplout	k5eAaPmNgFnS	obeplout
v	v	k7c6	v
letech	let	k1gInPc6	let
1519	[number]	k4	1519
<g/>
–	–	k?	–
<g/>
1522	[number]	k4	1522
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
definitivně	definitivně	k6eAd1	definitivně
ukončila	ukončit	k5eAaPmAgFnS	ukončit
středověk	středověk	k1gInSc4	středověk
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc7	jeho
pojetí	pojetí	k1gNnSc4	pojetí
společnosti	společnost	k1gFnSc2	společnost
renesance	renesance	k1gFnSc2	renesance
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1526	[number]	k4	1526
zahájili	zahájit	k5eAaPmAgMnP	zahájit
Habsburkové	Habsburk	k1gMnPc1	Habsburk
budování	budování	k1gNnSc2	budování
mnohonárodnostního	mnohonárodnostní	k2eAgNnSc2d1	mnohonárodnostní
impéria	impérium	k1gNnSc2	impérium
kolem	kolem	k7c2	kolem
rakouských	rakouský	k2eAgFnPc2d1	rakouská
zemí	zem	k1gFnPc2	zem
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
do	do	k7c2	do
něhož	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
i	i	k9	i
řada	řada	k1gFnSc1	řada
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
samostatných	samostatný	k2eAgFnPc2d1	samostatná
zemí	zem	k1gFnPc2	zem
(	(	kIx(	(
<g/>
České	český	k2eAgFnSc2d1	Česká
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
<g/>
,	,	kIx,	,
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgFnPc1d1	Česká
země	zem	k1gFnPc1	zem
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Habsburků	Habsburk	k1gMnPc2	Habsburk
ztratily	ztratit	k5eAaPmAgInP	ztratit
území	území	k1gNnSc4	území
Slezska	Slezsko	k1gNnSc2	Slezsko
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1742	[number]	k4	1742
připadlo	připadnout	k5eAaPmAgNnS	připadnout
Prusku	Prusko	k1gNnSc3	Prusko
<g/>
.	.	kIx.	.
</s>
<s>
Finálním	finální	k2eAgInSc7d1	finální
produktem	produkt	k1gInSc7	produkt
bylo	být	k5eAaImAgNnS	být
roku	rok	k1gInSc2	rok
1867	[number]	k4	1867
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc4	Rakousko-Uhersko
<g/>
.	.	kIx.	.
</s>
<s>
Novým	nový	k2eAgInSc7d1	nový
státem	stát	k1gInSc7	stát
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
Nizozemí	Nizozemí	k1gNnSc1	Nizozemí
<g/>
,	,	kIx,	,
vzniklé	vzniklý	k2eAgNnSc1d1	vzniklé
z	z	k7c2	z
revoluce	revoluce	k1gFnSc2	revoluce
proti	proti	k7c3	proti
Španělsku	Španělsko	k1gNnSc3	Španělsko
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1565	[number]	k4	1565
<g/>
–	–	k?	–
<g/>
1581	[number]	k4	1581
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1707	[number]	k4	1707
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
propojením	propojení	k1gNnSc7	propojení
Anglie	Anglie	k1gFnSc2	Anglie
a	a	k8xC	a
Skotska	Skotsko	k1gNnSc2	Skotsko
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
připojením	připojení	k1gNnSc7	připojení
Irska	Irsko	k1gNnSc2	Irsko
Spojené	spojený	k2eAgNnSc4d1	spojené
království	království	k1gNnSc4	království
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
15	[number]	k4	15
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
vytvářely	vytvářet	k5eAaImAgFnP	vytvářet
země	zem	k1gFnPc4	zem
jako	jako	k9	jako
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
,	,	kIx,	,
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
nebo	nebo	k8xC	nebo
Anglie	Anglie	k1gFnSc1	Anglie
velké	velká	k1gFnSc2	velká
koloniální	koloniální	k2eAgNnSc4d1	koloniální
panství	panství	k1gNnSc4	panství
s	s	k7c7	s
rozsáhlými	rozsáhlý	k2eAgFnPc7d1	rozsáhlá
državami	država	k1gFnPc7	država
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
Americe	Amerika	k1gFnSc6	Amerika
a	a	k8xC	a
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
revoluce	revoluce	k1gFnSc1	revoluce
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
propukla	propuknout	k5eAaPmAgFnS	propuknout
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
a	a	k8xC	a
znamenala	znamenat	k5eAaImAgFnS	znamenat
ještě	ještě	k6eAd1	ještě
větší	veliký	k2eAgInSc4d2	veliký
blahobyt	blahobyt	k1gInSc4	blahobyt
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
způsobil	způsobit	k5eAaPmAgInS	způsobit
velký	velký	k2eAgInSc4d1	velký
nárůst	nárůst	k1gInSc4	nárůst
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
francouzské	francouzský	k2eAgFnSc6d1	francouzská
revoluci	revoluce	k1gFnSc6	revoluce
1789	[number]	k4	1789
<g/>
–	–	k?	–
<g/>
1794	[number]	k4	1794
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
v	v	k7c6	v
několika	několik	k4yIc6	několik
vlnách	vlna	k1gFnPc6	vlna
státy	stát	k1gInPc1	stát
na	na	k7c6	na
národním	národní	k2eAgInSc6d1	národní
principu	princip	k1gInSc6	princip
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
revoluce	revoluce	k1gFnSc1	revoluce
přinesla	přinést	k5eAaPmAgFnS	přinést
<g/>
:	:	kIx,	:
1822	[number]	k4	1822
Řecko	Řecko	k1gNnSc1	Řecko
svržením	svržení	k1gNnSc7	svržení
turecké	turecký	k2eAgFnSc2d1	turecká
nadvlády	nadvláda	k1gFnSc2	nadvláda
<g/>
,	,	kIx,	,
1830	[number]	k4	1830
Belgie	Belgie	k1gFnSc1	Belgie
oddělení	oddělení	k1gNnSc2	oddělení
od	od	k7c2	od
Nizozemí	Nizozemí	k1gNnSc2	Nizozemí
<g/>
,	,	kIx,	,
1859	[number]	k4	1859
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
sloučením	sloučení	k1gNnSc7	sloučení
dvou	dva	k4xCgMnPc2	dva
tureckých	turecký	k2eAgMnPc2d1	turecký
vazalů	vazal	k1gMnPc2	vazal
Valašska	Valašsko	k1gNnSc2	Valašsko
a	a	k8xC	a
Moldávie	Moldávie	k1gFnSc2	Moldávie
<g/>
,	,	kIx,	,
1861	[number]	k4	1861
Itálie	Itálie	k1gFnSc2	Itálie
sjednocením	sjednocení	k1gNnSc7	sjednocení
drobných	drobná	k1gFnPc2	drobná
italských	italský	k2eAgInPc2d1	italský
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
1871	[number]	k4	1871
Německo	Německo	k1gNnSc1	Německo
obdobným	obdobný	k2eAgInSc7d1	obdobný
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
sjednocením	sjednocení	k1gNnSc7	sjednocení
kolem	kolem	k7c2	kolem
Pruska	Prusko	k1gNnSc2	Prusko
<g/>
,	,	kIx,	,
1905	[number]	k4	1905
Norsko	Norsko	k1gNnSc1	Norsko
oddělením	oddělení	k1gNnSc7	oddělení
od	od	k7c2	od
Švédska	Švédsko	k1gNnSc2	Švédsko
<g/>
,	,	kIx,	,
1908	[number]	k4	1908
obnovené	obnovený	k2eAgNnSc1d1	obnovené
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
zrušením	zrušení	k1gNnSc7	zrušení
vazalství	vazalství	k1gNnPc2	vazalství
na	na	k7c6	na
Turecku	Turecko	k1gNnSc6	Turecko
a	a	k8xC	a
1912	[number]	k4	1912
Albánie	Albánie	k1gFnSc1	Albánie
svržením	svržení	k1gNnSc7	svržení
turecké	turecký	k2eAgFnSc2d1	turecká
nadvlády	nadvláda	k1gFnSc2	nadvláda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
1914	[number]	k4	1914
<g/>
–	–	k?	–
<g/>
1918	[number]	k4	1918
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
další	další	k2eAgFnSc1d1	další
generace	generace	k1gFnSc1	generace
evropských	evropský	k2eAgInPc2d1	evropský
států	stát	k1gInPc2	stát
<g/>
:	:	kIx,	:
1917	[number]	k4	1917
Finsko	Finsko	k1gNnSc1	Finsko
oddělením	oddělení	k1gNnSc7	oddělení
od	od	k7c2	od
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
1918	[number]	k4	1918
Polsko	Polsko	k1gNnSc1	Polsko
obnovením	obnovení	k1gNnSc7	obnovení
z	z	k7c2	z
částí	část	k1gFnPc2	část
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
Československo	Československo	k1gNnSc4	Československo
<g/>
,	,	kIx,	,
Rakousko	Rakousko	k1gNnSc4	Rakousko
a	a	k8xC	a
Maďarsko	Maďarsko	k1gNnSc4	Maďarsko
rozpadem	rozpad	k1gInSc7	rozpad
Rakousko-Uherska	Rakousko-Uhersko	k1gNnSc2	Rakousko-Uhersko
<g/>
,	,	kIx,	,
Estonsko	Estonsko	k1gNnSc1	Estonsko
<g/>
,	,	kIx,	,
Lotyšsko	Lotyšsko	k1gNnSc1	Lotyšsko
a	a	k8xC	a
Litva	Litva	k1gFnSc1	Litva
oddělením	oddělení	k1gNnSc7	oddělení
od	od	k7c2	od
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
Jugoslávie	Jugoslávie	k1gFnSc1	Jugoslávie
sloučením	sloučení	k1gNnSc7	sloučení
Srbska	Srbsko	k1gNnSc2	Srbsko
<g/>
,	,	kIx,	,
Černé	Černé	k2eAgFnSc2d1	Černé
Hory	hora	k1gFnSc2	hora
a	a	k8xC	a
části	část	k1gFnSc2	část
Rakousko-Uherska	Rakousko-Uhersk	k1gInSc2	Rakousko-Uhersk
<g/>
,	,	kIx,	,
1921	[number]	k4	1921
Irsko	Irsko	k1gNnSc1	Irsko
oddělením	oddělení	k1gNnSc7	oddělení
od	od	k7c2	od
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
1922	[number]	k4	1922
Sovětský	sovětský	k2eAgInSc4d1	sovětský
svaz	svaz	k1gInSc4	svaz
sloučením	sloučení	k1gNnSc7	sloučení
sovětských	sovětský	k2eAgFnPc2d1	sovětská
republik	republika	k1gFnPc2	republika
<g/>
,	,	kIx,	,
vzniklých	vzniklý	k2eAgFnPc2d1	vzniklá
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
Říjnové	říjnový	k2eAgFnSc2d1	říjnová
revoluce	revoluce	k1gFnSc2	revoluce
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
znamenala	znamenat	k5eAaImAgFnS	znamenat
podlomení	podlomení	k1gNnSc3	podlomení
evropské	evropský	k2eAgFnSc2d1	Evropská
vlády	vláda	k1gFnSc2	vláda
nad	nad	k7c7	nad
planetou	planeta	k1gFnSc7	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
1939	[number]	k4	1939
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
definitivně	definitivně	k6eAd1	definitivně
zničila	zničit	k5eAaPmAgFnS	zničit
evropskou	evropský	k2eAgFnSc4d1	Evropská
moc	moc	k1gFnSc4	moc
<g/>
,	,	kIx,	,
během	během	k7c2	během
studené	studený	k2eAgFnSc2d1	studená
války	válka	k1gFnSc2	válka
1946	[number]	k4	1946
<g/>
–	–	k?	–
<g/>
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
Evropa	Evropa	k1gFnSc1	Evropa
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
významné	významný	k2eAgInPc4d1	významný
politické	politický	k2eAgInPc4d1	politický
a	a	k8xC	a
ekonomické	ekonomický	k2eAgInPc4d1	ekonomický
bloky	blok	k1gInPc4	blok
<g/>
:	:	kIx,	:
východní	východní	k2eAgInSc4d1	východní
blok	blok	k1gInSc4	blok
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
ovládaný	ovládaný	k2eAgMnSc1d1	ovládaný
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c4	na
demokratické	demokratický	k2eAgFnPc4d1	demokratická
země	zem	k1gFnPc4	zem
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Československo	Československo	k1gNnSc1	Československo
přišlo	přijít	k5eAaPmAgNnS	přijít
o	o	k7c6	o
území	území	k1gNnSc6	území
Podkarpatské	podkarpatský	k2eAgFnSc2d1	Podkarpatská
Rusi	Rus	k1gFnSc2	Rus
<g/>
.	.	kIx.	.
</s>
<s>
Nových	Nových	k2eAgInPc2d1	Nových
států	stát	k1gInPc2	stát
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
už	už	k6eAd1	už
jen	jen	k9	jen
několik	několik	k4yIc4	několik
<g/>
:	:	kIx,	:
1944	[number]	k4	1944
Island	Island	k1gInSc1	Island
oddělením	oddělení	k1gNnSc7	oddělení
od	od	k7c2	od
Dánska	Dánsko	k1gNnSc2	Dánsko
<g/>
,	,	kIx,	,
1949	[number]	k4	1949
dva	dva	k4xCgInPc1	dva
německé	německý	k2eAgInPc1d1	německý
státy	stát	k1gInPc1	stát
obnovením	obnovení	k1gNnSc7	obnovení
v	v	k7c6	v
okupačních	okupační	k2eAgInPc6d1	okupační
pásmech	pásmo	k1gNnPc6	pásmo
<g/>
,	,	kIx,	,
1964	[number]	k4	1964
Malta	Malta	k1gFnSc1	Malta
vyhlášením	vyhlášení	k1gNnSc7	vyhlášení
nezávislosti	nezávislost	k1gFnSc2	nezávislost
na	na	k7c6	na
Británii	Británie	k1gFnSc6	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
významným	významný	k2eAgInSc7d1	významný
procesem	proces	k1gInSc7	proces
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
Evropě	Evropa	k1gFnSc6	Evropa
integrační	integrační	k2eAgInSc1d1	integrační
proces	proces	k1gInSc1	proces
<g/>
,	,	kIx,	,
zahájený	zahájený	k2eAgMnSc1d1	zahájený
vytvořením	vytvoření	k1gNnSc7	vytvoření
Beneluxu	Benelux	k1gInSc2	Benelux
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
vznikem	vznik	k1gInSc7	vznik
Severoatlantické	severoatlantický	k2eAgFnSc2d1	Severoatlantická
aliance	aliance	k1gFnSc2	aliance
(	(	kIx(	(
<g/>
NATO	NATO	kA	NATO
<g/>
)	)	kIx)	)
1949	[number]	k4	1949
<g/>
,	,	kIx,	,
Evropského	evropský	k2eAgNnSc2d1	Evropské
hospodářského	hospodářský	k2eAgNnSc2d1	hospodářské
společenství	společenství	k1gNnSc2	společenství
1957	[number]	k4	1957
a	a	k8xC	a
finální	finální	k2eAgFnSc2d1	finální
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
Integrovaná	integrovaný	k2eAgFnSc1d1	integrovaná
Evropa	Evropa	k1gFnSc1	Evropa
měla	mít	k5eAaImAgFnS	mít
původně	původně	k6eAd1	původně
vytvořit	vytvořit	k5eAaPmF	vytvořit
znovu	znovu	k6eAd1	znovu
evropské	evropský	k2eAgNnSc4d1	Evropské
mocenské	mocenský	k2eAgNnSc4d1	mocenské
centrum	centrum	k1gNnSc4	centrum
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zatím	zatím	k6eAd1	zatím
je	být	k5eAaImIp3nS	být
vývoj	vývoj	k1gInSc4	vývoj
toho	ten	k3xDgNnSc2	ten
dalek	dalek	k2eAgInSc1d1	dalek
<g/>
.	.	kIx.	.
</s>
<s>
Klíčový	klíčový	k2eAgMnSc1d1	klíčový
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
rok	rok	k1gInSc4	rok
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
destrukci	destrukce	k1gFnSc3	destrukce
komunistických	komunistický	k2eAgFnPc2d1	komunistická
totalit	totalita	k1gFnPc2	totalita
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
,	,	kIx,	,
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
<g/>
,	,	kIx,	,
Československu	Československo	k1gNnSc6	Československo
<g/>
,	,	kIx,	,
Bulharsku	Bulharsko	k1gNnSc6	Bulharsko
a	a	k8xC	a
Rumunsku	Rumunsko	k1gNnSc6	Rumunsko
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
v	v	k7c6	v
Albánii	Albánie	k1gFnSc6	Albánie
<g/>
.	.	kIx.	.
</s>
<s>
Východní	východní	k2eAgInSc1d1	východní
blok	blok	k1gInSc1	blok
se	se	k3xPyFc4	se
rozpadl	rozpadnout	k5eAaPmAgInS	rozpadnout
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
koncem	konec	k1gInSc7	konec
studené	studený	k2eAgFnSc2d1	studená
války	válka	k1gFnSc2	válka
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
i	i	k9	i
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
znovusjednotit	znovusjednotit	k5eAaImF	znovusjednotit
Německo	Německo	k1gNnSc1	Německo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
se	se	k3xPyFc4	se
rozpadl	rozpadnout	k5eAaPmAgInS	rozpadnout
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
nahrazen	nahradit	k5eAaPmNgInS	nahradit
Společenstvím	společenství	k1gNnSc7	společenství
nezávislých	závislý	k2eNgInPc2d1	nezávislý
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
vývoj	vývoj	k1gInSc1	vývoj
se	se	k3xPyFc4	se
neobešel	obešet	k5eNaPmAgInS	obešet
bez	bez	k7c2	bez
konfliktů	konflikt	k1gInPc2	konflikt
například	například	k6eAd1	například
v	v	k7c6	v
Moldávii	Moldávie	k1gFnSc6	Moldávie
<g/>
,	,	kIx,	,
pobaltských	pobaltský	k2eAgInPc6d1	pobaltský
státech	stát	k1gInPc6	stát
nebo	nebo	k8xC	nebo
na	na	k7c6	na
Kavkaze	Kavkaz	k1gInSc6	Kavkaz
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
začal	začít	k5eAaPmAgInS	začít
i	i	k9	i
rozpad	rozpad	k1gInSc1	rozpad
komunistické	komunistický	k2eAgFnSc2d1	komunistická
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
a	a	k8xC	a
následné	následný	k2eAgInPc1d1	následný
konflikty	konflikt	k1gInPc1	konflikt
opět	opět	k6eAd1	opět
přinesly	přinést	k5eAaPmAgInP	přinést
válku	válka	k1gFnSc4	válka
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Rozpadem	rozpad	k1gInSc7	rozpad
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
státy	stát	k1gInPc1	stát
následnické	následnický	k2eAgInPc1d1	následnický
<g/>
:	:	kIx,	:
Slovinsko	Slovinsko	k1gNnSc1	Slovinsko
<g/>
,	,	kIx,	,
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
<g/>
,	,	kIx,	,
Bosna	Bosna	k1gFnSc1	Bosna
a	a	k8xC	a
Hercegovina	Hercegovina	k1gFnSc1	Hercegovina
<g/>
,	,	kIx,	,
Makedonie	Makedonie	k1gFnSc1	Makedonie
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
Srbsko	Srbsko	k1gNnSc1	Srbsko
<g/>
,	,	kIx,	,
Černá	černý	k2eAgFnSc1d1	černá
Hora	hora	k1gFnSc1	hora
a	a	k8xC	a
Kosovská	kosovský	k2eAgFnSc1d1	Kosovská
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
se	se	k3xPyFc4	se
bez	bez	k7c2	bez
konfliktů	konflikt	k1gInPc2	konflikt
rozpadlo	rozpadnout	k5eAaPmAgNnS	rozpadnout
Československo	Československo	k1gNnSc1	Československo
na	na	k7c4	na
Česko	Česko	k1gNnSc4	Česko
a	a	k8xC	a
Slovensko	Slovensko	k1gNnSc4	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Demografie	demografie	k1gFnSc2	demografie
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Evropa	Evropa	k1gFnSc1	Evropa
má	mít	k5eAaImIp3nS	mít
718	[number]	k4	718
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
9,6	[number]	k4	9,6
%	%	kIx~	%
světové	světový	k2eAgFnSc2d1	světová
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Hustota	hustota	k1gFnSc1	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
je	být	k5eAaImIp3nS	být
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
,	,	kIx,	,
72	[number]	k4	72
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
/	/	kIx~	/
<g/>
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Evropa	Evropa	k1gFnSc1	Evropa
je	být	k5eAaImIp3nS	být
nejhustěji	husto	k6eAd3	husto
zalidněný	zalidněný	k2eAgInSc4d1	zalidněný
kontinent	kontinent	k1gInSc4	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Současný	současný	k2eAgInSc1d1	současný
stav	stav	k1gInSc1	stav
a	a	k8xC	a
rozmístění	rozmístění	k1gNnSc1	rozmístění
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
je	být	k5eAaImIp3nS	být
výsledkem	výsledek	k1gInSc7	výsledek
složitého	složitý	k2eAgInSc2d1	složitý
vývoje	vývoj	k1gInSc2	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc4d3	veliký
hustotu	hustota	k1gFnSc4	hustota
zalidnění	zalidnění	k1gNnPc2	zalidnění
mají	mít	k5eAaImIp3nP	mít
kromě	kromě	k7c2	kromě
miniaturních	miniaturní	k2eAgInPc2d1	miniaturní
států	stát	k1gInPc2	stát
Monaka	Monako	k1gNnSc2	Monako
<g/>
,	,	kIx,	,
Vatikánu	Vatikán	k1gInSc2	Vatikán
a	a	k8xC	a
Malty	Malta	k1gFnSc2	Malta
<g/>
,	,	kIx,	,
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
(	(	kIx(	(
<g/>
390	[number]	k4	390
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
/	/	kIx~	/
<g/>
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Belgie	Belgie	k1gFnSc1	Belgie
(	(	kIx(	(
<g/>
340	[number]	k4	340
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
/	/	kIx~	/
<g/>
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
a	a	k8xC	a
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
(	(	kIx(	(
<g/>
250	[number]	k4	250
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
/	/	kIx~	/
<g/>
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
nejnižší	nízký	k2eAgFnSc4d3	nejnižší
hustotu	hustota	k1gFnSc4	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
:	:	kIx,	:
Island	Island	k1gInSc1	Island
(	(	kIx(	(
<g/>
3	[number]	k4	3
obyvatelé	obyvatel	k1gMnPc1	obyvatel
<g/>
/	/	kIx~	/
<g/>
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kazachstán	Kazachstán	k1gInSc1	Kazachstán
(	(	kIx(	(
<g/>
4	[number]	k4	4
obyvatelé	obyvatel	k1gMnPc1	obyvatel
<g/>
/	/	kIx~	/
<g/>
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
evropský	evropský	k2eAgMnSc1d1	evropský
stát	stát	k5eAaPmF	stát
jen	jen	k9	jen
malou	malý	k2eAgFnSc7d1	malá
částí	část	k1gFnSc7	část
svého	svůj	k3xOyFgNnSc2	svůj
území	území	k1gNnSc2	území
<g/>
,	,	kIx,	,
a	a	k8xC	a
Rusko	Rusko	k1gNnSc1	Rusko
(	(	kIx(	(
<g/>
8	[number]	k4	8
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
/	/	kIx~	/
<g/>
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gFnSc1	jeho
hustota	hustota	k1gFnSc1	hustota
zalidnění	zalidnění	k1gNnSc1	zalidnění
vypočítaná	vypočítaný	k2eAgFnSc1d1	vypočítaná
na	na	k7c6	na
celé	celá	k1gFnSc6	celá
území	území	k1gNnPc2	území
Ruska	Ruska	k1gFnSc1	Ruska
hustotu	hustota	k1gFnSc4	hustota
v	v	k7c6	v
evropské	evropský	k2eAgFnSc6d1	Evropská
části	část	k1gFnSc6	část
zkresluje	zkreslovat	k5eAaImIp3nS	zkreslovat
<g/>
.	.	kIx.	.
</s>
<s>
Přírůstek	přírůstek	k1gInSc1	přírůstek
činí	činit	k5eAaImIp3nS	činit
0,3	[number]	k4	0,3
%	%	kIx~	%
<g/>
,	,	kIx,	,
porodnost	porodnost	k1gFnSc1	porodnost
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
13	[number]	k4	13
prom	proma	k1gFnPc2	proma
<g/>
.	.	kIx.	.
a	a	k8xC	a
úmrtnost	úmrtnost	k1gFnSc1	úmrtnost
11	[number]	k4	11
prom	proma	k1gFnPc2	proma
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
Evropy	Evropa	k1gFnSc2	Evropa
se	se	k3xPyFc4	se
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
dožívají	dožívat	k5eAaImIp3nP	dožívat
68	[number]	k4	68
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
muži	muž	k1gMnPc1	muž
<g/>
)	)	kIx)	)
a	a	k8xC	a
77	[number]	k4	77
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
ženy	žena	k1gFnPc1	žena
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
tvoří	tvořit	k5eAaImIp3nP	tvořit
hlavně	hlavně	k9	hlavně
tři	tři	k4xCgFnPc1	tři
větve	větev	k1gFnPc1	větev
indoevropské	indoevropský	k2eAgFnSc2d1	indoevropská
jazykové	jazykový	k2eAgFnSc2d1	jazyková
rodiny	rodina	k1gFnSc2	rodina
<g/>
:	:	kIx,	:
slovanská	slovanský	k2eAgFnSc1d1	Slovanská
<g/>
,	,	kIx,	,
germánská	germánský	k2eAgFnSc1d1	germánská
a	a	k8xC	a
románská	románský	k2eAgFnSc1d1	románská
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
národů	národ	k1gInPc2	národ
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
jiným	jiný	k2eAgFnPc3d1	jiná
jazykovým	jazykový	k2eAgFnPc3d1	jazyková
skupinám	skupina	k1gFnPc3	skupina
<g/>
,	,	kIx,	,
např.	např.	kA	např.
uralské	uralský	k2eAgNnSc1d1	uralský
nebo	nebo	k8xC	nebo
afroasijské	afroasijský	k2eAgNnSc1d1	afroasijský
<g/>
.	.	kIx.	.
</s>
<s>
Slovanskými	slovanský	k2eAgInPc7d1	slovanský
jazyky	jazyk	k1gInPc7	jazyk
se	se	k3xPyFc4	se
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
podle	podle	k7c2	podle
počtu	počet	k1gInSc2	počet
rodilých	rodilý	k2eAgMnPc2d1	rodilý
mluvčích	mluvčí	k1gMnPc2	mluvčí
mluví	mluvit	k5eAaImIp3nS	mluvit
nejvíce	hodně	k6eAd3	hodně
<g/>
.	.	kIx.	.
</s>
<s>
Nejpočetnějšími	početní	k2eAgInPc7d3	nejpočetnější
evropskými	evropský	k2eAgInPc7d1	evropský
národy	národ	k1gInPc7	národ
jsou	být	k5eAaImIp3nP	být
indoevropští	indoevropský	k2eAgMnPc1d1	indoevropský
Rusové	Rus	k1gMnPc1	Rus
<g/>
,	,	kIx,	,
Němci	Němec	k1gMnPc1	Němec
<g/>
,	,	kIx,	,
Italové	Ital	k1gMnPc1	Ital
<g/>
,	,	kIx,	,
Angličané	Angličan	k1gMnPc1	Angličan
<g/>
,	,	kIx,	,
Francouzi	Francouz	k1gMnPc1	Francouz
<g/>
,	,	kIx,	,
Ukrajinci	Ukrajinec	k1gMnPc1	Ukrajinec
<g/>
,	,	kIx,	,
Španělé	Španěl	k1gMnPc1	Španěl
a	a	k8xC	a
Poláci	Polák	k1gMnPc1	Polák
<g/>
.	.	kIx.	.
</s>
<s>
Znakem	znak	k1gInSc7	znak
všech	všecek	k3xTgFnPc2	všecek
průmyslových	průmyslový	k2eAgFnPc2d1	průmyslová
oblastí	oblast	k1gFnPc2	oblast
je	být	k5eAaImIp3nS	být
nakupení	nakupení	k1gNnSc1	nakupení
sídel	sídlo	k1gNnPc2	sídlo
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
,	,	kIx,	,
Nizozemsku	Nizozemsko	k1gNnSc6	Nizozemsko
<g/>
,	,	kIx,	,
Porúří	Porúří	k1gNnSc6	Porúří
<g/>
,	,	kIx,	,
Porýní	Porýní	k1gNnSc6	Porýní
a	a	k8xC	a
v	v	k7c6	v
Sasku	Sasko	k1gNnSc6	Sasko
<g/>
,	,	kIx,	,
v	v	k7c6	v
Horním	horní	k2eAgNnSc6d1	horní
Slezsku	Slezsko	k1gNnSc6	Slezsko
<g/>
,	,	kIx,	,
Donbasu	Donbas	k1gInSc6	Donbas
a	a	k8xC	a
Pádské	pádský	k2eAgFnSc3d1	Pádská
nížině	nížina	k1gFnSc3	nížina
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Náboženství	náboženství	k1gNnSc2	náboženství
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Nábožensky	nábožensky	k6eAd1	nábožensky
dominuje	dominovat	k5eAaImIp3nS	dominovat
evropskému	evropský	k2eAgInSc3d1	evropský
prostoru	prostor	k1gInSc3	prostor
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
však	však	k9	však
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
zemích	zem	k1gFnPc6	zem
ustupuje	ustupovat	k5eAaImIp3nS	ustupovat
silné	silný	k2eAgFnSc3d1	silná
sekularizaci	sekularizace	k1gFnSc3	sekularizace
a	a	k8xC	a
nezájmu	nezájem	k1gInSc3	nezájem
obyvatel	obyvatel	k1gMnPc2	obyvatel
o	o	k7c4	o
náboženské	náboženský	k2eAgFnPc4d1	náboženská
záležitosti	záležitost	k1gFnPc4	záležitost
<g/>
,	,	kIx,	,
nejvíce	hodně	k6eAd3	hodně
patrné	patrný	k2eAgNnSc1d1	patrné
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
případě	případ	k1gInSc6	případ
severských	severský	k2eAgFnPc2d1	severská
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
Česka	Česko	k1gNnSc2	Česko
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
Estonska	Estonsko	k1gNnSc2	Estonsko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
tradičně	tradičně	k6eAd1	tradičně
katolických	katolický	k2eAgFnPc6d1	katolická
zemích	zem	k1gFnPc6	zem
přetrval	přetrvat	k5eAaPmAgMnS	přetrvat
velký	velký	k2eAgInSc4d1	velký
vliv	vliv	k1gInSc4	vliv
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
klasickým	klasický	k2eAgInSc7d1	klasický
příkladem	příklad	k1gInSc7	příklad
jsou	být	k5eAaImIp3nP	být
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
,	,	kIx,	,
Litva	Litva	k1gFnSc1	Litva
<g/>
,	,	kIx,	,
Irsko	Irsko	k1gNnSc1	Irsko
<g/>
,	,	kIx,	,
Malta	Malta	k1gFnSc1	Malta
<g/>
,	,	kIx,	,
San	San	k1gFnSc1	San
Marino	Marina	k1gFnSc5	Marina
a	a	k8xC	a
samozřejmě	samozřejmě	k6eAd1	samozřejmě
Vatikán	Vatikán	k1gInSc1	Vatikán
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgFnPc4d1	další
větve	větev	k1gFnPc4	větev
křesťanství	křesťanství	k1gNnSc2	křesťanství
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
patří	patřit	k5eAaImIp3nS	patřit
pravoslaví	pravoslaví	k1gNnSc1	pravoslaví
a	a	k8xC	a
na	na	k7c6	na
severu	sever	k1gInSc6	sever
a	a	k8xC	a
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
protestantismus	protestantismus	k1gInSc4	protestantismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
na	na	k7c6	na
Balkáně	Balkán	k1gInSc6	Balkán
převládlo	převládnout	k5eAaPmAgNnS	převládnout
jako	jako	k8xC	jako
hlavní	hlavní	k2eAgNnSc4d1	hlavní
náboženství	náboženství	k1gNnSc4	náboženství
sunnitský	sunnitský	k2eAgInSc1d1	sunnitský
islám	islám	k1gInSc1	islám
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Hospodářství	hospodářství	k1gNnSc2	hospodářství
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Evropa	Evropa	k1gFnSc1	Evropa
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
hospodářsky	hospodářsky	k6eAd1	hospodářsky
nejvyspělejším	vyspělý	k2eAgFnPc3d3	nejvyspělejší
oblastem	oblast	k1gFnPc3	oblast
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Průmyslová	průmyslový	k2eAgNnPc1d1	průmyslové
odvětví	odvětví	k1gNnPc1	odvětví
mají	mít	k5eAaImIp3nP	mít
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
tradici	tradice	k1gFnSc4	tradice
<g/>
,	,	kIx,	,
zemědělství	zemědělství	k1gNnSc4	zemědělství
je	být	k5eAaImIp3nS	být
intenzivní	intenzivní	k2eAgInSc1d1	intenzivní
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
světového	světový	k2eAgNnSc2d1	světové
hlediska	hledisko	k1gNnSc2	hledisko
má	mít	k5eAaImIp3nS	mít
význam	význam	k1gInSc4	význam
těžba	těžba	k1gFnSc1	těžba
uhlí	uhlí	k1gNnSc2	uhlí
<g/>
,	,	kIx,	,
ropy	ropa	k1gFnSc2	ropa
<g/>
,	,	kIx,	,
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
<g/>
,	,	kIx,	,
železné	železný	k2eAgFnSc2d1	železná
rudy	ruda	k1gFnSc2	ruda
<g/>
,	,	kIx,	,
bauxitu	bauxit	k1gInSc2	bauxit
<g/>
,	,	kIx,	,
niklu	nikl	k1gInSc2	nikl
<g/>
,	,	kIx,	,
rtuti	rtuť	k1gFnSc2	rtuť
<g/>
,	,	kIx,	,
magnezitu	magnezit	k1gInSc2	magnezit
<g/>
,	,	kIx,	,
fosfátů	fosfát	k1gInPc2	fosfát
a	a	k8xC	a
draselných	draselný	k2eAgFnPc2d1	draselná
solí	sůl	k1gFnPc2	sůl
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitějším	důležitý	k2eAgNnSc7d3	nejdůležitější
průmyslovým	průmyslový	k2eAgNnSc7d1	průmyslové
odvětvím	odvětví	k1gNnSc7	odvětví
je	být	k5eAaImIp3nS	být
strojírenství	strojírenství	k1gNnSc1	strojírenství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
jsou	být	k5eAaImIp3nP	být
rozvinuty	rozvinout	k5eAaPmNgInP	rozvinout
všechny	všechen	k3xTgInPc1	všechen
druhy	druh	k1gInPc1	druh
dopravy	doprava	k1gFnSc2	doprava
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
největší	veliký	k2eAgInSc4d3	veliký
objem	objem	k1gInSc4	objem
zahraničního	zahraniční	k2eAgInSc2d1	zahraniční
obchodu	obchod	k1gInSc2	obchod
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
světadílů	světadíl	k1gInPc2	světadíl
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc7d3	veliký
evropskou	evropský	k2eAgFnSc7d1	Evropská
národní	národní	k2eAgFnSc7d1	národní
ekonomikou	ekonomika	k1gFnSc7	ekonomika
v	v	k7c6	v
nominálním	nominální	k2eAgNnSc6d1	nominální
HDP	HDP	kA	HDP
je	být	k5eAaImIp3nS	být
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
následované	následovaný	k2eAgNnSc1d1	následované
Francií	Francie	k1gFnSc7	Francie
<g/>
,	,	kIx,	,
Spojeným	spojený	k2eAgNnSc7d1	spojené
královstvím	království	k1gNnSc7	království
<g/>
,	,	kIx,	,
Itálií	Itálie	k1gFnSc7	Itálie
a	a	k8xC	a
Ruskem	Rusko	k1gNnSc7	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Těchto	tento	k3xDgFnPc2	tento
pět	pět	k4xCc1	pět
zemí	zem	k1gFnPc2	zem
je	být	k5eAaImIp3nS	být
umístěno	umístit	k5eAaPmNgNnS	umístit
ve	v	k7c6	v
světové	světový	k2eAgFnSc6d1	světová
top	topit	k5eAaImRp2nS	topit
10	[number]	k4	10
<g/>
,	,	kIx,	,
evropské	evropský	k2eAgFnSc2d1	Evropská
ekonomiky	ekonomika	k1gFnSc2	ekonomika
tedy	tedy	k9	tedy
tvoří	tvořit	k5eAaImIp3nP	tvořit
polovinu	polovina	k1gFnSc4	polovina
deseti	deset	k4xCc2	deset
největších	veliký	k2eAgFnPc2d3	veliký
ekonomik	ekonomika	k1gFnPc2	ekonomika
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
přeplněném	přeplněný	k2eAgInSc6d1	přeplněný
kontinentu	kontinent	k1gInSc6	kontinent
musí	muset	k5eAaImIp3nP	muset
divoce	divoce	k6eAd1	divoce
rostoucí	rostoucí	k2eAgFnPc1d1	rostoucí
rostliny	rostlina	k1gFnPc1	rostlina
a	a	k8xC	a
divoce	divoce	k6eAd1	divoce
žijící	žijící	k2eAgMnPc1d1	žijící
živočichové	živočich	k1gMnPc1	živočich
bojovat	bojovat	k5eAaImF	bojovat
o	o	k7c4	o
prostor	prostor	k1gInSc4	prostor
se	s	k7c7	s
zemědělstvím	zemědělství	k1gNnSc7	zemědělství
<g/>
,	,	kIx,	,
sídly	sídlo	k1gNnPc7	sídlo
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
průmyslem	průmysl	k1gInSc7	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
Evropa	Evropa	k1gFnSc1	Evropa
se	s	k7c7	s
svými	svůj	k3xOyFgFnPc7	svůj
mnoha	mnoho	k4c7	mnoho
klimaty	klima	k1gNnPc7	klima
a	a	k8xC	a
habitaty	habitat	k1gInPc7	habitat
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
obživu	obživa	k1gFnSc4	obživa
mnoha	mnoho	k4c3	mnoho
rozmanitým	rozmanitý	k2eAgFnPc3d1	rozmanitá
formám	forma	k1gFnPc3	forma
života	život	k1gInSc2	život
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Chladné	chladný	k2eAgFnPc1d1	chladná
oblasti	oblast	k1gFnPc1	oblast
kolem	kolem	k7c2	kolem
severního	severní	k2eAgInSc2d1	severní
polárního	polární	k2eAgInSc2d1	polární
kruhu	kruh	k1gInSc2	kruh
jsou	být	k5eAaImIp3nP	být
domovem	domov	k1gInSc7	domov
sobů	sob	k1gMnPc2	sob
<g/>
,	,	kIx,	,
hus	husa	k1gFnPc2	husa
a	a	k8xC	a
sovic	sovice	k1gFnPc2	sovice
sněžních	sněžní	k2eAgFnPc2d1	sněžní
<g/>
.	.	kIx.	.
</s>
<s>
Trochu	trochu	k6eAd1	trochu
více	hodně	k6eAd2	hodně
na	na	k7c4	na
jih	jih	k1gInSc4	jih
mechy	mech	k1gInPc4	mech
a	a	k8xC	a
lišejníky	lišejník	k1gInPc4	lišejník
<g/>
.	.	kIx.	.
</s>
<s>
Tundra	tundra	k1gFnSc1	tundra
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
po	po	k7c4	po
většinu	většina	k1gFnSc4	většina
roku	rok	k1gInSc2	rok
zmrzlá	zmrzlý	k2eAgFnSc1d1	zmrzlá
<g/>
,	,	kIx,	,
ustupuje	ustupovat	k5eAaImIp3nS	ustupovat
lesům	les	k1gInPc3	les
tvořeným	tvořený	k2eAgInSc7d1	tvořený
smrkem	smrk	k1gInSc7	smrk
a	a	k8xC	a
břízou	bříza	k1gFnSc7	bříza
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
lesy	les	k1gInPc1	les
se	se	k3xPyFc4	se
rozprostírají	rozprostírat	k5eAaImIp3nP	rozprostírat
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
severní	severní	k2eAgFnSc6d1	severní
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
potulují	potulovat	k5eAaImIp3nP	potulovat
se	se	k3xPyFc4	se
v	v	k7c6	v
nich	on	k3xPp3gInPc2	on
medvědi	medvěd	k1gMnPc1	medvěd
a	a	k8xC	a
vlci	vlk	k1gMnPc1	vlk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mírnějších	mírný	k2eAgNnPc6d2	mírnější
klimatech	klima	k1gNnPc6	klima
severozápadní	severozápadní	k2eAgFnSc2d1	severozápadní
Evropy	Evropa	k1gFnSc2	Evropa
jsou	být	k5eAaImIp3nP	být
smíšené	smíšený	k2eAgInPc1d1	smíšený
lesy	les	k1gInPc1	les
tvořené	tvořený	k2eAgInPc1d1	tvořený
dubem	dub	k1gInSc7	dub
<g/>
,	,	kIx,	,
jasanem	jasan	k1gInSc7	jasan
a	a	k8xC	a
bukem	buk	k1gInSc7	buk
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
domovem	domov	k1gInSc7	domov
veverek	veverka	k1gFnPc2	veverka
<g/>
,	,	kIx,	,
vysoké	vysoký	k2eAgFnSc2d1	vysoká
zvěře	zvěř	k1gFnSc2	zvěř
<g/>
,	,	kIx,	,
divokých	divoký	k2eAgMnPc2d1	divoký
kanců	kanec	k1gMnPc2	kanec
a	a	k8xC	a
mnoha	mnoho	k4c2	mnoho
druhů	druh	k1gInPc2	druh
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
Mořští	mořský	k2eAgMnPc1d1	mořský
ptáci	pták	k1gMnPc1	pták
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
racci	racek	k1gMnPc1	racek
<g/>
,	,	kIx,	,
alky	alka	k1gFnPc1	alka
a	a	k8xC	a
terejové	terej	k1gMnPc1	terej
<g/>
,	,	kIx,	,
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
na	na	k7c6	na
skalnatém	skalnatý	k2eAgNnSc6d1	skalnaté
pobřeží	pobřeží	k1gNnSc6	pobřeží
Atlantského	atlantský	k2eAgInSc2d1	atlantský
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Atlantském	atlantský	k2eAgInSc6d1	atlantský
oceánu	oceán	k1gInSc6	oceán
žije	žít	k5eAaImIp3nS	žít
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
různých	různý	k2eAgInPc2d1	různý
druhů	druh	k1gInPc2	druh
ryb	ryba	k1gFnPc2	ryba
včetně	včetně	k7c2	včetně
tresky	treska	k1gFnSc2	treska
<g/>
,	,	kIx,	,
sledě	sleď	k1gMnSc2	sleď
<g/>
,	,	kIx,	,
lososa	losos	k1gMnSc2	losos
<g/>
,	,	kIx,	,
sardinek	sardinka	k1gFnPc2	sardinka
a	a	k8xC	a
jesetera	jeseter	k1gMnSc2	jeseter
<g/>
.	.	kIx.	.
</s>
<s>
Travnaté	travnatý	k2eAgFnPc1d1	travnatá
oblasti	oblast	k1gFnPc1	oblast
střední	střední	k2eAgFnSc2d1	střední
Evropy	Evropa	k1gFnSc2	Evropa
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
využívány	využíván	k2eAgFnPc1d1	využívána
pro	pro	k7c4	pro
zemědělství	zemědělství	k1gNnSc4	zemědělství
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
také	také	k9	také
obživu	obživa	k1gFnSc4	obživa
mnoha	mnoho	k4c2	mnoho
savcům	savec	k1gMnPc3	savec
a	a	k8xC	a
ptákům	pták	k1gMnPc3	pták
<g/>
.	.	kIx.	.
</s>
<s>
Velcí	velký	k2eAgMnPc1d1	velký
sokoli	sokol	k1gMnPc1	sokol
a	a	k8xC	a
orli	orel	k1gMnPc1	orel
loví	lovit	k5eAaImIp3nP	lovit
koroptve	koroptev	k1gFnPc4	koroptev
<g/>
,	,	kIx,	,
křepelky	křepelka	k1gFnPc4	křepelka
<g/>
,	,	kIx,	,
myši	myš	k1gFnPc4	myš
<g/>
,	,	kIx,	,
krtky	krtek	k1gMnPc4	krtek
<g/>
,	,	kIx,	,
krysy	krysa	k1gFnPc4	krysa
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
malé	malý	k2eAgFnPc4d1	malá
živočichy	živočich	k1gMnPc7	živočich
tohoto	tento	k3xDgNnSc2	tento
pásma	pásmo	k1gNnSc2	pásmo
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
na	na	k7c4	na
jih	jih	k1gInSc4	jih
v	v	k7c6	v
horském	horský	k2eAgNnSc6d1	horské
pásmu	pásmo	k1gNnSc6	pásmo
Alp	Alpy	k1gFnPc2	Alpy
nalezneme	naleznout	k5eAaPmIp1nP	naleznout
formy	forma	k1gFnPc4	forma
života	život	k1gInSc2	život
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
typické	typický	k2eAgFnPc1d1	typická
pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
oblast	oblast	k1gFnSc4	oblast
<g/>
,	,	kIx,	,
např.	např.	kA	např.
savce	savec	k1gMnSc2	savec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
si	se	k3xPyFc3	se
vyhrabávají	vyhrabávat	k5eAaImIp3nP	vyhrabávat
nory	nora	k1gFnPc4	nora
a	a	k8xC	a
říká	říkat	k5eAaImIp3nS	říkat
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
svišti	svištit	k5eAaImRp2nS	svištit
<g/>
,	,	kIx,	,
a	a	k8xC	a
kamzíky	kamzík	k1gMnPc4	kamzík
podobné	podobný	k2eAgFnSc6d1	podobná
koze	koza	k1gFnSc6	koza
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
alpské	alpský	k2eAgFnPc4d1	alpská
květiny	květina	k1gFnPc4	květina
patří	patřit	k5eAaImIp3nS	patřit
tmavomodrý	tmavomodrý	k2eAgInSc1d1	tmavomodrý
hořec	hořec	k1gInSc1	hořec
a	a	k8xC	a
trsovitý	trsovitý	k2eAgInSc1d1	trsovitý
plesnivec	plesnivec	k1gInSc1	plesnivec
alpský	alpský	k2eAgInSc1d1	alpský
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
nejjižnějších	jižní	k2eAgInPc6d3	nejjižnější
cípech	cíp	k1gInPc6	cíp
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
daří	dařit	k5eAaImIp3nS	dařit
olivovníkům	olivovník	k1gInPc3	olivovník
<g/>
,	,	kIx,	,
cypřišům	cypřiš	k1gInPc3	cypřiš
a	a	k8xC	a
borovicím	borovice	k1gFnPc3	borovice
piniím	pinie	k1gFnPc3	pinie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
teplém	teplý	k2eAgNnSc6d1	teplé
slunci	slunce	k1gNnSc6	slunce
tohoto	tento	k3xDgNnSc2	tento
pásma	pásmo	k1gNnSc2	pásmo
se	se	k3xPyFc4	se
vyhřívají	vyhřívat	k5eAaImIp3nP	vyhřívat
hadi	had	k1gMnPc1	had
<g/>
,	,	kIx,	,
ještěrky	ještěrka	k1gFnPc1	ještěrka
a	a	k8xC	a
želvy	želva	k1gFnPc1	želva
<g/>
.	.	kIx.	.
</s>
<s>
Evropa	Evropa	k1gFnSc1	Evropa
vždy	vždy	k6eAd1	vždy
značně	značně	k6eAd1	značně
ovlivňovala	ovlivňovat	k5eAaImAgFnS	ovlivňovat
světovou	světový	k2eAgFnSc4d1	světová
politiku	politika	k1gFnSc4	politika
<g/>
.	.	kIx.	.
</s>
<s>
Staří	starý	k2eAgMnPc1d1	starý
Řekové	Řek	k1gMnPc1	Řek
jako	jako	k8xC	jako
první	první	k4xOgMnPc1	první
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
kolem	kolem	k6eAd1	kolem
roku	rok	k1gInSc2	rok
450	[number]	k4	450
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
demokracii	demokracie	k1gFnSc3	demokracie
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
si	se	k3xPyFc3	se
vládu	vláda	k1gFnSc4	vláda
volí	volit	k5eAaImIp3nS	volit
lid	lid	k1gInSc1	lid
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
všeobecně	všeobecně	k6eAd1	všeobecně
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
měla	mít	k5eAaImAgFnS	mít
silný	silný	k2eAgInSc4d1	silný
politický	politický	k2eAgInSc4d1	politický
vliv	vliv	k1gInSc4	vliv
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
církev	církev	k1gFnSc1	církev
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
moc	moc	k6eAd1	moc
přesunula	přesunout	k5eAaPmAgFnS	přesunout
z	z	k7c2	z
rukou	ruka	k1gFnPc2	ruka
církve	církev	k1gFnSc2	církev
do	do	k7c2	do
rukou	ruka	k1gFnSc7	ruka
několika	několik	k4yIc2	několik
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Španělska	Španělsko	k1gNnSc2	Španělsko
a	a	k8xC	a
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
státy	stát	k1gInPc1	stát
zbohatly	zbohatnout	k5eAaPmAgInP	zbohatnout
také	také	k6eAd1	také
kolonizací	kolonizace	k1gFnSc7	kolonizace
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
obou	dva	k4xCgFnPc2	dva
Amerik	Amerika	k1gFnPc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
těchto	tento	k3xDgFnPc2	tento
kolonií	kolonie	k1gFnPc2	kolonie
získala	získat	k5eAaPmAgFnS	získat
nezávislost	nezávislost	k1gFnSc4	nezávislost
až	až	k6eAd1	až
v	v	k7c6	v
minulém	minulý	k2eAgNnSc6d1	Minulé
století	století	k1gNnSc6	století
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
(	(	kIx(	(
<g/>
1914	[number]	k4	1914
<g/>
–	–	k?	–
<g/>
1918	[number]	k4	1918
<g/>
)	)	kIx)	)
i	i	k9	i
druhá	druhý	k4xOgFnSc1	druhý
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
začaly	začít	k5eAaPmAgFnP	začít
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
kontinent	kontinent	k1gInSc1	kontinent
rozdělil	rozdělit	k5eAaPmAgInS	rozdělit
na	na	k7c4	na
komunistické	komunistický	k2eAgFnPc4d1	komunistická
země	zem	k1gFnPc4	zem
na	na	k7c6	na
východě	východ	k1gInSc6	východ
<g/>
,	,	kIx,	,
vedené	vedený	k2eAgNnSc1d1	vedené
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
<g/>
,	,	kIx,	,
a	a	k8xC	a
nekomunistické	komunistický	k2eNgFnPc1d1	nekomunistická
země	zem	k1gFnPc1	zem
na	na	k7c6	na
západě	západ	k1gInSc6	západ
<g/>
,	,	kIx,	,
podporované	podporovaný	k2eAgInPc1d1	podporovaný
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
americkými	americký	k2eAgInPc7d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Evropa	Evropa	k1gFnSc1	Evropa
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
centrem	centrum	k1gNnSc7	centrum
boje	boj	k1gInSc2	boj
o	o	k7c4	o
moc	moc	k1gFnSc4	moc
mezi	mezi	k7c7	mezi
komunistickými	komunistický	k2eAgFnPc7d1	komunistická
a	a	k8xC	a
nekomunistickými	komunistický	k2eNgFnPc7d1	nekomunistická
zeměmi	zem	k1gFnPc7	zem
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
boj	boj	k1gInSc1	boj
byl	být	k5eAaImAgInS	být
znám	znám	k2eAgInSc1d1	znám
pod	pod	k7c7	pod
pojmem	pojem	k1gInSc7	pojem
studená	studený	k2eAgFnSc1d1	studená
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vedly	vést	k5eAaImAgFnP	vést
reformy	reforma	k1gFnPc1	reforma
v	v	k7c6	v
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
a	a	k8xC	a
východní	východní	k2eAgFnSc6d1	východní
Evropě	Evropa	k1gFnSc6	Evropa
k	k	k7c3	k
pádu	pád	k1gInSc3	pád
tamního	tamní	k2eAgInSc2d1	tamní
komunismu	komunismus	k1gInSc2	komunismus
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
,	,	kIx,	,
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
a	a	k8xC	a
Československa	Československo	k1gNnSc2	Československo
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
nové	nový	k2eAgInPc1d1	nový
státy	stát	k1gInPc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
Studená	studený	k2eAgFnSc1d1	studená
válka	válka	k1gFnSc1	válka
skončila	skončit	k5eAaPmAgFnS	skončit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
konflikt	konflikt	k1gInSc1	konflikt
mezi	mezi	k7c7	mezi
etnickými	etnický	k2eAgFnPc7d1	etnická
skupinami	skupina	k1gFnPc7	skupina
a	a	k8xC	a
ekonomické	ekonomický	k2eAgInPc1d1	ekonomický
problémy	problém	k1gInPc1	problém
způsobily	způsobit	k5eAaPmAgInP	způsobit
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Evropě	Evropa	k1gFnSc6	Evropa
nové	nový	k2eAgInPc4d1	nový
zdroje	zdroj	k1gInPc4	zdroj
napětí	napětí	k1gNnSc2	napětí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
bylo	být	k5eAaImAgNnS	být
15	[number]	k4	15
západoevropských	západoevropský	k2eAgInPc2d1	západoevropský
států	stát	k1gInPc2	stát
spojeno	spojit	k5eAaPmNgNnS	spojit
v	v	k7c4	v
Evropskou	evropský	k2eAgFnSc4d1	Evropská
unii	unie	k1gFnSc4	unie
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
organizace	organizace	k1gFnSc1	organizace
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
jejím	její	k3xOp3gInSc7	její
cílem	cíl	k1gInSc7	cíl
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
politicky	politicky	k6eAd1	politicky
a	a	k8xC	a
ekonomicky	ekonomicky	k6eAd1	ekonomicky
sjednotila	sjednotit	k5eAaPmAgFnS	sjednotit
různé	různý	k2eAgFnPc4d1	různá
země	zem	k1gFnPc4	zem
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc4	seznam
evropských	evropský	k2eAgInPc2d1	evropský
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
evropském	evropský	k2eAgInSc6d1	evropský
kontinentu	kontinent	k1gInSc6	kontinent
a	a	k8xC	a
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
náležících	náležící	k2eAgInPc6d1	náležící
ostrovech	ostrov	k1gInPc6	ostrov
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
území	území	k1gNnSc4	území
46	[number]	k4	46
nezávislých	závislý	k2eNgInPc2d1	nezávislý
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
těchto	tento	k3xDgFnPc2	tento
zemí	zem	k1gFnPc2	zem
může	moct	k5eAaImIp3nS	moct
však	však	k9	však
být	být	k5eAaImF	být
modifikován	modifikován	k2eAgInSc1d1	modifikován
na	na	k7c6	na
základě	základ	k1gInSc6	základ
jiných	jiný	k2eAgNnPc2d1	jiné
hledisek	hledisko	k1gNnPc2	hledisko
<g/>
.	.	kIx.	.
</s>
<s>
Vyřazeny	vyřazen	k2eAgMnPc4d1	vyřazen
můžou	můžou	k?	můžou
být	být	k5eAaImF	být
země	zem	k1gFnPc4	zem
<g/>
,	,	kIx,	,
jejíchž	jejíž	k3xOyRp3gMnPc6	jejíž
menší	malý	k2eAgFnSc1d2	menší
část	část	k1gFnSc1	část
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
většina	většina	k1gFnSc1	většina
jejich	jejich	k3xOp3gNnSc2	jejich
území	území	k1gNnSc2	území
leží	ležet	k5eAaImIp3nS	ležet
mimo	mimo	k7c4	mimo
Evropu	Evropa	k1gFnSc4	Evropa
a	a	k8xC	a
státy	stát	k1gInPc4	stát
jako	jako	k8xC	jako
takové	takový	k3xDgInPc1	takový
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
v	v	k7c6	v
jiném	jiný	k2eAgInSc6d1	jiný
kulturním	kulturní	k2eAgInSc6d1	kulturní
rámci	rámec	k1gInSc6	rámec
(	(	kIx(	(
<g/>
Kazachstán	Kazachstán	k1gInSc1	Kazachstán
a	a	k8xC	a
Turecko	Turecko	k1gNnSc1	Turecko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
na	na	k7c6	na
základě	základ	k1gInSc6	základ
kulturně-politického	kulturněolitický	k2eAgNnSc2d1	kulturně-politické
hlediska	hledisko	k1gNnSc2	hledisko
bývá	bývat	k5eAaImIp3nS	bývat
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
zařazován	zařazovat	k5eAaImNgInS	zařazovat
i	i	k9	i
Kypr	Kypr	k1gInSc1	Kypr
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
tři	tři	k4xCgInPc1	tři
státy	stát	k1gInPc1	stát
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
na	na	k7c6	na
rozmezí	rozmezí	k1gNnSc6	rozmezí
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
Asie	Asie	k1gFnSc2	Asie
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Kavkazu	Kavkaz	k1gInSc2	Kavkaz
a	a	k8xC	a
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
nejednotnému	jednotný	k2eNgNnSc3d1	nejednotné
určení	určení	k1gNnSc3	určení
evropsko-asijské	evropskosijský	k2eAgFnSc2d1	evropsko-asijský
hranice	hranice	k1gFnSc2	hranice
se	se	k3xPyFc4	se
můžou	můžou	k?	můžou
některé	některý	k3yIgFnPc4	některý
jejich	jejich	k3xOp3gFnPc4	jejich
části	část	k1gFnPc4	část
<g/>
,	,	kIx,	,
či	či	k8xC	či
dokonce	dokonce	k9	dokonce
celá	celý	k2eAgFnSc1d1	celá
jejich	jejich	k3xOp3gNnSc4	jejich
území	území	k1gNnSc4	území
<g/>
,	,	kIx,	,
nacházet	nacházet	k5eAaImF	nacházet
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
<g/>
:	:	kIx,	:
Arménie	Arménie	k1gFnSc2	Arménie
Arménie	Arménie	k1gFnSc2	Arménie
Ázerbájdžán	Ázerbájdžán	k1gInSc1	Ázerbájdžán
Ázerbájdžán	Ázerbájdžán	k1gInSc1	Ázerbájdžán
Gruzie	Gruzie	k1gFnSc2	Gruzie
Gruzie	Gruzie	k1gFnSc2	Gruzie
Kypr	Kypr	k1gInSc1	Kypr
Kypr	Kypr	k1gInSc1	Kypr
Kromě	kromě	k7c2	kromě
těchto	tento	k3xDgInPc2	tento
samostatných	samostatný	k2eAgInPc2d1	samostatný
států	stát	k1gInPc2	stát
se	se	k3xPyFc4	se
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
nachází	nacházet	k5eAaImIp3nS	nacházet
S	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Běloruska	Bělorusko	k1gNnSc2	Bělorusko
<g/>
,	,	kIx,	,
Vatikánu	Vatikán	k1gInSc2	Vatikán
<g/>
,	,	kIx,	,
Kazachstánu	Kazachstán	k1gInSc2	Kazachstán
a	a	k8xC	a
mezinárodně	mezinárodně	k6eAd1	mezinárodně
neuznaných	uznaný	k2eNgInPc2d1	neuznaný
států	stát	k1gInPc2	stát
jsou	být	k5eAaImIp3nP	být
všechny	všechen	k3xTgFnPc4	všechen
evropské	evropský	k2eAgFnPc4d1	Evropská
země	zem	k1gFnPc4	zem
členy	člen	k1gInPc4	člen
Rady	rada	k1gFnSc2	rada
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějším	významný	k2eAgNnSc7d3	nejvýznamnější
politickým	politický	k2eAgNnSc7d1	politické
uskupením	uskupení	k1gNnSc7	uskupení
současné	současný	k2eAgFnSc2d1	současná
Evropy	Evropa	k1gFnSc2	Evropa
je	být	k5eAaImIp3nS	být
potom	potom	k6eAd1	potom
Evropská	evropský	k2eAgFnSc1d1	Evropská
unie	unie	k1gFnSc1	unie
sdružující	sdružující	k2eAgFnSc1d1	sdružující
27	[number]	k4	27
evropských	evropský	k2eAgFnPc2d1	Evropská
zemí	zem	k1gFnPc2	zem
a	a	k8xC	a
Kypr	Kypr	k1gInSc1	Kypr
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
geograficky	geograficky	k6eAd1	geograficky
přičítán	přičítán	k2eAgInSc1d1	přičítán
Asii	Asie	k1gFnSc3	Asie
<g/>
,	,	kIx,	,
politicky	politicky	k6eAd1	politicky
<g/>
,	,	kIx,	,
historicky	historicky	k6eAd1	historicky
a	a	k8xC	a
kulturně	kulturně	k6eAd1	kulturně
pak	pak	k6eAd1	pak
spíše	spíše	k9	spíše
Evropě	Evropa	k1gFnSc3	Evropa
<g/>
.	.	kIx.	.
</s>
