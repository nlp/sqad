<s>
Evropa	Evropa	k1gFnSc1	Evropa
je	být	k5eAaImIp3nS	být
území	území	k1gNnSc4	území
brané	braný	k2eAgNnSc4d1	brané
buďto	buďto	k8xC	buďto
jako	jako	k9	jako
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
šesti	šest	k4xCc2	šest
světadílů	světadíl	k1gInPc2	světadíl
v	v	k7c6	v
jejich	jejich	k3xOp3gNnPc6	jejich
tradičních	tradiční	k2eAgNnPc6d1	tradiční
pojetích	pojetí	k1gNnPc6	pojetí
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
západní	západní	k2eAgFnSc4d1	západní
část	část	k1gFnSc4	část
Eurasie	Eurasie	k1gFnSc2	Eurasie
<g/>
.	.	kIx.	.
</s>
