<s>
Leopold	Leopold	k1gMnSc1
<g/>
,	,	kIx,
vévoda	vévoda	k1gMnSc1
z	z	k7c2
Brabantu	Brabant	k1gInSc2
</s>
<s>
Princ	princ	k1gMnSc1
Leopold	Leopold	k1gMnSc1
Jiná	jiný	k2eAgNnPc1d1
jména	jméno	k1gNnPc4
</s>
<s>
Francouzsky	francouzsky	k6eAd1
<g/>
:	:	kIx,
Léopold	Léopold	k1gInSc1
Ferdinand	Ferdinand	k1gMnSc1
Élie	Élie	k1gFnPc2
Victor	Victor	k1gMnSc1
Albert	Albert	k1gMnSc1
Marie	Maria	k1gFnSc2
</s>
<s>
Nizozemsky	nizozemsky	k6eAd1
<g/>
:	:	kIx,
Leopold	Leopold	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
Elias	Elias	k1gMnSc1
Viktor	Viktor	k1gMnSc1
Albert	Albert	k1gMnSc1
Maria	Mario	k1gMnSc2
Narození	narození	k1gNnSc2
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1859	#num#	k4
<g/>
Královský	královský	k2eAgInSc1d1
palác	palác	k1gInSc1
Laeken	Laeken	k2eAgInSc1d1
<g/>
,	,	kIx,
Laken	Laken	k1gInSc1
<g/>
,	,	kIx,
Brusel	Brusel	k1gInSc1
<g/>
,	,	kIx,
Belgie	Belgie	k1gFnSc1
Úmrtí	úmrtí	k1gNnSc2
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1869	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
9	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Královský	královský	k2eAgInSc1d1
palác	palác	k1gInSc1
Laeken	Laeken	k2eAgInSc1d1
<g/>
,	,	kIx,
Laken	Laken	k1gInSc1
<g/>
,	,	kIx,
Brusel	Brusel	k1gInSc1
<g/>
,	,	kIx,
Belgie	Belgie	k1gFnSc1
Příčina	příčina	k1gFnSc1
úmrtí	úmrtí	k1gNnPc2
</s>
<s>
zápal	zápal	k1gInSc1
plic	plíce	k1gFnPc2
Místo	místo	k7c2
pohřbení	pohřbení	k1gNnSc2
</s>
<s>
Kostel	kostel	k1gInSc1
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
z	z	k7c2
Laekenu	Laeken	k1gInSc2
Povolání	povolání	k1gNnSc2
</s>
<s>
aristokrat	aristokrat	k1gMnSc1
Titul	titul	k1gInSc4
</s>
<s>
Vévoda	vévoda	k1gMnSc1
z	z	k7c2
Brabantu	Brabant	k1gInSc2
Ocenění	ocenění	k1gNnSc1
</s>
<s>
rytíř	rytíř	k1gMnSc1
Řádu	řád	k1gInSc2
zlatého	zlatý	k2eAgNnSc2d1
rouna	rouno	k1gNnSc2
Nábož	Nábož	k1gFnSc1
<g/>
.	.	kIx.
vyznání	vyznání	k1gNnSc1
</s>
<s>
Katolická	katolický	k2eAgFnSc1d1
církev	církev	k1gFnSc1
Rodiče	rodič	k1gMnSc2
</s>
<s>
Leopold	Leopold	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Belgický	belgický	k2eAgMnSc1d1
a	a	k8xC
Marie	Marie	k1gFnSc1
Jindřiška	Jindřiška	k1gFnSc1
Rakouská	rakouský	k2eAgFnSc1d1
Příbuzní	příbuzný	k1gMnPc1
</s>
<s>
Klementina	Klementina	k1gFnSc1
Belgická	belgický	k2eAgFnSc1d1
<g/>
,	,	kIx,
Štěpánka	Štěpánka	k1gFnSc1
Belgická	belgický	k2eAgFnSc1d1
a	a	k8xC
Luisa	Luisa	k1gFnSc1
Belgická	belgický	k2eAgFnSc1d1
(	(	kIx(
<g/>
sourozenci	sourozenec	k1gMnPc1
<g/>
)	)	kIx)
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Princ	princ	k1gMnSc1
Leopold	Leopold	k1gMnSc1
Belgický	belgický	k2eAgMnSc1d1
<g/>
,	,	kIx,
vévoda	vévoda	k1gMnSc1
z	z	k7c2
Brabantu	Brabant	k1gInSc2
<g/>
,	,	kIx,
hrabě	hrabě	k1gMnSc1
z	z	k7c2
Henegavska	Henegavsko	k1gNnSc2
(	(	kIx(
<g/>
12	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1859	#num#	k4
-	-	kIx~
22	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1869	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
druhým	druhý	k4xOgMnSc7
dítětem	dítě	k1gNnSc7
a	a	k8xC
jediným	jediný	k2eAgMnSc7d1
synem	syn	k1gMnSc7
krále	král	k1gMnSc2
Leopolda	Leopold	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Belgického	belgický	k2eAgNnSc2d1
a	a	k8xC
jeho	jeho	k3xOp3gFnSc2
ženy	žena	k1gFnSc2
<g/>
,	,	kIx,
Marie	Maria	k1gFnSc2
Jindřišky	Jindřiška	k1gFnSc2
Rakouské	rakouský	k2eAgFnSc2d1
<g/>
,	,	kIx,
a	a	k8xC
následníkem	následník	k1gMnSc7
belgického	belgický	k2eAgInSc2d1
trůnu	trůn	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Princ	princ	k1gMnSc1
Leopold	Leopold	k1gMnSc1
se	se	k3xPyFc4
svou	svůj	k3xOyFgFnSc7
matkou	matka	k1gFnSc7
Marií	Maria	k1gFnSc7
Jindřiškou	Jindřiška	k1gFnSc7
v	v	k7c6
roce	rok	k1gInSc6
1864	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Smrt	smrt	k1gFnSc1
prince	princ	k1gMnSc2
Leopolda	Leopold	k1gMnSc2
</s>
<s>
Při	při	k7c6
narození	narození	k1gNnSc6
byl	být	k5eAaImAgMnS
Leopold	Leopold	k1gMnSc1
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
nejstarší	starý	k2eAgMnSc1d3
syn	syn	k1gMnSc1
tehdejšího	tehdejší	k2eAgMnSc2d1
korunního	korunní	k2eAgMnSc2d1
prince	princ	k1gMnSc2
<g/>
,	,	kIx,
oslovován	oslovován	k2eAgMnSc1d1
jako	jako	k8xS,k8xC
hrabě	hrabě	k1gMnSc1
z	z	k7c2
Hegenavska	Hegenavsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
době	doba	k1gFnSc6
jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
narození	narození	k1gNnSc2
byl	být	k5eAaImAgMnS
jeho	jeho	k3xOp3gMnSc1
dědeček	dědeček	k1gMnSc1
Leopold	Leopold	k1gMnSc1
<g/>
,	,	kIx,
bývalý	bývalý	k2eAgMnSc1d1
princ	princ	k1gMnSc1
Sasko-Kobursko-Gothajský	Sasko-Kobursko-Gothajský	k2eAgMnSc1d1
<g/>
,	,	kIx,
vládnoucím	vládnoucí	k2eAgMnSc7d1
belgickým	belgický	k2eAgMnSc7d1
králem	král	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Leopold	Leopold	k1gMnSc1
měl	mít	k5eAaImAgMnS
jednu	jeden	k4xCgFnSc4
starší	starý	k2eAgFnSc4d2
sestru	sestra	k1gFnSc4
<g/>
,	,	kIx,
princeznu	princezna	k1gFnSc4
Luisu	Luisa	k1gFnSc4
<g/>
,	,	kIx,
a	a	k8xC
dvě	dva	k4xCgFnPc4
mladší	mladý	k2eAgFnPc4d2
sestry	sestra	k1gFnPc4
<g/>
,	,	kIx,
princeznu	princezna	k1gFnSc4
Štěpánku	Štěpánka	k1gFnSc4
a	a	k8xC
princeznu	princezna	k1gFnSc4
Klementinu	Klementina	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
narodila	narodit	k5eAaPmAgFnS
po	po	k7c6
Leopoldově	Leopoldův	k2eAgFnSc6d1
smrti	smrt	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Představovala	představovat	k5eAaImAgFnS
poslední	poslední	k2eAgFnSc4d1
naději	naděje	k1gFnSc4
jejich	jejich	k3xOp3gMnPc2
rodičů	rodič	k1gMnPc2
na	na	k7c4
dalšího	další	k2eAgMnSc4d1
syna	syn	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
Princ	princ	k1gMnSc1
Leopold	Leopold	k1gMnSc1
na	na	k7c6
smrtelné	smrtelný	k2eAgFnSc6d1
posteli	postel	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
smrti	smrt	k1gFnSc6
jeho	on	k3xPp3gMnSc2,k3xOp3gMnSc2
dědečka	dědeček	k1gMnSc2
a	a	k8xC
nástupu	nástup	k1gInSc2
jeho	jeho	k3xOp3gMnSc2,k3xPp3gMnSc2
otce	otec	k1gMnSc2
na	na	k7c4
trůn	trůn	k1gInSc4
se	se	k3xPyFc4
Leopold	Leopold	k1gMnSc1
stal	stát	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1865	#num#	k4
vévodou	vévoda	k1gMnSc7
z	z	k7c2
Brabantu	Brabant	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Smrt	smrt	k1gFnSc1
</s>
<s>
Leopold	Leopold	k1gMnSc1
zemřel	zemřít	k5eAaPmAgMnS
v	v	k7c6
Lakenu	Laken	k1gInSc6
v	v	k7c6
Bruselu	Brusel	k1gInSc6
22	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1869	#num#	k4
na	na	k7c4
zápal	zápal	k1gInSc4
plic	plíce	k1gFnPc2
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yInSc4,k3yRnSc4,k3yQnSc4
spadl	spadnout	k5eAaPmAgInS
do	do	k7c2
rybníka	rybník	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
pohřbu	pohřeb	k1gInSc6
svého	svůj	k3xOyFgMnSc2
syna	syn	k1gMnSc2
se	se	k3xPyFc4
král	král	k1gMnSc1
Leopold	Leopold	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
„	„	k?
<g/>
zhroutil	zhroutit	k5eAaPmAgInS
na	na	k7c6
veřejnosti	veřejnost	k1gFnSc6
<g/>
,	,	kIx,
padl	padnout	k5eAaImAgInS,k5eAaPmAgInS
na	na	k7c4
kolena	koleno	k1gNnPc4
vedle	vedle	k7c2
rakve	rakev	k1gFnSc2
a	a	k8xC
nekontrolovatelně	kontrolovatelně	k6eNd1
vzlykal	vzlykat	k5eAaImAgInS
<g/>
.	.	kIx.
<g/>
“	“	k?
Tělo	tělo	k1gNnSc1
prince	princ	k1gMnSc2
Leopolda	Leopold	k1gMnSc2
bylo	být	k5eAaImAgNnS
pohřbeno	pohřbít	k5eAaPmNgNnS
v	v	k7c6
královské	královský	k2eAgFnSc6d1
kryptě	krypta	k1gFnSc6
v	v	k7c6
kostele	kostel	k1gInSc6
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
z	z	k7c2
Lakenu	Laken	k1gInSc2
v	v	k7c6
Bruselu	Brusel	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Leopoldova	Leopoldův	k2eAgFnSc1d1
předčasná	předčasný	k2eAgFnSc1d1
smrt	smrt	k1gFnSc1
zanechala	zanechat	k5eAaPmAgFnS
jeho	jeho	k3xOp3gFnSc3
otce	otka	k1gFnSc3
pouze	pouze	k6eAd1
s	s	k7c7
dvěma	dva	k4xCgFnPc7
dalšími	další	k2eAgFnPc7d1
dětmi	dítě	k1gFnPc7
<g/>
:	:	kIx,
princeznou	princezna	k1gFnSc7
Luisou	Luisa	k1gFnSc7
a	a	k8xC
princeznou	princezna	k1gFnSc7
Štěpánkou	Štěpánka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
smrti	smrt	k1gFnSc6
jejich	jejich	k3xOp3gMnSc2
syna	syn	k1gMnSc2
se	se	k3xPyFc4
Leopold	Leopolda	k1gFnPc2
a	a	k8xC
Marie	Maria	k1gFnSc2
Jindřiška	Jindřiška	k1gFnSc1
pokusili	pokusit	k5eAaPmAgMnP
mít	mít	k5eAaImF
další	další	k2eAgNnSc4d1
dítě	dítě	k1gNnSc4
v	v	k7c6
naději	naděje	k1gFnSc6
na	na	k7c4
syna	syn	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
narození	narození	k1gNnSc6
další	další	k2eAgFnSc2d1
dcery	dcera	k1gFnSc2
Klementiny	Klementina	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1872	#num#	k4
pár	pár	k4xCyI
opustil	opustit	k5eAaPmAgMnS
všechny	všechen	k3xTgFnPc4
naděje	naděje	k1gFnPc4
na	na	k7c4
dalšího	další	k2eAgMnSc4d1
syna	syn	k1gMnSc4
a	a	k8xC
jejich	jejich	k3xOp3gNnSc1
již	již	k6eAd1
napjaté	napjatý	k2eAgNnSc1d1
manželství	manželství	k1gNnSc1
se	se	k3xPyFc4
úplně	úplně	k6eAd1
rozpadlo	rozpadnout	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
jeho	jeho	k3xOp3gFnSc6
smrti	smrt	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
1909	#num#	k4
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
Leopold	Leopold	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
následován	následovat	k5eAaImNgMnS
jeho	jeho	k3xOp3gMnSc7
synovcem	synovec	k1gMnSc7
Albertem	Albert	k1gMnSc7
I.	I.	kA
<g/>
,	,	kIx,
jehož	jenž	k3xRgMnSc4,k3xOyRp3gMnSc4
nejstarší	starý	k2eAgMnSc1d3
syn	syn	k1gMnSc1
později	pozdě	k6eAd2
nastoupil	nastoupit	k5eAaPmAgMnS
na	na	k7c4
trůn	trůn	k1gInSc4
jako	jako	k8xS,k8xC
Leopold	Leopold	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s>
Vyznamenání	vyznamenání	k1gNnSc1
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
<g/>
:	:	kIx,
Rytíř	Rytíř	k1gMnSc1
Řádu	řád	k1gInSc2
zlatého	zlatý	k2eAgNnSc2d1
rouna	rouno	k1gNnSc2
<g/>
,	,	kIx,
11	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1866	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Předkové	předek	k1gMnPc1
</s>
<s>
Arnošt	Arnošt	k1gMnSc1
Fridrich	Fridrich	k1gMnSc1
<g/>
,	,	kIx,
vévoda	vévoda	k1gMnSc1
Sasko-Kobursko-Saalfeldský	Sasko-Kobursko-Saalfeldský	k2eAgMnSc1d1
</s>
<s>
František	František	k1gMnSc1
<g/>
,	,	kIx,
vévoda	vévoda	k1gMnSc1
Sasko-Kobursko-Saalfeldský	Sasko-Kobursko-Saalfeldský	k2eAgMnSc1d1
</s>
<s>
Žofie	Žofie	k1gFnSc1
Antonie	Antonie	k1gFnSc2
Brunšvicko-Wolfenbüttelská	Brunšvicko-Wolfenbüttelský	k2eAgFnSc1d1
</s>
<s>
Leopold	Leopold	k1gMnSc1
I.	I.	kA
Belgický	belgický	k2eAgMnSc1d1
</s>
<s>
Heinrich	Heinrich	k1gMnSc1
XXIV	XXIV	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
hrabě	hrabě	k1gMnSc1
Reuss	Reuss	k1gInSc1
z	z	k7c2
Ebersdorfu	Ebersdorf	k1gInSc2
</s>
<s>
Hraběnka	hraběnka	k1gFnSc1
Augusta	August	k1gMnSc2
Reuss	Reussa	k1gFnPc2
Ebersdorf	Ebersdorf	k1gMnSc1
</s>
<s>
Hraběnka	hraběnka	k1gFnSc1
Karoline	Karolin	k1gInSc5
Ernestine	Ernestin	k1gInSc5
z	z	k7c2
Erbach-Schönberg	Erbach-Schönberg	k1gMnSc1
</s>
<s>
Leopold	Leopold	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Belgický	belgický	k2eAgInSc1d1
</s>
<s>
Ludvík	Ludvík	k1gMnSc1
Filip	Filip	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
vévoda	vévoda	k1gMnSc1
Orleánský	orleánský	k2eAgMnSc1d1
</s>
<s>
Ludvík	Ludvík	k1gMnSc1
Filip	Filip	k1gMnSc1
Orleánský	orleánský	k2eAgMnSc1d1
</s>
<s>
Luisa	Luisa	k1gFnSc1
Marie	Marie	k1gFnSc1
Adelaida	Adelaida	k1gFnSc1
Bourbonská	bourbonský	k2eAgFnSc1d1
<g/>
,	,	kIx,
vévodkyně	vévodkyně	k1gFnSc1
Orleánská	orleánský	k2eAgFnSc1d1
</s>
<s>
Princezna	princezna	k1gFnSc1
Luisa	Luisa	k1gFnSc1
Orléanská	Orléanský	k2eAgFnSc1d1
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1
I.	I.	kA
Neapolsko-Sicilský	neapolsko-sicilský	k2eAgInSc4d1
</s>
<s>
Princezna	princezna	k1gFnSc1
Marie	Marie	k1gFnSc1
Amálie	Amálie	k1gFnSc1
Neapolsko-Sicilská	neapolsko-sicilský	k2eAgFnSc1d1
</s>
<s>
Marie	Marie	k1gFnSc1
Karolína	Karolína	k1gFnSc1
Rakouská	rakouský	k2eAgFnSc1d1
</s>
<s>
Princ	princ	k1gMnSc1
Leopold	Leopold	k1gMnSc1
<g/>
,	,	kIx,
vévoda	vévoda	k1gMnSc1
z	z	k7c2
Brabantu	Brabant	k1gInSc2
</s>
<s>
František	František	k1gMnSc1
I.	I.	kA
<g/>
,	,	kIx,
císař	císař	k1gMnSc1
Svaté	svatý	k2eAgFnSc2d1
říše	říš	k1gFnSc2
římské	římský	k2eAgFnSc2d1
</s>
<s>
Leopold	Leopold	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
císař	císař	k1gMnSc1
Svaté	svatý	k2eAgFnSc2d1
říše	říš	k1gFnSc2
římské	římský	k2eAgFnSc2d1
</s>
<s>
Marie	Marie	k1gFnSc1
Terezie	Terezie	k1gFnSc2
</s>
<s>
Arcivévoda	arcivévoda	k1gMnSc1
Josef	Josef	k1gMnSc1
<g/>
,	,	kIx,
uherský	uherský	k2eAgMnSc1d1
palatin	palatin	k1gMnSc1
</s>
<s>
Karel	Karel	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Španělský	španělský	k2eAgInSc1d1
</s>
<s>
Infantka	infantka	k1gFnSc1
Marie	Marie	k1gFnSc1
Ludovika	Ludovika	k1gFnSc1
Španělská	španělský	k2eAgFnSc1d1
</s>
<s>
Marie	Marie	k1gFnSc1
Amálie	Amálie	k1gFnSc2
Saská	saský	k2eAgFnSc1d1
</s>
<s>
Arcivévodkyně	arcivévodkyně	k1gFnSc1
Marie	Marie	k1gFnSc1
Jindřiška	Jindřiška	k1gFnSc1
Rakouská	rakouský	k2eAgFnSc1d1
</s>
<s>
Fridrich	Fridrich	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Evžen	Evžen	k1gMnSc1
<g/>
,	,	kIx,
vévoda	vévoda	k1gMnSc1
Württemberský	Württemberský	k2eAgMnSc1d1
</s>
<s>
Vévoda	vévoda	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
Württemberský	Württemberský	k2eAgMnSc1d1
</s>
<s>
Princezna	princezna	k1gFnSc1
Bedřiška	Bedřiška	k1gFnSc1
Braniborsko-Schwedtská	Braniborsko-Schwedtský	k2eAgFnSc1d1
</s>
<s>
Arcivévodkyně	arcivévodkyně	k1gFnSc1
Marie	Maria	k1gFnSc2
Dorotea	Dorote	k2eAgFnSc1d1
Württemberská	Württemberský	k2eAgFnSc1d1
</s>
<s>
Karel	Karel	k1gMnSc1
Kristián	Kristián	k1gMnSc1
<g/>
,	,	kIx,
princ	princ	k1gMnSc1
Nasavsko-Weilburský	Nasavsko-Weilburský	k2eAgMnSc1d1
</s>
<s>
Princezna	princezna	k1gFnSc1
Henrietta	Henriett	k1gInSc2
Nasavsko-Weilburská	Nasavsko-Weilburský	k2eAgFnSc1d1
</s>
<s>
Princezna	princezna	k1gFnSc1
Karolína	Karolína	k1gFnSc1
Oranžsko-Nasavská	Oranžsko-Nasavský	k2eAgFnSc1d1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Prince	princ	k1gMnSc2
Leopold	Leopolda	k1gFnPc2
<g/>
,	,	kIx,
Duke	Duke	k1gFnPc2
of	of	k?
Brabant	Brabant	k1gMnSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Hemeroteca	Hemeroteca	k1gMnSc1
Digital	Digital	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Biblioteca	Biblioteca	k1gMnSc1
Nacional	Nacional	k1gMnSc1
de	de	k?
Españ	Españ	k1gMnSc1
<g/>
.	.	kIx.
hemerotecadigital	hemerotecadigital	k1gMnSc1
<g/>
.	.	kIx.
<g/>
bne	bne	k?
<g/>
.	.	kIx.
<g/>
es	es	k1gNnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Galerie	galerie	k1gFnSc1
Leopold	Leopold	k1gMnSc1
Belgický	belgický	k2eAgMnSc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
