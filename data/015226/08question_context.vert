<s>
Princ	princ	k1gMnSc1
Leopold	Leopold	k1gMnSc1
Belgický	belgický	k2eAgMnSc1d1
<g/>
,	,	kIx,
vévoda	vévoda	k1gMnSc1
z	z	k7c2
Brabantu	Brabant	k1gInSc2
<g/>
,	,	kIx,
hrabě	hrabě	k1gMnSc1
z	z	k7c2
Henegavska	Henegavsko	k1gNnSc2
(	(	kIx(
<g/>
12	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1859	#num#	k4
-	-	kIx~
22	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1869	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
druhým	druhý	k4xOgMnSc7
dítětem	dítě	k1gNnSc7
a	a	k8xC
jediným	jediný	k2eAgMnSc7d1
synem	syn	k1gMnSc7
krále	král	k1gMnSc2
Leopolda	Leopold	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
</s>