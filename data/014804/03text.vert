<s>
Aniontová	Aniontový	k2eAgFnSc1d1
adiční	adiční	k2eAgFnSc1d1
polymerizace	polymerizace	k1gFnSc1
</s>
<s>
Aniontová	Aniontový	k2eAgFnSc1d1
adiční	adiční	k2eAgFnSc1d1
polymerizace	polymerizace	k1gFnSc1
je	být	k5eAaImIp3nS
druh	druh	k1gInSc1
polymerizační	polymerizační	k2eAgFnSc2d1
reakce	reakce	k1gFnSc2
<g/>
,	,	kIx,
patřící	patřící	k2eAgInPc1d1
mezi	mezi	k7c4
iontové	iontový	k2eAgFnPc4d1
polymerizace	polymerizace	k1gFnPc4
<g/>
,	,	kIx,
při	při	k7c6
které	který	k3yQgFnSc6,k3yRgFnSc6,k3yIgFnSc6
jsou	být	k5eAaImIp3nP
aktivními	aktivní	k2eAgInPc7d1
centry	centr	k1gInPc7
anionty	anion	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
mnoho	mnoho	k4c4
různých	různý	k2eAgNnPc2d1
využití	využití	k1gNnPc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
monomery	monomer	k1gInPc1
se	se	k3xPyFc4
však	však	k9
při	při	k7c6
ní	on	k3xPp3gFnSc6
nejčastěji	často	k6eAd3
používají	používat	k5eAaImIp3nP
vinylové	vinylový	k2eAgFnPc1d1
sloučeniny	sloučenina	k1gFnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
U	u	k7c2
některých	některý	k3yIgFnPc2
takových	takový	k3xDgFnPc2
reakcí	reakce	k1gFnPc2
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
ovlivňovat	ovlivňovat	k5eAaImF
strukturu	struktura	k1gFnSc4
a	a	k8xC
složení	složení	k1gNnSc4
produktů	produkt	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Produkt	produkt	k1gInSc1
redukčního	redukční	k2eAgNnSc2d1
párování	párování	k1gNnSc2
styrenu	styren	k1gInSc2
s	s	k7c7
lithiem	lithium	k1gNnSc7
<g/>
,	,	kIx,
1,4	1,4	k4
<g/>
-dilithio-	-dilithio-	k?
<g/>
1,4	1,4	k4
<g/>
-difenylbutan	-difenylbutan	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
původní	původní	k2eAgFnSc6d1
studii	studie	k1gFnSc6
Szwarc	Szwarc	k1gInSc1
zkoumal	zkoumat	k5eAaImAgInS
obdobnou	obdobný	k2eAgFnSc4d1
disodnou	disodný	k2eAgFnSc4d1
sloučeninu	sloučenina	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Roku	rok	k1gInSc2
1936	#num#	k4
Karl	Karl	k1gMnSc1
Ziegler	Ziegler	k1gMnSc1
navrhl	navrhnout	k5eAaPmAgMnS
aniontovou	aniontový	k2eAgFnSc4d1
polymerizaci	polymerizace	k1gFnSc4
styrenu	styren	k1gInSc2
a	a	k8xC
butadienu	butadien	k1gInSc2
postupnou	postupný	k2eAgFnSc7d1
adicí	adice	k1gFnSc7
monomeru	monomer	k1gInSc2
na	na	k7c4
alkyllithný	alkyllithný	k2eAgInSc4d1
iniciátor	iniciátor	k1gInSc4
probíhající	probíhající	k2eAgInSc4d1
bez	bez	k7c2
přenosu	přenos	k1gInSc2
řetězce	řetězec	k1gInSc2
či	či	k8xC
terminace	terminace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
dvacet	dvacet	k4xCc4
let	léto	k1gNnPc2
později	pozdě	k6eAd2
Szwarc	Szwarc	k1gInSc1
pozoroval	pozorovat	k5eAaImAgInS
„	„	k?
<g/>
živou	živá	k1gFnSc4
<g/>
“	“	k?
polymerizaci	polymerizace	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
jedné	jeden	k4xCgFnSc6
z	z	k7c2
průlomových	průlomový	k2eAgFnPc2d1
prací	práce	k1gFnPc2
v	v	k7c6
oblasti	oblast	k1gFnSc6
studia	studio	k1gNnSc2
polymerů	polymer	k1gInPc2
Szwarc	Szwarc	k1gInSc4
ukázal	ukázat	k5eAaPmAgInS
<g/>
,	,	kIx,
že	že	k8xS
při	při	k7c6
radikálové	radikálový	k2eAgFnSc6d1
adici	adice	k1gFnSc6
naftalenu	naftalen	k1gInSc2
na	na	k7c4
styren	styren	k1gInSc4
probíhá	probíhat	k5eAaImIp3nS
přesun	přesun	k1gInSc1
elektronů	elektron	k1gInPc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
vede	vést	k5eAaImIp3nS
k	k	k7c3
tvorbě	tvorba	k1gFnSc3
dianionu	dianion	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
rychle	rychle	k6eAd1
reaguje	reagovat	k5eAaBmIp3nS
se	s	k7c7
styrenem	styren	k1gInSc7
za	za	k7c2
vzniku	vznik	k1gInSc2
„	„	k?
<g/>
živého	živý	k1gMnSc2
<g/>
“	“	k?
polymeru	polymer	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důležité	důležitý	k2eAgNnSc1d1
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
při	při	k7c6
reakci	reakce	k1gFnSc6
použil	použít	k5eAaPmAgMnS
aprotické	aprotický	k2eAgNnSc4d1
rozpouštědlo	rozpouštědlo	k1gNnSc4
<g/>
,	,	kIx,
konkrétně	konkrétně	k6eAd1
tetrahydrofuran	tetrahydrofuran	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Szwarc	Szwarc	k1gInSc1
zdůraznil	zdůraznit	k5eAaPmAgInS
<g/>
,	,	kIx,
že	že	k8xS
kinetika	kinetika	k1gFnSc1
a	a	k8xC
termodynamika	termodynamika	k1gFnSc1
mají	mít	k5eAaImIp3nP
velký	velký	k2eAgInSc4d1
význam	význam	k1gInSc4
v	v	k7c6
tomto	tento	k3xDgInSc6
ději	děj	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
stejné	stejný	k2eAgFnSc6d1
době	doba	k1gFnSc6
zkoumal	zkoumat	k5eAaImAgInS
vzájemná	vzájemný	k2eAgNnPc4d1
působení	působení	k1gNnSc4
několika	několik	k4yIc2
iontových	iontový	k2eAgInPc2d1
párů	pár	k1gInPc2
a	a	k8xC
radikálových	radikálový	k2eAgInPc2d1
iontů	ion	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
práce	práce	k1gFnSc1
vedla	vést	k5eAaImAgFnS
k	k	k7c3
objevu	objev	k1gInSc3
použitelné	použitelný	k2eAgFnSc2d1
syntézy	syntéza	k1gFnSc2
polymerů	polymer	k1gInPc2
s	s	k7c7
možností	možnost	k1gFnSc7
ovlivňovat	ovlivňovat	k5eAaImF
jejich	jejich	k3xOp3gFnPc1
molekulové	molekulový	k2eAgFnPc1d1
hmotnosti	hmotnost	k1gFnPc1
<g/>
,	,	kIx,
rozdělení	rozdělení	k1gNnSc1
molekulových	molekulový	k2eAgFnPc2d1
hmotností	hmotnost	k1gFnPc2
a	a	k8xC
strukturu	struktura	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Použití	použití	k1gNnSc1
alkalických	alkalický	k2eAgInPc2d1
kovů	kov	k1gInPc2
k	k	k7c3
iniciaci	iniciace	k1gFnSc3
polymerizace	polymerizace	k1gFnSc1
1,3	1,3	k4
<g/>
-dienů	-dien	k1gMnPc2
vedlo	vést	k5eAaImAgNnS
k	k	k7c3
objevu	objev	k1gInSc3
cis-	cis-	k?
<g/>
1,4	1,4	k4
<g/>
-polyizoprenu	-polyizopreno	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Tento	tento	k3xDgInSc1
objev	objev	k1gInSc1
nastartoval	nastartovat	k5eAaPmAgInS
vývoj	vývoj	k1gInSc1
průmyslových	průmyslový	k2eAgFnPc2d1
aniontových	aniontův	k2eAgFnPc2d1
polymerizací	polymerizace	k1gFnPc2
s	s	k7c7
využitím	využití	k1gNnSc7
alkyllithných	alkyllithný	k2eAgMnPc2d1
iniciátorů	iniciátor	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vlastnosti	vlastnost	k1gFnPc1
monomeru	monomer	k1gInSc2
</s>
<s>
Monomery	monomer	k1gInPc1
použitelné	použitelný	k2eAgInPc1d1
při	při	k7c6
aniontové	aniontový	k2eAgFnSc6d1
polymerizaci	polymerizace	k1gFnSc6
lze	lze	k6eAd1
rozdělit	rozdělit	k5eAaPmF
do	do	k7c2
dvou	dva	k4xCgFnPc2
větších	veliký	k2eAgFnPc2d2
skupin	skupina	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vinylové	vinylový	k2eAgInPc1d1
monomery	monomer	k1gInPc1
mají	mít	k5eAaImIp3nP
obecný	obecný	k2eAgInSc4d1
vzorec	vzorec	k1gInSc4
CH	Ch	kA
<g/>
2	#num#	k4
<g/>
=	=	kIx~
<g/>
CHR	chr	k0
<g/>
,	,	kIx,
nejvýznamnější	významný	k2eAgNnSc1d3
z	z	k7c2
nich	on	k3xPp3gMnPc2
jsou	být	k5eAaImIp3nP
styren	styren	k1gInSc4
(	(	kIx(
<g/>
C	C	kA
<g/>
6	#num#	k4
<g/>
H	H	kA
<g/>
5	#num#	k4
<g/>
-CH	-CH	k?
<g/>
=	=	kIx~
<g/>
CH	Ch	kA
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
buta-	buta-	k?
<g/>
1,3	1,3	k4
<g/>
-dien	-dien	k1gInSc1
(	(	kIx(
<g/>
CH	Ch	kA
<g/>
2	#num#	k4
<g/>
=	=	kIx~
<g/>
CH-CH	CH-CH	k1gMnSc1
<g/>
=	=	kIx~
<g/>
CH	Ch	kA
<g/>
2	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
isopren	isopren	k1gInSc1
(	(	kIx(
<g/>
CH	Ch	kA
<g/>
2	#num#	k4
<g/>
=	=	kIx~
<g/>
C	C	kA
<g/>
(	(	kIx(
<g/>
CH	Ch	kA
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
CH	Ch	kA
<g/>
=	=	kIx~
<g/>
CH	Ch	kA
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Patří	patřit	k5eAaImIp3nS
sem	sem	k6eAd1
také	také	k9
vinylpyridin	vinylpyridin	k1gInSc1
<g/>
,	,	kIx,
vinylsulfony	vinylsulfon	k1gInPc1
<g/>
,	,	kIx,
vinylsulfoxidy	vinylsulfoxid	k1gInPc1
a	a	k8xC
vinylsilany	vinylsilan	k1gInPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Druhou	druhý	k4xOgFnSc7
skupinou	skupina	k1gFnSc7
jsou	být	k5eAaImIp3nP
akrylátové	akrylátový	k2eAgInPc1d1
estery	ester	k1gInPc1
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
akrylonitril	akrylonitril	k1gInSc1
<g/>
,	,	kIx,
methakryláty	methakrylát	k1gInPc1
<g/>
,	,	kIx,
kyanoakryláty	kyanoakrylát	k1gInPc1
a	a	k8xC
akrolein	akrolein	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Příklady	příklad	k1gInPc1
polárních	polární	k2eAgInPc2d1
monomerů	monomer	k1gInPc2
</s>
<s>
Příklady	příklad	k1gInPc1
vinylových	vinylový	k2eAgInPc2d1
monomerů	monomer	k1gInPc2
</s>
<s>
Cyklické	cyklický	k2eAgInPc1d1
monomery	monomer	k1gInPc1
</s>
<s>
Aniontová	Aniontový	k2eAgFnSc1d1
polymerizace	polymerizace	k1gFnSc1
s	s	k7c7
otevíraním	otevíraní	k1gNnSc7
kruhu	kruh	k1gInSc2
u	u	k7c2
ε	ε	k1gInSc2
iniciovaná	iniciovaný	k2eAgFnSc1d1
alkoxidem	alkoxid	k1gMnSc7
</s>
<s>
Hexamethylcyklotrisiloxan	Hexamethylcyklotrisiloxan	k1gInSc1
je	být	k5eAaImIp3nS
cyklický	cyklický	k2eAgInSc1d1
monomer	monomer	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
se	se	k3xPyFc4
polymerizuje	polymerizovat	k5eAaBmIp3nS
za	za	k7c2
vzniku	vznik	k1gInSc2
siloxanové	siloxanový	k2eAgInPc1d1
polymery	polymer	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
Aby	aby	kYmCp3nS
mohlo	moct	k5eAaImAgNnS
dojít	dojít	k5eAaPmF
k	k	k7c3
polymeraci	polymerace	k1gFnSc3
u	u	k7c2
vinylových	vinylový	k2eAgFnPc2d1
sloučenin	sloučenina	k1gFnPc2
<g/>
,	,	kIx,
tak	tak	k6eAd1
musí	muset	k5eAaImIp3nP
substituenty	substituent	k1gInPc1
na	na	k7c6
dvojné	dvojný	k2eAgFnSc6d1
vazbě	vazba	k1gFnSc6
být	být	k5eAaImF
schopné	schopný	k2eAgFnPc1d1
stabilizovat	stabilizovat	k5eAaBmF
záporný	záporný	k2eAgInSc4d1
náboj	náboj	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stabilizace	stabilizace	k1gFnSc1
probíhá	probíhat	k5eAaImIp3nS
prostřednictvím	prostřednictví	k1gNnSc7
delokalizace	delokalizace	k1gFnSc2
tohoto	tento	k3xDgInSc2
náboje	náboj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
k	k	k7c3
vlastnostem	vlastnost	k1gFnPc3
karboaniontového	karboaniontový	k2eAgNnSc2d1
propagačního	propagační	k2eAgNnSc2d1
centra	centrum	k1gNnSc2
nesmí	smět	k5eNaImIp3nS
být	být	k5eAaImF
přítomny	přítomen	k2eAgInPc4d1
substituenty	substituent	k1gInPc4
reagující	reagující	k2eAgMnSc1d1
se	s	k7c7
zásadou	zásada	k1gFnSc7
nebo	nebo	k8xC
s	s	k7c7
nukleofilem	nukleofil	k1gMnSc7
<g/>
,	,	kIx,
případně	případně	k6eAd1
musí	muset	k5eAaImIp3nP
být	být	k5eAaImF
chráněny	chráněn	k2eAgFnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Iniciace	iniciace	k1gFnSc1
</s>
<s>
Iniciátory	iniciátor	k1gInPc1
aniontové	aniontový	k2eAgFnSc2d1
polymerizace	polymerizace	k1gFnSc2
se	se	k3xPyFc4
vybírají	vybírat	k5eAaImIp3nP
podle	podle	k7c2
reaktivity	reaktivita	k1gFnSc2
monomerů	monomer	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
velmi	velmi	k6eAd1
elektrofilními	elektrofilní	k2eAgInPc7d1
monomery	monomer	k1gInPc7
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
jsou	být	k5eAaImIp3nP
kyanoakryláty	kyanoakrylát	k1gInPc1
<g/>
,	,	kIx,
se	se	k3xPyFc4
používají	používat	k5eAaImIp3nP
pouze	pouze	k6eAd1
slabé	slabý	k2eAgMnPc4d1
nukleofily	nukleofil	k1gMnPc4
<g/>
,	,	kIx,
například	například	k6eAd1
aminy	amin	k1gInPc4
<g/>
,	,	kIx,
fosfiny	fosfin	k1gInPc4
<g/>
,	,	kIx,
nebo	nebo	k8xC
dokonce	dokonce	k9
alkylhalogenidy	alkylhalogenida	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
méně	málo	k6eAd2
reaktivních	reaktivní	k2eAgInPc2d1
monomerů	monomer	k1gInPc2
<g/>
,	,	kIx,
například	například	k6eAd1
styrenu	styren	k1gInSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
použít	použít	k5eAaPmF
silné	silný	k2eAgMnPc4d1
nukleofily	nukleofil	k1gMnPc4
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
N-butyllithium	N-butyllithium	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
reaktantů	reaktant	k1gMnPc2
střední	střední	k2eAgFnSc2d1
reaktivity	reaktivita	k1gFnSc2
se	se	k3xPyFc4
používají	používat	k5eAaImIp3nP
i	i	k9
středně	středně	k6eAd1
reaktivní	reaktivní	k2eAgMnPc4d1
nukleofily	nukleofil	k1gMnPc4
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
například	například	k6eAd1
vinylpyridin	vinylpyridin	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Použité	použitý	k2eAgNnSc1d1
rozpouštědlo	rozpouštědlo	k1gNnSc1
se	se	k3xPyFc4
určuje	určovat	k5eAaImIp3nS
podle	podle	k7c2
reaktivity	reaktivita	k1gFnSc2
iniciátoru	iniciátor	k1gInSc2
a	a	k8xC
vlastností	vlastnost	k1gFnPc2
vznikajícího	vznikající	k2eAgInSc2d1
řetězce	řetězec	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
aniontových	aniontův	k2eAgFnPc2d1
sloučenin	sloučenina	k1gFnPc2
s	s	k7c7
nízkou	nízký	k2eAgFnSc7d1
reaktivitou	reaktivita	k1gFnSc7
<g/>
,	,	kIx,
například	například	k6eAd1
heterocyklických	heterocyklický	k2eAgInPc2d1
monomerů	monomer	k1gInPc2
<g/>
,	,	kIx,
lze	lze	k6eAd1
použít	použít	k5eAaPmF
mnoho	mnoho	k4c1
různých	různý	k2eAgNnPc2d1
rozpouštědel	rozpouštědlo	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Iniciace	iniciace	k1gFnSc2
přesunem	přesun	k1gInSc7
elektronů	elektron	k1gInPc2
</s>
<s>
Iniciace	iniciace	k1gFnSc1
polymerizace	polymerizace	k1gFnSc2
styrenu	styren	k1gInSc2
naftalenidem	naftalenid	k1gInSc7
sodným	sodný	k2eAgInSc7d1
probíhá	probíhat	k5eAaImIp3nS
skrz	skrz	k7c4
přesun	přesun	k1gInSc4
elektronů	elektron	k1gInPc2
z	z	k7c2
naftalenového	naftalenový	k2eAgInSc2d1
radikálového	radikálový	k2eAgInSc2d1
aniontu	anion	k1gInSc2
na	na	k7c4
monomer	monomer	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzniklý	vzniklý	k2eAgInSc1d1
radikál	radikál	k1gInSc1
se	se	k3xPyFc4
dimerizuje	dimerizovat	k5eAaImIp3nS,k5eAaPmIp3nS,k5eAaBmIp3nS
na	na	k7c4
dilithiosloučeninu	dilithiosloučenina	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
následně	následně	k6eAd1
funguje	fungovat	k5eAaImIp3nS
jako	jako	k9
iniciátor	iniciátor	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
tomto	tento	k3xDgInSc6
postupu	postup	k1gInSc6
je	být	k5eAaImIp3nS
nutné	nutný	k2eAgNnSc1d1
použít	použít	k5eAaPmF
polární	polární	k2eAgNnSc4d1
rozpouštědlo	rozpouštědlo	k1gNnSc4
<g/>
,	,	kIx,
potřebná	potřebný	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
i	i	k9
stabilita	stabilita	k1gFnSc1
radikálového	radikálový	k2eAgInSc2d1
aniontu	anion	k1gInSc2
a	a	k8xC
solvatace	solvatace	k1gFnSc2
vznikajících	vznikající	k2eAgInPc2d1
kationtů	kation	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Poté	poté	k6eAd1
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
přesunout	přesunout	k5eAaPmF
elektron	elektron	k1gInSc1
z	z	k7c2
radikálového	radikálový	k2eAgInSc2d1
aniontu	anion	k1gInSc2
na	na	k7c4
monomer	monomer	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Při	při	k7c6
iniciaci	iniciace	k1gFnSc6
může	moct	k5eAaImIp3nS
rovněž	rovněž	k9
dojít	dojít	k5eAaPmF
k	k	k7c3
přesunu	přesun	k1gInSc3
elektronů	elektron	k1gInPc2
z	z	k7c2
alkalického	alkalický	k2eAgInSc2d1
kovu	kov	k1gInSc2
na	na	k7c4
monomer	monomer	k1gInSc4
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
také	také	k9
vzniká	vznikat	k5eAaImIp3nS
radikálový	radikálový	k2eAgInSc4d1
anion	anion	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Iniciátor	iniciátor	k1gInSc1
se	se	k3xPyFc4
objevuje	objevovat	k5eAaImIp3nS
na	na	k7c4
poverchu	povercha	k1gFnSc4
kovu	kov	k1gInSc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
vratnému	vratný	k2eAgInSc3d1
přesunu	přesun	k1gInSc3
elektronu	elektron	k1gInSc2
na	na	k7c4
adsorbovaný	adsorbovaný	k2eAgInSc4d1
monomer	monomer	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Iniciace	iniciace	k1gFnSc2
silnými	silný	k2eAgInPc7d1
anionty	anion	k1gInPc7
</s>
<s>
K	k	k7c3
nukleofilním	nukleofilní	k2eAgMnPc3d1
iniciátorům	iniciátor	k1gMnPc3
patří	patřit	k5eAaImIp3nP
kovalentní	kovalentní	k2eAgInPc1d1
i	i	k8xC
iontové	iontový	k2eAgInPc1d1
amidy	amid	k1gInPc1
kovů	kov	k1gInPc2
<g/>
,	,	kIx,
alkoxidy	alkoxid	k1gInPc1
<g/>
,	,	kIx,
hydroxidy	hydroxid	k1gInPc1
<g/>
,	,	kIx,
kyanidy	kyanid	k1gInPc1
<g/>
,	,	kIx,
fosfiny	fosfin	k1gInPc1
<g/>
,	,	kIx,
aminy	amin	k1gInPc1
a	a	k8xC
organokovové	organokovový	k2eAgFnPc1d1
sloučeniny	sloučenina	k1gFnPc1
(	(	kIx(
<g/>
alkyllithné	alkyllithný	k2eAgFnPc1d1
sloučeniny	sloučenina	k1gFnPc1
a	a	k8xC
Grignardova	Grignardův	k2eAgNnPc1d1
činidla	činidlo	k1gNnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
POři	POři	k1gNnSc7
iniciaci	iniciace	k1gFnSc4
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
adici	adice	k1gFnSc3
neutrálního	neutrální	k2eAgNnSc2d1
(	(	kIx(
<g/>
B	B	kA
<g/>
:	:	kIx,
<g/>
)	)	kIx)
nebo	nebo	k8xC
záporného	záporný	k2eAgNnSc2d1
(	(	kIx(
<g/>
B	B	kA
<g/>
:	:	kIx,
<g/>
-	-	kIx~
<g/>
)	)	kIx)
nukleofilu	nukleofil	k1gMnSc3
na	na	k7c4
monomer	monomer	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nejčastěji	často	k6eAd3
se	se	k3xPyFc4
používají	používat	k5eAaImIp3nP
alkyllithné	alkyllithný	k2eAgFnPc1d1
sloučeniny	sloučenina	k1gFnPc1
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
obvykle	obvykle	k6eAd1
při	při	k7c6
polymerizaci	polymerizace	k1gFnSc6
styrenů	styren	k1gInPc2
a	a	k8xC
dienů	dien	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Monomery	monomer	k1gInPc1
aktivované	aktivovaný	k2eAgInPc1d1
silně	silně	k6eAd1
elektronegativními	elektronegativní	k2eAgFnPc7d1
skupinami	skupina	k1gFnPc7
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
iniciovány	iniciován	k2eAgInPc4d1
slabými	slabý	k2eAgInPc7d1
anionty	anion	k1gInPc7
i	i	k8xC
neutrálními	neutrální	k2eAgMnPc7d1
nukleofily	nukleofil	k1gMnPc7
(	(	kIx(
<g/>
například	například	k6eAd1
aminy	amin	k1gInPc1
a	a	k8xC
fosfiny	fosfin	k1gInPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příkladem	příklad	k1gInSc7
je	být	k5eAaImIp3nS
polymerace	polymerace	k1gFnSc1
kyanoakrylátů	kyanoakrylát	k1gInPc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
na	na	k7c4
výrobu	výroba	k1gFnSc4
sloučenin	sloučenina	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc4,k3yIgInPc4,k3yQgInPc4
jsou	být	k5eAaImIp3nP
základními	základní	k2eAgFnPc7d1
složkami	složka	k1gFnPc7
sekundových	sekundový	k2eAgFnPc2d1
lepidel	lepidlo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
případě	případ	k1gInSc6
stačí	stačit	k5eAaBmIp3nS
stopová	stopový	k2eAgNnPc4d1
množství	množství	k1gNnPc4
nečistot	nečistota	k1gFnPc2
k	k	k7c3
iniciaci	iniciace	k1gFnSc3
aniontové	aniontový	k2eAgFnSc3d1
nebo	nebo	k8xC
zwitteriontové	zwitteriontový	k2eAgFnSc2d1
polymerace	polymerace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Propagace	propagace	k1gFnSc1
</s>
<s>
Polymerizace	polymerizace	k1gFnSc1
styrenu	styren	k1gInSc2
iniciovaná	iniciovaný	k2eAgFnSc1d1
organolithnými	organolithný	k2eAgFnPc7d1
sloučeninami	sloučenina	k1gFnPc7
</s>
<s>
Během	během	k7c2
propagace	propagace	k1gFnSc2
aniontové	aniontový	k2eAgFnSc2d1
adiční	adiční	k2eAgFnSc2d1
polymerizace	polymerizace	k1gFnSc2
dojde	dojít	k5eAaPmIp3nS
k	k	k7c3
úplnému	úplný	k2eAgNnSc3d1
spotřebování	spotřebování	k1gNnSc3
monomeru	monomer	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
fáze	fáze	k1gFnPc4
je	být	k5eAaImIp3nS
často	často	k6eAd1
velmi	velmi	k6eAd1
rychlá	rychlý	k2eAgFnSc1d1
i	i	k9
za	za	k7c2
nízkých	nízký	k2eAgFnPc2d1
teplot	teplota	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Anionic	Anionice	k1gFnPc2
addition	addition	k1gInSc1
polymerization	polymerization	k1gInSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
Hsieh	Hsieha	k1gFnPc2
<g/>
,	,	kIx,
H.	H.	kA
<g/>
;	;	kIx,
<g/>
Quirk	Quirk	k1gInSc1
<g/>
,	,	kIx,
R.	R.	kA
Anionic	Anionice	k1gFnPc2
Polymerization	Polymerization	k1gInSc1
<g/>
:	:	kIx,
Principles	Principles	k1gInSc1
and	and	k?
practical	practicat	k5eAaPmAgInS
applications	applications	k1gInSc1
<g/>
;	;	kIx,
Marcel	Marcel	k1gMnSc1
Dekker	Dekker	k1gMnSc1
<g/>
,	,	kIx,
Inc	Inc	k1gMnSc1
<g/>
:	:	kIx,
New	New	k1gFnSc1
York	York	k1gInSc1
<g/>
,	,	kIx,
1996.1	1996.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
9	#num#	k4
10	#num#	k4
Quirk	Quirk	k1gInSc1
<g/>
,	,	kIx,
R.	R.	kA
Anionic	Anionice	k1gFnPc2
Polymerization	Polymerization	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
Encyclopedia	Encyclopedium	k1gNnSc2
of	of	k?
Polymer	polymer	k1gInSc1
Science	Science	k1gFnSc1
and	and	k?
Technology	technolog	k1gMnPc7
<g/>
;	;	kIx,
John	John	k1gMnSc1
Wiley	Wilea	k1gFnSc2
and	and	k?
Sons	Sons	k1gInSc1
<g/>
:	:	kIx,
New	New	k1gFnSc1
York	York	k1gInSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Sebastian	Sebastian	k1gMnSc1
Koltzenburg	Koltzenburg	k1gMnSc1
<g/>
;	;	kIx,
Michael	Michael	k1gMnSc1
Maskos	Maskos	k1gMnSc1
<g/>
;	;	kIx,
Oskar	Oskar	k1gMnSc1
Nuyken	Nuyken	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Polymer	polymer	k1gInSc1
Chemistry	Chemistr	k1gMnPc4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Springer	Springer	k1gInSc1
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
662	#num#	k4
<g/>
-	-	kIx~
<g/>
49279	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
M.	M.	kA
Szwarc	Szwarc	k1gInSc1
<g/>
;	;	kIx,
M.	M.	kA
Levy	Levy	k?
<g/>
;	;	kIx,
R.	R.	kA
Milkovich	Milkovich	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Polymerization	Polymerization	k1gInSc1
Initiated	Initiated	k1gInSc4
by	by	k9
Electron	Electron	k1gInSc4
Transfer	transfer	k1gInSc1
to	ten	k3xDgNnSc4
Monomer	monomer	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
New	New	k1gFnPc1
Method	Method	k1gInSc1
of	of	k?
Formation	Formation	k1gInSc1
of	of	k?
Block	Block	k1gInSc1
Polymers	Polymers	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Journal	Journal	k1gFnSc1
of	of	k?
the	the	k?
American	American	k1gInSc4
Chemical	Chemical	k1gMnSc2
Society	societa	k1gFnSc2
<g/>
.	.	kIx.
1956	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
2656	#num#	k4
<g/>
-	-	kIx~
<g/>
2657	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
M.	M.	kA
Szwarc	Szwarc	k1gInSc1
<g/>
.	.	kIx.
"	"	kIx"
<g/>
Living	Living	k1gInSc1
<g/>
"	"	kIx"
polymers	polymers	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nature	Natur	k1gMnSc5
<g/>
.	.	kIx.
1956	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
1168	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Smid	Smid	k1gInSc1
<g/>
,	,	kIx,
J.	J.	kA
Historical	Historical	k1gMnSc1
Perspectives	Perspectives	k1gMnSc1
on	on	k3xPp3gMnSc1
Living	Living	k1gInSc1
Anionic	Anionice	k1gFnPc2
Polymerization	Polymerization	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
J.	J.	kA
Polym	Polym	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sci	Sci	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Part	part	k1gInSc1
A.	A.	kA
<g/>
;	;	kIx,
2002	#num#	k4
<g/>
,	,	kIx,
40	#num#	k4
<g/>
,	,	kIx,
<g/>
pp	pp	k?
<g/>
.	.	kIx.
2101	#num#	k4
<g/>
-	-	kIx~
<g/>
2107	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
=	=	kIx~
<g/>
10.100	10.100	k4
<g/>
2	#num#	k4
<g/>
/	/	kIx~
<g/>
pola	pola	k1gFnSc1
<g/>
.102861	.102861	k4
2	#num#	k4
3	#num#	k4
Odian	Odiana	k1gFnPc2
<g/>
,	,	kIx,
G.	G.	kA
Ionic	Ionic	k1gMnSc1
Chain	Chain	k1gMnSc1
Polymerization	Polymerization	k1gInSc1
<g/>
;	;	kIx,
In	In	k1gMnSc1
Principles	Principles	k1gMnSc1
of	of	k?
Polymerization	Polymerization	k1gInSc1
<g/>
;	;	kIx,
Wiley-Interscience	Wiley-Interscience	k1gFnSc1
<g/>
:	:	kIx,
Staten	Staten	k2eAgInSc1d1
Island	Island	k1gInSc1
<g/>
,	,	kIx,
New	New	k1gFnSc1
York	York	k1gInSc1
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
,	,	kIx,
pp	pp	k?
<g/>
.	.	kIx.
372	#num#	k4
<g/>
-	-	kIx~
<g/>
463	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Chemie	chemie	k1gFnSc1
</s>
