<s>
Index	index	k1gInSc1	index
tělesné	tělesný	k2eAgFnSc2d1	tělesná
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
označovaný	označovaný	k2eAgInSc4d1	označovaný
zkratkou	zkratka	k1gFnSc7	zkratka
BMI	BMI	kA	BMI
(	(	kIx(	(
<g/>
z	z	k7c2	z
anglického	anglický	k2eAgInSc2d1	anglický
body	bod	k1gInPc7	bod
mass	mass	k6eAd1	mass
index	index	k1gInSc1	index
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
číslo	číslo	k1gNnSc4	číslo
používané	používaný	k2eAgNnSc4d1	používané
jako	jako	k8xC	jako
indikátor	indikátor	k1gInSc4	indikátor
podváhy	podváha	k1gFnSc2	podváha
<g/>
,	,	kIx,	,
normální	normální	k2eAgFnSc2d1	normální
tělesné	tělesný	k2eAgFnSc2d1	tělesná
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
,	,	kIx,	,
nadváhy	nadváha	k1gFnSc2	nadváha
a	a	k8xC	a
obezity	obezita	k1gFnSc2	obezita
<g/>
,	,	kIx,	,
umožňující	umožňující	k2eAgNnSc1d1	umožňující
statistické	statistický	k2eAgNnSc1d1	statistické
porovnávání	porovnávání	k1gNnSc1	porovnávání
tělesné	tělesný	k2eAgFnSc2d1	tělesná
hmotnosti	hmotnost	k1gFnSc2	hmotnost
lidí	člověk	k1gMnPc2	člověk
s	s	k7c7	s
různou	různý	k2eAgFnSc7d1	různá
výškou	výška	k1gFnSc7	výška
<g/>
.	.	kIx.	.
</s>
<s>
Výslednou	výsledný	k2eAgFnSc4d1	výsledná
hodnotu	hodnota	k1gFnSc4	hodnota
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
interpretovat	interpretovat	k5eAaBmF	interpretovat
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
věku	věk	k1gInSc6	věk
a	a	k8xC	a
pohlaví	pohlaví	k1gNnSc6	pohlaví
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
zatímco	zatímco	k8xS	zatímco
hodnota	hodnota	k1gFnSc1	hodnota
BMI	BMI	kA	BMI
<g/>
=	=	kIx~	=
<g/>
23	[number]	k4	23
znamená	znamenat	k5eAaImIp3nS	znamenat
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
10	[number]	k4	10
let	léto	k1gNnPc2	léto
obezitu	obezita	k1gFnSc4	obezita
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
15	[number]	k4	15
let	léto	k1gNnPc2	léto
jde	jít	k5eAaImIp3nS	jít
již	již	k6eAd1	již
o	o	k7c4	o
standardní	standardní	k2eAgFnSc4d1	standardní
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
upravit	upravit	k5eAaPmF	upravit
interpretaci	interpretace	k1gFnSc4	interpretace
pro	pro	k7c4	pro
obyvatele	obyvatel	k1gMnSc4	obyvatel
Asie	Asie	k1gFnSc1	Asie
(	(	kIx(	(
<g/>
jiná	jiný	k2eAgFnSc1d1	jiná
stavba	stavba	k1gFnSc1	stavba
těla	tělo	k1gNnSc2	tělo
<g/>
)	)	kIx)	)
a	a	k8xC	a
sportovce	sportovec	k1gMnSc4	sportovec
(	(	kIx(	(
<g/>
BMI	BMI	kA	BMI
nedělá	dělat	k5eNaImIp3nS	dělat
rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
obsahem	obsah	k1gInSc7	obsah
tuku	tuk	k1gInSc2	tuk
a	a	k8xC	a
svalů	sval	k1gInPc2	sval
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Index	index	k1gInSc1	index
BMI	BMI	kA	BMI
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
spočítat	spočítat	k5eAaPmF	spočítat
vydělením	vydělení	k1gNnSc7	vydělení
hmotnosti	hmotnost	k1gFnSc2	hmotnost
daného	daný	k2eAgMnSc2d1	daný
člověka	člověk	k1gMnSc2	člověk
druhou	druhý	k4xOgFnSc4	druhý
mocninou	mocnina	k1gFnSc7	mocnina
jeho	jeho	k3xOp3gFnSc2	jeho
výšky	výška	k1gFnSc2	výška
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
BMI	BMI	kA	BMI
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
hmotnost	hmotnost	k1gFnSc1	hmotnost
[	[	kIx(	[
<g/>
kg	kg	kA	kg
<g/>
]	]	kIx)	]
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
<g/>
výška	výška	k1gFnSc1	výška
[	[	kIx(	[
<g/>
m	m	kA	m
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mbox	mbox	k1gInSc1	mbox
<g/>
{	{	kIx(	{
<g/>
BMI	BMI	kA	BMI
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mbox	mbox	k1gInSc1	mbox
<g/>
{	{	kIx(	{
<g/>
hmotnost	hmotnost	k1gFnSc1	hmotnost
[	[	kIx(	[
<g/>
kg	kg	kA	kg
<g/>
]	]	kIx)	]
<g/>
}}	}}	k?	}}
<g/>
{{	{{	k?	{{
<g/>
\	\	kIx~	\
<g/>
mbox	mbox	k1gInSc1	mbox
<g/>
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
výška	výška	k1gFnSc1	výška
[	[	kIx(	[
<g/>
m	m	kA	m
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
}}	}}	k?	}}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}}	}}}}	k?	}}}}
:	:	kIx,	:
Do	do	k7c2	do
tohoto	tento	k3xDgInSc2	tento
vzorečku	vzoreček	k1gInSc2	vzoreček
se	se	k3xPyFc4	se
dosazuje	dosazovat	k5eAaImIp3nS	dosazovat
hmotnost	hmotnost	k1gFnSc1	hmotnost
v	v	k7c6	v
kilogramech	kilogram	k1gInPc6	kilogram
a	a	k8xC	a
výška	výška	k1gFnSc1	výška
v	v	k7c6	v
metrech	metr	k1gInPc6	metr
a	a	k8xC	a
výsledná	výsledný	k2eAgFnSc1d1	výsledná
jednotka	jednotka	k1gFnSc1	jednotka
kg	kg	kA	kg
<g/>
/	/	kIx~	/
<g/>
m2	m2	k4	m2
se	se	k3xPyFc4	se
často	často	k6eAd1	často
vynechává	vynechávat	k5eAaImIp3nS	vynechávat
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
stanovení	stanovení	k1gNnSc4	stanovení
hodnoty	hodnota	k1gFnSc2	hodnota
BMI	BMI	kA	BMI
se	se	k3xPyFc4	se
také	také	k9	také
používají	používat	k5eAaImIp3nP	používat
tabulky	tabulka	k1gFnPc1	tabulka
<g/>
,	,	kIx,	,
nomogramy	nomogram	k1gInPc1	nomogram
nebo	nebo	k8xC	nebo
počítačové	počítačový	k2eAgInPc1d1	počítačový
programy	program	k1gInPc1	program
<g/>
.	.	kIx.	.
</s>
<s>
Index	index	k1gInSc1	index
tělesné	tělesný	k2eAgFnSc2d1	tělesná
hmotnosti	hmotnost	k1gFnSc2	hmotnost
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
letech	let	k1gInPc6	let
1830	[number]	k4	1830
<g/>
–	–	k?	–
<g/>
1850	[number]	k4	1850
belgický	belgický	k2eAgMnSc1d1	belgický
matematik	matematik	k1gMnSc1	matematik
a	a	k8xC	a
statistik	statistik	k1gMnSc1	statistik
Adolphe	Adolph	k1gFnSc2	Adolph
Quetelet	Quetelet	k1gInSc1	Quetelet
při	při	k7c6	při
práci	práce	k1gFnSc6	práce
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
systému	systém	k1gInSc6	systém
"	"	kIx"	"
<g/>
sociální	sociální	k2eAgFnSc2d1	sociální
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
BMI	BMI	kA	BMI
někdy	někdy	k6eAd1	někdy
označuje	označovat	k5eAaImIp3nS	označovat
také	také	k9	také
jako	jako	k9	jako
Queteletův	Queteletův	k2eAgInSc1d1	Queteletův
index	index	k1gInSc1	index
<g/>
.	.	kIx.	.
</s>
<s>
BMI	BMI	kA	BMI
je	být	k5eAaImIp3nS	být
nejčastěji	často	k6eAd3	často
používaným	používaný	k2eAgInSc7d1	používaný
ukazatelem	ukazatel	k1gInSc7	ukazatel
<g/>
,	,	kIx,	,
používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
i	i	k9	i
jiné	jiný	k2eAgInPc1d1	jiný
indexy	index	k1gInPc1	index
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Brocův	Brocův	k2eAgInSc1d1	Brocův
index	index	k1gInSc1	index
<g/>
,	,	kIx,	,
Rohrerův	Rohrerův	k2eAgInSc1d1	Rohrerův
index	index	k1gInSc1	index
nebo	nebo	k8xC	nebo
WHR	WHR	kA	WHR
index	index	k1gInSc1	index
<g/>
.	.	kIx.	.
</s>
<s>
Brocův	Brocův	k2eAgInSc4d1	Brocův
a	a	k8xC	a
Rohrerův	Rohrerův	k2eAgInSc4d1	Rohrerův
index	index	k1gInSc4	index
vycházejí	vycházet	k5eAaImIp3nP	vycházet
rovněž	rovněž	k9	rovněž
z	z	k7c2	z
poměrů	poměr	k1gInPc2	poměr
výšky	výška	k1gFnSc2	výška
a	a	k8xC	a
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
WHR	WHR	kA	WHR
index	index	k1gInSc1	index
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
poměr	poměr	k1gInSc1	poměr
mezi	mezi	k7c7	mezi
pasem	pas	k1gInSc7	pas
a	a	k8xC	a
boky	bok	k1gInPc7	bok
<g/>
.	.	kIx.	.
</s>
<s>
Rohrerův	Rohrerův	k2eAgInSc1d1	Rohrerův
index	index	k1gInSc1	index
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
především	především	k9	především
v	v	k7c6	v
pediatrii	pediatrie	k1gFnSc6	pediatrie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
indexu	index	k1gInSc6	index
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
výška	výška	k1gFnSc1	výška
postavy	postava	k1gFnSc2	postava
se	s	k7c7	s
3	[number]	k4	3
<g/>
.	.	kIx.	.
mocninou	mocnina	k1gFnSc7	mocnina
(	(	kIx(	(
<g/>
index	index	k1gInSc1	index
má	mít	k5eAaImIp3nS	mít
pak	pak	k6eAd1	pak
fyzikální	fyzikální	k2eAgInSc1d1	fyzikální
rozměr	rozměr	k1gInSc1	rozměr
hustoty	hustota	k1gFnSc2	hustota
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
lépe	dobře	k6eAd2	dobře
vystihuje	vystihovat	k5eAaImIp3nS	vystihovat
odchylky	odchylka	k1gFnPc4	odchylka
i	i	k9	i
pro	pro	k7c4	pro
malé	malý	k2eAgFnPc4d1	malá
či	či	k8xC	či
vysoké	vysoký	k2eAgFnPc4d1	vysoká
postavy	postava	k1gFnPc4	postava
(	(	kIx(	(
<g/>
vyjma	vyjma	k7c2	vyjma
případu	případ	k1gInSc2	případ
novorozenců	novorozenec	k1gMnPc2	novorozenec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dospívání	dospívání	k1gNnSc6	dospívání
totiž	totiž	k8xC	totiž
index	index	k1gInSc1	index
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
neroste	růst	k5eNaImIp3nS	růst
s	s	k7c7	s
věkem	věk	k1gInSc7	věk
jako	jako	k8xC	jako
BMI	BMI	kA	BMI
(	(	kIx(	(
<g/>
ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
při	při	k7c6	při
dospívání	dospívání	k1gNnSc6	dospívání
zvětšuje	zvětšovat	k5eAaImIp3nS	zvětšovat
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
až	až	k9	až
o	o	k7c4	o
+1	+1	k4	+1
BMI	BMI	kA	BMI
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
negativní	negativní	k2eAgInSc4d1	negativní
jev	jev	k1gInSc4	jev
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
BMI	BMI	kA	BMI
se	se	k3xPyFc4	se
obecně	obecně	k6eAd1	obecně
dá	dát	k5eAaPmIp3nS	dát
považovat	považovat	k5eAaImF	považovat
pouze	pouze	k6eAd1	pouze
za	za	k7c4	za
statistický	statistický	k2eAgInSc4d1	statistický
nástroj	nástroj	k1gInSc4	nástroj
<g/>
,	,	kIx,	,
u	u	k7c2	u
konkrétního	konkrétní	k2eAgNnSc2d1	konkrétní
jedince	jedinko	k6eAd1	jedinko
je	být	k5eAaImIp3nS	být
BMI	BMI	kA	BMI
příliš	příliš	k6eAd1	příliš
jednoduchým	jednoduchý	k2eAgInSc7d1	jednoduchý
prostředkem	prostředek	k1gInSc7	prostředek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
ignoruje	ignorovat	k5eAaImIp3nS	ignorovat
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
důležitých	důležitý	k2eAgInPc2d1	důležitý
faktorů	faktor	k1gInPc2	faktor
(	(	kIx(	(
<g/>
např.	např.	kA	např.
stavbu	stavba	k1gFnSc4	stavba
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
množství	množství	k1gNnSc2	množství
svalstva	svalstvo	k1gNnSc2	svalstvo
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
klinické	klinický	k2eAgFnSc6d1	klinická
praxi	praxe	k1gFnSc6	praxe
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
obvykle	obvykle	k6eAd1	obvykle
používají	používat	k5eAaImIp3nP	používat
přesnější	přesný	k2eAgInPc4d2	přesnější
testy	test	k1gInPc4	test
jako	jako	k8xS	jako
měření	měření	k1gNnSc4	měření
tloušťky	tloušťka	k1gFnSc2	tloušťka
podkožního	podkožní	k2eAgInSc2d1	podkožní
tuku	tuk	k1gInSc2	tuk
<g/>
,	,	kIx,	,
impedanční	impedanční	k2eAgNnSc1d1	impedanční
měření	měření	k1gNnSc1	měření
atd.	atd.	kA	atd.
BMI	BMI	kA	BMI
je	být	k5eAaImIp3nS	být
nejužitečnější	užitečný	k2eAgFnSc1d3	nejužitečnější
pro	pro	k7c4	pro
statistické	statistický	k2eAgInPc4d1	statistický
průzkumy	průzkum	k1gInPc4	průzkum
mezi	mezi	k7c7	mezi
rozsáhlejšími	rozsáhlý	k2eAgInPc7d2	rozsáhlejší
vzorky	vzorek	k1gInPc7	vzorek
populace	populace	k1gFnSc2	populace
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
pro	pro	k7c4	pro
zkoumání	zkoumání	k1gNnSc4	zkoumání
korelace	korelace	k1gFnSc2	korelace
mezi	mezi	k7c7	mezi
obezitou	obezita	k1gFnSc7	obezita
a	a	k8xC	a
jinými	jiný	k2eAgInPc7d1	jiný
faktory	faktor	k1gInPc7	faktor
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
pro	pro	k7c4	pro
použití	použití	k1gNnSc4	použití
BMI	BMI	kA	BMI
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gNnSc4	jeho
použití	použití	k1gNnSc4	použití
stačí	stačit	k5eAaBmIp3nS	stačit
v	v	k7c6	v
datech	datum	k1gNnPc6	datum
uvádět	uvádět	k5eAaImF	uvádět
výšku	výška	k1gFnSc4	výška
a	a	k8xC	a
hmotnost	hmotnost	k1gFnSc4	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
v	v	k7c6	v
datech	datum	k1gNnPc6	datum
byly	být	k5eAaImAgFnP	být
dostupné	dostupný	k2eAgInPc1d1	dostupný
např.	např.	kA	např.
výsledky	výsledek	k1gInPc1	výsledek
měření	měření	k1gNnSc4	měření
bioimpedance	bioimpedance	k1gFnSc2	bioimpedance
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
by	by	kYmCp3nS	by
BMI	BMI	kA	BMI
zbytečný	zbytečný	k2eAgMnSc1d1	zbytečný
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
však	však	k9	však
pro	pro	k7c4	pro
běžné	běžný	k2eAgInPc4d1	běžný
průzkumy	průzkum	k1gInPc4	průzkum
obvykle	obvykle	k6eAd1	obvykle
neplatí	platit	k5eNaImIp3nP	platit
<g/>
.	.	kIx.	.
</s>
<s>
Výpočet	výpočet	k1gInSc1	výpočet
BMI	BMI	kA	BMI
pro	pro	k7c4	pro
konkrétního	konkrétní	k2eAgMnSc4d1	konkrétní
jednotlivce	jednotlivec	k1gMnSc4	jednotlivec
nelze	lze	k6eNd1	lze
proto	proto	k8xC	proto
brát	brát	k5eAaImF	brát
jako	jako	k8xS	jako
absolutní	absolutní	k2eAgInSc4d1	absolutní
ukazatel	ukazatel	k1gInSc4	ukazatel
<g/>
,	,	kIx,	,
spíše	spíše	k9	spíše
jen	jen	k9	jen
jako	jako	k9	jako
přibližné	přibližný	k2eAgNnSc4d1	přibližné
vodítko	vodítko	k1gNnSc4	vodítko
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
použito	použít	k5eAaPmNgNnS	použít
jen	jen	k6eAd1	jen
jako	jako	k8xS	jako
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
více	hodně	k6eAd2	hodně
prostředků	prostředek	k1gInPc2	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
populaci	populace	k1gFnSc6	populace
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
hodnoty	hodnota	k1gFnPc1	hodnota
indexu	index	k1gInSc2	index
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
od	od	k7c2	od
přibližně	přibližně	k6eAd1	přibližně
15	[number]	k4	15
(	(	kIx(	(
<g/>
závažná	závažný	k2eAgFnSc1d1	závažná
podvýživa	podvýživa	k1gFnSc1	podvýživa
<g/>
)	)	kIx)	)
až	až	k9	až
přes	přes	k7c4	přes
40	[number]	k4	40
(	(	kIx(	(
<g/>
morbidní	morbidní	k2eAgFnSc1d1	morbidní
obezita	obezita	k1gFnSc1	obezita
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přesné	přesný	k2eAgFnPc1d1	přesná
hranice	hranice	k1gFnPc1	hranice
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgFnPc7d1	jednotlivá
kategoriemi	kategorie	k1gFnPc7	kategorie
(	(	kIx(	(
<g/>
závažná	závažný	k2eAgFnSc1d1	závažná
podvýživa	podvýživa	k1gFnSc1	podvýživa
<g/>
,	,	kIx,	,
podvýživa	podvýživa	k1gFnSc1	podvýživa
<g/>
,	,	kIx,	,
optimální	optimální	k2eAgFnSc1d1	optimální
váha	váha	k1gFnSc1	váha
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
různými	různý	k2eAgMnPc7d1	různý
odborníky	odborník	k1gMnPc7	odborník
liší	lišit	k5eAaImIp3nS	lišit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
všeobecně	všeobecně	k6eAd1	všeobecně
je	být	k5eAaImIp3nS	být
BMI	BMI	kA	BMI
pod	pod	k7c4	pod
18,5	[number]	k4	18,5
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
podváhu	podváha	k1gFnSc4	podváha
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
příznakem	příznak	k1gInSc7	příznak
nějaké	nějaký	k3yIgFnSc2	nějaký
poruchy	porucha	k1gFnSc2	porucha
stravování	stravování	k1gNnSc2	stravování
či	či	k8xC	či
jiného	jiný	k2eAgInSc2d1	jiný
zdravotního	zdravotní	k2eAgInSc2d1	zdravotní
problému	problém	k1gInSc2	problém
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
BMI	BMI	kA	BMI
nad	nad	k7c7	nad
25	[number]	k4	25
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
nadváhu	nadváha	k1gFnSc4	nadváha
a	a	k8xC	a
nad	nad	k7c7	nad
30	[number]	k4	30
za	za	k7c4	za
příznak	příznak	k1gInSc4	příznak
obezity	obezita	k1gFnSc2	obezita
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
hranice	hranice	k1gFnPc1	hranice
platí	platit	k5eAaImIp3nP	platit
pro	pro	k7c4	pro
dospělé	dospělí	k1gMnPc4	dospělí
starší	starý	k2eAgMnPc1d2	starší
20	[number]	k4	20
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Běžně	běžně	k6eAd1	běžně
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
následující	následující	k2eAgFnPc4d1	následující
hranice	hranice	k1gFnPc4	hranice
<g/>
:	:	kIx,	:
Jak	jak	k8xC	jak
už	už	k6eAd1	už
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
uvedeno	uveden	k2eAgNnSc1d1	uvedeno
<g/>
,	,	kIx,	,
BMI	BMI	kA	BMI
je	být	k5eAaImIp3nS	být
pouhým	pouhý	k2eAgInSc7d1	pouhý
statistickým	statistický	k2eAgInSc7d1	statistický
nástrojem	nástroj	k1gInSc7	nástroj
<g/>
,	,	kIx,	,
u	u	k7c2	u
konkrétních	konkrétní	k2eAgFnPc2d1	konkrétní
osob	osoba	k1gFnPc2	osoba
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
klinický	klinický	k2eAgInSc1d1	klinický
stav	stav	k1gInSc1	stav
lišit	lišit	k5eAaImF	lišit
od	od	k7c2	od
významu	význam	k1gInSc2	význam
naměřené	naměřený	k2eAgFnSc2d1	naměřená
hodnoty	hodnota	k1gFnSc2	hodnota
BMI	BMI	kA	BMI
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
kulturista	kulturista	k1gMnSc1	kulturista
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
hodnotu	hodnota	k1gFnSc4	hodnota
BMI	BMI	kA	BMI
nad	nad	k7c7	nad
30	[number]	k4	30
a	a	k8xC	a
přesto	přesto	k8xC	přesto
nebýt	být	k5eNaImF	být
obézní	obézní	k2eAgMnSc1d1	obézní
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
vysoká	vysoký	k2eAgFnSc1d1	vysoká
hodnota	hodnota	k1gFnSc1	hodnota
indexu	index	k1gInSc2	index
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
něj	on	k3xPp3gMnSc2	on
dána	dát	k5eAaPmNgFnS	dát
velkým	velký	k2eAgNnSc7d1	velké
množstvím	množství	k1gNnSc7	množství
svalové	svalový	k2eAgFnSc2d1	svalová
hmoty	hmota	k1gFnSc2	hmota
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
starší	starý	k2eAgMnPc1d2	starší
lidé	člověk	k1gMnPc1	člověk
s	s	k7c7	s
malým	malý	k2eAgNnSc7d1	malé
množstvím	množství	k1gNnSc7	množství
svalstva	svalstvo	k1gNnSc2	svalstvo
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
ze	z	k7c2	z
zdravotního	zdravotní	k2eAgNnSc2d1	zdravotní
hlediska	hledisko	k1gNnSc2	hledisko
obézní	obézní	k2eAgMnPc1d1	obézní
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
jejich	jejich	k3xOp3gFnSc1	jejich
BMI	BMI	kA	BMI
je	být	k5eAaImIp3nS	být
řadí	řadit	k5eAaImIp3nS	řadit
do	do	k7c2	do
kategorie	kategorie	k1gFnSc2	kategorie
ideální	ideální	k2eAgFnSc2d1	ideální
váhy	váha	k1gFnSc2	váha
<g/>
.	.	kIx.	.
</s>
<s>
Hranice	hranice	k1gFnSc1	hranice
hodnot	hodnota	k1gFnPc2	hodnota
BMI	BMI	kA	BMI
se	se	k3xPyFc4	se
také	také	k9	také
liší	lišit	k5eAaImIp3nP	lišit
pro	pro	k7c4	pro
různé	různý	k2eAgFnPc4d1	různá
populace	populace	k1gFnPc4	populace
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
Asiaté	Asiat	k1gMnPc1	Asiat
používají	používat	k5eAaImIp3nP	používat
o	o	k7c4	o
něco	něco	k3yInSc4	něco
nižší	nízký	k2eAgFnSc1d2	nižší
hranice	hranice	k1gFnSc1	hranice
<g/>
,	,	kIx,	,
za	za	k7c4	za
obézní	obézní	k2eAgNnSc4d1	obézní
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
považují	považovat	k5eAaImIp3nP	považovat
již	již	k9	již
lidé	člověk	k1gMnPc1	člověk
s	s	k7c7	s
BMI	BMI	kA	BMI
nad	nad	k7c7	nad
27,5	[number]	k4	27,5
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
ideální	ideální	k2eAgMnSc1d1	ideální
se	se	k3xPyFc4	se
stanoví	stanovit	k5eAaPmIp3nS	stanovit
BMI	BMI	kA	BMI
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
18,5	[number]	k4	18,5
<g/>
–	–	k?	–
<g/>
23	[number]	k4	23
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
nejatraktivnější	atraktivní	k2eAgFnSc4d3	nejatraktivnější
(	(	kIx(	(
<g/>
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
kultuře	kultura	k1gFnSc6	kultura
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
BMI	BMI	kA	BMI
okolo	okolo	k7c2	okolo
19	[number]	k4	19
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
na	na	k7c6	na
dolní	dolní	k2eAgFnSc6d1	dolní
hranici	hranice	k1gFnSc6	hranice
ideální	ideální	k2eAgFnSc2d1	ideální
váhy	váha	k1gFnSc2	váha
s	s	k7c7	s
podváhou	podváha	k1gFnSc7	podváha
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
atraktivita	atraktivita	k1gFnSc1	atraktivita
je	být	k5eAaImIp3nS	být
spojena	spojit	k5eAaPmNgFnS	spojit
s	s	k7c7	s
mládím	mládí	k1gNnSc7	mládí
a	a	k8xC	a
u	u	k7c2	u
něj	on	k3xPp3gInSc2	on
je	být	k5eAaImIp3nS	být
ideální	ideální	k2eAgFnSc1d1	ideální
váha	váha	k1gFnSc1	váha
(	(	kIx(	(
<g/>
zdravé	zdravý	k2eAgNnSc1d1	zdravé
BMI	BMI	kA	BMI
<g/>
)	)	kIx)	)
nižší	nízký	k2eAgFnSc1d2	nižší
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
populaci	populace	k1gFnSc4	populace
USA	USA	kA	USA
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
nejmenší	malý	k2eAgFnSc2d3	nejmenší
úmrtnosti	úmrtnost	k1gFnSc2	úmrtnost
optimální	optimální	k2eAgFnSc2d1	optimální
BMI	BMI	kA	BMI
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
18	[number]	k4	18
až	až	k9	až
27	[number]	k4	27
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
studie	studie	k1gFnSc2	studie
na	na	k7c6	na
evropské	evropský	k2eAgFnSc6d1	Evropská
populaci	populace	k1gFnSc6	populace
je	být	k5eAaImIp3nS	být
optimální	optimální	k2eAgMnSc1d1	optimální
BMI	BMI	kA	BMI
zhruba	zhruba	k6eAd1	zhruba
25	[number]	k4	25
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
nemocnosti	nemocnost	k1gFnSc2	nemocnost
je	být	k5eAaImIp3nS	být
optimální	optimální	k2eAgMnSc1d1	optimální
BMI	BMI	kA	BMI
obdobné	obdobný	k2eAgFnPc1d1	obdobná
<g/>
,	,	kIx,	,
tomu	ten	k3xDgNnSc3	ten
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
i	i	k9	i
minimální	minimální	k2eAgInPc1d1	minimální
náklady	náklad	k1gInPc1	náklad
na	na	k7c4	na
léčbu	léčba	k1gFnSc4	léčba
<g/>
.	.	kIx.	.
</s>
<s>
Odchylky	odchylka	k1gFnPc1	odchylka
od	od	k7c2	od
optimálního	optimální	k2eAgNnSc2d1	optimální
BMI	BMI	kA	BMI
u	u	k7c2	u
matek	matka	k1gFnPc2	matka
také	také	k9	také
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
riziko	riziko	k1gNnSc4	riziko
obezity	obezita	k1gFnSc2	obezita
potomků	potomek	k1gMnPc2	potomek
<g/>
.	.	kIx.	.
</s>
<s>
BMI	BMI	kA	BMI
Prime	prim	k1gInSc5	prim
se	se	k3xPyFc4	se
získá	získat	k5eAaPmIp3nS	získat
jednoduchou	jednoduchý	k2eAgFnSc7d1	jednoduchá
úpravou	úprava	k1gFnSc7	úprava
výpočtu	výpočet	k1gInSc2	výpočet
BMI	BMI	kA	BMI
<g/>
.	.	kIx.	.
</s>
<s>
BMI	BMI	kA	BMI
Prime	prim	k1gInSc5	prim
je	být	k5eAaImIp3nS	být
poměr	poměr	k1gInSc1	poměr
osobního	osobní	k2eAgNnSc2d1	osobní
BMI	BMI	kA	BMI
k	k	k7c3	k
horní	horní	k2eAgFnSc3d1	horní
hranici	hranice	k1gFnSc3	hranice
BMI	BMI	kA	BMI
normální	normální	k2eAgFnSc2d1	normální
váhy	váha	k1gFnSc2	váha
(	(	kIx(	(
<g/>
definovaná	definovaný	k2eAgFnSc1d1	definovaná
na	na	k7c4	na
25	[number]	k4	25
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
poměr	poměr	k1gInSc4	poměr
dvou	dva	k4xCgFnPc2	dva
hodnot	hodnota	k1gFnPc2	hodnota
BMI	BMI	kA	BMI
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
BMI	BMI	kA	BMI
Prime	prim	k1gInSc5	prim
bezjednotková	bezjednotková	k1gFnSc1	bezjednotková
veličina	veličina	k1gFnSc1	veličina
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
přiřazených	přiřazený	k2eAgFnPc2d1	přiřazená
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Osoby	osoba	k1gFnPc4	osoba
s	s	k7c7	s
BMI	BMI	kA	BMI
Prime	prim	k1gInSc5	prim
menším	malý	k2eAgInSc6d2	menší
než	než	k8xS	než
0,74	[number]	k4	0,74
jsou	být	k5eAaImIp3nP	být
podvyživené	podvyživený	k2eAgFnPc1d1	podvyživená
<g/>
;	;	kIx,	;
osoby	osoba	k1gFnPc1	osoba
s	s	k7c7	s
váhou	váha	k1gFnSc7	váha
mezi	mezi	k7c7	mezi
0,74	[number]	k4	0,74
a	a	k8xC	a
1,00	[number]	k4	1,00
mají	mít	k5eAaImIp3nP	mít
optimální	optimální	k2eAgFnSc4d1	optimální
váhu	váha	k1gFnSc4	váha
<g/>
;	;	kIx,	;
a	a	k8xC	a
osoby	osoba	k1gFnPc4	osoba
s	s	k7c7	s
více	hodně	k6eAd2	hodně
jak	jak	k8xC	jak
1,00	[number]	k4	1,00
mají	mít	k5eAaImIp3nP	mít
nadváhou	nadváha	k1gFnSc7	nadváha
<g/>
.	.	kIx.	.
</s>
<s>
BMI	BMI	kA	BMI
Prime	prim	k1gInSc5	prim
je	být	k5eAaImIp3nS	být
užitečné	užitečný	k2eAgNnSc1d1	užitečné
v	v	k7c6	v
medicíně	medicína	k1gFnSc6	medicína
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
říká	říkat	k5eAaImIp3nS	říkat
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
jedinec	jedinec	k1gMnSc1	jedinec
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
od	od	k7c2	od
horní	horní	k2eAgFnSc2d1	horní
váhové	váhový	k2eAgFnSc2d1	váhová
hranice	hranice	k1gFnSc2	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
osoba	osoba	k1gFnSc1	osoba
s	s	k7c7	s
BMI	BMI	kA	BMI
34	[number]	k4	34
má	mít	k5eAaImIp3nS	mít
BMI	BMI	kA	BMI
Prime	prim	k1gInSc5	prim
34	[number]	k4	34
<g/>
/	/	kIx~	/
<g/>
25	[number]	k4	25
=	=	kIx~	=
1,36	[number]	k4	1,36
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
36	[number]	k4	36
<g/>
%	%	kIx~	%
nad	nad	k7c7	nad
její	její	k3xOp3gFnSc7	její
horní	horní	k2eAgFnSc7d1	horní
váhovou	váhový	k2eAgFnSc7d1	váhová
hranicí	hranice	k1gFnSc7	hranice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Asii	Asie	k1gFnSc6	Asie
a	a	k8xC	a
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Čině	čina	k1gFnSc6	čina
je	být	k5eAaImIp3nS	být
BMI	BMI	kA	BMI
Prime	prim	k1gInSc5	prim
počítáno	počítat	k5eAaImNgNnS	počítat
s	s	k7c7	s
horní	horní	k2eAgFnSc7d1	horní
hranicí	hranice	k1gFnSc7	hranice
BMI	BMI	kA	BMI
23	[number]	k4	23
místo	místo	k7c2	místo
25	[number]	k4	25
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
BMI	BMI	kA	BMI
Prime	prim	k1gInSc5	prim
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
srovnání	srovnání	k1gNnSc4	srovnání
i	i	k9	i
mezi	mezi	k7c7	mezi
populací	populace	k1gFnSc7	populace
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
jiné	jiný	k2eAgFnPc4d1	jiná
horní	horní	k2eAgFnPc4d1	horní
hranice	hranice	k1gFnPc4	hranice
BMI	BMI	kA	BMI
<g/>
.	.	kIx.	.
</s>
<s>
BMI	BMI	kA	BMI
je	být	k5eAaImIp3nS	být
poslední	poslední	k2eAgFnSc7d1	poslední
dobou	doba	k1gFnSc7	doba
kritizován	kritizovat	k5eAaImNgMnS	kritizovat
jako	jako	k8xC	jako
zastaralý	zastaralý	k2eAgInSc1d1	zastaralý
a	a	k8xC	a
nedostatečný	dostatečný	k2eNgInSc1d1	nedostatečný
ukazatel	ukazatel	k1gInSc1	ukazatel
a	a	k8xC	a
měl	mít	k5eAaImAgInS	mít
by	by	kYmCp3nS	by
být	být	k5eAaImF	být
přehodnocen	přehodnotit	k5eAaPmNgInS	přehodnotit
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc7	svůj
kvadratickou	kvadratický	k2eAgFnSc7d1	kvadratická
závislostí	závislost	k1gFnSc7	závislost
na	na	k7c6	na
výšce	výška	k1gFnSc6	výška
ignoruje	ignorovat	k5eAaImIp3nS	ignorovat
přirozenou	přirozený	k2eAgFnSc4d1	přirozená
závislost	závislost	k1gFnSc4	závislost
na	na	k7c6	na
třetí	třetí	k4xOgFnSc6	třetí
mocnině	mocnina	k1gFnSc6	mocnina
<g/>
.	.	kIx.	.
</s>
<s>
Nerozlišuje	rozlišovat	k5eNaImIp3nS	rozlišovat
mezi	mezi	k7c4	mezi
tuky	tuk	k1gInPc4	tuk
a	a	k8xC	a
svaly	sval	k1gInPc4	sval
<g/>
.	.	kIx.	.
</s>
<s>
Kategorie	kategorie	k1gFnPc1	kategorie
(	(	kIx(	(
<g/>
intervaly	interval	k1gInPc1	interval
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
historicky	historicky	k6eAd1	historicky
mění	měnit	k5eAaImIp3nS	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
zdravotními	zdravotní	k2eAgNnPc7d1	zdravotní
riziky	riziko	k1gNnPc7	riziko
koreluje	korelovat	k5eAaImIp3nS	korelovat
lépe	dobře	k6eAd2	dobře
poměr	poměr	k1gInSc1	poměr
obvodu	obvod	k1gInSc2	obvod
pasu	pas	k1gInSc2	pas
a	a	k8xC	a
boků	bok	k1gInPc2	bok
(	(	kIx(	(
<g/>
WHR	WHR	kA	WHR
<g/>
)	)	kIx)	)
a	a	k8xC	a
ještě	ještě	k9	ještě
lépe	dobře	k6eAd2	dobře
poměr	poměr	k1gInSc1	poměr
mezi	mezi	k7c7	mezi
obvodem	obvod	k1gInSc7	obvod
pasu	pas	k1gInSc2	pas
a	a	k8xC	a
výškou	výška	k1gFnSc7	výška
(	(	kIx(	(
<g/>
WHtR	WHtR	k1gFnSc7	WHtR
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
