<s>
Index	index	k1gInSc1	index
tělesné	tělesný	k2eAgFnSc2d1	tělesná
hmotnosti	hmotnost	k1gFnSc2	hmotnost
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
letech	let	k1gInPc6	let
1830	[number]	k4	1830
<g/>
–	–	k?	–
<g/>
1850	[number]	k4	1850
belgický	belgický	k2eAgMnSc1d1	belgický
matematik	matematik	k1gMnSc1	matematik
a	a	k8xC	a
statistik	statistik	k1gMnSc1	statistik
Adolphe	Adolph	k1gFnSc2	Adolph
Quetelet	Quetelet	k1gInSc1	Quetelet
při	při	k7c6	při
práci	práce	k1gFnSc6	práce
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
systému	systém	k1gInSc6	systém
"	"	kIx"	"
<g/>
sociální	sociální	k2eAgFnSc2d1	sociální
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
BMI	BMI	kA	BMI
někdy	někdy	k6eAd1	někdy
označuje	označovat	k5eAaImIp3nS	označovat
také	také	k9	také
jako	jako	k9	jako
Queteletův	Queteletův	k2eAgInSc1d1	Queteletův
index	index	k1gInSc1	index
<g/>
.	.	kIx.	.
</s>
