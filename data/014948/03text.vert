<s>
Miloslav	Miloslav	k1gMnSc1
Denk	Denk	k1gMnSc1
</s>
<s>
Miloslav	Miloslav	k1gMnSc1
Denk	Denk	k1gMnSc1
Miloslav	Miloslav	k1gMnSc1
Denk	Denk	k1gMnSc1
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
<g/>
Osobní	osobní	k2eAgFnSc2d1
informace	informace	k1gFnSc2
Datum	datum	k1gNnSc1
narození	narození	k1gNnSc2
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1957	#num#	k4
(	(	kIx(
<g/>
63	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Místo	místo	k7c2
narození	narození	k1gNnSc2
</s>
<s>
Československo	Československo	k1gNnSc1
Klubové	klubový	k2eAgFnSc2d1
informace	informace	k1gFnSc2
</s>
<s>
Konec	konec	k1gInSc1
hráčské	hráčský	k2eAgFnSc2d1
kariéry	kariéra	k1gFnSc2
Pozice	pozice	k1gFnSc2
</s>
<s>
útočník	útočník	k1gMnSc1
Profesionální	profesionální	k2eAgFnSc2d1
kluby	klub	k1gInPc1
</s>
<s>
Roky	rok	k1gInPc1
</s>
<s>
Klub	klub	k1gInSc1
</s>
<s>
Záp	Záp	k?
<g/>
.	.	kIx.
(	(	kIx(
<g/>
góly	gól	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
1981	#num#	k4
<g/>
–	–	k?
<g/>
19821982	#num#	k4
<g/>
–	–	k?
<g/>
19871987	#num#	k4
<g/>
–	–	k?
<g/>
198819891990	#num#	k4
VTJ	VTJ	kA
Tábor	Tábor	k1gInSc1
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
Spartak	Spartak	k1gInSc4
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
Xaverov	Xaverov	k1gInSc4
Horní	horní	k2eAgFnSc1d1
Počernice	Počernice	k1gFnPc1
Sabah	Sabah	k1gMnSc1
FA12	FA12	k1gMnSc1
(	(	kIx(
<g/>
4	#num#	k4
<g/>
)	)	kIx)
<g/>
84	#num#	k4
(	(	kIx(
<g/>
18	#num#	k4
<g/>
)	)	kIx)
<g/>
25	#num#	k4
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Úspěchy	úspěch	k1gInPc1
</s>
<s>
Fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
Československa	Československo	k1gNnSc2
</s>
<s>
1983	#num#	k4
<g/>
/	/	kIx~
<g/>
1984	#num#	k4
</s>
<s>
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
</s>
<s>
1984	#num#	k4
<g/>
/	/	kIx~
<g/>
1985	#num#	k4
</s>
<s>
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
</s>
<s>
1985	#num#	k4
<g/>
/	/	kIx~
<g/>
1986	#num#	k4
</s>
<s>
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
</s>
<s>
1986	#num#	k4
<g/>
/	/	kIx~
<g/>
1987	#num#	k4
</s>
<s>
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
</s>
<s>
→	→	k?
Šipka	šipka	k1gFnSc1
znamená	znamenat	k5eAaImIp3nS
hostování	hostování	k1gNnSc4
hráče	hráč	k1gMnSc2
v	v	k7c6
daném	daný	k2eAgInSc6d1
klubu	klub	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Miloslav	Miloslav	k1gMnSc1
Denk	Denk	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
19	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1957	#num#	k4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
bývalý	bývalý	k2eAgMnSc1d1
český	český	k2eAgMnSc1d1
fotbalový	fotbalový	k2eAgMnSc1d1
útočník	útočník	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Fotbalová	fotbalový	k2eAgFnSc1d1
kariéra	kariéra	k1gFnSc1
</s>
<s>
Se	s	k7c7
Spartou	Sparta	k1gFnSc7
získal	získat	k5eAaPmAgMnS
třikrát	třikrát	k6eAd1
ligový	ligový	k2eAgInSc4d1
titul	titul	k1gInSc4
<g/>
,	,	kIx,
hrál	hrát	k5eAaImAgInS
i	i	k9
v	v	k7c6
evropských	evropský	k2eAgInPc6d1
pohárech	pohár	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
odchodu	odchod	k1gInSc6
ze	z	k7c2
Sparty	Sparta	k1gFnSc2
hrál	hrát	k5eAaImAgInS
mj.	mj.	kA
v	v	k7c6
Malajsii	Malajsie	k1gFnSc6
za	za	k7c4
Sabah	Sabah	k1gInSc4
FA	fa	k1gNnSc2
nebo	nebo	k8xC
po	po	k7c6
návratu	návrat	k1gInSc6
za	za	k7c4
Hradec	Hradec	k1gInSc4
Králové	Králová	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
československé	československý	k2eAgFnSc6d1
lize	liga	k1gFnSc6
nastoupil	nastoupit	k5eAaPmAgMnS
ve	v	k7c6
109	#num#	k4
utkáních	utkání	k1gNnPc6
a	a	k8xC
dal	dát	k5eAaPmAgMnS
19	#num#	k4
gólů	gól	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
krajské	krajský	k2eAgFnSc6d1
úrovni	úroveň	k1gFnSc6
hrál	hrát	k5eAaImAgInS
fotbal	fotbal	k1gInSc4
ještě	ještě	k9
ve	v	k7c6
svých	svůj	k3xOyFgInPc6
53	#num#	k4
letech	léto	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dresu	dres	k1gInSc6
Sparty	Sparta	k1gFnSc2
je	být	k5eAaImIp3nS
pravidelných	pravidelný	k2eAgFnPc2d1
účastníkem	účastník	k1gMnSc7
tradičních	tradiční	k2eAgNnPc6d1
silvestrovských	silvestrovský	k2eAgNnPc6d1
derby	derby	k1gNnPc6
veteránů	veterán	k1gMnPc2
Sparta	Sparta	k1gFnSc1
<g/>
–	–	k?
<g/>
Slavia	Slavia	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Ligová	ligový	k2eAgFnSc1d1
bilance	bilance	k1gFnSc1
</s>
<s>
Ročník	ročník	k1gInSc1
</s>
<s>
Zápasy	zápas	k1gInPc1
</s>
<s>
Góly	gól	k1gInPc1
</s>
<s>
Klub	klub	k1gInSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
československá	československý	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
1982	#num#	k4
<g/>
/	/	kIx~
<g/>
83	#num#	k4
</s>
<s>
15	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
československá	československý	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
1983	#num#	k4
<g/>
/	/	kIx~
<g/>
84	#num#	k4
</s>
<s>
26	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
československá	československý	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
1984	#num#	k4
<g/>
/	/	kIx~
<g/>
85	#num#	k4
</s>
<s>
23	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
československá	československý	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
1985	#num#	k4
<g/>
/	/	kIx~
<g/>
86	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
československá	československý	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
1986	#num#	k4
<g/>
/	/	kIx~
<g/>
87	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
československá	československý	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
1987	#num#	k4
<g/>
/	/	kIx~
<g/>
88	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
Spartak	Spartak	k1gInSc1
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
československá	československý	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
1988	#num#	k4
<g/>
/	/	kIx~
<g/>
89	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
Spartak	Spartak	k1gInSc1
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
</s>
<s>
CELKEM	celkem	k6eAd1
</s>
<s>
109	#num#	k4
</s>
<s>
19	#num#	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Player	Player	k1gInSc1
History	Histor	k1gInPc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
TJ	tj	kA
Sparta	Sparta	k1gFnSc1
ČKD	ČKD	kA
Praha	Praha	k1gFnSc1
–	–	k?
1983	#num#	k4
<g/>
/	/	kIx~
<g/>
1984	#num#	k4
Brankáři	brankář	k1gMnPc1
</s>
<s>
Jiří	Jiří	k1gMnSc1
Dostál	Dostál	k1gMnSc1
•	•	k?
Jaroslav	Jaroslav	k1gMnSc1
Olejár	Olejár	k1gMnSc1
•	•	k?
Jan	Jan	k1gMnSc1
Stejskal	Stejskal	k1gMnSc1
Hráči	hráč	k1gMnPc1
</s>
<s>
Jan	Jan	k1gMnSc1
Berger	Berger	k1gMnSc1
•	•	k?
Miloš	Miloš	k1gMnSc1
Beznoska	beznoska	k1gMnSc1
•	•	k?
Július	Július	k1gMnSc1
Bielik	Bielik	k1gMnSc1
•	•	k?
Michal	Michal	k1gMnSc1
Bílek	Bílek	k1gMnSc1
•	•	k?
Vlastimil	Vlastimil	k1gMnSc1
Calta	calta	k1gFnSc1
•	•	k?
Miloslav	Miloslav	k1gMnSc1
Denk	Denk	k1gMnSc1
•	•	k?
Daniel	Daniel	k1gMnSc1
Drahokoupil	Drahokoupil	k1gMnSc1
•	•	k?
Stanislav	Stanislav	k1gMnSc1
Griga	Grig	k1gMnSc2
•	•	k?
Ivan	Ivan	k1gMnSc1
Hašek	Hašek	k1gMnSc1
•	•	k?
Jozef	Jozef	k1gMnSc1
Chovanec	Chovanec	k1gMnSc1
•	•	k?
Josef	Josef	k1gMnSc1
Jarolím	Jarolí	k1gNnSc7
•	•	k?
Jiří	Jiří	k1gMnSc2
Kabyl	Kabyl	k1gInSc1
•	•	k?
Vítězslav	Vítězslav	k1gMnSc1
Lavička	Lavička	k1gMnSc1
•	•	k?
Zdeněk	Zdeněk	k1gMnSc1
Procházka	Procházka	k1gMnSc1
•	•	k?
Tomáš	Tomáš	k1gMnSc1
Skuhravý	Skuhravý	k1gMnSc1
•	•	k?
František	František	k1gMnSc1
Straka	Straka	k1gMnSc1
•	•	k?
Zdeněk	Zdeněk	k1gMnSc1
Ščasný	Ščasný	k2eAgMnSc1d1
</s>
<s>
TJ	tj	kA
Sparta	Sparta	k1gFnSc1
ČKD	ČKD	kA
Praha	Praha	k1gFnSc1
–	–	k?
1984	#num#	k4
<g/>
/	/	kIx~
<g/>
1985	#num#	k4
Brankáři	brankář	k1gMnPc1
</s>
<s>
Jan	Jan	k1gMnSc1
Stejskal	Stejskal	k1gMnSc1
•	•	k?
Jaroslav	Jaroslav	k1gMnSc1
Olejár	Olejár	k1gMnSc1
•	•	k?
André	André	k1gMnSc1
Houška	Houška	k1gMnSc1
Hráči	hráč	k1gMnPc1
</s>
<s>
Jan	Jan	k1gMnSc1
Berger	Berger	k1gMnSc1
•	•	k?
Miloš	Miloš	k1gMnSc1
Beznoska	beznoska	k1gMnSc1
•	•	k?
Július	Július	k1gMnSc1
Bielik	Bielik	k1gMnSc1
•	•	k?
Vlastimil	Vlastimil	k1gMnSc1
Calta	calta	k1gFnSc1
•	•	k?
Miloslav	Miloslav	k1gMnSc1
Denk	Denk	k1gMnSc1
•	•	k?
Daniel	Daniel	k1gMnSc1
Drahokoupil	Drahokoupil	k1gMnSc1
•	•	k?
Stanislav	Stanislav	k1gMnSc1
Griga	Grig	k1gMnSc2
•	•	k?
Ivan	Ivan	k1gMnSc1
Hašek	Hašek	k1gMnSc1
•	•	k?
Jozef	Jozef	k1gMnSc1
Chovanec	Chovanec	k1gMnSc1
•	•	k?
Josef	Josef	k1gMnSc1
Jarolím	Jarolí	k1gNnSc7
•	•	k?
Jiří	Jiří	k1gMnSc2
Kabyl	Kabyl	k1gInSc1
•	•	k?
Vítězslav	Vítězslav	k1gMnSc1
Lavička	Lavička	k1gMnSc1
•	•	k?
Stanislav	Stanislav	k1gMnSc1
Lieskovský	Lieskovský	k2eAgMnSc1d1
•	•	k?
Pavel	Pavel	k1gMnSc1
Matějka	Matějka	k1gMnSc1
•	•	k?
Petar	Petar	k1gMnSc1
Novák	Novák	k1gMnSc1
•	•	k?
Lubomír	Lubomír	k1gMnSc1
Pokluda	Pokluda	k1gMnSc1
•	•	k?
Richard	Richard	k1gMnSc1
Polák	Polák	k1gMnSc1
•	•	k?
Zdeněk	Zdeněk	k1gMnSc1
Procházka	Procházka	k1gMnSc1
•	•	k?
Vladimír	Vladimír	k1gMnSc1
Rosenberger	Rosenberger	k1gMnSc1
•	•	k?
František	František	k1gMnSc1
Straka	Straka	k1gMnSc1
•	•	k?
Zdeněk	Zdeněk	k1gMnSc1
Ščasný	Ščasný	k2eAgMnSc1d1
•	•	k?
Josef	Josef	k1gMnSc1
Valkoun	Valkoun	k1gMnSc1
</s>
<s>
TJ	tj	kA
Sparta	Sparta	k1gFnSc1
ČKD	ČKD	kA
Praha	Praha	k1gFnSc1
–	–	k?
1986	#num#	k4
<g/>
/	/	kIx~
<g/>
1987	#num#	k4
Brankáři	brankář	k1gMnPc1
</s>
<s>
Jan	Jan	k1gMnSc1
Stejskal	Stejskal	k1gMnSc1
Hráči	hráč	k1gMnPc1
</s>
<s>
Július	Július	k1gMnSc1
Bielik	Bielik	k1gMnSc1
•	•	k?
Michal	Michal	k1gMnSc1
Bílek	Bílek	k1gMnSc1
•	•	k?
Vlastimil	Vlastimil	k1gMnSc1
Calta	calta	k1gFnSc1
•	•	k?
Ivan	Ivan	k1gMnSc1
Čabala	Čabal	k1gMnSc2
•	•	k?
Miloslav	Miloslav	k1gMnSc1
Denk	Denk	k1gMnSc1
•	•	k?
Daniel	Daniel	k1gMnSc1
Drahokoupil	Drahokoupil	k1gMnSc1
•	•	k?
Ivan	Ivan	k1gMnSc1
Hašek	Hašek	k1gMnSc1
•	•	k?
Jozef	Jozef	k1gMnSc1
Chovanec	Chovanec	k1gMnSc1
•	•	k?
Josef	Josef	k1gMnSc1
Jarolím	Jarole	k1gFnPc3
•	•	k?
Karel	Karel	k1gMnSc1
Kadlec	Kadlec	k1gMnSc1
•	•	k?
Boris	Boris	k1gMnSc1
Kočí	Kočí	k1gMnSc1
•	•	k?
Vítězslav	Vítězslav	k1gMnSc1
Lavička	Lavička	k1gMnSc1
•	•	k?
Pavel	Pavel	k1gMnSc1
Mejdr	Mejdr	k1gInSc1
•	•	k?
Václav	Václav	k1gMnSc1
Němeček	Němeček	k1gMnSc1
•	•	k?
Petar	Petar	k1gMnSc1
Novák	Novák	k1gMnSc1
•	•	k?
Jiří	Jiří	k1gMnSc1
Novotný	Novotný	k1gMnSc1
•	•	k?
Jan	Jan	k1gMnSc1
Orgoník	Orgoník	k1gMnSc1
•	•	k?
Zdeněk	Zdeněk	k1gMnSc1
Procházka	Procházka	k1gMnSc1
•	•	k?
Tomáš	Tomáš	k1gMnSc1
Skuhravý	Skuhravý	k1gMnSc1
•	•	k?
František	František	k1gMnSc1
Straka	Straka	k1gMnSc1
•	•	k?
Petr	Petr	k1gMnSc1
Vrabec	Vrabec	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Fotbal	fotbal	k1gInSc1
</s>
