<s>
Jakarta	Jakarta	k1gFnSc1	Jakarta
(	(	kIx(	(
<g/>
Džakarta	Džakarta	k1gFnSc1	Džakarta
<g/>
,	,	kIx,	,
indonésky	indonésky	k6eAd1	indonésky
Jakarta	Jakarta	k1gFnSc1	Jakarta
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
Batavia	Batavia	k1gFnSc1	Batavia
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc4d1	hlavní
a	a	k8xC	a
největší	veliký	k2eAgNnSc4d3	veliký
město	město	k1gNnSc4	město
Indonésie	Indonésie	k1gFnSc2	Indonésie
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
více	hodně	k6eAd2	hodně
než	než	k8xS	než
9	[number]	k4	9
miliónů	milión	k4xCgInPc2	milión
obyvatel	obyvatel	k1gMnPc2	obyvatel
bez	bez	k7c2	bez
aglomerací	aglomerace	k1gFnPc2	aglomerace
<g/>
,	,	kIx,	,
v	v	k7c6	v
aglomeraci	aglomerace	k1gFnSc6	aglomerace
zvané	zvaný	k2eAgNnSc1d1	zvané
Jabotabek	Jabotabek	k1gInSc4	Jabotabek
více	hodně	k6eAd2	hodně
než	než	k8xS	než
26	[number]	k4	26
milionů	milion	k4xCgInPc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
