<s>
Růžová	růžový	k2eAgFnSc1d1	růžová
je	být	k5eAaImIp3nS	být
přírodní	přírodní	k2eAgFnSc1d1	přírodní
barva	barva	k1gFnSc1	barva
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
smícháním	smíchání	k1gNnSc7	smíchání
červené	červený	k2eAgFnSc2d1	červená
a	a	k8xC	a
bílé	bílý	k2eAgFnSc2d1	bílá
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
název	název	k1gInSc4	název
získala	získat	k5eAaPmAgFnS	získat
podle	podle	k7c2	podle
květin	květina	k1gFnPc2	květina
-	-	kIx~	-
růží	růž	k1gFnPc2	růž
<g/>
.	.	kIx.	.
</s>
<s>
Nenajdeme	najít	k5eNaPmIp1nP	najít
ji	on	k3xPp3gFnSc4	on
ve	v	k7c6	v
spektru	spektrum	k1gNnSc6	spektrum
světelného	světelný	k2eAgInSc2d1	světelný
paprsku	paprsek	k1gInSc2	paprsek
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nemá	mít	k5eNaImIp3nS	mít
připsanou	připsaný	k2eAgFnSc4d1	připsaná
vlnovou	vlnový	k2eAgFnSc4d1	vlnová
délku	délka	k1gFnSc4	délka
Významy	význam	k1gInPc4	význam
Růžová	růžový	k2eAgFnSc1d1	růžová
barva	barva	k1gFnSc1	barva
je	být	k5eAaImIp3nS	být
symbolem	symbol	k1gInSc7	symbol
dívek	dívka	k1gFnPc2	dívka
<g/>
,	,	kIx,	,
ženskosti	ženskost	k1gFnSc2	ženskost
<g/>
,	,	kIx,	,
něžnosti	něžnost	k1gFnSc2	něžnost
a	a	k8xC	a
romantiky	romantika	k1gFnSc2	romantika
<g/>
.	.	kIx.	.
</s>
<s>
Muž	muž	k1gMnSc1	muž
s	s	k7c7	s
náklonností	náklonnost	k1gFnSc7	náklonnost
k	k	k7c3	k
růžové	růžový	k2eAgFnSc3d1	růžová
barvě	barva	k1gFnSc3	barva
(	(	kIx(	(
<g/>
např.	např.	kA	např.
nosící	nosící	k2eAgFnSc1d1	nosící
růžové	růžový	k2eAgNnSc4d1	růžové
oblečení	oblečení	k1gNnSc4	oblečení
<g/>
)	)	kIx)	)
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
okolím	okolí	k1gNnSc7	okolí
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
zženštilého	zženštilý	k2eAgMnSc4d1	zženštilý
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byli	být	k5eAaImAgMnP	být
homosexuální	homosexuální	k2eAgMnPc1d1	homosexuální
vězni	vězeň	k1gMnPc1	vězeň
v	v	k7c6	v
koncentračních	koncentrační	k2eAgInPc6d1	koncentrační
táborech	tábor	k1gInPc6	tábor
označováni	označovat	k5eAaImNgMnP	označovat
právě	právě	k9	právě
růžovými	růžový	k2eAgInPc7d1	růžový
trojúhelníky	trojúhelník	k1gInPc7	trojúhelník
-	-	kIx~	-
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
tehdejší	tehdejší	k2eAgNnSc1d1	tehdejší
označení	označení	k1gNnSc1	označení
židů	žid	k1gMnPc2	žid
žlutou	žlutý	k2eAgFnSc7d1	žlutá
hvězdou	hvězda	k1gFnSc7	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
je	být	k5eAaImIp3nS	být
růžová	růžový	k2eAgFnSc1d1	růžová
barva	barva	k1gFnSc1	barva
příznačně	příznačně	k6eAd1	příznačně
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
znak	znak	k1gInSc4	znak
zženštělosti	zženštělost	k1gFnSc2	zženštělost
<g/>
.	.	kIx.	.
</s>
<s>
Růžové	růžový	k2eAgFnPc4d1	růžová
růže	růž	k1gFnPc4	růž
a	a	k8xC	a
srdce	srdce	k1gNnSc4	srdce
jsou	být	k5eAaImIp3nP	být
symboly	symbol	k1gInPc4	symbol
romantické	romantický	k2eAgFnSc2d1	romantická
lásky	láska	k1gFnSc2	láska
<g/>
.	.	kIx.	.
</s>
<s>
Jemná	jemný	k2eAgFnSc1d1	jemná
pokožka	pokožka	k1gFnSc1	pokožka
a	a	k8xC	a
pleť	pleť	k1gFnSc1	pleť
novorozeňat	novorozeně	k1gNnPc2	novorozeně
je	být	k5eAaImIp3nS	být
růžová	růžový	k2eAgFnSc1d1	růžová
<g/>
.	.	kIx.	.
</s>
<s>
Růžová	růžový	k2eAgFnSc1d1	růžová
je	být	k5eAaImIp3nS	být
také	také	k9	také
barva	barva	k1gFnSc1	barva
mírnosti	mírnost	k1gFnSc2	mírnost
<g/>
,	,	kIx,	,
něhy	něha	k1gFnSc2	něha
a	a	k8xC	a
vyšších	vysoký	k2eAgInPc2d2	vyšší
citů	cit	k1gInPc2	cit
v	v	k7c6	v
lásce	láska	k1gFnSc6	láska
<g/>
.	.	kIx.	.
</s>
<s>
Růžová	růžový	k2eAgFnSc1d1	růžová
může	moct	k5eAaImIp3nS	moct
též	též	k9	též
symbolizovat	symbolizovat	k5eAaImF	symbolizovat
rozpouštění	rozpouštění	k1gNnSc3	rozpouštění
protikladů	protiklad	k1gInPc2	protiklad
bílé	bílý	k2eAgFnSc2d1	bílá
a	a	k8xC	a
červené	červená	k1gFnSc2	červená
v	v	k7c6	v
symbolice	symbolika	k1gFnSc6	symbolika
barev	barva	k1gFnPc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
růžová	růžový	k2eAgFnSc1d1	růžová
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
růžový	růžový	k2eAgMnSc1d1	růžový
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
