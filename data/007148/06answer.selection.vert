<s>
Růžová	růžový	k2eAgFnSc1d1	růžová
je	být	k5eAaImIp3nS	být
přírodní	přírodní	k2eAgFnSc1d1	přírodní
barva	barva	k1gFnSc1	barva
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
smícháním	smíchání	k1gNnSc7	smíchání
červené	červený	k2eAgFnSc2d1	červená
a	a	k8xC	a
bílé	bílý	k2eAgFnSc2d1	bílá
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
