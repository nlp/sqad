<s>
Který	který	k3yRgMnSc1	který
americký	americký	k2eAgMnSc1d1	americký
atlet	atlet	k1gMnSc1	atlet
získal	získat	k5eAaPmAgMnS	získat
na	na	k7c6	na
letních	letní	k2eAgFnPc6d1	letní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
v	v	k7c6	v
Pekingu	Peking	k1gInSc6	Peking
stříbrnou	stříbrný	k2eAgFnSc4d1	stříbrná
medaili	medaile	k1gFnSc4	medaile
v	v	k7c6	v
běhu	běh	k1gInSc6	běh
na	na	k7c4	na
110	[number]	k4	110
metrů	metr	k1gInPc2	metr
překážek	překážka	k1gFnPc2	překážka
<g/>
?	?	kIx.	?
</s>
