<p>
<s>
David	David	k1gMnSc1	David
Payne	Payn	k1gInSc5	Payn
(	(	kIx(	(
<g/>
*	*	kIx~	*
24	[number]	k4	24
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1982	[number]	k4	1982
<g/>
,	,	kIx,	,
Cincinnati	Cincinnati	k1gFnSc1	Cincinnati
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
atlet	atlet	k1gMnSc1	atlet
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
získal	získat	k5eAaPmAgMnS	získat
na	na	k7c6	na
letních	letní	k2eAgFnPc6d1	letní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
v	v	k7c6	v
Pekingu	Peking	k1gInSc6	Peking
stříbrnou	stříbrný	k2eAgFnSc4d1	stříbrná
medaili	medaile	k1gFnSc4	medaile
v	v	k7c6	v
běhu	běh	k1gInSc6	běh
na	na	k7c4	na
110	[number]	k4	110
metrů	metr	k1gInPc2	metr
překážek	překážka	k1gFnPc2	překážka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
získal	získat	k5eAaPmAgMnS	získat
stříbrnou	stříbrný	k2eAgFnSc4d1	stříbrná
medaili	medaile	k1gFnSc4	medaile
na	na	k7c6	na
Panamerických	panamerický	k2eAgFnPc6d1	Panamerická
hrách	hra	k1gFnPc6	hra
v	v	k7c6	v
brazilském	brazilský	k2eAgMnSc6d1	brazilský
Rio	Rio	k1gMnSc6	Rio
de	de	k?	de
Janeiru	Janeira	k1gFnSc4	Janeira
a	a	k8xC	a
bronz	bronz	k1gInSc4	bronz
na	na	k7c6	na
světovém	světový	k2eAgInSc6d1	světový
šampionátu	šampionát	k1gInSc6	šampionát
v	v	k7c6	v
Ósace	Ósaka	k1gFnSc6	Ósaka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témž	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
doběhl	doběhnout	k5eAaPmAgMnS	doběhnout
druhý	druhý	k4xOgMnSc1	druhý
na	na	k7c6	na
světovém	světový	k2eAgNnSc6d1	světové
atletickém	atletický	k2eAgNnSc6d1	atletické
finále	finále	k1gNnSc6	finále
ve	v	k7c6	v
Stuttgartu	Stuttgart	k1gInSc6	Stuttgart
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
olympiádě	olympiáda	k1gFnSc6	olympiáda
v	v	k7c6	v
Pekingu	Peking	k1gInSc6	Peking
2008	[number]	k4	2008
nestačil	stačit	k5eNaBmAgInS	stačit
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
kubánského	kubánský	k2eAgMnSc4d1	kubánský
překážkáře	překážkář	k1gMnSc4	překážkář
Dayrona	Dayron	k1gMnSc4	Dayron
Roblese	Roblesa	k1gFnSc6	Roblesa
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
vybojoval	vybojovat	k5eAaPmAgMnS	vybojovat
druhou	druhý	k4xOgFnSc4	druhý
bronzovou	bronzový	k2eAgFnSc4d1	bronzová
medaili	medaile	k1gFnSc4	medaile
na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
,	,	kIx,	,
když	když	k8xS	když
cílem	cíl	k1gInSc7	cíl
proběhl	proběhnout	k5eAaPmAgMnS	proběhnout
v	v	k7c6	v
čase	čas	k1gInSc6	čas
13,15	[number]	k4	13,15
s.	s.	k?	s.
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
čase	čas	k1gInSc6	čas
doběhl	doběhnout	k5eAaPmAgMnS	doběhnout
také	také	k9	také
jeho	jeho	k3xOp3gMnSc1	jeho
reprezentační	reprezentační	k2eAgMnSc1d1	reprezentační
kolega	kolega	k1gMnSc1	kolega
Terrence	Terrence	k1gFnSc2	Terrence
Trammell	Trammell	k1gMnSc1	Trammell
<g/>
,	,	kIx,	,
kterému	který	k3yIgMnSc3	který
cílová	cílový	k2eAgFnSc1d1	cílová
kamera	kamera	k1gFnSc1	kamera
přidělila	přidělit	k5eAaPmAgFnS	přidělit
stříbro	stříbro	k1gNnSc4	stříbro
a	a	k8xC	a
zlato	zlato	k1gNnSc4	zlato
získal	získat	k5eAaPmAgMnS	získat
v	v	k7c6	v
čase	čas	k1gInSc6	čas
13,14	[number]	k4	13,14
s	s	k7c7	s
barbadoský	barbadoský	k1gMnSc1	barbadoský
atlet	atlet	k1gMnSc1	atlet
Ryan	Ryan	k1gMnSc1	Ryan
Brathwaite	Brathwait	k1gInSc5	Brathwait
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Osobní	osobní	k2eAgInPc4d1	osobní
rekordy	rekord	k1gInPc4	rekord
==	==	k?	==
</s>
</p>
<p>
<s>
60	[number]	k4	60
m	m	kA	m
př	př	kA	př
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
hala	hala	k1gFnSc1	hala
<g/>
)	)	kIx)	)
-	-	kIx~	-
(	(	kIx(	(
<g/>
7,54	[number]	k4	7,54
-	-	kIx~	-
26	[number]	k4	26
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
Boston	Boston	k1gInSc1	Boston
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
110	[number]	k4	110
m	m	kA	m
př	př	kA	př
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
dráha	dráha	k1gFnSc1	dráha
<g/>
)	)	kIx)	)
-	-	kIx~	-
(	(	kIx(	(
<g/>
13,02	[number]	k4	13,02
-	-	kIx~	-
31	[number]	k4	31
<g/>
.	.	kIx.	.
srpen	srpen	k1gInSc1	srpen
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
Ósaka	Ósaka	k1gFnSc1	Ósaka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
David	David	k1gMnSc1	David
Payne	Payn	k1gInSc5	Payn
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
David	David	k1gMnSc1	David
Payne	Payn	k1gInSc5	Payn
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
IAAF	IAAF	kA	IAAF
</s>
</p>
<p>
<s>
Profil	profil	k1gInSc1	profil
na	na	k7c4	na
www.usatf.org	www.usatf.org	k1gInSc4	www.usatf.org
</s>
</p>
