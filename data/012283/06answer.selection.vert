<s>
Český	český	k2eAgInSc1d1	český
jazyk	jazyk	k1gInSc1	jazyk
neboli	neboli	k8xC	neboli
čeština	čeština	k1gFnSc1	čeština
je	být	k5eAaImIp3nS	být
západoslovanský	západoslovanský	k2eAgInSc4d1	západoslovanský
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
nejbližší	blízký	k2eAgFnSc6d3	nejbližší
slovenštině	slovenština	k1gFnSc6	slovenština
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
polštině	polština	k1gFnSc3	polština
a	a	k8xC	a
lužické	lužický	k2eAgFnSc3d1	Lužická
srbštině	srbština	k1gFnSc3	srbština
<g/>
.	.	kIx.	.
</s>
