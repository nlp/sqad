<s>
Charlotte	Charlotte	k5eAaPmIp2nP	Charlotte
je	on	k3xPp3gNnSc4	on
největší	veliký	k2eAgNnSc4d3	veliký
město	město	k1gNnSc4	město
amerického	americký	k2eAgInSc2d1	americký
státu	stát	k1gInSc2	stát
Severní	severní	k2eAgFnSc1d1	severní
Karolína	Karolína	k1gFnSc1	Karolína
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
největší	veliký	k2eAgInSc1d3	veliký
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
odhadu	odhad	k1gInSc2	odhad
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
má	mít	k5eAaImIp3nS	mít
město	město	k1gNnSc4	město
610	[number]	k4	610
949	[number]	k4	949
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
celá	celý	k2eAgFnSc1d1	celá
metropolitní	metropolitní	k2eAgFnSc1d1	metropolitní
oblast	oblast	k1gFnSc1	oblast
2	[number]	k4	2
420	[number]	k4	420
745	[number]	k4	745
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Metropolitní	metropolitní	k2eAgFnSc1d1	metropolitní
oblast	oblast	k1gFnSc1	oblast
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejrychleji	rychle	k6eAd3	rychle
rostoucích	rostoucí	k2eAgMnPc2d1	rostoucí
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Městu	město	k1gNnSc3	město
se	se	k3xPyFc4	se
přezdívá	přezdívat	k5eAaImIp3nS	přezdívat
The	The	k1gFnSc1	The
Queen	Queno	k1gNnPc2	Queno
City	City	k1gFnSc2	City
(	(	kIx(	(
<g/>
Královnino	královnin	k2eAgNnSc1d1	královnino
město	město	k1gNnSc1	město
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Charlotte	Charlotit	k5eAaPmRp2nP	Charlotit
je	být	k5eAaImIp3nS	být
pojmenováno	pojmenovat	k5eAaPmNgNnS	pojmenovat
po	po	k7c6	po
královně	královna	k1gFnSc6	královna
Charlottě	Charlotta	k1gFnSc6	Charlotta
<g/>
,	,	kIx,	,
manželce	manželka	k1gFnSc3	manželka
Jiřího	Jiří	k1gMnSc2	Jiří
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
krále	král	k1gMnSc4	král
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Charlotte	Charlotte	k5eAaPmIp2nP	Charlotte
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
20	[number]	k4	20
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1768	[number]	k4	1768
na	na	k7c6	na
křižovatce	křižovatka	k1gFnSc6	křižovatka
dvou	dva	k4xCgFnPc2	dva
indiánských	indiánský	k2eAgFnPc2d1	indiánská
obchodních	obchodní	k2eAgFnPc2d1	obchodní
cest	cesta	k1gFnPc2	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
<g/>
,	,	kIx,	,
Great	Great	k2eAgMnSc1d1	Great
Wagon	Wagon	k1gMnSc1	Wagon
Road	Road	k1gMnSc1	Road
(	(	kIx(	(
<g/>
Cesta	cesta	k1gFnSc1	cesta
velkého	velký	k2eAgInSc2d1	velký
vozu	vůz	k1gInSc2	vůz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vedla	vést	k5eAaImAgFnS	vést
od	od	k7c2	od
severu	sever	k1gInSc2	sever
k	k	k7c3	k
jihu	jih	k1gInSc3	jih
<g/>
,	,	kIx,	,
její	její	k3xOp3gMnSc1	její
trasu	trasa	k1gFnSc4	trasa
dnes	dnes	k6eAd1	dnes
přibližně	přibližně	k6eAd1	přibližně
kopíruje	kopírovat	k5eAaImIp3nS	kopírovat
U.S	U.S	k1gFnSc1	U.S
Route	Rout	k1gInSc5	Rout
21	[number]	k4	21
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
spojovala	spojovat	k5eAaImAgFnS	spojovat
východ	východ	k1gInSc4	východ
a	a	k8xC	a
západ	západ	k1gInSc4	západ
podél	podél	k7c2	podél
dnešní	dnešní	k2eAgFnSc2d1	dnešní
moderní	moderní	k2eAgFnSc2d1	moderní
Trade	Trad	k1gInSc5	Trad
Street	Streeta	k1gFnPc2	Streeta
(	(	kIx(	(
<g/>
Obchodní	obchodní	k2eAgFnPc1d1	obchodní
ulice	ulice	k1gFnPc1	ulice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
přišli	přijít	k5eAaPmAgMnP	přijít
po	po	k7c4	po
Great	Great	k2eAgInSc4d1	Great
Wagon	Wagon	k1gInSc4	Wagon
Road	Roada	k1gFnPc2	Roada
na	na	k7c6	na
úpatí	úpatí	k1gNnSc6	úpatí
Carolina	Carolin	k2eAgInSc2d1	Carolin
osadníci	osadník	k1gMnPc1	osadník
z	z	k7c2	z
Pensylvánie	Pensylvánie	k1gFnSc2	Pensylvánie
německého	německý	k2eAgMnSc2d1	německý
a	a	k8xC	a
Ulstersko-Irského	ulsterskorský	k2eAgInSc2d1	ulstersko-irský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Charlotte	Charlotte	k5eAaPmIp2nP	Charlotte
se	se	k3xPyFc4	se
přezdívá	přezdívat	k5eAaImIp3nS	přezdívat
The	The	k1gFnSc1	The
City	City	k1gFnSc2	City
of	of	k?	of
Trees	Trees	k1gInSc1	Trees
(	(	kIx(	(
<g/>
Město	město	k1gNnSc1	město
stromů	strom	k1gInPc2	strom
<g/>
)	)	kIx)	)
a	a	k8xC	a
The	The	k1gMnSc1	The
City	City	k1gFnSc2	City
of	of	k?	of
Churches	Churches	k1gInSc1	Churches
(	(	kIx(	(
<g/>
Město	město	k1gNnSc1	město
kostelů	kostel	k1gInPc2	kostel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1755	[number]	k4	1755
si	se	k3xPyFc3	se
Thomas	Thomas	k1gMnSc1	Thomas
Polk	Polk	k1gMnSc1	Polk
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgMnPc2	první
osadníků	osadník	k1gMnPc2	osadník
a	a	k8xC	a
strýc	strýc	k1gMnSc1	strýc
prezidenta	prezident	k1gMnSc2	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
Jamese	Jamese	k1gFnSc2	Jamese
K.	K.	kA	K.
Polka	Polka	k1gFnSc1	Polka
<g/>
,	,	kIx,	,
postavil	postavit	k5eAaPmAgMnS	postavit
dům	dům	k1gInSc4	dům
na	na	k7c6	na
křižovatce	křižovatka	k1gFnSc6	křižovatka
obchodní	obchodní	k2eAgFnSc2d1	obchodní
indiánské	indiánský	k2eAgFnSc2d1	indiánská
cesty	cesta	k1gFnSc2	cesta
a	a	k8xC	a
Great	Great	k2eAgMnSc1d1	Great
Wagon	Wagon	k1gMnSc1	Wagon
Road	Road	k1gMnSc1	Road
<g/>
,	,	kIx,	,
na	na	k7c6	na
tomto	tento	k3xDgNnSc6	tento
místě	místo	k1gNnSc6	místo
následně	následně	k6eAd1	následně
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
komunita	komunita	k1gFnSc1	komunita
"	"	kIx"	"
<g/>
Charlotte	Charlott	k1gInSc5	Charlott
Town	Towno	k1gNnPc2	Towno
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yQgFnSc2	který
se	se	k3xPyFc4	se
oficiálně	oficiálně	k6eAd1	oficiálně
stalo	stát	k5eAaPmAgNnS	stát
město	město	k1gNnSc1	město
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1768	[number]	k4	1768
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
křižovatka	křižovatka	k1gFnSc1	křižovatka
<g/>
,	,	kIx,	,
situovaná	situovaný	k2eAgFnSc1d1	situovaná
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
dlouhého	dlouhý	k2eAgNnSc2d1	dlouhé
stoupání	stoupání	k1gNnSc2	stoupání
úpatí	úpatí	k1gNnSc2	úpatí
hor	hora	k1gFnPc2	hora
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
tvoří	tvořit	k5eAaImIp3nP	tvořit
střed	střed	k1gInSc4	střed
Uptown	Uptown	k1gInSc1	Uptown
Charlotte	Charlott	k1gMnSc5	Charlott
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
obchodní	obchodní	k2eAgFnSc2d1	obchodní
cesty	cesta	k1gFnSc2	cesta
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
Trade	Trad	k1gInSc5	Trad
Street	Streeta	k1gFnPc2	Streeta
(	(	kIx(	(
<g/>
Obchodní	obchodní	k2eAgFnPc1d1	obchodní
ulice	ulice	k1gFnPc1	ulice
<g/>
)	)	kIx)	)
a	a	k8xC	a
Great	Great	k2eAgMnSc1d1	Great
Wagon	Wagon	k1gMnSc1	Wagon
Road	Road	k1gMnSc1	Road
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
Tryon	Tryon	k1gMnSc1	Tryon
Street	Street	k1gMnSc1	Street
(	(	kIx(	(
<g/>
Tryonova	Tryonův	k2eAgFnSc1d1	Tryonův
ulice	ulice	k1gFnSc1	ulice
<g/>
)	)	kIx)	)
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
Williama	William	k1gMnSc4	William
Tryoana	Tryoan	k1gMnSc4	Tryoan
<g/>
,	,	kIx,	,
královského	královský	k2eAgMnSc4d1	královský
guvernéra	guvernér	k1gMnSc4	guvernér
koloniální	koloniální	k2eAgFnSc2d1	koloniální
Severní	severní	k2eAgFnSc2d1	severní
Karolíny	Karolína	k1gFnSc2	Karolína
<g/>
.	.	kIx.	.
</s>
<s>
Křižovatka	křižovatka	k1gFnSc1	křižovatka
těchto	tento	k3xDgFnPc2	tento
dvou	dva	k4xCgFnPc2	dva
ulic	ulice	k1gFnPc2	ulice
je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Trade	Trad	k1gInSc5	Trad
&	&	k?	&
Tryon	Tryona	k1gFnPc2	Tryona
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
jednoduše	jednoduše	k6eAd1	jednoduše
říká	říkat	k5eAaImIp3nS	říkat
The	The	k1gFnSc1	The
Square	square	k1gInSc1	square
(	(	kIx(	(
<g/>
Náměstí	náměstí	k1gNnSc1	náměstí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k8xS	jak
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
okres	okres	k1gInSc1	okres
jsou	být	k5eAaImIp3nP	být
pojmenovány	pojmenovat	k5eAaPmNgInP	pojmenovat
po	po	k7c6	po
německé	německý	k2eAgFnSc6d1	německá
princezně	princezna	k1gFnSc6	princezna
Charlotte	Charlott	k1gInSc5	Charlott
Mecklenburg-Strelitz	Mecklenburg-Strelitz	k1gInSc4	Mecklenburg-Strelitz
<g/>
,	,	kIx,	,
manželce	manželka	k1gFnSc3	manželka
britského	britský	k2eAgMnSc2d1	britský
krále	král	k1gMnSc2	král
Jiřího	Jiří	k1gMnSc2	Jiří
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Oddanost	oddanost	k1gFnSc1	oddanost
králi	král	k1gMnSc3	král
Jiřímu	Jiří	k1gMnSc3	Jiří
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
choti	choť	k1gFnSc2	choť
ale	ale	k8xC	ale
neměla	mít	k5eNaImAgFnS	mít
dlouhého	dlouhý	k2eAgNnSc2d1	dlouhé
trvání	trvání	k1gNnSc2	trvání
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1775	[number]	k4	1775
měšťané	měšťan	k1gMnPc1	měšťan
údajně	údajně	k6eAd1	údajně
podepsali	podepsat	k5eAaPmAgMnP	podepsat
prohlášení	prohlášení	k1gNnSc4	prohlášení
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stalo	stát	k5eAaPmAgNnS	stát
známé	známý	k2eAgNnSc1d1	známé
jako	jako	k9	jako
Mecklenburg	Mecklenburg	k1gInSc4	Mecklenburg
Declaration	Declaration	k1gInSc1	Declaration
of	of	k?	of
Independence	Independence	k1gFnSc1	Independence
(	(	kIx(	(
<g/>
Meklenburská	meklenburský	k2eAgFnSc1d1	Meklenburská
deklarace	deklarace	k1gFnSc1	deklarace
nezávislosti	nezávislost	k1gFnSc2	nezávislost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
kopie	kopie	k1gFnSc1	kopie
byla	být	k5eAaImAgFnS	být
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
údajně	údajně	k6eAd1	údajně
zaslána	zaslat	k5eAaPmNgFnS	zaslat
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
nikdy	nikdy	k6eAd1	nikdy
nebyla	být	k5eNaImAgFnS	být
oficiálně	oficiálně	k6eAd1	oficiálně
doložena	doložit	k5eAaPmNgFnS	doložit
<g/>
,	,	kIx,	,
kontinentálnímu	kontinentální	k2eAgInSc3d1	kontinentální
kongresu	kongres	k1gInSc3	kongres
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
datum	datum	k1gNnSc1	datum
je	být	k5eAaImIp3nS	být
vyobrazeno	vyobrazit	k5eAaPmNgNnS	vyobrazit
na	na	k7c6	na
státní	státní	k2eAgFnSc6d1	státní
vlajce	vlajka	k1gFnSc6	vlajka
Severní	severní	k2eAgFnSc2d1	severní
Karolíny	Karolína	k1gFnSc2	Karolína
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
jedenáct	jedenáct	k4xCc4	jedenáct
dní	den	k1gInPc2	den
se	se	k3xPyFc4	se
titíž	týž	k3xTgMnPc1	týž
měšťané	měšťan	k1gMnPc1	měšťan
sešli	sejít	k5eAaPmAgMnP	sejít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
a	a	k8xC	a
schválili	schválit	k5eAaPmAgMnP	schválit
Mecklenburg	Mecklenburg	k1gInSc4	Mecklenburg
Resolves	Resolves	k1gInSc1	Resolves
(	(	kIx(	(
<g/>
Meklenburská	meklenburský	k2eAgNnPc1d1	meklenburský
usnesení	usnesení	k1gNnPc1	usnesení
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
seznam	seznam	k1gInSc1	seznam
zákonů	zákon	k1gInPc2	zákon
pro	pro	k7c4	pro
nově	nově	k6eAd1	nově
nezávislé	závislý	k2eNgNnSc4d1	nezávislé
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
války	válka	k1gFnSc2	válka
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
měly	mít	k5eAaImAgFnP	mít
v	v	k7c4	v
Charlotte	Charlott	k1gInSc5	Charlott
svá	svůj	k3xOyFgNnPc4	svůj
tábořiště	tábořiště	k1gNnSc4	tábořiště
americká	americký	k2eAgFnSc1d1	americká
i	i	k8xC	i
britská	britský	k2eAgFnSc1d1	britská
armáda	armáda	k1gFnSc1	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
bojů	boj	k1gInPc2	boj
Britských	britský	k2eAgNnPc2d1	Britské
vojsk	vojsko	k1gNnPc2	vojsko
s	s	k7c7	s
obyvateli	obyvatel	k1gMnPc7	obyvatel
Charlotte	Charlott	k1gInSc5	Charlott
vesnice	vesnice	k1gFnSc2	vesnice
získala	získat	k5eAaPmAgFnS	získat
od	od	k7c2	od
frustrovaného	frustrovaný	k2eAgMnSc2d1	frustrovaný
lorda	lord	k1gMnSc2	lord
generála	generál	k1gMnSc2	generál
Charlese	Charles	k1gMnSc2	Charles
Cornwallise	Cornwallise	k1gFnSc2	Cornwallise
trvalou	trvalý	k2eAgFnSc4d1	trvalá
přezdívku	přezdívka	k1gFnSc4	přezdívka
Hornet	Horneta	k1gFnPc2	Horneta
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Nest	Nesta	k1gFnPc2	Nesta
(	(	kIx(	(
<g/>
Sršní	sršní	k2eAgNnSc1d1	sršní
hnízdo	hnízdo	k1gNnSc1	hnízdo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pojmenování	pojmenování	k1gNnSc1	pojmenování
různých	různý	k2eAgNnPc2d1	různé
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
Independence	Independence	k1gFnSc1	Independence
Boulevard	Boulevarda	k1gFnPc2	Boulevarda
(	(	kIx(	(
<g/>
bulvár	bulvár	k1gInSc1	bulvár
Nezávislosti	nezávislost	k1gFnSc2	nezávislost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Independence	Independence	k1gFnSc1	Independence
High	High	k1gMnSc1	High
School	School	k1gInSc1	School
(	(	kIx(	(
<g/>
střední	střední	k2eAgFnSc1d1	střední
škola	škola	k1gFnSc1	škola
Nezávislosti	nezávislost	k1gFnSc2	nezávislost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Independence	Independence	k1gFnPc4	Independence
Center	centrum	k1gNnPc2	centrum
(	(	kIx(	(
<g/>
Centrum	centrum	k1gNnSc1	centrum
nezávislosti	nezávislost	k1gFnSc2	nezávislost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Freedom	Freedom	k1gInSc1	Freedom
Park	park	k1gInSc1	park
(	(	kIx(	(
<g/>
park	park	k1gInSc1	park
Svobody	svoboda	k1gFnSc2	svoboda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Freedom	Freedom	k1gInSc1	Freedom
Drive	drive	k1gInSc1	drive
(	(	kIx(	(
<g/>
cesta	cesta	k1gFnSc1	cesta
Svobody	svoboda	k1gFnSc2	svoboda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jméno	jméno	k1gNnSc1	jméno
bývalého	bývalý	k2eAgInSc2d1	bývalý
týmu	tým	k1gInSc2	tým
NBA	NBA	kA	NBA
Charlotte	Charlott	k1gInSc5	Charlott
Hornets	Hornetsa	k1gFnPc2	Hornetsa
jsou	být	k5eAaImIp3nP	být
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Charlotte	Charlott	k1gInSc5	Charlott
byla	být	k5eAaImAgFnS	být
ideologickým	ideologický	k2eAgNnSc7d1	ideologické
ohniskem	ohnisko	k1gNnSc7	ohnisko
revolucionářského	revolucionářský	k2eAgNnSc2d1	revolucionářské
cítění	cítění	k1gNnSc2	cítění
-	-	kIx~	-
během	během	k7c2	během
války	válka	k1gFnSc2	válka
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
a	a	k8xC	a
ještě	ještě	k6eAd1	ještě
nějakou	nějaký	k3yIgFnSc4	nějaký
dobu	doba	k1gFnSc4	doba
poté	poté	k6eAd1	poté
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1799	[number]	k4	1799
přinesl	přinést	k5eAaPmAgInS	přinést
dvanáctiletý	dvanáctiletý	k2eAgInSc1d1	dvanáctiletý
Conrad	Conrad	k1gInSc1	Conrad
Reed	Reeda	k1gFnPc2	Reeda
domů	domů	k6eAd1	domů
kámen	kámen	k1gInSc4	kámen
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vážil	vážit	k5eAaImAgInS	vážit
téměř	téměř	k6eAd1	téměř
osm	osm	k4xCc4	osm
kilogramů	kilogram	k1gInPc2	kilogram
(	(	kIx(	(
<g/>
17	[number]	k4	17
liber	libra	k1gFnPc2	libra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc4	ten
pak	pak	k6eAd1	pak
rodina	rodina	k1gFnSc1	rodina
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
používala	používat	k5eAaImAgFnS	používat
jako	jako	k8xS	jako
mohutný	mohutný	k2eAgInSc1d1	mohutný
práh	práh	k1gInSc1	práh
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
jeden	jeden	k4xCgMnSc1	jeden
klenotník	klenotník	k1gMnSc1	klenotník
rozpoznal	rozpoznat	k5eAaPmAgMnS	rozpoznat
téměř	téměř	k6eAd1	téměř
čisté	čistý	k2eAgNnSc4d1	čisté
zlato	zlato	k1gNnSc4	zlato
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
pak	pak	k6eAd1	pak
koupil	koupit	k5eAaPmAgMnS	koupit
za	za	k7c4	za
směšnou	směšný	k2eAgFnSc4d1	směšná
částku	částka	k1gFnSc4	částka
-	-	kIx~	-
$	$	kIx~	$
<g/>
3.50	[number]	k4	3.50
<g/>
.	.	kIx.	.
</s>
<s>
Objev	objev	k1gInSc1	objev
mladého	mladý	k2eAgMnSc2d1	mladý
Reeda	Reed	k1gMnSc2	Reed
(	(	kIx(	(
<g/>
první	první	k4xOgInSc4	první
ověřený	ověřený	k2eAgInSc4d1	ověřený
nález	nález	k1gInSc4	nález
zlata	zlato	k1gNnSc2	zlato
v	v	k7c6	v
mladém	mladé	k1gNnSc6	mladé
USA	USA	kA	USA
<g/>
)	)	kIx)	)
dal	dát	k5eAaPmAgMnS	dát
podnět	podnět	k1gInSc4	podnět
ke	k	k7c3	k
zrodu	zrod	k1gInSc2	zrod
první	první	k4xOgFnSc2	první
zlaté	zlatý	k2eAgFnSc2d1	zlatá
horečky	horečka	k1gFnSc2	horečka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
bylo	být	k5eAaImAgNnS	být
během	běh	k1gInSc7	běh
19	[number]	k4	19
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
až	až	k9	až
do	do	k7c2	do
počátku	počátek	k1gInSc2	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
objeveno	objeven	k2eAgNnSc1d1	objeveno
mnoho	mnoho	k6eAd1	mnoho
zlatých	zlatý	k2eAgFnPc2d1	zlatá
žil	žíla	k1gFnPc2	žíla
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1837	[number]	k4	1837
založena	založit	k5eAaPmNgFnS	založit
Charlotte	Charlott	k1gInSc5	Charlott
Mint	Mintum	k1gNnPc2	Mintum
(	(	kIx(	(
<g/>
Charlottská	Charlottský	k2eAgFnSc1d1	Charlottský
mincovna	mincovna	k1gFnSc1	mincovna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgFnSc1d1	severní
Karolína	Karolína	k1gFnSc1	Karolína
vedla	vést	k5eAaImAgFnS	vést
v	v	k7c6	v
národní	národní	k2eAgFnSc6d1	národní
produkci	produkce	k1gFnSc6	produkce
zlata	zlato	k1gNnSc2	zlato
až	až	k9	až
do	do	k7c2	do
kalifornské	kalifornský	k2eAgFnSc2d1	kalifornská
zlaté	zlatý	k2eAgFnSc2d1	zlatá
horečky	horečka	k1gFnSc2	horečka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1848	[number]	k4	1848
<g/>
;	;	kIx,	;
celkový	celkový	k2eAgInSc1d1	celkový
objem	objem	k1gInSc1	objem
výroby	výroba	k1gFnSc2	výroba
byl	být	k5eAaImAgInS	být
pak	pak	k6eAd1	pak
následujícími	následující	k2eAgFnPc7d1	následující
horečkami	horečka	k1gFnPc7	horečka
zmenšován	zmenšovat	k5eAaImNgInS	zmenšovat
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
místní	místní	k2eAgFnPc1d1	místní
skupiny	skupina	k1gFnPc1	skupina
stále	stále	k6eAd1	stále
příležitostně	příležitostně	k6eAd1	příležitostně
zlato	zlato	k1gNnSc4	zlato
rýžují	rýžovat	k5eAaImIp3nP	rýžovat
ve	v	k7c4	v
(	(	kIx(	(
<g/>
většinou	většina	k1gFnSc7	většina
venkovských	venkovský	k2eAgFnPc2d1	venkovská
<g/>
)	)	kIx)	)
potocích	potok	k1gInPc6	potok
a	a	k8xC	a
malých	malý	k2eAgFnPc6d1	malá
řekách	řeka	k1gFnPc6	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Těžební	těžební	k2eAgInSc1d1	těžební
důl	důl	k1gInSc1	důl
Reed	Reeda	k1gFnPc2	Reeda
fungoval	fungovat	k5eAaImAgInS	fungovat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1912	[number]	k4	1912
<g/>
.	.	kIx.	.
</s>
<s>
Charlottská	Charlottský	k2eAgFnSc1d1	Charlottský
mincovna	mincovna	k1gFnSc1	mincovna
byla	být	k5eAaImAgFnS	být
aktivní	aktivní	k2eAgFnSc1d1	aktivní
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1861	[number]	k4	1861
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ji	on	k3xPp3gFnSc4	on
získali	získat	k5eAaPmAgMnP	získat
při	při	k7c6	při
vypuknutí	vypuknutí	k1gNnSc6	vypuknutí
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
Severu	sever	k1gInSc2	sever
proti	proti	k7c3	proti
Jihu	jih	k1gInSc3	jih
ozbrojené	ozbrojený	k2eAgFnSc2d1	ozbrojená
síly	síla	k1gFnSc2	síla
Konfederace	konfederace	k1gFnSc2	konfederace
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
války	válka	k1gFnSc2	válka
už	už	k6eAd1	už
mincovna	mincovna	k1gFnSc1	mincovna
nebyla	být	k5eNaImAgFnS	být
znovu	znovu	k6eAd1	znovu
otevřena	otevřen	k2eAgFnSc1d1	otevřena
<g/>
,	,	kIx,	,
v	v	k7c6	v
její	její	k3xOp3gFnSc6	její
budově	budova	k1gFnSc6	budova
dnes	dnes	k6eAd1	dnes
sídlí	sídlet	k5eAaImIp3nS	sídlet
Mint	Mint	k1gMnSc1	Mint
Museum	museum	k1gNnSc1	museum
of	of	k?	of
Art	Art	k1gFnSc1	Art
(	(	kIx(	(
<g/>
Mincovní	mincovní	k2eAgNnSc1d1	mincovní
muzeum	muzeum	k1gNnSc1	muzeum
umění	umění	k1gNnSc2	umění
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
začaly	začít	k5eAaPmAgFnP	začít
ve	v	k7c6	v
městě	město	k1gNnSc6	město
vznikat	vznikat	k5eAaImF	vznikat
nové	nový	k2eAgInPc4d1	nový
kostely	kostel	k1gInPc4	kostel
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
presbyteriánské	presbyteriánský	k2eAgNnSc1d1	presbyteriánské
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
baptistické	baptistický	k2eAgFnSc2d1	baptistická
<g/>
,	,	kIx,	,
metodistické	metodistický	k2eAgFnSc2d1	metodistická
<g/>
,	,	kIx,	,
episkopalistické	episkopalistický	k2eAgFnSc2d1	episkopalistický
<g/>
,	,	kIx,	,
luteránské	luteránský	k2eAgFnSc2d1	luteránská
a	a	k8xC	a
katolické	katolický	k2eAgFnSc2d1	katolická
víry	víra	k1gFnSc2	víra
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
nich	on	k3xPp3gMnPc2	on
pak	pak	k6eAd1	pak
Charlotte	Charlott	k1gInSc5	Charlott
získalo	získat	k5eAaPmAgNnS	získat
svou	svůj	k3xOyFgFnSc4	svůj
přezdívku	přezdívka	k1gFnSc4	přezdívka
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
City	City	k1gFnSc2	City
of	of	k?	of
Churches	Churches	k1gInSc1	Churches
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
zažilo	zažít	k5eAaPmAgNnS	zažít
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
rozkvět	rozkvět	k1gInSc4	rozkvět
po	po	k7c6	po
občanské	občanský	k2eAgFnSc6d1	občanská
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
Charlotte	Charlott	k1gInSc5	Charlott
bylo	být	k5eAaImAgNnS	být
centrem	centrum	k1gNnSc7	centrum
výroby	výroba	k1gFnSc2	výroba
bavlny	bavlna	k1gFnSc2	bavlna
a	a	k8xC	a
železničním	železniční	k2eAgInSc7d1	železniční
uzlem	uzel	k1gInSc7	uzel
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
americká	americký	k2eAgFnSc1d1	americká
vláda	vláda	k1gFnSc1	vláda
založila	založit	k5eAaPmAgFnS	založit
severně	severně	k6eAd1	severně
od	od	k7c2	od
dnešního	dnešní	k2eAgInSc2d1	dnešní
Wilkinsonova	Wilkinsonův	k2eAgInSc2d1	Wilkinsonův
bulváru	bulvár	k1gInSc2	bulvár
Camp	camp	k1gInSc4	camp
Greene	Green	k1gInSc5	Green
<g/>
,	,	kIx,	,
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
opět	opět	k6eAd1	opět
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
vojáků	voják	k1gMnPc2	voják
a	a	k8xC	a
válečných	válečný	k2eAgMnPc2d1	válečný
dodavatelů	dodavatel	k1gMnPc2	dodavatel
zde	zde	k6eAd1	zde
navíc	navíc	k6eAd1	navíc
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
i	i	k9	i
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
přispělo	přispět	k5eAaPmAgNnS	přispět
i	i	k9	i
z	z	k7c2	z
dlouhodobého	dlouhodobý	k2eAgNnSc2d1	dlouhodobé
hlediska	hledisko	k1gNnSc2	hledisko
k	k	k7c3	k
nárůstu	nárůst	k1gInSc3	nárůst
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Charlottský	Charlottský	k2eAgInSc1d1	Charlottský
moderní	moderní	k2eAgInSc1d1	moderní
bankovnický	bankovnický	k2eAgInSc1d1	bankovnický
průmysl	průmysl	k1gInSc1	průmysl
získal	získat	k5eAaPmAgInS	získat
na	na	k7c6	na
důležitosti	důležitost	k1gFnSc6	důležitost
v	v	k7c6	v
sedmdesátých	sedmdesátý	k4xOgNnPc6	sedmdesátý
a	a	k8xC	a
devadesátých	devadesátý	k4xOgNnPc6	devadesátý
letech	léto	k1gNnPc6	léto
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
především	především	k9	především
díky	díky	k7c3	díky
finančníkovi	finančník	k1gMnSc3	finančník
Hugh	Hugh	k1gMnSc1	Hugh
McCollovi	McColl	k1gMnSc3	McColl
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
jeho	jeho	k3xOp3gNnSc7	jeho
vedením	vedení	k1gNnSc7	vedení
se	se	k3xPyFc4	se
z	z	k7c2	z
North	Northa	k1gFnPc2	Northa
Carolina	Carolin	k2eAgMnSc2d1	Carolin
National	National	k1gMnSc2	National
Bank	banka	k1gFnPc2	banka
(	(	kIx(	(
<g/>
Národní	národní	k2eAgFnSc2d1	národní
banky	banka	k1gFnSc2	banka
Severní	severní	k2eAgFnSc2d1	severní
Karolíny	Karolína	k1gFnSc2	Karolína
<g/>
)	)	kIx)	)
stal	stát	k5eAaPmAgMnS	stát
impozantní	impozantní	k2eAgMnSc1d1	impozantní
národní	národní	k2eAgMnSc1d1	národní
hráč	hráč	k1gMnSc1	hráč
<g/>
,	,	kIx,	,
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
<g/>
,	,	kIx,	,
skrze	skrze	k?	skrze
řadu	řada	k1gFnSc4	řada
agresivních	agresivní	k2eAgMnPc2d1	agresivní
zisků	zisk	k1gInPc2	zisk
<g/>
,	,	kIx,	,
Bank	banka	k1gFnPc2	banka
of	of	k?	of
America	America	k1gMnSc1	America
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
banka	banka	k1gFnSc1	banka
<g/>
,	,	kIx,	,
First	First	k1gFnSc1	First
Union	union	k1gInSc1	union
<g/>
,	,	kIx,	,
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
podobný	podobný	k2eAgInSc4d1	podobný
růst	růst	k1gInSc4	růst
<g/>
,	,	kIx,	,
a	a	k8xC	a
nyní	nyní	k6eAd1	nyní
je	být	k5eAaImIp3nS	být
po	po	k7c4	po
fúzi	fúze	k1gFnSc4	fúze
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xC	jako
Wachovia	Wachovia	k1gFnSc1	Wachovia
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
na	na	k7c6	na
základě	základ	k1gInSc6	základ
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
aktiv	aktiv	k1gInSc1	aktiv
<g/>
,	,	kIx,	,
Charlotte	Charlott	k1gInSc5	Charlott
po	po	k7c6	po
New	New	k1gMnSc6	New
York	York	k1gInSc1	York
City	City	k1gFnSc6	City
druhé	druhý	k4xOgNnSc4	druhý
největší	veliký	k2eAgNnSc4d3	veliký
bankovní	bankovní	k2eAgNnSc4d1	bankovní
centrum	centrum	k1gNnSc4	centrum
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
731	[number]	k4	731
424	[number]	k4	424
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
50,0	[number]	k4	50,0
<g/>
%	%	kIx~	%
Bílí	bílý	k2eAgMnPc1d1	bílý
Američané	Američan	k1gMnPc1	Američan
35,0	[number]	k4	35,0
<g/>
%	%	kIx~	%
Afroameričané	Afroameričan	k1gMnPc1	Afroameričan
0,5	[number]	k4	0,5
<g/>
%	%	kIx~	%
Američtí	americký	k2eAgMnPc1d1	americký
indiáni	indián	k1gMnPc1	indián
5,0	[number]	k4	5,0
<g/>
%	%	kIx~	%
Asijští	asijský	k2eAgMnPc1d1	asijský
Američané	Američan	k1gMnPc1	Američan
0,1	[number]	k4	0,1
<g/>
%	%	kIx~	%
Pacifičtí	pacifický	k2eAgMnPc1d1	pacifický
ostrované	ostrovan	k1gMnPc1	ostrovan
6,8	[number]	k4	6,8
<g/>
%	%	kIx~	%
Jiná	jiný	k2eAgFnSc1d1	jiná
rasa	rasa	k1gFnSc1	rasa
2,7	[number]	k4	2,7
<g/>
%	%	kIx~	%
Dvě	dva	k4xCgNnPc1	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
ras	ras	k1gMnSc1	ras
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
hispánského	hispánský	k2eAgMnSc2d1	hispánský
nebo	nebo	k8xC	nebo
latinskoamerického	latinskoamerický	k2eAgInSc2d1	latinskoamerický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
rasu	rasa	k1gFnSc4	rasa
<g/>
,	,	kIx,	,
tvořili	tvořit	k5eAaImAgMnP	tvořit
13,1	[number]	k4	13,1
<g/>
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
odhadu	odhad	k1gInSc2	odhad
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Charlotte	Charlott	k1gInSc5	Charlott
žije	žít	k5eAaImIp3nS	žít
610	[number]	k4	610
949	[number]	k4	949
a	a	k8xC	a
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Mecklenburg	Mecklenburg	k1gMnSc1	Mecklenburg
896	[number]	k4	896
372	[number]	k4	372
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
žijících	žijící	k2eAgMnPc2d1	žijící
v	v	k7c6	v
mecklenburském	mecklenburský	k2eAgInSc6d1	mecklenburský
okrese	okres	k1gInSc6	okres
překročí	překročit	k5eAaPmIp3nS	překročit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
1	[number]	k4	1
milion	milion	k4xCgInSc1	milion
<g/>
.	.	kIx.	.
</s>
<s>
Kombinovaná	kombinovaný	k2eAgFnSc1d1	kombinovaná
statistická	statistický	k2eAgFnSc1d1	statistická
oblast	oblast	k1gFnSc1	oblast
Charlotte-Gastonia-Concord	Charlotte-Gastonia-Concorda	k1gFnPc2	Charlotte-Gastonia-Concorda
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
splněna	splněn	k2eAgNnPc4d1	splněno
určitá	určitý	k2eAgNnPc4d1	určité
kritéria	kritérion	k1gNnPc4	kritérion
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
sousední	sousední	k2eAgFnSc2d1	sousední
statistické	statistický	k2eAgFnSc2d1	statistická
oblasti	oblast	k1gFnSc2	oblast
spojit	spojit	k5eAaPmF	spojit
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
2	[number]	k4	2
420	[number]	k4	420
745	[number]	k4	745
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
výsledků	výsledek	k1gInPc2	výsledek
komplexnějšího	komplexní	k2eAgNnSc2d2	komplexnější
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
je	být	k5eAaImIp3nS	být
hustota	hustota	k1gFnSc1	hustota
charlottských	charlottský	k2eAgMnPc2d1	charlottský
obyvatel	obyvatel	k1gMnPc2	obyvatel
861.9	[number]	k4	861.9
<g/>
/	/	kIx~	/
<g/>
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
průměrné	průměrný	k2eAgFnSc6d1	průměrná
hustotě	hustota	k1gFnSc6	hustota
367.2	[number]	k4	367.2
<g/>
/	/	kIx~	/
<g/>
km2	km2	k4	km2
nalezneme	naleznout	k5eAaPmIp1nP	naleznout
230	[number]	k4	230
434	[number]	k4	434
obytných	obytný	k2eAgFnPc2d1	obytná
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
příjem	příjem	k1gInSc1	příjem
domácnosti	domácnost	k1gFnSc2	domácnost
činí	činit	k5eAaImIp3nS	činit
$	$	kIx~	$
<g/>
46,975	[number]	k4	46,975
a	a	k8xC	a
průměrný	průměrný	k2eAgInSc1d1	průměrný
příjem	příjem	k1gInSc1	příjem
rodiny	rodina	k1gFnSc2	rodina
je	být	k5eAaImIp3nS	být
$	$	kIx~	$
<g/>
56,517	[number]	k4	56,517
<g/>
.	.	kIx.	.
</s>
<s>
Muži	muž	k1gMnPc1	muž
průměrně	průměrně	k6eAd1	průměrně
vydělávají	vydělávat	k5eAaImIp3nP	vydělávat
$	$	kIx~	$
<g/>
38,767	[number]	k4	38,767
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
ženy	žena	k1gFnPc1	žena
$	$	kIx~	$
<g/>
29,218	[number]	k4	29,218
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
příjem	příjem	k1gInSc1	příjem
na	na	k7c4	na
jednoho	jeden	k4xCgMnSc4	jeden
obyvatele	obyvatel	k1gMnSc4	obyvatel
je	být	k5eAaImIp3nS	být
$	$	kIx~	$
<g/>
26,823	[number]	k4	26,823
<g/>
.	.	kIx.	.
10,6	[number]	k4	10,6
%	%	kIx~	%
všech	všecek	k3xTgMnPc2	všecek
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
7,8	[number]	k4	7,8
%	%	kIx~	%
rodin	rodina	k1gFnPc2	rodina
žije	žít	k5eAaImIp3nS	žít
pod	pod	k7c7	pod
hranicí	hranice	k1gFnSc7	hranice
chudoby	chudoba	k1gFnSc2	chudoba
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
hranicí	hranice	k1gFnSc7	hranice
chudoby	chudoba	k1gFnSc2	chudoba
žije	žít	k5eAaImIp3nS	žít
i	i	k9	i
13,8	[number]	k4	13,8
%	%	kIx~	%
lidí	člověk	k1gMnPc2	člověk
mladších	mladý	k2eAgInPc2d2	mladší
osmnácti	osmnáct	k4xCc2	osmnáct
let	léto	k1gNnPc2	léto
a	a	k8xC	a
7,8	[number]	k4	7,8
%	%	kIx~	%
starších	starý	k2eAgInPc2d2	starší
šedesáti	šedesát	k4xCc7	šedesát
pěti	pět	k4xCc2	pět
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
na	na	k7c4	na
629	[number]	k4	629
km2	km2	k4	km2
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
627,5	[number]	k4	627,5
km2	km2	k4	km2
tvoří	tvořit	k5eAaImIp3nS	tvořit
pevnina	pevnina	k1gFnSc1	pevnina
a	a	k8xC	a
1,6	[number]	k4	1,6
km2	km2	k4	km2
voda	voda	k1gFnSc1	voda
<g/>
.	.	kIx.	.
</s>
<s>
Charlotte	Charlotit	k5eAaBmRp2nP	Charlotit
tvoří	tvořit	k5eAaImIp3nS	tvořit
většinu	většina	k1gFnSc4	většina
Mecklelnburg	Mecklelnburg	k1gMnSc1	Mecklelnburg
County	Counta	k1gFnSc2	Counta
(	(	kIx(	(
<g/>
okresu	okres	k1gInSc2	okres
<g/>
)	)	kIx)	)
v	v	k7c6	v
Carolina	Carolin	k2eAgFnSc1d1	Carolina
Piedmont	Piedmont	k1gInSc1	Piedmont
(	(	kIx(	(
<g/>
oblast	oblast	k1gFnSc1	oblast
náhorní	náhorní	k2eAgFnSc2d1	náhorní
plošiny	plošina	k1gFnSc2	plošina
na	na	k7c6	na
východě	východ	k1gInSc6	východ
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
leží	ležet	k5eAaImIp3nS	ležet
mezi	mezi	k7c7	mezi
Atlantskou	atlantský	k2eAgFnSc7d1	Atlantská
nížinou	nížina	k1gFnSc7	nížina
a	a	k8xC	a
Appalačským	Appalačský	k2eAgNnSc7d1	Appalačské
pohořím	pohoří	k1gNnSc7	pohoří
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Uptown	Uptown	k1gMnSc1	Uptown
Charlotte	Charlott	k1gInSc5	Charlott
(	(	kIx(	(
<g/>
horní	horní	k2eAgFnSc1d1	horní
část	část	k1gFnSc1	část
města	město	k1gNnSc2	město
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
postaveno	postavit	k5eAaPmNgNnS	postavit
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
dvou	dva	k4xCgFnPc2	dva
bývalých	bývalý	k2eAgFnPc2d1	bývalá
povrchových	povrchový	k2eAgFnPc2d1	povrchová
dolů	dol	k1gInPc2	dol
zlata	zlato	k1gNnSc2	zlato
-	-	kIx~	-
Catherine	Catherin	k1gInSc5	Catherin
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
a	a	k8xC	a
Rudisill	Rudisillum	k1gNnPc2	Rudisillum
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
název	název	k1gInSc4	název
získala	získat	k5eAaPmAgFnS	získat
tato	tento	k3xDgFnSc1	tento
část	část	k1gFnSc1	část
města	město	k1gNnSc2	město
podle	podle	k7c2	podle
polohy	poloha	k1gFnSc2	poloha
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
totiž	totiž	k9	totiž
situována	situovat	k5eAaBmNgFnS	situovat
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
dlouhého	dlouhý	k2eAgNnSc2d1	dlouhé
stoupání	stoupání	k1gNnSc2	stoupání
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgFnPc7	dva
říčkami	říčka	k1gFnPc7	říčka
<g/>
.	.	kIx.	.
</s>
<s>
Charlotte	Charlotit	k5eAaBmRp2nP	Charlotit
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
228	[number]	k4	228
metrů	metr	k1gInPc2	metr
(	(	kIx(	(
<g/>
Charlotte	Charlott	k1gInSc5	Charlott
Douglas	Douglas	k1gInSc4	Douglas
mezinárodní	mezinárodní	k2eAgNnPc4d1	mezinárodní
letiště	letiště	k1gNnPc4	letiště
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
American	American	k1gInSc1	American
Lung	Lung	k1gInSc1	Lung
Association	Association	k1gInSc1	Association
(	(	kIx(	(
<g/>
Americká	americký	k2eAgFnSc1d1	americká
plicní	plicní	k2eAgFnSc1d1	plicní
asociace	asociace	k1gFnSc1	asociace
<g/>
)	)	kIx)	)
zařadila	zařadit	k5eAaPmAgFnS	zařadit
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
zprávě	zpráva	k1gFnSc6	zpráva
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
Charlotte	Charlott	k1gInSc5	Charlott
na	na	k7c4	na
šestnácté	šestnáctý	k4xOgNnSc4	šestnáctý
místo	místo	k1gNnSc4	místo
žebříčku	žebříček	k1gInSc2	žebříček
amerických	americký	k2eAgNnPc2d1	americké
měst	město	k1gNnPc2	město
nejvíce	hodně	k6eAd3	hodně
znečištěných	znečištěný	k2eAgMnPc2d1	znečištěný
smogem	smog	k1gInSc7	smog
<g/>
.	.	kIx.	.
</s>
<s>
Charlotte	Charlotte	k5eAaPmIp2nP	Charlotte
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
severoamerickém	severoamerický	k2eAgNnSc6d1	severoamerické
vlhkém	vlhký	k2eAgNnSc6d1	vlhké
subtropickém	subtropický	k2eAgNnSc6d1	subtropické
pásmu	pásmo	k1gNnSc6	pásmo
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
má	mít	k5eAaImIp3nS	mít
mírné	mírný	k2eAgFnPc4d1	mírná
zimy	zima	k1gFnPc4	zima
a	a	k8xC	a
teplá	teplý	k2eAgNnPc4d1	teplé
vlhká	vlhký	k2eAgNnPc4d1	vlhké
léta	léto	k1gNnPc4	léto
<g/>
.	.	kIx.	.
</s>
<s>
Lednová	lednový	k2eAgFnSc1d1	lednová
ranní	ranní	k1gFnSc1	ranní
minima	minimum	k1gNnSc2	minimum
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
kolem	kolem	k7c2	kolem
0	[number]	k4	0
°	°	k?	°
<g/>
C	C	kA	C
(	(	kIx(	(
<g/>
32	[number]	k4	32
°	°	k?	°
<g/>
F	F	kA	F
<g/>
)	)	kIx)	)
a	a	k8xC	a
odpolední	odpolední	k2eAgFnSc1d1	odpolední
maxima	maxima	k1gFnSc1	maxima
kolem	kolem	k7c2	kolem
11	[number]	k4	11
°	°	k?	°
<g/>
C	C	kA	C
(	(	kIx(	(
<g/>
51	[number]	k4	51
°	°	k?	°
<g/>
F	F	kA	F
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
jsou	být	k5eAaImIp3nP	být
minima	minimum	k1gNnPc1	minimum
průměrně	průměrně	k6eAd1	průměrně
22	[number]	k4	22
°	°	k?	°
<g/>
C	C	kA	C
(	(	kIx(	(
<g/>
71	[number]	k4	71
°	°	k?	°
<g/>
F	F	kA	F
<g/>
)	)	kIx)	)
a	a	k8xC	a
maxima	maximum	k1gNnSc2	maximum
32	[number]	k4	32
°	°	k?	°
<g/>
C	C	kA	C
(	(	kIx(	(
<g/>
90	[number]	k4	90
°	°	k?	°
<g/>
F	F	kA	F
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
zaznamenaná	zaznamenaný	k2eAgFnSc1d1	zaznamenaná
teplota	teplota	k1gFnSc1	teplota
byla	být	k5eAaImAgFnS	být
40	[number]	k4	40
°	°	k?	°
<g/>
C	C	kA	C
(	(	kIx(	(
<g/>
104	[number]	k4	104
°	°	k?	°
<g/>
F	F	kA	F
<g/>
)	)	kIx)	)
6	[number]	k4	6
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1954	[number]	k4	1954
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
nejnižší	nízký	k2eAgMnPc1d3	nejnižší
zaznamenaní	zaznamenaný	k2eAgMnPc1d1	zaznamenaný
teplota	teplota	k1gFnSc1	teplota
<g/>
,	,	kIx,	,
-21	-21	k4	-21
°	°	k?	°
<g/>
C	C	kA	C
(	(	kIx(	(
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
°	°	k?	°
<g/>
F	F	kA	F
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
naměřena	naměřit	k5eAaBmNgFnS	naměřit
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
</s>
<s>
Charlotte	Charlotte	k5eAaPmIp2nP	Charlotte
je	on	k3xPp3gFnPc4	on
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
poloze	poloha	k1gFnSc3	poloha
ovlivňováno	ovlivňován	k2eAgNnSc1d1	ovlivňováno
Golfským	golfský	k2eAgInSc7d1	golfský
proudem	proud	k1gInSc7	proud
-	-	kIx~	-
proudí	proudit	k5eAaPmIp3nS	proudit
sem	sem	k6eAd1	sem
subtropická	subtropický	k2eAgFnSc1d1	subtropická
vlhkost	vlhkost	k1gFnSc1	vlhkost
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
roku	rok	k1gInSc2	rok
má	mít	k5eAaImIp3nS	mít
tak	tak	k9	tak
dostatek	dostatek	k1gInSc1	dostatek
srážek	srážka	k1gFnPc2	srážka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
množství	množství	k1gNnSc1	množství
jasných	jasný	k2eAgNnPc2d1	jasné
slunečných	slunečný	k2eAgNnPc2d1	slunečné
a	a	k8xC	a
příjemně	příjemně	k6eAd1	příjemně
teplých	teplý	k2eAgInPc2d1	teplý
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Průměrně	průměrně	k6eAd1	průměrně
spadne	spadnout	k5eAaPmIp3nS	spadnout
v	v	k7c6	v
Charlotte	Charlott	k1gInSc5	Charlott
za	za	k7c4	za
rok	rok	k1gInSc4	rok
1105,3	[number]	k4	1105,3
mm	mm	kA	mm
(	(	kIx(	(
<g/>
43,52	[number]	k4	43,52
in	in	k?	in
<g/>
)	)	kIx)	)
srážek	srážka	k1gFnPc2	srážka
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
sněhu	sníh	k1gInSc2	sníh
a	a	k8xC	a
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
vnitrozemní	vnitrozemní	k2eAgFnSc2d1	vnitrozemní
polohy	poloha	k1gFnSc2	poloha
mnohem	mnohem	k6eAd1	mnohem
častějších	častý	k2eAgFnPc2d2	častější
ledových	ledový	k2eAgFnPc2d1	ledová
bouří	bouř	k1gFnPc2	bouř
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgFnPc4d1	významná
bouře	bouř	k1gFnPc4	bouř
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
město	město	k1gNnSc1	město
přímo	přímo	k6eAd1	přímo
zasáhl	zasáhnout	k5eAaPmAgMnS	zasáhnout
hurikán	hurikán	k1gInSc4	hurikán
Hugo	Hugo	k1gMnSc1	Hugo
<g/>
.	.	kIx.	.
</s>
<s>
Městem	město	k1gNnSc7	město
se	se	k3xPyFc4	se
prohnal	prohnat	k5eAaPmAgMnS	prohnat
rychlostí	rychlost	k1gFnSc7	rychlost
přes	přes	k7c4	přes
160	[number]	k4	160
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
a	a	k8xC	a
způsobil	způsobit	k5eAaPmAgInS	způsobit
tak	tak	k6eAd1	tak
obrovské	obrovský	k2eAgFnPc4d1	obrovská
škody	škoda	k1gFnPc4	škoda
na	na	k7c6	na
majetku	majetek	k1gInSc6	majetek
<g/>
.	.	kIx.	.
98	[number]	k4	98
procent	procento	k1gNnPc2	procento
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
ocitlo	ocitnout	k5eAaPmAgNnS	ocitnout
bez	bez	k7c2	bez
elektřiny	elektřina	k1gFnSc2	elektřina
<g/>
.	.	kIx.	.
</s>
<s>
Elektrická	elektrický	k2eAgNnPc1d1	elektrické
vedení	vedení	k1gNnPc1	vedení
byla	být	k5eAaImAgNnP	být
na	na	k7c6	na
některých	některý	k3yIgNnPc6	některý
místech	místo	k1gNnPc6	místo
opravena	opravit	k5eAaPmNgFnS	opravit
až	až	k9	až
po	po	k7c6	po
několika	několik	k4yIc6	několik
týdnech	týden	k1gInPc6	týden
a	a	k8xC	a
odstraňování	odstraňování	k1gNnSc1	odstraňování
škod	škoda	k1gFnPc2	škoda
trvalo	trvat	k5eAaImAgNnS	trvat
měsíce	měsíc	k1gInPc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Charlotte	Charlott	k1gInSc5	Charlott
bylo	být	k5eAaImAgNnS	být
tehdy	tehdy	k6eAd1	tehdy
zničeno	zničit	k5eAaPmNgNnS	zničit
přes	přes	k7c4	přes
80	[number]	k4	80
000	[number]	k4	000
stromů	strom	k1gInPc2	strom
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
Charlotte	Charlott	k1gInSc5	Charlott
leží	ležet	k5eAaImIp3nS	ležet
daleko	daleko	k6eAd1	daleko
od	od	k7c2	od
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
,	,	kIx,	,
obyvatelé	obyvatel	k1gMnPc1	obyvatel
<g/>
,	,	kIx,	,
vedení	vedení	k1gNnSc1	vedení
města	město	k1gNnSc2	město
ani	ani	k9	ani
technické	technický	k2eAgNnSc1d1	technické
vybavení	vybavení	k1gNnSc1	vybavení
nebylo	být	k5eNaImAgNnS	být
připraveno	připravit	k5eAaPmNgNnS	připravit
na	na	k7c4	na
tak	tak	k6eAd1	tak
silný	silný	k2eAgInSc4d1	silný
hurikán	hurikán	k1gInSc4	hurikán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2002	[number]	k4	2002
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
Charlotte	Charlott	k1gInSc5	Charlott
(	(	kIx(	(
<g/>
a	a	k8xC	a
většinu	většina	k1gFnSc4	většina
centrální	centrální	k2eAgFnSc2d1	centrální
Severní	severní	k2eAgFnSc2d1	severní
Karolíny	Karolína	k1gFnSc2	Karolína
<g/>
)	)	kIx)	)
obrovská	obrovský	k2eAgFnSc1d1	obrovská
lední	lední	k2eAgFnSc1d1	lední
bouře	bouře	k1gFnSc1	bouře
<g/>
.	.	kIx.	.
1,2	[number]	k4	1,2
milionů	milion	k4xCgInPc2	milion
zákazníků	zákazník	k1gMnPc2	zákazník
Duke	Duk	k1gFnSc2	Duk
Power	Power	k1gMnSc1	Power
(	(	kIx(	(
<g/>
dodavatel	dodavatel	k1gMnSc1	dodavatel
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
ocitlo	ocitnout	k5eAaPmAgNnS	ocitnout
bez	bez	k7c2	bez
elektřiny	elektřina	k1gFnSc2	elektřina
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
zástupce	zástupce	k1gMnSc2	zástupce
Duke	Duk	k1gInSc2	Duk
Energy	Energ	k1gInPc1	Energ
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Tahle	tenhle	k3xDgFnSc1	tenhle
ledová	ledový	k2eAgFnSc1d1	ledová
bouře	bouře	k1gFnSc1	bouře
překonává	překonávat	k5eAaImIp3nS	překonávat
škody	škoda	k1gFnPc4	škoda
způsobené	způsobený	k2eAgFnPc4d1	způsobená
hurikánem	hurikán	k1gInSc7	hurikán
Hugem	Hugo	k1gMnSc7	Hugo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
způsobil	způsobit	k5eAaPmAgInS	způsobit
696	[number]	k4	696
000	[number]	k4	000
výpadků	výpadek	k1gInPc2	výpadek
proudu	proud	k1gInSc2	proud
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Během	během	k7c2	během
výjimečně	výjimečně	k6eAd1	výjimečně
chladného	chladný	k2eAgInSc2d1	chladný
prosince	prosinec	k1gInSc2	prosinec
bylo	být	k5eAaImAgNnS	být
mnoho	mnoho	k4c1	mnoho
lidí	člověk	k1gMnPc2	člověk
bez	bez	k7c2	bez
elektrického	elektrický	k2eAgInSc2d1	elektrický
proudu	proud	k1gInSc2	proud
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dva	dva	k4xCgInPc4	dva
týdny	týden	k1gInPc4	týden
<g/>
.	.	kIx.	.
</s>
<s>
Uptown	Uptown	k1gNnSc1	Uptown
<g/>
:	:	kIx,	:
hlavní	hlavní	k2eAgFnSc1d1	hlavní
obchodní	obchodní	k2eAgFnSc1d1	obchodní
oblast	oblast	k1gFnSc1	oblast
složená	složený	k2eAgFnSc1d1	složená
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
čtyř	čtyři	k4xCgInPc2	čtyři
volebních	volební	k2eAgInPc2d1	volební
obvodů	obvod	k1gInPc2	obvod
Cotswold	Cotswolda	k1gFnPc2	Cotswolda
<g/>
:	:	kIx,	:
křižovatka	křižovatka	k1gFnSc1	křižovatka
silnic	silnice	k1gFnPc2	silnice
Randolph	Randolph	k1gMnSc1	Randolph
Road	Road	k1gMnSc1	Road
a	a	k8xC	a
Sharon	Sharon	k1gMnSc1	Sharon
Amity	Amita	k1gFnSc2	Amita
Road	Road	k1gMnSc1	Road
South	South	k1gMnSc1	South
End	End	k1gMnSc1	End
<g/>
:	:	kIx,	:
na	na	k7c4	na
jih	jih	k1gInSc4	jih
od	od	k7c2	od
Uptown	Uptowna	k1gFnPc2	Uptowna
Dilworth	Dilwortha	k1gFnPc2	Dilwortha
<g/>
:	:	kIx,	:
na	na	k7c4	na
jihozápad	jihozápad	k1gInSc4	jihozápad
od	od	k7c2	od
Uptown	Uptowna	k1gFnPc2	Uptowna
Elizabeth	Elizabeth	k1gFnSc2	Elizabeth
<g/>
:	:	kIx,	:
podél	podél	k7c2	podél
Elizabeth	Elizabeth	k1gFnSc2	Elizabeth
Avenue	avenue	k1gFnSc2	avenue
Myers	Myersa	k1gFnPc2	Myersa
Park	park	k1gInSc1	park
<g/>
:	:	kIx,	:
na	na	k7c4	na
jih	jih	k1gInSc4	jih
<g />
.	.	kIx.	.
</s>
<s>
od	od	k7c2	od
South	Southa	k1gFnPc2	Southa
End	End	k1gFnSc2	End
Plaza-Midwood	Plaza-Midwood	k1gInSc1	Plaza-Midwood
<g/>
:	:	kIx,	:
na	na	k7c4	na
východ	východ	k1gInSc4	východ
od	od	k7c2	od
Uptown	Uptowna	k1gFnPc2	Uptowna
a	a	k8xC	a
podél	podél	k7c2	podél
The	The	k1gMnSc2	The
Plaza	plaz	k1gMnSc2	plaz
NoDa	NoDus	k1gMnSc2	NoDus
<g/>
:	:	kIx,	:
kolem	kolem	k7c2	kolem
Nort	Norta	k1gFnPc2	Norta
Davidson	Davidson	k1gInSc1	Davidson
Street	Street	k1gMnSc1	Street
SouthPark	SouthPark	k1gInSc1	SouthPark
<g/>
:	:	kIx,	:
křižovatka	křižovatka	k1gFnSc1	křižovatka
silnic	silnice	k1gFnPc2	silnice
Sharon	Sharon	k1gMnSc1	Sharon
Road	Road	k1gMnSc1	Road
a	a	k8xC	a
Fairview	Fairview	k1gFnSc1	Fairview
Road	Roado	k1gNnPc2	Roado
University	universita	k1gFnSc2	universita
City	city	k1gNnSc1	city
<g/>
:	:	kIx,	:
nejseverovýchodnější	severovýchodný	k2eAgFnSc1d3	severovýchodný
část	část	k1gFnSc1	část
města	město	k1gNnSc2	město
kolem	kolem	k7c2	kolem
charlottské	charlottský	k2eAgFnSc2d1	charlottský
univerzity	univerzita	k1gFnSc2	univerzita
Eastland	Eastlanda	k1gFnPc2	Eastlanda
<g/>
:	:	kIx,	:
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
východního	východní	k2eAgInSc2d1	východní
Charlotte	Charlott	k1gInSc5	Charlott
Starmount	Starmounto	k1gNnPc2	Starmounto
<g/>
<g />
.	.	kIx.	.
</s>
<s>
:	:	kIx,	:
oblast	oblast	k1gFnSc1	oblast
South	South	k1gMnSc1	South
Boulevard	Boulevard	k1gMnSc1	Boulevard
Ballantyne	Ballantyn	k1gInSc5	Ballantyn
<g/>
:	:	kIx,	:
podél	podél	k7c2	podél
hranice	hranice	k1gFnSc2	hranice
mezi	mezi	k7c7	mezi
Severní	severní	k2eAgFnSc7d1	severní
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc7d1	jižní
Karolínou	Karolína	k1gFnSc7	Karolína
The	The	k1gFnSc2	The
Arboretum	arboretum	k1gNnSc4	arboretum
<g/>
:	:	kIx,	:
podél	podél	k7c2	podél
silnice	silnice	k1gFnSc2	silnice
Pineville-Matthews	Pineville-Matthewsa	k1gFnPc2	Pineville-Matthewsa
Road	Road	k1gInSc1	Road
Steele	Steel	k1gInSc2	Steel
Creek	Creky	k1gFnPc2	Creky
<g/>
:	:	kIx,	:
nejjihozápadnější	jihozápadný	k2eAgFnSc1d3	jihozápadný
část	část	k1gFnSc1	část
Charlotte	Charlott	k1gMnSc5	Charlott
Biddleville	Biddlevill	k1gMnSc5	Biddlevill
<g/>
:	:	kIx,	:
nejzápadnější	západní	k2eAgFnSc1d3	nejzápadnější
část	část	k1gFnSc1	část
Charlotte	Charlott	k1gInSc5	Charlott
<g/>
,	,	kIx,	,
podel	podel	k1gInSc1	podel
silnice	silnice	k1gFnSc2	silnice
Beatties	Beattiesa	k1gFnPc2	Beattiesa
Ford	ford	k1gInSc1	ford
Road	Road	k1gMnSc1	Road
Derita	Derita	k1gMnSc1	Derita
<g/>
:	:	kIx,	:
na	na	k7c4	na
sever	sever	k1gInSc4	sever
of	of	k?	of
vnitrostátní	vnitrostátní	k2eAgFnSc2d1	vnitrostátní
silnice	silnice	k1gFnSc2	silnice
č.	č.	k?	č.
85	[number]	k4	85
(	(	kIx(	(
<g/>
I-	I-	k1gFnSc1	I-
<g/>
85	[number]	k4	85
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podél	podél	k7c2	podél
silnice	silnice	k1gFnSc2	silnice
West	West	k1gMnSc1	West
Sugar	Sugar	k1gMnSc1	Sugar
Creek	Creek	k1gMnSc1	Creek
Road	Road	k1gMnSc1	Road
Sedgefield	Sedgefield	k1gMnSc1	Sedgefield
<g/>
:	:	kIx,	:
na	na	k7c4	na
jih	jih	k1gInSc4	jih
od	od	k7c2	od
Dilworth	Dilwortha	k1gFnPc2	Dilwortha
Řízení	řízení	k1gNnSc4	řízení
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
založeno	založit	k5eAaPmNgNnS	založit
na	na	k7c4	na
tzv.	tzv.	kA	tzv.
council-manager	councilanager	k1gInSc4	council-manager
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
řízeno	řídit	k5eAaImNgNnS	řídit
zastupitelstvem	zastupitelstvo	k1gNnSc7	zastupitelstvo
(	(	kIx(	(
<g/>
council	council	k1gMnSc1	council
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
představuje	představovat	k5eAaImIp3nS	představovat
výkonnou	výkonný	k2eAgFnSc4d1	výkonná
moc	moc	k1gFnSc4	moc
(	(	kIx(	(
<g/>
vydává	vydávat	k5eAaImIp3nS	vydávat
nařízení	nařízení	k1gNnSc1	nařízení
<g/>
,	,	kIx,	,
určuje	určovat	k5eAaImIp3nS	určovat
hlasovací	hlasovací	k2eAgNnPc4d1	hlasovací
práva	právo	k1gNnPc4	právo
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
starosta	starosta	k1gMnSc1	starosta
(	(	kIx(	(
<g/>
mayor	mayor	k1gInSc1	mayor
<g/>
)	)	kIx)	)
město	město	k1gNnSc1	město
pouze	pouze	k6eAd1	pouze
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
<g/>
.	.	kIx.	.
</s>
<s>
Starosta	Starosta	k1gMnSc1	Starosta
a	a	k8xC	a
zastupitelstvo	zastupitelstvo	k1gNnSc1	zastupitelstvo
jsou	být	k5eAaImIp3nP	být
voleni	volit	k5eAaImNgMnP	volit
každé	každý	k3xTgInPc4	každý
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
na	na	k7c4	na
neomezený	omezený	k2eNgInSc4d1	neomezený
počet	počet	k1gInSc4	počet
funkčních	funkční	k2eAgNnPc2d1	funkční
období	období	k1gNnPc2	období
<g/>
.	.	kIx.	.
</s>
<s>
Starosta	Starosta	k1gMnSc1	Starosta
je	být	k5eAaImIp3nS	být
předsedou	předseda	k1gMnSc7	předseda
zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
z	z	k7c2	z
úřední	úřední	k2eAgFnSc2d1	úřední
moci	moc	k1gFnSc2	moc
(	(	kIx(	(
<g/>
ex	ex	k6eAd1	ex
officio	officio	k6eAd1	officio
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hlasuje	hlasovat	k5eAaImIp3nS	hlasovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
případě	případ	k1gInSc6	případ
nerozhodného	rozhodný	k2eNgNnSc2d1	nerozhodné
hlasování	hlasování	k1gNnSc2	hlasování
zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Charlottský	Charlottský	k2eAgMnSc1d1	Charlottský
starosta	starosta	k1gMnSc1	starosta
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
ostatních	ostatní	k2eAgMnPc2d1	ostatní
starostů	starosta	k1gMnPc2	starosta
tohoto	tento	k3xDgInSc2	tento
vládního	vládní	k2eAgInSc2d1	vládní
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
právo	právo	k1gNnSc4	právo
vetovat	vetovat	k5eAaBmF	vetovat
předpisy	předpis	k1gInPc4	předpis
vydané	vydaný	k2eAgFnPc4d1	vydaná
zastupitelstvem	zastupitelstvo	k1gNnSc7	zastupitelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Starostovo	Starostův	k2eAgNnSc1d1	Starostovo
veto	veto	k1gNnSc1	veto
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
přehlasováno	přehlasovat	k5eAaBmNgNnS	přehlasovat
dvoutřetinovou	dvoutřetinový	k2eAgFnSc7d1	dvoutřetinová
většinou	většina	k1gFnSc7	většina
v	v	k7c6	v
zastupitelstvu	zastupitelstvo	k1gNnSc6	zastupitelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Zastupitelstvo	zastupitelstvo	k1gNnSc1	zastupitelstvo
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
městského	městský	k2eAgMnSc4d1	městský
tajemníka	tajemník	k1gMnSc4	tajemník
(	(	kIx(	(
<g/>
city	city	k1gFnSc1	city
manager	manager	k1gMnSc1	manager
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
funkci	funkce	k1gFnSc4	funkce
hlavního	hlavní	k2eAgMnSc4d1	hlavní
vládního	vládní	k2eAgMnSc4d1	vládní
úředníka	úředník	k1gMnSc4	úředník
<g/>
.	.	kIx.	.
</s>
<s>
Volby	volba	k1gFnPc1	volba
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
ostatních	ostatní	k2eAgNnPc2d1	ostatní
měst	město	k1gNnPc2	město
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Karolíně	Karolína	k1gFnSc6	Karolína
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
založeny	založit	k5eAaPmNgFnP	založit
na	na	k7c4	na
stranictví	stranictví	k1gNnSc4	stranictví
<g/>
.	.	kIx.	.
</s>
<s>
Současný	současný	k2eAgMnSc1d1	současný
starosta	starosta	k1gMnSc1	starosta
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
Pat	pat	k1gInSc1	pat
McCrory	McCrora	k1gFnSc2	McCrora
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
republikánské	republikánský	k2eAgFnSc2d1	republikánská
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
McCroy	McCroa	k1gFnPc4	McCroa
zastává	zastávat	k5eAaImIp3nS	zastávat
funkci	funkce	k1gFnSc4	funkce
starosty	starosta	k1gMnSc2	starosta
od	od	k7c2	od
svého	svůj	k3xOyFgNnSc2	svůj
zvolení	zvolení	k1gNnSc1	zvolení
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
Starosta	Starosta	k1gMnSc1	Starosta
je	být	k5eAaImIp3nS	být
volen	volit	k5eAaImNgMnS	volit
každé	každý	k3xTgInPc4	každý
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
počet	počet	k1gInSc4	počet
jeho	jeho	k3xOp3gNnPc2	jeho
funkčních	funkční	k2eAgNnPc2d1	funkční
období	období	k1gNnPc2	období
není	být	k5eNaImIp3nS	být
omezen	omezit	k5eAaPmNgInS	omezit
<g/>
.	.	kIx.	.
</s>
<s>
Zastupitelstvo	zastupitelstvo	k1gNnSc1	zastupitelstvo
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
11	[number]	k4	11
členů	člen	k1gInPc2	člen
<g/>
:	:	kIx,	:
7	[number]	k4	7
z	z	k7c2	z
volebních	volební	k2eAgInPc2d1	volební
obvodů	obvod	k1gInPc2	obvod
a	a	k8xC	a
4	[number]	k4	4
at-large	atarge	k1gNnPc6	at-large
(	(	kIx(	(
<g/>
ti	ten	k3xDgMnPc1	ten
reprezentují	reprezentovat	k5eAaImIp3nP	reprezentovat
město	město	k1gNnSc4	město
jako	jako	k8xC	jako
celek	celek	k1gInSc4	celek
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
pouze	pouze	k6eAd1	pouze
jeho	jeho	k3xOp3gInPc1	jeho
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
volební	volební	k2eAgInPc1d1	volební
okresy	okres	k1gInPc1	okres
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současném	současný	k2eAgNnSc6d1	současné
zastupitelstvu	zastupitelstvo	k1gNnSc6	zastupitelstvo
mají	mít	k5eAaImIp3nP	mít
většinu	většina	k1gFnSc4	většina
<g/>
,	,	kIx,	,
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
7	[number]	k4	7
<g/>
:	:	kIx,	:
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
demokrati	demokrat	k1gMnPc1	demokrat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
měli	mít	k5eAaImAgMnP	mít
v	v	k7c6	v
Charlotte	Charlott	k1gInSc5	Charlott
silnou	silný	k2eAgFnSc4d1	silná
pozici	pozice	k1gFnSc4	pozice
severokarolínští	severokarolínský	k2eAgMnPc1d1	severokarolínský
republikáni	republikán	k1gMnPc1	republikán
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
měnící	měnící	k2eAgNnSc1d1	měnící
složení	složení	k1gNnSc1	složení
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
naopak	naopak	k6eAd1	naopak
upevňuje	upevňovat	k5eAaImIp3nS	upevňovat
postavení	postavení	k1gNnSc4	postavení
demokratické	demokratický	k2eAgFnSc2d1	demokratická
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
samosprávní	samosprávní	k2eAgFnPc1d1	samosprávní
obce	obec	k1gFnPc1	obec
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Karolíně	Karolína	k1gFnSc6	Karolína
nejsou	být	k5eNaImIp3nP	být
autonomní	autonomní	k2eAgFnSc4d1	autonomní
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
mnoho	mnoho	k4c1	mnoho
vládních	vládní	k2eAgNnPc2d1	vládní
rozhodnutí	rozhodnutí	k1gNnPc2	rozhodnutí
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
za	za	k7c4	za
vydávání	vydávání	k1gNnSc4	vydávání
nařízení	nařízení	k1gNnSc2	nařízení
je	být	k5eAaImIp3nS	být
zodpovědné	zodpovědný	k2eAgNnSc1d1	zodpovědné
zastupitelstvo	zastupitelstvo	k1gNnSc1	zastupitelstvo
<g/>
,	,	kIx,	,
schváleno	schválit	k5eAaPmNgNnS	schválit
North	Northa	k1gFnPc2	Northa
Carolina	Carolina	k1gFnSc1	Carolina
General	General	k1gFnSc1	General
Assembly	Assembly	k1gFnSc1	Assembly
(	(	kIx(	(
<g/>
Valným	valný	k2eAgNnSc7d1	Valné
shromážděním	shromáždění	k1gNnSc7	shromáždění
Severní	severní	k2eAgFnSc2d1	severní
Karolíny	Karolína	k1gFnSc2	Karolína
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byly	být	k5eAaImAgFnP	být
pravomoci	pravomoc	k1gFnPc1	pravomoc
samosprávních	samosprávní	k2eAgFnPc2d1	samosprávní
obcí	obec	k1gFnPc2	obec
široce	široko	k6eAd1	široko
interpretovány	interpretován	k2eAgFnPc1d1	interpretována
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
všichni	všechen	k3xTgMnPc1	všechen
zvolení	zvolený	k2eAgMnPc1d1	zvolený
starostové	starosta	k1gMnPc1	starosta
jsou	být	k5eAaImIp3nP	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
republikáni	republikán	k1gMnPc1	republikán
<g/>
,	,	kIx,	,
Charlotte	Charlott	k1gInSc5	Charlott
inklinuje	inklinovat	k5eAaImIp3nS	inklinovat
spíše	spíše	k9	spíše
k	k	k7c3	k
demokratům	demokrat	k1gMnPc3	demokrat
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
voliči	volič	k1gMnPc1	volič
jsou	být	k5eAaImIp3nP	být
nakloněni	naklonit	k5eAaPmNgMnP	naklonit
zástupcům	zástupce	k1gMnPc3	zástupce
obou	dva	k4xCgInPc6	dva
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yInSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
celostátního	celostátní	k2eAgNnSc2d1	celostátní
uspořádání	uspořádání	k1gNnSc2	uspořádání
<g/>
,	,	kIx,	,
Charlotte	Charlott	k1gInSc5	Charlott
je	být	k5eAaImIp3nS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
mezi	mezi	k7c4	mezi
tři	tři	k4xCgInPc4	tři
parlamentní	parlamentní	k2eAgInPc4d1	parlamentní
volební	volební	k2eAgInPc4d1	volební
okresy	okres	k1gInPc4	okres
<g/>
.	.	kIx.	.
</s>
<s>
Osmý	osmý	k4xOgInSc1	osmý
okres	okres	k1gInSc1	okres
je	být	k5eAaImIp3nS	být
zastoupen	zastoupit	k5eAaPmNgInS	zastoupit
republikánem	republikán	k1gMnSc7	republikán
Robinem	Robin	k1gMnSc7	Robin
Hayesem	Hayes	k1gMnSc7	Hayes
<g/>
,	,	kIx,	,
devátý	devátý	k4xOgMnSc1	devátý
Sue	Sue	k1gMnSc1	Sue
Myrickovou	Myricková	k1gFnSc4	Myricková
<g/>
,	,	kIx,	,
taktéž	taktéž	k?	taktéž
členkou	členka	k1gFnSc7	členka
republikánské	republikánský	k2eAgFnSc2d1	republikánská
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
a	a	k8xC	a
dvanáctý	dvanáctý	k4xOgInSc4	dvanáctý
okres	okres	k1gInSc4	okres
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
demokrat	demokrat	k1gMnSc1	demokrat
Mel	mlít	k5eAaImRp2nS	mlít
Watt	watt	k1gInSc4	watt
<g/>
.	.	kIx.	.
</s>
<s>
Charlotte	Charlotte	k5eAaPmIp2nP	Charlotte
bylo	být	k5eAaImAgNnS	být
a	a	k8xC	a
stále	stále	k6eAd1	stále
je	být	k5eAaImIp3nS	být
známé	známý	k2eAgNnSc1d1	známé
jako	jako	k9	jako
město	město	k1gNnSc1	město
kostelů	kostel	k1gInPc2	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
věřících	věřící	k2eAgMnPc2d1	věřící
obyvatelů	obyvatel	k1gMnPc2	obyvatel
jsou	být	k5eAaImIp3nP	být
křesťané	křesťan	k1gMnPc1	křesťan
různých	různý	k2eAgFnPc2d1	různá
protestantských	protestantský	k2eAgFnPc2d1	protestantská
denominací	denominace	k1gFnPc2	denominace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
byly	být	k5eAaImAgInP	být
většinou	většinou	k6eAd1	většinou
nejvýznamnější	významný	k2eAgInPc1d3	nejvýznamnější
presbyteriánské	presbyteriánský	k2eAgInPc1d1	presbyteriánský
kostely	kostel	k1gInPc1	kostel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
změny	změna	k1gFnPc1	změna
ve	v	k7c4	v
složení	složení	k1gNnSc4	složení
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
způsobené	způsobený	k2eAgNnSc1d1	způsobené
rychlým	rychlý	k2eAgInSc7d1	rychlý
růstem	růst	k1gInSc7	růst
<g/>
,	,	kIx,	,
s	s	k7c7	s
sebou	se	k3xPyFc7	se
přináší	přinášet	k5eAaImIp3nS	přinášet
množství	množství	k1gNnSc1	množství
nových	nový	k2eAgFnPc2d1	nová
denominací	denominace	k1gFnPc2	denominace
a	a	k8xC	a
věr	věra	k1gFnPc2	věra
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
se	se	k3xPyFc4	se
v	v	k7c6	v
Charlotte	Charlott	k1gInSc5	Charlott
nachází	nacházet	k5eAaImIp3nS	nacházet
700	[number]	k4	700
sakrálních	sakrální	k2eAgNnPc2d1	sakrální
míst	místo	k1gNnPc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Charlotte	Charlotte	k5eAaPmIp2nP	Charlotte
Area	area	k1gFnSc1	area
Transit	transit	k1gInSc4	transit
System	Syst	k1gMnSc7	Syst
-	-	kIx~	-
CATS	CATS	kA	CATS
(	(	kIx(	(
<g/>
Charlottský	Charlottský	k2eAgInSc1d1	Charlottský
dopravní	dopravní	k2eAgInSc1d1	dopravní
systém	systém	k1gInSc1	systém
<g/>
)	)	kIx)	)
provozuje	provozovat	k5eAaImIp3nS	provozovat
ve	v	k7c6	v
městě	město	k1gNnSc6	město
historické	historický	k2eAgFnSc2d1	historická
tramvaje	tramvaj	k1gFnSc2	tramvaj
<g/>
,	,	kIx,	,
spěšnou	spěšný	k2eAgFnSc4d1	spěšná
kyvadlovou	kyvadlový	k2eAgFnSc4d1	kyvadlová
dopravu	doprava	k1gFnSc4	doprava
a	a	k8xC	a
autobusy	autobus	k1gInPc4	autobus
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jezdí	jezdit	k5eAaImIp3nP	jezdit
po	po	k7c6	po
Charlotte	Charlott	k1gInSc5	Charlott
a	a	k8xC	a
nejbližších	blízký	k2eAgNnPc6d3	nejbližší
předměstích	předměstí	k1gNnPc6	předměstí
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rok	rok	k1gInSc4	rok
2025	[number]	k4	2025
je	být	k5eAaImIp3nS	být
naplánováno	naplánovat	k5eAaBmNgNnS	naplánovat
dokončení	dokončení	k1gNnSc1	dokončení
systému	systém	k1gInSc2	systém
koridorů	koridor	k1gInPc2	koridor
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
doplnit	doplnit	k5eAaPmF	doplnit
zavedenou	zavedený	k2eAgFnSc4d1	zavedená
autobusovou	autobusový	k2eAgFnSc4d1	autobusová
dopravu	doprava	k1gFnSc4	doprava
o	o	k7c6	o
železniční	železniční	k2eAgFnSc6d1	železniční
dvojího	dvojí	k4xRgMnSc2	dvojí
typu	typ	k1gInSc2	typ
<g/>
:	:	kIx,	:
Lynx	Lynx	k1gInSc1	Lynx
(	(	kIx(	(
<g/>
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
vlaky	vlak	k1gInPc4	vlak
obsluhující	obsluhující	k2eAgNnSc4d1	obsluhující
město	město	k1gNnSc4	město
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc6	jeho
předměstí	předměstí	k1gNnSc6	předměstí
-	-	kIx~	-
commuter	commuter	k1gMnSc1	commuter
rail	rail	k1gMnSc1	rail
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeden	jeden	k4xCgInSc1	jeden
typ	typ	k1gInSc1	typ
tramvajové	tramvajový	k2eAgFnSc2d1	tramvajová
dopravy	doprava	k1gFnSc2	doprava
-	-	kIx~	-
light	light	k1gMnSc1	light
rail	rail	k1gMnSc1	rail
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
systém	systém	k1gInSc1	systém
bude	být	k5eAaImBp3nS	být
dopravovat	dopravovat	k5eAaImF	dopravovat
cestující	cestující	k1gMnPc4	cestující
v	v	k7c6	v
5	[number]	k4	5
klíčových	klíčový	k2eAgInPc6d1	klíčový
koridorech	koridor	k1gInPc6	koridor
<g/>
.	.	kIx.	.
</s>
<s>
Odhadovaná	odhadovaný	k2eAgFnSc1d1	odhadovaná
cena	cena	k1gFnSc1	cena
tohoto	tento	k3xDgInSc2	tento
projektu	projekt	k1gInSc2	projekt
byla	být	k5eAaImAgFnS	být
$	$	kIx~	$
<g/>
1,7	[number]	k4	1,7
miliard	miliarda	k4xCgFnPc2	miliarda
<g/>
,	,	kIx,	,
nedávno	nedávno	k6eAd1	nedávno
však	však	k9	však
byla	být	k5eAaImAgFnS	být
přehodnocena	přehodnotit	k5eAaPmNgFnS	přehodnotit
na	na	k7c6	na
$	$	kIx~	$
<g/>
6	[number]	k4	6
miliard	miliarda	k4xCgFnPc2	miliarda
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
letiště	letiště	k1gNnSc1	letiště
Charlotte	Charlott	k1gInSc5	Charlott
<g/>
/	/	kIx~	/
<g/>
Douglas	Douglas	k1gInSc1	Douglas
je	být	k5eAaImIp3nS	být
13	[number]	k4	13
<g/>
.	.	kIx.	.
nejrušnější	rušný	k2eAgFnSc2d3	nejrušnější
na	na	k7c6	na
světě	svět	k1gInSc6	svět
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
pohybu	pohyb	k1gInSc2	pohyb
dopravy	doprava	k1gFnSc2	doprava
a	a	k8xC	a
24	[number]	k4	24
<g/>
.	.	kIx.	.
<g/>
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
podle	podle	k7c2	podle
počtu	počet	k1gInSc2	počet
přepravených	přepravený	k2eAgFnPc2d1	přepravená
cestujících	cestující	k1gFnPc2	cestující
(	(	kIx(	(
<g/>
http://www.aci.aero/cda/aci_common/display/main/aci_content07_c.jsp?zn=aci&	[url]	k?	http://www.aci.aero/cda/aci_common/display/main/aci_content07_c.jsp?zn=aci&
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Letiště	letiště	k1gNnSc1	letiště
je	být	k5eAaImIp3nS	být
využíváno	využívat	k5eAaImNgNnS	využívat
mnoha	mnoho	k4c7	mnoho
mezinárodními	mezinárodní	k2eAgInPc7d1	mezinárodní
i	i	k8xC	i
domácími	domácí	k2eAgFnPc7d1	domácí
leteckými	letecký	k2eAgFnPc7d1	letecká
společnostmi	společnost	k1gFnPc7	společnost
(	(	kIx(	(
<g/>
např.	např.	kA	např.
American	American	k1gMnSc1	American
Airlines	Airlines	k1gMnSc1	Airlines
<g/>
,	,	kIx,	,
Air	Air	k1gMnSc1	Air
Canada	Canada	k1gFnSc1	Canada
<g/>
,	,	kIx,	,
Continental	Continental	k1gMnSc1	Continental
<g/>
,	,	kIx,	,
Delta	delta	k1gFnSc1	delta
<g/>
,	,	kIx,	,
Northwest	Northwest	k1gFnSc1	Northwest
<g/>
,	,	kIx,	,
United	United	k1gInSc1	United
<g/>
,	,	kIx,	,
AirTran	AirTran	k1gInSc1	AirTran
Airways	Airways	k1gInSc1	Airways
<g/>
,	,	kIx,	,
JetBlue	JetBlue	k1gFnSc1	JetBlue
a	a	k8xC	a
Lufthansa	Lufthansa	k1gFnSc1	Lufthansa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
US	US	kA	US
Airways	Airways	k1gInSc4	Airways
je	být	k5eAaImIp3nS	být
Charlotte	Charlott	k1gInSc5	Charlott
<g/>
/	/	kIx~	/
<g/>
Douglas	Douglas	k1gInSc1	Douglas
nejvyužívanějším	využívaný	k2eAgNnSc7d3	nejvyužívanější
letištěm	letiště	k1gNnSc7	letiště
<g/>
.	.	kIx.	.
</s>
<s>
Charlotte	Charlotte	k5eAaPmIp2nP	Charlotte
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
mezi	mezi	k7c7	mezi
severovýchodním	severovýchodní	k2eAgInSc7d1	severovýchodní
a	a	k8xC	a
jihovýchodním	jihovýchodní	k2eAgInSc7d1	jihovýchodní
populačními	populační	k2eAgFnPc7d1	populační
centrem	centrum	k1gNnSc7	centrum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
toho	ten	k3xDgMnSc4	ten
je	být	k5eAaImIp3nS	být
důležitým	důležitý	k2eAgInSc7d1	důležitý
dopravním	dopravní	k2eAgInSc7d1	dopravní
uzlem	uzel	k1gInSc7	uzel
a	a	k8xC	a
hlavním	hlavní	k2eAgNnSc7d1	hlavní
distribučním	distribuční	k2eAgNnSc7d1	distribuční
centrem	centrum	k1gNnSc7	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Charlotte	Charlotte	k5eAaPmIp2nP	Charlotte
křižují	křižovat	k5eAaImIp3nP	křižovat
dvě	dva	k4xCgFnPc1	dva
hlavní	hlavní	k2eAgFnPc1d1	hlavní
mezistátní	mezistátní	k2eAgFnPc1d1	mezistátní
silnice	silnice	k1gFnPc1	silnice
I-85	I-85	k1gFnSc1	I-85
a	a	k8xC	a
I-	I-	k1gFnSc1	I-
<g/>
77	[number]	k4	77
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
setkávají	setkávat	k5eAaImIp3nP	setkávat
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
centra	centrum	k1gNnSc2	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Okružní	okružní	k2eAgFnSc1d1	okružní
silnice	silnice	k1gFnSc1	silnice
<g/>
,	,	kIx,	,
I-	I-	k1gFnSc1	I-
<g/>
485	[number]	k4	485
<g/>
,	,	kIx,	,
přezdívaná	přezdívaný	k2eAgFnSc1d1	přezdívaná
Outerbelt	Outerbelt	k1gInSc1	Outerbelt
(	(	kIx(	(
<g/>
vnější	vnější	k2eAgInSc1d1	vnější
pás	pás	k1gInSc1	pás
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
částečně	částečně	k6eAd1	částečně
hotová	hotový	k2eAgFnSc1d1	hotová
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
momentálně	momentálně	k6eAd1	momentálně
je	být	k5eAaImIp3nS	být
její	její	k3xOp3gFnSc1	její
stavba	stavba	k1gFnSc1	stavba
přerušena	přerušen	k2eAgFnSc1d1	přerušena
kvůli	kvůli	k7c3	kvůli
financování	financování	k1gNnSc3	financování
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
projekt	projekt	k1gInSc1	projekt
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
plánu	plán	k1gInSc6	plán
tuto	tento	k3xDgFnSc4	tento
silnici	silnice	k1gFnSc4	silnice
dokončit	dokončit	k5eAaPmF	dokončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
bude	být	k5eAaImBp3nS	být
Outerbelt	Outerbelt	k1gInSc1	Outerbelt
po	po	k7c6	po
obvodu	obvod	k1gInSc6	obvod
měřit	měřit	k5eAaImF	měřit
přibližně	přibližně	k6eAd1	přibližně
108	[number]	k4	108
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
vede	vést	k5eAaImIp3nS	vést
dálnice	dálnice	k1gFnSc1	dálnice
I-277	I-277	k1gFnSc1	I-277
(	(	kIx(	(
<g/>
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
smyčky	smyčka	k1gFnSc2	smyčka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnPc1	její
dvě	dva	k4xCgFnPc1	dva
části	část	k1gFnPc1	část
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
dálnice	dálnice	k1gFnSc1	dálnice
John	John	k1gMnSc1	John
Belk	Belk	k1gMnSc1	Belk
a	a	k8xC	a
Brookshire	Brookshir	k1gMnSc5	Brookshir
<g/>
.	.	kIx.	.
</s>
<s>
Charlotte	Charlott	k1gMnSc5	Charlott
Route	Rout	k1gMnSc5	Rout
4	[number]	k4	4
spojuje	spojovat	k5eAaImIp3nS	spojovat
důležité	důležitý	k2eAgFnPc4d1	důležitá
silnice	silnice	k1gFnPc4	silnice
ve	v	k7c6	v
smyčce	smyčka	k1gFnSc6	smyčka
mezi	mezi	k7c7	mezi
I-277	I-277	k1gFnSc7	I-277
a	a	k8xC	a
I-	I-	k1gFnSc7	I-
<g/>
458	[number]	k4	458
<g/>
.	.	kIx.	.
</s>
<s>
Amtrak	Amtrak	k1gInSc1	Amtrak
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Crescent	Crescent	k1gInSc1	Crescent
a	a	k8xC	a
vlaky	vlak	k1gInPc1	vlak
Carolinian	Caroliniany	k1gInPc2	Caroliniany
a	a	k8xC	a
Piedmont	Piedmonta	k1gFnPc2	Piedmonta
spojují	spojovat	k5eAaImIp3nP	spojovat
Charlotte	Charlott	k1gInSc5	Charlott
na	na	k7c6	na
severu	sever	k1gInSc6	sever
s	s	k7c7	s
New	New	k1gMnSc7	New
Yorkem	York	k1gInSc7	York
<g/>
,	,	kIx,	,
Philadelphií	Philadelphia	k1gFnSc7	Philadelphia
<g/>
,	,	kIx,	,
Baltomorem	Baltomor	k1gInSc7	Baltomor
<g/>
,	,	kIx,	,
Washingtonem	Washington	k1gInSc7	Washington
<g/>
,	,	kIx,	,
Richmondem	Richmond	k1gInSc7	Richmond
a	a	k8xC	a
Raleigh	Raleigh	k1gInSc4	Raleigh
a	a	k8xC	a
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
s	s	k7c7	s
Atlantou	Atlanta	k1gFnSc7	Atlanta
<g/>
,	,	kIx,	,
Birminghamem	Birmingham	k1gInSc7	Birmingham
a	a	k8xC	a
New	New	k1gFnSc1	New
Orleans	Orleans	k1gInSc1	Orleans
<g/>
.	.	kIx.	.
</s>
<s>
Železniční	železniční	k2eAgFnSc1d1	železniční
stanice	stanice	k1gFnSc1	stanice
Amtrak	Amtrak	k1gInSc1	Amtrak
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
adrese	adresa	k1gFnSc6	adresa
North	North	k1gMnSc1	North
Tryon	Tryon	k1gMnSc1	Tryon
1914	[number]	k4	1914
<g/>
.	.	kIx.	.
</s>
<s>
Arequipa	Arequipa	k1gFnSc1	Arequipa
<g/>
,	,	kIx,	,
Peru	Peru	k1gNnSc1	Peru
<g/>
,	,	kIx,	,
1962	[number]	k4	1962
Krefeld	Krefelda	k1gFnPc2	Krefelda
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
Limoges	Limogesa	k1gFnPc2	Limogesa
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
Pao-ting	Paoing	k1gInSc1	Pao-ting
<g/>
,	,	kIx,	,
Čína	Čína	k1gFnSc1	Čína
<g/>
,	,	kIx,	,
1987	[number]	k4	1987
Voroněž	Voroněž	k1gFnSc1	Voroněž
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
Vratislav	Vratislava	k1gFnPc2	Vratislava
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
Kumasi	Kumas	k1gMnPc1	Kumas
<g/>
,	,	kIx,	,
Ghana	Ghana	k1gFnSc1	Ghana
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Charlotte	Charlott	k1gInSc5	Charlott
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
internetové	internetový	k2eAgFnPc1d1	internetová
stránky	stránka	k1gFnPc1	stránka
</s>
