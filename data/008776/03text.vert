<p>
<s>
Český	český	k2eAgInSc1d1	český
Krumlov	Krumlov	k1gInSc1	Krumlov
(	(	kIx(	(
[	[	kIx(	[
<g/>
ˈ	ˈ	k?	ˈ
ˈ	ˈ	k?	ˈ
<g/>
]	]	kIx)	]
IPA	IPA	kA	IPA
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Böhmisch	Böhmisch	k1gMnSc1	Böhmisch
Krumau	Krumaus	k1gInSc2	Krumaus
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
Krummau	Krummaus	k1gInSc3	Krummaus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
v	v	k7c6	v
Jihočeském	jihočeský	k2eAgInSc6d1	jihočeský
kraji	kraj	k1gInSc6	kraj
<g/>
,	,	kIx,	,
22	[number]	k4	22
km	km	kA	km
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
Českých	český	k2eAgInPc2d1	český
Budějovic	Budějovice	k1gInPc2	Budějovice
<g/>
.	.	kIx.	.
</s>
<s>
Rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
hřebenem	hřeben	k1gInSc7	hřeben
Blanského	blanský	k2eAgInSc2d1	blanský
lesa	les	k1gInSc2	les
a	a	k8xC	a
protéká	protékat	k5eAaImIp3nS	protékat
jím	on	k3xPp3gInSc7	on
řeka	řeka	k1gFnSc1	řeka
Vltava	Vltava	k1gFnSc1	Vltava
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
turistické	turistický	k2eAgNnSc4d1	turistické
a	a	k8xC	a
kulturní	kulturní	k2eAgNnSc4d1	kulturní
centrum	centrum	k1gNnSc4	centrum
jižních	jižní	k2eAgFnPc2d1	jižní
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
přibližně	přibližně	k6eAd1	přibližně
13	[number]	k4	13
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
byl	být	k5eAaImAgInS	být
Český	český	k2eAgInSc1d1	český
Krumlov	Krumlov	k1gInSc1	Krumlov
sídelním	sídelní	k2eAgNnSc7d1	sídelní
městem	město	k1gNnSc7	město
mnoha	mnoho	k4c2	mnoho
mocných	mocný	k2eAgInPc2d1	mocný
českých	český	k2eAgInPc2d1	český
rodů	rod	k1gInPc2	rod
–	–	k?	–
Vítkovců	Vítkovec	k1gMnPc2	Vítkovec
<g/>
,	,	kIx,	,
pánů	pan	k1gMnPc2	pan
z	z	k7c2	z
Krumlova	Krumlov	k1gInSc2	Krumlov
<g/>
,	,	kIx,	,
Rožmberků	Rožmberk	k1gInPc2	Rožmberk
<g/>
,	,	kIx,	,
Eggenbergů	Eggenberg	k1gInPc2	Eggenberg
a	a	k8xC	a
Schwarzenbergů	Schwarzenberg	k1gInPc2	Schwarzenberg
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
cíleně	cíleně	k6eAd1	cíleně
pečovaly	pečovat	k5eAaImAgInP	pečovat
o	o	k7c4	o
jeho	jeho	k3xOp3gFnSc4	jeho
výstavbu	výstavba	k1gFnSc4	výstavba
a	a	k8xC	a
reprezentativní	reprezentativní	k2eAgInSc4d1	reprezentativní
charakter	charakter	k1gInSc4	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
Krumlov	Krumlov	k1gInSc1	Krumlov
byl	být	k5eAaImAgInS	být
zároveň	zároveň	k6eAd1	zároveň
hlavním	hlavní	k2eAgNnSc7d1	hlavní
správním	správní	k2eAgNnSc7d1	správní
centrem	centrum	k1gNnSc7	centrum
Rožmberského	rožmberský	k2eAgNnSc2d1	rožmberské
dominia	dominion	k1gNnSc2	dominion
a	a	k8xC	a
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
i	i	k9	i
Krumlovského	krumlovský	k2eAgNnSc2d1	krumlovské
vévodství	vévodství	k1gNnSc2	vévodství
a	a	k8xC	a
Dominia	dominion	k1gNnSc2	dominion
schwarzenberského	schwarzenberský	k2eAgNnSc2d1	Schwarzenberské
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Středověké	středověký	k2eAgNnSc1d1	středověké
centrum	centrum	k1gNnSc1	centrum
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
obklopuje	obklopovat	k5eAaImIp3nS	obklopovat
meandry	meandr	k1gInPc4	meandr
Vltavy	Vltava	k1gFnSc2	Vltava
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
městskou	městský	k2eAgFnSc7d1	městská
památkovou	památkový	k2eAgFnSc7d1	památková
rezervací	rezervace	k1gFnSc7	rezervace
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
je	být	k5eAaImIp3nS	být
zapsáno	zapsat	k5eAaPmNgNnS	zapsat
na	na	k7c6	na
seznamu	seznam	k1gInSc6	seznam
světového	světový	k2eAgNnSc2d1	světové
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
bylo	být	k5eAaImAgNnS	být
městskou	městský	k2eAgFnSc7d1	městská
památkovou	památkový	k2eAgFnSc7d1	památková
zónou	zóna	k1gFnSc7	zóna
vyhlášeno	vyhlášen	k2eAgNnSc4d1	vyhlášeno
předměstí	předměstí	k1gNnSc4	předměstí
Plešivec	plešivec	k1gMnSc1	plešivec
(	(	kIx(	(
<g/>
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
historického	historický	k2eAgNnSc2d1	historické
jádra	jádro	k1gNnSc2	jádro
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
Krumlov	Krumlov	k1gInSc1	Krumlov
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
také	také	k6eAd1	také
dějištěm	dějiště	k1gNnSc7	dějiště
několika	několik	k4yIc2	několik
filmů	film	k1gInPc2	film
a	a	k8xC	a
během	během	k7c2	během
roku	rok	k1gInSc2	rok
město	město	k1gNnSc1	město
hostí	hostit	k5eAaImIp3nS	hostit
řadu	řada	k1gFnSc4	řada
kulturních	kulturní	k2eAgFnPc2d1	kulturní
akcí	akce	k1gFnPc2	akce
a	a	k8xC	a
festivalů	festival	k1gInPc2	festival
–	–	k?	–
Slavnosti	slavnost	k1gFnSc3	slavnost
pětilisté	pětilistý	k2eAgFnSc2d1	pětilistá
růže	růž	k1gFnSc2	růž
<g/>
,	,	kIx,	,
MHF	MHF	kA	MHF
Český	český	k2eAgInSc1d1	český
Krumlov	Krumlov	k1gInSc1	Krumlov
<g/>
,	,	kIx,	,
Rallye	rallye	k1gFnSc7	rallye
Český	český	k2eAgInSc1d1	český
Krumlov	Krumlov	k1gInSc1	Krumlov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgNnPc7d1	další
městy	město	k1gNnPc7	město
Šumavského	šumavský	k2eAgNnSc2d1	Šumavské
podhůří	podhůří	k1gNnSc2	podhůří
jako	jako	k8xS	jako
Sušice	Sušice	k1gFnSc1	Sušice
<g/>
,	,	kIx,	,
Vimperk	Vimperk	k1gInSc1	Vimperk
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Prachatice	Prachatice	k1gFnPc1	Prachatice
bývá	bývat	k5eAaImIp3nS	bývat
nazýváno	nazýván	k2eAgNnSc1d1	nazýváno
Branou	brána	k1gFnSc7	brána
Šumavy	Šumava	k1gFnSc2	Šumava
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
Český	český	k2eAgInSc4d1	český
Krumlov	Krumlov	k1gInSc4	Krumlov
zvláště	zvláště	k6eAd1	zvláště
mezi	mezi	k7c7	mezi
šumavskými	šumavský	k2eAgMnPc7d1	šumavský
spisovateli	spisovatel	k1gMnPc7	spisovatel
a	a	k8xC	a
básníky	básník	k1gMnPc7	básník
vnímán	vnímat	k5eAaImNgInS	vnímat
jako	jako	k8xC	jako
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Šumavy	Šumava	k1gFnSc2	Šumava
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
Český	český	k2eAgInSc1d1	český
Krumlov	Krumlov	k1gInSc1	Krumlov
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
existence	existence	k1gFnSc2	existence
Šumavské	šumavský	k2eAgFnSc2d1	Šumavská
župy	župa	k1gFnSc2	župa
i	i	k9	i
nakrátko	nakrátko	k6eAd1	nakrátko
stal	stát	k5eAaPmAgInS	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Název	název	k1gInSc1	název
města	město	k1gNnSc2	město
==	==	k?	==
</s>
</p>
<p>
<s>
Jméno	jméno	k1gNnSc1	jméno
Krumlov	Krumlov	k1gInSc1	Krumlov
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
německého	německý	k2eAgMnSc2d1	německý
Krumme	Krumme	k1gMnSc2	Krumme
Aue	Aue	k1gMnSc2	Aue
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
křivý	křivý	k2eAgInSc1d1	křivý
luh	luh	k1gInSc1	luh
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
polohu	poloha	k1gFnSc4	poloha
města	město	k1gNnSc2	město
mezi	mezi	k7c7	mezi
esovitými	esovitý	k2eAgInPc7d1	esovitý
zákruty	zákrut	k1gInPc7	zákrut
řeky	řeka	k1gFnSc2	řeka
Vltavy	Vltava	k1gFnSc2	Vltava
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
podobného	podobný	k2eAgInSc2d1	podobný
keltského	keltský	k2eAgInSc2d1	keltský
základu	základ	k1gInSc2	základ
kamb	kamb	k1gInSc1	kamb
je	být	k5eAaImIp3nS	být
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
odvozen	odvozen	k2eAgInSc4d1	odvozen
i	i	k8xC	i
název	název	k1gInSc4	název
rakouské	rakouský	k2eAgFnSc2d1	rakouská
řeky	řeka	k1gFnSc2	řeka
Kamp	Kampa	k1gFnPc2	Kampa
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yQgFnPc4	který
leží	ležet	k5eAaImIp3nS	ležet
město	město	k1gNnSc1	město
Krumau	Krumaus	k1gInSc2	Krumaus
am	am	k?	am
Kamp	kamp	k1gInSc1	kamp
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
název	název	k1gInSc1	název
taktéž	taktéž	k?	taktéž
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
obdobného	obdobný	k2eAgInSc2d1	obdobný
starohornoněmeckého	starohornoněmecký	k2eAgInSc2d1	starohornoněmecký
krump	krumpit	k5eAaPmRp2nS	krumpit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
dochované	dochovaný	k2eAgFnSc6d1	dochovaná
zmínce	zmínka	k1gFnSc6	zmínka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1253	[number]	k4	1253
je	být	k5eAaImIp3nS	být
Krumlov	Krumlov	k1gInSc1	Krumlov
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k8xC	jako
Chrumbenowe	Chrumbenowe	k1gInSc1	Chrumbenowe
<g/>
,	,	kIx,	,
na	na	k7c6	na
latinských	latinský	k2eAgFnPc6d1	Latinská
listinách	listina	k1gFnPc6	listina
pak	pak	k6eAd1	pak
nese	nést	k5eAaImIp3nS	nést
jméno	jméno	k1gNnSc4	jméno
Crumlovia	Crumlovium	k1gNnSc2	Crumlovium
nebo	nebo	k8xC	nebo
Crumlovium	Crumlovium	k1gNnSc4	Crumlovium
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
býval	bývat	k5eAaImAgInS	bývat
používán	používat	k5eAaImNgInS	používat
název	název	k1gInSc1	název
Krumlov	Krumlov	k1gInSc1	Krumlov
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
<g/>
.	.	kIx.	.
</s>
<s>
Přídomek	přídomek	k1gInSc1	přídomek
Český	český	k2eAgInSc1d1	český
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
ke	k	k7c3	k
jménu	jméno	k1gNnSc3	jméno
města	město	k1gNnSc2	město
připojovat	připojovat	k5eAaImF	připojovat
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1439	[number]	k4	1439
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
jasně	jasně	k6eAd1	jasně
odlišil	odlišit	k5eAaPmAgInS	odlišit
od	od	k7c2	od
Moravského	moravský	k2eAgInSc2d1	moravský
Krumlova	Krumlov	k1gInSc2	Krumlov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
německém	německý	k2eAgInSc6d1	německý
jazyce	jazyk	k1gInSc6	jazyk
se	se	k3xPyFc4	se
název	název	k1gInSc1	název
Böhmisch	Böhmischa	k1gFnPc2	Böhmischa
Krumau	Krumaus	k1gInSc2	Krumaus
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
příliš	příliš	k6eAd1	příliš
nepoužíval	používat	k5eNaImAgMnS	používat
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
nemohlo	moct	k5eNaImAgNnS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
záměně	záměna	k1gFnSc3	záměna
s	s	k7c7	s
Krumlovem	Krumlov	k1gInSc7	Krumlov
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Kromau	Kromaus	k1gInSc3	Kromaus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oficiálně	oficiálně	k6eAd1	oficiálně
se	se	k3xPyFc4	se
však	však	k9	však
název	název	k1gInSc1	název
Český	český	k2eAgInSc1d1	český
Krumlov	Krumlov	k1gInSc1	Krumlov
používá	používat	k5eAaImIp3nS	používat
až	až	k9	až
výnosem	výnos	k1gInSc7	výnos
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
z	z	k7c2	z
30	[number]	k4	30
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1920	[number]	k4	1920
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
německé	německý	k2eAgFnSc2d1	německá
okupace	okupace	k1gFnSc2	okupace
v	v	k7c6	v
letech	let	k1gInPc6	let
1938	[number]	k4	1938
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
bylo	být	k5eAaImAgNnS	být
slovo	slovo	k1gNnSc1	slovo
Český	český	k2eAgInSc1d1	český
z	z	k7c2	z
názvu	název	k1gInSc2	název
odstraněno	odstraněn	k2eAgNnSc1d1	odstraněno
a	a	k8xC	a
město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
po	po	k7c4	po
onu	onen	k3xDgFnSc4	onen
dobu	doba	k1gFnSc4	doba
oficiálně	oficiálně	k6eAd1	oficiálně
jmenovalo	jmenovat	k5eAaImAgNnS	jmenovat
Krummau	Krummaa	k1gMnSc4	Krummaa
an	an	k?	an
der	drát	k5eAaImRp2nS	drát
Moldau	Moldaus	k1gInSc3	Moldaus
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Krumlov	Krumlov	k1gInSc1	Krumlov
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
Již	již	k6eAd1	již
od	od	k7c2	od
založení	založení	k1gNnSc2	založení
města	město	k1gNnSc2	město
vedle	vedle	k7c2	vedle
sebe	sebe	k3xPyFc4	sebe
žili	žít	k5eAaImAgMnP	žít
čeští	český	k2eAgMnPc1d1	český
i	i	k8xC	i
němečtí	německý	k2eAgMnPc1d1	německý
obyvatelé	obyvatel	k1gMnPc1	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
hornictví	hornictví	k1gNnSc2	hornictví
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
však	však	k9	však
postupně	postupně	k6eAd1	postupně
začala	začít	k5eAaPmAgFnS	začít
ve	v	k7c6	v
městě	město	k1gNnSc6	město
převažovat	převažovat	k5eAaImF	převažovat
němčina	němčina	k1gFnSc1	němčina
<g/>
,	,	kIx,	,
pročež	pročež	k6eAd1	pročež
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
mluvilo	mluvit	k5eAaImAgNnS	mluvit
ve	v	k7c6	v
městě	město	k1gNnSc6	město
z	z	k7c2	z
8	[number]	k4	8
300	[number]	k4	300
obyvatel	obyvatel	k1gMnPc2	obyvatel
72	[number]	k4	72
%	%	kIx~	%
německy	německy	k6eAd1	německy
a	a	k8xC	a
18	[number]	k4	18
%	%	kIx~	%
česky	česky	k6eAd1	česky
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
však	však	k9	však
po	po	k7c6	po
700	[number]	k4	700
letech	léto	k1gNnPc6	léto
někdy	někdy	k6eAd1	někdy
divoké	divoký	k2eAgMnPc4d1	divoký
<g/>
,	,	kIx,	,
jindy	jindy	k6eAd1	jindy
mírové	mírový	k2eAgNnSc4d1	Mírové
soužití	soužití	k1gNnSc4	soužití
Čechů	Čech	k1gMnPc2	Čech
a	a	k8xC	a
Němců	Němec	k1gMnPc2	Němec
v	v	k7c6	v
Českém	český	k2eAgInSc6d1	český
Krumlově	Krumlov	k1gInSc6	Krumlov
ukončila	ukončit	k5eAaPmAgFnS	ukončit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
musela	muset	k5eAaImAgFnS	muset
město	město	k1gNnSc4	město
opustit	opustit	k5eAaPmF	opustit
většina	většina	k1gFnSc1	většina
českého	český	k2eAgNnSc2d1	české
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
naopak	naopak	k6eAd1	naopak
naprostá	naprostý	k2eAgFnSc1d1	naprostá
většina	většina	k1gFnSc1	většina
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
německého	německý	k2eAgNnSc2d1	německé
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
24	[number]	k4	24
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2007	[number]	k4	2007
ze	z	k7c2	z
14	[number]	k4	14
056	[number]	k4	056
obyvatel	obyvatel	k1gMnPc2	obyvatel
bylo	být	k5eAaImAgNnS	být
14,7	[number]	k4	14,7
%	%	kIx~	%
mladších	mladý	k2eAgNnPc2d2	mladší
15	[number]	k4	15
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
údajů	údaj	k1gInPc2	údaj
ze	z	k7c2	z
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
bylo	být	k5eAaImAgNnS	být
26	[number]	k4	26
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
města	město	k1gNnSc2	město
věřících	věřící	k1gMnPc2	věřící
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
93	[number]	k4	93
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
přihlásilo	přihlásit	k5eAaPmAgNnS	přihlásit
k	k	k7c3	k
národnosti	národnost	k1gFnSc3	národnost
české	český	k2eAgFnSc3d1	Česká
<g/>
,	,	kIx,	,
2	[number]	k4	2
%	%	kIx~	%
k	k	k7c3	k
národnosti	národnost	k1gFnSc3	národnost
slovenské	slovenský	k2eAgFnSc3d1	slovenská
<g/>
,	,	kIx,	,
1	[number]	k4	1
%	%	kIx~	%
k	k	k7c3	k
národnosti	národnost	k1gFnSc3	národnost
německé	německý	k2eAgFnSc3d1	německá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Ekonomika	ekonomika	k1gFnSc1	ekonomika
==	==	k?	==
</s>
</p>
<p>
<s>
Transformace	transformace	k1gFnSc1	transformace
hospodářství	hospodářství	k1gNnSc2	hospodářství
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
znamenala	znamenat	k5eAaImAgFnS	znamenat
zánik	zánik	k1gInSc4	zánik
některých	některý	k3yIgMnPc2	některý
krumlovských	krumlovský	k2eAgMnPc2d1	krumlovský
závodních	závodní	k2eAgMnPc2d1	závodní
podniků	podnik	k1gInPc2	podnik
<g/>
,	,	kIx,	,
např.	např.	kA	např.
oděvního	oděvní	k2eAgInSc2d1	oděvní
závodu	závod	k1gInSc2	závod
OTAVAN	Otavan	k1gInSc1	Otavan
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
však	však	k9	však
díky	díky	k7c3	díky
turistickému	turistický	k2eAgInSc3d1	turistický
ruchu	ruch	k1gInSc2	ruch
vznikla	vzniknout	k5eAaPmAgNnP	vzniknout
mnohá	mnohý	k2eAgNnPc1d1	mnohé
pracovní	pracovní	k2eAgNnPc1d1	pracovní
místa	místo	k1gNnPc1	místo
ve	v	k7c6	v
službách	služba	k1gFnPc6	služba
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c4	v
ubytování	ubytování	k1gNnSc4	ubytování
a	a	k8xC	a
pohostinství	pohostinství	k1gNnSc4	pohostinství
<g/>
.	.	kIx.	.
</s>
<s>
Turistický	turistický	k2eAgInSc1d1	turistický
ruch	ruch	k1gInSc1	ruch
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
základem	základ	k1gInSc7	základ
místní	místní	k2eAgFnSc2d1	místní
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
Zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
se	se	k3xPyFc4	se
na	na	k7c4	na
české	český	k2eAgMnPc4d1	český
i	i	k8xC	i
zahraniční	zahraniční	k2eAgMnPc4d1	zahraniční
návštěvníky	návštěvník	k1gMnPc4	návštěvník
<g/>
,	,	kIx,	,
účastníky	účastník	k1gMnPc4	účastník
místních	místní	k2eAgInPc2d1	místní
festivalů	festival	k1gInPc2	festival
a	a	k8xC	a
také	také	k9	také
vodáky	vodák	k1gMnPc4	vodák
na	na	k7c6	na
Vltavě	Vltava	k1gFnSc6	Vltava
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
turistická	turistický	k2eAgFnSc1d1	turistická
sezóna	sezóna	k1gFnSc1	sezóna
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
letních	letní	k2eAgInPc6d1	letní
měsících	měsíc	k1gInPc6	měsíc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
na	na	k7c6	na
předměstí	předměstí	k1gNnSc6	předměstí
Domoradic	Domoradice	k1gFnPc2	Domoradice
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
založena	založit	k5eAaPmNgFnS	založit
nová	nový	k2eAgFnSc1d1	nová
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
zóna	zóna	k1gFnSc1	zóna
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
potravinářských	potravinářský	k2eAgInPc2d1	potravinářský
provozů	provoz	k1gInPc2	provoz
fungují	fungovat	k5eAaImIp3nP	fungovat
ve	v	k7c6	v
městě	město	k1gNnSc6	město
pekárny	pekárna	k1gFnSc2	pekárna
<g/>
,	,	kIx,	,
pobočka	pobočka	k1gFnSc1	pobočka
jihočeských	jihočeský	k2eAgFnPc2d1	Jihočeská
mlékáren	mlékárna	k1gFnPc2	mlékárna
Madeta	Madeta	k1gFnSc1	Madeta
a	a	k8xC	a
místní	místní	k2eAgInSc1d1	místní
pivovar	pivovar	k1gInSc1	pivovar
Eggenberg	Eggenberg	k1gInSc1	Eggenberg
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
ve	v	k7c6	v
městě	město	k1gNnSc6	město
k	k	k7c3	k
30	[number]	k4	30
<g/>
.	.	kIx.	.
dubnu	duben	k1gInSc6	duben
2018	[number]	k4	2018
dosahovala	dosahovat	k5eAaImAgFnS	dosahovat
3,61	[number]	k4	3,61
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Doprava	doprava	k1gFnSc1	doprava
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Silniční	silniční	k2eAgInSc1d1	silniční
===	===	k?	===
</s>
</p>
<p>
<s>
Městem	město	k1gNnSc7	město
prochází	procházet	k5eAaImIp3nS	procházet
silnice	silnice	k1gFnSc1	silnice
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
39	[number]	k4	39
a	a	k8xC	a
vede	vést	k5eAaImIp3nS	vést
z	z	k7c2	z
Lenory	Lenora	k1gFnSc2	Lenora
přes	přes	k7c4	přes
Volary	Volara	k1gFnPc4	Volara
kolem	kolem	k7c2	kolem
nádrže	nádrž	k1gFnSc2	nádrž
Lipno	Lipno	k1gNnSc1	Lipno
<g/>
,	,	kIx,	,
Českým	český	k2eAgInSc7d1	český
Krumlovem	Krumlov	k1gInSc7	Krumlov
prochází	procházet	k5eAaImIp3nS	procházet
ulicemi	ulice	k1gFnPc7	ulice
Chvalšinská	Chvalšinský	k2eAgFnSc1d1	Chvalšinská
a	a	k8xC	a
Budějovická	budějovický	k2eAgFnSc1d1	Budějovická
a	a	k8xC	a
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
České	český	k2eAgInPc4d1	český
Budějovice	Budějovice	k1gInPc4	Budějovice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dále	daleko	k6eAd2	daleko
zde	zde	k6eAd1	zde
prochází	procházet	k5eAaImIp3nS	procházet
silnice	silnice	k1gFnSc1	silnice
II	II	kA	II
<g/>
.	.	kIx.	.
třídy	třída	k1gFnSc2	třída
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Silnice	silnice	k1gFnSc1	silnice
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
157	[number]	k4	157
vede	vést	k5eAaImIp3nS	vést
z	z	k7c2	z
Českého	český	k2eAgInSc2d1	český
Krumlova	Krumlov	k1gInSc2	Krumlov
přes	přes	k7c4	přes
Kaplice	Kaplice	k1gFnPc4	Kaplice
(	(	kIx(	(
<g/>
nádraží	nádraží	k1gNnSc4	nádraží
<g/>
)	)	kIx)	)
a	a	k8xC	a
Trhové	trhový	k2eAgInPc1d1	trhový
Sviny	Svin	k1gInPc1	Svin
oklikou	oklika	k1gFnSc7	oklika
do	do	k7c2	do
Českých	český	k2eAgInPc2d1	český
Budějovic	Budějovice	k1gInPc2	Budějovice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Silnice	silnice	k1gFnSc1	silnice
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
160	[number]	k4	160
spojuje	spojovat	k5eAaImIp3nS	spojovat
Český	český	k2eAgInSc1d1	český
Krumlov	Krumlov	k1gInSc1	Krumlov
s	s	k7c7	s
obcemi	obec	k1gFnPc7	obec
Větřní	Větřní	k2eAgFnSc7d1	Větřní
<g/>
,	,	kIx,	,
Rožmberk	Rožmberk	k1gInSc1	Rožmberk
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
a	a	k8xC	a
Horní	horní	k2eAgNnSc1d1	horní
Dvořiště	dvořiště	k1gNnSc1	dvořiště
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
za	za	k7c4	za
Rožmberk	Rožmberk	k1gInSc4	Rožmberk
vede	vést	k5eAaImIp3nS	vést
podél	podél	k7c2	podél
řeky	řeka	k1gFnSc2	řeka
Vltavy	Vltava	k1gFnSc2	Vltava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Silnice	silnice	k1gFnSc1	silnice
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
166	[number]	k4	166
vede	vést	k5eAaImIp3nS	vést
z	z	k7c2	z
Krumlova	Krumlov	k1gInSc2	Krumlov
severozápadním	severozápadní	k2eAgInSc7d1	severozápadní
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
Chvalšiny	Chvalšina	k1gFnPc4	Chvalšina
a	a	k8xC	a
Smědeč	Smědeč	k1gInSc4	Smědeč
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
lze	lze	k6eAd1	lze
po	po	k7c6	po
jiných	jiný	k2eAgFnPc6d1	jiná
silnicích	silnice	k1gFnPc6	silnice
pokračovat	pokračovat	k5eAaImF	pokračovat
na	na	k7c4	na
Prachatice	Prachatice	k1gFnPc4	Prachatice
<g/>
.	.	kIx.	.
<g/>
Vjezd	vjezd	k1gInSc1	vjezd
do	do	k7c2	do
historického	historický	k2eAgNnSc2d1	historické
centra	centrum	k1gNnSc2	centrum
je	být	k5eAaImIp3nS	být
možný	možný	k2eAgInSc1d1	možný
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
příslušným	příslušný	k2eAgNnSc7d1	příslušné
povolením	povolení	k1gNnSc7	povolení
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
vydává	vydávat	k5eAaImIp3nS	vydávat
místní	místní	k2eAgFnSc1d1	místní
městská	městský	k2eAgFnSc1d1	městská
policie	policie	k1gFnSc1	policie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Železniční	železniční	k2eAgNnSc1d1	železniční
===	===	k?	===
</s>
</p>
<p>
<s>
Železniční	železniční	k2eAgFnSc1d1	železniční
trať	trať	k1gFnSc1	trať
194	[number]	k4	194
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
–	–	k?	–
Volary	Volara	k1gFnSc2	Volara
prochází	procházet	k5eAaImIp3nS	procházet
severní	severní	k2eAgFnSc7d1	severní
částí	část	k1gFnSc7	část
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Železniční	železniční	k2eAgFnSc1d1	železniční
stanice	stanice	k1gFnSc1	stanice
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
sídlišti	sídliště	k1gNnSc6	sídliště
Nádražní	nádražní	k2eAgNnPc1d1	nádražní
předměstí	předměstí	k1gNnPc1	předměstí
na	na	k7c4	na
úpatí	úpatí	k1gNnPc4	úpatí
hory	hora	k1gFnSc2	hora
Kleť	Kleť	k1gFnSc1	Kleť
<g/>
.	.	kIx.	.
</s>
<s>
Vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
od	od	k7c2	od
nádraží	nádraží	k1gNnSc2	nádraží
do	do	k7c2	do
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
2	[number]	k4	2
km	km	kA	km
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
její	její	k3xOp3gNnSc4	její
překonání	překonání	k1gNnSc4	překonání
lze	lze	k6eAd1	lze
využít	využít	k5eAaPmF	využít
autobusové	autobusový	k2eAgInPc4d1	autobusový
spoje	spoj	k1gInPc4	spoj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Autobusová	autobusový	k2eAgFnSc1d1	autobusová
===	===	k?	===
</s>
</p>
<p>
<s>
Autobusové	autobusový	k2eAgNnSc4d1	autobusové
nádraží	nádraží	k1gNnSc4	nádraží
<g/>
,	,	kIx,	,
momentálně	momentálně	k6eAd1	momentálně
procházející	procházející	k2eAgFnSc7d1	procházející
rekonstrukcí	rekonstrukce	k1gFnSc7	rekonstrukce
a	a	k8xC	a
úpravou	úprava	k1gFnSc7	úprava
na	na	k7c4	na
moderní	moderní	k2eAgInSc4d1	moderní
areál	areál	k1gInSc4	areál
<g/>
,	,	kIx,	,
najdeme	najít	k5eAaPmIp1nP	najít
nedaleko	nedaleko	k7c2	nedaleko
křižovatky	křižovatka	k1gFnSc2	křižovatka
ulic	ulice	k1gFnPc2	ulice
Objížďková	objížďkový	k2eAgNnPc4d1	objížďkový
a	a	k8xC	a
Nemocniční	nemocniční	k2eAgNnPc4d1	nemocniční
u	u	k7c2	u
krumlovské	krumlovský	k2eAgFnSc2d1	Krumlovská
nemocnice	nemocnice	k1gFnSc2	nemocnice
<g/>
.	.	kIx.	.
</s>
<s>
Vyjíždí	vyjíždět	k5eAaImIp3nS	vyjíždět
z	z	k7c2	z
něj	on	k3xPp3gInSc2	on
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
regionálních	regionální	k2eAgFnPc2d1	regionální
i	i	k8xC	i
dálkových	dálkový	k2eAgFnPc2d1	dálková
linek	linka	k1gFnPc2	linka
<g/>
,	,	kIx,	,
zastávku	zastávka	k1gFnSc4	zastávka
zde	zde	k6eAd1	zde
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
MHD	MHD	kA	MHD
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc4d1	další
větší	veliký	k2eAgFnSc4d2	veliký
autobusovou	autobusový	k2eAgFnSc4d1	autobusová
zastávku	zastávka	k1gFnSc4	zastávka
najdeme	najít	k5eAaPmIp1nP	najít
na	na	k7c6	na
sídlišti	sídliště	k1gNnSc6	sídliště
Špičák	špičák	k1gInSc4	špičák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přímé	přímý	k2eAgNnSc1d1	přímé
autobusové	autobusový	k2eAgNnSc1d1	autobusové
spojení	spojení	k1gNnSc1	spojení
s	s	k7c7	s
Prahou	Praha	k1gFnSc7	Praha
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
autobusy	autobus	k1gInPc4	autobus
Student	student	k1gMnSc1	student
Agency	Agenca	k1gFnSc2	Agenca
a	a	k8xC	a
FlixBus	FlixBus	k1gMnSc1	FlixBus
<g/>
,	,	kIx,	,
ČSAD	ČSAD	kA	ČSAD
Autobusy	autobus	k1gInPc1	autobus
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
(	(	kIx(	(
<g/>
autobusy	autobus	k1gInPc1	autobus
zajíždějí	zajíždět	k5eAaImIp3nP	zajíždět
pouze	pouze	k6eAd1	pouze
k	k	k7c3	k
autobusovému	autobusový	k2eAgNnSc3d1	autobusové
nádraží	nádraží	k1gNnSc3	nádraží
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Na	na	k7c4	na
Knížecí	knížecí	k2eAgFnSc4d1	knížecí
<g/>
)	)	kIx)	)
a	a	k8xC	a
M	M	kA	M
express	express	k1gInSc1	express
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
LEO	Leo	k1gMnSc1	Leo
Express	express	k1gInSc1	express
(	(	kIx(	(
<g/>
tyto	tento	k3xDgInPc1	tento
autobusy	autobus	k1gInPc1	autobus
zajíždějí	zajíždět	k5eAaImIp3nP	zajíždět
na	na	k7c6	na
ÚAN	ÚAN	kA	ÚAN
Florenc	Florenc	k1gFnSc4	Florenc
a	a	k8xC	a
k	k	k7c3	k
vlakovému	vlakový	k2eAgNnSc3d1	vlakové
nádraží	nádraží	k1gNnSc3	nádraží
Praha	Praha	k1gFnSc1	Praha
hl.	hl.	k?	hl.
n.	n.	k?	n.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Služby	služba	k1gFnPc1	služba
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
Českém	český	k2eAgInSc6d1	český
Krumlově	Krumlov	k1gInSc6	Krumlov
je	být	k5eAaImIp3nS	být
nemocnice	nemocnice	k1gFnSc1	nemocnice
se	s	k7c7	s
spádovou	spádový	k2eAgFnSc7d1	spádová
oblastí	oblast	k1gFnSc7	oblast
pro	pro	k7c4	pro
celý	celý	k2eAgInSc4d1	celý
okres	okres	k1gInSc4	okres
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
má	mít	k5eAaImIp3nS	mít
odpovídající	odpovídající	k2eAgNnSc1d1	odpovídající
množství	množství	k1gNnSc1	množství
školních	školní	k2eAgNnPc2d1	školní
zařízení	zařízení	k1gNnPc2	zařízení
(	(	kIx(	(
<g/>
gymnázium	gymnázium	k1gNnSc1	gymnázium
<g/>
,	,	kIx,	,
umělecko-průmyslovou	uměleckorůmyslový	k2eAgFnSc4d1	umělecko-průmyslová
školu	škola	k1gFnSc4	škola
<g/>
,	,	kIx,	,
pobočku	pobočka	k1gFnSc4	pobočka
institutu	institut	k1gInSc2	institut
CEVRO	CEVRO	kA	CEVRO
<g/>
,	,	kIx,	,
učiliště	učiliště	k1gNnSc1	učiliště
<g/>
,	,	kIx,	,
4	[number]	k4	4
základní	základní	k2eAgFnPc4d1	základní
školy	škola	k1gFnPc4	škola
<g/>
,	,	kIx,	,
základní	základní	k2eAgFnSc4d1	základní
uměleckou	umělecký	k2eAgFnSc4d1	umělecká
školu	škola	k1gFnSc4	škola
<g/>
,	,	kIx,	,
speciální	speciální	k2eAgFnSc4d1	speciální
školu	škola	k1gFnSc4	škola
a	a	k8xC	a
7	[number]	k4	7
mateřských	mateřský	k2eAgFnPc2d1	mateřská
škol	škola	k1gFnPc2	škola
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
fungují	fungovat	k5eAaImIp3nP	fungovat
zájmové	zájmový	k2eAgInPc1d1	zájmový
kroužky	kroužek	k1gInPc1	kroužek
a	a	k8xC	a
Dům	dům	k1gInSc1	dům
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
mládeže	mládež	k1gFnSc2	mládež
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
velkých	velký	k2eAgNnPc6d1	velké
sídlištích	sídliště	k1gNnPc6	sídliště
a	a	k8xC	a
v	v	k7c6	v
bezprostředním	bezprostřední	k2eAgNnSc6d1	bezprostřední
okolí	okolí	k1gNnSc6	okolí
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
najdeme	najít	k5eAaPmIp1nP	najít
samoobsluhy	samoobsluha	k1gFnPc4	samoobsluha
či	či	k8xC	či
Vietnamské	vietnamský	k2eAgFnPc4d1	vietnamská
večerky	večerka	k1gFnPc4	večerka
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
provozované	provozovaný	k2eAgFnSc2d1	provozovaná
spotřebním	spotřební	k2eAgNnSc7d1	spotřební
družstvem	družstvo	k1gNnSc7	družstvo
COOP	COOP	kA	COOP
v	v	k7c6	v
Kaplici	Kaplice	k1gFnSc6	Kaplice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
města	město	k1gNnSc2	město
u	u	k7c2	u
Domoradic	Domoradice	k1gInPc2	Domoradice
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
hypermarket	hypermarket	k1gInSc1	hypermarket
TERNO	terno	k1gNnSc4	terno
<g/>
,	,	kIx,	,
supermarket	supermarket	k1gInSc1	supermarket
Lidl	Lidl	k1gInSc1	Lidl
<g/>
,	,	kIx,	,
supermarket	supermarket	k1gInSc1	supermarket
Penny	penny	k1gNnSc1	penny
Market	market	k1gInSc1	market
<g/>
,	,	kIx,	,
hypermarket	hypermarket	k1gInSc1	hypermarket
Tesco	Tesco	k6eAd1	Tesco
a	a	k8xC	a
hypermarket	hypermarket	k1gInSc4	hypermarket
Kaufland	Kauflanda	k1gFnPc2	Kauflanda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stravovacích	stravovací	k2eAgNnPc2d1	stravovací
a	a	k8xC	a
ubytovacích	ubytovací	k2eAgNnPc2d1	ubytovací
zařízení	zařízení	k1gNnPc2	zařízení
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
městě	město	k1gNnSc6	město
nadprůměrně	nadprůměrně	k6eAd1	nadprůměrně
velký	velký	k2eAgInSc4d1	velký
počet	počet	k1gInSc4	počet
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
počtem	počet	k1gInSc7	počet
návštěvníků	návštěvník	k1gMnPc2	návštěvník
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Český	český	k2eAgInSc1d1	český
Krumlov	Krumlov	k1gInSc1	Krumlov
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
stanic	stanice	k1gFnPc2	stanice
vodáků	vodák	k1gMnPc2	vodák
na	na	k7c6	na
horním	horní	k2eAgInSc6d1	horní
toku	tok	k1gInSc6	tok
Vltavy	Vltava	k1gFnSc2	Vltava
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
blízkosti	blízkost	k1gFnSc6	blízkost
nachází	nacházet	k5eAaImIp3nS	nacházet
několik	několik	k4yIc1	několik
kempů	kemp	k1gInPc2	kemp
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Západně	západně	k6eAd1	západně
od	od	k7c2	od
města	město	k1gNnSc2	město
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
Dobrkovice	Dobrkovice	k1gFnPc4	Dobrkovice
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
sportovní	sportovní	k2eAgInSc1d1	sportovní
areál	areál	k1gInSc1	areál
<g/>
,	,	kIx,	,
krytý	krytý	k2eAgInSc1d1	krytý
plavecký	plavecký	k2eAgInSc4d1	plavecký
bazén	bazén	k1gInSc4	bazén
<g/>
,	,	kIx,	,
hokejový	hokejový	k2eAgInSc4d1	hokejový
a	a	k8xC	a
fotbalový	fotbalový	k2eAgInSc4d1	fotbalový
stadion	stadion	k1gInSc4	stadion
a	a	k8xC	a
kurty	kurt	k1gInPc4	kurt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pamětihodnosti	pamětihodnost	k1gFnSc6	pamětihodnost
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Zámek	zámek	k1gInSc1	zámek
===	===	k?	===
</s>
</p>
<p>
<s>
Zámek	zámek	k1gInSc1	zámek
Český	český	k2eAgInSc1d1	český
Krumlov	Krumlov	k1gInSc1	Krumlov
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
Pražském	pražský	k2eAgInSc6d1	pražský
hradě	hrad	k1gInSc6	hrad
druhý	druhý	k4xOgMnSc1	druhý
nejrozsáhlejší	rozsáhlý	k2eAgInSc1d3	nejrozsáhlejší
zámecký	zámecký	k2eAgInSc1d1	zámecký
areál	areál	k1gInSc1	areál
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
gotický	gotický	k2eAgInSc1d1	gotický
hrad	hrad	k1gInSc1	hrad
byl	být	k5eAaImAgInS	být
přestavěn	přestavět	k5eAaPmNgInS	přestavět
zejména	zejména	k9	zejména
v	v	k7c6	v
období	období	k1gNnSc6	období
renesance	renesance	k1gFnSc2	renesance
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Viléma	Vilém	k1gMnSc2	Vilém
z	z	k7c2	z
Rožmberka	Rožmberk	k1gInSc2	Rožmberk
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
barokní	barokní	k2eAgFnSc1d1	barokní
přestavba	přestavba	k1gFnSc1	přestavba
v	v	k7c6	v
barokním	barokní	k2eAgInSc6d1	barokní
slohu	sloh	k1gInSc6	sloh
se	se	k3xPyFc4	se
odehrála	odehrát	k5eAaPmAgFnS	odehrát
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
vévody	vévoda	k1gMnSc2	vévoda
Jana	Jan	k1gMnSc2	Jan
Kristiána	Kristián	k1gMnSc2	Kristián
I.	I.	kA	I.
z	z	k7c2	z
Eggenbergu	Eggenberg	k1gInSc2	Eggenberg
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
kompletní	kompletní	k2eAgFnSc1d1	kompletní
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
poslední	poslední	k2eAgFnSc1d1	poslední
významná	významný	k2eAgFnSc1d1	významná
přestavba	přestavba	k1gFnSc1	přestavba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
dala	dát	k5eAaPmAgFnS	dát
komplexu	komplex	k1gInSc2	komplex
jeho	jeho	k3xOp3gFnSc4	jeho
současnou	současný	k2eAgFnSc4d1	současná
podobu	podoba	k1gFnSc4	podoba
<g/>
,	,	kIx,	,
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Josefa	Josef	k1gMnSc2	Josef
I.	I.	kA	I.
Adama	Adam	k1gMnSc2	Adam
ze	z	k7c2	z
Schwarzenbergu	Schwarzenberg	k1gInSc2	Schwarzenberg
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
nechal	nechat	k5eAaPmAgInS	nechat
vystavět	vystavět	k5eAaPmF	vystavět
jak	jak	k6eAd1	jak
dnešní	dnešní	k2eAgNnSc4d1	dnešní
zámecké	zámecký	k2eAgNnSc4d1	zámecké
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
jedinečný	jedinečný	k2eAgInSc1d1	jedinečný
Plášťový	plášťový	k2eAgInSc1d1	plášťový
most	most	k1gInSc1	most
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
rarit	rarita	k1gFnPc2	rarita
Českého	český	k2eAgInSc2d1	český
Krumlova	Krumlov	k1gInSc2	Krumlov
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
hradní	hradní	k2eAgMnPc1d1	hradní
medvědi	medvěd	k1gMnPc1	medvěd
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
jsou	být	k5eAaImIp3nP	být
chováni	chovat	k5eAaImNgMnP	chovat
od	od	k7c2	od
dob	doba	k1gFnPc2	doba
Rožmberků	Rožmberk	k1gMnPc2	Rožmberk
v	v	k7c6	v
hradním	hradní	k2eAgInSc6d1	hradní
příkopu	příkop	k1gInSc6	příkop
mezi	mezi	k7c7	mezi
I.	I.	kA	I.
a	a	k8xC	a
II	II	kA	II
<g/>
.	.	kIx.	.
nádvořím	nádvoří	k1gNnSc7	nádvoří
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zámecký	zámecký	k2eAgInSc1d1	zámecký
komplex	komplex	k1gInSc1	komplex
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
členěn	členěn	k2eAgInSc1d1	členěn
na	na	k7c4	na
pět	pět	k4xCc4	pět
nádvoří	nádvoří	k1gNnPc2	nádvoří
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
tvoří	tvořit	k5eAaImIp3nP	tvořit
několik	několik	k4yIc4	několik
větších	veliký	k2eAgInPc2d2	veliký
celků	celek	k1gInPc2	celek
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Dolní	dolní	k2eAgInSc1d1	dolní
hrad	hrad	k1gInSc1	hrad
–	–	k?	–
nejpůvodnější	původní	k2eAgFnSc4d3	nejpůvodnější
část	část	k1gFnSc4	část
hradu	hrad	k1gInSc2	hrad
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Hrádek	hrádek	k1gInSc1	hrádek
s	s	k7c7	s
dominující	dominující	k2eAgFnSc7d1	dominující
věží	věž	k1gFnSc7	věž
</s>
</p>
<p>
<s>
Horní	horní	k2eAgInSc1d1	horní
hrad	hrad	k1gInSc1	hrad
–	–	k?	–
hlavní	hlavní	k2eAgFnSc4d1	hlavní
část	část	k1gFnSc4	část
zámeckého	zámecký	k2eAgInSc2d1	zámecký
areálu	areál	k1gInSc2	areál
obklopující	obklopující	k2eAgFnSc2d1	obklopující
III	III	kA	III
<g/>
.	.	kIx.	.
a	a	k8xC	a
IV	IV	kA	IV
<g/>
.	.	kIx.	.
nádvoří	nádvoří	k1gNnSc1	nádvoří
</s>
</p>
<p>
<s>
Plášťový	plášťový	k2eAgInSc1d1	plášťový
most	most	k1gInSc1	most
–	–	k?	–
několikaposchoďové	několikaposchoďový	k2eAgNnSc4d1	několikaposchoďový
přemostění	přemostění	k1gNnSc4	přemostění
Jelení	jelení	k2eAgFnSc2d1	jelení
zahrady	zahrada	k1gFnSc2	zahrada
spojující	spojující	k2eAgInSc1d1	spojující
zámek	zámek	k1gInSc1	zámek
<g/>
,	,	kIx,	,
barokní	barokní	k2eAgNnSc4d1	barokní
divadlo	divadlo	k1gNnSc4	divadlo
a	a	k8xC	a
zámeckou	zámecký	k2eAgFnSc4d1	zámecká
zahradu	zahrada	k1gFnSc4	zahrada
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1764	[number]	k4	1764
</s>
</p>
<p>
<s>
Zámecké	zámecký	k2eAgNnSc1d1	zámecké
barokní	barokní	k2eAgNnSc1d1	barokní
divadlo	divadlo	k1gNnSc1	divadlo
–	–	k?	–
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
mála	málo	k1gNnSc2	málo
dochovaných	dochovaný	k2eAgNnPc2d1	dochované
barokních	barokní	k2eAgNnPc2d1	barokní
divadel	divadlo	k1gNnPc2	divadlo
na	na	k7c6	na
světě	svět	k1gInSc6	svět
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1681	[number]	k4	1681
</s>
</p>
<p>
<s>
Letní	letní	k2eAgFnSc1d1	letní
a	a	k8xC	a
Zimní	zimní	k2eAgFnSc1d1	zimní
jízdárna	jízdárna	k1gFnSc1	jízdárna
</s>
</p>
<p>
<s>
Zámecká	zámecký	k2eAgFnSc1d1	zámecká
zahrada	zahrada	k1gFnSc1	zahrada
–	–	k?	–
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
700	[number]	k4	700
m	m	kA	m
<g/>
,	,	kIx,	,
o	o	k7c6	o
celkové	celkový	k2eAgFnSc6d1	celková
rozloze	rozloha	k1gFnSc6	rozloha
10	[number]	k4	10
ha	ha	kA	ha
</s>
</p>
<p>
<s>
Zámecká	zámecký	k2eAgFnSc1d1	zámecká
zahrada	zahrada	k1gFnSc1	zahrada
je	být	k5eAaImIp3nS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
francouzskou	francouzský	k2eAgFnSc4d1	francouzská
a	a	k8xC	a
anglickou	anglický	k2eAgFnSc4d1	anglická
část	část	k1gFnSc4	část
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
jmenované	jmenovaná	k1gFnSc6	jmenovaná
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Kaskádová	kaskádový	k2eAgFnSc1d1	kaskádová
fontána	fontána	k1gFnSc1	fontána
<g/>
,	,	kIx,	,
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
pak	pak	k6eAd1	pak
barokní	barokní	k2eAgInSc4d1	barokní
letohrádek	letohrádek	k1gInSc4	letohrádek
Belárie	Belárie	k1gFnSc2	Belárie
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
otáčivým	otáčivý	k2eAgNnSc7d1	otáčivé
hledištěm	hlediště	k1gNnSc7	hlediště
<g/>
,	,	kIx,	,
využívaným	využívaný	k2eAgNnSc7d1	využívané
v	v	k7c6	v
letní	letní	k2eAgFnSc6d1	letní
sezóně	sezóna	k1gFnSc6	sezóna
k	k	k7c3	k
divadelním	divadelní	k2eAgFnPc3d1	divadelní
a	a	k8xC	a
operním	operní	k2eAgFnPc3d1	operní
inscenacím	inscenace	k1gFnPc3	inscenace
pod	pod	k7c7	pod
širým	širý	k2eAgNnSc7d1	širé
nebem	nebe	k1gNnSc7	nebe
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vnitřní	vnitřní	k2eAgNnSc1d1	vnitřní
město	město	k1gNnSc1	město
===	===	k?	===
</s>
</p>
<p>
<s>
Vnitřní	vnitřní	k2eAgNnSc1d1	vnitřní
město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
centrální	centrální	k2eAgFnSc1d1	centrální
městská	městský	k2eAgFnSc1d1	městská
část	část	k1gFnSc1	část
se	s	k7c7	s
spolu	spolu	k6eAd1	spolu
přiléhajícím	přiléhající	k2eAgInSc7d1	přiléhající
Ostrovem	ostrov	k1gInSc7	ostrov
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
obklopena	obklopit	k5eAaPmNgFnS	obklopit
meandrem	meandr	k1gInSc7	meandr
řeky	řeka	k1gFnSc2	řeka
Vltavy	Vltava	k1gFnSc2	Vltava
<g/>
.	.	kIx.	.
</s>
<s>
Spojení	spojení	k1gNnSc1	spojení
s	s	k7c7	s
ostatními	ostatní	k2eAgFnPc7d1	ostatní
městskými	městský	k2eAgFnPc7d1	městská
částmi	část	k1gFnPc7	část
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
tři	tři	k4xCgInPc1	tři
mosty	most	k1gInPc1	most
–	–	k?	–
Lazebnický	lazebnický	k2eAgInSc1d1	lazebnický
most	most	k1gInSc1	most
<g/>
,	,	kIx,	,
most	most	k1gInSc1	most
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Edvarda	Edvard	k1gMnSc2	Edvard
Beneše	Beneš	k1gMnSc2	Beneš
<g/>
,	,	kIx,	,
most	most	k1gInSc4	most
Na	na	k7c6	na
Horní	horní	k2eAgFnSc6d1	horní
bráně	brána	k1gFnSc6	brána
<g/>
;	;	kIx,	;
a	a	k8xC	a
tzv.	tzv.	kA	tzv.
Jelení	jelení	k2eAgFnSc1d1	jelení
lávka	lávka	k1gFnSc1	lávka
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
ulic	ulice	k1gFnPc2	ulice
s	s	k7c7	s
pravidelným	pravidelný	k2eAgNnSc7d1	pravidelné
náměstím	náměstí	k1gNnSc7	náměstí
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
nezměněn	změnit	k5eNaPmNgInS	změnit
od	od	k7c2	od
doby	doba	k1gFnSc2	doba
svého	svůj	k3xOyFgNnSc2	svůj
založení	založení	k1gNnSc2	založení
<g/>
.	.	kIx.	.
</s>
<s>
Nalezneme	nalézt	k5eAaBmIp1nP	nalézt
zde	zde	k6eAd1	zde
velký	velký	k2eAgInSc4d1	velký
počet	počet	k1gInSc4	počet
zachovalých	zachovalý	k2eAgInPc2d1	zachovalý
městských	městský	k2eAgInPc2d1	městský
domů	dům	k1gInPc2	dům
–	–	k?	–
gotických	gotický	k2eAgInPc6d1	gotický
<g/>
,	,	kIx,	,
renesančních	renesanční	k2eAgInPc6d1	renesanční
a	a	k8xC	a
v	v	k7c6	v
menší	malý	k2eAgFnSc6d2	menší
míře	míra	k1gFnSc6	míra
i	i	k9	i
barokních	barokní	k2eAgFnPc2d1	barokní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Radnice	radnice	k1gFnSc1	radnice
–	–	k?	–
renesanční	renesanční	k2eAgFnSc1d1	renesanční
stavba	stavba	k1gFnSc1	stavba
ze	z	k7c2	z
2	[number]	k4	2
<g/>
.	.	kIx.	.
poloviny	polovina	k1gFnSc2	polovina
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
nám.	nám.	k?	nám.
Svornosti	svornost	k1gFnSc2	svornost
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kašna	kašna	k1gFnSc1	kašna
s	s	k7c7	s
morovým	morový	k2eAgInSc7d1	morový
sloupem	sloup	k1gInSc7	sloup
<g/>
–	–	k?	–
původně	původně	k6eAd1	původně
morový	morový	k2eAgInSc1d1	morový
sloup	sloup	k1gInSc1	sloup
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1716	[number]	k4	1716
s	s	k7c7	s
přistavěnou	přistavěný	k2eAgFnSc7d1	přistavěná
kašnou	kašna	k1gFnSc7	kašna
z	z	k7c2	z
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
nám.	nám.	k?	nám.
Svornosti	svornost	k1gFnSc2	svornost
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kaplanka	kaplanka	k1gFnSc1	kaplanka
–	–	k?	–
pozdně	pozdně	k6eAd1	pozdně
gotický	gotický	k2eAgInSc4d1	gotický
dům	dům	k1gInSc4	dům
s	s	k7c7	s
raně	raně	k6eAd1	raně
renesančními	renesanční	k2eAgInPc7d1	renesanční
prvky	prvek	k1gInPc7	prvek
(	(	kIx(	(
<g/>
ul	ul	kA	ul
<g/>
.	.	kIx.	.
Horní	horní	k2eAgFnSc1d1	horní
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Víta	Vít	k1gMnSc2	Vít
–	–	k?	–
gotický	gotický	k2eAgInSc4d1	gotický
trojlodní	trojlodní	k2eAgInSc4d1	trojlodní
kostel	kostel	k1gInSc4	kostel
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1408	[number]	k4	1408
<g/>
–	–	k?	–
<g/>
1439	[number]	k4	1439
(	(	kIx(	(
<g/>
ul	ul	kA	ul
<g/>
.	.	kIx.	.
Kostelní	kostelní	k2eAgFnSc4d1	kostelní
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
tzv.	tzv.	kA	tzv.
Krčínův	Krčínův	k2eAgInSc1d1	Krčínův
dům	dům	k1gInSc1	dům
–	–	k?	–
stavba	stavba	k1gFnSc1	stavba
s	s	k7c7	s
renesanční	renesanční	k2eAgFnSc7d1	renesanční
fasádou	fasáda	k1gFnSc7	fasáda
ze	z	k7c2	z
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
ul	ul	kA	ul
<g/>
.	.	kIx.	.
Kájovská	kájovský	k2eAgFnSc1d1	Kájovská
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tradičně	tradičně	k6eAd1	tradičně
spojována	spojován	k2eAgFnSc1d1	spojována
s	s	k7c7	s
osobou	osoba	k1gFnSc7	osoba
Jakuba	Jakub	k1gMnSc2	Jakub
Krčína	Krčín	k1gMnSc2	Krčín
z	z	k7c2	z
Jelčan	Jelčan	k1gMnSc1	Jelčan
a	a	k8xC	a
Sedlčan	Sedlčany	k1gInPc2	Sedlčany
</s>
</p>
<p>
<s>
Vlašský	vlašský	k2eAgInSc1d1	vlašský
dvůr	dvůr	k1gInSc1	dvůr
–	–	k?	–
gotická	gotický	k2eAgFnSc1d1	gotická
stavba	stavba	k1gFnSc1	stavba
s	s	k7c7	s
renesanční	renesanční	k2eAgFnSc7d1	renesanční
výzdobou	výzdoba	k1gFnSc7	výzdoba
a	a	k8xC	a
fasádou	fasáda	k1gFnSc7	fasáda
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
patřila	patřit	k5eAaImAgFnS	patřit
Jakubu	Jakub	k1gMnSc3	Jakub
Krčínovi	Krčín	k1gMnSc3	Krčín
(	(	kIx(	(
<g/>
ul	ul	kA	ul
<g/>
.	.	kIx.	.
Dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jezuitská	jezuitský	k2eAgFnSc1d1	jezuitská
kolej	kolej	k1gFnSc1	kolej
–	–	k?	–
čtyřkřídlá	čtyřkřídlý	k2eAgFnSc1d1	čtyřkřídlá
renesanční	renesanční	k2eAgFnSc1d1	renesanční
budova	budova	k1gFnSc1	budova
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1586	[number]	k4	1586
<g/>
–	–	k?	–
<g/>
1588	[number]	k4	1588
vystavěná	vystavěný	k2eAgFnSc1d1	vystavěná
z	z	k7c2	z
příkazu	příkaz	k1gInSc2	příkaz
Viléma	Vilém	k1gMnSc2	Vilém
z	z	k7c2	z
Rožmberka	Rožmberk	k1gInSc2	Rožmberk
(	(	kIx(	(
<g/>
ul	ul	kA	ul
<g/>
.	.	kIx.	.
Horní	horní	k2eAgFnSc1d1	horní
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Široká	široký	k2eAgFnSc1d1	široká
ulice	ulice	k1gFnSc1	ulice
–	–	k?	–
nejširší	široký	k2eAgFnSc1d3	nejširší
městská	městský	k2eAgFnSc1d1	městská
ulice	ulice	k1gFnSc1	ulice
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
využívaná	využívaný	k2eAgFnSc1d1	využívaná
k	k	k7c3	k
trhům	trh	k1gInPc3	trh
</s>
</p>
<p>
<s>
===	===	k?	===
Latrán	latrán	k1gInSc4	latrán
===	===	k?	===
</s>
</p>
<p>
<s>
Latrán	latrán	k1gInSc1	latrán
<g/>
,	,	kIx,	,
kdysi	kdysi	k6eAd1	kdysi
samostatné	samostatný	k2eAgNnSc4d1	samostatné
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
další	další	k2eAgFnSc1d1	další
historická	historický	k2eAgFnSc1d1	historická
čtvrť	čtvrť	k1gFnSc1	čtvrť
za	za	k7c7	za
řekou	řeka	k1gFnSc7	řeka
<g/>
,	,	kIx,	,
severně	severně	k6eAd1	severně
od	od	k7c2	od
Vnitřního	vnitřní	k2eAgNnSc2d1	vnitřní
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
v	v	k7c6	v
podhradí	podhradí	k1gNnSc6	podhradí
zámku	zámek	k1gInSc2	zámek
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yRgNnSc3	který
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
správně	správně	k6eAd1	správně
náležela	náležet	k5eAaImAgFnS	náležet
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Vnitřním	vnitřní	k2eAgNnSc7d1	vnitřní
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
spojen	spojit	k5eAaPmNgInS	spojit
dřevěným	dřevěný	k2eAgInSc7d1	dřevěný
Lazebnickým	lazebnický	k2eAgInSc7d1	lazebnický
mostem	most	k1gInSc7	most
a	a	k8xC	a
i	i	k9	i
zde	zde	k6eAd1	zde
nalezneme	naleznout	k5eAaPmIp1nP	naleznout
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
menších	malý	k2eAgInPc2d2	menší
gotických	gotický	k2eAgInPc2d1	gotický
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
čtvrti	čtvrt	k1gFnSc6	čtvrt
leží	ležet	k5eAaImIp3nS	ležet
i	i	k9	i
bývalá	bývalý	k2eAgFnSc1d1	bývalá
městská	městský	k2eAgFnSc1d1	městská
část	část	k1gFnSc1	část
tzv.	tzv.	kA	tzv.
Neustadt	Neustadta	k1gFnPc2	Neustadta
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
ulice	ulice	k1gFnSc1	ulice
Nové	Nová	k1gFnSc2	Nová
Město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
archeologickými	archeologický	k2eAgFnPc7d1	archeologická
výzkumy	výzkum	k1gInPc1	výzkum
prokázáno	prokázat	k5eAaPmNgNnS	prokázat
nejstarší	starý	k2eAgNnSc1d3	nejstarší
lidské	lidský	k2eAgNnSc1d1	lidské
osídlení	osídlení	k1gNnSc1	osídlení
ve	v	k7c6	v
městě	město	k1gNnSc6	město
z	z	k7c2	z
5	[number]	k4	5
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc1	tisíciletí
před	před	k7c7	před
naším	náš	k3xOp1gInSc7	náš
letopočtem	letopočet	k1gInSc7	letopočet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Budějovická	budějovický	k2eAgFnSc1d1	Budějovická
brána	brána	k1gFnSc1	brána
(	(	kIx(	(
<g/>
Latrán	latrán	k1gInSc1	latrán
č.	č.	k?	č.
p.	p.	k?	p.
104	[number]	k4	104
<g/>
)	)	kIx)	)
–	–	k?	–
nejmladší	mladý	k2eAgInSc4d3	nejmladší
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
jediná	jediný	k2eAgFnSc1d1	jediná
dochovaná	dochovaný	k2eAgFnSc1d1	dochovaná
brána	brána	k1gFnSc1	brána
z	z	k7c2	z
původního	původní	k2eAgNnSc2d1	původní
středověkého	středověký	k2eAgNnSc2d1	středověké
městského	městský	k2eAgNnSc2d1	Městské
opevnění	opevnění	k1gNnSc2	opevnění
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
zbudována	zbudovat	k5eAaPmNgFnS	zbudovat
nákladem	náklad	k1gInSc7	náklad
Petra	Petr	k1gMnSc2	Petr
Voka	Vokus	k1gMnSc2	Vokus
z	z	k7c2	z
Rožmberka	Rožmberk	k1gInSc2	Rožmberk
v	v	k7c6	v
letech	let	k1gInPc6	let
1598	[number]	k4	1598
<g/>
–	–	k?	–
<g/>
1602	[number]	k4	1602
architektem	architekt	k1gMnSc7	architekt
Domenikem	Domenik	k1gMnSc7	Domenik
Benedettem	Benedett	k1gMnSc7	Benedett
Comettou	Cometta	k1gMnSc7	Cometta
z	z	k7c2	z
Eckthurnu	Eckthurn	k1gInSc2	Eckthurn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Jošta	Jošt	k1gInSc2	Jošt
–	–	k?	–
tzv.	tzv.	kA	tzv.
Německý	německý	k2eAgInSc1d1	německý
kostel	kostel	k1gInSc1	kostel
(	(	kIx(	(
<g/>
ul	ul	kA	ul
<g/>
.	.	kIx.	.
Latrán	latrán	k1gInSc1	latrán
č.	č.	k?	č.
p.	p.	k?	p.
6	[number]	k4	6
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
gotický	gotický	k2eAgInSc1d1	gotický
kostel	kostel	k1gInSc1	kostel
z	z	k7c2	z
období	období	k1gNnSc2	období
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1330	[number]	k4	1330
založený	založený	k2eAgInSc1d1	založený
Petrem	Petr	k1gMnSc7	Petr
I.	I.	kA	I.
z	z	k7c2	z
Rožmberka	Rožmberk	k1gInSc2	Rožmberk
<g/>
,	,	kIx,	,
za	za	k7c2	za
Petra	Petr	k1gMnSc2	Petr
Voka	Vokus	k1gMnSc2	Vokus
z	z	k7c2	z
Rožmberka	Rožmberk	k1gInSc2	Rožmberk
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1594	[number]	k4	1594
<g/>
–	–	k?	–
<g/>
1599	[number]	k4	1599
renesančně	renesančně	k6eAd1	renesančně
přestavěn	přestavět	k5eAaPmNgInS	přestavět
(	(	kIx(	(
<g/>
architekt	architekt	k1gMnSc1	architekt
Domenico	Domenico	k1gMnSc1	Domenico
Benedetto	Benedetto	k1gNnSc4	Benedetto
Cometta	Comett	k1gMnSc2	Comett
z	z	k7c2	z
Eckthurnu	Eckthurn	k1gInSc2	Eckthurn
<g/>
)	)	kIx)	)
a	a	k8xC	a
věnován	věnovat	k5eAaPmNgInS	věnovat
krumlovským	krumlovský	k2eAgMnPc3d1	krumlovský
protestantům	protestant	k1gMnPc3	protestant
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
byl	být	k5eAaImAgInS	být
zasvěcen	zasvětit	k5eAaPmNgInS	zasvětit
Nejsvětější	nejsvětější	k2eAgInSc1d1	nejsvětější
Trojici	trojice	k1gFnSc3	trojice
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1615	[number]	k4	1615
byl	být	k5eAaImAgInS	být
kostel	kostel	k1gInSc1	kostel
opět	opět	k6eAd1	opět
zasvěcen	zasvětit	k5eAaPmNgInS	zasvětit
sv.	sv.	kA	sv.
Joštovi	Joštův	k2eAgMnPc1d1	Joštův
a	a	k8xC	a
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1773	[number]	k4	1773
ho	on	k3xPp3gInSc4	on
spravovali	spravovat	k5eAaImAgMnP	spravovat
jezuité	jezuita	k1gMnPc1	jezuita
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
byl	být	k5eAaImAgInS	být
barokizován	barokizován	k2eAgInSc1d1	barokizován
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1787	[number]	k4	1787
byl	být	k5eAaImAgInS	být
za	za	k7c2	za
josefinských	josefinský	k2eAgFnPc2d1	josefinská
reforem	reforma	k1gFnPc2	reforma
zrušen	zrušit	k5eAaPmNgInS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
je	být	k5eAaImIp3nS	být
využívaný	využívaný	k2eAgInSc4d1	využívaný
jako	jako	k8xC	jako
obytný	obytný	k2eAgInSc4d1	obytný
dům	dům	k1gInSc4	dům
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
budově	budova	k1gFnSc6	budova
také	také	k9	také
sídlí	sídlet	k5eAaImIp3nS	sídlet
muzeum	muzeum	k1gNnSc1	muzeum
marionet	marioneta	k1gFnPc2	marioneta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zámecký	zámecký	k2eAgInSc1d1	zámecký
pivovar	pivovar	k1gInSc1	pivovar
–	–	k?	–
dříve	dříve	k6eAd2	dříve
Schwarzenberský	schwarzenberský	k2eAgInSc1d1	schwarzenberský
pivovar	pivovar	k1gInSc1	pivovar
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
Pivovar	pivovar	k1gInSc1	pivovar
Eggenberg	Eggenberg	k1gInSc1	Eggenberg
</s>
</p>
<p>
<s>
Minoritský	minoritský	k2eAgInSc1d1	minoritský
klášter	klášter	k1gInSc1	klášter
–	–	k?	–
dnes	dnes	k6eAd1	dnes
klášter	klášter	k1gInSc4	klášter
Řádu	řád	k1gInSc2	řád
Křížovníků	křížovník	k1gMnPc2	křížovník
s	s	k7c7	s
červenou	červený	k2eAgFnSc7d1	červená
hvězdou	hvězda	k1gFnSc7	hvězda
</s>
</p>
<p>
<s>
===	===	k?	===
Plešivec	plešivec	k1gMnSc1	plešivec
===	===	k?	===
</s>
</p>
<p>
<s>
Plešivec	plešivec	k1gMnSc1	plešivec
je	být	k5eAaImIp3nS	být
historická	historický	k2eAgFnSc1d1	historická
část	část	k1gFnSc1	část
předměstí	předměstí	k1gNnSc2	předměstí
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
samostatnou	samostatný	k2eAgFnSc7d1	samostatná
městskou	městský	k2eAgFnSc7d1	městská
památkovou	památkový	k2eAgFnSc7d1	památková
zónou	zóna	k1gFnSc7	zóna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Synagoga	synagoga	k1gFnSc1	synagoga
–	–	k?	–
první	první	k4xOgFnSc1	první
železobetonová	železobetonový	k2eAgFnSc1d1	železobetonová
konstrukce	konstrukce	k1gFnSc1	konstrukce
v	v	k7c6	v
Rakousku-Uhersku	Rakousku-Uhersek	k1gInSc6	Rakousku-Uhersek
</s>
</p>
<p>
<s>
Plešivecké	Plešivecký	k2eAgNnSc1d1	Plešivecké
náměstí	náměstí	k1gNnSc1	náměstí
–	–	k?	–
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
renesančních	renesanční	k2eAgInPc2d1	renesanční
domů	dům	k1gInPc2	dům
</s>
</p>
<p>
<s>
Fotoateliér	fotoateliér	k1gInSc1	fotoateliér
Josefa	Josef	k1gMnSc2	Josef
Seidela	Seidel	k1gMnSc2	Seidel
–	–	k?	–
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
dochovaných	dochovaný	k2eAgMnPc2d1	dochovaný
a	a	k8xC	a
funkčních	funkční	k2eAgInPc2d1	funkční
historických	historický	k2eAgInPc2d1	historický
foto-ateliérů	fototeliér	k1gInPc2	foto-ateliér
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
</s>
</p>
<p>
<s>
==	==	k?	==
Kultura	kultura	k1gFnSc1	kultura
==	==	k?	==
</s>
</p>
<p>
<s>
Český	český	k2eAgInSc1d1	český
Krumlov	Krumlov	k1gInSc1	Krumlov
je	být	k5eAaImIp3nS	být
významným	významný	k2eAgNnSc7d1	významné
českým	český	k2eAgNnSc7d1	české
kulturním	kulturní	k2eAgNnSc7d1	kulturní
střediskem	středisko	k1gNnSc7	středisko
hostícím	hostící	k2eAgNnSc7d1	hostící
řadu	řada	k1gFnSc4	řada
festivalů	festival	k1gInPc2	festival
i	i	k8xC	i
pravidelných	pravidelný	k2eAgFnPc2d1	pravidelná
regionálních	regionální	k2eAgFnPc2d1	regionální
akcí	akce	k1gFnPc2	akce
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
velkým	velký	k2eAgNnSc7d1	velké
množstvím	množství	k1gNnSc7	množství
muzeí	muzeum	k1gNnPc2	muzeum
a	a	k8xC	a
galerií	galerie	k1gFnPc2	galerie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
důležité	důležitý	k2eAgFnPc4d1	důležitá
divadelní	divadelní	k2eAgFnPc4d1	divadelní
instituce	instituce	k1gFnPc4	instituce
patří	patřit	k5eAaImIp3nS	patřit
Městské	městský	k2eAgNnSc1d1	Městské
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	se	k3xPyFc4	se
při	při	k7c6	při
historickém	historický	k2eAgNnSc6d1	historické
centru	centrum	k1gNnSc6	centrum
v	v	k7c6	v
Horní	horní	k2eAgFnSc6d1	horní
ulici	ulice	k1gFnSc6	ulice
<g/>
,	,	kIx,	,
a	a	k8xC	a
především	především	k9	především
pak	pak	k6eAd1	pak
technický	technický	k2eAgInSc4d1	technický
div	div	k1gInSc4	div
Otáčivé	otáčivý	k2eAgNnSc1d1	otáčivé
hlediště	hlediště	k1gNnSc1	hlediště
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
je	být	k5eAaImIp3nS	být
situováno	situovat	k5eAaBmNgNnS	situovat
v	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
části	část	k1gFnSc6	část
Zámecké	zámecký	k2eAgFnSc2d1	zámecká
zahrady	zahrada	k1gFnSc2	zahrada
a	a	k8xC	a
které	který	k3yQgNnSc1	který
představuje	představovat	k5eAaImIp3nS	představovat
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
open-air	openira	k1gFnPc2	open-aira
scén	scéna	k1gFnPc2	scéna
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
se	se	k3xPyFc4	se
také	také	k9	také
nachází	nacházet	k5eAaImIp3nS	nacházet
kino	kino	k1gNnSc4	kino
Luna	luna	k1gFnSc1	luna
a	a	k8xC	a
Městská	městský	k2eAgFnSc1d1	městská
knihovna	knihovna	k1gFnSc1	knihovna
s	s	k7c7	s
pobočkami	pobočka	k1gFnPc7	pobočka
ve	v	k7c6	v
čtvrtích	čtvrt	k1gFnPc6	čtvrt
Plešivec	plešivec	k1gMnSc1	plešivec
<g/>
,	,	kIx,	,
Mír	mír	k1gInSc1	mír
a	a	k8xC	a
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
oficiální	oficiální	k2eAgFnPc4d1	oficiální
kamenné	kamenný	k2eAgFnPc4d1	kamenná
instituce	instituce	k1gFnPc4	instituce
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
zmínit	zmínit	k5eAaPmF	zmínit
i	i	k9	i
projekt	projekt	k1gInSc1	projekt
Ukradená	ukradený	k2eAgFnSc1d1	ukradená
galerie	galerie	k1gFnSc1	galerie
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
právě	právě	k6eAd1	právě
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
městě	město	k1gNnSc6	město
a	a	k8xC	a
kterou	který	k3yRgFnSc4	který
nalezneme	nalézt	k5eAaBmIp1nP	nalézt
v	v	k7c6	v
Hradební	hradební	k2eAgFnSc6d1	hradební
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
téže	týž	k3xTgFnSc6	týž
ulici	ulice	k1gFnSc6	ulice
lze	lze	k6eAd1	lze
také	také	k9	také
narazit	narazit	k5eAaPmF	narazit
na	na	k7c4	na
streetartovou	streetartový	k2eAgFnSc4d1	streetartová
tvorbu	tvorba	k1gFnSc4	tvorba
mnoha	mnoho	k4c2	mnoho
místních	místní	k2eAgMnPc2d1	místní
i	i	k8xC	i
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
experimentálních	experimentální	k2eAgMnPc2d1	experimentální
umělců	umělec	k1gMnPc2	umělec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Galerie	galerie	k1gFnSc2	galerie
a	a	k8xC	a
muzea	muzeum	k1gNnSc2	muzeum
===	===	k?	===
</s>
</p>
<p>
<s>
Historické	historický	k2eAgNnSc1d1	historické
jádro	jádro	k1gNnSc1	jádro
města	město	k1gNnSc2	město
nabízí	nabízet	k5eAaImIp3nS	nabízet
velký	velký	k2eAgInSc1d1	velký
počet	počet	k1gInSc1	počet
galerií	galerie	k1gFnPc2	galerie
umění	umění	k1gNnPc2	umění
a	a	k8xC	a
muzeí	muzeum	k1gNnPc2	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Regionální	regionální	k2eAgNnSc1d1	regionální
muzeum	muzeum	k1gNnSc1	muzeum
v	v	k7c6	v
Českém	český	k2eAgInSc6d1	český
Krumlově	Krumlov	k1gInSc6	Krumlov
představuje	představovat	k5eAaImIp3nS	představovat
skrze	skrze	k?	skrze
nově	nově	k6eAd1	nově
zrekonstruovanou	zrekonstruovaný	k2eAgFnSc4d1	zrekonstruovaná
expozici	expozice	k1gFnSc4	expozice
historii	historie	k1gFnSc4	historie
města	město	k1gNnSc2	město
a	a	k8xC	a
regionu	region	k1gInSc2	region
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
nálezy	nález	k1gInPc7	nález
z	z	k7c2	z
archeologického	archeologický	k2eAgNnSc2d1	Archeologické
naleziště	naleziště	k1gNnSc2	naleziště
Dobrkovické	Dobrkovický	k2eAgFnSc2d1	Dobrkovická
jeskyně	jeskyně	k1gFnSc2	jeskyně
<g/>
.	.	kIx.	.
</s>
<s>
Egon	Egon	k1gMnSc1	Egon
Schiele	Schiel	k1gInSc2	Schiel
Art	Art	k1gMnSc1	Art
Centrum	centrum	k1gNnSc1	centrum
návštěvníkům	návštěvník	k1gMnPc3	návštěvník
nabízí	nabízet	k5eAaImIp3nS	nabízet
stálou	stálý	k2eAgFnSc4d1	stálá
expozici	expozice	k1gFnSc4	expozice
věnovanou	věnovaný	k2eAgFnSc4d1	věnovaná
dílu	díl	k1gInSc3	díl
a	a	k8xC	a
životu	život	k1gInSc3	život
nejslavnějšího	slavný	k2eAgMnSc2d3	nejslavnější
krumlovského	krumlovský	k2eAgMnSc2d1	krumlovský
residenta	resident	k1gMnSc2	resident
Egona	Egon	k1gMnSc2	Egon
Schieleho	Schiele	k1gMnSc2	Schiele
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
výstavami	výstava	k1gFnPc7	výstava
světoznámých	světoznámý	k2eAgMnPc2d1	světoznámý
malířů	malíř	k1gMnPc2	malíř
<g/>
,	,	kIx,	,
grafiků	grafik	k1gMnPc2	grafik
i	i	k8xC	i
sochařů	sochař	k1gMnPc2	sochař
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
hlavní	hlavní	k2eAgFnPc4d1	hlavní
památky	památka	k1gFnPc4	památka
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
i	i	k9	i
muzeum	muzeum	k1gNnSc1	muzeum
Fotoateliér	fotoateliér	k1gInSc1	fotoateliér
Seidel	Seidel	k1gMnSc1	Seidel
–	–	k?	–
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
dochovaných	dochovaný	k2eAgInPc2d1	dochovaný
historických	historický	k2eAgInPc2d1	historický
fotoateliérů	fotoateliér	k1gInPc2	fotoateliér
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
i	i	k9	i
městská	městský	k2eAgFnSc1d1	městská
synagoga	synagoga	k1gFnSc1	synagoga
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
lze	lze	k6eAd1	lze
zhlédnout	zhlédnout	k5eAaPmF	zhlédnout
výstavu	výstava	k1gFnSc4	výstava
věnovanou	věnovaný	k2eAgFnSc4d1	věnovaná
rodině	rodina	k1gFnSc3	rodina
Ignáce	Ignác	k1gMnSc2	Ignác
Spira	Spir	k1gMnSc2	Spir
<g/>
,	,	kIx,	,
zakladatele	zakladatel	k1gMnSc2	zakladatel
Větříňských	Větříňský	k2eAgFnPc2d1	Větříňský
papíren	papírna	k1gFnPc2	papírna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
také	také	k9	také
možné	možný	k2eAgNnSc1d1	možné
sestoupit	sestoupit	k5eAaPmF	sestoupit
do	do	k7c2	do
podzemních	podzemní	k2eAgFnPc2d1	podzemní
prostor	prostora	k1gFnPc2	prostora
bývalého	bývalý	k2eAgInSc2d1	bývalý
grafitového	grafitový	k2eAgInSc2d1	grafitový
dolu	dol	k1gInSc2	dol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Regionální	regionální	k2eAgNnSc1d1	regionální
muzeum	muzeum	k1gNnSc1	muzeum
v	v	k7c6	v
Českém	český	k2eAgInSc6d1	český
Krumlově	Krumlov	k1gInSc6	Krumlov
</s>
</p>
<p>
<s>
Egon	Egon	k1gMnSc1	Egon
Schiele	Schiel	k1gInSc2	Schiel
Art	Art	k1gFnSc2	Art
Centrum	centrum	k1gNnSc1	centrum
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
bývalého	bývalý	k2eAgInSc2d1	bývalý
Městského	městský	k2eAgInSc2d1	městský
pivovaru	pivovar	k1gInSc2	pivovar
</s>
</p>
<p>
<s>
Muzeum	muzeum	k1gNnSc1	muzeum
Fotoateliér	fotoateliér	k1gInSc1	fotoateliér
Seidel	Seidel	k1gMnSc1	Seidel
</s>
</p>
<p>
<s>
Muzeum	muzeum	k1gNnSc1	muzeum
vltavínů	vltavín	k1gInPc2	vltavín
</s>
</p>
<p>
<s>
Muzeum	muzeum	k1gNnSc1	muzeum
historických	historický	k2eAgInPc2d1	historický
motocyklů	motocykl	k1gInPc2	motocykl
</s>
</p>
<p>
<s>
Synagoga	synagoga	k1gFnSc1	synagoga
v	v	k7c6	v
Českém	český	k2eAgInSc6d1	český
Krumlově	Krumlov	k1gInSc6	Krumlov
</s>
</p>
<p>
<s>
Grafitový	grafitový	k2eAgInSc1d1	grafitový
důl	důl	k1gInSc1	důl
v	v	k7c6	v
Českém	český	k2eAgInSc6d1	český
Krumlově	Krumlov	k1gInSc6	Krumlov
</s>
</p>
<p>
<s>
Wax	Wax	k?	Wax
muzeum	muzeum	k1gNnSc1	muzeum
–	–	k?	–
výstava	výstava	k1gFnSc1	výstava
voskových	voskový	k2eAgFnPc2d1	vosková
figurín	figurína	k1gFnPc2	figurína
</s>
</p>
<p>
<s>
===	===	k?	===
Festivaly	festival	k1gInPc4	festival
===	===	k?	===
</s>
</p>
<p>
<s>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
během	během	k7c2	během
roku	rok	k1gInSc2	rok
dějištěm	dějiště	k1gNnSc7	dějiště
několika	několik	k4yIc2	několik
regionálních	regionální	k2eAgInPc2d1	regionální
a	a	k8xC	a
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
kulturních	kulturní	k2eAgFnPc2d1	kulturní
akcí	akce	k1gFnPc2	akce
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gInPc4	on
hudební	hudební	k2eAgInPc4d1	hudební
festivaly	festival	k1gInPc4	festival
MHF	MHF	kA	MHF
Český	český	k2eAgInSc1d1	český
Krumlov	Krumlov	k1gInSc1	Krumlov
nebo	nebo	k8xC	nebo
Festival	festival	k1gInSc1	festival
barokních	barokní	k2eAgNnPc2d1	barokní
umění	umění	k1gNnSc2	umění
odehrávající	odehrávající	k2eAgMnSc1d1	odehrávající
se	se	k3xPyFc4	se
v	v	k7c6	v
prostorách	prostora	k1gFnPc6	prostora
Zámeckého	zámecký	k2eAgNnSc2d1	zámecké
barokního	barokní	k2eAgNnSc2d1	barokní
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Historický	historický	k2eAgInSc1d1	historický
festival	festival	k1gInSc1	festival
oslavující	oslavující	k2eAgFnSc2d1	oslavující
dějiny	dějiny	k1gFnPc4	dějiny
města	město	k1gNnSc2	město
a	a	k8xC	a
Rožmberský	rožmberský	k2eAgInSc1d1	rožmberský
rod	rod	k1gInSc1	rod
–	–	k?	–
Slavnosti	slavnost	k1gFnSc2	slavnost
pětilisté	pětilistý	k2eAgFnSc2d1	pětilistá
růže	růž	k1gFnSc2	růž
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejdůležitějších	důležitý	k2eAgFnPc2d3	nejdůležitější
událostí	událost	k1gFnPc2	událost
města	město	k1gNnSc2	město
a	a	k8xC	a
regionu	region	k1gInSc2	region
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
hudební	hudební	k2eAgInSc1d1	hudební
festival	festival	k1gInSc1	festival
Český	český	k2eAgInSc1d1	český
Krumlov	Krumlov	k1gInSc1	Krumlov
</s>
</p>
<p>
<s>
Festival	festival	k1gInSc1	festival
barokních	barokní	k2eAgNnPc2d1	barokní
umění	umění	k1gNnPc2	umění
</s>
</p>
<p>
<s>
Slavnosti	slavnost	k1gFnPc1	slavnost
pětilisté	pětilistý	k2eAgFnSc2d1	pětilistá
růže	růž	k1gFnSc2	růž
</s>
</p>
<p>
<s>
Svatováclavské	svatováclavský	k2eAgFnPc1d1	Svatováclavská
slavnosti	slavnost	k1gFnPc1	slavnost
</s>
</p>
<p>
<s>
Festival	festival	k1gInSc1	festival
komorní	komorní	k2eAgFnSc2d1	komorní
hudby	hudba	k1gFnSc2	hudba
</s>
</p>
<p>
<s>
Rallye	rallye	k1gFnSc4	rallye
Český	český	k2eAgInSc4d1	český
Krumlov	Krumlov	k1gInSc4	Krumlov
</s>
</p>
<p>
<s>
Festival	festival	k1gInSc1	festival
vína	víno	k1gNnSc2	víno
Český	český	k2eAgInSc1d1	český
Krumlov	Krumlov	k1gInSc1	Krumlov
</s>
</p>
<p>
<s>
Adventní	adventní	k2eAgFnPc1d1	adventní
slavnosti	slavnost	k1gFnPc1	slavnost
</s>
</p>
<p>
<s>
Den	den	k1gInSc1	den
bez	bez	k7c2	bez
bariér	bariéra	k1gFnPc2	bariéra
Český	český	k2eAgInSc4d1	český
Krumlov	Krumlov	k1gInSc4	Krumlov
</s>
</p>
<p>
<s>
Dny	den	k1gInPc1	den
evropského	evropský	k2eAgNnSc2d1	Evropské
dědictví	dědictví	k1gNnSc2	dědictví
</s>
</p>
<p>
<s>
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
Ekofilm	ekofilm	k1gInSc1	ekofilm
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Chov	chov	k1gInSc1	chov
medvědů	medvěd	k1gMnPc2	medvěd
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
krumlovském	krumlovský	k2eAgInSc6d1	krumlovský
zámku	zámek	k1gInSc6	zámek
má	mít	k5eAaImIp3nS	mít
chov	chov	k1gInSc1	chov
medvědů	medvěd	k1gMnPc2	medvěd
tradici	tradice	k1gFnSc4	tradice
starou	starý	k2eAgFnSc4d1	stará
přes	přes	k7c4	přes
400	[number]	k4	400
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
medvědi	medvěd	k1gMnPc1	medvěd
v	v	k7c6	v
Krumlově	Krumlov	k1gInSc6	Krumlov
objevili	objevit	k5eAaPmAgMnP	objevit
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
chovem	chov	k1gInSc7	chov
chtěli	chtít	k5eAaImAgMnP	chtít
Rožmberkové	Rožmberkové	k?	Rožmberkové
demonstrovat	demonstrovat	k5eAaBmF	demonstrovat
svůj	svůj	k3xOyFgInSc4	svůj
údajný	údajný	k2eAgInSc4d1	údajný
původ	původ	k1gInSc4	původ
a	a	k8xC	a
příbuzenství	příbuzenství	k1gNnSc1	příbuzenství
s	s	k7c7	s
římským	římský	k2eAgInSc7d1	římský
rodem	rod	k1gInSc7	rod
Orsiniů	Orsini	k1gMnPc2	Orsini
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hradním	hradní	k2eAgInSc6d1	hradní
příkopu	příkop	k1gInSc6	příkop
mezi	mezi	k7c7	mezi
I.	I.	kA	I.
a	a	k8xC	a
II	II	kA	II
<g/>
.	.	kIx.	.
nádvořím	nádvoří	k1gNnSc7	nádvoří
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gNnSc1	jejich
chov	chov	k1gInSc1	chov
doložen	doložen	k2eAgInSc1d1	doložen
až	až	k6eAd1	až
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1700	[number]	k4	1700
a	a	k8xC	a
s	s	k7c7	s
určitými	určitý	k2eAgFnPc7d1	určitá
přestávkami	přestávka	k1gFnPc7	přestávka
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
chovají	chovat	k5eAaImIp3nP	chovat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
S	s	k7c7	s
krumlovskými	krumlovský	k2eAgMnPc7d1	krumlovský
medvědy	medvěd	k1gMnPc7	medvěd
také	také	k6eAd1	také
souvisí	souviset	k5eAaImIp3nS	souviset
slavnost	slavnost	k1gFnSc1	slavnost
místních	místní	k2eAgMnPc2d1	místní
spoluobčanů	spoluobčan	k1gMnPc2	spoluobčan
probíhající	probíhající	k2eAgInSc4d1	probíhající
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
na	na	k7c4	na
Štědrý	štědrý	k2eAgInSc4d1	štědrý
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Medvědí	medvědí	k2eAgFnPc4d1	medvědí
Vánoce	Vánoce	k1gFnPc4	Vánoce
<g/>
.	.	kIx.	.
</s>
<s>
Dopoledne	dopoledne	k1gNnPc1	dopoledne
mohou	moct	k5eAaImIp3nP	moct
děti	dítě	k1gFnPc1	dítě
i	i	k8xC	i
dospělí	dospělí	k1gMnPc1	dospělí
za	za	k7c2	za
dozoru	dozor	k1gInSc2	dozor
medvědářů	medvědář	k1gMnPc2	medvědář
sestoupit	sestoupit	k5eAaPmF	sestoupit
do	do	k7c2	do
hradního	hradní	k2eAgInSc2d1	hradní
příkopu	příkop	k1gInSc2	příkop
a	a	k8xC	a
vlastnoručně	vlastnoručně	k6eAd1	vlastnoručně
vyrobené	vyrobený	k2eAgFnPc4d1	vyrobená
pamlsky	pamlska	k1gFnPc4	pamlska
připevnit	připevnit	k5eAaPmF	připevnit
na	na	k7c4	na
velký	velký	k2eAgInSc4d1	velký
vánoční	vánoční	k2eAgInSc4d1	vánoční
stromeček	stromeček	k1gInSc4	stromeček
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
jsou	být	k5eAaImIp3nP	být
vypuštěni	vypuštěn	k2eAgMnPc1d1	vypuštěn
medvědi	medvěd	k1gMnPc1	medvěd
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
si	se	k3xPyFc3	se
na	na	k7c6	na
pamlscích	pamlsek	k1gInPc6	pamlsek
a	a	k8xC	a
také	také	k9	také
na	na	k7c6	na
vánočním	vánoční	k2eAgInSc6d1	vánoční
stromečku	stromeček	k1gInSc6	stromeček
za	za	k7c2	za
pozornosti	pozornost	k1gFnSc2	pozornost
všech	všecek	k3xTgMnPc2	všecek
přítomných	přítomný	k1gMnPc2	přítomný
pochutnávají	pochutnávat	k5eAaImIp3nP	pochutnávat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nyní	nyní	k6eAd1	nyní
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
chovají	chovat	k5eAaImIp3nP	chovat
celkem	celkem	k6eAd1	celkem
dva	dva	k4xCgMnPc1	dva
medvědi	medvěd	k1gMnPc1	medvěd
-	-	kIx~	-
Kateřina	Kateřina	k1gFnSc1	Kateřina
(	(	kIx(	(
<g/>
*	*	kIx~	*
3.1	[number]	k4	3.1
<g/>
.	.	kIx.	.
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
a	a	k8xC	a
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc2	Terezie
(	(	kIx(	(
<g/>
*	*	kIx~	*
21.12	[number]	k4	21.12
<g/>
.	.	kIx.	.
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Medvěd	medvěd	k1gMnSc1	medvěd
Vok	Vok	k1gMnSc1	Vok
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
pošel	pojít	k5eAaPmAgInS	pojít
12	[number]	k4	12
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
pojmenovaný	pojmenovaný	k2eAgMnSc1d1	pojmenovaný
po	po	k7c6	po
Petru	Petr	k1gMnSc6	Petr
Vokovi	Voka	k1gMnSc6	Voka
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2016	[number]	k4	2016
uspán	uspat	k5eAaPmNgMnS	uspat
kvůli	kvůli	k7c3	kvůli
zdravotnímu	zdravotní	k2eAgInSc3d1	zdravotní
stavu	stav	k1gInSc3	stav
a	a	k8xC	a
vysokému	vysoký	k2eAgInSc3d1	vysoký
věku	věk	k1gInSc3	věk
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgMnSc1	tento
medvěd	medvěd	k1gMnSc1	medvěd
byl	být	k5eAaImAgMnS	být
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
znám	znát	k5eAaImIp1nS	znát
jako	jako	k9	jako
otec	otec	k1gMnSc1	otec
tří	tři	k4xCgMnPc2	tři
medvědů	medvěd	k1gMnPc2	medvěd
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
hráli	hrát	k5eAaImAgMnP	hrát
v	v	k7c6	v
dětském	dětský	k2eAgInSc6d1	dětský
seriálu	seriál	k1gInSc6	seriál
Méďové	méďa	k1gMnPc1	méďa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Erby	erb	k1gInPc7	erb
<g/>
,	,	kIx,	,
znaky	znak	k1gInPc7	znak
a	a	k8xC	a
symboly	symbol	k1gInPc7	symbol
ve	v	k7c6	v
městě	město	k1gNnSc6	město
===	===	k?	===
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
lze	lze	k6eAd1	lze
téměř	téměř	k6eAd1	téměř
na	na	k7c6	na
každém	každý	k3xTgInSc6	každý
kroku	krok	k1gInSc6	krok
pozorovat	pozorovat	k5eAaImF	pozorovat
nejrůznější	různý	k2eAgFnPc4d3	nejrůznější
rodovou	rodový	k2eAgFnSc4d1	rodová
symboliku	symbolika	k1gFnSc4	symbolika
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
však	však	k9	však
vítkovská	vítkovský	k2eAgFnSc1d1	Vítkovská
pětilistá	pětilistý	k2eAgFnSc1d1	pětilistá
růže	růže	k1gFnSc1	růže
<g/>
.	.	kIx.	.
</s>
<s>
Pětilistou	pětilistý	k2eAgFnSc4d1	pětilistá
růži	růže	k1gFnSc4	růže
Rožmberků	Rožmberk	k1gMnPc2	Rožmberk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
byli	být	k5eAaImAgMnP	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
vítkovských	vítkovský	k2eAgInPc2d1	vítkovský
rozrodů	rozrod	k1gInPc2	rozrod
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
červenou	červený	k2eAgFnSc4d1	červená
růži	růže	k1gFnSc4	růže
na	na	k7c6	na
bílém	bílý	k2eAgNnSc6d1	bílé
poli	pole	k1gNnSc6	pole
<g/>
,	,	kIx,	,
můžeme	moct	k5eAaImIp1nP	moct
zahlédnout	zahlédnout	k5eAaPmF	zahlédnout
na	na	k7c6	na
velkém	velký	k2eAgNnSc6d1	velké
množství	množství	k1gNnSc6	množství
domů	dům	k1gInPc2	dům
ve	v	k7c6	v
Vnitřním	vnitřní	k2eAgNnSc6d1	vnitřní
městě	město	k1gNnSc6	město
i	i	k8xC	i
na	na	k7c6	na
Latránu	latrán	k1gInSc6	latrán
v	v	k7c6	v
nejrůznějších	různý	k2eAgFnPc6d3	nejrůznější
podobách	podoba	k1gFnPc6	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Ostatně	ostatně	k6eAd1	ostatně
všudypřítomnost	všudypřítomnost	k1gFnSc1	všudypřítomnost
tohoto	tento	k3xDgMnSc4	tento
ve	v	k7c6	v
městě	město	k1gNnSc6	město
symbolu	symbol	k1gInSc2	symbol
dala	dát	k5eAaPmAgFnS	dát
jméno	jméno	k1gNnSc4	jméno
i	i	k8xC	i
největším	veliký	k2eAgFnPc3d3	veliký
krumlovským	krumlovský	k2eAgFnPc3d1	Krumlovská
slavnostem	slavnost	k1gFnPc3	slavnost
–	–	k?	–
Slavnostem	slavnost	k1gFnPc3	slavnost
pětilisté	pětilistý	k2eAgFnSc2d1	pětilistá
růže	růž	k1gFnSc2	růž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mimo	mimo	k6eAd1	mimo
také	také	k9	také
velmi	velmi	k6eAd1	velmi
početný	početný	k2eAgInSc1d1	početný
symbol	symbol	k1gInSc1	symbol
Schwarzenberské	schwarzenberský	k2eAgFnSc2d1	Schwarzenberská
primogenitury	primogenitura	k1gFnSc2	primogenitura
v	v	k7c6	v
modro-bílém	modroílý	k2eAgNnSc6d1	modro-bílé
provedení	provedení	k1gNnSc6	provedení
s	s	k7c7	s
havranem	havran	k1gMnSc7	havran
klovajícím	klovající	k2eAgMnSc7d1	klovající
hlavu	hlava	k1gFnSc4	hlava
Turka	Turek	k1gMnSc4	Turek
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
návštěvníci	návštěvník	k1gMnPc1	návštěvník
pozorovat	pozorovat	k5eAaImF	pozorovat
bohaté	bohatý	k2eAgFnPc1d1	bohatá
renesanční	renesanční	k2eAgFnPc1d1	renesanční
fasády	fasáda	k1gFnPc1	fasáda
městských	městský	k2eAgInPc2d1	městský
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
až	až	k9	až
s	s	k7c7	s
nebývale	nebývale	k6eAd1	nebývale
komickým	komický	k2eAgInSc7d1	komický
nádechem	nádech	k1gInSc7	nádech
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
městské	městský	k2eAgFnSc6d1	městská
radnici	radnice	k1gFnSc6	radnice
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
vyobrazeny	vyobrazit	k5eAaPmNgInP	vyobrazit
znaky	znak	k1gInPc7	znak
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
Českého	český	k2eAgNnSc2d1	české
království	království	k1gNnSc2	království
a	a	k8xC	a
rodové	rodový	k2eAgInPc4d1	rodový
erby	erb	k1gInPc4	erb
Eggenbergů	Eggenberg	k1gMnPc2	Eggenberg
a	a	k8xC	a
Schwarzenbergů	Schwarzenberg	k1gMnPc2	Schwarzenberg
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Věžný	věžný	k1gMnSc1	věžný
Nácl	Nácl	k1gMnSc1	Nácl
a	a	k8xC	a
krumlovská	krumlovský	k2eAgFnSc1d1	Krumlovská
fanfára	fanfára	k1gFnSc1	fanfára
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
bydlel	bydlet	k5eAaImAgMnS	bydlet
na	na	k7c6	na
zámecké	zámecký	k2eAgFnSc6d1	zámecká
věži	věž	k1gFnSc6	věž
nad	nad	k7c7	nad
městem	město	k1gNnSc7	město
věžný	věžný	k1gMnSc1	věžný
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
od	od	k7c2	od
osmi	osm	k4xCc2	osm
hodin	hodina	k1gFnPc2	hodina
ráno	ráno	k6eAd1	ráno
do	do	k7c2	do
čtyř	čtyři	k4xCgInPc2	čtyři
odpoledne	odpoledne	k6eAd1	odpoledne
v	v	k7c6	v
letních	letní	k2eAgInPc6d1	letní
měsících	měsíc	k1gInPc6	měsíc
zatroubil	zatroubit	k5eAaPmAgInS	zatroubit
na	na	k7c4	na
trubku	trubka	k1gFnSc4	trubka
každou	každý	k3xTgFnSc4	každý
hodinu	hodina	k1gFnSc4	hodina
krumlovskou	krumlovský	k2eAgFnSc4d1	Krumlovská
fanfáru	fanfára	k1gFnSc4	fanfára
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tradice	tradice	k1gFnSc2	tradice
se	se	k3xPyFc4	se
mu	on	k3xPp3gInSc3	on
říká	říkat	k5eAaImIp3nS	říkat
Věžnej	Věžnej	k1gFnSc4	Věžnej
Nácl	Nácla	k1gFnPc2	Nácla
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
nejznámějšího	známý	k2eAgMnSc2d3	nejznámější
krumlovského	krumlovský	k2eAgMnSc2d1	krumlovský
věžného	věžný	k1gMnSc2	věžný
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
povinnosti	povinnost	k1gFnSc2	povinnost
troubit	troubit	k5eAaImF	troubit
fanfáru	fanfára	k1gFnSc4	fanfára
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
udávat	udávat	k5eAaImF	udávat
čas	čas	k1gInSc4	čas
a	a	k8xC	a
případně	případně	k6eAd1	případně
ohlašovat	ohlašovat	k5eAaImF	ohlašovat
požár	požár	k1gInSc4	požár
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
mohl	moct	k5eAaImAgMnS	moct
věžný	věžný	k1gMnSc1	věžný
díky	díky	k7c3	díky
velkému	velký	k2eAgInSc3d1	velký
rozhledu	rozhled	k1gInSc3	rozhled
z	z	k7c2	z
věže	věž	k1gFnSc2	věž
odhadovat	odhadovat	k5eAaImF	odhadovat
blížící	blížící	k2eAgFnPc4d1	blížící
se	se	k3xPyFc4	se
změny	změna	k1gFnPc4	změna
počasí	počasí	k1gNnSc2	počasí
<g/>
.	.	kIx.	.
</s>
<s>
Měšťanům	Měšťan	k1gMnPc3	Měšťan
pak	pak	k6eAd1	pak
předpověď	předpověď	k1gFnSc1	předpověď
z	z	k7c2	z
věže	věž	k1gFnSc2	věž
sděloval	sdělovat	k5eAaImAgMnS	sdělovat
pomocí	pomocí	k7c2	pomocí
barevných	barevný	k2eAgInPc2d1	barevný
praporků	praporek	k1gInPc2	praporek
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
úřad	úřad	k1gInSc1	úřad
věžného	věžný	k1gMnSc2	věžný
zanikl	zaniknout	k5eAaPmAgInS	zaniknout
<g/>
,	,	kIx,	,
dodnes	dodnes	k6eAd1	dodnes
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
věže	věž	k1gFnSc2	věž
hrána	hrát	k5eAaImNgFnS	hrát
krumlovská	krumlovský	k2eAgFnSc1d1	Krumlovská
fanfára	fanfára	k1gFnSc1	fanfára
každé	každý	k3xTgFnPc4	každý
tři	tři	k4xCgFnPc4	tři
hodiny	hodina	k1gFnPc4	hodina
od	od	k7c2	od
deváté	devátý	k4xOgFnSc2	devátý
hodiny	hodina	k1gFnSc2	hodina
ranní	ranní	k2eAgInSc4d1	ranní
pomocí	pomocí	k7c2	pomocí
moderní	moderní	k2eAgFnSc2d1	moderní
techniky	technika	k1gFnSc2	technika
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
tzv.	tzv.	kA	tzv.
Věžném	věžný	k1gMnSc6	věžný
Náclovi	Nácl	k1gMnSc6	Nácl
dodnes	dodnes	k6eAd1	dodnes
koluje	kolovat	k5eAaImIp3nS	kolovat
dětská	dětský	k2eAgFnSc1d1	dětská
říkanka	říkanka	k1gFnSc1	říkanka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Hřbitov	hřbitov	k1gInSc4	hřbitov
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
zdejším	zdejší	k2eAgInSc6d1	zdejší
hřbitově	hřbitov	k1gInSc6	hřbitov
v	v	k7c6	v
místní	místní	k2eAgFnSc6d1	místní
části	část	k1gFnSc6	část
Horní	horní	k2eAgFnSc1d1	horní
Brána	brána	k1gFnSc1	brána
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
kaple	kaple	k1gFnSc1	kaple
Bolestné	bolestný	k2eAgFnSc2d1	bolestná
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
též	též	k9	též
udržované	udržovaný	k2eAgInPc4d1	udržovaný
německé	německý	k2eAgInPc4d1	německý
hroby	hrob	k1gInPc4	hrob
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
ně	on	k3xPp3gInPc4	on
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
hrob	hrob	k1gInSc1	hrob
ředitele	ředitel	k1gMnSc2	ředitel
schwarzenberského	schwarzenberský	k2eAgNnSc2d1	Schwarzenberské
panství	panství	k1gNnSc2	panství
Antona	Anton	k1gMnSc2	Anton
Wegwarta	Wegwart	k1gMnSc2	Wegwart
(	(	kIx(	(
<g/>
1838	[number]	k4	1838
<g/>
–	–	k?	–
<g/>
1898	[number]	k4	1898
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
gymnaziálního	gymnaziální	k2eAgMnSc2d1	gymnaziální
profesora	profesor	k1gMnSc2	profesor
Gregora	Gregor	k1gMnSc2	Gregor
Hetteggera	Hettegger	k1gMnSc2	Hettegger
(	(	kIx(	(
<g/>
1869	[number]	k4	1869
<g/>
–	–	k?	–
<g/>
1928	[number]	k4	1928
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Odpočívají	odpočívat	k5eAaImIp3nP	odpočívat
tu	tu	k6eAd1	tu
ale	ale	k9	ale
také	také	k9	také
třeba	třeba	k6eAd1	třeba
akademický	akademický	k2eAgMnSc1d1	akademický
sochař	sochař	k1gMnSc1	sochař
Stanislav	Stanislav	k1gMnSc1	Stanislav
Zadražil	Zadražil	k1gMnSc1	Zadražil
(	(	kIx(	(
<g/>
1931	[number]	k4	1931
<g/>
–	–	k?	–
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
či	či	k8xC	či
zpěvák	zpěvák	k1gMnSc1	zpěvák
Petr	Petr	k1gMnSc1	Petr
Muk	muka	k1gFnPc2	muka
(	(	kIx(	(
<g/>
1965	[number]	k4	1965
<g/>
–	–	k?	–
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pověsti	pověst	k1gFnSc6	pověst
==	==	k?	==
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
dodnes	dodnes	k6eAd1	dodnes
traduje	tradovat	k5eAaImIp3nS	tradovat
mnoho	mnoho	k4c1	mnoho
nejrůznějších	různý	k2eAgFnPc2d3	nejrůznější
pověstí	pověst	k1gFnPc2	pověst
týkajících	týkající	k2eAgFnPc2d1	týkající
se	se	k3xPyFc4	se
osoby	osoba	k1gFnPc1	osoba
tajemné	tajemný	k2eAgFnSc2d1	tajemná
dobrodějky	dobrodějka	k1gFnSc2	dobrodějka
Annabelly	Annabella	k1gFnSc2	Annabella
<g/>
,	,	kIx,	,
vědmy	vědma	k1gFnSc2	vědma
a	a	k8xC	a
léčitelky	léčitelka	k1gFnSc2	léčitelka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
žila	žít	k5eAaImAgFnS	žít
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
Dlouhé	Dlouhé	k2eAgFnSc6d1	Dlouhé
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc4	její
světnici	světnice	k1gFnSc4	světnice
s	s	k7c7	s
velkým	velký	k2eAgNnSc7d1	velké
ohništěm	ohniště	k1gNnSc7	ohniště
navštívilo	navštívit	k5eAaPmAgNnS	navštívit
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
mnoho	mnoho	k4c4	mnoho
místních	místní	k2eAgInPc2d1	místní
i	i	k8xC	i
lidí	člověk	k1gMnPc2	člověk
z	z	k7c2	z
daleka	daleko	k1gNnSc2	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Říká	říkat	k5eAaImIp3nS	říkat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
každému	každý	k3xTgMnSc3	každý
dobrému	dobrý	k2eAgMnSc3d1	dobrý
člověku	člověk	k1gMnSc3	člověk
vždy	vždy	k6eAd1	vždy
pomohla	pomoct	k5eAaPmAgFnS	pomoct
a	a	k8xC	a
ulevila	ulevit	k5eAaPmAgFnS	ulevit
od	od	k7c2	od
jeho	jeho	k3xOp3gInPc2	jeho
problémů	problém	k1gInPc2	problém
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
kdy	kdy	k6eAd1	kdy
a	a	k8xC	a
jak	jak	k6eAd1	jak
Annabella	Annabella	k1gFnSc1	Annabella
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
Annabelly	Annabella	k1gFnPc4	Annabella
z	z	k7c2	z
města	město	k1gNnSc2	město
odešly	odejít	k5eAaPmAgFnP	odejít
se	se	k3xPyFc4	se
traduje	tradovat	k5eAaImIp3nS	tradovat
více	hodně	k6eAd2	hodně
příběhů	příběh	k1gInPc2	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
světnice	světnice	k1gFnSc1	světnice
<g/>
,	,	kIx,	,
o	o	k7c6	o
níž	jenž	k3xRgFnSc6	jenž
<g/>
,	,	kIx,	,
kde	kde	k9	kde
se	se	k3xPyFc4	se
nacházela	nacházet	k5eAaImAgFnS	nacházet
<g/>
,	,	kIx,	,
víme	vědět	k5eAaImIp1nP	vědět
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
i	i	k9	i
se	s	k7c7	s
slovutným	slovutný	k2eAgNnSc7d1	slovutné
ohništěm	ohniště	k1gNnSc7	ohniště
zničena	zničit	k5eAaPmNgFnS	zničit
krátce	krátce	k6eAd1	krátce
po	po	k7c4	po
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgInPc4d1	světový
válce	válec	k1gInPc4	válec
při	při	k7c6	při
radikálních	radikální	k2eAgFnPc6d1	radikální
přestavbách	přestavba	k1gFnPc6	přestavba
domů	domů	k6eAd1	domů
v	v	k7c6	v
Dlouhé	Dlouhé	k2eAgFnSc6d1	Dlouhé
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
měly	mít	k5eAaImAgFnP	mít
kdysi	kdysi	k6eAd1	kdysi
nacházet	nacházet	k5eAaImF	nacházet
dvě	dva	k4xCgFnPc4	dva
podivné	podivný	k2eAgFnPc4d1	podivná
kamenné	kamenný	k2eAgFnPc4d1	kamenná
sochy	socha	k1gFnPc4	socha
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
přitahovaly	přitahovat	k5eAaImAgFnP	přitahovat
kolemjdoucí	kolemjdoucí	k2eAgFnPc1d1	kolemjdoucí
<g/>
,	,	kIx,	,
by	by	kYmCp3nP	by
i	i	k9	i
ty	ten	k3xDgMnPc4	ten
nejpočestnější	počestný	k2eAgMnPc4d3	nejpočestnější
poutníky	poutník	k1gMnPc4	poutník
natolik	natolik	k6eAd1	natolik
zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
městští	městský	k2eAgMnPc1d1	městský
radní	radní	k1gMnPc1	radní
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
sochy	socha	k1gFnPc4	socha
odstranit	odstranit	k5eAaPmF	odstranit
<g/>
.	.	kIx.	.
</s>
<s>
Jámy	jáma	k1gFnPc1	jáma
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
po	po	k7c6	po
oněch	onen	k3xDgInPc6	onen
kamenech	kámen	k1gInPc6	kámen
<g/>
,	,	kIx,	,
jimž	jenž	k3xRgInPc3	jenž
se	se	k3xPyFc4	se
říkalo	říkat	k5eAaImAgNnS	říkat
Kamenní	kamenný	k2eAgMnPc1d1	kamenný
bratři	bratr	k1gMnPc1	bratr
<g/>
,	,	kIx,	,
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
tak	tak	k9	tak
hluboké	hluboký	k2eAgFnPc1d1	hluboká
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
je	být	k5eAaImIp3nS	být
po	po	k7c4	po
několik	několik	k4yIc4	několik
dnů	den	k1gInPc2	den
nedařilo	dařit	k5eNaImAgNnS	dařit
zasypat	zasypat	k5eAaPmF	zasypat
<g/>
.	.	kIx.	.
</s>
<s>
Bratry	bratr	k1gMnPc4	bratr
pak	pak	k6eAd1	pak
konšelé	konšel	k1gMnPc1	konšel
nechali	nechat	k5eAaPmAgMnP	nechat
odklidit	odklidit	k5eAaPmF	odklidit
do	do	k7c2	do
stodoly	stodola	k1gFnSc2	stodola
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
z	z	k7c2	z
té	ten	k3xDgFnSc2	ten
během	během	k7c2	během
nocí	noc	k1gFnPc2	noc
vycházelo	vycházet	k5eAaImAgNnS	vycházet
tajemné	tajemný	k2eAgNnSc4d1	tajemné
světlo	světlo	k1gNnSc4	světlo
a	a	k8xC	a
ty	ten	k3xDgInPc4	ten
nejroztodivnější	roztodivný	k2eAgInPc4d3	nejroztodivnější
zvuky	zvuk	k1gInPc4	zvuk
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
děsilo	děsit	k5eAaImAgNnS	děsit
místního	místní	k2eAgMnSc4d1	místní
šafáře	šafář	k1gMnSc4	šafář
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
sochy	socha	k1gFnSc2	socha
raději	rád	k6eAd2	rád
opět	opět	k6eAd1	opět
postavit	postavit	k5eAaPmF	postavit
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
ale	ale	k8xC	ale
daleko	daleko	k6eAd1	daleko
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tom	ten	k3xDgInSc6	ten
se	se	k3xPyFc4	se
však	však	k9	však
začaly	začít	k5eAaPmAgFnP	začít
u	u	k7c2	u
kamenů	kámen	k1gInPc2	kámen
opět	opět	k6eAd1	opět
scházet	scházet	k5eAaImF	scházet
zástupy	zástup	k1gInPc4	zástup
poutníků	poutník	k1gMnPc2	poutník
a	a	k8xC	a
městské	městský	k2eAgFnSc6d1	městská
radě	rada	k1gFnSc6	rada
došla	dojít	k5eAaPmAgFnS	dojít
trpělivost	trpělivost	k1gFnSc1	trpělivost
<g/>
.	.	kIx.	.
</s>
<s>
Kameny	kámen	k1gInPc1	kámen
tak	tak	k9	tak
byly	být	k5eAaImAgInP	být
znovu	znovu	k6eAd1	znovu
vytrženy	vytrhnout	k5eAaPmNgInP	vytrhnout
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
naloženy	naložen	k2eAgFnPc1d1	naložena
na	na	k7c4	na
povoz	povoz	k1gInSc4	povoz
a	a	k8xC	a
odvezeny	odvezen	k2eAgFnPc4d1	odvezena
neznámo	neznámo	k6eAd1	neznámo
kam	kam	k6eAd1	kam
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
vyprávění	vyprávění	k1gNnSc2	vyprávění
<g/>
,	,	kIx,	,
když	když	k8xS	když
povoz	povoz	k1gInSc1	povoz
s	s	k7c7	s
kameny	kámen	k1gInPc7	kámen
projížděl	projíždět	k5eAaImAgInS	projíždět
městy	město	k1gNnPc7	město
a	a	k8xC	a
vesnicemi	vesnice	k1gFnPc7	vesnice
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgFnPc4	všechen
tamní	tamní	k2eAgInPc1d1	tamní
zvony	zvon	k1gInPc1	zvon
kostelů	kostel	k1gInPc2	kostel
a	a	k8xC	a
kapliček	kaplička	k1gFnPc2	kaplička
se	se	k3xPyFc4	se
samy	sám	k3xTgFnPc1	sám
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
rozezněly	rozeznět	k5eAaPmAgFnP	rozeznět
na	na	k7c4	na
pozdrav	pozdrav	k1gInSc1	pozdrav
kamenným	kamenný	k2eAgMnPc3d1	kamenný
bratřím	bratr	k1gMnPc3	bratr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
pověsti	pověst	k1gFnSc2	pověst
stával	stávat	k5eAaImAgInS	stávat
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
dnešního	dnešní	k2eAgInSc2d1	dnešní
Hrádku	Hrádok	k1gInSc2	Hrádok
klášter	klášter	k1gInSc1	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
klášter	klášter	k1gInSc1	klášter
zanikl	zaniknout	k5eAaPmAgInS	zaniknout
<g/>
,	,	kIx,	,
na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
místě	místo	k1gNnSc6	místo
se	se	k3xPyFc4	se
rozkládala	rozkládat	k5eAaImAgFnS	rozkládat
louka	louka	k1gFnSc1	louka
a	a	k8xC	a
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
býk	býk	k1gMnSc1	býk
vyryl	vyrýt	k5eAaPmAgMnS	vyrýt
rohem	roh	k1gInSc7	roh
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
veliký	veliký	k2eAgInSc4d1	veliký
zvon	zvon	k1gInSc4	zvon
<g/>
.	.	kIx.	.
</s>
<s>
Zvonu	zvon	k1gInSc3	zvon
se	se	k3xPyFc4	se
potom	potom	k6eAd1	potom
navždy	navždy	k6eAd1	navždy
říkalo	říkat	k5eAaImAgNnS	říkat
Býčí	býčí	k2eAgInSc4d1	býčí
zvon	zvon	k1gInSc4	zvon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jiná	jiný	k2eAgFnSc1d1	jiná
pověst	pověst	k1gFnSc1	pověst
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
zvonu	zvon	k1gInSc6	zvon
z	z	k7c2	z
kapličky	kaplička	k1gFnSc2	kaplička
sv.	sv.	kA	sv.
Jiří	Jiří	k1gMnSc1	Jiří
v	v	k7c6	v
Libomyšli	Libomyšle	k1gFnSc6	Libomyšle
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
na	na	k7c6	na
konci	konec	k1gInSc6	konec
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
kaplička	kaplička	k1gFnSc1	kaplička
velmi	velmi	k6eAd1	velmi
poškozena	poškozen	k2eAgFnSc1d1	poškozena
<g/>
,	,	kIx,	,
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
se	se	k3xPyFc4	se
lidé	člověk	k1gMnPc1	člověk
zvon	zvon	k1gInSc4	zvon
prodat	prodat	k5eAaPmF	prodat
do	do	k7c2	do
Lochovic	Lochovice	k1gFnPc2	Lochovice
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
paní	paní	k1gFnSc1	paní
z	z	k7c2	z
Lochovic	Lochovice	k1gFnPc2	Lochovice
prohrála	prohrát	k5eAaPmAgFnS	prohrát
zvon	zvon	k1gInSc4	zvon
v	v	k7c6	v
kartách	karta	k1gFnPc6	karta
s	s	k7c7	s
krumlovskými	krumlovský	k2eAgInPc7d1	krumlovský
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
zvon	zvon	k1gInSc1	zvon
skončil	skončit	k5eAaPmAgInS	skončit
v	v	k7c6	v
Českém	český	k2eAgInSc6d1	český
Krumlově	Krumlov	k1gInSc6	Krumlov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
je	být	k5eAaImIp3nS	být
jméno	jméno	k1gNnSc4	jméno
Latrán	latrán	k1gInSc1	latrán
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
od	od	k7c2	od
umístění	umístění	k1gNnSc2	umístění
čtvrti	čtvrt	k1gFnSc2	čtvrt
v	v	k7c6	v
podhradí	podhradí	k1gNnSc6	podhradí
<g/>
,	,	kIx,	,
dětem	dítě	k1gFnPc3	dítě
se	se	k3xPyFc4	se
často	často	k6eAd1	často
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
příběh	příběh	k1gInSc1	příběh
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
dávných	dávný	k2eAgFnPc6d1	dávná
dobách	doba	k1gFnPc6	doba
v	v	k7c6	v
místních	místní	k2eAgFnPc6d1	místní
hlubokých	hluboký	k2eAgFnPc6d1	hluboká
lesích	lese	k1gFnPc6	lese
žili	žít	k5eAaImAgMnP	žít
zlí	zlý	k2eAgMnPc1d1	zlý
lapkové	lapka	k1gMnPc1	lapka
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
těžce	těžce	k6eAd1	těžce
soužili	soužit	k5eAaImAgMnP	soužit
karavany	karavan	k1gInPc4	karavan
a	a	k8xC	a
nebohé	nebohý	k2eAgMnPc4d1	nebohý
obchodníky	obchodník	k1gMnPc4	obchodník
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
putovali	putovat	k5eAaImAgMnP	putovat
přes	přes	k7c4	přes
zdejší	zdejší	k2eAgInSc4d1	zdejší
brody	brod	k1gInPc4	brod
přes	přes	k7c4	přes
řeku	řeka	k1gFnSc4	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
však	však	k9	však
udatný	udatný	k2eAgMnSc1d1	udatný
rytíř	rytíř	k1gMnSc1	rytíř
Vítek	Vítek	k1gMnSc1	Vítek
jejich	jejich	k3xOp3gNnSc4	jejich
doupě	doupě	k1gNnSc4	doupě
nalezl	naleznout	k5eAaPmAgInS	naleznout
a	a	k8xC	a
do	do	k7c2	do
základů	základ	k1gInPc2	základ
je	být	k5eAaImIp3nS	být
vypálil	vypálit	k5eAaPmAgMnS	vypálit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
kamenném	kamenný	k2eAgInSc6d1	kamenný
ostrohu	ostroh	k1gInSc6	ostroh
si	se	k3xPyFc3	se
pak	pak	k6eAd1	pak
postavil	postavit	k5eAaPmAgMnS	postavit
svůj	svůj	k3xOyFgInSc4	svůj
mocný	mocný	k2eAgInSc4d1	mocný
krumlovský	krumlovský	k2eAgInSc4d1	krumlovský
hrad	hrad	k1gInSc4	hrad
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
zdejší	zdejší	k2eAgInPc4d1	zdejší
lesy	les	k1gInPc4	les
a	a	k8xC	a
brody	brod	k1gInPc4	brod
již	již	k9	již
na	na	k7c4	na
vždy	vždy	k6eAd1	vždy
chránit	chránit	k5eAaImF	chránit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nově	nově	k6eAd1	nově
založeném	založený	k2eAgNnSc6d1	založené
městě	město	k1gNnSc6	město
v	v	k7c6	v
podhradí	podhradí	k1gNnSc6	podhradí
pak	pak	k6eAd1	pak
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
první	první	k4xOgFnSc1	první
ulice	ulice	k1gFnSc1	ulice
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
ku	k	k7c3	k
připomínce	připomínka	k1gFnSc3	připomínka
vzniku	vznik	k1gInSc2	vznik
hradu	hrad	k1gInSc2	hrad
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
na	na	k7c4	na
paměť	paměť	k1gFnSc4	paměť
oněch	onen	k3xDgMnPc2	onen
lotrů	lotr	k1gMnPc2	lotr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
březích	břeh	k1gInPc6	břeh
Vltavy	Vltava	k1gFnSc2	Vltava
podle	podle	k7c2	podle
tradice	tradice	k1gFnSc2	tradice
žijí	žít	k5eAaImIp3nP	žít
mnozí	mnohý	k2eAgMnPc1d1	mnohý
skřítci	skřítek	k1gMnPc1	skřítek
a	a	k8xC	a
vodníci	vodník	k1gMnPc1	vodník
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
si	se	k3xPyFc3	se
stráží	strážit	k5eAaImIp3nP	strážit
svoje	svůj	k3xOyFgNnPc4	svůj
perlorodky	perlorodka	k1gFnPc4	perlorodka
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
jejichž	jejichž	k3xOyRp3gInSc4	jejichž
chov	chov	k1gInSc4	chov
a	a	k8xC	a
kvalitu	kvalita	k1gFnSc4	kvalita
byl	být	k5eAaImAgInS	být
Krumlov	Krumlov	k1gInSc1	Krumlov
kdysi	kdysi	k6eAd1	kdysi
znám	znám	k2eAgInSc1d1	znám
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Osobnosti	osobnost	k1gFnPc1	osobnost
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Rodáci	rodák	k1gMnPc5	rodák
===	===	k?	===
</s>
</p>
<p>
<s>
Záviš	Záviš	k1gMnSc1	Záviš
z	z	k7c2	z
Falkenštejna	Falkenštejn	k1gInSc2	Falkenštejn
(	(	kIx(	(
<g/>
1250	[number]	k4	1250
<g/>
–	–	k?	–
<g/>
1290	[number]	k4	1290
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
představitel	představitel	k1gMnSc1	představitel
rodu	rod	k1gInSc2	rod
pánů	pan	k1gMnPc2	pan
z	z	k7c2	z
Krumlova	Krumlov	k1gInSc2	Krumlov
a	a	k8xC	a
přední	přední	k2eAgMnSc1d1	přední
Vítkovec	Vítkovec	k1gMnSc1	Vítkovec
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
z	z	k7c2	z
Krumlova	Krumlov	k1gInSc2	Krumlov
(	(	kIx(	(
<g/>
1350	[number]	k4	1350
<g/>
??	??	k?	??
<g/>
–	–	k?	–
<g/>
1410	[number]	k4	1410
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stavitel	stavitel	k1gMnSc1	stavitel
</s>
</p>
<p>
<s>
Štěpán	Štěpán	k1gMnSc1	Štěpán
z	z	k7c2	z
Krumlova	Krumlov	k1gInSc2	Krumlov
(	(	kIx(	(
<g/>
1400	[number]	k4	1400
<g/>
–	–	k?	–
<g/>
1461	[number]	k4	1461
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vrchní	vrchní	k2eAgMnSc1d1	vrchní
stavitel	stavitel	k1gMnSc1	stavitel
Solnohradského	solnohradský	k2eAgNnSc2d1	solnohradský
arcibiskupství	arcibiskupství	k1gNnSc2	arcibiskupství
</s>
</p>
<p>
<s>
Václav	Václav	k1gMnSc1	Václav
z	z	k7c2	z
Krumlova	Krumlov	k1gInSc2	Krumlov
(	(	kIx(	(
<g/>
1420	[number]	k4	1420
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
–	–	k?	–
<g/>
1460	[number]	k4	1460
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
administrátor	administrátor	k1gMnSc1	administrátor
pražského	pražský	k2eAgNnSc2d1	Pražské
arcibiskupství	arcibiskupství	k1gNnSc2	arcibiskupství
</s>
</p>
<p>
<s>
Václav	Václav	k1gMnSc1	Václav
z	z	k7c2	z
Rovného	rovný	k2eAgInSc2d1	rovný
(	(	kIx(	(
<g/>
1448	[number]	k4	1448
<g/>
–	–	k?	–
<g/>
1531	[number]	k4	1531
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
humanista	humanista	k1gMnSc1	humanista
a	a	k8xC	a
kancléř	kancléř	k1gMnSc1	kancléř
Rožmberského	rožmberský	k2eAgNnSc2d1	rožmberské
dominia	dominion	k1gNnSc2	dominion
</s>
</p>
<p>
<s>
Martin	Martin	k1gMnSc1	Martin
Behaim	Behaim	k1gMnSc1	Behaim
(	(	kIx(	(
<g/>
1459	[number]	k4	1459
<g/>
–	–	k?	–
<g/>
1506	[number]	k4	1506
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
geograf	geograf	k1gMnSc1	geograf
a	a	k8xC	a
kartograf	kartograf	k1gMnSc1	kartograf
<g/>
,	,	kIx,	,
tvůrce	tvůrce	k1gMnSc1	tvůrce
nejstaršího	starý	k2eAgInSc2d3	nejstarší
dochovaného	dochovaný	k2eAgInSc2d1	dochovaný
glóbu	glóbus	k1gInSc2	glóbus
na	na	k7c6	na
světě	svět	k1gInSc6	svět
</s>
</p>
<p>
<s>
Kryštof	Kryštof	k1gMnSc1	Kryštof
Hecyrus	Hecyrus	k1gMnSc1	Hecyrus
(	(	kIx(	(
<g/>
1520	[number]	k4	1520
<g/>
–	–	k?	–
<g/>
1593	[number]	k4	1593
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
</s>
</p>
<p>
<s>
Petr	Petr	k1gMnSc1	Petr
Vok	Vok	k1gMnSc1	Vok
z	z	k7c2	z
Rožmberka	Rožmberk	k1gInSc2	Rožmberk
(	(	kIx(	(
<g/>
1539	[number]	k4	1539
<g/>
–	–	k?	–
<g/>
1611	[number]	k4	1611
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgMnSc1d1	poslední
vladař	vladař	k1gMnSc1	vladař
domu	dům	k1gInSc2	dům
rožmberského	rožmberský	k2eAgNnSc2d1	rožmberské
</s>
</p>
<p>
<s>
Jakub	Jakub	k1gMnSc1	Jakub
Hořčický	Hořčický	k2eAgMnSc1d1	Hořčický
z	z	k7c2	z
Tepence	Tepence	k1gFnSc2	Tepence
(	(	kIx(	(
<g/>
1575	[number]	k4	1575
<g/>
–	–	k?	–
<g/>
1622	[number]	k4	1622
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
alchymista	alchymista	k1gMnSc1	alchymista
a	a	k8xC	a
lékař	lékař	k1gMnSc1	lékař
Rudolfa	Rudolf	k1gMnSc2	Rudolf
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
František	František	k1gMnSc1	František
Fortin	Fortin	k1gMnSc1	Fortin
(	(	kIx(	(
<g/>
1727	[number]	k4	1727
<g/>
–	–	k?	–
<g/>
1762	[number]	k4	1762
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
architekt	architekt	k1gMnSc1	architekt
</s>
</p>
<p>
<s>
Václav	Václav	k1gMnSc1	Václav
Jan	Jan	k1gMnSc1	Jan
Frierenberger	Frierenberger	k1gMnSc1	Frierenberger
(	(	kIx(	(
<g/>
1759	[number]	k4	1759
<g/>
–	–	k?	–
<g/>
1823	[number]	k4	1823
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
generál	generál	k1gMnSc1	generál
z	z	k7c2	z
období	období	k1gNnSc2	období
napoleonských	napoleonský	k2eAgFnPc2d1	napoleonská
válek	válka	k1gFnPc2	válka
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Karel	Karel	k1gMnSc1	Karel
Ambrož	Ambrož	k1gMnSc1	Ambrož
(	(	kIx(	(
<g/>
1759	[number]	k4	1759
<g/>
–	–	k?	–
<g/>
1822	[number]	k4	1822
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
operní	operní	k2eAgMnSc1d1	operní
zpěvák	zpěvák	k1gMnSc1	zpěvák
</s>
</p>
<p>
<s>
Felix	Felix	k1gMnSc1	Felix
princ	princ	k1gMnSc1	princ
ze	z	k7c2	z
Schwarzenbergu	Schwarzenberg	k1gInSc2	Schwarzenberg
(	(	kIx(	(
<g/>
1800	[number]	k4	1800
<g/>
–	–	k?	–
<g/>
1852	[number]	k4	1852
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rakouský	rakouský	k2eAgMnSc1d1	rakouský
ministerský	ministerský	k2eAgMnSc1d1	ministerský
předseda	předseda	k1gMnSc1	předseda
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Andreas	Andreas	k1gMnSc1	Andreas
Huschak	Huschak	k1gMnSc1	Huschak
(	(	kIx(	(
<g/>
1804	[number]	k4	1804
<g/>
–	–	k?	–
<g/>
1905	[number]	k4	1905
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Pták	pták	k1gMnSc1	pták
(	(	kIx(	(
<g/>
1808	[number]	k4	1808
<g/>
–	–	k?	–
<g/>
1860	[number]	k4	1860
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hudebník	hudebník	k1gMnSc1	hudebník
a	a	k8xC	a
řezbář	řezbář	k1gMnSc1	řezbář
</s>
</p>
<p>
<s>
Franz	Franz	k1gMnSc1	Franz
Khemeter	Khemeter	k1gMnSc1	Khemeter
(	(	kIx(	(
<g/>
1845	[number]	k4	1845
<g/>
–	–	k?	–
<g/>
1924	[number]	k4	1924
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
českokrumlovský	českokrumlovský	k2eAgMnSc1d1	českokrumlovský
starosta	starosta	k1gMnSc1	starosta
(	(	kIx(	(
<g/>
1904	[number]	k4	1904
<g/>
–	–	k?	–
<g/>
1918	[number]	k4	1918
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Wolf	Wolf	k1gMnSc1	Wolf
(	(	kIx(	(
<g/>
1864	[number]	k4	1864
<g/>
–	–	k?	–
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fotograf	fotograf	k1gMnSc1	fotograf
</s>
</p>
<p>
<s>
Karl	Karl	k1gMnSc1	Karl
Tannich	Tannich	k1gMnSc1	Tannich
(	(	kIx(	(
<g/>
1883	[number]	k4	1883
<g/>
–	–	k?	–
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
archivář	archivář	k1gMnSc1	archivář
<g/>
,	,	kIx,	,
historik	historik	k1gMnSc1	historik
a	a	k8xC	a
českokrumlovský	českokrumlovský	k2eAgMnSc1d1	českokrumlovský
starosta	starosta	k1gMnSc1	starosta
(	(	kIx(	(
<g/>
1919	[number]	k4	1919
<g/>
–	–	k?	–
<g/>
1927	[number]	k4	1927
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Šmirous	Šmirous	k1gMnSc1	Šmirous
(	(	kIx(	(
<g/>
1890	[number]	k4	1890
<g/>
–	–	k?	–
<g/>
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fotograf	fotograf	k1gMnSc1	fotograf
<g/>
,	,	kIx,	,
průkopník	průkopník	k1gMnSc1	průkopník
barevné	barevný	k2eAgFnSc2d1	barevná
fotografie	fotografia	k1gFnSc2	fotografia
</s>
</p>
<p>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Šmirous	Šmirous	k1gMnSc1	Šmirous
(	(	kIx(	(
<g/>
1894	[number]	k4	1894
<g/>
–	–	k?	–
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
národní	národní	k2eAgMnSc1d1	národní
buditel	buditel	k1gMnSc1	buditel
a	a	k8xC	a
aktivista	aktivista	k1gMnSc1	aktivista
</s>
</p>
<p>
<s>
Anton	Anton	k1gMnSc1	Anton
Riedl	Riedl	k1gMnSc1	Riedl
(	(	kIx(	(
<g/>
1910	[number]	k4	1910
<g/>
–	–	k?	–
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
</s>
</p>
<p>
<s>
Petr	Petr	k1gMnSc1	Petr
Muk	muka	k1gFnPc2	muka
(	(	kIx(	(
<g/>
1965	[number]	k4	1965
<g/>
–	–	k?	–
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zpěvák	zpěvák	k1gMnSc1	zpěvák
</s>
</p>
<p>
<s>
Ruth	Ruth	k1gFnSc1	Ruth
Hálová	Hálová	k1gFnSc1	Hálová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1926	[number]	k4	1926
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
zachráněných	zachráněný	k2eAgFnPc2d1	zachráněná
židovských	židovský	k2eAgFnPc2d1	židovská
dětí	dítě	k1gFnPc2	dítě
sirem	sir	k1gMnSc7	sir
Nicholasem	Nicholas	k1gMnSc7	Nicholas
Wintonem	Winton	k1gInSc7	Winton
</s>
</p>
<p>
<s>
Helmut	Helmut	k1gMnSc1	Helmut
Doyscher	Doyschra	k1gFnPc2	Doyschra
(	(	kIx(	(
<g/>
1935	[number]	k4	1935
<g/>
–	–	k?	–
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
</s>
</p>
<p>
<s>
Anna	Anna	k1gFnSc1	Anna
Chromy	chrom	k1gInPc1	chrom
(	(	kIx(	(
<g/>
*	*	kIx~	*
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česko-rakouská	českoakouský	k2eAgFnSc1d1	česko-rakouská
malířka	malířka	k1gFnSc1	malířka
a	a	k8xC	a
sochařka	sochařka	k1gFnSc1	sochařka
</s>
</p>
<p>
<s>
Marian	Marian	k1gMnSc1	Marian
Jelínek	Jelínek	k1gMnSc1	Jelínek
(	(	kIx(	(
<g/>
*	*	kIx~	*
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hokejový	hokejový	k2eAgMnSc1d1	hokejový
trenér	trenér	k1gMnSc1	trenér
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Matura	matura	k1gFnSc1	matura
(	(	kIx(	(
<g/>
*	*	kIx~	*
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
skokan	skokan	k1gMnSc1	skokan
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Rypl	Rypl	k1gMnSc1	Rypl
(	(	kIx(	(
<g/>
1903	[number]	k4	1903
<g/>
-	-	kIx~	-
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
válečný	válečný	k2eAgMnSc1d1	válečný
letec	letec	k1gMnSc1	letec
</s>
</p>
<p>
<s>
===	===	k?	===
Osobnosti	osobnost	k1gFnPc1	osobnost
spjaté	spjatý	k2eAgFnPc1d1	spjatá
s	s	k7c7	s
městem	město	k1gNnSc7	město
===	===	k?	===
</s>
</p>
<p>
<s>
Egon	Egon	k1gMnSc1	Egon
Schiele	Schiel	k1gInSc2	Schiel
(	(	kIx(	(
<g/>
1890	[number]	k4	1890
<g/>
–	–	k?	–
<g/>
1918	[number]	k4	1918
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rakouský	rakouský	k2eAgMnSc1d1	rakouský
malíř	malíř	k1gMnSc1	malíř
</s>
</p>
<p>
<s>
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Fischer	Fischer	k1gMnSc1	Fischer
(	(	kIx(	(
<g/>
1894	[number]	k4	1894
<g/>
–	–	k?	–
<g/>
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Seidel	Seidel	k1gMnSc1	Seidel
(	(	kIx(	(
<g/>
1859	[number]	k4	1859
<g/>
–	–	k?	–
<g/>
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fotograf	fotograf	k1gMnSc1	fotograf
</s>
</p>
<p>
<s>
Ignác	Ignác	k1gMnSc1	Ignác
Spiro	Spiro	k1gNnSc4	Spiro
(	(	kIx(	(
<g/>
1817	[number]	k4	1817
<g/>
–	–	k?	–
<g/>
1894	[number]	k4	1894
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
Větříňských	Větříňský	k2eAgFnPc2d1	Větříňský
papíren	papírna	k1gFnPc2	papírna
</s>
</p>
<p>
<s>
Arnošt	Arnošt	k1gMnSc1	Arnošt
Porák	Porák	k1gMnSc1	Porák
(	(	kIx(	(
<g/>
1849	[number]	k4	1849
<g/>
–	–	k?	–
<g/>
1918	[number]	k4	1918
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
průmyslník	průmyslník	k1gMnSc1	průmyslník
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
papíren	papírna	k1gFnPc2	papírna
Loučovice	Loučovice	k1gFnSc2	Loučovice
</s>
</p>
<p>
<s>
Adolf	Adolf	k1gMnSc1	Adolf
Schwarzenberg	Schwarzenberg	k1gMnSc1	Schwarzenberg
(	(	kIx(	(
<g/>
1890	[number]	k4	1890
<g/>
–	–	k?	–
<g/>
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hlava	hlava	k1gFnSc1	hlava
krumlovsko-hlubocké	krumlovskolubocký	k2eAgFnSc2d1	krumlovsko-hlubocký
větve	větev	k1gFnSc2	větev
<g/>
,	,	kIx,	,
antifašista	antifašista	k1gMnSc1	antifašista
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Nepomuk	Nepomuk	k1gMnSc1	Nepomuk
II	II	kA	II
<g/>
.	.	kIx.	.
ze	z	k7c2	z
Schwarzenbergu	Schwarzenberg	k1gInSc2	Schwarzenberg
(	(	kIx(	(
<g/>
1860	[number]	k4	1860
<g/>
–	–	k?	–
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vévoda	vévoda	k1gMnSc1	vévoda
krumlovský	krumlovský	k2eAgMnSc1d1	krumlovský
a	a	k8xC	a
mecenáš	mecenáš	k1gMnSc1	mecenáš
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Kulich	Kulich	k1gMnSc1	Kulich
(	(	kIx(	(
<g/>
1869	[number]	k4	1869
<g/>
–	–	k?	–
<g/>
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
praktický	praktický	k2eAgInSc1d1	praktický
a	a	k8xC	a
schwarzenberský	schwarzenberský	k2eAgMnSc1d1	schwarzenberský
knížecí	knížecí	k2eAgMnSc1d1	knížecí
lékař	lékař	k1gMnSc1	lékař
<g/>
,	,	kIx,	,
vlastenec	vlastenec	k1gMnSc1	vlastenec
a	a	k8xC	a
aktivista	aktivista	k1gMnSc1	aktivista
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Záloha	záloha	k1gFnSc1	záloha
(	(	kIx(	(
<g/>
1926	[number]	k4	1926
<g/>
–	–	k?	–
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
archivář	archivář	k1gMnSc1	archivář
a	a	k8xC	a
promovaný	promovaný	k2eAgMnSc1d1	promovaný
historik	historik	k1gMnSc1	historik
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Šmirous	Šmirous	k1gMnSc1	Šmirous
(	(	kIx(	(
<g/>
1856	[number]	k4	1856
<g/>
–	–	k?	–
<g/>
1925	[number]	k4	1925
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
městský	městský	k2eAgMnSc1d1	městský
rada	rada	k1gMnSc1	rada
<g/>
,	,	kIx,	,
národní	národní	k2eAgMnSc1d1	národní
buditel	buditel	k1gMnSc1	buditel
a	a	k8xC	a
aktivista	aktivista	k1gMnSc1	aktivista
</s>
</p>
<p>
<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
Mařík	Mařík	k1gMnSc1	Mařík
<g/>
,	,	kIx,	,
učitel	učitel	k1gMnSc1	učitel
a	a	k8xC	a
vlastenec	vlastenec	k1gMnSc1	vlastenec
</s>
</p>
<p>
<s>
Adam	Adam	k1gMnSc1	Adam
František	František	k1gMnSc1	František
ze	z	k7c2	z
Schwarzenbergu	Schwarzenberg	k1gInSc2	Schwarzenberg
(	(	kIx(	(
<g/>
1680	[number]	k4	1680
<g/>
–	–	k?	–
<g/>
1732	[number]	k4	1732
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
krumlovský	krumlovský	k2eAgMnSc1d1	krumlovský
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Schwarzenbergů	Schwarzenberg	k1gMnPc2	Schwarzenberg
</s>
</p>
<p>
<s>
Eleonora	Eleonora	k1gFnSc1	Eleonora
Amálie	Amálie	k1gFnSc1	Amálie
ze	z	k7c2	z
Schwarzenbergu	Schwarzenberg	k1gInSc2	Schwarzenberg
(	(	kIx(	(
<g/>
1682	[number]	k4	1682
<g/>
–	–	k?	–
<g/>
1741	[number]	k4	1741
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
regentka	regentka	k1gFnSc1	regentka
Schwarzenberského	schwarzenberský	k2eAgNnSc2d1	Schwarzenberské
dominia	dominion	k1gNnSc2	dominion
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Adolf	Adolf	k1gMnSc1	Adolf
II	II	kA	II
<g/>
.	.	kIx.	.
ze	z	k7c2	z
Schwarzenbergu	Schwarzenberg	k1gInSc2	Schwarzenberg
(	(	kIx(	(
<g/>
1799	[number]	k4	1799
<g/>
–	–	k?	–
<g/>
1888	[number]	k4	1888
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
iniciátor	iniciátor	k1gMnSc1	iniciátor
modernizace	modernizace	k1gFnSc2	modernizace
jihočeského	jihočeský	k2eAgNnSc2d1	Jihočeské
hospodářství	hospodářství	k1gNnSc2	hospodářství
a	a	k8xC	a
průmyslu	průmysl	k1gInSc2	průmysl
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
I.	I.	kA	I.
Adam	Adam	k1gMnSc1	Adam
ze	z	k7c2	z
Schwarzenbergu	Schwarzenberg	k1gInSc2	Schwarzenberg
(	(	kIx(	(
<g/>
1722	[number]	k4	1722
<g/>
–	–	k?	–
<g/>
1782	[number]	k4	1782
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vévoda	vévoda	k1gMnSc1	vévoda
krumlovský	krumlovský	k2eAgMnSc1d1	krumlovský
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Kristián	Kristián	k1gMnSc1	Kristián
I.	I.	kA	I.
z	z	k7c2	z
Eggenbergu	Eggenberg	k1gInSc2	Eggenberg
(	(	kIx(	(
<g/>
1641	[number]	k4	1641
<g/>
–	–	k?	–
<g/>
1710	[number]	k4	1710
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vévoda	vévoda	k1gMnSc1	vévoda
krumlovský	krumlovský	k2eAgMnSc1d1	krumlovský
</s>
</p>
<p>
<s>
Marie	Marie	k1gFnSc1	Marie
Arnoštka	Arnoštka	k1gFnSc1	Arnoštka
z	z	k7c2	z
Eggenbergu	Eggenberg	k1gInSc2	Eggenberg
(	(	kIx(	(
<g/>
1649	[number]	k4	1649
<g/>
–	–	k?	–
<g/>
1719	[number]	k4	1719
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vévodkyně	vévodkyně	k1gFnSc1	vévodkyně
krumlovská	krumlovský	k2eAgFnSc1d1	Krumlovská
</s>
</p>
<p>
<s>
Jakub	Jakub	k1gMnSc1	Jakub
Krčín	Krčín	k1gMnSc1	Krčín
(	(	kIx(	(
<g/>
1535	[number]	k4	1535
<g/>
–	–	k?	–
<g/>
1604	[number]	k4	1604
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
architekt	architekt	k1gMnSc1	architekt
a	a	k8xC	a
rybníkář	rybníkář	k1gMnSc1	rybníkář
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Bílek	Bílek	k1gMnSc1	Bílek
z	z	k7c2	z
Bilenberku	Bilenberk	k1gInSc2	Bilenberk
(	(	kIx(	(
<g/>
1588	[number]	k4	1588
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
–	–	k?	–
<g/>
1657	[number]	k4	1657
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jezuitský	jezuitský	k2eAgMnSc1d1	jezuitský
farář	farář	k1gMnSc1	farář
a	a	k8xC	a
prelát	prelát	k1gMnSc1	prelát
</s>
</p>
<p>
<s>
Julio	Julio	k1gMnSc1	Julio
Caesar	Caesar	k1gMnSc1	Caesar
(	(	kIx(	(
<g/>
1586	[number]	k4	1586
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
–	–	k?	–
<g/>
1609	[number]	k4	1609
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
levoboček	levoboček	k1gMnSc1	levoboček
Rudolfa	Rudolf	k1gMnSc2	Rudolf
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vilém	Vilém	k1gMnSc1	Vilém
z	z	k7c2	z
Rožmberka	Rožmberk	k1gInSc2	Rožmberk
(	(	kIx(	(
<g/>
1535	[number]	k4	1535
<g/>
–	–	k?	–
<g/>
1592	[number]	k4	1592
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vladař	vladař	k1gMnSc1	vladař
Domu	dům	k1gInSc2	dům
rožmberského	rožmberský	k2eAgNnSc2d1	rožmberské
</s>
</p>
<p>
<s>
Oldřich	Oldřich	k1gMnSc1	Oldřich
II	II	kA	II
<g/>
.	.	kIx.	.
z	z	k7c2	z
Rožmberka	Rožmberk	k1gInSc2	Rožmberk
(	(	kIx(	(
<g/>
1403	[number]	k4	1403
<g/>
–	–	k?	–
<g/>
1462	[number]	k4	1462
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vladař	vladař	k1gMnSc1	vladař
Domu	dům	k1gInSc2	dům
rožmberského	rožmberský	k2eAgNnSc2d1	rožmberské
</s>
</p>
<p>
<s>
John	John	k1gMnSc1	John
Dee	Dee	k1gMnSc1	Dee
(	(	kIx(	(
<g/>
1527	[number]	k4	1527
<g/>
–	–	k?	–
<g/>
1609	[number]	k4	1609
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
anglický	anglický	k2eAgMnSc1d1	anglický
alchymista	alchymista	k1gMnSc1	alchymista
a	a	k8xC	a
špion	špion	k1gMnSc1	špion
</s>
</p>
<p>
<s>
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Balbín	Balbín	k1gMnSc1	Balbín
(	(	kIx(	(
<g/>
1621	[number]	k4	1621
<g/>
–	–	k?	–
<g/>
1688	[number]	k4	1688
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jezuitský	jezuitský	k2eAgMnSc1d1	jezuitský
pedagog	pedagog	k1gMnSc1	pedagog
a	a	k8xC	a
literát	literát	k1gMnSc1	literát
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Jakub	Jakub	k1gMnSc1	Jakub
Fortin	Fortin	k1gMnSc1	Fortin
(	(	kIx(	(
<g/>
1680	[number]	k4	1680
<g/>
–	–	k?	–
<g/>
1763	[number]	k4	1763
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
schwarzenberské	schwarzenberský	k2eAgFnSc2d1	Schwarzenberská
knížecí	knížecí	k2eAgFnSc2d1	knížecí
stavitel	stavitel	k1gMnSc1	stavitel
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Rosenauer	Rosenauer	k1gMnSc1	Rosenauer
(	(	kIx(	(
<g/>
1735	[number]	k4	1735
<g/>
–	–	k?	–
<g/>
1804	[number]	k4	1804
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stavitel	stavitel	k1gMnSc1	stavitel
Schwarzenberského	schwarzenberský	k2eAgInSc2d1	schwarzenberský
plavebního	plavební	k2eAgInSc2d1	plavební
kanálu	kanál	k1gInSc2	kanál
</s>
</p>
<p>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Hašek	Hašek	k1gMnSc1	Hašek
(	(	kIx(	(
<g/>
1892	[number]	k4	1892
<g/>
–	–	k?	–
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
archivář	archivář	k1gMnSc1	archivář
<g/>
,	,	kIx,	,
znovu-zakladatel	znovuakladatel	k1gMnSc1	znovu-zakladatel
Hudební	hudební	k2eAgFnSc2d1	hudební
školy	škola	k1gFnSc2	škola
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
</s>
</p>
<p>
<s>
Hynek	Hynek	k1gMnSc1	Hynek
Gross	Gross	k1gMnSc1	Gross
(	(	kIx(	(
<g/>
1859	[number]	k4	1859
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
archivář	archivář	k1gMnSc1	archivář
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
České	český	k2eAgFnSc2d1	Česká
královské	královský	k2eAgFnSc2d1	královská
společnosti	společnost	k1gFnSc2	společnost
nauk	nauka	k1gFnPc2	nauka
</s>
</p>
<p>
<s>
Jindřich	Jindřich	k1gMnSc1	Jindřich
Čapek	Čapek	k1gMnSc1	Čapek
<g/>
,	,	kIx,	,
knižní	knižní	k2eAgMnSc1d1	knižní
ilustrátor	ilustrátor	k1gMnSc1	ilustrátor
</s>
</p>
<p>
<s>
Ondřej	Ondřej	k1gMnSc1	Ondřej
Macek	Macek	k1gMnSc1	Macek
(	(	kIx(	(
<g/>
*	*	kIx~	*
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
cembalista	cembalista	k1gMnSc1	cembalista
a	a	k8xC	a
umělecký	umělecký	k2eAgMnSc1d1	umělecký
vedoucí	vedoucí	k1gMnSc1	vedoucí
barokního	barokní	k2eAgInSc2d1	barokní
souboru	soubor	k1gInSc2	soubor
Hof-Musici	Hof-Musice	k1gFnSc4	Hof-Musice
</s>
</p>
<p>
<s>
==	==	k?	==
Partnerská	partnerský	k2eAgNnPc1d1	partnerské
města	město	k1gNnPc1	město
==	==	k?	==
</s>
</p>
<p>
<s>
Hauzenberg	Hauzenberg	k1gInSc1	Hauzenberg
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
</s>
</p>
<p>
<s>
Miami	Miami	k1gNnSc1	Miami
Beach	Beacha	k1gFnPc2	Beacha
<g/>
,	,	kIx,	,
USA	USA	kA	USA
</s>
</p>
<p>
<s>
Vöcklabruck	Vöcklabruck	k1gInSc1	Vöcklabruck
<g/>
,	,	kIx,	,
Rakousko	Rakousko	k1gNnSc1	Rakousko
</s>
</p>
<p>
<s>
Slovinský	slovinský	k2eAgInSc1d1	slovinský
Hradec	Hradec	k1gInSc1	Hradec
<g/>
,	,	kIx,	,
Slovinsko	Slovinsko	k1gNnSc1	Slovinsko
</s>
</p>
<p>
<s>
San	San	k?	San
Gimignano	Gimignana	k1gFnSc5	Gimignana
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc5	Itálie
</s>
</p>
<p>
<s>
Llanwrtyd	Llanwrtyd	k6eAd1	Llanwrtyd
Wells	Wells	k1gInSc1	Wells
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
</s>
</p>
<p>
<s>
===	===	k?	===
Zahraniční	zahraniční	k2eAgMnPc1d1	zahraniční
partneři	partner	k1gMnPc1	partner
===	===	k?	===
</s>
</p>
<p>
<s>
Linec	Linec	k1gInSc1	Linec
<g/>
,	,	kIx,	,
Rakousko	Rakousko	k1gNnSc1	Rakousko
</s>
</p>
<p>
<s>
Pasov	Pasov	k1gInSc1	Pasov
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
</s>
</p>
<p>
<s>
==	==	k?	==
Členění	členění	k1gNnSc1	členění
města	město	k1gNnSc2	město
==	==	k?	==
</s>
</p>
<p>
<s>
Město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
šesti	šest	k4xCc2	šest
katastrálních	katastrální	k2eAgNnPc2d1	katastrální
území	území	k1gNnPc2	území
a	a	k8xC	a
deseti	deset	k4xCc2	deset
místních	místní	k2eAgFnPc2d1	místní
částí	část	k1gFnPc2	část
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
k.	k.	k?	k.
ú.	ú.	k?	ú.
Český	český	k2eAgInSc1d1	český
Krumlov	Krumlov	k1gInSc1	Krumlov
–	–	k?	–
části	část	k1gFnSc2	část
Horní	horní	k2eAgFnSc1d1	horní
Brána	brána	k1gFnSc1	brána
<g/>
,	,	kIx,	,
Latrán	latrán	k1gInSc1	latrán
<g/>
,	,	kIx,	,
Nádražní	nádražní	k2eAgNnSc1d1	nádražní
Předměstí	předměstí	k1gNnSc1	předměstí
<g/>
,	,	kIx,	,
Plešivec	plešivec	k1gMnSc1	plešivec
a	a	k8xC	a
Vnitřní	vnitřní	k2eAgNnSc1d1	vnitřní
Město	město	k1gNnSc1	město
</s>
</p>
<p>
<s>
k.	k.	k?	k.
ú.	ú.	k?	ú.
Kladné-Dobrkovice	Kladné-Dobrkovice	k1gFnSc2	Kladné-Dobrkovice
–	–	k?	–
část	část	k1gFnSc1	část
Nové	Nové	k2eAgFnPc1d1	Nové
Dobrkovice	Dobrkovice	k1gFnPc1	Dobrkovice
</s>
</p>
<p>
<s>
k.	k.	k?	k.
ú.	ú.	k?	ú.
Přísečná-Domoradice	Přísečná-Domoradice	k1gFnSc2	Přísečná-Domoradice
–	–	k?	–
část	část	k1gFnSc1	část
Domoradice	Domoradice	k1gFnSc1	Domoradice
</s>
</p>
<p>
<s>
k.	k.	k?	k.
ú.	ú.	k?	ú.
Slupenec	Slupenec	k1gInSc1	Slupenec
–	–	k?	–
část	část	k1gFnSc1	část
Slupenec	Slupenec	k1gInSc1	Slupenec
</s>
</p>
<p>
<s>
k.	k.	k?	k.
ú.	ú.	k?	ú.
Spolí-Nové	Spolí-Nové	k2eAgFnSc7d1	Spolí-Nové
Spolí	Spole	k1gFnSc7	Spole
–	–	k?	–
část	část	k1gFnSc1	část
Nové	Nová	k1gFnSc2	Nová
Spolí	Spole	k1gFnPc2	Spole
</s>
</p>
<p>
<s>
k.	k.	k?	k.
ú.	ú.	k?	ú.
Vyšný	vyšný	k2eAgInSc4d1	vyšný
–	–	k?	–
část	část	k1gFnSc4	část
Vyšný	vyšný	k2eAgMnSc5d1	vyšný
</s>
</p>
<p>
<s>
==	==	k?	==
Správní	správní	k2eAgNnPc1d1	správní
území	území	k1gNnPc1	území
==	==	k?	==
</s>
</p>
<p>
<s>
Český	český	k2eAgInSc1d1	český
Krumlov	Krumlov	k1gInSc1	Krumlov
byl	být	k5eAaImAgInS	být
dříve	dříve	k6eAd2	dříve
okresním	okresní	k2eAgNnSc7d1	okresní
městem	město	k1gNnSc7	město
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
obcí	obec	k1gFnSc7	obec
s	s	k7c7	s
rozšířenou	rozšířený	k2eAgFnSc7d1	rozšířená
působností	působnost	k1gFnSc7	působnost
a	a	k8xC	a
pověřeným	pověřený	k2eAgInSc7d1	pověřený
obecním	obecní	k2eAgInSc7d1	obecní
úřadem	úřad	k1gInSc7	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Okres	okres	k1gInSc1	okres
Český	český	k2eAgInSc4d1	český
Krumlov	Krumlov	k1gInSc4	Krumlov
ale	ale	k8xC	ale
stále	stále	k6eAd1	stále
existuje	existovat	k5eAaImIp3nS	existovat
a	a	k8xC	a
skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
ze	z	k7c2	z
45	[number]	k4	45
obcí	obec	k1gFnPc2	obec
<g/>
,	,	kIx,	,
ORP	ORP	kA	ORP
z	z	k7c2	z
31	[number]	k4	31
obcí	obec	k1gFnPc2	obec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Českokrumlovsko	Českokrumlovsko	k1gNnSc1	Českokrumlovsko
v	v	k7c6	v
době	doba	k1gFnSc6	doba
prvních	první	k4xOgInPc2	první
Lucemburků	Lucemburk	k1gInPc2	Lucemburk
1310	[number]	k4	1310
<g/>
–	–	k?	–
<g/>
1380	[number]	k4	1380
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
Krumlov	Krumlov	k1gInSc1	Krumlov
<g/>
:	:	kIx,	:
Státní	státní	k2eAgInSc1d1	státní
okresní	okresní	k2eAgInSc1d1	okresní
archiv	archiv	k1gInSc1	archiv
Český	český	k2eAgInSc1d1	český
Krumlov	Krumlov	k1gInSc1	Krumlov
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
77	[number]	k4	77
s.	s.	k?	s.
</s>
</p>
<p>
<s>
Českokrumlovsko	Českokrumlovsko	k1gNnSc1	Českokrumlovsko
1400	[number]	k4	1400
<g/>
–	–	k?	–
<g/>
1460	[number]	k4	1460
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
Krumlov	Krumlov	k1gInSc1	Krumlov
<g/>
:	:	kIx,	:
Státní	státní	k2eAgInSc1d1	státní
okresní	okresní	k2eAgInSc1d1	okresní
archiv	archiv	k1gInSc1	archiv
Český	český	k2eAgInSc1d1	český
Krumlov	Krumlov	k1gInSc1	Krumlov
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
89	[number]	k4	89
s.	s.	k?	s.
</s>
</p>
<p>
<s>
Českokrumlovsko	Českokrumlovsko	k1gNnSc1	Českokrumlovsko
v	v	k7c6	v
době	doba	k1gFnSc6	doba
Jagellonské	jagellonský	k2eAgFnSc2d1	Jagellonská
1470	[number]	k4	1470
<g/>
–	–	k?	–
<g/>
1526	[number]	k4	1526
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
Krumlov	Krumlov	k1gInSc1	Krumlov
<g/>
:	:	kIx,	:
Státní	státní	k2eAgInSc1d1	státní
okresní	okresní	k2eAgInSc1d1	okresní
archiv	archiv	k1gInSc1	archiv
Český	český	k2eAgInSc1d1	český
Krumlov	Krumlov	k1gInSc1	Krumlov
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
113	[number]	k4	113
s.	s.	k?	s.
</s>
</p>
<p>
<s>
Českokrumlovsko	Českokrumlovsko	k1gNnSc1	Českokrumlovsko
v	v	k7c6	v
proměnách	proměna	k1gFnPc6	proměna
staletí	staletí	k1gNnSc1	staletí
1250	[number]	k4	1250
<g/>
–	–	k?	–
<g/>
1850	[number]	k4	1850
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
Krumlov	Krumlov	k1gInSc1	Krumlov
<g/>
:	:	kIx,	:
Státní	státní	k2eAgInSc1d1	státní
okresní	okresní	k2eAgInSc1d1	okresní
archiv	archiv	k1gInSc1	archiv
Český	český	k2eAgInSc1d1	český
Krumlov	Krumlov	k1gInSc1	Krumlov
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
69	[number]	k4	69
s.	s.	k?	s.
</s>
</p>
<p>
<s>
Českokrumlovsko	Českokrumlovsko	k1gNnSc1	Českokrumlovsko
<g/>
.	.	kIx.	.
</s>
<s>
Mozaika	mozaika	k1gFnSc1	mozaika
dějin	dějiny	k1gFnPc2	dějiny
1850	[number]	k4	1850
<g/>
–	–	k?	–
<g/>
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
Krumlov	Krumlov	k1gInSc1	Krumlov
<g/>
:	:	kIx,	:
Státní	státní	k2eAgInSc1d1	státní
okresní	okresní	k2eAgInSc1d1	okresní
archiv	archiv	k1gInSc1	archiv
Český	český	k2eAgInSc1d1	český
Krumlov	Krumlov	k1gInSc1	Krumlov
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
83	[number]	k4	83
s.	s.	k?	s.
</s>
</p>
<p>
<s>
Českokrumlovsko	Českokrumlovsko	k1gNnSc1	Českokrumlovsko
1620	[number]	k4	1620
<g/>
–	–	k?	–
<g/>
1850	[number]	k4	1850
<g/>
.	.	kIx.	.
</s>
<s>
Třeboň	Třeboň	k1gFnSc1	Třeboň
<g/>
:	:	kIx,	:
Státní	státní	k2eAgInSc1d1	státní
oblastní	oblastní	k2eAgInSc1d1	oblastní
archiv	archiv	k1gInSc1	archiv
v	v	k7c6	v
Třeboni	Třeboň	k1gFnSc6	Třeboň
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
136	[number]	k4	136
s.	s.	k?	s.
</s>
</p>
<p>
<s>
GAŽI	GAŽI	kA	GAŽI
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
<g/>
;	;	kIx,	;
PAVELEC	Pavelec	k1gMnSc1	Pavelec
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
Krumlov	Krumlov	k1gInSc1	Krumlov
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
rezidenčního	rezidenční	k2eAgNnSc2d1	rezidenční
města	město	k1gNnSc2	město
k	k	k7c3	k
památce	památka	k1gFnSc3	památka
světového	světový	k2eAgNnSc2d1	světové
kulturního	kulturní	k2eAgNnSc2d1	kulturní
dědictví	dědictví	k1gNnSc2	dědictví
<g/>
.	.	kIx.	.
</s>
<s>
Příprava	příprava	k1gFnSc1	příprava
vydání	vydání	k1gNnSc2	vydání
Martin	Martina	k1gFnPc2	Martina
Gaži	Gaž	k1gFnSc2	Gaž
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
:	:	kIx,	:
NPÚ	NPÚ	kA	NPÚ
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
1039	[number]	k4	1039
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85033	[number]	k4	85033
<g/>
-	-	kIx~	-
<g/>
26	[number]	k4	26
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HAJNÁ	hajná	k1gFnSc1	hajná
<g/>
,	,	kIx,	,
Milena	Milena	k1gFnSc1	Milena
<g/>
.	.	kIx.	.
</s>
<s>
Rožmberkové	Rožmberkové	k?	Rožmberkové
<g/>
.	.	kIx.	.
</s>
<s>
Cestovní	cestovní	k2eAgMnSc1d1	cestovní
průvodce	průvodce	k1gMnSc1	průvodce
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
:	:	kIx,	:
NPÚ	NPÚ	kA	NPÚ
<g/>
,	,	kIx,	,
územní	územní	k2eAgFnSc1d1	územní
odborné	odborný	k2eAgNnSc4d1	odborné
pracoviště	pracoviště	k1gNnSc4	pracoviště
v	v	k7c6	v
Českých	český	k2eAgInPc6d1	český
Budějovicích	Budějovice	k1gInPc6	Budějovice
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
302	[number]	k4	302
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85033	[number]	k4	85033
<g/>
-	-	kIx~	-
<g/>
32	[number]	k4	32
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
35	[number]	k4	35
<g/>
-	-	kIx~	-
<g/>
49	[number]	k4	49
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PAVELEC	Pavelec	k1gMnSc1	Pavelec
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
Krumlov	Krumlov	k1gInSc1	Krumlov
</s>
</p>
<p>
<s>
ŠORM	Šorm	k1gMnSc1	Šorm
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
<g/>
.	.	kIx.	.
</s>
<s>
Pověsti	pověst	k1gFnPc1	pověst
o	o	k7c6	o
českých	český	k2eAgInPc6d1	český
zvonech	zvon	k1gInPc6	zvon
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
V.	V.	kA	V.
Kotrba	kotrba	k1gFnSc1	kotrba
<g/>
,	,	kIx,	,
1926	[number]	k4	1926
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VLČEK	Vlček	k1gMnSc1	Vlček
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
(	(	kIx(	(
<g/>
ed	ed	k?	ed
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
Krumlov	Krumlov	k1gInSc1	Krumlov
<g/>
.	.	kIx.	.
</s>
<s>
Historické	historický	k2eAgNnSc1d1	historické
centrum	centrum	k1gNnSc1	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Foibos	Foibos	k1gMnSc1	Foibos
<g/>
,	,	kIx,	,
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
200	[number]	k4	200
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
87073	[number]	k4	87073
<g/>
-	-	kIx~	-
<g/>
93	[number]	k4	93
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Českého	český	k2eAgInSc2d1	český
KrumlovaVlajka	KrumlovaVlajka	k1gFnSc1	KrumlovaVlajka
Českého	český	k2eAgInSc2d1	český
Krumlova	Krumlov	k1gInSc2	Krumlov
</s>
</p>
<p>
<s>
Krumlovské	krumlovský	k2eAgNnSc1d1	krumlovské
vévodství	vévodství	k1gNnSc1	vévodství
</s>
</p>
<p>
<s>
Hospodářský	hospodářský	k2eAgInSc1d1	hospodářský
ústav	ústav	k1gInSc1	ústav
v	v	k7c6	v
Českém	český	k2eAgInSc6d1	český
Krumlově	Krumlov	k1gInSc6	Krumlov
</s>
</p>
<p>
<s>
Židovský	židovský	k2eAgInSc1d1	židovský
hřbitov	hřbitov	k1gInSc1	hřbitov
v	v	k7c6	v
Českém	český	k2eAgInSc6d1	český
Krumlově	Krumlov	k1gInSc6	Krumlov
</s>
</p>
<p>
<s>
Český	český	k2eAgInSc1d1	český
Krumlov	Krumlov	k1gInSc1	Krumlov
(	(	kIx(	(
<g/>
hrad	hrad	k1gInSc1	hrad
a	a	k8xC	a
zámek	zámek	k1gInSc1	zámek
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Synagoga	synagoga	k1gFnSc1	synagoga
v	v	k7c6	v
Českém	český	k2eAgInSc6d1	český
Krumlově	Krumlov	k1gInSc6	Krumlov
</s>
</p>
<p>
<s>
Otáčivé	otáčivý	k2eAgNnSc4d1	otáčivé
hlediště	hlediště	k1gNnSc4	hlediště
Český	český	k2eAgInSc1d1	český
Krumlov	Krumlov	k1gInSc1	Krumlov
</s>
</p>
<p>
<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
hudební	hudební	k2eAgInSc1d1	hudební
festival	festival	k1gInSc1	festival
Český	český	k2eAgInSc1d1	český
Krumlov	Krumlov	k1gInSc1	Krumlov
</s>
</p>
<p>
<s>
Rožmberské	rožmberský	k2eAgNnSc1d1	rožmberské
dominium	dominium	k1gNnSc1	dominium
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
škol	škola	k1gFnPc2	škola
v	v	k7c6	v
Českém	český	k2eAgInSc6d1	český
Krumlově	Krumlov	k1gInSc6	Krumlov
</s>
</p>
<p>
<s>
Slavnosti	slavnost	k1gFnPc1	slavnost
pětilisté	pětilistý	k2eAgFnSc2d1	pětilistá
růže	růž	k1gFnSc2	růž
</s>
</p>
<p>
<s>
Okresní	okresní	k2eAgInSc1d1	okresní
soud	soud	k1gInSc1	soud
v	v	k7c6	v
Českém	český	k2eAgInSc6d1	český
Krumlově	Krumlov	k1gInSc6	Krumlov
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Český	český	k2eAgMnSc1d1	český
Krumlov	Krumlov	k1gInSc4	Krumlov
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Český	český	k2eAgInSc4d1	český
Krumlov	Krumlov	k1gInSc4	Krumlov
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc4d1	slovníkové
heslo	heslo	k1gNnSc4	heslo
Český	český	k2eAgInSc1d1	český
Krumlov	Krumlov	k1gInSc1	Krumlov
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Územně	územně	k6eAd1	územně
identifikační	identifikační	k2eAgInSc1d1	identifikační
registr	registr	k1gInSc1	registr
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc4	obec
Český	český	k2eAgInSc4d1	český
Krumlov	Krumlov	k1gInSc4	Krumlov
v	v	k7c6	v
Územně	územně	k6eAd1	územně
identifikačním	identifikační	k2eAgInSc6d1	identifikační
registru	registr	k1gInSc6	registr
ČR	ČR	kA	ČR
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
zámku	zámek	k1gInSc2	zámek
Český	český	k2eAgInSc1d1	český
Krumlov	Krumlov	k1gInSc1	Krumlov
</s>
</p>
<p>
<s>
statistické	statistický	k2eAgInPc1d1	statistický
údaje	údaj	k1gInPc1	údaj
města	město	k1gNnSc2	město
Český	český	k2eAgInSc1d1	český
Krumlov	Krumlov	k1gInSc1	Krumlov
</s>
</p>
<p>
<s>
stránky	stránka	k1gFnPc1	stránka
města	město	k1gNnSc2	město
Český	český	k2eAgInSc1d1	český
Krumlov	Krumlov	k1gInSc1	Krumlov
</s>
</p>
<p>
<s>
Český	český	k2eAgInSc1d1	český
Krumlov	Krumlov	k1gInSc1	Krumlov
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
oficiálního	oficiální	k2eAgInSc2d1	oficiální
portálu	portál	k1gInSc2	portál
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
</s>
</p>
<p>
<s>
Vyhlídkový	vyhlídkový	k2eAgInSc1d1	vyhlídkový
vrch	vrch	k1gInSc1	vrch
nedaleko	nedaleko	k7c2	nedaleko
historického	historický	k2eAgNnSc2d1	historické
centra	centrum	k1gNnSc2	centrum
Českého	český	k2eAgInSc2d1	český
Krumlova	Krumlov	k1gInSc2	Krumlov
</s>
</p>
<p>
<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
hudební	hudební	k2eAgInSc1d1	hudební
festival	festival	k1gInSc1	festival
Český	český	k2eAgInSc1d1	český
Krumlov	Krumlov	k1gInSc1	Krumlov
</s>
</p>
<p>
<s>
Turistický	turistický	k2eAgInSc4d1	turistický
portál	portál	k1gInSc4	portál
Český	český	k2eAgInSc1d1	český
Krumlov	Krumlov	k1gInSc1	Krumlov
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
</p>
<p>
<s>
Český	český	k2eAgInSc1d1	český
Krumlov	Krumlov	k1gInSc1	Krumlov
–	–	k?	–
od	od	k7c2	od
řeky	řeka	k1gFnSc2	řeka
k	k	k7c3	k
nebesům	nebesa	k1gNnPc3	nebesa
–	–	k?	–
video	video	k1gNnSc4	video
z	z	k7c2	z
archivu	archiv	k1gInSc2	archiv
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
v	v	k7c6	v
pořadu	pořad	k1gInSc6	pořad
Národní	národní	k2eAgInPc4d1	národní
klenoty	klenot	k1gInPc4	klenot
</s>
</p>
<p>
<s>
360	[number]	k4	360
<g/>
°	°	k?	°
fotografie	fotografia	k1gFnSc2	fotografia
</s>
</p>
<p>
<s>
Lex	Lex	k?	Lex
Schwarzenberg	Schwarzenberg	k1gInSc1	Schwarzenberg
</s>
</p>
<p>
<s>
Češi	Čech	k1gMnPc1	Čech
v	v	k7c6	v
zabraném	zabraný	k2eAgInSc6d1	zabraný
Krumlově	Krumlov	k1gInSc6	Krumlov
</s>
</p>
<p>
<s>
Kohoutí	kohoutí	k2eAgInSc1d1	kohoutí
kříž	kříž	k1gInSc1	kříž
–	–	k?	–
jihočeská	jihočeský	k2eAgFnSc1d1	Jihočeská
vědecká	vědecký	k2eAgFnSc1d1	vědecká
knihovna	knihovna	k1gFnSc1	knihovna
</s>
</p>
