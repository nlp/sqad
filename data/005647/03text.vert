<s>
Lucie	Lucie	k1gFnSc1	Lucie
Robinson	Robinson	k1gMnSc1	Robinson
<g/>
,	,	kIx,	,
občanským	občanský	k2eAgNnSc7d1	občanské
jménem	jméno	k1gNnSc7	jméno
Lucie	Lucie	k1gFnSc2	Lucie
Kneslová	Kneslový	k2eAgFnSc1d1	Kneslová
(	(	kIx(	(
<g/>
*	*	kIx~	*
7	[number]	k4	7
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1978	[number]	k4	1978
<g/>
,	,	kIx,	,
Jablonec	Jablonec	k1gInSc1	Jablonec
nad	nad	k7c7	nad
Nisou	Nisa	k1gFnSc7	Nisa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
portrétní	portrétní	k2eAgFnSc1d1	portrétní
<g/>
,	,	kIx,	,
výtvarná	výtvarný	k2eAgFnSc1d1	výtvarná
a	a	k8xC	a
módní	módní	k2eAgFnSc1d1	módní
fotografka	fotografka	k1gFnSc1	fotografka
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
koncepčnímu	koncepční	k2eAgNnSc3d1	koncepční
zapojení	zapojení	k1gNnSc3	zapojení
do	do	k7c2	do
kreativního	kreativní	k2eAgNnSc2d1	kreativní
a	a	k8xC	a
ekonomického	ekonomický	k2eAgNnSc2d1	ekonomické
zhodnocení	zhodnocení	k1gNnSc2	zhodnocení
konečného	konečný	k2eAgInSc2d1	konečný
mediálního	mediální	k2eAgInSc2d1	mediální
produktu	produkt	k1gInSc2	produkt
by	by	kYmCp3nP	by
mediální	mediální	k2eAgNnPc1d1	mediální
studia	studio	k1gNnPc1	studio
zvolila	zvolit	k5eAaPmAgNnP	zvolit
pro	pro	k7c4	pro
Lucii	Lucie	k1gFnSc4	Lucie
Robinson	Robinson	k1gMnSc1	Robinson
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
její	její	k3xOp3gFnSc2	její
zakázkové	zakázkový	k2eAgFnSc2d1	zakázková
práce	práce	k1gFnSc2	práce
kategorii	kategorie	k1gFnSc4	kategorie
"	"	kIx"	"
<g/>
image	image	k1gInSc1	image
maker	makro	k1gNnPc2	makro
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
výrazně	výrazně	k6eAd1	výrazně
přesahující	přesahující	k2eAgFnSc4d1	přesahující
pouhou	pouhý	k2eAgFnSc4d1	pouhá
fotografii	fotografia	k1gFnSc4	fotografia
<g/>
.	.	kIx.	.
</s>
<s>
Narodila	narodit	k5eAaPmAgFnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Jablonci	Jablonec	k1gInSc6	Jablonec
nad	nad	k7c7	nad
Nisou	Nisa	k1gFnSc7	Nisa
<g/>
,	,	kIx,	,
do	do	k7c2	do
rodiny	rodina	k1gFnSc2	rodina
s	s	k7c7	s
kořeny	kořen	k1gInPc7	kořen
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
a	a	k8xC	a
předrevolučním	předrevoluční	k2eAgNnSc6d1	předrevoluční
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Studovala	studovat	k5eAaImAgFnS	studovat
uměleckoprůmyslovou	uměleckoprůmyslový	k2eAgFnSc4d1	uměleckoprůmyslová
školu	škola	k1gFnSc4	škola
se	s	k7c7	s
zaměřením	zaměření	k1gNnSc7	zaměření
na	na	k7c4	na
šperky	šperk	k1gInPc4	šperk
a	a	k8xC	a
následně	následně	k6eAd1	následně
průmyslový	průmyslový	k2eAgInSc1d1	průmyslový
design	design	k1gInSc1	design
na	na	k7c6	na
pražské	pražský	k2eAgFnSc6d1	Pražská
Vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
uměleckoprůmyslové	uměleckoprůmyslový	k2eAgFnSc6d1	uměleckoprůmyslová
v	v	k7c6	v
ateliéru	ateliér	k1gInSc6	ateliér
profesora	profesor	k1gMnSc2	profesor
Diblíka	diblík	k1gMnSc2	diblík
<g/>
..	..	k?	..
Díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
dřívější	dřívější	k2eAgFnSc3d1	dřívější
kariéře	kariéra	k1gFnSc3	kariéra
v	v	k7c6	v
modelingu	modeling	k1gInSc6	modeling
–	–	k?	–
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
–	–	k?	–
a	a	k8xC	a
spolupráci	spolupráce	k1gFnSc3	spolupráce
se	se	k3xPyFc4	se
špičkou	špička	k1gFnSc7	špička
světových	světový	k2eAgMnPc2d1	světový
fotografů	fotograf	k1gMnPc2	fotograf
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
získala	získat	k5eAaPmAgFnS	získat
rané	raný	k2eAgFnPc4d1	raná
zkušenosti	zkušenost	k1gFnPc4	zkušenost
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
reklamní	reklamní	k2eAgFnSc2d1	reklamní
a	a	k8xC	a
módní	módní	k2eAgFnSc2d1	módní
fotografie	fotografia	k1gFnSc2	fotografia
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
úrovně	úroveň	k1gFnSc2	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
vystudovala	vystudovat	k5eAaPmAgFnS	vystudovat
kresbu	kresba	k1gFnSc4	kresba
a	a	k8xC	a
design	design	k1gInSc4	design
<g/>
,	,	kIx,	,
její	její	k3xOp3gInSc1	její
hlavní	hlavní	k2eAgInSc1d1	hlavní
zájem	zájem	k1gInSc1	zájem
patří	patřit	k5eAaImIp3nS	patřit
médiu	médium	k1gNnSc3	médium
fotografie	fotografia	k1gFnSc2	fotografia
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
první	první	k4xOgFnSc2	první
dekády	dekáda	k1gFnSc2	dekáda
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
Lucie	Lucie	k1gFnSc1	Lucie
Robinson	Robinson	k1gMnSc1	Robinson
etablovala	etablovat	k5eAaBmAgFnS	etablovat
jako	jako	k9	jako
portrétní	portrétní	k2eAgFnSc1d1	portrétní
a	a	k8xC	a
módní	módní	k2eAgFnSc1d1	módní
fotografka	fotografka	k1gFnSc1	fotografka
<g/>
.	.	kIx.	.
</s>
<s>
Pracovala	pracovat	k5eAaImAgFnS	pracovat
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
významnými	významný	k2eAgMnPc7d1	významný
lidmi	člověk	k1gMnPc7	člověk
a	a	k8xC	a
top	topit	k5eAaImRp2nS	topit
modelkami	modelka	k1gFnPc7	modelka
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
vysoce	vysoce	k6eAd1	vysoce
medializované	medializovaný	k2eAgFnSc2d1	medializovaná
kampaně	kampaň	k1gFnSc2	kampaň
firmy	firma	k1gFnSc2	firma
Louis	louis	k1gInPc2	louis
Vuitton	Vuitton	k1gInSc1	Vuitton
s	s	k7c7	s
fotografiemi	fotografia	k1gFnPc7	fotografia
osobností	osobnost	k1gFnPc2	osobnost
jako	jako	k8xC	jako
Pavlína	Pavlína	k1gFnSc1	Pavlína
Pořízková	Pořízková	k1gFnSc1	Pořízková
<g/>
,	,	kIx,	,
Miloš	Miloš	k1gMnSc1	Miloš
Forman	Forman	k1gMnSc1	Forman
a	a	k8xC	a
Helena	Helena	k1gFnSc1	Helena
Houdová	Houdová	k1gFnSc1	Houdová
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnPc1	její
portrétní	portrétní	k2eAgFnPc1d1	portrétní
fotografie	fotografia	k1gFnPc1	fotografia
<g/>
,	,	kIx,	,
módní	módní	k2eAgInPc1d1	módní
soubory	soubor	k1gInPc1	soubor
a	a	k8xC	a
fotografické	fotografický	k2eAgInPc1d1	fotografický
eseje	esej	k1gInPc1	esej
vycházejí	vycházet	k5eAaImIp3nP	vycházet
v	v	k7c6	v
časopisech	časopis	k1gInPc6	časopis
Elle	Ell	k1gFnSc2	Ell
<g/>
,	,	kIx,	,
Harper	Harper	k1gMnSc1	Harper
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Bazaar	Bazaar	k1gInSc1	Bazaar
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
Claire	Clair	k1gInSc5	Clair
<g/>
,	,	kIx,	,
InStyle	InStyl	k1gInSc5	InStyl
<g/>
,	,	kIx,	,
Vision	vision	k1gInSc1	vision
(	(	kIx(	(
<g/>
China	China	k1gFnSc1	China
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Wig	Wig	k1gMnSc1	Wig
(	(	kIx(	(
<g/>
UK	UK	kA	UK
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vsya	Vsy	k2eAgFnSc1d1	Vsy
Evropa	Evropa	k1gFnSc1	Evropa
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
SRN	SRN	kA	SRN
<g/>
)	)	kIx)	)
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
módních	módní	k2eAgFnPc2d1	módní
<g/>
,	,	kIx,	,
společenských	společenský	k2eAgInPc6d1	společenský
a	a	k8xC	a
designových	designový	k2eAgInPc6d1	designový
časopisech	časopis	k1gInPc6	časopis
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
publikacích	publikace	k1gFnPc6	publikace
jako	jako	k8xS	jako
Ona	onen	k3xDgFnSc1	onen
dnes	dnes	k6eAd1	dnes
a	a	k8xC	a
Esprit	esprit	k1gInSc4	esprit
Jiné	jiná	k1gFnSc2	jiná
<g/>
,	,	kIx,	,
zřejmě	zřejmě	k6eAd1	zřejmě
úmyslně	úmyslně	k6eAd1	úmyslně
anonymní	anonymní	k2eAgFnPc4d1	anonymní
práce	práce	k1gFnPc4	práce
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
využívány	využívat	k5eAaImNgInP	využívat
v	v	k7c6	v
reklamních	reklamní	k2eAgFnPc6d1	reklamní
kampaních	kampaň	k1gFnPc6	kampaň
na	na	k7c4	na
produkty	produkt	k1gInPc4	produkt
nadnárodních	nadnárodní	k2eAgFnPc2d1	nadnárodní
společností	společnost	k1gFnPc2	společnost
Cadbury	Cadbura	k1gFnSc2	Cadbura
<g/>
,	,	kIx,	,
Orange	Orange	k1gNnSc2	Orange
Mobile	mobile	k1gNnSc2	mobile
<g/>
,	,	kIx,	,
T-Mobile	T-Mobila	k1gFnSc3	T-Mobila
a	a	k8xC	a
Procter	Proctra	k1gFnPc2	Proctra
&	&	k?	&
Gamble	Gamble	k1gFnPc2	Gamble
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
její	její	k3xOp3gInSc4	její
nejznámější	známý	k2eAgFnSc1d3	nejznámější
charitativní	charitativní	k2eAgFnSc1d1	charitativní
práce	práce	k1gFnSc1	práce
patří	patřit	k5eAaImIp3nS	patřit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2009	[number]	k4	2009
<g/>
/	/	kIx~	/
<g/>
2010	[number]	k4	2010
fotografie	fotografia	k1gFnSc2	fotografia
nemocných	nemocný	k2eAgFnPc2d1	nemocná
dětí	dítě	k1gFnPc2	dítě
se	s	k7c7	s
snovými	snový	k2eAgFnPc7d1	snová
malůvkami	malůvka	k1gFnPc7	malůvka
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
<g/>
,	,	kIx,	,
vzniklé	vzniklý	k2eAgInPc4d1	vzniklý
pro	pro	k7c4	pro
nadaci	nadace	k1gFnSc4	nadace
Venduly	Vendula	k1gFnSc2	Vendula
Svobodové	Svobodová	k1gFnSc2	Svobodová
Kapka	kapka	k1gFnSc1	kapka
naděje	naděje	k1gFnSc1	naděje
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
práce	práce	k1gFnSc1	práce
se	se	k3xPyFc4	se
vizuálně	vizuálně	k6eAd1	vizuálně
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
kaligrafickým	kaligrafický	k2eAgInSc7d1	kaligrafický
figurativním	figurativní	k2eAgInSc7d1	figurativní
stylem	styl	k1gInSc7	styl
<g/>
,	,	kIx,	,
klasickou	klasický	k2eAgFnSc7d1	klasická
čistotou	čistota	k1gFnSc7	čistota
obrazové	obrazový	k2eAgFnSc2d1	obrazová
kompozice	kompozice	k1gFnSc2	kompozice
a	a	k8xC	a
intencionální	intencionální	k2eAgFnSc7d1	intencionální
estetizací	estetizace	k1gFnSc7	estetizace
reality	realita	k1gFnSc2	realita
barevností	barevnost	k1gFnPc2	barevnost
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
její	její	k3xOp3gFnSc7	její
absencí	absence	k1gFnSc7	absence
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
sémiologického	sémiologický	k2eAgInSc2d1	sémiologický
signifikátu	signifikát	k1gInSc2	signifikát
jsou	být	k5eAaImIp3nP	být
její	její	k3xOp3gInPc4	její
volné	volný	k2eAgInPc4d1	volný
ale	ale	k8xC	ale
i	i	k9	i
mnohé	mnohý	k2eAgFnPc4d1	mnohá
komerční	komerční	k2eAgFnPc4d1	komerční
práce	práce	k1gFnPc4	práce
charakteristické	charakteristický	k2eAgFnPc4d1	charakteristická
mnohovrstvostí	mnohovrstvost	k1gFnSc7	mnohovrstvost
<g/>
:	:	kIx,	:
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
očekávání	očekávání	k1gNnSc6	očekávání
<g/>
,	,	kIx,	,
vnímavosti	vnímavost	k1gFnSc6	vnímavost
i	i	k8xC	i
erudici	erudice	k1gFnSc6	erudice
daného	daný	k2eAgNnSc2d1	dané
publika	publikum	k1gNnSc2	publikum
otevírají	otevírat	k5eAaImIp3nP	otevírat
fotografie	fotografia	k1gFnPc1	fotografia
Lucie	Lucie	k1gFnSc2	Lucie
Robinson	Robinson	k1gMnSc1	Robinson
postupně	postupně	k6eAd1	postupně
své	svůj	k3xOyFgFnPc4	svůj
interpretační	interpretační	k2eAgFnPc4d1	interpretační
úrovně	úroveň	k1gFnPc4	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jejich	jejich	k3xOp3gInPc4	jejich
klíče	klíč	k1gInPc4	klíč
patří	patřit	k5eAaImIp3nP	patřit
Erós	erós	k1gInSc4	erós
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
svých	svůj	k3xOyFgFnPc6	svůj
podobách	podoba	k1gFnPc6	podoba
<g/>
,	,	kIx,	,
sprezzatura	sprezzatura	k1gFnSc1	sprezzatura
<g/>
,	,	kIx,	,
konotace	konotace	k1gFnSc1	konotace
archetypické	archetypický	k2eAgFnSc2d1	archetypická
mytologie	mytologie	k1gFnSc2	mytologie
<g/>
,	,	kIx,	,
nadhled	nadhled	k1gInSc1	nadhled
<g/>
,	,	kIx,	,
ironie	ironie	k1gFnSc1	ironie
až	až	k8xS	až
sarkasmus	sarkasmus	k1gInSc1	sarkasmus
<g/>
,	,	kIx,	,
afirmace	afirmace	k1gFnSc1	afirmace
diváka	divák	k1gMnSc2	divák
ve	v	k7c6	v
voyeurismu	voyeurismus	k1gInSc6	voyeurismus
či	či	k8xC	či
fetišismu	fetišismus	k1gInSc6	fetišismus
a	a	k8xC	a
sublimní	sublimní	k2eAgFnSc1d1	sublimní
psychologická	psychologický	k2eAgFnSc1d1	psychologická
manipulace	manipulace	k1gFnSc1	manipulace
–	–	k?	–
to	ten	k3xDgNnSc4	ten
vše	všechen	k3xTgNnSc4	všechen
ve	v	k7c6	v
službách	služba	k1gFnPc6	služba
přísné	přísný	k2eAgFnSc2d1	přísná
estetické	estetický	k2eAgFnSc2d1	estetická
normativity	normativita	k1gFnSc2	normativita
a	a	k8xC	a
pokusu	pokus	k1gInSc2	pokus
o	o	k7c6	o
negaci	negace	k1gFnSc6	negace
platnosti	platnost	k1gFnSc2	platnost
sociokulturní	sociokulturní	k2eAgFnSc2d1	sociokulturní
totality	totalita	k1gFnSc2	totalita
panující	panující	k2eAgFnSc2d1	panující
z	z	k7c2	z
titulu	titul	k1gInSc2	titul
fakticity	fakticita	k1gFnSc2	fakticita
a	a	k8xC	a
své	svůj	k3xOyFgFnSc2	svůj
zdánlivé	zdánlivý	k2eAgFnSc2d1	zdánlivá
evidentnosti	evidentnost	k1gFnSc2	evidentnost
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
citelnou	citelný	k2eAgFnSc7d1	citelná
noblesou	noblesa	k1gFnSc7	noblesa
jejího	její	k3xOp3gInSc2	její
přístupu	přístup	k1gInSc2	přístup
a	a	k8xC	a
selektivitou	selektivita	k1gFnSc7	selektivita
při	při	k7c6	při
výběru	výběr	k1gInSc6	výběr
komerčních	komerční	k2eAgFnPc2d1	komerční
zákazek	zákazka	k1gFnPc2	zákazka
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
šíře	šíře	k1gFnSc1	šíře
estetického	estetický	k2eAgInSc2d1	estetický
i	i	k8xC	i
intelektuálního	intelektuální	k2eAgInSc2d1	intelektuální
záběru	záběr	k1gInSc2	záběr
možným	možný	k2eAgInSc7d1	možný
vysvětlení	vysvětlení	k1gNnPc4	vysvětlení
hádanky	hádanka	k1gFnSc2	hádanka
atraktivity	atraktivita	k1gFnSc2	atraktivita
jejích	její	k3xOp3gFnPc2	její
fotografií	fotografia	k1gFnPc2	fotografia
pro	pro	k7c4	pro
tak	tak	k6eAd1	tak
nesourodé	sourodý	k2eNgFnPc4d1	nesourodá
skupiny	skupina	k1gFnPc4	skupina
recipientů	recipient	k1gInPc2	recipient
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
nadnárodní	nadnárodní	k2eAgMnPc1d1	nadnárodní
industriální	industriální	k2eAgMnPc1d1	industriální
reklamní	reklamní	k2eAgMnPc1d1	reklamní
klienti	klient	k1gMnPc1	klient
a	a	k8xC	a
mediální	mediální	k2eAgInSc4d1	mediální
trh	trh	k1gInSc4	trh
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
<g/>
,	,	kIx,	,
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
druhé	druhý	k4xOgFnSc6	druhý
pak	pak	k9	pak
sběratelé	sběratel	k1gMnPc1	sběratel
a	a	k8xC	a
evropský	evropský	k2eAgInSc4d1	evropský
trh	trh	k1gInSc4	trh
současného	současný	k2eAgNnSc2d1	současné
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
edice	edice	k1gFnSc1	edice
Lucie	Lucie	k1gFnSc2	Lucie
Robinson	Robinson	k1gMnSc1	Robinson
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
mimořádných	mimořádný	k2eAgInPc2d1	mimořádný
výsledků	výsledek	k1gInPc2	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
je	být	k5eAaImIp3nS	být
uznávána	uznáván	k2eAgFnSc1d1	uznávána
coby	coby	k?	coby
přední	přední	k2eAgFnSc1d1	přední
fotografka	fotografka	k1gFnSc1	fotografka
a	a	k8xC	a
talent	talent	k1gInSc1	talent
evropského	evropský	k2eAgNnSc2d1	Evropské
měřítka	měřítko	k1gNnSc2	měřítko
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc3	její
portrétní	portrétní	k2eAgFnSc3d1	portrétní
tvorbě	tvorba	k1gFnSc3	tvorba
bývá	bývat	k5eAaImIp3nS	bývat
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
fotografické	fotografický	k2eAgFnSc6d1	fotografická
komunitě	komunita	k1gFnSc6	komunita
vytýkán	vytýkán	k2eAgInSc1d1	vytýkán
chybějící	chybějící	k2eAgInSc1d1	chybějící
kritický	kritický	k2eAgInSc1d1	kritický
odstup	odstup	k1gInSc1	odstup
<g/>
,	,	kIx,	,
údajně	údajně	k6eAd1	údajně
jako	jako	k9	jako
výsledek	výsledek	k1gInSc1	výsledek
její	její	k3xOp3gFnSc2	její
vlastní	vlastní	k2eAgFnSc2d1	vlastní
příslušnosti	příslušnost	k1gFnSc2	příslušnost
k	k	k7c3	k
elitní	elitní	k2eAgFnSc3d1	elitní
společenské	společenský	k2eAgFnSc3d1	společenská
vrstvě	vrstva	k1gFnSc3	vrstva
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
zakázkových	zakázkový	k2eAgFnPc6d1	zakázková
pracích	práce	k1gFnPc6	práce
portrétuje	portrétovat	k5eAaImIp3nS	portrétovat
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnPc1	její
fotografie	fotografia	k1gFnPc1	fotografia
obdržely	obdržet	k5eAaPmAgFnP	obdržet
řadu	řada	k1gFnSc4	řada
domácích	domácí	k2eAgInPc2d1	domácí
i	i	k8xC	i
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
ocenění	ocenění	k1gNnPc2	ocenění
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
kterými	který	k3yRgFnPc7	který
je	být	k5eAaImIp3nS	být
také	také	k9	také
výhra	výhra	k1gFnSc1	výhra
na	na	k7c6	na
festivalu	festival	k1gInSc6	festival
Festival	festival	k1gInSc1	festival
international	internationat	k5eAaPmAgMnS	internationat
de	de	k?	de
mode	modus	k1gInSc5	modus
et	et	k?	et
de	de	k?	de
photographie	photographie	k1gFnSc2	photographie
v	v	k7c6	v
Cannes	Cannes	k1gNnSc6	Cannes
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
Prague	Pragu	k1gInSc2	Pragu
Fashion	Fashion	k1gInSc1	Fashion
Photo	Photo	k1gNnSc1	Photo
2007	[number]	k4	2007
za	za	k7c4	za
esej	esej	k1gFnSc4	esej
Czechoslovakia	Czechoslovakium	k1gNnSc2	Czechoslovakium
in	in	k?	in
Communism	Communism	k1gMnSc1	Communism
a	a	k8xC	a
trojnásobná	trojnásobný	k2eAgFnSc1d1	trojnásobná
výhra	výhra	k1gFnSc1	výhra
ceny	cena	k1gFnSc2	cena
novinářské	novinářský	k2eAgFnSc2d1	novinářská
poroty	porota	k1gFnSc2	porota
Press	Pressa	k1gFnPc2	Pressa
Award	Award	k1gMnSc1	Award
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
–	–	k?	–
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
ceny	cena	k1gFnSc2	cena
veřejnosti	veřejnost	k1gFnSc2	veřejnost
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
Schwarzkopf	Schwarzkopf	k1gInSc1	Schwarzkopf
Hairdressing	Hairdressing	k1gInSc1	Hairdressing
Awards	Awards	k1gInSc1	Awards
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
byla	být	k5eAaImAgFnS	být
uspořádána	uspořádán	k2eAgFnSc1d1	uspořádána
výstava	výstava	k1gFnSc1	výstava
jejích	její	k3xOp3gFnPc2	její
fotografií	fotografia	k1gFnPc2	fotografia
módy	móda	k1gFnSc2	móda
a	a	k8xC	a
architektury	architektura	k1gFnSc2	architektura
v	v	k7c6	v
newyorské	newyorský	k2eAgFnSc6d1	newyorská
instituci	instituce	k1gFnSc6	instituce
Museum	museum	k1gNnSc1	museum
of	of	k?	of
Arts	Artsa	k1gFnPc2	Artsa
and	and	k?	and
Design	design	k1gInSc1	design
(	(	kIx(	(
<g/>
MAD	MAD	kA	MAD
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
