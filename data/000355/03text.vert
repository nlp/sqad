<s>
Baltské	baltský	k2eAgNnSc1d1	Baltské
moře	moře	k1gNnSc1	moře
(	(	kIx(	(
<g/>
též	též	k9	též
Balt	Balt	k1gInSc1	Balt
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Ostsee	Ostsee	k1gFnSc1	Ostsee
<g/>
,	,	kIx,	,
dánsky	dánsky	k6eAd1	dánsky
Ø	Ø	k?	Ø
<g/>
,	,	kIx,	,
švédsky	švédsky	k6eAd1	švédsky
Östersjön	Östersjön	k1gInSc1	Östersjön
<g/>
,	,	kIx,	,
finsky	finsky	k6eAd1	finsky
Itämeri	Itämer	k1gMnPc1	Itämer
<g/>
,	,	kIx,	,
estonsky	estonsky	k6eAd1	estonsky
Läänemeri	Läänemer	k1gMnPc1	Läänemer
<g/>
,	,	kIx,	,
livonsky	livonsky	k6eAd1	livonsky
Vā	Vā	k1gFnSc1	Vā
mer	mer	k?	mer
<g/>
,	,	kIx,	,
sámsky	sámsky	k6eAd1	sámsky
Nuortamearra	Nuortamearra	k1gFnSc1	Nuortamearra
<g/>
,	,	kIx,	,
lotyšsky	lotyšsky	k6eAd1	lotyšsky
Baltijas	Baltijas	k1gInSc1	Baltijas
jū	jū	k?	jū
<g/>
,	,	kIx,	,
litevsky	litevsky	k6eAd1	litevsky
Baltijos	Baltijos	k1gInSc1	Baltijos
jū	jū	k?	jū
<g/>
,	,	kIx,	,
rusky	rusky	k6eAd1	rusky
Б	Б	k?	Б
м	м	k?	м
<g/>
,	,	kIx,	,
polsky	polsky	k6eAd1	polsky
Morze	Morze	k1gFnSc1	Morze
Bałtyckie	Bałtyckie	k1gFnSc1	Bałtyckie
<g/>
,	,	kIx,	,
Bałtyk	Bałtyk	k1gInSc1	Bałtyk
<g/>
,	,	kIx,	,
kašubsky	kašubsky	k6eAd1	kašubsky
Bôłt	Bôłt	k2eAgMnSc1d1	Bôłt
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
brakické	brakický	k2eAgNnSc1d1	brakické
šelfové	šelfový	k2eAgNnSc1d1	šelfové
moře	moře	k1gNnSc1	moře
Atlantského	atlantský	k2eAgInSc2d1	atlantský
oceánu	oceán	k1gInSc2	oceán
<g/>
,	,	kIx,	,
od	od	k7c2	od
nějž	jenž	k3xRgNnSc2	jenž
ho	on	k3xPp3gInSc4	on
dělí	dělit	k5eAaImIp3nP	dělit
ještě	ještě	k9	ještě
Severní	severní	k2eAgNnSc4d1	severní
moře	moře	k1gNnSc4	moře
a	a	k8xC	a
průlivy	průliv	k1gInPc1	průliv
Skagerrak	Skagerrak	k1gInSc1	Skagerrak
a	a	k8xC	a
Kattegat	Kattegat	k1gInSc1	Kattegat
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
latinského	latinský	k2eAgInSc2d1	latinský
Mare	Mar	k1gInSc2	Mar
Balticum	Balticum	k1gNnSc1	Balticum
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
je	být	k5eAaImIp3nS	být
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
od	od	k7c2	od
gótského	gótský	k2eAgInSc2d1	gótský
kmene	kmen	k1gInSc2	kmen
Baltů	Balt	k1gInPc2	Balt
<g/>
.	.	kIx.	.
</s>
<s>
Gótsky	gótsky	k6eAd1	gótsky
znamená	znamenat	k5eAaImIp3nS	znamenat
balt	balt	k1gMnSc1	balt
smělý	smělý	k2eAgMnSc1d1	smělý
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Název	název	k1gInSc1	název
zřejmě	zřejmě	k6eAd1	zřejmě
nepochází	pocházet	k5eNaImIp3nS	pocházet
z	z	k7c2	z
baltských	baltský	k2eAgInPc2d1	baltský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
podobnost	podobnost	k1gFnSc1	podobnost
jejich	jejich	k3xOp3gInSc2	jejich
výrazu	výraz	k1gInSc2	výraz
pro	pro	k7c4	pro
bílou	bílý	k2eAgFnSc4d1	bílá
barvu	barva	k1gFnSc4	barva
(	(	kIx(	(
<g/>
kmen	kmen	k1gInSc1	kmen
balt-	balt-	k?	balt-
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nápadná	nápadný	k2eAgFnSc1d1	nápadná
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
jazycích	jazyk	k1gInPc6	jazyk
místních	místní	k2eAgInPc2d1	místní
germánských	germánský	k2eAgInPc2d1	germánský
národů	národ	k1gInPc2	národ
se	se	k3xPyFc4	se
moře	moře	k1gNnSc1	moře
nazývá	nazývat	k5eAaImIp3nS	nazývat
Východním	východní	k2eAgInSc7d1	východní
<g/>
.	.	kIx.	.
</s>
<s>
Plocha	plocha	k1gFnSc1	plocha
Baltského	baltský	k2eAgNnSc2d1	Baltské
moře	moře	k1gNnSc2	moře
činí	činit	k5eAaImIp3nS	činit
422300	[number]	k4	422300
km2	km2	k4	km2
<g/>
,	,	kIx,	,
největší	veliký	k2eAgFnSc2d3	veliký
hloubky	hloubka	k1gFnSc2	hloubka
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
459	[number]	k4	459
m.	m.	k?	m.
</s>
<s>
Velké	velký	k2eAgInPc1d1	velký
výběžky	výběžek	k1gInPc1	výběžek
Baltského	baltský	k2eAgNnSc2d1	Baltské
moře	moře	k1gNnSc2	moře
představují	představovat	k5eAaImIp3nP	představovat
Botnický	botnický	k2eAgInSc4d1	botnický
záliv	záliv	k1gInSc4	záliv
<g/>
,	,	kIx,	,
Finský	finský	k2eAgInSc4d1	finský
záliv	záliv	k1gInSc4	záliv
a	a	k8xC	a
Rižský	rižský	k2eAgInSc4d1	rižský
záliv	záliv	k1gInSc4	záliv
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
ně	on	k3xPp3gMnPc4	on
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
ještě	ještě	k6eAd1	ještě
menší	malý	k2eAgInSc1d2	menší
Vyborský	Vyborský	k2eAgInSc1d1	Vyborský
záliv	záliv	k1gInSc1	záliv
<g/>
,	,	kIx,	,
Gdaňský	gdaňský	k2eAgInSc1d1	gdaňský
záliv	záliv	k1gInSc1	záliv
<g/>
,	,	kIx,	,
Pomořanský	pomořanský	k2eAgInSc1d1	pomořanský
záliv	záliv	k1gInSc1	záliv
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Fauna	fauna	k1gFnSc1	fauna
Baltského	baltský	k2eAgNnSc2d1	Baltské
moře	moře	k1gNnSc2	moře
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
atypická	atypický	k2eAgFnSc1d1	atypická
hlavně	hlavně	k9	hlavně
kvůli	kvůli	k7c3	kvůli
výskytu	výskyt	k1gInSc3	výskyt
jak	jak	k6eAd1	jak
mořských	mořský	k2eAgInPc2d1	mořský
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
sladkovodních	sladkovodní	k2eAgInPc2d1	sladkovodní
druhů	druh	k1gInPc2	druh
ryb	ryba	k1gFnPc2	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Baltské	baltský	k2eAgNnSc1d1	Baltské
moře	moře	k1gNnSc1	moře
je	být	k5eAaImIp3nS	být
se	s	k7c7	s
Severním	severní	k2eAgNnSc7d1	severní
mořem	moře	k1gNnSc7	moře
propojeno	propojit	k5eAaPmNgNnS	propojit
Kielským	kielský	k2eAgInSc7d1	kielský
průplavem	průplav	k1gInSc7	průplav
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
ostrovů	ostrov	k1gInPc2	ostrov
Baltského	baltský	k2eAgNnSc2d1	Baltské
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Známé	známý	k2eAgInPc1d1	známý
ostrovy	ostrov	k1gInPc1	ostrov
a	a	k8xC	a
souostroví	souostroví	k1gNnPc1	souostroví
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
Å	Å	k?	Å
<g/>
,	,	kIx,	,
Bornholm	Bornholm	k1gInSc1	Bornholm
<g/>
,	,	kIx,	,
Gotland	Gotland	k1gInSc1	Gotland
<g/>
,	,	kIx,	,
Haiuloto	Haiulota	k1gFnSc5	Haiulota
<g/>
,	,	kIx,	,
Saaremaa	Saaremaa	k1gMnSc1	Saaremaa
<g/>
,	,	kIx,	,
Hiiumaa	Hiiumaa	k1gMnSc1	Hiiumaa
<g/>
,	,	kIx,	,
Velassaaret	Velassaaret	k1gMnSc1	Velassaaret
<g/>
,	,	kIx,	,
Rujána	Rujána	k1gFnSc1	Rujána
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Úmoří	úmoří	k1gNnSc2	úmoří
Baltského	baltský	k2eAgNnSc2d1	Baltské
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
pevnina	pevnina	k1gFnSc1	pevnina
k	k	k7c3	k
Baltskému	baltský	k2eAgNnSc3d1	Baltské
moři	moře	k1gNnSc3	moře
přiléhá	přiléhat	k5eAaImIp3nS	přiléhat
z	z	k7c2	z
jihu	jih	k1gInSc2	jih
pobřežím	pobřeží	k1gNnSc7	pobřeží
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
Polska	Polsko	k1gNnSc2	Polsko
<g/>
,	,	kIx,	,
na	na	k7c6	na
západě	západ	k1gInSc6	západ
leží	ležet	k5eAaImIp3nS	ležet
Dánsko	Dánsko	k1gNnSc1	Dánsko
<g/>
,	,	kIx,	,
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
Švédsko	Švédsko	k1gNnSc4	Švédsko
a	a	k8xC	a
na	na	k7c6	na
východě	východ	k1gInSc6	východ
postupně	postupně	k6eAd1	postupně
(	(	kIx(	(
<g/>
od	od	k7c2	od
severu	sever	k1gInSc2	sever
k	k	k7c3	k
jihu	jih	k1gInSc3	jih
<g/>
)	)	kIx)	)
Finsko	Finsko	k1gNnSc1	Finsko
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
,	,	kIx,	,
Estonsko	Estonsko	k1gNnSc1	Estonsko
<g/>
,	,	kIx,	,
Lotyšsko	Lotyšsko	k1gNnSc1	Lotyšsko
<g/>
,	,	kIx,	,
Litva	Litva	k1gFnSc1	Litva
a	a	k8xC	a
znova	znova	k6eAd1	znova
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Patrně	patrně	k6eAd1	patrně
nejdůležitějšími	důležitý	k2eAgFnPc7d3	nejdůležitější
řekami	řeka	k1gFnPc7	řeka
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
do	do	k7c2	do
Baltského	baltský	k2eAgNnSc2d1	Baltské
moře	moře	k1gNnSc2	moře
přivádějí	přivádět	k5eAaImIp3nP	přivádět
své	svůj	k3xOyFgFnPc4	svůj
vody	voda	k1gFnPc4	voda
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
Něva	Něva	k1gFnSc1	Něva
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
Daugava	Daugava	k1gFnSc1	Daugava
v	v	k7c6	v
Lotyšsku	Lotyšsko	k1gNnSc6	Lotyšsko
<g/>
,	,	kIx,	,
Visla	visnout	k5eAaImAgFnS	visnout
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
,	,	kIx,	,
Němen	Němen	k1gInSc4	Němen
na	na	k7c4	na
pomezí	pomezí	k1gNnSc4	pomezí
Litvy	Litva	k1gFnSc2	Litva
a	a	k8xC	a
Ruska	Rusko	k1gNnSc2	Rusko
a	a	k8xC	a
Odra	Odra	k1gFnSc1	Odra
na	na	k7c6	na
pomezí	pomezí	k1gNnSc6	pomezí
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
do	do	k7c2	do
Baltského	baltský	k2eAgNnSc2d1	Baltské
moře	moře	k1gNnSc2	moře
přitéká	přitékat	k5eAaImIp3nS	přitékat
hodně	hodně	k6eAd1	hodně
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
výpar	výpar	k1gInSc1	výpar
je	být	k5eAaImIp3nS	být
nízký	nízký	k2eAgInSc1d1	nízký
<g/>
,	,	kIx,	,
salinita	salinita	k1gFnSc1	salinita
je	být	k5eAaImIp3nS	být
nižší	nízký	k2eAgFnSc1d2	nižší
než	než	k8xS	než
v	v	k7c6	v
okolním	okolní	k2eAgInSc6d1	okolní
oceánu	oceán	k1gInSc6	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Baltské	baltský	k2eAgNnSc1d1	Baltské
moře	moře	k1gNnSc1	moře
je	být	k5eAaImIp3nS	být
nejméně	málo	k6eAd3	málo
slaným	slaný	k2eAgNnSc7d1	slané
mořem	moře	k1gNnSc7	moře
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Baltském	baltský	k2eAgNnSc6d1	Baltské
moři	moře	k1gNnSc6	moře
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
k	k	k7c3	k
potopení	potopení	k1gNnSc3	potopení
trajektu	trajekt	k1gInSc2	trajekt
Estonia	Estonium	k1gNnSc2	Estonium
<g/>
.	.	kIx.	.
</s>
<s>
Moře	moře	k1gNnSc1	moře
se	se	k3xPyFc4	se
potýká	potýkat	k5eAaImIp3nS	potýkat
s	s	k7c7	s
velkým	velký	k2eAgNnSc7d1	velké
znečištěním	znečištění	k1gNnSc7	znečištění
vod	voda	k1gFnPc2	voda
<g/>
,	,	kIx,	,
předpoklady	předpoklad	k1gInPc1	předpoklad
pro	pro	k7c4	pro
znečištění	znečištění	k1gNnSc4	znečištění
tvoří	tvořit	k5eAaImIp3nP	tvořit
i	i	k9	i
přírodní	přírodní	k2eAgFnPc4d1	přírodní
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
svému	svůj	k3xOyFgInSc3	svůj
relativně	relativně	k6eAd1	relativně
mělkému	mělký	k2eAgInSc3d1	mělký
dnu	den	k1gInSc3	den
a	a	k8xC	a
malé	malý	k2eAgFnSc3d1	malá
výměně	výměna	k1gFnSc3	výměna
vod	voda	k1gFnPc2	voda
se	s	k7c7	s
sousedním	sousední	k2eAgNnSc7d1	sousední
Severním	severní	k2eAgNnSc7d1	severní
mořem	moře	k1gNnSc7	moře
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
drží	držet	k5eAaImIp3nP	držet
znečišťující	znečišťující	k2eAgFnPc4d1	znečišťující
látky	látka	k1gFnPc4	látka
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
podíl	podíl	k1gInSc1	podíl
na	na	k7c6	na
dnešním	dnešní	k2eAgInSc6d1	dnešní
neutěšeném	utěšený	k2eNgInSc6d1	neutěšený
stavu	stav	k1gInSc6	stav
má	mít	k5eAaImIp3nS	mít
bývalý	bývalý	k2eAgInSc1d1	bývalý
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
a	a	k8xC	a
země	země	k1gFnSc1	země
východního	východní	k2eAgInSc2d1	východní
bloku	blok	k1gInSc2	blok
<g/>
.	.	kIx.	.
</s>
<s>
Absence	absence	k1gFnSc1	absence
ekologických	ekologický	k2eAgFnPc2d1	ekologická
norem	norma	k1gFnPc2	norma
umožnila	umožnit	k5eAaPmAgFnS	umožnit
průmyslu	průmysl	k1gInSc2	průmysl
a	a	k8xC	a
továrnám	továrna	k1gFnPc3	továrna
vypouštět	vypouštět	k5eAaImF	vypouštět
odpad	odpad	k1gInSc4	odpad
přímo	přímo	k6eAd1	přímo
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
do	do	k7c2	do
vodních	vodní	k2eAgInPc2d1	vodní
toků	tok	k1gInPc2	tok
do	do	k7c2	do
něj	on	k3xPp3gNnSc2	on
ústících	ústící	k2eAgNnPc2d1	ústící
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
baltské	baltský	k2eAgFnSc6d1	Baltská
oblasti	oblast	k1gFnSc6	oblast
žije	žít	k5eAaImIp3nS	žít
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Státy	stát	k1gInPc1	stát
kolem	kolem	k7c2	kolem
Baltského	baltský	k2eAgNnSc2d1	Baltské
moře	moře	k1gNnSc2	moře
jsou	být	k5eAaImIp3nP	být
všechny	všechen	k3xTgInPc1	všechen
industrializované	industrializovaný	k2eAgInPc1d1	industrializovaný
a	a	k8xC	a
provozují	provozovat	k5eAaImIp3nP	provozovat
intenzivní	intenzivní	k2eAgNnSc4d1	intenzivní
hospodářství	hospodářství	k1gNnSc4	hospodářství
založené	založený	k2eAgFnSc2d1	založená
na	na	k7c6	na
chemickém	chemický	k2eAgNnSc6d1	chemické
hnojení	hnojení	k1gNnSc6	hnojení
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgInSc7d3	veliký
problémem	problém	k1gInSc7	problém
jsou	být	k5eAaImIp3nP	být
přítoky	přítok	k1gInPc4	přítok
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
znečištěné	znečištěný	k2eAgInPc1d1	znečištěný
od	od	k7c2	od
průmyslu	průmysl	k1gInSc2	průmysl
a	a	k8xC	a
hnojiv	hnojivo	k1gNnPc2	hnojivo
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
eutrofizaci	eutrofizace	k1gFnSc4	eutrofizace
a	a	k8xC	a
vyživují	vyživovat	k5eAaImIp3nP	vyživovat
v	v	k7c6	v
moři	moře	k1gNnSc6	moře
plankton	plankton	k1gInSc1	plankton
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
planktonu	plankton	k1gInSc2	plankton
živeného	živený	k2eAgInSc2d1	živený
z	z	k7c2	z
hnojiv	hnojivo	k1gNnPc2	hnojivo
spotřebovává	spotřebovávat	k5eAaImIp3nS	spotřebovávat
v	v	k7c6	v
moři	moře	k1gNnSc6	moře
kyslík	kyslík	k1gInSc4	kyslík
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nP	tvořit
prostředí	prostředí	k1gNnSc4	prostředí
nehostinné	hostinný	k2eNgNnSc4d1	nehostinné
pro	pro	k7c4	pro
veškerý	veškerý	k3xTgInSc4	veškerý
další	další	k2eAgInSc4d1	další
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
Baltu	Balt	k1gInSc2	Balt
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
rušná	rušný	k2eAgFnSc1d1	rušná
lodní	lodní	k2eAgFnSc1d1	lodní
doprava	doprava	k1gFnSc1	doprava
<g/>
,	,	kIx,	,
s	s	k7c7	s
čímž	což	k3yQnSc7	což
souvisí	souviset	k5eAaImIp3nS	souviset
i	i	k9	i
občasné	občasný	k2eAgFnPc4d1	občasná
havárie	havárie	k1gFnPc4	havárie
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
se	se	k3xPyFc4	se
potopila	potopit	k5eAaPmAgFnS	potopit
čínská	čínský	k2eAgFnSc1d1	čínská
nákladní	nákladní	k2eAgFnSc1d1	nákladní
loď	loď	k1gFnSc1	loď
Fu	fu	k0	fu
Shan	Shana	k1gFnPc2	Shana
Hai	Hai	k1gMnPc4	Hai
u	u	k7c2	u
břehů	břeh	k1gInPc2	břeh
dánského	dánský	k2eAgInSc2d1	dánský
ostrova	ostrov	k1gInSc2	ostrov
Bornholm	Bornholma	k1gFnPc2	Bornholma
<g/>
.	.	kIx.	.
</s>
<s>
Uniklo	uniknout	k5eAaPmAgNnS	uniknout
odhadem	odhad	k1gInSc7	odhad
1600	[number]	k4	1600
tun	tuna	k1gFnPc2	tuna
nafty	nafta	k1gFnSc2	nafta
a	a	k8xC	a
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
až	až	k9	až
66	[number]	k4	66
000	[number]	k4	000
tun	tuna	k1gFnPc2	tuna
umělých	umělý	k2eAgNnPc2d1	umělé
hnojiv	hnojivo	k1gNnPc2	hnojivo
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
loď	loď	k1gFnSc1	loď
převážela	převážet	k5eAaImAgFnS	převážet
<g/>
,	,	kIx,	,
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
skvrna	skvrna	k1gFnSc1	skvrna
byla	být	k5eAaImAgFnS	být
velká	velká	k1gFnSc1	velká
15	[number]	k4	15
čtverečních	čtvereční	k2eAgFnPc2d1	čtvereční
námořních	námořní	k2eAgFnPc2d1	námořní
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
</s>
<s>
Doslova	doslova	k6eAd1	doslova
časovanou	časovaný	k2eAgFnSc7d1	časovaná
bombou	bomba	k1gFnSc7	bomba
na	na	k7c6	na
dně	dno	k1gNnSc6	dno
Baltu	Balt	k1gInSc2	Balt
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
chemické	chemický	k2eAgFnPc1d1	chemická
zbraně	zbraň	k1gFnPc1	zbraň
<g/>
,	,	kIx,	,
kterých	který	k3yQgFnPc2	který
se	se	k3xPyFc4	se
Německo	Německo	k1gNnSc1	Německo
zbavovalo	zbavovat	k5eAaImAgNnS	zbavovat
bezprostředně	bezprostředně	k6eAd1	bezprostředně
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
odhadů	odhad	k1gInPc2	odhad
se	se	k3xPyFc4	se
u	u	k7c2	u
německých	německý	k2eAgInPc2d1	německý
břehů	břeh	k1gInPc2	břeh
nachází	nacházet	k5eAaImIp3nS	nacházet
až	až	k9	až
stovky	stovka	k1gFnPc4	stovka
tisíc	tisíc	k4xCgInPc2	tisíc
tun	tuna	k1gFnPc2	tuna
nádrží	nádrž	k1gFnPc2	nádrž
a	a	k8xC	a
munice	munice	k1gFnSc2	munice
obsahující	obsahující	k2eAgInSc1d1	obsahující
tabun	tabun	k1gInSc1	tabun
<g/>
,	,	kIx,	,
yperit	yperit	k1gInSc1	yperit
nebo	nebo	k8xC	nebo
Cyklon	cyklon	k1gInSc1	cyklon
B	B	kA	B
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
používal	používat	k5eAaImAgInS	používat
v	v	k7c6	v
koncentračních	koncentrační	k2eAgInPc6d1	koncentrační
táborech	tábor	k1gInPc6	tábor
<g/>
.	.	kIx.	.
</s>
