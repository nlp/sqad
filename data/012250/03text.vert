<p>
<s>
Aloe	aloe	k1gFnSc1	aloe
Blacc	Blacc	k1gFnSc1	Blacc
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Egbert	Egbert	k1gInSc1	Egbert
Nathaniel	Nathaniel	k1gInSc1	Nathaniel
Dawkins	Dawkins	k1gInSc4	Dawkins
III	III	kA	III
(	(	kIx(	(
<g/>
*	*	kIx~	*
7	[number]	k4	7
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1979	[number]	k4	1979
<g/>
,	,	kIx,	,
Laguna	laguna	k1gFnSc1	laguna
Hills	Hills	k1gInSc1	Hills
<g/>
,	,	kIx,	,
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
zpěvák	zpěvák	k1gMnSc1	zpěvák
a	a	k8xC	a
rapper	rapper	k1gMnSc1	rapper
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
panamským	panamský	k2eAgMnPc3d1	panamský
rodičům	rodič	k1gMnPc3	rodič
v	v	k7c6	v
Orange	Orange	k1gFnSc6	Orange
County	Counta	k1gFnSc2	Counta
<g/>
,	,	kIx,	,
vyrůstal	vyrůstat	k5eAaImAgMnS	vyrůstat
v	v	k7c6	v
Laguna	laguna	k1gFnSc1	laguna
Hills	Hills	k1gInSc4	Hills
a	a	k8xC	a
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c6	na
Univerzitě	univerzita	k1gFnSc6	univerzita
Jižní	jižní	k2eAgFnSc2d1	jižní
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
<g/>
.	.	kIx.	.
</s>
<s>
Hudbě	hudba	k1gFnSc3	hudba
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaImAgMnS	věnovat
již	již	k6eAd1	již
od	od	k7c2	od
dětství	dětství	k1gNnSc2	dětství
<g/>
,	,	kIx,	,
prvním	první	k4xOgInSc7	první
nástrojem	nástroj	k1gInSc7	nástroj
mu	on	k3xPp3gNnSc3	on
byla	být	k5eAaImAgFnS	být
trubka	trubka	k1gFnSc1	trubka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
začal	začít	k5eAaPmAgInS	začít
s	s	k7c7	s
producentem	producent	k1gMnSc7	producent
Exilem	exil	k1gInSc7	exil
vystupovat	vystupovat	k5eAaImF	vystupovat
v	v	k7c6	v
duu	duo	k1gNnSc6	duo
nazvaném	nazvaný	k2eAgNnSc6d1	nazvané
Emanon	Emanon	k1gInSc1	Emanon
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
první	první	k4xOgNnSc4	první
sólové	sólový	k2eAgNnSc4d1	sólové
studiové	studiový	k2eAgNnSc4d1	studiové
album	album	k1gNnSc4	album
Shine	Shin	k1gInSc5	Shin
Through	Througha	k1gFnPc2	Througha
vydal	vydat	k5eAaPmAgInS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
následovala	následovat	k5eAaImAgFnS	následovat
alba	alba	k1gFnSc1	alba
Good	Good	k1gMnSc1	Good
Things	Things	k1gInSc1	Things
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
a	a	k8xC	a
Lift	lift	k1gInSc1	lift
Your	Youra	k1gFnPc2	Youra
Spirit	Spirita	k1gFnPc2	Spirita
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Aloe	aloe	k1gFnSc2	aloe
Blacc	Blacc	k1gInSc1	Blacc
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgInSc1d1	oficiální
web	web	k1gInSc1	web
</s>
</p>
