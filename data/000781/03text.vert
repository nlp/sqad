<s>
Hamleti	Hamlet	k5eAaPmF	Hamlet
je	on	k3xPp3gNnSc4	on
české	český	k2eAgNnSc4d1	české
hudební	hudební	k2eAgNnSc4d1	hudební
uskupení	uskupení	k1gNnSc4	uskupení
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
v	v	k7c6	v
divadelním	divadelní	k2eAgInSc6d1	divadelní
klubu	klub	k1gInSc6	klub
divadla	divadlo	k1gNnSc2	divadlo
ABC	ABC	kA	ABC
jako	jako	k8xC	jako
mejdanové	mejdanový	k2eAgNnSc1d1	mejdanový
těleso	těleso	k1gNnSc1	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
skupiny	skupina	k1gFnSc2	skupina
jsou	být	k5eAaImIp3nP	být
Aleš	Aleš	k1gMnSc1	Aleš
Háma	Háma	k1gMnSc1	Háma
(	(	kIx(	(
<g/>
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
kytara	kytara	k1gFnSc1	kytara
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Hardy	Harda	k1gFnSc2	Harda
Marzuki	Marzuk	k1gFnSc2	Marzuk
(	(	kIx(	(
<g/>
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
kytara	kytara	k1gFnSc1	kytara
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jakub	Jakub	k1gMnSc1	Jakub
Wehrenberg	Wehrenberg	k1gMnSc1	Wehrenberg
(	(	kIx(	(
<g/>
basa	basa	k1gFnSc1	basa
<g/>
)	)	kIx)	)
a	a	k8xC	a
Dalibor	Dalibor	k1gMnSc1	Dalibor
Gondík	Gondík	k1gMnSc1	Gondík
(	(	kIx(	(
<g/>
bicí	bicí	k2eAgFnSc1d1	bicí
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hudební	hudební	k2eAgNnSc1d1	hudební
uskupení	uskupení	k1gNnSc1	uskupení
Hamleti	Hamle	k1gNnSc3	Hamle
nemá	mít	k5eNaImIp3nS	mít
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
repertoáru	repertoár	k1gInSc6	repertoár
žádné	žádný	k3yNgFnSc2	žádný
vlastní	vlastní	k2eAgFnSc2d1	vlastní
skladby	skladba	k1gFnSc2	skladba
<g/>
,	,	kIx,	,
na	na	k7c6	na
svých	svůj	k3xOyFgNnPc6	svůj
vystoupeních	vystoupení	k1gNnPc6	vystoupení
hrají	hrát	k5eAaImIp3nP	hrát
skladby	skladba	k1gFnPc1	skladba
z	z	k7c2	z
repertoáru	repertoár	k1gInSc2	repertoár
jiných	jiný	k2eAgFnPc2d1	jiná
českých	český	k2eAgFnPc2d1	Česká
i	i	k8xC	i
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
hudebních	hudební	k2eAgFnPc2d1	hudební
skupin	skupina	k1gFnPc2	skupina
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
Olympic	Olympice	k1gFnPc2	Olympice
<g/>
,	,	kIx,	,
Buty	Buty	k1gInPc1	Buty
<g/>
,	,	kIx,	,
Lucie	Lucie	k1gFnPc1	Lucie
<g/>
,	,	kIx,	,
Beatles	Beatles	k1gFnPc1	Beatles
<g/>
,	,	kIx,	,
Smokie	Smokie	k1gFnPc1	Smokie
<g/>
,	,	kIx,	,
Kiss	Kiss	k1gInSc1	Kiss
<g/>
,	,	kIx,	,
ABBA	ABBA	kA	ABBA
a	a	k8xC	a
jiných	jiný	k2eAgFnPc6d1	jiná
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
stránka	stránka	k1gFnSc1	stránka
skupiny	skupina	k1gFnSc2	skupina
Hamleti	Hamle	k1gNnSc3	Hamle
</s>
