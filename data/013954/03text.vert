<s>
Johann	Johann	k1gMnSc1
Christoph	Christoph	k1gMnSc1
Pepusch	Pepusch	k1gMnSc1
</s>
<s>
Johann	Johann	k1gMnSc1
Christoph	Christoph	k1gMnSc1
Pepusch	Pepusch	k1gMnSc1
Narození	narození	k1gNnPc2
</s>
<s>
1667	#num#	k4
<g/>
Berlín	Berlín	k1gInSc1
Úmrtí	úmrtí	k1gNnSc2
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
<g/>
jul	jul	k?
<g/>
.	.	kIx.
/	/	kIx~
31	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1752	#num#	k4
<g/>
greg	grega	k1gFnPc2
<g/>
.	.	kIx.
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
84	#num#	k4
<g/>
–	–	k?
<g/>
85	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Londýn	Londýn	k1gInSc1
Místo	místo	k7c2
pohřbení	pohřbení	k1gNnSc2
</s>
<s>
Brompton	Brompton	k1gInSc1
Cemetery	Cemeter	k1gMnPc7
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
</s>
<s>
Oxfordská	oxfordský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
hudební	hudební	k2eAgMnSc1d1
skladatel	skladatel	k1gMnSc1
<g/>
,	,	kIx,
muzikolog	muzikolog	k1gMnSc1
a	a	k8xC
hudební	hudební	k2eAgMnSc1d1
teoretik	teoretik	k1gMnSc1
Ocenění	ocenění	k1gNnSc2
</s>
<s>
člen	člen	k1gMnSc1
Královské	královský	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Johann	Johann	k1gMnSc1
Christoph	Christoph	k1gMnSc1
Pepusch	Pepusch	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
1667	#num#	k4
<g/>
,	,	kIx,
Berlín	Berlín	k1gInSc1
-	-	kIx~
20	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1752	#num#	k4
<g/>
,	,	kIx,
Londýn	Londýn	k1gInSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
hudební	hudební	k2eAgMnSc1d1
skladatel	skladatel	k1gMnSc1
a	a	k8xC
hudební	hudební	k2eAgMnSc1d1
vědec	vědec	k1gMnSc1
německého	německý	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
většinu	většina	k1gFnSc4
svého	svůj	k3xOyFgInSc2
tvůrčího	tvůrčí	k2eAgInSc2d1
života	život	k1gInSc2
strávil	strávit	k5eAaPmAgMnS
v	v	k7c6
Anglii	Anglie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7
nejslavnějším	slavný	k2eAgInSc7d3
dílem	díl	k1gInSc7
je	být	k5eAaImIp3nS
Žebrácká	žebrácký	k2eAgFnSc1d1
opera	opera	k1gFnSc1
(	(	kIx(
<g/>
1728	#num#	k4
<g/>
)	)	kIx)
na	na	k7c4
libreto	libreto	k1gNnSc4
Johna	John	k1gMnSc2
Gaye	gay	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Získal	získat	k5eAaPmAgMnS
doktorát	doktorát	k1gInSc4
hudby	hudba	k1gFnSc2
na	na	k7c6
univerzitě	univerzita	k1gFnSc6
v	v	k7c6
Oxfordu	Oxford	k1gInSc6
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
hudební	hudební	k2eAgMnSc1d1
vědec	vědec	k1gMnSc1
se	se	k3xPyFc4
zabýval	zabývat	k5eAaImAgMnS
především	především	k6eAd1
dějinami	dějiny	k1gFnPc7
hudby	hudba	k1gFnSc2
alžbětinské	alžbětinský	k2eAgFnSc2d1
epochy	epocha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Johann	Johann	k1gMnSc1
Christoph	Christoph	k1gMnSc1
Pepusch	Pepusch	k1gMnSc1
na	na	k7c6
německé	německý	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Johann	Johann	k1gInSc4
Christoph	Christoph	k1gInSc4
Pepusch	Pepuscha	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ola	ola	k?
<g/>
2002146839	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
115845917	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
8089	#num#	k4
3929	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
50047634	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
10034259	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
50047634	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Hudba	hudba	k1gFnSc1
</s>
