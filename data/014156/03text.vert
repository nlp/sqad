<s>
Jitrovník	Jitrovník	k1gMnSc1
</s>
<s>
Jitrovník	Jitrovník	k1gInSc1
Vrchol	vrchol	k1gInSc1
</s>
<s>
509	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Seznamy	seznam	k1gInPc4
</s>
<s>
Hory	hora	k1gFnSc2
Šluknovské	šluknovský	k2eAgFnSc2d1
pahorkatiny	pahorkatina	k1gFnSc2
Poznámka	poznámka	k1gFnSc1
</s>
<s>
vyhlídkové	vyhlídkový	k2eAgNnSc1d1
místo	místo	k1gNnSc1
Poloha	poloha	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc4
Pohoří	pohořet	k5eAaPmIp3nS
</s>
<s>
Šluknovská	šluknovský	k2eAgFnSc1d1
pahorkatina	pahorkatina	k1gFnSc1
/	/	kIx~
.	.	kIx.
/	/	kIx~
Šenovská	šenovský	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
/	/	kIx~
Hrazenská	Hrazenský	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
či	či	k8xC
též	též	k9
</s>
<s>
Šluknovská	šluknovský	k2eAgFnSc1d1
pahorkatina	pahorkatina	k1gFnSc1
/	/	kIx~
Šenovská	šenovský	k2eAgFnSc1d1
pahorkatina	pahorkatina	k1gFnSc1
/	/	kIx~
Hrazenská	Hrazenský	k2eAgFnSc1d1
pahorkatina	pahorkatina	k1gFnSc1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
P	P	kA
1	#num#	k4
<g/>
]	]	kIx)
Souřadnice	souřadnice	k1gFnSc1
</s>
<s>
51	#num#	k4
<g/>
°	°	k?
<g/>
0	#num#	k4
<g/>
′	′	k?
<g/>
7	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
14	#num#	k4
<g/>
°	°	k?
<g/>
30	#num#	k4
<g/>
′	′	k?
<g/>
28	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Jitrovník	Jitrovník	k1gMnSc1
</s>
<s>
Hornina	hornina	k1gFnSc1
</s>
<s>
žula	žula	k1gFnSc1
Povodí	povodí	k1gNnSc2
</s>
<s>
Labe	Labe	k1gNnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Jitrovník	Jitrovník	k1gInSc1
(	(	kIx(
<g/>
také	také	k9
Pytlák	pytlák	k1gMnSc1
<g/>
,	,	kIx,
německy	německy	k6eAd1
Jüttel	Jüttel	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
509	#num#	k4
metrů	metr	k1gInPc2
vysoký	vysoký	k2eAgInSc1d1
kopec	kopec	k1gInSc1
v	v	k7c6
okrese	okres	k1gInSc6
Děčín	Děčín	k1gInSc4
poblíž	poblíž	k7c2
Šluknova	Šluknov	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
pověsti	pověst	k1gFnSc2
se	se	k3xPyFc4
zde	zde	k6eAd1
prováděly	provádět	k5eAaImAgFnP
čarodějnické	čarodějnický	k2eAgFnPc1d1
reje	rej	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgInS
zde	zde	k6eAd1
pionýrský	pionýrský	k2eAgInSc1d1
tábor	tábor	k1gInSc1
ZPA	ZPA	kA
Praha	Praha	k1gFnSc1
(	(	kIx(
<g/>
1962	#num#	k4
–	–	k?
1990	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dnes	dnes	k6eAd1
už	už	k6eAd1
jsou	být	k5eAaImIp3nP
známky	známka	k1gFnPc1
jen	jen	k9
velice	velice	k6eAd1
chatrné	chatrný	k2eAgFnPc1d1
jen	jen	k9
z	z	k7c2
pahýlů	pahýl	k1gMnPc2
budovy	budova	k1gFnSc2
zarůstající	zarůstající	k2eAgFnSc7d1
vegetací	vegetace	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
kopci	kopec	k1gInSc6
je	být	k5eAaImIp3nS
vyhlídkové	vyhlídkový	k2eAgNnSc4d1
místo	místo	k1gNnSc4
do	do	k7c2
krajiny	krajina	k1gFnSc2
<g/>
,	,	kIx,
na	na	k7c4
něž	jenž	k3xRgMnPc4
se	se	k3xPyFc4
dá	dát	k5eAaPmIp3nS
dostat	dostat	k5eAaPmF
po	po	k7c6
turistické	turistický	k2eAgFnSc6d1
značce	značka	k1gFnSc6
od	od	k7c2
autobusové	autobusový	k2eAgFnSc2d1
zastávky	zastávka	k1gFnSc2
„	„	k?
<g/>
Království	království	k1gNnSc1
u	u	k7c2
Šluknova	Šluknov	k1gInSc2
<g/>
“	“	k?
nebo	nebo	k8xC
přímo	přímo	k6eAd1
ze	z	k7c2
Šluknova	Šluknov	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
↑	↑	k?
Rozdíly	rozdíl	k1gInPc1
v	v	k7c6
pojmenování	pojmenování	k1gNnSc6
vychází	vycházet	k5eAaImIp3nS
z	z	k7c2
odlišností	odlišnost	k1gFnPc2
ve	v	k7c6
dvou	dva	k4xCgInPc6
používaných	používaný	k2eAgInPc6d1
systémech	systém	k1gInPc6
geomorfologického	geomorfologický	k2eAgNnSc2d1
členění	členění	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
Demkova	Demkův	k2eAgNnSc2d1
pojetí	pojetí	k1gNnSc2
jsou	být	k5eAaImIp3nP
jednotky	jednotka	k1gFnPc1
nazývány	nazývat	k5eAaImNgFnP
Šenovská	šenovský	k2eAgFnSc1d1
a	a	k8xC
Hrazenská	Hrazenský	k2eAgFnSc1d1
pahorkatina	pahorkatina	k1gFnSc1
<g/>
,	,	kIx,
podle	podle	k7c2
pojetí	pojetí	k1gNnSc2
Balatky	balatka	k1gFnSc2
a	a	k8xC
Kalvody	Kalvoda	k1gMnSc2
pak	pak	k6eAd1
Šenovská	šenovský	k2eAgFnSc1d1
a	a	k8xC
Hrazenská	Hrazenský	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
přičemž	přičemž	k6eAd1
je	být	k5eAaImIp3nS
řadí	řadit	k5eAaImIp3nS
o	o	k7c4
úroveň	úroveň	k1gFnSc4
níže	nízce	k6eAd2
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Břetislav	Břetislav	k1gMnSc1
Balatka	balatka	k1gFnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
Kalvoda	Kalvoda	k1gMnSc1
-	-	kIx~
Geomorfologické	geomorfologický	k2eAgNnSc1d1
členění	členění	k1gNnSc1
reliéfu	reliéf	k1gInSc2
Čech	Čechy	k1gFnPc2
(	(	kIx(
<g/>
Kartografie	kartografie	k1gFnSc1
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7011	#num#	k4
<g/>
-	-	kIx~
<g/>
913	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
)	)	kIx)
<g/>
↑	↑	k?
DEMEK	DEMEK	kA
<g/>
,	,	kIx,
Jaromír	Jaromír	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zeměpisný	zeměpisný	k2eAgInSc1d1
lexikon	lexikon	k1gInSc1
ČSR	ČSR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hory	hora	k1gFnSc2
a	a	k8xC
nížiny	nížina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
1987	#num#	k4
<g/>
.	.	kIx.
584	#num#	k4
s.	s.	k?
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Šluknovský	šluknovský	k2eAgInSc1d1
výběžek	výběžek	k1gInSc1
</s>
