<p>
<s>
Chlopenní	chlopenní	k2eAgFnSc1d1	chlopenní
vada	vada	k1gFnSc1	vada
označuje	označovat	k5eAaImIp3nS	označovat
stav	stav	k1gInSc4	stav
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jedna	jeden	k4xCgNnPc1	jeden
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
srdečních	srdeční	k2eAgFnPc2d1	srdeční
chlopní	chlopeň	k1gFnPc2	chlopeň
nepracuje	pracovat	k5eNaImIp3nS	pracovat
správně	správně	k6eAd1	správně
<g/>
.	.	kIx.	.
</s>
<s>
Dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
nedostatečnému	dostatečný	k2eNgNnSc3d1	nedostatečné
otevírání	otevírání	k1gNnSc3	otevírání
a	a	k8xC	a
kladou	klást	k5eAaImIp3nP	klást
tak	tak	k6eAd1	tak
zvýšený	zvýšený	k2eAgInSc4d1	zvýšený
odpor	odpor	k1gInSc4	odpor
toku	tok	k1gInSc2	tok
krev	krev	k1gFnSc1	krev
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
při	při	k7c6	při
uzavření	uzavření	k1gNnSc6	uzavření
dostatečně	dostatečně	k6eAd1	dostatečně
netěsní	těsnit	k5eNaImIp3nS	těsnit
a	a	k8xC	a
krev	krev	k1gFnSc1	krev
se	se	k3xPyFc4	se
netěsností	netěsnost	k1gFnPc2	netěsnost
vrací	vracet	k5eAaImIp3nS	vracet
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
situace	situace	k1gFnPc1	situace
vedou	vést	k5eAaImIp3nP	vést
k	k	k7c3	k
přetěžování	přetěžování	k1gNnSc3	přetěžování
srdce	srdce	k1gNnSc2	srdce
a	a	k8xC	a
časem	čas	k1gInSc7	čas
ke	k	k7c3	k
zhoršení	zhoršení	k1gNnSc3	zhoršení
čerpací	čerpací	k2eAgFnSc2d1	čerpací
funkce	funkce	k1gFnSc2	funkce
srdce	srdce	k1gNnSc2	srdce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příčiny	příčina	k1gFnPc4	příčina
==	==	k?	==
</s>
</p>
<p>
<s>
Nejčastější	častý	k2eAgFnPc1d3	nejčastější
příčiny	příčina	k1gFnPc1	příčina
chlopenních	chlopenní	k2eAgFnPc2d1	chlopenní
vad	vada	k1gFnPc2	vada
obecně	obecně	k6eAd1	obecně
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
degenerativní	degenerativní	k2eAgFnPc1d1	degenerativní
změny	změna	k1gFnPc1	změna
</s>
</p>
<p>
<s>
vrozené	vrozený	k2eAgFnPc1d1	vrozená
vady	vada	k1gFnPc1	vada
chlopní	chlopeň	k1gFnPc2	chlopeň
</s>
</p>
<p>
<s>
prodělaná	prodělaný	k2eAgFnSc1d1	prodělaná
revmatická	revmatický	k2eAgFnSc1d1	revmatická
horečka	horečka	k1gFnSc1	horečka
</s>
</p>
<p>
<s>
infekce	infekce	k1gFnSc1	infekce
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
infekční	infekční	k2eAgFnSc1d1	infekční
endokarditida	endokarditida	k1gFnSc1	endokarditida
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
sekundárně	sekundárně	k6eAd1	sekundárně
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
poruše	porucha	k1gFnSc3	porucha
funkce	funkce	k1gFnSc2	funkce
chlopní	chlopeň	k1gFnPc2	chlopeň
řada	řada	k1gFnSc1	řada
stavů	stav	k1gInPc2	stav
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
spojeny	spojen	k2eAgInPc1d1	spojen
s	s	k7c7	s
rozšiřováním	rozšiřování	k1gNnSc7	rozšiřování
(	(	kIx(	(
<g/>
dilatací	dilatace	k1gFnPc2	dilatace
<g/>
)	)	kIx)	)
srdečních	srdeční	k2eAgInPc2d1	srdeční
oddílů	oddíl	k1gInPc2	oddíl
a	a	k8xC	a
následnému	následný	k2eAgInSc3d1	následný
vzniku	vznik	k1gInSc3	vznik
relativní	relativní	k2eAgFnSc2d1	relativní
nedostatečnosti	nedostatečnost	k1gFnSc2	nedostatečnost
chlopenních	chlopenní	k2eAgInPc2d1	chlopenní
cípů	cíp	k1gInPc2	cíp
</s>
</p>
<p>
<s>
==	==	k?	==
Dělení	dělení	k1gNnSc1	dělení
==	==	k?	==
</s>
</p>
<p>
<s>
Obecně	obecně	k6eAd1	obecně
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
</s>
</p>
<p>
<s>
zúžení	zúžení	k1gNnSc1	zúžení
-	-	kIx~	-
stenotické	stenotický	k2eAgFnPc1d1	stenotický
vady	vada	k1gFnPc1	vada
(	(	kIx(	(
<g/>
stenózy	stenóza	k1gFnPc1	stenóza
<g/>
)	)	kIx)	)
-	-	kIx~	-
srůstem	srůst	k1gInSc7	srůst
cípů	cíp	k1gInPc2	cíp
chlopní	chlopeň	k1gFnSc7	chlopeň
či	či	k8xC	či
ukládáním	ukládání	k1gNnSc7	ukládání
abnormálního	abnormální	k2eAgInSc2d1	abnormální
materiálu	materiál	k1gInSc2	materiál
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
nedostatečnému	dostatečný	k2eNgNnSc3d1	nedostatečné
otevírání	otevírání	k1gNnSc3	otevírání
a	a	k8xC	a
tedy	tedy	k9	tedy
k	k	k7c3	k
zúžení	zúžení	k1gNnSc3	zúžení
průsvitu	průsvit	k1gInSc2	průsvit
chlopně	chlopeň	k1gFnSc2	chlopeň
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
klade	klást	k5eAaImIp3nS	klást
toku	tok	k1gInSc3	tok
krve	krev	k1gFnSc2	krev
zvýšený	zvýšený	k2eAgInSc1d1	zvýšený
odpor	odpor	k1gInSc1	odpor
<g/>
,	,	kIx,	,
srdeční	srdeční	k2eAgInSc1d1	srdeční
oddíl	oddíl	k1gInSc1	oddíl
před	před	k7c7	před
chlopní	chlopeň	k1gFnSc7	chlopeň
je	být	k5eAaImIp3nS	být
přetěžován	přetěžován	k2eAgMnSc1d1	přetěžován
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
musí	muset	k5eAaImIp3nP	muset
vyvinout	vyvinout	k5eAaPmF	vyvinout
vyšší	vysoký	k2eAgInSc4d2	vyšší
tlak	tlak	k1gInSc4	tlak
k	k	k7c3	k
překonání	překonání	k1gNnSc3	překonání
překážky	překážka	k1gFnSc2	překážka
<g/>
.	.	kIx.	.
</s>
<s>
Dochází	docházet	k5eAaImIp3nS	docházet
tak	tak	k9	tak
ke	k	k7c3	k
zbytnění	zbytnění	k1gNnSc3	zbytnění
svaloviny	svalovina	k1gFnSc2	svalovina
(	(	kIx(	(
<g/>
hypertrofii	hypertrofie	k1gFnSc4	hypertrofie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
i	i	k9	i
rozšíření	rozšíření	k1gNnSc4	rozšíření
celého	celý	k2eAgInSc2d1	celý
srdečního	srdeční	k2eAgInSc2d1	srdeční
oddílu	oddíl	k1gInSc2	oddíl
(	(	kIx(	(
<g/>
dilataci	dilatace	k1gFnSc4	dilatace
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
nedostatečnost	nedostatečnost	k1gFnSc1	nedostatečnost
-	-	kIx~	-
regurgitační	regurgitační	k2eAgFnPc4d1	regurgitační
vady	vada	k1gFnPc4	vada
-	-	kIx~	-
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
poruchy	porucha	k1gFnSc2	porucha
cípů	cíp	k1gInPc2	cíp
(	(	kIx(	(
<g/>
jejich	jejich	k3xOp3gNnSc2	jejich
zjizvení	zjizvení	k1gNnSc2	zjizvení
či	či	k8xC	či
proděravění	proděravění	k1gNnSc2	proděravění
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
či	či	k8xC	či
struktur	struktura	k1gFnPc2	struktura
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
fixují	fixovat	k5eAaImIp3nP	fixovat
jejich	jejich	k3xOp3gFnSc4	jejich
polohu	poloha	k1gFnSc4	poloha
(	(	kIx(	(
<g/>
papilární	papilární	k2eAgInPc1d1	papilární
svaly	sval	k1gInPc1	sval
či	či	k8xC	či
jejich	jejich	k3xOp3gFnPc1	jejich
šlašinky	šlašinka	k1gFnPc1	šlašinka
<g/>
)	)	kIx)	)
či	či	k8xC	či
při	při	k7c6	při
nadměrném	nadměrný	k2eAgNnSc6d1	nadměrné
rozšíření	rozšíření	k1gNnSc6	rozšíření
prstence	prstenec	k1gInSc2	prstenec
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgNnSc6	jenž
jsou	být	k5eAaImIp3nP	být
cípy	cíp	k1gInPc1	cíp
chlopní	chlopeň	k1gFnPc2	chlopeň
upevněny	upevněn	k2eAgFnPc1d1	upevněna
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
narušena	narušit	k5eAaPmNgFnS	narušit
těsnící	těsnící	k2eAgFnSc1d1	těsnící
funkce	funkce	k1gFnSc1	funkce
chlopní	chlopeň	k1gFnPc2	chlopeň
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
za	za	k7c2	za
normálních	normální	k2eAgFnPc2d1	normální
okolností	okolnost	k1gFnPc2	okolnost
fungují	fungovat	k5eAaImIp3nP	fungovat
jako	jako	k9	jako
jednocestný	jednocestný	k2eAgInSc4d1	jednocestný
ventil	ventil	k1gInSc4	ventil
<g/>
.	.	kIx.	.
</s>
<s>
Krev	krev	k1gFnSc1	krev
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
netěsnící	těsnící	k2eNgFnSc7d1	netěsnící
chlopní	chlopeň	k1gFnSc7	chlopeň
vrací	vracet	k5eAaImIp3nS	vracet
do	do	k7c2	do
předcházejícího	předcházející	k2eAgInSc2d1	předcházející
oddílu	oddíl	k1gInSc2	oddíl
a	a	k8xC	a
jistá	jistý	k2eAgFnSc1d1	jistá
část	část	k1gFnSc1	část
vypuzované	vypuzovaný	k2eAgFnSc2d1	vypuzovaný
krve	krev	k1gFnSc2	krev
jakoby	jakoby	k8xS	jakoby
proudila	proudit	k5eAaImAgFnS	proudit
neustále	neustále	k6eAd1	neustále
"	"	kIx"	"
<g/>
tam	tam	k6eAd1	tam
a	a	k8xC	a
zpět	zpět	k6eAd1	zpět
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Srdeční	srdeční	k2eAgInPc1d1	srdeční
oddíly	oddíl	k1gInPc1	oddíl
jsou	být	k5eAaImIp3nP	být
tak	tak	k6eAd1	tak
přetěžovány	přetěžován	k2eAgMnPc4d1	přetěžován
nadměrným	nadměrný	k2eAgInSc7d1	nadměrný
objemem	objem	k1gInSc7	objem
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
rozšiřování	rozšiřování	k1gNnSc3	rozšiřování
i	i	k8xC	i
celkovému	celkový	k2eAgNnSc3d1	celkové
zbytnění	zbytnění	k1gNnSc3	zbytnění
svaloviny	svalovina	k1gFnSc2	svalovina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
častá	častý	k2eAgFnSc1d1	častá
je	být	k5eAaImIp3nS	být
kombinace	kombinace	k1gFnSc1	kombinace
obou	dva	k4xCgFnPc2	dva
vad	vada	k1gFnPc2	vada
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
chlopniPodle	chlopniPodle	k6eAd1	chlopniPodle
typu	typ	k1gInSc3	typ
vady	vada	k1gFnSc2	vada
a	a	k8xC	a
postižené	postižený	k2eAgFnSc2d1	postižená
chlopně	chlopeň	k1gFnSc2	chlopeň
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
rozeznávají	rozeznávat	k5eAaImIp3nP	rozeznávat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
na	na	k7c6	na
aortální	aortální	k2eAgFnSc6d1	aortální
chlopni	chlopeň	k1gFnSc6	chlopeň
<g/>
:	:	kIx,	:
aortální	aortální	k2eAgFnSc1d1	aortální
stenóza	stenóza	k1gFnSc1	stenóza
a	a	k8xC	a
aortální	aortální	k2eAgFnSc1d1	aortální
regurgitace	regurgitace	k1gFnSc1	regurgitace
(	(	kIx(	(
<g/>
aortální	aortální	k2eAgFnSc1d1	aortální
insuficience	insuficience	k1gFnSc1	insuficience
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
na	na	k7c6	na
mitrální	mitrální	k2eAgFnSc6d1	mitrální
chlopni	chlopeň	k1gFnSc6	chlopeň
<g/>
:	:	kIx,	:
mitrální	mitrální	k2eAgFnSc1d1	mitrální
stenóza	stenóza	k1gFnSc1	stenóza
a	a	k8xC	a
mitrální	mitrální	k2eAgFnSc1d1	mitrální
regurgitace	regurgitace	k1gFnSc1	regurgitace
(	(	kIx(	(
<g/>
mitrální	mitrální	k2eAgFnSc1d1	mitrální
insuficience	insuficience	k1gFnSc1	insuficience
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
případem	případ	k1gInSc7	případ
vady	vada	k1gFnSc2	vada
na	na	k7c6	na
mitrální	mitrální	k2eAgFnSc6d1	mitrální
chlopni	chlopeň	k1gFnSc6	chlopeň
je	být	k5eAaImIp3nS	být
prolaps	prolaps	k1gInSc4	prolaps
mitrální	mitrální	k2eAgFnSc2d1	mitrální
chlopně	chlopeň	k1gFnSc2	chlopeň
</s>
</p>
<p>
<s>
na	na	k7c6	na
trikuspidální	trikuspidální	k2eAgFnSc6d1	trikuspidální
chlopni	chlopeň	k1gFnSc6	chlopeň
<g/>
:	:	kIx,	:
trikuspidální	trikuspidální	k2eAgFnSc1d1	trikuspidální
stenóza	stenóza	k1gFnSc1	stenóza
a	a	k8xC	a
trikuspidální	trikuspidální	k2eAgFnSc1d1	trikuspidální
regurgitace	regurgitace	k1gFnSc1	regurgitace
(	(	kIx(	(
<g/>
trikuspidální	trikuspidální	k2eAgFnSc1d1	trikuspidální
insuficience	insuficience	k1gFnSc1	insuficience
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
na	na	k7c6	na
chlopni	chlopeň	k1gFnSc6	chlopeň
plícnice	plícnice	k1gFnSc2	plícnice
(	(	kIx(	(
<g/>
pulmonální	pulmonální	k2eAgFnSc6d1	pulmonální
chlopni	chlopeň	k1gFnSc6	chlopeň
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
pulmonální	pulmonální	k2eAgFnSc1d1	pulmonální
stenóza	stenóza	k1gFnSc1	stenóza
a	a	k8xC	a
pulmonální	pulmonální	k2eAgFnSc1d1	pulmonální
regurgitace	regurgitace	k1gFnSc1	regurgitace
(	(	kIx(	(
<g/>
pulmonální	pulmonální	k2eAgFnSc1d1	pulmonální
insuficience	insuficience	k1gFnSc1	insuficience
<g/>
)	)	kIx)	)
<g/>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
kombinují	kombinovat	k5eAaImIp3nP	kombinovat
vady	vada	k1gFnPc1	vada
na	na	k7c6	na
více	hodně	k6eAd2	hodně
chlopních	chlopeň	k1gFnPc6	chlopeň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Klinické	klinický	k2eAgInPc1d1	klinický
projevy	projev	k1gInPc1	projev
==	==	k?	==
</s>
</p>
<p>
<s>
Lehčí	lehký	k2eAgFnPc1d2	lehčí
chlopenní	chlopenní	k2eAgFnPc1d1	chlopenní
vady	vada	k1gFnPc1	vada
se	se	k3xPyFc4	se
neprojevují	projevovat	k5eNaImIp3nP	projevovat
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
potíže	potíž	k1gFnPc1	potíž
jsou	být	k5eAaImIp3nP	být
již	již	k6eAd1	již
projevem	projev	k1gInSc7	projev
většinou	většinou	k6eAd1	většinou
pokročilejší	pokročilý	k2eAgFnPc4d2	pokročilejší
vady	vada	k1gFnPc4	vada
<g/>
.	.	kIx.	.
</s>
<s>
Vada	vada	k1gFnSc1	vada
může	moct	k5eAaImIp3nS	moct
zůstat	zůstat	k5eAaPmF	zůstat
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
skryta	skrýt	k5eAaPmNgFnS	skrýt
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zjištěna	zjistit	k5eAaPmNgFnS	zjistit
náhodně	náhodně	k6eAd1	náhodně
během	během	k7c2	během
lékařského	lékařský	k2eAgNnSc2d1	lékařské
vyšetření	vyšetření	k1gNnSc2	vyšetření
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
vzniku	vznik	k1gInSc2	vznik
srdečního	srdeční	k2eAgInSc2d1	srdeční
šelestu	šelest	k1gInSc2	šelest
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
chlopenních	chlopenní	k2eAgFnPc2d1	chlopenní
vad	vada	k1gFnPc2	vada
má	mít	k5eAaImIp3nS	mít
tendenci	tendence	k1gFnSc4	tendence
se	se	k3xPyFc4	se
během	během	k7c2	během
života	život	k1gInSc2	život
postupně	postupně	k6eAd1	postupně
zhoršovat	zhoršovat	k5eAaImF	zhoršovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Základními	základní	k2eAgInPc7d1	základní
projevy	projev	k1gInPc7	projev
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
příznaky	příznak	k1gInPc1	příznak
srdečního	srdeční	k2eAgNnSc2d1	srdeční
selhání	selhání	k1gNnSc2	selhání
<g/>
:	:	kIx,	:
dušnost	dušnost	k1gFnSc1	dušnost
nejprve	nejprve	k6eAd1	nejprve
při	při	k7c6	při
námaze	námaha	k1gFnSc6	námaha
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
i	i	k9	i
v	v	k7c6	v
klidu	klid	k1gInSc6	klid
<g/>
,	,	kIx,	,
otoky	otok	k1gInPc4	otok
dolních	dolní	k2eAgFnPc2d1	dolní
končetin	končetina	k1gFnPc2	končetina
<g/>
,	,	kIx,	,
nadměrná	nadměrný	k2eAgFnSc1d1	nadměrná
únava	únava	k1gFnSc1	únava
<g/>
,	,	kIx,	,
nevýkonnost	nevýkonnost	k1gFnSc1	nevýkonnost
<g/>
,	,	kIx,	,
závratě	závrať	k1gFnPc1	závrať
až	až	k8xS	až
krátkodobé	krátkodobý	k2eAgInPc1d1	krátkodobý
poruchy	poruch	k1gInPc1	poruch
vědomí	vědomí	k1gNnSc2	vědomí
(	(	kIx(	(
<g/>
synkopy	synkopa	k1gFnSc2	synkopa
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
příznaky	příznak	k1gInPc1	příznak
z	z	k7c2	z
poruch	porucha	k1gFnPc2	porucha
srdečního	srdeční	k2eAgInSc2d1	srdeční
rytmu	rytmus	k1gInSc2	rytmus
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
často	často	k6eAd1	často
doprovází	doprovázet	k5eAaImIp3nP	doprovázet
chlopenní	chlopenní	k2eAgFnPc4d1	chlopenní
vady	vada	k1gFnPc4	vada
<g/>
:	:	kIx,	:
bušení	bušení	k1gNnSc4	bušení
srdce	srdce	k1gNnSc2	srdce
<g/>
,	,	kIx,	,
krátkodobé	krátkodobý	k2eAgFnSc2d1	krátkodobá
poruchy	porucha	k1gFnSc2	porucha
vědomí	vědomí	k1gNnSc1	vědomí
<g/>
,	,	kIx,	,
stavy	stav	k1gInPc1	stav
slabostí	slabost	k1gFnPc2	slabost
či	či	k8xC	či
závratí	závrať	k1gFnPc2	závrať
<g/>
,	,	kIx,	,
náhlé	náhlý	k2eAgNnSc4d1	náhlé
úmrtí	úmrtí	k1gNnSc4	úmrtí
</s>
</p>
<p>
<s>
bolesti	bolest	k1gFnPc1	bolest
na	na	k7c6	na
hrudi	hruď	k1gFnSc6	hruď
</s>
</p>
<p>
<s>
projevy	projev	k1gInPc1	projev
embolizace	embolizace	k1gFnSc2	embolizace
<g/>
:	:	kIx,	:
v	v	k7c6	v
rozšířených	rozšířený	k2eAgInPc6d1	rozšířený
srdečních	srdeční	k2eAgInPc6d1	srdeční
oddílech	oddíl	k1gInPc6	oddíl
mohou	moct	k5eAaImIp3nP	moct
vznikat	vznikat	k5eAaImF	vznikat
krevní	krevní	k2eAgFnPc1d1	krevní
sraženiny	sraženina	k1gFnPc1	sraženina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	s	k7c7	s
krevním	krevní	k2eAgInSc7d1	krevní
tokem	tok	k1gInSc7	tok
můžou	můžou	k?	můžou
zanést	zanést	k5eAaPmF	zanést
do	do	k7c2	do
jiných	jiný	k2eAgFnPc2d1	jiná
částí	část	k1gFnPc2	část
krevního	krevní	k2eAgInSc2d1	krevní
oběhu	oběh	k1gInSc2	oběh
<g/>
,	,	kIx,	,
tam	tam	k6eAd1	tam
ucpou	ucpat	k5eAaPmIp3nP	ucpat
větší	veliký	k2eAgFnSc4d2	veliký
či	či	k8xC	či
menší	malý	k2eAgFnSc4d2	menší
tepnu	tepna	k1gFnSc4	tepna
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vede	vést	k5eAaImIp3nS	vést
poruše	porucha	k1gFnSc3	porucha
prokrvení	prokrvení	k1gNnSc2	prokrvení
tkání	tkáň	k1gFnPc2	tkáň
zásobovaných	zásobovaný	k2eAgFnPc2d1	zásobovaná
touto	tento	k3xDgFnSc7	tento
tepnou	tepna	k1gFnSc7	tepna
až	až	k6eAd1	až
vzniku	vznik	k1gInSc2	vznik
infarktu	infarkt	k1gInSc2	infarkt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vyšetření	vyšetření	k1gNnSc2	vyšetření
==	==	k?	==
</s>
</p>
<p>
<s>
Základem	základ	k1gInSc7	základ
diagnostiky	diagnostika	k1gFnSc2	diagnostika
je	být	k5eAaImIp3nS	být
ultrazvukové	ultrazvukový	k2eAgNnSc1d1	ultrazvukové
vyšetření	vyšetření	k1gNnSc1	vyšetření
(	(	kIx(	(
<g/>
echokardiografie	echokardiografie	k1gFnSc1	echokardiografie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
prováděná	prováděný	k2eAgFnSc1d1	prováděná
z	z	k7c2	z
povrchu	povrch	k1gInSc2	povrch
těla	tělo	k1gNnSc2	tělo
nebo	nebo	k8xC	nebo
přesněji	přesně	k6eAd2	přesně
k	k	k7c3	k
vyhodnocení	vyhodnocení	k1gNnSc3	vyhodnocení
chlopní	chlopeň	k1gFnPc2	chlopeň
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
použita	použít	k5eAaPmNgFnS	použít
sonda	sonda	k1gFnSc1	sonda
zavedená	zavedený	k2eAgFnSc1d1	zavedená
do	do	k7c2	do
jícnu	jícen	k1gInSc2	jícen
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
těsné	těsný	k2eAgFnSc6d1	těsná
blízkosti	blízkost	k1gFnSc6	blízkost
srdce	srdce	k1gNnSc1	srdce
a	a	k8xC	a
vyšetření	vyšetření	k1gNnSc1	vyšetření
tak	tak	k6eAd1	tak
není	být	k5eNaImIp3nS	být
rušeno	rušen	k2eAgNnSc1d1	rušeno
z	z	k7c2	z
tkání	tkáň	k1gFnPc2	tkáň
hrudní	hrudní	k2eAgFnSc2d1	hrudní
stěny	stěna	k1gFnSc2	stěna
či	či	k8xC	či
plic	plíce	k1gFnPc2	plíce
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
vyšetření	vyšetření	k1gNnSc1	vyšetření
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
jícnová	jícnový	k2eAgFnSc1d1	jícnová
(	(	kIx(	(
<g/>
transesofageální	transesofageální	k2eAgFnSc1d1	transesofageální
<g/>
)	)	kIx)	)
echokardiografie	echokardiografie	k1gFnSc1	echokardiografie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ultrazvukové	ultrazvukový	k2eAgNnSc1d1	ultrazvukové
vyšetření	vyšetření	k1gNnSc1	vyšetření
je	být	k5eAaImIp3nS	být
dle	dle	k7c2	dle
potřeby	potřeba	k1gFnSc2	potřeba
možno	možno	k6eAd1	možno
doplnit	doplnit	k5eAaPmF	doplnit
invazivnějším	invazivný	k2eAgNnSc7d2	invazivnější
vyšetřením	vyšetření	k1gNnSc7	vyšetření
-	-	kIx~	-
srdeční	srdeční	k2eAgFnSc7d1	srdeční
katetrizací	katetrizace	k1gFnSc7	katetrizace
s	s	k7c7	s
měřením	měření	k1gNnSc7	měření
tlaku	tlak	k1gInSc2	tlak
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
srdečních	srdeční	k2eAgInPc6d1	srdeční
oddílech	oddíl	k1gInPc6	oddíl
<g/>
.	.	kIx.	.
</s>
<s>
Standardem	standard	k1gInSc7	standard
před	před	k7c7	před
operací	operace	k1gFnSc7	operace
postižené	postižený	k2eAgFnSc2d1	postižená
chlopně	chlopeň	k1gFnSc2	chlopeň
je	být	k5eAaImIp3nS	být
provedení	provedení	k1gNnSc1	provedení
koronarografie	koronarografie	k1gFnSc2	koronarografie
-	-	kIx~	-
tedy	tedy	k9	tedy
zobrazení	zobrazení	k1gNnSc1	zobrazení
věnčitých	věnčitý	k2eAgFnPc2d1	věnčitá
tepen	tepna	k1gFnPc2	tepna
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
zásobují	zásobovat	k5eAaImIp3nP	zásobovat
srdeční	srdeční	k2eAgInSc4d1	srdeční
sval	sval	k1gInSc4	sval
krví	krev	k1gFnPc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
zjistí	zjistit	k5eAaPmIp3nS	zjistit
významné	významný	k2eAgNnSc1d1	významné
postižení	postižení	k1gNnSc1	postižení
těchto	tento	k3xDgFnPc2	tento
tepen	tepna	k1gFnPc2	tepna
aterosklerózou	ateroskleróza	k1gFnSc7	ateroskleróza
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
u	u	k7c2	u
části	část	k1gFnSc2	část
nemocných	nemocný	k1gMnPc2	nemocný
současně	současně	k6eAd1	současně
při	při	k7c6	při
operaci	operace	k1gFnSc6	operace
našít	našít	k5eAaBmF	našít
srdeční	srdeční	k2eAgInSc4d1	srdeční
by-pass	byass	k1gInSc4	by-pass
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zlepší	zlepšit	k5eAaPmIp3nS	zlepšit
prokrvení	prokrvení	k1gNnSc4	prokrvení
srdečního	srdeční	k2eAgInSc2d1	srdeční
svalu	sval	k1gInSc2	sval
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
sledování	sledování	k1gNnSc3	sledování
funkce	funkce	k1gFnSc2	funkce
srdce	srdce	k1gNnSc2	srdce
se	se	k3xPyFc4	se
uplatňují	uplatňovat	k5eAaImIp3nP	uplatňovat
i	i	k9	i
zátěžová	zátěžový	k2eAgNnPc4d1	zátěžové
vyšetření	vyšetření	k1gNnPc4	vyšetření
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Léčba	léčba	k1gFnSc1	léčba
==	==	k?	==
</s>
</p>
<p>
<s>
Podávání	podávání	k1gNnSc1	podávání
léků	lék	k1gInPc2	lék
samotné	samotný	k2eAgNnSc1d1	samotné
postižení	postižení	k1gNnSc1	postižení
chlopní	chlopeň	k1gFnPc2	chlopeň
nevyléčí	vyléčit	k5eNaPmIp3nS	vyléčit
<g/>
.	.	kIx.	.
</s>
<s>
Podávají	podávat	k5eAaImIp3nP	podávat
se	se	k3xPyFc4	se
ke	k	k7c3	k
zmírnění	zmírnění	k1gNnSc3	zmírnění
potíží	potíž	k1gFnPc2	potíž
a	a	k8xC	a
uplatňují	uplatňovat	k5eAaImIp3nP	uplatňovat
u	u	k7c2	u
pacientů	pacient	k1gMnPc2	pacient
s	s	k7c7	s
relativně	relativně	k6eAd1	relativně
lehčím	lehký	k2eAgNnSc7d2	lehčí
postižením	postižení	k1gNnSc7	postižení
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
ještě	ještě	k6eAd1	ještě
nejsou	být	k5eNaImIp3nP	být
k	k	k7c3	k
operaci	operace	k1gFnSc3	operace
vhodni	vhodnout	k5eAaPmRp2nS	vhodnout
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
u	u	k7c2	u
nemocných	nemocný	k1gMnPc2	nemocný
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
něž	jenž	k3xRgNnSc4	jenž
by	by	kYmCp3nS	by
operace	operace	k1gFnSc1	operace
představovala	představovat	k5eAaImAgFnS	představovat
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
vysokého	vysoký	k2eAgInSc2d1	vysoký
věku	věk	k1gInSc2	věk
nebo	nebo	k8xC	nebo
dalších	další	k2eAgFnPc2d1	další
onemocnění	onemocnění	k1gNnSc4	onemocnění
příliš	příliš	k6eAd1	příliš
velkou	velký	k2eAgFnSc4d1	velká
zátěž	zátěž	k1gFnSc4	zátěž
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgFnPc2	některý
vad	vada	k1gFnPc2	vada
je	být	k5eAaImIp3nS	být
také	také	k9	také
možno	možno	k6eAd1	možno
léky	lék	k1gInPc7	lék
a	a	k8xC	a
změnou	změna	k1gFnSc7	změna
návyků	návyk	k1gInPc2	návyk
zpomalit	zpomalit	k5eAaPmF	zpomalit
zhoršování	zhoršování	k1gNnSc4	zhoršování
vady	vada	k1gFnSc2	vada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pokročilejší	pokročilý	k2eAgFnPc4d2	pokročilejší
chlopenní	chlopenní	k2eAgFnPc4d1	chlopenní
vady	vada	k1gFnPc4	vada
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
řešit	řešit	k5eAaImF	řešit
operačně	operačně	k6eAd1	operačně
-	-	kIx~	-
dle	dle	k7c2	dle
typu	typ	k1gInSc2	typ
postižení	postižení	k1gNnSc2	postižení
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
buď	buď	k8xC	buď
chirurgická	chirurgický	k2eAgFnSc1d1	chirurgická
úprava	úprava	k1gFnSc1	úprava
chlopně	chlopeň	k1gFnSc2	chlopeň
(	(	kIx(	(
<g/>
plastika	plastika	k1gFnSc1	plastika
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
její	její	k3xOp3gFnSc1	její
náhrada	náhrada	k1gFnSc1	náhrada
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
postižení	postižení	k1gNnSc2	postižení
chlopně	chlopeň	k1gFnSc2	chlopeň
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
chlopeň	chlopeň	k1gFnSc1	chlopeň
umělá	umělý	k2eAgFnSc1d1	umělá
nebo	nebo	k8xC	nebo
bioprotéza	bioprotéza	k1gFnSc1	bioprotéza
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgFnPc2	některý
stenotických	stenotický	k2eAgFnPc2d1	stenotický
vad	vada	k1gFnPc2	vada
je	být	k5eAaImIp3nS	být
také	také	k9	také
možno	možno	k6eAd1	možno
použít	použít	k5eAaPmF	použít
katetrizační	katetrizační	k2eAgFnSc4d1	katetrizační
balónkovou	balónkový	k2eAgFnSc4d1	balónková
valvuloplastiku	valvuloplastika	k1gFnSc4	valvuloplastika
-	-	kIx~	-
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
zúžené	zúžený	k2eAgFnSc2d1	zúžená
chlopně	chlopeň	k1gFnSc2	chlopeň
se	se	k3xPyFc4	se
nafoukne	nafouknout	k5eAaPmIp3nS	nafouknout
balónek	balónek	k1gInSc1	balónek
(	(	kIx(	(
<g/>
zavedený	zavedený	k2eAgInSc1d1	zavedený
přes	přes	k7c4	přes
některou	některý	k3yIgFnSc4	některý
velkou	velký	k2eAgFnSc4d1	velká
cévu	céva	k1gFnSc4	céva
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
rozruší	rozrušit	k5eAaPmIp3nS	rozrušit
spoje	spoj	k1gInPc4	spoj
mezi	mezi	k7c7	mezi
cípy	cíp	k1gInPc7	cíp
a	a	k8xC	a
zlepší	zlepšit	k5eAaPmIp3nS	zlepšit
funkci	funkce	k1gFnSc4	funkce
chlopně	chlopeň	k1gFnSc2	chlopeň
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
efekt	efekt	k1gInSc1	efekt
však	však	k9	však
není	být	k5eNaImIp3nS	být
tak	tak	k6eAd1	tak
velký	velký	k2eAgInSc1d1	velký
jako	jako	k8xS	jako
u	u	k7c2	u
náhrady	náhrada	k1gFnSc2	náhrada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
