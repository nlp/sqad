<s>
Emil	Emil	k1gMnSc1
Kopřiva	Kopřiva	k1gMnSc1
</s>
<s>
Emil	Emil	k1gMnSc1
Kopřiva	Kopřiva	k1gMnSc1
Emil	Emil	k1gMnSc1
Kopřiva	Kopřiva	k1gMnSc1
v	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
<g/>
Základní	základní	k2eAgFnSc1d1
informace	informace	k1gFnSc1
Narození	narození	k1gNnSc2
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1956	#num#	k4
(	(	kIx(
<g/>
64	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Žánry	žánr	k1gInPc7
</s>
<s>
progresivní	progresivní	k2eAgInSc1d1
rock	rock	k1gInSc1
<g/>
,	,	kIx,
art	art	k?
rock	rock	k1gInSc1
<g/>
,	,	kIx,
hard	hard	k6eAd1
rock	rock	k1gInSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
skladatel	skladatel	k1gMnSc1
<g/>
,	,	kIx,
hudebník	hudebník	k1gMnSc1
<g/>
,	,	kIx,
podnikatel	podnikatel	k1gMnSc1
Nástroje	nástroj	k1gInSc2
</s>
<s>
kytara	kytara	k1gFnSc1
Vydavatelé	vydavatel	k1gMnPc1
</s>
<s>
Panton	Panton	k1gInSc1
<g/>
,	,	kIx,
Supraphon	supraphon	k1gInSc1
Příbuzná	příbuzná	k1gFnSc1
témata	téma	k1gNnPc4
</s>
<s>
Synkopy	synkopa	k1gFnPc1
<g/>
,	,	kIx,
Futurum	futurum	k1gNnSc1
<g/>
,	,	kIx,
Špilberk	Špilberk	k1gInSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Emil	Emil	k1gMnSc1
Kopřiva	Kopřiva	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
9	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1956	#num#	k4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
český	český	k2eAgMnSc1d1
hudebník	hudebník	k1gMnSc1
<g/>
,	,	kIx,
kytarista	kytarista	k1gMnSc1
a	a	k8xC
skladatel	skladatel	k1gMnSc1
<g/>
,	,	kIx,
známý	známý	k1gMnSc1
jako	jako	k8xS,k8xC
člen	člen	k1gMnSc1
brněnských	brněnský	k2eAgFnPc2d1
rockových	rockový	k2eAgFnPc2d1
skupin	skupina	k1gFnPc2
Synkopy	synkopa	k1gFnSc2
a	a	k8xC
Futurum	futurum	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Biografie	biografie	k1gFnSc1
</s>
<s>
Kopřiva	Kopřiva	k1gMnSc1
přišel	přijít	k5eAaPmAgMnS
do	do	k7c2
Synkop	synkopa	k1gFnPc2
na	na	k7c6
začátku	začátek	k1gInSc6
roku	rok	k1gInSc2
1981	#num#	k4
a	a	k8xC
působil	působit	k5eAaImAgMnS
zde	zde	k6eAd1
jako	jako	k8xC,k8xS
jeden	jeden	k4xCgInSc1
ze	z	k7c2
dvou	dva	k4xCgMnPc2
kytaristů	kytarista	k1gMnPc2
společně	společně	k6eAd1
s	s	k7c7
jedním	jeden	k4xCgMnSc7
ze	z	k7c2
zakladatelů	zakladatel	k1gMnPc2
skupiny	skupina	k1gFnSc2
Petrem	Petr	k1gMnSc7
Smějou	smát	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInSc1
koncert	koncert	k1gInSc1
s	s	k7c7
Emilem	Emil	k1gMnSc7
Kopřivou	Kopřiva	k1gMnSc7
Synkopy	synkopa	k1gFnSc2
odehrály	odehrát	k5eAaPmAgFnP
20	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
toho	ten	k3xDgInSc2
roku	rok	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Kapela	kapela	k1gFnSc1
tehdy	tehdy	k6eAd1
hrála	hrát	k5eAaImAgFnS
art	art	k?
rockový	rockový	k2eAgInSc4d1
koncertní	koncertní	k2eAgInSc4d1
audiovizuální	audiovizuální	k2eAgInSc4d1
program	program	k1gInSc4
Sluneční	sluneční	k2eAgFnSc2d1
hodiny	hodina	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
v	v	k7c6
létě	léto	k1gNnSc6
1981	#num#	k4
nahrála	nahrát	k5eAaBmAgFnS,k5eAaPmAgFnS
a	a	k8xC
Panton	Panton	k1gInSc1
jej	on	k3xPp3gMnSc4
vydal	vydat	k5eAaPmAgInS
jako	jako	k8xS,k8xC
LP	LP	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
ale	ale	k8xC
odešel	odejít	k5eAaPmAgMnS
Petr	Petr	k1gMnSc1
Směja	Směja	k1gMnSc1
a	a	k8xC
Emil	Emil	k1gMnSc1
Kopřiva	Kopřiva	k1gMnSc1
tak	tak	k6eAd1
zůstal	zůstat	k5eAaPmAgMnS
ve	v	k7c6
skupině	skupina	k1gFnSc6
jediným	jediný	k2eAgMnSc7d1
kytaristou	kytarista	k1gMnSc7
<g/>
,	,	kIx,
takže	takže	k8xS
se	se	k3xPyFc4
zřejmě	zřejmě	k6eAd1
podílel	podílet	k5eAaImAgInS
i	i	k9
na	na	k7c6
singlu	singl	k1gInSc6
„	„	k?
<g/>
Mladší	mladý	k2eAgFnPc1d2
sestry	sestra	k1gFnPc1
vzpomínek	vzpomínka	k1gFnPc2
<g/>
“	“	k?
z	z	k7c2
roku	rok	k1gInSc2
1982	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Kopřiva	Kopřiva	k1gMnSc1
v	v	k7c6
roce	rok	k1gInSc6
1983	#num#	k4
odešel	odejít	k5eAaPmAgMnS
(	(	kIx(
<g/>
v	v	k7c6
Synkopách	synkopa	k1gFnPc6
jej	on	k3xPp3gInSc4
nahradil	nahradit	k5eAaPmAgMnS
Miloš	Miloš	k1gMnSc1
Makovský	Makovský	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
stal	stát	k5eAaPmAgMnS
se	s	k7c7
členem	člen	k1gMnSc7
nově	nově	k6eAd1
vzniklé	vzniklý	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
Futurum	futurum	k1gNnSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
byl	být	k5eAaImAgInS
opět	opět	k6eAd1
jedním	jeden	k4xCgMnSc7
ze	z	k7c2
dvou	dva	k4xCgMnPc2
kytaristů	kytarista	k1gMnPc2
(	(	kIx(
<g/>
tím	ten	k3xDgNnSc7
druhým	druhý	k4xOgNnSc7
byl	být	k5eAaImAgInS
Miloš	Miloš	k1gMnSc1
Morávek	Morávek	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kromě	kromě	k7c2
toho	ten	k3xDgNnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
také	také	k6eAd1
hrál	hrát	k5eAaImAgInS
částečně	částečně	k6eAd1
na	na	k7c4
baskytaru	baskytara	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Futurum	futurum	k1gNnSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
byl	být	k5eAaImAgMnS
vůdčí	vůdčí	k2eAgFnSc7d1
osobností	osobnost	k1gFnSc7
klávesista	klávesista	k1gMnSc1
a	a	k8xC
zpěvák	zpěvák	k1gMnSc1
Roman	Roman	k1gMnSc1
Dragoun	dragoun	k1gMnSc1
<g/>
,	,	kIx,
začalo	začít	k5eAaPmAgNnS
hrát	hrát	k5eAaImF
progresivní	progresivní	k2eAgInSc4d1
rock	rock	k1gInSc4
ovlivněný	ovlivněný	k2eAgMnSc1d1
King	King	k1gMnSc1
Crimson	Crimson	k1gMnSc1
<g/>
,	,	kIx,
UK	UK	kA
či	či	k8xC
Talking	Talking	k1gInSc1
Heads	Headsa	k1gFnPc2
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
roku	rok	k1gInSc2
1984	#num#	k4
vydalo	vydat	k5eAaPmAgNnS
desku	deska	k1gFnSc4
Ostrov	ostrov	k1gInSc4
Země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1985	#num#	k4
odešel	odejít	k5eAaPmAgMnS
Morávek	Morávek	k1gMnSc1
<g/>
,	,	kIx,
naopak	naopak	k6eAd1
novým	nový	k2eAgMnSc7d1
hudebníkem	hudebník	k1gMnSc7
v	v	k7c6
sestavě	sestava	k1gFnSc6
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
baskytarista	baskytarista	k1gMnSc1
Lubomír	Lubomír	k1gMnSc1
Eremiáš	Eremiáš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
později	pozdě	k6eAd2
vyšlo	vyjít	k5eAaPmAgNnS
druhé	druhý	k4xOgNnSc1
album	album	k1gNnSc4
Jedinečná	jedinečný	k2eAgFnSc1d1
šance	šance	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1988	#num#	k4
z	z	k7c2
kapely	kapela	k1gFnSc2
odešli	odejít	k5eAaPmAgMnP
Dragoun	dragoun	k1gMnSc1
a	a	k8xC
bubeník	bubeník	k1gMnSc1
Jan	Jan	k1gMnSc1
Seidl	Seidl	k1gMnSc1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
začali	začít	k5eAaPmAgMnP
hrát	hrát	k5eAaImF
se	s	k7c7
skupinou	skupina	k1gFnSc7
Stromboli	Strombole	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Futurum	futurum	k1gNnSc1
s	s	k7c7
Emilem	Emil	k1gMnSc7
Kopřivou	Kopřiva	k1gMnSc7
v	v	k7c6
čele	čelo	k1gNnSc6
<g/>
,	,	kIx,
jakožto	jakožto	k8xS
jediným	jediný	k2eAgMnSc7d1
původním	původní	k2eAgMnSc7d1
členem	člen	k1gMnSc7
<g/>
,	,	kIx,
ještě	ještě	k9
nějaký	nějaký	k3yIgInSc4
čas	čas	k1gInSc4
fungovalo	fungovat	k5eAaImAgNnS
jako	jako	k9
předkapela	předkapela	k1gFnSc1
právě	právě	k9
Stromboli	Strombole	k1gFnSc4
<g/>
,	,	kIx,
během	během	k7c2
roku	rok	k1gInSc2
1989	#num#	k4
ale	ale	k8xC
nakonec	nakonec	k6eAd1
zaniklo	zaniknout	k5eAaPmAgNnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Emil	Emil	k1gMnSc1
Kopřiva	Kopřiva	k1gMnSc1
následně	následně	k6eAd1
založil	založit	k5eAaPmAgMnS
vlastní	vlastní	k2eAgFnSc4d1
skupinu	skupina	k1gFnSc4
Špilberk	Špilberk	k1gInSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
navázala	navázat	k5eAaPmAgFnS
na	na	k7c4
poslední	poslední	k2eAgNnSc4d1
období	období	k1gNnSc4
Futura	futurum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Špilberk	Špilberk	k1gInSc4
ale	ale	k8xC
existoval	existovat	k5eAaImAgInS
pouze	pouze	k6eAd1
krátkou	krátký	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
<g/>
,	,	kIx,
roku	rok	k1gInSc2
1990	#num#	k4
vydala	vydat	k5eAaPmAgFnS
kapela	kapela	k1gFnSc1
jediný	jediný	k2eAgInSc4d1
singl	singl	k1gInSc4
a	a	k8xC
později	pozdě	k6eAd2
ukončila	ukončit	k5eAaPmAgFnS
činnost	činnost	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Kopřiva	Kopřiva	k1gMnSc1
se	se	k3xPyFc4
poté	poté	k6eAd1
začal	začít	k5eAaPmAgInS
věnovat	věnovat	k5eAaImF,k5eAaPmF
podnikání	podnikání	k1gNnSc4
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1994	#num#	k4
vznikla	vzniknout	k5eAaPmAgFnS
společnost	společnost	k1gFnSc1
Studio	studio	k1gNnSc1
Špilberk	Špilberk	k1gInSc1
Production	Production	k1gInSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
věnuje	věnovat	k5eAaPmIp3nS,k5eAaImIp3nS
produkci	produkce	k1gFnSc3
audio-	audio-	k?
i	i	k8xC
videonahrávek	videonahrávka	k1gFnPc2
<g/>
,	,	kIx,
jejich	jejich	k3xOp3gNnSc2
natáčení	natáčení	k1gNnSc2
a	a	k8xC
rovněž	rovněž	k9
se	se	k3xPyFc4
pohybuje	pohybovat	k5eAaImIp3nS
i	i	k9
v	v	k7c6
oblasti	oblast	k1gFnSc6
dabingu	dabing	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Futurum	futurum	k1gNnSc1
bylo	být	k5eAaImAgNnS
v	v	k7c6
„	„	k?
<g/>
klasické	klasický	k2eAgFnSc6d1
<g/>
“	“	k?
sestavě	sestava	k1gFnSc6
(	(	kIx(
<g/>
Dragoun	dragoun	k1gMnSc1
–	–	k?
Morávek	Morávek	k1gMnSc1
–	–	k?
Kopřiva	Kopřiva	k1gMnSc1
–	–	k?
Seidl	Seidl	k1gMnSc1
<g/>
)	)	kIx)
obnoveno	obnovit	k5eAaPmNgNnS
v	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
příležitostně	příležitostně	k6eAd1
koncertuje	koncertovat	k5eAaImIp3nS
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
opožděně	opožděně	k6eAd1
oslavilo	oslavit	k5eAaPmAgNnS
25	#num#	k4
<g/>
.	.	kIx.
výročí	výročí	k1gNnSc2
vzniku	vznik	k1gInSc2
skupiny	skupina	k1gFnSc2
dvojkoncertem	dvojkoncert	k1gInSc7
se	s	k7c7
všemi	všecek	k3xTgMnPc7
bývalými	bývalý	k2eAgMnPc7d1
členy	člen	k1gMnPc7
kapely	kapela	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
přelomu	přelom	k1gInSc6
roku	rok	k1gInSc2
2011	#num#	k4
<g/>
–	–	k?
<g/>
2012	#num#	k4
začala	začít	k5eAaPmAgFnS
opět	opět	k6eAd1
existovat	existovat	k5eAaImF
znovu	znovu	k6eAd1
obnovená	obnovený	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
Špilberk	Špilberk	k1gInSc1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
po	po	k7c6
více	hodně	k6eAd2
než	než	k8xS
20	#num#	k4
letech	léto	k1gNnPc6
vystoupila	vystoupit	k5eAaPmAgFnS
1	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2012	#num#	k4
na	na	k7c4
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Beat	beat	k1gInSc1
Festu	Festu	k?
v	v	k7c6
brněnském	brněnský	k2eAgInSc6d1
Semilasse	Semilass	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
listopadu	listopad	k1gInSc6
roku	rok	k1gInSc2
2013	#num#	k4
bylo	být	k5eAaImAgNnS
vydáno	vydat	k5eAaPmNgNnS
první	první	k4xOgNnSc1
album	album	k1gNnSc1
kapely	kapela	k1gFnSc2
<g/>
,	,	kIx,
s	s	k7c7
názvem	název	k1gInSc7
„	„	k?
<g/>
Spielberg	Spielberg	k1gMnSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
křest	křest	k1gInSc1
CD	CD	kA
se	se	k3xPyFc4
uskutečnil	uskutečnit	k5eAaPmAgInS
v	v	k7c6
brněnském	brněnský	k2eAgInSc6d1
Semilasse	Semilass	k1gInSc6
na	na	k7c6
Novemberfestu	Novemberfest	k1gInSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Diskografie	diskografie	k1gFnSc1
</s>
<s>
Se	s	k7c7
Synkopami	synkopa	k1gFnPc7
</s>
<s>
1981	#num#	k4
–	–	k?
Sluneční	sluneční	k2eAgFnSc2d1
hodiny	hodina	k1gFnSc2
(	(	kIx(
<g/>
album	album	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
1982	#num#	k4
–	–	k?
„	„	k?
<g/>
Mladší	mladý	k2eAgFnPc4d2
sestry	sestra	k1gFnPc4
vzpomínek	vzpomínka	k1gFnPc2
<g/>
“	“	k?
(	(	kIx(
<g/>
singl	singl	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
S	s	k7c7
Futurem	futurum	k1gNnSc7
</s>
<s>
viz	vidět	k5eAaImRp2nS
kompletní	kompletní	k2eAgFnSc1d1
diskografie	diskografie	k1gFnSc1
Futura	futurum	k1gNnSc2
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1
</s>
<s>
1990	#num#	k4
–	–	k?
Špilberk	Špilberk	k1gInSc1
<g/>
:	:	kIx,
„	„	k?
<g/>
Bouře	bouře	k1gFnSc1
doznívá	doznívat	k5eAaImIp3nS
<g/>
“	“	k?
(	(	kIx(
<g/>
singl	singl	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
1991	#num#	k4
–	–	k?
Loretta	Loretta	k1gMnSc1
<g/>
:	:	kIx,
Loretta	Loretta	k1gFnSc1
(	(	kIx(
<g/>
album	album	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
1992	#num#	k4
–	–	k?
Mňága	Mňága	k1gFnSc1
a	a	k8xC
Žďorp	Žďorp	k1gInSc1
<g/>
:	:	kIx,
Furt	Furt	k?
rovně	roveň	k1gFnSc2
(	(	kIx(
<g/>
album	album	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
2013	#num#	k4
–	–	k?
Špilberk	Špilberk	k1gInSc1
<g/>
:	:	kIx,
„	„	k?
<g/>
Spielberg	Spielberg	k1gMnSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
album	album	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
HISTORIE	historie	k1gFnSc1
SYNKOP	synkopa	k1gFnPc2
61	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Synkopy	synkopa	k1gFnSc2
<g/>
61	#num#	k4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
:	:	kIx,
Synkopy	synkopa	k1gFnSc2
<g/>
61	#num#	k4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
<g/>
.	.	kIx.
↑	↑	k?
TŮMA	Tůma	k1gMnSc1
<g/>
,	,	kIx,
Jaromír	Jaromír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Synkopy	synkopa	k1gFnSc2
61	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ceskyhudebnislovnik	Ceskyhudebnislovnik	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2009-02-09	2009-02-09	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
PRINC	princa	k1gFnPc2
<g/>
,	,	kIx,
Milan	Milan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historie	historie	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Futurum-brno	Futurum-brno	k1gNnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
BALÁK	Balák	k1gMnSc1
<g/>
,	,	kIx,
Miroslav	Miroslav	k1gMnSc1
<g/>
;	;	kIx,
KYTNAR	KYTNAR	kA
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Československý	československý	k2eAgInSc1d1
rock	rock	k1gInSc1
na	na	k7c6
gramofonových	gramofonový	k2eAgFnPc6d1
deskách	deska	k1gFnPc6
<g/>
:	:	kIx,
rocková	rockový	k2eAgFnSc1d1
diskografie	diskografie	k1gFnSc1
1960	#num#	k4
<g/>
–	–	k?
<g/>
1997	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Indies	Indies	k1gInSc1
Records	Recordsa	k1gFnPc2
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
902475	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
344	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obchodnirejstrik	Obchodnirejstrika	k1gFnPc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
GRATIAS	GRATIAS	kA
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
FUTURUM	futurum	k1gNnSc4
–	–	k?
návrat	návrat	k1gInSc1
z	z	k7c2
minulosti	minulost	k1gFnSc2
do	do	k7c2
současnosti	současnost	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ponrepo	Ponrepa	k1gFnSc5
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Kultovní	kultovní	k2eAgFnSc1d1
kapela	kapela	k1gFnSc1
Futurum	futurum	k1gNnSc4
slaví	slavit	k5eAaImIp3nS
25	#num#	k4
<g/>
.	.	kIx.
narozeniny	narozeniny	k1gFnPc4
<g/>
!	!	kIx.
</s>
<s desamb="1">
-	-	kIx~
Ale	ale	k9
odloženě	odloženě	k6eAd1
<g/>
!	!	kIx.
</s>
<s desamb="1">
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jmtv	Jmtv	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2009-01-15	2009-01-15	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
GRATIAS	GRATIAS	kA
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Decibely	decibel	k1gInPc4
nad	nad	k7c7
Brnem	Brno	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tři	tři	k4xCgFnPc4
desetiletí	desetiletí	k1gNnPc2
hlasité	hlasitý	k2eAgFnSc2d1
hudby	hudba	k1gFnSc2
za	za	k7c2
časů	čas	k1gInPc2
totality	totalita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
JOTA	jota	k1gFnSc1
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
.	.	kIx.
192	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7462	#num#	k4
<g/>
-	-	kIx~
<g/>
825	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
164	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Emil	Emil	k1gMnSc1
Kopřiva	Kopřiva	k1gMnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Synkopy	synkopa	k1gFnSc2
61	#num#	k4
Petr	Petr	k1gMnSc1
Směja	Směja	k1gMnSc1
•	•	k?
Pavel	Pavel	k1gMnSc1
Pokorný	Pokorný	k1gMnSc1
•	•	k?
Vilém	Vilém	k1gMnSc1
Majtner	Majtner	k1gMnSc1
•	•	k?
Jan	Jan	k1gMnSc1
Korbička	korbička	k1gFnSc1
•	•	k?
Jiří	Jiří	k1gMnSc1
RybářPetr	RybářPetr	k1gMnSc1
Fischer	Fischer	k1gMnSc1
•	•	k?
Miloš	Miloš	k1gMnSc1
Orság	Orság	k1gMnSc1
•	•	k?
Pavel	Pavel	k1gMnSc1
Smílek	Smílek	k1gMnSc1
•	•	k?
Antonín	Antonín	k1gMnSc1
Smílek	Smílek	k1gMnSc1
•	•	k?
Michal	Michal	k1gMnSc1
Gärtner	Gärtner	k1gMnSc1
•	•	k?
Alice	Alice	k1gFnSc1
Gärtnerová	Gärtnerová	k1gFnSc1
•	•	k?
Pavel	Pavel	k1gMnSc1
Váně	Váň	k1gMnSc2
•	•	k?
Pavel	Pavel	k1gMnSc1
Svoboda	Svoboda	k1gMnSc1
•	•	k?
Emil	Emil	k1gMnSc1
Kopřiva	Kopřiva	k1gMnSc1
•	•	k?
Vratislav	Vratislav	k1gMnSc1
Lukáš	Lukáš	k1gMnSc1
•	•	k?
Miloš	Miloš	k1gMnSc1
Makovský	Makovský	k1gMnSc1
•	•	k?
Veranie	Veranie	k1gFnSc2
Feraut	Feraut	k1gMnSc1
del	del	k?
Carmen	Carmen	k1gInSc1
•	•	k?
Miloš	Miloš	k1gMnSc1
Morávek	Morávek	k1gMnSc1
•	•	k?
Oldřich	Oldřich	k1gMnSc1
Veselý	Veselý	k1gMnSc1
•	•	k?
Jan	Jan	k1gMnSc1
Čarvaš	Čarvaš	k1gMnSc1
•	•	k?
Michal	Michal	k1gMnSc1
Polák	Polák	k1gMnSc1
Studiová	studiový	k2eAgNnPc1d1
alba	album	k1gNnPc4
</s>
<s>
Xantipa	Xantipa	k1gFnSc1
•	•	k?
Formule	formule	k1gFnSc1
1	#num#	k4
•	•	k?
Sluneční	sluneční	k2eAgFnSc2d1
hodiny	hodina	k1gFnSc2
•	•	k?
Křídlení	křídlení	k1gNnSc1
(	(	kIx(
<g/>
Flying	Flying	k1gInSc1
Time	Tim	k1gInSc2
<g/>
)	)	kIx)
•	•	k?
Zrcadla	zrcadlo	k1gNnSc2
•	•	k?
Dlouhá	dlouhý	k2eAgFnSc1d1
noc	noc	k1gFnSc1
Kompilace	kompilace	k1gFnSc2
</s>
<s>
25	#num#	k4
let	léto	k1gNnPc2
Synkop	synkopa	k1gFnPc2
<g/>
:	:	kIx,
Výběr	výběr	k1gInSc1
z	z	k7c2
let	léto	k1gNnPc2
1966	#num#	k4
<g/>
–	–	k?
<g/>
1974	#num#	k4
•	•	k?
Válka	Válka	k1gMnSc1
je	být	k5eAaImIp3nS
vůl	vůl	k1gMnSc1
(	(	kIx(
<g/>
The	The	k1gMnSc1
Best	Best	k1gMnSc1
of	of	k?
<g/>
)	)	kIx)
•	•	k?
Válka	válka	k1gFnSc1
je	být	k5eAaImIp3nS
vůl	vůl	k1gMnSc1
(	(	kIx(
<g/>
Vol	vol	k6eAd1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Hity	hit	k1gInPc7
1967	#num#	k4
-	-	kIx~
1979	#num#	k4
•	•	k?
Válka	Válka	k1gMnSc1
je	být	k5eAaImIp3nS
vůl	vůl	k1gMnSc1
(	(	kIx(
<g/>
Vol	vol	k6eAd1
<g/>
.	.	kIx.
3	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Nevydané	vydaný	k2eNgInPc4d1
hity	hit	k1gInPc4
&	&	k?
rarity	rarita	k1gFnSc2
•	•	k?
Zrcadla	zrcadlo	k1gNnPc1
<g/>
/	/	kIx~
<g/>
Dlouhá	dlouhý	k2eAgFnSc1d1
noc	noc	k1gFnSc1
•	•	k?
Festival-Xantipa-Formule	Festival-Xantipa-Formule	k1gFnSc1
1	#num#	k4
EP	EP	kA
</s>
<s>
Synkopy	synkopa	k1gFnPc1
61	#num#	k4
(	(	kIx(
<g/>
1968	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Synkopy	synkopa	k1gFnPc4
61	#num#	k4
(	(	kIx(
<g/>
1970	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Festival	festival	k1gInSc1
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
diskografie	diskografie	k1gFnSc1
Synkop	synkopa	k1gFnPc2
61	#num#	k4
•	•	k?
Progres	progres	k1gInSc1
2	#num#	k4
•	•	k?
Futurum	futurum	k1gNnSc4
•	•	k?
Taxi	taxi	k1gNnSc2
</s>
<s>
Futurum	futurum	k1gNnSc1
Roman	Roman	k1gMnSc1
Dragoun	dragoun	k1gMnSc1
•	•	k?
Miloš	Miloš	k1gMnSc1
Morávek	Morávek	k1gMnSc1
•	•	k?
Emil	Emil	k1gMnSc1
Kopřiva	Kopřiva	k1gMnSc1
•	•	k?
Jakub	Jakub	k1gMnSc1
Michálek	Michálek	k1gMnSc1
•	•	k?
Jan	Jan	k1gMnSc1
SeidlLubomír	SeidlLubomír	k1gMnSc1
Eremiáš	Eremiáš	k1gMnSc1
•	•	k?
Leopold	Leopold	k1gMnSc1
Dvořáček	Dvořáček	k1gMnSc1
•	•	k?
Pavel	Pavel	k1gMnSc1
Pelc	Pelc	k1gMnSc1
Studiová	studiový	k2eAgFnSc5d1
alba	album	k1gNnPc4
</s>
<s>
Ostrov	ostrov	k1gInSc1
Země	zem	k1gFnSc2
•	•	k?
Jedinečná	jedinečný	k2eAgFnSc1d1
šance	šance	k1gFnSc1
Koncertní	koncertní	k2eAgFnSc1d1
alba	alba	k1gFnSc1
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
narozeniny	narozeniny	k1gFnPc1
Kompilace	kompilace	k1gFnSc2
</s>
<s>
Ostrov	ostrov	k1gInSc1
Země	zem	k1gFnSc2
<g/>
/	/	kIx~
<g/>
Jedinečná	jedinečný	k2eAgFnSc1d1
šance	šance	k1gFnSc1
Singly	singl	k1gInPc1
</s>
<s>
„	„	k?
<g/>
Juliet	Juliet	k1gInSc1
<g/>
“	“	k?
•	•	k?
„	„	k?
<g/>
Superměsto	Superměsta	k1gMnSc5
<g/>
“	“	k?
•	•	k?
„	„	k?
<g/>
Jedinečná	jedinečný	k2eAgFnSc1d1
šance	šance	k1gFnSc1
<g/>
“	“	k?
Hudební	hudební	k2eAgInPc4d1
videozáznamy	videozáznam	k1gInPc4
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
narozeniny	narozeniny	k1gFnPc4
Související	související	k2eAgFnPc4d1
články	článek	k1gInPc4
</s>
<s>
Progres	progres	k1gInSc1
2	#num#	k4
•	•	k?
Synkopy	synkopa	k1gFnSc2
61	#num#	k4
•	•	k?
Stromboli	Strombole	k1gFnSc4
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
xx	xx	k?
<g/>
0	#num#	k4
<g/>
119281	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
122210661	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Hudba	hudba	k1gFnSc1
</s>
