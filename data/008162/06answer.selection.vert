<s>
Geologové	geolog	k1gMnPc1
se	se	k3xPyFc4
snažili	snažit	k5eAaImAgMnP
pomocí	pomocí	k7c2
magnetické	magnetický	k2eAgFnSc2d1
charakteristiky	charakteristika	k1gFnSc2
kamenů	kámen	k1gInPc2
z	z	k7c2
Pompejí	Pompeje	k1gFnPc2
určit	určit	k5eAaPmF
teplotu	teplota	k1gFnSc4
pyroklastické	pyroklastický	k2eAgFnSc2d1
vlny	vlna	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1
pohřbila	pohřbít	k5eAaPmAgFnS
město	město	k1gNnSc4
<g/>
.	.	kIx.
</s>