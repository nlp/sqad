<s>
Difuze	difuze	k1gFnSc1	difuze
je	být	k5eAaImIp3nS	být
proces	proces	k1gInSc1	proces
samovolného	samovolný	k2eAgNnSc2d1	samovolné
rozptylování	rozptylování	k1gNnSc2	rozptylování
částic	částice	k1gFnPc2	částice
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Veškeré	veškerý	k3xTgFnPc1	veškerý
látky	látka	k1gFnPc1	látka
mají	mít	k5eAaImIp3nP	mít
tendenci	tendence	k1gFnSc4	tendence
přecházet	přecházet	k5eAaImF	přecházet
z	z	k7c2	z
prostředí	prostředí	k1gNnSc2	prostředí
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
vyšší	vysoký	k2eAgFnSc7d2	vyšší
koncentrací	koncentrace	k1gFnSc7	koncentrace
do	do	k7c2	do
prostředí	prostředí	k1gNnSc2	prostředí
s	s	k7c7	s
nižší	nízký	k2eAgFnSc7d2	nižší
koncentrací	koncentrace	k1gFnSc7	koncentrace
<g/>
.	.	kIx.	.
</s>
<s>
Přirozenou	přirozený	k2eAgFnSc7d1	přirozená
vlastností	vlastnost	k1gFnSc7	vlastnost
látek	látka	k1gFnPc2	látka
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
její	její	k3xOp3gFnPc1	její
částice	částice	k1gFnPc1	částice
mohou	moct	k5eAaImIp3nP	moct
pohybovat	pohybovat	k5eAaImF	pohybovat
(	(	kIx(	(
<g/>
molekuly	molekula	k1gFnPc1	molekula
v	v	k7c6	v
nehybném	hybný	k2eNgInSc6d1	nehybný
roztoku	roztok	k1gInSc6	roztok
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
náhodného	náhodný	k2eAgInSc2d1	náhodný
pohybu	pohyb	k1gInSc2	pohyb
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
rozptylují	rozptylovat	k5eAaImIp3nP	rozptylovat
do	do	k7c2	do
celého	celý	k2eAgInSc2d1	celý
prostoru	prostor	k1gInSc2	prostor
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
mohou	moct	k5eAaImIp3nP	moct
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
<g/>
,	,	kIx,	,
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
jeho	jeho	k3xOp3gFnPc6	jeho
částech	část	k1gFnPc6	část
vyrovnají	vyrovnat	k5eAaBmIp3nP	vyrovnat
svou	svůj	k3xOyFgFnSc4	svůj
koncentraci	koncentrace	k1gFnSc4	koncentrace
<g/>
.	.	kIx.	.
</s>
<s>
Říkáme	říkat	k5eAaImIp1nP	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
látky	látka	k1gFnPc1	látka
difundují	difundovat	k5eAaImIp3nP	difundovat
<g/>
.	.	kIx.	.
</s>
<s>
Fyzikální	fyzikální	k2eAgFnSc7d1	fyzikální
podstatou	podstata	k1gFnSc7	podstata
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
druhého	druhý	k4xOgInSc2	druhý
termodynamického	termodynamický	k2eAgInSc2d1	termodynamický
zákona	zákon	k1gInSc2	zákon
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
termodynamický	termodynamický	k2eAgInSc1d1	termodynamický
systém	systém	k1gInSc1	systém
vždy	vždy	k6eAd1	vždy
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
svou	svůj	k3xOyFgFnSc4	svůj
entropii	entropie	k1gFnSc4	entropie
neboli	neboli	k8xC	neboli
míru	míra	k1gFnSc4	míra
neuspořádanosti	neuspořádanost	k1gFnSc2	neuspořádanost
svého	svůj	k3xOyFgInSc2	svůj
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
dospěje	dochvít	k5eAaPmIp3nS	dochvít
ke	k	k7c3	k
stavu	stav	k1gInSc3	stav
s	s	k7c7	s
nejnižší	nízký	k2eAgFnSc7d3	nejnižší
vnitřní	vnitřní	k2eAgFnSc7d1	vnitřní
energií	energie	k1gFnSc7	energie
<g/>
.	.	kIx.	.
</s>
<s>
Částice	částice	k1gFnPc1	částice
plynu	plyn	k1gInSc2	plyn
mají	mít	k5eAaImIp3nP	mít
největší	veliký	k2eAgFnSc4d3	veliký
kinetickou	kinetický	k2eAgFnSc4d1	kinetická
energii	energie	k1gFnSc4	energie
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
zde	zde	k6eAd1	zde
difuze	difuze	k1gFnSc1	difuze
probíhá	probíhat	k5eAaImIp3nS	probíhat
nejrychleji	rychle	k6eAd3	rychle
<g/>
.	.	kIx.	.
</s>
<s>
Částice	částice	k1gFnSc1	částice
nějakého	nějaký	k3yIgInSc2	nějaký
plynu	plyn	k1gInSc2	plyn
se	se	k3xPyFc4	se
prolínají	prolínat	k5eAaImIp3nP	prolínat
s	s	k7c7	s
plynem	plyn	k1gInSc7	plyn
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
původně	původně	k6eAd1	původně
<g/>
.	.	kIx.	.
</s>
<s>
Dokud	dokud	k8xS	dokud
se	se	k3xPyFc4	se
nevyrovnají	vyrovnat	k5eNaPmIp3nP	vyrovnat
koncentrace	koncentrace	k1gFnPc1	koncentrace
<g/>
,	,	kIx,	,
probíhá	probíhat	k5eAaImIp3nS	probíhat
difuze	difuze	k1gFnSc1	difuze
<g/>
.	.	kIx.	.
</s>
<s>
Závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
čase	čas	k1gInSc6	čas
a	a	k8xC	a
teplotě	teplota	k1gFnSc6	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Grahamův	Grahamův	k2eAgInSc1d1	Grahamův
zákon	zákon	k1gInSc1	zákon
difúze	difúze	k1gFnSc2	difúze
-	-	kIx~	-
za	za	k7c2	za
konstantní	konstantní	k2eAgFnSc2d1	konstantní
teploty	teplota	k1gFnSc2	teplota
a	a	k8xC	a
tlaku	tlak	k1gInSc2	tlak
je	být	k5eAaImIp3nS	být
rychlost	rychlost	k1gFnSc1	rychlost
difúze	difúze	k1gFnSc2	difúze
plynu	plynout	k5eAaImIp1nS	plynout
nepřímo	přímo	k6eNd1	přímo
úměrná	úměrný	k2eAgFnSc1d1	úměrná
druhé	druhý	k4xOgFnSc6	druhý
odmocnině	odmocnina	k1gFnSc6	odmocnina
jeho	jeho	k3xOp3gFnPc4	jeho
hustoty	hustota	k1gFnPc4	hustota
<g/>
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
i	i	k8xC	i
f	f	k?	f
u	u	k7c2	u
z	z	k7c2	z
e	e	k0	e
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
/	/	kIx~	/
:	:	kIx,	:
√	√	k?	√
h	h	k?	h
u	u	k7c2	u
s	s	k7c7	s
t	t	k?	t
o	o	k7c6	o
t	t	k?	t
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
difuze	difuze	k1gFnPc1	difuze
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
\	\	kIx~	\
<g/>
surd	surd	k6eAd1	surd
hustota	hustota	k1gFnSc1	hustota
<g/>
}	}	kIx)	}
:	:	kIx,	:
Částice	částice	k1gFnSc1	částice
se	se	k3xPyFc4	se
po	po	k7c6	po
sobě	se	k3xPyFc3	se
jen	jen	k9	jen
posouvají	posouvat	k5eAaImIp3nP	posouvat
<g/>
.	.	kIx.	.
</s>
<s>
Závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
čase	čas	k1gInSc6	čas
a	a	k8xC	a
teplotě	teplota	k1gFnSc6	teplota
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
na	na	k7c6	na
viskozitě	viskozita	k1gFnSc6	viskozita
a	a	k8xC	a
vzájemné	vzájemný	k2eAgFnSc3d1	vzájemná
rozpustnosti	rozpustnost	k1gFnSc3	rozpustnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tuhých	tuhý	k2eAgFnPc6d1	tuhá
látkách	látka	k1gFnPc6	látka
také	také	k9	také
probíhá	probíhat	k5eAaImIp3nS	probíhat
difuze	difuze	k1gFnSc1	difuze
<g/>
,	,	kIx,	,
nejlépe	dobře	k6eAd3	dobře
difundují	difundovat	k5eAaImIp3nP	difundovat
do	do	k7c2	do
tuhé	tuhý	k2eAgFnSc2d1	tuhá
látky	látka	k1gFnSc2	látka
plynné	plynný	k2eAgFnSc2d1	plynná
látky	látka	k1gFnSc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Prvky	prvek	k1gInPc1	prvek
mohou	moct	k5eAaImIp3nP	moct
difundovat	difundovat	k5eAaImF	difundovat
v	v	k7c6	v
objemu	objem	k1gInSc6	objem
pevné	pevný	k2eAgFnSc2d1	pevná
látky	látka	k1gFnSc2	látka
(	(	kIx(	(
<g/>
objemová	objemový	k2eAgFnSc1d1	objemová
difúze	difúze	k1gFnSc1	difúze
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
po	po	k7c6	po
hranicích	hranice	k1gFnPc6	hranice
zrn	zrno	k1gNnPc2	zrno
<g/>
.	.	kIx.	.
</s>
<s>
Difúze	difúze	k1gFnSc1	difúze
po	po	k7c6	po
hranicích	hranice	k1gFnPc6	hranice
zrn	zrno	k1gNnPc2	zrno
je	být	k5eAaImIp3nS	být
řádově	řádově	k6eAd1	řádově
rychlejší	rychlý	k2eAgFnSc1d2	rychlejší
než	než	k8xS	než
objemová	objemový	k2eAgFnSc1d1	objemová
difúze	difúze	k1gFnSc1	difúze
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
arsen	arsen	k1gInSc4	arsen
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
vrstevnatých	vrstevnatý	k2eAgFnPc2d1	vrstevnatá
struktur	struktura	k1gFnPc2	struktura
–	–	k?	–
integrované	integrovaný	k2eAgInPc4d1	integrovaný
obvody	obvod	k1gInPc4	obvod
<g/>
,	,	kIx,	,
fotovoltaické	fotovoltaický	k2eAgInPc4d1	fotovoltaický
články	článek	k1gInPc4	článek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
je	být	k5eAaImIp3nS	být
reakce	reakce	k1gFnSc1	reakce
velmi	velmi	k6eAd1	velmi
rychlá	rychlý	k2eAgFnSc1d1	rychlá
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
jev	jev	k1gInSc1	jev
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
významně	významně	k6eAd1	významně
podílí	podílet	k5eAaImIp3nS	podílet
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
biologických	biologický	k2eAgInPc6d1	biologický
<g/>
,	,	kIx,	,
chemických	chemický	k2eAgInPc6d1	chemický
i	i	k8xC	i
fyzikálních	fyzikální	k2eAgInPc6d1	fyzikální
procesech	proces	k1gInPc6	proces
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
látky	látka	k1gFnPc1	látka
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
uvolňují	uvolňovat	k5eAaImIp3nP	uvolňovat
z	z	k7c2	z
čajového	čajový	k2eAgInSc2d1	čajový
nálevového	nálevový	k2eAgInSc2d1	nálevový
sáčku	sáček	k1gInSc2	sáček
<g/>
,	,	kIx,	,
zalijeme	zalít	k5eAaPmIp1nP	zalít
<g/>
-li	i	k?	-li
jej	on	k3xPp3gMnSc4	on
horkou	horký	k2eAgFnSc7d1	horká
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
stejného	stejný	k2eAgInSc2d1	stejný
zákona	zákon	k1gInSc2	zákon
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
chovat	chovat	k5eAaImF	chovat
též	též	k9	též
cukr	cukr	k1gInSc4	cukr
<g/>
,	,	kIx,	,
přidáme	přidat	k5eAaPmIp1nP	přidat
<g/>
-li	i	k?	-li
jej	on	k3xPp3gNnSc4	on
později	pozdě	k6eAd2	pozdě
do	do	k7c2	do
tohoto	tento	k3xDgInSc2	tento
čaje	čaj	k1gInSc2	čaj
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Není	být	k5eNaImIp3nS	být
<g/>
-li	i	k?	-li
však	však	k9	však
voda	voda	k1gFnSc1	voda
teplá	teplý	k2eAgFnSc1d1	teplá
<g/>
,	,	kIx,	,
látky	látka	k1gFnPc1	látka
se	se	k3xPyFc4	se
z	z	k7c2	z
lístků	lístek	k1gInPc2	lístek
vůbec	vůbec	k9	vůbec
neuvolní	uvolnit	k5eNaPmIp3nS	uvolnit
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
<g/>
-li	i	k?	-li
teplá	teplý	k2eAgFnSc1d1	teplá
dost	dost	k6eAd1	dost
<g/>
,	,	kIx,	,
látky	látka	k1gFnPc1	látka
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
částečně	částečně	k6eAd1	částečně
uvolní	uvolnit	k5eAaPmIp3nS	uvolnit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nemají	mít	k5eNaImIp3nP	mít
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
dost	dost	k6eAd1	dost
energie	energie	k1gFnSc2	energie
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
pohyb	pohyb	k1gInSc4	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Jiným	jiný	k2eAgInSc7d1	jiný
příkladem	příklad	k1gInSc7	příklad
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
např.	např.	kA	např.
domácí	domácí	k2eAgNnSc1d1	domácí
kompostování	kompostování	k1gNnSc1	kompostování
nebo	nebo	k8xC	nebo
kompostování	kompostování	k1gNnSc1	kompostování
na	na	k7c6	na
malých	malý	k2eAgFnPc6d1	malá
hromadách	hromada	k1gFnPc6	hromada
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pomocí	pomocí	k7c2	pomocí
tohoto	tento	k3xDgInSc2	tento
přirozeného	přirozený	k2eAgInSc2d1	přirozený
procesu	proces	k1gInSc2	proces
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
zabezpečeno	zabezpečen	k2eAgNnSc4d1	zabezpečeno
pronikání	pronikání	k1gNnSc4	pronikání
kyslíku	kyslík	k1gInSc2	kyslík
směrem	směr	k1gInSc7	směr
dovnitř	dovnitř	k6eAd1	dovnitř
kompostovaného	kompostovaný	k2eAgInSc2d1	kompostovaný
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
jeho	jeho	k3xOp3gFnSc4	jeho
aerace	aerace	k1gFnSc1	aerace
<g/>
.	.	kIx.	.
</s>
<s>
Kyslík	kyslík	k1gInSc1	kyslík
ze	z	k7c2	z
vzduchu	vzduch	k1gInSc2	vzduch
(	(	kIx(	(
<g/>
prostředí	prostředí	k1gNnSc1	prostředí
o	o	k7c4	o
vyšší	vysoký	k2eAgFnSc4d2	vyšší
koncentraci	koncentrace	k1gFnSc4	koncentrace
<g/>
)	)	kIx)	)
samovolně	samovolně	k6eAd1	samovolně
přechází	přecházet	k5eAaImIp3nS	přecházet
do	do	k7c2	do
pórů	pór	k1gInPc2	pór
uvnitř	uvnitř	k7c2	uvnitř
kompostu	kompost	k1gInSc2	kompost
(	(	kIx(	(
<g/>
prostředí	prostředí	k1gNnSc4	prostředí
s	s	k7c7	s
nižší	nízký	k2eAgFnSc7d2	nižší
koncentrací	koncentrace	k1gFnSc7	koncentrace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přirozené	přirozený	k2eAgFnSc6d1	přirozená
aeraci	aerace	k1gFnSc6	aerace
se	se	k3xPyFc4	se
však	však	k9	však
také	také	k9	také
podílí	podílet	k5eAaImIp3nS	podílet
konvekce	konvekce	k1gFnSc1	konvekce
<g/>
.	.	kIx.	.
</s>
<s>
Difuzi	difuze	k1gFnSc4	difuze
popisuje	popisovat	k5eAaImIp3nS	popisovat
první	první	k4xOgInSc1	první
Fickův	Fickův	k2eAgInSc1d1	Fickův
zákon	zákon	k1gInSc1	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Difuze	difuze	k1gFnSc1	difuze
je	být	k5eAaImIp3nS	být
matematicky	matematicky	k6eAd1	matematicky
podchytitelný	podchytitelný	k2eAgInSc1d1	podchytitelný
chemický	chemický	k2eAgInSc1d1	chemický
jev	jev	k1gInSc1	jev
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
modelovatelný	modelovatelný	k2eAgInSc4d1	modelovatelný
výpočty	výpočet	k1gInPc4	výpočet
<g/>
.	.	kIx.	.
</s>
<s>
Osmóza	osmóza	k1gFnSc1	osmóza
je	být	k5eAaImIp3nS	být
určitým	určitý	k2eAgInSc7d1	určitý
specifickým	specifický	k2eAgInSc7d1	specifický
případem	případ	k1gInSc7	případ
difuze	difuze	k1gFnSc2	difuze
<g/>
.	.	kIx.	.
</s>
<s>
Difuze	difuze	k1gFnSc1	difuze
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
podstatou	podstata	k1gFnSc7	podstata
a	a	k8xC	a
hybnou	hybný	k2eAgFnSc7d1	hybná
silou	síla	k1gFnSc7	síla
osmózy	osmóza	k1gFnSc2	osmóza
a	a	k8xC	a
veškerých	veškerý	k3xTgInPc2	veškerý
osmotických	osmotický	k2eAgInPc2d1	osmotický
jevů	jev	k1gInPc2	jev
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
jsou	být	k5eAaImIp3nP	být
látky	látka	k1gFnPc1	látka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
vysokou	vysoký	k2eAgFnSc4d1	vysoká
koncentraci	koncentrace	k1gFnSc4	koncentrace
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
rozpuštěné	rozpuštěný	k2eAgInPc1d1	rozpuštěný
v	v	k7c6	v
nějakém	nějaký	k3yIgInSc6	nějaký
rozpouštědle	rozpouštědlo	k1gNnSc6	rozpouštědlo
odděleny	oddělit	k5eAaPmNgInP	oddělit
nějakou	nějaký	k3yIgFnSc7	nějaký
polopropustnou	polopropustný	k2eAgFnSc7d1	polopropustná
(	(	kIx(	(
<g/>
semipermeabilní	semipermeabilní	k2eAgFnSc7d1	semipermeabilní
<g/>
)	)	kIx)	)
bariérou	bariéra	k1gFnSc7	bariéra
(	(	kIx(	(
<g/>
nejčastěji	často	k6eAd3	často
membránou	membrána	k1gFnSc7	membrána
<g/>
)	)	kIx)	)
od	od	k7c2	od
směru	směr	k1gInSc2	směr
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgInP	mít
difundovat	difundovat	k5eAaImF	difundovat
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
touto	tento	k3xDgFnSc7	tento
polopropustnou	polopropustný	k2eAgFnSc7d1	polopropustná
bariérou	bariéra	k1gFnSc7	bariéra
může	moct	k5eAaImIp3nS	moct
procházet	procházet	k5eAaImF	procházet
rozpouštědlo	rozpouštědlo	k1gNnSc4	rozpouštědlo
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
přechází	přecházet	k5eAaImIp3nS	přecházet
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
obsahující	obsahující	k2eAgFnSc2d1	obsahující
rozpouštěné	rozpouštěný	k2eAgFnSc2d1	rozpouštěná
látky	látka	k1gFnSc2	látka
tak	tak	k6eAd1	tak
dlouho	dlouho	k6eAd1	dlouho
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
tyto	tento	k3xDgFnPc1	tento
látky	látka	k1gFnPc1	látka
nenaředí	naředit	k5eNaPmIp3nP	naředit
na	na	k7c4	na
stejnou	stejný	k2eAgFnSc4d1	stejná
koncentraci	koncentrace	k1gFnSc4	koncentrace
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
ze	z	k7c2	z
které	který	k3yRgMnPc4	který
rozpouštědlo	rozpouštědlo	k1gNnSc1	rozpouštědlo
přitéká	přitékat	k5eAaImIp3nS	přitékat
<g/>
.	.	kIx.	.
</s>
<s>
Jinak	jinak	k6eAd1	jinak
řečeno	říct	k5eAaPmNgNnS	říct
<g/>
,	,	kIx,	,
koncentrované	koncentrovaný	k2eAgFnPc1d1	koncentrovaná
látky	látka	k1gFnPc1	látka
si	se	k3xPyFc3	se
své	svůj	k3xOyFgNnSc4	svůj
rozpouštědlo	rozpouštědlo	k1gNnSc4	rozpouštědlo
jakoby	jakoby	k8xS	jakoby
přitáhnou	přitáhnout	k5eAaPmIp3nP	přitáhnout
k	k	k7c3	k
sobě	se	k3xPyFc3	se
<g/>
.	.	kIx.	.
</s>
<s>
Dokážou	dokázat	k5eAaPmIp3nP	dokázat
k	k	k7c3	k
tomu	ten	k3xDgMnSc3	ten
vyvinout	vyvinout	k5eAaPmF	vyvinout
značnou	značný	k2eAgFnSc4d1	značná
sílu	síla	k1gFnSc4	síla
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
překoná	překonat	k5eAaPmIp3nS	překonat
např.	např.	kA	např.
sílu	síla	k1gFnSc4	síla
gravitační	gravitační	k2eAgInPc1d1	gravitační
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
obrázek	obrázek	k1gInSc1	obrázek
"	"	kIx"	"
<g/>
Osmóza	osmóza	k1gFnSc1	osmóza
<g/>
"	"	kIx"	"
vpravo	vpravo	k6eAd1	vpravo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tuto	tento	k3xDgFnSc4	tento
sílu	síla	k1gFnSc4	síla
nazýváme	nazývat	k5eAaImIp1nP	nazývat
osmotickou	osmotický	k2eAgFnSc7d1	osmotická
silou	síla	k1gFnSc7	síla
<g/>
.	.	kIx.	.
</s>
<s>
Osmotické	osmotický	k2eAgInPc1d1	osmotický
jevy	jev	k1gInPc1	jev
jsou	být	k5eAaImIp3nP	být
významnou	významný	k2eAgFnSc7d1	významná
hybnou	hybný	k2eAgFnSc7d1	hybná
silou	síla	k1gFnSc7	síla
mnoha	mnoho	k4c2	mnoho
biologických	biologický	k2eAgInPc2d1	biologický
procesů	proces	k1gInPc2	proces
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
třešně	třešeň	k1gFnPc1	třešeň
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
mnoho	mnoho	k4c4	mnoho
cukru	cukr	k1gInSc2	cukr
<g/>
,	,	kIx,	,
za	za	k7c2	za
vydatného	vydatný	k2eAgInSc2d1	vydatný
deště	dešť	k1gInSc2	dešť
popraskají	popraskat	k5eAaPmIp3nP	popraskat
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
cukerný	cukerný	k2eAgInSc1d1	cukerný
roztok	roztok	k1gInSc1	roztok
uvnitř	uvnitř	k7c2	uvnitř
třešňových	třešňový	k2eAgFnPc2d1	třešňová
buněk	buňka	k1gFnPc2	buňka
vtahuje	vtahovat	k5eAaImIp3nS	vtahovat
okolní	okolní	k2eAgFnSc4d1	okolní
čistou	čistý	k2eAgFnSc4d1	čistá
vodu	voda	k1gFnSc4	voda
dovnitř	dovnitř	k6eAd1	dovnitř
buňky	buňka	k1gFnSc2	buňka
<g/>
;	;	kIx,	;
třešně	třešeň	k1gFnPc1	třešeň
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
takzvaném	takzvaný	k2eAgNnSc6d1	takzvané
hypotonickém	hypotonický	k2eAgNnSc6d1	hypotonické
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
)	)	kIx)	)
Proto	proto	k8xC	proto
naopak	naopak	k6eAd1	naopak
uschnou	uschnout	k5eAaPmIp3nP	uschnout
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
pomočeny	pomočen	k2eAgFnPc1d1	pomočen
naším	náš	k3xOp1gInSc7	náš
domácím	domácí	k2eAgInSc6d1	domácí
<g />
.	.	kIx.	.
</s>
<s>
mazlíčkem	mazlíček	k1gMnSc7	mazlíček
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Moč	moč	k1gFnSc1	moč
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
velice	velice	k6eAd1	velice
koncentrovaný	koncentrovaný	k2eAgInSc1d1	koncentrovaný
roztok	roztok	k1gInSc1	roztok
soli	sůl	k1gFnSc2	sůl
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vytahuje	vytahovat	k5eAaImIp3nS	vytahovat
vláhu	vláha	k1gFnSc4	vláha
z	z	k7c2	z
potřísněných	potřísněný	k2eAgFnPc2d1	potřísněná
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
;	;	kIx,	;
Moč	moč	k1gFnSc1	moč
představuje	představovat	k5eAaImIp3nS	představovat
pro	pro	k7c4	pro
ony	onen	k3xDgFnPc4	onen
rostliny	rostlina	k1gFnPc4	rostlina
tzv.	tzv.	kA	tzv.
hypertonické	hypertonický	k2eAgNnSc1d1	hypertonické
prostředí	prostředí	k1gNnSc1	prostředí
<g/>
)	)	kIx)	)
Proto	proto	k8xC	proto
také	také	k9	také
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
pacientovi	pacient	k1gMnSc3	pacient
podána	podat	k5eAaPmNgFnS	podat
nitrožilně	nitrožilně	k6eAd1	nitrožilně
čistá	čistý	k2eAgFnSc1d1	čistá
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tzv.	tzv.	kA	tzv.
fyziologický	fyziologický	k2eAgInSc1d1	fyziologický
roztok	roztok	k1gInSc1	roztok
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
obdobnou	obdobný	k2eAgFnSc4d1	obdobná
koncentraci	koncentrace	k1gFnSc4	koncentrace
rozpuštěných	rozpuštěný	k2eAgFnPc2d1	rozpuštěná
látek	látka	k1gFnPc2	látka
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
z	z	k7c2	z
osmotického	osmotický	k2eAgNnSc2d1	osmotické
hlediska	hledisko	k1gNnSc2	hledisko
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
isotonický	isotonický	k2eAgInSc4d1	isotonický
roztok	roztok	k1gInSc4	roztok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jakožto	jakožto	k8xS	jakožto
fyziologický	fyziologický	k2eAgInSc1d1	fyziologický
roztok	roztok	k1gInSc1	roztok
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
0,9	[number]	k4	0,9
<g/>
%	%	kIx~	%
roztok	roztok	k1gInSc1	roztok
NaCl	NaCla	k1gFnPc2	NaCla
<g/>
.	.	kIx.	.
</s>
<s>
Tohoto	tento	k3xDgInSc2	tento
principu	princip	k1gInSc2	princip
bývá	bývat	k5eAaImIp3nS	bývat
využíváno	využívat	k5eAaPmNgNnS	využívat
při	při	k7c6	při
určitých	určitý	k2eAgInPc6d1	určitý
způsobech	způsob	k1gInPc6	způsob
konzervace	konzervace	k1gFnSc2	konzervace
potravin	potravina	k1gFnPc2	potravina
<g/>
.	.	kIx.	.
</s>
<s>
Cukerný	cukerný	k2eAgInSc1d1	cukerný
sirup	sirup	k1gInSc1	sirup
a	a	k8xC	a
slanečci	slaneček	k1gMnPc1	slaneček
jsou	být	k5eAaImIp3nP	být
sterilizovaní	sterilizovaný	k2eAgMnPc1d1	sterilizovaný
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
patogeny	patogen	k1gInPc1	patogen
nemohou	moct	k5eNaImIp3nP	moct
přežít	přežít	k5eAaPmF	přežít
hypertonickou	hypertonický	k2eAgFnSc4d1	hypertonická
koncentraci	koncentrace	k1gFnSc4	koncentrace
cukru	cukr	k1gInSc2	cukr
<g/>
/	/	kIx~	/
<g/>
soli	sůl	k1gFnSc2	sůl
<g/>
.	.	kIx.	.
</s>
<s>
Počítačová	počítačový	k2eAgNnPc4d1	počítačové
simulace	simulace	k1gFnPc4	simulace
Difuze	difuze	k1gFnSc2	difuze
(	(	kIx(	(
<g/>
doporučujeme	doporučovat	k5eAaImIp1nP	doporučovat
vypnutí	vypnutí	k1gNnSc4	vypnutí
volby	volba	k1gFnSc2	volba
"	"	kIx"	"
<g/>
trace	trako	k6eAd1	trako
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
Jiná	jiné	k1gNnPc4	jiné
simulace	simulace	k1gFnSc2	simulace
Difuze	difuze	k1gFnSc2	difuze
První	první	k4xOgInSc4	první
Fickův	Fickův	k2eAgInSc4d1	Fickův
zákon	zákon	k1gInSc4	zákon
Druhý	druhý	k4xOgInSc4	druhý
Fickův	Fickův	k2eAgInSc4d1	Fickův
zákon	zákon	k1gInSc4	zákon
Turgor	turgor	k1gInSc4	turgor
Difuzor	difuzor	k1gInSc4	difuzor
</s>
