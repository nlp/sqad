<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nedokonalou	dokonalý	k2eNgFnSc4d1	nedokonalá
kouli	koule	k1gFnSc4	koule
s	s	k7c7	s
poloměrem	poloměr	k1gInSc7	poloměr
6378	[number]	k4	6378
km	km	kA	km
<g/>
,	,	kIx,	,
uprostřed	uprostřed	k6eAd1	uprostřed
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
malé	malý	k2eAgNnSc1d1	malé
pevné	pevný	k2eAgNnSc1d1	pevné
jádro	jádro	k1gNnSc1	jádro
obklopené	obklopený	k2eAgNnSc1d1	obklopené
polotekutým	polotekutý	k2eAgNnSc7d1	polotekuté
vnějším	vnější	k2eAgNnSc7d1	vnější
jádrem	jádro	k1gNnSc7	jádro
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
pláštěm	plášť	k1gInSc7	plášť
a	a	k8xC	a
zemskou	zemský	k2eAgFnSc7d1	zemská
kůrou	kůra	k1gFnSc7	kůra
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
oceánskou	oceánský	k2eAgFnSc4d1	oceánská
a	a	k8xC	a
kontinentální	kontinentální	k2eAgFnSc4d1	kontinentální
<g/>
.	.	kIx.	.
</s>
