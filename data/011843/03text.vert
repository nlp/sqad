<p>
<s>
Bota	bota	k1gFnSc1	bota
jménem	jméno	k1gNnSc7	jméno
Melichar	Melichar	k1gMnSc1	Melichar
je	být	k5eAaImIp3nS	být
český	český	k2eAgInSc4d1	český
dětský	dětský	k2eAgInSc4d1	dětský
film	film	k1gInSc4	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
režiséra	režisér	k1gMnSc2	režisér
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Trošky	troška	k1gFnSc2	troška
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Děj	děj	k1gInSc1	děj
==	==	k?	==
</s>
</p>
<p>
<s>
Děj	děj	k1gInSc1	děj
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
ve	v	k7c6	v
škole	škola	k1gFnSc6	škola
<g/>
.	.	kIx.	.
</s>
<s>
Žákům	Žák	k1gMnPc3	Žák
vadí	vadit	k5eAaImIp3nS	vadit
nová	nový	k2eAgFnSc1d1	nová
školní	školní	k2eAgFnSc1d1	školní
budova	budova	k1gFnSc1	budova
a	a	k8xC	a
také	také	k9	také
časté	častý	k2eAgNnSc1d1	časté
nucené	nucený	k2eAgNnSc1d1	nucené
přezouvání	přezouvání	k1gNnSc1	přezouvání
<g/>
.	.	kIx.	.
</s>
<s>
Prvňák	prvňák	k1gMnSc1	prvňák
Honzík	Honzík	k1gMnSc1	Honzík
dopadne	dopadnout	k5eAaPmIp3nS	dopadnout
zlodějku	zlodějka	k1gFnSc4	zlodějka
bot	bota	k1gFnPc2	bota
ztěžující	ztěžující	k2eAgInSc1d1	ztěžující
život	život	k1gInSc1	život
celé	celá	k1gFnSc2	celá
škole	škola	k1gFnSc3	škola
<g/>
.	.	kIx.	.
</s>
<s>
Natáčeno	natáčen	k2eAgNnSc1d1	natáčeno
v	v	k7c6	v
ZŠ	ZŠ	kA	ZŠ
Bítovská	Bítovská	k1gFnSc1	Bítovská
–	–	k?	–
Praha	Praha	k1gFnSc1	Praha
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Bota	bota	k1gFnSc1	bota
jménem	jméno	k1gNnSc7	jméno
Melichar	Melichar	k1gMnSc1	Melichar
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
<p>
<s>
Bota	bota	k1gFnSc1	bota
jménem	jméno	k1gNnSc7	jméno
Melichar	Melichar	k1gMnSc1	Melichar
na	na	k7c6	na
Kinoboxu	Kinobox	k1gInSc6	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Bota	bota	k1gFnSc1	bota
jménem	jméno	k1gNnSc7	jméno
Melichar	Melichar	k1gMnSc1	Melichar
na	na	k7c4	na
FDB	FDB	kA	FDB
</s>
</p>
