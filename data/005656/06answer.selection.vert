<s>
Woody	Woody	k6eAd1	Woody
Allen	allen	k1gInSc1	allen
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Allen	Allen	k1gMnSc1	Allen
Stewart	Stewart	k1gMnSc1	Stewart
Konigsberg	Konigsberg	k1gMnSc1	Konigsberg
(	(	kIx(	(
<g/>
*	*	kIx~	*
1	[number]	k4	1
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1935	[number]	k4	1935
New	New	k1gFnPc2	New
York	York	k1gInSc1	York
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
filmový	filmový	k2eAgMnSc1d1	filmový
režisér	režisér	k1gMnSc1	režisér
<g/>
,	,	kIx,	,
scenárista	scenárista	k1gMnSc1	scenárista
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
dramatik	dramatik	k1gMnSc1	dramatik
<g/>
,	,	kIx,	,
amatérský	amatérský	k2eAgMnSc1d1	amatérský
klarinetista	klarinetista	k1gMnSc1	klarinetista
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
komik	komik	k1gMnSc1	komik
<g/>
,	,	kIx,	,
čtyřnásobný	čtyřnásobný	k2eAgMnSc1d1	čtyřnásobný
držitel	držitel	k1gMnSc1	držitel
Oscara	Oscar	k1gMnSc2	Oscar
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
v	v	k7c6	v
uměleckém	umělecký	k2eAgInSc6d1	umělecký
světě	svět	k1gInSc6	svět
působí	působit	k5eAaImIp3nS	působit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
50	[number]	k4	50
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
