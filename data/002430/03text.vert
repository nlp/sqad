<s>
Mánie	mánie	k1gFnSc1	mánie
je	být	k5eAaImIp3nS	být
duševní	duševní	k2eAgFnSc1d1	duševní
porucha	porucha	k1gFnSc1	porucha
charakterizovaná	charakterizovaný	k2eAgFnSc1d1	charakterizovaná
expanzivní	expanzivní	k2eAgFnSc1d1	expanzivní
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
iritabilní	iritabilní	k2eAgFnSc7d1	iritabilní
náladou	nálada	k1gFnSc7	nálada
<g/>
.	.	kIx.	.
</s>
<s>
Diagnosticky	diagnosticky	k6eAd1	diagnosticky
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
jako	jako	k9	jako
manický	manický	k2eAgInSc1d1	manický
syndrom	syndrom	k1gInSc1	syndrom
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
bývá	bývat	k5eAaImIp3nS	bývat
spojena	spojit	k5eAaPmNgFnS	spojit
s	s	k7c7	s
depresí	deprese	k1gFnSc7	deprese
<g/>
;	;	kIx,	;
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
deprese	deprese	k1gFnSc2	deprese
vyskytne	vyskytnout	k5eAaPmIp3nS	vyskytnout
alespoň	alespoň	k9	alespoň
jedna	jeden	k4xCgFnSc1	jeden
manická	manický	k2eAgFnSc1d1	manická
epizoda	epizoda	k1gFnSc1	epizoda
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
diagnostikována	diagnostikovat	k5eAaBmNgFnS	diagnostikovat
bipolární	bipolární	k2eAgFnSc1d1	bipolární
afektivní	afektivní	k2eAgFnSc1d1	afektivní
porucha	porucha	k1gFnSc1	porucha
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
maniodepresivní	maniodepresivní	k2eAgFnSc1d1	maniodepresivní
psychóza	psychóza	k1gFnSc1	psychóza
<g/>
.	.	kIx.	.
</s>
<s>
Manický	manický	k2eAgInSc1d1	manický
syndrom	syndrom	k1gInSc1	syndrom
se	se	k3xPyFc4	se
však	však	k9	však
může	moct	k5eAaImIp3nS	moct
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
i	i	k9	i
u	u	k7c2	u
jiných	jiný	k2eAgNnPc2d1	jiné
onemocnění	onemocnění	k1gNnPc2	onemocnění
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
základě	základ	k1gInSc6	základ
nelze	lze	k6eNd1	lze
diagnostikovat	diagnostikovat	k5eAaBmF	diagnostikovat
bipolární	bipolární	k2eAgFnSc4d1	bipolární
afektivní	afektivní	k2eAgFnSc4d1	afektivní
poruchu	porucha	k1gFnSc4	porucha
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
u	u	k7c2	u
organických	organický	k2eAgNnPc2d1	organické
postižení	postižení	k1gNnPc2	postižení
mozku	mozek	k1gInSc2	mozek
-	-	kIx~	-
úrazy	úraz	k1gInPc1	úraz
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
nádory	nádor	k1gInPc4	nádor
mozku	mozek	k1gInSc2	mozek
<g/>
,	,	kIx,	,
intoxikace	intoxikace	k1gFnSc2	intoxikace
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
bývá	bývat	k5eAaImIp3nS	bývat
u	u	k7c2	u
progresivní	progresivní	k2eAgFnSc2d1	progresivní
paralýzy	paralýza	k1gFnSc2	paralýza
<g/>
,	,	kIx,	,
u	u	k7c2	u
schizofrenie	schizofrenie	k1gFnSc2	schizofrenie
a	a	k8xC	a
schizoafektivní	schizoafektivní	k2eAgFnSc2d1	schizoafektivní
poruchy	porucha	k1gFnSc2	porucha
<g/>
,	,	kIx,	,
u	u	k7c2	u
dekompenzací	dekompenzace	k1gFnPc2	dekompenzace
poruch	porucha	k1gFnPc2	porucha
osobnosti	osobnost	k1gFnSc2	osobnost
či	či	k8xC	či
u	u	k7c2	u
mentálně	mentálně	k6eAd1	mentálně
postižených	postižený	k2eAgMnPc2d1	postižený
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Projevy	projev	k1gInPc1	projev
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
opakem	opak	k1gInSc7	opak
deprese	deprese	k1gFnSc2	deprese
<g/>
.	.	kIx.	.
</s>
<s>
Nálada	nálada	k1gFnSc1	nálada
bývá	bývat	k5eAaImIp3nS	bývat
nadnesená	nadnesený	k2eAgFnSc1d1	nadnesená
<g/>
,	,	kIx,	,
expanzivní	expanzivní	k2eAgFnSc1d1	expanzivní
<g/>
.	.	kIx.	.
</s>
<s>
Myšlení	myšlení	k1gNnSc1	myšlení
a	a	k8xC	a
psychomotorika	psychomotorika	k1gFnSc1	psychomotorika
bývají	bývat	k5eAaImIp3nP	bývat
urychleny	urychlen	k2eAgInPc1d1	urychlen
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
impulsivním	impulsivní	k2eAgNnPc3d1	impulsivní
rozhodnutím	rozhodnutí	k1gNnPc3	rozhodnutí
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnPc1	jenž
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
závažné	závažný	k2eAgInPc4d1	závažný
následky	následek	k1gInPc4	následek
(	(	kIx(	(
<g/>
nerozvážné	rozvážný	k2eNgNnSc4d1	nerozvážné
utrácení	utrácení	k1gNnSc4	utrácení
<g/>
,	,	kIx,	,
ničení	ničení	k1gNnSc4	ničení
starých	starý	k2eAgInPc2d1	starý
a	a	k8xC	a
navazování	navazování	k1gNnSc3	navazování
nových	nový	k2eAgInPc2d1	nový
vztahů	vztah	k1gInPc2	vztah
<g/>
,	,	kIx,	,
výpověď	výpověď	k1gFnSc4	výpověď
v	v	k7c6	v
práci	práce	k1gFnSc6	práce
atp.	atp.	kA	atp.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tělesné	tělesný	k2eAgFnSc6d1	tělesná
oblasti	oblast	k1gFnSc6	oblast
dominuje	dominovat	k5eAaImIp3nS	dominovat
snížená	snížený	k2eAgFnSc1d1	snížená
potřeba	potřeba	k1gFnSc1	potřeba
spánku	spánek	k1gInSc2	spánek
<g/>
,	,	kIx,	,
zvýšená	zvýšený	k2eAgFnSc1d1	zvýšená
energie	energie	k1gFnSc1	energie
a	a	k8xC	a
zanedbávání	zanedbávání	k1gNnSc1	zanedbávání
somatických	somatický	k2eAgFnPc2d1	somatická
obtíží	obtíž	k1gFnPc2	obtíž
<g/>
.	.	kIx.	.
</s>
<s>
Vzhled	vzhled	k1gInSc1	vzhled
nemocného	nemocný	k1gMnSc2	nemocný
je	být	k5eAaImIp3nS	být
typický	typický	k2eAgInSc1d1	typický
-	-	kIx~	-
nemocný	mocný	k2eNgMnSc1d1	nemocný
je	být	k5eAaImIp3nS	být
usměvavý	usměvavý	k2eAgMnSc1d1	usměvavý
<g/>
,	,	kIx,	,
sebevědomý	sebevědomý	k2eAgMnSc1d1	sebevědomý
<g/>
,	,	kIx,	,
zvýšeně	zvýšeně	k6eAd1	zvýšeně
aktivní	aktivní	k2eAgFnSc1d1	aktivní
<g/>
,	,	kIx,	,
spontánně	spontánně	k6eAd1	spontánně
navazuje	navazovat	k5eAaImIp3nS	navazovat
rozhovor	rozhovor	k1gInSc4	rozhovor
<g/>
,	,	kIx,	,
rychle	rychle	k6eAd1	rychle
mluví	mluvit	k5eAaImIp3nS	mluvit
<g/>
,	,	kIx,	,
přeskakuje	přeskakovat	k5eAaImIp3nS	přeskakovat
z	z	k7c2	z
tématu	téma	k1gNnSc2	téma
na	na	k7c4	na
téma	téma	k1gNnSc4	téma
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
sdílný	sdílný	k2eAgInSc1d1	sdílný
<g/>
,	,	kIx,	,
jindy	jindy	k6eAd1	jindy
<g/>
,	,	kIx,	,
nesnášenlivý	snášenlivý	k2eNgMnSc1d1	nesnášenlivý
<g/>
,	,	kIx,	,
tvrdohlavý	tvrdohlavý	k2eAgMnSc1d1	tvrdohlavý
<g/>
.	.	kIx.	.
</s>
<s>
Nemocní	nemocný	k1gMnPc1	nemocný
nakupují	nakupovat	k5eAaBmIp3nP	nakupovat
nebo	nebo	k8xC	nebo
prodávají	prodávat	k5eAaImIp3nP	prodávat
různé	různý	k2eAgFnPc4d1	různá
věci	věc	k1gFnPc4	věc
<g/>
,	,	kIx,	,
podnikají	podnikat	k5eAaImIp3nP	podnikat
různé	různý	k2eAgFnPc4d1	různá
aktivity	aktivita	k1gFnPc4	aktivita
<g/>
,	,	kIx,	,
utrácejí	utrácet	k5eAaImIp3nP	utrácet
peníze	peníz	k1gInPc4	peníz
<g/>
,	,	kIx,	,
bývají	bývat	k5eAaImIp3nP	bývat
sexuálně	sexuálně	k6eAd1	sexuálně
podnikaví	podnikavý	k2eAgMnPc1d1	podnikavý
<g/>
.	.	kIx.	.
</s>
<s>
Symptomatika	Symptomatika	k1gFnSc1	Symptomatika
může	moct	k5eAaImIp3nS	moct
nabývat	nabývat	k5eAaImF	nabývat
různé	různý	k2eAgFnPc4d1	různá
intenzity	intenzita	k1gFnPc4	intenzita
<g/>
:	:	kIx,	:
Hypomanie	Hypomanie	k1gFnSc1	Hypomanie
je	být	k5eAaImIp3nS	být
mírnou	mírný	k2eAgFnSc7d1	mírná
formou	forma	k1gFnSc7	forma
<g/>
;	;	kIx,	;
tato	tento	k3xDgFnSc1	tento
nijak	nijak	k6eAd1	nijak
výrazně	výrazně	k6eAd1	výrazně
nenarušuje	narušovat	k5eNaImIp3nS	narušovat
psychosociální	psychosociální	k2eAgNnSc4d1	psychosociální
fungování	fungování	k1gNnSc4	fungování
jedince	jedinec	k1gMnSc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
charakterizována	charakterizovat	k5eAaBmNgFnS	charakterizovat
mírně	mírně	k6eAd1	mírně
nadnesenou	nadnesený	k2eAgFnSc7d1	nadnesená
náladou	nálada	k1gFnSc7	nálada
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
trvá	trvat	k5eAaImIp3nS	trvat
minimálně	minimálně	k6eAd1	minimálně
čtyři	čtyři	k4xCgInPc4	čtyři
po	po	k7c6	po
sobě	se	k3xPyFc3	se
jdoucí	jdoucí	k2eAgInPc4d1	jdoucí
dny	den	k1gInPc4	den
<g/>
,	,	kIx,	,
zvýšenou	zvýšený	k2eAgFnSc7d1	zvýšená
energií	energie	k1gFnSc7	energie
<g/>
,	,	kIx,	,
aktivitou	aktivita	k1gFnSc7	aktivita
a	a	k8xC	a
pocitem	pocit	k1gInSc7	pocit
zvýšené	zvýšený	k2eAgFnSc2d1	zvýšená
výkonnosti	výkonnost	k1gFnSc2	výkonnost
<g/>
.	.	kIx.	.
</s>
<s>
Mánie	mánie	k1gFnSc1	mánie
-	-	kIx~	-
zvýšená	zvýšený	k2eAgFnSc1d1	zvýšená
nálada	nálada	k1gFnSc1	nálada
až	až	k8xS	až
vzrušení	vzrušení	k1gNnSc1	vzrušení
trvá	trvat	k5eAaImIp3nS	trvat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
týden	týden	k1gInSc4	týden
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
myšlenkovému	myšlenkový	k2eAgInSc3d1	myšlenkový
trysku	trysk	k1gInSc3	trysk
<g/>
,	,	kIx,	,
zvýšenému	zvýšený	k2eAgNnSc3d1	zvýšené
sebevědomí	sebevědomí	k1gNnSc3	sebevědomí
a	a	k8xC	a
ztrátě	ztráta	k1gFnSc3	ztráta
sociálních	sociální	k2eAgFnPc2d1	sociální
zábran	zábrana	k1gFnPc2	zábrana
a	a	k8xC	a
riskantnímu	riskantní	k2eAgNnSc3d1	riskantní
jednání	jednání	k1gNnSc3	jednání
<g/>
.	.	kIx.	.
</s>
<s>
Mánie	mánie	k1gFnSc1	mánie
s	s	k7c7	s
psychotickými	psychotický	k2eAgInPc7d1	psychotický
příznaky	příznak	k1gInPc7	příznak
často	často	k6eAd1	často
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
hospitalizaci	hospitalizace	k1gFnSc3	hospitalizace
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
přítomny	přítomen	k2eAgInPc1d1	přítomen
bludy	blud	k1gInPc1	blud
a	a	k8xC	a
halucinace	halucinace	k1gFnPc1	halucinace
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
forma	forma	k1gFnSc1	forma
postihuje	postihovat	k5eAaImIp3nS	postihovat
zhruba	zhruba	k6eAd1	zhruba
třetinu	třetina	k1gFnSc4	třetina
nemocných	nemocný	k1gMnPc2	nemocný
<g/>
.	.	kIx.	.
</s>
<s>
Bludy	blud	k1gInPc1	blud
bývají	bývat	k5eAaImIp3nP	bývat
expanzivní	expanzivní	k2eAgInPc1d1	expanzivní
<g/>
,	,	kIx,	,
náboženské	náboženský	k2eAgInPc1d1	náboženský
<g/>
,	,	kIx,	,
erotomanické	erotomanický	k2eAgInPc1d1	erotomanický
apod.	apod.	kA	apod.
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
jsem	být	k5eAaImIp1nS	být
Ježíš	Ježíš	k1gMnSc1	Ježíš
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
miluje	milovat	k5eAaImIp3nS	milovat
mě	já	k3xPp1nSc4	já
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Manický	manický	k2eAgInSc1d1	manický
syndrom	syndrom	k1gInSc1	syndrom
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
na	na	k7c4	na
tři	tři	k4xCgInPc4	tři
typy	typ	k1gInPc4	typ
<g/>
:	:	kIx,	:
euforický	euforický	k2eAgInSc4d1	euforický
syndrom	syndrom	k1gInSc4	syndrom
-	-	kIx~	-
pocit	pocit	k1gInSc4	pocit
tupé	tupý	k2eAgFnSc2d1	tupá
blaženosti	blaženost	k1gFnSc2	blaženost
<g/>
,	,	kIx,	,
spokojenosti	spokojenost	k1gFnSc2	spokojenost
bez	bez	k7c2	bez
výraznější	výrazný	k2eAgFnSc2d2	výraznější
hyperaktivity	hyperaktivita	k1gFnSc2	hyperaktivita
<g/>
;	;	kIx,	;
stuporózní	stuporózní	k2eAgInSc1d1	stuporózní
manický	manický	k2eAgInSc1d1	manický
syndrom	syndrom	k1gInSc1	syndrom
-	-	kIx~	-
naprostá	naprostý	k2eAgFnSc1d1	naprostá
nepřítomnost	nepřítomnost	k1gFnSc1	nepřítomnost
psychomotorických	psychomotorický	k2eAgInPc2d1	psychomotorický
projevů	projev	k1gInPc2	projev
<g/>
;	;	kIx,	;
rezonantní	rezonantní	k2eAgInSc1d1	rezonantní
manický	manický	k2eAgInSc1d1	manický
syndrom	syndrom	k1gInSc1	syndrom
-	-	kIx~	-
podrážděnost	podrážděnost	k1gFnSc1	podrážděnost
<g/>
,	,	kIx,	,
nevlídnost	nevlídnost	k1gFnSc1	nevlídnost
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
i	i	k9	i
agrese	agrese	k1gFnSc1	agrese
<g/>
.	.	kIx.	.
</s>
