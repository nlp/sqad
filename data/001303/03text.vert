<s>
Uhlík	uhlík	k1gInSc1	uhlík
(	(	kIx(	(
<g/>
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
C	C	kA	C
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Carboneum	Carboneum	k1gInSc1	Carboneum
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
chemický	chemický	k2eAgInSc1d1	chemický
prvek	prvek	k1gInSc1	prvek
<g/>
,	,	kIx,	,
tvořící	tvořící	k2eAgInSc1d1	tvořící
základní	základní	k2eAgInSc1d1	základní
stavební	stavební	k2eAgInSc1d1	stavební
kámen	kámen	k1gInSc1	kámen
všech	všecek	k3xTgFnPc2	všecek
organických	organický	k2eAgFnPc2d1	organická
sloučenin	sloučenina	k1gFnPc2	sloučenina
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
všech	všecek	k3xTgMnPc2	všecek
živých	živý	k2eAgMnPc2d1	živý
organismů	organismus	k1gInPc2	organismus
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
planetě	planeta	k1gFnSc6	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Sloučeniny	sloučenina	k1gFnPc1	sloučenina
uhlíku	uhlík	k1gInSc2	uhlík
jsou	být	k5eAaImIp3nP	být
jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
základů	základ	k1gInPc2	základ
světové	světový	k2eAgFnSc2d1	světová
energetiky	energetika	k1gFnSc2	energetika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
především	především	k6eAd1	především
fosilní	fosilní	k2eAgNnPc4d1	fosilní
paliva	palivo	k1gNnPc4	palivo
jako	jako	k8xS	jako
zemní	zemní	k2eAgInSc4d1	zemní
plyn	plyn	k1gInSc4	plyn
a	a	k8xC	a
uhlí	uhlí	k1gNnSc4	uhlí
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
energetický	energetický	k2eAgInSc1d1	energetický
zdroj	zdroj	k1gInSc1	zdroj
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
elektřiny	elektřina	k1gFnSc2	elektřina
a	a	k8xC	a
vytápění	vytápění	k1gNnSc2	vytápění
<g/>
,	,	kIx,	,
produkty	produkt	k1gInPc1	produkt
zpracování	zpracování	k1gNnSc2	zpracování
ropy	ropa	k1gFnSc2	ropa
jsou	být	k5eAaImIp3nP	být
nezbytné	nezbytný	k2eAgInPc1d1	nezbytný
pro	pro	k7c4	pro
pohon	pohon	k1gInSc4	pohon
spalovacích	spalovací	k2eAgInPc2d1	spalovací
motorů	motor	k1gInPc2	motor
a	a	k8xC	a
tak	tak	k6eAd1	tak
silniční	silniční	k2eAgFnSc4d1	silniční
a	a	k8xC	a
železniční	železniční	k2eAgFnSc4d1	železniční
dopravu	doprava	k1gFnSc4	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Výrobky	výrobek	k1gInPc1	výrobek
chemického	chemický	k2eAgInSc2d1	chemický
průmyslu	průmysl	k1gInSc2	průmysl
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
uhlíku	uhlík	k1gInSc2	uhlík
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
našeho	náš	k3xOp1gInSc2	náš
každodenního	každodenní	k2eAgInSc2d1	každodenní
života	život	k1gInSc2	život
ať	ať	k8xC	ať
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
plastické	plastický	k2eAgFnPc4d1	plastická
hmoty	hmota	k1gFnPc4	hmota
<g/>
,	,	kIx,	,
umělá	umělý	k2eAgNnPc1d1	umělé
vlákna	vlákno	k1gNnPc1	vlákno
<g/>
,	,	kIx,	,
nátěrové	nátěrový	k2eAgFnPc1d1	nátěrová
hmoty	hmota	k1gFnPc1	hmota
<g/>
,	,	kIx,	,
léčiva	léčivo	k1gNnPc1	léčivo
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgInPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
bylo	být	k5eAaImAgNnS	být
popsáno	popsat	k5eAaPmNgNnS	popsat
přibližně	přibližně	k6eAd1	přibližně
10	[number]	k4	10
milionů	milion	k4xCgInPc2	milion
organických	organický	k2eAgFnPc2d1	organická
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Uhlík	uhlík	k1gInSc1	uhlík
je	být	k5eAaImIp3nS	být
typický	typický	k2eAgInSc1d1	typický
nekovový	kovový	k2eNgInSc1d1	nekovový
chemický	chemický	k2eAgInSc1d1	chemický
prvek	prvek	k1gInSc1	prvek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
elementárním	elementární	k2eAgInSc6d1	elementární
stavu	stav	k1gInSc6	stav
jako	jako	k8xC	jako
minerál	minerál	k1gInSc1	minerál
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
základních	základní	k2eAgFnPc6d1	základní
alotropních	alotropní	k2eAgFnPc6d1	alotropní
modifikacích	modifikace	k1gFnPc6	modifikace
a	a	k8xC	a
v	v	k7c4	v
posledních	poslední	k2eAgNnPc2d1	poslední
přibližně	přibližně	k6eAd1	přibližně
30	[number]	k4	30
letech	léto	k1gNnPc6	léto
byly	být	k5eAaImAgInP	být
objeveny	objevit	k5eAaPmNgInP	objevit
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
nebo	nebo	k8xC	nebo
laboratorně	laboratorně	k6eAd1	laboratorně
vytvořeny	vytvořen	k2eAgFnPc4d1	vytvořena
modifikace	modifikace	k1gFnPc4	modifikace
další	další	k2eAgFnPc4d1	další
<g/>
:	:	kIx,	:
Grafit	grafit	k1gInSc4	grafit
(	(	kIx(	(
<g/>
tuha	tuha	k1gFnSc1	tuha
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejčastější	častý	k2eAgFnPc4d3	nejčastější
přírodní	přírodní	k2eAgFnPc4d1	přírodní
modifikace	modifikace	k1gFnPc4	modifikace
uhlíku	uhlík	k1gInSc2	uhlík
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
struktura	struktura	k1gFnSc1	struktura
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
vrstev	vrstva	k1gFnPc2	vrstva
tzv.	tzv.	kA	tzv.
grafenu	grafen	k1gInSc2	grafen
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořit	k5eAaImNgInP	tvořit
uhlíky	uhlík	k1gInPc7	uhlík
navázanými	navázaný	k2eAgInPc7d1	navázaný
do	do	k7c2	do
šestiúhelníků	šestiúhelník	k1gInPc2	šestiúhelník
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
každý	každý	k3xTgInSc4	každý
uhlík	uhlík	k1gInSc4	uhlík
jsou	být	k5eAaImIp3nP	být
kovalentně	kovalentně	k6eAd1	kovalentně
vázány	vázán	k2eAgInPc4d1	vázán
další	další	k2eAgInPc4d1	další
tři	tři	k4xCgInPc4	tři
uhlíky	uhlík	k1gInPc4	uhlík
(	(	kIx(	(
<g/>
hybridizace	hybridizace	k1gFnSc1	hybridizace
sp	sp	k?	sp
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
rozsáhlý	rozsáhlý	k2eAgInSc1d1	rozsáhlý
systém	systém	k1gInSc1	systém
delokalizovaných	delokalizovaný	k2eAgInPc2d1	delokalizovaný
elektronů	elektron	k1gInPc2	elektron
(	(	kIx(	(
<g/>
π	π	k?	π
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
vrstvy	vrstva	k1gFnPc1	vrstva
spolu	spolu	k6eAd1	spolu
drží	držet	k5eAaImIp3nP	držet
pouze	pouze	k6eAd1	pouze
pomocí	pomocí	k7c2	pomocí
slabých	slabý	k2eAgFnPc2d1	slabá
interakcí	interakce	k1gFnPc2	interakce
tzv.	tzv.	kA	tzv.
van	van	k1gInSc1	van
der	drát	k5eAaImRp2nS	drát
Waalsovy	Waalsův	k2eAgFnSc2d1	Waalsova
síly	síla	k1gFnPc1	síla
<g/>
.	.	kIx.	.
</s>
<s>
Této	tento	k3xDgFnSc3	tento
vlastnosti	vlastnost	k1gFnSc3	vlastnost
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
např.	např.	kA	např.
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
tužek	tužka	k1gFnPc2	tužka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mletá	mletý	k2eAgFnSc1d1	mletá
tuha	tuha	k1gFnSc1	tuha
tvoří	tvořit	k5eAaImIp3nS	tvořit
základní	základní	k2eAgFnSc4d1	základní
složku	složka	k1gFnSc4	složka
tyčinky	tyčinka	k1gFnSc2	tyčinka
určené	určený	k2eAgFnSc2d1	určená
pro	pro	k7c4	pro
psaní	psaní	k1gNnSc4	psaní
a	a	k8xC	a
kreslení	kreslení	k1gNnSc4	kreslení
<g/>
.	.	kIx.	.
</s>
<s>
Grafit	grafit	k1gInSc1	grafit
vede	vést	k5eAaImIp3nS	vést
elektrický	elektrický	k2eAgInSc4d1	elektrický
proud	proud	k1gInSc4	proud
<g/>
.	.	kIx.	.
</s>
<s>
Diamant	diamant	k1gInSc1	diamant
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
uhlíkem	uhlík	k1gInSc7	uhlík
krystalizujícím	krystalizující	k2eAgInSc7d1	krystalizující
v	v	k7c6	v
soustavě	soustava	k1gFnSc6	soustava
krychlové	krychlový	k2eAgNnSc1d1	krychlové
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
nejtvrdším	tvrdý	k2eAgMnSc7d3	nejtvrdší
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
cenným	cenný	k2eAgInSc7d1	cenný
přírodním	přírodní	k2eAgInSc7d1	přírodní
nerostem	nerost	k1gInSc7	nerost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
každý	každý	k3xTgInSc4	každý
uhlík	uhlík	k1gInSc4	uhlík
jsou	být	k5eAaImIp3nP	být
kovalentně	kovalentně	k6eAd1	kovalentně
vázány	vázán	k2eAgInPc4d1	vázán
další	další	k2eAgInPc4d1	další
čtyři	čtyři	k4xCgInPc4	čtyři
uhlíky	uhlík	k1gInPc4	uhlík
(	(	kIx(	(
<g/>
hybridizace	hybridizace	k1gFnSc1	hybridizace
sp	sp	k?	sp
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
diamantů	diamant	k1gInPc2	diamant
se	se	k3xPyFc4	se
udává	udávat	k5eAaImIp3nS	udávat
v	v	k7c6	v
karátech	karát	k1gInPc6	karát
<g/>
,	,	kIx,	,
největším	veliký	k2eAgMnSc7d3	veliký
doposud	doposud	k6eAd1	doposud
nalezeným	nalezený	k2eAgInSc7d1	nalezený
diamantem	diamant	k1gInSc7	diamant
byl	být	k5eAaImAgInS	být
Cullinan	Cullinan	k1gInSc1	Cullinan
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
v	v	k7c6	v
surovém	surový	k2eAgInSc6d1	surový
stavu	stav	k1gInSc6	stav
při	při	k7c6	při
nálezu	nález	k1gInSc6	nález
v	v	k7c4	v
JAR	jar	k1gFnSc4	jar
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
váhy	váha	k1gFnSc2	váha
3	[number]	k4	3
106	[number]	k4	106
karátů	karát	k1gInPc2	karát
(	(	kIx(	(
<g/>
621,2	[number]	k4	621,2
gramů	gram	k1gInPc2	gram
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Diamanty	diamant	k1gInPc1	diamant
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
tvrdost	tvrdost	k1gFnSc4	tvrdost
a	a	k8xC	a
výbornou	výborný	k2eAgFnSc4d1	výborná
tepelnou	tepelný	k2eAgFnSc4d1	tepelná
vodivost	vodivost	k1gFnSc4	vodivost
(	(	kIx(	(
<g/>
až	až	k9	až
2300	[number]	k4	2300
W	W	kA	W
<g/>
·	·	k?	·
<g/>
m	m	kA	m
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
·	·	k?	·
<g/>
K	K	kA	K
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
při	při	k7c6	při
pokojové	pokojový	k2eAgFnSc6d1	pokojová
teplotě	teplota	k1gFnSc6	teplota
a	a	k8xC	a
normálním	normální	k2eAgNnSc6d1	normální
izotopickém	izotopický	k2eAgNnSc6d1	izotopické
složení	složení	k1gNnSc6	složení
<g/>
)	)	kIx)	)
v	v	k7c6	v
nejrůznějších	různý	k2eAgInPc6d3	nejrůznější
řezných	řezný	k2eAgInPc6d1	řezný
a	a	k8xC	a
vrtných	vrtný	k2eAgInPc6d1	vrtný
nástrojích	nástroj	k1gInPc6	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
vysokou	vysoký	k2eAgFnSc4d1	vysoká
cenu	cena	k1gFnSc4	cena
bývají	bývat	k5eAaImIp3nP	bývat
diamanty	diamant	k1gInPc1	diamant
vyráběny	vyrábět	k5eAaImNgInP	vyrábět
synteticky	synteticky	k6eAd1	synteticky
<g/>
.	.	kIx.	.
</s>
<s>
Lonsdaleit	Lonsdaleit	k1gInSc1	Lonsdaleit
<g/>
,	,	kIx,	,
též	též	k9	též
zvaný	zvaný	k2eAgInSc1d1	zvaný
"	"	kIx"	"
<g/>
šesterečný	šesterečný	k2eAgInSc1d1	šesterečný
diamant	diamant	k1gInSc1	diamant
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
řídce	řídce	k6eAd1	řídce
se	se	k3xPyFc4	se
vyskytující	vyskytující	k2eAgFnSc7d1	vyskytující
přírodní	přírodní	k2eAgFnSc7d1	přírodní
alotropní	alotropní	k2eAgFnSc7d1	alotropní
modifikací	modifikace	k1gFnSc7	modifikace
uhlíku	uhlík	k1gInSc2	uhlík
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
krystalová	krystalový	k2eAgFnSc1d1	krystalová
struktura	struktura	k1gFnSc1	struktura
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
u	u	k7c2	u
diamantu	diamant	k1gInSc2	diamant
atomy	atom	k1gInPc1	atom
uhlíku	uhlík	k1gInSc2	uhlík
vázanými	vázaný	k2eAgFnPc7d1	vázaná
jednoduchými	jednoduchý	k2eAgFnPc7d1	jednoduchá
kovalentními	kovalentní	k2eAgFnPc7d1	kovalentní
vazbami	vazba	k1gFnPc7	vazba
se	s	k7c7	s
čtyřmi	čtyři	k4xCgMnPc7	čtyři
sousedy	soused	k1gMnPc7	soused
<g/>
,	,	kIx,	,
krystalová	krystalový	k2eAgFnSc1d1	krystalová
soustava	soustava	k1gFnSc1	soustava
je	být	k5eAaImIp3nS	být
však	však	k9	však
šesterečná	šesterečný	k2eAgFnSc1d1	šesterečná
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
přírodního	přírodní	k2eAgInSc2d1	přírodní
lonsdaleitu	lonsdaleit	k1gInSc2	lonsdaleit
je	být	k5eAaImIp3nS	být
vysvětlován	vysvětlovat	k5eAaImNgInS	vysvětlovat
přeměnou	přeměna	k1gFnSc7	přeměna
grafitu	grafit	k1gInSc2	grafit
při	při	k7c6	při
dopadech	dopad	k1gInPc6	dopad
meteoritů	meteorit	k1gInPc2	meteorit
<g/>
.	.	kIx.	.
</s>
<s>
Chaoit	Chaoit	k1gInSc1	Chaoit
<g/>
,	,	kIx,	,
též	též	k9	též
zvaný	zvaný	k2eAgInSc1d1	zvaný
"	"	kIx"	"
<g/>
bílý	bílý	k2eAgInSc1d1	bílý
uhlík	uhlík	k1gInSc1	uhlík
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
řídce	řídce	k6eAd1	řídce
se	se	k3xPyFc4	se
vyskytující	vyskytující	k2eAgFnSc1d1	vyskytující
přírodní	přírodní	k2eAgFnSc1d1	přírodní
alotropní	alotropní	k2eAgFnSc1d1	alotropní
modifikace	modifikace	k1gFnSc1	modifikace
uhlíku	uhlík	k1gInSc2	uhlík
<g/>
,	,	kIx,	,
objevená	objevený	k2eAgFnSc1d1	objevená
r.	r.	kA	r.
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
nerost	nerost	k1gInSc1	nerost
krystalizuje	krystalizovat	k5eAaImIp3nS	krystalizovat
v	v	k7c6	v
šesterečné	šesterečný	k2eAgFnSc6d1	šesterečná
soustavě	soustava	k1gFnSc6	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
je	být	k5eAaImIp3nS	být
jako	jako	k9	jako
u	u	k7c2	u
lonsdaleitu	lonsdaleit	k1gInSc2	lonsdaleit
vysvětlován	vysvětlovat	k5eAaImNgMnS	vysvětlovat
přeměnou	přeměna	k1gFnSc7	přeměna
grafitu	grafit	k1gInSc2	grafit
při	při	k7c6	při
dopadech	dopad	k1gInPc6	dopad
meteoritů	meteorit	k1gInPc2	meteorit
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejichž	jejichž	k3xOyRp3gInPc6	jejichž
dopadových	dopadový	k2eAgInPc6d1	dopadový
kráterech	kráter	k1gInPc6	kráter
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
<g/>
.	.	kIx.	.
</s>
<s>
Grafen	Grafen	k1gInSc1	Grafen
je	být	k5eAaImIp3nS	být
forma	forma	k1gFnSc1	forma
uhlíku	uhlík	k1gInSc2	uhlík
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc7	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
jedna	jeden	k4xCgFnSc1	jeden
či	či	k8xC	či
několik	několik	k4yIc1	několik
málo	málo	k4c1	málo
vrstev	vrstva	k1gFnPc2	vrstva
rovinné	rovinný	k2eAgFnSc2d1	rovinná
sítě	síť	k1gFnSc2	síť
vzájemně	vzájemně	k6eAd1	vzájemně
propojených	propojený	k2eAgInPc2d1	propojený
atomů	atom	k1gInPc2	atom
uhlíku	uhlík	k1gInSc2	uhlík
uspořádaných	uspořádaný	k2eAgFnPc2d1	uspořádaná
do	do	k7c2	do
tvaru	tvar	k1gInSc2	tvar
šestiúhelníků	šestiúhelník	k1gMnPc2	šestiúhelník
(	(	kIx(	(
<g/>
hybridizace	hybridizace	k1gFnSc1	hybridizace
sp	sp	k?	sp
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
vlastně	vlastně	k9	vlastně
strukturní	strukturní	k2eAgFnSc4d1	strukturní
součást	součást	k1gFnSc4	součást
grafitu	grafit	k1gInSc2	grafit
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
si	se	k3xPyFc3	se
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
zvláštním	zvláštní	k2eAgFnPc3d1	zvláštní
fyzikálním	fyzikální	k2eAgFnPc3d1	fyzikální
vlastnostem	vlastnost	k1gFnPc3	vlastnost
<g/>
,	,	kIx,	,
výborné	výborná	k1gFnSc3	výborná
tepelné	tepelný	k2eAgFnSc2d1	tepelná
vodivosti	vodivost	k1gFnSc2	vodivost
(	(	kIx(	(
<g/>
přes	přes	k7c4	přes
4000	[number]	k4	4000
W	W	kA	W
<g/>
·	·	k?	·
<g/>
m	m	kA	m
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
·	·	k?	·
<g/>
K	K	kA	K
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
u	u	k7c2	u
izotopicky	izotopicky	k6eAd1	izotopicky
čištěného	čištěný	k2eAgInSc2d1	čištěný
grafenu	grafen	k1gInSc2	grafen
<g/>
)	)	kIx)	)
a	a	k8xC	a
využitelností	využitelnost	k1gFnSc7	využitelnost
pro	pro	k7c4	pro
mnohé	mnohé	k1gNnSc4	mnohé
elektronické	elektronický	k2eAgFnSc2d1	elektronická
a	a	k8xC	a
optické	optický	k2eAgFnSc2d1	optická
aplikace	aplikace	k1gFnSc2	aplikace
zasloužila	zasloužit	k5eAaPmAgFnS	zasloužit
vlastní	vlastní	k2eAgInSc4d1	vlastní
název	název	k1gInSc4	název
i	i	k8xC	i
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
v	v	k7c6	v
r.	r.	kA	r.
2010	[number]	k4	2010
pro	pro	k7c4	pro
své	svůj	k3xOyFgMnPc4	svůj
objevitele	objevitel	k1gMnPc4	objevitel
<g/>
.	.	kIx.	.
</s>
<s>
Grafyn	Grafyn	k1gInSc1	Grafyn
je	být	k5eAaImIp3nS	být
společné	společný	k2eAgNnSc4d1	společné
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
formy	forma	k1gFnPc4	forma
uhlíku	uhlík	k1gInSc2	uhlík
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
jako	jako	k9	jako
u	u	k7c2	u
grafenu	grafen	k1gInSc2	grafen
jedna	jeden	k4xCgFnSc1	jeden
či	či	k8xC	či
několik	několik	k4yIc1	několik
málo	málo	k4c1	málo
vrstev	vrstva	k1gFnPc2	vrstva
rovinné	rovinný	k2eAgFnSc2d1	rovinná
sítě	síť	k1gFnSc2	síť
vzájemně	vzájemně	k6eAd1	vzájemně
propojených	propojený	k2eAgInPc2d1	propojený
atomů	atom	k1gInPc2	atom
uhlíku	uhlík	k1gInSc2	uhlík
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
však	však	k9	však
již	již	k9	již
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
vloženým	vložený	k2eAgInPc3d1	vložený
lineárním	lineární	k2eAgInPc3d1	lineární
úsekům	úsek	k1gInPc3	úsek
-	-	kIx~	-
<g/>
C	C	kA	C
<g/>
≡	≡	k?	≡
<g/>
C	C	kA	C
<g/>
-	-	kIx~	-
(	(	kIx(	(
<g/>
hybridizace	hybridizace	k1gFnSc1	hybridizace
sp	sp	k?	sp
<g/>
)	)	kIx)	)
nejsou	být	k5eNaImIp3nP	být
uspořádány	uspořádat	k5eAaPmNgInP	uspořádat
do	do	k7c2	do
tvaru	tvar	k1gInSc2	tvar
šestiúhelníků	šestiúhelník	k1gInPc2	šestiúhelník
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
si	se	k3xPyFc3	se
zachovávají	zachovávat	k5eAaImIp3nP	zachovávat
mnohé	mnohý	k2eAgFnPc4d1	mnohá
zvláštní	zvláštní	k2eAgFnPc4d1	zvláštní
fyzikální	fyzikální	k2eAgFnPc4d1	fyzikální
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
takto	takto	k6eAd1	takto
vytvořeny	vytvořit	k5eAaPmNgFnP	vytvořit
grafyny	grafyen	k2eAgFnPc1d1	grafyen
různé	různý	k2eAgFnPc1d1	různá
symetrie	symetrie	k1gFnPc1	symetrie
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
pravoúhelníkové	pravoúhelníkový	k2eAgFnSc2d1	pravoúhelníkový
<g/>
.	.	kIx.	.
</s>
<s>
Fullereny	fulleren	k1gInPc1	fulleren
označují	označovat	k5eAaImIp3nP	označovat
sférické	sférický	k2eAgMnPc4d1	sférický
(	(	kIx(	(
<g/>
též	též	k9	též
elipsoidální	elipsoidální	k2eAgFnSc2d1	elipsoidální
či	či	k8xC	či
podobného	podobný	k2eAgInSc2d1	podobný
tvaru	tvar	k1gInSc2	tvar
<g/>
)	)	kIx)	)
molekuly	molekula	k1gFnPc1	molekula
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
prostorově	prostorově	k6eAd1	prostorově
uzavřené	uzavřený	k2eAgFnPc4d1	uzavřená
vrstvy	vrstva	k1gFnPc4	vrstva
grafenu	grafen	k1gInSc2	grafen
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
sítě	síť	k1gFnPc1	síť
uhlíkových	uhlíkový	k2eAgInPc2d1	uhlíkový
atomů	atom	k1gInPc2	atom
uspořádaných	uspořádaný	k2eAgInPc2d1	uspořádaný
do	do	k7c2	do
šestiúhelníků	šestiúhelník	k1gInPc2	šestiúhelník
<g/>
,	,	kIx,	,
doplněných	doplněná	k1gFnPc2	doplněná
kvůli	kvůli	k7c3	kvůli
prostorovému	prostorový	k2eAgNnSc3d1	prostorové
uzavření	uzavření	k1gNnSc3	uzavření
dvanácti	dvanáct	k4xCc2	dvanáct
pětiúhelníky	pětiúhelník	k1gInPc7	pětiúhelník
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
molekuly	molekula	k1gFnPc1	molekula
jsou	být	k5eAaImIp3nP	být
mimořádně	mimořádně	k6eAd1	mimořádně
odolné	odolný	k2eAgFnSc2d1	odolná
vůči	vůči	k7c3	vůči
vnějším	vnější	k2eAgInPc3d1	vnější
fyzikálním	fyzikální	k2eAgInPc3d1	fyzikální
vlivům	vliv	k1gInPc3	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
nejstabilnější	stabilní	k2eAgInSc1d3	nejstabilnější
známý	známý	k2eAgInSc1d1	známý
fulleren	fulleren	k1gInSc1	fulleren
je	být	k5eAaImIp3nS	být
molekula	molekula	k1gFnSc1	molekula
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgFnSc1d1	obsahující
60	[number]	k4	60
uhlíkových	uhlíkový	k2eAgInPc2d1	uhlíkový
atomů	atom	k1gInPc2	atom
<g/>
.	.	kIx.	.
</s>
<s>
Fullereny	fulleren	k1gInPc1	fulleren
se	se	k3xPyFc4	se
uměle	uměle	k6eAd1	uměle
připravují	připravovat	k5eAaImIp3nP	připravovat
pyrolýzou	pyrolýza	k1gFnSc7	pyrolýza
organických	organický	k2eAgFnPc2d1	organická
sloučenin	sloučenina	k1gFnPc2	sloučenina
laserem	laser	k1gInSc7	laser
<g/>
.	.	kIx.	.
</s>
<s>
Výskyt	výskyt	k1gInSc1	výskyt
přírodních	přírodní	k2eAgInPc2d1	přírodní
fullerenů	fulleren	k1gInPc2	fulleren
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
byl	být	k5eAaImAgInS	být
prokázán	prokázat	k5eAaPmNgInS	prokázat
v	v	k7c6	v
r.	r.	kA	r.
2010	[number]	k4	2010
Spitzerovým	Spitzerův	k2eAgInSc7d1	Spitzerův
teleskopem	teleskop	k1gInSc7	teleskop
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
objev	objev	k1gInSc4	objev
a	a	k8xC	a
studium	studium	k1gNnSc4	studium
vlastností	vlastnost	k1gFnPc2	vlastnost
fullerenů	fulleren	k1gInPc2	fulleren
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
udělena	udělen	k2eAgFnSc1d1	udělena
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
Robertu	Robert	k1gMnSc3	Robert
F.	F.	kA	F.
Curlovi	Curl	k1gMnSc3	Curl
a	a	k8xC	a
Richardu	Richarda	k1gFnSc4	Richarda
E.	E.	kA	E.
Smalleymu	Smalleym	k1gInSc2	Smalleym
a	a	k8xC	a
Haroldu	Harold	k1gInSc2	Harold
W.	W.	kA	W.
Krotoovi	Krotoa	k1gMnSc6	Krotoa
<g/>
.	.	kIx.	.
</s>
<s>
Fullerit	Fullerit	k1gInSc1	Fullerit
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
krystalová	krystalový	k2eAgFnSc1d1	krystalová
struktura	struktura	k1gFnSc1	struktura
tvořená	tvořený	k2eAgFnSc1d1	tvořená
fullereny	fulleren	k1gInPc1	fulleren
C	C	kA	C
<g/>
60	[number]	k4	60
<g/>
.	.	kIx.	.
</s>
<s>
Fullerit	Fullerit	k1gInSc1	Fullerit
krystalizuje	krystalizovat	k5eAaImIp3nS	krystalizovat
v	v	k7c6	v
krychlové	krychlový	k2eAgFnSc6d1	krychlová
soustavě	soustava	k1gFnSc6	soustava
(	(	kIx(	(
<g/>
nad	nad	k7c7	nad
teplotou	teplota	k1gFnSc7	teplota
~	~	kIx~	~
<g/>
250	[number]	k4	250
K	k	k7c3	k
v	v	k7c6	v
plošně	plošně	k6eAd1	plošně
centrované	centrovaný	k2eAgFnSc6d1	centrovaná
mřížce	mřížka	k1gFnSc6	mřížka
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
teplotou	teplota	k1gFnSc7	teplota
~	~	kIx~	~
<g/>
220	[number]	k4	220
K	K	kA	K
v	v	k7c6	v
prosté	prostý	k2eAgFnSc6d1	prostá
mřížce	mřížka	k1gFnSc6	mřížka
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
i	i	k9	i
soustava	soustava	k1gFnSc1	soustava
čtverečná	čtverečný	k2eAgFnSc1d1	čtverečná
<g/>
.	.	kIx.	.
</s>
<s>
Výskyt	výskyt	k1gInSc1	výskyt
přírodního	přírodní	k2eAgInSc2d1	přírodní
fulleritu	fullerit	k1gInSc2	fullerit
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
byl	být	k5eAaImAgInS	být
prokázán	prokázat	k5eAaPmNgInS	prokázat
v	v	k7c6	v
r.	r.	kA	r.
2012	[number]	k4	2012
Spitzerovým	Spitzerův	k2eAgInSc7d1	Spitzerův
teleskopem	teleskop	k1gInSc7	teleskop
<g/>
.	.	kIx.	.
</s>
<s>
Doposud	doposud	k6eAd1	doposud
nebyl	být	k5eNaImAgInS	být
uznán	uznat	k5eAaPmNgInS	uznat
Mezinárodní	mezinárodní	k2eAgFnSc7d1	mezinárodní
mineralogickou	mineralogický	k2eAgFnSc7d1	mineralogická
asociací	asociace	k1gFnSc7	asociace
jako	jako	k8xC	jako
minerál	minerál	k1gInSc1	minerál
<g/>
.	.	kIx.	.
</s>
<s>
Fullerenovou	Fullerenový	k2eAgFnSc4d1	Fullerenový
strukturu	struktura	k1gFnSc4	struktura
má	mít	k5eAaImIp3nS	mít
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
také	také	k9	také
tzv.	tzv.	kA	tzv.
skelný	skelný	k2eAgMnSc1d1	skelný
či	či	k8xC	či
sklovitý	sklovitý	k2eAgInSc1d1	sklovitý
uhlík	uhlík	k1gInSc1	uhlík
<g/>
,	,	kIx,	,
vyrobený	vyrobený	k2eAgInSc1d1	vyrobený
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
v	v	k7c6	v
laboratořích	laboratoř	k1gFnPc6	laboratoř
britské	britský	k2eAgFnSc2d1	britská
The	The	k1gFnSc2	The
Carborundum	Carborundum	k1gInSc4	Carborundum
Company	Compana	k1gFnSc2	Compana
<g/>
.	.	kIx.	.
</s>
<s>
Uhlíkové	Uhlíkové	k2eAgFnPc1d1	Uhlíkové
nanotrubice	nanotrubice	k1gFnPc1	nanotrubice
jsou	být	k5eAaImIp3nP	být
uměle	uměle	k6eAd1	uměle
vyrobené	vyrobený	k2eAgFnPc1d1	vyrobená
mikroskopické	mikroskopický	k2eAgFnPc1d1	mikroskopická
trubičky	trubička	k1gFnPc1	trubička
složené	složený	k2eAgFnPc1d1	složená
z	z	k7c2	z
válcově	válcově	k6eAd1	válcově
svinuté	svinutý	k2eAgFnSc2d1	svinutá
vrstvy	vrstva	k1gFnSc2	vrstva
grafenu	grafen	k1gInSc2	grafen
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
pouhých	pouhý	k2eAgInPc2d1	pouhý
několika	několik	k4yIc7	několik
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
100	[number]	k4	100
<g/>
)	)	kIx)	)
nanometrů	nanometr	k1gInPc2	nanometr
<g/>
.	.	kIx.	.
</s>
<s>
Perspektiva	perspektiva	k1gFnSc1	perspektiva
jejich	jejich	k3xOp3gNnSc2	jejich
využití	využití	k1gNnSc2	využití
se	se	k3xPyFc4	se
nabízí	nabízet	k5eAaImIp3nS	nabízet
např.	např.	kA	např.
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
velmi	velmi	k6eAd1	velmi
pevných	pevný	k2eAgNnPc2d1	pevné
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
lehkých	lehký	k2eAgInPc2d1	lehký
kompozitních	kompozitní	k2eAgInPc2d1	kompozitní
materiálů	materiál	k1gInPc2	materiál
a	a	k8xC	a
tkanin	tkanina	k1gFnPc2	tkanina
<g/>
,	,	kIx,	,
v	v	k7c6	v
elektronice	elektronika	k1gFnSc6	elektronika
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
mimořádně	mimořádně	k6eAd1	mimořádně
malých	malý	k2eAgInPc2d1	malý
tranzistorů	tranzistor	k1gInPc2	tranzistor
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
ideálního	ideální	k2eAgInSc2d1	ideální
materiálu	materiál	k1gInSc2	materiál
pro	pro	k7c4	pro
uchovávání	uchovávání	k1gNnSc4	uchovávání
čistého	čistý	k2eAgInSc2d1	čistý
vodíku	vodík	k1gInSc2	vodík
pro	pro	k7c4	pro
palivové	palivový	k2eAgInPc4d1	palivový
články	článek	k1gInPc4	článek
a	a	k8xC	a
mnohé	mnohý	k2eAgFnPc4d1	mnohá
další	další	k2eAgNnPc4d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Uhlíková	Uhlíková	k1gFnSc1	Uhlíková
nanopěna	nanopěn	k2eAgFnSc1d1	nanopěna
je	být	k5eAaImIp3nS	být
řídká	řídký	k2eAgFnSc1d1	řídká
prostorová	prostorový	k2eAgFnSc1d1	prostorová
síť	síť	k1gFnSc1	síť
tvořená	tvořený	k2eAgFnSc1d1	tvořená
klastry	klastr	k1gInPc4	klastr
uhlíkových	uhlíkový	k2eAgInPc2d1	uhlíkový
atomů	atom	k1gInPc2	atom
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
několika	několik	k4yIc2	několik
nanometrů	nanometr	k1gInPc2	nanometr
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
podobné	podobný	k2eAgInPc1d1	podobný
grafenu	grafen	k2eAgFnSc4d1	grafen
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
protože	protože	k8xS	protože
atomy	atom	k1gInPc1	atom
jsou	být	k5eAaImIp3nP	být
uspořádány	uspořádat	k5eAaPmNgInP	uspořádat
vedle	vedle	k7c2	vedle
šestiúhelníků	šestiúhelník	k1gInPc2	šestiúhelník
také	také	k9	také
do	do	k7c2	do
sedmiúhelníků	sedmiúhelník	k1gInPc2	sedmiúhelník
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
výsledná	výsledný	k2eAgFnSc1d1	výsledná
křivost	křivost	k1gFnSc1	křivost
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
fullerenů	fulleren	k1gInPc2	fulleren
záporná	záporný	k2eAgFnSc1d1	záporná
(	(	kIx(	(
<g/>
hyperbolická	hyperbolický	k2eAgFnSc1d1	hyperbolická
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
modifikace	modifikace	k1gFnSc1	modifikace
byla	být	k5eAaImAgFnS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
v	v	k7c6	v
r.	r.	kA	r.
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
Vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
se	se	k3xPyFc4	se
pozoruhodnými	pozoruhodný	k2eAgFnPc7d1	pozoruhodná
fyzikálními	fyzikální	k2eAgFnPc7d1	fyzikální
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
-	-	kIx~	-
vedle	vedle	k7c2	vedle
elektrické	elektrický	k2eAgFnSc2d1	elektrická
vodivosti	vodivost	k1gFnSc2	vodivost
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
paramagnetická	paramagnetický	k2eAgFnSc1d1	paramagnetická
<g/>
.	.	kIx.	.
</s>
<s>
Uhlík	Uhlík	k1gMnSc1	Uhlík
Q	Q	kA	Q
(	(	kIx(	(
<g/>
též	též	k9	též
Q-uhlík	Qhlík	k1gInSc1	Q-uhlík
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
umělá	umělý	k2eAgFnSc1d1	umělá
alotropní	alotropní	k2eAgFnSc1d1	alotropní
modifikace	modifikace	k1gFnSc1	modifikace
tvořená	tvořený	k2eAgFnSc1d1	tvořená
krystalickou	krystalický	k2eAgFnSc7d1	krystalická
nanovrstvou	nanovrstva	k1gFnSc7	nanovrstva
kovalentně	kovalentně	k6eAd1	kovalentně
vázaných	vázaný	k2eAgInPc2d1	vázaný
atomů	atom	k1gInPc2	atom
uhlíku	uhlík	k1gInSc2	uhlík
s	s	k7c7	s
hybridizací	hybridizace	k1gFnSc7	hybridizace
sp	sp	k?	sp
<g/>
3	[number]	k4	3
(	(	kIx(	(
<g/>
20	[number]	k4	20
<g/>
%	%	kIx~	%
<g/>
-	-	kIx~	-
<g/>
50	[number]	k4	50
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
sp	sp	k?	sp
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
monokrystalické	monokrystalický	k2eAgFnSc3d1	monokrystalická
nanočástice	nanočástika	k1gFnSc3	nanočástika
diamantu	diamant	k1gInSc2	diamant
<g/>
.	.	kIx.	.
</s>
<s>
Objev	objev	k1gInSc1	objev
byl	být	k5eAaImAgInS	být
zveřejněn	zveřejnit	k5eAaPmNgInS	zveřejnit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
se	se	k3xPyFc4	se
na	na	k7c6	na
substrátu	substrát	k1gInSc6	substrát
jiné	jiný	k2eAgFnSc2d1	jiná
látky	látka	k1gFnSc2	látka
(	(	kIx(	(
<g/>
safír	safír	k1gInSc1	safír
<g/>
,	,	kIx,	,
sklo	sklo	k1gNnSc1	sklo
<g/>
,	,	kIx,	,
plast	plast	k1gInSc1	plast
<g/>
)	)	kIx)	)
z	z	k7c2	z
vrstvy	vrstva	k1gFnSc2	vrstva
amorfního	amorfní	k2eAgInSc2d1	amorfní
uhlíku	uhlík	k1gInSc2	uhlík
jejím	její	k3xOp3gNnSc7	její
prudkým	prudký	k2eAgNnSc7d1	prudké
zahřátím	zahřátí	k1gNnSc7	zahřátí
laserovými	laserový	k2eAgInPc7d1	laserový
pulsy	puls	k1gInPc7	puls
na	na	k7c4	na
několik	několik	k4yIc4	několik
tisíc	tisíc	k4xCgInPc2	tisíc
stupňů	stupeň	k1gInPc2	stupeň
Celsia	Celsius	k1gMnSc2	Celsius
a	a	k8xC	a
následným	následný	k2eAgNnSc7d1	následné
prudkým	prudký	k2eAgNnSc7d1	prudké
ochlazením	ochlazení	k1gNnSc7	ochlazení
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
jsou	být	k5eAaImIp3nP	být
exotické	exotický	k2eAgFnPc4d1	exotická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
podle	podle	k7c2	podle
substrátu	substrát	k1gInSc2	substrát
<g/>
,	,	kIx,	,
tloušťky	tloušťka	k1gFnSc2	tloušťka
vrstvy	vrstva	k1gFnSc2	vrstva
a	a	k8xC	a
rychlosti	rychlost	k1gFnSc2	rychlost
ochlazení	ochlazení	k1gNnSc2	ochlazení
-	-	kIx~	-
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
feromagnetický	feromagnetický	k2eAgMnSc1d1	feromagnetický
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
při	při	k7c6	při
zahřátí	zahřátí	k1gNnSc6	zahřátí
světélkovat	světélkovat	k5eAaImF	světélkovat
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
vykazovat	vykazovat	k5eAaImF	vykazovat
tvrdost	tvrdost	k1gFnSc1	tvrdost
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
diamant	diamant	k1gInSc1	diamant
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
karbyn	karbyn	k1gInSc1	karbyn
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
karbin	karbin	k2eAgInSc1d1	karbin
podle	podle	k7c2	podle
ruského	ruský	k2eAgInSc2d1	ruský
originálu	originál	k1gInSc2	originál
<g/>
,	,	kIx,	,
též	též	k9	též
"	"	kIx"	"
<g/>
LAC	LAC	kA	LAC
<g/>
"	"	kIx"	"
z	z	k7c2	z
anglického	anglický	k2eAgMnSc2d1	anglický
linear	linear	k1gInSc1	linear
acetylenic	acetylenice	k1gFnPc2	acetylenice
carbon	carbon	k1gInSc1	carbon
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
forma	forma	k1gFnSc1	forma
tvořená	tvořený	k2eAgFnSc1d1	tvořená
lineárními	lineární	k2eAgFnPc7d1	lineární
molekulami	molekula	k1gFnPc7	molekula
polymerního	polymerní	k2eAgInSc2d1	polymerní
uhlíku	uhlík	k1gInSc2	uhlík
<g/>
,	,	kIx,	,
zapsatelnými	zapsatelný	k2eAgFnPc7d1	zapsatelná
vzorcem	vzorec	k1gInSc7	vzorec
-	-	kIx~	-
<g/>
(	(	kIx(	(
<g/>
C	C	kA	C
<g/>
≡	≡	k?	≡
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
n	n	k0	n
<g/>
-	-	kIx~	-
<g/>
.	.	kIx.	.
</s>
<s>
Uhlíkové	Uhlíkové	k2eAgInPc1d1	Uhlíkové
atomy	atom	k1gInPc1	atom
s	s	k7c7	s
hybridizací	hybridizace	k1gFnSc7	hybridizace
sp	sp	k?	sp
jsou	být	k5eAaImIp3nP	být
kovalentně	kovalentně	k6eAd1	kovalentně
vázány	vázat	k5eAaImNgInP	vázat
s	s	k7c7	s
dvěma	dva	k4xCgInPc7	dva
sousedními	sousední	k2eAgInPc7d1	sousední
atomy	atom	k1gInPc7	atom
<g/>
.	.	kIx.	.
</s>
<s>
Chemicky	chemicky	k6eAd1	chemicky
aktivní	aktivní	k2eAgInPc1d1	aktivní
konce	konec	k1gInPc1	konec
molekul	molekula	k1gFnPc2	molekula
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
vzájemně	vzájemně	k6eAd1	vzájemně
spojovat	spojovat	k5eAaImF	spojovat
a	a	k8xC	a
vytvářet	vytvářet	k5eAaImF	vytvářet
uzavřené	uzavřený	k2eAgInPc4d1	uzavřený
molekulární	molekulární	k2eAgInPc4d1	molekulární
řetězce	řetězec	k1gInPc4	řetězec
<g/>
.	.	kIx.	.
</s>
<s>
Karbyn	Karbyn	k1gInSc1	Karbyn
se	se	k3xPyFc4	se
chová	chovat	k5eAaImIp3nS	chovat
jako	jako	k9	jako
polovodič	polovodič	k1gInSc1	polovodič
<g/>
.	.	kIx.	.
</s>
<s>
Zájmem	zájem	k1gInSc7	zájem
nanotechnologů	nanotechnolog	k1gMnPc2	nanotechnolog
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
jeho	jeho	k3xOp3gFnPc4	jeho
mechanické	mechanický	k2eAgFnPc4d1	mechanická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
-	-	kIx~	-
při	při	k7c6	při
dobrém	dobrý	k2eAgNnSc6d1	dobré
rovnoběžném	rovnoběžný	k2eAgNnSc6d1	rovnoběžné
uspořádání	uspořádání	k1gNnSc6	uspořádání
makromolekul	makromolekula	k1gFnPc2	makromolekula
se	se	k3xPyFc4	se
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
směru	směr	k1gInSc6	směr
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
modulem	modul	k1gInSc7	modul
pružnosti	pružnost	k1gFnSc2	pružnost
40	[number]	k4	40
<g/>
krát	krát	k6eAd1	krát
vyšším	vysoký	k2eAgMnSc6d2	vyšší
než	než	k8xS	než
má	mít	k5eAaImIp3nS	mít
diamant	diamant	k1gInSc1	diamant
<g/>
;	;	kIx,	;
protože	protože	k8xS	protože
pevností	pevnost	k1gFnSc7	pevnost
v	v	k7c6	v
tahu	tah	k1gInSc6	tah
dvojnásobně	dvojnásobně	k6eAd1	dvojnásobně
převyšuje	převyšovat	k5eAaImIp3nS	převyšovat
i	i	k9	i
uhlíkové	uhlíkový	k2eAgFnPc4d1	uhlíková
nanotrubice	nanotrubice	k1gFnPc4	nanotrubice
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
vůbec	vůbec	k9	vůbec
nejpevnější	pevný	k2eAgInSc4d3	nejpevnější
známý	známý	k2eAgInSc4d1	známý
materiál	materiál	k1gInSc4	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Karbyn	Karbyn	k1gInSc1	Karbyn
byl	být	k5eAaImAgInS	být
připraven	připravit	k5eAaPmNgInS	připravit
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
v	v	k7c6	v
Ústavu	ústav	k1gInSc6	ústav
organických	organický	k2eAgFnPc2d1	organická
sloučenin	sloučenina	k1gFnPc2	sloučenina
Akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
SSSR	SSSR	kA	SSSR
<g/>
;	;	kIx,	;
ještě	ještě	k9	ještě
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
však	však	k9	však
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gFnSc1	jeho
existence	existence	k1gFnSc1	existence
jako	jako	k8xS	jako
alotropní	alotropní	k2eAgFnSc1d1	alotropní
modifikace	modifikace	k1gFnSc1	modifikace
uhlíku	uhlík	k1gInSc2	uhlík
zpochybňována	zpochybňován	k2eAgFnSc1d1	zpochybňována
<g/>
.	.	kIx.	.
</s>
<s>
Amorfní	amorfní	k2eAgInSc4d1	amorfní
uhlík	uhlík	k1gInSc4	uhlík
je	být	k5eAaImIp3nS	být
forma	forma	k1gFnSc1	forma
uhlíku	uhlík	k1gInSc2	uhlík
bez	bez	k7c2	bez
pravidelné	pravidelný	k2eAgFnSc2d1	pravidelná
krystalové	krystalový	k2eAgFnSc2d1	krystalová
struktury	struktura	k1gFnSc2	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
atomy	atom	k1gInPc4	atom
uhlíku	uhlík	k1gInSc2	uhlík
jak	jak	k8xC	jak
s	s	k7c7	s
hybridizací	hybridizace	k1gFnSc7	hybridizace
sp	sp	k?	sp
<g/>
2	[number]	k4	2
(	(	kIx(	(
<g/>
vázaný	vázaný	k2eAgInSc1d1	vázaný
s	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
sousedními	sousední	k2eAgInPc7d1	sousední
atomy	atom	k1gInPc7	atom
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
sp	sp	k?	sp
<g/>
3	[number]	k4	3
(	(	kIx(	(
<g/>
vázaný	vázaný	k2eAgInSc1d1	vázaný
s	s	k7c7	s
čtyřmi	čtyři	k4xCgInPc7	čtyři
sousedními	sousední	k2eAgInPc7d1	sousední
atomy	atom	k1gInPc7	atom
<g/>
)	)	kIx)	)
v	v	k7c6	v
různém	různý	k2eAgInSc6d1	různý
poměru	poměr	k1gInSc6	poměr
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
může	moct	k5eAaImIp3nS	moct
obsahovat	obsahovat	k5eAaImF	obsahovat
jak	jak	k6eAd1	jak
velké	velký	k2eAgFnPc4d1	velká
vakance	vakance	k1gFnPc4	vakance
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
nanokrystaly	nanokrystat	k5eAaPmAgFnP	nanokrystat
grafitu	grafit	k1gInSc2	grafit
nebo	nebo	k8xC	nebo
diamantu	diamant	k1gInSc2	diamant
v	v	k7c6	v
amorfní	amorfní	k2eAgFnSc6d1	amorfní
uhlíkové	uhlíkový	k2eAgFnSc6d1	uhlíková
matrici	matrice	k1gFnSc6	matrice
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
praktické	praktický	k2eAgNnSc4d1	praktické
využití	využití	k1gNnSc4	využití
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
např.	např.	kA	např.
jako	jako	k8xS	jako
tzv.	tzv.	kA	tzv.
aktivní	aktivní	k2eAgNnSc1d1	aktivní
uhlí	uhlí	k1gNnSc1	uhlí
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnSc1d1	další
nově	nově	k6eAd1	nově
objevenou	objevený	k2eAgFnSc7d1	objevená
amorfní	amorfní	k2eAgFnSc7d1	amorfní
modifikací	modifikace	k1gFnSc7	modifikace
jsou	být	k5eAaImIp3nP	být
tzv.	tzv.	kA	tzv.
uspořádané	uspořádaný	k2eAgInPc4d1	uspořádaný
amorfní	amorfní	k2eAgInPc4d1	amorfní
uhlíkové	uhlíkový	k2eAgInPc4d1	uhlíkový
klastry	klastr	k1gInPc4	klastr
(	(	kIx(	(
<g/>
OACC	OACC	kA	OACC
-	-	kIx~	-
z	z	k7c2	z
anglického	anglický	k2eAgMnSc2d1	anglický
ordered	ordered	k1gInSc4	ordered
amorphous	amorphous	k1gInSc1	amorphous
carbon	carbon	k1gNnSc1	carbon
clusters	clusters	k1gInSc1	clusters
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vytváří	vytvářet	k5eAaImIp3nS	vytvářet
se	se	k3xPyFc4	se
z	z	k7c2	z
fullerenů	fulleren	k1gInPc2	fulleren
C60	C60	k1gFnPc2	C60
(	(	kIx(	(
<g/>
propojených	propojený	k2eAgFnPc2d1	propojená
xylenovými	xylenův	k2eAgFnPc7d1	xylenův
molekulami	molekula	k1gFnPc7	molekula
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
struktura	struktura	k1gFnSc1	struktura
je	být	k5eAaImIp3nS	být
působením	působení	k1gNnSc7	působení
vysokého	vysoký	k2eAgInSc2d1	vysoký
tlaku	tlak	k1gInSc2	tlak
desítek	desítka	k1gFnPc2	desítka
GPa	GPa	k1gFnPc2	GPa
zborcena	zborcen	k2eAgFnSc1d1	zborcen
do	do	k7c2	do
kompaktní	kompaktní	k2eAgFnSc2d1	kompaktní
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
tvrdé	tvrdý	k2eAgFnPc1d1	tvrdá
amorfní	amorfní	k2eAgFnPc1d1	amorfní
látky	látka	k1gFnPc1	látka
<g/>
,	,	kIx,	,
schopné	schopný	k2eAgFnPc1d1	schopná
rýt	rýt	k5eAaImF	rýt
i	i	k9	i
diamant	diamant	k1gInSc4	diamant
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
při	při	k7c6	při
pokojové	pokojový	k2eAgFnSc6d1	pokojová
teplotě	teplota	k1gFnSc6	teplota
<g/>
,	,	kIx,	,
mohla	moct	k5eAaImAgFnS	moct
by	by	kYmCp3nS	by
v	v	k7c6	v
průmyslovém	průmyslový	k2eAgNnSc6d1	průmyslové
využití	využití	k1gNnSc6	využití
konkurovat	konkurovat	k5eAaImF	konkurovat
syntetickým	syntetický	k2eAgInPc3d1	syntetický
diamantům	diamant	k1gInPc3	diamant
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
jejichž	jejichž	k3xOyRp3gFnSc4	jejichž
výrobu	výroba	k1gFnSc4	výroba
jsou	být	k5eAaImIp3nP	být
nutné	nutný	k2eAgFnPc1d1	nutná
teploty	teplota	k1gFnPc1	teplota
kolem	kolem	k7c2	kolem
1500	[number]	k4	1500
°	°	k?	°
<g/>
C.	C.	kA	C.
V	v	k7c6	v
anorganických	anorganický	k2eAgFnPc6d1	anorganická
chemických	chemický	k2eAgFnPc6d1	chemická
sloučeninách	sloučenina	k1gFnPc6	sloučenina
se	se	k3xPyFc4	se
uhlík	uhlík	k1gInSc4	uhlík
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
mocenství	mocenství	k1gNnSc6	mocenství
+2	+2	k4	+2
<g/>
,	,	kIx,	,
+4	+4	k4	+4
a	a	k8xC	a
-	-	kIx~	-
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
oxidů	oxid	k1gInPc2	oxid
je	být	k5eAaImIp3nS	být
důležitý	důležitý	k2eAgInSc4d1	důležitý
především	především	k9	především
oxid	oxid	k1gInSc4	oxid
uhličitý	uhličitý	k2eAgInSc4d1	uhličitý
CO	co	k6eAd1	co
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
podílí	podílet	k5eAaImIp3nS	podílet
na	na	k7c4	na
vytváření	vytváření	k1gNnSc4	vytváření
rostlinných	rostlinný	k2eAgFnPc2d1	rostlinná
tkání	tkáň	k1gFnPc2	tkáň
v	v	k7c6	v
procesu	proces	k1gInSc6	proces
zvaném	zvaný	k2eAgInSc6d1	zvaný
fotosyntéza	fotosyntéza	k1gFnSc1	fotosyntéza
a	a	k8xC	a
současně	současně	k6eAd1	současně
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
do	do	k7c2	do
atmosféry	atmosféra	k1gFnSc2	atmosféra
pří	přít	k5eAaImIp3nS	přít
dýchání	dýchání	k1gNnSc1	dýchání
živých	živý	k2eAgInPc2d1	živý
organizmů	organizmus	k1gInPc2	organizmus
a	a	k8xC	a
spalování	spalování	k1gNnSc6	spalování
fosilních	fosilní	k2eAgNnPc2d1	fosilní
paliv	palivo	k1gNnPc2	palivo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
se	se	k3xPyFc4	se
CO2	CO2	k1gFnSc1	CO2
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
oxoniového	oxoniový	k2eAgInSc2d1	oxoniový
iontu	ion	k1gInSc2	ion
H	H	kA	H
<g/>
3	[number]	k4	3
<g/>
O	O	kA	O
<g/>
+	+	kIx~	+
a	a	k8xC	a
hydrogenuhličitanového	hydrogenuhličitanový	k2eAgInSc2d1	hydrogenuhličitanový
iontu	ion	k1gInSc2	ion
HCO	HCO	kA	HCO
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
chybně	chybně	k6eAd1	chybně
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
označováno	označovat	k5eAaImNgNnS	označovat
jako	jako	k9	jako
kyselina	kyselina	k1gFnSc1	kyselina
uhličitá	uhličitý	k2eAgFnSc1d1	uhličitá
<g/>
.	.	kIx.	.
</s>
<s>
Známy	znám	k2eAgInPc1d1	znám
jsou	být	k5eAaImIp3nP	být
především	především	k9	především
soli	sůl	k1gFnPc1	sůl
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
nerozpustná	rozpustný	k2eNgFnSc1d1	nerozpustná
<g/>
,	,	kIx,	,
rozpustné	rozpustný	k2eAgFnPc1d1	rozpustná
jsou	být	k5eAaImIp3nP	být
jen	jen	k9	jen
uhličitany	uhličitan	k1gInPc1	uhličitan
alkalických	alkalický	k2eAgInPc2d1	alkalický
kovů	kov	k1gInPc2	kov
a	a	k8xC	a
amoniaku	amoniak	k1gInSc2	amoniak
<g/>
.	.	kIx.	.
</s>
<s>
Uhličitany	uhličitan	k1gInPc1	uhličitan
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nP	řadit
též	též	k9	též
i	i	k9	i
mezi	mezi	k7c7	mezi
nerosty	nerost	k1gInPc7	nerost
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
jsou	být	k5eAaImIp3nP	být
uvedeny	uvést	k5eAaPmNgInP	uvést
v	v	k7c6	v
následující	následující	k2eAgFnSc6d1	následující
tabulce	tabulka	k1gFnSc6	tabulka
<g/>
.	.	kIx.	.
</s>
<s>
Oxid	oxid	k1gInSc1	oxid
uhlíku	uhlík	k1gInSc2	uhlík
s	s	k7c7	s
valencí	valence	k1gFnSc7	valence
+2	+2	k4	+2
<g/>
,	,	kIx,	,
oxid	oxid	k1gInSc1	oxid
uhelnatý	uhelnatý	k2eAgInSc1d1	uhelnatý
CO	co	k8xS	co
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
toxický	toxický	k2eAgInSc1d1	toxický
plyn	plyn	k1gInSc1	plyn
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
blokuje	blokovat	k5eAaImIp3nS	blokovat
krevní	krevní	k2eAgNnSc4d1	krevní
barvivo	barvivo	k1gNnSc4	barvivo
hemoglobin	hemoglobin	k1gInSc1	hemoglobin
a	a	k8xC	a
znemožňuje	znemožňovat	k5eAaImIp3nS	znemožňovat
tak	tak	k9	tak
dýchání	dýchání	k1gNnSc1	dýchání
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
spočívá	spočívat	k5eAaImIp3nS	spočívat
především	především	k9	především
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
bezbarvý	bezbarvý	k2eAgMnSc1d1	bezbarvý
a	a	k8xC	a
bez	bez	k7c2	bez
zápachu	zápach	k1gInSc2	zápach
a	a	k8xC	a
člověk	člověk	k1gMnSc1	člověk
proto	proto	k8xC	proto
jeho	jeho	k3xOp3gFnSc1	jeho
přítomnost	přítomnost	k1gFnSc1	přítomnost
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
nemůže	moct	k5eNaImIp3nS	moct
poznat	poznat	k5eAaPmF	poznat
svými	svůj	k3xOyFgInPc7	svůj
smysly	smysl	k1gInPc7	smysl
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
příčinou	příčina	k1gFnSc7	příčina
mnoha	mnoho	k4c2	mnoho
smrtelných	smrtelný	k2eAgFnPc2d1	smrtelná
otrav	otrava	k1gFnPc2	otrava
v	v	k7c6	v
uhelných	uhelný	k2eAgInPc6d1	uhelný
dolech	dol	k1gInPc6	dol
nebo	nebo	k8xC	nebo
v	v	k7c6	v
domácnostech	domácnost	k1gFnPc6	domácnost
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
k	k	k7c3	k
topení	topení	k1gNnSc3	topení
používal	používat	k5eAaImAgInS	používat
svítiplyn	svítiplyn	k1gInSc1	svítiplyn
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
sírou	síra	k1gFnSc7	síra
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
uhlík	uhlík	k1gInSc1	uhlík
toxickou	toxický	k2eAgFnSc4d1	toxická
kapalnou	kapalný	k2eAgFnSc4d1	kapalná
sloučeninu	sloučenina	k1gFnSc4	sloučenina
-	-	kIx~	-
sirouhlík	sirouhlík	k1gInSc4	sirouhlík
CS	CS	kA	CS
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
se	se	k3xPyFc4	se
vedením	vedení	k1gNnSc7	vedení
pár	pár	k4xCyI	pár
síry	síra	k1gFnSc2	síra
přes	přes	k7c4	přes
rozžhavený	rozžhavený	k2eAgInSc4d1	rozžhavený
uhlík	uhlík	k1gInSc4	uhlík
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
dusíkem	dusík	k1gInSc7	dusík
tvoří	tvořit	k5eAaImIp3nS	tvořit
uhlík	uhlík	k1gInSc4	uhlík
kyanidový	kyanidový	k2eAgInSc1d1	kyanidový
ion	ion	k1gInSc1	ion
CN	CN	kA	CN
<g/>
-	-	kIx~	-
a	a	k8xC	a
kyanovodík	kyanovodík	k1gInSc1	kyanovodík
HCN	HCN	kA	HCN
patří	patřit	k5eAaImIp3nS	patřit
také	také	k9	také
k	k	k7c3	k
mimořádně	mimořádně	k6eAd1	mimořádně
toxickým	toxický	k2eAgFnPc3d1	toxická
látkám	látka	k1gFnPc3	látka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
však	však	k9	však
můžeme	moct	k5eAaImIp1nP	moct
detekovat	detekovat	k5eAaImF	detekovat
čichem	čich	k1gInSc7	čich
jeho	jeho	k3xOp3gFnSc1	jeho
silný	silný	k2eAgInSc4d1	silný
zápach	zápach	k1gInSc4	zápach
po	po	k7c6	po
hořkých	hořký	k2eAgFnPc6d1	hořká
mandlích	mandle	k1gFnPc6	mandle
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
kovovými	kovový	k2eAgInPc7d1	kovový
prvky	prvek	k1gInPc7	prvek
tvoří	tvořit	k5eAaImIp3nP	tvořit
uhlík	uhlík	k1gInSc4	uhlík
karbidy	karbid	k1gInPc1	karbid
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgFnSc1d3	nejznámější
je	být	k5eAaImIp3nS	být
karbid	karbid	k1gInSc4	karbid
vápenatý	vápenatý	k2eAgInSc4d1	vápenatý
CaC	CaC	k1gFnSc7	CaC
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
při	při	k7c6	při
reakci	reakce	k1gFnSc6	reakce
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
acetylen	acetylen	k1gInSc1	acetylen
(	(	kIx(	(
<g/>
ethyn	ethyn	k1gInSc1	ethyn
<g/>
)	)	kIx)	)
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
dříve	dříve	k6eAd2	dříve
používán	používat	k5eAaImNgInS	používat
ke	k	k7c3	k
svícení	svícení	k1gNnSc3	svícení
v	v	k7c6	v
lampách	lampa	k1gFnPc6	lampa
<g/>
,	,	kIx,	,
karbidkách	karbidka	k1gFnPc6	karbidka
a	a	k8xC	a
na	na	k7c6	na
svařování	svařování	k1gNnSc6	svařování
<g/>
.	.	kIx.	.
</s>
<s>
Poměrně	poměrně	k6eAd1	poměrně
známý	známý	k2eAgInSc1d1	známý
je	být	k5eAaImIp3nS	být
i	i	k9	i
karbid	karbid	k1gInSc4	karbid
křemíku	křemík	k1gInSc2	křemík
SiC	sic	k8xC	sic
neboli	neboli	k8xC	neboli
karborundum	karborundum	k1gNnSc1	karborundum
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
má	mít	k5eAaImIp3nS	mít
krystalickou	krystalický	k2eAgFnSc4d1	krystalická
strukturu	struktura	k1gFnSc4	struktura
podobnou	podobný	k2eAgFnSc4d1	podobná
diamantu	diamant	k1gInSc3	diamant
a	a	k8xC	a
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
se	se	k3xPyFc4	se
mimořádnou	mimořádný	k2eAgFnSc7d1	mimořádná
tvrdostí	tvrdost	k1gFnSc7	tvrdost
<g/>
.	.	kIx.	.
</s>
<s>
Organické	organický	k2eAgFnPc1d1	organická
sloučeniny	sloučenina	k1gFnPc1	sloučenina
jsou	být	k5eAaImIp3nP	být
chemické	chemický	k2eAgFnPc1d1	chemická
látky	látka	k1gFnPc1	látka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
alespoň	alespoň	k9	alespoň
jeden	jeden	k4xCgInSc4	jeden
atom	atom	k1gInSc4	atom
uhlíku	uhlík	k1gInSc2	uhlík
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
vždy	vždy	k6eAd1	vždy
atom	atom	k1gInSc4	atom
vodíku	vodík	k1gInSc2	vodík
<g/>
,	,	kIx,	,
převážná	převážný	k2eAgFnSc1d1	převážná
většina	většina	k1gFnSc1	většina
přitom	přitom	k6eAd1	přitom
má	mít	k5eAaImIp3nS	mít
spolu	spolu	k6eAd1	spolu
vázané	vázaný	k2eAgInPc4d1	vázaný
atomy	atom	k1gInPc4	atom
uhlíku	uhlík	k1gInSc2	uhlík
vazbou	vazba	k1gFnSc7	vazba
C-	C-	k1gFnSc2	C-
<g/>
C.	C.	kA	C.
Každý	každý	k3xTgInSc4	každý
atom	atom	k1gInSc4	atom
uhlíku	uhlík	k1gInSc2	uhlík
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
vytvářet	vytvářet	k5eAaImF	vytvářet
celkem	celkem	k6eAd1	celkem
čtyři	čtyři	k4xCgInPc1	čtyři
tyto	tento	k3xDgInPc1	tento
tzv.	tzv.	kA	tzv.
jednoduché	jednoduchý	k2eAgFnPc4d1	jednoduchá
vazby	vazba	k1gFnPc4	vazba
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
i	i	k9	i
vazbu	vazba	k1gFnSc4	vazba
dvojnou	dvojný	k2eAgFnSc7d1	dvojná
C	C	kA	C
<g/>
=	=	kIx~	=
<g/>
C	C	kA	C
a	a	k8xC	a
vazbu	vazba	k1gFnSc4	vazba
trojnou	trojný	k2eAgFnSc4d1	trojná
C	C	kA	C
<g/>
≡	≡	k?	≡
<g/>
C.	C.	kA	C.
Mohou	moct	k5eAaImIp3nP	moct
proto	proto	k8xC	proto
vznikat	vznikat	k5eAaImF	vznikat
dlouhé	dlouhý	k2eAgInPc4d1	dlouhý
řetězce	řetězec	k1gInPc4	řetězec
a	a	k8xC	a
molekuly	molekula	k1gFnPc4	molekula
s	s	k7c7	s
rozvětvenou	rozvětvený	k2eAgFnSc7d1	rozvětvená
nebo	nebo	k8xC	nebo
cyklickou	cyklický	k2eAgFnSc7d1	cyklická
strukturou	struktura	k1gFnSc7	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
uhlíkem	uhlík	k1gInSc7	uhlík
se	se	k3xPyFc4	se
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
molekulách	molekula	k1gFnPc6	molekula
váží	vážit	k5eAaImIp3nP	vážit
i	i	k9	i
další	další	k2eAgInPc4d1	další
prvky	prvek	k1gInPc4	prvek
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
biogenní	biogenní	k2eAgInPc4d1	biogenní
prvky	prvek	k1gInPc4	prvek
vodík	vodík	k1gInSc1	vodík
<g/>
,	,	kIx,	,
kyslík	kyslík	k1gInSc1	kyslík
<g/>
,	,	kIx,	,
dusík	dusík	k1gInSc1	dusík
<g/>
,	,	kIx,	,
síra	síra	k1gFnSc1	síra
a	a	k8xC	a
fosfor	fosfor	k1gInSc1	fosfor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mohou	moct	k5eAaImIp3nP	moct
to	ten	k3xDgNnSc1	ten
být	být	k5eAaImF	být
i	i	k9	i
halogeny	halogen	k1gInPc1	halogen
<g/>
,	,	kIx,	,
křemík	křemík	k1gInSc1	křemík
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgInPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
existuje	existovat	k5eAaImIp3nS	existovat
nesmírně	smírně	k6eNd1	smírně
mnoho	mnoho	k4c4	mnoho
kombinací	kombinace	k1gFnPc2	kombinace
<g/>
;	;	kIx,	;
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
více	hodně	k6eAd2	hodně
než	než	k8xS	než
10	[number]	k4	10
milionů	milion	k4xCgInPc2	milion
organických	organický	k2eAgFnPc2d1	organická
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
výčet	výčet	k1gInSc1	výčet
je	být	k5eAaImIp3nS	být
pravidelně	pravidelně	k6eAd1	pravidelně
registrován	registrovat	k5eAaBmNgMnS	registrovat
v	v	k7c6	v
Beilsteinově	Beilsteinův	k2eAgFnSc6d1	Beilsteinův
databázi	databáze	k1gFnSc6	databáze
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
shromažďuje	shromažďovat	k5eAaImIp3nS	shromažďovat
souhnrná	souhnrný	k2eAgNnPc4d1	souhnrný
data	datum	k1gNnPc4	datum
o	o	k7c6	o
všech	všecek	k3xTgFnPc6	všecek
známých	známý	k2eAgFnPc6d1	známá
organických	organický	k2eAgFnPc6d1	organická
sloučeninách	sloučenina	k1gFnPc6	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
díky	díky	k7c3	díky
této	tento	k3xDgFnSc3	tento
obrovské	obrovský	k2eAgFnSc3d1	obrovská
rozmanitosti	rozmanitost	k1gFnSc3	rozmanitost
se	se	k3xPyFc4	se
organické	organický	k2eAgFnPc1d1	organická
látky	látka	k1gFnPc1	látka
staly	stát	k5eAaPmAgFnP	stát
základním	základní	k2eAgInSc7d1	základní
stavebním	stavební	k2eAgInSc7d1	stavební
kamenem	kámen	k1gInSc7	kámen
živé	živý	k2eAgFnSc2d1	živá
hmoty	hmota	k1gFnSc2	hmota
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
buňka	buňka	k1gFnSc1	buňka
živého	živý	k2eAgInSc2d1	živý
organismu	organismus	k1gInSc2	organismus
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
desetitisíce	desetitisíce	k1gInPc4	desetitisíce
chemických	chemický	k2eAgFnPc2d1	chemická
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
tu	ten	k3xDgFnSc4	ten
jedinou	jediný	k2eAgFnSc4d1	jediná
společnou	společný	k2eAgFnSc4d1	společná
vlastnost	vlastnost	k1gFnSc4	vlastnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gInSc1	jejich
základní	základní	k2eAgInSc1d1	základní
skelet	skelet	k1gInSc1	skelet
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
atomy	atom	k1gInPc7	atom
uhlíku	uhlík	k1gInSc2	uhlík
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
vazebných	vazebný	k2eAgInPc6d1	vazebný
stavech	stav	k1gInPc6	stav
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc4d1	následující
výčet	výčet	k1gInSc4	výčet
typů	typ	k1gInPc2	typ
organických	organický	k2eAgFnPc2d1	organická
sloučenin	sloučenina	k1gFnPc2	sloučenina
není	být	k5eNaImIp3nS	být
zdaleka	zdaleka	k6eAd1	zdaleka
úplný	úplný	k2eAgInSc1d1	úplný
a	a	k8xC	a
měl	mít	k5eAaImAgInS	mít
by	by	kYmCp3nS	by
pouze	pouze	k6eAd1	pouze
podat	podat	k5eAaPmF	podat
informaci	informace	k1gFnSc4	informace
o	o	k7c6	o
nejčastěji	často	k6eAd3	často
používaných	používaný	k2eAgInPc6d1	používaný
a	a	k8xC	a
vyráběných	vyráběný	k2eAgInPc6d1	vyráběný
typech	typ	k1gInPc6	typ
organických	organický	k2eAgFnPc2d1	organická
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Uhlovodíky	uhlovodík	k1gInPc1	uhlovodík
jsou	být	k5eAaImIp3nP	být
sloučeniny	sloučenina	k1gFnPc1	sloučenina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
molekule	molekula	k1gFnSc6	molekula
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
pouze	pouze	k6eAd1	pouze
atomy	atom	k1gInPc1	atom
uhlíku	uhlík	k1gInSc2	uhlík
a	a	k8xC	a
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
<g/>
:	:	kIx,	:
alifatické	alifatický	k2eAgInPc4d1	alifatický
uhlovodíky	uhlovodík	k1gInPc4	uhlovodík
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
molekuly	molekula	k1gFnPc1	molekula
mají	mít	k5eAaImIp3nP	mít
tvar	tvar	k1gInSc4	tvar
otevřeného	otevřený	k2eAgNnSc2d1	otevřené
(	(	kIx(	(
<g/>
případně	případně	k6eAd1	případně
rozvětveného	rozvětvený	k2eAgInSc2d1	rozvětvený
<g/>
)	)	kIx)	)
řetězce	řetězec	k1gInSc2	řetězec
a	a	k8xC	a
alicyklické	alicyklický	k2eAgInPc4d1	alicyklický
uhlovodíky	uhlovodík	k1gInPc4	uhlovodík
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
molekuly	molekula	k1gFnPc1	molekula
mají	mít	k5eAaImIp3nP	mít
tvar	tvar	k1gInSc4	tvar
řetězce	řetězec	k1gInSc2	řetězec
(	(	kIx(	(
<g/>
případně	případně	k6eAd1	případně
rozvětveného	rozvětvený	k2eAgNnSc2d1	rozvětvené
<g/>
)	)	kIx)	)
uzavřeného	uzavřený	k2eAgNnSc2d1	uzavřené
do	do	k7c2	do
jedné	jeden	k4xCgFnSc2	jeden
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
smyček	smyčka	k1gFnPc2	smyčka
<g/>
,	,	kIx,	,
a	a	k8xC	a
které	který	k3yRgFnPc1	který
nespadají	spadat	k5eNaImIp3nP	spadat
do	do	k7c2	do
aromatických	aromatický	k2eAgInPc2d1	aromatický
aromatické	aromatický	k2eAgInPc4d1	aromatický
uhlovodíky	uhlovodík	k1gInPc4	uhlovodík
(	(	kIx(	(
<g/>
areny	arena	k1gFnPc4	arena
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
molekuly	molekula	k1gFnPc1	molekula
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
alespoň	alespoň	k9	alespoň
jeden	jeden	k4xCgInSc1	jeden
uzavřený	uzavřený	k2eAgInSc1d1	uzavřený
řetězec	řetězec	k1gInSc1	řetězec
s	s	k7c7	s
delokalizovanými	delokalizovaný	k2eAgInPc7d1	delokalizovaný
elektrony	elektron	k1gInPc7	elektron
vazeb	vazba	k1gFnPc2	vazba
π	π	k?	π
(	(	kIx(	(
<g/>
nejtypičtější	typický	k2eAgMnSc1d3	nejtypičtější
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
benzenové	benzenový	k2eAgNnSc1d1	benzenové
jádro	jádro	k1gNnSc1	jádro
s	s	k7c7	s
šesti	šest	k4xCc2	šest
atomy	atom	k1gInPc7	atom
uhlíku	uhlík	k1gInSc2	uhlík
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc4	první
dvě	dva	k4xCgFnPc4	dva
skupiny	skupina	k1gFnPc4	skupina
pak	pak	k6eAd1	pak
podle	podle	k7c2	podle
vazeb	vazba	k1gFnPc2	vazba
mezi	mezi	k7c7	mezi
atomy	atom	k1gInPc7	atom
uhlíku	uhlík	k1gInSc2	uhlík
dělíme	dělit	k5eAaImIp1nP	dělit
na	na	k7c4	na
<g/>
:	:	kIx,	:
alkany	alkana	k1gFnPc4	alkana
s	s	k7c7	s
pouze	pouze	k6eAd1	pouze
jednoduchou	jednoduchý	k2eAgFnSc7d1	jednoduchá
vazbou	vazba	k1gFnSc7	vazba
C-C	C-C	k1gFnSc2	C-C
alkeny	alkena	k1gFnSc2	alkena
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgMnPc1d1	obsahující
minimálně	minimálně	k6eAd1	minimálně
jednu	jeden	k4xCgFnSc4	jeden
dvojnou	dvojný	k2eAgFnSc4d1	dvojná
vazbu	vazba	k1gFnSc4	vazba
C	C	kA	C
<g/>
=	=	kIx~	=
<g/>
C	C	kA	C
a	a	k8xC	a
alkyny	alkyna	k1gFnSc2	alkyna
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgMnPc1d1	obsahující
minimálně	minimálně	k6eAd1	minimálně
jednu	jeden	k4xCgFnSc4	jeden
trojnou	trojný	k2eAgFnSc4d1	trojná
vazbu	vazba	k1gFnSc4	vazba
C	C	kA	C
<g/>
≡	≡	k?	≡
<g/>
C	C	kA	C
Sloučeniny	sloučenina	k1gFnSc2	sloučenina
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
<g />
.	.	kIx.	.
</s>
<s>
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
molekule	molekula	k1gFnSc6	molekula
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
C	C	kA	C
<g/>
,	,	kIx,	,
H	H	kA	H
a	a	k8xC	a
O	O	kA	O
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
zhruba	zhruba	k6eAd1	zhruba
rozdělit	rozdělit	k5eAaPmF	rozdělit
do	do	k7c2	do
následujících	následující	k2eAgFnPc2d1	následující
skupin	skupina	k1gFnPc2	skupina
<g/>
:	:	kIx,	:
alkoholy	alkohol	k1gInPc1	alkohol
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgInSc1d1	obsahující
skupinu	skupina	k1gFnSc4	skupina
C-OH	C-OH	k1gFnSc2	C-OH
fenoly	fenol	k1gInPc1	fenol
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
skupinu	skupina	k1gFnSc4	skupina
-OH	-OH	k?	-OH
mají	mít	k5eAaImIp3nP	mít
připojenu	připojen	k2eAgFnSc4d1	připojena
k	k	k7c3	k
aromatickému	aromatický	k2eAgNnSc3d1	aromatické
jádru	jádro	k1gNnSc3	jádro
ethery	ether	k1gInPc4	ether
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgFnSc4d1	obsahující
skupinu	skupina	k1gFnSc4	skupina
C-O-C	C-O-C	k1gFnSc2	C-O-C
organické	organický	k2eAgInPc4d1	organický
peroxidy	peroxid	k1gInPc4	peroxid
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgFnSc4d1	obsahující
skupinu	skupina	k1gFnSc4	skupina
C-O-O-C	C-O-O-C	k1gFnSc2	C-O-O-C
<g />
.	.	kIx.	.
</s>
<s>
aldehydy	aldehyd	k1gInPc4	aldehyd
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgFnSc4d1	obsahující
skupinu	skupina	k1gFnSc4	skupina
HC	HC	kA	HC
<g/>
=	=	kIx~	=
<g/>
O	o	k7c4	o
ketony	keton	k1gInPc4	keton
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgFnSc4d1	obsahující
skupinu	skupina	k1gFnSc4	skupina
C-CO-C	C-CO-C	k1gFnSc2	C-CO-C
karboxylové	karboxylový	k2eAgFnSc2d1	karboxylová
kyseliny	kyselina	k1gFnSc2	kyselina
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgFnSc4d1	obsahující
skupinu	skupina	k1gFnSc4	skupina
-COOH	-COOH	k?	-COOH
estery	ester	k1gInPc4	ester
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgFnSc4d1	obsahující
skupinu	skupina	k1gFnSc4	skupina
R-C-OOR	R-C-OOR	k1gMnSc2	R-C-OOR
Další	další	k2eAgInPc1d1	další
typy	typ	k1gInPc7	typ
organických	organický	k2eAgFnPc2d1	organická
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
molekule	molekula	k1gFnSc6	molekula
obsahuji	obsahovat	k5eAaImIp1nS	obsahovat
i	i	k9	i
dusík	dusík	k1gInSc4	dusík
nebo	nebo	k8xC	nebo
síru	síra	k1gFnSc4	síra
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
uvedeny	uvést	k5eAaPmNgInP	uvést
v	v	k7c6	v
heslech	heslo	k1gNnPc6	heslo
těchto	tento	k3xDgInPc2	tento
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Uhlík	uhlík	k1gInSc1	uhlík
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
organických	organický	k2eAgFnPc2d1	organická
sloučeninách	sloučenina	k1gFnPc6	sloučenina
čtyřvazný	čtyřvazný	k2eAgInSc1d1	čtyřvazný
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
oxidačnímu	oxidační	k2eAgNnSc3d1	oxidační
číslu	číslo	k1gNnSc3	číslo
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
</s>
<s>
Vazba	vazba	k1gFnSc1	vazba
uhlíku	uhlík	k1gInSc2	uhlík
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
<g/>
:	:	kIx,	:
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
dvojná	dvojný	k2eAgFnSc1d1	dvojná
vazba	vazba	k1gFnSc1	vazba
je	být	k5eAaImIp3nS	být
vytvářena	vytvářit	k5eAaPmNgFnS	vytvářit
dvěma	dva	k4xCgInPc7	dva
el.	el.	k?	el.
páry	pár	k1gInPc1	pár
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
nejsou	být	k5eNaImIp3nP	být
rovnocenné	rovnocenný	k2eAgInPc1d1	rovnocenný
<g/>
.	.	kIx.	.
</s>
<s>
El.	El.	k?	El.
vazebné	vazebný	k2eAgFnSc2d1	vazebná
<g/>
,	,	kIx,	,
nazývané	nazývaný	k2eAgFnSc2d1	nazývaná
σ	σ	k?	σ
<g/>
,	,	kIx,	,
zbývající	zbývající	k2eAgMnSc1d1	zbývající
dva	dva	k4xCgInPc4	dva
π	π	k?	π
jsou	být	k5eAaImIp3nP	být
pohyblivější	pohyblivý	k2eAgMnPc1d2	pohyblivější
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
nositeli	nositel	k1gMnPc7	nositel
reaktivnosti	reaktivnost	k1gFnSc2	reaktivnost
dvojné	dvojný	k2eAgFnSc2d1	dvojná
vazby	vazba	k1gFnSc2	vazba
<g/>
.	.	kIx.	.
trojná	trojný	k2eAgFnSc1d1	trojná
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
sdílením	sdílení	k1gNnSc7	sdílení
tří	tři	k4xCgInPc2	tři
el.	el.	k?	el.
párů	pár	k1gInPc2	pár
<g/>
,	,	kIx,	,
dvěma	dva	k4xCgFnPc7	dva
σ	σ	k?	σ
a	a	k8xC	a
čtyřmi	čtyři	k4xCgNnPc7	čtyři
π	π	k?	π
rozvětvená	rozvětvený	k2eAgFnSc1d1	rozvětvená
nerozvětvená	rozvětvený	k2eNgFnSc1d1	nerozvětvená
uzavřená	uzavřený	k2eAgFnSc1d1	uzavřená
do	do	k7c2	do
cyklů	cyklus	k1gInPc2	cyklus
jednoduchých	jednoduchý	k2eAgInPc2d1	jednoduchý
nebo	nebo	k8xC	nebo
složených	složený	k2eAgInPc2d1	složený
Volné	volný	k2eAgFnSc2d1	volná
vazby	vazba	k1gFnPc1	vazba
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
strukturách	struktura	k1gFnPc6	struktura
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
obsazeny	obsazen	k2eAgInPc4d1	obsazen
atomy	atom	k1gInPc4	atom
H	H	kA	H
<g/>
,	,	kIx,	,
O	O	kA	O
<g/>
,	,	kIx,	,
S	s	k7c7	s
<g/>
,	,	kIx,	,
N	N	kA	N
nebo	nebo	k8xC	nebo
skupinami	skupina	k1gFnPc7	skupina
prvků	prvek	k1gInPc2	prvek
(	(	kIx(	(
<g/>
radikály	radikál	k1gInPc1	radikál
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sloučeniny	sloučenina	k1gFnPc1	sloučenina
s	s	k7c7	s
vazbami	vazba	k1gFnPc7	vazba
mezi	mezi	k7c7	mezi
uhlíky	uhlík	k1gInPc7	uhlík
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
nasycené	nasycený	k2eAgNnSc1d1	nasycené
(	(	kIx(	(
<g/>
větší	veliký	k2eAgFnSc1d2	veliký
stálost	stálost	k1gFnSc1	stálost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
dvojnou	dvojný	k2eAgFnSc7d1	dvojná
nebo	nebo	k8xC	nebo
trojnou	trojný	k2eAgFnSc7d1	trojná
vazbou	vazba	k1gFnSc7	vazba
jsou	být	k5eAaImIp3nP	být
nenasycené	nasycený	k2eNgFnPc1d1	nenasycená
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
i	i	k8xC	i
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
je	být	k5eAaImIp3nS	být
uhlík	uhlík	k1gInSc4	uhlík
poměrně	poměrně	k6eAd1	poměrně
značně	značně	k6eAd1	značně
rozšířeným	rozšířený	k2eAgInSc7d1	rozšířený
prvkem	prvek	k1gInSc7	prvek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
kůře	kůra	k1gFnSc6	kůra
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gInSc1	jeho
obsah	obsah	k1gInSc1	obsah
uváděn	uvádět	k5eAaImNgInS	uvádět
s	s	k7c7	s
relativně	relativně	k6eAd1	relativně
vysokým	vysoký	k2eAgInSc7d1	vysoký
rozptylem	rozptyl	k1gInSc7	rozptyl
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
800	[number]	k4	800
ppm	ppm	k?	ppm
(	(	kIx(	(
<g/>
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obsah	obsah	k1gInSc1	obsah
v	v	k7c6	v
mořské	mořský	k2eAgFnSc6d1	mořská
vodě	voda	k1gFnSc6	voda
činí	činit	k5eAaImIp3nS	činit
28	[number]	k4	28
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
l.	l.	k?	l.
Ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
připadá	připadat	k5eAaImIp3nS	připadat
jeden	jeden	k4xCgInSc4	jeden
atom	atom	k1gInSc4	atom
uhlíku	uhlík	k1gInSc2	uhlík
přibližně	přibližně	k6eAd1	přibližně
na	na	k7c4	na
20	[number]	k4	20
000	[number]	k4	000
atomů	atom	k1gInPc2	atom
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
využití	využití	k1gNnPc1	využití
uhlíku	uhlík	k1gInSc2	uhlík
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
zlepšení	zlepšení	k1gNnSc4	zlepšení
tvrdosti	tvrdost	k1gFnSc2	tvrdost
<g/>
.	.	kIx.	.
</s>
<s>
Proces	proces	k1gInSc1	proces
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgInSc6	jenž
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
dochází	docházet	k5eAaImIp3nS	docházet
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
impregnace	impregnace	k1gFnSc1	impregnace
uhlíku	uhlík	k1gInSc2	uhlík
a	a	k8xC	a
provádí	provádět	k5eAaImIp3nS	provádět
se	se	k3xPyFc4	se
buď	buď	k8xC	buď
kovy	kov	k1gInPc1	kov
(	(	kIx(	(
<g/>
měď	měď	k1gFnSc1	měď
<g/>
,	,	kIx,	,
hliník	hliník	k1gInSc1	hliník
<g/>
,	,	kIx,	,
cín	cín	k1gInSc1	cín
<g/>
,	,	kIx,	,
stříbro	stříbro	k1gNnSc1	stříbro
<g/>
,	,	kIx,	,
olovo	olovo	k1gNnSc1	olovo
<g/>
,	,	kIx,	,
kadmium	kadmium	k1gNnSc1	kadmium
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
syntetickými	syntetický	k2eAgFnPc7d1	syntetická
pryskyřicemi	pryskyřice	k1gFnPc7	pryskyřice
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tepelném	tepelný	k2eAgNnSc6d1	tepelné
zpracování	zpracování	k1gNnSc6	zpracování
uhlíku	uhlík	k1gInSc2	uhlík
vznikají	vznikat	k5eAaImIp3nP	vznikat
póry	pór	k1gInPc1	pór
-	-	kIx~	-
takový	takový	k3xDgInSc4	takový
proces	proces	k1gInSc4	proces
zveme	zvát	k5eAaImIp1nP	zvát
grafitace	grafitace	k1gFnPc1	grafitace
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Grafit	grafit	k1gInSc1	grafit
<g/>
.	.	kIx.	.
</s>
<s>
Grafit	grafit	k1gInSc1	grafit
neboli	neboli	k8xC	neboli
tuha	tuha	k1gFnSc1	tuha
je	být	k5eAaImIp3nS	být
nerost	nerost	k1gInSc4	nerost
neboli	neboli	k8xC	neboli
minerál	minerál	k1gInSc4	minerál
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
lokalitách	lokalita	k1gFnPc6	lokalita
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Jedny	jeden	k4xCgInPc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgInPc2d3	veliký
grafitových	grafitový	k2eAgInPc2d1	grafitový
dolů	dol	k1gInPc2	dol
se	se	k3xPyFc4	se
nalézají	nalézat	k5eAaImIp3nP	nalézat
v	v	k7c6	v
USA	USA	kA	USA
(	(	kIx(	(
<g/>
Texas	Texas	k1gInSc1	Texas
a	a	k8xC	a
stát	stát	k1gInSc1	stát
New	New	k1gFnPc2	New
York	York	k1gInSc1	York
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mexiku	Mexiko	k1gNnSc6	Mexiko
<g/>
,	,	kIx,	,
Indii	Indie	k1gFnSc6	Indie
a	a	k8xC	a
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
;	;	kIx,	;
významná	významný	k2eAgFnSc1d1	významná
byla	být	k5eAaImAgFnS	být
i	i	k9	i
ložiska	ložisko	k1gNnSc2	ložisko
v	v	k7c6	v
jižních	jižní	k2eAgFnPc6d1	jižní
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Grafit	grafit	k1gInSc1	grafit
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
zároveň	zároveň	k6eAd1	zároveň
složkou	složka	k1gFnSc7	složka
sazí	saze	k1gFnPc2	saze
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
vznikají	vznikat	k5eAaImIp3nP	vznikat
spalováním	spalování	k1gNnSc7	spalování
fosilních	fosilní	k2eAgNnPc2d1	fosilní
paliv	palivo	k1gNnPc2	palivo
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
přitom	přitom	k6eAd1	přitom
přítomen	přítomen	k2eAgMnSc1d1	přítomen
v	v	k7c6	v
částečkách	částečka	k1gFnPc6	částečka
natolik	natolik	k6eAd1	natolik
nepatrných	patrný	k2eNgMnPc2d1	nepatrný
rozměrů	rozměr	k1gInPc2	rozměr
<g/>
,	,	kIx,	,
že	že	k8xS	že
saze	saze	k1gFnPc1	saze
mají	mít	k5eAaImIp3nP	mít
spíše	spíše	k9	spíše
vlastnosti	vlastnost	k1gFnPc1	vlastnost
amorfního	amorfní	k2eAgInSc2d1	amorfní
uhlíku	uhlík	k1gInSc2	uhlík
<g/>
.	.	kIx.	.
</s>
<s>
Grafit	grafit	k1gInSc1	grafit
se	se	k3xPyFc4	se
průmyslově	průmyslově	k6eAd1	průmyslově
využívá	využívat	k5eAaImIp3nS	využívat
především	především	k9	především
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
tužek	tužka	k1gFnPc2	tužka
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
se	se	k3xPyFc4	se
nejprve	nejprve	k6eAd1	nejprve
velmi	velmi	k6eAd1	velmi
jemně	jemně	k6eAd1	jemně
namele	namlít	k5eAaPmIp3nS	namlít
společně	společně	k6eAd1	společně
s	s	k7c7	s
vápnem	vápno	k1gNnSc7	vápno
a	a	k8xC	a
vylisuje	vylisovat	k5eAaPmIp3nS	vylisovat
se	se	k3xPyFc4	se
do	do	k7c2	do
vhodného	vhodný	k2eAgInSc2d1	vhodný
tvaru	tvar	k1gInSc2	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
významné	významný	k2eAgNnSc1d1	významné
uplatnění	uplatnění	k1gNnSc1	uplatnění
grafitu	grafit	k1gInSc2	grafit
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
metalurgickém	metalurgický	k2eAgInSc6d1	metalurgický
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
značné	značný	k2eAgFnSc3d1	značná
tepelné	tepelný	k2eAgFnSc3d1	tepelná
odolnosti	odolnost	k1gFnSc3	odolnost
se	se	k3xPyFc4	se
z	z	k7c2	z
něho	on	k3xPp3gMnSc2	on
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
nádoby	nádoba	k1gFnPc1	nádoba
<g/>
,	,	kIx,	,
kokily	kokila	k1gFnPc1	kokila
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterých	který	k3yRgFnPc2	který
se	se	k3xPyFc4	se
odlévají	odlévat	k5eAaImIp3nP	odlévat
roztavené	roztavený	k2eAgInPc1d1	roztavený
kovy	kov	k1gInPc1	kov
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc1	jejich
slitiny	slitina	k1gFnPc1	slitina
<g/>
.	.	kIx.	.
</s>
<s>
Zamezí	zamezit	k5eAaPmIp3nS	zamezit
se	se	k3xPyFc4	se
tak	tak	k9	tak
kontaminaci	kontaminace	k1gFnSc4	kontaminace
slitiny	slitina	k1gFnSc2	slitina
kovem	kov	k1gInSc7	kov
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yQgInSc2	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
kokila	kokila	k1gFnSc1	kokila
musela	muset	k5eAaImAgFnS	muset
vyrobit	vyrobit	k5eAaPmF	vyrobit
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
grafitu	grafit	k1gInSc2	grafit
se	se	k3xPyFc4	se
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
i	i	k9	i
elektrody	elektroda	k1gFnPc4	elektroda
pro	pro	k7c4	pro
elektrolytickou	elektrolytický	k2eAgFnSc4d1	elektrolytická
výrobu	výroba	k1gFnSc4	výroba
hliníku	hliník	k1gInSc2	hliník
z	z	k7c2	z
taveniny	tavenina	k1gFnSc2	tavenina
směsi	směs	k1gFnSc2	směs
bauxitu	bauxit	k1gInSc2	bauxit
a	a	k8xC	a
kryolitu	kryolit	k1gInSc2	kryolit
nebo	nebo	k8xC	nebo
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
křemíku	křemík	k1gInSc2	křemík
z	z	k7c2	z
taveniny	tavenina	k1gFnSc2	tavenina
oxidu	oxid	k1gInSc2	oxid
křemičitého	křemičitý	k2eAgInSc2d1	křemičitý
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
grafitu	grafit	k1gInSc2	grafit
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
speciální	speciální	k2eAgInPc4d1	speciální
typy	typ	k1gInPc4	typ
elektrických	elektrický	k2eAgInPc2d1	elektrický
kontaktů	kontakt	k1gInPc2	kontakt
<g/>
,	,	kIx,	,
např.	např.	kA	např.
kartáčky	kartáček	k1gInPc7	kartáček
elektromotorů	elektromotor	k1gInPc2	elektromotor
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInPc1d1	hlavní
důvody	důvod	k1gInPc1	důvod
pro	pro	k7c4	pro
toto	tento	k3xDgNnSc4	tento
použití	použití	k1gNnSc4	použití
uhlíku	uhlík	k1gInSc2	uhlík
<g/>
:	:	kIx,	:
uhlík	uhlík	k1gInSc1	uhlík
se	se	k3xPyFc4	se
netaví	tavit	k5eNaImIp3nS	tavit
-	-	kIx~	-
kontakty	kontakt	k1gInPc1	kontakt
se	se	k3xPyFc4	se
nespékají	spékat	k5eNaImIp3nP	spékat
a	a	k8xC	a
neopalují	opalovat	k5eNaImIp3nP	opalovat
se	se	k3xPyFc4	se
při	při	k7c6	při
vysokých	vysoký	k2eAgFnPc6d1	vysoká
teplotách	teplota	k1gFnPc6	teplota
<g/>
,	,	kIx,	,
nízký	nízký	k2eAgInSc1d1	nízký
součinitel	součinitel	k1gInSc1	součinitel
tření	tření	k1gNnSc2	tření
<g/>
,	,	kIx,	,
stálý	stálý	k2eAgInSc4d1	stálý
přechodový	přechodový	k2eAgInSc4d1	přechodový
odpor	odpor	k1gInSc4	odpor
<g/>
.	.	kIx.	.
</s>
<s>
Grafitový	grafitový	k2eAgInSc1d1	grafitový
prach	prach	k1gInSc1	prach
se	se	k3xPyFc4	se
využíval	využívat	k5eAaPmAgInS	využívat
do	do	k7c2	do
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
přežitých	přežitý	k2eAgInPc2d1	přežitý
uhlíkových	uhlíkový	k2eAgInPc2d1	uhlíkový
mikrofonů	mikrofon	k1gInPc2	mikrofon
<g/>
.	.	kIx.	.
</s>
<s>
Uhlík	uhlík	k1gInSc1	uhlík
slouží	sloužit	k5eAaImIp3nS	sloužit
také	také	k9	také
jako	jako	k9	jako
součást	součást	k1gFnSc4	součást
maziv	mazivo	k1gNnPc2	mazivo
(	(	kIx(	(
<g/>
grafitová	grafitový	k2eAgFnSc1d1	grafitová
vazelína	vazelína	k1gFnSc1	vazelína
<g/>
,	,	kIx,	,
kolomaz	kolomaz	k1gFnSc1	kolomaz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Uměle	uměle	k6eAd1	uměle
vyrobenou	vyrobený	k2eAgFnSc7d1	vyrobená
formou	forma	k1gFnSc7	forma
uhlíku	uhlík	k1gInSc2	uhlík
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
skelný	skelný	k2eAgInSc4d1	skelný
uhlík	uhlík	k1gInSc4	uhlík
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
glassy	glassa	k1gFnSc2	glassa
carbon	carbona	k1gFnPc2	carbona
<g/>
,	,	kIx,	,
tradičně	tradičně	k6eAd1	tradičně
zvaný	zvaný	k2eAgInSc1d1	zvaný
též	též	k9	též
skelný	skelný	k2eAgInSc1d1	skelný
grafit	grafit	k1gInSc1	grafit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
vysokou	vysoký	k2eAgFnSc7d1	vysoká
hustotou	hustota	k1gFnSc7	hustota
<g/>
,	,	kIx,	,
nízkou	nízký	k2eAgFnSc7d1	nízká
pórovitostí	pórovitost	k1gFnSc7	pórovitost
a	a	k8xC	a
značnou	značný	k2eAgFnSc7d1	značná
chemickou	chemický	k2eAgFnSc7d1	chemická
a	a	k8xC	a
mechanickou	mechanický	k2eAgFnSc7d1	mechanická
odolností	odolnost	k1gFnSc7	odolnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
přesně	přesně	k6eAd1	přesně
řízeným	řízený	k2eAgInSc7d1	řízený
dlouhodobým	dlouhodobý	k2eAgInSc7d1	dlouhodobý
vysokoteplotním	vysokoteplotní	k2eAgInSc7d1	vysokoteplotní
(	(	kIx(	(
<g/>
pyrrolitickým	pyrrolitický	k2eAgInSc7d1	pyrrolitický
<g/>
)	)	kIx)	)
rozkladem	rozklad	k1gInSc7	rozklad
organických	organický	k2eAgFnPc2d1	organická
látek	látka	k1gFnPc2	látka
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
normálního	normální	k2eAgInSc2d1	normální
grafitu	grafit	k1gInSc2	grafit
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
mimořádným	mimořádný	k2eAgInPc3d1	mimořádný
fyzikálním	fyzikální	k2eAgInPc3d1	fyzikální
a	a	k8xC	a
chemický	chemický	k2eAgInSc4d1	chemický
vlastnostem	vlastnost	k1gFnPc3	vlastnost
skelného	skelný	k2eAgInSc2d1	skelný
grafitu	grafit	k1gInSc2	grafit
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gNnSc1	jeho
praktické	praktický	k2eAgNnSc1d1	praktické
využití	využití	k1gNnSc1	využití
stále	stále	k6eAd1	stále
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
i	i	k9	i
přes	přes	k7c4	přes
jeho	jeho	k3xOp3gFnSc4	jeho
poměrně	poměrně	k6eAd1	poměrně
vysokou	vysoký	k2eAgFnSc4d1	vysoká
cenu	cena	k1gFnSc4	cena
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
elektrochemii	elektrochemie	k1gFnSc4	elektrochemie
je	být	k5eAaImIp3nS	být
důležitý	důležitý	k2eAgInSc1d1	důležitý
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
povrchy	povrch	k1gInPc4	povrch
elektrod	elektroda	k1gFnPc2	elektroda
ze	z	k7c2	z
skelného	skelný	k2eAgInSc2d1	skelný
grafitu	grafit	k1gInSc2	grafit
jsou	být	k5eAaImIp3nP	být
chemicky	chemicky	k6eAd1	chemicky
vysoce	vysoce	k6eAd1	vysoce
odolné	odolný	k2eAgFnPc1d1	odolná
a	a	k8xC	a
lze	lze	k6eAd1	lze
na	na	k7c6	na
nich	on	k3xPp3gInPc6	on
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
vysokého	vysoký	k2eAgInSc2d1	vysoký
kladného	kladný	k2eAgInSc2d1	kladný
potenciálu	potenciál	k1gInSc2	potenciál
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
rozpuštění	rozpuštění	k1gNnSc3	rozpuštění
jako	jako	k8xC	jako
u	u	k7c2	u
normálních	normální	k2eAgFnPc2d1	normální
kovových	kovový	k2eAgFnPc2d1	kovová
elektrod	elektroda	k1gFnPc2	elektroda
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgMnSc4	ten
lze	lze	k6eAd1	lze
využít	využít	k5eAaPmF	využít
jak	jak	k8xS	jak
v	v	k7c6	v
analytické	analytický	k2eAgFnSc6d1	analytická
chemii	chemie	k1gFnSc6	chemie
při	při	k7c6	při
zkoumání	zkoumání	k1gNnSc6	zkoumání
elektrochemických	elektrochemický	k2eAgFnPc2d1	elektrochemická
vlastností	vlastnost	k1gFnPc2	vlastnost
organických	organický	k2eAgFnPc2d1	organická
molekul	molekula	k1gFnPc2	molekula
tak	tak	k9	tak
pro	pro	k7c4	pro
preparativní	preparativní	k2eAgFnSc4d1	preparativní
oxidaci	oxidace	k1gFnSc4	oxidace
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
některých	některý	k3yIgFnPc2	některý
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Analytická	analytický	k2eAgFnSc1d1	analytická
metoda	metoda	k1gFnSc1	metoda
GFAAS	GFAAS	kA	GFAAS
(	(	kIx(	(
<g/>
atomová	atomový	k2eAgFnSc1d1	atomová
absorpční	absorpční	k2eAgFnSc1d1	absorpční
spektrometrie	spektrometrie	k1gFnSc1	spektrometrie
s	s	k7c7	s
bezplamennou	bezplamenný	k2eAgFnSc7d1	bezplamenný
atomizací	atomizace	k1gFnSc7	atomizace
<g/>
)	)	kIx)	)
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
odpaření	odpaření	k1gNnSc4	odpaření
analyzovaného	analyzovaný	k2eAgInSc2d1	analyzovaný
vzorku	vzorek	k1gInSc2	vzorek
kyvetu	kyveta	k1gFnSc4	kyveta
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
během	během	k7c2	během
několika	několik	k4yIc2	několik
sekund	sekunda	k1gFnPc2	sekunda
zahřívá	zahřívat	k5eAaImIp3nS	zahřívat
až	až	k9	až
na	na	k7c4	na
teploty	teplota	k1gFnPc4	teplota
kolem	kolem	k7c2	kolem
3	[number]	k4	3
000	[number]	k4	000
°	°	k?	°
<g/>
C.	C.	kA	C.
Pokrytí	pokrytí	k1gNnPc4	pokrytí
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
plochy	plocha	k1gFnSc2	plocha
této	tento	k3xDgFnSc2	tento
kyvety	kyveta	k1gFnSc2	kyveta
skelným	skelný	k2eAgInSc7d1	skelný
grafitem	grafit	k1gInSc7	grafit
dramaticky	dramaticky	k6eAd1	dramaticky
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
její	její	k3xOp3gFnSc4	její
odolnost	odolnost	k1gFnSc4	odolnost
a	a	k8xC	a
prodlouží	prodloužit	k5eAaPmIp3nS	prodloužit
její	její	k3xOp3gFnSc4	její
použitelnost	použitelnost	k1gFnSc4	použitelnost
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
klasickou	klasický	k2eAgFnSc7d1	klasická
grafitovou	grafitový	k2eAgFnSc7d1	grafitová
kyvetou	kyveta	k1gFnSc7	kyveta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
metalurgii	metalurgie	k1gFnSc6	metalurgie
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
čištění	čištění	k1gNnSc4	čištění
kovů	kov	k1gInPc2	kov
na	na	k7c4	na
vysoké	vysoký	k2eAgFnPc4d1	vysoká
čistoty	čistota	k1gFnPc4	čistota
metodou	metoda	k1gFnSc7	metoda
zonálního	zonální	k2eAgNnSc2d1	zonální
tavení	tavení	k1gNnSc2	tavení
mohou	moct	k5eAaImIp3nP	moct
uplatnit	uplatnit	k5eAaPmF	uplatnit
trubice	trubice	k1gFnPc4	trubice
pokryté	pokrytý	k2eAgFnPc4d1	pokrytá
sklelným	sklelný	k2eAgInSc7d1	sklelný
grafitem	grafit	k1gInSc7	grafit
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
se	se	k3xPyFc4	se
tavení	tavení	k1gNnPc1	tavení
provádí	provádět	k5eAaImIp3nP	provádět
<g/>
.	.	kIx.	.
</s>
<s>
Laboratorní	laboratorní	k2eAgNnSc1d1	laboratorní
nádobí	nádobí	k1gNnSc1	nádobí
s	s	k7c7	s
povrchem	povrch	k1gInSc7	povrch
ze	z	k7c2	z
skelného	skelný	k2eAgInSc2d1	skelný
grafitu	grafit	k1gInSc2	grafit
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
stejné	stejný	k2eAgNnSc1d1	stejné
nebo	nebo	k8xC	nebo
i	i	k9	i
lepší	dobrý	k2eAgFnSc2d2	lepší
chemické	chemický	k2eAgFnSc2d1	chemická
odolnosti	odolnost	k1gFnSc2	odolnost
jako	jako	k8xC	jako
nádobí	nádobí	k1gNnSc2	nádobí
z	z	k7c2	z
platiny	platina	k1gFnSc2	platina
nebo	nebo	k8xC	nebo
její	její	k3xOp3gFnSc2	její
slitiny	slitina	k1gFnSc2	slitina
s	s	k7c7	s
rhodiem	rhodium	k1gNnSc7	rhodium
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Diamant	diamant	k1gInSc1	diamant
<g/>
.	.	kIx.	.
</s>
<s>
Představuje	představovat	k5eAaImIp3nS	představovat
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejvzácnějších	vzácný	k2eAgMnPc2d3	nejvzácnější
a	a	k8xC	a
nejdražších	drahý	k2eAgMnPc2d3	nejdražší
minerálů	minerál	k1gInPc2	minerál
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
barevných	barevný	k2eAgFnPc6d1	barevná
modifikacích	modifikace	k1gFnPc6	modifikace
od	od	k7c2	od
takřka	takřka	k6eAd1	takřka
průhledné	průhledný	k2eAgFnSc2d1	průhledná
až	až	k9	až
po	po	k7c4	po
černou	černá	k1gFnSc4	černá
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
diamantu	diamant	k1gInSc2	diamant
je	být	k5eAaImIp3nS	být
zapotřebí	zapotřebí	k6eAd1	zapotřebí
obrovských	obrovský	k2eAgInPc2d1	obrovský
tlaků	tlak	k1gInPc2	tlak
a	a	k8xC	a
vysokých	vysoký	k2eAgFnPc2d1	vysoká
teplot	teplota	k1gFnPc2	teplota
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
nalézány	nalézat	k5eAaImNgFnP	nalézat
především	především	k6eAd1	především
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žhavé	žhavý	k2eAgNnSc1d1	žhavé
magma	magma	k1gNnSc1	magma
z	z	k7c2	z
velkých	velký	k2eAgFnPc2d1	velká
hloubek	hloubka	k1gFnPc2	hloubka
vystoupilo	vystoupit	k5eAaPmAgNnS	vystoupit
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
a	a	k8xC	a
ztuhlo	ztuhnout	k5eAaPmAgNnS	ztuhnout
<g/>
.	.	kIx.	.
</s>
<s>
Naleziště	naleziště	k1gNnSc1	naleziště
s	s	k7c7	s
nejkvalitnějšími	kvalitní	k2eAgInPc7d3	nejkvalitnější
diamanty	diamant	k1gInPc7	diamant
leží	ležet	k5eAaImIp3nS	ležet
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
-	-	kIx~	-
JAR	jar	k1gFnSc1	jar
<g/>
,	,	kIx,	,
Namibie	Namibie	k1gFnSc1	Namibie
<g/>
,	,	kIx,	,
Sierra	Sierra	k1gFnSc1	Sierra
Leone	Leo	k1gMnSc5	Leo
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
<g/>
,	,	kIx,	,
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
Kanadě	Kanada	k1gFnSc6	Kanada
a	a	k8xC	a
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Diamanty	diamant	k1gInPc4	diamant
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
možno	možno	k6eAd1	možno
vyrábět	vyrábět	k5eAaImF	vyrábět
i	i	k9	i
průmyslově	průmyslově	k6eAd1	průmyslově
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
produkty	produkt	k1gInPc1	produkt
zdaleka	zdaleka	k6eAd1	zdaleka
nedosahují	dosahovat	k5eNaImIp3nP	dosahovat
kvalit	kvalita	k1gFnPc2	kvalita
přírodních	přírodní	k2eAgInPc2d1	přírodní
diamantů	diamant	k1gInPc2	diamant
<g/>
.	.	kIx.	.
</s>
<s>
Průmyslové	průmyslový	k2eAgInPc1d1	průmyslový
diamanty	diamant	k1gInPc1	diamant
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
využívají	využívat	k5eAaImIp3nP	využívat
především	především	k6eAd1	především
k	k	k7c3	k
osazování	osazování	k1gNnSc3	osazování
různých	různý	k2eAgFnPc2d1	různá
vrtných	vrtný	k2eAgFnPc2d1	vrtná
a	a	k8xC	a
řezných	řezný	k2eAgFnPc2d1	řezná
hlavic	hlavice	k1gFnPc2	hlavice
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
činnost	činnost	k1gFnSc4	činnost
musí	muset	k5eAaImIp3nS	muset
vykazovat	vykazovat	k5eAaImF	vykazovat
mimořádnou	mimořádný	k2eAgFnSc4d1	mimořádná
tvrdost	tvrdost	k1gFnSc4	tvrdost
a	a	k8xC	a
odolnost	odolnost	k1gFnSc4	odolnost
<g/>
.	.	kIx.	.
</s>
<s>
Přírodní	přírodní	k2eAgInPc1d1	přírodní
diamanty	diamant	k1gInPc1	diamant
slouží	sloužit	k5eAaImIp3nP	sloužit
již	již	k6eAd1	již
od	od	k7c2	od
pradávna	pradávno	k1gNnSc2	pradávno
především	především	k6eAd1	především
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
těch	ten	k3xDgInPc2	ten
nejdražších	drahý	k2eAgInPc2d3	nejdražší
šperků	šperk	k1gInPc2	šperk
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
diamant	diamant	k1gInSc1	diamant
zasadit	zasadit	k5eAaPmF	zasadit
do	do	k7c2	do
zlatého	zlatý	k2eAgInSc2d1	zlatý
nebo	nebo	k8xC	nebo
platinového	platinový	k2eAgInSc2d1	platinový
šperku	šperk	k1gInSc2	šperk
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
nejprve	nejprve	k6eAd1	nejprve
složitě	složitě	k6eAd1	složitě
a	a	k8xC	a
pečlivě	pečlivě	k6eAd1	pečlivě
broušen	broušen	k2eAgInSc1d1	broušen
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
úspěšnému	úspěšný	k2eAgNnSc3d1	úspěšné
vybroušení	vybroušení	k1gNnSc3	vybroušení
drahého	drahý	k2eAgInSc2d1	drahý
a	a	k8xC	a
vzácného	vzácný	k2eAgInSc2d1	vzácný
diamantu	diamant	k1gInSc2	diamant
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
nejen	nejen	k6eAd1	nejen
značné	značný	k2eAgFnPc4d1	značná
zkušenosti	zkušenost	k1gFnPc4	zkušenost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
zručnosti	zručnost	k1gFnSc2	zručnost
a	a	k8xC	a
trpělivosti	trpělivost	k1gFnSc2	trpělivost
<g/>
.	.	kIx.	.
</s>
<s>
Středisky	středisko	k1gNnPc7	středisko
broušení	broušení	k1gNnSc3	broušení
diamantů	diamant	k1gInPc2	diamant
a	a	k8xC	a
obchodu	obchod	k1gInSc2	obchod
s	s	k7c7	s
nimi	on	k3xPp3gFnPc7	on
jsou	být	k5eAaImIp3nP	být
belgické	belgický	k2eAgFnPc1d1	belgická
Antverpy	Antverpy	k1gFnPc1	Antverpy
a	a	k8xC	a
nizozemský	nizozemský	k2eAgInSc1d1	nizozemský
Rotterdam	Rotterdam	k1gInSc1	Rotterdam
a	a	k8xC	a
Amsterodam	Amsterodam	k1gInSc1	Amsterodam
<g/>
.	.	kIx.	.
</s>
<s>
Uhlík	uhlík	k1gInSc1	uhlík
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
sloučeniny	sloučenina	k1gFnPc1	sloučenina
hrály	hrát	k5eAaImAgFnP	hrát
zcela	zcela	k6eAd1	zcela
zásadní	zásadní	k2eAgFnSc4d1	zásadní
roli	role	k1gFnSc4	role
ve	v	k7c6	v
vzniku	vznik	k1gInSc6	vznik
života	život	k1gInSc2	život
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
ho	on	k3xPp3gMnSc4	on
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
známe	znát	k5eAaImIp1nP	znát
<g/>
.	.	kIx.	.
</s>
<s>
Těla	tělo	k1gNnSc2	tělo
všech	všecek	k3xTgInPc2	všecek
organismů	organismus	k1gInPc2	organismus
jsou	být	k5eAaImIp3nP	být
složená	složený	k2eAgFnSc1d1	složená
právě	právě	k6eAd1	právě
především	především	k9	především
z	z	k7c2	z
organických	organický	k2eAgFnPc2d1	organická
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
látek	látka	k1gFnPc2	látka
obsahujících	obsahující	k2eAgFnPc2d1	obsahující
uhlík	uhlík	k1gInSc4	uhlík
<g/>
.	.	kIx.	.
</s>
<s>
Cukry	cukr	k1gInPc1	cukr
<g/>
,	,	kIx,	,
tuky	tuk	k1gInPc1	tuk
<g/>
,	,	kIx,	,
různé	různý	k2eAgFnPc1d1	různá
kyseliny	kyselina	k1gFnPc1	kyselina
včetně	včetně	k7c2	včetně
aminokyselin	aminokyselina	k1gFnPc2	aminokyselina
a	a	k8xC	a
nukleových	nukleový	k2eAgFnPc2d1	nukleová
kyselin	kyselina	k1gFnPc2	kyselina
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgFnPc4	všechen
myslitelné	myslitelný	k2eAgFnPc4d1	myslitelná
organické	organický	k2eAgFnPc4d1	organická
látky	látka	k1gFnPc4	látka
v	v	k7c6	v
tělech	tělo	k1gNnPc6	tělo
pozemských	pozemský	k2eAgMnPc2d1	pozemský
organismů	organismus	k1gInPc2	organismus
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
uhlík	uhlík	k1gInSc4	uhlík
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lidském	lidský	k2eAgNnSc6d1	lidské
těle	tělo	k1gNnSc6	tělo
uhlík	uhlík	k1gInSc4	uhlík
představuje	představovat	k5eAaImIp3nS	představovat
18,5	[number]	k4	18,5
%	%	kIx~	%
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
,	,	kIx,	,
V	v	k7c6	v
sušině	sušina	k1gFnSc6	sušina
těla	tělo	k1gNnSc2	tělo
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
vody	voda	k1gFnSc2	voda
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
ještě	ještě	k9	ještě
mnohem	mnohem	k6eAd1	mnohem
více	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
uhlík	uhlík	k1gInSc1	uhlík
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
izotopů	izotop	k1gInPc2	izotop
12	[number]	k4	12
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
13C	[number]	k4	13C
a	a	k8xC	a
14	[number]	k4	14
<g/>
C.	C.	kA	C.
Jejich	jejich	k3xOp3gInSc4	jejich
vlastnosti	vlastnost	k1gFnPc1	vlastnost
včetně	včetně	k7c2	včetně
počtu	počet	k1gInSc2	počet
protonů	proton	k1gInPc2	proton
a	a	k8xC	a
neutronů	neutron	k1gInPc2	neutron
v	v	k7c6	v
jejich	jejich	k3xOp3gNnSc6	jejich
jádře	jádro	k1gNnSc6	jádro
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
tabulka	tabulka	k1gFnSc1	tabulka
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
těchto	tento	k3xDgInPc2	tento
přírodních	přírodní	k2eAgInPc2d1	přírodní
izotopů	izotop	k1gInPc2	izotop
existuje	existovat	k5eAaImIp3nS	existovat
přes	přes	k7c4	přes
deset	deset	k4xCc4	deset
dalších	další	k2eAgInPc2d1	další
uměle	uměle	k6eAd1	uměle
připravených	připravený	k2eAgInPc2d1	připravený
izotopů	izotop	k1gInPc2	izotop
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
nejstabilnější	stabilní	k2eAgInPc4d3	nejstabilnější
je	být	k5eAaImIp3nS	být
izotop	izotop	k1gInSc4	izotop
11C	[number]	k4	11C
s	s	k7c7	s
poločasem	poločas	k1gInSc7	poločas
rozpadu	rozpad	k1gInSc2	rozpad
20,3	[number]	k4	20,3
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
přírodní	přírodní	k2eAgInSc1d1	přírodní
radioaktivní	radioaktivní	k2eAgInSc1d1	radioaktivní
izotop	izotop	k1gInSc1	izotop
uhlíku	uhlík	k1gInSc2	uhlík
vzniká	vznikat	k5eAaImIp3nS	vznikat
ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
vrstvách	vrstva	k1gFnPc6	vrstva
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Primárním	primární	k2eAgInSc7d1	primární
činitelem	činitel	k1gInSc7	činitel
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
částice	částice	k1gFnSc1	částice
kosmického	kosmický	k2eAgNnSc2d1	kosmické
záření	záření	k1gNnSc2	záření
tvořené	tvořený	k2eAgNnSc4d1	tvořené
převážně	převážně	k6eAd1	převážně
protony	proton	k1gInPc7	proton
a	a	k8xC	a
jádry	jádro	k1gNnPc7	jádro
hélia	hélium	k1gNnSc2	hélium
(	(	kIx(	(
<g/>
α	α	k?	α
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
ve	v	k7c6	v
vysokých	vysoký	k2eAgFnPc6d1	vysoká
vrstvách	vrstva	k1gFnPc6	vrstva
atmosféry	atmosféra	k1gFnSc2	atmosféra
bombardují	bombardovat	k5eAaImIp3nP	bombardovat
zde	zde	k6eAd1	zde
přítomné	přítomný	k2eAgInPc1d1	přítomný
atomy	atom	k1gInPc1	atom
a	a	k8xC	a
molekuly	molekula	k1gFnPc1	molekula
(	(	kIx(	(
<g/>
především	především	k9	především
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
,	,	kIx,	,
dusíku	dusík	k1gInSc2	dusík
a	a	k8xC	a
vzácných	vzácný	k2eAgInPc2d1	vzácný
plynů	plyn	k1gInPc2	plyn
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
vzniká	vznikat	k5eAaImIp3nS	vznikat
sekundární	sekundární	k2eAgNnSc1d1	sekundární
kosmické	kosmický	k2eAgNnSc1d1	kosmické
záření	záření	k1gNnSc1	záření
<g/>
:	:	kIx,	:
elementární	elementární	k2eAgFnPc1d1	elementární
částice	částice	k1gFnPc1	částice
neutrony	neutron	k1gInPc4	neutron
<g/>
,	,	kIx,	,
positrony	positron	k1gInPc4	positron
<g/>
,	,	kIx,	,
miony	mion	k1gInPc4	mion
a	a	k8xC	a
piony	pion	k1gInPc4	pion
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
neutrony	neutron	k1gInPc1	neutron
pronikají	pronikat	k5eAaImIp3nP	pronikat
níže	nízce	k6eAd2	nízce
do	do	k7c2	do
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
převážně	převážně	k6eAd1	převážně
ve	v	k7c6	v
výškách	výška	k1gFnPc6	výška
9	[number]	k4	9
-	-	kIx~	-
15	[number]	k4	15
km	km	kA	km
reagují	reagovat	k5eAaBmIp3nP	reagovat
mj.	mj.	kA	mj.
s	s	k7c7	s
atomy	atom	k1gInPc1	atom
dusíku	dusík	k1gInSc2	dusík
14	[number]	k4	14
7	[number]	k4	7
N	N	kA	N
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
vzniká	vznikat	k5eAaImIp3nS	vznikat
izotop	izotop	k1gInSc1	izotop
uhlíku	uhlík	k1gInSc2	uhlík
14	[number]	k4	14
<g/>
C.	C.	kA	C.
Tuto	tento	k3xDgFnSc4	tento
reakci	reakce	k1gFnSc4	reakce
lze	lze	k6eAd1	lze
zapsat	zapsat	k5eAaPmF	zapsat
rovnicí	rovnice	k1gFnSc7	rovnice
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
7	[number]	k4	7
:	:	kIx,	:
:	:	kIx,	:
14	[number]	k4	14
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
N	N	kA	N
:	:	kIx,	:
+	+	kIx~	+
n	n	k0	n
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
6	[number]	k4	6
:	:	kIx,	:
:	:	kIx,	:
14	[number]	k4	14
:	:	kIx,	:
:	:	kIx,	:
C	C	kA	C
:	:	kIx,	:
+	+	kIx~	+
p	p	k?	p
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc4	mathrm
{	{	kIx(	{
<g/>
~	~	kIx~	~
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
7	[number]	k4	7
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
14	[number]	k4	14
<g/>
}	}	kIx)	}
<g/>
N	N	kA	N
<g/>
}	}	kIx)	}
+	+	kIx~	+
<g/>
n	n	k0	n
<g/>
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
~	~	kIx~	~
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
6	[number]	k4	6
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
14	[number]	k4	14
<g/>
}	}	kIx)	}
<g/>
C	C	kA	C
<g/>
}	}	kIx)	}
+	+	kIx~	+
<g/>
p	p	k?	p
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
vzniklý	vzniklý	k2eAgInSc1d1	vzniklý
uhlík	uhlík	k1gInSc1	uhlík
zde	zde	k6eAd1	zde
reaguje	reagovat	k5eAaBmIp3nS	reagovat
s	s	k7c7	s
přítomným	přítomný	k2eAgInSc7d1	přítomný
kyslíkem	kyslík	k1gInSc7	kyslík
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
promíchává	promíchávat	k5eAaImIp3nS	promíchávat
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
dostává	dostávat	k5eAaImIp3nS	dostávat
až	až	k9	až
k	k	k7c3	k
zemskému	zemský	k2eAgInSc3d1	zemský
povrchu	povrch	k1gInSc3	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Uhlík	uhlík	k1gInSc1	uhlík
14C	[number]	k4	14C
(	(	kIx(	(
<g/>
ať	ať	k8xS	ať
v	v	k7c6	v
atomické	atomický	k2eAgFnSc6d1	atomická
formě	forma	k1gFnSc6	forma
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
sloučeninách	sloučenina	k1gFnPc6	sloučenina
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
rozpadá	rozpadat	k5eAaImIp3nS	rozpadat
beta	beta	k1gNnSc1	beta
rozpadem	rozpad	k1gInSc7	rozpad
s	s	k7c7	s
poločasem	poločas	k1gInSc7	poločas
5	[number]	k4	5
730	[number]	k4	730
let	léto	k1gNnPc2	léto
na	na	k7c4	na
dusík	dusík	k1gInSc4	dusík
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
je	být	k5eAaImIp3nS	být
produkován	produkovat	k5eAaImNgInS	produkovat
elektron	elektron	k1gInSc1	elektron
a	a	k8xC	a
antineutrino	antineutrino	k1gNnSc1	antineutrino
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
6	[number]	k4	6
:	:	kIx,	:
:	:	kIx,	:
14	[number]	k4	14
:	:	kIx,	:
:	:	kIx,	:
C	C	kA	C
:	:	kIx,	:
→	→	k?	→
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
7	[number]	k4	7
:	:	kIx,	:
:	:	kIx,	:
14	[number]	k4	14
:	:	kIx,	:
:	:	kIx,	:
N	N	kA	N
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
-	-	kIx~	-
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ν	ν	k?	ν
̄	̄	k?	̄
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc4	mathrm
{	{	kIx(	{
<g/>
~	~	kIx~	~
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
6	[number]	k4	6
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
14	[number]	k4	14
<g/>
}	}	kIx)	}
<g/>
C	C	kA	C
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
~	~	kIx~	~
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
7	[number]	k4	7
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
14	[number]	k4	14
<g/>
}	}	kIx)	}
<g/>
N	N	kA	N
<g/>
}	}	kIx)	}
+	+	kIx~	+
<g/>
e	e	k0	e
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
bar	bar	k1gInSc1	bar
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
nu	nu	k9	nu
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}}	}}	k?	}}
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
intenzita	intenzita	k1gFnSc1	intenzita
kosmického	kosmický	k2eAgNnSc2d1	kosmické
záření	záření	k1gNnSc2	záření
je	být	k5eAaImIp3nS	být
relativně	relativně	k6eAd1	relativně
stabilní	stabilní	k2eAgFnSc1d1	stabilní
<g/>
,	,	kIx,	,
ustavila	ustavit	k5eAaPmAgFnS	ustavit
se	se	k3xPyFc4	se
v	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
atmosféře	atmosféra	k1gFnSc6	atmosféra
rovnováha	rovnováha	k1gFnSc1	rovnováha
mezi	mezi	k7c7	mezi
produkcí	produkce	k1gFnSc7	produkce
uhlíku	uhlík	k1gInSc2	uhlík
14C	[number]	k4	14C
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc7	jeho
radioaktivním	radioaktivní	k2eAgInSc7d1	radioaktivní
rozpadem	rozpad	k1gInSc7	rozpad
<g/>
.	.	kIx.	.
</s>
<s>
Poměr	poměr	k1gInSc1	poměr
všech	všecek	k3xTgInPc2	všecek
3	[number]	k4	3
izotopů	izotop	k1gInPc2	izotop
uhlíku	uhlík	k1gInSc2	uhlík
v	v	k7c6	v
atmosférickém	atmosférický	k2eAgInSc6d1	atmosférický
oxidu	oxid	k1gInSc6	oxid
uhličitém	uhličitý	k2eAgInSc6d1	uhličitý
i	i	k8xC	i
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
sloučeninách	sloučenina	k1gFnPc6	sloučenina
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
udržuje	udržovat	k5eAaImIp3nS	udržovat
na	na	k7c6	na
konstantní	konstantní	k2eAgFnSc6d1	konstantní
hodnotě	hodnota	k1gFnSc6	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Radiokarbonová	radiokarbonový	k2eAgFnSc1d1	radiokarbonová
metoda	metoda	k1gFnSc1	metoda
datování	datování	k1gNnSc2	datování
<g/>
.	.	kIx.	.
</s>
<s>
Princip	princip	k1gInSc1	princip
této	tento	k3xDgFnSc2	tento
metody	metoda	k1gFnSc2	metoda
zjišťování	zjišťování	k1gNnSc2	zjišťování
stáří	stáří	k1gNnSc2	stáří
materiálu	materiál	k1gInSc2	materiál
vychází	vycházet	k5eAaImIp3nS	vycházet
ze	z	k7c2	z
skutečnosti	skutečnost	k1gFnSc2	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
organismy	organismus	k1gInPc1	organismus
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
průběžně	průběžně	k6eAd1	průběžně
přijímají	přijímat	k5eAaImIp3nP	přijímat
a	a	k8xC	a
vydávají	vydávat	k5eAaPmIp3nP	vydávat
atmosférický	atmosférický	k2eAgMnSc1d1	atmosférický
CO2	CO2	k1gMnPc1	CO2
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc7	jeho
produkty	produkt	k1gInPc7	produkt
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
rostlin	rostlina	k1gFnPc2	rostlina
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
děje	dít	k5eAaImIp3nS	dít
především	především	k9	především
fotosyntézou	fotosyntéza	k1gFnSc7	fotosyntéza
<g/>
,	,	kIx,	,
u	u	k7c2	u
živočichů	živočich	k1gMnPc2	živočich
příjmem	příjem	k1gInSc7	příjem
rostlinných	rostlinný	k2eAgInPc2d1	rostlinný
produktů	produkt	k1gInPc2	produkt
-	-	kIx~	-
býložravci	býložravec	k1gMnPc1	býložravec
a	a	k8xC	a
následně	následně	k6eAd1	následně
predátoři	predátor	k1gMnPc1	predátor
<g/>
.	.	kIx.	.
</s>
<s>
Poměr	poměr	k1gInSc1	poměr
14	[number]	k4	14
<g/>
C	C	kA	C
<g/>
/	/	kIx~	/
<g/>
12	[number]	k4	12
<g/>
C	C	kA	C
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
proto	proto	k8xC	proto
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
života	život	k1gInSc2	život
daného	daný	k2eAgInSc2d1	daný
organizmu	organizmus	k1gInSc2	organizmus
konstantní	konstantní	k2eAgFnSc1d1	konstantní
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
organismu	organismus	k1gInSc2	organismus
nebo	nebo	k8xC	nebo
odumření	odumření	k1gNnSc4	odumření
jakékoliv	jakýkoliv	k3yIgFnSc2	jakýkoliv
biologické	biologický	k2eAgFnSc2d1	biologická
tkáně	tkáň	k1gFnSc2	tkáň
se	se	k3xPyFc4	se
výměna	výměna	k1gFnSc1	výměna
uhlíku	uhlík	k1gInSc2	uhlík
mezi	mezi	k7c7	mezi
organismem	organismus	k1gInSc7	organismus
a	a	k8xC	a
prostředím	prostředí	k1gNnSc7	prostředí
zastaví	zastavit	k5eAaPmIp3nP	zastavit
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
v	v	k7c6	v
organismu	organismus	k1gInSc6	organismus
další	další	k2eAgInSc4d1	další
radioaktivní	radioaktivní	k2eAgInSc4d1	radioaktivní
uhlík	uhlík	k1gInSc4	uhlík
14C	[number]	k4	14C
nevzniká	vznikat	k5eNaImIp3nS	vznikat
<g/>
,	,	kIx,	,
klesá	klesat	k5eAaImIp3nS	klesat
jeho	jeho	k3xOp3gFnSc1	jeho
koncentrace	koncentrace	k1gFnSc1	koncentrace
podle	podle	k7c2	podle
zákona	zákon	k1gInSc2	zákon
radioaktivního	radioaktivní	k2eAgInSc2d1	radioaktivní
rozpadu	rozpad	k1gInSc2	rozpad
<g/>
.	.	kIx.	.
</s>
<s>
Radiokarbonová	radiokarbonový	k2eAgFnSc1d1	radiokarbonová
metoda	metoda	k1gFnSc1	metoda
datování	datování	k1gNnSc2	datování
využívá	využívat	k5eAaImIp3nS	využívat
tohoto	tento	k3xDgInSc2	tento
jevu	jev	k1gInSc2	jev
a	a	k8xC	a
v	v	k7c6	v
archeologickém	archeologický	k2eAgInSc6d1	archeologický
či	či	k8xC	či
jiném	jiný	k2eAgInSc6d1	jiný
nálezu	nález	k1gInSc6	nález
pozůstatku	pozůstatek	k1gInSc6	pozůstatek
živé	živý	k2eAgFnSc2d1	živá
hmoty	hmota	k1gFnSc2	hmota
(	(	kIx(	(
<g/>
zbytky	zbytek	k1gInPc1	zbytek
tkání	tkáň	k1gFnPc2	tkáň
<g/>
,	,	kIx,	,
kosti	kost	k1gFnPc1	kost
<g/>
,	,	kIx,	,
popel	popel	k1gInSc1	popel
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
analyzován	analyzovat	k5eAaImNgInS	analyzovat
poměr	poměr	k1gInSc1	poměr
14	[number]	k4	14
<g/>
C	C	kA	C
<g/>
/	/	kIx~	/
<g/>
12	[number]	k4	12
<g/>
C.	C.	kA	C.
Zjištěný	zjištěný	k2eAgInSc4d1	zjištěný
poměr	poměr	k1gInSc4	poměr
pak	pak	k6eAd1	pak
poměrně	poměrně	k6eAd1	poměrně
přesně	přesně	k6eAd1	přesně
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
na	na	k7c4	na
dobu	doba	k1gFnSc4	doba
zániku	zánik	k1gInSc2	zánik
dané	daný	k2eAgFnSc2d1	daná
živé	živý	k2eAgFnSc2d1	živá
hmoty	hmota	k1gFnSc2	hmota
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
uvedenému	uvedený	k2eAgInSc3d1	uvedený
poločasu	poločas	k1gInSc3	poločas
rozpadu	rozpad	k1gInSc2	rozpad
uhlíku	uhlík	k1gInSc2	uhlík
14C	[number]	k4	14C
je	být	k5eAaImIp3nS	být
metoda	metoda	k1gFnSc1	metoda
optimálně	optimálně	k6eAd1	optimálně
použitelná	použitelný	k2eAgFnSc1d1	použitelná
pro	pro	k7c4	pro
objekty	objekt	k1gInPc4	objekt
o	o	k7c6	o
stáří	stáří	k1gNnSc6	stáří
několika	několik	k4yIc2	několik
set	sto	k4xCgNnPc2	sto
až	až	k9	až
50	[number]	k4	50
tisíc	tisíc	k4xCgInPc2	tisíc
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Cotton	Cotton	k1gInSc1	Cotton
F.	F.	kA	F.
<g/>
A.	A.	kA	A.
<g/>
,	,	kIx,	,
Wilkinson	Wilkinson	k1gMnSc1	Wilkinson
J.	J.	kA	J.
<g/>
:	:	kIx,	:
<g/>
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
souborné	souborný	k2eAgNnSc1d1	souborné
zpracování	zpracování	k1gNnSc1	zpracování
pro	pro	k7c4	pro
pokročilé	pokročilý	k1gMnPc4	pokročilý
<g/>
,	,	kIx,	,
ACADEMIA	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1973	[number]	k4	1973
Holzbecher	Holzbechra	k1gFnPc2	Holzbechra
Z.	Z.	kA	Z.
<g/>
:	:	kIx,	:
<g/>
Analytická	analytický	k2eAgFnSc1d1	analytická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
SNTL	SNTL	kA	SNTL
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1974	[number]	k4	1974
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Heinrich	Heinrich	k1gMnSc1	Heinrich
Remy	remy	k1gNnSc2	remy
<g/>
,	,	kIx,	,
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
1	[number]	k4	1
<g />
.	.	kIx.	.
</s>
<s>
<g/>
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1961	[number]	k4	1961
N.	N.	kA	N.
N.	N.	kA	N.
Greenwood	Greenwooda	k1gFnPc2	Greenwooda
-	-	kIx~	-
A.	A.	kA	A.
Earnshaw	Earnshaw	k1gFnSc1	Earnshaw
<g/>
,	,	kIx,	,
Chemie	chemie	k1gFnSc1	chemie
prvků	prvek	k1gInPc2	prvek
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1993	[number]	k4	1993
ISBN	ISBN	kA	ISBN
80-85427-38-9	[number]	k4	80-85427-38-9
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
uhlík	uhlík	k1gInSc4	uhlík
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc1	Wikimedium
Commons	Commons	k1gInSc4	Commons
Slovníkové	slovníkový	k2eAgNnSc4d1	slovníkové
heslo	heslo	k1gNnSc4	heslo
uhlík	uhlík	k1gInSc4	uhlík
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Chemický	chemický	k2eAgInSc1d1	chemický
vzdělávací	vzdělávací	k2eAgInSc1d1	vzdělávací
portál	portál	k1gInSc1	portál
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Carbon	Carbon	k1gInSc4	Carbon
on	on	k3xPp3gMnSc1	on
Britannica	Britannica	k1gMnSc1	Britannica
Carbon	Carbon	k1gMnSc1	Carbon
-	-	kIx~	-
Super	super	k2eAgInSc1d1	super
Stuff	Stuff	k1gInSc1	Stuff
<g/>
.	.	kIx.	.
</s>
<s>
Animation	Animation	k1gInSc1	Animation
with	with	k1gMnSc1	with
sound	sound	k1gMnSc1	sound
and	and	k?	and
interactive	interactiv	k1gInSc5	interactiv
3	[number]	k4	3
<g/>
D-models	Dodelsa	k1gFnPc2	D-modelsa
<g/>
.	.	kIx.	.
</s>
