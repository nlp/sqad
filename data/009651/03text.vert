<p>
<s>
Jicchak	Jicchak	k1gInSc1	Jicchak
Jicchaki	Jicchak	k1gFnSc2	Jicchak
(	(	kIx(	(
<g/>
hebrejsky	hebrejsky	k6eAd1	hebrejsky
<g/>
:	:	kIx,	:
י	י	k?	י
י	י	k?	י
<g/>
,	,	kIx,	,
žil	žít	k5eAaImAgMnS	žít
26	[number]	k4	26
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1936	[number]	k4	1936
–	–	k?	–
19	[number]	k4	19
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
izraelský	izraelský	k2eAgMnSc1d1	izraelský
politik	politik	k1gMnSc1	politik
a	a	k8xC	a
poslanec	poslanec	k1gMnSc1	poslanec
Knesetu	Kneset	k1gInSc2	Kneset
za	za	k7c4	za
strany	strana	k1gFnPc4	strana
Šlomcijon	Šlomcijona	k1gFnPc2	Šlomcijona
<g/>
,	,	kIx,	,
Likud	Likuda	k1gFnPc2	Likuda
a	a	k8xC	a
Jisra	Jisro	k1gNnSc2	Jisro
<g/>
'	'	kIx"	'
<g/>
el	ela	k1gFnPc2	ela
Achat	achat	k5eAaImF	achat
</s>
</p>
<p>
<s>
==	==	k?	==
Biografie	biografie	k1gFnSc2	biografie
==	==	k?	==
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Tiberias	Tiberiasa	k1gFnPc2	Tiberiasa
<g/>
.	.	kIx.	.
</s>
<s>
Vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
psychologii	psychologie	k1gFnSc4	psychologie
a	a	k8xC	a
pedagogiku	pedagogika	k1gFnSc4	pedagogika
na	na	k7c6	na
Bar-Ilanově	Bar-Ilanův	k2eAgFnSc6d1	Bar-Ilanova
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
psycholog	psycholog	k1gMnSc1	psycholog
a	a	k8xC	a
vzdělávací	vzdělávací	k2eAgMnSc1d1	vzdělávací
poradce	poradce	k1gMnSc1	poradce
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
osvědčení	osvědčení	k1gNnSc4	osvědčení
pro	pro	k7c4	pro
výkon	výkon	k1gInSc4	výkon
profese	profes	k1gFnSc2	profes
učitele	učitel	k1gMnPc4	učitel
tělesné	tělesný	k2eAgFnSc2d1	tělesná
výchovy	výchova	k1gFnSc2	výchova
na	na	k7c6	na
základních	základní	k2eAgFnPc6d1	základní
školách	škola	k1gFnPc6	škola
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1961-1963	[number]	k4	1961-1963
byl	být	k5eAaImAgMnS	být
ředitelem	ředitel	k1gMnSc7	ředitel
zvláštní	zvláštní	k2eAgFnSc2d1	zvláštní
školy	škola	k1gFnSc2	škola
v	v	k7c6	v
Tiberiasu	Tiberias	k1gInSc6	Tiberias
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Politická	politický	k2eAgFnSc1d1	politická
dráha	dráha	k1gFnSc1	dráha
==	==	k?	==
</s>
</p>
<p>
<s>
Byl	být	k5eAaImAgMnS	být
aktivní	aktivní	k2eAgMnSc1d1	aktivní
v	v	k7c6	v
hnutí	hnutí	k1gNnSc6	hnutí
Makabi	Makab	k1gFnSc2	Makab
v	v	k7c6	v
Tiberiasu	Tiberias	k1gInSc6	Tiberias
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
se	se	k3xPyFc4	se
připojil	připojit	k5eAaPmAgInS	připojit
ke	k	k7c3	k
straně	strana	k1gFnSc3	strana
Šlomcijon	Šlomcijona	k1gFnPc2	Šlomcijona
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejíž	jejíž	k3xOyRp3gFnSc6	jejíž
kandidátce	kandidátka	k1gFnSc6	kandidátka
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
Knesetu	Kneset	k1gInSc2	Kneset
po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
<g/>
.	.	kIx.	.
</s>
<s>
Nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
do	do	k7c2	do
parlamentního	parlamentní	k2eAgInSc2d1	parlamentní
výboru	výbor	k1gInSc2	výbor
pro	pro	k7c4	pro
vzdělávání	vzdělávání	k1gNnSc4	vzdělávání
a	a	k8xC	a
kulturu	kultura	k1gFnSc4	kultura
a	a	k8xC	a
výboru	výbor	k1gInSc6	výbor
House	house	k1gNnSc1	house
Committee	Committee	k1gInSc1	Committee
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
předsedou	předseda	k1gMnSc7	předseda
výboru	výbor	k1gInSc2	výbor
pro	pro	k7c4	pro
prověření	prověření	k1gNnSc4	prověření
systému	systém	k1gInSc2	systém
základního	základní	k2eAgNnSc2d1	základní
školství	školství	k1gNnSc2	školství
a	a	k8xC	a
školskou	školský	k2eAgFnSc4d1	školská
reformu	reforma	k1gFnSc4	reforma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
funkčního	funkční	k2eAgNnSc2d1	funkční
období	období	k1gNnSc2	období
formace	formace	k1gFnSc2	formace
Šlomcijon	Šlomcijona	k1gFnPc2	Šlomcijona
splynula	splynout	k5eAaPmAgFnS	splynout
se	s	k7c7	s
stranou	strana	k1gFnSc7	strana
Likud	Likuda	k1gFnPc2	Likuda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
čase	čas	k1gInSc6	čas
ale	ale	k8xC	ale
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
Jicchaki	Jicchaki	k1gNnPc2	Jicchaki
z	z	k7c2	z
Likudu	Likud	k1gInSc2	Likud
odešel	odejít	k5eAaPmAgMnS	odejít
a	a	k8xC	a
dočasně	dočasně	k6eAd1	dočasně
byl	být	k5eAaImAgInS	být
nezařazeným	zařazený	k2eNgMnSc7d1	nezařazený
poslancem	poslanec	k1gMnSc7	poslanec
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
vlastní	vlastní	k2eAgFnSc4d1	vlastní
poslaneckou	poslanecký	k2eAgFnSc4d1	Poslanecká
frakci	frakce	k1gFnSc4	frakce
Jisra	Jisr	k1gInSc2	Jisr
<g/>
'	'	kIx"	'
<g/>
el	ela	k1gFnPc2	ela
Achat	achat	k5eAaImF	achat
(	(	kIx(	(
<g/>
Jeden	jeden	k4xCgInSc1	jeden
Izrael	Izrael	k1gInSc1	Izrael
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Strana	strana	k1gFnSc1	strana
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
nezískala	získat	k5eNaPmAgFnS	získat
žádné	žádný	k3yNgInPc4	žádný
mandáty	mandát	k1gInPc4	mandát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Kneset	Kneset	k1gInSc1	Kneset
–	–	k?	–
Jicchak	Jicchak	k1gInSc1	Jicchak
Jicchaki	Jicchak	k1gFnSc2	Jicchak
</s>
</p>
