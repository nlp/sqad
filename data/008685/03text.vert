<p>
<s>
Marek	Marek	k1gMnSc1	Marek
Janáč	Janáč	k1gMnSc1	Janáč
(	(	kIx(	(
<g/>
*	*	kIx~	*
8	[number]	k4	8
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1971	[number]	k4	1971
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
publicista	publicista	k1gMnSc1	publicista
a	a	k8xC	a
dokumentarista	dokumentarista	k1gMnSc1	dokumentarista
<g/>
,	,	kIx,	,
někdejší	někdejší	k2eAgMnSc1d1	někdejší
pracovník	pracovník	k1gMnSc1	pracovník
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozhlas	rozhlas	k1gInSc1	rozhlas
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
rozhlase	rozhlas	k1gInSc6	rozhlas
začal	začít	k5eAaPmAgInS	začít
pracovat	pracovat	k5eAaImF	pracovat
5	[number]	k4	5
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1991	[number]	k4	1991
a	a	k8xC	a
věnoval	věnovat	k5eAaPmAgMnS	věnovat
se	se	k3xPyFc4	se
denní	denní	k2eAgFnSc4d1	denní
režii	režie	k1gFnSc4	režie
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc4d1	následující
rok	rok	k1gInSc4	rok
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
členem	člen	k1gMnSc7	člen
redakce	redakce	k1gFnSc2	redakce
A	A	kA	A
<g/>
–	–	k?	–
<g/>
Z	Z	kA	Z
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
debutoval	debutovat	k5eAaBmAgInS	debutovat
dokumentem	dokument	k1gInSc7	dokument
Bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
nebylo	být	k5eNaImAgNnS	být
<g/>
...	...	k?	...
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
přešel	přejít	k5eAaPmAgMnS	přejít
na	na	k7c6	na
stanici	stanice	k1gFnSc6	stanice
Radiožurnál	radiožurnál	k1gInSc4	radiožurnál
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
setrval	setrvat	k5eAaPmAgInS	setrvat
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
převzal	převzít	k5eAaPmAgInS	převzít
vedení	vedení	k1gNnSc4	vedení
populárně-vědeckého	populárněědecký	k2eAgInSc2d1	populárně-vědecký
magazínu	magazín	k1gInSc2	magazín
Meteor	meteor	k1gInSc1	meteor
<g/>
.	.	kIx.	.
</s>
<s>
Janáčem	Janáč	k1gInSc7	Janáč
vytvořené	vytvořený	k2eAgInPc4d1	vytvořený
příspěvky	příspěvek	k1gInPc1	příspěvek
či	či	k8xC	či
pořady	pořad	k1gInPc1	pořad
sklízely	sklízet	k5eAaImAgInP	sklízet
ocenění	ocenění	k1gNnSc4	ocenění
jak	jak	k8xC	jak
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
ovšem	ovšem	k9	ovšem
z	z	k7c2	z
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
odešel	odejít	k5eAaPmAgMnS	odejít
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
působí	působit	k5eAaImIp3nS	působit
v	v	k7c6	v
redakci	redakce	k1gFnSc6	redakce
vědecko-populárního	vědeckoopulární	k2eAgInSc2d1	vědecko-populární
časopisu	časopis	k1gInSc2	časopis
Vesmír	vesmír	k1gInSc1	vesmír
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Televize	televize	k1gFnSc1	televize
==	==	k?	==
</s>
</p>
<p>
<s>
Již	již	k6eAd1	již
během	během	k7c2	během
rozhlasové	rozhlasový	k2eAgFnSc2d1	rozhlasová
éry	éra	k1gFnSc2	éra
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
také	také	k9	také
s	s	k7c7	s
Českou	český	k2eAgFnSc7d1	Česká
televizí	televize	k1gFnSc7	televize
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
kterou	který	k3yIgFnSc4	který
společně	společně	k6eAd1	společně
s	s	k7c7	s
Pavlem	Pavel	k1gMnSc7	Pavel
Tumlířem	tumlíř	k1gMnSc7	tumlíř
a	a	k8xC	a
Milanem	Milan	k1gMnSc7	Milan
Harvalíkem	Harvalík	k1gMnSc7	Harvalík
připravil	připravit	k5eAaPmAgMnS	připravit
52	[number]	k4	52
dílný	dílný	k2eAgInSc1d1	dílný
seriál	seriál	k1gInSc1	seriál
Divnopis	Divnopis	k1gInSc1	Divnopis
<g/>
.	.	kIx.	.
</s>
<s>
Autorská	autorský	k2eAgFnSc1d1	autorská
spolupráce	spolupráce	k1gFnSc1	spolupráce
s	s	k7c7	s
pořady	pořad	k1gInPc7	pořad
Ta	ten	k3xDgFnSc1	ten
naše	náš	k3xOp1gFnSc1	náš
povaha	povaha	k1gFnSc1	povaha
česká	český	k2eAgFnSc1d1	Česká
<g/>
,	,	kIx,	,
PORT	port	k1gInSc1	port
<g/>
.	.	kIx.	.
</s>
<s>
Režisér	režisér	k1gMnSc1	režisér
dokumentů	dokument	k1gInPc2	dokument
Před	před	k7c7	před
rokem	rok	k1gInSc7	rok
začalo	začít	k5eAaPmAgNnS	začít
pršet	pršet	k5eAaImF	pršet
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Stopaři	stopař	k1gMnSc3	stopař
dinosaurů	dinosaurus	k1gMnPc2	dinosaurus
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
či	či	k8xC	či
Brány	brána	k1gFnPc4	brána
(	(	kIx(	(
<g/>
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Spoluautor	spoluautor	k1gMnSc1	spoluautor
dokumentu	dokument	k1gInSc2	dokument
Těla	tělo	k1gNnSc2	tělo
nevydávat	vydávat	k5eNaImF	vydávat
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
rež	rež	k1gFnSc1	rež
<g/>
.	.	kIx.	.
</s>
<s>
Martin	Martin	k1gMnSc1	Martin
Vadas	Vadas	k1gMnSc1	Vadas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Publikace	publikace	k1gFnSc1	publikace
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
CD	CD	kA	CD
===	===	k?	===
</s>
</p>
<p>
<s>
Autor	autor	k1gMnSc1	autor
CD	CD	kA	CD
Události	událost	k1gFnSc2	událost
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Události	událost	k1gFnPc1	událost
1998	[number]	k4	1998
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2CD	[number]	k4	2CD
Druhá	druhý	k4xOgFnSc1	druhý
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Osmdesát	osmdesát	k4xCc4	osmdesát
v	v	k7c6	v
hodině	hodina	k1gFnSc6	hodina
(	(	kIx(	(
<g/>
dokument	dokument	k1gInSc1	dokument
k	k	k7c3	k
80	[number]	k4	80
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc2	výročí
rozhlasu	rozhlas	k1gInSc2	rozhlas
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Druhá	druhý	k4xOgFnSc1	druhý
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
(	(	kIx(	(
<g/>
s	s	k7c7	s
interaktivní	interaktivní	k2eAgFnSc7d1	interaktivní
mapou	mapa	k1gFnSc7	mapa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1968-1969	[number]	k4	1968-1969
(	(	kIx(	(
<g/>
CD	CD	kA	CD
věnované	věnovaný	k2eAgFnSc6d1	věnovaná
invazi	invaze	k1gFnSc6	invaze
do	do	k7c2	do
Československa	Československo	k1gNnSc2	Československo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
(	(	kIx(	(
<g/>
CD	CD	kA	CD
věnované	věnovaný	k2eAgNnSc1d1	věnované
pádu	pád	k1gInSc3	pád
komunismu	komunismus	k1gInSc2	komunismus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přispěvatel	přispěvatel	k1gMnSc1	přispěvatel
na	na	k7c4	na
CD	CD	kA	CD
Události	událost	k1gFnSc2	událost
1999	[number]	k4	1999
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Knihy	kniha	k1gFnSc2	kniha
===	===	k?	===
</s>
</p>
<p>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
Pavlem	Pavel	k1gMnSc7	Pavel
Tumlířem	tumlíř	k1gMnSc7	tumlíř
a	a	k8xC	a
Milanem	Milan	k1gMnSc7	Milan
Harvalíkem	Harvalík	k1gMnSc7	Harvalík
napsal	napsat	k5eAaBmAgMnS	napsat
knihy	kniha	k1gFnSc2	kniha
Divnopis	Divnopis	k1gInSc1	Divnopis
I	I	kA	I
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
a	a	k8xC	a
Divnopis	Divnopis	k1gInSc1	Divnopis
II	II	kA	II
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mapující	mapující	k2eAgInSc4d1	mapující
původ	původ	k1gInSc4	původ
názvů	název	k1gInPc2	název
některých	některý	k3yIgFnPc2	některý
obcí	obec	k1gFnPc2	obec
a	a	k8xC	a
měst	město	k1gNnPc2	město
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ostatní	ostatní	k2eAgInPc1d1	ostatní
projekty	projekt	k1gInPc1	projekt
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
založil	založit	k5eAaPmAgMnS	založit
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
se	se	k3xPyFc4	se
S.	S.	kA	S.
Hrzalem	Hrzal	k1gMnSc7	Hrzal
<g/>
)	)	kIx)	)
a	a	k8xC	a
zorganizoval	zorganizovat	k5eAaPmAgMnS	zorganizovat
soutěž	soutěž	k1gFnSc4	soutěž
pro	pro	k7c4	pro
školní	školní	k2eAgFnPc4d1	školní
děti	dítě	k1gFnPc4	dítě
mezi	mezi	k7c7	mezi
10	[number]	k4	10
a	a	k8xC	a
15	[number]	k4	15
lety	léto	k1gNnPc7	léto
pojmenovanou	pojmenovaný	k2eAgFnSc7d1	pojmenovaná
Expedice	expedice	k1gFnPc1	expedice
vesmír	vesmír	k1gInSc1	vesmír
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
měla	mít	k5eAaImAgFnS	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
popularizaci	popularizace	k1gFnSc4	popularizace
vědy	věda	k1gFnSc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
ročníky	ročník	k1gInPc1	ročník
soutěže	soutěž	k1gFnSc2	soutěž
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgFnP	konat
pod	pod	k7c7	pod
stejným	stejný	k2eAgInSc7d1	stejný
názvem	název	k1gInSc7	název
v	v	k7c6	v
létech	léto	k1gNnPc6	léto
2014	[number]	k4	2014
a	a	k8xC	a
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ocenění	ocenění	k1gNnSc1	ocenění
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
získal	získat	k5eAaPmAgMnS	získat
na	na	k7c6	na
mezinárodním	mezinárodní	k2eAgInSc6d1	mezinárodní
festivalu	festival	k1gInSc6	festival
Prix	Prix	k1gInSc1	Prix
Bohemia	bohemia	k1gFnSc1	bohemia
Radio	radio	k1gNnSc4	radio
národní	národní	k2eAgFnSc4d1	národní
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
publicistický	publicistický	k2eAgInSc4d1	publicistický
pořad	pořad	k1gInSc4	pořad
Mámo	máma	k1gFnSc5	máma
<g/>
,	,	kIx,	,
já	já	k3xPp1nSc1	já
fetuju	fetovat	k5eAaImIp1nS	fetovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
postoupil	postoupit	k5eAaPmAgMnS	postoupit
jeho	jeho	k3xOp3gMnSc1	jeho
feature	featur	k1gMnSc5	featur
Velká	velká	k1gFnSc1	velká
pardubická	pardubický	k2eAgFnSc1d1	pardubická
mezi	mezi	k7c4	mezi
sedm	sedm	k4xCc4	sedm
nejlepších	dobrý	k2eAgInPc2d3	nejlepší
dokumentů	dokument	k1gInPc2	dokument
na	na	k7c6	na
mezinárodním	mezinárodní	k2eAgInSc6d1	mezinárodní
festivalu	festival	k1gInSc6	festival
Prix	Prix	k1gInSc4	Prix
Europa	Europ	k1gMnSc4	Europ
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
rok	rok	k1gInSc4	rok
2004	[number]	k4	2004
obdržel	obdržet	k5eAaPmAgMnS	obdržet
od	od	k7c2	od
Českého	český	k2eAgInSc2d1	český
klubu	klub	k1gInSc2	klub
skeptiků	skeptik	k1gMnPc2	skeptik
Sisyfos	Sisyfos	k1gMnSc1	Sisyfos
ocenění	ocenění	k1gNnPc4	ocenění
bronzový	bronzový	k2eAgInSc4d1	bronzový
Bludný	bludný	k2eAgInSc4d1	bludný
balvan	balvan	k1gInSc4	balvan
"	"	kIx"	"
<g/>
za	za	k7c4	za
nápaditou	nápaditý	k2eAgFnSc4d1	nápaditá
a	a	k8xC	a
přesvědčivou	přesvědčivý	k2eAgFnSc4d1	přesvědčivá
propagaci	propagace	k1gFnSc4	propagace
grafologie	grafologie	k1gFnSc2	grafologie
v	v	k7c6	v
rozhlasovém	rozhlasový	k2eAgInSc6d1	rozhlasový
seriálu	seriál	k1gInSc6	seriál
Prezidentské	prezidentský	k2eAgInPc4d1	prezidentský
rukopisy	rukopis	k1gInPc4	rukopis
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	on	k3xPp3gInSc4	on
dokument	dokument	k1gInSc4	dokument
Komunismus	komunismus	k1gInSc1	komunismus
získal	získat	k5eAaPmAgInS	získat
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
ocenění	ocenění	k1gNnSc2	ocenění
Dokument	dokument	k1gInSc1	dokument
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Stejného	stejný	k2eAgInSc2d1	stejný
roku	rok	k1gInSc2	rok
získal	získat	k5eAaPmAgMnS	získat
na	na	k7c6	na
přehlídce	přehlídka	k1gFnSc6	přehlídka
Report	report	k1gInSc1	report
hlavní	hlavní	k2eAgInSc1d1	hlavní
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
dokument	dokument	k1gInSc4	dokument
Válečný	válečný	k2eAgInSc4d1	válečný
dekameron	dekameron	k1gInSc4	dekameron
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
stejné	stejný	k2eAgFnSc6d1	stejná
přehlídce	přehlídka	k1gFnSc6	přehlídka
získal	získat	k5eAaPmAgMnS	získat
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
1	[number]	k4	1
<g/>
.	.	kIx.	.
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
feature	featur	k1gMnSc5	featur
Ruší	rušit	k5eAaImIp3nS	rušit
nám	my	k3xPp1nPc3	my
to	ten	k3xDgNnSc1	ten
natáčení	natáčení	k1gNnSc4	natáčení
a	a	k8xC	a
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
další	další	k2eAgInPc4d1	další
1	[number]	k4	1
<g/>
.	.	kIx.	.
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
dokument	dokument	k1gInSc4	dokument
Svět	svět	k1gInSc1	svět
Miroslava	Miroslava	k1gFnSc1	Miroslava
Z.	Z.	kA	Z.
(	(	kIx(	(
<g/>
Zikmunda	Zikmund	k1gMnSc2	Zikmund
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
s	s	k7c7	s
dokumentem	dokument	k1gInSc7	dokument
Mokrsko	Mokrsko	k1gNnSc1	Mokrsko
2008	[number]	k4	2008
(	(	kIx(	(
<g/>
spoluautorka	spoluautorka	k1gFnSc1	spoluautorka
J.	J.	kA	J.
Jirátová	Jirátový	k2eAgFnSc1d1	Jirátová
<g/>
)	)	kIx)	)
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
festival	festival	k1gInSc1	festival
Prix	Prix	k1gInSc1	Prix
Bohemia	bohemia	k1gFnSc1	bohemia
Radio	radio	k1gNnSc4	radio
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
dokument	dokument	k1gInSc4	dokument
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
získal	získat	k5eAaPmAgMnS	získat
od	od	k7c2	od
Učené	učený	k2eAgFnSc2d1	učená
společnosti	společnost	k1gFnSc2	společnost
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
čestné	čestný	k2eAgNnSc4d1	čestné
uznání	uznání	k1gNnSc4	uznání
za	za	k7c4	za
vynikající	vynikající	k2eAgFnSc4d1	vynikající
popularizaci	popularizace	k1gFnSc4	popularizace
vědy	věda	k1gFnSc2	věda
v	v	k7c6	v
rozhlasovém	rozhlasový	k2eAgInSc6d1	rozhlasový
pořadu	pořad	k1gInSc6	pořad
Meteor	meteor	k1gInSc1	meteor
<g/>
.	.	kIx.	.
</s>
<s>
Akademie	akademie	k1gFnSc1	akademie
věd	věda	k1gFnPc2	věda
jej	on	k3xPp3gMnSc4	on
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2014	[number]	k4	2014
ocenila	ocenit	k5eAaPmAgFnS	ocenit
Čestnou	čestný	k2eAgFnSc7d1	čestná
medailí	medaile	k1gFnSc7	medaile
Vojtěcha	Vojtěch	k1gMnSc2	Vojtěch
Náprstka	Náprstka	k1gFnSc1	Náprstka
za	za	k7c4	za
dlouholetou	dlouholetý	k2eAgFnSc4d1	dlouholetá
popularizaci	popularizace	k1gFnSc4	popularizace
vědeckých	vědecký	k2eAgInPc2d1	vědecký
poznatků	poznatek	k1gInPc2	poznatek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Marek	Marek	k1gMnSc1	Marek
Janáč	Janáč	k1gMnSc1	Janáč
</s>
</p>
<p>
<s>
osobní	osobní	k2eAgFnPc1d1	osobní
webové	webový	k2eAgFnPc1d1	webová
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
<p>
<s>
KOČÍK	KOČÍK	kA	KOČÍK
<g/>
,	,	kIx,	,
René	René	k1gMnSc1	René
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Maříže	Maříž	k1gFnSc2	Maříž
přes	přes	k7c4	přes
Felbabku	Felbabka	k1gFnSc4	Felbabka
do	do	k7c2	do
Hajan	Hajany	k1gInPc2	Hajany
<g/>
.	.	kIx.	.
</s>
<s>
Týdeník	týdeník	k1gInSc1	týdeník
Rozhlas	rozhlas	k1gInSc1	rozhlas
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
2006	[number]	k4	2006
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2016	[number]	k4	2016
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
18	[number]	k4	18
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
48	[number]	k4	48
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Marek	Marek	k1gMnSc1	Marek
Janáč	Janáč	k1gMnSc1	Janáč
na	na	k7c6	na
Twitteru	Twitter	k1gInSc6	Twitter
</s>
</p>
