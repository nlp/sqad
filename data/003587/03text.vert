<s>
Telekomunikace	telekomunikace	k1gFnSc1	telekomunikace
neboli	neboli	k8xC	neboli
sdělovací	sdělovací	k2eAgFnSc1d1	sdělovací
technika	technika	k1gFnSc1	technika
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
předáváním	předávání	k1gNnSc7	předávání
informací	informace	k1gFnPc2	informace
na	na	k7c4	na
dálku	dálka	k1gFnSc4	dálka
(	(	kIx(	(
<g/>
z	z	k7c2	z
řeckého	řecký	k2eAgInSc2d1	řecký
tele	tele	k1gNnSc1	tele
–	–	k?	–
vzdálený	vzdálený	k2eAgMnSc1d1	vzdálený
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
tak	tak	k6eAd1	tak
zároveň	zároveň	k6eAd1	zároveň
druhem	druh	k1gInSc7	druh
dopravy	doprava	k1gFnSc2	doprava
a	a	k8xC	a
technologií	technologie	k1gFnPc2	technologie
sloužící	sloužící	k2eAgMnSc1d1	sloužící
k	k	k7c3	k
dorozumívání	dorozumívání	k1gNnSc3	dorozumívání
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
vlastně	vlastně	k9	vlastně
také	také	k9	také
o	o	k7c4	o
specifický	specifický	k2eAgInSc4d1	specifický
druh	druh	k1gInSc4	druh
elektronické	elektronický	k2eAgFnSc2d1	elektronická
komunikace	komunikace	k1gFnSc2	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
Komunikace	komunikace	k1gFnSc1	komunikace
může	moct	k5eAaImIp3nS	moct
probíhat	probíhat	k5eAaImF	probíhat
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgInPc7	dva
subjekty	subjekt	k1gInPc7	subjekt
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
od	od	k7c2	od
jednoho	jeden	k4xCgMnSc2	jeden
odesílatele	odesílatel	k1gMnSc2	odesílatel
k	k	k7c3	k
mnoha	mnoho	k4c2	mnoho
příjemcům	příjemce	k1gMnPc3	příjemce
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
telekomunikace	telekomunikace	k1gFnPc4	telekomunikace
patří	patřit	k5eAaImIp3nS	patřit
telegrafie	telegrafie	k1gFnSc1	telegrafie
dálnopis	dálnopis	k1gInSc1	dálnopis
bezdrátová	bezdrátový	k2eAgFnSc1d1	bezdrátová
telegrafie	telegrafie	k1gFnSc1	telegrafie
telefonie	telefonie	k1gFnSc2	telefonie
telefotografie	telefotografie	k1gFnSc1	telefotografie
telefax	telefax	k1gInSc1	telefax
mobilní	mobilní	k2eAgFnSc2d1	mobilní
telefonie	telefonie	k1gFnSc2	telefonie
radiofonie	radiofonie	k1gFnSc2	radiofonie
mobilní	mobilní	k2eAgFnSc2d1	mobilní
radiofonie	radiofonie	k1gFnSc2	radiofonie
(	(	kIx(	(
<g/>
přenosné	přenosný	k2eAgFnSc2d1	přenosná
vysílačky	vysílačka	k1gFnSc2	vysílačka
<g/>
)	)	kIx)	)
Mezi	mezi	k7c7	mezi
způsoby	způsob	k1gInPc7	způsob
hromadné	hromadný	k2eAgFnSc2d1	hromadná
elektronické	elektronický	k2eAgFnSc2d1	elektronická
komunikace	komunikace	k1gFnSc2	komunikace
patří	patřit	k5eAaImIp3nS	patřit
rozhlas	rozhlas	k1gInSc1	rozhlas
rozhlas	rozhlas	k1gInSc1	rozhlas
po	po	k7c6	po
drátě	drát	k1gInSc6	drát
televize	televize	k1gFnSc2	televize
kabelová	kabelový	k2eAgFnSc1d1	kabelová
televize	televize	k1gFnSc1	televize
U	u	k7c2	u
telekomunikací	telekomunikace	k1gFnPc2	telekomunikace
založených	založený	k2eAgFnPc2d1	založená
na	na	k7c6	na
počítačích	počítač	k1gInPc6	počítač
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
předchozí	předchozí	k2eAgNnSc4d1	předchozí
dělení	dělení	k1gNnSc4	dělení
smysl	smysl	k1gInSc4	smysl
<g/>
,	,	kIx,	,
síť	síť	k1gFnSc4	síť
dvoubodových	dvoubodový	k2eAgInPc2d1	dvoubodový
spojů	spoj	k1gInPc2	spoj
může	moct	k5eAaImIp3nS	moct
sloužit	sloužit	k5eAaImF	sloužit
k	k	k7c3	k
hromadné	hromadný	k2eAgFnSc3d1	hromadná
komunikaci	komunikace	k1gFnSc3	komunikace
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
fyzicky	fyzicky	k6eAd1	fyzicky
sdílené	sdílený	k2eAgNnSc1d1	sdílené
médium	médium	k1gNnSc1	médium
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
využito	využít	k5eAaPmNgNnS	využít
k	k	k7c3	k
dvoubodové	dvoubodový	k2eAgFnSc3d1	dvoubodová
komunikaci	komunikace	k1gFnSc3	komunikace
<g/>
.	.	kIx.	.
počítačové	počítačový	k2eAgFnSc2d1	počítačová
sítě	síť	k1gFnSc2	síť
Internet	Internet	k1gInSc4	Internet
digitální	digitální	k2eAgFnSc1d1	digitální
pagerové	pagerová	k1gFnPc1	pagerová
sítě	síť	k1gFnSc2	síť
U	u	k7c2	u
telekomunikací	telekomunikace	k1gFnPc2	telekomunikace
se	se	k3xPyFc4	se
často	často	k6eAd1	často
požadují	požadovat	k5eAaImIp3nP	požadovat
různé	různý	k2eAgFnPc4d1	různá
vlastnosti	vlastnost	k1gFnPc4	vlastnost
–	–	k?	–
odolnost	odolnost	k1gFnSc1	odolnost
proti	proti	k7c3	proti
chybám	chyba	k1gFnPc3	chyba
<g/>
,	,	kIx,	,
zaručená	zaručený	k2eAgFnSc1d1	zaručená
rychlost	rychlost	k1gFnSc1	rychlost
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
zajištění	zajištění	k1gNnSc3	zajištění
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
kódovaní	kódovaný	k2eAgMnPc1d1	kódovaný
<g/>
,	,	kIx,	,
samodetekující	samodetekující	k2eAgInSc4d1	samodetekující
kód	kód	k1gInSc4	kód
<g/>
,	,	kIx,	,
samoopravný	samoopravný	k2eAgInSc4d1	samoopravný
kód	kód	k1gInSc4	kód
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
prostředky	prostředek	k1gInPc4	prostředek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
informatika	informatika	k1gFnSc1	informatika
<g/>
.	.	kIx.	.
</s>
<s>
Svět	svět	k1gInSc1	svět
internetu	internet	k1gInSc6	internet
nám	my	k3xPp1nPc3	my
nabízí	nabízet	k5eAaImIp3nS	nabízet
synchronní	synchronní	k2eAgInPc4d1	synchronní
a	a	k8xC	a
asynchronní	asynchronní	k2eAgInPc4d1	asynchronní
způsoby	způsob	k1gInPc4	způsob
komunikace	komunikace	k1gFnSc2	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
Synchronní	synchronní	k2eAgFnSc4d1	synchronní
komunikaci	komunikace	k1gFnSc4	komunikace
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
různé	různý	k2eAgInPc1d1	různý
nástroje	nástroj	k1gInPc1	nástroj
<g/>
,	,	kIx,	,
např.	např.	kA	např.
chat	chata	k1gFnPc2	chata
<g/>
,	,	kIx,	,
VoIP	VoIP	k1gFnSc1	VoIP
telefonie	telefonie	k1gFnSc1	telefonie
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
Skype	Skyp	k1gInSc5	Skyp
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
asynchronní	asynchronní	k2eAgFnSc4d1	asynchronní
považujeme	považovat	k5eAaImIp1nP	považovat
takové	takový	k3xDgInPc4	takový
způsoby	způsob	k1gInPc4	způsob
komunikace	komunikace	k1gFnSc2	komunikace
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterých	který	k3yIgInPc6	který
není	být	k5eNaImIp3nS	být
nutná	nutný	k2eAgFnSc1d1	nutná
okamžitá	okamžitý	k2eAgFnSc1d1	okamžitá
reakce	reakce	k1gFnSc1	reakce
<g/>
,	,	kIx,	,
např.	např.	kA	např.
diskusní	diskusní	k2eAgInSc1d1	diskusní
fóra	fórum	k1gNnSc2	fórum
a	a	k8xC	a
e-mail	eail	k1gInSc1	e-mail
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
internetu	internet	k1gInSc6	internet
lze	lze	k6eAd1	lze
přenášet	přenášet	k5eAaImF	přenášet
v	v	k7c6	v
digitální	digitální	k2eAgFnSc6d1	digitální
formě	forma	k1gFnSc6	forma
rozhlas	rozhlas	k1gInSc4	rozhlas
i	i	k8xC	i
televizi	televize	k1gFnSc4	televize
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
zajistit	zajistit	k5eAaPmF	zajistit
i	i	k9	i
další	další	k2eAgFnPc4d1	další
doplňkové	doplňkový	k2eAgFnPc4d1	doplňková
služby	služba	k1gFnPc4	služba
v	v	k7c6	v
rozsahu	rozsah	k1gInSc6	rozsah
podobném	podobný	k2eAgInSc6d1	podobný
klasické	klasický	k2eAgFnSc3d1	klasická
telefonii	telefonie	k1gFnSc3	telefonie
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
telekomunikace	telekomunikace	k1gFnSc2	telekomunikace
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Komunikace	komunikace	k1gFnSc1	komunikace
-	-	kIx~	-
článek	článek	k1gInSc1	článek
o	o	k7c6	o
zásadách	zásada	k1gFnPc6	zásada
správné	správný	k2eAgFnSc2d1	správná
komunikace	komunikace	k1gFnSc2	komunikace
přes	přes	k7c4	přes
email	email	k1gInSc4	email
<g/>
,	,	kIx,	,
mobil	mobil	k1gInSc4	mobil
<g/>
,	,	kIx,	,
telefon	telefon	k1gInSc4	telefon
<g/>
,	,	kIx,	,
ICQ	ICQ	kA	ICQ
<g/>
,	,	kIx,	,
Skype	Skyp	k1gInSc5	Skyp
aj.	aj.	kA	aj.
Lupa	lupa	k1gFnSc1	lupa
-	-	kIx~	-
server	server	k1gInSc1	server
o	o	k7c6	o
českém	český	k2eAgInSc6d1	český
internetu	internet	k1gInSc6	internet
pokrývající	pokrývající	k2eAgNnSc4d1	pokrývající
rovněž	rovněž	k9	rovněž
problematiku	problematika	k1gFnSc4	problematika
elektronické	elektronický	k2eAgFnSc2d1	elektronická
komunikace	komunikace	k1gFnSc2	komunikace
Sdělovací	sdělovací	k2eAgFnSc1d1	sdělovací
technika	technika	k1gFnSc1	technika
je	být	k5eAaImIp3nS	být
také	také	k9	také
název	název	k1gInSc1	název
odborného	odborný	k2eAgInSc2d1	odborný
časopisu	časopis	k1gInSc2	časopis
vydávaného	vydávaný	k2eAgInSc2d1	vydávaný
stejnojmenným	stejnojmenný	k2eAgNnSc7d1	stejnojmenné
nakladatelstvím	nakladatelství	k1gNnSc7	nakladatelství
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
