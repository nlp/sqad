<s>
James	James	k1gMnSc1	James
Nachtwey	Nachtwea	k1gFnSc2	Nachtwea
(	(	kIx(	(
<g/>
*	*	kIx~	*
14	[number]	k4	14
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1948	[number]	k4	1948
Syracuse	Syracuse	k1gFnSc1	Syracuse
(	(	kIx(	(
<g/>
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
))	))	k?	))
je	být	k5eAaImIp3nS	být
vlivný	vlivný	k2eAgMnSc1d1	vlivný
americký	americký	k2eAgMnSc1d1	americký
fotožurnalista	fotožurnalista	k1gMnSc1	fotožurnalista
a	a	k8xC	a
válečný	válečný	k2eAgMnSc1d1	válečný
fotograf	fotograf	k1gMnSc1	fotograf
<g/>
.	.	kIx.	.
</s>
<s>
Pracuje	pracovat	k5eAaImIp3nS	pracovat
jako	jako	k9	jako
korespondent	korespondent	k1gMnSc1	korespondent
pro	pro	k7c4	pro
časopis	časopis	k1gInSc4	časopis
Time	Tim	k1gFnSc2	Tim
v	v	k7c6	v
Bagdádu	Bagdád	k1gInSc6	Bagdád
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
členem	člen	k1gInSc7	člen
agentury	agentura	k1gFnSc2	agentura
Magnum	Magnum	k1gNnSc1	Magnum
Photos	Photosa	k1gFnPc2	Photosa
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1986	[number]	k4	1986
do	do	k7c2	do
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zámoří	zámoří	k1gNnSc6	zámoří
byl	být	k5eAaImAgInS	být
celkem	celkem	k6eAd1	celkem
pětkrát	pětkrát	k6eAd1	pětkrát
oceněn	ocenit	k5eAaPmNgInS	ocenit
Klubem	klub	k1gInSc7	klub
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
novinářů	novinář	k1gMnPc2	novinář
(	(	kIx(	(
<g/>
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1983	[number]	k4	1983
<g/>
,	,	kIx,	,
1984	[number]	k4	1984
<g/>
,	,	kIx,	,
1986	[number]	k4	1986
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
a	a	k8xC	a
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
a	a	k8xC	a
obdržel	obdržet	k5eAaPmAgMnS	obdržet
od	od	k7c2	od
ní	on	k3xPp3gFnSc2	on
Zlatou	Zlata	k1gFnSc7	Zlata
medailí	medaile	k1gFnPc2	medaile
Roberta	Robert	k1gMnSc2	Robert
Capy	capa	k1gFnSc2	capa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
byl	být	k5eAaImAgMnS	být
při	při	k7c6	při
útoku	útok	k1gInSc6	útok
na	na	k7c4	na
jeho	jeho	k3xOp3gInSc4	jeho
konvoj	konvoj	k1gInSc4	konvoj
v	v	k7c6	v
Bagdádu	Bagdád	k1gInSc6	Bagdád
zraněn	zranit	k5eAaPmNgMnS	zranit
granátem	granát	k1gInSc7	granát
<g/>
,	,	kIx,	,
když	když	k8xS	když
tam	tam	k6eAd1	tam
pracoval	pracovat	k5eAaImAgMnS	pracovat
pro	pro	k7c4	pro
magazín	magazín	k1gInSc4	magazín
Time	Tim	k1gFnSc2	Tim
<g/>
.	.	kIx.	.
</s>
<s>
Nachtwey	Nachtwey	k1gInPc4	Nachtwey
začal	začít	k5eAaPmAgInS	začít
pracovat	pracovat	k5eAaImF	pracovat
jako	jako	k9	jako
fotograf	fotograf	k1gMnSc1	fotograf
pro	pro	k7c4	pro
malé	malý	k2eAgNnSc4d1	malé
nakladatelství	nakladatelství	k1gNnSc4	nakladatelství
v	v	k7c6	v
Novém	nový	k2eAgNnSc6d1	nové
Mexiku	Mexiko	k1gNnSc6	Mexiko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
se	se	k3xPyFc4	se
přestěhoval	přestěhovat	k5eAaPmAgInS	přestěhovat
do	do	k7c2	do
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
pracovat	pracovat	k5eAaImF	pracovat
jako	jako	k9	jako
fotograf	fotograf	k1gMnSc1	fotograf
na	na	k7c6	na
volné	volný	k2eAgFnSc6d1	volná
noze	noha	k1gFnSc6	noha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
svého	svůj	k3xOyFgInSc2	svůj
prvního	první	k4xOgInSc2	první
úkolu	úkol	k1gInSc2	úkol
v	v	k7c6	v
Severním	severní	k2eAgNnSc6d1	severní
Irsku	Irsko	k1gNnSc6	Irsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dokumentoval	dokumentovat	k5eAaBmAgMnS	dokumentovat
občanské	občanský	k2eAgInPc4d1	občanský
nepokoje	nepokoj	k1gInPc4	nepokoj
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1984	[number]	k4	1984
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
fotograf	fotograf	k1gMnSc1	fotograf
pro	pro	k7c4	pro
magazín	magazín	k1gInSc4	magazín
Time	Tim	k1gFnSc2	Tim
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Black	Black	k1gInSc4	Black
Star	star	k1gFnSc2	star
pracoval	pracovat	k5eAaImAgMnS	pracovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
a	a	k8xC	a
členem	člen	k1gInSc7	člen
Magnum	Magnum	k1gInSc1	Magnum
Photos	Photosa	k1gFnPc2	Photosa
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1986	[number]	k4	1986
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
ze	z	k7c2	z
zakládajících	zakládající	k2eAgMnPc2d1	zakládající
členů	člen	k1gMnPc2	člen
představenstva	představenstvo	k1gNnSc2	představenstvo
agentury	agentura	k1gFnSc2	agentura
VII	VII	kA	VII
Photo	Photo	k1gNnSc4	Photo
Agency	Agenca	k1gFnSc2	Agenca
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
celou	celá	k1gFnSc4	celá
svou	svůj	k3xOyFgFnSc4	svůj
kariéru	kariéra	k1gFnSc4	kariéra
fotografoval	fotografovat	k5eAaImAgInS	fotografovat
ozbrojené	ozbrojený	k2eAgInPc4d1	ozbrojený
konflikty	konflikt	k1gInPc4	konflikt
a	a	k8xC	a
sociální	sociální	k2eAgInPc4d1	sociální
problémy	problém	k1gInPc4	problém
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
Latinské	latinský	k2eAgFnSc6d1	Latinská
Americe	Amerika	k1gFnSc6	Amerika
a	a	k8xC	a
na	na	k7c6	na
Středním	střední	k2eAgInSc6d1	střední
východě	východ	k1gInSc6	východ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemích	zem	k1gFnPc6	zem
bývalého	bývalý	k2eAgInSc2d1	bývalý
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
dokumentoval	dokumentovat	k5eAaBmAgMnS	dokumentovat
válečné	válečný	k2eAgInPc4d1	válečný
konflikty	konflikt	k1gInPc4	konflikt
<g/>
,	,	kIx,	,
hlad	hlad	k1gInSc4	hlad
<g/>
,	,	kIx,	,
a	a	k8xC	a
znečištění	znečištění	k1gNnSc1	znečištění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
v	v	k7c6	v
USA	USA	kA	USA
fotografuje	fotografovat	k5eAaImIp3nS	fotografovat
podmínky	podmínka	k1gFnPc4	podmínka
ve	v	k7c6	v
věznicích	věznice	k1gFnPc6	věznice
<g/>
.	.	kIx.	.
</s>
<s>
Vyfotil	vyfotit	k5eAaPmAgMnS	vyfotit
také	také	k9	také
první	první	k4xOgFnPc1	první
volby	volba	k1gFnPc1	volba
bez	bez	k7c2	bez
rozlišení	rozlišení	k1gNnSc2	rozlišení
ras	rasa	k1gFnPc2	rasa
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Nachtwey	Nachtwey	k1gInPc4	Nachtwey
pracoval	pracovat	k5eAaImAgInS	pracovat
také	také	k9	také
na	na	k7c6	na
reportáži	reportáž	k1gFnSc6	reportáž
postižených	postižený	k2eAgNnPc2d1	postižené
míst	místo	k1gNnPc2	místo
tsunami	tsunami	k1gNnSc2	tsunami
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Asii	Asie	k1gFnSc6	Asie
26	[number]	k4	26
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
také	také	k9	také
známou	známý	k2eAgFnSc4d1	známá
sérii	série	k1gFnSc4	série
fotografií	fotografia	k1gFnPc2	fotografia
s	s	k7c7	s
názvem	název	k1gInSc7	název
Body	bod	k1gInPc4	bod
of	of	k?	of
work	work	k6eAd1	work
z	z	k7c2	z
průběhu	průběh	k1gInSc2	průběh
útoků	útok	k1gInPc2	útok
na	na	k7c4	na
Světové	světový	k2eAgNnSc4d1	světové
obchodní	obchodní	k2eAgNnSc4d1	obchodní
centrum	centrum	k1gNnSc4	centrum
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
International	Internationat	k5eAaImAgMnS	Internationat
Center	centrum	k1gNnPc2	centrum
of	of	k?	of
Photography	Photographa	k1gFnSc2	Photographa
<g/>
,	,	kIx,	,
New	New	k1gMnPc2	New
York	York	k1gInSc1	York
Museum	museum	k1gNnSc4	museum
of	of	k?	of
Photographic	Photographice	k1gFnPc2	Photographice
Arts	Arts	k1gInSc1	Arts
<g/>
,	,	kIx,	,
San	San	k1gMnSc1	San
Diego	Diego	k1gMnSc1	Diego
Culturgest	Culturgest	k1gMnSc1	Culturgest
<g/>
,	,	kIx,	,
Lisabon	Lisabon	k1gInSc1	Lisabon
Bibliothè	Bibliothè	k1gFnSc2	Bibliothè
nationale	nationale	k6eAd1	nationale
de	de	k?	de
France	Franc	k1gMnSc4	Franc
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc4	Paříž
Palazzo	Palazza	k1gFnSc5	Palazza
Esposizione	Esposizion	k1gInSc5	Esposizion
<g/>
,	,	kIx,	,
Řím	Řím	k1gInSc1	Řím
El	Ela	k1gFnPc2	Ela
Círculo	Círcula	k1gFnSc5	Círcula
de	de	k?	de
Bellas	Bellas	k1gMnSc1	Bellas
Artes	Artes	k1gMnSc1	Artes
<g/>
,	,	kIx,	,
Madrid	Madrid	k1gInSc1	Madrid
Galerie	galerie	k1gFnSc2	galerie
Fahey	Fahea	k1gFnSc2	Fahea
<g/>
/	/	kIx~	/
<g/>
Klein	Klein	k1gMnSc1	Klein
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
Karolinum	Karolinum	k1gNnSc1	Karolinum
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
Hasselblad	Hasselblad	k1gInSc1	Hasselblad
Center	centrum	k1gNnPc2	centrum
Švédsko	Švédsko	k1gNnSc1	Švédsko
Canon	Canon	kA	Canon
Gallery	Galler	k1gInPc1	Galler
<g/>
,	,	kIx,	,
Amsterdam	Amsterdam	k1gInSc1	Amsterdam
Nieuwe	Nieuwe	k1gNnSc1	Nieuwe
Kerk	Kerk	k1gInSc1	Kerk
<g/>
,	,	kIx,	,
Amsterdam	Amsterdam	k1gInSc1	Amsterdam
Massachusetts	Massachusetts	k1gNnSc1	Massachusetts
College	College	k1gInSc1	College
of	of	k?	of
Art	Art	k1gFnSc1	Art
<g/>
,	,	kIx,	,
Boston	Boston	k1gInSc1	Boston
Hood	Hooda	k1gFnPc2	Hooda
Museum	museum	k1gNnSc1	museum
of	of	k?	of
Art	Art	k1gMnSc1	Art
<g/>
,	,	kIx,	,
Dartmouth	Dartmouth	k1gMnSc1	Dartmouth
College	Colleg	k1gFnSc2	Colleg
C	C	kA	C
<g/>
/	/	kIx~	/
<g/>
O	O	kA	O
Berlín	Berlín	k1gInSc1	Berlín
<g/>
,	,	kIx,	,
Berlín	Berlín	k1gInSc1	Berlín
Stadtmuseum	Stadtmuseum	k1gInSc1	Stadtmuseum
Göhre	Göhr	k1gInSc5	Göhr
<g/>
,	,	kIx,	,
Jena	Jena	k1gFnSc1	Jena
2001	[number]	k4	2001
<g/>
:	:	kIx,	:
L	L	kA	L
<g/>
<g />
.	.	kIx.	.
</s>
<s>
'	'	kIx"	'
<g/>
Occhio	Occhia	k1gMnSc5	Occhia
Testimone	Testimon	k1gMnSc5	Testimon
2000	[number]	k4	2000
<g/>
:	:	kIx,	:
Inferno	inferno	k1gNnSc1	inferno
(	(	kIx(	(
<g/>
Phaidon	Phaidon	k1gNnSc1	Phaidon
Press	Pressa	k1gFnPc2	Pressa
<g/>
;	;	kIx,	;
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
7148	[number]	k4	7148
<g/>
-	-	kIx~	-
<g/>
3815	[number]	k4	3815
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
1999	[number]	k4	1999
<g/>
:	:	kIx,	:
Civil	civil	k1gMnSc1	civil
Wars	Wars	k1gInSc4	Wars
1997	[number]	k4	1997
<g/>
:	:	kIx,	:
Ground	Ground	k1gInSc1	Ground
Level	level	k1gInSc1	level
1989	[number]	k4	1989
<g/>
:	:	kIx,	:
Deeds	Deedsa	k1gFnPc2	Deedsa
of	of	k?	of
War	War	k1gMnSc1	War
(	(	kIx(	(
<g/>
Thames	Thames	k1gMnSc1	Thames
and	and	k?	and
Hudson	Hudson	k1gMnSc1	Hudson
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
500	[number]	k4	500
<g/>
-	-	kIx~	-
<g/>
54152	[number]	k4	54152
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
–	–	k?	–
Fotografie	fotografia	k1gFnPc4	fotografia
z	z	k7c2	z
Nicaraguy	Nicaragua	k1gFnSc2	Nicaragua
<g/>
,	,	kIx,	,
El	Ela	k1gFnPc2	Ela
Salvadoru	Salvador	k1gInSc2	Salvador
<g/>
,	,	kIx,	,
Libanonu	Libanon	k1gInSc2	Libanon
a	a	k8xC	a
Afghánistánu	Afghánistán	k1gInSc2	Afghánistán
<g/>
.	.	kIx.	.
1993	[number]	k4	1993
-	-	kIx~	-
Cena	cena	k1gFnSc1	cena
W.	W.	kA	W.
Eugena	Eugen	k2eAgFnSc1d1	Eugena
Smithe	Smithe	k1gFnSc1	Smithe
</s>
