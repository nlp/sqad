<p>
<s>
Obwalden	Obwaldna	k1gFnPc2	Obwaldna
či	či	k8xC	či
Unterwalden	Unterwaldna	k1gFnPc2	Unterwaldna
ob	ob	k7c4	ob
dem	dem	k?	dem
Wald	Wald	k1gMnSc1	Wald
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
26	[number]	k4	26
kantonů	kanton	k1gInPc2	kanton
Švýcarské	švýcarský	k2eAgFnSc2d1	švýcarská
konfederace	konfederace	k1gFnSc2	konfederace
<g/>
.	.	kIx.	.
</s>
<s>
Metropolí	metropol	k1gFnSc7	metropol
Obwaldenu	Obwalden	k1gInSc2	Obwalden
<g/>
,	,	kIx,	,
ležícího	ležící	k2eAgMnSc2d1	ležící
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
části	část	k1gFnSc6	část
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Sarnen	Sarnen	k1gInSc1	Sarnen
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
kantonem	kanton	k1gInSc7	kanton
Nidwalden	Nidwaldna	k1gFnPc2	Nidwaldna
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
jako	jako	k8xC	jako
polokanton	polokanton	k1gInSc4	polokanton
rozdělením	rozdělení	k1gNnSc7	rozdělení
historického	historický	k2eAgInSc2d1	historický
kantonu	kanton	k1gInSc2	kanton
Unterwalden	Unterwaldna	k1gFnPc2	Unterwaldna
<g/>
,	,	kIx,	,
jednoho	jeden	k4xCgMnSc4	jeden
ze	z	k7c2	z
tří	tři	k4xCgMnPc2	tři
zakládajících	zakládající	k2eAgMnPc2d1	zakládající
členů	člen	k1gMnPc2	člen
Staré	Staré	k2eAgFnSc2d1	Staré
konfederace	konfederace	k1gFnSc2	konfederace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hospodářství	hospodářství	k1gNnSc1	hospodářství
==	==	k?	==
</s>
</p>
<p>
<s>
Obwaldenské	Obwaldenský	k2eAgFnSc3d1	Obwaldenský
ekonomice	ekonomika	k1gFnSc3	ekonomika
dominují	dominovat	k5eAaImIp3nP	dominovat
malé	malý	k2eAgInPc1d1	malý
a	a	k8xC	a
střední	střední	k2eAgInPc1d1	střední
podniky	podnik	k1gInPc1	podnik
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
se	se	k3xPyFc4	se
specializuje	specializovat	k5eAaBmIp3nS	specializovat
v	v	k7c6	v
takových	takový	k3xDgInPc6	takový
oborech	obor	k1gInPc6	obor
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
výroba	výroba	k1gFnSc1	výroba
miniaturních	miniaturní	k2eAgInPc2d1	miniaturní
strojů	stroj	k1gInPc2	stroj
<g/>
,	,	kIx,	,
umělých	umělý	k2eAgFnPc2d1	umělá
hmot	hmota	k1gFnPc2	hmota
<g/>
,	,	kIx,	,
zdravotních	zdravotní	k2eAgFnPc2d1	zdravotní
potřeb	potřeba	k1gFnPc2	potřeba
či	či	k8xC	či
nanotechnologií	nanotechnologie	k1gFnPc2	nanotechnologie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stále	stále	k6eAd1	stále
velký	velký	k2eAgInSc1d1	velký
význam	význam	k1gInSc1	význam
mají	mít	k5eAaImIp3nP	mít
tradiční	tradiční	k2eAgFnPc4d1	tradiční
obory	obora	k1gFnPc4	obora
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
zejména	zejména	k9	zejména
lesnictví	lesnictví	k1gNnSc1	lesnictví
a	a	k8xC	a
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
související	související	k2eAgInPc4d1	související
obory	obor	k1gInPc4	obor
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
zemědělství	zemědělství	k1gNnSc1	zemědělství
<g/>
.	.	kIx.	.
</s>
<s>
Zemědělská	zemědělský	k2eAgFnSc1d1	zemědělská
výroba	výroba	k1gFnSc1	výroba
se	se	k3xPyFc4	se
specializuje	specializovat	k5eAaBmIp3nS	specializovat
na	na	k7c4	na
produkci	produkce	k1gFnSc4	produkce
mléčných	mléčný	k2eAgInPc2d1	mléčný
výrobků	výrobek	k1gInPc2	výrobek
a	a	k8xC	a
masa	maso	k1gNnSc2	maso
<g/>
.	.	kIx.	.
</s>
<s>
Statky	statek	k1gInPc1	statek
jsou	být	k5eAaImIp3nP	být
rodinnými	rodinný	k2eAgInPc7d1	rodinný
podniky	podnik	k1gInPc7	podnik
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
odvětvím	odvětví	k1gNnSc7	odvětví
zdejší	zdejší	k2eAgFnSc2d1	zdejší
ekonomiky	ekonomika	k1gFnSc2	ekonomika
je	být	k5eAaImIp3nS	být
turistika	turistika	k1gFnSc1	turistika
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
zařízení	zařízení	k1gNnPc2	zařízení
vybudovaných	vybudovaný	k2eAgNnPc2d1	vybudované
pro	pro	k7c4	pro
turistický	turistický	k2eAgInSc4d1	turistický
ruch	ruch	k1gInSc4	ruch
nyní	nyní	k6eAd1	nyní
přináší	přinášet	k5eAaImIp3nS	přinášet
zisky	zisk	k1gInPc4	zisk
zdejšímu	zdejší	k2eAgInSc3d1	zdejší
průmyslu	průmysl	k1gInSc3	průmysl
a	a	k8xC	a
obyvatelstvu	obyvatelstvo	k1gNnSc3	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
turistickém	turistický	k2eAgNnSc6d1	turistické
odvětví	odvětví	k1gNnSc6	odvětví
je	být	k5eAaImIp3nS	být
také	také	k9	také
přímo	přímo	k6eAd1	přímo
či	či	k8xC	či
nepřímo	přímo	k6eNd1	přímo
zaměstnána	zaměstnat	k5eAaPmNgFnS	zaměstnat
celá	celý	k2eAgFnSc1d1	celá
jedna	jeden	k4xCgFnSc1	jeden
čtvrtina	čtvrtina	k1gFnSc1	čtvrtina
obytatelstva	obytatelstvo	k1gNnSc2	obytatelstvo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Politický	politický	k2eAgInSc1d1	politický
přehled	přehled	k1gInSc1	přehled
==	==	k?	==
</s>
</p>
<p>
<s>
Obwalden	Obwaldna	k1gFnPc2	Obwaldna
má	mít	k5eAaImIp3nS	mít
ve	v	k7c6	v
švýcarské	švýcarský	k2eAgFnSc6d1	švýcarská
Radě	rada	k1gFnSc6	rada
států	stát	k1gInPc2	stát
pouze	pouze	k6eAd1	pouze
jednoho	jeden	k4xCgMnSc4	jeden
zástupce	zástupce	k1gMnSc4	zástupce
místo	místo	k7c2	místo
obvyklých	obvyklý	k2eAgInPc2d1	obvyklý
dvou	dva	k4xCgInPc2	dva
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
pozůstatek	pozůstatek	k1gInSc4	pozůstatek
po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
někdejším	někdejší	k2eAgInSc6d1	někdejší
statutu	statut	k1gInSc6	statut
polokantonu	polokanton	k1gInSc2	polokanton
<g/>
.	.	kIx.	.
</s>
<s>
Kantonální	kantonální	k2eAgInSc1d1	kantonální
parlament	parlament	k1gInSc1	parlament
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
"	"	kIx"	"
<g/>
Rada	rada	k1gFnSc1	rada
kantonu	kanton	k1gInSc2	kanton
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Kantonsrat	Kantonsrat	k1gInSc1	Kantonsrat
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
55	[number]	k4	55
poslanců	poslanec	k1gMnPc2	poslanec
<g/>
.	.	kIx.	.
</s>
<s>
Zdejší	zdejší	k2eAgFnSc1d1	zdejší
pětičlenná	pětičlenný	k2eAgFnSc1d1	pětičlenná
vláda	vláda	k1gFnSc1	vláda
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
"	"	kIx"	"
<g/>
Vládní	vládní	k2eAgFnSc1d1	vládní
rada	rada	k1gFnSc1	rada
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Regierungsrat	Regierungsrat	k1gInSc1	Regierungsrat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografický	geografický	k2eAgInSc4d1	geografický
přehled	přehled	k1gInSc4	přehled
a	a	k8xC	a
správní	správní	k2eAgNnSc4d1	správní
členění	členění	k1gNnSc4	členění
==	==	k?	==
</s>
</p>
<p>
<s>
Území	území	k1gNnSc1	území
kantonu	kanton	k1gInSc2	kanton
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
izolovaných	izolovaný	k2eAgFnPc2d1	izolovaná
částí	část	k1gFnPc2	část
<g/>
:	:	kIx,	:
větší	veliký	k2eAgFnSc1d2	veliký
západní	západní	k2eAgFnSc1d1	západní
část	část	k1gFnSc1	část
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
šest	šest	k4xCc4	šest
obcí	obec	k1gFnPc2	obec
(	(	kIx(	(
<g/>
Sarnen	Sarnen	k1gInSc1	Sarnen
<g/>
,	,	kIx,	,
Kerns	Kerns	k1gInSc1	Kerns
<g/>
,	,	kIx,	,
Alpnach	Alpnach	k1gMnSc1	Alpnach
<g/>
,	,	kIx,	,
Sachseln	Sachseln	k1gMnSc1	Sachseln
<g/>
,	,	kIx,	,
Giswil	Giswil	k1gMnSc1	Giswil
a	a	k8xC	a
Lungern	Lungern	k1gMnSc1	Lungern
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
menší	malý	k2eAgFnSc1d2	menší
východní	východní	k2eAgFnSc1d1	východní
část	část	k1gFnSc1	část
je	být	k5eAaImIp3nS	být
exklávou	exklávý	k2eAgFnSc4d1	exklávý
tvořenou	tvořený	k2eAgFnSc4d1	tvořená
územím	území	k1gNnSc7	území
obce	obec	k1gFnSc2	obec
Engelberg	Engelberg	k1gMnSc1	Engelberg
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Obwalden	Obwaldna	k1gFnPc2	Obwaldna
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgInSc1d1	oficiální
web	web	k1gInSc1	web
kantonu	kanton	k1gInSc2	kanton
Obwalden	Obwaldna	k1gFnPc2	Obwaldna
</s>
</p>
