<s>
Mango	mango	k1gNnSc1	mango
je	být	k5eAaImIp3nS	být
tropické	tropický	k2eAgNnSc4d1	tropické
ovoce	ovoce	k1gNnSc4	ovoce
<g/>
,	,	kIx,	,
plod	plod	k1gInSc4	plod
mangovníku	mangovník	k1gInSc2	mangovník
<g/>
.	.	kIx.	.
</s>
<s>
Mango	mango	k1gNnSc1	mango
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
rodu	rod	k1gInSc2	rod
Mangifera	Mangifero	k1gNnSc2	Mangifero
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
69	[number]	k4	69
botanických	botanický	k2eAgInPc2d1	botanický
druhů	druh	k1gInPc2	druh
a	a	k8xC	a
přes	přes	k7c4	přes
1000	[number]	k4	1000
odrůd	odrůda	k1gFnPc2	odrůda
tohoto	tento	k3xDgNnSc2	tento
ovoce	ovoce	k1gNnSc2	ovoce
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
znám	znám	k2eAgInSc4d1	znám
jeho	on	k3xPp3gInSc4	on
přesný	přesný	k2eAgInSc4d1	přesný
původ	původ	k1gInSc4	původ
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
má	mít	k5eAaImIp3nS	mít
se	se	k3xPyFc4	se
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
mango	mango	k1gNnSc1	mango
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
jižní	jižní	k2eAgFnSc2d1	jižní
a	a	k8xC	a
jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
<g/>
,	,	kIx,	,
Barmě	Barma	k1gFnSc6	Barma
<g/>
,	,	kIx,	,
na	na	k7c6	na
Srí	Srí	k1gFnSc6	Srí
Lance	lance	k1gNnSc2	lance
<g/>
,	,	kIx,	,
v	v	k7c6	v
Pákistánu	Pákistán	k1gInSc6	Pákistán
a	a	k8xC	a
Bangladéši	Bangladéš	k1gInSc6	Bangladéš
byly	být	k5eAaImAgFnP	být
nalezeny	naleznout	k5eAaPmNgFnP	naleznout
fosilie	fosilie	k1gFnPc1	fosilie
staré	starý	k2eAgFnPc1d1	stará
25	[number]	k4	25
až	až	k9	až
30	[number]	k4	30
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
název	název	k1gInSc1	název
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
malajálamského	malajálamský	k2eAgNnSc2d1	malajálamský
manga	mango	k1gNnSc2	mango
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
si	se	k3xPyFc3	se
později	pozdě	k6eAd2	pozdě
portugalští	portugalský	k2eAgMnPc1d1	portugalský
obchodníci	obchodník	k1gMnPc1	obchodník
upravili	upravit	k5eAaPmAgMnP	upravit
na	na	k7c4	na
manga	mango	k1gNnPc4	mango
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
výtečným	výtečný	k2eAgInSc7d1	výtečný
zdrojem	zdroj	k1gInSc7	zdroj
vitaminů	vitamin	k1gInPc2	vitamin
A	A	kA	A
<g/>
,	,	kIx,	,
C	C	kA	C
<g/>
,	,	kIx,	,
beta	beta	k1gNnSc1	beta
karotenu	karoten	k1gInSc2	karoten
<g/>
,	,	kIx,	,
draslíku	draslík	k1gInSc2	draslík
a	a	k8xC	a
vlákniny	vláknina	k1gFnSc2	vláknina
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
málo	málo	k1gNnSc1	málo
tuku	tuk	k1gInSc2	tuk
a	a	k8xC	a
sodíku	sodík	k1gInSc2	sodík
<g/>
.	.	kIx.	.
</s>
<s>
Plody	plod	k1gInPc1	plod
jsou	být	k5eAaImIp3nP	být
různé	různý	k2eAgFnPc4d1	různá
barvy	barva	k1gFnPc4	barva
<g/>
,	,	kIx,	,
velikosti	velikost	k1gFnSc6	velikost
i	i	k8xC	i
tvaru	tvar	k1gInSc6	tvar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
i	i	k9	i
zelených	zelený	k2eAgNnPc2d1	zelené
mang	mango	k1gNnPc2	mango
k	k	k7c3	k
různým	různý	k2eAgInPc3d1	různý
kulinářským	kulinářský	k2eAgInPc3d1	kulinářský
účelům	účel	k1gInPc3	účel
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
jej	on	k3xPp3gMnSc4	on
nechat	nechat	k5eAaPmF	nechat
dozrát	dozrát	k5eAaPmF	dozrát
při	při	k7c6	při
pokojové	pokojový	k2eAgFnSc6d1	pokojová
teplotě	teplota	k1gFnSc6	teplota
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
bude	být	k5eAaImBp3nS	být
měkké	měkký	k2eAgNnSc1d1	měkké
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
přímé	přímý	k2eAgFnPc4d1	přímá
konzumace	konzumace	k1gFnPc4	konzumace
čerstvých	čerstvý	k2eAgMnPc2d1	čerstvý
<g/>
,	,	kIx,	,
zralých	zralý	k2eAgInPc2d1	zralý
plodů	plod	k1gInPc2	plod
se	se	k3xPyFc4	se
hodí	hodit	k5eAaImIp3nS	hodit
např.	např.	kA	např.
jako	jako	k8xC	jako
příloha	příloha	k1gFnSc1	příloha
k	k	k7c3	k
masům	maso	k1gNnPc3	maso
a	a	k8xC	a
omáčkám	omáčka	k1gFnPc3	omáčka
<g/>
.	.	kIx.	.
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
při	při	k7c6	při
zrakové	zrakový	k2eAgFnSc6d1	zraková
únavě	únava	k1gFnSc6	únava
a	a	k8xC	a
šerosleposti	šeroslepost	k1gFnSc6	šeroslepost
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
přináší	přinášet	k5eAaImIp3nS	přinášet
do	do	k7c2	do
kůže	kůže	k1gFnSc2	kůže
a	a	k8xC	a
vlasů	vlas	k1gInPc2	vlas
barvu	barva	k1gFnSc4	barva
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
působí	působit	k5eAaImIp3nP	působit
preventivně	preventivně	k6eAd1	preventivně
proti	proti	k7c3	proti
infekcím	infekce	k1gFnPc3	infekce
a	a	k8xC	a
nemocem	nemoc	k1gFnPc3	nemoc
z	z	k7c2	z
nachlazení	nachlazení	k1gNnSc2	nachlazení
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
uklidňuje	uklidňovat	k5eAaImIp3nS	uklidňovat
nervy	nerv	k1gInPc1	nerv
<g/>
,	,	kIx,	,
dodává	dodávat	k5eAaImIp3nS	dodávat
sílu	síla	k1gFnSc4	síla
ve	v	k7c6	v
stresových	stresový	k2eAgFnPc6d1	stresová
situacích	situace	k1gFnPc6	situace
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc4	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Tabulka	tabulka	k1gFnSc1	tabulka
udává	udávat	k5eAaImIp3nS	udávat
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
průměrný	průměrný	k2eAgInSc1d1	průměrný
obsah	obsah	k1gInSc1	obsah
živin	živina	k1gFnPc2	živina
<g/>
,	,	kIx,	,
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
vitamínů	vitamín	k1gInPc2	vitamín
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
nutričních	nutriční	k2eAgInPc2d1	nutriční
parametrů	parametr	k1gInPc2	parametr
zjištěných	zjištěný	k2eAgInPc2d1	zjištěný
v	v	k7c6	v
plodech	plod	k1gInPc6	plod
manga	mango	k1gNnSc2	mango
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
mango	mango	k1gNnSc1	mango
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnSc2	Wikimedium
Commons	Commons	k1gInSc4	Commons
Galerie	galerie	k1gFnSc2	galerie
mango	mango	k1gNnSc4	mango
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
mango	mango	k1gNnSc4	mango
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
