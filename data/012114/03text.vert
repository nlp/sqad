<p>
<s>
Asie	Asie	k1gFnSc1	Asie
je	být	k5eAaImIp3nS	být
svou	svůj	k3xOyFgFnSc7	svůj
rozlohou	rozloha	k1gFnSc7	rozloha
44	[number]	k4	44
603	[number]	k4	603
853	[number]	k4	853
km2	km2	k4	km2
největší	veliký	k2eAgInPc1d3	veliký
<g/>
,	,	kIx,	,
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
4	[number]	k4	4
miliardami	miliarda	k4xCgFnPc7	miliarda
obyvatel	obyvatel	k1gMnPc2	obyvatel
nejlidnatější	lidnatý	k2eAgInPc1d3	nejlidnatější
a	a	k8xC	a
zhruba	zhruba	k6eAd1	zhruba
od	od	k7c2	od
přelomu	přelom	k1gInSc2	přelom
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
rovněž	rovněž	k9	rovněž
nejhustěji	husto	k6eAd3	husto
osídlený	osídlený	k2eAgInSc1d1	osídlený
světadíl	světadíl	k1gInSc1	světadíl
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
součást	součást	k1gFnSc4	součást
kontinentu	kontinent	k1gInSc2	kontinent
zvaného	zvaný	k2eAgInSc2d1	zvaný
Eurasie	Eurasie	k1gFnPc4	Eurasie
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
Eurafrasie	Eurafrasie	k1gFnSc1	Eurafrasie
<g/>
.	.	kIx.	.
</s>
<s>
Rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
se	se	k3xPyFc4	se
především	především	k9	především
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
a	a	k8xC	a
severní	severní	k2eAgFnSc6d1	severní
polokouli	polokoule	k1gFnSc6	polokoule
<g/>
.	.	kIx.	.
</s>
<s>
Pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
8,6	[number]	k4	8,6
%	%	kIx~	%
povrchu	povrch	k1gInSc2	povrch
Země	zem	k1gFnSc2	zem
(	(	kIx(	(
<g/>
29,9	[number]	k4	29,9
%	%	kIx~	%
souše	souš	k1gFnSc2	souš
<g/>
)	)	kIx)	)
a	a	k8xC	a
její	její	k3xOp3gNnSc1	její
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
představuje	představovat	k5eAaImIp3nS	představovat
60	[number]	k4	60
%	%	kIx~	%
světové	světový	k2eAgFnSc2d1	světová
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
počet	počet	k1gInSc1	počet
lidí	člověk	k1gMnPc2	člověk
žijících	žijící	k2eAgMnPc2d1	žijící
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
téměř	téměř	k6eAd1	téměř
zčtyřnásobil	zčtyřnásobit	k5eAaPmAgMnS	zčtyřnásobit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hranice	hranice	k1gFnSc1	hranice
Asie	Asie	k1gFnSc2	Asie
==	==	k?	==
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
Viz	vidět	k5eAaImRp2nS	vidět
též	též	k9	též
Hranice	hranice	k1gFnSc1	hranice
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nejzazší	zadní	k2eAgInPc1d3	nejzazší
body	bod	k1gInPc1	bod
asijské	asijský	k2eAgFnSc2d1	asijská
pevniny	pevnina	k1gFnSc2	pevnina
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Sever	sever	k1gInSc1	sever
<g/>
:	:	kIx,	:
Čeljuskinův	Čeljuskinův	k2eAgInSc1d1	Čeljuskinův
mys	mys	k1gInSc1	mys
(	(	kIx(	(
<g/>
77	[number]	k4	77
<g/>
°	°	k?	°
44	[number]	k4	44
<g/>
'	'	kIx"	'
s.	s.	k?	s.
š.	š.	k?	š.
<g/>
)	)	kIx)	)
na	na	k7c6	na
poloostrově	poloostrov	k1gInSc6	poloostrov
Tajmyr	Tajmyr	k1gInSc1	Tajmyr
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
</s>
</p>
<p>
<s>
Východ	východ	k1gInSc1	východ
<g/>
:	:	kIx,	:
Děžňovův	Děžňovův	k2eAgInSc1d1	Děžňovův
mys	mys	k1gInSc1	mys
(	(	kIx(	(
<g/>
169	[number]	k4	169
<g/>
°	°	k?	°
40	[number]	k4	40
<g/>
'	'	kIx"	'
z.	z.	k?	z.
d.	d.	k?	d.
<g/>
)	)	kIx)	)
na	na	k7c6	na
poloostrově	poloostrov	k1gInSc6	poloostrov
Čukotka	Čukotka	k1gFnSc1	Čukotka
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
</s>
</p>
<p>
<s>
Jih	jih	k1gInSc1	jih
<g/>
:	:	kIx,	:
Mys	mys	k1gInSc1	mys
Buru	Buru	k?	Buru
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
°	°	k?	°
25	[number]	k4	25
<g/>
'	'	kIx"	'
s.	s.	k?	s.
š.	š.	k?	š.
<g/>
)	)	kIx)	)
na	na	k7c6	na
Malajském	malajský	k2eAgInSc6d1	malajský
poloostrově	poloostrov	k1gInSc6	poloostrov
</s>
</p>
<p>
<s>
Západ	západ	k1gInSc1	západ
<g/>
:	:	kIx,	:
Mys	mys	k1gInSc1	mys
Baba	baba	k1gFnSc1	baba
(	(	kIx(	(
<g/>
26	[number]	k4	26
<g/>
°	°	k?	°
3	[number]	k4	3
<g/>
'	'	kIx"	'
v.	v.	k?	v.
d.	d.	k?	d.
<g/>
)	)	kIx)	)
v	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
DardanelVzhledem	DardanelVzhled	k1gInSc7	DardanelVzhled
k	k	k7c3	k
pozemnímu	pozemní	k2eAgNnSc3d1	pozemní
spojení	spojení	k1gNnSc3	spojení
Asie	Asie	k1gFnSc2	Asie
s	s	k7c7	s
Evropou	Evropa	k1gFnSc7	Evropa
i	i	k8xC	i
Afrikou	Afrika	k1gFnSc7	Afrika
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
rozcházejí	rozcházet	k5eAaImIp3nP	rozcházet
názory	názor	k1gInPc1	názor
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
kudy	kudy	k6eAd1	kudy
vést	vést	k5eAaImF	vést
hranici	hranice	k1gFnSc4	hranice
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgInPc7	tento
kontinenty	kontinent	k1gInPc7	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Rozšířená	rozšířený	k2eAgFnSc1d1	rozšířená
a	a	k8xC	a
na	na	k7c6	na
českých	český	k2eAgFnPc6d1	Česká
školách	škola	k1gFnPc6	škola
vyučovaná	vyučovaný	k2eAgFnSc1d1	vyučovaná
verze	verze	k1gFnSc1	verze
hranice	hranice	k1gFnSc1	hranice
mezi	mezi	k7c7	mezi
Evropou	Evropa	k1gFnSc7	Evropa
a	a	k8xC	a
Asií	Asie	k1gFnSc7	Asie
vede	vést	k5eAaImIp3nS	vést
z	z	k7c2	z
Bajdarackého	Bajdaracký	k2eAgInSc2d1	Bajdaracký
zálivu	záliv	k1gInSc2	záliv
po	po	k7c6	po
východním	východní	k2eAgNnSc6d1	východní
úpatí	úpatí	k1gNnSc6	úpatí
Uralu	Ural	k1gInSc2	Ural
(	(	kIx(	(
<g/>
Ural	Ural	k1gInSc1	Ural
leží	ležet	k5eAaImIp3nS	ležet
celý	celý	k2eAgInSc1d1	celý
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
po	po	k7c6	po
řece	řeka	k1gFnSc6	řeka
Embě	Emba	k1gFnSc6	Emba
<g/>
,	,	kIx,	,
po	po	k7c6	po
pobřeží	pobřeží	k1gNnSc6	pobřeží
Kaspického	kaspický	k2eAgNnSc2d1	Kaspické
moře	moře	k1gNnSc2	moře
a	a	k8xC	a
Kumomanyčskou	Kumomanyčský	k2eAgFnSc7d1	Kumomanyčský
sníženinou	sníženina	k1gFnSc7	sníženina
na	na	k7c4	na
sever	sever	k1gInSc4	sever
od	od	k7c2	od
Kavkazu	Kavkaz	k1gInSc2	Kavkaz
(	(	kIx(	(
<g/>
Kavkaz	Kavkaz	k1gInSc1	Kavkaz
leží	ležet	k5eAaImIp3nS	ležet
už	už	k6eAd1	už
celý	celý	k2eAgInSc1d1	celý
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
<g/>
)	)	kIx)	)
do	do	k7c2	do
Azovského	azovský	k2eAgNnSc2d1	Azovské
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Dale	Dale	k1gFnSc1	Dale
k	k	k7c3	k
jihozápadu	jihozápad	k1gInSc3	jihozápad
oba	dva	k4xCgInPc4	dva
světadíly	světadíl	k1gInPc4	světadíl
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
Černé	Černé	k2eAgNnSc1d1	Černé
moře	moře	k1gNnSc1	moře
<g/>
,	,	kIx,	,
průliv	průliv	k1gInSc1	průliv
Bospor	Bospor	k1gInSc1	Bospor
<g/>
,	,	kIx,	,
Marmarské	Marmarský	k2eAgNnSc4d1	Marmarské
moře	moře	k1gNnSc4	moře
a	a	k8xC	a
průliv	průliv	k1gInSc4	průliv
Dardanely	Dardanely	k1gFnPc4	Dardanely
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
obou	dva	k4xCgInPc2	dva
světadílů	světadíl	k1gInPc2	světadíl
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
území	území	k1gNnSc1	území
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
Kazachstánu	Kazachstán	k1gInSc2	Kazachstán
a	a	k8xC	a
Turecka	Turecko	k1gNnSc2	Turecko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
Afriky	Afrika	k1gFnSc2	Afrika
je	být	k5eAaImIp3nS	být
Asie	Asie	k1gFnSc1	Asie
oddělena	oddělit	k5eAaPmNgFnS	oddělit
Suezským	suezský	k2eAgInSc7d1	suezský
průplavem	průplav	k1gInSc7	průplav
<g/>
.	.	kIx.	.
</s>
<s>
Sinajský	sinajský	k2eAgInSc1d1	sinajský
poloostrov	poloostrov	k1gInSc1	poloostrov
už	už	k6eAd1	už
tedy	tedy	k9	tedy
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
a	a	k8xC	a
Egypt	Egypt	k1gInSc1	Egypt
tak	tak	k6eAd1	tak
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
částí	část	k1gFnSc7	část
území	území	k1gNnSc2	území
do	do	k7c2	do
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
částí	část	k1gFnPc2	část
do	do	k7c2	do
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
na	na	k7c6	na
jih	jih	k1gInSc1	jih
oba	dva	k4xCgInPc4	dva
světadíly	světadíl	k1gInPc4	světadíl
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
Rudé	rudý	k2eAgNnSc1d1	Rudé
moře	moře	k1gNnSc1	moře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ani	ani	k8xC	ani
hranici	hranice	k1gFnSc4	hranice
s	s	k7c7	s
Austrálií	Austrálie	k1gFnSc7	Austrálie
a	a	k8xC	a
Oceánií	Oceánie	k1gFnSc7	Oceánie
není	být	k5eNaImIp3nS	být
snadné	snadný	k2eAgNnSc1d1	snadné
stanovit	stanovit	k5eAaPmF	stanovit
<g/>
.	.	kIx.	.
</s>
<s>
Australská	australský	k2eAgFnSc1d1	australská
pevnina	pevnina	k1gFnSc1	pevnina
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
asijské	asijský	k2eAgNnSc1d1	asijské
nedotýká	dotýkat	k5eNaImIp3nS	dotýkat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
leží	ležet	k5eAaImIp3nS	ležet
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
,	,	kIx,	,
u	u	k7c2	u
nichž	jenž	k3xRgInPc2	jenž
není	být	k5eNaImIp3nS	být
vždy	vždy	k6eAd1	vždy
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yRgInSc3	který
světadílu	světadíl	k1gInSc3	světadíl
je	být	k5eAaImIp3nS	být
přiřadit	přiřadit	k5eAaPmF	přiřadit
<g/>
.	.	kIx.	.
</s>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
Nová	nový	k2eAgFnSc1d1	nová
Guinea	Guinea	k1gFnSc1	Guinea
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
řadí	řadit	k5eAaImIp3nS	řadit
k	k	k7c3	k
Oceánii	Oceánie	k1gFnSc3	Oceánie
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
území	území	k1gNnSc1	území
Indonésie	Indonésie	k1gFnSc2	Indonésie
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
do	do	k7c2	do
obou	dva	k4xCgInPc2	dva
světadílů	světadíl	k1gInPc2	světadíl
<g/>
.	.	kIx.	.
</s>
<s>
Zoogeografové	Zoogeograf	k1gMnPc1	Zoogeograf
pracují	pracovat	k5eAaImIp3nP	pracovat
s	s	k7c7	s
tzv.	tzv.	kA	tzv.
Wallaceovou	Wallaceův	k2eAgFnSc7d1	Wallaceova
linií	linie	k1gFnSc7	linie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
ekosystémy	ekosystém	k1gInPc4	ekosystém
považované	považovaný	k2eAgFnSc2d1	považovaná
za	za	k7c4	za
čistě	čistě	k6eAd1	čistě
asijské	asijský	k2eAgMnPc4d1	asijský
od	od	k7c2	od
smíšených	smíšený	k2eAgInPc2d1	smíšený
asijsko-australských	asijskoustralský	k2eAgInPc2d1	asijsko-australský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejjasněji	jasně	k6eAd3	jasně
vymezená	vymezený	k2eAgFnSc1d1	vymezená
je	být	k5eAaImIp3nS	být
hranice	hranice	k1gFnSc1	hranice
Asie	Asie	k1gFnSc2	Asie
a	a	k8xC	a
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
jsou	být	k5eAaImIp3nP	být
odděleny	oddělen	k2eAgFnPc1d1	oddělena
88	[number]	k4	88
km	km	kA	km
širokou	široký	k2eAgFnSc7d1	široká
Beringovou	Beringův	k2eAgFnSc7d1	Beringova
úžinou	úžina	k1gFnSc7	úžina
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
k	k	k7c3	k
sobě	se	k3xPyFc3	se
blíží	blížit	k5eAaImIp3nS	blížit
asijská	asijský	k2eAgFnSc1d1	asijská
Čukotka	Čukotka	k1gFnSc1	Čukotka
a	a	k8xC	a
americká	americký	k2eAgFnSc1d1	americká
Aljaška	Aljaška	k1gFnSc1	Aljaška
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejzazší	zadní	k2eAgInPc1d3	nejzazší
body	bod	k1gInPc1	bod
na	na	k7c6	na
ostrovech	ostrov	k1gInPc6	ostrov
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Sever	sever	k1gInSc1	sever
<g/>
:	:	kIx,	:
Arktický	arktický	k2eAgInSc1d1	arktický
mys	mys	k1gInSc1	mys
(	(	kIx(	(
<g/>
81	[number]	k4	81
<g/>
°	°	k?	°
16	[number]	k4	16
<g/>
'	'	kIx"	'
s.	s.	k?	s.
š.	š.	k?	š.
<g/>
)	)	kIx)	)
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Komsomolec	komsomolec	k1gMnSc1	komsomolec
v	v	k7c6	v
ruském	ruský	k2eAgNnSc6d1	ruské
souostroví	souostroví	k1gNnSc6	souostroví
Severní	severní	k2eAgFnSc2d1	severní
země	zem	k1gFnSc2	zem
</s>
</p>
<p>
<s>
Východ	východ	k1gInSc1	východ
<g/>
:	:	kIx,	:
Uprostřed	uprostřed	k7c2	uprostřed
Beringova	Beringův	k2eAgInSc2d1	Beringův
průlivu	průliv	k1gInSc2	průliv
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
rusko-americké	ruskomerický	k2eAgNnSc4d1	rusko-americké
souostroví	souostroví	k1gNnSc4	souostroví
Diomédovy	Diomédův	k2eAgInPc4d1	Diomédův
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
fyzickogeograficky	fyzickogeograficky	k6eAd1	fyzickogeograficky
nepatří	patřit	k5eNaImIp3nP	patřit
jednoznačně	jednoznačně	k6eAd1	jednoznačně
ani	ani	k8xC	ani
k	k	k7c3	k
Asii	Asie	k1gFnSc3	Asie
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
k	k	k7c3	k
Americe	Amerika	k1gFnSc3	Amerika
(	(	kIx(	(
<g/>
západní	západní	k2eAgInSc4d1	západní
ostrov	ostrov	k1gInSc4	ostrov
leží	ležet	k5eAaImIp3nS	ležet
asi	asi	k9	asi
35	[number]	k4	35
km	km	kA	km
od	od	k7c2	od
asijské	asijský	k2eAgFnSc2d1	asijská
pevniny	pevnina	k1gFnSc2	pevnina
<g/>
,	,	kIx,	,
východní	východní	k2eAgInSc4d1	východní
ostrov	ostrov	k1gInSc4	ostrov
asi	asi	k9	asi
36	[number]	k4	36
km	km	kA	km
od	od	k7c2	od
americké	americký	k2eAgFnSc2d1	americká
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
politicky	politicky	k6eAd1	politicky
je	být	k5eAaImIp3nS	být
však	však	k9	však
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
mezi	mezi	k7c4	mezi
Rusko	Rusko	k1gNnSc4	Rusko
a	a	k8xC	a
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
<g/>
.	.	kIx.	.
</s>
<s>
Tříkilometrovým	tříkilometrový	k2eAgInSc7d1	tříkilometrový
průlivem	průliv	k1gInSc7	průliv
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgInPc7	dva
ostrovy	ostrov	k1gInPc7	ostrov
také	také	k6eAd1	také
probíhá	probíhat	k5eAaImIp3nS	probíhat
Datová	datový	k2eAgFnSc1d1	datová
hranice	hranice	k1gFnSc1	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Přiřadíme	přiřadit	k5eAaPmIp1nP	přiřadit
<g/>
-li	i	k?	-li
ruský	ruský	k2eAgInSc1d1	ruský
ostrov	ostrov	k1gInSc1	ostrov
k	k	k7c3	k
Asii	Asie	k1gFnSc3	Asie
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
jeho	jeho	k3xOp3gInSc1	jeho
východní	východní	k2eAgInSc1d1	východní
okraj	okraj	k1gInSc4	okraj
nejvýchodnějším	východní	k2eAgInSc7d3	nejvýchodnější
bodem	bod	k1gInSc7	bod
světadílu	světadíl	k1gInSc2	světadíl
(	(	kIx(	(
<g/>
169	[number]	k4	169
<g/>
°	°	k?	°
0	[number]	k4	0
<g/>
'	'	kIx"	'
z.	z.	k?	z.
d.	d.	k?	d.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jih	jih	k1gInSc1	jih
<g/>
:	:	kIx,	:
Záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
ostrovy	ostrov	k1gInPc1	ostrov
ještě	ještě	k9	ještě
přiřadíme	přiřadit	k5eAaPmIp1nP	přiřadit
k	k	k7c3	k
Asii	Asie	k1gFnSc3	Asie
a	a	k8xC	a
které	který	k3yRgNnSc1	který
již	již	k6eAd1	již
k	k	k7c3	k
Austrálii	Austrálie	k1gFnSc3	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
nejjednodušší	jednoduchý	k2eAgFnSc6d3	nejjednodušší
variantě	varianta	k1gFnSc6	varianta
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
hranice	hranice	k1gFnSc1	hranice
vede	vést	k5eAaImIp3nS	vést
mezi	mezi	k7c7	mezi
Malajským	malajský	k2eAgNnSc7d1	Malajské
souostrovím	souostroví	k1gNnSc7	souostroví
na	na	k7c6	na
asijské	asijský	k2eAgFnSc6d1	asijská
straně	strana	k1gFnSc6	strana
a	a	k8xC	a
ostrovy	ostrov	k1gInPc1	ostrov
Ashmore	Ashmor	k1gInSc5	Ashmor
a	a	k8xC	a
Cartier	Cartier	k1gInSc1	Cartier
na	na	k7c4	na
australské	australský	k2eAgFnPc4d1	australská
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nejjižnější	jižní	k2eAgInSc1d3	nejjižnější
bod	bod	k1gInSc1	bod
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
ostrůvku	ostrůvek	k1gInSc6	ostrůvek
u	u	k7c2	u
indonéského	indonéský	k2eAgInSc2d1	indonéský
ostrova	ostrov	k1gInSc2	ostrov
Rote	Rot	k1gFnSc2	Rot
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
°	°	k?	°
1	[number]	k4	1
<g/>
'	'	kIx"	'
j.	j.	k?	j.
š.	š.	k?	š.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Západ	západ	k1gInSc1	západ
<g/>
:	:	kIx,	:
I	i	k9	i
v	v	k7c6	v
Egejském	egejský	k2eAgNnSc6d1	Egejské
moři	moře	k1gNnSc6	moře
leží	ležet	k5eAaImIp3nS	ležet
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgInPc4d1	mnohý
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
leží	ležet	k5eAaImIp3nS	ležet
těsně	těsně	k6eAd1	těsně
u	u	k7c2	u
pobřeží	pobřeží	k1gNnSc2	pobřeží
Malé	Malé	k2eAgFnSc2d1	Malé
Asie	Asie	k1gFnSc2	Asie
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Samos	Samos	k1gInSc4	Samos
1,6	[number]	k4	1,6
km	km	kA	km
daleko	daleko	k6eAd1	daleko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
politicky	politicky	k6eAd1	politicky
náleží	náležet	k5eAaImIp3nP	náležet
Řecku	Řecko	k1gNnSc3	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Zohlednění	zohlednění	k1gNnSc1	zohlednění
jejich	jejich	k3xOp3gFnSc2	jejich
geologické	geologický	k2eAgFnSc2d1	geologická
spřízněnosti	spřízněnost	k1gFnSc2	spřízněnost
s	s	k7c7	s
asijskou	asijský	k2eAgFnSc7d1	asijská
pevninou	pevnina	k1gFnSc7	pevnina
by	by	kYmCp3nS	by
zřejmě	zřejmě	k6eAd1	zřejmě
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
posunout	posunout	k5eAaPmF	posunout
hranici	hranice	k1gFnSc4	hranice
až	až	k9	až
na	na	k7c4	na
západní	západní	k2eAgNnSc4d1	západní
pobřeží	pobřeží	k1gNnSc4	pobřeží
ostrovů	ostrov	k1gInPc2	ostrov
Lesbos	Lesbos	k1gInSc1	Lesbos
a	a	k8xC	a
Chios	Chios	k1gInSc1	Chios
(	(	kIx(	(
<g/>
25	[number]	k4	25
<g/>
°	°	k?	°
50	[number]	k4	50
<g/>
'	'	kIx"	'
v.	v.	k?	v.
d.	d.	k?	d.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
ale	ale	k9	ale
znamenalo	znamenat	k5eAaImAgNnS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
část	část	k1gFnSc1	část
řeckého	řecký	k2eAgNnSc2d1	řecké
území	území	k1gNnSc2	území
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
se	s	k7c7	s
všeobecně	všeobecně	k6eAd1	všeobecně
přijímaným	přijímaný	k2eAgInSc7d1	přijímaný
názorem	názor	k1gInSc7	názor
<g/>
.	.	kIx.	.
</s>
<s>
Nejzápadnější	západní	k2eAgInSc1d3	nejzápadnější
ostrov	ostrov	k1gInSc1	ostrov
pod	pod	k7c7	pod
tureckou	turecký	k2eAgFnSc7d1	turecká
správou	správa	k1gFnSc7	správa
je	být	k5eAaImIp3nS	být
Gökçeada	Gökçeada	k1gFnSc1	Gökçeada
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
má	mít	k5eAaImIp3nS	mít
ale	ale	k9	ale
zřetelně	zřetelně	k6eAd1	zřetelně
blíž	blízce	k6eAd2	blízce
k	k	k7c3	k
evropské	evropský	k2eAgFnSc3d1	Evropská
než	než	k8xS	než
k	k	k7c3	k
asijské	asijský	k2eAgFnSc3d1	asijská
části	část	k1gFnSc3	část
Turecka	Turecko	k1gNnSc2	Turecko
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
nejzápadnější	západní	k2eAgFnSc4d3	nejzápadnější
výspu	výspa	k1gFnSc4	výspa
Asie	Asie	k1gFnSc2	Asie
lze	lze	k6eAd1	lze
tedy	tedy	k8xC	tedy
buď	buď	k8xC	buď
považovat	považovat	k5eAaImF	považovat
ostrov	ostrov	k1gInSc4	ostrov
Bozcaada	Bozcaada	k1gFnSc1	Bozcaada
(	(	kIx(	(
<g/>
25	[number]	k4	25
<g/>
°	°	k?	°
58	[number]	k4	58
<g/>
'	'	kIx"	'
v.	v.	k?	v.
d.	d.	k?	d.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
prohlásit	prohlásit	k5eAaPmF	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
veškeré	veškerý	k3xTgInPc1	veškerý
egejské	egejský	k2eAgInPc1d1	egejský
ostrovy	ostrov	k1gInPc1	ostrov
už	už	k6eAd1	už
patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
Evropě	Evropa	k1gFnSc3	Evropa
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
od	od	k7c2	od
asijské	asijský	k2eAgFnSc2d1	asijská
pevniny	pevnina	k1gFnSc2	pevnina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Asijské	asijský	k2eAgInPc4d1	asijský
geografické	geografický	k2eAgInPc4d1	geografický
rekordy	rekord	k1gInPc4	rekord
==	==	k?	==
</s>
</p>
<p>
<s>
Mnohé	mnohý	k2eAgInPc1d1	mnohý
asijské	asijský	k2eAgInPc1d1	asijský
geografické	geografický	k2eAgInPc1d1	geografický
rekordy	rekord	k1gInPc1	rekord
jsou	být	k5eAaImIp3nP	být
současně	současně	k6eAd1	současně
světovými	světový	k2eAgInPc7d1	světový
rekordy	rekord	k1gInPc7	rekord
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
<g/>
:	:	kIx,	:
Mount	Mount	k1gInSc1	Mount
Everest	Everest	k1gInSc1	Everest
–	–	k?	–
8848	[number]	k4	8848
m	m	kA	m
(	(	kIx(	(
<g/>
Nepál	Nepál	k1gInSc1	Nepál
<g/>
,	,	kIx,	,
Čína	Čína	k1gFnSc1	Čína
<g/>
,	,	kIx,	,
Himálaj	Himálaj	k1gFnSc1	Himálaj
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
současně	současně	k6eAd1	současně
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
horou	hora	k1gFnSc7	hora
světa	svět	k1gInSc2	svět
</s>
</p>
<p>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
činná	činný	k2eAgFnSc1d1	činná
sopka	sopka	k1gFnSc1	sopka
<g/>
:	:	kIx,	:
Ključevskaja	Ključevskaja	k1gFnSc1	Ključevskaja
–	–	k?	–
4750	[number]	k4	4750
m	m	kA	m
(	(	kIx(	(
<g/>
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
,	,	kIx,	,
Kamčatka	Kamčatka	k1gFnSc1	Kamčatka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nejnižší	nízký	k2eAgInSc1d3	nejnižší
bod	bod	k1gInSc1	bod
<g/>
:	:	kIx,	:
hladina	hladina	k1gFnSc1	hladina
Mrtvého	mrtvý	k2eAgNnSc2d1	mrtvé
moře	moře	k1gNnSc2	moře
–	–	k?	–
400	[number]	k4	400
m	m	kA	m
pod	pod	k7c7	pod
úrovní	úroveň	k1gFnSc7	úroveň
světového	světový	k2eAgInSc2d1	světový
oceánu	oceán	k1gInSc2	oceán
(	(	kIx(	(
<g/>
Izrael	Izrael	k1gInSc1	Izrael
<g/>
,	,	kIx,	,
Jordánsko	Jordánsko	k1gNnSc1	Jordánsko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nejhlubší	hluboký	k2eAgFnSc1d3	nejhlubší
kryptodeprese	kryptodeprese	k1gFnSc1	kryptodeprese
<g/>
:	:	kIx,	:
Bajkal	Bajkal	k1gInSc1	Bajkal
–	–	k?	–
dno	dno	k1gNnSc1	dno
−	−	k?	−
m	m	kA	m
pod	pod	k7c7	pod
úrovní	úroveň	k1gFnSc7	úroveň
hladiny	hladina	k1gFnSc2	hladina
oceánu	oceán	k1gInSc2	oceán
(	(	kIx(	(
<g/>
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
současně	současně	k6eAd1	současně
nejhlubší	hluboký	k2eAgFnSc7d3	nejhlubší
kryptodepresí	kryptodeprese	k1gFnSc7	kryptodeprese
na	na	k7c6	na
světě	svět	k1gInSc6	svět
</s>
</p>
<p>
<s>
Nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
řeka	řeka	k1gFnSc1	řeka
<g/>
:	:	kIx,	:
Jang-c	Jang	k1gFnSc1	Jang-c
<g/>
'	'	kIx"	'
<g/>
-ťiang	-ťiang	k1gInSc1	-ťiang
–	–	k?	–
6379	[number]	k4	6379
km	km	kA	km
(	(	kIx(	(
<g/>
Čína	Čína	k1gFnSc1	Čína
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
3	[number]	k4	3
<g/>
.	.	kIx.	.
nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
řeka	řeka	k1gFnSc1	řeka
světa	svět	k1gInSc2	svět
a	a	k8xC	a
s	s	k7c7	s
průtokem	průtok	k1gInSc7	průtok
31	[number]	k4	31
900	[number]	k4	900
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
4	[number]	k4	4
<g/>
.	.	kIx.	.
nejvodnatější	vodnatý	k2eAgInSc1d3	nejvodnatější
tok	tok	k1gInSc1	tok
této	tento	k3xDgFnSc2	tento
planety	planeta	k1gFnSc2	planeta
</s>
</p>
<p>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
ostrov	ostrov	k1gInSc1	ostrov
<g/>
:	:	kIx,	:
Kalimantan	Kalimantan	k1gInSc1	Kalimantan
nebo	nebo	k8xC	nebo
také	také	k9	také
(	(	kIx(	(
<g/>
Borneo	Borneo	k1gNnSc1	Borneo
<g/>
)	)	kIx)	)
–	–	k?	–
725	[number]	k4	725
460	[number]	k4	460
km2	km2	k4	km2
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
největší	veliký	k2eAgInSc1d3	veliký
ostrov	ostrov	k1gInSc1	ostrov
světa	svět	k1gInSc2	svět
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
poloostrov	poloostrov	k1gInSc1	poloostrov
<g/>
:	:	kIx,	:
Arabský	arabský	k2eAgInSc1d1	arabský
poloostrov	poloostrov	k1gInSc1	poloostrov
–	–	k?	–
2	[number]	k4	2
780	[number]	k4	780
000	[number]	k4	000
km2	km2	k4	km2
</s>
</p>
<p>
<s>
Nejvzdálenější	vzdálený	k2eAgNnSc1d3	nejvzdálenější
místo	místo	k1gNnSc1	místo
od	od	k7c2	od
moře	moře	k1gNnSc2	moře
<g/>
:	:	kIx,	:
Džungarská	Džungarský	k2eAgFnSc1d1	Džungarská
pánev	pánev	k1gFnSc1	pánev
(	(	kIx(	(
<g/>
Čína	Čína	k1gFnSc1	Čína
<g/>
)	)	kIx)	)
–	–	k?	–
2650	[number]	k4	2650
km	km	kA	km
od	od	k7c2	od
Severního	severní	k2eAgNnSc2d1	severní
ledového	ledový	k2eAgNnSc2d1	ledové
i	i	k9	i
od	od	k7c2	od
Indického	indický	k2eAgInSc2d1	indický
oceánu	oceán	k1gInSc2	oceán
je	být	k5eAaImIp3nS	být
současně	současně	k6eAd1	současně
nejvnitrozemštějším	vnitrozemský	k2eAgInSc7d3	vnitrozemský
bodem	bod	k1gInSc7	bod
na	na	k7c6	na
světě	svět	k1gInSc6	svět
</s>
</p>
<p>
<s>
Největší	veliký	k2eAgNnSc1d3	veliký
jezero	jezero	k1gNnSc1	jezero
<g/>
:	:	kIx,	:
Kaspické	kaspický	k2eAgNnSc1d1	Kaspické
moře	moře	k1gNnSc1	moře
–	–	k?	–
371	[number]	k4	371
000	[number]	k4	000
km2	km2	k4	km2
(	(	kIx(	(
<g/>
největší	veliký	k2eAgNnSc1d3	veliký
jezero	jezero	k1gNnSc1	jezero
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nejhlubší	hluboký	k2eAgNnSc1d3	nejhlubší
jezero	jezero	k1gNnSc1	jezero
<g/>
:	:	kIx,	:
Bajkal	Bajkal	k1gInSc1	Bajkal
–	–	k?	–
1642	[number]	k4	1642
m	m	kA	m
(	(	kIx(	(
<g/>
nejhlubší	hluboký	k2eAgNnSc1d3	nejhlubší
jezero	jezero	k1gNnSc1	jezero
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nejlidnatější	lidnatý	k2eAgInSc1d3	nejlidnatější
stát	stát	k1gInSc1	stát
<g/>
:	:	kIx,	:
Čína	Čína	k1gFnSc1	Čína
–	–	k?	–
1	[number]	k4	1
348	[number]	k4	348
083	[number]	k4	083
800	[number]	k4	800
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
nejlidnatější	lidnatý	k2eAgInSc1d3	nejlidnatější
stát	stát	k1gInSc1	stát
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
stát	stát	k1gInSc1	stát
<g/>
:	:	kIx,	:
Rusko	Rusko	k1gNnSc1	Rusko
–	–	k?	–
13	[number]	k4	13
200	[number]	k4	200
000	[number]	k4	000
km2	km2	k4	km2
(	(	kIx(	(
<g/>
asijská	asijský	k2eAgFnSc1d1	asijská
část	část	k1gFnSc1	část
<g/>
;	;	kIx,	;
celé	celý	k2eAgNnSc1d1	celé
Rusko	Rusko	k1gNnSc1	Rusko
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgInSc1d3	veliký
stát	stát	k1gInSc1	stát
světa	svět	k1gInSc2	svět
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Geologie	geologie	k1gFnSc2	geologie
==	==	k?	==
</s>
</p>
<p>
<s>
Asie	Asie	k1gFnSc1	Asie
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
spojením	spojení	k1gNnSc7	spojení
několika	několik	k4yIc3	několik
platforem	platforma	k1gFnPc2	platforma
<g/>
.	.	kIx.	.
</s>
<s>
Sibiřská	sibiřský	k2eAgFnSc1d1	sibiřská
platforma	platforma	k1gFnSc1	platforma
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
samostatným	samostatný	k2eAgInSc7d1	samostatný
kontinentem	kontinent	k1gInSc7	kontinent
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
spojil	spojit	k5eAaPmAgInS	spojit
s	s	k7c7	s
Baltikou	Baltika	k1gFnSc7	Baltika
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
pohoří	pohoří	k1gNnSc1	pohoří
Ural	Ural	k1gInSc1	Ural
<g/>
.	.	kIx.	.
</s>
<s>
Čínská	čínský	k2eAgFnSc1d1	čínská
platforma	platforma	k1gFnSc1	platforma
se	se	k3xPyFc4	se
pospojovala	pospojovat	k5eAaPmAgFnS	pospojovat
z	z	k7c2	z
menších	malý	k2eAgInPc2d2	menší
bloků	blok	k1gInPc2	blok
(	(	kIx(	(
<g/>
Severní	severní	k2eAgFnSc1d1	severní
Čína	Čína	k1gFnSc1	Čína
<g/>
,	,	kIx,	,
Blok	blok	k1gInSc1	blok
Jang-c	Jang	k1gInSc1	Jang-c
<g/>
'	'	kIx"	'
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc1d1	jižní
Čína	Čína	k1gFnSc1	Čína
<g/>
)	)	kIx)	)
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
spojila	spojit	k5eAaPmAgFnS	spojit
se	s	k7c7	s
Sibiří	Sibiř	k1gFnSc7	Sibiř
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nejmladší	mladý	k2eAgFnSc6d3	nejmladší
době	doba	k1gFnSc6	doba
pak	pak	k6eAd1	pak
do	do	k7c2	do
Asie	Asie	k1gFnSc2	Asie
narazily	narazit	k5eAaPmAgInP	narazit
dvě	dva	k4xCgFnPc4	dva
platformy	platforma	k1gFnPc4	platforma
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
oddělily	oddělit	k5eAaPmAgFnP	oddělit
od	od	k7c2	od
někdejší	někdejší	k2eAgFnSc2d1	někdejší
Gondwany	Gondwana	k1gFnSc2	Gondwana
<g/>
:	:	kIx,	:
Indická	indický	k2eAgFnSc1d1	indická
a	a	k8xC	a
Arabská	arabský	k2eAgFnSc1d1	arabská
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Sibiřské	sibiřský	k2eAgFnPc1d1	sibiřská
jednotky	jednotka	k1gFnPc1	jednotka
===	===	k?	===
</s>
</p>
<p>
<s>
Sibiřská	sibiřský	k2eAgFnSc1d1	sibiřská
(	(	kIx(	(
<g/>
angarská	angarský	k2eAgFnSc1d1	angarský
<g/>
)	)	kIx)	)
platforma	platforma	k1gFnSc1	platforma
představuje	představovat	k5eAaImIp3nS	představovat
velký	velký	k2eAgInSc4d1	velký
stabilní	stabilní	k2eAgInSc4d1	stabilní
blok	blok	k1gInSc4	blok
<g/>
.	.	kIx.	.
</s>
<s>
Aldanský	Aldanský	k2eAgInSc4d1	Aldanský
a	a	k8xC	a
anabarský	anabarský	k2eAgInSc4d1	anabarský
štít	štít	k1gInSc4	štít
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
horniny	hornina	k1gFnPc1	hornina
až	až	k9	až
3,5	[number]	k4	3,5
miliardy	miliarda	k4xCgFnSc2	miliarda
let	léto	k1gNnPc2	léto
staré	starý	k2eAgFnPc1d1	stará
<g/>
.	.	kIx.	.
</s>
<s>
Člení	členit	k5eAaImIp3nS	členit
se	se	k3xPyFc4	se
následovně	následovně	k6eAd1	následovně
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Předbajkalské	Předbajkalský	k2eAgFnPc1d1	Předbajkalský
jednotky	jednotka	k1gFnPc1	jednotka
</s>
</p>
<p>
<s>
Aldanský	Aldanský	k2eAgInSc1d1	Aldanský
štít	štít	k1gInSc1	štít
</s>
</p>
<p>
<s>
Anabarský	Anabarský	k2eAgInSc1d1	Anabarský
štít	štít	k1gInSc1	štít
</s>
</p>
<p>
<s>
Vrásová	vrásový	k2eAgNnPc1d1	vrásový
pásma	pásmo	k1gNnPc1	pásmo
bajkalského	bajkalský	k2eAgNnSc2d1	Bajkalské
vrásnění	vrásnění	k1gNnSc2	vrásnění
(	(	kIx(	(
<g/>
Bajkalské	bajkalský	k2eAgNnSc1d1	Bajkalské
pohoří	pohoří	k1gNnSc1	pohoří
<g/>
,	,	kIx,	,
Jenisejské	jenisejský	k2eAgNnSc1d1	jenisejský
pohoří	pohoří	k1gNnSc1	pohoří
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Depresní	depresní	k2eAgFnPc1d1	depresní
jednotky	jednotka	k1gFnPc1	jednotka
–	–	k?	–
syneklízy	syneklíza	k1gFnSc2	syneklíza
a	a	k8xC	a
okrajové	okrajový	k2eAgInPc4d1	okrajový
prohyby	prohyb	k1gInPc4	prohyb
(	(	kIx(	(
<g/>
tunguzská	tunguzský	k2eAgFnSc1d1	Tunguzská
a	a	k8xC	a
viljujská	viljujský	k2eAgFnSc1d1	viljujský
syneklíza	syneklíza	k1gFnSc1	syneklíza
<g/>
,	,	kIx,	,
předsajansko-bajkalský	předsajanskoajkalský	k2eAgInSc1d1	předsajansko-bajkalský
prohyb	prohyb	k1gInSc1	prohyb
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Čínské	čínský	k2eAgFnPc1d1	čínská
jednotky	jednotka	k1gFnPc1	jednotka
===	===	k?	===
</s>
</p>
<p>
<s>
Čínská	čínský	k2eAgFnSc1d1	čínská
platforma	platforma	k1gFnSc1	platforma
je	být	k5eAaImIp3nS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
vystupující	vystupující	k2eAgInPc4d1	vystupující
masívy	masív	k1gInPc4	masív
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimiž	jenž	k3xRgFnPc7	jenž
leží	ležet	k5eAaImIp3nP	ležet
mladší	mladý	k2eAgFnPc4d2	mladší
jednotky	jednotka	k1gFnPc4	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
prekambrické	prekambrický	k2eAgInPc4d1	prekambrický
štíty	štít	k1gInPc4	štít
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Tarimský	Tarimský	k2eAgInSc1d1	Tarimský
štít	štít	k1gInSc1	štít
</s>
</p>
<p>
<s>
Severočínský	severočínský	k2eAgInSc1d1	severočínský
štít	štít	k1gInSc1	štít
</s>
</p>
<p>
<s>
Korejský	korejský	k2eAgInSc1d1	korejský
štít	štít	k1gInSc1	štít
</s>
</p>
<p>
<s>
Inšanský	Inšanský	k2eAgInSc4d1	Inšanský
masiv	masiv	k1gInSc4	masiv
(	(	kIx(	(
<g/>
hřbet	hřbet	k1gInSc4	hřbet
Vnitřního	vnitřní	k2eAgNnSc2d1	vnitřní
Mongolska	Mongolsko	k1gNnSc2	Mongolsko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Středočínský	Středočínský	k2eAgInSc1d1	Středočínský
blok	blok	k1gInSc1	blok
</s>
</p>
<p>
<s>
Indočínský	indočínský	k2eAgInSc1d1	indočínský
masivČínská	masivČínský	k2eAgFnSc1d1	masivČínský
platforma	platforma	k1gFnSc1	platforma
také	také	k9	také
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
řadu	řada	k1gFnSc4	řada
plošin	plošina	k1gFnPc2	plošina
a	a	k8xC	a
poklesových	poklesový	k2eAgFnPc2d1	poklesová
pánví	pánev	k1gFnPc2	pánev
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
jsou	být	k5eAaImIp3nP	být
pokryty	pokrýt	k5eAaPmNgFnP	pokrýt
paleozoickými	paleozoický	k2eAgFnPc7d1	paleozoická
<g/>
,	,	kIx,	,
mezozoickými	mezozoický	k2eAgInPc7d1	mezozoický
a	a	k8xC	a
kenozoickými	kenozoický	k2eAgInPc7d1	kenozoický
sedimenty	sediment	k1gInPc7	sediment
různého	různý	k2eAgInSc2d1	různý
charakteru	charakter	k1gInSc2	charakter
a	a	k8xC	a
mocnosti	mocnost	k1gFnSc2	mocnost
<g/>
.	.	kIx.	.
</s>
<s>
Krystalické	krystalický	k2eAgNnSc1d1	krystalické
podloží	podloží	k1gNnSc1	podloží
leží	ležet	k5eAaImIp3nS	ležet
místy	místo	k1gNnPc7	místo
ve	v	k7c6	v
větších	veliký	k2eAgFnPc6d2	veliký
hloubkách	hloubka	k1gFnPc6	hloubka
než	než	k8xS	než
10	[number]	k4	10
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pánev	pánev	k1gFnSc1	pánev
Ordos	Ordos	k1gInSc1	Ordos
(	(	kIx(	(
<g/>
kambrium	kambrium	k1gNnSc1	kambrium
až	až	k8xS	až
ordovik	ordovik	k1gInSc1	ordovik
<g/>
)	)	kIx)	)
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
mimořádně	mimořádně	k6eAd1	mimořádně
mocné	mocný	k2eAgMnPc4d1	mocný
(	(	kIx(	(
<g/>
až	až	k9	až
7	[number]	k4	7
km	km	kA	km
<g/>
)	)	kIx)	)
platformně	platformně	k6eAd1	platformně
uložené	uložený	k2eAgFnSc2d1	uložená
spraše	spraš	k1gFnSc2	spraš
</s>
</p>
<p>
<s>
Pánev	pánev	k1gFnSc1	pánev
Chuang-che	Chuanghe	k1gNnSc2	Chuang-che
</s>
</p>
<p>
<s>
Pchjongjangská	pchjongjangský	k2eAgFnSc1d1	pchjongjangský
pánev	pánev	k1gFnSc1	pánev
</s>
</p>
<p>
<s>
Sečuánská	Sečuánský	k2eAgFnSc1d1	Sečuánská
(	(	kIx(	(
<g/>
Rudá	rudý	k2eAgFnSc1d1	rudá
<g/>
,	,	kIx,	,
Červená	červený	k2eAgFnSc1d1	červená
<g/>
)	)	kIx)	)
pánev	pánev	k1gFnSc1	pánev
(	(	kIx(	(
<g/>
synklinála	synklinála	k1gFnSc1	synklinála
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
koncem	koncem	k7c2	koncem
permu	perm	k1gInSc2	perm
<g/>
,	,	kIx,	,
3	[number]	k4	3
až	až	k9	až
4	[number]	k4	4
km	km	kA	km
mocné	mocný	k2eAgFnSc2d1	mocná
akumulace	akumulace	k1gFnSc2	akumulace
z	z	k7c2	z
průběhu	průběh	k1gInSc2	průběh
celých	celý	k2eAgFnPc2d1	celá
druhohor	druhohory	k1gFnPc2	druhohory
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nanningská	Nanningský	k2eAgFnSc1d1	Nanningský
pánev	pánev	k1gFnSc1	pánev
</s>
</p>
<p>
<s>
Wutchanská	Wutchanský	k2eAgFnSc1d1	Wutchanský
pánev	pánev	k1gFnSc1	pánev
</s>
</p>
<p>
<s>
Alašanská	Alašanský	k2eAgFnSc1d1	Alašanský
pánev	pánev	k1gFnSc1	pánev
</s>
</p>
<p>
<s>
Platformní	platformní	k2eAgInPc1d1	platformní
sedimenty	sediment	k1gInPc1	sediment
v	v	k7c6	v
Indočíně	Indočína	k1gFnSc6	Indočína
(	(	kIx(	(
<g/>
perm	perm	k1gInSc1	perm
<g/>
,	,	kIx,	,
trias	trias	k1gInSc1	trias
<g/>
,	,	kIx,	,
jura	jura	k1gFnSc1	jura
<g/>
,	,	kIx,	,
křída	křída	k1gFnSc1	křída
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Kundwanská	Kundwanský	k2eAgFnSc1d1	Kundwanský
Asie	Asie	k1gFnSc1	Asie
===	===	k?	===
</s>
</p>
<p>
<s>
Indická	indický	k2eAgFnSc1d1	indická
platforma	platforma	k1gFnSc1	platforma
a	a	k8xC	a
Madagaskar	Madagaskar	k1gInSc1	Madagaskar
byly	být	k5eAaImAgInP	být
původně	původně	k6eAd1	původně
spojeny	spojit	k5eAaPmNgInP	spojit
s	s	k7c7	s
Afrikou	Afrika	k1gFnSc7	Afrika
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
třetihorách	třetihory	k1gFnPc6	třetihory
se	se	k3xPyFc4	se
vydaly	vydat	k5eAaPmAgFnP	vydat
na	na	k7c4	na
sever	sever	k1gInSc4	sever
k	k	k7c3	k
Asii	Asie	k1gFnSc3	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Madagaskar	Madagaskar	k1gInSc1	Madagaskar
se	se	k3xPyFc4	se
oddělil	oddělit	k5eAaPmAgInS	oddělit
a	a	k8xC	a
zůstal	zůstat	k5eAaPmAgInS	zůstat
poblíž	poblíž	k7c2	poblíž
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Indie	Indie	k1gFnSc1	Indie
narazila	narazit	k5eAaPmAgFnS	narazit
do	do	k7c2	do
Čínské	čínský	k2eAgFnSc2d1	čínská
platformy	platforma	k1gFnSc2	platforma
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
vyvolala	vyvolat	k5eAaPmAgFnS	vyvolat
mohutné	mohutný	k2eAgNnSc4d1	mohutné
vrásnění	vrásnění	k1gNnSc4	vrásnění
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
výsledkem	výsledek	k1gInSc7	výsledek
je	být	k5eAaImIp3nS	být
Himálaj	Himálaj	k1gFnSc1	Himálaj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
západě	západ	k1gInSc6	západ
Indické	indický	k2eAgFnSc2d1	indická
platformy	platforma	k1gFnSc2	platforma
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
vodorovně	vodorovně	k6eAd1	vodorovně
uložené	uložený	k2eAgFnPc1d1	uložená
čedičové	čedičový	k2eAgFnPc1d1	čedičová
vrstvy	vrstva	k1gFnPc1	vrstva
vulkanického	vulkanický	k2eAgInSc2d1	vulkanický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
trapy	trap	k1gInPc4	trap
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Dekánské	Dekánský	k2eAgInPc1d1	Dekánský
trapy	trap	k1gInPc1	trap
či	či	k8xC	či
sibiřské	sibiřský	k2eAgInPc1d1	sibiřský
trapy	trap	k1gInPc1	trap
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
100	[number]	k4	100
m	m	kA	m
silná	silný	k2eAgFnSc1d1	silná
vrstva	vrstva	k1gFnSc1	vrstva
čediče	čedič	k1gInSc2	čedič
překrývá	překrývat	k5eAaImIp3nS	překrývat
stará	starý	k2eAgNnPc4d1	staré
jádra	jádro	k1gNnPc4	jádro
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejstarší	starý	k2eAgNnPc1d3	nejstarší
kontinentální	kontinentální	k2eAgNnPc1d1	kontinentální
jádra	jádro	k1gNnPc1	jádro
se	se	k3xPyFc4	se
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
během	během	k7c2	během
protoindického	protoindický	k2eAgInSc2d1	protoindický
geotektonického	geotektonický	k2eAgInSc2d1	geotektonický
cyklu	cyklus	k1gInSc2	cyklus
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInPc1d3	nejstarší
metamorfity	metamorfit	k1gInPc1	metamorfit
jsou	být	k5eAaImIp3nP	být
staré	starý	k2eAgInPc1d1	starý
až	až	k9	až
3,5	[number]	k4	3,5
miliardy	miliarda	k4xCgFnPc4	miliarda
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
dhárvárského	dhárvárský	k2eAgInSc2d1	dhárvárský
geotektonického	geotektonický	k2eAgInSc2d1	geotektonický
cyklu	cyklus	k1gInSc2	cyklus
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
sedimentaci	sedimentace	k1gFnSc3	sedimentace
<g/>
,	,	kIx,	,
vulkanickým	vulkanický	k2eAgInPc3d1	vulkanický
procesům	proces	k1gInPc3	proces
a	a	k8xC	a
průnikům	průnik	k1gInPc3	průnik
granitů	granit	k1gInPc2	granit
<g/>
,	,	kIx,	,
dotvářela	dotvářet	k5eAaImAgNnP	dotvářet
se	se	k3xPyFc4	se
kontinentální	kontinentální	k2eAgNnPc1d1	kontinentální
jádra	jádro	k1gNnPc1	jádro
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Arabská	arabský	k2eAgFnSc1d1	arabská
platforma	platforma	k1gFnSc1	platforma
byla	být	k5eAaImAgFnS	být
původně	původně	k6eAd1	původně
součástí	součást	k1gFnSc7	součást
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
začala	začít	k5eAaPmAgFnS	začít
se	se	k3xPyFc4	se
od	od	k7c2	od
ní	on	k3xPp3gFnSc2	on
oddělovat	oddělovat	k5eAaImF	oddělovat
příkopovou	příkopový	k2eAgFnSc7d1	příkopová
propadlinou	propadlina	k1gFnSc7	propadlina
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
nyní	nyní	k6eAd1	nyní
vyplňuje	vyplňovat	k5eAaImIp3nS	vyplňovat
Rudé	rudý	k2eAgNnSc1d1	Rudé
moře	moře	k1gNnSc1	moře
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnPc1d3	nejstarší
horniny	hornina	k1gFnPc1	hornina
jsou	být	k5eAaImIp3nP	být
asi	asi	k9	asi
1	[number]	k4	1
miliardu	miliarda	k4xCgFnSc4	miliarda
let	léto	k1gNnPc2	léto
staré	starý	k2eAgInPc1d1	starý
a	a	k8xC	a
nacházejí	nacházet	k5eAaImIp3nP	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
haliské	haliský	k2eAgFnSc6d1	haliský
formaci	formace	k1gFnSc6	formace
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
polovinu	polovina	k1gFnSc4	polovina
platformy	platforma	k1gFnSc2	platforma
tvoří	tvořit	k5eAaImIp3nS	tvořit
prekambrické	prekambrický	k2eAgFnSc2d1	prekambrický
metamorfity	metamorfita	k1gFnSc2	metamorfita
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
polovina	polovina	k1gFnSc1	polovina
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
granitoidů	granitoid	k1gInPc2	granitoid
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
západním	západní	k2eAgInSc6d1	západní
okraji	okraj	k1gInSc6	okraj
eroduje	erodovat	k5eAaImIp3nS	erodovat
a	a	k8xC	a
klesá	klesat	k5eAaImIp3nS	klesat
do	do	k7c2	do
příkopu	příkop	k1gInSc2	příkop
Rudého	rudý	k2eAgNnSc2d1	Rudé
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Východní	východní	k2eAgInSc1d1	východní
okraj	okraj	k1gInSc1	okraj
se	se	k3xPyFc4	se
zvedá	zvedat	k5eAaImIp3nS	zvedat
i	i	k9	i
s	s	k7c7	s
akumulovanými	akumulovaný	k2eAgFnPc7d1	akumulovaná
usazeninami	usazenina	k1gFnPc7	usazenina
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
vlastním	vlastní	k2eAgInSc7d1	vlastní
štítem	štít	k1gInSc7	štít
a	a	k8xC	a
Perským	perský	k2eAgInSc7d1	perský
zálivem	záliv	k1gInSc7	záliv
je	být	k5eAaImIp3nS	být
mocný	mocný	k2eAgInSc4d1	mocný
platformní	platformní	k2eAgInSc4d1	platformní
pokryv	pokryv	k1gInSc4	pokryv
prvohor	prvohory	k1gFnPc2	prvohory
<g/>
,	,	kIx,	,
druhohor	druhohory	k1gFnPc2	druhohory
a	a	k8xC	a
třetihor	třetihory	k1gFnPc2	třetihory
<g/>
.	.	kIx.	.
</s>
<s>
Nacházejí	nacházet	k5eAaImIp3nP	nacházet
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
hojné	hojný	k2eAgFnPc1d1	hojná
formace	formace	k1gFnPc1	formace
s	s	k7c7	s
ložisky	ložisko	k1gNnPc7	ložisko
ropy	ropa	k1gFnSc2	ropa
z	z	k7c2	z
období	období	k1gNnSc2	období
svrchní	svrchní	k2eAgFnSc2d1	svrchní
křídy	křída	k1gFnSc2	křída
a	a	k8xC	a
eocénu	eocén	k1gInSc2	eocén
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kaledonidy	Kaledonid	k1gInPc4	Kaledonid
===	===	k?	===
</s>
</p>
<p>
<s>
Kaledonidy	Kaledonid	k1gInPc1	Kaledonid
jsou	být	k5eAaImIp3nP	být
horská	horský	k2eAgNnPc4d1	horské
pásma	pásmo	k1gNnPc4	pásmo
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnPc4	jejichž
vývoj	vývoj	k1gInSc1	vývoj
začal	začít	k5eAaPmAgInS	začít
během	během	k7c2	během
starších	starý	k2eAgFnPc2d2	starší
prvohor	prvohory	k1gFnPc2	prvohory
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Evropy	Evropa	k1gFnSc2	Evropa
probíhalo	probíhat	k5eAaImAgNnS	probíhat
kaledonské	kaledonský	k2eAgNnSc1d1	kaledonské
vrásnění	vrásnění	k1gNnSc1	vrásnění
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
zvedání	zvedání	k1gNnSc1	zvedání
podél	podél	k7c2	podél
starých	starý	k2eAgInPc2d1	starý
zlomů	zlom	k1gInPc2	zlom
mnohdy	mnohdy	k6eAd1	mnohdy
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
i	i	k9	i
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sibiřské	sibiřský	k2eAgInPc1d1	sibiřský
a	a	k8xC	a
středoasijské	středoasijský	k2eAgInPc1d1	středoasijský
kaledonidy	kaledonid	k1gInPc1	kaledonid
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
z	z	k7c2	z
tzv.	tzv.	kA	tzv.
uralsko-mongolské	uralskoongolský	k2eAgFnSc2d1	uralsko-mongolský
mobilní	mobilní	k2eAgFnSc2d1	mobilní
zóny	zóna	k1gFnSc2	zóna
mezi	mezi	k7c7	mezi
fenosarmatskou	fenosarmatský	k2eAgFnSc7d1	fenosarmatský
<g/>
,	,	kIx,	,
sibiřskou	sibiřský	k2eAgFnSc7d1	sibiřská
a	a	k8xC	a
čínskou	čínský	k2eAgFnSc7d1	čínská
platformou	platforma	k1gFnSc7	platforma
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
sem	sem	k6eAd1	sem
Altaj	Altaj	k1gInSc1	Altaj
<g/>
,	,	kIx,	,
Sajany	Sajan	k1gInPc1	Sajan
<g/>
,	,	kIx,	,
Ťan-šan	Ťan-šan	k1gInSc1	Ťan-šan
<g/>
,	,	kIx,	,
Kazašská	kazašský	k2eAgFnSc1d1	kazašská
pahorkatina	pahorkatina	k1gFnSc1	pahorkatina
<g/>
,	,	kIx,	,
Sibiřské	sibiřský	k2eAgFnPc1d1	sibiřská
starokaledonidy	starokaledonida	k1gFnPc1	starokaledonida
(	(	kIx(	(
<g/>
salairidy	salairid	k1gInPc1	salairid
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Čínské	čínský	k2eAgFnPc1d1	čínská
kaledonidy	kaledonida	k1gFnPc1	kaledonida
leží	ležet	k5eAaImIp3nP	ležet
zčásti	zčásti	k6eAd1	zčásti
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Číně	Čína	k1gFnSc6	Čína
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
k	k	k7c3	k
nim	on	k3xPp3gInPc3	on
patří	patřit	k5eAaImIp3nP	patřit
pohoří	pohoří	k1gNnSc4	pohoří
Nan-šan	Nan-šana	k1gFnPc2	Nan-šana
<g/>
.	.	kIx.	.
</s>
<s>
Najdeme	najít	k5eAaPmIp1nP	najít
zde	zde	k6eAd1	zde
5	[number]	k4	5
až	až	k9	až
6	[number]	k4	6
km	km	kA	km
mocný	mocný	k2eAgInSc4d1	mocný
sinijský	sinijský	k2eAgInSc4d1	sinijský
komplex	komplex	k1gInSc4	komplex
fylitů	fylit	k1gInPc2	fylit
<g/>
,	,	kIx,	,
kvarcitů	kvarcit	k1gInPc2	kvarcit
<g/>
,	,	kIx,	,
zelenokamenů	zelenokamen	k1gInPc2	zelenokamen
a	a	k8xC	a
mramorů	mramor	k1gInPc2	mramor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Hercynidy	hercynidy	k1gInPc4	hercynidy
===	===	k?	===
</s>
</p>
<p>
<s>
Hercynidy	hercynidy	k1gInPc1	hercynidy
jsou	být	k5eAaImIp3nP	být
horská	horský	k2eAgNnPc4d1	horské
pásma	pásmo	k1gNnPc4	pásmo
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnPc4	jejichž
vývoj	vývoj	k1gInSc1	vývoj
začal	začít	k5eAaPmAgInS	začít
během	během	k7c2	během
mladších	mladý	k2eAgFnPc2d2	mladší
prvohor	prvohory	k1gFnPc2	prvohory
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Evropy	Evropa	k1gFnSc2	Evropa
probíhalo	probíhat	k5eAaImAgNnS	probíhat
hercynské	hercynský	k2eAgNnSc1d1	hercynské
vrásnění	vrásnění	k1gNnSc1	vrásnění
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
zvedání	zvedání	k1gNnSc1	zvedání
podél	podél	k7c2	podél
starých	starý	k2eAgInPc2d1	starý
zlomů	zlom	k1gInPc2	zlom
mnohdy	mnohdy	k6eAd1	mnohdy
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
i	i	k9	i
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
Střední	střední	k2eAgFnSc6d1	střední
Asii	Asie	k1gFnSc6	Asie
jsou	být	k5eAaImIp3nP	být
hercynidy	hercynidy	k1gInPc4	hercynidy
silně	silně	k6eAd1	silně
promíchané	promíchaný	k2eAgInPc4d1	promíchaný
s	s	k7c7	s
kaledonidy	kaledonid	k1gInPc7	kaledonid
a	a	k8xC	a
nejde	jít	k5eNaImIp3nS	jít
o	o	k7c4	o
samostatný	samostatný	k2eAgInSc4d1	samostatný
horský	horský	k2eAgInSc4d1	horský
systém	systém	k1gInSc4	systém
<g/>
.	.	kIx.	.
</s>
<s>
Hercynského	hercynský	k2eAgNnSc2d1	hercynské
stáří	stáří	k1gNnSc2	stáří
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
západní	západní	k2eAgFnSc1d1	západní
část	část	k1gFnSc1	část
Ťan-šanu	Ťan-šan	k1gInSc2	Ťan-šan
(	(	kIx(	(
<g/>
Kyzylkumsko-ťanšanská	Kyzylkumsko-ťanšanský	k2eAgFnSc1d1	Kyzylkumsko-ťanšanský
větev	větev	k1gFnSc1	větev
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kazašská	kazašský	k2eAgFnSc1d1	kazašská
pahorkatina	pahorkatina	k1gFnSc1	pahorkatina
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jižní	jižní	k2eAgFnSc1d1	jižní
část	část	k1gFnSc1	část
Altaje	Altaj	k1gInSc2	Altaj
a	a	k8xC	a
Sajanů	Sajan	k1gInPc2	Sajan
(	(	kIx(	(
<g/>
Irtyšsko-zajsanská	Irtyšskoajsanský	k2eAgFnSc1d1	Irtyšsko-zajsanský
větev	větev	k1gFnSc1	větev
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mongolsko-ochotské	mongolskochotský	k2eAgInPc1d1	mongolsko-ochotský
hercynidy	hercynidy	k1gInPc1	hercynidy
se	se	k3xPyFc4	se
táhnou	táhnout	k5eAaImIp3nP	táhnout
z	z	k7c2	z
Mongolska	Mongolsko	k1gNnSc2	Mongolsko
až	až	k9	až
k	k	k7c3	k
Ochotskému	ochotský	k2eAgNnSc3d1	Ochotské
moři	moře	k1gNnSc3	moře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vnitročínské	Vnitročínský	k2eAgInPc1d1	Vnitročínský
hercynidy	hercynidy	k1gInPc1	hercynidy
se	se	k3xPyFc4	se
táhnou	táhnout	k5eAaImIp3nP	táhnout
mezi	mezi	k7c7	mezi
Pamírem	Pamír	k1gInSc7	Pamír
a	a	k8xC	a
východočínskými	východočínský	k2eAgFnPc7d1	východočínský
kaledonidami	kaledonida	k1gFnPc7	kaledonida
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
sem	sem	k6eAd1	sem
Kchun-lun	Kchunun	k1gNnSc1	Kchun-lun
(	(	kIx(	(
<g/>
vklíněn	vklíněn	k2eAgInSc1d1	vklíněn
mezi	mezi	k7c4	mezi
tarimský	tarimský	k2eAgInSc4d1	tarimský
a	a	k8xC	a
tibetský	tibetský	k2eAgInSc4d1	tibetský
masiv	masiv	k1gInSc4	masiv
<g/>
)	)	kIx)	)
a	a	k8xC	a
Čchin-ling	Čchining	k1gInSc1	Čchin-ling
(	(	kIx(	(
<g/>
vklíněn	vklínit	k5eAaPmNgInS	vklínit
mezi	mezi	k7c4	mezi
severočínský	severočínský	k2eAgInSc4d1	severočínský
a	a	k8xC	a
středočínský	středočínský	k2eAgInSc4d1	středočínský
blok	blok	k1gInSc4	blok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sibiřské	sibiřský	k2eAgInPc1d1	sibiřský
a	a	k8xC	a
středoasijské	středoasijský	k2eAgInPc1d1	středoasijský
hercynidy	hercynidy	k1gInPc1	hercynidy
a	a	k8xC	a
kaledonidy	kaledonid	k1gInPc1	kaledonid
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
velkých	velký	k2eAgFnPc6d1	velká
plochách	plocha	k1gFnPc6	plocha
pokryty	pokrýt	k5eAaPmNgInP	pokrýt
druhohorními	druhohorní	k2eAgFnPc7d1	druhohorní
a	a	k8xC	a
třetihorními	třetihorní	k2eAgFnPc7d1	třetihorní
nezvrásněnými	zvrásněný	k2eNgFnPc7d1	zvrásněný
vrstvami	vrstva	k1gFnPc7	vrstva
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
tak	tak	k6eAd1	tak
tvoří	tvořit	k5eAaImIp3nP	tvořit
mladé	mladý	k2eAgFnPc1d1	mladá
platformy	platforma	k1gFnPc1	platforma
<g/>
:	:	kIx,	:
Západosibiřská	západosibiřský	k2eAgFnSc1d1	Západosibiřská
nížina	nížina	k1gFnSc1	nížina
<g/>
,	,	kIx,	,
Turanská	Turanský	k2eAgFnSc1d1	Turanská
nížina	nížina	k1gFnSc1	nížina
<g/>
,	,	kIx,	,
Tádžická	tádžický	k2eAgFnSc1d1	tádžická
pánev	pánev	k1gFnSc1	pánev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Alpidy	Alpid	k1gInPc4	Alpid
===	===	k?	===
</s>
</p>
<p>
<s>
Alpidy	Alpid	k1gInPc1	Alpid
jsou	být	k5eAaImIp3nP	být
horská	horský	k2eAgNnPc4d1	horské
pásma	pásmo	k1gNnPc4	pásmo
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnPc4	jejichž
vývoj	vývoj	k1gInSc1	vývoj
začal	začít	k5eAaPmAgInS	začít
ve	v	k7c6	v
druhohorách	druhohory	k1gFnPc6	druhohory
nebo	nebo	k8xC	nebo
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
zvedat	zvedat	k5eAaImF	zvedat
během	během	k7c2	během
kimmerského	kimmerský	k2eAgNnSc2d1	kimmerský
vrásnění	vrásnění	k1gNnSc2	vrásnění
<g/>
,	,	kIx,	,
některé	některý	k3yIgNnSc1	některý
až	až	k9	až
během	během	k7c2	během
alpinského	alpinský	k2eAgNnSc2d1	alpinské
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tethydní	Tethydní	k2eAgFnPc1d1	Tethydní
alpidy	alpida	k1gFnPc1	alpida
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
z	z	k7c2	z
bývalého	bývalý	k2eAgInSc2d1	bývalý
oceánu	oceán	k1gInSc2	oceán
Tethys	Tethysa	k1gFnPc2	Tethysa
a	a	k8xC	a
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
asijskou	asijský	k2eAgFnSc4d1	asijská
část	část	k1gFnSc4	část
Alpsko-himálajského	alpskoimálajský	k2eAgInSc2d1	alpsko-himálajský
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nP	patřit
sem	sem	k6eAd1	sem
Pontidy	Pontid	k1gInPc4	Pontid
<g/>
,	,	kIx,	,
Anatolidy	Anatolid	k1gInPc4	Anatolid
a	a	k8xC	a
Tauridy	Taurid	k1gInPc4	Taurid
v	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
<g/>
,	,	kIx,	,
ostrov	ostrov	k1gInSc1	ostrov
Kypr	Kypr	k1gInSc1	Kypr
<g/>
,	,	kIx,	,
Krymsko-kavkazská	krymskoavkazský	k2eAgFnSc1d1	krymsko-kavkazský
oblast	oblast	k1gFnSc1	oblast
<g/>
,	,	kIx,	,
Malý	malý	k2eAgInSc1d1	malý
Kavkaz	Kavkaz	k1gInSc1	Kavkaz
a	a	k8xC	a
Arménská	arménský	k2eAgFnSc1d1	arménská
vysočina	vysočina	k1gFnSc1	vysočina
<g/>
,	,	kIx,	,
Zagros	Zagrosa	k1gFnPc2	Zagrosa
(	(	kIx(	(
<g/>
Iranidy	Iranida	k1gFnPc1	Iranida
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
centrální	centrální	k2eAgNnSc4d1	centrální
íránské	íránský	k2eAgNnSc4d1	íránské
pásmo	pásmo	k1gNnSc4	pásmo
s	s	k7c7	s
lutským	lutský	k2eAgInSc7d1	lutský
masivem	masiv	k1gInSc7	masiv
<g/>
,	,	kIx,	,
makránsko-balúčistánská	makránskoalúčistánský	k2eAgFnSc1d1	makránsko-balúčistánský
flyšová	flyšový	k2eAgFnSc1d1	flyšový
jednotka	jednotka	k1gFnSc1	jednotka
<g/>
,	,	kIx,	,
východoíránská	východoíránský	k2eAgNnPc1d1	východoíránský
pohoří	pohoří	k1gNnPc1	pohoří
<g/>
,	,	kIx,	,
ománská	ománský	k2eAgFnSc1d1	ománská
ofiolitová	ofiolitový	k2eAgFnSc1d1	ofiolitový
zóna	zóna	k1gFnSc1	zóna
<g/>
,	,	kIx,	,
orogenní	orogenní	k2eAgFnSc1d1	orogenní
zóna	zóna	k1gFnSc1	zóna
Alborzu	Alborz	k1gInSc2	Alborz
<g/>
,	,	kIx,	,
Kopet	Kopet	k1gInSc1	Kopet
Dag	dag	kA	dag
<g/>
,	,	kIx,	,
centrální	centrální	k2eAgInSc1d1	centrální
pákistánský	pákistánský	k2eAgInSc4d1	pákistánský
hřbet	hřbet	k1gInSc4	hřbet
(	(	kIx(	(
<g/>
kvétský	kvétský	k2eAgInSc4d1	kvétský
hřbet	hřbet	k1gInSc4	hřbet
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pamírsko-hindúkušsko-karákoramská	pamírskoindúkušskoarákoramský	k2eAgFnSc1d1	pamírsko-hindúkušsko-karákoramský
elevace	elevace	k1gFnSc1	elevace
<g/>
,	,	kIx,	,
Himálaj	Himálaj	k1gFnSc1	Himálaj
a	a	k8xC	a
Transhimálaj	Transhimálaj	k1gFnSc1	Transhimálaj
<g/>
,	,	kIx,	,
Indoganžská	Indoganžský	k2eAgFnSc1d1	Indoganžská
nížina	nížina	k1gFnSc1	nížina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Alpinského	Alpinský	k2eAgNnSc2d1	Alpinské
stáří	stáří	k1gNnSc2	stáří
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
pásma	pásmo	k1gNnPc1	pásmo
podél	podél	k7c2	podél
pobřeží	pobřeží	k1gNnSc2	pobřeží
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
cirkumpacifické	cirkumpacifický	k2eAgFnPc4d1	cirkumpacifický
alpidy	alpida	k1gFnPc4	alpida
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
ovšem	ovšem	k9	ovšem
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
kontaktem	kontakt	k1gInSc7	kontakt
jiných	jiný	k2eAgFnPc2d1	jiná
desek	deska	k1gFnPc2	deska
(	(	kIx(	(
<g/>
pacifické	pacifický	k2eAgNnSc1d1	pacifické
s	s	k7c7	s
eurasijskou	eurasijský	k2eAgFnSc7d1	eurasijská
<g/>
)	)	kIx)	)
během	během	k7c2	během
druhohorního	druhohorní	k2eAgNnSc2d1	druhohorní
jenšanského	jenšanský	k2eAgNnSc2d1	jenšanský
vrásnění	vrásnění	k1gNnSc2	vrásnění
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nP	patřit
sem	sem	k6eAd1	sem
starší	starý	k2eAgMnPc1d2	starší
pásma	pásmo	k1gNnSc2	pásmo
v	v	k7c6	v
Laosu	Laos	k1gInSc6	Laos
<g/>
,	,	kIx,	,
Čukotsko-katasijský	Čukotskoatasijský	k2eAgInSc1d1	Čukotsko-katasijský
vulkanický	vulkanický	k2eAgInSc1d1	vulkanický
pás	pás	k1gInSc1	pás
(	(	kIx(	(
<g/>
Kamčatka	Kamčatka	k1gFnSc1	Kamčatka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
třetihorách	třetihory	k1gFnPc6	třetihory
vznikla	vzniknout	k5eAaPmAgNnP	vzniknout
vulkanická	vulkanický	k2eAgNnPc1d1	vulkanické
pásma	pásmo	k1gNnPc1	pásmo
Kuril	Kurily	k1gFnPc2	Kurily
<g/>
,	,	kIx,	,
Japonska	Japonsko	k1gNnSc2	Japonsko
a	a	k8xC	a
Filipín	Filipíny	k1gFnPc2	Filipíny
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
další	další	k2eAgInPc1d1	další
ostrovní	ostrovní	k2eAgInPc1d1	ostrovní
oblouky	oblouk	k1gInPc1	oblouk
na	na	k7c6	na
západním	západní	k2eAgInSc6d1	západní
okraji	okraj	k1gInSc6	okraj
pacifické	pacifický	k2eAgFnSc2d1	Pacifická
desky	deska	k1gFnSc2	deska
(	(	kIx(	(
<g/>
Velké	velký	k2eAgInPc1d1	velký
a	a	k8xC	a
Malé	Malé	k2eAgInPc1d1	Malé
Sundy	sund	k1gInPc1	sund
<g/>
,	,	kIx,	,
Moluky	Moluky	k1gFnPc1	Moluky
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Zemětřesení	zemětřesení	k1gNnSc1	zemětřesení
a	a	k8xC	a
sopečná	sopečný	k2eAgFnSc1d1	sopečná
činnost	činnost	k1gFnSc1	činnost
===	===	k?	===
</s>
</p>
<p>
<s>
Mnoho	mnoho	k4c1	mnoho
výše	vysoce	k6eAd2	vysoce
uvedených	uvedený	k2eAgFnPc2d1	uvedená
oblastí	oblast	k1gFnPc2	oblast
je	být	k5eAaImIp3nS	být
seizmicky	seizmicky	k6eAd1	seizmicky
aktivních	aktivní	k2eAgFnPc2d1	aktivní
a	a	k8xC	a
zemětřesení	zemětřesení	k1gNnPc1	zemětřesení
zde	zde	k6eAd1	zde
nejsou	být	k5eNaImIp3nP	být
vzácným	vzácný	k2eAgInSc7d1	vzácný
jevem	jev	k1gInSc7	jev
<g/>
.	.	kIx.	.
</s>
<s>
Týká	týkat	k5eAaImIp3nS	týkat
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
ostrovních	ostrovní	k2eAgMnPc2d1	ostrovní
oblouků	oblouk	k1gInPc2	oblouk
na	na	k7c6	na
styku	styk	k1gInSc6	styk
litosférických	litosférický	k2eAgFnPc2d1	litosférická
desek	deska	k1gFnPc2	deska
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Japonsko	Japonsko	k1gNnSc1	Japonsko
<g/>
,	,	kIx,	,
Tchaj-wan	Tchajan	k1gInSc1	Tchaj-wan
<g/>
,	,	kIx,	,
Sumatra	Sumatra	k1gFnSc1	Sumatra
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
mnoha	mnoho	k4c2	mnoho
oblastí	oblast	k1gFnPc2	oblast
alpsko-himálajského	alpskoimálajský	k2eAgInSc2d1	alpsko-himálajský
systému	systém	k1gInSc2	systém
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Malá	malý	k2eAgFnSc1d1	malá
Asie	Asie	k1gFnSc1	Asie
<g/>
,	,	kIx,	,
Kavkaz	Kavkaz	k1gInSc1	Kavkaz
<g/>
,	,	kIx,	,
Írán	Írán	k1gInSc1	Írán
<g/>
,	,	kIx,	,
Kašmír	Kašmír	k1gInSc1	Kašmír
<g/>
)	)	kIx)	)
i	i	k8xC	i
jiných	jiný	k2eAgFnPc2d1	jiná
zlomových	zlomový	k2eAgFnPc2d1	zlomová
oblastí	oblast	k1gFnPc2	oblast
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Sečuánská	Sečuánský	k2eAgFnSc1d1	Sečuánská
pánev	pánev	k1gFnSc1	pánev
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oblastí	oblast	k1gFnSc7	oblast
vulkanického	vulkanický	k2eAgInSc2d1	vulkanický
původu	původ	k1gInSc2	původ
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
<g/>
,	,	kIx,	,
dodnes	dodnes	k6eAd1	dodnes
činné	činný	k2eAgFnPc1d1	činná
sopky	sopka	k1gFnPc1	sopka
najdeme	najít	k5eAaPmIp1nP	najít
opět	opět	k6eAd1	opět
především	především	k9	především
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Asii	Asie	k1gFnSc6	Asie
podél	podél	k7c2	podél
okraje	okraj	k1gInSc2	okraj
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
<g/>
:	:	kIx,	:
Kamčatka	Kamčatka	k1gFnSc1	Kamčatka
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc1	Japonsko
<g/>
,	,	kIx,	,
Filipíny	Filipíny	k1gFnPc1	Filipíny
<g/>
,	,	kIx,	,
Indonésie	Indonésie	k1gFnPc1	Indonésie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vodstvo	vodstvo	k1gNnSc1	vodstvo
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
mnohé	mnohý	k2eAgInPc1d1	mnohý
z	z	k7c2	z
nejdelších	dlouhý	k2eAgInPc2d3	nejdelší
a	a	k8xC	a
nejvodnějších	vodný	k2eAgInPc2d3	vodný
veletoků	veletok	k1gInPc2	veletok
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
Čchang-ťiang	Čchang-ťiang	k1gInSc1	Čchang-ťiang
(	(	kIx(	(
<g/>
Dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
řeka	řeka	k1gFnSc1	řeka
<g/>
,	,	kIx,	,
Evropanům	Evropan	k1gMnPc3	Evropan
známá	známý	k2eAgFnSc1d1	známá
též	též	k9	též
jako	jako	k8xS	jako
Jang-c	Jang	k1gFnSc1	Jang-c
<g/>
'	'	kIx"	'
<g/>
-ťiang	-ťiang	k1gInSc1	-ťiang
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Chuang-che	Chuanghe	k1gNnSc1	Chuang-che
(	(	kIx(	(
<g/>
Žlutá	žlutý	k2eAgFnSc1d1	žlutá
řeka	řeka	k1gFnSc1	řeka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ob	Ob	k1gInSc1	Ob
<g/>
,	,	kIx,	,
Jenisej	Jenisej	k1gInSc1	Jenisej
<g/>
,	,	kIx,	,
Lena	Lena	k1gFnSc1	Lena
<g/>
,	,	kIx,	,
Amur	Amur	k1gInSc1	Amur
<g/>
,	,	kIx,	,
Mekong	Mekong	k1gInSc1	Mekong
<g/>
,	,	kIx,	,
Brahmaputra	Brahmaputra	k1gFnSc1	Brahmaputra
<g/>
,	,	kIx,	,
Ganga	Ganga	k1gFnSc1	Ganga
<g/>
,	,	kIx,	,
Indus	Indus	k1gInSc1	Indus
<g/>
,	,	kIx,	,
Eufrat	Eufrat	k1gInSc1	Eufrat
a	a	k8xC	a
Tigris	Tigris	k1gInSc1	Tigris
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnPc1d1	velká
sibiřské	sibiřský	k2eAgFnPc1d1	sibiřská
řeky	řeka	k1gFnPc1	řeka
(	(	kIx(	(
<g/>
Ob	Ob	k1gInSc1	Ob
<g/>
,	,	kIx,	,
Jenisej	Jenisej	k1gInSc1	Jenisej
<g/>
,	,	kIx,	,
Lena	Lena	k1gFnSc1	Lena
<g/>
)	)	kIx)	)
odtékají	odtékat	k5eAaImIp3nP	odtékat
do	do	k7c2	do
Severního	severní	k2eAgInSc2d1	severní
ledového	ledový	k2eAgInSc2d1	ledový
oceánu	oceán	k1gInSc2	oceán
<g/>
,	,	kIx,	,
čínské	čínský	k2eAgInPc1d1	čínský
a	a	k8xC	a
indočínské	indočínský	k2eAgInPc1d1	indočínský
veletoky	veletok	k1gInPc1	veletok
(	(	kIx(	(
<g/>
Chuang-che	Chuanghe	k1gNnSc1	Chuang-che
<g/>
,	,	kIx,	,
Čchang-ťiang	Čchang-ťiang	k1gInSc1	Čchang-ťiang
a	a	k8xC	a
Mekong	Mekong	k1gInSc1	Mekong
<g/>
)	)	kIx)	)
míří	mířit	k5eAaImIp3nS	mířit
na	na	k7c4	na
východ	východ	k1gInSc4	východ
do	do	k7c2	do
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
<g/>
,	,	kIx,	,
další	další	k2eAgMnPc1d1	další
směřují	směřovat	k5eAaImIp3nP	směřovat
k	k	k7c3	k
jihu	jih	k1gInSc3	jih
do	do	k7c2	do
Indického	indický	k2eAgInSc2d1	indický
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnPc1d1	velká
oblasti	oblast	k1gFnPc1	oblast
v	v	k7c6	v
nitru	nitro	k1gNnSc6	nitro
Asie	Asie	k1gFnSc2	Asie
jsou	být	k5eAaImIp3nP	být
bezodtoké	bezodtoký	k2eAgFnPc1d1	bezodtoká
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
bez	bez	k7c2	bez
spojení	spojení	k1gNnSc2	spojení
se	s	k7c7	s
světovým	světový	k2eAgInSc7d1	světový
oceánem	oceán	k1gInSc7	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Řeky	řeka	k1gFnSc2	řeka
se	se	k3xPyFc4	se
buď	buď	k8xC	buď
ztrácejí	ztrácet	k5eAaImIp3nP	ztrácet
v	v	k7c6	v
pouštích	poušť	k1gFnPc6	poušť
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Gobi	Gobi	k1gNnSc1	Gobi
<g/>
,	,	kIx,	,
Taklamakan	Taklamakan	k1gInSc1	Taklamakan
<g/>
,	,	kIx,	,
Karakum	Karakum	k1gInSc1	Karakum
<g/>
,	,	kIx,	,
Kyzylkum	Kyzylkum	k1gInSc1	Kyzylkum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
končí	končit	k5eAaImIp3nS	končit
ve	v	k7c6	v
více	hodně	k6eAd2	hodně
méně	málo	k6eAd2	málo
slaných	slaný	k2eAgNnPc6d1	slané
jezerech	jezero	k1gNnPc6	jezero
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
se	se	k3xPyFc4	se
voda	voda	k1gFnSc1	voda
vypařuje	vypařovat	k5eAaImIp3nS	vypařovat
(	(	kIx(	(
<g/>
Balchaš	Balchaš	k1gInSc1	Balchaš
<g/>
,	,	kIx,	,
Aralské	aralský	k2eAgNnSc1d1	Aralské
jezero	jezero	k1gNnSc1	jezero
<g/>
,	,	kIx,	,
Kaspické	kaspický	k2eAgNnSc1d1	Kaspické
moře	moře	k1gNnSc1	moře
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
významných	významný	k2eAgFnPc2d1	významná
řek	řeka	k1gFnPc2	řeka
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc4	ten
týká	týkat	k5eAaImIp3nS	týkat
např.	např.	kA	např.
Amudarji	Amudarje	k1gFnSc4	Amudarje
<g/>
,	,	kIx,	,
Syrdarji	Syrdarja	k1gFnSc3	Syrdarja
a	a	k8xC	a
Tarimu	Tarim	k1gInSc3	Tarim
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
nejhlubší	hluboký	k2eAgNnSc1d3	nejhlubší
jezero	jezero	k1gNnSc1	jezero
světa	svět	k1gInSc2	svět
Bajkal	Bajkal	k1gInSc1	Bajkal
(	(	kIx(	(
<g/>
odkud	odkud	k6eAd1	odkud
voda	voda	k1gFnSc1	voda
odtéká	odtékat	k5eAaImIp3nS	odtékat
řekou	řeka	k1gFnSc7	řeka
Angarou	Angarý	k2eAgFnSc7d1	Angarý
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
sladkovodní	sladkovodní	k2eAgMnSc1d1	sladkovodní
a	a	k8xC	a
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
asi	asi	k9	asi
pětinu	pětina	k1gFnSc4	pětina
světových	světový	k2eAgFnPc2d1	světová
zásob	zásoba	k1gFnPc2	zásoba
sladké	sladký	k2eAgFnSc2d1	sladká
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Podnebí	podnebí	k1gNnSc2	podnebí
==	==	k?	==
</s>
</p>
<p>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
obrovské	obrovský	k2eAgFnSc3d1	obrovská
rozloze	rozloha	k1gFnSc3	rozloha
Asie	Asie	k1gFnSc2	Asie
zde	zde	k6eAd1	zde
najdeme	najít	k5eAaPmIp1nP	najít
všechny	všechen	k3xTgInPc4	všechen
hlavní	hlavní	k2eAgInPc4d1	hlavní
podnebné	podnebný	k2eAgInPc4d1	podnebný
pásy	pás	k1gInPc4	pás
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgNnPc1d1	velké
území	území	k1gNnPc1	území
v	v	k7c6	v
nitru	nitro	k1gNnSc6	nitro
kontinentu	kontinent	k1gInSc2	kontinent
mají	mít	k5eAaImIp3nP	mít
silně	silně	k6eAd1	silně
kontinentální	kontinentální	k2eAgNnPc1d1	kontinentální
podnebí	podnebí	k1gNnPc1	podnebí
s	s	k7c7	s
velkými	velký	k2eAgInPc7d1	velký
teplotními	teplotní	k2eAgInPc7d1	teplotní
rozdíly	rozdíl	k1gInPc7	rozdíl
mezi	mezi	k7c7	mezi
zimou	zima	k1gFnSc7	zima
a	a	k8xC	a
létem	léto	k1gNnSc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Tlak	tlak	k1gInSc1	tlak
vzduchu	vzduch	k1gInSc2	vzduch
přepočtený	přepočtený	k2eAgInSc1d1	přepočtený
na	na	k7c4	na
hladinu	hladina	k1gFnSc4	hladina
moře	moře	k1gNnSc2	moře
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
hodnot	hodnota	k1gFnPc2	hodnota
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Ulánbátaru	Ulánbátar	k1gInSc2	Ulánbátar
a	a	k8xC	a
Sibiře	Sibiř	k1gFnSc2	Sibiř
(	(	kIx(	(
<g/>
103	[number]	k4	103
kPa	kPa	k?	kPa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
tlaková	tlakový	k2eAgFnSc1d1	tlaková
výše	výše	k1gFnSc1	výše
(	(	kIx(	(
<g/>
anticyklóna	anticyklóna	k1gFnSc1	anticyklóna
<g/>
)	)	kIx)	)
nad	nad	k7c7	nad
střední	střední	k2eAgFnSc7d1	střední
Sibiří	Sibiř	k1gFnSc7	Sibiř
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
velká	velký	k2eAgFnSc1d1	velká
tlaková	tlakový	k2eAgFnSc1d1	tlaková
níže	níže	k1gFnSc1	níže
(	(	kIx(	(
<g/>
cyklóna	cyklóna	k1gFnSc1	cyklóna
<g/>
)	)	kIx)	)
vzniká	vznikat	k5eAaImIp3nS	vznikat
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
kolem	kolem	k7c2	kolem
Dillí	Dillí	k1gNnSc2	Dillí
(	(	kIx(	(
<g/>
99,5	[number]	k4	99,5
kPa	kPa	k?	kPa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejnižší	nízký	k2eAgFnPc1d3	nejnižší
lednové	lednový	k2eAgFnPc1d1	lednová
teploty	teplota	k1gFnPc1	teplota
na	na	k7c6	na
Sibiři	Sibiř	k1gFnSc6	Sibiř
mohou	moct	k5eAaImIp3nP	moct
klesnout	klesnout	k5eAaPmF	klesnout
i	i	k9	i
pod	pod	k7c4	pod
−	−	k?	−
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Asii	Asie	k1gFnSc6	Asie
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
kolem	kolem	k7c2	kolem
+25	+25	k4	+25
°	°	k?	°
<g/>
C.	C.	kA	C.
Rozpětí	rozpětí	k1gNnSc1	rozpětí
průměrných	průměrný	k2eAgFnPc2d1	průměrná
červencových	červencový	k2eAgFnPc2d1	červencová
teplot	teplota	k1gFnPc2	teplota
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
5	[number]	k4	5
do	do	k7c2	do
35	[number]	k4	35
stupňů	stupeň	k1gInPc2	stupeň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Množství	množství	k1gNnSc1	množství
srážek	srážka	k1gFnPc2	srážka
je	být	k5eAaImIp3nS	být
ovlivněno	ovlivnit	k5eAaPmNgNnS	ovlivnit
především	především	k9	především
vysokými	vysoký	k2eAgFnPc7d1	vysoká
asijskými	asijský	k2eAgNnPc7d1	asijské
horstvy	horstvo	k1gNnPc7	horstvo
<g/>
.	.	kIx.	.
</s>
<s>
Jih	jih	k1gInSc1	jih
a	a	k8xC	a
jihovýchod	jihovýchod	k1gInSc1	jihovýchod
kontinentu	kontinent	k1gInSc2	kontinent
je	být	k5eAaImIp3nS	být
extrémně	extrémně	k6eAd1	extrémně
vlhký	vlhký	k2eAgInSc1d1	vlhký
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
ve	v	k7c6	v
srážkovém	srážkový	k2eAgInSc6d1	srážkový
stínu	stín	k1gInSc6	stín
za	za	k7c7	za
Himálajem	Himálaj	k1gInSc7	Himálaj
leží	ležet	k5eAaImIp3nS	ležet
velmi	velmi	k6eAd1	velmi
suché	suchý	k2eAgFnSc2d1	suchá
pouštní	pouštní	k2eAgFnSc2d1	pouštní
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Roční	roční	k2eAgNnSc1d1	roční
období	období	k1gNnSc1	období
v	v	k7c6	v
Himálaji	Himálaj	k1gFnSc6	Himálaj
a	a	k8xC	a
na	na	k7c4	na
jih	jih	k1gInSc4	jih
od	od	k7c2	od
něj	on	k3xPp3gMnSc2	on
jsou	být	k5eAaImIp3nP	být
ovlivňována	ovlivňovat	k5eAaImNgFnS	ovlivňovat
pravidelnými	pravidelný	k2eAgInPc7d1	pravidelný
monzuny	monzun	k1gInPc7	monzun
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
tyto	tento	k3xDgFnPc4	tento
klimatické	klimatický	k2eAgFnPc4d1	klimatická
oblasti	oblast	k1gFnPc4	oblast
podle	podle	k7c2	podle
Köppena	Köppen	k2eAgNnPc4d1	Köppen
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Tundrové	tundrový	k2eAgNnSc1d1	tundrové
podnebí	podnebí	k1gNnSc1	podnebí
</s>
</p>
<p>
<s>
Boreální	boreální	k2eAgNnSc1d1	boreální
podnebí	podnebí	k1gNnSc1	podnebí
</s>
</p>
<p>
<s>
Stepní	stepní	k2eAgNnSc1d1	stepní
podnebí	podnebí	k1gNnSc1	podnebí
</s>
</p>
<p>
<s>
Mírně	mírně	k6eAd1	mírně
teplé	teplý	k2eAgNnSc1d1	teplé
monzunové	monzunový	k2eAgNnSc1d1	monzunové
podnebí	podnebí	k1gNnSc1	podnebí
</s>
</p>
<p>
<s>
Monzunové	monzunový	k2eAgNnSc1d1	monzunové
podnebí	podnebí	k1gNnSc1	podnebí
</s>
</p>
<p>
<s>
Podnebí	podnebí	k1gNnSc1	podnebí
tropického	tropický	k2eAgInSc2d1	tropický
deštného	deštný	k2eAgInSc2d1	deštný
pralesa	prales	k1gInSc2	prales
</s>
</p>
<p>
<s>
Studené	Studené	k2eAgFnPc1d1	Studené
pouště	poušť	k1gFnPc1	poušť
</s>
</p>
<p>
<s>
Horké	Horké	k2eAgFnPc1d1	Horké
pouště	poušť	k1gFnPc1	poušť
</s>
</p>
<p>
<s>
Středomořské	středomořský	k2eAgNnSc1d1	středomořské
podnebí	podnebí	k1gNnSc1	podnebí
</s>
</p>
<p>
<s>
==	==	k?	==
Vegetace	vegetace	k1gFnSc2	vegetace
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Asii	Asie	k1gFnSc6	Asie
(	(	kIx(	(
<g/>
na	na	k7c6	na
Sibiři	Sibiř	k1gFnSc6	Sibiř
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
hlavními	hlavní	k2eAgInPc7d1	hlavní
biotopy	biotop	k1gInPc7	biotop
tundra	tundra	k1gFnSc1	tundra
<g/>
,	,	kIx,	,
lesotundra	lesotundra	k1gFnSc1	lesotundra
a	a	k8xC	a
tajga	tajga	k1gFnSc1	tajga
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mírném	mírný	k2eAgInSc6d1	mírný
podnebném	podnebný	k2eAgInSc6d1	podnebný
pásu	pás	k1gInSc6	pás
najdeme	najít	k5eAaPmIp1nP	najít
listnaté	listnatý	k2eAgInPc4d1	listnatý
opadavé	opadavý	k2eAgInPc4d1	opadavý
lesy	les	k1gInPc4	les
mírného	mírný	k2eAgInSc2d1	mírný
pásu	pás	k1gInSc2	pás
<g/>
,	,	kIx,	,
lesostep	lesostep	k1gFnSc4	lesostep
a	a	k8xC	a
step	step	k1gFnSc4	step
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
jih	jih	k1gInSc4	jih
pak	pak	k6eAd1	pak
polopouště	polopoušť	k1gFnSc2	polopoušť
a	a	k8xC	a
pouště	poušť	k1gFnSc2	poušť
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
a	a	k8xC	a
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Asii	Asie	k1gFnSc6	Asie
pak	pak	k6eAd1	pak
původní	původní	k2eAgNnPc4d1	původní
společenstva	společenstvo	k1gNnPc4	společenstvo
spadají	spadat	k5eAaPmIp3nP	spadat
pod	pod	k7c4	pod
monzunové	monzunový	k2eAgInPc4d1	monzunový
opadavé	opadavý	k2eAgInPc4d1	opadavý
lesy	les	k1gInPc4	les
a	a	k8xC	a
tropické	tropický	k2eAgInPc4d1	tropický
deštné	deštný	k2eAgInPc4d1	deštný
lesy	les	k1gInPc4	les
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velká	velký	k2eAgNnPc1d1	velké
území	území	k1gNnPc1	území
na	na	k7c6	na
Sibiři	Sibiř	k1gFnSc6	Sibiř
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
trvale	trvale	k6eAd1	trvale
zmrzlá	zmrzlý	k2eAgFnSc1d1	zmrzlá
půda	půda	k1gFnSc1	půda
(	(	kIx(	(
<g/>
permafrost	permafrost	k1gFnSc1	permafrost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
na	na	k7c4	na
jih	jih	k1gInSc4	jih
ji	on	k3xPp3gFnSc4	on
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaPmF	nalézt
v	v	k7c6	v
Tibetu	Tibet	k1gInSc6	Tibet
kvůli	kvůli	k7c3	kvůli
vysoké	vysoký	k2eAgFnSc3d1	vysoká
nadmořské	nadmořský	k2eAgFnSc3d1	nadmořská
výšce	výška	k1gFnSc3	výška
<g/>
.	.	kIx.	.
</s>
<s>
Ledovce	ledovec	k1gInPc1	ledovec
se	se	k3xPyFc4	se
zachovaly	zachovat	k5eAaPmAgInP	zachovat
v	v	k7c6	v
nejvyšších	vysoký	k2eAgNnPc6d3	nejvyšší
pohořích	pohoří	k1gNnPc6	pohoří
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Himálaj	Himálaj	k1gFnSc1	Himálaj
<g/>
,	,	kIx,	,
Pamír	Pamír	k1gInSc1	Pamír
<g/>
,	,	kIx,	,
Ťan-šan	Ťan-šan	k1gInSc1	Ťan-šan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kvůli	kvůli	k7c3	kvůli
globálnímu	globální	k2eAgNnSc3d1	globální
oteplování	oteplování	k1gNnSc3	oteplování
se	se	k3xPyFc4	se
zkracují	zkracovat	k5eAaImIp3nP	zkracovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1900	[number]	k4	1900
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
947	[number]	k4	947
milionu	milion	k4xCgInSc2	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
Asie	Asie	k1gFnSc2	Asie
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
čtyřnásobně	čtyřnásobně	k6eAd1	čtyřnásobně
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
4	[number]	k4	4
175	[number]	k4	175
038	[number]	k4	038
363	[number]	k4	363
a	a	k8xC	a
stále	stále	k6eAd1	stále
stoupá	stoupat	k5eAaImIp3nS	stoupat
<g/>
.	.	kIx.	.
</s>
<s>
Polovinu	polovina	k1gFnSc4	polovina
tvoří	tvořit	k5eAaImIp3nP	tvořit
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
do	do	k7c2	do
20	[number]	k4	20
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
žijí	žít	k5eAaImIp3nP	žít
přibližně	přibližně	k6eAd1	přibližně
dvě	dva	k4xCgFnPc1	dva
třetiny	třetina	k1gFnPc1	třetina
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
obyvatel	obyvatel	k1gMnPc2	obyvatel
má	mít	k5eAaImIp3nS	mít
Čína	Čína	k1gFnSc1	Čína
s	s	k7c7	s
1,3	[number]	k4	1,3
mld.	mld.	k?	mld.
</s>
</p>
<p>
<s>
==	==	k?	==
Politická	politický	k2eAgFnSc1d1	politická
geografie	geografie	k1gFnSc1	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Regiony	region	k1gInPc1	region
a	a	k8xC	a
státy	stát	k1gInPc1	stát
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Asie	Asie	k1gFnSc2	Asie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Asie	Asie	k1gFnSc2	Asie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Asie	Asie	k1gFnSc2	Asie
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Kategorie	kategorie	k1gFnPc1	kategorie
Asie	Asie	k1gFnSc2	Asie
ve	v	k7c6	v
Wikizprávách	Wikizpráva	k1gFnPc6	Wikizpráva
</s>
</p>
<p>
<s>
Průvodce	průvodce	k1gMnSc1	průvodce
Asie	Asie	k1gFnSc2	Asie
ve	v	k7c6	v
Wikicestách	Wikicesta	k1gFnPc6	Wikicesta
</s>
</p>
<p>
<s>
Fyzická	fyzický	k2eAgFnSc1d1	fyzická
geografie	geografie	k1gFnSc1	geografie
Asie	Asie	k1gFnSc2	Asie
</s>
</p>
<p>
<s>
Fyzická	fyzický	k2eAgFnSc1d1	fyzická
geografie	geografie	k1gFnSc1	geografie
Asie	Asie	k1gFnSc2	Asie
(	(	kIx(	(
<g/>
středoškolské	středoškolský	k2eAgInPc1d1	středoškolský
učební	učební	k2eAgInPc1d1	učební
materiály	materiál	k1gInPc1	materiál
<g/>
)	)	kIx)	)
</s>
</p>
