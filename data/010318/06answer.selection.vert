<s>
Kvůli	kvůli	k7c3	kvůli
Coriolisově	Coriolisův	k2eAgFnSc3d1	Coriolisova
síle	síla	k1gFnSc3	síla
se	se	k3xPyFc4	se
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
polokouli	polokoule	k1gFnSc6	polokoule
hurikány	hurikán	k1gInPc1	hurikán
a	a	k8xC	a
tlakové	tlakový	k2eAgFnPc1d1	tlaková
níže	níže	k1gFnPc1	níže
točí	točit	k5eAaImIp3nP	točit
doleva	doleva	k6eAd1	doleva
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
proti	proti	k7c3	proti
směru	směr	k1gInSc3	směr
hodinových	hodinový	k2eAgFnPc2d1	hodinová
ručiček	ručička	k1gFnPc2	ručička
<g/>
.	.	kIx.	.
</s>
