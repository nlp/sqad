<p>
<s>
Severní	severní	k2eAgFnSc1d1	severní
polokoule	polokoule	k1gFnSc1	polokoule
nebo	nebo	k8xC	nebo
severní	severní	k2eAgFnSc1d1	severní
hemisféra	hemisféra	k1gFnSc1	hemisféra
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
část	část	k1gFnSc1	část
povrchu	povrch	k1gInSc2	povrch
planety	planeta	k1gFnSc2	planeta
Země	zem	k1gFnSc2	zem
na	na	k7c4	na
sever	sever	k1gInSc4	sever
od	od	k7c2	od
rovníku	rovník	k1gInSc2	rovník
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
°	°	k?	°
s	s	k7c7	s
<g/>
/	/	kIx~	/
<g/>
j.	j.	k?	j.
z.	z.	k?	z.
š.	š.	k?	š.
<g/>
)	)	kIx)	)
až	až	k9	až
po	po	k7c4	po
severní	severní	k2eAgInSc4d1	severní
pól	pól	k1gInSc4	pól
(	(	kIx(	(
<g/>
+	+	kIx~	+
<g/>
90	[number]	k4	90
<g/>
°	°	k?	°
s.	s.	k?	s.
z.	z.	k?	z.
š.	š.	k?	š.
<g/>
)	)	kIx)	)
Středem	středem	k7c2	středem
povrchu	povrch	k1gInSc2	povrch
severní	severní	k2eAgFnSc2d1	severní
polokoule	polokoule	k1gFnSc2	polokoule
je	být	k5eAaImIp3nS	být
severní	severní	k2eAgInSc4d1	severní
pól	pól	k1gInSc4	pól
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
polokouli	polokoule	k1gFnSc6	polokoule
leží	ležet	k5eAaImIp3nS	ležet
celá	celý	k2eAgFnSc1d1	celá
Evropa	Evropa	k1gFnSc1	Evropa
<g/>
,	,	kIx,	,
celá	celý	k2eAgFnSc1d1	celá
Severní	severní	k2eAgFnSc1d1	severní
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
,	,	kIx,	,
Asie	Asie	k1gFnSc1	Asie
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
části	část	k1gFnSc2	část
Indonésie	Indonésie	k1gFnSc2	Indonésie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zhruba	zhruba	k6eAd1	zhruba
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc1d1	jižní
Amerika	Amerika	k1gFnSc1	Amerika
severně	severně	k6eAd1	severně
od	od	k7c2	od
Amazonky	Amazonka	k1gFnSc2	Amazonka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Meteorologie	meteorologie	k1gFnSc2	meteorologie
==	==	k?	==
</s>
</p>
<p>
<s>
Astronomické	astronomický	k2eAgNnSc1d1	astronomické
léto	léto	k1gNnSc1	léto
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
polokouli	polokoule	k1gFnSc6	polokoule
od	od	k7c2	od
letního	letní	k2eAgInSc2d1	letní
slunovratu	slunovrat	k1gInSc2	slunovrat
21	[number]	k4	21
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
do	do	k7c2	do
podzimní	podzimní	k2eAgFnSc2d1	podzimní
rovnodennosti	rovnodennost	k1gFnSc2	rovnodennost
22	[number]	k4	22
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
a	a	k8xC	a
astronomická	astronomický	k2eAgFnSc1d1	astronomická
zima	zima	k1gFnSc1	zima
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
od	od	k7c2	od
zimního	zimní	k2eAgInSc2d1	zimní
slunovratu	slunovrat	k1gInSc2	slunovrat
22	[number]	k4	22
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
do	do	k7c2	do
jarní	jarní	k2eAgFnSc2d1	jarní
rovnodennosti	rovnodennost	k1gFnSc2	rovnodennost
22	[number]	k4	22
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
noci	noc	k1gFnSc6	noc
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
vidět	vidět	k5eAaImF	vidět
převážně	převážně	k6eAd1	převážně
severní	severní	k2eAgFnSc1d1	severní
hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
obloha	obloha	k1gFnSc1	obloha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kvůli	kvůli	k7c3	kvůli
Coriolisově	Coriolisův	k2eAgFnSc3d1	Coriolisova
síle	síla	k1gFnSc3	síla
se	se	k3xPyFc4	se
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
polokouli	polokoule	k1gFnSc6	polokoule
hurikány	hurikán	k1gInPc1	hurikán
a	a	k8xC	a
tlakové	tlakový	k2eAgFnPc1d1	tlaková
níže	níže	k1gFnPc1	níže
točí	točit	k5eAaImIp3nP	točit
doleva	doleva	k6eAd1	doleva
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
proti	proti	k7c3	proti
směru	směr	k1gInSc3	směr
hodinových	hodinový	k2eAgFnPc2d1	hodinová
ručiček	ručička	k1gFnPc2	ručička
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
vodních	vodní	k2eAgInPc2d1	vodní
vírů	vír	k1gInPc2	vír
je	být	k5eAaImIp3nS	být
působení	působení	k1gNnSc1	působení
Coriolisovy	Coriolisův	k2eAgFnSc2d1	Coriolisova
síly	síla	k1gFnSc2	síla
překryto	překrýt	k5eAaPmNgNnS	překrýt
jinými	jiný	k2eAgFnPc7d1	jiná
silami	síla	k1gFnPc7	síla
a	a	k8xC	a
směr	směr	k1gInSc1	směr
otáčení	otáčení	k1gNnSc2	otáčení
tak	tak	k6eAd1	tak
většinou	většinou	k6eAd1	většinou
neurčuje	určovat	k5eNaImIp3nS	určovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Polární	polární	k2eAgFnSc2d1	polární
záře	zář	k1gFnSc2	zář
vyskytující	vyskytující	k2eAgFnSc2d1	vyskytující
se	se	k3xPyFc4	se
nad	nad	k7c7	nad
polárními	polární	k2eAgFnPc7d1	polární
oblastmi	oblast	k1gFnPc7	oblast
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
polokoulí	polokoule	k1gFnSc7	polokoule
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Aurora	Aurora	k1gFnSc1	Aurora
borealis	borealis	k1gFnSc1	borealis
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
záře	zář	k1gFnSc2	zář
vyskytující	vyskytující	k2eAgFnSc2d1	vyskytující
se	se	k3xPyFc4	se
nad	nad	k7c7	nad
polokoulí	polokoule	k1gFnSc7	polokoule
jižní	jižní	k2eAgFnSc7d1	jižní
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Aurora	Aurora	k1gFnSc1	Aurora
australis	australis	k1gFnSc1	australis
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pevniny	pevnina	k1gFnPc4	pevnina
a	a	k8xC	a
oceány	oceán	k1gInPc4	oceán
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
celé	celý	k2eAgFnSc2d1	celá
planety	planeta	k1gFnSc2	planeta
(	(	kIx(	(
<g/>
510	[number]	k4	510
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
km2	km2	k4	km2
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zřetelně	zřetelně	k6eAd1	zřetelně
více	hodně	k6eAd2	hodně
vodních	vodní	k2eAgFnPc2d1	vodní
ploch	plocha	k1gFnPc2	plocha
než	než	k8xS	než
pevniny	pevnina	k1gFnSc2	pevnina
–	–	k?	–
celosvětově	celosvětově	k6eAd1	celosvětově
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
pevnina	pevnina	k1gFnSc1	pevnina
29	[number]	k4	29
procent	procento	k1gNnPc2	procento
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
150	[number]	k4	150
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Podíl	podíl	k1gInSc1	podíl
pevniny	pevnina	k1gFnSc2	pevnina
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
polokouli	polokoule	k1gFnSc6	polokoule
asi	asi	k9	asi
dvakrát	dvakrát	k6eAd1	dvakrát
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
polokouli	polokoule	k1gFnSc6	polokoule
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
polokouli	polokoule	k1gFnSc6	polokoule
připadá	připadat	k5eAaImIp3nS	připadat
zhruba	zhruba	k6eAd1	zhruba
67,3	[number]	k4	67,3
procent	procento	k1gNnPc2	procento
celosvětové	celosvětový	k2eAgFnSc2d1	celosvětová
pevniny	pevnina	k1gFnSc2	pevnina
a	a	k8xC	a
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
32,7	[number]	k4	32,7
procent	procento	k1gNnPc2	procento
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
40	[number]	k4	40
procent	procento	k1gNnPc2	procento
severní	severní	k2eAgFnSc2d1	severní
polokoule	polokoule	k1gFnSc2	polokoule
tak	tak	k6eAd1	tak
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
pevnina	pevnina	k1gFnSc1	pevnina
a	a	k8xC	a
60	[number]	k4	60
procent	procento	k1gNnPc2	procento
oceány	oceán	k1gInPc7	oceán
<g/>
,	,	kIx,	,
na	na	k7c4	na
jižní	jižní	k2eAgNnSc4d1	jižní
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
20	[number]	k4	20
procent	procento	k1gNnPc2	procento
pevniny	pevnina	k1gFnSc2	pevnina
a	a	k8xC	a
80	[number]	k4	80
procent	procento	k1gNnPc2	procento
oceánů	oceán	k1gInPc2	oceán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Jižní	jižní	k2eAgFnSc1d1	jižní
polokoule	polokoule	k1gFnSc1	polokoule
</s>
</p>
