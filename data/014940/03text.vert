<s>
Štěpán	Štěpán	k1gMnSc1
Kozub	kozub	k1gInSc4
</s>
<s>
Štěpán	Štěpán	k1gMnSc1
Kozub	kozub	k1gInSc4
Narození	narození	k1gNnSc2
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1996	#num#	k4
(	(	kIx(
<g/>
25	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
BohumínČesko	BohumínČesko	k1gNnSc1
Česko	Česko	k1gNnSc1
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
</s>
<s>
Janáčkova	Janáčkův	k2eAgFnSc1d1
konzervatoř	konzervatoř	k1gFnSc1
Manžel	manžel	k1gMnSc1
<g/>
(	(	kIx(
<g/>
ka	ka	k?
<g/>
)	)	kIx)
</s>
<s>
Barbora	Barbora	k1gFnSc1
Kozubová	Kozubová	k1gFnSc1
Děti	dítě	k1gFnPc1
</s>
<s>
Anežka	Anežka	k1gFnSc1
Kozubová	Kozubový	k2eAgFnSc1d1
Významné	významný	k2eAgFnPc4d1
role	role	k1gFnPc4
</s>
<s>
Lupták	Lupták	k1gInSc1
(	(	kIx(
<g/>
Lajna	lajna	k1gFnSc1
<g/>
,	,	kIx,
Luptákův	Luptákův	k2eAgInSc1d1
vlogísek	vlogísek	k1gInSc1
<g/>
)	)	kIx)
<g/>
Matěj	Matěj	k1gMnSc1
Sehnal	Sehnal	k1gMnSc1
(	(	kIx(
<g/>
Místo	místo	k7c2
zločinu	zločin	k1gInSc2
Ostrava	Ostrava	k1gFnSc1
<g/>
)	)	kIx)
<g/>
Štěpán	Štěpán	k1gMnSc1
Kozub	kozub	k1gInSc1
(	(	kIx(
<g/>
sKORO	skoro	k6eAd1
NA	na	k7c6
mizině	mizina	k1gFnSc6
<g/>
)	)	kIx)
<g/>
Lumír	Lumír	k1gMnSc1
<g/>
,	,	kIx,
David	David	k1gMnSc1
Votrubek	Votrubek	k1gInSc1
<g/>
,	,	kIx,
Jugoslav	Jugoslav	k1gMnSc1
Vavara	Vavara	k1gFnSc1
<g/>
,	,	kIx,
Pražák	Pražák	k1gMnSc1
Voliver	Voliver	k1gMnSc1
(	(	kIx(
<g/>
Tři	tři	k4xCgMnPc1
tygři	tygr	k1gMnPc1
<g/>
)	)	kIx)
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Štěpán	Štěpán	k1gMnSc1
Kozub	kozub	k1gInSc1
(	(	kIx(
<g/>
*	*	kIx~
13	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1996	#num#	k4
Bohumín	Bohumín	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
český	český	k2eAgMnSc1d1
herec	herec	k1gMnSc1
<g/>
,	,	kIx,
komik	komik	k1gMnSc1
<g/>
,	,	kIx,
zpěvák	zpěvák	k1gMnSc1
a	a	k8xC
režisér	režisér	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
2018	#num#	k4
působí	působit	k5eAaImIp3nS
jako	jako	k9
herec	herec	k1gMnSc1
a	a	k8xC
umělecký	umělecký	k2eAgMnSc1d1
šéf	šéf	k1gMnSc1
v	v	k7c6
Divadle	divadlo	k1gNnSc6
Mír	mír	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7
komediální	komediální	k2eAgFnSc1d1
improvizační	improvizační	k2eAgFnSc1d1
show	show	k1gFnSc1
Tři	tři	k4xCgMnPc4
tygři	tygr	k1gMnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Životopis	životopis	k1gInSc1
</s>
<s>
Štěpán	Štěpán	k1gMnSc1
Kozub	kozub	k1gInSc1
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgInS
13	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1996	#num#	k4
v	v	k7c6
Bohumíně	Bohumín	k1gInSc6
<g/>
,	,	kIx,
později	pozdě	k6eAd2
se	se	k3xPyFc4
přestěhoval	přestěhovat	k5eAaPmAgInS
s	s	k7c7
rodiči	rodič	k1gMnPc7
do	do	k7c2
městečka	městečko	k1gNnSc2
Javorník	Javorník	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
chodil	chodit	k5eAaImAgMnS
na	na	k7c4
základní	základní	k2eAgFnSc4d1
školu	škola	k1gFnSc4
a	a	k8xC
poté	poté	k6eAd1
nastoupil	nastoupit	k5eAaPmAgMnS
na	na	k7c4
Janáčkovu	Janáčkův	k2eAgFnSc4d1
konzervatoř	konzervatoř	k1gFnSc4
v	v	k7c6
Ostravě	Ostrava	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
studoval	studovat	k5eAaImAgMnS
herectví	herectví	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
tohoto	tento	k3xDgNnSc2
studia	studio	k1gNnSc2
již	již	k6eAd1
hostoval	hostovat	k5eAaImAgInS
v	v	k7c6
Národním	národní	k2eAgNnSc6d1
divadle	divadlo	k1gNnSc6
moravskoslezském	moravskoslezský	k2eAgNnSc6d1
a	a	k8xC
v	v	k7c6
Komorní	komorní	k2eAgFnSc6d1
scéně	scéna	k1gFnSc6
Aréna	aréna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
začal	začít	k5eAaPmAgInS
působit	působit	k5eAaImF
v	v	k7c6
Divadle	divadlo	k1gNnSc6
Mír	Míra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
nominován	nominovat	k5eAaBmNgMnS
na	na	k7c4
Cenu	cena	k1gFnSc4
Thálie	Thálie	k1gFnSc2
a	a	k8xC
získal	získat	k5eAaPmAgMnS
Cenu	cena	k1gFnSc4
divadelní	divadelní	k2eAgFnSc2d1
kritiky	kritika	k1gFnSc2
Talent	talent	k1gInSc1
roku	rok	k1gInSc2
2015	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
byl	být	k5eAaImAgInS
za	za	k7c4
film	film	k1gInSc4
Ruchoth	Ruchoth	k1gMnSc1
Raoth	Raoth	k1gMnSc1
na	na	k7c6
festivalu	festival	k1gInSc6
nezávislých	závislý	k2eNgInPc2d1
hororových	hororový	k2eAgInPc2d1
filmů	film	k1gInPc2
v	v	k7c6
Las	laso	k1gNnPc2
Vegas	Vegasa	k1gFnPc2
oceněn	oceněn	k2eAgMnSc1d1
jako	jako	k8xS,k8xC
nejlepší	dobrý	k2eAgMnSc1d3
herec	herec	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Žije	žít	k5eAaImIp3nS
v	v	k7c6
Ostravě	Ostrava	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dne	den	k1gInSc2
31	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2019	#num#	k4
se	se	k3xPyFc4
oženil	oženit	k5eAaPmAgMnS
s	s	k7c7
Barborou	Barbora	k1gFnSc7
Drulákovou	Drulákův	k2eAgFnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
20	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2020	#num#	k4
se	se	k3xPyFc4
jim	on	k3xPp3gMnPc3
narodila	narodit	k5eAaPmAgFnS
dcera	dcera	k1gFnSc1
Anežka	Anežka	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2020	#num#	k4
vydal	vydat	k5eAaPmAgInS
spolu	spolu	k6eAd1
s	s	k7c7
hudebníkem	hudebník	k1gMnSc7
Jiřím	Jiří	k1gMnSc7
Krhutem	Krhut	k1gMnSc7
album	album	k1gNnSc4
Prásknu	prásknout	k5eAaPmIp1nS
bičem	bič	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Ve	v	k7c6
stejném	stejný	k2eAgInSc6d1
roce	rok	k1gInSc6
si	se	k3xPyFc3
zahrál	zahrát	k5eAaPmAgMnS
jednu	jeden	k4xCgFnSc4
z	z	k7c2
hlavních	hlavní	k2eAgFnPc2d1
rolí	role	k1gFnPc2
v	v	k7c6
seriálu	seriál	k1gInSc6
sKORO	skoro	k6eAd1
NA	na	k7c6
mizině	mizina	k1gFnSc6
<g/>
,	,	kIx,
pojednávajícím	pojednávající	k2eAgNnSc7d1
o	o	k7c4
dění	dění	k1gNnSc4
v	v	k7c6
Divadle	divadlo	k1gNnSc6
Mír	Míra	k1gFnPc2
za	za	k7c4
koronavirových	koronavirových	k2eAgNnSc4d1
opatření	opatření	k1gNnSc4
<g/>
,	,	kIx,
herci	herec	k1gMnPc1
ztvárnili	ztvárnit	k5eAaPmAgMnP
fiktivní	fiktivní	k2eAgFnPc4d1
verze	verze	k1gFnPc4
sebe	sebe	k3xPyFc4
sama	sám	k3xTgMnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Kromě	kromě	k7c2
toho	ten	k3xDgNnSc2
též	též	k9
ztvárnil	ztvárnit	k5eAaPmAgMnS
jednu	jeden	k4xCgFnSc4
z	z	k7c2
hlavních	hlavní	k2eAgFnPc2d1
rolí	role	k1gFnPc2
v	v	k7c6
seriálu	seriál	k1gInSc6
Místo	místo	k7c2
zločinu	zločin	k1gInSc2
Ostrava	Ostrava	k1gFnSc1
a	a	k8xC
objevil	objevit	k5eAaPmAgMnS
se	se	k3xPyFc4
v	v	k7c6
malé	malý	k2eAgFnSc6d1
roli	role	k1gFnSc6
v	v	k7c6
minisérii	minisérie	k1gFnSc6
Stockholmský	stockholmský	k2eAgInSc4d1
syndrom	syndrom	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2021	#num#	k4
napsal	napsat	k5eAaBmAgMnS,k5eAaPmAgMnS
a	a	k8xC
režíroval	režírovat	k5eAaImAgMnS
svůj	svůj	k3xOyFgInSc4
první	první	k4xOgInSc4
film	film	k1gInSc4
s	s	k7c7
názvem	název	k1gInSc7
Volným	volný	k2eAgInSc7d1
pádem	pád	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Filmografie	filmografie	k1gFnSc1
</s>
<s>
Film	film	k1gInSc1
</s>
<s>
RokNázevRole	RokNázevRole	k1gFnSc1
</s>
<s>
20148	#num#	k4
mm	mm	kA
terapieAdam	terapieAdam	k6eAd1
</s>
<s>
2016	#num#	k4
<g/>
PirkoMarek	PirkoMarka	k1gFnPc2
</s>
<s>
2017	#num#	k4
<g/>
Ruchoth	Ruchoth	k1gMnSc1
RaothJakub	RaothJakub	k1gMnSc1
</s>
<s>
Zahradnictví	zahradnictví	k1gNnSc1
<g/>
:	:	kIx,
DezertérLudvík	DezertérLudvík	k1gMnSc1
Kubeš	Kubeš	k1gMnSc1
</s>
<s>
2018	#num#	k4
<g/>
Všechno	všechen	k3xTgNnSc1
budemotorkář	budemotorkář	k1gMnSc1
Roman	Roman	k1gMnSc1
</s>
<s>
Dukla	Dukla	k1gFnSc1
61	#num#	k4
<g/>
Polok	Polok	k1gInSc1
</s>
<s>
Bůh	bůh	k1gMnSc1
s	s	k7c7
námi	my	k3xPp1nPc7
–	–	k?
od	od	k7c2
defenestrace	defenestrace	k1gFnSc2
k	k	k7c3
Bílé	bílý	k2eAgInPc1d1
hořezimní	hořezimní	k2eAgMnSc1d1
král	král	k1gMnSc1
Fridrich	Fridrich	k1gMnSc1
Falcký	falcký	k2eAgMnSc1d1
</s>
<s>
2019	#num#	k4
<g/>
Přes	přes	k7c4
prstyrozhodčí	prstyrozhodčí	k1gNnSc4
</s>
<s>
2020	#num#	k4
<g/>
Hrana	hrana	k1gFnSc1
zlomu	zlom	k1gInSc2
</s>
<s>
Stockholmský	stockholmský	k2eAgInSc1d1
syndromkumpán	syndromkumpán	k2eAgInSc1d1
</s>
<s>
PorotaOliver	PorotaOliver	k1gMnSc1
Bárta	Bárta	k1gMnSc1
</s>
<s>
Smrt	smrt	k1gFnSc1
talentovaného	talentovaný	k2eAgMnSc2d1
vepře	vepř	k1gMnSc2
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
řezník	řezník	k1gMnSc1
</s>
<s>
2021	#num#	k4
<g/>
Shoky	Shoka	k1gFnSc2
&	&	k?
Morthy	Mortha	k1gFnSc2
<g/>
:	:	kIx,
Poslední	poslední	k2eAgFnSc1d1
velká	velká	k1gFnSc1
akceŠimon	akceŠimona	k1gFnPc2
(	(	kIx(
<g/>
„	„	k?
<g/>
Shoky	Shoka	k1gFnSc2
<g/>
“	“	k?
<g/>
)	)	kIx)
</s>
<s>
Zbožňovanýpřítel	Zbožňovanýpřítel	k1gInSc1
Anety	Aneta	k1gFnSc2
</s>
<s>
Zátopekmladý	Zátopekmladý	k2eAgMnSc1d1
komunista	komunista	k1gMnSc1
</s>
<s>
Volným	volný	k2eAgInSc7d1
pádem	pád	k1gInSc7
</s>
<s>
Televize	televize	k1gFnSc1
a	a	k8xC
internet	internet	k1gInSc1
</s>
<s>
RokNázevRolePoznámky	RokNázevRolePoznámka	k1gFnPc1
</s>
<s>
2014	#num#	k4
<g/>
Pečený	pečený	k2eAgInSc1d1
sněhulákrůzné	sněhulákrůzný	k2eAgFnSc3d1
roletelevizní	roletelevizní	k2eAgFnSc3d1
pořad	pořad	k1gInSc1
</s>
<s>
2014	#num#	k4
<g/>
–	–	k?
<g/>
2016	#num#	k4
<g/>
Rozsudektelevizní	Rozsudektelevizní	k2eAgInSc1d1
seriál	seriál	k1gInSc1
<g/>
,	,	kIx,
díly	díl	k1gInPc1
<g/>
:	:	kIx,
„	„	k?
<g/>
Vychovatelka	vychovatelka	k1gFnSc1
<g/>
“	“	k?
a	a	k8xC
„	„	k?
<g/>
Rukojmí	rukojmí	k1gMnSc1
<g/>
“	“	k?
</s>
<s>
2016	#num#	k4
<g/>
Případy	případ	k1gInPc1
1	#num#	k4
<g/>
.	.	kIx.
odděleníJakub	odděleníJakub	k1gInSc1
Šindelářtelevizní	Šindelářtelevizní	k2eAgInSc1d1
seriál	seriál	k1gInSc1
<g/>
,	,	kIx,
díl	díl	k1gInSc1
<g/>
:	:	kIx,
„	„	k?
<g/>
Súdánský	súdánský	k2eAgMnSc1d1
student	student	k1gMnSc1
<g/>
“	“	k?
</s>
<s>
2017	#num#	k4
<g/>
Četníci	četník	k1gMnPc1
z	z	k7c2
LuhačovicLukáš	LuhačovicLukáš	k1gFnSc2
Mencltelevizní	Mencltelevizní	k2eAgInSc1d1
seriál	seriál	k1gInSc1
<g/>
,	,	kIx,
díl	díl	k1gInSc1
<g/>
:	:	kIx,
„	„	k?
<g/>
Bestie	bestie	k1gFnSc1
<g/>
“	“	k?
</s>
<s>
Policie	policie	k1gFnSc1
ModravaVojta	ModravaVojt	k1gInSc2
Slámatelevizní	Slámatelevizní	k2eAgInSc1d1
seriál	seriál	k1gInSc1
<g/>
,	,	kIx,
2	#num#	k4
díly	díl	k1gInPc1
</s>
<s>
Spravedlnostvyšetřovatel	Spravedlnostvyšetřovatel	k1gMnSc1
Marektelevizní	Marektelevizní	k2eAgFnSc2d1
minisérie	minisérie	k1gFnSc2
</s>
<s>
Kapitán	kapitán	k1gMnSc1
ExnerJan	ExnerJan	k1gMnSc1
Krempatelevizní	Krempatelevizní	k2eAgInSc1d1
seriál	seriál	k1gInSc1
<g/>
,	,	kIx,
2	#num#	k4
díly	díl	k1gInPc1
</s>
<s>
2017	#num#	k4
<g/>
–	–	k?
<g/>
2019	#num#	k4
<g/>
LajnaIgor	LajnaIgor	k1gInSc1
Luptákinternetový	Luptákinternetový	k2eAgInSc4d1
seriál	seriál	k1gInSc4
</s>
<s>
2018	#num#	k4
<g/>
RédlMilectelevizní	RédlMilectelevizní	k2eAgInSc1d1
seriál	seriál	k1gInSc1
<g/>
,	,	kIx,
2	#num#	k4
díly	díl	k1gInPc1
</s>
<s>
SpecialistéMichaltelevizní	SpecialistéMichaltelevizní	k2eAgInSc1d1
seriál	seriál	k1gInSc1
<g/>
,	,	kIx,
díl	díl	k1gInSc1
<g/>
:	:	kIx,
„	„	k?
<g/>
Tajnosti	tajnost	k1gFnPc4
a	a	k8xC
lži	lež	k1gFnPc4
<g/>
“	“	k?
</s>
<s>
2019	#num#	k4
<g/>
Luptákův	Luptákův	k2eAgInSc1d1
vlogísekLuptákinternetový	vlogísekLuptákinternetový	k2eAgInSc1d1
seriál	seriál	k1gInSc1
</s>
<s>
Hrobáritelevizní	Hrobáritelevizní	k2eAgInSc1d1
seriál	seriál	k1gInSc1
<g/>
,	,	kIx,
díl	díl	k1gInSc1
<g/>
:	:	kIx,
„	„	k?
<g/>
Tri	Tri	k1gMnSc1
želania	želanium	k1gNnSc2
<g/>
“	“	k?
</s>
<s>
2019	#num#	k4
<g/>
–	–	k?
<g/>
Tři	tři	k4xCgMnPc4
tygřirůzné	tygřirůzný	k2eAgFnSc2d1
roleinternetový	roleinternetový	k2eAgInSc4d1
seriál	seriál	k1gInSc4
<g/>
,	,	kIx,
série	série	k1gFnSc1
skečů	skeč	k1gInPc2
</s>
<s>
2020	#num#	k4
<g/>
–	–	k?
<g/>
Už	už	k6eAd1
tam	tam	k6eAd1
budem	budem	k?
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
docent	docent	k1gMnSc1
Štrunctelevizní	Štrunctelevizní	k2eAgInSc4d1
pořad	pořad	k1gInSc4
</s>
<s>
2020	#num#	k4
<g/>
sKORO	skoro	k6eAd1
NA	na	k7c4
miziněŠtěpán	miziněŠtěpán	k2eAgInSc4d1
Kozubinternetový	Kozubinternetový	k2eAgInSc4d1
seriálhrál	seriálhrál	k1gInSc4
fiktivní	fiktivní	k2eAgFnSc4d1
verzi	verze	k1gFnSc4
sebe	sebe	k3xPyFc4
sama	sám	k3xTgFnSc1
</s>
<s>
Místo	místo	k7c2
zločinu	zločin	k1gInSc2
OstravaMatěj	OstravaMatěj	k1gFnSc2
Sehnaltelevizní	Sehnaltelevizeň	k1gFnPc2
seriál	seriál	k1gInSc1
</s>
<s>
VyPlašenírůzné	VyPlašenírůzný	k2eAgNnSc4d1
roleinternetový	roleinternetový	k2eAgInSc4d1
pořad	pořad	k1gInSc4
</s>
<s>
2021	#num#	k4
<g/>
Zločiny	zločin	k1gInPc1
Velké	velký	k2eAgInPc1d1
PrahyKleindiesttelevizní	PrahyKleindiesttelevizeň	k1gFnSc7
seriál	seriál	k1gInSc1
<g/>
,	,	kIx,
díl	díl	k1gInSc1
<g/>
:	:	kIx,
„	„	k?
<g/>
Zmizelá	zmizelý	k2eAgFnSc1d1
<g/>
“	“	k?
</s>
<s>
Ptáci	pták	k1gMnPc1
na	na	k7c4
kolejíchtelevizní	kolejíchtelevizní	k2eAgInSc4d1
seriál	seriál	k1gInSc4
</s>
<s>
Režie	režie	k1gFnSc1
</s>
<s>
2019	#num#	k4
<g/>
-dosud	-dosud	k1gMnSc1
–	–	k?
Tři	tři	k4xCgMnPc1
tygři	tygr	k1gMnPc1
(	(	kIx(
<g/>
skeče	skeč	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
2020	#num#	k4
–	–	k?
VyPlašení	vyplašení	k1gNnSc1
(	(	kIx(
<g/>
internetová	internetový	k2eAgFnSc1d1
improvizační	improvizační	k2eAgFnSc1d1
parodie	parodie	k1gFnSc1
na	na	k7c4
reality	realita	k1gFnPc4
show	show	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
2020	#num#	k4
–	–	k?
Riško	Riško	k1gNnSc4
a	a	k8xC
Fučo	Fučo	k1gNnSc4
<g/>
:	:	kIx,
Na	na	k7c6
konci	konec	k1gInSc6
cesty	cesta	k1gFnSc2
(	(	kIx(
<g/>
film	film	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
2021	#num#	k4
–	–	k?
Volným	volný	k2eAgInSc7d1
pádem	pád	k1gInSc7
(	(	kIx(
<g/>
film	film	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
2021	#num#	k4
–	–	k?
Kumštýři	kumštýř	k1gMnPc1
(	(	kIx(
<g/>
film	film	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Divadelní	divadelní	k2eAgFnSc1d1
role	role	k1gFnSc1
<g/>
,	,	kIx,
výběr	výběr	k1gInSc1
</s>
<s>
2015	#num#	k4
William	William	k1gInSc1
Shakespeare	Shakespeare	k1gMnSc1
<g/>
:	:	kIx,
Něco	něco	k3yInSc1
za	za	k7c4
něco	něco	k3yInSc4
<g/>
,	,	kIx,
kníže	kníže	k1gMnSc1
vídeňský	vídeňský	k2eAgMnSc1d1
Vincentio	Vincentio	k1gMnSc1
<g/>
,	,	kIx,
Komorní	komorní	k2eAgFnSc1d1
scéna	scéna	k1gFnSc1
Aréna	aréna	k1gFnSc1
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Ivan	Ivan	k1gMnSc1
Krejčí	Krejčí	k1gMnSc1
</s>
<s>
2016	#num#	k4
Ladislav	Ladislav	k1gMnSc1
Klíma	Klíma	k1gMnSc1
<g/>
:	:	kIx,
Lidská	lidský	k2eAgFnSc1d1
tragikomedie	tragikomedie	k1gFnSc1
<g/>
,	,	kIx,
Odjinud	odjinud	k6eAd1
<g/>
,	,	kIx,
Komorní	komorní	k2eAgFnSc1d1
scéna	scéna	k1gFnSc1
Aréna	aréna	k1gFnSc1
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Ivan	Ivan	k1gMnSc1
Krejčí	Krejčí	k1gMnSc1
</s>
<s>
2016	#num#	k4
Urs	Urs	k1gFnSc1
Widmer	Widmra	k1gFnPc2
<g/>
:	:	kIx,
Top	topit	k5eAaImRp2nS
Dogs	Dogs	k1gInSc1
<g/>
,	,	kIx,
pan	pan	k1gMnSc1
Kozub	kozub	k1gInSc1
<g/>
,	,	kIx,
Komorní	komorní	k2eAgFnSc1d1
scéna	scéna	k1gFnSc1
Aréna	aréna	k1gFnSc1
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Grzegorz	Grzegorz	k1gMnSc1
Kempinsky	Kempinsky	k1gMnSc1
</s>
<s>
2017	#num#	k4
Tomáš	Tomáš	k1gMnSc1
Vůjtek	Vůjtek	k1gMnSc1
<g/>
:	:	kIx,
Smíření	smíření	k1gNnSc1
<g/>
,	,	kIx,
bratr	bratr	k1gMnSc1
<g/>
,	,	kIx,
Komorní	komorní	k2eAgFnSc1d1
scéna	scéna	k1gFnSc1
Aréna	aréna	k1gFnSc1
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Ivan	Ivan	k1gMnSc1
Krejčí	Krejčí	k1gMnSc1
</s>
<s>
2018	#num#	k4
Anton	Anton	k1gMnSc1
Pavlovič	Pavlovič	k1gMnSc1
Čechov	Čechov	k1gMnSc1
<g/>
:	:	kIx,
Tři	tři	k4xCgFnPc1
sestry	sestra	k1gFnPc1
<g/>
,	,	kIx,
Nikolaj	Nikolaj	k1gMnSc1
Tuzenbach	Tuzenbach	k1gMnSc1
<g/>
,	,	kIx,
Komorní	komorní	k2eAgFnSc1d1
scéna	scéna	k1gFnSc1
Aréna	aréna	k1gFnSc1
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Ivan	Ivan	k1gMnSc1
Krejčí	Krejčí	k1gMnSc1
</s>
<s>
2018	#num#	k4
Sébastien	Sébastien	k1gInSc1
Thiéry	Thiéra	k1gFnSc2
<g/>
:	:	kIx,
Kdo	kdo	k3yInSc1,k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
pan	pan	k1gMnSc1
Schmitt	Schmitt	k1gMnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
,	,	kIx,
pan	pan	k1gMnSc1
Béliére	Béliér	k1gMnSc5
<g/>
,	,	kIx,
Divadlo	divadlo	k1gNnSc1
Mír	Míra	k1gFnPc2
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Václav	Václav	k1gMnSc1
Klemens	Klemens	k1gInSc1
</s>
<s>
2019	#num#	k4
Mike	Mike	k1gNnSc1
Bartlett	Bartletta	k1gFnPc2
<g/>
:	:	kIx,
Bull	bulla	k1gFnPc2
<g/>
,	,	kIx,
Michal	Michal	k1gMnSc1
<g/>
,	,	kIx,
Divadlo	divadlo	k1gNnSc1
Mír	Míra	k1gFnPc2
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Albert	Alberta	k1gFnPc2
Čuba	čuba	k1gFnSc1
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2019	#num#	k4
Clément	Clément	k1gMnSc1
Michel	Michel	k1gMnSc1
<g/>
:	:	kIx,
Už	už	k6eAd1
ani	ani	k8xC
den	den	k1gInSc4
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
,	,	kIx,
Přemysl	Přemysl	k1gMnSc1
<g/>
,	,	kIx,
Divadlo	divadlo	k1gNnSc1
Mír	Míra	k1gFnPc2
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Albert	Alberta	k1gFnPc2
Čuba	čuba	k1gFnSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
SLÍVOVÁ	Slívová	k1gFnSc1
<g/>
,	,	kIx,
Hana	Hana	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
herci	herec	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
milují	milovat	k5eAaImIp3nP
pozornost	pozornost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mně	já	k3xPp1nSc3
je	být	k5eAaImIp3nS
při	při	k7c6
příchodu	příchod	k1gInSc6
na	na	k7c4
scénu	scéna	k1gFnSc4
zle	zle	k6eAd1
<g/>
,	,	kIx,
říká	říkat	k5eAaImIp3nS
herec	herec	k1gMnSc1
Štěpán	Štěpán	k1gMnSc1
Kozub	kozub	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-08-01	2020-08-01	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
JANDOVÁ	Jandová	k1gFnSc1
<g/>
,	,	kIx,
Lucie	Lucie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Štěpán	Štěpán	k1gMnSc1
Kozub	kozub	k1gInSc1
<g/>
:	:	kIx,
Měl	mít	k5eAaImAgInS
jsem	být	k5eAaImIp1nS
110	#num#	k4
kilo	kilo	k1gNnSc1
a	a	k8xC
říkali	říkat	k5eAaImAgMnP
<g/>
,	,	kIx,
že	že	k8xS
jsem	být	k5eAaImIp1nS
k	k	k7c3
ničemu	nic	k3yNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Právo	právo	k1gNnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-05-16	2020-05-16	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
TAUSSIKOVÁ	TAUSSIKOVÁ	kA
<g/>
,	,	kIx,
Alice	Alice	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fenomenální	fenomenální	k2eAgInSc4d1
rok	rok	k1gInSc4
Komorní	komorní	k2eAgFnSc2d1
scény	scéna	k1gFnSc2
Aréna	aréna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Divadelní	divadelní	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2016-03-25	2016-03-25	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
SPÁČILOVÁ	Spáčilová	k1gFnSc1
<g/>
,	,	kIx,
Mirka	Mirka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Štěpán	Štěpána	k1gFnPc2
Kozub	kozub	k1gInSc4
ze	z	k7c2
seriálu	seriál	k1gInSc2
sKORO	skoro	k6eAd1
NA	na	k7c6
mizině	mizina	k1gFnSc6
točí	točit	k5eAaImIp3nS
film	film	k1gInSc4
Hrana	hrana	k1gFnSc1
zlomu	zlom	k1gInSc2
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-01-02	2020-01-02	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Štěpán	Štěpán	k1gMnSc1
Kozub	kozub	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Divadlo	divadlo	k1gNnSc1
Mír	Míra	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Divadlo	divadlo	k1gNnSc1
Mír	Míra	k1gFnPc2
<g/>
,	,	kIx,
2020-06-19	2020-06-19	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
BENNETOVÁ	BENNETOVÁ	kA
<g/>
,	,	kIx,
Karolína	Karolína	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostravský	ostravský	k2eAgMnSc1d1
herec	herec	k1gMnSc1
Štěpán	Štěpán	k1gMnSc1
Kozub	kozub	k1gInSc4
se	se	k3xPyFc4
v	v	k7c6
pondělí	pondělí	k1gNnSc6
stal	stát	k5eAaPmAgMnS
tatínkem	tatínek	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moravskoslezský	moravskoslezský	k2eAgInSc1d1
deník	deník	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
BÁTOR	BÁTOR	kA
<g/>
,	,	kIx,
Milan	Milan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
Štěpána	Štěpán	k1gMnSc2
Kozuba	Kozub	k1gMnSc2
v	v	k7c6
písních	píseň	k1gFnPc6
Jiřího	Jiří	k1gMnSc2
Krhuta	Krhut	k1gMnSc2
vyrostl	vyrůst	k5eAaPmAgMnS
zajímavý	zajímavý	k2eAgMnSc1d1
zpěvák	zpěvák	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Album	album	k1gNnSc1
Prásknu	prásknout	k5eAaPmIp1nS
bičem	bič	k1gInSc7
je	být	k5eAaImIp3nS
velkým	velký	k2eAgNnSc7d1
překvapením	překvapení	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostravan	Ostravan	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
BENNETOVÁ	BENNETOVÁ	kA
<g/>
,	,	kIx,
Karolína	Karolína	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Herec	herec	k1gMnSc1
Štěpán	Štěpán	k1gMnSc1
Kozub	kozub	k1gInSc4
je	být	k5eAaImIp3nS
mužem	muž	k1gMnSc7
mnoha	mnoho	k4c2
talentů	talent	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Internet	Internet	k1gInSc1
teď	teď	k6eAd1
baví	bavit	k5eAaImIp3nS
svým	svůj	k3xOyFgInSc7
zpěvem	zpěv	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deník	deník	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
KOSTKOVÁ	Kostková	k1gFnSc1
<g/>
,	,	kIx,
Tereza	Tereza	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Miluji	milovat	k5eAaImIp1nS
improvizaci	improvizace	k1gFnSc4
a	a	k8xC
zároveň	zároveň	k6eAd1
nenávidím	návidět	k5eNaImIp1nS,k5eAaImIp1nS
<g/>
,	,	kIx,
když	když	k8xS
nemám	mít	k5eNaImIp1nS
jistotu	jistota	k1gFnSc4
<g/>
,	,	kIx,
přiznává	přiznávat	k5eAaImIp3nS
herec	herec	k1gMnSc1
Štěpán	Štěpán	k1gMnSc1
Kozub	kozub	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
STAŠOVÁ	STAŠOVÁ	kA
<g/>
,	,	kIx,
Gabriela	Gabriela	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Herectví	herectví	k1gNnSc1
v	v	k7c6
době	doba	k1gFnSc6
pandemie	pandemie	k1gFnSc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
potřeba	potřeba	k6eAd1
ukázat	ukázat	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
svět	svět	k1gInSc1
nekončí	končit	k5eNaImIp3nS
<g/>
,	,	kIx,
říkají	říkat	k5eAaImIp3nP
známé	známý	k2eAgFnPc4d1
tváře	tvář	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deník	deník	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
KUTĚJ	KUTĚJ	k?
<g/>
,	,	kIx,
Richard	Richard	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Štěpán	Štěpán	k1gMnSc1
Kozub	kozub	k1gInSc4
natočil	natočit	k5eAaBmAgMnS
film	film	k1gInSc4
Volným	volný	k2eAgInSc7d1
pádem	pád	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Premiéra	premiéra	k1gFnSc1
byla	být	k5eAaImAgFnS
v	v	k7c4
sobotu	sobota	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deník	deník	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
Smrt	smrt	k1gFnSc4
talentovaného	talentovaný	k2eAgMnSc2d1
vepře	vepř	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Divadlo	divadlo	k1gNnSc1
Mír	Míra	k1gFnPc2
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
MALINDA	MALINDA	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někdo	někdo	k3yInSc1
ty	ten	k3xDgMnPc4
psychopaty	psychopat	k1gMnPc4
musí	muset	k5eAaImIp3nP
hrát	hrát	k5eAaImF
<g/>
,	,	kIx,
říká	říkat	k5eAaImIp3nS
specialista	specialista	k1gMnSc1
na	na	k7c4
trotly	trotl	k1gMnPc4
Štěpán	Štěpána	k1gFnPc2
Kozub	kozub	k1gInSc1
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Štěpán	Štěpán	k1gMnSc1
Kozub	kozub	k1gInSc1
v	v	k7c6
Česko-Slovenské	česko-slovenský	k2eAgFnSc6d1
filmové	filmový	k2eAgFnSc6d1
databázi	databáze	k1gFnSc6
</s>
<s>
Štěpán	Štěpán	k1gMnSc1
Kozub	kozub	k1gInSc1
ve	v	k7c6
Filmové	filmový	k2eAgFnSc6d1
databázi	databáze	k1gFnSc6
</s>
<s>
Štěpán	Štěpán	k1gMnSc1
Kozub	kozub	k1gInSc4
v	v	k7c4
Internet	Internet	k1gInSc4
Movie	Movie	k1gFnSc2
Database	Databasa	k1gFnSc3
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Štěpán	Štěpán	k1gMnSc1
Kozub	kozub	k1gInSc4
v	v	k7c6
databázi	databáze	k1gFnSc6
i-divadlo	i-divadlo	k1gNnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
xx	xx	k?
<g/>
0	#num#	k4
<g/>
247584	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Česko	Česko	k1gNnSc1
|	|	kIx~
Film	film	k1gInSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
|	|	kIx~
Televize	televize	k1gFnSc1
</s>
