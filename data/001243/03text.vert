<p>
<s>
John	John	k1gMnSc1	John
Fitzgerald	Fitzgerald	k1gMnSc1	Fitzgerald
Kennedy	Kenneda	k1gMnSc2	Kenneda
(	(	kIx(	(
<g/>
29	[number]	k4	29
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1917	[number]	k4	1917
–	–	k?	–
22.	[number]	k4	22.
listopadu	listopad	k1gInSc2	listopad
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
neformálně	formálně	k6eNd1	formálně
označovaný	označovaný	k2eAgInSc1d1	označovaný
jako	jako	k8xS	jako
Jack	Jack	k1gInSc1	Jack
Kennedy	Kenneda	k1gMnSc2	Kenneda
či	či	k8xC	či
JFK	JFK	kA	JFK
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
35.	[number]	k4	35.
prezident	prezident	k1gMnSc1	prezident
USA	USA	kA	USA
(	(	kIx(	(
<g/>
1961	[number]	k4	1961
<g/>
–	–	k?	–
<g/>
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
jediného	jediný	k2eAgMnSc4d1	jediný
prezidenta	prezident	k1gMnSc4	prezident
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
USA	USA	kA	USA
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
hlásil	hlásit	k5eAaImAgMnS	hlásit
ke	k	k7c3	k
katolické	katolický	k2eAgFnSc3d1	katolická
církvi	církev	k1gFnSc3	církev
<g/>
.	.	kIx.	.
</s>
<s>
Dosud	dosud	k6eAd1	dosud
je	být	k5eAaImIp3nS	být
i	i	k9	i
2.	[number]	k4	2.
nejmladším	mladý	k2eAgMnSc7d3	nejmladší
zvoleným	zvolený	k2eAgMnSc7d1	zvolený
prezidentem	prezident	k1gMnSc7	prezident
(	(	kIx(	(
<g/>
nejmladším	mladý	k2eAgMnSc7d3	nejmladší
prezidentem	prezident	k1gMnSc7	prezident
je	být	k5eAaImIp3nS	být
Theodore	Theodor	k1gMnSc5	Theodor
Roosevelt	Roosevelt	k1gMnSc1	Roosevelt
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
do	do	k7c2	do
úřadu	úřad	k1gInSc2	úřad
ve	v	k7c6	v
42	[number]	k4	42
letech	léto	k1gNnPc6	léto
po	po	k7c6	po
atentátu	atentát	k1gInSc6	atentát
na	na	k7c4	na
svého	svůj	k3xOyFgMnSc4	svůj
předchůdce	předchůdce	k1gMnSc4	předchůdce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Americká	americký	k2eAgFnSc1d1	americká
veřejnost	veřejnost	k1gFnSc1	veřejnost
jej	on	k3xPp3gMnSc4	on
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
největších	veliký	k2eAgMnPc2d3	veliký
prezidentů	prezident	k1gMnPc2	prezident
<g/>
,	,	kIx,	,
historikové	historik	k1gMnPc1	historik
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
opatrnější	opatrný	k2eAgFnPc1d2	opatrnější
a	a	k8xC	a
mluví	mluvit	k5eAaImIp3nP	mluvit
spíše	spíše	k9	spíše
o	o	k7c6	o
mírně	mírně	k6eAd1	mírně
nadprůměrném	nadprůměrný	k2eAgNnSc6d1	nadprůměrné
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Během	během	k7c2	během
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
ukončené	ukončená	k1gFnSc2	ukončená
kulkou	kulka	k1gFnSc7	kulka
atentátníka	atentátník	k1gMnSc2	atentátník
<g/>
,	,	kIx,	,
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
Invaze	invaze	k1gFnSc1	invaze
v	v	k7c6	v
Zátoce	zátoka	k1gFnSc6	zátoka
sviní	svině	k1gFnPc2	svině
<g/>
,	,	kIx,	,
Karibská	karibský	k2eAgFnSc1d1	karibská
krize	krize	k1gFnSc1	krize
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
vybudována	vybudovat	k5eAaPmNgFnS	vybudovat
Berlínská	berlínský	k2eAgFnSc1d1	Berlínská
zeď	zeď	k1gFnSc1	zeď
a	a	k8xC	a
začaly	začít	k5eAaPmAgInP	začít
první	první	k4xOgInPc1	první
střety	střet	k1gInPc1	střet
Války	válka	k1gFnSc2	válka
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
vesmírné	vesmírný	k2eAgNnSc1d1	vesmírné
soupeření	soupeření	k1gNnSc1	soupeření
mezi	mezi	k7c7	mezi
USA	USA	kA	USA
a	a	k8xC	a
SSSR	SSSR	kA	SSSR
a	a	k8xC	a
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
tažení	tažení	k1gNnSc6	tažení
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
i	i	k8xC	i
v	v	k7c6	v
USA	USA	kA	USA
hnutí	hnutí	k1gNnPc2	hnutí
za	za	k7c4	za
lidská	lidský	k2eAgNnPc4d1	lidské
práva	právo	k1gNnPc4	právo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Mládí	mládí	k1gNnSc1	mládí
==	==	k?	==
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
29.	[number]	k4	29.
května	květen	k1gInSc2	květen
1917	[number]	k4	1917
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Brookline	Brooklin	k1gInSc5	Brooklin
v	v	k7c6	v
Massachusetts	Massachusetts	k1gNnPc2	Massachusetts
do	do	k7c2	do
katolické	katolický	k2eAgFnSc2d1	katolická
rodiny	rodina	k1gFnSc2	rodina
s	s	k7c7	s
irskými	irský	k2eAgInPc7d1	irský
kořeny	kořen	k1gInPc7	kořen
Rose	Rose	k1gMnSc1	Rose
Fitzgerald	Fitzgerald	k1gMnSc1	Fitzgerald
Kennedyové	Kennedyová	k1gFnSc2	Kennedyová
a	a	k8xC	a
Josephu	Joseph	k1gInSc2	Joseph
Kennedymu	Kennedym	k1gInSc2	Kennedym
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
sedm	sedm	k4xCc4	sedm
mladších	mladý	k2eAgMnPc2d2	mladší
sourozenců	sourozenec	k1gMnPc2	sourozenec
a	a	k8xC	a
jednoho	jeden	k4xCgMnSc2	jeden
staršího	starý	k2eAgMnSc2d2	starší
bratra	bratr	k1gMnSc2	bratr
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
Joe	Joe	k1gMnSc1	Joe
byl	být	k5eAaImAgMnS	být
bohatý	bohatý	k2eAgMnSc1d1	bohatý
podnikatel	podnikatel	k1gMnSc1	podnikatel
<g/>
,	,	kIx,	,
investor	investor	k1gMnSc1	investor
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
mezi	mezi	k7c4	mezi
roky	rok	k1gInPc4	rok
1938	[number]	k4	1938
a	a	k8xC	a
1940	[number]	k4	1940
působil	působit	k5eAaImAgInS	působit
jako	jako	k9	jako
americký	americký	k2eAgMnSc1d1	americký
velvyslanec	velvyslanec	k1gMnSc1	velvyslanec
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Vychovával	vychovávat	k5eAaImAgMnS	vychovávat
své	svůj	k3xOyFgFnPc4	svůj
děti	dítě	k1gFnPc4	dítě
k	k	k7c3	k
mimořádné	mimořádný	k2eAgFnSc3d1	mimořádná
ctižádosti	ctižádost	k1gFnSc3	ctižádost
<g/>
,	,	kIx,	,
soutěživosti	soutěživost	k1gFnSc3	soutěživost
a	a	k8xC	a
snaze	snaha	k1gFnSc3	snaha
za	za	k7c4	za
každou	každý	k3xTgFnSc4	každý
cenu	cena	k1gFnSc4	cena
vyhrát	vyhrát	k5eAaPmF	vyhrát
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
určitou	určitý	k2eAgFnSc4d1	určitá
dobu	doba	k1gFnSc4	doba
byl	být	k5eAaImAgMnS	být
Joeovým	Joeův	k2eAgMnSc7d1	Joeův
bodyguardem	bodyguard	k1gMnSc7	bodyguard
chameleonský	chameleonský	k2eAgMnSc1d1	chameleonský
Frederick	Frederick	k1gMnSc1	Frederick
Joubert	Joubert	k1gMnSc1	Joubert
Duquesne	Duquesne	k1gMnSc1	Duquesne
<g/>
.	.	kIx.	.
</s>
<s>
Johnova	Johnův	k2eAgFnSc1d1	Johnova
matka	matka	k1gFnSc1	matka
byla	být	k5eAaImAgFnS	být
zbožná	zbožný	k2eAgFnSc1d1	zbožná
katolička	katolička	k1gFnSc1	katolička
<g/>
,	,	kIx,	,
o	o	k7c6	o
jejíž	jejíž	k3xOyRp3gFnSc6	jejíž
výchově	výchova	k1gFnSc6	výchova
literatura	literatura	k1gFnSc1	literatura
udává	udávat	k5eAaImIp3nS	udávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
byla	být	k5eAaImAgFnS	být
pedantská	pedantský	k2eAgFnSc1d1	pedantská
a	a	k8xC	a
neláskyplná	láskyplný	k2eNgFnSc1d1	láskyplný
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
oficiálních	oficiální	k2eAgInPc2d1	oficiální
životopisů	životopis	k1gInPc2	životopis
se	se	k3xPyFc4	se
v	v	k7c6	v
domácnosti	domácnost	k1gFnSc6	domácnost
Kennedyů	Kennedy	k1gInPc2	Kennedy
na	na	k7c4	na
Abbotsford	Abbotsford	k1gInSc4	Abbotsford
Road	Roada	k1gFnPc2	Roada
v	v	k7c6	v
Brooklinu	Brooklin	k1gInSc6	Brooklin
v	v	k7c6	v
Bostonu	Boston	k1gInSc6	Boston
k	k	k7c3	k
sobě	se	k3xPyFc3	se
lidé	člověk	k1gMnPc1	člověk
chovali	chovat	k5eAaImAgMnP	chovat
slušně	slušně	k6eAd1	slušně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
čistém	čistý	k2eAgInSc6d1	čistý
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
se	se	k3xPyFc4	se
nemilovali	milovat	k5eNaImAgMnP	milovat
<g/>
,	,	kIx,	,
existovali	existovat	k5eAaImAgMnP	existovat
vedle	vedle	k7c2	vedle
sebe	se	k3xPyFc2	se
<g/>
,	,	kIx,	,
žili	žít	k5eAaImAgMnP	žít
spolu	spolu	k6eAd1	spolu
<g/>
,	,	kIx,	,
a	a	k8xC	a
děti	dítě	k1gFnPc1	dítě
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
soutěžily	soutěžit	k5eAaImAgInP	soutěžit
o	o	k7c4	o
přízeň	přízeň	k1gFnSc4	přízeň
rodičů	rodič	k1gMnPc2	rodič
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
absolvování	absolvování	k1gNnSc6	absolvování
Choate	Choat	k1gInSc5	Choat
School	School	k1gInSc4	School
ve	v	k7c6	v
Wallingfordu	Wallingford	k1gInSc6	Wallingford
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Connecticut	Connecticut	k1gInSc1	Connecticut
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
JFK	JFK	kA	JFK
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
jeho	jeho	k3xOp3gMnSc1	jeho
starší	starý	k2eAgMnSc1d2	starší
bratr	bratr	k1gMnSc1	bratr
Joe	Joe	k1gMnSc1	Joe
ml	ml	kA	ml
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
na	na	k7c4	na
Harvard	Harvard	k1gInSc4	Harvard
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1940	[number]	k4	1940
absolvoval	absolvovat	k5eAaPmAgInS	absolvovat
s	s	k7c7	s
vyznamenáním	vyznamenání	k1gNnSc7	vyznamenání
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
sklonku	sklonek	k1gInSc6	sklonek
třicátých	třicátý	k4xOgNnPc2	třicátý
let	léto	k1gNnPc2	léto
cestoval	cestovat	k5eAaImAgMnS	cestovat
po	po	k7c6	po
Evropě	Evropa	k1gFnSc6	Evropa
i	i	k8xC	i
Blízkém	blízký	k2eAgInSc6d1	blízký
východě	východ	k1gInSc6	východ
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
navštívil	navštívit	k5eAaPmAgMnS	navštívit
také	také	k9	také
okupovanou	okupovaný	k2eAgFnSc4d1	okupovaná
Prahu	Praha	k1gFnSc4	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
diplomové	diplomový	k2eAgFnSc6d1	Diplomová
práci	práce	k1gFnSc6	práce
Mnichovský	mnichovský	k2eAgInSc1d1	mnichovský
appeasement	appeasement	k1gInSc1	appeasement
obhajoval	obhajovat	k5eAaImAgInS	obhajovat
Mnichovskou	mnichovský	k2eAgFnSc4d1	Mnichovská
dohodu	dohoda	k1gFnSc4	dohoda
a	a	k8xC	a
vysvětloval	vysvětlovat	k5eAaImAgMnS	vysvětlovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Británii	Británie	k1gFnSc3	Británie
a	a	k8xC	a
Francii	Francie	k1gFnSc3	Francie
poskytla	poskytnout	k5eAaPmAgFnS	poskytnout
čas	čas	k1gInSc4	čas
na	na	k7c4	na
válečné	válečný	k2eAgFnPc4d1	válečná
přípravy	příprava	k1gFnPc4	příprava
<g/>
.	.	kIx.	.
</s>
<s>
Práci	práce	k1gFnSc4	práce
později	pozdě	k6eAd2	pozdě
díky	díky	k7c3	díky
jeho	jeho	k3xOp3gMnSc3	jeho
otci	otec	k1gMnSc3	otec
přepracoval	přepracovat	k5eAaPmAgMnS	přepracovat
komentátor	komentátor	k1gMnSc1	komentátor
deníku	deník	k1gInSc2	deník
New	New	k1gFnPc2	New
York	York	k1gInSc1	York
Times	Times	k1gMnSc1	Times
Arthur	Arthur	k1gMnSc1	Arthur
Krock	Krock	k1gMnSc1	Krock
a	a	k8xC	a
vyšla	vyjít	k5eAaPmAgFnS	vyjít
knižně	knižně	k6eAd1	knižně
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Proč	proč	k6eAd1	proč
Anglie	Anglie	k1gFnSc1	Anglie
spala	spát	k5eAaImAgFnS	spát
<g/>
.	.	kIx.	.
</s>
<s>
Stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
bestsellerem	bestseller	k1gInSc7	bestseller
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
USA	USA	kA	USA
se	se	k3xPyFc4	se
prodalo	prodat	k5eAaPmAgNnS	prodat
55 000	[number]	k4	55 000
a	a	k8xC	a
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
35 000	[number]	k4	35 000
výtisků	výtisk	k1gInPc2	výtisk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
–	–	k?	–
přes	přes	k7c4	přes
svůj	svůj	k3xOyFgInSc4	svůj
chatrný	chatrný	k2eAgInSc4d1	chatrný
zdravotní	zdravotní	k2eAgInSc4d1	zdravotní
stav	stav	k1gInSc4	stav
–	–	k?	–
do	do	k7c2	do
armády	armáda	k1gFnSc2	armáda
v	v	k7c6	v
hodnosti	hodnost	k1gFnSc6	hodnost
podporučíka	podporučík	k1gMnSc2	podporučík
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
2	[number]	k4	2
roky	rok	k1gInPc7	rok
později	pozdě	k6eAd2	pozdě
působil	působit	k5eAaImAgInS	působit
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Šalamounových	Šalamounových	k2eAgInPc2d1	Šalamounových
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jako	jako	k9	jako
poručík	poručík	k1gMnSc1	poručík
(	(	kIx(	(
<g/>
Lieutenant	Lieutenant	k1gMnSc1	Lieutenant
(	(	kIx(	(
<g/>
Junior	junior	k1gMnSc1	junior
Grade	grad	k1gInSc5	grad
<g/>
))	))	k?	))
velel	velet	k5eAaImAgInS	velet
torpédovému	torpédový	k2eAgInSc3d1	torpédový
člunu	člun	k1gInSc2	člun
PT-109	PT-109	k1gFnPc2	PT-109
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
noci	noc	k1gFnSc6	noc
z	z	k7c2	z
1.	[number]	k4	1.
na	na	k7c4	na
2.	[number]	k4	2.
srpna	srpen	k1gInSc2	srpen
1943	[number]	k4	1943
byl	být	k5eAaImAgMnS	být
PT-109	PT-109	k1gMnSc1	PT-109
taranován	taranován	k2eAgMnSc1d1	taranován
a	a	k8xC	a
potopen	potopen	k2eAgMnSc1d1	potopen
v	v	k7c6	v
úžině	úžina	k1gFnSc6	úžina
Blackett	Blackett	k1gMnSc1	Blackett
japonským	japonský	k2eAgMnSc7d1	japonský
torpédoborcem	torpédoborec	k1gMnSc7	torpédoborec
Amagiri	Amagir	k1gFnSc2	Amagir
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
utrpěl	utrpět	k5eAaPmAgMnS	utrpět
JFK	JFK	kA	JFK
další	další	k2eAgMnSc1d1	další
vážné	vážný	k2eAgNnSc4d1	vážné
zranění	zranění	k1gNnSc4	zranění
zad	záda	k1gNnPc2	záda
(	(	kIx(	(
<g/>
první	první	k4xOgFnSc7	první
měl	mít	k5eAaImAgMnS	mít
již	již	k6eAd1	již
z	z	k7c2	z
amerického	americký	k2eAgInSc2d1	americký
fotbalu	fotbal	k1gInSc2	fotbal
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
se	se	k3xPyFc4	se
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
posádkou	posádka	k1gFnSc7	posádka
skrýval	skrývat	k5eAaImAgInS	skrývat
bez	bez	k7c2	bez
jídla	jídlo	k1gNnSc2	jídlo
na	na	k7c6	na
březích	břeh	k1gInPc6	břeh
ostrůvku	ostrůvek	k1gInSc2	ostrůvek
<g/>
,	,	kIx,	,
než	než	k8xS	než
byli	být	k5eAaImAgMnP	být
zachráněni	zachránit	k5eAaPmNgMnP	zachránit
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
kritickou	kritický	k2eAgFnSc4d1	kritická
událost	událost	k1gFnSc4	událost
mu	on	k3xPp3gMnSc3	on
později	pozdě	k6eAd2	pozdě
připomínal	připomínat	k5eAaImAgInS	připomínat
kokos	kokos	k1gInSc1	kokos
s	s	k7c7	s
vyrytou	vyrytý	k2eAgFnSc7d1	vyrytá
zprávou	zpráva	k1gFnSc7	zpráva
o	o	k7c6	o
trosečnících	trosečník	k1gMnPc6	trosečník
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
pracovním	pracovní	k2eAgInSc6d1	pracovní
stole	stol	k1gInSc6	stol
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
obdržel	obdržet	k5eAaPmAgMnS	obdržet
Purpurové	purpurový	k2eAgNnSc4d1	purpurové
srdce	srdce	k1gNnSc4	srdce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
pracoval	pracovat	k5eAaImAgMnS	pracovat
krátce	krátce	k6eAd1	krátce
jako	jako	k9	jako
novinář	novinář	k1gMnSc1	novinář
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
přítomen	přítomen	k2eAgInSc1d1	přítomen
při	při	k7c6	při
mírových	mírový	k2eAgInPc6d1	mírový
rozhovorech	rozhovor	k1gInPc6	rozhovor
v	v	k7c6	v
Postupimi	Postupim	k1gFnSc6	Postupim
a	a	k8xC	a
na	na	k7c6	na
zakládací	zakládací	k2eAgFnSc6d1	zakládací
schůzi	schůze	k1gFnSc6	schůze
OSN	OSN	kA	OSN
v	v	k7c6	v
San	San	k1gFnSc6	San
Francisku	Francisek	k1gInSc2	Francisek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
celého	celý	k2eAgNnSc2d1	celé
dětství	dětství	k1gNnSc2	dětství
a	a	k8xC	a
dospívání	dospívání	k1gNnSc2	dospívání
prošel	projít	k5eAaPmAgInS	projít
několika	několik	k4yIc7	několik
školami	škola	k1gFnPc7	škola
a	a	k8xC	a
prodělal	prodělat	k5eAaPmAgMnS	prodělat
snad	snad	k9	snad
všechny	všechen	k3xTgFnPc4	všechen
možné	možný	k2eAgFnPc4d1	možná
dětské	dětský	k2eAgFnPc4d1	dětská
nemoci	nemoc	k1gFnPc4	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
menšími	malý	k2eAgInPc7d2	menší
či	či	k8xC	či
větším	veliký	k2eAgInSc7d2	veliký
přestávkami	přestávka	k1gFnPc7	přestávka
v	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
měl	mít	k5eAaImAgMnS	mít
zdravotní	zdravotní	k2eAgInPc4d1	zdravotní
problémy	problém	k1gInPc4	problém
téměř	téměř	k6eAd1	téměř
pořád	pořád	k6eAd1	pořád
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
lékaři	lékař	k1gMnPc1	lékař
zjistili	zjistit	k5eAaPmAgMnP	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
trpí	trpět	k5eAaImIp3nP	trpět
Addisonovou	Addisonový	k2eAgFnSc7d1	Addisonový
nemocí	nemoc	k1gFnSc7	nemoc
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
mu	on	k3xPp3gMnSc3	on
pomalu	pomalu	k6eAd1	pomalu
likvidovala	likvidovat	k5eAaBmAgFnS	likvidovat
nadledviny	nadledvina	k1gFnPc4	nadledvina
a	a	k8xC	a
imunitní	imunitní	k2eAgInSc4d1	imunitní
systém	systém	k1gInSc4	systém
a	a	k8xC	a
nutila	nutit	k5eAaImAgFnS	nutit
ho	on	k3xPp3gMnSc4	on
brát	brát	k5eAaImF	brát
pravidelně	pravidelně	k6eAd1	pravidelně
injekce	injekce	k1gFnSc2	injekce
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válečném	válečný	k2eAgNnSc6d1	válečné
zranění	zranění	k1gNnSc6	zranění
měl	mít	k5eAaImAgInS	mít
vleklé	vleklý	k2eAgInPc4d1	vleklý
problémy	problém	k1gInPc4	problém
se	s	k7c7	s
zády	záda	k1gNnPc7	záda
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
nimž	jenž	k3xRgFnPc3	jenž
musel	muset	k5eAaImAgMnS	muset
prodělat	prodělat	k5eAaPmF	prodělat
několik	několik	k4yIc4	několik
operací	operace	k1gFnPc2	operace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
12.	[number]	k4	12.
srpna	srpen	k1gInSc2	srpen
1944	[number]	k4	1944
tragicky	tragicky	k6eAd1	tragicky
zahynul	zahynout	k5eAaPmAgMnS	zahynout
jeho	jeho	k3xOp3gMnSc1	jeho
starší	starý	k2eAgMnSc1d2	starší
bratr	bratr	k1gMnSc1	bratr
Joe	Joe	k1gMnSc1	Joe
v	v	k7c6	v
letecké	letecký	k2eAgFnSc6d1	letecká
vojenské	vojenský	k2eAgFnSc6d1	vojenská
akci	akce	k1gFnSc6	akce
<g/>
.	.	kIx.	.
</s>
<s>
Joe	Joe	k?	Joe
Kennedy	Kenneda	k1gMnSc2	Kenneda
ml	ml	kA	ml
<g/>
.	.	kIx.	.
pilotoval	pilotovat	k5eAaImAgMnS	pilotovat
bombardér	bombardér	k1gMnSc1	bombardér
PB4Y-1	PB4Y-1	k1gMnSc1	PB4Y-1
Liberator	Liberator	k1gMnSc1	Liberator
(	(	kIx(	(
<g/>
BuNo	buna	k1gFnSc5	buna
32271	[number]	k4	32271
<g/>
,	,	kIx,	,
T-11	T-11	k1gFnSc1	T-11
<g/>
)	)	kIx)	)
přestavěný	přestavěný	k2eAgInSc1d1	přestavěný
na	na	k7c4	na
létající	létající	k2eAgFnSc4d1	létající
pumu	puma	k1gFnSc4	puma
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
splněno	splněn	k2eAgNnSc4d1	splněno
25	[number]	k4	25
ostrých	ostrý	k2eAgFnPc2d1	ostrá
akcí	akce	k1gFnPc2	akce
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
příležitost	příležitost	k1gFnSc4	příležitost
odcestovat	odcestovat	k5eAaPmF	odcestovat
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
na	na	k7c4	na
poslední	poslední	k2eAgInSc4d1	poslední
let	let	k1gInSc4	let
v	v	k7c6	v
operaci	operace	k1gFnSc6	operace
Afrodita	Afrodita	k1gFnSc1	Afrodita
přihlásil	přihlásit	k5eAaPmAgMnS	přihlásit
jako	jako	k9	jako
dobrovolník	dobrovolník	k1gMnSc1	dobrovolník
<g/>
.	.	kIx.	.
</s>
<s>
Liberator	Liberator	k1gInSc1	Liberator
byl	být	k5eAaImAgInS	být
naložen	naložit	k5eAaPmNgInS	naložit
výbušninami	výbušnina	k1gFnPc7	výbušnina
(	(	kIx(	(
<g/>
celkem	celkem	k6eAd1	celkem
9602,5	[number]	k4	9602,5
kg	kg	kA	kg
Torpexu	Torpex	k1gInSc2	Torpex
a	a	k8xC	a
272,1	[number]	k4	272,1
kg	kg	kA	kg
TNT	TNT	kA	TNT
<g/>
)	)	kIx)	)
a	a	k8xC	a
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
naveden	navést	k5eAaPmNgInS	navést
na	na	k7c4	na
rozestavěné	rozestavěný	k2eAgNnSc4d1	rozestavěné
postavení	postavení	k1gNnSc4	postavení
německého	německý	k2eAgMnSc2d1	německý
150mm	150m	k1gNnSc7	150m
děla	dít	k5eAaBmAgFnS	dít
V-3	V-3	k1gMnSc3	V-3
u	u	k7c2	u
Mimoyecques	Mimoyecquesa	k1gFnPc2	Mimoyecquesa
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
nad	nad	k7c7	nad
Anglií	Anglie	k1gFnSc7	Anglie
ale	ale	k8xC	ale
Liberator	Liberator	k1gInSc1	Liberator
explodoval	explodovat	k5eAaBmAgInS	explodovat
<g/>
.	.	kIx.	.
</s>
<s>
JFK	JFK	kA	JFK
tak	tak	k6eAd1	tak
zaujal	zaujmout	k5eAaPmAgInS	zaujmout
místo	místo	k7c2	místo
nejstaršího	starý	k2eAgMnSc2d3	nejstarší
syna	syn	k1gMnSc2	syn
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
přebral	přebrat	k5eAaPmAgMnS	přebrat
i	i	k9	i
břemeno	břemeno	k1gNnSc4	břemeno
politických	politický	k2eAgFnPc2d1	politická
ambicí	ambice	k1gFnPc2	ambice
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Počátky	počátek	k1gInPc4	počátek
politické	politický	k2eAgFnSc2d1	politická
kariéry	kariéra	k1gFnSc2	kariéra
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
věnovat	věnovat	k5eAaPmF	věnovat
politice	politika	k1gFnSc3	politika
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
kongresmanem	kongresman	k1gMnSc7	kongresman
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
poprvé	poprvé	k6eAd1	poprvé
senátorem	senátor	k1gMnSc7	senátor
(	(	kIx(	(
<g/>
znovuzvolen	znovuzvolit	k5eAaPmNgInS	znovuzvolit
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
za	za	k7c4	za
stát	stát	k1gInSc4	stát
Massachusetts	Massachusetts	k1gNnSc2	Massachusetts
<g/>
,	,	kIx,	,
když	když	k8xS	když
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
o	o	k7c4	o
70 000	[number]	k4	70 000
hlasů	hlas	k1gInPc2	hlas
nad	nad	k7c7	nad
republikánským	republikánský	k2eAgMnSc7d1	republikánský
senátorem	senátor	k1gMnSc7	senátor
Henry	Henry	k1gMnSc1	Henry
Cabotem	Cabot	k1gMnSc7	Cabot
Lodgem	Lodg	k1gMnSc7	Lodg
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
Lodge	Lodge	k1gInSc1	Lodge
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
nominován	nominovat	k5eAaBmNgMnS	nominovat
jako	jako	k9	jako
kandidát	kandidát	k1gMnSc1	kandidát
na	na	k7c4	na
viceprezidenta	viceprezident	k1gMnSc4	viceprezident
v	v	k7c6	v
republikánské	republikánský	k2eAgFnSc6d1	republikánská
straně	strana	k1gFnSc6	strana
a	a	k8xC	a
opět	opět	k6eAd1	opět
proti	proti	k7c3	proti
Kennedymu	Kennedym	k1gInSc3	Kennedym
prohrál	prohrát	k5eAaPmAgMnS	prohrát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
ho	on	k3xPp3gMnSc4	on
JFK	JFK	kA	JFK
jmenoval	jmenovat	k5eAaBmAgInS	jmenovat
velvyslancem	velvyslanec	k1gMnSc7	velvyslanec
v	v	k7c6	v
Jižním	jižní	k2eAgInSc6d1	jižní
Vietnamu	Vietnam	k1gInSc6	Vietnam
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
se	se	k3xPyFc4	se
JFK	JFK	kA	JFK
pustil	pustit	k5eAaPmAgInS	pustit
do	do	k7c2	do
budování	budování	k1gNnSc2	budování
svého	svůj	k3xOyFgInSc2	svůj
vlivu	vliv	k1gInSc2	vliv
a	a	k8xC	a
popularity	popularita	k1gFnSc2	popularita
v	v	k7c6	v
očích	oko	k1gNnPc6	oko
amerického	americký	k2eAgInSc2d1	americký
národa	národ	k1gInSc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
rozhodnut	rozhodnout	k5eAaPmNgMnS	rozhodnout
stát	stát	k5eAaPmF	stát
se	se	k3xPyFc4	se
prezidentem	prezident	k1gMnSc7	prezident
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
úsilí	úsilí	k1gNnSc6	úsilí
ho	on	k3xPp3gInSc4	on
bezvýhradně	bezvýhradně	k6eAd1	bezvýhradně
podporovala	podporovat	k5eAaImAgFnS	podporovat
celá	celý	k2eAgFnSc1d1	celá
rodina	rodina	k1gFnSc1	rodina
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
kampaně	kampaň	k1gFnSc2	kampaň
se	se	k3xPyFc4	se
i	i	k9	i
přes	přes	k7c4	přes
svůj	svůj	k3xOyFgInSc4	svůj
odpor	odpor	k1gInSc4	odpor
k	k	k7c3	k
politice	politika	k1gFnSc3	politika
účastnila	účastnit	k5eAaImAgFnS	účastnit
i	i	k9	i
jeho	jeho	k3xOp3gFnSc1	jeho
manželka	manželka	k1gFnSc1	manželka
Jackie	Jackie	k1gFnSc1	Jackie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
prodělala	prodělat	k5eAaPmAgFnS	prodělat
dvě	dva	k4xCgNnPc4	dva
riziková	rizikový	k2eAgNnPc4d1	rizikové
těhotenství	těhotenství	k1gNnPc4	těhotenství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
prošlo	projít	k5eAaPmAgNnS	projít
jejich	jejich	k3xOp3gNnSc1	jejich
manželství	manželství	k1gNnSc2	manželství
krizi	krize	k1gFnSc4	krize
<g/>
,	,	kIx,	,
během	během	k7c2	během
níž	jenž	k3xRgFnSc2	jenž
jeho	jeho	k3xOp3gMnPc4	jeho
žena	žena	k1gFnSc1	žena
jednou	jednou	k6eAd1	jednou
potratila	potratit	k5eAaPmAgFnS	potratit
a	a	k8xC	a
po	po	k7c6	po
druhém	druhý	k4xOgNnSc6	druhý
rizikovém	rizikový	k2eAgNnSc6d1	rizikové
těhotenství	těhotenství	k1gNnSc6	těhotenství
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
27.	[number]	k4	27.
listopadu	listopad	k1gInSc6	listopad
1957	[number]	k4	1957
narodila	narodit	k5eAaPmAgFnS	narodit
dcera	dcera	k1gFnSc1	dcera
Caroline	Carolin	k1gInSc5	Carolin
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
tehdy	tehdy	k6eAd1	tehdy
vlastně	vlastně	k9	vlastně
štěstí	štěstí	k1gNnSc4	štěstí
<g/>
,	,	kIx,	,
že	že	k8xS	že
nezískal	získat	k5eNaPmAgMnS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
demokratickou	demokratický	k2eAgFnSc4d1	demokratická
nominaci	nominace	k1gFnSc4	nominace
na	na	k7c4	na
úřad	úřad	k1gInSc4	úřad
viceprezidenta	viceprezident	k1gMnSc2	viceprezident
USA	USA	kA	USA
<g/>
,	,	kIx,	,
o	o	k7c4	o
kterou	který	k3yIgFnSc4	který
usiloval	usilovat	k5eAaImAgMnS	usilovat
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
by	by	kYmCp3nS	by
získal	získat	k5eAaPmAgMnS	získat
nálepku	nálepka	k1gFnSc4	nálepka
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
prohrál	prohrát	k5eAaPmAgInS	prohrát
s	s	k7c7	s
Eisenhowerem	Eisenhower	k1gInSc7	Eisenhower
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
zisku	zisk	k1gInSc3	zisk
nominace	nominace	k1gFnSc2	nominace
potřeboval	potřebovat	k5eAaImAgInS	potřebovat
686,5	[number]	k4	686,5
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
získal	získat	k5eAaPmAgMnS	získat
654,5	[number]	k4	654,5
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
sjezdu	sjezd	k1gInSc6	sjezd
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
místo	místo	k6eAd1	místo
něj	on	k3xPp3gInSc4	on
navržen	navržen	k2eAgInSc4d1	navržen
Estés	Estés	k1gInSc4	Estés
Kefauer	Kefauer	k1gMnSc1	Kefauer
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
neúspěšný	úspěšný	k2eNgInSc4d1	neúspěšný
tandem	tandem	k1gInSc4	tandem
s	s	k7c7	s
Adlaiem	Adlaius	k1gMnSc7	Adlaius
Stevensonem	Stevenson	k1gMnSc7	Stevenson
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1960	[number]	k4	1960
získal	získat	k5eAaPmAgInS	získat
těsnou	těsný	k2eAgFnSc4d1	těsná
nadpoloviční	nadpoloviční	k2eAgFnSc4d1	nadpoloviční
většinu	většina	k1gFnSc4	většina
v	v	k7c6	v
prezidentských	prezidentský	k2eAgFnPc6d1	prezidentská
volbách	volba	k1gFnPc6	volba
<g/>
.	.	kIx.	.
</s>
<s>
Nixona	Nixon	k1gMnSc4	Nixon
porazil	porazit	k5eAaPmAgMnS	porazit
o	o	k7c4	o
119 450	[number]	k4	119 450
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
když	když	k8xS	když
získal	získat	k5eAaPmAgMnS	získat
34 227 096	[number]	k4	34 227 096
hlasů	hlas	k1gInPc2	hlas
voličů	volič	k1gMnPc2	volič
<g/>
.	.	kIx.	.
</s>
<s>
Vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
ve	v	k7c6	v
23	[number]	k4	23
státech	stát	k1gInPc6	stát
Unie	unie	k1gFnSc2	unie
a	a	k8xC	a
na	na	k7c4	na
hlasy	hlas	k1gInPc4	hlas
volitelů	volitel	k1gMnPc2	volitel
poměrem	poměr	k1gInSc7	poměr
303	[number]	k4	303
<g/>
:	:	kIx,	:
<g/>
219	[number]	k4	219
<g/>
.	.	kIx.	.
20.	[number]	k4	20.
ledna	leden	k1gInSc2	leden
1961	[number]	k4	1961
se	se	k3xPyFc4	se
oficiálně	oficiálně	k6eAd1	oficiálně
stal	stát	k5eAaPmAgInS	stát
prezidentem	prezident	k1gMnSc7	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
(	(	kIx(	(
<g/>
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
i	i	k9	i
jeho	jeho	k3xOp3gMnSc1	jeho
další	další	k2eAgMnSc1d1	další
syn	syn	k1gMnSc1	syn
John	John	k1gMnSc1	John
junior	junior	k1gMnSc1	junior
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1961	[number]	k4	1961
se	se	k3xPyFc4	se
uprázdnilo	uprázdnit	k5eAaPmAgNnS	uprázdnit
křeslo	křeslo	k1gNnSc1	křeslo
v	v	k7c6	v
Senátu	senát	k1gInSc6	senát
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
Joe	Joe	k1gMnSc2	Joe
P.	P.	kA	P.
Kennedy	Kenneda	k1gMnSc2	Kenneda
chtěl	chtít	k5eAaImAgMnS	chtít
tuto	tento	k3xDgFnSc4	tento
funkci	funkce	k1gFnSc4	funkce
ponechat	ponechat	k5eAaPmF	ponechat
pro	pro	k7c4	pro
nejmladšího	mladý	k2eAgMnSc4d3	nejmladší
z	z	k7c2	z
bratrů	bratr	k1gMnPc2	bratr
Edwarda	Edward	k1gMnSc2	Edward
Moorea	Mooreus	k1gMnSc2	Mooreus
Kennedyho	Kennedy	k1gMnSc2	Kennedy
<g/>
,	,	kIx,	,
kterému	který	k3yRgMnSc3	který
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
chvíli	chvíle	k1gFnSc6	chvíle
ještě	ještě	k6eAd1	ještě
nebylo	být	k5eNaImAgNnS	být
30	[number]	k4	30
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
nar.	nar.	kA	nar.
22.	[number]	k4	22.
<g/>
2.1932	[number]	k4	2.1932
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
nesplňoval	splňovat	k5eNaImAgInS	splňovat
věkovou	věkový	k2eAgFnSc4d1	věková
hranici	hranice	k1gFnSc4	hranice
pro	pro	k7c4	pro
senátora	senátor	k1gMnSc4	senátor
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
hlava	hlava	k1gFnSc1	hlava
rodiny	rodina	k1gFnSc2	rodina
přesvědčila	přesvědčit	k5eAaPmAgFnS	přesvědčit
massachusettského	massachusettský	k2eAgMnSc4d1	massachusettský
guvernéra	guvernér	k1gMnSc4	guvernér
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
do	do	k7c2	do
Senátu	senát	k1gInSc2	senát
dočasně	dočasně	k6eAd1	dočasně
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
rodinného	rodinný	k2eAgMnSc4d1	rodinný
přítele	přítel	k1gMnSc4	přítel
Kennedyů	Kennedy	k1gMnPc2	Kennedy
Benjamina	Benjamin	k1gMnSc4	Benjamin
Smithe	Smith	k1gMnSc4	Smith
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
mandátu	mandát	k1gInSc2	mandát
vzdal	vzdát	k5eAaPmAgMnS	vzdát
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
Edwarda	Edward	k1gMnSc2	Edward
Kennedyho	Kennedy	k1gMnSc2	Kennedy
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
v	v	k7c6	v
řádných	řádný	k2eAgFnPc6d1	řádná
senátních	senátní	k2eAgFnPc6d1	senátní
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
a	a	k8xC	a
znovuzvolen	znovuzvolit	k5eAaPmNgMnS	znovuzvolit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1970	[number]	k4	1970
<g/>
,	,	kIx,	,
1976	[number]	k4	1976
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
a	a	k8xC	a
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
druhým	druhý	k4xOgMnSc7	druhý
nejdéle	dlouho	k6eAd3	dlouho
sloužícím	sloužící	k2eAgMnSc7d1	sloužící
senátorem	senátor	k1gMnSc7	senátor
v	v	k7c6	v
Kongresu	kongres	k1gInSc6	kongres
USA	USA	kA	USA
po	po	k7c6	po
západovirginském	západovirginský	k2eAgMnSc6d1	západovirginský
senátorovi	senátor	k1gMnSc6	senátor
Robertu	Robert	k1gMnSc6	Robert
Byrdovi	Byrda	k1gMnSc6	Byrda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Prezident	prezident	k1gMnSc1	prezident
USA	USA	kA	USA
==	==	k?	==
</s>
</p>
<p>
<s>
Těsný	těsný	k2eAgInSc1d1	těsný
rozdíl	rozdíl	k1gInSc1	rozdíl
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
s	s	k7c7	s
Richardem	Richard	k1gMnSc7	Richard
Nixonem	Nixon	k1gMnSc7	Nixon
napovídal	napovídat	k5eAaBmAgMnS	napovídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gNnSc1	jeho
prezidentství	prezidentství	k1gNnSc1	prezidentství
bude	být	k5eAaImBp3nS	být
nesmírně	smírně	k6eNd1	smírně
obtížné	obtížný	k2eAgNnSc1d1	obtížné
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
v	v	k7c6	v
domácích	domácí	k2eAgInPc6d1	domácí
problémech	problém	k1gInPc6	problém
se	se	k3xPyFc4	se
plně	plně	k6eAd1	plně
nemohl	moct	k5eNaImAgMnS	moct
opřít	opřít	k5eAaPmF	opřít
ani	ani	k8xC	ani
o	o	k7c4	o
Sněmovnu	sněmovna	k1gFnSc4	sněmovna
reprezentantů	reprezentant	k1gMnPc2	reprezentant
a	a	k8xC	a
Senát	senát	k1gInSc1	senát
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
v	v	k7c6	v
nich	on	k3xPp3gMnPc6	on
demokraté	demokrat	k1gMnPc1	demokrat
měli	mít	k5eAaImAgMnP	mít
většinu	většina	k1gFnSc4	většina
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
totiž	totiž	k9	totiž
bylo	být	k5eAaImAgNnS	být
ze	z	k7c2	z
starého	starý	k2eAgInSc2d1	starý
Jihu	jih	k1gInSc2	jih
a	a	k8xC	a
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
řadě	řada	k1gFnSc6	řada
otázek	otázka	k1gFnPc2	otázka
neměli	mít	k5eNaImAgMnP	mít
daleko	daleko	k6eAd1	daleko
k	k	k7c3	k
postojům	postoj	k1gInPc3	postoj
konzervativních	konzervativní	k2eAgMnPc2d1	konzervativní
republikánů	republikán	k1gMnPc2	republikán
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
jim	on	k3xPp3gMnPc3	on
rozhodně	rozhodně	k6eAd1	rozhodně
chyběla	chybět	k5eAaImAgFnS	chybět
snaha	snaha	k1gFnSc1	snaha
provést	provést	k5eAaPmF	provést
potřebné	potřebný	k2eAgFnPc4d1	potřebná
reformy	reforma	k1gFnPc4	reforma
<g/>
.	.	kIx.	.
</s>
<s>
Prvními	první	k4xOgFnPc7	první
zkouškami	zkouška	k1gFnPc7	zkouška
prošel	projít	k5eAaPmAgMnS	projít
již	již	k9	již
při	při	k7c6	při
vytváření	vytváření	k1gNnSc6	vytváření
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
dosazování	dosazování	k1gNnSc1	dosazování
lidí	člověk	k1gMnPc2	člověk
do	do	k7c2	do
administrativy	administrativa	k1gFnSc2	administrativa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
nakonec	nakonec	k6eAd1	nakonec
podařilo	podařit	k5eAaPmAgNnS	podařit
prosadit	prosadit	k5eAaPmF	prosadit
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
ministra	ministr	k1gMnSc2	ministr
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
svého	svůj	k3xOyFgMnSc2	svůj
bratra	bratr	k1gMnSc2	bratr
Roberta	Robert	k1gMnSc2	Robert
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Bobbyho	Bobby	k1gMnSc2	Bobby
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
Kennedyho	Kennedy	k1gMnSc2	Kennedy
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
stranu	strana	k1gFnSc4	strana
postavil	postavit	k5eAaPmAgMnS	postavit
prakticky	prakticky	k6eAd1	prakticky
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
krizových	krizový	k2eAgFnPc6d1	krizová
situacích	situace	k1gFnPc6	situace
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
Po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
svého	svůj	k3xOyFgInSc2	svůj
nedokončeného	dokončený	k2eNgInSc2d1	nedokončený
mandátu	mandát	k1gInSc2	mandát
měl	mít	k5eAaImAgInS	mít
poměrně	poměrně	k6eAd1	poměrně
vysokou	vysoký	k2eAgFnSc4d1	vysoká
a	a	k8xC	a
stabilní	stabilní	k2eAgFnSc4d1	stabilní
důvěru	důvěra	k1gFnSc4	důvěra
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
politiky	politika	k1gFnSc2	politika
<g/>
,	,	kIx,	,
důvěra	důvěra	k1gFnSc1	důvěra
v	v	k7c6	v
domácí	domácí	k2eAgFnSc6d1	domácí
politice	politika	k1gFnSc6	politika
měla	mít	k5eAaImAgFnS	mít
výrazně	výrazně	k6eAd1	výrazně
větší	veliký	k2eAgInPc4d2	veliký
výkyvy	výkyv	k1gInPc4	výkyv
a	a	k8xC	a
nebyla	být	k5eNaImAgFnS	být
tak	tak	k6eAd1	tak
velká	velký	k2eAgFnSc1d1	velká
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
problémem	problém	k1gInSc7	problém
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
čekal	čekat	k5eAaImAgMnS	čekat
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
snaha	snaha	k1gFnSc1	snaha
o	o	k7c4	o
oživení	oživení	k1gNnSc4	oživení
americké	americký	k2eAgFnSc2d1	americká
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
,	,	kIx,	,
s	s	k7c7	s
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
stagnací	stagnace	k1gFnSc7	stagnace
se	se	k3xPyFc4	se
projevil	projevit	k5eAaPmAgInS	projevit
i	i	k9	i
problém	problém	k1gInSc1	problém
poměrně	poměrně	k6eAd1	poměrně
vysoké	vysoký	k2eAgFnSc2d1	vysoká
nezaměstnanosti	nezaměstnanost	k1gFnSc2	nezaměstnanost
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
situaci	situace	k1gFnSc4	situace
řešila	řešit	k5eAaImAgFnS	řešit
jeho	jeho	k3xOp3gFnSc1	jeho
vláda	vláda	k1gFnSc1	vláda
pomocí	pomocí	k7c2	pomocí
daňových	daňový	k2eAgFnPc2d1	daňová
reforem	reforma	k1gFnPc2	reforma
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
dopad	dopad	k1gInSc4	dopad
na	na	k7c4	na
ekonomiku	ekonomika	k1gFnSc4	ekonomika
USA	USA	kA	USA
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
hodnocen	hodnotit	k5eAaImNgMnS	hodnotit
jako	jako	k9	jako
pozitivní	pozitivní	k2eAgMnSc1d1	pozitivní
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
body	bod	k1gInPc1	bod
jeho	jeho	k3xOp3gInSc2	jeho
volebního	volební	k2eAgInSc2d1	volební
programu	program	k1gInSc2	program
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
dařilo	dařit	k5eAaImAgNnS	dařit
prosazovat	prosazovat	k5eAaImF	prosazovat
velmi	velmi	k6eAd1	velmi
obtížně	obtížně	k6eAd1	obtížně
<g/>
,	,	kIx,	,
těmito	tento	k3xDgInPc7	tento
body	bod	k1gInPc7	bod
byla	být	k5eAaImAgFnS	být
především	především	k9	především
nová	nový	k2eAgFnSc1d1	nová
koncepce	koncepce	k1gFnSc1	koncepce
ve	v	k7c6	v
školství	školství	k1gNnSc6	školství
<g/>
,	,	kIx,	,
zdravotnictví	zdravotnictví	k1gNnSc6	zdravotnictví
a	a	k8xC	a
bydlení	bydlení	k1gNnSc6	bydlení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velkým	velký	k2eAgInSc7d1	velký
problémem	problém	k1gInSc7	problém
byla	být	k5eAaImAgFnS	být
otázka	otázka	k1gFnSc1	otázka
občanských	občanský	k2eAgNnPc2d1	občanské
práv	právo	k1gNnPc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Segregace	segregace	k1gFnSc1	segregace
černošského	černošský	k2eAgNnSc2d1	černošské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
na	na	k7c6	na
Jihu	jih	k1gInSc6	jih
se	se	k3xPyFc4	se
projevila	projevit	k5eAaPmAgFnS	projevit
v	v	k7c6	v
několika	několik	k4yIc6	několik
krizích	krize	k1gFnPc6	krize
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pak	pak	k6eAd1	pak
při	při	k7c6	při
zákazech	zákaz	k1gInPc6	zákaz
přístupu	přístup	k1gInSc2	přístup
černošských	černošský	k2eAgMnPc2d1	černošský
studentů	student	k1gMnPc2	student
na	na	k7c4	na
některé	některý	k3yIgFnPc4	některý
jižanské	jižanský	k2eAgFnPc4d1	jižanská
univerzity	univerzita	k1gFnPc4	univerzita
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1960	[number]	k4	1960
v	v	k7c6	v
Alabamě	Alabama	k1gFnSc6	Alabama
<g/>
,	,	kIx,	,
v	v	k7c6	v
září	září	k1gNnSc6	září
1962	[number]	k4	1962
v	v	k7c6	v
Mississippi	Mississippi	k1gFnSc6	Mississippi
nebo	nebo	k8xC	nebo
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1963	[number]	k4	1963
opět	opět	k6eAd1	opět
v	v	k7c6	v
Alabamě	Alabama	k1gFnSc6	Alabama
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Několikrát	několikrát	k6eAd1	několikrát
musely	muset	k5eAaImAgFnP	muset
zasahovat	zasahovat	k5eAaImF	zasahovat
federální	federální	k2eAgFnSc2d1	federální
složky	složka	k1gFnSc2	složka
a	a	k8xC	a
několikrát	několikrát	k6eAd1	několikrát
musel	muset	k5eAaImAgMnS	muset
Robert	Robert	k1gMnSc1	Robert
Kennedy	Kenneda	k1gMnSc2	Kenneda
z	z	k7c2	z
titulu	titul	k1gInSc2	titul
ministra	ministr	k1gMnSc2	ministr
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
silně	silně	k6eAd1	silně
intervenovat	intervenovat	k5eAaImF	intervenovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
situaci	situace	k1gFnSc4	situace
uklidnil	uklidnit	k5eAaPmAgMnS	uklidnit
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
Kennedy	Kenned	k1gMnPc4	Kenned
podporoval	podporovat	k5eAaImAgMnS	podporovat
myšlenku	myšlenka	k1gFnSc4	myšlenka
rovnosti	rovnost	k1gFnSc2	rovnost
všech	všecek	k3xTgMnPc2	všecek
lidí	člověk	k1gMnPc2	člověk
(	(	kIx(	(
<g/>
podpořil	podpořit	k5eAaPmAgInS	podpořit
například	například	k6eAd1	například
demonstraci	demonstrace	k1gFnSc4	demonstrace
za	za	k7c4	za
občanská	občanský	k2eAgNnPc4d1	občanské
práva	právo	k1gNnPc4	právo
u	u	k7c2	u
Lincolnova	Lincolnův	k2eAgNnSc2d1	Lincolnovo
památníků	památník	k1gInPc2	památník
28.	[number]	k4	28.
srpna	srpen	k1gInSc2	srpen
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Martin	Martin	k1gMnSc1	Martin
Luther	Luthra	k1gFnPc2	Luthra
King	King	k1gMnSc1	King
přednesl	přednést	k5eAaPmAgMnS	přednést
svůj	svůj	k3xOyFgInSc4	svůj
nejslavnější	slavný	k2eAgInSc4d3	nejslavnější
projev	projev	k1gInSc4	projev
I	i	k9	i
have	have	k6eAd1	have
a	a	k8xC	a
dream	dream	k6eAd1	dream
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
kvůli	kvůli	k7c3	kvůli
slabé	slabý	k2eAgFnSc3d1	slabá
pozici	pozice	k1gFnSc3	pozice
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
politického	politický	k2eAgNnSc2d1	politické
rozložení	rozložení	k1gNnSc2	rozložení
sil	síla	k1gFnPc2	síla
prosadit	prosadit	k5eAaPmF	prosadit
jen	jen	k9	jen
minimum	minimum	k1gNnSc4	minimum
nových	nový	k2eAgInPc2d1	nový
zákonů	zákon	k1gInPc2	zákon
o	o	k7c6	o
občanských	občanský	k2eAgNnPc6d1	občanské
právech	právo	k1gNnPc6	právo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
Po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
svého	svůj	k3xOyFgInSc2	svůj
mandátu	mandát	k1gInSc2	mandát
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
tajit	tajit	k5eAaImF	tajit
nejen	nejen	k6eAd1	nejen
své	svůj	k3xOyFgInPc4	svůj
zdravotní	zdravotní	k2eAgInPc4d1	zdravotní
problémy	problém	k1gInPc4	problém
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
své	svůj	k3xOyFgFnPc4	svůj
mimomanželské	mimomanželský	k2eAgFnPc4d1	mimomanželská
aktivity	aktivita	k1gFnPc4	aktivita
k	k	k7c3	k
čemuž	což	k3yRnSc3	což
neváhal	váhat	k5eNaImAgMnS	váhat
využít	využít	k5eAaPmF	využít
i	i	k9	i
svého	svůj	k3xOyFgInSc2	svůj
vlivu	vliv	k1gInSc2	vliv
na	na	k7c4	na
Hooverovu	Hooverův	k2eAgFnSc4d1	Hooverova
FBI	FBI	kA	FBI
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaImIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc1	jeho
žena	žena	k1gFnSc1	žena
byla	být	k5eAaImAgFnS	být
ochotna	ochoten	k2eAgFnSc1d1	ochotna
tento	tento	k3xDgInSc4	tento
jeho	jeho	k3xOp3gInSc4	jeho
povahový	povahový	k2eAgInSc4d1	povahový
rys	rys	k1gInSc4	rys
tiše	tiš	k1gFnSc2	tiš
tolerovat	tolerovat	k5eAaImF	tolerovat
<g/>
,	,	kIx,	,
alespoň	alespoň	k9	alespoň
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
veřejné	veřejný	k2eAgFnSc2d1	veřejná
podpory	podpora	k1gFnSc2	podpora
týče	týkat	k5eAaImIp3nS	týkat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
své	svůj	k3xOyFgNnSc4	svůj
postavení	postavení	k1gNnSc4	postavení
první	první	k4xOgFnSc2	první
dámy	dáma	k1gFnSc2	dáma
si	se	k3xPyFc3	se
nějakou	nějaký	k3yIgFnSc4	nějaký
dobu	doba	k1gFnSc4	doba
zvykala	zvykat	k5eAaImAgFnS	zvykat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
měla	mít	k5eAaImAgFnS	mít
zálibu	záliba	k1gFnSc4	záliba
v	v	k7c6	v
umění	umění	k1gNnSc6	umění
a	a	k8xC	a
kultuře	kultura	k1gFnSc6	kultura
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ji	on	k3xPp3gFnSc4	on
pomohlo	pomoct	k5eAaPmAgNnS	pomoct
se	se	k3xPyFc4	se
adaptovat	adaptovat	k5eAaBmF	adaptovat
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
v	v	k7c6	v
Bílém	bílý	k2eAgInSc6d1	bílý
domě	dům	k1gInSc6	dům
pořádala	pořádat	k5eAaImAgFnS	pořádat
mnoho	mnoho	k4c4	mnoho
večírků	večírek	k1gInPc2	večírek
za	za	k7c2	za
přítomnosti	přítomnost	k1gFnSc2	přítomnost
hollywoodských	hollywoodský	k2eAgFnPc2d1	hollywoodská
celebrit	celebrita	k1gFnPc2	celebrita
a	a	k8xC	a
dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
moderní	moderní	k2eAgInSc4d1	moderní
styl	styl	k1gInSc4	styl
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
u	u	k7c2	u
veřejnosti	veřejnost	k1gFnSc2	veřejnost
(	(	kIx(	(
<g/>
ostatně	ostatně	k6eAd1	ostatně
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
<g/>
)	)	kIx)	)
velmi	velmi	k6eAd1	velmi
oblíbená	oblíbený	k2eAgFnSc1d1	oblíbená
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
měla	mít	k5eAaImAgFnS	mít
mnoho	mnoho	k4c4	mnoho
fanoušků	fanoušek	k1gMnPc2	fanoušek
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
projevovalo	projevovat	k5eAaImAgNnS	projevovat
při	při	k7c6	při
zahraničních	zahraniční	k2eAgFnPc6d1	zahraniční
cestách	cesta	k1gFnPc6	cesta
s	s	k7c7	s
prezidentem	prezident	k1gMnSc7	prezident
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
při	při	k7c6	při
cestě	cesta	k1gFnSc6	cesta
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
velkou	velký	k2eAgFnSc7d1	velká
vřelostí	vřelost	k1gFnSc7	vřelost
Evropanů	Evropan	k1gMnPc2	Evropan
k	k	k7c3	k
prezidentskému	prezidentský	k2eAgInSc3d1	prezidentský
páru	pár	k1gInSc3	pár
<g/>
.	.	kIx.	.
</s>
<s>
J.	J.	kA	J.
F.	F.	kA	F.
Kennedy	Kenneda	k1gMnSc2	Kenneda
byl	být	k5eAaImAgMnS	být
oblíben	oblíbit	k5eAaPmNgMnS	oblíbit
u	u	k7c2	u
veřejnosti	veřejnost	k1gFnSc2	veřejnost
i	i	k8xC	i
u	u	k7c2	u
novinářů	novinář	k1gMnPc2	novinář
také	také	k9	také
díky	díky	k7c3	díky
svému	svůj	k3xOyFgInSc3	svůj
pozitivnímu	pozitivní	k2eAgInSc3d1	pozitivní
přístupu	přístup	k1gInSc3	přístup
k	k	k7c3	k
nim	on	k3xPp3gMnPc3	on
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
svého	svůj	k3xOyFgInSc2	svůj
pověstného	pověstný	k2eAgInSc2d1	pověstný
humoru	humor	k1gInSc2	humor
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
první	první	k4xOgMnSc1	první
začal	začít	k5eAaPmAgMnS	začít
pořádat	pořádat	k5eAaImF	pořádat
tiskové	tiskový	k2eAgFnSc2d1	tisková
konference	konference	k1gFnSc2	konference
prezidenta	prezident	k1gMnSc2	prezident
v	v	k7c6	v
přímém	přímý	k2eAgInSc6d1	přímý
přenosu	přenos	k1gInSc6	přenos
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
mu	on	k3xPp3gMnSc3	on
u	u	k7c2	u
občanů	občan	k1gMnPc2	občan
přineslo	přinést	k5eAaPmAgNnS	přinést
velkou	velký	k2eAgFnSc4d1	velká
oblibu	obliba	k1gFnSc4	obliba
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
ptali	ptat	k5eAaImAgMnP	ptat
na	na	k7c4	na
jeho	jeho	k3xOp3gInPc4	jeho
pocity	pocit	k1gInPc4	pocit
z	z	k7c2	z
prezidentování	prezidentování	k1gNnSc2	prezidentování
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
místo	místo	k7c2	místo
velkých	velký	k2eAgNnPc2d1	velké
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
rád	rád	k6eAd1	rád
používal	používat	k5eAaImAgMnS	používat
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
projevech	projev	k1gInPc6	projev
<g/>
,	,	kIx,	,
zavtipkoval	zavtipkovat	k5eAaPmAgMnS	zavtipkovat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Mám	mít	k5eAaImIp1nS	mít
krásný	krásný	k2eAgInSc1d1	krásný
dům	dům	k1gInSc1	dům
<g/>
,	,	kIx,	,
do	do	k7c2	do
kanceláře	kancelář	k1gFnSc2	kancelář
to	ten	k3xDgNnSc1	ten
není	být	k5eNaImIp3nS	být
daleko	daleko	k6eAd1	daleko
<g/>
,	,	kIx,	,
plat	plat	k1gInSc1	plat
je	být	k5eAaImIp3nS	být
dobrý	dobrý	k2eAgInSc1d1	dobrý
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Tento	tento	k3xDgInSc1	tento
příklad	příklad	k1gInSc1	příklad
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgMnPc2d1	další
<g/>
,	,	kIx,	,
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Kennedy	Kenned	k1gMnPc4	Kenned
měl	mít	k5eAaImAgInS	mít
cit	cit	k1gInSc1	cit
pro	pro	k7c4	pro
práci	práce	k1gFnSc4	práce
s	s	k7c7	s
veřejným	veřejný	k2eAgNnSc7d1	veřejné
míněním	mínění	k1gNnSc7	mínění
a	a	k8xC	a
uměl	umět	k5eAaImAgMnS	umět
ho	on	k3xPp3gMnSc4	on
využívat	využívat	k5eAaPmF	využívat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
Zahraniční	zahraniční	k2eAgFnSc3d1	zahraniční
politice	politika	k1gFnSc3	politika
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
se	se	k3xPyFc4	se
cítil	cítit	k5eAaImAgMnS	cítit
silnější	silný	k2eAgMnSc1d2	silnější
<g/>
,	,	kIx,	,
dominovalo	dominovat	k5eAaImAgNnS	dominovat
soupeření	soupeření	k1gNnSc1	soupeření
se	s	k7c7	s
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Studená	studený	k2eAgFnSc1d1	studená
válka	válka	k1gFnSc1	válka
a	a	k8xC	a
celkové	celkový	k2eAgNnSc1d1	celkové
rozložení	rozložení	k1gNnSc1	rozložení
sil	síla	k1gFnPc2	síla
dvou	dva	k4xCgFnPc2	dva
velmocí	velmoc	k1gFnPc2	velmoc
mělo	mít	k5eAaImAgNnS	mít
ovšem	ovšem	k9	ovšem
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
ostatní	ostatní	k2eAgInPc1d1	ostatní
problémy	problém	k1gInPc1	problém
(	(	kIx(	(
<g/>
politika	politika	k1gFnSc1	politika
v	v	k7c6	v
Latinské	latinský	k2eAgFnSc6d1	Latinská
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
politika	politika	k1gFnSc1	politika
vůči	vůči	k7c3	vůči
Kubě	Kuba	k1gFnSc3	Kuba
<g/>
,	,	kIx,	,
otázky	otázka	k1gFnPc1	otázka
Západního	západní	k2eAgInSc2d1	západní
Berlína	Berlín	k1gInSc2	Berlín
<g/>
,	,	kIx,	,
Laosu	Laos	k1gInSc2	Laos
<g/>
,	,	kIx,	,
Vietnamu	Vietnam	k1gInSc2	Vietnam
i	i	k8xC	i
spolupráce	spolupráce	k1gFnSc2	spolupráce
se	s	k7c7	s
západoevropskými	západoevropský	k2eAgMnPc7d1	západoevropský
spojenci	spojenec	k1gMnPc7	spojenec
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgFnP	být
do	do	k7c2	do
velké	velký	k2eAgFnSc2d1	velká
míry	míra	k1gFnSc2	míra
vždy	vždy	k6eAd1	vždy
otázkami	otázka	k1gFnPc7	otázka
soupeření	soupeření	k1gNnSc2	soupeření
s	s	k7c7	s
vlivem	vliv	k1gInSc7	vliv
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
a	a	k8xC	a
komunismu	komunismus	k1gInSc2	komunismus
jako	jako	k8xS	jako
takového	takový	k3xDgNnSc2	takový
<g/>
.	.	kIx.	.
</s>
<s>
JFK	JFK	kA	JFK
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
odpůrcem	odpůrce	k1gMnSc7	odpůrce
zbrojení	zbrojení	k1gNnSc2	zbrojení
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
neústupnost	neústupnost	k1gFnSc4	neústupnost
N.	N.	kA	N.
S.	S.	kA	S.
Chruščova	Chruščův	k2eAgInSc2d1	Chruščův
času	čas	k1gInSc2	čas
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
nutnosti	nutnost	k1gFnSc3	nutnost
zvyšovat	zvyšovat	k5eAaImF	zvyšovat
výdaje	výdaj	k1gInPc4	výdaj
na	na	k7c4	na
obranu	obrana	k1gFnSc4	obrana
<g/>
.	.	kIx.	.
</s>
<s>
Velkým	velký	k2eAgInSc7d1	velký
neúspěchem	neúspěch	k1gInSc7	neúspěch
jeho	jeho	k3xOp3gFnSc2	jeho
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
politiky	politika	k1gFnSc2	politika
byl	být	k5eAaImAgInS	být
pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
svržení	svržení	k1gNnSc4	svržení
Castrova	Castrův	k2eAgInSc2d1	Castrův
režimu	režim	k1gInSc2	režim
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
kubánských	kubánský	k2eAgMnPc2d1	kubánský
emigrantů	emigrant	k1gMnPc2	emigrant
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1961	[number]	k4	1961
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
invaze	invaze	k1gFnSc2	invaze
v	v	k7c6	v
Zátoce	zátoka	k1gFnSc6	zátoka
sviní	svině	k1gFnPc2	svině
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
naprosto	naprosto	k6eAd1	naprosto
nedokonalé	dokonalý	k2eNgFnSc3d1	nedokonalá
přípravě	příprava	k1gFnSc3	příprava
a	a	k8xC	a
špatnému	špatný	k2eAgInSc3d1	špatný
odhadu	odhad	k1gInSc3	odhad
politických	politický	k2eAgFnPc2d1	politická
<g/>
,	,	kIx,	,
vojenských	vojenský	k2eAgFnPc2d1	vojenská
i	i	k8xC	i
špionážních	špionážní	k2eAgFnPc2d1	špionážní
složek	složka	k1gFnPc2	složka
<g/>
,	,	kIx,	,
akce	akce	k1gFnSc1	akce
selhala	selhat	k5eAaPmAgFnS	selhat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dalším	další	k2eAgInSc7d1	další
problémem	problém	k1gInSc7	problém
bylo	být	k5eAaImAgNnS	být
jednání	jednání	k1gNnSc1	jednání
se	s	k7c7	s
sovětským	sovětský	k2eAgMnSc7d1	sovětský
vůdcem	vůdce	k1gMnSc7	vůdce
Nikitou	Nikita	k1gMnSc7	Nikita
Chruščovem	Chruščov	k1gInSc7	Chruščov
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
schůzce	schůzka	k1gFnSc6	schůzka
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1961	[number]	k4	1961
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
zklamán	zklamat	k5eAaPmNgMnS	zklamat
jeho	jeho	k3xOp3gMnSc7	jeho
neústupným	ústupný	k2eNgMnSc7d1	neústupný
a	a	k8xC	a
mnohdy	mnohdy	k6eAd1	mnohdy
otevřeně	otevřeně	k6eAd1	otevřeně
nepřátelským	přátelský	k2eNgInSc7d1	nepřátelský
postojem	postoj	k1gInSc7	postoj
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pak	pak	k6eAd1	pak
v	v	k7c6	v
otázce	otázka	k1gFnSc6	otázka
všeobecného	všeobecný	k2eAgNnSc2d1	všeobecné
odzbrojování	odzbrojování	k1gNnSc2	odzbrojování
a	a	k8xC	a
v	v	k7c6	v
otázce	otázka	k1gFnSc6	otázka
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
Západního	západní	k2eAgInSc2d1	západní
Berlína	Berlín	k1gInSc2	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
projevilo	projevit	k5eAaPmAgNnS	projevit
při	při	k7c6	při
první	první	k4xOgFnSc6	první
velké	velký	k2eAgFnSc6d1	velká
krizi	krize	k1gFnSc6	krize
<g/>
,	,	kIx,	,
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
a	a	k8xC	a
srpnu	srpen	k1gInSc6	srpen
1961	[number]	k4	1961
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
postavení	postavení	k1gNnSc3	postavení
tzv.	tzv.	kA	tzv.
berlínské	berlínský	k2eAgFnSc2d1	Berlínská
zdi	zeď	k1gFnSc2	zeď
<g/>
.	.	kIx.	.
</s>
<s>
Kennedy	Kenned	k1gMnPc4	Kenned
dal	dát	k5eAaPmAgInS	dát
ovšem	ovšem	k9	ovšem
již	již	k6eAd1	již
při	při	k7c6	při
televizním	televizní	k2eAgInSc6d1	televizní
projevu	projev	k1gInSc6	projev
25.	[number]	k4	25.
července	červenec	k1gInSc2	červenec
1961	[number]	k4	1961
Moskvě	Moskva	k1gFnSc6	Moskva
striktně	striktně	k6eAd1	striktně
najevo	najevo	k6eAd1	najevo
že	že	k8xS	že
svobodu	svoboda	k1gFnSc4	svoboda
Západního	západní	k2eAgInSc2d1	západní
Berlína	Berlín	k1gInSc2	Berlín
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
připraven	připravit	k5eAaPmNgMnS	připravit
bránit	bránit	k5eAaImF	bránit
i	i	k9	i
vojensky	vojensky	k6eAd1	vojensky
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc7	druhý
a	a	k8xC	a
ještě	ještě	k6eAd1	ještě
závažnější	závažný	k2eAgFnSc1d2	závažnější
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
tzv.	tzv.	kA	tzv.
karibská	karibský	k2eAgFnSc1d1	karibská
krize	krize	k1gFnSc1	krize
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
a	a	k8xC	a
červnu	červen	k1gInSc6	červen
1962	[number]	k4	1962
se	se	k3xPyFc4	se
Sověti	Sovět	k1gMnPc1	Sovět
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c6	na
rozmístění	rozmístění	k1gNnSc6	rozmístění
jaderných	jaderný	k2eAgFnPc2d1	jaderná
střel	střela	k1gFnPc2	střela
v	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
umístit	umístit	k5eAaPmF	umístit
na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
rakety	raketa	k1gFnPc4	raketa
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
by	by	kYmCp3nP	by
jim	on	k3xPp3gMnPc3	on
umožňovaly	umožňovat	k5eAaImAgInP	umožňovat
útok	útok	k1gInSc4	útok
na	na	k7c4	na
východní	východní	k2eAgNnSc4d1	východní
pobřeží	pobřeží	k1gNnSc4	pobřeží
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
co	co	k9	co
zpravodajské	zpravodajský	k2eAgFnPc1d1	zpravodajská
služby	služba	k1gFnPc1	služba
zjistily	zjistit	k5eAaPmAgFnP	zjistit
budování	budování	k1gNnSc4	budování
základen	základna	k1gFnPc2	základna
<g/>
,	,	kIx,	,
rozmísťování	rozmísťování	k1gNnSc4	rozmísťování
těchto	tento	k3xDgFnPc2	tento
raket	raketa	k1gFnPc2	raketa
a	a	k8xC	a
současně	současně	k6eAd1	současně
jejich	jejich	k3xOp3gFnSc4	jejich
dopravu	doprava	k1gFnSc4	doprava
sovětskými	sovětský	k2eAgFnPc7d1	sovětská
loděmi	loď	k1gFnPc7	loď
na	na	k7c4	na
Kubu	Kuba	k1gFnSc4	Kuba
<g/>
,	,	kIx,	,
stál	stát	k5eAaImAgMnS	stát
Kennedy	Kenneda	k1gMnSc2	Kenneda
před	před	k7c7	před
otázkou	otázka	k1gFnSc7	otázka
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
tuto	tento	k3xDgFnSc4	tento
krizi	krize	k1gFnSc4	krize
vyřešit	vyřešit	k5eAaPmF	vyřešit
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
tlak	tlak	k1gInSc1	tlak
zejména	zejména	k9	zejména
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
armádních	armádní	k2eAgMnPc2d1	armádní
kruhů	kruh	k1gInPc2	kruh
na	na	k7c4	na
přímý	přímý	k2eAgInSc4d1	přímý
zásah	zásah	k1gInSc4	zásah
<g/>
,	,	kIx,	,
bombardování	bombardování	k1gNnSc4	bombardování
<g/>
,	,	kIx,	,
invazi	invaze	k1gFnSc4	invaze
či	či	k8xC	či
jiný	jiný	k2eAgInSc4d1	jiný
vojenský	vojenský	k2eAgInSc4d1	vojenský
způsob	způsob	k1gInSc4	způsob
konfliktu	konflikt	k1gInSc2	konflikt
byl	být	k5eAaImAgInS	být
poměrně	poměrně	k6eAd1	poměrně
silný	silný	k2eAgInSc1d1	silný
<g/>
,	,	kIx,	,
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
se	se	k3xPyFc4	se
Kennedy	Kenned	k1gMnPc7	Kenned
nakonec	nakonec	k6eAd1	nakonec
tváří	tvářet	k5eAaImIp3nS	tvářet
v	v	k7c4	v
tvář	tvář	k1gFnSc4	tvář
hrozbě	hrozba	k1gFnSc3	hrozba
nukleární	nukleární	k2eAgFnSc2d1	nukleární
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
zahájit	zahájit	k5eAaPmF	zahájit
námořní	námořní	k2eAgFnSc4d1	námořní
blokádu	blokáda	k1gFnSc4	blokáda
Kuby	Kuba	k1gFnSc2	Kuba
a	a	k8xC	a
vyzvat	vyzvat	k5eAaPmF	vyzvat
Sověty	Sovět	k1gMnPc7	Sovět
veřejně	veřejně	k6eAd1	veřejně
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
ustoupili	ustoupit	k5eAaPmAgMnP	ustoupit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
televizním	televizní	k2eAgInSc6d1	televizní
projevu	projev	k1gInSc6	projev
22.	[number]	k4	22.
října	říjen	k1gInSc2	říjen
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
sledovalo	sledovat	k5eAaImAgNnS	sledovat
100	[number]	k4	100
miliónů	milión	k4xCgInPc2	milión
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
varoval	varovat	k5eAaImAgMnS	varovat
Chruščova	Chruščův	k2eAgMnSc4d1	Chruščův
<g/>
,	,	kIx,	,
před	před	k7c7	před
uvrhnutím	uvrhnutí	k1gNnSc7	uvrhnutí
světa	svět	k1gInSc2	svět
do	do	k7c2	do
války	válka	k1gFnSc2	válka
a	a	k8xC	a
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
nemohou	moct	k5eNaImIp3nP	moct
takovéto	takovýto	k3xDgFnPc4	takovýto
ohrožení	ohrožení	k1gNnSc1	ohrožení
své	svůj	k3xOyFgFnSc2	svůj
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
tolerovat	tolerovat	k5eAaImF	tolerovat
<g/>
.	.	kIx.	.
</s>
<s>
Usvědčil	usvědčit	k5eAaPmAgMnS	usvědčit
Sověty	Sovět	k1gMnPc4	Sovět
ze	z	k7c2	z
lživého	lživý	k2eAgNnSc2d1	lživé
tvrzení	tvrzení	k1gNnSc2	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
Kubu	Kuba	k1gFnSc4	Kuba
jsou	být	k5eAaImIp3nP	být
dopravovány	dopravován	k2eAgInPc1d1	dopravován
pouze	pouze	k6eAd1	pouze
obranné	obranný	k2eAgInPc4d1	obranný
prostředky	prostředek	k1gInPc4	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Sověti	Sovět	k1gMnPc1	Sovět
evidentně	evidentně	k6eAd1	evidentně
s	s	k7c7	s
takovou	takový	k3xDgFnSc7	takový
reakcí	reakce	k1gFnSc7	reakce
nepočítali	počítat	k5eNaImAgMnP	počítat
a	a	k8xC	a
byli	být	k5eAaImAgMnP	být
zaskočeni	zaskočit	k5eAaPmNgMnP	zaskočit
<g/>
,	,	kIx,	,
28.	[number]	k4	28.
října	říjen	k1gInSc2	říjen
po	po	k7c6	po
diplomatických	diplomatický	k2eAgNnPc6d1	diplomatické
jednáních	jednání	k1gNnPc6	jednání
své	svůj	k3xOyFgNnSc1	svůj
rakety	raketa	k1gFnPc4	raketa
z	z	k7c2	z
Kuby	Kuba	k1gFnSc2	Kuba
stáhli	stáhnout	k5eAaPmAgMnP	stáhnout
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c4	za
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
záruku	záruka	k1gFnSc4	záruka
USA	USA	kA	USA
<g/>
,	,	kIx,	,
že	že	k8xS	že
neprovedou	provést	k5eNaPmIp3nP	provést
invazi	invaze	k1gFnSc4	invaze
na	na	k7c4	na
Kubu	Kuba	k1gFnSc4	Kuba
a	a	k8xC	a
za	za	k7c4	za
skryté	skrytý	k2eAgNnSc4d1	skryté
ujednání	ujednání	k1gNnSc4	ujednání
<g/>
,	,	kIx,	,
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
něhož	jenž	k3xRgInSc2	jenž
USA	USA	kA	USA
stáhly	stáhnout	k5eAaPmAgInP	stáhnout
jaderné	jaderný	k2eAgInPc1d1	jaderný
střely	střel	k1gInPc1	střel
ze	z	k7c2	z
základen	základna	k1gFnPc2	základna
v	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
</s>
</p>
<p>
</p>
<p>
<s>
Po	po	k7c6	po
této	tento	k3xDgFnSc6	tento
Karibské	karibský	k2eAgFnSc6d1	karibská
krizi	krize	k1gFnSc6	krize
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
snaze	snaha	k1gFnSc6	snaha
obou	dva	k4xCgFnPc2	dva
stran	strana	k1gFnPc2	strana
k	k	k7c3	k
jednání	jednání	k1gNnSc3	jednání
o	o	k7c6	o
určité	určitý	k2eAgFnSc6d1	určitá
formě	forma	k1gFnSc6	forma
odzbrojení	odzbrojení	k1gNnSc2	odzbrojení
<g/>
,	,	kIx,	,
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
směru	směr	k1gInSc6	směr
velmi	velmi	k6eAd1	velmi
aktivní	aktivní	k2eAgMnSc1d1	aktivní
a	a	k8xC	a
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1963	[number]	k4	1963
pronesl	pronést	k5eAaPmAgInS	pronést
slavný	slavný	k2eAgInSc4d1	slavný
tzv.	tzv.	kA	tzv.
mírový	mírový	k2eAgInSc4d1	mírový
projev	projev	k1gInSc4	projev
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
požadoval	požadovat	k5eAaImAgMnS	požadovat
mír	mír	k1gInSc4	mír
pro	pro	k7c4	pro
muže	muž	k1gMnPc4	muž
a	a	k8xC	a
ženy	žena	k1gFnPc4	žena
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
mír	mír	k1gInSc1	mír
nejen	nejen	k6eAd1	nejen
pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
jednou	jeden	k4xCgFnSc7	jeden
provždy	provždy	k6eAd1	provždy
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
z	z	k7c2	z
vůle	vůle	k1gFnSc2	vůle
všech	všecek	k3xTgMnPc2	všecek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
by	by	kYmCp3nS	by
nebyl	být	k5eNaImAgInS	být
nikomu	nikdo	k3yNnSc3	nikdo
vnucován	vnucovat	k5eAaImNgMnS	vnucovat
zbraněmi	zbraň	k1gFnPc7	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
léta	léto	k1gNnSc2	léto
1963	[number]	k4	1963
po	po	k7c6	po
mnoha	mnoho	k4c6	mnoho
jednáních	jednání	k1gNnPc6	jednání
pak	pak	k6eAd1	pak
Sovětský	sovětský	k2eAgInSc4d1	sovětský
svaz	svaz	k1gInSc4	svaz
vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
souhlas	souhlas	k1gInSc4	souhlas
s	s	k7c7	s
první	první	k4xOgFnSc7	první
smlouvou	smlouva	k1gFnSc7	smlouva
o	o	k7c6	o
zákazu	zákaz	k1gInSc6	zákaz
zkoušek	zkouška	k1gFnPc2	zkouška
jaderných	jaderný	k2eAgFnPc2d1	jaderná
zbraní	zbraň	k1gFnPc2	zbraň
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgFnPc7	dva
velmocemi	velmoc	k1gFnPc7	velmoc
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
americko-sovětského	americkoovětský	k2eAgInSc2d1	americko-sovětský
souboje	souboj	k1gInSc2	souboj
byl	být	k5eAaImAgInS	být
také	také	k9	také
souboj	souboj	k1gInSc1	souboj
v	v	k7c6	v
dobývání	dobývání	k1gNnSc6	dobývání
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
sovětských	sovětský	k2eAgInPc6d1	sovětský
úspěších	úspěch	k1gInPc6	úspěch
byl	být	k5eAaImAgInS	být
nucen	nucen	k2eAgInSc1d1	nucen
reagovat	reagovat	k5eAaBmF	reagovat
a	a	k8xC	a
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1961	[number]	k4	1961
představil	představit	k5eAaPmAgMnS	představit
ve	v	k7c6	v
zvláštním	zvláštní	k2eAgNnSc6d1	zvláštní
poselství	poselství	k1gNnSc6	poselství
v	v	k7c6	v
Kongresu	kongres	k1gInSc6	kongres
<g/>
,	,	kIx,	,
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
smělý	smělý	k2eAgInSc4d1	smělý
a	a	k8xC	a
neuvěřitelný	uvěřitelný	k2eNgInSc4d1	neuvěřitelný
plán	plán	k1gInSc4	plán
<g/>
,	,	kIx,	,
do	do	k7c2	do
konce	konec	k1gInSc2	konec
desetiletí	desetiletí	k1gNnSc2	desetiletí
poslat	poslat	k5eAaPmF	poslat
lidskou	lidský	k2eAgFnSc4d1	lidská
posádku	posádka	k1gFnSc4	posádka
na	na	k7c4	na
Měsíc	měsíc	k1gInSc4	měsíc
a	a	k8xC	a
zajistit	zajistit	k5eAaPmF	zajistit
její	její	k3xOp3gInSc4	její
bezpečný	bezpečný	k2eAgInSc4d1	bezpečný
návrat	návrat	k1gInSc4	návrat
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
de-facto	deacto	k1gNnSc4	de-facto
odstartoval	odstartovat	k5eAaPmAgMnS	odstartovat
program	program	k1gInSc4	program
Apollo	Apollo	k1gMnSc1	Apollo
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgInS	být
završen	završit	k5eAaPmNgInS	završit
20.	[number]	k4	20.
července	červenec	k1gInSc2	červenec
1969	[number]	k4	1969
první	první	k4xOgInSc1	první
krokem	krok	k1gInSc7	krok
člověka	člověk	k1gMnSc2	člověk
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
(	(	kIx(	(
<g/>
Neil	Neil	k1gMnSc1	Neil
Armstrong	Armstrong	k1gMnSc1	Armstrong
při	při	k7c6	při
letu	let	k1gInSc6	let
Apolla	Apollo	k1gNnSc2	Apollo
11	[number]	k4	11
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
začal	začít	k5eAaPmAgInS	začít
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
přípravě	příprava	k1gFnSc6	příprava
souboje	souboj	k1gInSc2	souboj
o	o	k7c4	o
své	svůj	k3xOyFgNnSc4	svůj
druhé	druhý	k4xOgNnSc4	druhý
volební	volební	k2eAgNnSc4d1	volební
období	období	k1gNnSc4	období
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
některé	některý	k3yIgInPc4	některý
neúspěchy	neúspěch	k1gInPc4	neúspěch
cítil	cítit	k5eAaImAgMnS	cítit
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
velkou	velký	k2eAgFnSc4d1	velká
šanci	šance	k1gFnSc4	šance
uspět	uspět	k5eAaPmF	uspět
i	i	k9	i
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
volbách	volba	k1gFnPc6	volba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1963	[number]	k4	1963
jeho	jeho	k3xOp3gNnSc1	jeho
další	další	k2eAgInPc4d1	další
plány	plán	k1gInPc4	plán
tragicky	tragicky	k6eAd1	tragicky
přerušil	přerušit	k5eAaPmAgMnS	přerušit
atentát	atentát	k1gInSc4	atentát
při	při	k7c6	při
jeho	jeho	k3xOp3gFnSc6	jeho
cestě	cesta	k1gFnSc6	cesta
Dallasem	Dallas	k1gInSc7	Dallas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Osobní	osobní	k2eAgInSc4d1	osobní
život	život	k1gInSc4	život
==	==	k?	==
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
mladého	mladý	k2eAgMnSc4d1	mladý
a	a	k8xC	a
charismatického	charismatický	k2eAgMnSc4d1	charismatický
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
reprezentativní	reprezentativní	k2eAgInSc1d1	reprezentativní
vzhled	vzhled	k1gInSc1	vzhled
a	a	k8xC	a
šarm	šarm	k1gInSc1	šarm
působil	působit	k5eAaImAgInS	působit
na	na	k7c4	na
velké	velký	k2eAgNnSc4d1	velké
procento	procento	k1gNnSc4	procento
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
okouzlující	okouzlující	k2eAgInSc1d1	okouzlující
<g/>
,	,	kIx,	,
inteligentní	inteligentní	k2eAgInSc1d1	inteligentní
a	a	k8xC	a
žádaný	žádaný	k2eAgInSc1d1	žádaný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
soukromém	soukromý	k2eAgInSc6d1	soukromý
životě	život	k1gInSc6	život
byl	být	k5eAaImAgInS	být
především	především	k6eAd1	především
nemocný	mocný	k2eNgMnSc1d1	nemocný
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
nepolepšitelný	polepšitelný	k2eNgMnSc1d1	nepolepšitelný
sukničkář	sukničkář	k1gMnSc1	sukničkář
<g/>
,	,	kIx,	,
ohleduplný	ohleduplný	k2eAgMnSc1d1	ohleduplný
manžel	manžel	k1gMnSc1	manžel
a	a	k8xC	a
oddaný	oddaný	k2eAgMnSc1d1	oddaný
syn	syn	k1gMnSc1	syn
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jeho	on	k3xPp3gNnSc2	on
chování	chování	k1gNnSc2	chování
se	se	k3xPyFc4	se
usuzuje	usuzovat	k5eAaImIp3nS	usuzovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
psychopat	psychopat	k1gMnSc1	psychopat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
obdivoval	obdivovat	k5eAaImAgMnS	obdivovat
Adolfa	Adolf	k1gMnSc4	Adolf
Hitlera	Hitler	k1gMnSc4	Hitler
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
také	také	k9	také
charismatický	charismatický	k2eAgMnSc1d1	charismatický
psychopat	psychopat	k1gMnSc1	psychopat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
budoucí	budoucí	k2eAgFnSc4d1	budoucí
manželku	manželka	k1gFnSc4	manželka
Jacqueline	Jacquelin	k1gInSc5	Jacquelin
Bouvierovou	Bouvierová	k1gFnSc4	Bouvierová
poznal	poznat	k5eAaPmAgMnS	poznat
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1951	[number]	k4	1951
na	na	k7c6	na
večeři	večeře	k1gFnSc6	večeře
u	u	k7c2	u
manželů	manžel	k1gMnPc2	manžel
Bartlettových	Bartlettových	k2eAgMnPc1d1	Bartlettových
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
navzájem	navzájem	k6eAd1	navzájem
zapůsobili	zapůsobit	k5eAaPmAgMnP	zapůsobit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rozešli	rozejít	k5eAaPmAgMnP	rozejít
se	se	k3xPyFc4	se
bez	bez	k7c2	bez
dalšího	další	k2eAgInSc2d1	další
významnějšího	významný	k2eAgInSc2d2	významnější
zájmu	zájem	k1gInSc2	zájem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
také	také	k9	také
intimněji	intimně	k6eAd2	intimně
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
Audrey	Audre	k1gMnPc7	Audre
Hepburnovou	Hepburnová	k1gFnSc4	Hepburnová
a	a	k8xC	a
o	o	k7c4	o
něco	něco	k3yInSc4	něco
později	pozdě	k6eAd2	pozdě
s	s	k7c7	s
Marilyn	Marilyn	k1gFnSc7	Marilyn
Monroe	Monro	k1gInSc2	Monro
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Jackie	Jackie	k1gFnPc4	Jackie
se	se	k3xPyFc4	se
zasnoubil	zasnoubit	k5eAaPmAgInS	zasnoubit
až	až	k6eAd1	až
23.	[number]	k4	23.
června	červen	k1gInSc2	červen
1953	[number]	k4	1953
a	a	k8xC	a
12.	[number]	k4	12.
září	září	k1gNnSc2	září
se	se	k3xPyFc4	se
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
v	v	k7c6	v
Newportu	Newport	k1gInSc6	Newport
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Rhode	Rhodos	k1gInSc5	Rhodos
Island	Island	k1gInSc4	Island
vzali	vzít	k5eAaPmAgMnP	vzít
<g/>
.	.	kIx.	.
</s>
<s>
Jackie	Jackie	k1gFnSc1	Jackie
byla	být	k5eAaImAgFnS	být
zajímavá	zajímavý	k2eAgFnSc1d1	zajímavá
žena	žena	k1gFnSc1	žena
<g/>
.	.	kIx.	.
</s>
<s>
Vysoce	vysoce	k6eAd1	vysoce
inteligentní	inteligentní	k2eAgNnPc1d1	inteligentní
<g/>
,	,	kIx,	,
okouzlující	okouzlující	k2eAgNnPc1d1	okouzlující
<g/>
,	,	kIx,	,
vtipná	vtipný	k2eAgNnPc1d1	vtipné
a	a	k8xC	a
reprezentativně	reprezentativně	k6eAd1	reprezentativně
působící	působící	k2eAgInPc1d1	působící
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
veřejnosti	veřejnost	k1gFnSc6	veřejnost
projevovala	projevovat	k5eAaImAgFnS	projevovat
své	svůj	k3xOyFgFnPc4	svůj
emoce	emoce	k1gFnPc4	emoce
minimálně	minimálně	k6eAd1	minimálně
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
dokonalá	dokonalý	k2eAgFnSc1d1	dokonalá
herečka	herečka	k1gFnSc1	herečka
a	a	k8xC	a
dokázala	dokázat	k5eAaPmAgFnS	dokázat
se	se	k3xPyFc4	se
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
její	její	k3xOp3gMnSc1	její
manžel	manžel	k1gMnSc1	manžel
až	až	k9	až
nadlidsky	nadlidsky	k6eAd1	nadlidsky
ovládat	ovládat	k5eAaImF	ovládat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rodinném	rodinný	k2eAgNnSc6d1	rodinné
prostředí	prostředí	k1gNnSc6	prostředí
však	však	k9	však
byla	být	k5eAaImAgFnS	být
schopna	schopen	k2eAgFnSc1d1	schopna
vyvolat	vyvolat	k5eAaPmF	vyvolat
hysterické	hysterický	k2eAgFnSc2d1	hysterická
scény	scéna	k1gFnSc2	scéna
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pokud	pokud	k8xS	pokud
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
bránění	bránění	k1gNnSc4	bránění
soukromí	soukromí	k1gNnSc2	soukromí
jejích	její	k3xOp3gFnPc2	její
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
rodina	rodina	k1gFnSc1	rodina
nastěhovala	nastěhovat	k5eAaPmAgFnS	nastěhovat
do	do	k7c2	do
Bílého	bílý	k2eAgInSc2d1	bílý
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
Jackie	Jackie	k1gFnPc1	Jackie
celé	celý	k2eAgNnSc1d1	celé
zařízení	zařízení	k1gNnSc1	zařízení
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
vkusem	vkus	k1gInSc7	vkus
přestavěla	přestavět	k5eAaPmAgFnS	přestavět
a	a	k8xC	a
vedla	vést	k5eAaImAgFnS	vést
prezidentskou	prezidentský	k2eAgFnSc4d1	prezidentská
domácnost	domácnost	k1gFnSc4	domácnost
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
zodpovědností	zodpovědnost	k1gFnSc7	zodpovědnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
1954	[number]	k4	1954
prodělal	prodělat	k5eAaPmAgMnS	prodělat
složitou	složitý	k2eAgFnSc4d1	složitá
operaci	operace	k1gFnSc4	operace
páteře	páteř	k1gFnSc2	páteř
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ho	on	k3xPp3gMnSc4	on
téměř	téměř	k6eAd1	téměř
stála	stát	k5eAaImAgFnS	stát
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
manželka	manželka	k1gFnSc1	manželka
ho	on	k3xPp3gMnSc4	on
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
velmi	velmi	k6eAd1	velmi
podporovala	podporovat	k5eAaImAgFnS	podporovat
a	a	k8xC	a
starala	starat	k5eAaImAgFnS	starat
se	se	k3xPyFc4	se
o	o	k7c4	o
něj	on	k3xPp3gMnSc4	on
v	v	k7c6	v
době	doba	k1gFnSc6	doba
jeho	jeho	k3xOp3gFnSc2	jeho
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
rekonvalescence	rekonvalescence	k1gFnSc2	rekonvalescence
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
napsal	napsat	k5eAaBmAgMnS	napsat
i	i	k8xC	i
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
opravdovou	opravdový	k2eAgFnSc4d1	opravdová
knihu	kniha	k1gFnSc4	kniha
Profily	profil	k1gInPc4	profil
odvahy	odvaha	k1gFnSc2	odvaha
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yIgFnSc7	který
mu	on	k3xPp3gMnSc3	on
Jackie	Jackie	k1gFnSc1	Jackie
velmi	velmi	k6eAd1	velmi
pomohla	pomoct	k5eAaPmAgFnS	pomoct
a	a	k8xC	a
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
oceněna	ocenit	k5eAaPmNgFnS	ocenit
Pulitzerovou	Pulitzerův	k2eAgFnSc7d1	Pulitzerova
cenou	cena	k1gFnSc7	cena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dodnes	dodnes	k6eAd1	dodnes
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejpopulárnější	populární	k2eAgFnSc4d3	nejpopulárnější
z	z	k7c2	z
amerických	americký	k2eAgMnPc2d1	americký
prezidentů	prezident	k1gMnPc2	prezident
<g/>
,	,	kIx,	,
tuto	tento	k3xDgFnSc4	tento
jeho	jeho	k3xOp3gMnSc3	jeho
stálou	stálý	k2eAgFnSc4d1	stálá
popularitu	popularita	k1gFnSc4	popularita
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
několik	několik	k4yIc4	několik
faktorů	faktor	k1gInPc2	faktor
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimiž	jenž	k3xRgInPc7	jenž
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
předním	přední	k2eAgNnSc6d1	přední
místě	místo	k1gNnSc6	místo
jeho	jeho	k3xOp3gFnSc4	jeho
schopnost	schopnost	k1gFnSc4	schopnost
pracovat	pracovat	k5eAaImF	pracovat
s	s	k7c7	s
médii	médium	k1gNnPc7	médium
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
zásluha	zásluha	k1gFnSc1	zásluha
o	o	k7c4	o
znovu	znovu	k6eAd1	znovu
získané	získaný	k2eAgNnSc1d1	získané
americké	americký	k2eAgNnSc1d1	americké
sebevědomí	sebevědomí	k1gNnSc1	sebevědomí
(	(	kIx(	(
<g/>
odvaha	odvaha	k1gFnSc1	odvaha
v	v	k7c6	v
Karibské	karibský	k2eAgFnSc6d1	karibská
krizi	krize	k1gFnSc6	krize
<g/>
,	,	kIx,	,
zahájení	zahájení	k1gNnSc6	zahájení
projektu	projekt	k1gInSc2	projekt
Apollo	Apollo	k1gMnSc1	Apollo
<g/>
)	)	kIx)	)
a	a	k8xC	a
také	také	k9	také
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
zastřelen	zastřelit	k5eAaPmNgMnS	zastřelit
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
obětí	oběť	k1gFnSc7	oběť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Atentát	atentát	k1gInSc1	atentát
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Atentát	atentát	k1gInSc1	atentát
na	na	k7c4	na
Johna	John	k1gMnSc4	John
Fitzgeralda	Fitzgerald	k1gMnSc2	Fitzgerald
Kennedyho	Kennedy	k1gMnSc2	Kennedy
byl	být	k5eAaImAgInS	být
spáchán	spáchat	k5eAaPmNgInS	spáchat
v	v	k7c4	v
pátek	pátek	k1gInSc4	pátek
22.	[number]	k4	22.
listopadu	listopad	k1gInSc2	listopad
1963	[number]	k4	1963
v	v	k7c6	v
texaském	texaský	k2eAgNnSc6d1	texaské
městě	město	k1gNnSc6	město
Dallas	Dallas	k1gInSc1	Dallas
ve	v	k7c6	v
12.	[number]	k4	12.
<g/>
30	[number]	k4	30
místního	místní	k2eAgInSc2d1	místní
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
John	John	k1gMnSc1	John
F.	F.	kA	F.
Kennedy	Kenned	k1gMnPc4	Kenned
byl	být	k5eAaImAgMnS	být
smrtelně	smrtelně	k6eAd1	smrtelně
raněn	ranit	k5eAaPmNgMnS	ranit
střelou	střela	k1gFnSc7	střela
z	z	k7c2	z
pušky	puška	k1gFnSc2	puška
Carcano	Carcana	k1gFnSc5	Carcana
M91	M91	k1gMnSc6	M91
<g/>
/	/	kIx~	/
<g/>
38	[number]	k4	38
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
kampaně	kampaň	k1gFnSc2	kampaň
pro	pro	k7c4	pro
prezidentské	prezidentský	k2eAgFnPc4d1	prezidentská
volby	volba	k1gFnPc4	volba
1964	[number]	k4	1964
projížděl	projíždět	k5eAaImAgInS	projíždět
ulicemi	ulice	k1gFnPc7	ulice
Dallasu	Dallas	k1gInSc6	Dallas
v	v	k7c6	v
otevřeném	otevřený	k2eAgInSc6d1	otevřený
voze	vůz	k1gInSc6	vůz
doprovázený	doprovázený	k2eAgInSc1d1	doprovázený
manželkou	manželka	k1gFnSc7	manželka
Jacqueline	Jacquelin	k1gInSc5	Jacquelin
<g/>
,	,	kIx,	,
texaským	texaský	k2eAgMnSc7d1	texaský
guvernérem	guvernér	k1gMnSc7	guvernér
Johnem	John	k1gMnSc7	John
Connallym	Connallym	k1gInSc4	Connallym
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc7	jeho
manželkou	manželka	k1gFnSc7	manželka
Nellie	Nellie	k1gFnSc2	Nellie
<g/>
.	.	kIx.	.
</s>
<s>
Vážně	vážně	k6eAd1	vážně
zraněn	zraněn	k2eAgMnSc1d1	zraněn
byl	být	k5eAaImAgInS	být
rovněž	rovněž	k9	rovněž
guvernér	guvernér	k1gMnSc1	guvernér
Connaly	Connaly	k1gMnSc1	Connaly
<g/>
,	,	kIx,	,
atentát	atentát	k1gInSc4	atentát
však	však	k9	však
přežil	přežít	k5eAaPmAgMnS	přežít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kennedy	Kenned	k1gMnPc4	Kenned
se	se	k3xPyFc4	se
tak	tak	k9	tak
po	po	k7c6	po
Lincolnovi	Lincoln	k1gMnSc6	Lincoln
<g/>
,	,	kIx,	,
Garfieldovi	Garfield	k1gMnSc3	Garfield
a	a	k8xC	a
McKinleymu	McKinleymum	k1gNnSc3	McKinleymum
stal	stát	k5eAaPmAgMnS	stát
čtvrtým	čtvrtý	k4xOgMnSc7	čtvrtý
americkým	americký	k2eAgMnSc7d1	americký
prezidentem	prezident	k1gMnSc7	prezident
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
následkům	následek	k1gInPc3	následek
atentátu	atentát	k1gInSc3	atentát
spáchanému	spáchaný	k2eAgInSc3d1	spáchaný
v	v	k7c6	v
době	doba	k1gFnSc6	doba
jeho	on	k3xPp3gNnSc2	on
funkčního	funkční	k2eAgNnSc2d1	funkční
období	období	k1gNnSc2	období
<g/>
.	.	kIx.	.
<g/>
Vyšetřování	vyšetřování	k1gNnSc1	vyšetřování
Warrenovy	Warrenův	k2eAgFnSc2d1	Warrenova
komise	komise	k1gFnSc2	komise
(	(	kIx(	(
<g/>
1963	[number]	k4	1963
<g/>
–	–	k?	–
<g/>
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Zvláštního	zvláštní	k2eAgInSc2d1	zvláštní
výboru	výbor	k1gInSc2	výbor
pro	pro	k7c4	pro
vyšetření	vyšetření	k1gNnSc4	vyšetření
atentátu	atentát	k1gInSc2	atentát
Sněmovny	sněmovna	k1gFnSc2	sněmovna
reprezentantů	reprezentant	k1gMnPc2	reprezentant
(	(	kIx(	(
<g/>
zkr.	zkr.	kA	zkr.
HSCA	HSCA	kA	HSCA
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1976	[number]	k4	1976
<g/>
–	–	k?	–
<g/>
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
a	a	k8xC	a
různých	různý	k2eAgFnPc2d1	různá
vládních	vládní	k2eAgFnPc2d1	vládní
agentur	agentura	k1gFnPc2	agentura
došla	dojít	k5eAaPmAgFnS	dojít
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
atentát	atentát	k1gInSc1	atentát
byl	být	k5eAaImAgInS	být
spáchán	spáchán	k2eAgInSc1d1	spáchán
Lee	Lea	k1gFnSc3	Lea
Harvey	Harvea	k1gFnSc2	Harvea
Oswaldem	Oswald	k1gMnSc7	Oswald
<g/>
,	,	kIx,	,
bývalým	bývalý	k2eAgMnSc7d1	bývalý
příslušníkem	příslušník	k1gMnSc7	příslušník
Námořní	námořní	k2eAgFnSc2d1	námořní
pěchoty	pěchota	k1gFnSc2	pěchota
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sněmovní	sněmovní	k2eAgFnSc1d1	sněmovní
komise	komise	k1gFnSc1	komise
navíc	navíc	k6eAd1	navíc
konstatovala	konstatovat	k5eAaBmAgFnS	konstatovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vražda	vražda	k1gFnSc1	vražda
byla	být	k5eAaImAgFnS	být
výsledkem	výsledek	k1gInSc7	výsledek
"	"	kIx"	"
<g/>
rozsáhlého	rozsáhlý	k2eAgNnSc2d1	rozsáhlé
spiknutí	spiknutí	k1gNnSc2	spiknutí
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Povaha	povaha	k1gFnSc1	povaha
tohoto	tento	k3xDgNnSc2	tento
spiknutí	spiknutí	k1gNnSc2	spiknutí
ovšem	ovšem	k9	ovšem
nebyla	být	k5eNaImAgFnS	být
vyjasněna	vyjasněn	k2eAgFnSc1d1	vyjasněna
a	a	k8xC	a
existuje	existovat	k5eAaImIp3nS	existovat
pouze	pouze	k6eAd1	pouze
řada	řada	k1gFnSc1	řada
podezření	podezření	k1gNnSc2	podezření
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozadí	pozadí	k1gNnSc6	pozadí
atentátu	atentát	k1gInSc3	atentát
mohl	moct	k5eAaImAgInS	moct
stát	stát	k5eAaImF	stát
například	například	k6eAd1	například
kubánský	kubánský	k2eAgInSc4d1	kubánský
režim	režim	k1gInSc4	režim
Fidela	Fidel	k1gMnSc2	Fidel
Castra	Castr	k1gMnSc2	Castr
<g/>
,	,	kIx,	,
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
<g/>
,	,	kIx,	,
kubánské	kubánský	k2eAgFnSc2d1	kubánská
komunity	komunita	k1gFnSc2	komunita
na	na	k7c6	na
území	území	k1gNnSc6	území
USA	USA	kA	USA
<g/>
,	,	kIx,	,
mafie	mafie	k1gFnSc1	mafie
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Podezření	podezření	k1gNnSc1	podezření
se	se	k3xPyFc4	se
nevyhnulo	vyhnout	k5eNaPmAgNnS	vyhnout
ani	ani	k8xC	ani
složkám	složka	k1gFnPc3	složka
americké	americký	k2eAgFnSc2d1	americká
vlády	vláda	k1gFnSc2	vláda
jako	jako	k8xC	jako
FBI	FBI	kA	FBI
<g/>
,	,	kIx,	,
CIA	CIA	kA	CIA
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
viceprezidentovi	viceprezident	k1gMnSc3	viceprezident
Lyndonu	Lyndon	k1gMnSc3	Lyndon
B.	B.	kA	B.
Johnsonovi	Johnson	k1gMnSc3	Johnson
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
ve	v	k7c6	v
shodě	shoda	k1gFnSc6	shoda
s	s	k7c7	s
ústavou	ústava	k1gFnSc7	ústava
po	po	k7c4	po
Kennedyho	Kennedy	k1gMnSc4	Kennedy
smrti	smrt	k1gFnSc2	smrt
převzal	převzít	k5eAaPmAgInS	převzít
úřad	úřad	k1gInSc1	úřad
hlavy	hlava	k1gFnSc2	hlava
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobný	pravděpodobný	k2eAgMnSc1d1	pravděpodobný
střelec	střelec	k1gMnSc1	střelec
Lee	Lea	k1gFnSc6	Lea
Harvey	Harvea	k1gFnPc1	Harvea
Oswald	Oswalda	k1gFnPc2	Oswalda
se	se	k3xPyFc4	se
k	k	k7c3	k
ničemu	nic	k3yNnSc3	nic
nepřiznal	přiznat	k5eNaPmAgMnS	přiznat
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
zavražděn	zavraždit	k5eAaPmNgMnS	zavraždit
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
po	po	k7c6	po
zadržení	zadržení	k1gNnSc6	zadržení
<g/>
.	.	kIx.	.
<g/>
Další	další	k2eAgInPc1d1	další
poznatky	poznatek	k1gInPc1	poznatek
k	k	k7c3	k
zavraždění	zavraždění	k1gNnSc3	zavraždění
prezidenta	prezident	k1gMnSc2	prezident
přináší	přinášet	k5eAaImIp3nS	přinášet
odborná	odborný	k2eAgFnSc1d1	odborná
a	a	k8xC	a
hloubková	hloubkový	k2eAgFnSc1d1	hloubková
sonda	sonda	k1gFnSc1	sonda
-	-	kIx~	-
publikace	publikace	k1gFnSc1	publikace
od	od	k7c2	od
majora	major	k1gMnSc2	major
Ralph	Ralpha	k1gFnPc2	Ralpha
P.	P.	kA	P.
<g/>
Ganis	Ganis	k1gFnSc2	Ganis
-	-	kIx~	-
The	The	k1gFnPc1	The
Skorzeny	Skorzen	k2eAgFnPc1d1	Skorzen
Papers	Papers	k1gInSc4	Papers
<g/>
:	:	kIx,	:
Evidence	evidence	k1gFnSc1	evidence
for	forum	k1gNnPc2	forum
the	the	k?	the
Plot	plot	k1gInSc1	plot
to	ten	k3xDgNnSc1	ten
kill	kilnout	k5eAaPmAgInS	kilnout
JFK	JFK	kA	JFK
<g/>
,	,	kIx,	,
o	o	k7c6	o
příběhu	příběh	k1gInSc6	příběh
Otto	Otto	k1gMnSc1	Otto
Skorzenyho	Skorzeny	k1gMnSc2	Skorzeny
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
cestě	cesta	k1gFnSc6	cesta
do	do	k7c2	do
Dallasu	Dallas	k1gInSc2	Dallas
v	v	k7c6	v
roli	role	k1gFnSc6	role
poradce	poradce	k1gMnSc2	poradce
<g/>
,	,	kIx,	,
obchodníka	obchodník	k1gMnSc2	obchodník
se	s	k7c7	s
zbraněmi	zbraň	k1gFnPc7	zbraň
<g/>
.	.	kIx.	.
<g/>
Atentát	atentát	k1gInSc1	atentát
na	na	k7c4	na
populárního	populární	k2eAgMnSc4d1	populární
prezidenta	prezident	k1gMnSc4	prezident
vyvolal	vyvolat	k5eAaPmAgInS	vyvolat
celosvětový	celosvětový	k2eAgInSc1d1	celosvětový
ohlas	ohlas	k1gInSc1	ohlas
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgInSc1d1	státní
pohřeb	pohřeb	k1gInSc1	pohřeb
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
Arlingtonském	Arlingtonský	k2eAgInSc6d1	Arlingtonský
národním	národní	k2eAgInSc6d1	národní
hřbitově	hřbitov	k1gInSc6	hřbitov
25.	[number]	k4	25.
listopadu	listopad	k1gInSc2	listopad
1963	[number]	k4	1963
a	a	k8xC	a
účastnilo	účastnit	k5eAaImAgNnS	účastnit
se	se	k3xPyFc4	se
jej	on	k3xPp3gNnSc4	on
220	[number]	k4	220
zástupců	zástupce	k1gMnPc2	zástupce
zemí	zem	k1gFnPc2	zem
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
nevyjímaje	nevyjímaje	k7c4	nevyjímaje
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
málokterá	málokterý	k3yIgFnSc1	málokterý
jiná	jiný	k2eAgFnSc1d1	jiná
politická	politický	k2eAgFnSc1d1	politická
událost	událost	k1gFnSc1	událost
se	se	k3xPyFc4	se
zavraždění	zavraždění	k1gNnPc2	zavraždění
Kennedyho	Kennedy	k1gMnSc2	Kennedy
zapsalo	zapsat	k5eAaPmAgNnS	zapsat
do	do	k7c2	do
paměti	paměť	k1gFnSc2	paměť
veřejnosti	veřejnost	k1gFnSc2	veřejnost
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Citáty	citát	k1gInPc1	citát
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
Christopher	Christophra	k1gFnPc2	Christophra
Andersen	Andersen	k1gMnSc1	Andersen
<g/>
:	:	kIx,	:
Jack	Jack	k1gMnSc1	Jack
a	a	k8xC	a
Jackie	Jackie	k1gFnSc1	Jackie
Kennedyovi	Kennedyovi	k1gRnPc1	Kennedyovi
–	–	k?	–
americké	americký	k2eAgNnSc1d1	americké
manželství	manželství	k1gNnSc1	manželství
<g/>
,	,	kIx,	,
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
PRÁH	práh	k1gInSc1	práh
pro	pro	k7c4	pro
Knižní	knižní	k2eAgInSc4d1	knižní
klub	klub	k1gInSc4	klub
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1997.	[number]	k4	1997.
</s>
</p>
<p>
<s>
Robert	Robert	k1gMnSc1	Robert
Dallek	Dallek	k1gMnSc1	Dallek
<g/>
:	:	kIx,	:
Nedokončený	dokončený	k2eNgInSc1d1	nedokončený
život	život	k1gInSc1	život
<g/>
.	.	kIx.	.
</s>
<s>
John	John	k1gMnSc1	John
F.	F.	kA	F.
Kennedy	Kenneda	k1gMnSc2	Kenneda
<g/>
.	.	kIx.	.
1917	[number]	k4	1917
<g/>
–	–	k?	–
<g/>
1963	[number]	k4	1963
<g/>
.	.	kIx.	.
</s>
<s>
Argo	Argo	k1gNnSc1	Argo
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2006.	[number]	k4	2006.
</s>
</p>
<p>
<s>
Profiles	Profiles	k1gInSc1	Profiles
in	in	k?	in
Courage	Courage	k1gInSc1	Courage
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Knižní	knižní	k2eAgFnPc4d1	knižní
publikace	publikace	k1gFnPc4	publikace
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
napsal	napsat	k5eAaBmAgMnS	napsat
JFK	JFK	kA	JFK
===	===	k?	===
</s>
</p>
<p>
<s>
Why	Why	k?	Why
England	England	k1gInSc1	England
Slept	Slept	k1gInSc1	Slept
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
1941.	[number]	k4	1941.
</s>
</p>
<p>
<s>
Portréty	portrét	k1gInPc1	portrét
odvahy	odvaha	k1gFnSc2	odvaha
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Argo	Argo	k1gNnSc1	Argo
<g/>
,	,	kIx,	,
2013.	[number]	k4	2013.
(	(	kIx(	(
<g/>
Profiles	Profilesa	k1gFnPc2	Profilesa
in	in	k?	in
Courage	Courage	k1gFnPc2	Courage
<g/>
,	,	kIx,	,
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Pulitzerova	Pulitzerův	k2eAgFnSc1d1	Pulitzerova
cena	cena	k1gFnSc1	cena
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
A	a	k9	a
Nation	Nation	k1gInSc1	Nation
of	of	k?	of
Emigrants	Emigrants	k1gInSc1	Emigrants
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
1958.	[number]	k4	1958.
</s>
</p>
<p>
<s>
The	The	k?	The
Strategy	Strateg	k1gInPc1	Strateg
of	of	k?	of
Peace	Peaec	k1gInSc2	Peaec
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
1960.	[number]	k4	1960.
</s>
</p>
<p>
<s>
To	ten	k3xDgNnSc1	ten
Turn	Turn	k1gNnSc1	Turn
the	the	k?	the
Tide	Tid	k1gInSc2	Tid
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
1962.	[number]	k4	1962.
</s>
</p>
<p>
<s>
The	The	k?	The
Burden	Burdno	k1gNnPc2	Burdno
and	and	k?	and
the	the	k?	the
Glory	Glora	k1gFnSc2	Glora
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
1964.	[number]	k4	1964.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Stěžejní	stěžejní	k2eAgFnSc1d1	stěžejní
literatura	literatura	k1gFnSc1	literatura
o	o	k7c4	o
JFK	JFK	kA	JFK
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Adler	Adler	k1gMnSc1	Adler
<g/>
,	,	kIx,	,
Bill	Bill	k1gMnSc1	Bill
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Kennedy	Kenneda	k1gMnSc2	Kenneda
Wit	Wit	k1gMnSc2	Wit
<g/>
,	,	kIx,	,
Boston	Boston	k1gInSc1	Boston
1965	[number]	k4	1965
</s>
</p>
<p>
<s>
Budín	Budín	k1gMnSc1	Budín
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
<g/>
:	:	kIx,	:
Dynastie	dynastie	k1gFnSc1	dynastie
Kennedyů	Kennedy	k1gInPc2	Kennedy
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1969	[number]	k4	1969
</s>
</p>
<p>
<s>
Cross	Cross	k6eAd1	Cross
<g/>
,	,	kIx,	,
Robin	Robina	k1gFnPc2	Robina
<g/>
:	:	kIx,	:
J.F.K.	J.F.K.	k1gFnPc2	J.F.K.
A	a	k8xC	a
Hidden	Hiddna	k1gFnPc2	Hiddna
Life	Life	k1gNnSc1	Life
<g/>
,	,	kIx,	,
Londýn	Londýn	k1gInSc1	Londýn
1992	[number]	k4	1992
</s>
</p>
<p>
<s>
Hersh	Hersh	k1gMnSc1	Hersh
<g/>
,	,	kIx,	,
Seymour	Seymour	k1gMnSc1	Seymour
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Dark	Dark	k1gMnSc1	Dark
Side	Sid	k1gFnSc2	Sid
of	of	k?	of
Camelot	Camelot	k1gInSc1	Camelot
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
1997	[number]	k4	1997
</s>
</p>
<p>
<s>
Lane	Lane	k1gFnSc1	Lane
<g/>
,	,	kIx,	,
Mark	Mark	k1gMnSc1	Mark
<g/>
:	:	kIx,	:
Honba	honba	k1gFnSc1	honba
za	za	k7c7	za
právem	právo	k1gNnSc7	právo
<g/>
.	.	kIx.	.
</s>
<s>
Kritika	kritika	k1gFnSc1	kritika
způsobu	způsob	k1gInSc2	způsob
vyšetřování	vyšetřování	k1gNnSc2	vyšetřování
vražd	vražda	k1gFnPc2	vražda
presidenta	president	k1gMnSc2	president
Kennedyho	Kennedy	k1gMnSc2	Kennedy
<g/>
,	,	kIx,	,
policisty	policista	k1gMnSc2	policista
J.	J.	kA	J.
D.	D.	kA	D.
Tippita	Tippita	k1gFnSc1	Tippita
a	a	k8xC	a
Lee	Lea	k1gFnSc3	Lea
Harveye	Harveye	k1gFnPc2	Harveye
Oswalda	Oswaldo	k1gNnSc2	Oswaldo
<g/>
,	,	kIx,	,
prováděného	prováděný	k2eAgNnSc2d1	prováděné
Warrenovou	Warrenová	k1gFnSc4	Warrenová
komisí	komise	k1gFnPc2	komise
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1967.	[number]	k4	1967.
(	(	kIx(	(
<g/>
Z	z	k7c2	z
anglického	anglický	k2eAgInSc2d1	anglický
originálu	originál	k1gInSc2	originál
Rush	Rush	k1gMnSc1	Rush
to	ten	k3xDgNnSc1	ten
Judgment	Judgment	k1gInSc1	Judgment
<g/>
,	,	kIx,	,
Londýn	Londýn	k1gInSc1	Londýn
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
MacGregor	MacGregor	k1gInSc1	MacGregor
Burns	Burns	k1gInSc1	Burns
<g/>
,	,	kIx,	,
James	James	k1gMnSc1	James
<g/>
:	:	kIx,	:
John	John	k1gMnSc1	John
Kennedy	Kenneda	k1gMnSc2	Kenneda
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
political	politicat	k5eAaPmAgMnS	politicat
Profile	profil	k1gInSc5	profil
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
1960	[number]	k4	1960
</s>
</p>
<p>
<s>
Manchester	Manchester	k1gInSc1	Manchester
<g/>
,	,	kIx,	,
William	William	k1gInSc1	William
<g/>
:	:	kIx,	:
Portrait	Portrait	k1gInSc1	Portrait
of	of	k?	of
a	a	k8xC	a
President	president	k1gMnSc1	president
<g/>
,	,	kIx,	,
Boston	Boston	k1gInSc1	Boston
1962	[number]	k4	1962
</s>
</p>
<p>
<s>
Puzzo	Puzza	k1gFnSc5	Puzza
<g/>
,	,	kIx,	,
Mario	Mario	k1gMnSc1	Mario
<g/>
:	:	kIx,	:
K	K	kA	K
znamená	znamenat	k5eAaImIp3nS	znamenat
Keneddy	Kenedda	k1gFnPc4	Kenedda
</s>
</p>
<p>
<s>
Salinger	Salinger	k1gMnSc1	Salinger
<g/>
,	,	kIx,	,
Pierre	Pierr	k1gMnSc5	Pierr
<g/>
:	:	kIx,	:
With	With	k1gInSc1	With
Kennedy	Kenneda	k1gMnSc2	Kenneda
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
1966	[number]	k4	1966
</s>
</p>
<p>
<s>
Schlesinger	Schlesinger	k1gMnSc1	Schlesinger
<g/>
,	,	kIx,	,
Arthur	Arthur	k1gMnSc1	Arthur
mladší	mladý	k2eAgMnSc1d2	mladší
<g/>
:	:	kIx,	:
A	a	k8xC	a
Thousand	Thousand	k1gInSc4	Thousand
Days	Daysa	k1gFnPc2	Daysa
<g/>
.	.	kIx.	.
</s>
<s>
John	John	k1gMnSc1	John
F.	F.	kA	F.
Kennedy	Kenneda	k1gMnSc2	Kenneda
in	in	k?	in
the	the	k?	the
White	Whit	k1gInSc5	Whit
House	house	k1gNnSc1	house
<g/>
,	,	kIx,	,
Boston	Boston	k1gInSc1	Boston
1965	[number]	k4	1965
</s>
</p>
<p>
<s>
Sidney	Sidnea	k1gFnPc1	Sidnea
<g/>
,	,	kIx,	,
Hugh	Hugh	k1gMnSc1	Hugh
<g/>
:	:	kIx,	:
John	John	k1gMnSc1	John
F.	F.	kA	F.
Kennedy	Kenneda	k1gMnSc2	Kenneda
<g/>
,	,	kIx,	,
President	president	k1gMnSc1	president
<g/>
,	,	kIx,	,
Boston	Boston	k1gInSc1	Boston
1965	[number]	k4	1965
</s>
</p>
<p>
<s>
Sorensen	Sorensen	k1gInSc1	Sorensen
<g/>
,	,	kIx,	,
Theodore	Theodor	k1gMnSc5	Theodor
<g/>
:	:	kIx,	:
The	The	k1gMnSc2	The
Kennedy	Kenneda	k1gMnSc2	Kenneda
Legacy	Legaca	k1gMnSc2	Legaca
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
Peaceful	Peaceful	k1gInSc4	Peaceful
Revolution	Revolution	k1gInSc4	Revolution
for	forum	k1gNnPc2	forum
the	the	k?	the
Seventies	Seventies	k1gMnSc1	Seventies
<g/>
,	,	kIx,	,
Toronto	Toronto	k1gNnSc1	Toronto
1969	[number]	k4	1969
</s>
</p>
<p>
<s>
Summers	Summers	k1gInSc1	Summers
<g/>
,	,	kIx,	,
Anthony	Anthon	k1gInPc1	Anthon
<g/>
:	:	kIx,	:
Conspiracy	Conspiraca	k1gFnPc1	Conspiraca
who	who	k?	who
killed	killed	k1gMnSc1	killed
President	president	k1gMnSc1	president
Kennedy	Kenneda	k1gMnSc2	Kenneda
<g/>
,	,	kIx,	,
Boston	Boston	k1gInSc1	Boston
1983	[number]	k4	1983
</s>
</p>
<p>
<s>
Taterová	Taterová	k1gFnSc1	Taterová
<g/>
,	,	kIx,	,
Milada	Milada	k1gFnSc1	Milada
<g/>
,	,	kIx,	,
Novák	Novák	k1gMnSc1	Novák
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
:	:	kIx,	:
Kdo	kdo	k3yInSc1	kdo
střílí	střílet	k5eAaImIp3nS	střílet
na	na	k7c4	na
presidenty	president	k1gMnPc4	president
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1968	[number]	k4	1968
</s>
</p>
<p>
<s>
Volek	Volek	k1gMnSc1	Volek
<g/>
,	,	kIx,	,
Jindřich	Jindřich	k1gMnSc1	Jindřich
<g/>
:	:	kIx,	:
Meze	mez	k1gFnPc1	mez
odvahy	odvaha	k1gFnSc2	odvaha
<g/>
.	.	kIx.	.
</s>
<s>
Politický	politický	k2eAgInSc1d1	politický
profil	profil	k1gInSc1	profil
J.	J.	kA	J.
F.	F.	kA	F.
Kennedyho	Kennedy	k1gMnSc2	Kennedy
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1965	[number]	k4	1965
</s>
</p>
<p>
<s>
Volek	Volek	k1gMnSc1	Volek
Jindřich	Jindřich	k1gMnSc1	Jindřich
<g/>
:	:	kIx,	:
Kritické	kritický	k2eAgInPc1d1	kritický
dny	den	k1gInPc1	den
J.	J.	kA	J.
F.	F.	kA	F.
Kennedyho	Kennedy	k1gMnSc2	Kennedy
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1968	[number]	k4	1968
</s>
</p>
<p>
<s>
Whalen	Whalen	k1gInSc1	Whalen
J.	J.	kA	J.
Richard	Richard	k1gMnSc1	Richard
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Founding	Founding	k1gInSc1	Founding
Father	Fathra	k1gFnPc2	Fathra
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
1964	[number]	k4	1964
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
John	John	k1gMnSc1	John
Fitzgerald	Fitzgerald	k1gMnSc1	Fitzgerald
Kennedy	Kenneda	k1gMnSc2	Kenneda
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
John	John	k1gMnSc1	John
Fitzgerald	Fitzgerald	k1gMnSc1	Fitzgerald
Kennedy	Kenneda	k1gMnSc2	Kenneda
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
John	John	k1gMnSc1	John
Fitzgerald	Fitzgerald	k1gMnSc1	Fitzgerald
Kennedy	Kenneda	k1gMnSc2	Kenneda
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
John	John	k1gMnSc1	John
Fitzgerald	Fitzgerald	k1gMnSc1	Fitzgerald
Kennedy	Kenneda	k1gMnSc2	Kenneda
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Kennedyho	Kennedyze	k6eAd1	Kennedyze
knihovna	knihovna	k1gFnSc1	knihovna
v	v	k7c6	v
Bostonu	Boston	k1gInSc6	Boston
<g/>
,	,	kIx,	,
MA	MA	kA	MA
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgInSc1d1	oficiální
životopis	životopis	k1gInSc1	životopis
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Bílého	bílý	k2eAgInSc2d1	bílý
domu	dům	k1gInSc2	dům
</s>
</p>
<p>
<s>
50	[number]	k4	50
let	let	k1gInSc4	let
od	od	k7c2	od
atentátu	atentát	k1gInSc2	atentát
na	na	k7c6	na
JFK	JFK	kA	JFK
–	–	k?	–
Velvyslanectví	velvyslanectví	k1gNnSc1	velvyslanectví
USA	USA	kA	USA
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
</s>
</p>
<p>
<s>
Nově	nově	k6eAd1	nově
nalezené	nalezený	k2eAgInPc1d1	nalezený
pásky	pásek	k1gInPc1	pásek
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
Kennedyho	Kennedy	k1gMnSc4	Kennedy
jako	jako	k8xS	jako
introverta	introvert	k1gMnSc4	introvert
<g/>
,	,	kIx,	,
aktualne.cz	aktualne.cz	k1gMnSc1	aktualne.cz
<g/>
,	,	kIx,	,
14.	[number]	k4	14.
říjen	říjen	k1gInSc1	říjen
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
</s>
</p>
<p>
<s>
Atentát	atentát	k1gInSc1	atentát
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
<g/>
:	:	kIx,	:
nejasno	jasno	k6eNd1	jasno
i	i	k9	i
po	po	k7c6	po
44	[number]	k4	44
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
aktualne.cz	aktualne.cz	k1gMnSc1	aktualne.cz
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Tuček	Tuček	k1gMnSc1	Tuček
<g/>
,	,	kIx,	,
22.	[number]	k4	22.
listopad	listopad	k1gInSc1	listopad
2007	[number]	k4	2007
</s>
</p>
<p>
<s>
Zapruderův	Zapruderův	k2eAgInSc1d1	Zapruderův
8	[number]	k4	8
mm	mm	kA	mm
film	film	k1gInSc1	film
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nixův	Nixův	k2eAgInSc1d1	Nixův
8	[number]	k4	8
mm	mm	kA	mm
film	film	k1gInSc1	film
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
JFK	JFK	kA	JFK
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
film	film	k1gInSc4	film
Olivera	Oliver	k1gMnSc2	Oliver
Stonea	Stoneus	k1gMnSc2	Stoneus
</s>
</p>
<p>
<s>
Projev	projev	k1gInSc1	projev
JFK	JFK	kA	JFK
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
</s>
</p>
<p>
<s>
Atentát	atentát	k1gInSc1	atentát
na	na	k7c4	na
Johna	John	k1gMnSc4	John
Fitzgeralda	Fitzgerald	k1gMnSc4	Fitzgerald
Kennedyho	Kennedy	k1gMnSc4	Kennedy
</s>
</p>
<p>
<s>
22.	[number]	k4	22.
listopad	listopad	k1gInSc4	listopad
-	-	kIx~	-
den	den	k1gInSc4	den
kdy	kdy	k6eAd1	kdy
zavraždili	zavraždit	k5eAaPmAgMnP	zavraždit
Kennedyho	Kennedy	k1gMnSc4	Kennedy
</s>
</p>
<p>
<s>
John	John	k1gMnSc1	John
Fitzgerald	Fitzgerald	k1gMnSc1	Fitzgerald
Kennedy	Kenneda	k1gMnSc2	Kenneda
-	-	kIx~	-
video	video	k1gNnSc1	video
z	z	k7c2	z
cyklu	cyklus	k1gInSc2	cyklus
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
Historický	historický	k2eAgInSc1d1	historický
magazín	magazín	k1gInSc1	magazín
</s>
</p>
<p>
<s>
Před	před	k7c7	před
100	[number]	k4	100
lety	léto	k1gNnPc7	léto
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
šarmantní	šarmantní	k2eAgMnSc1d1	šarmantní
muž	muž	k1gMnSc1	muž
<g/>
:	:	kIx,	:
J.	J.	kA	J.
F.	F.	kA	F.
Kennedy	Kenneda	k1gMnSc2	Kenneda
</s>
</p>
