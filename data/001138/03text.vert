<p>
<s>
Česko	Česko	k1gNnSc1	Česko
<g/>
,	,	kIx,	,
úředním	úřední	k2eAgInSc7d1	úřední
názvem	název	k1gInSc7	název
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
stát	stát	k5eAaPmF	stát
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Samostatným	samostatný	k2eAgInSc7d1	samostatný
státem	stát	k1gInSc7	stát
se	se	k3xPyFc4	se
Česko	Česko	k1gNnSc1	Česko
stalo	stát	k5eAaPmAgNnS	stát
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
tradice	tradice	k1gFnPc4	tradice
státnosti	státnost	k1gFnSc2	státnost
Československa	Československo	k1gNnSc2	Československo
<g/>
,	,	kIx,	,
Českého	český	k2eAgNnSc2d1	české
království	království	k1gNnSc2	království
<g/>
,	,	kIx,	,
Českého	český	k2eAgNnSc2d1	české
knížectví	knížectví	k1gNnSc2	knížectví
a	a	k8xC	a
Velké	velký	k2eAgFnSc2d1	velká
Moravy	Morava	k1gFnSc2	Morava
<g/>
,	,	kIx,	,
sahající	sahající	k2eAgInSc4d1	sahající
do	do	k7c2	do
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
české	český	k2eAgFnSc2d1	Česká
ústavy	ústava	k1gFnSc2	ústava
je	být	k5eAaImIp3nS	být
parlamentní	parlamentní	k2eAgInSc1d1	parlamentní
<g/>
,	,	kIx,	,
demokratický	demokratický	k2eAgInSc1d1	demokratický
právní	právní	k2eAgInSc1d1	právní
stát	stát	k1gInSc1	stát
s	s	k7c7	s
liberálním	liberální	k2eAgInSc7d1	liberální
státním	státní	k2eAgInSc7d1	státní
režimem	režim	k1gInSc7	režim
a	a	k8xC	a
politickým	politický	k2eAgInSc7d1	politický
systémem	systém	k1gInSc7	systém
založeným	založený	k2eAgInSc7d1	založený
na	na	k7c6	na
svobodné	svobodný	k2eAgFnSc6d1	svobodná
soutěži	soutěž	k1gFnSc6	soutěž
politických	politický	k2eAgFnPc2d1	politická
stran	strana	k1gFnPc2	strana
a	a	k8xC	a
hnutí	hnutí	k1gNnPc2	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
Hlavou	hlava	k1gFnSc7	hlava
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
prezident	prezident	k1gMnSc1	prezident
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
vrcholným	vrcholný	k2eAgInSc7d1	vrcholný
a	a	k8xC	a
jediným	jediný	k2eAgInSc7d1	jediný
zákonodárným	zákonodárný	k2eAgInSc7d1	zákonodárný
orgánem	orgán	k1gInSc7	orgán
je	být	k5eAaImIp3nS	být
dvoukomorový	dvoukomorový	k2eAgInSc1d1	dvoukomorový
Parlament	parlament	k1gInSc1	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
moci	moct	k5eAaImF	moct
výkonné	výkonný	k2eAgFnPc4d1	výkonná
stojí	stát	k5eAaImIp3nS	stát
vláda	vláda	k1gFnSc1	vláda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Česko	Česko	k1gNnSc1	Česko
je	být	k5eAaImIp3nS	být
země	zem	k1gFnPc4	zem
s	s	k7c7	s
tržním	tržní	k2eAgNnSc7d1	tržní
hospodářstvím	hospodářství	k1gNnSc7	hospodářství
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
podle	podle	k7c2	podle
ekonomických	ekonomický	k2eAgInPc2d1	ekonomický
<g/>
,	,	kIx,	,
sociálních	sociální	k2eAgInPc2d1	sociální
a	a	k8xC	a
politických	politický	k2eAgInPc2d1	politický
indikátorů	indikátor	k1gInPc2	indikátor
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
HDP	HDP	kA	HDP
na	na	k7c4	na
obyvatele	obyvatel	k1gMnPc4	obyvatel
<g/>
,	,	kIx,	,
index	index	k1gInSc1	index
lidského	lidský	k2eAgInSc2d1	lidský
rozvoje	rozvoj	k1gInSc2	rozvoj
<g/>
,	,	kIx,	,
index	index	k1gInSc1	index
svobody	svoboda	k1gFnSc2	svoboda
tisku	tisk	k1gInSc2	tisk
<g/>
,	,	kIx,	,
index	index	k1gInSc1	index
svobody	svoboda	k1gFnSc2	svoboda
internetu	internet	k1gInSc2	internet
od	od	k7c2	od
cenzury	cenzura	k1gFnSc2	cenzura
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
vysoce	vysoce	k6eAd1	vysoce
rozvinutým	rozvinutý	k2eAgInPc3d1	rozvinutý
státům	stát	k1gInPc3	stát
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomicky	ekonomicky	k6eAd1	ekonomicky
patří	patřit	k5eAaImIp3nS	patřit
dle	dle	k7c2	dle
Světové	světový	k2eAgFnSc2d1	světová
banky	banka	k1gFnSc2	banka
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
31	[number]	k4	31
nejbohatších	bohatý	k2eAgInPc2d3	nejbohatší
států	stát	k1gInPc2	stát
světa	svět	k1gInSc2	svět
s	s	k7c7	s
nejvyššími	vysoký	k2eAgInPc7d3	Nejvyšší
finančními	finanční	k2eAgInPc7d1	finanční
příjmy	příjem	k1gInPc7	příjem
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
<g/>
,	,	kIx,	,
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
státy	stát	k1gInPc7	stát
má	mít	k5eAaImIp3nS	mít
velmi	velmi	k6eAd1	velmi
malý	malý	k2eAgInSc1d1	malý
podíl	podíl	k1gInSc1	podíl
obyvatel	obyvatel	k1gMnPc2	obyvatel
žijících	žijící	k2eAgMnPc2d1	žijící
pod	pod	k7c7	pod
prahem	práh	k1gInSc7	práh
chudoby	chudoba	k1gFnSc2	chudoba
<g/>
.	.	kIx.	.
</s>
<s>
Vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
též	též	k9	též
poměrně	poměrně	k6eAd1	poměrně
nízkou	nízký	k2eAgFnSc4d1	nízká
nerovnost	nerovnost	k1gFnSc4	nerovnost
mezi	mezi	k7c7	mezi
nejbohatšími	bohatý	k2eAgMnPc7d3	nejbohatší
a	a	k8xC	a
nejchudšími	chudý	k2eAgMnPc7d3	nejchudší
obyvateli	obyvatel	k1gMnPc7	obyvatel
a	a	k8xC	a
relativně	relativně	k6eAd1	relativně
vyvážené	vyvážený	k2eAgNnSc4d1	vyvážené
přerozdělování	přerozdělování	k1gNnSc4	přerozdělování
bohatství	bohatství	k1gNnSc2	bohatství
napříč	napříč	k7c7	napříč
populací	populace	k1gFnSc7	populace
<g/>
.	.	kIx.	.
</s>
<s>
Míra	Míra	k1gFnSc1	Míra
nezaměstnanosti	nezaměstnanost	k1gFnSc2	nezaměstnanost
je	být	k5eAaImIp3nS	být
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
nízká	nízký	k2eAgFnSc1d1	nízká
a	a	k8xC	a
pod	pod	k7c7	pod
průměrem	průměr	k1gInSc7	průměr
vyspělých	vyspělý	k2eAgFnPc2d1	vyspělá
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
indexu	index	k1gInSc6	index
ekologické	ekologický	k2eAgFnSc2d1	ekologická
stopy	stopa	k1gFnSc2	stopa
je	být	k5eAaImIp3nS	být
Česko	Česko	k1gNnSc4	Česko
oproti	oproti	k7c3	oproti
některým	některý	k3yIgFnPc3	některý
jiným	jiný	k2eAgFnPc3d1	jiná
vyspělým	vyspělý	k2eAgFnPc3d1	vyspělá
zemím	zem	k1gFnPc3	zem
menším	malý	k2eAgMnSc7d2	menší
ekologickým	ekologický	k2eAgMnSc7d1	ekologický
dlužníkem	dlužník	k1gMnSc7	dlužník
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Global	globat	k5eAaImAgMnS	globat
Peace	Peace	k1gFnPc4	Peace
Index	index	k1gInSc4	index
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vypracovává	vypracovávat	k5eAaImIp3nS	vypracovávat
každoročně	každoročně	k6eAd1	každoročně
Institute	institut	k1gInSc5	institut
for	forum	k1gNnPc2	forum
Economics	Economics	k1gInSc1	Economics
and	and	k?	and
Peace	Peace	k1gFnSc2	Peace
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Česko	Česko	k1gNnSc1	Česko
šestou	šestý	k4xOgFnSc7	šestý
nejbezpečnější	bezpečný	k2eAgFnSc7d3	nejbezpečnější
zemí	zem	k1gFnSc7	zem
na	na	k7c6	na
světě	svět	k1gInSc6	svět
(	(	kIx(	(
<g/>
index	index	k1gInSc1	index
zohledňuje	zohledňovat	k5eAaImIp3nS	zohledňovat
hrozbu	hrozba	k1gFnSc4	hrozba
válečného	válečný	k2eAgInSc2d1	válečný
konfliktu	konflikt	k1gInSc2	konflikt
i	i	k9	i
úroveň	úroveň	k1gFnSc4	úroveň
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
násilné	násilný	k2eAgFnSc2d1	násilná
kriminality	kriminalita	k1gFnSc2	kriminalita
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Česko	Česko	k1gNnSc1	Česko
je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
Organizace	organizace	k1gFnSc2	organizace
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
Severoatlantické	severoatlantický	k2eAgFnSc2d1	Severoatlantická
aliance	aliance	k1gFnSc2	aliance
<g />
,	,	kIx,	,
Organizace	organizace	k1gFnSc1	organizace
pro	pro	k7c4	pro
hospodářskou	hospodářský	k2eAgFnSc4d1	hospodářská
spolupráci	spolupráce	k1gFnSc4	spolupráce
a	a	k8xC	a
rozvoj	rozvoj	k1gInSc4	rozvoj
<g/>
,	,	kIx,	,
Světové	světový	k2eAgFnSc2d1	světová
obchodní	obchodní	k2eAgFnSc2d1	obchodní
organizace	organizace	k1gFnSc2	organizace
<g/>
,	,	kIx,	,
Mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
měnového	měnový	k2eAgInSc2d1	měnový
fondu	fond	k1gInSc2	fond
<g/>
,	,	kIx,	,
Světové	světový	k2eAgFnSc2d1	světová
banky	banka	k1gFnSc2	banka
<g/>
,	,	kIx,	,
Rady	rada	k1gFnSc2	rada
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
Organizace	organizace	k1gFnSc2	organizace
pro	pro	k7c4	pro
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
a	a	k8xC	a
spolupráci	spolupráce	k1gFnSc4	spolupráce
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
Evropské	evropský	k2eAgFnSc2d1	Evropská
celní	celní	k2eAgFnSc2d1	celní
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
součástí	součást	k1gFnSc7	součást
Schengenského	schengenský	k2eAgInSc2d1	schengenský
prostoru	prostor	k1gInSc2	prostor
<g/>
,	,	kIx,	,
Evropského	evropský	k2eAgInSc2d1	evropský
hospodářského	hospodářský	k2eAgInSc2d1	hospodářský
prostoru	prostor	k1gInSc2	prostor
<g/>
,	,	kIx,	,
členem	člen	k1gInSc7	člen
Visegrádské	visegrádský	k2eAgFnSc2d1	Visegrádská
skupiny	skupina	k1gFnSc2	skupina
a	a	k8xC	a
jiných	jiný	k2eAgFnPc2d1	jiná
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
struktur	struktura	k1gFnPc2	struktura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Česko	Česko	k1gNnSc1	Česko
je	být	k5eAaImIp3nS	být
vnitrozemský	vnitrozemský	k2eAgInSc1d1	vnitrozemský
stát	stát	k1gInSc1	stát
tvořený	tvořený	k2eAgInSc1d1	tvořený
částmi	část	k1gFnPc7	část
historických	historický	k2eAgFnPc2d1	historická
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
byly	být	k5eAaImAgInP	být
po	po	k7c4	po
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
období	období	k1gNnSc4	období
svého	svůj	k3xOyFgInSc2	svůj
dějinného	dějinný	k2eAgInSc2d1	dějinný
vývoje	vývoj	k1gInSc2	vývoj
součástí	součást	k1gFnPc2	součást
zemí	zem	k1gFnPc2	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
Čechy	Čechy	k1gFnPc1	Čechy
a	a	k8xC	a
Morava	Morava	k1gFnSc1	Morava
<g/>
,	,	kIx,	,
k	k	k7c3	k
nimž	jenž	k3xRgInPc3	jenž
byly	být	k5eAaImAgInP	být
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
připojeny	připojen	k2eAgInPc1d1	připojen
i	i	k8xC	i
České	český	k2eAgInPc1d1	český
Rakousy	Rakousy	k1gInPc1	Rakousy
a	a	k8xC	a
České	český	k2eAgNnSc1d1	české
Slezsko	Slezsko	k1gNnSc1	Slezsko
<g/>
.	.	kIx.	.
</s>
<s>
Česko	Česko	k1gNnSc1	Česko
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
78866	[number]	k4	78866
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Sousedí	sousedit	k5eAaImIp3nS	sousedit
na	na	k7c6	na
západě	západ	k1gInSc6	západ
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
(	(	kIx(	(
<g/>
délka	délka	k1gFnSc1	délka
hranice	hranice	k1gFnSc1	hranice
810	[number]	k4	810
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
severu	sever	k1gInSc6	sever
s	s	k7c7	s
Polskem	Polsko	k1gNnSc7	Polsko
(	(	kIx(	(
<g/>
762	[number]	k4	762
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
východě	východ	k1gInSc6	východ
se	s	k7c7	s
Slovenskem	Slovensko	k1gNnSc7	Slovensko
(	(	kIx(	(
<g/>
252	[number]	k4	252
km	km	kA	km
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
s	s	k7c7	s
Rakouskem	Rakousko	k1gNnSc7	Rakousko
(	(	kIx(	(
<g/>
466	[number]	k4	466
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Administrativně	administrativně	k6eAd1	administrativně
se	se	k3xPyFc4	se
Česko	Česko	k1gNnSc1	Česko
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
osm	osm	k4xCc4	osm
územních	územní	k2eAgInPc2d1	územní
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
na	na	k7c4	na
14	[number]	k4	14
samosprávných	samosprávný	k2eAgInPc2d1	samosprávný
krajů	kraj	k1gInPc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
i	i	k9	i
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
krajů	kraj	k1gInPc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
žilo	žít	k5eAaImAgNnS	žít
přibližně	přibližně	k6eAd1	přibližně
10,6	[number]	k4	10,6
milionu	milion	k4xCgInSc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Výrazná	výrazný	k2eAgFnSc1d1	výrazná
většina	většina	k1gFnSc1	většina
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nS	hlásit
k	k	k7c3	k
české	český	k2eAgFnSc3d1	Česká
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
moravské	moravský	k2eAgFnSc2d1	Moravská
národnosti	národnost	k1gFnSc2	národnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historické	historický	k2eAgInPc1d1	historický
státní	státní	k2eAgInPc1d1	státní
útvary	útvar	k1gInPc1	útvar
na	na	k7c6	na
území	území	k1gNnSc6	území
Česka	Česko	k1gNnSc2	Česko
==	==	k?	==
</s>
</p>
<p>
<s>
Prvním	první	k4xOgInSc7	první
doloženým	doložený	k2eAgInSc7d1	doložený
státním	státní	k2eAgInSc7d1	státní
útvarem	útvar	k1gInSc7	útvar
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
byl	být	k5eAaImAgInS	být
nadkmenový	nadkmenový	k2eAgInSc1d1	nadkmenový
svaz	svaz	k1gInSc1	svaz
Sámovy	Sámův	k2eAgFnSc2d1	Sámova
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
pak	pak	k6eAd1	pak
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
Velkomoravská	velkomoravský	k2eAgFnSc1d1	Velkomoravská
říše	říše	k1gFnSc1	říše
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
907	[number]	k4	907
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
pod	pod	k7c7	pod
náporem	nápor	k1gInSc7	nápor
kočovných	kočovný	k2eAgInPc2d1	kočovný
maďarských	maďarský	k2eAgInPc2d1	maďarský
kmenů	kmen	k1gInPc2	kmen
<g/>
,	,	kIx,	,
těžiště	těžiště	k1gNnSc1	těžiště
státního	státní	k2eAgInSc2d1	státní
vývoje	vývoj	k1gInSc2	vývoj
se	se	k3xPyFc4	se
přesunulo	přesunout	k5eAaPmAgNnS	přesunout
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Tamní	tamní	k2eAgMnPc1d1	tamní
panovníci	panovník	k1gMnPc1	panovník
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
vybudovali	vybudovat	k5eAaPmAgMnP	vybudovat
středověký	středověký	k2eAgInSc4d1	středověký
přemyslovský	přemyslovský	k2eAgInSc4d1	přemyslovský
stát	stát	k1gInSc4	stát
<g/>
,	,	kIx,	,
též	též	k9	též
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
český	český	k2eAgInSc1d1	český
stát	stát	k1gInSc1	stát
(	(	kIx(	(
<g/>
České	český	k2eAgNnSc1d1	české
knížectví	knížectví	k1gNnSc1	knížectví
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
České	český	k2eAgNnSc1d1	české
království	království	k1gNnSc1	království
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
od	od	k7c2	od
přelomu	přelom	k1gInSc2	přelom
10	[number]	k4	10
<g/>
.	.	kIx.	.
a	a	k8xC	a
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
tvořící	tvořící	k2eAgFnSc4d1	tvořící
součást	součást	k1gFnSc4	součást
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
doby	doba	k1gFnSc2	doba
Karla	Karel	k1gMnSc2	Karel
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1348	[number]	k4	1348
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
země	zem	k1gFnPc4	zem
podléhající	podléhající	k2eAgFnSc4d1	podléhající
českému	český	k2eAgMnSc3d1	český
králi	král	k1gMnSc3	král
užívalo	užívat	k5eAaImAgNnS	užívat
rovněž	rovněž	k6eAd1	rovněž
pojmu	pojem	k1gInSc3	pojem
země	zem	k1gFnSc2	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
<g/>
,	,	kIx,	,
kterýžto	kterýžto	k?	kterýžto
pojem	pojem	k1gInSc1	pojem
zahrnoval	zahrnovat	k5eAaImAgInS	zahrnovat
i	i	k9	i
Moravské	moravský	k2eAgNnSc1d1	Moravské
markrabství	markrabství	k1gNnSc1	markrabství
a	a	k8xC	a
Vévodství	vévodství	k1gNnSc1	vévodství
Horní	horní	k2eAgNnSc1d1	horní
a	a	k8xC	a
Dolní	dolní	k2eAgNnSc1d1	dolní
Slezsko	Slezsko	k1gNnSc1	Slezsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1526	[number]	k4	1526
byly	být	k5eAaImAgFnP	být
České	český	k2eAgFnPc1d1	Česká
země	zem	k1gFnPc1	zem
postupně	postupně	k6eAd1	postupně
začleňovány	začleňovat	k5eAaImNgFnP	začleňovat
do	do	k7c2	do
habsburské	habsburský	k2eAgFnSc2d1	habsburská
monarchie	monarchie	k1gFnSc2	monarchie
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
vládci	vládce	k1gMnPc1	vládce
využili	využít	k5eAaPmAgMnP	využít
svého	svůj	k3xOyFgNnSc2	svůj
vítězství	vítězství	k1gNnSc1	vítězství
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
na	na	k7c6	na
Bílé	bílý	k2eAgFnSc6d1	bílá
hoře	hora	k1gFnSc6	hora
nad	nad	k7c7	nad
českými	český	k2eAgInPc7d1	český
stavy	stav	k1gInPc7	stav
(	(	kIx(	(
<g/>
1620	[number]	k4	1620
<g/>
)	)	kIx)	)
k	k	k7c3	k
výraznému	výrazný	k2eAgNnSc3d1	výrazné
omezení	omezení	k1gNnSc3	omezení
dřívější	dřívější	k2eAgFnSc2d1	dřívější
samostatnosti	samostatnost	k1gFnSc2	samostatnost
Českého	český	k2eAgNnSc2d1	české
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
<g/>
,	,	kIx,	,
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1749	[number]	k4	1749
navzájem	navzájem	k6eAd1	navzájem
státoprávně	státoprávně	k6eAd1	státoprávně
nespojité	spojitý	k2eNgFnPc1d1	nespojitá
<g/>
,	,	kIx,	,
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
korunními	korunní	k2eAgFnPc7d1	korunní
zeměmi	zem	k1gFnPc7	zem
Habsburků	Habsburk	k1gMnPc2	Habsburk
až	až	k8xS	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1804	[number]	k4	1804
měla	mít	k5eAaImAgFnS	mít
habsburská	habsburský	k2eAgFnSc1d1	habsburská
monarchie	monarchie	k1gFnSc1	monarchie
oficiální	oficiální	k2eAgFnSc1d1	oficiální
název	název	k1gInSc4	název
Rakouské	rakouský	k2eAgNnSc4d1	rakouské
císařství	císařství	k1gNnSc4	císařství
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1867	[number]	k4	1867
se	se	k3xPyFc4	se
mocnářství	mocnářství	k1gNnSc2	mocnářství
nazývalo	nazývat	k5eAaImAgNnS	nazývat
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1	Rakousko-Uhersko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
zániku	zánik	k1gInSc6	zánik
Rakouska-Uherska	Rakouska-Uhersk	k1gInSc2	Rakouska-Uhersk
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
Československo	Československo	k1gNnSc1	Československo
jako	jako	k8xS	jako
unitární	unitární	k2eAgInSc1d1	unitární
stát	stát	k1gInSc1	stát
s	s	k7c7	s
republikánským	republikánský	k2eAgNnSc7d1	republikánské
zřízením	zřízení	k1gNnSc7	zřízení
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
bylo	být	k5eAaImAgNnS	být
území	území	k1gNnSc1	území
současné	současný	k2eAgFnSc2d1	současná
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
okupováno	okupován	k2eAgNnSc1d1	okupováno
německou	německý	k2eAgFnSc7d1	německá
armádou	armáda	k1gFnSc7	armáda
a	a	k8xC	a
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
loutkový	loutkový	k2eAgInSc1d1	loutkový
protektorát	protektorát	k1gInSc1	protektorát
Čechy	Čechy	k1gFnPc1	Čechy
a	a	k8xC	a
Morava	Morava	k1gFnSc1	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Československo	Československo	k1gNnSc1	Československo
bylo	být	k5eAaImAgNnS	být
obnoveno	obnovit	k5eAaPmNgNnS	obnovit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
mělo	mít	k5eAaImAgNnS	mít
oficiální	oficiální	k2eAgInSc4d1	oficiální
název	název	k1gInSc4	název
Československá	československý	k2eAgFnSc1d1	Československá
socialistická	socialistický	k2eAgFnSc1d1	socialistická
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
federalizována	federalizován	k2eAgFnSc1d1	federalizována
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
tohoto	tento	k3xDgInSc2	tento
procesu	proces	k1gInSc2	proces
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
Česká	český	k2eAgFnSc1d1	Česká
socialistická	socialistický	k2eAgFnSc1d1	socialistická
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
formálně	formálně	k6eAd1	formálně
svrchovaný	svrchovaný	k2eAgInSc1d1	svrchovaný
národní	národní	k2eAgInSc1d1	národní
stát	stát	k1gInSc1	stát
<g/>
,	,	kIx,	,
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
práva	právo	k1gNnSc2	právo
přímý	přímý	k2eAgMnSc1d1	přímý
předchůdce	předchůdce	k1gMnSc1	předchůdce
současné	současný	k2eAgFnSc2d1	současná
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
oficiální	oficiální	k2eAgInSc1d1	oficiální
název	název	k1gInSc1	název
nese	nést	k5eAaImIp3nS	nést
český	český	k2eAgInSc1d1	český
stát	stát	k1gInSc1	stát
od	od	k7c2	od
6	[number]	k4	6
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
po	po	k7c6	po
pádu	pád	k1gInSc6	pád
komunistického	komunistický	k2eAgInSc2d1	komunistický
režimu	režim	k1gInSc2	režim
vypuštěno	vypuštěn	k2eAgNnSc1d1	vypuštěno
z	z	k7c2	z
názvu	název	k1gInSc2	název
slovo	slovo	k1gNnSc1	slovo
"	"	kIx"	"
<g/>
socialistická	socialistický	k2eAgFnSc1d1	socialistická
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Československo	Československo	k1gNnSc1	Československo
získalo	získat	k5eAaPmAgNnS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
nový	nový	k2eAgInSc4d1	nový
oficiální	oficiální	k2eAgInSc4d1	oficiální
název	název	k1gInSc4	název
<g/>
:	:	kIx,	:
Česká	český	k2eAgFnSc1d1	Česká
a	a	k8xC	a
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
Federativní	federativní	k2eAgFnSc1d1	federativní
Republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
nato	nato	k6eAd1	nato
<g/>
,	,	kIx,	,
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
však	však	k9	však
zaniklo	zaniknout	k5eAaPmAgNnS	zaniknout
a	a	k8xC	a
místo	místo	k7c2	místo
něj	on	k3xPp3gInSc2	on
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
dva	dva	k4xCgInPc1	dva
nové	nový	k2eAgInPc1d1	nový
státy	stát	k1gInPc1	stát
<g/>
,	,	kIx,	,
Česko	Česko	k1gNnSc1	Česko
a	a	k8xC	a
Slovensko	Slovensko	k1gNnSc1	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Subjektem	subjekt	k1gInSc7	subjekt
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
práva	právo	k1gNnSc2	právo
se	se	k3xPyFc4	se
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
stala	stát	k5eAaPmAgFnS	stát
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zánikem	zánik	k1gInSc7	zánik
federace	federace	k1gFnSc2	federace
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1993	[number]	k4	1993
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
Ústava	ústava	k1gFnSc1	ústava
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
jejíž	jejíž	k3xOyRp3gFnSc2	jejíž
preambule	preambule	k1gFnSc2	preambule
navazuje	navazovat	k5eAaImIp3nS	navazovat
nový	nový	k2eAgInSc1d1	nový
stát	stát	k1gInSc1	stát
na	na	k7c4	na
tradice	tradice	k1gFnPc4	tradice
státnosti	státnost	k1gFnSc2	státnost
Československa	Československo	k1gNnSc2	Československo
a	a	k8xC	a
bývalých	bývalý	k2eAgFnPc2d1	bývalá
zemí	zem	k1gFnPc2	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Existenci	existence	k1gFnSc4	existence
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
jako	jako	k8xS	jako
subjektu	subjekt	k1gInSc2	subjekt
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
práva	právo	k1gNnSc2	právo
uznávají	uznávat	k5eAaImIp3nP	uznávat
všechny	všechen	k3xTgInPc1	všechen
státy	stát	k1gInPc1	stát
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
8	[number]	k4	8
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byly	být	k5eAaImAgFnP	být
navázány	navázán	k2eAgInPc4d1	navázán
diplomatické	diplomatický	k2eAgInPc4d1	diplomatický
vztahy	vztah	k1gInPc4	vztah
<g/>
,	,	kIx,	,
nebyla	být	k5eNaImAgFnS	být
uznávána	uznáván	k2eAgFnSc1d1	uznávána
jako	jako	k8xC	jako
samostatný	samostatný	k2eAgInSc1d1	samostatný
stát	stát	k1gInSc1	stát
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
Lichtenštejnska	Lichtenštejnsko	k1gNnSc2	Lichtenštejnsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lichtenštejnsko	Lichtenštejnsko	k1gNnSc1	Lichtenštejnsko
kladlo	klást	k5eAaImAgNnS	klást
jako	jako	k9	jako
předběžnou	předběžný	k2eAgFnSc4d1	předběžná
podmínku	podmínka	k1gFnSc4	podmínka
uznání	uznání	k1gNnSc3	uznání
a	a	k8xC	a
navázání	navázání	k1gNnSc3	navázání
diplomatických	diplomatický	k2eAgInPc2d1	diplomatický
styků	styk	k1gInPc2	styk
český	český	k2eAgInSc4d1	český
souhlas	souhlas	k1gInSc4	souhlas
dvoustranně	dvoustranně	k6eAd1	dvoustranně
jednat	jednat	k5eAaImF	jednat
o	o	k7c6	o
tématech	téma	k1gNnPc6	téma
majetkového	majetkový	k2eAgInSc2d1	majetkový
charakteru	charakter	k1gInSc2	charakter
(	(	kIx(	(
<g/>
majetkové	majetkový	k2eAgInPc1d1	majetkový
spory	spor	k1gInPc1	spor
existovaly	existovat	k5eAaImAgInP	existovat
již	již	k6eAd1	již
mezi	mezi	k7c7	mezi
Lichtenštejnskem	Lichtenštejnsko	k1gNnSc7	Lichtenštejnsko
a	a	k8xC	a
Československem	Československo	k1gNnSc7	Československo
od	od	k7c2	od
vzniku	vznik	k1gInSc2	vznik
Československa	Československo	k1gNnSc2	Československo
<g/>
,	,	kIx,	,
nově	nově	k6eAd1	nově
šlo	jít	k5eAaImAgNnS	jít
i	i	k9	i
o	o	k7c6	o
vyvlastnění	vyvlastnění	k1gNnSc6	vyvlastnění
majetku	majetek	k1gInSc2	majetek
rodu	rod	k1gInSc2	rod
Lichtenštejnů	Lichtenštejn	k1gMnPc2	Lichtenštejn
podle	podle	k7c2	podle
Benešových	Benešových	k2eAgInPc2d1	Benešových
dekretů	dekret	k1gInPc2	dekret
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lichtenštejnská	lichtenštejnský	k2eAgFnSc1d1	lichtenštejnská
snaha	snaha	k1gFnSc1	snaha
zabránit	zabránit	k5eAaPmF	zabránit
přijetí	přijetí	k1gNnSc3	přijetí
Česka	Česko	k1gNnSc2	Česko
do	do	k7c2	do
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
organizací	organizace	k1gFnPc2	organizace
nebyla	být	k5eNaImAgFnS	být
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Název	název	k1gInSc4	název
a	a	k8xC	a
státní	státní	k2eAgInPc4d1	státní
symboly	symbol	k1gInPc4	symbol
==	==	k?	==
</s>
</p>
<p>
<s>
Oficiálním	oficiální	k2eAgInSc7d1	oficiální
názvem	název	k1gInSc7	název
státu	stát	k1gInSc2	stát
podle	podle	k7c2	podle
ústavy	ústava	k1gFnSc2	ústava
je	být	k5eAaImIp3nS	být
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
<g/>
;	;	kIx,	;
jednoslovný	jednoslovný	k2eAgInSc1d1	jednoslovný
název	název	k1gInSc1	název
Česko	Česko	k1gNnSc1	Česko
se	se	k3xPyFc4	se
v	v	k7c6	v
ústavě	ústava	k1gFnSc6	ústava
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
(	(	kIx(	(
<g/>
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
se	se	k3xPyFc4	se
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
nevyskytovalo	vyskytovat	k5eNaImAgNnS	vyskytovat
Československo	Československo	k1gNnSc1	Československo
–	–	k?	–
zeměpisný	zeměpisný	k2eAgInSc1d1	zeměpisný
název	název	k1gInSc1	název
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
nutně	nutně	k6eAd1	nutně
součástí	součást	k1gFnSc7	součást
ústavy	ústava	k1gFnSc2	ústava
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
však	však	k9	však
součástí	součást	k1gFnSc7	součást
oficiální	oficiální	k2eAgFnSc2d1	oficiální
databáze	databáze	k1gFnSc2	databáze
OSN	OSN	kA	OSN
coby	coby	k?	coby
jednoslovný	jednoslovný	k2eAgInSc4d1	jednoslovný
název	název	k1gInSc4	název
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2016	[number]	k4	2016
vláda	vláda	k1gFnSc1	vláda
oficiálně	oficiálně	k6eAd1	oficiálně
schválila	schválit	k5eAaPmAgFnS	schválit
též	též	k6eAd1	též
překlady	překlad	k1gInPc4	překlad
zeměpisného	zeměpisný	k2eAgInSc2d1	zeměpisný
názvu	název	k1gInSc2	název
Česko	Česko	k1gNnSc4	Česko
do	do	k7c2	do
angličtiny	angličtina	k1gFnSc2	angličtina
(	(	kIx(	(
<g/>
Czechia	Czechia	k1gFnSc1	Czechia
<g/>
)	)	kIx)	)
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
světových	světový	k2eAgInPc2d1	světový
jazyků	jazyk	k1gInPc2	jazyk
(	(	kIx(	(
<g/>
fr.	fr.	k?	fr.
Tchéquie	Tchéquie	k1gFnSc1	Tchéquie
<g/>
,	,	kIx,	,
šp	šp	k?	šp
<g/>
.	.	kIx.	.
</s>
<s>
Chequia	Chequia	k1gFnSc1	Chequia
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Část	část	k1gFnSc1	část
veřejnosti	veřejnost	k1gFnSc2	veřejnost
slovo	slovo	k1gNnSc1	slovo
Česko	Česko	k1gNnSc4	Česko
odmítá	odmítat	k5eAaImIp3nS	odmítat
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
nezvyk	nezvyk	k1gInSc4	nezvyk
–	–	k?	–
obdobně	obdobně	k6eAd1	obdobně
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
byl	být	k5eAaImAgMnS	být
zpočátku	zpočátku	k6eAd1	zpočátku
odmítán	odmítán	k2eAgMnSc1d1	odmítán
např.	např.	kA	např.
i	i	k9	i
název	název	k1gInSc1	název
Československo	Československo	k1gNnSc1	Československo
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
užití	užití	k1gNnSc1	užití
výrazu	výraz	k1gInSc2	výraz
Česko	Česko	k1gNnSc1	Česko
je	být	k5eAaImIp3nS	být
doloženo	doložit	k5eAaPmNgNnS	doložit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1704	[number]	k4	1704
jako	jako	k8xS	jako
zeměpisné	zeměpisný	k2eAgNnSc4d1	zeměpisné
označení	označení	k1gNnSc4	označení
zahrnující	zahrnující	k2eAgFnPc1d1	zahrnující
všechny	všechen	k3xTgFnPc1	všechen
České	český	k2eAgFnPc1d1	Česká
země	zem	k1gFnPc1	zem
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1777	[number]	k4	1777
pak	pak	k6eAd1	pak
jako	jako	k8xS	jako
synonymum	synonymum	k1gNnSc4	synonymum
ke	k	k7c3	k
slovu	slovo	k1gNnSc3	slovo
Čechy	Čech	k1gMnPc4	Čech
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
národního	národní	k2eAgNnSc2d1	národní
obrození	obrození	k1gNnSc2	obrození
se	se	k3xPyFc4	se
používaly	používat	k5eAaImAgFnP	používat
i	i	k9	i
tvary	tvar	k1gInPc7	tvar
češský	češský	k1gMnSc1	češský
a	a	k8xC	a
Češsko	Češsko	k1gNnSc1	Češsko
odvozené	odvozený	k2eAgNnSc1d1	odvozené
ze	z	k7c2	z
základu	základ	k1gInSc2	základ
"	"	kIx"	"
<g/>
Čechy	Čechy	k1gFnPc1	Čechy
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nesprávně	správně	k6eNd1	správně
se	se	k3xPyFc4	se
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
užíval	užívat	k5eAaImAgInS	užívat
i	i	k9	i
tvar	tvar	k1gInSc1	tvar
čechský	čechský	k2eAgInSc1d1	čechský
<g/>
.	.	kIx.	.
</s>
<s>
Vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
vypadnutí	vypadnutí	k1gNnSc4	vypadnutí
jednoho	jeden	k4xCgMnSc2	jeden
"	"	kIx"	"
<g/>
s	s	k7c7	s
<g/>
"	"	kIx"	"
z	z	k7c2	z
termínu	termín	k1gInSc2	termín
měla	mít	k5eAaImAgFnS	mít
také	také	k9	také
jazyková	jazykový	k2eAgFnSc1d1	jazyková
úspornost	úspornost	k1gFnSc1	úspornost
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
výraz	výraz	k1gInSc1	výraz
Česko	Česko	k1gNnSc1	Česko
opět	opět	k6eAd1	opět
objevuje	objevovat	k5eAaImIp3nS	objevovat
též	též	k9	též
jako	jako	k9	jako
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
všechny	všechen	k3xTgFnPc4	všechen
České	český	k2eAgFnPc4d1	Česká
země	zem	k1gFnPc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
významu	význam	k1gInSc6	význam
jej	on	k3xPp3gInSc4	on
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
začal	začít	k5eAaPmAgMnS	začít
prosazovat	prosazovat	k5eAaImF	prosazovat
moravský	moravský	k2eAgMnSc1d1	moravský
jazykovědec	jazykovědec	k1gMnSc1	jazykovědec
František	František	k1gMnSc1	František
Trávníček	Trávníček	k1gMnSc1	Trávníček
<g/>
.	.	kIx.	.
</s>
<s>
Slovník	slovník	k1gInSc1	slovník
spisovného	spisovný	k2eAgInSc2d1	spisovný
jazyka	jazyk	k1gInSc2	jazyk
českého	český	k2eAgInSc2d1	český
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
jej	on	k3xPp3gMnSc4	on
uvádí	uvádět	k5eAaImIp3nS	uvádět
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
významech	význam	k1gInPc6	význam
a	a	k8xC	a
jako	jako	k9	jako
zastaralý	zastaralý	k2eAgInSc1d1	zastaralý
<g/>
,	,	kIx,	,
Slovník	slovník	k1gInSc1	slovník
spisovné	spisovný	k2eAgFnSc2d1	spisovná
češtiny	čeština	k1gFnSc2	čeština
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1978	[number]	k4	1978
již	jenž	k3xRgFnSc4	jenž
archaičnost	archaičnost	k1gFnSc4	archaičnost
konstatuje	konstatovat	k5eAaBmIp3nS	konstatovat
jen	jen	k9	jen
u	u	k7c2	u
prvního	první	k4xOgInSc2	první
významu	význam	k1gInSc2	význam
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
ve	v	k7c6	v
významu	význam	k1gInSc6	význam
označení	označení	k1gNnSc1	označení
české	český	k2eAgFnSc2d1	Česká
části	část	k1gFnSc2	část
federace	federace	k1gFnSc2	federace
stylovou	stylový	k2eAgFnSc4d1	stylová
příznačnost	příznačnost	k1gFnSc4	příznačnost
neuvádí	uvádět	k5eNaImIp3nS	uvádět
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1993	[number]	k4	1993
jej	on	k3xPp3gMnSc4	on
Český	český	k2eAgInSc1d1	český
úřad	úřad	k1gInSc1	úřad
zeměměřický	zeměměřický	k2eAgInSc1d1	zeměměřický
a	a	k8xC	a
katastrální	katastrální	k2eAgInSc1d1	katastrální
na	na	k7c6	na
základě	základ	k1gInSc6	základ
pověření	pověření	k1gNnSc2	pověření
vlády	vláda	k1gFnSc2	vláda
určil	určit	k5eAaPmAgInS	určit
jako	jako	k9	jako
jednoslovné	jednoslovný	k2eAgNnSc4d1	jednoslovné
označení	označení	k1gNnSc4	označení
čerstvě	čerstvě	k6eAd1	čerstvě
osamostatněné	osamostatněný	k2eAgFnSc2d1	osamostatněná
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
po	po	k7c6	po
vášnivých	vášnivý	k2eAgInPc6d1	vášnivý
sporech	spor	k1gInPc6	spor
<g/>
,	,	kIx,	,
po	po	k7c6	po
podpoře	podpora	k1gFnSc6	podpora
Českou	český	k2eAgFnSc7d1	Česká
geografickou	geografický	k2eAgFnSc7d1	geografická
společností	společnost	k1gFnSc7	společnost
a	a	k8xC	a
navzdory	navzdory	k7c3	navzdory
odporu	odpor	k1gInSc3	odpor
prezidenta	prezident	k1gMnSc2	prezident
Havla	Havel	k1gMnSc2	Havel
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
osobností	osobnost	k1gFnPc2	osobnost
se	se	k3xPyFc4	se
termín	termín	k1gInSc1	termín
výrazně	výrazně	k6eAd1	výrazně
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
<g/>
,	,	kIx,	,
zachoval	zachovat	k5eAaPmAgInS	zachovat
si	se	k3xPyFc3	se
však	však	k9	však
<g />
.	.	kIx.	.
</s>
<s>
jistou	jistý	k2eAgFnSc4d1	jistá
míru	míra	k1gFnSc4	míra
kontroverznosti	kontroverznost	k1gFnSc2	kontroverznost
a	a	k8xC	a
někteří	některý	k3yIgMnPc1	některý
jej	on	k3xPp3gMnSc4	on
pociťují	pociťovat	k5eAaImIp3nP	pociťovat
jako	jako	k8xC	jako
novotvar	novotvar	k1gInSc1	novotvar
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgInSc1d1	státní
symboly	symbol	k1gInPc7	symbol
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
jsou	být	k5eAaImIp3nP	být
velký	velký	k2eAgInSc4d1	velký
a	a	k8xC	a
malý	malý	k2eAgInSc4d1	malý
státní	státní	k2eAgInSc4d1	státní
znak	znak	k1gInSc4	znak
<g/>
,	,	kIx,	,
státní	státní	k2eAgFnSc1d1	státní
vlajka	vlajka	k1gFnSc1	vlajka
(	(	kIx(	(
<g/>
republika	republika	k1gFnSc1	republika
po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
federace	federace	k1gFnSc2	federace
převzala	převzít	k5eAaPmAgFnS	převzít
i	i	k9	i
původní	původní	k2eAgFnSc4d1	původní
vlajku	vlajka	k1gFnSc4	vlajka
Československa	Československo	k1gNnSc2	Československo
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
Slovensko	Slovensko	k1gNnSc1	Slovensko
nemělo	mít	k5eNaImAgNnS	mít
zájem	zájem	k1gInSc4	zájem
tuto	tento	k3xDgFnSc4	tento
vlajku	vlajka	k1gFnSc4	vlajka
použít	použít	k5eAaPmF	použít
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
standarta	standarta	k1gFnSc1	standarta
prezidenta	prezident	k1gMnSc2	prezident
<g/>
,	,	kIx,	,
státní	státní	k2eAgFnSc4d1	státní
pečeť	pečeť	k1gFnSc4	pečeť
<g/>
,	,	kIx,	,
státní	státní	k2eAgFnPc4d1	státní
barvy	barva	k1gFnPc4	barva
a	a	k8xC	a
státní	státní	k2eAgFnSc1d1	státní
hymna	hymna	k1gFnSc1	hymna
Kde	kde	k6eAd1	kde
domov	domov	k1gInSc1	domov
můj	můj	k3xOp1gInSc1	můj
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgInPc1d1	státní
symboly	symbol	k1gInPc1	symbol
poukazují	poukazovat	k5eAaImIp3nP	poukazovat
na	na	k7c4	na
tradice	tradice	k1gFnPc4	tradice
středověkého	středověký	k2eAgInSc2d1	středověký
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
(	(	kIx(	(
<g/>
znak	znak	k1gInSc1	znak
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
husitského	husitský	k2eAgNnSc2d1	husitské
hnutí	hnutí	k1gNnSc2	hnutí
(	(	kIx(	(
<g/>
heslo	heslo	k1gNnSc1	heslo
na	na	k7c6	na
prezidentské	prezidentský	k2eAgFnSc6d1	prezidentská
standartě	standarta	k1gFnSc6	standarta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
národního	národní	k2eAgNnSc2d1	národní
obrození	obrození	k1gNnSc2	obrození
(	(	kIx(	(
<g/>
hymna	hymna	k1gFnSc1	hymna
<g/>
)	)	kIx)	)
a	a	k8xC	a
Československa	Československo	k1gNnSc2	Československo
(	(	kIx(	(
<g/>
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Původní	původní	k2eAgNnSc1d1	původní
osídlení	osídlení	k1gNnSc1	osídlení
===	===	k?	===
</s>
</p>
<p>
<s>
Území	území	k1gNnSc1	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
bylo	být	k5eAaImAgNnS	být
člověkem	člověk	k1gMnSc7	člověk
osídleno	osídlen	k2eAgNnSc4d1	osídleno
již	již	k6eAd1	již
před	před	k7c4	před
asi	asi	k9	asi
750	[number]	k4	750
000	[number]	k4	000
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
osídlení	osídlení	k1gNnSc4	osídlení
území	území	k1gNnSc2	území
Česka	Česko	k1gNnSc2	Česko
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
od	od	k7c2	od
28	[number]	k4	28
000	[number]	k4	000
let	léto	k1gNnPc2	léto
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
svědčí	svědčit	k5eAaImIp3nS	svědčit
řada	řada	k1gFnSc1	řada
archeologických	archeologický	k2eAgInPc2d1	archeologický
nálezů	nález	k1gInPc2	nález
(	(	kIx(	(
<g/>
kultura	kultura	k1gFnSc1	kultura
s	s	k7c7	s
nálevkovitými	nálevkovitý	k2eAgInPc7d1	nálevkovitý
poháry	pohár	k1gInPc7	pohár
<g/>
,	,	kIx,	,
únětická	únětický	k2eAgFnSc1d1	únětická
kultura	kultura	k1gFnSc1	kultura
<g/>
,	,	kIx,	,
lengyelská	lengyelský	k2eAgFnSc1d1	lengyelská
kultura	kultura	k1gFnSc1	kultura
ad	ad	k7c4	ad
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
3	[number]	k4	3
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
obývali	obývat	k5eAaImAgMnP	obývat
tuto	tento	k3xDgFnSc4	tento
oblast	oblast	k1gFnSc4	oblast
Keltové	Kelt	k1gMnPc1	Kelt
(	(	kIx(	(
<g/>
Bojové	bojový	k2eAgFnSc2d1	bojová
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
n.	n.	k?	n.
l.	l.	k?	l.
přicházely	přicházet	k5eAaImAgFnP	přicházet
kmeny	kmen	k1gInPc4	kmen
Germánů	Germán	k1gMnPc2	Germán
(	(	kIx(	(
<g/>
Markomani	Markoman	k1gMnPc1	Markoman
a	a	k8xC	a
Kvádové	Kvád	k1gMnPc1	Kvád
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
historicky	historicky	k6eAd1	historicky
doloženým	doložený	k2eAgMnSc7d1	doložený
panovníkem	panovník	k1gMnSc7	panovník
na	na	k7c6	na
českém	český	k2eAgNnSc6d1	české
území	území	k1gNnSc6	území
byl	být	k5eAaImAgMnS	být
markomanský	markomanský	k2eAgMnSc1d1	markomanský
král	král	k1gMnSc1	král
Marobud	Marobud	k1gMnSc1	Marobud
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Česka	Česko	k1gNnSc2	Česko
objevovali	objevovat	k5eAaImAgMnP	objevovat
první	první	k4xOgMnPc1	první
Slované	Slovan	k1gMnPc1	Slovan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
vytvořily	vytvořit	k5eAaPmAgInP	vytvořit
slovanské	slovanský	k2eAgInPc1d1	slovanský
kmeny	kmen	k1gInPc1	kmen
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
franského	franský	k2eAgMnSc2d1	franský
kupce	kupec	k1gMnSc2	kupec
Sáma	Sámo	k1gMnSc2	Sámo
tzv.	tzv.	kA	tzv.
Sámovu	Sámův	k2eAgFnSc4d1	Sámova
říši	říše	k1gFnSc4	říše
(	(	kIx(	(
<g/>
asi	asi	k9	asi
623	[number]	k4	623
<g/>
–	–	k?	–
<g/>
659	[number]	k4	659
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
však	však	k9	však
spíše	spíše	k9	spíše
o	o	k7c4	o
nadkmenový	nadkmenový	k2eAgInSc4d1	nadkmenový
svaz	svaz	k1gInSc4	svaz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
830	[number]	k4	830
<g/>
–	–	k?	–
<g/>
833	[number]	k4	833
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
,	,	kIx,	,
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
,	,	kIx,	,
v	v	k7c6	v
severním	severní	k2eAgNnSc6d1	severní
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
a	a	k8xC	a
na	na	k7c6	na
západním	západní	k2eAgInSc6d1	západní
Zakarpatsku	Zakarpatsek	k1gInSc6	Zakarpatsek
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
pod	pod	k7c7	pod
nadvládou	nadvláda	k1gFnSc7	nadvláda
dynastie	dynastie	k1gFnSc1	dynastie
Mojmírovců	Mojmírovec	k1gMnPc2	Mojmírovec
Velkomoravská	velkomoravský	k2eAgFnSc1d1	Velkomoravská
říše	říše	k1gFnSc1	říše
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
postupně	postupně	k6eAd1	postupně
zahrnula	zahrnout	k5eAaPmAgFnS	zahrnout
i	i	k9	i
Čechy	Čechy	k1gFnPc1	Čechy
(	(	kIx(	(
<g/>
890	[number]	k4	890
<g/>
–	–	k?	–
<g/>
894	[number]	k4	894
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Slezsko	Slezsko	k1gNnSc4	Slezsko
<g/>
,	,	kIx,	,
Lužici	Lužice	k1gFnSc4	Lužice
<g/>
,	,	kIx,	,
Malopolsko	Malopolsko	k1gNnSc4	Malopolsko
a	a	k8xC	a
zbytek	zbytek	k1gInSc4	zbytek
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
<g/>
.	.	kIx.	.
</s>
<s>
Velkomoravská	velkomoravský	k2eAgFnSc1d1	Velkomoravská
říše	říše	k1gFnSc1	říše
byla	být	k5eAaImAgFnS	být
již	již	k6eAd1	již
plnohodnotným	plnohodnotný	k2eAgInSc7d1	plnohodnotný
státním	státní	k2eAgInSc7d1	státní
útvarem	útvar	k1gInSc7	útvar
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
christianizována	christianizován	k2eAgFnSc1d1	christianizována
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
byzantské	byzantský	k2eAgFnSc2d1	byzantská
misie	misie	k1gFnSc2	misie
Cyrila	Cyril	k1gMnSc2	Cyril
a	a	k8xC	a
Metoděje	Metoděj	k1gMnSc2	Metoděj
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
pozval	pozvat	k5eAaPmAgMnS	pozvat
panovník	panovník	k1gMnSc1	panovník
Rostislav	Rostislav	k1gMnSc1	Rostislav
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
jeho	jeho	k3xOp3gMnSc2	jeho
následníka	následník	k1gMnSc2	následník
Svatopluka	Svatopluk	k1gMnSc2	Svatopluk
I.	I.	kA	I.
říše	říš	k1gFnPc4	říš
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
maxima	maxima	k1gFnSc1	maxima
své	svůj	k3xOyFgFnSc2	svůj
moci	moc	k1gFnSc2	moc
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
však	však	k9	však
přišel	přijít	k5eAaPmAgInS	přijít
rychlý	rychlý	k2eAgInSc1d1	rychlý
pád	pád	k1gInSc1	pád
<g/>
.	.	kIx.	.
</s>
<s>
Čechy	Čechy	k1gFnPc1	Čechy
se	se	k3xPyFc4	se
od	od	k7c2	od
Velké	velký	k2eAgFnSc2d1	velká
Moravy	Morava	k1gFnSc2	Morava
odtrhly	odtrhnout	k5eAaPmAgFnP	odtrhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
894	[number]	k4	894
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
906	[number]	k4	906
nebo	nebo	k8xC	nebo
907	[number]	k4	907
byla	být	k5eAaImAgFnS	být
rozvrácena	rozvrácen	k2eAgFnSc1d1	rozvrácena
tehdy	tehdy	k6eAd1	tehdy
ještě	ještě	k9	ještě
kočovnými	kočovný	k2eAgMnPc7d1	kočovný
Maďary	Maďar	k1gMnPc7	Maďar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Středověk	středověk	k1gInSc1	středověk
===	===	k?	===
</s>
</p>
<p>
<s>
Počátky	počátek	k1gInPc4	počátek
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
spadají	spadat	k5eAaPmIp3nP	spadat
do	do	k7c2	do
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
pokřtěn	pokřtěn	k2eAgMnSc1d1	pokřtěn
první	první	k4xOgMnSc1	první
doložený	doložený	k2eAgMnSc1d1	doložený
český	český	k2eAgMnSc1d1	český
kníže	kníže	k1gMnSc1	kníže
z	z	k7c2	z
dynastie	dynastie	k1gFnSc2	dynastie
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
Bořivoj	Bořivoj	k1gMnSc1	Bořivoj
I.	I.	kA	I.
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
10	[number]	k4	10
<g/>
.	.	kIx.	.
a	a	k8xC	a
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
stát	stát	k1gInSc1	stát
konsolidoval	konsolidovat	k5eAaBmAgInS	konsolidovat
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
připojeno	připojit	k5eAaPmNgNnS	připojit
území	území	k1gNnSc1	území
Moravy	Morava	k1gFnSc2	Morava
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgNnSc1d1	české
knížectví	knížectví	k1gNnSc1	knížectví
postupně	postupně	k6eAd1	postupně
získalo	získat	k5eAaPmAgNnS	získat
znaky	znak	k1gInPc4	znak
víceméně	víceméně	k9	víceméně
autonomního	autonomní	k2eAgInSc2d1	autonomní
středověkého	středověký	k2eAgInSc2d1	středověký
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
součástí	součást	k1gFnSc7	součást
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
(	(	kIx(	(
<g/>
pražské	pražský	k2eAgNnSc1d1	Pražské
biskupství	biskupství	k1gNnSc1	biskupství
založeno	založit	k5eAaPmNgNnS	založit
roku	rok	k1gInSc2	rok
973	[number]	k4	973
<g/>
,	,	kIx,	,
hlavním	hlavní	k2eAgMnSc7d1	hlavní
národním	národní	k2eAgMnSc7d1	národní
světcem	světec	k1gMnSc7	světec
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
svatý	svatý	k2eAgMnSc1d1	svatý
Václav	Václav	k1gMnSc1	Václav
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
České	český	k2eAgNnSc1d1	české
království	království	k1gNnSc1	království
nicméně	nicméně	k8xC	nicméně
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
až	až	k9	až
roku	rok	k1gInSc2	rok
1198	[number]	k4	1198
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
německý	německý	k2eAgMnSc1d1	německý
král	král	k1gMnSc1	král
uznal	uznat	k5eAaPmAgMnS	uznat
český	český	k2eAgInSc4d1	český
královský	královský	k2eAgInSc4d1	královský
titul	titul	k1gInSc4	titul
jako	jako	k8xC	jako
dědičný	dědičný	k2eAgInSc1d1	dědičný
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
císař	císař	k1gMnSc1	císař
Fridrich	Fridrich	k1gMnSc1	Fridrich
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Štaufský	Štaufský	k2eAgInSc1d1	Štaufský
roku	rok	k1gInSc2	rok
1212	[number]	k4	1212
Zlatou	zlatý	k2eAgFnSc7d1	zlatá
bulou	bula	k1gFnSc7	bula
sicilskou	sicilský	k2eAgFnSc4d1	sicilská
vystavenou	vystavená	k1gFnSc4	vystavená
přemyslovskému	přemyslovský	k2eAgMnSc3d1	přemyslovský
králi	král	k1gMnSc3	král
Přemyslu	Přemysl	k1gMnSc3	Přemysl
Otakarovi	Otakar	k1gMnSc3	Otakar
I.	I.	kA	I.
včetně	včetně	k7c2	včetně
dalších	další	k2eAgNnPc2d1	další
privilegií	privilegium	k1gNnPc2	privilegium
Českého	český	k2eAgNnSc2d1	české
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgMnSc1d1	český
panovník	panovník	k1gMnSc1	panovník
byl	být	k5eAaImAgMnS	být
napříště	napříště	k6eAd1	napříště
osvobozen	osvobodit	k5eAaPmNgMnS	osvobodit
od	od	k7c2	od
všech	všecek	k3xTgFnPc2	všecek
povinností	povinnost	k1gFnPc2	povinnost
vůči	vůči	k7c3	vůči
Svaté	svatý	k2eAgFnSc3d1	svatá
říši	říš	k1gFnSc3	říš
římské	římský	k2eAgFnSc2d1	římská
až	až	k9	až
na	na	k7c4	na
účast	účast	k1gFnSc4	účast
na	na	k7c6	na
říšských	říšský	k2eAgInPc6d1	říšský
sněmech	sněm	k1gInPc6	sněm
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
I.	I.	kA	I.
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zejména	zejména	k9	zejména
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Přemysl	Přemysl	k1gMnSc1	Přemysl
Otakar	Otakar	k1gMnSc1	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
vybudovali	vybudovat	k5eAaPmAgMnP	vybudovat
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
državu	država	k1gFnSc4	država
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
sahala	sahat	k5eAaImAgFnS	sahat
až	až	k9	až
za	za	k7c2	za
Alpy	alpa	k1gFnSc2	alpa
a	a	k8xC	a
k	k	k7c3	k
Jaderskému	jaderský	k2eAgNnSc3d1	Jaderské
moři	moře	k1gNnSc3	moře
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
obrátil	obrátit	k5eAaPmAgInS	obrátit
svou	svůj	k3xOyFgFnSc4	svůj
pozornost	pozornost	k1gFnSc4	pozornost
také	také	k9	také
na	na	k7c4	na
sever	sever	k1gInSc4	sever
a	a	k8xC	a
východ	východ	k1gInSc4	východ
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
nabýt	nabýt	k5eAaPmF	nabýt
území	území	k1gNnSc4	území
přes	přes	k7c4	přes
Polsko	Polsko	k1gNnSc4	Polsko
až	až	k9	až
k	k	k7c3	k
Baltskému	baltský	k2eAgNnSc3d1	Baltské
moři	moře	k1gNnSc3	moře
a	a	k8xC	a
pro	pro	k7c4	pro
svého	svůj	k3xOyFgMnSc4	svůj
desetiletého	desetiletý	k2eAgMnSc4d1	desetiletý
syna	syn	k1gMnSc4	syn
Václava	Václav	k1gMnSc2	Václav
III	III	kA	III
<g/>
.	.	kIx.	.
získal	získat	k5eAaPmAgMnS	získat
dočasně	dočasně	k6eAd1	dočasně
uherskou	uherský	k2eAgFnSc4d1	uherská
královskou	královský	k2eAgFnSc4d1	královská
korunu	koruna	k1gFnSc4	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zavraždění	zavraždění	k1gNnSc6	zavraždění
Václava	Václav	k1gMnSc2	Václav
III	III	kA	III
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
České	český	k2eAgNnSc1d1	české
království	království	k1gNnSc1	království
ocitlo	ocitnout	k5eAaPmAgNnS	ocitnout
v	v	k7c6	v
chaosu	chaos	k1gInSc6	chaos
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
volba	volba	k1gFnSc1	volba
Jana	Jan	k1gMnSc2	Jan
Lucemburského	lucemburský	k2eAgMnSc2d1	lucemburský
českým	český	k2eAgMnSc7d1	český
králem	král	k1gMnSc7	král
umožnila	umožnit	k5eAaPmAgFnS	umožnit
nový	nový	k2eAgInSc4d1	nový
vzestup	vzestup	k1gInSc4	vzestup
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vyvrcholil	vyvrcholit	k5eAaPmAgInS	vyvrcholit
zejména	zejména	k9	zejména
za	za	k7c4	za
panování	panování	k1gNnSc4	panování
Janova	Janov	k1gInSc2	Janov
syna	syn	k1gMnSc2	syn
Karla	Karel	k1gMnSc2	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1316	[number]	k4	1316
<g/>
–	–	k?	–
<g/>
1378	[number]	k4	1378
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1319	[number]	k4	1319
<g/>
–	–	k?	–
<g/>
1329	[number]	k4	1329
byla	být	k5eAaImAgFnS	být
k	k	k7c3	k
Českému	český	k2eAgNnSc3d1	české
království	království	k1gNnSc3	království
připojena	připojen	k2eAgFnSc1d1	připojena
Horní	horní	k2eAgFnSc1d1	horní
Lužice	Lužice	k1gFnSc1	Lužice
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1335	[number]	k4	1335
také	také	k9	také
město	město	k1gNnSc1	město
Vratislav	Vratislava	k1gFnPc2	Vratislava
<g/>
,	,	kIx,	,
k	k	k7c3	k
němuž	jenž	k3xRgNnSc3	jenž
přiléhala	přiléhat	k5eAaImAgFnS	přiléhat
značná	značný	k2eAgFnSc1d1	značná
část	část	k1gFnSc1	část
Slezska	Slezsko	k1gNnSc2	Slezsko
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1348	[number]	k4	1348
bylo	být	k5eAaImAgNnS	být
načas	načas	k6eAd1	načas
připojeno	připojen	k2eAgNnSc1d1	připojeno
Braniborsko	Braniborsko	k1gNnSc1	Braniborsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Už	už	k6eAd1	už
v	v	k7c6	v
době	doba	k1gFnSc6	doba
vlády	vláda	k1gFnSc2	vláda
Karla	Karel	k1gMnSc2	Karel
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
lze	lze	k6eAd1	lze
spatřovat	spatřovat	k5eAaImF	spatřovat
počátky	počátek	k1gInPc4	počátek
českého	český	k2eAgNnSc2d1	české
reformního	reformní	k2eAgNnSc2d1	reformní
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
usilovalo	usilovat	k5eAaImAgNnS	usilovat
o	o	k7c4	o
prohloubení	prohloubení	k1gNnSc4	prohloubení
osobní	osobní	k2eAgFnSc2d1	osobní
zbožnosti	zbožnost	k1gFnSc2	zbožnost
a	a	k8xC	a
o	o	k7c4	o
nápravu	náprava	k1gFnSc4	náprava
zesvětštělé	zesvětštělý	k2eAgFnSc2d1	zesvětštělá
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
o	o	k7c4	o
obrodu	obroda	k1gFnSc4	obroda
celé	celý	k2eAgFnSc2d1	celá
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Náboženské	náboženský	k2eAgInPc1d1	náboženský
spory	spor	k1gInPc1	spor
vygradovaly	vygradovat	k5eAaPmAgInP	vygradovat
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Karlova	Karlův	k2eAgMnSc2d1	Karlův
syna	syn	k1gMnSc2	syn
Václava	Václav	k1gMnSc2	Václav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
upálení	upálení	k1gNnSc6	upálení
Mistra	mistr	k1gMnSc2	mistr
Jana	Jan	k1gMnSc2	Jan
Husa	Hus	k1gMnSc2	Hus
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1415	[number]	k4	1415
v	v	k7c6	v
německé	německý	k2eAgFnSc6d1	německá
Kostnici	Kostnice	k1gFnSc6	Kostnice
napětí	napětí	k1gNnSc2	napětí
mezi	mezi	k7c7	mezi
Husovými	Husův	k2eAgMnPc7d1	Husův
příznivci	příznivec	k1gMnPc7	příznivec
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc7	jeho
odpůrci	odpůrce	k1gMnPc7	odpůrce
přešlo	přejít	k5eAaPmAgNnS	přejít
v	v	k7c4	v
otevřené	otevřený	k2eAgNnSc4d1	otevřené
nepřátelství	nepřátelství	k1gNnSc4	nepřátelství
a	a	k8xC	a
události	událost	k1gFnPc1	událost
vyústily	vyústit	k5eAaPmAgFnP	vyústit
v	v	k7c4	v
husitské	husitský	k2eAgInPc4d1	husitský
války	válek	k1gInPc4	válek
<g/>
.	.	kIx.	.
</s>
<s>
Radikální	radikální	k2eAgMnPc1d1	radikální
husité	husita	k1gMnPc1	husita
založili	založit	k5eAaPmAgMnP	založit
město	město	k1gNnSc4	město
Tábor	tábor	k1gMnSc1	tábor
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
centrem	centr	k1gInSc7	centr
husitské	husitský	k2eAgFnSc2d1	husitská
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Žižka	Žižka	k1gMnSc1	Žižka
z	z	k7c2	z
Trocnova	Trocnov	k1gInSc2	Trocnov
a	a	k8xC	a
Prokop	Prokop	k1gMnSc1	Prokop
Holý	Holý	k1gMnSc1	Holý
poté	poté	k6eAd1	poté
porazili	porazit	k5eAaPmAgMnP	porazit
všechny	všechen	k3xTgFnPc4	všechen
čtyři	čtyři	k4xCgFnPc4	čtyři
křížové	křížový	k2eAgFnPc4d1	křížová
výpravy	výprava	k1gFnPc4	výprava
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Války	válka	k1gFnPc1	válka
byly	být	k5eAaImAgFnP	být
ukončeny	ukončit	k5eAaPmNgFnP	ukončit
dohodou	dohoda	k1gFnSc7	dohoda
mezi	mezi	k7c7	mezi
Basilejským	basilejský	k2eAgInSc7d1	basilejský
koncilem	koncil	k1gInSc7	koncil
a	a	k8xC	a
umírněnými	umírněný	k2eAgMnPc7d1	umírněný
husity	husita	k1gMnPc7	husita
tzv.	tzv.	kA	tzv.
Basilejskými	basilejský	k2eAgNnPc7d1	Basilejské
kompaktáty	kompaktáta	k1gNnPc7	kompaktáta
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1436	[number]	k4	1436
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
osobě	osoba	k1gFnSc6	osoba
Jiřího	Jiří	k1gMnSc2	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
si	se	k3xPyFc3	se
země	zem	k1gFnPc4	zem
dokonce	dokonce	k9	dokonce
zvolila	zvolit	k5eAaPmAgFnS	zvolit
umírněného	umírněný	k2eAgMnSc4d1	umírněný
husitu	husita	k1gMnSc4	husita
jako	jako	k8xS	jako
krále	král	k1gMnSc4	král
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
vnější	vnější	k2eAgInSc1d1	vnější
tlak	tlak	k1gInSc1	tlak
krále	král	k1gMnSc2	král
Jiřího	Jiří	k1gMnSc2	Jiří
přiměl	přimět	k5eAaPmAgInS	přimět
přepustit	přepustit	k5eAaPmF	přepustit
z	z	k7c2	z
taktických	taktický	k2eAgInPc2d1	taktický
důvodů	důvod	k1gInPc2	důvod
český	český	k2eAgInSc4d1	český
trůn	trůn	k1gInSc4	trůn
rodu	rod	k1gInSc2	rod
Jagellonců	Jagellonec	k1gMnPc2	Jagellonec
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
však	však	k9	však
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Moháče	Moháč	k1gInSc2	Moháč
(	(	kIx(	(
<g/>
1526	[number]	k4	1526
<g/>
)	)	kIx)	)
padl	padnout	k5eAaPmAgInS	padnout
druhý	druhý	k4xOgInSc1	druhý
Jagellonec	Jagellonec	k1gInSc1	Jagellonec
na	na	k7c6	na
českém	český	k2eAgInSc6d1	český
trůnu	trůn	k1gInSc6	trůn
<g/>
,	,	kIx,	,
Ludvík	Ludvík	k1gMnSc1	Ludvík
<g/>
,	,	kIx,	,
získali	získat	k5eAaPmAgMnP	získat
ho	on	k3xPp3gNnSc4	on
Habsburkové	Habsburk	k1gMnPc1	Habsburk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
s	s	k7c7	s
následující	následující	k2eAgFnSc7d1	následující
dynastií	dynastie	k1gFnSc7	dynastie
habsbursko-lotrinskou	habsburskootrinský	k2eAgFnSc7d1	habsbursko-lotrinská
<g/>
,	,	kIx,	,
vládli	vládnout	k5eAaImAgMnP	vládnout
zemi	zem	k1gFnSc4	zem
dalších	další	k2eAgInPc2d1	další
téměř	téměř	k6eAd1	téměř
400	[number]	k4	400
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Novověk	novověk	k1gInSc4	novověk
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1526	[number]	k4	1526
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
český	český	k2eAgInSc4d1	český
trůn	trůn	k1gInSc4	trůn
zvolen	zvolen	k2eAgMnSc1d1	zvolen
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
I.	I.	kA	I.
Habsburský	habsburský	k2eAgMnSc1d1	habsburský
a	a	k8xC	a
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
dynastie	dynastie	k1gFnSc1	dynastie
Habsburků	Habsburk	k1gMnPc2	Habsburk
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zemi	zem	k1gFnSc4	zem
postupně	postupně	k6eAd1	postupně
včlenila	včlenit	k5eAaPmAgFnS	včlenit
do	do	k7c2	do
habsburské	habsburský	k2eAgFnSc2d1	habsburská
monarchie	monarchie	k1gFnSc2	monarchie
<g/>
.	.	kIx.	.
</s>
<s>
Ferdinandův	Ferdinandův	k2eAgMnSc1d1	Ferdinandův
vnuk	vnuk	k1gMnSc1	vnuk
Rudolf	Rudolf	k1gMnSc1	Rudolf
II	II	kA	II
<g/>
.	.	kIx.	.
si	se	k3xPyFc3	se
ještě	ještě	k6eAd1	ještě
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
poslední	poslední	k2eAgInSc1d1	poslední
Habsburk	Habsburk	k1gInSc1	Habsburk
<g/>
)	)	kIx)	)
zvolil	zvolit	k5eAaPmAgInS	zvolit
Prahu	Praha	k1gFnSc4	Praha
za	za	k7c4	za
sídelní	sídelní	k2eAgNnSc4d1	sídelní
město	město	k1gNnSc4	město
a	a	k8xC	a
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
katolík	katolík	k1gMnSc1	katolík
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
částečně	částečně	k6eAd1	částečně
tolerantní	tolerantní	k2eAgMnSc1d1	tolerantní
vůči	vůči	k7c3	vůči
českému	český	k2eAgInSc3d1	český
protestantismu	protestantismus	k1gInSc3	protestantismus
(	(	kIx(	(
<g/>
Rudolfův	Rudolfův	k2eAgInSc1d1	Rudolfův
majestát	majestát	k1gInSc1	majestát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
však	však	k9	však
éra	éra	k1gFnSc1	éra
tolerance	tolerance	k1gFnSc1	tolerance
skončila	skončit	k5eAaPmAgFnS	skončit
a	a	k8xC	a
náboženské	náboženský	k2eAgNnSc4d1	náboženské
napětí	napětí	k1gNnSc4	napětí
znovu	znovu	k6eAd1	znovu
vzrostlo	vzrůst	k5eAaPmAgNnS	vzrůst
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1618	[number]	k4	1618
vypuklo	vypuknout	k5eAaPmAgNnS	vypuknout
proti	proti	k7c3	proti
katolickému	katolický	k2eAgMnSc3d1	katolický
panovníkovi	panovník	k1gMnSc3	panovník
ozbrojené	ozbrojený	k2eAgNnSc1d1	ozbrojené
povstání	povstání	k1gNnSc1	povstání
českých	český	k2eAgInPc2d1	český
protestantských	protestantský	k2eAgInPc2d1	protestantský
stavů	stav	k1gInPc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Defenestrace	defenestrace	k1gFnSc1	defenestrace
císařských	císařský	k2eAgMnPc2d1	císařský
místodržících	místodržící	k1gMnPc2	místodržící
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1618	[number]	k4	1618
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
i	i	k9	i
počátkem	počátkem	k7c2	počátkem
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Vojsko	vojsko	k1gNnSc1	vojsko
českých	český	k2eAgInPc2d1	český
stavů	stav	k1gInPc2	stav
bylo	být	k5eAaImAgNnS	být
roku	rok	k1gInSc2	rok
1620	[number]	k4	1620
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
na	na	k7c6	na
Bílé	bílý	k2eAgFnSc6d1	bílá
hoře	hora	k1gFnSc6	hora
poraženo	porazit	k5eAaPmNgNnS	porazit
a	a	k8xC	a
stavovští	stavovský	k2eAgMnPc1d1	stavovský
vůdcové	vůdce	k1gMnPc1	vůdce
byli	být	k5eAaImAgMnP	být
veřejně	veřejně	k6eAd1	veřejně
popraveni	popravit	k5eAaPmNgMnP	popravit
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Začala	začít	k5eAaPmAgFnS	začít
násilná	násilný	k2eAgFnSc1d1	násilná
rekatolizace	rekatolizace	k1gFnSc1	rekatolizace
českých	český	k2eAgMnPc2d1	český
protestantů	protestant	k1gMnPc2	protestant
<g/>
.	.	kIx.	.
</s>
<s>
Rozsáhlý	rozsáhlý	k2eAgInSc1d1	rozsáhlý
majetek	majetek	k1gInSc1	majetek
české	český	k2eAgFnSc2d1	Česká
exilové	exilový	k2eAgFnSc2d1	exilová
šlechty	šlechta	k1gFnSc2	šlechta
a	a	k8xC	a
inteligence	inteligence	k1gFnSc2	inteligence
připadl	připadnout	k5eAaPmAgMnS	připadnout
věrným	věrný	k2eAgMnSc7d1	věrný
stoupencům	stoupenec	k1gMnPc3	stoupenec
Habsburků	Habsburk	k1gInPc2	Habsburk
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
klesla	klesnout	k5eAaPmAgFnS	klesnout
populace	populace	k1gFnSc1	populace
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
a	a	k8xC	a
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
o	o	k7c4	o
necelých	celý	k2eNgNnPc2d1	necelé
30	[number]	k4	30
%	%	kIx~	%
na	na	k7c4	na
zhruba	zhruba	k6eAd1	zhruba
1,75	[number]	k4	1,75
milionu	milion	k4xCgInSc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1627	[number]	k4	1627
bylo	být	k5eAaImAgNnS	být
pro	pro	k7c4	pro
Čechy	Čechy	k1gFnPc4	Čechy
vydáno	vydat	k5eAaPmNgNnS	vydat
Obnovené	obnovený	k2eAgNnSc1d1	obnovené
zřízení	zřízení	k1gNnSc1	zřízení
zemské	zemský	k2eAgNnSc1d1	zemské
<g/>
,	,	kIx,	,
kterým	který	k3yIgMnPc3	který
Habsburkové	Habsburk	k1gMnPc1	Habsburk
získali	získat	k5eAaPmAgMnP	získat
český	český	k2eAgInSc4d1	český
královský	královský	k2eAgInSc4d1	královský
titul	titul	k1gInSc4	titul
dědičně	dědičně	k6eAd1	dědičně
<g/>
,	,	kIx,	,
za	za	k7c4	za
jediné	jediný	k2eAgNnSc4d1	jediné
povolené	povolený	k2eAgNnSc4d1	povolené
náboženství	náboženství	k1gNnSc4	náboženství
bylo	být	k5eAaImAgNnS	být
prohlášeno	prohlásit	k5eAaPmNgNnS	prohlásit
katolické	katolický	k2eAgNnSc1d1	katolické
a	a	k8xC	a
jazyk	jazyk	k1gMnSc1	jazyk
německý	německý	k2eAgMnSc1d1	německý
byl	být	k5eAaImAgInS	být
formálně	formálně	k6eAd1	formálně
zrovnoprávněn	zrovnoprávnit	k5eAaPmNgInS	zrovnoprávnit
s	s	k7c7	s
českým	český	k2eAgInSc7d1	český
<g/>
,	,	kIx,	,
fakticky	fakticky	k6eAd1	fakticky
byl	být	k5eAaImAgInS	být
ale	ale	k8xC	ale
upřednostněn	upřednostnit	k5eAaPmNgInS	upřednostnit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Správními	správní	k2eAgFnPc7d1	správní
reformami	reforma	k1gFnPc7	reforma
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1749	[number]	k4	1749
byly	být	k5eAaImAgFnP	být
země	zem	k1gFnPc1	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgNnSc1d1	české
de	de	k?	de
facto	facto	k1gNnSc1	facto
spojeny	spojen	k2eAgInPc1d1	spojen
s	s	k7c7	s
Rakouskem	Rakousko	k1gNnSc7	Rakousko
do	do	k7c2	do
unitárního	unitární	k2eAgInSc2d1	unitární
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
nadále	nadále	k6eAd1	nadále
se	se	k3xPyFc4	se
korunovali	korunovat	k5eAaBmAgMnP	korunovat
čeští	český	k2eAgMnPc1d1	český
králové	král	k1gMnPc1	král
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Českého	český	k2eAgNnSc2d1	české
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
hladomoru	hladomor	k1gInSc2	hladomor
v	v	k7c6	v
letech	let	k1gInPc6	let
1771	[number]	k4	1771
<g/>
–	–	k?	–
<g/>
1772	[number]	k4	1772
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
nejméně	málo	k6eAd3	málo
250	[number]	k4	250
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
rozsáhlým	rozsáhlý	k2eAgInPc3d1	rozsáhlý
nevolnickým	nevolnický	k2eAgInPc3d1	nevolnický
nepokojům	nepokoj	k1gInPc3	nepokoj
<g/>
.	.	kIx.	.
</s>
<s>
Náboženskou	náboženský	k2eAgFnSc4d1	náboženská
toleranci	tolerance	k1gFnSc4	tolerance
a	a	k8xC	a
zrušení	zrušení	k1gNnSc4	zrušení
nevolnictví	nevolnictví	k1gNnSc2	nevolnictví
přinesly	přinést	k5eAaPmAgFnP	přinést
až	až	k9	až
reformy	reforma	k1gFnPc1	reforma
Josefa	Josef	k1gMnSc2	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1781	[number]	k4	1781
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
rovněž	rovněž	k9	rovněž
zvýšil	zvýšit	k5eAaPmAgMnS	zvýšit
důraz	důraz	k1gInSc4	důraz
na	na	k7c4	na
centralizaci	centralizace	k1gFnSc4	centralizace
monarchie	monarchie	k1gFnSc2	monarchie
<g/>
.	.	kIx.	.
</s>
<s>
Této	tento	k3xDgFnSc3	tento
centralizaci	centralizace	k1gFnSc3	centralizace
napomáhalo	napomáhat	k5eAaBmAgNnS	napomáhat
preferování	preferování	k1gNnSc1	preferování
německého	německý	k2eAgInSc2d1	německý
jazyka	jazyk	k1gInSc2	jazyk
ve	v	k7c6	v
státní	státní	k2eAgFnSc6d1	státní
i	i	k8xC	i
církevní	církevní	k2eAgFnSc6d1	církevní
správě	správa	k1gFnSc6	správa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
odpověď	odpověď	k1gFnSc4	odpověď
na	na	k7c4	na
poněmčování	poněmčování	k1gNnSc4	poněmčování
národa	národ	k1gInSc2	národ
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
kultury	kultura	k1gFnSc2	kultura
se	se	k3xPyFc4	se
koncem	konec	k1gInSc7	konec
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
začalo	začít	k5eAaPmAgNnS	začít
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
vzmáhat	vzmáhat	k5eAaImF	vzmáhat
české	český	k2eAgNnSc4d1	české
národní	národní	k2eAgNnSc4d1	národní
obrození	obrození	k1gNnSc4	obrození
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
snaha	snaha	k1gFnSc1	snaha
o	o	k7c4	o
obnovu	obnova	k1gFnSc4	obnova
české	český	k2eAgFnSc2d1	Česká
kultury	kultura	k1gFnSc2	kultura
a	a	k8xC	a
jazyka	jazyk	k1gInSc2	jazyk
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
o	o	k7c4	o
získání	získání	k1gNnSc4	získání
politické	politický	k2eAgFnSc2d1	politická
moci	moc	k1gFnSc2	moc
stranami	strana	k1gFnPc7	strana
zastupujícími	zastupující	k2eAgInPc7d1	zastupující
zájmy	zájem	k1gInPc7	zájem
českého	český	k2eAgNnSc2d1	české
etnika	etnikum	k1gNnSc2	etnikum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1	Rakousko-Uhersko
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
za	za	k7c2	za
dlouhého	dlouhý	k2eAgNnSc2d1	dlouhé
panování	panování	k1gNnSc2	panování
císaře	císař	k1gMnSc2	císař
Františka	František	k1gMnSc2	František
Josefa	Josef	k1gMnSc2	Josef
I.	I.	kA	I.
nastal	nastat	k5eAaPmAgInS	nastat
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
významný	významný	k2eAgInSc4d1	významný
hospodářský	hospodářský	k2eAgInSc4d1	hospodářský
rozvoj	rozvoj	k1gInSc4	rozvoj
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
(	(	kIx(	(
<g/>
asi	asi	k9	asi
70	[number]	k4	70
%	%	kIx~	%
<g/>
)	)	kIx)	)
průmyslu	průmysl	k1gInSc2	průmysl
Rakouska-Uherska	Rakouska-Uhersko	k1gNnSc2	Rakouska-Uhersko
se	se	k3xPyFc4	se
soustředila	soustředit	k5eAaPmAgFnS	soustředit
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
včetně	včetně	k7c2	včetně
oblastí	oblast	k1gFnPc2	oblast
osídlených	osídlený	k2eAgFnPc2d1	osídlená
Němci	Němec	k1gMnPc7	Němec
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgFnPc1d1	Česká
politické	politický	k2eAgFnPc1d1	politická
elity	elita	k1gFnPc1	elita
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
František	František	k1gMnSc1	František
Palacký	Palacký	k1gMnSc1	Palacký
a	a	k8xC	a
František	František	k1gMnSc1	František
Ladislav	Ladislav	k1gMnSc1	Ladislav
Rieger	Rieger	k1gMnSc1	Rieger
<g/>
,	,	kIx,	,
dospěly	dochvít	k5eAaPmAgFnP	dochvít
k	k	k7c3	k
názoru	názor	k1gInSc3	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
federalizované	federalizovaný	k2eAgNnSc1d1	federalizované
a	a	k8xC	a
vcelku	vcelku	k6eAd1	vcelku
demokraticky	demokraticky	k6eAd1	demokraticky
uspořádané	uspořádaný	k2eAgNnSc1d1	uspořádané
Rakousko	Rakousko	k1gNnSc1	Rakousko
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
Předlitavsko	Předlitavsko	k1gNnSc4	Předlitavsko
<g/>
)	)	kIx)	)
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
nejvýhodnějším	výhodný	k2eAgInSc7d3	nejvýhodnější
životním	životní	k2eAgInSc7d1	životní
prostorem	prostor	k1gInSc7	prostor
pro	pro	k7c4	pro
český	český	k2eAgInSc4d1	český
národ	národ	k1gInSc4	národ
i	i	k8xC	i
jiné	jiný	k2eAgInPc1d1	jiný
slovanské	slovanský	k2eAgInPc1d1	slovanský
národy	národ	k1gInPc1	národ
Střední	střední	k2eAgFnSc2d1	střední
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc2d1	jižní
Evropy	Evropa	k1gFnSc2	Evropa
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
austroslavismus	austroslavismus	k1gInSc1	austroslavismus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mocnářství	mocnářství	k1gNnSc1	mocnářství
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
ochranou	ochrana	k1gFnSc7	ochrana
proti	proti	k7c3	proti
mocným	mocný	k2eAgInPc3d1	mocný
státům	stát	k1gInPc3	stát
na	na	k7c6	na
západě	západ	k1gInSc6	západ
i	i	k8xC	i
na	na	k7c6	na
východě	východ	k1gInSc6	východ
<g/>
,	,	kIx,	,
jmenovitě	jmenovitě	k6eAd1	jmenovitě
proti	proti	k7c3	proti
Německu	Německo	k1gNnSc3	Německo
a	a	k8xC	a
Ruskému	ruský	k2eAgNnSc3d1	ruské
imperiu	imperium	k1gNnSc3	imperium
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jazyková	jazykový	k2eAgNnPc1d1	jazykové
nařízení	nařízení	k1gNnPc1	nařízení
z	z	k7c2	z
dubna	duben	k1gInSc2	duben
1897	[number]	k4	1897
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zrovnoprávnila	zrovnoprávnit	k5eAaPmAgFnS	zrovnoprávnit
češtinu	čeština	k1gFnSc4	čeština
s	s	k7c7	s
němčinou	němčina	k1gFnSc7	němčina
<g/>
,	,	kIx,	,
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
pádu	pád	k1gInSc3	pád
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
k	k	k7c3	k
národnostním	národnostní	k2eAgInPc3d1	národnostní
nepokojům	nepokoj	k1gInPc3	nepokoj
mezi	mezi	k7c7	mezi
Čechy	Čech	k1gMnPc7	Čech
a	a	k8xC	a
Němci	Němec	k1gMnPc7	Němec
<g/>
.	.	kIx.	.
</s>
<s>
Volební	volební	k2eAgInSc1d1	volební
zákon	zákon	k1gInSc1	zákon
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1907	[number]	k4	1907
zavedl	zavést	k5eAaPmAgInS	zavést
všeobecné	všeobecný	k2eAgNnSc4d1	všeobecné
hlasovací	hlasovací	k2eAgNnSc4d1	hlasovací
právo	právo	k1gNnSc4	právo
pro	pro	k7c4	pro
muže	muž	k1gMnPc4	muž
<g/>
.	.	kIx.	.
</s>
<s>
Období	období	k1gNnSc1	období
všestranného	všestranný	k2eAgInSc2d1	všestranný
rozmachu	rozmach	k1gInSc2	rozmach
českého	český	k2eAgInSc2d1	český
národa	národ	k1gInSc2	národ
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
politiky	politika	k1gFnSc2	politika
<g/>
,	,	kIx,	,
hospodářství	hospodářství	k1gNnSc2	hospodářství
<g/>
,	,	kIx,	,
kultury	kultura	k1gFnSc2	kultura
a	a	k8xC	a
vzdělání	vzdělání	k1gNnSc2	vzdělání
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Rakouska-Uherska	Rakouska-Uhersko	k1gNnSc2	Rakouska-Uhersko
skončilo	skončit	k5eAaPmAgNnS	skončit
vypuknutím	vypuknutí	k1gNnSc7	vypuknutí
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1914	[number]	k4	1914
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
posléze	posléze	k6eAd1	posléze
celosvětový	celosvětový	k2eAgInSc4d1	celosvětový
ničivý	ničivý	k2eAgInSc4d1	ničivý
konflikt	konflikt	k1gInSc4	konflikt
započali	započnout	k5eAaPmAgMnP	započnout
vídeňští	vídeňský	k2eAgMnPc1d1	vídeňský
politici	politik	k1gMnPc1	politik
po	po	k7c6	po
Sarajevském	sarajevský	k2eAgInSc6d1	sarajevský
atentátu	atentát	k1gInSc6	atentát
na	na	k7c4	na
rakousko-uherského	rakouskoherský	k2eAgMnSc4d1	rakousko-uherský
následníka	následník	k1gMnSc4	následník
trůnu	trůn	k1gInSc2	trůn
Františka	František	k1gMnSc2	František
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Este	Est	k1gMnSc5	Est
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
domnění	domnění	k1gNnSc6	domnění
<g/>
,	,	kIx,	,
že	že	k8xS	že
válka	válka	k1gFnSc1	válka
proti	proti	k7c3	proti
malému	malý	k2eAgNnSc3d1	malé
Srbskému	srbský	k2eAgNnSc3d1	srbské
království	království	k1gNnSc3	království
bude	být	k5eAaImBp3nS	být
pro	pro	k7c4	pro
zdánlivě	zdánlivě	k6eAd1	zdánlivě
mocné	mocný	k2eAgNnSc4d1	mocné
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc4	Rakousko-Uhersko
snadnou	snadný	k2eAgFnSc7d1	snadná
záležitostí	záležitost	k1gFnSc7	záležitost
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
však	však	k9	však
rychle	rychle	k6eAd1	rychle
zapojily	zapojit	k5eAaPmAgFnP	zapojit
další	další	k2eAgFnPc1d1	další
mocnosti	mocnost	k1gFnPc1	mocnost
a	a	k8xC	a
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1	Rakousko-Uhersko
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
stalo	stát	k5eAaPmAgNnS	stát
pouhým	pouhý	k2eAgInSc7d1	pouhý
přívěskem	přívěsek	k1gInSc7	přívěsek
Německé	německý	k2eAgFnSc2d1	německá
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
vilémovského	vilémovský	k2eAgNnSc2d1	vilémovské
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
zasadilo	zasadit	k5eAaPmAgNnS	zasadit
austroslavismu	austroslavismus	k1gInSc6	austroslavismus
smrtelnou	smrtelný	k2eAgFnSc4d1	smrtelná
ránu	rána	k1gFnSc4	rána
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc4	Rakousko-Uhersko
nakonec	nakonec	k6eAd1	nakonec
skončila	skončit	k5eAaPmAgFnS	skončit
válka	válka	k1gFnSc1	válka
naprostou	naprostý	k2eAgFnSc4d1	naprostá
katastrofou	katastrofa	k1gFnSc7	katastrofa
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc7	jeho
rozpadem	rozpad	k1gInSc7	rozpad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
První	první	k4xOgFnSc1	první
Československá	československý	k2eAgFnSc1d1	Československá
republika	republika	k1gFnSc1	republika
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
bojovalo	bojovat	k5eAaImAgNnS	bojovat
1,5	[number]	k4	1,5
milionu	milion	k4xCgInSc2	milion
mužů	muž	k1gMnPc2	muž
odvedených	odvedený	k2eAgMnPc2d1	odvedený
z	z	k7c2	z
českých	český	k2eAgInPc2d1	český
okresů	okres	k1gInPc2	okres
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
padlo	padnout	k5eAaImAgNnS	padnout
138	[number]	k4	138
000	[number]	k4	000
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
monarchie	monarchie	k1gFnSc2	monarchie
a	a	k8xC	a
asi	asi	k9	asi
pět	pět	k4xCc4	pět
a	a	k8xC	a
půl	půl	k1xP	půl
tisíce	tisíc	k4xCgInSc2	tisíc
(	(	kIx(	(
<g/>
jen	jen	k9	jen
do	do	k7c2	do
konce	konec	k1gInSc2	konec
války	válka	k1gFnSc2	válka
<g/>
)	)	kIx)	)
v	v	k7c6	v
Československých	československý	k2eAgFnPc6d1	Československá
legiích	legie	k1gFnPc6	legie
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
válečného	válečný	k2eAgNnSc2d1	válečné
soupeření	soupeření	k1gNnSc2	soupeření
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
90	[number]	k4	90
000	[number]	k4	000
dobrovolníků	dobrovolník	k1gMnPc2	dobrovolník
zformovalo	zformovat	k5eAaPmAgNnS	zformovat
Československé	československý	k2eAgFnPc4d1	Československá
legie	legie	k1gFnPc4	legie
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
Itálii	Itálie	k1gFnSc6	Itálie
a	a	k8xC	a
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bojovali	bojovat	k5eAaImAgMnP	bojovat
proti	proti	k7c3	proti
centrálním	centrální	k2eAgFnPc3d1	centrální
mocnostem	mocnost	k1gFnPc3	mocnost
(	(	kIx(	(
<g/>
tedy	tedy	k8xC	tedy
i	i	k9	i
rodnému	rodný	k2eAgNnSc3d1	rodné
Rakousko-Uhersku	Rakousko-Uhersko	k1gNnSc3	Rakousko-Uhersko
<g/>
)	)	kIx)	)
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
i	i	k9	i
proti	proti	k7c3	proti
ruským	ruský	k2eAgMnPc3d1	ruský
bolševikům	bolševik	k1gMnPc3	bolševik
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
Rakouska-Uherska	Rakouska-Uhersko	k1gNnSc2	Rakouska-Uhersko
byly	být	k5eAaImAgFnP	být
po	po	k7c6	po
28	[number]	k4	28
<g/>
.	.	kIx.	.
říjnu	říjen	k1gInSc6	říjen
1918	[number]	k4	1918
země	zem	k1gFnSc2	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
<g/>
,	,	kIx,	,
části	část	k1gFnSc2	část
Uherského	uherský	k2eAgNnSc2d1	Uherské
království	království	k1gNnSc2	království
včetně	včetně	k7c2	včetně
Podkarpatské	podkarpatský	k2eAgFnSc2d1	Podkarpatská
Rusi	Rus	k1gFnSc2	Rus
spojeny	spojit	k5eAaPmNgInP	spojit
do	do	k7c2	do
nového	nový	k2eAgInSc2d1	nový
státního	státní	k2eAgInSc2d1	státní
útvaru	útvar	k1gInSc2	útvar
<g/>
,	,	kIx,	,
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
prvním	první	k4xOgMnSc7	první
prezidentem	prezident	k1gMnSc7	prezident
byl	být	k5eAaImAgMnS	být
zvolen	zvolen	k2eAgMnSc1d1	zvolen
Tomáš	Tomáš	k1gMnSc1	Tomáš
Garrigue	Garrigue	k1gNnSc2	Garrigue
Masaryk	Masaryk	k1gMnSc1	Masaryk
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1914	[number]	k4	1914
pracoval	pracovat	k5eAaImAgInS	pracovat
pro	pro	k7c4	pro
českou	český	k2eAgFnSc4d1	Česká
resp.	resp.	kA	resp.
československou	československý	k2eAgFnSc4d1	Československá
samostatnost	samostatnost	k1gFnSc4	samostatnost
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
Dohody	dohoda	k1gFnSc2	dohoda
a	a	k8xC	a
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
zejména	zejména	k9	zejména
Edvarda	Edvard	k1gMnSc4	Edvard
Beneše	Beneš	k1gMnSc4	Beneš
a	a	k8xC	a
Milana	Milan	k1gMnSc4	Milan
Rastislava	Rastislav	k1gMnSc4	Rastislav
Štefánika	Štefánik	k1gMnSc4	Štefánik
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
od	od	k7c2	od
vzniku	vznik	k1gInSc2	vznik
státu	stát	k1gInSc2	stát
až	až	k6eAd1	až
do	do	k7c2	do
zániku	zánik	k1gInSc2	zánik
tzv.	tzv.	kA	tzv.
první	první	k4xOgFnSc2	první
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
oficiálně	oficiálně	k6eAd1	oficiálně
Republiky	republika	k1gFnSc2	republika
československé	československý	k2eAgFnSc2d1	Československá
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
Československo	Československo	k1gNnSc1	Československo
unitárním	unitární	k2eAgInSc7d1	unitární
státem	stát	k1gInSc7	stát
a	a	k8xC	a
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
i	i	k9	i
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1933	[number]	k4	1933
jediným	jediný	k2eAgMnSc7d1	jediný
skutečně	skutečně	k6eAd1	skutečně
demokratickým	demokratický	k2eAgInSc7d1	demokratický
státem	stát	k1gInSc7	stát
ve	v	k7c6	v
Střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
<g/>
Přes	přes	k7c4	přes
svůj	svůj	k3xOyFgInSc4	svůj
deklarovaný	deklarovaný	k2eAgInSc4d1	deklarovaný
národní	národní	k2eAgInSc4d1	národní
charakter	charakter	k1gInSc4	charakter
<g/>
,	,	kIx,	,
založený	založený	k2eAgInSc4d1	založený
na	na	k7c6	na
čechoslovakismu	čechoslovakismus	k1gInSc6	čechoslovakismus
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
Československo	Československo	k1gNnSc1	Československo
multietnickým	multietnický	k2eAgInSc7d1	multietnický
státem	stát	k1gInSc7	stát
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgNnSc6	který
žilo	žít	k5eAaImAgNnS	žít
6	[number]	k4	6
747	[number]	k4	747
000	[number]	k4	000
Čechů	Čech	k1gMnPc2	Čech
<g/>
,	,	kIx,	,
3	[number]	k4	3
124	[number]	k4	124
000	[number]	k4	000
Němců	Němec	k1gMnPc2	Němec
<g/>
,	,	kIx,	,
2	[number]	k4	2
014	[number]	k4	014
000	[number]	k4	000
Slováků	Slovák	k1gMnPc2	Slovák
<g/>
,	,	kIx,	,
745	[number]	k4	745
000	[number]	k4	000
Maďarů	Maďar	k1gMnPc2	Maďar
<g/>
,	,	kIx,	,
462	[number]	k4	462
000	[number]	k4	000
Rusínů	rusín	k1gInPc2	rusín
<g/>
,	,	kIx,	,
181	[number]	k4	181
000	[number]	k4	000
občanů	občan	k1gMnPc2	občan
židovské	židovský	k2eAgFnSc2d1	židovská
národnosti	národnost	k1gFnSc2	národnost
a	a	k8xC	a
76	[number]	k4	76
000	[number]	k4	000
Poláků	Polák	k1gMnPc2	Polák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
nezávislosti	nezávislost	k1gFnSc2	nezávislost
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
pohraničním	pohraniční	k2eAgInPc3d1	pohraniční
konfliktům	konflikt	k1gInPc3	konflikt
s	s	k7c7	s
Polskem	Polsko	k1gNnSc7	Polsko
a	a	k8xC	a
Maďarskem	Maďarsko	k1gNnSc7	Maďarsko
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
k	k	k7c3	k
nepokojům	nepokoj	k1gInPc3	nepokoj
v	v	k7c6	v
německých	německý	k2eAgFnPc6d1	německá
oblastech	oblast	k1gFnPc6	oblast
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Problém	problém	k1gInSc1	problém
nepřátelských	přátelský	k2eNgMnPc2d1	nepřátelský
sousedů	soused	k1gMnPc2	soused
se	se	k3xPyFc4	se
Československo	Československo	k1gNnSc4	Československo
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
jeho	jeho	k3xOp3gMnSc1	jeho
dlouholetý	dlouholetý	k2eAgMnSc1d1	dlouholetý
ministr	ministr	k1gMnSc1	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
Edvard	Edvard	k1gMnSc1	Edvard
Beneš	Beneš	k1gMnSc1	Beneš
<g/>
,	,	kIx,	,
pokusilo	pokusit	k5eAaPmAgNnS	pokusit
vyřešit	vyřešit	k5eAaPmF	vyřešit
spojenectvím	spojenectví	k1gNnSc7	spojenectví
zvaným	zvaný	k2eAgNnSc7d1	zvané
Malá	malý	k2eAgFnSc1d1	malá
dohoda	dohoda	k1gFnSc1	dohoda
<g/>
,	,	kIx,	,
systémem	systém	k1gInSc7	systém
spojeneckých	spojenecký	k2eAgFnPc2d1	spojenecká
smluv	smlouva	k1gFnPc2	smlouva
s	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1935	[number]	k4	1935
i	i	k9	i
smlouvou	smlouva	k1gFnSc7	smlouva
se	s	k7c7	s
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Sudetští	sudetský	k2eAgMnPc1d1	sudetský
Němci	Němec	k1gMnPc1	Němec
<g/>
,	,	kIx,	,
žijící	žijící	k2eAgMnPc1d1	žijící
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
pohraničních	pohraniční	k2eAgFnPc6d1	pohraniční
oblastech	oblast	k1gFnPc6	oblast
přilehlých	přilehlý	k2eAgFnPc6d1	přilehlá
k	k	k7c3	k
Německu	Německo	k1gNnSc3	Německo
a	a	k8xC	a
Rakousku	Rakousko	k1gNnSc3	Rakousko
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
Velké	velký	k2eAgFnSc2d1	velká
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
krize	krize	k1gFnSc2	krize
<g/>
,	,	kIx,	,
masivní	masivní	k2eAgFnSc2d1	masivní
nezaměstnanosti	nezaměstnanost	k1gFnSc2	nezaměstnanost
(	(	kIx(	(
<g/>
která	který	k3yQgFnSc1	který
však	však	k9	však
postihla	postihnout	k5eAaPmAgFnS	postihnout
všechny	všechen	k3xTgFnPc4	všechen
národnosti	národnost	k1gFnPc4	národnost
<g/>
)	)	kIx)	)
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1933	[number]	k4	1933
také	také	k6eAd1	také
intenzivní	intenzivní	k2eAgFnSc2d1	intenzivní
nacistické	nacistický	k2eAgFnSc2d1	nacistická
propagandy	propaganda	k1gFnSc2	propaganda
radikalizovali	radikalizovat	k5eAaBmAgMnP	radikalizovat
a	a	k8xC	a
začali	začít	k5eAaPmAgMnP	začít
požadovat	požadovat	k5eAaImF	požadovat
odtržení	odtržení	k1gNnSc4	odtržení
od	od	k7c2	od
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc4	tento
snahy	snaha	k1gFnPc4	snaha
reprezentovala	reprezentovat	k5eAaImAgFnS	reprezentovat
Sudetoněmecká	sudetoněmecký	k2eAgFnSc1d1	Sudetoněmecká
strana	strana	k1gFnSc1	strana
(	(	kIx(	(
<g/>
Sudetendeutsche	Sudetendeutsche	k1gFnSc1	Sudetendeutsche
Partei	Parte	k1gFnSc2	Parte
<g/>
,	,	kIx,	,
SdP	SdP	k1gFnSc1	SdP
<g/>
)	)	kIx)	)
vedená	vedený	k2eAgFnSc1d1	vedená
Konradem	Konrad	k1gInSc7	Konrad
Henleinem	Henlein	k1gInSc7	Henlein
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
nátlak	nátlak	k1gInSc4	nátlak
Německé	německý	k2eAgFnSc2d1	německá
říše	říš	k1gFnSc2	říš
(	(	kIx(	(
<g/>
posílené	posílená	k1gFnSc3	posílená
tzv.	tzv.	kA	tzv.
anšlusem	anšlus	k1gInSc7	anšlus
Rakouska	Rakousko	k1gNnSc2	Rakousko
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
a	a	k8xC	a
evropských	evropský	k2eAgFnPc2d1	Evropská
mocností	mocnost	k1gFnPc2	mocnost
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
a	a	k8xC	a
Itálie	Itálie	k1gFnSc2	Itálie
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
září	září	k1gNnSc6	září
roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
Československo	Československo	k1gNnSc4	Československo
Mnichovskou	mnichovský	k2eAgFnSc7d1	Mnichovská
dohodou	dohoda	k1gFnSc7	dohoda
donuceno	donutit	k5eAaPmNgNnS	donutit
postoupit	postoupit	k5eAaPmF	postoupit
Německu	Německo	k1gNnSc3	Německo
rozsáhlé	rozsáhlý	k2eAgFnSc2d1	rozsáhlá
pohraniční	pohraniční	k2eAgFnSc2d1	pohraniční
oblasti	oblast	k1gFnSc2	oblast
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Sudety	Sudety	k1gInPc1	Sudety
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mnichovská	mnichovský	k2eAgFnSc1d1	Mnichovská
dohoda	dohoda	k1gFnSc1	dohoda
bývá	bývat	k5eAaImIp3nS	bývat
také	také	k9	také
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k9	jako
Mnichovská	mnichovský	k2eAgFnSc1d1	Mnichovská
zrada	zrada	k1gFnSc1	zrada
či	či	k8xC	či
Mnichovský	mnichovský	k2eAgInSc1d1	mnichovský
diktát	diktát	k1gInSc1	diktát
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
zástupci	zástupce	k1gMnPc1	zástupce
československé	československý	k2eAgFnSc2d1	Československá
strany	strana	k1gFnSc2	strana
nebyli	být	k5eNaImAgMnP	být
přizváni	přizvat	k5eAaPmNgMnP	přizvat
k	k	k7c3	k
jednání	jednání	k1gNnSc3	jednání
a	a	k8xC	a
Německá	německý	k2eAgFnSc1d1	německá
říše	říše	k1gFnSc1	říše
zároveň	zároveň	k6eAd1	zároveň
hrozila	hrozit	k5eAaImAgFnS	hrozit
vojenským	vojenský	k2eAgNnSc7d1	vojenské
přepadením	přepadení	k1gNnSc7	přepadení
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
se	se	k3xPyFc4	se
platná	platný	k2eAgFnSc1d1	platná
vojenská	vojenský	k2eAgFnSc1d1	vojenská
aliance	aliance	k1gFnSc1	aliance
Československa	Československo	k1gNnSc2	Československo
s	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
ukázala	ukázat	k5eAaPmAgFnS	ukázat
jako	jako	k9	jako
zcela	zcela	k6eAd1	zcela
neúčinná	účinný	k2eNgFnSc1d1	neúčinná
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
postupného	postupný	k2eAgInSc2d1	postupný
záboru	zábor	k1gInSc2	zábor
sudetských	sudetský	k2eAgFnPc2d1	sudetská
oblastí	oblast	k1gFnPc2	oblast
Německem	Německo	k1gNnSc7	Německo
(	(	kIx(	(
<g/>
mnohdy	mnohdy	k6eAd1	mnohdy
s	s	k7c7	s
početným	početný	k2eAgNnSc7d1	početné
českým	český	k2eAgNnSc7d1	české
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
<g/>
)	)	kIx)	)
připadly	připadnout	k5eAaPmAgFnP	připadnout
jižní	jižní	k2eAgFnPc1d1	jižní
oblasti	oblast	k1gFnPc1	oblast
Slovenska	Slovensko	k1gNnSc2	Slovensko
a	a	k8xC	a
Podkarpatské	podkarpatský	k2eAgFnSc6d1	Podkarpatská
Rusi	Rus	k1gFnSc6	Rus
s	s	k7c7	s
maďarsky	maďarsky	k6eAd1	maďarsky
mluvícím	mluvící	k2eAgNnSc7d1	mluvící
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
<g/>
.	.	kIx.	.
</s>
<s>
Malou	malý	k2eAgFnSc4d1	malá
část	část	k1gFnSc4	část
československého	československý	k2eAgNnSc2d1	Československé
území	území	k1gNnSc2	území
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
oblast	oblast	k1gFnSc1	oblast
Těšínska	Těšínsko	k1gNnSc2	Těšínsko
<g/>
,	,	kIx,	,
zabralo	zabrat	k5eAaPmAgNnS	zabrat
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc4	název
takto	takto	k6eAd1	takto
okleštěného	okleštěný	k2eAgInSc2d1	okleštěný
státního	státní	k2eAgInSc2d1	státní
útvaru	útvar	k1gInSc2	útvar
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgMnS	začít
psát	psát	k5eAaImF	psát
se	s	k7c7	s
spojovníkem	spojovník	k1gInSc7	spojovník
(	(	kIx(	(
<g/>
Česko-Slovensko	Česko-Slovensko	k1gNnSc1	Česko-Slovensko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zbývající	zbývající	k2eAgNnSc4d1	zbývající
krátké	krátký	k2eAgNnSc4d1	krátké
období	období	k1gNnSc4	období
od	od	k7c2	od
Mnichovské	mnichovský	k2eAgFnSc2d1	Mnichovská
dohody	dohoda	k1gFnSc2	dohoda
až	až	k9	až
do	do	k7c2	do
úplného	úplný	k2eAgNnSc2d1	úplné
rozbití	rozbití	k1gNnSc2	rozbití
Československa	Československo	k1gNnSc2	Československo
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1939	[number]	k4	1939
se	se	k3xPyFc4	se
vžilo	vžít	k5eAaPmAgNnS	vžít
označení	označení	k1gNnSc1	označení
druhá	druhý	k4xOgFnSc1	druhý
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Protektorát	protektorát	k1gInSc1	protektorát
Čechy	Čechy	k1gFnPc1	Čechy
a	a	k8xC	a
Morava	Morava	k1gFnSc1	Morava
===	===	k?	===
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
14	[number]	k4	14
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1939	[number]	k4	1939
Slovensko	Slovensko	k1gNnSc1	Slovensko
vyhlásilo	vyhlásit	k5eAaPmAgNnS	vyhlásit
samostatnost	samostatnost	k1gFnSc4	samostatnost
a	a	k8xC	a
po	po	k7c6	po
okupaci	okupace	k1gFnSc6	okupace
německými	německý	k2eAgFnPc7d1	německá
vojsky	vojsky	k6eAd1	vojsky
15	[number]	k4	15
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1939	[number]	k4	1939
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
zbytku	zbytek	k1gInSc6	zbytek
československého	československý	k2eAgNnSc2d1	Československé
území	území	k1gNnSc2	území
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
bez	bez	k7c2	bez
Sudet	Sudety	k1gInPc2	Sudety
<g/>
,	,	kIx,	,
připojených	připojený	k2eAgInPc2d1	připojený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
k	k	k7c3	k
Německu	Německo	k1gNnSc3	Německo
<g/>
,	,	kIx,	,
a	a	k8xC	a
východní	východní	k2eAgFnSc2d1	východní
části	část	k1gFnSc2	část
československého	československý	k2eAgNnSc2d1	Československé
Těšínska	Těšínsko	k1gNnSc2	Těšínsko
<g/>
,	,	kIx,	,
připojené	připojený	k2eAgInPc4d1	připojený
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
k	k	k7c3	k
Polsku	Polsko	k1gNnSc3	Polsko
<g/>
)	)	kIx)	)
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
Protektorát	protektorát	k1gInSc1	protektorát
Čechy	Čechy	k1gFnPc1	Čechy
a	a	k8xC	a
Morava	Morava	k1gFnSc1	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
Edvard	Edvard	k1gMnSc1	Edvard
Beneš	Beneš	k1gMnSc1	Beneš
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
zorganizoval	zorganizovat	k5eAaPmAgMnS	zorganizovat
exilovou	exilový	k2eAgFnSc4d1	exilová
vládu	vláda	k1gFnSc4	vláda
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
prozatímní	prozatímní	k2eAgNnSc1d1	prozatímní
státní	státní	k2eAgNnSc1d1	státní
zřízení	zřízení	k1gNnSc1	zřízení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Německá	německý	k2eAgFnSc1d1	německá
okupace	okupace	k1gFnSc1	okupace
Československa	Československo	k1gNnSc2	Československo
se	se	k3xPyFc4	se
setkala	setkat	k5eAaPmAgFnS	setkat
s	s	k7c7	s
masivním	masivní	k2eAgInSc7d1	masivní
odbojem	odboj	k1gInSc7	odboj
obyvatel	obyvatel	k1gMnPc2	obyvatel
země	zem	k1gFnSc2	zem
a	a	k8xC	a
skupin	skupina	k1gFnPc2	skupina
podporovaných	podporovaný	k2eAgFnPc2d1	podporovaná
ze	z	k7c2	z
zahraničí	zahraničí	k1gNnSc2	zahraničí
(	(	kIx(	(
<g/>
zejm.	zejm.	k?	zejm.
operace	operace	k1gFnSc1	operace
Anthropoid	Anthropoid	k1gInSc1	Anthropoid
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c4	na
který	který	k3yQgInSc4	který
nacisté	nacista	k1gMnPc1	nacista
reagovali	reagovat	k5eAaBmAgMnP	reagovat
terorem	teror	k1gInSc7	teror
(	(	kIx(	(
<g/>
např.	např.	kA	např.
vyhlazení	vyhlazení	k1gNnSc1	vyhlazení
Lidic	Lidice	k1gInPc2	Lidice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
nacisté	nacista	k1gMnPc1	nacista
realizovali	realizovat	k5eAaBmAgMnP	realizovat
politiku	politika	k1gFnSc4	politika
totálního	totální	k2eAgNnSc2d1	totální
nasazení	nasazení	k1gNnSc2	nasazení
českých	český	k2eAgFnPc2d1	Česká
pracovních	pracovní	k2eAgFnPc2d1	pracovní
sil	síla	k1gFnPc2	síla
na	na	k7c6	na
území	území	k1gNnSc6	území
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
likvidaci	likvidace	k1gFnSc4	likvidace
židovské	židovský	k2eAgFnSc2d1	židovská
diaspory	diaspora	k1gFnSc2	diaspora
na	na	k7c6	na
území	území	k1gNnSc6	území
Protektorátu	protektorát	k1gInSc2	protektorát
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgInSc1d3	nejznámější
údaj	údaj	k1gInSc1	údaj
o	o	k7c6	o
počtu	počet	k1gInSc6	počet
obětí	oběť	k1gFnPc2	oběť
nacistické	nacistický	k2eAgFnSc2d1	nacistická
okupace	okupace	k1gFnSc2	okupace
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
výzkumu	výzkum	k1gInSc2	výzkum
zveřejněného	zveřejněný	k2eAgInSc2d1	zveřejněný
Gustavem	Gustav	k1gMnSc7	Gustav
Hajčíkem	Hajčík	k1gMnSc7	Hajčík
a	a	k8xC	a
Jaroslavem	Jaroslav	k1gMnSc7	Jaroslav
Voleníkem	Voleník	k1gMnSc7	Voleník
roku	rok	k1gInSc2	rok
1956	[number]	k4	1956
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
něhož	jenž	k3xRgInSc2	jenž
během	během	k7c2	během
války	válka	k1gFnSc2	válka
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
360	[number]	k4	360
000	[number]	k4	000
Čechoslováků	Čechoslovák	k1gMnPc2	Čechoslovák
<g/>
.	.	kIx.	.
</s>
<s>
Konečné	Konečné	k2eAgNnSc1d1	Konečné
řešení	řešení	k1gNnSc1	řešení
české	český	k2eAgFnSc2d1	Česká
otázky	otázka	k1gFnSc2	otázka
zapadalo	zapadat	k5eAaImAgNnS	zapadat
do	do	k7c2	do
Generalplánu	Generalplán	k1gInSc2	Generalplán
Ost	Ost	k1gFnSc2	Ost
<g/>
,	,	kIx,	,
nacistického	nacistický	k2eAgInSc2d1	nacistický
plánu	plán	k1gInSc2	plán
na	na	k7c4	na
likvidaci	likvidace	k1gFnSc4	likvidace
<g/>
,	,	kIx,	,
poněmčení	poněmčení	k1gNnSc4	poněmčení
a	a	k8xC	a
vysídlení	vysídlení	k1gNnSc4	vysídlení
příslušníků	příslušník	k1gMnPc2	příslušník
pěti	pět	k4xCc2	pět
slovanských	slovanský	k2eAgInPc2d1	slovanský
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
měl	mít	k5eAaImAgInS	mít
vytvořit	vytvořit	k5eAaPmF	vytvořit
životní	životní	k2eAgInSc4d1	životní
prostor	prostor	k1gInSc4	prostor
pro	pro	k7c4	pro
německé	německý	k2eAgNnSc4d1	německé
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Československo	Československo	k1gNnSc1	Československo
v	v	k7c6	v
letech	let	k1gInPc6	let
1945	[number]	k4	1945
<g/>
–	–	k?	–
<g/>
1992	[number]	k4	1992
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Osvobození	osvobození	k1gNnPc2	osvobození
Československa	Československo	k1gNnSc2	Československo
a	a	k8xC	a
období	období	k1gNnSc2	období
do	do	k7c2	do
února	únor	k1gInSc2	únor
1948	[number]	k4	1948
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1945	[number]	k4	1945
bylo	být	k5eAaImAgNnS	být
dokončeno	dokončit	k5eAaPmNgNnS	dokončit
osvobozování	osvobozování	k1gNnSc1	osvobozování
Československa	Československo	k1gNnSc2	Československo
spojenci	spojenec	k1gMnPc7	spojenec
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
obnoven	obnovit	k5eAaPmNgInS	obnovit
formálně	formálně	k6eAd1	formálně
demokratický	demokratický	k2eAgInSc1d1	demokratický
stát	stát	k1gInSc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
Období	období	k1gNnSc1	období
1945	[number]	k4	1945
<g/>
–	–	k?	–
<g/>
1948	[number]	k4	1948
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
nazýváno	nazývat	k5eAaImNgNnS	nazývat
třetí	třetí	k4xOgFnSc1	třetí
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
osvobozování	osvobozování	k1gNnSc6	osvobozování
Československa	Československo	k1gNnSc2	Československo
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
okolo	okolo	k7c2	okolo
140	[number]	k4	140
000	[number]	k4	000
sovětských	sovětský	k2eAgMnPc2d1	sovětský
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jejich	jejich	k3xOp3gInSc6	jejich
boku	bok	k1gInSc6	bok
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
okolo	okolo	k7c2	okolo
11	[number]	k4	11
700	[number]	k4	700
Čechoslováků	Čechoslovák	k1gMnPc2	Čechoslovák
<g/>
,	,	kIx,	,
příslušníků	příslušník	k1gMnPc2	příslušník
1	[number]	k4	1
<g/>
.	.	kIx.	.
čs	čs	kA	čs
<g/>
.	.	kIx.	.
armádního	armádní	k2eAgInSc2d1	armádní
sboru	sbor	k1gInSc2	sbor
<g/>
,	,	kIx,	,
kterému	který	k3yRgMnSc3	který
velel	velet	k5eAaImAgInS	velet
Ludvík	Ludvík	k1gMnSc1	Ludvík
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
armádní	armádní	k2eAgInSc1d1	armádní
sbor	sbor	k1gInSc1	sbor
se	se	k3xPyFc4	se
vyznamenal	vyznamenat	k5eAaPmAgInS	vyznamenat
v	v	k7c6	v
bojích	boj	k1gInPc6	boj
u	u	k7c2	u
Sokolova	Sokolovo	k1gNnSc2	Sokolovo
a	a	k8xC	a
o	o	k7c4	o
Kyjev	Kyjev	k1gInSc4	Kyjev
a	a	k8xC	a
krutě	krutě	k6eAd1	krutě
krvácel	krvácet	k5eAaImAgMnS	krvácet
v	v	k7c6	v
karpatsko-dukelské	karpatskoukelský	k2eAgFnSc6d1	karpatsko-dukelská
operaci	operace	k1gFnSc6	operace
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
pomoci	pomoct	k5eAaPmF	pomoct
Slovenskému	slovenský	k2eAgNnSc3d1	slovenské
národnímu	národní	k2eAgNnSc3d1	národní
povstání	povstání	k1gNnSc3	povstání
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
osvobozování	osvobozování	k1gNnSc6	osvobozování
Československa	Československo	k1gNnSc2	Československo
dále	daleko	k6eAd2	daleko
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
66	[number]	k4	66
495	[number]	k4	495
Rumunů	Rumun	k1gMnPc2	Rumun
<g/>
,	,	kIx,	,
1	[number]	k4	1
302	[number]	k4	302
Poláků	Polák	k1gMnPc2	Polák
a	a	k8xC	a
351	[number]	k4	351
Američanů	Američan	k1gMnPc2	Američan
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
osvobození	osvobození	k1gNnSc3	osvobození
Československa	Československo	k1gNnSc2	Československo
také	také	k6eAd1	také
pomohlo	pomoct	k5eAaPmAgNnS	pomoct
české	český	k2eAgNnSc4d1	české
květnové	květnový	k2eAgNnSc4d1	květnové
povstání	povstání	k1gNnSc4	povstání
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc1	jehož
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
nejznámější	známý	k2eAgInPc4d3	nejznámější
součástí	součást	k1gFnSc7	součást
bylo	být	k5eAaImAgNnS	být
Pražské	pražský	k2eAgNnSc1d1	Pražské
povstání	povstání	k1gNnSc1	povstání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
osvobození	osvobození	k1gNnSc6	osvobození
Slovenska	Slovensko	k1gNnSc2	Slovensko
postupovala	postupovat	k5eAaImAgFnS	postupovat
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
od	od	k7c2	od
března	březen	k1gInSc2	březen
1945	[number]	k4	1945
na	na	k7c4	na
české	český	k2eAgNnSc4d1	české
území	území	k1gNnSc4	území
z	z	k7c2	z
Ostravska	Ostravsko	k1gNnSc2	Ostravsko
a	a	k8xC	a
od	od	k7c2	od
Bratislavy	Bratislava	k1gFnSc2	Bratislava
(	(	kIx(	(
<g/>
zde	zde	k6eAd1	zde
společně	společně	k6eAd1	společně
s	s	k7c7	s
rumunskou	rumunský	k2eAgFnSc7d1	rumunská
armádou	armáda	k1gFnSc7	armáda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
i	i	k9	i
Pražská	pražský	k2eAgFnSc1d1	Pražská
ofenzíva	ofenzíva	k1gFnSc1	ofenzíva
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
k	k	k7c3	k
Praze	Praha	k1gFnSc3	Praha
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
zúčastnily	zúčastnit	k5eAaPmAgFnP	zúčastnit
také	také	k9	také
jednotky	jednotka	k1gFnPc1	jednotka
rumunské	rumunský	k2eAgFnSc2d1	rumunská
a	a	k8xC	a
polské	polský	k2eAgFnSc2d1	polská
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Sovětská	sovětský	k2eAgNnPc4d1	sovětské
vojska	vojsko	k1gNnPc4	vojsko
postupovala	postupovat	k5eAaImAgFnS	postupovat
až	až	k6eAd1	až
k	k	k7c3	k
dohodnuté	dohodnutý	k2eAgFnSc3d1	dohodnutá
demarkační	demarkační	k2eAgFnSc3d1	demarkační
linii	linie	k1gFnSc3	linie
s	s	k7c7	s
americkou	americký	k2eAgFnSc7d1	americká
armádou	armáda	k1gFnSc7	armáda
a	a	k8xC	a
osvobodila	osvobodit	k5eAaPmAgFnS	osvobodit
přitom	přitom	k6eAd1	přitom
zhruba	zhruba	k6eAd1	zhruba
dvě	dva	k4xCgFnPc1	dva
třetiny	třetina	k1gFnPc1	třetina
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Zbytek	zbytek	k1gInSc1	zbytek
českého	český	k2eAgInSc2d1	český
prostoru	prostor	k1gInSc2	prostor
západně	západně	k6eAd1	západně
od	od	k7c2	od
linie	linie	k1gFnSc2	linie
Karlovy	Karlův	k2eAgInPc1d1	Karlův
Vary	Vary	k1gInPc1	Vary
–	–	k?	–
Plzeň	Plzeň	k1gFnSc4	Plzeň
–	–	k?	–
České	český	k2eAgInPc4d1	český
Budějovice	Budějovice	k1gInPc4	Budějovice
osvobodila	osvobodit	k5eAaPmAgFnS	osvobodit
americká	americký	k2eAgFnSc1d1	americká
armáda	armáda	k1gFnSc1	armáda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
1945	[number]	k4	1945
až	až	k9	až
do	do	k7c2	do
února	únor	k1gInSc2	únor
1948	[number]	k4	1948
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
jevům	jev	k1gInPc3	jev
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
podle	podle	k7c2	podle
některých	některý	k3yIgMnPc2	některý
historiků	historik	k1gMnPc2	historik
sporné	sporný	k2eAgInPc1d1	sporný
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
bylo	být	k5eAaImAgNnS	být
vysídlení	vysídlení	k1gNnSc1	vysídlení
Němců	Němec	k1gMnPc2	Němec
z	z	k7c2	z
Československa	Československo	k1gNnSc2	Československo
do	do	k7c2	do
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
,	,	kIx,	,
omezení	omezení	k1gNnSc2	omezení
stranické	stranický	k2eAgFnSc2d1	stranická
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
,	,	kIx,	,
rozsáhlé	rozsáhlý	k2eAgNnSc1d1	rozsáhlé
znárodňování	znárodňování	k1gNnSc1	znárodňování
těžkého	těžký	k2eAgInSc2d1	těžký
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
,	,	kIx,	,
energetiky	energetika	k1gFnSc2	energetika
<g/>
,	,	kIx,	,
filmového	filmový	k2eAgInSc2d1	filmový
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
,	,	kIx,	,
bank	banka	k1gFnPc2	banka
<g/>
,	,	kIx,	,
pojišťoven	pojišťovna	k1gFnPc2	pojišťovna
<g/>
,	,	kIx,	,
větších	veliký	k2eAgInPc2d2	veliký
stavebních	stavební	k2eAgInPc2d1	stavební
podniků	podnik	k1gInPc2	podnik
atd.	atd.	kA	atd.
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Benešovy	Benešův	k2eAgInPc1d1	Benešův
dekrety	dekret	k1gInPc1	dekret
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
například	například	k6eAd1	například
odsun	odsun	k1gInSc4	odsun
Němců	Němec	k1gMnPc2	Němec
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
odsouhlasily	odsouhlasit	k5eAaPmAgFnP	odsouhlasit
tři	tři	k4xCgFnPc1	tři
vítězné	vítězný	k2eAgFnPc1d1	vítězná
mocnosti	mocnost	k1gFnPc1	mocnost
na	na	k7c6	na
Postupimské	postupimský	k2eAgFnSc6d1	Postupimská
konferenci	konference	k1gFnSc6	konference
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Československo	Československo	k1gNnSc1	Československo
pod	pod	k7c7	pod
vládou	vláda	k1gFnSc7	vláda
Komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
parlamentních	parlamentní	k2eAgFnPc6d1	parlamentní
volbách	volba	k1gFnPc6	volba
1946	[number]	k4	1946
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
Komunistická	komunistický	k2eAgFnSc1d1	komunistická
strana	strana	k1gFnSc1	strana
Československa	Československo	k1gNnSc2	Československo
a	a	k8xC	a
tyto	tento	k3xDgFnPc1	tento
volby	volba	k1gFnPc1	volba
bývají	bývat	k5eAaImIp3nP	bývat
označované	označovaný	k2eAgFnPc1d1	označovaná
jako	jako	k8xS	jako
poslední	poslední	k2eAgFnPc1d1	poslední
svobodné	svobodný	k2eAgFnPc1d1	svobodná
demokratické	demokratický	k2eAgFnPc1d1	demokratická
volby	volba	k1gFnPc1	volba
na	na	k7c4	na
dobu	doba	k1gFnSc4	doba
více	hodně	k6eAd2	hodně
než	než	k8xS	než
čtyřiceti	čtyřicet	k4xCc2	čtyřicet
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1948	[number]	k4	1948
se	se	k3xPyFc4	se
komunisté	komunista	k1gMnPc1	komunista
chopili	chopit	k5eAaPmAgMnP	chopit
moci	moct	k5eAaImF	moct
pučem	puč	k1gInSc7	puč
a	a	k8xC	a
zlikvidovali	zlikvidovat	k5eAaPmAgMnP	zlikvidovat
zbytky	zbytek	k1gInPc4	zbytek
demokratického	demokratický	k2eAgInSc2d1	demokratický
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Těžce	těžce	k6eAd1	těžce
nemocný	mocný	k2eNgMnSc1d1	nemocný
prezident	prezident	k1gMnSc1	prezident
Edvard	Edvard	k1gMnSc1	Edvard
Beneš	Beneš	k1gMnSc1	Beneš
abdikoval	abdikovat	k5eAaBmAgMnS	abdikovat
a	a	k8xC	a
ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
místo	místo	k1gNnSc4	místo
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
vůdce	vůdce	k1gMnSc1	vůdce
komunistů	komunista	k1gMnPc2	komunista
Klement	Klement	k1gMnSc1	Klement
Gottwald	Gottwald	k1gMnSc1	Gottwald
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
totalitním	totalitní	k2eAgInSc7d1	totalitní
státem	stát	k1gInSc7	stát
a	a	k8xC	a
součástí	součást	k1gFnSc7	součást
východního	východní	k2eAgInSc2d1	východní
bloku	blok	k1gInSc2	blok
pod	pod	k7c7	pod
dominancí	dominance	k1gFnSc7	dominance
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
potlačeny	potlačen	k2eAgFnPc1d1	potlačena
svobody	svoboda	k1gFnPc1	svoboda
a	a	k8xC	a
struktury	struktura	k1gFnPc1	struktura
občanské	občanský	k2eAgFnSc2d1	občanská
společnosti	společnost	k1gFnSc2	společnost
počínaje	počínaje	k7c7	počínaje
zrušením	zrušení	k1gNnSc7	zrušení
samosprávných	samosprávný	k2eAgFnPc2d1	samosprávná
zemí	zem	k1gFnPc2	zem
(	(	kIx(	(
<g/>
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
až	až	k9	až
po	po	k7c6	po
potlačení	potlačení	k1gNnSc6	potlačení
svobodného	svobodný	k2eAgInSc2d1	svobodný
spolkového	spolkový	k2eAgInSc2d1	spolkový
a	a	k8xC	a
ekonomického	ekonomický	k2eAgInSc2d1	ekonomický
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
vlně	vlna	k1gFnSc3	vlna
emigrace	emigrace	k1gFnSc2	emigrace
lidí	člověk	k1gMnPc2	člověk
do	do	k7c2	do
zemí	zem	k1gFnPc2	zem
neovládaných	ovládaný	k2eNgMnPc2d1	ovládaný
komunismem	komunismus	k1gInSc7	komunismus
<g/>
.	.	kIx.	.
</s>
<s>
Kolektivizace	kolektivizace	k1gFnSc1	kolektivizace
zemědělství	zemědělství	k1gNnSc2	zemědělství
a	a	k8xC	a
měnová	měnový	k2eAgFnSc1d1	měnová
reforma	reforma	k1gFnSc1	reforma
(	(	kIx(	(
<g/>
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
připravily	připravit	k5eAaPmAgFnP	připravit
miliony	milion	k4xCgInPc1	milion
občanů	občan	k1gMnPc2	občan
o	o	k7c4	o
majetek	majetek	k1gInSc4	majetek
<g/>
.	.	kIx.	.
</s>
<s>
Tisíce	tisíc	k4xCgInPc1	tisíc
občanů	občan	k1gMnPc2	občan
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
obětí	oběť	k1gFnSc7	oběť
politický	politický	k2eAgInSc4d1	politický
procesů	proces	k1gInPc2	proces
<g/>
,	,	kIx,	,
mnohdy	mnohdy	k6eAd1	mnohdy
i	i	k9	i
justičních	justiční	k2eAgFnPc2d1	justiční
vražd	vražda	k1gFnPc2	vražda
–	–	k?	–
k	k	k7c3	k
nejznámějším	známý	k2eAgFnPc3d3	nejznámější
obětem	oběť	k1gFnPc3	oběť
perzekucí	perzekuce	k1gFnPc2	perzekuce
patří	patřit	k5eAaImIp3nS	patřit
Milada	Milada	k1gFnSc1	Milada
Horáková	Horáková	k1gFnSc1	Horáková
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
Slánský	Slánský	k1gMnSc1	Slánský
či	či	k8xC	či
Heliodor	Heliodor	k1gMnSc1	Heliodor
Píka	píka	k1gFnSc1	píka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
Gottwaldově	Gottwaldův	k2eAgFnSc6d1	Gottwaldova
smrti	smrt	k1gFnSc6	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
prezidentem	prezident	k1gMnSc7	prezident
Antonín	Antonín	k1gMnSc1	Antonín
Zápotocký	Zápotocký	k1gMnSc1	Zápotocký
<g/>
,	,	kIx,	,
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
pak	pak	k6eAd1	pak
Antonín	Antonín	k1gMnSc1	Antonín
Novotný	Novotný	k1gMnSc1	Novotný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Koncem	koncem	k7c2	koncem
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
a	a	k8xC	a
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
postupné	postupný	k2eAgFnSc3d1	postupná
liberalizaci	liberalizace	k1gFnSc3	liberalizace
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vyvrcholila	vyvrcholit	k5eAaPmAgFnS	vyvrcholit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
abdikoval	abdikovat	k5eAaBmAgMnS	abdikovat
prezident	prezident	k1gMnSc1	prezident
ČSSR	ČSSR	kA	ČSSR
Novotný	Novotný	k1gMnSc1	Novotný
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc7	jeho
nástupcem	nástupce	k1gMnSc7	nástupce
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
armádní	armádní	k2eAgMnSc1d1	armádní
generál	generál	k1gMnSc1	generál
ve	v	k7c6	v
výslužbě	výslužba	k1gFnSc6	výslužba
Ludvík	Ludvík	k1gMnSc1	Ludvík
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Novým	nový	k2eAgMnSc7d1	nový
generálním	generální	k2eAgMnSc7d1	generální
tajemníkem	tajemník	k1gMnSc7	tajemník
KSČ	KSČ	kA	KSČ
byl	být	k5eAaImAgMnS	být
zvolen	zvolen	k2eAgMnSc1d1	zvolen
Slovák	Slovák	k1gMnSc1	Slovák
Alexandr	Alexandr	k1gMnSc1	Alexandr
Dubček	Dubček	k1gMnSc1	Dubček
<g/>
.	.	kIx.	.
</s>
<s>
Liberalizační	liberalizační	k2eAgNnSc1d1	liberalizační
hnutí	hnutí	k1gNnSc1	hnutí
známé	známý	k2eAgNnSc1d1	známé
jako	jako	k8xS	jako
pražské	pražský	k2eAgNnSc1d1	Pražské
jaro	jaro	k1gNnSc1	jaro
bylo	být	k5eAaImAgNnS	být
však	však	k9	však
potlačeno	potlačen	k2eAgNnSc1d1	potlačeno
invazí	invaze	k1gFnSc7	invaze
vojsk	vojsko	k1gNnPc2	vojsko
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
zemí	zem	k1gFnPc2	zem
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
21	[number]	k4	21
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
<s>
Trvalejším	trvalý	k2eAgInSc7d2	trvalejší
státoprávním	státoprávní	k2eAgInSc7d1	státoprávní
důsledkem	důsledek	k1gInSc7	důsledek
pražského	pražský	k2eAgNnSc2d1	Pražské
jara	jaro	k1gNnSc2	jaro
zůstala	zůstat	k5eAaPmAgFnS	zůstat
federalizace	federalizace	k1gFnSc1	federalizace
<g/>
,	,	kIx,	,
nastolená	nastolený	k2eAgFnSc1d1	nastolená
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc3	leden
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
unitární	unitární	k2eAgInSc4d1	unitární
stát	stát	k1gInSc4	stát
formálně	formálně	k6eAd1	formálně
změnila	změnit	k5eAaPmAgFnS	změnit
na	na	k7c4	na
federaci	federace	k1gFnSc4	federace
dvou	dva	k4xCgInPc2	dva
suverénních	suverénní	k2eAgInPc2d1	suverénní
národních	národní	k2eAgInPc2d1	národní
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
obsazení	obsazení	k1gNnSc6	obsazení
Československa	Československo	k1gNnSc2	Československo
emigrovalo	emigrovat	k5eAaBmAgNnS	emigrovat
kolem	kolem	k7c2	kolem
100	[number]	k4	100
000	[number]	k4	000
především	především	k6eAd1	především
vzdělaných	vzdělaný	k2eAgMnPc2d1	vzdělaný
lidí	člověk	k1gMnPc2	člověk
do	do	k7c2	do
demokratických	demokratický	k2eAgFnPc2d1	demokratická
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
další	další	k2eAgFnSc1d1	další
ztráta	ztráta	k1gFnSc1	ztráta
kvalifikovaných	kvalifikovaný	k2eAgMnPc2d1	kvalifikovaný
odborníků	odborník	k1gMnPc2	odborník
ještě	ještě	k6eAd1	ještě
umocnila	umocnit	k5eAaPmAgFnS	umocnit
postupný	postupný	k2eAgInSc4d1	postupný
hospodářský	hospodářský	k2eAgInSc4d1	hospodářský
úpadek	úpadek	k1gInSc4	úpadek
Československa	Československo	k1gNnSc2	Československo
<g/>
,	,	kIx,	,
kterému	který	k3yQgNnSc3	který
byla	být	k5eAaImAgFnS	být
země	země	k1gFnSc1	země
vystavena	vystavit	k5eAaPmNgFnS	vystavit
již	již	k6eAd1	již
od	od	k7c2	od
připojení	připojení	k1gNnSc2	připojení
k	k	k7c3	k
sovětskému	sovětský	k2eAgInSc3d1	sovětský
bloku	blok	k1gInSc3	blok
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
Československa	Československo	k1gNnSc2	Československo
bylo	být	k5eAaImAgNnS	být
fakticky	fakticky	k6eAd1	fakticky
okupováno	okupovat	k5eAaBmNgNnS	okupovat
Sovětskou	sovětský	k2eAgFnSc7d1	sovětská
armádou	armáda	k1gFnSc7	armáda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
definitivně	definitivně	k6eAd1	definitivně
odešla	odejít	k5eAaPmAgFnS	odejít
až	až	k6eAd1	až
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
Režim	režim	k1gInSc1	režim
takzvané	takzvaný	k2eAgFnSc2d1	takzvaná
normalizace	normalizace	k1gFnSc2	normalizace
na	na	k7c4	na
dvacet	dvacet	k4xCc4	dvacet
let	léto	k1gNnPc2	léto
potlačil	potlačit	k5eAaPmAgMnS	potlačit
občanské	občanský	k2eAgFnSc2d1	občanská
svobody	svoboda	k1gFnSc2	svoboda
<g/>
,	,	kIx,	,
čemuž	což	k3yQnSc3	což
se	se	k3xPyFc4	se
snažilo	snažit	k5eAaImAgNnS	snažit
vzdorovat	vzdorovat	k5eAaImF	vzdorovat
zejména	zejména	k9	zejména
hnutí	hnutí	k1gNnPc2	hnutí
Charta	charta	k1gFnSc1	charta
77	[number]	k4	77
<g/>
.	.	kIx.	.
</s>
<s>
Prezidentem	prezident	k1gMnSc7	prezident
ČSSR	ČSSR	kA	ČSSR
v	v	k7c6	v
letech	let	k1gInPc6	let
1975	[number]	k4	1975
až	až	k6eAd1	až
1989	[number]	k4	1989
byl	být	k5eAaImAgMnS	být
Slovák	Slovák	k1gMnSc1	Slovák
Gustáv	Gustáva	k1gFnPc2	Gustáva
Husák	Husák	k1gMnSc1	Husák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Sametová	sametový	k2eAgFnSc1d1	sametová
revoluce	revoluce	k1gFnSc1	revoluce
a	a	k8xC	a
léta	léto	k1gNnSc2	léto
1990	[number]	k4	1990
<g/>
–	–	k?	–
<g/>
1992	[number]	k4	1992
====	====	k?	====
</s>
</p>
<p>
<s>
Sametová	sametový	k2eAgFnSc1d1	sametová
revoluce	revoluce	k1gFnSc1	revoluce
<g/>
,	,	kIx,	,
zahájená	zahájený	k2eAgFnSc1d1	zahájená
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
svrhla	svrhnout	k5eAaPmAgFnS	svrhnout
komunistický	komunistický	k2eAgInSc4d1	komunistický
režim	režim	k1gInSc4	režim
a	a	k8xC	a
umožnila	umožnit	k5eAaPmAgFnS	umožnit
obnovu	obnova	k1gFnSc4	obnova
demokracie	demokracie	k1gFnSc2	demokracie
a	a	k8xC	a
svobodného	svobodný	k2eAgNnSc2d1	svobodné
podnikání	podnikání	k1gNnSc2	podnikání
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
29	[number]	k4	29
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1989	[number]	k4	1989
byl	být	k5eAaImAgMnS	být
prezidentem	prezident	k1gMnSc7	prezident
republiky	republika	k1gFnSc2	republika
zvolen	zvolen	k2eAgMnSc1d1	zvolen
dosavadní	dosavadní	k2eAgMnSc1d1	dosavadní
disident	disident	k1gMnSc1	disident
a	a	k8xC	a
dramatik	dramatik	k1gMnSc1	dramatik
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
<g/>
.	.	kIx.	.
</s>
<s>
Společenská	společenský	k2eAgFnSc1d1	společenská
transformace	transformace	k1gFnSc1	transformace
zároveň	zároveň	k6eAd1	zároveň
způsobila	způsobit	k5eAaPmAgFnS	způsobit
dramatický	dramatický	k2eAgInSc4d1	dramatický
nárůst	nárůst	k1gInSc4	nárůst
kriminality	kriminalita	k1gFnSc2	kriminalita
<g/>
,	,	kIx,	,
značné	značný	k2eAgNnSc1d1	značné
zadlužení	zadlužení	k1gNnSc1	zadlužení
státu	stát	k1gInSc2	stát
a	a	k8xC	a
prohlubování	prohlubování	k1gNnSc2	prohlubování
federalizace	federalizace	k1gFnSc2	federalizace
až	až	k9	až
k	k	k7c3	k
rozpadu	rozpad	k1gInSc3	rozpad
společného	společný	k2eAgInSc2d1	společný
státu	stát	k1gInSc2	stát
Čechů	Čech	k1gMnPc2	Čech
a	a	k8xC	a
Slováků	Slovák	k1gMnPc2	Slovák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
totiž	totiž	k9	totiž
začala	začít	k5eAaPmAgFnS	začít
být	být	k5eAaImF	být
opožděně	opožděně	k6eAd1	opožděně
uváděna	uvádět	k5eAaImNgFnS	uvádět
do	do	k7c2	do
praxe	praxe	k1gFnSc2	praxe
federalizace	federalizace	k1gFnSc2	federalizace
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
sice	sice	k8xC	sice
formálně	formálně	k6eAd1	formálně
platila	platit	k5eAaImAgFnS	platit
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
prakticky	prakticky	k6eAd1	prakticky
byla	být	k5eAaImAgFnS	být
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
zmrazena	zmrazit	k5eAaPmNgNnP	zmrazit
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
oběma	dva	k4xCgFnPc7	dva
složkami	složka	k1gFnPc7	složka
federace	federace	k1gFnSc2	federace
<g/>
,	,	kIx,	,
Českou	český	k2eAgFnSc7d1	Česká
republikou	republika	k1gFnSc7	republika
a	a	k8xC	a
Slovenskou	slovenský	k2eAgFnSc7d1	slovenská
republikou	republika	k1gFnSc7	republika
<g/>
,	,	kIx,	,
rychle	rychle	k6eAd1	rychle
narůstaly	narůstat	k5eAaImAgInP	narůstat
rozpory	rozpor	k1gInPc1	rozpor
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
pomlčková	pomlčkový	k2eAgFnSc1d1	pomlčková
válka	válka	k1gFnSc1	válka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
nakonec	nakonec	k6eAd1	nakonec
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
rozpadu	rozpad	k1gInSc3	rozpad
společného	společný	k2eAgInSc2d1	společný
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Československo	Československo	k1gNnSc1	Československo
zaniklo	zaniknout	k5eAaPmAgNnS	zaniknout
mírovou	mírový	k2eAgFnSc7d1	mírová
cestou	cesta	k1gFnSc7	cesta
ke	k	k7c3	k
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosinci	prosinec	k1gInSc3	prosinec
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Dosavadní	dosavadní	k2eAgFnPc1d1	dosavadní
národní	národní	k2eAgFnPc1d1	národní
republiky	republika	k1gFnPc1	republika
převzaly	převzít	k5eAaPmAgFnP	převzít
právní	právní	k2eAgInSc4d1	právní
řád	řád	k1gInSc4	řád
zanikající	zanikající	k2eAgFnSc2d1	zanikající
federace	federace	k1gFnSc2	federace
a	a	k8xC	a
rozdělily	rozdělit	k5eAaPmAgFnP	rozdělit
si	se	k3xPyFc3	se
její	její	k3xOp3gInSc4	její
majetek	majetek	k1gInSc4	majetek
a	a	k8xC	a
závazky	závazek	k1gInPc4	závazek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Samostatnost	samostatnost	k1gFnSc1	samostatnost
Česka	Česko	k1gNnSc2	Česko
===	===	k?	===
</s>
</p>
<p>
<s>
Samostatnost	samostatnost	k1gFnSc4
nabylo	nabýt	k5eAaPmAgNnS
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1993	#num#	k4
jako	jako	k8xC,k8xS
nástupnický	nástupnický	k2eAgInSc1d1
stát	stát	k1gInSc1
Československa	Československo	k1gNnSc2
<g/>
,	,	kIx,
předtím	předtím	k6eAd1
existovalo	existovat	k5eAaImAgNnS
jako	jako	k9
jedna	jeden	k4xCgFnSc1
ze	z	k7c2
dvou	dva	k4xCgFnPc2
republik	republika	k1gFnPc2
československé	československý	k2eAgFnSc2d1
federace	federace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Zapojila	zapojit	k5eAaPmAgFnS	zapojit
se	se	k3xPyFc4	se
do	do	k7c2	do
západoevropských	západoevropský	k2eAgFnPc2d1	západoevropská
politických	politický	k2eAgFnPc2d1	politická
struktur	struktura	k1gFnPc2	struktura
<g/>
.	.	kIx.	.
</s>
<s>
12	[number]	k4	12
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1999	[number]	k4	1999
byla	být	k5eAaImAgFnS	být
přijata	přijmout	k5eAaPmNgFnS	přijmout
do	do	k7c2	do
NATO	NATO	kA	NATO
a	a	k8xC	a
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2004	[number]	k4	2004
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
do	do	k7c2	do
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
přistoupila	přistoupit	k5eAaPmAgFnS	přistoupit
k	k	k7c3	k
Schengenským	schengenský	k2eAgFnPc3d1	Schengenská
dohodám	dohoda	k1gFnPc3	dohoda
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejichž	jejichž	k3xOyRp3gInSc6	jejichž
základě	základ	k1gInSc6	základ
se	s	k7c7	s
21	[number]	k4	21
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2007	[number]	k4	2007
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
Schengenského	schengenský	k2eAgInSc2d1	schengenský
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
<g/>
Prezidentem	prezident	k1gMnSc7	prezident
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
byl	být	k5eAaImAgInS	být
až	až	k6eAd1	až
do	do	k7c2	do
března	březen	k1gInSc2	březen
2003	[number]	k4	2003
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
již	již	k6eAd1	již
československým	československý	k2eAgMnSc7d1	československý
prezidentem	prezident	k1gMnSc7	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
nástupcem	nástupce	k1gMnSc7	nástupce
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
7	[number]	k4	7
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
podruhé	podruhé	k6eAd1	podruhé
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
března	březen	k1gInSc2	březen
2013	[number]	k4	2013
je	být	k5eAaImIp3nS	být
prezidentem	prezident	k1gMnSc7	prezident
republiky	republika	k1gFnSc2	republika
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
prvním	první	k4xOgMnSc7	první
prezidentem	prezident	k1gMnSc7	prezident
zvoleným	zvolený	k2eAgMnSc7d1	zvolený
v	v	k7c6	v
lidovém	lidový	k2eAgNnSc6d1	lidové
hlasování	hlasování	k1gNnSc6	hlasování
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
svůj	svůj	k3xOyFgInSc4	svůj
mandát	mandát	k1gInSc4	mandát
obhájil	obhájit	k5eAaPmAgMnS	obhájit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
Česko	Česko	k1gNnSc1	Česko
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
sousedí	sousedit	k5eAaImIp3nP	sousedit
se	s	k7c7	s
čtyřmi	čtyři	k4xCgInPc7	čtyři
státy	stát	k1gInPc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
západě	západ	k1gInSc6	západ
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
Německo	Německo	k1gNnSc4	Německo
<g/>
,	,	kIx,	,
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
o	o	k7c4	o
Polsko	Polsko	k1gNnSc4	Polsko
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
o	o	k7c4	o
Slovensko	Slovensko	k1gNnSc4	Slovensko
a	a	k8xC	a
jižní	jižní	k2eAgFnSc6d1	jižní
hranici	hranice	k1gFnSc6	hranice
sdílí	sdílet	k5eAaImIp3nS	sdílet
s	s	k7c7	s
Rakouskem	Rakousko	k1gNnSc7	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
západní	západní	k2eAgFnSc2d1	západní
společné	společný	k2eAgFnSc2d1	společná
hranice	hranice	k1gFnSc2	hranice
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
činí	činit	k5eAaImIp3nS	činit
810,7	[number]	k4	810,7
km	km	kA	km
<g/>
,	,	kIx,	,
s	s	k7c7	s
Rakouskem	Rakousko	k1gNnSc7	Rakousko
je	být	k5eAaImIp3nS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
466,1	[number]	k4	466,1
km	km	kA	km
<g/>
,	,	kIx,	,
se	s	k7c7	s
Slovenskem	Slovensko	k1gNnSc7	Slovensko
251,8	[number]	k4	251,8
km	km	kA	km
a	a	k8xC	a
s	s	k7c7	s
Polskem	Polsko	k1gNnSc7	Polsko
na	na	k7c6	na
severu	sever	k1gInSc6	sever
761,8	[number]	k4	761,8
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
rozloha	rozloha	k1gFnSc1	rozloha
činí	činit	k5eAaImIp3nS	činit
78	[number]	k4	78
866	[number]	k4	866
km2	km2	k4	km2
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
2	[number]	k4	2
%	%	kIx~	%
tvoří	tvořit	k5eAaImIp3nP	tvořit
vodní	vodní	k2eAgFnPc1d1	vodní
plochy	plocha	k1gFnPc1	plocha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Geologie	geologie	k1gFnSc2	geologie
<g/>
,	,	kIx,	,
geomorfologie	geomorfologie	k1gFnSc2	geomorfologie
a	a	k8xC	a
půdy	půda	k1gFnSc2	půda
===	===	k?	===
</s>
</p>
<p>
<s>
Převážná	převážný	k2eAgFnSc1d1	převážná
část	část	k1gFnSc1	část
území	území	k1gNnSc2	území
náleží	náležet	k5eAaImIp3nS	náležet
ke	k	k7c3	k
geologicky	geologicky	k6eAd1	geologicky
stabilnímu	stabilní	k2eAgInSc3d1	stabilní
Českému	český	k2eAgInSc3d1	český
masivu	masiv	k1gInSc3	masiv
<g/>
,	,	kIx,	,
vyzdviženému	vyzdvižený	k2eAgMnSc3d1	vyzdvižený
hercynským	hercynský	k2eAgNnPc3d1	hercynské
vrásněním	vrásnění	k1gNnPc3	vrásnění
v	v	k7c6	v
období	období	k1gNnSc6	období
devonu	devon	k1gInSc2	devon
a	a	k8xC	a
karbonu	karbon	k1gInSc2	karbon
(	(	kIx(	(
<g/>
v	v	k7c6	v
prvohorách	prvohory	k1gFnPc6	prvohory
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
Západních	západní	k2eAgInPc2d1	západní
Karpat	Karpaty	k1gInPc2	Karpaty
na	na	k7c6	na
východě	východ	k1gInSc6	východ
území	území	k1gNnSc2	území
je	být	k5eAaImIp3nS	být
mladší	mladý	k2eAgFnSc1d2	mladší
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
vyzdvižena	vyzdvihnout	k5eAaPmNgFnS	vyzdvihnout
alpínským	alpínský	k2eAgNnSc7d1	alpínské
vrásněním	vrásnění	k1gNnSc7	vrásnění
v	v	k7c6	v
období	období	k1gNnSc6	období
třetihor	třetihory	k1gFnPc2	třetihory
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
geomorfologického	geomorfologický	k2eAgNnSc2d1	Geomorfologické
hlediska	hledisko	k1gNnSc2	hledisko
leží	ležet	k5eAaImIp3nS	ležet
Česko	Česko	k1gNnSc1	Česko
na	na	k7c4	na
rozhraní	rozhraní	k1gNnSc4	rozhraní
dvou	dva	k4xCgFnPc2	dva
horských	horský	k2eAgFnPc2d1	horská
soustav	soustava	k1gFnPc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgFnSc1d1	západní
a	a	k8xC	a
střední	střední	k2eAgFnSc1d1	střední
část	část	k1gFnSc1	část
vyplňuje	vyplňovat	k5eAaImIp3nS	vyplňovat
Česká	český	k2eAgFnSc1d1	Česká
vysočina	vysočina	k1gFnSc1	vysočina
<g/>
,	,	kIx,	,
mající	mající	k2eAgMnSc1d1	mající
převážně	převážně	k6eAd1	převážně
ráz	ráz	k1gInSc4	ráz
pahorkatin	pahorkatina	k1gFnPc2	pahorkatina
až	až	k8xS	až
vrchovin	vrchovina	k1gFnPc2	vrchovina
(	(	kIx(	(
<g/>
Šumava	Šumava	k1gFnSc1	Šumava
<g/>
,	,	kIx,	,
Český	český	k2eAgInSc1d1	český
les	les	k1gInSc1	les
<g/>
,	,	kIx,	,
Krušné	krušný	k2eAgFnPc1d1	krušná
hory	hora	k1gFnPc1	hora
<g/>
,	,	kIx,	,
Děčínská	děčínský	k2eAgFnSc1d1	Děčínská
vrchovina	vrchovina	k1gFnSc1	vrchovina
<g/>
,	,	kIx,	,
Jizerské	jizerský	k2eAgFnPc1d1	Jizerská
hory	hora	k1gFnPc1	hora
<g/>
,	,	kIx,	,
Krkonoše	Krkonoše	k1gFnPc1	Krkonoše
<g/>
,	,	kIx,	,
Orlické	orlický	k2eAgFnPc1d1	Orlická
hory	hora	k1gFnPc1	hora
<g/>
,	,	kIx,	,
Králický	králický	k2eAgInSc1d1	králický
Sněžník	Sněžník	k1gInSc1	Sněžník
<g/>
,	,	kIx,	,
Jeseníky	Jeseník	k1gInPc1	Jeseník
<g/>
,	,	kIx,	,
Českomoravská	českomoravský	k2eAgFnSc1d1	Českomoravská
vrchovina	vrchovina	k1gFnSc1	vrchovina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
východní	východní	k2eAgFnSc2d1	východní
části	část	k1gFnSc2	část
státu	stát	k1gInSc2	stát
zasahují	zasahovat	k5eAaImIp3nP	zasahovat
Západní	západní	k2eAgInPc1d1	západní
Karpaty	Karpaty	k1gInPc1	Karpaty
(	(	kIx(	(
<g/>
Moravskoslezské	moravskoslezský	k2eAgFnPc1d1	Moravskoslezská
Beskydy	Beskydy	k1gFnPc1	Beskydy
<g/>
,	,	kIx,	,
Bílé	bílý	k2eAgInPc1d1	bílý
Karpaty	Karpaty	k1gInPc1	Karpaty
<g/>
,	,	kIx,	,
Javorníky	Javorník	k1gInPc1	Javorník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
celkové	celkový	k2eAgFnSc2d1	celková
plochy	plocha	k1gFnSc2	plocha
Česka	Česko	k1gNnSc2	Česko
leží	ležet	k5eAaImIp3nS	ležet
52	[number]	k4	52
817	[number]	k4	817
km	km	kA	km
<g/>
2	[number]	k4	2
(	(	kIx(	(
<g/>
67	[number]	k4	67
%	%	kIx~	%
<g/>
)	)	kIx)	)
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
do	do	k7c2	do
500	[number]	k4	500
m	m	kA	m
<g/>
,	,	kIx,	,
25	[number]	k4	25
222	[number]	k4	222
km	km	kA	km
<g/>
2	[number]	k4	2
(	(	kIx(	(
<g/>
32	[number]	k4	32
%	%	kIx~	%
<g/>
)	)	kIx)	)
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
500	[number]	k4	500
až	až	k9	až
1	[number]	k4	1
000	[number]	k4	000
m	m	kA	m
a	a	k8xC	a
pouze	pouze	k6eAd1	pouze
827	[number]	k4	827
km	km	kA	km
<g/>
2	[number]	k4	2
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1,05	[number]	k4	1,05
%	%	kIx~	%
<g/>
)	)	kIx)	)
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
nad	nad	k7c7	nad
1	[number]	k4	1
000	[number]	k4	000
m	m	kA	m
<g/>
;	;	kIx,	;
střední	střední	k2eAgFnSc1d1	střední
nadmořská	nadmořský	k2eAgFnSc1d1	nadmořská
výška	výška	k1gFnSc1	výška
činí	činit	k5eAaImIp3nS	činit
430	[number]	k4	430
m.	m.	k?	m.
</s>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
českým	český	k2eAgInSc7d1	český
vrcholem	vrchol	k1gInSc7	vrchol
je	být	k5eAaImIp3nS	být
hora	hora	k1gFnSc1	hora
Sněžka	Sněžka	k1gFnSc1	Sněžka
s	s	k7c7	s
1603	[number]	k4	1603
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
,	,	kIx,	,
nejnižším	nízký	k2eAgInSc7d3	nejnižší
pak	pak	k6eAd1	pak
Labe	Labe	k1gNnPc4	Labe
na	na	k7c6	na
odtoku	odtok	k1gInSc6	odtok
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
u	u	k7c2	u
Hřenska	Hřensko	k1gNnSc2	Hřensko
se	s	k7c7	s
115	[number]	k4	115
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
</s>
<s>
Hranická	hranický	k2eAgFnSc1d1	Hranická
propast	propast	k1gFnSc1	propast
je	být	k5eAaImIp3nS	být
nejhlubší	hluboký	k2eAgFnSc7d3	nejhlubší
zatopenou	zatopený	k2eAgFnSc7d1	zatopená
jeskyní	jeskyně	k1gFnSc7	jeskyně
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Nejrozsáhlejším	rozsáhlý	k2eAgNnSc7d3	nejrozsáhlejší
krasovým	krasový	k2eAgNnSc7d1	krasové
územím	území	k1gNnSc7	území
je	být	k5eAaImIp3nS	být
Moravský	moravský	k2eAgInSc1d1	moravský
kras	kras	k1gInSc1	kras
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
vyvřelých	vyvřelý	k2eAgFnPc2d1	vyvřelá
hornin	hornina	k1gFnPc2	hornina
v	v	k7c6	v
ČR	ČR	kA	ČR
převládají	převládat	k5eAaImIp3nP	převládat
žula	žula	k1gFnSc1	žula
<g/>
,	,	kIx,	,
čedič	čedič	k1gInSc1	čedič
a	a	k8xC	a
znělec	znělec	k1gInSc1	znělec
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
usazených	usazený	k2eAgMnPc2d1	usazený
pískovec	pískovec	k1gInSc4	pískovec
<g/>
,	,	kIx,	,
vápenec	vápenec	k1gInSc4	vápenec
a	a	k8xC	a
břidlice	břidlice	k1gFnPc4	břidlice
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
přeměněných	přeměněný	k2eAgMnPc2d1	přeměněný
rula	rout	k5eAaImAgFnS	rout
<g/>
,	,	kIx,	,
svor	svor	k1gInSc1	svor
a	a	k8xC	a
fylit	fylit	k1gInSc1	fylit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Půdní	půdní	k2eAgInSc1d1	půdní
pokryv	pokryv	k1gInSc1	pokryv
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
značnou	značný	k2eAgFnSc7d1	značná
variabilitou	variabilita	k1gFnSc7	variabilita
<g/>
.	.	kIx.	.
</s>
<s>
Nejrozšířenějším	rozšířený	k2eAgInSc7d3	nejrozšířenější
typem	typ	k1gInSc7	typ
půd	půda	k1gFnPc2	půda
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
jsou	být	k5eAaImIp3nP	být
hnědé	hnědý	k2eAgFnPc1d1	hnědá
půdy	půda	k1gFnPc1	půda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nížinách	nížina	k1gFnPc6	nížina
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nP	nacházet
úrodné	úrodný	k2eAgFnPc1d1	úrodná
černozemě	černozem	k1gFnPc1	černozem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
členění	členění	k1gNnSc2	členění
krajiny	krajina	k1gFnSc2	krajina
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
území	území	k1gNnSc6	území
ČR	ČR	kA	ČR
zastoupeny	zastoupit	k5eAaPmNgFnP	zastoupit
čtyři	čtyři	k4xCgFnPc1	čtyři
biogeografické	biogeografický	k2eAgFnPc1d1	biogeografická
podprovincie	podprovincie	k1gFnPc1	podprovincie
<g/>
:	:	kIx,	:
Celé	celý	k2eAgNnSc1d1	celé
území	území	k1gNnSc1	území
Čech	Čechy	k1gFnPc2	Čechy
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
hercynská	hercynský	k2eAgFnSc1d1	hercynská
podprovincie	podprovincie	k1gFnSc1	podprovincie
<g/>
,	,	kIx,	,
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
a	a	k8xC	a
ve	v	k7c6	v
Slezsku	Slezsko	k1gNnSc6	Slezsko
pak	pak	k6eAd1	pak
lze	lze	k6eAd1	lze
identifikovat	identifikovat	k5eAaBmF	identifikovat
polonskou	polonský	k2eAgFnSc4d1	polonský
podprovincii	podprovincie	k1gFnSc4	podprovincie
<g/>
,	,	kIx,	,
západokarpatskou	západokarpatský	k2eAgFnSc4d1	západokarpatská
podprovincii	podprovincie	k1gFnSc4	podprovincie
a	a	k8xC	a
severopanonskou	severopanonský	k2eAgFnSc4d1	severopanonský
podprovincii	podprovincie	k1gFnSc4	podprovincie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
podobné	podobný	k2eAgFnSc6d1	podobná
typologii	typologie	k1gFnSc6	typologie
ekoregionů	ekoregion	k1gInPc2	ekoregion
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
užívá	užívat	k5eAaImIp3nS	užívat
Světový	světový	k2eAgInSc1d1	světový
fond	fond	k1gInSc1	fond
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
přírody	příroda	k1gFnSc2	příroda
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nP	tvořit
území	území	k1gNnSc4	území
Čech	Čechy	k1gFnPc2	Čechy
převážně	převážně	k6eAd1	převážně
středoevropský	středoevropský	k2eAgInSc4d1	středoevropský
smíšený	smíšený	k2eAgInSc4d1	smíšený
les	les	k1gInSc4	les
a	a	k8xC	a
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
a	a	k8xC	a
ve	v	k7c6	v
Slezsku	Slezsko	k1gNnSc6	Slezsko
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
malé	malý	k2eAgFnPc1d1	malá
enklávy	enkláva	k1gFnPc1	enkláva
panonského	panonský	k2eAgInSc2d1	panonský
smíšeného	smíšený	k2eAgInSc2d1	smíšený
lesa	les	k1gInSc2	les
<g/>
,	,	kIx,	,
západoevropského	západoevropský	k2eAgInSc2d1	západoevropský
listnatého	listnatý	k2eAgInSc2d1	listnatý
lesa	les	k1gInSc2	les
a	a	k8xC	a
karpatského	karpatský	k2eAgInSc2d1	karpatský
jehličnatého	jehličnatý	k2eAgInSc2d1	jehličnatý
lesa	les	k1gInSc2	les
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Hydrologie	hydrologie	k1gFnSc2	hydrologie
a	a	k8xC	a
podnebí	podnebí	k1gNnSc2	podnebí
===	===	k?	===
</s>
</p>
<p>
<s>
Českým	český	k2eAgNnSc7d1	české
územím	území	k1gNnSc7	území
prochází	procházet	k5eAaImIp3nS	procházet
hlavní	hlavní	k2eAgNnSc4d1	hlavní
evropské	evropský	k2eAgNnSc4d1	Evropské
rozvodí	rozvodí	k1gNnSc4	rozvodí
oddělující	oddělující	k2eAgNnSc1d1	oddělující
úmoří	úmoří	k1gNnSc1	úmoří
Severního	severní	k2eAgNnSc2d1	severní
<g/>
,	,	kIx,	,
Baltského	baltský	k2eAgNnSc2d1	Baltské
a	a	k8xC	a
Černého	Černého	k2eAgNnSc2d1	Černého
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnPc1d1	hlavní
říční	říční	k2eAgFnPc1d1	říční
osy	osa	k1gFnPc1	osa
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
Labe	Labe	k1gNnSc2	Labe
(	(	kIx(	(
<g/>
370	[number]	k4	370
km	km	kA	km
<g/>
)	)	kIx)	)
s	s	k7c7	s
Vltavou	Vltava	k1gFnSc7	Vltava
(	(	kIx(	(
<g/>
433	[number]	k4	433
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
řeka	řeka	k1gFnSc1	řeka
Morava	Morava	k1gFnSc1	Morava
(	(	kIx(	(
<g/>
246	[number]	k4	246
km	km	kA	km
<g/>
)	)	kIx)	)
s	s	k7c7	s
Dyjí	Dyje	k1gFnSc7	Dyje
(	(	kIx(	(
<g/>
306	[number]	k4	306
km	km	kA	km
<g/>
)	)	kIx)	)
a	a	k8xC	a
ve	v	k7c6	v
Slezsku	Slezsko	k1gNnSc6	Slezsko
Odra	Odra	k1gFnSc1	Odra
(	(	kIx(	(
<g/>
135	[number]	k4	135
km	km	kA	km
<g/>
)	)	kIx)	)
s	s	k7c7	s
Opavou	Opava	k1gFnSc7	Opava
(	(	kIx(	(
<g/>
131	[number]	k4	131
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
tok	tok	k1gInSc1	tok
mají	mít	k5eAaImIp3nP	mít
na	na	k7c6	na
území	území	k1gNnSc6	území
ČR	ČR	kA	ČR
též	též	k9	též
Ohře	Ohře	k1gFnSc2	Ohře
(	(	kIx(	(
<g/>
246	[number]	k4	246
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Sázava	Sázava	k1gFnSc1	Sázava
(	(	kIx(	(
<g/>
225	[number]	k4	225
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jihlava	Jihlava	k1gFnSc1	Jihlava
(	(	kIx(	(
<g/>
180	[number]	k4	180
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Svratka	Svratka	k1gFnSc1	Svratka
(	(	kIx(	(
<g/>
168	[number]	k4	168
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jizera	Jizera	k1gFnSc1	Jizera
(	(	kIx(	(
<g/>
167	[number]	k4	167
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Lužnice	Lužnice	k1gFnSc1	Lužnice
(	(	kIx(	(
<g/>
157	[number]	k4	157
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Berounka	Berounka	k1gFnSc1	Berounka
(	(	kIx(	(
<g/>
139	[number]	k4	139
km	km	kA	km
<g/>
)	)	kIx)	)
a	a	k8xC	a
Otava	Otava	k1gFnSc1	Otava
(	(	kIx(	(
<g/>
111	[number]	k4	111
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgNnSc7d3	veliký
přírodním	přírodní	k2eAgNnSc7d1	přírodní
jezerem	jezero	k1gNnSc7	jezero
Česka	Česko	k1gNnSc2	Česko
je	být	k5eAaImIp3nS	být
Černé	Černé	k2eAgNnSc1d1	Černé
jezero	jezero	k1gNnSc1	jezero
na	na	k7c6	na
Šumavě	Šumava	k1gFnSc6	Šumava
<g/>
.	.	kIx.	.
</s>
<s>
Podnebí	podnebí	k1gNnSc1	podnebí
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
mírné	mírný	k2eAgFnSc2d1	mírná
<g/>
,	,	kIx,	,
přechodné	přechodný	k2eAgFnSc2d1	přechodná
mezi	mezi	k7c7	mezi
kontinentálním	kontinentální	k2eAgInSc7d1	kontinentální
a	a	k8xC	a
oceánickým	oceánický	k2eAgInSc7d1	oceánický
typem	typ	k1gInSc7	typ
<g/>
.	.	kIx.	.
</s>
<s>
Typické	typický	k2eAgNnSc1d1	typické
je	být	k5eAaImIp3nS	být
střídání	střídání	k1gNnSc1	střídání
čtyř	čtyři	k4xCgFnPc2	čtyři
ročních	roční	k2eAgFnPc2d1	roční
období	období	k1gNnPc2	období
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
charakterizováno	charakterizovat	k5eAaBmNgNnS	charakterizovat
převládajícím	převládající	k2eAgNnSc7d1	převládající
západním	západní	k2eAgNnSc7d1	západní
prouděním	proudění	k1gNnSc7	proudění
a	a	k8xC	a
intenzivní	intenzivní	k2eAgFnSc7d1	intenzivní
cyklonální	cyklonální	k2eAgFnSc7d1	cyklonální
činností	činnost	k1gFnSc7	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Přímořský	přímořský	k2eAgInSc1d1	přímořský
vliv	vliv	k1gInSc1	vliv
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
,	,	kIx,	,
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
a	a	k8xC	a
ve	v	k7c6	v
Slezsku	Slezsko	k1gNnSc6	Slezsko
již	již	k9	již
přibývá	přibývat	k5eAaImIp3nS	přibývat
kontinentálních	kontinentální	k2eAgInPc2d1	kontinentální
podnebných	podnebný	k2eAgInPc2d1	podnebný
vlivů	vliv	k1gInPc2	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc4d3	veliký
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
podnebí	podnebí	k1gNnSc4	podnebí
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
však	však	k9	však
má	mít	k5eAaImIp3nS	mít
nadmořská	nadmořský	k2eAgFnSc1d1	nadmořská
výška	výška	k1gFnSc1	výška
a	a	k8xC	a
reliéf	reliéf	k1gInSc1	reliéf
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Typické	typický	k2eAgFnPc1d1	typická
jsou	být	k5eAaImIp3nP	být
hojné	hojný	k2eAgFnPc1d1	hojná
srážky	srážka	k1gFnPc1	srážka
a	a	k8xC	a
přechody	přechod	k1gInPc1	přechod
frontálních	frontální	k2eAgInPc2d1	frontální
systémů	systém	k1gInPc2	systém
–	–	k?	–
ročně	ročně	k6eAd1	ročně
jich	on	k3xPp3gFnPc2	on
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
přes	přes	k7c4	přes
území	území	k1gNnSc4	území
Česka	Česko	k1gNnSc2	Česko
projde	projít	k5eAaPmIp3nS	projít
140	[number]	k4	140
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
srážek	srážka	k1gFnPc2	srážka
spadne	spadnout	k5eAaPmIp3nS	spadnout
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
nebo	nebo	k8xC	nebo
červenci	červenec	k1gInSc6	červenec
<g/>
,	,	kIx,	,
nejméně	málo	k6eAd3	málo
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
nebo	nebo	k8xC	nebo
únoru	únor	k1gInSc6	únor
<g/>
.	.	kIx.	.
</s>
<s>
Nejsrážkovějším	srážkový	k2eAgNnSc7d3	srážkový
místem	místo	k1gNnSc7	místo
v	v	k7c6	v
ČR	ČR	kA	ČR
jsou	být	k5eAaImIp3nP	být
Jizerské	jizerský	k2eAgFnPc1d1	Jizerská
hory	hora	k1gFnPc1	hora
(	(	kIx(	(
<g/>
zejm.	zejm.	k?	zejm.
oblast	oblast	k1gFnSc4	oblast
Bílého	bílý	k2eAgInSc2d1	bílý
Potoka	potok	k1gInSc2	potok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejsušším	suchý	k2eAgMnSc7d3	nejsušší
pak	pak	k6eAd1	pak
Libědice	Libědice	k1gFnSc2	Libědice
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Chomutov	Chomutov	k1gInSc1	Chomutov
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgNnPc1d1	ležící
ve	v	k7c6	v
srážkovém	srážkový	k2eAgInSc6d1	srážkový
stínu	stín	k1gInSc6	stín
Krušných	krušný	k2eAgFnPc2d1	krušná
hor.	hor.	k?	hor.
<g/>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
roční	roční	k2eAgFnSc1d1	roční
teplota	teplota	k1gFnSc1	teplota
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c7	mezi
5,5	[number]	k4	5,5
°	°	k?	°
<g/>
C	C	kA	C
až	až	k9	až
9	[number]	k4	9
°	°	k?	°
<g/>
C.	C.	kA	C.
Nejchladnějším	chladný	k2eAgInSc7d3	nejchladnější
měsícem	měsíc	k1gInSc7	měsíc
roku	rok	k1gInSc2	rok
je	být	k5eAaImIp3nS	být
leden	leden	k1gInSc1	leden
<g/>
,	,	kIx,	,
nejteplejším	teplý	k2eAgInSc7d3	nejteplejší
červenec	červenec	k1gInSc4	červenec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dlouhodobém	dlouhodobý	k2eAgInSc6d1	dlouhodobý
průměru	průměr	k1gInSc6	průměr
je	být	k5eAaImIp3nS	být
dělí	dělit	k5eAaImIp3nS	dělit
20	[number]	k4	20
°	°	k?	°
<g/>
C.	C.	kA	C.
Tropických	tropický	k2eAgInPc2d1	tropický
dnů	den	k1gInPc2	den
je	být	k5eAaImIp3nS	být
zaznamenáváno	zaznamenávat	k5eAaImNgNnS	zaznamenávat
průměrně	průměrně	k6eAd1	průměrně
12	[number]	k4	12
ročně	ročně	k6eAd1	ročně
<g/>
,	,	kIx,	,
tropické	tropický	k2eAgFnPc1d1	tropická
noci	noc	k1gFnPc1	noc
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
vzácné	vzácný	k2eAgInPc1d1	vzácný
<g/>
.	.	kIx.	.
</s>
<s>
Arktické	arktický	k2eAgInPc1d1	arktický
dny	den	k1gInPc1	den
bývají	bývat	k5eAaImIp3nP	bývat
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Nejteplejšími	teplý	k2eAgNnPc7d3	nejteplejší
místy	místo	k1gNnPc7	místo
jsou	být	k5eAaImIp3nP	být
oblasti	oblast	k1gFnSc6	oblast
Dyjsko-svrateckého	dyjskovratecký	k2eAgInSc2d1	dyjsko-svratecký
a	a	k8xC	a
Dolnomoravského	dolnomoravský	k2eAgInSc2d1	dolnomoravský
úvalu	úval	k1gInSc2	úval
a	a	k8xC	a
pak	pak	k6eAd1	pak
velká	velký	k2eAgNnPc1d1	velké
města	město	k1gNnPc1	město
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
teplotu	teplota	k1gFnSc4	teplota
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
hustá	hustý	k2eAgFnSc1d1	hustá
zástavba	zástavba	k1gFnSc1	zástavba
<g/>
.	.	kIx.	.
</s>
<s>
Nejstudenějším	studený	k2eAgNnSc7d3	nejstudenější
místem	místo	k1gNnSc7	místo
je	být	k5eAaImIp3nS	být
vrchol	vrchol	k1gInSc4	vrchol
Sněžky	Sněžka	k1gFnSc2	Sněžka
<g/>
.	.	kIx.	.
</s>
<s>
Největrnějším	větrný	k2eAgNnSc7d3	největrnější
místem	místo	k1gNnSc7	místo
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
je	být	k5eAaImIp3nS	být
vrchol	vrchol	k1gInSc4	vrchol
Milešovky	Milešovka	k1gFnSc2	Milešovka
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
místo	místo	k1gNnSc4	místo
s	s	k7c7	s
největším	veliký	k2eAgInSc7d3	veliký
počtem	počet	k1gInSc7	počet
bouří	bouř	k1gFnPc2	bouř
v	v	k7c6	v
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Fauna	Faun	k1gMnSc4	Faun
a	a	k8xC	a
flora	flor	k1gMnSc4	flor
===	===	k?	===
</s>
</p>
<p>
<s>
Flóra	Flóra	k1gFnSc1	Flóra
a	a	k8xC	a
fauna	fauna	k1gFnSc1	fauna
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
svědčí	svědčit	k5eAaImIp3nS	svědčit
o	o	k7c6	o
vzájemném	vzájemný	k2eAgNnSc6d1	vzájemné
prolínání	prolínání	k1gNnSc6	prolínání
hlavních	hlavní	k2eAgInPc2d1	hlavní
směrů	směr	k1gInPc2	směr
<g/>
,	,	kIx,	,
kterými	který	k3yQgInPc7	který
se	se	k3xPyFc4	se
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
šířilo	šířit	k5eAaImAgNnS	šířit
rostlinstvo	rostlinstvo	k1gNnSc1	rostlinstvo
a	a	k8xC	a
živočišstvo	živočišstvo	k1gNnSc1	živočišstvo
<g/>
.	.	kIx.	.
</s>
<s>
Lesy	les	k1gInPc1	les
zaujímají	zaujímat	k5eAaImIp3nP	zaujímat
33	[number]	k4	33
%	%	kIx~	%
celkové	celkový	k2eAgFnSc2d1	celková
rozlohy	rozloha	k1gFnSc2	rozloha
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Českou	český	k2eAgFnSc4d1	Česká
republiku	republika	k1gFnSc4	republika
jsou	být	k5eAaImIp3nP	být
typické	typický	k2eAgInPc1d1	typický
smíšené	smíšený	k2eAgInPc1d1	smíšený
dubové	dubový	k2eAgInPc1d1	dubový
<g/>
,	,	kIx,	,
jedlové	jedlový	k2eAgInPc1d1	jedlový
a	a	k8xC	a
smrkové	smrkový	k2eAgInPc1d1	smrkový
lesy	les	k1gInPc1	les
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhové	druhový	k2eAgFnSc6d1	druhová
skladbě	skladba	k1gFnSc6	skladba
ovšem	ovšem	k9	ovšem
převládají	převládat	k5eAaImIp3nP	převládat
jehličnany	jehličnan	k1gInPc4	jehličnan
(	(	kIx(	(
<g/>
asi	asi	k9	asi
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
třetin	třetina	k1gFnPc2	třetina
<g/>
)	)	kIx)	)
oproti	oproti	k7c3	oproti
listnáčům	listnáč	k1gInPc3	listnáč
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
přirozený	přirozený	k2eAgInSc1d1	přirozený
původní	původní	k2eAgInSc1d1	původní
poměr	poměr	k1gInSc1	poměr
byl	být	k5eAaImAgInS	být
obrácený	obrácený	k2eAgMnSc1d1	obrácený
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
tuto	tento	k3xDgFnSc4	tento
změnu	změna	k1gFnSc4	změna
může	moct	k5eAaImIp3nS	moct
masivní	masivní	k2eAgFnSc1d1	masivní
výsadba	výsadba	k1gFnSc1	výsadba
zejména	zejména	k9	zejména
smrčin	smrčina	k1gFnPc2	smrčina
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
začala	začít	k5eAaPmAgFnS	začít
na	na	k7c6	na
našem	náš	k3xOp1gNnSc6	náš
území	území	k1gNnSc6	území
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českých	český	k2eAgInPc6d1	český
lesích	les	k1gInPc6	les
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
živočišstvo	živočišstvo	k1gNnSc1	živočišstvo
typické	typický	k2eAgNnSc1d1	typické
pro	pro	k7c4	pro
biom	biom	k1gInSc4	biom
smíšených	smíšený	k2eAgInPc2d1	smíšený
lesů	les	k1gInPc2	les
mírného	mírný	k2eAgInSc2d1	mírný
podnebného	podnebný	k2eAgInSc2d1	podnebný
pásu	pás	k1gInSc2	pás
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
ČR	ČR	kA	ČR
roste	růst	k5eAaImIp3nS	růst
zhruba	zhruba	k6eAd1	zhruba
3,5	[number]	k4	3,5
tisíce	tisíc	k4xCgInSc2	tisíc
druhů	druh	k1gInPc2	druh
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
přes	přes	k7c4	přes
2,5	[number]	k4	2,5
tisíce	tisíc	k4xCgInSc2	tisíc
je	být	k5eAaImIp3nS	být
původních	původní	k2eAgFnPc2d1	původní
<g/>
.	.	kIx.	.
</s>
<s>
Pěstováno	pěstován	k2eAgNnSc1d1	pěstováno
je	být	k5eAaImIp3nS	být
500	[number]	k4	500
běžnějších	běžný	k2eAgInPc2d2	běžnější
druhů	druh	k1gInPc2	druh
dřevin	dřevina	k1gFnPc2	dřevina
a	a	k8xC	a
přibližně	přibližně	k6eAd1	přibližně
2	[number]	k4	2
000	[number]	k4	000
taxonů	taxon	k1gInPc2	taxon
dřevin	dřevina	k1gFnPc2	dřevina
<g/>
.	.	kIx.	.
</s>
<s>
Nejběžnějšími	běžný	k2eAgInPc7d3	nejběžnější
druhy	druh	k1gInPc7	druh
divoce	divoce	k6eAd1	divoce
žijících	žijící	k2eAgNnPc2d1	žijící
zvířat	zvíře	k1gNnPc2	zvíře
jsou	být	k5eAaImIp3nP	být
zajíci	zajíc	k1gMnPc1	zajíc
<g/>
,	,	kIx,	,
vydry	vydra	k1gFnPc1	vydra
a	a	k8xC	a
kuny	kuna	k1gFnPc1	kuna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lesích	les	k1gInPc6	les
a	a	k8xC	a
na	na	k7c6	na
polích	pole	k1gNnPc6	pole
převládají	převládat	k5eAaImIp3nP	převládat
bažanti	bažant	k1gMnPc1	bažant
<g/>
,	,	kIx,	,
koroptve	koroptev	k1gFnPc1	koroptev
<g/>
,	,	kIx,	,
divoká	divoký	k2eAgNnPc1d1	divoké
prasata	prase	k1gNnPc1	prase
<g/>
,	,	kIx,	,
vysoká	vysoký	k2eAgFnSc1d1	vysoká
zvěř	zvěř	k1gFnSc1	zvěř
<g/>
,	,	kIx,	,
kachny	kachna	k1gFnPc1	kachna
a	a	k8xC	a
husy	husa	k1gFnPc1	husa
<g/>
.	.	kIx.	.
</s>
<s>
Vzácnější	vzácný	k2eAgMnSc1d2	vzácnější
jsou	být	k5eAaImIp3nP	být
orli	orel	k1gMnPc1	orel
a	a	k8xC	a
volavky	volavka	k1gFnPc1	volavka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
Moravy	Morava	k1gFnSc2	Morava
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
i	i	k9	i
vlci	vlk	k1gMnPc1	vlk
a	a	k8xC	a
medvěd	medvěd	k1gMnSc1	medvěd
hnědý	hnědý	k2eAgMnSc1d1	hnědý
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
vzácně	vzácně	k6eAd1	vzácně
<g/>
.	.	kIx.	.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
počet	počet	k1gInSc1	počet
druhů	druh	k1gMnPc2	druh
živočichů	živočich	k1gMnPc2	živočich
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
je	být	k5eAaImIp3nS	být
odhadován	odhadovat	k5eAaImNgInS	odhadovat
na	na	k7c4	na
40	[number]	k4	40
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
nejméně	málo	k6eAd3	málo
28	[number]	k4	28
124	[number]	k4	124
druhů	druh	k1gInPc2	druh
je	být	k5eAaImIp3nS	být
bezobratlých	bezobratlý	k2eAgFnPc2d1	bezobratlý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ochrana	ochrana	k1gFnSc1	ochrana
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
===	===	k?	===
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
Environmental	Environmental	k1gMnSc1	Environmental
Performance	performance	k1gFnSc2	performance
Index	index	k1gInSc1	index
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
vypracovává	vypracovávat	k5eAaImIp3nS	vypracovávat
Yaleská	Yaleský	k2eAgFnSc1d1	Yaleská
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ČR	ČR	kA	ČR
27	[number]	k4	27
<g/>
.	.	kIx.	.
nejohleduplnější	ohleduplný	k2eAgFnSc7d3	nejohleduplnější
zemí	zem	k1gFnSc7	zem
k	k	k7c3	k
životnímu	životní	k2eAgNnSc3d1	životní
prostředí	prostředí	k1gNnSc3	prostředí
na	na	k7c6	na
světě	svět	k1gInSc6	svět
(	(	kIx(	(
<g/>
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zachovalá	zachovalý	k2eAgFnSc1d1	zachovalá
příroda	příroda	k1gFnSc1	příroda
je	být	k5eAaImIp3nS	být
chráněna	chránit	k5eAaImNgFnS	chránit
v	v	k7c6	v
chráněných	chráněný	k2eAgNnPc6d1	chráněné
územích	území	k1gNnPc6	území
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
orgánem	orgán	k1gInSc7	orgán
ochrany	ochrana	k1gFnSc2	ochrana
přírody	příroda	k1gFnSc2	příroda
a	a	k8xC	a
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
je	být	k5eAaImIp3nS	být
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
čtyři	čtyři	k4xCgFnPc4	čtyři
oblasti	oblast	k1gFnPc4	oblast
s	s	k7c7	s
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
mírou	míra	k1gFnSc7	míra
ochrany	ochrana	k1gFnSc2	ochrana
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
národní	národní	k2eAgInPc1d1	národní
parky	park	k1gInPc1	park
<g/>
:	:	kIx,	:
Krkonošský	krkonošský	k2eAgInSc1d1	krkonošský
národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
(	(	kIx(	(
<g/>
od	od	k7c2	od
17	[number]	k4	17
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
Šumava	Šumava	k1gFnSc1	Šumava
(	(	kIx(	(
<g/>
od	od	k7c2	od
20	[number]	k4	20
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
Podyjí	Podyjí	k1gNnSc2	Podyjí
(	(	kIx(	(
<g/>
od	od	k7c2	od
20	[number]	k4	20
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
a	a	k8xC	a
Národní	národní	k2eAgInSc4d1	národní
park	park	k1gInSc4	park
České	český	k2eAgNnSc1d1	české
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
(	(	kIx(	(
<g/>
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
oznámilo	oznámit	k5eAaPmAgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
připravuje	připravovat	k5eAaImIp3nS	připravovat
vyhlášení	vyhlášení	k1gNnSc4	vyhlášení
Národního	národní	k2eAgInSc2d1	národní
parku	park	k1gInSc2	park
Křivoklátsko	Křivoklátsko	k1gNnSc1	Křivoklátsko
<g/>
.	.	kIx.	.
<g/>
Mezi	mezi	k7c4	mezi
chráněná	chráněný	k2eAgNnPc4d1	chráněné
území	území	k1gNnPc4	území
patří	patřit	k5eAaImIp3nS	patřit
(	(	kIx(	(
<g/>
v	v	k7c6	v
závorce	závorka	k1gFnSc6	závorka
jejich	jejich	k3xOp3gInSc4	jejich
počet	počet	k1gInSc4	počet
k	k	k7c3	k
3	[number]	k4	3
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
národní	národní	k2eAgInPc1d1	národní
parky	park	k1gInPc1	park
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
chráněné	chráněný	k2eAgFnSc2d1	chráněná
krajinné	krajinný	k2eAgFnSc2d1	krajinná
oblasti	oblast	k1gFnSc2	oblast
(	(	kIx(	(
<g/>
26	[number]	k4	26
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
národní	národní	k2eAgFnPc1d1	národní
přírodní	přírodní	k2eAgFnPc1d1	přírodní
rezervace	rezervace	k1gFnPc1	rezervace
(	(	kIx(	(
<g/>
107	[number]	k4	107
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
přírodní	přírodní	k2eAgFnPc1d1	přírodní
rezervace	rezervace	k1gFnPc1	rezervace
(	(	kIx(	(
<g/>
815	[number]	k4	815
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
národní	národní	k2eAgFnPc1d1	národní
přírodní	přírodní	k2eAgFnPc1d1	přírodní
památky	památka	k1gFnPc1	památka
(	(	kIx(	(
<g/>
120	[number]	k4	120
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
přírodní	přírodní	k2eAgFnPc1d1	přírodní
památky	památka	k1gFnPc1	památka
(	(	kIx(	(
<g/>
1531	[number]	k4	1531
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Politický	politický	k2eAgInSc1d1	politický
systém	systém	k1gInSc1	systém
==	==	k?	==
</s>
</p>
<p>
<s>
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
je	být	k5eAaImIp3nS	být
zastupitelská	zastupitelský	k2eAgFnSc1d1	zastupitelská
demokracie	demokracie	k1gFnSc1	demokracie
<g/>
,	,	kIx,	,
parlamentní	parlamentní	k2eAgFnSc1d1	parlamentní
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Výkonnou	výkonný	k2eAgFnSc7d1	výkonná
mocí	moc	k1gFnSc7	moc
disponuje	disponovat	k5eAaBmIp3nS	disponovat
prezident	prezident	k1gMnSc1	prezident
a	a	k8xC	a
vláda	vláda	k1gFnSc1	vláda
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
vláda	vláda	k1gFnSc1	vláda
je	být	k5eAaImIp3nS	být
vrcholným	vrcholný	k2eAgInSc7d1	vrcholný
orgánem	orgán	k1gInSc7	orgán
výkonné	výkonný	k2eAgFnSc2d1	výkonná
moci	moc	k1gFnSc2	moc
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
je	být	k5eAaImIp3nS	být
odpovědná	odpovědný	k2eAgFnSc1d1	odpovědná
Poslanecké	poslanecký	k2eAgFnSc3d1	Poslanecká
sněmovně	sněmovna	k1gFnSc3	sněmovna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlavou	hlava	k1gFnSc7	hlava
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
prezident	prezident	k1gMnSc1	prezident
<g/>
,	,	kIx,	,
volený	volený	k2eAgInSc1d1	volený
přímou	přímý	k2eAgFnSc7d1	přímá
volbou	volba	k1gFnSc7	volba
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
se	s	k7c7	s
souhlasem	souhlas	k1gInSc7	souhlas
Senátu	senát	k1gInSc2	senát
soudce	soudce	k1gMnSc2	soudce
Ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
<g/>
,	,	kIx,	,
za	za	k7c2	za
určitých	určitý	k2eAgFnPc2d1	určitá
podmínek	podmínka	k1gFnPc2	podmínka
může	moct	k5eAaImIp3nS	moct
rozpustit	rozpustit	k5eAaPmF	rozpustit
Poslaneckou	poslanecký	k2eAgFnSc4d1	Poslanecká
sněmovnu	sněmovna	k1gFnSc4	sněmovna
a	a	k8xC	a
vetovat	vetovat	k5eAaBmF	vetovat
zákony	zákon	k1gInPc4	zákon
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
státního	státní	k2eAgInSc2d1	státní
rozpočtu	rozpočet	k1gInSc2	rozpočet
a	a	k8xC	a
ústavních	ústavní	k2eAgInPc2d1	ústavní
zákonů	zákon	k1gInPc2	zákon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
také	také	k9	také
předsedu	předseda	k1gMnSc4	předseda
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
její	její	k3xOp3gInPc4	její
členy	člen	k1gInPc4	člen
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
na	na	k7c4	na
jeho	jeho	k3xOp3gInSc4	jeho
návrh	návrh	k1gInSc4	návrh
<g/>
.	.	kIx.	.
</s>
<s>
Přijímá	přijímat	k5eAaImIp3nS	přijímat
demisi	demise	k1gFnSc4	demise
předsedy	předseda	k1gMnSc2	předseda
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc7	jeho
prostřednictvím	prostřednictví	k1gNnSc7	prostřednictví
i	i	k9	i
od	od	k7c2	od
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
členů	člen	k1gInPc2	člen
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
tradičně	tradičně	k6eAd1	tradičně
sídlí	sídlet	k5eAaImIp3nS	sídlet
na	na	k7c6	na
Pražském	pražský	k2eAgInSc6d1	pražský
hradě	hrad	k1gInSc6	hrad
<g/>
,	,	kIx,	,
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
má	mít	k5eAaImIp3nS	mít
též	též	k9	též
zámek	zámek	k1gInSc1	zámek
v	v	k7c6	v
Lánech	lán	k1gInPc6	lán
<g/>
.	.	kIx.	.
</s>
<s>
Uděluje	udělovat	k5eAaImIp3nS	udělovat
či	či	k8xC	či
propůjčuje	propůjčovat	k5eAaImIp3nS	propůjčovat
státní	státní	k2eAgNnSc1d1	státní
vyznamenání	vyznamenání	k1gNnSc1	vyznamenání
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyššími	vysoký	k2eAgMnPc7d3	nejvyšší
státními	státní	k2eAgMnPc7d1	státní
vyznamenáními	vyznamenání	k1gNnPc7	vyznamenání
jsou	být	k5eAaImIp3nP	být
Řád	řád	k1gInSc4	řád
Bílého	bílý	k2eAgInSc2d1	bílý
lva	lev	k1gInSc2	lev
<g/>
,	,	kIx,	,
Řád	řád	k1gInSc1	řád
Tomáše	Tomáš	k1gMnSc2	Tomáš
Garrigua	Garriguus	k1gMnSc2	Garriguus
Masaryka	Masaryk	k1gMnSc2	Masaryk
<g/>
,	,	kIx,	,
Medaile	medaile	k1gFnSc2	medaile
Za	za	k7c4	za
hrdinství	hrdinství	k1gNnSc4	hrdinství
a	a	k8xC	a
Medaile	medaile	k1gFnPc4	medaile
Za	za	k7c4	za
zásluhy	zásluha	k1gFnPc4	zásluha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Centrální	centrální	k2eAgFnSc7d1	centrální
bankou	banka	k1gFnSc7	banka
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
nezávislá	závislý	k2eNgFnSc1d1	nezávislá
Česká	český	k2eAgFnSc1d1	Česká
národní	národní	k2eAgFnSc1d1	národní
banka	banka	k1gFnSc1	banka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
mj.	mj.	kA	mj.
vydává	vydávat	k5eAaPmIp3nS	vydávat
českou	český	k2eAgFnSc4d1	Česká
měnu	měna	k1gFnSc4	měna
<g/>
,	,	kIx,	,
českou	český	k2eAgFnSc4d1	Česká
korunu	koruna	k1gFnSc4	koruna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Parlament	parlament	k1gInSc1	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
je	být	k5eAaImIp3nS	být
dvoukomorový	dvoukomorový	k2eAgMnSc1d1	dvoukomorový
<g/>
,	,	kIx,	,
s	s	k7c7	s
Poslaneckou	poslanecký	k2eAgFnSc7d1	Poslanecká
sněmovnou	sněmovna	k1gFnSc7	sněmovna
a	a	k8xC	a
Senátem	senát	k1gInSc7	senát
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
se	se	k3xPyFc4	se
volí	volit	k5eAaImIp3nS	volit
200	[number]	k4	200
poslanců	poslanec	k1gMnPc2	poslanec
každé	každý	k3xTgInPc4	každý
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
na	na	k7c6	na
základě	základ	k1gInSc6	základ
poměrného	poměrný	k2eAgNnSc2d1	poměrné
zastoupení	zastoupení	k1gNnSc2	zastoupení
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
se	se	k3xPyFc4	se
volbami	volba	k1gFnPc7	volba
obmění	obměnit	k5eAaPmIp3nS	obměnit
třetina	třetina	k1gFnSc1	třetina
Senátu	senát	k1gInSc2	senát
na	na	k7c6	na
základě	základ	k1gInSc6	základ
dvoukolových	dvoukolový	k2eAgFnPc2d1	dvoukolová
většinových	většinový	k2eAgFnPc2d1	většinová
voleb	volba	k1gFnPc2	volba
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
z	z	k7c2	z
81	[number]	k4	81
senátorů	senátor	k1gMnPc2	senátor
má	mít	k5eAaImIp3nS	mít
šestiletý	šestiletý	k2eAgInSc4d1	šestiletý
mandát	mandát	k1gInSc4	mandát
<g/>
.	.	kIx.	.
</s>
<s>
Sněmovna	sněmovna	k1gFnSc1	sněmovna
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
Thunovském	Thunovský	k2eAgInSc6d1	Thunovský
paláci	palác	k1gInSc6	palác
<g/>
,	,	kIx,	,
Senát	senát	k1gInSc1	senát
ve	v	k7c6	v
Valdštejnském	valdštejnský	k2eAgInSc6d1	valdštejnský
paláci	palác	k1gInSc6	palác
na	na	k7c6	na
Malé	Malé	k2eAgFnSc6d1	Malé
Straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ústavní	ústavní	k2eAgInSc1d1	ústavní
soud	soud	k1gInSc1	soud
o	o	k7c4	o
celkem	celkem	k6eAd1	celkem
15	[number]	k4	15
soudcích	soudce	k1gMnPc6	soudce
je	být	k5eAaImIp3nS	být
garantem	garant	k1gMnSc7	garant
ústavnosti	ústavnost	k1gFnSc2	ústavnost
<g/>
,	,	kIx,	,
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
ochranu	ochrana	k1gFnSc4	ochrana
základním	základní	k2eAgInSc7d1	základní
(	(	kIx(	(
<g/>
ústavním	ústavní	k2eAgInSc7d1	ústavní
<g/>
)	)	kIx)	)
právům	právo	k1gNnPc3	právo
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
i	i	k9	i
rušit	rušit	k5eAaImF	rušit
zákony	zákon	k1gInPc4	zákon
či	či	k8xC	či
jejich	jejich	k3xOp3gNnPc4	jejich
ustanovení	ustanovení	k1gNnPc4	ustanovení
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
ale	ale	k9	ale
součástí	součást	k1gFnSc7	součást
systému	systém	k1gInSc2	systém
obecných	obecný	k2eAgInPc2d1	obecný
soudů	soud	k1gInPc2	soud
<g/>
,	,	kIx,	,
vrcholnými	vrcholný	k2eAgInPc7d1	vrcholný
orgány	orgán	k1gInPc7	orgán
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
<g/>
,	,	kIx,	,
působící	působící	k2eAgInPc1d1	působící
v	v	k7c6	v
civilní	civilní	k2eAgFnSc6d1	civilní
i	i	k8xC	i
trestní	trestní	k2eAgFnSc3d1	trestní
justici	justice	k1gFnSc3	justice
<g/>
,	,	kIx,	,
a	a	k8xC	a
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
správní	správní	k2eAgInSc1d1	správní
soud	soud	k1gInSc1	soud
s	s	k7c7	s
agendou	agenda	k1gFnSc7	agenda
správního	správní	k2eAgNnSc2d1	správní
soudnictví	soudnictví	k1gNnSc2	soudnictví
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vláda	vláda	k1gFnSc1	vláda
a	a	k8xC	a
státní	státní	k2eAgFnSc1d1	státní
správa	správa	k1gFnSc1	správa
===	===	k?	===
</s>
</p>
<p>
<s>
Vláda	vláda	k1gFnSc1	vláda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
je	být	k5eAaImIp3nS	být
vrcholný	vrcholný	k2eAgInSc1d1	vrcholný
orgán	orgán	k1gInSc1	orgán
výkonné	výkonný	k2eAgFnSc2d1	výkonná
moci	moc	k1gFnSc2	moc
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
postavení	postavení	k1gNnSc1	postavení
vymezuje	vymezovat	k5eAaImIp3nS	vymezovat
Ústava	ústava	k1gFnSc1	ústava
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
předsedy	předseda	k1gMnSc2	předseda
vlády	vláda	k1gFnSc2	vláda
(	(	kIx(	(
<g/>
premiéra	premiér	k1gMnSc2	premiér
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
místopředsedů	místopředseda	k1gMnPc2	místopředseda
vlády	vláda	k1gFnSc2	vláda
(	(	kIx(	(
<g/>
vicepremiérů	vicepremiér	k1gMnPc2	vicepremiér
<g/>
)	)	kIx)	)
a	a	k8xC	a
ministrů	ministr	k1gMnPc2	ministr
<g/>
.	.	kIx.	.
</s>
<s>
Úřad	úřad	k1gInSc1	úřad
vlády	vláda	k1gFnSc2	vláda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
Strakovy	Strakův	k2eAgFnSc2d1	Strakova
akademie	akademie	k1gFnSc2	akademie
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
Malé	Malé	k2eAgFnSc6d1	Malé
Straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
Tradičním	tradiční	k2eAgNnSc7d1	tradiční
sídlem	sídlo	k1gNnSc7	sídlo
premiéra	premiéra	k1gFnSc1	premiéra
je	být	k5eAaImIp3nS	být
Kramářova	kramářův	k2eAgFnSc1d1	Kramářova
vila	vila	k1gFnSc1	vila
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Premiéry	premiéra	k1gFnPc1	premiéra
samostatné	samostatný	k2eAgFnSc2d1	samostatná
ČR	ČR	kA	ČR
byli	být	k5eAaImAgMnP	být
<g/>
:	:	kIx,	:
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
–	–	k?	–
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Tošovský	Tošovský	k1gMnSc1	Tošovský
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
–	–	k?	–
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
–	–	k?	–
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Špidla	Špidla	k1gMnSc1	Špidla
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
Gross	Gross	k1gMnSc1	Gross
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
–	–	k?	–
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Paroubek	Paroubek	k1gInSc1	Paroubek
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
–	–	k?	–
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mirek	Mirek	k1gMnSc1	Mirek
Topolánek	Topolánek	k1gMnSc1	Topolánek
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
–	–	k?	–
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Fischer	Fischer	k1gMnSc1	Fischer
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
2009	[number]	k4	2009
<g/>
–	–	k?	–
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
Nečas	Nečas	k1gMnSc1	Nečas
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
–	–	k?	–
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Rusnok	Rusnok	k1gInSc1	Rusnok
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
–	–	k?	–
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Sobotka	Sobotka	k1gMnSc1	Sobotka
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
–	–	k?	–
<g/>
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
a	a	k8xC	a
Andrej	Andrej	k1gMnSc1	Andrej
Babiš	Babiš	k1gMnSc1	Babiš
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
od	od	k7c2	od
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Součástí	součást	k1gFnSc7	součást
všech	všecek	k3xTgFnPc2	všecek
vlád	vláda	k1gFnPc2	vláda
ČR	ČR	kA	ČR
bylo	být	k5eAaImAgNnS	být
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
financí	finance	k1gFnPc2	finance
<g/>
,	,	kIx,	,
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
<g/>
,	,	kIx,	,
vnitra	vnitro	k1gNnSc2	vnitro
<g/>
,	,	kIx,	,
obrany	obrana	k1gFnSc2	obrana
<g/>
,	,	kIx,	,
práce	práce	k1gFnSc2	práce
a	a	k8xC	a
sociálních	sociální	k2eAgFnPc2d1	sociální
věcí	věc	k1gFnPc2	věc
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
místní	místní	k2eAgInSc4d1	místní
rozvoj	rozvoj	k1gInSc4	rozvoj
<g/>
,	,	kIx,	,
dopravy	doprava	k1gFnSc2	doprava
<g/>
,	,	kIx,	,
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
,	,	kIx,	,
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
<g/>
,	,	kIx,	,
školství	školství	k1gNnSc2	školství
<g/>
,	,	kIx,	,
zdravotnictví	zdravotnictví	k1gNnSc2	zdravotnictví
<g/>
,	,	kIx,	,
zemědělství	zemědělství	k1gNnSc2	zemědělství
a	a	k8xC	a
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1993	[number]	k4	1993
<g/>
–	–	k?	–
<g/>
1996	[number]	k4	1996
existovalo	existovat	k5eAaImAgNnS	existovat
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
národního	národní	k2eAgInSc2d1	národní
majetku	majetek	k1gInSc2	majetek
a	a	k8xC	a
privatizace	privatizace	k1gFnSc2	privatizace
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	let	k1gInPc6	let
2003	[number]	k4	2003
<g/>
–	–	k?	–
<g/>
2007	[number]	k4	2007
ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
informatiky	informatika	k1gFnSc2	informatika
<g/>
.	.	kIx.	.
</s>
<s>
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
řídí	řídit	k5eAaImIp3nS	řídit
mj.	mj.	kA	mj.
Policii	policie	k1gFnSc4	policie
ČR	ČR	kA	ČR
a	a	k8xC	a
Hasičský	hasičský	k2eAgInSc1d1	hasičský
záchranný	záchranný	k2eAgInSc1d1	záchranný
sbor	sbor	k1gInSc1	sbor
<g/>
,	,	kIx,	,
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
dopravy	doprava	k1gFnSc2	doprava
Ředitelství	ředitelství	k1gNnSc2	ředitelství
silnic	silnice	k1gFnPc2	silnice
a	a	k8xC	a
dálnic	dálnice	k1gFnPc2	dálnice
a	a	k8xC	a
Státní	státní	k2eAgInSc1d1	státní
fond	fond	k1gInSc1	fond
dopravní	dopravní	k2eAgFnSc2d1	dopravní
infrastruktury	infrastruktura	k1gFnSc2	infrastruktura
<g/>
,	,	kIx,	,
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
kultury	kultura	k1gFnSc2	kultura
Národní	národní	k2eAgFnSc2d1	národní
památkový	památkový	k2eAgInSc4d1	památkový
ústav	ústav	k1gInSc4	ústav
<g/>
,	,	kIx,	,
ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
financí	finance	k1gFnPc2	finance
Celní	celní	k2eAgFnSc4d1	celní
správu	správa	k1gFnSc4	správa
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
obrany	obrana	k1gFnSc2	obrana
Armádu	armáda	k1gFnSc4	armáda
ČR	ČR	kA	ČR
a	a	k8xC	a
Vojenské	vojenský	k2eAgNnSc1d1	vojenské
zpravodajství	zpravodajství	k1gNnSc1	zpravodajství
<g/>
,	,	kIx,	,
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
<g />
.	.	kIx.	.
</s>
<s>
práce	práce	k1gFnPc1	práce
Českou	český	k2eAgFnSc4d1	Česká
správu	správa	k1gFnSc4	správa
sociálního	sociální	k2eAgNnSc2d1	sociální
zabezpečení	zabezpečení	k1gNnSc2	zabezpečení
a	a	k8xC	a
Úřad	úřad	k1gInSc1	úřad
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
průmyslu	průmysl	k1gInSc2	průmysl
Českou	český	k2eAgFnSc4d1	Česká
obchodní	obchodní	k2eAgFnSc4d1	obchodní
inspekci	inspekce	k1gFnSc4	inspekce
<g/>
,	,	kIx,	,
ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
Vězeňskou	vězeňský	k2eAgFnSc4d1	vězeňská
službu	služba	k1gFnSc4	služba
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
soudní	soudní	k2eAgFnSc1d1	soudní
moc	moc	k1gFnSc1	moc
je	být	k5eAaImIp3nS	být
proti	proti	k7c3	proti
tomu	ten	k3xDgNnSc3	ten
nezávislá	závislý	k2eNgFnSc1d1	nezávislá
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
školství	školství	k1gNnSc2	školství
Českou	český	k2eAgFnSc4d1	Česká
školní	školní	k2eAgFnSc4d1	školní
inspekci	inspekce	k1gFnSc4	inspekce
<g/>
,	,	kIx,	,
ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
zahraničí	zahraničí	k1gNnSc2	zahraničí
zastupitelské	zastupitelský	k2eAgInPc1d1	zastupitelský
úřady	úřad	k1gInPc1	úřad
ČR	ČR	kA	ČR
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgNnPc1d1	české
centra	centrum	k1gNnPc1	centrum
<g/>
,	,	kIx,	,
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
zemědělství	zemědělství	k1gNnSc2	zemědělství
Státní	státní	k2eAgFnSc4d1	státní
veterinární	veterinární	k2eAgFnSc4d1	veterinární
správu	správa	k1gFnSc4	správa
<g/>
,	,	kIx,	,
Státní	státní	k2eAgFnSc4d1	státní
zemědělskou	zemědělský	k2eAgFnSc4d1	zemědělská
a	a	k8xC	a
potravinářskou	potravinářský	k2eAgFnSc4d1	potravinářská
inspekci	inspekce	k1gFnSc4	inspekce
<g/>
,	,	kIx,	,
Státní	státní	k2eAgInSc1d1	státní
pozemkový	pozemkový	k2eAgInSc1d1	pozemkový
úřad	úřad	k1gInSc1	úřad
<g/>
,	,	kIx,	,
Český	český	k2eAgInSc1d1	český
úřad	úřad	k1gInSc1	úřad
zeměměřický	zeměměřický	k2eAgInSc1d1	zeměměřický
a	a	k8xC	a
katastrální	katastrální	k2eAgInSc1d1	katastrální
a	a	k8xC	a
Státní	státní	k2eAgInSc1d1	státní
zemědělský	zemědělský	k2eAgInSc1d1	zemědělský
intervenční	intervenční	k2eAgInSc1d1	intervenční
fond	fond	k1gInSc1	fond
<g/>
,	,	kIx,	,
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
Agenturu	agentura	k1gFnSc4	agentura
ochrany	ochrana	k1gFnSc2	ochrana
přírody	příroda	k1gFnSc2	příroda
a	a	k8xC	a
krajiny	krajina	k1gFnSc2	krajina
<g/>
,	,	kIx,	,
Českou	český	k2eAgFnSc4d1	Česká
inspekci	inspekce	k1gFnSc4	inspekce
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
a	a	k8xC	a
Český	český	k2eAgInSc1d1	český
hydrometeorologický	hydrometeorologický	k2eAgInSc1d1	hydrometeorologický
ústav	ústav	k1gInSc1	ústav
<g/>
,	,	kIx,	,
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
pro	pro	k7c4	pro
místní	místní	k2eAgInSc4d1	místní
rozvoj	rozvoj	k1gInSc4	rozvoj
Státní	státní	k2eAgInSc1d1	státní
fond	fond	k1gInSc1	fond
rozvoje	rozvoj	k1gInSc2	rozvoj
bydlení	bydlení	k1gNnSc2	bydlení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Víceméně	víceméně	k9	víceméně
samostatnými	samostatný	k2eAgInPc7d1	samostatný
ústředními	ústřední	k2eAgInPc7d1	ústřední
orgány	orgán	k1gInPc7	orgán
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
jsou	být	k5eAaImIp3nP	být
Národní	národní	k2eAgInSc1d1	národní
bezpečnostní	bezpečnostní	k2eAgInSc1d1	bezpečnostní
úřad	úřad	k1gInSc1	úřad
<g/>
,	,	kIx,	,
Český	český	k2eAgInSc1d1	český
telekomunikační	telekomunikační	k2eAgInSc1d1	telekomunikační
úřad	úřad	k1gInSc1	úřad
<g/>
,	,	kIx,	,
Úřad	úřad	k1gInSc1	úřad
průmyslového	průmyslový	k2eAgNnSc2d1	průmyslové
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
<g/>
,	,	kIx,	,
Český	český	k2eAgInSc1d1	český
statistický	statistický	k2eAgInSc1d1	statistický
úřad	úřad	k1gInSc1	úřad
<g/>
,	,	kIx,	,
Český	český	k2eAgInSc1d1	český
báňský	báňský	k2eAgInSc1d1	báňský
úřad	úřad	k1gInSc1	úřad
<g/>
,	,	kIx,	,
Energetický	energetický	k2eAgInSc1d1	energetický
regulační	regulační	k2eAgInSc1d1	regulační
úřad	úřad	k1gInSc1	úřad
<g/>
,	,	kIx,	,
Úřad	úřad	k1gInSc1	úřad
pro	pro	k7c4	pro
ochranu	ochrana	k1gFnSc4	ochrana
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
soutěže	soutěž	k1gFnSc2	soutěž
(	(	kIx(	(
<g/>
antimonopolní	antimonopolní	k2eAgInSc1d1	antimonopolní
úřad	úřad	k1gInSc1	úřad
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Správa	správa	k1gFnSc1	správa
státních	státní	k2eAgFnPc2d1	státní
hmotných	hmotný	k2eAgFnPc2d1	hmotná
rezerv	rezerva	k1gFnPc2	rezerva
<g/>
,	,	kIx,	,
Státní	státní	k2eAgInSc1d1	státní
úřad	úřad	k1gInSc1	úřad
pro	pro	k7c4	pro
jadernou	jaderný	k2eAgFnSc4d1	jaderná
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
<g/>
,	,	kIx,	,
Úřad	úřad	k1gInSc1	úřad
pro	pro	k7c4	pro
mezinárodněprávní	mezinárodněprávní	k2eAgFnSc4d1	mezinárodněprávní
ochranu	ochrana	k1gFnSc4	ochrana
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
Úřad	úřad	k1gInSc1	úřad
pro	pro	k7c4	pro
zahraniční	zahraniční	k2eAgInPc4d1	zahraniční
styky	styk	k1gInPc4	styk
a	a	k8xC	a
informace	informace	k1gFnPc4	informace
<g/>
,	,	kIx,	,
Rada	rada	k1gFnSc1	rada
pro	pro	k7c4	pro
rozhlasové	rozhlasový	k2eAgNnSc4d1	rozhlasové
a	a	k8xC	a
televizní	televizní	k2eAgNnSc4d1	televizní
vysílání	vysílání	k1gNnSc4	vysílání
<g/>
,	,	kIx,	,
Úřad	úřad	k1gInSc1	úřad
pro	pro	k7c4	pro
ochranu	ochrana	k1gFnSc4	ochrana
osobních	osobní	k2eAgInPc2d1	osobní
údajů	údaj	k1gInPc2	údaj
a	a	k8xC	a
Bezpečnostní	bezpečnostní	k2eAgFnSc1d1	bezpečnostní
informační	informační	k2eAgFnSc1d1	informační
služba	služba	k1gFnSc1	služba
<g/>
.	.	kIx.	.
</s>
<s>
Zcela	zcela	k6eAd1	zcela
mimo	mimo	k7c4	mimo
moc	moc	k1gFnSc4	moc
výkonnou	výkonný	k2eAgFnSc7d1	výkonná
<g/>
,	,	kIx,	,
soudní	soudní	k2eAgFnSc7d1	soudní
i	i	k8xC	i
zákonodárnou	zákonodárný	k2eAgFnSc7d1	zákonodárná
je	být	k5eAaImIp3nS	být
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
kontrolní	kontrolní	k2eAgInSc1d1	kontrolní
úřad	úřad	k1gInSc1	úřad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sociální	sociální	k2eAgInSc4d1	sociální
smír	smír	k1gInSc4	smír
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
tzv.	tzv.	kA	tzv.
tripartita	tripartita	k1gFnSc1	tripartita
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
pravidelná	pravidelný	k2eAgNnPc1d1	pravidelné
trojstranná	trojstranný	k2eAgNnPc1d1	trojstranné
jednání	jednání	k1gNnPc1	jednání
mezi	mezi	k7c7	mezi
vládou	vláda	k1gFnSc7	vláda
<g/>
,	,	kIx,	,
největší	veliký	k2eAgFnSc7d3	veliký
odborovou	odborový	k2eAgFnSc7d1	odborová
centrálou	centrála	k1gFnSc7	centrála
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
(	(	kIx(	(
<g/>
Českomoravskou	českomoravský	k2eAgFnSc7d1	Českomoravská
konfederací	konfederace	k1gFnSc7	konfederace
odborových	odborový	k2eAgInPc2d1	odborový
svazů	svaz	k1gInPc2	svaz
<g/>
)	)	kIx)	)
a	a	k8xC	a
zaměstnavateli	zaměstnavatel	k1gMnSc3	zaměstnavatel
reprezentovanými	reprezentovaný	k2eAgInPc7d1	reprezentovaný
Hospodářskou	hospodářský	k2eAgFnSc7d1	hospodářská
komorou	komora	k1gFnSc7	komora
ČR	ČR	kA	ČR
a	a	k8xC	a
Svazem	svaz	k1gInSc7	svaz
průmyslu	průmysl	k1gInSc2	průmysl
a	a	k8xC	a
dopravy	doprava	k1gFnSc2	doprava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
éře	éra	k1gFnSc6	éra
samostatné	samostatný	k2eAgFnSc6d1	samostatná
ČR	ČR	kA	ČR
se	se	k3xPyFc4	se
na	na	k7c6	na
vládě	vláda	k1gFnSc6	vláda
podílely	podílet	k5eAaImAgFnP	podílet
politické	politický	k2eAgFnPc1d1	politická
strany	strana	k1gFnPc1	strana
ODS	ODS	kA	ODS
<g/>
,	,	kIx,	,
KDU-ČSL	KDU-ČSL	k1gFnSc1	KDU-ČSL
<g/>
,	,	kIx,	,
KDS	KDS	kA	KDS
<g/>
,	,	kIx,	,
ODA	ODA	kA	ODA
<g/>
,	,	kIx,	,
ČSSD	ČSSD	kA	ČSSD
<g/>
,	,	kIx,	,
US-DEU	US-DEU	k1gMnPc1	US-DEU
<g/>
,	,	kIx,	,
Zelení	Zelený	k1gMnPc1	Zelený
<g/>
,	,	kIx,	,
TOP	topit	k5eAaImRp2nS	topit
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
,	,	kIx,	,
VV	VV	kA	VV
<g/>
,	,	kIx,	,
LIDEM	Lido	k1gNnSc7	Lido
a	a	k8xC	a
ANO	ano	k9	ano
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
československé	československý	k2eAgFnSc6d1	Československá
historii	historie	k1gFnSc6	historie
to	ten	k3xDgNnSc1	ten
byly	být	k5eAaImAgFnP	být
navíc	navíc	k6eAd1	navíc
národní	národní	k2eAgMnPc1d1	národní
demokraté	demokrat	k1gMnPc1	demokrat
<g/>
,	,	kIx,	,
agrárníci	agrárník	k1gMnPc1	agrárník
<g/>
,	,	kIx,	,
národní	národní	k2eAgMnPc1d1	národní
socialisté	socialist	k1gMnPc1	socialist
<g/>
,	,	kIx,	,
živnostníci	živnostník	k1gMnPc1	živnostník
<g/>
,	,	kIx,	,
ľuďáci	ľuďák	k1gMnPc1	ľuďák
<g/>
,	,	kIx,	,
němečtí	německý	k2eAgMnPc1d1	německý
agrárníci	agrárník	k1gMnPc1	agrárník
<g/>
,	,	kIx,	,
němečtí	německý	k2eAgMnPc1d1	německý
křesťanští	křesťanský	k2eAgMnPc1d1	křesťanský
sociálové	sociál	k1gMnPc1	sociál
<g/>
,	,	kIx,	,
němečtí	německý	k2eAgMnPc1d1	německý
sociální	sociální	k2eAgMnPc1d1	sociální
demokraté	demokrat	k1gMnPc1	demokrat
<g/>
,	,	kIx,	,
Národní	národní	k2eAgNnSc1d1	národní
sjednocení	sjednocení	k1gNnSc1	sjednocení
<g/>
,	,	kIx,	,
Strana	strana	k1gFnSc1	strana
národní	národní	k2eAgFnSc2d1	národní
jednoty	jednota	k1gFnSc2	jednota
<g/>
,	,	kIx,	,
Demokratická	demokratický	k2eAgFnSc1d1	demokratická
strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
Strana	strana	k1gFnSc1	strana
slobody	sloboda	k1gFnSc2	sloboda
<g/>
,	,	kIx,	,
KSČ	KSČ	kA	KSČ
<g/>
,	,	kIx,	,
KSS	KSS	kA	KSS
<g/>
,	,	kIx,	,
Strana	strana	k1gFnSc1	strana
slovenskej	slovenskej	k?	slovenskej
obrody	obroda	k1gFnSc2	obroda
<g/>
,	,	kIx,	,
Občanské	občanský	k2eAgNnSc1d1	občanské
fórum	fórum	k1gNnSc1	fórum
<g/>
,	,	kIx,	,
Verejnosť	Verejnosť	k1gFnSc1	Verejnosť
proti	proti	k7c3	proti
násiliu	násilium	k1gNnSc3	násilium
<g/>
,	,	kIx,	,
KDH	KDH	kA	KDH
<g/>
,	,	kIx,	,
ODÚ	ODÚ	kA	ODÚ
<g/>
,	,	kIx,	,
HZDS	HZDS	kA	HZDS
<g/>
,	,	kIx,	,
OH	OH	kA	OH
a	a	k8xC	a
HSD-SMS	HSD-SMS	k1gFnSc2	HSD-SMS
<g/>
.	.	kIx.	.
</s>
<s>
Historicky	historicky	k6eAd1	historicky
prvními	první	k4xOgFnPc7	první
českými	český	k2eAgFnPc7d1	Česká
politickými	politický	k2eAgFnPc7d1	politická
stranami	strana	k1gFnPc7	strana
byli	být	k5eAaImAgMnP	být
staročeši	staročech	k1gMnPc1	staročech
(	(	kIx(	(
<g/>
1848	[number]	k4	1848
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mladočeši	mladočech	k1gMnPc1	mladočech
(	(	kIx(	(
<g/>
1874	[number]	k4	1874
<g/>
)	)	kIx)	)
a	a	k8xC	a
sociální	sociální	k2eAgMnPc1d1	sociální
demokraté	demokrat	k1gMnPc1	demokrat
(	(	kIx(	(
<g/>
1878	[number]	k4	1878
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Sociálně	sociálně	k6eAd1	sociálně
demokratická	demokratický	k2eAgFnSc1d1	demokratická
strana	strana	k1gFnSc1	strana
českoslovanská	českoslovanský	k2eAgFnSc1d1	českoslovanská
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Právní	právní	k2eAgMnSc1d1	právní
a	a	k8xC	a
soudní	soudní	k2eAgInSc1d1	soudní
systém	systém	k1gInSc1	systém
===	===	k?	===
</s>
</p>
<p>
<s>
Český	český	k2eAgInSc1d1	český
právní	právní	k2eAgInSc1d1	právní
řád	řád	k1gInSc1	řád
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
germánské	germánský	k2eAgFnSc2d1	germánská
větve	větev	k1gFnSc2	větev
kontinentálního	kontinentální	k2eAgInSc2d1	kontinentální
typu	typ	k1gInSc2	typ
právní	právní	k2eAgFnSc2d1	právní
kultury	kultura	k1gFnSc2	kultura
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
zvané	zvaný	k2eAgNnSc1d1	zvané
též	též	k9	též
římské	římský	k2eAgNnSc1d1	římské
právo	právo	k1gNnSc1	právo
<g/>
,	,	kIx,	,
v	v	k7c6	v
anglických	anglický	k2eAgFnPc6d1	anglická
zemích	zem	k1gFnPc6	zem
civil	civil	k1gMnSc1	civil
law	law	k?	law
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
předpisy	předpis	k1gInPc7	předpis
přijímanými	přijímaný	k2eAgMnPc7d1	přijímaný
českými	český	k2eAgMnPc7d1	český
zákonodárci	zákonodárce	k1gMnPc7	zákonodárce
<g/>
,	,	kIx,	,
právem	právem	k6eAd1	právem
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
mezinárodními	mezinárodní	k2eAgFnPc7d1	mezinárodní
dohodami	dohoda	k1gFnPc7	dohoda
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
ratifikovány	ratifikován	k2eAgMnPc4d1	ratifikován
českým	český	k2eAgInSc7d1	český
parlamentem	parlament	k1gInSc7	parlament
a	a	k8xC	a
některými	některý	k3yIgInPc7	některý
nálezy	nález	k1gInPc7	nález
Ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
(	(	kIx(	(
<g/>
těmi	ten	k3xDgMnPc7	ten
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
nějakou	nějaký	k3yIgFnSc4	nějaký
část	část	k1gFnSc4	část
zákonů	zákon	k1gInPc2	zákon
označují	označovat	k5eAaImIp3nP	označovat
za	za	k7c4	za
protiústavní	protiústavní	k2eAgFnPc4d1	protiústavní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
předpisy	předpis	k1gInPc1	předpis
jsou	být	k5eAaImIp3nP	být
pravidelně	pravidelně	k6eAd1	pravidelně
vydávány	vydávat	k5eAaPmNgInP	vydávat
ve	v	k7c6	v
Sbírce	sbírka	k1gFnSc6	sbírka
zákonů	zákon	k1gInPc2	zákon
a	a	k8xC	a
Sbírce	sbírka	k1gFnSc3	sbírka
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
smluv	smlouva	k1gFnPc2	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
je	být	k5eAaImIp3nS	být
unitární	unitární	k2eAgInSc4d1	unitární
stát	stát	k1gInSc4	stát
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
jeho	jeho	k3xOp3gFnPc1	jeho
části	část	k1gFnPc1	část
nemohou	moct	k5eNaImIp3nP	moct
mít	mít	k5eAaImF	mít
vlastní	vlastní	k2eAgFnSc4d1	vlastní
legislativu	legislativa	k1gFnSc4	legislativa
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
například	například	k6eAd1	například
ve	v	k7c6	v
federacích	federace	k1gFnPc6	federace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Základem	základ	k1gInSc7	základ
právního	právní	k2eAgInSc2d1	právní
systému	systém	k1gInSc2	systém
je	být	k5eAaImIp3nS	být
Ústava	ústava	k1gFnSc1	ústava
ČR	ČR	kA	ČR
přijatá	přijatý	k2eAgFnSc1d1	přijatá
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
trestní	trestní	k2eAgInSc1d1	trestní
zákoník	zákoník	k1gInSc1	zákoník
je	být	k5eAaImIp3nS	být
účinný	účinný	k2eAgInSc1d1	účinný
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
občanský	občanský	k2eAgInSc1d1	občanský
zákoník	zákoník	k1gInSc1	zákoník
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Tradici	tradice	k1gFnSc4	tradice
soukromého	soukromý	k2eAgNnSc2d1	soukromé
práva	právo	k1gNnSc2	právo
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
silně	silně	k6eAd1	silně
formoval	formovat	k5eAaImAgInS	formovat
rakouský	rakouský	k2eAgInSc1d1	rakouský
všeobecný	všeobecný	k2eAgInSc1d1	všeobecný
občanský	občanský	k2eAgInSc1d1	občanský
zákoník	zákoník	k1gInSc1	zákoník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Soudní	soudní	k2eAgFnSc1d1	soudní
moc	moc	k1gFnSc1	moc
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
Ústavním	ústavní	k2eAgInSc7d1	ústavní
soudem	soud	k1gInSc7	soud
a	a	k8xC	a
soustavou	soustava	k1gFnSc7	soustava
obecných	obecný	k2eAgInPc2d1	obecný
soudů	soud	k1gInPc2	soud
<g/>
,	,	kIx,	,
Ústavní	ústavní	k2eAgInSc1d1	ústavní
soud	soud	k1gInSc1	soud
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
ústavním	ústavní	k2eAgInSc7d1	ústavní
orgánem	orgán	k1gInSc7	orgán
soudního	soudní	k2eAgInSc2d1	soudní
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
úkolem	úkol	k1gInSc7	úkol
je	být	k5eAaImIp3nS	být
zejména	zejména	k9	zejména
provádět	provádět	k5eAaImF	provádět
kontrolu	kontrola	k1gFnSc4	kontrola
ústavnosti	ústavnost	k1gFnSc2	ústavnost
a	a	k8xC	a
plnit	plnit	k5eAaImF	plnit
některé	některý	k3yIgInPc4	některý
úkoly	úkol	k1gInPc4	úkol
volebního	volební	k2eAgNnSc2d1	volební
a	a	k8xC	a
politického	politický	k2eAgNnSc2d1	politické
soudnictví	soudnictví	k1gNnSc2	soudnictví
<g/>
.	.	kIx.	.
</s>
<s>
Ústavní	ústavní	k2eAgInSc1d1	ústavní
soud	soud	k1gInSc1	soud
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
bývalé	bývalý	k2eAgFnSc2d1	bývalá
moravské	moravský	k2eAgFnSc2d1	Moravská
Zemské	zemský	k2eAgFnSc2d1	zemská
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
.	.	kIx.	.
</s>
<s>
Předsedy	předseda	k1gMnPc4	předseda
Ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
samostatného	samostatný	k2eAgNnSc2d1	samostatné
Česka	Česko	k1gNnSc2	Česko
byli	být	k5eAaImAgMnP	být
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Kessler	Kessler	k1gMnSc1	Kessler
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
–	–	k?	–
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Miloš	Miloš	k1gMnSc1	Miloš
Holeček	Holeček	k1gMnSc1	Holeček
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
a	a	k8xC	a
Pavel	Pavel	k1gMnSc1	Pavel
Rychetský	Rychetský	k1gMnSc1	Rychetský
(	(	kIx(	(
<g/>
od	od	k7c2	od
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Soustava	soustava	k1gFnSc1	soustava
obecných	obecný	k2eAgInPc2d1	obecný
soudů	soud	k1gInPc2	soud
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
soudů	soud	k1gInPc2	soud
okresních	okresní	k2eAgInPc2d1	okresní
<g/>
,	,	kIx,	,
krajských	krajský	k2eAgInPc2d1	krajský
a	a	k8xC	a
vrchních	vrchní	k2eAgInPc2d1	vrchní
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
vrcholu	vrchol	k1gInSc6	vrchol
stojí	stát	k5eAaImIp3nS	stát
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
a	a	k8xC	a
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
správní	správní	k2eAgInSc1d1	správní
soud	soud	k1gInSc1	soud
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
instituce	instituce	k1gFnPc1	instituce
sídlí	sídlet	k5eAaImIp3nP	sídlet
také	také	k9	také
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
Nejvyšší	vysoký	k2eAgNnSc1d3	nejvyšší
státní	státní	k2eAgNnSc1d1	státní
zastupitelství	zastupitelství	k1gNnSc1	zastupitelství
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
tradici	tradice	k1gFnSc6	tradice
germánského	germánský	k2eAgNnSc2d1	germánské
římského	římský	k2eAgNnSc2d1	římské
práva	právo	k1gNnSc2	právo
obvyklé	obvyklý	k2eAgNnSc1d1	obvyklé
<g/>
,	,	kIx,	,
soustava	soustava	k1gFnSc1	soustava
obecných	obecný	k2eAgInPc2d1	obecný
soudů	soud	k1gInPc2	soud
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
tři	tři	k4xCgFnPc4	tři
větve	větev	k1gFnPc4	větev
<g/>
:	:	kIx,	:
soudnictví	soudnictví	k1gNnSc4	soudnictví
civilní	civilní	k2eAgNnSc4d1	civilní
<g/>
,	,	kIx,	,
trestní	trestní	k2eAgNnSc4d1	trestní
a	a	k8xC	a
správní	správní	k2eAgNnSc4d1	správní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zahraniční	zahraniční	k2eAgInPc1d1	zahraniční
vztahy	vztah	k1gInPc1	vztah
===	===	k?	===
</s>
</p>
<p>
<s>
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
má	mít	k5eAaImIp3nS	mít
zavedenou	zavedený	k2eAgFnSc4d1	zavedená
strukturu	struktura	k1gFnSc4	struktura
zahraničních	zahraniční	k2eAgInPc2d1	zahraniční
vztahů	vztah	k1gInPc2	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
OSN	OSN	kA	OSN
<g/>
,	,	kIx,	,
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
(	(	kIx(	(
<g/>
EU	EU	kA	EU
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
NATO	NATO	kA	NATO
<g/>
,	,	kIx,	,
Organizace	organizace	k1gFnSc1	organizace
pro	pro	k7c4	pro
hospodářskou	hospodářský	k2eAgFnSc4d1	hospodářská
spolupráci	spolupráce	k1gFnSc4	spolupráce
a	a	k8xC	a
rozvoj	rozvoj	k1gInSc4	rozvoj
(	(	kIx(	(
<g/>
OECD	OECD	kA	OECD
<g/>
)	)	kIx)	)
a	a	k8xC	a
Rady	rada	k1gFnSc2	rada
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pozorovatelem	pozorovatel	k1gMnSc7	pozorovatel
Organizace	organizace	k1gFnSc2	organizace
amerických	americký	k2eAgInPc2d1	americký
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
státy	stát	k1gInPc1	stát
(	(	kIx(	(
<g/>
138	[number]	k4	138
států	stát	k1gInPc2	stát
ke	k	k7c3	k
dni	den	k1gInSc3	den
3	[number]	k4	3
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
je	být	k5eAaImIp3nS	být
devět	devět	k4xCc4	devět
států	stát	k1gInPc2	stát
zastoupeno	zastoupit	k5eAaPmNgNnS	zastoupit
diplomaty	diplomat	k1gMnPc4	diplomat
v	v	k7c6	v
hodnosti	hodnost	k1gFnSc6	hodnost
chargé	chargá	k1gFnSc2	chargá
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
affaires	affaires	k1gMnSc1	affaires
<g/>
)	)	kIx)	)
a	a	k8xC	a
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
resp.	resp.	kA	resp.
nadstátní	nadstátní	k2eAgFnSc2d1	nadstátní
organizace	organizace	k1gFnSc2	organizace
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
diplomatické	diplomatický	k2eAgInPc4d1	diplomatický
styky	styk	k1gInPc4	styk
s	s	k7c7	s
Českou	český	k2eAgFnSc7d1	Česká
republikou	republika	k1gFnSc7	republika
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
velvyslanectví	velvyslanectví	k1gNnSc4	velvyslanectví
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
a	a	k8xC	a
některé	některý	k3yIgInPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
mají	mít	k5eAaImIp3nP	mít
generální	generální	k2eAgInPc1d1	generální
konzuláty	konzulát	k1gInPc1	konzulát
nebo	nebo	k8xC	nebo
konzuláty	konzulát	k1gInPc1	konzulát
v	v	k7c6	v
určitých	určitý	k2eAgNnPc6d1	určité
městech	město	k1gNnPc6	město
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
má	mít	k5eAaImIp3nS	mít
ve	v	k7c6	v
státech	stát	k1gInPc6	stát
a	a	k8xC	a
u	u	k7c2	u
mezinárodních	mezinárodní	k2eAgInPc2d1	mezinárodní
resp.	resp.	kA	resp.
nadstátních	nadstátní	k2eAgFnPc2d1	nadstátní
organizací	organizace	k1gFnPc2	organizace
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yIgInPc7	který
má	mít	k5eAaImIp3nS	mít
diplomatické	diplomatický	k2eAgInPc4d1	diplomatický
styky	styk	k1gInPc4	styk
<g/>
,	,	kIx,	,
recipročně	recipročně	k6eAd1	recipročně
svá	svůj	k3xOyFgNnPc4	svůj
velvyslanectví	velvyslanectví	k1gNnPc4	velvyslanectví
(	(	kIx(	(
<g/>
ambasády	ambasáda	k1gFnPc4	ambasáda
<g/>
)	)	kIx)	)
a	a	k8xC	a
konzuláty	konzulát	k1gInPc1	konzulát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dle	dle	k7c2	dle
tzv.	tzv.	kA	tzv.
Visa	viso	k1gNnPc4	viso
Restriction	Restriction	k1gInSc1	Restriction
Indexu	index	k1gInSc2	index
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
mají	mít	k5eAaImIp3nP	mít
čeští	český	k2eAgMnPc1d1	český
občané	občan	k1gMnPc1	občan
možnost	možnost	k1gFnSc4	možnost
bezvízového	bezvízový	k2eAgInSc2d1	bezvízový
vstupu	vstup	k1gInSc2	vstup
do	do	k7c2	do
167	[number]	k4	167
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
zařazují	zařazovat	k5eAaImIp3nP	zařazovat
mězi	mězi	k6eAd1	mězi
nejméně	málo	k6eAd3	málo
vízy	vízo	k1gNnPc7	vízo
omezené	omezený	k2eAgInPc1d1	omezený
národy	národ	k1gInPc7	národ
<g/>
.	.	kIx.	.
<g/>
Hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
v	v	k7c6	v
zaměřování	zaměřování	k1gNnSc6	zaměřování
a	a	k8xC	a
upřesňování	upřesňování	k1gNnSc6	upřesňování
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
politiky	politika	k1gFnSc2	politika
mají	mít	k5eAaImIp3nP	mít
premiér	premiér	k1gMnSc1	premiér
a	a	k8xC	a
ministr	ministr	k1gMnSc1	ministr
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zahraniční	zahraniční	k2eAgFnSc4d1	zahraniční
politiku	politika	k1gFnSc4	politika
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
je	být	k5eAaImIp3nS	být
zásadní	zásadní	k2eAgNnSc4d1	zásadní
členství	členství	k1gNnSc4	členství
v	v	k7c6	v
Evropské	evropský	k2eAgFnSc6d1	Evropská
unii	unie	k1gFnSc6	unie
<g/>
.	.	kIx.	.
</s>
<s>
Té	ten	k3xDgFnSc3	ten
ČR	ČR	kA	ČR
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
i	i	k9	i
předsedala	předsedat	k5eAaImAgFnS	předsedat
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
má	mít	k5eAaImIp3nS	mít
silné	silný	k2eAgFnPc4d1	silná
vazby	vazba	k1gFnPc4	vazba
se	s	k7c7	s
Slovenskem	Slovensko	k1gNnSc7	Slovensko
<g/>
,	,	kIx,	,
Polskem	Polsko	k1gNnSc7	Polsko
a	a	k8xC	a
Maďarskem	Maďarsko	k1gNnSc7	Maďarsko
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
jako	jako	k8xS	jako
člen	člen	k1gMnSc1	člen
Visegrádské	visegrádský	k2eAgFnSc2d1	Visegrádská
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Rozsáhlé	rozsáhlý	k2eAgInPc4d1	rozsáhlý
styky	styk	k1gInPc4	styk
má	mít	k5eAaImIp3nS	mít
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
se	s	k7c7	s
sousedním	sousední	k2eAgNnSc7d1	sousední
Německem	Německo	k1gNnSc7	Německo
a	a	k8xC	a
dalšími	další	k2eAgInPc7d1	další
členskými	členský	k2eAgInPc7d1	členský
státy	stát	k1gInPc7	stát
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
se	s	k7c7	s
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
americkými	americký	k2eAgMnPc7d1	americký
a	a	k8xC	a
s	s	k7c7	s
Izraelem	Izrael	k1gInSc7	Izrael
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Blízkém	blízký	k2eAgInSc6d1	blízký
východě	východ	k1gInSc6	východ
je	být	k5eAaImIp3nS	být
důležitým	důležitý	k2eAgMnSc7d1	důležitý
partnerem	partner	k1gMnSc7	partner
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
má	mít	k5eAaImIp3nS	mít
špatné	špatný	k2eAgInPc4d1	špatný
vztahy	vztah	k1gInPc4	vztah
s	s	k7c7	s
Ruskem	Rusko	k1gNnSc7	Rusko
a	a	k8xC	a
Čínou	Čína	k1gFnSc7	Čína
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Čeští	český	k2eAgMnPc1d1	český
představitelé	představitel	k1gMnPc1	představitel
podporovali	podporovat	k5eAaImAgMnP	podporovat
disidenty	disident	k1gMnPc4	disident
v	v	k7c6	v
Barmě	Barma	k1gFnSc6	Barma
<g/>
,	,	kIx,	,
Bělorusku	Bělorusko	k1gNnSc6	Bělorusko
<g/>
,	,	kIx,	,
Moldavsku	Moldavsko	k1gNnSc6	Moldavsko
a	a	k8xC	a
na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
<g/>
.	.	kIx.	.
<g/>
Ke	k	k7c3	k
slavným	slavný	k2eAgMnPc3d1	slavný
českým	český	k2eAgMnPc3d1	český
diplomatům	diplomat	k1gMnPc3	diplomat
minulosti	minulost	k1gFnSc2	minulost
patřili	patřit	k5eAaImAgMnP	patřit
Edvard	Edvard	k1gMnSc1	Edvard
Beneš	Beneš	k1gMnSc1	Beneš
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Masaryk	Masaryk	k1gMnSc1	Masaryk
či	či	k8xC	či
Jiří	Jiří	k1gMnSc1	Jiří
Dienstbier	Dienstbier	k1gMnSc1	Dienstbier
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ozbrojené	ozbrojený	k2eAgFnPc1d1	ozbrojená
síly	síla	k1gFnPc1	síla
===	===	k?	===
</s>
</p>
<p>
<s>
Armáda	armáda	k1gFnSc1	armáda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
je	být	k5eAaImIp3nS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
do	do	k7c2	do
tří	tři	k4xCgFnPc2	tři
větví	větev	k1gFnPc2	větev
<g/>
:	:	kIx,	:
Pozemní	pozemní	k2eAgFnSc2d1	pozemní
síly	síla	k1gFnSc2	síla
AČR	AČR	kA	AČR
<g/>
,	,	kIx,	,
Vzdušné	vzdušný	k2eAgFnPc1d1	vzdušná
síly	síla	k1gFnPc1	síla
AČR	AČR	kA	AČR
a	a	k8xC	a
Síly	síla	k1gFnPc4	síla
podpory	podpora	k1gFnSc2	podpora
a	a	k8xC	a
výcviku	výcvik	k1gInSc2	výcvik
AČR	AČR	kA	AČR
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
je	být	k5eAaImIp3nS	být
vrchním	vrchní	k2eAgMnSc7d1	vrchní
velitelem	velitel	k1gMnSc7	velitel
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
byla	být	k5eAaImAgFnS	být
zrušena	zrušit	k5eAaPmNgFnS	zrušit
základní	základní	k2eAgFnSc1d1	základní
vojenská	vojenský	k2eAgFnSc1d1	vojenská
služba	služba	k1gFnSc1	služba
a	a	k8xC	a
armáda	armáda	k1gFnSc1	armáda
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stala	stát	k5eAaPmAgFnS	stát
plně	plně	k6eAd1	plně
profesionální	profesionální	k2eAgFnSc7d1	profesionální
organizací	organizace	k1gFnSc7	organizace
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1999	[number]	k4	1999
země	země	k1gFnSc1	země
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
do	do	k7c2	do
Severoatlantické	severoatlantický	k2eAgFnSc2d1	Severoatlantická
aliance	aliance	k1gFnSc2	aliance
<g/>
,	,	kIx,	,
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
níž	nízce	k6eAd2	nízce
plní	plnit	k5eAaImIp3nP	plnit
své	svůj	k3xOyFgInPc4	svůj
vojenské	vojenský	k2eAgInPc4d1	vojenský
závazky	závazek	k1gInPc4	závazek
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
aktivních	aktivní	k2eAgMnPc2d1	aktivní
vojáků	voják	k1gMnPc2	voják
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
29	[number]	k4	29
300	[number]	k4	300
včetně	včetně	k7c2	včetně
civilních	civilní	k2eAgMnPc2d1	civilní
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
České	český	k2eAgFnPc1d1	Česká
jednotky	jednotka	k1gFnPc1	jednotka
se	se	k3xPyFc4	se
podílely	podílet	k5eAaImAgFnP	podílet
na	na	k7c6	na
operacích	operace	k1gFnPc6	operace
UNPROFOR	UNPROFOR	kA	UNPROFOR
<g/>
,	,	kIx,	,
SFOR	SFOR	kA	SFOR
<g/>
,	,	kIx,	,
EUFOR	EUFOR	kA	EUFOR
v	v	k7c6	v
Bosně	Bosna	k1gFnSc6	Bosna
a	a	k8xC	a
Hercegovině	Hercegovina	k1gFnSc6	Hercegovina
<g/>
,	,	kIx,	,
KFOR	KFOR	kA	KFOR
v	v	k7c4	v
Kosovu	Kosův	k2eAgFnSc4d1	Kosova
a	a	k8xC	a
ISAF	ISAF	kA	ISAF
v	v	k7c6	v
Afghánistánu	Afghánistán	k1gInSc6	Afghánistán
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
působí	působit	k5eAaImIp3nS	působit
čeští	český	k2eAgMnPc1d1	český
vojáci	voják	k1gMnPc1	voják
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgNnSc1d1	české
letectvo	letectvo	k1gNnSc1	letectvo
se	se	k3xPyFc4	se
také	také	k9	také
podílí	podílet	k5eAaImIp3nS	podílet
na	na	k7c6	na
obraně	obrana	k1gFnSc6	obrana
vzdušného	vzdušný	k2eAgInSc2d1	vzdušný
prostoru	prostor	k1gInSc2	prostor
Pobaltských	pobaltský	k2eAgInPc2d1	pobaltský
států	stát	k1gInPc2	stát
a	a	k8xC	a
Islandu	Island	k1gInSc2	Island
<g/>
.	.	kIx.	.
<g/>
Vysokou	vysoký	k2eAgFnSc4d1	vysoká
prestiž	prestiž	k1gFnSc4	prestiž
mají	mít	k5eAaImIp3nP	mít
tradičně	tradičně	k6eAd1	tradičně
čeští	český	k2eAgMnPc1d1	český
vojenští	vojenský	k2eAgMnPc1d1	vojenský
chemici	chemik	k1gMnPc1	chemik
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
31	[number]	k4	31
<g/>
.	.	kIx.	.
pluk	pluk	k1gInSc1	pluk
radiační	radiační	k2eAgInSc1d1	radiační
<g/>
,	,	kIx,	,
chemické	chemický	k2eAgFnSc2d1	chemická
a	a	k8xC	a
biologické	biologický	k2eAgFnSc2d1	biologická
ochrany	ochrana	k1gFnSc2	ochrana
v	v	k7c6	v
Liberci	Liberec	k1gInSc6	Liberec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
svého	své	k1gNnSc2	své
prvního	první	k4xOgNnSc2	první
nasazení	nasazení	k1gNnSc2	nasazení
v	v	k7c6	v
mezinárodních	mezinárodní	k2eAgFnPc6d1	mezinárodní
silách	síla	k1gFnPc6	síla
v	v	k7c6	v
operaci	operace	k1gFnSc6	operace
Pouštní	pouštní	k2eAgFnSc2d1	pouštní
bouře	bouř	k1gFnSc2	bouř
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
hlavním	hlavní	k2eAgInPc3d1	hlavní
českým	český	k2eAgInPc3d1	český
příspěvkům	příspěvek	k1gInPc3	příspěvek
ke	k	k7c3	k
spojeneckým	spojenecký	k2eAgFnPc3d1	spojenecká
akcím	akce	k1gFnPc3	akce
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
liberečtí	liberecký	k2eAgMnPc1d1	liberecký
chemici	chemik	k1gMnPc1	chemik
pravidelně	pravidelně	k6eAd1	pravidelně
velí	velet	k5eAaImIp3nP	velet
mnohonárodnímu	mnohonárodní	k2eAgInSc3d1	mnohonárodní
praporu	prapor	k1gInSc3	prapor
radiační	radiační	k2eAgInSc1d1	radiační
<g/>
,	,	kIx,	,
chemické	chemický	k2eAgFnSc2d1	chemická
a	a	k8xC	a
biologické	biologický	k2eAgFnSc2d1	biologická
ochrany	ochrana	k1gFnSc2	ochrana
Sil	síla	k1gFnPc2	síla
rychlé	rychlý	k2eAgFnSc2d1	rychlá
reakce	reakce	k1gFnSc2	reakce
NATO	NATO	kA	NATO
(	(	kIx(	(
<g/>
NATO	NATO	kA	NATO
Response	response	k1gFnSc1	response
Force	force	k1gFnSc1	force
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Výzbroj	výzbroj	k1gInSc1	výzbroj
Armády	armáda	k1gFnSc2	armáda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
nadzvukové	nadzvukový	k2eAgFnSc2d1	nadzvuková
stíhačky	stíhačka	k1gFnSc2	stíhačka
JAS-39	JAS-39	k1gMnSc1	JAS-39
Gripen	Gripen	k2eAgMnSc1d1	Gripen
<g/>
,	,	kIx,	,
bojové	bojový	k2eAgInPc1d1	bojový
letouny	letoun	k1gInPc1	letoun
Aero	aero	k1gNnSc4	aero
L-159	L-159	k1gFnSc2	L-159
Alca	Alcum	k1gNnSc2	Alcum
<g/>
,	,	kIx,	,
útočné	útočný	k2eAgFnSc2d1	útočná
helikoptéry	helikoptéra	k1gFnSc2	helikoptéra
Mil	míle	k1gFnPc2	míle
Mi-	Mi-	k1gFnSc4	Mi-
<g/>
35	[number]	k4	35
<g/>
,	,	kIx,	,
obrněné	obrněný	k2eAgInPc1d1	obrněný
transportéry	transportér	k1gInPc1	transportér
<g/>
:	:	kIx,	:
Pandur	pandur	k1gMnSc1	pandur
II	II	kA	II
<g/>
,	,	kIx,	,
BVP-	BVP-	k1gFnSc1	BVP-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
BVP-	BVP-	k1gFnSc1	BVP-
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
OT-90	OT-90	k1gFnSc1	OT-90
a	a	k8xC	a
modernizované	modernizovaný	k2eAgInPc1d1	modernizovaný
tanky	tank	k1gInPc1	tank
T-	T-	k1gFnSc1	T-
<g/>
72	[number]	k4	72
<g/>
M	M	kA	M
<g/>
4	[number]	k4	4
<g/>
CZ	CZ	kA	CZ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejznámějšími	známý	k2eAgInPc7d3	nejznámější
českými	český	k2eAgInPc7d1	český
<g/>
,	,	kIx,	,
potažmo	potažmo	k6eAd1	potažmo
československými	československý	k2eAgMnPc7d1	československý
<g/>
,	,	kIx,	,
vojáky	voják	k1gMnPc7	voják
a	a	k8xC	a
vojevůdci	vojevůdce	k1gMnPc1	vojevůdce
minulosti	minulost	k1gFnSc2	minulost
byli	být	k5eAaImAgMnP	být
Jan	Jan	k1gMnSc1	Jan
Žižka	Žižka	k1gMnSc1	Žižka
<g/>
,	,	kIx,	,
Albrecht	Albrecht	k1gMnSc1	Albrecht
z	z	k7c2	z
Valdštejna	Valdštejno	k1gNnSc2	Valdštejno
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Radecký	Radecký	k1gMnSc1	Radecký
z	z	k7c2	z
Radče	Radč	k1gFnSc2	Radč
<g/>
,	,	kIx,	,
Ludvík	Ludvík	k1gMnSc1	Ludvík
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Kubiš	Kubiš	k1gMnSc1	Kubiš
<g/>
,	,	kIx,	,
Jozef	Jozef	k1gMnSc1	Jozef
Gabčík	Gabčík	k1gMnSc1	Gabčík
či	či	k8xC	či
František	František	k1gMnSc1	František
Fajtl	Fajtl	k1gMnSc1	Fajtl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Lidská	lidský	k2eAgNnPc1d1	lidské
práva	právo	k1gNnPc1	právo
===	===	k?	===
</s>
</p>
<p>
<s>
Lidská	lidský	k2eAgNnPc1d1	lidské
práva	právo	k1gNnPc1	právo
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
jsou	být	k5eAaImIp3nP	být
garantována	garantován	k2eAgNnPc1d1	garantováno
Listinou	listina	k1gFnSc7	listina
základních	základní	k2eAgNnPc2d1	základní
práv	právo	k1gNnPc2	právo
a	a	k8xC	a
svobod	svoboda	k1gFnPc2	svoboda
a	a	k8xC	a
mezinárodními	mezinárodní	k2eAgFnPc7d1	mezinárodní
smlouvami	smlouva	k1gFnPc7	smlouva
o	o	k7c6	o
lidských	lidský	k2eAgNnPc6d1	lidské
právech	právo	k1gNnPc6	právo
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
případy	případ	k1gInPc1	případ
porušování	porušování	k1gNnSc2	porušování
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
jako	jako	k8xC	jako
diskriminace	diskriminace	k1gFnSc1	diskriminace
romských	romský	k2eAgFnPc2d1	romská
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
kterým	který	k3yQgInSc7	který
Evropská	evropský	k2eAgFnSc1d1	Evropská
komise	komise	k1gFnSc1	komise
požádala	požádat	k5eAaPmAgFnS	požádat
Česko	Česko	k1gNnSc4	Česko
o	o	k7c4	o
podání	podání	k1gNnSc4	podání
vysvětlení	vysvětlení	k1gNnSc2	vysvětlení
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
nezákonné	zákonný	k2eNgFnPc1d1	nezákonná
sterilizace	sterilizace	k1gFnPc1	sterilizace
romských	romský	k2eAgFnPc2d1	romská
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
za	za	k7c2	za
které	který	k3yRgFnSc2	který
se	se	k3xPyFc4	se
vláda	vláda	k1gFnSc1	vláda
omluvila	omluvit	k5eAaPmAgFnS	omluvit
<g/>
.	.	kIx.	.
</s>
<s>
Dochází	docházet	k5eAaImIp3nS	docházet
také	také	k9	také
k	k	k7c3	k
porušování	porušování	k1gNnSc3	porušování
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
v	v	k7c6	v
některých	některý	k3yIgNnPc6	některý
zařízeních	zařízení	k1gNnPc6	zařízení
pro	pro	k7c4	pro
seniory	senior	k1gMnPc4	senior
a	a	k8xC	a
v	v	k7c6	v
psychiatrických	psychiatrický	k2eAgFnPc6d1	psychiatrická
léčebnách	léčebna	k1gFnPc6	léčebna
<g/>
.	.	kIx.	.
<g/>
Roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
byl	být	k5eAaImAgMnS	být
k	k	k7c3	k
ochraně	ochrana	k1gFnSc3	ochrana
základních	základní	k2eAgNnPc2d1	základní
práv	právo	k1gNnPc2	právo
a	a	k8xC	a
svobod	svoboda	k1gFnPc2	svoboda
zřízen	zřízen	k2eAgInSc1d1	zřízen
úřad	úřad	k1gInSc1	úřad
Veřejného	veřejný	k2eAgMnSc2d1	veřejný
ochránce	ochránce	k1gMnSc2	ochránce
práv	právo	k1gNnPc2	právo
(	(	kIx(	(
<g/>
ombudsmana	ombudsman	k1gMnSc4	ombudsman
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Autorita	autorita	k1gFnSc1	autorita
ombudmana	ombudman	k1gMnSc2	ombudman
je	být	k5eAaImIp3nS	být
však	však	k9	však
spíše	spíše	k9	spíše
neformální	formální	k2eNgNnPc1d1	neformální
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
indexu	index	k1gInSc2	index
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
svobody	svoboda	k1gFnSc2	svoboda
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
The	The	k1gFnPc7	The
Heritage	Heritage	k1gFnSc1	Heritage
Foundation	Foundation	k1gInSc1	Foundation
a	a	k8xC	a
The	The	k1gMnSc1	The
Wall	Wall	k1gMnSc1	Wall
Street	Street	k1gMnSc1	Street
Journal	Journal	k1gMnSc1	Journal
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Česko	Česko	k1gNnSc1	Česko
24	[number]	k4	24
<g/>
.	.	kIx.	.
nejsvobodnější	svobodný	k2eAgFnSc7d3	nejsvobodnější
zemí	zem	k1gFnSc7	zem
na	na	k7c6	na
světě	svět	k1gInSc6	svět
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
obchodu	obchod	k1gInSc2	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Světového	světový	k2eAgInSc2d1	světový
indexu	index	k1gInSc2	index
svobody	svoboda	k1gFnSc2	svoboda
tisku	tisk	k1gInSc2	tisk
<g/>
,	,	kIx,	,
vytvářeného	vytvářený	k2eAgInSc2d1	vytvářený
organizací	organizace	k1gFnPc2	organizace
Reporters	Reporters	k1gInSc1	Reporters
without	without	k1gMnSc1	without
borders	borders	k1gInSc1	borders
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
ČR	ČR	kA	ČR
21	[number]	k4	21
<g/>
.	.	kIx.	.
nejsvobodnější	svobodný	k2eAgNnSc1d3	nejsvobodnější
mediální	mediální	k2eAgNnSc1d1	mediální
prostředí	prostředí	k1gNnSc1	prostředí
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Oba	dva	k4xCgInPc1	dva
údaje	údaj	k1gInPc1	údaj
platí	platit	k5eAaImIp3nP	platit
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Praha	Praha	k1gFnSc1	Praha
je	být	k5eAaImIp3nS	být
sídlem	sídlo	k1gNnSc7	sídlo
Radio	radio	k1gNnSc4	radio
Free	Free	k1gNnSc2	Free
Europe	Europ	k1gInSc5	Europ
<g/>
/	/	kIx~	/
<g/>
Radio	radio	k1gNnSc1	radio
Liberty	Libert	k1gInPc7	Libert
<g/>
.	.	kIx.	.
</s>
<s>
Stanice	stanice	k1gFnSc1	stanice
dnes	dnes	k6eAd1	dnes
sídlí	sídlet	k5eAaImIp3nS	sídlet
na	na	k7c4	na
Hagiboru	Hagibora	k1gFnSc4	Hagibora
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
ji	on	k3xPp3gFnSc4	on
do	do	k7c2	do
Československa	Československo	k1gNnSc2	Československo
osobně	osobně	k6eAd1	osobně
pozval	pozvat	k5eAaPmAgMnS	pozvat
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
<g/>
.	.	kIx.	.
<g/>
Osoby	osoba	k1gFnPc1	osoba
stejného	stejný	k2eAgNnSc2d1	stejné
pohlaví	pohlaví	k1gNnSc2	pohlaví
mohou	moct	k5eAaImIp3nP	moct
v	v	k7c6	v
ČR	ČR	kA	ČR
uzavřít	uzavřít	k5eAaPmF	uzavřít
tzv.	tzv.	kA	tzv.
registrované	registrovaný	k2eAgNnSc1d1	registrované
partnerství	partnerství	k1gNnSc1	partnerství
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
LGBT	LGBT	kA	LGBT
práva	právo	k1gNnPc4	právo
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
nejznámějším	známý	k2eAgMnPc3d3	nejznámější
českým	český	k2eAgMnPc3d1	český
aktivistům	aktivista	k1gMnPc3	aktivista
patří	patřit	k5eAaImIp3nS	patřit
pražská	pražský	k2eAgFnSc1d1	Pražská
rodačka	rodačka	k1gFnSc1	rodačka
Berta	Berta	k1gFnSc1	Berta
von	von	k1gInSc1	von
Suttnerová	Suttnerová	k1gFnSc1	Suttnerová
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
pacifistický	pacifistický	k2eAgInSc4d1	pacifistický
boj	boj	k1gInSc4	boj
získala	získat	k5eAaPmAgFnS	získat
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
mír	mír	k1gInSc4	mír
<g/>
,	,	kIx,	,
či	či	k8xC	či
Jan	Jan	k1gMnSc1	Jan
Palach	palach	k1gInSc1	palach
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
upálil	upálit	k5eAaPmAgMnS	upálit
v	v	k7c6	v
protestu	protest	k1gInSc6	protest
proti	proti	k7c3	proti
sovětské	sovětský	k2eAgFnSc3d1	sovětská
okupaci	okupace	k1gFnSc3	okupace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Administrativní	administrativní	k2eAgNnSc1d1	administrativní
dělení	dělení	k1gNnSc1	dělení
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Kraje	kraj	k1gInPc4	kraj
====	====	k?	====
</s>
</p>
<p>
<s>
Území	území	k1gNnSc1	území
Česka	Česko	k1gNnSc2	Česko
se	se	k3xPyFc4	se
dříve	dříve	k6eAd2	dříve
dělilo	dělit	k5eAaImAgNnS	dělit
na	na	k7c4	na
sedm	sedm	k4xCc4	sedm
krajů	kraj	k1gInPc2	kraj
podle	podle	k7c2	podle
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
územním	územní	k2eAgNnSc6d1	územní
členění	členění	k1gNnSc6	členění
státu	stát	k1gInSc2	stát
v	v	k7c6	v
novelizovaném	novelizovaný	k2eAgNnSc6d1	novelizované
znění	znění	k1gNnSc6	znění
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
kraje	kraj	k1gInPc1	kraj
byly	být	k5eAaImAgInP	být
zavedeny	zavést	k5eAaPmNgInP	zavést
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
slovenskými	slovenský	k2eAgInPc7d1	slovenský
kraji	kraj	k1gInPc7	kraj
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
federalizací	federalizace	k1gFnSc7	federalizace
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
zákona	zákon	k1gInSc2	zákon
tyto	tento	k3xDgInPc4	tento
kraje	kraj	k1gInPc4	kraj
stále	stále	k6eAd1	stále
existují	existovat	k5eAaImIp3nP	existovat
<g/>
,	,	kIx,	,
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
zastíněny	zastínit	k5eAaPmNgInP	zastínit
existencí	existence	k1gFnSc7	existence
nových	nový	k2eAgInPc2d1	nový
<g/>
,	,	kIx,	,
samosprávných	samosprávný	k2eAgInPc2d1	samosprávný
krajů	kraj	k1gInPc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Osmou	osma	k1gFnSc7	osma
<g/>
,	,	kIx,	,
samostatnou	samostatný	k2eAgFnSc7d1	samostatná
územní	územní	k2eAgFnSc7d1	územní
jednotkou	jednotka	k1gFnSc7	jednotka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
některých	některý	k3yIgNnPc2	některý
hledisek	hledisko	k1gNnPc2	hledisko
postavena	postaven	k2eAgFnSc1d1	postavena
na	na	k7c4	na
roveň	roveň	k1gFnSc4	roveň
krajům	kraj	k1gInPc3	kraj
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kraje	kraj	k1gInPc1	kraj
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
okresy	okres	k1gInPc4	okres
<g/>
,	,	kIx,	,
dohromady	dohromady	k6eAd1	dohromady
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
76	[number]	k4	76
okresů	okres	k1gInPc2	okres
(	(	kIx(	(
<g/>
poslední	poslední	k2eAgMnSc1d1	poslední
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
–	–	k?	–
okres	okres	k1gInSc1	okres
Jeseník	Jeseník	k1gInSc1	Jeseník
–	–	k?	–
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
až	až	k9	až
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
území	území	k1gNnSc1	území
krajů	kraj	k1gInPc2	kraj
definováno	definovat	k5eAaBmNgNnS	definovat
výčtem	výčet	k1gInSc7	výčet
okresů	okres	k1gInPc2	okres
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Původní	původní	k2eAgInPc1d1	původní
krajské	krajský	k2eAgInPc1d1	krajský
národní	národní	k2eAgInPc1d1	národní
výbory	výbor	k1gInPc1	výbor
(	(	kIx(	(
<g/>
KNV	KNV	kA	KNV
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
národní	národní	k2eAgInPc1d1	národní
výbory	výbor	k1gInPc1	výbor
obcí	obec	k1gFnPc2	obec
a	a	k8xC	a
okresů	okres	k1gInPc2	okres
<g/>
,	,	kIx,	,
nebyly	být	k5eNaImAgInP	být
samosprávné	samosprávný	k2eAgInPc1d1	samosprávný
v	v	k7c6	v
dnešním	dnešní	k2eAgInSc6d1	dnešní
smyslu	smysl	k1gInSc6	smysl
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
samosprávné	samosprávný	k2eAgFnPc1d1	samosprávná
činnosti	činnost	k1gFnPc1	činnost
a	a	k8xC	a
pravomoci	pravomoc	k1gFnPc1	pravomoc
nebyly	být	k5eNaImAgFnP	být
legislativně	legislativně	k6eAd1	legislativně
rozlišeny	rozlišit	k5eAaPmNgFnP	rozlišit
od	od	k7c2	od
výkonu	výkon	k1gInSc2	výkon
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
<g/>
,	,	kIx,	,
měly	mít	k5eAaImAgFnP	mít
však	však	k9	však
své	svůj	k3xOyFgFnPc4	svůj
volené	volený	k2eAgFnPc4d1	volená
orgány	orgány	k1gFnPc4	orgány
(	(	kIx(	(
<g/>
plénum	plénum	k1gNnSc4	plénum
a	a	k8xC	a
radu	rada	k1gFnSc4	rada
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
pravomoci	pravomoc	k1gFnPc1	pravomoc
byly	být	k5eAaImAgFnP	být
stanoveny	stanovit	k5eAaPmNgFnP	stanovit
<g/>
.	.	kIx.	.
</s>
<s>
KNV	KNV	kA	KNV
byly	být	k5eAaImAgFnP	být
zrušeny	zrušit	k5eAaPmNgFnP	zrušit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
kraje	kraj	k1gInSc2	kraj
jako	jako	k8xC	jako
územní	územní	k2eAgInSc4d1	územní
článek	článek	k1gInSc4	článek
existují	existovat	k5eAaImIp3nP	existovat
dále	daleko	k6eAd2	daleko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nemají	mít	k5eNaImIp3nP	mít
žádný	žádný	k3yNgInSc4	žádný
volený	volený	k2eAgInSc4d1	volený
sbor	sbor	k1gInSc4	sbor
ani	ani	k8xC	ani
obecný	obecný	k2eAgInSc4d1	obecný
správní	správní	k2eAgInSc4d1	správní
úřad	úřad	k1gInSc4	úřad
<g/>
;	;	kIx,	;
jejich	jejich	k3xOp3gInPc3	jejich
obvodům	obvod	k1gInPc3	obvod
pouze	pouze	k6eAd1	pouze
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
obvody	obvod	k1gInPc1	obvod
působnosti	působnost	k1gFnSc2	působnost
krajských	krajský	k2eAgInPc2d1	krajský
soudů	soud	k1gInPc2	soud
<g/>
,	,	kIx,	,
státních	státní	k2eAgNnPc2d1	státní
zastupitelství	zastupitelství	k1gNnPc2	zastupitelství
a	a	k8xC	a
některých	některý	k3yIgInPc2	některý
dalších	další	k2eAgInPc2d1	další
orgánů	orgán	k1gInPc2	orgán
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2000	[number]	k4	2000
bylo	být	k5eAaImAgNnS	být
ústavním	ústavní	k2eAgInSc7d1	ústavní
zákonem	zákon	k1gInSc7	zákon
č.	č.	k?	č.
347	[number]	k4	347
<g/>
/	/	kIx~	/
<g/>
1997	[number]	k4	1997
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
zřízeno	zřídit	k5eAaPmNgNnS	zřídit
14	[number]	k4	14
vyšších	vysoký	k2eAgInPc2d2	vyšší
územních	územní	k2eAgInPc2d1	územní
samosprávných	samosprávný	k2eAgInPc2d1	samosprávný
celků	celek	k1gInPc2	celek
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
názvy	název	k1gInPc1	název
obsahovaly	obsahovat	k5eAaImAgInP	obsahovat
slovo	slovo	k1gNnSc4	slovo
kraj	kraj	k1gInSc4	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
byly	být	k5eAaImAgFnP	být
novelizací	novelizace	k1gFnSc7	novelizace
Ústavy	ústava	k1gFnSc2	ústava
tyto	tento	k3xDgInPc1	tento
nové	nový	k2eAgInPc1d1	nový
celky	celek	k1gInPc1	celek
nazvány	nazvat	k5eAaPmNgInP	nazvat
kraji	kraj	k1gInPc7	kraj
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
dosud	dosud	k6eAd1	dosud
nebyly	být	k5eNaImAgInP	být
zrušeny	zrušit	k5eAaPmNgInP	zrušit
územní	územní	k2eAgInPc1d1	územní
kraje	kraj	k1gInPc1	kraj
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
<g/>
.	.	kIx.	.
</s>
<s>
Krajský	krajský	k2eAgInSc1d1	krajský
úřad	úřad	k1gInSc1	úřad
je	být	k5eAaImIp3nS	být
krajským	krajský	k2eAgInSc7d1	krajský
orgánem	orgán	k1gInSc7	orgán
vykonávajícím	vykonávající	k2eAgInSc7d1	vykonávající
též	též	k6eAd1	též
přenesenou	přenesený	k2eAgFnSc4d1	přenesená
působnost	působnost	k1gFnSc4	působnost
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
<g/>
;	;	kIx,	;
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
čele	čelo	k1gNnSc6	čelo
stojí	stát	k5eAaImIp3nS	stát
ředitel	ředitel	k1gMnSc1	ředitel
<g/>
.	.	kIx.	.
</s>
<s>
Hlavou	hlava	k1gFnSc7	hlava
každého	každý	k3xTgInSc2	každý
kraje	kraj	k1gInSc2	kraj
je	být	k5eAaImIp3nS	být
hejtman	hejtman	k1gMnSc1	hejtman
<g/>
;	;	kIx,	;
pouze	pouze	k6eAd1	pouze
hlavou	hlava	k1gFnSc7	hlava
Prahy	Praha	k1gFnSc2	Praha
je	být	k5eAaImIp3nS	být
primátor	primátor	k1gMnSc1	primátor
<g/>
.	.	kIx.	.
</s>
<s>
Územní	územní	k2eAgInPc1d1	územní
kraje	kraj	k1gInPc1	kraj
ani	ani	k8xC	ani
samosprávné	samosprávný	k2eAgInPc1d1	samosprávný
kraje	kraj	k1gInPc1	kraj
nerespektují	respektovat	k5eNaImIp3nP	respektovat
hranice	hranice	k1gFnPc4	hranice
historických	historický	k2eAgFnPc2d1	historická
Českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
Jihočeského	jihočeský	k2eAgInSc2d1	jihočeský
kraje	kraj	k1gInSc2	kraj
<g/>
,	,	kIx,	,
Jihomoravského	jihomoravský	k2eAgInSc2d1	jihomoravský
kraje	kraj	k1gInSc2	kraj
<g/>
,	,	kIx,	,
Pardubického	pardubický	k2eAgInSc2d1	pardubický
kraje	kraj	k1gInSc2	kraj
a	a	k8xC	a
kraje	kraj	k1gInSc2	kraj
Vysočina	vysočina	k1gFnSc1	vysočina
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
jak	jak	k6eAd1	jak
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
a	a	k8xC	a
území	území	k1gNnSc6	území
Olomouckého	olomoucký	k2eAgInSc2d1	olomoucký
kraje	kraj	k1gInSc2	kraj
a	a	k8xC	a
Moravskoslezského	moravskoslezský	k2eAgInSc2d1	moravskoslezský
kraje	kraj	k1gInSc2	kraj
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
jak	jak	k6eAd1	jak
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
v	v	k7c6	v
Českém	český	k2eAgNnSc6d1	české
Slezsku	Slezsko	k1gNnSc6	Slezsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Okresy	okres	k1gInPc1	okres
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
byly	být	k5eAaImAgInP	být
rovněž	rovněž	k6eAd1	rovněž
okresní	okresní	k2eAgInPc1d1	okresní
národní	národní	k2eAgInPc1d1	národní
výbory	výbor	k1gInPc1	výbor
transformovány	transformován	k2eAgInPc1d1	transformován
na	na	k7c4	na
okresní	okresní	k2eAgInPc4d1	okresní
úřady	úřad	k1gInPc4	úřad
<g/>
,	,	kIx,	,
namísto	namísto	k7c2	namísto
sboru	sbor	k1gInSc2	sbor
voleného	volený	k2eAgNnSc2d1	volené
občany	občan	k1gMnPc4	občan
je	být	k5eAaImIp3nS	být
řídila	řídit	k5eAaImAgFnS	řídit
okresní	okresní	k2eAgNnSc4d1	okresní
shromáždění	shromáždění	k1gNnSc4	shromáždění
volená	volený	k2eAgFnSc1d1	volená
krajským	krajský	k2eAgNnSc7d1	krajské
zastupitelstvem	zastupitelstvo	k1gNnSc7	zastupitelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
dni	den	k1gInSc3	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2003	[number]	k4	2003
byly	být	k5eAaImAgInP	být
okresní	okresní	k2eAgInPc1d1	okresní
úřady	úřad	k1gInPc1	úřad
zrušeny	zrušit	k5eAaPmNgInP	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Okresy	okres	k1gInPc1	okres
jako	jako	k8xC	jako
jednotka	jednotka	k1gFnSc1	jednotka
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
nadále	nadále	k6eAd1	nadále
existují	existovat	k5eAaImIp3nP	existovat
<g/>
,	,	kIx,	,
okresy	okres	k1gInPc1	okres
také	také	k9	také
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
jednotkou	jednotka	k1gFnSc7	jednotka
statistickou	statistický	k2eAgFnSc7d1	statistická
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Obce	obec	k1gFnPc1	obec
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc1	jejich
správní	správní	k2eAgInPc1d1	správní
obvody	obvod	k1gInPc1	obvod
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
byly	být	k5eAaImAgInP	být
místní	místní	k2eAgInPc1d1	místní
a	a	k8xC	a
městské	městský	k2eAgInPc1d1	městský
národní	národní	k2eAgInPc1d1	národní
výbory	výbor	k1gInPc1	výbor
přeměněny	přeměněn	k2eAgInPc1d1	přeměněn
v	v	k7c4	v
obecní	obecní	k2eAgInPc4d1	obecní
a	a	k8xC	a
městské	městský	k2eAgInPc4d1	městský
úřady	úřad	k1gInPc4	úřad
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
volené	volený	k2eAgInPc1d1	volený
orgány	orgán	k1gInPc1	orgán
získaly	získat	k5eAaPmAgInP	získat
takzvanou	takzvaný	k2eAgFnSc4d1	takzvaná
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
působnost	působnost	k1gFnSc4	působnost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
začala	začít	k5eAaPmAgFnS	začít
být	být	k5eAaImF	být
rozlišována	rozlišovat	k5eAaImNgFnS	rozlišovat
od	od	k7c2	od
přenesené	přenesený	k2eAgFnSc2d1	přenesená
působnosti	působnost	k1gFnSc2	působnost
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
výkon	výkon	k1gInSc4	výkon
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
na	na	k7c6	na
nejnižší	nízký	k2eAgFnSc6d3	nejnižší
úrovni	úroveň	k1gFnSc6	úroveň
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgInS	zvolit
smíšený	smíšený	k2eAgInSc1d1	smíšený
model	model	k1gInSc1	model
<g/>
:	:	kIx,	:
obecní	obecní	k2eAgInSc1d1	obecní
úřad	úřad	k1gInSc1	úřad
je	být	k5eAaImIp3nS	být
obecním	obecní	k2eAgInSc7d1	obecní
orgánem	orgán	k1gInSc7	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
se	s	k7c7	s
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1993	[number]	k4	1993
stala	stát	k5eAaPmAgFnS	stát
samostatným	samostatný	k2eAgInSc7d1	samostatný
státem	stát	k1gInSc7	stát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c4	na
vnitřní	vnitřní	k2eAgNnSc4d1	vnitřní
územní	územní	k2eAgNnSc4d1	územní
členění	členění	k1gNnSc4	členění
to	ten	k3xDgNnSc1	ten
vliv	vliv	k1gInSc4	vliv
nemělo	mít	k5eNaImAgNnS	mít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
všeobecné	všeobecný	k2eAgFnSc2d1	všeobecná
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
se	se	k3xPyFc4	se
kraje	kraj	k1gInPc1	kraj
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c4	na
správní	správní	k2eAgInPc4d1	správní
obvody	obvod	k1gInPc4	obvod
obcí	obec	k1gFnPc2	obec
s	s	k7c7	s
rozšířenou	rozšířený	k2eAgFnSc7d1	rozšířená
působností	působnost	k1gFnSc7	působnost
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
též	též	k9	též
zvané	zvaný	k2eAgInPc1d1	zvaný
"	"	kIx"	"
<g/>
malé	malý	k2eAgInPc1d1	malý
okresy	okres	k1gInPc1	okres
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
obce	obec	k1gFnSc2	obec
III	III	kA	III
<g/>
.	.	kIx.	.
typu	typ	k1gInSc2	typ
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Takovými	takový	k3xDgFnPc7	takový
obcemi	obec	k1gFnPc7	obec
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
všechna	všechen	k3xTgNnPc4	všechen
dosavadní	dosavadní	k2eAgNnPc4d1	dosavadní
okresní	okresní	k2eAgNnPc4d1	okresní
města	město	k1gNnPc4	město
<g/>
,	,	kIx,	,
přibyla	přibýt	k5eAaPmAgFnS	přibýt
k	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
však	však	k9	však
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgNnPc2d1	další
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
obvody	obvod	k1gInPc1	obvod
se	se	k3xPyFc4	se
někde	někde	k6eAd1	někde
dále	daleko	k6eAd2	daleko
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c4	na
správní	správní	k2eAgInPc4d1	správní
obvody	obvod	k1gInPc4	obvod
obcí	obec	k1gFnPc2	obec
s	s	k7c7	s
pověřeným	pověřený	k2eAgInSc7d1	pověřený
obecním	obecní	k2eAgInSc7d1	obecní
úřadem	úřad	k1gInSc7	úřad
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
obce	obec	k1gFnSc2	obec
II	II	kA	II
<g/>
.	.	kIx.	.
typu	typ	k1gInSc2	typ
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vykonávají	vykonávat	k5eAaImIp3nP	vykonávat
některé	některý	k3yIgFnPc1	některý
specifické	specifický	k2eAgFnPc1d1	specifická
pravomoci	pravomoc	k1gFnPc1	pravomoc
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
i	i	k9	i
pro	pro	k7c4	pro
okolní	okolní	k2eAgFnPc4d1	okolní
obce	obec	k1gFnPc4	obec
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
menší	malý	k2eAgInPc1d2	menší
bývají	bývat	k5eAaImIp3nP	bývat
některé	některý	k3yIgInPc1	některý
správní	správní	k2eAgInPc1d1	správní
obvody	obvod	k1gInPc1	obvod
matrik	matrika	k1gFnPc2	matrika
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
působnosti	působnost	k1gFnPc1	působnost
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
vykonávají	vykonávat	k5eAaImIp3nP	vykonávat
pro	pro	k7c4	pro
své	svůj	k3xOyFgNnSc4	svůj
území	území	k1gNnSc4	území
všechny	všechen	k3xTgFnPc1	všechen
obce	obec	k1gFnPc1	obec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
NUTS	NUTS	kA	NUTS
====	====	k?	====
</s>
</p>
<p>
<s>
Členění	členění	k1gNnSc1	členění
na	na	k7c4	na
jednotky	jednotka	k1gFnPc4	jednotka
NUTS	NUTS	kA	NUTS
1	[number]	k4	1
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
území	území	k1gNnSc4	území
o	o	k7c4	o
velikost	velikost	k1gFnSc4	velikost
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
miliónů	milión	k4xCgInPc2	milión
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
nebylo	být	k5eNaImAgNnS	být
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
zavedeno	zaveden	k2eAgNnSc1d1	zavedeno
<g/>
.	.	kIx.	.
</s>
<s>
Jednotku	jednotka	k1gFnSc4	jednotka
NUTS	NUTS	kA	NUTS
1	[number]	k4	1
tak	tak	k6eAd1	tak
tvoří	tvořit	k5eAaImIp3nP	tvořit
celé	celý	k2eAgNnSc4d1	celé
území	území	k1gNnSc4	území
Česka	Česko	k1gNnSc2	Česko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Některé	některý	k3yIgInPc1	některý
kraje	kraj	k1gInPc1	kraj
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
směrem	směr	k1gInSc7	směr
nahoru	nahoru	k6eAd1	nahoru
<g/>
,	,	kIx,	,
sdružují	sdružovat	k5eAaImIp3nP	sdružovat
do	do	k7c2	do
statistických	statistický	k2eAgFnPc2d1	statistická
oblastí	oblast	k1gFnPc2	oblast
zvaných	zvaný	k2eAgInPc2d1	zvaný
též	též	k9	též
NUTS	NUTS	kA	NUTS
2	[number]	k4	2
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
mají	mít	k5eAaImIp3nP	mít
mít	mít	k5eAaImF	mít
srovnatelný	srovnatelný	k2eAgInSc4d1	srovnatelný
počet	počet	k1gInSc4	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mohly	moct	k5eAaImAgFnP	moct
být	být	k5eAaImF	být
centrálně	centrálně	k6eAd1	centrálně
řízeny	řídit	k5eAaImNgFnP	řídit
v	v	k7c6	v
projektech	projekt	k1gInPc6	projekt
partnerství	partnerství	k1gNnSc1	partnerství
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
a	a	k8xC	a
při	při	k7c6	při
financování	financování	k1gNnSc6	financování
místních	místní	k2eAgInPc2d1	místní
projektů	projekt	k1gInPc2	projekt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Plzeňský	plzeňský	k2eAgInSc1d1	plzeňský
a	a	k8xC	a
Jihočeský	jihočeský	k2eAgInSc1d1	jihočeský
kraj	kraj	k1gInSc1	kraj
jsou	být	k5eAaImIp3nP	být
tak	tak	k6eAd1	tak
sdruženy	sdružit	k5eAaPmNgInP	sdružit
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
NUTS	NUTS	kA	NUTS
Jihozápad	jihozápad	k1gInSc1	jihozápad
<g/>
,	,	kIx,	,
Karlovarský	karlovarský	k2eAgInSc1d1	karlovarský
a	a	k8xC	a
Ústecký	ústecký	k2eAgMnSc1d1	ústecký
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
NUTS	NUTS	kA	NUTS
Severozápad	severozápad	k1gInSc1	severozápad
<g/>
,	,	kIx,	,
Liberecký	liberecký	k2eAgInSc1d1	liberecký
<g/>
,	,	kIx,	,
Královéhradecký	královéhradecký	k2eAgInSc1d1	královéhradecký
a	a	k8xC	a
Pardubický	pardubický	k2eAgMnSc1d1	pardubický
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
NUTS	NUTS	kA	NUTS
Severovýchod	severovýchod	k1gInSc1	severovýchod
<g/>
,	,	kIx,	,
Vysočina	vysočina	k1gFnSc1	vysočina
a	a	k8xC	a
Jihomoravský	jihomoravský	k2eAgMnSc1d1	jihomoravský
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
NUTS	NUTS	kA	NUTS
Jihovýchod	jihovýchod	k1gInSc1	jihovýchod
<g/>
,	,	kIx,	,
Olomoucký	olomoucký	k2eAgMnSc1d1	olomoucký
a	a	k8xC	a
Zlínský	zlínský	k2eAgMnSc1d1	zlínský
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
NUTS	NUTS	kA	NUTS
Střední	střední	k2eAgFnSc1d1	střední
Morava	Morava	k1gFnSc1	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
NUTS	NUTS	kA	NUTS
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
NUTS	NUTS	kA	NUTS
Střední	střední	k2eAgFnPc4d1	střední
Čechy	Čechy	k1gFnPc4	Čechy
a	a	k8xC	a
NUTS	NUTS	kA	NUTS
Moravskoslezsko	Moravskoslezsko	k1gNnSc1	Moravskoslezsko
je	být	k5eAaImIp3nS	být
každá	každý	k3xTgFnSc1	každý
tvořená	tvořený	k2eAgFnSc1d1	tvořená
jediným	jediný	k2eAgInSc7d1	jediný
krajem	kraj	k1gInSc7	kraj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kraje	kraj	k1gInPc1	kraj
jsou	být	k5eAaImIp3nP	být
umístěny	umístit	k5eAaPmNgInP	umístit
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
NUTS	NUTS	kA	NUTS
3	[number]	k4	3
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
NUTS	NUTS	kA	NUTS
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
NUTS	NUTS	kA	NUTS
Střední	střední	k2eAgFnPc4d1	střední
Čechy	Čechy	k1gFnPc4	Čechy
a	a	k8xC	a
NUTS	NUTS	kA	NUTS
Moravskoslezsko	Moravskoslezsko	k1gNnSc1	Moravskoslezsko
zároveň	zároveň	k6eAd1	zároveň
i	i	k9	i
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
NUTS	NUTS	kA	NUTS
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Euroregiony	euroregion	k1gInPc4	euroregion
====	====	k?	====
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
posílení	posílení	k1gNnSc4	posílení
regionální	regionální	k2eAgFnSc2d1	regionální
spolupráce	spolupráce	k1gFnSc2	spolupráce
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
vznikají	vznikat	k5eAaImIp3nP	vznikat
takzvané	takzvaný	k2eAgInPc4d1	takzvaný
euroregiony	euroregion	k1gInPc4	euroregion
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zpravidla	zpravidla	k6eAd1	zpravidla
v	v	k7c6	v
regionech	region	k1gInPc6	region
dělených	dělený	k2eAgFnPc2d1	dělená
státními	státní	k2eAgFnPc7d1	státní
hranicemi	hranice	k1gFnPc7	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgMnSc1d3	nejstarší
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
Euroregion	euroregion	k1gInSc1	euroregion
Nisa	Nisa	k1gFnSc1	Nisa
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c4	na
trojmezí	trojmezí	k1gNnSc4	trojmezí
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
Polska	Polsko	k1gNnSc2	Polsko
především	především	k9	především
v	v	k7c6	v
povodí	povodí	k1gNnSc6	povodí
Lužické	lužický	k2eAgFnSc2d1	Lužická
Nisy	Nisa	k1gFnSc2	Nisa
<g/>
.	.	kIx.	.
</s>
<s>
Zabírá	zabírat	k5eAaImIp3nS	zabírat
plochu	plocha	k1gFnSc4	plocha
13	[number]	k4	13
033	[number]	k4	033
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
na	na	k7c6	na
českém	český	k2eAgNnSc6d1	české
území	území	k1gNnSc6	území
3	[number]	k4	3
163	[number]	k4	163
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ekonomika	ekonomika	k1gFnSc1	ekonomika
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Obecná	obecný	k2eAgFnSc1d1	obecná
charakteristika	charakteristika	k1gFnSc1	charakteristika
===	===	k?	===
</s>
</p>
<p>
<s>
Hospodářství	hospodářství	k1gNnSc1	hospodářství
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
tradičně	tradičně	k6eAd1	tradičně
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejrozvinutějším	rozvinutý	k2eAgFnPc3d3	nejrozvinutější
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Hrubý	hrubý	k2eAgInSc1d1	hrubý
domácí	domácí	k2eAgInSc1d1	domácí
produkt	produkt	k1gInSc1	produkt
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
výše	výše	k1gFnSc1	výše
vyspělých	vyspělý	k2eAgInPc2d1	vyspělý
států	stát	k1gInPc2	stát
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
byl	být	k5eAaImAgInS	být
hrubý	hrubý	k2eAgInSc1d1	hrubý
domácí	domácí	k2eAgInSc1d1	domácí
produkt	produkt	k1gInSc1	produkt
(	(	kIx(	(
<g/>
HDP	HDP	kA	HDP
<g/>
)	)	kIx)	)
na	na	k7c4	na
osobu	osoba	k1gFnSc4	osoba
v	v	k7c6	v
paritě	parita	k1gFnSc6	parita
kupní	kupní	k2eAgFnSc2d1	kupní
síly	síla	k1gFnSc2	síla
31	[number]	k4	31
600	[number]	k4	600
amerických	americký	k2eAgInPc2d1	americký
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Výkonnost	výkonnost	k1gFnSc1	výkonnost
české	český	k2eAgFnSc2d1	Česká
ekonomiky	ekonomika	k1gFnSc2	ekonomika
činí	činit	k5eAaImIp3nS	činit
84	[number]	k4	84
%	%	kIx~	%
průměru	průměr	k1gInSc6	průměr
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
ekonomika	ekonomika	k1gFnSc1	ekonomika
je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
16	[number]	k4	16
<g/>
.	.	kIx.	.
nejsilnější	silný	k2eAgFnSc1d3	nejsilnější
v	v	k7c6	v
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc7	svůj
výkonností	výkonnost	k1gFnSc7	výkonnost
předběhla	předběhnout	k5eAaPmAgFnS	předběhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
ekonomiku	ekonomika	k1gFnSc4	ekonomika
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
,	,	kIx,	,
Kypru	Kypr	k1gInSc2	Kypr
a	a	k8xC	a
nyní	nyní	k6eAd1	nyní
se	se	k3xPyFc4	se
blíží	blížit	k5eAaImIp3nS	blížit
úrovni	úroveň	k1gFnSc3	úroveň
Itálie	Itálie	k1gFnSc2	Itálie
a	a	k8xC	a
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
Světová	světový	k2eAgFnSc1d1	světová
banka	banka	k1gFnSc1	banka
(	(	kIx(	(
<g/>
WB	WB	kA	WB
<g/>
)	)	kIx)	)
již	již	k6eAd1	již
Česko	Česko	k1gNnSc1	Česko
vyřadila	vyřadit	k5eAaPmAgFnS	vyřadit
ze	z	k7c2	z
seznamu	seznam	k1gInSc2	seznam
"	"	kIx"	"
<g/>
rozvojových	rozvojový	k2eAgFnPc2d1	rozvojová
zemí	zem	k1gFnPc2	zem
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
investičního	investiční	k2eAgNnSc2d1	investiční
hlediska	hledisko	k1gNnSc2	hledisko
banka	banka	k1gFnSc1	banka
JPMorgan	JPMorgan	k1gMnSc1	JPMorgan
Chase	chasa	k1gFnSc6	chasa
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
české	český	k2eAgInPc4d1	český
státní	státní	k2eAgInPc4d1	státní
dluhopisy	dluhopis	k1gInPc4	dluhopis
zařadila	zařadit	k5eAaPmAgFnS	zařadit
do	do	k7c2	do
indexu	index	k1gInSc2	index
"	"	kIx"	"
<g/>
rozvíjejících	rozvíjející	k2eAgFnPc2d1	rozvíjející
se	se	k3xPyFc4	se
trhů	trh	k1gInPc2	trh
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
emerging	emerging	k1gInSc1	emerging
markets	markets	k1gInSc1	markets
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
dnes	dnes	k6eAd1	dnes
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
třicítce	třicítka	k1gFnSc3	třicítka
nejvyspělejších	vyspělý	k2eAgInPc2d3	nejvyspělejší
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
plátcům	plátce	k1gMnPc3	plátce
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
z	z	k7c2	z
rozpočtu	rozpočet	k1gInSc2	rozpočet
Světové	světový	k2eAgFnSc2d1	světová
banky	banka	k1gFnSc2	banka
neberou	brát	k5eNaImIp3nP	brát
peníze	peníz	k1gInPc4	peníz
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
naopak	naopak	k6eAd1	naopak
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
vkládají	vkládat	k5eAaImIp3nP	vkládat
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Indexu	index	k1gInSc2	index
společenského	společenský	k2eAgInSc2d1	společenský
rozvoje	rozvoj	k1gInSc2	rozvoj
(	(	kIx(	(
<g/>
Social	Social	k1gInSc1	Social
Progress	Progress	k1gInSc1	Progress
Index	index	k1gInSc1	index
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
nezisková	ziskový	k2eNgFnSc1d1	nezisková
organizace	organizace	k1gFnSc1	organizace
Social	Social	k1gMnSc1	Social
Progress	Progress	k1gInSc1	Progress
Imperative	imperativ	k1gInSc5	imperativ
a	a	k8xC	a
společnost	společnost	k1gFnSc1	společnost
Deloitte	Deloitt	k1gMnSc5	Deloitt
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ČR	ČR	kA	ČR
22	[number]	k4	22
<g/>
.	.	kIx.	.
nejrozvinutějším	rozvinutý	k2eAgInSc7d3	nejrozvinutější
státem	stát	k1gInSc7	stát
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
k	k	k7c3	k
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
Indexu	index	k1gInSc2	index
lidského	lidský	k2eAgInSc2d1	lidský
rozvoje	rozvoj	k1gInSc2	rozvoj
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vypočítává	vypočítávat	k5eAaImIp3nS	vypočítávat
OSN	OSN	kA	OSN
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
28	[number]	k4	28
<g/>
.	.	kIx.	.
nejrozvinutějším	rozvinutý	k2eAgInSc7d3	nejrozvinutější
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
bohatství	bohatství	k1gNnSc2	bohatství
svých	svůj	k3xOyFgMnPc2	svůj
občanů	občan	k1gMnPc2	občan
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
sestavuje	sestavovat	k5eAaImIp3nS	sestavovat
skupina	skupina	k1gFnSc1	skupina
Allianz	Allianza	k1gFnPc2	Allianza
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
taktéž	taktéž	k?	taktéž
na	na	k7c4	na
28	[number]	k4	28
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Jmění	jmění	k1gNnSc1	jmění
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
studie	studie	k1gFnSc2	studie
Global	globat	k5eAaImAgInS	globat
Wealth	Wealth	k1gInSc1	Wealth
Report	report	k1gInSc4	report
společnosti	společnost	k1gFnSc2	společnost
The	The	k1gFnSc2	The
Boston	Boston	k1gInSc1	Boston
Consulting	Consulting	k1gInSc1	Consulting
Group	Group	k1gInSc1	Group
v	v	k7c6	v
ČR	ČR	kA	ČR
rozloženo	rozložit	k5eAaPmNgNnS	rozložit
poměrně	poměrně	k6eAd1	poměrně
rovnoměrně	rovnoměrně	k6eAd1	rovnoměrně
<g/>
:	:	kIx,	:
pět	pět	k4xCc4	pět
procent	procento	k1gNnPc2	procento
nejbohatších	bohatý	k2eAgMnPc2d3	nejbohatší
Čechů	Čech	k1gMnPc2	Čech
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
45	[number]	k4	45
procent	procento	k1gNnPc2	procento
celkového	celkový	k2eAgNnSc2d1	celkové
finančního	finanční	k2eAgNnSc2d1	finanční
bohatství	bohatství	k1gNnSc2	bohatství
(	(	kIx(	(
<g/>
v	v	k7c6	v
USA	USA	kA	USA
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
77	[number]	k4	77
procent	procento	k1gNnPc2	procento
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
příjmová	příjmový	k2eAgFnSc1d1	příjmová
nerovnost	nerovnost	k1gFnSc1	nerovnost
mezi	mezi	k7c7	mezi
lidmi	člověk	k1gMnPc7	člověk
patří	patřit	k5eAaImIp3nP	patřit
v	v	k7c6	v
ČR	ČR	kA	ČR
k	k	k7c3	k
nejnižším	nízký	k2eAgFnPc3d3	nejnižší
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
souvisí	souviset	k5eAaImIp3nS	souviset
i	i	k9	i
odhad	odhad	k1gInSc1	odhad
OECD	OECD	kA	OECD
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
ČR	ČR	kA	ČR
je	být	k5eAaImIp3nS	být
nejmenší	malý	k2eAgFnSc1d3	nejmenší
míra	míra	k1gFnSc1	míra
chudoby	chudoba	k1gFnSc2	chudoba
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
ČR	ČR	kA	ČR
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
exportní	exportní	k2eAgFnSc7d1	exportní
ekonomikou	ekonomika	k1gFnSc7	ekonomika
<g/>
,	,	kIx,	,
český	český	k2eAgInSc1d1	český
export	export	k1gInSc1	export
překonal	překonat	k5eAaPmAgInS	překonat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
hranici	hranice	k1gFnSc6	hranice
3,9	[number]	k4	3,9
bilionu	bilion	k4xCgInSc2	bilion
korun	koruna	k1gFnPc2	koruna
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
byl	být	k5eAaImAgInS	být
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
nový	nový	k2eAgInSc1d1	nový
historický	historický	k2eAgInSc1d1	historický
rekord	rekord	k1gInSc1	rekord
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
mířilo	mířit	k5eAaImAgNnS	mířit
83,3	[number]	k4	83,3
procenta	procento	k1gNnSc2	procento
vývozu	vývoz	k1gInSc2	vývoz
z	z	k7c2	z
ČR	ČR	kA	ČR
do	do	k7c2	do
zemí	zem	k1gFnPc2	zem
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
států	stát	k1gInPc2	stát
dominoval	dominovat	k5eAaImAgInS	dominovat
vývoz	vývoz	k1gInSc1	vývoz
do	do	k7c2	do
Německa	Německo	k1gNnSc2	Německo
(	(	kIx(	(
<g/>
29,4	[number]	k4	29,4
procenta	procento	k1gNnSc2	procento
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
odstupem	odstup	k1gInSc7	odstup
následovaly	následovat	k5eAaImAgInP	následovat
Slovensko	Slovensko	k1gNnSc4	Slovensko
(	(	kIx(	(
<g/>
7,2	[number]	k4	7,2
procenta	procento	k1gNnSc2	procento
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
(	(	kIx(	(
<g/>
6,8	[number]	k4	6,8
<g/>
)	)	kIx)	)
a	a	k8xC	a
Čína	Čína	k1gFnSc1	Čína
(	(	kIx(	(
<g/>
7,0	[number]	k4	7,0
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zdanění	zdanění	k1gNnSc1	zdanění
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
v	v	k7c6	v
ČR	ČR	kA	ČR
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
39,6	[number]	k4	39,6
procenta	procento	k1gNnSc2	procento
HDP	HDP	kA	HDP
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
strukturu	struktura	k1gFnSc4	struktura
zdanění	zdanění	k1gNnSc2	zdanění
je	být	k5eAaImIp3nS	být
typická	typický	k2eAgFnSc1d1	typická
nízká	nízký	k2eAgFnSc1d1	nízká
míra	míra	k1gFnSc1	míra
přímých	přímý	k2eAgFnPc2d1	přímá
daní	daň	k1gFnPc2	daň
<g/>
,	,	kIx,	,
průměrná	průměrný	k2eAgFnSc1d1	průměrná
míra	míra	k1gFnSc1	míra
nepřímých	přímý	k2eNgFnPc2d1	nepřímá
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
vysoká	vysoký	k2eAgFnSc1d1	vysoká
míra	míra	k1gFnSc1	míra
sociálních	sociální	k2eAgInPc2d1	sociální
příspěvků	příspěvek	k1gInPc2	příspěvek
(	(	kIx(	(
<g/>
sociální	sociální	k2eAgNnSc4d1	sociální
a	a	k8xC	a
zdravotní	zdravotní	k2eAgNnSc4d1	zdravotní
pojištění	pojištění	k1gNnSc4	pojištění
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Typickým	typický	k2eAgInSc7d1	typický
rysem	rys	k1gInSc7	rys
české	český	k2eAgFnSc2d1	Česká
ekonomiky	ekonomika	k1gFnSc2	ekonomika
je	být	k5eAaImIp3nS	být
relativně	relativně	k6eAd1	relativně
nízká	nízký	k2eAgFnSc1d1	nízká
míra	míra	k1gFnSc1	míra
nezaměstnanosti	nezaměstnanost	k1gFnSc2	nezaměstnanost
<g/>
,	,	kIx,	,
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2016	[number]	k4	2016
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
ČR	ČR	kA	ČR
nejnižší	nízký	k2eAgFnSc6d3	nejnižší
nezaměstnanosti	nezaměstnanost	k1gFnSc6	nezaměstnanost
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Evropské	evropský	k2eAgFnSc6d1	Evropská
unii	unie	k1gFnSc6	unie
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
zadlužení	zadlužení	k1gNnSc1	zadlužení
bylo	být	k5eAaImAgNnS	být
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2015	[number]	k4	2015
sedmé	sedmý	k4xOgFnSc2	sedmý
nejnižší	nízký	k2eAgFnSc4d3	nejnižší
v	v	k7c6	v
EU	EU	kA	EU
<g/>
.	.	kIx.	.
<g/>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
2016	[number]	k4	2016
činil	činit	k5eAaImAgInS	činit
státní	státní	k2eAgInSc1d1	státní
dluh	dluh	k1gInSc1	dluh
1,836	[number]	k4	1,836
bilionu	bilion	k4xCgInSc2	bilion
korun	koruna	k1gFnPc2	koruna
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
představovalo	představovat	k5eAaImAgNnS	představovat
40,3	[number]	k4	40,3
procenta	procento	k1gNnSc2	procento
hrubého	hrubý	k2eAgInSc2d1	hrubý
domácího	domácí	k2eAgInSc2d1	domácí
produktu	produkt	k1gInSc2	produkt
(	(	kIx(	(
<g/>
průměr	průměr	k1gInSc1	průměr
EU	EU	kA	EU
85,2	[number]	k4	85,2
procenta	procento	k1gNnSc2	procento
HDP	HDP	kA	HDP
<g/>
,	,	kIx,	,
eurozóny	eurozón	k1gInPc1	eurozón
90,7	[number]	k4	90,7
procenta	procento	k1gNnSc2	procento
HDP	HDP	kA	HDP
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dluhy	dluh	k1gInPc1	dluh
českých	český	k2eAgFnPc2d1	Česká
domácností	domácnost	k1gFnPc2	domácnost
byly	být	k5eAaImAgFnP	být
k	k	k7c3	k
červnu	červen	k1gInSc3	červen
2016	[number]	k4	2016
1,369	[number]	k4	1,369
bilionu	bilion	k4xCgInSc2	bilion
korun	koruna	k1gFnPc2	koruna
<g/>
,	,	kIx,	,
dluhy	dluh	k1gInPc4	dluh
firem	firma	k1gFnPc2	firma
vůči	vůči	k7c3	vůči
bankám	banka	k1gFnPc3	banka
1,176	[number]	k4	1,176
bilionu	bilion	k4xCgInSc2	bilion
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
dividend	dividenda	k1gFnPc2	dividenda
odplynulo	odplynout	k5eAaPmAgNnS	odplynout
z	z	k7c2	z
Česka	Česko	k1gNnSc2	Česko
do	do	k7c2	do
zahraničí	zahraničí	k1gNnSc2	zahraničí
289	[number]	k4	289
miliard	miliarda	k4xCgFnPc2	miliarda
korun	koruna	k1gFnPc2	koruna
a	a	k8xC	a
podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
odhadů	odhad	k1gInPc2	odhad
se	se	k3xPyFc4	se
ročně	ročně	k6eAd1	ročně
vyvádí	vyvádět	k5eAaImIp3nS	vyvádět
z	z	k7c2	z
Česka	Česko	k1gNnSc2	Česko
do	do	k7c2	do
zahraničí	zahraničí	k1gNnSc2	zahraničí
až	až	k6eAd1	až
700	[number]	k4	700
miliard	miliarda	k4xCgFnPc2	miliarda
korun	koruna	k1gFnPc2	koruna
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
přímý	přímý	k2eAgInSc4d1	přímý
důsledek	důsledek	k1gInSc4	důsledek
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
investic	investice	k1gFnPc2	investice
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
2016	[number]	k4	2016
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
ČR	ČR	kA	ČR
23	[number]	k4	23
200	[number]	k4	200
dolarových	dolarový	k2eAgMnPc2d1	dolarový
milionářů	milionář	k1gMnPc2	milionář
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
ratingová	ratingový	k2eAgFnSc1d1	ratingová
agentura	agentura	k1gFnSc1	agentura
Standard	standard	k1gInSc1	standard
&	&	k?	&
Poor	Poor	k1gInSc1	Poor
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
oceňuje	oceňovat	k5eAaImIp3nS	oceňovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
úvěrovou	úvěrový	k2eAgFnSc4d1	úvěrová
spolehlivost	spolehlivost	k1gFnSc4	spolehlivost
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
stupněm	stupeň	k1gInSc7	stupeň
AA-	AA-	k1gFnSc2	AA-
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
poradenské	poradenský	k2eAgFnSc2d1	poradenská
sítě	síť	k1gFnSc2	síť
BDO	BDO	kA	BDO
bylo	být	k5eAaImAgNnS	být
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2016	[number]	k4	2016
Česko	Česko	k1gNnSc1	Česko
26	[number]	k4	26
<g/>
.	.	kIx.	.
nejatraktivnější	atraktivní	k2eAgFnSc1d3	nejatraktivnější
zemí	zem	k1gFnSc7	zem
světa	svět	k1gInSc2	svět
pro	pro	k7c4	pro
investory	investor	k1gMnPc4	investor
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
významným	významný	k2eAgFnPc3d1	významná
hospodářským	hospodářský	k2eAgFnPc3d1	hospodářská
institucím	instituce	k1gFnPc3	instituce
patří	patřit	k5eAaImIp3nS	patřit
Pražská	pražský	k2eAgFnSc1d1	Pražská
burza	burza	k1gFnSc1	burza
(	(	kIx(	(
<g/>
a	a	k8xC	a
její	její	k3xOp3gInSc1	její
index	index	k1gInSc1	index
PX	PX	kA	PX
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
korupcí	korupce	k1gFnSc7	korupce
se	se	k3xPyFc4	se
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
setkalo	setkat	k5eAaPmAgNnS	setkat
60	[number]	k4	60
%	%	kIx~	%
dotázaných	dotázaný	k2eAgMnPc2d1	dotázaný
podnikatelů	podnikatel	k1gMnPc2	podnikatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Měna	měna	k1gFnSc1	měna
===	===	k?	===
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgNnSc1d1	oficiální
českou	český	k2eAgFnSc7d1	Česká
měnou	měna	k1gFnSc7	měna
je	být	k5eAaImIp3nS	být
koruna	koruna	k1gFnSc1	koruna
česká	český	k2eAgFnSc1d1	Česká
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
Kč	Kč	kA	Kč
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vydává	vydávat	k5eAaImIp3nS	vydávat
ji	on	k3xPp3gFnSc4	on
Česká	český	k2eAgFnSc1d1	Česká
národní	národní	k2eAgFnSc1d1	národní
banka	banka	k1gFnSc1	banka
<g/>
,	,	kIx,	,
zcela	zcela	k6eAd1	zcela
nezávislá	závislý	k2eNgFnSc1d1	nezávislá
na	na	k7c6	na
vládě	vláda	k1gFnSc6	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Členy	člen	k1gInPc1	člen
jejího	její	k3xOp3gNnSc2	její
vedení	vedení	k1gNnSc2	vedení
–	–	k?	–
bankovní	bankovní	k2eAgFnSc2d1	bankovní
rady	rada	k1gFnSc2	rada
–	–	k?	–
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
prezident	prezident	k1gMnSc1	prezident
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Kurz	kurz	k1gInSc1	kurz
měny	měna	k1gFnSc2	měna
je	být	k5eAaImIp3nS	být
plovoucí	plovoucí	k2eAgNnSc1d1	plovoucí
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
Česká	český	k2eAgFnSc1d1	Česká
národní	národní	k2eAgFnSc1d1	národní
banka	banka	k1gFnSc1	banka
nerozhodne	rozhodnout	k5eNaPmIp3nS	rozhodnout
jinak	jinak	k6eAd1	jinak
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
od	od	k7c2	od
7	[number]	k4	7
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2013	[number]	k4	2013
intervenovala	intervenovat	k5eAaImAgFnS	intervenovat
na	na	k7c6	na
devizovém	devizový	k2eAgInSc6d1	devizový
trhu	trh	k1gInSc6	trh
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
držet	držet	k5eAaImF	držet
kurz	kurz	k1gInSc4	kurz
koruny	koruna	k1gFnSc2	koruna
vůči	vůči	k7c3	vůči
euru	euro	k1gNnSc3	euro
přibližně	přibližně	k6eAd1	přibližně
na	na	k7c6	na
hladině	hladina	k1gFnSc6	hladina
27	[number]	k4	27
korun	koruna	k1gFnPc2	koruna
za	za	k7c4	za
euro	euro	k1gNnSc4	euro
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
k	k	k7c3	k
opatření	opatření	k1gNnSc3	opatření
byly	být	k5eAaImAgFnP	být
obavy	obava	k1gFnPc4	obava
z	z	k7c2	z
deflace	deflace	k1gFnSc2	deflace
<g/>
.	.	kIx.	.
</s>
<s>
Intervence	intervence	k1gFnPc1	intervence
byly	být	k5eAaImAgFnP	být
ukončeny	ukončit	k5eAaPmNgFnP	ukončit
na	na	k7c6	na
jaře	jaro	k6eAd1	jaro
2017	[number]	k4	2017
a	a	k8xC	a
koruna	koruna	k1gFnSc1	koruna
brzy	brzy	k6eAd1	brzy
posílila	posílit	k5eAaPmAgFnS	posílit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přístupovou	přístupový	k2eAgFnSc7d1	přístupová
smlouvou	smlouva	k1gFnSc7	smlouva
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
se	se	k3xPyFc4	se
Česko	Česko	k1gNnSc1	Česko
zavázalo	zavázat	k5eAaPmAgNnS	zavázat
přijmout	přijmout	k5eAaPmF	přijmout
evropskou	evropský	k2eAgFnSc4d1	Evropská
měnu	měna	k1gFnSc4	měna
euro	euro	k1gNnSc1	euro
<g/>
.	.	kIx.	.
</s>
<s>
Vládou	vláda	k1gFnSc7	vláda
však	však	k9	však
dosud	dosud	k6eAd1	dosud
nebyl	být	k5eNaImAgInS	být
stanoven	stanovit	k5eAaPmNgInS	stanovit
termín	termín	k1gInSc1	termín
přijetí	přijetí	k1gNnSc2	přijetí
ani	ani	k8xC	ani
termín	termín	k1gInSc1	termín
vstupu	vstup	k1gInSc2	vstup
do	do	k7c2	do
mechanismu	mechanismus	k1gInSc2	mechanismus
směnných	směnný	k2eAgInPc2d1	směnný
kurzů	kurz	k1gInPc2	kurz
ERM	ERM	kA	ERM
II	II	kA	II
(	(	kIx(	(
<g/>
kurz	kurz	k1gInSc1	kurz
koruny	koruna	k1gFnSc2	koruna
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
chvíle	chvíle	k1gFnSc2	chvíle
navázán	navázán	k2eAgInSc1d1	navázán
na	na	k7c4	na
kurz	kurz	k1gInSc4	kurz
eura	euro	k1gNnSc2	euro
a	a	k8xC	a
kopíroval	kopírovat	k5eAaImAgMnS	kopírovat
by	by	kYmCp3nS	by
ho	on	k3xPp3gMnSc4	on
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
musí	muset	k5eAaImIp3nS	muset
přijetí	přijetí	k1gNnSc4	přijetí
eura	euro	k1gNnSc2	euro
předcházet	předcházet	k5eAaImF	předcházet
<g/>
.	.	kIx.	.
</s>
<s>
Vstup	vstup	k1gInSc1	vstup
do	do	k7c2	do
mechanismu	mechanismus	k1gInSc2	mechanismus
ERM	ERM	kA	ERM
II	II	kA	II
je	být	k5eAaImIp3nS	být
poslední	poslední	k2eAgFnSc1d1	poslední
z	z	k7c2	z
pěti	pět	k4xCc2	pět
konvergenčních	konvergenční	k2eAgNnPc2d1	konvergenční
kritérií	kritérion	k1gNnPc2	kritérion
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
ČR	ČR	kA	ČR
dosud	dosud	k6eAd1	dosud
nesplnila	splnit	k5eNaPmAgFnS	splnit
<g/>
.	.	kIx.	.
</s>
<s>
Přijetí	přijetí	k1gNnSc1	přijetí
eura	euro	k1gNnSc2	euro
brání	bránit	k5eAaImIp3nS	bránit
obavy	obava	k1gFnPc4	obava
z	z	k7c2	z
dluhové	dluhový	k2eAgFnSc2d1	dluhová
krize	krize	k1gFnSc2	krize
v	v	k7c6	v
eurozóně	eurozón	k1gInSc6	eurozón
a	a	k8xC	a
nízká	nízký	k2eAgFnSc1d1	nízká
podpora	podpora	k1gFnSc1	podpora
přijetí	přijetí	k1gNnSc2	přijetí
eura	euro	k1gNnSc2	euro
u	u	k7c2	u
veřejnosti	veřejnost	k1gFnSc2	veřejnost
–	–	k?	–
podle	podle	k7c2	podle
průzkumu	průzkum	k1gInSc2	průzkum
CVVM	CVVM	kA	CVVM
z	z	k7c2	z
dubna	duben	k1gInSc2	duben
2017	[number]	k4	2017
si	se	k3xPyFc3	se
zavedení	zavedení	k1gNnSc4	zavedení
eura	euro	k1gNnSc2	euro
přálo	přát	k5eAaImAgNnS	přát
jen	jen	k9	jen
21	[number]	k4	21
%	%	kIx~	%
respondentů	respondent	k1gMnPc2	respondent
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
po	po	k7c4	po
předchozích	předchozí	k2eAgNnPc2d1	předchozí
šest	šest	k4xCc4	šest
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
se	se	k3xPyFc4	se
podpora	podpora	k1gFnSc1	podpora
pohybovala	pohybovat	k5eAaImAgFnS	pohybovat
nad	nad	k7c7	nad
50	[number]	k4	50
%	%	kIx~	%
<g/>
,	,	kIx,	,
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
mírně	mírně	k6eAd1	mírně
poklesla	poklesnout	k5eAaPmAgFnS	poklesnout
a	a	k8xC	a
k	k	k7c3	k
prudkému	prudký	k2eAgInSc3d1	prudký
nárůstu	nárůst	k1gInSc3	nárůst
odporu	odpor	k1gInSc2	odpor
došlo	dojít	k5eAaPmAgNnS	dojít
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
2009	[number]	k4	2009
a	a	k8xC	a
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
<g/>
K	k	k7c3	k
historickým	historický	k2eAgFnPc3d1	historická
měnám	měna	k1gFnPc3	měna
užívaným	užívaný	k2eAgFnPc3d1	užívaná
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
patří	patřit	k5eAaImIp3nS	patřit
československá	československý	k2eAgFnSc1d1	Československá
koruna	koruna	k1gFnSc1	koruna
<g/>
,	,	kIx,	,
protektorátní	protektorátní	k2eAgFnSc1d1	protektorátní
koruna	koruna	k1gFnSc1	koruna
<g/>
,	,	kIx,	,
rakousko-uherská	rakouskoherský	k2eAgFnSc1d1	rakousko-uherská
koruna	koruna	k1gFnSc1	koruna
či	či	k8xC	či
rakousko-uherský	rakouskoherský	k2eAgInSc1d1	rakousko-uherský
zlatý	zlatý	k1gInSc1	zlatý
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
významným	významný	k2eAgFnPc3d1	významná
mincím	mince	k1gFnPc3	mince
minulosti	minulost	k1gFnSc2	minulost
patří	patřit	k5eAaImIp3nS	patřit
pražský	pražský	k2eAgInSc1d1	pražský
groš	groš	k1gInSc1	groš
či	či	k8xC	či
jáchymovský	jáchymovský	k2eAgInSc1d1	jáchymovský
tolar	tolar	k1gInSc1	tolar
<g/>
,	,	kIx,	,
od	od	k7c2	od
nějž	jenž	k3xRgNnSc2	jenž
je	být	k5eAaImIp3nS	být
patrně	patrně	k6eAd1	patrně
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
i	i	k8xC	i
slovo	slovo	k1gNnSc1	slovo
dolar	dolar	k1gInSc4	dolar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vývoj	vývoj	k1gInSc1	vývoj
ekonomiky	ekonomika	k1gFnSc2	ekonomika
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
se	se	k3xPyFc4	se
počátky	počátek	k1gInPc7	počátek
moderního	moderní	k2eAgInSc2d1	moderní
průmyslu	průmysl	k1gInSc2	průmysl
dají	dát	k5eAaPmIp3nP	dát
vysledovat	vysledovat	k5eAaPmF	vysledovat
do	do	k7c2	do
časů	čas	k1gInPc2	čas
panování	panování	k1gNnSc2	panování
císaře	císař	k1gMnSc2	císař
Rudolfa	Rudolf	k1gMnSc2	Rudolf
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
zárodky	zárodek	k1gInPc7	zárodek
moderní	moderní	k2eAgFnSc2d1	moderní
vědy	věda	k1gFnSc2	věda
objevovat	objevovat	k5eAaImF	objevovat
i	i	k9	i
první	první	k4xOgFnSc2	první
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
technologie	technologie	k1gFnSc2	technologie
a	a	k8xC	a
myšlení	myšlení	k1gNnSc2	myšlení
<g/>
.	.	kIx.	.
</s>
<s>
Proces	proces	k1gInSc4	proces
přerušila	přerušit	k5eAaPmAgFnS	přerušit
třicetiletá	třicetiletý	k2eAgFnSc1d1	třicetiletá
válka	válka	k1gFnSc1	válka
a	a	k8xC	a
i	i	k9	i
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
jisté	jistý	k2eAgFnSc3d1	jistá
prodlevě	prodleva	k1gFnSc3	prodleva
v	v	k7c6	v
navázání	navázání	k1gNnSc6	navázání
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
tradici	tradice	k1gFnSc4	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Počátky	počátek	k1gInPc1	počátek
industrializace	industrializace	k1gFnSc2	industrializace
zde	zde	k6eAd1	zde
souvisejí	souviset	k5eAaImIp3nP	souviset
s	s	k7c7	s
ideologií	ideologie	k1gFnSc7	ideologie
merkantilismu	merkantilismus	k1gInSc2	merkantilismus
a	a	k8xC	a
osvícenským	osvícenský	k2eAgInSc7d1	osvícenský
absolutismem	absolutismus	k1gInSc7	absolutismus
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
a	a	k8xC	a
Josefa	Josef	k1gMnSc2	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Průkopníky	průkopník	k1gMnPc4	průkopník
zde	zde	k6eAd1	zde
byli	být	k5eAaImAgMnP	být
zejména	zejména	k9	zejména
šlechtici	šlechtic	k1gMnPc1	šlechtic
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
průmysl	průmysl	k1gInSc1	průmysl
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
masově	masově	k6eAd1	masově
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
textilní	textilní	k2eAgInSc1d1	textilní
<g/>
,	,	kIx,	,
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
ještě	ještě	k6eAd1	ještě
osamocený	osamocený	k2eAgMnSc1d1	osamocený
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zlom	zlom	k1gInSc1	zlom
přišel	přijít	k5eAaPmAgInS	přijít
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
české	český	k2eAgFnPc1d1	Česká
země	zem	k1gFnPc1	zem
začaly	začít	k5eAaPmAgFnP	začít
rychle	rychle	k6eAd1	rychle
industrializovat	industrializovat	k5eAaBmF	industrializovat
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
tzv.	tzv.	kA	tzv.
průmyslové	průmyslový	k2eAgFnSc6d1	průmyslová
revoluci	revoluce	k1gFnSc6	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Rozvíjely	rozvíjet	k5eAaImAgInP	rozvíjet
se	se	k3xPyFc4	se
nové	nový	k2eAgInPc1d1	nový
obory	obor	k1gInPc1	obor
(	(	kIx(	(
<g/>
zejm.	zejm.	k?	zejm.
cukrovarnictví	cukrovarnictví	k1gNnSc1	cukrovarnictví
<g/>
,	,	kIx,	,
sklářství	sklářství	k1gNnSc1	sklářství
<g/>
,	,	kIx,	,
pivovarnictví	pivovarnictví	k1gNnSc1	pivovarnictví
<g/>
,	,	kIx,	,
výroba	výroba	k1gFnSc1	výroba
porcelánu	porcelán	k1gInSc2	porcelán
<g/>
,	,	kIx,	,
chemický	chemický	k2eAgInSc1d1	chemický
průmysl	průmysl	k1gInSc1	průmysl
<g/>
,	,	kIx,	,
těžký	těžký	k2eAgInSc1d1	těžký
průmysl	průmysl	k1gInSc1	průmysl
a	a	k8xC	a
strojírenství	strojírenství	k1gNnSc2	strojírenství
<g/>
)	)	kIx)	)
a	a	k8xC	a
od	od	k7c2	od
20	[number]	k4	20
<g/>
.	.	kIx.	.
a	a	k8xC	a
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
průmysl	průmysl	k1gInSc1	průmysl
také	také	k9	také
mechanizoval	mechanizovat	k5eAaBmAgInS	mechanizovat
<g/>
,	,	kIx,	,
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
parního	parní	k2eAgInSc2d1	parní
stroje	stroj	k1gInSc2	stroj
<g/>
,	,	kIx,	,
klíčového	klíčový	k2eAgInSc2d1	klíčový
vynálezu	vynález	k1gInSc2	vynález
epochy	epocha	k1gFnSc2	epocha
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
nové	nový	k2eAgFnPc1d1	nová
technologie	technologie	k1gFnPc1	technologie
(	(	kIx(	(
<g/>
elektřina	elektřina	k1gFnSc1	elektřina
<g/>
,	,	kIx,	,
telegraf	telegraf	k1gInSc1	telegraf
<g/>
,	,	kIx,	,
telefon	telefon	k1gInSc1	telefon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rodilo	rodit	k5eAaImAgNnS	rodit
se	se	k3xPyFc4	se
kapitalistické	kapitalistický	k2eAgNnSc1d1	kapitalistické
finančnictví	finančnictví	k1gNnSc1	finančnictví
(	(	kIx(	(
<g/>
Živnobanka	Živnobanka	k1gFnSc1	Živnobanka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mohutnělo	mohutnět	k5eAaImAgNnS	mohutnět
strojírenství	strojírenství	k1gNnSc1	strojírenství
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
fúzí	fúze	k1gFnPc2	fúze
a	a	k8xC	a
koncentrace	koncentrace	k1gFnSc2	koncentrace
kapitálu	kapitál	k1gInSc2	kapitál
(	(	kIx(	(
<g/>
např.	např.	kA	např.
koncerny	koncern	k1gInPc4	koncern
Škoda	škoda	k1gFnSc1	škoda
a	a	k8xC	a
ČKD	ČKD	kA	ČKD
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
začalo	začít	k5eAaPmAgNnS	začít
vyrábět	vyrábět	k5eAaImF	vyrábět
stroje	stroj	k1gInPc4	stroj
pro	pro	k7c4	pro
rozvíjející	rozvíjející	k2eAgFnSc4d1	rozvíjející
se	se	k3xPyFc4	se
železnici	železnice	k1gFnSc4	železnice
a	a	k8xC	a
systémy	systém	k1gInPc4	systém
městské	městský	k2eAgFnSc2d1	městská
hromadné	hromadný	k2eAgFnSc2d1	hromadná
dopravy	doprava	k1gFnSc2	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
výroba	výroba	k1gFnSc1	výroba
automobilů	automobil	k1gInPc2	automobil
a	a	k8xC	a
motocyklů	motocykl	k1gInPc2	motocykl
(	(	kIx(	(
<g/>
Laurin	Laurin	k1gInSc1	Laurin
a	a	k8xC	a
Klement	Klement	k1gMnSc1	Klement
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
české	český	k2eAgFnPc1d1	Česká
země	zem	k1gFnPc1	zem
nejprůmyslovější	průmyslový	k2eAgFnPc1d3	nejprůmyslovější
částí	část	k1gFnSc7	část
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
Rakousko-uherské	rakouskoherský	k2eAgFnSc2d1	rakousko-uherská
monarchie	monarchie	k1gFnSc2	monarchie
<g/>
.	.	kIx.	.
<g/>
Za	za	k7c4	za
První	první	k4xOgFnPc4	první
republiky	republika	k1gFnPc4	republika
rozmach	rozmach	k1gInSc1	rozmach
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
<g/>
,	,	kIx,	,
objevila	objevit	k5eAaPmAgFnS	objevit
se	se	k3xPyFc4	se
i	i	k9	i
nová	nový	k2eAgFnSc1d1	nová
dynamicky	dynamicky	k6eAd1	dynamicky
expandující	expandující	k2eAgNnSc4d1	expandující
odvětví	odvětví	k1gNnSc4	odvětví
(	(	kIx(	(
<g/>
např.	např.	kA	např.
obuvnictví	obuvnictví	k1gNnSc1	obuvnictví
–	–	k?	–
firma	firma	k1gFnSc1	firma
Baťa	Baťa	k1gMnSc1	Baťa
či	či	k8xC	či
zbrojařství	zbrojařství	k1gNnSc1	zbrojařství
–	–	k?	–
Zbrojovka	zbrojovka	k1gFnSc1	zbrojovka
Brno	Brno	k1gNnSc4	Brno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
kritéria	kritérion	k1gNnSc2	kritérion
"	"	kIx"	"
<g/>
národního	národní	k2eAgInSc2d1	národní
důchodu	důchod	k1gInSc2	důchod
na	na	k7c4	na
obyvatele	obyvatel	k1gMnPc4	obyvatel
<g/>
"	"	kIx"	"
byla	být	k5eAaImAgFnS	být
meziválečná	meziválečný	k2eAgFnSc1d1	meziválečná
československá	československý	k2eAgFnSc1d1	Československá
ekonomika	ekonomika	k1gFnSc1	ekonomika
14	[number]	k4	14
<g/>
.	.	kIx.	.
nejvyspělejší	vyspělý	k2eAgFnSc2d3	nejvyspělejší
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
produkce	produkce	k1gFnSc1	produkce
byla	být	k5eAaImAgFnS	být
desátá	desátý	k4xOgFnSc1	desátý
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Silně	silně	k6eAd1	silně
exportní	exportní	k2eAgFnSc4d1	exportní
ekonomiku	ekonomika	k1gFnSc4	ekonomika
ovšem	ovšem	k9	ovšem
těžce	těžce	k6eAd1	těžce
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
světová	světový	k2eAgFnSc1d1	světová
krize	krize	k1gFnSc1	krize
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
začala	začít	k5eAaPmAgFnS	začít
roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
<g/>
.	.	kIx.	.
</s>
<s>
Vlekla	vleknout	k5eAaImAgFnS	vleknout
se	se	k3xPyFc4	se
takřka	takřka	k6eAd1	takřka
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
byl	být	k5eAaImAgInS	být
průmysl	průmysl	k1gInSc1	průmysl
v	v	k7c6	v
mimořádné	mimořádný	k2eAgFnSc6d1	mimořádná
míře	míra	k1gFnSc6	míra
zestátněn	zestátněn	k2eAgMnSc1d1	zestátněn
a	a	k8xC	a
po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
komunistů	komunista	k1gMnPc2	komunista
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
začalo	začít	k5eAaPmAgNnS	začít
být	být	k5eAaImF	být
hospodářství	hospodářství	k1gNnSc1	hospodářství
centrálně	centrálně	k6eAd1	centrálně
řízené	řízený	k2eAgNnSc1d1	řízené
a	a	k8xC	a
plánované	plánovaný	k2eAgNnSc1d1	plánované
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
se	se	k3xPyFc4	se
podvolilo	podvolit	k5eAaPmAgNnS	podvolit
potřebám	potřeba	k1gFnPc3	potřeba
Rady	rada	k1gFnSc2	rada
vzájemné	vzájemný	k2eAgFnSc2d1	vzájemná
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
pomoci	pomoc	k1gFnSc2	pomoc
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
sdružovala	sdružovat	k5eAaImAgFnS	sdružovat
socialistické	socialistický	k2eAgInPc4d1	socialistický
státy	stát	k1gInPc4	stát
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c4	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
k	k	k7c3	k
přílišné	přílišný	k2eAgFnSc3d1	přílišná
orientaci	orientace	k1gFnSc3	orientace
na	na	k7c4	na
těžký	těžký	k2eAgInSc4d1	těžký
průmysl	průmysl	k1gInSc4	průmysl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
dal	dát	k5eAaPmAgInS	dát
prostor	prostor	k1gInSc1	prostor
širší	široký	k2eAgFnSc4d2	širší
a	a	k8xC	a
vyváženější	vyvážený	k2eAgFnSc6d2	vyváženější
škále	škála	k1gFnSc6	škála
odvětví	odvětví	k1gNnSc2	odvětví
<g/>
,	,	kIx,	,
navázalo	navázat	k5eAaPmAgNnS	navázat
se	se	k3xPyFc4	se
na	na	k7c4	na
dobrou	dobrý	k2eAgFnSc4d1	dobrá
strojírenskou	strojírenský	k2eAgFnSc4d1	strojírenská
a	a	k8xC	a
chemickou	chemický	k2eAgFnSc4d1	chemická
tradici	tradice	k1gFnSc4	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
vedení	vedení	k1gNnSc2	vedení
KSČ	KSČ	kA	KSČ
plánovalo	plánovat	k5eAaImAgNnS	plánovat
větší	veliký	k2eAgFnSc4d2	veliký
ekonomickou	ekonomický	k2eAgFnSc4d1	ekonomická
reformu	reforma	k1gFnSc4	reforma
(	(	kIx(	(
<g/>
Oty	Ota	k1gMnSc2	Ota
Šika	Šikus	k1gMnSc2	Šikus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
by	by	kYmCp3nS	by
centrálně	centrálně	k6eAd1	centrálně
řízenou	řízený	k2eAgFnSc4d1	řízená
ekonomiku	ekonomika	k1gFnSc4	ekonomika
skloubila	skloubit	k5eAaPmAgFnS	skloubit
s	s	k7c7	s
volným	volný	k2eAgInSc7d1	volný
trhem	trh	k1gInSc7	trh
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vpád	vpád	k1gInSc4	vpád
sovětských	sovětský	k2eAgNnPc2d1	sovětské
vojsk	vojsko	k1gNnPc2	vojsko
ji	on	k3xPp3gFnSc4	on
znemožnil	znemožnit	k5eAaPmAgInS	znemožnit
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgNnSc1d1	Nové
vedení	vedení	k1gNnSc1	vedení
KSČ	KSČ	kA	KSČ
tak	tak	k8xS	tak
zvolilo	zvolit	k5eAaPmAgNnS	zvolit
cestu	cesta	k1gFnSc4	cesta
velkých	velký	k2eAgInPc2d1	velký
infrastrukturních	infrastrukturní	k2eAgInPc2d1	infrastrukturní
a	a	k8xC	a
energetických	energetický	k2eAgInPc2d1	energetický
projektů	projekt	k1gInPc2	projekt
<g/>
,	,	kIx,	,
u	u	k7c2	u
nichž	jenž	k3xRgInPc2	jenž
bylo	být	k5eAaImAgNnS	být
centrální	centrální	k2eAgNnSc1d1	centrální
řízení	řízení	k1gNnSc1	řízení
částečně	částečně	k6eAd1	částečně
výhodou	výhoda	k1gFnSc7	výhoda
(	(	kIx(	(
<g/>
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
výstavba	výstavba	k1gFnSc1	výstavba
sídlišť	sídliště	k1gNnPc2	sídliště
<g/>
,	,	kIx,	,
dálnic	dálnice	k1gFnPc2	dálnice
<g/>
,	,	kIx,	,
jaderných	jaderný	k2eAgFnPc2d1	jaderná
elektráren	elektrárna	k1gFnPc2	elektrárna
<g/>
,	,	kIx,	,
přehrad	přehrada	k1gFnPc2	přehrada
<g/>
,	,	kIx,	,
doindustrializace	doindustrializace	k1gFnSc1	doindustrializace
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hospodářství	hospodářství	k1gNnSc1	hospodářství
ovšem	ovšem	k9	ovšem
nedokázalo	dokázat	k5eNaPmAgNnS	dokázat
pružně	pružně	k6eAd1	pružně
reagovat	reagovat	k5eAaBmF	reagovat
na	na	k7c4	na
potřeby	potřeba	k1gFnPc4	potřeba
spotřebitelů	spotřebitel	k1gMnPc2	spotřebitel
a	a	k8xC	a
nezachytilo	zachytit	k5eNaPmAgNnS	zachytit
ani	ani	k8xC	ani
technologický	technologický	k2eAgInSc1d1	technologický
převrat	převrat	k1gInSc1	převrat
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
počítačovou	počítačový	k2eAgFnSc4d1	počítačová
a	a	k8xC	a
digitální	digitální	k2eAgFnSc4d1	digitální
revoluci	revoluce	k1gFnSc4	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Specifickou	specifický	k2eAgFnSc7d1	specifická
linií	linie	k1gFnSc7	linie
<g/>
,	,	kIx,	,
snad	snad	k9	snad
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
ohledech	ohled	k1gInPc6	ohled
i	i	k9	i
podvratnou	podvratný	k2eAgFnSc4d1	podvratná
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
družstevnictví	družstevnictví	k1gNnSc1	družstevnictví
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
i	i	k9	i
nečekaná	čekaný	k2eNgFnSc1d1	nečekaná
pružnost	pružnost	k1gFnSc1	pružnost
a	a	k8xC	a
efektivita	efektivita	k1gFnSc1	efektivita
(	(	kIx(	(
<g/>
např.	např.	kA	např.
výroba	výroba	k1gFnSc1	výroba
počítačů	počítač	k1gMnPc2	počítač
v	v	k7c4	v
JZD	JZD	kA	JZD
Slušovice	Slušovice	k1gFnPc4	Slušovice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
tzv.	tzv.	kA	tzv.
perestrojky	perestrojka	k1gFnSc2	perestrojka
<g/>
,	,	kIx,	,
přestavby	přestavba	k1gFnPc4	přestavba
hospodářství	hospodářství	k1gNnSc2	hospodářství
<g/>
,	,	kIx,	,
k	k	k7c3	k
níž	jenž	k3xRgFnSc3	jenž
impulsy	impuls	k1gInPc1	impuls
přišly	přijít	k5eAaPmAgFnP	přijít
z	z	k7c2	z
Moskvy	Moskva	k1gFnSc2	Moskva
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
této	tento	k3xDgFnSc3	tento
linii	linie	k1gFnSc3	linie
dán	dát	k5eAaPmNgInS	dát
větší	veliký	k2eAgInSc1d2	veliký
prostor	prostor	k1gInSc1	prostor
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
bylo	být	k5eAaImAgNnS	být
částečně	částečně	k6eAd1	částečně
dovoleno	dovolen	k2eAgNnSc4d1	dovoleno
soukromé	soukromý	k2eAgNnSc4d1	soukromé
podnikání	podnikání	k1gNnSc4	podnikání
(	(	kIx(	(
<g/>
např.	např.	kA	např.
i	i	k8xC	i
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
současných	současný	k2eAgFnPc2d1	současná
českých	český	k2eAgFnPc2d1	Česká
firem	firma	k1gFnPc2	firma
<g/>
,	,	kIx,	,
antivirová	antivirový	k2eAgFnSc1d1	antivirová
společnost	společnost	k1gFnSc1	společnost
Avast	Avast	k1gFnSc1	Avast
<g/>
,	,	kIx,	,
začínala	začínat	k5eAaImAgFnS	začínat
jako	jako	k9	jako
perestrojkové	perestrojkový	k2eAgNnSc4d1	perestrojkový
družstvo	družstvo	k1gNnSc4	družstvo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Reformy	reforma	k1gFnPc1	reforma
byly	být	k5eAaImAgFnP	být
však	však	k9	však
příliš	příliš	k6eAd1	příliš
pomalé	pomalý	k2eAgFnPc1d1	pomalá
a	a	k8xC	a
nesetkaly	setkat	k5eNaPmAgFnP	setkat
se	se	k3xPyFc4	se
ani	ani	k8xC	ani
s	s	k7c7	s
takovou	takový	k3xDgFnSc7	takový
podporou	podpora	k1gFnSc7	podpora
veřejnosti	veřejnost	k1gFnSc2	veřejnost
jako	jako	k8xS	jako
reformy	reforma	k1gFnSc2	reforma
konce	konec	k1gInSc2	konec
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Politická	politický	k2eAgFnSc1d1	politická
revoluce	revoluce	k1gFnSc1	revoluce
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
ukončila	ukončit	k5eAaPmAgFnS	ukončit
komunistickou	komunistický	k2eAgFnSc4d1	komunistická
vládu	vláda	k1gFnSc4	vláda
i	i	k9	i
pokusy	pokus	k1gInPc4	pokus
o	o	k7c4	o
reformy	reforma	k1gFnPc4	reforma
na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
konci	konec	k1gInSc6	konec
<g/>
.	.	kIx.	.
</s>
<s>
Došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
transformaci	transformace	k1gFnSc3	transformace
centrálně	centrálně	k6eAd1	centrálně
řízeného	řízený	k2eAgNnSc2d1	řízené
hospodářství	hospodářství	k1gNnSc2	hospodářství
a	a	k8xC	a
privatizaci	privatizace	k1gFnSc4	privatizace
státního	státní	k2eAgInSc2d1	státní
majetku	majetek	k1gInSc2	majetek
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
kuponová	kuponový	k2eAgFnSc1d1	kuponová
privatizace	privatizace	k1gFnSc1	privatizace
<g/>
,	,	kIx,	,
velká	velký	k2eAgFnSc1d1	velká
privatizace	privatizace	k1gFnSc1	privatizace
<g/>
,	,	kIx,	,
malá	malý	k2eAgFnSc1d1	malá
privatizace	privatizace	k1gFnSc1	privatizace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
restitucí	restituce	k1gFnPc2	restituce
byla	být	k5eAaImAgFnS	být
také	také	k9	také
navrácena	navrácen	k2eAgFnSc1d1	navrácena
část	část	k1gFnSc1	část
majetků	majetek	k1gInPc2	majetek
znárodněných	znárodněný	k2eAgInPc2d1	znárodněný
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
<s>
Důsledky	důsledek	k1gInPc1	důsledek
otevřené	otevřený	k2eAgFnSc2d1	otevřená
konkurence	konkurence	k1gFnSc2	konkurence
nejvíce	nejvíce	k6eAd1	nejvíce
postihly	postihnout	k5eAaPmAgFnP	postihnout
odvětví	odvětví	k1gNnSc4	odvětví
těžkého	těžký	k2eAgInSc2d1	těžký
průmyslu	průmysl	k1gInSc2	průmysl
a	a	k8xC	a
těžkého	těžký	k2eAgNnSc2d1	těžké
strojírenství	strojírenství	k1gNnSc2	strojírenství
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
nebyly	být	k5eNaImAgFnP	být
konkurenceschopné	konkurenceschopný	k2eAgFnPc1d1	konkurenceschopná
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
byly	být	k5eAaImAgInP	být
chybně	chybně	k6eAd1	chybně
privatizovány	privatizovat	k5eAaImNgInP	privatizovat
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
podniků	podnik	k1gInPc2	podnik
byla	být	k5eAaImAgFnS	být
začleněna	začlenit	k5eAaPmNgFnS	začlenit
do	do	k7c2	do
nadnárodních	nadnárodní	k2eAgFnPc2d1	nadnárodní
korporací	korporace	k1gFnPc2	korporace
<g/>
.	.	kIx.	.
</s>
<s>
Podíl	podíl	k1gInSc1	podíl
průmyslu	průmysl	k1gInSc2	průmysl
na	na	k7c6	na
tvorbě	tvorba	k1gFnSc6	tvorba
HDP	HDP	kA	HDP
klesl	klesnout	k5eAaPmAgMnS	klesnout
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
rokem	rok	k1gInSc7	rok
1990	[number]	k4	1990
na	na	k7c4	na
polovinu	polovina	k1gFnSc4	polovina
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
místo	místo	k1gNnSc4	místo
zabral	zabrat	k5eAaPmAgMnS	zabrat
sektor	sektor	k1gInSc4	sektor
služeb	služba	k1gFnPc2	služba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
vstupu	vstup	k1gInSc6	vstup
země	zem	k1gFnSc2	zem
do	do	k7c2	do
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
následovalo	následovat	k5eAaImAgNnS	následovat
období	období	k1gNnSc1	období
hospodářského	hospodářský	k2eAgInSc2d1	hospodářský
růstu	růst	k1gInSc2	růst
<g/>
.	.	kIx.	.
</s>
<s>
Pomohly	pomoct	k5eAaPmAgFnP	pomoct
i	i	k9	i
přímé	přímý	k2eAgFnPc1d1	přímá
dotace	dotace	k1gFnPc1	dotace
<g/>
,	,	kIx,	,
od	od	k7c2	od
momentu	moment	k1gInSc2	moment
vstupu	vstup	k1gInSc2	vstup
do	do	k7c2	do
půlky	půlka	k1gFnSc2	půlka
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
Česko	Česko	k1gNnSc1	Česko
získalo	získat	k5eAaPmAgNnS	získat
z	z	k7c2	z
rozpočtu	rozpočet	k1gInSc2	rozpočet
EU	EU	kA	EU
o	o	k7c4	o
627,8	[number]	k4	627,8
miliardy	miliarda	k4xCgFnSc2	miliarda
korun	koruna	k1gFnPc2	koruna
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
než	než	k8xS	než
do	do	k7c2	do
něj	on	k3xPp3gNnSc2	on
odvedlo	odvést	k5eAaPmAgNnS	odvést
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
jako	jako	k9	jako
většinu	většina	k1gFnSc4	většina
států	stát	k1gInPc2	stát
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
i	i	k8xC	i
Česko	Česko	k1gNnSc4	Česko
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
světová	světový	k2eAgFnSc1d1	světová
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
krize	krize	k1gFnSc1	krize
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Projevila	projevit	k5eAaPmAgFnS	projevit
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
poklesem	pokles	k1gInSc7	pokles
HDP	HDP	kA	HDP
o	o	k7c6	o
4,5	[number]	k4	4,5
%	%	kIx~	%
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
zvyšováním	zvyšování	k1gNnSc7	zvyšování
nezaměstnanosti	nezaměstnanost	k1gFnSc2	nezaměstnanost
a	a	k8xC	a
růstem	růst	k1gInSc7	růst
zadlužení	zadlužení	k1gNnSc2	zadlužení
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Bankovní	bankovní	k2eAgInSc1d1	bankovní
systém	systém	k1gInSc1	systém
oproti	oproti	k7c3	oproti
tomu	ten	k3xDgNnSc3	ten
krizi	krize	k1gFnSc6	krize
odolal	odolat	k5eAaPmAgInS	odolat
a	a	k8xC	a
negativní	negativní	k2eAgInSc4d1	negativní
jevy	jev	k1gInPc4	jev
nebo	nebo	k8xC	nebo
přímo	přímo	k6eAd1	přímo
krachy	krach	k1gInPc4	krach
bank	banka	k1gFnPc2	banka
jako	jako	k8xS	jako
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
státech	stát	k1gInPc6	stát
EU	EU	kA	EU
se	se	k3xPyFc4	se
neuskutečnily	uskutečnit	k5eNaPmAgInP	uskutečnit
<g/>
.	.	kIx.	.
</s>
<s>
ČR	ČR	kA	ČR
se	se	k3xPyFc4	se
vyhnula	vyhnout	k5eAaPmAgFnS	vyhnout
i	i	k9	i
dluhové	dluhový	k2eAgFnSc3d1	dluhová
krizi	krize	k1gFnSc3	krize
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
postihla	postihnout	k5eAaPmAgFnS	postihnout
některé	některý	k3yIgInPc4	některý
státy	stát	k1gInPc4	stát
eurozóny	eurozóna	k1gFnSc2	eurozóna
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
se	se	k3xPyFc4	se
obnovil	obnovit	k5eAaPmAgInS	obnovit
hospodářský	hospodářský	k2eAgInSc1d1	hospodářský
růst	růst	k1gInSc1	růst
a	a	k8xC	a
česká	český	k2eAgFnSc1d1	Česká
ekonomika	ekonomika	k1gFnSc1	ekonomika
prokazuje	prokazovat	k5eAaImIp3nS	prokazovat
vysokou	vysoký	k2eAgFnSc4d1	vysoká
výkonnost	výkonnost	k1gFnSc4	výkonnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Největší	veliký	k2eAgFnPc4d3	veliký
firmy	firma	k1gFnPc4	firma
===	===	k?	===
</s>
</p>
<p>
<s>
I	i	k9	i
po	po	k7c6	po
rozsáhlé	rozsáhlý	k2eAgFnSc6d1	rozsáhlá
privatizaci	privatizace	k1gFnSc6	privatizace
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
zbyl	zbýt	k5eAaPmAgMnS	zbýt
v	v	k7c6	v
ekonomice	ekonomika	k1gFnSc6	ekonomika
silný	silný	k2eAgInSc4d1	silný
veřejný	veřejný	k2eAgInSc4d1	veřejný
sektor	sektor	k1gInSc4	sektor
s	s	k7c7	s
velkými	velký	k2eAgMnPc7d1	velký
zaměstnavateli	zaměstnavatel	k1gMnPc7	zaměstnavatel
(	(	kIx(	(
<g/>
Česká	český	k2eAgFnSc1d1	Česká
pošta	pošta	k1gFnSc1	pošta
<g/>
,	,	kIx,	,
České	český	k2eAgFnPc1d1	Česká
dráhy	dráha	k1gFnPc1	dráha
<g/>
,	,	kIx,	,
Dopravní	dopravní	k2eAgInSc1d1	dopravní
podnik	podnik	k1gInSc1	podnik
hl.	hl.	k?	hl.
m.	m.	k?	m.
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
Český	český	k2eAgInSc1d1	český
Aeroholding	Aeroholding	k1gInSc1	Aeroholding
<g/>
,	,	kIx,	,
Lesy	les	k1gInPc1	les
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
Čepro	Čepro	k1gNnSc1	Čepro
<g/>
,	,	kIx,	,
ČEPS	ČEPS	kA	ČEPS
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
případě	případ	k1gInSc6	případ
energetické	energetický	k2eAgFnSc2d1	energetická
firmy	firma	k1gFnSc2	firma
ČEZ	ČEZ	kA	ČEZ
i	i	k9	i
s	s	k7c7	s
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejziskovějších	ziskový	k2eAgFnPc2d3	nejziskovější
firem	firma	k1gFnPc2	firma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Největším	veliký	k2eAgMnSc7d3	veliký
soukromým	soukromý	k2eAgMnSc7d1	soukromý
zaměstnavatelem	zaměstnavatel	k1gMnSc7	zaměstnavatel
se	se	k3xPyFc4	se
časem	časem	k6eAd1	časem
stala	stát	k5eAaPmAgFnS	stát
potravinářsko-chemická	potravinářskohemický	k2eAgFnSc1d1	potravinářsko-chemický
skupina	skupina	k1gFnSc1	skupina
Agrofert	Agrofert	k1gMnSc1	Agrofert
(	(	kIx(	(
<g/>
Andrej	Andrej	k1gMnSc1	Andrej
Babiš	Babiš	k1gMnSc1	Babiš
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jakýmsi	jakýsi	k3yIgMnSc7	jakýsi
vítězem	vítěz	k1gMnSc7	vítěz
"	"	kIx"	"
<g/>
loterie	loterie	k1gFnSc1	loterie
kuponové	kuponový	k2eAgFnSc2d1	kuponová
privatizace	privatizace	k1gFnSc2	privatizace
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
skupina	skupina	k1gFnSc1	skupina
PPF	PPF	kA	PPF
Petra	Petr	k1gMnSc2	Petr
Kellnera	Kellner	k1gMnSc2	Kellner
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
specializuje	specializovat	k5eAaBmIp3nS	specializovat
zejména	zejména	k9	zejména
na	na	k7c4	na
finanční	finanční	k2eAgNnSc4d1	finanční
podnikání	podnikání	k1gNnSc4	podnikání
(	(	kIx(	(
<g/>
Home	Home	k1gFnSc1	Home
Credit	Credit	k1gFnSc1	Credit
<g/>
,	,	kIx,	,
Air	Air	k1gFnSc1	Air
Bank	bank	k1gInSc1	bank
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výkladní	výkladní	k2eAgFnSc7d1	výkladní
skříní	skříň	k1gFnSc7	skříň
nového	nový	k2eAgInSc2d1	nový
režimu	režim	k1gInSc2	režim
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
mezinárodně	mezinárodně	k6eAd1	mezinárodně
úspěšní	úspěšný	k2eAgMnPc1d1	úspěšný
podnikatelé	podnikatel	k1gMnPc1	podnikatel
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
založili	založit	k5eAaPmAgMnP	založit
firmu	firma	k1gFnSc4	firma
po	po	k7c6	po
revoluci	revoluce	k1gFnSc6	revoluce
tzv.	tzv.	kA	tzv.
na	na	k7c6	na
zelené	zelený	k2eAgFnSc6d1	zelená
louce	louka	k1gFnSc6	louka
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
ne	ne	k9	ne
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
starších	starý	k2eAgInPc2d2	starší
podniků	podnik	k1gInPc2	podnik
<g/>
)	)	kIx)	)
–	–	k?	–
k	k	k7c3	k
takovým	takový	k3xDgInPc3	takový
patří	patřit	k5eAaImIp3nS	patřit
firma	firma	k1gFnSc1	firma
Seznam	seznam	k1gInSc1	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
(	(	kIx(	(
<g/>
Ivo	Ivo	k1gMnSc1	Ivo
Lukačovič	Lukačovič	k1gMnSc1	Lukačovič
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
softwarové	softwarový	k2eAgFnSc2d1	softwarová
antivirové	antivirový	k2eAgFnSc2d1	antivirová
firmy	firma	k1gFnSc2	firma
Avast	Avast	k1gMnSc1	Avast
(	(	kIx(	(
<g/>
Pavel	Pavel	k1gMnSc1	Pavel
Baudiš	Baudiš	k1gMnSc1	Baudiš
<g/>
,	,	kIx,	,
Eduard	Eduard	k1gMnSc1	Eduard
Kučera	Kučera	k1gMnSc1	Kučera
<g/>
)	)	kIx)	)
a	a	k8xC	a
AVG	AVG	kA	AVG
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
česká	český	k2eAgFnSc1d1	Česká
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
prodaná	prodaný	k2eAgFnSc1d1	prodaná
do	do	k7c2	do
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
,	,	kIx,	,
posléze	posléze	k6eAd1	posléze
odkoupená	odkoupený	k2eAgNnPc4d1	odkoupené
Avastem	Avast	k1gInSc7	Avast
<g/>
)	)	kIx)	)
či	či	k8xC	či
internetový	internetový	k2eAgInSc1d1	internetový
obchod	obchod	k1gInSc1	obchod
Alza	Alza	k1gFnSc1	Alza
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
(	(	kIx(	(
<g/>
Aleš	Aleš	k1gMnSc1	Aleš
Zavoral	Zavoral	k1gMnSc1	Zavoral
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Průmysl	průmysl	k1gInSc1	průmysl
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
orientoval	orientovat	k5eAaBmAgMnS	orientovat
na	na	k7c4	na
automobily	automobil	k1gInPc4	automobil
a	a	k8xC	a
s	s	k7c7	s
nimi	on	k3xPp3gFnPc7	on
spjatou	spjatý	k2eAgFnSc4d1	spjatá
výrobu	výroba	k1gFnSc4	výroba
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
výroba	výroba	k1gFnSc1	výroba
plochých	plochý	k2eAgNnPc2d1	ploché
skel	sklo	k1gNnPc2	sklo
<g/>
,	,	kIx,	,
součástek	součástka	k1gFnPc2	součástka
a	a	k8xC	a
gumárenství	gumárenství	k1gNnPc2	gumárenství
(	(	kIx(	(
<g/>
Škoda	škoda	k1gFnSc1	škoda
<g/>
,	,	kIx,	,
Hyundai	Hyundai	k1gNnSc1	Hyundai
<g/>
,	,	kIx,	,
TPCA	TPCA	kA	TPCA
<g/>
,	,	kIx,	,
Johnson	Johnson	k1gMnSc1	Johnson
Controls	Controls	k1gInSc1	Controls
<g/>
,	,	kIx,	,
AGC	AGC	kA	AGC
<g/>
,	,	kIx,	,
MTX	MTX	kA	MTX
<g/>
,	,	kIx,	,
Sungwoo	Sungwoo	k6eAd1	Sungwoo
Hitech	hit	k1gInPc6	hit
<g/>
,	,	kIx,	,
Brose	Brose	k1gFnPc1	Brose
CZ	CZ	kA	CZ
<g/>
,	,	kIx,	,
Continental	Continental	k1gMnSc1	Continental
Automotive	Automotiv	k1gInSc5	Automotiv
<g/>
,	,	kIx,	,
ČGS	ČGS	kA	ČGS
Holding	holding	k1gInSc1	holding
<g/>
,	,	kIx,	,
Continental	Continental	k1gMnSc1	Continental
Barum	barum	k1gInSc1	barum
<g/>
,	,	kIx,	,
Automotive	Automotiv	k1gInSc5	Automotiv
Lighting	Lighting	k1gInSc1	Lighting
<g/>
,	,	kIx,	,
TRW	TRW	kA	TRW
<g/>
,	,	kIx,	,
Witte	Witt	k1gMnSc5	Witt
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
výroba	výroba	k1gFnSc1	výroba
vlakových	vlakový	k2eAgFnPc2d1	vlaková
souprav	souprava	k1gFnPc2	souprava
<g/>
,	,	kIx,	,
kolejových	kolejový	k2eAgNnPc2d1	kolejové
zařízení	zařízení	k1gNnPc2	zařízení
a	a	k8xC	a
autobusů	autobus	k1gInPc2	autobus
se	se	k3xPyFc4	se
udržela	udržet	k5eAaPmAgFnS	udržet
(	(	kIx(	(
<g/>
Škoda	škoda	k1gFnSc1	škoda
Transportation	Transportation	k1gInSc1	Transportation
<g/>
,	,	kIx,	,
Bonatrans	Bonatrans	k1gInSc1	Bonatrans
Group	Group	k1gInSc1	Group
<g/>
,	,	kIx,	,
Iveco	Iveco	k1gNnSc1	Iveco
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Minulý	minulý	k2eAgInSc1d1	minulý
režim	režim	k1gInSc1	režim
vybudoval	vybudovat	k5eAaPmAgInS	vybudovat
základnu	základna	k1gFnSc4	základna
především	především	k9	především
pro	pro	k7c4	pro
rozvoj	rozvoj	k1gInSc4	rozvoj
potravinářského	potravinářský	k2eAgMnSc2d1	potravinářský
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
petro	petro	k6eAd1	petro
<g/>
)	)	kIx)	)
<g/>
chemického	chemický	k2eAgInSc2d1	chemický
průmyslu	průmysl	k1gInSc2	průmysl
a	a	k8xC	a
energetiky	energetika	k1gFnSc2	energetika
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
potravinářství	potravinářství	k1gNnSc6	potravinářství
se	se	k3xPyFc4	se
krom	krom	k7c2	krom
Agrofertu	Agrofert	k1gInSc2	Agrofert
prosadily	prosadit	k5eAaPmAgFnP	prosadit
firmy	firma	k1gFnPc1	firma
Hruška	hruška	k1gFnSc1	hruška
<g/>
,	,	kIx,	,
Kofola	Kofola	k1gFnSc1	Kofola
či	či	k8xC	či
Hamé	Hamé	k1gNnSc1	Hamé
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
vytvoření	vytvoření	k1gNnSc6	vytvoření
české	český	k2eAgNnSc1d1	české
fast	fast	k5eAaPmF	fast
foodové	foodový	k2eAgFnSc2d1	foodový
sítě	síť	k1gFnSc2	síť
se	se	k3xPyFc4	se
pokusila	pokusit	k5eAaPmAgFnS	pokusit
firma	firma	k1gFnSc1	firma
Crocodille	Crocodille	k1gFnSc1	Crocodille
(	(	kIx(	(
<g/>
sendvičérie	sendvičérie	k1gFnSc1	sendvičérie
Crocodille	Crocodille	k1gFnSc1	Crocodille
a	a	k8xC	a
bageterie	bageterie	k1gFnSc1	bageterie
Bageterie	Bageterie	k1gFnSc2	Bageterie
Boulevard	Boulevarda	k1gFnPc2	Boulevarda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
petrochemii	petrochemie	k1gFnSc6	petrochemie
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
skupina	skupina	k1gFnSc1	skupina
KKCG	KKCG	kA	KKCG
Karla	Karel	k1gMnSc2	Karel
Komárka	Komárek	k1gMnSc2	Komárek
(	(	kIx(	(
<g/>
MND	MND	kA	MND
<g/>
,	,	kIx,	,
Vítkovice	Vítkovice	k1gInPc1	Vítkovice
Holding	holding	k1gInSc1	holding
ad	ad	k7c4	ad
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ENI	ENI	kA	ENI
<g/>
,	,	kIx,	,
Slovnaft	Slovnaft	k1gMnSc1	Slovnaft
<g/>
,	,	kIx,	,
OMV	OMV	kA	OMV
<g/>
,	,	kIx,	,
Shell	Shell	k1gInSc4	Shell
<g/>
,	,	kIx,	,
Synthos	Synthos	k1gInSc4	Synthos
<g/>
,	,	kIx,	,
Armex	Armex	k1gInSc4	Armex
<g/>
,	,	kIx,	,
Tank	tank	k1gInSc4	tank
ONO	onen	k3xDgNnSc4	onen
<g/>
,	,	kIx,	,
v	v	k7c6	v
energetice	energetika	k1gFnSc6	energetika
Energetický	energetický	k2eAgMnSc1d1	energetický
a	a	k8xC	a
průmyslový	průmyslový	k2eAgInSc1d1	průmyslový
holding	holding	k1gInSc1	holding
(	(	kIx(	(
<g/>
Daniel	Daniel	k1gMnSc1	Daniel
Křetínský	Křetínský	k1gMnSc1	Křetínský
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
RWE	RWE	kA	RWE
<g/>
,	,	kIx,	,
Unipetrol	Unipetrol	k1gInSc1	Unipetrol
<g/>
,	,	kIx,	,
Pražská	pražský	k2eAgFnSc1d1	Pražská
plynárenská	plynárenský	k2eAgFnSc1d1	plynárenská
<g/>
,	,	kIx,	,
Alpiq	Alpiq	k1gMnSc1	Alpiq
<g/>
,	,	kIx,	,
E.	E.	kA	E.
<g/>
ON	on	k3xPp3gMnSc1	on
<g/>
,	,	kIx,	,
Lama	lama	k1gMnSc1	lama
Energy	Energ	k1gMnPc4	Energ
Group	Group	k1gInSc1	Group
<g/>
,	,	kIx,	,
Eriell	Eriell	k1gInSc1	Eriell
<g/>
,	,	kIx,	,
Pražská	pražský	k2eAgFnSc1d1	Pražská
energetika	energetika	k1gFnSc1	energetika
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
mnoho	mnoho	k4c4	mnoho
potíží	potíž	k1gFnPc2	potíž
se	se	k3xPyFc4	se
drží	držet	k5eAaImIp3nS	držet
stále	stále	k6eAd1	stále
ještě	ještě	k6eAd1	ještě
silní	silný	k2eAgMnPc1d1	silný
hráči	hráč	k1gMnPc1	hráč
i	i	k9	i
v	v	k7c6	v
těžkém	těžký	k2eAgInSc6d1	těžký
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
,	,	kIx,	,
těžařství	těžařství	k1gNnSc6	těžařství
a	a	k8xC	a
těžkém	těžký	k2eAgNnSc6d1	těžké
strojírenství	strojírenství	k1gNnSc6	strojírenství
(	(	kIx(	(
<g/>
OKD	OKD	kA	OKD
<g/>
,	,	kIx,	,
ArcelorMittal	ArcelorMittal	k1gMnSc1	ArcelorMittal
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
,	,	kIx,	,
Třinecké	třinecký	k2eAgFnPc1d1	třinecká
železárny	železárna	k1gFnPc1	železárna
<g/>
,	,	kIx,	,
Czech	Czech	k1gInSc1	Czech
Coal	Coal	k1gInSc1	Coal
Pavla	Pavel	k1gMnSc2	Pavel
Tykače	Tykač	k1gMnSc2	Tykač
<g/>
,	,	kIx,	,
Sokolovská	sokolovský	k2eAgFnSc1d1	Sokolovská
uhelná	uhelný	k2eAgFnSc1d1	uhelná
<g/>
,	,	kIx,	,
Severočeské	severočeský	k2eAgInPc1d1	severočeský
doly	dol	k1gInPc1	dol
<g/>
,	,	kIx,	,
Ferona	Ferona	k1gFnSc1	Ferona
či	či	k8xC	či
Finitrading	Finitrading	k1gInSc1	Finitrading
Tomáše	Tomáš	k1gMnSc2	Tomáš
Chrenka	Chrenka	k1gFnSc1	Chrenka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
elektronice	elektronika	k1gFnSc6	elektronika
a	a	k8xC	a
přesném	přesný	k2eAgNnSc6d1	přesné
strojírenství	strojírenství	k1gNnSc6	strojírenství
fungují	fungovat	k5eAaImIp3nP	fungovat
především	především	k9	především
továrny	továrna	k1gFnPc1	továrna
Siemens	siemens	k1gInSc1	siemens
<g/>
,	,	kIx,	,
Bosch	Bosch	kA	Bosch
<g/>
,	,	kIx,	,
Foxconn	Foxconn	k1gNnSc1	Foxconn
<g/>
,	,	kIx,	,
Panasonic	Panasonic	kA	Panasonic
<g/>
,	,	kIx,	,
ABB	ABB	kA	ABB
<g/>
,	,	kIx,	,
AT	AT	kA	AT
Computers	Computers	k1gInSc1	Computers
<g/>
,	,	kIx,	,
eD	eD	k?	eD
<g/>
'	'	kIx"	'
system	syst	k1gMnSc7	syst
<g/>
,	,	kIx,	,
SWS	SWS	kA	SWS
<g/>
,	,	kIx,	,
AVX	AVX	kA	AVX
Czech	Czech	k1gInSc4	Czech
Republic	Republice	k1gFnPc2	Republice
a	a	k8xC	a
Inventec	Inventec	k1gInSc1	Inventec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozvinuly	rozvinout	k5eAaPmAgInP	rozvinout
se	se	k3xPyFc4	se
i	i	k9	i
firmy	firma	k1gFnSc2	firma
v	v	k7c6	v
některých	některý	k3yIgNnPc6	některý
netradičních	tradiční	k2eNgNnPc6d1	netradiční
odvětvích	odvětví	k1gNnPc6	odvětví
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
personalistika	personalistika	k1gFnSc1	personalistika
(	(	kIx(	(
<g/>
ManpowerGroup	ManpowerGroup	k1gInSc1	ManpowerGroup
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
outsourcing	outsourcing	k1gInSc1	outsourcing
(	(	kIx(	(
<g/>
ISS	ISS	kA	ISS
Facility	Facilit	k1gInPc1	Facilit
Services	Services	k1gInSc1	Services
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
výherní	výherní	k2eAgInPc4d1	výherní
automaty	automat	k1gInPc4	automat
(	(	kIx(	(
<g/>
European	European	k1gInSc4	European
Data	datum	k1gNnSc2	datum
Project	Projecta	k1gFnPc2	Projecta
<g/>
)	)	kIx)	)
či	či	k8xC	či
zdravotní	zdravotní	k2eAgFnPc1d1	zdravotní
služby	služba	k1gFnPc1	služba
(	(	kIx(	(
<g/>
Phoenix	Phoenix	k1gInSc1	Phoenix
<g/>
,	,	kIx,	,
Alliance	Alliance	k1gFnSc1	Alliance
Healthcare	Healthcar	k1gMnSc5	Healthcar
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
realit	realita	k1gFnPc2	realita
a	a	k8xC	a
developmentu	development	k1gInSc2	development
dominuje	dominovat	k5eAaImIp3nS	dominovat
CPI	cpi	kA	cpi
Group	Group	k1gInSc1	Group
(	(	kIx(	(
<g/>
Radovan	Radovan	k1gMnSc1	Radovan
Vítek	Vítek	k1gMnSc1	Vítek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Klíčovými	klíčový	k2eAgMnPc7d1	klíčový
hráči	hráč	k1gMnPc7	hráč
v	v	k7c6	v
bankovním	bankovní	k2eAgInSc6d1	bankovní
sektoru	sektor	k1gInSc6	sektor
jsou	být	k5eAaImIp3nP	být
Česká	český	k2eAgFnSc1d1	Česká
spořitelna	spořitelna	k1gFnSc1	spořitelna
<g/>
,	,	kIx,	,
Komerční	komerční	k2eAgFnSc1d1	komerční
banka	banka	k1gFnSc1	banka
<g/>
,	,	kIx,	,
ČSOB	ČSOB	kA	ČSOB
<g/>
,	,	kIx,	,
Česká	český	k2eAgFnSc1d1	Česká
pojišťovna	pojišťovna	k1gFnSc1	pojišťovna
a	a	k8xC	a
Kooperativa	kooperativa	k1gFnSc1	kooperativa
pojišťovna	pojišťovna	k1gFnSc1	pojišťovna
<g/>
,	,	kIx,	,
ve	v	k7c6	v
stavebnictví	stavebnictví	k1gNnSc6	stavebnictví
Metrostav	metrostav	k1gInSc1	metrostav
<g/>
,	,	kIx,	,
Skanska	Skansko	k1gNnPc1	Skansko
<g/>
,	,	kIx,	,
OHL	OHL	kA	OHL
ŽS	ŽS	kA	ŽS
<g/>
,	,	kIx,	,
Strabag	Strabag	k1gInSc1	Strabag
a	a	k8xC	a
Eurovia	Eurovia	k1gFnSc1	Eurovia
<g/>
,	,	kIx,	,
v	v	k7c6	v
mobilních	mobilní	k2eAgFnPc6d1	mobilní
komunikacích	komunikace	k1gFnPc6	komunikace
O	o	k7c4	o
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
T-Mobile	T-Mobila	k1gFnSc6	T-Mobila
a	a	k8xC	a
Vodafone	Vodafon	k1gInSc5	Vodafon
<g/>
.	.	kIx.	.
</s>
<s>
Významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
hrají	hrát	k5eAaImIp3nP	hrát
i	i	k9	i
mnohostranné	mnohostranný	k2eAgFnSc2d1	mnohostranná
finanční	finanční	k2eAgFnSc2d1	finanční
skupiny	skupina	k1gFnSc2	skupina
–	–	k?	–
BMM	BMM	kA	BMM
Group	Group	k1gMnSc1	Group
(	(	kIx(	(
<g/>
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Bakala	Bakal	k1gMnSc2	Bakal
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Penta	Penta	k1gMnSc1	Penta
Investments	Investments	k1gInSc1	Investments
(	(	kIx(	(
<g/>
Marek	Marek	k1gMnSc1	Marek
Dospiva	Dospiva	k1gFnSc1	Dospiva
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Haščák	Haščák	k1gMnSc1	Haščák
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
Kúšik	Kúšik	k1gMnSc1	Kúšik
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Unimex	Unimex	k1gInSc1	Unimex
Group	Group	k1gInSc1	Group
(	(	kIx(	(
<g/>
Jiří	Jiří	k1gMnSc1	Jiří
Šimáně	Šimán	k1gInSc6	Šimán
<g/>
)	)	kIx)	)
či	či	k8xC	či
GES	ges	k1gNnSc1	ges
Group	Group	k1gMnSc1	Group
Ivana	Ivan	k1gMnSc2	Ivan
Zacha	Zach	k1gMnSc2	Zach
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
velkým	velký	k2eAgFnPc3d1	velká
firmám	firma	k1gFnPc3	firma
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
vodárenská	vodárenský	k2eAgNnPc4d1	vodárenské
Veolia	Veolium	k1gNnPc4	Veolium
nebo	nebo	k8xC	nebo
tabákové	tabákový	k2eAgInPc4d1	tabákový
Imperial	Imperial	k1gInSc4	Imperial
Tobacco	Tobacco	k6eAd1	Tobacco
a	a	k8xC	a
Philip	Philip	k1gInSc4	Philip
Morris	Morris	k1gFnSc2	Morris
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zemědělství	zemědělství	k1gNnSc1	zemědělství
===	===	k?	===
</s>
</p>
<p>
<s>
Pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
se	se	k3xPyFc4	se
hlavně	hlavně	k9	hlavně
obilí	obilí	k1gNnSc2	obilí
(	(	kIx(	(
<g/>
pšenice	pšenice	k1gFnSc1	pšenice
<g/>
,	,	kIx,	,
ječmen	ječmen	k1gInSc1	ječmen
<g/>
,	,	kIx,	,
kukuřice	kukuřice	k1gFnSc1	kukuřice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
brambory	brambor	k1gInPc1	brambor
<g/>
,	,	kIx,	,
cukrová	cukrový	k2eAgFnSc1d1	cukrová
řepa	řepa	k1gFnSc1	řepa
<g/>
;	;	kIx,	;
z	z	k7c2	z
technických	technický	k2eAgFnPc2d1	technická
plodin	plodina	k1gFnPc2	plodina
len	len	k1gInSc1	len
a	a	k8xC	a
řepka	řepka	k1gFnSc1	řepka
<g/>
.	.	kIx.	.
</s>
<s>
Význam	význam	k1gInSc1	význam
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
pěstování	pěstování	k1gNnSc1	pěstování
chmele	chmel	k1gInSc2	chmel
<g/>
,	,	kIx,	,
sadařství	sadařství	k1gNnSc2	sadařství
a	a	k8xC	a
vinohradnictví	vinohradnictví	k1gNnSc2	vinohradnictví
<g/>
.	.	kIx.	.
</s>
<s>
Základem	základ	k1gInSc7	základ
živočišné	živočišný	k2eAgFnSc2d1	živočišná
výroby	výroba	k1gFnSc2	výroba
je	být	k5eAaImIp3nS	být
chov	chov	k1gInSc4	chov
skotu	skot	k1gInSc2	skot
<g/>
,	,	kIx,	,
prasat	prase	k1gNnPc2	prase
a	a	k8xC	a
drůbeže	drůbež	k1gFnSc2	drůbež
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
včelařství	včelařství	k1gNnSc1	včelařství
nebo	nebo	k8xC	nebo
chov	chov	k1gInSc1	chov
sladkovodních	sladkovodní	k2eAgFnPc2d1	sladkovodní
ryb	ryba	k1gFnPc2	ryba
(	(	kIx(	(
<g/>
zvláště	zvláště	k9	zvláště
kaprů	kapr	k1gMnPc2	kapr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zemědělci	zemědělec	k1gMnPc1	zemědělec
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2016	[number]	k4	2016
hospodařili	hospodařit	k5eAaImAgMnP	hospodařit
na	na	k7c4	na
4,26	[number]	k4	4,26
milionů	milion	k4xCgInPc2	milion
hektarů	hektar	k1gInPc2	hektar
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
půdy	půda	k1gFnSc2	půda
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
představovalo	představovat	k5eAaImAgNnS	představovat
54	[number]	k4	54
procent	procento	k1gNnPc2	procento
celkové	celkový	k2eAgFnSc2d1	celková
rozlohy	rozloha	k1gFnSc2	rozloha
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jednoho	jeden	k4xCgMnSc4	jeden
obyvatele	obyvatel	k1gMnSc4	obyvatel
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
připadlo	připadnout	k5eAaPmAgNnS	připadnout
0,42	[number]	k4	0,42
hektaru	hektar	k1gInSc2	hektar
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
půdy	půda	k1gFnSc2	půda
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
0,30	[number]	k4	0,30
ha	ha	kA	ha
půdy	půda	k1gFnSc2	půda
orné	orný	k2eAgFnSc2d1	orná
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
evropskému	evropský	k2eAgInSc3d1	evropský
průměru	průměr	k1gInSc3	průměr
<g/>
.	.	kIx.	.
</s>
<s>
Třetinu	třetina	k1gFnSc4	třetina
půdního	půdní	k2eAgInSc2d1	půdní
fondu	fond	k1gInSc2	fond
tvoří	tvořit	k5eAaImIp3nP	tvořit
lesy	les	k1gInPc1	les
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
ubývá	ubývat	k5eAaImIp3nS	ubývat
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
půdy	půda	k1gFnSc2	půda
<g/>
,	,	kIx,	,
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2016	[number]	k4	2016
asi	asi	k9	asi
15	[number]	k4	15
tisíc	tisíc	k4xCgInPc2	tisíc
hektarů	hektar	k1gInPc2	hektar
<g/>
,	,	kIx,	,
oproti	oproti	k7c3	oproti
tomu	ten	k3xDgNnSc3	ten
výměra	výměra	k1gFnSc1	výměra
lesů	les	k1gInPc2	les
vzrostla	vzrůst	k5eAaPmAgFnS	vzrůst
o	o	k7c4	o
16	[number]	k4	16
tisíc	tisíc	k4xCgInPc2	tisíc
hektarů	hektar	k1gInPc2	hektar
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
klesá	klesat	k5eAaImIp3nS	klesat
výměra	výměra	k1gFnSc1	výměra
orné	orný	k2eAgFnSc2d1	orná
půdy	půda	k1gFnSc2	půda
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
trvalých	trvalý	k2eAgInPc2d1	trvalý
travních	travní	k2eAgInPc2d1	travní
porostů	porost	k1gInPc2	porost
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
lučin	lučina	k1gFnPc2	lučina
a	a	k8xC	a
pastvin	pastvina	k1gFnPc2	pastvina
<g/>
.	.	kIx.	.
</s>
<s>
Těch	ten	k3xDgMnPc2	ten
v	v	k7c6	v
uvedeném	uvedený	k2eAgNnSc6d1	uvedené
období	období	k1gNnSc6	období
přibylo	přibýt	k5eAaPmAgNnS	přibýt
71	[number]	k4	71
tisíc	tisíc	k4xCgInPc2	tisíc
hektarů	hektar	k1gInPc2	hektar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Typické	typický	k2eAgFnPc1d1	typická
pro	pro	k7c4	pro
ČR	ČR	kA	ČR
je	být	k5eAaImIp3nS	být
vysoké	vysoký	k2eAgNnSc4d1	vysoké
procento	procento	k1gNnSc4	procento
zemědělských	zemědělský	k2eAgInPc2d1	zemědělský
podniků	podnik	k1gInPc2	podnik
vlastněných	vlastněný	k2eAgFnPc2d1	vlastněná
právnickými	právnický	k2eAgFnPc7d1	právnická
osobami	osoba	k1gFnPc7	osoba
<g/>
,	,	kIx,	,
po	po	k7c6	po
Francii	Francie	k1gFnSc6	Francie
(	(	kIx(	(
<g/>
29,2	[number]	k4	29,2
%	%	kIx~	%
<g/>
)	)	kIx)	)
jsme	být	k5eAaImIp1nP	být
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
ohledu	ohled	k1gInSc6	ohled
s	s	k7c7	s
13,5	[number]	k4	13,5
%	%	kIx~	%
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
<s>
Typický	typický	k2eAgMnSc1d1	typický
pro	pro	k7c4	pro
české	český	k2eAgNnSc4d1	české
zemědělství	zemědělství	k1gNnSc4	zemědělství
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
velký	velký	k2eAgInSc1d1	velký
podíl	podíl	k1gInSc1	podíl
pronajaté	pronajatý	k2eAgFnSc2d1	pronajatá
půdy	půda	k1gFnSc2	půda
(	(	kIx(	(
<g/>
asi	asi	k9	asi
90	[number]	k4	90
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
velikostní	velikostní	k2eAgFnSc1d1	velikostní
struktura	struktura	k1gFnSc1	struktura
zemědělských	zemědělský	k2eAgInPc2d1	zemědělský
podniků	podnik	k1gInPc2	podnik
v	v	k7c6	v
ČR	ČR	kA	ČR
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
struktury	struktura	k1gFnSc2	struktura
podniků	podnik	k1gInPc2	podnik
v	v	k7c6	v
ostatních	ostatní	k2eAgFnPc6d1	ostatní
zemích	zem	k1gFnPc6	zem
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgInPc1d1	velký
podniky	podnik	k1gInPc1	podnik
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
ty	ty	k3xPp2nSc1	ty
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
50	[number]	k4	50
hektary	hektar	k1gInPc7	hektar
obdělávané	obdělávaný	k2eAgFnSc2d1	obdělávaná
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
půdy	půda	k1gFnSc2	půda
<g/>
,	,	kIx,	,
v	v	k7c6	v
ČR	ČR	kA	ČR
obhospodařují	obhospodařovat	k5eAaImIp3nP	obhospodařovat
92,2	[number]	k4	92,2
%	%	kIx~	%
z	z	k7c2	z
celkové	celkový	k2eAgFnSc2d1	celková
výměry	výměra	k1gFnSc2	výměra
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
zbytku	zbytek	k1gInSc6	zbytek
EU	EU	kA	EU
jsou	být	k5eAaImIp3nP	být
obvyklejší	obvyklý	k2eAgInPc1d2	obvyklejší
menší	malý	k2eAgInPc1d2	menší
podniky	podnik	k1gInPc1	podnik
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
je	být	k5eAaImIp3nS	být
mj.	mj.	kA	mj.
česká	český	k2eAgFnSc1d1	Česká
tradice	tradice	k1gFnSc1	tradice
družstevnictví	družstevnictví	k1gNnSc2	družstevnictví
i	i	k8xC	i
kolektivizace	kolektivizace	k1gFnSc1	kolektivizace
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
2004	[number]	k4	2004
pracovalo	pracovat	k5eAaImAgNnS	pracovat
v	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
141	[number]	k4	141
000	[number]	k4	000
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
ve	v	k7c6	v
struktuře	struktura	k1gFnSc6	struktura
zaměstnanosti	zaměstnanost	k1gFnSc2	zaměstnanost
to	ten	k3xDgNnSc1	ten
představovalo	představovat	k5eAaImAgNnS	představovat
2,9	[number]	k4	2,9
%	%	kIx~	%
pracovníků	pracovník	k1gMnPc2	pracovník
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Typické	typický	k2eAgNnSc1d1	typické
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
počet	počet	k1gInSc1	počet
pracovníků	pracovník	k1gMnPc2	pracovník
v	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
od	od	k7c2	od
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
trvale	trvale	k6eAd1	trvale
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
.	.	kIx.	.
<g/>
K	k	k7c3	k
zásadním	zásadní	k2eAgInPc3d1	zásadní
výsledkům	výsledek	k1gInPc3	výsledek
českého	český	k2eAgNnSc2d1	české
chovatelství	chovatelství	k1gNnSc2	chovatelství
a	a	k8xC	a
šlechtitelství	šlechtitelství	k1gNnSc2	šlechtitelství
patří	patřit	k5eAaImIp3nS	patřit
plemena	plemeno	k1gNnPc4	plemeno
jako	jako	k8xS	jako
Československý	československý	k2eAgMnSc1d1	československý
vlčák	vlčák	k1gMnSc1	vlčák
<g/>
,	,	kIx,	,
Český	český	k2eAgMnSc1d1	český
teriér	teriér	k1gMnSc1	teriér
<g/>
,	,	kIx,	,
Pražský	pražský	k2eAgMnSc1d1	pražský
krysařík	krysařík	k1gMnSc1	krysařík
<g/>
,	,	kIx,	,
Chodský	chodský	k2eAgMnSc1d1	chodský
pes	pes	k1gMnSc1	pes
<g/>
,	,	kIx,	,
Český	český	k2eAgMnSc1d1	český
fousek	fousek	k1gMnSc1	fousek
či	či	k8xC	či
Starokladrubský	Starokladrubský	k2eAgMnSc1d1	Starokladrubský
kůň	kůň	k1gMnSc1	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Unikátní	unikátní	k2eAgInSc1d1	unikátní
byl	být	k5eAaImAgInS	být
rovněž	rovněž	k9	rovněž
projekt	projekt	k1gInSc1	projekt
na	na	k7c4	na
záchranu	záchrana	k1gFnSc4	záchrana
Huculského	huculský	k2eAgMnSc2d1	huculský
koně	kůň	k1gMnSc2	kůň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Průmysl	průmysl	k1gInSc1	průmysl
===	===	k?	===
</s>
</p>
<p>
<s>
Průmysl	průmysl	k1gInSc1	průmysl
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
35	[number]	k4	35
%	%	kIx~	%
hrubého	hrubý	k2eAgInSc2d1	hrubý
produktu	produkt	k1gInSc2	produkt
českého	český	k2eAgNnSc2d1	české
hospodářství	hospodářství	k1gNnSc2	hospodářství
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgMnPc7d1	hlavní
průmyslovými	průmyslový	k2eAgMnPc7d1	průmyslový
centry	centr	k1gMnPc7	centr
jsou	být	k5eAaImIp3nP	být
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
Ostravsko	Ostravsko	k1gNnSc1	Ostravsko
<g/>
,	,	kIx,	,
Plzeň	Plzeň	k1gFnSc1	Plzeň
a	a	k8xC	a
Mladá	mladá	k1gFnSc1	mladá
Boleslav	Boleslav	k1gMnSc1	Boleslav
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
hlavní	hlavní	k2eAgNnSc4d1	hlavní
odvětví	odvětví	k1gNnSc4	odvětví
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
patří	patřit	k5eAaImIp3nS	patřit
průmysl	průmysl	k1gInSc4	průmysl
chemický	chemický	k2eAgInSc4d1	chemický
<g/>
,	,	kIx,	,
strojírenský	strojírenský	k2eAgInSc4d1	strojírenský
<g/>
,	,	kIx,	,
hutnický	hutnický	k2eAgInSc4d1	hutnický
a	a	k8xC	a
potravinářský	potravinářský	k2eAgInSc4d1	potravinářský
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnPc1d1	další
významná	významný	k2eAgNnPc1d1	významné
odvětví	odvětví	k1gNnPc1	odvětví
jsou	být	k5eAaImIp3nP	být
energetický	energetický	k2eAgInSc4d1	energetický
průmysl	průmysl	k1gInSc4	průmysl
<g/>
,	,	kIx,	,
stavebnictví	stavebnictví	k1gNnSc1	stavebnictví
a	a	k8xC	a
výroba	výroba	k1gFnSc1	výroba
spotřebního	spotřební	k2eAgNnSc2d1	spotřební
zboží	zboží	k1gNnSc2	zboží
<g/>
.	.	kIx.	.
</s>
<s>
Méně	málo	k6eAd2	málo
významná	významný	k2eAgNnPc1d1	významné
odvětví	odvětví	k1gNnPc1	odvětví
jsou	být	k5eAaImIp3nP	být
zbrojní	zbrojní	k2eAgInSc4d1	zbrojní
průmysl	průmysl	k1gInSc4	průmysl
a	a	k8xC	a
např.	např.	kA	např.
sklářství	sklářství	k1gNnSc1	sklářství
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
ovšem	ovšem	k9	ovšem
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
tradici	tradice	k1gFnSc4	tradice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velký	velký	k2eAgInSc1d1	velký
význam	význam	k1gInSc1	význam
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
pro	pro	k7c4	pro
export	export	k1gInSc4	export
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
automobilový	automobilový	k2eAgInSc4d1	automobilový
průmysl	průmysl	k1gInSc4	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgMnSc7d3	veliký
výrobcem	výrobce	k1gMnSc7	výrobce
automobilů	automobil	k1gInPc2	automobil
je	být	k5eAaImIp3nS	být
společnost	společnost	k1gFnSc1	společnost
Škoda	škoda	k6eAd1	škoda
Auto	auto	k1gNnSc1	auto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
vyrobily	vyrobit	k5eAaPmAgInP	vyrobit
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
společnosti	společnost	k1gFnSc6	společnost
Škoda	škoda	k1gFnSc1	škoda
(	(	kIx(	(
<g/>
patřící	patřící	k2eAgFnSc1d1	patřící
německému	německý	k2eAgNnSc3d1	německé
koncernu	koncern	k1gInSc2	koncern
Volkswagen	volkswagen	k1gInSc1	volkswagen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
TPCA	TPCA	kA	TPCA
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
sdružuje	sdružovat	k5eAaImIp3nS	sdružovat
japonskou	japonský	k2eAgFnSc4d1	japonská
firmu	firma	k1gFnSc4	firma
Toyota	toyota	k1gFnSc1	toyota
a	a	k8xC	a
francouzskou	francouzský	k2eAgFnSc7d1	francouzská
Peugeot-Citroën	Peugeot-Citroën	k1gInSc4	Peugeot-Citroën
<g/>
,	,	kIx,	,
a	a	k8xC	a
jihokorejská	jihokorejský	k2eAgFnSc1d1	jihokorejská
firma	firma	k1gFnSc1	firma
Hyundai	Hyunda	k1gFnSc2	Hyunda
dohromady	dohromady	k6eAd1	dohromady
rekordních	rekordní	k2eAgInPc2d1	rekordní
1	[number]	k4	1
298	[number]	k4	298
236	[number]	k4	236
osobních	osobní	k2eAgInPc2d1	osobní
automobilů	automobil	k1gInPc2	automobil
<g/>
.	.	kIx.	.
<g/>
Tradice	tradice	k1gFnSc1	tradice
českého	český	k2eAgInSc2d1	český
automobilového	automobilový	k2eAgInSc2d1	automobilový
průmyslu	průmysl	k1gInSc2	průmysl
sahá	sahat	k5eAaImIp3nS	sahat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1897	[number]	k4	1897
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Kopřivnici	Kopřivnice	k1gFnSc6	Kopřivnice
vyroben	vyrobit	k5eAaPmNgInS	vyrobit
první	první	k4xOgInSc1	první
český	český	k2eAgInSc1d1	český
automobil	automobil	k1gInSc1	automobil
se	s	k7c7	s
spalovacím	spalovací	k2eAgInSc7d1	spalovací
motorem	motor	k1gInSc7	motor
značky	značka	k1gFnSc2	značka
President	president	k1gMnSc1	president
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
mladoboleslavská	mladoboleslavský	k2eAgFnSc1d1	mladoboleslavská
továrna	továrna	k1gFnSc1	továrna
Laurin	Laurin	k1gInSc1	Laurin
&	&	k?	&
Klement	Klement	k1gMnSc1	Klement
začala	začít	k5eAaPmAgFnS	začít
vyrábět	vyrábět	k5eAaImF	vyrábět
motocykly	motocykl	k1gInPc4	motocykl
Slavia	Slavium	k1gNnSc2	Slavium
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
roku	rok	k1gInSc2	rok
1905	[number]	k4	1905
se	se	k3xPyFc4	se
i	i	k9	i
v	v	k7c6	v
Mladé	mladý	k2eAgFnSc6d1	mladá
Boleslavi	Boleslaev	k1gFnSc6	Boleslaev
přeorientovali	přeorientovat	k5eAaPmAgMnP	přeorientovat
na	na	k7c4	na
automobily	automobil	k1gInPc4	automobil
<g/>
.	.	kIx.	.
</s>
<s>
Továrna	továrna	k1gFnSc1	továrna
Laurin	Laurin	k1gInSc1	Laurin
&	&	k?	&
Klement	Klement	k1gMnSc1	Klement
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
základnou	základna	k1gFnSc7	základna
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
vyrostla	vyrůst	k5eAaPmAgFnS	vyrůst
automobilka	automobilka	k1gFnSc1	automobilka
Škoda	škoda	k1gFnSc1	škoda
<g/>
.	.	kIx.	.
</s>
<s>
Nejprodávanějšími	prodávaný	k2eAgFnPc7d3	nejprodávanější
škodovkami	škodovka	k1gFnPc7	škodovka
současnosti	současnost	k1gFnPc4	současnost
jsou	být	k5eAaImIp3nP	být
Škoda	škoda	k1gFnSc1	škoda
Octavia	octavia	k1gFnSc1	octavia
<g/>
,	,	kIx,	,
Škoda	škoda	k1gFnSc1	škoda
Fabia	fabia	k1gFnSc1	fabia
<g/>
,	,	kIx,	,
Škoda	škoda	k1gFnSc1	škoda
Superb	Superb	k1gMnSc1	Superb
a	a	k8xC	a
Škoda	Škoda	k1gMnSc1	Škoda
Yeti	yeti	k1gMnSc1	yeti
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
uspěly	uspět	k5eAaPmAgFnP	uspět
Škoda	škoda	k1gFnSc1	škoda
Felicia	felicia	k1gFnSc1	felicia
<g/>
,	,	kIx,	,
Škoda	Škoda	k1gMnSc1	Škoda
Roomster	Roomster	k1gMnSc1	Roomster
<g/>
,	,	kIx,	,
Škoda	Škoda	k1gMnSc1	Škoda
Favorit	favorit	k1gMnSc1	favorit
či	či	k8xC	či
řada	řada	k1gFnSc1	řada
Škoda	škoda	k1gFnSc1	škoda
742	[number]	k4	742
(	(	kIx(	(
<g/>
vč.	vč.	k?	vč.
modelu	model	k1gInSc2	model
Škoda	škoda	k1gFnSc1	škoda
120	[number]	k4	120
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
dalším	další	k2eAgFnPc3d1	další
automobilovým	automobilový	k2eAgFnPc3d1	automobilová
značkám	značka	k1gFnPc3	značka
patří	patřit	k5eAaImIp3nS	patřit
též	též	k9	též
Tatra	Tatra	k1gFnSc1	Tatra
(	(	kIx(	(
<g/>
těžké	těžký	k2eAgInPc1d1	těžký
nákladní	nákladní	k2eAgInPc1d1	nákladní
automobily	automobil	k1gInPc1	automobil
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgInSc1	třetí
nejstarší	starý	k2eAgMnSc1d3	nejstarší
výrobce	výrobce	k1gMnSc1	výrobce
automobilů	automobil	k1gInPc2	automobil
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Avia	Avia	k1gFnSc1	Avia
(	(	kIx(	(
<g/>
střední	střední	k2eAgInPc1d1	střední
nákladní	nákladní	k2eAgInPc1d1	nákladní
automobily	automobil	k1gInPc1	automobil
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
k	k	k7c3	k
historickým	historický	k2eAgFnPc3d1	historická
pak	pak	k6eAd1	pak
Praga	Praga	k1gFnSc1	Praga
<g/>
,	,	kIx,	,
Liaz	liaz	k1gInSc1	liaz
a	a	k8xC	a
Karosa	karosa	k1gFnSc1	karosa
<g/>
.	.	kIx.	.
</s>
<s>
Tatra	Tatra	k1gFnSc1	Tatra
se	se	k3xPyFc4	se
proslavila	proslavit	k5eAaPmAgFnS	proslavit
nákladním	nákladní	k2eAgInSc7d1	nákladní
vozem	vůz	k1gInSc7	vůz
Tatra	Tatra	k1gFnSc1	Tatra
815	[number]	k4	815
či	či	k8xC	či
armádní	armádní	k2eAgInSc4d1	armádní
Tatrou	Tatra	k1gFnSc7	Tatra
813	[number]	k4	813
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějšími	známý	k2eAgInPc7d3	nejznámější
osobními	osobní	k2eAgInPc7d1	osobní
automobily	automobil	k1gInPc7	automobil
z	z	k7c2	z
kopřivnické	kopřivnický	k2eAgFnSc2d1	kopřivnická
továrny	továrna	k1gFnSc2	továrna
byly	být	k5eAaImAgFnP	být
Tatra	Tatra	k1gFnSc1	Tatra
603	[number]	k4	603
a	a	k8xC	a
Tatra	Tatra	k1gFnSc1	Tatra
97	[number]	k4	97
<g/>
.	.	kIx.	.
</s>
<s>
Tatra	Tatra	k1gFnSc1	Tatra
77	[number]	k4	77
byla	být	k5eAaImAgFnS	být
prvním	první	k4xOgMnSc6	první
sériově	sériově	k6eAd1	sériově
vyráběným	vyráběný	k2eAgInSc7d1	vyráběný
vozem	vůz	k1gInSc7	vůz
s	s	k7c7	s
aerodynamickou	aerodynamický	k2eAgFnSc7d1	aerodynamická
karosérií	karosérie	k1gFnSc7	karosérie
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
voze	vůz	k1gInSc6	vůz
Tatra	Tatra	k1gFnSc1	Tatra
87	[number]	k4	87
podnikli	podniknout	k5eAaPmAgMnP	podniknout
cestovatelé	cestovatel	k1gMnPc1	cestovatel
Jiří	Jiří	k1gMnSc1	Jiří
Hanzelka	Hanzelka	k1gMnSc1	Hanzelka
a	a	k8xC	a
Miroslav	Miroslav	k1gMnSc1	Miroslav
Zikmund	Zikmund	k1gMnSc1	Zikmund
své	svůj	k3xOyFgFnSc2	svůj
slavné	slavný	k2eAgFnSc2d1	slavná
cesty	cesta	k1gFnSc2	cesta
do	do	k7c2	do
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Autobusy	autobus	k1gInPc1	autobus
dnes	dnes	k6eAd1	dnes
vyrábí	vyrábět	k5eAaImIp3nP	vyrábět
firmy	firma	k1gFnSc2	firma
Iveco	Iveco	k6eAd1	Iveco
Czech	Czech	k1gInSc4	Czech
Republic	Republice	k1gFnPc2	Republice
a	a	k8xC	a
SOR	SOR	kA	SOR
Libchavy	Libchava	k1gFnSc2	Libchava
<g/>
,	,	kIx,	,
nejznámějšími	známý	k2eAgInPc7d3	nejznámější
českými	český	k2eAgInPc7d1	český
autobusy	autobus	k1gInPc7	autobus
jsou	být	k5eAaImIp3nP	být
Škoda	škoda	k6eAd1	škoda
706	[number]	k4	706
RTO	RTO	kA	RTO
či	či	k8xC	či
Karosa	karosa	k1gFnSc1	karosa
ŠM	ŠM	kA	ŠM
11	[number]	k4	11
<g/>
.	.	kIx.	.
</s>
<s>
Trolejbusy	trolejbus	k1gInPc4	trolejbus
dříve	dříve	k6eAd2	dříve
vyráběla	vyrábět	k5eAaImAgFnS	vyrábět
Škoda	škoda	k1gFnSc1	škoda
Ostrov	ostrov	k1gInSc1	ostrov
(	(	kIx(	(
<g/>
např.	např.	kA	např.
typ	typ	k1gInSc1	typ
Škoda	škoda	k1gFnSc1	škoda
9	[number]	k4	9
<g/>
Tr	Tr	k1gFnPc2	Tr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
především	především	k9	především
Škoda	Škoda	k1gMnSc1	Škoda
Electric	Electric	k1gMnSc1	Electric
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgMnPc7d1	další
českými	český	k2eAgMnPc7d1	český
výrobci	výrobce	k1gMnPc7	výrobce
dopravních	dopravní	k2eAgInPc2d1	dopravní
a	a	k8xC	a
zemědělských	zemědělský	k2eAgInPc2d1	zemědělský
prostředků	prostředek	k1gInPc2	prostředek
jsou	být	k5eAaImIp3nP	být
Zetor	zetor	k1gInSc4	zetor
(	(	kIx(	(
<g/>
traktory	traktor	k1gInPc1	traktor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kaipan	Kaipan	k1gInSc1	Kaipan
(	(	kIx(	(
<g/>
roadstery	roadster	k1gInPc1	roadster
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jawa	jawa	k1gFnSc1	jawa
(	(	kIx(	(
<g/>
motocykly	motocykl	k1gInPc1	motocykl
<g/>
)	)	kIx)	)
a	a	k8xC	a
Čezeta	Čezeta	k1gFnSc1	Čezeta
(	(	kIx(	(
<g/>
elektrické	elektrický	k2eAgInPc1d1	elektrický
skútry	skútr	k1gInPc1	skútr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kolejová	kolejový	k2eAgNnPc4d1	kolejové
vozidla	vozidlo	k1gNnPc4	vozidlo
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
především	především	k9	především
Škoda	škoda	k1gFnSc1	škoda
Transportation	Transportation	k1gInSc1	Transportation
(	(	kIx(	(
<g/>
tramvaje	tramvaj	k1gFnPc1	tramvaj
<g/>
,	,	kIx,	,
trolejbusy	trolejbus	k1gInPc1	trolejbus
<g/>
,	,	kIx,	,
metro	metro	k1gNnSc1	metro
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Historicky	historicky	k6eAd1	historicky
je	být	k5eAaImIp3nS	být
však	však	k9	však
nejslavnějším	slavný	k2eAgNnSc7d3	nejslavnější
českým	český	k2eAgNnSc7d1	české
drážním	drážní	k2eAgNnSc7d1	drážní
vozidlem	vozidlo	k1gNnSc7	vozidlo
tramvaj	tramvaj	k1gFnSc1	tramvaj
Tatra	Tatra	k1gFnSc1	Tatra
T3	T3	k1gFnSc1	T3
z	z	k7c2	z
ČKD	ČKD	kA	ČKD
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
s	s	k7c7	s
14	[number]	k4	14
tisíci	tisíc	k4xCgInPc7	tisíc
vyrobenými	vyrobený	k2eAgInPc7d1	vyrobený
kusy	kus	k1gInPc7	kus
nejpočetnější	početní	k2eAgFnSc4d3	nejpočetnější
tramvaj	tramvaj	k1gFnSc4	tramvaj
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
železničními	železniční	k2eAgNnPc7d1	železniční
vozidly	vozidlo	k1gNnPc7	vozidlo
pak	pak	k9	pak
motorový	motorový	k2eAgInSc4d1	motorový
vůz	vůz	k1gInSc4	vůz
řady	řada	k1gFnSc2	řada
M	M	kA	M
290.0	[number]	k4	290.0
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
strela	strela	k1gFnSc1	strela
z	z	k7c2	z
Tatry	Tatra	k1gFnSc2	Tatra
Kopřivnice	Kopřivnice	k1gFnSc2	Kopřivnice
(	(	kIx(	(
<g/>
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výroba	výroba	k1gFnSc1	výroba
letounů	letoun	k1gInPc2	letoun
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c4	v
Aero	aero	k1gNnSc4	aero
Vodochody	Vodochod	k1gInPc4	Vodochod
(	(	kIx(	(
<g/>
armádní	armádní	k2eAgInSc4d1	armádní
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
Letu	let	k1gInSc6	let
Kunovice	Kunovice	k1gFnPc4	Kunovice
(	(	kIx(	(
<g/>
civilní	civilní	k2eAgFnSc4d1	civilní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
také	také	k9	také
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
Avii	Avia	k1gFnSc6	Avia
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejslavnějším	slavný	k2eAgMnPc3d3	nejslavnější
českým	český	k2eAgMnPc3d1	český
letounům	letoun	k1gMnPc3	letoun
patřily	patřit	k5eAaImAgFnP	patřit
Avia	Avia	k1gFnSc1	Avia
B-534	B-534	k1gFnSc1	B-534
a	a	k8xC	a
Avia	Avia	k1gFnSc1	Avia
S-199	S-199	k1gFnSc1	S-199
(	(	kIx(	(
<g/>
důležitá	důležitý	k2eAgFnSc1d1	důležitá
zbraň	zbraň	k1gFnSc1	zbraň
v	v	k7c6	v
izraelsko-arabské	izraelskorabský	k2eAgFnSc6d1	izraelsko-arabská
válce	válka	k1gFnSc6	válka
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bitevník	bitevník	k1gInSc1	bitevník
Aero	aero	k1gNnSc1	aero
L-159	L-159	k1gMnSc2	L-159
Alca	Alcus	k1gMnSc2	Alcus
zase	zase	k9	zase
sehrává	sehrávat	k5eAaImIp3nS	sehrávat
značnou	značný	k2eAgFnSc4d1	značná
roli	role	k1gFnSc4	role
v	v	k7c6	v
boji	boj	k1gInSc6	boj
irácké	irácký	k2eAgFnSc2d1	irácká
armády	armáda	k1gFnSc2	armáda
proti	proti	k7c3	proti
Islámskému	islámský	k2eAgInSc3d1	islámský
státu	stát	k1gInSc3	stát
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
cvičné	cvičný	k2eAgInPc1d1	cvičný
vojenské	vojenský	k2eAgInPc1d1	vojenský
letouny	letoun	k1gInPc1	letoun
se	se	k3xPyFc4	se
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
osvědčily	osvědčit	k5eAaPmAgInP	osvědčit
Aero	aero	k1gNnSc4	aero
L-29	L-29	k1gFnPc2	L-29
Delfín	Delfín	k1gMnSc1	Delfín
či	či	k8xC	či
Aero	aero	k1gNnSc4	aero
L-39	L-39	k1gMnSc1	L-39
Albatros	albatros	k1gMnSc1	albatros
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Kunovicích	Kunovice	k1gFnPc6	Kunovice
se	se	k3xPyFc4	se
specializovali	specializovat	k5eAaBmAgMnP	specializovat
na	na	k7c4	na
kluzáky	kluzák	k1gInPc4	kluzák
(	(	kIx(	(
<g/>
zejm.	zejm.	k?	zejm.
Let	let	k1gInSc1	let
L-13	L-13	k1gMnSc1	L-13
Blaník	Blaník	k1gInSc1	Blaník
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
jejich	jejich	k3xOp3gInSc4	jejich
Let	let	k1gInSc4	let
L-410	L-410	k1gFnSc1	L-410
Turbolet	Turbolet	k1gInSc1	Turbolet
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
nejpoužívanějších	používaný	k2eAgNnPc2d3	nejpoužívanější
českých	český	k2eAgNnPc2d1	české
letadel	letadlo	k1gNnPc2	letadlo
<g/>
,	,	kIx,	,
uplatnil	uplatnit	k5eAaPmAgMnS	uplatnit
se	se	k3xPyFc4	se
především	především	k9	především
v	v	k7c6	v
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
probíhala	probíhat	k5eAaImAgFnS	probíhat
výroba	výroba	k1gFnSc1	výroba
zemědělských	zemědělský	k2eAgInPc2d1	zemědělský
a	a	k8xC	a
akrobatických	akrobatický	k2eAgInPc2d1	akrobatický
letounů	letoun	k1gInPc2	letoun
také	také	k9	také
v	v	k7c6	v
Otrokovicích	Otrokovice	k1gFnPc6	Otrokovice
(	(	kIx(	(
<g/>
Z-	Z-	k1gFnPc2	Z-
<g/>
42	[number]	k4	42
<g/>
,	,	kIx,	,
Z-	Z-	k1gFnSc1	Z-
<g/>
50	[number]	k4	50
<g/>
,	,	kIx,	,
Z-37	Z-37	k1gFnSc1	Z-37
Čmelák	čmelák	k1gMnSc1	čmelák
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
republika	republika	k1gFnSc1	republika
hodně	hodně	k6eAd1	hodně
vsadila	vsadit	k5eAaPmAgFnS	vsadit
na	na	k7c4	na
zbrojní	zbrojní	k2eAgInSc4d1	zbrojní
průmysl	průmysl	k1gInSc4	průmysl
<g/>
,	,	kIx,	,
založeny	založen	k2eAgFnPc1d1	založena
byly	být	k5eAaImAgFnP	být
tehdy	tehdy	k6eAd1	tehdy
Česká	český	k2eAgFnSc1d1	Česká
zbrojovka	zbrojovka	k1gFnSc1	zbrojovka
Strakonice	Strakonice	k1gFnPc1	Strakonice
<g/>
,	,	kIx,	,
Zbrojovka	zbrojovka	k1gFnSc1	zbrojovka
Brno	Brno	k1gNnSc1	Brno
a	a	k8xC	a
Česká	český	k2eAgFnSc1d1	Česká
zbrojovka	zbrojovka	k1gFnSc1	zbrojovka
Uherský	uherský	k2eAgInSc1d1	uherský
Brod	Brod	k1gInSc1	Brod
(	(	kIx(	(
<g/>
nejznámějšími	známý	k2eAgFnPc7d3	nejznámější
českými	český	k2eAgFnPc7d1	Česká
zbraněmi	zbraň	k1gFnPc7	zbraň
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
jsou	být	k5eAaImIp3nP	být
tank	tank	k1gInSc4	tank
LT	LT	kA	LT
vz.	vz.	k?	vz.
38	[number]	k4	38
<g/>
,	,	kIx,	,
samopal	samopal	k1gInSc1	samopal
vzor	vzor	k1gInSc1	vzor
61	[number]	k4	61
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
škorpion	škorpion	k1gMnSc1	škorpion
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pistole	pistole	k1gFnSc1	pistole
CZ	CZ	kA	CZ
75	[number]	k4	75
<g/>
,	,	kIx,	,
lehký	lehký	k2eAgInSc1d1	lehký
kulomet	kulomet	k1gInSc1	kulomet
vz.	vz.	k?	vz.
26	[number]	k4	26
<g/>
,	,	kIx,	,
samopal	samopal	k1gInSc1	samopal
vzor	vzor	k1gInSc1	vzor
58	[number]	k4	58
<g/>
,	,	kIx,	,
SKOT	skot	k1gInSc1	skot
(	(	kIx(	(
<g/>
OT-	OT-	k1gFnSc1	OT-
<g/>
64	[number]	k4	64
<g/>
)	)	kIx)	)
či	či	k8xC	či
pistole	pistole	k1gFnSc1	pistole
vz.	vz.	k?	vz.
52	[number]	k4	52
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dříve	dříve	k6eAd2	dříve
silný	silný	k2eAgInSc1d1	silný
textilní	textilní	k2eAgInSc1d1	textilní
a	a	k8xC	a
obuvnický	obuvnický	k2eAgInSc1d1	obuvnický
průmysl	průmysl	k1gInSc1	průmysl
(	(	kIx(	(
<g/>
Baťa	Baťa	k1gMnSc1	Baťa
ad	ad	k7c4	ad
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
již	již	k9	již
v	v	k7c4	v
ČR	ČR	kA	ČR
významné	významný	k2eAgNnSc1d1	významné
zastoupení	zastoupení	k1gNnSc1	zastoupení
nemá	mít	k5eNaImIp3nS	mít
<g/>
.	.	kIx.	.
</s>
<s>
Prodej	prodej	k1gInSc1	prodej
českých	český	k2eAgFnPc2d1	Česká
hodinek	hodinka	k1gFnPc2	hodinka
PRIM	prima	k1gFnPc2	prima
také	také	k9	také
výrazně	výrazně	k6eAd1	výrazně
klesl	klesnout	k5eAaPmAgInS	klesnout
<g/>
,	,	kIx,	,
z	z	k7c2	z
více	hodně	k6eAd2	hodně
než	než	k8xS	než
půl	půl	k1xP	půl
milionu	milion	k4xCgInSc2	milion
prodávaných	prodávaný	k2eAgMnPc2d1	prodávaný
kusů	kus	k1gInPc2	kus
ročně	ročně	k6eAd1	ročně
na	na	k7c6	na
konci	konec	k1gInSc6	konec
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tradičním	tradiční	k2eAgNnSc7d1	tradiční
odvětvím	odvětví	k1gNnSc7	odvětví
je	být	k5eAaImIp3nS	být
pivovarnictví	pivovarnictví	k1gNnSc1	pivovarnictví
<g/>
.	.	kIx.	.
</s>
<s>
Pivo	pivo	k1gNnSc1	pivo
se	se	k3xPyFc4	se
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
pilo	pít	k5eAaImAgNnS	pít
a	a	k8xC	a
vyrábělo	vyrábět	k5eAaImAgNnS	vyrábět
minimálně	minimálně	k6eAd1	minimálně
od	od	k7c2	od
roku	rok	k1gInSc2	rok
993	[number]	k4	993
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
máme	mít	k5eAaImIp1nP	mít
dochovánu	dochován	k2eAgFnSc4d1	dochována
zprávu	zpráva	k1gFnSc4	zpráva
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
biskup	biskup	k1gMnSc1	biskup
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
zakázal	zakázat	k5eAaPmAgInS	zakázat
mnichům	mnich	k1gMnPc3	mnich
Břevnovského	břevnovský	k2eAgInSc2d1	břevnovský
kláštera	klášter	k1gInSc2	klášter
pivo	pivo	k1gNnSc4	pivo
vyrábět	vyrábět	k5eAaImF	vyrábět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
roku	rok	k1gInSc2	rok
1842	[number]	k4	1842
druh	druh	k1gInSc1	druh
piva	pivo	k1gNnSc2	pivo
(	(	kIx(	(
<g/>
pils	pils	k1gInSc1	pils
<g/>
,	,	kIx,	,
pilsner	pilsner	k1gInSc1	pilsner
<g/>
,	,	kIx,	,
pivo	pivo	k1gNnSc1	pivo
plzeňského	plzeňský	k2eAgInSc2d1	plzeňský
typu	typ	k1gInSc2	typ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
nejrozšířenějším	rozšířený	k2eAgInSc7d3	nejrozšířenější
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
produkci	produkce	k1gFnSc6	produkce
piva	pivo	k1gNnSc2	pivo
je	být	k5eAaImIp3nS	být
ČR	ČR	kA	ČR
na	na	k7c4	na
15	[number]	k4	15
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
je	být	k5eAaImIp3nS	být
40	[number]	k4	40
velkých	velký	k2eAgInPc2d1	velký
průmyslových	průmyslový	k2eAgInPc2d1	průmyslový
pivovarů	pivovar	k1gInPc2	pivovar
a	a	k8xC	a
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
mnoho	mnoho	k6eAd1	mnoho
pivovarů	pivovar	k1gInPc2	pivovar
malých	malý	k2eAgInPc2d1	malý
<g/>
.	.	kIx.	.
</s>
<s>
Největšími	veliký	k2eAgInPc7d3	veliký
pivovary	pivovar	k1gInPc7	pivovar
jsou	být	k5eAaImIp3nP	být
Plzeňský	plzeňský	k2eAgInSc1d1	plzeňský
Prazdroj	prazdroj	k1gInSc1	prazdroj
(	(	kIx(	(
<g/>
značky	značka	k1gFnSc2	značka
Pilsner	Pilsner	k1gMnSc1	Pilsner
Urquell	Urquell	k1gMnSc1	Urquell
<g/>
,	,	kIx,	,
Gambrinus	Gambrinus	k1gMnSc1	Gambrinus
<g/>
,	,	kIx,	,
Velkopopovický	velkopopovický	k2eAgMnSc1d1	velkopopovický
Kozel	Kozel	k1gMnSc1	Kozel
<g/>
,	,	kIx,	,
Radegast	Radegast	k1gMnSc1	Radegast
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Pivovary	pivovar	k1gInPc1	pivovar
Staropramen	staropramen	k1gInSc1	staropramen
(	(	kIx(	(
<g/>
Staropramen	staropramen	k1gInSc1	staropramen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Heineken	Heineken	k1gInSc4	Heineken
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
Starobrno	Starobrno	k1gNnSc1	Starobrno
<g/>
,	,	kIx,	,
Krušovice	Krušovice	k1gFnSc1	Krušovice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Budějovický	budějovický	k2eAgInSc1d1	budějovický
Budvar	budvar	k1gInSc1	budvar
(	(	kIx(	(
<g/>
Budvar	budvar	k1gInSc1	budvar
<g/>
)	)	kIx)	)
a	a	k8xC	a
PMS	PMS	kA	PMS
Přerov	Přerov	k1gInSc1	Přerov
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
malým	malý	k2eAgInPc3d1	malý
pivovarům	pivovar	k1gInPc3	pivovar
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
Bernard	Bernard	k1gMnSc1	Bernard
v	v	k7c6	v
Humpolci	Humpolec	k1gInSc6	Humpolec
<g/>
.	.	kIx.	.
<g/>
Významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
hrála	hrát	k5eAaImAgFnS	hrát
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
výroba	výroba	k1gFnSc1	výroba
skla	sklo	k1gNnSc2	sklo
a	a	k8xC	a
bižuterie	bižuterie	k1gFnSc2	bižuterie
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
České	český	k2eAgNnSc4d1	české
sklo	sklo	k1gNnSc4	sklo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Těžba	těžba	k1gFnSc1	těžba
===	===	k?	===
</s>
</p>
<p>
<s>
Ke	k	k7c3	k
klíčovým	klíčový	k2eAgFnPc3d1	klíčová
nerostným	nerostný	k2eAgFnPc3d1	nerostná
surovinám	surovina	k1gFnPc3	surovina
těženým	těžený	k2eAgFnPc3d1	těžená
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
patří	patřit	k5eAaImIp3nS	patřit
černé	černý	k2eAgNnSc4d1	černé
a	a	k8xC	a
hnědé	hnědý	k2eAgNnSc4d1	hnědé
uhlí	uhlí	k1gNnSc4	uhlí
<g/>
,	,	kIx,	,
dál	daleko	k6eAd2	daleko
kaolín	kaolín	k1gInSc1	kaolín
<g/>
,	,	kIx,	,
jíl	jíl	k1gInSc1	jíl
<g/>
,	,	kIx,	,
grafit	grafit	k1gInSc1	grafit
<g/>
,	,	kIx,	,
vápenec	vápenec	k1gInSc1	vápenec
nebo	nebo	k8xC	nebo
jiné	jiný	k2eAgFnPc1d1	jiná
stavební	stavební	k2eAgFnPc1d1	stavební
hmoty	hmota	k1gFnPc1	hmota
<g/>
.	.	kIx.	.
</s>
<s>
Naleziště	naleziště	k1gNnSc1	naleziště
uranu	uran	k1gInSc2	uran
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
u	u	k7c2	u
obce	obec	k1gFnSc2	obec
Dolní	dolní	k2eAgFnSc1d1	dolní
Rožínka	Rožínka	k1gFnSc1	Rožínka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Krušných	krušný	k2eAgFnPc2d1	krušná
hor	hora	k1gFnPc2	hora
objeveny	objevit	k5eAaPmNgFnP	objevit
velké	velká	k1gFnPc1	velká
zásoby	zásoba	k1gFnSc2	zásoba
lithia	lithium	k1gNnSc2	lithium
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
malém	malý	k2eAgInSc6d1	malý
rozsahu	rozsah	k1gInSc6	rozsah
se	se	k3xPyFc4	se
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Moravy	Morava	k1gFnSc2	Morava
těží	těžet	k5eAaImIp3nS	těžet
ropa	ropa	k1gFnSc1	ropa
a	a	k8xC	a
zemní	zemní	k2eAgInSc1d1	zemní
plyn	plyn	k1gInSc1	plyn
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
větší	veliký	k2eAgInSc1d2	veliký
objem	objem	k1gInSc1	objem
těchto	tento	k3xDgFnPc2	tento
surovin	surovina	k1gFnPc2	surovina
je	být	k5eAaImIp3nS	být
dovážen	dovážet	k5eAaImNgInS	dovážet
z	z	k7c2	z
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
třetinu	třetina	k1gFnSc4	třetina
země	zem	k1gFnSc2	zem
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
lesy	les	k1gInPc1	les
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
dřevo	dřevo	k1gNnSc4	dřevo
též	též	k9	též
k	k	k7c3	k
vývozním	vývozní	k2eAgInPc3d1	vývozní
artiklům	artikl	k1gInPc3	artikl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Telekomunikace	telekomunikace	k1gFnSc1	telekomunikace
a	a	k8xC	a
IT	IT	kA	IT
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
se	se	k3xPyFc4	se
prudce	prudko	k6eAd1	prudko
rozvíjejí	rozvíjet	k5eAaImIp3nP	rozvíjet
telekomunikace	telekomunikace	k1gFnPc4	telekomunikace
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc4d3	veliký
telefonní	telefonní	k2eAgInSc4d1	telefonní
operátor	operátor	k1gInSc4	operátor
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
Český	český	k2eAgInSc1d1	český
Telecom	Telecom	k1gInSc1	Telecom
se	se	k3xPyFc4	se
spojil	spojit	k5eAaPmAgInS	spojit
s	s	k7c7	s
globální	globální	k2eAgFnSc7d1	globální
společností	společnost	k1gFnSc7	společnost
Telefónica	Telefónicum	k1gNnSc2	Telefónicum
a	a	k8xC	a
přejmenoval	přejmenovat	k5eAaPmAgMnS	přejmenovat
se	se	k3xPyFc4	se
na	na	k7c6	na
O2	O2	k1gFnSc6	O2
Czech	Czecha	k1gFnPc2	Czecha
Republic	Republice	k1gFnPc2	Republice
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
společnost	společnost	k1gFnSc1	společnost
je	být	k5eAaImIp3nS	být
také	také	k9	také
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
společnostmi	společnost	k1gFnPc7	společnost
T-Mobile	T-Mobil	k1gMnSc5	T-Mobil
<g/>
,	,	kIx,	,
Vodafone	Vodafon	k1gInSc5	Vodafon
a	a	k8xC	a
U	u	k7c2	u
<g/>
:	:	kIx,	:
<g/>
fon	fon	k?	fon
<g/>
,	,	kIx,	,
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgMnPc2d3	veliký
mobilních	mobilní	k2eAgMnPc2d1	mobilní
operátorů	operátor	k1gMnPc2	operátor
a	a	k8xC	a
poskytovatelů	poskytovatel	k1gMnPc2	poskytovatel
internetu	internet	k1gInSc2	internet
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Česko	Česko	k1gNnSc1	Česko
má	mít	k5eAaImIp3nS	mít
nejvíce	nejvíce	k6eAd1	nejvíce
předplatitelů	předplatitel	k1gMnPc2	předplatitel
bezdrátového	bezdrátový	k2eAgNnSc2d1	bezdrátové
připojení	připojení	k1gNnSc2	připojení
v	v	k7c6	v
Evropské	evropský	k2eAgFnSc6d1	Evropská
unii	unie	k1gFnSc6	unie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
ČR	ČR	kA	ČR
1,89	[number]	k4	1,89
milionu	milion	k4xCgInSc3	milion
pevných	pevný	k2eAgFnPc2d1	pevná
telefonních	telefonní	k2eAgFnPc2d1	telefonní
linek	linka	k1gFnPc2	linka
<g/>
,	,	kIx,	,
14	[number]	k4	14
milionů	milion	k4xCgInPc2	milion
mobilních	mobilní	k2eAgInPc2d1	mobilní
telefonů	telefon	k1gInPc2	telefon
a	a	k8xC	a
přes	přes	k7c4	přes
4	[number]	k4	4
miliony	milion	k4xCgInPc7	milion
internetových	internetový	k2eAgNnPc2d1	internetové
připojení	připojení	k1gNnSc2	připojení
<g/>
.	.	kIx.	.
</s>
<s>
Internet	Internet	k1gInSc4	Internet
užívalo	užívat	k5eAaImAgNnS	užívat
8,2	[number]	k4	8,2
milionu	milion	k4xCgInSc2	milion
Čechů	Čech	k1gMnPc2	Čech
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
77,5	[number]	k4	77,5
procent	procento	k1gNnPc2	procento
obyvatel	obyvatel	k1gMnPc2	obyvatel
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgFnSc1d1	národní
internetová	internetový	k2eAgFnSc1d1	internetová
doména	doména	k1gFnSc1	doména
nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
řádu	řád	k1gInSc2	řád
pro	pro	k7c4	pro
Českou	český	k2eAgFnSc4d1	Česká
republiku	republika	k1gFnSc4	republika
je	být	k5eAaImIp3nS	být
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
bylo	být	k5eAaImAgNnS	být
zaregistrováno	zaregistrovat	k5eAaPmNgNnS	zaregistrovat
přes	přes	k7c4	přes
1	[number]	k4	1
280	[number]	k4	280
000	[number]	k4	000
adres	adresa	k1gFnPc2	adresa
s	s	k7c7	s
doménou	doména	k1gFnSc7	doména
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
.	.	kIx.	.
<g/>
Dvě	dva	k4xCgFnPc1	dva
antivirové	antivirový	k2eAgFnPc1d1	antivirová
obří	obří	k2eAgFnPc1d1	obří
společnosti	společnost	k1gFnPc1	společnost
Avast	Avast	k1gFnSc1	Avast
a	a	k8xC	a
AVG	AVG	kA	AVG
byly	být	k5eAaImAgFnP	být
založeny	založit	k5eAaPmNgFnP	založit
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2016	[number]	k4	2016
bylo	být	k5eAaImAgNnS	být
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Avast	Avast	k1gInSc1	Avast
získal	získat	k5eAaPmAgInS	získat
konkurenční	konkurenční	k2eAgInSc4d1	konkurenční
AVG	AVG	kA	AVG
za	za	k7c4	za
1,3	[number]	k4	1,3
miliard	miliarda	k4xCgFnPc2	miliarda
$	$	kIx~	$
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
mají	mít	k5eAaImIp3nP	mít
tyto	tento	k3xDgFnPc4	tento
společnosti	společnost	k1gFnPc4	společnost
uživatelskou	uživatelský	k2eAgFnSc4d1	Uživatelská
základnu	základna	k1gFnSc4	základna
okolo	okolo	k7c2	okolo
400	[number]	k4	400
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
40	[number]	k4	40
<g/>
%	%	kIx~	%
podíl	podíl	k1gInSc4	podíl
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
mimo	mimo	k7c4	mimo
Čínu	Čína	k1gFnSc4	Čína
<g/>
.	.	kIx.	.
<g/>
Praha	Praha	k1gFnSc1	Praha
je	být	k5eAaImIp3nS	být
sídlem	sídlo	k1gNnSc7	sídlo
evropské	evropský	k2eAgFnSc2d1	Evropská
agentury	agentura	k1gFnSc2	agentura
GSA	GSA	kA	GSA
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
řídí	řídit	k5eAaImIp3nS	řídit
navigační	navigační	k2eAgInSc4d1	navigační
systém	systém	k1gInSc4	systém
Galileo	Galilea	k1gFnSc5	Galilea
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
agentura	agentura	k1gFnSc1	agentura
EU	EU	kA	EU
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
bývalé	bývalý	k2eAgFnSc2d1	bývalá
Státní	státní	k2eAgFnSc2d1	státní
plánovací	plánovací	k2eAgFnSc2d1	plánovací
komise	komise	k1gFnSc2	komise
v	v	k7c6	v
Holešovicích	Holešovice	k1gFnPc6	Holešovice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Doprava	doprava	k1gFnSc1	doprava
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Silniční	silniční	k2eAgFnSc4d1	silniční
síť	síť	k1gFnSc4	síť
====	====	k?	====
</s>
</p>
<p>
<s>
Délka	délka	k1gFnSc1	délka
silniční	silniční	k2eAgFnSc2d1	silniční
dopravní	dopravní	k2eAgFnSc2d1	dopravní
sítě	síť	k1gFnSc2	síť
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
činila	činit	k5eAaImAgFnS	činit
55	[number]	k4	55
737,5	[number]	k4	737,5
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
776	[number]	k4	776
km	km	kA	km
tvořily	tvořit	k5eAaImAgInP	tvořit
dálnice	dálnice	k1gFnPc4	dálnice
a	a	k8xC	a
6	[number]	k4	6
244,9	[number]	k4	244,9
km	km	kA	km
silnice	silnice	k1gFnSc2	silnice
I.	I.	kA	I.
třídy	třída	k1gFnSc2	třída
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
459,4	[number]	k4	459,4
km	km	kA	km
rychlostních	rychlostní	k2eAgFnPc2d1	rychlostní
komunikací	komunikace	k1gFnPc2	komunikace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Dálniční	dálniční	k2eAgFnSc1d1	dálniční
síť	síť	k1gFnSc1	síť
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
ve	v	k7c6	v
výstavbě	výstavba	k1gFnSc6	výstavba
<g/>
.	.	kIx.	.
</s>
<s>
Páteřní	páteřní	k2eAgFnSc7d1	páteřní
komunikací	komunikace	k1gFnSc7	komunikace
bude	být	k5eAaImBp3nS	být
po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
dokončení	dokončení	k1gNnSc6	dokončení
dálnice	dálnice	k1gFnSc2	dálnice
D	D	kA	D
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
spojí	spojit	k5eAaPmIp3nS	spojit
Prahu	Praha	k1gFnSc4	Praha
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc4	Brno
a	a	k8xC	a
Ostravu	Ostrava	k1gFnSc4	Ostrava
s	s	k7c7	s
Polskem	Polsko	k1gNnSc7	Polsko
(	(	kIx(	(
<g/>
směr	směr	k1gInSc4	směr
Katovice	Katovice	k1gFnPc4	Katovice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
dokončenými	dokončený	k2eAgFnPc7d1	dokončená
komunikacemi	komunikace	k1gFnPc7	komunikace
jsou	být	k5eAaImIp3nP	být
dálnice	dálnice	k1gFnPc1	dálnice
D	D	kA	D
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
spojující	spojující	k2eAgNnSc4d1	spojující
Brno	Brno	k1gNnSc4	Brno
a	a	k8xC	a
Bratislavu	Bratislava	k1gFnSc4	Bratislava
<g/>
,	,	kIx,	,
dálnice	dálnice	k1gFnSc1	dálnice
D5	D5	k1gFnSc1	D5
spojující	spojující	k2eAgFnSc4d1	spojující
Prahu	Praha	k1gFnSc4	Praha
<g/>
,	,	kIx,	,
Plzeň	Plzeň	k1gFnSc1	Plzeň
a	a	k8xC	a
Německo	Německo	k1gNnSc1	Německo
(	(	kIx(	(
<g/>
směr	směr	k1gInSc1	směr
Norimberk	Norimberk	k1gInSc1	Norimberk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dálnice	dálnice	k1gFnSc1	dálnice
D8	D8	k1gFnSc1	D8
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
přes	přes	k7c4	přes
Ústí	ústí	k1gNnSc4	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
do	do	k7c2	do
Německa	Německo	k1gNnSc2	Německo
(	(	kIx(	(
<g/>
směr	směr	k1gInSc1	směr
Drážďany	Drážďany	k1gInPc1	Drážďany
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dálnice	dálnice	k1gFnSc1	dálnice
D10	D10	k1gFnSc1	D10
(	(	kIx(	(
<g/>
Praha	Praha	k1gFnSc1	Praha
<g/>
–	–	k?	–
<g/>
Turnov	Turnov	k1gInSc1	Turnov
<g/>
)	)	kIx)	)
a	a	k8xC	a
dálnice	dálnice	k1gFnSc1	dálnice
D46	D46	k1gFnSc1	D46
(	(	kIx(	(
<g/>
Vyškov	Vyškov	k1gInSc1	Vyškov
<g/>
–	–	k?	–
<g/>
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
různě	různě	k6eAd1	různě
pokročilém	pokročilý	k2eAgInSc6d1	pokročilý
stavu	stav	k1gInSc6	stav
výstavby	výstavba	k1gFnSc2	výstavba
jsou	být	k5eAaImIp3nP	být
dále	daleko	k6eAd2	daleko
dálnice	dálnice	k1gFnPc4	dálnice
D	D	kA	D
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
dálnice	dálnice	k1gFnSc1	dálnice
D	D	kA	D
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
dálnice	dálnice	k1gFnSc1	dálnice
D	D	kA	D
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
dálnice	dálnice	k1gFnSc1	dálnice
D	D	kA	D
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
dálnice	dálnice	k1gFnSc1	dálnice
D	D	kA	D
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
dálnice	dálnice	k1gFnSc1	dálnice
D	D	kA	D
<g/>
11	[number]	k4	11
<g/>
,	,	kIx,	,
dálnice	dálnice	k1gFnSc1	dálnice
D	D	kA	D
<g/>
35	[number]	k4	35
<g/>
,	,	kIx,	,
dálnice	dálnice	k1gFnSc1	dálnice
D	D	kA	D
<g/>
43	[number]	k4	43
<g/>
,	,	kIx,	,
dálnice	dálnice	k1gFnSc1	dálnice
D	D	kA	D
<g/>
48	[number]	k4	48
<g/>
,	,	kIx,	,
dálnice	dálnice	k1gFnSc1	dálnice
D	D	kA	D
<g/>
49	[number]	k4	49
<g/>
,	,	kIx,	,
dálnice	dálnice	k1gFnSc1	dálnice
D	D	kA	D
<g/>
52	[number]	k4	52
<g/>
,	,	kIx,	,
dálnice	dálnice	k1gFnSc1	dálnice
D55	D55	k1gFnSc1	D55
a	a	k8xC	a
dálnice	dálnice	k1gFnSc1	dálnice
D	D	kA	D
<g/>
56	[number]	k4	56
<g/>
.	.	kIx.	.
</s>
<s>
Maximální	maximální	k2eAgFnSc1d1	maximální
povolená	povolený	k2eAgFnSc1d1	povolená
rychlost	rychlost	k1gFnSc1	rychlost
na	na	k7c6	na
dálnici	dálnice	k1gFnSc6	dálnice
činí	činit	k5eAaImIp3nS	činit
130	[number]	k4	130
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
Za	za	k7c4	za
užívání	užívání	k1gNnSc4	užívání
dálnic	dálnice	k1gFnPc2	dálnice
platí	platit	k5eAaImIp3nP	platit
motoristé	motorista	k1gMnPc1	motorista
dálniční	dálniční	k2eAgMnPc1d1	dálniční
poplatek	poplatek	k1gInSc4	poplatek
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
existuje	existovat	k5eAaImIp3nS	existovat
systém	systém	k1gInSc1	systém
elektronického	elektronický	k2eAgNnSc2d1	elektronické
mýtného	mýtné	k1gNnSc2	mýtné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Úmrtnost	úmrtnost	k1gFnSc1	úmrtnost
na	na	k7c6	na
českých	český	k2eAgFnPc6d1	Česká
silnicích	silnice	k1gFnPc6	silnice
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
přepočtu	přepočet	k1gInSc6	přepočet
na	na	k7c4	na
obyvatele	obyvatel	k1gMnPc4	obyvatel
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
přibližně	přibližně	k6eAd1	přibližně
1,6	[number]	k4	1,6
<g/>
×	×	k?	×
vyšší	vysoký	k2eAgMnSc1d2	vyšší
než	než	k8xS	než
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
intenzita	intenzita	k1gFnSc1	intenzita
dopravy	doprava	k1gFnSc2	doprava
je	být	k5eAaImIp3nS	být
nižší	nízký	k2eAgFnSc1d2	nižší
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
1286	[number]	k4	1286
usmrcených	usmrcený	k2eAgFnPc2d1	usmrcená
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
se	se	k3xPyFc4	se
kolísavě	kolísavě	k6eAd1	kolísavě
snižuje	snižovat	k5eAaImIp3nS	snižovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
na	na	k7c6	na
silnicích	silnice	k1gFnPc6	silnice
802	[number]	k4	802
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Každé	každý	k3xTgNnSc1	každý
registrované	registrovaný	k2eAgNnSc1d1	registrované
vozidlo	vozidlo	k1gNnSc1	vozidlo
v	v	k7c6	v
ČR	ČR	kA	ČR
musí	muset	k5eAaImIp3nS	muset
mít	mít	k5eAaImF	mít
státní	státní	k2eAgFnSc4d1	státní
poznávací	poznávací	k2eAgFnSc4d1	poznávací
značku	značka	k1gFnSc4	značka
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
současná	současný	k2eAgFnSc1d1	současná
podoba	podoba	k1gFnSc1	podoba
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Železnice	železnice	k1gFnSc1	železnice
====	====	k?	====
</s>
</p>
<p>
<s>
Česko	Česko	k1gNnSc1	Česko
má	mít	k5eAaImIp3nS	mít
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
9	[number]	k4	9
568	[number]	k4	568
km	km	kA	km
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejhustších	hustý	k2eAgFnPc2d3	nejhustší
železničních	železniční	k2eAgFnPc2d1	železniční
sítí	síť	k1gFnPc2	síť
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
3	[number]	k4	3
212	[number]	k4	212
km	km	kA	km
tratí	trať	k1gFnPc2	trať
elektrizováno	elektrizovat	k5eAaBmNgNnS	elektrizovat
a	a	k8xC	a
1	[number]	k4	1
906	[number]	k4	906
km	km	kA	km
je	být	k5eAaImIp3nS	být
dvou-	dvou-	k?	dvou-
a	a	k8xC	a
vícekolejných	vícekolejný	k2eAgInPc2d1	vícekolejný
<g/>
.	.	kIx.	.
</s>
<s>
Správcem	správce	k1gMnSc7	správce
a	a	k8xC	a
provozovatelem	provozovatel	k1gMnSc7	provozovatel
naprosté	naprostý	k2eAgFnSc2d1	naprostá
většiny	většina	k1gFnSc2	většina
železniční	železniční	k2eAgFnSc2d1	železniční
infrastruktury	infrastruktura	k1gFnSc2	infrastruktura
je	být	k5eAaImIp3nS	být
státní	státní	k2eAgFnSc1d1	státní
organizace	organizace	k1gFnSc1	organizace
Správa	správa	k1gFnSc1	správa
železniční	železniční	k2eAgFnSc2d1	železniční
dopravní	dopravní	k2eAgFnSc2d1	dopravní
cesty	cesta	k1gFnSc2	cesta
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgFnPc1d1	Česká
dráhy	dráha	k1gFnPc1	dráha
provozují	provozovat	k5eAaImIp3nP	provozovat
vysokorychlostní	vysokorychlostní	k2eAgInPc4d1	vysokorychlostní
vlaky	vlak	k1gInPc4	vlak
Pendolino	Pendolin	k2eAgNnSc1d1	Pendolino
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jelikož	jelikož	k8xS	jelikož
žádné	žádný	k3yNgFnPc1	žádný
tratě	trať	k1gFnPc1	trať
nejsou	být	k5eNaImIp3nP	být
vysokorychlostní	vysokorychlostní	k2eAgFnPc1d1	vysokorychlostní
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gFnSc1	jejich
maximální	maximální	k2eAgFnSc1d1	maximální
rychlost	rychlost	k1gFnSc1	rychlost
omezena	omezit	k5eAaPmNgFnS	omezit
na	na	k7c4	na
160	[number]	k4	160
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
</s>
</p>
<p>
<s>
Přepravní	přepravní	k2eAgInSc4d1	přepravní
výkon	výkon	k1gInSc4	výkon
necelých	celý	k2eNgInPc2d1	necelý
165	[number]	k4	165
milionů	milion	k4xCgInPc2	milion
cestujících	cestující	k1gMnPc2	cestující
při	při	k7c6	při
6	[number]	k4	6
590	[number]	k4	590
milionech	milion	k4xCgInPc6	milion
osobokilometrů	osobokilometr	k1gInPc2	osobokilometr
i	i	k9	i
13	[number]	k4	13
770	[number]	k4	770
milionů	milion	k4xCgInPc2	milion
tunokilometrů	tunokilometr	k1gInPc2	tunokilometr
v	v	k7c6	v
nákladní	nákladní	k2eAgFnSc6d1	nákladní
dopravě	doprava	k1gFnSc6	doprava
znamená	znamenat	k5eAaImIp3nS	znamenat
za	za	k7c4	za
poslední	poslední	k2eAgNnPc4d1	poslední
desetiletí	desetiletí	k1gNnPc4	desetiletí
značný	značný	k2eAgInSc4d1	značný
pokles	pokles	k1gInSc4	pokles
<g/>
.	.	kIx.	.
<g/>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
je	být	k5eAaImIp3nS	být
Česko	Česko	k1gNnSc1	Česko
zapojeno	zapojit	k5eAaPmNgNnS	zapojit
do	do	k7c2	do
sítě	síť	k1gFnSc2	síť
mezinárodních	mezinárodní	k2eAgInPc2d1	mezinárodní
vlaků	vlak	k1gInPc2	vlak
EuroCity	EuroCita	k1gFnSc2	EuroCita
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
některá	některý	k3yIgNnPc4	některý
z	z	k7c2	z
mezinárodních	mezinárodní	k2eAgNnPc2d1	mezinárodní
spojení	spojení	k1gNnPc2	spojení
i	i	k8xC	i
vlaky	vlak	k1gInPc1	vlak
SuperCity	SuperCit	k2eAgInPc1d1	SuperCit
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc1	jejichž
těžiště	těžiště	k1gNnSc1	těžiště
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
lince	linka	k1gFnSc6	linka
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Největším	veliký	k2eAgMnSc7d3	veliký
železničním	železniční	k2eAgMnSc7d1	železniční
dopravcem	dopravce	k1gMnSc7	dopravce
jsou	být	k5eAaImIp3nP	být
České	český	k2eAgFnSc2d1	Česká
dráhy	dráha	k1gFnSc2	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Nákladní	nákladní	k2eAgInSc1d1	nákladní
železniční	železniční	k2eAgInSc1d1	železniční
trh	trh	k1gInSc1	trh
je	být	k5eAaImIp3nS	být
liberalizován	liberalizován	k2eAgMnSc1d1	liberalizován
<g/>
,	,	kIx,	,
liberalizace	liberalizace	k1gFnSc1	liberalizace
v	v	k7c6	v
osobní	osobní	k2eAgFnSc6d1	osobní
dopravě	doprava	k1gFnSc6	doprava
probíhá	probíhat	k5eAaImIp3nS	probíhat
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
osobní	osobní	k2eAgFnSc2d1	osobní
železniční	železniční	k2eAgFnSc2d1	železniční
dopravy	doprava	k1gFnSc2	doprava
je	být	k5eAaImIp3nS	být
podporována	podporovat	k5eAaImNgFnS	podporovat
z	z	k7c2	z
veřejných	veřejný	k2eAgInPc2d1	veřejný
rozpočtů	rozpočet	k1gInPc2	rozpočet
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
dlouhodobých	dlouhodobý	k2eAgFnPc2d1	dlouhodobá
smluv	smlouva	k1gFnPc2	smlouva
s	s	k7c7	s
dopravci	dopravce	k1gMnPc7	dopravce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
železničním	železniční	k2eAgInSc7d1	železniční
uzlem	uzel	k1gInSc7	uzel
je	být	k5eAaImIp3nS	být
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
nádraží	nádraží	k1gNnSc1	nádraží
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
stará	starý	k2eAgFnSc1d1	stará
budova	budova	k1gFnSc1	budova
je	být	k5eAaImIp3nS	být
památkově	památkově	k6eAd1	památkově
chráněnou	chráněný	k2eAgFnSc7d1	chráněná
secesní	secesní	k2eAgFnSc7d1	secesní
stavbou	stavba	k1gFnSc7	stavba
z	z	k7c2	z
dílny	dílna	k1gFnSc2	dílna
architekta	architekt	k1gMnSc2	architekt
Josefa	Josef	k1gMnSc2	Josef
Fanty	Fanta	k1gMnSc2	Fanta
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodně	mezinárodně	k6eAd1	mezinárodně
využívaný	využívaný	k2eAgInSc1d1	využívaný
je	být	k5eAaImIp3nS	být
unikátní	unikátní	k2eAgInSc1d1	unikátní
Železniční	železniční	k2eAgInSc1d1	železniční
zkušební	zkušební	k2eAgInSc1d1	zkušební
okruh	okruh	k1gInSc1	okruh
Cerhenice	Cerhenice	k1gFnSc2	Cerhenice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Letectví	letectví	k1gNnSc2	letectví
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
je	být	k5eAaImIp3nS	být
celkem	celkem	k6eAd1	celkem
91	[number]	k4	91
civilních	civilní	k2eAgNnPc2d1	civilní
letišť	letiště	k1gNnPc2	letiště
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
46	[number]	k4	46
má	mít	k5eAaImIp3nS	mít
zpevněný	zpevněný	k2eAgInSc4d1	zpevněný
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
5	[number]	k4	5
letišť	letiště	k1gNnPc2	letiště
vypravuje	vypravovat	k5eAaImIp3nS	vypravovat
pravidelné	pravidelný	k2eAgInPc4d1	pravidelný
lety	let	k1gInPc4	let
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
letecké	letecký	k2eAgFnSc2d1	letecká
dopravy	doprava	k1gFnSc2	doprava
se	se	k3xPyFc4	se
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
realizuje	realizovat	k5eAaBmIp3nS	realizovat
přes	přes	k7c4	přes
pražské	pražský	k2eAgNnSc4d1	Pražské
letiště	letiště	k1gNnSc4	letiště
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
přepravilo	přepravit	k5eAaPmAgNnS	přepravit
13	[number]	k4	13
milionů	milion	k4xCgInPc2	milion
pasažérů	pasažér	k1gMnPc2	pasažér
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zimní	zimní	k2eAgFnSc6d1	zimní
sezóně	sezóna	k1gFnSc6	sezóna
2016	[number]	k4	2016
<g/>
/	/	kIx~	/
<g/>
17	[number]	k4	17
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
létalo	létat	k5eAaImAgNnS	létat
do	do	k7c2	do
105	[number]	k4	105
destinací	destinace	k1gFnPc2	destinace
v	v	k7c6	v
39	[number]	k4	39
zemích	zem	k1gFnPc6	zem
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgNnSc4d1	další
letiště	letiště	k1gNnSc4	letiště
s	s	k7c7	s
pravidelným	pravidelný	k2eAgInSc7d1	pravidelný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
méně	málo	k6eAd2	málo
hustým	hustý	k2eAgInSc7d1	hustý
civilním	civilní	k2eAgInSc7d1	civilní
provozem	provoz	k1gInSc7	provoz
patří	patřit	k5eAaImIp3nS	patřit
letiště	letiště	k1gNnSc2	letiště
Brno-Tuřany	Brno-Tuřan	k1gMnPc4	Brno-Tuřan
<g/>
,	,	kIx,	,
Ostrava-Leoše	Ostrava-Leoš	k1gMnPc4	Ostrava-Leoš
Janáčka	Janáček	k1gMnSc2	Janáček
<g/>
,	,	kIx,	,
Pardubice	Pardubice	k1gInPc1	Pardubice
a	a	k8xC	a
Karlovy	Karlův	k2eAgInPc1d1	Karlův
Vary	Vary	k1gInPc1	Vary
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Působí	působit	k5eAaImIp3nP	působit
zde	zde	k6eAd1	zde
tři	tři	k4xCgFnPc1	tři
hlavní	hlavní	k2eAgFnPc1d1	hlavní
pravidelné	pravidelný	k2eAgFnPc1d1	pravidelná
letecké	letecký	k2eAgFnPc1d1	letecká
společnosti	společnost	k1gFnPc1	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgMnSc1d3	nejstarší
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
jsou	být	k5eAaImIp3nP	být
České	český	k2eAgFnPc1d1	Česká
aerolinie	aerolinie	k1gFnPc1	aerolinie
<g/>
,	,	kIx,	,
největší	veliký	k2eAgFnSc1d3	veliký
letecká	letecký	k2eAgFnSc1d1	letecká
společnost	společnost	k1gFnSc1	společnost
je	být	k5eAaImIp3nS	být
Travel	Travel	k1gInSc4	Travel
Service	Service	k1gFnSc2	Service
<g/>
,	,	kIx,	,
pod	pod	k7c4	pod
který	který	k3yQgInSc4	který
spadá	spadat	k5eAaImIp3nS	spadat
také	také	k9	také
nízkonákladová	nízkonákladový	k2eAgFnSc1d1	nízkonákladová
společnost	společnost	k1gFnSc1	společnost
SmartWings	SmartWingsa	k1gFnPc2	SmartWingsa
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgFnPc4d1	další
české	český	k2eAgFnPc4d1	Česká
letecké	letecký	k2eAgFnPc4d1	letecká
společnosti	společnost	k1gFnPc4	společnost
patří	patřit	k5eAaImIp3nS	patřit
Van	van	k1gInSc1	van
Air	Air	k1gMnSc5	Air
Europe	Europ	k1gMnSc5	Europ
<g/>
,	,	kIx,	,
Silver	Silver	k1gInSc4	Silver
Air	Air	k1gFnSc2	Air
a	a	k8xC	a
další	další	k2eAgFnSc2d1	další
letecké	letecký	k2eAgFnSc2d1	letecká
společnosti	společnost	k1gFnSc2	společnost
poskytující	poskytující	k2eAgFnSc2d1	poskytující
služby	služba	k1gFnSc2	služba
aerotaxi	aerotaxi	k1gNnSc2	aerotaxi
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
ABS	ABS	kA	ABS
Jets	Jetsa	k1gFnPc2	Jetsa
<g/>
,	,	kIx,	,
CTR	CTR	kA	CTR
Flight	Flight	k1gMnSc1	Flight
Services	Services	k1gMnSc1	Services
<g/>
,	,	kIx,	,
G-JET	G-JET	k1gMnSc1	G-JET
či	či	k8xC	či
Silesia	Silesia	k1gFnSc1	Silesia
Air	Air	k1gFnSc1	Air
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Vodní	vodní	k2eAgFnSc1d1	vodní
doprava	doprava	k1gFnSc1	doprava
====	====	k?	====
</s>
</p>
<p>
<s>
Vodní	vodní	k2eAgFnSc1d1	vodní
doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
je	být	k5eAaImIp3nS	být
provozována	provozován	k2eAgFnSc1d1	provozována
na	na	k7c6	na
řekách	řeka	k1gFnPc6	řeka
Labe	Labe	k1gNnSc2	Labe
a	a	k8xC	a
Vltava	Vltava	k1gFnSc1	Vltava
a	a	k8xC	a
dalších	další	k2eAgFnPc6d1	další
uzavřených	uzavřený	k2eAgFnPc6d1	uzavřená
vodních	vodní	k2eAgFnPc6d1	vodní
plochách	plocha	k1gFnPc6	plocha
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
rekreační	rekreační	k2eAgInPc4d1	rekreační
účely	účel	k1gInPc4	účel
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějším	významný	k2eAgMnSc7d3	nejvýznamnější
provozovatelem	provozovatel	k1gMnSc7	provozovatel
vodní	vodní	k2eAgFnSc2d1	vodní
dopravy	doprava	k1gFnSc2	doprava
je	být	k5eAaImIp3nS	být
Československá	československý	k2eAgFnSc1d1	Československá
plavba	plavba	k1gFnSc1	plavba
labská	labský	k2eAgFnSc1d1	Labská
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ČR	ČR	kA	ČR
disponuje	disponovat	k5eAaBmIp3nS	disponovat
i	i	k9	i
vlastním	vlastní	k2eAgNnSc7d1	vlastní
přístavním	přístavní	k2eAgNnSc7d1	přístavní
územím	území	k1gNnSc7	území
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
námořního	námořní	k2eAgInSc2d1	námořní
přístavu	přístav	k1gInSc2	přístav
v	v	k7c6	v
Hamburku	Hamburk	k1gInSc6	Hamburk
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
</s>
<s>
<g/>
</s>
<s>
Přístavy	přístav	k1gInPc1	přístav
Moldauhafen	Moldauhafna	k1gFnPc2	Moldauhafna
a	a	k8xC	a
Saalehafen	Saalehafna	k1gFnPc2	Saalehafna
byly	být	k5eAaImAgFnP	být
na	na	k7c6	na
základě	základ	k1gInSc6	základ
Versailleské	versailleský	k2eAgFnSc2d1	Versailleská
smlouvy	smlouva	k1gFnSc2	smlouva
pronajaty	pronajat	k2eAgInPc4d1	pronajat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
na	na	k7c4	na
99	[number]	k4	99
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2028	[number]	k4	2028
<g/>
)	)	kIx)	)
Československu	Československo	k1gNnSc6	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
Československo	Československo	k1gNnSc1	Československo
zakoupilo	zakoupit	k5eAaPmAgNnS	zakoupit
i	i	k9	i
přístav	přístav	k1gInSc1	přístav
Peutehafen	Peutehafen	k2eAgInSc1d1	Peutehafen
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
je	on	k3xPp3gNnPc4	on
spravuje	spravovat	k5eAaImIp3nS	spravovat
Česko	Česko	k1gNnSc1	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Přístavy	přístav	k1gInPc4	přístav
využívala	využívat	k5eAaPmAgFnS	využívat
Československá	československý	k2eAgFnSc1d1	Československá
námořní	námořní	k2eAgFnSc1d1	námořní
plavba	plavba	k1gFnSc1	plavba
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
Česká	český	k2eAgFnSc1d1	Česká
námořní	námořní	k2eAgFnSc1d1	námořní
plavba	plavba	k1gFnSc1	plavba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velkou	velký	k2eAgFnSc4d1	velká
tradici	tradice	k1gFnSc4	tradice
(	(	kIx(	(
<g/>
již	již	k6eAd1	již
od	od	k7c2	od
Jakuba	Jakub	k1gMnSc2	Jakub
Krčína	Krčín	k1gMnSc2	Krčín
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
vodohospodářství	vodohospodářství	k1gNnSc1	vodohospodářství
<g/>
,	,	kIx,	,
splavňování	splavňování	k1gNnSc1	splavňování
<g/>
,	,	kIx,	,
budování	budování	k1gNnSc1	budování
jezů	jez	k1gInPc2	jez
a	a	k8xC	a
přehrad	přehrada	k1gFnPc2	přehrada
<g/>
.	.	kIx.	.
</s>
<s>
Největšími	veliký	k2eAgFnPc7d3	veliký
přehradami	přehrada	k1gFnPc7	přehrada
v	v	k7c6	v
ČR	ČR	kA	ČR
jsou	být	k5eAaImIp3nP	být
Orlík	Orlík	k1gInSc1	Orlík
<g/>
,	,	kIx,	,
Lipno	Lipno	k1gNnSc1	Lipno
<g/>
,	,	kIx,	,
Švihov	Švihov	k1gInSc1	Švihov
(	(	kIx(	(
<g/>
Želivka	Želivka	k1gFnSc1	Želivka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Nechranice	Nechranice	k1gFnSc1	Nechranice
<g/>
,	,	kIx,	,
Slapy	slap	k1gInPc1	slap
<g/>
,	,	kIx,	,
Slezská	slezský	k2eAgFnSc1d1	Slezská
Harta	Harta	k1gFnSc1	Harta
<g/>
,	,	kIx,	,
Vranov	Vranov	k1gInSc1	Vranov
<g/>
,	,	kIx,	,
Dalešice	Dalešice	k1gFnSc1	Dalešice
<g/>
,	,	kIx,	,
Rozkoš	rozkoš	k1gFnSc1	rozkoš
a	a	k8xC	a
soustava	soustava	k1gFnSc1	soustava
nádrží	nádrž	k1gFnPc2	nádrž
Nové	Nové	k2eAgInPc1d1	Nové
Mlýny	mlýn	k1gInPc1	mlýn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Městská	městský	k2eAgFnSc1d1	městská
hromadná	hromadný	k2eAgFnSc1d1	hromadná
doprava	doprava	k1gFnSc1	doprava
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
existuje	existovat	k5eAaImIp3nS	existovat
velká	velký	k2eAgFnSc1d1	velká
tradice	tradice	k1gFnSc1	tradice
městské	městský	k2eAgFnSc2d1	městská
hromadné	hromadný	k2eAgFnSc2d1	hromadná
dopravy	doprava	k1gFnSc2	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Systémy	systém	k1gInPc1	systém
MHD	MHD	kA	MHD
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgInP	začít
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
od	od	k7c2	od
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
–	–	k?	–
koněspřežná	koněspřežný	k2eAgFnSc1d1	koněspřežná
tramvaj	tramvaj	k1gFnSc1	tramvaj
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
byla	být	k5eAaImAgNnP	být
zavedena	zavést	k5eAaPmNgNnP	zavést
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1869	[number]	k4	1869
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1875	[number]	k4	1875
pak	pak	k6eAd1	pak
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
parní	parní	k2eAgFnSc4d1	parní
tramvaj	tramvaj	k1gFnSc4	tramvaj
mělo	mít	k5eAaImAgNnS	mít
také	také	k9	také
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
uvedena	uveden	k2eAgFnSc1d1	uvedena
roku	rok	k1gInSc2	rok
1884	[number]	k4	1884
<g/>
.	.	kIx.	.
</s>
<s>
Prvenství	prvenství	k1gNnSc1	prvenství
při	při	k7c6	při
zavádění	zavádění	k1gNnSc6	zavádění
elektrických	elektrický	k2eAgFnPc2d1	elektrická
tramvají	tramvaj	k1gFnPc2	tramvaj
si	se	k3xPyFc3	se
zajistily	zajistit	k5eAaPmAgFnP	zajistit
Teplice	Teplice	k1gFnPc1	Teplice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
elektřinou	elektřina	k1gFnSc7	elektřina
poháněná	poháněný	k2eAgFnSc1d1	poháněná
tramvaj	tramvaj	k1gFnSc1	tramvaj
vyjela	vyjet	k5eAaPmAgFnS	vyjet
na	na	k7c4	na
trať	trať	k1gFnSc4	trať
roku	rok	k1gInSc2	rok
1895	[number]	k4	1895
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
přidala	přidat	k5eAaPmAgFnS	přidat
Praha	Praha	k1gFnSc1	Praha
(	(	kIx(	(
<g/>
zde	zde	k6eAd1	zde
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1891	[number]	k4	1891
fungovala	fungovat	k5eAaImAgFnS	fungovat
převážně	převážně	k6eAd1	převážně
předváděcí	předváděcí	k2eAgFnSc1d1	předváděcí
Křižíkova	Křižíkův	k2eAgFnSc1d1	Křižíkova
elektrická	elektrický	k2eAgFnSc1d1	elektrická
dráha	dráha	k1gFnSc1	dráha
na	na	k7c6	na
Letné	Letná	k1gFnSc6	Letná
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1897	[number]	k4	1897
Liberec	Liberec	k1gInSc1	Liberec
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1899	[number]	k4	1899
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
,	,	kIx,	,
Plzeň	Plzeň	k1gFnSc1	Plzeň
a	a	k8xC	a
Ústí	ústí	k1gNnSc1	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
,	,	kIx,	,
1900	[number]	k4	1900
Brno	Brno	k1gNnSc1	Brno
a	a	k8xC	a
Jablonec	Jablonec	k1gInSc1	Jablonec
nad	nad	k7c7	nad
Nisou	Nisa	k1gFnSc7	Nisa
<g/>
,	,	kIx,	,
1901	[number]	k4	1901
Most	most	k1gInSc1	most
a	a	k8xC	a
Litvínov	Litvínov	k1gInSc1	Litvínov
a	a	k8xC	a
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
,	,	kIx,	,
1902	[number]	k4	1902
Mariánské	mariánský	k2eAgFnPc1d1	Mariánská
Lázně	lázeň	k1gFnPc1	lázeň
<g/>
,	,	kIx,	,
1905	[number]	k4	1905
Opava	Opava	k1gFnSc1	Opava
<g/>
,	,	kIx,	,
1909	[number]	k4	1909
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
a	a	k8xC	a
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g/>
,	,	kIx,	,
1911	[number]	k4	1911
Těšín	Těšín	k1gInSc4	Těšín
a	a	k8xC	a
1916	[number]	k4	1916
Bohumín	Bohumín	k1gInSc1	Bohumín
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
funguje	fungovat	k5eAaImIp3nS	fungovat
v	v	k7c6	v
ČR	ČR	kA	ČR
sedm	sedm	k4xCc4	sedm
tramvajových	tramvajový	k2eAgInPc2d1	tramvajový
systémů	systém	k1gInPc2	systém
<g/>
:	:	kIx,	:
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
Ostravě	Ostrava	k1gFnSc6	Ostrava
<g/>
,	,	kIx,	,
Plzni	Plzeň	k1gFnSc6	Plzeň
<g/>
,	,	kIx,	,
Liberci	Liberec	k1gInSc6	Liberec
<g/>
,	,	kIx,	,
Olomouci	Olomouc	k1gFnSc6	Olomouc
a	a	k8xC	a
meziměstská	meziměstský	k2eAgFnSc1d1	meziměstská
tramvaj	tramvaj	k1gFnSc1	tramvaj
mezi	mezi	k7c7	mezi
Mostem	most	k1gInSc7	most
a	a	k8xC	a
Litvínovem	Litvínov	k1gInSc7	Litvínov
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
je	být	k5eAaImIp3nS	být
patrný	patrný	k2eAgInSc1d1	patrný
trend	trend	k1gInSc1	trend
úbytku	úbytek	k1gInSc2	úbytek
tramvajové	tramvajový	k2eAgFnSc2d1	tramvajová
dopravy	doprava	k1gFnSc2	doprava
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
autobusové	autobusový	k2eAgInPc1d1	autobusový
<g/>
.	.	kIx.	.
</s>
<s>
Autobusová	autobusový	k2eAgFnSc1d1	autobusová
městská	městský	k2eAgFnSc1d1	městská
doprava	doprava	k1gFnSc1	doprava
se	se	k3xPyFc4	se
od	od	k7c2	od
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
integruje	integrovat	k5eAaBmIp3nS	integrovat
s	s	k7c7	s
příměstskou	příměstský	k2eAgFnSc7d1	příměstská
<g/>
,	,	kIx,	,
větší	veliký	k2eAgFnSc4d2	veliký
roli	role	k1gFnSc4	role
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
také	také	k9	také
hraje	hrát	k5eAaImIp3nS	hrát
městská	městský	k2eAgFnSc1d1	městská
železnice	železnice	k1gFnSc1	železnice
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Esko	eska	k1gFnSc5	eska
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Okrajovou	okrajový	k2eAgFnSc4d1	okrajová
roli	role	k1gFnSc4	role
v	v	k7c6	v
systému	systém	k1gInSc6	systém
hromadné	hromadný	k2eAgFnSc2d1	hromadná
dopravy	doprava	k1gFnSc2	doprava
hrají	hrát	k5eAaImIp3nP	hrát
lanové	lanový	k2eAgFnPc1d1	lanová
dráhy	dráha	k1gFnPc1	dráha
(	(	kIx(	(
<g/>
Karlovy	Karlův	k2eAgInPc1d1	Karlův
Vary	Vary	k1gInPc1	Vary
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
či	či	k8xC	či
vodní	vodní	k2eAgFnSc1d1	vodní
doprava	doprava	k1gFnSc1	doprava
(	(	kIx(	(
<g/>
pět	pět	k4xCc4	pět
pražských	pražský	k2eAgInPc2d1	pražský
přívozů	přívoz	k1gInPc2	přívoz
<g/>
,	,	kIx,	,
doprava	doprava	k1gFnSc1	doprava
na	na	k7c6	na
Brněnské	brněnský	k2eAgFnSc6d1	brněnská
přehradě	přehrada	k1gFnSc6	přehrada
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jediným	jediný	k2eAgNnSc7d1	jediné
městem	město	k1gNnSc7	město
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
systém	systém	k1gInSc1	systém
městské	městský	k2eAgFnSc2d1	městská
podzemní	podzemní	k2eAgFnSc2d1	podzemní
dráhy	dráha	k1gFnSc2	dráha
<g/>
,	,	kIx,	,
takzvaného	takzvaný	k2eAgNnSc2d1	takzvané
metra	metro	k1gNnSc2	metro
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Provoz	provoz	k1gInSc1	provoz
pražského	pražský	k2eAgNnSc2d1	Pražské
metra	metro	k1gNnSc2	metro
byl	být	k5eAaImAgInS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
9	[number]	k4	9
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1974	[number]	k4	1974
<g/>
.	.	kIx.	.
</s>
<s>
Síť	síť	k1gFnSc1	síť
tras	tras	k1gInSc1	tras
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2015	[number]	k4	2015
měřila	měřit	k5eAaImAgFnS	měřit
65,2	[number]	k4	65,2
km	km	kA	km
<g/>
,	,	kIx,	,
soupravy	souprava	k1gFnPc1	souprava
zastavovaly	zastavovat	k5eAaImAgFnP	zastavovat
v	v	k7c6	v
61	[number]	k4	61
stanicích	stanice	k1gFnPc6	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Pražské	pražský	k2eAgNnSc1d1	Pražské
metro	metro	k1gNnSc1	metro
přepraví	přepravit	k5eAaPmIp3nS	přepravit
přes	přes	k7c4	přes
1,6	[number]	k4	1,6
milionů	milion	k4xCgInPc2	milion
cestujících	cestující	k1gMnPc2	cestující
denně	denně	k6eAd1	denně
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
hodnocení	hodnocení	k1gNnSc2	hodnocení
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
automobilové	automobilový	k2eAgFnSc2d1	automobilová
federace	federace	k1gFnSc2	federace
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
má	mít	k5eAaImIp3nS	mít
Praha	Praha	k1gFnSc1	Praha
čtvrtou	čtvrtá	k1gFnSc4	čtvrtá
nejkvalitnější	kvalitní	k2eAgFnSc4d3	nejkvalitnější
městskou	městský	k2eAgFnSc4d1	městská
hromadnou	hromadný	k2eAgFnSc4d1	hromadná
dopravu	doprava	k1gFnSc4	doprava
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
(	(	kIx(	(
<g/>
po	po	k7c6	po
Mnichovu	Mnichov	k1gInSc6	Mnichov
<g/>
,	,	kIx,	,
Helsinkách	Helsinky	k1gFnPc6	Helsinky
a	a	k8xC	a
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Energetika	energetika	k1gFnSc1	energetika
===	===	k?	===
</s>
</p>
<p>
<s>
Česko	Česko	k1gNnSc1	Česko
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
snižuje	snižovat	k5eAaImIp3nS	snižovat
svojí	svojit	k5eAaImIp3nS	svojit
energetickou	energetický	k2eAgFnSc4d1	energetická
závislost	závislost	k1gFnSc4	závislost
na	na	k7c6	na
hnědém	hnědý	k2eAgNnSc6d1	hnědé
uhlí	uhlí	k1gNnSc6	uhlí
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
negativně	negativně	k6eAd1	negativně
projevovala	projevovat	k5eAaImAgFnS	projevovat
v	v	k7c6	v
kvalitě	kvalita	k1gFnSc6	kvalita
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
v	v	k7c6	v
severních	severní	k2eAgFnPc6d1	severní
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
bylo	být	k5eAaImAgNnS	být
65,4	[number]	k4	65,4
procent	procento	k1gNnPc2	procento
elektřiny	elektřina	k1gFnSc2	elektřina
vyráběno	vyrábět	k5eAaImNgNnS	vyrábět
pomocí	pomocí	k7c2	pomocí
páry	pára	k1gFnSc2	pára
a	a	k8xC	a
spalováním	spalování	k1gNnSc7	spalování
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
uhlí	uhlí	k1gNnSc2	uhlí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
30	[number]	k4	30
procent	procento	k1gNnPc2	procento
v	v	k7c6	v
jaderných	jaderný	k2eAgFnPc6d1	jaderná
elektrárnách	elektrárna	k1gFnPc6	elektrárna
a	a	k8xC	a
4,6	[number]	k4	4,6
procenta	procento	k1gNnSc2	procento
z	z	k7c2	z
obnovitelných	obnovitelný	k2eAgInPc2d1	obnovitelný
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
vodních	vodní	k2eAgFnPc2d1	vodní
elektráren	elektrárna	k1gFnPc2	elektrárna
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgInSc7d3	veliký
českým	český	k2eAgInSc7d1	český
energetickým	energetický	k2eAgInSc7d1	energetický
zdrojem	zdroj	k1gInSc7	zdroj
je	být	k5eAaImIp3nS	být
Jaderná	jaderný	k2eAgFnSc1d1	jaderná
elektrárna	elektrárna	k1gFnSc1	elektrárna
Temelín	Temelín	k1gInSc1	Temelín
(	(	kIx(	(
<g/>
14	[number]	k4	14
%	%	kIx~	%
výroby	výroba	k1gFnSc2	výroba
elektřiny	elektřina	k1gFnSc2	elektřina
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
další	další	k2eAgFnSc1d1	další
jaderná	jaderný	k2eAgFnSc1d1	jaderná
elektrárna	elektrárna	k1gFnSc1	elektrárna
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Dukovanech	Dukovany	k1gInPc6	Dukovany
<g/>
.	.	kIx.	.
</s>
<s>
Česko	Česko	k1gNnSc1	Česko
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
největší	veliký	k2eAgMnPc4d3	veliký
vývozce	vývozce	k1gMnPc4	vývozce
proudu	proud	k1gInSc2	proud
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
vyvezlo	vyvézt	k5eAaPmAgNnS	vyvézt
asi	asi	k9	asi
17	[number]	k4	17
TWh	TWh	k1gFnPc2	TWh
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
vyrobí	vyrobit	k5eAaPmIp3nS	vyrobit
Temelín	Temelín	k1gInSc1	Temelín
<g/>
.	.	kIx.	.
</s>
<s>
ČR	ČR	kA	ČR
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
sedmém	sedmý	k4xOgNnSc6	sedmý
místě	místo	k1gNnSc6	místo
na	na	k7c6	na
světě	svět	k1gInSc6	svět
ve	v	k7c6	v
vývozu	vývoz	k1gInSc6	vývoz
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
a	a	k8xC	a
na	na	k7c6	na
desátém	desátý	k4xOgInSc6	desátý
ve	v	k7c6	v
výrobě	výroba	k1gFnSc6	výroba
energie	energie	k1gFnSc2	energie
z	z	k7c2	z
jádra	jádro	k1gNnSc2	jádro
<g/>
.	.	kIx.	.
<g/>
Zemní	zemní	k2eAgInSc1d1	zemní
plyn	plyn	k1gInSc1	plyn
do	do	k7c2	do
Česka	Česko	k1gNnSc2	Česko
dodává	dodávat	k5eAaImIp3nS	dodávat
ruská	ruský	k2eAgFnSc1d1	ruská
společnost	společnost	k1gFnSc1	společnost
Gazprom	Gazprom	k1gInSc1	Gazprom
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
tři	tři	k4xCgFnPc1	tři
čtvrtiny	čtvrtina	k1gFnPc1	čtvrtina
spotřeby	spotřeba	k1gFnSc2	spotřeba
a	a	k8xC	a
zbytek	zbytek	k1gInSc1	zbytek
dodávají	dodávat	k5eAaImIp3nP	dodávat
norské	norský	k2eAgFnPc4d1	norská
společnosti	společnost	k1gFnPc4	společnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2013	[number]	k4	2013
byl	být	k5eAaImAgInS	být
otevřen	otevřen	k2eAgInSc1d1	otevřen
plynovod	plynovod	k1gInSc1	plynovod
Gazela	gazela	k1gFnSc1	gazela
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
přivádí	přivádět	k5eAaImIp3nS	přivádět
ruský	ruský	k2eAgInSc1d1	ruský
plyn	plyn	k1gInSc1	plyn
také	také	k6eAd1	také
severní	severní	k2eAgFnSc7d1	severní
cestou	cesta	k1gFnSc7	cesta
<g/>
,	,	kIx,	,
Česko	Česko	k1gNnSc1	Česko
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
zbavilo	zbavit	k5eAaPmAgNnS	zbavit
závislosti	závislost	k1gFnSc2	závislost
na	na	k7c6	na
plynu	plyn	k1gInSc6	plyn
z	z	k7c2	z
plynovodů	plynovod	k1gInPc2	plynovod
vedoucích	vedoucí	k1gMnPc2	vedoucí
přes	přes	k7c4	přes
Ukrajinu	Ukrajina	k1gFnSc4	Ukrajina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
dodávky	dodávka	k1gFnSc2	dodávka
několikrát	několikrát	k6eAd1	několikrát
blokovány	blokován	k2eAgFnPc1d1	blokována
kvůli	kvůli	k7c3	kvůli
sporům	spor	k1gInPc3	spor
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
a	a	k8xC	a
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Ropu	ropa	k1gFnSc4	ropa
do	do	k7c2	do
Česka	Česko	k1gNnSc2	Česko
přivádějí	přivádět	k5eAaImIp3nP	přivádět
ropovod	ropovod	k1gInSc4	ropovod
Družba	družba	k1gMnSc1	družba
z	z	k7c2	z
Ruska	Rusko	k1gNnSc2	Rusko
a	a	k8xC	a
ropovod	ropovod	k1gInSc1	ropovod
Ingolstadt	Ingolstadt	k1gInSc1	Ingolstadt
z	z	k7c2	z
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Maloobchod	maloobchod	k1gInSc4	maloobchod
===	===	k?	===
</s>
</p>
<p>
<s>
Symbolem	symbol	k1gInSc7	symbol
konzumního	konzumní	k2eAgInSc2d1	konzumní
životního	životní	k2eAgInSc2d1	životní
stylu	styl	k1gInSc2	styl
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
po	po	k7c6	po
revoluci	revoluce	k1gFnSc6	revoluce
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
supermarkety	supermarket	k1gInPc4	supermarket
a	a	k8xC	a
hypermarkety	hypermarket	k1gInPc4	hypermarket
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
ovládly	ovládnout	k5eAaPmAgInP	ovládnout
maloobchodní	maloobchodní	k2eAgFnSc4d1	maloobchodní
sféru	sféra	k1gFnSc4	sféra
více	hodně	k6eAd2	hodně
než	než	k8xS	než
kdekoli	kdekoli	k6eAd1	kdekoli
jinde	jinde	k6eAd1	jinde
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Cestovní	cestovní	k2eAgInSc1d1	cestovní
ruch	ruch	k1gInSc1	ruch
===	===	k?	===
</s>
</p>
<p>
<s>
České	český	k2eAgNnSc1d1	české
hospodářství	hospodářství	k1gNnSc1	hospodářství
získává	získávat	k5eAaImIp3nS	získávat
vysoké	vysoký	k2eAgInPc4d1	vysoký
příjmy	příjem	k1gInPc4	příjem
z	z	k7c2	z
cestovního	cestovní	k2eAgInSc2d1	cestovní
ruchu	ruch	k1gInSc2	ruch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
celkové	celkový	k2eAgInPc4d1	celkový
výnosy	výnos	k1gInPc4	výnos
z	z	k7c2	z
cestovního	cestovní	k2eAgInSc2d1	cestovní
ruchu	ruch	k1gInSc2	ruch
dosáhly	dosáhnout	k5eAaPmAgFnP	dosáhnout
118,13	[number]	k4	118,13
miliardy	miliarda	k4xCgFnPc4	miliarda
korun	koruna	k1gFnPc2	koruna
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
tehdy	tehdy	k6eAd1	tehdy
představovalo	představovat	k5eAaImAgNnS	představovat
5,5	[number]	k4	5,5
%	%	kIx~	%
HDP	HDP	kA	HDP
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
však	však	k8xC	však
zájem	zájem	k1gInSc1	zájem
cizinců	cizinec	k1gMnPc2	cizinec
o	o	k7c6	o
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
posílení	posílení	k1gNnSc2	posílení
kurzu	kurz	k1gInSc2	kurz
české	český	k2eAgFnSc2d1	Česká
koruny	koruna	k1gFnSc2	koruna
<g/>
,	,	kIx,	,
poněkud	poněkud	k6eAd1	poněkud
opadl	opadnout	k5eAaPmAgMnS	opadnout
<g/>
.	.	kIx.	.
</s>
<s>
Posléze	posléze	k6eAd1	posléze
koruna	koruna	k1gFnSc1	koruna
oslabila	oslabit	k5eAaPmAgFnS	oslabit
<g/>
;	;	kIx,	;
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
navštívilo	navštívit	k5eAaPmAgNnS	navštívit
Česko	Česko	k1gNnSc1	Česko
9,3	[number]	k4	9,3
milionu	milion	k4xCgInSc2	milion
turistů	turist	k1gMnPc2	turist
ze	z	k7c2	z
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
je	být	k5eAaImIp3nS	být
pátým	pátý	k4xOgNnSc7	pátý
nejnavštěvovanějším	navštěvovaný	k2eAgNnSc7d3	nejnavštěvovanější
městem	město	k1gNnSc7	město
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
po	po	k7c6	po
Londýnu	Londýn	k1gInSc3	Londýn
<g/>
,	,	kIx,	,
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
Istanbulu	Istanbul	k1gInSc6	Istanbul
a	a	k8xC	a
Římu	Řím	k1gInSc3	Řím
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
agentury	agentura	k1gFnSc2	agentura
CzechTourism	CzechTourisma	k1gFnPc2	CzechTourisma
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
nejnavštěvovanějšími	navštěvovaný	k2eAgInPc7d3	nejnavštěvovanější
turistickými	turistický	k2eAgInPc7d1	turistický
cíli	cíl	k1gInPc7	cíl
v	v	k7c6	v
ČR	ČR	kA	ČR
Pražský	pražský	k2eAgInSc1d1	pražský
hrad	hrad	k1gInSc1	hrad
(	(	kIx(	(
<g/>
1	[number]	k4	1
799	[number]	k4	799
300	[number]	k4	300
turistů	turist	k1gMnPc2	turist
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Zoologická	zoologický	k2eAgFnSc1d1	zoologická
zahrada	zahrada	k1gFnSc1	zahrada
Praha	Praha	k1gFnSc1	Praha
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1	[number]	k4	1
382	[number]	k4	382
200	[number]	k4	200
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
AquaPalace	AquaPalace	k1gFnSc1	AquaPalace
Praha	Praha	k1gFnSc1	Praha
(	(	kIx(	(
<g/>
845	[number]	k4	845
300	[number]	k4	300
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Dolní	dolní	k2eAgFnSc1d1	dolní
oblast	oblast	k1gFnSc1	oblast
Vítkovic	Vítkovice	k1gInPc2	Vítkovice
a	a	k8xC	a
Landek	Landek	k1gInSc1	Landek
Park	park	k1gInSc1	park
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
(	(	kIx(	(
<g/>
808	[number]	k4	808
900	[number]	k4	900
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Staroměstská	staroměstský	k2eAgFnSc1d1	Staroměstská
radnice	radnice	k1gFnSc1	radnice
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
orloje	orloj	k1gInSc2	orloj
<g/>
)	)	kIx)	)
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
(	(	kIx(	(
<g/>
739	[number]	k4	739
800	[number]	k4	800
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Pivovarské	pivovarský	k2eAgNnSc1d1	Pivovarské
muzeum	muzeum	k1gNnSc1	muzeum
a	a	k8xC	a
podzemí	podzemí	k1gNnSc1	podzemí
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
(	(	kIx(	(
<g/>
600	[number]	k4	600
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Zoologická	zoologický	k2eAgFnSc1d1	zoologická
zahrada	zahrada	k1gFnSc1	zahrada
Zlín-Lešná	Zlín-Lešný	k2eAgFnSc1d1	Zlín-Lešná
(	(	kIx(	(
<g/>
585	[number]	k4	585
100	[number]	k4	100
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Židovské	židovský	k2eAgNnSc1d1	Židovské
muzeum	muzeum	k1gNnSc1	muzeum
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
(	(	kIx(	(
<g/>
580	[number]	k4	580
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Petřínská	petřínský	k2eAgFnSc1d1	Petřínská
rozhledna	rozhledna	k1gFnSc1	rozhledna
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
(	(	kIx(	(
<g/>
557	[number]	k4	557
400	[number]	k4	400
<g/>
)	)	kIx)	)
a	a	k8xC	a
Zoologická	zoologický	k2eAgFnSc1d1	zoologická
zahrada	zahrada	k1gFnSc1	zahrada
Ostrava	Ostrava	k1gFnSc1	Ostrava
(	(	kIx(	(
<g/>
540	[number]	k4	540
500	[number]	k4	500
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Úspěch	úspěch	k1gInSc1	úspěch
pražské	pražský	k2eAgFnSc2d1	Pražská
zoo	zoo	k1gFnSc2	zoo
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
její	její	k3xOp3gFnSc7	její
prestiží	prestiž	k1gFnSc7	prestiž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
uživatelé	uživatel	k1gMnPc1	uživatel
největšího	veliký	k2eAgInSc2d3	veliký
cestovatelského	cestovatelský	k2eAgInSc2d1	cestovatelský
serveru	server	k1gInSc2	server
TripAdvisor	TripAdvisor	k1gMnSc1	TripAdvisor
pražskou	pražský	k2eAgFnSc4d1	Pražská
zoo	zoo	k1gFnSc4	zoo
určili	určit	k5eAaPmAgMnP	určit
jako	jako	k9	jako
čtvrtou	čtvrtá	k1gFnSc4	čtvrtá
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Magazín	magazín	k1gInSc1	magazín
Forbes	forbes	k1gInSc1	forbes
ji	on	k3xPp3gFnSc4	on
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
sedmou	sedmý	k4xOgFnSc4	sedmý
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
<g/>
Z	z	k7c2	z
hradů	hrad	k1gInPc2	hrad
a	a	k8xC	a
zámků	zámek	k1gInPc2	zámek
jsou	být	k5eAaImIp3nP	být
kromě	kromě	k7c2	kromě
Pražského	pražský	k2eAgInSc2d1	pražský
hradu	hrad	k1gInSc2	hrad
nejnavštěvovanější	navštěvovaný	k2eAgInSc1d3	nejnavštěvovanější
zámek	zámek	k1gInSc1	zámek
Lednice	Lednice	k1gFnSc1	Lednice
<g/>
,	,	kIx,	,
zámek	zámek	k1gInSc1	zámek
Český	český	k2eAgInSc1d1	český
Krumlov	Krumlov	k1gInSc1	Krumlov
<g/>
,	,	kIx,	,
zámek	zámek	k1gInSc1	zámek
Hluboká	Hluboká	k1gFnSc1	Hluboká
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
<g/>
,	,	kIx,	,
hrad	hrad	k1gInSc1	hrad
Karlštejn	Karlštejn	k1gInSc1	Karlštejn
<g/>
,	,	kIx,	,
zámek	zámek	k1gInSc1	zámek
Průhonice	Průhonice	k1gFnPc1	Průhonice
<g/>
,	,	kIx,	,
zámek	zámek	k1gInSc1	zámek
Dětenice	Dětenice	k1gFnSc2	Dětenice
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
zámek	zámek	k1gInSc1	zámek
Konopiště	Konopiště	k1gNnSc2	Konopiště
<g/>
,	,	kIx,	,
arcibiskupský	arcibiskupský	k2eAgInSc4d1	arcibiskupský
zámek	zámek	k1gInSc4	zámek
a	a	k8xC	a
zahrady	zahrada	k1gFnPc4	zahrada
v	v	k7c6	v
Kroměříži	Kroměříž	k1gFnSc6	Kroměříž
a	a	k8xC	a
zámek	zámek	k1gInSc1	zámek
Loučeň	Loučeň	k1gFnSc1	Loučeň
<g/>
.	.	kIx.	.
<g/>
Ve	v	k7c6	v
Středočeském	středočeský	k2eAgInSc6d1	středočeský
kraji	kraj	k1gInSc6	kraj
jsou	být	k5eAaImIp3nP	být
turistickými	turistický	k2eAgNnPc7d1	turistické
lákadly	lákadlo	k1gNnPc7	lákadlo
také	také	k9	také
kostnice	kostnice	k1gFnPc1	kostnice
v	v	k7c6	v
Sedlci	Sedlec	k1gInSc6	Sedlec
v	v	k7c6	v
Kutné	kutný	k2eAgFnSc6d1	Kutná
Hoře	hora	k1gFnSc6	hora
a	a	k8xC	a
chrám	chrám	k1gInSc1	chrám
svaté	svatý	k2eAgFnSc2d1	svatá
Barbory	Barbora	k1gFnSc2	Barbora
tamtéž	tamtéž	k6eAd1	tamtéž
<g/>
,	,	kIx,	,
hrady	hrad	k1gInPc1	hrad
Křivoklát	Křivoklát	k1gInSc1	Křivoklát
<g/>
,	,	kIx,	,
Český	český	k2eAgInSc1d1	český
Šternberk	Šternberk	k1gInSc1	Šternberk
a	a	k8xC	a
Kokořín	Kokořín	k1gInSc1	Kokořín
<g/>
,	,	kIx,	,
zámky	zámek	k1gInPc1	zámek
Poděbrady	Poděbrady	k1gInPc1	Poděbrady
a	a	k8xC	a
Veltrusy	Veltrusy	k1gInPc1	Veltrusy
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
v	v	k7c6	v
Jihočeském	jihočeský	k2eAgInSc6d1	jihočeský
kraji	kraj	k1gInSc6	kraj
klášter	klášter	k1gInSc4	klášter
Vyšší	vysoký	k2eAgInSc1d2	vyšší
Brod	Brod	k1gInSc1	Brod
<g/>
,	,	kIx,	,
hrady	hrad	k1gInPc1	hrad
Rožmberk	Rožmberk	k1gInSc1	Rožmberk
a	a	k8xC	a
Zvíkov	Zvíkov	k1gInSc1	Zvíkov
<g/>
,	,	kIx,	,
zámky	zámek	k1gInPc1	zámek
Orlík	Orlík	k1gInSc1	Orlík
<g/>
,	,	kIx,	,
Červená	červený	k2eAgFnSc1d1	červená
Lhota	Lhota	k1gFnSc1	Lhota
a	a	k8xC	a
Jindřichův	Jindřichův	k2eAgInSc1d1	Jindřichův
Hradec	Hradec	k1gInSc1	Hradec
<g/>
,	,	kIx,	,
v	v	k7c6	v
Karlovarském	karlovarský	k2eAgInSc6d1	karlovarský
kraji	kraj	k1gInSc6	kraj
hrad	hrad	k1gInSc1	hrad
Loket	loket	k1gInSc1	loket
<g/>
,	,	kIx,	,
v	v	k7c6	v
Ústeckém	ústecký	k2eAgInSc6d1	ústecký
kraji	kraj	k1gInSc6	kraj
Památník	památník	k1gInSc1	památník
Terezín	Terezín	k1gInSc1	Terezín
<g/>
,	,	kIx,	,
hrad	hrad	k1gInSc1	hrad
Hazmburk	Hazmburk	k1gInSc1	Hazmburk
a	a	k8xC	a
zámek	zámek	k1gInSc1	zámek
Duchcov	Duchcovo	k1gNnPc2	Duchcovo
<g/>
,	,	kIx,	,
v	v	k7c6	v
Libereckém	liberecký	k2eAgInSc6d1	liberecký
<g />
.	.	kIx.	.
</s>
<s>
hrady	hrad	k1gInPc1	hrad
Trosky	troska	k1gFnSc2	troska
<g/>
,	,	kIx,	,
Grabštejn	Grabštejn	k1gNnSc1	Grabštejn
<g/>
,	,	kIx,	,
Lemberk	Lemberk	k1gInSc1	Lemberk
<g/>
,	,	kIx,	,
Bezděz	Bezděz	k1gInSc1	Bezděz
a	a	k8xC	a
zámek	zámek	k1gInSc1	zámek
Sychrov	Sychrovo	k1gNnPc2	Sychrovo
<g/>
,	,	kIx,	,
v	v	k7c6	v
Královéhradeckém	královéhradecký	k2eAgInSc6d1	královéhradecký
Bílá	bílý	k2eAgFnSc1d1	bílá
věž	věž	k1gFnSc1	věž
v	v	k7c6	v
Hradci	Hradec	k1gInSc6	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
,	,	kIx,	,
hrad	hrad	k1gInSc1	hrad
Kost	kost	k1gFnSc1	kost
<g/>
,	,	kIx,	,
zámky	zámek	k1gInPc1	zámek
Ratibořice	Ratibořice	k1gFnPc1	Ratibořice
<g/>
,	,	kIx,	,
Karlova	Karlův	k2eAgFnSc1d1	Karlova
Koruna	koruna	k1gFnSc1	koruna
a	a	k8xC	a
Hrádek	hrádek	k1gInSc1	hrádek
u	u	k7c2	u
Nechanic	Nechanice	k1gFnPc2	Nechanice
<g/>
,	,	kIx,	,
v	v	k7c6	v
Pardubickém	pardubický	k2eAgInSc6d1	pardubický
Soubor	soubor	k1gInSc1	soubor
lidových	lidový	k2eAgFnPc2d1	lidová
staveb	stavba	k1gFnPc2	stavba
u	u	k7c2	u
Hlinska	Hlinsko	k1gNnSc2	Hlinsko
a	a	k8xC	a
zámek	zámek	k1gInSc1	zámek
<g />
.	.	kIx.	.
</s>
<s>
Pardubice	Pardubice	k1gInPc1	Pardubice
<g/>
,	,	kIx,	,
na	na	k7c6	na
Vysočině	vysočina	k1gFnSc6	vysočina
zámek	zámek	k1gInSc1	zámek
Telč	Telč	k1gFnSc1	Telč
<g/>
,	,	kIx,	,
v	v	k7c6	v
Olomouckém	olomoucký	k2eAgInSc6d1	olomoucký
hrad	hrad	k1gInSc1	hrad
Bouzov	Bouzov	k1gInSc4	Bouzov
<g/>
,	,	kIx,	,
v	v	k7c6	v
Jihomoravském	jihomoravský	k2eAgInSc6d1	jihomoravský
katedrála	katedrála	k1gFnSc1	katedrála
sv.	sv.	kA	sv.
Petra	Petra	k1gFnSc1	Petra
a	a	k8xC	a
Pavla	Pavla	k1gFnSc1	Pavla
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
hrady	hrad	k1gInPc1	hrad
Špilberk	Špilberk	k1gInSc1	Špilberk
<g/>
,	,	kIx,	,
Pernštejn	Pernštejn	k1gInSc1	Pernštejn
<g/>
,	,	kIx,	,
Veveří	veveří	k2eAgInSc1d1	veveří
a	a	k8xC	a
Bítov	Bítov	k1gInSc1	Bítov
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Zlínském	zlínský	k2eAgInSc6d1	zlínský
Valašské	valašský	k2eAgInPc1d1	valašský
muzeum	muzeum	k1gNnSc4	muzeum
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
v	v	k7c6	v
Rožnově	Rožnov	k1gInSc6	Rožnov
pod	pod	k7c7	pod
Radhoštěm	Radhošť	k1gInSc7	Radhošť
<g/>
,	,	kIx,	,
hrad	hrad	k1gInSc1	hrad
Buchlov	Buchlov	k1gInSc1	Buchlov
a	a	k8xC	a
zámek	zámek	k1gInSc4	zámek
Buchlovice	Buchlovice	k1gFnPc4	Buchlovice
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejznámějším	známý	k2eAgFnPc3d3	nejznámější
pražským	pražský	k2eAgFnPc3d1	Pražská
pamětihodnostem	pamětihodnost	k1gFnPc3	pamětihodnost
patří	patřit	k5eAaImIp3nS	patřit
krom	krom	k7c2	krom
dosud	dosud	k6eAd1	dosud
jmenovaných	jmenovaný	k2eAgInPc2d1	jmenovaný
Týnský	týnský	k2eAgInSc4d1	týnský
chrám	chrám	k1gInSc4	chrám
<g/>
,	,	kIx,	,
Staronová	staronový	k2eAgFnSc1d1	staronová
synagoga	synagoga	k1gFnSc1	synagoga
<g/>
,	,	kIx,	,
Starý	starý	k2eAgInSc1d1	starý
židovský	židovský	k2eAgInSc4d1	židovský
hřbitov	hřbitov	k1gInSc4	hřbitov
<g/>
,	,	kIx,	,
Betlémská	betlémský	k2eAgFnSc1d1	Betlémská
kaple	kaple	k1gFnSc1	kaple
<g/>
,	,	kIx,	,
Břevnovský	břevnovský	k2eAgInSc1d1	břevnovský
klášter	klášter	k1gInSc1	klášter
<g/>
,	,	kIx,	,
Stavovské	stavovský	k2eAgNnSc1d1	Stavovské
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
Pražské	pražský	k2eAgNnSc1d1	Pražské
Jezulátko	Jezulátko	k1gNnSc1	Jezulátko
<g/>
,	,	kIx,	,
Emauzský	emauzský	k2eAgInSc1d1	emauzský
klášter	klášter	k1gInSc1	klášter
<g/>
,	,	kIx,	,
palác	palác	k1gInSc1	palác
Kinských	Kinských	k2eAgInSc1d1	Kinských
<g/>
,	,	kIx,	,
Maiselova	Maiselův	k2eAgFnSc1d1	Maiselova
synagoga	synagoga	k1gFnSc1	synagoga
<g/>
,	,	kIx,	,
Pivovar	pivovar	k1gInSc1	pivovar
U	u	k7c2	u
Fleků	Flek	k1gMnPc2	Flek
<g/>
,	,	kIx,	,
Španělská	španělský	k2eAgFnSc1d1	španělská
synagoga	synagoga	k1gFnSc1	synagoga
<g/>
,	,	kIx,	,
pražská	pražský	k2eAgFnSc1d1	Pražská
Loreta	loreta	k1gFnSc1	loreta
<g/>
,	,	kIx,	,
z	z	k7c2	z
novějších	nový	k2eAgFnPc2d2	novější
pak	pak	k6eAd1	pak
Lennonova	Lennonův	k2eAgFnSc1d1	Lennonova
zeď	zeď	k1gFnSc1	zeď
či	či	k8xC	či
Žižkovská	žižkovský	k2eAgFnSc1d1	Žižkovská
televizní	televizní	k2eAgFnSc1d1	televizní
věž	věž	k1gFnSc1	věž
<g/>
.	.	kIx.	.
</s>
<s>
Krom	krom	k7c2	krom
katedrály	katedrála	k1gFnSc2	katedrála
sv.	sv.	kA	sv.
Víta	Vít	k1gMnSc2	Vít
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
turisticky	turisticky	k6eAd1	turisticky
nejatraktivnějším	atraktivní	k2eAgFnPc3d3	nejatraktivnější
částem	část	k1gFnPc3	část
Pražského	pražský	k2eAgInSc2d1	pražský
hradu	hrad	k1gInSc2	hrad
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
ulička	ulička	k1gFnSc1	ulička
<g/>
,	,	kIx,	,
bazilika	bazilika	k1gFnSc1	bazilika
sv.	sv.	kA	sv.
Jiří	Jiří	k1gMnSc2	Jiří
<g/>
,	,	kIx,	,
Královská	královský	k2eAgFnSc1d1	královská
zahrada	zahrada	k1gFnSc1	zahrada
<g/>
,	,	kIx,	,
Letohrádek	letohrádek	k1gInSc1	letohrádek
královny	královna	k1gFnSc2	královna
Anny	Anna	k1gFnSc2	Anna
a	a	k8xC	a
Starý	starý	k2eAgInSc4d1	starý
královský	královský	k2eAgInSc4d1	královský
palác	palác	k1gInSc4	palác
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejznámějšími	známý	k2eAgInPc7d3	nejznámější
mimopražskými	mimopražský	k2eAgInPc7d1	mimopražský
a	a	k8xC	a
mimobrněnskými	mimobrněnský	k2eAgInPc7d1	mimobrněnský
svatostánky	svatostánek	k1gInPc7	svatostánek
jsou	být	k5eAaImIp3nP	být
katedrála	katedrála	k1gFnSc1	katedrála
svatého	svatý	k2eAgMnSc2d1	svatý
Václava	Václav	k1gMnSc2	Václav
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
synagoga	synagoga	k1gFnSc1	synagoga
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
<g/>
,	,	kIx,	,
kostel	kostel	k1gInSc1	kostel
Nejsvětější	nejsvětější	k2eAgFnSc2d1	nejsvětější
Trojice	trojice	k1gFnSc2	trojice
ve	v	k7c6	v
Fulneku	Fulnek	k1gInSc6	Fulnek
<g/>
,	,	kIx,	,
katedrála	katedrála	k1gFnSc1	katedrála
svatého	svatý	k2eAgMnSc2d1	svatý
Bartoloměje	Bartoloměj	k1gMnSc2	Bartoloměj
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
<g/>
,	,	kIx,	,
katedrála	katedrála	k1gFnSc1	katedrála
Božského	božský	k2eAgMnSc2d1	božský
Spasitele	spasitel	k1gMnSc2	spasitel
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
a	a	k8xC	a
katedrála	katedrála	k1gFnSc1	katedrála
svatého	svatý	k2eAgMnSc2d1	svatý
Štěpána	Štěpán	k1gMnSc2	Štěpán
v	v	k7c6	v
Litoměřicích	Litoměřice	k1gInPc6	Litoměřice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
ČR	ČR	kA	ČR
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
700	[number]	k4	700
muzeí	muzeum	k1gNnPc2	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgMnSc7d3	veliký
je	být	k5eAaImIp3nS	být
Národní	národní	k2eAgNnSc1d1	národní
muzeum	muzeum	k1gNnSc1	muzeum
(	(	kIx(	(
<g/>
jeho	jeho	k3xOp3gFnSc7	jeho
součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
Přírodovědecké	přírodovědecký	k2eAgNnSc1d1	Přírodovědecké
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
,	,	kIx,	,
Historické	historický	k2eAgNnSc1d1	historické
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
,	,	kIx,	,
Náprstkovo	Náprstkův	k2eAgNnSc1d1	Náprstkovo
muzeum	muzeum	k1gNnSc1	muzeum
asijských	asijský	k2eAgFnPc2d1	asijská
<g/>
,	,	kIx,	,
afrických	africký	k2eAgFnPc2d1	africká
a	a	k8xC	a
amerických	americký	k2eAgFnPc2d1	americká
kultur	kultura	k1gFnPc2	kultura
a	a	k8xC	a
Muzeum	muzeum	k1gNnSc1	muzeum
české	český	k2eAgFnSc2d1	Česká
hudby	hudba	k1gFnSc2	hudba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Moravské	moravský	k2eAgNnSc1d1	Moravské
zemské	zemský	k2eAgNnSc1d1	zemské
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
,	,	kIx,	,
Židovské	židovský	k2eAgNnSc1d1	Židovské
muzeum	muzeum	k1gNnSc1	muzeum
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
Národní	národní	k2eAgNnSc1d1	národní
technické	technický	k2eAgNnSc1d1	technické
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
,	,	kIx,	,
Uměleckoprůmyslové	uměleckoprůmyslový	k2eAgNnSc1d1	Uměleckoprůmyslové
muzeum	muzeum	k1gNnSc1	muzeum
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
Muzeum	muzeum	k1gNnSc1	muzeum
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
(	(	kIx(	(
<g/>
vlastní	vlastní	k2eAgInSc1d1	vlastní
Langweilův	Langweilův	k2eAgInSc1d1	Langweilův
model	model	k1gInSc1	model
Prahy	Praha	k1gFnSc2	Praha
a	a	k8xC	a
spravuje	spravovat	k5eAaImIp3nS	spravovat
i	i	k9	i
Müllerovu	Müllerův	k2eAgFnSc4d1	Müllerova
vilu	vila	k1gFnSc4	vila
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vojenské	vojenský	k2eAgNnSc1d1	vojenské
technické	technický	k2eAgNnSc1d1	technické
muzeum	muzeum	k1gNnSc1	muzeum
Lešany	Lešana	k1gFnSc2	Lešana
<g/>
,	,	kIx,	,
Muzeum	muzeum	k1gNnSc1	muzeum
umění	umění	k1gNnSc1	umění
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
,	,	kIx,	,
Slovácké	slovácký	k2eAgNnSc1d1	Slovácké
muzeum	muzeum	k1gNnSc1	muzeum
v	v	k7c6	v
Uherském	uherský	k2eAgNnSc6d1	Uherské
Hradišti	Hradiště	k1gNnSc6	Hradiště
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejnavštěvovanějším	navštěvovaný	k2eAgMnPc3d3	nejnavštěvovanější
patří	patřit	k5eAaImIp3nS	patřit
též	též	k9	též
Hornické	hornický	k2eAgNnSc1d1	Hornické
muzeum	muzeum	k1gNnSc1	muzeum
v	v	k7c6	v
Příbrami	Příbram	k1gFnSc6	Příbram
a	a	k8xC	a
České	český	k2eAgNnSc1d1	české
muzeum	muzeum	k1gNnSc1	muzeum
stříbra	stříbro	k1gNnSc2	stříbro
v	v	k7c6	v
Kutné	kutný	k2eAgFnSc6d1	Kutná
Hoře	hora	k1gFnSc6	hora
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
přírodních	přírodní	k2eAgFnPc2d1	přírodní
památek	památka	k1gFnPc2	památka
to	ten	k3xDgNnSc1	ten
jsou	být	k5eAaImIp3nP	být
Punkevní	punkevní	k2eAgFnPc1d1	punkevní
jeskyně	jeskyně	k1gFnPc1	jeskyně
a	a	k8xC	a
propast	propast	k1gFnSc1	propast
Macocha	Macocha	k1gFnSc1	Macocha
<g/>
,	,	kIx,	,
soutěsky	soutěska	k1gFnPc1	soutěska
Kamenice	Kamenice	k1gFnSc2	Kamenice
<g/>
,	,	kIx,	,
Pravčická	Pravčický	k2eAgFnSc1d1	Pravčická
brána	brána	k1gFnSc1	brána
<g/>
,	,	kIx,	,
Koněpruské	Koněpruský	k2eAgFnSc2d1	Koněpruská
jeskyně	jeskyně	k1gFnSc2	jeskyně
<g/>
,	,	kIx,	,
Bozkovské	Bozkovský	k2eAgFnSc2d1	Bozkovská
dolomitové	dolomitový	k2eAgFnSc2d1	dolomitová
jeskyně	jeskyně	k1gFnSc2	jeskyně
či	či	k8xC	či
Jetřichovické	Jetřichovický	k2eAgFnSc2d1	Jetřichovická
vyhlídky	vyhlídka	k1gFnSc2	vyhlídka
<g/>
.	.	kIx.	.
<g/>
Mimořádné	mimořádný	k2eAgNnSc4d1	mimořádné
postavení	postavení	k1gNnSc4	postavení
mají	mít	k5eAaImIp3nP	mít
kulturní	kulturní	k2eAgFnPc4d1	kulturní
památky	památka	k1gFnPc4	památka
zapsané	zapsaný	k2eAgFnPc4d1	zapsaná
na	na	k7c4	na
seznam	seznam	k1gInSc4	seznam
světového	světový	k2eAgNnSc2d1	světové
kulturního	kulturní	k2eAgNnSc2d1	kulturní
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
již	již	k6eAd1	již
uvedených	uvedený	k2eAgInPc2d1	uvedený
(	(	kIx(	(
<g/>
Kroměříž	Kroměříž	k1gFnSc1	Kroměříž
<g/>
,	,	kIx,	,
Český	český	k2eAgInSc1d1	český
Krumlov	Krumlov	k1gInSc1	Krumlov
<g/>
,	,	kIx,	,
centrum	centrum	k1gNnSc1	centrum
Prahy	Praha	k1gFnSc2	Praha
<g/>
</s>
<s>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
vila	vít	k5eAaImAgFnS	vít
Tugendhat	Tugendhat	k1gFnSc1	Tugendhat
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
vesnice	vesnice	k1gFnSc1	vesnice
Holašovice	Holašovice	k1gFnSc1	Holašovice
<g/>
,	,	kIx,	,
historické	historický	k2eAgNnSc1d1	historické
jádro	jádro	k1gNnSc1	jádro
Kutné	kutný	k2eAgFnSc2d1	Kutná
Hory	hora	k1gFnSc2	hora
<g/>
,	,	kIx,	,
Lednicko-valtický	lednickoaltický	k2eAgInSc1d1	lednicko-valtický
areál	areál	k1gInSc1	areál
<g/>
,	,	kIx,	,
zámek	zámek	k1gInSc1	zámek
a	a	k8xC	a
zámecký	zámecký	k2eAgInSc1d1	zámecký
areál	areál	k1gInSc1	areál
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
,	,	kIx,	,
sloup	sloup	k1gInSc1	sloup
Nejsvětější	nejsvětější	k2eAgFnSc2d1	nejsvětější
Trojice	trojice	k1gFnSc2	trojice
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
,	,	kIx,	,
historické	historický	k2eAgNnSc1d1	historické
centrum	centrum	k1gNnSc1	centrum
Třebíče	Třebíč	k1gFnSc2	Třebíč
(	(	kIx(	(
<g/>
Třebíčská	třebíčský	k2eAgFnSc1d1	Třebíčská
židovská	židovský	k2eAgFnSc1d1	židovská
čtvrť	čtvrť	k1gFnSc1	čtvrť
a	a	k8xC	a
Bazilika	bazilika	k1gFnSc1	bazilika
svatého	svatý	k2eAgMnSc2d1	svatý
Prokopa	Prokop	k1gMnSc2	Prokop
<g/>
)	)	kIx)	)
a	a	k8xC	a
poutní	poutní	k2eAgInSc1d1	poutní
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
na	na	k7c6	na
Zelené	Zelené	k2eAgFnSc6d1	Zelené
hoře	hora	k1gFnSc6	hora
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
seznam	seznam	k1gInSc4	seznam
nehmotného	hmotný	k2eNgNnSc2d1	nehmotné
světového	světový	k2eAgNnSc2d1	světové
kulturního	kulturní	k2eAgNnSc2d1	kulturní
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
byly	být	k5eAaImAgInP	být
zapsány	zapsat	k5eAaPmNgInP	zapsat
Jízda	jízda	k1gFnSc1	jízda
králů	král	k1gMnPc2	král
na	na	k7c6	na
Slovácku	Slovácko	k1gNnSc6	Slovácko
a	a	k8xC	a
na	na	k7c6	na
Hané	Haná	k1gFnSc6	Haná
<g/>
,	,	kIx,	,
sokolnictví	sokolnictví	k1gNnSc6	sokolnictví
<g/>
,	,	kIx,	,
masopustní	masopustní	k2eAgInPc1d1	masopustní
průvody	průvod	k1gInPc1	průvod
s	s	k7c7	s
maskami	maska	k1gFnPc7	maska
na	na	k7c4	na
Hlinecku	Hlinecka	k1gFnSc4	Hlinecka
<g/>
,	,	kIx,	,
slovácký	slovácký	k2eAgInSc4d1	slovácký
verbuňk	verbuňk	k1gInSc4	verbuňk
a	a	k8xC	a
nejnověji	nově	k6eAd3	nově
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
české	český	k2eAgNnSc1d1	české
loutkářství	loutkářství	k1gNnSc1	loutkářství
<g/>
.	.	kIx.	.
<g/>
Některé	některý	k3yIgFnPc1	některý
lokality	lokalita	k1gFnPc1	lokalita
mají	mít	k5eAaImIp3nP	mít
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
význam	význam	k1gInSc4	význam
pro	pro	k7c4	pro
<g />
.	.	kIx.	.
</s>
<s>
spjatost	spjatost	k1gFnSc1	spjatost
s	s	k7c7	s
českými	český	k2eAgFnPc7d1	Česká
dějinami	dějiny	k1gFnPc7	dějiny
nebo	nebo	k8xC	nebo
mýty	mýtus	k1gInPc4	mýtus
–	–	k?	–
hora	hora	k1gFnSc1	hora
Říp	Říp	k1gInSc1	Říp
<g/>
,	,	kIx,	,
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
(	(	kIx(	(
<g/>
zejm.	zejm.	k?	zejm.
Vyšehradský	vyšehradský	k2eAgInSc4d1	vyšehradský
hřbitov	hřbitov	k1gInSc4	hřbitov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
Antonín	Antonín	k1gMnSc1	Antonín
Dvořák	Dvořák	k1gMnSc1	Dvořák
i	i	k8xC	i
Bedřich	Bedřich	k1gMnSc1	Bedřich
Smetana	Smetana	k1gMnSc1	Smetana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hora	hora	k1gFnSc1	hora
Blaník	Blaník	k1gInSc1	Blaník
<g/>
,	,	kIx,	,
Velehrad	Velehrad	k1gInSc1	Velehrad
spojený	spojený	k2eAgInSc1d1	spojený
s	s	k7c7	s
památkou	památka	k1gFnSc7	památka
cyrilometodějskou	cyrilometodějský	k2eAgFnSc7d1	Cyrilometodějská
a	a	k8xC	a
velkomoravskou	velkomoravský	k2eAgFnSc4d1	Velkomoravská
<g/>
,	,	kIx,	,
kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Václava	Václav	k1gMnSc2	Václav
ve	v	k7c6	v
Staré	Staré	k2eAgFnSc6d1	Staré
Boleslavi	Boleslaev	k1gFnSc6	Boleslaev
spjatý	spjatý	k2eAgInSc1d1	spjatý
s	s	k7c7	s
legendou	legenda	k1gFnSc7	legenda
o	o	k7c4	o
zabití	zabití	k1gNnSc4	zabití
knížete	kníže	k1gMnSc2	kníže
Václava	Václav	k1gMnSc2	Václav
<g/>
,	,	kIx,	,
pravoslavný	pravoslavný	k2eAgInSc1d1	pravoslavný
chrám	chrám	k1gInSc1	chrám
svatých	svatý	k1gMnPc2	svatý
Cyrila	Cyril	k1gMnSc2	Cyril
a	a	k8xC	a
Metoděje	Metoděj	k1gMnSc2	Metoděj
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
zahynuli	zahynout	k5eAaPmAgMnP	zahynout
strůjci	strůjce	k1gMnPc1	strůjce
atentátu	atentát	k1gInSc2	atentát
na	na	k7c4	na
Heydricha	Heydrich	k1gMnSc4	Heydrich
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
oblíbeným	oblíbený	k2eAgFnPc3d1	oblíbená
turistickým	turistický	k2eAgFnPc3d1	turistická
akcím	akce	k1gFnPc3	akce
patří	patřit	k5eAaImIp3nS	patřit
Muzejní	muzejní	k2eAgFnSc1d1	muzejní
noc	noc	k1gFnSc1	noc
<g/>
,	,	kIx,	,
Noc	noc	k1gFnSc1	noc
kostelů	kostel	k1gInPc2	kostel
<g/>
,	,	kIx,	,
Hradozámecká	Hradozámecký	k2eAgFnSc1d1	Hradozámecká
noc	noc	k1gFnSc1	noc
<g/>
,	,	kIx,	,
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
nejslavnějších	slavný	k2eAgFnPc2d3	nejslavnější
bitev	bitva	k1gFnPc2	bitva
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
se	se	k3xPyFc4	se
odehrály	odehrát	k5eAaPmAgFnP	odehrát
na	na	k7c6	na
českém	český	k2eAgNnSc6d1	české
území	území	k1gNnSc6	území
(	(	kIx(	(
<g/>
bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Slavkova	Slavkov	k1gInSc2	Slavkov
<g/>
,	,	kIx,	,
bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Hradce	Hradec	k1gInSc2	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
,	,	kIx,	,
bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Kolína	Kolín	k1gInSc2	Kolín
<g/>
,	,	kIx,	,
obléhání	obléhání	k1gNnSc1	obléhání
Brna	Brno	k1gNnSc2	Brno
Švédy	švéda	k1gFnSc2	švéda
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Dny	den	k1gInPc1	den
NATO	NATO	kA	NATO
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
či	či	k8xC	či
armádní	armádní	k2eAgNnPc4d1	armádní
Bahna	bahno	k1gNnPc4	bahno
<g/>
.	.	kIx.	.
</s>
<s>
Krom	krom	k7c2	krom
toho	ten	k3xDgNnSc2	ten
řada	řada	k1gFnSc1	řada
festivalů	festival	k1gInPc2	festival
a	a	k8xC	a
veletrhů	veletrh	k1gInPc2	veletrh
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
příslušný	příslušný	k2eAgInSc1d1	příslušný
oddíl	oddíl	k1gInSc1	oddíl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tradičními	tradiční	k2eAgInPc7d1	tradiční
turistickými	turistický	k2eAgInPc7d1	turistický
magnety	magnet	k1gInPc7	magnet
bývala	bývat	k5eAaImAgNnP	bývat
lázeňská	lázeňský	k2eAgNnPc1d1	lázeňské
města	město	k1gNnPc1	město
jako	jako	k8xC	jako
Karlovy	Karlův	k2eAgInPc1d1	Karlův
Vary	Vary	k1gInPc1	Vary
<g/>
,	,	kIx,	,
Mariánské	mariánský	k2eAgFnPc1d1	Mariánská
Lázně	lázeň	k1gFnPc1	lázeň
a	a	k8xC	a
Františkovy	Františkův	k2eAgFnPc1d1	Františkova
Lázně	lázeň	k1gFnPc1	lázeň
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
lázeňství	lázeňství	k1gNnSc1	lázeňství
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
do	do	k7c2	do
krize	krize	k1gFnSc2	krize
<g/>
.	.	kIx.	.
</s>
<s>
Karlovy	Karlův	k2eAgInPc1d1	Karlův
Vary	Vary	k1gInPc1	Vary
si	se	k3xPyFc3	se
však	však	k8xC	však
zachovávají	zachovávat	k5eAaImIp3nP	zachovávat
atraktivitu	atraktivita	k1gFnSc4	atraktivita
kupříkladu	kupříkladu	k6eAd1	kupříkladu
i	i	k9	i
díky	díky	k7c3	díky
každoročnímu	každoroční	k2eAgInSc3d1	každoroční
mezinárodnímu	mezinárodní	k2eAgInSc3d1	mezinárodní
filmovému	filmový	k2eAgInSc3d1	filmový
festivalu	festival	k1gInSc3	festival
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
filmová	filmový	k2eAgFnSc1d1	filmová
přehlídka	přehlídka	k1gFnSc1	přehlídka
založená	založený	k2eAgFnSc1d1	založená
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
největší	veliký	k2eAgFnSc7d3	veliký
kulturní	kulturní	k2eAgFnSc7d1	kulturní
akcí	akce	k1gFnSc7	akce
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Významné	významný	k2eAgFnPc1d1	významná
rekreační	rekreační	k2eAgFnPc1d1	rekreační
oblasti	oblast	k1gFnPc1	oblast
v	v	k7c6	v
ČR	ČR	kA	ČR
jsou	být	k5eAaImIp3nP	být
České	český	k2eAgNnSc1d1	české
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
,	,	kIx,	,
Český	český	k2eAgInSc1d1	český
ráj	ráj	k1gInSc1	ráj
<g/>
,	,	kIx,	,
Krkonoše	Krkonoše	k1gFnPc1	Krkonoše
a	a	k8xC	a
Šumava	Šumava	k1gFnSc1	Šumava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
dle	dle	k7c2	dle
Českého	český	k2eAgInSc2d1	český
statistického	statistický	k2eAgInSc2d1	statistický
úřadu	úřad	k1gInSc2	úřad
10	[number]	k4	10
578	[number]	k4	578
820	[number]	k4	820
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
úmrtnost	úmrtnost	k1gFnSc1	úmrtnost
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
porodnost	porodnost	k1gFnSc1	porodnost
–	–	k?	–
příčinou	příčina	k1gFnSc7	příčina
je	být	k5eAaImIp3nS	být
migrace	migrace	k1gFnSc1	migrace
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
z	z	k7c2	z
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
a	a	k8xC	a
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Migrace	migrace	k1gFnSc1	migrace
do	do	k7c2	do
ČR	ČR	kA	ČR
též	též	k9	též
převyšuje	převyšovat	k5eAaImIp3nS	převyšovat
počet	počet	k1gInSc1	počet
odchodů	odchod	k1gInPc2	odchod
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
se	se	k3xPyFc4	se
přistěhovalo	přistěhovat	k5eAaPmAgNnS	přistěhovat
34	[number]	k4	34
900	[number]	k4	900
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
se	se	k3xPyFc4	se
vystěhovalo	vystěhovat	k5eAaPmAgNnS	vystěhovat
18	[number]	k4	18
900	[number]	k4	900
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
věk	věk	k1gInSc1	věk
činil	činit	k5eAaImAgInS	činit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
42	[number]	k4	42
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Nejlidnatějším	lidnatý	k2eAgInSc7d3	nejlidnatější
krajem	kraj	k1gInSc7	kraj
je	být	k5eAaImIp3nS	být
Středočeský	středočeský	k2eAgInSc1d1	středočeský
<g/>
,	,	kIx,	,
s	s	k7c7	s
1	[number]	k4	1
326	[number]	k4	326
876	[number]	k4	876
obyvateli	obyvatel	k1gMnPc7	obyvatel
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jediným	jediný	k2eAgInSc7d1	jediný
krajem	kraj	k1gInSc7	kraj
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žije	žít	k5eAaImIp3nS	žít
více	hodně	k6eAd2	hodně
lidí	člověk	k1gMnPc2	člověk
než	než	k8xS	než
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
má	mít	k5eAaImIp3nS	mít
1	[number]	k4	1
267	[number]	k4	267
449	[number]	k4	449
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vývoj	vývoj	k1gInSc1	vývoj
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Etnické	etnický	k2eAgNnSc4d1	etnické
složení	složení	k1gNnSc4	složení
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
posledním	poslední	k2eAgNnSc6d1	poslední
sčítání	sčítání	k1gNnSc6	sčítání
lidu	lid	k1gInSc2	lid
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
se	s	k7c7	s
63,7	[number]	k4	63,7
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
Česka	Česko	k1gNnSc2	Česko
přihlásilo	přihlásit	k5eAaPmAgNnS	přihlásit
k	k	k7c3	k
české	český	k2eAgFnSc3d1	Česká
národnosti	národnost	k1gFnSc3	národnost
(	(	kIx(	(
<g/>
86	[number]	k4	86
%	%	kIx~	%
z	z	k7c2	z
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
k	k	k7c3	k
nějaké	nějaký	k3yIgFnSc3	nějaký
národnosti	národnost	k1gFnSc3	národnost
přihlásili	přihlásit	k5eAaPmAgMnP	přihlásit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zcela	zcela	k6eAd1	zcela
převažuje	převažovat	k5eAaImIp3nS	převažovat
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
okresech	okres	k1gInPc6	okres
Česka	Česko	k1gNnSc2	Česko
<g/>
,	,	kIx,	,
4,9	[number]	k4	4,9
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
deklarovalo	deklarovat	k5eAaBmAgNnS	deklarovat
národnost	národnost	k1gFnSc4	národnost
moravskou	moravský	k2eAgFnSc4d1	Moravská
a	a	k8xC	a
0,1	[number]	k4	0,1
%	%	kIx~	%
národnost	národnost	k1gFnSc4	národnost
slezskou	slezský	k2eAgFnSc4d1	Slezská
<g/>
,	,	kIx,	,
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
případech	případ	k1gInPc6	případ
rovněž	rovněž	k9	rovněž
hovořící	hovořící	k2eAgMnPc1d1	hovořící
převážně	převážně	k6eAd1	převážně
česky	česky	k6eAd1	česky
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
názoru	názor	k1gInSc2	názor
Českého	český	k2eAgInSc2d1	český
statistického	statistický	k2eAgInSc2d1	statistický
úřadu	úřad	k1gInSc2	úřad
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
důsledek	důsledek	k1gInSc4	důsledek
rozdělení	rozdělení	k1gNnSc2	rozdělení
české	český	k2eAgFnSc2d1	Česká
národnosti	národnost	k1gFnSc2	národnost
<g/>
,	,	kIx,	,
před	před	k7c7	před
sčítáním	sčítání	k1gNnSc7	sčítání
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
ještě	ještě	k6eAd1	ještě
jednotné	jednotný	k2eAgInPc1d1	jednotný
<g/>
,	,	kIx,	,
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
medializace	medializace	k1gFnSc2	medializace
moravské	moravský	k2eAgFnSc2d1	Moravská
národnostní	národnostní	k2eAgFnSc2d1	národnostní
problematiky	problematika	k1gFnSc2	problematika
a	a	k8xC	a
tedy	tedy	k9	tedy
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
uměle	uměle	k6eAd1	uměle
<g/>
;	;	kIx,	;
politická	politický	k2eAgFnSc1d1	politická
strana	strana	k1gFnSc1	strana
Moravané	Moravan	k1gMnPc1	Moravan
tento	tento	k3xDgInSc4	tento
stav	stav	k1gInSc4	stav
naopak	naopak	k6eAd1	naopak
hodnotí	hodnotit	k5eAaImIp3nS	hodnotit
jako	jako	k9	jako
projev	projev	k1gInSc4	projev
spontánního	spontánní	k2eAgNnSc2d1	spontánní
národního	národní	k2eAgNnSc2d1	národní
obrození	obrození	k1gNnSc2	obrození
<g/>
.	.	kIx.	.
</s>
<s>
Faktem	fakt	k1gInSc7	fakt
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
moravskou	moravský	k2eAgFnSc4d1	Moravská
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
slezskou	slezský	k2eAgFnSc4d1	Slezská
a	a	k8xC	a
po	po	k7c4	po
určitou	určitý	k2eAgFnSc4d1	určitá
dobu	doba	k1gFnSc4	doba
i	i	k8xC	i
českou	český	k2eAgFnSc4d1	Česká
a	a	k8xC	a
slovenskou	slovenský	k2eAgFnSc4d1	slovenská
národnost	národnost	k1gFnSc4	národnost
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
různých	různý	k2eAgNnPc6d1	různé
obdobích	období	k1gNnPc6	období
existence	existence	k1gFnSc1	existence
Československa	Československo	k1gNnSc2	Československo
možno	možno	k6eAd1	možno
deklarovat	deklarovat	k5eAaBmF	deklarovat
jen	jen	k9	jen
omezeně	omezeně	k6eAd1	omezeně
(	(	kIx(	(
<g/>
skupina	skupina	k1gFnSc1	skupina
nebyla	být	k5eNaImAgFnS	být
uváděna	uvádět	k5eAaImNgFnS	uvádět
v	v	k7c6	v
koncových	koncový	k2eAgFnPc6d1	koncová
statistikách	statistika	k1gFnPc6	statistika
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
vůbec	vůbec	k9	vůbec
ne	ne	k9	ne
(	(	kIx(	(
<g/>
ke	k	k7c3	k
skupině	skupina	k1gFnSc3	skupina
nebylo	být	k5eNaImAgNnS	být
možno	možno	k6eAd1	možno
se	se	k3xPyFc4	se
přihlásit	přihlásit	k5eAaPmF	přihlásit
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
a	a	k8xC	a
teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
rozpadem	rozpad	k1gInSc7	rozpad
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
poprvé	poprvé	k6eAd1	poprvé
plně	plně	k6eAd1	plně
přihlédnuto	přihlédnut	k2eAgNnSc1d1	přihlédnuto
ke	k	k7c3	k
každé	každý	k3xTgFnSc3	každý
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
čtyř	čtyři	k4xCgFnPc2	čtyři
národností	národnost	k1gFnPc2	národnost
jednotlivě	jednotlivě	k6eAd1	jednotlivě
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
26	[number]	k4	26
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
Česka	Česko	k1gNnSc2	Česko
využilo	využít	k5eAaPmAgNnS	využít
při	při	k7c6	při
sčítání	sčítání	k1gNnSc6	sčítání
lidu	lid	k1gInSc2	lid
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
možnost	možnost	k1gFnSc4	možnost
nevyplňovat	vyplňovat	k5eNaImF	vyplňovat
ve	v	k7c6	v
sčítacích	sčítací	k2eAgInPc6d1	sčítací
formulářích	formulář	k1gInPc6	formulář
dobrovolnou	dobrovolný	k2eAgFnSc4d1	dobrovolná
kolonku	kolonka	k1gFnSc4	kolonka
národnosti	národnost	k1gFnSc2	národnost
a	a	k8xC	a
ponechalo	ponechat	k5eAaPmAgNnS	ponechat
ji	on	k3xPp3gFnSc4	on
prázdnou	prázdná	k1gFnSc4	prázdná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Náboženství	náboženství	k1gNnSc2	náboženství
===	===	k?	===
</s>
</p>
<p>
<s>
Česko	Česko	k1gNnSc1	Česko
má	mít	k5eAaImIp3nS	mít
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejméně	málo	k6eAd3	málo
náboženských	náboženský	k2eAgFnPc2d1	náboženská
populací	populace	k1gFnPc2	populace
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průzkumech	průzkum	k1gInPc6	průzkum
projektu	projekt	k1gInSc2	projekt
Eurobarometr	Eurobarometr	k1gInSc4	Eurobarometr
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
odpovědělo	odpovědět	k5eAaPmAgNnS	odpovědět
19	[number]	k4	19
%	%	kIx~	%
tázaných	tázaný	k2eAgFnPc2d1	tázaná
<g/>
,	,	kIx,	,
že	že	k8xS	že
věří	věřit	k5eAaImIp3nS	věřit
v	v	k7c4	v
Boha	bůh	k1gMnSc4	bůh
<g/>
,	,	kIx,	,
50	[number]	k4	50
%	%	kIx~	%
věří	věřit	k5eAaImIp3nS	věřit
v	v	k7c4	v
nějakou	nějaký	k3yIgFnSc4	nějaký
životní	životní	k2eAgFnSc4d1	životní
či	či	k8xC	či
duchovní	duchovní	k2eAgFnSc4d1	duchovní
sílu	síla	k1gFnSc4	síla
a	a	k8xC	a
30	[number]	k4	30
%	%	kIx~	%
nevěří	věřit	k5eNaImIp3nS	věřit
v	v	k7c4	v
nic	nic	k3yNnSc4	nic
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
posledního	poslední	k2eAgNnSc2d1	poslední
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
je	být	k5eAaImIp3nS	být
3,6	[number]	k4	3,6
miliónu	milión	k4xCgInSc2	milión
obyvatel	obyvatel	k1gMnPc2	obyvatel
bez	bez	k7c2	bez
vyznání	vyznání	k1gNnSc2	vyznání
nebo	nebo	k8xC	nebo
bez	bez	k7c2	bez
náboženské	náboženský	k2eAgFnSc2d1	náboženská
víry	víra	k1gFnSc2	víra
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
34,2	[number]	k4	34,2
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
různým	různý	k2eAgFnPc3d1	různá
církvím	církev	k1gFnPc3	církev
a	a	k8xC	a
náboženských	náboženský	k2eAgFnPc6d1	náboženská
společnostem	společnost	k1gFnPc3	společnost
se	se	k3xPyFc4	se
přihlásilo	přihlásit	k5eAaPmAgNnS	přihlásit
necelých	celý	k2eNgInPc2d1	necelý
1,5	[number]	k4	1,5
miliónů	milión	k4xCgInPc2	milión
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
13,9	[number]	k4	13,9
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
věřící	věřící	k2eAgMnPc1d1	věřící
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nehlásící	hlásící	k2eNgMnSc1d1	nehlásící
se	se	k3xPyFc4	se
k	k	k7c3	k
žádné	žádný	k3yNgFnSc3	žádný
církvi	církev	k1gFnSc3	církev
ani	ani	k8xC	ani
náboženské	náboženský	k2eAgFnSc3d1	náboženská
společnosti	společnost	k1gFnSc3	společnost
se	se	k3xPyFc4	se
označilo	označit	k5eAaPmAgNnS	označit
707	[number]	k4	707
tisíc	tisíc	k4xCgInPc2	tisíc
osob	osoba	k1gFnPc2	osoba
(	(	kIx(	(
<g/>
6,7	[number]	k4	6,7
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
se	se	k3xPyFc4	se
tak	tak	k9	tak
k	k	k7c3	k
nějaké	nějaký	k3yIgFnSc3	nějaký
víře	víra	k1gFnSc3	víra
či	či	k8xC	či
vyznání	vyznání	k1gNnSc1	vyznání
přihlásilo	přihlásit	k5eAaPmAgNnS	přihlásit
2,1	[number]	k4	2,1
miliónů	milión	k4xCgInPc2	milión
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
20,6	[number]	k4	20,6
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
Česka	Česko	k1gNnSc2	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
4,7	[number]	k4	4,7
miliónu	milión	k4xCgInSc2	milión
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
45,2	[number]	k4	45,2
%	%	kIx~	%
<g/>
)	)	kIx)	)
tuto	tento	k3xDgFnSc4	tento
dobrovolnou	dobrovolný	k2eAgFnSc4d1	dobrovolná
kolonku	kolonka	k1gFnSc4	kolonka
ve	v	k7c6	v
sčítacích	sčítací	k2eAgInPc6d1	sčítací
formulářích	formulář	k1gInPc6	formulář
nevyplnilo	vyplnit	k5eNaPmAgNnS	vyplnit
<g/>
.	.	kIx.	.
</s>
<s>
Nejpočetnější	početní	k2eAgNnSc4d3	nejpočetnější
náboženství	náboženství	k1gNnSc4	náboženství
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
je	být	k5eAaImIp3nS	být
křesťanství	křesťanství	k1gNnSc4	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc7d3	veliký
náboženskou	náboženský	k2eAgFnSc7d1	náboženská
skupinou	skupina	k1gFnSc7	skupina
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
církev	církev	k1gFnSc1	církev
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
1,08	[number]	k4	1,08
miliónu	milión	k4xCgInSc2	milión
věřících	věřící	k1gMnPc2	věřící
(	(	kIx(	(
<g/>
10,4	[number]	k4	10,4
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
pokles	pokles	k1gInSc4	pokles
oproti	oproti	k7c3	oproti
roku	rok	k1gInSc3	rok
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
hlásilo	hlásit	k5eAaImAgNnS	hlásit
celkem	celek	k1gInSc7	celek
2,7	[number]	k4	2,7
miliónu	milión	k4xCgInSc2	milión
věřících	věřící	k1gMnPc2	věřící
(	(	kIx(	(
<g/>
26,8	[number]	k4	26,8
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Následovaly	následovat	k5eAaImAgFnP	následovat
velké	velká	k1gFnPc1	velká
protestantské	protestantský	k2eAgFnSc2d1	protestantská
církve	církev	k1gFnSc2	církev
Českobratrská	českobratrský	k2eAgFnSc1d1	Českobratrská
církev	církev	k1gFnSc1	církev
evangelická	evangelický	k2eAgFnSc1d1	evangelická
<g/>
,	,	kIx,	,
s	s	k7c7	s
52	[number]	k4	52
tisíci	tisíc	k4xCgInPc7	tisíc
členů	člen	k1gMnPc2	člen
(	(	kIx(	(
<g/>
0,49	[number]	k4	0,49
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
Církev	církev	k1gFnSc1	církev
československá	československý	k2eAgFnSc1d1	Československá
husitská	husitský	k2eAgFnSc1d1	husitská
<g/>
,	,	kIx,	,
s	s	k7c7	s
39	[number]	k4	39
tisíci	tisíc	k4xCgInPc7	tisíc
členů	člen	k1gMnPc2	člen
(	(	kIx(	(
<g/>
0,37	[number]	k4	0,37
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vyšší	vysoký	k2eAgInSc4d2	vyšší
podíl	podíl	k1gInSc4	podíl
věřících	věřící	k1gMnPc2	věřící
měla	mít	k5eAaImAgFnS	mít
ještě	ještě	k9	ještě
Pravoslavná	pravoslavný	k2eAgFnSc1d1	pravoslavná
církev	církev	k1gFnSc1	církev
<g/>
,	,	kIx,	,
k	k	k7c3	k
níž	jenž	k3xRgFnSc3	jenž
se	se	k3xPyFc4	se
přihlásilo	přihlásit	k5eAaPmAgNnS	přihlásit
21	[number]	k4	21
tisíc	tisíc	k4xCgInPc2	tisíc
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
příznivci	příznivec	k1gMnPc1	příznivec
jediismu	jediismus	k1gInSc2	jediismus
s	s	k7c7	s
15	[number]	k4	15
tisíci	tisíc	k4xCgInPc7	tisíc
<g/>
,	,	kIx,	,
Svědkové	svědek	k1gMnPc1	svědek
Jehovovi	Jehovův	k2eAgMnPc1d1	Jehovův
s	s	k7c7	s
13	[number]	k4	13
tisíci	tisíc	k4xCgInPc7	tisíc
<g/>
,	,	kIx,	,
Církev	církev	k1gFnSc1	církev
bratrská	bratrský	k2eAgFnSc1d1	bratrská
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
Křesťanská	křesťanský	k2eAgNnPc4d1	křesťanské
společenství	společenství	k1gNnPc4	společenství
<g/>
,	,	kIx,	,
obě	dva	k4xCgFnPc1	dva
s	s	k7c7	s
10	[number]	k4	10
tisíci	tisíc	k4xCgInPc7	tisíc
věřícími	věřící	k1gMnPc7	věřící
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
vyznavačů	vyznavač	k1gMnPc2	vyznavač
judaismu	judaismus	k1gInSc2	judaismus
byl	být	k5eAaImAgInS	být
1	[number]	k4	1
500	[number]	k4	500
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
k	k	k7c3	k
islámu	islám	k1gInSc3	islám
se	se	k3xPyFc4	se
hlásilo	hlásit	k5eAaImAgNnS	hlásit
téměř	téměř	k6eAd1	téměř
3	[number]	k4	3
500	[number]	k4	500
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
různým	různý	k2eAgInPc3d1	různý
odvětvím	odvětví	k1gNnSc7	odvětví
buddhismu	buddhismus	k1gInSc2	buddhismus
přes	přes	k7c4	přes
6	[number]	k4	6
100	[number]	k4	100
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
ateismu	ateismus	k1gInSc3	ateismus
se	se	k3xPyFc4	se
přihlásilo	přihlásit	k5eAaPmAgNnS	přihlásit
1	[number]	k4	1
075	[number]	k4	075
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
pohanství	pohanství	k1gNnSc1	pohanství
deklarovalo	deklarovat	k5eAaBmAgNnS	deklarovat
863	[number]	k4	863
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podíl	podíl	k1gInSc1	podíl
deklarovaných	deklarovaný	k2eAgMnPc2d1	deklarovaný
věřících	věřící	k1gMnPc2	věřící
se	se	k3xPyFc4	se
od	od	k7c2	od
předešlého	předešlý	k2eAgNnSc2d1	předešlé
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
značně	značně	k6eAd1	značně
snížil	snížit	k5eAaPmAgInS	snížit
<g/>
.	.	kIx.	.
</s>
<s>
Výrazně	výrazně	k6eAd1	výrazně
klesl	klesnout	k5eAaPmAgInS	klesnout
i	i	k9	i
počet	počet	k1gInSc1	počet
osob	osoba	k1gFnPc2	osoba
nedeklarujících	deklarující	k2eNgFnPc2d1	deklarující
žádné	žádný	k3yNgNnSc1	žádný
náboženství	náboženství	k1gNnSc1	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
počet	počet	k1gInSc1	počet
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
rozhodly	rozhodnout	k5eAaPmAgFnP	rozhodnout
na	na	k7c4	na
dobrovolnou	dobrovolný	k2eAgFnSc4d1	dobrovolná
otázku	otázka	k1gFnSc4	otázka
neodpovídat	odpovídat	k5eNaImF	odpovídat
<g/>
.	.	kIx.	.
<g/>
Geograficky	geograficky	k6eAd1	geograficky
je	být	k5eAaImIp3nS	být
vyšší	vysoký	k2eAgFnSc1d2	vyšší
koncentrace	koncentrace	k1gFnSc1	koncentrace
věřících	věřící	k1gMnPc2	věřící
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
a	a	k8xC	a
východní	východní	k2eAgFnSc6d1	východní
Moravě	Morava	k1gFnSc6	Morava
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
severní	severní	k2eAgFnPc1d1	severní
a	a	k8xC	a
severozápadní	severozápadní	k2eAgFnPc1d1	severozápadní
Čechy	Čechy	k1gFnPc1	Čechy
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
vyšší	vysoký	k2eAgInSc4d2	vyšší
podíl	podíl	k1gInSc4	podíl
osob	osoba	k1gFnPc2	osoba
bez	bez	k7c2	bez
vyznání	vyznání	k1gNnSc2	vyznání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podíl	podíl	k1gInSc1	podíl
věřících	věřící	k2eAgMnPc2d1	věřící
obyvatel	obyvatel	k1gMnPc2	obyvatel
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Historicky	historicky	k6eAd1	historicky
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
v	v	k7c6	v
náboženském	náboženský	k2eAgInSc6d1	náboženský
vývoji	vývoj	k1gInSc6	vývoj
sehrálo	sehrát	k5eAaPmAgNnS	sehrát
husitství	husitství	k1gNnSc1	husitství
<g/>
,	,	kIx,	,
utrakvismus	utrakvismus	k1gInSc1	utrakvismus
<g/>
,	,	kIx,	,
Jednota	jednota	k1gFnSc1	jednota
Bratrská	bratrský	k2eAgFnSc1d1	bratrská
(	(	kIx(	(
<g/>
a	a	k8xC	a
její	její	k3xOp3gFnSc4	její
součást	součást	k1gFnSc4	součást
Moravští	moravský	k2eAgMnPc1d1	moravský
bratři	bratr	k1gMnPc1	bratr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
reformace	reformace	k1gFnSc1	reformace
sehrála	sehrát	k5eAaPmAgFnS	sehrát
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
i	i	k9	i
v	v	k7c6	v
národním	národní	k2eAgNnSc6d1	národní
obrození	obrození	k1gNnSc6	obrození
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
moderní	moderní	k2eAgInSc1d1	moderní
český	český	k2eAgInSc1d1	český
národ	národ	k1gInSc1	národ
se	s	k7c7	s
protestantským	protestantský	k2eAgInSc7d1	protestantský
nakonec	nakonec	k6eAd1	nakonec
nestal	stát	k5eNaPmAgInS	stát
<g/>
.	.	kIx.	.
</s>
<s>
Katolická	katolický	k2eAgFnSc1d1	katolická
a	a	k8xC	a
pravoslavná	pravoslavný	k2eAgFnSc1d1	pravoslavná
církev	církev	k1gFnSc1	církev
uznávají	uznávat	k5eAaImIp3nP	uznávat
svaté	svatý	k2eAgInPc1d1	svatý
<g/>
,	,	kIx,	,
z	z	k7c2	z
českých	český	k2eAgFnPc2d1	Česká
osobností	osobnost	k1gFnPc2	osobnost
uznávají	uznávat	k5eAaImIp3nP	uznávat
obě	dva	k4xCgNnPc4	dva
svatost	svatost	k1gFnSc1	svatost
Ivana	Ivan	k1gMnSc2	Ivan
<g/>
,	,	kIx,	,
Ludmily	Ludmila	k1gFnSc2	Ludmila
<g/>
,	,	kIx,	,
Václava	Václav	k1gMnSc2	Václav
a	a	k8xC	a
Prokopa	Prokop	k1gMnSc2	Prokop
<g/>
.	.	kIx.	.
</s>
<s>
Katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
navíc	navíc	k6eAd1	navíc
uznává	uznávat	k5eAaImIp3nS	uznávat
Vojtěcha	Vojtěch	k1gMnSc4	Vojtěch
<g/>
,	,	kIx,	,
Radima	Radim	k1gMnSc4	Radim
<g/>
,	,	kIx,	,
Anežku	Anežka	k1gFnSc4	Anežka
Českou	český	k2eAgFnSc4d1	Česká
<g/>
,	,	kIx,	,
Zdislavu	Zdislava	k1gFnSc4	Zdislava
z	z	k7c2	z
Lemberka	Lemberka	k1gFnSc1	Lemberka
<g/>
,	,	kIx,	,
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
<g/>
,	,	kIx,	,
Jana	Jan	k1gMnSc2	Jan
Sarkandera	Sarkander	k1gMnSc2	Sarkander
a	a	k8xC	a
Jana	Jan	k1gMnSc2	Jan
Nepomuka	Nepomuk	k1gMnSc4	Nepomuk
Neumanna	Neumann	k1gMnSc4	Neumann
a	a	k8xC	a
Klementa	Klement	k1gMnSc4	Klement
Mariu	Mario	k1gMnSc3	Mario
Hofbauera	Hofbauero	k1gNnSc2	Hofbauero
<g/>
.	.	kIx.	.
</s>
<s>
Pravoslavní	pravoslavný	k2eAgMnPc1d1	pravoslavný
navíc	navíc	k6eAd1	navíc
ještě	ještě	k9	ještě
svatého	svatý	k2eAgMnSc2d1	svatý
Rostislava	Rostislav	k1gMnSc2	Rostislav
a	a	k8xC	a
Gorazda	Gorazd	k1gMnSc2	Gorazd
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Matěje	Matěj	k1gMnSc2	Matěj
Pavlíka	Pavlík	k1gMnSc2	Pavlík
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
ukrýval	ukrývat	k5eAaImAgInS	ukrývat
parašutisty	parašutista	k1gMnPc4	parašutista
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
zabili	zabít	k5eAaPmAgMnP	zabít
nacistického	nacistický	k2eAgMnSc4d1	nacistický
pohlavára	pohlavár	k1gMnSc4	pohlavár
Heydricha	Heydrich	k1gMnSc4	Heydrich
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Etnografické	etnografický	k2eAgFnSc2d1	etnografická
skupiny	skupina	k1gFnSc2	skupina
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc4	několik
etnografických	etnografický	k2eAgFnPc2d1	etnografická
skupin	skupina	k1gFnPc2	skupina
úzce	úzko	k6eAd1	úzko
spjatých	spjatý	k2eAgFnPc2d1	spjatá
s	s	k7c7	s
krajem	kraj	k1gInSc7	kraj
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žijí	žít	k5eAaImIp3nP	žít
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
lišily	lišit	k5eAaImAgFnP	lišit
některými	některý	k3yIgInPc7	některý
kulturními	kulturní	k2eAgInPc7d1	kulturní
rysy	rys	k1gInPc7	rys
a	a	k8xC	a
dialektem	dialekt	k1gInSc7	dialekt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
to	ten	k3xDgNnSc1	ten
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
Chodové	Chod	k1gMnPc1	Chod
<g/>
,	,	kIx,	,
Plzeňáci	Plzeňák	k1gMnPc1	Plzeňák
<g/>
,	,	kIx,	,
Blaťáci	Blaťák	k1gMnPc1	Blaťák
a	a	k8xC	a
Doudlebové	Doudleb	k1gMnPc1	Doudleb
<g/>
,	,	kIx,	,
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
Horáci	Horák	k1gMnPc1	Horák
<g/>
,	,	kIx,	,
Hanáci	Hanák	k1gMnPc1	Hanák
<g/>
,	,	kIx,	,
Moravští	moravský	k2eAgMnPc1d1	moravský
Chorvati	Chorvat	k1gMnPc1	Chorvat
<g/>
,	,	kIx,	,
Moravští	moravský	k2eAgMnPc1d1	moravský
Slováci	Slovák	k1gMnPc1	Slovák
<g/>
,	,	kIx,	,
Podlužáci	Podlužák	k1gMnPc1	Podlužák
<g/>
,	,	kIx,	,
Valaši	Valach	k1gMnPc1	Valach
<g/>
,	,	kIx,	,
Laši	Lach	k1gMnPc1	Lach
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
a	a	k8xC	a
ve	v	k7c6	v
Slezsku	Slezsko	k1gNnSc6	Slezsko
např.	např.	kA	např.
Goralé	goral	k1gMnPc5	goral
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
rozdíly	rozdíl	k1gInPc1	rozdíl
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgInP	začít
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
stírat	stírat	k5eAaImF	stírat
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
krajové	krajový	k2eAgFnPc1d1	krajová
zvláštnosti	zvláštnost	k1gFnPc1	zvláštnost
se	se	k3xPyFc4	se
však	však	k9	však
dosud	dosud	k6eAd1	dosud
udržují	udržovat	k5eAaImIp3nP	udržovat
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
těchto	tento	k3xDgFnPc2	tento
geograficky	geograficky	k6eAd1	geograficky
rozlišitelných	rozlišitelný	k2eAgFnPc2d1	rozlišitelná
etnografických	etnografický	k2eAgFnPc2d1	etnografická
skupin	skupina	k1gFnPc2	skupina
stojí	stát	k5eAaImIp3nS	stát
za	za	k7c4	za
zmínku	zmínka	k1gFnSc4	zmínka
skupiny	skupina	k1gFnSc2	skupina
víceméně	víceméně	k9	víceméně
rozptýlené	rozptýlený	k2eAgFnPc1d1	rozptýlená
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
také	také	k9	také
tvoří	tvořit	k5eAaImIp3nP	tvořit
specifické	specifický	k2eAgFnPc1d1	specifická
etnografické	etnografický	k2eAgFnPc1d1	etnografická
skupiny	skupina	k1gFnPc1	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
romskou	romský	k2eAgFnSc4d1	romská
a	a	k8xC	a
židovskou	židovský	k2eAgFnSc4d1	židovská
etnografickou	etnografický	k2eAgFnSc4d1	etnografická
skupinu	skupina	k1gFnSc4	skupina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Cizinci	cizinec	k1gMnPc1	cizinec
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
přibližně	přibližně	k6eAd1	přibližně
464	[number]	k4	464
700	[number]	k4	700
cizinců	cizinec	k1gMnPc2	cizinec
(	(	kIx(	(
<g/>
4,3	[number]	k4	4,3
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
než	než	k8xS	než
55	[number]	k4	55
<g/>
%	%	kIx~	%
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
zde	zde	k6eAd1	zde
mělo	mít	k5eAaImAgNnS	mít
udělený	udělený	k2eAgInSc4d1	udělený
trvalý	trvalý	k2eAgInSc4d1	trvalý
pobyt	pobyt	k1gInSc4	pobyt
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Prahu	Praha	k1gFnSc4	Praha
a	a	k8xC	a
Středočeský	středočeský	k2eAgInSc1d1	středočeský
kraj	kraj	k1gInSc1	kraj
připadá	připadat	k5eAaImIp3nS	připadat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
polovina	polovina	k1gFnSc1	polovina
z	z	k7c2	z
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
cizinců	cizinec	k1gMnPc2	cizinec
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
cizinců	cizinec	k1gMnPc2	cizinec
pocházelo	pocházet	k5eAaImAgNnS	pocházet
z	z	k7c2	z
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
(	(	kIx(	(
<g/>
23	[number]	k4	23
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ti	ten	k3xDgMnPc1	ten
tak	tak	k6eAd1	tak
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
předběhli	předběhnout	k5eAaPmAgMnP	předběhnout
cizince	cizinec	k1gMnPc4	cizinec
ze	z	k7c2	z
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
nyní	nyní	k6eAd1	nyní
tvoří	tvořit	k5eAaImIp3nP	tvořit
22	[number]	k4	22
%	%	kIx~	%
podílu	podíl	k1gInSc2	podíl
<g/>
,	,	kIx,	,
následují	následovat	k5eAaImIp3nP	následovat
cizinci	cizinec	k1gMnPc1	cizinec
z	z	k7c2	z
Vietnamu	Vietnam	k1gInSc2	Vietnam
(	(	kIx(	(
<g/>
12	[number]	k4	12
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ruska	Rusko	k1gNnSc2	Rusko
(	(	kIx(	(
<g/>
8	[number]	k4	8
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Německa	Německo	k1gNnSc2	Německo
(	(	kIx(	(
<g/>
5	[number]	k4	5
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
Polska	Polsko	k1gNnSc2	Polsko
(	(	kIx(	(
<g/>
4	[number]	k4	4
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vietnamci	Vietnamec	k1gMnPc1	Vietnamec
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
Česku	Česko	k1gNnSc3	Česko
nejdéle	dlouho	k6eAd3	dlouho
(	(	kIx(	(
<g/>
průměrně	průměrně	k6eAd1	průměrně
8	[number]	k4	8
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lidé	člověk	k1gMnPc1	člověk
z	z	k7c2	z
bývalé	bývalý	k2eAgFnSc2d1	bývalá
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
jen	jen	k9	jen
o	o	k7c4	o
rok	rok	k1gInSc4	rok
méně	málo	k6eAd2	málo
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
vysokoškolsky	vysokoškolsky	k6eAd1	vysokoškolsky
vzdělaných	vzdělaný	k2eAgMnPc2d1	vzdělaný
do	do	k7c2	do
naší	náš	k3xOp1gFnSc2	náš
země	zem	k1gFnSc2	zem
přichází	přicházet	k5eAaImIp3nS	přicházet
z	z	k7c2	z
Ruska	Rusko	k1gNnSc2	Rusko
(	(	kIx(	(
<g/>
27	[number]	k4	27
%	%	kIx~	%
imigrantů	imigrant	k1gMnPc2	imigrant
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
z	z	k7c2	z
výdělku	výdělek	k1gInSc2	výdělek
posílají	posílat	k5eAaImIp3nP	posílat
domů	domů	k6eAd1	domů
Ukrajinci	Ukrajinec	k1gMnPc1	Ukrajinec
(	(	kIx(	(
<g/>
17	[number]	k4	17
%	%	kIx~	%
příjmu	příjem	k1gInSc2	příjem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Česko	Česko	k1gNnSc1	Česko
však	však	k9	však
stále	stále	k6eAd1	stále
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
poměrně	poměrně	k6eAd1	poměrně
etnicky	etnicky	k6eAd1	etnicky
homogenní	homogenní	k2eAgFnSc7d1	homogenní
zemí	zem	k1gFnSc7	zem
oproti	oproti	k7c3	oproti
průměru	průměr	k1gInSc3	průměr
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
–	–	k?	–
z	z	k7c2	z
28	[number]	k4	28
států	stát	k1gInPc2	stát
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
24	[number]	k4	24
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
etnické	etnický	k2eAgFnSc2d1	etnická
heterogenity	heterogenita	k1gFnSc2	heterogenita
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
sousedním	sousední	k2eAgNnSc6d1	sousední
Německu	Německo	k1gNnSc6	Německo
žil	žít	k5eAaImAgInS	žít
největší	veliký	k2eAgInSc1d3	veliký
počet	počet	k1gInSc1	počet
cizinců	cizinec	k1gMnPc2	cizinec
v	v	k7c6	v
EU	EU	kA	EU
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
jest	být	k5eAaImIp3nS	být
7,2	[number]	k4	7,2
milionů	milion	k4xCgInPc2	milion
cizinců	cizinec	k1gMnPc2	cizinec
(	(	kIx(	(
<g/>
9	[number]	k4	9
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
10,8	[number]	k4	10,8
%	%	kIx~	%
a	a	k8xC	a
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
12	[number]	k4	12
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Největší	veliký	k2eAgNnPc1d3	veliký
města	město	k1gNnPc1	město
podle	podle	k7c2	podle
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
===	===	k?	===
</s>
</p>
<p>
<s>
Statutárními	statutární	k2eAgNnPc7d1	statutární
městy	město	k1gNnPc7	město
jsou	být	k5eAaImIp3nP	být
navíc	navíc	k6eAd1	navíc
také	také	k9	také
Karlovy	Karlův	k2eAgInPc1d1	Karlův
Vary	Vary	k1gInPc1	Vary
<g/>
,	,	kIx,	,
Mladá	mladá	k1gFnSc1	mladá
Boleslav	Boleslav	k1gMnSc1	Boleslav
<g/>
,	,	kIx,	,
Chomutov	Chomutov	k1gInSc1	Chomutov
<g/>
,	,	kIx,	,
Přerov	Přerov	k1gInSc1	Přerov
<g/>
,	,	kIx,	,
Jablonec	Jablonec	k1gInSc1	Jablonec
nad	nad	k7c7	nad
Nisou	Nisa	k1gFnSc7	Nisa
a	a	k8xC	a
Prostějov	Prostějov	k1gInSc1	Prostějov
<g/>
.	.	kIx.	.
</s>
<s>
Dřívějšími	dřívější	k2eAgInPc7d1	dřívější
okresními	okresní	k2eAgInPc7d1	okresní
městy	město	k1gNnPc7	město
jsou	být	k5eAaImIp3nP	být
navíc	navíc	k6eAd1	navíc
ještě	ještě	k9	ještě
Benešov	Benešov	k1gInSc1	Benešov
<g/>
,	,	kIx,	,
Beroun	Beroun	k1gInSc1	Beroun
<g/>
,	,	kIx,	,
Blansko	Blansko	k1gNnSc1	Blansko
<g/>
,	,	kIx,	,
Bruntál	Bruntál	k1gInSc1	Bruntál
<g/>
,	,	kIx,	,
Břeclav	Břeclav	k1gFnSc1	Břeclav
<g/>
,	,	kIx,	,
Česká	český	k2eAgFnSc1d1	Česká
Lípa	lípa	k1gFnSc1	lípa
<g/>
,	,	kIx,	,
Český	český	k2eAgInSc1d1	český
Krumlov	Krumlov	k1gInSc1	Krumlov
<g/>
,	,	kIx,	,
Domažlice	Domažlice	k1gFnPc1	Domažlice
<g/>
,	,	kIx,	,
Havlíčkův	Havlíčkův	k2eAgInSc1d1	Havlíčkův
Brod	Brod	k1gInSc1	Brod
<g/>
,	,	kIx,	,
Hodonín	Hodonín	k1gInSc1	Hodonín
<g/>
,	,	kIx,	,
Cheb	Cheb	k1gInSc1	Cheb
<g/>
,	,	kIx,	,
Chrudim	Chrudim	k1gFnSc1	Chrudim
<g/>
,	,	kIx,	,
Jeseník	Jeseník	k1gInSc1	Jeseník
<g/>
,	,	kIx,	,
Jičín	Jičín	k1gInSc1	Jičín
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Jindřichův	Jindřichův	k2eAgInSc1d1	Jindřichův
Hradec	Hradec	k1gInSc1	Hradec
<g/>
,	,	kIx,	,
Klatovy	Klatovy	k1gInPc1	Klatovy
<g/>
,	,	kIx,	,
Kolín	Kolín	k1gInSc1	Kolín
<g/>
,	,	kIx,	,
Kroměříž	Kroměříž	k1gFnSc1	Kroměříž
<g/>
,	,	kIx,	,
Kutná	kutný	k2eAgFnSc1d1	Kutná
Hora	hora	k1gFnSc1	hora
<g/>
,	,	kIx,	,
Litoměřice	Litoměřice	k1gInPc1	Litoměřice
<g/>
,	,	kIx,	,
Louny	Louny	k1gInPc1	Louny
<g/>
,	,	kIx,	,
Mělník	Mělník	k1gInSc1	Mělník
<g/>
,	,	kIx,	,
Náchod	Náchod	k1gInSc1	Náchod
<g/>
,	,	kIx,	,
Nový	nový	k2eAgInSc1d1	nový
Jičín	Jičín	k1gInSc1	Jičín
<g/>
,	,	kIx,	,
Nymburk	Nymburk	k1gInSc1	Nymburk
<g/>
,	,	kIx,	,
Pelhřimov	Pelhřimov	k1gInSc1	Pelhřimov
<g/>
,	,	kIx,	,
Písek	Písek	k1gInSc1	Písek
<g/>
,	,	kIx,	,
Prachatice	Prachatice	k1gFnPc1	Prachatice
<g/>
,	,	kIx,	,
Příbram	Příbram	k1gFnSc1	Příbram
<g/>
,	,	kIx,	,
Rakovník	Rakovník	k1gInSc1	Rakovník
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Rokycany	Rokycany	k1gInPc1	Rokycany
<g/>
,	,	kIx,	,
Rychnov	Rychnov	k1gInSc1	Rychnov
nad	nad	k7c7	nad
Kněžnou	kněžna	k1gFnSc7	kněžna
<g/>
,	,	kIx,	,
Semily	Semily	k1gInPc7	Semily
<g/>
,	,	kIx,	,
Sokolov	Sokolov	k1gInSc1	Sokolov
<g/>
,	,	kIx,	,
Strakonice	Strakonice	k1gFnPc1	Strakonice
<g/>
,	,	kIx,	,
Svitavy	Svitava	k1gFnPc1	Svitava
<g/>
,	,	kIx,	,
Šumperk	Šumperk	k1gInSc1	Šumperk
<g/>
,	,	kIx,	,
Tábor	Tábor	k1gInSc1	Tábor
<g/>
,	,	kIx,	,
Tachov	Tachov	k1gInSc1	Tachov
<g/>
,	,	kIx,	,
Trutnov	Trutnov	k1gInSc1	Trutnov
<g/>
,	,	kIx,	,
Třebíč	Třebíč	k1gFnSc1	Třebíč
<g/>
,	,	kIx,	,
Třinec	Třinec	k1gInSc1	Třinec
<g/>
,	,	kIx,	,
Uherské	uherský	k2eAgNnSc1d1	Uherské
Hradiště	Hradiště	k1gNnSc1	Hradiště
<g/>
,	,	kIx,	,
Ústí	ústí	k1gNnSc1	ústí
nad	nad	k7c7	nad
Orlicí	Orlice	k1gFnSc7	Orlice
<g/>
,	,	kIx,	,
Vsetín	Vsetín	k1gInSc1	Vsetín
<g/>
,	,	kIx,	,
Vyškov	Vyškov	k1gInSc1	Vyškov
<g/>
,	,	kIx,	,
Znojmo	Znojmo	k1gNnSc1	Znojmo
<g/>
,	,	kIx,	,
Žďár	Žďár	k1gInSc1	Žďár
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
<g/>
.	.	kIx.	.
</s>
<s>
Největšími	veliký	k2eAgNnPc7d3	veliký
městy	město	k1gNnPc7	město
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
nejsou	být	k5eNaImIp3nP	být
ani	ani	k8xC	ani
statutární	statutární	k2eAgInSc1d1	statutární
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
okresní	okresní	k2eAgInPc1d1	okresní
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
Orlová	Orlová	k1gFnSc1	Orlová
<g/>
,	,	kIx,	,
Litvínov	Litvínov	k1gInSc1	Litvínov
<g/>
,	,	kIx,	,
Český	český	k2eAgInSc1d1	český
Těšín	Těšín	k1gInSc1	Těšín
<g/>
,	,	kIx,	,
Krnov	Krnov	k1gInSc1	Krnov
<g/>
,	,	kIx,	,
Kopřivnice	Kopřivnice	k1gFnSc1	Kopřivnice
<g/>
,	,	kIx,	,
Bohumín	Bohumín	k1gInSc1	Bohumín
a	a	k8xC	a
Jirkov	Jirkov	k1gInSc1	Jirkov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kultura	kultura	k1gFnSc1	kultura
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Literární	literární	k2eAgFnPc1d1	literární
dějiny	dějiny	k1gFnPc1	dějiny
počínají	počínat	k5eAaImIp3nP	počínat
ústní	ústní	k2eAgFnSc7d1	ústní
tradicí	tradice	k1gFnSc7	tradice
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
vyniká	vynikat	k5eAaImIp3nS	vynikat
pověst	pověst	k1gFnSc1	pověst
o	o	k7c6	o
praotci	praotec	k1gMnSc6	praotec
Čechovi	Čech	k1gMnSc6	Čech
<g/>
,	,	kIx,	,
Krokovi	Kroek	k1gMnSc6	Kroek
<g/>
,	,	kIx,	,
Libuši	Libuše	k1gFnSc6	Libuše
<g/>
,	,	kIx,	,
Přemyslu	Přemysl	k1gMnSc3	Přemysl
Oráčovi	oráč	k1gMnSc3	oráč
a	a	k8xC	a
dívčí	dívčí	k2eAgFnSc3d1	dívčí
válce	válka	k1gFnSc3	válka
<g/>
.	.	kIx.	.
</s>
<s>
Počátky	počátek	k1gInPc1	počátek
české	český	k2eAgFnSc2d1	Česká
psané	psaný	k2eAgFnSc2d1	psaná
literatury	literatura	k1gFnSc2	literatura
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
činností	činnost	k1gFnSc7	činnost
Konstantina	Konstantin	k1gMnSc2	Konstantin
Filozofa	filozof	k1gMnSc2	filozof
a	a	k8xC	a
jeho	on	k3xPp3gMnSc2	on
bratra	bratr	k1gMnSc2	bratr
Metoděje	Metoděj	k1gMnSc2	Metoděj
na	na	k7c6	na
Velké	velký	k2eAgFnSc6d1	velká
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
žáky	žák	k1gMnPc7	žák
(	(	kIx(	(
<g/>
nejvýznamnějším	významný	k2eAgMnSc6d3	nejvýznamnější
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
byl	být	k5eAaImAgMnS	být
Kliment	Kliment	k1gMnSc1	Kliment
Ochridský	Ochridský	k2eAgMnSc1d1	Ochridský
<g/>
)	)	kIx)	)
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
<g/>
,	,	kIx,	,
ve	v	k7c6	v
staroslověnštině	staroslověnština	k1gFnSc6	staroslověnština
a	a	k8xC	a
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
písma	písmo	k1gNnSc2	písmo
hlaholice	hlaholice	k1gFnSc2	hlaholice
<g/>
,	,	kIx,	,
první	první	k4xOgFnPc1	první
české	český	k2eAgFnPc1d1	Česká
literární	literární	k2eAgFnPc1d1	literární
památky	památka	k1gFnPc1	památka
(	(	kIx(	(
<g/>
Proglas	Proglas	k1gInSc1	Proglas
<g/>
,	,	kIx,	,
Život	život	k1gInSc1	život
Metodějův	Metodějův	k2eAgInSc1d1	Metodějův
<g/>
,	,	kIx,	,
Život	život	k1gInSc1	život
Konstantinův	Konstantinův	k2eAgInSc1d1	Konstantinův
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vyhnání	vyhnání	k1gNnSc6	vyhnání
této	tento	k3xDgFnSc2	tento
skupiny	skupina	k1gFnSc2	skupina
z	z	k7c2	z
Moravy	Morava	k1gFnSc2	Morava
začala	začít	k5eAaPmAgFnS	začít
klíčovou	klíčový	k2eAgFnSc4d1	klíčová
roli	role	k1gFnSc4	role
sehrávat	sehrávat	k5eAaImF	sehrávat
latina	latina	k1gFnSc1	latina
<g/>
,	,	kIx,	,
vznikaly	vznikat	k5eAaImAgInP	vznikat
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
legendy	legenda	k1gFnPc1	legenda
(	(	kIx(	(
<g/>
Kristiánova	Kristiánův	k2eAgFnSc1d1	Kristiánova
legenda	legenda	k1gFnSc1	legenda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kroniky	kronika	k1gFnPc1	kronika
(	(	kIx(	(
<g/>
zejm.	zejm.	k?	zejm.
Kosmova	Kosmův	k2eAgFnSc1d1	Kosmova
Kronika	kronika	k1gFnSc1	kronika
Čechů	Čech	k1gMnPc2	Čech
<g/>
)	)	kIx)	)
i	i	k9	i
jiné	jiný	k2eAgInPc4d1	jiný
žánry	žánr	k1gInPc4	žánr
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Vita	vit	k2eAgFnSc1d1	Vita
Caroli	Carole	k1gFnSc3	Carole
<g/>
,	,	kIx,	,
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
životopis	životopis	k1gInSc1	životopis
krále	král	k1gMnSc2	král
Karla	Karel	k1gMnSc2	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prvními	první	k4xOgFnPc7	první
významnými	významný	k2eAgMnPc7d1	významný
česky	česky	k6eAd1	česky
psanými	psaný	k2eAgInPc7d1	psaný
texty	text	k1gInPc7	text
byly	být	k5eAaImAgFnP	být
Alexandreida	Alexandreida	k1gFnSc1	Alexandreida
a	a	k8xC	a
Dalimilova	Dalimilův	k2eAgFnSc1d1	Dalimilova
kronika	kronika	k1gFnSc1	kronika
<g/>
.	.	kIx.	.
</s>
<s>
Prvními	první	k4xOgMnPc7	první
autory	autor	k1gMnPc7	autor
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
psali	psát	k5eAaImAgMnP	psát
i	i	k9	i
česky	česky	k6eAd1	česky
<g/>
,	,	kIx,	,
a	a	k8xC	a
kteří	který	k3yQgMnPc1	který
došli	dojít	k5eAaPmAgMnP	dojít
světového	světový	k2eAgInSc2d1	světový
věhlasu	věhlas	k1gInSc2	věhlas
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
zakladatelů	zakladatel	k1gMnPc2	zakladatel
evropské	evropský	k2eAgFnSc2d1	Evropská
reformace	reformace	k1gFnSc2	reformace
<g/>
,	,	kIx,	,
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
Amos	Amos	k1gMnSc1	Amos
Komenský	Komenský	k1gMnSc1	Komenský
<g/>
,	,	kIx,	,
nejvýznamnější	významný	k2eAgMnSc1d3	nejvýznamnější
představitel	představitel	k1gMnSc1	představitel
humanismu	humanismus	k1gInSc2	humanismus
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
literatuře	literatura	k1gFnSc6	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgMnPc7d1	další
významnými	významný	k2eAgMnPc7d1	významný
autory	autor	k1gMnPc7	autor
reformace	reformace	k1gFnSc2	reformace
byli	být	k5eAaImAgMnP	být
Jeroným	Jeroným	k1gMnSc1	Jeroným
Pražský	pražský	k2eAgMnSc1d1	pražský
a	a	k8xC	a
Petr	Petr	k1gMnSc1	Petr
Chelčický	Chelčický	k2eAgMnSc1d1	Chelčický
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
stavovského	stavovský	k2eAgNnSc2d1	Stavovské
povstání	povstání	k1gNnSc2	povstání
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
na	na	k7c6	na
Bílé	bílý	k2eAgFnSc6d1	bílá
hoře	hora	k1gFnSc6	hora
roku	rok	k1gInSc2	rok
1620	[number]	k4	1620
nastalo	nastat	k5eAaPmAgNnS	nastat
složité	složitý	k2eAgNnSc1d1	složité
období	období	k1gNnSc1	období
vytlačování	vytlačování	k1gNnSc2	vytlačování
a	a	k8xC	a
odumírání	odumírání	k1gNnSc2	odumírání
češtiny	čeština	k1gFnSc2	čeština
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
tomu	ten	k3xDgInSc3	ten
se	se	k3xPyFc4	se
v	v	k7c6	v
barokní	barokní	k2eAgFnSc6d1	barokní
éře	éra	k1gFnSc6	éra
postavil	postavit	k5eAaPmAgMnS	postavit
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Balbín	Balbín	k1gMnSc1	Balbín
<g/>
.	.	kIx.	.
</s>
<s>
Ústup	ústup	k1gInSc1	ústup
češtiny	čeština	k1gFnSc2	čeština
však	však	k9	však
zastavil	zastavit	k5eAaPmAgInS	zastavit
až	až	k9	až
proces	proces	k1gInSc1	proces
tzv.	tzv.	kA	tzv.
národního	národní	k2eAgNnSc2d1	národní
obrození	obrození	k1gNnSc2	obrození
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
začal	začít	k5eAaPmAgInS	začít
na	na	k7c6	na
konci	konec	k1gInSc6	konec
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Klíčovou	klíčový	k2eAgFnSc7d1	klíčová
postavou	postava	k1gFnSc7	postava
první	první	k4xOgFnSc2	první
etapy	etapa	k1gFnSc2	etapa
obrození	obrození	k1gNnSc2	obrození
byl	být	k5eAaImAgMnS	být
lingvista	lingvista	k1gMnSc1	lingvista
Josef	Josef	k1gMnSc1	Josef
Dobrovský	Dobrovský	k1gMnSc1	Dobrovský
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
etapě	etapa	k1gFnSc6	etapa
to	ten	k3xDgNnSc1	ten
byli	být	k5eAaImAgMnP	být
Josef	Josef	k1gMnSc1	Josef
Jungmann	Jungmann	k1gMnSc1	Jungmann
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
kladl	klást	k5eAaImAgMnS	klást
důraz	důraz	k1gInSc4	důraz
na	na	k7c4	na
jazykovou	jazykový	k2eAgFnSc4d1	jazyková
koncepci	koncepce	k1gFnSc4	koncepce
národa	národ	k1gInSc2	národ
<g/>
,	,	kIx,	,
a	a	k8xC	a
Pavel	Pavel	k1gMnSc1	Pavel
Jozef	Jozef	k1gMnSc1	Jozef
Šafařík	Šafařík	k1gMnSc1	Šafařík
<g/>
,	,	kIx,	,
představitel	představitel	k1gMnSc1	představitel
panslovanských	panslovanský	k2eAgFnPc2d1	panslovanská
tendencí	tendence	k1gFnPc2	tendence
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
objevovat	objevovat	k5eAaImF	objevovat
také	také	k9	také
již	již	k9	již
první	první	k4xOgFnSc1	první
svébytná	svébytný	k2eAgFnSc1d1	svébytná
literatura	literatura	k1gFnSc1	literatura
(	(	kIx(	(
<g/>
Jan	Jan	k1gMnSc1	Jan
Kollár	Kollár	k1gMnSc1	Kollár
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Ladislav	Ladislav	k1gMnSc1	Ladislav
Čelakovský	Čelakovský	k2eAgMnSc1d1	Čelakovský
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Proces	proces	k1gInSc1	proces
vyvrcholil	vyvrcholit	k5eAaPmAgInS	vyvrcholit
ve	v	k7c6	v
třetí	třetí	k4xOgFnSc6	třetí
etapě	etapa	k1gFnSc6	etapa
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
František	František	k1gMnSc1	František
Palacký	Palacký	k1gMnSc1	Palacký
a	a	k8xC	a
Karel	Karel	k1gMnSc1	Karel
Havlíček	Havlíček	k1gMnSc1	Havlíček
Borovský	Borovský	k1gMnSc1	Borovský
dokončili	dokončit	k5eAaPmAgMnP	dokončit
koncept	koncept	k1gInSc4	koncept
českého	český	k2eAgInSc2d1	český
národa	národ	k1gInSc2	národ
politicky	politicky	k6eAd1	politicky
a	a	k8xC	a
kdy	kdy	k6eAd1	kdy
vznikla	vzniknout	k5eAaPmAgNnP	vzniknout
i	i	k9	i
vrcholná	vrcholný	k2eAgNnPc1d1	vrcholné
díla	dílo	k1gNnPc1	dílo
literární	literární	k2eAgNnPc1d1	literární
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
už	už	k6eAd1	už
básnická	básnický	k2eAgFnSc1d1	básnická
(	(	kIx(	(
<g/>
Havlíček	Havlíček	k1gMnSc1	Havlíček
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Hynek	Hynek	k1gMnSc1	Hynek
Mácha	Mácha	k1gMnSc1	Mácha
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Jaromír	Jaromír	k1gMnSc1	Jaromír
Erben	Erben	k1gMnSc1	Erben
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
prozaická	prozaický	k2eAgFnSc1d1	prozaická
(	(	kIx(	(
<g/>
Božena	Božena	k1gFnSc1	Božena
Němcová	Němcová	k1gFnSc1	Němcová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
či	či	k8xC	či
divadelní	divadelní	k2eAgMnSc1d1	divadelní
(	(	kIx(	(
<g/>
Josef	Josef	k1gMnSc1	Josef
Kajetán	Kajetán	k1gMnSc1	Kajetán
Tyl	Tyl	k1gMnSc1	Tyl
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Sabina	Sabina	k1gMnSc1	Sabina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
literární	literární	k2eAgInSc1d1	literární
život	život	k1gInSc1	život
začal	začít	k5eAaPmAgInS	začít
prudce	prudko	k6eAd1	prudko
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
<g/>
,	,	kIx,	,
vznikaly	vznikat	k5eAaImAgFnP	vznikat
skupiny	skupina	k1gFnPc1	skupina
s	s	k7c7	s
různým	různý	k2eAgInSc7d1	různý
programem	program	k1gInSc7	program
–	–	k?	–
májovci	májovec	k1gMnPc1	májovec
(	(	kIx(	(
<g/>
Jan	Jan	k1gMnSc1	Jan
Neruda	Neruda	k1gMnSc1	Neruda
<g/>
,	,	kIx,	,
Vítězslav	Vítězslav	k1gMnSc1	Vítězslav
Hálek	hálka	k1gFnPc2	hálka
<g/>
,	,	kIx,	,
Jakub	Jakub	k1gMnSc1	Jakub
Arbes	Arbes	k1gMnSc1	Arbes
<g/>
,	,	kIx,	,
Karolína	Karolína	k1gFnSc1	Karolína
Světlá	světlý	k2eAgFnSc1d1	světlá
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ruchovci	ruchovec	k1gMnPc1	ruchovec
(	(	kIx(	(
<g/>
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Čech	Čech	k1gMnSc1	Čech
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
lumírovci	lumírovec	k1gMnPc1	lumírovec
(	(	kIx(	(
<g/>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Vrchlický	Vrchlický	k1gMnSc1	Vrchlický
<g/>
,	,	kIx,	,
Julius	Julius	k1gMnSc1	Julius
Zeyer	Zeyer	k1gMnSc1	Zeyer
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
realisté	realista	k1gMnPc1	realista
(	(	kIx(	(
<g/>
Alois	Alois	k1gMnSc1	Alois
Jirásek	Jirásek	k1gMnSc1	Jirásek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
moderna	moderna	k1gFnSc1	moderna
(	(	kIx(	(
<g/>
Antonín	Antonín	k1gMnSc1	Antonín
Sova	Sova	k1gMnSc1	Sova
<g/>
,	,	kIx,	,
Otokar	Otokar	k1gMnSc1	Otokar
Březina	Březina	k1gMnSc1	Březina
<g/>
)	)	kIx)	)
či	či	k8xC	či
tzv.	tzv.	kA	tzv.
anarchističtí	anarchistický	k2eAgMnPc1d1	anarchistický
buřiči	buřič	k1gMnPc1	buřič
(	(	kIx(	(
<g/>
Viktor	Viktor	k1gMnSc1	Viktor
Dyk	Dyk	k?	Dyk
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
Bezruč	Bezruč	k1gMnSc1	Bezruč
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
začínala	začínat	k5eAaImAgFnS	začínat
též	též	k9	též
vzkvétat	vzkvétat	k5eAaImF	vzkvétat
německy	německy	k6eAd1	německy
psaná	psaný	k2eAgFnSc1d1	psaná
literatura	literatura	k1gFnSc1	literatura
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
světovým	světový	k2eAgInSc7d1	světový
fenoménem	fenomén	k1gInSc7	fenomén
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
díky	díky	k7c3	díky
Franzi	Franze	k1gFnSc3	Franze
Kafkovi	Kafka	k1gMnSc3	Kafka
(	(	kIx(	(
<g/>
Proces	proces	k1gInSc1	proces
<g/>
,	,	kIx,	,
Zámek	zámek	k1gInSc1	zámek
<g/>
,	,	kIx,	,
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
jiným	jiný	k2eAgInSc7d1	jiný
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Rainer	Rainer	k1gMnSc1	Rainer
Maria	Mario	k1gMnSc2	Mario
Rilke	Rilke	k1gNnSc2	Rilke
<g/>
,	,	kIx,	,
Gustav	Gustav	k1gMnSc1	Gustav
Meyrink	Meyrink	k1gInSc1	Meyrink
<g/>
,	,	kIx,	,
Franz	Franz	k1gMnSc1	Franz
Werfel	Werfel	k1gMnSc1	Werfel
<g/>
,	,	kIx,	,
Max	Max	k1gMnSc1	Max
Brod	Brod	k1gInSc1	Brod
<g/>
,	,	kIx,	,
Egon	Egon	k1gMnSc1	Egon
Erwín	Erwín	k1gMnSc1	Erwín
Kisch	Kisch	k1gMnSc1	Kisch
<g/>
,	,	kIx,	,
Karl	Karl	k1gMnSc1	Karl
Kraus	Kraus	k1gMnSc1	Kraus
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
von	von	k1gInSc1	von
Ebner-Eschenbachová	Ebner-Eschenbachová	k1gFnSc1	Ebner-Eschenbachová
<g/>
,	,	kIx,	,
Adalbert	Adalbert	k1gMnSc1	Adalbert
Stifter	Stifter	k1gMnSc1	Stifter
<g/>
,	,	kIx,	,
Ottfried	Ottfried	k1gMnSc1	Ottfried
Preussler	Preussler	k1gMnSc1	Preussler
<g/>
,	,	kIx,	,
Leo	Leo	k1gMnSc1	Leo
Perutz	Perutz	k1gMnSc1	Perutz
<g/>
,	,	kIx,	,
Ernst	Ernst	k1gMnSc1	Ernst
Weiss	Weiss	k1gMnSc1	Weiss
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
si	se	k3xPyFc3	se
vydobyli	vydobýt	k5eAaPmAgMnP	vydobýt
světový	světový	k2eAgInSc4d1	světový
význam	význam	k1gInSc4	význam
i	i	k8xC	i
česky	česky	k6eAd1	česky
píšící	píšící	k2eAgMnPc1d1	píšící
tvůrci	tvůrce	k1gMnPc1	tvůrce
<g/>
,	,	kIx,	,
především	především	k9	především
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Hašek	Hašek	k1gMnSc1	Hašek
(	(	kIx(	(
<g/>
zejm.	zejm.	k?	zejm.
Osudy	osud	k1gInPc1	osud
dobrého	dobrý	k2eAgMnSc2d1	dobrý
vojáka	voják	k1gMnSc2	voják
Švejka	Švejk	k1gMnSc2	Švejk
<g/>
)	)	kIx)	)
a	a	k8xC	a
Karel	Karel	k1gMnSc1	Karel
Čapek	Čapek	k1gMnSc1	Čapek
(	(	kIx(	(
<g/>
zejm.	zejm.	k?	zejm.
Válka	válka	k1gFnSc1	válka
s	s	k7c7	s
mloky	mlok	k1gMnPc7	mlok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Silná	silný	k2eAgFnSc1d1	silná
byla	být	k5eAaImAgFnS	být
též	též	k9	též
levicová	levicový	k2eAgFnSc1d1	levicová
avantgarda	avantgarda	k1gFnSc1	avantgarda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
sdružila	sdružit	k5eAaPmAgFnS	sdružit
do	do	k7c2	do
spolku	spolek	k1gInSc2	spolek
Devětsil	Devětsil	k1gInSc1	Devětsil
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaPmAgInS	věnovat
nejprve	nejprve	k6eAd1	nejprve
proletářské	proletářský	k2eAgFnSc3d1	proletářská
poezii	poezie	k1gFnSc3	poezie
<g/>
,	,	kIx,	,
posléze	posléze	k6eAd1	posléze
vymyslel	vymyslet	k5eAaPmAgInS	vymyslet
směr	směr	k1gInSc1	směr
poetismus	poetismus	k1gInSc4	poetismus
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
většina	většina	k1gFnSc1	většina
autorů	autor	k1gMnPc2	autor
nakonec	nakonec	k6eAd1	nakonec
přešla	přejít	k5eAaPmAgFnS	přejít
k	k	k7c3	k
surrealismu	surrealismus	k1gInSc3	surrealismus
<g/>
.	.	kIx.	.
</s>
<s>
Členem	člen	k1gMnSc7	člen
Devětsilu	Devětsil	k1gInSc2	Devětsil
byl	být	k5eAaImAgMnS	být
i	i	k8xC	i
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Seifert	Seifert	k1gMnSc1	Seifert
<g/>
,	,	kIx,	,
dosud	dosud	k6eAd1	dosud
jediný	jediný	k2eAgMnSc1d1	jediný
Čech	Čech	k1gMnSc1	Čech
oceněný	oceněný	k2eAgMnSc1d1	oceněný
Nobelovou	Nobelův	k2eAgFnSc7d1	Nobelova
cenou	cena	k1gFnSc7	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
(	(	kIx(	(
<g/>
roku	rok	k1gInSc2	rok
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalším	další	k2eAgMnPc3d1	další
významným	významný	k2eAgMnPc3d1	významný
členům	člen	k1gMnPc3	člen
patřili	patřit	k5eAaImAgMnP	patřit
Jiří	Jiří	k1gMnSc1	Jiří
Wolker	Wolker	k1gMnSc1	Wolker
<g/>
,	,	kIx,	,
Vítězslav	Vítězslav	k1gMnSc1	Vítězslav
Nezval	Nezval	k1gMnSc1	Nezval
<g/>
,	,	kIx,	,
Vladislav	Vladislav	k1gMnSc1	Vladislav
Vančura	Vančura	k1gMnSc1	Vančura
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Halas	Halas	k1gMnSc1	Halas
a	a	k8xC	a
Karel	Karel	k1gMnSc1	Karel
Teige	Teig	k1gFnSc2	Teig
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
avantgardní	avantgardní	k2eAgInPc4d1	avantgardní
kruhy	kruh	k1gInPc4	kruh
stáli	stát	k5eAaImAgMnP	stát
např.	např.	kA	např.
Ivan	Ivan	k1gMnSc1	Ivan
Olbracht	Olbracht	k1gMnSc1	Olbracht
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Holan	Holan	k1gMnSc1	Holan
či	či	k8xC	či
Ladislav	Ladislav	k1gMnSc1	Ladislav
Klíma	Klíma	k1gMnSc1	Klíma
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
největšího	veliký	k2eAgInSc2d3	veliký
ohlasu	ohlas	k1gInSc2	ohlas
dostalo	dostat	k5eAaPmAgNnS	dostat
Milanu	Milan	k1gMnSc3	Milan
Kunderovi	Kunder	k1gMnSc3	Kunder
(	(	kIx(	(
<g/>
zejm.	zejm.	k?	zejm.
Nesnesitelná	snesitelný	k2eNgFnSc1d1	nesnesitelná
lehkost	lehkost	k1gFnSc1	lehkost
bytí	bytí	k1gNnSc2	bytí
<g/>
,	,	kIx,	,
Žert	žert	k1gInSc1	žert
<g/>
)	)	kIx)	)
a	a	k8xC	a
Bohumilu	Bohumil	k1gMnSc6	Bohumil
Hrabalovi	Hrabal	k1gMnSc6	Hrabal
<g/>
.	.	kIx.	.
</s>
<s>
Literatura	literatura	k1gFnSc1	literatura
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
z	z	k7c2	z
politických	politický	k2eAgInPc2d1	politický
důvodů	důvod	k1gInPc2	důvod
(	(	kIx(	(
<g/>
zvláště	zvláště	k6eAd1	zvláště
po	po	k7c6	po
sovětské	sovětský	k2eAgFnSc6d1	sovětská
okupaci	okupace	k1gFnSc6	okupace
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
rozdělila	rozdělit	k5eAaPmAgFnS	rozdělit
na	na	k7c4	na
exilovou	exilový	k2eAgFnSc4d1	exilová
<g/>
,	,	kIx,	,
samizdatovou	samizdatový	k2eAgFnSc4d1	samizdatová
a	a	k8xC	a
oficiální	oficiální	k2eAgFnSc4d1	oficiální
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
významným	významný	k2eAgMnPc3d1	významný
exilovým	exilový	k2eAgMnPc3d1	exilový
autorům	autor	k1gMnPc3	autor
krom	krom	k7c2	krom
Kundery	Kundera	k1gFnSc2	Kundera
patřili	patřit	k5eAaImAgMnP	patřit
Pavel	Pavel	k1gMnSc1	Pavel
Kohout	Kohout	k1gMnSc1	Kohout
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Škvorecký	Škvorecký	k2eAgMnSc1d1	Škvorecký
a	a	k8xC	a
Arnošt	Arnošt	k1gMnSc1	Arnošt
Lustig	Lustig	k1gMnSc1	Lustig
<g/>
.	.	kIx.	.
</s>
<s>
Samizdatová	samizdatový	k2eAgFnSc1d1	samizdatová
literatura	literatura	k1gFnSc1	literatura
se	se	k3xPyFc4	se
štěpila	štěpit	k5eAaImAgFnS	štěpit
na	na	k7c4	na
disent	disent	k1gInSc4	disent
spjatý	spjatý	k2eAgInSc4d1	spjatý
zejména	zejména	k6eAd1	zejména
s	s	k7c7	s
Chartou	charta	k1gFnSc7	charta
77	[number]	k4	77
(	(	kIx(	(
<g/>
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
<g/>
,	,	kIx,	,
Ivan	Ivan	k1gMnSc1	Ivan
Klíma	Klíma	k1gMnSc1	Klíma
<g/>
,	,	kIx,	,
Ludvík	Ludvík	k1gMnSc1	Ludvík
Vaculík	Vaculík	k1gMnSc1	Vaculík
<g/>
)	)	kIx)	)
a	a	k8xC	a
tzv.	tzv.	kA	tzv.
underground	underground	k1gInSc1	underground
(	(	kIx(	(
<g/>
Egon	Egon	k1gMnSc1	Egon
Bondy	bond	k1gInPc4	bond
<g/>
,	,	kIx,	,
Ivan	Ivan	k1gMnSc1	Ivan
Martin	Martin	k1gMnSc1	Martin
Jirous	Jirous	k1gMnSc1	Jirous
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
oficiálních	oficiální	k2eAgMnPc2d1	oficiální
prozaiků	prozaik	k1gMnPc2	prozaik
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
největšího	veliký	k2eAgInSc2d3	veliký
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
věhlasu	věhlas	k1gInSc2	věhlas
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Hrabalem	Hrabal	k1gMnSc7	Hrabal
Ladislav	Ladislav	k1gMnSc1	Ladislav
Fuks	Fuks	k1gMnSc1	Fuks
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
sametové	sametový	k2eAgFnSc6d1	sametová
revoluci	revoluce	k1gFnSc6	revoluce
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
toto	tento	k3xDgNnSc1	tento
rozdělení	rozdělení	k1gNnSc1	rozdělení
literárního	literární	k2eAgInSc2d1	literární
života	život	k1gInSc2	život
padlo	padnout	k5eAaPmAgNnS	padnout
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nových	nový	k2eAgMnPc2d1	nový
autorů	autor	k1gMnPc2	autor
se	se	k3xPyFc4	se
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
nejvíce	nejvíce	k6eAd1	nejvíce
prosadili	prosadit	k5eAaPmAgMnP	prosadit
Michal	Michal	k1gMnSc1	Michal
Viewegh	Viewegh	k1gMnSc1	Viewegh
<g/>
,	,	kIx,	,
Patrik	Patrik	k1gMnSc1	Patrik
Ouředník	Ouředník	k1gMnSc1	Ouředník
a	a	k8xC	a
Jáchym	Jáchym	k1gMnSc1	Jáchym
Topol	Topol	k1gMnSc1	Topol
<g/>
.	.	kIx.	.
<g/>
K	k	k7c3	k
významným	významný	k2eAgMnPc3d1	významný
novinářům	novinář	k1gMnPc3	novinář
krom	krom	k7c2	krom
výše	výše	k1gFnSc2	výše
uvedených	uvedený	k2eAgFnPc2d1	uvedená
patřili	patřit	k5eAaImAgMnP	patřit
Julius	Julius	k1gMnSc1	Julius
Fučík	Fučík	k1gMnSc1	Fučík
<g/>
,	,	kIx,	,
Milena	Milena	k1gFnSc1	Milena
Jesenská	Jesenská	k1gFnSc1	Jesenská
nebo	nebo	k8xC	nebo
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Peroutka	Peroutka	k1gMnSc1	Peroutka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
ČR	ČR	kA	ČR
je	být	k5eAaImIp3nS	být
udělována	udělován	k2eAgFnSc1d1	udělována
významná	významný	k2eAgFnSc1d1	významná
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
literární	literární	k2eAgFnSc1d1	literární
Cena	cena	k1gFnSc1	cena
Franze	Franze	k1gFnSc2	Franze
Kafky	Kafka	k1gMnSc2	Kafka
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
domácích	domácí	k2eAgFnPc2d1	domácí
literárních	literární	k2eAgFnPc2d1	literární
cen	cena	k1gFnPc2	cena
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejvýznamnějším	významný	k2eAgMnPc3d3	nejvýznamnější
Magnesia	magnesium	k1gNnPc1	magnesium
Litera	litera	k1gFnSc1	litera
<g/>
,	,	kIx,	,
Státní	státní	k2eAgFnPc1d1	státní
ceny	cena	k1gFnPc1	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
a	a	k8xC	a
za	za	k7c4	za
překladatelské	překladatelský	k2eAgNnSc4d1	překladatelské
dílo	dílo	k1gNnSc4	dílo
<g/>
,	,	kIx,	,
Cena	cena	k1gFnSc1	cena
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Seiferta	Seifert	k1gMnSc2	Seifert
<g/>
,	,	kIx,	,
Cena	cena	k1gFnSc1	cena
Jiřího	Jiří	k1gMnSc2	Jiří
Ortena	Ortena	k1gFnSc1	Ortena
či	či	k8xC	či
Cena	cena	k1gFnSc1	cena
Josefa	Josef	k1gMnSc2	Josef
Škvoreckého	Škvorecký	k2eAgMnSc2d1	Škvorecký
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ČR	ČR	kA	ČR
má	mít	k5eAaImIp3nS	mít
unikátní	unikátní	k2eAgFnSc1d1	unikátní
síť	síť	k1gFnSc1	síť
veřejných	veřejný	k2eAgFnPc2d1	veřejná
knihoven	knihovna	k1gFnPc2	knihovna
<g/>
,	,	kIx,	,
nejhustší	hustý	k2eAgFnSc1d3	nejhustší
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
5400	[number]	k4	5400
knihovnami	knihovna	k1gFnPc7	knihovna
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středu	střed	k1gInSc6	střed
této	tento	k3xDgFnSc2	tento
sítě	síť	k1gFnSc2	síť
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Národní	národní	k2eAgFnSc1d1	národní
knihovna	knihovna	k1gFnSc1	knihovna
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
pražském	pražský	k2eAgNnSc6d1	Pražské
Klementinu	Klementinum	k1gNnSc6	Klementinum
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejcennějším	cenný	k2eAgFnPc3d3	nejcennější
památkám	památka	k1gFnPc3	památka
schraňovaným	schraňovaný	k2eAgFnPc3d1	schraňovaná
Národní	národní	k2eAgFnSc2d1	národní
knihovnou	knihovna	k1gFnSc7	knihovna
ČR	ČR	kA	ČR
patří	patřit	k5eAaImIp3nS	patřit
rukopis	rukopis	k1gInSc4	rukopis
Kodexu	kodex	k1gInSc2	kodex
vyšehradského	vyšehradský	k2eAgInSc2d1	vyšehradský
(	(	kIx(	(
<g/>
jehož	jehož	k3xOyRp3gFnSc1	jehož
cena	cena	k1gFnSc1	cena
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
na	na	k7c6	na
miliardu	miliarda	k4xCgFnSc4	miliarda
korun	koruna	k1gFnPc2	koruna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Lobkovický	lobkovický	k2eAgInSc1d1	lobkovický
graduál	graduál	k1gInSc1	graduál
<g/>
,	,	kIx,	,
rukopisy	rukopis	k1gInPc1	rukopis
středověké	středověký	k2eAgFnSc2d1	středověká
pražské	pražský	k2eAgFnSc2d1	Pražská
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Knihy	kniha	k1gFnPc1	kniha
na	na	k7c6	na
řetězech	řetěz	k1gInPc6	řetěz
<g/>
"	"	kIx"	"
z	z	k7c2	z
knihovny	knihovna	k1gFnSc2	knihovna
jáchymovské	jáchymovský	k2eAgFnSc2d1	Jáchymovská
městské	městský	k2eAgFnSc2d1	městská
školy	škola	k1gFnSc2	škola
ze	z	k7c2	z
16	[number]	k4	16
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
století	století	k1gNnSc1	století
<g/>
,	,	kIx,	,
Codex	Codex	k1gInSc1	Codex
pictoricus	pictoricus	k1gInSc1	pictoricus
Mexicanus	Mexicanus	k1gInSc1	Mexicanus
misionáře	misionář	k1gMnSc2	misionář
Ignáce	Ignác	k1gMnSc2	Ignác
Tirsche	Tirsch	k1gMnSc2	Tirsch
<g/>
,	,	kIx,	,
autografy	autograf	k1gInPc1	autograf
Jana	Jan	k1gMnSc2	Jan
Husa	Hus	k1gMnSc2	Hus
a	a	k8xC	a
Jakoubka	Jakoubek	k1gMnSc4	Jakoubek
ze	z	k7c2	z
Stříbra	stříbro	k1gNnSc2	stříbro
<g/>
,	,	kIx,	,
sbírka	sbírka	k1gFnSc1	sbírka
grafických	grafický	k2eAgInPc2d1	grafický
listů	list	k1gInPc2	list
univerzitních	univerzitní	k2eAgFnPc2d1	univerzitní
tezí	teze	k1gFnPc2	teze
<g/>
,	,	kIx,	,
list	list	k1gInSc1	list
Gutenbergovy	Gutenbergův	k2eAgFnSc2d1	Gutenbergova
bible	bible	k1gFnSc2	bible
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1454	[number]	k4	1454
<g/>
,	,	kIx,	,
Opatovický	opatovický	k2eAgInSc4d1	opatovický
homiliář	homiliář	k1gInSc4	homiliář
<g/>
,	,	kIx,	,
Mattioliho	Mattioli	k1gMnSc4	Mattioli
herbář	herbář	k1gInSc4	herbář
ze	z	k7c2	z
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
nejstarší	starý	k2eAgInSc1d3	nejstarší
tisk	tisk	k1gInSc1	tisk
na	na	k7c6	na
českém	český	k2eAgNnSc6d1	české
území	území	k1gNnSc6	území
(	(	kIx(	(
<g/>
latinská	latinský	k2eAgNnPc1d1	latinské
Statuta	statuta	k1gNnPc1	statuta
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1349	[number]	k4	1349
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zlomek	zlomek	k1gInSc4	zlomek
Žaltáře	žaltář	k1gInSc2	žaltář
z	z	k7c2	z
Řezna	Řezno	k1gNnSc2	Řezno
pocházející	pocházející	k2eAgFnSc1d1	pocházející
z	z	k7c2	z
konce	konec	k1gInSc2	konec
8	[number]	k4	8
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
řecké	řecký	k2eAgInPc4d1	řecký
papyry	papyr	k1gInPc4	papyr
z	z	k7c2	z
1	[number]	k4	1
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
Svatojiřský	svatojiřský	k2eAgInSc1d1	svatojiřský
antifonář	antifonář	k1gInSc1	antifonář
či	či	k8xC	či
Velislavova	Velislavův	k2eAgFnSc1d1	Velislavova
bible	bible	k1gFnSc1	bible
<g/>
.	.	kIx.	.
</s>
<s>
Zdaleka	zdaleka	k6eAd1	zdaleka
nejcennější	cenný	k2eAgInSc1d3	nejcennější
rukopis	rukopis	k1gInSc1	rukopis
vzniklý	vzniklý	k2eAgInSc1d1	vzniklý
na	na	k7c6	na
českém	český	k2eAgNnSc6d1	české
území	území	k1gNnSc6	území
<g/>
,	,	kIx,	,
perla	perla	k1gFnSc1	perla
středověkého	středověký	k2eAgNnSc2d1	středověké
knihařství	knihařství	k1gNnSc2	knihařství
zvaná	zvaný	k2eAgFnSc1d1	zvaná
Codex	Codex	k1gInSc4	Codex
gigas	gigas	k1gInSc4	gigas
(	(	kIx(	(
<g/>
též	též	k9	též
Ďáblova	ďáblův	k2eAgFnSc1d1	Ďáblova
bible	bible	k1gFnSc1	bible
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
však	však	k9	však
byl	být	k5eAaImAgInS	být
za	za	k7c2	za
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
ukořistěn	ukořistěn	k2eAgInSc1d1	ukořistěn
Švédy	Švéd	k1gMnPc7	Švéd
a	a	k8xC	a
do	do	k7c2	do
ČR	ČR	kA	ČR
bývá	bývat	k5eAaImIp3nS	bývat
jen	jen	k9	jen
výjimečně	výjimečně	k6eAd1	výjimečně
zapůjčován	zapůjčován	k2eAgMnSc1d1	zapůjčován
<g/>
.	.	kIx.	.
</s>
<s>
Vzácné	vzácný	k2eAgInPc1d1	vzácný
tisky	tisk	k1gInPc1	tisk
a	a	k8xC	a
rukopisy	rukopis	k1gInPc1	rukopis
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Žlutický	žlutický	k2eAgInSc1d1	žlutický
kancionál	kancionál	k1gInSc1	kancionál
<g/>
)	)	kIx)	)
schraňuje	schraňovat	k5eAaImIp3nS	schraňovat
též	též	k9	též
Památník	památník	k1gInSc1	památník
národního	národní	k2eAgNnSc2d1	národní
písemnictví	písemnictví	k1gNnSc2	písemnictví
sídlící	sídlící	k2eAgFnSc1d1	sídlící
ve	v	k7c6	v
Strahovském	strahovský	k2eAgInSc6d1	strahovský
klášteře	klášter	k1gInSc6	klášter
<g/>
,	,	kIx,	,
v	v	k7c6	v
letohrádku	letohrádek	k1gInSc6	letohrádek
Hvězda	hvězda	k1gFnSc1	hvězda
a	a	k8xC	a
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
Třetí	třetí	k4xOgFnSc3	třetí
Petschkově	Petschkův	k2eAgFnSc3d1	Petschkova
vile	vila	k1gFnSc3	vila
v	v	k7c6	v
Bubenči	Bubeneč	k1gFnSc6	Bubeneč
<g/>
.	.	kIx.	.
</s>
<s>
Památník	památník	k1gInSc1	památník
disponuje	disponovat	k5eAaBmIp3nS	disponovat
mj.	mj.	kA	mj.
rozsáhlou	rozsáhlý	k2eAgFnSc7d1	rozsáhlá
sbírkou	sbírka	k1gFnSc7	sbírka
ex	ex	k6eAd1	ex
libris	libris	k1gFnPc2	libris
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Divadlo	divadlo	k1gNnSc1	divadlo
===	===	k?	===
</s>
</p>
<p>
<s>
České	český	k2eAgNnSc1d1	české
divadelnictví	divadelnictví	k1gNnSc1	divadelnictví
má	mít	k5eAaImIp3nS	mít
kořeny	kořen	k1gInPc4	kořen
již	již	k6eAd1	již
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarším	starý	k2eAgInSc7d3	nejstarší
dochovaným	dochovaný	k2eAgInSc7d1	dochovaný
dramatickým	dramatický	k2eAgInSc7d1	dramatický
dílem	díl	k1gInSc7	díl
s	s	k7c7	s
využitím	využití	k1gNnSc7	využití
češtiny	čeština	k1gFnSc2	čeština
je	být	k5eAaImIp3nS	být
zlomek	zlomek	k1gInSc1	zlomek
česko-latinské	českoatinský	k2eAgFnSc2d1	česko-latinská
hry	hra	k1gFnSc2	hra
ze	z	k7c2	z
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
zvané	zvaný	k2eAgFnPc1d1	zvaná
obvykle	obvykle	k6eAd1	obvykle
Mastičkář	mastičkář	k1gMnSc1	mastičkář
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
jakousi	jakýsi	k3yIgFnSc4	jakýsi
mezihru	mezihra	k1gFnSc4	mezihra
v	v	k7c6	v
inscenaci	inscenace	k1gFnSc6	inscenace
evangelijních	evangelijní	k2eAgFnPc2d1	evangelijní
scén	scéna	k1gFnPc2	scéna
–	–	k?	–
popisuje	popisovat	k5eAaImIp3nS	popisovat
situaci	situace	k1gFnSc4	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jde	jít	k5eAaImIp3nS	jít
Ježíšova	Ježíšův	k2eAgFnSc1d1	Ježíšova
matka	matka	k1gFnSc1	matka
Marie	Maria	k1gFnSc2	Maria
na	na	k7c4	na
trh	trh	k1gInSc4	trh
koupit	koupit	k5eAaPmF	koupit
vonné	vonný	k2eAgFnPc4d1	vonná
masti	mast	k1gFnPc4	mast
k	k	k7c3	k
nabalzamování	nabalzamování	k1gNnSc3	nabalzamování
mrtvého	mrtvý	k2eAgNnSc2d1	mrtvé
Ježíšova	Ježíšův	k2eAgNnSc2d1	Ježíšovo
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Scéna	scéna	k1gFnSc1	scéna
se	se	k3xPyFc4	se
však	však	k9	však
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
osamostatňovala	osamostatňovat	k5eAaImAgFnS	osamostatňovat
a	a	k8xC	a
popisovala	popisovat	k5eAaImAgFnS	popisovat
satiricky	satiricky	k6eAd1	satiricky
poměry	poměr	k1gInPc1	poměr
v	v	k7c6	v
měšťanském	měšťanský	k2eAgNnSc6d1	měšťanské
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
sehrálo	sehrát	k5eAaPmAgNnS	sehrát
divadlo	divadlo	k1gNnSc1	divadlo
výraznou	výrazný	k2eAgFnSc4d1	výrazná
roli	role	k1gFnSc4	role
v	v	k7c6	v
národním	národní	k2eAgNnSc6d1	národní
obrození	obrození	k1gNnSc6	obrození
(	(	kIx(	(
<g/>
Václav	Václav	k1gMnSc1	Václav
Kliment	Kliment	k1gMnSc1	Kliment
Klicpera	Klicpera	k1gFnSc1	Klicpera
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Kajetán	Kajetán	k1gMnSc1	Kajetán
Tyl	Tyl	k1gMnSc1	Tyl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Emancipační	emancipační	k2eAgFnPc1d1	emancipační
snahy	snaha	k1gFnPc1	snaha
českého	český	k2eAgInSc2d1	český
národa	národ	k1gInSc2	národ
se	se	k3xPyFc4	se
v	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
projevily	projevit	k5eAaPmAgFnP	projevit
otevřením	otevření	k1gNnSc7	otevření
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1883	[number]	k4	1883
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgNnSc1d1	národní
divadlo	divadlo	k1gNnSc1	divadlo
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
uvádí	uvádět	k5eAaImIp3nS	uvádět
jak	jak	k6eAd1	jak
opery	opera	k1gFnSc2	opera
tak	tak	k8xC	tak
činohry	činohra	k1gFnSc2	činohra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
pronikaly	pronikat	k5eAaImAgFnP	pronikat
do	do	k7c2	do
českého	český	k2eAgNnSc2d1	české
divadla	divadlo	k1gNnSc2	divadlo
evropské	evropský	k2eAgInPc4d1	evropský
literární	literární	k2eAgInPc4d1	literární
směry	směr	k1gInPc4	směr
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
realismus	realismus	k1gInSc1	realismus
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
reprezentoval	reprezentovat	k5eAaImAgMnS	reprezentovat
Ladislav	Ladislav	k1gMnSc1	Ladislav
Stroupežnický	Stroupežnický	k2eAgMnSc1d1	Stroupežnický
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
svou	svůj	k3xOyFgFnSc7	svůj
vesnickou	vesnický	k2eAgFnSc7d1	vesnická
veselohrou	veselohra	k1gFnSc7	veselohra
Naši	náš	k3xOp1gMnPc1	náš
furianti	furiant	k1gMnPc1	furiant
<g/>
,	,	kIx,	,
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
zejména	zejména	k9	zejména
bratři	bratr	k1gMnPc1	bratr
Mrštíkové	Mrštíková	k1gFnSc2	Mrštíková
svou	svůj	k3xOyFgFnSc7	svůj
Maryšou	Maryša	k1gFnSc7	Maryša
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
Gabriela	Gabriela	k1gFnSc1	Gabriela
Preissová	Preissová	k1gFnSc1	Preissová
přinesla	přinést	k5eAaPmAgFnS	přinést
na	na	k7c4	na
česká	český	k2eAgNnPc4d1	české
jeviště	jeviště	k1gNnPc4	jeviště
silná	silný	k2eAgNnPc4d1	silné
témata	téma	k1gNnPc4	téma
(	(	kIx(	(
<g/>
Gazdina	gazdina	k1gFnSc1	gazdina
Roba	roba	k1gFnSc1	roba
<g/>
,	,	kIx,	,
Její	její	k3xOp3gFnSc1	její
pastorkyňa	pastorkyňa	k1gFnSc1	pastorkyňa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
tuto	tento	k3xDgFnSc4	tento
moderní	moderní	k2eAgFnSc4d1	moderní
dramatiku	dramatika	k1gFnSc4	dramatika
bojoval	bojovat	k5eAaImAgMnS	bojovat
zejména	zejména	k9	zejména
režisér	režisér	k1gMnSc1	režisér
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Kvapil	Kvapil	k1gMnSc1	Kvapil
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
též	též	k9	též
prezentoval	prezentovat	k5eAaBmAgMnS	prezentovat
jako	jako	k8xC	jako
dramatik	dramatik	k1gMnSc1	dramatik
(	(	kIx(	(
<g/>
Princezna	princezna	k1gFnSc1	princezna
Pampeliška	pampeliška	k1gFnSc1	pampeliška
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
avantgardního	avantgardní	k2eAgNnSc2d1	avantgardní
divadla	divadlo	k1gNnSc2	divadlo
reprezentovaného	reprezentovaný	k2eAgInSc2d1	reprezentovaný
Osvobozeným	osvobozený	k2eAgNnSc7d1	osvobozené
divadlem	divadlo	k1gNnSc7	divadlo
Jiřího	Jiří	k1gMnSc2	Jiří
Voskovce	Voskovec	k1gMnSc2	Voskovec
a	a	k8xC	a
Jana	Jan	k1gMnSc2	Jan
Wericha	Werich	k1gMnSc2	Werich
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Emilem	Emil	k1gMnSc7	Emil
Františkem	František	k1gMnSc7	František
Burianem	Burian	k1gMnSc7	Burian
<g/>
,	,	kIx,	,
Jiřím	Jiří	k1gMnSc7	Jiří
Frejkou	Frejka	k1gFnSc7	Frejka
<g/>
,	,	kIx,	,
Jindřichem	Jindřich	k1gMnSc7	Jindřich
Honzlem	Honzl	k1gMnSc7	Honzl
či	či	k8xC	či
Jiřím	Jiří	k1gMnSc7	Jiří
Mahenem	Mahen	k1gMnSc7	Mahen
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
světových	světový	k2eAgNnPc6d1	světové
jevištích	jeviště	k1gNnPc6	jeviště
se	se	k3xPyFc4	se
nejvíce	hodně	k6eAd3	hodně
prosadily	prosadit	k5eAaPmAgFnP	prosadit
hry	hra	k1gFnPc1	hra
Karla	Karel	k1gMnSc2	Karel
Čapka	Čapek	k1gMnSc2	Čapek
(	(	kIx(	(
<g/>
R.	R.	kA	R.
<g/>
U.	U.	kA	U.
<g/>
R.	R.	kA	R.
<g/>
,	,	kIx,	,
Věc	věc	k1gFnSc1	věc
Makropulos	Makropulos	k1gMnSc1	Makropulos
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Františka	František	k1gMnSc4	František
Langera	Langer	k1gMnSc4	Langer
(	(	kIx(	(
<g/>
Periférie	periférie	k1gFnSc2	periférie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
získal	získat	k5eAaPmAgInS	získat
výjimečný	výjimečný	k2eAgInSc1d1	výjimečný
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
ohlas	ohlas	k1gInSc1	ohlas
projekt	projekt	k1gInSc1	projekt
Alfreda	Alfred	k1gMnSc2	Alfred
Radoka	Radoek	k1gMnSc2	Radoek
Laterna	laterna	k1gFnSc1	laterna
magika	magika	k1gFnSc1	magika
<g/>
,	,	kIx,	,
první	první	k4xOgInSc4	první
multimediální	multimediální	k2eAgInSc4d1	multimediální
umělecký	umělecký	k2eAgInSc4d1	umělecký
projekt	projekt	k1gInSc4	projekt
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
připravený	připravený	k2eAgMnSc1d1	připravený
původně	původně	k6eAd1	původně
pro	pro	k7c4	pro
světovou	světový	k2eAgFnSc4d1	světová
výstavu	výstava	k1gFnSc4	výstava
Expo	Expo	k1gNnSc1	Expo
58	[number]	k4	58
<g/>
.	.	kIx.	.
</s>
<s>
Vznikl	vzniknout	k5eAaPmAgInS	vzniknout
též	též	k9	též
fenomén	fenomén	k1gInSc1	fenomén
divadel	divadlo	k1gNnPc2	divadlo
malých	malý	k2eAgFnPc2d1	malá
forem	forma	k1gFnPc2	forma
<g/>
:	:	kIx,	:
Semafor	Semafor	k1gInSc1	Semafor
(	(	kIx(	(
<g/>
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Šlitr	Šlitr	k1gMnSc1	Šlitr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
divadlo	divadlo	k1gNnSc1	divadlo
Na	na	k7c6	na
zábradlí	zábradlí	k1gNnSc6	zábradlí
(	(	kIx(	(
<g/>
Jan	Jan	k1gMnSc1	Jan
Grossman	Grossman	k1gMnSc1	Grossman
<g/>
,	,	kIx,	,
Ivan	Ivan	k1gMnSc1	Ivan
Vyskočil	Vyskočil	k1gMnSc1	Vyskočil
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Činoherní	činoherní	k2eAgInSc1d1	činoherní
klub	klub	k1gInSc1	klub
(	(	kIx(	(
<g/>
Ladislav	Ladislav	k1gMnSc1	Ladislav
Smoček	Smoček	k1gMnSc1	Smoček
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Husa	husa	k1gFnSc1	husa
na	na	k7c6	na
provázku	provázek	k1gInSc6	provázek
<g/>
,	,	kIx,	,
Ypsilonka	Ypsilonka	k1gFnSc1	Ypsilonka
(	(	kIx(	(
<g/>
Jan	Jan	k1gMnSc1	Jan
Schmid	Schmida	k1gFnPc2	Schmida
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Divadlo	divadlo	k1gNnSc1	divadlo
Sklep	sklep	k1gInSc1	sklep
či	či	k8xC	či
divadlo	divadlo	k1gNnSc1	divadlo
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
(	(	kIx(	(
<g/>
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Svěrák	Svěrák	k1gMnSc1	Svěrák
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
Smoljak	Smoljak	k1gMnSc1	Smoljak
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
nejúspěšnějším	úspěšný	k2eAgMnSc7d3	nejúspěšnější
dramatikem	dramatik	k1gMnSc7	dramatik
stal	stát	k5eAaPmAgMnS	stát
Pavel	Pavel	k1gMnSc1	Pavel
Kohout	Kohout	k1gMnSc1	Kohout
(	(	kIx(	(
<g/>
zejm.	zejm.	k?	zejm.
August	August	k1gMnSc1	August
<g/>
,	,	kIx,	,
August	August	k1gMnSc1	August
<g/>
,	,	kIx,	,
august	august	k1gMnSc1	august
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
absurdní	absurdní	k2eAgNnSc4d1	absurdní
drama	drama	k1gNnSc4	drama
reprezentoval	reprezentovat	k5eAaImAgMnS	reprezentovat
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Zlíně	Zlín	k1gInSc6	Zlín
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
známý	známý	k2eAgMnSc1d1	známý
britský	britský	k2eAgMnSc1d1	britský
dramatik	dramatik	k1gMnSc1	dramatik
Tom	Tom	k1gMnSc1	Tom
Stoppard	Stoppard	k1gMnSc1	Stoppard
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
divadelním	divadelní	k2eAgNnPc3d1	divadelní
oceněním	ocenění	k1gNnPc3	ocenění
patří	patřit	k5eAaImIp3nP	patřit
Ceny	cena	k1gFnPc1	cena
Thálie	Thálie	k1gFnSc1	Thálie
či	či	k8xC	či
Cena	cena	k1gFnSc1	cena
Alfréda	Alfréd	k1gMnSc2	Alfréd
Radoka	Radoek	k1gMnSc2	Radoek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Film	film	k1gInSc1	film
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
éře	éra	k1gFnSc6	éra
němého	němý	k2eAgInSc2d1	němý
filmu	film	k1gInSc2	film
hranice	hranice	k1gFnSc2	hranice
vlasti	vlast	k1gFnSc2	vlast
překročil	překročit	k5eAaPmAgInS	překročit
věhlas	věhlas	k1gInSc1	věhlas
odvážných	odvážný	k2eAgInPc2d1	odvážný
filmových	filmový	k2eAgInPc2d1	filmový
projektů	projekt	k1gInPc2	projekt
Gustava	Gustav	k1gMnSc2	Gustav
Machatého	Machatý	k1gMnSc2	Machatý
Erotikon	Erotikon	k1gMnSc1	Erotikon
(	(	kIx(	(
<g/>
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
a	a	k8xC	a
Extase	extase	k1gFnSc1	extase
(	(	kIx(	(
<g/>
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
ohlasu	ohlas	k1gInSc2	ohlas
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
československá	československý	k2eAgFnSc1d1	Československá
nová	nový	k2eAgFnSc1d1	nová
vlna	vlna	k1gFnSc1	vlna
(	(	kIx(	(
<g/>
Miloš	Miloš	k1gMnSc1	Miloš
Forman	Forman	k1gMnSc1	Forman
<g/>
,	,	kIx,	,
Věra	Věra	k1gFnSc1	Věra
Chytilová	Chytilová	k1gFnSc1	Chytilová
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Menzel	Menzel	k1gMnSc1	Menzel
<g/>
,	,	kIx,	,
Ján	Ján	k1gMnSc1	Ján
Kadár	Kadár	k1gMnSc1	Kadár
<g/>
,	,	kIx,	,
Elmar	Elmar	k1gMnSc1	Elmar
Klos	Klos	k1gInSc1	Klos
<g/>
,	,	kIx,	,
Jaromil	Jaromil	k1gMnSc1	Jaromil
Jireš	Jireš	k1gMnSc1	Jireš
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Vláčil	vláčit	k5eAaImAgMnS	vláčit
<g/>
,	,	kIx,	,
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Jasný	jasný	k2eAgMnSc1d1	jasný
<g/>
,	,	kIx,	,
Juraj	Juraj	k1gFnSc1	Juraj
Jakubisko	Jakubisko	k1gNnSc1	Jakubisko
<g/>
,	,	kIx,	,
Juraj	Juraj	k1gMnSc1	Juraj
Herz	Herz	k1gMnSc1	Herz
<g/>
,	,	kIx,	,
Ivan	Ivan	k1gMnSc1	Ivan
Passer	Passer	k1gMnSc1	Passer
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Němec	Němec	k1gMnSc1	Němec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Klíčovými	klíčový	k2eAgInPc7d1	klíčový
snímky	snímek	k1gInPc7	snímek
československé	československý	k2eAgFnSc2d1	Československá
nové	nový	k2eAgFnSc2d1	nová
vlny	vlna	k1gFnSc2	vlna
byly	být	k5eAaImAgInP	být
Ostře	ostro	k6eAd1	ostro
sledované	sledovaný	k2eAgInPc1d1	sledovaný
vlaky	vlak	k1gInPc1	vlak
<g/>
,	,	kIx,	,
Obchod	obchod	k1gInSc1	obchod
na	na	k7c6	na
korze	korzo	k1gNnSc6	korzo
<g/>
,	,	kIx,	,
Lásky	láska	k1gFnPc1	láska
jedné	jeden	k4xCgFnSc2	jeden
plavovlásky	plavovláska	k1gFnSc2	plavovláska
<g/>
,	,	kIx,	,
Hoří	hořet	k5eAaImIp3nS	hořet
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
panenko	panenka	k1gFnSc5	panenka
<g/>
,	,	kIx,	,
Sedmikrásky	sedmikráska	k1gFnSc2	sedmikráska
<g/>
,	,	kIx,	,
Černý	Černý	k1gMnSc1	Černý
Petr	Petr	k1gMnSc1	Petr
<g/>
,	,	kIx,	,
Skřivánci	Skřivánek	k1gMnPc1	Skřivánek
na	na	k7c4	na
niti	nit	k1gFnPc4	nit
<g/>
,	,	kIx,	,
Markéta	Markéta	k1gFnSc1	Markéta
Lazarová	Lazarová	k1gFnSc1	Lazarová
<g/>
,	,	kIx,	,
Až	až	k6eAd1	až
přijde	přijít	k5eAaPmIp3nS	přijít
kocour	kocour	k1gMnSc1	kocour
<g/>
,	,	kIx,	,
Spalovač	spalovač	k1gMnSc1	spalovač
mrtvol	mrtvola	k1gFnPc2	mrtvola
<g/>
,	,	kIx,	,
Transport	transport	k1gInSc1	transport
z	z	k7c2	z
ráje	ráj	k1gInSc2	ráj
<g/>
,	,	kIx,	,
Rozmarné	rozmarný	k2eAgNnSc1d1	Rozmarné
léto	léto	k1gNnSc1	léto
<g/>
,	,	kIx,	,
Všichni	všechen	k3xTgMnPc1	všechen
dobří	dobrý	k2eAgMnPc1d1	dobrý
rodáci	rodák	k1gMnPc1	rodák
<g/>
,	,	kIx,	,
Démanty	démant	k1gInPc1	démant
noci	noc	k1gFnSc2	noc
<g/>
,	,	kIx,	,
Intimní	intimní	k2eAgNnSc1d1	intimní
osvětlení	osvětlení	k1gNnSc1	osvětlení
<g/>
,	,	kIx,	,
Žert	žert	k1gInSc1	žert
či	či	k8xC	či
O	o	k7c6	o
slavnosti	slavnost	k1gFnSc6	slavnost
a	a	k8xC	a
hostech	host	k1gMnPc6	host
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vážné	vážný	k2eAgFnPc4d1	vážná
umělecké	umělecký	k2eAgFnPc4d1	umělecká
ambice	ambice	k1gFnPc4	ambice
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
naplnit	naplnit	k5eAaPmF	naplnit
Karlu	Karla	k1gFnSc4	Karla
Kachyňovi	Kachyňův	k2eAgMnPc1d1	Kachyňův
(	(	kIx(	(
<g/>
Ucho	ucho	k1gNnSc4	ucho
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Otakaru	Otakar	k1gMnSc3	Otakar
Vávrovi	Vávra	k1gMnSc3	Vávra
<g/>
,	,	kIx,	,
Jiřímu	Jiří	k1gMnSc3	Jiří
Krejčíkovi	Krejčík	k1gMnSc3	Krejčík
(	(	kIx(	(
<g/>
Vyšší	vysoký	k2eAgInSc1d2	vyšší
princip	princip	k1gInSc1	princip
<g/>
)	)	kIx)	)
či	či	k8xC	či
Karlu	Karel	k1gMnSc3	Karel
Zemanovi	Zeman	k1gMnSc3	Zeman
(	(	kIx(	(
<g/>
Vynález	vynález	k1gInSc1	vynález
zkázy	zkáza	k1gFnSc2	zkáza
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
30	[number]	k4	30
<g/>
.	.	kIx.	.
až	až	k9	až
50	[number]	k4	50
<g/>
.	.	kIx.	.
léta	léto	k1gNnPc4	léto
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
70	[number]	k4	70
<g/>
.	.	kIx.	.
až	až	k9	až
80	[number]	k4	80
<g/>
.	.	kIx.	.
léta	léto	k1gNnSc2	léto
ovšem	ovšem	k9	ovšem
svědčila	svědčit	k5eAaImAgFnS	svědčit
více	hodně	k6eAd2	hodně
populárnímu	populární	k2eAgInSc3d1	populární
filmu	film	k1gInSc3	film
než	než	k8xS	než
uměleckému	umělecký	k2eAgMnSc3d1	umělecký
<g/>
,	,	kIx,	,
s	s	k7c7	s
nadčasovou	nadčasový	k2eAgFnSc7d1	nadčasová
elegancí	elegance	k1gFnSc7	elegance
tohoto	tento	k3xDgInSc2	tento
prostoru	prostor	k1gInSc2	prostor
využili	využít	k5eAaPmAgMnP	využít
třeba	třeba	k9	třeba
režiséři	režisér	k1gMnPc1	režisér
Bořivoj	Bořivoj	k1gMnSc1	Bořivoj
Zeman	Zeman	k1gMnSc1	Zeman
<g/>
,	,	kIx,	,
Oldřich	Oldřich	k1gMnSc1	Oldřich
Lipský	lipský	k2eAgMnSc1d1	lipský
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Vorlíček	Vorlíček	k1gMnSc1	Vorlíček
(	(	kIx(	(
<g/>
Tři	tři	k4xCgInPc1	tři
oříšky	oříšek	k1gInPc1	oříšek
pro	pro	k7c4	pro
Popelku	Popelka	k1gFnSc4	Popelka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
Frič	Frič	k1gMnSc1	Frič
a	a	k8xC	a
Ladislav	Ladislav	k1gMnSc1	Ladislav
Smoljak	Smoljak	k1gMnSc1	Smoljak
<g/>
,	,	kIx,	,
scenáristé	scenárista	k1gMnPc1	scenárista
Jiří	Jiří	k1gMnSc1	Jiří
Brdečka	Brdečka	k1gMnSc1	Brdečka
(	(	kIx(	(
<g/>
Limonádový	limonádový	k2eAgMnSc1d1	limonádový
Joe	Joe	k1gMnSc1	Joe
<g/>
,	,	kIx,	,
Adéla	Adéla	k1gFnSc1	Adéla
ještě	ještě	k6eAd1	ještě
nevečeřela	večeřet	k5eNaImAgFnS	večeřet
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Miloš	Miloš	k1gMnSc1	Miloš
Macourek	Macourek	k1gMnSc1	Macourek
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Svěrák	Svěrák	k1gMnSc1	Svěrák
nebo	nebo	k8xC	nebo
skladatelé	skladatel	k1gMnPc1	skladatel
filmové	filmový	k2eAgFnSc2d1	filmová
hudby	hudba	k1gFnSc2	hudba
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Liška	Liška	k1gMnSc1	Liška
<g/>
,	,	kIx,	,
Luboš	Luboš	k1gMnSc1	Luboš
Fišer	Fišer	k1gMnSc1	Fišer
a	a	k8xC	a
Petr	Petr	k1gMnSc1	Petr
Hapka	Hapka	k1gMnSc1	Hapka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Snímky	snímek	k1gInPc1	snímek
Obchod	obchod	k1gInSc1	obchod
na	na	k7c6	na
korze	korzo	k1gNnSc6	korzo
(	(	kIx(	(
<g/>
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ostře	ostro	k6eAd1	ostro
sledované	sledovaný	k2eAgInPc1d1	sledovaný
vlaky	vlak	k1gInPc1	vlak
(	(	kIx(	(
<g/>
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
a	a	k8xC	a
Kolja	Kolja	k1gMnSc1	Kolja
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
získaly	získat	k5eAaPmAgFnP	získat
Oscara	Oscar	k1gMnSc2	Oscar
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
neanglicky	anglicky	k6eNd1	anglicky
mluvený	mluvený	k2eAgInSc4d1	mluvený
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
šest	šest	k4xCc1	šest
dalších	další	k2eAgInPc2d1	další
filmů	film	k1gInPc2	film
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
do	do	k7c2	do
užší	úzký	k2eAgFnSc2d2	užší
nominace	nominace	k1gFnSc2	nominace
<g/>
:	:	kIx,	:
Lásky	láska	k1gFnPc1	láska
jedné	jeden	k4xCgFnSc2	jeden
plavovlásky	plavovláska	k1gFnSc2	plavovláska
(	(	kIx(	(
<g/>
1966	[number]	k4	1966
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Hoří	hořet	k5eAaImIp3nP	hořet
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
panenko	panenka	k1gFnSc5	panenka
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vesničko	vesnička	k1gFnSc5	vesnička
má	mít	k5eAaImIp3nS	mít
středisková	střediskový	k2eAgFnSc1d1	středisková
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Obecná	obecný	k2eAgFnSc1d1	obecná
škola	škola	k1gFnSc1	škola
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Musíme	muset	k5eAaImIp1nP	muset
si	se	k3xPyFc3	se
pomáhat	pomáhat	k5eAaImF	pomáhat
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
a	a	k8xC	a
Želary	Želar	k1gInPc1	Želar
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
věhlasu	věhlas	k1gInSc2	věhlas
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
českým	český	k2eAgInPc3d1	český
loutkovým	loutkový	k2eAgInPc3d1	loutkový
a	a	k8xC	a
animovaným	animovaný	k2eAgInPc3d1	animovaný
filmům	film	k1gInPc3	film
režisérů	režisér	k1gMnPc2	režisér
jako	jako	k8xS	jako
byli	být	k5eAaImAgMnP	být
Jiří	Jiří	k1gMnSc1	Jiří
Trnka	Trnka	k1gMnSc1	Trnka
<g/>
,	,	kIx,	,
Hermína	Hermína	k1gFnSc1	Hermína
Týrlová	Týrlová	k1gFnSc1	Týrlová
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Miler	Miler	k1gMnSc1	Miler
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Švankmajer	Švankmajer	k1gMnSc1	Švankmajer
(	(	kIx(	(
<g/>
zejm.	zejm.	k?	zejm.
Něco	něco	k6eAd1	něco
z	z	k7c2	z
Alenky	Alenka	k1gFnSc2	Alenka
<g/>
)	)	kIx)	)
a	a	k8xC	a
Břetislav	Břetislav	k1gMnSc1	Břetislav
Pojar	Pojar	k1gMnSc1	Pojar
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
této	tento	k3xDgFnSc3	tento
tradici	tradice	k1gFnSc3	tradice
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
televizní	televizní	k2eAgInSc1d1	televizní
cyklus	cyklus	k1gInSc1	cyklus
Večerníčků	večerníček	k1gInPc2	večerníček
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgMnSc6	jenž
spolupracovali	spolupracovat	k5eAaImAgMnP	spolupracovat
i	i	k9	i
přední	přední	k2eAgMnPc1d1	přední
výtvarníci	výtvarník	k1gMnPc1	výtvarník
jako	jako	k8xS	jako
Adolf	Adolf	k1gMnSc1	Adolf
Born	Born	k1gMnSc1	Born
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Smetana	Smetana	k1gMnSc1	Smetana
či	či	k8xC	či
Vladimír	Vladimír	k1gMnSc1	Vladimír
Jiránek	Jiránek	k1gMnSc1	Jiránek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Především	především	k9	především
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
než	než	k8xS	než
českých	český	k2eAgFnPc6d1	Česká
kinematografiích	kinematografie	k1gFnPc6	kinematografie
se	se	k3xPyFc4	se
prosadila	prosadit	k5eAaPmAgFnS	prosadit
řada	řada	k1gFnSc1	řada
českých	český	k2eAgMnPc2d1	český
rodáků	rodák	k1gMnPc2	rodák
<g/>
,	,	kIx,	,
z	z	k7c2	z
režisérů	režisér	k1gMnPc2	režisér
například	například	k6eAd1	například
Georg	Georg	k1gMnSc1	Georg
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Pabst	Pabst	k1gMnSc1	Pabst
a	a	k8xC	a
Karel	Karel	k1gMnSc1	Karel
Reisz	Reisz	k1gMnSc1	Reisz
<g/>
,	,	kIx,	,
z	z	k7c2	z
kameramanů	kameraman	k1gMnPc2	kameraman
Karl	Karl	k1gMnSc1	Karl
Freund	Freund	k1gMnSc1	Freund
<g/>
,	,	kIx,	,
z	z	k7c2	z
herců	herec	k1gMnPc2	herec
Herbert	Herbert	k1gMnSc1	Herbert
Lom	lom	k1gInSc1	lom
nebo	nebo	k8xC	nebo
Barbara	Barbara	k1gFnSc1	Barbara
Bouchetová	Bouchetový	k2eAgFnSc1d1	Bouchetový
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
českých	český	k2eAgMnPc2d1	český
herců	herec	k1gMnPc2	herec
patří	patřit	k5eAaImIp3nS	patřit
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
k	k	k7c3	k
nejznámějším	známý	k2eAgFnPc3d3	nejznámější
Karel	Karel	k1gMnSc1	Karel
Roden	Roden	k1gInSc1	Roden
a	a	k8xC	a
Libuše	Libuše	k1gFnSc1	Libuše
Šafránková	Šafránková	k1gFnSc1	Šafránková
<g/>
,	,	kIx,	,
populární	populární	k2eAgFnSc1d1	populární
díky	díky	k7c3	díky
pohádce	pohádka	k1gFnSc6	pohádka
Tři	tři	k4xCgInPc1	tři
oříšky	oříšek	k1gInPc1	oříšek
pro	pro	k7c4	pro
Popelku	Popelka	k1gFnSc4	Popelka
zejména	zejména	k9	zejména
v	v	k7c6	v
Norsku	Norsko	k1gNnSc6	Norsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Filmové	filmový	k2eAgInPc4d1	filmový
ateliéry	ateliér	k1gInPc4	ateliér
Barrandov	Barrandov	k1gInSc1	Barrandov
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
největším	veliký	k2eAgFnPc3d3	veliký
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
je	on	k3xPp3gNnPc4	on
využívají	využívat	k5eAaPmIp3nP	využívat
zejména	zejména	k9	zejména
zahraniční	zahraniční	k2eAgFnPc1d1	zahraniční
produkce	produkce	k1gFnPc1	produkce
<g/>
,	,	kIx,	,
vznikaly	vznikat	k5eAaImAgInP	vznikat
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
mj.	mj.	kA	mj.
filmy	film	k1gInPc1	film
Mission	Mission	k1gInSc1	Mission
<g/>
:	:	kIx,	:
Impossible	Impossible	k1gMnSc5	Impossible
<g/>
,	,	kIx,	,
Casino	Casina	k1gMnSc5	Casina
Royale	Royal	k1gMnSc5	Royal
<g/>
,	,	kIx,	,
Letopisy	letopis	k1gInPc1	letopis
Narnie	Narnie	k1gFnSc2	Narnie
<g/>
:	:	kIx,	:
Princ	princ	k1gMnSc1	princ
Kaspian	Kaspian	k1gMnSc1	Kaspian
<g/>
,	,	kIx,	,
Letopisy	letopis	k1gInPc1	letopis
Narnie	Narnie	k1gFnSc2	Narnie
<g/>
:	:	kIx,	:
Lev	Lev	k1gMnSc1	Lev
<g/>
,	,	kIx,	,
čarodějnice	čarodějnice	k1gFnSc1	čarodějnice
a	a	k8xC	a
skříň	skříň	k1gFnSc1	skříň
<g/>
,	,	kIx,	,
Dítě	Dítě	k2eAgNnSc1d1	Dítě
číslo	číslo	k1gNnSc1	číslo
44	[number]	k4	44
<g/>
,	,	kIx,	,
Hannibal	Hannibal	k1gInSc1	Hannibal
–	–	k?	–
Zrození	zrození	k1gNnSc1	zrození
<g/>
,	,	kIx,	,
Iluzionista	iluzionista	k1gMnSc1	iluzionista
<g/>
,	,	kIx,	,
Vetřelec	vetřelec	k1gMnSc1	vetřelec
vs	vs	k?	vs
<g/>
.	.	kIx.	.
</s>
<s>
Predátor	predátor	k1gMnSc1	predátor
<g/>
,	,	kIx,	,
Agent	agent	k1gMnSc1	agent
bez	bez	k7c2	bez
minulosti	minulost	k1gFnSc2	minulost
aj.	aj.	kA	aj.
</s>
</p>
<p>
<s>
===	===	k?	===
Hudba	hudba	k1gFnSc1	hudba
===	===	k?	===
</s>
</p>
<p>
<s>
Česká	český	k2eAgFnSc1d1	Česká
hudba	hudba	k1gFnSc1	hudba
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgInPc4	svůj
kořeny	kořen	k1gInPc4	kořen
v	v	k7c6	v
nejméně	málo	k6eAd3	málo
1000	[number]	k4	1000
let	léto	k1gNnPc2	léto
staré	starý	k2eAgFnSc2d1	stará
duchovní	duchovní	k2eAgFnSc4d1	duchovní
hudbě	hudba	k1gFnSc3	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc1d3	nejstarší
duchovní	duchovní	k2eAgFnSc1d1	duchovní
píseň	píseň	k1gFnSc1	píseň
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
byla	být	k5eAaImAgFnS	být
staroslověnská	staroslověnský	k2eAgFnSc1d1	staroslověnská
<g/>
:	:	kIx,	:
Hospodine	Hospodin	k1gMnSc5	Hospodin
<g/>
,	,	kIx,	,
pomiluj	pomilovat	k5eAaPmRp2nS	pomilovat
ny	ny	k?	ny
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
na	na	k7c6	na
konci	konec	k1gInSc6	konec
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
počátku	počátek	k1gInSc2	počátek
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
je	být	k5eAaImIp3nS	být
zřetelně	zřetelně	k6eAd1	zřetelně
staroslověnský	staroslověnský	k2eAgMnSc1d1	staroslověnský
<g/>
,	,	kIx,	,
pronikly	proniknout	k5eAaPmAgFnP	proniknout
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
ovšem	ovšem	k9	ovšem
i	i	k9	i
prvky	prvek	k1gInPc1	prvek
staročeštiny	staročeština	k1gFnSc2	staročeština
(	(	kIx(	(
<g/>
patrně	patrně	k6eAd1	patrně
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
času	čas	k1gInSc2	čas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc4	první
duchovní	duchovní	k2eAgFnSc4d1	duchovní
píseň	píseň	k1gFnSc4	píseň
ve	v	k7c6	v
staročeštině	staročeština	k1gFnSc6	staročeština
Svatý	svatý	k1gMnSc5	svatý
Václave	Václav	k1gMnSc5	Václav
<g/>
,	,	kIx,	,
vévodo	vévoda	k1gMnSc5	vévoda
české	český	k2eAgFnPc4d1	Česká
země	zem	k1gFnSc2	zem
(	(	kIx(	(
<g/>
též	též	k9	též
Svatováclavský	svatováclavský	k2eAgInSc1d1	svatováclavský
chorál	chorál	k1gInSc1	chorál
<g/>
)	)	kIx)	)
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
ve	v	k7c6	v
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Zapsána	zapsat	k5eAaPmNgFnS	zapsat
byla	být	k5eAaImAgFnS	být
ale	ale	k8xC	ale
až	až	k9	až
ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
kronice	kronika	k1gFnSc6	kronika
Beneše	Beneš	k1gMnSc2	Beneš
Krabice	krabice	k1gFnSc2	krabice
z	z	k7c2	z
Veitmile	Veitmil	k1gMnSc5	Veitmil
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
významná	významný	k2eAgFnSc1d1	významná
je	být	k5eAaImIp3nS	být
Ostrovská	ostrovský	k2eAgFnSc1d1	Ostrovská
píseň	píseň	k1gFnSc1	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Zabývá	zabývat	k5eAaImIp3nS	zabývat
se	s	k7c7	s
přítomností	přítomnost	k1gFnSc7	přítomnost
Krista	Kristus	k1gMnSc2	Kristus
ve	v	k7c6	v
svátosti	svátost	k1gFnSc6	svátost
oltářní	oltářní	k2eAgFnSc6d1	oltářní
<g/>
.	.	kIx.	.
</s>
<s>
Zapsána	zapsán	k2eAgFnSc1d1	zapsána
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
kodexu	kodex	k1gInSc6	kodex
kláštera	klášter	k1gInSc2	klášter
z	z	k7c2	z
Ostrova	ostrov	k1gInSc2	ostrov
u	u	k7c2	u
Davle	Davle	k1gFnSc2	Davle
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
prvního	první	k4xOgInSc2	první
verše	verš	k1gInSc2	verš
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
též	též	k6eAd1	též
říká	říkat	k5eAaImIp3nS	říkat
Slovo	slovo	k1gNnSc1	slovo
do	do	k7c2	do
světa	svět	k1gInSc2	svět
stvorenie	stvorenie	k1gFnSc2	stvorenie
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
svatováclavského	svatováclavský	k2eAgInSc2d1	svatováclavský
chorálu	chorál	k1gInSc2	chorál
již	již	k6eAd1	již
složitější	složitý	k2eAgFnSc4d2	složitější
básnickou	básnický	k2eAgFnSc4d1	básnická
formu	forma	k1gFnSc4	forma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Významným	významný	k2eAgInSc7d1	významný
centrem	centr	k1gInSc7	centr
středověké	středověký	k2eAgFnSc2d1	středověká
hudby	hudba	k1gFnSc2	hudba
byla	být	k5eAaImAgFnS	být
Šumava	Šumava	k1gFnSc1	Šumava
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
hudební	hudební	k2eAgFnPc1d1	hudební
paměti	paměť	k1gFnPc1	paměť
zde	zde	k6eAd1	zde
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
knihovny	knihovna	k1gFnSc2	knihovna
cisterciáckého	cisterciácký	k2eAgInSc2d1	cisterciácký
kláštera	klášter	k1gInSc2	klášter
ve	v	k7c6	v
Vyšším	vysoký	k2eAgInSc6d2	vyšší
Brodě	Brod	k1gInSc6	Brod
<g/>
,	,	kIx,	,
založeného	založený	k2eAgInSc2d1	založený
roku	rok	k1gInSc2	rok
1259	[number]	k4	1259
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
rukopis	rukopis	k1gInSc1	rukopis
č.	č.	k?	č.
42	[number]	k4	42
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1410	[number]	k4	1410
zde	zde	k6eAd1	zde
popisuje	popisovat	k5eAaImIp3nS	popisovat
píseň	píseň	k1gFnSc4	píseň
Jezu	Jezu	k1gMnSc5	Jezu
Kriste	Kristus	k1gMnSc5	Kristus
<g/>
,	,	kIx,	,
ščedrý	ščedrý	k2eAgInSc1d1	ščedrý
kněže	kněže	k?	kněže
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
si	se	k3xPyFc3	se
zpívali	zpívat	k5eAaImAgMnP	zpívat
také	také	k9	také
husité	husita	k1gMnPc1	husita
<g/>
.	.	kIx.	.
</s>
<s>
Husitskou	husitský	k2eAgFnSc4d1	husitská
písňovou	písňový	k2eAgFnSc4d1	písňová
tvorbu	tvorba	k1gFnSc4	tvorba
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
tvorbu	tvorba	k1gFnSc4	tvorba
přelomu	přelom	k1gInSc2	přelom
14	[number]	k4	14
<g/>
.	.	kIx.	.
a	a	k8xC	a
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
zachoval	zachovat	k5eAaPmAgInS	zachovat
Jistebnický	jistebnický	k2eAgInSc1d1	jistebnický
kancionál	kancionál	k1gInSc1	kancionál
(	(	kIx(	(
<g/>
zejm.	zejm.	k?	zejm.
Ktož	Ktož	k1gFnSc1	Ktož
jsú	jsú	k?	jsú
boží	boží	k2eAgMnPc1d1	boží
bojovníci	bojovník	k1gMnPc1	bojovník
a	a	k8xC	a
Povstaň	povstat	k5eAaPmRp2nS	povstat
<g/>
,	,	kIx,	,
povstaň	povstat	k5eAaPmRp2nS	povstat
veliké	veliký	k2eAgNnSc4d1	veliké
město	město	k1gNnSc4	město
pražské	pražský	k2eAgFnSc2d1	Pražská
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Důležitou	důležitý	k2eAgFnSc7d1	důležitá
etapou	etapa	k1gFnSc7	etapa
ve	v	k7c6	v
vývoji	vývoj	k1gInSc6	vývoj
české	český	k2eAgFnSc2d1	Česká
hudby	hudba	k1gFnSc2	hudba
bylo	být	k5eAaImAgNnS	být
baroko	baroko	k1gNnSc1	baroko
(	(	kIx(	(
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
hudba	hudba	k1gFnSc1	hudba
ustavila	ustavit	k5eAaPmAgFnS	ustavit
jako	jako	k9	jako
samostatný	samostatný	k2eAgInSc4d1	samostatný
profesionální	profesionální	k2eAgInSc4d1	profesionální
obor	obor	k1gInSc4	obor
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
první	první	k4xOgMnPc4	první
české	český	k2eAgMnPc4d1	český
hudební	hudební	k2eAgMnPc4d1	hudební
skladatele	skladatel	k1gMnPc4	skladatel
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
barokní	barokní	k2eAgMnPc4d1	barokní
tvůrce	tvůrce	k1gMnPc4	tvůrce
jako	jako	k9	jako
byli	být	k5eAaImAgMnP	být
Adam	Adam	k1gMnSc1	Adam
Michna	Michna	k1gMnSc1	Michna
z	z	k7c2	z
Otradovic	Otradovice	k1gFnPc2	Otradovice
<g/>
,	,	kIx,	,
Heinrich	Heinrich	k1gMnSc1	Heinrich
Biber	Biber	k1gMnSc1	Biber
(	(	kIx(	(
<g/>
český	český	k2eAgMnSc1d1	český
Němec	Němec	k1gMnSc1	Němec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Dismas	Dismas	k1gMnSc1	Dismas
Zelenka	Zelenka	k1gMnSc1	Zelenka
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
Rejcha	Rejcha	k1gMnSc1	Rejcha
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Václav	Václav	k1gMnSc1	Václav
Stamic	Stamic	k1gMnSc1	Stamic
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Mysliveček	Mysliveček	k1gMnSc1	Mysliveček
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Ladislav	Ladislav	k1gMnSc1	Ladislav
Dusík	dusík	k1gInSc1	dusík
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Antonín	Antonín	k1gMnSc1	Antonín
Benda	Benda	k1gMnSc1	Benda
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Xaver	Xaver	k1gMnSc1	Xaver
Richter	Richter	k1gMnSc1	Richter
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Křtitel	křtitel	k1gMnSc1	křtitel
Vaňhal	Vaňhal	k1gMnSc1	Vaňhal
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Xaver	Xavera	k1gFnPc2	Xavera
Brixi	Brixe	k1gFnSc4	Brixe
či	či	k8xC	či
Leopold	Leopold	k1gMnSc1	Leopold
Koželuh	koželuh	k1gMnSc1	koželuh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
národního	národní	k2eAgNnSc2d1	národní
obrození	obrození	k1gNnSc2	obrození
na	na	k7c4	na
barokní	barokní	k2eAgFnSc4d1	barokní
hudbu	hudba	k1gFnSc4	hudba
bezprostředně	bezprostředně	k6eAd1	bezprostředně
navázali	navázat	k5eAaPmAgMnP	navázat
Jan	Jan	k1gMnSc1	Jan
Jakub	Jakub	k1gMnSc1	Jakub
Ryba	Ryba	k1gMnSc1	Ryba
či	či	k8xC	či
Václav	Václav	k1gMnSc1	Václav
Jan	Jan	k1gMnSc1	Jan
Křtitel	křtitel	k1gMnSc1	křtitel
Tomášek	Tomášek	k1gMnSc1	Tomášek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Toto	tento	k3xDgNnSc1	tento
byla	být	k5eAaImAgFnS	být
základna	základna	k1gFnSc1	základna
<g/>
,	,	kIx,	,
na	na	k7c4	na
niž	jenž	k3xRgFnSc4	jenž
se	se	k3xPyFc4	se
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
mohli	moct	k5eAaImAgMnP	moct
postavit	postavit	k5eAaPmF	postavit
klíčoví	klíčový	k2eAgMnPc1d1	klíčový
autoři	autor	k1gMnPc1	autor
moderní	moderní	k2eAgFnSc2d1	moderní
české	český	k2eAgFnSc2d1	Česká
vážné	vážný	k2eAgFnSc2d1	vážná
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
Bedřich	Bedřich	k1gMnSc1	Bedřich
Smetana	Smetana	k1gMnSc1	Smetana
(	(	kIx(	(
<g/>
zejm.	zejm.	k?	zejm.
Má	mít	k5eAaImIp3nS	mít
vlast	vlast	k1gFnSc1	vlast
<g/>
,	,	kIx,	,
Prodaná	prodaný	k2eAgFnSc1d1	prodaná
nevěsta	nevěsta	k1gFnSc1	nevěsta
<g/>
)	)	kIx)	)
a	a	k8xC	a
Antonín	Antonín	k1gMnSc1	Antonín
Dvořák	Dvořák	k1gMnSc1	Dvořák
<g/>
,	,	kIx,	,
nejslavnější	slavný	k2eAgMnSc1d3	nejslavnější
český	český	k2eAgMnSc1d1	český
skladatel	skladatel	k1gMnSc1	skladatel
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
(	(	kIx(	(
<g/>
zejm.	zejm.	k?	zejm.
Novosvětská	novosvětský	k2eAgFnSc1d1	Novosvětská
<g/>
,	,	kIx,	,
Rusalka	rusalka	k1gFnSc1	rusalka
a	a	k8xC	a
Slovanské	slovanský	k2eAgInPc1d1	slovanský
tance	tanec	k1gInPc1	tanec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tradice	tradice	k1gFnSc1	tradice
pak	pak	k6eAd1	pak
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
s	s	k7c7	s
neztenčenou	ztenčený	k2eNgFnSc7d1	neztenčená
silou	síla	k1gFnSc7	síla
<g/>
,	,	kIx,	,
především	především	k9	především
díla	dílo	k1gNnSc2	dílo
Leoše	Leoš	k1gMnSc2	Leoš
Janáčka	Janáček	k1gMnSc2	Janáček
pronikla	proniknout	k5eAaPmAgFnS	proniknout
do	do	k7c2	do
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
repertoáru	repertoár	k1gInSc2	repertoár
mnoha	mnoho	k4c2	mnoho
operních	operní	k2eAgInPc2d1	operní
domů	dům	k1gInPc2	dům
jsou	být	k5eAaImIp3nP	být
všechny	všechen	k3xTgFnPc1	všechen
jeho	jeho	k3xOp3gFnPc1	jeho
opery	opera	k1gFnPc1	opera
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
Její	její	k3xOp3gFnSc1	její
pastorkyňa	pastorkyňa	k1gFnSc1	pastorkyňa
(	(	kIx(	(
<g/>
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Jenůfa	Jenůfa	k1gFnSc1	Jenůfa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Káťa	Káťa	k1gFnSc1	Káťa
Kabanová	Kabanová	k1gFnSc1	Kabanová
<g/>
,	,	kIx,	,
Z	z	k7c2	z
mrtvého	mrtvý	k2eAgInSc2d1	mrtvý
domu	dům	k1gInSc2	dům
a	a	k8xC	a
Věc	věc	k1gFnSc1	věc
Makropulos	Makropulosa	k1gFnPc2	Makropulosa
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
uváděny	uvádět	k5eAaImNgFnP	uvádět
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
jeho	jeho	k3xOp3gInPc1	jeho
Glagolská	glagolský	k2eAgFnSc1d1	Glagolská
mše	mše	k1gFnSc1	mše
a	a	k8xC	a
Sinfonietta	Sinfonietta	k1gFnSc1	Sinfonietta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgMnPc1d1	další
významní	významný	k2eAgMnPc1d1	významný
čeští	český	k2eAgMnPc1d1	český
hudební	hudební	k2eAgMnPc1d1	hudební
skladatelé	skladatel	k1gMnPc1	skladatel
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
jsou	být	k5eAaImIp3nP	být
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Fibich	Fibich	k1gMnSc1	Fibich
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Foerster	Foerster	k1gMnSc1	Foerster
<g/>
,	,	kIx,	,
Vítězslav	Vítězslav	k1gMnSc1	Vítězslav
Novák	Novák	k1gMnSc1	Novák
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Suk	Suk	k1gMnSc1	Suk
starší	starší	k1gMnSc1	starší
(	(	kIx(	(
<g/>
zeť	zeť	k1gMnSc1	zeť
Antonína	Antonín	k1gMnSc2	Antonín
Dvořáka	Dvořák	k1gMnSc2	Dvořák
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
Hába	Hába	k1gMnSc1	Hába
<g/>
,	,	kIx,	,
Ervín	Ervín	k1gMnSc1	Ervín
Schulhoff	Schulhoff	k1gMnSc1	Schulhoff
<g/>
,	,	kIx,	,
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Martinů	Martinů	k1gMnSc1	Martinů
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Haas	Haas	k1gInSc1	Haas
<g/>
,	,	kIx,	,
Viktor	Viktor	k1gMnSc1	Viktor
Ullmann	Ullmann	k1gMnSc1	Ullmann
či	či	k8xC	či
Petr	Petr	k1gMnSc1	Petr
Eben	eben	k1gInSc1	eben
<g/>
.	.	kIx.	.
</s>
<s>
Překvapivého	překvapivý	k2eAgInSc2d1	překvapivý
úspěchu	úspěch	k1gInSc2	úspěch
mj.	mj.	kA	mj.
na	na	k7c6	na
scéně	scéna	k1gFnSc6	scéna
newyorské	newyorský	k2eAgFnSc6d1	newyorská
Metropolitan	metropolitan	k1gInSc1	metropolitan
Opera	opera	k1gFnSc1	opera
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
Jaromír	Jaromír	k1gMnSc1	Jaromír
Weinberger	Weinberger	k1gMnSc1	Weinberger
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
operou	opera	k1gFnSc7	opera
Švanda	Švanda	k1gMnSc1	Švanda
dudák	dudák	k1gMnSc1	dudák
<g/>
.	.	kIx.	.
</s>
<s>
Známými	známý	k2eAgMnPc7d1	známý
operetními	operetní	k2eAgMnPc7d1	operetní
skladateli	skladatel	k1gMnPc7	skladatel
byli	být	k5eAaImAgMnP	být
mj.	mj.	kA	mj.
Oskar	Oskar	k1gMnSc1	Oskar
Nedbal	Nedbal	k1gMnSc1	Nedbal
či	či	k8xC	či
Rudolf	Rudolf	k1gMnSc1	Rudolf
Friml	Friml	k1gMnSc1	Friml
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1860	[number]	k4	1860
se	se	k3xPyFc4	se
v	v	k7c6	v
Kališti	kaliště	k1gNnSc6	kaliště
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
narodil	narodit	k5eAaPmAgMnS	narodit
světově	světově	k6eAd1	světově
proslulý	proslulý	k2eAgMnSc1d1	proslulý
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
mluvící	mluvící	k2eAgMnSc1d1	mluvící
Gustav	Gustav	k1gMnSc1	Gustav
Mahler	Mahler	k1gMnSc1	Mahler
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
Hollywoodu	Hollywood	k1gInSc6	Hollywood
se	se	k3xPyFc4	se
prosadil	prosadit	k5eAaPmAgMnS	prosadit
brněnský	brněnský	k2eAgMnSc1d1	brněnský
rodák	rodák	k1gMnSc1	rodák
Erich	Erich	k1gMnSc1	Erich
Korngold	Korngold	k1gMnSc1	Korngold
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
se	se	k3xPyFc4	se
narodili	narodit	k5eAaPmAgMnP	narodit
i	i	k9	i
klavírní	klavírní	k2eAgMnSc1d1	klavírní
virtuózové	virtuózové	k?	virtuózové
Ignaz	Ignaz	k1gInSc1	Ignaz
Moscheles	Moscheles	k1gInSc1	Moscheles
<g/>
,	,	kIx,	,
Alfred	Alfred	k1gMnSc1	Alfred
Brendel	Brendlo	k1gNnPc2	Brendlo
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
Serkin	Serkin	k1gMnSc1	Serkin
a	a	k8xC	a
Alice	Alice	k1gFnSc1	Alice
Herz-Sommerová	Herz-Sommerová	k1gFnSc1	Herz-Sommerová
<g/>
,	,	kIx,	,
z	z	k7c2	z
vídeňské	vídeňský	k2eAgFnSc2d1	Vídeňská
české	český	k2eAgFnSc2d1	Česká
komunity	komunita	k1gFnSc2	komunita
vzešel	vzejít	k5eAaPmAgMnS	vzejít
Ernst	Ernst	k1gMnSc1	Ernst
Křenek	Křenek	k1gMnSc1	Křenek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prosadili	prosadit	k5eAaPmAgMnP	prosadit
se	se	k3xPyFc4	se
i	i	k9	i
čeští	český	k2eAgMnPc1d1	český
hudební	hudební	k2eAgMnPc1d1	hudební
interpreti	interpret	k1gMnPc1	interpret
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
jmenovat	jmenovat	k5eAaImF	jmenovat
mnoho	mnoho	k4c4	mnoho
jmen	jméno	k1gNnPc2	jméno
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
dirigenti	dirigent	k1gMnPc1	dirigent
Rafael	Rafael	k1gMnSc1	Rafael
Kubelík	Kubelík	k1gMnSc1	Kubelík
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Talich	Talich	k1gMnSc1	Talich
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Neumann	Neumann	k1gMnSc1	Neumann
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Ančerl	Ančerl	k1gMnSc1	Ančerl
nebo	nebo	k8xC	nebo
Jiří	Jiří	k1gMnSc1	Jiří
Bělohlávek	Bělohlávek	k1gMnSc1	Bělohlávek
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
pak	pak	k6eAd1	pak
Petr	Petr	k1gMnSc1	Petr
Altrichter	Altrichter	k1gMnSc1	Altrichter
<g/>
,	,	kIx,	,
Tomáš	Tomáš	k1gMnSc1	Tomáš
Netopil	topit	k5eNaImAgMnS	topit
a	a	k8xC	a
Jakub	Jakub	k1gMnSc1	Jakub
Hrůša	Hrůša	k1gMnSc1	Hrůša
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
hudební	hudební	k2eAgMnPc1d1	hudební
instrumentalisté	instrumentalista	k1gMnPc1	instrumentalista
<g/>
:	:	kIx,	:
houslisté	houslista	k1gMnPc1	houslista
František	František	k1gMnSc1	František
Benda	Benda	k1gMnSc1	Benda
<g/>
,	,	kIx,	,
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Živný	živný	k2eAgMnSc1d1	živný
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Křtitel	křtitel	k1gMnSc1	křtitel
Václav	Václav	k1gMnSc1	Václav
Kalivoda	Kalivoda	k1gMnSc1	Kalivoda
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Ondříček	Ondříček	k1gMnSc1	Ondříček
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Kubelík	Kubelík	k1gMnSc1	Kubelík
a	a	k8xC	a
Josef	Josef	k1gMnSc1	Josef
Suk	Suk	k1gMnSc1	Suk
mladší	mladý	k2eAgMnSc1d2	mladší
(	(	kIx(	(
<g/>
Dvořákův	Dvořákův	k2eAgMnSc1d1	Dvořákův
pravnuk	pravnuk	k1gMnSc1	pravnuk
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
violoncellista	violoncellista	k1gMnSc1	violoncellista
David	David	k1gMnSc1	David
Popper	Popper	k1gMnSc1	Popper
<g/>
,	,	kIx,	,
cembalistka	cembalistka	k1gFnSc1	cembalistka
Zuzana	Zuzana	k1gFnSc1	Zuzana
Růžičková	Růžičková	k1gFnSc1	Růžičková
či	či	k8xC	či
hornista	hornista	k1gMnSc1	hornista
Radek	Radek	k1gMnSc1	Radek
Baborák	baborák	k1gInSc4	baborák
<g/>
.	.	kIx.	.
</s>
<s>
Nelze	lze	k6eNd1	lze
opomenout	opomenout	k5eAaPmF	opomenout
pěvkyně	pěvkyně	k1gFnPc4	pěvkyně
jako	jako	k9	jako
byly	být	k5eAaImAgFnP	být
nebo	nebo	k8xC	nebo
jsou	být	k5eAaImIp3nP	být
Ema	Ema	k1gFnSc1	Ema
Destinnová	Destinnová	k1gFnSc1	Destinnová
<g/>
,	,	kIx,	,
Maria	Maria	k1gFnSc1	Maria
Jeritza	Jeritza	k1gFnSc1	Jeritza
<g/>
,	,	kIx,	,
Jarmila	Jarmila	k1gFnSc1	Jarmila
Novotná	Novotná	k1gFnSc1	Novotná
<g/>
,	,	kIx,	,
Gabriela	Gabriela	k1gFnSc1	Gabriela
Beňačková	Beňačková	k1gFnSc1	Beňačková
<g/>
,	,	kIx,	,
Eva	Eva	k1gFnSc1	Eva
Urbanová	Urbanová	k1gFnSc1	Urbanová
a	a	k8xC	a
Magdalena	Magdalena	k1gFnSc1	Magdalena
Kožená	kožený	k2eAgFnSc1d1	kožená
<g/>
.	.	kIx.	.
</s>
<s>
Velkými	velký	k2eAgMnPc7d1	velký
českými	český	k2eAgMnPc7d1	český
tenoristy	tenorista	k1gMnPc7	tenorista
byli	být	k5eAaImAgMnP	být
Beno	Beno	k6eAd1	Beno
Blachut	Blachut	k1gMnSc1	Blachut
a	a	k8xC	a
Ivo	Ivo	k1gMnSc1	Ivo
Žídek	Žídek	k1gMnSc1	Žídek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
velmi	velmi	k6eAd1	velmi
úspěšný	úspěšný	k2eAgMnSc1d1	úspěšný
je	být	k5eAaImIp3nS	být
barytonista	barytonista	k1gMnSc1	barytonista
Adam	Adam	k1gMnSc1	Adam
Plachetka	plachetka	k1gFnSc1	plachetka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
hudebních	hudební	k2eAgNnPc2d1	hudební
těles	těleso	k1gNnPc2	těleso
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
významu	význam	k1gInSc2	význam
nejen	nejen	k6eAd1	nejen
symfonický	symfonický	k2eAgInSc1d1	symfonický
orchestr	orchestr	k1gInSc1	orchestr
Česká	český	k2eAgFnSc1d1	Česká
filharmonie	filharmonie	k1gFnSc1	filharmonie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
další	další	k2eAgInPc1d1	další
symfonické	symfonický	k2eAgInPc1d1	symfonický
a	a	k8xC	a
komorní	komorní	k2eAgInPc1d1	komorní
orchestry	orchestr	k1gInPc1	orchestr
a	a	k8xC	a
nově	nova	k1gFnSc3	nova
tzv.	tzv.	kA	tzv.
barokní	barokní	k2eAgInPc1d1	barokní
soubory	soubor	k1gInPc1	soubor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
byly	být	k5eAaImAgFnP	být
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
úspěšné	úspěšný	k2eAgInPc1d1	úspěšný
také	také	k9	také
české	český	k2eAgInPc1d1	český
pěvecké	pěvecký	k2eAgInPc1d1	pěvecký
sbory	sbor	k1gInPc1	sbor
a	a	k8xC	a
malá	malý	k2eAgNnPc1d1	malé
hudební	hudební	k2eAgNnPc1d1	hudební
tělesa	těleso	k1gNnPc1	těleso
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
smyčcové	smyčcový	k2eAgInPc4d1	smyčcový
kvartety	kvartet	k1gInPc4	kvartet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
také	také	k9	také
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
významný	významný	k2eAgInSc1d1	významný
hudební	hudební	k2eAgInSc1d1	hudební
festival	festival	k1gInSc1	festival
vážné	vážný	k2eAgFnSc2d1	vážná
hudby	hudba	k1gFnSc2	hudba
Pražské	pražský	k2eAgNnSc1d1	Pražské
jaro	jaro	k1gNnSc1	jaro
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
tradičně	tradičně	k6eAd1	tradičně
koná	konat	k5eAaImIp3nS	konat
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
pražských	pražský	k2eAgInPc6d1	pražský
koncertních	koncertní	k2eAgInPc6d1	koncertní
sálech	sál	k1gInPc6	sál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Češi	Čech	k1gMnPc1	Čech
se	se	k3xPyFc4	se
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
ujali	ujmout	k5eAaPmAgMnP	ujmout
i	i	k9	i
v	v	k7c6	v
nových	nový	k2eAgInPc6d1	nový
žánrech	žánr	k1gInPc6	žánr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jazzu	jazz	k1gInSc6	jazz
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Ježek	Ježek	k1gMnSc1	Ježek
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
Vitouš	Vitouš	k1gMnSc1	Vitouš
<g/>
,	,	kIx,	,
orchestr	orchestr	k1gInSc4	orchestr
Gustava	Gustav	k1gMnSc2	Gustav
Broma	Brom	k1gMnSc2	Brom
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
populární	populární	k2eAgFnSc6d1	populární
hudbě	hudba	k1gFnSc6	hudba
působili	působit	k5eAaImAgMnP	působit
Jan	Jan	k1gMnSc1	Jan
Hammer	Hammer	k1gMnSc1	Hammer
nebo	nebo	k8xC	nebo
Karel	Karel	k1gMnSc1	Karel
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
v	v	k7c6	v
hudbě	hudba	k1gFnSc6	hudba
folkové	folkový	k2eAgFnSc2d1	folková
Karel	Karel	k1gMnSc1	Karel
Kryl	Kryl	k1gMnSc1	Kryl
či	či	k8xC	či
Jaromír	Jaromír	k1gMnSc1	Jaromír
Nohavica	Nohavica	k1gMnSc1	Nohavica
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
populární	populární	k2eAgFnSc4d1	populární
dechovou	dechový	k2eAgFnSc4d1	dechová
hudbu	hudba	k1gFnSc4	hudba
rakouskou	rakouský	k2eAgFnSc4d1	rakouská
(	(	kIx(	(
<g/>
zejména	zejména	k6eAd1	zejména
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
rozvinula	rozvinout	k5eAaPmAgFnS	rozvinout
i	i	k9	i
její	její	k3xOp3gFnSc1	její
specifická	specifický	k2eAgFnSc1d1	specifická
česká	český	k2eAgFnSc1d1	Česká
verze	verze	k1gFnSc1	verze
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
díky	díky	k7c3	díky
Františku	František	k1gMnSc3	František
Kmochovi	kmochův	k2eAgMnPc5d1	kmochův
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
světě	svět	k1gInSc6	svět
nejznámější	známý	k2eAgFnSc2d3	nejznámější
české	český	k2eAgFnSc2d1	Česká
melodie	melodie	k1gFnSc2	melodie
jsou	být	k5eAaImIp3nP	být
dodnes	dodnes	k6eAd1	dodnes
právě	právě	k9	právě
ty	ten	k3xDgFnPc1	ten
dechovkové	dechovkový	k2eAgFnPc1d1	dechovková
(	(	kIx(	(
<g/>
zejm.	zejm.	k?	zejm.
Vjezd	vjezd	k1gInSc1	vjezd
gladiátorů	gladiátor	k1gMnPc2	gladiátor
Julia	Julius	k1gMnSc2	Julius
Fučíka	Fučík	k1gMnSc2	Fučík
a	a	k8xC	a
Škoda	škoda	k1gFnSc1	škoda
lásky	láska	k1gFnSc2	láska
Jaromíra	Jaromír	k1gMnSc2	Jaromír
Vejvody	Vejvoda	k1gMnSc2	Vejvoda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pop-music	popusic	k1gFnSc6	pop-music
si	se	k3xPyFc3	se
vysokou	vysoký	k2eAgFnSc4d1	vysoká
popularitu	popularita	k1gFnSc4	popularita
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
i	i	k8xC	i
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
udržuje	udržovat	k5eAaImIp3nS	udržovat
zpěvák	zpěvák	k1gMnSc1	zpěvák
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
soutěži	soutěž	k1gFnSc6	soutěž
Eurovision	Eurovision	k1gInSc1	Eurovision
Song	song	k1gInSc4	song
Contest	Contest	k1gFnSc4	Contest
z	z	k7c2	z
českých	český	k2eAgMnPc2d1	český
reprezentantů	reprezentant	k1gMnPc2	reprezentant
nejvíce	hodně	k6eAd3	hodně
uspěl	uspět	k5eAaPmAgMnS	uspět
Mikolas	Mikolas	k1gMnSc1	Mikolas
Josef	Josef	k1gMnSc1	Josef
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Výtvarné	výtvarný	k2eAgNnSc1d1	výtvarné
umění	umění	k1gNnSc1	umění
===	===	k?	===
</s>
</p>
<p>
<s>
U	u	k7c2	u
prvního	první	k4xOgMnSc2	první
a	a	k8xC	a
také	také	k6eAd1	také
zdaleka	zdaleka	k6eAd1	zdaleka
nejslavnějšího	slavný	k2eAgNnSc2d3	nejslavnější
českého	český	k2eAgNnSc2d1	české
výtvarného	výtvarný	k2eAgNnSc2d1	výtvarné
díla	dílo	k1gNnSc2	dílo
autora	autor	k1gMnSc2	autor
neznáme	neznat	k5eAaImIp1nP	neznat
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
sošku	soška	k1gFnSc4	soška
Věstonické	věstonický	k2eAgFnSc2d1	Věstonická
venuše	venuše	k1gFnSc2	venuše
<g/>
,	,	kIx,	,
patrně	patrně	k6eAd1	patrně
nejstarší	starý	k2eAgFnSc4d3	nejstarší
keramickou	keramický	k2eAgFnSc4d1	keramická
sošku	soška	k1gFnSc4	soška
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
mladším	mladý	k2eAgInSc6d2	mladší
paleolitu	paleolit	k1gInSc6	paleolit
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
nalezena	nalezen	k2eAgFnSc1d1	nalezena
roku	rok	k1gInSc2	rok
1925	[number]	k4	1925
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Většina	většina	k1gFnSc1	většina
výtvarných	výtvarný	k2eAgMnPc2d1	výtvarný
umělců	umělec	k1gMnPc2	umělec
však	však	k9	však
byla	být	k5eAaImAgFnS	být
anonymních	anonymní	k2eAgFnPc2d1	anonymní
ještě	ještě	k9	ještě
v	v	k7c6	v
éře	éra	k1gFnSc6	éra
gotické	gotický	k2eAgFnSc6d1	gotická
<g/>
,	,	kIx,	,
malířství	malířství	k1gNnSc1	malířství
bylo	být	k5eAaImAgNnS	být
tehdy	tehdy	k6eAd1	tehdy
ostatně	ostatně	k6eAd1	ostatně
vnímáno	vnímat	k5eAaImNgNnS	vnímat
jako	jako	k9	jako
řemeslo	řemeslo	k1gNnSc1	řemeslo
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
autor	autor	k1gMnSc1	autor
není	být	k5eNaImIp3nS	být
tak	tak	k6eAd1	tak
podstatný	podstatný	k2eAgInSc1d1	podstatný
<g/>
,	,	kIx,	,
a	a	k8xC	a
nikoli	nikoli	k9	nikoli
jako	jako	k9	jako
umění	umění	k1gNnSc1	umění
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
umělcích	umělec	k1gMnPc6	umělec
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
hovoříme	hovořit	k5eAaImIp1nP	hovořit
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
pojmů	pojem	k1gInPc2	pojem
jako	jako	k9	jako
Mistr	mistr	k1gMnSc1	mistr
litoměřického	litoměřický	k2eAgInSc2d1	litoměřický
oltáře	oltář	k1gInSc2	oltář
<g/>
,	,	kIx,	,
Mistr	mistr	k1gMnSc1	mistr
třeboňského	třeboňský	k2eAgInSc2d1	třeboňský
oltáře	oltář	k1gInSc2	oltář
<g/>
,	,	kIx,	,
Mistr	mistr	k1gMnSc1	mistr
vyšebrodského	vyšebrodský	k2eAgInSc2d1	vyšebrodský
oltáře	oltář	k1gInSc2	oltář
či	či	k8xC	či
Mistr	mistr	k1gMnSc1	mistr
Theodorik	Theodorik	k1gMnSc1	Theodorik
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Autorské	autorský	k2eAgNnSc1d1	autorské
malířství	malířství	k1gNnSc1	malířství
přišlo	přijít	k5eAaPmAgNnS	přijít
na	na	k7c4	na
scénu	scéna	k1gFnSc4	scéna
v	v	k7c6	v
baroku	baroko	k1gNnSc6	baroko
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejvýznamnějším	významný	k2eAgMnPc3d3	nejvýznamnější
českým	český	k2eAgMnPc3d1	český
barokním	barokní	k2eAgMnPc3d1	barokní
malířům	malíř	k1gMnPc3	malíř
patřili	patřit	k5eAaImAgMnP	patřit
Karel	Karel	k1gMnSc1	Karel
Škréta	Škréta	k1gMnSc1	Škréta
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Kupecký	kupecký	k2eAgMnSc1d1	kupecký
a	a	k8xC	a
Petr	Petr	k1gMnSc1	Petr
Brandl	Brandl	k1gMnSc1	Brandl
<g/>
.	.	kIx.	.
</s>
<s>
Zcela	zcela	k6eAd1	zcela
zvláštní	zvláštní	k2eAgNnSc4d1	zvláštní
postavení	postavení	k1gNnSc4	postavení
měl	mít	k5eAaImAgMnS	mít
Václav	Václav	k1gMnSc1	Václav
Hollar	Hollar	k1gMnSc1	Hollar
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
proslul	proslout	k5eAaPmAgMnS	proslout
svými	svůj	k3xOyFgFnPc7	svůj
rytinami	rytina	k1gFnPc7	rytina
<g/>
.	.	kIx.	.
</s>
<s>
Vrcholná	vrcholný	k2eAgNnPc1d1	vrcholné
sochařská	sochařský	k2eAgNnPc1d1	sochařské
díla	dílo	k1gNnPc1	dílo
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
éře	éra	k1gFnSc6	éra
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
Matyáš	Matyáš	k1gMnSc1	Matyáš
Bernard	Bernard	k1gMnSc1	Bernard
Braun	Braun	k1gMnSc1	Braun
a	a	k8xC	a
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Maxmilián	Maxmilián	k1gMnSc1	Maxmilián
Brokoff	Brokoff	k1gMnSc1	Brokoff
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
probíhal	probíhat	k5eAaImAgInS	probíhat
proces	proces	k1gInSc4	proces
českého	český	k2eAgNnSc2d1	české
národního	národní	k2eAgNnSc2d1	národní
obrození	obrození	k1gNnSc2	obrození
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
obrozenci	obrozenec	k1gMnPc1	obrozenec
na	na	k7c6	na
malířství	malířství	k1gNnSc6	malířství
nekladli	klást	k5eNaImAgMnP	klást
takový	takový	k3xDgInSc4	takový
důraz	důraz	k1gInSc4	důraz
jako	jako	k9	jako
na	na	k7c4	na
literaturu	literatura	k1gFnSc4	literatura
<g/>
,	,	kIx,	,
divadlo	divadlo	k1gNnSc4	divadlo
či	či	k8xC	či
vědu	věda	k1gFnSc4	věda
<g/>
.	.	kIx.	.
</s>
<s>
Malířství	malířství	k1gNnSc1	malířství
zůstávalo	zůstávat	k5eAaImAgNnS	zůstávat
na	na	k7c6	na
pozici	pozice	k1gFnSc6	pozice
řemesla	řemeslo	k1gNnSc2	řemeslo
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
v	v	k7c6	v
krajinářství	krajinářství	k1gNnSc6	krajinářství
vynikal	vynikat	k5eAaImAgMnS	vynikat
Josef	Josef	k1gMnSc1	Josef
Matěj	Matěj	k1gMnSc1	Matěj
Navrátil	Navrátil	k1gMnSc1	Navrátil
<g/>
,	,	kIx,	,
v	v	k7c6	v
portrétu	portrét	k1gInSc6	portrét
a	a	k8xC	a
zátiší	zátiší	k1gNnSc1	zátiší
Karel	Karla	k1gFnPc2	Karla
Purkyně	Purkyně	k1gFnSc2	Purkyně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zlom	zlom	k1gInSc1	zlom
přišel	přijít	k5eAaPmAgInS	přijít
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
do	do	k7c2	do
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
dorazila	dorazit	k5eAaPmAgFnS	dorazit
vlna	vlna	k1gFnSc1	vlna
romantismu	romantismus	k1gInSc2	romantismus
a	a	k8xC	a
realismu	realismus	k1gInSc2	realismus
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějším	významný	k2eAgMnSc7d3	nejvýznamnější
představitelem	představitel	k1gMnSc7	představitel
malířského	malířský	k2eAgInSc2d1	malířský
romantismu	romantismus	k1gInSc2	romantismus
byl	být	k5eAaImAgMnS	být
Josef	Josef	k1gMnSc1	Josef
Mánes	Mánes	k1gMnSc1	Mánes
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
známý	známý	k2eAgInSc1d1	známý
především	především	k9	především
díky	díky	k7c3	díky
výzdobě	výzdoba	k1gFnSc3	výzdoba
pražského	pražský	k2eAgInSc2d1	pražský
orloje	orloj	k1gInSc2	orloj
<g/>
.	.	kIx.	.
</s>
<s>
Realistickou	realistický	k2eAgFnSc4d1	realistická
malbu	malba	k1gFnSc4	malba
zvolil	zvolit	k5eAaPmAgMnS	zvolit
například	například	k6eAd1	například
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Čermák	Čermák	k1gMnSc1	Čermák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Se	s	k7c7	s
70	[number]	k4	70
<g/>
.	.	kIx.	.
a	a	k8xC	a
80	[number]	k4	80
<g/>
.	.	kIx.	.
lety	léto	k1gNnPc7	léto
nastoupila	nastoupit	k5eAaPmAgFnS	nastoupit
tzv.	tzv.	kA	tzv.
Generace	generace	k1gFnSc1	generace
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
tvůrci	tvůrce	k1gMnPc1	tvůrce
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
nějak	nějak	k6eAd1	nějak
podíleli	podílet	k5eAaImAgMnP	podílet
na	na	k7c6	na
výzdobě	výzdoba	k1gFnSc6	výzdoba
právě	právě	k6eAd1	právě
stavěné	stavěný	k2eAgFnSc2d1	stavěná
"	"	kIx"	"
<g/>
Zlaté	zlatý	k2eAgFnSc2d1	zlatá
kapličky	kaplička	k1gFnSc2	kaplička
<g/>
"	"	kIx"	"
<g/>
:	:	kIx,	:
Mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
se	se	k3xPyFc4	se
největšího	veliký	k2eAgInSc2d3	veliký
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
ohlasu	ohlas	k1gInSc2	ohlas
dostalo	dostat	k5eAaPmAgNnS	dostat
Mikoláši	Mikoláš	k1gMnSc6	Mikoláš
Alšovi	Aleš	k1gMnSc6	Aleš
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalším	další	k2eAgMnPc3d1	další
členům	člen	k1gMnPc3	člen
generace	generace	k1gFnSc2	generace
patřili	patřit	k5eAaImAgMnP	patřit
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Hynais	Hynais	k1gFnSc2	Hynais
<g/>
,	,	kIx,	,
Julius	Julius	k1gMnSc1	Julius
Mařák	Mařák	k1gMnSc1	Mařák
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Brožík	Brožík	k1gMnSc1	Brožík
<g/>
,	,	kIx,	,
Jakub	Jakub	k1gMnSc1	Jakub
Schikaneder	Schikaneder	k1gMnSc1	Schikaneder
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Ženíšek	Ženíšek	k1gMnSc1	Ženíšek
či	či	k8xC	či
Josef	Josef	k1gMnSc1	Josef
Tulka	tulka	k1gFnSc1	tulka
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
této	tento	k3xDgFnSc3	tento
generaci	generace	k1gFnSc3	generace
patřil	patřit	k5eAaImAgMnS	patřit
i	i	k8xC	i
sochař	sochař	k1gMnSc1	sochař
Josef	Josef	k1gMnSc1	Josef
Václav	Václav	k1gMnSc1	Václav
Myslbek	Myslbek	k1gMnSc1	Myslbek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Většina	většina	k1gFnSc1	většina
z	z	k7c2	z
autorů	autor	k1gMnPc2	autor
Generace	generace	k1gFnSc2	generace
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
nadále	nadále	k6eAd1	nadále
oscilovala	oscilovat	k5eAaImAgNnP	oscilovat
mezi	mezi	k7c7	mezi
romantickou	romantický	k2eAgFnSc7d1	romantická
a	a	k8xC	a
realistickou	realistický	k2eAgFnSc7d1	realistická
malbou	malba	k1gFnSc7	malba
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
krajiny	krajina	k1gFnSc2	krajina
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
však	však	k9	však
brzy	brzy	k6eAd1	brzy
některým	některý	k3yIgMnPc3	některý
autorům	autor	k1gMnPc3	autor
nestačilo	stačit	k5eNaBmAgNnS	stačit
<g/>
.	.	kIx.	.
</s>
<s>
Krajinář	krajinář	k1gMnSc1	krajinář
Antonín	Antonín	k1gMnSc1	Antonín
Chittussi	Chittusse	k1gFnSc4	Chittusse
začal	začít	k5eAaPmAgMnS	začít
měnit	měnit	k5eAaImF	měnit
techniku	technika	k1gFnSc4	technika
krajinomalby	krajinomalba	k1gFnSc2	krajinomalba
až	až	k9	až
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
na	na	k7c4	na
pokraj	pokraj	k1gInSc4	pokraj
impresionismu	impresionismus	k1gInSc2	impresionismus
<g/>
.	.	kIx.	.
</s>
<s>
Vrcholným	vrcholný	k2eAgMnSc7d1	vrcholný
představitelem	představitel	k1gMnSc7	představitel
tohoto	tento	k3xDgInSc2	tento
směru	směr	k1gInSc2	směr
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
stal	stát	k5eAaPmAgMnS	stát
Antonín	Antonín	k1gMnSc1	Antonín
Slavíček	Slavíček	k1gMnSc1	Slavíček
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
Luďka	Luděk	k1gMnSc2	Luděk
Marolda	Marold	k1gMnSc2	Marold
je	být	k5eAaImIp3nS	být
zase	zase	k9	zase
někdy	někdy	k6eAd1	někdy
označováno	označovat	k5eAaImNgNnS	označovat
za	za	k7c4	za
předzvěst	předzvěst	k1gFnSc4	předzvěst
malby	malba	k1gFnSc2	malba
secesní	secesní	k2eAgFnSc2d1	secesní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Právě	právě	k9	právě
secese	secese	k1gFnSc1	secese
pak	pak	k6eAd1	pak
patří	patřit	k5eAaImIp3nS	patřit
ke	k	k7c3	k
klíčovým	klíčový	k2eAgInPc3d1	klíčový
směrům	směr	k1gInPc3	směr
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
vynořily	vynořit	k5eAaPmAgInP	vynořit
na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
prostředí	prostředí	k1gNnSc6	prostředí
sehrála	sehrát	k5eAaPmAgFnS	sehrát
mimořádnou	mimořádný	k2eAgFnSc4d1	mimořádná
roli	role	k1gFnSc4	role
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnSc1	její
hlavní	hlavní	k2eAgMnSc1d1	hlavní
představitel	představitel	k1gMnSc1	představitel
<g/>
,	,	kIx,	,
Alfons	Alfons	k1gMnSc1	Alfons
Mucha	Mucha	k1gMnSc1	Mucha
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
nejznámějším	známý	k2eAgMnSc7d3	nejznámější
českým	český	k2eAgMnSc7d1	český
malířem	malíř	k1gMnSc7	malíř
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Mucha	Mucha	k1gMnSc1	Mucha
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgMnS	proslavit
krom	krom	k7c2	krom
svých	svůj	k3xOyFgInPc2	svůj
známých	známý	k2eAgInPc2d1	známý
plakátů	plakát	k1gInPc2	plakát
také	také	k6eAd1	také
cyklem	cyklus	k1gInSc7	cyklus
20	[number]	k4	20
velkoformátových	velkoformátový	k2eAgInPc2d1	velkoformátový
obrazů	obraz	k1gInPc2	obraz
Slovanská	slovanský	k2eAgFnSc1d1	Slovanská
epopej	epopej	k1gFnSc1	epopej
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
shrnuje	shrnovat	k5eAaImIp3nS	shrnovat
dějiny	dějiny	k1gFnPc4	dějiny
českého	český	k2eAgInSc2d1	český
národa	národ	k1gInSc2	národ
a	a	k8xC	a
Slovanů	Slovan	k1gInPc2	Slovan
<g/>
.	.	kIx.	.
</s>
<s>
Vystavena	vystaven	k2eAgFnSc1d1	vystavena
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
Veletržním	veletržní	k2eAgInSc6d1	veletržní
paláci	palác	k1gInSc6	palác
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
bývala	bývat	k5eAaImAgFnS	bývat
v	v	k7c6	v
Moravském	moravský	k2eAgInSc6d1	moravský
Krumlově	Krumlov	k1gInSc6	Krumlov
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
secesi	secese	k1gFnSc3	secese
lze	lze	k6eAd1	lze
řadit	řadit	k5eAaImF	řadit
i	i	k9	i
dílo	dílo	k1gNnSc1	dílo
Maxe	Max	k1gMnSc2	Max
Švabinského	Švabinský	k2eAgMnSc2d1	Švabinský
a	a	k8xC	a
Jana	Jan	k1gMnSc2	Jan
Preislera	Preisler	k1gMnSc2	Preisler
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
i	i	k8xC	i
významní	významný	k2eAgMnPc1d1	významný
sochaři	sochař	k1gMnPc1	sochař
<g/>
:	:	kIx,	:
František	František	k1gMnSc1	František
Bílek	Bílek	k1gMnSc1	Bílek
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Štursa	Štursa	k1gFnSc1	Štursa
či	či	k8xC	či
Ladislav	Ladislav	k1gMnSc1	Ladislav
Šaloun	Šaloun	k1gMnSc1	Šaloun
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Secese	secese	k1gFnSc1	secese
provokovala	provokovat	k5eAaImAgFnS	provokovat
svým	svůj	k3xOyFgInSc7	svůj
sklonem	sklon	k1gInSc7	sklon
k	k	k7c3	k
užitkovosti	užitkovost	k1gFnSc3	užitkovost
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
jinak	jinak	k6eAd1	jinak
ctila	ctít	k5eAaImAgFnS	ctít
techniky	technika	k1gFnSc2	technika
klasické	klasický	k2eAgFnSc6d1	klasická
a	a	k8xC	a
akademické	akademický	k2eAgFnSc6d1	akademická
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
těm	ten	k3xDgMnPc3	ten
se	se	k3xPyFc4	se
však	však	k8xC	však
na	na	k7c6	na
konci	konec	k1gInSc6	konec
století	století	k1gNnSc2	století
začaly	začít	k5eAaPmAgFnP	začít
bouřit	bouřit	k5eAaImF	bouřit
nové	nový	k2eAgInPc4d1	nový
směry	směr	k1gInPc4	směr
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k9	především
expresionismus	expresionismus	k1gInSc1	expresionismus
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
české	český	k2eAgFnSc2d1	Česká
expresionistické	expresionistický	k2eAgFnSc2d1	expresionistická
skupiny	skupina	k1gFnSc2	skupina
Osma	osma	k1gFnSc1	osma
patřili	patřit	k5eAaImAgMnP	patřit
Bohumil	Bohumil	k1gMnSc1	Bohumil
Kubišta	Kubišta	k1gMnSc1	Kubišta
<g/>
,	,	kIx,	,
Emil	Emil	k1gMnSc1	Emil
Filla	Filla	k1gMnSc1	Filla
či	či	k8xC	či
Otakar	Otakar	k1gMnSc1	Otakar
Kubín	Kubín	k1gMnSc1	Kubín
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
Osmy	osma	k1gFnSc2	osma
pak	pak	k6eAd1	pak
přecházeli	přecházet	k5eAaImAgMnP	přecházet
ke	k	k7c3	k
kubismu	kubismus	k1gInSc3	kubismus
<g/>
,	,	kIx,	,
dalšímu	další	k2eAgNnSc3d1	další
novému	nový	k2eAgNnSc3d1	nové
avantgardnímu	avantgardní	k2eAgNnSc3d1	avantgardní
směru	směr	k1gInSc6	směr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Právě	právě	k9	právě
avantgarda	avantgarda	k1gFnSc1	avantgarda
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
určovat	určovat	k5eAaImF	určovat
směr	směr	k1gInSc4	směr
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
kubismu	kubismus	k1gInSc2	kubismus
k	k	k7c3	k
čisté	čistý	k2eAgFnSc3d1	čistá
abstraktní	abstraktní	k2eAgFnSc3d1	abstraktní
malbě	malba	k1gFnSc3	malba
došel	dojít	k5eAaPmAgMnS	dojít
František	František	k1gMnSc1	František
Kupka	Kupka	k1gMnSc1	Kupka
<g/>
.	.	kIx.	.
</s>
<s>
Doznívající	doznívající	k2eAgInSc1d1	doznívající
kubismus	kubismus	k1gInSc1	kubismus
hledající	hledající	k2eAgFnSc2d1	hledající
nové	nový	k2eAgFnSc2d1	nová
formy	forma	k1gFnSc2	forma
vyjádření	vyjádření	k1gNnPc2	vyjádření
byl	být	k5eAaImAgInS	být
pěstován	pěstovat	k5eAaImNgInS	pěstovat
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
Tvrdošíjní	tvrdošíjný	k2eAgMnPc1d1	tvrdošíjný
(	(	kIx(	(
<g/>
zejm.	zejm.	k?	zejm.
Josef	Josef	k1gMnSc1	Josef
Čapek	Čapek	k1gMnSc1	Čapek
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Zrzavý	zrzavý	k2eAgMnSc1d1	zrzavý
a	a	k8xC	a
sochař	sochař	k1gMnSc1	sochař
Otto	Otto	k1gMnSc1	Otto
Gutfreund	Gutfreund	k1gMnSc1	Gutfreund
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
avantgardního	avantgardní	k2eAgInSc2d1	avantgardní
Devětsilu	Devětsil	k1gInSc2	Devětsil
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
nadchli	nadchnout	k5eAaPmAgMnP	nadchnout
pro	pro	k7c4	pro
surrealismus	surrealismus	k1gInSc4	surrealismus
(	(	kIx(	(
<g/>
Toyen	Toyen	k1gInSc1	Toyen
<g/>
,	,	kIx,	,
Jindřich	Jindřich	k1gMnSc1	Jindřich
Štyrský	Štyrský	k2eAgMnSc1d1	Štyrský
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Šíma	Šíma	k1gMnSc1	Šíma
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Avantgardě	avantgarda	k1gFnSc3	avantgarda
navzdory	navzdory	k6eAd1	navzdory
si	se	k3xPyFc3	se
svou	svůj	k3xOyFgFnSc7	svůj
cestou	cesta	k1gFnSc7	cesta
šel	jít	k5eAaImAgMnS	jít
Josef	Josef	k1gMnSc1	Josef
Lada	lado	k1gNnSc2	lado
–	–	k?	–
a	a	k8xC	a
i	i	k9	i
on	on	k3xPp3gMnSc1	on
dnes	dnes	k6eAd1	dnes
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejslavnějším	slavný	k2eAgMnPc3d3	nejslavnější
českým	český	k2eAgMnPc3d1	český
malířům	malíř	k1gMnPc3	malíř
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	stoletý	k2eAgMnPc1d1	stoletý
autoři	autor	k1gMnPc1	autor
povětšinou	povětšinou	k6eAd1	povětšinou
rozvíjeli	rozvíjet	k5eAaImAgMnP	rozvíjet
objevy	objev	k1gInPc4	objev
avantgardní	avantgardní	k2eAgFnSc2d1	avantgardní
revoluce	revoluce	k1gFnSc2	revoluce
–	–	k?	–
v	v	k7c6	v
abstraktní	abstraktní	k2eAgFnSc6d1	abstraktní
tvorbě	tvorba	k1gFnSc6	tvorba
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgMnS	být
například	například	k6eAd1	například
Vladimír	Vladimír	k1gMnSc1	Vladimír
Vašíček	Vašíček	k1gMnSc1	Vašíček
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
díla	dílo	k1gNnSc2	dílo
jí	on	k3xPp3gFnSc2	on
věnoval	věnovat	k5eAaImAgMnS	věnovat
Oldřich	Oldřich	k1gMnSc1	Oldřich
Lajsek	Lajsek	k?	Lajsek
<g/>
,	,	kIx,	,
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
Medek	Medek	k1gMnSc1	Medek
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Boudník	Boudník	k1gMnSc1	Boudník
aj.	aj.	kA	aj.
Na	na	k7c4	na
surrealistickou	surrealistický	k2eAgFnSc4d1	surrealistická
hravost	hravost	k1gFnSc4	hravost
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
známých	známý	k2eAgFnPc6d1	známá
kolážích	koláž	k1gFnPc6	koláž
<g/>
,	,	kIx,	,
navazoval	navazovat	k5eAaImAgInS	navazovat
v	v	k7c6	v
exilu	exil	k1gInSc6	exil
působící	působící	k2eAgMnSc1d1	působící
Jiří	Jiří	k1gMnSc1	Jiří
Kolář	Kolář	k1gMnSc1	Kolář
či	či	k8xC	či
doma	doma	k6eAd1	doma
tvořící	tvořící	k2eAgMnSc1d1	tvořící
Jan	Jan	k1gMnSc1	Jan
Švankmajer	Švankmajer	k1gMnSc1	Švankmajer
<g/>
.	.	kIx.	.
</s>
<s>
Zcela	zcela	k6eAd1	zcela
nového	nový	k2eAgInSc2d1	nový
směru	směr	k1gInSc2	směr
zvaného	zvaný	k2eAgInSc2d1	zvaný
pop-art	poprt	k1gInSc1	pop-art
se	se	k3xPyFc4	se
dotkl	dotknout	k5eAaPmAgInS	dotknout
Kája	Kája	k?	Kája
Saudek	Saudek	k1gInSc1	Saudek
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
členové	člen	k1gMnPc1	člen
skupiny	skupina	k1gFnSc2	skupina
Tvrdohlaví	tvrdohlavý	k2eAgMnPc1d1	tvrdohlavý
(	(	kIx(	(
<g/>
Jiří	Jiří	k1gMnSc1	Jiří
David	David	k1gMnSc1	David
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
Nikl	Nikl	k1gMnSc1	Nikl
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Róna	Róna	k1gMnSc1	Róna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejznámějšími	známý	k2eAgMnPc7d3	nejznámější
představiteli	představitel	k1gMnPc7	představitel
české	český	k2eAgFnSc2d1	Česká
umělecké	umělecký	k2eAgFnSc2d1	umělecká
fotografie	fotografia	k1gFnSc2	fotografia
jsou	být	k5eAaImIp3nP	být
František	František	k1gMnSc1	František
Drtikol	Drtikol	k1gInSc1	Drtikol
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Sudek	sudka	k1gFnPc2	sudka
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Saudek	Saudek	k1gMnSc1	Saudek
či	či	k8xC	či
Josef	Josef	k1gMnSc1	Josef
Koudelka	Koudelka	k1gMnSc1	Koudelka
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
fenoménem	fenomén	k1gInSc7	fenomén
v	v	k7c6	v
umělecké	umělecký	k2eAgFnSc6d1	umělecká
fotografii	fotografia	k1gFnSc6	fotografia
je	být	k5eAaImIp3nS	být
Miroslav	Miroslav	k1gMnSc1	Miroslav
Tichý	Tichý	k1gMnSc1	Tichý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
výtvarném	výtvarný	k2eAgNnSc6d1	výtvarné
umění	umění	k1gNnSc6	umění
hraje	hrát	k5eAaImIp3nS	hrát
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
knižní	knižní	k2eAgFnSc2d1	knižní
ilustrace	ilustrace	k1gFnSc2	ilustrace
<g/>
,	,	kIx,	,
karikatura	karikatura	k1gFnSc1	karikatura
a	a	k8xC	a
kreslený	kreslený	k2eAgInSc1d1	kreslený
film	film	k1gInSc1	film
<g/>
.	.	kIx.	.
</s>
<s>
Mistrem	mistr	k1gMnSc7	mistr
karikatury	karikatura	k1gFnSc2	karikatura
byl	být	k5eAaImAgMnS	být
František	František	k1gMnSc1	František
Gellner	Gellner	k1gMnSc1	Gellner
<g/>
,	,	kIx,	,
v	v	k7c6	v
knižní	knižní	k2eAgFnSc6d1	knižní
ilustraci	ilustrace	k1gFnSc6	ilustrace
vynikli	vyniknout	k5eAaPmAgMnP	vyniknout
Viktor	Viktor	k1gMnSc1	Viktor
Oliva	Oliva	k1gMnSc1	Oliva
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Lada	Lada	k1gFnSc1	Lada
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Trnka	Trnka	k1gMnSc1	Trnka
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Burian	Burian	k1gMnSc1	Burian
<g/>
,	,	kIx,	,
Adolf	Adolf	k1gMnSc1	Adolf
Born	Born	k1gMnSc1	Born
či	či	k8xC	či
Květa	Květa	k1gFnSc1	Květa
Pacovská	Pacovská	k1gFnSc1	Pacovská
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
za	za	k7c2	za
knižní	knižní	k2eAgFnSc2d1	knižní
ilustrace	ilustrace	k1gFnSc2	ilustrace
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
získala	získat	k5eAaPmAgFnS	získat
Cenu	cena	k1gFnSc4	cena
Hanse	Hans	k1gMnSc4	Hans
Christiana	Christian	k1gMnSc4	Christian
Andersena	Andersen	k1gMnSc4	Andersen
od	od	k7c2	od
Mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
sdružení	sdružení	k1gNnSc2	sdružení
pro	pro	k7c4	pro
dětskou	dětský	k2eAgFnSc4d1	dětská
knihu	kniha	k1gFnSc4	kniha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kresleném	kreslený	k2eAgInSc6d1	kreslený
filmu	film	k1gInSc6	film
se	se	k3xPyFc4	se
prosadili	prosadit	k5eAaPmAgMnP	prosadit
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Smetana	Smetana	k1gMnSc1	Smetana
či	či	k8xC	či
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Miler	Miler	k1gMnSc1	Miler
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
významným	významný	k2eAgMnPc3d1	významný
německým	německý	k2eAgMnPc3d1	německý
malířům	malíř	k1gMnPc3	malíř
narozeným	narozený	k2eAgMnPc3d1	narozený
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
patřili	patřit	k5eAaImAgMnP	patřit
Anton	Anton	k1gMnSc1	Anton
Raphael	Raphael	k1gMnSc1	Raphael
Mengs	Mengs	k1gInSc1	Mengs
<g/>
,	,	kIx,	,
Alfred	Alfred	k1gMnSc1	Alfred
Kubin	Kubin	k1gMnSc1	Kubin
<g/>
,	,	kIx,	,
Gabriel	Gabriel	k1gMnSc1	Gabriel
Max	Max	k1gMnSc1	Max
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Führich	Führich	k1gMnSc1	Führich
či	či	k8xC	či
Emil	Emil	k1gMnSc1	Emil
Orlik	Orlik	k1gMnSc1	Orlik
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c4	za
nejcennější	cenný	k2eAgNnSc4d3	nejcennější
výtvarné	výtvarný	k2eAgNnSc4d1	výtvarné
dílo	dílo	k1gNnSc4	dílo
na	na	k7c6	na
českém	český	k2eAgNnSc6d1	české
území	území	k1gNnSc6	území
odborníci	odborník	k1gMnPc1	odborník
označují	označovat	k5eAaImIp3nP	označovat
obraz	obraz	k1gInSc4	obraz
benátského	benátský	k2eAgMnSc2d1	benátský
malíře	malíř	k1gMnSc2	malíř
Tiziana	Tizian	k1gMnSc2	Tizian
Apollo	Apollo	k1gMnSc1	Apollo
a	a	k8xC	a
Marsyas	Marsyas	k1gMnSc1	Marsyas
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
vystaven	vystavit	k5eAaPmNgInS	vystavit
v	v	k7c6	v
obrazárně	obrazárna	k1gFnSc6	obrazárna
kroměřížského	kroměřížský	k2eAgInSc2d1	kroměřížský
zámku	zámek	k1gInSc2	zámek
<g/>
.	.	kIx.	.
</s>
<s>
Nejcennější	cenný	k2eAgNnPc1d3	nejcennější
díla	dílo	k1gNnPc1	dílo
českého	český	k2eAgNnSc2d1	české
výtvarného	výtvarný	k2eAgNnSc2d1	výtvarné
umění	umění	k1gNnSc2	umění
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
shromažďovat	shromažďovat	k5eAaImF	shromažďovat
Národní	národní	k2eAgFnSc1d1	národní
galerie	galerie	k1gFnSc1	galerie
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
děl	dělo	k1gNnPc2	dělo
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
autorů	autor	k1gMnPc2	autor
v	v	k7c6	v
jejím	její	k3xOp3gNnSc6	její
vlastnictví	vlastnictví	k1gNnSc6	vlastnictví
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejcennějším	cenný	k2eAgFnPc3d3	nejcennější
Růžencová	růžencový	k2eAgFnSc1d1	Růžencová
slavnost	slavnost	k1gFnSc1	slavnost
Albrechta	Albrecht	k1gMnSc2	Albrecht
Dürera	Dürera	k1gFnSc1	Dürera
<g/>
,	,	kIx,	,
Adam	Adam	k1gMnSc1	Adam
a	a	k8xC	a
Eva	Eva	k1gFnSc1	Eva
Lucase	Lucasa	k1gFnSc3	Lucasa
Cranacha	Cranach	k1gMnSc2	Cranach
staršího	starší	k1gMnSc2	starší
<g/>
,	,	kIx,	,
autoportrét	autoportrét	k1gInSc1	autoportrét
Henri	Henr	k1gFnSc2	Henr
Rousseaua	Rousseau	k1gMnSc2	Rousseau
<g/>
,	,	kIx,	,
Rembrandtův	Rembrandtův	k2eAgMnSc1d1	Rembrandtův
Učenec	učenec	k1gMnSc1	učenec
v	v	k7c6	v
pracovně	pracovna	k1gFnSc6	pracovna
<g/>
,	,	kIx,	,
Šimon	Šimon	k1gMnSc1	Šimon
a	a	k8xC	a
malý	malý	k2eAgMnSc1d1	malý
Ježíš	Ježíš	k1gMnSc1	Ježíš
Petra	Petr	k1gMnSc2	Petr
Brandla	Brandla	k1gMnSc2	Brandla
<g/>
,	,	kIx,	,
Milenci	milenec	k1gMnPc1	milenec
Augusta	Augusta	k1gMnSc1	Augusta
Renoira	Renoira	k1gMnSc1	Renoira
<g/>
,	,	kIx,	,
Panny	Panna	k1gFnPc1	Panna
Gustava	Gustav	k1gMnSc2	Gustav
Klimta	Klimt	k1gMnSc2	Klimt
<g/>
,	,	kIx,	,
Zelená	zelený	k2eAgFnSc1d1	zelená
pšeničné	pšeničný	k2eAgNnSc4d1	pšeničné
pole	pole	k1gNnSc4	pole
s	s	k7c7	s
cypřiši	cypřiš	k1gInPc7	cypřiš
Vincenta	Vincent	k1gMnSc2	Vincent
van	vana	k1gFnPc2	vana
Gogha	Gogh	k1gMnSc2	Gogh
<g/>
,	,	kIx,	,
autoportrét	autoportrét	k1gInSc4	autoportrét
Ilji	Ilja	k1gMnSc2	Ilja
Repina	Repin	k1gMnSc2	Repin
<g/>
,	,	kIx,	,
V	v	k7c6	v
Moulin	moulin	k2eAgMnSc1d1	moulin
Rouge	rouge	k1gFnSc7	rouge
Henri	Henr	k1gFnSc2	Henr
de	de	k?	de
Toulouse-Lautreca	Toulouse-Lautreca	k1gFnSc1	Toulouse-Lautreca
či	či	k8xC	či
Vzpomínka	vzpomínka	k1gFnSc1	vzpomínka
na	na	k7c6	na
Le	Le	k1gFnSc6	Le
Havre	Havr	k1gInSc5	Havr
Pabla	Pabl	k1gMnSc2	Pabl
Picassa	Picass	k1gMnSc2	Picass
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgFnSc1d1	národní
galerie	galerie	k1gFnSc1	galerie
vystavuje	vystavovat	k5eAaImIp3nS	vystavovat
ve	v	k7c6	v
Šternberském	šternberský	k2eAgInSc6d1	šternberský
paláci	palác	k1gInSc6	palác
<g/>
,	,	kIx,	,
Schwarzenberském	schwarzenberský	k2eAgInSc6d1	schwarzenberský
paláci	palác	k1gInSc6	palác
<g/>
,	,	kIx,	,
klášteře	klášter	k1gInSc6	klášter
sv.	sv.	kA	sv.
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
,	,	kIx,	,
Jízdárně	jízdárna	k1gFnSc6	jízdárna
Pražského	pražský	k2eAgInSc2d1	pražský
hradu	hrad	k1gInSc2	hrad
<g/>
,	,	kIx,	,
Salmovském	Salmovský	k2eAgInSc6d1	Salmovský
paláci	palác	k1gInSc6	palác
<g/>
,	,	kIx,	,
Anežském	anežský	k2eAgInSc6d1	anežský
klášteře	klášter	k1gInSc6	klášter
<g/>
,	,	kIx,	,
paláci	palác	k1gInSc6	palác
Kinských	Kinský	k1gMnPc2	Kinský
<g/>
,	,	kIx,	,
Veletržním	veletržní	k2eAgInSc6d1	veletržní
paláci	palác	k1gInSc6	palác
<g/>
,	,	kIx,	,
v	v	k7c6	v
domě	dům	k1gInSc6	dům
U	u	k7c2	u
černé	černý	k2eAgFnSc2d1	černá
matky	matka	k1gFnSc2	matka
Boží	boží	k2eAgFnSc2d1	boží
<g/>
,	,	kIx,	,
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
Zbraslav	Zbraslav	k1gFnSc4	Zbraslav
<g/>
,	,	kIx,	,
v	v	k7c6	v
klášteře	klášter	k1gInSc6	klášter
ve	v	k7c6	v
Žďáru	Žďár	k1gInSc6	Žďár
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
a	a	k8xC	a
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
Fryštát	Fryštát	k1gInSc1	Fryštát
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalším	další	k2eAgFnPc3d1	další
významným	významný	k2eAgFnPc3d1	významná
galeriím	galerie	k1gFnPc3	galerie
patří	patřit	k5eAaImIp3nS	patřit
Moravská	moravský	k2eAgFnSc1d1	Moravská
galerie	galerie	k1gFnSc1	galerie
(	(	kIx(	(
<g/>
vystavuje	vystavovat	k5eAaImIp3nS	vystavovat
v	v	k7c6	v
pěti	pět	k4xCc6	pět
budovách	budova	k1gFnPc6	budova
<g/>
:	:	kIx,	:
Místodržitelský	místodržitelský	k2eAgInSc1d1	místodržitelský
palác	palác	k1gInSc1	palác
<g/>
,	,	kIx,	,
Pražákův	Pražákův	k2eAgInSc1d1	Pražákův
palác	palác	k1gInSc1	palác
<g/>
,	,	kIx,	,
Uměleckoprůmyslové	uměleckoprůmyslový	k2eAgNnSc1d1	Uměleckoprůmyslové
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
,	,	kIx,	,
Jurkovičova	Jurkovičův	k2eAgFnSc1d1	Jurkovičova
vila	vila	k1gFnSc1	vila
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
a	a	k8xC	a
Rodný	rodný	k2eAgInSc1d1	rodný
dům	dům	k1gInSc1	dům
Josefa	Josef	k1gMnSc2	Josef
Hoffmanna	Hoffmann	k1gMnSc2	Hoffmann
v	v	k7c6	v
Brtnici	Brtnice	k1gFnSc6	Brtnice
u	u	k7c2	u
Jihlavy	Jihlava	k1gFnSc2	Jihlava
<g/>
)	)	kIx)	)
a	a	k8xC	a
Galerie	galerie	k1gFnSc1	galerie
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
(	(	kIx(	(
<g/>
dům	dům	k1gInSc1	dům
U	u	k7c2	u
Zlatého	zlatý	k2eAgInSc2d1	zlatý
prstenu	prsten	k1gInSc2	prsten
<g/>
,	,	kIx,	,
dům	dům	k1gInSc1	dům
U	u	k7c2	u
Kamenného	kamenný	k2eAgInSc2d1	kamenný
zvonu	zvon	k1gInSc2	zvon
<g/>
,	,	kIx,	,
Staroměstská	staroměstský	k2eAgFnSc1d1	Staroměstská
radnice	radnice	k1gFnSc1	radnice
<g/>
,	,	kIx,	,
Bílkova	Bílkův	k2eAgFnSc1d1	Bílkova
vila	vila	k1gFnSc1	vila
<g/>
,	,	kIx,	,
Dům	dům	k1gInSc1	dům
Františka	František	k1gMnSc2	František
Bílka	Bílek	k1gMnSc2	Bílek
v	v	k7c6	v
Chýnově	Chýnov	k1gInSc6	Chýnov
<g/>
,	,	kIx,	,
Městská	městský	k2eAgFnSc1d1	městská
knihovna	knihovna	k1gFnSc1	knihovna
na	na	k7c6	na
Mariánském	mariánský	k2eAgNnSc6d1	Mariánské
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
,	,	kIx,	,
Trojský	trojský	k2eAgInSc4d1	trojský
zámek	zámek	k1gInSc4	zámek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
nejznámějším	známý	k2eAgFnPc3d3	nejznámější
sochám	socha	k1gFnPc3	socha
v	v	k7c6	v
ČR	ČR	kA	ČR
patří	patřit	k5eAaImIp3nS	patřit
Šalounova	Šalounův	k2eAgFnSc1d1	Šalounova
socha	socha	k1gFnSc1	socha
Jana	Jan	k1gMnSc2	Jan
Husa	Hus	k1gMnSc2	Hus
na	na	k7c6	na
Staroměstském	staroměstský	k2eAgNnSc6d1	Staroměstské
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
,	,	kIx,	,
Myslbekův	Myslbekův	k2eAgInSc1d1	Myslbekův
pomník	pomník	k1gInSc1	pomník
knížete	kníže	k1gMnSc2	kníže
Václava	Václav	k1gMnSc2	Václav
na	na	k7c6	na
Václavském	václavský	k2eAgNnSc6d1	Václavské
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
,	,	kIx,	,
Zoubkův	Zoubkův	k2eAgInSc1d1	Zoubkův
Pomník	pomník	k1gInSc1	pomník
obětem	oběť	k1gFnPc3	oběť
komunismu	komunismus	k1gInSc3	komunismus
na	na	k7c6	na
Petříně	Petřín	k1gInSc6	Petřín
<g/>
,	,	kIx,	,
Kafkova	Kafkův	k2eAgFnSc1d1	Kafkova
socha	socha	k1gFnSc1	socha
Jana	Jan	k1gMnSc2	Jan
Žižky	Žižka	k1gMnSc2	Žižka
v	v	k7c6	v
Národním	národní	k2eAgInSc6d1	národní
památníku	památník	k1gInSc6	památník
na	na	k7c6	na
Vítkově	Vítkov	k1gInSc6	Vítkov
(	(	kIx(	(
<g/>
největší	veliký	k2eAgFnSc1d3	veliký
jezdecká	jezdecký	k2eAgFnSc1d1	jezdecká
socha	socha	k1gFnSc1	socha
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc7d3	veliký
českou	český	k2eAgFnSc7d1	Česká
sochou	socha	k1gFnSc7	socha
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
byl	být	k5eAaImAgInS	být
Stalinův	Stalinův	k2eAgInSc1d1	Stalinův
pomník	pomník	k1gInSc1	pomník
na	na	k7c6	na
Letné	Letná	k1gFnSc6	Letná
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
však	však	k9	však
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
místě	místo	k1gNnSc6	místo
stál	stát	k5eAaImAgMnS	stát
jen	jen	k9	jen
v	v	k7c6	v
letech	let	k1gInPc6	let
1955	[number]	k4	1955
<g/>
–	–	k?	–
<g/>
1962	[number]	k4	1962
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
místě	místo	k1gNnSc6	místo
nachází	nacházet	k5eAaImIp3nS	nacházet
Pražský	pražský	k2eAgInSc1d1	pražský
metronom	metronom	k1gInSc1	metronom
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
oblíbenou	oblíbený	k2eAgFnSc4d1	oblíbená
u	u	k7c2	u
turistů	turist	k1gMnPc2	turist
stala	stát	k5eAaPmAgFnS	stát
Černého	Černého	k2eAgFnSc1d1	Černého
Hlava	hlava	k1gFnSc1	hlava
Franze	Franze	k1gFnSc2	Franze
Kafky	Kafka	k1gMnSc2	Kafka
u	u	k7c2	u
obchodního	obchodní	k2eAgNnSc2d1	obchodní
centra	centrum	k1gNnSc2	centrum
Quadrio	Quadrio	k6eAd1	Quadrio
<g/>
.	.	kIx.	.
</s>
<s>
Černý	Černý	k1gMnSc1	Černý
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
strhl	strhnout	k5eAaPmAgMnS	strhnout
pozornost	pozornost	k1gFnSc4	pozornost
také	také	k9	také
svou	svůj	k3xOyFgFnSc7	svůj
provokací	provokace	k1gFnSc7	provokace
Entropa	Entrop	k1gMnSc2	Entrop
<g/>
.	.	kIx.	.
</s>
<s>
Anna	Anna	k1gFnSc1	Anna
Chromy	chrom	k1gInPc1	chrom
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
u	u	k7c2	u
Stavovského	stavovský	k2eAgNnSc2d1	Stavovské
divadla	divadlo	k1gNnSc2	divadlo
instalovala	instalovat	k5eAaBmAgFnS	instalovat
jeden	jeden	k4xCgInSc1	jeden
ze	z	k7c2	z
svých	svůj	k3xOyFgInPc2	svůj
Plášťů	plášť	k1gInPc2	plášť
svědomí	svědomí	k1gNnSc2	svědomí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejvýznamnější	významný	k2eAgFnSc7d3	nejvýznamnější
českou	český	k2eAgFnSc7d1	Česká
výtvarnou	výtvarný	k2eAgFnSc7d1	výtvarná
cenou	cena	k1gFnSc7	cena
je	být	k5eAaImIp3nS	být
Cena	cena	k1gFnSc1	cena
Jindřicha	Jindřich	k1gMnSc2	Jindřich
Chalupeckého	Chalupecký	k2eAgMnSc2d1	Chalupecký
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Architektura	architektura	k1gFnSc1	architektura
===	===	k?	===
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc4	první
fázi	fáze	k1gFnSc4	fáze
vývoje	vývoj	k1gInSc2	vývoj
architektury	architektura	k1gFnSc2	architektura
na	na	k7c6	na
českém	český	k2eAgNnSc6d1	české
území	území	k1gNnSc6	území
s	s	k7c7	s
dochovanými	dochovaný	k2eAgFnPc7d1	dochovaná
stavbami	stavba	k1gFnPc7	stavba
tvoří	tvořit	k5eAaImIp3nP	tvořit
románská	románský	k2eAgFnSc1d1	románská
architektura	architektura	k1gFnSc1	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
předchozí	předchozí	k2eAgFnSc2d1	předchozí
<g/>
,	,	kIx,	,
velkomoravské	velkomoravský	k2eAgFnSc2d1	Velkomoravská
fáze	fáze	k1gFnSc2	fáze
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
dochovaly	dochovat	k5eAaPmAgFnP	dochovat
pouze	pouze	k6eAd1	pouze
archeologické	archeologický	k2eAgInPc1d1	archeologický
nálezy	nález	k1gInPc1	nález
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
románského	románský	k2eAgInSc2d1	románský
slohu	sloh	k1gInSc2	sloh
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
první	první	k4xOgFnSc2	první
kamenné	kamenný	k2eAgFnSc2d1	kamenná
stavby	stavba	k1gFnSc2	stavba
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
především	především	k9	především
kostely	kostel	k1gInPc1	kostel
a	a	k8xC	a
budovy	budova	k1gFnPc1	budova
klášterů	klášter	k1gInPc2	klášter
<g/>
,	,	kIx,	,
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
období	období	k1gNnSc2	období
také	také	k6eAd1	také
první	první	k4xOgInPc1	první
hrady	hrad	k1gInPc1	hrad
a	a	k8xC	a
městské	městský	k2eAgFnPc1d1	městská
stavby	stavba	k1gFnPc1	stavba
(	(	kIx(	(
<g/>
opevnění	opevnění	k1gNnPc1	opevnění
<g/>
,	,	kIx,	,
domy	dům	k1gInPc1	dům
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stavby	stavba	k1gFnPc1	stavba
románské	románský	k2eAgFnSc2d1	románská
architektury	architektura	k1gFnSc2	architektura
vznikaly	vznikat	k5eAaImAgFnP	vznikat
na	na	k7c6	na
českém	český	k2eAgNnSc6d1	české
území	území	k1gNnSc6	území
od	od	k7c2	od
sklonku	sklonek	k1gInSc2	sklonek
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
zvolna	zvolna	k6eAd1	zvolna
prosazovat	prosazovat	k5eAaImF	prosazovat
sloh	sloh	k1gInSc4	sloh
gotický	gotický	k2eAgInSc4d1	gotický
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Gotická	gotický	k2eAgFnSc1d1	gotická
architektura	architektura	k1gFnSc1	architektura
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
vrcholila	vrcholit	k5eAaImAgFnS	vrcholit
za	za	k7c4	za
Karla	Karel	k1gMnSc4	Karel
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
nechal	nechat	k5eAaPmAgMnS	nechat
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
postavit	postavit	k5eAaPmF	postavit
ve	v	k7c6	v
vrcholně	vrcholně	k6eAd1	vrcholně
gotickém	gotický	k2eAgInSc6d1	gotický
slohu	sloh	k1gInSc6	sloh
Karlův	Karlův	k2eAgInSc4d1	Karlův
most	most	k1gInSc4	most
a	a	k8xC	a
rozjela	rozjet	k5eAaPmAgFnS	rozjet
se	se	k3xPyFc4	se
velkorysá	velkorysý	k2eAgFnSc1d1	velkorysá
výstavba	výstavba	k1gFnSc1	výstavba
katedrály	katedrála	k1gFnSc2	katedrála
sv.	sv.	kA	sv.
Víta	Vít	k1gMnSc2	Vít
<g/>
,	,	kIx,	,
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
architekta	architekt	k1gMnSc2	architekt
Petra	Petr	k1gMnSc2	Petr
Parléře	Parléř	k1gMnSc2	Parléř
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc2	jeho
syna	syn	k1gMnSc2	syn
Jana	Jan	k1gMnSc2	Jan
Parléře	Parléř	k1gMnSc2	Parléř
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
schránku	schránka	k1gFnSc4	schránka
nových	nový	k2eAgInPc2d1	nový
korunovačních	korunovační	k2eAgInPc2d1	korunovační
klenotů	klenot	k1gInPc2	klenot
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
nechal	nechat	k5eAaPmAgMnS	nechat
Karel	Karel	k1gMnSc1	Karel
vytvořit	vytvořit	k5eAaPmF	vytvořit
<g/>
,	,	kIx,	,
dal	dát	k5eAaPmAgMnS	dát
ve	v	k7c6	v
středních	střední	k2eAgFnPc6d1	střední
Čechách	Čechy	k1gFnPc6	Čechy
vystavět	vystavět	k5eAaPmF	vystavět
hrad	hrad	k1gInSc4	hrad
Karlštejn	Karlštejn	k1gInSc1	Karlštejn
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
architektem	architekt	k1gMnSc7	architekt
byl	být	k5eAaImAgMnS	být
Matyáš	Matyáš	k1gMnSc1	Matyáš
z	z	k7c2	z
Arrasu	Arras	k1gInSc2	Arras
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dalšího	další	k2eAgInSc2d1	další
vrcholu	vrchol	k1gInSc2	vrchol
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
gotika	gotika	k1gFnSc1	gotika
v	v	k7c6	v
éře	éra	k1gFnSc6	éra
vlády	vláda	k1gFnSc2	vláda
Jagellonců	Jagellonec	k1gMnPc2	Jagellonec
(	(	kIx(	(
<g/>
též	též	k9	též
se	se	k3xPyFc4	se
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
vladislavské	vladislavský	k2eAgFnSc6d1	vladislavská
či	či	k8xC	či
jagellonské	jagellonský	k2eAgFnSc6d1	Jagellonská
gotice	gotika	k1gFnSc6	gotika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vladislav	Vladislav	k1gMnSc1	Vladislav
Jagellonský	jagellonský	k2eAgMnSc1d1	jagellonský
zahájil	zahájit	k5eAaPmAgMnS	zahájit
velkolepou	velkolepý	k2eAgFnSc4d1	velkolepá
přestavbu	přestavba	k1gFnSc4	přestavba
Pražského	pražský	k2eAgInSc2d1	pražský
hradu	hrad	k1gInSc2	hrad
a	a	k8xC	a
povolal	povolat	k5eAaPmAgMnS	povolat
ze	z	k7c2	z
Saska	Sasko	k1gNnSc2	Sasko
stavitele	stavitel	k1gMnSc2	stavitel
Benedikta	Benedikt	k1gMnSc2	Benedikt
Rejta	Rejt	k1gMnSc2	Rejt
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
Vladislavský	vladislavský	k2eAgInSc1d1	vladislavský
sál	sál	k1gInSc1	sál
a	a	k8xC	a
chrám	chrám	k1gInSc1	chrám
svaté	svatý	k2eAgFnSc2d1	svatá
Barbory	Barbora	k1gFnSc2	Barbora
v	v	k7c6	v
Kutné	kutný	k2eAgFnSc6d1	Kutná
Hoře	hora	k1gFnSc6	hora
<g/>
,	,	kIx,	,
na	na	k7c6	na
jehož	jehož	k3xOyRp3gFnSc6	jehož
výstavbě	výstavba	k1gFnSc6	výstavba
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
další	další	k2eAgMnSc1d1	další
známý	známý	k2eAgMnSc1d1	známý
stavitel	stavitel	k1gMnSc1	stavitel
Matěj	Matěj	k1gMnSc1	Matěj
Rejsek	rejsek	k1gMnSc1	rejsek
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
pražské	pražský	k2eAgFnSc2d1	Pražská
Prašné	prašný	k2eAgFnSc2d1	prašná
brány	brána	k1gFnSc2	brána
<g/>
.	.	kIx.	.
</s>
<s>
Antonín	Antonín	k1gMnSc1	Antonín
Pilgram	Pilgram	k1gInSc4	Pilgram
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
zanechal	zanechat	k5eAaPmAgMnS	zanechat
otisky	otisk	k1gInPc7	otisk
pozdní	pozdní	k2eAgFnSc2d1	pozdní
gotiky	gotika	k1gFnSc2	gotika
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Letohádek	Letohádek	k1gInSc1	Letohádek
královny	královna	k1gFnSc2	královna
Anny	Anna	k1gFnSc2	Anna
na	na	k7c6	na
Pražském	pražský	k2eAgInSc6d1	pražský
hradě	hrad	k1gInSc6	hrad
bývá	bývat	k5eAaImIp3nS	bývat
označován	označovat	k5eAaImNgInS	označovat
za	za	k7c4	za
stylově	stylově	k6eAd1	stylově
nejčistší	čistý	k2eAgFnSc4d3	nejčistší
renesanční	renesanční	k2eAgFnSc4d1	renesanční
stavbu	stavba	k1gFnSc4	stavba
na	na	k7c4	na
sever	sever	k1gInSc4	sever
od	od	k7c2	od
Alp	Alpy	k1gFnPc2	Alpy
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
významným	významný	k2eAgFnPc3d1	významná
renesančním	renesanční	k2eAgFnPc3d1	renesanční
stavbám	stavba	k1gFnPc3	stavba
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
letohrádek	letohrádek	k1gInSc4	letohrádek
Hvězda	hvězda	k1gFnSc1	hvězda
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
zámek	zámek	k1gInSc1	zámek
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Také	také	k9	také
v	v	k7c6	v
éře	éra	k1gFnSc6	éra
barokní	barokní	k2eAgFnSc2d1	barokní
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
působili	působit	k5eAaImAgMnP	působit
významní	významný	k2eAgMnPc1d1	významný
architekti	architekt	k1gMnPc1	architekt
jako	jako	k8xS	jako
Carlo	Carlo	k1gNnSc1	Carlo
Lurago	Lurago	k1gNnSc1	Lurago
(	(	kIx(	(
<g/>
Klementinum	Klementinum	k1gNnSc1	Klementinum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Francesco	Francesco	k1gMnSc1	Francesco
Caratti	Caratť	k1gFnSc2	Caratť
(	(	kIx(	(
<g/>
Černínský	Černínský	k2eAgInSc1d1	Černínský
palác	palác	k1gInSc1	palác
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Baptista	baptista	k1gMnSc1	baptista
Mathey	Mathea	k1gFnSc2	Mathea
(	(	kIx(	(
<g/>
Arcibiskupský	arcibiskupský	k2eAgInSc1d1	arcibiskupský
palác	palác	k1gInSc1	palác
<g/>
,	,	kIx,	,
Toskánský	toskánský	k2eAgInSc1d1	toskánský
palác	palác	k1gInSc1	palác
<g/>
,	,	kIx,	,
letohrádek	letohrádek	k1gInSc1	letohrádek
v	v	k7c6	v
Tróji	Trója	k1gFnSc6	Trója
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g />
.	.	kIx.	.
</s>
<s>
Blažej	Blažej	k1gMnSc1	Blažej
Santini-Aichel	Santini-Aichel	k1gMnSc1	Santini-Aichel
(	(	kIx(	(
<g/>
kostel	kostel	k1gInSc1	kostel
na	na	k7c6	na
Zelené	Zelené	k2eAgFnSc6d1	Zelené
hoře	hora	k1gFnSc6	hora
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Maxmilián	Maxmilián	k1gMnSc1	Maxmilián
Kaňka	Kaňka	k1gMnSc1	Kaňka
(	(	kIx(	(
<g/>
Karlova	Karlův	k2eAgFnSc1d1	Karlova
Koruna	koruna	k1gFnSc1	koruna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kryštof	Kryštof	k1gMnSc1	Kryštof
Dientzenhofer	Dientzenhofer	k1gMnSc1	Dientzenhofer
(	(	kIx(	(
<g/>
kostel	kostel	k1gInSc1	kostel
svaté	svatý	k2eAgFnSc2d1	svatá
Markéty	Markéta	k1gFnSc2	Markéta
v	v	k7c6	v
Břevnovském	břevnovský	k2eAgInSc6d1	břevnovský
klášteře	klášter	k1gInSc6	klášter
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Kilián	Kilián	k1gMnSc1	Kilián
Ignác	Ignác	k1gMnSc1	Ignác
Dientzenhofer	Dientzenhofer	k1gMnSc1	Dientzenhofer
(	(	kIx(	(
<g/>
malostranský	malostranský	k2eAgInSc1d1	malostranský
i	i	k8xC	i
staroměstský	staroměstský	k2eAgInSc1d1	staroměstský
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
architekturu	architektura	k1gFnSc4	architektura
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
typický	typický	k2eAgInSc4d1	typický
historismus	historismus	k1gInSc4	historismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gMnSc6	jeho
duchu	duch	k1gMnSc6	duch
tvořil	tvořit	k5eAaImAgMnS	tvořit
například	například	k6eAd1	například
architekt	architekt	k1gMnSc1	architekt
a	a	k8xC	a
stavitel	stavitel	k1gMnSc1	stavitel
Josef	Josef	k1gMnSc1	Josef
Hlávka	Hlávka	k1gMnSc1	Hlávka
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
Rezidence	rezidence	k1gFnSc1	rezidence
bukovinských	bukovinský	k2eAgMnPc2d1	bukovinský
metropolitů	metropolita	k1gMnPc2	metropolita
v	v	k7c6	v
Černovicích	Černovice	k1gFnPc6	Černovice
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
<g/>
,	,	kIx,	,
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
na	na	k7c6	na
území	území	k1gNnSc6	území
Rakousko-Uherska	Rakousko-Uhersk	k1gInSc2	Rakousko-Uhersk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
zapsaná	zapsaný	k2eAgFnSc1d1	zapsaná
na	na	k7c4	na
Seznam	seznam	k1gInSc4	seznam
světového	světový	k2eAgNnSc2d1	světové
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
Dílem	dílem	k6eAd1	dílem
historismu	historismus	k1gInSc3	historismus
byly	být	k5eAaImAgFnP	být
i	i	k9	i
budovy	budova	k1gFnPc1	budova
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
<g/>
,	,	kIx,	,
Národního	národní	k2eAgNnSc2d1	národní
muzea	muzeum	k1gNnSc2	muzeum
či	či	k8xC	či
Rudolfina	Rudolfinum	k1gNnSc2	Rudolfinum
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Významná	významný	k2eAgFnSc1d1	významná
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
architektuře	architektura	k1gFnSc6	architektura
vlna	vlna	k1gFnSc1	vlna
secese	secese	k1gFnSc2	secese
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
zejm.	zejm.	k?	zejm.
Obecní	obecní	k2eAgInSc1d1	obecní
dům	dům	k1gInSc1	dům
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
z	z	k7c2	z
architektů	architekt	k1gMnPc2	architekt
Antonín	Antonín	k1gMnSc1	Antonín
Balšánek	balšánek	k1gMnSc1	balšánek
<g/>
,	,	kIx,	,
Osvald	Osvald	k1gMnSc1	Osvald
Polívka	Polívka	k1gMnSc1	Polívka
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Fanta	Fanta	k1gMnSc1	Fanta
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Letzel	Letzel	k1gMnSc1	Letzel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
válkou	válka	k1gFnSc7	válka
pak	pak	k6eAd1	pak
kubismu	kubismus	k1gInSc2	kubismus
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
českou	český	k2eAgFnSc7d1	Česká
specialitou	specialita	k1gFnSc7	specialita
(	(	kIx(	(
<g/>
dům	dům	k1gInSc1	dům
U	u	k7c2	u
Černé	Černé	k2eAgFnSc2d1	Černé
Matky	matka	k1gFnSc2	matka
Boží	božit	k5eAaImIp3nP	božit
Josefa	Josef	k1gMnSc2	Josef
Gočára	Gočár	k1gMnSc2	Gočár
<g/>
,	,	kIx,	,
Kovařovicova	Kovařovicův	k2eAgFnSc1d1	Kovařovicova
vila	vila	k1gFnSc1	vila
Josefa	Josef	k1gMnSc2	Josef
Chochola	Chochola	k1gFnSc1	Chochola
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
architektura	architektura	k1gFnSc1	architektura
tíhla	tíhnout	k5eAaImAgFnS	tíhnout
k	k	k7c3	k
funkcionalismu	funkcionalismus	k1gInSc3	funkcionalismus
(	(	kIx(	(
<g/>
Veletržní	veletržní	k2eAgInSc1d1	veletržní
palác	palác	k1gInSc1	palác
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
Baťův	Baťův	k2eAgInSc1d1	Baťův
mrakodrap	mrakodrap	k1gInSc1	mrakodrap
ve	v	k7c6	v
Zlíně	Zlín	k1gInSc6	Zlín
či	či	k8xC	či
vila	vít	k5eAaImAgFnS	vít
Tugendhat	Tugendhat	k1gFnPc4	Tugendhat
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
z	z	k7c2	z
dílny	dílna	k1gFnSc2	dílna
Ludwiga	Ludwig	k1gMnSc2	Ludwig
Miese	Miese	k1gFnSc2	Miese
van	vana	k1gFnPc2	vana
der	drát	k5eAaImRp2nS	drát
Rohe	Rohus	k1gMnSc5	Rohus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
k	k	k7c3	k
jeho	jeho	k3xOp3gMnPc3	jeho
představitelům	představitel	k1gMnPc3	představitel
patřili	patřit	k5eAaImAgMnP	patřit
Jan	Jan	k1gMnSc1	Jan
Kotěra	Kotěra	k1gFnSc1	Kotěra
a	a	k8xC	a
Josef	Josef	k1gMnSc1	Josef
Gočár	Gočár	k1gMnSc1	Gočár
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
pracoval	pracovat	k5eAaImAgMnS	pracovat
i	i	k9	i
významný	významný	k2eAgMnSc1d1	významný
slovinský	slovinský	k2eAgMnSc1d1	slovinský
architekt	architekt	k1gMnSc1	architekt
Josip	Josip	k1gMnSc1	Josip
Plečnik	Plečnik	k1gMnSc1	Plečnik
(	(	kIx(	(
<g/>
zejm.	zejm.	k?	zejm.
kostel	kostel	k1gInSc1	kostel
Nejsvětějšího	nejsvětější	k2eAgNnSc2d1	nejsvětější
Srdce	srdce	k1gNnSc2	srdce
Páně	páně	k2eAgNnSc2d1	páně
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
Jiřího	Jiří	k1gMnSc2	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
Janák	Janák	k1gMnSc1	Janák
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
rovněž	rovněž	k9	rovněž
pokusil	pokusit	k5eAaPmAgMnS	pokusit
vytvořit	vytvořit	k5eAaPmF	vytvořit
"	"	kIx"	"
<g/>
národní	národní	k2eAgInSc4d1	národní
sloh	sloh	k1gInSc4	sloh
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kombinaci	kombinace	k1gFnSc4	kombinace
lidové	lidový	k2eAgFnSc2d1	lidová
a	a	k8xC	a
moderní	moderní	k2eAgFnSc2d1	moderní
architektury	architektura	k1gFnSc2	architektura
(	(	kIx(	(
<g/>
zejm.	zejm.	k?	zejm.
krematorium	krematorium	k1gNnSc1	krematorium
v	v	k7c6	v
Pardubicích	Pardubice	k1gInPc6	Pardubice
a	a	k8xC	a
palác	palác	k1gInSc1	palác
Adria	Adrium	k1gNnSc2	Adrium
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podobnou	podobný	k2eAgFnSc7d1	podobná
cestou	cesta	k1gFnSc7	cesta
šel	jít	k5eAaImAgMnS	jít
Dušan	Dušan	k1gMnSc1	Dušan
Jurkovič	Jurkovič	k1gMnSc1	Jurkovič
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
vyžadován	vyžadován	k2eAgInSc4d1	vyžadován
jako	jako	k8xS	jako
oficiální	oficiální	k2eAgInSc4d1	oficiální
styl	styl	k1gInSc4	styl
socialistický	socialistický	k2eAgInSc1d1	socialistický
realismus	realismus	k1gInSc1	realismus
(	(	kIx(	(
<g/>
též	též	k9	též
zvaný	zvaný	k2eAgInSc1d1	zvaný
sorela	sorel	k1gMnSc2	sorel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Charakteristickými	charakteristický	k2eAgFnPc7d1	charakteristická
stavbami	stavba	k1gFnPc7	stavba
v	v	k7c6	v
jeho	jeho	k3xOp3gMnSc6	jeho
duchu	duch	k1gMnSc6	duch
jsou	být	k5eAaImIp3nP	být
hotel	hotel	k1gInSc4	hotel
Jalta	Jalt	k1gInSc2	Jalt
na	na	k7c6	na
Václavském	václavský	k2eAgNnSc6d1	Václavské
náměstí	náměstí	k1gNnSc6	náměstí
či	či	k8xC	či
sídliště	sídliště	k1gNnSc4	sídliště
Poruba	Poruba	k1gFnSc1	Poruba
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
<g/>
.	.	kIx.	.
</s>
<s>
Specifickou	specifický	k2eAgFnSc7d1	specifická
odnoží	odnož	k1gFnSc7	odnož
sorely	sorela	k1gFnSc2	sorela
byl	být	k5eAaImAgInS	být
tzv.	tzv.	kA	tzv.
stalinský	stalinský	k2eAgInSc4d1	stalinský
neoklasicismus	neoklasicismus	k1gInSc4	neoklasicismus
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
hotel	hotel	k1gInSc1	hotel
Internacional	Internacional	k1gFnSc2	Internacional
v	v	k7c6	v
pražských	pražský	k2eAgFnPc6d1	Pražská
Dejvicích	Dejvice	k1gFnPc6	Dejvice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
však	však	k9	však
v	v	k7c6	v
architektuře	architektura	k1gFnSc6	architektura
(	(	kIx(	(
<g/>
a	a	k8xC	a
ovšem	ovšem	k9	ovšem
i	i	k9	i
v	v	k7c6	v
designu	design	k1gInSc6	design
<g/>
)	)	kIx)	)
prosadil	prosadit	k5eAaPmAgMnS	prosadit
styl	styl	k1gInSc4	styl
nový	nový	k2eAgInSc4d1	nový
<g/>
,	,	kIx,	,
zvaný	zvaný	k2eAgInSc4d1	zvaný
bruselský	bruselský	k2eAgInSc4d1	bruselský
–	–	k?	–
to	ten	k3xDgNnSc1	ten
podle	podle	k7c2	podle
toho	ten	k3xDgInSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
představen	představit	k5eAaPmNgInS	představit
na	na	k7c6	na
světové	světový	k2eAgFnSc6d1	světová
výstavě	výstava	k1gFnSc6	výstava
Expo	Expo	k1gNnSc1	Expo
58	[number]	k4	58
v	v	k7c6	v
Bruselu	Brusel	k1gInSc6	Brusel
<g/>
.	.	kIx.	.
</s>
<s>
Vyznačoval	vyznačovat	k5eAaImAgInS	vyznačovat
se	se	k3xPyFc4	se
oblými	oblý	k2eAgInPc7d1	oblý
tvary	tvar	k1gInPc7	tvar
a	a	k8xC	a
skleněnými	skleněný	k2eAgFnPc7d1	skleněná
fasádami	fasáda	k1gFnPc7	fasáda
<g/>
.	.	kIx.	.
</s>
<s>
Typickou	typický	k2eAgFnSc7d1	typická
stavbou	stavba	k1gFnSc7	stavba
v	v	k7c6	v
bruselském	bruselský	k2eAgInSc6d1	bruselský
stylu	styl	k1gInSc6	styl
byly	být	k5eAaImAgFnP	být
především	především	k9	především
výstavní	výstavní	k2eAgInSc4d1	výstavní
pavilon	pavilon	k1gInSc4	pavilon
a	a	k8xC	a
restaurace	restaurace	k1gFnSc1	restaurace
československého	československý	k2eAgInSc2d1	československý
pavilonu	pavilon	k1gInSc2	pavilon
na	na	k7c6	na
Expu	Expo	k1gNnSc6	Expo
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
důležitými	důležitý	k2eAgFnPc7d1	důležitá
stavbami	stavba	k1gFnPc7	stavba
bruselského	bruselský	k2eAgInSc2d1	bruselský
stylu	styl	k1gInSc2	styl
byly	být	k5eAaImAgFnP	být
pavilon	pavilon	k1gInSc4	pavilon
Z	z	k7c2	z
na	na	k7c6	na
brněnském	brněnský	k2eAgNnSc6d1	brněnské
výstavišti	výstaviště	k1gNnSc6	výstaviště
<g/>
,	,	kIx,	,
plavecký	plavecký	k2eAgInSc1d1	plavecký
stadion	stadion	k1gInSc1	stadion
v	v	k7c6	v
Podolí	Podolí	k1gNnSc6	Podolí
či	či	k8xC	či
nádraží	nádraží	k1gNnSc6	nádraží
v	v	k7c6	v
Havířově	Havířov	k1gInSc6	Havířov
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
však	však	k9	však
bruselský	bruselský	k2eAgInSc4d1	bruselský
styl	styl	k1gInSc4	styl
vytlačila	vytlačit	k5eAaPmAgFnS	vytlačit
česká	český	k2eAgFnSc1d1	Česká
verze	verze	k1gFnSc1	verze
brutalismu	brutalismus	k1gInSc2	brutalismus
<g/>
.	.	kIx.	.
</s>
<s>
Ceněny	ceněn	k2eAgInPc1d1	ceněn
jsou	být	k5eAaImIp3nP	být
zejména	zejména	k9	zejména
projekty	projekt	k1gInPc1	projekt
z	z	k7c2	z
dílny	dílna	k1gFnSc2	dílna
Věry	Věra	k1gFnSc2	Věra
Machoninové	Machoninový	k2eAgFnSc2d1	Machoninová
a	a	k8xC	a
jejího	její	k3xOp3gMnSc2	její
manžela	manžel	k1gMnSc2	manžel
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Machonina	Machonin	k2eAgMnSc2d1	Machonin
(	(	kIx(	(
<g/>
dům	dům	k1gInSc1	dům
bytové	bytový	k2eAgFnSc2d1	bytová
kultury	kultura	k1gFnSc2	kultura
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
hotel	hotel	k1gInSc1	hotel
Thermal	Thermal	k1gInSc1	Thermal
v	v	k7c6	v
Karlových	Karlův	k2eAgInPc6d1	Karlův
Varech	Vary	k1gInPc6	Vary
<g/>
,	,	kIx,	,
obchodní	obchodní	k2eAgInSc4d1	obchodní
dům	dům	k1gInSc4	dům
Kotva	kotva	k1gFnSc1	kotva
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
velvyslanectví	velvyslanectví	k1gNnSc6	velvyslanectví
ČSSR	ČSSR	kA	ČSSR
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dalších	další	k2eAgFnPc2d1	další
brutalistických	brutalistický	k2eAgFnPc2d1	brutalistický
staveb	stavba	k1gFnPc2	stavba
je	být	k5eAaImIp3nS	být
připomínáno	připomínán	k2eAgNnSc1d1	připomínáno
zejména	zejména	k9	zejména
československé	československý	k2eAgNnSc1d1	Československé
velvyslanectví	velvyslanectví	k1gNnSc1	velvyslanectví
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
architektů	architekt	k1gMnPc2	architekt
Jana	Jan	k1gMnSc2	Jan
Bočana	bočan	k1gMnSc2	bočan
<g/>
,	,	kIx,	,
Jana	Jan	k1gMnSc2	Jan
Šrámka	Šrámek	k1gMnSc2	Šrámek
a	a	k8xC	a
Karla	Karel	k1gMnSc2	Karel
Štěpánského	štěpánský	k2eAgMnSc2d1	štěpánský
<g/>
,	,	kIx,	,
hotel	hotel	k1gInSc1	hotel
Intercontinental	Intercontinental	k1gMnSc1	Intercontinental
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
od	od	k7c2	od
Karla	Karel	k1gMnSc2	Karel
Bubeníčka	Bubeníček	k1gMnSc2	Bubeníček
a	a	k8xC	a
Karla	Karel	k1gMnSc2	Karel
Filsaka	Filsak	k1gMnSc2	Filsak
<g/>
,	,	kIx,	,
žižkovský	žižkovský	k2eAgInSc1d1	žižkovský
televizní	televizní	k2eAgInSc1d1	televizní
vysílač	vysílač	k1gInSc1	vysílač
Václava	Václav	k1gMnSc2	Václav
Aulického	Aulický	k2eAgMnSc2d1	Aulický
či	či	k8xC	či
stavby	stavba	k1gFnPc4	stavba
Karla	Karel	k1gMnSc2	Karel
Pragera	Prager	k1gMnSc2	Prager
(	(	kIx(	(
<g/>
budova	budova	k1gFnSc1	budova
bývalého	bývalý	k2eAgNnSc2d1	bývalé
Federálního	federální	k2eAgNnSc2d1	federální
shromáždění	shromáždění	k1gNnSc2	shromáždění
<g/>
,	,	kIx,	,
Nová	nový	k2eAgFnSc1d1	nová
scéna	scéna	k1gFnSc1	scéna
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejoceňovanější	oceňovaný	k2eAgFnSc7d3	nejoceňovanější
stavbou	stavba	k1gFnSc7	stavba
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
však	však	k9	však
byl	být	k5eAaImAgInS	být
vysílač	vysílač	k1gInSc1	vysílač
na	na	k7c6	na
Ještědu	Ještěd	k1gInSc6	Ještěd
od	od	k7c2	od
Karla	Karel	k1gMnSc2	Karel
Hubáčka	Hubáček	k1gMnSc2	Hubáček
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
polistopadové	polistopadový	k2eAgFnSc6d1	polistopadová
architektuře	architektura	k1gFnSc6	architektura
výraznou	výrazný	k2eAgFnSc4d1	výrazná
roli	role	k1gFnSc4	role
sehrálo	sehrát	k5eAaPmAgNnS	sehrát
dílo	dílo	k1gNnSc1	dílo
Franka	Frank	k1gMnSc2	Frank
Gehryho	Gehry	k1gMnSc2	Gehry
a	a	k8xC	a
Vlado	Vlado	k1gNnSc1	Vlado
Miluniće	Milunić	k1gFnSc2	Milunić
Tančící	tančící	k2eAgInSc1d1	tančící
dům	dům	k1gInSc1	dům
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
přímo	přímo	k6eAd1	přímo
inicioval	iniciovat	k5eAaBmAgMnS	iniciovat
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
<g/>
,	,	kIx,	,
a	a	k8xC	a
jež	jenž	k3xRgNnSc1	jenž
bývá	bývat	k5eAaImIp3nS	bývat
uváděno	uvádět	k5eAaImNgNnS	uvádět
jako	jako	k8xS	jako
symbol	symbol	k1gInSc1	symbol
postmoderní	postmoderní	k2eAgFnSc2d1	postmoderní
architektury	architektura	k1gFnSc2	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
významných	významný	k2eAgMnPc2d1	významný
světových	světový	k2eAgMnPc2d1	světový
architektů	architekt	k1gMnPc2	architekt
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
tvořili	tvořit	k5eAaImAgMnP	tvořit
ještě	ještě	k9	ještě
Jean	Jean	k1gMnSc1	Jean
Nouvel	Nouvel	k1gMnSc1	Nouvel
(	(	kIx(	(
<g/>
Zlatý	zlatý	k2eAgMnSc1d1	zlatý
Anděl	Anděl	k1gMnSc1	Anděl
na	na	k7c6	na
pražském	pražský	k2eAgInSc6d1	pražský
Smíchově	Smíchov	k1gInSc6	Smíchov
<g/>
)	)	kIx)	)
či	či	k8xC	či
Ricardo	Ricardo	k1gNnSc1	Ricardo
Bofill	Bofill	k1gMnSc1	Bofill
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c4	na
modernizaci	modernizace	k1gFnSc4	modernizace
kdysi	kdysi	k6eAd1	kdysi
dělnického	dělnický	k2eAgInSc2d1	dělnický
Karlína	Karlín	k1gInSc2	Karlín
(	(	kIx(	(
<g/>
Corso	Corsa	k1gFnSc5	Corsa
Karlín	Karlín	k1gInSc1	Karlín
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přípravách	příprava	k1gFnPc6	příprava
je	být	k5eAaImIp3nS	být
projekt	projekt	k1gInSc1	projekt
proměny	proměna	k1gFnSc2	proměna
okolí	okolí	k1gNnSc2	okolí
Masarykova	Masarykův	k2eAgNnSc2d1	Masarykovo
nádraží	nádraží	k1gNnSc2	nádraží
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
vypracovala	vypracovat	k5eAaPmAgFnS	vypracovat
držitelka	držitelka	k1gFnSc1	držitelka
Pritzkerovy	Pritzkerův	k2eAgFnSc2d1	Pritzkerova
ceny	cena	k1gFnSc2	cena
Zaha	Zahum	k1gNnSc2	Zahum
Hadid	Hadida	k1gFnPc2	Hadida
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
projektů	projekt	k1gInPc2	projekt
domácích	domácí	k2eAgMnPc2d1	domácí
architektů	architekt	k1gMnPc2	architekt
si	se	k3xPyFc3	se
největší	veliký	k2eAgInSc4d3	veliký
kredit	kredit	k1gInSc4	kredit
získal	získat	k5eAaPmAgInS	získat
projekt	projekt	k1gInSc1	projekt
Národní	národní	k2eAgFnSc2d1	národní
technické	technický	k2eAgFnSc2d1	technická
knihovny	knihovna	k1gFnSc2	knihovna
v	v	k7c6	v
pražských	pražský	k2eAgFnPc6d1	Pražská
Dejvicích	Dejvice	k1gFnPc6	Dejvice
<g/>
.	.	kIx.	.
</s>
<s>
Hojně	hojně	k6eAd1	hojně
diskutovaný	diskutovaný	k2eAgInSc1d1	diskutovaný
projekt	projekt	k1gInSc1	projekt
nové	nový	k2eAgFnSc2d1	nová
budovy	budova	k1gFnSc2	budova
Národní	národní	k2eAgFnSc2d1	národní
knihovny	knihovna	k1gFnSc2	knihovna
Jana	Jan	k1gMnSc2	Jan
Kaplického	Kaplický	k2eAgMnSc2d1	Kaplický
zůstal	zůstat	k5eAaPmAgInS	zůstat
jen	jen	k9	jen
na	na	k7c6	na
papíře	papír	k1gInSc6	papír
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Českými	český	k2eAgMnPc7d1	český
rodáky	rodák	k1gMnPc7	rodák
byli	být	k5eAaImAgMnP	být
i	i	k9	i
významní	významný	k2eAgMnPc1d1	významný
architekti	architekt	k1gMnPc1	architekt
Adolf	Adolf	k1gMnSc1	Adolf
Loos	Loos	k1gInSc1	Loos
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Hoffmann	Hoffmann	k1gMnSc1	Hoffmann
<g/>
,	,	kIx,	,
Joseph	Joseph	k1gMnSc1	Joseph
Maria	Mario	k1gMnSc2	Mario
Olbrich	Olbrich	k1gMnSc1	Olbrich
či	či	k8xC	či
Balthasar	Balthasar	k1gMnSc1	Balthasar
Neumann	Neumann	k1gMnSc1	Neumann
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Videoherní	Videoherní	k2eAgFnSc1d1	Videoherní
tvorba	tvorba	k1gFnSc1	tvorba
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
sedmdesátých	sedmdesátý	k4xOgNnPc6	sedmdesátý
a	a	k8xC	a
hlavně	hlavně	k9	hlavně
osmdesátých	osmdesátý	k4xOgNnPc6	osmdesátý
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
objevilo	objevit	k5eAaPmAgNnS	objevit
nové	nový	k2eAgNnSc1d1	nové
odvětví	odvětví	k1gNnSc1	odvětví
kulturní	kulturní	k2eAgFnSc2d1	kulturní
tvorby	tvorba	k1gFnSc2	tvorba
–	–	k?	–
počítačové	počítačový	k2eAgFnSc2d1	počítačová
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
hlavně	hlavně	k9	hlavně
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
Tajemství	tajemství	k1gNnSc1	tajemství
Oslího	oslí	k2eAgInSc2d1	oslí
ostrova	ostrov	k1gInSc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
následovaly	následovat	k5eAaImAgFnP	následovat
<g/>
,	,	kIx,	,
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
poměrně	poměrně	k6eAd1	poměrně
úspěšné	úspěšný	k2eAgFnSc2d1	úspěšná
a	a	k8xC	a
populární	populární	k2eAgFnSc2d1	populární
hry	hra	k1gFnSc2	hra
jako	jako	k8xC	jako
Dračí	dračí	k2eAgFnSc2d1	dračí
historie	historie	k1gFnSc2	historie
<g/>
,	,	kIx,	,
Fish	Fish	k1gInSc4	Fish
Fillets	Filletsa	k1gFnPc2	Filletsa
či	či	k8xC	či
Brány	brána	k1gFnSc2	brána
Skeldalu	Skeldal	k1gInSc2	Skeldal
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
se	se	k3xPyFc4	se
povedlo	povést	k5eAaPmAgNnS	povést
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
uspět	uspět	k5eAaPmF	uspět
hře	hra	k1gFnSc3	hra
Hidden	Hiddna	k1gFnPc2	Hiddna
&	&	k?	&
Dangerous	Dangerous	k1gMnSc1	Dangerous
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
letech	léto	k1gNnPc6	léto
následovaly	následovat	k5eAaImAgInP	následovat
mezinárodně	mezinárodně	k6eAd1	mezinárodně
úspěšné	úspěšný	k2eAgInPc1d1	úspěšný
herní	herní	k2eAgInPc1d1	herní
tituly	titul	k1gInPc1	titul
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
Operace	operace	k1gFnSc1	operace
Flashpoint	Flashpoint	k1gInSc1	Flashpoint
<g/>
,	,	kIx,	,
Mafia	Mafia	k1gFnSc1	Mafia
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
City	City	k1gFnSc2	City
of	of	k?	of
Lost	Lost	k1gInSc1	Lost
Heaven	Heavna	k1gFnPc2	Heavna
či	či	k8xC	či
Vietcong	Vietconga	k1gFnPc2	Vietconga
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
světoznámým	světoznámý	k2eAgFnPc3d1	světoznámá
českým	český	k2eAgFnPc3d1	Česká
hrám	hra	k1gFnPc3	hra
patří	patřit	k5eAaImIp3nS	patřit
dvojka	dvojka	k1gFnSc1	dvojka
a	a	k8xC	a
trojka	trojka	k1gFnSc1	trojka
Mafie	mafie	k1gFnSc1	mafie
<g/>
,	,	kIx,	,
Euro	euro	k1gNnSc1	euro
Truck	truck	k1gInSc1	truck
Simulator	Simulator	k1gMnSc1	Simulator
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
dvojka	dvojka	k1gFnSc1	dvojka
<g/>
,	,	kIx,	,
American	American	k1gInSc1	American
Truck	truck	k1gInSc1	truck
Simulator	Simulator	k1gInSc1	Simulator
<g/>
,	,	kIx,	,
ArmA	ArmA	k1gFnSc1	ArmA
(	(	kIx(	(
<g/>
plus	plus	k1gInSc1	plus
dvojka	dvojka	k1gFnSc1	dvojka
a	a	k8xC	a
trojka	trojka	k1gFnSc1	trojka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Silent	Silent	k1gMnSc1	Silent
Hill	Hill	k1gMnSc1	Hill
<g/>
:	:	kIx,	:
Downpour	Downpour	k1gMnSc1	Downpour
<g/>
,	,	kIx,	,
Machinarium	Machinarium	k1gNnSc1	Machinarium
<g/>
,	,	kIx,	,
18	[number]	k4	18
Wheels	Wheels	k1gInSc1	Wheels
of	of	k?	of
Steel	Steel	k1gInSc1	Steel
<g/>
,	,	kIx,	,
DayZ	DayZ	k1gFnSc1	DayZ
<g/>
,	,	kIx,	,
Bus	bus	k1gInSc1	bus
Driver	driver	k1gInSc1	driver
<g/>
,	,	kIx,	,
Botanicula	Botanicula	k1gFnSc1	Botanicula
či	či	k8xC	či
Kingdom	Kingdom	k1gInSc1	Kingdom
Come	Com	k1gInSc2	Com
<g/>
:	:	kIx,	:
Deliverance	Deliverance	k1gFnSc1	Deliverance
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
funguje	fungovat	k5eAaImIp3nS	fungovat
několik	několik	k4yIc1	několik
světově	světově	k6eAd1	světově
známých	známý	k2eAgNnPc2d1	známé
herních	herní	k2eAgNnPc2d1	herní
studií	studio	k1gNnPc2	studio
<g/>
,	,	kIx,	,
jako	jako	k9	jako
2K	[number]	k4	2K
Czech	Czecha	k1gFnPc2	Czecha
<g/>
,	,	kIx,	,
Bohemia	bohemia	k1gFnSc1	bohemia
Interactive	Interactiv	k1gInSc5	Interactiv
Studio	studio	k1gNnSc1	studio
<g/>
,	,	kIx,	,
SCS	SCS	kA	SCS
Software	software	k1gInSc1	software
<g/>
,	,	kIx,	,
Amanita	Amanita	k1gFnSc1	Amanita
Design	design	k1gInSc1	design
nebo	nebo	k8xC	nebo
Madfinger	Madfinger	k1gMnSc1	Madfinger
Games	Games	k1gMnSc1	Games
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
jsou	být	k5eAaImIp3nP	být
české	český	k2eAgFnPc1d1	Česká
hry	hra	k1gFnPc1	hra
oceňovány	oceňován	k2eAgFnPc1d1	oceňována
na	na	k7c6	na
Anifilmu	Anifilm	k1gInSc6	Anifilm
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
soutěže	soutěž	k1gFnSc2	soutěž
Česká	český	k2eAgFnSc1d1	Česká
hra	hra	k1gFnSc1	hra
roku	rok	k1gInSc2	rok
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2011	[number]	k4	2011
až	až	k9	až
2013	[number]	k4	2013
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
soutěže	soutěž	k1gFnSc2	soutěž
Booom	Booom	k1gInSc1	Booom
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
se	se	k3xPyFc4	se
setkat	setkat	k5eAaPmF	setkat
s	s	k7c7	s
názorem	názor	k1gInSc7	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
videohry	videohra	k1gFnPc1	videohra
jsou	být	k5eAaImIp3nP	být
největším	veliký	k2eAgInSc7d3	veliký
kulturním	kulturní	k2eAgInSc7d1	kulturní
exportem	export	k1gInSc7	export
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Populární	populární	k2eAgFnSc1d1	populární
kultura	kultura	k1gFnSc1	kultura
===	===	k?	===
</s>
</p>
<p>
<s>
Krom	krom	k7c2	krom
kultury	kultura	k1gFnSc2	kultura
vysoké	vysoký	k2eAgFnSc2d1	vysoká
a	a	k8xC	a
lidové	lidový	k2eAgFnSc2d1	lidová
se	se	k3xPyFc4	se
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
všude	všude	k6eAd1	všude
jinde	jinde	k6eAd1	jinde
<g/>
,	,	kIx,	,
rozvíjela	rozvíjet	k5eAaImAgFnS	rozvíjet
od	od	k7c2	od
počátků	počátek	k1gInPc2	počátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
i	i	k9	i
kultura	kultura	k1gFnSc1	kultura
populární	populární	k2eAgFnSc1d1	populární
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
popkulturní	popkulturní	k2eAgInPc1d1	popkulturní
fenomény	fenomén	k1gInPc1	fenomén
jsou	být	k5eAaImIp3nP	být
pevně	pevně	k6eAd1	pevně
usazeny	usadit	k5eAaPmNgFnP	usadit
v	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
kolektivním	kolektivní	k2eAgNnSc6d1	kolektivní
vědomí	vědomí	k1gNnSc6	vědomí
a	a	k8xC	a
slouží	sloužit	k5eAaImIp3nS	sloužit
nezřídka	nezřídka	k6eAd1	nezřídka
i	i	k9	i
k	k	k7c3	k
národní	národní	k2eAgFnSc3d1	národní
sebeidentifikaci	sebeidentifikace	k1gFnSc3	sebeidentifikace
<g/>
.	.	kIx.	.
</s>
<s>
Anketa	anketa	k1gFnSc1	anketa
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
Kniha	kniha	k1gFnSc1	kniha
mého	můj	k3xOp1gNnSc2	můj
srdce	srdce	k1gNnSc2	srdce
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
například	například	k6eAd1	například
ukázala	ukázat	k5eAaPmAgFnS	ukázat
masovost	masovost	k1gFnSc1	masovost
obliby	obliba	k1gFnSc2	obliba
literárních	literární	k2eAgFnPc2d1	literární
postav	postava	k1gFnPc2	postava
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
sluha	sluha	k1gMnSc1	sluha
Saturnin	Saturnin	k1gMnSc1	Saturnin
z	z	k7c2	z
humoristického	humoristický	k2eAgInSc2d1	humoristický
románu	román	k1gInSc2	román
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Jirotky	Jirotka	k1gFnSc2	Jirotka
<g/>
,	,	kIx,	,
Švejk	Švejk	k1gMnSc1	Švejk
z	z	k7c2	z
románu	román	k1gInSc2	román
Haškova	Haškův	k2eAgInSc2d1	Haškův
či	či	k8xC	či
Rychlé	Rychlé	k2eAgInPc4d1	Rychlé
šípy	šíp	k1gInPc4	šíp
z	z	k7c2	z
chlapeckých	chlapecký	k2eAgInPc2d1	chlapecký
románů	román	k1gInPc2	román
a	a	k8xC	a
komiksů	komiks	k1gInPc2	komiks
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Foglara	Foglar	k1gMnSc2	Foglar
<g/>
.	.	kIx.	.
</s>
<s>
Podobná	podobný	k2eAgFnSc1d1	podobná
anketa	anketa	k1gFnSc1	anketa
o	o	k7c4	o
Největšího	veliký	k2eAgMnSc4d3	veliký
Čecha	Čech	k1gMnSc4	Čech
zase	zase	k9	zase
ukázala	ukázat	k5eAaPmAgFnS	ukázat
roli	role	k1gFnSc4	role
humoristického	humoristický	k2eAgInSc2d1	humoristický
fenoménu	fenomén	k1gInSc2	fenomén
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
<g/>
.	.	kIx.	.
</s>
<s>
Význačnou	význačný	k2eAgFnSc4d1	význačná
roli	role	k1gFnSc4	role
sehrála	sehrát	k5eAaPmAgFnS	sehrát
též	též	k9	též
dvojice	dvojice	k1gFnSc1	dvojice
loutek	loutka	k1gFnPc2	loutka
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
za	za	k7c2	za
první	první	k4xOgFnSc2	první
republiky	republika	k1gFnSc2	republika
Josef	Josef	k1gMnSc1	Josef
Skupa	skupa	k1gFnSc1	skupa
<g/>
:	:	kIx,	:
Spejbl	Spejbl	k1gMnSc1	Spejbl
a	a	k8xC	a
Hurvínek	Hurvínek	k1gMnSc1	Hurvínek
<g/>
.	.	kIx.	.
</s>
<s>
Skupa	skupa	k1gFnSc1	skupa
tím	ten	k3xDgNnSc7	ten
navázal	navázat	k5eAaPmAgInS	navázat
na	na	k7c4	na
velkou	velký	k2eAgFnSc4d1	velká
českou	český	k2eAgFnSc4d1	Česká
loutkářskou	loutkářský	k2eAgFnSc4d1	Loutkářská
tradici	tradice	k1gFnSc4	tradice
<g/>
,	,	kIx,	,
spjatou	spjatý	k2eAgFnSc4d1	spjatá
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
se	s	k7c7	s
jménem	jméno	k1gNnSc7	jméno
Matěje	Matěj	k1gMnSc2	Matěj
Kopeckého	Kopecký	k1gMnSc2	Kopecký
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dětské	dětský	k2eAgFnSc2d1	dětská
literatury	literatura	k1gFnSc2	literatura
vzešla	vzejít	k5eAaPmAgFnS	vzejít
podobně	podobně	k6eAd1	podobně
populární	populární	k2eAgFnSc1d1	populární
postavička	postavička	k1gFnSc1	postavička
Ferdy	Ferda	k1gMnSc2	Ferda
Mravence	mravenec	k1gMnSc2	mravenec
(	(	kIx(	(
<g/>
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc2	jeho
souputníka	souputník	k1gMnSc2	souputník
Brouka	brouk	k1gMnSc2	brouk
Pytlíka	pytlík	k1gMnSc2	pytlík
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
stvořil	stvořit	k5eAaPmAgMnS	stvořit
Ondřej	Ondřej	k1gMnSc1	Ondřej
Sekora	Sekor	k1gMnSc2	Sekor
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejpopulárnějším	populární	k2eAgInPc3d3	nejpopulárnější
českým	český	k2eAgInPc3d1	český
komiksům	komiks	k1gInPc3	komiks
patří	patřit	k5eAaImIp3nS	patřit
Čtyřlístek	čtyřlístek	k1gInSc1	čtyřlístek
<g/>
.	.	kIx.	.
</s>
<s>
Zdrojem	zdroj	k1gInSc7	zdroj
podobných	podobný	k2eAgInPc2d1	podobný
popkulturních	popkulturní	k2eAgInPc2d1	popkulturní
fenoménů	fenomén	k1gInPc2	fenomén
se	se	k3xPyFc4	se
v	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
stal	stát	k5eAaPmAgInS	stát
film	film	k1gInSc1	film
a	a	k8xC	a
především	především	k9	především
televize	televize	k1gFnSc1	televize
–	–	k?	–
cyklus	cyklus	k1gInSc1	cyklus
televizních	televizní	k2eAgInPc2d1	televizní
Večerníčků	večerníček	k1gInPc2	večerníček
přinesl	přinést	k5eAaPmAgMnS	přinést
populární	populární	k2eAgFnSc2d1	populární
postavičky	postavička	k1gFnSc2	postavička
jako	jako	k8xS	jako
Krteček	krteček	k1gMnSc1	krteček
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Milera	Milero	k1gNnSc2	Milero
<g/>
,	,	kIx,	,
Bob	Bob	k1gMnSc1	Bob
a	a	k8xC	a
Bobek	Bobek	k1gMnSc1	Bobek
či	či	k8xC	či
Pat	pat	k1gInSc1	pat
a	a	k8xC	a
Mat	mat	k1gInSc1	mat
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Jiránka	Jiránek	k1gMnSc2	Jiránek
<g/>
,	,	kIx,	,
Maxipes	Maxipes	k1gInSc1	Maxipes
Fík	fík	k1gInSc1	fík
Jiřího	Jiří	k1gMnSc2	Jiří
Šalamouna	Šalamoun	k1gMnSc2	Šalamoun
<g/>
,	,	kIx,	,
Křemílek	křemílek	k1gInSc1	křemílek
a	a	k8xC	a
Vochomůrka	Vochomůrka	k1gMnSc1	Vochomůrka
či	či	k8xC	či
Rákosníček	rákosníček	k1gMnSc1	rákosníček
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Smetany	smetana	k1gFnSc2	smetana
<g/>
,	,	kIx,	,
Mach	macha	k1gFnPc2	macha
a	a	k8xC	a
Šebestová	Šebestová	k1gFnSc1	Šebestová
Adolfa	Adolf	k1gMnSc2	Adolf
Borna	Born	k1gMnSc2	Born
či	či	k8xC	či
Rumcajs	Rumcajs	k1gInSc1	Rumcajs
Radka	Radek	k1gMnSc2	Radek
Pilaře	Pilař	k1gMnSc2	Pilař
<g/>
.	.	kIx.	.
</s>
<s>
Televizní	televizní	k2eAgFnSc1d1	televizní
a	a	k8xC	a
filmová	filmový	k2eAgFnSc1d1	filmová
tvorba	tvorba	k1gFnSc1	tvorba
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
70	[number]	k4	70
<g/>
.	.	kIx.	.
a	a	k8xC	a
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
zase	zase	k9	zase
přinesla	přinést	k5eAaPmAgFnS	přinést
fenomény	fenomén	k1gInPc4	fenomén
jako	jako	k8xS	jako
Pan	Pan	k1gMnSc1	Pan
Tau	tau	k1gNnSc1	tau
<g/>
,	,	kIx,	,
Arabela	Arabela	k1gFnSc1	Arabela
či	či	k8xC	či
Návštěvníci	návštěvník	k1gMnPc1	návštěvník
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýraznější	výrazný	k2eAgFnSc7d3	nejvýraznější
postavou	postava	k1gFnSc7	postava
české	český	k2eAgFnSc2d1	Česká
pop-music	popusic	k1gFnSc2	pop-music
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
formovat	formovat	k5eAaImF	formovat
od	od	k7c2	od
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
zpěvák	zpěvák	k1gMnSc1	zpěvák
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
prosazují	prosazovat	k5eAaImIp3nP	prosazovat
české	český	k2eAgFnPc1d1	Česká
modelky	modelka	k1gFnPc1	modelka
<g/>
,	,	kIx,	,
k	k	k7c3	k
nimž	jenž	k3xRgMnPc3	jenž
patří	patřit	k5eAaImIp3nS	patřit
Karolína	Karolína	k1gFnSc1	Karolína
Kurková	Kurková	k1gFnSc1	Kurková
<g/>
,	,	kIx,	,
Eva	Eva	k1gFnSc1	Eva
Herzigová	Herzigový	k2eAgFnSc1d1	Herzigová
<g/>
,	,	kIx,	,
Pavlína	Pavlína	k1gFnSc1	Pavlína
Pořízková	Pořízková	k1gFnSc1	Pořízková
<g/>
,	,	kIx,	,
Petra	Petra	k1gFnSc1	Petra
Němcová	Němcová	k1gFnSc1	Němcová
<g/>
,	,	kIx,	,
Taťána	Taťána	k1gFnSc1	Taťána
Kuchařová	Kuchařová	k1gFnSc1	Kuchařová
<g/>
,	,	kIx,	,
Daniela	Daniela	k1gFnSc1	Daniela
Peštová	Peštová	k1gFnSc1	Peštová
a	a	k8xC	a
Alena	Alena	k1gFnSc1	Alena
Šeredová	Šeredový	k2eAgFnSc1d1	Šeredová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Média	médium	k1gNnPc1	médium
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
ČR	ČR	kA	ČR
existuje	existovat	k5eAaImIp3nS	existovat
systém	systém	k1gInSc4	systém
veřejnoprávních	veřejnoprávní	k2eAgNnPc2d1	veřejnoprávní
médií	médium	k1gNnPc2	médium
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
jsou	být	k5eAaImIp3nP	být
ze	z	k7c2	z
zákona	zákon	k1gInSc2	zákon
placena	platit	k5eAaImNgFnS	platit
z	z	k7c2	z
veřejných	veřejný	k2eAgInPc2d1	veřejný
peněz	peníze	k1gInPc2	peníze
(	(	kIx(	(
<g/>
koncesionářské	koncesionářský	k2eAgInPc4d1	koncesionářský
poplatky	poplatek	k1gInPc4	poplatek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
jen	jen	k6eAd1	jen
omezenou	omezený	k2eAgFnSc7d1	omezená
možností	možnost	k1gFnSc7	možnost
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
správy	správa	k1gFnSc2	správa
je	být	k5eAaImIp3nS	být
ovlivňovat	ovlivňovat	k5eAaImF	ovlivňovat
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
takovým	takový	k3xDgFnPc3	takový
institucím	instituce	k1gFnPc3	instituce
patří	patřit	k5eAaImIp3nS	patřit
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
(	(	kIx(	(
<g/>
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
státní	státní	k2eAgFnSc4d1	státní
Československou	československý	k2eAgFnSc4d1	Československá
televizi	televize	k1gFnSc4	televize
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
vysílání	vysílání	k1gNnSc1	vysílání
započalo	započnout	k5eAaPmAgNnS	započnout
roku	rok	k1gInSc2	rok
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Český	český	k2eAgInSc1d1	český
rozhlas	rozhlas	k1gInSc1	rozhlas
(	(	kIx(	(
<g/>
Československý	československý	k2eAgInSc1d1	československý
rozhlas	rozhlas	k1gInSc1	rozhlas
začal	začít	k5eAaPmAgInS	začít
vysílat	vysílat	k5eAaImF	vysílat
roku	rok	k1gInSc2	rok
1923	[number]	k4	1923
<g/>
)	)	kIx)	)
a	a	k8xC	a
Česká	český	k2eAgFnSc1d1	Česká
tisková	tiskový	k2eAgFnSc1d1	tisková
kancelář	kancelář	k1gFnSc1	kancelář
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ovšem	ovšem	k9	ovšem
není	být	k5eNaImIp3nS	být
placena	platit	k5eAaImNgFnS	platit
z	z	k7c2	z
koncesionářských	koncesionářský	k2eAgInPc2d1	koncesionářský
poplatků	poplatek	k1gInPc2	poplatek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
soukromá	soukromý	k2eAgFnSc1d1	soukromá
televizní	televizní	k2eAgFnSc1d1	televizní
stanice	stanice	k1gFnSc1	stanice
Premiéra	premiér	k1gMnSc2	premiér
TV	TV	kA	TV
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
Prima	prima	k6eAd1	prima
<g/>
)	)	kIx)	)
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc4	rok
nato	nato	k6eAd1	nato
začala	začít	k5eAaPmAgFnS	začít
vysílat	vysílat	k5eAaImF	vysílat
TV	TV	kA	TV
Nova	nova	k1gFnSc1	nova
<g/>
.	.	kIx.	.
</s>
<s>
Stanice	stanice	k1gFnSc1	stanice
veřejnoprávní	veřejnoprávní	k2eAgFnSc2d1	veřejnoprávní
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
(	(	kIx(	(
<g/>
ČT	ČT	kA	ČT
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
ČT	ČT	kA	ČT
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
ČT	ČT	kA	ČT
sport	sport	k1gInSc1	sport
<g/>
,	,	kIx,	,
ČT	ČT	kA	ČT
:	:	kIx,	:
<g/>
D	D	kA	D
<g/>
,	,	kIx,	,
ČT	ČT	kA	ČT
art	art	k?	art
<g/>
,	,	kIx,	,
ČT	ČT	kA	ČT
<g/>
24	[number]	k4	24
<g/>
)	)	kIx)	)
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
kanály	kanál	k1gInPc7	kanál
provozovanými	provozovaný	k2eAgInPc7d1	provozovaný
společnostmi	společnost	k1gFnPc7	společnost
CET	ceta	k1gFnPc2	ceta
21	[number]	k4	21
(	(	kIx(	(
<g/>
Nova	nova	k1gFnSc1	nova
<g/>
,	,	kIx,	,
Nova	nova	k1gFnSc1	nova
Cinema	Cinema	k1gFnSc1	Cinema
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Nova	nova	k1gFnSc1	nova
Action	Action	k1gInSc1	Action
<g/>
,	,	kIx,	,
Nova	nova	k1gFnSc1	nova
2	[number]	k4	2
<g/>
,	,	kIx,	,
Nova	nova	k1gFnSc1	nova
Gold	Gold	k1gInSc1	Gold
<g/>
,	,	kIx,	,
Nova	nova	k1gFnSc1	nova
Sport	sport	k1gInSc1	sport
1	[number]	k4	1
<g/>
,	,	kIx,	,
Nova	nova	k1gFnSc1	nova
Sport	sport	k1gInSc1	sport
2	[number]	k4	2
<g/>
,	,	kIx,	,
Nova	nova	k1gFnSc1	nova
International	International	k1gFnSc1	International
<g/>
)	)	kIx)	)
a	a	k8xC	a
FTV	FTV	kA	FTV
Prima	prima	k1gFnSc1	prima
(	(	kIx(	(
<g/>
Prima	prima	k1gFnSc1	prima
<g/>
,	,	kIx,	,
Prima	prima	k2eAgInSc1d1	prima
Cool	Cool	k1gInSc1	Cool
<g/>
,	,	kIx,	,
Prima	prima	k6eAd1	prima
Love	lov	k1gInSc5	lov
<g/>
,	,	kIx,	,
Prima	prima	k2eAgMnSc1d1	prima
Zoom	Zoom	k1gMnSc1	Zoom
<g/>
,	,	kIx,	,
Prima	prima	k2eAgMnSc1d1	prima
Max	Max	k1gMnSc1	Max
<g/>
,	,	kIx,	,
Prima	prima	k1gFnSc1	prima
Comedy	Comeda	k1gMnSc2	Comeda
Central	Central	k1gMnSc2	Central
<g/>
)	)	kIx)	)
televiznímu	televizní	k2eAgInSc3d1	televizní
trhu	trh	k1gInSc3	trh
i	i	k9	i
po	po	k7c6	po
digitalizaci	digitalizace	k1gFnSc6	digitalizace
vysílání	vysílání	k1gNnSc2	vysílání
dodnes	dodnes	k6eAd1	dodnes
dominují	dominovat	k5eAaImIp3nP	dominovat
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
například	například	k6eAd1	například
dosáhly	dosáhnout	k5eAaPmAgFnP	dosáhnout
souhrnné	souhrnný	k2eAgFnPc1d1	souhrnná
sledovanosti	sledovanost	k1gFnPc1	sledovanost
82	[number]	k4	82
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Této	tento	k3xDgFnSc3	tento
"	"	kIx"	"
<g/>
velké	velký	k2eAgFnSc3d1	velká
trojce	trojka	k1gFnSc3	trojka
<g/>
"	"	kIx"	"
nejúspěšněji	úspěšně	k6eAd3	úspěšně
konkuruje	konkurovat	k5eAaImIp3nS	konkurovat
TV	TV	kA	TV
Barrandov	Barrandov	k1gInSc4	Barrandov
(	(	kIx(	(
<g/>
kanály	kanál	k1gInPc4	kanál
Barrandov	Barrandov	k1gInSc1	Barrandov
<g/>
,	,	kIx,	,
Kino	kino	k1gNnSc1	kino
Barrandov	Barrandov	k1gInSc1	Barrandov
<g/>
,	,	kIx,	,
Barrandov	Barrandov	k1gInSc1	Barrandov
Krimi	krimi	k1gFnSc1	krimi
<g/>
,	,	kIx,	,
Barrandov	Barrandov	k1gInSc1	Barrandov
News	News	k1gInSc1	News
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poměrně	poměrně	k6eAd1	poměrně
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
tradici	tradice	k1gFnSc4	tradice
má	mít	k5eAaImIp3nS	mít
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
též	též	k6eAd1	též
specializovaná	specializovaný	k2eAgFnSc1d1	specializovaná
hudební	hudební	k2eAgFnSc1d1	hudební
televize	televize	k1gFnSc1	televize
Óčko	Óčko	k6eAd1	Óčko
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
září	září	k1gNnSc2	září
2000	[number]	k4	2000
doplnily	doplnit	k5eAaPmAgFnP	doplnit
nabídku	nabídka	k1gFnSc4	nabídka
také	také	k9	také
satelitní	satelitní	k2eAgFnSc1d1	satelitní
televize	televize	k1gFnSc1	televize
<g/>
.	.	kIx.	.
</s>
<s>
Nejsledovanější	sledovaný	k2eAgInSc1d3	nejsledovanější
hlavní	hlavní	k2eAgInSc1d1	hlavní
zpravodajský	zpravodajský	k2eAgInSc1d1	zpravodajský
pořad	pořad	k1gInSc1	pořad
vysílá	vysílat	k5eAaImIp3nS	vysílat
TV	TV	kA	TV
Nova	nova	k1gFnSc1	nova
<g/>
.	.	kIx.	.
<g/>
Nejčtenějšími	čtený	k2eAgInPc7d3	nejčtenější
deníky	deník	k1gInPc7	deník
jsou	být	k5eAaImIp3nP	být
Blesk	blesk	k1gInSc4	blesk
<g/>
,	,	kIx,	,
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
DNES	dnes	k6eAd1	dnes
<g/>
,	,	kIx,	,
Právo	právo	k1gNnSc1	právo
<g/>
,	,	kIx,	,
Deník	deník	k1gInSc1	deník
<g/>
,	,	kIx,	,
Aha	aha	k1gNnSc1	aha
<g/>
!	!	kIx.	!
</s>
<s>
a	a	k8xC	a
Lidové	lidový	k2eAgFnPc1d1	lidová
noviny	novina	k1gFnPc1	novina
<g/>
.	.	kIx.	.
</s>
<s>
Nejčtenějším	čtený	k2eAgMnSc7d3	nejčtenější
zdarma	zdarma	k6eAd1	zdarma
rozdávaným	rozdávaný	k2eAgInSc7d1	rozdávaný
deníkem	deník	k1gInSc7	deník
je	být	k5eAaImIp3nS	být
Metro	metro	k1gNnSc1	metro
<g/>
.	.	kIx.	.
</s>
<s>
Nejprodávanější	prodávaný	k2eAgInPc1d3	nejprodávanější
zpravodajské	zpravodajský	k2eAgInPc1d1	zpravodajský
časopisy	časopis	k1gInPc1	časopis
jsou	být	k5eAaImIp3nP	být
Téma	téma	k1gNnSc1	téma
<g/>
,	,	kIx,	,
Reflex	reflex	k1gInSc1	reflex
<g/>
,	,	kIx,	,
Týden	týden	k1gInSc1	týden
<g/>
,	,	kIx,	,
Květy	květ	k1gInPc1	květ
a	a	k8xC	a
Respekt	respekt	k1gInSc1	respekt
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
společenskými	společenský	k2eAgInPc7d1	společenský
časopisy	časopis	k1gInPc7	časopis
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2017	[number]	k4	2017
byly	být	k5eAaImAgFnP	být
Rytmus	rytmus	k1gInSc4	rytmus
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
Nedělní	nedělní	k2eAgInSc1d1	nedělní
Blesk	blesk	k1gInSc1	blesk
<g/>
,	,	kIx,	,
Pestrý	pestrý	k2eAgInSc1d1	pestrý
svět	svět	k1gInSc1	svět
či	či	k8xC	či
Sedmička	sedmička	k1gFnSc1	sedmička
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dětských	dětský	k2eAgInPc2d1	dětský
časopisů	časopis	k1gInPc2	časopis
se	se	k3xPyFc4	se
drží	držet	k5eAaImIp3nS	držet
Sluníčko	sluníčko	k1gNnSc1	sluníčko
<g/>
,	,	kIx,	,
ABC	ABC	kA	ABC
a	a	k8xC	a
Mateřídouška	mateřídouška	k1gFnSc1	mateřídouška
<g/>
.	.	kIx.	.
<g/>
Historický	historický	k2eAgInSc4d1	historický
význam	význam	k1gInSc4	význam
měly	mít	k5eAaImAgInP	mít
Národní	národní	k2eAgInPc1d1	národní
listy	list	k1gInPc1	list
<g/>
,	,	kIx,	,
Právo	právo	k1gNnSc1	právo
lidu	lid	k1gInSc2	lid
<g/>
,	,	kIx,	,
České	český	k2eAgNnSc1d1	české
slovo	slovo	k1gNnSc1	slovo
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
Svobodné	svobodný	k2eAgNnSc1d1	svobodné
slovo	slovo	k1gNnSc1	slovo
a	a	k8xC	a
Slovo	slovo	k1gNnSc1	slovo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Zemědělské	zemědělský	k2eAgFnPc1d1	zemědělská
noviny	novina	k1gFnPc1	novina
<g/>
,	,	kIx,	,
Práce	práce	k1gFnPc1	práce
<g/>
,	,	kIx,	,
Lidová	lidový	k2eAgFnSc1d1	lidová
demokracie	demokracie	k1gFnSc1	demokracie
či	či	k8xC	či
Mladý	mladý	k2eAgInSc1d1	mladý
svět	svět	k1gInSc1	svět
<g/>
.	.	kIx.	.
</s>
<s>
Váhu	váha	k1gFnSc4	váha
měly	mít	k5eAaImAgFnP	mít
v	v	k7c4	v
minulosti	minulost	k1gFnPc4	minulost
i	i	k8xC	i
literární	literární	k2eAgInPc4d1	literární
časopisy	časopis	k1gInPc4	časopis
a	a	k8xC	a
intelektuální	intelektuální	k2eAgFnPc4d1	intelektuální
revue	revue	k1gFnPc4	revue
jako	jako	k8xC	jako
Přítomnost	přítomnost	k1gFnSc1	přítomnost
či	či	k8xC	či
Tvorba	tvorba	k1gFnSc1	tvorba
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
a	a	k8xC	a
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
nebo	nebo	k8xC	nebo
Literární	literární	k2eAgFnPc1d1	literární
noviny	novina	k1gFnPc1	novina
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
zaniklým	zaniklý	k2eAgInPc3d1	zaniklý
časopisům	časopis	k1gInPc3	časopis
pro	pro	k7c4	pro
mládež	mládež	k1gFnSc4	mládež
patří	patřit	k5eAaImIp3nS	patřit
Mladý	mladý	k2eAgMnSc1d1	mladý
hlasatel	hlasatel	k1gMnSc1	hlasatel
či	či	k8xC	či
Junák	junák	k1gMnSc1	junák
<g/>
,	,	kIx,	,
do	do	k7c2	do
nichž	jenž	k3xRgFnPc2	jenž
přispíval	přispívat	k5eAaImAgMnS	přispívat
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Foglar	Foglar	k1gMnSc1	Foglar
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Ohníček	ohníček	k1gInSc1	ohníček
a	a	k8xC	a
Sedmička	sedmička	k1gFnSc1	sedmička
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejúspěšnějšími	úspěšný	k2eAgFnPc7d3	nejúspěšnější
stanicemi	stanice	k1gFnPc7	stanice
veřejnoprávního	veřejnoprávní	k2eAgInSc2d1	veřejnoprávní
rozhlasu	rozhlas	k1gInSc2	rozhlas
jsou	být	k5eAaImIp3nP	být
Radiožurnál	radiožurnál	k1gInSc1	radiožurnál
a	a	k8xC	a
ČRo	ČRo	k1gFnSc1	ČRo
Dvojka	dvojka	k1gFnSc1	dvojka
<g/>
.	.	kIx.	.
</s>
<s>
Nejposlouchanějšími	poslouchaný	k2eAgInPc7d3	nejposlouchanější
soukromými	soukromý	k2eAgInPc7d1	soukromý
rádii	rádius	k1gInPc7	rádius
jsou	být	k5eAaImIp3nP	být
Impuls	impuls	k1gInSc4	impuls
<g/>
,	,	kIx,	,
Evropa	Evropa	k1gFnSc1	Evropa
2	[number]	k4	2
a	a	k8xC	a
Frekvence	frekvence	k1gFnSc1	frekvence
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
Českému	český	k2eAgInSc3d1	český
internetu	internet	k1gInSc3	internet
kraluje	kralovat	k5eAaImIp3nS	kralovat
vyhledávač	vyhledávač	k1gMnSc1	vyhledávač
Seznam	seznam	k1gInSc1	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
.	.	kIx.	.
</s>
<s>
Největšími	veliký	k2eAgInPc7d3	veliký
zpravodajskými	zpravodajský	k2eAgInPc7d1	zpravodajský
weby	web	k1gInPc7	web
jsou	být	k5eAaImIp3nP	být
iDNES	iDNES	k?	iDNES
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
a	a	k8xC	a
Novinky	novinka	k1gFnPc1	novinka
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
.	.	kIx.	.
</s>
<s>
Nejnavštěvovanější	navštěvovaný	k2eAgFnSc7d3	nejnavštěvovanější
kulturní	kulturní	k2eAgFnSc7d1	kulturní
stránkou	stránka	k1gFnSc7	stránka
je	být	k5eAaImIp3nS	být
Česko-Slovenská	českolovenský	k2eAgFnSc1d1	česko-slovenská
filmová	filmový	k2eAgFnSc1d1	filmová
databáze	databáze	k1gFnSc1	databáze
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
Wikipedie	Wikipedie	k1gFnSc1	Wikipedie
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
a	a	k8xC	a
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
co	co	k9	co
do	do	k7c2	do
počtu	počet	k1gInSc2	počet
hesel	heslo	k1gNnPc2	heslo
největší	veliký	k2eAgFnSc7d3	veliký
českou	český	k2eAgFnSc7d1	Česká
encyklopedií	encyklopedie	k1gFnSc7	encyklopedie
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Festivaly	festival	k1gInPc1	festival
<g/>
,	,	kIx,	,
přehlídky	přehlídka	k1gFnPc1	přehlídka
<g/>
,	,	kIx,	,
ceny	cena	k1gFnPc1	cena
===	===	k?	===
</s>
</p>
<p>
<s>
Kulturní	kulturní	k2eAgNnSc1d1	kulturní
a	a	k8xC	a
společenské	společenský	k2eAgNnSc1d1	společenské
dění	dění	k1gNnSc1	dění
pravidelně	pravidelně	k6eAd1	pravidelně
vrcholí	vrcholit	k5eAaImIp3nS	vrcholit
na	na	k7c6	na
různých	různý	k2eAgInPc6d1	různý
festivalech	festival	k1gInPc6	festival
a	a	k8xC	a
přehlídkách	přehlídka	k1gFnPc6	přehlídka
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
největším	veliký	k2eAgInPc3d3	veliký
festivalům	festival	k1gInPc3	festival
vážné	vážný	k2eAgFnSc2d1	vážná
hudby	hudba	k1gFnSc2	hudba
patří	patřit	k5eAaImIp3nS	patřit
Pražské	pražský	k2eAgNnSc1d1	Pražské
jaro	jaro	k1gNnSc1	jaro
<g/>
,	,	kIx,	,
Smetanova	Smetanův	k2eAgFnSc1d1	Smetanova
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
a	a	k8xC	a
Janáčkovy	Janáčkův	k2eAgInPc1d1	Janáčkův
Hukvaldy	Hukvaldy	k1gInPc1	Hukvaldy
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
jazzu	jazz	k1gInSc2	jazz
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
Jazz	jazz	k1gInSc1	jazz
Goes	Goes	k1gInSc4	Goes
to	ten	k3xDgNnSc1	ten
Town	Town	k1gNnSc1	Town
v	v	k7c6	v
Hradci	Hradec	k1gInSc6	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
,	,	kIx,	,
Bohemia	bohemia	k1gFnSc1	bohemia
JazzFest	JazzFest	k1gMnSc1	JazzFest
pořádaný	pořádaný	k2eAgMnSc1d1	pořádaný
v	v	k7c6	v
různých	různý	k2eAgNnPc6d1	různé
městech	město	k1gNnPc6	město
současně	současně	k6eAd1	současně
a	a	k8xC	a
Prague	Prague	k1gFnSc1	Prague
Proms	Promsa	k1gFnPc2	Promsa
<g/>
.	.	kIx.	.
<g/>
Největším	veliký	k2eAgInSc7d3	veliký
tanečním	taneční	k2eAgInSc7d1	taneční
festivalem	festival	k1gInSc7	festival
je	být	k5eAaImIp3nS	být
Tanec	tanec	k1gInSc1	tanec
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
výtvarného	výtvarný	k2eAgNnSc2d1	výtvarné
umění	umění	k1gNnSc2	umění
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
přehlídka	přehlídka	k1gFnSc1	přehlídka
Pražské	pražský	k2eAgNnSc1d1	Pražské
bienále	bienále	k1gNnSc4	bienále
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc4d1	velká
tradici	tradice	k1gFnSc4	tradice
má	mít	k5eAaImIp3nS	mít
festival	festival	k1gInSc1	festival
scénografie	scénografie	k1gFnSc2	scénografie
Pražské	pražský	k2eAgFnSc2d1	Pražská
Quadriennale	Quadriennale	k1gFnSc2	Quadriennale
<g/>
.	.	kIx.	.
</s>
<s>
Zcela	zcela	k6eAd1	zcela
zvláštní	zvláštní	k2eAgFnSc7d1	zvláštní
výtvarnou	výtvarný	k2eAgFnSc7d1	výtvarná
akcí	akce	k1gFnSc7	akce
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
Festival	festival	k1gInSc1	festival
světla	světlo	k1gNnSc2	světlo
Signal	Signal	k1gFnSc2	Signal
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
velkým	velký	k2eAgFnPc3d1	velká
divadelním	divadelní	k2eAgFnPc3d1	divadelní
akcím	akce	k1gFnPc3	akce
patří	patřit	k5eAaImIp3nS	patřit
tradiční	tradiční	k2eAgInSc1d1	tradiční
festival	festival	k1gInSc1	festival
amatétského	amatétský	k2eAgNnSc2d1	amatétský
divadla	divadlo	k1gNnSc2	divadlo
Jiráskův	Jiráskův	k2eAgInSc4d1	Jiráskův
Hronov	Hronov	k1gInSc4	Hronov
<g/>
.	.	kIx.	.
</s>
<s>
Loutkářská	loutkářský	k2eAgFnSc1d1	Loutkářská
Chrudim	Chrudim	k1gFnSc1	Chrudim
je	být	k5eAaImIp3nS	být
nejstarší	starý	k2eAgInSc1d3	nejstarší
loutkářský	loutkářský	k2eAgInSc1d1	loutkářský
festival	festival	k1gInSc1	festival
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
loutky	loutka	k1gFnPc4	loutka
je	být	k5eAaImIp3nS	být
zaměřena	zaměřit	k5eAaPmNgFnS	zaměřit
též	též	k9	též
Skupova	Skupův	k2eAgFnSc1d1	Skupova
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
přitom	přitom	k6eAd1	přitom
bylo	být	k5eAaImAgNnS	být
české	český	k2eAgNnSc1d1	české
loutkařství	loutkařství	k1gNnSc1	loutkařství
zařazeno	zařazen	k2eAgNnSc1d1	zařazeno
k	k	k7c3	k
nehmotnému	hmotný	k2eNgNnSc3d1	nehmotné
dědictví	dědictví	k1gNnSc3	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
moderní	moderní	k2eAgInSc4d1	moderní
cirkus	cirkus	k1gInSc4	cirkus
a	a	k8xC	a
akrobacii	akrobacie	k1gFnSc4	akrobacie
se	se	k3xPyFc4	se
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
Letní	letní	k2eAgFnSc1d1	letní
Letná	Letná	k1gFnSc1	Letná
<g/>
.	.	kIx.	.
</s>
<s>
Divadlo	divadlo	k1gNnSc1	divadlo
i	i	k9	i
hudbu	hudba	k1gFnSc4	hudba
s	s	k7c7	s
dobročinností	dobročinnost	k1gFnSc7	dobročinnost
spojuje	spojovat	k5eAaImIp3nS	spojovat
festival	festival	k1gInSc1	festival
v	v	k7c6	v
bohnické	bohnický	k2eAgFnSc6d1	Bohnická
psychiatrické	psychiatrický	k2eAgFnSc6d1	psychiatrická
léčebně	léčebna	k1gFnSc6	léčebna
Mezi	mezi	k7c4	mezi
ploty	plot	k1gInPc4	plot
<g/>
.	.	kIx.	.
</s>
<s>
Největšími	veliký	k2eAgFnPc7d3	veliký
knižními	knižní	k2eAgFnPc7d1	knižní
akcemi	akce	k1gFnPc7	akce
jsou	být	k5eAaImIp3nP	být
Svět	svět	k1gInSc4	svět
knihy	kniha	k1gFnSc2	kniha
a	a	k8xC	a
Festival	festival	k1gInSc1	festival
spisovatelů	spisovatel	k1gMnPc2	spisovatel
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
rockové	rockový	k2eAgFnSc2d1	rocková
a	a	k8xC	a
popové	popový	k2eAgFnSc2d1	popová
hudby	hudba	k1gFnSc2	hudba
jsou	být	k5eAaImIp3nP	být
největšími	veliký	k2eAgInPc7d3	veliký
festivaly	festival	k1gInPc7	festival
Rock	rock	k1gInSc1	rock
for	forum	k1gNnPc2	forum
People	People	k1gFnSc2	People
<g/>
,	,	kIx,	,
Colours	Colours	k1gInSc1	Colours
of	of	k?	of
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
,	,	kIx,	,
Trutnov	Trutnov	k1gInSc1	Trutnov
Open	Opena	k1gFnPc2	Opena
Air	Air	k1gFnPc2	Air
Festival	festival	k1gInSc1	festival
<g/>
,	,	kIx,	,
Benátská	benátský	k2eAgFnSc1d1	Benátská
noc	noc	k1gFnSc1	noc
<g/>
,	,	kIx,	,
Hrady	hrad	k1gInPc1	hrad
CZ	CZ	kA	CZ
<g/>
,	,	kIx,	,
Votvírák	Votvírák	k1gMnSc1	Votvírák
<g/>
,	,	kIx,	,
United	United	k1gMnSc1	United
Islands	Islandsa	k1gFnPc2	Islandsa
of	of	k?	of
Prague	Prague	k1gNnSc4	Prague
<g/>
,	,	kIx,	,
Sázavafest	Sázavafest	k1gFnSc4	Sázavafest
<g/>
,	,	kIx,	,
Rock	rock	k1gInSc4	rock
for	forum	k1gNnPc2	forum
Churchill	Churchilla	k1gFnPc2	Churchilla
<g/>
,	,	kIx,	,
Footfest	Footfest	k1gFnSc1	Footfest
<g/>
,	,	kIx,	,
Keltská	keltský	k2eAgFnSc1d1	keltská
noc	noc	k1gFnSc1	noc
či	či	k8xC	či
Mácháč	Mácháč	k1gInSc1	Mácháč
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
rapu	rapa	k1gFnSc4	rapa
a	a	k8xC	a
hip	hip	k0	hip
hopu	hopus	k1gInSc6	hopus
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc7d3	veliký
akcí	akce	k1gFnSc7	akce
Hip	hip	k0	hip
Hop	hop	k0	hop
Kemp	kemp	k1gInSc1	kemp
u	u	k7c2	u
Hradce	Hradec	k1gInSc2	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
,	,	kIx,	,
největšími	veliký	k2eAgInPc7d3	veliký
metalovými	metalový	k2eAgInPc7d1	metalový
festivaly	festival	k1gInPc7	festival
jsou	být	k5eAaImIp3nP	být
Masters	Masters	k1gInSc4	Masters
of	of	k?	of
Rock	rock	k1gInSc1	rock
ve	v	k7c6	v
Vizovicích	Vizovice	k1gFnPc6	Vizovice
a	a	k8xC	a
Brutal	Brutal	k1gMnSc1	Brutal
Assault	Assault	k1gMnSc1	Assault
v	v	k7c6	v
Josefově	Josefov	k1gInSc6	Josefov
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
taneční	taneční	k2eAgFnSc2d1	taneční
hudby	hudba	k1gFnSc2	hudba
Beats	Beatsa	k1gFnPc2	Beatsa
for	forum	k1gNnPc2	forum
Love	lov	k1gInSc5	lov
ve	v	k7c6	v
Vítkovicích	Vítkovice	k1gInPc6	Vítkovice
<g/>
,	,	kIx,	,
Let	let	k1gInSc1	let
It	It	k1gFnSc2	It
Roll	Rolla	k1gFnPc2	Rolla
a	a	k8xC	a
Mighty	Mighta	k1gFnSc2	Mighta
Sounds	Soundsa	k1gFnPc2	Soundsa
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc7d3	veliký
akcí	akce	k1gFnSc7	akce
lidové	lidový	k2eAgFnSc2d1	lidová
hudby	hudba	k1gFnSc2	hudba
je	být	k5eAaImIp3nS	být
tradičně	tradičně	k6eAd1	tradičně
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
folklorní	folklorní	k2eAgInSc1d1	folklorní
festival	festival	k1gInSc1	festival
Strážnice	Strážnice	k1gFnSc2	Strážnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Největším	veliký	k2eAgInSc7d3	veliký
a	a	k8xC	a
nejtradičnějším	tradiční	k2eAgInSc7d3	nejtradičnější
filmovým	filmový	k2eAgInSc7d1	filmový
festivalem	festival	k1gInSc7	festival
je	být	k5eAaImIp3nS	být
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
filmový	filmový	k2eAgInSc1d1	filmový
festival	festival	k1gInSc1	festival
Karlovy	Karlův	k2eAgInPc1d1	Karlův
Vary	Vary	k1gInPc1	Vary
<g/>
.	.	kIx.	.
</s>
<s>
Jemu	on	k3xPp3gMnSc3	on
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
širokým	široký	k2eAgInSc7d1	široký
záběrem	záběr	k1gInSc7	záběr
konkurovat	konkurovat	k5eAaImF	konkurovat
Febiofest	Febiofest	k1gFnSc4	Febiofest
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
festivaly	festival	k1gInPc1	festival
mají	mít	k5eAaImIp3nP	mít
užší	úzký	k2eAgNnSc4d2	užší
žánrové	žánrový	k2eAgNnSc4d1	žánrové
zaměření	zaměření	k1gNnSc4	zaměření
–	–	k?	–
na	na	k7c4	na
dětský	dětský	k2eAgInSc4d1	dětský
film	film	k1gInSc4	film
se	se	k3xPyFc4	se
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
Film	film	k1gInSc1	film
festival	festival	k1gInSc1	festival
Zlín	Zlín	k1gInSc1	Zlín
<g/>
,	,	kIx,	,
na	na	k7c4	na
film	film	k1gInSc4	film
kreslený	kreslený	k2eAgInSc4d1	kreslený
a	a	k8xC	a
loutkový	loutkový	k2eAgInSc4d1	loutkový
Anifest	Anifest	k1gInSc4	Anifest
<g/>
,	,	kIx,	,
na	na	k7c4	na
film	film	k1gInSc4	film
dokumentární	dokumentární	k2eAgInSc4d1	dokumentární
Jeden	jeden	k4xCgInSc4	jeden
svět	svět	k1gInSc4	svět
a	a	k8xC	a
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g/>
,	,	kIx,	,
výhradně	výhradně	k6eAd1	výhradně
české	český	k2eAgInPc1d1	český
filmy	film	k1gInPc1	film
jsou	být	k5eAaImIp3nP	být
uváděny	uvádět	k5eAaImNgInP	uvádět
na	na	k7c6	na
Finále	finála	k1gFnSc6	finála
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
tradici	tradice	k1gFnSc4	tradice
má	mít	k5eAaImIp3nS	mít
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
televizní	televizní	k2eAgInSc1d1	televizní
festival	festival	k1gInSc1	festival
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tradičními	tradiční	k2eAgFnPc7d1	tradiční
přehlídkami	přehlídka	k1gFnPc7	přehlídka
průmyslovými	průmyslový	k2eAgFnPc7d1	průmyslová
jsou	být	k5eAaImIp3nP	být
brněnský	brněnský	k2eAgInSc1d1	brněnský
strojírenský	strojírenský	k2eAgInSc1d1	strojírenský
veletrh	veletrh	k1gInSc1	veletrh
a	a	k8xC	a
zemědělská	zemědělský	k2eAgFnSc1d1	zemědělská
výstava	výstava	k1gFnSc1	výstava
Země	zem	k1gFnSc2	zem
živitelka	živitelka	k1gFnSc1	živitelka
v	v	k7c6	v
Českých	český	k2eAgInPc6d1	český
Budějovicích	Budějovice	k1gInPc6	Budějovice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ke	k	k7c3	k
společensko-kulturním	společenskoulturní	k2eAgFnPc3d1	společensko-kulturní
událostem	událost	k1gFnPc3	událost
patří	patřit	k5eAaImIp3nS	patřit
rovněž	rovněž	k9	rovněž
udílení	udílení	k1gNnSc1	udílení
různých	různý	k2eAgFnPc2d1	různá
cen	cena	k1gFnPc2	cena
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc3d3	veliký
prestiži	prestiž	k1gFnSc3	prestiž
se	se	k3xPyFc4	se
těší	těšit	k5eAaImIp3nS	těšit
cena	cena	k1gFnSc1	cena
Český	český	k2eAgInSc1d1	český
lev	lev	k1gInSc1	lev
pro	pro	k7c4	pro
filmaře	filmař	k1gMnPc4	filmař
<g/>
,	,	kIx,	,
Ceny	cena	k1gFnPc4	cena
Thálie	Thálie	k1gFnSc2	Thálie
pro	pro	k7c4	pro
divadelníky	divadelník	k1gMnPc4	divadelník
<g/>
,	,	kIx,	,
Ceny	cena	k1gFnSc2	cena
Anděl	Anděla	k1gFnPc2	Anděla
pro	pro	k7c4	pro
hudebníky	hudebník	k1gMnPc4	hudebník
<g/>
,	,	kIx,	,
Sportovec	sportovec	k1gMnSc1	sportovec
roku	rok	k1gInSc2	rok
či	či	k8xC	či
ceny	cena	k1gFnSc2	cena
Česká	český	k2eAgFnSc1d1	Česká
hlava	hlava	k1gFnSc1	hlava
pro	pro	k7c4	pro
vědce	vědec	k1gMnPc4	vědec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Věda	věda	k1gFnSc1	věda
a	a	k8xC	a
vzdělávání	vzdělávání	k1gNnSc1	vzdělávání
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Věda	věda	k1gFnSc1	věda
a	a	k8xC	a
technika	technika	k1gFnSc1	technika
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Humanitní	humanitní	k2eAgFnSc2d1	humanitní
a	a	k8xC	a
sociální	sociální	k2eAgFnSc2d1	sociální
vědy	věda	k1gFnSc2	věda
====	====	k?	====
</s>
</p>
<p>
<s>
Zakladatelem	zakladatel	k1gMnSc7	zakladatel
české	český	k2eAgFnSc2d1	Česká
vzdělanosti	vzdělanost	k1gFnSc2	vzdělanost
byl	být	k5eAaImAgMnS	být
Konstantin	Konstantin	k1gMnSc1	Konstantin
Filozof	filozof	k1gMnSc1	filozof
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
významný	významný	k2eAgMnSc1d1	významný
myslitel	myslitel	k1gMnSc1	myslitel
působící	působící	k2eAgMnSc1d1	působící
na	na	k7c6	na
českém	český	k2eAgNnSc6d1	české
území	území	k1gNnSc6	území
<g/>
.	.	kIx.	.
</s>
<s>
Významnými	významný	k2eAgMnPc7d1	významný
středověkými	středověký	k2eAgMnPc7d1	středověký
teology	teolog	k1gMnPc7	teolog
byli	být	k5eAaImAgMnP	být
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
<g/>
,	,	kIx,	,
Jeroným	Jeroným	k1gMnSc1	Jeroným
Pražský	pražský	k2eAgMnSc1d1	pražský
a	a	k8xC	a
Petr	Petr	k1gMnSc1	Petr
Chelčický	Chelčický	k2eAgMnSc1d1	Chelčický
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Amos	Amos	k1gMnSc1	Amos
Komenský	Komenský	k1gMnSc1	Komenský
podstatným	podstatný	k2eAgInSc7d1	podstatný
způsobem	způsob	k1gInSc7	způsob
přispěl	přispět	k5eAaPmAgInS	přispět
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
moderní	moderní	k2eAgFnSc2d1	moderní
pedagogiky	pedagogika	k1gFnSc2	pedagogika
<g/>
,	,	kIx,	,
především	především	k9	především
ve	v	k7c6	v
spisech	spis	k1gInPc6	spis
Didaktica	Didaktic	k2eAgFnSc1d1	Didaktic
magna	magna	k1gFnSc1	magna
(	(	kIx(	(
<g/>
Velká	velký	k2eAgFnSc1d1	velká
didaktika	didaktika	k1gFnSc1	didaktika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Janua	Janua	k1gFnSc1	Janua
linguarum	linguarum	k1gInSc1	linguarum
reserata	reserata	k1gFnSc1	reserata
(	(	kIx(	(
<g/>
Dveře	dveře	k1gFnPc1	dveře
jazyků	jazyk	k1gInPc2	jazyk
otevřené	otevřený	k2eAgFnPc1d1	otevřená
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Orbis	orbis	k1gInSc1	orbis
pictus	pictus	k1gInSc1	pictus
(	(	kIx(	(
<g/>
Svět	svět	k1gInSc1	svět
v	v	k7c6	v
obrazech	obraz	k1gInPc6	obraz
<g/>
)	)	kIx)	)
a	a	k8xC	a
Schola	Schola	k1gFnSc1	Schola
ludus	ludus	k1gInSc1	ludus
(	(	kIx(	(
<g/>
Škola	škola	k1gFnSc1	škola
na	na	k7c6	na
jevišti	jeviště	k1gNnSc6	jeviště
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Židovskou	židovský	k2eAgFnSc4d1	židovská
středověkou	středověký	k2eAgFnSc4d1	středověká
vzdělanost	vzdělanost	k1gFnSc4	vzdělanost
reprezentoval	reprezentovat	k5eAaImAgInS	reprezentovat
především	především	k6eAd1	především
rabbi	rabbe	k1gFnSc4	rabbe
Löw	Löw	k1gMnSc3	Löw
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
i	i	k9	i
do	do	k7c2	do
českých	český	k2eAgFnPc2d1	Česká
pověstí	pověst	k1gFnPc2	pověst
(	(	kIx(	(
<g/>
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
golemem	golem	k1gMnSc7	golem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějším	významný	k2eAgMnSc7d3	nejvýznamnější
historikem	historik	k1gMnSc7	historik
<g/>
,	,	kIx,	,
filozofem	filozof	k1gMnSc7	filozof
a	a	k8xC	a
intelektuálem	intelektuál	k1gMnSc7	intelektuál
barokní	barokní	k2eAgFnSc2d1	barokní
éry	éra	k1gFnSc2	éra
(	(	kIx(	(
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Balbín	Balbín	k1gMnSc1	Balbín
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výraznou	výrazný	k2eAgFnSc4d1	výrazná
roli	role	k1gFnSc4	role
v	v	k7c6	v
národním	národní	k2eAgNnSc6d1	národní
obrození	obrození	k1gNnSc6	obrození
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
polovina	polovina	k1gFnSc1	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
sehrála	sehrát	k5eAaPmAgFnS	sehrát
jazykověda	jazykověda	k1gFnSc1	jazykověda
(	(	kIx(	(
<g/>
Josef	Josef	k1gMnSc1	Josef
Dobrovský	Dobrovský	k1gMnSc1	Dobrovský
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Jungmann	Jungmann	k1gMnSc1	Jungmann
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Jozef	Jozef	k1gMnSc1	Jozef
Šafařík	Šafařík	k1gMnSc1	Šafařík
<g/>
,	,	kIx,	,
Ján	Ján	k1gMnSc1	Ján
Kollár	Kollár	k1gMnSc1	Kollár
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Ladislav	Ladislav	k1gMnSc1	Ladislav
Čelakovský	Čelakovský	k2eAgMnSc1d1	Čelakovský
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
historiografie	historiografie	k1gFnSc1	historiografie
(	(	kIx(	(
<g/>
Gelasius	Gelasius	k1gMnSc1	Gelasius
Dobner	Dobner	k1gMnSc1	Dobner
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Palacký	Palacký	k1gMnSc1	Palacký
<g/>
)	)	kIx)	)
a	a	k8xC	a
folkloristika	folkloristika	k1gFnSc1	folkloristika
(	(	kIx(	(
<g/>
Karel	Karel	k1gMnSc1	Karel
Jaromír	Jaromír	k1gMnSc1	Jaromír
Erben	Erben	k1gMnSc1	Erben
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
století	století	k1gNnSc2	století
sílu	síl	k1gInSc2	síl
české	český	k2eAgFnSc2d1	Česká
vzdělanosti	vzdělanost	k1gFnSc2	vzdělanost
manifestoval	manifestovat	k5eAaBmAgInS	manifestovat
Ottův	Ottův	k2eAgInSc1d1	Ottův
slovník	slovník	k1gInSc1	slovník
naučný	naučný	k2eAgInSc1d1	naučný
(	(	kIx(	(
<g/>
vydáván	vydávat	k5eAaPmNgMnS	vydávat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1888	[number]	k4	1888
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
vůdčím	vůdčí	k2eAgFnPc3d1	vůdčí
osobnostem	osobnost	k1gFnPc3	osobnost
českého	český	k2eAgInSc2d1	český
intelektuálního	intelektuální	k2eAgInSc2d1	intelektuální
života	život	k1gInSc2	život
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
patřil	patřit	k5eAaImAgMnS	patřit
filozof	filozof	k1gMnSc1	filozof
Tomáš	Tomáš	k1gMnSc1	Tomáš
Garrigue	Garrigue	k1gFnPc2	Garrigue
Masaryk	Masaryk	k1gMnSc1	Masaryk
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
okruh	okruh	k1gInSc1	okruh
kolem	kolem	k7c2	kolem
časopisu	časopis	k1gInSc2	časopis
Čas	čas	k1gInSc1	čas
(	(	kIx(	(
<g/>
Jan	Jan	k1gMnSc1	Jan
Herben	Herben	k2eAgMnSc1d1	Herben
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Gebauer	Gebauer	k1gMnSc1	Gebauer
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
tento	tento	k3xDgInSc1	tento
okruh	okruh	k1gInSc1	okruh
odstartoval	odstartovat	k5eAaPmAgInS	odstartovat
tzv.	tzv.	kA	tzv.
rukopisné	rukopisný	k2eAgInPc1d1	rukopisný
spory	spor	k1gInPc1	spor
roku	rok	k1gInSc2	rok
1886	[number]	k4	1886
a	a	k8xC	a
spor	spor	k1gInSc1	spor
o	o	k7c4	o
smysl	smysl	k1gInSc4	smysl
českých	český	k2eAgFnPc2d1	Česká
dějin	dějiny	k1gFnPc2	dějiny
roku	rok	k1gInSc2	rok
1912	[number]	k4	1912
<g/>
,	,	kIx,	,
právě	právě	k6eAd1	právě
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
diskusích	diskuse	k1gFnPc6	diskuse
český	český	k2eAgInSc1d1	český
národ	národ	k1gInSc1	národ
tehdy	tehdy	k6eAd1	tehdy
nejvíce	hodně	k6eAd3	hodně
ohmatával	ohmatávat	k5eAaImAgMnS	ohmatávat
svou	svůj	k3xOyFgFnSc4	svůj
identitu	identita	k1gFnSc4	identita
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgMnSc7d1	významný
intelektuálem	intelektuál	k1gMnSc7	intelektuál
byl	být	k5eAaImAgMnS	být
i	i	k9	i
Konstantin	Konstantin	k1gMnSc1	Konstantin
Jireček	Jireček	k1gMnSc1	Jireček
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
rozvinul	rozvinout	k5eAaPmAgInS	rozvinout
obor	obor	k1gInSc4	obor
byzantologie	byzantologie	k1gFnSc2	byzantologie
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
např.	např.	kA	např.
Jirečkova	Jirečkův	k2eAgFnSc1d1	Jirečkova
linie	linie	k1gFnSc1	linie
<g/>
)	)	kIx)	)
a	a	k8xC	a
sehrál	sehrát	k5eAaPmAgMnS	sehrát
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
v	v	k7c6	v
bulharském	bulharský	k2eAgNnSc6d1	bulharské
národním	národní	k2eAgNnSc6d1	národní
obrození	obrození	k1gNnSc6	obrození
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
vynikající	vynikající	k2eAgMnSc1d1	vynikající
orientalista	orientalista	k1gMnSc1	orientalista
a	a	k8xC	a
archeolog	archeolog	k1gMnSc1	archeolog
Alois	Alois	k1gMnSc1	Alois
Musil	Musil	k1gMnSc1	Musil
(	(	kIx(	(
<g/>
bratranec	bratranec	k1gMnSc1	bratranec
slavného	slavný	k2eAgMnSc2d1	slavný
rakouského	rakouský	k2eAgMnSc2d1	rakouský
spisovatele	spisovatel	k1gMnSc2	spisovatel
Roberta	Robert	k1gMnSc2	Robert
Musila	Musil	k1gMnSc2	Musil
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc4d1	velká
společenskou	společenský	k2eAgFnSc4d1	společenská
roli	role	k1gFnSc4	role
sehrával	sehrávat	k5eAaImAgMnS	sehrávat
národopisec	národopisec	k1gMnSc1	národopisec
Vojta	Vojta	k1gMnSc1	Vojta
Náprstek	náprstek	k1gInSc1	náprstek
či	či	k8xC	či
etnograf	etnograf	k1gMnSc1	etnograf
Emil	Emil	k1gMnSc1	Emil
Holub	Holub	k1gMnSc1	Holub
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Navzdory	navzdory	k7c3	navzdory
rozvoji	rozvoj	k1gInSc3	rozvoj
<g/>
,	,	kIx,	,
mnozí	mnohý	k2eAgMnPc1d1	mnohý
nadaní	nadaný	k2eAgMnPc1d1	nadaný
rodáci	rodák	k1gMnPc1	rodák
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
německy	německy	k6eAd1	německy
mluvící	mluvící	k2eAgFnSc1d1	mluvící
<g/>
,	,	kIx,	,
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
české	český	k2eAgFnSc2d1	Česká
země	zem	k1gFnSc2	zem
opouštěli	opouštět	k5eAaImAgMnP	opouštět
a	a	k8xC	a
uplatňovali	uplatňovat	k5eAaImAgMnP	uplatňovat
se	se	k3xPyFc4	se
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
a	a	k8xC	a
jinde	jinde	k6eAd1	jinde
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
filosof	filosof	k1gMnSc1	filosof
Edmund	Edmund	k1gMnSc1	Edmund
Husserl	Husserl	k1gMnSc1	Husserl
<g/>
,	,	kIx,	,
psychoanalytik	psychoanalytik	k1gMnSc1	psychoanalytik
Sigmund	Sigmund	k1gMnSc1	Sigmund
Freud	Freud	k1gMnSc1	Freud
<g/>
,	,	kIx,	,
ekonom	ekonom	k1gMnSc1	ekonom
Joseph	Joseph	k1gMnSc1	Joseph
Schumpeter	Schumpeter	k1gMnSc1	Schumpeter
<g/>
,	,	kIx,	,
marxistický	marxistický	k2eAgMnSc1d1	marxistický
teoretik	teoretik	k1gMnSc1	teoretik
Karl	Karl	k1gMnSc1	Karl
Kautsky	Kautsky	k1gMnSc1	Kautsky
<g/>
,	,	kIx,	,
právní	právní	k2eAgMnSc1d1	právní
teoretik	teoretik	k1gMnSc1	teoretik
Hans	Hans	k1gMnSc1	Hans
Kelsen	Kelsen	k2eAgMnSc1d1	Kelsen
<g/>
,	,	kIx,	,
ekonom	ekonom	k1gMnSc1	ekonom
Eugen	Eugen	k2eAgInSc4d1	Eugen
Böhm	Böhm	k1gInSc4	Böhm
von	von	k1gInSc1	von
Bawerk	Bawerk	k1gInSc1	Bawerk
<g/>
,	,	kIx,	,
psycholog	psycholog	k1gMnSc1	psycholog
Max	Max	k1gMnSc1	Max
Wertheimer	Wertheimer	k1gMnSc1	Wertheimer
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnPc1d1	hudební
teoretici	teoretik	k1gMnPc1	teoretik
Eduard	Eduard	k1gMnSc1	Eduard
Hanslick	Hanslick	k1gMnSc1	Hanslick
a	a	k8xC	a
Guido	Guido	k1gNnSc1	Guido
Adler	Adler	k1gMnSc1	Adler
<g/>
,	,	kIx,	,
jazykovědec	jazykovědec	k1gMnSc1	jazykovědec
Julius	Julius	k1gMnSc1	Julius
Pokorny	Pokorny	k?	Pokorny
či	či	k8xC	či
filozof	filozof	k1gMnSc1	filozof
Herbert	Herbert	k1gMnSc1	Herbert
Feigl	Feigl	k1gMnSc1	Feigl
<g/>
.	.	kIx.	.
</s>
<s>
Odcházeli	odcházet	k5eAaImAgMnP	odcházet
ale	ale	k8xC	ale
i	i	k9	i
někteří	některý	k3yIgMnPc1	některý
česky	česky	k6eAd1	česky
mluvící	mluvící	k2eAgMnPc1d1	mluvící
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
historik	historik	k1gMnSc1	historik
umění	umění	k1gNnSc2	umění
Max	Max	k1gMnSc1	Max
Dvořák	Dvořák	k1gMnSc1	Dvořák
či	či	k8xC	či
antropolog	antropolog	k1gMnSc1	antropolog
Aleš	Aleš	k1gMnSc1	Aleš
Hrdlička	Hrdlička	k1gMnSc1	Hrdlička
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
zůstal	zůstat	k5eAaPmAgMnS	zůstat
filozof	filozof	k1gMnSc1	filozof
Bernard	Bernard	k1gMnSc1	Bernard
Bolzano	Bolzana	k1gFnSc5	Bolzana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c2	za
první	první	k4xOgFnSc2	první
republiky	republika	k1gFnSc2	republika
vědeckých	vědecký	k2eAgInPc2d1	vědecký
úspěchů	úspěch	k1gInPc2	úspěch
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
orientalista	orientalista	k1gMnSc1	orientalista
Bedřich	Bedřich	k1gMnSc1	Bedřich
Hrozný	hrozný	k2eAgMnSc1d1	hrozný
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
rozluštil	rozluštit	k5eAaPmAgInS	rozluštit
jazyk	jazyk	k1gInSc4	jazyk
Chetitů	Chetit	k1gMnPc2	Chetit
<g/>
,	,	kIx,	,
archeolog	archeolog	k1gMnSc1	archeolog
Karel	Karel	k1gMnSc1	Karel
Absolon	Absolon	k1gMnSc1	Absolon
objevil	objevit	k5eAaPmAgMnS	objevit
proslulou	proslulý	k2eAgFnSc4d1	proslulá
Věstonickou	věstonický	k2eAgFnSc4d1	Věstonická
venuši	venuše	k1gFnSc4	venuše
<g/>
,	,	kIx,	,
významným	významný	k2eAgMnSc7d1	významný
archeologem	archeolog	k1gMnSc7	archeolog
byl	být	k5eAaImAgMnS	být
i	i	k9	i
Lubor	Lubor	k1gMnSc1	Lubor
Niederle	Niederle	k1gFnSc2	Niederle
<g/>
.	.	kIx.	.
</s>
<s>
Mimořádný	mimořádný	k2eAgInSc4d1	mimořádný
rozkvět	rozkvět	k1gInSc4	rozkvět
zažívala	zažívat	k5eAaImAgFnS	zažívat
jazykověda	jazykověda	k1gFnSc1	jazykověda
<g/>
,	,	kIx,	,
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
se	se	k3xPyFc4	se
ustavil	ustavit	k5eAaPmAgInS	ustavit
tzv.	tzv.	kA	tzv.
Pražský	pražský	k2eAgInSc1d1	pražský
lingvistický	lingvistický	k2eAgInSc1d1	lingvistický
kroužek	kroužek	k1gInSc1	kroužek
(	(	kIx(	(
<g/>
Vilém	Vilém	k1gMnSc1	Vilém
Mathesius	Mathesius	k1gMnSc1	Mathesius
<g/>
,	,	kIx,	,
Roman	Roman	k1gMnSc1	Roman
Jakobson	Jakobson	k1gMnSc1	Jakobson
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Mukařovský	Mukařovský	k1gMnSc1	Mukařovský
<g/>
,	,	kIx,	,
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Havránek	Havránek	k1gMnSc1	Havránek
<g/>
,	,	kIx,	,
René	René	k1gMnSc1	René
Wellek	Wellek	k1gMnSc1	Wellek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
prosazoval	prosazovat	k5eAaImAgMnS	prosazovat
strukturalistický	strukturalistický	k2eAgInSc4d1	strukturalistický
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
jazyku	jazyk	k1gInSc3	jazyk
a	a	k8xC	a
světu	svět	k1gInSc3	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nacistická	nacistický	k2eAgFnSc1d1	nacistická
okupace	okupace	k1gFnSc1	okupace
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
vyhnala	vyhnat	k5eAaPmAgFnS	vyhnat
i	i	k8xC	i
významné	významný	k2eAgMnPc4d1	významný
budoucí	budoucí	k2eAgMnPc4d1	budoucí
myslitele	myslitel	k1gMnPc4	myslitel
<g/>
,	,	kIx,	,
sociologa	sociolog	k1gMnSc2	sociolog
Ernesta	Ernest	k1gMnSc2	Ernest
Gellnera	Gellner	k1gMnSc2	Gellner
a	a	k8xC	a
filozofa	filozof	k1gMnSc2	filozof
Viléma	Vilém	k1gMnSc2	Vilém
Flussera	Flusser	k1gMnSc2	Flusser
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
zájem	zájem	k1gInSc4	zájem
světa	svět	k1gInSc2	svět
budil	budit	k5eAaImAgInS	budit
neortodoxní	ortodoxní	k2eNgInSc1d1	neortodoxní
marxismus	marxismus	k1gInSc1	marxismus
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgInS	být
hlavním	hlavní	k2eAgNnSc7d1	hlavní
ideovým	ideový	k2eAgNnSc7d1	ideové
zázemím	zázemí	k1gNnSc7	zázemí
pražského	pražský	k2eAgNnSc2d1	Pražské
jara	jaro	k1gNnSc2	jaro
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
a	a	k8xC	a
který	který	k3yQgInSc4	který
reprezentovali	reprezentovat	k5eAaImAgMnP	reprezentovat
zejména	zejména	k9	zejména
Karel	Karel	k1gMnSc1	Karel
Kosík	Kosík	k1gMnSc1	Kosík
(	(	kIx(	(
<g/>
spis	spis	k1gInSc1	spis
Dialektika	dialektik	k1gMnSc2	dialektik
konkrétního	konkrétní	k2eAgMnSc2d1	konkrétní
<g/>
)	)	kIx)	)
či	či	k8xC	či
Eduard	Eduard	k1gMnSc1	Eduard
Goldstücker	Goldstücker	k1gMnSc1	Goldstücker
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
to	ten	k3xDgNnSc1	ten
byly	být	k5eAaImAgFnP	být
spíše	spíše	k9	spíše
myšlenky	myšlenka	k1gFnPc1	myšlenka
filozofa	filozof	k1gMnSc2	filozof
Jana	Jan	k1gMnSc2	Jan
Patočky	Patočka	k1gMnSc2	Patočka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
ideovou	ideový	k2eAgFnSc7d1	ideová
základnou	základna	k1gFnSc7	základna
Charty	charta	k1gFnSc2	charta
77	[number]	k4	77
<g/>
,	,	kIx,	,
či	či	k8xC	či
filozofa	filozof	k1gMnSc2	filozof
undergroundu	underground	k1gInSc2	underground
Egona	Egon	k1gMnSc4	Egon
Bondyho	Bondy	k1gMnSc4	Bondy
<g/>
.	.	kIx.	.
</s>
<s>
Vysokou	vysoký	k2eAgFnSc4d1	vysoká
úroveň	úroveň	k1gFnSc4	úroveň
měla	mít	k5eAaImAgFnS	mít
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
socialistického	socialistický	k2eAgInSc2d1	socialistický
režimu	režim	k1gInSc2	režim
egyptologie	egyptologie	k1gFnSc2	egyptologie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
prezentovala	prezentovat	k5eAaBmAgFnS	prezentovat
mj.	mj.	kA	mj.
vykopávkami	vykopávka	k1gFnPc7	vykopávka
v	v	k7c6	v
Abusíru	Abusír	k1gInSc6	Abusír
(	(	kIx(	(
<g/>
zejm.	zejm.	k?	zejm.
Miroslav	Miroslav	k1gMnSc1	Miroslav
Verner	Verner	k1gMnSc1	Verner
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštní	zvláštní	k2eAgFnSc7d1	zvláštní
kapitolou	kapitola	k1gFnSc7	kapitola
dějin	dějiny	k1gFnPc2	dějiny
vědy	věda	k1gFnSc2	věda
v	v	k7c6	v
socialistickém	socialistický	k2eAgNnSc6d1	socialistické
Československu	Československo	k1gNnSc6	Československo
byl	být	k5eAaImAgInS	být
státní	státní	k2eAgInSc1d1	státní
psychologický	psychologický	k2eAgInSc1d1	psychologický
výzkum	výzkum	k1gInSc1	výzkum
účinků	účinek	k1gInPc2	účinek
LSD	LSD	kA	LSD
<g/>
,	,	kIx,	,
podílel	podílet	k5eAaImAgMnS	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
něm	on	k3xPp3gMnSc6	on
i	i	k9	i
Stanislav	Stanislav	k1gMnSc1	Stanislav
Grof	Grof	k1gMnSc1	Grof
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
USA	USA	kA	USA
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
metodu	metoda	k1gFnSc4	metoda
holotropního	holotropní	k2eAgNnSc2d1	holotropní
dýchání	dýchání	k1gNnSc2	dýchání
<g/>
.	.	kIx.	.
</s>
<s>
Světový	světový	k2eAgInSc4d1	světový
zájem	zájem	k1gInSc4	zájem
vzbudily	vzbudit	k5eAaPmAgInP	vzbudit
i	i	k9	i
archeologické	archeologický	k2eAgInPc1d1	archeologický
experimenty	experiment	k1gInPc1	experiment
Pavla	Pavel	k1gMnSc2	Pavel
Pavla	Pavel	k1gMnSc2	Pavel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Přírodní	přírodní	k2eAgFnSc2d1	přírodní
<g/>
,	,	kIx,	,
exaktní	exaktní	k2eAgFnSc2d1	exaktní
a	a	k8xC	a
technické	technický	k2eAgFnSc2d1	technická
vědy	věda	k1gFnSc2	věda
====	====	k?	====
</s>
</p>
<p>
<s>
Zásadním	zásadní	k2eAgInSc7d1	zásadní
impulsem	impuls	k1gInSc7	impuls
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
vědeckého	vědecký	k2eAgNnSc2d1	vědecké
myšlení	myšlení	k1gNnSc2	myšlení
(	(	kIx(	(
<g/>
byť	byť	k8xS	byť
to	ten	k3xDgNnSc1	ten
nebylo	být	k5eNaImAgNnS	být
v	v	k7c6	v
počátcích	počátek	k1gInPc6	počátek
samozřejmě	samozřejmě	k6eAd1	samozřejmě
odděleno	oddělen	k2eAgNnSc1d1	odděleno
plně	plně	k6eAd1	plně
od	od	k7c2	od
myšlení	myšlení	k1gNnSc2	myšlení
filozofického	filozofický	k2eAgNnSc2d1	filozofické
a	a	k8xC	a
teologického	teologický	k2eAgNnSc2d1	teologické
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
založení	založení	k1gNnSc4	založení
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
univerzity	univerzita	k1gFnSc2	univerzita
králem	král	k1gMnSc7	král
Karlem	Karel	k1gMnSc7	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
roku	rok	k1gInSc2	rok
1348	[number]	k4	1348
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
první	první	k4xOgFnSc4	první
univerzitu	univerzita	k1gFnSc4	univerzita
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nelze	lze	k6eNd1	lze
vyloučit	vyloučit	k5eAaPmF	vyloučit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1459	[number]	k4	1459
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
Českém	český	k2eAgInSc6d1	český
Krumlově	Krumlov	k1gInSc6	Krumlov
vynikající	vynikající	k2eAgMnSc1d1	vynikající
kartograf	kartograf	k1gMnSc1	kartograf
Martin	Martin	k1gMnSc1	Martin
Behaim	Behaim	k1gMnSc1	Behaim
<g/>
,	,	kIx,	,
tvůrce	tvůrce	k1gMnSc1	tvůrce
nejstaršího	starý	k2eAgInSc2d3	nejstarší
dochovaného	dochovaný	k2eAgInSc2d1	dochovaný
glóbusu	glóbus	k1gInSc2	glóbus
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1460	[number]	k4	1460
se	se	k3xPyFc4	se
v	v	k7c6	v
Chebu	Cheb	k1gInSc6	Cheb
narodil	narodit	k5eAaPmAgMnS	narodit
významný	významný	k2eAgMnSc1d1	významný
matematik	matematik	k1gMnSc1	matematik
Johannes	Johannes	k1gMnSc1	Johannes
Widmann	Widmann	k1gMnSc1	Widmann
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
zavedl	zavést	k5eAaPmAgMnS	zavést
znaménka	znaménko	k1gNnSc2	znaménko
plus	plus	k6eAd1	plus
a	a	k8xC	a
minus	minus	k6eAd1	minus
<g/>
,	,	kIx,	,
působil	působit	k5eAaImAgMnS	působit
ale	ale	k9	ale
po	po	k7c4	po
většinu	většina	k1gFnSc4	většina
života	život	k1gInSc2	život
v	v	k7c6	v
Lipsku	Lipsko	k1gNnSc6	Lipsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Érou	éra	k1gFnSc7	éra
vědeckého	vědecký	k2eAgInSc2d1	vědecký
rozkvětu	rozkvět	k1gInSc2	rozkvět
byla	být	k5eAaImAgFnS	být
i	i	k9	i
epocha	epocha	k1gFnSc1	epocha
Rudolfa	Rudolf	k1gMnSc2	Rudolf
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
pražském	pražský	k2eAgInSc6d1	pražský
dvoře	dvůr	k1gInSc6	dvůr
působili	působit	k5eAaImAgMnP	působit
astronomové	astronom	k1gMnPc1	astronom
Tycho	Tyc	k1gMnSc2	Tyc
de	de	k?	de
Brahe	Brah	k1gMnSc2	Brah
a	a	k8xC	a
Johannes	Johannes	k1gMnSc1	Johannes
Kepler	Kepler	k1gMnSc1	Kepler
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
navíc	navíc	k6eAd1	navíc
působil	působit	k5eAaImAgMnS	působit
i	i	k9	i
významný	významný	k2eAgMnSc1d1	významný
židovský	židovský	k2eAgMnSc1d1	židovský
matematik	matematik	k1gMnSc1	matematik
David	David	k1gMnSc1	David
Gans	Gans	k1gInSc4	Gans
<g/>
.	.	kIx.	.
</s>
<s>
Lékař	lékař	k1gMnSc1	lékař
Ján	Ján	k1gMnSc1	Ján
Jesenský	Jesenský	k1gMnSc1	Jesenský
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
provedl	provést	k5eAaPmAgMnS	provést
první	první	k4xOgFnSc4	první
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
pitvu	pitva	k1gFnSc4	pitva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
vrcholným	vrcholný	k2eAgMnPc3d1	vrcholný
vzdělancům	vzdělanec	k1gMnPc3	vzdělanec
domácí	domácí	k2eAgFnSc2d1	domácí
barokní	barokní	k2eAgFnSc2d1	barokní
vědy	věda	k1gFnSc2	věda
patřil	patřit	k5eAaImAgMnS	patřit
Jan	Jan	k1gMnSc1	Jan
Marek	Marek	k1gMnSc1	Marek
Marci	Marek	k1gMnPc1	Marek
z	z	k7c2	z
Kronlandu	Kronland	k1gInSc2	Kronland
či	či	k8xC	či
botanik	botanik	k1gMnSc1	botanik
Georg	Georg	k1gMnSc1	Georg
Joseph	Joseph	k1gMnSc1	Joseph
Kamel	Kamel	k1gMnSc1	Kamel
<g/>
.	.	kIx.	.
</s>
<s>
Kněz	kněz	k1gMnSc1	kněz
Prokop	Prokop	k1gMnSc1	Prokop
Diviš	Diviš	k1gMnSc1	Diviš
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
hromosvod	hromosvod	k1gInSc4	hromosvod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
se	se	k3xPyFc4	se
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
tak	tak	k9	tak
trochu	trochu	k6eAd1	trochu
náhodou	náhodou	k6eAd1	náhodou
<g/>
,	,	kIx,	,
také	také	k9	také
narodil	narodit	k5eAaPmAgMnS	narodit
Alois	Alois	k1gMnSc1	Alois
Senefelder	Senefelder	k1gMnSc1	Senefelder
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
roku	rok	k1gInSc2	rok
1796	[number]	k4	1796
vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
litografii	litografie	k1gFnSc4	litografie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozvoji	rozvoj	k1gInSc6	rozvoj
věd	věda	k1gFnPc2	věda
výrazně	výrazně	k6eAd1	výrazně
napomohlo	napomoct	k5eAaPmAgNnS	napomoct
založení	založení	k1gNnSc1	založení
Královské	královský	k2eAgFnSc2d1	královská
české	český	k2eAgFnSc2d1	Česká
společnosti	společnost	k1gFnSc2	společnost
nauk	nauka	k1gFnPc2	nauka
roku	rok	k1gInSc2	rok
1784	[number]	k4	1784
(	(	kIx(	(
<g/>
právě	právě	k9	právě
na	na	k7c4	na
její	její	k3xOp3gFnSc4	její
tradici	tradice	k1gFnSc4	tradice
dnes	dnes	k6eAd1	dnes
navazuje	navazovat	k5eAaImIp3nS	navazovat
Akademie	akademie	k1gFnSc1	akademie
věd	věda	k1gFnPc2	věda
ČR	ČR	kA	ČR
<g/>
)	)	kIx)	)
a	a	k8xC	a
Vlasteneckého	vlastenecký	k2eAgNnSc2d1	vlastenecké
muzea	muzeum	k1gNnSc2	muzeum
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
roku	rok	k1gInSc2	rok
1818	[number]	k4	1818
<g/>
.	.	kIx.	.
</s>
<s>
Výraznou	výrazný	k2eAgFnSc4d1	výrazná
roli	role	k1gFnSc4	role
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
sehráli	sehrát	k5eAaPmAgMnP	sehrát
největší	veliký	k2eAgFnSc3d3	veliký
české	český	k2eAgFnSc3d1	Česká
vědecké	vědecký	k2eAgFnSc3d1	vědecká
osobnosti	osobnost	k1gFnSc3	osobnost
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
Ignác	Ignác	k1gMnSc1	Ignác
Born	Born	k1gMnSc1	Born
a	a	k8xC	a
Kašpar	Kašpar	k1gMnSc1	Kašpar
Šternberk	Šternberk	k1gInSc1	Šternberk
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
instituce	instituce	k1gFnPc1	instituce
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
procesu	proces	k1gInSc2	proces
českého	český	k2eAgNnSc2d1	české
národního	národní	k2eAgNnSc2d1	národní
obrození	obrození	k1gNnSc2	obrození
<g/>
.	.	kIx.	.
</s>
<s>
Přírodní	přírodní	k2eAgFnSc4d1	přírodní
a	a	k8xC	a
technické	technický	k2eAgFnSc2d1	technická
vědy	věda	k1gFnSc2	věda
rozvíjeli	rozvíjet	k5eAaImAgMnP	rozvíjet
v	v	k7c6	v
éře	éra	k1gFnSc6	éra
obrození	obrození	k1gNnPc2	obrození
Jan	Jan	k1gMnSc1	Jan
Evangelista	evangelista	k1gMnSc1	evangelista
Purkyně	Purkyně	k1gFnSc1	Purkyně
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Presl	Presl	k1gInSc1	Presl
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Bořivoj	Bořivoj	k1gMnSc1	Bořivoj
Presl	Presl	k1gInSc1	Presl
či	či	k8xC	či
vynálezce	vynálezce	k1gMnSc1	vynálezce
lodního	lodní	k2eAgInSc2d1	lodní
šroubu	šroub	k1gInSc2	šroub
Josef	Josef	k1gMnSc1	Josef
Ressel	Ressel	k1gMnSc1	Ressel
<g/>
.	.	kIx.	.
</s>
<s>
František	František	k1gMnSc1	František
Josef	Josef	k1gMnSc1	Josef
Gerstner	Gerstner	k1gMnSc1	Gerstner
sestrojil	sestrojit	k5eAaPmAgMnS	sestrojit
první	první	k4xOgInSc4	první
parní	parní	k2eAgInSc4d1	parní
stroj	stroj	k1gInSc4	stroj
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
(	(	kIx(	(
<g/>
1805	[number]	k4	1805
<g/>
–	–	k?	–
<g/>
1807	[number]	k4	1807
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Božek	Božek	k1gMnSc1	Božek
předvedl	předvést	k5eAaPmAgMnS	předvést
první	první	k4xOgInSc4	první
parovůz	parovůz	k1gInSc4	parovůz
(	(	kIx(	(
<g/>
1815	[number]	k4	1815
<g/>
)	)	kIx)	)
a	a	k8xC	a
paroloď	paroloď	k1gFnSc1	paroloď
(	(	kIx(	(
<g/>
1817	[number]	k4	1817
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bratranci	bratranec	k1gMnPc1	bratranec
Veverkové	Veverková	k1gFnSc2	Veverková
vynalezli	vynaleznout	k5eAaPmAgMnP	vynaleznout
ruchadlo	ruchadlo	k1gNnSc4	ruchadlo
<g/>
.	.	kIx.	.
</s>
<s>
Německou	německý	k2eAgFnSc4d1	německá
vědu	věda	k1gFnSc4	věda
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
reprezentoval	reprezentovat	k5eAaImAgInS	reprezentovat
například	například	k6eAd1	například
Christian	Christian	k1gMnSc1	Christian
Doppler	Doppler	k1gMnSc1	Doppler
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
působil	působit	k5eAaImAgMnS	působit
i	i	k9	i
francouzský	francouzský	k2eAgMnSc1d1	francouzský
paleontolog	paleontolog	k1gMnSc1	paleontolog
Joachim	Joachim	k1gMnSc1	Joachim
Barrande	Barrand	k1gInSc5	Barrand
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
I	i	k9	i
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
přírodních	přírodní	k2eAgFnPc2d1	přírodní
věd	věda	k1gFnPc2	věda
svou	svůj	k3xOyFgFnSc4	svůj
vlast	vlast	k1gFnSc4	vlast
opustilo	opustit	k5eAaPmAgNnS	opustit
mnoho	mnoho	k4c1	mnoho
talentovaných	talentovaný	k2eAgMnPc2d1	talentovaný
rodáků	rodák	k1gMnPc2	rodák
<g/>
.	.	kIx.	.
</s>
<s>
Matematik	matematik	k1gMnSc1	matematik
Kurt	Kurt	k1gMnSc1	Kurt
Gödel	Gödel	k1gMnSc1	Gödel
<g/>
,	,	kIx,	,
biologové	biolog	k1gMnPc1	biolog
Gerty	Gert	k1gMnPc4	Gert
Coriová	Coriový	k2eAgFnSc1d1	Coriová
a	a	k8xC	a
Carl	Carl	k1gInSc4	Carl
Cori	Cor	k1gMnPc1	Cor
(	(	kIx(	(
<g/>
nositelé	nositel	k1gMnPc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
fyziologii	fyziologie	k1gFnSc4	fyziologie
a	a	k8xC	a
lékařství	lékařství	k1gNnSc4	lékařství
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
astronom	astronom	k1gMnSc1	astronom
Johann	Johann	k1gMnSc1	Johann
Palisa	Palisa	k1gFnSc1	Palisa
<g/>
,	,	kIx,	,
fyzik	fyzik	k1gMnSc1	fyzik
Georg	Georg	k1gMnSc1	Georg
Placzek	Placzek	k1gInSc1	Placzek
<g/>
,	,	kIx,	,
chemik	chemik	k1gMnSc1	chemik
Johann	Johann	k1gMnSc1	Johann
Josef	Josef	k1gMnSc1	Josef
Loschmidt	Loschmidt	k1gMnSc1	Loschmidt
<g/>
,	,	kIx,	,
průkopník	průkopník	k1gMnSc1	průkopník
půdní	půdní	k2eAgFnSc2d1	půdní
mechaniky	mechanika	k1gFnSc2	mechanika
Karl	Karl	k1gMnSc1	Karl
von	von	k1gInSc4	von
Terzaghi	Terzaghi	k1gNnSc2	Terzaghi
<g/>
,	,	kIx,	,
matematička	matematička	k1gFnSc1	matematička
Olga	Olga	k1gFnSc1	Olga
Taussky-Toddová	Taussky-Toddová	k1gFnSc1	Taussky-Toddová
<g/>
,	,	kIx,	,
botanik	botanik	k1gMnSc1	botanik
Heinrich	Heinrich	k1gMnSc1	Heinrich
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Schott	Schott	k1gMnSc1	Schott
<g/>
,	,	kIx,	,
astronomové	astronom	k1gMnPc1	astronom
Theodor	Theodora	k1gFnPc2	Theodora
von	von	k1gInSc1	von
Oppolzer	Oppolzer	k1gMnSc1	Oppolzer
a	a	k8xC	a
Joseph	Joseph	k1gMnSc1	Joseph
Johann	Johann	k1gMnSc1	Johann
von	von	k1gInSc4	von
Littrow	Littrow	k1gFnSc2	Littrow
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
dermatologie	dermatologie	k1gFnSc2	dermatologie
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
von	von	k1gInSc4	von
Hebra	Hebro	k1gNnSc2	Hebro
<g/>
,	,	kIx,	,
chemik	chemik	k1gMnSc1	chemik
Hans	Hans	k1gMnSc1	Hans
Tropsch	Tropsch	k1gMnSc1	Tropsch
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
odešel	odejít	k5eAaPmAgMnS	odejít
i	i	k9	i
český	český	k2eAgMnSc1d1	český
lékař	lékař	k1gMnSc1	lékař
Karel	Karel	k1gMnSc1	Karel
Rokytanský	Rokytanský	k2eAgMnSc1d1	Rokytanský
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
německy	německy	k6eAd1	německy
mluvících	mluvící	k2eAgMnPc2d1	mluvící
vědců	vědec	k1gMnPc2	vědec
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
naopak	naopak	k6eAd1	naopak
zůstali	zůstat	k5eAaPmAgMnP	zůstat
pracovat	pracovat	k5eAaImF	pracovat
světově	světově	k6eAd1	světově
významný	významný	k2eAgMnSc1d1	významný
biolog	biolog	k1gMnSc1	biolog
Gregor	Gregor	k1gMnSc1	Gregor
Mendel	Mendel	k1gMnSc1	Mendel
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
genetiky	genetika	k1gFnSc2	genetika
<g/>
,	,	kIx,	,
či	či	k8xC	či
fyzik	fyzik	k1gMnSc1	fyzik
Ernst	Ernst	k1gMnSc1	Ernst
Mach	Mach	k1gMnSc1	Mach
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
univerzitě	univerzita	k1gFnSc6	univerzita
krátký	krátký	k2eAgInSc4d1	krátký
čas	čas	k1gInSc4	čas
působil	působit	k5eAaImAgMnS	působit
i	i	k9	i
fyzik	fyzik	k1gMnSc1	fyzik
Albert	Albert	k1gMnSc1	Albert
Einstein	Einstein	k1gMnSc1	Einstein
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ani	ani	k8xC	ani
česká	český	k2eAgFnSc1d1	Česká
věda	věda	k1gFnSc1	věda
však	však	k9	však
nestála	stát	k5eNaImAgFnS	stát
v	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
stranou	stranou	k6eAd1	stranou
<g/>
.	.	kIx.	.
</s>
<s>
Rozvíjela	rozvíjet	k5eAaImAgFnS	rozvíjet
se	se	k3xPyFc4	se
v	v	k7c6	v
těsném	těsný	k2eAgNnSc6d1	těsné
sepětí	sepětí	k1gNnSc6	sepětí
s	s	k7c7	s
průmyslem	průmysl	k1gInSc7	průmysl
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
rychle	rychle	k6eAd1	rychle
mohutněl	mohutnět	k5eAaImAgInS	mohutnět
<g/>
.	.	kIx.	.
</s>
<s>
Jejími	její	k3xOp3gMnPc7	její
klíčovými	klíčový	k2eAgMnPc7d1	klíčový
představiteli	představitel	k1gMnPc7	představitel
byli	být	k5eAaImAgMnP	být
vynálezce	vynálezce	k1gMnSc4	vynálezce
obloukové	obloukový	k2eAgFnSc2d1	oblouková
lampy	lampa	k1gFnSc2	lampa
František	František	k1gMnSc1	František
Křižík	Křižík	k1gMnSc1	Křižík
či	či	k8xC	či
objevitel	objevitel	k1gMnSc1	objevitel
čtyř	čtyři	k4xCgFnPc2	čtyři
krevních	krevní	k2eAgFnPc2d1	krevní
skupin	skupina	k1gFnPc2	skupina
Jan	Jan	k1gMnSc1	Jan
Janský	janský	k2eAgInSc1d1	janský
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
dějin	dějiny	k1gFnPc2	dějiny
fotografické	fotografický	k2eAgFnSc2d1	fotografická
a	a	k8xC	a
polygrafické	polygrafický	k2eAgFnSc2d1	polygrafická
techniky	technika	k1gFnSc2	technika
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
výrazně	výrazně	k6eAd1	výrazně
zasáhly	zasáhnout	k5eAaPmAgInP	zasáhnout
Jakub	Jakub	k1gMnSc1	Jakub
Husník	Husník	k1gMnSc1	Husník
a	a	k8xC	a
Karel	Karel	k1gMnSc1	Karel
Klíč	klíč	k1gInSc1	klíč
<g/>
.	.	kIx.	.
</s>
<s>
Průkopníkem	průkopník	k1gMnSc7	průkopník
aviatiky	aviatika	k1gFnSc2	aviatika
byl	být	k5eAaImAgMnS	být
Jan	Jan	k1gMnSc1	Jan
Kašpar	Kašpar	k1gMnSc1	Kašpar
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Kříženecký	kříženecký	k2eAgMnSc1d1	kříženecký
roku	rok	k1gInSc2	rok
1898	[number]	k4	1898
představil	představit	k5eAaPmAgInS	představit
první	první	k4xOgInSc4	první
kinematograf	kinematograf	k1gInSc4	kinematograf
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prostor	prostor	k1gInSc1	prostor
pro	pro	k7c4	pro
vědu	věda	k1gFnSc4	věda
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
i	i	k9	i
nová	nový	k2eAgFnSc1d1	nová
Československá	československý	k2eAgFnSc1d1	Československá
republika	republika	k1gFnSc1	republika
založená	založený	k2eAgFnSc1d1	založená
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
byla	být	k5eAaImAgNnP	být
tehdy	tehdy	k6eAd1	tehdy
(	(	kIx(	(
<g/>
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
založena	založen	k2eAgFnSc1d1	založena
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
topologie	topologie	k1gFnSc2	topologie
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
prosadil	prosadit	k5eAaPmAgMnS	prosadit
matematik	matematik	k1gMnSc1	matematik
Eduard	Eduard	k1gMnSc1	Eduard
Čech	Čech	k1gMnSc1	Čech
<g/>
.	.	kIx.	.
</s>
<s>
Botanik	botanik	k1gMnSc1	botanik
Alberto	Alberta	k1gFnSc5	Alberta
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Frič	Frič	k1gMnSc1	Frič
objevil	objevit	k5eAaPmAgMnS	objevit
řadu	řada	k1gFnSc4	řada
nových	nový	k2eAgInPc2d1	nový
kaktusů	kaktus	k1gInPc2	kaktus
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgMnSc7d1	další
významným	významný	k2eAgMnSc7d1	významný
botanikem	botanik	k1gMnSc7	botanik
byl	být	k5eAaImAgMnS	být
Karel	Karel	k1gMnSc1	Karel
Domin	domino	k1gNnPc2	domino
<g/>
.	.	kIx.	.
</s>
<s>
Technik	technik	k1gMnSc1	technik
Viktor	Viktor	k1gMnSc1	Viktor
Kaplan	Kaplan	k1gMnSc1	Kaplan
vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
nový	nový	k2eAgInSc4d1	nový
druh	druh	k1gInSc4	druh
turbíny	turbína	k1gFnSc2	turbína
<g/>
.	.	kIx.	.
</s>
<s>
Hans	Hans	k1gMnSc1	Hans
Ledwinka	Ledwinka	k1gFnSc1	Ledwinka
zkonstruoval	zkonstruovat	k5eAaPmAgInS	zkonstruovat
roku	rok	k1gInSc2	rok
1934	[number]	k4	1934
první	první	k4xOgInSc4	první
sériově	sériově	k6eAd1	sériově
vyráběný	vyráběný	k2eAgInSc4d1	vyráběný
automobil	automobil	k1gInSc4	automobil
s	s	k7c7	s
aerodynamickou	aerodynamický	k2eAgFnSc7d1	aerodynamická
karosérií	karosérie	k1gFnSc7	karosérie
Tatra	Tatra	k1gFnSc1	Tatra
77	[number]	k4	77
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgMnSc1d1	další
automobilový	automobilový	k2eAgMnSc1d1	automobilový
konstruktér	konstruktér	k1gMnSc1	konstruktér
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Porsche	Porsche	k1gNnSc2	Porsche
se	se	k3xPyFc4	se
také	také	k9	také
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
prosadil	prosadit	k5eAaPmAgMnS	prosadit
se	s	k7c7	s
zejména	zejména	k9	zejména
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
a	a	k8xC	a
jako	jako	k9	jako
spolupracovník	spolupracovník	k1gMnSc1	spolupracovník
s	s	k7c7	s
nacisty	nacista	k1gMnPc7	nacista
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
do	do	k7c2	do
republiky	republika	k1gFnSc2	republika
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
ani	ani	k8xC	ani
vrátit	vrátit	k5eAaPmF	vrátit
nemohl	moct	k5eNaImAgMnS	moct
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
Němci	Němec	k1gMnPc7	Němec
odsunutými	odsunutý	k2eAgMnPc7d1	odsunutý
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
byl	být	k5eAaImAgMnS	být
i	i	k9	i
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2007	[number]	k4	2007
Peter	Peter	k1gMnSc1	Peter
Grünberg	Grünberg	k1gMnSc1	Grünberg
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
bylo	být	k5eAaImAgNnS	být
největším	veliký	k2eAgInSc7d3	veliký
úspěchem	úspěch	k1gInSc7	úspěch
české	český	k2eAgFnSc2d1	Česká
vědy	věda	k1gFnSc2	věda
udělení	udělení	k1gNnSc4	udělení
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
chemii	chemie	k1gFnSc4	chemie
Jaroslavu	Jaroslava	k1gFnSc4	Jaroslava
Heyrovskému	Heyrovský	k2eAgInSc3d1	Heyrovský
roku	rok	k1gInSc3	rok
1959	[number]	k4	1959
<g/>
,	,	kIx,	,
za	za	k7c4	za
objev	objev	k1gInSc4	objev
polarografie	polarografie	k1gFnSc2	polarografie
a	a	k8xC	a
analytickou	analytický	k2eAgFnSc4d1	analytická
chemii	chemie	k1gFnSc4	chemie
<g/>
.	.	kIx.	.
</s>
<s>
Vynálezem	vynález	k1gInSc7	vynález
kontaktních	kontaktní	k2eAgFnPc2d1	kontaktní
čoček	čočka	k1gFnPc2	čočka
a	a	k8xC	a
silonu	silon	k1gInSc2	silon
proslul	proslout	k5eAaPmAgMnS	proslout
Otto	Otto	k1gMnSc1	Otto
Wichterle	Wichterle	k1gFnSc2	Wichterle
<g/>
.	.	kIx.	.
</s>
<s>
Oldřich	Oldřich	k1gMnSc1	Oldřich
Homuta	Homut	k1gMnSc4	Homut
vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
roku	rok	k1gInSc2	rok
1957	[number]	k4	1957
praktickou	praktický	k2eAgFnSc4d1	praktická
pomůcku	pomůcka	k1gFnSc4	pomůcka
do	do	k7c2	do
domácnosti	domácnost	k1gFnSc2	domácnost
<g/>
:	:	kIx,	:
remosku	remoska	k1gFnSc4	remoska
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgMnSc7d1	významný
astronomem	astronom	k1gMnSc7	astronom
byl	být	k5eAaImAgMnS	být
Antonín	Antonín	k1gMnSc1	Antonín
Mrkos	Mrkos	k1gMnSc1	Mrkos
působící	působící	k2eAgMnSc1d1	působící
na	na	k7c6	na
Hvězdárně	hvězdárna	k1gFnSc6	hvězdárna
Kleť	Kleť	k1gFnSc4	Kleť
<g/>
.	.	kIx.	.
</s>
<s>
Armin	Armin	k2eAgInSc1d1	Armin
Delong	Delong	k1gInSc1	Delong
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
mimořádných	mimořádný	k2eAgInPc2d1	mimořádný
výsledků	výsledek	k1gInPc2	výsledek
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
mikroskopie	mikroskopie	k1gFnSc2	mikroskopie
<g/>
.	.	kIx.	.
</s>
<s>
Stanislav	Stanislav	k1gMnSc1	Stanislav
Brebera	Brebero	k1gNnSc2	Brebero
vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
trhavinu	trhavina	k1gFnSc4	trhavina
Semtex	semtex	k1gInSc1	semtex
<g/>
.	.	kIx.	.
</s>
<s>
Výkladní	výkladní	k2eAgFnSc7d1	výkladní
skříní	skříň	k1gFnSc7	skříň
režimu	režim	k1gInSc2	režim
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
účast	účast	k1gFnSc4	účast
na	na	k7c6	na
sovětském	sovětský	k2eAgInSc6d1	sovětský
vesmírném	vesmírný	k2eAgInSc6d1	vesmírný
programu	program	k1gInSc6	program
Interkosmos	Interkosmos	k1gMnSc1	Interkosmos
(	(	kIx(	(
<g/>
čs	čs	kA	čs
<g/>
.	.	kIx.	.
družice	družice	k1gFnSc1	družice
Magion	Magion	k1gInSc1	Magion
<g/>
,	,	kIx,	,
první	první	k4xOgInSc1	první
československý	československý	k2eAgMnSc1d1	československý
kosmonaut	kosmonaut	k1gMnSc1	kosmonaut
Vladimír	Vladimír	k1gMnSc1	Vladimír
Remek	Remek	k1gMnSc1	Remek
–	–	k?	–
první	první	k4xOgMnSc1	první
člověk	člověk	k1gMnSc1	člověk
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
nebyl	být	k5eNaImAgInS	být
občanem	občan	k1gMnSc7	občan
USA	USA	kA	USA
či	či	k8xC	či
SSSR	SSSR	kA	SSSR
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Symbolem	symbol	k1gInSc7	symbol
porevoluční	porevoluční	k2eAgFnSc2d1	porevoluční
vědy	věda	k1gFnSc2	věda
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
chemik	chemik	k1gMnSc1	chemik
Antonín	Antonín	k1gMnSc1	Antonín
Holý	Holý	k1gMnSc1	Holý
<g/>
,	,	kIx,	,
tvůrce	tvůrce	k1gMnSc1	tvůrce
poměrně	poměrně	k6eAd1	poměrně
účinných	účinný	k2eAgInPc2d1	účinný
léků	lék	k1gInPc2	lék
proti	proti	k7c3	proti
AIDS	AIDS	kA	AIDS
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
Evropskou	evropský	k2eAgFnSc7d1	Evropská
unií	unie	k1gFnSc7	unie
bylo	být	k5eAaImAgNnS	být
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
několik	několik	k4yIc1	několik
nových	nový	k2eAgNnPc2d1	nové
vědeckých	vědecký	k2eAgNnPc2d1	vědecké
center	centrum	k1gNnPc2	centrum
zaměřených	zaměřený	k2eAgNnPc2d1	zaměřené
mj.	mj.	kA	mj.
na	na	k7c4	na
nanotechnologie	nanotechnologie	k1gFnPc4	nanotechnologie
a	a	k8xC	a
laserovou	laserový	k2eAgFnSc4d1	laserová
techniku	technika	k1gFnSc4	technika
(	(	kIx(	(
<g/>
CEITEC	CEITEC	kA	CEITEC
<g/>
,	,	kIx,	,
ELI	ELI	kA	ELI
Beamlines	Beamlines	k1gMnSc1	Beamlines
<g/>
,	,	kIx,	,
HiLASE	HiLASE	k1gMnSc1	HiLASE
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Antarktidě	Antarktida	k1gFnSc6	Antarktida
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
česká	český	k2eAgFnSc1d1	Česká
Mendelova	Mendelův	k2eAgFnSc1d1	Mendelova
polární	polární	k2eAgFnSc1d1	polární
stanice	stanice	k1gFnSc1	stanice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Školství	školství	k1gNnSc2	školství
===	===	k?	===
</s>
</p>
<p>
<s>
Školství	školství	k1gNnSc1	školství
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
je	být	k5eAaImIp3nS	být
organizováno	organizovat	k5eAaBmNgNnS	organizovat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
základních	základní	k2eAgFnPc2d1	základní
<g/>
,	,	kIx,	,
středních	střední	k2eAgFnPc2d1	střední
<g/>
,	,	kIx,	,
vyšších	vysoký	k2eAgFnPc2d2	vyšší
odborných	odborný	k2eAgFnPc2d1	odborná
a	a	k8xC	a
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vysoké	vysoký	k2eAgFnPc1d1	vysoká
školy	škola	k1gFnPc1	škola
jsou	být	k5eAaImIp3nP	být
veřejné	veřejný	k2eAgFnPc1d1	veřejná
<g/>
,	,	kIx,	,
státní	státní	k2eAgFnPc1d1	státní
a	a	k8xC	a
soukromé	soukromý	k2eAgFnPc1d1	soukromá
<g/>
,	,	kIx,	,
nejstarší	starý	k2eAgFnSc1d3	nejstarší
a	a	k8xC	a
nejvýznamnější	významný	k2eAgFnSc1d3	nejvýznamnější
je	být	k5eAaImIp3nS	být
Univerzita	univerzita	k1gFnSc1	univerzita
Karlova	Karlův	k2eAgFnSc1d1	Karlova
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
nejstarším	starý	k2eAgNnSc7d3	nejstarší
vysokým	vysoký	k2eAgNnSc7d1	vysoké
učením	učení	k1gNnSc7	učení
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
(	(	kIx(	(
<g/>
založena	založen	k2eAgFnSc1d1	založena
1348	[number]	k4	1348
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc7	druhý
nejstarší	starý	k2eAgFnSc7d3	nejstarší
univerzitou	univerzita	k1gFnSc7	univerzita
v	v	k7c6	v
ČR	ČR	kA	ČR
je	být	k5eAaImIp3nS	být
Univerzita	univerzita	k1gFnSc1	univerzita
Palackého	Palacký	k1gMnSc2	Palacký
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
roku	rok	k1gInSc2	rok
1573	[number]	k4	1573
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc7d3	nejstarší
vysokou	vysoký	k2eAgFnSc7d1	vysoká
školou	škola	k1gFnSc7	škola
uměleckého	umělecký	k2eAgInSc2d1	umělecký
typu	typ	k1gInSc2	typ
je	být	k5eAaImIp3nS	být
Akademie	akademie	k1gFnSc1	akademie
výtvarných	výtvarný	k2eAgNnPc2d1	výtvarné
umění	umění	k1gNnPc2	umění
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
svou	svůj	k3xOyFgFnSc4	svůj
historii	historie	k1gFnSc4	historie
píše	psát	k5eAaImIp3nS	psát
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1799	[number]	k4	1799
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
významnými	významný	k2eAgFnPc7d1	významná
vysokými	vysoký	k2eAgFnPc7d1	vysoká
školami	škola	k1gFnPc7	škola
jsou	být	k5eAaImIp3nP	být
České	český	k2eAgFnSc3d1	Česká
vysoké	vysoká	k1gFnSc3	vysoká
učení	učení	k1gNnSc2	učení
technické	technický	k2eAgInPc4d1	technický
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
(	(	kIx(	(
<g/>
založeno	založit	k5eAaPmNgNnS	založit
1707	[number]	k4	1707
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vysoké	vysoký	k2eAgNnSc1d1	vysoké
učení	učení	k1gNnSc1	učení
technické	technický	k2eAgNnSc1d1	technické
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
(	(	kIx(	(
<g/>
založeno	založit	k5eAaPmNgNnS	založit
1899	[number]	k4	1899
<g/>
)	)	kIx)	)
či	či	k8xC	či
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
univerzita	univerzita	k1gFnSc1	univerzita
(	(	kIx(	(
<g/>
založena	založen	k2eAgFnSc1d1	založena
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
Akademie	akademie	k1gFnSc1	akademie
múzických	múzický	k2eAgNnPc2d1	múzické
umění	umění	k1gNnPc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
její	její	k3xOp3gFnPc4	její
součásti	součást	k1gFnPc4	součást
FAMU	FAMU	kA	FAMU
vystudovala	vystudovat	k5eAaPmAgFnS	vystudovat
nejen	nejen	k6eAd1	nejen
řada	řada	k1gFnSc1	řada
osobností	osobnost	k1gFnPc2	osobnost
českého	český	k2eAgInSc2d1	český
a	a	k8xC	a
slovenského	slovenský	k2eAgInSc2d1	slovenský
filmu	film	k1gInSc2	film
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
známí	známý	k2eAgMnPc1d1	známý
zahraniční	zahraniční	k2eAgMnPc1d1	zahraniční
filmaři	filmař	k1gMnPc1	filmař
Agnieszka	Agnieszka	k1gFnSc1	Agnieszka
Holland	Holland	k1gInSc1	Holland
<g/>
,	,	kIx,	,
Emir	Emir	k1gMnSc1	Emir
Kusturica	Kusturica	k1gMnSc1	Kusturica
<g/>
,	,	kIx,	,
Lordan	Lordan	k1gMnSc1	Lordan
Zafranović	Zafranović	k1gMnSc1	Zafranović
či	či	k8xC	či
Goran	Goran	k1gMnSc1	Goran
Paskaljević	Paskaljević	k1gMnSc1	Paskaljević
<g/>
.	.	kIx.	.
</s>
<s>
Největšími	veliký	k2eAgMnPc7d3	veliký
co	co	k3yRnSc4	co
do	do	k7c2	do
počtu	počet	k1gInSc2	počet
studentů	student	k1gMnPc2	student
jsou	být	k5eAaImIp3nP	být
Univerzita	univerzita	k1gFnSc1	univerzita
Karlova	Karlův	k2eAgFnSc1d1	Karlova
(	(	kIx(	(
<g/>
49	[number]	k4	49
094	[number]	k4	094
studentů	student	k1gMnPc2	student
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
a	a	k8xC	a
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
univerzita	univerzita	k1gFnSc1	univerzita
(	(	kIx(	(
<g/>
38	[number]	k4	38
216	[number]	k4	216
studentů	student	k1gMnPc2	student
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
žebříčku	žebříček	k1gInSc2	žebříček
QS	QS	kA	QS
World	World	k1gMnSc1	World
University	universita	k1gFnSc2	universita
Rankings	Rankingsa	k1gFnPc2	Rankingsa
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
je	být	k5eAaImIp3nS	být
Univerzita	univerzita	k1gFnSc1	univerzita
Karlova	Karlův	k2eAgFnSc1d1	Karlova
244	[number]	k4	244
<g/>
.	.	kIx.	.
nejlepší	dobrý	k2eAgNnSc1d3	nejlepší
vysokou	vysoký	k2eAgFnSc7d1	vysoká
školou	škola	k1gFnSc7	škola
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgNnSc1d1	české
vysoké	vysoký	k2eAgNnSc1d1	vysoké
učení	učení	k1gNnSc1	učení
technické	technický	k2eAgNnSc1d1	technické
se	se	k3xPyFc4	se
umístilo	umístit	k5eAaPmAgNnS	umístit
na	na	k7c4	na
411	[number]	k4	411
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
420	[number]	k4	420
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
se	se	k3xPyFc4	se
umístila	umístit	k5eAaPmAgFnS	umístit
na	na	k7c4	na
551	[number]	k4	551
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
600	[number]	k4	600
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
Vysoké	vysoká	k1gFnSc3	vysoká
učení	učení	k1gNnSc2	učení
technické	technický	k2eAgInPc4d1	technický
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
na	na	k7c4	na
651	[number]	k4	651
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
700	[number]	k4	700
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
škola	škola	k1gFnSc1	škola
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
byla	být	k5eAaImAgFnS	být
zařazena	zařadit	k5eAaPmNgFnS	zařadit
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
701	[number]	k4	701
<g/>
+	+	kIx~	+
(	(	kIx(	(
<g/>
pod	pod	k7c7	pod
700	[number]	k4	700
<g/>
.	.	kIx.	.
místem	místo	k1gNnSc7	místo
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
dalšího	další	k2eAgNnSc2d1	další
rozlišování	rozlišování	k1gNnSc2	rozlišování
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiné	k1gNnSc1	jiné
české	český	k2eAgFnSc2d1	Česká
vysoké	vysoký	k2eAgFnSc2d1	vysoká
školy	škola	k1gFnSc2	škola
žebříček	žebříček	k1gInSc1	žebříček
QS	QS	kA	QS
do	do	k7c2	do
měření	měření	k1gNnSc2	měření
nezahrnuje	zahrnovat	k5eNaImIp3nS	zahrnovat
<g/>
.	.	kIx.	.
<g/>
Nejznámějším	známý	k2eAgInSc7d3	nejznámější
systémem	systém	k1gInSc7	systém
měření	měření	k1gNnSc1	měření
a	a	k8xC	a
srovnávání	srovnávání	k1gNnSc1	srovnávání
kvality	kvalita	k1gFnSc2	kvalita
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
šanghajský	šanghajský	k2eAgInSc1d1	šanghajský
žebříček	žebříček	k1gInSc1	žebříček
(	(	kIx(	(
<g/>
ARWU	ARWU	kA	ARWU
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
každoročně	každoročně	k6eAd1	každoročně
určující	určující	k2eAgNnSc4d1	určující
500	[number]	k4	500
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Jedinou	jediný	k2eAgFnSc7d1	jediná
českou	český	k2eAgFnSc7d1	Česká
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
dosud	dosud	k6eAd1	dosud
objevila	objevit	k5eAaPmAgFnS	objevit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Univerzita	univerzita	k1gFnSc1	univerzita
Karlova	Karlův	k2eAgFnSc1d1	Karlova
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
se	se	k3xPyFc4	se
umístila	umístit	k5eAaPmAgFnS	umístit
na	na	k7c4	na
201	[number]	k4	201
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
300	[number]	k4	300
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
ARWU	ARWU	kA	ARWU
tak	tak	k9	tak
Univerzita	univerzita	k1gFnSc1	univerzita
Karlova	Karlův	k2eAgFnSc1d1	Karlova
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
2	[number]	k4	2
procenta	procento	k1gNnSc2	procento
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
<g/>
Existují	existovat	k5eAaImIp3nP	existovat
také	také	k9	také
méně	málo	k6eAd2	málo
formalizovaná	formalizovaný	k2eAgNnPc4d1	formalizované
srovnání	srovnání	k1gNnPc4	srovnání
spíše	spíše	k9	spíše
publicistického	publicistický	k2eAgInSc2d1	publicistický
typu	typ	k1gInSc2	typ
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
čas	čas	k1gInSc4	čas
od	od	k7c2	od
času	čas	k1gInSc2	čas
za	za	k7c4	za
nejkvalitnější	kvalitní	k2eAgFnSc4d3	nejkvalitnější
univerzitu	univerzita	k1gFnSc4	univerzita
v	v	k7c6	v
ČR	ČR	kA	ČR
označí	označit	k5eAaPmIp3nS	označit
třeba	třeba	k6eAd1	třeba
brněnskou	brněnský	k2eAgFnSc4d1	brněnská
Masarykovu	Masarykův	k2eAgFnSc4d1	Masarykova
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
činí	činit	k5eAaImIp3nS	činit
podíl	podíl	k1gInSc4	podíl
<g />
.	.	kIx.	.
</s>
<s>
vysokoškolsky	vysokoškolsky	k6eAd1	vysokoškolsky
vzdělaného	vzdělaný	k2eAgNnSc2d1	vzdělané
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
celkem	celkem	k6eAd1	celkem
10,7	[number]	k4	10,7
%	%	kIx~	%
<g/>
;	;	kIx,	;
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
podíl	podíl	k1gInSc1	podíl
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
Praha	Praha	k1gFnSc1	Praha
(	(	kIx(	(
<g/>
20,7	[number]	k4	20,7
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
(	(	kIx(	(
<g/>
20,6	[number]	k4	20,6
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
Olomouc	Olomouc	k1gFnSc1	Olomouc
(	(	kIx(	(
<g/>
17,9	[number]	k4	17,9
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
K	k	k7c3	k
nejslavnějším	slavný	k2eAgFnPc3d3	nejslavnější
středním	střední	k2eAgFnPc3d1	střední
školám	škola	k1gFnPc3	škola
patří	patřit	k5eAaImIp3nS	patřit
Pražská	pražský	k2eAgFnSc1d1	Pražská
konzervatoř	konzervatoř	k1gFnSc1	konzervatoř
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
škola	škola	k1gFnSc1	škola
svého	svůj	k3xOyFgInSc2	svůj
druhu	druh	k1gInSc2	druh
na	na	k7c6	na
světě	svět	k1gInSc6	svět
(	(	kIx(	(
<g/>
založena	založen	k2eAgFnSc1d1	založena
1808	[number]	k4	1808
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Školu	škola	k1gFnSc4	škola
vedli	vést	k5eAaImAgMnP	vést
i	i	k9	i
Antonín	Antonín	k1gMnSc1	Antonín
Dvořák	Dvořák	k1gMnSc1	Dvořák
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Suk	Suk	k1gMnSc1	Suk
<g/>
,	,	kIx,	,
Vítězslav	Vítězslav	k1gMnSc1	Vítězslav
Novák	Novák	k1gMnSc1	Novák
či	či	k8xC	či
Josef	Josef	k1gMnSc1	Josef
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Foerster	Foerster	k1gMnSc1	Foerster
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
studie	studie	k1gFnSc2	studie
společnosti	společnost	k1gFnSc2	společnost
Pearson	Pearsona	k1gFnPc2	Pearsona
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
má	mít	k5eAaImIp3nS	mít
ČR	ČR	kA	ČR
22	[number]	k4	22
<g/>
.	.	kIx.	.
nejkvalitnější	kvalitní	k2eAgNnSc1d3	nejkvalitnější
školství	školství	k1gNnSc1	školství
mezi	mezi	k7c7	mezi
vyspělými	vyspělý	k2eAgFnPc7d1	vyspělá
zeměmi	zem	k1gFnPc7	zem
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
informací	informace	k1gFnPc2	informace
OECD	OECD	kA	OECD
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
Česko	Česko	k1gNnSc1	Česko
směřuje	směřovat	k5eAaImIp3nS	směřovat
do	do	k7c2	do
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
10	[number]	k4	10
procent	procento	k1gNnPc2	procento
veřejných	veřejný	k2eAgInPc2d1	veřejný
výdajů	výdaj	k1gInPc2	výdaj
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
v	v	k7c6	v
srovnávacím	srovnávací	k2eAgInSc6d1	srovnávací
žebříčku	žebříček	k1gInSc6	žebříček
řadí	řadit	k5eAaImIp3nS	řadit
na	na	k7c4	na
předposlední	předposlední	k2eAgNnSc4d1	předposlední
místo	místo	k1gNnSc4	místo
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Japonskem	Japonsko	k1gNnSc7	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Vyjádřeno	vyjádřen	k2eAgNnSc1d1	vyjádřeno
podílem	podíl	k1gInSc7	podíl
HDP	HDP	kA	HDP
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
asi	asi	k9	asi
4,6	[number]	k4	4,6
%	%	kIx~	%
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
podstatně	podstatně	k6eAd1	podstatně
méně	málo	k6eAd2	málo
než	než	k8xS	než
průměr	průměr	k1gInSc1	průměr
zemí	zem	k1gFnPc2	zem
OECD	OECD	kA	OECD
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
činí	činit	k5eAaImIp3nS	činit
6,1	[number]	k4	6,1
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
čeští	český	k2eAgMnPc1d1	český
pedagogové	pedagog	k1gMnPc1	pedagog
dostávají	dostávat	k5eAaImIp3nP	dostávat
čtvrté	čtvrtý	k4xOgInPc1	čtvrtý
nejnižší	nízký	k2eAgInPc1d3	nejnižší
platy	plat	k1gInPc1	plat
z	z	k7c2	z
více	hodně	k6eAd2	hodně
než	než	k8xS	než
třiceti	třicet	k4xCc2	třicet
zemí	zem	k1gFnPc2	zem
sledovaných	sledovaný	k2eAgMnPc2d1	sledovaný
OECD	OECD	kA	OECD
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sport	sport	k1gInSc1	sport
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
statistiky	statistika	k1gFnSc2	statistika
České	český	k2eAgFnSc2d1	Česká
unie	unie	k1gFnSc2	unie
sportu	sport	k1gInSc2	sport
jsou	být	k5eAaImIp3nP	být
nejpopulárnějšími	populární	k2eAgInPc7d3	nejpopulárnější
sporty	sport	k1gInPc7	sport
v	v	k7c6	v
ČR	ČR	kA	ČR
podle	podle	k7c2	podle
velikosti	velikost	k1gFnSc2	velikost
členské	členský	k2eAgFnSc2d1	členská
základny	základna	k1gFnSc2	základna
sportovních	sportovní	k2eAgInPc2d1	sportovní
oddílů	oddíl	k1gInPc2	oddíl
<g/>
:	:	kIx,	:
fotbal	fotbal	k1gInSc1	fotbal
<g/>
,	,	kIx,	,
tenis	tenis	k1gInSc1	tenis
<g/>
,	,	kIx,	,
lední	lední	k2eAgInSc1d1	lední
hokej	hokej	k1gInSc1	hokej
<g/>
,	,	kIx,	,
volejbal	volejbal	k1gInSc1	volejbal
<g/>
,	,	kIx,	,
florbal	florbal	k1gInSc1	florbal
<g/>
,	,	kIx,	,
golf	golf	k1gInSc1	golf
<g/>
,	,	kIx,	,
hokejbal	hokejbal	k1gInSc1	hokejbal
<g/>
,	,	kIx,	,
atletika	atletika	k1gFnSc1	atletika
<g/>
,	,	kIx,	,
basketbal	basketbal	k1gInSc1	basketbal
a	a	k8xC	a
lyžování	lyžování	k1gNnSc1	lyžování
<g/>
.	.	kIx.	.
</s>
<s>
Pořadí	pořadí	k1gNnSc1	pořadí
sportovních	sportovní	k2eAgFnPc2d1	sportovní
disciplín	disciplína	k1gFnPc2	disciplína
podle	podle	k7c2	podle
sledovanosti	sledovanost	k1gFnSc2	sledovanost
obecenstvem	obecenstvo	k1gNnSc7	obecenstvo
je	být	k5eAaImIp3nS	být
následující	následující	k2eAgInSc1d1	následující
<g/>
:	:	kIx,	:
lední	lední	k2eAgInSc1d1	lední
hokej	hokej	k1gInSc1	hokej
<g/>
,	,	kIx,	,
biatlon	biatlon	k1gInSc1	biatlon
<g/>
,	,	kIx,	,
fotbal	fotbal	k1gInSc1	fotbal
<g/>
,	,	kIx,	,
lyžování	lyžování	k1gNnSc4	lyžování
<g/>
,	,	kIx,	,
tenis	tenis	k1gInSc4	tenis
<g/>
,	,	kIx,	,
florbal	florbal	k1gInSc4	florbal
<g/>
,	,	kIx,	,
basketbal	basketbal	k1gInSc4	basketbal
a	a	k8xC	a
volejbal	volejbal	k1gInSc4	volejbal
<g/>
.	.	kIx.	.
</s>
<s>
Nejsledovanějšími	sledovaný	k2eAgFnPc7d3	nejsledovanější
sportovními	sportovní	k2eAgFnPc7d1	sportovní
událostmi	událost	k1gFnPc7	událost
jsou	být	k5eAaImIp3nP	být
lední	lední	k2eAgInSc4d1	lední
hokej	hokej	k1gInSc4	hokej
na	na	k7c6	na
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
a	a	k8xC	a
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
ledním	lední	k2eAgInSc6d1	lední
hokeji	hokej	k1gInSc6	hokej
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgNnSc4d1	další
nejsledovanější	sledovaný	k2eAgNnSc4d3	nejsledovanější
patří	patřit	k5eAaImIp3nS	patřit
mistrovství	mistrovství	k1gNnSc4	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
<g/>
,	,	kIx,	,
Liga	liga	k1gFnSc1	liga
mistrů	mistr	k1gMnPc2	mistr
UEFA	UEFA	kA	UEFA
a	a	k8xC	a
mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Olympijské	olympijský	k2eAgFnPc1d1	olympijská
hry	hra	k1gFnPc1	hra
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
čeští	český	k2eAgMnPc1d1	český
sportovci	sportovec	k1gMnPc1	sportovec
významných	významný	k2eAgInPc2d1	významný
úspěchů	úspěch	k1gInPc2	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1900	[number]	k4	1900
se	se	k3xPyFc4	se
František	František	k1gMnSc1	František
Janda-Suk	Janda-Suk	k1gMnSc1	Janda-Suk
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
2	[number]	k4	2
<g/>
.	.	kIx.	.
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
získal	získat	k5eAaPmAgMnS	získat
stříbrnou	stříbrný	k2eAgFnSc4d1	stříbrná
medaili	medaile	k1gFnSc4	medaile
v	v	k7c6	v
hodu	hod	k1gInSc6	hod
diskem	disk	k1gInSc7	disk
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
letních	letní	k2eAgFnPc6d1	letní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
získala	získat	k5eAaPmAgFnS	získat
gymnastka	gymnastka	k1gFnSc1	gymnastka
Věra	Věra	k1gFnSc1	Věra
Čáslavská	Čáslavská	k1gFnSc1	Čáslavská
sedm	sedm	k4xCc4	sedm
zlatých	zlatá	k1gFnPc2	zlatá
olympijských	olympijský	k2eAgFnPc2d1	olympijská
medailí	medaile	k1gFnPc2	medaile
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
×	×	k?	×
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
×	×	k?	×
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Legendární	legendární	k2eAgMnSc1d1	legendární
běžec	běžec	k1gMnSc1	běžec
Emil	Emil	k1gMnSc1	Emil
Zátopek	Zátopek	k1gInSc4	Zátopek
obdržel	obdržet	k5eAaPmAgMnS	obdržet
čtyři	čtyři	k4xCgFnPc4	čtyři
zlaté	zlatá	k1gFnPc4	zlatá
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
×	×	k?	×
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
×	×	k?	×
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tři	tři	k4xCgInPc4	tři
zlaté	zlatý	k1gInPc4	zlatý
má	mít	k5eAaImIp3nS	mít
oštěpař	oštěpař	k1gMnSc1	oštěpař
Jan	Jan	k1gMnSc1	Jan
Železný	Železný	k1gMnSc1	Železný
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
Dvě	dva	k4xCgFnPc1	dva
zlaté	zlatá	k1gFnPc1	zlatá
získali	získat	k5eAaPmAgMnP	získat
rychlostní	rychlostní	k2eAgMnPc1d1	rychlostní
kanoisté	kanoista	k1gMnPc1	kanoista
Josef	Josef	k1gMnSc1	Josef
Holeček	Holeček	k1gMnSc1	Holeček
(	(	kIx(	(
<g/>
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
a	a	k8xC	a
Martin	Martin	k1gMnSc1	Martin
Doktor	doktor	k1gMnSc1	doktor
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kanoistka	kanoistka	k1gFnSc1	kanoistka
Štěpánka	Štěpánka	k1gFnSc1	Štěpánka
Hilgertová	Hilgertová	k1gFnSc1	Hilgertová
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
a	a	k8xC	a
oštěpařka	oštěpařka	k1gFnSc1	oštěpařka
Barbora	Barbora	k1gFnSc1	Barbora
Špotáková	Špotáková	k1gFnSc1	Špotáková
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
zimních	zimní	k2eAgFnPc6d1	zimní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
vybojovala	vybojovat	k5eAaPmAgFnS	vybojovat
rychlobruslařka	rychlobruslařka	k1gFnSc1	rychlobruslařka
Martina	Martina	k1gFnSc1	Martina
Sáblíková	Sáblíková	k1gFnSc1	Sáblíková
tři	tři	k4xCgFnPc4	tři
zlaté	zlatý	k2eAgFnPc4d1	zlatá
medaile	medaile	k1gFnPc4	medaile
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
×	×	k?	×
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
×	×	k?	×
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lyžařka	lyžařka	k1gFnSc1	lyžařka
Kateřina	Kateřina	k1gFnSc1	Kateřina
Neumannová	Neumannová	k1gFnSc1	Neumannová
přivezla	přivézt	k5eAaPmAgFnS	přivézt
ze	z	k7c2	z
ZOH	ZOH	kA	ZOH
celkem	celkem	k6eAd1	celkem
šest	šest	k4xCc4	šest
medailí	medaile	k1gFnPc2	medaile
<g/>
.	.	kIx.	.
</s>
<s>
Ester	Ester	k1gFnSc1	Ester
Ledecká	Ledecká	k1gFnSc1	Ledecká
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
prvním	první	k4xOgInSc7	první
českým	český	k2eAgInSc7d1	český
sportovcem	sportovec	k1gMnSc7	sportovec
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
vybojoval	vybojovat	k5eAaPmAgMnS	vybojovat
olympijské	olympijský	k2eAgNnSc4d1	Olympijské
zlato	zlato	k1gNnSc4	zlato
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
různých	různý	k2eAgInPc6d1	různý
sportech	sport	k1gInPc6	sport
–	–	k?	–
ve	v	k7c6	v
snowboardingu	snowboarding	k1gInSc6	snowboarding
a	a	k8xC	a
ve	v	k7c6	v
sjezdovém	sjezdový	k2eAgNnSc6d1	sjezdové
lyžování	lyžování	k1gNnSc6	lyžování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Lehká	lehký	k2eAgFnSc1d1	lehká
atletika	atletika	k1gFnSc1	atletika
===	===	k?	===
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
letních	letní	k2eAgFnPc2d1	letní
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
se	se	k3xPyFc4	se
čeští	český	k2eAgMnPc1d1	český
lehcí	lehký	k2eAgMnPc1d1	lehký
atleti	atlet	k1gMnPc1	atlet
účastní	účastnit	k5eAaImIp3nP	účastnit
také	také	k9	také
pravidelných	pravidelný	k2eAgNnPc2d1	pravidelné
mistrovství	mistrovství	k1gNnPc2	mistrovství
světa	svět	k1gInSc2	svět
a	a	k8xC	a
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dresu	dres	k1gInSc6	dres
samostatného	samostatný	k2eAgNnSc2d1	samostatné
Česka	Česko	k1gNnSc2	Česko
se	se	k3xPyFc4	se
mistry	mistr	k1gMnPc7	mistr
světa	svět	k1gInSc2	svět
v	v	k7c6	v
atletice	atletika	k1gFnSc6	atletika
stali	stát	k5eAaPmAgMnP	stát
oštěpaři	oštěpař	k1gMnPc1	oštěpař
Jan	Jan	k1gMnSc1	Jan
Železný	Železný	k1gMnSc1	Železný
<g/>
,	,	kIx,	,
Vítězslav	Vítězslav	k1gMnSc1	Vítězslav
Veselý	Veselý	k1gMnSc1	Veselý
a	a	k8xC	a
Barbora	Barbora	k1gFnSc1	Barbora
Špotáková	Špotáková	k1gFnSc1	Špotáková
<g/>
,	,	kIx,	,
trojskokanka	trojskokanka	k1gFnSc1	trojskokanka
Šárka	Šárka	k1gFnSc1	Šárka
Kašpárková	Kašpárková	k1gFnSc1	Kašpárková
<g/>
,	,	kIx,	,
běžci	běžec	k1gMnPc1	běžec
Ludmila	Ludmila	k1gFnSc1	Ludmila
Formanová	Formanová	k1gFnSc1	Formanová
a	a	k8xC	a
Zuzana	Zuzana	k1gFnSc1	Zuzana
Hejnová	Hejnová	k1gFnSc1	Hejnová
<g/>
,	,	kIx,	,
desetibojaři	desetibojař	k1gMnPc1	desetibojař
Tomáš	Tomáš	k1gMnSc1	Tomáš
Dvořák	Dvořák	k1gMnSc1	Dvořák
a	a	k8xC	a
Roman	Roman	k1gMnSc1	Roman
Šebrle	Šebrle	k1gFnSc2	Šebrle
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hale	hala	k1gFnSc6	hala
navíc	navíc	k6eAd1	navíc
zlato	zlato	k1gNnSc1	zlato
brali	brát	k5eAaImAgMnP	brát
běžec	běžec	k1gMnSc1	běžec
Pavel	Pavel	k1gMnSc1	Pavel
Maslák	Maslák	k1gMnSc1	Maslák
(	(	kIx(	(
<g/>
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
již	již	k6eAd1	již
třikrát	třikrát	k6eAd1	třikrát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sedmibojař	sedmibojař	k1gMnSc1	sedmibojař
Robert	Robert	k1gMnSc1	Robert
Změlík	Změlík	k1gMnSc1	Změlík
a	a	k8xC	a
skokanka	skokanka	k1gFnSc1	skokanka
o	o	k7c6	o
tyči	tyč	k1gFnSc6	tyč
Pavla	Pavla	k1gFnSc1	Pavla
Hamáčková	Hamáčková	k1gFnSc1	Hamáčková
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
krom	krom	k7c2	krom
jmenovaných	jmenovaná	k1gFnPc2	jmenovaná
zlato	zlato	k1gNnSc4	zlato
získali	získat	k5eAaPmAgMnP	získat
i	i	k8xC	i
skokanka	skokanka	k1gFnSc1	skokanka
o	o	k7c6	o
tyči	tyč	k1gFnSc6	tyč
Jiřina	Jiřina	k1gFnSc1	Jiřina
Ptáčníková	Ptáčníková	k1gFnSc1	Ptáčníková
<g/>
,	,	kIx,	,
v	v	k7c6	v
hale	hala	k1gFnSc6	hala
překážkář	překážkář	k1gMnSc1	překážkář
Petr	Petr	k1gMnSc1	Petr
Svoboda	Svoboda	k1gMnSc1	Svoboda
a	a	k8xC	a
běžci	běžec	k1gMnPc1	běžec
Denisa	Denisa	k1gFnSc1	Denisa
Rosolová	Rosolová	k1gFnSc1	Rosolová
a	a	k8xC	a
Jakub	Jakub	k1gMnSc1	Jakub
Holuša	Holuša	k1gMnSc1	Holuša
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
dresu	dres	k1gInSc6	dres
Československa	Československo	k1gNnSc2	Československo
titul	titul	k1gInSc4	titul
mistra	mistr	k1gMnSc2	mistr
světa	svět	k1gInSc2	svět
vybojovali	vybojovat	k5eAaPmAgMnP	vybojovat
i	i	k9	i
další	další	k2eAgMnPc1d1	další
Češi	Čech	k1gMnPc1	Čech
<g/>
:	:	kIx,	:
běžkyně	běžkyně	k1gFnSc1	běžkyně
Jarmila	Jarmila	k1gFnSc1	Jarmila
Kratochvílová	Kratochvílová	k1gFnSc1	Kratochvílová
<g/>
,	,	kIx,	,
koulařka	koulařka	k1gFnSc1	koulařka
Helena	Helena	k1gFnSc1	Helena
Fibingerová	Fibingerová	k1gFnSc1	Fibingerová
<g/>
,	,	kIx,	,
v	v	k7c6	v
hale	hala	k1gFnSc6	hala
pak	pak	k6eAd1	pak
koulař	koulař	k1gMnSc1	koulař
Remigius	Remigius	k1gMnSc1	Remigius
Machura	Machura	k1gFnSc1	Machura
a	a	k8xC	a
skokan	skokan	k1gMnSc1	skokan
do	do	k7c2	do
dálky	dálka	k1gFnSc2	dálka
Jan	Jan	k1gMnSc1	Jan
Leitner	Leitner	k1gMnSc1	Leitner
<g/>
.	.	kIx.	.
</s>
<s>
Mistry	mistr	k1gMnPc4	mistr
Evropy	Evropa	k1gFnSc2	Evropa
byli	být	k5eAaImAgMnP	být
běžci	běžec	k1gMnPc1	běžec
Emil	Emil	k1gMnSc1	Emil
Zátopek	Zátopek	k1gInSc1	Zátopek
a	a	k8xC	a
Jaroslava	Jaroslava	k1gFnSc1	Jaroslava
Jehličková	jehličkový	k2eAgFnSc1d1	Jehličková
<g/>
,	,	kIx,	,
překážkář	překážkář	k1gMnSc1	překážkář
Jindřich	Jindřich	k1gMnSc1	Jindřich
Roudný	Roudný	k2eAgMnSc1d1	Roudný
<g/>
,	,	kIx,	,
koulař	koulař	k1gMnSc1	koulař
Jiří	Jiří	k1gMnSc1	Jiří
Skobla	Skobla	k1gMnSc1	Skobla
<g/>
,	,	kIx,	,
chodec	chodec	k1gMnSc1	chodec
Josef	Josef	k1gMnSc1	Josef
Doležal	Doležal	k1gMnSc1	Doležal
<g/>
,	,	kIx,	,
oštěpařka	oštěpařka	k1gFnSc1	oštěpařka
Dana	Dana	k1gFnSc1	Dana
Zátopková	Zátopková	k1gFnSc1	Zátopková
<g/>
,	,	kIx,	,
skokanka	skokanka	k1gFnSc1	skokanka
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
Miloslava	Miloslava	k1gFnSc1	Miloslava
Rezková	Rezková	k1gFnSc1	Rezková
<g/>
,	,	kIx,	,
diskař	diskař	k1gMnSc1	diskař
Ludvík	Ludvík	k1gMnSc1	Ludvík
Daněk	Daněk	k1gMnSc1	Daněk
<g/>
,	,	kIx,	,
v	v	k7c6	v
hale	hala	k1gFnSc6	hala
titul	titul	k1gInSc1	titul
navíc	navíc	k6eAd1	navíc
krom	krom	k7c2	krom
jmenovaných	jmenovaný	k1gMnPc2	jmenovaný
získali	získat	k5eAaPmAgMnP	získat
skokani	skokan	k1gMnPc1	skokan
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
Milada	Milada	k1gFnSc1	Milada
Karbanová	Karbanová	k1gFnSc1	Karbanová
a	a	k8xC	a
Vladimír	Vladimír	k1gMnSc1	Vladimír
Malý	Malý	k1gMnSc1	Malý
<g/>
,	,	kIx,	,
běžci	běžec	k1gMnPc1	běžec
Karel	Karel	k1gMnSc1	Karel
Kolář	Kolář	k1gMnSc1	Kolář
<g/>
,	,	kIx,	,
Taťána	Taťána	k1gFnSc1	Taťána
Kocembová	Kocembová	k1gFnSc1	Kocembová
<g/>
,	,	kIx,	,
Lubomír	Lubomír	k1gMnSc1	Lubomír
Tesáček	tesáček	k1gInSc1	tesáček
a	a	k8xC	a
Milena	Milena	k1gFnSc1	Milena
Matějkovičová	Matějkovičová	k1gFnSc1	Matějkovičová
<g/>
,	,	kIx,	,
překážkář	překážkář	k1gMnSc1	překážkář
Aleš	Aleš	k1gMnSc1	Aleš
Höffer	Höffer	k1gMnSc1	Höffer
<g/>
,	,	kIx,	,
koulař	koulař	k1gMnSc1	koulař
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Brabec	Brabec	k1gMnSc1	Brabec
a	a	k8xC	a
skokanka	skokanka	k1gFnSc1	skokanka
do	do	k7c2	do
dálky	dálka	k1gFnSc2	dálka
Jarmila	Jarmila	k1gFnSc1	Jarmila
Nygrýnová	Nygrýnová	k1gFnSc1	Nygrýnová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Běžkyně	běžkyně	k1gFnSc1	běžkyně
Jarmila	Jarmila	k1gFnSc1	Jarmila
Kratochvílová	Kratochvílová	k1gFnSc1	Kratochvílová
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
drží	držet	k5eAaImIp3nS	držet
světový	světový	k2eAgInSc4d1	světový
rekord	rekord	k1gInSc4	rekord
v	v	k7c6	v
běhu	běh	k1gInSc6	běh
na	na	k7c4	na
800	[number]	k4	800
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
nejdéle	dlouho	k6eAd3	dlouho
platný	platný	k2eAgInSc4d1	platný
světový	světový	k2eAgInSc4d1	světový
rekord	rekord	k1gInSc4	rekord
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
ženské	ženský	k2eAgFnSc2d1	ženská
atletiky	atletika	k1gFnSc2	atletika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Fotbal	fotbal	k1gInSc1	fotbal
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Reprezentace	reprezentace	k1gFnSc2	reprezentace
====	====	k?	====
</s>
</p>
<p>
<s>
Fotbalová	fotbalový	k2eAgFnSc1d1	fotbalová
reprezentace	reprezentace	k1gFnSc1	reprezentace
Československa	Československo	k1gNnSc2	Československo
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
získala	získat	k5eAaPmAgFnS	získat
dvě	dva	k4xCgFnPc4	dva
stříbrné	stříbrný	k2eAgFnPc4d1	stříbrná
medaile	medaile	k1gFnPc4	medaile
na	na	k7c4	na
mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1934	[number]	k4	1934
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1962	[number]	k4	1962
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
vyhrálo	vyhrát	k5eAaPmAgNnS	vyhrát
Československo	Československo	k1gNnSc1	Československo
mistrovství	mistrovství	k1gNnSc2	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
1976	[number]	k4	1976
v	v	k7c6	v
Jugoslávii	Jugoslávie	k1gFnSc6	Jugoslávie
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
ovšem	ovšem	k9	ovšem
bylo	být	k5eAaImAgNnS	být
z	z	k7c2	z
22	[number]	k4	22
hráčů	hráč	k1gMnPc2	hráč
na	na	k7c6	na
soupisce	soupiska	k1gFnSc6	soupiska
jen	jen	k9	jen
sedm	sedm	k4xCc4	sedm
Čechů	Čech	k1gMnPc2	Čech
<g/>
.	.	kIx.	.
</s>
<s>
Stříbro	stříbro	k1gNnSc1	stříbro
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
turnaji	turnaj	k1gInSc6	turnaj
národní	národní	k2eAgInSc1d1	národní
tým	tým	k1gInSc1	tým
vybojoval	vybojovat	k5eAaPmAgInS	vybojovat
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
bronz	bronz	k1gInSc4	bronz
pak	pak	k6eAd1	pak
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1980	[number]	k4	1980
a	a	k8xC	a
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
získal	získat	k5eAaPmAgInS	získat
olympijský	olympijský	k2eAgInSc1d1	olympijský
výběr	výběr	k1gInSc1	výběr
ČSSR	ČSSR	kA	ČSSR
zlaté	zlatý	k2eAgFnSc2d1	zlatá
medaile	medaile	k1gFnSc2	medaile
na	na	k7c6	na
letních	letní	k2eAgFnPc6d1	letní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Klubový	klubový	k2eAgInSc1d1	klubový
fotbal	fotbal	k1gInSc1	fotbal
====	====	k?	====
</s>
</p>
<p>
<s>
Před	před	k7c7	před
druhou	druhý	k4xOgFnSc7	druhý
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
patřily	patřit	k5eAaImAgFnP	patřit
AC	AC	kA	AC
Sparta	Sparta	k1gFnSc1	Sparta
Praha	Praha	k1gFnSc1	Praha
a	a	k8xC	a
SK	Sk	kA	Sk
Slavia	Slavia	k1gFnSc1	Slavia
Praha	Praha	k1gFnSc1	Praha
k	k	k7c3	k
nejlepším	dobrý	k2eAgInPc3d3	nejlepší
klubům	klub	k1gInPc3	klub
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Sparta	Sparta	k1gFnSc1	Sparta
dvakrát	dvakrát	k6eAd1	dvakrát
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
Středoevropský	středoevropský	k2eAgInSc4d1	středoevropský
pohár	pohár	k1gInSc4	pohár
(	(	kIx(	(
<g/>
1927	[number]	k4	1927
<g/>
,	,	kIx,	,
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejprestižnější	prestižní	k2eAgFnSc4d3	nejprestižnější
klubovou	klubový	k2eAgFnSc4d1	klubová
soutěž	soutěž	k1gFnSc4	soutěž
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
Slavia	Slavia	k1gFnSc1	Slavia
jednou	jednou	k6eAd1	jednou
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
založení	založení	k1gNnSc6	založení
evropských	evropský	k2eAgInPc2d1	evropský
pohárů	pohár	k1gInPc2	pohár
<g/>
,	,	kIx,	,
organizovaných	organizovaný	k2eAgInPc2d1	organizovaný
od	od	k7c2	od
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
asociací	asociace	k1gFnPc2	asociace
UEFA	UEFA	kA	UEFA
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
v	v	k7c6	v
nich	on	k3xPp3gNnPc6	on
nejdál	daleko	k6eAd3	daleko
dostali	dostat	k5eAaPmAgMnP	dostat
Dukla	Dukla	k1gFnSc1	Dukla
Praha	Praha	k1gFnSc1	Praha
(	(	kIx(	(
<g/>
semifinále	semifinále	k1gNnPc6	semifinále
PMEZ	PMEZ	kA	PMEZ
1966	[number]	k4	1966
<g/>
/	/	kIx~	/
<g/>
67	[number]	k4	67
<g/>
,	,	kIx,	,
semifinále	semifinále	k1gNnSc6	semifinále
PVP	PVP	kA	PVP
1985	[number]	k4	1985
<g/>
/	/	kIx~	/
<g/>
86	[number]	k4	86
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Sparta	Sparta	k1gFnSc1	Sparta
(	(	kIx(	(
<g/>
semifinále	semifinále	k1gNnSc1	semifinále
PMEZ	PMEZ	kA	PMEZ
<g />
.	.	kIx.	.
</s>
<s>
1991	[number]	k4	1991
<g/>
/	/	kIx~	/
<g/>
92	[number]	k4	92
<g/>
,	,	kIx,	,
semifinále	semifinále	k1gNnSc6	semifinále
PVP	PVP	kA	PVP
1972	[number]	k4	1972
<g/>
/	/	kIx~	/
<g/>
73	[number]	k4	73
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Baník	Baník	k1gInSc1	Baník
Ostrava	Ostrava	k1gFnSc1	Ostrava
(	(	kIx(	(
<g/>
semifinále	semifinále	k1gNnPc6	semifinále
PVP	PVP	kA	PVP
1978	[number]	k4	1978
<g/>
/	/	kIx~	/
<g/>
79	[number]	k4	79
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bohemians	Bohemians	k1gInSc1	Bohemians
Praha	Praha	k1gFnSc1	Praha
(	(	kIx(	(
<g/>
semifinále	semifinále	k1gNnPc6	semifinále
UEFA	UEFA	kA	UEFA
1982	[number]	k4	1982
<g/>
/	/	kIx~	/
<g/>
83	[number]	k4	83
<g/>
)	)	kIx)	)
a	a	k8xC	a
Slavia	Slavia	k1gFnSc1	Slavia
(	(	kIx(	(
<g/>
semifinále	semifinále	k1gNnPc6	semifinále
UEFA	UEFA	kA	UEFA
1995	[number]	k4	1995
<g/>
/	/	kIx~	/
<g/>
96	[number]	k4	96
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc4	čtvrtfinále
hrály	hrát	k5eAaImAgFnP	hrát
SK	Sk	kA	Sk
Hradec	Hradec	k1gInSc4	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
,	,	kIx,	,
Zbrojovka	zbrojovka	k1gFnSc1	zbrojovka
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
TJ	tj	kA	tj
Vítkovice	Vítkovice	k1gInPc1	Vítkovice
<g/>
,	,	kIx,	,
Sigma	sigma	k1gNnSc4	sigma
Olomouc	Olomouc	k1gFnSc1	Olomouc
a	a	k8xC	a
Slovan	Slovan	k1gMnSc1	Slovan
Liberec	Liberec	k1gInSc1	Liberec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Osobnosti	osobnost	k1gFnPc1	osobnost
====	====	k?	====
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Masopust	Masopust	k1gMnSc1	Masopust
(	(	kIx(	(
<g/>
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
a	a	k8xC	a
Pavel	Pavel	k1gMnSc1	Pavel
Nedvěd	Nedvěd	k1gMnSc1	Nedvěd
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
vyhráli	vyhrát	k5eAaPmAgMnP	vyhrát
anketu	anketa	k1gFnSc4	anketa
Zlatý	zlatý	k2eAgInSc4d1	zlatý
míč	míč	k1gInSc4	míč
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
každoročně	každoročně	k6eAd1	každoročně
hledá	hledat	k5eAaImIp3nS	hledat
nejlepšího	dobrý	k2eAgMnSc4d3	nejlepší
fotbalistu	fotbalista	k1gMnSc4	fotbalista
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Brankář	brankář	k1gMnSc1	brankář
Ivo	Ivo	k1gMnSc1	Ivo
Viktor	Viktor	k1gMnSc1	Viktor
skončil	skončit	k5eAaPmAgMnS	skončit
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
anketě	anketa	k1gFnSc6	anketa
roku	rok	k1gInSc2	rok
1976	[number]	k4	1976
třetí	třetí	k4xOgInSc1	třetí
<g/>
.	.	kIx.	.
</s>
<s>
Oldřich	Oldřich	k1gMnSc1	Oldřich
Nejedlý	Nejedlý	k1gMnSc1	Nejedlý
byl	být	k5eAaImAgMnS	být
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
střelcem	střelec	k1gMnSc7	střelec
mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
1934	[number]	k4	1934
<g/>
.	.	kIx.	.
</s>
<s>
Milan	Milan	k1gMnSc1	Milan
Baroš	Baroš	k1gMnSc1	Baroš
byl	být	k5eAaImAgMnS	být
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
střelcem	střelec	k1gMnSc7	střelec
mistrovství	mistrovství	k1gNnSc2	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
startů	start	k1gInPc2	start
za	za	k7c4	za
reprezentaci	reprezentace	k1gFnSc4	reprezentace
Československa	Československo	k1gNnSc2	Československo
si	se	k3xPyFc3	se
připsal	připsat	k5eAaPmAgMnS	připsat
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Nehoda	nehoda	k1gFnSc1	nehoda
<g/>
,	,	kIx,	,
za	za	k7c4	za
reprezentaci	reprezentace	k1gFnSc4	reprezentace
ČR	ČR	kA	ČR
Petr	Petr	k1gMnSc1	Petr
Čech	Čech	k1gMnSc1	Čech
<g/>
,	,	kIx,	,
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
československým	československý	k2eAgMnSc7d1	československý
reprezentačním	reprezentační	k2eAgMnSc7d1	reprezentační
střelcem	střelec	k1gMnSc7	střelec
byl	být	k5eAaImAgMnS	být
Antonín	Antonín	k1gMnSc1	Antonín
Puč	puč	k1gInSc4	puč
<g/>
,	,	kIx,	,
v	v	k7c6	v
dresu	dres	k1gInSc6	dres
ČR	ČR	kA	ČR
Jan	Jan	k1gMnSc1	Jan
Koller	Koller	k1gMnSc1	Koller
<g/>
.	.	kIx.	.
</s>
<s>
Nejprestižnější	prestižní	k2eAgFnSc4d3	nejprestižnější
evropskou	evropský	k2eAgFnSc4d1	Evropská
pohárovou	pohárový	k2eAgFnSc4d1	pohárová
soutěž	soutěž	k1gFnSc4	soutěž
<g/>
,	,	kIx,	,
Ligu	liga	k1gFnSc4	liga
mistrů	mistr	k1gMnPc2	mistr
<g/>
,	,	kIx,	,
vyhráli	vyhrát	k5eAaPmAgMnP	vyhrát
Vladimír	Vladimír	k1gMnSc1	Vladimír
Šmicer	Šmicer	k1gMnSc1	Šmicer
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
Baroš	Baroš	k1gMnSc1	Baroš
(	(	kIx(	(
<g/>
oba	dva	k4xCgMnPc1	dva
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
s	s	k7c7	s
FC	FC	kA	FC
Liverpool	Liverpool	k1gInSc1	Liverpool
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Marek	Marek	k1gMnSc1	Marek
Jankulovski	Jankulovsk	k1gFnSc2	Jankulovsk
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
s	s	k7c7	s
AC	AC	kA	AC
Milan	Milan	k1gMnSc1	Milan
<g/>
)	)	kIx)	)
a	a	k8xC	a
brankář	brankář	k1gMnSc1	brankář
Petr	Petr	k1gMnSc1	Petr
Čech	Čech	k1gMnSc1	Čech
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
s	s	k7c7	s
Chelsea	Chelseum	k1gNnPc1	Chelseum
FC	FC	kA	FC
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nedvěd	Nedvěd	k1gMnSc1	Nedvěd
získal	získat	k5eAaPmAgMnS	získat
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
s	s	k7c7	s
Laziem	Lazium	k1gNnSc7	Lazium
Řím	Řím	k1gInSc1	Řím
Pohár	pohár	k1gInSc1	pohár
vítězů	vítěz	k1gMnPc2	vítěz
pohárů	pohár	k1gInPc2	pohár
<g/>
.	.	kIx.	.
</s>
<s>
Pohár	pohár	k1gInSc1	pohár
UEFA	UEFA	kA	UEFA
(	(	kIx(	(
<g/>
potažmo	potažmo	k6eAd1	potažmo
nástupnickou	nástupnický	k2eAgFnSc4d1	nástupnická
Evropskou	evropský	k2eAgFnSc4d1	Evropská
ligu	liga	k1gFnSc4	liga
<g/>
)	)	kIx)	)
vyhráli	vyhrát	k5eAaPmAgMnP	vyhrát
Jiří	Jiří	k1gMnSc1	Jiří
Němec	Němec	k1gMnSc1	Němec
<g/>
,	,	kIx,	,
Radoslav	Radoslav	k1gMnSc1	Radoslav
Látal	Látal	k1gMnSc1	Látal
(	(	kIx(	(
<g/>
oba	dva	k4xCgMnPc1	dva
1997	[number]	k4	1997
se	s	k7c7	s
Schalke	Schalke	k1gFnSc7	Schalke
0	[number]	k4	0
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Šmicer	Šmicer	k1gMnSc1	Šmicer
<g/>
,	,	kIx,	,
Patrik	Patrik	k1gMnSc1	Patrik
Berger	Berger	k1gMnSc1	Berger
(	(	kIx(	(
<g/>
oba	dva	k4xCgMnPc1	dva
2001	[number]	k4	2001
s	s	k7c7	s
Liverpoolem	Liverpool	k1gInSc7	Liverpool
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Radek	Radek	k1gMnSc1	Radek
Šírl	Šírl	k1gMnSc1	Šírl
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
se	s	k7c7	s
Zenitem	zenit	k1gInSc7	zenit
Petrohrad	Petrohrad	k1gInSc1	Petrohrad
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tomáš	Tomáš	k1gMnSc1	Tomáš
Hübschman	Hübschman	k1gMnSc1	Hübschman
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
se	s	k7c7	s
Šachťarem	Šachťar	k1gInSc7	Šachťar
Doněck	Doněck	k1gInSc1	Doněck
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tomáš	Tomáš	k1gMnSc1	Tomáš
Ujfaluši	Ujfaluše	k1gFnSc4	Ujfaluše
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
s	s	k7c7	s
klubem	klub	k1gInSc7	klub
Atlético	Atlético	k1gMnSc1	Atlético
Madrid	Madrid	k1gInSc1	Madrid
<g/>
)	)	kIx)	)
a	a	k8xC	a
Petr	Petr	k1gMnSc1	Petr
Čech	Čech	k1gMnSc1	Čech
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
s	s	k7c7	s
Chelsea	Chelse	k2eAgNnPc1d1	Chelse
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Legendárním	legendární	k2eAgMnSc7d1	legendární
střelcem	střelec	k1gMnSc7	střelec
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc4	jehož
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
kariéry	kariéra	k1gFnSc2	kariéra
však	však	k9	však
pohltila	pohltit	k5eAaPmAgFnS	pohltit
druhá	druhý	k4xOgFnSc1	druhý
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Josef	Josef	k1gMnSc1	Josef
Bican	Bican	k1gMnSc1	Bican
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
brankářů	brankář	k1gMnPc2	brankář
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
na	na	k7c6	na
světě	svět	k1gInSc6	svět
byl	být	k5eAaImAgMnS	být
František	František	k1gMnSc1	František
Plánička	plánička	k1gFnSc1	plánička
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalším	další	k2eAgMnPc3d1	další
úspěšným	úspěšný	k2eAgMnPc3d1	úspěšný
fotbalistům	fotbalista	k1gMnPc3	fotbalista
patří	patřit	k5eAaImIp3nP	patřit
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Pluskal	Pluskal	k1gMnSc1	Pluskal
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
Novák	Novák	k1gMnSc1	Novák
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
Panenka	panenka	k1gFnSc1	panenka
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
Vízek	Vízek	k1gMnSc1	Vízek
<g/>
,	,	kIx,	,
Tomáš	Tomáš	k1gMnSc1	Tomáš
Skuhravý	Skuhravý	k1gMnSc1	Skuhravý
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Poborský	Poborský	k2eAgMnSc1d1	Poborský
a	a	k8xC	a
Tomáš	Tomáš	k1gMnSc1	Tomáš
Rosický	rosický	k2eAgMnSc1d1	rosický
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Lední	lední	k2eAgInSc1d1	lední
hokej	hokej	k1gInSc1	hokej
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
ledním	lední	k2eAgInSc6d1	lední
hokeji	hokej	k1gInSc6	hokej
patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
největším	veliký	k2eAgInPc3d3	veliký
reprezentačním	reprezentační	k2eAgInPc3d1	reprezentační
úspěchům	úspěch	k1gInPc3	úspěch
zlatá	zlatý	k2eAgFnSc1d1	zlatá
medaile	medaile	k1gFnPc1	medaile
na	na	k7c6	na
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
v	v	k7c6	v
Naganu	Nagano	k1gNnSc6	Nagano
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
cena	cena	k1gFnSc1	cena
tkví	tkvět	k5eAaImIp3nS	tkvět
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
turnaje	turnaj	k1gInSc2	turnaj
zúčastnili	zúčastnit	k5eAaPmAgMnP	zúčastnit
poprvé	poprvé	k6eAd1	poprvé
všichni	všechen	k3xTgMnPc1	všechen
nejlepší	dobrý	k2eAgMnPc1d3	nejlepší
hráči	hráč	k1gMnPc1	hráč
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
profesionálů	profesionál	k1gMnPc2	profesionál
ze	z	k7c2	z
severoamerické	severoamerický	k2eAgFnSc2d1	severoamerická
NHL	NHL	kA	NHL
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
hvězdám	hvězda	k1gFnPc3	hvězda
týmu	tým	k1gInSc2	tým
patřili	patřit	k5eAaImAgMnP	patřit
zejména	zejména	k9	zejména
Jaromír	Jaromír	k1gMnSc1	Jaromír
Jágr	Jágr	k1gMnSc1	Jágr
a	a	k8xC	a
brankář	brankář	k1gMnSc1	brankář
Dominik	Dominik	k1gMnSc1	Dominik
Hašek	Hašek	k1gMnSc1	Hašek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Co	co	k3yInSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
hokejových	hokejový	k2eAgFnPc2d1	hokejová
individualit	individualita	k1gFnPc2	individualita
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Jágr	Jágr	k1gMnSc1	Jágr
co	co	k9	co
do	do	k7c2	do
počtu	počet	k1gInSc2	počet
bodů	bod	k1gInPc2	bod
nejlepším	dobrý	k2eAgInPc3d3	nejlepší
v	v	k7c6	v
současnosti	současnost	k1gFnSc2	současnost
aktivním	aktivní	k2eAgMnSc7d1	aktivní
střelcem	střelec	k1gMnSc7	střelec
v	v	k7c6	v
NHL	NHL	kA	NHL
a	a	k8xC	a
druhým	druhý	k4xOgMnPc3	druhý
nejlepším	dobrý	k2eAgMnPc3d3	nejlepší
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
historii	historie	k1gFnSc6	historie
této	tento	k3xDgFnSc2	tento
ligy	liga	k1gFnSc2	liga
<g/>
.	.	kIx.	.
</s>
<s>
Dvakrát	dvakrát	k6eAd1	dvakrát
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
Stanleyův	Stanleyův	k2eAgInSc4d1	Stanleyův
pohár	pohár	k1gInSc4	pohár
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
/	/	kIx~	/
<g/>
91	[number]	k4	91
a	a	k8xC	a
1991	[number]	k4	1991
<g/>
/	/	kIx~	/
<g/>
92	[number]	k4	92
s	s	k7c7	s
klubem	klub	k1gInSc7	klub
Pittsburgh	Pittsburgh	k1gInSc1	Pittsburgh
Penguins	Penguinsa	k1gFnPc2	Penguinsa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc4	dva
Stanley	Stanle	k2eAgInPc4d1	Stanle
Cupy	cup	k1gInPc4	cup
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
Hašek	Hašek	k1gMnSc1	Hašek
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
s	s	k7c7	s
Detroit	Detroit	k1gInSc1	Detroit
Red	Red	k1gMnSc1	Red
Wings	Wings	k1gInSc1	Wings
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
/	/	kIx~	/
<g/>
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
/	/	kIx~	/
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Třikrát	třikrát	k6eAd1	třikrát
tuto	tento	k3xDgFnSc4	tento
prestižní	prestižní	k2eAgFnSc4d1	prestižní
trofej	trofej	k1gFnSc4	trofej
vyhráli	vyhrát	k5eAaPmAgMnP	vyhrát
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Pouzar	Pouzar	k1gMnSc1	Pouzar
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Hrdina	Hrdina	k1gMnSc1	Hrdina
<g/>
,	,	kIx,	,
dvakrát	dvakrát	k6eAd1	dvakrát
Petr	Petr	k1gMnSc1	Petr
Sýkora	Sýkora	k1gMnSc1	Sýkora
<g/>
,	,	kIx,	,
Patrik	Patrik	k1gMnSc1	Patrik
Eliáš	Eliáš	k1gMnSc1	Eliáš
<g/>
,	,	kIx,	,
Bobby	Bobba	k1gFnPc1	Bobba
Holík	Holík	k1gMnSc1	Holík
<g/>
,	,	kIx,	,
Michal	Michal	k1gMnSc1	Michal
Rozsíval	Rozsíval	k1gMnSc1	Rozsíval
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Fischer	Fischer	k1gMnSc1	Fischer
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
bodů	bod	k1gInPc2	bod
v	v	k7c6	v
kanadském	kanadský	k2eAgNnSc6d1	kanadské
bodování	bodování	k1gNnSc6	bodování
získali	získat	k5eAaPmAgMnP	získat
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
části	část	k1gFnSc6	část
NHL	NHL	kA	NHL
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Jágrem	Jágr	k1gMnSc7	Jágr
<g/>
,	,	kIx,	,
Eliášem	Eliáš	k1gMnSc7	Eliáš
<g/>
,	,	kIx,	,
Holíkem	Holík	k1gMnSc7	Holík
a	a	k8xC	a
Sýkorou	sýkora	k1gFnSc7	sýkora
také	také	k9	také
Milan	Milan	k1gMnSc1	Milan
Hejduk	Hejduk	k1gMnSc1	Hejduk
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
Nedvěd	Nedvěd	k1gMnSc1	Nedvěd
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
Straka	Straka	k1gMnSc1	Straka
a	a	k8xC	a
Václav	Václav	k1gMnSc1	Václav
Prospal	prospat	k5eAaPmAgMnS	prospat
<g/>
,	,	kIx,	,
ve	v	k7c4	v
Stanley	Stanle	k1gMnPc4	Stanle
Cupu	cup	k1gInSc2	cup
David	David	k1gMnSc1	David
Krejčí	Krejčí	k1gMnSc1	Krejčí
a	a	k8xC	a
Michal	Michal	k1gMnSc1	Michal
Pivoňka	Pivoňka	k1gMnSc1	Pivoňka
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1989	[number]	k4	1989
však	však	k9	však
čeští	český	k2eAgMnPc1d1	český
hokejisté	hokejista	k1gMnPc1	hokejista
NHL	NHL	kA	NHL
mohli	moct	k5eAaImAgMnP	moct
hrát	hrát	k5eAaImF	hrát
jen	jen	k9	jen
výjimečně	výjimečně	k6eAd1	výjimečně
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
měli	mít	k5eAaImAgMnP	mít
světovou	světový	k2eAgFnSc4d1	světová
úroveň	úroveň	k1gFnSc4	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Velkých	velká	k1gFnPc2	velká
úspěchů	úspěch	k1gInPc2	úspěch
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
slavná	slavný	k2eAgFnSc1d1	slavná
generace	generace	k1gFnSc1	generace
čtyřicátých	čtyřicátý	k4xOgNnPc2	čtyřicátý
let	léto	k1gNnPc2	léto
jako	jako	k9	jako
byli	být	k5eAaImAgMnP	být
Vladimír	Vladimír	k1gMnSc1	Vladimír
Bouzek	Bouzek	k1gInSc4	Bouzek
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
Konopásek	Konopásek	k1gMnSc1	Konopásek
<g/>
,	,	kIx,	,
Bohumil	Bohumil	k1gMnSc1	Bohumil
Modrý	modrý	k2eAgMnSc1d1	modrý
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Roziňák	Roziňák	k1gMnSc1	Roziňák
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Zábrodský	Zábrodský	k2eAgMnSc1d1	Zábrodský
<g/>
,	,	kIx,	,
Augustin	Augustin	k1gMnSc1	Augustin
Bubník	Bubník	k1gMnSc1	Bubník
a	a	k8xC	a
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Drobný	drobný	k2eAgMnSc1d1	drobný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
padesátých	padesátý	k4xOgNnPc6	padesátý
a	a	k8xC	a
šedesátých	šedesátý	k4xOgNnPc6	šedesátý
letech	léto	k1gNnPc6	léto
vynikli	vyniknout	k5eAaPmAgMnP	vyniknout
Vlastimil	Vlastimil	k1gMnSc1	Vlastimil
Bubník	Bubník	k1gMnSc1	Bubník
<g/>
,	,	kIx,	,
Bronislav	Bronislav	k1gMnSc1	Bronislav
Danda	Danda	k1gMnSc1	Danda
a	a	k8xC	a
Václav	Václav	k1gMnSc1	Václav
Pantůček	Pantůček	k1gMnSc1	Pantůček
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sedmdesátých	sedmdesátý	k4xOgNnPc6	sedmdesátý
letech	léto	k1gNnPc6	léto
pak	pak	k6eAd1	pak
Jiří	Jiří	k1gMnSc1	Jiří
Bubla	Bubla	k1gMnSc1	Bubla
<g/>
,	,	kIx,	,
Ivan	Ivan	k1gMnSc1	Ivan
Hlinka	Hlinka	k1gMnSc1	Hlinka
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Holeček	Holeček	k1gMnSc1	Holeček
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Holík	Holík	k1gMnSc1	Holík
<g/>
,	,	kIx,	,
Oldřich	Oldřich	k1gMnSc1	Oldřich
Machač	Machač	k1gMnSc1	Machač
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Martinec	Martinec	k1gMnSc1	Martinec
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Pospíšil	Pospíšil	k1gMnSc1	Pospíšil
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
Dvořák	Dvořák	k1gMnSc1	Dvořák
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Nedomanský	Nedomanský	k2eAgMnSc1d1	Nedomanský
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
Nový	Nový	k1gMnSc1	Nový
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Holík	Holík	k1gMnSc1	Holík
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
Chalupa	Chalupa	k1gMnSc1	Chalupa
<g/>
.	.	kIx.	.
</s>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Růžička	Růžička	k1gMnSc1	Růžička
je	být	k5eAaImIp3nS	být
jediným	jediný	k2eAgMnSc7d1	jediný
hráčem	hráč	k1gMnSc7	hráč
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zažil	zažít	k5eAaPmAgMnS	zažít
největší	veliký	k2eAgInSc4d3	veliký
úspěch	úspěch	k1gInSc4	úspěch
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
zlato	zlato	k1gNnSc4	zlato
z	z	k7c2	z
MS	MS	kA	MS
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
i	i	k8xC	i
let	léto	k1gNnPc2	léto
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
(	(	kIx(	(
<g/>
zlato	zlato	k1gNnSc4	zlato
z	z	k7c2	z
Nagana	Nagano	k1gNnSc2	Nagano
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Šlégr	Šlégr	k1gMnSc1	Šlégr
je	být	k5eAaImIp3nS	být
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Jágrem	Jágr	k1gMnSc7	Jágr
jediným	jediný	k2eAgMnSc7d1	jediný
českým	český	k2eAgInSc7d1	český
členem	člen	k1gInSc7	člen
prestižního	prestižní	k2eAgInSc2d1	prestižní
Triple	tripl	k1gInSc5	tripl
Gold	Gold	k1gInSc4	Gold
Clubu	club	k1gInSc2	club
<g/>
,	,	kIx,	,
do	do	k7c2	do
něhož	jenž	k3xRgNnSc2	jenž
symbolicky	symbolicky	k6eAd1	symbolicky
vstupují	vstupovat	k5eAaImIp3nP	vstupovat
hráči	hráč	k1gMnPc1	hráč
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
vyhráli	vyhrát	k5eAaPmAgMnP	vyhrát
mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
olympijské	olympijský	k2eAgFnPc4d1	olympijská
hry	hra	k1gFnPc4	hra
i	i	k8xC	i
Stanleyův	Stanleyův	k2eAgInSc4d1	Stanleyův
pohár	pohár	k1gInSc4	pohár
<g/>
.	.	kIx.	.
</s>
<s>
Mužstvo	mužstvo	k1gNnSc1	mužstvo
Československa	Československo	k1gNnSc2	Československo
vyhrálo	vyhrát	k5eAaPmAgNnS	vyhrát
mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
roku	rok	k1gInSc2	rok
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
1949	[number]	k4	1949
<g/>
,	,	kIx,	,
1972	[number]	k4	1972
<g/>
,	,	kIx,	,
1976	[number]	k4	1976
<g/>
,	,	kIx,	,
1977	[number]	k4	1977
a	a	k8xC	a
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
</s>
<s>
Mužstvo	mužstvo	k1gNnSc1	mužstvo
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
pak	pak	k6eAd1	pak
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
a	a	k8xC	a
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Pět	pět	k4xCc1	pět
zlatých	zlatá	k1gFnPc2	zlatá
z	z	k7c2	z
MS	MS	kA	MS
má	mít	k5eAaImIp3nS	mít
František	František	k1gMnSc1	František
Kaberle	Kaberle	k1gFnSc2	Kaberle
a	a	k8xC	a
David	David	k1gMnSc1	David
Výborný	Výborný	k1gMnSc1	Výborný
<g/>
,	,	kIx,	,
čtyři	čtyři	k4xCgInPc4	čtyři
Pavel	Pavel	k1gMnSc1	Pavel
Patera	Patera	k1gMnSc1	Patera
a	a	k8xC	a
Martin	Martin	k1gMnSc1	Martin
Procházka	Procházka	k1gMnSc1	Procházka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kluby	klub	k1gInPc1	klub
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
vyhrály	vyhrát	k5eAaPmAgInP	vyhrát
českou	český	k2eAgFnSc4d1	Česká
hokejovou	hokejový	k2eAgFnSc4d1	hokejová
ligu	liga	k1gFnSc4	liga
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
HC	HC	kA	HC
Olomouc	Olomouc	k1gFnSc4	Olomouc
<g/>
,	,	kIx,	,
HC	HC	kA	HC
Vsetín	Vsetín	k1gInSc1	Vsetín
<g/>
,	,	kIx,	,
HC	HC	kA	HC
Sparta	Sparta	k1gFnSc1	Sparta
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
HC	HC	kA	HC
Slavia	Slavia	k1gFnSc1	Slavia
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Zlín	Zlín	k1gInSc1	Zlín
<g/>
,	,	kIx,	,
Dynamo	dynamo	k1gNnSc1	dynamo
Pardubice	Pardubice	k1gInPc1	Pardubice
<g/>
,	,	kIx,	,
HC	HC	kA	HC
Karlovy	Karlův	k2eAgInPc1d1	Karlův
Vary	Vary	k1gInPc1	Vary
<g/>
,	,	kIx,	,
HC	HC	kA	HC
Oceláři	ocelář	k1gMnPc1	ocelář
Třinec	Třinec	k1gInSc4	Třinec
<g/>
,	,	kIx,	,
HC	HC	kA	HC
Škoda	škoda	k1gFnSc1	škoda
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
,	,	kIx,	,
HC	HC	kA	HC
Litvínov	Litvínov	k1gInSc1	Litvínov
</s>
<s>
<g/>
,	,	kIx,	,
Bílí	bílý	k2eAgMnPc1d1	bílý
Tygři	tygr	k1gMnPc1	tygr
Liberec	Liberec	k1gInSc4	Liberec
a	a	k8xC	a
Kometa	kometa	k1gFnSc1	kometa
Brno	Brno	k1gNnSc4	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Československou	československý	k2eAgFnSc4d1	Československá
ligu	liga	k1gFnSc4	liga
navíc	navíc	k6eAd1	navíc
také	také	k9	také
LTC	LTC	kA	LTC
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
ATK	ATK	kA	ATK
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Motor	motor	k1gInSc1	motor
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
,	,	kIx,	,
HC	HC	kA	HC
Vítkovice	Vítkovice	k1gInPc1	Vítkovice
<g/>
,	,	kIx,	,
Kladno	Kladno	k1gNnSc1	Kladno
a	a	k8xC	a
Dukla	Dukla	k1gFnSc1	Dukla
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
KHL	KHL	kA	KHL
působil	působit	k5eAaImAgInS	působit
jen	jen	k9	jen
krátkou	krátký	k2eAgFnSc4d1	krátká
dobu	doba	k1gFnSc4	doba
český	český	k2eAgInSc4d1	český
klub	klub	k1gInSc4	klub
HC	HC	kA	HC
Lev	lev	k1gInSc1	lev
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
probojoval	probojovat	k5eAaPmAgMnS	probojovat
se	se	k3xPyFc4	se
až	až	k9	až
do	do	k7c2	do
jejího	její	k3xOp3gNnSc2	její
finále	finále	k1gNnSc2	finále
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Tenis	tenis	k1gInSc1	tenis
===	===	k?	===
</s>
</p>
<p>
<s>
Mimořádně	mimořádně	k6eAd1	mimořádně
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
tenisová	tenisový	k2eAgFnSc1d1	tenisová
škola	škola	k1gFnSc1	škola
<g/>
.	.	kIx.	.
</s>
<s>
Martina	Martina	k1gFnSc1	Martina
Navrátilová	Navrátilová	k1gFnSc1	Navrátilová
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
devětkrát	devětkrát	k6eAd1	devětkrát
nejprestižnější	prestižní	k2eAgInSc4d3	nejprestižnější
tenisový	tenisový	k2eAgInSc4d1	tenisový
turnaj	turnaj	k1gInSc4	turnaj
Wimbledon	Wimbledon	k1gInSc1	Wimbledon
(	(	kIx(	(
<g/>
1978	[number]	k4	1978
<g/>
,	,	kIx,	,
1979	[number]	k4	1979
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
<g/>
,	,	kIx,	,
1983	[number]	k4	1983
<g/>
,	,	kIx,	,
1984	[number]	k4	1984
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
<g/>
,	,	kIx,	,
1986	[number]	k4	1986
<g/>
,	,	kIx,	,
1987	[number]	k4	1987
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dvakrát	dvakrát	k6eAd1	dvakrát
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
Wimbledon	Wimbledon	k1gInSc4	Wimbledon
Petra	Petra	k1gFnSc1	Petra
Kvitová	Kvitová	k1gFnSc1	Kvitová
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
a	a	k8xC	a
jednou	jednou	k6eAd1	jednou
Jana	Jana	k1gFnSc1	Jana
Novotná	Novotná	k1gFnSc1	Novotná
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jediným	jediný	k2eAgMnSc7d1	jediný
českým	český	k2eAgMnSc7d1	český
mužským	mužský	k2eAgMnSc7d1	mužský
vítězem	vítěz	k1gMnSc7	vítěz
turnaje	turnaj	k1gInSc2	turnaj
ve	v	k7c6	v
Wimbledonu	Wimbledon	k1gInSc6	Wimbledon
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
open	open	k1gNnSc1	open
éře	éra	k1gFnSc3	éra
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Jan	Jan	k1gMnSc1	Jan
Kodeš	Kodeš	k1gMnSc1	Kodeš
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
zde	zde	k6eAd1	zde
triumfoval	triumfovat	k5eAaBmAgMnS	triumfovat
i	i	k9	i
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Drobný	drobný	k2eAgMnSc1d1	drobný
(	(	kIx(	(
<g/>
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ivan	Ivan	k1gMnSc1	Ivan
Lendl	Lendl	k1gMnSc1	Lendl
byl	být	k5eAaImAgMnS	být
270	[number]	k4	270
týdnů	týden	k1gInPc2	týden
světovou	světový	k2eAgFnSc7d1	světová
jedničkou	jednička	k1gFnSc7	jednička
v	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
ATP	atp	kA	atp
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
dosud	dosud	k6eAd1	dosud
jediný	jediný	k2eAgMnSc1d1	jediný
český	český	k2eAgMnSc1d1	český
hráč	hráč	k1gMnSc1	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Ženskému	ženský	k2eAgInSc3d1	ženský
žebříčku	žebříček	k1gInSc3	žebříček
WTA	WTA	kA	WTA
kralovala	kralovat	k5eAaImAgFnS	kralovat
celkem	celkem	k6eAd1	celkem
332	[number]	k4	332
týdnů	týden	k1gInPc2	týden
Martina	Martina	k1gFnSc1	Martina
Navrátilová	Navrátilová	k1gFnSc1	Navrátilová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
se	s	k7c7	s
světovou	světový	k2eAgFnSc7d1	světová
hráčkou	hráčka	k1gFnSc7	hráčka
č.	č.	k?	č.
1	[number]	k4	1
stala	stát	k5eAaPmAgFnS	stát
rovněž	rovněž	k9	rovněž
Karolína	Karolína	k1gFnSc1	Karolína
Plíšková	plíškový	k2eAgFnSc1d1	Plíšková
<g/>
.	.	kIx.	.
</s>
<s>
Krom	krom	k7c2	krom
jmenovaných	jmenovaný	k2eAgMnPc2d1	jmenovaný
hráčů	hráč	k1gMnPc2	hráč
vyhráli	vyhrát	k5eAaPmAgMnP	vyhrát
některý	některý	k3yIgInSc4	některý
z	z	k7c2	z
tzv.	tzv.	kA	tzv.
grandslamových	grandslamový	k2eAgInPc2d1	grandslamový
turnajů	turnaj	k1gInPc2	turnaj
také	také	k9	také
Petr	Petr	k1gMnSc1	Petr
Korda	Korda	k1gMnSc1	Korda
(	(	kIx(	(
<g/>
Austrálie	Austrálie	k1gFnSc1	Austrálie
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
a	a	k8xC	a
Hana	Hana	k1gFnSc1	Hana
Mandlíková	Mandlíková	k1gFnSc1	Mandlíková
(	(	kIx(	(
<g/>
Austrálie	Austrálie	k1gFnSc1	Austrálie
1980	[number]	k4	1980
<g/>
,	,	kIx,	,
1987	[number]	k4	1987
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
1981	[number]	k4	1981
<g/>
,	,	kIx,	,
US	US	kA	US
Open	Open	k1gInSc4	Open
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Deblovou	deblový	k2eAgFnSc7d1	deblová
specialistkou	specialistka	k1gFnSc7	specialistka
byla	být	k5eAaImAgFnS	být
Helena	Helena	k1gFnSc1	Helena
Suková	Suková	k1gFnSc1	Suková
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
ve	v	k7c6	v
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
čtrnáct	čtrnáct	k4xCc4	čtrnáct
grandlamových	grandlamový	k2eAgInPc2d1	grandlamový
turnajů	turnaj	k1gInPc2	turnaj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mužská	mužský	k2eAgFnSc1d1	mužská
tenisová	tenisový	k2eAgFnSc1d1	tenisová
reprezentace	reprezentace	k1gFnSc1	reprezentace
Československa	Československo	k1gNnSc2	Československo
resp.	resp.	kA	resp.
Česka	Česko	k1gNnPc4	Česko
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
třikrát	třikrát	k6eAd1	třikrát
Davis	Davis	k1gFnSc1	Davis
Cup	cup	k1gInSc1	cup
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
1980	[number]	k4	1980
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
a	a	k8xC	a
2013	[number]	k4	2013
(	(	kIx(	(
<g/>
poslední	poslední	k2eAgInPc1d1	poslední
dvě	dva	k4xCgFnPc1	dva
soutěže	soutěž	k1gFnPc1	soutěž
jako	jako	k8xS	jako
Český	český	k2eAgInSc1d1	český
daviscupový	daviscupový	k2eAgInSc1d1	daviscupový
tým	tým	k1gInSc1	tým
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ženská	ženský	k2eAgFnSc1d1	ženská
tenisová	tenisový	k2eAgFnSc1d1	tenisová
reprezentace	reprezentace	k1gFnSc1	reprezentace
Československa	Československo	k1gNnSc2	Československo
resp.	resp.	kA	resp.
Česka	Česko	k1gNnSc2	Česko
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
dokonce	dokonce	k9	dokonce
desetkrát	desetkrát	k6eAd1	desetkrát
ve	v	k7c6	v
Fed	Fed	k1gFnSc6	Fed
Cupu	cup	k1gInSc2	cup
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
1983	[number]	k4	1983
<g/>
,	,	kIx,	,
1984	[number]	k4	1984
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
a	a	k8xC	a
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
fedcupový	fedcupový	k2eAgInSc1d1	fedcupový
tým	tým	k1gInSc1	tým
tedy	tedy	k8xC	tedy
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
tuto	tento	k3xDgFnSc4	tento
prestižní	prestižní	k2eAgFnSc4d1	prestižní
soutěž	soutěž	k1gFnSc4	soutěž
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
již	již	k6eAd1	již
pětkrát	pětkrát	k6eAd1	pětkrát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ostatní	ostatní	k2eAgInPc4d1	ostatní
sporty	sport	k1gInPc4	sport
===	===	k?	===
</s>
</p>
<p>
<s>
České	český	k2eAgFnPc1d1	Česká
basketbalistky	basketbalistka	k1gFnPc1	basketbalistka
mají	mít	k5eAaImIp3nP	mít
tři	tři	k4xCgNnPc1	tři
stříbra	stříbro	k1gNnPc1	stříbro
(	(	kIx(	(
<g/>
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
1971	[number]	k4	1971
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
a	a	k8xC	a
tři	tři	k4xCgInPc4	tři
bronzy	bronz	k1gInPc4	bronz
(	(	kIx(	(
<g/>
1957	[number]	k4	1957
<g/>
,	,	kIx,	,
1959	[number]	k4	1959
<g/>
,	,	kIx,	,
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
z	z	k7c2	z
mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
vyhráli	vyhrát	k5eAaPmAgMnP	vyhrát
mistrovství	mistrovství	k1gNnSc4	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Čeští	český	k2eAgMnPc1d1	český
basketbalisté	basketbalista	k1gMnPc1	basketbalista
vyhráli	vyhrát	k5eAaPmAgMnP	vyhrát
mistrovství	mistrovství	k1gNnSc4	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
šestkrát	šestkrát	k6eAd1	šestkrát
přivezli	přivézt	k5eAaPmAgMnP	přivézt
stříbro	stříbro	k1gNnSc4	stříbro
(	(	kIx(	(
<g/>
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
1951	[number]	k4	1951
<g/>
,	,	kIx,	,
1955	[number]	k4	1955
<g/>
,	,	kIx,	,
1959	[number]	k4	1959
<g/>
,	,	kIx,	,
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejprestižnější	prestižní	k2eAgFnSc4d3	nejprestižnější
evropskou	evropský	k2eAgFnSc4d1	Evropská
pohárovou	pohárový	k2eAgFnSc4d1	pohárová
soutěž	soutěž	k1gFnSc4	soutěž
v	v	k7c6	v
basketbale	basketbal	k1gInSc6	basketbal
žen	žena	k1gFnPc2	žena
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Euroliga	Euroliga	k1gFnSc1	Euroliga
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
Pohár	pohár	k1gInSc1	pohár
mistrů	mistr	k1gMnPc2	mistr
evropských	evropský	k2eAgFnPc2d1	Evropská
zemí	zem	k1gFnPc2	zem
<g/>
)	)	kIx)	)
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
Sparta	Sparta	k1gFnSc1	Sparta
Praha	Praha	k1gFnSc1	Praha
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
/	/	kIx~	/
<g/>
76	[number]	k4	76
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Gambrinus	gambrinus	k1gInSc1	gambrinus
Brno	Brno	k1gNnSc1	Brno
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
a	a	k8xC	a
USK	USK	kA	USK
Praha	Praha	k1gFnSc1	Praha
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
/	/	kIx~	/
<g/>
15	[number]	k4	15
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
analogické	analogický	k2eAgFnSc6d1	analogická
mužské	mužský	k2eAgFnSc6d1	mužská
soutěži	soutěž	k1gFnSc6	soutěž
se	se	k3xPyFc4	se
probojovaly	probojovat	k5eAaPmAgInP	probojovat
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
kluby	klub	k1gInPc7	klub
Spartak	Spartak	k1gInSc1	Spartak
Brno	Brno	k1gNnSc1	Brno
(	(	kIx(	(
<g/>
1963	[number]	k4	1963
<g/>
/	/	kIx~	/
<g/>
64	[number]	k4	64
<g/>
,	,	kIx,	,
1967	[number]	k4	1967
<g/>
/	/	kIx~	/
<g/>
68	[number]	k4	68
<g/>
)	)	kIx)	)
a	a	k8xC	a
Slavia	Slavia	k1gFnSc1	Slavia
VŠ	vš	k0	vš
Praha	Praha	k1gFnSc1	Praha
(	(	kIx(	(
<g/>
1965	[number]	k4	1965
<g/>
/	/	kIx~	/
<g/>
66	[number]	k4	66
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
českým	český	k2eAgMnSc7d1	český
basketbalistou	basketbalista	k1gMnSc7	basketbalista
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
vyhlášen	vyhlášen	k2eAgMnSc1d1	vyhlášen
Jiří	Jiří	k1gMnSc1	Jiří
Zídek	Zídek	k1gMnSc1	Zídek
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Jiří	Jiří	k1gMnSc1	Jiří
Zídek	Zídek	k1gMnSc1	Zídek
mladší	mladý	k2eAgMnSc1d2	mladší
byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgMnSc7	první
českým	český	k2eAgMnSc7d1	český
hráčem	hráč	k1gMnSc7	hráč
v	v	k7c6	v
americké	americký	k2eAgFnSc6d1	americká
NBA	NBA	kA	NBA
<g/>
.	.	kIx.	.
</s>
<s>
Následovali	následovat	k5eAaImAgMnP	následovat
ho	on	k3xPp3gMnSc4	on
Jiří	Jiří	k1gMnSc1	Jiří
Welsch	Welsch	k1gMnSc1	Welsch
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Veselý	Veselý	k1gMnSc1	Veselý
a	a	k8xC	a
nejnověji	nově	k6eAd3	nově
Tomáš	Tomáš	k1gMnSc1	Tomáš
Satoranský	Satoranský	k2eAgMnSc1d1	Satoranský
<g/>
.	.	kIx.	.
</s>
<s>
Hana	Hana	k1gFnSc1	Hana
Horáková	Horáková	k1gFnSc1	Horáková
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
nejlepší	dobrý	k2eAgFnSc7d3	nejlepší
basketbalistkou	basketbalistka	k1gFnSc7	basketbalistka
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
<g/>
Ženská	ženský	k2eAgFnSc1d1	ženská
házenkářská	házenkářský	k2eAgFnSc1d1	házenkářská
reprezentace	reprezentace	k1gFnSc1	reprezentace
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jednou	jednou	k6eAd1	jednou
přivezla	přivézt	k5eAaPmAgFnS	přivézt
stříbro	stříbro	k1gNnSc4	stříbro
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
a	a	k8xC	a
jednou	jednou	k6eAd1	jednou
bronz	bronz	k1gInSc4	bronz
(	(	kIx(	(
<g/>
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Muži	muž	k1gMnPc1	muž
vyhráli	vyhrát	k5eAaPmAgMnP	vyhrát
mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
krom	krom	k7c2	krom
toho	ten	k3xDgNnSc2	ten
mají	mít	k5eAaImIp3nP	mít
dvě	dva	k4xCgNnPc1	dva
stříbra	stříbro	k1gNnPc1	stříbro
(	(	kIx(	(
<g/>
1958	[number]	k4	1958
<g/>
,	,	kIx,	,
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
a	a	k8xC	a
dva	dva	k4xCgInPc4	dva
bronzy	bronz	k1gInPc4	bronz
(	(	kIx(	(
<g/>
1954	[number]	k4	1954
<g/>
,	,	kIx,	,
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Získali	získat	k5eAaPmAgMnP	získat
též	též	k9	též
stříbro	stříbro	k1gNnSc4	stříbro
na	na	k7c6	na
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
<g/>
.	.	kIx.	.
</s>
<s>
Tým	tým	k1gInSc1	tým
mužů	muž	k1gMnPc2	muž
Dukly	Dukla	k1gFnSc2	Dukla
Praha	Praha	k1gFnSc1	Praha
třikrát	třikrát	k6eAd1	třikrát
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
Pohár	pohár	k1gInSc1	pohár
mistrů	mistr	k1gMnPc2	mistr
evropských	evropský	k2eAgFnPc2d1	Evropská
zemí	zem	k1gFnPc2	zem
(	(	kIx(	(
<g/>
1957	[number]	k4	1957
<g/>
,	,	kIx,	,
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ženský	ženský	k2eAgInSc4d1	ženský
tým	tým	k1gInSc4	tým
Sparty	Sparta	k1gFnSc2	Sparta
Praha	Praha	k1gFnSc1	Praha
jednou	jednou	k6eAd1	jednou
(	(	kIx(	(
<g/>
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Filip	Filip	k1gMnSc1	Filip
Jícha	jícha	k1gFnSc1	jícha
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
vyhlášen	vyhlásit	k5eAaPmNgMnS	vyhlásit
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
házenkářem	házenkář	k1gMnSc7	házenkář
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
<g/>
Ve	v	k7c6	v
volejbale	volejbal	k1gInSc6	volejbal
má	mít	k5eAaImIp3nS	mít
mužská	mužský	k2eAgFnSc1d1	mužská
reprezentace	reprezentace	k1gFnSc1	reprezentace
dva	dva	k4xCgInPc1	dva
tituly	titul	k1gInPc1	titul
mistra	mistr	k1gMnSc2	mistr
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
1956	[number]	k4	1956
<g/>
,	,	kIx,	,
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tři	tři	k4xCgInPc1	tři
tituly	titul	k1gInPc1	titul
mistra	mistr	k1gMnSc2	mistr
Evropy	Evropa	k1gFnSc2	Evropa
(	(	kIx(	(
<g/>
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
1955	[number]	k4	1955
<g/>
,	,	kIx,	,
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stříbro	stříbro	k1gNnSc4	stříbro
a	a	k8xC	a
bronz	bronz	k1gInSc4	bronz
z	z	k7c2	z
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
(	(	kIx(	(
<g/>
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ženy	žena	k1gFnPc1	žena
vyhrály	vyhrát	k5eAaPmAgFnP	vyhrát
roku	rok	k1gInSc3	rok
1955	[number]	k4	1955
mistrovství	mistrovství	k1gNnSc2	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
českým	český	k2eAgMnSc7d1	český
volejbalistou	volejbalista	k1gMnSc7	volejbalista
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
vyhlášen	vyhlášen	k2eAgMnSc1d1	vyhlášen
Josef	Josef	k1gMnSc1	Josef
Musil	Musil	k1gMnSc1	Musil
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
rovněž	rovněž	k9	rovněž
prvním	první	k4xOgMnSc7	první
Čechem	Čech	k1gMnSc7	Čech
uvedeným	uvedený	k2eAgInSc7d1	uvedený
do	do	k7c2	do
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
volejbalové	volejbalový	k2eAgFnSc2d1	volejbalová
Síně	síň	k1gFnSc2	síň
slávy	sláva	k1gFnSc2	sláva
<g/>
.	.	kIx.	.
<g/>
Ke	k	k7c3	k
slavným	slavný	k2eAgMnPc3d1	slavný
šachistům	šachista	k1gMnPc3	šachista
patřili	patřit	k5eAaImAgMnP	patřit
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Steinitz	Steinitz	k1gMnSc1	Steinitz
<g/>
,	,	kIx,	,
Richard	Richard	k1gMnSc1	Richard
Réti	Rét	k1gFnSc2	Rét
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Rétiho	Réti	k1gMnSc4	Réti
hra	hra	k1gFnSc1	hra
<g/>
)	)	kIx)	)
či	či	k8xC	či
Věra	Věra	k1gFnSc1	Věra
Menčíková	Menčíková	k1gFnSc1	Menčíková
<g/>
.	.	kIx.	.
</s>
<s>
Českým	český	k2eAgInSc7d1	český
přínosem	přínos	k1gInSc7	přínos
světovému	světový	k2eAgInSc3d1	světový
šachu	šach	k1gInSc3	šach
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
Traxlerův	Traxlerův	k2eAgInSc4d1	Traxlerův
protiútok	protiútok	k1gInSc4	protiútok
<g/>
.	.	kIx.	.
</s>
<s>
Nejúspěšnějším	úspěšný	k2eAgMnSc7d3	nejúspěšnější
českým	český	k2eAgMnSc7d1	český
šachistou	šachista	k1gMnSc7	šachista
současnosti	současnost	k1gFnSc2	současnost
je	být	k5eAaImIp3nS	být
David	David	k1gMnSc1	David
Navara	Navar	k1gMnSc2	Navar
<g/>
.	.	kIx.	.
</s>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Enge	Eng	k1gInSc2	Eng
je	být	k5eAaImIp3nS	být
jediným	jediný	k2eAgMnSc7d1	jediný
českým	český	k2eAgMnSc7d1	český
účastníkem	účastník	k1gMnSc7	účastník
závodů	závod	k1gInPc2	závod
Formule	formule	k1gFnSc2	formule
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
sportech	sport	k1gInPc6	sport
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
v	v	k7c6	v
éře	éra	k1gFnSc6	éra
samostatného	samostatný	k2eAgNnSc2d1	samostatné
Česka	Česko	k1gNnSc2	Česko
výrazné	výrazný	k2eAgFnSc2d1	výrazná
osobnosti	osobnost	k1gFnSc2	osobnost
–	–	k?	–
v	v	k7c6	v
biatlonu	biatlon	k1gInSc6	biatlon
(	(	kIx(	(
<g/>
Gabriela	Gabriela	k1gFnSc1	Gabriela
Koukalová	Koukalová	k1gFnSc1	Koukalová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
judu	judo	k1gNnSc6	judo
(	(	kIx(	(
<g/>
Lukáš	Lukáš	k1gMnSc1	Lukáš
Krpálek	Krpálek	k1gMnSc1	Krpálek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
krasobruslení	krasobruslení	k1gNnSc6	krasobruslení
(	(	kIx(	(
<g/>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Verner	Verner	k1gMnSc1	Verner
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
silniční	silniční	k2eAgFnSc6d1	silniční
cyklistice	cyklistika	k1gFnSc6	cyklistika
(	(	kIx(	(
<g/>
Roman	Roman	k1gMnSc1	Roman
Kreuziger	Kreuziger	k1gMnSc1	Kreuziger
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
horských	horský	k2eAgNnPc6d1	horské
kolech	kolo	k1gNnPc6	kolo
(	(	kIx(	(
<g/>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Kulhavý	kulhavý	k2eAgMnSc1d1	kulhavý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
snowboardkrosu	snowboardkros	k1gInSc6	snowboardkros
(	(	kIx(	(
<g/>
Eva	Eva	k1gFnSc1	Eva
Samková	Samková	k1gFnSc1	Samková
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
veslování	veslování	k1gNnSc1	veslování
(	(	kIx(	(
<g/>
Miroslava	Miroslava	k1gFnSc1	Miroslava
Knapková	Knapková	k1gFnSc1	Knapková
<g/>
,	,	kIx,	,
Ondřej	Ondřej	k1gMnSc1	Ondřej
Synek	Synek	k1gMnSc1	Synek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kanoistice	kanoistika	k1gFnSc6	kanoistika
(	(	kIx(	(
<g/>
Martin	Martin	k1gMnSc1	Martin
Fuksa	Fuksa	k1gMnSc1	Fuksa
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Dostál	Dostál	k1gMnSc1	Dostál
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
klasickém	klasický	k2eAgNnSc6d1	klasické
lyžování	lyžování	k1gNnSc6	lyžování
(	(	kIx(	(
<g/>
Lukáš	Lukáš	k1gMnSc1	Lukáš
Bauer	Bauer	k1gMnSc1	Bauer
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
akrobatickém	akrobatický	k2eAgNnSc6d1	akrobatické
lyžování	lyžování	k1gNnSc6	lyžování
(	(	kIx(	(
<g/>
Aleš	Aleš	k1gMnSc1	Aleš
Valenta	Valenta	k1gMnSc1	Valenta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
střelbě	střelba	k1gFnSc6	střelba
(	(	kIx(	(
<g/>
Kateřina	Kateřina	k1gFnSc1	Kateřina
Emmons	Emmonsa	k1gFnPc2	Emmonsa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
moderním	moderní	k2eAgInSc6d1	moderní
pětiboji	pětiboj	k1gInSc6	pětiboj
(	(	kIx(	(
<g/>
David	David	k1gMnSc1	David
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
alpském	alpský	k2eAgNnSc6d1	alpské
lyžování	lyžování	k1gNnSc6	lyžování
(	(	kIx(	(
<g/>
Šárka	Šárka	k1gFnSc1	Šárka
Záhrobská	Záhrobský	k2eAgFnSc1d1	Záhrobská
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
sportovním	sportovní	k2eAgNnSc6d1	sportovní
lezení	lezení	k1gNnSc1	lezení
(	(	kIx(	(
<g/>
Adam	Adam	k1gMnSc1	Adam
Ondra	Ondra	k1gMnSc1	Ondra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
skikrosu	skikrosa	k1gFnSc4	skikrosa
(	(	kIx(	(
<g/>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Kraus	Kraus	k1gMnSc1	Kraus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dráhové	dráhový	k2eAgFnSc3d1	dráhová
cyklistice	cyklistika	k1gFnSc3	cyklistika
(	(	kIx(	(
<g/>
Lada	Lada	k1gFnSc1	Lada
Kozlíková	kozlíkový	k2eAgFnSc1d1	Kozlíková
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
cyklokrosu	cyklokros	k1gInSc3	cyklokros
(	(	kIx(	(
<g/>
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Štybar	Štybar	k1gMnSc1	Štybar
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
v	v	k7c6	v
poměrně	poměrně	k6eAd1	poměrně
málo	málo	k6eAd1	málo
sledovaných	sledovaný	k2eAgInPc6d1	sledovaný
sportech	sport	k1gInPc6	sport
jako	jako	k9	jako
jsou	být	k5eAaImIp3nP	být
skiboby	skibob	k1gInPc1	skibob
(	(	kIx(	(
<g/>
Alena	Alena	k1gFnSc1	Alena
Housová	Housová	k1gFnSc1	Housová
<g/>
,	,	kIx,	,
Irena	Irena	k1gFnSc1	Irena
Francová-Dohnálková	Francová-Dohnálková	k1gFnSc1	Francová-Dohnálková
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
krasojízda	krasojízda	k1gFnSc1	krasojízda
(	(	kIx(	(
<g/>
Martina	Martina	k1gFnSc1	Martina
Štěpánková	Štěpánková	k1gFnSc1	Štěpánková
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sportovní	sportovní	k2eAgInSc1d1	sportovní
rybolov	rybolov	k1gInSc1	rybolov
(	(	kIx(	(
<g/>
Kateřina	Kateřina	k1gFnSc1	Kateřina
Marková	Marková	k1gFnSc1	Marková
<g/>
)	)	kIx)	)
či	či	k8xC	či
orientační	orientační	k2eAgInSc1d1	orientační
běh	běh	k1gInSc1	běh
(	(	kIx(	(
<g/>
Dana	Dana	k1gFnSc1	Dana
Brožková	Brožková	k1gFnSc1	Brožková
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
éry	éra	k1gFnSc2	éra
Československa	Československo	k1gNnSc2	Československo
lze	lze	k6eAd1	lze
vzpomenout	vzpomenout	k5eAaPmF	vzpomenout
legendy	legenda	k1gFnSc2	legenda
kolové	kolový	k2eAgMnPc4d1	kolový
bratry	bratr	k1gMnPc4	bratr
Pospíšily	Pospíšil	k1gMnPc4	Pospíšil
<g/>
,	,	kIx,	,
skokany	skokan	k1gMnPc4	skokan
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
Jiřího	Jiří	k1gMnSc2	Jiří
Rašku	Raška	k1gMnSc4	Raška
<g/>
,	,	kIx,	,
Pavla	Pavel	k1gMnSc4	Pavel
Ploce	Ploce	k1gMnSc4	Ploce
a	a	k8xC	a
Jiřího	Jiří	k1gMnSc4	Jiří
Parmu	Parma	k1gFnSc4	Parma
<g/>
,	,	kIx,	,
cyklokrosaře	cyklokrosař	k1gMnSc4	cyklokrosař
Radomíra	Radomír	k1gMnSc4	Radomír
Šimůnka	Šimůnek	k1gMnSc4	Šimůnek
či	či	k8xC	či
zápasníka	zápasník	k1gMnSc4	zápasník
Vítězslava	Vítězslav	k1gMnSc4	Vítězslav
Máchu	Mácha	k1gMnSc4	Mácha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Soutěže	soutěž	k1gFnSc2	soutěž
a	a	k8xC	a
sportoviště	sportoviště	k1gNnSc2	sportoviště
===	===	k?	===
</s>
</p>
<p>
<s>
K	k	k7c3	k
největším	veliký	k2eAgFnPc3d3	veliký
pravidelným	pravidelný	k2eAgFnPc3d1	pravidelná
mezinárodním	mezinárodní	k2eAgFnPc3d1	mezinárodní
sportovním	sportovní	k2eAgFnPc3d1	sportovní
akcím	akce	k1gFnPc3	akce
v	v	k7c6	v
ČR	ČR	kA	ČR
patří	patřit	k5eAaImIp3nS	patřit
motocyklová	motocyklový	k2eAgFnSc1d1	motocyklová
Velká	velký	k2eAgFnSc1d1	velká
cena	cena	k1gFnSc1	cena
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
atletická	atletický	k2eAgFnSc1d1	atletická
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
tretra	tretra	k1gFnSc1	tretra
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
<g/>
,	,	kIx,	,
běžecký	běžecký	k2eAgInSc1d1	běžecký
Pražský	pražský	k2eAgInSc1d1	pražský
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
maraton	maraton	k1gInSc1	maraton
<g/>
,	,	kIx,	,
jezdecká	jezdecký	k2eAgFnSc1d1	jezdecká
Velká	velká	k1gFnSc1	velká
pardubická	pardubický	k2eAgFnSc1d1	pardubická
<g/>
,	,	kIx,	,
tenisový	tenisový	k2eAgInSc1d1	tenisový
turnaj	turnaj	k1gInSc1	turnaj
Prague	Prague	k1gInSc1	Prague
Open	Open	k1gInSc4	Open
(	(	kIx(	(
<g/>
navazující	navazující	k2eAgFnPc4d1	navazující
na	na	k7c6	na
Czech	Cze	k1gFnPc6	Cze
Open	Openo	k1gNnPc2	Openo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lyžařská	lyžařský	k2eAgFnSc1d1	lyžařská
Jizerská	jizerský	k2eAgFnSc1d1	Jizerská
padesátka	padesátka	k1gFnSc1	padesátka
<g/>
,	,	kIx,	,
automobilová	automobilový	k2eAgFnSc1d1	automobilová
Barum	barum	k1gInSc4	barum
rallye	rallye	k1gFnPc1	rallye
ve	v	k7c6	v
Zlíně	Zlín	k1gInSc6	Zlín
či	či	k8xC	či
plochodrážní	plochodrážní	k2eAgFnSc1d1	plochodrážní
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
přilba	přilba	k1gFnSc1	přilba
v	v	k7c6	v
Pardubicích	Pardubice	k1gInPc6	Pardubice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
byli	být	k5eAaImAgMnP	být
významnými	významný	k2eAgFnPc7d1	významná
akcemi	akce	k1gFnPc7	akce
i	i	k8xC	i
České	český	k2eAgFnPc4d1	Česká
hokejové	hokejový	k2eAgFnPc4d1	hokejová
hry	hra	k1gFnPc4	hra
či	či	k8xC	či
Závod	závod	k1gInSc4	závod
míru	mír	k1gInSc2	mír
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vodním	vodní	k2eAgInSc6d1	vodní
slalomu	slalom	k1gInSc6	slalom
či	či	k8xC	či
v	v	k7c6	v
biatlonu	biatlon	k1gInSc6	biatlon
se	se	k3xPyFc4	se
v	v	k7c6	v
ČR	ČR	kA	ČR
pravidelně	pravidelně	k6eAd1	pravidelně
konají	konat	k5eAaImIp3nP	konat
závody	závod	k1gInPc1	závod
světového	světový	k2eAgInSc2d1	světový
poháru	pohár	k1gInSc2	pohár
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
domácích	domácí	k2eAgFnPc2d1	domácí
soutěží	soutěž	k1gFnPc2	soutěž
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejsledovanějším	sledovaný	k2eAgFnPc3d3	nejsledovanější
pravidelným	pravidelný	k2eAgFnPc3d1	pravidelná
soutěžím	soutěž	k1gFnPc3	soutěž
1	[number]	k4	1
<g/>
.	.	kIx.	.
česká	český	k2eAgFnSc1d1	Česká
fotbalová	fotbalový	k2eAgFnSc1d1	fotbalová
liga	liga	k1gFnSc1	liga
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Fotbalová	fotbalový	k2eAgFnSc1d1	fotbalová
liga	liga	k1gFnSc1	liga
Československa	Československo	k1gNnSc2	Československo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Česká	český	k2eAgFnSc1d1	Česká
hokejová	hokejový	k2eAgFnSc1d1	hokejová
extraliga	extraliga	k1gFnSc1	extraliga
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Československá	československý	k2eAgFnSc1d1	Československá
hokejová	hokejový	k2eAgFnSc1d1	hokejová
liga	liga	k1gFnSc1	liga
<g/>
)	)	kIx)	)
a	a	k8xC	a
Česká	český	k2eAgFnSc1d1	Česká
basketbalová	basketbalový	k2eAgFnSc1d1	basketbalová
liga	liga	k1gFnSc1	liga
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Československá	československý	k2eAgFnSc1d1	Československá
basketbalová	basketbalový	k2eAgFnSc1d1	basketbalová
liga	liga	k1gFnSc1	liga
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
ČR	ČR	kA	ČR
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
i	i	k9	i
řada	řada	k1gFnSc1	řada
jednorázových	jednorázový	k2eAgFnPc2d1	jednorázová
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
sportovních	sportovní	k2eAgFnPc2d1	sportovní
akcí	akce	k1gFnPc2	akce
<g/>
,	,	kIx,	,
kupříkladu	kupříkladu	k6eAd1	kupříkladu
na	na	k7c6	na
pražském	pražský	k2eAgInSc6d1	pražský
stadionu	stadion	k1gInSc6	stadion
v	v	k7c6	v
Edenu	Eden	k1gInSc6	Eden
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
hrálo	hrát	k5eAaImAgNnS	hrát
finále	finále	k1gNnSc1	finále
evropského	evropský	k2eAgInSc2d1	evropský
Superpoháru	superpohár	k1gInSc2	superpohár
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
se	se	k3xPyFc4	se
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
(	(	kIx(	(
<g/>
Eden	Eden	k1gInSc1	Eden
a	a	k8xC	a
Letná	Letná	k1gFnSc1	Letná
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
(	(	kIx(	(
<g/>
Andrův	Andrův	k2eAgInSc1d1	Andrův
stadion	stadion	k1gInSc1	stadion
<g/>
)	)	kIx)	)
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
v	v	k7c6	v
Uherském	uherský	k2eAgNnSc6d1	Uherské
Hradišti	Hradiště	k1gNnSc6	Hradiště
(	(	kIx(	(
<g/>
Stadion	stadion	k1gInSc1	stadion
Miroslava	Miroslav	k1gMnSc2	Miroslav
Valenty	Valenta	k1gMnSc2	Valenta
<g/>
)	)	kIx)	)
konalo	konat	k5eAaImAgNnS	konat
Mistrovství	mistrovství	k1gNnSc1	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
hráčů	hráč	k1gMnPc2	hráč
do	do	k7c2	do
21	[number]	k4	21
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
desetkrát	desetkrát	k6eAd1	desetkrát
se	se	k3xPyFc4	se
v	v	k7c6	v
ČR	ČR	kA	ČR
konalo	konat	k5eAaImAgNnS	konat
mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
ledním	lední	k2eAgInSc6d1	lední
hokeji	hokej	k1gInSc6	hokej
(	(	kIx(	(
<g/>
1933	[number]	k4	1933
<g/>
,	,	kIx,	,
1938	[number]	k4	1938
<g/>
,	,	kIx,	,
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
1959	[number]	k4	1959
<g/>
,	,	kIx,	,
1972	[number]	k4	1972
<g/>
,	,	kIx,	,
1978	[number]	k4	1978
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
1985	[number]	k4	1985
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
několikrát	několikrát	k6eAd1	několikrát
též	též	k9	též
mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
a	a	k8xC	a
Evropy	Evropa	k1gFnSc2	Evropa
v	v	k7c6	v
basketbale	basketbal	k1gInSc6	basketbal
mužů	muž	k1gMnPc2	muž
(	(	kIx(	(
<g/>
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
i	i	k8xC	i
žen	žena	k1gFnPc2	žena
(	(	kIx(	(
<g/>
1956	[number]	k4	1956
<g/>
,	,	kIx,	,
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
házené	házená	k1gFnSc6	házená
mužů	muž	k1gMnPc2	muž
(	(	kIx(	(
<g/>
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
i	i	k8xC	i
žen	žena	k1gFnPc2	žena
(	(	kIx(	(
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
a	a	k8xC	a
Evropy	Evropa	k1gFnSc2	Evropa
ve	v	k7c6	v
volejbale	volejbal	k1gInSc6	volejbal
mužů	muž	k1gMnPc2	muž
(	(	kIx(	(
<g/>
1949	[number]	k4	1949
<g/>
,	,	kIx,	,
1958	[number]	k4	1958
<g/>
,	,	kIx,	,
1966	[number]	k4	1966
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
i	i	k8xC	i
žen	žena	k1gFnPc2	žena
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1949	[number]	k4	1949
<g/>
,	,	kIx,	,
1958	[number]	k4	1958
<g/>
,	,	kIx,	,
1986	[number]	k4	1986
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mistrovství	mistrovství	k1gNnSc1	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
v	v	k7c6	v
atletice	atletika	k1gFnSc6	atletika
1978	[number]	k4	1978
a	a	k8xC	a
dvě	dva	k4xCgNnPc4	dva
halová	halový	k2eAgNnPc4d1	halové
atletická	atletický	k2eAgNnPc4d1	atletické
mistrovství	mistrovství	k1gNnPc4	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
(	(	kIx(	(
<g/>
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
klasickém	klasický	k2eAgNnSc6d1	klasické
lyžování	lyžování	k1gNnSc6	lyžování
2009	[number]	k4	2009
v	v	k7c6	v
Liberci	Liberec	k1gInSc6	Liberec
<g/>
,	,	kIx,	,
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
biatlonu	biatlon	k1gInSc6	biatlon
2013	[number]	k4	2013
<g />
.	.	kIx.	.
</s>
<s>
v	v	k7c6	v
Novém	nový	k2eAgNnSc6d1	nové
Městě	město	k1gNnSc6	město
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
,	,	kIx,	,
dvě	dva	k4xCgFnPc1	dva
zimní	zimní	k2eAgFnPc1d1	zimní
univerziády	univerziáda	k1gFnPc1	univerziáda
(	(	kIx(	(
<g/>
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
deset	deset	k4xCc1	deset
mistrovství	mistrovství	k1gNnPc2	mistrovství
světa	svět	k1gInSc2	svět
či	či	k8xC	či
Evropy	Evropa	k1gFnSc2	Evropa
v	v	k7c6	v
krasobruslení	krasobruslení	k1gNnSc6	krasobruslení
(	(	kIx(	(
<g/>
1908	[number]	k4	1908
<g/>
,	,	kIx,	,
1928	[number]	k4	1928
<g/>
,	,	kIx,	,
1934	[number]	k4	1934
<g/>
,	,	kIx,	,
1937	[number]	k4	1937
<g/>
,	,	kIx,	,
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
florbale	florbal	k1gInSc5	florbal
1998	[number]	k4	1998
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1981	[number]	k4	1981
se	se	k3xPyFc4	se
na	na	k7c6	na
brněnském	brněnský	k2eAgInSc6d1	brněnský
velodromu	velodrom	k1gInSc6	velodrom
konalo	konat	k5eAaImAgNnS	konat
kupříkladu	kupříkladu	k6eAd1	kupříkladu
i	i	k9	i
mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
dráhové	dráhový	k2eAgFnSc6d1	dráhová
cyklistice	cyklistika	k1gFnSc6	cyklistika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
nejmodernějším	moderní	k2eAgNnPc3d3	nejmodernější
sportovištím	sportoviště	k1gNnPc3	sportoviště
patří	patřit	k5eAaImIp3nS	patřit
fotbalové	fotbalový	k2eAgFnSc2d1	fotbalová
stánky	stánka	k1gFnSc2	stánka
Eden	Eden	k1gInSc1	Eden
a	a	k8xC	a
Letná	Letná	k1gFnSc1	Letná
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
stadion	stadion	k1gInSc4	stadion
ve	v	k7c6	v
Štruncových	Štruncových	k2eAgInPc6d1	Štruncových
sadech	sad	k1gInPc6	sad
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
<g/>
,	,	kIx,	,
víceúčelové	víceúčelový	k2eAgFnPc4d1	víceúčelová
haly	hala	k1gFnPc4	hala
O2	O2	k1gMnSc2	O2
Arena	Aren	k1gMnSc2	Aren
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
Ostravar	Ostravar	k1gInSc1	Ostravar
Aréna	aréna	k1gFnSc1	aréna
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
<g/>
,	,	kIx,	,
KV	KV	kA	KV
Arena	Aren	k1gInSc2	Aren
v	v	k7c6	v
Karlových	Karlův	k2eAgInPc6d1	Karlův
Varech	Vary	k1gInPc6	Vary
a	a	k8xC	a
Home	Home	k1gFnPc6	Home
Credit	Credit	k1gFnPc2	Credit
Arena	Aren	k1gInSc2	Aren
v	v	k7c6	v
Liberci	Liberec	k1gInSc6	Liberec
<g/>
,	,	kIx,	,
atletický	atletický	k2eAgInSc1d1	atletický
Městský	městský	k2eAgInSc1d1	městský
stadion	stadion	k1gInSc1	stadion
v	v	k7c6	v
Ostravě-Vítkovicích	Ostravě-Vítkovice	k1gFnPc6	Ostravě-Vítkovice
<g/>
,	,	kIx,	,
Masarykův	Masarykův	k2eAgInSc1d1	Masarykův
okruh	okruh	k1gInSc1	okruh
pro	pro	k7c4	pro
motocyklové	motocyklový	k2eAgInPc4d1	motocyklový
závody	závod	k1gInPc4	závod
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
či	či	k8xC	či
můstky	můstek	k1gInPc4	můstek
na	na	k7c6	na
Ještědu	Ještěd	k1gInSc6	Ještěd
<g/>
.	.	kIx.	.
</s>
<s>
Historicky	historicky	k6eAd1	historicky
významnými	významný	k2eAgFnPc7d1	významná
jsou	být	k5eAaImIp3nP	být
též	též	k9	též
Velký	velký	k2eAgInSc4d1	velký
strahovský	strahovský	k2eAgInSc4d1	strahovský
stadion	stadion	k1gInSc4	stadion
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yRgNnSc6	který
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgInP	konat
mj.	mj.	kA	mj.
Sokolské	sokolský	k2eAgInPc1d1	sokolský
slety	slet	k1gInPc1	slet
a	a	k8xC	a
který	který	k3yIgInSc1	který
býval	bývat	k5eAaImAgInS	bývat
uváděn	uvádět	k5eAaImNgInS	uvádět
jako	jako	k8xC	jako
největší	veliký	k2eAgInSc1d3	veliký
sportovní	sportovní	k2eAgInSc1d1	sportovní
stadion	stadion	k1gInSc1	stadion
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
pražská	pražský	k2eAgFnSc1d1	Pražská
Sportovní	sportovní	k2eAgFnSc1d1	sportovní
hala	hala	k1gFnSc1	hala
v	v	k7c6	v
Holešovicích	Holešovice	k1gFnPc6	Holešovice
<g/>
,	,	kIx,	,
lední	lední	k2eAgInSc1d1	lední
stadion	stadion	k1gInSc1	stadion
na	na	k7c4	na
Štvanici	Štvanice	k1gFnSc4	Štvanice
<g/>
,	,	kIx,	,
tenisový	tenisový	k2eAgInSc4d1	tenisový
stadion	stadion	k1gInSc4	stadion
na	na	k7c4	na
Štvanici	Štvanice	k1gFnSc4	Štvanice
<g/>
,	,	kIx,	,
Stadion	stadion	k1gNnSc4	stadion
Evžena	Evžen	k1gMnSc2	Evžen
Rošického	Rošický	k2eAgMnSc2d1	Rošický
na	na	k7c6	na
Strahově	Strahov	k1gInSc6	Strahov
či	či	k8xC	či
můstky	můstek	k1gInPc4	můstek
Čerťák	Čerťák	k1gInSc1	Čerťák
v	v	k7c6	v
Harrachově	Harrachov	k1gInSc6	Harrachov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Organizačně	organizačně	k6eAd1	organizačně
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
špici	špice	k1gFnSc6	špice
českého	český	k2eAgInSc2d1	český
sportu	sport	k1gInSc2	sport
Česká	český	k2eAgFnSc1d1	Česká
unie	unie	k1gFnSc1	unie
sportu	sport	k1gInSc2	sport
a	a	k8xC	a
Český	český	k2eAgInSc1d1	český
olympijský	olympijský	k2eAgInSc1d1	olympijský
výbor	výbor	k1gInSc1	výbor
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
největším	veliký	k2eAgInPc3d3	veliký
sportovním	sportovní	k2eAgInPc3d1	sportovní
svazům	svaz	k1gInPc3	svaz
patří	patřit	k5eAaImIp3nS	patřit
Fotbalová	fotbalový	k2eAgFnSc1d1	fotbalová
asociace	asociace	k1gFnSc1	asociace
ČR	ČR	kA	ČR
a	a	k8xC	a
Český	český	k2eAgInSc1d1	český
svaz	svaz	k1gInSc1	svaz
ledního	lední	k2eAgInSc2d1	lední
hokeje	hokej	k1gInSc2	hokej
<g/>
.	.	kIx.	.
</s>
<s>
Nejpopulárnější	populární	k2eAgFnSc7d3	nejpopulárnější
všesportovní	všesportovní	k2eAgFnSc7d1	všesportovní
anketou	anketa	k1gFnSc7	anketa
je	být	k5eAaImIp3nS	být
Sportovec	sportovec	k1gMnSc1	sportovec
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
významnými	významný	k2eAgFnPc7d1	významná
oborovými	oborový	k2eAgFnPc7d1	oborová
anketami	anketa	k1gFnPc7	anketa
jsou	být	k5eAaImIp3nP	být
Fotbalista	fotbalista	k1gMnSc1	fotbalista
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
hokejka	hokejka	k1gFnSc1	hokejka
<g/>
,	,	kIx,	,
Atlet	atlet	k1gMnSc1	atlet
roku	rok	k1gInSc2	rok
či	či	k8xC	či
Zlatý	zlatý	k2eAgInSc4d1	zlatý
kanár	kanár	k1gInSc4	kanár
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Tělovýchova	tělovýchova	k1gFnSc1	tělovýchova
===	===	k?	===
</s>
</p>
<p>
<s>
Počátky	počátek	k1gInPc1	počátek
organizované	organizovaný	k2eAgFnSc2d1	organizovaná
české	český	k2eAgFnSc2d1	Česká
tělovýchovy	tělovýchova	k1gFnSc2	tělovýchova
sahají	sahat	k5eAaImIp3nP	sahat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1862	[number]	k4	1862
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Miroslav	Miroslav	k1gMnSc1	Miroslav
Tyrš	Tyrš	k1gMnSc1	Tyrš
<g/>
,	,	kIx,	,
Jindřich	Jindřich	k1gMnSc1	Jindřich
Fügner	Fügner	k1gMnSc1	Fügner
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
založili	založit	k5eAaPmAgMnP	založit
spolek	spolek	k1gInSc4	spolek
Sokol	Sokol	k1gMnSc1	Sokol
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
rozrostl	rozrůst	k5eAaPmAgInS	rozrůst
v	v	k7c4	v
důležitou	důležitý	k2eAgFnSc4d1	důležitá
národní	národní	k2eAgFnSc4d1	národní
organizaci	organizace	k1gFnSc4	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
něho	on	k3xPp3gMnSc2	on
se	se	k3xPyFc4	se
časem	časem	k6eAd1	časem
odštěpily	odštěpit	k5eAaPmAgInP	odštěpit
levicová	levicový	k2eAgFnSc1d1	levicová
Dělnická	dělnický	k2eAgFnSc1d1	Dělnická
tělocvičná	tělocvičný	k2eAgFnSc1d1	Tělocvičná
jednota	jednota	k1gFnSc1	jednota
a	a	k8xC	a
katolický	katolický	k2eAgMnSc1d1	katolický
Orel	Orel	k1gMnSc1	Orel
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
organizace	organizace	k1gFnPc1	organizace
pořádaly	pořádat	k5eAaImAgFnP	pořádat
pravidelná	pravidelný	k2eAgNnPc1d1	pravidelné
masová	masový	k2eAgNnPc1d1	masové
cvičení	cvičení	k1gNnPc1	cvičení
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc7d3	veliký
celonárodní	celonárodní	k2eAgFnSc7d1	celonárodní
akcí	akce	k1gFnSc7	akce
v	v	k7c6	v
době	doba	k1gFnSc6	doba
před	před	k7c7	před
převzetím	převzetí	k1gNnSc7	převzetí
moci	moc	k1gFnSc2	moc
komunisty	komunista	k1gMnPc7	komunista
byly	být	k5eAaImAgFnP	být
Sokolem	Sokol	k1gMnSc7	Sokol
pořádané	pořádaný	k2eAgInPc4d1	pořádaný
Všesokolské	všesokolský	k2eAgInPc4d1	všesokolský
slety	slet	k1gInPc4	slet
na	na	k7c6	na
Strahovském	strahovský	k2eAgInSc6d1	strahovský
stadionu	stadion	k1gInSc6	stadion
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
éře	éra	k1gFnSc6	éra
socialismu	socialismus	k1gInSc2	socialismus
navázaly	navázat	k5eAaPmAgInP	navázat
na	na	k7c4	na
ně	on	k3xPp3gFnPc4	on
tzv.	tzv.	kA	tzv.
spartakiády	spartakiáda	k1gFnPc1	spartakiáda
<g/>
,	,	kIx,	,
pořádané	pořádaný	k2eAgFnPc1d1	pořádaná
státu	stát	k1gInSc3	stát
podřízeným	podřízený	k2eAgInSc7d1	podřízený
Československým	československý	k2eAgInSc7d1	československý
svazem	svaz	k1gInSc7	svaz
tělesné	tělesný	k2eAgFnSc2d1	tělesná
výchovy	výchova	k1gFnSc2	výchova
(	(	kIx(	(
<g/>
ČSTV	ČSTV	kA	ČSTV
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velkou	velký	k2eAgFnSc4d1	velká
tradici	tradice	k1gFnSc4	tradice
má	mít	k5eAaImIp3nS	mít
česká	český	k2eAgFnSc1d1	Česká
turistika	turistika	k1gFnSc1	turistika
<g/>
.	.	kIx.	.
</s>
<s>
Klub	klub	k1gInSc1	klub
českých	český	k2eAgMnPc2d1	český
turistů	turist	k1gMnPc2	turist
<g/>
,	,	kIx,	,
založený	založený	k2eAgInSc4d1	založený
Vojtou	Vojta	k1gMnSc7	Vojta
Náprstkem	náprstek	k1gInSc7	náprstek
<g/>
,	,	kIx,	,
pečuje	pečovat	k5eAaImIp3nS	pečovat
o	o	k7c4	o
unikátní	unikátní	k2eAgFnSc4d1	unikátní
síť	síť	k1gFnSc4	síť
turistických	turistický	k2eAgFnPc2d1	turistická
značek	značka	k1gFnPc2	značka
<g/>
,	,	kIx,	,
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejhustších	hustý	k2eAgFnPc2d3	nejhustší
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInPc1	její
počátky	počátek	k1gInPc1	počátek
sahají	sahat	k5eAaImIp3nP	sahat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1889	[number]	k4	1889
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc4d1	velká
společenskou	společenský	k2eAgFnSc4d1	společenská
roli	role	k1gFnSc4	role
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
sehrál	sehrát	k5eAaPmAgMnS	sehrát
český	český	k2eAgInSc4d1	český
tramping	tramping	k1gInSc4	tramping
<g/>
.	.	kIx.	.
</s>
<s>
Populární	populární	k2eAgMnSc1d1	populární
je	být	k5eAaImIp3nS	být
také	také	k9	také
požární	požární	k2eAgInSc1d1	požární
sport	sport	k1gInSc1	sport
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Průkopníky	průkopník	k1gMnPc4	průkopník
moderního	moderní	k2eAgInSc2d1	moderní
sportu	sport	k1gInSc2	sport
a	a	k8xC	a
olympismu	olympismus	k1gInSc2	olympismus
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
byli	být	k5eAaImAgMnP	být
Jiří	Jiří	k1gMnSc1	Jiří
Guth-Jarkovský	Guth-Jarkovský	k2eAgMnSc1d1	Guth-Jarkovský
a	a	k8xC	a
Josef	Josef	k1gMnSc1	Josef
Rössler-Ořovský	Rössler-Ořovský	k1gMnSc1	Rössler-Ořovský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Další	další	k2eAgFnPc1d1	další
charakteristiky	charakteristika	k1gFnPc1	charakteristika
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Kuchyně	kuchyně	k1gFnSc2	kuchyně
===	===	k?	===
</s>
</p>
<p>
<s>
Česká	český	k2eAgFnSc1d1	Česká
kuchyně	kuchyně	k1gFnSc1	kuchyně
je	být	k5eAaImIp3nS	být
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
polohou	poloha	k1gFnSc7	poloha
Česka	Česko	k1gNnSc2	Česko
v	v	k7c6	v
průsečíku	průsečík	k1gInSc6	průsečík
západních	západní	k2eAgInPc2d1	západní
a	a	k8xC	a
východních	východní	k2eAgInPc2d1	východní
vlivů	vliv	k1gInPc2	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Půldruhého	půldruhý	k2eAgNnSc2d1	půldruhé
století	století	k1gNnSc2	století
ovlivňovala	ovlivňovat	k5eAaImAgFnS	ovlivňovat
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
ovlivňována	ovlivňovat	k5eAaImNgFnS	ovlivňovat
kuchyní	kuchyně	k1gFnSc7	kuchyně
kulturně	kulturně	k6eAd1	kulturně
i	i	k9	i
jazykově	jazykově	k6eAd1	jazykově
blízkého	blízký	k2eAgNnSc2d1	blízké
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mnohem	mnohem	k6eAd1	mnohem
déle	dlouho	k6eAd2	dlouho
trvala	trvat	k5eAaImAgFnS	trvat
interakce	interakce	k1gFnSc1	interakce
s	s	k7c7	s
německou	německý	k2eAgFnSc7d1	německá
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
pak	pak	k6eAd1	pak
bavorskou	bavorský	k2eAgFnSc7d1	bavorská
<g/>
)	)	kIx)	)
a	a	k8xC	a
rakouskou	rakouský	k2eAgFnSc7d1	rakouská
kuchyní	kuchyně	k1gFnSc7	kuchyně
<g/>
,	,	kIx,	,
od	od	k7c2	od
kterých	který	k3yRgFnPc2	který
různé	různý	k2eAgInPc4d1	různý
pokrmy	pokrm	k1gInPc4	pokrm
přijímala	přijímat	k5eAaImAgNnP	přijímat
(	(	kIx(	(
<g/>
vídeňský	vídeňský	k2eAgInSc4d1	vídeňský
řízek	řízek	k1gInSc4	řízek
<g/>
,	,	kIx,	,
vepřo-knedlo-zelo	vepřonedloet	k5eAaPmAgNnS	vepřo-knedlo-zet
<g/>
,	,	kIx,	,
bramboračka	bramboračka	k1gFnSc1	bramboračka
<g/>
,	,	kIx,	,
štrůdl	štrůdl	k1gInSc1	štrůdl
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
tyto	tento	k3xDgFnPc4	tento
kuchyně	kuchyně	k1gFnPc4	kuchyně
naopak	naopak	k6eAd1	naopak
inspirovala	inspirovat	k5eAaBmAgFnS	inspirovat
(	(	kIx(	(
<g/>
povidla	povidla	k1gNnPc1	povidla
<g/>
,	,	kIx,	,
koláče	koláč	k1gInPc1	koláč
<g/>
,	,	kIx,	,
svíčková	svíčková	k1gFnSc1	svíčková
na	na	k7c6	na
smetaně	smetana	k1gFnSc6	smetana
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obecně	obecně	k6eAd1	obecně
česká	český	k2eAgNnPc1d1	české
jídla	jídlo	k1gNnPc1	jídlo
zapadají	zapadat	k5eAaPmIp3nP	zapadat
do	do	k7c2	do
středoevropských	středoevropský	k2eAgFnPc2d1	středoevropská
chutí	chuť	k1gFnPc2	chuť
<g/>
,	,	kIx,	,
s	s	k7c7	s
některými	některý	k3yIgFnPc7	některý
místními	místní	k2eAgFnPc7d1	místní
originalitami	originalita	k1gFnPc7	originalita
–	–	k?	–
krom	krom	k7c2	krom
už	už	k6eAd1	už
uvedených	uvedený	k2eAgInPc2d1	uvedený
např.	např.	kA	např.
smažený	smažený	k2eAgInSc1d1	smažený
sýr	sýr	k1gInSc1	sýr
<g/>
,	,	kIx,	,
knedlíky	knedlík	k1gInPc1	knedlík
<g/>
,	,	kIx,	,
utopenci	utopenec	k1gMnPc1	utopenec
<g/>
,	,	kIx,	,
olomoucké	olomoucký	k2eAgInPc1d1	olomoucký
syrečky	syreček	k1gInPc1	syreček
<g/>
,	,	kIx,	,
vánoční	vánoční	k2eAgMnSc1d1	vánoční
kapr	kapr	k1gMnSc1	kapr
(	(	kIx(	(
<g/>
Český	český	k2eAgMnSc1d1	český
kapr	kapr	k1gMnSc1	kapr
je	být	k5eAaImIp3nS	být
obchodní	obchodní	k2eAgFnSc7d1	obchodní
značkou	značka	k1gFnSc7	značka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hořické	hořický	k2eAgFnPc1d1	Hořická
trubičky	trubička	k1gFnPc1	trubička
<g/>
,	,	kIx,	,
škubánky	škubánek	k1gInPc1	škubánek
<g/>
,	,	kIx,	,
pivní	pivní	k2eAgInSc1d1	pivní
sýr	sýr	k1gInSc1	sýr
atd.	atd.	kA	atd.
Východní	východní	k2eAgInPc1d1	východní
vlivy	vliv	k1gInPc1	vliv
(	(	kIx(	(
<g/>
slovenské	slovenský	k2eAgFnPc1d1	slovenská
<g/>
,	,	kIx,	,
maďarské	maďarský	k2eAgFnPc1d1	maďarská
<g/>
,	,	kIx,	,
polské	polský	k2eAgFnPc1d1	polská
či	či	k8xC	či
ruské	ruský	k2eAgFnPc1d1	ruská
<g/>
)	)	kIx)	)
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
kuchyni	kuchyně	k1gFnSc6	kuchyně
zastupují	zastupovat	k5eAaImIp3nP	zastupovat
např.	např.	kA	např.
boršč	boršč	k1gInSc1	boršč
<g/>
,	,	kIx,	,
trdelník	trdelník	k1gMnSc1	trdelník
<g/>
,	,	kIx,	,
živáňská	živáňský	k2eAgFnSc1d1	živáňská
pečeně	pečeně	k1gFnSc1	pečeně
<g/>
,	,	kIx,	,
guláš	guláš	k1gInSc1	guláš
anebo	anebo	k8xC	anebo
tlačenka	tlačenka	k1gFnSc1	tlačenka
<g/>
.	.	kIx.	.
</s>
<s>
Ryby	Ryby	k1gFnPc1	Ryby
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
jídelníčku	jídelníček	k1gInSc6	jídelníček
málo	málo	k6eAd1	málo
časté	častý	k2eAgInPc1d1	častý
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
mořské	mořský	k2eAgInPc1d1	mořský
plody	plod	k1gInPc1	plod
se	se	k3xPyFc4	se
rozmohly	rozmoct	k5eAaPmAgInP	rozmoct
až	až	k9	až
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
moderním	moderní	k2eAgNnSc7d1	moderní
stravováním	stravování	k1gNnSc7	stravování
a	a	k8xC	a
zásobováním	zásobování	k1gNnSc7	zásobování
potravinami	potravina	k1gFnPc7	potravina
z	z	k7c2	z
dovozu	dovoz	k1gInSc2	dovoz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
<g/>
,	,	kIx,	,
v	v	k7c6	v
evropském	evropský	k2eAgInSc6d1	evropský
kontextu	kontext	k1gInSc6	kontext
mimořádné	mimořádný	k2eAgNnSc4d1	mimořádné
je	být	k5eAaImIp3nS	být
použití	použití	k1gNnSc4	použití
jedlých	jedlý	k2eAgFnPc2d1	jedlá
hub	houba	k1gFnPc2	houba
a	a	k8xC	a
mletého	mletý	k2eAgInSc2d1	mletý
máku	mák	k1gInSc2	mák
<g/>
.	.	kIx.	.
</s>
<s>
Tvary	tvar	k1gInPc1	tvar
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
pečiva	pečivo	k1gNnSc2	pečivo
–	–	k?	–
např.	např.	kA	např.
rohlíků	rohlík	k1gInPc2	rohlík
–	–	k?	–
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
)	)	kIx)	)
neobvyklé	obvyklý	k2eNgInPc4d1	neobvyklý
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
samotné	samotný	k2eAgNnSc1d1	samotné
těsto	těsto	k1gNnSc1	těsto
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgInSc2	jenž
jsou	být	k5eAaImIp3nP	být
vyráběny	vyráběn	k2eAgInPc1d1	vyráběn
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
běžné	běžný	k2eAgNnSc1d1	běžné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
podobu	podoba	k1gFnSc4	podoba
novověké	novověký	k2eAgFnSc2d1	novověká
české	český	k2eAgFnSc2d1	Česká
kuchyně	kuchyně	k1gFnSc2	kuchyně
měla	mít	k5eAaImAgFnS	mít
zásadní	zásadní	k2eAgInSc4d1	zásadní
vliv	vliv	k1gInSc4	vliv
Magdalena	Magdalena	k1gFnSc1	Magdalena
Dobromila	Dobromila	k1gFnSc1	Dobromila
Rettigová	Rettigový	k2eAgFnSc1d1	Rettigová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
českých	český	k2eAgInPc2d1	český
nápojů	nápoj	k1gInPc2	nápoj
vyniká	vynikat	k5eAaImIp3nS	vynikat
pivo	pivo	k1gNnSc1	pivo
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc1	jehož
vaření	vaření	k1gNnSc1	vaření
tu	tu	k6eAd1	tu
má	mít	k5eAaImIp3nS	mít
staletou	staletý	k2eAgFnSc4d1	staletá
tradici	tradice	k1gFnSc4	tradice
a	a	k8xC	a
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgInPc4d3	nejznámější
a	a	k8xC	a
nejkvalitnější	kvalitní	k2eAgInPc4d3	nejkvalitnější
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
EU	EU	kA	EU
je	být	k5eAaImIp3nS	být
název	název	k1gInSc4	název
České	český	k2eAgNnSc1d1	české
pivo	pivo	k1gNnSc1	pivo
chráněným	chráněný	k2eAgNnSc7d1	chráněné
zeměpisným	zeměpisný	k2eAgNnSc7d1	zeměpisné
označením	označení	k1gNnSc7	označení
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
První	první	k4xOgInSc1	první
známý	známý	k2eAgInSc1d1	známý
pivovar	pivovar	k1gInSc1	pivovar
tu	tu	k6eAd1	tu
existoval	existovat	k5eAaImAgInS	existovat
už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1118	[number]	k4	1118
a	a	k8xC	a
Česko	Česko	k1gNnSc1	Česko
má	mít	k5eAaImIp3nS	mít
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
spotřebu	spotřeba	k1gFnSc4	spotřeba
piva	pivo	k1gNnSc2	pivo
na	na	k7c4	na
osobu	osoba	k1gFnSc4	osoba
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Vysoce	vysoce	k6eAd1	vysoce
alkoholická	alkoholický	k2eAgFnSc1d1	alkoholická
slivovice	slivovice	k1gFnSc1	slivovice
<g/>
,	,	kIx,	,
pálená	pálený	k2eAgFnSc1d1	pálená
hlavně	hlavně	k9	hlavně
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
Moravě	Morava	k1gFnSc6	Morava
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
běžně	běžně	k6eAd1	běžně
známa	znám	k2eAgFnSc1d1	známa
i	i	k9	i
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
státech	stát	k1gInPc6	stát
východní	východní	k2eAgFnSc2d1	východní
a	a	k8xC	a
jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
zato	zato	k6eAd1	zato
bylinné	bylinný	k2eAgInPc4d1	bylinný
likéry	likér	k1gInPc4	likér
Fernet	fernet	k1gInSc1	fernet
Stock	Stock	k1gMnSc1	Stock
a	a	k8xC	a
Becherovka	becherovka	k1gFnSc1	becherovka
jsou	být	k5eAaImIp3nP	být
českými	český	k2eAgFnPc7d1	Česká
specialitami	specialita	k1gFnPc7	specialita
<g/>
.	.	kIx.	.
</s>
<s>
Víno	víno	k1gNnSc1	víno
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
především	především	k9	především
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
Moravě	Morava	k1gFnSc6	Morava
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
k	k	k7c3	k
pěstování	pěstování	k1gNnSc3	pěstování
révy	réva	k1gFnSc2	réva
podstatně	podstatně	k6eAd1	podstatně
lepší	dobrý	k2eAgFnPc4d2	lepší
podmínky	podmínka	k1gFnPc4	podmínka
než	než	k8xS	než
jiné	jiný	k2eAgFnPc4d1	jiná
části	část	k1gFnPc4	část
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
byly	být	k5eAaImAgInP	být
tu	tu	k6eAd1	tu
i	i	k9	i
vyšlechtěny	vyšlechtěn	k2eAgFnPc4d1	vyšlechtěna
domácí	domácí	k2eAgFnPc4d1	domácí
odrůdy	odrůda	k1gFnPc4	odrůda
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Cabernet	Cabernet	k1gInSc1	Cabernet
Moravia	Moravia	k1gFnSc1	Moravia
<g/>
,	,	kIx,	,
Pálava	Pálava	k1gFnSc1	Pálava
nebo	nebo	k8xC	nebo
Muškát	muškát	k1gInSc1	muškát
moravský	moravský	k2eAgInSc1d1	moravský
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kofeinová	kofeinový	k2eAgFnSc1d1	kofeinová
limonáda	limonáda	k1gFnSc1	limonáda
Kofola	Kofola	k1gFnSc1	Kofola
je	být	k5eAaImIp3nS	být
další	další	k2eAgFnSc7d1	další
českou	český	k2eAgFnSc7d1	Česká
specialitou	specialita	k1gFnSc7	specialita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Svátky	svátek	k1gInPc4	svátek
===	===	k?	===
</s>
</p>
<p>
<s>
Dny	den	k1gInPc1	den
pracovního	pracovní	k2eAgInSc2d1	pracovní
klidu	klid	k1gInSc2	klid
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
svátky	svátek	k1gInPc1	svátek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
vážou	vázat	k5eAaImIp3nP	vázat
k	k	k7c3	k
historickým	historický	k2eAgFnPc3d1	historická
událostem	událost	k1gFnPc3	událost
nebo	nebo	k8xC	nebo
tradicím	tradice	k1gFnPc3	tradice
a	a	k8xC	a
státní	státní	k2eAgInPc4d1	státní
svátky	svátek	k1gInPc4	svátek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
váží	vážit	k5eAaImIp3nP	vážit
k	k	k7c3	k
české	český	k2eAgFnSc3d1	Česká
státnosti	státnost	k1gFnSc3	státnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1	[number]	k4	1
Označuje	označovat	k5eAaImIp3nS	označovat
tzv.	tzv.	kA	tzv.
ostatní	ostatní	k2eAgInPc4d1	ostatní
svátky	svátek	k1gInPc4	svátek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
nesouvisejí	souviset	k5eNaImIp3nP	souviset
s	s	k7c7	s
českou	český	k2eAgFnSc7d1	Česká
státností	státnost	k1gFnSc7	státnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
hlav	hlava	k1gFnPc2	hlava
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
</s>
</p>
<p>
<s>
Světové	světový	k2eAgNnSc1d1	světové
dědictví	dědictví	k1gNnSc1	dědictví
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Česko	Česko	k1gNnSc1	Česko
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
český	český	k2eAgMnSc1d1	český
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Kategorie	kategorie	k1gFnSc1	kategorie
Česko	Česko	k1gNnSc1	Česko
ve	v	k7c6	v
Wikizprávách	Wikizpráva	k1gFnPc6	Wikizpráva
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Česko	Česko	k1gNnSc4	Česko
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Téma	téma	k1gNnSc1	téma
Česko	Česko	k1gNnSc4	Česko
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Průvodce	průvodka	k1gFnSc3	průvodka
Česko	Česko	k1gNnSc1	Česko
ve	v	k7c6	v
Wikicestách	Wikicesta	k1gFnPc6	Wikicesta
</s>
</p>
<p>
<s>
Česko	Česko	k1gNnSc1	Česko
na	na	k7c4	na
OpenStreetMap	OpenStreetMap	k1gInSc4	OpenStreetMap
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
<p>
<s>
Portál	portál	k1gInSc1	portál
veřejné	veřejný	k2eAgFnSc2d1	veřejná
správy	správa	k1gFnSc2	správa
</s>
</p>
<p>
<s>
Národní	národní	k2eAgInSc1d1	národní
geoportál	geoportál	k1gInSc1	geoportál
</s>
</p>
