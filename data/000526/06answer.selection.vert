<s>
Krkonošský	krkonošský	k2eAgInSc1d1	krkonošský
národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
byl	být	k5eAaImAgInS	být
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
17	[number]	k4	17
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1963	[number]	k4	1963
vládním	vládní	k2eAgNnSc7d1	vládní
nařízením	nařízení	k1gNnSc7	nařízení
č.	č.	k?	č.
41	[number]	k4	41
<g/>
/	/	kIx~	/
<g/>
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
po	po	k7c6	po
polském	polský	k2eAgInSc6d1	polský
Karkonoskem	Karkonosko	k1gNnSc7	Karkonosko
Parku	park	k1gInSc2	park
Narodowem	Narodowem	k1gInSc4	Narodowem
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
16	[number]	k4	16
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1959	[number]	k4	1959
<g/>
.	.	kIx.	.
</s>
