<s>
Krkonošský	krkonošský	k2eAgInSc1d1	krkonošský
národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
též	též	k9	též
KRNAP	KRNAP	kA	KRNAP
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
chráněné	chráněný	k2eAgNnSc1d1	chráněné
území	území	k1gNnSc1	území
nalézající	nalézající	k2eAgMnSc1d1	nalézající
se	se	k3xPyFc4	se
na	na	k7c6	na
geomorfologickém	geomorfologický	k2eAgInSc6d1	geomorfologický
celku	celek	k1gInSc6	celek
Krkonoše	Krkonoše	k1gFnPc1	Krkonoše
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c4	na
severozápadu	severozápad	k1gInSc2	severozápad
okresu	okres	k1gInSc2	okres
Trutnov	Trutnov	k1gInSc1	Trutnov
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
také	také	k9	také
do	do	k7c2	do
okresu	okres	k1gInSc2	okres
Semily	Semily	k1gInPc1	Semily
a	a	k8xC	a
Jablonec	Jablonec	k1gInSc1	Jablonec
nad	nad	k7c7	nad
Nisou	Nisa	k1gFnSc7	Nisa
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgFnSc1d1	severní
hranice	hranice	k1gFnSc1	hranice
parku	park	k1gInSc2	park
je	být	k5eAaImIp3nS	být
vedena	vést	k5eAaImNgFnS	vést
po	po	k7c6	po
státní	státní	k2eAgFnSc6d1	státní
hranici	hranice	k1gFnSc6	hranice
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ho	on	k3xPp3gMnSc4	on
současně	současně	k6eAd1	současně
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
od	od	k7c2	od
Karkonoskiego	Karkonoskiego	k6eAd1	Karkonoskiego
Parku	park	k1gInSc3	park
Narodowego	Narodowego	k6eAd1	Narodowego
na	na	k7c6	na
polské	polský	k2eAgFnSc6d1	polská
straně	strana	k1gFnSc6	strana
Krkonoš	Krkonoše	k1gFnPc2	Krkonoše
<g/>
.	.	kIx.	.
</s>
<s>
Hranice	hranice	k1gFnSc1	hranice
ochranného	ochranný	k2eAgNnSc2d1	ochranné
pásma	pásmo	k1gNnSc2	pásmo
dále	daleko	k6eAd2	daleko
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
od	od	k7c2	od
Žacléře	Žacléř	k1gInSc2	Žacléř
a	a	k8xC	a
pak	pak	k6eAd1	pak
po	po	k7c6	po
ose	osa	k1gFnSc6	osa
silnice	silnice	k1gFnSc2	silnice
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
14	[number]	k4	14
k	k	k7c3	k
Mladým	mladý	k2eAgMnPc3d1	mladý
Bukům	buk	k1gInPc3	buk
<g/>
,	,	kIx,	,
Rudníku	rudník	k1gMnSc6	rudník
<g/>
,	,	kIx,	,
Vrchlabí	Vrchlabí	k1gNnSc6	Vrchlabí
<g/>
,	,	kIx,	,
Jilemnici	Jilemnice	k1gFnSc6	Jilemnice
<g/>
,	,	kIx,	,
Vysokému	vysoký	k2eAgInSc3d1	vysoký
nad	nad	k7c7	nad
Jizerou	Jizera	k1gFnSc7	Jizera
a	a	k8xC	a
ke	k	k7c3	k
Kořenovu	Kořenův	k2eAgMnSc3d1	Kořenův
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
loga	logo	k1gNnSc2	logo
</s>
<s>
Krkonošský	krkonošský	k2eAgInSc1d1	krkonošský
národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
byl	být	k5eAaImAgInS	být
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
17	[number]	k4	17
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1963	[number]	k4	1963
vládním	vládní	k2eAgNnSc7d1	vládní
nařízením	nařízení	k1gNnSc7	nařízení
č.	č.	k?	č.
41	[number]	k4	41
<g/>
/	/	kIx~	/
<g/>
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
po	po	k7c6	po
polském	polský	k2eAgInSc6d1	polský
Karkonoskem	Karkonosko	k1gNnSc7	Karkonosko
Parku	park	k1gInSc2	park
Narodowem	Narodowem	k1gInSc4	Narodowem
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
16	[number]	k4	16
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1959	[number]	k4	1959
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1986	[number]	k4	1986
byl	být	k5eAaImAgInS	být
národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
o	o	k7c4	o
ochranné	ochranný	k2eAgNnSc4d1	ochranné
pásmo	pásmo	k1gNnSc4	pásmo
vyhláškou	vyhláška	k1gFnSc7	vyhláška
vlády	vláda	k1gFnSc2	vláda
č.	č.	k?	č.
58	[number]	k4	58
<g/>
/	/	kIx~	/
<g/>
1986	[number]	k4	1986
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
Vládním	vládní	k2eAgNnSc7d1	vládní
nařízením	nařízení	k1gNnSc7	nařízení
č.	č.	k?	č.
165	[number]	k4	165
<g/>
/	/	kIx~	/
<g/>
1991	[number]	k4	1991
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
byl	být	k5eAaImAgInS	být
znovu	znovu	k6eAd1	znovu
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
statut	statut	k1gInSc1	statut
Krkonošského	krkonošský	k2eAgInSc2d1	krkonošský
národního	národní	k2eAgInSc2d1	národní
parku	park	k1gInSc2	park
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Posláním	poslání	k1gNnSc7	poslání
národního	národní	k2eAgInSc2d1	národní
parku	park	k1gInSc2	park
je	být	k5eAaImIp3nS	být
uchování	uchování	k1gNnSc4	uchování
a	a	k8xC	a
zlepšení	zlepšení	k1gNnSc4	zlepšení
jeho	jeho	k3xOp3gNnSc7	jeho
<g />
.	.	kIx.	.
</s>
<s>
přírodního	přírodní	k2eAgNnSc2d1	přírodní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
ochrana	ochrana	k1gFnSc1	ochrana
či	či	k8xC	či
obnova	obnova	k1gFnSc1	obnova
samořídících	samořídící	k2eAgFnPc2d1	samořídící
funkcí	funkce	k1gFnPc2	funkce
přírodních	přírodní	k2eAgInPc2d1	přírodní
systémů	systém	k1gInPc2	systém
<g/>
,	,	kIx,	,
přísná	přísný	k2eAgFnSc1d1	přísná
ochrana	ochrana	k1gFnSc1	ochrana
volně	volně	k6eAd1	volně
žijících	žijící	k2eAgMnPc2d1	žijící
živočichů	živočich	k1gMnPc2	živočich
a	a	k8xC	a
planě	planě	k6eAd1	planě
rostoucích	rostoucí	k2eAgFnPc2d1	rostoucí
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
zachování	zachování	k1gNnSc4	zachování
typického	typický	k2eAgInSc2d1	typický
vzhledu	vzhled	k1gInSc2	vzhled
krajiny	krajina	k1gFnSc2	krajina
<g/>
,	,	kIx,	,
naplňování	naplňování	k1gNnSc6	naplňování
vědeckých	vědecký	k2eAgInPc2d1	vědecký
a	a	k8xC	a
výchovných	výchovný	k2eAgInPc2d1	výchovný
cílů	cíl	k1gInPc2	cíl
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
využití	využití	k1gNnSc1	využití
území	území	k1gNnSc2	území
národního	národní	k2eAgInSc2d1	národní
parku	park	k1gInSc2	park
k	k	k7c3	k
ekologicky	ekologicky	k6eAd1	ekologicky
únosné	únosný	k2eAgFnSc3d1	únosná
turistice	turistika	k1gFnSc3	turistika
a	a	k8xC	a
rekreaci	rekreace	k1gFnSc4	rekreace
nezhoršující	zhoršující	k2eNgFnSc4d1	zhoršující
životní	životní	k2eAgNnSc4d1	životní
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Současně	současně	k6eAd1	současně
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
o	o	k7c4	o
rozdělení	rozdělení	k1gNnSc4	rozdělení
parku	park	k1gInSc2	park
do	do	k7c2	do
tří	tři	k4xCgFnPc2	tři
zón	zóna	k1gFnPc2	zóna
-	-	kIx~	-
přísná	přísný	k2eAgFnSc1d1	přísná
přírodní	přírodní	k2eAgFnSc1d1	přírodní
<g/>
,	,	kIx,	,
řízená	řízený	k2eAgFnSc1d1	řízená
přírodní	přírodní	k2eAgFnSc1d1	přírodní
a	a	k8xC	a
okrajová	okrajový	k2eAgFnSc1d1	okrajová
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
prvních	první	k4xOgInPc2	první
dvou	dva	k4xCgInPc2	dva
zón	zóna	k1gFnPc2	zóna
je	být	k5eAaImIp3nS	být
zákaz	zákaz	k1gInSc1	zákaz
vstupu	vstup	k1gInSc3	vstup
mimo	mimo	k7c4	mimo
značené	značený	k2eAgFnPc4d1	značená
cesty	cesta	k1gFnPc4	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
třetí	třetí	k4xOgFnSc6	třetí
zóně	zóna	k1gFnSc6	zóna
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
návštěvníci	návštěvník	k1gMnPc1	návštěvník
pohybovat	pohybovat	k5eAaImF	pohybovat
většinou	většinou	k6eAd1	většinou
volně	volně	k6eAd1	volně
i	i	k9	i
po	po	k7c6	po
loukách	louka	k1gFnPc6	louka
a	a	k8xC	a
lesích	les	k1gInPc6	les
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
původní	původní	k2eAgNnSc4d1	původní
území	území	k1gNnSc4	území
národního	národní	k2eAgInSc2d1	národní
parku	park	k1gInSc2	park
bylo	být	k5eAaImAgNnS	být
zmenšeno	zmenšit	k5eAaPmNgNnS	zmenšit
<g/>
.	.	kIx.	.
</s>
<s>
Hranice	hranice	k1gFnSc1	hranice
KRNAP	KRNAP	kA	KRNAP
se	se	k3xPyFc4	se
posunuly	posunout	k5eAaPmAgInP	posunout
více	hodně	k6eAd2	hodně
do	do	k7c2	do
hor	hora	k1gFnPc2	hora
a	a	k8xC	a
vyjmuty	vyjmout	k5eAaPmNgFnP	vyjmout
z	z	k7c2	z
KRNAP	KRNAP	kA	KRNAP
byla	být	k5eAaImAgFnS	být
zastavěná	zastavěný	k2eAgFnSc1d1	zastavěná
území	území	k1gNnSc6	území
několika	několik	k4yIc2	několik
měst	město	k1gNnPc2	město
a	a	k8xC	a
tyto	tento	k3xDgFnPc1	tento
oblasti	oblast	k1gFnPc1	oblast
přeřazeny	přeřadit	k5eAaPmNgFnP	přeřadit
do	do	k7c2	do
ochranného	ochranný	k2eAgNnSc2d1	ochranné
pásma	pásmo	k1gNnSc2	pásmo
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
je	být	k5eAaImIp3nS	být
ochrana	ochrana	k1gFnSc1	ochrana
na	na	k7c6	na
podobné	podobný	k2eAgFnSc6d1	podobná
úrovni	úroveň	k1gFnSc6	úroveň
jako	jako	k8xS	jako
v	v	k7c6	v
CHKO	CHKO	kA	CHKO
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgNnPc4	dva
města	město	k1gNnPc4	město
uprostřed	uprostřed	k7c2	uprostřed
hor	hora	k1gFnPc2	hora
Špindlerův	Špindlerův	k2eAgInSc4d1	Špindlerův
Mlýn	mlýn	k1gInSc4	mlýn
a	a	k8xC	a
Pec	Pec	k1gFnSc4	Pec
pod	pod	k7c7	pod
Sněžkou	Sněžka	k1gFnSc7	Sněžka
dokonce	dokonce	k9	dokonce
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
jakési	jakýsi	k3yIgNnSc1	jakýsi
díry	díra	k1gFnPc1	díra
v	v	k7c6	v
mapě	mapa	k1gFnSc6	mapa
KRNAP	KRNAP	kA	KRNAP
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
byly	být	k5eAaImAgInP	být
oba	dva	k4xCgInPc1	dva
národní	národní	k2eAgInPc1d1	národní
parky	park	k1gInPc1	park
na	na	k7c6	na
české	český	k2eAgFnSc6d1	Česká
i	i	k8xC	i
polské	polský	k2eAgFnSc6d1	polská
straně	strana	k1gFnSc6	strana
společně	společně	k6eAd1	společně
zařazeny	zařadit	k5eAaPmNgFnP	zařadit
do	do	k7c2	do
sítě	síť	k1gFnSc2	síť
biosférických	biosférický	k2eAgFnPc2d1	biosférická
rezervací	rezervace	k1gFnPc2	rezervace
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
Péči	péče	k1gFnSc4	péče
o	o	k7c4	o
park	park	k1gInSc4	park
a	a	k8xC	a
administrativní	administrativní	k2eAgNnSc4d1	administrativní
zajištění	zajištění	k1gNnSc4	zajištění
činností	činnost	k1gFnPc2	činnost
obstarává	obstarávat	k5eAaImIp3nS	obstarávat
Správa	správa	k1gFnSc1	správa
KRNAP	KRNAP	kA	KRNAP
sídlící	sídlící	k2eAgFnSc1d1	sídlící
ve	v	k7c6	v
Vrchlabí	Vrchlabí	k1gNnSc6	Vrchlabí
<g/>
,	,	kIx,	,
terénní	terénní	k2eAgFnSc1d1	terénní
služba	služba	k1gFnSc1	služba
sídlí	sídlet	k5eAaImIp3nS	sídlet
ve	v	k7c6	v
Špindlerově	Špindlerův	k2eAgInSc6d1	Špindlerův
Mlýně	mlýn	k1gInSc6	mlýn
<g/>
.	.	kIx.	.
</s>
<s>
Příroda	příroda	k1gFnSc1	příroda
Krkonoš	Krkonoše	k1gFnPc2	Krkonoše
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
rozmanitá	rozmanitý	k2eAgFnSc1d1	rozmanitá
-	-	kIx~	-
geologické	geologický	k2eAgNnSc1d1	geologické
podloží	podloží	k1gNnSc1	podloží
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc4	jeho
dynamický	dynamický	k2eAgInSc4d1	dynamický
vývoj	vývoj	k1gInSc4	vývoj
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
<g/>
,	,	kIx,	,
ovlivnění	ovlivnění	k1gNnSc1	ovlivnění
chladným	chladný	k2eAgNnSc7d1	chladné
klimatem	klima	k1gNnSc7	klima
a	a	k8xC	a
následné	následný	k2eAgNnSc1d1	následné
oteplení	oteplení	k1gNnSc1	oteplení
mělo	mít	k5eAaImAgNnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
vznik	vznik	k1gInSc4	vznik
rozmanitých	rozmanitý	k2eAgInPc2d1	rozmanitý
biotopů	biotop	k1gInPc2	biotop
a	a	k8xC	a
zachování	zachování	k1gNnSc4	zachování
pro	pro	k7c4	pro
naši	náš	k3xOp1gFnSc4	náš
přírodu	příroda	k1gFnSc4	příroda
vzácných	vzácný	k2eAgInPc2d1	vzácný
druhů	druh	k1gInPc2	druh
jak	jak	k8xS	jak
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
tak	tak	k9	tak
živočichů	živočich	k1gMnPc2	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
asi	asi	k9	asi
300	[number]	k4	300
druhů	druh	k1gInPc2	druh
obratlovců	obratlovec	k1gMnPc2	obratlovec
a	a	k8xC	a
přes	přes	k7c4	přes
1200	[number]	k4	1200
druhů	druh	k1gInPc2	druh
cévnatých	cévnatý	k2eAgFnPc2d1	cévnatá
rostlin	rostlina	k1gFnPc2	rostlina
a	a	k8xC	a
několikanásobně	několikanásobně	k6eAd1	několikanásobně
více	hodně	k6eAd2	hodně
výtrusných	výtrusný	k2eAgFnPc2d1	výtrusná
rostlin	rostlina	k1gFnPc2	rostlina
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
mechy	mech	k1gInPc1	mech
<g/>
,	,	kIx,	,
hlenky	hlenka	k1gFnPc1	hlenka
<g/>
,	,	kIx,	,
kapradiny	kapradina	k1gFnPc1	kapradina
<g/>
,	,	kIx,	,
lišejníky	lišejník	k1gInPc1	lišejník
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
krkonošské	krkonošský	k2eAgFnSc6d1	Krkonošská
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
několik	několik	k4yIc1	několik
endemitů	endemit	k1gInPc2	endemit
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Krkonoše	Krkonoše	k1gFnPc1	Krkonoše
<g/>
.	.	kIx.	.
</s>
<s>
Krkonošský	krkonošský	k2eAgInSc1d1	krkonošský
národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
leží	ležet	k5eAaImIp3nS	ležet
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
geomorfologickém	geomorfologický	k2eAgInSc6d1	geomorfologický
celku	celek	k1gInSc6	celek
Krkonoše	Krkonoše	k1gFnPc1	Krkonoše
v	v	k7c6	v
Krkonošsko-jesenické	krkonošskoesenický	k2eAgFnSc6d1	krkonošsko-jesenický
soustavě	soustava	k1gFnSc6	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
bodem	bod	k1gInSc7	bod
parku	park	k1gInSc2	park
(	(	kIx(	(
<g/>
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
i	i	k9	i
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
Sněžka	Sněžka	k1gFnSc1	Sněžka
s	s	k7c7	s
1603	[number]	k4	1603
metry	metr	k1gInPc4	metr
nad	nad	k7c7	nad
mořem	moře	k1gNnSc7	moře
<g/>
.	.	kIx.	.
</s>
<s>
Dělí	dělit	k5eAaImIp3nS	dělit
se	se	k3xPyFc4	se
na	na	k7c4	na
Vrchlabskou	vrchlabský	k2eAgFnSc4d1	vrchlabská
vrchovinu	vrchovina	k1gFnSc4	vrchovina
a	a	k8xC	a
Krkonošské	krkonošský	k2eAgInPc4d1	krkonošský
rozsochy	rozsoch	k1gInPc4	rozsoch
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
,	,	kIx,	,
a	a	k8xC	a
Krkonošské	krkonošský	k2eAgInPc4d1	krkonošský
hřbety	hřbet	k1gInPc4	hřbet
při	při	k7c6	při
severní	severní	k2eAgFnSc6d1	severní
hranici	hranice	k1gFnSc6	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
patrny	patrn	k2eAgInPc1d1	patrn
vlivy	vliv	k1gInPc1	vliv
posledního	poslední	k2eAgNnSc2d1	poslední
zalednění	zalednění	k1gNnSc2	zalednění
-	-	kIx~	-
kary	kara	k1gFnPc1	kara
<g/>
,	,	kIx,	,
nivační	nivační	k2eAgFnPc1d1	nivační
kary	kara	k1gFnPc1	kara
<g/>
,	,	kIx,	,
morény	moréna	k1gFnPc1	moréna
<g/>
,	,	kIx,	,
tory	torus	k1gInPc1	torus
<g/>
,	,	kIx,	,
kamenná	kamenný	k2eAgNnPc1d1	kamenné
moře	moře	k1gNnPc1	moře
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
relikty	relikt	k1gInPc1	relikt
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
parku	park	k1gInSc2	park
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Albeřic	Albeřice	k1gFnPc2	Albeřice
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
také	také	k9	také
krasové	krasový	k2eAgInPc4d1	krasový
jevy	jev	k1gInPc4	jev
-	-	kIx~	-
Albeřické	Albeřický	k2eAgInPc4d1	Albeřický
lomy	lom	k1gInPc4	lom
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Geologie	geologie	k1gFnSc2	geologie
Krkonoš	Krkonoše	k1gFnPc2	Krkonoše
<g/>
.	.	kIx.	.
</s>
<s>
Geologické	geologický	k2eAgNnSc1d1	geologické
podloží	podloží	k1gNnSc1	podloží
krkonošské	krkonošský	k2eAgFnSc2d1	Krkonošská
oblasti	oblast	k1gFnSc2	oblast
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
pestré	pestrý	k2eAgNnSc1d1	pestré
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
území	území	k1gNnSc2	území
spadá	spadat	k5eAaImIp3nS	spadat
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
tzv.	tzv.	kA	tzv.
krkonošsko-jizerského	krkonošskoizerský	k2eAgNnSc2d1	krkonošsko-jizerské
krystalinika	krystalinikum	k1gNnSc2	krystalinikum
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
významná	významný	k2eAgFnSc1d1	významná
část	část	k1gFnSc1	část
krkonošsko-jizerský	krkonošskoizerský	k2eAgInSc1d1	krkonošsko-jizerský
pluton	pluton	k1gInSc4	pluton
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
tvořený	tvořený	k2eAgInSc4d1	tvořený
granitoidy	granitoid	k1gInPc4	granitoid
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
na	na	k7c4	na
území	území	k1gNnSc4	území
národního	národní	k2eAgInSc2d1	národní
parku	park	k1gInSc2	park
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
hlavní	hlavní	k2eAgInSc4d1	hlavní
hřbet	hřbet	k1gInSc4	hřbet
od	od	k7c2	od
Mrtvého	mrtvý	k2eAgInSc2d1	mrtvý
vrchu	vrch	k1gInSc2	vrch
(	(	kIx(	(
<g/>
1060	[number]	k4	1060
m	m	kA	m
<g/>
)	)	kIx)	)
až	až	k9	až
k	k	k7c3	k
západnímu	západní	k2eAgNnSc3d1	západní
úpatí	úpatí	k1gNnSc3	úpatí
Sněžky	Sněžka	k1gFnSc2	Sněžka
(	(	kIx(	(
<g/>
1603	[number]	k4	1603
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kontaktním	kontaktní	k2eAgInSc6d1	kontaktní
dvoře	dvůr	k1gInSc6	dvůr
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
táhne	táhnout	k5eAaImIp3nS	táhnout
od	od	k7c2	od
Hvězdy	hvězda	k1gFnSc2	hvězda
(	(	kIx(	(
<g/>
959	[number]	k4	959
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
Čertovu	čertův	k2eAgFnSc4d1	Čertova
horu	hora	k1gFnSc4	hora
(	(	kIx(	(
<g/>
1210	[number]	k4	1210
m	m	kA	m
<g/>
)	)	kIx)	)
ke	k	k7c3	k
Svorové	svorový	k2eAgFnSc3d1	Svorová
hoře	hora	k1gFnSc3	hora
(	(	kIx(	(
<g/>
1411	[number]	k4	1411
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
šedé	šedý	k2eAgInPc1d1	šedý
svory	svor	k1gInPc1	svor
až	až	k8xS	až
fylity	fylit	k1gInPc1	fylit
<g/>
.	.	kIx.	.
</s>
<s>
Východní	východní	k2eAgFnSc1d1	východní
část	část	k1gFnSc1	část
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
tvořena	tvořit	k5eAaImNgFnS	tvořit
krkonošskými	krkonošský	k2eAgFnPc7d1	Krkonošská
rulami	rula	k1gFnPc7	rula
a	a	k8xC	a
fylity	fylit	k1gInPc7	fylit
<g/>
,	,	kIx,	,
západní	západní	k2eAgFnSc1d1	západní
část	část	k1gFnSc1	část
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
fylity	fylit	k1gInPc7	fylit
ponikelské	ponikelský	k2eAgFnSc2d1	ponikelská
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Okrajově	okrajově	k6eAd1	okrajově
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
Krkonošského	krkonošský	k2eAgInSc2d1	krkonošský
národního	národní	k2eAgInSc2d1	národní
parku	park	k1gInSc2	park
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
podkrkonošská	podkrkonošský	k2eAgFnSc1d1	Podkrkonošská
pánev	pánev	k1gFnSc1	pánev
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
uloženy	uložit	k5eAaPmNgFnP	uložit
sedimentární	sedimentární	k2eAgFnPc1d1	sedimentární
horniny	hornina	k1gFnPc1	hornina
stáří	stáří	k1gNnSc2	stáří
karbonu	karbon	k1gInSc2	karbon
až	až	k8xS	až
permu	perm	k1gInSc2	perm
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
horské	horský	k2eAgFnPc4d1	horská
oblasti	oblast	k1gFnPc4	oblast
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
je	být	k5eAaImIp3nS	být
typické	typický	k2eAgNnSc1d1	typické
výrazné	výrazný	k2eAgNnSc1d1	výrazné
střídání	střídání	k1gNnSc1	střídání
ročních	roční	k2eAgNnPc2d1	roční
období	období	k1gNnPc2	období
a	a	k8xC	a
vliv	vliv	k1gInSc4	vliv
Atlantického	atlantický	k2eAgInSc2d1	atlantický
oceánu	oceán	k1gInSc2	oceán
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
velmi	velmi	k6eAd1	velmi
neproměnlivé	proměnlivý	k2eNgNnSc4d1	neproměnlivé
počasí	počasí	k1gNnSc4	počasí
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
mikroklimatické	mikroklimatický	k2eAgFnPc4d1	mikroklimatická
podmínky	podmínka	k1gFnPc4	podmínka
je	být	k5eAaImIp3nS	být
zásadní	zásadní	k2eAgFnSc1d1	zásadní
orientace	orientace	k1gFnSc1	orientace
svahů	svah	k1gInPc2	svah
vůči	vůči	k7c3	vůči
slunečnímu	sluneční	k2eAgInSc3d1	sluneční
svitu	svit	k1gInSc3	svit
a	a	k8xC	a
proudění	proudění	k1gNnSc3	proudění
větrů	vítr	k1gInPc2	vítr
-	-	kIx~	-
to	ten	k3xDgNnSc1	ten
jsou	být	k5eAaImIp3nP	být
faktory	faktor	k1gInPc1	faktor
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
rostliny	rostlina	k1gFnPc4	rostlina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
na	na	k7c6	na
těchto	tento	k3xDgNnPc6	tento
místech	místo	k1gNnPc6	místo
rostou	růst	k5eAaImIp3nP	růst
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
horských	horský	k2eAgFnPc6d1	horská
oblastech	oblast	k1gFnPc6	oblast
Krkonoš	Krkonoše	k1gFnPc2	Krkonoše
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
zimních	zimní	k2eAgInPc6d1	zimní
a	a	k8xC	a
podzimních	podzimní	k2eAgInPc6d1	podzimní
měsících	měsíc	k1gInPc6	měsíc
častá	častý	k2eAgFnSc1d1	častá
teplotní	teplotní	k2eAgFnSc1d1	teplotní
inverze	inverze	k1gFnSc1	inverze
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
může	moct	k5eAaImIp3nS	moct
trvat	trvat	k5eAaImF	trvat
i	i	k9	i
několik	několik	k4yIc4	několik
týdnů	týden	k1gInPc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
roční	roční	k2eAgFnSc1d1	roční
teplota	teplota	k1gFnSc1	teplota
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c7	mezi
0,2	[number]	k4	0,2
°	°	k?	°
<g/>
C	C	kA	C
na	na	k7c6	na
Sněžce	Sněžka	k1gFnSc6	Sněžka
<g/>
,	,	kIx,	,
v	v	k7c6	v
nižších	nízký	k2eAgFnPc6d2	nižší
polohách	poloha	k1gFnPc6	poloha
jsou	být	k5eAaImIp3nP	být
průměrné	průměrný	k2eAgFnPc1d1	průměrná
roční	roční	k2eAgFnPc1d1	roční
teploty	teplota	k1gFnPc1	teplota
vyšší	vysoký	k2eAgFnSc2d2	vyšší
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Dolní	dolní	k2eAgFnSc1d1	dolní
Malá	malý	k2eAgFnSc1d1	malá
Úpa	Úpa	k1gFnSc1	Úpa
3,9	[number]	k4	3,9
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
Bedřichov	Bedřichov	k1gInSc1	Bedřichov
4,7	[number]	k4	4,7
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
a	a	k8xC	a
nejteplejším	teplý	k2eAgInSc7d3	nejteplejší
měsícem	měsíc	k1gInSc7	měsíc
je	být	k5eAaImIp3nS	být
červenec	červenec	k1gInSc4	červenec
a	a	k8xC	a
nejstudenějším	studený	k2eAgInSc7d3	nejstudenější
leden	leden	k1gInSc4	leden
<g/>
.	.	kIx.	.
</s>
<s>
Sněhová	sněhový	k2eAgFnSc1d1	sněhová
pokrývka	pokrývka	k1gFnSc1	pokrývka
na	na	k7c6	na
vrcholcích	vrcholek	k1gInPc6	vrcholek
hor	hora	k1gFnPc2	hora
se	se	k3xPyFc4	se
udrží	udržet	k5eAaPmIp3nS	udržet
až	až	k9	až
180	[number]	k4	180
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Hranice	hranice	k1gFnSc1	hranice
alpínského	alpínský	k2eAgInSc2d1	alpínský
lesa	les	k1gInSc2	les
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
1250	[number]	k4	1250
až	až	k9	až
1350	[number]	k4	1350
m.	m.	k?	m.
Ve	v	k7c6	v
vysokých	vysoký	k2eAgFnPc6d1	vysoká
polohách	poloha	k1gFnPc6	poloha
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
několik	několik	k4yIc1	několik
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
glaciálními	glaciální	k2eAgInPc7d1	glaciální
relikty	relikt	k1gInPc7	relikt
(	(	kIx(	(
<g/>
tzn.	tzn.	kA	tzn.
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
tu	tu	k6eAd1	tu
přežily	přežít	k5eAaPmAgFnP	přežít
od	od	k7c2	od
posledního	poslední	k2eAgNnSc2d1	poslední
zalednění	zalednění	k1gNnSc2	zalednění
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
např.	např.	kA	např.
všivec	všivec	k1gInSc4	všivec
krkonošský	krkonošský	k2eAgInSc1d1	krkonošský
<g/>
,	,	kIx,	,
lomikámen	lomikámen	k1gInSc1	lomikámen
sněžný	sněžný	k2eAgInSc1d1	sněžný
<g/>
,	,	kIx,	,
ostružiník	ostružiník	k1gInSc1	ostružiník
moruška	moruška	k1gFnSc1	moruška
<g/>
,	,	kIx,	,
rašeliník	rašeliník	k1gInSc1	rašeliník
Lindbergův	Lindbergův	k2eAgInSc1d1	Lindbergův
<g/>
,	,	kIx,	,
šídlatka	šídlatka	k1gFnSc1	šídlatka
jezerní	jezerní	k2eAgFnSc2d1	jezerní
aj.	aj.	kA	aj.
Vegetační	vegetační	k2eAgInPc1d1	vegetační
stupně	stupeň	k1gInPc1	stupeň
Krkonoš	Krkonoše	k1gFnPc2	Krkonoše
<g/>
:	:	kIx,	:
submontánní	submontánní	k2eAgFnSc1d1	submontánní
(	(	kIx(	(
<g/>
400	[number]	k4	400
-	-	kIx~	-
800	[number]	k4	800
m	m	kA	m
n.	n.	k?	n.
<g/>
m.	m.	k?	m.
<g/>
)	)	kIx)	)
-	-	kIx~	-
Původně	původně	k6eAd1	původně
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
stupni	stupeň	k1gInSc6	stupeň
převažovaly	převažovat	k5eAaImAgInP	převažovat
listnaté	listnatý	k2eAgInPc1d1	listnatý
až	až	k8xS	až
smíšené	smíšený	k2eAgInPc1d1	smíšený
lesy	les	k1gInPc1	les
se	s	k7c7	s
zastoupením	zastoupení	k1gNnSc7	zastoupení
buku	buk	k1gInSc2	buk
lesního	lesní	k2eAgInSc2d1	lesní
<g/>
,	,	kIx,	,
javoru	javor	k1gInSc2	javor
klen	klen	k2eAgInSc1d1	klen
<g/>
,	,	kIx,	,
jeřábem	jeřáb	k1gMnSc7	jeřáb
ptačím	ptačí	k2eAgMnSc7d1	ptačí
<g/>
,	,	kIx,	,
olší	olše	k1gFnSc7	olše
šedou	šedý	k2eAgFnSc7d1	šedá
a	a	k8xC	a
na	na	k7c6	na
polské	polský	k2eAgFnSc6d1	polská
straně	strana	k1gFnSc6	strana
i	i	k8xC	i
modřínem	modřín	k1gInSc7	modřín
opadavým	opadavý	k2eAgInSc7d1	opadavý
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
stromy	strom	k1gInPc1	strom
byly	být	k5eAaImAgInP	být
vykáceny	vykácen	k2eAgInPc1d1	vykácen
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
zde	zde	k6eAd1	zde
uměle	uměle	k6eAd1	uměle
vysázena	vysázen	k2eAgFnSc1d1	vysázena
smrková	smrkový	k2eAgFnSc1d1	smrková
monokultura	monokultura	k1gFnSc1	monokultura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
podrostu	podrost	k1gInSc6	podrost
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
chráněné	chráněný	k2eAgFnPc1d1	chráněná
rostliny	rostlina	k1gFnPc1	rostlina
jako	jako	k8xC	jako
kyčelnice	kyčelnice	k1gFnPc1	kyčelnice
cibulkonosná	cibulkonosný	k2eAgFnSc1d1	cibulkonosná
a	a	k8xC	a
devítilistá	devítilistý	k2eAgFnSc1d1	devítilistá
<g/>
,	,	kIx,	,
sasanka	sasanka	k1gFnSc1	sasanka
pryskyřníkovitá	pryskyřníkovitá	k1gFnSc1	pryskyřníkovitá
a	a	k8xC	a
hajní	hajní	k2eAgInPc1d1	hajní
<g/>
,	,	kIx,	,
česnek	česnek	k1gInSc4	česnek
medvědí	medvědí	k2eAgInSc4d1	medvědí
<g/>
,	,	kIx,	,
lilie	lilie	k1gFnSc1	lilie
zlatohlavá	zlatohlavý	k2eAgFnSc1d1	zlatohlavá
<g/>
.	.	kIx.	.
montánní	montánní	k2eAgFnSc1d1	montánní
(	(	kIx(	(
<g/>
800	[number]	k4	800
-	-	kIx~	-
1200	[number]	k4	1200
m	m	kA	m
n.	n.	k?	n.
<g/>
m.	m.	k?	m.
<g/>
)	)	kIx)	)
-	-	kIx~	-
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
stupni	stupeň	k1gInSc6	stupeň
převažují	převažovat	k5eAaImIp3nP	převažovat
smrkové	smrkový	k2eAgInPc4d1	smrkový
porosty	porost	k1gInPc4	porost
(	(	kIx(	(
<g/>
původní	původní	k2eAgInSc4d1	původní
i	i	k8xC	i
uměle	uměle	k6eAd1	uměle
vysázené	vysázený	k2eAgInPc1d1	vysázený
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tmavé	tmavý	k2eAgInPc1d1	tmavý
lesy	les	k1gInPc1	les
jsou	být	k5eAaImIp3nP	být
vhodným	vhodný	k2eAgNnSc7d1	vhodné
prostředím	prostředí	k1gNnSc7	prostředí
pro	pro	k7c4	pro
růst	růst	k1gInSc4	růst
kapradin	kapradina	k1gFnPc2	kapradina
(	(	kIx(	(
<g/>
kapraď	kapraď	k1gFnSc1	kapraď
samec	samec	k1gMnSc1	samec
<g/>
,	,	kIx,	,
žebrovice	žebrovice	k1gFnSc1	žebrovice
různolistá	různolistý	k2eAgFnSc1d1	různolistá
<g/>
,	,	kIx,	,
papratka	papratka	k1gFnSc1	papratka
horská	horský	k2eAgFnSc1d1	horská
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
tu	tu	k6eAd1	tu
jsou	být	k5eAaImIp3nP	být
bezlesé	bezlesý	k2eAgFnPc4d1	bezlesá
horské	horský	k2eAgFnPc4d1	horská
louky	louka	k1gFnPc4	louka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
hojně	hojně	k6eAd1	hojně
zastoupeny	zastoupit	k5eAaPmNgInP	zastoupit
vzácné	vzácný	k2eAgInPc1d1	vzácný
druhy	druh	k1gInPc1	druh
rostlin	rostlina	k1gFnPc2	rostlina
jako	jako	k8xS	jako
např.	např.	kA	např.
zvonek	zvonek	k1gInSc4	zvonek
krkonošský	krkonošský	k2eAgInSc1d1	krkonošský
<g/>
,	,	kIx,	,
violka	violka	k1gFnSc1	violka
sudetská	sudetský	k2eAgFnSc1d1	sudetská
<g/>
,	,	kIx,	,
prha	prha	k1gFnSc1	prha
arnika	arnika	k1gFnSc1	arnika
<g/>
,	,	kIx,	,
devětsil	devětsil	k1gInSc1	devětsil
bílý	bílý	k2eAgInSc1d1	bílý
<g/>
,	,	kIx,	,
náholník	náholník	k1gInSc1	náholník
jednokvětý	jednokvětý	k2eAgInSc1d1	jednokvětý
a	a	k8xC	a
několik	několik	k4yIc1	několik
zástupců	zástupce	k1gMnPc2	zástupce
čeledi	čeleď	k1gFnSc2	čeleď
vstavačovitých	vstavačovitá	k1gFnPc2	vstavačovitá
<g/>
.	.	kIx.	.
subalpínský	subalpínský	k2eAgMnSc1d1	subalpínský
(	(	kIx(	(
<g/>
1200	[number]	k4	1200
-	-	kIx~	-
1450	[number]	k4	1450
m	m	kA	m
n.	n.	k?	n.
<g/>
m.	m.	k?	m.
<g/>
)	)	kIx)	)
-	-	kIx~	-
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
stupni	stupeň	k1gInSc6	stupeň
jsou	být	k5eAaImIp3nP	být
nejcennější	cenný	k2eAgInPc1d3	nejcennější
biotopy	biotop	k1gInPc1	biotop
Krkonoš	Krkonoše	k1gFnPc2	Krkonoše
-	-	kIx~	-
smilkové	smilkový	k2eAgFnPc1d1	Smilková
louky	louka	k1gFnPc1	louka
<g/>
,	,	kIx,	,
severská	severský	k2eAgNnPc1d1	severské
rašeliniště	rašeliniště	k1gNnPc1	rašeliniště
a	a	k8xC	a
porosty	porost	k1gInPc1	porost
kleče	kleče	k6eAd1	kleče
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bylinném	bylinný	k2eAgNnSc6d1	bylinné
patru	patro	k1gNnSc6	patro
jsou	být	k5eAaImIp3nP	být
zastoupeny	zastoupit	k5eAaPmNgFnP	zastoupit
smilka	smilka	k1gFnSc1	smilka
tuhá	tuhý	k2eAgFnSc1d1	tuhá
<g/>
,	,	kIx,	,
třtina	třtina	k1gFnSc1	třtina
chloupkatá	chloupkatý	k2eAgFnSc1d1	chloupkatá
<g/>
,	,	kIx,	,
brusnicovité	brusnicovitý	k2eAgFnPc1d1	brusnicovitý
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
,	,	kIx,	,
šicha	šicha	k1gFnSc1	šicha
oboupohlavná	oboupohlavný	k2eAgFnSc1d1	oboupohlavná
aj.	aj.	kA	aj.
V	v	k7c6	v
subalpínském	subalpínský	k2eAgInSc6d1	subalpínský
stupni	stupeň	k1gInSc6	stupeň
se	se	k3xPyFc4	se
rostou	růst	k5eAaImIp3nP	růst
výše	vysoce	k6eAd2	vysoce
zmíněné	zmíněný	k2eAgFnPc1d1	zmíněná
reliktní	reliktní	k2eAgFnPc1d1	reliktní
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
.	.	kIx.	.
alpínský	alpínský	k2eAgMnSc1d1	alpínský
(	(	kIx(	(
<g/>
1450	[number]	k4	1450
-	-	kIx~	-
1603	[number]	k4	1603
m	m	kA	m
n.	n.	k?	n.
<g/>
m.	m.	k?	m.
<g/>
)	)	kIx)	)
-	-	kIx~	-
Druhově	druhově	k6eAd1	druhově
nejbohatší	bohatý	k2eAgInPc1d3	nejbohatší
jsou	být	k5eAaImIp3nP	být
strmé	strmý	k2eAgInPc1d1	strmý
skalní	skalní	k2eAgInPc1d1	skalní
svahy	svah	k1gInPc1	svah
a	a	k8xC	a
jámy	jáma	k1gFnSc2	jáma
(	(	kIx(	(
<g/>
známé	známý	k2eAgFnSc2d1	známá
jako	jako	k8xS	jako
tzv.	tzv.	kA	tzv.
krkonošské	krkonošský	k2eAgFnSc2d1	Krkonošská
botanické	botanický	k2eAgFnSc2d1	botanická
zahrádky	zahrádka	k1gFnSc2	zahrádka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Svahy	svah	k1gInPc1	svah
karů	kar	k1gInPc2	kar
jsou	být	k5eAaImIp3nP	být
porostlé	porostlý	k2eAgFnPc1d1	porostlá
např.	např.	kA	např.
omějem	oměj	k1gInSc7	oměj
zdobným	zdobný	k2eAgInSc7d1	zdobný
a	a	k8xC	a
štíhlým	štíhlý	k2eAgInSc7d1	štíhlý
<g/>
,	,	kIx,	,
mléčivcem	mléčivec	k1gInSc7	mléčivec
alpským	alpský	k2eAgInSc7d1	alpský
<g/>
,	,	kIx,	,
havézí	havézit	k5eAaPmIp3nS	havézit
česnáčkovou	česnáčková	k1gFnSc7	česnáčková
a	a	k8xC	a
mnoha	mnoho	k4c7	mnoho
druhy	druh	k1gInPc7	druh
kapraďorostů	kapraďorost	k1gInPc2	kapraďorost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
pramenišť	prameniště	k1gNnPc2	prameniště
roste	růst	k5eAaImIp3nS	růst
česnek	česnek	k1gInSc4	česnek
sibiřský	sibiřský	k2eAgInSc4d1	sibiřský
<g/>
,	,	kIx,	,
prvosenka	prvosenka	k1gFnSc1	prvosenka
nejmenší	malý	k2eAgFnSc1d3	nejmenší
<g/>
,	,	kIx,	,
lepnice	lepnice	k1gFnSc1	lepnice
alpská	alpský	k2eAgFnSc1d1	alpská
<g/>
,	,	kIx,	,
kropenáč	kropenáč	k1gInSc1	kropenáč
vytrvalý	vytrvalý	k2eAgInSc1d1	vytrvalý
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dřevin	dřevina	k1gFnPc2	dřevina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
ale	ale	k8xC	ale
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
miniaturního	miniaturní	k2eAgInSc2d1	miniaturní
vzrůstu	vzrůst	k1gInSc2	vzrůst
kvůli	kvůli	k7c3	kvůli
tvrdým	tvrdý	k2eAgFnPc3d1	tvrdá
podmínkám	podmínka	k1gFnPc3	podmínka
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
zastoupeny	zastoupit	k5eAaPmNgInP	zastoupit
druhy	druh	k1gInPc1	druh
jako	jako	k8xS	jako
vrba	vrba	k1gFnSc1	vrba
slezská	slezský	k2eAgFnSc1d1	Slezská
<g/>
,	,	kIx,	,
bříza	bříza	k1gFnSc1	bříza
karpatská	karpatský	k2eAgFnSc1d1	Karpatská
<g/>
,	,	kIx,	,
střemcha	střemcha	k1gFnSc1	střemcha
skalní	skalní	k2eAgFnSc1d1	skalní
<g/>
,	,	kIx,	,
jeřáb	jeřáb	k1gInSc1	jeřáb
sudetský	sudetský	k2eAgInSc1d1	sudetský
a	a	k8xC	a
borovice	borovice	k1gFnSc1	borovice
kleč	kleč	k1gFnSc1	kleč
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
bohatá	bohatý	k2eAgNnPc4d1	bohaté
rostlinná	rostlinný	k2eAgNnPc4d1	rostlinné
společenstva	společenstvo	k1gNnPc4	společenstvo
jsou	být	k5eAaImIp3nP	být
vázány	vázán	k2eAgInPc1d1	vázán
i	i	k8xC	i
rozmanité	rozmanitý	k2eAgInPc1d1	rozmanitý
druhy	druh	k1gInPc1	druh
živočichů	živočich	k1gMnPc2	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Živočišná	živočišný	k2eAgNnPc1d1	živočišné
společenstva	společenstvo	k1gNnPc1	společenstvo
se	se	k3xPyFc4	se
zformovala	zformovat	k5eAaPmAgFnS	zformovat
během	během	k7c2	během
poslední	poslední	k2eAgFnSc2d1	poslední
doby	doba	k1gFnSc2	doba
ledové	ledový	k2eAgFnSc2d1	ledová
a	a	k8xC	a
následujícího	následující	k2eAgNnSc2d1	následující
příznivějšího	příznivý	k2eAgNnSc2d2	příznivější
období	období	k1gNnSc2	období
(	(	kIx(	(
<g/>
zvaného	zvaný	k2eAgInSc2d1	zvaný
holocén	holocén	k1gInSc1	holocén
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nižších	nízký	k2eAgFnPc6d2	nižší
polohách	poloha	k1gFnPc6	poloha
jsou	být	k5eAaImIp3nP	být
zastoupeny	zastoupit	k5eAaPmNgInP	zastoupit
druhy	druh	k1gInPc1	druh
eurosibiřské	eurosibiřský	k2eAgFnSc2d1	eurosibiřská
fauny	fauna	k1gFnSc2	fauna
a	a	k8xC	a
s	s	k7c7	s
rostoucí	rostoucí	k2eAgFnSc7d1	rostoucí
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
přibývají	přibývat	k5eAaImIp3nP	přibývat
horské	horský	k2eAgInPc1d1	horský
druhy	druh	k1gInPc1	druh
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
několik	několik	k4yIc4	několik
druhů	druh	k1gInPc2	druh
bezobratlých	bezobratlí	k1gMnPc2	bezobratlí
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
zde	zde	k6eAd1	zde
zastupují	zastupovat	k5eAaImIp3nP	zastupovat
glaciální	glaciální	k2eAgInPc4d1	glaciální
relikty	relikt	k1gInPc4	relikt
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
např.	např.	kA	např.
slíďák	slíďák	k1gMnSc1	slíďák
ostnonohý	ostnonohý	k2eAgMnSc1d1	ostnonohý
<g/>
,	,	kIx,	,
vrkoč	vrkoč	k1gFnSc1	vrkoč
severní	severní	k2eAgFnSc1d1	severní
<g/>
,	,	kIx,	,
jepice	jepice	k1gFnSc1	jepice
horská	horský	k2eAgFnSc1d1	horská
<g/>
,	,	kIx,	,
střevlík	střevlík	k1gMnSc1	střevlík
Nebria	Nebrium	k1gNnSc2	Nebrium
gyllenhali	gyllenhat	k5eAaPmAgMnP	gyllenhat
<g/>
,	,	kIx,	,
vážky	vážka	k1gFnPc1	vážka
Somatochlora	Somatochlor	k1gMnSc2	Somatochlor
alpestris	alpestris	k1gFnSc1	alpestris
a	a	k8xC	a
Aeschna	Aeschen	k2eAgFnSc1d1	Aeschna
coerulea	coerulea	k1gFnSc1	coerulea
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
obratlovců	obratlovec	k1gMnPc2	obratlovec
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
Krkonoších	Krkonoše	k1gFnPc6	Krkonoše
pozorovány	pozorován	k2eAgInPc1d1	pozorován
druhy	druh	k1gInPc1	druh
jako	jako	k8xS	jako
kos	kos	k1gMnSc1	kos
horský	horský	k2eAgMnSc1d1	horský
severoevropský	severoevropský	k2eAgMnSc1d1	severoevropský
<g/>
,	,	kIx,	,
slavík	slavík	k1gMnSc1	slavík
modráček	modráček	k1gMnSc1	modráček
tundrový	tundrový	k2eAgMnSc1d1	tundrový
<g/>
,	,	kIx,	,
kulík	kulík	k1gMnSc1	kulík
hnědý	hnědý	k2eAgMnSc1d1	hnědý
<g/>
,	,	kIx,	,
čečetka	čečetka	k1gFnSc1	čečetka
zimní	zimní	k2eAgFnSc1d1	zimní
a	a	k8xC	a
nejrozšířenějším	rozšířený	k2eAgMnSc7d3	nejrozšířenější
hlodavcem	hlodavec	k1gMnSc7	hlodavec
je	být	k5eAaImIp3nS	být
hraboš	hraboš	k1gMnSc1	hraboš
mokřadní	mokřadní	k2eAgMnSc1d1	mokřadní
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
endemický	endemický	k2eAgInSc4d1	endemický
druh	druh	k1gInSc4	druh
Krkonoš	Krkonoše	k1gFnPc2	Krkonoše
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
pouze	pouze	k6eAd1	pouze
jepici	jepice	k1gFnSc4	jepice
krkonošskou	krkonošský	k2eAgFnSc4d1	Krkonošská
(	(	kIx(	(
<g/>
Rhithrogena	Rhithrogen	k2eAgMnSc4d1	Rhithrogen
corcontica	corconticus	k1gMnSc4	corconticus
<g/>
)	)	kIx)	)
a	a	k8xC	a
dva	dva	k4xCgInPc4	dva
poddruhy	poddruh	k1gInPc4	poddruh
i	i	k8xC	i
jinde	jinde	k6eAd1	jinde
se	se	k3xPyFc4	se
vyskytujících	vyskytující	k2eAgMnPc2d1	vyskytující
živočichů	živočich	k1gMnPc2	živočich
-	-	kIx~	-
motýla	motýl	k1gMnSc2	motýl
huňatce	huňatec	k1gMnSc2	huňatec
žlutopáseho	žlutopáse	k1gMnSc2	žlutopáse
(	(	kIx(	(
<g/>
Torula	torula	k1gFnSc1	torula
quadrifaria	quadrifarium	k1gNnSc2	quadrifarium
sudetica	sudetic	k1gInSc2	sudetic
<g/>
)	)	kIx)	)
a	a	k8xC	a
plže	plž	k1gMnSc4	plž
vřetenovku	vřetenovka	k1gFnSc4	vřetenovka
krkonošskou	krkonošský	k2eAgFnSc4d1	Krkonošská
(	(	kIx(	(
<g/>
Cochlodina	Cochlodin	k2eAgNnPc1d1	Cochlodin
dubiosa	dubiosa	k1gNnPc1	dubiosa
corcontica	corcontic	k2eAgNnPc1d1	corcontic
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
KRNAP	KRNAP	kA	KRNAP
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Krkonošský	krkonošský	k2eAgInSc4d1	krkonošský
národní	národní	k2eAgInSc4d1	národní
park	park	k1gInSc4	park
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Krkonošský	krkonošský	k2eAgInSc1d1	krkonošský
národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
na	na	k7c4	na
OpenStreetMap	OpenStreetMap	k1gInSc4	OpenStreetMap
Správa	správa	k1gFnSc1	správa
Krkonošského	krkonošský	k2eAgInSc2d1	krkonošský
národního	národní	k2eAgInSc2d1	národní
parku	park	k1gInSc2	park
Karkonoski	Karkonosk	k1gFnSc2	Karkonosk
Park	park	k1gInSc1	park
Narodowy	Narodowa	k1gFnSc2	Narodowa
-	-	kIx~	-
polská	polský	k2eAgFnSc1d1	polská
část	část	k1gFnSc1	část
Krkonošský	krkonošský	k2eAgInSc1d1	krkonošský
národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
-	-	kIx~	-
video	video	k1gNnSc1	video
z	z	k7c2	z
cyklu	cyklus	k1gInSc2	cyklus
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
Návraty	návrat	k1gInPc1	návrat
k	k	k7c3	k
divočině	divočina	k1gFnSc3	divočina
</s>
