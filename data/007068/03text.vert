<s>
Sázava	Sázava	k1gFnSc1	Sázava
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Sasau	Sasaus	k1gInSc3	Sasaus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
řeka	řeka	k1gFnSc1	řeka
v	v	k7c6	v
Kraji	kraj	k1gInSc6	kraj
Vysočina	vysočina	k1gFnSc1	vysočina
a	a	k8xC	a
ve	v	k7c6	v
Středočeském	středočeský	k2eAgInSc6d1	středočeský
kraji	kraj	k1gInSc6	kraj
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
Najdeku	Najdek	k1gInSc2	Najdek
a	a	k8xC	a
Šlakhamrů	Šlakhamr	k1gInPc2	Šlakhamr
tvoří	tvořit	k5eAaImIp3nS	tvořit
Sázava	Sázava	k1gFnSc1	Sázava
část	část	k1gFnSc1	část
historické	historický	k2eAgFnSc2d1	historická
zemské	zemský	k2eAgFnSc2d1	zemská
hranice	hranice	k1gFnSc2	hranice
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
Moravy	Morava	k1gFnSc2	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Odvodňuje	odvodňovat	k5eAaImIp3nS	odvodňovat
část	část	k1gFnSc4	část
Českomoravské	českomoravský	k2eAgFnSc2d1	Českomoravská
vrchoviny	vrchovina	k1gFnSc2	vrchovina
a	a	k8xC	a
severní	severní	k2eAgFnSc1d1	severní
oblast	oblast	k1gFnSc1	oblast
Středočeské	středočeský	k2eAgFnSc2d1	Středočeská
pahorkatiny	pahorkatina	k1gFnSc2	pahorkatina
<g/>
.	.	kIx.	.
</s>
<s>
Vodáci	Vodák	k1gMnPc1	Vodák
a	a	k8xC	a
trampové	tramp	k1gMnPc1	tramp
ji	on	k3xPp3gFnSc4	on
často	často	k6eAd1	často
nazývají	nazývat	k5eAaImIp3nP	nazývat
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
řeka	řeka	k1gFnSc1	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Neoficiální	oficiální	k2eNgInSc1d1	neoficiální
název	název	k1gInSc1	název
nesouvisí	souviset	k5eNaImIp3nS	souviset
s	s	k7c7	s
dolováním	dolování	k1gNnSc7	dolování
zlata	zlato	k1gNnSc2	zlato
v	v	k7c6	v
Jílovém	Jílové	k1gNnSc6	Jílové
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
s	s	k7c7	s
barvou	barva	k1gFnSc7	barva
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
odnášení	odnášení	k1gNnSc1	odnášení
jílovité	jílovitý	k2eAgFnSc2d1	jílovitá
půdy	půda	k1gFnSc2	půda
a	a	k8xC	a
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
zvláště	zvláště	k6eAd1	zvláště
dobře	dobře	k6eAd1	dobře
vidět	vidět	k5eAaImF	vidět
na	na	k7c6	na
soutoku	soutok	k1gInSc6	soutok
s	s	k7c7	s
Vltavou	Vltava	k1gFnSc7	Vltava
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
délka	délka	k1gFnSc1	délka
řeky	řeka	k1gFnSc2	řeka
činí	činit	k5eAaImIp3nS	činit
225,9	[number]	k4	225,9
km	km	kA	km
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
208,3	[number]	k4	208,3
km	km	kA	km
sjízdných	sjízdný	k2eAgInPc2d1	sjízdný
na	na	k7c4	na
sportovní	sportovní	k2eAgFnSc4d1	sportovní
lodi	loď	k1gFnPc4	loď
<g/>
.	.	kIx.	.
</s>
<s>
Plocha	plocha	k1gFnSc1	plocha
povodí	povodí	k1gNnSc2	povodí
měří	měřit	k5eAaImIp3nS	měřit
4350,3	[number]	k4	4350,3
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Pramení	pramenit	k5eAaImIp3nS	pramenit
jako	jako	k9	jako
Stružný	Stružný	k2eAgInSc1d1	Stružný
potok	potok	k1gInSc1	potok
zhruba	zhruba	k6eAd1	zhruba
1	[number]	k4	1
km	km	kA	km
severozápadně	severozápadně	k6eAd1	severozápadně
od	od	k7c2	od
Šindelného	Šindelný	k2eAgInSc2d1	Šindelný
vrchu	vrch	k1gInSc2	vrch
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
757	[number]	k4	757
m.	m.	k?	m.
Ten	ten	k3xDgMnSc1	ten
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
menšími	malý	k2eAgInPc7d2	menší
potoky	potok	k1gInPc7	potok
napájí	napájet	k5eAaImIp3nS	napájet
rybník	rybník	k1gInSc4	rybník
Velké	velký	k2eAgNnSc1d1	velké
Dářko	Dářko	k1gNnSc1	Dářko
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
výtoku	výtok	k1gInSc2	výtok
z	z	k7c2	z
Velkého	velký	k2eAgInSc2d1	velký
Dářka	Dářek	k1gInSc2	Dářek
je	být	k5eAaImIp3nS	být
již	již	k9	již
říčka	říčka	k1gFnSc1	říčka
nazývána	nazýván	k2eAgFnSc1d1	nazývána
Sázavou	Sázava	k1gFnSc7	Sázava
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
její	její	k3xOp3gInSc4	její
pramen	pramen	k1gInSc4	pramen
se	se	k3xPyFc4	se
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
vedly	vést	k5eAaImAgInP	vést
spory	spor	k1gInPc1	spor
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
zdrojnice	zdrojnice	k1gFnSc1	zdrojnice
odtékající	odtékající	k2eAgFnSc1d1	odtékající
z	z	k7c2	z
Velkého	velký	k2eAgInSc2d1	velký
Dářka	Dářek	k1gInSc2	Dářek
dnes	dnes	k6eAd1	dnes
pojmenovaná	pojmenovaný	k2eAgFnSc1d1	pojmenovaná
jako	jako	k8xS	jako
Sázava	Sázava	k1gFnSc1	Sázava
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
původně	původně	k6eAd1	původně
jmenovala	jmenovat	k5eAaBmAgFnS	jmenovat
Polná	Polný	k2eAgFnSc1d1	Polná
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
současný	současný	k2eAgInSc1d1	současný
Stržský	Stržský	k2eAgInSc1d1	Stržský
potok	potok	k1gInSc1	potok
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
kronikách	kronika	k1gFnPc6	kronika
uváděn	uvádět	k5eAaImNgInS	uvádět
jako	jako	k8xC	jako
Sázava	Sázava	k1gFnSc1	Sázava
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
své	svůj	k3xOyFgNnSc4	svůj
hydrologické	hydrologický	k2eAgNnSc4d1	hydrologické
opodstatnění	opodstatnění	k1gNnSc4	opodstatnění
ve	v	k7c6	v
větší	veliký	k2eAgFnSc6d2	veliký
ploše	plocha	k1gFnSc6	plocha
povodí	povodí	k1gNnSc6	povodí
i	i	k8xC	i
větším	veliký	k2eAgInSc6d2	veliký
průměrném	průměrný	k2eAgInSc6d1	průměrný
průtoku	průtok	k1gInSc6	průtok
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
soutoku	soutok	k1gInSc2	soutok
obou	dva	k4xCgFnPc2	dva
zdrojnic	zdrojnice	k1gFnPc2	zdrojnice
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
Žďárem	Žďár	k1gInSc7	Žďár
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
(	(	kIx(	(
<g/>
říční	říční	k2eAgInSc1d1	říční
kilometr	kilometr	k1gInSc1	kilometr
208	[number]	k4	208
<g/>
)	)	kIx)	)
a	a	k8xC	a
Přibyslaví	Přibyslav	k1gFnSc7	Přibyslav
(	(	kIx(	(
<g/>
ř.	ř.	k?	ř.
km	km	kA	km
184	[number]	k4	184
<g/>
)	)	kIx)	)
řeka	řeka	k1gFnSc1	řeka
protéká	protékat	k5eAaImIp3nS	protékat
údolím	údolí	k1gNnSc7	údolí
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
spádem	spád	k1gInSc7	spád
a	a	k8xC	a
peřejemi	peřej	k1gFnPc7	peřej
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
Přibyslaví	Přibyslav	k1gFnPc2	Přibyslav
se	se	k3xPyFc4	se
údolí	údolí	k1gNnSc1	údolí
otvírá	otvírat	k5eAaImIp3nS	otvírat
a	a	k8xC	a
řeka	řeka	k1gFnSc1	řeka
meandruje	meandrovat	k5eAaImIp3nS	meandrovat
k	k	k7c3	k
Havlíčkovu	Havlíčkův	k2eAgInSc3d1	Havlíčkův
Brodu	Brod	k1gInSc3	Brod
(	(	kIx(	(
<g/>
ř.	ř.	k?	ř.
km	km	kA	km
163	[number]	k4	163
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
charakter	charakter	k1gInSc1	charakter
má	mít	k5eAaImIp3nS	mít
až	až	k9	až
pod	pod	k7c4	pod
město	město	k1gNnSc4	město
Světlá	světlat	k5eAaImIp3nS	světlat
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
(	(	kIx(	(
<g/>
ř.	ř.	k?	ř.
km	km	kA	km
144	[number]	k4	144
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
údolí	údolí	k1gNnSc1	údolí
řeky	řeka	k1gFnSc2	řeka
svírá	svírat	k5eAaImIp3nS	svírat
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nS	tvořit
nejkrásnější	krásný	k2eAgFnSc1d3	nejkrásnější
část	část	k1gFnSc1	část
–	–	k?	–
peřeje	peřej	k1gFnSc2	peřej
Stvořidla	Stvořidlo	k1gNnSc2	Stvořidlo
(	(	kIx(	(
<g/>
od	od	k7c2	od
ř.	ř.	k?	ř.
km	km	kA	km
139	[number]	k4	139
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
cca	cca	kA	cca
5	[number]	k4	5
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
několika	několik	k4yIc6	několik
kilometrech	kilometr	k1gInPc6	kilometr
řeka	řeka	k1gFnSc1	řeka
přechází	přecházet	k5eAaImIp3nS	přecházet
do	do	k7c2	do
středního	střední	k2eAgInSc2d1	střední
toku	tok	k1gInSc2	tok
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
mírný	mírný	k2eAgInSc1d1	mírný
<g/>
,	,	kIx,	,
s	s	k7c7	s
častými	častý	k2eAgInPc7d1	častý
jezy	jez	k1gInPc7	jez
a	a	k8xC	a
bez	bez	k7c2	bez
proudu	proud	k1gInSc2	proud
<g/>
.	.	kIx.	.
</s>
<s>
Charakter	charakter	k1gInSc1	charakter
řeky	řeka	k1gFnSc2	řeka
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
mění	měnit	k5eAaImIp3nS	měnit
až	až	k9	až
pod	pod	k7c7	pod
Týncem	Týnec	k1gInSc7	Týnec
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
(	(	kIx(	(
<g/>
ř.	ř.	k?	ř.
km	km	kA	km
19	[number]	k4	19
<g/>
)	)	kIx)	)
u	u	k7c2	u
Krhanic	krhanice	k1gFnPc2	krhanice
(	(	kIx(	(
<g/>
ř.	ř.	k?	ř.
km	km	kA	km
15	[number]	k4	15
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tok	tok	k1gInSc1	tok
se	se	k3xPyFc4	se
zařezává	zařezávat	k5eAaImIp3nS	zařezávat
do	do	k7c2	do
hlubokého	hluboký	k2eAgNnSc2d1	hluboké
údolí	údolí	k1gNnSc2	údolí
se	s	k7c7	s
strmými	strmý	k2eAgFnPc7d1	strmá
stráněmi	stráň	k1gFnPc7	stráň
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
v	v	k7c6	v
kamenném	kamenný	k2eAgNnSc6d1	kamenné
řečišti	řečiště	k1gNnSc6	řečiště
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
četné	četný	k2eAgInPc4d1	četný
peřeje	peřej	k1gInPc4	peřej
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
pod	pod	k7c7	pod
Medníkem	Medník	k1gInSc7	Medník
sídlí	sídlet	k5eAaImIp3nS	sídlet
několik	několik	k4yIc1	několik
známých	známý	k2eAgFnPc2d1	známá
trampských	trampský	k2eAgFnPc2d1	trampská
osad	osada	k1gFnPc2	osada
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Toronto	Toronto	k1gNnSc1	Toronto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
průtoku	průtok	k1gInSc6	průtok
Pikovicemi	Pikovice	k1gFnPc7	Pikovice
(	(	kIx(	(
<g/>
ř.	ř.	k?	ř.
km	km	kA	km
3,5	[number]	k4	3,5
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
řeka	řeka	k1gFnSc1	řeka
opět	opět	k6eAd1	opět
uklidní	uklidnit	k5eAaPmIp3nS	uklidnit
a	a	k8xC	a
vlévá	vlévat	k5eAaImIp3nS	vlévat
se	se	k3xPyFc4	se
u	u	k7c2	u
Davle	Davle	k1gFnSc2	Davle
do	do	k7c2	do
Vltavy	Vltava	k1gFnSc2	Vltava
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
hladina	hladina	k1gFnSc1	hladina
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
vzdutá	vzdutý	k2eAgFnSc1d1	vzdutá
Vranskou	vranský	k2eAgFnSc7d1	Vranská
přehradou	přehrada	k1gFnSc7	přehrada
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgInSc7d3	veliký
přítokem	přítok	k1gInSc7	přítok
Sázavy	Sázava	k1gFnPc4	Sázava
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
délky	délka	k1gFnSc2	délka
toku	tok	k1gInSc2	tok
<g/>
,	,	kIx,	,
plochy	plocha	k1gFnPc1	plocha
povodí	povodí	k1gNnSc2	povodí
a	a	k8xC	a
vodnosti	vodnost	k1gFnSc2	vodnost
týče	týkat	k5eAaImIp3nS	týkat
je	být	k5eAaImIp3nS	být
řeka	řeka	k1gFnSc1	řeka
Želivka	Želivka	k1gFnSc1	Želivka
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
hustota	hustota	k1gFnSc1	hustota
říční	říční	k2eAgFnSc2d1	říční
sítě	síť	k1gFnSc2	síť
činí	činit	k5eAaImIp3nS	činit
1,29	[number]	k4	1,29
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
se	se	k3xPyFc4	se
v	v	k7c6	v
povodí	povodí	k1gNnSc6	povodí
Sázavy	Sázava	k1gFnSc2	Sázava
nachází	nacházet	k5eAaImIp3nS	nacházet
4890	[number]	k4	4890
vodních	vodní	k2eAgInPc2d1	vodní
toků	tok	k1gInPc2	tok
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
do	do	k7c2	do
jednoho	jeden	k4xCgInSc2	jeden
kilometru	kilometr	k1gInSc2	kilometr
a	a	k8xC	a
1369	[number]	k4	1369
vodních	vodní	k2eAgInPc2d1	vodní
toků	tok	k1gInPc2	tok
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
1	[number]	k4	1
až	až	k9	až
10	[number]	k4	10
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Vodotečí	vodoteč	k1gFnSc7	vodoteč
dlouhých	dlouhý	k2eAgFnPc2d1	dlouhá
10	[number]	k4	10
až	až	k9	až
20	[number]	k4	20
km	km	kA	km
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
povodí	povodí	k1gNnSc6	povodí
celkem	celkem	k6eAd1	celkem
třicet	třicet	k4xCc4	třicet
tři	tři	k4xCgFnPc4	tři
<g/>
.	.	kIx.	.
</s>
<s>
Potoků	potok	k1gInPc2	potok
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
mají	mít	k5eAaImIp3nP	mít
délku	délka	k1gFnSc4	délka
mezi	mezi	k7c7	mezi
20	[number]	k4	20
až	až	k9	až
40	[number]	k4	40
km	km	kA	km
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
celkově	celkově	k6eAd1	celkově
čtrnáct	čtrnáct	k4xCc4	čtrnáct
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
délce	délka	k1gFnSc6	délka
40	[number]	k4	40
až	až	k9	až
60	[number]	k4	60
km	km	kA	km
se	se	k3xPyFc4	se
v	v	k7c6	v
povodí	povodí	k1gNnSc6	povodí
Sázavy	Sázava	k1gFnSc2	Sázava
nalézá	nalézat	k5eAaImIp3nS	nalézat
pouze	pouze	k6eAd1	pouze
jeden	jeden	k4xCgInSc4	jeden
vodní	vodní	k2eAgInSc4d1	vodní
tok	tok	k1gInSc4	tok
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jím	on	k3xPp3gInSc7	on
řeka	řeka	k1gFnSc1	řeka
Trnava	Trnava	k1gFnSc1	Trnava
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
délka	délka	k1gFnSc1	délka
činí	činit	k5eAaImIp3nS	činit
56,3	[number]	k4	56,3
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Vodní	vodní	k2eAgFnPc1d1	vodní
toky	toka	k1gFnPc1	toka
delší	dlouhý	k2eAgFnPc1d2	delší
než	než	k8xS	než
60	[number]	k4	60
kilometrů	kilometr	k1gInPc2	kilometr
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
povodí	povodí	k1gNnSc6	povodí
jen	jen	k6eAd1	jen
tři	tři	k4xCgInPc4	tři
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
samotné	samotný	k2eAgFnSc2d1	samotná
Sázavy	Sázava	k1gFnSc2	Sázava
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
ještě	ještě	k9	ještě
řeky	řeka	k1gFnPc1	řeka
Želivka	Želivka	k1gFnSc1	Želivka
a	a	k8xC	a
Blanice	Blanice	k1gFnSc1	Blanice
<g/>
.	.	kIx.	.
</s>
<s>
Stržský	Stržský	k2eAgInSc1d1	Stržský
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
zleva	zleva	k6eAd1	zleva
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
211,6	[number]	k4	211,6
Staviště	staviště	k1gNnSc2	staviště
<g/>
,	,	kIx,	,
zleva	zleva	k6eAd1	zleva
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
209,8	[number]	k4	209,8
Poděšínský	Poděšínský	k2eAgInSc1d1	Poděšínský
potok	potok	k1gInSc1	potok
(	(	kIx(	(
<g/>
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
též	též	k9	též
Nížkovský	Nížkovský	k2eAgInSc1d1	Nížkovský
potok	potok	k1gInSc1	potok
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zleva	zleva	k6eAd1	zleva
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
192,1	[number]	k4	192,1
Losenický	Losenický	k2eAgInSc4d1	Losenický
potok	potok	k1gInSc4	potok
<g/>
,	,	kIx,	,
zprava	zprava	k6eAd1	zprava
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
187,5	[number]	k4	187,5
Doberský	Doberský	k2eAgInSc1d1	Doberský
potok	potok	k1gInSc1	potok
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
zprava	zprava	k6eAd1	zprava
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
181,7	[number]	k4	181,7
Borovský	borovský	k2eAgInSc4d1	borovský
potok	potok	k1gInSc4	potok
<g/>
,	,	kIx,	,
zprava	zprava	k6eAd1	zprava
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
177,6	[number]	k4	177,6
Simtanský	Simtanský	k2eAgInSc4d1	Simtanský
potok	potok	k1gInSc4	potok
<g/>
,	,	kIx,	,
zprava	zprava	k6eAd1	zprava
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
172,4	[number]	k4	172,4
Břevnický	Břevnický	k2eAgInSc4d1	Břevnický
potok	potok	k1gInSc4	potok
<g/>
,	,	kIx,	,
zprava	zprava	k6eAd1	zprava
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
166,6	[number]	k4	166,6
Šlapanka	Šlapanka	k1gFnSc1	Šlapanka
<g/>
,	,	kIx,	,
zleva	zleva	k6eAd1	zleva
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
164,4	[number]	k4	164,4
Žabinec	žabinec	k1gInSc4	žabinec
<g/>
,	,	kIx,	,
zleva	zleva	k6eAd1	zleva
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
162,6	[number]	k4	162,6
Úsobský	Úsobský	k2eAgInSc4d1	Úsobský
potok	potok	k1gInSc4	potok
<g/>
,	,	kIx,	,
zleva	zleva	k6eAd1	zleva
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
156,9	[number]	k4	156,9
Perlový	perlový	k2eAgInSc4d1	perlový
potok	potok	k1gInSc4	potok
<g/>
,	,	kIx,	,
zleva	zleva	k6eAd1	zleva
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
153,3	[number]	k4	153,3
Lučický	Lučický	k2eAgInSc4d1	Lučický
potok	potok	k1gInSc4	potok
<g/>
,	,	kIx,	,
zprava	zprava	k6eAd1	zprava
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
152,0	[number]	k4	152,0
Křivoláčský	Křivoláčský	k2eAgInSc4d1	Křivoláčský
potok	potok	k1gInSc4	potok
<g/>
,	,	kIx,	,
zleva	zleva	k6eAd1	zleva
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
151,0	[number]	k4	151,0
Ředkovský	Ředkovský	k2eAgInSc4d1	Ředkovský
potok	potok	k1gInSc4	potok
<g/>
,	,	kIx,	,
zleva	zleva	k6eAd1	zleva
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
149,9	[number]	k4	149,9
Sázavka	Sázavka	k1gFnSc1	Sázavka
<g/>
,	,	kIx,	,
zprava	zprava	k6eAd1	zprava
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
144,8	[number]	k4	144,8
Závidkovický	Závidkovický	k2eAgInSc4d1	Závidkovický
potok	potok	k1gInSc4	potok
<g/>
,	,	kIx,	,
zleva	zleva	k6eAd1	zleva
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
144,2	[number]	k4	144,2
Žebrákovský	Žebrákovský	k2eAgInSc4d1	Žebrákovský
potok	potok	k1gInSc4	potok
<g/>
,	,	kIx,	,
zprava	zprava	k6eAd1	zprava
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
142,5	[number]	k4	142,5
Pstružný	pstružný	k2eAgInSc4d1	pstružný
potok	potok	k1gInSc4	potok
<g/>
,	,	kIx,	,
zleva	zleva	k6eAd1	zleva
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
141,1	[number]	k4	141,1
Olešenský	Olešenský	k2eAgInSc4d1	Olešenský
potok	potok	k1gInSc4	potok
<g/>
,	,	kIx,	,
zprava	zprava	k6eAd1	zprava
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
129,1	[number]	k4	129,1
Jestřebnice	Jestřebnice	k1gFnSc2	Jestřebnice
<g/>
,	,	kIx,	,
zprava	zprava	k6eAd1	zprava
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
123,9	[number]	k4	123,9
Ostrovský	ostrovský	k2eAgInSc4d1	ostrovský
potok	potok	k1gInSc4	potok
<g/>
,	,	kIx,	,
zprava	zprava	k6eAd1	zprava
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
105,0	[number]	k4	105,0
Želivka	Želivka	k1gFnSc1	Želivka
<g/>
,	,	kIx,	,
zleva	zleva	k6eAd1	zleva
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
98,8	[number]	k4	98,8
Štěpánovský	Štěpánovský	k2eAgInSc4d1	Štěpánovský
potok	potok	k1gInSc4	potok
<g/>
,	,	kIx,	,
zleva	zleva	k6eAd1	zleva
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
95,6	[number]	k4	95,6
Čestínský	Čestínský	k2eAgInSc4d1	Čestínský
potok	potok	k1gInSc4	potok
<g/>
,	,	kIx,	,
zprava	zprava	k6eAd1	zprava
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
<g />
.	.	kIx.	.
</s>
<s>
km	km	kA	km
88,6	[number]	k4	88,6
Losinský	Losinský	k2eAgInSc4d1	Losinský
potok	potok	k1gInSc4	potok
<g/>
,	,	kIx,	,
zprava	zprava	k6eAd1	zprava
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
86,4	[number]	k4	86,4
Blanice	Blanice	k1gFnPc1	Blanice
(	(	kIx(	(
<g/>
Vlašimská	vlašimský	k2eAgFnSc1d1	Vlašimská
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zleva	zleva	k6eAd1	zleva
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
78,6	[number]	k4	78,6
Křešický	Křešický	k2eAgInSc4d1	Křešický
potok	potok	k1gInSc4	potok
<g/>
,	,	kIx,	,
zleva	zleva	k6eAd1	zleva
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
71,9	[number]	k4	71,9
Nučický	nučický	k2eAgInSc1d1	nučický
potok	potok	k1gInSc1	potok
(	(	kIx(	(
<g/>
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
též	též	k9	též
Vlkančický	Vlkančický	k2eAgInSc1d1	Vlkančický
potok	potok	k1gInSc1	potok
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zprava	zprava	k6eAd1	zprava
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
49,1	[number]	k4	49,1
Jevanský	Jevanský	k2eAgInSc4d1	Jevanský
potok	potok	k1gInSc4	potok
<g/>
,	,	kIx,	,
zprava	zprava	k6eAd1	zprava
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
48,7	[number]	k4	48,7
Vodslivský	Vodslivský	k2eAgInSc4d1	Vodslivský
potok	potok	k1gInSc4	potok
<g/>
,	,	kIx,	,
zleva	zleva	k6eAd1	zleva
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
46,5	[number]	k4	46,5
Mnichovka	Mnichovka	k1gFnSc1	Mnichovka
<g/>
,	,	kIx,	,
zprava	zprava	k6eAd1	zprava
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
37,6	[number]	k4	37,6
Benešovský	benešovský	k2eAgInSc4d1	benešovský
potok	potok	k1gInSc4	potok
<g/>
,	,	kIx,	,
zleva	zleva	k6eAd1	zleva
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
32,3	[number]	k4	32,3
Konopišťský	konopišťský	k2eAgInSc4d1	konopišťský
potok	potok	k1gInSc4	potok
<g/>
,	,	kIx,	,
zleva	zleva	k6eAd1	zleva
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
ř.	ř.	k?	ř.
km	km	kA	km
31,3	[number]	k4	31,3
Kamenický	kamenický	k2eAgInSc1d1	kamenický
potok	potok	k1gInSc1	potok
(	(	kIx(	(
<g/>
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
též	též	k9	též
Čakovický	Čakovický	k2eAgInSc1d1	Čakovický
potok	potok	k1gInSc1	potok
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zprava	zprava	k6eAd1	zprava
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
22,5	[number]	k4	22,5
Janovický	janovický	k2eAgInSc4d1	janovický
potok	potok	k1gInSc4	potok
<g/>
,	,	kIx,	,
zleva	zleva	k6eAd1	zleva
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
19,2	[number]	k4	19,2
Chotouňský	Chotouňský	k2eAgInSc4d1	Chotouňský
potok	potok	k1gInSc4	potok
<g/>
,	,	kIx,	,
zprava	zprava	k6eAd1	zprava
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
9,7	[number]	k4	9,7
Sázava	Sázava	k1gFnSc1	Sázava
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c7	mezi
toky	tok	k1gInPc7	tok
vrchovinno-nížinné	vrchovinnoížinný	k2eAgFnSc2d1	vrchovinno-nížinný
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zimním	zimní	k2eAgNnSc6d1	zimní
a	a	k8xC	a
jarním	jarní	k2eAgNnSc6d1	jarní
období	období	k1gNnSc6	období
odteče	odtéct	k5eAaPmIp3nS	odtéct
nad	nad	k7c7	nad
60	[number]	k4	60
<g/>
%	%	kIx~	%
celoročního	celoroční	k2eAgInSc2d1	celoroční
odtoku	odtok	k1gInSc2	odtok
<g/>
.	.	kIx.	.
</s>
<s>
Maxim	Maxim	k1gMnSc1	Maxim
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
od	od	k7c2	od
února	únor	k1gInSc2	únor
do	do	k7c2	do
dubna	duben	k1gInSc2	duben
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
táním	tání	k1gNnSc7	tání
sněhu	sníh	k1gInSc2	sníh
na	na	k7c6	na
Českomoravské	českomoravský	k2eAgFnSc6d1	Českomoravská
vrchovině	vrchovina	k1gFnSc6	vrchovina
<g/>
.	.	kIx.	.
</s>
<s>
Minimální	minimální	k2eAgInPc1d1	minimální
průtoky	průtok	k1gInPc1	průtok
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
letních	letní	k2eAgInPc6d1	letní
a	a	k8xC	a
podzimních	podzimní	k2eAgInPc6d1	podzimní
měsících	měsíc	k1gInPc6	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
až	až	k8xS	až
srpnu	srpen	k1gInSc6	srpen
mohou	moct	k5eAaImIp3nP	moct
hladinu	hladina	k1gFnSc4	hladina
zvýšit	zvýšit	k5eAaPmF	zvýšit
přívalové	přívalový	k2eAgFnPc4d1	přívalová
srážky	srážka	k1gFnPc4	srážka
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
průtok	průtok	k1gInSc1	průtok
u	u	k7c2	u
ústí	ústí	k1gNnSc2	ústí
činí	činit	k5eAaImIp3nS	činit
25,2	[number]	k4	25,2
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Ten	ten	k3xDgInSc1	ten
je	být	k5eAaImIp3nS	být
ochuzen	ochudit	k5eAaPmNgInS	ochudit
o	o	k7c4	o
odběr	odběr	k1gInSc4	odběr
pitné	pitný	k2eAgFnSc2d1	pitná
vody	voda	k1gFnSc2	voda
pro	pro	k7c4	pro
Prahu	Praha	k1gFnSc4	Praha
z	z	k7c2	z
povodí	povodí	k1gNnSc2	povodí
Želivky	Želivka	k1gFnSc2	Želivka
(	(	kIx(	(
<g/>
průměrně	průměrně	k6eAd1	průměrně
2,5	[number]	k4	2,5
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Průměrné	průměrný	k2eAgInPc1d1	průměrný
měsíční	měsíční	k2eAgInPc1d1	měsíční
průtoky	průtok	k1gInPc1	průtok
Sázavy	Sázava	k1gFnSc2	Sázava
ve	v	k7c4	v
stanici	stanice	k1gFnSc4	stanice
Nespeky	Nespek	k1gInPc1	Nespek
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
<g/>
:	:	kIx,	:
Hlásné	hlásný	k2eAgInPc4d1	hlásný
profily	profil	k1gInPc4	profil
<g/>
:	:	kIx,	:
Sázava	Sázava	k1gFnSc1	Sázava
je	být	k5eAaImIp3nS	být
vodáky	vodák	k1gMnPc4	vodák
často	často	k6eAd1	často
vyhledávanou	vyhledávaný	k2eAgFnSc7d1	vyhledávaná
řekou	řeka	k1gFnSc7	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
vodáky	vodák	k1gMnPc4	vodák
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
vyhledávají	vyhledávat	k5eAaImIp3nP	vyhledávat
divokou	divoký	k2eAgFnSc4d1	divoká
vodu	voda	k1gFnSc4	voda
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
atraktivní	atraktivní	k2eAgInPc1d1	atraktivní
zejména	zejména	k9	zejména
Stvořidla	Stvořidlo	k1gNnPc1	Stvořidlo
a	a	k8xC	a
za	za	k7c2	za
velmi	velmi	k6eAd1	velmi
vysokých	vysoký	k2eAgInPc2d1	vysoký
vodních	vodní	k2eAgInPc2d1	vodní
stavů	stav	k1gInPc2	stav
i	i	k8xC	i
peřeje	peřej	k1gFnSc2	peřej
na	na	k7c6	na
dolním	dolní	k2eAgInSc6d1	dolní
toku	tok	k1gInSc6	tok
v	v	k7c6	v
úseku	úsek	k1gInSc6	úsek
Týnec-Pikovice	Týnec-Pikovice	k1gFnSc2	Týnec-Pikovice
<g/>
.	.	kIx.	.
</s>
<s>
Stvořidla	Stvořidlo	k1gNnPc1	Stvořidlo
jsou	být	k5eAaImIp3nP	být
sjízdná	sjízdný	k2eAgNnPc1d1	sjízdné
pouze	pouze	k6eAd1	pouze
za	za	k7c2	za
alespoň	alespoň	k9	alespoň
mírně	mírně	k6eAd1	mírně
zvýšeného	zvýšený	k2eAgInSc2d1	zvýšený
vodního	vodní	k2eAgInSc2d1	vodní
stavu	stav	k1gInSc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Úsek	úsek	k1gInSc1	úsek
pod	pod	k7c7	pod
Týncem	Týnec	k1gInSc7	Týnec
bývá	bývat	k5eAaImIp3nS	bývat
sjízdný	sjízdný	k2eAgInSc1d1	sjízdný
po	po	k7c4	po
většinu	většina	k1gFnSc4	většina
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
období	období	k1gNnSc6	období
největšího	veliký	k2eAgNnSc2d3	veliký
sucha	sucho	k1gNnSc2	sucho
bývají	bývat	k5eAaImIp3nP	bývat
obvykle	obvykle	k6eAd1	obvykle
právě	právě	k9	právě
ve	v	k7c6	v
vodácky	vodácky	k6eAd1	vodácky
atraktivním	atraktivní	k2eAgNnSc6d1	atraktivní
období	období	k1gNnSc6	období
letních	letní	k2eAgFnPc2d1	letní
prázdnin	prázdniny	k1gFnPc2	prázdniny
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
středním	střední	k2eAgInSc6d1	střední
a	a	k8xC	a
dolním	dolní	k2eAgInSc6d1	dolní
toku	tok	k1gInSc6	tok
je	být	k5eAaImIp3nS	být
vybudována	vybudován	k2eAgFnSc1d1	vybudována
vodácká	vodácký	k2eAgFnSc1d1	vodácká
infrastruktura	infrastruktura	k1gFnSc1	infrastruktura
<g/>
.	.	kIx.	.
</s>
<s>
Najdete	najít	k5eAaPmIp2nP	najít
zde	zde	k6eAd1	zde
vodácké	vodácký	k2eAgInPc1d1	vodácký
kempy	kemp	k1gInPc1	kemp
a	a	k8xC	a
tábořiště	tábořiště	k1gNnPc1	tábořiště
a	a	k8xC	a
působí	působit	k5eAaImIp3nS	působit
zde	zde	k6eAd1	zde
několik	několik	k4yIc4	několik
půjčoven	půjčovna	k1gFnPc2	půjčovna
vodáckého	vodácký	k2eAgNnSc2d1	vodácké
vybavení	vybavení	k1gNnSc2	vybavení
<g/>
.	.	kIx.	.
</s>
<s>
Spodní	spodní	k2eAgFnSc1d1	spodní
část	část	k1gFnSc1	část
Sázavy	Sázava	k1gFnSc2	Sázava
je	být	k5eAaImIp3nS	být
jediný	jediný	k2eAgInSc1d1	jediný
po	po	k7c4	po
většinu	většina	k1gFnSc4	většina
roku	rok	k1gInSc2	rok
sjízdný	sjízdný	k2eAgMnSc1d1	sjízdný
a	a	k8xC	a
peřejnatý	peřejnatý	k2eAgInSc1d1	peřejnatý
úsek	úsek	k1gInSc1	úsek
řeky	řeka	k1gFnSc2	řeka
blízko	blízko	k7c2	blízko
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
již	již	k6eAd1	již
od	od	k7c2	od
pozdního	pozdní	k2eAgNnSc2d1	pozdní
jara	jaro	k1gNnSc2	jaro
o	o	k7c6	o
víkendech	víkend	k1gInPc6	víkend
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
na	na	k7c6	na
Sázavě	Sázava	k1gFnSc6	Sázava
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
vodáků	vodák	k1gMnPc2	vodák
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
je	být	k5eAaImIp3nS	být
hojně	hojně	k6eAd1	hojně
navštěvovaný	navštěvovaný	k2eAgInSc1d1	navštěvovaný
střední	střední	k2eAgInSc1d1	střední
a	a	k8xC	a
dolní	dolní	k2eAgInSc1d1	dolní
tok	tok	k1gInSc1	tok
mezi	mezi	k7c7	mezi
Horkou	Horká	k1gFnSc7	Horká
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
a	a	k8xC	a
Pikovicemi	Pikovice	k1gFnPc7	Pikovice
<g/>
.	.	kIx.	.
</s>
<s>
Dominantou	dominanta	k1gFnSc7	dominanta
Sázavy	Sázava	k1gFnSc2	Sázava
je	být	k5eAaImIp3nS	být
hezká	hezký	k2eAgFnSc1d1	hezká
příroda	příroda	k1gFnSc1	příroda
<g/>
,	,	kIx,	,
občas	občas	k6eAd1	občas
mírné	mírný	k2eAgInPc1d1	mírný
peřeje	peřej	k1gInPc1	peřej
a	a	k8xC	a
řada	řada	k1gFnSc1	řada
sjízdných	sjízdný	k2eAgInPc2d1	sjízdný
jezů	jez	k1gInPc2	jez
<g/>
.	.	kIx.	.
</s>
<s>
Přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
i	i	k9	i
mnoho	mnoho	k4c1	mnoho
historicky	historicky	k6eAd1	historicky
zajímavých	zajímavý	k2eAgNnPc2d1	zajímavé
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Ledeč	Ledeč	k1gFnSc1	Ledeč
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
<g/>
,	,	kIx,	,
Český	český	k2eAgInSc1d1	český
Šternberk	Šternberk	k1gInSc1	Šternberk
<g/>
,	,	kIx,	,
město	město	k1gNnSc1	město
Sázava	Sázava	k1gFnSc1	Sázava
,	,	kIx,	,
zřícenina	zřícenina	k1gFnSc1	zřícenina
Zbořený	zbořený	k2eAgInSc4d1	zbořený
Kostelec	Kostelec	k1gInSc4	Kostelec
<g/>
,	,	kIx,	,
Týnec	Týnec	k1gInSc1	Týnec
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
<g/>
...	...	k?	...
Nebezpečné	bezpečný	k2eNgInPc1d1	nebezpečný
jezy	jez	k1gInPc1	jez
na	na	k7c6	na
Sázavě	Sázava	k1gFnSc6	Sázava
<g/>
:	:	kIx,	:
Výstavbou	výstavba	k1gFnSc7	výstavba
přehrady	přehrada	k1gFnSc2	přehrada
ve	v	k7c6	v
Vraném	vraný	k2eAgNnSc6d1	Vrané
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
(	(	kIx(	(
<g/>
1930	[number]	k4	1930
<g/>
–	–	k?	–
<g/>
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gNnSc1	jejíž
napouštění	napouštění	k1gNnSc1	napouštění
započalo	započnout	k5eAaPmAgNnS	započnout
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
roku	rok	k1gInSc2	rok
1935	[number]	k4	1935
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vzedmula	vzedmout	k5eAaPmAgFnS	vzedmout
hladina	hladina	k1gFnSc1	hladina
Sázavy	Sázava	k1gFnSc2	Sázava
až	až	k9	až
po	po	k7c4	po
dolní	dolní	k2eAgFnSc4d1	dolní
špičku	špička	k1gFnSc4	špička
ostrova	ostrov	k1gInSc2	ostrov
v	v	k7c6	v
Pikovicích	Pikovice	k1gFnPc6	Pikovice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zde	zde	k6eAd1	zde
existovala	existovat	k5eAaImAgFnS	existovat
pravidelná	pravidelný	k2eAgFnSc1d1	pravidelná
osobní	osobní	k2eAgFnSc1d1	osobní
vodní	vodní	k2eAgFnSc1d1	vodní
doprava	doprava	k1gFnSc1	doprava
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
provozovala	provozovat	k5eAaImAgFnS	provozovat
Pražská	pražský	k2eAgFnSc1d1	Pražská
paroplavební	paroplavební	k2eAgFnSc1d1	paroplavební
společnost	společnost	k1gFnSc1	společnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přístavišti	přístaviště	k1gNnSc6	přístaviště
v	v	k7c6	v
Pikovicích	Pikovice	k1gFnPc6	Pikovice
bylo	být	k5eAaImAgNnS	být
tak	tak	k6eAd1	tak
možno	možno	k6eAd1	možno
zahlédnout	zahlédnout	k5eAaPmF	zahlédnout
například	například	k6eAd1	například
motorovou	motorový	k2eAgFnSc4d1	motorová
loď	loď	k1gFnSc4	loď
Sázavu	Sázava	k1gFnSc4	Sázava
vyrobenou	vyrobený	k2eAgFnSc4d1	vyrobená
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
v	v	k7c6	v
berlínské	berlínský	k2eAgFnSc6d1	Berlínská
loděnici	loděnice	k1gFnSc6	loděnice
Anker	Anker	k1gMnSc1	Anker
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1928	[number]	k4	1928
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
kapacita	kapacita	k1gFnSc1	kapacita
činila	činit	k5eAaImAgFnS	činit
100	[number]	k4	100
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Dolní	dolní	k2eAgInSc1d1	dolní
tok	tok	k1gInSc1	tok
Sázavy	Sázava	k1gFnSc2	Sázava
od	od	k7c2	od
Pikovic	Pikovice	k1gFnPc2	Pikovice
(	(	kIx(	(
<g/>
říční	říční	k2eAgFnSc1d1	říční
km	km	kA	km
2,50	[number]	k4	2,50
<g/>
)	)	kIx)	)
po	po	k7c6	po
ústí	ústí	k1gNnSc6	ústí
v	v	k7c6	v
Davli	Davle	k1gFnSc6	Davle
(	(	kIx(	(
<g/>
říční	říční	k2eAgFnSc1d1	říční
km	km	kA	km
0,00	[number]	k4	0,00
<g/>
)	)	kIx)	)
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
vodní	vodní	k2eAgFnPc4d1	vodní
cesty	cesta	k1gFnPc4	cesta
účelové	účelový	k2eAgFnPc4d1	účelová
<g/>
.	.	kIx.	.
</s>
<s>
Viz	vidět	k5eAaImRp2nS	vidět
též	též	k9	též
Přívozy	přívoz	k1gInPc4	přívoz
na	na	k7c6	na
Sázavě	Sázava	k1gFnSc6	Sázava
<g/>
.	.	kIx.	.
</s>
<s>
Žďár	Žďár	k1gInSc1	Žďár
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
Přibyslav	Přibyslava	k1gFnPc2	Přibyslava
Havlíčkův	Havlíčkův	k2eAgInSc1d1	Havlíčkův
Brod	Brod	k1gInSc1	Brod
Světlá	světlat	k5eAaImIp3nS	světlat
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
Ledeč	Ledeč	k1gInSc4	Ledeč
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
Zruč	Zruč	k1gInSc4	Zruč
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
Sázava	Sázava	k1gFnSc1	Sázava
Týnec	Týnec	k1gInSc1	Týnec
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
</s>
