<p>
<s>
Tukotuko	Tukotuko	k1gNnSc1	Tukotuko
(	(	kIx(	(
<g/>
Ctenomys	Ctenomys	k1gInSc1	Ctenomys
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rodové	rodový	k2eAgNnSc4d1	rodové
jméno	jméno	k1gNnSc4	jméno
asi	asi	k9	asi
50	[number]	k4	50
druhů	druh	k1gInPc2	druh
malých	malý	k2eAgMnPc2d1	malý
jihoamerických	jihoamerický	k2eAgMnPc2d1	jihoamerický
hlodavců	hlodavec	k1gMnPc2	hlodavec
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
příbuzní	příbuzný	k1gMnPc1	příbuzný
dikobrazů	dikobraz	k1gMnPc2	dikobraz
<g/>
,	,	kIx,	,
morčat	morče	k1gNnPc2	morče
a	a	k8xC	a
kapybar	kapybara	k1gFnPc2	kapybara
jsou	být	k5eAaImIp3nP	být
jihoamerickou	jihoamerický	k2eAgFnSc7d1	jihoamerická
verzí	verze	k1gFnSc7	verze
pytlonošů	pytlonoš	k1gMnPc2	pytlonoš
a	a	k8xC	a
hrabošů	hraboš	k1gMnPc2	hraboš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tukotukové	Tukotuk	k1gMnPc1	Tukotuk
jsou	být	k5eAaImIp3nP	být
drobní	drobný	k2eAgMnPc1d1	drobný
podsadití	podsaditý	k2eAgMnPc1d1	podsaditý
hlodavci	hlodavec	k1gMnPc1	hlodavec
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
hlavou	hlava	k1gFnSc7	hlava
a	a	k8xC	a
krátkýma	krátký	k2eAgFnPc7d1	krátká
nohama	noha	k1gFnPc7	noha
přizpůsobenýma	přizpůsobený	k2eAgFnPc7d1	přizpůsobená
k	k	k7c3	k
hrabání	hrabání	k1gNnSc3	hrabání
nor	nora	k1gFnPc2	nora
<g/>
;	;	kIx,	;
jejich	jejich	k3xOp3gInSc4	jejich
krátký	krátký	k2eAgInSc4d1	krátký
ocásek	ocásek	k1gInSc4	ocásek
je	být	k5eAaImIp3nS	být
osrstěný	osrstěný	k2eAgMnSc1d1	osrstěný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Druhy	druh	k1gInPc4	druh
==	==	k?	==
</s>
</p>
<p>
<s>
tukotuko	tukotuko	k6eAd1	tukotuko
argentinský	argentinský	k2eAgMnSc1d1	argentinský
(	(	kIx(	(
<g/>
Ctenomys	Ctenomys	k1gInSc1	Ctenomys
argentinus	argentinus	k1gInSc1	argentinus
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
tukotuko	tukotuko	k6eAd1	tukotuko
jižní	jižní	k2eAgInSc1d1	jižní
(	(	kIx(	(
<g/>
Ctenomys	Ctenomys	k1gInSc1	Ctenomys
australis	australis	k1gFnSc2	australis
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
tukotuko	tukotuko	k6eAd1	tukotuko
Azarův	Azarův	k2eAgInSc1d1	Azarův
(	(	kIx(	(
<g/>
Ctenomys	Ctenomys	k1gInSc1	Ctenomys
azarae	azara	k1gInSc2	azara
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
tukotuko	tukotuko	k6eAd1	tukotuko
bolivijský	bolivijský	k2eAgInSc4d1	bolivijský
(	(	kIx(	(
<g/>
Ctenomys	Ctenomys	k1gInSc4	Ctenomys
boliviensis	boliviensis	k1gFnSc2	boliviensis
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
tukotuko	tukotuko	k6eAd1	tukotuko
Bonettův	Bonettův	k2eAgInSc1d1	Bonettův
(	(	kIx(	(
<g/>
Ctenomys	Ctenomys	k1gInSc1	Ctenomys
bonettoi	bonetto	k1gFnSc2	bonetto
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
tukotuko	tukotuko	k6eAd1	tukotuko
brazilský	brazilský	k2eAgInSc1d1	brazilský
(	(	kIx(	(
<g/>
Ctenomys	Ctenomys	k1gInSc1	Ctenomys
brasiliensis	brasiliensis	k1gFnSc2	brasiliensis
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
tukotuko	tukotuko	k6eAd1	tukotuko
bělobřichý	bělobřichý	k2eAgInSc1d1	bělobřichý
(	(	kIx(	(
<g/>
Ctenomys	Ctenomys	k1gInSc1	Ctenomys
colburni	colburnit	k5eAaPmRp2nS	colburnit
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
tukotuko	tukotuko	k6eAd1	tukotuko
Connoverův	Connoverův	k2eAgInSc1d1	Connoverův
(	(	kIx(	(
<g/>
Ctenomys	Ctenomys	k1gInSc1	Ctenomys
conoveri	conover	k1gFnSc2	conover
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
tukotuko	tukotuko	k6eAd1	tukotuko
čako	čako	k6eAd1	čako
(	(	kIx(	(
<g/>
Ctenomys	Ctenomys	k1gInSc1	Ctenomys
dorsalis	dorsalis	k1gFnSc2	dorsalis
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
tukotuko	tukotuko	k6eAd1	tukotuko
Emilův	Emilův	k2eAgInSc1d1	Emilův
(	(	kIx(	(
<g/>
Ctenomys	Ctenomys	k1gInSc1	Ctenomys
emilianus	emilianus	k1gInSc1	emilianus
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ctenomys	Ctenomys	k6eAd1	Ctenomys
frater	frater	k1gInSc1	frater
</s>
</p>
<p>
<s>
Ctenomys	Ctenomys	k6eAd1	Ctenomys
fulvus	fulvus	k1gInSc1	fulvus
</s>
</p>
<p>
<s>
tukotuko	tukotuko	k6eAd1	tukotuko
Haigův	Haigův	k2eAgInSc1d1	Haigův
(	(	kIx(	(
<g/>
Ctenomys	Ctenomys	k1gInSc1	Ctenomys
haigi	haig	k1gFnSc2	haig
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ctenomys	Ctenomys	k1gInSc1	Ctenomys
knighti	knighť	k1gFnSc2	knighť
</s>
</p>
<p>
<s>
Ctenomys	Ctenomys	k6eAd1	Ctenomys
latro	latro	k6eAd1	latro
</s>
</p>
<p>
<s>
Ctenomys	Ctenomys	k6eAd1	Ctenomys
leucodon	leucodon	k1gInSc1	leucodon
</s>
</p>
<p>
<s>
tukotuko	tukotuko	k6eAd1	tukotuko
Lewisův	Lewisův	k2eAgInSc1d1	Lewisův
(	(	kIx(	(
<g/>
Ctenomys	Ctenomys	k1gInSc1	Ctenomys
lewisi	lewise	k1gFnSc4	lewise
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
tukotuko	tukotuko	k6eAd1	tukotuko
magelánský	magelánský	k2eAgInSc1d1	magelánský
(	(	kIx(	(
<g/>
Ctenomys	Ctenomys	k1gInSc1	Ctenomys
magellanicus	magellanicus	k1gInSc1	magellanicus
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
tukotuko	tukotuko	k6eAd1	tukotuko
maulský	maulský	k2eAgInSc1d1	maulský
(	(	kIx(	(
<g/>
Ctenomys	Ctenomys	k1gInSc1	Ctenomys
maulinus	maulinus	k1gInSc1	maulinus
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
tukotuko	tukotuko	k6eAd1	tukotuko
mendózský	mendózský	k2eAgInSc1d1	mendózský
(	(	kIx(	(
<g/>
Ctenomys	Ctenomys	k1gInSc1	Ctenomys
mendocinus	mendocinus	k1gInSc1	mendocinus
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
tukotuko	tukotuko	k6eAd1	tukotuko
menší	malý	k2eAgInSc1d2	menší
(	(	kIx(	(
<g/>
Ctenomys	Ctenomys	k1gInSc1	Ctenomys
minutus	minutus	k1gInSc1	minutus
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
tukotuko	tukotuko	k6eAd1	tukotuko
Nattererův	Nattererův	k2eAgInSc1d1	Nattererův
(	(	kIx(	(
<g/>
Ctenomys	Ctenomys	k1gInSc1	Ctenomys
nattereri	natterer	k1gFnSc2	natterer
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ctenomys	Ctenomys	k6eAd1	Ctenomys
occultus	occultus	k1gInSc1	occultus
</s>
</p>
<p>
<s>
tukotuko	tukotuko	k6eAd1	tukotuko
vysokohorský	vysokohorský	k2eAgInSc1d1	vysokohorský
(	(	kIx(	(
<g/>
Ctenomys	Ctenomys	k1gInSc1	Ctenomys
opimus	opimus	k1gInSc1	opimus
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
tukotuko	tukotuko	k6eAd1	tukotuko
Pearsonův	Pearsonův	k2eAgInSc1d1	Pearsonův
(	(	kIx(	(
<g/>
Ctenomys	Ctenomys	k1gInSc1	Ctenomys
pearsoni	pearsoň	k1gFnSc3	pearsoň
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
tukotuko	tukotuko	k6eAd1	tukotuko
korrientský	korrientský	k2eAgInSc1d1	korrientský
(	(	kIx(	(
<g/>
Ctenomys	Ctenomys	k1gInSc1	Ctenomys
perrensis	perrensis	k1gFnSc2	perrensis
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
tukotuko	tukotuko	k6eAd1	tukotuko
peruánský	peruánský	k2eAgMnSc1d1	peruánský
(	(	kIx(	(
<g/>
Ctenomys	Ctenomys	k1gInSc1	Ctenomys
peruanus	peruanus	k1gInSc1	peruanus
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
tukotuko	tukotuko	k6eAd1	tukotuko
sanlujský	sanlujský	k2eAgInSc1d1	sanlujský
(	(	kIx(	(
<g/>
Ctenomys	Ctenomys	k1gInSc1	Ctenomys
pontifex	pontifex	k1gMnSc1	pontifex
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
tukotuko	tukotuko	k6eAd1	tukotuko
Porteousův	Porteousův	k2eAgInSc1d1	Porteousův
(	(	kIx(	(
<g/>
Ctenomys	Ctenomys	k1gInSc1	Ctenomys
porteousi	porteouse	k1gFnSc4	porteouse
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
tukotuko	tukotuko	k6eAd1	tukotuko
saltský	saltský	k2eAgInSc1d1	saltský
(	(	kIx(	(
<g/>
Ctenomys	Ctenomys	k1gInSc1	Ctenomys
saltarius	saltarius	k1gInSc1	saltarius
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ctenomys	Ctenomys	k6eAd1	Ctenomys
sericeus	sericeus	k1gInSc1	sericeus
</s>
</p>
<p>
<s>
Ctenomys	Ctenomys	k1gInSc1	Ctenomys
sociabilis	sociabilis	k1gFnSc2	sociabilis
</s>
</p>
<p>
<s>
tukotuko	tukotuko	k6eAd1	tukotuko
Steinbachův	Steinbachův	k2eAgInSc1d1	Steinbachův
(	(	kIx(	(
<g/>
Ctenomys	Ctenomys	k1gInSc1	Ctenomys
steinbachi	steinbach	k1gFnSc2	steinbach
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
tukotuko	tukotuko	k6eAd1	tukotuko
talaský	talaský	k2eAgInSc1d1	talaský
(	(	kIx(	(
<g/>
Ctenomys	Ctenomys	k1gInSc1	Ctenomys
talarum	talarum	k1gNnSc1	talarum
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ctenomys	Ctenomys	k6eAd1	Ctenomys
torquatus	torquatus	k1gInSc1	torquatus
</s>
</p>
<p>
<s>
Ctenomys	Ctenomys	k6eAd1	Ctenomys
tuconax	tuconax	k1gInSc1	tuconax
</s>
</p>
<p>
<s>
tukotuko	tukotuko	k6eAd1	tukotuko
tukumánský	tukumánský	k2eAgInSc1d1	tukumánský
(	(	kIx(	(
<g/>
Ctenomys	Ctenomys	k1gInSc1	Ctenomys
tucumanus	tucumanus	k1gInSc1	tucumanus
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ctenomys	Ctenomys	k6eAd1	Ctenomys
validus	validus	k1gInSc1	validus
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
tukotuko	tukotuko	k6eAd1	tukotuko
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Tukotuko	Tukotuko	k6eAd1	Tukotuko
na	na	k7c6	na
webu	web	k1gInSc6	web
Celý	celý	k2eAgInSc1d1	celý
svět	svět	k1gInSc1	svět
</s>
</p>
