<p>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
církev	církev	k1gFnSc1	církev
(	(	kIx(	(
<g/>
nazývána	nazýván	k2eAgFnSc1d1	nazývána
též	též	k9	též
Latinská	latinský	k2eAgFnSc1d1	Latinská
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
běžné	běžný	k2eAgFnSc6d1	běžná
mluvě	mluva	k1gFnSc6	mluva
často	často	k6eAd1	často
označovaná	označovaný	k2eAgFnSc1d1	označovaná
nepřesně	přesně	k6eNd1	přesně
jako	jako	k8xC	jako
katolická	katolický	k2eAgFnSc1d1	katolická
(	(	kIx(	(
<g/>
všeobecná	všeobecný	k2eAgFnSc1d1	všeobecná
<g/>
,	,	kIx,	,
univerzální	univerzální	k2eAgFnSc1d1	univerzální
<g/>
)	)	kIx)	)
církev	církev	k1gFnSc1	církev
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc1d3	veliký
z	z	k7c2	z
třiadvaceti	třiadvacet	k4xCc2	třiadvacet
autonomních	autonomní	k2eAgFnPc2d1	autonomní
katolických	katolický	k2eAgFnPc2d1	katolická
církví	církev	k1gFnPc2	církev
a	a	k8xC	a
jedinou	jediný	k2eAgFnSc7d1	jediná
západní	západní	k2eAgFnSc7d1	západní
katolickou	katolický	k2eAgFnSc7d1	katolická
církví	církev	k1gFnSc7	církev
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
1,13	[number]	k4	1,13
miliardy	miliarda	k4xCgFnSc2	miliarda
pokřtěných	pokřtěný	k2eAgMnPc2d1	pokřtěný
římských	římský	k2eAgMnPc2d1	římský
katolíků	katolík	k1gMnPc2	katolík
činí	činit	k5eAaImIp3nS	činit
římskokatolickou	římskokatolický	k2eAgFnSc4d1	Římskokatolická
církev	církev	k1gFnSc4	církev
největší	veliký	k2eAgFnSc7d3	veliký
křesťanskou	křesťanský	k2eAgFnSc7d1	křesťanská
církví	církev	k1gFnSc7	církev
(	(	kIx(	(
<g/>
zahrnující	zahrnující	k2eAgFnSc4d1	zahrnující
polovinu	polovina	k1gFnSc4	polovina
všech	všecek	k3xTgMnPc2	všecek
křesťanů	křesťan	k1gMnPc2	křesťan
<g/>
)	)	kIx)	)
a	a	k8xC	a
největší	veliký	k2eAgFnSc7d3	veliký
náboženskou	náboženský	k2eAgFnSc7d1	náboženská
organizací	organizace	k1gFnSc7	organizace
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
zdaleka	zdaleka	k6eAd1	zdaleka
největší	veliký	k2eAgFnSc7d3	veliký
organizovanou	organizovaný	k2eAgFnSc7d1	organizovaná
náboženskou	náboženský	k2eAgFnSc7d1	náboženská
skupinou	skupina	k1gFnSc7	skupina
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
církvi	církev	k1gFnSc3	církev
podle	podle	k7c2	podle
posledního	poslední	k2eAgNnSc2d1	poslední
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
hlásí	hlásit	k5eAaImIp3nS	hlásit
1	[number]	k4	1
083	[number]	k4	083
899	[number]	k4	899
lidí	člověk	k1gMnPc2	člověk
(	(	kIx(	(
<g/>
10,26	[number]	k4	10,26
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
Česka	Česko	k1gNnSc2	Česko
<g/>
,	,	kIx,	,
18,7	[number]	k4	18,7
%	%	kIx~	%
z	z	k7c2	z
odpovídajících	odpovídající	k2eAgMnPc2d1	odpovídající
respondentů	respondent	k1gMnPc2	respondent
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
absolutních	absolutní	k2eAgNnPc6d1	absolutní
číslech	číslo	k1gNnPc6	číslo
poklesl	poklesnout	k5eAaPmAgInS	poklesnout
počet	počet	k1gInSc1	počet
lidí	člověk	k1gMnPc2	člověk
preferujících	preferující	k2eAgFnPc2d1	preferující
tuto	tento	k3xDgFnSc4	tento
církev	církev	k1gFnSc1	církev
na	na	k7c6	na
40	[number]	k4	40
%	%	kIx~	%
stavu	stav	k1gInSc2	stav
oproti	oproti	k7c3	oproti
roku	rok	k1gInSc3	rok
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
Římskokatolické	římskokatolický	k2eAgFnSc3d1	Římskokatolická
církvi	církev	k1gFnSc3	církev
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nS	hlásit
49,83	[number]	k4	49,83
<g/>
%	%	kIx~	%
lidí	člověk	k1gMnPc2	člověk
z	z	k7c2	z
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
všech	všecek	k3xTgMnPc2	všecek
věřících	věřící	k1gMnPc2	věřící
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
společenství	společenství	k1gNnSc2	společenství
s	s	k7c7	s
římským	římský	k2eAgMnSc7d1	římský
biskupem	biskup	k1gMnSc7	biskup
<g/>
,	,	kIx,	,
papežem	papež	k1gMnSc7	papež
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
spojuje	spojovat	k5eAaImIp3nS	spojovat
všechny	všechen	k3xTgFnPc4	všechen
katolické	katolický	k2eAgFnPc4d1	katolická
církve	církev	k1gFnPc4	církev
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
římskokatolickou	římskokatolický	k2eAgFnSc4d1	Římskokatolická
církev	církev	k1gFnSc4	církev
typický	typický	k2eAgInSc4d1	typický
latinský	latinský	k2eAgInSc4d1	latinský
ritus	ritus	k1gInSc4	ritus
<g/>
;	;	kIx,	;
až	až	k9	až
na	na	k7c4	na
výjimky	výjimka	k1gFnPc4	výjimka
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
římský	římský	k2eAgInSc4d1	římský
ritus	ritus	k1gInSc4	ritus
<g/>
,	,	kIx,	,
místně	místně	k6eAd1	místně
církev	církev	k1gFnSc1	církev
užívá	užívat	k5eAaImIp3nS	užívat
i	i	k9	i
jiných	jiný	k2eAgInPc2d1	jiný
ritů	rit	k1gInPc2	rit
(	(	kIx(	(
<g/>
milánská	milánský	k2eAgFnSc1d1	Milánská
arcidiecéze	arcidiecéze	k1gFnSc1	arcidiecéze
užívá	užívat	k5eAaImIp3nS	užívat
ambrosiánský	ambrosiánský	k2eAgInSc4d1	ambrosiánský
ritus	ritus	k1gInSc4	ritus
<g/>
,	,	kIx,	,
v	v	k7c6	v
Zairu	Zairo	k1gNnSc6	Zairo
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
upraveného	upravený	k2eAgInSc2d1	upravený
ritu	rit	k1gInSc2	rit
<g/>
,	,	kIx,	,
označovaného	označovaný	k2eAgInSc2d1	označovaný
často	často	k6eAd1	často
jako	jako	k8xC	jako
zairský	zairský	k2eAgInSc1d1	zairský
ritus	ritus	k1gInSc1	ritus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Počátky	počátek	k1gInPc1	počátek
římskokatolické	římskokatolický	k2eAgFnSc2d1	Římskokatolická
církve	církev	k1gFnSc2	církev
jakožto	jakožto	k8xS	jakožto
samostatné	samostatný	k2eAgFnPc4d1	samostatná
organizace	organizace	k1gFnPc4	organizace
oddělené	oddělený	k2eAgFnPc4d1	oddělená
od	od	k7c2	od
většiny	většina	k1gFnSc2	většina
východních	východní	k2eAgFnPc2d1	východní
církví	církev	k1gFnPc2	církev
souvisejí	souviset	k5eAaImIp3nP	souviset
s	s	k7c7	s
Velkým	velký	k2eAgNnSc7d1	velké
schizmatem	schizma	k1gNnSc7	schizma
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1054	[number]	k4	1054
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
si	se	k3xPyFc3	se
papež	papež	k1gMnSc1	papež
<g/>
,	,	kIx,	,
římský	římský	k2eAgMnSc1d1	římský
patriarcha	patriarcha	k1gMnSc1	patriarcha
<g/>
,	,	kIx,	,
nárokoval	nárokovat	k5eAaImAgMnS	nárokovat
moc	moc	k6eAd1	moc
nad	nad	k7c7	nad
čtyřmi	čtyři	k4xCgInPc7	čtyři
východními	východní	k2eAgInPc7d1	východní
patriarcháty	patriarchát	k1gInPc7	patriarchát
<g/>
,	,	kIx,	,
ty	ten	k3xDgInPc1	ten
se	se	k3xPyFc4	se
odmítly	odmítnout	k5eAaPmAgInP	odmítnout
podrobit	podrobit	k5eAaPmF	podrobit
a	a	k8xC	a
církevní	církevní	k2eAgMnPc1d1	církevní
představitelé	představitel	k1gMnPc1	představitel
Říma	Řím	k1gInSc2	Řím
a	a	k8xC	a
Konstantinopole	Konstantinopol	k1gInSc2	Konstantinopol
se	se	k3xPyFc4	se
po	po	k7c6	po
řadě	řada	k1gFnSc6	řada
teologických	teologický	k2eAgInPc2d1	teologický
i	i	k8xC	i
mocenských	mocenský	k2eAgInPc2d1	mocenský
sporů	spor	k1gInPc2	spor
vzájemně	vzájemně	k6eAd1	vzájemně
exkomunikovali	exkomunikovat	k5eAaBmAgMnP	exkomunikovat
a	a	k8xC	a
prokleli	proklít	k5eAaPmAgMnP	proklít
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
exkomunikace	exkomunikace	k1gFnSc1	exkomunikace
byla	být	k5eAaImAgFnS	být
zrušena	zrušit	k5eAaPmNgFnS	zrušit
teprve	teprve	k6eAd1	teprve
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
2	[number]	k4	2
<g/>
.	.	kIx.	.
vatikánského	vatikánský	k2eAgInSc2d1	vatikánský
koncilu	koncil	k1gInSc2	koncil
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
<g/>
.	.	kIx.	.
</s>
<s>
Metaforicky	metaforicky	k6eAd1	metaforicky
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
církev	církev	k1gFnSc1	církev
opět	opět	k6eAd1	opět
dýchá	dýchat	k5eAaImIp3nS	dýchat
oběma	dva	k4xCgFnPc7	dva
plícemi	plíce	k1gFnPc7	plíce
<g/>
.	.	kIx.	.
</s>
<s>
Katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
přímou	přímý	k2eAgFnSc4d1	přímá
pokračovatelku	pokračovatelka	k1gFnSc4	pokračovatelka
prvotní	prvotní	k2eAgFnSc2d1	prvotní
církve	církev	k1gFnSc2	církev
založené	založený	k2eAgNnSc1d1	založené
Kristem	Kristus	k1gMnSc7	Kristus
<g/>
,	,	kIx,	,
římského	římský	k2eAgMnSc2d1	římský
biskupa	biskup	k1gMnSc2	biskup
označuje	označovat	k5eAaImIp3nS	označovat
za	za	k7c4	za
přímého	přímý	k2eAgMnSc4d1	přímý
následníka	následník	k1gMnSc4	následník
sv.	sv.	kA	sv.
Petra	Petr	k1gMnSc4	Petr
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgMnSc4	jenž
Kristus	Kristus	k1gMnSc1	Kristus
dle	dle	k7c2	dle
ní	on	k3xPp3gFnSc2	on
učinil	učinit	k5eAaImAgMnS	učinit
hlavou	hlava	k1gFnSc7	hlava
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
a	a	k8xC	a
biskupy	biskup	k1gInPc1	biskup
těch	ten	k3xDgFnPc2	ten
církví	církev	k1gFnPc2	církev
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
dodržují	dodržovat	k5eAaImIp3nP	dodržovat
při	při	k7c6	při
svěcení	svěcení	k1gNnSc6	svěcení
apoštolskou	apoštolský	k2eAgFnSc4d1	apoštolská
posloupnost	posloupnost	k1gFnSc4	posloupnost
<g/>
,	,	kIx,	,
za	za	k7c4	za
přímé	přímý	k2eAgMnPc4d1	přímý
následníky	následník	k1gMnPc4	následník
dvanácti	dvanáct	k4xCc2	dvanáct
apoštolů	apoštol	k1gMnPc2	apoštol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Věřící	věřící	k1gMnPc5	věřící
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
vatikánské	vatikánský	k2eAgFnSc2d1	Vatikánská
ročenky	ročenka	k1gFnSc2	ročenka
Annuario	Annuario	k6eAd1	Annuario
Pontificio	Pontificio	k6eAd1	Pontificio
měla	mít	k5eAaImAgFnS	mít
římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
církev	církev	k1gFnSc1	církev
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
1	[number]	k4	1
272	[number]	k4	272
000	[number]	k4	000
000	[number]	k4	000
členů	člen	k1gMnPc2	člen
(	(	kIx(	(
<g/>
pokřtěných	pokřtěný	k2eAgMnPc2d1	pokřtěný
katolíků	katolík	k1gMnPc2	katolík
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ji	on	k3xPp3gFnSc4	on
činí	činit	k5eAaImIp3nS	činit
největší	veliký	k2eAgFnSc7d3	veliký
křesťanskou	křesťanský	k2eAgFnSc7d1	křesťanská
církví	církev	k1gFnSc7	církev
(	(	kIx(	(
<g/>
zahrnující	zahrnující	k2eAgMnPc1d1	zahrnující
celosvětově	celosvětově	k6eAd1	celosvětově
asi	asi	k9	asi
polovinu	polovina	k1gFnSc4	polovina
všech	všecek	k3xTgMnPc2	všecek
křesťanů	křesťan	k1gMnPc2	křesťan
<g/>
)	)	kIx)	)
a	a	k8xC	a
největší	veliký	k2eAgFnSc7d3	veliký
náboženskou	náboženský	k2eAgFnSc7d1	náboženská
organizací	organizace	k1gFnSc7	organizace
na	na	k7c6	na
světě	svět	k1gInSc6	svět
(	(	kIx(	(
<g/>
zahrnující	zahrnující	k2eAgFnSc1d1	zahrnující
cca	cca	kA	cca
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
6	[number]	k4	6
lidstva	lidstvo	k1gNnSc2	lidstvo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Působí	působit	k5eAaImIp3nS	působit
celosvětově	celosvětově	k6eAd1	celosvětově
<g/>
,	,	kIx,	,
počet	počet	k1gInSc1	počet
jejích	její	k3xOp3gMnPc2	její
příznivců	příznivec	k1gMnPc2	příznivec
se	se	k3xPyFc4	se
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
oblastech	oblast	k1gFnPc6	oblast
liší	lišit	k5eAaImIp3nS	lišit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejkatoličtějším	katolický	k2eAgInSc7d3	katolický
kontinentem	kontinent	k1gInSc7	kontinent
světa	svět	k1gInSc2	svět
je	být	k5eAaImIp3nS	být
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
církev	církev	k1gFnSc1	církev
drtivě	drtivě	k6eAd1	drtivě
převládá	převládat	k5eAaImIp3nS	převládat
v	v	k7c6	v
prakticky	prakticky	k6eAd1	prakticky
celé	celý	k2eAgFnSc6d1	celá
Jižní	jižní	k2eAgFnSc3d1	jižní
a	a	k8xC	a
Střední	střední	k2eAgFnSc3d1	střední
Americe	Amerika	k1gFnSc3	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Hlásí	hlásit	k5eAaImIp3nS	hlásit
se	se	k3xPyFc4	se
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
polovina	polovina	k1gFnSc1	polovina
věřících	věřící	k2eAgMnPc2d1	věřící
obyvatel	obyvatel	k1gMnPc2	obyvatel
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
a	a	k8xC	a
některých	některý	k3yIgInPc6	některý
ostrovních	ostrovní	k2eAgInPc6d1	ostrovní
státech	stát	k1gInPc6	stát
Karibiku	Karibik	k1gInSc2	Karibik
je	být	k5eAaImIp3nS	být
počet	počet	k1gInSc1	počet
katolíků	katolík	k1gMnPc2	katolík
převýšen	převýšit	k5eAaPmNgInS	převýšit
počtem	počet	k1gInSc7	počet
protestantů	protestant	k1gMnPc2	protestant
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
je	být	k5eAaImIp3nS	být
celkem	celkem	k6eAd1	celkem
77,3	[number]	k4	77,3
milionů	milion	k4xCgInPc2	milion
věřících	věřící	k2eAgMnPc2d1	věřící
katolíků	katolík	k1gMnPc2	katolík
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
polovina	polovina	k1gFnSc1	polovina
všech	všecek	k3xTgMnPc2	všecek
věřících	věřící	k2eAgMnPc2d1	věřící
katolíků	katolík	k1gMnPc2	katolík
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
následuje	následovat	k5eAaImIp3nS	následovat
Evropa	Evropa	k1gFnSc1	Evropa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
udává	udávat	k5eAaImIp3nS	udávat
podíl	podíl	k1gInSc1	podíl
katolíků	katolík	k1gMnPc2	katolík
v	v	k7c6	v
rozpětí	rozpětí	k1gNnSc6	rozpětí
30	[number]	k4	30
<g/>
–	–	k?	–
<g/>
40	[number]	k4	40
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
jedna	jeden	k4xCgFnSc1	jeden
čtvrtina	čtvrtina	k1gFnSc1	čtvrtina
z	z	k7c2	z
celosvětového	celosvětový	k2eAgInSc2d1	celosvětový
počtu	počet	k1gInSc2	počet
věřících	věřící	k2eAgMnPc2d1	věřící
katolíků	katolík	k1gMnPc2	katolík
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
se	se	k3xPyFc4	se
k	k	k7c3	k
Římskokatolické	římskokatolický	k2eAgFnSc3d1	Římskokatolická
církvi	církev	k1gFnSc3	církev
hlásí	hlásit	k5eAaImIp3nS	hlásit
25	[number]	k4	25
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
asi	asi	k9	asi
jedna	jeden	k4xCgFnSc1	jeden
šestina	šestina	k1gFnSc1	šestina
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Nejméně	málo	k6eAd3	málo
katolickým	katolický	k2eAgInSc7d1	katolický
kontinentem	kontinent	k1gInSc7	kontinent
je	být	k5eAaImIp3nS	být
Asie	Asie	k1gFnSc1	Asie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
katolíci	katolík	k1gMnPc1	katolík
představují	představovat	k5eAaImIp3nP	představovat
na	na	k7c6	na
území	území	k1gNnSc6	území
drtivé	drtivý	k2eAgFnSc2d1	drtivá
většiny	většina	k1gFnSc2	většina
asijských	asijský	k2eAgInPc2d1	asijský
států	stát	k1gInPc2	stát
zanedbatelnou	zanedbatelný	k2eAgFnSc4d1	zanedbatelná
menšinu	menšina	k1gFnSc4	menšina
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
pronásledovanou	pronásledovaný	k2eAgFnSc4d1	pronásledovaná
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
tvoří	tvořit	k5eAaImIp3nP	tvořit
římští	římský	k2eAgMnPc1d1	římský
katolíci	katolík	k1gMnPc1	katolík
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
asijských	asijský	k2eAgInPc6d1	asijský
státech	stát	k1gInPc6	stát
(	(	kIx(	(
<g/>
Filipíny	Filipíny	k1gFnPc1	Filipíny
a	a	k8xC	a
Východní	východní	k2eAgInSc1d1	východní
Timor	Timor	k1gInSc1	Timor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
rychle	rychle	k6eAd1	rychle
roste	růst	k5eAaImIp3nS	růst
podíl	podíl	k1gInSc1	podíl
katolíků	katolík	k1gMnPc2	katolík
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
a	a	k8xC	a
Asii	Asie	k1gFnSc6	Asie
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgMnSc3	ten
ke	k	k7c3	k
stagnaci	stagnace	k1gFnSc3	stagnace
až	až	k8xS	až
poklesu	pokles	k1gInSc2	pokles
dochází	docházet	k5eAaImIp3nS	docházet
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
(	(	kIx(	(
<g/>
zde	zde	k6eAd1	zde
speciálně	speciálně	k6eAd1	speciálně
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
výrazně	výrazně	k6eAd1	výrazně
prosazují	prosazovat	k5eAaImIp3nP	prosazovat
evangelikálové	evangelikál	k1gMnPc1	evangelikál
<g/>
)	)	kIx)	)
a	a	k8xC	a
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dochází	docházet	k5eAaImIp3nS	docházet
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
států	stát	k1gInPc2	stát
k	k	k7c3	k
výraznému	výrazný	k2eAgInSc3d1	výrazný
postupu	postup	k1gInSc3	postup
sekularizace	sekularizace	k1gFnSc2	sekularizace
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Organizace	organizace	k1gFnSc2	organizace
==	==	k?	==
</s>
</p>
<p>
<s>
Coby	Coby	k?	Coby
organizaci	organizace	k1gFnSc4	organizace
určuje	určovat	k5eAaImIp3nS	určovat
fungování	fungování	k1gNnSc1	fungování
římskokatolické	římskokatolický	k2eAgFnSc2d1	Římskokatolická
církve	církev	k1gFnSc2	církev
Kodex	kodex	k1gInSc1	kodex
kánonického	kánonický	k2eAgNnSc2d1	kánonický
práva	právo	k1gNnSc2	právo
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
CIC	cic	k1gInSc1	cic
<g/>
,	,	kIx,	,
vydán	vydán	k2eAgInSc1d1	vydán
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Druhý	druhý	k4xOgInSc1	druhý
vatikánský	vatikánský	k2eAgInSc1d1	vatikánský
koncil	koncil	k1gInSc1	koncil
deklaroval	deklarovat	k5eAaBmAgInS	deklarovat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
konstituci	konstituce	k1gFnSc6	konstituce
Lumen	lumen	k1gNnSc1	lumen
gentium	gentium	k1gNnSc1	gentium
(	(	kIx(	(
<g/>
čl	čl	kA	čl
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
jedna	jeden	k4xCgFnSc1	jeden
<g/>
,	,	kIx,	,
svatá	svatý	k2eAgFnSc1d1	svatá
<g/>
,	,	kIx,	,
katolická	katolický	k2eAgFnSc1d1	katolická
a	a	k8xC	a
apoštolská	apoštolský	k2eAgFnSc1d1	apoštolská
<g/>
"	"	kIx"	"
církev	církev	k1gFnSc1	církev
"	"	kIx"	"
<g/>
subsistuje	subsistovat	k5eAaBmIp3nS	subsistovat
(	(	kIx(	(
<g/>
subsistit	subsistit	k5eAaPmF	subsistit
<g/>
,	,	kIx,	,
uskutečňuje	uskutečňovat	k5eAaImIp3nS	uskutečňovat
se	se	k3xPyFc4	se
<g/>
)	)	kIx)	)
v	v	k7c6	v
katolické	katolický	k2eAgFnSc6d1	katolická
církvi	církev	k1gFnSc6	církev
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
řízena	řízen	k2eAgFnSc1d1	řízena
Petrovým	Petrův	k2eAgMnSc7d1	Petrův
nástupcem	nástupce	k1gMnSc7	nástupce
a	a	k8xC	a
biskupy	biskup	k1gMnPc7	biskup
ve	v	k7c6	v
společenství	společenství	k1gNnSc6	společenství
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Petrovým	Petrův	k2eAgMnSc7d1	Petrův
nástupcem	nástupce	k1gMnSc7	nástupce
se	se	k3xPyFc4	se
míní	mínit	k5eAaImIp3nS	mínit
papež	papež	k1gMnSc1	papež
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Církevní	církevní	k2eAgFnSc1d1	církevní
správa	správa	k1gFnSc1	správa
se	se	k3xPyFc4	se
člení	členit	k5eAaImIp3nS	členit
na	na	k7c6	na
jurisdikční	jurisdikční	k2eAgFnSc6d1	jurisdikční
oblasti	oblast	k1gFnSc6	oblast
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
vymezené	vymezený	k2eAgFnPc1d1	vymezená
na	na	k7c6	na
územním	územní	k2eAgInSc6d1	územní
základě	základ	k1gInSc6	základ
<g/>
,	,	kIx,	,
existují	existovat	k5eAaImIp3nP	existovat
však	však	k9	však
i	i	k9	i
jiné	jiný	k2eAgFnPc4d1	jiná
možnosti	možnost	k1gFnPc4	možnost
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
vymezení	vymezení	k1gNnPc4	vymezení
instituční	instituční	k2eAgNnPc4d1	instituční
<g/>
,	,	kIx,	,
kupříkladu	kupříkladu	k6eAd1	kupříkladu
vojenský	vojenský	k2eAgInSc1d1	vojenský
ordinariát	ordinariát	k1gInSc1	ordinariát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
základní	základní	k2eAgFnSc4d1	základní
a	a	k8xC	a
standardní	standardní	k2eAgFnSc4d1	standardní
jednotku	jednotka	k1gFnSc4	jednotka
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
diecézi	diecéze	k1gFnSc4	diecéze
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
dále	daleko	k6eAd2	daleko
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c4	na
děkanáty	děkanát	k1gInPc4	děkanát
a	a	k8xC	a
tyto	tento	k3xDgFnPc4	tento
dále	daleko	k6eAd2	daleko
na	na	k7c6	na
farnosti	farnost	k1gFnSc6	farnost
<g/>
.	.	kIx.	.
</s>
<s>
Diecéze	diecéze	k1gFnPc1	diecéze
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
sdružovat	sdružovat	k5eAaImF	sdružovat
v	v	k7c6	v
církevní	církevní	k2eAgFnSc6d1	církevní
metropoli	metropol	k1gFnSc6	metropol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vztah	vztah	k1gInSc1	vztah
k	k	k7c3	k
ostatním	ostatní	k2eAgFnPc3d1	ostatní
katolickým	katolický	k2eAgFnPc3d1	katolická
církvím	církev	k1gFnPc3	církev
===	===	k?	===
</s>
</p>
<p>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
církev	církev	k1gFnSc1	církev
sdílí	sdílet	k5eAaImIp3nS	sdílet
s	s	k7c7	s
ostatními	ostatní	k2eAgFnPc7d1	ostatní
katolickými	katolický	k2eAgFnPc7d1	katolická
církvemi	církev	k1gFnPc7	církev
společenství	společenství	k1gNnSc2	společenství
ve	v	k7c6	v
svátostech	svátost	k1gFnPc6	svátost
a	a	k8xC	a
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
věřícím	věřící	k1gMnPc3	věřící
z	z	k7c2	z
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
katolických	katolický	k2eAgFnPc2d1	katolická
církví	církev	k1gFnPc2	církev
za	za	k7c2	za
určitých	určitý	k2eAgFnPc2d1	určitá
podmínek	podmínka	k1gFnPc2	podmínka
změnu	změna	k1gFnSc4	změna
příslušnosti	příslušnost	k1gFnSc2	příslušnost
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
církvi	církev	k1gFnSc3	církev
<g/>
,	,	kIx,	,
např.	např.	kA	např.
při	při	k7c6	při
migraci	migrace	k1gFnSc6	migrace
<g/>
,	,	kIx,	,
svatbě	svatba	k1gFnSc6	svatba
apod.	apod.	kA	apod.
<g/>
;	;	kIx,	;
teoreticky	teoreticky	k6eAd1	teoreticky
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
papežem	papež	k1gMnSc7	papež
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
biskupem	biskup	k1gMnSc7	biskup
církve	církev	k1gFnSc2	církev
latinského	latinský	k2eAgInSc2d1	latinský
obřadu	obřad	k1gInSc2	obřad
<g/>
,	,	kIx,	,
zvolen	zvolit	k5eAaPmNgMnS	zvolit
i	i	k8xC	i
věřící	věřící	k1gFnSc1	věřící
některé	některý	k3yIgFnSc2	některý
z	z	k7c2	z
východních	východní	k2eAgFnPc2d1	východní
katolických	katolický	k2eAgFnPc2d1	katolická
církví	církev	k1gFnPc2	církev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zdravotní	zdravotní	k2eAgNnPc1d1	zdravotní
a	a	k8xC	a
charitativní	charitativní	k2eAgNnPc1d1	charitativní
střediska	středisko	k1gNnPc1	středisko
==	==	k?	==
</s>
</p>
<p>
<s>
Katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
spravuje	spravovat	k5eAaImIp3nS	spravovat
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
5158	[number]	k4	5158
nemocnic	nemocnice	k1gFnPc2	nemocnice
(	(	kIx(	(
<g/>
nejvíce	hodně	k6eAd3	hodně
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
a	a	k8xC	a
Africe	Afrika	k1gFnSc6	Afrika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
16	[number]	k4	16
523	[number]	k4	523
ostatních	ostatní	k2eAgNnPc2d1	ostatní
zdravotních	zdravotní	k2eAgNnPc2d1	zdravotní
středisek	středisko	k1gNnPc2	středisko
<g/>
,	,	kIx,	,
612	[number]	k4	612
leprosárií	leprosárie	k1gFnPc2	leprosárie
<g/>
,	,	kIx,	,
15	[number]	k4	15
679	[number]	k4	679
domů	dům	k1gInPc2	dům
pro	pro	k7c4	pro
staré	starý	k2eAgMnPc4d1	starý
<g/>
,	,	kIx,	,
nemocné	mocný	k2eNgMnPc4d1	nemocný
a	a	k8xC	a
handicapované	handicapovaný	k2eAgMnPc4d1	handicapovaný
lidi	člověk	k1gMnPc4	člověk
(	(	kIx(	(
<g/>
nejvíce	nejvíce	k6eAd1	nejvíce
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
–	–	k?	–
8304	[number]	k4	8304
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
9492	[number]	k4	9492
sirotčinců	sirotčinec	k1gInPc2	sirotčinec
<g/>
,	,	kIx,	,
14	[number]	k4	14
576	[number]	k4	576
manželských	manželský	k2eAgFnPc2d1	manželská
poraden	poradna	k1gFnPc2	poradna
(	(	kIx(	(
<g/>
nejvíce	nejvíce	k6eAd1	nejvíce
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
5670	[number]	k4	5670
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
3782	[number]	k4	3782
sociálně-vzdělávacích	sociálnězdělávací	k2eAgNnPc2d1	sociálně-vzdělávací
středisek	středisko	k1gNnPc2	středisko
a	a	k8xC	a
37	[number]	k4	37
601	[number]	k4	601
dalších	další	k2eAgFnPc2d1	další
institucí	instituce	k1gFnPc2	instituce
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Vzdělávací	vzdělávací	k2eAgFnPc1d1	vzdělávací
a	a	k8xC	a
výchovné	výchovný	k2eAgFnPc1d1	výchovná
instituce	instituce	k1gFnPc1	instituce
==	==	k?	==
</s>
</p>
<p>
<s>
Katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
spravuje	spravovat	k5eAaImIp3nS	spravovat
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
73	[number]	k4	73
580	[number]	k4	580
mateřských	mateřský	k2eAgFnPc2d1	mateřská
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
navštěvuje	navštěvovat	k5eAaImIp3nS	navštěvovat
7	[number]	k4	7
043	[number]	k4	043
634	[number]	k4	634
dětí	dítě	k1gFnPc2	dítě
<g/>
;	;	kIx,	;
dále	daleko	k6eAd2	daleko
96	[number]	k4	96
283	[number]	k4	283
základních	základní	k2eAgFnPc2d1	základní
škol	škola	k1gFnPc2	škola
1	[number]	k4	1
<g/>
.	.	kIx.	.
stupně	stupeň	k1gInSc2	stupeň
pro	pro	k7c4	pro
33	[number]	k4	33
516	[number]	k4	516
860	[number]	k4	860
žáků	žák	k1gMnPc2	žák
<g/>
;	;	kIx,	;
46	[number]	k4	46
339	[number]	k4	339
základních	základní	k2eAgFnPc2d1	základní
škol	škola	k1gFnPc2	škola
2	[number]	k4	2
<g/>
.	.	kIx.	.
stupně	stupeň	k1gInSc2	stupeň
a	a	k8xC	a
středních	střední	k2eAgFnPc2d1	střední
škol	škola	k1gFnPc2	škola
pro	pro	k7c4	pro
19	[number]	k4	19
760	[number]	k4	760
924	[number]	k4	924
žáků	žák	k1gMnPc2	žák
<g/>
.	.	kIx.	.
</s>
<s>
Katolické	katolický	k2eAgFnPc4d1	katolická
vyšší	vysoký	k2eAgFnPc4d2	vyšší
školy	škola	k1gFnPc4	škola
navštěvuje	navštěvovat	k5eAaImIp3nS	navštěvovat
2	[number]	k4	2
477	[number]	k4	477
636	[number]	k4	636
studentů	student	k1gMnPc2	student
<g/>
,	,	kIx,	,
univerzity	univerzita	k1gFnSc2	univerzita
pak	pak	k6eAd1	pak
2	[number]	k4	2
719	[number]	k4	719
643	[number]	k4	643
studentů	student	k1gMnPc2	student
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Učení	učení	k1gNnSc2	učení
==	==	k?	==
</s>
</p>
<p>
<s>
Nauka	nauka	k1gFnSc1	nauka
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
se	se	k3xPyFc4	se
v	v	k7c6	v
základních	základní	k2eAgFnPc6d1	základní
–	–	k?	–
a	a	k8xC	a
nejpodstatnějších	podstatný	k2eAgInPc2d3	nejpodstatnější
–	–	k?	–
věcech	věc	k1gFnPc6	věc
shoduje	shodovat	k5eAaImIp3nS	shodovat
s	s	k7c7	s
naukou	nauka	k1gFnSc7	nauka
i	i	k8xC	i
ostatních	ostatní	k2eAgFnPc2d1	ostatní
církví	církev	k1gFnPc2	církev
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
svědčí	svědčit	k5eAaImIp3nS	svědčit
společná	společný	k2eAgNnPc4d1	společné
kréda	krédo	k1gNnPc4	krédo
<g/>
,	,	kIx,	,
zvl.	zvl.	kA	zvl.
Nicejsko-konstantinopolské	Nicejskoonstantinopolský	k2eAgNnSc1d1	Nicejsko-konstantinopolské
vyznání	vyznání	k1gNnSc1	vyznání
víry	víra	k1gFnSc2	víra
<g/>
.	.	kIx.	.
</s>
<s>
Souhrn	souhrn	k1gInSc1	souhrn
katolické	katolický	k2eAgFnSc2d1	katolická
věrouky	věrouka	k1gFnSc2	věrouka
je	být	k5eAaImIp3nS	být
obsažen	obsáhnout	k5eAaPmNgInS	obsáhnout
v	v	k7c6	v
Katechismu	katechismus	k1gInSc6	katechismus
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
zmiňujeme	zmiňovat	k5eAaImIp1nP	zmiňovat
některá	některý	k3yIgNnPc4	některý
specifika	specifikon	k1gNnPc4	specifikon
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgInPc7	jenž
se	se	k3xPyFc4	se
katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Svátosti	svátost	k1gFnSc6	svátost
===	===	k?	===
</s>
</p>
<p>
<s>
Katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
uznává	uznávat	k5eAaImIp3nS	uznávat
sedm	sedm	k4xCc4	sedm
svátostí	svátost	k1gFnPc2	svátost
(	(	kIx(	(
<g/>
čísla	číslo	k1gNnSc2	číslo
v	v	k7c6	v
seznamu	seznam	k1gInSc6	seznam
označují	označovat	k5eAaImIp3nP	označovat
příslušné	příslušný	k2eAgInPc4d1	příslušný
odstavce	odstavec	k1gInPc4	odstavec
Katechismu	katechismus	k1gInSc2	katechismus
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
(	(	kIx(	(
<g/>
KKC	KKC	kA	KKC
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Křest	křest	k1gInSc1	křest
<g/>
:	:	kIx,	:
KKC	KKC	kA	KKC
1213	[number]	k4	1213
<g/>
–	–	k?	–
<g/>
1284	[number]	k4	1284
</s>
</p>
<p>
<s>
Biřmování	biřmování	k1gNnSc1	biřmování
<g/>
:	:	kIx,	:
KKC	KKC	kA	KKC
1285	[number]	k4	1285
<g/>
–	–	k?	–
<g/>
1321	[number]	k4	1321
</s>
</p>
<p>
<s>
Eucharistie	eucharistie	k1gFnSc1	eucharistie
<g/>
:	:	kIx,	:
KKC	KKC	kA	KKC
1322	[number]	k4	1322
<g/>
–	–	k?	–
<g/>
1419	[number]	k4	1419
</s>
</p>
<p>
<s>
Svátost	svátost	k1gFnSc1	svátost
smíření	smíření	k1gNnSc2	smíření
<g/>
:	:	kIx,	:
KKC	KKC	kA	KKC
1422	[number]	k4	1422
<g/>
–	–	k?	–
<g/>
1498	[number]	k4	1498
</s>
</p>
<p>
<s>
Svátost	svátost	k1gFnSc1	svátost
nemocných	nemocný	k1gMnPc2	nemocný
<g/>
:	:	kIx,	:
KKC	KKC	kA	KKC
1499	[number]	k4	1499
<g/>
–	–	k?	–
<g/>
1532	[number]	k4	1532
</s>
</p>
<p>
<s>
Kněžství	kněžství	k1gNnSc1	kněžství
<g/>
:	:	kIx,	:
KKC	KKC	kA	KKC
1536	[number]	k4	1536
<g/>
–	–	k?	–
<g/>
1600	[number]	k4	1600
</s>
</p>
<p>
<s>
Manželství	manželství	k1gNnSc1	manželství
<g/>
:	:	kIx,	:
KKC	KKC	kA	KKC
1601	[number]	k4	1601
<g/>
–	–	k?	–
<g/>
1666	[number]	k4	1666
</s>
</p>
<p>
<s>
===	===	k?	===
Liturgie	liturgie	k1gFnSc1	liturgie
a	a	k8xC	a
modlitba	modlitba	k1gFnSc1	modlitba
===	===	k?	===
</s>
</p>
<p>
<s>
Středem	středem	k7c2	středem
života	život	k1gInSc2	život
církve	církev	k1gFnSc2	církev
je	být	k5eAaImIp3nS	být
liturgie	liturgie	k1gFnSc1	liturgie
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
hraje	hrát	k5eAaImIp3nS	hrát
nejdůležitější	důležitý	k2eAgFnSc4d3	nejdůležitější
úlohu	úloha	k1gFnSc4	úloha
eucharistická	eucharistický	k2eAgFnSc1d1	eucharistická
bohoslužba	bohoslužba	k1gFnSc1	bohoslužba
(	(	kIx(	(
<g/>
=	=	kIx~	=
mše	mše	k1gFnSc2	mše
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
liturgie	liturgie	k1gFnSc2	liturgie
je	být	k5eAaImIp3nS	být
též	též	k9	též
Denní	denní	k2eAgFnSc1d1	denní
modlitba	modlitba	k1gFnSc1	modlitba
církve	církev	k1gFnSc2	církev
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
breviář	breviář	k1gInSc1	breviář
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
též	též	k9	též
Liturgie	liturgie	k1gFnSc1	liturgie
hodin	hodina	k1gFnPc2	hodina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
modlitba	modlitba	k1gFnSc1	modlitba
založená	založený	k2eAgFnSc1d1	založená
na	na	k7c6	na
žalmech	žalm	k1gInPc6	žalm
a	a	k8xC	a
jiných	jiný	k2eAgInPc6d1	jiný
biblických	biblický	k2eAgInPc6d1	biblický
textech	text	k1gInPc6	text
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
uvádět	uvádět	k5eAaImF	uvádět
křesťana	křesťan	k1gMnSc4	křesťan
do	do	k7c2	do
života	život	k1gInSc2	život
v	v	k7c6	v
Boží	boží	k2eAgFnSc6d1	boží
přítomnosti	přítomnost	k1gFnSc6	přítomnost
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Krom	krom	k7c2	krom
toho	ten	k3xDgNnSc2	ten
existuje	existovat	k5eAaImIp3nS	existovat
v	v	k7c6	v
církvi	církev	k1gFnSc6	církev
mnoho	mnoho	k6eAd1	mnoho
dalších	další	k2eAgInPc2d1	další
způsobů	způsob	k1gInPc2	způsob
modlitby	modlitba	k1gFnSc2	modlitba
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
už	už	k6eAd1	už
spontánní	spontánní	k2eAgFnSc3d1	spontánní
nebo	nebo	k8xC	nebo
ustálené	ustálený	k2eAgFnSc3d1	ustálená
<g/>
,	,	kIx,	,
společné	společný	k2eAgFnSc3d1	společná
nebo	nebo	k8xC	nebo
soukromé	soukromý	k2eAgFnSc3d1	soukromá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dějiny	dějiny	k1gFnPc4	dějiny
Římskokatolické	římskokatolický	k2eAgFnSc2d1	Římskokatolická
církve	církev	k1gFnSc2	církev
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Církve	církev	k1gFnPc1	církev
ve	v	k7c6	v
společenství	společenství	k1gNnSc6	společenství
s	s	k7c7	s
Římsko-katolickou	římskoatolický	k2eAgFnSc7d1	římsko-katolická
církví	církev	k1gFnSc7	církev
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
křesťanství	křesťanství	k1gNnSc2	křesťanství
</s>
</p>
<p>
<s>
Katolictví	katolictví	k1gNnSc1	katolictví
</s>
</p>
<p>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
církev	církev	k1gFnSc1	církev
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
</s>
</p>
<p>
<s>
Sedm	sedm	k4xCc1	sedm
hlavních	hlavní	k2eAgInPc2d1	hlavní
hříchů	hřích	k1gInPc2	hřích
</s>
</p>
<p>
<s>
Inkvizice	inkvizice	k1gFnSc1	inkvizice
</s>
</p>
<p>
<s>
Sexuální	sexuální	k2eAgInPc4d1	sexuální
skandály	skandál	k1gInPc4	skandál
katolických	katolický	k2eAgMnPc2d1	katolický
duchovních	duchovní	k1gMnPc2	duchovní
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
světců	světec	k1gMnPc2	světec
a	a	k8xC	a
mučedníků	mučedník	k1gMnPc2	mučedník
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
církev	církev	k1gFnSc1	církev
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
</s>
</p>
<p>
<s>
Apoštolský	apoštolský	k2eAgInSc1d1	apoštolský
stolec	stolec	k1gInSc1	stolec
</s>
</p>
