<s>
Říjen	říjen	k1gInSc1	říjen
je	být	k5eAaImIp3nS	být
desátým	desátý	k4xOgInSc7	desátý
měsícem	měsíc	k1gInSc7	měsíc
roku	rok	k1gInSc2	rok
podle	podle	k7c2	podle
Gregoriánského	gregoriánský	k2eAgInSc2d1	gregoriánský
kalendáře	kalendář	k1gInSc2	kalendář
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
31	[number]	k4	31
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
české	český	k2eAgNnSc1d1	české
jméno	jméno	k1gNnSc1	jméno
je	být	k5eAaImIp3nS	být
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
od	od	k7c2	od
jelení	jelení	k2eAgFnSc2d1	jelení
říje	říje	k1gFnSc2	říje
<g/>
.	.	kIx.	.
</s>
<s>
Latinský	latinský	k2eAgInSc1d1	latinský
název	název	k1gInSc1	název
October	Octobero	k1gNnPc2	Octobero
znamená	znamenat	k5eAaImIp3nS	znamenat
osmý	osmý	k4xOgInSc4	osmý
měsíc	měsíc	k1gInSc4	měsíc
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
z	z	k7c2	z
římského	římský	k2eAgInSc2d1	římský
kalendáře	kalendář	k1gInSc2	kalendář
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
nějž	jenž	k3xRgNnSc2	jenž
rok	rok	k1gInSc1	rok
začínal	začínat	k5eAaImAgInS	začínat
březnem	březen	k1gInSc7	březen
<g/>
.	.	kIx.	.
</s>
<s>
Slunce	slunce	k1gNnSc1	slunce
je	být	k5eAaImIp3nS	být
začátkem	začátek	k1gInSc7	začátek
října	říjen	k1gInSc2	říjen
ve	v	k7c6	v
znamení	znamení	k1gNnSc6	znamení
vah	váha	k1gFnPc2	váha
a	a	k8xC	a
přechází	přecházet	k5eAaImIp3nS	přecházet
do	do	k7c2	do
znamení	znamení	k1gNnSc2	znamení
štíra	štír	k1gMnSc2	štír
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
měsíci	měsíc	k1gInSc6	měsíc
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
podzimní	podzimní	k2eAgFnSc4d1	podzimní
sklizeň	sklizeň	k1gFnSc4	sklizeň
a	a	k8xC	a
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
se	se	k3xPyFc4	se
tradičně	tradičně	k6eAd1	tradičně
slaví	slavit	k5eAaImIp3nS	slavit
posvícení	posvícení	k1gNnSc4	posvícení
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
říjen	říjen	k1gInSc1	říjen
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
