<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Madagaskaru	Madagaskar	k1gInSc2	Madagaskar
je	být	k5eAaImIp3nS	být
odvozená	odvozený	k2eAgFnSc1d1	odvozená
z	z	k7c2	z
vlajky	vlajka	k1gFnSc2	vlajka
kmene	kmen	k1gInSc2	kmen
Hovo	Hovo	k6eAd1	Hovo
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
bílý	bílý	k2eAgInSc1d1	bílý
svislý	svislý	k2eAgInSc1d1	svislý
pruh	pruh	k1gInSc1	pruh
u	u	k7c2	u
žerdi	žerď	k1gFnSc2	žerď
(	(	kIx(	(
<g/>
zabírá	zabírat	k5eAaImIp3nS	zabírat
třetinu	třetina	k1gFnSc4	třetina
délky	délka	k1gFnSc2	délka
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
)	)	kIx)	)
a	a	k8xC	a
dva	dva	k4xCgInPc4	dva
vodorovné	vodorovný	k2eAgInPc4d1	vodorovný
pruhy	pruh	k1gInPc4	pruh
v	v	k7c6	v
červené	červený	k2eAgFnSc6d1	červená
a	a	k8xC	a
zelené	zelený	k2eAgFnSc6d1	zelená
barvě	barva	k1gFnSc6	barva
<g/>
.	.	kIx.	.
</s>
<s>
Bílá	bílý	k2eAgFnSc1d1	bílá
barva	barva	k1gFnSc1	barva
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
čistotu	čistota	k1gFnSc4	čistota
ideálů	ideál	k1gInPc2	ideál
<g/>
,	,	kIx,	,
červená	červenat	k5eAaImIp3nS	červenat
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
,	,	kIx,	,
zelená	zelenat	k5eAaImIp3nS	zelenat
naději	naděje	k1gFnSc4	naděje
ve	v	k7c4	v
šťastnou	šťastný	k2eAgFnSc4d1	šťastná
budoucnost	budoucnost	k1gFnSc4	budoucnost
<g/>
.	.	kIx.	.
</s>
<s>
Bílá	bílý	k2eAgFnSc1d1	bílá
a	a	k8xC	a
červená	červený	k2eAgFnSc1d1	červená
barva	barva	k1gFnSc1	barva
byly	být	k5eAaImAgFnP	být
už	už	k6eAd1	už
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
na	na	k7c6	na
vlajce	vlajka	k1gFnSc6	vlajka
madagaskarského	madagaskarský	k2eAgNnSc2d1	madagaskarské
království	království	k1gNnSc2	království
Hovů	Hov	k1gInPc2	Hov
<g/>
,	,	kIx,	,
zelená	zelená	k1gFnSc1	zelená
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
obyvatele	obyvatel	k1gMnSc4	obyvatel
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Bílá	bílý	k2eAgFnSc1d1	bílá
spojuje	spojovat	k5eAaImIp3nS	spojovat
s	s	k7c7	s
knížecím	knížecí	k2eAgInSc7d1	knížecí
rodem	rod	k1gInSc7	rod
Volafotsů	Volafots	k1gMnPc2	Volafots
<g/>
,	,	kIx,	,
červená	červený	k2eAgFnSc1d1	červená
s	s	k7c7	s
rodem	rod	k1gInSc7	rod
Volamenů	Volamen	k1gMnPc2	Volamen
ze	z	k7c2	z
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
<g/>
,	,	kIx,	,
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
schválená	schválený	k2eAgFnSc1d1	schválená
ústavně	ústavně	k6eAd1	ústavně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Historicke	Historicke	k1gFnPc3	Historicke
prezidentské	prezidentský	k2eAgFnSc2d1	prezidentská
vlajky	vlajka	k1gFnSc2	vlajka
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
prvního	první	k4xOgMnSc2	první
madagaskarského	madagaskarský	k2eAgMnSc2d1	madagaskarský
prezidena	preziden	k1gMnSc2	preziden
Philibert	Philibert	k1gMnSc1	Philibert
Tsiranana	Tsiranan	k1gMnSc2	Tsiranan
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
ještě	ještě	k6eAd1	ještě
autonomní	autonomní	k2eAgFnSc2d1	autonomní
republiky	republika	k1gFnSc2	republika
<g/>
)	)	kIx)	)
schválena	schválit	k5eAaPmNgFnS	schválit
první	první	k4xOgFnSc1	první
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
již	již	k6eAd1	již
samostatné	samostatný	k2eAgFnSc6d1	samostatná
republice	republika	k1gFnSc6	republika
schválena	schválit	k5eAaPmNgFnS	schválit
pro	pro	k7c4	pro
stejného	stejný	k2eAgInSc2d1	stejný
prezidenta	prezident	k1gMnSc2	prezident
nová	nový	k2eAgFnSc1d1	nová
personalizovaná	personalizovaný	k2eAgFnSc1d1	personalizovaná
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Vlajky	vlajka	k1gFnPc1	vlajka
madagaskarských	madagaskarský	k2eAgMnPc2d1	madagaskarský
prezidentů	prezident	k1gMnPc2	prezident
se	se	k3xPyFc4	se
s	s	k7c7	s
osobou	osoba	k1gFnSc7	osoba
prezidenta	prezident	k1gMnSc2	prezident
mění	měnit	k5eAaImIp3nS	měnit
a	a	k8xC	a
liší	lišit	k5eAaImIp3nS	lišit
se	se	k3xPyFc4	se
i	i	k9	i
lícove	lícov	k1gInSc5	lícov
a	a	k8xC	a
rubové	rubový	k2eAgFnSc2d1	rubová
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Vlajka	vlajka	k1gFnSc1	vlajka
Madagaskaru	Madagaskar	k1gInSc2	Madagaskar
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
Madagaskaru	Madagaskar	k1gInSc2	Madagaskar
</s>
</p>
<p>
<s>
Madagaskarská	madagaskarský	k2eAgFnSc1d1	madagaskarská
hymna	hymna	k1gFnSc1	hymna
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Madagaskarská	madagaskarský	k2eAgFnSc1d1	madagaskarská
vlajka	vlajka	k1gFnSc1	vlajka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
