<p>
<s>
Den	den	k1gInSc1	den
Země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
každoroční	každoroční	k2eAgFnSc1d1	každoroční
událost	událost	k1gFnSc1	událost
organizovaná	organizovaný	k2eAgFnSc1d1	organizovaná
22	[number]	k4	22
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
a	a	k8xC	a
zaměřená	zaměřený	k2eAgFnSc1d1	zaměřená
na	na	k7c4	na
propagaci	propagace	k1gFnSc4	propagace
a	a	k8xC	a
podporu	podpora	k1gFnSc4	podpora
ekologie	ekologie	k1gFnSc2	ekologie
a	a	k8xC	a
ochranu	ochrana	k1gFnSc4	ochrana
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
svátek	svátek	k1gInSc1	svátek
je	být	k5eAaImIp3nS	být
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
původními	původní	k2eAgInPc7d1	původní
dny	den	k1gInPc7	den
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgFnP	konat
při	při	k7c6	při
oslavách	oslava	k1gFnPc6	oslava
jarní	jarní	k2eAgFnSc2d1	jarní
rovnodennosti	rovnodennost	k1gFnSc2	rovnodennost
<g/>
,	,	kIx,	,
21	[number]	k4	21
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
a	a	k8xC	a
oslavovaly	oslavovat	k5eAaImAgInP	oslavovat
příchod	příchod	k1gInSc4	příchod
jara	jaro	k1gNnSc2	jaro
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
moderním	moderní	k2eAgNnSc6d1	moderní
pojetí	pojetí	k1gNnSc6	pojetí
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
ekologicky	ekologicky	k6eAd1	ekologicky
motivovaný	motivovaný	k2eAgInSc4d1	motivovaný
svátek	svátek	k1gInSc4	svátek
upozorňující	upozorňující	k2eAgMnPc4d1	upozorňující
lidi	člověk	k1gMnPc4	člověk
na	na	k7c4	na
dopady	dopad	k1gInPc4	dopad
ničení	ničení	k1gNnSc2	ničení
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Historie	historie	k1gFnSc1	historie
vyhlášení	vyhlášení	k1gNnSc2	vyhlášení
tohoto	tento	k3xDgInSc2	tento
dne	den	k1gInSc2	den
sahá	sahat	k5eAaImIp3nS	sahat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
především	především	k9	především
John	John	k1gMnSc1	John
McConnell	McConnell	k1gMnSc1	McConnell
začal	začít	k5eAaPmAgMnS	začít
volat	volat	k5eAaImF	volat
po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
dne	den	k1gInSc2	den
Země	zem	k1gFnSc2	zem
a	a	k8xC	a
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
těchto	tento	k3xDgFnPc2	tento
snah	snaha	k1gFnPc2	snaha
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
i	i	k9	i
vlajku	vlajka	k1gFnSc4	vlajka
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
Den	den	k1gInSc1	den
Země	zem	k1gFnSc2	zem
byl	být	k5eAaImAgInS	být
slaven	slavit	k5eAaImNgInS	slavit
22	[number]	k4	22
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1970	[number]	k4	1970
v	v	k7c6	v
San	San	k1gFnSc6	San
Francisku	Francisek	k1gInSc2	Francisek
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
oslava	oslava	k1gFnSc1	oslava
byla	být	k5eAaImAgFnS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
zamýšlena	zamýšlet	k5eAaImNgFnS	zamýšlet
nejen	nejen	k6eAd1	nejen
jako	jako	k8xS	jako
ekologicky	ekologicky	k6eAd1	ekologicky
zaměřený	zaměřený	k2eAgInSc1d1	zaměřený
svátek	svátek	k1gInSc1	svátek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
jako	jako	k9	jako
levicově	levicově	k6eAd1	levicově
orientovaný	orientovaný	k2eAgInSc1d1	orientovaný
happening	happening	k1gInSc1	happening
bojovníků	bojovník	k1gMnPc2	bojovník
proti	proti	k7c3	proti
válce	válka	k1gFnSc3	válka
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Kampaň	kampaň	k1gFnSc4	kampaň
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
svátkem	svátek	k1gInSc7	svátek
spojená	spojený	k2eAgFnSc1d1	spojená
si	se	k3xPyFc3	se
kladla	klást	k5eAaImAgFnS	klást
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
přenést	přenést	k5eAaPmF	přenést
otázku	otázka	k1gFnSc4	otázka
ekologie	ekologie	k1gFnSc2	ekologie
do	do	k7c2	do
politických	politický	k2eAgInPc2d1	politický
kruhů	kruh	k1gInPc2	kruh
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
zvýšit	zvýšit	k5eAaPmF	zvýšit
energetickou	energetický	k2eAgFnSc4d1	energetická
účinnost	účinnost	k1gFnSc4	účinnost
<g/>
,	,	kIx,	,
recyklovat	recyklovat	k5eAaBmF	recyklovat
odpadky	odpadek	k1gInPc4	odpadek
a	a	k8xC	a
hledat	hledat	k5eAaImF	hledat
obnovitelné	obnovitelný	k2eAgInPc4d1	obnovitelný
zdroje	zdroj	k1gInPc4	zdroj
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
akce	akce	k1gFnSc1	akce
měla	mít	k5eAaImAgFnS	mít
velký	velký	k2eAgInSc4d1	velký
ohlas	ohlas	k1gInSc4	ohlas
zejména	zejména	k9	zejména
mezi	mezi	k7c4	mezi
studenty	student	k1gMnPc4	student
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
OSN	OSN	kA	OSN
začala	začít	k5eAaPmAgFnS	začít
Den	den	k1gInSc4	den
Země	zem	k1gFnSc2	zem
organizovat	organizovat	k5eAaBmF	organizovat
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
se	se	k3xPyFc4	se
k	k	k7c3	k
Americe	Amerika	k1gFnSc3	Amerika
připojil	připojit	k5eAaPmAgInS	připojit
i	i	k9	i
zbytek	zbytek	k1gInSc1	zbytek
světa	svět	k1gInSc2	svět
a	a	k8xC	a
22	[number]	k4	22
<g/>
.	.	kIx.	.
duben	duben	k1gInSc1	duben
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
Mezinárodním	mezinárodní	k2eAgInSc7d1	mezinárodní
dnem	den	k1gInSc7	den
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
se	se	k3xPyFc4	se
Den	den	k1gInSc1	den
Země	zem	k1gFnSc2	zem
slaví	slavit	k5eAaImIp3nS	slavit
také	také	k9	také
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dnes	dnes	k6eAd1	dnes
slaví	slavit	k5eAaImIp3nP	slavit
Den	den	k1gInSc4	den
Země	zem	k1gFnSc2	zem
víc	hodně	k6eAd2	hodně
jak	jak	k8xC	jak
miliarda	miliarda	k4xCgFnSc1	miliarda
lidí	člověk	k1gMnPc2	člověk
ve	v	k7c6	v
více	hodně	k6eAd2	hodně
jak	jak	k8xC	jak
193	[number]	k4	193
státech	stát	k1gInPc6	stát
světa	svět	k1gInSc2	svět
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
největším	veliký	k2eAgInSc7d3	veliký
sekulárním	sekulární	k2eAgInSc7d1	sekulární
svátkem	svátek	k1gInSc7	svátek
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yIgInSc2	který
jsou	být	k5eAaImIp3nP	být
zapojení	zapojený	k2eAgMnPc1d1	zapojený
lidé	člověk	k1gMnPc1	člověk
společně	společně	k6eAd1	společně
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
původ	původ	k1gInSc4	původ
<g/>
,	,	kIx,	,
víru	víra	k1gFnSc4	víra
či	či	k8xC	či
národnost	národnost	k1gFnSc4	národnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Den	dna	k1gFnPc2	dna
Země	zem	k1gFnSc2	zem
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
