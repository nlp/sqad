<p>
<s>
Poštolka	poštolka	k1gFnSc1	poštolka
rudonohá	rudonohý	k2eAgFnSc1d1	rudonohá
(	(	kIx(	(
<g/>
Falco	Falco	k1gMnSc1	Falco
vespertinus	vespertinus	k1gMnSc1	vespertinus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
malý	malý	k2eAgInSc1d1	malý
druh	druh	k1gInSc1	druh
dravého	dravý	k2eAgMnSc2d1	dravý
ptáka	pták	k1gMnSc2	pták
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
sokolovitých	sokolovitý	k2eAgMnPc2d1	sokolovitý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Velikosti	velikost	k1gFnPc1	velikost
poštolky	poštolka	k1gFnSc2	poštolka
obecné	obecná	k1gFnSc2	obecná
(	(	kIx(	(
<g/>
délka	délka	k1gFnSc1	délka
těla	tělo	k1gNnSc2	tělo
28	[number]	k4	28
<g/>
–	–	k?	–
<g/>
34	[number]	k4	34
cm	cm	kA	cm
<g/>
,	,	kIx,	,
rozpětí	rozpětí	k1gNnSc6	rozpětí
křídel	křídlo	k1gNnPc2	křídlo
65	[number]	k4	65
<g/>
–	–	k?	–
<g/>
76	[number]	k4	76
cm	cm	kA	cm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Samec	samec	k1gInSc1	samec
je	být	k5eAaImIp3nS	být
celý	celý	k2eAgInSc1d1	celý
modrošedý	modrošedý	k2eAgInSc1d1	modrošedý
<g/>
,	,	kIx,	,
s	s	k7c7	s
červenými	červený	k2eAgFnPc7d1	červená
kalhotkami	kalhotky	k1gFnPc7	kalhotky
a	a	k8xC	a
podocasními	podocasní	k2eAgFnPc7d1	podocasní
krovkami	krovka	k1gFnPc7	krovka
a	a	k8xC	a
stříbřitě	stříbřitě	k6eAd1	stříbřitě
šedými	šedý	k2eAgFnPc7d1	šedá
letkami	letka	k1gFnPc7	letka
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnSc1	samice
má	mít	k5eAaImIp3nS	mít
spodinu	spodina	k1gFnSc4	spodina
rezavou	rezavý	k2eAgFnSc4d1	rezavá
bez	bez	k7c2	bez
proužkování	proužkování	k1gNnSc2	proužkování
<g/>
,	,	kIx,	,
hřbet	hřbet	k1gInSc1	hřbet
šedý	šedý	k2eAgInSc1d1	šedý
s	s	k7c7	s
tmavým	tmavý	k2eAgNnSc7d1	tmavé
proužkováním	proužkování	k1gNnSc7	proužkování
a	a	k8xC	a
hlavu	hlava	k1gFnSc4	hlava
žlutavě	žlutavě	k6eAd1	žlutavě
bílou	bílý	k2eAgFnSc4d1	bílá
<g/>
.	.	kIx.	.
</s>
<s>
Mladí	mladý	k2eAgMnPc1d1	mladý
ptáci	pták	k1gMnPc1	pták
se	se	k3xPyFc4	se
podobají	podobat	k5eAaImIp3nP	podobat
ostříži	ostříž	k1gMnSc3	ostříž
lesnímu	lesní	k2eAgMnSc3d1	lesní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Souvislý	souvislý	k2eAgInSc1d1	souvislý
areál	areál	k1gInSc1	areál
rozšíření	rozšíření	k1gNnSc2	rozšíření
sahá	sahat	k5eAaImIp3nS	sahat
od	od	k7c2	od
západní	západní	k2eAgFnSc2d1	západní
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
,	,	kIx,	,
Běloruska	Bělorusko	k1gNnSc2	Bělorusko
a	a	k8xC	a
Ruska	Rusko	k1gNnSc2	Rusko
východně	východně	k6eAd1	východně
po	po	k7c6	po
120	[number]	k4	120
<g/>
.	.	kIx.	.
poledník	poledník	k1gMnSc1	poledník
na	na	k7c6	na
Sibiři	Sibiř	k1gFnSc6	Sibiř
<g/>
.	.	kIx.	.
</s>
<s>
Ostrůvkovitě	ostrůvkovitě	k6eAd1	ostrůvkovitě
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
i	i	k9	i
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
dalších	další	k2eAgInPc6d1	další
státech	stát	k1gInPc6	stát
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Tažný	tažný	k2eAgInSc1d1	tažný
druh	druh	k1gInSc1	druh
<g/>
,	,	kIx,	,
zimuje	zimovat	k5eAaImIp3nS	zimovat
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
v	v	k7c6	v
otevřené	otevřený	k2eAgFnSc6d1	otevřená
krajině	krajina	k1gFnSc6	krajina
s	s	k7c7	s
lesíky	lesík	k1gInPc7	lesík
<g/>
,	,	kIx,	,
ve	v	k7c6	v
stepích	step	k1gFnPc6	step
a	a	k8xC	a
v	v	k7c6	v
údolích	údolí	k1gNnPc6	údolí
kolem	kolem	k7c2	kolem
řek	řeka	k1gFnPc2	řeka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Výskyt	výskyt	k1gInSc1	výskyt
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
poštolka	poštolka	k1gFnSc1	poštolka
rudonohá	rudonohý	k2eAgFnSc1d1	rudonohá
dříve	dříve	k6eAd2	dříve
hnízdila	hnízdit	k5eAaImAgFnS	hnízdit
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgNnSc4d1	poslední
hnízdění	hnízdění	k1gNnSc4	hnízdění
bylo	být	k5eAaImAgNnS	být
prokázáno	prokázat	k5eAaPmNgNnS	prokázat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
u	u	k7c2	u
Grygova	Grygův	k2eAgInSc2d1	Grygův
na	na	k7c6	na
Olomoucku	Olomoucko	k1gNnSc6	Olomoucko
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
bývají	bývat	k5eAaImIp3nP	bývat
pozorováni	pozorován	k2eAgMnPc1d1	pozorován
jednotliví	jednotlivý	k2eAgMnPc1d1	jednotlivý
ptáci	pták	k1gMnPc1	pták
nebo	nebo	k8xC	nebo
páry	pára	k1gFnPc1	pára
i	i	k9	i
v	v	k7c6	v
hnízdním	hnízdní	k2eAgNnSc6d1	hnízdní
období	období	k1gNnSc6	období
(	(	kIx(	(
<g/>
především	především	k9	především
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
Moravě	Morava	k1gFnSc6	Morava
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
další	další	k2eAgNnPc1d1	další
hnízdění	hnízdění	k1gNnPc1	hnízdění
však	však	k9	však
doloženo	doložen	k2eAgNnSc1d1	doloženo
nebylo	být	k5eNaImAgNnS	být
<g/>
.	.	kIx.	.
</s>
<s>
Každoročně	každoročně	k6eAd1	každoročně
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
poštolka	poštolka	k1gFnSc1	poštolka
rudonohá	rudonohý	k2eAgFnSc1d1	rudonohá
na	na	k7c6	na
Ostravsku	Ostravsko	k1gNnSc6	Ostravsko
(	(	kIx(	(
pozorování	pozorování	k1gNnSc1	pozorování
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2010	[number]	k4	2010
až	až	k9	až
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Hnízdění	hnízdění	k1gNnSc2	hnízdění
==	==	k?	==
</s>
</p>
<p>
<s>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
většinou	většina	k1gFnSc7	většina
v	v	k7c6	v
koloniích	kolonie	k1gFnPc6	kolonie
ve	v	k7c6	v
starých	starý	k2eAgNnPc6d1	staré
hnízdech	hnízdo	k1gNnPc6	hnízdo
krkavcovitých	krkavcovitý	k2eAgMnPc2d1	krkavcovitý
ptáků	pták	k1gMnPc2	pták
(	(	kIx(	(
<g/>
např.	např.	kA	např.
havranů	havran	k1gMnPc2	havran
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vzácně	vzácně	k6eAd1	vzácně
i	i	k9	i
samostatně	samostatně	k6eAd1	samostatně
<g/>
.	.	kIx.	.
</s>
<s>
Snůška	snůška	k1gFnSc1	snůška
čítá	čítat	k5eAaImIp3nS	čítat
3-4	[number]	k4	3-4
(	(	kIx(	(
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
nažloutlých	nažloutlý	k2eAgInPc2d1	nažloutlý
nebo	nebo	k8xC	nebo
rezavých	rezavý	k2eAgInPc2d1	rezavý
<g/>
,	,	kIx,	,
hustě	hustě	k6eAd1	hustě
tmavě	tmavě	k6eAd1	tmavě
skvrnitých	skvrnitý	k2eAgNnPc2d1	skvrnité
vajec	vejce	k1gNnPc2	vejce
o	o	k7c6	o
rozměrech	rozměr	k1gInPc6	rozměr
36,8	[number]	k4	36,8
x	x	k?	x
29,2	[number]	k4	29,2
mm	mm	kA	mm
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
inkubace	inkubace	k1gFnSc1	inkubace
trvá	trvat	k5eAaImIp3nS	trvat
22-23	[number]	k4	22-23
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Sedí	sedit	k5eAaImIp3nP	sedit
oba	dva	k4xCgMnPc1	dva
ptáci	pták	k1gMnPc1	pták
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
krmí	krmit	k5eAaImIp3nP	krmit
zpočátku	zpočátku	k6eAd1	zpočátku
pouze	pouze	k6eAd1	pouze
samice	samice	k1gFnSc1	samice
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
na	na	k7c6	na
hnízdě	hnízdo	k1gNnSc6	hnízdo
porcuje	porcovat	k5eAaImIp3nS	porcovat
potravu	potrava	k1gFnSc4	potrava
přinášenou	přinášený	k2eAgFnSc4d1	přinášená
samcem	samec	k1gInSc7	samec
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
oba	dva	k4xCgMnPc1	dva
rodiče	rodič	k1gMnPc1	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Vzletnosti	vzletnost	k1gFnPc1	vzletnost
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
26-28	[number]	k4	26-28
dnů	den	k1gInPc2	den
a	a	k8xC	a
pohlavně	pohlavně	k6eAd1	pohlavně
dospívají	dospívat	k5eAaImIp3nP	dospívat
po	po	k7c6	po
1	[number]	k4	1
<g/>
.	.	kIx.	.
roce	rok	k1gInSc6	rok
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Potrava	potrava	k1gFnSc1	potrava
==	==	k?	==
</s>
</p>
<p>
<s>
Loví	lovit	k5eAaImIp3nS	lovit
hlavně	hlavně	k9	hlavně
hmyz	hmyz	k1gInSc1	hmyz
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
i	i	k9	i
drobné	drobný	k2eAgMnPc4d1	drobný
savce	savec	k1gMnPc4	savec
<g/>
.	.	kIx.	.
</s>
<s>
Kořisti	kořist	k1gFnSc3	kořist
se	se	k3xPyFc4	se
zmocňuje	zmocňovat	k5eAaImIp3nS	zmocňovat
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
i	i	k8xC	i
ve	v	k7c6	v
vzduchu	vzduch	k1gInSc6	vzduch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Video	video	k1gNnSc1	video
==	==	k?	==
</s>
</p>
<p>
<s>
Jarmila	Jarmila	k1gFnSc1	Jarmila
Kačírková	Kačírková	k1gFnSc1	Kačírková
<g/>
:	:	kIx,	:
Poštolka	poštolka	k1gFnSc1	poštolka
rudonohá	rudonohý	k2eAgFnSc1d1	rudonohá
(	(	kIx(	(
<g/>
Falco	Falco	k1gMnSc1	Falco
vespertinus	vespertinus	k1gMnSc1	vespertinus
<g/>
)	)	kIx)	)
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
<g/>
,	,	kIx,	,
Hortobágy	Hortobág	k1gInPc1	Hortobág
<g/>
,	,	kIx,	,
30.4	[number]	k4	30.4
<g/>
.2019	.2019	k4	.2019
</s>
</p>
<p>
<s>
https://youtu.be/SD9cv6E4isU	[url]	k4	https://youtu.be/SD9cv6E4isU
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
poštolka	poštolka	k1gFnSc1	poštolka
rudonohá	rudonohý	k2eAgFnSc1d1	rudonohá
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Falco	Falco	k1gMnSc1	Falco
vespertinus	vespertinus	k1gMnSc1	vespertinus
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
