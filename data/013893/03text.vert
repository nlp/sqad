<s>
Stolní	stolní	k2eAgInSc4d1
tenis	tenis	k1gInSc4
na	na	k7c6
Letních	letní	k2eAgFnPc6d1
olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
1996	#num#	k4
</s>
<s>
Stolní	stolní	k2eAgMnPc1d1
tenisna	tenisna	k6eAd1
Letních	letní	k2eAgFnPc6d1
olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
1996	#num#	k4
</s>
<s>
Základní	základní	k2eAgInPc1d1
údaje	údaj	k1gInPc1
Pořadatel	pořadatel	k1gMnSc1
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1
federace	federace	k1gFnSc1
stolního	stolní	k2eAgMnSc2d1
tenisuMezinárodní	tenisuMezinárodní	k2eAgInSc4d1
olympijský	olympijský	k2eAgInSc4d1
výbor	výbor	k1gInSc4
Dějiště	dějiště	k1gNnSc2
</s>
<s>
Georgia	Georgia	k1gFnSc1
World	Worlda	k1gFnPc2
Congress	Congressa	k1gFnPc2
Center	centrum	k1gNnPc2
Datum	datum	k1gNnSc1
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
-	-	kIx~
4	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1996	#num#	k4
Soutěže	soutěž	k1gFnSc2
</s>
<s>
4	#num#	k4
(	(	kIx(
<g/>
2	#num#	k4
muži	muž	k1gMnPc7
<g/>
,	,	kIx,
2	#num#	k4
ženy	žena	k1gFnPc1
<g/>
)	)	kIx)
Startující	startující	k2eAgFnSc1d1
</s>
<s>
166	#num#	k4
z	z	k7c2
51	#num#	k4
zemí	zem	k1gFnPc2
Soutěže	soutěž	k1gFnSc2
</s>
<s>
Dvouhramužiženy	Dvouhramužižen	k2eAgFnPc1d1
</s>
<s>
Čtyřhramužiženy	Čtyřhramužižen	k2eAgFnPc1d1
</s>
<s>
<<	<<	k?
1992	#num#	k4
<g/>
seznam	seznam	k1gInSc4
medailistů	medailista	k1gMnPc2
<g/>
2000	#num#	k4
>>	>>	k?
</s>
<s>
Stolní	stolní	k2eAgInSc4d1
tenis	tenis	k1gInSc4
na	na	k7c6
Letních	letní	k2eAgFnPc6d1
olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
1996	#num#	k4
probíhalo	probíhat	k5eAaImAgNnS
ve	v	k7c6
sportovní	sportovní	k2eAgFnSc6d1
hale	hala	k1gFnSc6
Georgia	Georgium	k1gNnSc2
World	Worlda	k1gFnPc2
Congress	Congressa	k1gFnPc2
Center	centrum	k1gNnPc2
v	v	k7c6
Atlantě	Atlanta	k1gFnSc6
</s>
<s>
Přehled	přehled	k1gInSc1
medailí	medaile	k1gFnPc2
</s>
<s>
Pořadí	pořadí	k1gNnSc1
</s>
<s>
Země	země	k1gFnSc1
</s>
<s>
Zlato	zlato	k1gNnSc1
</s>
<s>
Stříbro	stříbro	k1gNnSc1
</s>
<s>
Bronz	bronz	k1gInSc1
</s>
<s>
Celkově	celkově	k6eAd1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čína	Čína	k1gFnSc1
(	(	kIx(
<g/>
CHN	CHN	kA
<g/>
)	)	kIx)
<g/>
4318	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tchaj-wan	Tchaj-wan	k1gInSc1
(	(	kIx(
<g/>
TPE	TPE	kA
<g/>
)	)	kIx)
<g/>
0	#num#	k4
<g/>
101	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
(	(	kIx(
<g/>
KOR	KOR	kA
<g/>
)	)	kIx)
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
22	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Německo	Německo	k1gNnSc1
(	(	kIx(
<g/>
GER	Gera	k1gFnPc2
<g/>
)	)	kIx)
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
11	#num#	k4
</s>
<s>
Celkem	celkem	k6eAd1
<g/>
44412	#num#	k4
</s>
<s>
Medailisté	medailista	k1gMnPc1
</s>
<s>
Soutěž	soutěž	k1gFnSc1
</s>
<s>
Zlato	zlato	k1gNnSc1
</s>
<s>
Stříbro	stříbro	k1gNnSc1
</s>
<s>
Bronz	bronz	k1gInSc1
</s>
<s>
Dvouhra	dvouhra	k1gFnSc1
muži	muž	k1gMnSc3
</s>
<s>
Liou	Liou	k6eAd1
Kuo-liangČína	Kuo-liangČína	k1gFnSc1
(	(	kIx(
<g/>
CHN	CHN	kA
<g/>
)	)	kIx)
</s>
<s>
Wang	Wang	k1gInSc1
TaoČína	TaoČín	k1gInSc2
(	(	kIx(
<g/>
CHN	CHN	kA
<g/>
)	)	kIx)
</s>
<s>
Jörg	Jörg	k1gInSc1
RosskopfNěmecko	RosskopfNěmecko	k1gNnSc1
(	(	kIx(
<g/>
GER	Gera	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
Čtyřhra	čtyřhra	k1gFnSc1
muži	muž	k1gMnSc3
</s>
<s>
Liou	Liou	k5eAaPmIp1nS
Kuo-lianga	Kuo-lianga	k1gFnSc1
Kong	Kongo	k1gNnPc2
LinghuiČína	LinghuiČín	k1gInSc2
(	(	kIx(
<g/>
CHN	CHN	kA
<g/>
)	)	kIx)
</s>
<s>
Lü	Lü	k?
Lina	Lina	k1gFnSc1
Wang	Wang	k1gInSc1
TaoČína	TaoČína	k1gFnSc1
(	(	kIx(
<g/>
CHN	CHN	kA
<g/>
)	)	kIx)
</s>
<s>
Lee	Lea	k1gFnSc3
Chul-Seunga	Chul-Seunga	k1gFnSc1
Yoo	Yoo	k1gFnSc1
Nam-KyuJižní	Nam-KyuJižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
(	(	kIx(
<g/>
KOR	KOR	kA
<g/>
)	)	kIx)
</s>
<s>
Dvouhra	dvouhra	k1gFnSc1
žen	žena	k1gFnPc2
</s>
<s>
Teng	Teng	k1gInSc1
Ja-pchingČína	Ja-pchingČín	k1gInSc2
(	(	kIx(
<g/>
CHN	CHN	kA
<g/>
)	)	kIx)
</s>
<s>
Chen	Chen	k1gMnSc1
JingTchaj-wan	JingTchaj-wan	k1gMnSc1
(	(	kIx(
<g/>
TPE	TPE	kA
<g/>
)	)	kIx)
</s>
<s>
Qiao	Qiao	k1gNnSc1
HongČína	HongČín	k1gInSc2
(	(	kIx(
<g/>
CHN	CHN	kA
<g/>
)	)	kIx)
</s>
<s>
Čtyřhra	čtyřhra	k1gFnSc1
žen	žena	k1gFnPc2
</s>
<s>
Teng	Teng	k1gMnSc1
Ja-pchinga	Ja-pching	k1gMnSc2
Qiao	Qiao	k1gMnSc1
HongČína	HongČín	k1gMnSc2
(	(	kIx(
<g/>
CHN	CHN	kA
<g/>
)	)	kIx)
</s>
<s>
Liu	Liu	k?
Weia	Weia	k1gMnSc1
Qiao	Qiao	k1gMnSc1
YunpingČína	YunpingČína	k1gFnSc1
(	(	kIx(
<g/>
CHN	CHN	kA
<g/>
)	)	kIx)
</s>
<s>
Park	park	k1gInSc1
Hae-Junga	Hae-Jung	k1gMnSc2
Ryu	Ryu	k1gMnSc2
Ji-HaeJižní	Ji-HaeJižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
(	(	kIx(
<g/>
KOR	KOR	kA
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Sporty	sport	k1gInPc1
na	na	k7c6
Letních	letní	k2eAgFnPc6d1
olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
1996	#num#	k4
v	v	k7c6
Atlantě	Atlanta	k1gFnSc6
</s>
<s>
Atletika	atletika	k1gFnSc1
•	•	k?
Badminton	badminton	k1gInSc1
•	•	k?
Baseball	baseball	k1gInSc1
•	•	k?
Basketbal	basketbal	k1gInSc1
•	•	k?
Box	box	k1gInSc1
•	•	k?
Cyklistika	cyklistika	k1gFnSc1
•	•	k?
Fotbal	fotbal	k1gInSc1
•	•	k?
Gymnastika	gymnastika	k1gFnSc1
(	(	kIx(
<g/>
sportovní	sportovní	k2eAgFnSc1d1
<g/>
,	,	kIx,
moderní	moderní	k2eAgFnSc1d1
<g/>
)	)	kIx)
•	•	k?
Házená	házená	k1gFnSc1
•	•	k?
Jachting	jachting	k1gInSc1
•	•	k?
Jezdectví	jezdectví	k1gNnSc2
•	•	k?
Judo	judo	k1gNnSc1
•	•	k?
Kanoistika	kanoistika	k1gFnSc1
•	•	k?
Lukostřelba	lukostřelba	k1gFnSc1
•	•	k?
Moderní	moderní	k2eAgInSc1d1
pětiboj	pětiboj	k1gInSc1
•	•	k?
Plavání	plavání	k1gNnSc1
•	•	k?
Plážový	plážový	k2eAgInSc4d1
volejbal	volejbal	k1gInSc4
•	•	k?
Pozemní	pozemní	k2eAgInSc4d1
hokej	hokej	k1gInSc4
•	•	k?
Skoky	skok	k1gInPc4
do	do	k7c2
vody	voda	k1gFnSc2
•	•	k?
Softball	softball	k1gInSc1
•	•	k?
Sportovní	sportovní	k2eAgFnSc1d1
střelba	střelba	k1gFnSc1
•	•	k?
Stolní	stolní	k2eAgInSc4d1
tenis	tenis	k1gInSc4
•	•	k?
Synchronizované	synchronizovaný	k2eAgNnSc4d1
plavání	plavání	k1gNnSc4
•	•	k?
Šerm	šerm	k1gInSc1
•	•	k?
Tenis	tenis	k1gInSc1
•	•	k?
Veslování	veslování	k1gNnSc2
•	•	k?
Vodní	vodní	k2eAgNnSc4d1
pólo	pólo	k1gNnSc4
•	•	k?
Volejbal	volejbal	k1gInSc1
•	•	k?
Vzpírání	vzpírání	k1gNnSc2
•	•	k?
Zápas	zápas	k1gInSc1
</s>
<s>
Stolní	stolní	k2eAgInSc4d1
tenis	tenis	k1gInSc4
na	na	k7c6
letních	letní	k2eAgFnPc6d1
olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
</s>
<s>
Soul	Soul	k1gInSc1
1988	#num#	k4
•	•	k?
Barcelona	Barcelona	k1gFnSc1
1992	#num#	k4
•	•	k?
Atlanta	Atlanta	k1gFnSc1
1996	#num#	k4
•	•	k?
Sydney	Sydney	k1gNnSc1
2000	#num#	k4
•	•	k?
Athény	Athéna	k1gFnSc2
2004	#num#	k4
•	•	k?
Peking	Peking	k1gInSc1
2008	#num#	k4
•	•	k?
Londýn	Londýn	k1gInSc1
2012	#num#	k4
•	•	k?
Rio	Rio	k1gFnPc2
de	de	k?
Janeiro	Janeiro	k1gNnSc1
2016	#num#	k4
•	•	k?
Tokio	Tokio	k1gNnSc1
2020	#num#	k4
•	•	k?
Paříž	Paříž	k1gFnSc4
2024	#num#	k4
Seznam	seznam	k1gInSc4
olympijských	olympijský	k2eAgMnPc2d1
medailistů	medailista	k1gMnPc2
ve	v	k7c6
stolním	stolní	k2eAgInSc6d1
tenisu	tenis	k1gInSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Sport	sport	k1gInSc1
</s>
