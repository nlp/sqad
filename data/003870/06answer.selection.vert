<s>
Symbolem	symbol	k1gInSc7	symbol
Japonska	Japonsko	k1gNnSc2	Japonsko
je	být	k5eAaImIp3nS	být
sopka	sopka	k1gFnSc1	sopka
Fudži	Fudž	k1gFnSc3	Fudž
(	(	kIx(	(
<g/>
Fudži-san	Fudžian	k1gInSc1	Fudži-san
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
nerodilí	rodilý	k2eNgMnPc1d1	nerodilý
mluvčí	mluvčí	k1gMnPc1	mluvčí
někdy	někdy	k6eAd1	někdy
nesprávně	správně	k6eNd1	správně
říkají	říkat	k5eAaImIp3nP	říkat
Fudži-jama	Fudžiama	k1gNnSc4	Fudži-jama
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vzniká	vznikat	k5eAaImIp3nS	vznikat
špatným	špatný	k2eAgNnSc7d1	špatné
čtením	čtení	k1gNnSc7	čtení
znaku	znak	k1gInSc2	znak
pro	pro	k7c4	pro
horu	hora	k1gFnSc4	hora
(	(	kIx(	(
<g/>
japonské	japonský	k2eAgNnSc4d1	Japonské
čtení	čtení	k1gNnSc4	čtení
"	"	kIx"	"
<g/>
jama	jama	k6eAd1	jama
<g/>
"	"	kIx"	"
zaměněno	zaměnit	k5eAaPmNgNnS	zaměnit
za	za	k7c4	za
sinojaponské	sinojaponský	k2eAgNnSc4d1	sinojaponské
čtení	čtení	k1gNnSc4	čtení
"	"	kIx"	"
<g/>
san	san	k?	san
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
