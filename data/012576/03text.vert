<p>
<s>
Hippies	Hippies	k1gMnSc1	Hippies
[	[	kIx(	[
<g/>
hipíz	hipíz	k1gMnSc1	hipíz
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
jedn	jedn	k1gInSc1	jedn
<g/>
.	.	kIx.	.
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
hippy	hipp	k1gInPc1	hipp
(	(	kIx(	(
<g/>
děti	dítě	k1gFnPc1	dítě
květin	květina	k1gFnPc2	květina
neboli	neboli	k8xC	neboli
'	'	kIx"	'
<g/>
Květinové	květinový	k2eAgNnSc1d1	květinové
hnutí	hnutí	k1gNnSc1	hnutí
<g/>
'	'	kIx"	'
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
hnutí	hnutí	k1gNnSc1	hnutí
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
hnutí	hnutí	k1gNnSc4	hnutí
neorganizované	organizovaný	k2eNgNnSc4d1	neorganizované
<g/>
,	,	kIx,	,
vycházející	vycházející	k2eAgNnSc4d1	vycházející
z	z	k7c2	z
generace	generace	k1gFnSc2	generace
beatniků	beatnik	k1gMnPc2	beatnik
<g/>
.	.	kIx.	.
</s>
<s>
Šířit	šířit	k5eAaImF	šířit
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
ze	z	k7c2	z
západního	západní	k2eAgNnSc2d1	západní
pobřeží	pobřeží	k1gNnSc2	pobřeží
USA	USA	kA	USA
<g/>
,	,	kIx,	,
ze	z	k7c2	z
San	San	k1gFnSc2	San
Francisca	Francisc	k1gInSc2	Francisc
<g/>
.	.	kIx.	.
</s>
<s>
Cíle	cíl	k1gInPc1	cíl
"	"	kIx"	"
<g/>
hippísáků	hippísák	k1gInPc2	hippísák
<g/>
"	"	kIx"	"
byly	být	k5eAaImAgInP	být
nejednotné	jednotný	k2eNgInPc1d1	nejednotný
<g/>
,	,	kIx,	,
za	za	k7c4	za
základní	základní	k2eAgNnSc4d1	základní
lze	lze	k6eAd1	lze
ale	ale	k8xC	ale
považovat	považovat	k5eAaImF	považovat
vzpouru	vzpoura	k1gFnSc4	vzpoura
proti	proti	k7c3	proti
společnosti	společnost	k1gFnSc3	společnost
<g/>
,	,	kIx,	,
mír	mír	k1gInSc4	mír
<g/>
,	,	kIx,	,
lásku	láska	k1gFnSc4	láska
<g/>
,	,	kIx,	,
přátelství	přátelství	k1gNnSc4	přátelství
<g/>
,	,	kIx,	,
svobodu	svoboda	k1gFnSc4	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
naplnění	naplnění	k1gNnSc3	naplnění
těchto	tento	k3xDgInPc2	tento
cílů	cíl	k1gInPc2	cíl
se	se	k3xPyFc4	se
sdružovali	sdružovat	k5eAaImAgMnP	sdružovat
do	do	k7c2	do
skupin	skupina	k1gFnPc2	skupina
či	či	k8xC	či
komun	komuna	k1gFnPc2	komuna
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
pak	pak	k6eAd1	pak
často	často	k6eAd1	často
provozovaly	provozovat	k5eAaImAgInP	provozovat
volnou	volný	k2eAgFnSc4d1	volná
lásku	láska	k1gFnSc4	láska
(	(	kIx(	(
<g/>
free	free	k6eAd1	free
love	lov	k1gInSc5	lov
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
užívaly	užívat	k5eAaImAgFnP	užívat
drogy	droga	k1gFnPc1	droga
–	–	k?	–
zvláště	zvláště	k6eAd1	zvláště
psychedelické	psychedelický	k2eAgInPc1d1	psychedelický
(	(	kIx(	(
<g/>
LSD	LSD	kA	LSD
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Častým	častý	k2eAgInSc7d1	častý
cílem	cíl	k1gInSc7	cíl
byl	být	k5eAaImAgInS	být
někdy	někdy	k6eAd1	někdy
ale	ale	k8xC	ale
jen	jen	k9	jen
opak	opak	k1gInSc1	opak
chování	chování	k1gNnSc2	chování
většinové	většinový	k2eAgFnSc2d1	většinová
společnosti	společnost	k1gFnSc2	společnost
a	a	k8xC	a
vyjádření	vyjádření	k1gNnSc2	vyjádření
protestu	protest	k1gInSc2	protest
<g/>
.	.	kIx.	.
</s>
<s>
Hippies	Hippies	k1gInSc4	Hippies
propagovali	propagovat	k5eAaImAgMnP	propagovat
spontánní	spontánní	k2eAgNnSc4d1	spontánní
chování	chování	k1gNnSc4	chování
<g/>
,	,	kIx,	,
lásku	láska	k1gFnSc4	láska
a	a	k8xC	a
prosazovali	prosazovat	k5eAaImAgMnP	prosazovat
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
člověk	člověk	k1gMnSc1	člověk
má	mít	k5eAaImIp3nS	mít
užít	užít	k5eAaPmF	užít
života	život	k1gInSc2	život
a	a	k8xC	a
nebudovat	budovat	k5eNaImF	budovat
kariéru	kariéra	k1gFnSc4	kariéra
<g/>
,	,	kIx,	,
nehonit	honit	k5eNaImF	honit
se	se	k3xPyFc4	se
za	za	k7c7	za
penězi	peníze	k1gInPc7	peníze
<g/>
.	.	kIx.	.
</s>
<s>
Příslušníci	příslušník	k1gMnPc1	příslušník
hippies	hippiesa	k1gFnPc2	hippiesa
nosili	nosit	k5eAaImAgMnP	nosit
často	často	k6eAd1	často
dlouhé	dlouhý	k2eAgInPc4d1	dlouhý
vlasy	vlas	k1gInPc4	vlas
<g/>
,	,	kIx,	,
volné	volný	k2eAgNnSc4d1	volné
oblečení	oblečení	k1gNnSc4	oblečení
a	a	k8xC	a
korálky	korálek	k1gInPc4	korálek
a	a	k8xC	a
celkově	celkově	k6eAd1	celkově
prosazovali	prosazovat	k5eAaImAgMnP	prosazovat
"	"	kIx"	"
<g/>
primitivní	primitivní	k2eAgInSc4d1	primitivní
<g/>
"	"	kIx"	"
vzhled	vzhled	k1gInSc4	vzhled
jako	jako	k8xC	jako
projev	projev	k1gInSc4	projev
negativní	negativní	k2eAgFnSc2d1	negativní
reakce	reakce	k1gFnSc2	reakce
proti	proti	k7c3	proti
většinové	většinový	k2eAgFnSc3d1	většinová
metrosexualitě	metrosexualita	k1gFnSc3	metrosexualita
u	u	k7c2	u
mužů	muž	k1gMnPc2	muž
v	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
40	[number]	k4	40
<g/>
.	.	kIx.	.
a	a	k8xC	a
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dalším	další	k2eAgMnSc7d1	další
poměrně	poměrně	k6eAd1	poměrně
výrazným	výrazný	k2eAgInSc7d1	výrazný
projevem	projev	k1gInSc7	projev
spojeným	spojený	k2eAgInSc7d1	spojený
s	s	k7c7	s
tímto	tento	k3xDgNnSc7	tento
hnutím	hnutí	k1gNnSc7	hnutí
bylo	být	k5eAaImAgNnS	být
tíhnutí	tíhnutí	k1gNnSc1	tíhnutí
k	k	k7c3	k
mysticismu	mysticismus	k1gInSc3	mysticismus
<g/>
,	,	kIx,	,
různým	různý	k2eAgMnPc3d1	různý
druhům	druh	k1gMnPc3	druh
východních	východní	k2eAgNnPc2d1	východní
náboženství	náboženství	k1gNnPc2	náboženství
a	a	k8xC	a
filosofií	filosofie	k1gFnPc2	filosofie
<g/>
,	,	kIx,	,
objevování	objevování	k1gNnSc1	objevování
na	na	k7c6	na
Západě	západ	k1gInSc6	západ
doposud	doposud	k6eAd1	doposud
neznámých	známý	k2eNgFnPc2d1	neznámá
kultur	kultura	k1gFnPc2	kultura
a	a	k8xC	a
zvyků	zvyk	k1gInPc2	zvyk
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc2	jejich
propojování	propojování	k1gNnSc2	propojování
se	s	k7c7	s
soudobou	soudobý	k2eAgFnSc7d1	soudobá
kulturou	kultura	k1gFnSc7	kultura
a	a	k8xC	a
způsobem	způsob	k1gInSc7	způsob
uvažování	uvažování	k1gNnSc1	uvažování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hippie	hippie	k1gMnSc1	hippie
móda	móda	k1gFnSc1	móda
měla	mít	k5eAaImAgFnS	mít
zásadní	zásadní	k2eAgInSc4d1	zásadní
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
populární	populární	k2eAgFnSc4d1	populární
hudbu	hudba	k1gFnSc4	hudba
<g/>
,	,	kIx,	,
televizi	televize	k1gFnSc4	televize
<g/>
,	,	kIx,	,
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
literaturu	literatura	k1gFnSc4	literatura
a	a	k8xC	a
výtvarné	výtvarný	k2eAgNnSc4d1	výtvarné
umění	umění	k1gNnSc4	umění
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
bylo	být	k5eAaImAgNnS	být
mnoho	mnoho	k4c1	mnoho
aspektů	aspekt	k1gInPc2	aspekt
asimilováno	asimilován	k2eAgNnSc4d1	asimilováno
do	do	k7c2	do
většinové	většinový	k2eAgFnSc2d1	většinová
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Dědictví	dědictví	k1gNnSc1	dědictví
hippies	hippiesa	k1gFnPc2	hippiesa
lze	lze	k6eAd1	lze
pozorovat	pozorovat	k5eAaImF	pozorovat
v	v	k7c6	v
soudobé	soudobý	k2eAgFnSc6d1	soudobá
kultuře	kultura	k1gFnSc6	kultura
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
formách	forma	k1gFnPc6	forma
–	–	k?	–
od	od	k7c2	od
zdravé	zdravý	k2eAgFnSc2d1	zdravá
výživy	výživa	k1gFnSc2	výživa
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
hudební	hudební	k2eAgInPc4d1	hudební
festivaly	festival	k1gInPc4	festival
po	po	k7c4	po
kybernetickou	kybernetický	k2eAgFnSc4d1	kybernetická
revoluci	revoluce	k1gFnSc4	revoluce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Konec	konec	k1gInSc1	konec
Éry	éra	k1gFnSc2	éra
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
festival	festival	k1gInSc1	festival
v	v	k7c6	v
Altamontu	Altamont	k1gInSc6	Altamont
kde	kde	k6eAd1	kde
ochranka	ochranka	k1gFnSc1	ochranka
tvořená	tvořený	k2eAgFnSc1d1	tvořená
členy	člen	k1gMnPc7	člen
motorkářského	motorkářský	k2eAgInSc2d1	motorkářský
gangu	gang	k1gInSc2	gang
Hells	Hellsa	k1gFnPc2	Hellsa
Angels	Angelsa	k1gFnPc2	Angelsa
ubodala	ubodat	k5eAaPmAgFnS	ubodat
černošského	černošský	k2eAgMnSc4d1	černošský
fanouška	fanoušek	k1gMnSc4	fanoušek
Mereditha	Meredith	k1gMnSc4	Meredith
Huntera	Hunter	k1gMnSc4	Hunter
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pacifismus	pacifismus	k1gInSc4	pacifismus
stoupenců	stoupenec	k1gMnPc2	stoupenec
hippies	hippies	k1gMnSc1	hippies
ovlivnil	ovlivnit	k5eAaPmAgMnS	ovlivnit
i	i	k9	i
politické	politický	k2eAgNnSc4d1	politické
dění	dění	k1gNnSc4	dění
tehdejšího	tehdejší	k2eAgInSc2d1	tehdejší
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
došlo	dojít	k5eAaPmAgNnS	dojít
kromě	kromě	k7c2	kromě
USA	USA	kA	USA
k	k	k7c3	k
vzpourám	vzpoura	k1gFnPc3	vzpoura
mládeže	mládež	k1gFnSc2	mládež
i	i	k9	i
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
jiných	jiný	k2eAgFnPc6d1	jiná
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
hlavně	hlavně	k6eAd1	hlavně
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
největším	veliký	k2eAgInPc3d3	veliký
střetům	střet	k1gInPc3	střet
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
či	či	k8xC	či
Západním	západní	k2eAgNnSc6d1	západní
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
japonská	japonský	k2eAgFnSc1d1	japonská
mládež	mládež	k1gFnSc1	mládež
například	například	k6eAd1	například
protestovala	protestovat	k5eAaBmAgFnS	protestovat
proti	proti	k7c3	proti
americkým	americký	k2eAgFnPc3d1	americká
vojenským	vojenský	k2eAgFnPc3d1	vojenská
základnám	základna	k1gFnPc3	základna
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
silně	silně	k6eAd1	silně
nekonformního	konformní	k2eNgInSc2d1	nekonformní
způsobu	způsob	k1gInSc2	způsob
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
provokoval	provokovat	k5eAaImAgInS	provokovat
většinovou	většinový	k2eAgFnSc4d1	většinová
konzervativní	konzervativní	k2eAgFnSc4d1	konzervativní
americkou	americký	k2eAgFnSc4d1	americká
společnost	společnost	k1gFnSc4	společnost
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
angažovali	angažovat	k5eAaBmAgMnP	angažovat
v	v	k7c6	v
protestech	protest	k1gInPc6	protest
proti	proti	k7c3	proti
válce	válka	k1gFnSc3	válka
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
<g/>
,	,	kIx,	,
organizovali	organizovat	k5eAaBmAgMnP	organizovat
obrovské	obrovský	k2eAgFnPc4d1	obrovská
protiválečné	protiválečný	k2eAgFnPc4d1	protiválečná
demonstrace	demonstrace	k1gFnPc4	demonstrace
<g/>
.	.	kIx.	.
</s>
<s>
Tehdejší	tehdejší	k2eAgFnSc1d1	tehdejší
americká	americký	k2eAgFnSc1d1	americká
mládež	mládež	k1gFnSc1	mládež
totiž	totiž	k9	totiž
byla	být	k5eAaImAgFnS	být
do	do	k7c2	do
stále	stále	k6eAd1	stále
se	se	k3xPyFc4	se
prodlužující	prodlužující	k2eAgFnSc4d1	prodlužující
a	a	k8xC	a
ne	ne	k9	ne
příliš	příliš	k6eAd1	příliš
úspěšné	úspěšný	k2eAgFnSc2d1	úspěšná
války	válka	k1gFnSc2	válka
povolávána	povoláván	k2eAgFnSc1d1	povolávána
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
pomocí	pomocí	k7c2	pomocí
tzv.	tzv.	kA	tzv.
draft	draft	k1gInSc1	draft
lottery	lotter	k1gInPc1	lotter
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
odpor	odpor	k1gInSc4	odpor
proti	proti	k7c3	proti
konfliktu	konflikt	k1gInSc3	konflikt
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
rychle	rychle	k6eAd1	rychle
a	a	k8xC	a
trval	trvat	k5eAaImAgInS	trvat
velmi	velmi	k6eAd1	velmi
dlouho	dlouho	k6eAd1	dlouho
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
této	tento	k3xDgFnSc2	tento
války	válka	k1gFnSc2	válka
zmizel	zmizet	k5eAaPmAgMnS	zmizet
i	i	k9	i
společný	společný	k2eAgMnSc1d1	společný
nepřítel	nepřítel	k1gMnSc1	nepřítel
a	a	k8xC	a
hippies	hippies	k1gMnSc1	hippies
jako	jako	k8xS	jako
masové	masový	k2eAgNnSc1d1	masové
hnutí	hnutí	k1gNnSc1	hnutí
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
asimilovalo	asimilovat	k5eAaBmAgNnS	asimilovat
<g/>
;	;	kIx,	;
největší	veliký	k2eAgFnSc1d3	veliký
část	část	k1gFnSc1	část
stoupenců	stoupenec	k1gMnPc2	stoupenec
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stala	stát	k5eAaPmAgFnS	stát
běžnou	běžný	k2eAgFnSc7d1	běžná
součástí	součást	k1gFnSc7	součást
většinové	většinový	k2eAgFnSc2d1	většinová
společnosti	společnost	k1gFnSc2	společnost
i	i	k8xC	i
s	s	k7c7	s
jejími	její	k3xOp3gFnPc7	její
hodnotami	hodnota	k1gFnPc7	hodnota
<g/>
,	,	kIx,	,
malá	malý	k2eAgFnSc1d1	malá
část	část	k1gFnSc1	část
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
drogám	droga	k1gFnPc3	droga
a	a	k8xC	a
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
malá	malý	k2eAgFnSc1d1	malá
část	část	k1gFnSc1	část
příslušníků	příslušník	k1gMnPc2	příslušník
hippies	hippiesa	k1gFnPc2	hippiesa
zůstala	zůstat	k5eAaPmAgFnS	zůstat
ve	v	k7c6	v
společenstvích	společenství	k1gNnPc6	společenství
věrna	věren	k2eAgFnSc1d1	věrna
všem	všecek	k3xTgInPc3	všecek
původním	původní	k2eAgInPc3d1	původní
ideálům	ideál	k1gInPc3	ideál
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
a	a	k8xC	a
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
některé	některý	k3yIgFnPc1	některý
atributy	atribut	k1gInPc1	atribut
hnutí	hnutí	k1gNnPc2	hnutí
získaly	získat	k5eAaPmAgInP	získat
na	na	k7c6	na
popularitě	popularita	k1gFnSc6	popularita
<g/>
,	,	kIx,	,
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
nejen	nejen	k6eAd1	nejen
o	o	k7c4	o
protiválečný	protiválečný	k2eAgInSc4d1	protiválečný
aktivismus	aktivismus	k1gInSc4	aktivismus
(	(	kIx(	(
<g/>
který	který	k3yQgMnSc1	který
například	například	k6eAd1	například
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
měl	mít	k5eAaImAgInS	mít
podobu	podoba	k1gFnSc4	podoba
protestů	protest	k1gInPc2	protest
proti	proti	k7c3	proti
americkým	americký	k2eAgFnPc3d1	americká
a	a	k8xC	a
sovětským	sovětský	k2eAgFnPc3d1	sovětská
balistickým	balistický	k2eAgFnPc3d1	balistická
střelám	střela	k1gFnPc3	střela
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
i	i	k9	i
o	o	k7c4	o
zvýšení	zvýšení	k1gNnSc4	zvýšení
zájmu	zájem	k1gInSc2	zájem
o	o	k7c4	o
východní	východní	k2eAgNnPc4d1	východní
náboženství	náboženství	k1gNnPc4	náboženství
<g/>
,	,	kIx,	,
ekologii	ekologie	k1gFnSc4	ekologie
<g/>
,	,	kIx,	,
vegetariánství	vegetariánství	k1gNnSc4	vegetariánství
<g/>
,	,	kIx,	,
či	či	k8xC	či
alternativní	alternativní	k2eAgFnSc4d1	alternativní
kulturu	kultura	k1gFnSc4	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Objevily	objevit	k5eAaPmAgInP	objevit
se	se	k3xPyFc4	se
zcela	zcela	k6eAd1	zcela
nové	nový	k2eAgInPc4d1	nový
hudební	hudební	k2eAgInPc4d1	hudební
proudy	proud	k1gInPc4	proud
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
získaly	získat	k5eAaPmAgInP	získat
rychle	rychle	k6eAd1	rychle
na	na	k7c6	na
popularitě	popularita	k1gFnSc6	popularita
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
reggae	reggaat	k5eAaPmIp3nS	reggaat
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
bývalí	bývalý	k2eAgMnPc1d1	bývalý
členové	člen	k1gMnPc1	člen
subkultury	subkultura	k1gFnSc2	subkultura
hippies	hippies	k1gInSc1	hippies
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
byli	být	k5eAaImAgMnP	být
fanoušky	fanoušek	k1gMnPc7	fanoušek
kapely	kapela	k1gFnSc2	kapela
Black	Black	k1gInSc1	Black
Sabbath	Sabbath	k1gInSc1	Sabbath
<g/>
,	,	kIx,	,
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
subkulturu	subkultura	k1gFnSc4	subkultura
heavy-metalu	heavyetal	k1gMnSc3	heavy-metal
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Snaha	snaha	k1gFnSc1	snaha
některých	některý	k3yIgMnPc2	některý
aktivistů	aktivista	k1gMnPc2	aktivista
oživit	oživit	k5eAaPmF	oživit
sílu	síla	k1gFnSc4	síla
hippies	hippiesa	k1gFnPc2	hippiesa
v	v	k7c6	v
časech	čas	k1gInPc6	čas
války	válka	k1gFnSc2	válka
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
ztroskotala	ztroskotat	k5eAaPmAgFnS	ztroskotat
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
války	válka	k1gFnSc2	válka
byli	být	k5eAaImAgMnP	být
posíláni	posílat	k5eAaImNgMnP	posílat
již	již	k9	již
vojáci	voják	k1gMnPc1	voják
z	z	k7c2	z
profesionální	profesionální	k2eAgFnSc2d1	profesionální
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
že	že	k8xS	že
konflikt	konflikt	k1gInSc1	konflikt
měl	mít	k5eAaImAgInS	mít
stále	stále	k6eAd1	stále
do	do	k7c2	do
určité	určitý	k2eAgFnSc2d1	určitá
míry	míra	k1gFnSc2	míra
podporu	podpor	k1gInSc2	podpor
mezi	mezi	k7c7	mezi
veřejností	veřejnost	k1gFnSc7	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přesto	přesto	k8xC	přesto
ale	ale	k9	ale
na	na	k7c6	na
protestech	protest	k1gInPc6	protest
proti	proti	k7c3	proti
tomuto	tento	k3xDgInSc3	tento
konfliktu	konflikt	k1gInSc3	konflikt
zazněla	zaznít	k5eAaPmAgNnP	zaznít
podobná	podobný	k2eAgNnPc1d1	podobné
hesla	heslo	k1gNnPc1	heslo
jako	jako	k8xC	jako
před	před	k7c7	před
třiceti	třicet	k4xCc7	třicet
lety	let	k1gInPc7	let
a	a	k8xC	a
ozvali	ozvat	k5eAaPmAgMnP	ozvat
se	se	k3xPyFc4	se
i	i	k9	i
někteří	některý	k3yIgMnPc1	některý
původní	původní	k2eAgMnPc1d1	původní
účastníci	účastník	k1gMnPc1	účastník
protestů	protest	k1gInPc2	protest
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Politická	politický	k2eAgFnSc1d1	politická
orientace	orientace	k1gFnSc1	orientace
==	==	k?	==
</s>
</p>
<p>
<s>
Z	z	k7c2	z
politických	politický	k2eAgFnPc2d1	politická
ideologií	ideologie	k1gFnPc2	ideologie
má	mít	k5eAaImIp3nS	mít
hnutí	hnutí	k1gNnSc4	hnutí
nejblíže	blízce	k6eAd3	blízce
k	k	k7c3	k
anarchismu	anarchismus	k1gInSc3	anarchismus
<g/>
,	,	kIx,	,
komunismu	komunismus	k1gInSc3	komunismus
či	či	k8xC	či
zelenému	zelené	k1gNnSc3	zelené
liberalismu	liberalismus	k1gInSc2	liberalismus
<g/>
.	.	kIx.	.
</s>
<s>
Hippies	Hippies	k1gInSc4	Hippies
navazovali	navazovat	k5eAaImAgMnP	navazovat
na	na	k7c4	na
myšlenky	myšlenka	k1gFnPc4	myšlenka
bohémských	bohémský	k2eAgMnPc2d1	bohémský
intelektuálů	intelektuál	k1gMnPc2	intelektuál
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
William	William	k1gInSc4	William
Blake	Blak	k1gFnSc2	Blak
<g/>
,	,	kIx,	,
Walt	Walt	k1gMnSc1	Walt
Whitman	Whitman	k1gMnSc1	Whitman
<g/>
,	,	kIx,	,
Ralph	Ralph	k1gMnSc1	Ralph
Waldo	Waldo	k1gNnSc1	Waldo
Emerson	Emerson	k1gMnSc1	Emerson
<g/>
,	,	kIx,	,
Henry	Henry	k1gMnSc1	Henry
David	David	k1gMnSc1	David
Thoreau	Thoreaa	k1gFnSc4	Thoreaa
<g/>
,	,	kIx,	,
Hermann	Hermann	k1gMnSc1	Hermann
Hesse	Hesse	k1gFnSc2	Hesse
<g/>
,	,	kIx,	,
Arthur	Arthur	k1gMnSc1	Arthur
Rimbaud	Rimbaud	k1gMnSc1	Rimbaud
<g/>
,	,	kIx,	,
Oscar	Oscar	k1gMnSc1	Oscar
Wilde	Wild	k1gInSc5	Wild
<g/>
,	,	kIx,	,
Aldous	Aldous	k1gInSc4	Aldous
Huxley	Huxlea	k1gFnSc2	Huxlea
<g/>
,	,	kIx,	,
<g/>
ale	ale	k9	ale
i	i	k8xC	i
myslitelů	myslitel	k1gMnPc2	myslitel
Guya	Guyus	k1gMnSc2	Guyus
Deborda	Debord	k1gMnSc2	Debord
<g/>
,	,	kIx,	,
Friedricha	Friedrich	k1gMnSc2	Friedrich
Nietzscheho	Nietzsche	k1gMnSc2	Nietzsche
a	a	k8xC	a
u	u	k7c2	u
některých	některý	k3yIgMnPc2	některý
byla	být	k5eAaImAgFnS	být
též	též	k9	též
oblíbená	oblíbený	k2eAgFnSc1d1	oblíbená
postava	postava	k1gFnSc1	postava
Chea	Che	k2eAgFnSc1d1	Che
Guevary	Guevar	k1gInPc7	Guevar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
největšího	veliký	k2eAgInSc2d3	veliký
rozkvětu	rozkvět	k1gInSc2	rozkvět
hippies	hippiesa	k1gFnPc2	hippiesa
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
a	a	k8xC	a
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Evropě	Evropa	k1gFnSc6	Evropa
nárůst	nárůst	k1gInSc4	nárůst
i	i	k8xC	i
takzvaná	takzvaný	k2eAgFnSc1d1	takzvaná
Nová	nový	k2eAgFnSc1d1	nová
levice	levice	k1gFnSc1	levice
(	(	kIx(	(
<g/>
New	New	k1gMnSc1	New
Left	Left	k1gMnSc1	Left
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stoupenci	stoupenec	k1gMnPc1	stoupenec
tohoto	tento	k3xDgNnSc2	tento
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
vysokoškolští	vysokoškolský	k2eAgMnPc1d1	vysokoškolský
studenti	student	k1gMnPc1	student
a	a	k8xC	a
intelektuálové	intelektuál	k1gMnPc1	intelektuál
<g/>
,	,	kIx,	,
sdíleli	sdílet	k5eAaImAgMnP	sdílet
s	s	k7c7	s
hippies	hippies	k1gInSc1	hippies
některé	některý	k3yIgInPc4	některý
názory	názor	k1gInPc4	názor
na	na	k7c4	na
svět	svět	k1gInSc4	svět
<g/>
,	,	kIx,	,
prosazovali	prosazovat	k5eAaImAgMnP	prosazovat
mír	mír	k1gInSc4	mír
<g/>
,	,	kIx,	,
socialismus	socialismus	k1gInSc1	socialismus
<g/>
,	,	kIx,	,
lidská	lidský	k2eAgNnPc1d1	lidské
práva	právo	k1gNnPc1	právo
<g/>
,	,	kIx,	,
feminismus	feminismus	k1gInSc1	feminismus
<g/>
,	,	kIx,	,
velkou	velký	k2eAgFnSc4d1	velká
míru	míra	k1gFnSc4	míra
osobní	osobní	k2eAgFnSc2d1	osobní
svobody	svoboda	k1gFnSc2	svoboda
<g/>
,	,	kIx,	,
legalizaci	legalizace	k1gFnSc4	legalizace
lehkých	lehký	k2eAgFnPc2d1	lehká
drog	droga	k1gFnPc2	droga
atd.	atd.	kA	atd.
</s>
<s>
Z	z	k7c2	z
Nové	Nové	k2eAgFnSc2d1	Nové
levice	levice	k1gFnSc2	levice
vzešla	vzejít	k5eAaPmAgFnS	vzejít
i	i	k9	i
zelená	zelený	k2eAgFnSc1d1	zelená
politika	politika	k1gFnSc1	politika
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
ideologický	ideologický	k2eAgInSc1d1	ideologický
základ	základ	k1gInSc1	základ
moderních	moderní	k2eAgMnPc2d1	moderní
"	"	kIx"	"
<g/>
zelených	zelený	k2eAgMnPc2d1	zelený
<g/>
"	"	kIx"	"
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
z	z	k7c2	z
hippies	hippiesa	k1gFnPc2	hippiesa
se	se	k3xPyFc4	se
přikláněli	přiklánět	k5eAaImAgMnP	přiklánět
také	také	k9	také
k	k	k7c3	k
anarchoprimitivismu	anarchoprimitivismus	k1gInSc3	anarchoprimitivismus
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
Ačkoliv	ačkoliv	k8xS	ačkoliv
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
a	a	k8xC	a
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Evropě	Evropa	k1gFnSc6	Evropa
hnutí	hnutí	k1gNnSc1	hnutí
odmítalo	odmítat	k5eAaImAgNnS	odmítat
nacionalismus	nacionalismus	k1gInSc4	nacionalismus
a	a	k8xC	a
prosazovalo	prosazovat	k5eAaImAgNnS	prosazovat
kosmopolitismus	kosmopolitismus	k1gInSc4	kosmopolitismus
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
občané	občan	k1gMnPc1	občan
světa	svět	k1gInSc2	svět
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
tzv.	tzv.	kA	tzv.
sovětského	sovětský	k2eAgInSc2d1	sovětský
bloku	blok	k1gInSc2	blok
(	(	kIx(	(
<g/>
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
,	,	kIx,	,
baltské	baltský	k2eAgFnPc1d1	Baltská
republiky	republika	k1gFnPc1	republika
SSSR	SSSR	kA	SSSR
a	a	k8xC	a
sovětská	sovětský	k2eAgFnSc1d1	sovětská
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
<g/>
,	,	kIx,	,
Československo	Československo	k1gNnSc1	Československo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
často	často	k6eAd1	často
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
míšení	míšení	k1gNnSc3	míšení
ideálů	ideál	k1gInPc2	ideál
hnutí	hnutí	k1gNnSc2	hnutí
hippies	hippiesa	k1gFnPc2	hippiesa
a	a	k8xC	a
vlastenectví	vlastenectví	k1gNnPc2	vlastenectví
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
odporu	odpor	k1gInSc3	odpor
k	k	k7c3	k
Sovětskému	sovětský	k2eAgInSc3d1	sovětský
svazu	svaz	k1gInSc3	svaz
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
projevovalo	projevovat	k5eAaImAgNnS	projevovat
například	například	k6eAd1	například
oblibou	obliba	k1gFnSc7	obliba
folkových	folkový	k2eAgInPc2d1	folkový
motivů	motiv	k1gInPc2	motiv
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
vyšívané	vyšívaný	k2eAgFnPc4d1	vyšívaná
plátěné	plátěný	k2eAgFnPc4d1	plátěná
košile	košile	k1gFnPc4	košile
nebo	nebo	k8xC	nebo
hudebních	hudební	k2eAgMnPc2d1	hudební
interpretů	interpret	k1gMnPc2	interpret
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
byl	být	k5eAaImAgMnS	být
Karel	Karel	k1gMnSc1	Karel
Kryl	Kryl	k1gMnSc1	Kryl
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
Na	na	k7c4	na
vzhled	vzhled	k1gInSc4	vzhled
hippies	hippiesa	k1gFnPc2	hippiesa
reagovali	reagovat	k5eAaBmAgMnP	reagovat
stoupenci	stoupenec	k1gMnPc1	stoupenec
subkultury	subkultura	k1gFnSc2	subkultura
Skinhead	skinhead	k1gMnSc1	skinhead
zkrácením	zkrácení	k1gNnSc7	zkrácení
vlasů	vlas	k1gInPc2	vlas
a	a	k8xC	a
vytvořením	vytvoření	k1gNnSc7	vytvoření
své	svůj	k3xOyFgFnSc2	svůj
"	"	kIx"	"
<g/>
hard	hard	k6eAd1	hard
and	and	k?	and
smart	smart	k1gInSc1	smart
<g/>
"	"	kIx"	"
image	image	k1gInSc1	image
(	(	kIx(	(
<g/>
např.	např.	kA	např.
košile	košile	k1gFnSc2	košile
<g/>
,	,	kIx,	,
těžké	těžký	k2eAgFnPc1d1	těžká
boty	bota	k1gFnPc1	bota
<g/>
,	,	kIx,	,
uplé	uplý	k2eAgFnPc1d1	uplý
džíny	džíny	k1gFnPc1	džíny
<g/>
,	,	kIx,	,
kabáty	kabát	k1gInPc1	kabát
<g/>
,	,	kIx,	,
upravené	upravený	k2eAgInPc1d1	upravený
vousy	vous	k1gInPc1	vous
"	"	kIx"	"
<g/>
kotlety	kotleta	k1gFnPc1	kotleta
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
,	,	kIx,	,
jako	jako	k8xC	jako
protiklad	protiklad	k1gInSc1	protiklad
proti	proti	k7c3	proti
volnému	volný	k2eAgNnSc3d1	volné
oblečení	oblečení	k1gNnSc3	oblečení
a	a	k8xC	a
neupravenému	upravený	k2eNgMnSc3d1	neupravený
"	"	kIx"	"
<g/>
divokému	divoký	k2eAgMnSc3d1	divoký
<g/>
"	"	kIx"	"
vzhledu	vzhled	k1gInSc3	vzhled
<g/>
.	.	kIx.	.
</s>
<s>
Skinheadi	skinhead	k1gMnPc1	skinhead
také	také	k9	také
nesouhlasili	souhlasit	k5eNaImAgMnP	souhlasit
a	a	k8xC	a
opovrhovali	opovrhovat	k5eAaImAgMnP	opovrhovat
jejich	jejich	k3xOp3gMnSc4	jejich
"	"	kIx"	"
<g/>
volnomyšlenkářským	volnomyšlenkářský	k2eAgInSc7d1	volnomyšlenkářský
a	a	k8xC	a
nezodpovědným	zodpovědný	k2eNgInSc7d1	nezodpovědný
<g/>
"	"	kIx"	"
přístupem	přístup	k1gInSc7	přístup
k	k	k7c3	k
životu	život	k1gInSc3	život
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
volné	volný	k2eAgFnSc2d1	volná
lásky	láska	k1gFnSc2	láska
byli	být	k5eAaImAgMnP	být
spíše	spíše	k9	spíše
pro	pro	k7c4	pro
rodinnou	rodinný	k2eAgFnSc4d1	rodinná
cestu	cesta	k1gFnSc4	cesta
<g/>
,	,	kIx,	,
místo	místo	k7c2	místo
nezaměstnání	nezaměstnání	k1gNnSc2	nezaměstnání
byli	být	k5eAaImAgMnP	být
pracující	pracující	k2eAgMnPc1d1	pracující
třídou	třída	k1gFnSc7	třída
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgInPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
byli	být	k5eAaImAgMnP	být
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
hlavními	hlavní	k2eAgMnPc7d1	hlavní
přirozenými	přirozený	k2eAgMnPc7d1	přirozený
oponenty	oponent	k1gMnPc7	oponent
hippies	hippiesa	k1gFnPc2	hippiesa
právě	právě	k6eAd1	právě
skinheadi	skinhead	k1gMnPc1	skinhead
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Umění	umění	k1gNnSc1	umění
==	==	k?	==
</s>
</p>
<p>
<s>
Během	během	k7c2	během
období	období	k1gNnSc2	období
jejich	jejich	k3xOp3gInSc2	jejich
největšího	veliký	k2eAgInSc2d3	veliký
rozmachu	rozmach	k1gInSc2	rozmach
zcela	zcela	k6eAd1	zcela
neprogramově	programově	k6eNd1	programově
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
či	či	k8xC	či
se	se	k3xPyFc4	se
podíleli	podílet	k5eAaImAgMnP	podílet
na	na	k7c4	na
vytvoření	vytvoření	k1gNnSc4	vytvoření
alternativního	alternativní	k2eAgNnSc2d1	alternativní
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýraznějším	výrazný	k2eAgInSc7d3	nejvýraznější
projevem	projev	k1gInSc7	projev
byla	být	k5eAaImAgFnS	být
hudba	hudba	k1gFnSc1	hudba
–	–	k?	–
beat	beat	k1gInSc4	beat
<g/>
,	,	kIx,	,
psychedelický	psychedelický	k2eAgInSc4d1	psychedelický
rock	rock	k1gInSc4	rock
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
a	a	k8xC	a
z	z	k7c2	z
něho	on	k3xPp3gNnSc2	on
vyplývající	vyplývající	k2eAgInPc1d1	vyplývající
<g/>
)	)	kIx)	)
hard	hard	k1gInSc1	hard
rock	rock	k1gInSc1	rock
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Jefferson	Jefferson	k1gMnSc1	Jefferson
Airplane	Airplan	k1gMnSc5	Airplan
<g/>
,	,	kIx,	,
Beatles	Beatles	k1gFnPc1	Beatles
<g/>
,	,	kIx,	,
The	The	k1gFnPc1	The
Doors	Doors	k1gInSc1	Doors
<g/>
,	,	kIx,	,
Janis	Janis	k1gInSc1	Janis
Joplin	Joplina	k1gFnPc2	Joplina
<g/>
,	,	kIx,	,
Jimi	on	k3xPp3gFnPc7	on
Hendrix	Hendrix	k1gInSc1	Hendrix
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
samotní	samotný	k2eAgMnPc1d1	samotný
hudebníci	hudebník	k1gMnPc1	hudebník
a	a	k8xC	a
skupiny	skupina	k1gFnPc1	skupina
do	do	k7c2	do
svých	svůj	k3xOyFgInPc2	svůj
textů	text	k1gInPc2	text
zařadili	zařadit	k5eAaPmAgMnP	zařadit
témata	téma	k1gNnPc4	téma
kritizující	kritizující	k2eAgFnSc1d1	kritizující
společnost	společnost	k1gFnSc1	společnost
<g/>
,	,	kIx,	,
používání	používání	k1gNnSc1	používání
drog	droga	k1gFnPc2	droga
a	a	k8xC	a
samozřejmě	samozřejmě	k6eAd1	samozřejmě
i	i	k9	i
odpor	odpor	k1gInSc4	odpor
k	k	k7c3	k
válce	válka	k1gFnSc3	válka
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgMnPc1d1	dnešní
stoupenci	stoupenec	k1gMnPc1	stoupenec
hippies	hippiesa	k1gFnPc2	hippiesa
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
často	často	k6eAd1	často
velký	velký	k2eAgInSc4d1	velký
obdiv	obdiv	k1gInSc4	obdiv
k	k	k7c3	k
tehdejším	tehdejší	k2eAgFnPc3d1	tehdejší
hudebním	hudební	k2eAgFnPc3d1	hudební
skupinám	skupina	k1gFnPc3	skupina
a	a	k8xC	a
festivalům	festival	k1gInPc3	festival
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
které	který	k3yRgFnPc4	který
patří	patřit	k5eAaImIp3nP	patřit
např.	např.	kA	např.
velmi	velmi	k6eAd1	velmi
známý	známý	k2eAgInSc4d1	známý
Woodstock	Woodstock	k1gInSc4	Woodstock
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Cestování	cestování	k1gNnSc2	cestování
==	==	k?	==
</s>
</p>
<p>
<s>
Cestování	cestování	k1gNnSc1	cestování
<g/>
,	,	kIx,	,
vnitrostátní	vnitrostátní	k2eAgFnSc1d1	vnitrostátní
i	i	k8xC	i
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
výrazným	výrazný	k2eAgInSc7d1	výrazný
rysem	rys	k1gInSc7	rys
hippie	hippie	k1gMnSc1	hippie
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Populární	populární	k2eAgFnPc1d1	populární
byly	být	k5eAaImAgFnP	být
školní	školní	k2eAgInPc4d1	školní
autobusy	autobus	k1gInPc4	autobus
podobné	podobný	k2eAgInPc4d1	podobný
autobusu	autobus	k1gInSc3	autobus
Furthur	Furthura	k1gFnPc2	Furthura
Kena	Kenus	k1gMnSc4	Kenus
Keseyho	Kesey	k1gMnSc4	Kesey
nebo	nebo	k8xC	nebo
minibusy	minibus	k1gInPc4	minibus
značky	značka	k1gFnSc2	značka
Volkswagen	volkswagen	k1gInSc1	volkswagen
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
skupiny	skupina	k1gFnPc1	skupina
přátel	přítel	k1gMnPc2	přítel
mohly	moct	k5eAaImAgFnP	moct
cestovat	cestovat	k5eAaImF	cestovat
levně	levně	k6eAd1	levně
a	a	k8xC	a
přitom	přitom	k6eAd1	přitom
společně	společně	k6eAd1	společně
<g/>
.	.	kIx.	.
</s>
<s>
VW	VW	kA	VW
Bus	bus	k1gInSc1	bus
(	(	kIx(	(
<g/>
Volkswagen	volkswagen	k1gInSc1	volkswagen
typ	typ	k1gInSc1	typ
2	[number]	k4	2
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xS	jako
hippie	hippie	k1gMnSc1	hippie
a	a	k8xC	a
undergroundový	undergroundový	k2eAgInSc1d1	undergroundový
symbol	symbol	k1gInSc1	symbol
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
autobusů	autobus	k1gInPc2	autobus
bylo	být	k5eAaImAgNnS	být
přemalováno	přemalován	k2eAgNnSc1d1	přemalováno
a	a	k8xC	a
<g/>
/	/	kIx~	/
<g/>
nebo	nebo	k8xC	nebo
malováno	malován	k2eAgNnSc1d1	malováno
na	na	k7c4	na
zakázku	zakázka	k1gFnSc4	zakázka
<g/>
.	.	kIx.	.
</s>
<s>
Symbol	symbol	k1gInSc1	symbol
míru	mír	k1gInSc2	mír
často	často	k6eAd1	často
nahrazuje	nahrazovat	k5eAaImIp3nS	nahrazovat
logo	logo	k1gNnSc4	logo
Volkswagen	volkswagen	k1gInSc1	volkswagen
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
příslušníci	příslušník	k1gMnPc1	příslušník
hippies	hippies	k1gInSc4	hippies
si	se	k3xPyFc3	se
oblíbili	oblíbit	k5eAaPmAgMnP	oblíbit
stopování	stopování	k1gNnPc4	stopování
jako	jako	k8xC	jako
hlavní	hlavní	k2eAgInSc4d1	hlavní
způsob	způsob	k1gInSc4	způsob
dopravy	doprava	k1gFnSc2	doprava
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
ekonomické	ekonomický	k2eAgNnSc1d1	ekonomické
<g/>
,	,	kIx,	,
šetrné	šetrný	k2eAgInPc1d1	šetrný
k	k	k7c3	k
životnímu	životní	k2eAgNnSc3d1	životní
prostředí	prostředí	k1gNnSc3	prostředí
a	a	k8xC	a
poznávali	poznávat	k5eAaImAgMnP	poznávat
tak	tak	k9	tak
nové	nový	k2eAgMnPc4d1	nový
lidi	člověk	k1gMnPc4	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hippies	Hippies	k1gInSc4	Hippies
měli	mít	k5eAaImAgMnP	mít
tendenci	tendence	k1gFnSc4	tendence
cestovat	cestovat	k5eAaImF	cestovat
nalehko	nalehko	k6eAd1	nalehko
<g/>
,	,	kIx,	,
moci	moct	k5eAaImF	moct
se	se	k3xPyFc4	se
zvednout	zvednout	k5eAaPmF	zvednout
a	a	k8xC	a
jít	jít	k5eAaImF	jít
na	na	k7c4	na
některou	některý	k3yIgFnSc4	některý
z	z	k7c2	z
akcí	akce	k1gFnPc2	akce
(	(	kIx(	(
<g/>
demonstrace	demonstrace	k1gFnSc1	demonstrace
proti	proti	k7c3	proti
válce	válka	k1gFnSc3	válka
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
kyselinové	kyselinový	k2eAgInPc4d1	kyselinový
testy	test	k1gInPc4	test
<g/>
"	"	kIx"	"
Kena	Ken	k2eAgFnSc1d1	Kena
Keseye	Keseye	k1gFnSc1	Keseye
<g/>
,	,	kIx,	,
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Příslušníkům	příslušník	k1gMnPc3	příslušník
hippies	hippies	k1gInSc1	hippies
stačilo	stačit	k5eAaBmAgNnS	stačit
nějaké	nějaký	k3yIgNnSc1	nějaký
oblečení	oblečení	k1gNnSc1	oblečení
v	v	k7c6	v
batohu	batoh	k1gInSc6	batoh
a	a	k8xC	a
mohli	moct	k5eAaImAgMnP	moct
stopovat	stopovat	k5eAaImF	stopovat
kdekoliv	kdekoliv	k6eAd1	kdekoliv
<g/>
.	.	kIx.	.
</s>
<s>
Hippie	hippie	k1gMnSc1	hippie
domácnosti	domácnost	k1gFnSc2	domácnost
přivítaly	přivítat	k5eAaPmAgInP	přivítat
přenocování	přenocování	k1gNnSc4	přenocování
hostů	host	k1gMnPc2	host
v	v	k7c6	v
improvizovaných	improvizovaný	k2eAgFnPc6d1	improvizovaná
podmínkách	podmínka	k1gFnPc6	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
většinou	většinou	k6eAd1	většinou
spolupracovali	spolupracovat	k5eAaImAgMnP	spolupracovat
na	na	k7c4	na
splnění	splnění	k1gNnSc4	splnění
základních	základní	k2eAgFnPc2d1	základní
potřeb	potřeba	k1gFnPc2	potřeba
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
života	život	k1gInSc2	život
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
vidět	vidět	k5eAaImF	vidět
u	u	k7c2	u
Rainbow	Rainbow	k1gFnSc2	Rainbow
Family	Famila	k1gFnSc2	Famila
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
léta	léto	k1gNnSc2	léto
a	a	k8xC	a
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
celé	celý	k2eAgFnSc2d1	celá
rodiny	rodina	k1gFnSc2	rodina
cestovaly	cestovat	k5eAaImAgFnP	cestovat
společně	společně	k6eAd1	společně
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
nákladních	nákladní	k2eAgInPc6d1	nákladní
automobilech	automobil	k1gInPc6	automobil
a	a	k8xC	a
autobusech	autobus	k1gInPc6	autobus
na	na	k7c4	na
Renesanční	renesanční	k2eAgInPc4d1	renesanční
trhy	trh	k1gInPc4	trh
(	(	kIx(	(
<g/>
Renaissance	Renaissance	k1gFnPc4	Renaissance
fair	fair	k6eAd1	fair
<g/>
)	)	kIx)	)
v	v	k7c6	v
a	a	k8xC	a
severní	severní	k2eAgFnSc6d1	severní
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vyráběli	vyrábět	k5eAaImAgMnP	vyrábět
řemeslné	řemeslný	k2eAgInPc4d1	řemeslný
výrobky	výrobek	k1gInPc4	výrobek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
pak	pak	k6eAd1	pak
prodávali	prodávat	k5eAaImAgMnP	prodávat
veřejnosti	veřejnost	k1gFnSc2	veřejnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vrchol	vrchol	k1gInSc1	vrchol
zkušenosti	zkušenost	k1gFnSc2	zkušenost
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
byl	být	k5eAaImAgInS	být
festival	festival	k1gInSc1	festival
Woodstock	Woodstock	k1gInSc1	Woodstock
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
od	od	k7c2	od
15	[number]	k4	15
<g/>
.	.	kIx.	.
do	do	k7c2	do
19	[number]	k4	19
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1969	[number]	k4	1969
nedaleko	nedaleko	k7c2	nedaleko
městečka	městečko	k1gNnSc2	městečko
Bethel	Bethela	k1gFnPc2	Bethela
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
.	.	kIx.	.
</s>
<s>
Festival	festival	k1gInSc1	festival
navštívilo	navštívit	k5eAaPmAgNnS	navštívit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
500	[number]	k4	500
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dědictví	dědictví	k1gNnSc2	dědictví
==	==	k?	==
</s>
</p>
<p>
<s>
Dědictví	dědictví	k1gNnSc1	dědictví
hnutí	hnutí	k1gNnSc2	hnutí
hippies	hippiesa	k1gFnPc2	hippiesa
nadále	nadále	k6eAd1	nadále
prostupuje	prostupovat	k5eAaImIp3nS	prostupovat
západní	západní	k2eAgFnSc7d1	západní
společností	společnost	k1gFnSc7	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Otevřenost	otevřenost	k1gFnSc1	otevřenost
v	v	k7c6	v
sexuálních	sexuální	k2eAgFnPc6d1	sexuální
záležitostech	záležitost	k1gFnPc6	záležitost
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
běžnější	běžný	k2eAgNnSc1d2	běžnější
<g/>
.	.	kIx.	.
</s>
<s>
Přijala	přijmout	k5eAaPmAgFnS	přijmout
se	se	k3xPyFc4	se
větší	veliký	k2eAgFnSc1d2	veliký
náboženská	náboženský	k2eAgFnSc1d1	náboženská
a	a	k8xC	a
kulturní	kulturní	k2eAgFnSc1d1	kulturní
rozmanitost	rozmanitost	k1gFnSc1	rozmanitost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Odlišný	odlišný	k2eAgInSc1d1	odlišný
vzhled	vzhled	k1gInSc1	vzhled
a	a	k8xC	a
oděv	oděv	k1gInSc1	oděv
byl	být	k5eAaImAgInS	být
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
bezprostředních	bezprostřední	k2eAgNnPc2d1	bezprostřední
dědictví	dědictví	k1gNnPc2	dědictví
hippies	hippiesa	k1gFnPc2	hippiesa
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
a	a	k8xC	a
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
kníry	knír	k1gInPc7	knír
<g/>
,	,	kIx,	,
vousy	vous	k1gInPc1	vous
a	a	k8xC	a
dlouhé	dlouhý	k2eAgInPc1d1	dlouhý
vlasy	vlas	k1gInPc1	vlas
staly	stát	k5eAaPmAgInP	stát
samozřejmostí	samozřejmost	k1gFnSc7	samozřejmost
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
multietnické	multietnický	k2eAgNnSc1d1	multietnické
oblečení	oblečení	k1gNnSc1	oblečení
vévodí	vévodit	k5eAaImIp3nS	vévodit
světu	svět	k1gInSc3	svět
módy	móda	k1gFnSc2	móda
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
široká	široký	k2eAgFnSc1d1	široká
škála	škála	k1gFnSc1	škála
osobního	osobní	k2eAgInSc2d1	osobní
vzhledu	vzhled	k1gInSc2	vzhled
a	a	k8xC	a
oděvních	oděvní	k2eAgInPc2d1	oděvní
stylů	styl	k1gInPc2	styl
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
nudismu	nudismus	k1gInSc2	nudismus
staly	stát	k5eAaPmAgFnP	stát
přijatelné	přijatelný	k2eAgFnPc1d1	přijatelná
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
neobvyklé	obvyklý	k2eNgNnSc1d1	neobvyklé
před	před	k7c7	před
érou	éra	k1gFnSc7	éra
hippies	hippiesa	k1gFnPc2	hippiesa
<g/>
.	.	kIx.	.
</s>
<s>
Hippies	Hippies	k1gMnSc1	Hippies
taky	taky	k6eAd1	taky
inspirovali	inspirovat	k5eAaBmAgMnP	inspirovat
pokles	pokles	k1gInSc4	pokles
popularity	popularita	k1gFnSc2	popularita
kravaty	kravata	k1gFnSc2	kravata
a	a	k8xC	a
dalšího	další	k2eAgNnSc2d1	další
formálního	formální	k2eAgNnSc2d1	formální
společenského	společenský	k2eAgNnSc2d1	společenské
oblečení	oblečení	k1gNnSc2	oblečení
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
fakticky	fakticky	k6eAd1	fakticky
povinné	povinný	k2eAgNnSc1d1	povinné
pro	pro	k7c4	pro
muže	muž	k1gMnPc4	muž
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
a	a	k8xC	a
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhé	Dlouhé	k2eAgInPc4d1	Dlouhé
vlasy	vlas	k1gInPc4	vlas
jako	jako	k8xC	jako
dědictví	dědictví	k1gNnPc4	dědictví
po	po	k7c4	po
hippies	hippies	k1gInSc4	hippies
převzala	převzít	k5eAaPmAgFnS	převzít
také	také	k9	také
hard	hard	k6eAd1	hard
rocková	rockový	k2eAgFnSc1d1	rocková
a	a	k8xC	a
metalová	metalový	k2eAgFnSc1d1	metalová
subkultura	subkultura	k1gFnSc1	subkultura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hippies	Hippiesa	k1gFnPc2	Hippiesa
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
se	se	k3xPyFc4	se
začali	začít	k5eAaPmAgMnP	začít
sympatizanti	sympatizant	k1gMnPc1	sympatizant
hnutí	hnutí	k1gNnSc2	hnutí
hippies	hippiesa	k1gFnPc2	hippiesa
(	(	kIx(	(
<g/>
oficiálním	oficiální	k2eAgInSc7d1	oficiální
tiskem	tisk	k1gInSc7	tisk
označovaní	označovaný	k2eAgMnPc1d1	označovaný
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
máničky	mánička	k1gFnSc2	mánička
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
chuligáni	chuligán	k1gMnPc1	chuligán
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
objevovat	objevovat	k5eAaImF	objevovat
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
a	a	k8xC	a
byli	být	k5eAaImAgMnP	být
režimem	režim	k1gInSc7	režim
pronásledováni	pronásledován	k2eAgMnPc1d1	pronásledován
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
za	za	k7c4	za
protisocialistickou	protisocialistický	k2eAgFnSc4d1	protisocialistická
činnost	činnost	k1gFnSc4	činnost
a	a	k8xC	a
vyhýbání	vyhýbání	k1gNnSc1	vyhýbání
se	se	k3xPyFc4	se
vojenské	vojenský	k2eAgFnSc2d1	vojenská
povinnosti	povinnost	k1gFnSc2	povinnost
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgNnSc7d1	významné
místem	místo	k1gNnSc7	místo
projevů	projev	k1gInPc2	projev
proti	proti	k7c3	proti
tehdejšímu	tehdejší	k2eAgInSc3d1	tehdejší
režimu	režim	k1gInSc3	režim
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
tzv.	tzv.	kA	tzv.
Lennonova	Lennonův	k2eAgFnSc1d1	Lennonova
zeď	zeď	k1gFnSc1	zeď
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
Kampě	Kampa	k1gFnSc6	Kampa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hesla	heslo	k1gNnPc4	heslo
a	a	k8xC	a
pokřiky	pokřik	k1gInPc4	pokřik
hippies	hippiesa	k1gFnPc2	hippiesa
==	==	k?	==
</s>
</p>
<p>
<s>
Milujte	milovat	k5eAaImRp2nP	milovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
neválčete	válčit	k5eNaImRp2nP	válčit
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
Make	Makus	k1gInSc5	Makus
love	lov	k1gInSc5	lov
<g/>
,	,	kIx,	,
not	nota	k1gFnPc2	nota
war	war	k?	war
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Osvoboď	osvobodit	k5eAaPmRp2nS	osvobodit
svou	svůj	k3xOyFgFnSc4	svůj
mysl	mysl	k1gFnSc4	mysl
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
Let	léto	k1gNnPc2	léto
your	youra	k1gFnPc2	youra
minds	mindsa	k1gFnPc2	mindsa
be	be	k?	be
free	fre	k1gInSc2	fre
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Síla	síla	k1gFnSc1	síla
květin	květina	k1gFnPc2	květina
(	(	kIx(	(
<g/>
Flower	Flower	k1gMnSc1	Flower
power	power	k1gMnSc1	power
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Dej	dát	k5eAaPmRp2nS	dát
míru	míra	k1gFnSc4	míra
šanci	šance	k1gFnSc4	šance
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
Give	Give	k1gFnPc6	Give
peace	peaec	k1gInSc2	peaec
a	a	k8xC	a
chance	chanec	k1gInSc2	chanec
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mír	mír	k1gInSc4	mír
a	a	k8xC	a
lásku	láska	k1gFnSc4	láska
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
Peace	Peaka	k1gFnSc3	Peaka
&	&	k?	&
Love	lov	k1gInSc5	lov
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mír	mír	k1gInSc4	mír
prosím	prosit	k5eAaImIp1nS	prosit
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
Peace	Peace	k1gMnSc5	Peace
please	pleas	k1gMnSc5	pleas
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nevěřte	věřit	k5eNaImRp2nP	věřit
nikomu	nikdo	k3yNnSc3	nikdo
<g/>
,	,	kIx,	,
komu	kdo	k3yInSc3	kdo
je	být	k5eAaImIp3nS	být
přes	přes	k7c4	přes
třicet	třicet	k4xCc4	třicet
(	(	kIx(	(
<g/>
Don	dona	k1gFnPc2	dona
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Trust	trust	k1gInSc1	trust
Anyone	Anyon	k1gInSc5	Anyon
Over	Over	k1gInSc1	Over
Thirty	Thirt	k1gInPc1	Thirt
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vše	všechen	k3xTgNnSc1	všechen
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
potřebujete	potřebovat	k5eAaImIp2nP	potřebovat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
láska	láska	k1gFnSc1	láska
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
All	All	k1gMnSc3	All
you	you	k?	you
need	need	k1gMnSc1	need
is	is	k?	is
love	lov	k1gInSc5	lov
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Moje	můj	k3xOp1gFnSc1	můj
svoboda	svoboda	k1gFnSc1	svoboda
končí	končit	k5eAaImIp3nS	končit
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
začíná	začínat	k5eAaImIp3nS	začínat
svoboda	svoboda	k1gFnSc1	svoboda
druhého	druhý	k4xOgMnSc2	druhý
(	(	kIx(	(
<g/>
My	my	k3xPp1nPc1	my
freedom	freedom	k1gInSc1	freedom
ends	ends	k1gInSc1	ends
where	wher	k1gInSc5	wher
the	the	k?	the
freedom	freedom	k1gInSc1	freedom
of	of	k?	of
another	anothra	k1gFnPc2	anothra
begins	begins	k6eAd1	begins
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Láska	láska	k1gFnSc1	láska
ano	ano	k9	ano
<g/>
,	,	kIx,	,
peníze	peníz	k1gInPc4	peníz
ne	ne	k9	ne
(	(	kIx(	(
<g/>
Make	Make	k1gInSc1	Make
love	lov	k1gInSc5	lov
<g/>
,	,	kIx,	,
not	nota	k1gFnPc2	nota
money	monea	k1gFnSc2	monea
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
HANÁK	Hanák	k1gMnSc1	Hanák
<g/>
,	,	kIx,	,
Ondřej	Ondřej	k1gMnSc1	Ondřej
<g/>
,	,	kIx,	,
Hippies	Hippies	k1gMnSc1	Hippies
–	–	k?	–
slepé	slepý	k2eAgNnSc1d1	slepé
rameno	rameno	k1gNnSc1	rameno
mrtvé	mrtvý	k2eAgFnSc2d1	mrtvá
řeky	řeka	k1gFnSc2	řeka
<g/>
,	,	kIx,	,
Argo	Argo	k1gNnSc1	Argo
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
</s>
</p>
<p>
<s>
McCLEARY	McCLEARY	k?	McCLEARY
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Bassett	Bassett	k1gMnSc1	Bassett
<g/>
,	,	kIx,	,
Hippie	hippie	k1gMnSc1	hippie
encyklopedie	encyklopedie	k1gFnSc2	encyklopedie
–	–	k?	–
Kulturní	kulturní	k2eAgFnPc1d1	kulturní
encyklopedie	encyklopedie	k1gFnPc1	encyklopedie
(	(	kIx(	(
<g/>
a	a	k8xC	a
frazeikon	frazeikon	k1gMnSc1	frazeikon
<g/>
)	)	kIx)	)
60	[number]	k4	60
<g/>
.	.	kIx.	.
a	a	k8xC	a
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
Volvox	Volvox	k1gInSc1	Volvox
Globator	Globator	k1gInSc1	Globator
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Móda	móda	k1gFnSc1	móda
šedesátých	šedesátý	k4xOgFnPc2	šedesátý
let	léto	k1gNnPc2	léto
</s>
</p>
<p>
<s>
Móda	móda	k1gFnSc1	móda
sedmdesátých	sedmdesátý	k4xOgNnPc2	sedmdesátý
let	léto	k1gNnPc2	léto
</s>
</p>
<p>
<s>
A	a	k9	a
bude	být	k5eAaImBp3nS	být
hůř	zle	k6eAd2	zle
<g/>
!	!	kIx.	!
</s>
</p>
<p>
<s>
Vlasy	vlas	k1gInPc1	vlas
(	(	kIx(	(
<g/>
muzikál	muzikál	k1gInSc1	muzikál
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Hudební	hudební	k2eAgInSc1d1	hudební
festival	festival	k1gInSc1	festival
Woodstock	Woodstocka	k1gFnPc2	Woodstocka
</s>
</p>
<p>
<s>
Neo-Hippies	Neo-Hippies	k1gMnSc1	Neo-Hippies
</s>
</p>
<p>
<s>
Timothy	Timotha	k1gFnPc1	Timotha
Leary	Leara	k1gFnSc2	Leara
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Hippies	Hippiesa	k1gFnPc2	Hippiesa
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
hippy	hippa	k1gFnPc1	hippa
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
–	–	k?	–
stránky	stránka	k1gFnSc2	stránka
hnutí	hnutí	k1gNnSc2	hnutí
hippies	hippiesa	k1gFnPc2	hippiesa
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
(	(	kIx(	(
<g/>
Wiki	Wik	k1gFnSc6	Wik
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
dánsky	dánsky	k6eAd1	dánsky
<g/>
)	)	kIx)	)
Hippie	hippie	k1gMnSc1	hippie
–	–	k?	–
town	town	k1gMnSc1	town
Christiania	Christianium	k1gNnSc2	Christianium
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Hippyland	Hippyland	k1gInSc1	Hippyland
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Hippies	Hippies	k1gInSc1	Hippies
od	od	k7c2	od
A	a	k8xC	a
do	do	k7c2	do
Z	z	k7c2	z
</s>
</p>
