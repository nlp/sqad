<s>
Eduard	Eduard	k1gMnSc1
VIII	VIII	kA
<g/>
.	.	kIx.
</s>
<s>
Eduard	Eduard	k1gMnSc1
VIII	VIII	kA
<g/>
.	.	kIx.
</s>
<s>
král	král	k1gMnSc1
Spojeného	spojený	k2eAgNnSc2d1
království	království	k1gNnSc2
</s>
<s>
Eduard	Eduard	k1gMnSc1
VIII	VIII	kA
<g/>
.	.	kIx.
</s>
<s>
Doba	doba	k1gFnSc1
vlády	vláda	k1gFnSc2
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
leden	leden	k1gInSc1
1936	#num#	k4
–	–	k?
11	#num#	k4
<g/>
.	.	kIx.
prosinec	prosinec	k1gInSc1
1936	#num#	k4
</s>
<s>
Korunovace	korunovace	k1gFnSc1
</s>
<s>
nebyl	být	k5eNaImAgInS
korunován	korunovat	k5eAaBmNgInS
</s>
<s>
Křest	křest	k1gInSc1
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1894	#num#	k4
</s>
<s>
Narození	narození	k1gNnSc1
</s>
<s>
23	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1894	#num#	k4
</s>
<s>
White	Whiit	k5eAaBmRp2nP,k5eAaPmRp2nP,k5eAaImRp2nP
Lodge	Lodge	k1gInSc4
</s>
<s>
Úmrtí	úmrtí	k1gNnSc1
</s>
<s>
28	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1972	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
77	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
Paříž	Paříž	k1gFnSc1
<g/>
,	,	kIx,
Francie	Francie	k1gFnSc1
Francie	Francie	k1gFnSc1
</s>
<s>
Pohřben	pohřben	k2eAgInSc1d1
</s>
<s>
Frogmore	Frogmor	k1gMnSc5
<g/>
,	,	kIx,
Berkshire	Berkshir	k1gMnSc5
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
</s>
<s>
Jiří	Jiří	k1gMnSc1
V.	V.	kA
</s>
<s>
Nástupce	nástupce	k1gMnSc1
</s>
<s>
Jiří	Jiří	k1gMnSc1
VI	VI	kA
<g/>
.	.	kIx.
</s>
<s>
Manželka	manželka	k1gFnSc1
</s>
<s>
Wallis	Wallis	k1gFnSc1
Warfieldová	Warfieldová	k1gFnSc1
</s>
<s>
Rod	rod	k1gInSc1
</s>
<s>
Windsorská	windsorský	k2eAgFnSc1d1
dynastie	dynastie	k1gFnSc1
</s>
<s>
Otec	otec	k1gMnSc1
</s>
<s>
Jiří	Jiří	k1gMnSc1
V.	V.	kA
</s>
<s>
Matka	matka	k1gFnSc1
</s>
<s>
Marie	Marie	k1gFnSc1
z	z	k7c2
Tecku	Teck	k1gInSc2
</s>
<s>
Podpis	podpis	k1gInSc1
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Eduard	Eduard	k1gMnSc1
VIII	VIII	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
23	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1894	#num#	k4
–	–	k?
28	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1972	#num#	k4
Paříž	Paříž	k1gFnSc1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
celým	celý	k2eAgNnSc7d1
jménem	jméno	k1gNnSc7
Edward	Edward	k1gMnSc1
Albert	Albert	k1gMnSc1
Christian	Christian	k1gMnSc1
George	Georg	k1gMnSc2
Andrew	Andrew	k1gMnSc1
Patrick	Patrick	k1gMnSc1
David	David	k1gMnSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
král	král	k1gMnSc1
Spojeného	spojený	k2eAgNnSc2d1
království	království	k1gNnSc2
a	a	k8xC
britských	britský	k2eAgFnPc2d1
dominií	dominie	k1gFnPc2
a	a	k8xC
císař	císař	k1gMnSc1
Indie	Indie	k1gFnSc2
od	od	k7c2
20	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1936	#num#	k4
až	až	k9
do	do	k7c2
své	svůj	k3xOyFgFnSc2
abdikace	abdikace	k1gFnSc2
11	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1936	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byl	být	k5eAaImAgMnS
okamžitě	okamžitě	k6eAd1
nahrazen	nahradit	k5eAaPmNgMnS
svým	svůj	k3xOyFgMnSc7
mladším	mladý	k2eAgMnSc7d2
bratrem	bratr	k1gMnSc7
Jiřím	Jiří	k1gMnSc7
VI	VI	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Eduard	Eduard	k1gMnSc1
byl	být	k5eAaImAgMnS
třetím	třetí	k4xOgMnSc7
britským	britský	k2eAgMnSc7d1
panovníkem	panovník	k1gMnSc7
pocházejícím	pocházející	k2eAgMnSc7d1
z	z	k7c2
Windsorské	windsorský	k2eAgFnSc2d1
dynastie	dynastie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Pouhý	pouhý	k2eAgInSc4d1
měsíc	měsíc	k1gInSc4
po	po	k7c6
svém	svůj	k3xOyFgInSc6
nástupu	nástup	k1gInSc6
vyvolal	vyvolat	k5eAaPmAgMnS
Eduard	Eduard	k1gMnSc1
ústavní	ústavní	k2eAgFnSc4d1
krizi	krize	k1gFnSc4
<g/>
,	,	kIx,
když	když	k8xS
požádal	požádat	k5eAaPmAgMnS
o	o	k7c4
ruku	ruka	k1gFnSc4
rozvedenou	rozvedený	k2eAgFnSc4d1
Američanku	Američanka	k1gFnSc4
Wallis	Wallis	k1gFnSc4
Simpsonovou	Simpsonová	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
když	když	k8xS
z	z	k7c2
právního	právní	k2eAgNnSc2d1
hlediska	hledisko	k1gNnSc2
se	se	k3xPyFc4
s	s	k7c7
ní	on	k3xPp3gFnSc7
mohl	moct	k5eAaImAgInS
Eduard	Eduard	k1gMnSc1
oženit	oženit	k5eAaPmF
a	a	k8xC
zůstat	zůstat	k5eAaPmF
králem	král	k1gMnSc7
<g/>
,	,	kIx,
britský	britský	k2eAgMnSc1d1
premiér	premiér	k1gMnSc1
tomuto	tento	k3xDgInSc3
sňatku	sňatek	k1gInSc3
odporoval	odporovat	k5eAaImAgMnS
tvrzením	tvrzení	k1gNnSc7
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
ji	on	k3xPp3gFnSc4
občané	občan	k1gMnPc1
nepřijali	přijmout	k5eNaPmAgMnP
za	za	k7c4
svou	svůj	k3xOyFgFnSc4
královnu	královna	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Eduard	Eduard	k1gMnSc1
si	se	k3xPyFc3
uvědomoval	uvědomovat	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nP
ministři	ministr	k1gMnPc1
v	v	k7c6
případě	případ	k1gInSc6
jeho	jeho	k3xOp3gInSc2,k3xPp3gInSc2
sňatku	sňatek	k1gInSc2
rezignovali	rezignovat	k5eAaBmAgMnP
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
by	by	kYmCp3nS
mohlo	moct	k5eAaImAgNnS
přivést	přivést	k5eAaPmF
zemi	zem	k1gFnSc4
k	k	k7c3
všeobecným	všeobecný	k2eAgFnPc3d1
volbám	volba	k1gFnPc3
a	a	k8xC
jeho	on	k3xPp3gInSc4,k3xOp3gInSc4
status	status	k1gInSc4
jako	jako	k8xS,k8xC
politicky	politicky	k6eAd1
neutrálního	neutrální	k2eAgMnSc2d1
panovníka	panovník	k1gMnSc2
by	by	kYmCp3nS
byl	být	k5eAaImAgInS
vážně	vážně	k6eAd1
poškozen	poškodit	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dal	dát	k5eAaPmAgMnS
tedy	tedy	k9
přednost	přednost	k1gFnSc4
své	svůj	k3xOyFgFnSc3
abdikaci	abdikace	k1gFnSc3
a	a	k8xC
stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
tak	tak	k6eAd1
jediným	jediný	k2eAgMnSc7d1
panovníkem	panovník	k1gMnSc7
království	království	k1gNnSc2
Commonwealthu	Commonwealtha	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
se	se	k3xPyFc4
dobrovolně	dobrovolně	k6eAd1
vzdal	vzdát	k5eAaPmAgMnS
trůnu	trůn	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Doba	doba	k1gFnSc1
jeho	jeho	k3xOp3gFnSc2
vlády	vláda	k1gFnSc2
trvala	trvat	k5eAaImAgFnS
pouhých	pouhý	k2eAgInPc2d1
325	#num#	k4
dní	den	k1gInPc2
a	a	k8xC
stala	stát	k5eAaPmAgFnS
se	se	k3xPyFc4
tak	tak	k9
jedním	jeden	k4xCgNnSc7
z	z	k7c2
nejkratších	krátký	k2eAgNnPc2d3
období	období	k1gNnPc2
vlády	vláda	k1gFnSc2
britských	britský	k2eAgMnPc2d1
panovníků	panovník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Eduard	Eduard	k1gMnSc1
nebyl	být	k5eNaImAgMnS
nikdy	nikdy	k6eAd1
korunován	korunovat	k5eAaBmNgMnS
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
své	svůj	k3xOyFgFnSc6
abdikaci	abdikace	k1gFnSc6
přijal	přijmout	k5eAaPmAgMnS
status	status	k1gInSc4
syna	syn	k1gMnSc2
panovníka	panovník	k1gMnSc2
a	a	k8xC
byl	být	k5eAaImAgInS
mu	on	k3xPp3gMnSc3
udělen	udělit	k5eAaPmNgInS
titul	titul	k1gInSc1
vévody	vévoda	k1gMnSc2
z	z	k7c2
Windsoru	Windsor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
období	období	k1gNnSc6
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
byl	být	k5eAaImAgInS
nejdříve	dříve	k6eAd3
zařazen	zařadit	k5eAaPmNgInS
u	u	k7c2
britské	britský	k2eAgFnSc2d1
vojenské	vojenský	k2eAgFnSc2d1
mise	mise	k1gFnSc2
ve	v	k7c6
Francii	Francie	k1gFnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
byl	být	k5eAaImAgMnS
obviněn	obvinit	k5eAaPmNgMnS
z	z	k7c2
pronacistických	pronacistický	k2eAgFnPc2d1
sympatií	sympatie	k1gFnPc2
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
přesunut	přesunout	k5eAaPmNgInS
na	na	k7c4
Bahamy	Bahamy	k1gFnPc4
jako	jako	k8xS,k8xC
guvernér	guvernér	k1gMnSc1
a	a	k8xC
vrchní	vrchní	k2eAgMnSc1d1
velitel	velitel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
válce	válka	k1gFnSc6
mu	on	k3xPp3gMnSc3
nebyla	být	k5eNaImAgFnS
nabídnuta	nabídnut	k2eAgFnSc1d1
žádná	žádný	k3yNgFnSc1
oficiální	oficiální	k2eAgFnSc1d1
funkce	funkce	k1gFnSc1
a	a	k8xC
zbytek	zbytek	k1gInSc1
života	život	k1gInSc2
strávil	strávit	k5eAaPmAgInS
v	v	k7c6
ústraní	ústraní	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Biografie	biografie	k1gFnSc1
</s>
<s>
Původ	původ	k1gInSc1
<g/>
,	,	kIx,
mládí	mládí	k1gNnSc1
</s>
<s>
Mladý	mladý	k2eAgMnSc1d1
Eduard	Eduard	k1gMnSc1
</s>
<s>
Eduard	Eduard	k1gMnSc1
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgMnS
23	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
v	v	k7c6
Richmondu	Richmond	k1gInSc6
jako	jako	k8xS,k8xC
nejstarší	starý	k2eAgMnSc1d3
syn	syn	k1gMnSc1
Jiřího	Jiří	k1gMnSc2
<g/>
,	,	kIx,
vévody	vévoda	k1gMnSc2
z	z	k7c2
Yorku	York	k1gInSc2
(	(	kIx(
<g/>
později	pozdě	k6eAd2
krále	král	k1gMnSc2
Jiřího	Jiří	k1gMnSc2
V.	V.	kA
<g/>
)	)	kIx)
a	a	k8xC
jeho	jeho	k3xOp3gFnPc1
manželky	manželka	k1gFnPc1
Marie	Maria	k1gFnSc2
z	z	k7c2
Tecku	Teck	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Studoval	studovat	k5eAaImAgInS
na	na	k7c6
námořních	námořní	k2eAgFnPc6d1
školách	škola	k1gFnPc6
<g/>
,	,	kIx,
nejprve	nejprve	k6eAd1
v	v	k7c6
Osborne	Osborn	k1gInSc5
a	a	k8xC
později	pozdě	k6eAd2
v	v	k7c6
Dartmouthu	Dartmouth	k1gInSc6
a	a	k8xC
sloužil	sloužit	k5eAaImAgMnS
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
u	u	k7c2
britského	britský	k2eAgNnSc2d1
námořnictva	námořnictvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yRnSc4,k3yQnSc4,k3yInSc4
jeho	jeho	k3xOp3gMnSc1
otec	otec	k1gMnSc1
nastoupil	nastoupit	k5eAaPmAgMnS
na	na	k7c4
britský	britský	k2eAgInSc4d1
trůn	trůn	k1gInSc4
<g/>
,	,	kIx,
mu	on	k3xPp3gMnSc3
byl	být	k5eAaImAgInS
udělen	udělen	k2eAgInSc4d1
titul	titul	k1gInSc4
prince	princ	k1gMnSc2
z	z	k7c2
Walesu	Wales	k1gInSc2
a	a	k8xC
začal	začít	k5eAaPmAgMnS
se	se	k3xPyFc4
připravovat	připravovat	k5eAaImF
na	na	k7c4
budoucí	budoucí	k2eAgFnPc4d1
královské	královský	k2eAgFnPc4d1
povinnosti	povinnost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc1
námořní	námořní	k2eAgFnSc1d1
kariéra	kariéra	k1gFnSc1
byla	být	k5eAaImAgFnS
ukončena	ukončit	k5eAaPmNgFnS
a	a	k8xC
začal	začít	k5eAaPmAgInS
studovat	studovat	k5eAaImF
na	na	k7c6
Magdalenině	Magdalenin	k2eAgFnSc6d1
koleji	kolej	k1gFnSc6
v	v	k7c6
Oxfordu	Oxford	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
byl	být	k5eAaImAgMnS
podle	podle	k7c2
svých	svůj	k3xOyFgMnPc2
učitelů	učitel	k1gMnPc2
na	na	k7c4
takové	takový	k3xDgNnSc4
studium	studium	k1gNnSc4
nedostatečně	dostatečně	k6eNd1
připraven	připravit	k5eAaPmNgMnS
<g/>
,	,	kIx,
odešel	odejít	k5eAaPmAgMnS
odsud	odsud	k6eAd1
už	už	k6eAd1
po	po	k7c6
osmi	osm	k4xCc6
semestrech	semestr	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
době	doba	k1gFnSc6
vypuknutí	vypuknutí	k1gNnSc2
první	první	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
Eduard	Eduard	k1gMnSc1
dosáhl	dosáhnout	k5eAaPmAgMnS
věku	věk	k1gInSc3
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
již	již	k6eAd1
mohl	moct	k5eAaImAgInS
sloužit	sloužit	k5eAaImF
v	v	k7c6
britské	britský	k2eAgFnSc6d1
armádě	armáda	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
armády	armáda	k1gFnSc2
vstoupil	vstoupit	k5eAaPmAgMnS
v	v	k7c6
červnu	červen	k1gInSc6
1914	#num#	k4
a	a	k8xC
sloužil	sloužit	k5eAaImAgInS
u	u	k7c2
granátnické	granátnický	k2eAgFnSc2d1
stráže	stráž	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Eduard	Eduard	k1gMnSc1
měl	mít	k5eAaImAgMnS
zájem	zájem	k1gInSc4
o	o	k7c4
službu	služba	k1gFnSc4
na	na	k7c6
frontě	fronta	k1gFnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
na	na	k7c6
základě	základ	k1gInSc6
rozhodnutí	rozhodnutí	k1gNnSc2
ministra	ministr	k1gMnSc2
války	válka	k1gFnSc2
mu	on	k3xPp3gMnSc3
v	v	k7c6
tom	ten	k3xDgNnSc6
bylo	být	k5eAaImAgNnS
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
následníkovi	následník	k1gMnSc3
trůnu	trůn	k1gInSc2
<g/>
,	,	kIx,
zabráněno	zabráněn	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
Eduard	Eduard	k1gMnSc1
byl	být	k5eAaImAgMnS
svědkem	svědek	k1gMnSc7
válečných	válečný	k2eAgInPc2d1
střetů	střet	k1gInPc2
v	v	k7c6
první	první	k4xOgFnSc6
linii	linie	k1gFnSc6
a	a	k8xC
mohl	moct	k5eAaImAgMnS
sledovat	sledovat	k5eAaImF
boje	boj	k1gInPc4
na	na	k7c6
frontě	fronta	k1gFnSc6
tak	tak	k6eAd1
často	často	k6eAd1
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
chtěl	chtít	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc4
role	role	k1gFnPc4
ve	v	k7c6
válce	válka	k1gFnSc6
<g/>
,	,	kIx,
ačkoli	ačkoli	k8xS
se	se	k3xPyFc4
do	do	k7c2
ní	on	k3xPp3gFnSc2
příliš	příliš	k6eAd1
aktivně	aktivně	k6eAd1
nezapojoval	zapojovat	k5eNaImAgMnS
<g/>
,	,	kIx,
mu	on	k3xPp3gMnSc3
přinesla	přinést	k5eAaPmAgFnS
velkou	velký	k2eAgFnSc4d1
popularitu	popularita	k1gFnSc4
mezi	mezi	k7c7
válečnými	válečný	k2eAgMnPc7d1
veterány	veterán	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s>
Královské	královský	k2eAgFnPc1d1
povinnosti	povinnost	k1gFnPc1
</s>
<s>
Eduard	Eduard	k1gMnSc1
a	a	k8xC
Wallis	Wallis	k1gFnSc1
Simpsonová	Simpsonový	k2eAgFnSc1d1
na	na	k7c6
dovolené	dovolená	k1gFnSc6
v	v	k7c6
Jugoslávii	Jugoslávie	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
1936	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
20	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
zastupoval	zastupovat	k5eAaImAgMnS
Eduard	Eduard	k1gMnSc1
svého	své	k1gNnSc2
otce	otec	k1gMnSc2
při	při	k7c6
některých	některý	k3yIgFnPc6
příležitostech	příležitost	k1gFnPc6
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
jak	jak	k6eAd1
v	v	k7c6
Británii	Británie	k1gFnSc6
<g/>
,	,	kIx,
tak	tak	k6eAd1
i	i	k9
v	v	k7c6
zahraničí	zahraničí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zajímal	zajímat	k5eAaImAgMnS
se	se	k3xPyFc4
o	o	k7c6
chudé	chudý	k2eAgFnSc6d1
části	část	k1gFnSc6
země	zem	k1gFnSc2
a	a	k8xC
v	v	k7c6
letech	léto	k1gNnPc6
1919	#num#	k4
až	až	k9
1935	#num#	k4
podnikl	podniknout	k5eAaPmAgInS
16	#num#	k4
cest	cesta	k1gFnPc2
po	po	k7c6
zemích	zem	k1gFnPc6
Britského	britský	k2eAgNnSc2d1
impéria	impérium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc4
atraktivní	atraktivní	k2eAgInSc1d1
vzhled	vzhled	k1gInSc1
a	a	k8xC
charisma	charisma	k1gNnSc1
<g/>
,	,	kIx,
zcestovalost	zcestovalost	k1gFnSc1
i	i	k8xC
status	status	k1gInSc4
svobodného	svobodný	k2eAgMnSc2d1
muže	muž	k1gMnSc2
mu	on	k3xPp3gMnSc3
přinesly	přinést	k5eAaPmAgFnP
velkou	velký	k2eAgFnSc4d1
pozornost	pozornost	k1gFnSc4
a	a	k8xC
stal	stát	k5eAaPmAgInS
se	se	k3xPyFc4
populární	populární	k2eAgInSc1d1
jako	jako	k8xS,k8xC
filmová	filmový	k2eAgFnSc1d1
hvězda	hvězda	k1gFnSc1
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
době	doba	k1gFnSc6
největší	veliký	k2eAgFnSc2d3
slávy	sláva	k1gFnSc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
nejfotografovanější	fotografovaný	k2eAgFnSc7d3
celebritou	celebrita	k1gFnSc7
a	a	k8xC
určoval	určovat	k5eAaImAgInS
mužskou	mužský	k2eAgFnSc4d1
módu	móda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Eduardova	Eduardův	k2eAgFnSc1d1
zženštilost	zženštilost	k1gFnSc1
a	a	k8xC
jiné	jiný	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
jeho	jeho	k3xOp3gFnSc2
bezstarostné	bezstarostný	k2eAgFnSc2d1
povahy	povaha	k1gFnSc2
ve	v	k7c6
20	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
30	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
působily	působit	k5eAaImAgFnP
starosti	starost	k1gFnPc1
britskému	britský	k2eAgMnSc3d1
premiérovi	premiér	k1gMnSc3
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gMnSc3
otci	otec	k1gMnSc3
králi	král	k1gMnSc3
i	i	k8xC
jiným	jiný	k1gMnPc3
jeho	jeho	k3xOp3gMnPc3
přátelům	přítel	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiří	Jiří	k1gMnSc1
V.	V.	kA
byl	být	k5eAaImAgMnS
nespokojen	spokojen	k2eNgMnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
Eduard	Eduard	k1gMnSc1
nebyl	být	k5eNaImAgMnS
schopen	schopen	k2eAgMnSc1d1
usadit	usadit	k5eAaPmF
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
jeho	jeho	k3xOp3gMnSc1
bratr	bratr	k1gMnSc1
Albert	Albert	k1gMnSc1
se	se	k3xPyFc4
oženil	oženit	k5eAaPmAgMnS
a	a	k8xC
měl	mít	k5eAaImAgMnS
dvě	dva	k4xCgFnPc4
děti	dítě	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
1930	#num#	k4
věnoval	věnovat	k5eAaImAgMnS,k5eAaPmAgMnS
král	král	k1gMnSc1
Eduardovi	Eduard	k1gMnSc3
dům	dům	k1gInSc1
v	v	k7c6
Sunningdale	Sunningdala	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
se	se	k3xPyFc4
Eduard	Eduard	k1gMnSc1
stýkal	stýkat	k5eAaImAgMnS
s	s	k7c7
několika	několik	k4yIc7
provdanými	provdaný	k2eAgFnPc7d1
ženami	žena	k1gFnPc7
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgFnPc2
jedna	jeden	k4xCgFnSc1
ho	on	k3xPp3gInSc4
seznámila	seznámit	k5eAaPmAgFnS
s	s	k7c7
Američankou	Američanka	k1gFnSc7
Wallis	Wallis	k1gFnSc7
Simpsonovou	Simpsonová	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgNnPc1
se	se	k3xPyFc4
rozvedla	rozvést	k5eAaPmAgNnP
se	s	k7c7
svým	svůj	k3xOyFgInSc7
prvním	první	k4xOgInSc7
manželem	manžel	k1gMnSc7
roku	rok	k1gInSc2
1927	#num#	k4
a	a	k8xC
krátce	krátce	k6eAd1
nato	nato	k6eAd1
se	se	k3xPyFc4
provdala	provdat	k5eAaPmAgFnS
za	za	k7c2
amerického	americký	k2eAgMnSc2d1
obchodníka	obchodník	k1gMnSc2
Ernesta	Ernest	k1gMnSc2
Simpsona	Simpson	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Eduard	Eduard	k1gMnSc1
a	a	k8xC
Wallis	Wallis	k1gInSc1
se	se	k3xPyFc4
stali	stát	k5eAaPmAgMnP
milenci	milenec	k1gMnPc1
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
to	ten	k3xDgNnSc4
Eduard	Eduard	k1gMnSc1
před	před	k7c7
svým	svůj	k3xOyFgMnSc7
otcem	otec	k1gMnSc7
popíral	popírat	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
poměr	poměr	k1gInSc1
ještě	ještě	k9
zhoršil	zhoršit	k5eAaPmAgInS
již	již	k6eAd1
tak	tak	k6eAd1
nedobrý	dobrý	k2eNgInSc4d1
vztah	vztah	k1gInSc4
mezi	mezi	k7c7
Eduardem	Eduard	k1gMnSc7
a	a	k8xC
jeho	jeho	k3xOp3gMnSc7
otcem	otec	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Vláda	vláda	k1gFnSc1
</s>
<s>
Král	Král	k1gMnSc1
Jiří	Jiří	k1gMnSc1
zemřel	zemřít	k5eAaPmAgMnS
20	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1936	#num#	k4
a	a	k8xC
Eduard	Eduard	k1gMnSc1
nastoupil	nastoupit	k5eAaPmAgMnS
na	na	k7c4
trůn	trůn	k1gInSc4
jako	jako	k8xC,k8xS
Eduard	Eduard	k1gMnSc1
VIII	VIII	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hned	hned	k6eAd1
následující	následující	k2eAgInSc4d1
den	den	k1gInSc4
porušil	porušit	k5eAaPmAgMnS
Eduard	Eduard	k1gMnSc1
královský	královský	k2eAgInSc4d1
protokol	protokol	k1gInSc4
<g/>
,	,	kIx,
když	když	k8xS
sledoval	sledovat	k5eAaImAgMnS
vyhlášení	vyhlášení	k1gNnSc4
o	o	k7c6
svém	svůj	k3xOyFgNnSc6
jmenování	jmenování	k1gNnSc6
panovníkem	panovník	k1gMnSc7
z	z	k7c2
okna	okno	k1gNnSc2
St	St	kA
James	James	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Palace	Palace	k1gFnSc2
v	v	k7c6
doprovodu	doprovod	k1gInSc6
tehdy	tehdy	k6eAd1
ještě	ještě	k9
stále	stále	k6eAd1
vdané	vdaný	k2eAgFnPc4d1
Simpsonové	Simpsonová	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Eduard	Eduard	k1gMnSc1
působil	působit	k5eAaImAgMnS
ve	v	k7c6
vládních	vládní	k2eAgInPc6d1
kruzích	kruh	k1gInPc6
nesnáze	snadno	k6eNd2
svými	svůj	k3xOyFgFnPc7
aktivitami	aktivita	k1gFnPc7
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
mohly	moct	k5eAaImAgFnP
být	být	k5eAaImF
interpretovány	interpretovat	k5eAaBmNgInP
jako	jako	k9
zásahy	zásah	k1gInPc1
do	do	k7c2
politiky	politika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
návštěvě	návštěva	k1gFnSc6
vesnic	vesnice	k1gFnPc2
postižených	postižený	k2eAgFnPc2d1
snižováním	snižování	k1gNnSc7
těžby	těžba	k1gFnSc2
uhlí	uhlí	k1gNnSc2
se	se	k3xPyFc4
vyjádřil	vyjádřit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
pro	pro	k7c4
nezaměstnané	zaměstnaný	k2eNgMnPc4d1
horníky	horník	k1gMnPc4
mělo	mít	k5eAaImAgNnS
být	být	k5eAaImF
něco	něco	k6eAd1
podniknuto	podniknout	k5eAaPmNgNnS
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
bylo	být	k5eAaImAgNnS
vykládáno	vykládat	k5eAaImNgNnS
jako	jako	k9
kritika	kritika	k1gFnSc1
současné	současný	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
<g/>
,	,	kIx,
i	i	k9
když	když	k8xS
nebylo	být	k5eNaImAgNnS
jasné	jasný	k2eAgNnSc1d1
<g/>
,	,	kIx,
zda	zda	k8xS
to	ten	k3xDgNnSc4
tak	tak	k6eAd1
Eduard	Eduard	k1gMnSc1
zamýšlel	zamýšlet	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ministři	ministr	k1gMnPc1
se	se	k3xPyFc4
obávali	obávat	k5eAaImAgMnP
posílat	posílat	k5eAaImF
tajné	tajný	k2eAgInPc4d1
dokumenty	dokument	k1gInPc4
do	do	k7c2
Fort	Fort	k?
Belvederu	Belveder	k1gInSc2
<g/>
,	,	kIx,
protože	protože	k8xS
se	se	k3xPyFc4
obávali	obávat	k5eAaImAgMnP
<g/>
,	,	kIx,
že	že	k8xS
Eduard	Eduard	k1gMnSc1
si	se	k3xPyFc3
není	být	k5eNaImIp3nS
jist	jist	k2eAgInSc1d1
jejich	jejich	k3xOp3gInSc7
významem	význam	k1gInSc7
a	a	k8xC
paní	paní	k1gFnSc1
Simpsonová	Simpsonová	k1gFnSc1
nebo	nebo	k8xC
její	její	k3xOp3gMnPc1
hosté	host	k1gMnPc1
by	by	kYmCp3nP
se	se	k3xPyFc4
k	k	k7c3
nim	on	k3xPp3gFnPc3
mohli	moct	k5eAaImAgMnP
dostat	dostat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
průběhu	průběh	k1gInSc6
srpna	srpen	k1gInSc2
a	a	k8xC
září	září	k1gNnSc2
podnikli	podniknout	k5eAaPmAgMnP
Eduard	Eduard	k1gMnSc1
a	a	k8xC
Simpsonová	Simpsonová	k1gFnSc1
okružní	okružní	k2eAgFnSc4d1
cestu	cesta	k1gFnSc4
lodí	loď	k1gFnPc2
po	po	k7c6
Středozemním	středozemní	k2eAgNnSc6d1
moři	moře	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
říjnu	říjen	k1gInSc6
začalo	začít	k5eAaPmAgNnS
být	být	k5eAaImF
zřejmé	zřejmý	k2eAgNnSc1d1
(	(	kIx(
<g/>
zvláště	zvláště	k6eAd1
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yQnSc4,k3yInSc4,k3yRnSc4
byl	být	k5eAaImAgInS
potvrzen	potvrzen	k2eAgInSc1d1
rozvod	rozvod	k1gInSc1
manželů	manžel	k1gMnPc2
Simpsonových	Simpsonův	k2eAgMnPc2d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
nový	nový	k2eAgMnSc1d1
král	král	k1gMnSc1
chce	chtít	k5eAaImIp3nS
se	s	k7c7
svou	svůj	k3xOyFgFnSc7
milenkou	milenka	k1gFnSc7
oženit	oženit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byly	být	k5eAaImAgFnP
zahájeny	zahájit	k5eAaPmNgFnP
přípravy	příprava	k1gFnPc1
na	na	k7c4
korunovaci	korunovace	k1gFnSc4
krále	král	k1gMnSc2
a	a	k8xC
královny	královna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
ohledem	ohled	k1gInSc7
na	na	k7c4
církevní	církevní	k2eAgNnSc4d1
prostředí	prostředí	k1gNnSc4
a	a	k8xC
nejasnosti	nejasnost	k1gFnPc4
ohledně	ohledně	k7c2
církevního	církevní	k2eAgInSc2d1
postoje	postoj	k1gInSc2
ke	k	k7c3
sňatku	sňatek	k1gInSc3
se	se	k3xPyFc4
neměla	mít	k5eNaImAgFnS
korunovace	korunovace	k1gFnSc1
konat	konat	k5eAaImF
ve	v	k7c6
Westminsterském	Westminsterský	k2eAgNnSc6d1
opatství	opatství	k1gNnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
v	v	k7c4
Banqueting	Banqueting	k1gInSc4
House	house	k1gNnSc1
na	na	k7c6
Whitehallu	Whitehall	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Abdikace	abdikace	k1gFnSc1
</s>
<s>
Edward	Edward	k1gMnSc1
VIII	VIII	kA
s	s	k7c7
tureckým	turecký	k2eAgMnSc7d1
prezidentem	prezident	k1gMnSc7
Mustafou	Mustafa	k1gMnSc7
Kemalem	Kemal	k1gMnSc7
Atatürkem	Atatürek	k1gMnSc7
v	v	k7c6
Istanbulu	Istanbul	k1gInSc6
<g/>
,	,	kIx,
(	(	kIx(
<g/>
4	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1936	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Eduard	Eduard	k1gMnSc1
při	při	k7c6
návštévě	návštéva	k1gFnSc6
Kanady	Kanada	k1gFnSc2
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1936	#num#	k4
pozval	pozvat	k5eAaPmAgMnS
Eduard	Eduard	k1gMnSc1
premiéra	premiér	k1gMnSc2
Stanleye	Stanleye	k1gNnSc2
Baldwina	Baldwin	k1gMnSc2
do	do	k7c2
Buckinghamského	buckinghamský	k2eAgInSc2d1
paláce	palác	k1gInSc2
a	a	k8xC
vyjádřil	vyjádřit	k5eAaPmAgMnS
svůj	svůj	k3xOyFgInSc4
úmysl	úmysl	k1gInSc4
oženit	oženit	k5eAaPmF
se	se	k3xPyFc4
s	s	k7c7
Wallis	Wallis	k1gFnSc7
Simpsonovou	Simpsonová	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Baldwin	Baldwin	k1gMnSc1
ho	on	k3xPp3gMnSc4
informoval	informovat	k5eAaBmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
jeho	jeho	k3xOp3gMnPc1
poddaní	poddaný	k1gMnPc1
budou	být	k5eAaImBp3nP
považovat	považovat	k5eAaImF
tento	tento	k3xDgInSc4
sňatek	sňatek	k1gInSc4
za	za	k7c4
morálně	morálně	k6eAd1
neakceptovatelný	akceptovatelný	k2eNgInSc4d1
<g/>
,	,	kIx,
zvláště	zvláště	k6eAd1
proto	proto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
tento	tento	k3xDgInSc4
sňatek	sňatek	k1gInSc4
s	s	k7c7
rozvedenou	rozvedený	k2eAgFnSc7d1
ženou	žena	k1gFnSc7
kritizován	kritizovat	k5eAaImNgMnS
anglikánskou	anglikánský	k2eAgFnSc7d1
církví	církev	k1gFnSc7
<g/>
,	,	kIx,
a	a	k8xC
lidé	člověk	k1gMnPc1
Simpsonovou	Simpsonová	k1gFnSc4
jako	jako	k8xS,k8xC
královnu	královna	k1gFnSc4
nepřijmou	přijmout	k5eNaPmIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Eduard	Eduard	k1gMnSc1
byl	být	k5eAaImAgMnS
jako	jako	k9
král	král	k1gMnSc1
hlavním	hlavní	k2eAgMnSc7d1
představitelem	představitel	k1gMnSc7
církve	církev	k1gFnSc2
a	a	k8xC
kněží	kněz	k1gMnPc1
očekávali	očekávat	k5eAaImAgMnP
<g/>
,	,	kIx,
že	že	k8xS
bude	být	k5eAaImBp3nS
názory	názor	k1gInPc7
církve	církev	k1gFnSc2
respektovat	respektovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
Eduard	Eduard	k1gMnSc1
navrhl	navrhnout	k5eAaPmAgMnS
alternativu	alternativa	k1gFnSc4
morganatického	morganatický	k2eAgNnSc2d1
manželství	manželství	k1gNnSc2
<g/>
,	,	kIx,
v	v	k7c6
jejímž	jejíž	k3xOyRp3gInSc6
rámci	rámec	k1gInSc6
by	by	kYmCp3nP
zůstal	zůstat	k5eAaPmAgInS
králem	král	k1gMnSc7
<g/>
,	,	kIx,
avšak	avšak	k8xC
jeho	jeho	k3xOp3gFnSc1
manželka	manželka	k1gFnSc1
by	by	kYmCp3nS
nepoužívala	používat	k5eNaImAgFnS
titul	titul	k1gInSc4
královny	královna	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
byl	být	k5eAaImAgInS
by	by	kYmCp3nS
jí	on	k3xPp3gFnSc3
udělen	udělen	k2eAgInSc1d1
nižší	nízký	k2eAgInSc4d2
šlechtický	šlechtický	k2eAgInSc4d1
titul	titul	k1gInSc4
a	a	k8xC
jejich	jejich	k3xOp3gMnPc1
případní	případný	k2eAgMnPc1d1
potomci	potomek	k1gMnPc1
by	by	kYmCp3nP
nebyli	být	k5eNaImAgMnP
následníky	následník	k1gMnPc7
trůnu	trůn	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
návrh	návrh	k1gInSc1
byl	být	k5eAaImAgInS
britskou	britský	k2eAgFnSc7d1
vládou	vláda	k1gFnSc7
a	a	k8xC
vládami	vláda	k1gFnPc7
jiných	jiný	k2eAgFnPc2d1
dominií	dominie	k1gFnPc2
na	na	k7c6
základě	základ	k1gInSc6
zákona	zákon	k1gInSc2
z	z	k7c2
roku	rok	k1gInSc2
1931	#num#	k4
(	(	kIx(
<g/>
Statute	statut	k1gInSc5
of	of	k?
Westminster	Westminster	k1gInSc1
1931	#num#	k4
<g/>
)	)	kIx)
odmítnut	odmítnout	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
zákon	zákon	k1gInSc1
uvádí	uvádět	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
změny	změna	k1gFnPc1
v	v	k7c6
následnictví	následnictví	k1gNnSc6
na	na	k7c6
britském	britský	k2eAgInSc6d1
trůnu	trůn	k1gInSc6
vyžadují	vyžadovat	k5eAaImIp3nP
souhlas	souhlas	k1gInSc4
parlamentů	parlament	k1gInPc2
dominií	dominie	k1gFnPc2
i	i	k8xC
britského	britský	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předsedové	předseda	k1gMnPc1
vlád	vláda	k1gFnPc2
Austrálie	Austrálie	k1gFnSc2
<g/>
,	,	kIx,
Kanady	Kanada	k1gFnSc2
a	a	k8xC
Jihoafrické	jihoafrický	k2eAgFnSc2d1
unie	unie	k1gFnSc2
vyjádřili	vyjádřit	k5eAaPmAgMnP
svůj	svůj	k3xOyFgInSc4
nesouhlas	nesouhlas	k1gInSc4
s	s	k7c7
Eduardovým	Eduardův	k2eAgInSc7d1
sňatkem	sňatek	k1gInSc7
s	s	k7c7
rozvedenou	rozvedený	k2eAgFnSc7d1
ženou	žena	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Eduard	Eduard	k1gMnSc1
pak	pak	k6eAd1
sdělil	sdělit	k5eAaPmAgMnS
Baldwinovi	Baldwin	k1gMnSc3
<g/>
,	,	kIx,
že	že	k8xS
bude	být	k5eAaImBp3nS
abdikovat	abdikovat	k5eAaBmF
<g/>
,	,	kIx,
pokud	pokud	k8xS
by	by	kYmCp3nS
jeho	jeho	k3xOp3gInSc1
sňatek	sňatek	k1gInSc1
nebyl	být	k5eNaImAgInS
přijatelný	přijatelný	k2eAgInSc1d1
pro	pro	k7c4
britskou	britský	k2eAgFnSc4d1
veřejnost	veřejnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Baldwin	Baldwina	k1gFnPc2
pak	pak	k6eAd1
navrhl	navrhnout	k5eAaPmAgMnS
tři	tři	k4xCgFnPc4
možnosti	možnost	k1gFnPc4
–	–	k?
zavržení	zavržení	k1gNnSc1
myšlenky	myšlenka	k1gFnSc2
na	na	k7c4
sňatek	sňatek	k1gInSc4
<g/>
,	,	kIx,
sňatek	sňatek	k1gInSc1
proti	proti	k7c3
vůli	vůle	k1gFnSc3
vlády	vláda	k1gFnSc2
anebo	anebo	k8xC
abdikace	abdikace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Eduard	Eduard	k1gMnSc1
se	se	k3xPyFc4
sňatku	sňatek	k1gInSc3
nechtěl	chtít	k5eNaImAgInS
vzdát	vzdát	k5eAaPmF
a	a	k8xC
věděl	vědět	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
vyvolal	vyvolat	k5eAaPmAgInS
demisi	demise	k1gFnSc4
vlády	vláda	k1gFnSc2
a	a	k8xC
ústavní	ústavní	k2eAgFnSc3d1
krizi	krize	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zvolil	zvolit	k5eAaPmAgMnS
tedy	tedy	k9
abdikaci	abdikace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Dokument	dokument	k1gInSc1
o	o	k7c6
tom	ten	k3xDgNnSc6
podepsal	podepsat	k5eAaPmAgMnS
ve	v	k7c6
Fort	Fort	k?
Belvedere	Belveder	k1gInSc5
10	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1936	#num#	k4
v	v	k7c6
přítomnosti	přítomnost	k1gFnSc6
svých	svůj	k3xOyFgMnPc2
bratrů	bratr	k1gMnPc2
Alberta	Albert	k1gMnSc2
<g/>
,	,	kIx,
vévody	vévoda	k1gMnSc2
z	z	k7c2
Yorku	York	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
Jiřího	Jiří	k1gMnSc2
<g/>
,	,	kIx,
vévody	vévoda	k1gMnSc2
z	z	k7c2
Kentu	Kent	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následující	následující	k2eAgInSc4d1
den	den	k1gInSc4
vyslovil	vyslovit	k5eAaPmAgInS
královský	královský	k2eAgInSc1d1
souhlas	souhlas	k1gInSc1
s	s	k7c7
deklarací	deklarace	k1gFnSc7
o	o	k7c6
své	svůj	k3xOyFgFnSc6
abdikaci	abdikace	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
zákona	zákon	k1gInSc2
z	z	k7c2
roku	rok	k1gInSc2
1931	#num#	k4
musela	muset	k5eAaImAgFnS
s	s	k7c7
jeho	jeho	k3xOp3gFnSc7
abdikací	abdikace	k1gFnSc7
souhlasit	souhlasit	k5eAaImF
i	i	k9
všechna	všechen	k3xTgNnPc4
dominia	dominion	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
abdikaci	abdikace	k1gFnSc6
vysvětlil	vysvětlit	k5eAaPmAgMnS
národu	národ	k1gInSc2
v	v	k7c6
rozhlasovém	rozhlasový	k2eAgInSc6d1
projevu	projev	k1gInSc6
důvody	důvod	k1gInPc4
své	svůj	k3xOyFgFnSc2
abdikace	abdikace	k1gFnSc2
a	a	k8xC
odjel	odjet	k5eAaPmAgMnS
do	do	k7c2
Rakouska	Rakousko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc4
bratr	bratr	k1gMnSc1
Albert	Albert	k1gMnSc1
<g/>
,	,	kIx,
vévoda	vévoda	k1gMnSc1
z	z	k7c2
Yorku	York	k1gInSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
jeho	jeho	k3xOp3gMnSc7
nástupcem	nástupce	k1gMnSc7
jako	jako	k8xS,k8xC
Jiří	Jiří	k1gMnPc1
VI	VI	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Eduard	Eduard	k1gMnSc1
se	se	k3xPyFc4
s	s	k7c7
Wallis	Wallis	k1gFnSc7
Simpsonovou	Simpsonový	k2eAgFnSc7d1
oženil	oženit	k5eAaPmAgMnS
3	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1937	#num#	k4
v	v	k7c6
soukromém	soukromý	k2eAgInSc6d1
obřadu	obřad	k1gInSc6
ve	v	k7c6
Francii	Francie	k1gFnSc6
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
byl	být	k5eAaImAgInS
později	pozdě	k6eAd2
potvrzen	potvrdit	k5eAaPmNgInS
obřadem	obřad	k1gInSc7
v	v	k7c6
Anglii	Anglie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Simpsonová	Simpsonová	k1gFnSc1
po	po	k7c6
svatbě	svatba	k1gFnSc6
přijala	přijmout	k5eAaPmAgFnS
příjmení	příjmení	k1gNnSc4
Warfieldová	Warfieldová	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Druhá	druhý	k4xOgFnSc1
světová	světový	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
<s>
Eduard	Eduard	k1gMnSc1
v	v	k7c6
roce	rok	k1gInSc6
1945	#num#	k4
</s>
<s>
V	v	k7c6
říjnu	říjen	k1gInSc6
1937	#num#	k4
Eduard	Eduard	k1gMnSc1
se	se	k3xPyFc4
svou	svůj	k3xOyFgFnSc7
manželkou	manželka	k1gFnSc7
navštívili	navštívit	k5eAaPmAgMnP
<g/>
,	,	kIx,
navzdory	navzdory	k7c3
doporučení	doporučení	k1gNnSc3
britské	britský	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
<g/>
,	,	kIx,
nacistické	nacistický	k2eAgNnSc4d1
Německo	Německo	k1gNnSc4
a	a	k8xC
setkali	setkat	k5eAaPmAgMnP
se	se	k3xPyFc4
s	s	k7c7
Adolfem	Adolf	k1gMnSc7
Hitlerem	Hitler	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
návštěva	návštěva	k1gFnSc1
byla	být	k5eAaImAgFnS
využita	využít	k5eAaPmNgFnS
německou	německý	k2eAgFnSc7d1
propagandou	propaganda	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
návštěvy	návštěva	k1gFnSc2
Eduard	Eduard	k1gMnSc1
dokonce	dokonce	k9
používal	používat	k5eAaImAgMnS
nacistický	nacistický	k2eAgInSc4d1
pozdrav	pozdrav	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
používal	používat	k5eAaImAgInS
i	i	k9
v	v	k7c6
kruhu	kruh	k1gInSc6
rodiny	rodina	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Bývalý	bývalý	k2eAgMnSc1d1
rakouský	rakouský	k2eAgMnSc1d1
velvyslanec	velvyslanec	k1gMnSc1
uváděl	uvádět	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
Eduard	Eduard	k1gMnSc1
dával	dávat	k5eAaImAgMnS
přednost	přednost	k1gFnSc4
fašismu	fašismus	k1gInSc2
jako	jako	k8xC,k8xS
hrázi	hráze	k1gFnSc4
proti	proti	k7c3
komunismu	komunismus	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Eduardovy	Eduardův	k2eAgInPc4d1
zážitky	zážitek	k1gInPc4
z	z	k7c2
první	první	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
ho	on	k3xPp3gNnSc4
vedly	vést	k5eAaImAgFnP
k	k	k7c3
příklonu	příklon	k1gInSc3
k	k	k7c3
appeasementu	appeasement	k1gInSc3
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
vypuknutí	vypuknutí	k1gNnSc6
války	válka	k1gFnSc2
v	v	k7c6
září	září	k1gNnSc6
roku	rok	k1gInSc2
1939	#num#	k4
byli	být	k5eAaImAgMnP
Eduard	Eduard	k1gMnSc1
s	s	k7c7
manželkou	manželka	k1gFnSc7
převezeni	převézt	k5eAaPmNgMnP
z	z	k7c2
Francie	Francie	k1gFnSc2
do	do	k7c2
Británie	Británie	k1gFnSc2
a	a	k8xC
Eduard	Eduard	k1gMnSc1
byl	být	k5eAaImAgMnS
jmenován	jmenovat	k5eAaImNgMnS,k5eAaBmNgMnS
generálmajorem	generálmajor	k1gMnSc7
přiděleným	přidělený	k2eAgMnSc7d1
k	k	k7c3
britské	britský	k2eAgFnSc3d1
vojenské	vojenský	k2eAgFnSc3d1
misi	mise	k1gFnSc3
ve	v	k7c6
Francii	Francie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
únoru	únor	k1gInSc6
1940	#num#	k4
prohlásil	prohlásit	k5eAaPmAgMnS
německý	německý	k2eAgMnSc1d1
velvyslanec	velvyslanec	k1gMnSc1
v	v	k7c6
Haagu	Haag	k1gInSc6
Julius	Julius	k1gMnSc1
von	von	k1gInSc1
Zech-Burkersroda	Zech-Burkersroda	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
Eduard	Eduard	k1gMnSc1
zavinil	zavinit	k5eAaPmAgMnS
únik	únik	k1gInSc4
informací	informace	k1gFnPc2
o	o	k7c6
plánech	plán	k1gInPc6
spojenecké	spojenecký	k2eAgFnSc2d1
obrany	obrana	k1gFnSc2
Belgie	Belgie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
napadení	napadení	k1gNnSc6
Francie	Francie	k1gFnSc2
se	se	k3xPyFc4
Eduard	Eduard	k1gMnSc1
se	s	k7c7
svou	svůj	k3xOyFgFnSc7
manželkou	manželka	k1gFnSc7
přesunul	přesunout	k5eAaPmAgMnS
na	na	k7c4
jih	jih	k1gInSc4
a	a	k8xC
později	pozdě	k6eAd2
do	do	k7c2
Portugalska	Portugalsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Eduard	Eduard	k1gMnSc1
požádal	požádat	k5eAaPmAgMnS
německé	německý	k2eAgNnSc4d1
vojsko	vojsko	k1gNnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
postavilo	postavit	k5eAaPmAgNnS
k	k	k7c3
jeho	jeho	k3xOp3gInSc3
domu	dům	k1gInSc3
v	v	k7c6
Paříži	Paříž	k1gFnSc6
stráž	stráž	k1gFnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
nedošlo	dojít	k5eNaPmAgNnS
k	k	k7c3
jeho	jeho	k3xOp3gNnSc3
drancování	drancování	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poraženecký	poraženecký	k2eAgInSc4d1
rozhovor	rozhovor	k1gInSc4
s	s	k7c7
Eduardem	Eduard	k1gMnSc7
byl	být	k5eAaImAgInS
poslední	poslední	k2eAgFnSc7d1
kapkou	kapka	k1gFnSc7
pro	pro	k7c4
britskou	britský	k2eAgFnSc4d1
vládu	vláda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Winston	Winston	k1gInSc1
Churchill	Churchill	k1gInSc4
mu	on	k3xPp3gMnSc3
pohrozil	pohrozit	k5eAaPmAgMnS
soudem	soud	k1gInSc7
<g/>
,	,	kIx,
pokud	pokud	k8xS
se	se	k3xPyFc4
nevrátí	vrátit	k5eNaPmIp3nS
do	do	k7c2
Británie	Británie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
srpnu	srpen	k1gInSc6
byli	být	k5eAaImAgMnP
Eduard	Eduard	k1gMnSc1
s	s	k7c7
manželkou	manželka	k1gFnSc7
dopraveni	dopravit	k5eAaPmNgMnP
na	na	k7c4
Bahamy	Bahamy	k1gFnPc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
podle	podle	k7c2
Churchilla	Churchillo	k1gNnSc2
nehrozilo	hrozit	k5eNaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
Eduard	Eduard	k1gMnSc1
mohl	moct	k5eAaImAgMnS
poškozovat	poškozovat	k5eAaImF
britské	britský	k2eAgInPc4d1
zájmy	zájem	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Eduard	Eduard	k1gMnSc1
zde	zde	k6eAd1
zastával	zastávat	k5eAaImAgMnS
až	až	k6eAd1
do	do	k7c2
konce	konec	k1gInSc2
války	válka	k1gFnSc2
funkci	funkce	k1gFnSc4
guvernéra	guvernér	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Mnoho	mnoho	k4c1
historiků	historik	k1gMnPc2
se	se	k3xPyFc4
domnívá	domnívat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
Hitler	Hitler	k1gMnSc1
byl	být	k5eAaImAgMnS
připraven	připravit	k5eAaPmNgMnS
dosadit	dosadit	k5eAaPmF
Eduarda	Eduard	k1gMnSc4
na	na	k7c4
britský	britský	k2eAgInSc4d1
trůn	trůn	k1gInSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
podpořil	podpořit	k5eAaPmAgMnS
myšlenku	myšlenka	k1gFnSc4
fašismu	fašismus	k1gInSc2
v	v	k7c6
Británii	Británie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
známo	znám	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
jak	jak	k8xC,k8xS
Eduard	Eduard	k1gMnSc1
<g/>
,	,	kIx,
tak	tak	k8xS,k8xC
především	především	k9
jeho	jeho	k3xOp3gFnSc1
manželka	manželka	k1gFnSc1
<g/>
,	,	kIx,
sympatizovali	sympatizovat	k5eAaImAgMnP
před	před	k7c7
druhou	druhý	k4xOgFnSc7
světovou	světový	k2eAgFnSc7d1
válkou	válka	k1gFnSc7
a	a	k8xC
v	v	k7c6
jejím	její	k3xOp3gInSc6
průběhu	průběh	k1gInSc6
s	s	k7c7
fašismem	fašismus	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vévoda	vévoda	k1gMnSc1
Karl	Karl	k1gMnSc1
Alexander	Alexandra	k1gFnPc2
von	von	k1gInSc1
Württemberg	Württemberg	k1gInSc1
prozradil	prozradit	k5eAaPmAgMnS
FBI	FBI	kA
<g/>
,	,	kIx,
že	že	k8xS
Eduardova	Eduardův	k2eAgFnSc1d1
manželka	manželka	k1gFnSc1
se	se	k3xPyFc4
stýkala	stýkat	k5eAaImAgFnS
s	s	k7c7
německým	německý	k2eAgMnSc7d1
velvyslancem	velvyslanec	k1gMnSc7
v	v	k7c6
Londýně	Londýn	k1gInSc6
<g/>
,	,	kIx,
Joachimem	Joachim	k1gMnSc7
von	von	k1gInSc4
Ribbentropem	Ribbentrop	k1gInSc7
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
s	s	k7c7
ním	on	k3xPp3gNnSc7
v	v	k7c6
kontaktu	kontakt	k1gInSc6
i	i	k8xC
v	v	k7c6
průběhu	průběh	k1gInSc6
války	válka	k1gFnSc2
a	a	k8xC
předávala	předávat	k5eAaImAgFnS
mu	on	k3xPp3gMnSc3
některé	některý	k3yIgFnPc4
tajné	tajný	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Pozdní	pozdní	k2eAgNnSc1d1
období	období	k1gNnSc1
</s>
<s>
Po	po	k7c6
válce	válka	k1gFnSc6
se	se	k3xPyFc4
oba	dva	k4xCgMnPc1
vrátili	vrátit	k5eAaPmAgMnP
do	do	k7c2
Francie	Francie	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
žili	žít	k5eAaImAgMnP
v	v	k7c6
ústraní	ústraní	k1gNnSc6
<g/>
,	,	kIx,
protože	protože	k8xS
Eduardovi	Eduardův	k2eAgMnPc1d1
nebyla	být	k5eNaImAgNnP
<g/>
,	,	kIx,
poté	poté	k6eAd1
co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
skončil	skončit	k5eAaPmAgMnS
ve	v	k7c6
funkci	funkce	k1gFnSc6
guvernéra	guvernér	k1gMnSc2
na	na	k7c6
Bahamách	Bahamy	k1gFnPc6
<g/>
,	,	kIx,
nabídnutá	nabídnutý	k2eAgFnSc1d1
žádná	žádný	k3yNgFnSc1
oficiální	oficiální	k2eAgFnSc1d1
funkce	funkce	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gInPc4
výdaje	výdaj	k1gInPc4
byly	být	k5eAaImAgInP
částečně	částečně	k6eAd1
financovány	financován	k2eAgInPc1d1
britskou	britský	k2eAgFnSc7d1
vládou	vláda	k1gFnSc7
a	a	k8xC
město	město	k1gNnSc1
Paříž	Paříž	k1gFnSc1
jim	on	k3xPp3gMnPc3
pronajalo	pronajmout	k5eAaPmAgNnS
dům	dům	k1gInSc4
za	za	k7c4
symbolickou	symbolický	k2eAgFnSc4d1
cenu	cena	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1951	#num#	k4
vydal	vydat	k5eAaPmAgMnS
Eduard	Eduard	k1gMnSc1
knihu	kniha	k1gFnSc4
"	"	kIx"
<g/>
Příběh	příběh	k1gInSc1
krále	král	k1gMnSc2
<g/>
"	"	kIx"
a	a	k8xC
příjmy	příjem	k1gInPc1
z	z	k7c2
vydání	vydání	k1gNnSc2
této	tento	k3xDgFnSc2
knihy	kniha	k1gFnSc2
přispívaly	přispívat	k5eAaImAgFnP
do	do	k7c2
rozpočtu	rozpočet	k1gInSc2
dvojice	dvojice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Královská	královský	k2eAgFnSc1d1
rodina	rodina	k1gFnSc1
Eduardovu	Eduardův	k2eAgFnSc4d1
manželku	manželka	k1gFnSc4
nikdy	nikdy	k6eAd1
nepřijala	přijmout	k5eNaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
On	on	k3xPp3gMnSc1
sám	sám	k3xTgMnSc1
se	se	k3xPyFc4
občas	občas	k6eAd1
setkal	setkat	k5eAaPmAgMnS
s	s	k7c7
některým	některý	k3yIgMnSc7
jejím	její	k3xOp3gMnSc7
členem	člen	k1gMnSc7
a	a	k8xC
účastnil	účastnit	k5eAaImAgMnS
se	se	k3xPyFc4
Jiřího	Jiří	k1gMnSc4
pohřbu	pohřeb	k1gInSc2
<g/>
,	,	kIx,
i	i	k9
když	když	k8xS
korunovaci	korunovace	k1gFnSc4
své	svůj	k3xOyFgFnSc2
neteře	neteř	k1gFnSc2
Alžběty	Alžběta	k1gFnSc2
sledoval	sledovat	k5eAaImAgInS
pouze	pouze	k6eAd1
v	v	k7c6
televizi	televize	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1965	#num#	k4
se	se	k3xPyFc4
manželé	manžel	k1gMnPc1
vrátili	vrátit	k5eAaPmAgMnP
do	do	k7c2
Anglie	Anglie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
60	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
se	se	k3xPyFc4
Eduardovo	Eduardův	k2eAgNnSc1d1
zdraví	zdraví	k1gNnSc1
zhoršilo	zhoršit	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1971	#num#	k4
byla	být	k5eAaImAgFnS
u	u	k7c2
něho	on	k3xPp3gNnSc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
náruživého	náruživý	k2eAgMnSc2d1
kuřáka	kuřák	k1gMnSc2
<g/>
,	,	kIx,
diagnostikována	diagnostikován	k2eAgFnSc1d1
rakovina	rakovina	k1gFnSc1
jícnu	jícen	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zemřel	zemřít	k5eAaPmAgMnS
28	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1972	#num#	k4
v	v	k7c6
Paříži	Paříž	k1gFnSc6
a	a	k8xC
byl	být	k5eAaImAgInS
pochován	pochovat	k5eAaPmNgInS
ve	v	k7c6
Frogmore	Frogmor	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s>
Odrazy	odraz	k1gInPc1
v	v	k7c6
populární	populární	k2eAgFnSc6d1
kultuře	kultura	k1gFnSc6
</s>
<s>
O	o	k7c6
jeho	jeho	k3xOp3gFnSc6
abdikaci	abdikace	k1gFnSc6
a	a	k8xC
vztahu	vztah	k1gInSc2
k	k	k7c3
Wallis	Wallis	k1gFnSc3
Simpsonové	Simpsonová	k1gFnSc2
pojednává	pojednávat	k5eAaImIp3nS
britský	britský	k2eAgInSc1d1
televizní	televizní	k2eAgInSc1d1
seriál	seriál	k1gInSc1
Edvard	Edvard	k1gMnSc1
a	a	k8xC
paní	paní	k1gFnSc1
Simpsonová	Simpsonová	k1gFnSc1
(	(	kIx(
<g/>
Edward	Edward	k1gMnSc1
&	&	k?
Mrs	Mrs	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Simpson	Simpson	k1gNnSc1
<g/>
)	)	kIx)
z	z	k7c2
roku	rok	k1gInSc2
1980	#num#	k4
<g/>
,	,	kIx,
kde	kde	k6eAd1
jeho	jeho	k3xOp3gFnSc4
postavu	postava	k1gFnSc4
ztvárnil	ztvárnit	k5eAaPmAgMnS
britský	britský	k2eAgMnSc1d1
herec	herec	k1gMnSc1
Edward	Edward	k1gMnSc1
Fox	fox	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Podobně	podobně	k6eAd1
se	se	k3xPyFc4
objevuje	objevovat	k5eAaImIp3nS
v	v	k7c6
oscarovém	oscarový	k2eAgInSc6d1
filmu	film	k1gInSc6
Králova	Králův	k2eAgFnSc1d1
řeč	řeč	k1gFnSc1
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kde	kde	k6eAd1
však	však	k9
ústřední	ústřední	k2eAgFnSc7d1
postavou	postava	k1gFnSc7
je	být	k5eAaImIp3nS
jeho	jeho	k3xOp3gNnSc1
bratr	bratr	k1gMnSc1
Albert	Albert	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Dostalo	dostat	k5eAaPmAgNnS
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
nemalé	malý	k2eNgFnSc3d1
pozornosti	pozornost	k1gFnSc3
v	v	k7c6
seriálu	seriál	k1gInSc6
The	The	k1gFnSc2
Crown	Crowna	k1gFnPc2
stanice	stanice	k1gFnSc2
Netflix	Netflix	k1gInSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc7
hlavní	hlavní	k2eAgFnSc7d1
postavou	postava	k1gFnSc7
je	být	k5eAaImIp3nS
královna	královna	k1gFnSc1
a	a	k8xC
Eduardova	Eduardův	k2eAgFnSc1d1
neteř	neteř	k1gFnSc1
Alžběta	Alžběta	k1gFnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Tituly	titul	k1gInPc1
<g/>
,	,	kIx,
oslovení	oslovení	k1gNnSc1
a	a	k8xC
vyznamenání	vyznamenání	k1gNnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Tituly	titul	k1gInPc1
a	a	k8xC
vyznamenání	vyznamenání	k1gNnSc1
Eduarda	Eduard	k1gMnSc2
VIII	VIII	kA
<g/>
..	..	k?
</s>
<s>
Tituly	titul	k1gInPc1
a	a	k8xC
oslovení	oslovení	k1gNnSc1
</s>
<s>
23	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1894	#num#	k4
–	–	k?
28	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1898	#num#	k4
<g/>
:	:	kIx,
Jeho	jeho	k3xOp3gFnSc1
Výsost	výsost	k1gFnSc1
princ	princ	k1gMnSc1
Eduard	Eduard	k1gMnSc1
z	z	k7c2
Yorku	York	k1gInSc2
</s>
<s>
28	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1898	#num#	k4
–	–	k?
22	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1901	#num#	k4
<g/>
:	:	kIx,
Jeho	jeho	k3xOp3gFnSc1
královská	královský	k2eAgFnSc1d1
Výsost	výsost	k1gFnSc1
princ	princ	k1gMnSc1
Eduard	Eduard	k1gMnSc1
z	z	k7c2
Yorku	York	k1gInSc2
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
–	–	k?
9	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1901	#num#	k4
<g/>
:	:	kIx,
Jeho	jeho	k3xOp3gFnSc1
královská	královský	k2eAgFnSc1d1
Výsost	výsost	k1gFnSc1
princ	princ	k1gMnSc1
Eduard	Eduard	k1gMnSc1
z	z	k7c2
Cornwallu	Cornwall	k1gInSc2
a	a	k8xC
Yorku	York	k1gInSc2
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1901	#num#	k4
–	–	k?
6	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1910	#num#	k4
<g/>
:	:	kIx,
Jeho	jeho	k3xOp3gFnSc1
královská	královský	k2eAgFnSc1d1
Výsost	výsost	k1gFnSc1
princ	princ	k1gMnSc1
Eduard	Eduard	k1gMnSc1
z	z	k7c2
Walesu	Wales	k1gInSc2
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
–	–	k?
23	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1910	#num#	k4
<g/>
:	:	kIx,
Jeho	jeho	k3xOp3gFnSc1
královská	královský	k2eAgFnSc1d1
Výsost	výsost	k1gFnSc1
vévoda	vévoda	k1gMnSc1
z	z	k7c2
Cornwallu	Cornwall	k1gInSc2
</s>
<s>
23	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1910	#num#	k4
–	–	k?
20	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1936	#num#	k4
<g/>
:	:	kIx,
Jeho	jeho	k3xOp3gFnSc1
královská	královský	k2eAgFnSc1d1
Výsost	výsost	k1gFnSc1
princ	princa	k1gFnPc2
z	z	k7c2
Walesu	Wales	k1gInSc2
</s>
<s>
ve	v	k7c6
Skotsku	Skotsko	k1gNnSc6
<g/>
:	:	kIx,
Jeho	jeho	k3xOp3gFnSc1
královská	královský	k2eAgFnSc1d1
Výsost	výsost	k1gFnSc1
princ	princ	k1gMnSc1
Eduard	Eduard	k1gMnSc1
<g/>
,	,	kIx,
vévoda	vévoda	k1gMnSc1
z	z	k7c2
Rothesay	Rothesaa	k1gFnSc2
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
–	–	k?
11	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1936	#num#	k4
<g/>
:	:	kIx,
Jeho	jeho	k3xOp3gNnSc4
Veličenstvo	veličenstvo	k1gNnSc4
král	král	k1gMnSc1
</s>
<s>
příležitostně	příležitostně	k6eAd1
<g/>
,	,	kIx,
s	s	k7c7
ohledem	ohled	k1gInSc7
na	na	k7c6
Indii	Indie	k1gFnSc6
<g/>
:	:	kIx,
Jeho	jeho	k3xOp3gNnSc4
Veličenstvo	veličenstvo	k1gNnSc4
král-císař	král-císař	k1gMnSc1
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1936	#num#	k4
–	–	k?
8	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1937	#num#	k4
<g/>
:	:	kIx,
Jeho	jeho	k3xOp3gFnSc1
královská	královský	k2eAgFnSc1d1
Výsost	výsost	k1gFnSc1
princ	princ	k1gMnSc1
Eduard	Eduard	k1gMnSc1
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1937	#num#	k4
–	–	k?
28	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1972	#num#	k4
<g/>
:	:	kIx,
Jeho	jeho	k3xOp3gFnSc1
královská	královský	k2eAgFnSc1d1
Výsost	výsost	k1gFnSc1
vévoda	vévoda	k1gMnSc1
z	z	k7c2
Windsoru	Windsor	k1gInSc2
</s>
<s>
Vývod	vývod	k1gInSc1
z	z	k7c2
předků	předek	k1gInPc2
</s>
<s>
Arnošt	Arnošt	k1gMnSc1
I.	I.	kA
Sasko-Kobursko-Gothajský	Sasko-Kobursko-Gothajský	k2eAgInSc4d1
</s>
<s>
Albert	Albert	k1gMnSc1
Sasko-Kobursko-Gothajský	Sasko-Kobursko-Gothajský	k2eAgMnSc1d1
</s>
<s>
Luisa	Luisa	k1gFnSc1
Sasko-Gothajsko-Altenburská	Sasko-Gothajsko-Altenburský	k2eAgFnSc1d1
</s>
<s>
Eduard	Eduard	k1gMnSc1
VII	VII	kA
<g/>
.	.	kIx.
</s>
<s>
Eduard	Eduard	k1gMnSc1
August	August	k1gMnSc1
Hannoverský	hannoverský	k2eAgMnSc1d1
</s>
<s>
královna	královna	k1gFnSc1
Viktorie	Viktoria	k1gFnSc2
</s>
<s>
Viktorie	Viktorie	k1gFnSc1
Sasko-Kobursko-Saalfeldská	Sasko-Kobursko-Saalfeldský	k2eAgFnSc1d1
</s>
<s>
Jiří	Jiří	k1gMnSc1
V.	V.	kA
</s>
<s>
Fridrich	Fridrich	k1gMnSc1
Vilém	Vilém	k1gMnSc1
Šlesvicko-Holštýnsko-Sonderbursko-Glücksburský	Šlesvicko-Holštýnsko-Sonderbursko-Glücksburský	k2eAgMnSc1d1
</s>
<s>
Kristián	Kristián	k1gMnSc1
IX	IX	kA
<g/>
.	.	kIx.
</s>
<s>
Luisa	Luisa	k1gFnSc1
Karolina	Karolinum	k1gNnSc2
Hesensko-Kasselská	hesensko-kasselský	k2eAgFnSc1d1
</s>
<s>
Alexandra	Alexandra	k1gFnSc1
Dánská	dánský	k2eAgFnSc1d1
</s>
<s>
Vilém	Vilém	k1gMnSc1
Hesensko-Kasselský	hesensko-kasselský	k2eAgMnSc1d1
</s>
<s>
Luisa	Luisa	k1gFnSc1
Hesensko-Kasselská	hesensko-kasselský	k2eAgFnSc1d1
</s>
<s>
Luisa	Luisa	k1gFnSc1
Šarlota	Šarlota	k1gFnSc1
Dánská	dánský	k2eAgFnSc1d1
</s>
<s>
Eduard	Eduard	k1gMnSc1
VIII	VIII	kA
<g/>
.	.	kIx.
</s>
<s>
Ludvík	Ludvík	k1gMnSc1
Württemberský	Württemberský	k2eAgMnSc1d1
</s>
<s>
Alexandr	Alexandr	k1gMnSc1
Württemberský	Württemberský	k2eAgMnSc1d1
</s>
<s>
Henrietta	Henrietta	k1gFnSc1
Nasavsko-Weilburská	Nasavsko-Weilburský	k2eAgFnSc1d1
</s>
<s>
František	František	k1gMnSc1
z	z	k7c2
Tecku	Teck	k1gInSc2
</s>
<s>
László	László	k?
Rhédy	Rhéd	k1gInPc1
de	de	k?
Kis-Rhéde	Kis-Rhéd	k1gMnSc5
</s>
<s>
Claudine	Claudinout	k5eAaPmIp3nS
Rhédey	Rhéde	k1gMnPc4
von	von	k1gInSc1
Kis-Rhéde	Kis-Rhéd	k1gMnSc5
</s>
<s>
Ágnes	Ágnes	k1gMnSc1
Inczédy	Inczéda	k1gFnSc2
de	de	k?
Nagy-Várad	Nagy-Várad	k1gInSc1
</s>
<s>
Marie	Marie	k1gFnSc1
z	z	k7c2
Tecku	Teck	k1gInSc2
</s>
<s>
Jiří	Jiří	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s>
Adolf	Adolf	k1gMnSc1
z	z	k7c2
Cambridge	Cambridge	k1gFnSc2
</s>
<s>
Šarlota	Šarlota	k1gFnSc1
Meklenbursko-Střelická	Meklenbursko-Střelický	k2eAgFnSc1d1
</s>
<s>
Marie	Marie	k1gFnSc1
Adelaida	Adelaida	k1gFnSc1
z	z	k7c2
Cambridge	Cambridge	k1gFnSc2
</s>
<s>
Fridrich	Fridrich	k1gMnSc1
Hesensko-Kasselský	hesensko-kasselský	k2eAgMnSc1d1
</s>
<s>
Augusta	Augusta	k1gMnSc1
Hesensko-Kaselská	Hesensko-Kaselský	k2eAgNnPc1d1
</s>
<s>
Karolina	Karolinum	k1gNnPc1
Nasavsko-Usingenská	Nasavsko-Usingenský	k2eAgNnPc1d1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Edward	Edward	k1gMnSc1
VIII	VIII	kA
of	of	k?
the	the	k?
United	United	k1gInSc1
Kingdom	Kingdom	k1gInSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
<g/>
↑	↑	k?
http://www.novinky.cz/zahranicni/evropa/375340-v-britanii-zverejnili-film-z-roku-1933-kde-soucasna-kralovna-hajluje.html	http://www.novinky.cz/zahranicni/evropa/375340-v-britanii-zverejnili-film-z-roku-1933-kde-soucasna-kralovna-hajluje.html	k1gInSc1
-	-	kIx~
V	v	k7c6
Británii	Británie	k1gFnSc6
zveřejnili	zveřejnit	k5eAaPmAgMnP
film	film	k1gInSc4
z	z	k7c2
roku	rok	k1gInSc2
1933	#num#	k4
<g/>
,	,	kIx,
kde	kde	k6eAd1
současná	současný	k2eAgFnSc1d1
královna	královna	k1gFnSc1
hajluje	hajlovat	k5eAaImIp3nS
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Eduard	Eduard	k1gMnSc1
VIII	VIII	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Britský	britský	k2eAgInSc4d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Osoba	osoba	k1gFnSc1
Edward	Edward	k1gMnSc1
VIII	VIII	kA
<g/>
.	.	kIx.
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
George	Georg	k1gMnSc2
Frederick	Frederick	k1gMnSc1
Ernest	Ernest	k1gMnSc1
Albert	Albert	k1gMnSc1
</s>
<s>
Princ	princ	k1gMnSc1
z	z	k7c2
Walesu	Wales	k1gInSc2
Edward	Edward	k1gMnSc1
Albert	Albert	k1gMnSc1
Christian	Christian	k1gMnSc1
David	David	k1gMnSc1
<g/>
1911	#num#	k4
<g/>
–	–	k?
<g/>
1936	#num#	k4
</s>
<s>
Nástupce	nástupce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Charles	Charles	k1gMnSc1
<g/>
,	,	kIx,
princ	princ	k1gMnSc1
z	z	k7c2
Walesu	Wales	k1gInSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Britští	britský	k2eAgMnPc1d1
panovníci	panovník	k1gMnPc1
Království	království	k1gNnSc1
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
(	(	kIx(
<g/>
1707	#num#	k4
<g/>
–	–	k?
<g/>
1801	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Anna	Anna	k1gFnSc1
Stuartovna	Stuartovna	k1gFnSc1
(	(	kIx(
<g/>
1702	#num#	k4
<g/>
–	–	k?
<g/>
1714	#num#	k4
<g/>
)	)	kIx)
<g/>
1	#num#	k4
</s>
<s>
Jiří	Jiří	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
1714	#num#	k4
<g/>
–	–	k?
<g/>
1727	#num#	k4
<g/>
)	)	kIx)
<g/>
1	#num#	k4
</s>
<s>
Jiří	Jiří	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1727	#num#	k4
<g/>
–	–	k?
<g/>
1760	#num#	k4
<g/>
)	)	kIx)
<g/>
1	#num#	k4
</s>
<s>
Jiří	Jiří	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1760	#num#	k4
<g/>
–	–	k?
<g/>
1801	#num#	k4
<g/>
)	)	kIx)
<g/>
1	#num#	k4
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
a	a	k8xC
Irska	Irsko	k1gNnSc2
(	(	kIx(
<g/>
1801	#num#	k4
<g/>
–	–	k?
<g/>
1927	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jiří	Jiří	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1801	#num#	k4
<g/>
–	–	k?
<g/>
1820	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jiří	Jiří	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1820	#num#	k4
<g/>
–	–	k?
<g/>
1830	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vilém	Vilém	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Britský	britský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1830	#num#	k4
<g/>
–	–	k?
<g/>
1837	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Viktorie	Viktorie	k1gFnSc1
(	(	kIx(
<g/>
1837	#num#	k4
<g/>
–	–	k?
<g/>
1901	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Eduard	Eduard	k1gMnSc1
VII	VII	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1901	#num#	k4
<g/>
–	–	k?
<g/>
1910	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jiří	Jiří	k1gMnSc1
V.	V.	kA
(	(	kIx(
<g/>
1910	#num#	k4
<g/>
–	–	k?
<g/>
1927	#num#	k4
<g/>
)	)	kIx)
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
a	a	k8xC
Severního	severní	k2eAgNnSc2d1
Irska	Irsko	k1gNnSc2
(	(	kIx(
<g/>
od	od	k7c2
r.	r.	kA
1927	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jiří	Jiří	k1gMnSc1
V.	V.	kA
(	(	kIx(
<g/>
1927	#num#	k4
<g/>
–	–	k?
<g/>
1936	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Eduard	Eduard	k1gMnSc1
VIII	VIII	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1936	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jiří	Jiří	k1gMnSc1
VI	VI	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1936	#num#	k4
<g/>
–	–	k?
<g/>
1952	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Alžběta	Alžběta	k1gFnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
od	od	k7c2
1952	#num#	k4
<g/>
)	)	kIx)
1	#num#	k4
<g/>
Rovněž	rovněž	k9
vládce	vládce	k1gMnSc2
Irska	Irsko	k1gNnSc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
xx	xx	k?
<g/>
0	#num#	k4
<g/>
105512	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
118528963	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
1472	#num#	k4
0461	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
50000854	#num#	k4
|	|	kIx~
ULAN	ULAN	kA
<g/>
:	:	kIx,
500189840	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
47553571	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
50000854	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lidé	člověk	k1gMnPc1
|	|	kIx~
Monarchie	monarchie	k1gFnSc1
|	|	kIx~
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
