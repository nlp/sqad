<s>
Studoval	studovat	k5eAaImAgInS
na	na	k7c6
námořních	námořní	k2eAgFnPc6d1
školách	škola	k1gFnPc6
<g/>
,	,	kIx,
nejprve	nejprve	k6eAd1
v	v	k7c6
Osborne	Osborn	k1gInSc5
a	a	k8xC
později	pozdě	k6eAd2
v	v	k7c6
Dartmouthu	Dartmouth	k1gInSc6
a	a	k8xC
sloužil	sloužit	k5eAaImAgMnS
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
u	u	k7c2
britského	britský	k2eAgNnSc2d1
námořnictva	námořnictvo	k1gNnSc2
<g/>
.	.	kIx.
</s>