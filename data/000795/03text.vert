<s>
Carl	Carl	k1gMnSc1	Carl
Sandburg	Sandburg	k1gMnSc1	Sandburg
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1878	[number]	k4	1878
-	-	kIx~	-
22	[number]	k4	22
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
novinář	novinář	k1gMnSc1	novinář
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
hudebník	hudebník	k1gMnSc1	hudebník
a	a	k8xC	a
historik	historik	k1gMnSc1	historik
<g/>
.	.	kIx.	.
</s>
<s>
Třikrát	třikrát	k6eAd1	třikrát
obdržel	obdržet	k5eAaPmAgMnS	obdržet
Pulitzerovu	Pulitzerův	k2eAgFnSc4d1	Pulitzerova
cenu	cena	k1gFnSc4	cena
(	(	kIx(	(
<g/>
1919	[number]	k4	1919
<g/>
,	,	kIx,	,
1940	[number]	k4	1940
<g/>
,	,	kIx,	,
1951	[number]	k4	1951
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dvakrát	dvakrát	k6eAd1	dvakrát
za	za	k7c4	za
dílo	dílo	k1gNnSc4	dílo
literární	literární	k2eAgNnSc4d1	literární
<g/>
,	,	kIx,	,
jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
životopis	životopis	k1gInSc4	životopis
Abrahama	Abraham	k1gMnSc2	Abraham
Lincolna	Lincoln	k1gMnSc2	Lincoln
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
rodina	rodina	k1gFnSc1	rodina
pocházela	pocházet	k5eAaImAgFnS	pocházet
ze	z	k7c2	z
Švédska	Švédsko	k1gNnSc2	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
třinácti	třináct	k4xCc6	třináct
letech	léto	k1gNnPc6	léto
odešel	odejít	k5eAaPmAgMnS	odejít
ze	z	k7c2	z
školy	škola	k1gFnSc2	škola
i	i	k8xC	i
domova	domov	k1gInSc2	domov
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
se	se	k3xPyFc4	se
toulat	toulat	k5eAaImF	toulat
<g/>
.	.	kIx.	.
</s>
<s>
Živil	živit	k5eAaImAgInS	živit
se	se	k3xPyFc4	se
rozvážením	rozvážení	k1gNnSc7	rozvážení
mléka	mléko	k1gNnSc2	mléko
<g/>
,	,	kIx,	,
příležitostnou	příležitostný	k2eAgFnSc7d1	příležitostná
zedničinou	zedničina	k1gFnSc7	zedničina
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
portýr	portýr	k1gMnSc1	portýr
či	či	k8xC	či
sluha	sluha	k1gMnSc1	sluha
v	v	k7c6	v
hotelu	hotel	k1gInSc6	hotel
nebo	nebo	k8xC	nebo
prací	práce	k1gFnPc2	práce
na	na	k7c6	na
farmách	farma	k1gFnPc6	farma
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
dostal	dostat	k5eAaPmAgMnS	dostat
příležitost	příležitost	k1gFnSc4	příležitost
psát	psát	k5eAaImF	psát
pro	pro	k7c4	pro
deník	deník	k1gInSc4	deník
Chicago	Chicago	k1gNnSc4	Chicago
Daily	Daila	k1gFnSc2	Daila
News	Newsa	k1gFnPc2	Newsa
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
začala	začít	k5eAaPmAgFnS	začít
jeho	jeho	k3xOp3gFnSc1	jeho
literární	literární	k2eAgFnSc1d1	literární
kariéra	kariéra	k1gFnSc1	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Chicago	Chicago	k1gNnSc1	Chicago
<g/>
,	,	kIx,	,
nejprůmyslovější	průmyslový	k2eAgNnSc1d3	nejprůmyslovější
a	a	k8xC	a
nejvíce	hodně	k6eAd3	hodně
se	se	k3xPyFc4	se
rozvíjející	rozvíjející	k2eAgNnSc1d1	rozvíjející
město	město	k1gNnSc1	město
Ameriky	Amerika	k1gFnSc2	Amerika
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
ho	on	k3xPp3gMnSc4	on
silně	silně	k6eAd1	silně
ovlivnilo	ovlivnit	k5eAaPmAgNnS	ovlivnit
a	a	k8xC	a
jemu	on	k3xPp3gMnSc3	on
je	být	k5eAaImIp3nS	být
také	také	k9	také
věnována	věnován	k2eAgFnSc1d1	věnována
značná	značný	k2eAgFnSc1d1	značná
část	část	k1gFnSc1	část
jeho	on	k3xPp3gNnSc2	on
básnického	básnický	k2eAgNnSc2d1	básnické
díla	dílo	k1gNnSc2	dílo
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejslavnějších	slavný	k2eAgFnPc2d3	nejslavnější
sbírek	sbírka	k1gFnPc2	sbírka
nese	nést	k5eAaImIp3nS	nést
dokonce	dokonce	k9	dokonce
název	název	k1gInSc1	název
Chicago	Chicago	k1gNnSc1	Chicago
Poems	Poems	k1gInSc1	Poems
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgInPc4	svůj
první	první	k4xOgInPc4	první
verše	verš	k1gInPc4	verš
publikoval	publikovat	k5eAaBmAgMnS	publikovat
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Charles	Charles	k1gMnSc1	Charles
Sandburg	Sandburg	k1gMnSc1	Sandburg
<g/>
.	.	kIx.	.
</s>
<s>
Fascinovala	fascinovat	k5eAaBmAgFnS	fascinovat
ho	on	k3xPp3gNnSc4	on
postava	postava	k1gFnSc1	postava
Abrahama	Abraham	k1gMnSc2	Abraham
Lincolna	Lincoln	k1gMnSc2	Lincoln
a	a	k8xC	a
věnoval	věnovat	k5eAaPmAgMnS	věnovat
jí	jíst	k5eAaImIp3nS	jíst
několik	několik	k4yIc4	několik
biografických	biografický	k2eAgFnPc2d1	biografická
knih	kniha	k1gFnPc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
předmětem	předmět	k1gInSc7	předmět
jeho	on	k3xPp3gInSc2	on
publicistického	publicistický	k2eAgInSc2d1	publicistický
zájmu	zájem	k1gInSc2	zájem
byl	být	k5eAaImAgMnS	být
fotograf	fotograf	k1gMnSc1	fotograf
Edward	Edward	k1gMnSc1	Edward
Steichen	Steichno	k1gNnPc2	Steichno
<g/>
.	.	kIx.	.
</s>
<s>
Známé	známý	k2eAgFnPc1d1	známá
jsou	být	k5eAaImIp3nP	být
též	též	k9	též
jeho	jeho	k3xOp3gFnSc2	jeho
knihy	kniha	k1gFnSc2	kniha
pohádek	pohádka	k1gFnPc2	pohádka
Rootabaga	Rootabaga	k1gFnSc1	Rootabaga
Stories	Stories	k1gInSc1	Stories
<g/>
,	,	kIx,	,
Rootabaga	Rootabaga	k1gFnSc1	Rootabaga
Pigeons	Pigeonsa	k1gFnPc2	Pigeonsa
a	a	k8xC	a
Potato	Potat	k2eAgNnSc4d1	Potato
Face	Face	k1gNnSc4	Face
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
tradiční	tradiční	k2eAgMnPc4d1	tradiční
pohádkové	pohádkový	k2eAgMnPc4d1	pohádkový
hrdiny	hrdina	k1gMnPc4	hrdina
a	a	k8xC	a
rekvizity	rekvizita	k1gFnPc1	rekvizita
byly	být	k5eAaImAgFnP	být
nahrazeny	nahrazen	k2eAgInPc4d1	nahrazen
předměty	předmět	k1gInPc4	předmět
moderního	moderní	k2eAgNnSc2d1	moderní
velkoměsta	velkoměsto	k1gNnSc2	velkoměsto
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
vlaky	vlak	k1gInPc4	vlak
<g/>
,	,	kIx,	,
mrakodrapy	mrakodrap	k1gInPc4	mrakodrap
nebo	nebo	k8xC	nebo
preclíky	preclík	k1gInPc4	preclík
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgInPc4	svůj
verše	verš	k1gInPc4	verš
rovněž	rovněž	k9	rovněž
zhudebňoval	zhudebňovat	k5eAaImAgInS	zhudebňovat
a	a	k8xC	a
zpíval	zpívat	k5eAaImAgInS	zpívat
<g/>
.	.	kIx.	.
</s>
<s>
Sbíral	sbírat	k5eAaImAgMnS	sbírat
také	také	k9	také
americkou	americký	k2eAgFnSc4d1	americká
lidovou	lidový	k2eAgFnSc4d1	lidová
poezii	poezie	k1gFnSc4	poezie
a	a	k8xC	a
popěvky	popěvka	k1gFnPc4	popěvka
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
shromáždil	shromáždit	k5eAaPmAgMnS	shromáždit
ve	v	k7c6	v
třech	tři	k4xCgFnPc6	tři
sbírkách	sbírka	k1gFnPc6	sbírka
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
zdaleka	zdaleka	k6eAd1	zdaleka
nejpopulárnější	populární	k2eAgInPc4d3	nejpopulárnější
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
kniha	kniha	k1gFnSc1	kniha
The	The	k1gMnSc1	The
American	American	k1gMnSc1	American
Songbag	Songbag	k1gMnSc1	Songbag
<g/>
.	.	kIx.	.
</s>
<s>
Politicky	politicky	k6eAd1	politicky
byl	být	k5eAaImAgInS	být
vlevo	vlevo	k6eAd1	vlevo
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
člen	člen	k1gMnSc1	člen
Socialist	socialist	k1gMnSc1	socialist
Party	parta	k1gFnSc2	parta
of	of	k?	of
America	America	k1gMnSc1	America
a	a	k8xC	a
podporoval	podporovat	k5eAaImAgMnS	podporovat
hnutí	hnutí	k1gNnSc4	hnutí
za	za	k7c4	za
práva	právo	k1gNnPc4	právo
černochů	černoch	k1gMnPc2	černoch
<g/>
,	,	kIx,	,
především	především	k9	především
organizaci	organizace	k1gFnSc4	organizace
National	National	k1gFnSc2	National
Association	Association	k1gInSc1	Association
for	forum	k1gNnPc2	forum
the	the	k?	the
Advancement	Advancement	k1gMnSc1	Advancement
of	of	k?	of
Colored	Colored	k1gMnSc1	Colored
People	People	k1gMnSc1	People
<g/>
.	.	kIx.	.
</s>
<s>
Režisér	režisér	k1gMnSc1	režisér
Steven	Stevna	k1gFnPc2	Stevna
Spielberg	Spielberg	k1gMnSc1	Spielberg
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
Sandburgova	Sandburgův	k2eAgFnSc1d1	Sandburgův
tvář	tvář	k1gFnSc1	tvář
byla	být	k5eAaImAgFnS	být
jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
vzorů	vzor	k1gInPc2	vzor
pro	pro	k7c4	pro
vytvoření	vytvoření	k1gNnSc4	vytvoření
tváře	tvář	k1gFnSc2	tvář
E.T.	E.T.	k1gMnSc2	E.T.
mimozemšťana	mimozemšťan	k1gMnSc2	mimozemšťan
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
Reckless	Reckless	k1gInSc1	Reckless
Ecstasy	Ecstasa	k1gFnSc2	Ecstasa
(	(	kIx(	(
<g/>
1904	[number]	k4	1904
<g/>
)	)	kIx)	)
Abe	Abe	k1gMnSc1	Abe
Lincoln	Lincoln	k1gMnSc1	Lincoln
Grows	Growsa	k1gFnPc2	Growsa
Up	Up	k1gFnPc2	Up
Incidentals	Incidentals	k1gInSc1	Incidentals
(	(	kIx(	(
<g/>
1904	[number]	k4	1904
<g/>
)	)	kIx)	)
Plaint	Plainta	k1gFnPc2	Plainta
of	of	k?	of
a	a	k8xC	a
Rose	Rosa	k1gFnSc6	Rosa
(	(	kIx(	(
<g/>
1908	[number]	k4	1908
<g/>
)	)	kIx)	)
Chicago	Chicago	k1gNnSc1	Chicago
Poems	Poemsa	k1gFnPc2	Poemsa
(	(	kIx(	(
<g/>
1916	[number]	k4	1916
<g/>
)	)	kIx)	)
Cornhuskers	Cornhuskersa	k1gFnPc2	Cornhuskersa
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
<g/>
)	)	kIx)	)
Smoke	Smok	k1gInSc2	Smok
and	and	k?	and
Steel	Steel	k1gMnSc1	Steel
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
Slabs	Slabs	k1gInSc1	Slabs
of	of	k?	of
<g />
.	.	kIx.	.
</s>
<s>
the	the	k?	the
Sunburnt	Sunburnt	k1gMnSc1	Sunburnt
West	West	k1gMnSc1	West
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
Selected	Selected	k1gInSc1	Selected
Poems	Poems	k1gInSc1	Poems
(	(	kIx(	(
<g/>
1926	[number]	k4	1926
<g/>
)	)	kIx)	)
Good	Gooda	k1gFnPc2	Gooda
Morning	Morning	k1gInSc1	Morning
<g/>
,	,	kIx,	,
America	America	k1gFnSc1	America
(	(	kIx(	(
<g/>
1928	[number]	k4	1928
<g/>
)	)	kIx)	)
Early	earl	k1gMnPc4	earl
Moon	Moona	k1gFnPc2	Moona
(	(	kIx(	(
<g/>
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
People	People	k1gMnSc1	People
<g/>
,	,	kIx,	,
Yes	Yes	k1gMnSc1	Yes
(	(	kIx(	(
<g/>
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
Complete	Comple	k1gNnSc2	Comple
Poems	Poems	k1gInSc1	Poems
(	(	kIx(	(
<g/>
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
Selected	Selected	k1gInSc1	Selected
<g />
.	.	kIx.	.
</s>
<s>
Poems	Poems	k6eAd1	Poems
of	of	k?	of
Carl	Carl	k1gMnSc1	Carl
Sandburg	Sandburg	k1gMnSc1	Sandburg
(	(	kIx(	(
<g/>
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
Sandburg	Sandburg	k1gInSc1	Sandburg
Range	Rang	k1gInSc2	Rang
(	(	kIx(	(
<g/>
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
Harvest	Harvest	k1gInSc1	Harvest
Poems	Poemsa	k1gFnPc2	Poemsa
<g/>
,	,	kIx,	,
1910	[number]	k4	1910
<g/>
-	-	kIx~	-
<g/>
1960	[number]	k4	1960
(	(	kIx(	(
<g/>
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
Wind	Wind	k1gInSc1	Wind
Song	song	k1gInSc1	song
(	(	kIx(	(
<g/>
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
Honey	Honea	k1gFnSc2	Honea
and	and	k?	and
Salt	salto	k1gNnPc2	salto
(	(	kIx(	(
<g/>
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
Breathing	Breathing	k1gInSc1	Breathing
Tokens	Tokens	k1gInSc1	Tokens
(	(	kIx(	(
<g/>
1978	[number]	k4	1978
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Billy	Bill	k1gMnPc7	Bill
Sunday	Sundaa	k1gFnSc2	Sundaa
and	and	k?	and
other	other	k1gInSc1	other
poems	poems	k1gInSc1	poems
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
Poems	Poemsa	k1gFnPc2	Poemsa
for	forum	k1gNnPc2	forum
Children	Childrno	k1gNnPc2	Childrno
Nowhere	Nowher	k1gInSc5	Nowher
Near	Near	k1gMnSc1	Near
Old	Olda	k1gFnPc2	Olda
Enough	Enough	k1gMnSc1	Enough
to	ten	k3xDgNnSc4	ten
Vote	Vote	k1gNnSc4	Vote
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
Joseffy	Joseff	k1gInPc1	Joseff
(	(	kIx(	(
<g/>
1910	[number]	k4	1910
<g/>
)	)	kIx)	)
You	You	k1gMnSc1	You
and	and	k?	and
Your	Your	k1gMnSc1	Your
Job	Job	k1gMnSc1	Job
(	(	kIx(	(
<g/>
1910	[number]	k4	1910
<g/>
)	)	kIx)	)
Chicago	Chicago	k1gNnSc4	Chicago
Race	Rac	k1gFnSc2	Rac
Riots	Riotsa	k1gFnPc2	Riotsa
(	(	kIx(	(
<g/>
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
Clarence	Clarenec	k1gInSc2	Clarenec
<g />
.	.	kIx.	.
</s>
<s>
Darrow	Darrow	k?	Darrow
of	of	k?	of
Chicago	Chicago	k1gNnSc4	Chicago
(	(	kIx(	(
<g/>
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
Rootabaga	Rootabaga	k1gFnSc1	Rootabaga
Stories	Stories	k1gMnSc1	Stories
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
Rootabaga	Rootabaga	k1gFnSc1	Rootabaga
Pigeons	Pigeonsa	k1gFnPc2	Pigeonsa
(	(	kIx(	(
<g/>
1923	[number]	k4	1923
<g/>
)	)	kIx)	)
Potato	Potat	k2eAgNnSc1d1	Potato
Face	Face	k1gNnSc1	Face
(	(	kIx(	(
<g/>
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
Remembrance	Remembrance	k1gFnSc1	Remembrance
Rock	rock	k1gInSc1	rock
(	(	kIx(	(
<g/>
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
Lincoln	Lincoln	k1gMnSc1	Lincoln
Collector	Collector	k1gMnSc1	Collector
<g/>
:	:	kIx,	:
the	the	k?	the
story	story	k1gFnSc1	story
of	of	k?	of
the	the	k?	the
Oliver	Oliver	k1gMnSc1	Oliver
R.	R.	kA	R.
Barrett	Barrett	k1gMnSc1	Barrett
Lincoln	Lincoln	k1gMnSc1	Lincoln
collection	collection	k1gInSc1	collection
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Wedding	Wedding	k1gInSc1	Wedding
Procession	Procession	k1gInSc1	Procession
of	of	k?	of
the	the	k?	the
Rag	Rag	k1gMnSc1	Rag
Doll	Doll	k1gMnSc1	Doll
and	and	k?	and
the	the	k?	the
Broom	Broom	k1gInSc1	Broom
Handle	Handl	k1gMnSc5	Handl
and	and	k?	and
Who	Who	k1gMnSc3	Who
Was	Was	k1gMnSc3	Was
In	In	k1gMnSc3	In
It	It	k1gMnSc3	It
(	(	kIx(	(
<g/>
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
Abraham	Abraham	k1gMnSc1	Abraham
Lincoln	Lincoln	k1gMnSc1	Lincoln
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Prairie	Prairie	k1gFnSc2	Prairie
Years	Years	k1gInSc1	Years
(	(	kIx(	(
<g/>
1926	[number]	k4	1926
<g/>
)	)	kIx)	)
Abe	Abe	k1gMnSc1	Abe
Lincoln	Lincoln	k1gMnSc1	Lincoln
Grows	Growsa	k1gFnPc2	Growsa
Up	Up	k1gMnSc1	Up
(	(	kIx(	(
<g/>
1928	[number]	k4	1928
<g/>
)	)	kIx)	)
Steichen	Steichna	k1gFnPc2	Steichna
the	the	k?	the
Photographer	Photographra	k1gFnPc2	Photographra
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
Mary	Mary	k1gFnSc1	Mary
Lincoln	Lincoln	k1gMnSc1	Lincoln
<g/>
:	:	kIx,	:
Wife	Wife	k1gFnSc1	Wife
and	and	k?	and
Widow	Widow	k1gFnSc1	Widow
(	(	kIx(	(
<g/>
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
Abraham	Abraham	k1gMnSc1	Abraham
Lincoln	Lincoln	k1gMnSc1	Lincoln
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
War	War	k1gFnSc2	War
Years	Years	k1gInSc1	Years
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
Storm	Storm	k1gInSc1	Storm
over	over	k1gMnSc1	over
the	the	k?	the
Land	Land	k1gMnSc1	Land
(	(	kIx(	(
<g/>
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
Home	Hom	k1gInSc2	Hom
Front	front	k1gInSc1	front
Memo	Memo	k1gMnSc1	Memo
(	(	kIx(	(
<g/>
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
Always	Always	k1gInSc1	Always
the	the	k?	the
Young	Young	k1gInSc1	Young
Strangers	Strangers	k1gInSc1	Strangers
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
Prairie-Town	Prairie-Town	k1gMnSc1	Prairie-Town
Boy	boy	k1gMnSc1	boy
(	(	kIx(	(
<g/>
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Letters	Lettersa	k1gFnPc2	Lettersa
of	of	k?	of
Carl	Carl	k1gMnSc1	Carl
Sandburg	Sandburg	k1gMnSc1	Sandburg
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
Ever	Ever	k1gInSc1	Ever
the	the	k?	the
Winds	Winds	k1gInSc1	Winds
of	of	k?	of
Chance	Chanec	k1gInSc2	Chanec
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
American	American	k1gMnSc1	American
Songbag	Songbag	k1gMnSc1	Songbag
(	(	kIx(	(
<g/>
1927	[number]	k4	1927
<g/>
)	)	kIx)	)
Songs	Songsa	k1gFnPc2	Songsa
of	of	k?	of
America	America	k1gMnSc1	America
(	(	kIx(	(
<g/>
1927	[number]	k4	1927
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
New	New	k1gMnSc1	New
American	American	k1gMnSc1	American
Songbag	Songbag	k1gMnSc1	Songbag
(	(	kIx(	(
<g/>
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
Lid	lid	k1gInSc1	lid
<g/>
,	,	kIx,	,
ano	ano	k9	ano
lid	lid	k1gInSc1	lid
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
K.	K.	kA	K.
Marel	Marel	k1gInSc4	Marel
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Ocel	ocel	k1gFnSc1	ocel
a	a	k8xC	a
dým	dým	k1gInSc1	dým
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Družstvo	družstvo	k1gNnSc4	družstvo
Dílo	dílo	k1gNnSc1	dílo
1946	[number]	k4	1946
<g/>
.	.	kIx.	.
</s>
<s>
Dobré	dobrý	k2eAgNnSc1d1	dobré
jitro	jitro	k1gNnSc1	jitro
<g/>
,	,	kIx,	,
Ameriko	Amerika	k1gFnSc5	Amerika
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Státní	státní	k2eAgNnSc1d1	státní
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
krásné	krásný	k2eAgFnSc2d1	krásná
literatury	literatura	k1gFnSc2	literatura
<g/>
,	,	kIx,	,
hudby	hudba	k1gFnSc2	hudba
a	a	k8xC	a
umění	umění	k1gNnSc1	umění
1959	[number]	k4	1959
<g/>
.	.	kIx.	.
</s>
<s>
Pohádky	pohádka	k1gFnPc1	pohádka
z	z	k7c2	z
bramborových	bramborový	k2eAgInPc2d1	bramborový
řádků	řádek	k1gInPc2	řádek
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
SNDK	SNDK	kA	SNDK
1965	[number]	k4	1965
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Carl	Carl	k1gInSc1	Carl
Sandburg	Sandburg	k1gInSc4	Sandburg
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Osoba	osoba	k1gFnSc1	osoba
Carl	Carl	k1gInSc1	Carl
Sandburg	Sandburg	k1gInSc1	Sandburg
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Stránky	stránka	k1gFnSc2	stránka
pro	pro	k7c4	pro
"	"	kIx"	"
<g/>
Sandburg	Sandburg	k1gMnSc1	Sandburg
Studies	Studies	k1gMnSc1	Studies
<g/>
"	"	kIx"	"
Portrét	portrét	k1gInSc1	portrét
na	na	k7c4	na
Poets	Poets	k1gInSc4	Poets
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
Portrét	portrét	k1gInSc1	portrét
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Poetry	Poetr	k1gInPc4	Poetr
Foundation	Foundation	k1gInSc4	Foundation
</s>
