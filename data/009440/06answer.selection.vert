<s>
K	k	k7c3	k
popisu	popis	k1gInSc3	popis
redoxní	redoxní	k2eAgFnSc2d1	redoxní
reakce	reakce	k1gFnSc2	reakce
jsou	být	k5eAaImIp3nP	být
nezbytné	nezbytný	k2eAgInPc1d1	nezbytný
alespoň	alespoň	k9	alespoň
dva	dva	k4xCgInPc4	dva
redoxní	redoxní	k2eAgInPc4d1	redoxní
páry	pár	k1gInPc4	pár
<g/>
,	,	kIx,	,
za	za	k7c2	za
běžných	běžný	k2eAgFnPc2d1	běžná
chemických	chemický	k2eAgFnPc2d1	chemická
podmínek	podmínka	k1gFnPc2	podmínka
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
samovolně	samovolně	k6eAd1	samovolně
nevyskytují	vyskytovat	k5eNaImIp3nP	vyskytovat
volné	volný	k2eAgInPc4d1	volný
elektrony	elektron	k1gInPc4	elektron
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
mohly	moct	k5eAaImAgFnP	moct
účastnit	účastnit	k5eAaImF	účastnit
reakce	reakce	k1gFnPc1	reakce
<g/>
.	.	kIx.	.
</s>
