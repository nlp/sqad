<p>
<s>
Redoxní	Redoxní	k1gNnSc1	Redoxní
pár	pár	k4xCyI	pár
(	(	kIx(	(
<g/>
také	také	k9	také
oxidačně-redukční	oxidačněedukční	k2eAgInSc1d1	oxidačně-redukční
pár	pár	k4xCyI	pár
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
dílčí	dílčí	k2eAgFnSc1d1	dílčí
reakce	reakce	k1gFnSc1	reakce
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
redoxní	redoxní	k2eAgFnSc2d1	redoxní
reakce	reakce	k1gFnSc2	reakce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obecné	obecný	k2eAgFnSc6d1	obecná
formě	forma	k1gFnSc6	forma
platí	platit	k5eAaImIp3nS	platit
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
<s>
⇌	⇌	k?	⇌
</s>
</p>
<p>
</p>
<p>
<s>
O	o	k7c6	o
</s>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
Red	Red	k1gFnSc1	Red
<g/>
\	\	kIx~	\
\	\	kIx~	\
<g/>
rightleftharpoons	rightleftharpoons	k1gInSc1	rightleftharpoons
\	\	kIx~	\
Ox	Ox	k1gFnSc1	Ox
<g/>
\	\	kIx~	\
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathit	mathit	k1gInSc1	mathit
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
e	e	k0	e
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
}}	}}	k?	}}
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
<g/>
kde	kde	k6eAd1	kde
Red	Red	k1gFnSc1	Red
značí	značit	k5eAaImIp3nS	značit
redukující	redukující	k2eAgFnSc1d1	redukující
<g/>
,	,	kIx,	,
Ox	Ox	k1gFnSc1	Ox
oxidující	oxidující	k2eAgFnSc4d1	oxidující
formu	forma	k1gFnSc4	forma
částice	částice	k1gFnSc2	částice
(	(	kIx(	(
<g/>
molekuly	molekula	k1gFnSc2	molekula
<g/>
,	,	kIx,	,
atomu	atom	k1gInSc2	atom
nebo	nebo	k8xC	nebo
iontu	ion	k1gInSc2	ion
<g/>
)	)	kIx)	)
a	a	k8xC	a
e	e	k0	e
<g/>
−	−	k?	−
značí	značit	k5eAaImIp3nS	značit
elektron	elektron	k1gInSc1	elektron
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
čtení	čtení	k1gNnSc6	čtení
daného	daný	k2eAgInSc2d1	daný
zápisu	zápis	k1gInSc2	zápis
zleva	zleva	k6eAd1	zleva
doprava	doprava	k6eAd1	doprava
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
Red	Red	k1gFnSc4	Red
jako	jako	k8xC	jako
donor	donor	k1gInSc4	donor
elektronu	elektron	k1gInSc2	elektron
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
čtení	čtení	k1gNnSc6	čtení
zprava	zprava	k6eAd1	zprava
doleva	doleva	k6eAd1	doleva
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
Ox	Ox	k1gFnSc4	Ox
jako	jako	k8xS	jako
akceptor	akceptor	k1gInSc4	akceptor
<g/>
.	.	kIx.	.
</s>
<s>
Alternativně	alternativně	k6eAd1	alternativně
můžeme	moct	k5eAaImIp1nP	moct
hovořit	hovořit	k5eAaImF	hovořit
i	i	k9	i
o	o	k7c6	o
redukčním	redukční	k2eAgNnSc6d1	redukční
a	a	k8xC	a
oxidačním	oxidační	k2eAgNnSc6d1	oxidační
činidle	činidlo	k1gNnSc6	činidlo
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
K	k	k7c3	k
popisu	popis	k1gInSc3	popis
redoxní	redoxní	k2eAgFnSc2d1	redoxní
reakce	reakce	k1gFnSc2	reakce
jsou	být	k5eAaImIp3nP	být
nezbytné	nezbytný	k2eAgInPc1d1	nezbytný
alespoň	alespoň	k9	alespoň
dva	dva	k4xCgInPc4	dva
redoxní	redoxní	k2eAgInPc4d1	redoxní
páry	pár	k1gInPc4	pár
<g/>
,	,	kIx,	,
za	za	k7c2	za
běžných	běžný	k2eAgFnPc2d1	běžná
chemických	chemický	k2eAgFnPc2d1	chemická
podmínek	podmínka	k1gFnPc2	podmínka
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
samovolně	samovolně	k6eAd1	samovolně
nevyskytují	vyskytovat	k5eNaImIp3nP	vyskytovat
volné	volný	k2eAgInPc4d1	volný
elektrony	elektron	k1gInPc4	elektron
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
mohly	moct	k5eAaImAgFnP	moct
účastnit	účastnit	k5eAaImF	účastnit
reakce	reakce	k1gFnPc1	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
se	se	k3xPyFc4	se
v	v	k7c6	v
jakékoliv	jakýkoliv	k3yIgFnSc6	jakýkoliv
dílčí	dílčí	k2eAgFnSc6d1	dílčí
reakci	reakce	k1gFnSc6	reakce
formálně	formálně	k6eAd1	formálně
uvolní	uvolnit	k5eAaPmIp3nS	uvolnit
jeden	jeden	k4xCgInSc1	jeden
elektron	elektron	k1gInSc1	elektron
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
musí	muset	k5eAaImIp3nS	muset
proběhnout	proběhnout	k5eAaPmF	proběhnout
další	další	k2eAgFnPc4d1	další
dílčí	dílčí	k2eAgFnPc4d1	dílčí
reakce	reakce	k1gFnPc4	reakce
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
daný	daný	k2eAgInSc4d1	daný
elektron	elektron	k1gInSc4	elektron
přijme	přijmout	k5eAaPmIp3nS	přijmout
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Red	Red	k?	Red
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⇌	⇌	k?	⇌
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Ox	Ox	k?	Ox
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
redoxní	redoxnit	k5eAaImIp3nS	redoxnit
pár	pár	k4xCyI	pár
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Ox	Ox	k?	Ox
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⇌	⇌	k?	⇌
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Red	Red	k?	Red
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
.	.	kIx.	.
redoxní	redoxnit	k5eAaImIp3nS	redoxnit
pár	pár	k4xCyI	pár
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Red	Red	k?	Red
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Ox	Ox	k?	Ox
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⇌	⇌	k?	⇌
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Ox	Ox	k?	Ox
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Red	Red	k?	Red
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
redoxní	redoxní	k2eAgFnSc1d1	redoxní
reakce	reakce	k1gFnSc1	reakce
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
begin	begin	k1gMnSc1	begin
<g/>
{	{	kIx(	{
<g/>
aligned	aligned	k1gMnSc1	aligned
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
text	text	k1gInSc1	text
<g/>
{	{	kIx(	{
<g/>
Red	Red	k1gFnSc1	Red
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
&	&	k?	&
<g/>
\	\	kIx~	\
\	\	kIx~	\
<g/>
rightleftharpoons	rightleftharpoons	k6eAd1	rightleftharpoons
\	\	kIx~	\
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
text	text	k1gInSc1	text
<g/>
<g />
.	.	kIx.	.
</s>
<s>
{	{	kIx(	{
<g/>
Ox	Ox	k1gFnSc6	Ox
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
+	+	kIx~	+
<g/>
\	\	kIx~	\
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
text	text	k1gInSc1	text
<g/>
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}}	}}	k?	}}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
}	}	kIx)	}
<g/>
&	&	k?	&
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
text	text	k1gInSc1	text
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
<g />
.	.	kIx.	.
</s>
<s>
redoxní	redoxní	k1gNnSc1	redoxní
pár	pár	k4xCyI	pár
<g/>
}}	}}	k?	}}
<g/>
\\	\\	k?	\\
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
text	text	k1gInSc1	text
<g/>
{	{	kIx(	{
<g/>
Ox	Ox	k1gFnSc1	Ox
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
+	+	kIx~	+
<g/>
\	\	kIx~	\
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
text	text	k1gInSc1	text
<g/>
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}}	}}	k?	}}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
<g />
.	.	kIx.	.
</s>
<s>
}	}	kIx)	}
<g/>
&	&	k?	&
<g/>
\	\	kIx~	\
\	\	kIx~	\
<g/>
rightleftharpoons	rightleftharpoons	k6eAd1	rightleftharpoons
\	\	kIx~	\
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
text	text	k1gInSc1	text
<g/>
{	{	kIx(	{
<g/>
Red	Red	k1gFnSc1	Red
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
&	&	k?	&
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
text	text	k1gInSc1	text
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
redoxní	redoxní	k1gNnSc4	redoxní
pár	pár	k4xCyI	pár
<g/>
}}	}}	k?	}}
<g/>
\\	\\	k?	\\
<g/>
-------	-------	k?	-------
<g />
.	.	kIx.	.
</s>
<s>
<g/>
&	&	k?	&
<g/>
---------	---------	k?	---------
<g/>
\\	\\	k?	\\
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
text	text	k1gInSc1	text
<g/>
{	{	kIx(	{
<g/>
Red	Red	k1gFnSc1	Red
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
+	+	kIx~	+
<g/>
\	\	kIx~	\
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
text	text	k1gInSc1	text
<g/>
{	{	kIx(	{
<g/>
Ox	Ox	k1gFnSc1	Ox
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s>
<g/>
&	&	k?	&
<g/>
\	\	kIx~	\
\	\	kIx~	\
<g/>
rightleftharpoons	rightleftharpoons	k6eAd1	rightleftharpoons
\	\	kIx~	\
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
text	text	k1gInSc1	text
<g/>
{	{	kIx(	{
<g/>
Ox	Ox	k1gFnSc1	Ox
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
+	+	kIx~	+
<g/>
\	\	kIx~	\
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
text	text	k1gInSc1	text
<g/>
{	{	kIx(	{
<g/>
Red	Red	k1gFnSc1	Red
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
&	&	k?	&
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
text	text	k1gInSc1	text
<g/>
{	{	kIx(	{
<g/>
redoxní	redoxní	k2eAgFnSc1d1	redoxní
reakce	reakce	k1gFnSc1	reakce
<g/>
}}	}}	k?	}}
<g/>
\\\	\\\	k?	\\\
<g/>
end	end	k?	end
<g/>
{	{	kIx(	{
<g/>
aligned	aligned	k1gMnSc1	aligned
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Příklady	příklad	k1gInPc1	příklad
==	==	k?	==
</s>
</p>
<p>
<s>
Jako	jako	k8xC	jako
příklad	příklad	k1gInSc1	příklad
můžeme	moct	k5eAaImIp1nP	moct
provést	provést	k5eAaPmF	provést
jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
rozpuštění	rozpuštění	k1gNnSc1	rozpuštění
měděné	měděný	k2eAgFnSc2d1	měděná
tyče	tyč	k1gFnSc2	tyč
ve	v	k7c6	v
vodném	vodný	k2eAgInSc6d1	vodný
roztoku	roztok	k1gInSc6	roztok
obsahujícím	obsahující	k2eAgNnSc7d1	obsahující
ionty	ion	k1gInPc7	ion
stříbra	stříbro	k1gNnSc2	stříbro
<g/>
.	.	kIx.	.
</s>
<s>
Měď	měď	k1gFnSc1	měď
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
oxiduje	oxidovat	k5eAaBmIp3nS	oxidovat
zatímco	zatímco	k8xS	zatímco
stříbro	stříbro	k1gNnSc1	stříbro
se	se	k3xPyFc4	se
během	během	k7c2	během
reakce	reakce	k1gFnSc2	reakce
redukuje	redukovat	k5eAaBmIp3nS	redukovat
na	na	k7c4	na
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
atomy	atom	k1gInPc4	atom
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Cu	Cu	k?	Cu
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⟶	⟶	k?	⟶
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Cu	Cu	k?	Cu
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
redoxní	redoxeň	k1gFnPc2	redoxeň
pár	pár	k4xCyI	pár
<g/>
,	,	kIx,	,
následuje	následovat	k5eAaImIp3nS	následovat
oxidace	oxidace	k1gFnSc1	oxidace
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Ag	Ag	k?	Ag
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⟶	⟶	k?	⟶
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Ag	Ag	k?	Ag
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
.	.	kIx.	.
redoxní	redoxeň	k1gFnPc2	redoxeň
pár	pár	k4xCyI	pár
<g/>
,	,	kIx,	,
následuje	následovat	k5eAaImIp3nS	následovat
redukce	redukce	k1gFnSc1	redukce
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Cu	Cu	k?	Cu
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Ag	Ag	k?	Ag
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⟶	⟶	k?	⟶
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Cu	Cu	k?	Cu
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Ag	Ag	k?	Ag
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
redoxní	redoxní	k2eAgFnSc1d1	redoxní
reakce	reakce	k1gFnSc1	reakce
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
begin	begin	k1gMnSc1	begin
<g/>
{	{	kIx(	{
<g/>
aligned	aligned	k1gMnSc1	aligned
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
text	text	k1gInSc1	text
<g/>
{	{	kIx(	{
<g/>
Cu	Cu	k1gFnSc1	Cu
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
&	&	k?	&
<g/>
\	\	kIx~	\
\	\	kIx~	\
<g/>
longrightarrow	longrightarrow	k?	longrightarrow
\	\	kIx~	\
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
text	text	k1gInSc1	text
<g/>
{	{	kIx(	{
<g/>
Cu	Cu	k1gFnSc1	Cu
<g/>
}}	}}	k?	}}
<g/>
^	^	kIx~	^
<g/>
<g />
.	.	kIx.	.
</s>
<s>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
+	+	kIx~	+
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
2	[number]	k4	2
<g/>
\	\	kIx~	\
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
text	text	k1gInSc1	text
<g/>
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}}	}}	k?	}}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
}	}	kIx)	}
<g/>
&	&	k?	&
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
text	text	k1gInSc1	text
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
redoxní	redoxní	k1gNnSc2	redoxní
pár	pár	k4xCyI	pár
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
následuje	následovat	k5eAaImIp3nS	následovat
oxidace	oxidace	k1gFnSc1	oxidace
<g/>
}}	}}	k?	}}
<g/>
\\	\\	k?	\\
<g/>
2	[number]	k4	2
<g/>
\	\	kIx~	\
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
text	text	k1gInSc1	text
<g/>
{	{	kIx(	{
<g/>
Ag	Ag	k1gFnSc1	Ag
<g/>
}}	}}	k?	}}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
+	+	kIx~	+
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
+	+	kIx~	+
<g/>
\	\	kIx~	\
2	[number]	k4	2
<g/>
\	\	kIx~	\
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
text	text	k1gInSc1	text
<g/>
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}}	}}	k?	}}
<g/>
<g />
.	.	kIx.	.
</s>
<s>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
}	}	kIx)	}
<g/>
&	&	k?	&
<g/>
\	\	kIx~	\
\	\	kIx~	\
<g/>
longrightarrow	longrightarrow	k?	longrightarrow
\	\	kIx~	\
2	[number]	k4	2
<g/>
\	\	kIx~	\
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
text	text	k1gInSc1	text
<g/>
{	{	kIx(	{
<g/>
Ag	Ag	k1gFnSc1	Ag
<g/>
}}	}}	k?	}}
<g/>
&	&	k?	&
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
text	text	k1gInSc1	text
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
redoxní	redoxeň	k1gFnPc2	redoxeň
pár	pár	k4xCyI	pár
<g/>
,	,	kIx,	,
následuje	následovat	k5eAaImIp3nS	následovat
redukce	redukce	k1gFnSc1	redukce
<g/>
<g />
.	.	kIx.	.
</s>
<s>
}}	}}	k?	}}
<g/>
\\	\\	k?	\\
<g/>
-------	-------	k?	-------
<g/>
&	&	k?	&
<g/>
---------	---------	k?	---------
<g/>
\\	\\	k?	\\
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
text	text	k1gInSc1	text
<g/>
{	{	kIx(	{
<g/>
Cu	Cu	k1gFnSc1	Cu
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
+2	+2	k4	+2
<g/>
\	\	kIx~	\
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
text	text	k1gInSc1	text
<g/>
{	{	kIx(	{
<g/>
Ag	Ag	k1gFnSc1	Ag
<g/>
}}	}}	k?	}}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
+	+	kIx~	+
<g/>
}	}	kIx)	}
<g/>
&	&	k?	&
<g/>
<g />
.	.	kIx.	.
</s>
<s>
\	\	kIx~	\
<g/>
longrightarrow	longrightarrow	k?	longrightarrow
\	\	kIx~	\
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
text	text	k1gInSc1	text
<g/>
{	{	kIx(	{
<g/>
Cu	Cu	k1gFnSc1	Cu
<g/>
}}	}}	k?	}}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
+	+	kIx~	+
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
2	[number]	k4	2
<g/>
\	\	kIx~	\
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
text	text	k1gInSc1	text
<g/>
{	{	kIx(	{
<g/>
Ag	Ag	k1gFnSc1	Ag
<g/>
}}	}}	k?	}}
<g/>
&	&	k?	&
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
text	text	k1gInSc1	text
<g/>
{	{	kIx(	{
<g/>
redoxní	redoxní	k2eAgFnSc1d1	redoxní
reakce	reakce	k1gFnSc1	reakce
<g/>
}}	}}	k?	}}
<g/>
\\\	\\\	k?	\\\
<g/>
end	end	k?	end
<g/>
{	{	kIx(	{
<g/>
aligned	aligned	k1gMnSc1	aligned
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
<s>
Dílčí	dílčí	k2eAgFnSc1d1	dílčí
reakce	reakce	k1gFnSc1	reakce
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
také	také	k9	také
komplexnější	komplexní	k2eAgFnSc4d2	komplexnější
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
odhadnout	odhadnout	k5eAaPmF	odhadnout
vhodná	vhodný	k2eAgNnPc4d1	vhodné
oxidační	oxidační	k2eAgNnPc4d1	oxidační
čísla	číslo	k1gNnPc4	číslo
příslušných	příslušný	k2eAgInPc2d1	příslušný
atomů	atom	k1gInPc2	atom
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kyselém	kyselý	k2eAgNnSc6d1	kyselé
vodném	vodné	k1gNnSc6	vodné
roztoku	roztok	k1gInSc2	roztok
reaguje	reagovat	k5eAaBmIp3nS	reagovat
dusitan	dusitan	k1gInSc1	dusitan
sodný	sodný	k2eAgInSc1d1	sodný
na	na	k7c4	na
dusičnan	dusičnan	k1gInSc4	dusičnan
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
je	být	k5eAaImIp3nS	být
přidáván	přidávat	k5eAaImNgInS	přidávat
manganistan	manganistan	k1gInSc1	manganistan
draselný	draselný	k2eAgInSc1d1	draselný
<g/>
.	.	kIx.	.
</s>
<s>
Kyselina	kyselina	k1gFnSc1	kyselina
manganistá	manganistý	k2eAgFnSc1d1	manganistý
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
redukuje	redukovat	k5eAaBmIp3nS	redukovat
na	na	k7c6	na
Mn	Mn	k1gFnSc6	Mn
<g/>
2	[number]	k4	2
<g/>
+	+	kIx~	+
iont	iont	k1gInSc1	iont
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dílčí	dílčí	k2eAgFnSc6d1	dílčí
reakci	reakce	k1gFnSc6	reakce
manganu	mangan	k1gInSc2	mangan
jsou	být	k5eAaImIp3nP	být
jeho	jeho	k3xOp3gInPc4	jeho
oxidační	oxidační	k2eAgInPc4d1	oxidační
stupně	stupeň	k1gInPc4	stupeň
rozhodující	rozhodující	k2eAgInPc4d1	rozhodující
<g/>
,	,	kIx,	,
oproti	oproti	k7c3	oproti
tomu	ten	k3xDgMnSc3	ten
oxidační	oxidační	k2eAgInPc1d1	oxidační
stupně	stupeň	k1gInPc1	stupeň
atomů	atom	k1gInPc2	atom
kyslíku	kyslík	k1gInSc2	kyslík
a	a	k8xC	a
vodíku	vodík	k1gInSc2	vodík
zůstanou	zůstat	k5eAaPmIp3nP	zůstat
nezměněny	změnit	k5eNaPmNgFnP	změnit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
M	M	kA	M
</s>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
7	[number]	k4	7
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
O	o	k7c6	o
</s>
</p>
<p>
</p>
<p>
<s>
4	[number]	k4	4
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
<s>
8	[number]	k4	8
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
</p>
<p>
<s>
3	[number]	k4	3
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
O	o	k7c6	o
</s>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
5	[number]	k4	5
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⟶	⟶	k?	⟶
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
M	M	kA	M
</s>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
12	[number]	k4	12
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
O	o	k7c6	o
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc4	mathrm
{{	{{	k?	{{
<g/>
\	\	kIx~	\
<g/>
overset	overset	k5eAaBmF	overset
{	{	kIx(	{
<g/>
+	+	kIx~	+
<g/>
7	[number]	k4	7
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
Mn	Mn	k1gMnSc1	Mn
<g/>
}}	}}	k?	}}
<g/>
O_	O_	k1gMnSc1	O_
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
+	+	kIx~	+
<g/>
\	\	kIx~	\
8	[number]	k4	8
<g/>
\	\	kIx~	\
H_	H_	k1gFnSc1	H_
<g />
.	.	kIx.	.
</s>
<s>
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
O	o	k7c4	o
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
+	+	kIx~	+
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
5	[number]	k4	5
<g/>
\	\	kIx~	\
e	e	k0	e
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
longrightarrow	longrightarrow	k?	longrightarrow
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
overset	overset	k5eAaPmF	overset
{	{	kIx(	{
<g/>
+	+	kIx~	+
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
Mn	Mn	k1gFnSc1	Mn
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
+	+	kIx~	+
<g/>
}}}	}}}	k?	}}}
<g/>
+	+	kIx~	+
<g/>
12	[number]	k4	12
<g/>
\	\	kIx~	\
H_	H_	k1gFnSc2	H_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
O	o	k7c4	o
<g/>
}	}	kIx)	}
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Redox-paar	Redoxaara	k1gFnPc2	Redox-paara
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Redoxní	Redoxní	k2eAgFnSc1d1	Redoxní
reakce	reakce	k1gFnSc1	reakce
</s>
</p>
