<p>
<s>
Velká	velký	k2eAgFnSc1d1	velká
Kumánie	Kumánie	k1gFnSc1	Kumánie
(	(	kIx(	(
<g/>
maďarsky	maďarsky	k6eAd1	maďarsky
Nagykunság	Nagykunság	k1gMnSc1	Nagykunság
[	[	kIx(	[
<g/>
naďkunšág	naďkunšág	k1gMnSc1	naďkunšág
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
historický	historický	k2eAgInSc1d1	historický
a	a	k8xC	a
zeměpisný	zeměpisný	k2eAgInSc1d1	zeměpisný
region	region	k1gInSc1	region
v	v	k7c6	v
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
Jasovsko-velkokumánsko-solnocké	Jasovskoelkokumánskoolnocký	k2eAgFnSc6d1	Jasovsko-velkokumánsko-solnocký
župě	župa	k1gFnSc6	župa
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
městy	město	k1gNnPc7	město
Szolnok	Szolnok	k1gInSc1	Szolnok
a	a	k8xC	a
Debrecín	Debrecín	k1gInSc1	Debrecín
<g/>
.	.	kIx.	.
</s>
<s>
Pojmenování	pojmenování	k1gNnSc1	pojmenování
regionu	region	k1gInSc2	region
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
kočovnického	kočovnický	k2eAgInSc2d1	kočovnický
kmene	kmen	k1gInSc2	kmen
Kumánů	Kumán	k1gInPc2	Kumán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Kumánie	Kumánie	k1gFnSc1	Kumánie
</s>
</p>
<p>
<s>
Malá	malý	k2eAgFnSc1d1	malá
Kumánie	Kumánie	k1gFnSc1	Kumánie
</s>
</p>
