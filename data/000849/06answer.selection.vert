<s>
Jednou	jeden	k4xCgFnSc7
ze	z	k7c2
základních	základní	k2eAgFnPc2d1
vlastností	vlastnost	k1gFnPc2
trojúhelníku	trojúhelník	k1gInSc2
v	v	k7c6
"	"	kIx"
<g/>
obyčejné	obyčejný	k2eAgFnSc6d1
<g/>
"	"	kIx"
euklidovské	euklidovský	k2eAgFnSc6d1
rovině	rovina	k1gFnSc6
je	být	k5eAaImIp3nS
skutečnost	skutečnost	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
součet	součet	k1gInSc1
velikostí	velikost	k1gFnPc2
jeho	jeho	k3xOp3gInPc2
vnitřních	vnitřní	k2eAgInPc2d1
úhlů	úhel	k1gInPc2
je	být	k5eAaImIp3nS
roven	roven	k2eAgInSc1d1
180	[number]	k4
<g/>
°	°	k?
(	(	kIx(
<g/>
π	π	k?
v	v	k7c6
obloukové	obloukový	k2eAgFnSc6d1
míře	míra	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>