<s>
Homér	Homér	k1gMnSc1	Homér
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
Ὅ	Ὅ	k?	Ὅ
Homéros	Homérosa	k1gFnPc2	Homérosa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jméno	jméno	k1gNnSc4	jméno
nejstaršího	starý	k2eAgMnSc2d3	nejstarší
známého	známý	k2eAgMnSc2d1	známý
řeckého	řecký	k2eAgMnSc2d1	řecký
básníka	básník	k1gMnSc2	básník
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgNnSc3	jenž
se	se	k3xPyFc4	se
připisuje	připisovat	k5eAaImIp3nS	připisovat
sepsání	sepsání	k1gNnSc2	sepsání
epických	epický	k2eAgFnPc2d1	epická
básní	báseň	k1gFnPc2	báseň
Ilias	Ilias	k1gFnSc1	Ilias
a	a	k8xC	a
Odysseia	Odysseia	k1gFnSc1	Odysseia
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
jméno	jméno	k1gNnSc1	jméno
se	se	k3xPyFc4	se
vykládalo	vykládat	k5eAaImAgNnS	vykládat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
Rukojmí	rukojmí	k1gMnSc1	rukojmí
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
častěji	často	k6eAd2	často
podle	podle	k7c2	podle
jeho	jeho	k3xOp3gFnSc2	jeho
domnělé	domnělý	k2eAgFnSc2d1	domnělá
slepoty	slepota	k1gFnSc2	slepota
ze	z	k7c2	z
spojení	spojení	k1gNnSc2	spojení
ὁ	ὁ	k?	ὁ
μ	μ	k?	μ
ὁ	ὁ	k?	ὁ
(	(	kIx(	(
<g/>
ho	on	k3xPp3gMnSc4	on
mē	mē	k?	mē
horō	horō	k?	horō
<g/>
)	)	kIx)	)
-	-	kIx~	-
"	"	kIx"	"
<g/>
nevidící	vidící	k2eNgMnSc1d1	nevidící
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
Slepý	slepý	k2eAgMnSc1d1	slepý
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Krom	krom	k7c2	krom
obou	dva	k4xCgInPc2	dva
eposů	epos	k1gInPc2	epos
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
také	také	k9	také
připisují	připisovat	k5eAaImIp3nP	připisovat
tzv.	tzv.	kA	tzv.
Homérské	homérský	k2eAgInPc1d1	homérský
hymny	hymnus	k1gInPc1	hymnus
<g/>
,	,	kIx,	,
směšnohrdinský	směšnohrdinský	k2eAgInSc1d1	směšnohrdinský
epos	epos	k1gInSc1	epos
Batrachomyomachia	Batrachomyomachia	k1gFnSc1	Batrachomyomachia
(	(	kIx(	(
<g/>
Válka	válka	k1gFnSc1	válka
žab	žába	k1gFnPc2	žába
a	a	k8xC	a
myší	myš	k1gFnPc2	myš
<g/>
)	)	kIx)	)
parodující	parodující	k2eAgFnSc1d1	parodující
Ilias	Ilias	k1gFnSc1	Ilias
<g/>
,	,	kIx,	,
lyrické	lyrický	k2eAgFnPc1d1	lyrická
básně	báseň	k1gFnPc1	báseň
na	na	k7c4	na
oslavu	oslava	k1gFnSc4	oslava
antických	antický	k2eAgNnPc2d1	antické
božstev	božstvo	k1gNnPc2	božstvo
<g/>
,	,	kIx,	,
epigramy	epigram	k1gInPc4	epigram
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
zlomky	zlomek	k1gInPc4	zlomek
<g/>
.	.	kIx.	.
</s>
<s>
Homérova	Homérův	k2eAgFnSc1d1	Homérova
podoba	podoba	k1gFnSc1	podoba
není	být	k5eNaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
sochy	socha	k1gFnPc1	socha
nevycházejí	vycházet	k5eNaImIp3nP	vycházet
ze	z	k7c2	z
skutečnosti	skutečnost	k1gFnSc2	skutečnost
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
o	o	k7c6	o
životě	život	k1gInSc6	život
této	tento	k3xDgFnSc2	tento
postavy	postava	k1gFnSc2	postava
nebylo	být	k5eNaImAgNnS	být
známo	znám	k2eAgNnSc1d1	známo
nic	nic	k3yNnSc1	nic
určitého	určitý	k2eAgNnSc2d1	určité
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
Homérovo	Homérův	k2eAgNnSc1d1	Homérovo
rodiště	rodiště	k1gNnSc1	rodiště
se	se	k3xPyFc4	se
uvádělo	uvádět	k5eAaImAgNnS	uvádět
mnoho	mnoho	k4c1	mnoho
míst	místo	k1gNnPc2	místo
(	(	kIx(	(
<g/>
Athény	Athéna	k1gFnPc1	Athéna
<g/>
,	,	kIx,	,
Pylos	Pylos	k1gMnSc1	Pylos
<g/>
,	,	kIx,	,
Kolofón	Kolofón	k1gMnSc1	Kolofón
<g/>
,	,	kIx,	,
Argos	Argos	k1gMnSc1	Argos
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
město	město	k1gNnSc4	město
Smyrna	Smyrna	k1gFnSc1	Smyrna
na	na	k7c6	na
maloasijském	maloasijský	k2eAgNnSc6d1	maloasijské
pobřeží	pobřeží	k1gNnSc6	pobřeží
nebo	nebo	k8xC	nebo
ostrov	ostrov	k1gInSc1	ostrov
Chios	Chiosa	k1gFnPc2	Chiosa
<g/>
.	.	kIx.	.
</s>
<s>
Legendu	legenda	k1gFnSc4	legenda
o	o	k7c6	o
Homérově	Homérův	k2eAgInSc6d1	Homérův
životě	život	k1gInSc6	život
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
v	v	k7c6	v
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Hérodotos	Hérodotos	k1gMnSc1	Hérodotos
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
prý	prý	k9	prý
jako	jako	k9	jako
nemanželské	manželský	k2eNgNnSc4d1	nemanželské
dítě	dítě	k1gNnSc4	dítě
u	u	k7c2	u
řeky	řeka	k1gFnSc2	řeka
Meles	Melesa	k1gFnPc2	Melesa
nedaleko	nedaleko	k7c2	nedaleko
Smyrny	Smyrna	k1gFnSc2	Smyrna
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc1	jeho
původní	původní	k2eAgNnSc1d1	původní
jméno	jméno	k1gNnSc1	jméno
bylo	být	k5eAaImAgNnS	být
Melésigenés	Melésigenés	k1gInSc4	Melésigenés
(	(	kIx(	(
<g/>
narozený	narozený	k2eAgMnSc1d1	narozený
u	u	k7c2	u
Melesu	Meles	k1gInSc2	Meles
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
údajně	údajně	k6eAd1	údajně
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Íos	Íos	k1gFnSc2	Íos
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
pramenů	pramen	k1gInPc2	pramen
se	se	k3xPyFc4	se
shoduje	shodovat	k5eAaImIp3nS	shodovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
jmenovala	jmenovat	k5eAaBmAgFnS	jmenovat
Kreitheis	Kreitheis	k1gFnSc4	Kreitheis
<g/>
,	,	kIx,	,
údaje	údaj	k1gInPc1	údaj
o	o	k7c6	o
otci	otec	k1gMnSc6	otec
se	se	k3xPyFc4	se
rozcházejí	rozcházet	k5eAaImIp3nP	rozcházet
<g/>
.	.	kIx.	.
</s>
<s>
Všechna	všechen	k3xTgNnPc1	všechen
starověká	starověký	k2eAgNnPc1d1	starověké
vyobrazení	vyobrazení	k1gNnPc1	vyobrazení
jsou	být	k5eAaImIp3nP	být
idealistická	idealistický	k2eAgNnPc1d1	idealistické
a	a	k8xC	a
neopírají	opírat	k5eNaImIp3nP	opírat
se	se	k3xPyFc4	se
o	o	k7c4	o
skutečnost	skutečnost	k1gFnSc4	skutečnost
<g/>
,	,	kIx,	,
na	na	k7c6	na
základě	základ	k1gInSc6	základ
domnělého	domnělý	k2eAgInSc2d1	domnělý
významu	význam	k1gInSc2	význam
jeho	jeho	k3xOp3gNnPc1	jeho
jména	jméno	k1gNnPc1	jméno
jej	on	k3xPp3gNnSc4	on
představují	představovat	k5eAaImIp3nP	představovat
většinou	většinou	k6eAd1	většinou
jako	jako	k9	jako
slepého	slepý	k2eAgMnSc2d1	slepý
starce	stařec	k1gMnSc2	stařec
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
vůbec	vůbec	k9	vůbec
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
historickou	historický	k2eAgFnSc4d1	historická
osobu	osoba	k1gFnSc4	osoba
<g/>
,	,	kIx,	,
potom	potom	k6eAd1	potom
působil	působit	k5eAaImAgMnS	působit
mezi	mezi	k7c7	mezi
léty	léto	k1gNnPc7	léto
1200	[number]	k4	1200
až	až	k8xS	až
700	[number]	k4	700
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
uvádí	uvádět	k5eAaImIp3nS	uvádět
počátek	počátek	k1gInSc4	počátek
osmého	osmý	k4xOgNnSc2	osmý
století	století	k1gNnSc2	století
jako	jako	k8xC	jako
nejzazší	zadní	k2eAgFnSc1d3	nejzazší
hranice	hranice	k1gFnSc1	hranice
jeho	jeho	k3xOp3gInSc2	jeho
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Dřívější	dřívější	k2eAgNnSc1d1	dřívější
datum	datum	k1gNnSc1	datum
udává	udávat	k5eAaImIp3nS	udávat
jen	jen	k9	jen
několik	několik	k4yIc1	několik
badatelů	badatel	k1gMnPc2	badatel
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
množství	množství	k1gNnSc4	množství
údajů	údaj	k1gInPc2	údaj
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
často	často	k6eAd1	často
si	se	k3xPyFc3	se
protiřečících	protiřečící	k2eAgMnPc2d1	protiřečící
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
osoba	osoba	k1gFnSc1	osoba
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
skutečně	skutečně	k6eAd1	skutečně
žila	žít	k5eAaImAgFnS	žít
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
obvyklé	obvyklý	k2eAgFnSc3d1	obvyklá
představě	představa	k1gFnSc3	představa
Homéra	Homér	k1gMnSc2	Homér
jako	jako	k8xS	jako
slepého	slepý	k2eAgMnSc2d1	slepý
a	a	k8xC	a
chudého	chudý	k2eAgMnSc2d1	chudý
putujícího	putující	k2eAgMnSc2d1	putující
pěvce	pěvec	k1gMnSc2	pěvec
mluví	mluvit	k5eAaImIp3nS	mluvit
důkladná	důkladný	k2eAgFnSc1d1	důkladná
znalost	znalost	k1gFnSc1	znalost
horní	horní	k2eAgFnSc2d1	horní
aristokratické	aristokratický	k2eAgFnSc2d1	aristokratická
vrstvy	vrstva	k1gFnSc2	vrstva
jeho	jeho	k3xOp3gNnPc2	jeho
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
takový	takový	k3xDgMnSc1	takový
putující	putující	k2eAgMnSc1d1	putující
básník	básník	k1gMnSc1	básník
nemohl	moct	k5eNaImAgMnS	moct
mít	mít	k5eAaImF	mít
<g/>
.	.	kIx.	.
</s>
<s>
Homér	Homér	k1gMnSc1	Homér
je	být	k5eAaImIp3nS	být
obecně	obecně	k6eAd1	obecně
známý	známý	k2eAgInSc1d1	známý
především	především	k9	především
jako	jako	k8xC	jako
autor	autor	k1gMnSc1	autor
dvou	dva	k4xCgInPc2	dva
eposů	epos	k1gInPc2	epos
stojících	stojící	k2eAgInPc2d1	stojící
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
řecké	řecký	k2eAgFnSc2d1	řecká
a	a	k8xC	a
evropské	evropský	k2eAgFnSc2d1	Evropská
literatury	literatura	k1gFnSc2	literatura
<g/>
,	,	kIx,	,
Iliady	Iliada	k1gFnSc2	Iliada
a	a	k8xC	a
Odysseje	Odyssea	k1gFnSc2	Odyssea
<g/>
.	.	kIx.	.
</s>
<s>
Skutečné	skutečný	k2eAgInPc1d1	skutečný
autorství	autorství	k1gNnSc2	autorství
těchto	tento	k3xDgNnPc2	tento
děl	dělo	k1gNnPc2	dělo
je	být	k5eAaImIp3nS	být
však	však	k9	však
sporné	sporný	k2eAgNnSc1d1	sporné
<g/>
.	.	kIx.	.
</s>
<s>
Jistý	jistý	k2eAgMnSc1d1	jistý
se	se	k3xPyFc4	se
zdá	zdát	k5eAaPmIp3nS	zdát
původ	původ	k1gInSc1	původ
eposů	epos	k1gInPc2	epos
v	v	k7c6	v
řecké	řecký	k2eAgFnSc6d1	řecká
Malé	Malé	k2eAgFnSc6d1	Malé
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Potvrzuje	potvrzovat	k5eAaImIp3nS	potvrzovat
to	ten	k3xDgNnSc1	ten
jazyková	jazykový	k2eAgFnSc1d1	jazyková
analýza	analýza	k1gFnSc1	analýza
díla	dílo	k1gNnSc2	dílo
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
níž	jenž	k3xRgFnSc2	jenž
jsou	být	k5eAaImIp3nP	být
obě	dva	k4xCgFnPc1	dva
básně	báseň	k1gFnPc1	báseň
sepsány	sepsán	k2eAgFnPc1d1	sepsána
v	v	k7c6	v
iónském	iónský	k2eAgInSc6d1	iónský
dialektu	dialekt	k1gInSc6	dialekt
řečtiny	řečtina	k1gFnSc2	řečtina
<g/>
.	.	kIx.	.
</s>
<s>
Ilias	Ilias	k1gFnSc1	Ilias
a	a	k8xC	a
Odyssea	Odyssea	k1gFnSc1	Odyssea
jsou	být	k5eAaImIp3nP	být
první	první	k4xOgInPc4	první
písemné	písemný	k2eAgInPc4d1	písemný
doklady	doklad	k1gInPc4	doklad
řeckých	řecký	k2eAgFnPc2d1	řecká
dějin	dějiny	k1gFnPc2	dějiny
a	a	k8xC	a
jimi	on	k3xPp3gMnPc7	on
podle	podle	k7c2	podle
klasického	klasický	k2eAgNnSc2d1	klasické
mínění	mínění	k1gNnSc2	mínění
začínají	začínat	k5eAaImIp3nP	začínat
evropské	evropský	k2eAgFnPc1d1	Evropská
kulturní	kulturní	k2eAgFnPc1d1	kulturní
a	a	k8xC	a
duchovní	duchovní	k2eAgFnPc1d1	duchovní
dějiny	dějiny	k1gFnPc1	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
Základním	základní	k2eAgInSc7d1	základní
jazykem	jazyk	k1gInSc7	jazyk
je	být	k5eAaImIp3nS	být
iónština	iónština	k1gFnSc1	iónština
archaické	archaický	k2eAgFnSc2d1	archaická
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
místy	místy	k6eAd1	místy
prostoupená	prostoupený	k2eAgFnSc1d1	prostoupená
příklady	příklad	k1gInPc4	příklad
v	v	k7c6	v
aiolském	aiolský	k2eAgInSc6d1	aiolský
dialektu	dialekt	k1gInSc6	dialekt
a	a	k8xC	a
zjevně	zjevně	k6eAd1	zjevně
podáními	podání	k1gNnPc7	podání
staršího	starší	k1gMnSc2	starší
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgInPc1d1	mnohý
verše	verš	k1gInPc1	verš
se	s	k7c7	s
vlivem	vliv	k1gInSc7	vliv
ústního	ústní	k2eAgNnSc2d1	ústní
předávání	předávání	k1gNnSc2	předávání
básní	báseň	k1gFnPc2	báseň
opakovaně	opakovaně	k6eAd1	opakovaně
vynořují	vynořovat	k5eAaImIp3nP	vynořovat
jako	jako	k9	jako
výplně	výplň	k1gFnPc1	výplň
mezer	mezera	k1gFnPc2	mezera
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
okřídlená	okřídlený	k2eAgNnPc1d1	okřídlené
slova	slovo	k1gNnPc1	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
helénizmu	helénizmus	k1gInSc2	helénizmus
existovalo	existovat	k5eAaImAgNnS	existovat
několik	několik	k4yIc1	několik
redakcí	redakce	k1gFnPc2	redakce
eposů	epos	k1gInPc2	epos
<g/>
,	,	kIx,	,
první	první	k4xOgFnPc4	první
redakce	redakce	k1gFnPc4	redakce
(	(	kIx(	(
<g/>
první	první	k4xOgNnSc4	první
písemné	písemný	k2eAgNnSc4d1	písemné
zaznamenání	zaznamenání	k1gNnSc4	zaznamenání
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
připisuje	připisovat	k5eAaImIp3nS	připisovat
podnětu	podnět	k1gInSc2	podnět
athénského	athénský	k2eAgMnSc2d1	athénský
tyrana	tyran	k1gMnSc2	tyran
Peisistrata	Peisistrat	k1gMnSc2	Peisistrat
či	či	k8xC	či
jeho	on	k3xPp3gMnSc2	on
syna	syn	k1gMnSc2	syn
Hipparcha	Hipparch	k1gMnSc2	Hipparch
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
užívané	užívaný	k2eAgNnSc1d1	užívané
znění	znění	k1gNnSc1	znění
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
Aristarcha	aristarch	k1gMnSc2	aristarch
ze	z	k7c2	z
Samothráky	Samothrák	k1gMnPc7	Samothrák
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
básně	báseň	k1gFnPc4	báseň
rozdělil	rozdělit	k5eAaPmAgMnS	rozdělit
do	do	k7c2	do
rovněž	rovněž	k9	rovněž
po	po	k7c4	po
dnešek	dnešek	k1gInSc4	dnešek
užívaných	užívaný	k2eAgInPc2d1	užívaný
zpěvů	zpěv	k1gInPc2	zpěv
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
časovému	časový	k2eAgNnSc3d1	časové
zařazení	zařazení	k1gNnSc3	zařazení
homérských	homérský	k2eAgInPc2d1	homérský
eposů	epos	k1gInPc2	epos
se	se	k3xPyFc4	se
použila	použít	k5eAaPmAgNnP	použít
různá	různý	k2eAgNnPc1d1	různé
srovnání	srovnání	k1gNnPc1	srovnání
<g/>
.	.	kIx.	.
</s>
<s>
Posloužila	posloužit	k5eAaPmAgFnS	posloužit
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
epika	epika	k1gFnSc1	epika
Hésioda	Hésiod	k1gMnSc2	Hésiod
z	z	k7c2	z
Asker	Askra	k1gFnPc2	Askra
<g/>
,	,	kIx,	,
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
v	v	k7c6	v
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Nabízí	nabízet	k5eAaImIp3nS	nabízet
se	se	k3xPyFc4	se
také	také	k9	také
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
Nestorův	Nestorův	k2eAgInSc4d1	Nestorův
pohár	pohár	k1gInSc4	pohár
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
zlatá	zlatý	k2eAgFnSc1d1	zlatá
zdobená	zdobený	k2eAgFnSc1d1	zdobená
nádoba	nádoba	k1gFnSc1	nádoba
<g/>
,	,	kIx,	,
uchovávaná	uchovávaný	k2eAgFnSc1d1	uchovávaná
dnes	dnes	k6eAd1	dnes
v	v	k7c6	v
Národním	národní	k2eAgNnSc6d1	národní
archeologickém	archeologický	k2eAgNnSc6d1	Archeologické
muzeu	muzeum	k1gNnSc6	muzeum
v	v	k7c6	v
Athénách	Athéna	k1gFnPc6	Athéna
<g/>
,	,	kIx,	,
pocházející	pocházející	k2eAgFnPc1d1	pocházející
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
kolem	kolem	k7c2	kolem
720	[number]	k4	720
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
výzdoba	výzdoba	k1gFnSc1	výzdoba
souhlasí	souhlasit	k5eAaImIp3nS	souhlasit
s	s	k7c7	s
některými	některý	k3yIgFnPc7	některý
scénami	scéna	k1gFnPc7	scéna
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	on	k3xPp3gNnPc4	on
líčí	líčit	k5eAaImIp3nS	líčit
Homér	Homér	k1gMnSc1	Homér
<g/>
.	.	kIx.	.
</s>
<s>
Důležité	důležitý	k2eAgNnSc1d1	důležité
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
historické	historický	k2eAgNnSc4d1	historické
klima	klima	k1gNnSc4	klima
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
8	[number]	k4	8
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
příznivé	příznivý	k2eAgFnPc1d1	příznivá
pro	pro	k7c4	pro
vznik	vznik	k1gInSc4	vznik
eposů	epos	k1gInPc2	epos
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
od	od	k7c2	od
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
již	již	k6eAd1	již
v	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
neexistovala	existovat	k5eNaImAgFnS	existovat
nepopiratelná	popiratelný	k2eNgFnSc1d1	nepopiratelná
šlechtická	šlechtický	k2eAgFnSc1d1	šlechtická
kultura	kultura	k1gFnSc1	kultura
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
jsou	být	k5eAaImIp3nP	být
eposy	epos	k1gInPc4	epos
oslavou	oslava	k1gFnSc7	oslava
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
bodem	bod	k1gInSc7	bod
pak	pak	k6eAd1	pak
jsou	být	k5eAaImIp3nP	být
partie	partie	k1gFnPc1	partie
Iliady	Iliada	k1gFnPc1	Iliada
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
možná	možná	k9	možná
odkazují	odkazovat	k5eAaImIp3nP	odkazovat
na	na	k7c6	na
události	událost	k1gFnSc6	událost
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
tyto	tento	k3xDgInPc1	tento
odkazy	odkaz	k1gInPc1	odkaz
však	však	k9	však
nejsou	být	k5eNaImIp3nP	být
jednoznačné	jednoznačný	k2eAgNnSc1d1	jednoznačné
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
partie	partie	k1gFnSc1	partie
Iliady	Iliada	k1gFnSc2	Iliada
vyložit	vyložit	k5eAaPmF	vyložit
i	i	k9	i
jinak	jinak	k6eAd1	jinak
a	a	k8xC	a
literatura	literatura	k1gFnSc1	literatura
ostatně	ostatně	k6eAd1	ostatně
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
anachronická	anachronický	k2eAgFnSc1d1	anachronická
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
datování	datování	k1gNnSc1	datování
na	na	k7c6	na
základě	základ	k1gInSc6	základ
historických	historický	k2eAgFnPc2d1	historická
událostí	událost	k1gFnPc2	událost
obtížné	obtížný	k2eAgFnPc1d1	obtížná
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
stop	stopa	k1gFnPc2	stopa
však	však	k9	však
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
polovinu	polovina	k1gFnSc4	polovina
8	[number]	k4	8
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Homérovy	Homérův	k2eAgInPc4d1	Homérův
antické	antický	k2eAgInPc4d1	antický
životopisy	životopis	k1gInPc4	životopis
připisují	připisovat	k5eAaImIp3nP	připisovat
Homérovi	Homérův	k2eAgMnPc1d1	Homérův
kromě	kromě	k7c2	kromě
dvou	dva	k4xCgInPc2	dva
nejznámějších	známý	k2eAgMnPc2d3	nejznámější
eposů	epos	k1gInPc2	epos
i	i	k8xC	i
další	další	k2eAgNnPc4d1	další
literární	literární	k2eAgNnPc4d1	literární
díla	dílo	k1gNnPc4	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Převážně	převážně	k6eAd1	převážně
se	se	k3xPyFc4	se
bezpochyby	bezpochyby	k6eAd1	bezpochyby
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
díla	dílo	k1gNnPc4	dílo
připsaná	připsaný	k2eAgNnPc4d1	připsané
mu	on	k3xPp3gMnSc3	on
neprávem	neprávo	k1gNnSc7	neprávo
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
je	on	k3xPp3gMnPc4	on
dnes	dnes	k6eAd1	dnes
známe	znát	k5eAaImIp1nP	znát
ve	v	k7c6	v
zlomcích	zlomek	k1gInPc6	zlomek
<g/>
,	,	kIx,	,
v	v	k7c6	v
úplnosti	úplnost	k1gFnSc6	úplnost
se	se	k3xPyFc4	se
dochoval	dochovat	k5eAaPmAgInS	dochovat
krátký	krátký	k2eAgInSc1d1	krátký
parodický	parodický	k2eAgInSc1d1	parodický
epos	epos	k1gInSc1	epos
Žabomyší	žabomyší	k2eAgFnSc1d1	žabomyší
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
Obzvlášť	obzvlášť	k6eAd1	obzvlášť
sporné	sporný	k2eAgNnSc1d1	sporné
je	být	k5eAaImIp3nS	být
autorství	autorství	k1gNnSc1	autorství
také	také	k9	také
Homérovi	Homér	k1gMnSc3	Homér
připisovaných	připisovaný	k2eAgFnPc2d1	připisovaná
33	[number]	k4	33
básní	báseň	k1gFnPc2	báseň
<g/>
,	,	kIx,	,
zvaných	zvaný	k2eAgInPc2d1	zvaný
Homérské	homérský	k2eAgInPc4d1	homérský
hymny	hymnus	k1gInPc4	hymnus
<g/>
,	,	kIx,	,
básní	báseň	k1gFnPc2	báseň
opěvujících	opěvující	k2eAgFnPc2d1	opěvující
bohy	bůh	k1gMnPc7	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Stojí	stát	k5eAaImIp3nS	stát
oběma	dva	k4xCgInPc3	dva
eposům	epos	k1gInPc3	epos
stylisticky	stylisticky	k6eAd1	stylisticky
blízko	blízko	k6eAd1	blízko
<g/>
.	.	kIx.	.
</s>
<s>
Rhapsódi	Rhapsód	k1gMnPc1	Rhapsód
je	on	k3xPp3gNnSc4	on
přednášeli	přednášet	k5eAaImAgMnP	přednášet
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
své	svůj	k3xOyFgFnSc2	svůj
recitace	recitace	k1gFnSc2	recitace
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgMnPc1d3	nejznámější
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
jsou	být	k5eAaImIp3nP	být
Hymnus	hymnus	k1gInSc4	hymnus
na	na	k7c4	na
Apollóna	Apollón	k1gMnSc4	Apollón
a	a	k8xC	a
Hymnus	hymnus	k1gInSc4	hymnus
na	na	k7c4	na
Afrodítu	Afrodíta	k1gFnSc4	Afrodíta
<g/>
.	.	kIx.	.
</s>
<s>
Homérská	homérský	k2eAgFnSc1d1	homérská
otázka	otázka	k1gFnSc1	otázka
je	být	k5eAaImIp3nS	být
literárněvědný	literárněvědný	k2eAgInSc4d1	literárněvědný
spor	spor	k1gInSc4	spor
o	o	k7c4	o
osobu	osoba	k1gFnSc4	osoba
Homéra	Homér	k1gMnSc2	Homér
a	a	k8xC	a
původ	původ	k1gInSc4	původ
obou	dva	k4xCgInPc2	dva
Homérových	Homérův	k2eAgInPc2d1	Homérův
eposů	epos	k1gInPc2	epos
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
přitom	přitom	k6eAd1	přitom
o	o	k7c4	o
problém	problém	k1gInSc4	problém
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
Homér	Homér	k1gMnSc1	Homér
(	(	kIx(	(
<g/>
lhostejno	lhostejno	k6eAd1	lhostejno
zda	zda	k8xS	zda
žil	žít	k5eAaImAgMnS	žít
nebo	nebo	k8xC	nebo
ne	ne	k9	ne
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
opravdu	opravdu	k6eAd1	opravdu
autorem	autor	k1gMnSc7	autor
obou	dva	k4xCgFnPc2	dva
eposů	epos	k1gInPc2	epos
-	-	kIx~	-
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
filologicky	filologicky	k6eAd1	filologicky
nepravděpodobné	pravděpodobný	k2eNgNnSc1d1	nepravděpodobné
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
mezi	mezi	k7c7	mezi
Iliadou	Iliada	k1gFnSc7	Iliada
a	a	k8xC	a
Odysseou	Odyssea	k1gFnSc7	Odyssea
leží	ležet	k5eAaImIp3nS	ležet
jazykově	jazykově	k6eAd1	jazykově
přibližně	přibližně	k6eAd1	přibližně
50	[number]	k4	50
let	léto	k1gNnPc2	léto
-	-	kIx~	-
nebo	nebo	k8xC	nebo
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Homér	Homér	k1gMnSc1	Homér
shrnuto	shrnut	k2eAgNnSc4d1	shrnuto
víc	hodně	k6eAd2	hodně
různých	různý	k2eAgMnPc2d1	různý
básníků	básník	k1gMnPc2	básník
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
sepsali	sepsat	k5eAaPmAgMnP	sepsat
starší	starý	k2eAgMnPc1d2	starší
<g/>
,	,	kIx,	,
ústně	ústně	k6eAd1	ústně
tradovanou	tradovaný	k2eAgFnSc4d1	tradovaná
látku	látka	k1gFnSc4	látka
<g/>
.	.	kIx.	.
</s>
<s>
Spor	spor	k1gInSc1	spor
vychází	vycházet	k5eAaImIp3nS	vycházet
ze	z	k7c2	z
skutečnosti	skutečnost	k1gFnSc2	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
eposech	epos	k1gInPc6	epos
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
místa	místo	k1gNnPc1	místo
s	s	k7c7	s
porušením	porušení	k1gNnSc7	porušení
časové	časový	k2eAgFnSc2d1	časová
linie	linie	k1gFnSc2	linie
<g/>
,	,	kIx,	,
navzájem	navzájem	k6eAd1	navzájem
si	se	k3xPyFc3	se
odporující	odporující	k2eAgFnSc1d1	odporující
nebo	nebo	k8xC	nebo
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
nedostatky	nedostatek	k1gInPc7	nedostatek
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
osobu	osoba	k1gFnSc4	osoba
Homéra	Homér	k1gMnSc2	Homér
a	a	k8xC	a
původ	původ	k1gInSc4	původ
jeho	jeho	k3xOp3gNnPc2	jeho
děl	dělo	k1gNnPc2	dělo
rozpoutal	rozpoutat	k5eAaPmAgMnS	rozpoutat
spor	spor	k1gInSc4	spor
roku	rok	k1gInSc2	rok
1795	[number]	k4	1795
německý	německý	k2eAgMnSc1d1	německý
klasický	klasický	k2eAgMnSc1d1	klasický
filolog	filolog	k1gMnSc1	filolog
Friedrich	Friedrich	k1gMnSc1	Friedrich
August	August	k1gMnSc1	August
Wolf	Wolf	k1gMnSc1	Wolf
spisem	spis	k1gInSc7	spis
Prolegomena	prolegomena	k1gNnPc1	prolegomena
ad	ad	k7c4	ad
Homerum	Homerum	k1gNnSc4	Homerum
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
neznámých	známý	k2eNgMnPc2d1	neznámý
básníků	básník	k1gMnPc2	básník
původci	původce	k1gMnPc1	původce
menších	malý	k2eAgInPc2d2	menší
zpěvů	zpěv	k1gInPc2	zpěv
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
v	v	k7c6	v
Peisistratově	Peisistratův	k2eAgFnSc6d1	Peisistratův
době	doba	k1gFnSc6	doba
jeden	jeden	k4xCgMnSc1	jeden
básník	básník	k1gMnSc1	básník
složil	složit	k5eAaPmAgMnS	složit
celé	celý	k2eAgNnSc4d1	celé
dílo	dílo	k1gNnSc4	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
něm	on	k3xPp3gMnSc6	on
se	se	k3xPyFc4	se
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
konzervativní	konzervativní	k2eAgFnSc1d1	konzervativní
škola	škola	k1gFnSc1	škola
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
připouštěla	připouštět	k5eAaImAgFnS	připouštět
víceméně	víceméně	k9	víceméně
volnou	volný	k2eAgFnSc4d1	volná
absorpci	absorpce	k1gFnSc4	absorpce
předchozích	předchozí	k2eAgInPc2d1	předchozí
existujících	existující	k2eAgInPc2d1	existující
zpěvů	zpěv	k1gInPc2	zpěv
při	při	k7c6	při
tvorbě	tvorba	k1gFnSc6	tvorba
Iliady	Iliada	k1gFnSc2	Iliada
a	a	k8xC	a
Odysseje	Odyssea	k1gFnSc2	Odyssea
i	i	k9	i
existenci	existence	k1gFnSc4	existence
závažných	závažný	k2eAgFnPc2d1	závažná
interpolací	interpolace	k1gFnPc2	interpolace
(	(	kIx(	(
<g/>
vpisů	vpis	k1gInPc2	vpis
do	do	k7c2	do
textu	text	k1gInSc2	text
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
připisovala	připisovat	k5eAaImAgFnS	připisovat
hlavní	hlavní	k2eAgNnSc4d1	hlavní
úsilí	úsilí	k1gNnSc4	úsilí
při	při	k7c6	při
tvorbě	tvorba	k1gFnSc6	tvorba
děl	dít	k5eAaImAgMnS	dít
geniálnímu	geniální	k2eAgMnSc3d1	geniální
básníkovi	básník	k1gMnSc3	básník
působícímu	působící	k2eAgInSc3d1	působící
v	v	k7c6	v
předhistorické	předhistorický	k2eAgFnSc6d1	předhistorická
době	doba	k1gFnSc6	doba
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
významnou	významný	k2eAgFnSc7d1	významná
osobou	osoba	k1gFnSc7	osoba
sporu	spor	k1gInSc2	spor
byl	být	k5eAaImAgMnS	být
Gottfried	Gottfried	k1gMnSc1	Gottfried
Hermann	Hermann	k1gMnSc1	Hermann
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
předpokládal	předpokládat	k5eAaImAgMnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
dva	dva	k4xCgMnPc1	dva
neznámí	známý	k2eNgMnPc1d1	neznámý
básníci	básník	k1gMnPc1	básník
<g/>
,	,	kIx,	,
zpívající	zpívající	k2eAgMnPc1d1	zpívající
o	o	k7c6	o
hněvu	hněv	k1gInSc6	hněv
Achillově	Achillův	k2eAgInSc6d1	Achillův
a	a	k8xC	a
návratu	návrat	k1gInSc6	návrat
Odyssea	Odyssea	k1gFnSc1	Odyssea
získali	získat	k5eAaPmAgMnP	získat
v	v	k7c6	v
rané	raný	k2eAgFnSc6d1	raná
době	doba	k1gFnSc6	doba
takovou	takový	k3xDgFnSc4	takový
slávu	sláva	k1gFnSc4	sláva
<g/>
,	,	kIx,	,
že	že	k8xS	že
zatlačili	zatlačit	k5eAaPmAgMnP	zatlačit
do	do	k7c2	do
pozadí	pozadí	k1gNnSc2	pozadí
ostatní	ostatní	k2eAgInPc4d1	ostatní
příběhy	příběh	k1gInPc4	příběh
vyprávějící	vyprávějící	k2eAgInSc4d1	vyprávějící
o	o	k7c6	o
Tróji	Trója	k1gFnSc6	Trója
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc1	jejich
příběhy	příběh	k1gInPc1	příběh
byly	být	k5eAaImAgInP	být
rozhojněny	rozhojnit	k5eAaPmNgInP	rozhojnit
dalšími	další	k2eAgFnPc7d1	další
generacemi	generace	k1gFnPc7	generace
rhapsódů	rhapsód	k1gMnPc2	rhapsód
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
části	část	k1gFnPc1	část
Iliady	Iliada	k1gFnSc2	Iliada
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
nicméně	nicméně	k8xC	nicméně
zdály	zdát	k5eAaImAgFnP	zdát
starší	starý	k2eAgFnPc1d2	starší
než	než	k8xS	než
báseň	báseň	k1gFnSc1	báseň
o	o	k7c6	o
hněvu	hněv	k1gInSc6	hněv
Achilově	Achilův	k2eAgInSc6d1	Achilův
<g/>
.	.	kIx.	.
</s>
<s>
Různí	různý	k2eAgMnPc1d1	různý
další	další	k2eAgMnPc1d1	další
autoři	autor	k1gMnPc1	autor
se	se	k3xPyFc4	se
v	v	k7c6	v
Iliadě	Iliada	k1gFnSc6	Iliada
a	a	k8xC	a
Odysseji	Odyssea	k1gFnSc6	Odyssea
pokoušeli	pokoušet	k5eAaImAgMnP	pokoušet
najít	najít	k5eAaPmF	najít
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
původní	původní	k2eAgFnPc4d1	původní
písně	píseň	k1gFnPc4	píseň
nebo	nebo	k8xC	nebo
původní	původní	k2eAgNnSc4d1	původní
jádro	jádro	k1gNnSc4	jádro
<g/>
,	,	kIx,	,
vždy	vždy	k6eAd1	vždy
se	se	k3xPyFc4	se
však	však	k9	však
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
odlišovali	odlišovat	k5eAaImAgMnP	odlišovat
<g/>
.	.	kIx.	.
</s>
<s>
Homérská	homérský	k2eAgFnSc1d1	homérská
otázka	otázka	k1gFnSc1	otázka
dodnes	dodnes	k6eAd1	dodnes
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
nerozřešena	rozřešen	k2eNgFnSc1d1	nerozřešena
<g/>
,	,	kIx,	,
blízké	blízký	k2eAgFnSc6d1	blízká
pravdě	pravda	k1gFnSc6	pravda
se	se	k3xPyFc4	se
zdá	zdát	k5eAaPmIp3nS	zdát
jen	jen	k9	jen
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
každý	každý	k3xTgInSc1	každý
epos	epos	k1gInSc1	epos
má	mít	k5eAaImIp3nS	mít
jiného	jiný	k2eAgMnSc4d1	jiný
autora	autor	k1gMnSc4	autor
<g/>
,	,	kIx,	,
že	že	k8xS	že
Odyssea	Odyssea	k1gFnSc1	Odyssea
má	mít	k5eAaImIp3nS	mít
autora	autor	k1gMnSc4	autor
jednoho	jeden	k4xCgMnSc4	jeden
<g/>
,	,	kIx,	,
že	že	k8xS	že
Odyssea	Odyssea	k1gFnSc1	Odyssea
je	být	k5eAaImIp3nS	být
mladší	mladý	k2eAgFnSc1d2	mladší
než	než	k8xS	než
Iliada	Iliada	k1gFnSc1	Iliada
<g/>
,	,	kIx,	,
nepochybné	pochybný	k2eNgNnSc1d1	nepochybné
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
eposy	epos	k1gInPc1	epos
mají	mít	k5eAaImIp3nP	mít
základy	základ	k1gInPc4	základ
(	(	kIx(	(
<g/>
třeba	třeba	k6eAd1	třeba
některé	některý	k3yIgInPc4	některý
obraty	obrat	k1gInPc4	obrat
<g/>
)	)	kIx)	)
už	už	k6eAd1	už
v	v	k7c6	v
době	doba	k1gFnSc6	doba
brzké	brzký	k2eAgFnSc6d1	brzká
po	po	k7c6	po
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
popisují	popisovat	k5eAaImIp3nP	popisovat
<g/>
.	.	kIx.	.
</s>
<s>
Homérské	homérský	k2eAgInPc1d1	homérský
eposy	epos	k1gInPc1	epos
se	se	k3xPyFc4	se
u	u	k7c2	u
Řeků	Řek	k1gMnPc2	Řek
těšily	těšit	k5eAaImAgFnP	těšit
ohromné	ohromný	k2eAgFnPc1d1	ohromná
úctě	úcta	k1gFnSc6	úcta
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
poezii	poezie	k1gFnSc4	poezie
a	a	k8xC	a
přednášely	přednášet	k5eAaImAgFnP	přednášet
se	se	k3xPyFc4	se
o	o	k7c6	o
největších	veliký	k2eAgFnPc6d3	veliký
slavnostech	slavnost	k1gFnPc6	slavnost
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
školním	školní	k2eAgNnSc6d1	školní
vyučování	vyučování	k1gNnSc6	vyučování
se	se	k3xPyFc4	se
užívaly	užívat	k5eAaImAgFnP	užívat
jako	jako	k9	jako
vzor	vzor	k1gInSc1	vzor
<g/>
.	.	kIx.	.
</s>
<s>
Řekům	Řek	k1gMnPc3	Řek
<g/>
,	,	kIx,	,
politicky	politicky	k6eAd1	politicky
rozděleným	rozdělený	k2eAgInSc7d1	rozdělený
<g/>
,	,	kIx,	,
také	také	k9	také
pomáhaly	pomáhat	k5eAaImAgFnP	pomáhat
vytvářet	vytvářet	k5eAaImF	vytvářet
společné	společný	k2eAgNnSc4d1	společné
řecké	řecký	k2eAgNnSc4d1	řecké
vědomí	vědomí	k1gNnSc4	vědomí
<g/>
.	.	kIx.	.
</s>
<s>
Homérská	homérský	k2eAgNnPc4d1	homérské
témata	téma	k1gNnPc4	téma
byla	být	k5eAaImAgFnS	být
všudepřítomná	všudepřítomný	k2eAgFnSc1d1	všudepřítomná
v	v	k7c6	v
antické	antický	k2eAgFnSc6d1	antická
řecké	řecký	k2eAgFnSc6d1	řecká
literatuře	literatura	k1gFnSc6	literatura
a	a	k8xC	a
hojně	hojně	k6eAd1	hojně
zastoupená	zastoupený	k2eAgFnSc1d1	zastoupená
i	i	k9	i
v	v	k7c6	v
pozdější	pozdní	k2eAgFnSc6d2	pozdější
literatuře	literatura	k1gFnSc6	literatura
evropské	evropský	k2eAgFnSc6d1	Evropská
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgInPc1d1	mnohý
obraty	obrat	k1gInPc1	obrat
a	a	k8xC	a
okřídlená	okřídlený	k2eAgNnPc1d1	okřídlené
slova	slovo	k1gNnPc1	slovo
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
Homérových	Homérův	k2eAgInPc2d1	Homérův
eposů	epos	k1gInPc2	epos
(	(	kIx(	(
<g/>
dokonce	dokonce	k9	dokonce
i	i	k9	i
samo	sám	k3xTgNnSc1	sám
spojení	spojení	k1gNnSc1	spojení
"	"	kIx"	"
<g/>
okřídlená	okřídlený	k2eAgNnPc1d1	okřídlené
slova	slovo	k1gNnPc1	slovo
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ilias	Ilias	k1gFnSc1	Ilias
a	a	k8xC	a
Odyssea	Odyssea	k1gFnSc1	Odyssea
byly	být	k5eAaImAgFnP	být
tak	tak	k6eAd1	tak
dokonalými	dokonalý	k2eAgInPc7d1	dokonalý
díly	díl	k1gInPc7	díl
svého	svůj	k3xOyFgInSc2	svůj
žánru	žánr	k1gInSc2	žánr
<g/>
,	,	kIx,	,
že	že	k8xS	že
srovnatelná	srovnatelný	k2eAgNnPc1d1	srovnatelné
epická	epický	k2eAgNnPc1d1	epické
díla	dílo	k1gNnPc1	dílo
již	již	k6eAd1	již
v	v	k7c6	v
řecké	řecký	k2eAgFnSc6d1	řecká
literatuře	literatura	k1gFnSc6	literatura
nevznikla	vzniknout	k5eNaPmAgFnS	vzniknout
a	a	k8xC	a
pozdější	pozdní	k2eAgMnPc1d2	pozdější
básníci	básník	k1gMnPc1	básník
(	(	kIx(	(
<g/>
Kallimachos	Kallimachos	k1gMnSc1	Kallimachos
z	z	k7c2	z
Kyrény	Kyréna	k1gFnSc2	Kyréna
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
epické	epický	k2eAgNnSc4d1	epické
básnictví	básnictví	k1gNnSc4	básnictví
oživit	oživit	k5eAaPmF	oživit
pomocí	pomocí	k7c2	pomocí
odlišných	odlišný	k2eAgInPc2d1	odlišný
přístupů	přístup	k1gInPc2	přístup
<g/>
.	.	kIx.	.
</s>
