<s>
Homér	Homér	k1gMnSc1	Homér
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
Ὅ	Ὅ	k?	Ὅ
Homéros	Homérosa	k1gFnPc2	Homérosa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jméno	jméno	k1gNnSc4	jméno
nejstaršího	starý	k2eAgMnSc2d3	nejstarší
známého	známý	k2eAgMnSc2d1	známý
řeckého	řecký	k2eAgMnSc2d1	řecký
básníka	básník	k1gMnSc2	básník
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgNnSc3	jenž
se	se	k3xPyFc4	se
připisuje	připisovat	k5eAaImIp3nS	připisovat
sepsání	sepsání	k1gNnSc2	sepsání
epických	epický	k2eAgFnPc2d1	epická
básní	báseň	k1gFnPc2	báseň
Ilias	Ilias	k1gFnSc1	Ilias
a	a	k8xC	a
Odysseia	Odysseia	k1gFnSc1	Odysseia
<g/>
.	.	kIx.	.
</s>
