Dle	dle	k7c2
tradice	tradice	k1gFnSc2
měla	mít	k5eAaImAgFnS
Nina	Nina	k1gFnSc1
vidění	vidění	k1gNnSc2
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yQgInSc6,k3yIgInSc6,k3yRgInSc6
se	se	k3xPyFc4
jí	on	k3xPp3gFnSc7
zjevila	zjevit	k5eAaPmAgFnS
Panna	Panna	k1gFnSc1
Maria	Maria	k1gFnSc1
<g/>
,	,	kIx,
podala	podat	k5eAaPmAgFnS
jí	jíst	k5eAaImIp3nS
kříž	kříž	k1gInSc4
z	z	k7c2
révových	révový	k2eAgFnPc2d1
ratolestí	ratolest	k1gFnPc2
a	a	k8xC
pravila	pravit	k5eAaImAgFnS,k5eAaBmAgFnS
<g/>
:	:	kIx,
"	"	kIx"
<g/>
Jdi	jít	k5eAaImRp2nS
do	do	k7c2
Hibérie	Hibérie	k1gFnSc2
a	a	k8xC
zvěstuj	zvěstovat	k5eAaImRp2nS
evangelium	evangelium	k1gNnSc1
Ježíše	Ježíš	k1gMnSc2
Krista	Kristus	k1gMnSc2
<g/>
,	,	kIx,
a	a	k8xC
najdeš	najít	k5eAaPmIp2nS
milost	milost	k1gFnSc4
před	před	k7c7
Hospodinem	Hospodin	k1gMnSc7
<g/>
.	.	kIx.
