<s>
Svatá	svatý	k2eAgFnSc1d1
Nina	Nina	k1gFnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
není	být	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
ozdrojován	ozdrojován	k2eAgMnSc1d1
a	a	k8xC
může	moct	k5eAaImIp3nS
tedy	tedy	k9
obsahovat	obsahovat	k5eAaImF
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yRgFnPc4,k3yIgFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
ověřit	ověřit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
Jste	být	k5eAaImIp2nP
<g/>
-li	-li	k?
s	s	k7c7
popisovaným	popisovaný	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
seznámeni	seznámen	k2eAgMnPc1d1
<g/>
,	,	kIx,
pomozte	pomoct	k5eAaPmRp2nPwC
doložit	doložit	k5eAaPmF
uvedená	uvedený	k2eAgNnPc4d1
tvrzení	tvrzení	k1gNnPc4
doplněním	doplnění	k1gNnSc7
referencí	reference	k1gFnPc2
na	na	k7c4
věrohodné	věrohodný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Svatá	svatý	k2eAgFnSc1d1
Kristýna	Kristýna	k1gFnSc1
Narození	narození	k1gNnSc2
</s>
<s>
296	#num#	k4
<g/>
Kappadokie	Kappadokie	k1gFnSc1
Úmrtí	úmrtí	k1gNnSc2
</s>
<s>
27	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
335	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
38	#num#	k4
<g/>
–	–	k?
<g/>
39	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Bodbe	Bodb	k1gInSc5
Monastery	Monaster	k1gInPc5
Povolání	povolání	k1gNnSc1
</s>
<s>
misionářka	misionářka	k1gFnSc1
Nábož	Nábož	k1gFnSc1
<g/>
.	.	kIx.
vyznání	vyznání	k1gNnSc1
</s>
<s>
pravoslaví	pravoslaví	k1gNnSc1
Funkce	funkce	k1gFnSc2
</s>
<s>
Apoštol	apoštol	k1gMnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Svatá	svatý	k2eAgFnSc1d1
Kristýna	Kristýna	k1gFnSc1
</s>
<s>
Svatá	svatý	k2eAgFnSc1d1
Nina	Nina	k1gFnSc1
či	či	k8xC
svatá	svatý	k2eAgFnSc1d1
Kristýna	Kristýna	k1gFnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
také	také	k9
Nino	Nina	k1gFnSc5
<g/>
,	,	kIx,
gruzínsky	gruzínsky	k6eAd1
წ	წ	k?
ნ	ნ	k?
<g/>
,	,	kIx,
ts	ts	k0
<g/>
'	'	kIx"
<g/>
minda	minda	k1gFnSc1
Nino	Nina	k1gFnSc5
<g/>
)	)	kIx)
(	(	kIx(
<g/>
okolo	okolo	k7c2
290	#num#	k4
Palestina	Palestina	k1gFnSc1
–	–	k?
okolo	okolo	k7c2
350	#num#	k4
Mccheta	Mccheto	k1gNnSc2
<g/>
,	,	kIx,
Gruzie	Gruzie	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
patronkou	patronka	k1gFnSc7
Gruzie	Gruzie	k1gFnSc1
a	a	k8xC
první	první	k4xOgFnSc7
křesťankou	křesťanka	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
na	na	k7c6
tamní	tamní	k2eAgNnSc4d1
území	území	k1gNnSc4
přinesla	přinést	k5eAaPmAgFnS
křesťanství	křesťanství	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bývá	bývat	k5eAaImIp3nS
označována	označovat	k5eAaImNgFnS
za	za	k7c4
apoštolku	apoštolka	k1gFnSc4
Gruzie	Gruzie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Gruzii	Gruzie	k1gFnSc6
je	být	k5eAaImIp3nS
známá	známý	k2eAgFnSc1d1
pod	pod	k7c7
jménem	jméno	k1gNnSc7
Nino	Nina	k1gFnSc5
(	(	kIx(
<g/>
Ninona	Ninona	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tamními	tamní	k2eAgMnPc7d1
obrácenými	obrácený	k2eAgMnPc7d1
věřícími	věřící	k1gMnPc7
bývala	bývat	k5eAaImAgFnS
označována	označován	k2eAgFnSc1d1
jako	jako	k8xS,k8xC
Kristiana	Kristian	k1gMnSc2
nebo	nebo	k8xC
Kristina	Kristin	k2eAgMnSc2d1
<g/>
,	,	kIx,
tedy	tedy	k8xC
křesťanka	křesťanka	k1gFnSc1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
je	být	k5eAaImIp3nS
známější	známý	k2eAgFnSc1d2
pod	pod	k7c7
tímto	tento	k3xDgNnSc7
jménem	jméno	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bývá	bývat	k5eAaImIp3nS
zobrazována	zobrazovat	k5eAaImNgFnS
s	s	k7c7
křížem	kříž	k1gInSc7
z	z	k7c2
ratolestí	ratolestí	k1gNnSc2
révy	réva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Podle	podle	k7c2
nejrozšířenější	rozšířený	k2eAgFnSc2d3
tradice	tradice	k1gFnSc2
se	se	k3xPyFc4
Kristýna	Kristýna	k1gFnSc1
narodila	narodit	k5eAaPmAgFnS
ve	v	k7c6
městě	město	k1gNnSc6
Kolastra	Kolastrum	k1gNnSc2
v	v	k7c6
Kappadokii	Kappadokie	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
spadala	spadat	k5eAaImAgFnS,k5eAaPmAgFnS
pod	pod	k7c4
římské	římský	k2eAgNnSc4d1
impérium	impérium	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gInPc4
rodina	rodina	k1gFnSc1
mluvila	mluvit	k5eAaImAgFnS
řecky	řecky	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Názory	názor	k1gInPc7
římskokatolické	římskokatolický	k2eAgFnSc2d1
a	a	k8xC
pravoslavné	pravoslavný	k2eAgFnSc2d1
církve	církev	k1gFnSc2
na	na	k7c4
Kristýnin	Kristýnin	k2eAgInSc4d1
původ	původ	k1gInSc4
se	se	k3xPyFc4
nicméně	nicméně	k8xC
rozcházejí	rozcházet	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s>
Legenda	legenda	k1gFnSc1
pravoslavné	pravoslavný	k2eAgFnSc2d1
tradice	tradice	k1gFnSc2
</s>
<s>
Dle	dle	k7c2
tradice	tradice	k1gFnSc2
východních	východní	k2eAgFnPc2d1
církví	církev	k1gFnPc2
byla	být	k5eAaImAgFnS
jedináčkem	jedináček	k1gMnSc7
z	z	k7c2
významné	významný	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejím	její	k3xOp3gMnSc7
otcem	otec	k1gMnSc7
byl	být	k5eAaImAgMnS
římský	římský	k2eAgMnSc1d1
generál	generál	k1gMnSc1
Zabulon	Zabulon	k1gInSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc1
rod	rod	k1gInSc1
byl	být	k5eAaImAgInS
údajně	údajně	k6eAd1
spřízněn	spřízněn	k2eAgInSc1d1
se	s	k7c7
svatým	svatý	k2eAgMnSc7d1
Jiřím	Jiří	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc7
matkou	matka	k1gFnSc7
byla	být	k5eAaImAgFnS
Zuzana	Zuzana	k1gFnSc1
<g/>
,	,	kIx,
příbuzná	příbuzná	k1gFnSc1
jeruzalémského	jeruzalémský	k2eAgMnSc2d1
patriarchy	patriarcha	k1gMnSc2
Houbnala	Houbnala	k1gFnSc2
I.	I.	kA
V	v	k7c6
dětství	dětství	k1gNnSc6
se	se	k3xPyFc4
o	o	k7c4
Ninu	Nina	k1gFnSc4
starala	starat	k5eAaImAgFnS
její	její	k3xOp3gFnSc1
příbuzná	příbuzný	k2eAgFnSc1d1
jeptiška	jeptiška	k1gFnSc1
Sára	Sára	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
spolu	spolu	k6eAd1
se	s	k7c7
strýcem	strýc	k1gMnSc7
(	(	kIx(
<g/>
jeruzalémským	jeruzalémský	k2eAgMnSc7d1
patriarchou	patriarcha	k1gMnSc7
<g/>
)	)	kIx)
dohlížela	dohlížet	k5eAaImAgFnS
na	na	k7c4
tradiční	tradiční	k2eAgFnSc4d1
výchovu	výchova	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
strýci	strýc	k1gMnSc3
se	se	k3xPyFc4
později	pozdě	k6eAd2
Nina	Nina	k1gFnSc1
dostala	dostat	k5eAaPmAgFnS
do	do	k7c2
Říma	Řím	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tam	tam	k6eAd1
se	se	k3xPyFc4
po	po	k7c6
čase	čas	k1gInSc6
rozhodla	rozhodnout	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
vydá	vydat	k5eAaPmIp3nS
hlásat	hlásat	k5eAaImF
Písmo	písmo	k1gNnSc4
do	do	k7c2
Hibérie	Hibérie	k1gFnSc2
(	(	kIx(
<g/>
dnešní	dnešní	k2eAgFnSc2d1
Gruzie	Gruzie	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
na	na	k7c6
jejímž	jejíž	k3xOyRp3gNnSc6
území	území	k1gNnSc6
se	se	k3xPyFc4
údajně	údajně	k6eAd1
nacházela	nacházet	k5eAaImAgFnS
Ježíšova	Ježíšův	k2eAgFnSc1d1
tunika	tunika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dle	dle	k7c2
tradice	tradice	k1gFnSc2
měla	mít	k5eAaImAgFnS
Nina	Nina	k1gFnSc1
vidění	vidění	k1gNnSc2
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yQgInSc6,k3yIgInSc6,k3yRgInSc6
se	se	k3xPyFc4
jí	on	k3xPp3gFnSc7
zjevila	zjevit	k5eAaPmAgFnS
Panna	Panna	k1gFnSc1
Maria	Maria	k1gFnSc1
<g/>
,	,	kIx,
podala	podat	k5eAaPmAgFnS
jí	jíst	k5eAaImIp3nS
kříž	kříž	k1gInSc4
z	z	k7c2
révových	révový	k2eAgFnPc2d1
ratolestí	ratolest	k1gFnPc2
a	a	k8xC
pravila	pravit	k5eAaImAgFnS,k5eAaBmAgFnS
<g/>
:	:	kIx,
"	"	kIx"
<g/>
Jdi	jít	k5eAaImRp2nS
do	do	k7c2
Hibérie	Hibérie	k1gFnSc2
a	a	k8xC
zvěstuj	zvěstovat	k5eAaImRp2nS
evangelium	evangelium	k1gNnSc1
Ježíše	Ježíš	k1gMnSc2
Krista	Kristus	k1gMnSc2
<g/>
,	,	kIx,
a	a	k8xC
najdeš	najít	k5eAaPmIp2nS
milost	milost	k1gFnSc4
před	před	k7c7
Hospodinem	Hospodin	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Já	já	k3xPp1nSc1
ti	ten	k3xDgMnPc1
budu	být	k5eAaImBp1nS
štítem	štít	k1gInSc7
proti	proti	k7c3
všem	všecek	k3xTgMnPc3
nepřátelům	nepřítel	k1gMnPc3
<g/>
,	,	kIx,
viditelným	viditelný	k2eAgMnPc3d1
i	i	k8xC
neviditelným	viditelný	k2eNgMnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
síle	síla	k1gFnSc3
tohoto	tento	k3xDgInSc2
kříže	kříž	k1gInSc2
postavíš	postavit	k5eAaPmIp2nS
v	v	k7c6
zemi	zem	k1gFnSc6
spásný	spásný	k2eAgInSc4d1
prapor	prapor	k1gInSc4
víry	víra	k1gFnSc2
v	v	k7c4
mého	můj	k3xOp1gMnSc4
milovaného	milovaný	k1gMnSc4
Syna	syn	k1gMnSc4
a	a	k8xC
Pána	pán	k1gMnSc4
<g/>
.	.	kIx.
<g/>
"	"	kIx"
</s>
<s>
Podle	podle	k7c2
legendy	legenda	k1gFnSc2
měla	mít	k5eAaImAgFnS
Nina	Nina	k1gFnSc1
schopnost	schopnost	k1gFnSc4
zázračně	zázračně	k6eAd1
uzdravovat	uzdravovat	k5eAaImF
nemocné	nemocný	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
jejích	její	k3xOp3gFnPc6
schopnostech	schopnost	k1gFnPc6
se	se	k3xPyFc4
doslechla	doslechnout	k5eAaPmAgFnS
královna	královna	k1gFnSc1
Nana	Nana	k1gFnSc1
<g/>
,	,	kIx,
jíž	jenž	k3xRgFnSc2
následně	následně	k6eAd1
Kristýna	Kristýna	k1gFnSc1
uzdravila	uzdravit	k5eAaPmAgFnS
malého	malý	k2eAgMnSc4d1
syna	syn	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Královna	královna	k1gFnSc1
se	se	k3xPyFc4
poté	poté	k6eAd1
obrátila	obrátit	k5eAaPmAgFnS
na	na	k7c4
křesťanství	křesťanství	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc7
manžel	manžel	k1gMnSc1
král	král	k1gMnSc1
Mirian	Mirian	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
se	se	k3xPyFc4
nejprve	nejprve	k6eAd1
novému	nový	k2eAgNnSc3d1
náboženství	náboženství	k1gNnSc3
bránil	bránit	k5eAaImAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
se	se	k3xPyFc4
ale	ale	k9
na	na	k7c6
lovu	lov	k1gInSc6
ocitl	ocitnout	k5eAaPmAgInS
oslepen	oslepit	k5eAaPmNgInS
temnotou	temnota	k1gFnSc7
a	a	k8xC
ztracen	ztratit	k5eAaPmNgMnS
v	v	k7c6
lese	les	k1gInSc6
<g/>
,	,	kIx,
pomodlil	pomodlit	k5eAaPmAgMnS
se	se	k3xPyFc4
prý	prý	k9
k	k	k7c3
"	"	kIx"
<g/>
Nininému	Nininý	k1gMnSc3
bohu	bůh	k1gMnSc3
<g/>
"	"	kIx"
a	a	k8xC
poté	poté	k6eAd1
našel	najít	k5eAaPmAgMnS
cestu	cesta	k1gFnSc4
zpět	zpět	k6eAd1
k	k	k7c3
družině	družina	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nedlouho	dlouho	k6eNd1
poté	poté	k6eAd1
konvertoval	konvertovat	k5eAaBmAgMnS
a	a	k8xC
roku	rok	k1gInSc2
327	#num#	k4
vyhlásil	vyhlásit	k5eAaPmAgInS
křesťanství	křesťanství	k1gNnSc4
jako	jako	k8xC,k8xS
oficiální	oficiální	k2eAgNnSc4d1
náboženství	náboženství	k1gNnSc4
Hibérie	Hibérie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kristýna	Kristýna	k1gFnSc1
až	až	k9
do	do	k7c2
smrti	smrt	k1gFnSc2
okolo	okolo	k7c2
roku	rok	k1gInSc2
350	#num#	k4
působila	působit	k5eAaImAgFnS
na	na	k7c6
území	území	k1gNnSc6
království	království	k1gNnSc2
a	a	k8xC
šířila	šířit	k5eAaImAgFnS
křesťanství	křesťanství	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Legenda	legenda	k1gFnSc1
římskokatolické	římskokatolický	k2eAgFnSc2d1
tradice	tradice	k1gFnSc2
</s>
<s>
Západní	západní	k2eAgFnPc1d1
tradice	tradice	k1gFnPc1
čerpají	čerpat	k5eAaImIp3nP
především	především	k9
z	z	k7c2
textů	text	k1gInPc2
Tyrannia	Tyrannium	k1gNnSc2
Rufina	Rufino	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
nich	on	k3xPp3gMnPc2
pocházela	pocházet	k5eAaImAgFnS
Nina	Nina	k1gFnSc1
(	(	kIx(
<g/>
v	v	k7c6
tomto	tento	k3xDgInSc6
kontextu	kontext	k1gInSc6
nazývaná	nazývaný	k2eAgFnSc1d1
častěji	často	k6eAd2
Kristýna	Kristýna	k1gFnSc1
<g/>
)	)	kIx)
z	z	k7c2
Říma	Řím	k1gInSc2
<g/>
,	,	kIx,
Jeruzaléma	Jeruzalém	k1gInSc2
nebo	nebo	k8xC
Galie	Galie	k1gFnSc2
(	(	kIx(
<g/>
dnešní	dnešní	k2eAgFnSc2d1
Francie	Francie	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
její	její	k3xOp3gInSc1
rodokmen	rodokmen	k1gInSc1
je	být	k5eAaImIp3nS
neznámý	známý	k2eNgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
vlády	vláda	k1gFnSc2
císaře	císař	k1gMnSc2
Konstantina	Konstantin	k1gMnSc2
Velikého	veliký	k2eAgMnSc2d1
byla	být	k5eAaImAgFnS
zajata	zajmout	k5eAaPmNgFnS
Hibery	Hiber	k1gMnPc7
(	(	kIx(
<g/>
Gruzínci	Gruzínec	k1gMnPc7
<g/>
)	)	kIx)
a	a	k8xC
dostala	dostat	k5eAaPmAgFnS
se	se	k3xPyFc4
do	do	k7c2
otroctví	otroctví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
jiné	jiný	k2eAgFnSc2d1
verze	verze	k1gFnSc2
na	na	k7c6
území	území	k1gNnSc6
dnešní	dnešní	k2eAgFnSc2d1
Gruzie	Gruzie	k1gFnSc2
prchla	prchnout	k5eAaPmAgFnS
již	již	k6eAd1
dříve	dříve	k6eAd2
za	za	k7c4
pronásledování	pronásledování	k1gNnSc4
křesťanů	křesťan	k1gMnPc2
císařem	císař	k1gMnSc7
Diokleciánem	Dioklecián	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Úcta	úcta	k1gFnSc1
a	a	k8xC
tradice	tradice	k1gFnSc1
</s>
<s>
V	v	k7c6
Gruzii	Gruzie	k1gFnSc6
slaví	slavit	k5eAaImIp3nS
svátek	svátek	k1gInSc1
své	svůj	k3xOyFgFnSc2
Kristýny	Kristýna	k1gFnSc2
14	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kristýnina	Kristýnin	k2eAgFnSc1d1
památka	památka	k1gFnSc1
se	se	k3xPyFc4
slaví	slavit	k5eAaImIp3nS
v	v	k7c6
Řecku	Řecko	k1gNnSc6
27	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c6
Arménii	Arménie	k1gFnSc6
29	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
http://catholica.cz/?id=178	http://catholica.cz/?id=178	k4
<g/>
↑	↑	k?
http://catholica.cz/?id=178	http://catholica.cz/?id=178	k4
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
V.	V.	kA
<g/>
Schauber	Schauber	k1gMnSc1
-	-	kIx~
H.	H.	kA
<g/>
M.	M.	kA
<g/>
Schindler	Schindler	k1gMnSc1
<g/>
:	:	kIx,
Rok	rok	k1gInSc1
se	s	k7c7
svatými	svatý	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Karmelitánské	karmelitánský	k2eAgFnPc4d1
nakladatelství	nakladatelství	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
129794848	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
5130	#num#	k4
1568	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
93026261	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
35544861	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
93026261	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Gruzie	Gruzie	k1gFnSc1
|	|	kIx~
Křesťanství	křesťanství	k1gNnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
</s>
