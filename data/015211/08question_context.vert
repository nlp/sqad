<s>
Svatá	svatý	k2eAgFnSc1d1
Nina	Nina	k1gFnSc1
či	či	k8xC
svatá	svatý	k2eAgFnSc1d1
Kristýna	Kristýna	k1gFnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
také	také	k9
Nino	Nina	k1gFnSc5
<g/>
,	,	kIx,
gruzínsky	gruzínsky	k6eAd1
წ	წ	k?
ნ	ნ	k?
<g/>
,	,	kIx,
ts	ts	k0
<g/>
'	'	kIx"
<g/>
minda	minda	k1gFnSc1
Nino	Nina	k1gFnSc5
<g/>
)	)	kIx)
(	(	kIx(
<g/>
okolo	okolo	k7c2
290	#num#	k4
Palestina	Palestina	k1gFnSc1
–	–	k?
okolo	okolo	k7c2
350	#num#	k4
Mccheta	Mccheto	k1gNnSc2
<g/>
,	,	kIx,
Gruzie	Gruzie	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
patronkou	patronka	k1gFnSc7
Gruzie	Gruzie	k1gFnSc1
a	a	k8xC
první	první	k4xOgFnSc7
křesťankou	křesťanka	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
na	na	k7c6
tamní	tamní	k2eAgNnSc4d1
území	území	k1gNnSc4
přinesla	přinést	k5eAaPmAgFnS
křesťanství	křesťanství	k1gNnSc4
<g/>
.	.	kIx.
</s>