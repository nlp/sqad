<p>
<s>
Okarína	okarína	k1gFnSc1	okarína
(	(	kIx(	(
<g/>
z	z	k7c2	z
italského	italský	k2eAgNnSc2d1	italské
ocarina	ocarino	k1gNnSc2	ocarino
-	-	kIx~	-
zdrobněliny	zdrobnělina	k1gFnSc2	zdrobnělina
od	od	k7c2	od
oca	oca	k?	oca
=	=	kIx~	=
husa	husa	k1gFnSc1	husa
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
husička	husička	k1gFnSc1	husička
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
starobylý	starobylý	k2eAgInSc1d1	starobylý
dechový	dechový	k2eAgInSc1d1	dechový
hudební	hudební	k2eAgInSc1d1	hudební
nástroj	nástroj	k1gInSc1	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
různé	různý	k2eAgInPc4d1	různý
typy	typ	k1gInPc4	typ
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
typická	typický	k2eAgFnSc1d1	typická
okarína	okarína	k1gFnSc1	okarína
je	být	k5eAaImIp3nS	být
uzavřený	uzavřený	k2eAgInSc1d1	uzavřený
nástroj	nástroj	k1gInSc1	nástroj
s	s	k7c7	s
čtyřmi	čtyři	k4xCgInPc7	čtyři
až	až	k6eAd1	až
dvanácti	dvanáct	k4xCc7	dvanáct
otvory	otvor	k1gInPc7	otvor
a	a	k8xC	a
náustkem	náustek	k1gInSc7	náustek
<g/>
.	.	kIx.	.
</s>
<s>
Běžně	běžně	k6eAd1	běžně
je	být	k5eAaImIp3nS	být
vyráběna	vyrábět	k5eAaImNgFnS	vyrábět
z	z	k7c2	z
hlíny	hlína	k1gFnSc2	hlína
nebo	nebo	k8xC	nebo
keramiky	keramika	k1gFnSc2	keramika
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dají	dát	k5eAaPmIp3nP	dát
se	se	k3xPyFc4	se
použít	použít	k5eAaPmF	použít
i	i	k9	i
jiné	jiný	k2eAgInPc1d1	jiný
materiály	materiál	k1gInPc1	materiál
jako	jako	k8xS	jako
plast	plast	k1gInSc1	plast
<g/>
,	,	kIx,	,
dřevo	dřevo	k1gNnSc1	dřevo
<g/>
,	,	kIx,	,
kov	kov	k1gInSc1	kov
nebo	nebo	k8xC	nebo
rohovina	rohovina	k1gFnSc1	rohovina
<g/>
.	.	kIx.	.
</s>
<s>
Příklad	příklad	k1gInSc1	příklad
okaríny	okarína	k1gFnSc2	okarína
vyrobené	vyrobený	k2eAgFnSc2d1	vyrobená
z	z	k7c2	z
rohoviny	rohovina	k1gFnSc2	rohovina
je	být	k5eAaImIp3nS	být
starověký	starověký	k2eAgInSc1d1	starověký
německý	německý	k2eAgInSc1d1	německý
gemshorn	gemshorn	k1gInSc1	gemshorn
(	(	kIx(	(
<g/>
nástroj	nástroj	k1gInSc1	nástroj
vyrobený	vyrobený	k2eAgInSc1d1	vyrobený
z	z	k7c2	z
rohu	roh	k1gInSc2	roh
kamzíka	kamzík	k1gMnSc2	kamzík
nebo	nebo	k8xC	nebo
kozy	koza	k1gFnSc2	koza
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Okarína	okarína	k1gFnSc1	okarína
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
starobylé	starobylý	k2eAgFnSc2d1	starobylá
skupiny	skupina	k1gFnSc2	skupina
nástrojů	nástroj	k1gInPc2	nástroj
staré	starý	k2eAgFnSc2d1	stará
až	až	k8xS	až
12	[number]	k4	12
000	[number]	k4	000
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Jí	jíst	k5eAaImIp3nS	jíst
podobné	podobný	k2eAgInPc1d1	podobný
nástroje	nástroj	k1gInPc1	nástroj
měly	mít	k5eAaImAgInP	mít
velký	velký	k2eAgInSc4d1	velký
význam	význam	k1gInSc4	význam
v	v	k7c6	v
čínské	čínský	k2eAgFnSc6d1	čínská
a	a	k8xC	a
mexické	mexický	k2eAgFnSc6d1	mexická
kultuře	kultura	k1gFnSc6	kultura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
hrála	hrát	k5eAaImAgFnS	hrát
okarína	okarína	k1gFnSc1	okarína
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
v	v	k7c6	v
její	její	k3xOp3gFnSc6	její
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
historii	historie	k1gFnSc6	historie
písní	píseň	k1gFnPc2	píseň
a	a	k8xC	a
tanců	tanec	k1gInPc2	tanec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
je	být	k5eAaImIp3nS	být
tradiční	tradiční	k2eAgFnSc1d1	tradiční
okarína	okarína	k1gFnSc1	okarína
známa	znám	k2eAgFnSc1d1	známa
jako	jako	k8xC	jako
tsuchubie	tsuchubie	k1gFnSc1	tsuchubie
(	(	kIx(	(
<g/>
doslova	doslova	k6eAd1	doslova
hliněná	hliněný	k2eAgFnSc1d1	hliněná
flétna	flétna	k1gFnSc1	flétna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Aztékové	Azték	k1gMnPc1	Azték
a	a	k8xC	a
Mayové	May	k1gMnPc1	May
vyráběli	vyrábět	k5eAaImAgMnP	vyrábět
vlastní	vlastní	k2eAgFnPc4d1	vlastní
verze	verze	k1gFnPc4	verze
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byli	být	k5eAaImAgMnP	být
to	ten	k3xDgNnSc4	ten
Aztékové	Azték	k1gMnPc1	Azték
<g/>
,	,	kIx,	,
od	od	k7c2	od
kterých	který	k3yRgMnPc2	který
Evropané	Evropan	k1gMnPc1	Evropan
převzali	převzít	k5eAaPmAgMnP	převzít
písně	píseň	k1gFnPc4	píseň
a	a	k8xC	a
tance	tanec	k1gInPc1	tanec
doprovázené	doprovázený	k2eAgInPc1d1	doprovázený
okarínou	okarína	k1gFnSc7	okarína
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
evropské	evropský	k2eAgFnSc6d1	Evropská
společnosti	společnost	k1gFnSc6	společnost
se	se	k3xPyFc4	se
okarína	okarína	k1gFnSc1	okarína
stala	stát	k5eAaPmAgFnS	stát
populární	populární	k2eAgFnSc7d1	populární
hračkou	hračka	k1gFnSc7	hračka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Moderní	moderní	k2eAgFnSc1d1	moderní
evropská	evropský	k2eAgFnSc1d1	Evropská
okarína	okarína	k1gFnSc1	okarína
se	se	k3xPyFc4	se
datuje	datovat	k5eAaImIp3nS	datovat
do	do	k7c2	do
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Giuseppe	Giusepp	k1gInSc5	Giusepp
Donati	Donat	k1gMnPc1	Donat
z	z	k7c2	z
Budria	Budrium	k1gNnSc2	Budrium
<g/>
,	,	kIx,	,
města	město	k1gNnSc2	město
nedaleko	nedaleko	k7c2	nedaleko
Bologni	Bologna	k1gFnSc6	Bologna
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
přetvořil	přetvořit	k5eAaPmAgMnS	přetvořit
okarínu	okarína	k1gFnSc4	okarína
z	z	k7c2	z
hračky	hračka	k1gFnSc2	hračka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
uměla	umět	k5eAaImAgFnS	umět
hrát	hrát	k5eAaImF	hrát
jen	jen	k9	jen
pár	pár	k4xCyI	pár
tónů	tón	k1gInPc2	tón
<g/>
,	,	kIx,	,
na	na	k7c4	na
více	hodně	k6eAd2	hodně
komplexní	komplexní	k2eAgInSc4d1	komplexní
nástroj	nástroj	k1gInSc4	nástroj
(	(	kIx(	(
<g/>
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xC	jako
první	první	k4xOgFnSc1	první
klasická	klasický	k2eAgFnSc1d1	klasická
okarína	okarína	k1gFnSc1	okarína
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Předchozí	předchozí	k2eAgFnSc1d1	předchozí
forma	forma	k1gFnSc1	forma
okaríny	okarína	k1gFnSc2	okarína
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
byla	být	k5eAaImAgFnS	být
známa	znám	k2eAgFnSc1d1	známa
jako	jako	k8xC	jako
gemshorn	gemshorn	k1gInSc1	gemshorn
(	(	kIx(	(
<g/>
dutý	dutý	k2eAgInSc1d1	dutý
roh	roh	k1gInSc1	roh
zvířete	zvíře	k1gNnSc2	zvíře
s	s	k7c7	s
vyvrtanými	vyvrtaný	k2eAgInPc7d1	vyvrtaný
otvory	otvor	k1gInPc7	otvor
po	po	k7c6	po
straně	strana	k1gFnSc6	strana
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
György	Györg	k1gInPc1	Györg
Ligeti	Liget	k1gMnPc1	Liget
použil	použít	k5eAaPmAgMnS	použít
čtyři	čtyři	k4xCgFnPc4	čtyři
okaríny	okarína	k1gFnPc4	okarína
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
díle	dílo	k1gNnSc6	dílo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hudebníci	hudebník	k1gMnPc1	hudebník
hrající	hrající	k2eAgMnSc1d1	hrající
na	na	k7c4	na
dřevěné	dřevěný	k2eAgInPc4d1	dřevěný
dechové	dechový	k2eAgInPc4d1	dechový
nástroje	nástroj	k1gInPc4	nástroj
podpořili	podpořit	k5eAaPmAgMnP	podpořit
okarínami	okarína	k1gFnPc7	okarína
své	svůj	k3xOyFgInPc4	svůj
vlastní	vlastní	k2eAgInPc4d1	vlastní
nástroje	nástroj	k1gInPc4	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
tohoto	tento	k3xDgNnSc2	tento
díla	dílo	k1gNnSc2	dílo
je	být	k5eAaImIp3nS	být
Violin	Violin	k2eAgInSc1d1	Violin
Concerto	Concerta	k1gFnSc5	Concerta
<g/>
,	,	kIx,	,
Koncert	koncert	k1gInSc1	koncert
pro	pro	k7c4	pro
housle	housle	k1gFnPc4	housle
a	a	k8xC	a
orchestr	orchestr	k1gInSc4	orchestr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
hrála	hrát	k5eAaImAgFnS	hrát
okarína	okarína	k1gFnSc1	okarína
hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
The	The	k1gFnPc2	The
Legend	legenda	k1gFnPc2	legenda
of	of	k?	of
Zelda	Zelda	k1gMnSc1	Zelda
<g/>
:	:	kIx,	:
Ocarina	Ocarina	k1gMnSc1	Ocarina
of	of	k?	of
Time	Tim	k1gFnSc2	Tim
<g/>
,	,	kIx,	,
hře	hra	k1gFnSc3	hra
na	na	k7c4	na
Nintendo	Nintendo	k1gNnSc4	Nintendo
64	[number]	k4	64
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
přivodila	přivodit	k5eAaBmAgFnS	přivodit
značný	značný	k2eAgInSc4d1	značný
zájem	zájem	k1gInSc4	zájem
a	a	k8xC	a
podstatně	podstatně	k6eAd1	podstatně
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
prodej	prodej	k1gInSc4	prodej
tohoto	tento	k3xDgInSc2	tento
nástroje	nástroj	k1gInSc2	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
také	také	k9	také
použita	použít	k5eAaPmNgFnS	použít
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
hrách	hra	k1gFnPc6	hra
série	série	k1gFnSc2	série
The	The	k1gFnSc2	The
Legend	legenda	k1gFnPc2	legenda
of	of	k?	of
Zelda	Zelda	k1gMnSc1	Zelda
(	(	kIx(	(
<g/>
tato	tento	k3xDgFnSc1	tento
původně	původně	k6eAd1	původně
japonská	japonský	k2eAgFnSc1d1	japonská
hra	hra	k1gFnSc1	hra
nemá	mít	k5eNaImIp3nS	mít
českou	český	k2eAgFnSc4d1	Česká
verzi	verze	k1gFnSc4	verze
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Ocarina	Ocarino	k1gNnSc2	Ocarino
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
okarína	okarína	k1gFnSc1	okarína
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
okarína	okarína	k1gFnSc1	okarína
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
