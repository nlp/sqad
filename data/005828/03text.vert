<s>
Jaromír	Jaromír	k1gMnSc1	Jaromír
Nohavica	Nohavica	k1gMnSc1	Nohavica
(	(	kIx(	(
<g/>
*	*	kIx~	*
7	[number]	k4	7
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1953	[number]	k4	1953
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
folkový	folkový	k2eAgMnSc1d1	folkový
písničkář	písničkář	k1gMnSc1	písničkář
<g/>
,	,	kIx,	,
textař	textař	k1gMnSc1	textař
<g/>
,	,	kIx,	,
libretista	libretista	k1gMnSc1	libretista
<g/>
,	,	kIx,	,
osobitý	osobitý	k2eAgMnSc1d1	osobitý
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
,	,	kIx,	,
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
kytarista	kytarista	k1gMnSc1	kytarista
<g/>
.	.	kIx.	.
</s>
<s>
Hraje	hrát	k5eAaImIp3nS	hrát
také	také	k9	také
na	na	k7c4	na
heligonku	heligonka	k1gFnSc4	heligonka
<g/>
.	.	kIx.	.
</s>
<s>
Pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Ostravy	Ostrava	k1gFnSc2	Ostrava
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
píseň	píseň	k1gFnSc4	píseň
napsal	napsat	k5eAaPmAgMnS	napsat
již	již	k6eAd1	již
v	v	k7c6	v
osmi	osm	k4xCc6	osm
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
kytaru	kytara	k1gFnSc4	kytara
hraje	hrát	k5eAaImIp3nS	hrát
od	od	k7c2	od
svých	svůj	k3xOyFgNnPc2	svůj
třinácti	třináct	k4xCc2	třináct
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
původně	původně	k6eAd1	původně
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
chtěl	chtít	k5eAaImAgMnS	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
hrál	hrát	k5eAaImAgMnS	hrát
na	na	k7c4	na
housle	housle	k1gFnPc4	housle
<g/>
,	,	kIx,	,
k	k	k7c3	k
těm	ten	k3xDgMnPc3	ten
ale	ale	k9	ale
nepřilnul	přilnout	k5eNaPmAgInS	přilnout
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
se	se	k3xPyFc4	se
však	však	k9	však
navíc	navíc	k6eAd1	navíc
naučil	naučit	k5eAaPmAgMnS	naučit
hrát	hrát	k5eAaImF	hrát
na	na	k7c4	na
několik	několik	k4yIc4	několik
hudebních	hudební	k2eAgInPc2d1	hudební
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1978	[number]	k4	1978
<g/>
–	–	k?	–
<g/>
1981	[number]	k4	1981
dálkově	dálkově	k6eAd1	dálkově
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c6	na
SŠ	SŠ	kA	SŠ
knihovnické	knihovnický	k2eAgInPc4d1	knihovnický
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgMnS	začít
také	také	k9	také
studovat	studovat	k5eAaImF	studovat
na	na	k7c6	na
Vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
báňské	báňský	k2eAgFnSc6d1	báňská
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
studia	studio	k1gNnPc4	studio
přerušil	přerušit	k5eAaPmAgMnS	přerušit
<g/>
,	,	kIx,	,
vystřídal	vystřídat	k5eAaPmAgMnS	vystřídat
několik	několik	k4yIc4	několik
zaměstnání	zaměstnání	k1gNnPc2	zaměstnání
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
raději	rád	k6eAd2	rád
zůstal	zůstat	k5eAaPmAgMnS	zůstat
volným	volný	k2eAgMnSc7d1	volný
textařem	textař	k1gMnSc7	textař
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
oboru	obor	k1gInSc6	obor
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgMnS	proslavit
textem	text	k1gInSc7	text
Lásko	láska	k1gFnSc5	láska
<g/>
,	,	kIx,	,
voníš	vonět	k5eAaImIp2nS	vonět
deštěm	dešť	k1gInSc7	dešť
pro	pro	k7c4	pro
Marii	Maria	k1gFnSc4	Maria
Rottrovou	Rottrový	k2eAgFnSc4d1	Rottrová
(	(	kIx(	(
<g/>
She	She	k1gFnSc4	She
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Gone	Gone	k1gFnSc7	Gone
od	od	k7c2	od
Black	Blacka	k1gFnPc2	Blacka
Sabbath	Sabbatha	k1gFnPc2	Sabbatha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
psal	psát	k5eAaImAgInS	psát
také	také	k9	také
například	například	k6eAd1	například
pro	pro	k7c4	pro
Věru	Věra	k1gFnSc4	Věra
Špinarovou	Špinarová	k1gFnSc4	Špinarová
atd.	atd.	kA	atd.
Později	pozdě	k6eAd2	pozdě
začal	začít	k5eAaPmAgInS	začít
samostatně	samostatně	k6eAd1	samostatně
vystupovat	vystupovat	k5eAaImF	vystupovat
<g/>
,	,	kIx,	,
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
i	i	k8xC	i
s	s	k7c7	s
kapelou	kapela	k1gFnSc7	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Spolupracoval	spolupracovat	k5eAaImAgInS	spolupracovat
s	s	k7c7	s
méně	málo	k6eAd2	málo
významnými	významný	k2eAgFnPc7d1	významná
regionálními	regionální	k2eAgFnPc7d1	regionální
kapelami	kapela	k1gFnPc7	kapela
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
například	například	k6eAd1	například
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
Čechomor	Čechomora	k1gFnPc2	Čechomora
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
herec	herec	k1gMnSc1	herec
ztvárnil	ztvárnit	k5eAaPmAgMnS	ztvárnit
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
"	"	kIx"	"
<g/>
Jarka	Jarka	k1gFnSc1	Jarka
Nohavici	nohavice	k1gFnSc4	nohavice
<g/>
"	"	kIx"	"
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Petra	Petr	k1gMnSc2	Petr
Zelenky	Zelenka	k1gMnSc2	Zelenka
Rok	rok	k1gInSc1	rok
ďábla	ďábel	k1gMnSc2	ďábel
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
hudbu	hudba	k1gFnSc4	hudba
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
filmu	film	k1gInSc3	film
dostal	dostat	k5eAaPmAgInS	dostat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Čechomorem	Čechomor	k1gMnSc7	Čechomor
a	a	k8xC	a
Karlem	Karel	k1gMnSc7	Karel
Holasem	Holas	k1gMnSc7	Holas
Českého	český	k2eAgInSc2d1	český
lva	lev	k1gInSc2	lev
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
hudbu	hudba	k1gFnSc4	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejpopulárnějších	populární	k2eAgMnPc2d3	nejpopulárnější
zpěváků	zpěvák	k1gMnPc2	zpěvák
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
získal	získat	k5eAaPmAgMnS	získat
i	i	k9	i
některá	některý	k3yIgNnPc4	některý
ocenění	ocenění	k1gNnPc4	ocenění
<g/>
.	.	kIx.	.
</s>
<s>
Časopis	časopis	k1gInSc1	časopis
Rock	rock	k1gInSc1	rock
&	&	k?	&
pop	pop	k1gInSc1	pop
vyhodnotil	vyhodnotit	k5eAaPmAgInS	vyhodnotit
jako	jako	k9	jako
nejlepší	dobrý	k2eAgNnSc4d3	nejlepší
české	český	k2eAgNnSc4d1	české
hudební	hudební	k2eAgNnSc4d1	hudební
album	album	k1gNnSc4	album
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
jeho	jeho	k3xOp3gFnSc4	jeho
Divné	divný	k2eAgNnSc1d1	divné
století	století	k1gNnSc1	století
<g/>
.	.	kIx.	.
</s>
<s>
Akademie	akademie	k1gFnSc1	akademie
populární	populární	k2eAgFnSc2d1	populární
hudby	hudba	k1gFnSc2	hudba
jej	on	k3xPp3gMnSc4	on
zařadila	zařadit	k5eAaPmAgFnS	zařadit
při	při	k7c6	při
vyhlašování	vyhlašování	k1gNnSc6	vyhlašování
výsledků	výsledek	k1gInPc2	výsledek
ceny	cena	k1gFnSc2	cena
Anděl	Anděla	k1gFnPc2	Anděla
2003	[number]	k4	2003
do	do	k7c2	do
Síně	síň	k1gFnSc2	síň
slávy	sláva	k1gFnSc2	sláva
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
nominován	nominovat	k5eAaBmNgMnS	nominovat
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
dalších	další	k2eAgFnPc6d1	další
kategoriích	kategorie	k1gFnPc6	kategorie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
cenách	cena	k1gFnPc6	cena
Anděl	Anděl	k1gMnSc1	Anděl
2009	[number]	k4	2009
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
nejprodávanější	prodávaný	k2eAgNnSc1d3	nejprodávanější
album	album	k1gNnSc1	album
a	a	k8xC	a
v	v	k7c6	v
Anděl	Anděla	k1gFnPc2	Anděla
2010	[number]	k4	2010
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
zpěvákem	zpěvák	k1gMnSc7	zpěvák
dvacetiletí	dvacetiletí	k1gNnSc2	dvacetiletí
<g/>
.	.	kIx.	.
</s>
<s>
Bydlí	bydlet	k5eAaImIp3nS	bydlet
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
<g/>
,	,	kIx,	,
předtím	předtím	k6eAd1	předtím
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
Českém	český	k2eAgInSc6d1	český
Těšíně	Těšín	k1gInSc6	Těšín
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
ženu	žena	k1gFnSc4	žena
Martinu	Martina	k1gFnSc4	Martina
a	a	k8xC	a
dvě	dva	k4xCgFnPc4	dva
děti	dítě	k1gFnPc4	dítě
–	–	k?	–
syna	syn	k1gMnSc2	syn
Jakuba	Jakub	k1gMnSc2	Jakub
a	a	k8xC	a
dceru	dcera	k1gFnSc4	dcera
Lenku	Lenka	k1gFnSc4	Lenka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
získal	získat	k5eAaPmAgMnS	získat
titul	titul	k1gInSc4	titul
mistra	mistr	k1gMnSc2	mistr
republiky	republika	k1gFnSc2	republika
ve	v	k7c6	v
stolní	stolní	k2eAgFnSc6d1	stolní
hře	hra	k1gFnSc6	hra
scrabble	scrabble	k6eAd1	scrabble
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
literárním	literární	k2eAgMnSc7d1	literární
oblíbencem	oblíbenec	k1gMnSc7	oblíbenec
je	být	k5eAaImIp3nS	být
Bohumil	Bohumil	k1gMnSc1	Bohumil
Hrabal	Hrabal	k1gMnSc1	Hrabal
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2008	[number]	k4	2008
bylo	být	k5eAaImAgNnS	být
vydáno	vydán	k2eAgNnSc1d1	vydáno
album	album	k1gNnSc1	album
Ikarus	Ikarus	k1gMnSc1	Ikarus
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
nahráno	nahrát	k5eAaBmNgNnS	nahrát
na	na	k7c6	na
osmi	osm	k4xCc6	osm
koncertech	koncert	k1gInPc6	koncert
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
odtajnění	odtajnění	k1gNnSc4	odtajnění
spolupracovníků	spolupracovník	k1gMnPc2	spolupracovník
StB	StB	k1gMnPc2	StB
se	se	k3xPyFc4	se
zjistilo	zjistit	k5eAaPmAgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
registrech	registr	k1gInPc6	registr
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1986	[number]	k4	1986
veden	veden	k2eAgMnSc1d1	veden
pod	pod	k7c7	pod
krycím	krycí	k2eAgNnSc7d1	krycí
jménem	jméno	k1gNnSc7	jméno
"	"	kIx"	"
<g/>
Mirek	Mirek	k1gMnSc1	Mirek
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
k	k	k7c3	k
vynucené	vynucený	k2eAgFnSc3d1	vynucená
spolupráci	spolupráce	k1gFnSc3	spolupráce
se	se	k3xPyFc4	se
Nohavica	Nohavica	k1gMnSc1	Nohavica
později	pozdě	k6eAd2	pozdě
přiznal	přiznat	k5eAaPmAgMnS	přiznat
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
webu	web	k1gInSc6	web
reagoval	reagovat	k5eAaBmAgMnS	reagovat
na	na	k7c4	na
zprávu	zpráva	k1gFnSc4	zpráva
a	a	k8xC	a
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nikoho	nikdo	k3yNnSc4	nikdo
neudal	udat	k5eNaPmAgMnS	udat
a	a	k8xC	a
že	že	k8xS	že
StB	StB	k1gFnSc1	StB
všechna	všechen	k3xTgNnPc1	všechen
jména	jméno	k1gNnPc1	jméno
znala	znát	k5eAaImAgNnP	znát
<g/>
.	.	kIx.	.
</s>
<s>
Nalezená	nalezený	k2eAgFnSc1d1	nalezená
část	část	k1gFnSc1	část
souvisejícího	související	k2eAgInSc2d1	související
spisu	spis	k1gInSc2	spis
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Nohavica	Nohavica	k1gMnSc1	Nohavica
poskytoval	poskytovat	k5eAaImAgMnS	poskytovat
i	i	k9	i
informace	informace	k1gFnPc4	informace
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
sice	sice	k8xC	sice
poměrně	poměrně	k6eAd1	poměrně
banální	banální	k2eAgNnPc1d1	banální
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
podrobné	podrobný	k2eAgNnSc1d1	podrobné
a	a	k8xC	a
StB	StB	k1gFnSc1	StB
je	být	k5eAaImIp3nS	být
všechny	všechen	k3xTgInPc4	všechen
znát	znát	k5eAaImF	znát
nemusela	muset	k5eNaImAgFnS	muset
<g/>
;	;	kIx,	;
Nohavica	Nohavic	k2eAgFnSc1d1	Nohavica
přesnost	přesnost	k1gFnSc1	přesnost
zápisu	zápis	k1gInSc2	zápis
zpochybňuje	zpochybňovat	k5eAaImIp3nS	zpochybňovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2007	[number]	k4	2007
média	médium	k1gNnSc2	médium
téma	téma	k1gNnSc1	téma
Nohavicovy	Nohavicův	k2eAgFnSc2d1	Nohavicova
spolupráce	spolupráce	k1gFnSc2	spolupráce
s	s	k7c7	s
StB	StB	k1gFnSc7	StB
znovu	znovu	k6eAd1	znovu
otevřela	otevřít	k5eAaPmAgFnS	otevřít
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
písní	píseň	k1gFnSc7	píseň
Udavač	udavač	k1gMnSc1	udavač
z	z	k7c2	z
Těšína	Těšín	k1gInSc2	Těšín
od	od	k7c2	od
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Hutky	Hutka	k1gMnSc2	Hutka
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yIgNnSc4	který
a	a	k8xC	a
v	v	k7c6	v
komentářích	komentář	k1gInPc6	komentář
k	k	k7c3	k
níž	jenž	k3xRgFnSc3	jenž
Hutka	Hutka	k1gMnSc1	Hutka
odsuzuje	odsuzovat	k5eAaImIp3nS	odsuzovat
Nohavicovu	Nohavicův	k2eAgFnSc4d1	Nohavicova
spolupráci	spolupráce	k1gFnSc4	spolupráce
a	a	k8xC	a
konkrétně	konkrétně	k6eAd1	konkrétně
donášení	donášení	k1gNnSc2	donášení
na	na	k7c4	na
Karla	Karel	k1gMnSc4	Karel
Kryla	krýt	k5eAaImAgFnS	krýt
po	po	k7c6	po
jejich	jejich	k3xOp3gNnSc6	jejich
setkání	setkání	k1gNnSc6	setkání
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1989	[number]	k4	1989
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
a	a	k8xC	a
také	také	k9	také
donášel	donášet	k5eAaImAgMnS	donášet
na	na	k7c4	na
spisovatele	spisovatel	k1gMnSc4	spisovatel
Pavla	Pavel	k1gMnSc4	Pavel
Kohouta	Kohout	k1gMnSc4	Kohout
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Nohavica	Nohavica	k1gMnSc1	Nohavica
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
minulostí	minulost	k1gFnSc7	minulost
nedostatečně	dostatečně	k6eNd1	dostatečně
veřejně	veřejně	k6eAd1	veřejně
vyrovnal	vyrovnat	k5eAaPmAgMnS	vyrovnat
<g/>
.	.	kIx.	.
</s>
<s>
Nohavica	Nohavica	k6eAd1	Nohavica
však	však	k9	však
s	s	k7c7	s
novináři	novinář	k1gMnPc7	novinář
hovoří	hovořit	k5eAaImIp3nS	hovořit
jen	jen	k9	jen
výjimečně	výjimečně	k6eAd1	výjimečně
<g/>
,	,	kIx,	,
po	po	k7c6	po
"	"	kIx"	"
<g/>
udavačské	udavačský	k2eAgFnSc6d1	udavačská
<g/>
"	"	kIx"	"
aféře	aféra	k1gFnSc6	aféra
dokonce	dokonce	k9	dokonce
vzkázal	vzkázat	k5eAaPmAgMnS	vzkázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
tiskem	tisk	k1gInSc7	tisk
komunikovat	komunikovat	k5eAaImF	komunikovat
už	už	k6eAd1	už
nikdy	nikdy	k6eAd1	nikdy
nebude	být	k5eNaImBp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
hlavní	hlavní	k2eAgNnSc1d1	hlavní
komunikační	komunikační	k2eAgNnSc1d1	komunikační
médium	médium	k1gNnSc1	médium
pro	pro	k7c4	pro
kontakt	kontakt	k1gInSc4	kontakt
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
fanoušky	fanoušek	k1gMnPc7	fanoušek
využívá	využívat	k5eAaImIp3nS	využívat
internet	internet	k1gInSc1	internet
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
webu	web	k1gInSc6	web
zveřejnil	zveřejnit	k5eAaPmAgInS	zveřejnit
volně	volně	k6eAd1	volně
ke	k	k7c3	k
stažení	stažení	k1gNnSc3	stažení
MP3	MP3	k1gMnPc2	MP3
album	album	k1gNnSc4	album
Pražská	pražský	k2eAgFnSc1d1	Pražská
pálená	pálená	k1gFnSc1	pálená
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
i	i	k9	i
na	na	k7c6	na
sklonku	sklonek	k1gInSc6	sklonek
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
mini	mini	k2eAgInSc2d1	mini
CD	CD	kA	CD
Od	od	k7c2	od
Jarka	Jarek	k1gMnSc2	Jarek
pod	pod	k7c4	pod
stromeček	stromeček	k1gInSc4	stromeček
<g/>
.	.	kIx.	.
</s>
<s>
Pravidelně	pravidelně	k6eAd1	pravidelně
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
webu	web	k1gInSc6	web
zveřejňoval	zveřejňovat	k5eAaImAgMnS	zveřejňovat
každé	každý	k3xTgNnSc4	každý
pondělí	pondělí	k1gNnSc4	pondělí
jednu	jeden	k4xCgFnSc4	jeden
píseň	píseň	k1gFnSc4	píseň
popsanou	popsaný	k2eAgFnSc7d1	popsaná
dobovými	dobový	k2eAgInPc7d1	dobový
audio	audio	k2eAgInPc7d1	audio
a	a	k8xC	a
video	video	k1gNnSc4	video
ukázkami	ukázka	k1gFnPc7	ukázka
<g/>
,	,	kIx,	,
fotografiemi	fotografia	k1gFnPc7	fotografia
a	a	k8xC	a
zejména	zejména	k9	zejména
komentářem	komentář	k1gInSc7	komentář
mapujícím	mapující	k2eAgInSc7d1	mapující
na	na	k7c6	na
pozadí	pozadí	k1gNnSc6	pozadí
dobových	dobový	k2eAgFnPc2d1	dobová
souvislostí	souvislost	k1gFnPc2	souvislost
celou	celý	k2eAgFnSc4d1	celá
svou	svůj	k3xOyFgFnSc4	svůj
tvorbu	tvorba	k1gFnSc4	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Časem	časem	k6eAd1	časem
se	se	k3xPyFc4	se
však	však	k9	však
tato	tento	k3xDgFnSc1	tento
činnost	činnost	k1gFnSc1	činnost
stala	stát	k5eAaPmAgFnS	stát
pouze	pouze	k6eAd1	pouze
příležitostnou	příležitostný	k2eAgFnSc4d1	příležitostná
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhodobý	dlouhodobý	k2eAgInSc1d1	dlouhodobý
a	a	k8xC	a
jedinečný	jedinečný	k2eAgInSc1d1	jedinečný
projekt	projekt	k1gInSc1	projekt
nese	nést	k5eAaImIp3nS	nést
název	název	k1gInSc1	název
Archiv	archiv	k1gInSc1	archiv
pod	pod	k7c7	pod
lupou	lupa	k1gFnSc7	lupa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
vydal	vydat	k5eAaPmAgInS	vydat
a	a	k8xC	a
přes	přes	k7c4	přes
internet	internet	k1gInSc4	internet
distribuoval	distribuovat	k5eAaBmAgInS	distribuovat
DVD	DVD	kA	DVD
z	z	k7c2	z
benefičního	benefiční	k2eAgInSc2d1	benefiční
adventního	adventní	k2eAgInSc2d1	adventní
koncertu	koncert	k1gInSc2	koncert
<g/>
,	,	kIx,	,
použil	použít	k5eAaPmAgInS	použít
protokol	protokol	k1gInSc1	protokol
BitTorrent	BitTorrent	k1gInSc1	BitTorrent
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
píseň	píseň	k1gFnSc1	píseň
Pane	Pan	k1gMnSc5	Pan
prezidente	prezident	k1gMnSc5	prezident
(	(	kIx(	(
<g/>
nářek	nářek	k1gInSc1	nářek
důchodce	důchodce	k1gMnSc2	důchodce
nad	nad	k7c7	nad
změnami	změna	k1gFnPc7	změna
v	v	k7c6	v
životě	život	k1gInSc6	život
<g/>
)	)	kIx)	)
nazpívali	nazpívat	k5eAaBmAgMnP	nazpívat
Žamboši	Žamboš	k1gMnPc1	Žamboš
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Pane	Pan	k1gMnSc5	Pan
Nohavico	Nohavico	k6eAd1	Nohavico
a	a	k8xC	a
stěžují	stěžovat	k5eAaImIp3nP	stěžovat
si	se	k3xPyFc3	se
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
na	na	k7c4	na
těžký	těžký	k2eAgInSc4d1	těžký
úděl	úděl	k1gInSc4	úděl
folkového	folkový	k2eAgMnSc2d1	folkový
písničkáře	písničkář	k1gMnSc2	písničkář
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
velký	velký	k2eAgMnSc1d1	velký
fanoušek	fanoušek	k1gMnSc1	fanoušek
fotbalového	fotbalový	k2eAgInSc2d1	fotbalový
klubu	klub	k1gInSc2	klub
FC	FC	kA	FC
Baník	Baník	k1gInSc1	Baník
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
,	,	kIx,	,
nazpíval	nazpívat	k5eAaPmAgMnS	nazpívat
také	také	k9	také
klubovou	klubový	k2eAgFnSc4d1	klubová
hymnu	hymna	k1gFnSc4	hymna
"	"	kIx"	"
<g/>
Baníčku	Baníček	k1gInSc2	Baníček
my	my	k3xPp1nPc1	my
jsme	být	k5eAaImIp1nP	být
s	s	k7c7	s
tebou	ty	k3xPp2nSc7	ty
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
honorárním	honorární	k2eAgMnSc7d1	honorární
členem	člen	k1gMnSc7	člen
občanského	občanský	k2eAgNnSc2d1	občanské
sdružení	sdružení	k1gNnSc2	sdružení
Baník	Baník	k1gInSc1	Baník
Baníku	Baník	k1gInSc2	Baník
<g/>
.	.	kIx.	.
</s>
<s>
Zpívá	zpívat	k5eAaImIp3nS	zpívat
především	především	k9	především
vlastní	vlastní	k2eAgFnPc4d1	vlastní
písně	píseň	k1gFnPc4	píseň
<g/>
,	,	kIx,	,
také	také	k9	také
však	však	k9	však
překlady	překlad	k1gInPc4	překlad
písní	píseň	k1gFnPc2	píseň
ruských	ruský	k2eAgMnPc2d1	ruský
autorů	autor	k1gMnPc2	autor
Vladimira	Vladimir	k1gInSc2	Vladimir
Vysockého	vysocký	k2eAgMnSc2d1	vysocký
nebo	nebo	k8xC	nebo
Bulata	Bulat	k1gMnSc2	Bulat
Okudžavy	Okudžava	k1gFnSc2	Okudžava
–	–	k?	–
sám	sám	k3xTgMnSc1	sám
se	se	k3xPyFc4	se
mnohokrát	mnohokrát	k6eAd1	mnohokrát
zmínil	zmínit	k5eAaPmAgMnS	zmínit
o	o	k7c6	o
svém	svůj	k3xOyFgNnSc6	svůj
tíhnutí	tíhnutí	k1gNnSc6	tíhnutí
k	k	k7c3	k
ruským	ruský	k2eAgFnPc3d1	ruská
inspiracím	inspirace	k1gFnPc3	inspirace
<g/>
.	.	kIx.	.
</s>
<s>
Zhudebnil	zhudebnit	k5eAaPmAgMnS	zhudebnit
a	a	k8xC	a
nazpíval	nazpívat	k5eAaPmAgMnS	nazpívat
některé	některý	k3yIgFnPc4	některý
básně	báseň	k1gFnPc4	báseň
ze	z	k7c2	z
Slezských	slezský	k2eAgFnPc2d1	Slezská
písní	píseň	k1gFnPc2	píseň
Petra	Petr	k1gMnSc2	Petr
Bezruče	Bezruč	k1gInSc2	Bezruč
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
zhudebnil	zhudebnit	k5eAaPmAgMnS	zhudebnit
báseň	báseň	k1gFnSc4	báseň
"	"	kIx"	"
<g/>
Dezertér	dezertér	k1gMnSc1	dezertér
<g/>
"	"	kIx"	"
ze	z	k7c2	z
sbírky	sbírka	k1gFnSc2	sbírka
Jiřího	Jiří	k1gMnSc2	Jiří
Šotoly	Šotola	k1gMnSc2	Šotola
"	"	kIx"	"
<g/>
Za	za	k7c4	za
život	život	k1gInSc4	život
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
písně	píseň	k1gFnPc1	píseň
jsou	být	k5eAaImIp3nP	být
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
oblíbené	oblíbený	k2eAgNnSc4d1	oblíbené
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
zpívá	zpívat	k5eAaImIp3nS	zpívat
o	o	k7c6	o
obyčejném	obyčejný	k2eAgInSc6d1	obyčejný
životě	život	k1gInSc6	život
a	a	k8xC	a
o	o	k7c6	o
člověku	člověk	k1gMnSc6	člověk
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
lyricko-epické	lyrickopický	k2eAgFnPc1d1	lyricko-epická
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
však	však	k9	však
i	i	k9	i
ironizující	ironizující	k2eAgMnSc1d1	ironizující
<g/>
.	.	kIx.	.
</s>
<s>
Nohavica	Nohavica	k6eAd1	Nohavica
rád	rád	k6eAd1	rád
používá	používat	k5eAaImIp3nS	používat
nářečí	nářečí	k1gNnSc4	nářečí
z	z	k7c2	z
Ostravska	Ostravsko	k1gNnSc2	Ostravsko
a	a	k8xC	a
okolí	okolí	k1gNnSc2	okolí
Těšína	Těšín	k1gInSc2	Těšín
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
písničkářské	písničkářský	k2eAgFnSc2d1	písničkářská
tvorby	tvorba	k1gFnSc2	tvorba
překládá	překládat	k5eAaImIp3nS	překládat
Mozartovy	Mozartův	k2eAgFnSc2d1	Mozartova
opery	opera	k1gFnSc2	opera
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
obdržel	obdržet	k5eAaPmAgInS	obdržet
za	za	k7c4	za
český	český	k2eAgInSc4d1	český
překlad	překlad	k1gInSc4	překlad
opery	opera	k1gFnSc2	opera
Cosi	cosi	k3yInSc4	cosi
fan	fana	k1gFnPc2	fana
tutte	tutit	k5eAaImRp2nP	tutit
Cenu	cena	k1gFnSc4	cena
Sazky	Sazka	k1gFnSc2	Sazka
a	a	k8xC	a
Divadelních	divadelní	k2eAgFnPc2d1	divadelní
novin	novina	k1gFnPc2	novina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
přeložil	přeložit	k5eAaPmAgMnS	přeložit
operu	opera	k1gFnSc4	opera
Don	Don	k1gMnSc1	Don
Giovanni	Giovann	k1gMnPc1	Giovann
<g/>
.	.	kIx.	.
</s>
<s>
Darmoděj	darmoděj	k1gMnSc1	darmoděj
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
–	–	k?	–
LP	LP	kA	LP
Osmá	osmý	k4xOgFnSc1	osmý
barva	barva	k1gFnSc1	barva
duhy	duha	k1gFnSc2	duha
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
–	–	k?	–
MC	MC	kA	MC
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
–	–	k?	–
CD	CD	kA	CD
<g/>
)	)	kIx)	)
V	v	k7c6	v
tom	ten	k3xDgInSc6	ten
roce	rok	k1gInSc6	rok
pitomém	pitomý	k2eAgInSc6d1	pitomý
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
Mikymauzoleum	Mikymauzoleum	k1gInSc1	Mikymauzoleum
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
Tři	tři	k4xCgMnPc1	tři
čuníci	čuník	k1gMnPc1	čuník
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
Darmoděj	darmoděj	k1gMnSc1	darmoděj
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
Divné	divný	k2eAgNnSc1d1	divné
století	století	k1gNnSc1	století
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
Koncert	koncert	k1gInSc1	koncert
–	–	k?	–
s	s	k7c7	s
Kapelou	kapela	k1gFnSc7	kapela
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
3	[number]	k4	3
<g/>
×	×	k?	×
Jarek	Jarek	k1gMnSc1	Jarek
Nohavica	Nohavica	k1gMnSc1	Nohavica
–	–	k?	–
reedice	reedice	k1gFnSc1	reedice
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
Moje	můj	k3xOp1gNnSc1	můj
smutné	smutný	k2eAgNnSc1d1	smutné
srdce	srdce	k1gNnSc1	srdce
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
Babylon	Babylon	k1gInSc1	Babylon
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
Pražská	pražský	k2eAgFnSc1d1	Pražská
pálená	pálená	k1gFnSc1	pálená
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
–	–	k?	–
(	(	kIx(	(
<g/>
volně	volně	k6eAd1	volně
ke	k	k7c3	k
stažení	stažení	k1gNnSc3	stažení
<g/>
)	)	kIx)	)
Doma	doma	k6eAd1	doma
CD	CD	kA	CD
<g/>
+	+	kIx~	+
<g/>
DVD	DVD	kA	DVD
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
Od	od	k7c2	od
Jarka	Jarek	k1gMnSc2	Jarek
pod	pod	k7c4	pod
stromeček	stromeček	k1gInSc4	stromeček
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
–	–	k?	–
(	(	kIx(	(
<g/>
volně	volně	k6eAd1	volně
ke	k	k7c3	k
stažení	stažení	k1gNnSc3	stažení
<g/>
)	)	kIx)	)
Box	box	k1gInSc4	box
4	[number]	k4	4
CD	CD	kA	CD
–	–	k?	–
reedice	reedice	k1gFnSc1	reedice
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
Ikarus	Ikarus	k1gMnSc1	Ikarus
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
Z	z	k7c2	z
pódia	pódium	k1gNnSc2	pódium
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
album	album	k1gNnSc1	album
je	být	k5eAaImIp3nS	být
zdarma	zdarma	k6eAd1	zdarma
ke	k	k7c3	k
stažení	stažení	k1gNnSc3	stažení
jako	jako	k8xS	jako
torrent	torrent	k1gInSc1	torrent
<g/>
.	.	kIx.	.
</s>
<s>
Jarek	Jarek	k1gMnSc1	Jarek
Nohavica	Nohavica	k1gMnSc1	Nohavica
jej	on	k3xPp3gMnSc4	on
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
jako	jako	k8xS	jako
dar	dar	k1gInSc4	dar
svým	svůj	k3xOyFgMnPc3	svůj
fanouškům	fanoušek	k1gMnPc3	fanoušek
k	k	k7c3	k
Vánocům	Vánoce	k1gFnPc3	Vánoce
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
V	v	k7c6	v
Lucerně	lucerna	k1gFnSc6	lucerna
CD	CD	kA	CD
<g/>
+	+	kIx~	+
<g/>
DVD	DVD	kA	DVD
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
také	také	k9	také
na	na	k7c4	na
Blu-ray	Blua	k2eAgFnPc4d1	Blu-ra
Virtuálky	Virtuálka	k1gFnPc4	Virtuálka
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
–	–	k?	–
(	(	kIx(	(
<g/>
volně	volně	k6eAd1	volně
ke	k	k7c3	k
stažení	stažení	k1gNnSc3	stažení
<g/>
)	)	kIx)	)
Platinová	platinový	k2eAgFnSc1d1	platinová
kolekce	kolekce	k1gFnSc1	kolekce
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
–	–	k?	–
reedice	reedice	k1gFnSc1	reedice
Virtuálky	Virtuálka	k1gFnSc2	Virtuálka
2	[number]	k4	2
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
–	–	k?	–
(	(	kIx(	(
<g/>
volně	volně	k6eAd1	volně
ke	k	k7c3	k
stažení	stažení	k1gNnSc3	stažení
<g/>
)	)	kIx)	)
Adventní	adventní	k2eAgInSc1d1	adventní
koncert	koncert	k1gInSc1	koncert
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
–	–	k?	–
(	(	kIx(	(
<g/>
volně	volně	k6eAd1	volně
ke	k	k7c3	k
stažení	stažení	k1gNnSc3	stažení
jako	jako	k8xS	jako
torrent	torrent	k1gInSc1	torrent
<g/>
)	)	kIx)	)
Virtuálky	Virtuálka	k1gFnSc2	Virtuálka
3	[number]	k4	3
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
–	–	k?	–
(	(	kIx(	(
<g/>
volně	volně	k6eAd1	volně
ke	k	k7c3	k
stažení	stažení	k1gNnSc3	stažení
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Tak	tak	k9	tak
mě	já	k3xPp1nSc4	já
tu	tu	k6eAd1	tu
máš	mít	k5eAaImIp2nS	mít
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
Koncerty	koncert	k1gInPc1	koncert
1982	[number]	k4	1982
a	a	k8xC	a
1984	[number]	k4	1984
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
Půlnoční	půlnoční	k2eAgInSc1d1	půlnoční
trolejbus	trolejbus	k1gInSc1	trolejbus
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
–	–	k?	–
(	(	kIx(	(
<g/>
volně	volně	k6eAd1	volně
ke	k	k7c3	k
stažení	stažení	k1gNnSc3	stažení
jako	jako	k8xS	jako
torrent	torrent	k1gInSc1	torrent
<g/>
)	)	kIx)	)
Tenkrát	tenkrát	k6eAd1	tenkrát
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
–	–	k?	–
kompilace	kompilace	k1gFnSc2	kompilace
k	k	k7c3	k
60	[number]	k4	60
<g/>
.	.	kIx.	.
narozeninám	narozeniny	k1gFnPc3	narozeniny
Kometa	kometa	k1gFnSc1	kometa
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
–	–	k?	–
the	the	k?	the
best	best	k1gMnSc1	best
of	of	k?	of
Jaromír	Jaromír	k1gMnSc1	Jaromír
Nohavica	Nohavica	k1gMnSc1	Nohavica
Jarek	Jarek	k1gMnSc1	Jarek
Nohavica	Nohavica	k1gMnSc1	Nohavica
a	a	k8xC	a
přátelé	přítel	k1gMnPc1	přítel
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
Cesty	cesta	k1gFnSc2	cesta
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
–	–	k?	–
EP	EP	kA	EP
Písně	píseň	k1gFnSc2	píseň
pro	pro	k7c4	pro
V.	V.	kA	V.
V.	V.	kA	V.
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
–	–	k?	–
2EP	[number]	k4	2EP
Folkové	folkový	k2eAgFnPc1d1	folková
Vánoce	Vánoce	k1gFnPc1	Vánoce
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Folkfórum	Folkfórum	k1gInSc1	Folkfórum
–	–	k?	–
spolu	spolu	k6eAd1	spolu
<g/>
...	...	k?	...
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
Morava	Morava	k1gFnSc1	Morava
'	'	kIx"	'
<g/>
91	[number]	k4	91
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
Folkový	folkový	k2eAgInSc1d1	folkový
kolotoč	kolotoč	k1gInSc1	kolotoč
I.	I.	kA	I.
Šťastné	Šťastná	k1gFnSc2	Šťastná
a	a	k8xC	a
veselé	veselá	k1gFnSc2	veselá
<g/>
...	...	k?	...
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
Česká	český	k2eAgFnSc1d1	Česká
mše	mše	k1gFnSc1	mše
vánoční	vánoční	k2eAgFnSc1d1	vánoční
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
Vita	vit	k2eAgFnSc1d1	Vita
'	'	kIx"	'
<g/>
93	[number]	k4	93
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Hudba	hudba	k1gFnSc1	hudba
bez	bez	k7c2	bez
bariér	bariéra	k1gFnPc2	bariéra
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
Králíci	Králík	k1gMnPc1	Králík
<g/>
,	,	kIx,	,
ptáci	pták	k1gMnPc1	pták
a	a	k8xC	a
hvězdy	hvězda	k1gFnPc1	hvězda
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
Pavlína	Pavlína	k1gFnSc1	Pavlína
Jíšová	Jíšová	k1gFnSc1	Jíšová
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
Neřež	řezat	k5eNaImRp2nS	řezat
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
Sloni	sloň	k1gFnSc2wR	sloň
v	v	k7c6	v
porcelánu	porcelán	k1gInSc6	porcelán
I.	I.	kA	I.
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
Sešli	sejít	k5eAaPmAgMnP	sejít
se	se	k3xPyFc4	se
I.	I.	kA	I.
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
Rok	rok	k1gInSc1	rok
<g />
.	.	kIx.	.
</s>
<s>
ďábla	ďábel	k1gMnSc2	ďábel
(	(	kIx(	(
<g/>
Soundtrack	soundtrack	k1gInSc1	soundtrack
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
Romeo	Romeo	k1gMnSc1	Romeo
a	a	k8xC	a
Julie	Julie	k1gFnSc1	Julie
-	-	kIx~	-
muzikál	muzikál	k1gInSc1	muzikál
na	na	k7c6	na
ledě	led	k1gInSc6	led
(	(	kIx(	(
<g/>
Soundtrack	soundtrack	k1gInSc1	soundtrack
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
Těšínské	Těšínské	k2eAgFnSc2d1	Těšínské
niebo	nieba	k1gFnSc5	nieba
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
Havěť	havěť	k1gFnSc1	havěť
všelijaká	všelijaký	k3yIgFnSc1	všelijaký
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
Strážce	strážce	k1gMnSc1	strážce
plamene	plamen	k1gInSc2	plamen
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
50	[number]	k4	50
<g />
.	.	kIx.	.
</s>
<s>
miniatur	miniatura	k1gFnPc2	miniatura
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
Moravský	moravský	k2eAgInSc1d1	moravský
folk	folk	k1gInSc1	folk
v	v	k7c6	v
Lucerně	lucerna	k1gFnSc6	lucerna
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
Inzerát	inzerát	k1gInSc1	inzerát
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
Wabi	Wab	k1gFnSc2	Wab
&	&	k?	&
Ďáblovo	ďáblův	k2eAgNnSc1d1	ďáblovo
stádo	stádo	k1gNnSc1	stádo
–	–	k?	–
Příběhy	příběh	k1gInPc1	příběh
písní	píseň	k1gFnPc2	píseň
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
Hity	hit	k1gInPc1	hit
folk	folk	k1gInSc1	folk
&	&	k?	&
country	country	k2eAgInSc1d1	country
1998	[number]	k4	1998
Nejhezčí	hezký	k2eAgFnPc1d3	nejhezčí
folkové	folkový	k2eAgFnPc1d1	folková
písničky	písnička	k1gFnPc1	písnička
Barvy	barva	k1gFnSc2	barva
českého	český	k2eAgInSc2d1	český
folku	folk	k1gInSc2	folk
Nejhezčí	hezký	k2eAgFnSc2d3	nejhezčí
folkové	folkový	k2eAgFnSc2d1	folková
písničky	písnička	k1gFnSc2	písnička
2	[number]	k4	2
České	český	k2eAgInPc1d1	český
hity	hit	k1gInPc1	hit
<g />
.	.	kIx.	.
</s>
<s>
90	[number]	k4	90
<g/>
-tých	ýcha	k1gFnPc2	-týcha
let	let	k1gInSc4	let
Zvláštní	zvláštní	k2eAgFnSc2d1	zvláštní
bytosti	bytost	k1gFnSc2	bytost
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
Rok	rok	k1gInSc1	rok
ďábla	ďábel	k1gMnSc2	ďábel
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
Koncert	koncert	k1gInSc1	koncert
Jaromíra	Jaromír	k1gMnSc4	Jaromír
Nohavici	nohavice	k1gFnSc6	nohavice
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
Darmoděj	darmoděj	k1gMnSc1	darmoděj
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
Šťastné	Šťastné	k2eAgInPc1d1	Šťastné
a	a	k8xC	a
veselé	veselý	k2eAgInPc1d1	veselý
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
Bílá	bílý	k2eAgNnPc4d1	bílé
místa	místo	k1gNnPc4	místo
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
Kdo	kdo	k3yInSc1	kdo
je	být	k5eAaImIp3nS	být
kdo	kdo	k3yInSc1	kdo
-	-	kIx~	-
Jarek	Jarek	k1gMnSc1	Jarek
Nohavica	Nohavica	k1gMnSc1	Nohavica
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
Sešli	sejít	k5eAaPmAgMnP	sejít
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
Vzpomínka	vzpomínka	k1gFnSc1	vzpomínka
na	na	k7c4	na
Karla	Karel	k1gMnSc4	Karel
Kryla	Kryl	k1gMnSc4	Kryl
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
Jak	jak	k8xS	jak
se	se	k3xPyFc4	se
žije	žít	k5eAaImIp3nS	žít
zpěvnému	zpěvný	k2eAgNnSc3d1	zpěvné
svědomí	svědomí	k1gNnSc3	svědomí
národa	národ	k1gInSc2	národ
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
Divné	divný	k2eAgNnSc4d1	divné
století	století	k1gNnSc4	století
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
Jaromir	Jaromir	k1gMnSc1	Jaromir
Nohavica	Nohavica	k1gMnSc1	Nohavica
i	i	k8xC	i
Karel	Karel	k1gMnSc1	Karel
Plihal	Plihal	k1gMnSc1	Plihal
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
–	–	k?	–
Polsko	Polsko	k1gNnSc4	Polsko
Bulat	bulat	k5eAaImF	bulat
Okudžava	Okudžava	k1gFnSc1	Okudžava
-	-	kIx~	-
Věčná	věčný	k2eAgFnSc1d1	věčná
píseň	píseň	k1gFnSc1	píseň
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
Ahoj	ahoj	k0	ahoj
<g/>
,	,	kIx,	,
Ostravo	Ostrava	k1gFnSc5	Ostrava
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
–	–	k?	–
záznam	záznam	k1gInSc4	záznam
koncertu	koncert	k1gInSc2	koncert
Básníci	básník	k1gMnPc1	básník
Live	Live	k1gInSc4	Live
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
Poprvé	poprvé	k6eAd1	poprvé
<g/>
,	,	kIx,	,
aneb	aneb	k?	aneb
střípky	střípek	k1gInPc1	střípek
ze	z	k7c2	z
života	život	k1gInSc2	život
J.	J.	kA	J.
N.	N.	kA	N.
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
Jarek	Jarka	k1gFnPc2	Jarka
a	a	k8xC	a
Amadeus	Amadeus	k1gMnSc1	Amadeus
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
Strážce	strážce	k1gMnSc1	strážce
plamene	plamen	k1gInSc2	plamen
v	v	k7c6	v
obrazech	obraz	k1gInPc6	obraz
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
20	[number]	k4	20
<g/>
ers	ers	k?	ers
-	-	kIx~	-
Making	Making	k1gInSc1	Making
Of	Of	k1gFnSc2	Of
<g/>
...	...	k?	...
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
Zašlapané	zašlapaný	k2eAgInPc1d1	zašlapaný
projekty	projekt	k1gInPc1	projekt
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
2009	[number]	k4	2009
–	–	k?	–
Nohavica	Nohavica	k1gMnSc1	Nohavica
jako	jako	k8xS	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
řečníků	řečník	k1gMnPc2	řečník
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
o	o	k7c6	o
Folkovém	folkový	k2eAgInSc6d1	folkový
kolotoči	kolotoč	k1gInSc6	kolotoč
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
Dokumentární	dokumentární	k2eAgInPc4d1	dokumentární
filmy	film	k1gInPc4	film
Poprvé	poprvé	k6eAd1	poprvé
<g/>
,	,	kIx,	,
aneb	aneb	k?	aneb
střípky	střípek	k1gInPc1	střípek
ze	z	k7c2	z
života	život	k1gInSc2	život
J.	J.	kA	J.
N.	N.	kA	N.
a	a	k8xC	a
Jarek	Jarek	k1gMnSc1	Jarek
a	a	k8xC	a
Amadeus	Amadeus	k1gMnSc1	Amadeus
vyšly	vyjít	k5eAaPmAgFnP	vyjít
14	[number]	k4	14
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2009	[number]	k4	2009
na	na	k7c4	na
DVD	DVD	kA	DVD
Rodinné	rodinný	k2eAgNnSc1d1	rodinné
stříbro	stříbro	k1gNnSc1	stříbro
I.	I.	kA	I.
(	(	kIx(	(
<g/>
Indies	Indies	k1gMnSc1	Indies
Happy	Happa	k1gFnSc2	Happa
Trails	Trails	k1gInSc1	Trails
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Divadélko	divadélko	k1gNnSc1	divadélko
pod	pod	k7c7	pod
věží	věž	k1gFnSc7	věž
<g/>
,	,	kIx,	,
1983	[number]	k4	1983
Studio	studio	k1gNnSc1	studio
B	B	kA	B
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
–	–	k?	–
koncert	koncert	k1gInSc1	koncert
v	v	k7c6	v
televizním	televizní	k2eAgNnSc6d1	televizní
studiu	studio	k1gNnSc6	studio
Svou	svůj	k3xOyFgFnSc4	svůj
káru	kára	k1gFnSc4	kára
táhnem	táhnem	k?	táhnem
dál	daleko	k6eAd2	daleko
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
Na	na	k7c6	na
plovárně	plovárna	k1gFnSc6	plovárna
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
Chtěl	chtít	k5eAaImAgMnS	chtít
jsem	být	k5eAaImIp1nS	být
jí	on	k3xPp3gFnSc3	on
zazpívat	zazpívat	k5eAaPmF	zazpívat
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Torst	Torst	k1gMnSc1	Torst
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
248	[number]	k4	248
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7215	[number]	k4	7215
<g/>
-	-	kIx~	-
<g/>
399	[number]	k4	399
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
