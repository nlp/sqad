<s>
Otcem	otec	k1gMnSc7	otec
Sókratovým	Sókratův	k2eAgFnPc3d1	Sókratova
byl	být	k5eAaImAgInS	být
sochař	sochař	k1gMnSc1	sochař
Sofróniskos	Sofróniskosa	k1gFnPc2	Sofróniskosa
a	a	k8xC	a
matkou	matka	k1gFnSc7	matka
porodní	porodní	k2eAgFnSc1d1	porodní
bába	bába	k1gFnSc1	bába
Fainareté	Fainaretý	k2eAgFnSc2d1	Fainaretý
<g/>
,	,	kIx,	,
pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
dému	démos	k1gInSc2	démos
Antiochidova	Antiochidův	k2eAgMnSc2d1	Antiochidův
a	a	k8xC	a
narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
roce	rok	k1gInSc6	rok
77	[number]	k4	77
<g/>
.	.	kIx.	.
</s>
