<s>
Prototyp	prototyp	k1gInSc1
</s>
<s>
Prototyp	prototyp	k1gInSc1
(	(	kIx(
<g/>
z	z	k7c2
řeckého	řecký	k2eAgMnSc2d1
prótos	prótos	k1gMnSc1
<g/>
,	,	kIx,
první	první	k4xOgFnSc1
<g/>
,	,	kIx,
a	a	k8xC
typos	typos	k1gInSc1
<g/>
,	,	kIx,
ražba	ražba	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
vzorový	vzorový	k2eAgInSc4d1
<g/>
,	,	kIx,
pokusný	pokusný	k2eAgInSc4d1
první	první	k4xOgInSc4
výrobek	výrobek	k1gInSc4
nebo	nebo	k8xC
vůbec	vůbec	k9
příkladný	příkladný	k2eAgInSc1d1
exemplář	exemplář	k1gInSc1
nějaké	nějaký	k3yIgFnSc2
třídy	třída	k1gFnSc2
věcí	věc	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tak	tak	k6eAd1
byla	být	k5eAaImAgFnS
Eiffelova	Eiffelův	k2eAgFnSc1d1
věž	věž	k1gFnSc1
v	v	k7c6
Paříži	Paříž	k1gFnSc6
(	(	kIx(
<g/>
1889	#num#	k4
<g/>
)	)	kIx)
prototypem	prototyp	k1gInSc7
ocelové	ocelový	k2eAgFnSc2d1
rozhledny	rozhledna	k1gFnSc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
podle	podle	k7c2
něhož	jenž	k3xRgInSc2
se	se	k3xPyFc4
stavěla	stavět	k5eAaImAgNnP
i	i	k8xC
petřínská	petřínský	k2eAgFnSc1d1
rozhledna	rozhledna	k1gFnSc1
v	v	k7c6
Praze	Praha	k1gFnSc6
(	(	kIx(
<g/>
1891	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Prototyp	prototyp	k1gInSc1
letadla	letadlo	k1gNnSc2
Helios	Heliosa	k1gFnPc2
na	na	k7c4
sluneční	sluneční	k2eAgInSc4d1
pohon	pohon	k1gInSc4
</s>
<s>
Navrženou	navržený	k2eAgFnSc4d1
novinku	novinka	k1gFnSc4
<g/>
,	,	kIx,
nový	nový	k2eAgInSc4d1
typ	typ	k1gInSc4
nějakého	nějaký	k3yIgInSc2
průmyslového	průmyslový	k2eAgInSc2d1
výrobku	výrobek	k1gInSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
vyzkoušet	vyzkoušet	k5eAaPmF
dřív	dříve	k6eAd2
<g/>
,	,	kIx,
než	než	k8xS
se	se	k3xPyFc4
rozhodne	rozhodnout	k5eAaPmIp3nS
o	o	k7c6
její	její	k3xOp3gFnSc6
výrobě	výroba	k1gFnSc6
nebo	nebo	k8xC
než	než	k8xS
se	se	k3xPyFc4
výroba	výroba	k1gFnSc1
rozběhne	rozběhnout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
se	se	k3xPyFc4
nejprve	nejprve	k6eAd1
staví	stavět	k5eAaImIp3nP
např.	např.	kA
zmenšené	zmenšený	k2eAgInPc1d1
modely	model	k1gInPc1
<g/>
,	,	kIx,
ale	ale	k8xC
pak	pak	k6eAd1
se	se	k3xPyFc4
musí	muset	k5eAaImIp3nS
přikročit	přikročit	k5eAaPmF
k	k	k7c3
výrobě	výroba	k1gFnSc3
funkčního	funkční	k2eAgInSc2d1
prototypu	prototyp	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgNnPc1
je	být	k5eAaImIp3nS
pochopitelně	pochopitelně	k6eAd1
náročná	náročný	k2eAgFnSc1d1
<g/>
,	,	kIx,
protože	protože	k8xS
pro	pro	k7c4
ni	on	k3xPp3gFnSc4
ještě	ještě	k6eAd1
nejsou	být	k5eNaImIp3nP
k	k	k7c3
dispozici	dispozice	k1gFnSc3
výrobní	výrobní	k2eAgInPc1d1
nástroje	nástroj	k1gInPc1
hromadné	hromadný	k2eAgFnSc2d1
výroby	výroba	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
musí	muset	k5eAaImIp3nS
se	se	k3xPyFc4
vyrábět	vyrábět	k5eAaImF
více	hodně	k6eAd2
méně	málo	k6eAd2
„	„	k?
<g/>
ručně	ručně	k6eAd1
<g/>
“	“	k?
v	v	k7c6
několika	několik	k4yIc6
málo	málo	k6eAd1
kusech	kus	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
výrobu	výroba	k1gFnSc4
modelu	model	k1gInSc2
či	či	k8xC
prototypu	prototyp	k1gInSc2
lze	lze	k6eAd1
využít	využít	k5eAaPmF
také	také	k9
3D	3D	k4
tisk	tisk	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
však	však	k9
potřeba	potřeba	k6eAd1
ještě	ještě	k6eAd1
povrchově	povrchově	k6eAd1
opracovat	opracovat	k5eAaPmF
<g/>
,	,	kIx,
proto	proto	k8xC
je	být	k5eAaImIp3nS
nejvhodnějším	vhodný	k2eAgNnSc7d3
řešením	řešení	k1gNnSc7
využít	využít	k5eAaPmF
služeb	služba	k1gFnPc2
prototypovacího	prototypovací	k2eAgNnSc2d1
studia	studio	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
má	mít	k5eAaImIp3nS
přístup	přístup	k1gInSc1
k	k	k7c3
široké	široký	k2eAgFnSc3d1
škále	škála	k1gFnSc3
technologií	technologie	k1gFnPc2
a	a	k8xC
nespoléhá	spoléhat	k5eNaImIp3nS
jen	jen	k9
na	na	k7c4
jeden	jeden	k4xCgInSc4
typ	typ	k1gInSc4
3D	3D	k4
tiskáren	tiskárna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
softwarovém	softwarový	k2eAgNnSc6d1
inženýrství	inženýrství	k1gNnSc6
je	být	k5eAaImIp3nS
prototyp	prototyp	k1gInSc4
kus	kus	k1gInSc1
kódu	kód	k1gInSc2
(	(	kIx(
<g/>
rutina	rutina	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
z	z	k7c2
níž	jenž	k3xRgFnSc2
se	se	k3xPyFc4
různými	různý	k2eAgFnPc7d1
změnami	změna	k1gFnPc7
„	„	k?
<g/>
klonují	klonovat	k5eAaImIp3nP
<g/>
“	“	k?
další	další	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
se	se	k3xPyFc4
ušetří	ušetřit	k5eAaPmIp3nS
mnoho	mnoho	k4c1
času	čas	k1gInSc2
na	na	k7c6
psaní	psaní	k1gNnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
hlavně	hlavně	k9
na	na	k7c4
ladění	ladění	k1gNnSc4
<g/>
,	,	kIx,
protože	protože	k8xS
původní	původní	k2eAgInSc1d1
základ	základ	k1gInSc1
je	být	k5eAaImIp3nS
už	už	k6eAd1
odladěn	odladěn	k2eAgMnSc1d1
a	a	k8xC
vyzkoušen	vyzkoušen	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Softwarové	softwarový	k2eAgNnSc4d1
prototypování	prototypování	k1gNnSc4
–	–	k?
vytváření	vytváření	k1gNnSc2
prototypů	prototyp	k1gInPc2
(	(	kIx(
<g/>
neúplných	úplný	k2eNgFnPc2d1
verzí	verze	k1gFnPc2
<g/>
)	)	kIx)
počítačových	počítačový	k2eAgInPc2d1
programů	program	k1gInPc2
–	–	k?
je	být	k5eAaImIp3nS
jednou	jeden	k4xCgFnSc7
ze	z	k7c2
součástí	součást	k1gFnPc2
vývojového	vývojový	k2eAgInSc2d1
cyklu	cyklus	k1gInSc2
softwaru	software	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
sémantice	sémantika	k1gFnSc6
se	s	k7c7
prototypem	prototyp	k1gInSc7
rozumí	rozumět	k5eAaImIp3nS
vzorný	vzorný	k2eAgInSc1d1
příklad	příklad	k1gInSc1
nějaké	nějaký	k3yIgFnSc2
významové	významový	k2eAgFnSc2d1
kategorie	kategorie	k1gFnSc2
<g/>
,	,	kIx,
s	s	k7c7
nímž	jenž	k3xRgMnSc7
se	se	k3xPyFc4
pak	pak	k6eAd1
porovnávají	porovnávat	k5eAaImIp3nP
další	další	k2eAgInPc4d1
významy	význam	k1gInPc4
a	a	k8xC
podle	podle	k7c2
jejich	jejich	k3xOp3gFnSc2
blízkosti	blízkost	k1gFnSc2
rozhoduje	rozhodovat	k5eAaImIp3nS
<g/>
,	,	kIx,
zda	zda	k8xS
do	do	k7c2
téže	tenže	k3xDgFnSc2,k3xTgFnSc2
kategorie	kategorie	k1gFnSc2
také	také	k9
patří	patřit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
metrologii	metrologie	k1gFnSc6
se	s	k7c7
prototypem	prototyp	k1gInSc7
rozumí	rozumět	k5eAaImIp3nS
vzorová	vzorový	k2eAgFnSc1d1
realizace	realizace	k1gFnSc1
měrné	měrný	k2eAgFnSc2d1
jednotky	jednotka	k1gFnSc2
<g/>
,	,	kIx,
například	například	k6eAd1
prototypový	prototypový	k2eAgInSc4d1
kilogram	kilogram	k1gInSc4
z	z	k7c2
platinového	platinový	k2eAgNnSc2d1
iridia	iridium	k1gNnSc2
<g/>
,	,	kIx,
umístěný	umístěný	k2eAgInSc1d1
v	v	k7c6
Pařížském	pařížský	k2eAgInSc6d1
Bureau	Bureaus	k1gInSc6
des	des	k1gNnSc1
poids	poids	k1gInSc1
et	et	k?
mesures	mesures	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tam	tam	k6eAd1
je	být	k5eAaImIp3nS
umístěn	umístěn	k2eAgInSc1d1
i	i	k8xC
prototypový	prototypový	k2eAgInSc1d1
metr	metr	k1gInSc1
<g/>
,	,	kIx,
tyč	tyč	k1gFnSc1
se	s	k7c7
dvěma	dva	k4xCgFnPc7
ryskami	ryska	k1gFnPc7
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
je	být	k5eAaImIp3nS
však	však	k9
jako	jako	k9
normál	normál	k1gInSc4
od	od	k7c2
roku	rok	k1gInSc2
1983	#num#	k4
nahrazen	nahrazen	k2eAgInSc1d1
novou	nova	k1gFnSc7
<g/>
,	,	kIx,
fyzikální	fyzikální	k2eAgFnSc7d1
definicí	definice	k1gFnSc7
metru	metr	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
prototyp	prototyp	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85107813	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85107813	#num#	k4
</s>
