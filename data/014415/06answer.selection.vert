<s desamb="1">
Ta	ten	k3xDgNnPc1
je	být	k5eAaImIp3nS
pochopitelně	pochopitelně	k6eAd1
náročná	náročný	k2eAgFnSc1d1
<g/>
,	,	kIx,
protože	protože	k8xS
pro	pro	k7c4
ni	on	k3xPp3gFnSc4
ještě	ještě	k6eAd1
nejsou	být	k5eNaImIp3nP
k	k	k7c3
dispozici	dispozice	k1gFnSc3
výrobní	výrobní	k2eAgInPc1d1
nástroje	nástroj	k1gInPc1
hromadné	hromadný	k2eAgFnSc2d1
výroby	výroba	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
musí	muset	k5eAaImIp3nS
se	se	k3xPyFc4
vyrábět	vyrábět	k5eAaImF
více	hodně	k6eAd2
méně	málo	k6eAd2
„	„	k?
<g/>
ručně	ručně	k6eAd1
<g/>
“	“	k?
v	v	k7c6
několika	několik	k4yIc6
málo	málo	k6eAd1
kusech	kus	k1gInPc6
<g/>
.	.	kIx.
</s>