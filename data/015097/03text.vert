<s>
Lualaba	Lualaba	k1gFnSc1
</s>
<s>
Lualaba	Lualaba	k1gFnSc1
Základní	základní	k2eAgFnSc1d1
informace	informace	k1gFnSc1
Délka	délka	k1gFnSc1
toku	tok	k1gInSc2
</s>
<s>
1800	#num#	k4
km	km	kA
Světadíl	světadíl	k1gInSc1
</s>
<s>
Afrika	Afrika	k1gFnSc1
Pramen	pramen	k1gInSc1
</s>
<s>
Musofi	Musofi	k6eAd1
Ústí	ústí	k1gNnSc1
</s>
<s>
Kongo	Kongo	k1gNnSc1
Protéká	protékat	k5eAaImIp3nS
</s>
<s>
Konžská	konžský	k2eAgFnSc1d1
demokratická	demokratický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
Konžská	konžský	k2eAgFnSc1d1
demokratická	demokratický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
Úmoří	úmoří	k1gNnSc2
<g/>
,	,	kIx,
povodí	povodí	k1gNnSc2
</s>
<s>
Atlantský	atlantský	k2eAgInSc1d1
oceán	oceán	k1gInSc1
<g/>
,	,	kIx,
Kongo	Kongo	k1gNnSc4
Geodata	Geodat	k1gMnSc2
OpenStreetMap	OpenStreetMap	k1gMnSc1
</s>
<s>
OSM	osm	k4xCc1
<g/>
,	,	kIx,
WMF	WMF	kA
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Lualaba	Lualaba	k1gFnSc1
je	být	k5eAaImIp3nS
řeka	řeka	k1gFnSc1
v	v	k7c6
Demokratické	demokratický	k2eAgFnSc6d1
republice	republika	k1gFnSc6
Kongo	Kongo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
horní	horní	k2eAgInSc1d1
tok	tok	k1gInSc1
řeky	řeka	k1gFnSc2
Kongo	Kongo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
největší	veliký	k2eAgFnSc7d3
zdrojnicí	zdrojnice	k1gFnSc7
podle	podle	k7c2
průtoku	průtok	k1gInSc2
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
podle	podle	k7c2
délky	délka	k1gFnSc2
toku	tok	k1gInSc2
je	být	k5eAaImIp3nS
Chambeshi	Chambeshe	k1gFnSc4
delší	dlouhý	k2eAgFnSc4d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
1800	#num#	k4
km	km	kA
dlouhá	dlouhý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Průběh	průběh	k1gInSc1
toku	tok	k1gInSc2
</s>
<s>
Pramení	pramenit	k5eAaImIp3nS
poblíž	poblíž	k6eAd1
Musofi	Musofe	k1gFnSc4
v	v	k7c6
blízkosti	blízkost	k1gFnSc6
Lubumbashi	Lubumbash	k1gFnSc2
v	v	k7c6
provincii	provincie	k1gFnSc6
Katanga	Katang	k1gMnSc2
v	v	k7c6
nadmořské	nadmořský	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
1400	#num#	k4
m	m	kA
a	a	k8xC
teče	téct	k5eAaImIp3nS
na	na	k7c4
sever	sever	k1gInSc4
přes	přes	k7c4
planinu	planina	k1gFnSc4
Katanga	Katanga	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
přechází	přecházet	k5eAaImIp3nS
v	v	k7c4
planinu	planina	k1gFnSc4
Manika	Manikum	k1gNnSc2
s	s	k7c7
vodopády	vodopád	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
pro	pro	k7c4
ni	on	k3xPp3gFnSc4
charakteristické	charakteristický	k2eAgNnSc1d1
střídání	střídání	k1gNnSc1
úseků	úsek	k1gInPc2
s	s	k7c7
peřejemi	peřej	k1gFnPc7
a	a	k8xC
rovných	rovný	k2eAgFnPc2d1
částí	část	k1gFnPc2
s	s	k7c7
klidným	klidný	k2eAgInSc7d1
tokem	tok	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejprudší	prudký	k2eAgInSc4d3
spád	spád	k1gInSc4
(	(	kIx(
<g/>
475	#num#	k4
m	m	kA
na	na	k7c4
72	#num#	k4
km	km	kA
<g/>
)	)	kIx)
má	mít	k5eAaImIp3nS
v	v	k7c6
soutěsce	soutěska	k1gFnSc6
Nzilo	Nzilo	k1gNnSc1
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc7,k3yRgFnSc7,k3yIgFnSc7
se	se	k3xPyFc4
prořezává	prořezávat	k5eAaImIp3nS
skrze	skrze	k?
jižní	jižní	k2eAgInPc4d1
výběžky	výběžek	k1gInPc4
pohoří	pohořet	k5eAaPmIp3nS
Mitumba	Mitumba	k1gFnSc1
ke	k	k7c3
Kamalondo	Kamalondo	k6eAd1
Trough	Trougha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
města	město	k1gNnSc2
Bukama	Bukama	k?
teče	téct	k5eAaImIp3nS
řeka	řeka	k1gFnSc1
líně	líně	k6eAd1
a	a	k8xC
silně	silně	k6eAd1
meandruje	meandrovat	k5eAaImIp3nS
po	po	k7c6
plochém	plochý	k2eAgNnSc6d1
dně	dno	k1gNnSc6
propadliny	propadlina	k1gFnSc2
Upemba	Upemba	k1gFnSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
nacházejí	nacházet	k5eAaImIp3nP
bažinatá	bažinatý	k2eAgNnPc1d1
jezera	jezero	k1gNnPc1
(	(	kIx(
<g/>
Upemba	Upemba	k1gFnSc1
<g/>
,	,	kIx,
Kisale	Kisala	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pod	pod	k7c7
městem	město	k1gNnSc7
Kongolo	Kongola	k1gFnSc5
si	se	k3xPyFc3
řeka	řeka	k1gFnSc1
vytvořila	vytvořit	k5eAaPmAgFnS
cestu	cesta	k1gFnSc4
přes	přes	k7c4
krystalické	krystalický	k2eAgFnPc4d1
horniny	hornina	k1gFnPc4
soutěskou	soutěska	k1gFnSc7
Portes	Portes	k1gMnSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
Enfer	Enfer	k1gMnSc1
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
vytváří	vytvářet	k5eAaImIp3nS,k5eAaPmIp3nS
peřeje	peřej	k1gInPc4
a	a	k8xC
vodopády	vodopád	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Níže	nízce	k6eAd2
po	po	k7c6
toku	tok	k1gInSc6
následuje	následovat	k5eAaImIp3nS
ještě	ještě	k6eAd1
několik	několik	k4yIc4
skupin	skupina	k1gFnPc2
vodopádů	vodopád	k1gInPc2
a	a	k8xC
peřejí	peřej	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
městy	město	k1gNnPc7
Kindu	Kind	k1gInSc2
a	a	k8xC
Ubundu	Ubund	k1gInSc2
řeka	řek	k1gMnSc2
opět	opět	k6eAd1
kladně	kladně	k6eAd1
teče	téct	k5eAaImIp3nS
v	v	k7c6
široké	široký	k2eAgFnSc6d1
dolině	dolina	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nedaleko	daleko	k6eNd1
jižně	jižně	k6eAd1
od	od	k7c2
rovníku	rovník	k1gInSc2
u	u	k7c2
Kisangani	Kisangaň	k1gFnSc6
padá	padat	k5eAaImIp3nS
Boyomskými	Boyomský	k2eAgInPc7d1
vodopády	vodopád	k1gInPc7
do	do	k7c2
Konžské	konžský	k2eAgFnSc2d1
pánve	pánev	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
oficiálně	oficiálně	k6eAd1
začíná	začínat	k5eAaImIp3nS
řeka	řeka	k1gFnSc1
Kongo	Kongo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Přítoky	přítok	k1gInPc1
</s>
<s>
Největší	veliký	k2eAgInPc1d3
přítoky	přítok	k1gInPc1
jsou	být	k5eAaImIp3nP
Lowa	Low	k1gInSc2
<g/>
,	,	kIx,
Ulindi	Ulind	k1gMnPc1
<g/>
,	,	kIx,
Luama	Luama	k1gNnSc1
<g/>
,	,	kIx,
Lukuga	Lukuga	k1gFnSc1
<g/>
,	,	kIx,
Lufira	Lufira	k1gFnSc1
<g/>
,	,	kIx,
Lubudi	Lubud	k1gMnPc1
<g/>
,	,	kIx,
Luvua	Luvua	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Využití	využití	k1gNnSc1
</s>
<s>
Na	na	k7c6
řece	řeka	k1gFnSc6
nedaleko	nedaleko	k7c2
vodopádů	vodopád	k1gInPc2
Nzilo	Nzilo	k1gNnSc4
byla	být	k5eAaImAgFnS
vybudována	vybudován	k2eAgFnSc1d1
přehradní	přehradní	k2eAgFnSc1d1
nádrž	nádrž	k1gFnSc1
Nzilo	Nzilo	k1gNnSc1
s	s	k7c7
vodní	vodní	k2eAgFnSc7d1
elektrárnou	elektrárna	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vodní	vodní	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
je	být	k5eAaImIp3nS
možná	možná	k9
od	od	k7c2
Bukamy	Bukam	k1gInPc4
v	v	k7c6
délce	délka	k1gFnSc6
640	#num#	k4
km	km	kA
až	až	k8xS
ke	k	k7c3
Kongole	Kongola	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
je	být	k5eAaImIp3nS
možná	možná	k9
mezi	mezi	k7c7
Kasongo	Kasongo	k6eAd1
a	a	k8xC
Kibombo	Kibomba	k1gFnSc5
v	v	k7c6
délce	délka	k1gFnSc6
100	#num#	k4
km	km	kA
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byly	být	k5eAaImAgFnP
použity	použit	k2eAgFnPc1d1
informace	informace	k1gFnPc1
z	z	k7c2
Velké	velký	k2eAgFnSc2d1
sovětské	sovětský	k2eAgFnSc2d1
encyklopedie	encyklopedie	k1gFnSc2
<g/>
,	,	kIx,
heslo	heslo	k1gNnSc1
„	„	k?
<g/>
К	К	k?
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byly	být	k5eAaImAgFnP
použity	použit	k2eAgFnPc1d1
informace	informace	k1gFnPc1
z	z	k7c2
Velké	velký	k2eAgFnSc2d1
sovětské	sovětský	k2eAgFnSc2d1
encyklopedie	encyklopedie	k1gFnSc2
<g/>
,	,	kIx,
heslo	heslo	k1gNnSc1
„	„	k?
<g/>
Л	Л	k?
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Lualaba	Lualab	k1gMnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
VIAF	VIAF	kA
<g/>
:	:	kIx,
122599504	#num#	k4
</s>
