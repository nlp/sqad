<s>
Ernst	Ernst	k1gMnSc1	Ernst
Haeckel	Haeckel	k1gMnSc1	Haeckel
se	se	k3xPyFc4	se
dál	daleko	k6eAd2	daleko
pokoušel	pokoušet	k5eAaImAgInS	pokoušet
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
Darwinovo	Darwinův	k2eAgNnSc4d1	Darwinovo
dílo	dílo	k1gNnSc4	dílo
a	a	k8xC	a
také	také	k9	také
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
přišel	přijít	k5eAaPmAgInS	přijít
s	s	k7c7	s
onou	onen	k3xDgFnSc7	onen
značně	značně	k6eAd1	značně
nepřesnou	přesný	k2eNgFnSc7d1	nepřesná
<g/>
,	,	kIx,	,
zjednodušující	zjednodušující	k2eAgFnSc7d1	zjednodušující
a	a	k8xC	a
zavádějící	zavádějící	k2eAgFnSc7d1	zavádějící
formulací	formulace	k1gFnSc7	formulace
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
člověk	člověk	k1gMnSc1	člověk
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
z	z	k7c2	z
opice	opice	k1gFnSc2	opice
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
