<s>
Brno	Brno	k1gNnSc1	Brno
je	být	k5eAaImIp3nS	být
centrem	centr	k1gInSc7	centr
soudní	soudní	k2eAgFnSc2d1	soudní
moci	moc	k1gFnSc2	moc
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
sídlem	sídlo	k1gNnSc7	sídlo
jak	jak	k6eAd1	jak
Ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
<g/>
,	,	kIx,	,
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
správního	správní	k2eAgInSc2d1	správní
soudu	soud	k1gInSc2	soud
i	i	k8xC	i
Nejvyššího	vysoký	k2eAgNnSc2d3	nejvyšší
státního	státní	k2eAgNnSc2d1	státní
zastupitelství	zastupitelství	k1gNnSc2	zastupitelství
<g/>
.	.	kIx.	.
</s>
