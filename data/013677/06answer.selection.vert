<s>
Akademie	akademie	k1gFnSc1
věd	věda	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
(	(	kIx(
<g/>
zkratka	zkratka	k1gFnSc1
AV	AV	kA
ČR	ČR	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
organizační	organizační	k2eAgFnSc1d1
složka	složka	k1gFnSc1
státu	stát	k1gInSc2
sdružující	sdružující	k2eAgFnSc2d1
vědecké	vědecký	k2eAgFnSc2d1
ústavy	ústava	k1gFnSc2
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
zabývající	zabývající	k2eAgFnSc1d1
se	se	k3xPyFc4
převážně	převážně	k6eAd1
základním	základní	k2eAgInSc7d1
výzkumem	výzkum	k1gInSc7
<g/>
.	.	kIx.
</s>