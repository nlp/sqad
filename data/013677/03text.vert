<s>
Akademie	akademie	k1gFnSc1
věd	věda	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
Akademie	akademie	k1gFnSc1
věd	věda	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
Ústředí	ústředí	k1gNnSc2
Akademie	akademie	k1gFnSc2
věd	věda	k1gFnPc2
na	na	k7c6
Národní	národní	k2eAgFnSc6d1
třídě	třída	k1gFnSc6
v	v	k7c6
Praze	Praha	k1gFnSc6
Sídlo	sídlo	k1gNnSc4
</s>
<s>
Akademie	akademie	k1gFnSc1
věd	věda	k1gFnPc2
ČRNárodní	ČRNárodní	k2eAgInPc1d1
1009	#num#	k4
<g/>
/	/	kIx~
<g/>
3	#num#	k4
<g/>
Praha	Praha	k1gFnSc1
1	#num#	k4
<g/>
,	,	kIx,
Staré	Stará	k1gFnPc1
Město	město	k1gNnSc1
<g/>
117	#num#	k4
20	#num#	k4
Praha	Praha	k1gFnSc1
1	#num#	k4
Souřadnice	souřadnice	k1gFnSc1
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
4	#num#	k4
<g/>
′	′	k?
<g/>
53,78	53,78	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
14	#num#	k4
<g/>
°	°	k?
<g/>
24	#num#	k4
<g/>
′	′	k?
<g/>
52,72	52,72	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Funkční	funkční	k2eAgNnSc4d1
období	období	k1gNnSc4
</s>
<s>
2017	#num#	k4
<g/>
–	–	k?
<g/>
2021	#num#	k4
Předsedkyně	předsedkyně	k1gFnSc1
</s>
<s>
prof.	prof.	kA
RNDr.	RNDr.	kA
Eva	Eva	k1gFnSc1
Zažímalová	Zažímalová	k1gFnSc1
<g/>
,	,	kIx,
CSc.	CSc.	kA
Místopředseda	místopředseda	k1gMnSc1
</s>
<s>
prof.	prof.	kA
Jan	Jan	k1gMnSc1
Řídký	řídký	k2eAgMnSc1d1
<g/>
,	,	kIx,
DrSc	DrSc	kA
<g/>
.	.	kIx.
Místopředseda	místopředseda	k1gMnSc1
</s>
<s>
RNDr.	RNDr.	kA
Zdeněk	Zdeněk	k1gMnSc1
Havlas	Havlas	k1gMnSc1
<g/>
,	,	kIx,
DrSc	DrSc	kA
<g/>
.	.	kIx.
Místopředseda	místopředseda	k1gMnSc1
</s>
<s>
PhDr.	PhDr.	kA
Pavel	Pavel	k1gMnSc1
Baran	Baran	k1gMnSc1
<g/>
,	,	kIx,
CSc.	CSc.	kA
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
www.avcr.cz	www.avcr.cz	k1gInSc1
Datová	datový	k2eAgFnSc1d1
schránka	schránka	k1gFnSc1
</s>
<s>
fr	fr	k0
<g/>
6	#num#	k4
<g/>
adt	adt	k?
<g/>
5	#num#	k4
IČO	IČO	kA
</s>
<s>
60165171	#num#	k4
(	(	kIx(
<g/>
VR	vr	k0
<g/>
)	)	kIx)
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
můžou	můžou	k?
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Akademie	akademie	k1gFnSc1
věd	věda	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
(	(	kIx(
<g/>
zkratka	zkratka	k1gFnSc1
AV	AV	kA
ČR	ČR	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
organizační	organizační	k2eAgFnSc1d1
složka	složka	k1gFnSc1
státu	stát	k1gInSc2
sdružující	sdružující	k2eAgFnSc2d1
vědecké	vědecký	k2eAgFnSc2d1
ústavy	ústava	k1gFnSc2
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
zabývající	zabývající	k2eAgFnSc1d1
se	se	k3xPyFc4
převážně	převážně	k6eAd1
základním	základní	k2eAgInSc7d1
výzkumem	výzkum	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
zákona	zákon	k1gInSc2
může	moct	k5eAaImIp3nS
zakládat	zakládat	k5eAaImF
tzv.	tzv.	kA
veřejné	veřejný	k2eAgFnSc2d1
výzkumné	výzkumný	k2eAgFnSc2d1
instituce	instituce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Akademie	akademie	k1gFnSc1
věd	věda	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
byla	být	k5eAaImAgFnS
ustanovena	ustanovit	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1992	#num#	k4
rozhodnutím	rozhodnutí	k1gNnSc7
České	český	k2eAgFnSc2d1
národní	národní	k2eAgFnSc2d1
rady	rada	k1gFnSc2
jako	jako	k8xC,k8xS
nástupnická	nástupnický	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
Československé	československý	k2eAgFnSc2d1
akademie	akademie	k1gFnSc2
věd	věda	k1gFnPc2
(	(	kIx(
<g/>
ČSAV	ČSAV	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současnosti	současnost	k1gFnSc6
představuje	představovat	k5eAaImIp3nS
hlavní	hlavní	k2eAgFnSc4d1
veřejnoprávní	veřejnoprávní	k2eAgFnSc4d1
instituci	instituce	k1gFnSc4
zabývající	zabývající	k2eAgFnSc4d1
se	s	k7c7
základním	základní	k2eAgInSc7d1
výzkumem	výzkum	k1gInSc7
v	v	k7c6
Česku	Česko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
AV	AV	kA
ČR	ČR	kA
je	být	k5eAaImIp3nS
financována	financován	k2eAgFnSc1d1
především	především	k9
ze	z	k7c2
státního	státní	k2eAgInSc2d1
rozpočtu	rozpočet	k1gInSc2
<g/>
;	;	kIx,
v	v	k7c6
současnosti	současnost	k1gFnSc6
v	v	k7c6
ní	on	k3xPp3gFnSc6
pracuje	pracovat	k5eAaImIp3nS
asi	asi	k9
6400	#num#	k4
zaměstnanců	zaměstnanec	k1gMnPc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgMnPc2
více	hodně	k6eAd2
než	než	k8xS
polovina	polovina	k1gFnSc1
má	mít	k5eAaImIp3nS
univerzitní	univerzitní	k2eAgNnSc4d1
vzdělání	vzdělání	k1gNnSc4
resp.	resp.	kA
doktorský	doktorský	k2eAgInSc4d1
titul	titul	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
AV	AV	kA
ČR	ČR	kA
ideově	ideově	k6eAd1
navazuje	navazovat	k5eAaImIp3nS
na	na	k7c4
činnost	činnost	k1gFnSc4
těchto	tento	k3xDgFnPc2
institucí	instituce	k1gFnPc2
<g/>
:	:	kIx,
</s>
<s>
Královská	královský	k2eAgFnSc1d1
česká	český	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
nauk	nauka	k1gFnPc2
(	(	kIx(
<g/>
1784	#num#	k4
<g/>
–	–	k?
<g/>
1952	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Česká	český	k2eAgFnSc1d1
akademie	akademie	k1gFnSc1
věd	věda	k1gFnPc2
a	a	k8xC
umění	umění	k1gNnPc2
(	(	kIx(
<g/>
1890	#num#	k4
<g/>
–	–	k?
<g/>
1952	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Československá	československý	k2eAgFnSc1d1
akademie	akademie	k1gFnSc1
věd	věda	k1gFnPc2
(	(	kIx(
<g/>
ČSAV	ČSAV	kA
<g/>
)	)	kIx)
(	(	kIx(
<g/>
1953	#num#	k4
<g/>
–	–	k?
<g/>
1992	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Sídlem	sídlo	k1gNnSc7
ústředí	ústředí	k1gNnSc2
Akademie	akademie	k1gFnSc2
je	být	k5eAaImIp3nS
budova	budova	k1gFnSc1
Akademie	akademie	k1gFnSc2
věd	věda	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
na	na	k7c6
Starém	starý	k2eAgNnSc6d1
Městě	město	k1gNnSc6
<g/>
,	,	kIx,
Národní	národní	k2eAgFnSc6d1
č.	č.	k?
<g/>
p.	p.	k?
1009	#num#	k4
<g/>
/	/	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Organizační	organizační	k2eAgFnSc1d1
struktura	struktura	k1gFnSc1
</s>
<s>
Vrcholným	vrcholný	k2eAgInSc7d1
orgánem	orgán	k1gInSc7
je	být	k5eAaImIp3nS
volený	volený	k2eAgInSc1d1
akademický	akademický	k2eAgInSc1d1
sněm	sněm	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc4,k3yRgInSc4,k3yIgInSc4
mj.	mj.	kA
volí	volit	k5eAaImIp3nS
předsedu	předseda	k1gMnSc4
Akademie	akademie	k1gFnSc2
věd	věda	k1gFnPc2
se	s	k7c7
čtyřletým	čtyřletý	k2eAgNnSc7d1
funkčním	funkční	k2eAgNnSc7d1
obdobím	období	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Předsedové	předseda	k1gMnPc1
Akademie	akademie	k1gFnSc2
věd	věda	k1gFnPc2
</s>
<s>
prof.	prof.	kA
RNDr.	RNDr.	kA
Eva	Eva	k1gFnSc1
Zažímalová	Zažímalová	k1gFnSc1
<g/>
,	,	kIx,
CSc.	CSc.	kA
<g/>
,	,	kIx,
předsedkyně	předsedkyně	k1gFnSc1
Akademie	akademie	k1gFnSc2
věd	věda	k1gFnPc2
ČR	ČR	kA
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
leden	leden	k1gInSc1
1993	#num#	k4
–	–	k?
25	#num#	k4
<g/>
.	.	kIx.
únor	únor	k1gInSc1
1993	#num#	k4
–	–	k?
Jiří	Jiří	k1gMnSc1
Velemínský	Velemínský	k2eAgMnSc1d1
(	(	kIx(
<g/>
předseda	předseda	k1gMnSc1
Výboru	výbor	k1gInSc2
pro	pro	k7c4
řízení	řízení	k1gNnSc4
pracovišť	pracoviště	k1gNnPc2
Československé	československý	k2eAgFnSc2d1
akademie	akademie	k1gFnSc2
věd	věda	k1gFnPc2
<g/>
,	,	kIx,
než	než	k8xS
byla	být	k5eAaImAgFnS
provedena	provést	k5eAaPmNgFnS
řádná	řádný	k2eAgFnSc1d1
volba	volba	k1gFnSc1
vedení	vedení	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
únor	únor	k1gInSc1
1993	#num#	k4
–	–	k?
27	#num#	k4
<g/>
.	.	kIx.
březen	březen	k1gInSc1
2001	#num#	k4
–	–	k?
Rudolf	Rudolf	k1gMnSc1
Zahradník	Zahradník	k1gMnSc1
</s>
<s>
27	#num#	k4
<g/>
.	.	kIx.
březen	březen	k1gInSc1
2001	#num#	k4
–	–	k?
24	#num#	k4
<g/>
.	.	kIx.
březen	březen	k1gInSc1
2005	#num#	k4
–	–	k?
Helena	Helena	k1gFnSc1
Illnerová	Illnerová	k1gFnSc1
</s>
<s>
24	#num#	k4
<g/>
.	.	kIx.
březen	březen	k1gInSc1
2005	#num#	k4
–	–	k?
24	#num#	k4
<g/>
.	.	kIx.
březen	březen	k1gInSc1
2009	#num#	k4
–	–	k?
Václav	Václav	k1gMnSc1
Pačes	pačes	k1gInSc1
</s>
<s>
24	#num#	k4
<g/>
.	.	kIx.
březen	březen	k1gInSc1
2009	#num#	k4
–	–	k?
24	#num#	k4
<g/>
.	.	kIx.
březen	březen	k1gInSc1
2017	#num#	k4
–	–	k?
Jiří	Jiří	k1gMnSc1
Drahoš	Drahoš	k1gMnSc1
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
březen	březen	k1gInSc1
2017	#num#	k4
–	–	k?
dosud	dosud	k6eAd1
–	–	k?
Eva	Eva	k1gFnSc1
Zažímalová	Zažímalová	k1gFnSc1
</s>
<s>
Ústavy	ústava	k1gFnPc1
Akademie	akademie	k1gFnSc2
věd	věda	k1gFnPc2
</s>
<s>
Prof.	prof.	kA
Ing.	ing.	kA
Jiří	Jiří	k1gMnSc1
Drahoš	Drahoš	k1gMnSc1
<g/>
,	,	kIx,
DrSc	DrSc	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
bývalý	bývalý	k2eAgMnSc1d1
předseda	předseda	k1gMnSc1
Akademie	akademie	k1gFnSc2
věd	věda	k1gFnPc2
ČR	ČR	kA
</s>
<s>
prof.	prof.	kA
RNDr.	RNDr.	kA
Václav	Václav	k1gMnSc1
Pačes	pačes	k1gInSc1
<g/>
,	,	kIx,
DrSc	DrSc	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
bývalý	bývalý	k2eAgMnSc1d1
předseda	předseda	k1gMnSc1
Akademie	akademie	k1gFnSc2
věd	věda	k1gFnPc2
ČR	ČR	kA
</s>
<s>
AV	AV	kA
ČR	ČR	kA
zahrnuje	zahrnovat	k5eAaImIp3nS
přes	přes	k7c4
50	#num#	k4
vědeckých	vědecký	k2eAgInPc2d1
ústavů	ústav	k1gInPc2
rozdělených	rozdělený	k2eAgInPc2d1
do	do	k7c2
tří	tři	k4xCgFnPc2
oblastí	oblast	k1gFnPc2
a	a	k8xC
celkem	celkem	k6eAd1
devíti	devět	k4xCc2
sekcí	sekce	k1gFnPc2
<g/>
:	:	kIx,
</s>
<s>
Oblast	oblast	k1gFnSc1
věd	věda	k1gFnPc2
o	o	k7c6
neživé	živý	k2eNgFnSc6d1
přírodě	příroda	k1gFnSc6
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
sekce	sekce	k1gFnSc1
matematiky	matematika	k1gFnSc2
<g/>
,	,	kIx,
fyziky	fyzika	k1gFnSc2
a	a	k8xC
informatiky	informatika	k1gFnSc2
</s>
<s>
Astronomický	astronomický	k2eAgInSc1d1
ústav	ústav	k1gInSc1
AV	AV	kA
ČR	ČR	kA
</s>
<s>
Fyzikální	fyzikální	k2eAgInSc1d1
ústav	ústav	k1gInSc1
AV	AV	kA
ČR	ČR	kA
</s>
<s>
Matematický	matematický	k2eAgInSc1d1
ústav	ústav	k1gInSc1
AV	AV	kA
ČR	ČR	kA
</s>
<s>
Ústav	ústav	k1gInSc1
informatiky	informatika	k1gFnSc2
AV	AV	kA
ČR	ČR	kA
</s>
<s>
Ústav	ústav	k1gInSc1
jaderné	jaderný	k2eAgFnSc2d1
fyziky	fyzika	k1gFnSc2
AV	AV	kA
ČR	ČR	kA
</s>
<s>
Ústav	ústav	k1gInSc1
teorie	teorie	k1gFnSc2
informace	informace	k1gFnSc2
a	a	k8xC
automatizace	automatizace	k1gFnSc2
AV	AV	kA
ČR	ČR	kA
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
sekce	sekce	k1gFnSc1
aplikované	aplikovaný	k2eAgFnSc2d1
fyziky	fyzika	k1gFnSc2
</s>
<s>
Ústav	ústav	k1gInSc1
fotoniky	fotonika	k1gFnSc2
a	a	k8xC
elektroniky	elektronika	k1gFnSc2
AV	AV	kA
ČR	ČR	kA
</s>
<s>
Ústav	ústav	k1gInSc1
fyziky	fyzika	k1gFnSc2
materiálů	materiál	k1gInPc2
AV	AV	kA
ČR	ČR	kA
</s>
<s>
Ústav	ústav	k1gInSc1
fyziky	fyzika	k1gFnSc2
plazmatu	plazma	k1gNnSc2
AV	AV	kA
ČR	ČR	kA
</s>
<s>
Ústav	ústav	k1gInSc1
přístrojové	přístrojový	k2eAgFnSc2d1
techniky	technika	k1gFnSc2
AV	AV	kA
ČR	ČR	kA
</s>
<s>
Ústav	ústav	k1gInSc1
pro	pro	k7c4
hydrodynamiku	hydrodynamika	k1gFnSc4
AV	AV	kA
ČR	ČR	kA
</s>
<s>
Ústav	ústav	k1gInSc1
teoretické	teoretický	k2eAgFnSc2d1
a	a	k8xC
aplikované	aplikovaný	k2eAgFnSc2d1
mechaniky	mechanika	k1gFnSc2
AV	AV	kA
ČR	ČR	kA
</s>
<s>
Ústav	ústav	k1gInSc1
termomechaniky	termomechanika	k1gFnSc2
AV	AV	kA
ČR	ČR	kA
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
sekce	sekce	k1gFnSc1
věd	věda	k1gFnPc2
o	o	k7c6
Zemi	zem	k1gFnSc6
</s>
<s>
Geofyzikální	geofyzikální	k2eAgInSc1d1
ústav	ústav	k1gInSc1
AV	AV	kA
ČR	ČR	kA
</s>
<s>
Geologický	geologický	k2eAgInSc1d1
ústav	ústav	k1gInSc1
AV	AV	kA
ČR	ČR	kA
</s>
<s>
Ústav	ústav	k1gInSc1
fyziky	fyzika	k1gFnSc2
atmosféry	atmosféra	k1gFnSc2
AV	AV	kA
ČR	ČR	kA
</s>
<s>
Ústav	ústav	k1gInSc1
geoniky	geonik	k1gInPc1
AV	AV	kA
ČR	ČR	kA
</s>
<s>
Ústav	ústav	k1gInSc1
struktury	struktura	k1gFnSc2
a	a	k8xC
mechaniky	mechanika	k1gFnSc2
hornin	hornina	k1gFnPc2
AV	AV	kA
ČR	ČR	kA
</s>
<s>
Oblast	oblast	k1gFnSc1
věd	věda	k1gFnPc2
o	o	k7c6
živé	živý	k2eAgFnSc6d1
přírodě	příroda	k1gFnSc6
a	a	k8xC
chemických	chemický	k2eAgFnPc2d1
věd	věda	k1gFnPc2
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
sekce	sekce	k1gFnSc1
chemických	chemický	k2eAgFnPc2d1
věd	věda	k1gFnPc2
</s>
<s>
Ústav	ústav	k1gInSc1
analytické	analytický	k2eAgFnSc2d1
chemie	chemie	k1gFnSc2
AV	AV	kA
ČR	ČR	kA
</s>
<s>
Ústav	ústav	k1gInSc1
anorganické	anorganický	k2eAgFnSc2d1
chemie	chemie	k1gFnSc2
AV	AV	kA
ČR	ČR	kA
</s>
<s>
Ústav	ústav	k1gInSc1
chemických	chemický	k2eAgInPc2d1
procesů	proces	k1gInPc2
AV	AV	kA
ČR	ČR	kA
</s>
<s>
Ústav	ústav	k1gInSc1
fyzikální	fyzikální	k2eAgFnSc2d1
chemie	chemie	k1gFnSc2
J.	J.	kA
Heyrovského	Heyrovský	k2eAgMnSc2d1
AV	AV	kA
ČR	ČR	kA
</s>
<s>
Ústav	ústav	k1gInSc1
makromolekulární	makromolekulární	k2eAgFnSc2d1
chemie	chemie	k1gFnSc2
AV	AV	kA
ČR	ČR	kA
</s>
<s>
Ústav	ústav	k1gInSc1
organické	organický	k2eAgFnSc2d1
chemie	chemie	k1gFnSc2
a	a	k8xC
biochemie	biochemie	k1gFnSc2
AV	AV	kA
ČR	ČR	kA
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
sekce	sekce	k1gFnSc1
biologických	biologický	k2eAgFnPc2d1
a	a	k8xC
lékařských	lékařský	k2eAgFnPc2d1
věd	věda	k1gFnPc2
</s>
<s>
Biofyzikální	biofyzikální	k2eAgInSc1d1
ústav	ústav	k1gInSc1
AV	AV	kA
ČR	ČR	kA
</s>
<s>
Biotechnologický	biotechnologický	k2eAgInSc1d1
ústav	ústav	k1gInSc1
AV	AV	kA
ČR	ČR	kA
</s>
<s>
Fyziologický	fyziologický	k2eAgInSc1d1
ústav	ústav	k1gInSc1
AV	AV	kA
ČR	ČR	kA
</s>
<s>
Mikrobiologický	mikrobiologický	k2eAgInSc1d1
ústav	ústav	k1gInSc1
AV	AV	kA
ČR	ČR	kA
</s>
<s>
Ústav	ústav	k1gInSc1
experimentální	experimentální	k2eAgFnSc2d1
botaniky	botanika	k1gFnSc2
AV	AV	kA
ČR	ČR	kA
</s>
<s>
Ústav	ústav	k1gInSc1
experimentální	experimentální	k2eAgFnSc2d1
medicíny	medicína	k1gFnSc2
AV	AV	kA
ČR	ČR	kA
</s>
<s>
Ústav	ústav	k1gInSc1
molekulární	molekulární	k2eAgFnSc2d1
genetiky	genetika	k1gFnSc2
AV	AV	kA
ČR	ČR	kA
</s>
<s>
Ústav	ústav	k1gInSc1
živočišné	živočišný	k2eAgFnSc2d1
fyziologie	fyziologie	k1gFnSc2
a	a	k8xC
genetiky	genetika	k1gFnSc2
AV	AV	kA
ČR	ČR	kA
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
sekce	sekce	k1gFnSc1
biologicko-ekologických	biologicko-ekologický	k2eAgFnPc2d1
věd	věda	k1gFnPc2
</s>
<s>
Biologické	biologický	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
AV	AV	kA
ČR	ČR	kA
</s>
<s>
Entomologický	entomologický	k2eAgInSc1d1
ústav	ústav	k1gInSc1
AV	AV	kA
ČR	ČR	kA
</s>
<s>
Hydrobiologický	hydrobiologický	k2eAgInSc1d1
ústav	ústav	k1gInSc1
AV	AV	kA
ČR	ČR	kA
</s>
<s>
Parazitologický	parazitologický	k2eAgInSc1d1
ústav	ústav	k1gInSc1
AV	AV	kA
ČR	ČR	kA
</s>
<s>
Ústav	ústav	k1gInSc1
molekulární	molekulární	k2eAgFnSc2d1
biologie	biologie	k1gFnSc2
rostlin	rostlina	k1gFnPc2
AV	AV	kA
ČR	ČR	kA
</s>
<s>
Ústav	ústav	k1gInSc1
půdní	půdní	k2eAgFnSc2d1
biologie	biologie	k1gFnSc2
AV	AV	kA
ČR	ČR	kA
</s>
<s>
Botanický	botanický	k2eAgInSc1d1
ústav	ústav	k1gInSc1
AV	AV	kA
ČR	ČR	kA
</s>
<s>
Ústav	ústav	k1gInSc1
biologie	biologie	k1gFnSc2
obratlovců	obratlovec	k1gMnPc2
AV	AV	kA
ČR	ČR	kA
</s>
<s>
Ústav	ústav	k1gInSc1
výzkumu	výzkum	k1gInSc2
globální	globální	k2eAgFnSc2d1
změny	změna	k1gFnSc2
</s>
<s>
Oblast	oblast	k1gFnSc1
humanitních	humanitní	k2eAgFnPc2d1
a	a	k8xC
společenských	společenský	k2eAgFnPc2d1
věd	věda	k1gFnPc2
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
sekce	sekce	k1gFnSc1
sociálně-ekonomických	sociálně-ekonomický	k2eAgFnPc2d1
věd	věda	k1gFnPc2
</s>
<s>
Knihovna	knihovna	k1gFnSc1
AV	AV	kA
ČR	ČR	kA
</s>
<s>
Národohospodářský	národohospodářský	k2eAgInSc1d1
ústav	ústav	k1gInSc1
AV	AV	kA
ČR	ČR	kA
</s>
<s>
Psychologický	psychologický	k2eAgInSc1d1
ústav	ústav	k1gInSc1
AV	AV	kA
ČR	ČR	kA
</s>
<s>
Sociologický	sociologický	k2eAgInSc1d1
ústav	ústav	k1gInSc1
AV	AV	kA
ČR	ČR	kA
</s>
<s>
Ústav	ústav	k1gInSc1
státu	stát	k1gInSc2
a	a	k8xC
práva	právo	k1gNnSc2
AV	AV	kA
ČR	ČR	kA
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
sekce	sekce	k1gFnSc1
historických	historický	k2eAgFnPc2d1
věd	věda	k1gFnPc2
</s>
<s>
Archeologický	archeologický	k2eAgInSc1d1
ústav	ústav	k1gInSc1
AV	AV	kA
ČR	ČR	kA
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc1
</s>
<s>
Archeologický	archeologický	k2eAgInSc1d1
ústav	ústav	k1gInSc1
AV	AV	kA
ČR	ČR	kA
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
</s>
<s>
Historický	historický	k2eAgInSc1d1
ústav	ústav	k1gInSc1
AV	AV	kA
ČR	ČR	kA
</s>
<s>
Masarykův	Masarykův	k2eAgInSc1d1
ústav	ústav	k1gInSc1
a	a	k8xC
Archiv	archiv	k1gInSc1
AV	AV	kA
ČR	ČR	kA
</s>
<s>
Ústav	ústav	k1gInSc1
dějin	dějiny	k1gFnPc2
umění	umění	k1gNnSc2
AV	AV	kA
ČR	ČR	kA
</s>
<s>
Ústav	ústav	k1gInSc1
pro	pro	k7c4
soudobé	soudobý	k2eAgFnPc4d1
dějiny	dějiny	k1gFnPc4
AV	AV	kA
ČR	ČR	kA
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
sekce	sekce	k1gFnSc1
humanitních	humanitní	k2eAgFnPc2d1
a	a	k8xC
filologických	filologický	k2eAgFnPc2d1
věd	věda	k1gFnPc2
</s>
<s>
Etnologický	etnologický	k2eAgInSc1d1
ústav	ústav	k1gInSc1
AV	AV	kA
ČR	ČR	kA
</s>
<s>
Filosofický	filosofický	k2eAgInSc1d1
ústav	ústav	k1gInSc1
AV	AV	kA
ČR	ČR	kA
</s>
<s>
Orientální	orientální	k2eAgInSc1d1
ústav	ústav	k1gInSc1
AV	AV	kA
ČR	ČR	kA
</s>
<s>
Slovanský	slovanský	k2eAgInSc1d1
ústav	ústav	k1gInSc1
AV	AV	kA
ČR	ČR	kA
</s>
<s>
Ústav	ústav	k1gInSc1
pro	pro	k7c4
českou	český	k2eAgFnSc4d1
literaturu	literatura	k1gFnSc4
AV	AV	kA
ČR	ČR	kA
</s>
<s>
Ústav	ústav	k1gInSc1
pro	pro	k7c4
jazyk	jazyk	k1gInSc4
český	český	k2eAgInSc4d1
AV	AV	kA
ČR	ČR	kA
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
ostatní	ostatní	k2eAgMnPc1d1
</s>
<s>
Komorní	komorní	k2eAgInSc1d1
orchestr	orchestr	k1gInSc1
Akademie	akademie	k1gFnSc2
věd	věda	k1gFnPc2
(	(	kIx(
<g/>
KOA	KOA	kA
<g/>
)	)	kIx)
</s>
<s>
Zrušené	zrušený	k2eAgInPc1d1
ústavy	ústav	k1gInPc1
ČSAV	ČSAV	kA
<g/>
,	,	kIx,
resp.	resp.	kA
AV	AV	kA
ČR	ČR	kA
</s>
<s>
Ústav	ústav	k1gInSc1
pro	pro	k7c4
výzkum	výzkum	k1gInSc4
společenského	společenský	k2eAgNnSc2d1
vědomí	vědomí	k1gNnSc2
a	a	k8xC
vědeckého	vědecký	k2eAgInSc2d1
ateismu	ateismus	k1gInSc2
ČSAV	ČSAV	kA
</s>
<s>
Ústav	ústav	k1gInSc1
vědeckého	vědecký	k2eAgInSc2d1
ateismu	ateismus	k1gInSc2
ČSAV	ČSAV	kA
</s>
<s>
Ústav	ústav	k1gInSc1
společenského	společenský	k2eAgNnSc2d1
vědomí	vědomí	k1gNnSc2
ČSAV	ČSAV	kA
</s>
<s>
Ústav	ústav	k1gInSc1
etiky	etika	k1gFnSc2
a	a	k8xC
religionistiky	religionistika	k1gFnSc2
AV	AV	kA
ČR	ČR	kA
</s>
<s>
Farmakologický	farmakologický	k2eAgInSc1d1
ústav	ústav	k1gInSc1
AV	AV	kA
ČR	ČR	kA
</s>
<s>
Geografický	geografický	k2eAgInSc1d1
ústav	ústav	k1gInSc1
AV	AV	kA
ČR	ČR	kA
</s>
<s>
Československo-sovětský	československo-sovětský	k2eAgInSc1d1
institut	institut	k1gInSc1
ČSAV	ČSAV	kA
</s>
<s>
Ekonomický	ekonomický	k2eAgInSc1d1
ústav	ústav	k1gInSc1
ČSAV	ČSAV	kA
</s>
<s>
Laboratoř	laboratoř	k1gFnSc1
evoluční	evoluční	k2eAgFnSc2d1
biologie	biologie	k1gFnSc2
ČSAV	ČSAV	kA
</s>
<s>
Prognostický	prognostický	k2eAgInSc1d1
ústav	ústav	k1gInSc1
</s>
<s>
Sociálně	sociálně	k6eAd1
ekonomický	ekonomický	k2eAgInSc1d1
ústav	ústav	k1gInSc1
ČSAV	ČSAV	kA
</s>
<s>
Ústav	ústav	k1gInSc1
dějin	dějiny	k1gFnPc2
evropských	evropský	k2eAgFnPc2d1
socialistických	socialistický	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
ČSAV	ČSAV	kA
</s>
<s>
Ústav	ústav	k1gInSc1
dějin	dějiny	k1gFnPc2
střední	střední	k2eAgFnSc2d1
a	a	k8xC
východní	východní	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
AV	AV	kA
ČR	ČR	kA
</s>
<s>
Ústav	ústav	k1gInSc1
ekologie	ekologie	k1gFnSc2
průmyslové	průmyslový	k2eAgFnSc2d1
krajiny	krajina	k1gFnSc2
ČSAV	ČSAV	kA
</s>
<s>
Ústav	ústav	k1gInSc1
fyziologických	fyziologický	k2eAgFnPc2d1
regulací	regulace	k1gFnPc2
ČSAV	ČSAV	kA
</s>
<s>
Ústav	ústav	k1gInSc1
chemie	chemie	k1gFnSc2
skelných	skelný	k2eAgInPc2d1
a	a	k8xC
keramických	keramický	k2eAgInPc2d1
materiálů	materiál	k1gInPc2
ČSAV	ČSAV	kA
</s>
<s>
Ústav	ústav	k1gInSc1
nukleární	nukleární	k2eAgFnSc2d1
biologie	biologie	k1gFnSc2
a	a	k8xC
radiochemie	radiochemie	k1gFnSc2
ČSAV	ČSAV	kA
</s>
<s>
Ústav	ústav	k1gInSc1
obecné	obecný	k2eAgFnSc2d1
energetiky	energetika	k1gFnSc2
ČSAV	ČSAV	kA
</s>
<s>
Ústav	ústav	k1gInSc1
teorie	teorie	k1gFnSc2
a	a	k8xC
historie	historie	k1gFnSc2
vědy	věda	k1gFnSc2
ČSAV	ČSAV	kA
</s>
<s>
Ústav	ústav	k1gInSc1
teorie	teorie	k1gFnSc2
hutnických	hutnický	k2eAgInPc2d1
procesů	proces	k1gInPc2
ČSAV	ČSAV	kA
</s>
<s>
Ústavu	ústava	k1gFnSc4
technologie	technologie	k1gFnSc2
a	a	k8xC
spolehlivosti	spolehlivost	k1gFnSc2
strojních	strojní	k2eAgFnPc2d1
konstrukcí	konstrukce	k1gFnPc2
ČSAV	ČSAV	kA
</s>
<s>
Výzkumný	výzkumný	k2eAgInSc1d1
ústav	ústav	k1gInSc1
pedagogický	pedagogický	k2eAgInSc1d1
Jana	Jana	k1gFnSc1
Amose	Amos	k1gMnSc2
Komenského	Komenský	k1gMnSc2
</s>
<s>
Kabinet	kabinet	k1gInSc1
pro	pro	k7c4
studium	studium	k1gNnSc4
českého	český	k2eAgNnSc2d1
divadla	divadlo	k1gNnSc2
</s>
<s>
Ústav	ústav	k1gInSc1
systémové	systémový	k2eAgFnSc2d1
biologie	biologie	k1gFnSc2
a	a	k8xC
ekologie	ekologie	k1gFnSc2
AV	AV	kA
ČR	ČR	kA
(	(	kIx(
<g/>
→	→	k?
Centrum	centrum	k1gNnSc1
pro	pro	k7c4
výzkum	výzkum	k1gInSc4
biodiverzity	biodiverzita	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Centrum	centrum	k1gNnSc1
výzkumu	výzkum	k1gInSc2
globální	globální	k2eAgFnSc2d1
změny	změna	k1gFnSc2
AV	AV	kA
ČR	ČR	kA
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Akademie	akademie	k1gFnSc2
věd	věda	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Zpráva	zpráva	k1gFnSc1
Nastane	nastat	k5eAaPmIp3nS
vražda	vražda	k1gFnSc1
české	český	k2eAgFnSc2d1
vědy	věda	k1gFnSc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
ve	v	k7c6
Wikizprávách	Wikizpráva	k1gFnPc6
</s>
<s>
Akademie	akademie	k1gFnSc1
věd	věda	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
Pracoviště	pracoviště	k1gNnSc1
AV	AV	kA
</s>
<s>
Videa	video	k1gNnPc4
a	a	k8xC
reportáže	reportáž	k1gFnPc4
z	z	k7c2
Akademie	akademie	k1gFnSc2
věd	věda	k1gFnPc2
</s>
<s>
Akademie	akademie	k1gFnSc1
Věd	věda	k1gFnPc2
v	v	k7c6
ohrožení	ohrožení	k1gNnSc6
</s>
<s>
Věda	věda	k1gFnSc1
pro	pro	k7c4
život	život	k1gInSc4
–	–	k?
Výstava	výstava	k1gFnSc1
Akademie	akademie	k1gFnSc2
věd	věda	k1gFnPc2
ČR	ČR	kA
</s>
<s>
Otevřená	otevřený	k2eAgFnSc1d1
věda	věda	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
kn	kn	k?
<g/>
20010709016	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
5094076-4	5094076-4	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
1015	#num#	k4
3316	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
93086736	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
172364749	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
93086736	#num#	k4
</s>
