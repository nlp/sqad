<s>
Kosatka	kosatka	k1gFnSc1	kosatka
dravá	dravý	k2eAgFnSc1d1	dravá
(	(	kIx(	(
<g/>
Orcinus	Orcinus	k1gMnSc1	Orcinus
orca	orca	k1gMnSc1	orca
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
známá	známý	k2eAgFnSc1d1	známá
také	také	k9	také
jako	jako	k8xS	jako
velryba	velryba	k1gFnSc1	velryba
zabiják	zabiják	k1gInSc1	zabiják
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgMnSc7d3	veliký
zástupcem	zástupce	k1gMnSc7	zástupce
čeledi	čeleď	k1gFnSc2	čeleď
delfínovití	delfínovitý	k2eAgMnPc5d1	delfínovitý
<g/>
.	.	kIx.	.
</s>
