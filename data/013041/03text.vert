<p>
<s>
Kosatka	kosatka	k1gFnSc1	kosatka
dravá	dravý	k2eAgFnSc1d1	dravá
(	(	kIx(	(
<g/>
Orcinus	Orcinus	k1gMnSc1	Orcinus
orca	orca	k1gMnSc1	orca
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
známá	známý	k2eAgFnSc1d1	známá
také	také	k9	také
jako	jako	k8xC	jako
velryba	velryba	k1gFnSc1	velryba
zabiják	zabiják	k1gInSc1	zabiják
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgMnSc7d3	veliký
zástupcem	zástupce	k1gMnSc7	zástupce
čeledi	čeleď	k1gFnSc2	čeleď
delfínovití	delfínovitý	k2eAgMnPc5d1	delfínovitý
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
všestranným	všestranný	k2eAgMnSc7d1	všestranný
predátorem	predátor	k1gMnSc7	predátor
stojícím	stojící	k2eAgMnSc7d1	stojící
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
mořského	mořský	k2eAgInSc2d1	mořský
potravního	potravní	k2eAgInSc2d1	potravní
řetězce	řetězec	k1gInSc2	řetězec
<g/>
,	,	kIx,	,
požírá	požírat	k5eAaImIp3nS	požírat
ryby	ryba	k1gFnPc4	ryba
<g/>
,	,	kIx,	,
želvy	želva	k1gFnPc4	želva
<g/>
,	,	kIx,	,
ptáky	pták	k1gMnPc4	pták
<g/>
,	,	kIx,	,
tuleně	tuleň	k1gMnPc4	tuleň
<g/>
,	,	kIx,	,
žraloky	žralok	k1gMnPc4	žralok
a	a	k8xC	a
také	také	k9	také
jiné	jiný	k2eAgMnPc4d1	jiný
kytovce	kytovec	k1gMnPc4	kytovec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
velryba	velryba	k1gFnSc1	velryba
zabiják	zabiják	k1gMnSc1	zabiják
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Killer	Killer	k1gInSc1	Killer
Whale	Whale	k1gFnSc2	Whale
[	[	kIx(	[
<g/>
ˈ	ˈ	k?	ˈ
ˌ	ˌ	k?	ˌ
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
<g/>
)	)	kIx)	)
odráží	odrážet	k5eAaImIp3nS	odrážet
její	její	k3xOp3gNnPc4	její
pověst	pověst	k1gFnSc1	pověst
velkolepého	velkolepý	k2eAgNnSc2d1	velkolepé
a	a	k8xC	a
obávaného	obávaný	k2eAgMnSc2d1	obávaný
mořského	mořský	k2eAgMnSc2d1	mořský
savce	savec	k1gMnSc2	savec
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
byla	být	k5eAaImAgNnP	být
popsána	popsat	k5eAaPmNgNnP	popsat
již	již	k6eAd1	již
v	v	k7c6	v
Historii	historie	k1gFnSc6	historie
naturalis	naturalis	k1gFnSc2	naturalis
římského	římský	k2eAgMnSc2d1	římský
filozofa	filozof	k1gMnSc2	filozof
Plinia	Plinium	k1gNnSc2	Plinium
staršího	starší	k1gMnSc2	starší
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
už	už	k6eAd1	už
víme	vědět	k5eAaImIp1nP	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
kosatka	kosatka	k1gFnSc1	kosatka
není	být	k5eNaImIp3nS	být
nebezpečná	bezpečný	k2eNgFnSc1d1	nebezpečná
člověku	člověk	k1gMnSc6	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
nebyl	být	k5eNaImAgInS	být
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
žádný	žádný	k3yNgInSc1	žádný
záměrný	záměrný	k2eAgInSc1d1	záměrný
útok	útok	k1gInSc1	útok
kosatky	kosatka	k1gFnSc2	kosatka
na	na	k7c4	na
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
pouze	pouze	k6eAd1	pouze
k	k	k7c3	k
několika	několik	k4yIc3	několik
málo	málo	k4c3	málo
případům	případ	k1gInPc3	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
kosatky	kosatka	k1gFnPc1	kosatka
napadly	napadnout	k5eAaPmAgFnP	napadnout
člověka	člověk	k1gMnSc4	člověk
či	či	k8xC	či
malou	malý	k2eAgFnSc4d1	malá
loď	loď	k1gFnSc4	loď
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
si	se	k3xPyFc3	se
je	být	k5eAaImIp3nS	být
spletly	splést	k5eAaPmAgInP	splést
se	s	k7c7	s
svojí	svůj	k3xOyFgFnSc7	svůj
oblíbenou	oblíbený	k2eAgFnSc7d1	oblíbená
kořistí	kořist	k1gFnSc7	kořist
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozpoznání	rozpoznání	k1gNnSc6	rozpoznání
omylu	omyl	k1gInSc2	omyl
svůj	svůj	k3xOyFgInSc4	svůj
útok	útok	k1gInSc4	útok
vždy	vždy	k6eAd1	vždy
okamžitě	okamžitě	k6eAd1	okamžitě
ukončily	ukončit	k5eAaPmAgInP	ukončit
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
u	u	k7c2	u
kosatek	kosatka	k1gFnPc2	kosatka
chovaných	chovaný	k2eAgFnPc2d1	chovaná
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
se	se	k3xPyFc4	se
vyskytlo	vyskytnout	k5eAaPmAgNnS	vyskytnout
několik	několik	k4yIc1	několik
málo	málo	k4c1	málo
případů	případ	k1gInPc2	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
kosatky	kosatka	k1gFnPc1	kosatka
napadly	napadnout	k5eAaPmAgFnP	napadnout
personál	personál	k1gInSc4	personál
mořského	mořský	k2eAgNnSc2d1	mořské
akvária	akvárium	k1gNnSc2	akvárium
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pojmenování	pojmenování	k1gNnPc1	pojmenování
==	==	k?	==
</s>
</p>
<p>
<s>
Jméno	jméno	k1gNnSc1	jméno
"	"	kIx"	"
<g/>
Orca	Orca	k1gFnSc1	Orca
<g/>
"	"	kIx"	"
dali	dát	k5eAaPmAgMnP	dát
tomuto	tento	k3xDgNnSc3	tento
savci	savec	k1gMnPc1	savec
již	již	k6eAd1	již
staří	starý	k2eAgMnPc1d1	starý
Římané	Říman	k1gMnPc1	Říman
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
ho	on	k3xPp3gMnSc4	on
odvodili	odvodit	k5eAaPmAgMnP	odvodit
od	od	k7c2	od
řeckého	řecký	k2eAgNnSc2d1	řecké
slova	slovo	k1gNnSc2	slovo
ὄ	ὄ	k?	ὄ
(	(	kIx(	(
<g/>
oryx	oryx	k1gInSc1	oryx
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
všeobecně	všeobecně	k6eAd1	všeobecně
přisuzuje	přisuzovat	k5eAaImIp3nS	přisuzovat
velrybám	velryba	k1gFnPc3	velryba
<g/>
,	,	kIx,	,
velkým	velký	k2eAgFnPc3d1	velká
rybám	ryba	k1gFnPc3	ryba
nebo	nebo	k8xC	nebo
mořským	mořský	k2eAgFnPc3d1	mořská
příšerám	příšera	k1gFnPc3	příšera
<g/>
.	.	kIx.	.
</s>
<s>
Přídavné	přídavný	k2eAgNnSc1d1	přídavné
jméno	jméno	k1gNnSc1	jméno
Orcino	Orcin	k2eAgNnSc1d1	Orcin
zase	zase	k9	zase
znamená	znamenat	k5eAaImIp3nS	znamenat
démon	démon	k1gMnSc1	démon
pocházející	pocházející	k2eAgMnSc1d1	pocházející
z	z	k7c2	z
pekla	peklo	k1gNnSc2	peklo
<g/>
:	:	kIx,	:
Orco	Orco	k6eAd1	Orco
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
Římany	Říman	k1gMnPc4	Říman
bůh	bůh	k1gMnSc1	bůh
podzemí	podzemí	k1gNnPc2	podzemí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
České	český	k2eAgNnSc1d1	české
pojmenování	pojmenování	k1gNnSc1	pojmenování
kosatka	kosatka	k1gFnSc1	kosatka
bylo	být	k5eAaImAgNnS	být
přejato	přejmout	k5eAaPmNgNnS	přejmout
do	do	k7c2	do
vědeckého	vědecký	k2eAgNnSc2d1	vědecké
názvosloví	názvosloví	k1gNnSc2	názvosloví
z	z	k7c2	z
ruštiny	ruština	k1gFnSc2	ruština
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
obecně	obecně	k6eAd1	obecně
přijímáno	přijímat	k5eAaImNgNnS	přijímat
<g/>
.	.	kIx.	.
</s>
<s>
Složitější	složitý	k2eAgFnSc1d2	složitější
je	být	k5eAaImIp3nS	být
problematika	problematika	k1gFnSc1	problematika
pojmenování	pojmenování	k1gNnSc2	pojmenování
kosatky	kosatka	k1gFnSc2	kosatka
dravé	dravý	k2eAgFnSc2d1	dravá
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
v	v	k7c6	v
anglicky	anglicky	k6eAd1	anglicky
mluvících	mluvící	k2eAgFnPc6d1	mluvící
zemích	zem	k1gFnPc6	zem
za	za	k7c4	za
výrazné	výrazný	k2eAgFnPc4d1	výrazná
podpory	podpora	k1gFnPc4	podpora
vědců	vědec	k1gMnPc2	vědec
prosazuje	prosazovat	k5eAaImIp3nS	prosazovat
označení	označení	k1gNnSc4	označení
druhu	druh	k1gInSc2	druh
vědeckým	vědecký	k2eAgNnSc7d1	vědecké
jménem	jméno	k1gNnSc7	jméno
orca	orcum	k1gNnSc2	orcum
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
populárnější	populární	k2eAgMnSc1d2	populárnější
než	než	k8xS	než
tradiční	tradiční	k2eAgNnSc1d1	tradiční
jméno	jméno	k1gNnSc1	jméno
killer	killer	k1gInSc1	killer
whale	whale	k1gFnSc1	whale
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
velryba	velryba	k1gFnSc1	velryba
zabiják	zabiják	k1gMnSc1	zabiják
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
ten	ten	k3xDgMnSc1	ten
je	být	k5eAaImIp3nS	být
veřejností	veřejnost	k1gFnSc7	veřejnost
stále	stále	k6eAd1	stále
široce	široko	k6eAd1	široko
používán	používat	k5eAaImNgInS	používat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc1	několik
důvodů	důvod	k1gInPc2	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
je	být	k5eAaImIp3nS	být
(	(	kIx(	(
<g/>
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
ohledech	ohled	k1gInPc6	ohled
cíleně	cíleně	k6eAd1	cíleně
<g/>
)	)	kIx)	)
prosazována	prosazován	k2eAgFnSc1d1	prosazována
změna	změna	k1gFnSc1	změna
názvu	název	k1gInSc2	název
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k6eAd1	především
je	být	k5eAaImIp3nS	být
slovo	slovo	k1gNnSc1	slovo
velryba	velryba	k1gFnSc1	velryba
v	v	k7c6	v
názvu	název	k1gInSc6	název
delfínovitého	delfínovitý	k2eAgInSc2d1	delfínovitý
druhu	druh	k1gInSc2	druh
matoucí	matoucí	k2eAgMnSc1d1	matoucí
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
druhé	druhý	k4xOgNnSc4	druhý
je	být	k5eAaImIp3nS	být
název	název	k1gInSc1	název
orca	orca	k6eAd1	orca
obvyklý	obvyklý	k2eAgInSc4d1	obvyklý
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
evropských	evropský	k2eAgInPc2d1	evropský
jazyků	jazyk	k1gInPc2	jazyk
a	a	k8xC	a
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
spolupráce	spolupráce	k1gFnSc2	spolupráce
při	při	k7c6	při
výzkumu	výzkum	k1gInSc6	výzkum
druhů	druh	k1gInPc2	druh
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
významný	významný	k2eAgInSc1d1	významný
tlak	tlak	k1gInSc1	tlak
na	na	k7c6	na
sjednocení	sjednocení	k1gNnSc6	sjednocení
jmen	jméno	k1gNnPc2	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
slovo	slovo	k1gNnSc1	slovo
zabiják	zabiják	k1gInSc1	zabiják
často	často	k6eAd1	často
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
mylný	mylný	k2eAgInSc4d1	mylný
dojem	dojem	k1gInSc4	dojem
<g/>
,	,	kIx,	,
že	že	k8xS	že
tvor	tvor	k1gMnSc1	tvor
zabíjí	zabíjet	k5eAaImIp3nS	zabíjet
lidi	člověk	k1gMnPc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
reputaci	reputace	k1gFnSc4	reputace
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
napravit	napravit	k5eAaPmF	napravit
jen	jen	k9	jen
prosazením	prosazení	k1gNnSc7	prosazení
odlišného	odlišný	k2eAgNnSc2d1	odlišné
jména	jméno	k1gNnSc2	jméno
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
má	mít	k5eAaImIp3nS	mít
usnadnit	usnadnit	k5eAaPmF	usnadnit
prosazování	prosazování	k1gNnSc4	prosazování
ochrany	ochrana	k1gFnSc2	ochrana
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Všeobecně	všeobecně	k6eAd1	všeobecně
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
velryba	velryba	k1gFnSc1	velryba
zabiják	zabiják	k1gMnSc1	zabiják
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
killer	killer	k1gInSc1	killer
whale	whale	k1gInSc1	whale
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
špatným	špatný	k2eAgInSc7d1	špatný
překladem	překlad	k1gInSc7	překlad
jména	jméno	k1gNnSc2	jméno
z	z	k7c2	z
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
zvířeti	zvíře	k1gNnSc6	zvíře
dali	dát	k5eAaPmAgMnP	dát
španělští	španělský	k2eAgMnPc1d1	španělský
námořníci	námořník	k1gMnPc1	námořník
a	a	k8xC	a
který	který	k3yRgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
být	být	k5eAaImF	být
správněji	správně	k6eAd2	správně
přeložen	přeložit	k5eAaPmNgInS	přeložit
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
zabiják	zabiják	k1gInSc1	zabiják
velryb	velryba	k1gFnPc2	velryba
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
whale-killer	whaleiller	k1gInSc1	whale-killer
[	[	kIx(	[
<g/>
ˈ	ˈ	k?	ˈ
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
původní	původní	k2eAgNnSc4d1	původní
jméno	jméno	k1gNnSc4	jméno
bylo	být	k5eAaImAgNnS	být
jen	jen	k9	jen
špatným	špatný	k2eAgInSc7d1	špatný
překladem	překlad	k1gInSc7	překlad
<g/>
,	,	kIx,	,
také	také	k9	také
podpořila	podpořit	k5eAaPmAgFnS	podpořit
snahu	snaha	k1gFnSc4	snaha
o	o	k7c4	o
změnu	změna	k1gFnSc4	změna
jména	jméno	k1gNnSc2	jméno
<g/>
.	.	kIx.	.
<g/>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc3	množství
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
dávají	dávat	k5eAaImIp3nP	dávat
přednost	přednost	k1gFnSc4	přednost
původnímu	původní	k2eAgInSc3d1	původní
názvu	název	k1gInSc3	název
<g/>
,	,	kIx,	,
poukazuje	poukazovat	k5eAaImIp3nS	poukazovat
na	na	k7c4	na
fakt	fakt	k1gInSc4	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
kosatky	kosatka	k1gFnPc1	kosatka
nepochybně	pochybně	k6eNd1	pochybně
zabíjejí	zabíjet	k5eAaImIp3nP	zabíjet
mnoho	mnoho	k4c4	mnoho
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
jiných	jiný	k2eAgMnPc2d1	jiný
kytovců	kytovec	k1gMnPc2	kytovec
<g/>
.	.	kIx.	.
</s>
<s>
Zastánci	zastánce	k1gMnPc1	zastánce
původního	původní	k2eAgInSc2d1	původní
názvu	název	k1gInSc2	název
argumentují	argumentovat	k5eAaImIp3nP	argumentovat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
zabíjení	zabíjení	k1gNnSc1	zabíjení
v	v	k7c6	v
názvu	název	k1gInSc6	název
se	se	k3xPyFc4	se
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
pouze	pouze	k6eAd1	pouze
u	u	k7c2	u
španělských	španělský	k2eAgMnPc2d1	španělský
námořníků	námořník	k1gMnPc2	námořník
<g/>
.	.	kIx.	.
</s>
<s>
Kmeny	kmen	k1gInPc1	kmen
Haida	Haido	k1gNnSc2	Haido
v	v	k7c6	v
Britské	britský	k2eAgFnSc6d1	britská
Kolumbii	Kolumbie	k1gFnSc6	Kolumbie
nazývají	nazývat	k5eAaImIp3nP	nazývat
zvíře	zvíře	k1gNnSc4	zvíře
skana	skan	k1gInSc2	skan
(	(	kIx(	(
<g/>
zabíjející	zabíjející	k2eAgMnSc1d1	zabíjející
démon	démon	k1gMnSc1	démon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Japonci	Japonec	k1gMnPc1	Japonec
je	on	k3xPp3gMnPc4	on
nazývají	nazývat	k5eAaImIp3nP	nazývat
šači	šač	k1gFnPc1	šač
(	(	kIx(	(
<g/>
鯱	鯱	k?	鯱
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
kandži	kandzat	k5eAaPmIp1nS	kandzat
znak	znak	k1gInSc1	znak
kombinuje	kombinovat	k5eAaImIp3nS	kombinovat
radikály	radikál	k1gInPc4	radikál
pro	pro	k7c4	pro
rybu	ryba	k1gFnSc4	ryba
(	(	kIx(	(
<g/>
魚	魚	k?	魚
<g/>
)	)	kIx)	)
a	a	k8xC	a
tygra	tygr	k1gMnSc2	tygr
(	(	kIx(	(
<g/>
虎	虎	k?	虎
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jiný	jiný	k2eAgInSc1d1	jiný
anglický	anglický	k2eAgInSc1d1	anglický
název	název	k1gInSc1	název
pro	pro	k7c4	pro
kosatku	kosatka	k1gFnSc4	kosatka
je	být	k5eAaImIp3nS	být
Grampus	Grampus	k1gInSc1	Grampus
<g/>
.	.	kIx.	.
</s>
<s>
Koliduje	kolidovat	k5eAaImIp3nS	kolidovat
však	však	k9	však
s	s	k7c7	s
názvem	název	k1gInSc7	název
jiného	jiný	k2eAgMnSc2d1	jiný
kytovce	kytovec	k1gMnSc2	kytovec
(	(	kIx(	(
<g/>
Grampus	Grampus	k1gMnSc1	Grampus
griseus	griseus	k1gMnSc1	griseus
–	–	k?	–
plískavice	plískavice	k1gFnSc1	plískavice
šedá	šedat	k5eAaImIp3nS	šedat
<g/>
)	)	kIx)	)
a	a	k8xC	a
nyní	nyní	k6eAd1	nyní
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jen	jen	k9	jen
zřídka	zřídka	k6eAd1	zřídka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Taxonomie	taxonomie	k1gFnSc2	taxonomie
a	a	k8xC	a
evoluce	evoluce	k1gFnSc2	evoluce
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
se	se	k3xPyFc4	se
rodové	rodový	k2eAgNnSc1d1	rodové
jméno	jméno	k1gNnSc1	jméno
kosatka	kosatka	k1gFnSc1	kosatka
používá	používat	k5eAaImIp3nS	používat
i	i	k9	i
pro	pro	k7c4	pro
kosatku	kosatka	k1gFnSc4	kosatka
černou	černý	k2eAgFnSc4d1	černá
(	(	kIx(	(
<g/>
Pseudorca	Pseudorca	k1gMnSc1	Pseudorca
crassidens	crassidensa	k1gFnPc2	crassidensa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
však	však	k9	však
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
nepatří	patřit	k5eNaImIp3nP	patřit
do	do	k7c2	do
stejného	stejný	k2eAgInSc2d1	stejný
rodu	rod	k1gInSc2	rod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
rod	rod	k1gInSc1	rod
kosatka	kosatka	k1gFnSc1	kosatka
(	(	kIx(	(
<g/>
Orcinus	Orcinus	k1gInSc1	Orcinus
<g/>
)	)	kIx)	)
čítá	čítat	k5eAaImIp3nS	čítat
pouze	pouze	k6eAd1	pouze
jeden	jeden	k4xCgInSc4	jeden
druh	druh	k1gInSc4	druh
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
kosatku	kosatka	k1gFnSc4	kosatka
dravou	dravý	k2eAgFnSc4d1	dravá
(	(	kIx(	(
<g/>
Orcinus	Orcinus	k1gMnSc1	Orcinus
orca	orca	k1gMnSc1	orca
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
však	však	k9	však
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
budoucnu	budoucno	k1gNnSc6	budoucno
rozdělí	rozdělit	k5eAaPmIp3nS	rozdělit
na	na	k7c4	na
více	hodně	k6eAd2	hodně
poddruhů	poddruh	k1gInPc2	poddruh
či	či	k8xC	či
dokonce	dokonce	k9	dokonce
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
<g/>
Stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
u	u	k7c2	u
vorvaňova	vorvaňův	k2eAgInSc2d1	vorvaňův
<g />
.	.	kIx.	.
</s>
<s>
rodu	rod	k1gInSc3	rod
Physeter	Physeter	k1gInSc1	Physeter
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Orcinus	Orcinus	k1gMnSc1	Orcinus
z	z	k7c2	z
kladistického	kladistický	k2eAgInSc2d1	kladistický
pohledu	pohled	k1gInSc2	pohled
rodem	rod	k1gInSc7	rod
s	s	k7c7	s
jedním	jeden	k4xCgInSc7	jeden
rozšířeným	rozšířený	k2eAgInSc7d1	rozšířený
druhem	druh	k1gInSc7	druh
bez	bez	k7c2	bez
přímých	přímý	k2eAgMnPc2d1	přímý
příbuzných	příbuzný	k1gMnPc2	příbuzný
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
paleontologové	paleontolog	k1gMnPc1	paleontolog
věří	věřit	k5eAaImIp3nP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
kosatka	kosatka	k1gFnSc1	kosatka
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgMnSc7d1	hlavní
kandidátem	kandidát	k1gMnSc7	kandidát
majícím	mající	k2eAgInSc7d1	mající
anagenetickou	anagenetický	k2eAgFnSc7d1	anagenetický
evoluční	evoluční	k2eAgFnSc7d1	evoluční
historii	historie	k1gFnSc4	historie
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
že	že	k8xS	že
vývoj	vývoj	k1gInSc4	vývoj
druhu	druh	k1gInSc2	druh
od	od	k7c2	od
předka	předek	k1gMnSc2	předek
k	k	k7c3	k
potomkovi	potomek	k1gMnSc3	potomek
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
bez	bez	k7c2	bez
rozdělení	rozdělení	k1gNnSc2	rozdělení
linie	linie	k1gFnSc2	linie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nP	by
měli	mít	k5eAaImAgMnP	mít
pravdu	pravda	k1gFnSc4	pravda
<g/>
,	,	kIx,	,
činilo	činit	k5eAaImAgNnS	činit
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
z	z	k7c2	z
kosatky	kosatka	k1gFnSc2	kosatka
dravé	dravý	k2eAgFnSc2d1	dravá
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgInPc2d3	nejstarší
druhů	druh	k1gInPc2	druh
delfínovitých	delfínovitý	k2eAgInPc2d1	delfínovitý
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
ne	ne	k9	ne
tak	tak	k6eAd1	tak
starý	starý	k2eAgInSc4d1	starý
jako	jako	k8xS	jako
čeleď	čeleď	k1gFnSc4	čeleď
samotnou	samotný	k2eAgFnSc4d1	samotná
<g/>
,	,	kIx,	,
starou	starý	k2eAgFnSc4d1	stará
přinejmenším	přinejmenším	k6eAd1	přinejmenším
5	[number]	k4	5
miliónů	milión	k4xCgInPc2	milión
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
Kosatka	kosatka	k1gFnSc1	kosatka
dravá	dravý	k2eAgFnSc1d1	dravá
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
ekotypech	ekotyp	k1gInPc6	ekotyp
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
ekotypy	ekotyp	k1gInPc4	ekotyp
žijící	žijící	k2eAgInPc4d1	žijící
v	v	k7c6	v
Severním	severní	k2eAgNnSc6d1	severní
Pacifiku	Pacifik	k1gInSc6	Pacifik
patří	patřit	k5eAaImIp3nS	patřit
ekotyp	ekotyp	k1gInSc1	ekotyp
residentní	residentní	k2eAgInSc1d1	residentní
<g/>
,	,	kIx,	,
transientní	transientní	k2eAgInSc1d1	transientní
a	a	k8xC	a
ekotyp	ekotyp	k1gInSc1	ekotyp
žijící	žijící	k2eAgMnSc1d1	žijící
v	v	k7c6	v
šelfových	šelfový	k2eAgNnPc6d1	šelfové
mořích	moře	k1gNnPc6	moře
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
offshore	offshor	k1gInSc5	offshor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Residentní	Residentní	k2eAgInSc1d1	Residentní
ekotyp	ekotyp	k1gInSc1	ekotyp
je	být	k5eAaImIp3nS	být
převážně	převážně	k6eAd1	převážně
rybožravý	rybožravý	k2eAgInSc1d1	rybožravý
<g/>
,	,	kIx,	,
přednostně	přednostně	k6eAd1	přednostně
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nP	živit
lososovitými	lososovitý	k2eAgFnPc7d1	lososovitá
rybami	ryba	k1gFnPc7	ryba
(	(	kIx(	(
<g/>
losos	losos	k1gMnSc1	losos
čavyča	čavyča	k1gFnSc1	čavyča
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
loví	lovit	k5eAaImIp3nP	lovit
však	však	k9	však
i	i	k9	i
jiné	jiný	k2eAgFnPc4d1	jiná
ryby	ryba	k1gFnPc4	ryba
(	(	kIx(	(
<g/>
halibut	halibut	k1gMnSc1	halibut
<g/>
,	,	kIx,	,
hřebeník	hřebeník	k1gMnSc1	hřebeník
ad	ad	k7c4	ad
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Utváří	utvářit	k5eAaPmIp3nP	utvářit
matrilineární	matrilineární	k2eAgNnPc4d1	matrilineární
stáda	stádo	k1gNnPc4	stádo
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnPc1	jejichž
členové	člen	k1gMnPc1	člen
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
komunikují	komunikovat	k5eAaImIp3nP	komunikovat
unikátními	unikátní	k2eAgInPc7d1	unikátní
dialekty	dialekt	k1gInPc7	dialekt
<g/>
,	,	kIx,	,
obecně	obecně	k6eAd1	obecně
hodně	hodně	k6eAd1	hodně
vokalizuje	vokalizovat	k5eAaBmIp3nS	vokalizovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
populace	populace	k1gFnPc4	populace
residentního	residentní	k2eAgInSc2d1	residentní
ekotypu	ekotyp	k1gInSc2	ekotyp
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
jižní	jižní	k2eAgMnPc1d1	jižní
residenti	resident	k1gMnPc1	resident
<g/>
:	:	kIx,	:
<g/>
J	J	kA	J
,	,	kIx,	,
K	K	kA	K
<g/>
,	,	kIx,	,
L	L	kA	L
stádaseverní	stádasevernit	k5eAaPmIp3nP	stádasevernit
residenti	resident	k1gMnPc1	resident
<g/>
:	:	kIx,	:
A	a	k9	a
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
A	A	kA	A
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
A	A	kA	A
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
B	B	kA	B
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
C	C	kA	C
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
D	D	kA	D
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
G	G	kA	G
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
G	G	kA	G
<g />
.	.	kIx.	.
</s>
<s>
<g/>
12	[number]	k4	12
<g/>
,	,	kIx,	,
H	H	kA	H
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
I	I	kA	I
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
I	I	kA	I
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
I	I	kA	I
<g/>
11	[number]	k4	11
<g/>
,	,	kIx,	,
I	I	kA	I
<g/>
18	[number]	k4	18
<g/>
,	,	kIx,	,
I	I	kA	I
<g/>
31	[number]	k4	31
<g/>
,	,	kIx,	,
R	R	kA	R
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
W1	W1	k1gFnSc4	W1
stádajižní	stádajižný	k2eAgMnPc1d1	stádajižný
aljašští	aljašský	k2eAgMnPc1d1	aljašský
residenti	resident	k1gMnPc1	resident
<g/>
:	:	kIx,	:
AB	AB	kA	AB
<g/>
,	,	kIx,	,
AB	AB	kA	AB
<g/>
<g />
.	.	kIx.	.
</s>
<s>
25	[number]	k4	25
<g/>
,	,	kIx,	,
AD	ad	k7c4	ad
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
AD	ad	k7c4	ad
<g/>
8	[number]	k4	8
<g/>
,	,	kIx,	,
AD	ad	k7c4	ad
<g/>
11	[number]	k4	11
<g/>
,	,	kIx,	,
AD	ad	k7c4	ad
<g/>
16	[number]	k4	16
<g/>
,	,	kIx,	,
AE	AE	kA	AE
<g/>
,	,	kIx,	,
AF	AF	kA	AF
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
AF	AF	kA	AF
<g/>
22	[number]	k4	22
<g/>
,	,	kIx,	,
AG	AG	kA	AG
<g/>
,	,	kIx,	,
AI	AI	kA	AI
<g/>
,	,	kIx,	,
AJ	aj	kA	aj
<g/>
,	,	kIx,	,
AJ	aj	kA	aj
<g/>
8	[number]	k4	8
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
AK	AK	kA	AK
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
AK	AK	kA	AK
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
AN	AN	kA	AN
<g/>
10	[number]	k4	10
<g/>
,	,	kIx,	,
AX	AX	kA	AX
<g/>
,	,	kIx,	,
AY	AY	kA	AY
stádazápadní	stádazápadní	k2eAgFnSc1d1	stádazápadní
aljašští	aljašský	k2eAgMnPc1d1	aljašský
residentiTransientní	residentiTransientní	k2eAgMnPc4d1	residentiTransientní
(	(	kIx(	(
<g/>
též	též	k9	též
Biggsův	Biggsův	k2eAgInSc1d1	Biggsův
<g/>
)	)	kIx)	)
ekotyp	ekotyp	k1gInSc1	ekotyp
loví	lovit	k5eAaImIp3nS	lovit
mořské	mořský	k2eAgMnPc4d1	mořský
savce	savec	k1gMnPc4	savec
(	(	kIx(	(
<g/>
tuleně	tuleň	k1gMnPc4	tuleň
<g/>
,	,	kIx,	,
lachtany	lachtan	k1gMnPc4	lachtan
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgMnPc4d1	jiný
kytovce	kytovec	k1gMnPc4	kytovec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nP	tvořit
menší	malý	k2eAgNnPc1d2	menší
stáda	stádo	k1gNnPc1	stádo
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
vokalizuje	vokalizovat	k5eAaBmIp3nS	vokalizovat
(	(	kIx(	(
<g/>
aby	aby	k9	aby
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
neupoutal	upoutat	k5eNaPmAgMnS	upoutat
pozornost	pozornost	k1gFnSc4	pozornost
kořisti	kořist	k1gFnSc2	kořist
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
menší	malý	k2eAgNnPc1d2	menší
matrilineární	matrilineární	k2eAgNnPc1d1	matrilineární
stáda	stádo	k1gNnPc1	stádo
do	do	k7c2	do
cca	cca	kA	cca
10	[number]	k4	10
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
residentů	resident	k1gMnPc2	resident
někteří	některý	k3yIgMnPc1	některý
v	v	k7c6	v
dospělosti	dospělost	k1gFnSc6	dospělost
opouští	opouštět	k5eAaImIp3nS	opouštět
mateřské	mateřský	k2eAgNnSc1d1	mateřské
stádo	stádo	k1gNnSc1	stádo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ekotyp	Ekotyp	k1gMnSc1	Ekotyp
žijící	žijící	k2eAgMnSc1d1	žijící
v	v	k7c6	v
šelfových	šelfový	k2eAgNnPc6d1	šelfové
mořích	moře	k1gNnPc6	moře
(	(	kIx(	(
<g/>
offshore	offshor	k1gMnSc5	offshor
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejméně	málo	k6eAd3	málo
prozkoumaný	prozkoumaný	k2eAgInSc1d1	prozkoumaný
<g/>
.	.	kIx.	.
</s>
<s>
Nejspíše	nejspíše	k9	nejspíše
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nP	živit
především	především	k9	především
lovem	lov	k1gInSc7	lov
ryb	ryba	k1gFnPc2	ryba
a	a	k8xC	a
paryb	paryba	k1gFnPc2	paryba
(	(	kIx(	(
<g/>
žraloci	žralok	k1gMnPc1	žralok
<g/>
,	,	kIx,	,
halibut	halibut	k1gMnSc1	halibut
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bohatě	bohatě	k6eAd1	bohatě
vokalizují	vokalizovat	k5eAaBmIp3nP	vokalizovat
<g/>
,	,	kIx,	,
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
ve	v	k7c6	v
větších	veliký	k2eAgNnPc6d2	veliký
stádech	stádo	k1gNnPc6	stádo
(	(	kIx(	(
<g/>
20	[number]	k4	20
<g/>
–	–	k?	–
<g/>
70	[number]	k4	70
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
,	,	kIx,	,
občas	občas	k6eAd1	občas
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
vidět	vidět	k5eAaImF	vidět
i	i	k9	i
skupinu	skupina	k1gFnSc4	skupina
čítající	čítající	k2eAgFnSc4d1	čítající
okolo	okolo	k6eAd1	okolo
200	[number]	k4	200
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Severním	severní	k2eAgInSc6d1	severní
Atlantiku	Atlantik	k1gInSc6	Atlantik
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
2	[number]	k4	2
ekotypy	ekotyp	k1gInPc4	ekotyp
<g/>
,	,	kIx,	,
typ	typ	k1gInSc4	typ
1	[number]	k4	1
a	a	k8xC	a
typ	typ	k1gInSc1	typ
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Typ	typ	k1gInSc1	typ
1	[number]	k4	1
loví	lovit	k5eAaImIp3nS	lovit
nejspíše	nejspíše	k9	nejspíše
ryby	ryba	k1gFnPc4	ryba
i	i	k8xC	i
mořské	mořský	k2eAgMnPc4d1	mořský
savce	savec	k1gMnPc4	savec
<g/>
,	,	kIx,	,
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
značné	značný	k2eAgNnSc1d1	značné
opotřebování	opotřebování	k1gNnSc1	opotřebování
chrupu	chrup	k1gInSc2	chrup
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
sem	sem	k6eAd1	sem
skupiny	skupina	k1gFnPc4	skupina
žijící	žijící	k2eAgFnPc4d1	žijící
u	u	k7c2	u
pobřeží	pobřeží	k1gNnSc2	pobřeží
Norska	Norsko	k1gNnSc2	Norsko
<g/>
,	,	kIx,	,
Skotska	Skotsko	k1gNnSc2	Skotsko
a	a	k8xC	a
Islandu	Island	k1gInSc2	Island
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Typ	typ	k1gInSc1	typ
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
typ	typ	k1gInSc1	typ
1	[number]	k4	1
<g/>
,	,	kIx,	,
nejspíše	nejspíše	k9	nejspíše
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nP	živit
výhradně	výhradně	k6eAd1	výhradně
mořskými	mořský	k2eAgMnPc7d1	mořský
savci	savec	k1gMnPc7	savec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
vodách	voda	k1gFnPc6	voda
Jižního	jižní	k2eAgInSc2d1	jižní
oceánu	oceán	k1gInSc2	oceán
byly	být	k5eAaImAgInP	být
pozorovány	pozorovat	k5eAaImNgInP	pozorovat
4	[number]	k4	4
ekotypy	ekotyp	k1gInPc1	ekotyp
<g/>
,	,	kIx,	,
typ	typ	k1gInSc1	typ
A	A	kA	A
<g/>
,	,	kIx,	,
B	B	kA	B
<g/>
,	,	kIx,	,
C	C	kA	C
a	a	k8xC	a
D.	D.	kA	D.
</s>
</p>
<p>
<s>
Typ	typ	k1gInSc1	typ
A	a	k9	a
nejspíše	nejspíše	k9	nejspíše
loví	lovit	k5eAaImIp3nP	lovit
především	především	k9	především
plejtváka	plejtvák	k1gMnSc4	plejtvák
malého	malý	k2eAgMnSc4d1	malý
<g/>
,	,	kIx,	,
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
více	hodně	k6eAd2	hodně
na	na	k7c6	na
otevřeném	otevřený	k2eAgNnSc6d1	otevřené
moři	moře	k1gNnSc6	moře
mimo	mimo	k7c4	mimo
pole	pole	k1gNnSc4	pole
ledových	ledový	k2eAgFnPc2d1	ledová
ker	kra	k1gFnPc2	kra
<g/>
,	,	kIx,	,
podobný	podobný	k2eAgInSc1d1	podobný
transientnímu	transientní	k2eAgInSc3d1	transientní
ekotypu	ekotyp	k1gInSc3	ekotyp
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Typ	typ	k1gInSc1	typ
B	B	kA	B
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
ve	v	k7c6	v
2	[number]	k4	2
subekotypech	subekotyp	k1gInPc6	subekotyp
<g/>
,	,	kIx,	,
velkém	velký	k2eAgNnSc6d1	velké
a	a	k8xC	a
malém	malý	k2eAgNnSc6d1	malé
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
typ	typ	k1gInSc1	typ
B	B	kA	B
je	být	k5eAaImIp3nS	být
menšího	malý	k2eAgInSc2d2	menší
vzrůstu	vzrůst	k1gInSc2	vzrůst
než	než	k8xS	než
typ	typ	k1gInSc4	typ
A	A	kA	A
<g/>
,	,	kIx,	,
spíše	spíše	k9	spíše
šedé	šedý	k2eAgFnPc1d1	šedá
barvy	barva	k1gFnPc1	barva
<g/>
,	,	kIx,	,
usazování	usazování	k1gNnSc1	usazování
rozsivek	rozsivka	k1gFnPc2	rozsivka
na	na	k7c4	na
kůži	kůže	k1gFnSc4	kůže
zabarvuje	zabarvovat	k5eAaImIp3nS	zabarvovat
bílá	bílý	k2eAgNnPc4d1	bílé
místa	místo	k1gNnPc4	místo
na	na	k7c6	na
těle	tělo	k1gNnSc6	tělo
do	do	k7c2	do
hnědožluta	hnědožlut	k1gMnSc2	hnědožlut
<g/>
,	,	kIx,	,
loví	lovit	k5eAaImIp3nP	lovit
především	především	k9	především
tuleně	tuleň	k1gMnPc4	tuleň
v	v	k7c6	v
ledových	ledový	k2eAgFnPc6d1	ledová
polích	pole	k1gFnPc6	pole
<g/>
.	.	kIx.	.
</s>
<s>
Malý	malý	k2eAgInSc1d1	malý
typ	typ	k1gInSc1	typ
B	B	kA	B
bývá	bývat	k5eAaImIp3nS	bývat
nazýván	nazývat	k5eAaImNgInS	nazývat
typem	typ	k1gInSc7	typ
Gerlacheovým	Gerlacheův	k2eAgInSc7d1	Gerlacheův
<g/>
,	,	kIx,	,
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
nejvíce	hodně	k6eAd3	hodně
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Gerlacheovy	Gerlacheův	k2eAgFnSc2d1	Gerlacheův
úžiny	úžina	k1gFnSc2	úžina
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
pozorování	pozorování	k1gNnSc2	pozorování
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nS	živit
hlavně	hlavně	k9	hlavně
tučňáky	tučňák	k1gMnPc4	tučňák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Typ	typ	k1gInSc1	typ
C	C	kA	C
<g/>
,	,	kIx,	,
též	též	k9	též
typ	typ	k1gInSc1	typ
Rossova	Rossův	k2eAgNnSc2d1	Rossovo
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
šedé	šedý	k2eAgFnPc4d1	šedá
barvy	barva	k1gFnPc4	barva
<g/>
,	,	kIx,	,
s	s	k7c7	s
kůží	kůže	k1gFnSc7	kůže
často	často	k6eAd1	často
pokrytou	pokrytý	k2eAgFnSc4d1	pokrytá
rozsivkami	rozsivka	k1gFnPc7	rozsivka
<g/>
,	,	kIx,	,
nejmenší	malý	k2eAgInSc1d3	nejmenší
známý	známý	k2eAgInSc1d1	známý
typ	typ	k1gInSc1	typ
kosatky	kosatka	k1gFnSc2	kosatka
(	(	kIx(	(
<g/>
okolo	okolo	k7c2	okolo
6	[number]	k4	6
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
ve	v	k7c6	v
východních	východní	k2eAgFnPc6d1	východní
antarktických	antarktický	k2eAgFnPc6d1	antarktická
vodách	voda	k1gFnPc6	voda
v	v	k7c6	v
poli	pole	k1gNnSc6	pole
ledových	ledový	k2eAgFnPc2d1	ledová
ker	kra	k1gFnPc2	kra
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
pozorován	pozorovat	k5eAaImNgInS	pozorovat
při	při	k7c6	při
lovu	lov	k1gInSc6	lov
ryby	ryba	k1gFnSc2	ryba
druhu	druh	k1gInSc2	druh
Dissostichus	Dissostichus	k1gInSc1	Dissostichus
mawsoni	mawson	k1gMnPc1	mawson
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Typ	typ	k1gInSc1	typ
D	D	kA	D
je	být	k5eAaImIp3nS	být
snadno	snadno	k6eAd1	snadno
odlišitelný	odlišitelný	k2eAgMnSc1d1	odlišitelný
podle	podle	k7c2	podle
kulatějšího	kulatý	k2eAgInSc2d2	kulatější
tvaru	tvar	k1gInSc2	tvar
hlavy	hlava	k1gFnSc2	hlava
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
malého	malý	k2eAgInSc2d1	malý
bílého	bílý	k2eAgInSc2d1	bílý
pruhu	pruh	k1gInSc2	pruh
za	za	k7c7	za
okem	oke	k1gNnSc7	oke
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
subantarktických	subantarktický	k2eAgFnPc6d1	subantarktický
vodách	voda	k1gFnPc6	voda
<g/>
,	,	kIx,	,
spatřen	spatřit	k5eAaPmNgInS	spatřit
při	při	k7c6	při
lovu	lov	k1gInSc6	lov
ledovky	ledovka	k1gFnSc2	ledovka
patagonské	patagonský	k2eAgFnSc2d1	patagonská
<g/>
.	.	kIx.	.
</s>
<s>
Spekuluje	spekulovat	k5eAaImIp3nS	spekulovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
zdali	zdali	k8xS	zdali
nejde	jít	k5eNaImIp3nS	jít
o	o	k7c4	o
samostatný	samostatný	k2eAgInSc4d1	samostatný
druh	druh	k1gInSc4	druh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Zvířata	zvíře	k1gNnPc1	zvíře
jsou	být	k5eAaImIp3nP	být
charakteristicky	charakteristicky	k6eAd1	charakteristicky
kontrastně	kontrastně	k6eAd1	kontrastně
zbarvena	zbarven	k2eAgFnSc1d1	zbarvena
<g/>
,	,	kIx,	,
s	s	k7c7	s
černým	černý	k2eAgMnSc7d1	černý
hřbetem	hřbet	k1gMnSc7	hřbet
<g/>
,	,	kIx,	,
bílou	bílý	k2eAgFnSc7d1	bílá
hrudí	hruď	k1gFnSc7	hruď
a	a	k8xC	a
boky	bok	k1gInPc7	bok
a	a	k8xC	a
bílou	bílý	k2eAgFnSc7d1	bílá
skvrnou	skvrna	k1gFnSc7	skvrna
nad	nad	k7c7	nad
a	a	k8xC	a
mezi	mezi	k7c7	mezi
očima	oko	k1gNnPc7	oko
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
těžké	těžký	k2eAgNnSc4d1	těžké
a	a	k8xC	a
podsadité	podsaditý	k2eAgNnSc4d1	podsadité
tělo	tělo	k1gNnSc4	tělo
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
hřbetní	hřbetní	k2eAgFnSc7d1	hřbetní
ploutví	ploutev	k1gFnSc7	ploutev
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
9	[number]	k4	9
m	m	kA	m
délky	délka	k1gFnSc2	délka
a	a	k8xC	a
hmotnosti	hmotnost	k1gFnSc2	hmotnost
až	až	k9	až
10	[number]	k4	10
tun	tuna	k1gFnPc2	tuna
<g/>
;	;	kIx,	;
samice	samice	k1gFnPc1	samice
jsou	být	k5eAaImIp3nP	být
menší	malý	k2eAgFnPc1d2	menší
<g/>
,	,	kIx,	,
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
délky	délka	k1gFnSc2	délka
8,5	[number]	k4	8,5
m	m	kA	m
a	a	k8xC	a
hmotnosti	hmotnost	k1gFnSc6	hmotnost
5	[number]	k4	5
tun	tuna	k1gFnPc2	tuna
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
při	při	k7c6	při
narození	narození	k1gNnSc6	narození
váží	vážit	k5eAaImIp3nS	vážit
180	[number]	k4	180
kg	kg	kA	kg
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
2,4	[number]	k4	2,4
m	m	kA	m
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Hřbetní	hřbetní	k2eAgFnSc1d1	hřbetní
ploutev	ploutev	k1gFnSc1	ploutev
je	být	k5eAaImIp3nS	být
vysoká	vysoký	k2eAgFnSc1d1	vysoká
asi	asi	k9	asi
1,8	[number]	k4	1,8
m	m	kA	m
<g/>
,	,	kIx,	,
u	u	k7c2	u
samců	samec	k1gMnPc2	samec
je	být	k5eAaImIp3nS	být
vyšší	vysoký	k2eAgFnSc1d2	vyšší
a	a	k8xC	a
vzpřímenější	vzpřímený	k2eAgFnSc1d2	vzpřímenější
než	než	k8xS	než
u	u	k7c2	u
samic	samice	k1gFnPc2	samice
<g/>
.	.	kIx.	.
</s>
<s>
Plavou	plavý	k2eAgFnSc7d1	plavá
rychlostí	rychlost	k1gFnSc7	rychlost
až	až	k9	až
55	[number]	k4	55
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
,	,	kIx,	,
průměrně	průměrně	k6eAd1	průměrně
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
rychlostí	rychlost	k1gFnSc7	rychlost
10	[number]	k4	10
až	až	k9	až
13	[number]	k4	13
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
<g/>
Dospělí	dospělý	k2eAgMnPc1d1	dospělý
samci	samec	k1gMnPc1	samec
kosatky	kosatka	k1gFnSc2	kosatka
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
impozantní	impozantní	k2eAgFnSc4d1	impozantní
a	a	k8xC	a
jen	jen	k9	jen
těžko	těžko	k6eAd1	těžko
si	se	k3xPyFc3	se
je	být	k5eAaImIp3nS	být
lze	lze	k6eAd1	lze
splést	splést	k5eAaPmF	splést
s	s	k7c7	s
nějakým	nějaký	k3yIgMnSc7	nějaký
jiným	jiný	k2eAgMnSc7d1	jiný
mořským	mořský	k2eAgMnSc7d1	mořský
tvorem	tvor	k1gMnSc7	tvor
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
a	a	k8xC	a
mláďata	mládě	k1gNnPc1	mládě
lze	lze	k6eAd1	lze
v	v	k7c6	v
teplejších	teplý	k2eAgFnPc6d2	teplejší
vodách	voda	k1gFnPc6	voda
na	na	k7c4	na
větší	veliký	k2eAgFnSc4d2	veliký
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
zaměnit	zaměnit	k5eAaPmF	zaměnit
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
podobnými	podobný	k2eAgInPc7d1	podobný
druhy	druh	k1gInPc7	druh
čeledi	čeleď	k1gFnSc3	čeleď
delfínovití	delfínovitý	k2eAgMnPc1d1	delfínovitý
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
kosatka	kosatka	k1gFnSc1	kosatka
černá	černý	k2eAgFnSc1d1	černá
(	(	kIx(	(
<g/>
Pseudorca	Pseudorca	k1gFnSc1	Pseudorca
crassidens	crassidensa	k1gFnPc2	crassidensa
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
plískavice	plískavice	k1gFnSc1	plískavice
šedá	šedá	k1gFnSc1	šedá
(	(	kIx(	(
<g/>
Grampus	Grampus	k1gMnSc1	Grampus
griseus	griseus	k1gMnSc1	griseus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Většina	většina	k1gFnSc1	většina
historických	historický	k2eAgInPc2d1	historický
údajů	údaj	k1gInPc2	údaj
o	o	k7c6	o
kosatkách	kosatka	k1gFnPc6	kosatka
byla	být	k5eAaImAgFnS	být
získána	získán	k2eAgFnSc1d1	získána
z	z	k7c2	z
dlouholetých	dlouholetý	k2eAgFnPc2d1	dlouholetá
zkušeností	zkušenost	k1gFnPc2	zkušenost
obyvatel	obyvatel	k1gMnPc2	obyvatel
pobřeží	pobřeží	k1gNnSc2	pobřeží
Britské	britský	k2eAgFnSc2d1	britská
Kolumbie	Kolumbie	k1gFnSc2	Kolumbie
a	a	k8xC	a
Washingtonu	Washington	k1gInSc2	Washington
a	a	k8xC	a
pozorováním	pozorování	k1gNnSc7	pozorování
zajatých	zajatý	k2eAgFnPc2d1	zajatá
velryb	velryba	k1gFnPc2	velryba
<g/>
.	.	kIx.	.
</s>
<s>
Úplnost	úplnost	k1gFnSc1	úplnost
studia	studio	k1gNnSc2	studio
a	a	k8xC	a
zaznamenaná	zaznamenaný	k2eAgFnSc1d1	zaznamenaná
složitá	složitý	k2eAgFnSc1d1	složitá
sociální	sociální	k2eAgFnSc1d1	sociální
struktura	struktura	k1gFnSc1	struktura
stád	stádo	k1gNnPc2	stádo
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
populaci	populace	k1gFnSc6	populace
nasvědčuje	nasvědčovat	k5eAaImIp3nS	nasvědčovat
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
informace	informace	k1gFnPc1	informace
jsou	být	k5eAaImIp3nP	být
detailní	detailní	k2eAgNnSc4d1	detailní
a	a	k8xC	a
přesné	přesný	k2eAgNnSc4d1	přesné
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
proměnlivé	proměnlivý	k2eAgFnPc1d1	proměnlivá
skupiny	skupina	k1gFnPc1	skupina
a	a	k8xC	a
skupiny	skupina	k1gFnPc1	skupina
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
oceánech	oceán	k1gInPc6	oceán
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
dost	dost	k6eAd1	dost
odlišné	odlišný	k2eAgFnPc4d1	odlišná
sociální	sociální	k2eAgFnPc4d1	sociální
charakteristiky	charakteristika	k1gFnPc4	charakteristika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Samice	samice	k1gFnPc1	samice
poprvé	poprvé	k6eAd1	poprvé
otěhotní	otěhotnět	k5eAaPmIp3nP	otěhotnět
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
15	[number]	k4	15
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
procházejí	procházet	k5eAaImIp3nP	procházet
periodami	perioda	k1gFnPc7	perioda
polyestrických	polyestrický	k2eAgMnPc2d1	polyestrický
cyklů	cyklus	k1gInPc2	cyklus
s	s	k7c7	s
necyklickými	cyklický	k2eNgNnPc7d1	necyklické
obdobími	období	k1gNnPc7	období
tří	tři	k4xCgFnPc2	tři
a	a	k8xC	a
šesti	šest	k4xCc2	šest
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
těhotenství	těhotenství	k1gNnSc2	těhotenství
kolísá	kolísat	k5eAaImIp3nS	kolísat
mezi	mezi	k7c7	mezi
15	[number]	k4	15
a	a	k8xC	a
18	[number]	k4	18
měsíci	měsíc	k1gInPc7	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Matky	matka	k1gFnPc4	matka
rodí	rodit	k5eAaImIp3nS	rodit
jedno	jeden	k4xCgNnSc4	jeden
mládě	mládě	k1gNnSc4	mládě
přibližně	přibližně	k6eAd1	přibližně
každých	každý	k3xTgNnPc2	každý
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
analyzovaných	analyzovaný	k2eAgNnPc6d1	analyzované
stabilních	stabilní	k2eAgNnPc6d1	stabilní
stádech	stádo	k1gNnPc6	stádo
nastává	nastávat	k5eAaImIp3nS	nastávat
porod	porod	k1gInSc1	porod
v	v	k7c4	v
jakoukoliv	jakýkoliv	k3yIgFnSc4	jakýkoliv
roční	roční	k2eAgFnSc4d1	roční
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
k	k	k7c3	k
nejoblíbenějším	oblíbený	k2eAgInPc3d3	nejoblíbenější
měsícům	měsíc	k1gInPc3	měsíc
však	však	k9	však
patří	patřit	k5eAaImIp3nS	patřit
zimní	zimní	k2eAgNnSc1d1	zimní
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Úmrtnost	úmrtnost	k1gFnSc1	úmrtnost
nově	nově	k6eAd1	nově
narozených	narozený	k2eAgNnPc2d1	narozené
mláďat	mládě	k1gNnPc2	mládě
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
vysoká	vysoká	k1gFnSc1	vysoká
–	–	k?	–
podle	podle	k7c2	podle
jednoho	jeden	k4xCgInSc2	jeden
průzkumu	průzkum	k1gInSc2	průzkum
téměř	téměř	k6eAd1	téměř
polovina	polovina	k1gFnSc1	polovina
mláďat	mládě	k1gNnPc2	mládě
nedosáhne	dosáhnout	k5eNaPmIp3nS	dosáhnout
věku	věk	k1gInSc2	věk
šesti	šest	k4xCc2	šest
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
péči	péče	k1gFnSc4	péče
po	po	k7c4	po
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
pevnou	pevný	k2eAgFnSc4d1	pevná
potravu	potrava	k1gFnSc4	potrava
však	však	k9	však
přijímají	přijímat	k5eAaImIp3nP	přijímat
už	už	k6eAd1	už
po	po	k7c6	po
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
<s>
Matky	matka	k1gFnPc1	matka
je	on	k3xPp3gFnPc4	on
krmí	krmit	k5eAaImIp3nP	krmit
až	až	k6eAd1	až
do	do	k7c2	do
věku	věk	k1gInSc2	věk
cca	cca	kA	cca
40	[number]	k4	40
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
za	za	k7c4	za
život	život	k1gInSc4	život
vychovají	vychovat	k5eAaPmIp3nP	vychovat
asi	asi	k9	asi
5	[number]	k4	5
mláďat	mládě	k1gNnPc2	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
dožívají	dožívat	k5eAaImIp3nP	dožívat
50	[number]	k4	50
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
ve	v	k7c6	v
výjimečných	výjimečný	k2eAgInPc6d1	výjimečný
případech	případ	k1gInPc6	případ
však	však	k9	však
mohou	moct	k5eAaImIp3nP	moct
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
80	[number]	k4	80
až	až	k9	až
90	[number]	k4	90
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
zahajují	zahajovat	k5eAaImIp3nP	zahajovat
sexuální	sexuální	k2eAgFnSc4d1	sexuální
aktivitu	aktivita	k1gFnSc4	aktivita
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
15	[number]	k4	15
let	léto	k1gNnPc2	léto
a	a	k8xC	a
průměrně	průměrně	k6eAd1	průměrně
se	se	k3xPyFc4	se
dožívají	dožívat	k5eAaImIp3nP	dožívat
30	[number]	k4	30
<g/>
,	,	kIx,	,
výjimečně	výjimečně	k6eAd1	výjimečně
až	až	k9	až
50	[number]	k4	50
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Kosatka	kosatka	k1gFnSc1	kosatka
upřednostňuje	upřednostňovat	k5eAaImIp3nS	upřednostňovat
nižší	nízký	k2eAgFnPc4d2	nižší
teploty	teplota	k1gFnPc4	teplota
a	a	k8xC	a
polární	polární	k2eAgInPc4d1	polární
regiony	region	k1gInPc4	region
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
se	se	k3xPyFc4	se
kosatky	kosatka	k1gFnPc1	kosatka
občas	občas	k6eAd1	občas
potápí	potápět	k5eAaImIp3nP	potápět
do	do	k7c2	do
velkých	velký	k2eAgFnPc2d1	velká
hloubek	hloubka	k1gFnPc2	hloubka
<g/>
,	,	kIx,	,
obecně	obecně	k6eAd1	obecně
dávají	dávat	k5eAaImIp3nP	dávat
přednost	přednost	k1gFnSc4	přednost
pobřežním	pobřežní	k2eAgFnPc3d1	pobřežní
oblastem	oblast	k1gFnPc3	oblast
před	před	k7c7	před
pelagiálním	pelagiální	k2eAgNnSc7d1	pelagiální
prostředím	prostředí	k1gNnSc7	prostředí
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
Kosatky	kosatka	k1gFnPc1	kosatka
jsou	být	k5eAaImIp3nP	být
relativně	relativně	k6eAd1	relativně
velmi	velmi	k6eAd1	velmi
početné	početný	k2eAgFnPc1d1	početná
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
severovýchodního	severovýchodní	k2eAgInSc2d1	severovýchodní
Pacifiku	Pacifik	k1gInSc2	Pacifik
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Kanada	Kanada	k1gFnSc1	Kanada
přechází	přecházet	k5eAaImIp3nS	přecházet
v	v	k7c4	v
Aljašku	Aljaška	k1gFnSc4	Aljaška
<g/>
,	,	kIx,	,
při	při	k7c6	při
pobřeží	pobřeží	k1gNnSc6	pobřeží
Islandu	Island	k1gInSc2	Island
a	a	k8xC	a
při	při	k7c6	při
pobřeží	pobřeží	k1gNnSc6	pobřeží
severního	severní	k2eAgNnSc2d1	severní
Norska	Norsko	k1gNnSc2	Norsko
<g/>
.	.	kIx.	.
</s>
<s>
Pravidelně	pravidelně	k6eAd1	pravidelně
jsou	být	k5eAaImIp3nP	být
pozorovány	pozorovat	k5eAaImNgFnP	pozorovat
ve	v	k7c6	v
vodách	voda	k1gFnPc6	voda
Antarktidy	Antarktida	k1gFnSc2	Antarktida
u	u	k7c2	u
polárního	polární	k2eAgInSc2d1	polární
ledovce	ledovec	k1gInSc2	ledovec
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
se	se	k3xPyFc4	se
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
vydávají	vydávat	k5eAaPmIp3nP	vydávat
pod	pod	k7c4	pod
ledový	ledový	k2eAgInSc4d1	ledový
příkrov	příkrov	k1gInSc4	příkrov
a	a	k8xC	a
přežívají	přežívat	k5eAaImIp3nP	přežívat
dýchaje	dýchat	k5eAaImSgInS	dýchat
ve	v	k7c6	v
vzduchových	vzduchový	k2eAgFnPc6d1	vzduchová
kapsách	kapsa	k1gFnPc6	kapsa
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
běluha	běluha	k1gFnSc1	běluha
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
v	v	k7c6	v
Arktidě	Arktida	k1gFnSc6	Arktida
jsou	být	k5eAaImIp3nP	být
kosatky	kosatka	k1gFnPc1	kosatka
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
pozorovány	pozorovat	k5eAaImNgInP	pozorovat
jen	jen	k6eAd1	jen
zřídka	zřídka	k6eAd1	zřídka
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
vody	voda	k1gFnPc1	voda
probouzejí	probouzet	k5eAaImIp3nP	probouzet
jejich	jejich	k3xOp3gInSc4	jejich
zájem	zájem	k1gInSc4	zájem
jen	jen	k9	jen
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
Informace	informace	k1gFnPc1	informace
z	z	k7c2	z
oblastí	oblast	k1gFnPc2	oblast
vzdálených	vzdálený	k2eAgFnPc2d1	vzdálená
od	od	k7c2	od
pobřeží	pobřeží	k1gNnSc2	pobřeží
a	a	k8xC	a
z	z	k7c2	z
tropických	tropický	k2eAgFnPc2d1	tropická
vod	voda	k1gFnPc2	voda
jsou	být	k5eAaImIp3nP	být
více	hodně	k6eAd2	hodně
kusé	kusý	k2eAgFnPc1d1	kusá
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dlouhodobá	dlouhodobý	k2eAgFnSc1d1	dlouhodobá
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
ne	ne	k9	ne
častá	častý	k2eAgFnSc1d1	častá
<g/>
,	,	kIx,	,
pozorování	pozorování	k1gNnPc1	pozorování
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
kosatky	kosatka	k1gFnPc1	kosatka
mohou	moct	k5eAaImIp3nP	moct
přežít	přežít	k5eAaPmF	přežít
v	v	k7c6	v
téměř	téměř	k6eAd1	téměř
všech	všecek	k3xTgNnPc6	všecek
teplotních	teplotní	k2eAgNnPc6d1	teplotní
pásmech	pásmo	k1gNnPc6	pásmo
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Neexistuje	existovat	k5eNaImIp3nS	existovat
žádný	žádný	k3yNgInSc4	žádný
odhad	odhad	k1gInSc4	odhad
celkové	celkový	k2eAgFnSc2d1	celková
světové	světový	k2eAgFnSc2d1	světová
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lokální	lokální	k2eAgInPc1d1	lokální
odhady	odhad	k1gInPc1	odhad
udávají	udávat	k5eAaImIp3nP	udávat
70	[number]	k4	70
<g/>
–	–	k?	–
<g/>
80	[number]	k4	80
000	[number]	k4	000
v	v	k7c6	v
Antarktidě	Antarktida	k1gFnSc6	Antarktida
<g/>
,	,	kIx,	,
8	[number]	k4	8
000	[number]	k4	000
v	v	k7c6	v
tropických	tropický	k2eAgFnPc6d1	tropická
vodách	voda	k1gFnPc6	voda
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
(	(	kIx(	(
<g/>
kosatky	kosatka	k1gFnPc1	kosatka
se	se	k3xPyFc4	se
v	v	k7c6	v
tropech	trop	k1gInPc6	trop
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
jen	jen	k9	jen
řídce	řídce	k6eAd1	řídce
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
rozloha	rozloha	k1gFnSc1	rozloha
této	tento	k3xDgFnSc2	tento
oblasti	oblast	k1gFnSc2	oblast
je	být	k5eAaImIp3nS	být
19	[number]	k4	19
miliónů	milión	k4xCgInPc2	milión
km2	km2	k4	km2
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
tropickou	tropický	k2eAgFnSc4d1	tropická
populaci	populace	k1gFnSc4	populace
nelze	lze	k6eNd1	lze
zanedbat	zanedbat	k5eAaPmF	zanedbat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
až	až	k9	až
2	[number]	k4	2
000	[number]	k4	000
u	u	k7c2	u
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
,	,	kIx,	,
1	[number]	k4	1
500	[number]	k4	500
v	v	k7c6	v
chladnějším	chladný	k2eAgInSc6d2	chladnější
severovýchodním	severovýchodní	k2eAgInSc6d1	severovýchodní
Pacifiku	Pacifik	k1gInSc6	Pacifik
a	a	k8xC	a
1	[number]	k4	1
500	[number]	k4	500
u	u	k7c2	u
Norska	Norsko	k1gNnSc2	Norsko
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
přidáme	přidat	k5eAaPmIp1nP	přidat
velmi	velmi	k6eAd1	velmi
hrubé	hrubý	k2eAgInPc1d1	hrubý
odhady	odhad	k1gInPc1	odhad
nesledovaných	sledovaný	k2eNgNnPc2d1	nesledované
území	území	k1gNnPc2	území
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
činit	činit	k5eAaImF	činit
celková	celkový	k2eAgFnSc1d1	celková
populace	populace	k1gFnSc1	populace
asi	asi	k9	asi
100	[number]	k4	100
000	[number]	k4	000
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Sociální	sociální	k2eAgInPc1d1	sociální
vztahy	vztah	k1gInPc1	vztah
==	==	k?	==
</s>
</p>
<p>
<s>
Kosatky	kosatka	k1gFnPc1	kosatka
mají	mít	k5eAaImIp3nP	mít
složitý	složitý	k2eAgInSc4d1	složitý
systém	systém	k1gInSc4	systém
sociálních	sociální	k2eAgFnPc2d1	sociální
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc7d1	základní
skupinou	skupina	k1gFnSc7	skupina
je	být	k5eAaImIp3nS	být
mateřská	mateřský	k2eAgFnSc1d1	mateřská
linie	linie	k1gFnSc1	linie
<g/>
,	,	kIx,	,
skládající	skládající	k2eAgInPc4d1	skládající
se	se	k3xPyFc4	se
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
samice	samice	k1gFnSc2	samice
(	(	kIx(	(
<g/>
matky	matka	k1gFnSc2	matka
rodu	rod	k1gInSc2	rod
<g/>
)	)	kIx)	)
a	a	k8xC	a
jejích	její	k3xOp3gMnPc2	její
potomků	potomek	k1gMnPc2	potomek
<g/>
.	.	kIx.	.
</s>
<s>
Synové	syn	k1gMnPc1	syn
a	a	k8xC	a
dcery	dcera	k1gFnPc1	dcera
matky	matka	k1gFnSc2	matka
rodu	rod	k1gInSc2	rod
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
část	část	k1gFnSc4	část
této	tento	k3xDgFnSc2	tento
linie	linie	k1gFnSc2	linie
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
synové	syn	k1gMnPc1	syn
a	a	k8xC	a
dcery	dcera	k1gFnPc1	dcera
jejích	její	k3xOp3gFnPc2	její
dcer	dcera	k1gFnPc2	dcera
(	(	kIx(	(
<g/>
synové	syn	k1gMnPc1	syn
a	a	k8xC	a
dcery	dcera	k1gFnPc1	dcera
jejích	její	k3xOp3gMnPc2	její
synů	syn	k1gMnPc2	syn
se	se	k3xPyFc4	se
připojují	připojovat	k5eAaImIp3nP	připojovat
k	k	k7c3	k
mateřské	mateřský	k2eAgFnSc3d1	mateřská
linii	linie	k1gFnSc3	linie
svých	svůj	k3xOyFgFnPc2	svůj
matek	matka	k1gFnPc2	matka
<g/>
)	)	kIx)	)
a	a	k8xC	a
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
tak	tak	k6eAd1	tak
rodinný	rodinný	k2eAgInSc4d1	rodinný
strom	strom	k1gInSc4	strom
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
samice	samice	k1gFnPc1	samice
mohou	moct	k5eAaImIp3nP	moct
žít	žít	k5eAaImF	žít
až	až	k9	až
90	[number]	k4	90
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
neobvyklý	obvyklý	k2eNgInSc4d1	neobvyklý
život	život	k1gInSc4	život
čtyř	čtyři	k4xCgFnPc2	čtyři
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
pěti	pět	k4xCc2	pět
generací	generace	k1gFnPc2	generace
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
linii	linie	k1gFnSc6	linie
<g/>
.	.	kIx.	.
</s>
<s>
Skupiny	skupina	k1gFnPc1	skupina
založené	založený	k2eAgFnPc1d1	založená
na	na	k7c6	na
mateřských	mateřský	k2eAgFnPc6d1	mateřská
liniích	linie	k1gFnPc6	linie
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
stabilní	stabilní	k2eAgFnPc1d1	stabilní
po	po	k7c4	po
mnoho	mnoho	k4c4	mnoho
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivci	jednotlivec	k1gMnPc1	jednotlivec
se	se	k3xPyFc4	se
oddělují	oddělovat	k5eAaImIp3nP	oddělovat
od	od	k7c2	od
těchto	tento	k3xDgFnPc2	tento
svých	svůj	k3xOyFgFnPc2	svůj
skupin	skupina	k1gFnPc2	skupina
jen	jen	k9	jen
na	na	k7c4	na
několik	několik	k4yIc4	několik
hodin	hodina	k1gFnPc2	hodina
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
spáření	spáření	k1gNnSc2	spáření
se	se	k3xPyFc4	se
nebo	nebo	k8xC	nebo
shánění	shánění	k1gNnSc1	shánění
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Nebylo	být	k5eNaImAgNnS	být
zaznamenáno	zaznamenat	k5eAaPmNgNnS	zaznamenat
žádné	žádný	k3yNgNnSc1	žádný
trvalé	trvalý	k2eAgNnSc1d1	trvalé
oddělení	oddělení	k1gNnSc1	oddělení
jedince	jedinec	k1gMnSc2	jedinec
z	z	k7c2	z
jeho	jeho	k3xOp3gFnSc2	jeho
mateřské	mateřský	k2eAgFnSc2d1	mateřská
linie	linie	k1gFnSc2	linie
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
velikost	velikost	k1gFnSc1	velikost
mateřské	mateřský	k2eAgFnSc2d1	mateřská
linie	linie	k1gFnSc2	linie
<g/>
,	,	kIx,	,
zaznamenaná	zaznamenaný	k2eAgFnSc1d1	zaznamenaná
v	v	k7c6	v
severovýchodním	severovýchodní	k2eAgInSc6d1	severovýchodní
Pacifiku	Pacifik	k1gInSc6	Pacifik
<g/>
,	,	kIx,	,
činila	činit	k5eAaImAgFnS	činit
9	[number]	k4	9
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
Mateřské	mateřský	k2eAgFnPc1d1	mateřská
linie	linie	k1gFnPc1	linie
mají	mít	k5eAaImIp3nP	mít
tendenci	tendence	k1gFnSc4	tendence
sdružovat	sdružovat	k5eAaImF	sdružovat
se	se	k3xPyFc4	se
s	s	k7c7	s
několika	několik	k4yIc2	několik
málo	málo	k1gNnSc1	málo
jinými	jiný	k2eAgFnPc7d1	jiná
mateřskými	mateřský	k2eAgFnPc7d1	mateřská
liniemi	linie	k1gFnPc7	linie
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yQgInPc7	který
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
stádo	stádo	k1gNnSc1	stádo
skládající	skládající	k2eAgMnSc1d1	skládající
se	se	k3xPyFc4	se
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
z	z	k7c2	z
18	[number]	k4	18
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
stáda	stádo	k1gNnSc2	stádo
sdílejí	sdílet	k5eAaImIp3nP	sdílet
tentýž	týž	k3xTgInSc4	týž
dialekt	dialekt	k1gInSc4	dialekt
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
sekce	sekce	k1gFnSc2	sekce
Zpěv	zpěv	k1gInSc1	zpěv
<g/>
)	)	kIx)	)
a	a	k8xC	a
skládají	skládat	k5eAaImIp3nP	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
blízce	blízce	k6eAd1	blízce
příbuzných	příbuzný	k2eAgFnPc2d1	příbuzná
částí	část	k1gFnPc2	část
mateřských	mateřský	k2eAgFnPc2d1	mateřská
linií	linie	k1gFnPc2	linie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
mateřských	mateřský	k2eAgFnPc2d1	mateřská
linií	linie	k1gFnPc2	linie
se	se	k3xPyFc4	se
stáda	stádo	k1gNnSc2	stádo
utvořená	utvořený	k2eAgFnSc1d1	utvořená
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
shánění	shánění	k1gNnSc2	shánění
potravy	potrava	k1gFnSc2	potrava
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
dnů	den	k1gInPc2	den
nebo	nebo	k8xC	nebo
týdnů	týden	k1gInPc2	týden
rozpadají	rozpadat	k5eAaImIp3nP	rozpadat
a	a	k8xC	a
zase	zase	k9	zase
spojují	spojovat	k5eAaImIp3nP	spojovat
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgNnSc1d3	veliký
zaznamenané	zaznamenaný	k2eAgNnSc1d1	zaznamenané
stádo	stádo	k1gNnSc1	stádo
mělo	mít	k5eAaImAgNnS	mít
49	[number]	k4	49
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
Další	další	k2eAgFnSc7d1	další
úrovní	úroveň	k1gFnSc7	úroveň
skupiny	skupina	k1gFnSc2	skupina
je	být	k5eAaImIp3nS	být
klan	klan	k1gInSc1	klan
<g/>
.	.	kIx.	.
</s>
<s>
Klan	klan	k1gInSc1	klan
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
stád	stádo	k1gNnPc2	stádo
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
mají	mít	k5eAaImIp3nP	mít
podobný	podobný	k2eAgInSc4d1	podobný
dialekt	dialekt	k1gInSc4	dialekt
<g/>
.	.	kIx.	.
</s>
<s>
Příbuzenství	příbuzenství	k1gNnSc1	příbuzenství
mezi	mezi	k7c7	mezi
stády	stádo	k1gNnPc7	stádo
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
jeví	jevit	k5eAaImIp3nS	jevit
jako	jako	k9	jako
genealogické	genealogický	k2eAgNnSc1d1	genealogické
<g/>
,	,	kIx,	,
většinou	většina	k1gFnSc7	většina
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
části	část	k1gFnPc1	část
rodin	rodina	k1gFnPc2	rodina
se	s	k7c7	s
společným	společný	k2eAgInSc7d1	společný
předkem	předek	k1gInSc7	předek
na	na	k7c6	na
mateřské	mateřský	k2eAgFnSc6d1	mateřská
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
Různé	různý	k2eAgInPc1d1	různý
klany	klan	k1gInPc1	klan
mohou	moct	k5eAaImIp3nP	moct
zabírat	zabírat	k5eAaImF	zabírat
totéž	týž	k3xTgNnSc4	týž
geografické	geografický	k2eAgNnSc4d1	geografické
území	území	k1gNnSc4	území
a	a	k8xC	a
proto	proto	k8xC	proto
bylo	být	k5eAaImAgNnS	být
často	často	k6eAd1	často
zaznamenáno	zaznamenat	k5eAaPmNgNnS	zaznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
stáda	stádo	k1gNnPc4	stádo
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
klanů	klan	k1gInPc2	klan
cestují	cestovat	k5eAaImIp3nP	cestovat
společně	společně	k6eAd1	společně
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
místní	místní	k2eAgNnPc1d1	místní
stáda	stádo	k1gNnPc1	stádo
sdružují	sdružovat	k5eAaImIp3nP	sdružovat
do	do	k7c2	do
klanu	klan	k1gInSc2	klan
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
před	před	k7c7	před
promísením	promísení	k1gNnSc7	promísení
jednotlivci	jednotlivec	k1gMnSc6	jednotlivec
uvítají	uvítat	k5eAaPmIp3nP	uvítat
zformováním	zformování	k1gNnSc7	zformování
dvou	dva	k4xCgFnPc2	dva
paralelních	paralelní	k2eAgFnPc2d1	paralelní
pokrevních	pokrevní	k2eAgFnPc2d1	pokrevní
linií	linie	k1gFnPc2	linie
tváří	tvář	k1gFnSc7	tvář
v	v	k7c4	v
tvář	tvář	k1gFnSc4	tvář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Konečné	Konečné	k2eAgNnSc1d1	Konečné
stadium	stadium	k1gNnSc1	stadium
sdružování	sdružování	k1gNnSc2	sdružování
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
předchozích	předchozí	k2eAgFnPc2d1	předchozí
přirozených	přirozený	k2eAgFnPc2d1	přirozená
skupin	skupina	k1gFnPc2	skupina
spíše	spíše	k9	spíše
náhodné	náhodný	k2eAgNnSc4d1	náhodné
a	a	k8xC	a
lidmi	člověk	k1gMnPc7	člověk
zavedené	zavedený	k2eAgFnPc4d1	zavedená
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
komunita	komunita	k1gFnSc1	komunita
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
nejasně	jasně	k6eNd1	jasně
definováno	definovat	k5eAaBmNgNnS	definovat
jako	jako	k8xC	jako
skupina	skupina	k1gFnSc1	skupina
klanů	klan	k1gInPc2	klan
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yIgFnPc2	který
bylo	být	k5eAaImAgNnS	být
pozorováno	pozorovat	k5eAaImNgNnS	pozorovat
pravidelné	pravidelný	k2eAgNnSc1d1	pravidelné
míchání	míchání	k1gNnSc1	míchání
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Komunity	komunita	k1gFnPc1	komunita
se	se	k3xPyFc4	se
nevytvářejí	vytvářet	k5eNaImIp3nP	vytvářet
na	na	k7c6	na
základě	základ	k1gInSc6	základ
zřetelných	zřetelný	k2eAgFnPc2d1	zřetelná
rodinných	rodinný	k2eAgFnPc2d1	rodinná
nebo	nebo	k8xC	nebo
vokálních	vokální	k2eAgFnPc2d1	vokální
vazeb	vazba	k1gFnPc2	vazba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
severovýchodním	severovýchodní	k2eAgInSc6d1	severovýchodní
Pacifiku	Pacifik	k1gInSc6	Pacifik
byly	být	k5eAaImAgFnP	být
identifikovány	identifikovat	k5eAaBmNgFnP	identifikovat
tyto	tento	k3xDgFnPc1	tento
tři	tři	k4xCgFnPc1	tři
komunity	komunita	k1gFnPc1	komunita
<g/>
:	:	kIx,	:
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
jižní	jižní	k2eAgFnSc1d1	jižní
komunita	komunita	k1gFnSc1	komunita
(	(	kIx(	(
<g/>
1	[number]	k4	1
klan	klan	k1gInSc1	klan
<g/>
,	,	kIx,	,
3	[number]	k4	3
stáda	stádo	k1gNnSc2	stádo
<g/>
,	,	kIx,	,
92	[number]	k4	92
kosatek	kosatka	k1gFnPc2	kosatka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
severní	severní	k2eAgFnSc1d1	severní
komunita	komunita	k1gFnSc1	komunita
(	(	kIx(	(
<g/>
3	[number]	k4	3
klany	klan	k1gInPc7	klan
<g/>
,	,	kIx,	,
16	[number]	k4	16
stád	stádo	k1gNnPc2	stádo
<g/>
,	,	kIx,	,
214	[number]	k4	214
kosatek	kosatka	k1gFnPc2	kosatka
z	z	k7c2	z
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
jihoaljašská	jihoaljašský	k2eAgFnSc1d1	jihoaljašský
komunita	komunita	k1gFnSc1	komunita
(	(	kIx(	(
<g/>
2	[number]	k4	2
klany	klan	k1gInPc7	klan
<g/>
,	,	kIx,	,
11	[number]	k4	11
stád	stádo	k1gNnPc2	stádo
<g/>
,	,	kIx,	,
211	[number]	k4	211
kosatek	kosatka	k1gFnPc2	kosatka
z	z	k7c2	z
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
podotknout	podotknout	k5eAaPmF	podotknout
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
hierarchie	hierarchie	k1gFnSc1	hierarchie
je	být	k5eAaImIp3nS	být
platná	platný	k2eAgFnSc1d1	platná
jen	jen	k9	jen
pro	pro	k7c4	pro
místní	místní	k2eAgFnPc4d1	místní
skupiny	skupina	k1gFnPc4	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Dočasné	dočasný	k2eAgNnSc1d1	dočasné
<g/>
,	,	kIx,	,
savce	savec	k1gMnPc4	savec
lovící	lovící	k2eAgFnSc2d1	lovící
skupiny	skupina	k1gFnSc2	skupina
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
menší	malý	k2eAgFnPc1d2	menší
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ačkoliv	ačkoliv	k8xS	ačkoliv
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
založeny	založit	k5eAaPmNgInP	založit
na	na	k7c6	na
mateřských	mateřský	k2eAgFnPc6d1	mateřská
liniích	linie	k1gFnPc6	linie
<g/>
,	,	kIx,	,
samci	samec	k1gMnPc1	samec
jsou	být	k5eAaImIp3nP	být
mnohem	mnohem	k6eAd1	mnohem
více	hodně	k6eAd2	hodně
ochotni	ochoten	k2eAgMnPc1d1	ochoten
se	se	k3xPyFc4	se
rozdělit	rozdělit	k5eAaPmF	rozdělit
a	a	k8xC	a
žít	žít	k5eAaImF	žít
samostatným	samostatný	k2eAgInSc7d1	samostatný
životem	život	k1gInSc7	život
<g/>
.	.	kIx.	.
</s>
<s>
Dočasné	dočasný	k2eAgFnPc1d1	dočasná
skupiny	skupina	k1gFnPc1	skupina
jsou	být	k5eAaImIp3nP	být
přesto	přesto	k8xC	přesto
spojeny	spojit	k5eAaPmNgFnP	spojit
volnější	volný	k2eAgFnSc7d2	volnější
vazbou	vazba	k1gFnSc7	vazba
založenou	založený	k2eAgFnSc7d1	založená
na	na	k7c6	na
jejich	jejich	k3xOp3gInSc6	jejich
dialektu	dialekt	k1gInSc6	dialekt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Denní	denní	k2eAgFnSc1d1	denní
aktivita	aktivita	k1gFnSc1	aktivita
kosatek	kosatka	k1gFnPc2	kosatka
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
do	do	k7c2	do
čtyř	čtyři	k4xCgFnPc2	čtyři
aktivit	aktivita	k1gFnPc2	aktivita
–	–	k?	–
lov	lov	k1gInSc1	lov
<g/>
,	,	kIx,	,
cestování	cestování	k1gNnSc4	cestování
<g/>
,	,	kIx,	,
odpočinek	odpočinek	k1gInSc4	odpočinek
a	a	k8xC	a
společenský	společenský	k2eAgInSc4d1	společenský
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vzájemném	vzájemný	k2eAgInSc6d1	vzájemný
kontaktu	kontakt	k1gInSc6	kontakt
projevují	projevovat	k5eAaImIp3nP	projevovat
kosatky	kosatka	k1gFnPc1	kosatka
značnou	značný	k2eAgFnSc4d1	značná
míru	míra	k1gFnSc4	míra
entuziasmu	entuziasmus	k1gInSc2	entuziasmus
<g/>
,	,	kIx,	,
předvádějí	předvádět	k5eAaImIp3nP	předvádět
širokou	široký	k2eAgFnSc4d1	široká
škálu	škála	k1gFnSc4	škála
zvedání	zvedání	k1gNnSc2	zvedání
se	se	k3xPyFc4	se
a	a	k8xC	a
výskoků	výskok	k1gInPc2	výskok
nad	nad	k7c4	nad
vodu	voda	k1gFnSc4	voda
<g/>
,	,	kIx,	,
pozorovacích	pozorovací	k2eAgInPc2d1	pozorovací
skoků	skok	k1gInPc2	skok
<g/>
,	,	kIx,	,
úderů	úder	k1gInPc2	úder
ocasu	ocas	k1gInSc2	ocas
do	do	k7c2	do
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
stání	stání	k1gNnSc2	stání
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
samčích	samčí	k2eAgFnPc6d1	samčí
skupinách	skupina	k1gFnPc6	skupina
se	se	k3xPyFc4	se
jednotliví	jednotlivý	k2eAgMnPc1d1	jednotlivý
členové	člen	k1gMnPc1	člen
často	často	k6eAd1	často
dotýkají	dotýkat	k5eAaImIp3nP	dotýkat
ztopořenými	ztopořený	k2eAgInPc7d1	ztopořený
penisy	penis	k1gInPc7	penis
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgInPc4	tento
doteky	dotek	k1gInPc4	dotek
částí	část	k1gFnPc2	část
hry	hra	k1gFnSc2	hra
nebo	nebo	k8xC	nebo
vyjádřením	vyjádření	k1gNnSc7	vyjádření
dominance	dominance	k1gFnSc1	dominance
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Potrava	potrava	k1gFnSc1	potrava
==	==	k?	==
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgInPc7	jenž
se	se	k3xPyFc4	se
kosatky	kosatka	k1gFnPc1	kosatka
živí	živit	k5eAaImIp3nP	živit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
extrémně	extrémně	k6eAd1	extrémně
široký	široký	k2eAgInSc1d1	široký
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
populace	populace	k1gFnPc1	populace
projevují	projevovat	k5eAaImIp3nP	projevovat
tendence	tendence	k1gFnPc4	tendence
ke	k	k7c3	k
specializaci	specializace	k1gFnSc3	specializace
na	na	k7c4	na
určitý	určitý	k2eAgInSc4d1	určitý
typ	typ	k1gInSc4	typ
potravy	potrava	k1gFnSc2	potrava
a	a	k8xC	a
ignorují	ignorovat	k5eAaImIp3nP	ignorovat
ostatní	ostatní	k2eAgInPc4d1	ostatní
možné	možný	k2eAgInPc4d1	možný
druhy	druh	k1gInPc4	druh
kořisti	kořist	k1gFnSc2	kořist
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
určité	určitý	k2eAgFnSc3d1	určitá
části	část	k1gFnSc3	část
populace	populace	k1gFnSc2	populace
v	v	k7c6	v
Norsku	Norsko	k1gNnSc6	Norsko
a	a	k8xC	a
Grónsku	Grónsko	k1gNnSc6	Grónsko
se	se	k3xPyFc4	se
specializují	specializovat	k5eAaBmIp3nP	specializovat
na	na	k7c4	na
sledě	sleď	k1gMnPc4	sleď
a	a	k8xC	a
sledují	sledovat	k5eAaImIp3nP	sledovat
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
jejich	jejich	k3xOp3gFnSc4	jejich
podzimní	podzimní	k2eAgFnSc4d1	podzimní
migrační	migrační	k2eAgFnSc4d1	migrační
trasu	trasa	k1gFnSc4	trasa
k	k	k7c3	k
norskému	norský	k2eAgNnSc3d1	norské
pobřeží	pobřeží	k1gNnSc3	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgFnPc1d1	jiná
skupiny	skupina	k1gFnPc1	skupina
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
loví	lovit	k5eAaImIp3nS	lovit
tuleně	tuleň	k1gMnPc4	tuleň
<g/>
.	.	kIx.	.
</s>
<s>
Kosatky	kosatka	k1gFnPc1	kosatka
jsou	být	k5eAaImIp3nP	být
jedinými	jediný	k2eAgMnPc7d1	jediný
savci	savec	k1gMnPc7	savec
s	s	k7c7	s
takovou	takový	k3xDgFnSc7	takový
potravní	potravní	k2eAgFnSc7d1	potravní
různorodostí	různorodost	k1gFnSc7	různorodost
mezi	mezi	k7c7	mezi
jednotlivci	jednotlivec	k1gMnPc7	jednotlivec
žijícími	žijící	k2eAgMnPc7d1	žijící
na	na	k7c6	na
stejném	stejný	k2eAgNnSc6d1	stejné
území	území	k1gNnSc6	území
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kosatka	kosatka	k1gFnSc1	kosatka
je	být	k5eAaImIp3nS	být
jediným	jediný	k2eAgMnSc7d1	jediný
kytovcem	kytovec	k1gMnSc7	kytovec
pravidelně	pravidelně	k6eAd1	pravidelně
se	s	k7c7	s
živícím	živící	k2eAgInSc7d1	živící
jinými	jiný	k2eAgMnPc7d1	jiný
kytovci	kytovec	k1gMnPc7	kytovec
<g/>
.	.	kIx.	.
</s>
<s>
Pozorování	pozorování	k1gNnSc1	pozorování
bylo	být	k5eAaImAgNnS	být
napočítáno	napočítat	k5eAaPmNgNnS	napočítat
22	[number]	k4	22
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
kosatky	kosatka	k1gFnPc4	kosatka
loví	lovit	k5eAaImIp3nS	lovit
–	–	k?	–
zkoumáním	zkoumání	k1gNnSc7	zkoumání
obsahu	obsah	k1gInSc2	obsah
žaludku	žaludek	k1gInSc2	žaludek
<g/>
,	,	kIx,	,
pozorováním	pozorování	k1gNnSc7	pozorování
zranění	zranění	k1gNnSc2	zranění
na	na	k7c6	na
tělech	tělo	k1gNnPc6	tělo
jiných	jiný	k2eAgMnPc2d1	jiný
kytovců	kytovec	k1gMnPc2	kytovec
či	či	k8xC	či
přímým	přímý	k2eAgNnSc7d1	přímé
pozorováním	pozorování	k1gNnSc7	pozorování
jejich	jejich	k3xOp3gInSc2	jejich
lovu	lov	k1gInSc2	lov
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
Celkem	celkem	k6eAd1	celkem
oblíbenou	oblíbený	k2eAgFnSc7d1	oblíbená
kořistí	kořist	k1gFnSc7	kořist
je	být	k5eAaImIp3nS	být
relativně	relativně	k6eAd1	relativně
malá	malý	k2eAgFnSc1d1	malá
běluha	běluha	k1gFnSc1	běluha
mořská	mořský	k2eAgFnSc1d1	mořská
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stáda	stádo	k1gNnSc2	stádo
kosatek	kosatka	k1gFnPc2	kosatka
dokážou	dokázat	k5eAaPmIp3nP	dokázat
ulovit	ulovit	k5eAaPmF	ulovit
i	i	k9	i
velryby	velryba	k1gFnPc1	velryba
podstatně	podstatně	k6eAd1	podstatně
větší	veliký	k2eAgFnPc1d2	veliký
<g/>
,	,	kIx,	,
než	než	k8xS	než
jsou	být	k5eAaImIp3nP	být
sami	sám	k3xTgMnPc1	sám
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
plejtvák	plejtvák	k1gMnSc1	plejtvák
myšok	myšok	k1gInSc1	myšok
<g/>
,	,	kIx,	,
plejtvák	plejtvák	k1gMnSc1	plejtvák
malý	malý	k1gMnSc1	malý
<g/>
,	,	kIx,	,
plejtvák	plejtvák	k1gMnSc1	plejtvák
sejval	sejvat	k5eAaBmAgMnS	sejvat
<g/>
,	,	kIx,	,
plejtvákovec	plejtvákovec	k1gInSc1	plejtvákovec
šedý	šedý	k2eAgInSc1d1	šedý
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
mladé	mladý	k2eAgMnPc4d1	mladý
plejtváky	plejtvák	k1gMnPc4	plejtvák
obrovské	obrovský	k2eAgNnSc1d1	obrovské
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Skupina	skupina	k1gFnSc1	skupina
kosatek	kosatka	k1gFnPc2	kosatka
může	moct	k5eAaImIp3nS	moct
zdolat	zdolat	k5eAaPmF	zdolat
mladého	mladý	k2eAgMnSc4d1	mladý
plejtváka	plejtvák	k1gMnSc4	plejtvák
obrovského	obrovský	k2eAgInSc2d1	obrovský
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gInSc4	on
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
matkou	matka	k1gFnSc7	matka
štve	štvát	k5eAaImIp3nS	štvát
mořem	moře	k1gNnSc7	moře
až	až	k6eAd1	až
se	se	k3xPyFc4	se
unaví	unavit	k5eAaPmIp3nP	unavit
<g/>
.	.	kIx.	.
</s>
<s>
Nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
kosatky	kosatka	k1gFnPc1	kosatka
rozhodnou	rozhodnout	k5eAaPmIp3nP	rozhodnout
oddělit	oddělit	k5eAaPmF	oddělit
pár	pár	k1gInSc4	pár
a	a	k8xC	a
obklopit	obklopit	k5eAaPmF	obklopit
mladou	mladý	k2eAgFnSc4d1	mladá
velrybu	velryba	k1gFnSc4	velryba
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
zabránit	zabránit	k5eAaPmF	zabránit
jí	jíst	k5eAaImIp3nS	jíst
vrátit	vrátit	k5eAaPmF	vrátit
se	se	k3xPyFc4	se
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
moře	moře	k1gNnSc2	moře
k	k	k7c3	k
nadechnutí	nadechnutí	k1gNnSc3	nadechnutí
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
se	se	k3xPyFc4	se
velryba	velryba	k1gFnSc1	velryba
utopí	utopit	k5eAaPmIp3nS	utopit
<g/>
,	,	kIx,	,
kosatkám	kosatka	k1gFnPc3	kosatka
už	už	k6eAd1	už
nic	nic	k3yNnSc1	nic
nebrání	bránit	k5eNaImIp3nS	bránit
nasytit	nasytit	k5eAaPmF	nasytit
se	se	k3xPyFc4	se
jejím	její	k3xOp3gNnSc7	její
masem	maso	k1gNnSc7	maso
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Byl	být	k5eAaImAgInS	být
také	také	k9	také
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
jeden	jeden	k4xCgInSc1	jeden
případ	případ	k1gInSc1	případ
pravděpodobného	pravděpodobný	k2eAgInSc2d1	pravděpodobný
kanibalismu	kanibalismus	k1gInSc2	kanibalismus
kosatek	kosatka	k1gFnPc2	kosatka
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Ve	v	k7c6	v
studii	studie	k1gFnSc6	studie
V.	V.	kA	V.
I.	I.	kA	I.
Shevenka	Shevenka	k1gFnSc1	Shevenka
v	v	k7c6	v
teplých	teplý	k2eAgFnPc6d1	teplá
oblastech	oblast	k1gFnPc6	oblast
jižního	jižní	k2eAgInSc2d1	jižní
Pacifiku	Pacifik	k1gInSc2	Pacifik
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
je	být	k5eAaImIp3nS	být
zaznamenán	zaznamenán	k2eAgInSc1d1	zaznamenán
případ	případ	k1gInSc1	případ
dvou	dva	k4xCgMnPc2	dva
samců	samec	k1gMnPc2	samec
kosatky	kosatka	k1gFnSc2	kosatka
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
žaludek	žaludek	k1gInSc1	žaludek
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
zbytky	zbytek	k1gInPc4	zbytek
jiné	jiný	k2eAgFnPc1d1	jiná
kosatky	kosatka	k1gFnSc2	kosatka
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
30	[number]	k4	30
zajatých	zajatá	k1gFnPc2	zajatá
a	a	k8xC	a
prozkoumaných	prozkoumaný	k2eAgFnPc2d1	prozkoumaná
kosatek	kosatka	k1gFnPc2	kosatka
mělo	mít	k5eAaImAgNnS	mít
při	při	k7c6	při
tomto	tento	k3xDgNnSc6	tento
pozorování	pozorování	k1gNnSc6	pozorování
11	[number]	k4	11
prázdné	prázdný	k2eAgInPc4d1	prázdný
žaludky	žaludek	k1gInPc4	žaludek
–	–	k?	–
neobvykle	obvykle	k6eNd1	obvykle
velké	velký	k2eAgNnSc4d1	velké
procento	procento	k1gNnSc4	procento
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
kosatky	kosatka	k1gFnPc1	kosatka
byly	být	k5eAaImAgFnP	být
přinuceny	přinutit	k5eAaPmNgFnP	přinutit
ke	k	k7c3	k
kanibalismu	kanibalismus	k1gInSc3	kanibalismus
nedostatkem	nedostatek	k1gInSc7	nedostatek
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
Ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
se	se	k3xPyFc4	se
kosatky	kosatka	k1gFnPc1	kosatka
živí	živit	k5eAaImIp3nP	živit
třiceti	třicet	k4xCc7	třicet
druhy	druh	k1gInPc1	druh
ryb	ryba	k1gFnPc2	ryba
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
lososy	losos	k1gMnPc7	losos
<g/>
,	,	kIx,	,
sledi	sleď	k1gMnPc7	sleď
<g/>
,	,	kIx,	,
tuňáky	tuňák	k1gMnPc7	tuňák
<g/>
,	,	kIx,	,
čavyči	čavyča	k1gFnSc6	čavyča
a	a	k8xC	a
kisuči	kisuč	k1gFnSc6	kisuč
<g/>
.	.	kIx.	.
</s>
<s>
Žraloky	žralok	k1gMnPc7	žralok
obrovské	obrovský	k2eAgFnSc2d1	obrovská
<g/>
,	,	kIx,	,
žraloky	žralok	k1gMnPc7	žralok
dlouhoploutvé	dlouhoploutvá	k1gFnSc2	dlouhoploutvá
<g/>
,	,	kIx,	,
a	a	k8xC	a
příležitostně	příležitostně	k6eAd1	příležitostně
dokonce	dokonce	k9	dokonce
žraloky	žralok	k1gMnPc4	žralok
bílé	bílý	k2eAgFnSc2d1	bílá
loví	lovit	k5eAaImIp3nP	lovit
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gNnPc4	jejich
nutričně	nutričně	k6eAd1	nutričně
hodnotná	hodnotný	k2eAgNnPc4d1	hodnotné
játra	játra	k1gNnPc4	játra
(	(	kIx(	(
<g/>
někteří	některý	k3yIgMnPc1	některý
vědci	vědec	k1gMnPc1	vědec
předpokládají	předpokládat	k5eAaImIp3nP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
žraloky	žralok	k1gMnPc4	žralok
odstraňují	odstraňovat	k5eAaImIp3nP	odstraňovat
též	též	k9	též
jako	jako	k9	jako
nežádoucí	žádoucí	k2eNgFnSc3d1	nežádoucí
konkurenci	konkurence	k1gFnSc3	konkurence
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Polární	polární	k2eAgFnPc1d1	polární
populace	populace	k1gFnPc1	populace
loví	lovit	k5eAaImIp3nP	lovit
také	také	k6eAd1	také
jiné	jiný	k2eAgMnPc4d1	jiný
mořské	mořský	k2eAgMnPc4d1	mořský
savce	savec	k1gMnPc4	savec
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
většiny	většina	k1gFnSc2	většina
druhů	druh	k1gInPc2	druh
tuleňů	tuleň	k1gMnPc2	tuleň
a	a	k8xC	a
lvounů	lvoun	k1gMnPc2	lvoun
<g/>
.	.	kIx.	.
</s>
<s>
Méně	málo	k6eAd2	málo
často	často	k6eAd1	často
se	se	k3xPyFc4	se
stávají	stávat	k5eAaImIp3nP	stávat
obětí	oběť	k1gFnSc7	oběť
také	také	k9	také
mroži	mrož	k1gMnPc1	mrož
a	a	k8xC	a
mořské	mořský	k2eAgFnPc1d1	mořská
vydry	vydra	k1gFnPc1	vydra
<g/>
.	.	kIx.	.
</s>
<s>
Kořistí	kořistit	k5eAaImIp3nS	kořistit
se	se	k3xPyFc4	se
též	též	k9	též
může	moct	k5eAaImIp3nS	moct
stát	stát	k5eAaImF	stát
sedm	sedm	k4xCc4	sedm
druhů	druh	k1gInPc2	druh
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
všech	všecek	k3xTgInPc2	všecek
druhů	druh	k1gInPc2	druh
tučňáků	tučňák	k1gMnPc2	tučňák
a	a	k8xC	a
mořských	mořský	k2eAgMnPc2d1	mořský
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
kormoráni	kormorán	k1gMnPc5	kormorán
<g/>
.	.	kIx.	.
</s>
<s>
Kosatky	kosatka	k1gFnPc1	kosatka
nepohrdnou	pohrdnout	k5eNaPmIp3nP	pohrdnout
ani	ani	k9	ani
hlavonožci	hlavonožec	k1gMnPc1	hlavonožec
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
chobotnice	chobotnice	k1gFnPc4	chobotnice
a	a	k8xC	a
olihně	oliheň	k1gFnPc4	oliheň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
zabíjení	zabíjení	k1gNnSc6	zabíjení
jsou	být	k5eAaImIp3nP	být
kosatky	kosatka	k1gFnPc1	kosatka
velmi	velmi	k6eAd1	velmi
vynalézavé	vynalézavý	k2eAgFnPc1d1	vynalézavá
a	a	k8xC	a
hravé	hravý	k2eAgFnPc1d1	hravá
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
si	se	k3xPyFc3	se
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
pohazují	pohazovat	k5eAaImIp3nP	pohazovat
s	s	k7c7	s
tuleni	tuleň	k1gMnPc7	tuleň
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
je	být	k5eAaImIp3nS	být
omráčí	omráčet	k5eAaImIp3nP	omráčet
a	a	k8xC	a
zabijí	zabít	k5eAaPmIp3nP	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
lososi	losos	k1gMnPc1	losos
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
loveni	loven	k2eAgMnPc1d1	loven
jednotlivcem	jednotlivec	k1gMnSc7	jednotlivec
nebo	nebo	k8xC	nebo
malou	malý	k2eAgFnSc7d1	malá
skupinkou	skupinka	k1gFnSc7	skupinka
<g/>
,	,	kIx,	,
sledi	sleď	k1gMnPc1	sleď
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
chytáni	chytat	k5eAaImNgMnP	chytat
technikou	technika	k1gFnSc7	technika
kolotočového	kolotočový	k2eAgNnSc2d1	kolotočové
krmení	krmení	k1gNnSc2	krmení
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
kosatka	kosatka	k1gFnSc1	kosatka
nažene	nahnat	k5eAaPmIp3nS	nahnat
sledě	sleď	k1gMnSc4	sleď
do	do	k7c2	do
těsné	těsný	k2eAgFnSc2d1	těsná
koule	koule	k1gFnSc2	koule
vypouštěním	vypouštění	k1gNnSc7	vypouštění
výbuchů	výbuch	k1gInPc2	výbuch
bublin	bublina	k1gFnPc2	bublina
nebo	nebo	k8xC	nebo
šlehy	šleha	k1gFnSc2	šleha
svých	svůj	k3xOyFgFnPc2	svůj
bočních	boční	k2eAgFnPc2d1	boční
ploutví	ploutev	k1gFnPc2	ploutev
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
zasáhne	zasáhnout	k5eAaPmIp3nS	zasáhnout
kouli	koule	k1gFnSc4	koule
plným	plný	k2eAgInSc7d1	plný
úderem	úder	k1gInSc7	úder
svého	svůj	k3xOyFgInSc2	svůj
ocasu	ocas	k1gInSc2	ocas
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
omráčí	omráčet	k5eAaImIp3nS	omráčet
nebo	nebo	k8xC	nebo
usmrtí	usmrtit	k5eAaPmIp3nS	usmrtit
10	[number]	k4	10
<g/>
–	–	k?	–
<g/>
15	[number]	k4	15
sleďů	sleď	k1gMnPc2	sleď
a	a	k8xC	a
okamžitě	okamžitě	k6eAd1	okamžitě
je	on	k3xPp3gMnPc4	on
zkonzumuje	zkonzumovat	k5eAaPmIp3nS	zkonzumovat
<g/>
.	.	kIx.	.
</s>
<s>
Technika	technika	k1gFnSc1	technika
kolotočového	kolotočový	k2eAgNnSc2d1	kolotočové
krmení	krmení	k1gNnSc2	krmení
byla	být	k5eAaImAgFnS	být
doložena	doložit	k5eAaPmNgFnS	doložit
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
norské	norský	k2eAgFnSc6d1	norská
kosatčí	kosatčí	k2eAgFnSc6d1	kosatčí
populaci	populace	k1gFnSc6	populace
a	a	k8xC	a
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
oceánských	oceánský	k2eAgInPc2d1	oceánský
druhů	druh	k1gInPc2	druh
delfínů	delfín	k1gMnPc2	delfín
<g/>
.	.	kIx.	.
</s>
<s>
Lvouni	lvoun	k1gMnPc1	lvoun
jsou	být	k5eAaImIp3nP	být
zabíjeni	zabíjen	k2eAgMnPc1d1	zabíjen
údery	úder	k1gInPc1	úder
hlavou	hlava	k1gFnSc7	hlava
nebo	nebo	k8xC	nebo
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
zasaženi	zasáhnout	k5eAaPmNgMnP	zasáhnout
a	a	k8xC	a
omráčeni	omráčit	k5eAaPmNgMnP	omráčit
úderem	úder	k1gInSc7	úder
ocasu	ocas	k1gInSc2	ocas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
populacích	populace	k1gFnPc6	populace
lze	lze	k6eAd1	lze
najít	najít	k5eAaPmF	najít
různé	různý	k2eAgFnPc4d1	různá
specializované	specializovaný	k2eAgFnPc4d1	specializovaná
techniky	technika	k1gFnPc4	technika
lovu	lov	k1gInSc2	lov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Patagonii	Patagonie	k1gFnSc6	Patagonie
se	se	k3xPyFc4	se
kosatky	kosatka	k1gFnPc1	kosatka
živí	živit	k5eAaImIp3nP	živit
mláďaty	mládě	k1gNnPc7	mládě
lachtana	lachtan	k1gMnSc4	lachtan
hřívnatého	hřívnatý	k2eAgMnSc4d1	hřívnatý
a	a	k8xC	a
rypouše	rypouš	k1gMnSc4	rypouš
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
nahánějí	nahánět	k5eAaImIp3nP	nahánět
na	na	k7c4	na
pláže	pláž	k1gFnPc4	pláž
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
i	i	k9	i
za	za	k7c4	za
cenu	cena	k1gFnSc4	cena
vlastního	vlastní	k2eAgNnSc2d1	vlastní
dočasného	dočasný	k2eAgNnSc2d1	dočasné
uváznutí	uváznutí	k1gNnSc2	uváznutí
<g/>
.	.	kIx.	.
</s>
<s>
Jinde	jinde	k6eAd1	jinde
kosatky	kosatka	k1gFnPc1	kosatka
provádějí	provádět	k5eAaImIp3nP	provádět
pozorovací	pozorovací	k2eAgInPc4d1	pozorovací
skoky	skok	k1gInPc4	skok
a	a	k8xC	a
lokalizují	lokalizovat	k5eAaBmIp3nP	lokalizovat
tak	tak	k9	tak
tuleně	tuleň	k1gMnPc4	tuleň
odpočívající	odpočívající	k2eAgFnSc1d1	odpočívající
na	na	k7c6	na
ledových	ledový	k2eAgFnPc6d1	ledová
krách	kra	k1gFnPc6	kra
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
vlnu	vlna	k1gFnSc4	vlna
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
převalí	převalit	k5eAaPmIp3nS	převalit
přes	přes	k7c4	přes
kru	kra	k1gFnSc4	kra
a	a	k8xC	a
přiměje	přimět	k5eAaPmIp3nS	přimět
tuleně	tuleň	k1gMnPc4	tuleň
skočit	skočit	k5eAaPmF	skočit
do	do	k7c2	do
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
na	na	k7c4	na
ně	on	k3xPp3gFnPc4	on
čeká	čekat	k5eAaImIp3nS	čekat
druhá	druhý	k4xOgFnSc1	druhý
kosatka	kosatka	k1gFnSc1	kosatka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
kosatka	kosatka	k1gFnSc1	kosatka
sní	snít	k5eAaImIp3nS	snít
60	[number]	k4	60
kg	kg	kA	kg
jídla	jídlo	k1gNnSc2	jídlo
denně	denně	k6eAd1	denně
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
ohromné	ohromný	k2eAgFnSc3d1	ohromná
rozmanitosti	rozmanitost	k1gFnSc3	rozmanitost
potravy	potrava	k1gFnSc2	potrava
a	a	k8xC	a
absenci	absence	k1gFnSc4	absence
přirozených	přirozený	k2eAgMnPc2d1	přirozený
nepřátel	nepřítel	k1gMnPc2	nepřítel
lze	lze	k6eAd1	lze
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
kosatka	kosatka	k1gFnSc1	kosatka
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
potravního	potravní	k2eAgInSc2d1	potravní
řetězce	řetězec	k1gInSc2	řetězec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zpěv	zpěv	k1gInSc1	zpěv
==	==	k?	==
</s>
</p>
<p>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
jiní	jiný	k2eAgMnPc1d1	jiný
delfíni	delfín	k1gMnPc1	delfín
jsou	být	k5eAaImIp3nP	být
kosatky	kosatka	k1gFnPc4	kosatka
velmi	velmi	k6eAd1	velmi
zpěvná	zpěvný	k2eAgNnPc4d1	zpěvné
zvířata	zvíře	k1gNnPc4	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Vydávají	vydávat	k5eAaImIp3nP	vydávat
různé	různý	k2eAgInPc1d1	různý
druhy	druh	k1gInPc1	druh
cvakání	cvakání	k1gNnSc2	cvakání
a	a	k8xC	a
hvízdání	hvízdání	k1gNnSc2	hvízdání
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
slouží	sloužit	k5eAaImIp3nP	sloužit
ke	k	k7c3	k
komunikaci	komunikace	k1gFnSc3	komunikace
a	a	k8xC	a
echolokaci	echolokace	k1gFnSc3	echolokace
<g/>
.	.	kIx.	.
</s>
<s>
Typ	typ	k1gInSc1	typ
vytvářených	vytvářený	k2eAgInPc2d1	vytvářený
zvuků	zvuk	k1gInPc2	zvuk
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
liší	lišit	k5eAaImIp3nP	lišit
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
aktuální	aktuální	k2eAgFnSc6d1	aktuální
aktivitě	aktivita	k1gFnSc6	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
odpočinku	odpočinek	k1gInSc2	odpočinek
jsou	být	k5eAaImIp3nP	být
kosatky	kosatka	k1gFnPc1	kosatka
mnohem	mnohem	k6eAd1	mnohem
tišší	tichý	k2eAgFnPc1d2	tišší
<g/>
,	,	kIx,	,
vydávají	vydávat	k5eAaImIp3nP	vydávat
jen	jen	k9	jen
nahodilá	nahodilý	k2eAgNnPc1d1	nahodilé
volání	volání	k1gNnPc1	volání
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
se	se	k3xPyFc4	se
silně	silně	k6eAd1	silně
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
volání	volání	k1gNnPc2	volání
vydávaných	vydávaný	k2eAgNnPc2d1	vydávané
při	při	k7c6	při
aktivní	aktivní	k2eAgFnSc6d1	aktivní
skupinové	skupinový	k2eAgFnSc6d1	skupinová
činnosti	činnost	k1gFnSc6	činnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stálá	stálý	k2eAgNnPc1d1	stálé
stáda	stádo	k1gNnPc1	stádo
kosatek	kosatka	k1gFnPc2	kosatka
mají	mít	k5eAaImIp3nP	mít
větší	veliký	k2eAgInPc1d2	veliký
sklony	sklon	k1gInPc1	sklon
ke	k	k7c3	k
zpěvu	zpěv	k1gInSc3	zpěv
než	než	k8xS	než
nestálé	stálý	k2eNgFnSc2d1	nestálá
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Vědci	vědec	k1gMnPc1	vědec
se	se	k3xPyFc4	se
domnívají	domnívat	k5eAaImIp3nP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
to	ten	k3xDgNnSc4	ten
existují	existovat	k5eAaImIp3nP	existovat
dva	dva	k4xCgInPc4	dva
hlavní	hlavní	k2eAgInPc4d1	hlavní
důvody	důvod	k1gInPc4	důvod
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
prvé	prvý	k4xOgFnPc4	prvý
–	–	k?	–
usedlé	usedlý	k2eAgFnPc1d1	usedlá
kosatky	kosatka	k1gFnPc1	kosatka
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
ve	v	k7c6	v
stejných	stejný	k2eAgFnPc6d1	stejná
sociálních	sociální	k2eAgFnPc6d1	sociální
skupinách	skupina	k1gFnPc6	skupina
mnohem	mnohem	k6eAd1	mnohem
déle	dlouho	k6eAd2	dlouho
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
mnohem	mnohem	k6eAd1	mnohem
komplikovanější	komplikovaný	k2eAgInPc4d2	komplikovanější
sociální	sociální	k2eAgInPc4d1	sociální
vztahy	vztah	k1gInPc4	vztah
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
odrážejí	odrážet	k5eAaImIp3nP	odrážet
v	v	k7c6	v
častější	častý	k2eAgFnSc6d2	častější
komunikaci	komunikace	k1gFnSc6	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
Nezávislé	závislý	k2eNgFnPc1d1	nezávislá
kosatky	kosatka	k1gFnPc1	kosatka
spolu	spolu	k6eAd1	spolu
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
jen	jen	k9	jen
po	po	k7c4	po
prchavé	prchavý	k2eAgInPc4d1	prchavý
zlomky	zlomek	k1gInPc4	zlomek
času	čas	k1gInSc2	čas
(	(	kIx(	(
<g/>
obvyklá	obvyklý	k2eAgFnSc1d1	obvyklá
perioda	perioda	k1gFnSc1	perioda
jsou	být	k5eAaImIp3nP	být
hodiny	hodina	k1gFnPc4	hodina
nebo	nebo	k8xC	nebo
dny	den	k1gInPc4	den
<g/>
)	)	kIx)	)
a	a	k8xC	a
proto	proto	k8xC	proto
komunikují	komunikovat	k5eAaImIp3nP	komunikovat
méně	málo	k6eAd2	málo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c4	za
druhé	druhý	k4xOgFnPc4	druhý
–	–	k?	–
nezávislé	závislý	k2eNgFnPc1d1	nezávislá
kosatky	kosatka	k1gFnPc1	kosatka
se	se	k3xPyFc4	se
mnohem	mnohem	k6eAd1	mnohem
raději	rád	k6eAd2	rád
živí	živit	k5eAaImIp3nP	živit
mořskými	mořský	k2eAgMnPc7d1	mořský
savci	savec	k1gMnPc7	savec
než	než	k8xS	než
stálá	stálý	k2eAgNnPc1d1	stálé
stáda	stádo	k1gNnPc1	stádo
upřednostňující	upřednostňující	k2eAgFnSc2d1	upřednostňující
ryby	ryba	k1gFnSc2	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Kosatky	kosatka	k1gFnPc1	kosatka
lovící	lovící	k2eAgMnPc4d1	lovící
savce	savec	k1gMnPc4	savec
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
mnohem	mnohem	k6eAd1	mnohem
tišší	tichý	k2eAgNnSc1d2	tišší
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
zmenšily	zmenšit	k5eAaPmAgFnP	zmenšit
pravděpodobnost	pravděpodobnost	k1gFnSc4	pravděpodobnost
odhalení	odhalení	k1gNnSc2	odhalení
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
většinou	většinou	k6eAd1	většinou
používá	používat	k5eAaImIp3nS	používat
lovící	lovící	k2eAgFnSc1d1	lovící
kosatka	kosatka	k1gFnSc1	kosatka
pro	pro	k7c4	pro
echolokaci	echolokace	k1gFnSc4	echolokace
jenom	jenom	k6eAd1	jenom
jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
cvaknutí	cvaknutí	k1gNnSc1	cvaknutí
(	(	kIx(	(
<g/>
zvané	zvaný	k2eAgNnSc1d1	zvané
šifrované	šifrovaný	k2eAgNnSc1d1	šifrované
cvaknutí	cvaknutí	k1gNnSc1	cvaknutí
<g/>
)	)	kIx)	)
oproti	oproti	k7c3	oproti
dlouhému	dlouhý	k2eAgInSc3d1	dlouhý
řetězci	řetězec	k1gInSc3	řetězec
cvaknutí	cvaknutí	k1gNnSc2	cvaknutí
pozorovanému	pozorovaný	k2eAgInSc3d1	pozorovaný
u	u	k7c2	u
jiných	jiný	k2eAgInPc2d1	jiný
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stálá	stálý	k2eAgNnPc1d1	stálé
stáda	stádo	k1gNnPc1	stádo
užívají	užívat	k5eAaImIp3nP	užívat
místní	místní	k2eAgInPc1d1	místní
dialekty	dialekt	k1gInPc1	dialekt
<g/>
.	.	kIx.	.
</s>
<s>
Každé	každý	k3xTgNnSc1	každý
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgFnPc4	svůj
písně	píseň	k1gFnPc4	píseň
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
série	série	k1gFnSc1	série
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
hvizdů	hvizd	k1gInPc2	hvizd
a	a	k8xC	a
cvaknutí	cvaknutí	k1gNnSc2	cvaknutí
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
stále	stále	k6eAd1	stále
opakuje	opakovat	k5eAaImIp3nS	opakovat
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
každý	každý	k3xTgInSc1	každý
člen	člen	k1gInSc1	člen
stáda	stádo	k1gNnSc2	stádo
zná	znát	k5eAaImIp3nS	znát
všechny	všechen	k3xTgFnPc4	všechen
jeho	jeho	k3xOp3gFnPc4	jeho
písně	píseň	k1gFnPc4	píseň
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
je	být	k5eAaImIp3nS	být
nemožné	možný	k2eNgNnSc1d1	nemožné
po	po	k7c6	po
hlase	hlas	k1gInSc6	hlas
identifikovat	identifikovat	k5eAaBmF	identifikovat
jediné	jediný	k2eAgNnSc1d1	jediné
zvíře	zvíře	k1gNnSc1	zvíře
<g/>
,	,	kIx,	,
jenom	jenom	k9	jenom
dialektovou	dialektový	k2eAgFnSc4d1	dialektová
skupinu	skupina	k1gFnSc4	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
píseň	píseň	k1gFnSc1	píseň
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
známa	znám	k2eAgFnSc1d1	známa
jen	jen	k8xS	jen
jedné	jeden	k4xCgFnSc3	jeden
skupině	skupina	k1gFnSc3	skupina
nebo	nebo	k8xC	nebo
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
sdílena	sdílet	k5eAaImNgFnS	sdílet
několika	několik	k4yIc7	několik
skupinami	skupina	k1gFnPc7	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Stupeň	stupeň	k1gInSc1	stupeň
podobnosti	podobnost	k1gFnSc2	podobnost
písní	píseň	k1gFnPc2	píseň
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgFnPc7	dva
skupinami	skupina	k1gFnPc7	skupina
se	se	k3xPyFc4	se
obecně	obecně	k6eAd1	obecně
jeví	jevit	k5eAaImIp3nS	jevit
být	být	k5eAaImF	být
funkcí	funkce	k1gFnPc2	funkce
jejich	jejich	k3xOp3gFnSc2	jejich
genealogické	genealogický	k2eAgFnSc2d1	genealogická
blízkosti	blízkost	k1gFnSc2	blízkost
spíše	spíše	k9	spíše
než	než	k8xS	než
blízkosti	blízkost	k1gFnSc2	blízkost
geografické	geografický	k2eAgFnSc2d1	geografická
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
skupiny	skupina	k1gFnPc1	skupina
sdílející	sdílející	k2eAgInPc4d1	sdílející
stejné	stejný	k2eAgInPc4d1	stejný
předky	předek	k1gInPc4	předek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rozrůstající	rozrůstající	k2eAgMnSc1d1	rozrůstající
se	se	k3xPyFc4	se
ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
velmi	velmi	k6eAd1	velmi
podobné	podobný	k2eAgFnPc4d1	podobná
písně	píseň	k1gFnPc4	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
písně	píseň	k1gFnPc1	píseň
přecházejí	přecházet	k5eAaImIp3nP	přecházet
z	z	k7c2	z
matky	matka	k1gFnSc2	matka
na	na	k7c4	na
dítě	dítě	k1gNnSc4	dítě
během	během	k7c2	během
období	období	k1gNnSc2	období
kojení	kojení	k1gNnSc2	kojení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kosatky	kosatka	k1gFnSc2	kosatka
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
byly	být	k5eAaImAgFnP	být
vědecky	vědecky	k6eAd1	vědecky
popsány	popsat	k5eAaPmNgFnP	popsat
jako	jako	k8xC	jako
druh	druh	k1gInSc1	druh
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1758	[number]	k4	1758
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
kosatky	kosatka	k1gFnPc1	kosatka
lidem	člověk	k1gMnPc3	člověk
známy	znám	k2eAgFnPc1d1	známa
již	již	k9	již
od	od	k7c2	od
pravěku	pravěk	k1gInSc2	pravěk
<g/>
.	.	kIx.	.
</s>
<s>
Pouštní	pouštní	k2eAgFnSc1d1	pouštní
kultura	kultura	k1gFnSc1	kultura
Nazca	Nazca	k1gFnSc1	Nazca
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
na	na	k7c6	na
planině	planina	k1gFnSc6	planina
Nazca	Nazc	k1gInSc2	Nazc
kresbu	kresba	k1gFnSc4	kresba
představující	představující	k2eAgFnSc4d1	představující
kosatku	kosatka	k1gFnSc4	kosatka
někdy	někdy	k6eAd1	někdy
mezi	mezi	k7c7	mezi
roky	rok	k1gInPc7	rok
200	[number]	k4	200
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
a	a	k8xC	a
600	[number]	k4	600
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
První	první	k4xOgInSc4	první
dochovaný	dochovaný	k2eAgInSc4d1	dochovaný
popis	popis	k1gInSc4	popis
kosatky	kosatka	k1gFnSc2	kosatka
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Přírodopisu	přírodopis	k1gInSc6	přírodopis
Plinia	Plinium	k1gNnSc2	Plinium
Staršího	starší	k1gMnSc2	starší
(	(	kIx(	(
<g/>
publikován	publikovat	k5eAaBmNgInS	publikovat
77	[number]	k4	77
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
navozuje	navozovat	k5eAaImIp3nS	navozovat
dojem	dojem	k1gInSc4	dojem
nepřemožitelnosti	nepřemožitelnost	k1gFnSc2	nepřemožitelnost
vše	všechen	k3xTgNnSc4	všechen
konzumujícího	konzumující	k2eAgMnSc4d1	konzumující
dravce	dravec	k1gMnSc4	dravec
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Plinius	Plinius	k1gInSc1	Plinius
na	na	k7c4	na
vlastní	vlastní	k2eAgNnPc4d1	vlastní
oči	oko	k1gNnPc4	oko
viděl	vidět	k5eAaImAgMnS	vidět
zmasakrování	zmasakrování	k1gNnSc1	zmasakrování
velryby	velryba	k1gFnSc2	velryba
uvízlé	uvízlý	k2eAgFnSc2d1	uvízlá
v	v	k7c6	v
přístavu	přístav	k1gInSc6	přístav
poblíž	poblíž	k7c2	poblíž
Říma	Řím	k1gInSc2	Řím
<g/>
,	,	kIx,	,
napsal	napsat	k5eAaPmAgMnS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
nepřátelé	nepřítel	k1gMnPc1	nepřítel
[	[	kIx(	[
<g/>
ostatních	ostatní	k2eAgFnPc2d1	ostatní
velryb	velryba	k1gFnPc2	velryba
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
...	...	k?	...
na	na	k7c4	na
které	který	k3yQgMnPc4	který
najíždějí	najíždět	k5eAaImIp3nP	najíždět
a	a	k8xC	a
prorážejí	prorážet	k5eAaImIp3nP	prorážet
je	on	k3xPp3gFnPc4	on
jako	jako	k8xC	jako
beranidla	beranidlo	k1gNnSc2	beranidlo
válečných	válečný	k2eAgFnPc2d1	válečná
lodí	loď	k1gFnPc2	loď
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
O	o	k7c6	o
jejich	jejich	k3xOp3gInSc6	jejich
vzhledu	vzhled	k1gInSc6	vzhled
nezanechal	zanechat	k5eNaPmAgMnS	zanechat
žádný	žádný	k3yNgInSc4	žádný
bližší	blízký	k2eAgInSc4d2	bližší
popis	popis	k1gInSc4	popis
<g/>
,	,	kIx,	,
viděl	vidět	k5eAaImAgInS	vidět
jen	jen	k9	jen
obrovskou	obrovský	k2eAgFnSc4d1	obrovská
masu	masa	k1gFnSc4	masa
krutých	krutý	k2eAgNnPc2d1	kruté
těl	tělo	k1gNnPc2	tělo
se	se	k3xPyFc4	se
zuby	zub	k1gInPc7	zub
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Původní	původní	k2eAgMnPc1d1	původní
obyvatelé	obyvatel	k1gMnPc1	obyvatel
Ameriky	Amerika	k1gFnSc2	Amerika
při	při	k7c6	při
pobřeží	pobřeží	k1gNnSc6	pobřeží
severovýchodního	severovýchodní	k2eAgInSc2d1	severovýchodní
Pacifiku	Pacifik	k1gInSc2	Pacifik
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
kmeny	kmen	k1gInPc1	kmen
Tlingit	Tlingita	k1gFnPc2	Tlingita
<g/>
,	,	kIx,	,
Haida	Haida	k1gFnSc1	Haida
<g/>
,	,	kIx,	,
a	a	k8xC	a
Tsimshian	Tsimshian	k1gInSc1	Tsimshian
<g/>
,	,	kIx,	,
připisují	připisovat	k5eAaImIp3nP	připisovat
kosatce	kosatec	k1gInPc1	kosatec
význačné	význačný	k2eAgInPc1d1	význačný
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
jejich	jejich	k3xOp3gNnSc6	jejich
náboženství	náboženství	k1gNnSc1	náboženství
a	a	k8xC	a
umění	umění	k1gNnSc1	umění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kosatka	kosatka	k1gFnSc1	kosatka
a	a	k8xC	a
moderní	moderní	k2eAgMnSc1d1	moderní
člověk	člověk	k1gMnSc1	člověk
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Lov	lov	k1gInSc1	lov
===	===	k?	===
</s>
</p>
<p>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
byly	být	k5eAaImAgInP	být
počty	počet	k1gInPc1	počet
větších	veliký	k2eAgInPc2d2	veliký
druhů	druh	k1gInPc2	druh
velryb	velryba	k1gFnPc2	velryba
příliš	příliš	k6eAd1	příliš
zredukovány	zredukován	k2eAgFnPc1d1	zredukována
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
i	i	k9	i
kosatky	kosatka	k1gFnPc1	kosatka
staly	stát	k5eAaPmAgFnP	stát
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
předmětem	předmět	k1gInSc7	předmět
komerčního	komerční	k2eAgInSc2d1	komerční
lovu	lov	k1gInSc2	lov
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
byl	být	k5eAaImAgInS	být
náhle	náhle	k6eAd1	náhle
přerušen	přerušit	k5eAaPmNgInS	přerušit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
<g/>
,	,	kIx,	,
když	když	k8xS	když
vstoupilo	vstoupit	k5eAaPmAgNnS	vstoupit
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
moratorium	moratorium	k1gNnSc4	moratorium
na	na	k7c4	na
veškerý	veškerý	k3xTgInSc4	veškerý
lov	lov	k1gInSc4	lov
velryb	velryba	k1gFnPc2	velryba
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
z	z	k7c2	z
taxonomického	taxonomický	k2eAgNnSc2d1	taxonomické
hlediska	hledisko	k1gNnSc2	hledisko
je	být	k5eAaImIp3nS	být
kosatka	kosatka	k1gFnSc1	kosatka
delfín	delfín	k1gMnSc1	delfín
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dost	dost	k6eAd1	dost
velká	velký	k2eAgFnSc1d1	velká
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
ocitla	ocitnout	k5eAaPmAgFnS	ocitnout
pod	pod	k7c7	pod
dohledem	dohled	k1gInSc7	dohled
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
velrybářské	velrybářský	k2eAgFnSc2d1	velrybářská
komise	komise	k1gFnSc2	komise
(	(	kIx(	(
<g/>
International	International	k1gFnSc1	International
Whaling	Whaling	k1gInSc1	Whaling
Commission	Commission	k1gInSc1	Commission
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Největším	veliký	k2eAgMnSc7d3	veliký
lovcem	lovec	k1gMnSc7	lovec
kosatek	kosatka	k1gFnPc2	kosatka
bylo	být	k5eAaImAgNnS	být
Norsko	Norsko	k1gNnSc1	Norsko
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
lovilo	lovit	k5eAaImAgNnS	lovit
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
let	léto	k1gNnPc2	léto
1938	[number]	k4	1938
až	až	k6eAd1	až
1981	[number]	k4	1981
průměrně	průměrně	k6eAd1	průměrně
56	[number]	k4	56
zvířat	zvíře	k1gNnPc2	zvíře
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Japonsko	Japonsko	k1gNnSc1	Japonsko
spotřebovalo	spotřebovat	k5eAaPmAgNnS	spotřebovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1981	[number]	k4	1981
průměrně	průměrně	k6eAd1	průměrně
43	[number]	k4	43
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Údaje	údaj	k1gInPc4	údaj
z	z	k7c2	z
válečného	válečný	k2eAgNnSc2d1	válečné
období	období	k1gNnSc2	období
nejsou	být	k5eNaImIp3nP	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
nebo	nebo	k8xC	nebo
jsou	být	k5eAaImIp3nP	být
mnohem	mnohem	k6eAd1	mnohem
menší	malý	k2eAgFnPc1d2	menší
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
každoročně	každoročně	k6eAd1	každoročně
ulovil	ulovit	k5eAaPmAgInS	ulovit
v	v	k7c6	v
Antarktidě	Antarktida	k1gFnSc6	Antarktida
několik	několik	k4yIc4	několik
málo	málo	k4c4	málo
zvířat	zvíře	k1gNnPc2	zvíře
s	s	k7c7	s
extrémní	extrémní	k2eAgFnSc7d1	extrémní
výjimkou	výjimka	k1gFnSc7	výjimka
sezóny	sezóna	k1gFnSc2	sezóna
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
916	[number]	k4	916
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dnes	dnes	k6eAd1	dnes
nevykazuje	vykazovat	k5eNaImIp3nS	vykazovat
žádná	žádný	k3yNgFnSc1	žádný
země	zem	k1gFnSc2	zem
významný	významný	k2eAgInSc4d1	významný
lov	lov	k1gInSc4	lov
<g/>
.	.	kIx.	.
</s>
<s>
Japonci	Japonec	k1gMnPc1	Japonec
obvykle	obvykle	k6eAd1	obvykle
uloví	ulovit	k5eAaPmIp3nP	ulovit
každoročně	každoročně	k6eAd1	každoročně
několik	několik	k4yIc4	několik
jedinců	jedinec	k1gMnPc2	jedinec
jako	jako	k8xS	jako
součást	součást	k1gFnSc4	součást
jejich	jejich	k3xOp3gInSc2	jejich
kontroverzního	kontroverzní	k2eAgInSc2d1	kontroverzní
programu	program	k1gInSc2	program
vědeckého	vědecký	k2eAgInSc2d1	vědecký
výzkumu	výzkum	k1gInSc2	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
nízká	nízký	k2eAgFnSc1d1	nízká
úroveň	úroveň	k1gFnSc1	úroveň
lovu	lov	k1gInSc2	lov
je	být	k5eAaImIp3nS	být
udržována	udržován	k2eAgFnSc1d1	udržována
Indonésií	Indonésie	k1gFnSc7	Indonésie
a	a	k8xC	a
Grónskem	Grónsko	k1gNnSc7	Grónsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Krom	krom	k7c2	krom
lovu	lov	k1gInSc2	lov
pro	pro	k7c4	pro
maso	maso	k1gNnSc4	maso
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
kosatky	kosatka	k1gFnPc1	kosatka
zabíjeny	zabíjen	k2eAgFnPc1d1	zabíjena
i	i	k9	i
jako	jako	k9	jako
konkurenti	konkurent	k1gMnPc1	konkurent
rybářů	rybář	k1gMnPc2	rybář
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
padesátých	padesátý	k4xOgNnPc6	padesátý
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
letectvo	letectvo	k1gNnSc1	letectvo
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
na	na	k7c4	na
žádost	žádost	k1gFnSc4	žádost
islandské	islandský	k2eAgFnSc2d1	islandská
vlády	vláda	k1gFnSc2	vláda
použilo	použít	k5eAaPmAgNnS	použít
bomby	bomba	k1gFnPc4	bomba
a	a	k8xC	a
ostřelovače	ostřelovač	k1gMnPc4	ostřelovač
na	na	k7c4	na
vybíjení	vybíjení	k1gNnSc4	vybíjení
kosatek	kosatka	k1gFnPc2	kosatka
v	v	k7c6	v
islandských	islandský	k2eAgFnPc6d1	islandská
vodách	voda	k1gFnPc6	voda
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
konkurovaly	konkurovat	k5eAaImAgFnP	konkurovat
lidem	člověk	k1gMnPc3	člověk
při	při	k7c6	při
lovu	lov	k1gInSc6	lov
ryb	ryba	k1gFnPc2	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Operace	operace	k1gFnSc1	operace
byla	být	k5eAaImAgFnS	být
chápána	chápat	k5eAaImNgFnS	chápat
jako	jako	k8xC	jako
velký	velký	k2eAgInSc4d1	velký
úspěch	úspěch	k1gInSc4	úspěch
rybářů	rybář	k1gMnPc2	rybář
a	a	k8xC	a
islandské	islandský	k2eAgFnSc2d1	islandská
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
mnoho	mnoho	k4c1	mnoho
lidí	člověk	k1gMnPc2	člověk
nebylo	být	k5eNaImAgNnS	být
přesvědčeno	přesvědčit	k5eAaPmNgNnS	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
kosatky	kosatka	k1gFnPc1	kosatka
jsou	být	k5eAaImIp3nP	být
odpovědny	odpověden	k2eAgFnPc1d1	odpovědna
za	za	k7c4	za
pokles	pokles	k1gInSc4	pokles
rybích	rybí	k2eAgInPc2d1	rybí
stavů	stav	k1gInPc2	stav
–	–	k?	–
vinu	vinout	k5eAaImIp1nS	vinout
připisovali	připisovat	k5eAaImAgMnP	připisovat
nadměrnému	nadměrný	k2eAgInSc3d1	nadměrný
lovu	lov	k1gInSc3	lov
provozovanému	provozovaný	k2eAgInSc3d1	provozovaný
lidmi	člověk	k1gMnPc7	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
debata	debata	k1gFnSc1	debata
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
obnovení	obnovení	k1gNnSc3	obnovení
výzkumu	výzkum	k1gInSc2	výzkum
rybích	rybí	k2eAgInPc2d1	rybí
stavů	stav	k1gInPc2	stav
v	v	k7c6	v
Severním	severní	k2eAgInSc6d1	severní
Atlantiku	Atlantik	k1gInSc6	Atlantik
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
dosud	dosud	k6eAd1	dosud
nedal	dát	k5eNaPmAgInS	dát
za	za	k7c4	za
pravdu	pravda	k1gFnSc4	pravda
žádné	žádný	k3yNgFnPc4	žádný
straně	strana	k1gFnSc6	strana
tohoto	tento	k3xDgInSc2	tento
sporu	spor	k1gInSc2	spor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kosatka	kosatka	k1gFnSc1	kosatka
je	být	k5eAaImIp3nS	být
občas	občas	k6eAd1	občas
také	také	k9	také
zabíjena	zabíjen	k2eAgFnSc1d1	zabíjena
čistě	čistě	k6eAd1	čistě
jen	jen	k9	jen
ze	z	k7c2	z
strachu	strach	k1gInSc2	strach
z	z	k7c2	z
její	její	k3xOp3gFnSc2	její
pověsti	pověst	k1gFnSc2	pověst
<g/>
.	.	kIx.	.
</s>
<s>
Dosud	dosud	k6eAd1	dosud
nebyl	být	k5eNaImAgMnS	být
žádný	žádný	k3yNgMnSc1	žádný
člověk	člověk	k1gMnSc1	člověk
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
úmyslně	úmyslně	k6eAd1	úmyslně
napaden	napadnout	k5eAaPmNgMnS	napadnout
kosatkou	kosatka	k1gFnSc7	kosatka
(	(	kIx(	(
<g/>
vyskytlo	vyskytnout	k5eAaPmAgNnS	vyskytnout
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
jedno	jeden	k4xCgNnSc1	jeden
napadení	napadení	k1gNnSc1	napadení
surfaře	surfař	k1gMnSc2	surfař
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
si	se	k3xPyFc3	se
kosatka	kosatka	k1gFnSc1	kosatka
asi	asi	k9	asi
spletla	splést	k5eAaPmAgFnS	splést
s	s	k7c7	s
tuleněm	tuleň	k1gMnSc7	tuleň
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c4	po
poznání	poznání	k1gNnSc4	poznání
svého	svůj	k3xOyFgInSc2	svůj
omylu	omyl	k1gInSc2	omyl
postiženého	postižený	k1gMnSc2	postižený
bez	bez	k7c2	bez
vážnějšího	vážní	k2eAgNnSc2d2	vážnější
zranění	zranění	k1gNnSc2	zranění
pustila	pustit	k5eAaPmAgFnS	pustit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Námořníci	námořník	k1gMnPc1	námořník
u	u	k7c2	u
břehů	břeh	k1gInPc2	břeh
Aljašky	Aljaška	k1gFnSc2	Aljaška
přesto	přesto	k8xC	přesto
příležitostně	příležitostně	k6eAd1	příležitostně
střílejí	střílet	k5eAaImIp3nP	střílet
po	po	k7c6	po
zvířatech	zvíře	k1gNnPc6	zvíře
z	z	k7c2	z
obavy	obava	k1gFnSc2	obava
o	o	k7c4	o
své	svůj	k3xOyFgInPc4	svůj
životy	život	k1gInPc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Strach	strach	k1gInSc1	strach
se	se	k3xPyFc4	se
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
podařilo	podařit	k5eAaPmAgNnS	podařit
dost	dost	k6eAd1	dost
rozptýlit	rozptýlit	k5eAaPmF	rozptýlit
díky	díky	k7c3	díky
lepšímu	dobrý	k2eAgNnSc3d2	lepší
informování	informování	k1gNnSc3	informování
o	o	k7c6	o
tomto	tento	k3xDgInSc6	tento
druhu	druh	k1gInSc6	druh
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
předvádění	předvádění	k1gNnSc2	předvádění
kosatek	kosatka	k1gFnPc2	kosatka
v	v	k7c6	v
mořských	mořský	k2eAgInPc6d1	mořský
akváriích	akvárium	k1gNnPc6	akvárium
a	a	k8xC	a
jiných	jiný	k2eAgFnPc6d1	jiná
atrakcích	atrakce	k1gFnPc6	atrakce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Útoky	útok	k1gInPc4	útok
na	na	k7c6	na
lodi	loď	k1gFnSc6	loď
===	===	k?	===
</s>
</p>
<p>
<s>
Jisté	jistý	k2eAgNnSc1d1	jisté
riziko	riziko	k1gNnSc1	riziko
mohou	moct	k5eAaImIp3nP	moct
kosatky	kosatka	k1gFnPc1	kosatka
představovat	představovat	k5eAaImF	představovat
pro	pro	k7c4	pro
malé	malý	k2eAgFnPc4d1	malá
jachty	jachta	k1gFnPc4	jachta
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
by	by	kYmCp3nP	by
si	se	k3xPyFc3	se
mohly	moct	k5eAaImAgFnP	moct
splést	splést	k5eAaPmF	splést
s	s	k7c7	s
některým	některý	k3yIgInSc7	některý
z	z	k7c2	z
velkých	velká	k1gFnPc2	velká
kytovců	kytovec	k1gMnPc2	kytovec
<g/>
,	,	kIx,	,
kterými	který	k3yRgMnPc7	který
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nP	živit
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
jachta	jachta	k1gFnSc1	jachta
natřena	natřen	k2eAgFnSc1d1	natřena
stejnou	stejný	k2eAgFnSc7d1	stejná
barvou	barva	k1gFnSc7	barva
<g/>
,	,	kIx,	,
jakou	jaký	k3yIgFnSc7	jaký
má	mít	k5eAaImIp3nS	mít
kůže	kůže	k1gFnSc1	kůže
běluhy	běluha	k1gFnSc2	běluha
mořské	mořský	k2eAgFnSc2d1	mořská
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
řídký	řídký	k2eAgInSc4d1	řídký
jev	jev	k1gInSc4	jev
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
lehkých	lehký	k2eAgFnPc2d1	lehká
dřevěných	dřevěný	k2eAgFnPc2d1	dřevěná
jachet	jachta	k1gFnPc2	jachta
ovšem	ovšem	k9	ovšem
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
katastrofální	katastrofální	k2eAgInPc4d1	katastrofální
následky	následek	k1gInPc4	následek
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
vážně	vážně	k6eAd1	vážně
poškozeny	poškodit	k5eAaPmNgFnP	poškodit
hned	hned	k6eAd1	hned
prvním	první	k4xOgInSc7	první
útokem	útok	k1gInSc7	útok
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
než	než	k8xS	než
kosatky	kosatka	k1gFnPc1	kosatka
zjistí	zjistit	k5eAaPmIp3nP	zjistit
svůj	svůj	k3xOyFgInSc4	svůj
omyl	omyl	k1gInSc4	omyl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejznámějším	známý	k2eAgInSc7d3	nejznámější
případem	případ	k1gInSc7	případ
útoku	útok	k1gInSc2	útok
kosatky	kosatka	k1gFnSc2	kosatka
na	na	k7c4	na
loď	loď	k1gFnSc4	loď
je	být	k5eAaImIp3nS	být
napadení	napadení	k1gNnSc1	napadení
jachty	jachta	k1gFnSc2	jachta
Lucette	Lucett	k1gInSc5	Lucett
(	(	kIx(	(
<g/>
19	[number]	k4	19
tun	tuna	k1gFnPc2	tuna
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
13	[number]	k4	13
metrů	metr	k1gInPc2	metr
<g/>
)	)	kIx)	)
brzy	brzy	k6eAd1	brzy
ráno	ráno	k6eAd1	ráno
15	[number]	k4	15
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1972	[number]	k4	1972
<g/>
,	,	kIx,	,
200	[number]	k4	200
námořních	námořní	k2eAgFnPc2d1	námořní
mil	míle	k1gFnPc2	míle
západně	západně	k6eAd1	západně
od	od	k7c2	od
Galapág	Galapágy	k1gFnPc2	Galapágy
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
loď	loď	k1gFnSc4	loď
zaútočilo	zaútočit	k5eAaPmAgNnS	zaútočit
asi	asi	k9	asi
dvacetičlenné	dvacetičlenný	k2eAgNnSc1d1	dvacetičlenné
stádo	stádo	k1gNnSc1	stádo
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
okamžitě	okamžitě	k6eAd1	okamžitě
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
zjistilo	zjistit	k5eAaPmAgNnS	zjistit
svůj	svůj	k3xOyFgInSc4	svůj
omyl	omyl	k1gInSc4	omyl
<g/>
,	,	kIx,	,
útok	útok	k1gInSc4	útok
ukončilo	ukončit	k5eAaPmAgNnS	ukončit
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
již	již	k6eAd1	již
pozdě	pozdě	k6eAd1	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
celkem	celkem	k6eAd1	celkem
tří	tři	k4xCgInPc2	tři
útoků	útok	k1gInPc2	útok
prvního	první	k4xOgNnSc2	první
(	(	kIx(	(
<g/>
a	a	k8xC	a
posledního	poslední	k2eAgInSc2d1	poslední
<g/>
)	)	kIx)	)
nájezdu	nájezd	k1gInSc2	nájezd
byl	být	k5eAaImAgInS	být
veden	vést	k5eAaImNgInS	vést
proti	proti	k7c3	proti
olověnému	olověný	k2eAgInSc3d1	olověný
kýlu	kýl	k1gInSc3	kýl
lodi	loď	k1gFnSc2	loď
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
samozřejmě	samozřejmě	k6eAd1	samozřejmě
vydržel	vydržet	k5eAaPmAgMnS	vydržet
(	(	kIx(	(
<g/>
jeden	jeden	k4xCgMnSc1	jeden
člen	člen	k1gMnSc1	člen
posádky	posádka	k1gFnSc2	posádka
zahlédl	zahlédnout	k5eAaPmAgMnS	zahlédnout
kosatku	kosatka	k1gFnSc4	kosatka
s	s	k7c7	s
těžkým	těžký	k2eAgNnSc7d1	těžké
poraněním	poranění	k1gNnSc7	poranění
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
což	což	k3yRnSc4	což
byl	být	k5eAaImAgInS	být
asi	asi	k9	asi
následek	následek	k1gInSc1	následek
tohoto	tento	k3xDgInSc2	tento
útoku	útok	k1gInSc2	útok
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
dřevěná	dřevěný	k2eAgFnSc1d1	dřevěná
obšívka	obšívka	k1gFnSc1	obšívka
lodi	loď	k1gFnSc2	loď
tvořená	tvořený	k2eAgFnSc1d1	tvořená
ze	z	k7c2	z
čtyři	čtyři	k4xCgInPc1	čtyři
centimetry	centimetr	k1gInPc1	centimetr
tlustých	tlustý	k2eAgFnPc2d1	tlustá
planěk	plaňka	k1gFnPc2	plaňka
sosnoviny	sosnovina	k1gFnSc2	sosnovina
neobstála	obstát	k5eNaPmAgFnS	obstát
(	(	kIx(	(
<g/>
kosatka	kosatka	k1gFnSc1	kosatka
při	při	k7c6	při
lovu	lov	k1gInSc6	lov
velkých	velký	k2eAgFnPc2d1	velká
velryb	velryba	k1gFnPc2	velryba
vráží	vrážet	k5eAaImIp3nP	vrážet
do	do	k7c2	do
kořisti	kořist	k1gFnSc2	kořist
rychlostí	rychlost	k1gFnSc7	rychlost
20	[number]	k4	20
<g/>
–	–	k?	–
<g/>
25	[number]	k4	25
uzlů	uzel	k1gInPc2	uzel
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
přitom	přitom	k6eAd1	přitom
útočí	útočit	k5eAaImIp3nS	útočit
zdola	zdola	k6eAd1	zdola
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Loď	loď	k1gFnSc1	loď
se	se	k3xPyFc4	se
během	během	k7c2	během
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
minut	minuta	k1gFnPc2	minuta
potopila	potopit	k5eAaPmAgFnS	potopit
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
posádka	posádka	k1gFnSc1	posádka
ji	on	k3xPp3gFnSc4	on
ještě	ještě	k6eAd1	ještě
stihla	stihnout	k5eAaPmAgFnS	stihnout
opustit	opustit	k5eAaPmF	opustit
v	v	k7c6	v
záchranných	záchranný	k2eAgInPc6d1	záchranný
člunech	člun	k1gInPc6	člun
a	a	k8xC	a
po	po	k7c6	po
38	[number]	k4	38
dnech	den	k1gInPc6	den
byla	být	k5eAaImAgFnS	být
zachráněna	zachránit	k5eAaPmNgFnS	zachránit
japonskou	japonský	k2eAgFnSc7d1	japonská
rybářskou	rybářský	k2eAgFnSc7d1	rybářská
lodí	loď	k1gFnSc7	loď
Toka	toka	k1gFnSc1	toka
Maru	Maru	k1gFnSc1	Maru
II	II	kA	II
asi	asi	k9	asi
400	[number]	k4	400
mil	míle	k1gFnPc2	míle
severoseverovýchodně	severoseverovýchodně	k6eAd1	severoseverovýchodně
od	od	k7c2	od
místa	místo	k1gNnSc2	místo
ztroskotání	ztroskotání	k1gNnSc2	ztroskotání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zajetí	zajetí	k1gNnSc2	zajetí
===	===	k?	===
</s>
</p>
<p>
<s>
Inteligence	inteligence	k1gFnSc1	inteligence
<g/>
,	,	kIx,	,
schopnost	schopnost	k1gFnSc1	schopnost
tréninku	trénink	k1gInSc2	trénink
<g/>
,	,	kIx,	,
dobrá	dobrý	k2eAgFnSc1d1	dobrá
pověst	pověst	k1gFnSc1	pověst
<g/>
,	,	kIx,	,
hravost	hravost	k1gFnSc1	hravost
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
a	a	k8xC	a
majestátní	majestátní	k2eAgFnSc1d1	majestátní
velikost	velikost	k1gFnSc1	velikost
kosatek	kosatka	k1gFnPc2	kosatka
učinila	učinit	k5eAaPmAgFnS	učinit
představení	představení	k1gNnSc4	představení
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
velmi	velmi	k6eAd1	velmi
populární	populární	k2eAgInSc1d1	populární
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
zajetí	zajetí	k1gNnSc1	zajetí
kosatky	kosatka	k1gFnSc2	kosatka
a	a	k8xC	a
její	její	k3xOp3gNnSc1	její
předvádění	předvádění	k1gNnSc1	předvádění
se	se	k3xPyFc4	se
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
ve	v	k7c6	v
Vancouveru	Vancouver	k1gInSc6	Vancouver
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
15	[number]	k4	15
let	léto	k1gNnPc2	léto
bylo	být	k5eAaImAgNnS	být
za	za	k7c7	za
tímto	tento	k3xDgInSc7	tento
účelem	účel	k1gInSc7	účel
odchyceno	odchycen	k2eAgNnSc1d1	odchycen
v	v	k7c6	v
Tichém	tichý	k2eAgInSc6d1	tichý
oceánu	oceán	k1gInSc6	oceán
asi	asi	k9	asi
šedesát	šedesát	k4xCc1	šedesát
nebo	nebo	k8xC	nebo
sedmdesát	sedmdesát	k4xCc1	sedmdesát
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
a	a	k8xC	a
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
let	léto	k1gNnPc2	léto
80	[number]	k4	80
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
kosatky	kosatka	k1gFnPc1	kosatka
obvykle	obvykle	k6eAd1	obvykle
odebíraly	odebírat	k5eAaImAgFnP	odebírat
z	z	k7c2	z
islandských	islandský	k2eAgFnPc2d1	islandská
vod	voda	k1gFnPc2	voda
(	(	kIx(	(
<g/>
50	[number]	k4	50
za	za	k7c4	za
5	[number]	k4	5
let	léto	k1gNnPc2	léto
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
začaly	začít	k5eAaPmAgFnP	začít
být	být	k5eAaImF	být
úspěšně	úspěšně	k6eAd1	úspěšně
odchovávány	odchováván	k2eAgFnPc4d1	odchovávána
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
a	a	k8xC	a
odchyt	odchyt	k1gInSc1	odchyt
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
výrazně	výrazně	k6eAd1	výrazně
vzácnějším	vzácný	k2eAgNnSc7d2	vzácnější
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
kosatek	kosatka	k1gFnPc2	kosatka
chovaných	chovaný	k2eAgFnPc2d1	chovaná
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
vyvinout	vyvinout	k5eAaPmF	vyvinout
některé	některý	k3yIgFnPc4	některý
patologické	patologický	k2eAgFnPc4d1	patologická
změny	změna	k1gFnPc4	změna
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgNnPc2	jenž
nejčastější	častý	k2eAgMnSc1d3	nejčastější
je	být	k5eAaImIp3nS	být
zborcení	zborcený	k2eAgMnPc1d1	zborcený
hřbetní	hřbetní	k2eAgMnPc1d1	hřbetní
ploutve	ploutev	k1gFnSc2	ploutev
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
postihuje	postihovat	k5eAaImIp3nS	postihovat
asi	asi	k9	asi
60	[number]	k4	60
<g/>
–	–	k?	–
<g/>
90	[number]	k4	90
%	%	kIx~	%
chovaných	chovaný	k2eAgInPc2d1	chovaný
samců	samec	k1gInPc2	samec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
byly	být	k5eAaImAgInP	být
zaznamenány	zaznamenat	k5eAaPmNgInP	zaznamenat
případy	případ	k1gInPc1	případ
kosatek	kosatka	k1gFnPc2	kosatka
napadající	napadající	k2eAgMnPc4d1	napadající
lidi	člověk	k1gMnPc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
vodním	vodní	k2eAgInSc6d1	vodní
parku	park	k1gInSc6	park
Sealand	Sealand	k1gInSc4	Sealand
of	of	k?	of
Victoria	Victorium	k1gNnSc2	Victorium
(	(	kIx(	(
<g/>
kde	kde	k6eAd1	kde
nebylo	být	k5eNaImAgNnS	být
zaměstnancům	zaměstnanec	k1gMnPc3	zaměstnanec
dovoleno	dovolit	k5eAaPmNgNnS	dovolit
vstupovat	vstupovat	k5eAaImF	vstupovat
do	do	k7c2	do
vody	voda	k1gFnSc2	voda
ke	k	k7c3	k
kosatkám	kosatka	k1gFnPc3	kosatka
<g/>
)	)	kIx)	)
skupinou	skupina	k1gFnSc7	skupina
kosatek	kosatka	k1gFnPc2	kosatka
zabita	zabit	k2eAgFnSc1d1	zabita
trenérka	trenérka	k1gFnSc1	trenérka
Keltie	Keltie	k1gFnSc2	Keltie
Byrne	Byrn	k1gMnSc5	Byrn
<g/>
.	.	kIx.	.
</s>
<s>
Zvířata	zvíře	k1gNnPc1	zvíře
patrně	patrně	k6eAd1	patrně
nevěděla	vědět	k5eNaImAgNnP	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
člověk	člověk	k1gMnSc1	člověk
nevydrží	vydržet	k5eNaPmIp3nS	vydržet
dlouho	dlouho	k6eAd1	dlouho
pod	pod	k7c7	pod
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
kosatek	kosatka	k1gFnPc2	kosatka
údajně	údajně	k6eAd1	údajně
zabila	zabít	k5eAaPmAgFnS	zabít
turistu	turista	k1gMnSc4	turista
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
vplížil	vplížit	k5eAaPmAgInS	vplížit
do	do	k7c2	do
bazénu	bazén	k1gInSc2	bazén
(	(	kIx(	(
<g/>
uvažuje	uvažovat	k5eAaImIp3nS	uvažovat
se	se	k3xPyFc4	se
však	však	k9	však
také	také	k9	také
<g/>
,	,	kIx,	,
že	že	k8xS	že
turista	turista	k1gMnSc1	turista
mohl	moct	k5eAaImAgMnS	moct
být	být	k5eAaImF	být
obětí	oběť	k1gFnSc7	oběť
hypotermie	hypotermie	k1gFnSc2	hypotermie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
července	červenec	k1gInSc2	červenec
2004	[number]	k4	2004
kosatka	kosatka	k1gFnSc1	kosatka
během	během	k7c2	během
produkce	produkce	k1gFnSc2	produkce
parku	park	k1gInSc2	park
SeaWorld	SeaWorldo	k1gNnPc2	SeaWorldo
v	v	k7c6	v
Texasu	Texas	k1gInSc6	Texas
strčila	strčit	k5eAaPmAgFnS	strčit
svého	svůj	k3xOyFgMnSc4	svůj
trenéra	trenér	k1gMnSc4	trenér
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
o	o	k7c4	o
ni	on	k3xPp3gFnSc4	on
již	již	k6eAd1	již
10	[number]	k4	10
let	léto	k1gNnPc2	léto
staral	starat	k5eAaImAgMnS	starat
<g/>
,	,	kIx,	,
pod	pod	k7c4	pod
vodu	voda	k1gFnSc4	voda
a	a	k8xC	a
tlačila	tlačit	k5eAaImAgFnS	tlačit
ho	on	k3xPp3gMnSc4	on
proti	proti	k7c3	proti
okraji	okraj	k1gInSc3	okraj
nádrže	nádrž	k1gFnSc2	nádrž
<g/>
;	;	kIx,	;
trenéra	trenér	k1gMnSc2	trenér
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
dostat	dostat	k5eAaPmF	dostat
z	z	k7c2	z
dosahu	dosah	k1gInSc2	dosah
zuřícího	zuřící	k2eAgNnSc2d1	zuřící
zvířete	zvíře	k1gNnSc2	zvíře
až	až	k9	až
po	po	k7c6	po
několika	několik	k4yIc6	několik
minutách	minuta	k1gFnPc6	minuta
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgMnPc2d3	nejznámější
dokumentovaných	dokumentovaný	k2eAgMnPc2d1	dokumentovaný
případů	případ	k1gInPc2	případ
vnitrodruhové	vnitrodruhový	k2eAgFnSc2d1	vnitrodruhová
agrese	agrese	k1gFnSc2	agrese
kosatky	kosatka	k1gFnSc2	kosatka
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
během	během	k7c2	během
představení	představení	k1gNnSc2	představení
jedna	jeden	k4xCgFnSc1	jeden
samice	samice	k1gFnSc1	samice
<g/>
,	,	kIx,	,
Kandu	Kanda	k1gFnSc4	Kanda
V	V	kA	V
(	(	kIx(	(
<g/>
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
pokládala	pokládat	k5eAaImAgFnS	pokládat
za	za	k7c4	za
dominantní	dominantní	k2eAgFnSc4d1	dominantní
samici	samice	k1gFnSc4	samice
<g/>
)	)	kIx)	)
praštila	praštit	k5eAaPmAgFnS	praštit
samici	samice	k1gFnSc4	samice
Corky	Corka	k1gFnSc2	Corka
II	II	kA	II
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
dovezena	dovézt	k5eAaPmNgFnS	dovézt
z	z	k7c2	z
Marineworldu	Marineworld	k1gInSc2	Marineworld
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
asi	asi	k9	asi
měsíc	měsíc	k1gInSc4	měsíc
před	před	k7c7	před
incidentem	incident	k1gInSc7	incident
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Podle	podle	k7c2	podle
svědectví	svědectví	k1gNnSc2	svědectví
slyšelo	slyšet	k5eAaImAgNnS	slyšet
celé	celý	k2eAgNnSc4d1	celé
publikum	publikum	k1gNnSc4	publikum
hlasité	hlasitý	k2eAgNnSc4d1	hlasité
mlaskavé	mlaskavý	k2eAgNnSc4d1	mlaskavé
plácnutí	plácnutí	k1gNnSc4	plácnutí
<g/>
.	.	kIx.	.
</s>
<s>
Trenéři	trenér	k1gMnPc1	trenér
se	se	k3xPyFc4	se
nejprve	nejprve	k6eAd1	nejprve
snažili	snažit	k5eAaImAgMnP	snažit
pokračovat	pokračovat	k5eAaImF	pokračovat
v	v	k7c4	v
představení	představení	k1gNnSc4	představení
<g/>
,	,	kIx,	,
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
se	se	k3xPyFc4	se
však	však	k9	však
<g/>
,	,	kIx,	,
že	že	k8xS	že
Kandu	Kanda	k1gMnSc4	Kanda
V	V	kA	V
utrpěla	utrpět	k5eAaPmAgFnS	utrpět
zranění	zranění	k1gNnSc4	zranění
tepny	tepna	k1gFnSc2	tepna
poblíž	poblíž	k7c2	poblíž
svých	svůj	k3xOyFgFnPc2	svůj
čelistí	čelist	k1gFnPc2	čelist
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgInSc2	jenž
začala	začít	k5eAaPmAgFnS	začít
chrlit	chrlit	k5eAaImF	chrlit
krev	krev	k1gFnSc1	krev
<g/>
.	.	kIx.	.
</s>
<s>
Představení	představení	k1gNnSc1	představení
bylo	být	k5eAaImAgNnS	být
ukončeno	ukončit	k5eAaPmNgNnS	ukončit
a	a	k8xC	a
Kandu	Kanda	k1gFnSc4	Kanda
po	po	k7c6	po
45	[number]	k4	45
minutách	minuta	k1gFnPc6	minuta
krvácení	krvácení	k1gNnSc4	krvácení
zemřela	zemřít	k5eAaPmAgFnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Odpůrci	odpůrce	k1gMnPc1	odpůrce
těchto	tento	k3xDgFnPc6	tento
představení	představený	k1gMnPc1	představený
chápali	chápat	k5eAaImAgMnP	chápat
takovéto	takovýto	k3xDgInPc4	takovýto
incidenty	incident	k1gInPc4	incident
jako	jako	k8xS	jako
potvrzení	potvrzení	k1gNnSc4	potvrzení
své	svůj	k3xOyFgFnSc2	svůj
kritiky	kritika	k1gFnSc2	kritika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SeaWorld	SeaWorld	k1gInSc1	SeaWorld
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
držení	držení	k1gNnSc2	držení
kosatek	kosatka	k1gFnPc2	kosatka
odchycených	odchycený	k2eAgFnPc2d1	odchycená
z	z	k7c2	z
přírody	příroda	k1gFnSc2	příroda
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
terčem	terč	k1gInSc7	terč
kritiky	kritika	k1gFnSc2	kritika
nadace	nadace	k1gFnSc2	nadace
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
zvířat	zvíře	k1gNnPc2	zvíře
v	v	k7c6	v
jejich	jejich	k3xOp3gNnSc6	jejich
přirozeném	přirozený	k2eAgNnSc6d1	přirozené
prostředí	prostředí	k1gNnSc6	prostředí
Born	Born	k1gNnSc1	Born
Free	Free	k1gNnSc2	Free
Foundation	Foundation	k1gInSc4	Foundation
především	především	k9	především
kvůli	kvůli	k7c3	kvůli
pokračující	pokračující	k2eAgFnSc7d1	pokračující
zajetí	zajetí	k1gNnSc4	zajetí
kosatky	kosatka	k1gFnSc2	kosatka
Corky	Corka	k1gFnSc2	Corka
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
nadace	nadace	k1gFnSc1	nadace
usiluje	usilovat	k5eAaImIp3nS	usilovat
vrátit	vrátit	k5eAaPmF	vrátit
k	k	k7c3	k
její	její	k3xOp3gFnSc3	její
rodině	rodina	k1gFnSc3	rodina
do	do	k7c2	do
stáda	stádo	k1gNnSc2	stádo
A5	A5	k1gFnSc2	A5
–	–	k?	–
velkého	velký	k2eAgNnSc2d1	velké
hejna	hejno	k1gNnSc2	hejno
kosatek	kosatka	k1gFnPc2	kosatka
vyskytujícího	vyskytující	k2eAgNnSc2d1	vyskytující
se	se	k3xPyFc4	se
u	u	k7c2	u
pobřeží	pobřeží	k1gNnSc2	pobřeží
Britské	britský	k2eAgFnSc2d1	britská
Kolumbie	Kolumbie	k1gFnSc2	Kolumbie
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Film	film	k1gInSc1	film
a	a	k8xC	a
kultura	kultura	k1gFnSc1	kultura
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
hrály	hrát	k5eAaImAgFnP	hrát
kosatky	kosatka	k1gFnPc1	kosatka
ve	v	k7c6	v
fiktivních	fiktivní	k2eAgInPc6d1	fiktivní
příbězích	příběh	k1gInPc6	příběh
roli	role	k1gFnSc4	role
hltavých	hltavý	k2eAgMnPc2d1	hltavý
predátorů	predátor	k1gMnPc2	predátor
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnPc4	jejichž
chování	chování	k1gNnSc1	chování
podněcuje	podněcovat	k5eAaImIp3nS	podněcovat
hrdiny	hrdina	k1gMnPc4	hrdina
k	k	k7c3	k
úsilí	úsilí	k1gNnSc3	úsilí
pomoci	pomoc	k1gFnSc2	pomoc
jejich	jejich	k3xOp3gFnSc2	jejich
kořisti	kořist	k1gFnSc2	kořist
utéci	utéct	k5eAaPmF	utéct
<g/>
.	.	kIx.	.
</s>
<s>
Nejextrémnějším	extrémní	k2eAgInSc7d3	nejextrémnější
příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
nepříliš	příliš	k6eNd1	příliš
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
film	film	k1gInSc1	film
Orca	Orcum	k1gNnSc2	Orcum
zabiják	zabiják	k1gInSc1	zabiják
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
příběh	příběh	k1gInSc1	příběh
samce	samec	k1gMnSc4	samec
kosatky	kosatka	k1gFnSc2	kosatka
<g/>
,	,	kIx,	,
vydávajícího	vydávající	k2eAgMnSc2d1	vydávající
se	se	k3xPyFc4	se
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
zuřivé	zuřivý	k2eAgFnSc2d1	zuřivá
pomsty	pomsta	k1gFnSc2	pomsta
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gFnSc1	jeho
březí	březí	k2eAgFnSc1d1	březí
samice	samice	k1gFnSc1	samice
zabita	zabit	k2eAgFnSc1d1	zabita
lidmi	člověk	k1gMnPc7	člověk
(	(	kIx(	(
<g/>
zjevná	zjevný	k2eAgFnSc1d1	zjevná
je	být	k5eAaImIp3nS	být
inspirace	inspirace	k1gFnSc1	inspirace
filmem	film	k1gInSc7	film
Čelisti	čelist	k1gFnSc2	čelist
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pokračující	pokračující	k2eAgInSc1d1	pokračující
výzkum	výzkum	k1gInSc1	výzkum
zvířete	zvíře	k1gNnSc2	zvíře
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
popularita	popularita	k1gFnSc1	popularita
v	v	k7c6	v
akváriích	akvárium	k1gNnPc6	akvárium
přinesly	přinést	k5eAaPmAgFnP	přinést
dramatické	dramatický	k2eAgNnSc4d1	dramatické
vylepšení	vylepšení	k1gNnSc4	vylepšení
jeho	jeho	k3xOp3gInSc2	jeho
obrazu	obraz	k1gInSc2	obraz
na	na	k7c6	na
veřejnosti	veřejnost	k1gFnSc6	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
je	být	k5eAaImIp3nS	být
kosatka	kosatka	k1gFnSc1	kosatka
chápána	chápat	k5eAaImNgFnS	chápat
především	především	k9	především
jako	jako	k9	jako
respektovaný	respektovaný	k2eAgMnSc1d1	respektovaný
predátor	predátor	k1gMnSc1	predátor
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
pro	pro	k7c4	pro
lidi	člověk	k1gMnPc4	člověk
představuje	představovat	k5eAaImIp3nS	představovat
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
malou	malý	k2eAgFnSc4d1	malá
hrozbu	hrozba	k1gFnSc4	hrozba
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
připomíná	připomínat	k5eAaImIp3nS	připomínat
změnu	změna	k1gFnSc4	změna
náhledu	náhled	k1gInSc2	náhled
na	na	k7c4	na
severoamerického	severoamerický	k2eAgMnSc4d1	severoamerický
vlka	vlk	k1gMnSc4	vlk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Film	film	k1gInSc4	film
Zachraňte	zachránit	k5eAaPmRp2nP	zachránit
Willyho	Willy	k1gMnSc2	Willy
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
zaměřil	zaměřit	k5eAaPmAgInS	zaměřit
na	na	k7c4	na
otázku	otázka	k1gFnSc4	otázka
osvobození	osvobození	k1gNnSc2	osvobození
kosatek	kosatka	k1gFnPc2	kosatka
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
<g/>
.	.	kIx.	.
</s>
<s>
Kosatka	kosatka	k1gFnSc1	kosatka
hrající	hrající	k2eAgFnSc1d1	hrající
ve	v	k7c6	v
filmu	film	k1gInSc6	film
<g/>
,	,	kIx,	,
Keiko	Keiko	k1gNnSc4	Keiko
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
původně	původně	k6eAd1	původně
odchycena	odchytit	k5eAaPmNgFnS	odchytit
ve	v	k7c6	v
vodách	voda	k1gFnPc6	voda
Islandu	Island	k1gInSc2	Island
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
přípravě	příprava	k1gFnSc6	příprava
na	na	k7c4	na
návrat	návrat	k1gInSc4	návrat
do	do	k7c2	do
volné	volný	k2eAgFnSc2d1	volná
přírody	příroda	k1gFnSc2	příroda
v	v	k7c6	v
akváriu	akvárium	k1gNnSc6	akvárium
v	v	k7c6	v
oregonském	oregonský	k2eAgInSc6d1	oregonský
Newportu	Newport	k1gInSc6	Newport
u	u	k7c2	u
(	(	kIx(	(
<g/>
Oregon	Oregona	k1gFnPc2	Oregona
Coast	Coast	k1gInSc1	Coast
Aquarium	Aquarium	k1gNnSc1	Aquarium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
přemístěna	přemístit	k5eAaPmNgFnS	přemístit
do	do	k7c2	do
severních	severní	k2eAgFnPc2d1	severní
vod	voda	k1gFnPc2	voda
<g/>
,	,	kIx,	,
svého	svůj	k3xOyFgInSc2	svůj
původního	původní	k2eAgInSc2d1	původní
životního	životní	k2eAgInSc2d1	životní
prostoru	prostor	k1gInSc2	prostor
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
nepřestala	přestat	k5eNaPmAgFnS	přestat
být	být	k5eAaImF	být
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c6	na
lidech	člověk	k1gMnPc6	člověk
až	až	k8xS	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ohrožení	ohrožení	k1gNnSc1	ohrožení
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
===	===	k?	===
</s>
</p>
<p>
<s>
Olejová	olejový	k2eAgFnSc1d1	olejová
skvrna	skvrna	k1gFnSc1	skvrna
z	z	k7c2	z
tankeru	tanker	k1gInSc2	tanker
Exxon	Exxon	k1gInSc1	Exxon
Valdez	Valdez	k1gInSc1	Valdez
měla	mít	k5eAaImAgFnS	mít
určitý	určitý	k2eAgInSc4d1	určitý
nepříznivý	příznivý	k2eNgInSc4d1	nepříznivý
efekt	efekt	k1gInSc4	efekt
na	na	k7c4	na
aljašskou	aljašský	k2eAgFnSc4d1	aljašská
populaci	populace	k1gFnSc4	populace
<g/>
.	.	kIx.	.
</s>
<s>
Jedno	jeden	k4xCgNnSc1	jeden
stádo	stádo	k1gNnSc1	stádo
bylo	být	k5eAaImAgNnS	být
zachyceno	zachytit	k5eAaPmNgNnS	zachytit
skvrnou	skvrna	k1gFnSc7	skvrna
a	a	k8xC	a
ačkoliv	ačkoliv	k8xS	ačkoliv
jeho	jeho	k3xOp3gMnPc1	jeho
členové	člen	k1gMnPc1	člen
ihned	ihned	k6eAd1	ihned
plavali	plavat	k5eAaImAgMnP	plavat
skrz	skrz	k7c4	skrz
olej	olej	k1gInSc4	olej
do	do	k7c2	do
čistých	čistý	k2eAgFnPc2d1	čistá
vod	voda	k1gFnPc2	voda
<g/>
,	,	kIx,	,
jedenáct	jedenáct	k4xCc4	jedenáct
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
(	(	kIx(	(
<g/>
asi	asi	k9	asi
polovina	polovina	k1gFnSc1	polovina
<g/>
)	)	kIx)	)
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
v	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
dnech	den	k1gInPc6	den
a	a	k8xC	a
týdnech	týden	k1gInPc6	týden
<g/>
.	.	kIx.	.
</s>
<s>
Skvrna	skvrna	k1gFnSc1	skvrna
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
snížila	snížit	k5eAaPmAgFnS	snížit
množství	množství	k1gNnSc3	množství
dostupné	dostupný	k2eAgFnSc2d1	dostupná
potravy	potrava	k1gFnSc2	potrava
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
lososi	losos	k1gMnPc1	losos
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
dalšímu	další	k2eAgNnSc3d1	další
snížení	snížení	k1gNnSc3	snížení
lokální	lokální	k2eAgFnSc2d1	lokální
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2004	[number]	k4	2004
vědci	vědec	k1gMnPc1	vědec
z	z	k7c2	z
North	Northa	k1gFnPc2	Northa
Gulf	Gulf	k1gInSc1	Gulf
Oceanic	Oceanice	k1gFnPc2	Oceanice
Society	societa	k1gFnSc2	societa
publikovali	publikovat	k5eAaBmAgMnP	publikovat
zprávu	zpráva	k1gFnSc4	zpráva
<g/>
,	,	kIx,	,
že	že	k8xS	že
stádo	stádo	k1gNnSc1	stádo
zvané	zvaný	k2eAgFnSc2d1	zvaná
AT1	AT1	k1gFnSc2	AT1
se	se	k3xPyFc4	se
nyní	nyní	k6eAd1	nyní
zmenšilo	zmenšit	k5eAaPmAgNnS	zmenšit
na	na	k7c4	na
sedm	sedm	k4xCc4	sedm
členů	člen	k1gInPc2	člen
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
kvůli	kvůli	k7c3	kvůli
skvrně	skvrna	k1gFnSc3	skvrna
úplně	úplně	k6eAd1	úplně
zastavila	zastavit	k5eAaPmAgFnS	zastavit
jeho	jeho	k3xOp3gFnSc1	jeho
reprodukce	reprodukce	k1gFnSc1	reprodukce
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
vymře	vymřít	k5eAaPmIp3nS	vymřít
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
Podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
jiná	jiný	k2eAgNnPc1d1	jiné
zvířata	zvíře	k1gNnPc1	zvíře
na	na	k7c6	na
vysokých	vysoký	k2eAgFnPc6d1	vysoká
úrovních	úroveň	k1gFnPc6	úroveň
potravního	potravní	k2eAgInSc2d1	potravní
řetězce	řetězec	k1gInSc2	řetězec
jsou	být	k5eAaImIp3nP	být
kosatky	kosatka	k1gFnPc4	kosatka
postiženy	postižen	k2eAgInPc1d1	postižen
hromaděním	hromadění	k1gNnSc7	hromadění
polychlorovaných	polychlorovaný	k2eAgInPc2d1	polychlorovaný
bifenylů	bifenyl	k1gInPc2	bifenyl
ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
tělech	tělo	k1gNnPc6	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Vyšetření	vyšetření	k1gNnSc1	vyšetření
zvířat	zvíře	k1gNnPc2	zvíře
u	u	k7c2	u
washingtonského	washingtonský	k2eAgNnSc2d1	washingtonské
pobřeží	pobřeží	k1gNnSc2	pobřeží
vedlo	vést	k5eAaImAgNnS	vést
ke	k	k7c3	k
zjištění	zjištění	k1gNnSc3	zjištění
<g/>
,	,	kIx,	,
že	že	k8xS	že
úroveň	úroveň	k1gFnSc4	úroveň
PCB	PCB	kA	PCB
v	v	k7c6	v
tělech	tělo	k1gNnPc6	tělo
tamních	tamní	k2eAgFnPc2d1	tamní
kosatek	kosatka	k1gFnPc2	kosatka
je	být	k5eAaImIp3nS	být
vyšší	vysoký	k2eAgFnSc1d2	vyšší
než	než	k8xS	než
u	u	k7c2	u
tuleně	tuleň	k1gMnSc2	tuleň
obecného	obecný	k2eAgMnSc2d1	obecný
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterého	který	k3yRgNnSc2	který
však	však	k9	však
již	již	k9	již
byly	být	k5eAaImAgFnP	být
zaznamenány	zaznamenat	k5eAaPmNgFnP	zaznamenat
známky	známka	k1gFnPc1	známka
onemocnění	onemocnění	k1gNnSc2	onemocnění
zapříčiněného	zapříčiněný	k2eAgInSc2d1	zapříčiněný
touto	tento	k3xDgFnSc7	tento
chemikálií	chemikálie	k1gFnSc7	chemikálie
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
kosatek	kosatka	k1gFnPc2	kosatka
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
žádné	žádný	k3yNgInPc1	žádný
takové	takový	k3xDgInPc1	takový
příznaky	příznak	k1gInPc1	příznak
zaznamenány	zaznamenat	k5eAaPmNgInP	zaznamenat
nebyly	být	k5eNaImAgInP	být
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
to	ten	k3xDgNnSc1	ten
u	u	k7c2	u
nich	on	k3xPp3gMnPc2	on
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
nějakým	nějaký	k3yIgInPc3	nějaký
následkům	následek	k1gInPc3	následek
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nejpravděpodobnější	pravděpodobný	k2eAgFnSc1d3	nejpravděpodobnější
snížená	snížený	k2eAgFnSc1d1	snížená
schopnost	schopnost	k1gFnSc1	schopnost
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
ostatní	ostatní	k2eAgInPc4d1	ostatní
tlaky	tlak	k1gInPc4	tlak
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
jimž	jenž	k3xRgInPc3	jenž
musí	muset	k5eAaImIp3nP	muset
kosatky	kosatka	k1gFnPc4	kosatka
čelit	čelit	k5eAaImF	čelit
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
rozsáhlé	rozsáhlý	k2eAgNnSc1d1	rozsáhlé
pozorování	pozorování	k1gNnSc1	pozorování
velryb	velryba	k1gFnPc2	velryba
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
již	již	k6eAd1	již
některé	některý	k3yIgInPc1	některý
průzkumy	průzkum	k1gInPc1	průzkum
označují	označovat	k5eAaImIp3nP	označovat
za	za	k7c4	za
příčinu	příčina	k1gFnSc4	příčina
změn	změna	k1gFnPc2	změna
jejich	jejich	k3xOp3gNnPc4	jejich
chování	chování	k1gNnSc4	chování
<g/>
.	.	kIx.	.
</s>
<s>
Silný	silný	k2eAgInSc1d1	silný
hluk	hluk	k1gInSc1	hluk
lodí	loď	k1gFnPc2	loď
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
u	u	k7c2	u
některých	některý	k3yIgNnPc2	některý
stád	stádo	k1gNnPc2	stádo
kosatek	kosatka	k1gFnPc2	kosatka
změnu	změna	k1gFnSc4	změna
frekvencí	frekvence	k1gFnSc7	frekvence
jejich	jejich	k3xOp3gFnPc2	jejich
písní	píseň	k1gFnPc2	píseň
a	a	k8xC	a
volání	volání	k1gNnPc2	volání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Orca	Orc	k1gInSc2	Orc
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
HOYT	HOYT	kA	HOYT
<g/>
,	,	kIx,	,
Erich	Erich	k1gMnSc1	Erich
<g/>
.	.	kIx.	.
</s>
<s>
Orca	Orca	k1gMnSc1	Orca
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Whale	Whale	k1gFnSc2	Whale
Called	Called	k1gMnSc1	Called
Killer	Killer	k1gMnSc1	Killer
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
Camden	Camdna	k1gFnPc2	Camdna
House	house	k1gNnSc1	house
Publishing	Publishing	k1gInSc4	Publishing
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
920656250	[number]	k4	920656250
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
FORD	ford	k1gInSc1	ford
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
K.	K.	kA	K.
B.	B.	kA	B.
Encyclopedia	Encyclopedium	k1gNnPc4	Encyclopedium
of	of	k?	of
Marine	Marin	k1gInSc5	Marin
Mammals	Mammals	k1gInSc4	Mammals
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
Academic	Academic	k1gMnSc1	Academic
Press	Pressa	k1gFnPc2	Pressa
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
125513402	[number]	k4	125513402
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
Killer	Killer	k1gInSc1	Killer
Whale	Whale	k1gFnSc1	Whale
<g/>
,	,	kIx,	,
s.	s.	k?	s.
669	[number]	k4	669
<g/>
–	–	k?	–
<g/>
675	[number]	k4	675
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
National	Nationat	k5eAaImAgMnS	Nationat
Audubon	Audubon	k1gMnSc1	Audubon
Society	societa	k1gFnSc2	societa
Guide	Guid	k1gInSc5	Guid
to	ten	k3xDgNnSc1	ten
Marine	Marin	k1gInSc5	Marin
Mammals	Mammalsa	k1gFnPc2	Mammalsa
of	of	k?	of
the	the	k?	the
World	World	k1gInSc1	World
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
Alfred	Alfred	k1gMnSc1	Alfred
A.	A.	kA	A.
Knopf	Knopf	k1gMnSc1	Knopf
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
375411410	[number]	k4	375411410
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ELLIS	ELLIS	kA	ELLIS
<g/>
,	,	kIx,	,
Graeme	Graem	k1gInSc5	Graem
<g/>
;	;	kIx,	;
OBEE	OBEE	kA	OBEE
<g/>
,	,	kIx,	,
Bruce	Bruce	k1gMnSc1	Bruce
<g/>
.	.	kIx.	.
</s>
<s>
Guardians	Guardians	k1gInSc1	Guardians
of	of	k?	of
the	the	k?	the
Whales	Whales	k1gInSc1	Whales
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
Whitecap	Whitecap	k1gInSc1	Whitecap
Books	Books	k1gInSc1	Books
ISBN	ISBN	kA	ISBN
1551100347	[number]	k4	1551100347
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
FORD	ford	k1gInSc1	ford
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
K.	K.	kA	K.
&	&	k?	&
B.	B.	kA	B.
<g/>
;	;	kIx,	;
ELLIS	ELLIS	kA	ELLIS
<g/>
,	,	kIx,	,
Graeme	Graem	k1gInSc5	Graem
<g/>
;	;	kIx,	;
BALCOMB	BALCOMB	kA	BALCOMB
<g/>
,	,	kIx,	,
Kenneth	Kenneth	k1gMnSc1	Kenneth
C.	C.	kA	C.
Killer	Killer	k1gMnSc1	Killer
Whales	Whales	k1gMnSc1	Whales
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
UBC	UBC	kA	UBC
Press	Pressa	k1gFnPc2	Pressa
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
774804696	[number]	k4	774804696
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
kosatka	kosatka	k1gFnSc1	kosatka
dravá	dravý	k2eAgFnSc1d1	dravá
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Orcinus	Orcinus	k1gMnSc1	Orcinus
orca	orca	k1gMnSc1	orca
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
od	od	k7c2	od
Monterey	Monterea	k1gFnSc2	Monterea
Bay	Bay	k1gFnSc2	Bay
Whale	Whale	k1gFnSc2	Whale
Watch	Watch	k1gInSc1	Watch
<g/>
:	:	kIx,	:
Kosatky	kosatka	k1gFnSc2	kosatka
útočící	útočící	k2eAgFnSc2d1	útočící
na	na	k7c6	na
plejtvákovce	plejtvákovka	k1gFnSc6	plejtvákovka
šedého	šedý	k2eAgNnSc2d1	šedé
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Dravé	dravý	k2eAgNnSc1d1	dravé
chování	chování	k1gNnSc1	chování
nestálých	stálý	k2eNgFnPc2d1	nestálá
kosatek	kosatka	k1gFnPc2	kosatka
v	v	k7c6	v
zálivu	záliv	k1gInSc6	záliv
Monterey	Monterea	k1gFnSc2	Monterea
<g/>
,	,	kIx,	,
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Archiv	archiv	k1gInSc1	archiv
snímků	snímek	k1gInPc2	snímek
kosatek	kosatka	k1gFnPc2	kosatka
z	z	k7c2	z
arktického	arktický	k2eAgNnSc2d1	arktické
Norska	Norsko	k1gNnSc2	Norsko
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Výzkumný	výzkumný	k2eAgInSc1d1	výzkumný
projekt	projekt	k1gInSc1	projekt
studující	studující	k2eAgFnSc2d1	studující
kosatky	kosatka	k1gFnSc2	kosatka
v	v	k7c6	v
norské	norský	k2eAgFnSc6d1	norská
Arktidě	Arktida	k1gFnSc6	Arktida
(	(	kIx(	(
<g/>
norsky	norsky	k6eAd1	norsky
<g/>
)	)	kIx)	)
</s>
</p>
