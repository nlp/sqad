<s>
Ajmarština	Ajmarština	k1gFnSc1	Ajmarština
<g/>
,	,	kIx,	,
aymarština	aymarština	k1gFnSc1	aymarština
<g/>
,	,	kIx,	,
ajmara	ajmara	k1gFnSc1	ajmara
nebo	nebo	k8xC	nebo
aymara	aymara	k1gFnSc1	aymara
(	(	kIx(	(
<g/>
španělsky	španělsky	k6eAd1	španělsky
aimara	aimara	k1gFnSc1	aimara
<g/>
,	,	kIx,	,
aimará	aimarý	k2eAgFnSc1d1	aimarý
<g/>
,	,	kIx,	,
aymara	aymara	k1gFnSc1	aymara
či	či	k8xC	či
aymará	aymarý	k2eAgFnSc1d1	aymarý
<g/>
,	,	kIx,	,
vlastní	vlastní	k2eAgInSc1d1	vlastní
název	název	k1gInSc1	název
aymar	aymara	k1gFnPc2	aymara
aru	ar	k1gInSc2	ar
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
indiánský	indiánský	k2eAgInSc1d1	indiánský
jazyk	jazyk	k1gInSc1	jazyk
z	z	k7c2	z
And	Anda	k1gFnPc2	Anda
<g/>
.	.	kIx.	.
</s>
