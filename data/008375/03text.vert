<p>
<s>
Cchaj	Cchaj	k1gFnSc1	Cchaj
Lun	luna	k1gFnPc2	luna
(	(	kIx(	(
<g/>
čínsky	čínsky	k6eAd1	čínsky
pchin-jinem	pchinin	k1gInSc7	pchin-jin
Cà	Cà	k1gFnSc2	Cà
Lún	Lún	k1gFnSc2	Lún
<g/>
,	,	kIx,	,
znaky	znak	k1gInPc4	znak
zjednodušené	zjednodušený	k2eAgFnSc2d1	zjednodušená
蔡	蔡	k?	蔡
<g/>
,	,	kIx,	,
tradiční	tradiční	k2eAgInSc1d1	tradiční
蔡	蔡	k?	蔡
<g/>
,	,	kIx,	,
asi	asi	k9	asi
50	[number]	k4	50
<g/>
,	,	kIx,	,
Kuej-jang	Kuejang	k1gInSc1	Kuej-jang
<g/>
,	,	kIx,	,
Chu-nan	Chuan	k1gInSc1	Chu-nan
–	–	k?	–
121	[number]	k4	121
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
vysoký	vysoký	k2eAgMnSc1d1	vysoký
čínský	čínský	k2eAgMnSc1d1	čínský
úředník	úředník	k1gMnSc1	úředník
a	a	k8xC	a
ministr	ministr	k1gMnSc1	ministr
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
čínské	čínský	k2eAgFnSc3d1	čínská
tradici	tradice	k1gFnSc3	tradice
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
vynálezce	vynálezce	k1gMnSc4	vynálezce
papíru	papír	k1gInSc2	papír
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Cchaj	Cchaj	k1gMnSc1	Cchaj
Lun	luna	k1gFnPc2	luna
byl	být	k5eAaImAgMnS	být
eunuch	eunuch	k1gMnSc1	eunuch
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
do	do	k7c2	do
služeb	služba	k1gFnPc2	služba
císařského	císařský	k2eAgInSc2d1	císařský
paláce	palác	k1gInSc2	palác
v	v	k7c6	v
roce	rok	k1gInSc6	rok
75	[number]	k4	75
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
89	[number]	k4	89
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
hlavním	hlavní	k2eAgMnSc7d1	hlavní
eunuchem	eunuch	k1gMnSc7	eunuch
a	a	k8xC	a
ministrem	ministr	k1gMnSc7	ministr
orby	orba	k1gFnSc2	orba
pod	pod	k7c7	pod
císařem	císař	k1gMnSc7	císař
Che	che	k0	che
z	z	k7c2	z
dynastie	dynastie	k1gFnSc2	dynastie
Chan	Chan	k1gMnSc1	Chan
(	(	kIx(	(
<g/>
vládl	vládnout	k5eAaImAgMnS	vládnout
88	[number]	k4	88
<g/>
–	–	k?	–
<g/>
105	[number]	k4	105
<g/>
/	/	kIx~	/
<g/>
106	[number]	k4	106
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
105	[number]	k4	105
Cchaj	Cchaj	k1gInSc1	Cchaj
pojal	pojmout	k5eAaPmAgInS	pojmout
myšlenku	myšlenka	k1gFnSc4	myšlenka
vytvoření	vytvoření	k1gNnSc2	vytvoření
listů	list	k1gInPc2	list
papíru	papír	k1gInSc2	papír
z	z	k7c2	z
macerované	macerovaný	k2eAgFnSc2d1	macerovaná
kůry	kůra	k1gFnSc2	kůra
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
konopného	konopný	k2eAgInSc2d1	konopný
odpadu	odpad	k1gInSc2	odpad
<g/>
,	,	kIx,	,
starých	starý	k2eAgInPc2d1	starý
hadrů	hadr	k1gInPc2	hadr
a	a	k8xC	a
rybářských	rybářský	k2eAgFnPc2d1	rybářská
sítí	síť	k1gFnPc2	síť
<g/>
.	.	kIx.	.
</s>
<s>
Ukázalo	ukázat	k5eAaPmAgNnS	ukázat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
takto	takto	k6eAd1	takto
získaný	získaný	k2eAgInSc1d1	získaný
papír	papír	k1gInSc1	papír
se	se	k3xPyFc4	se
mnohem	mnohem	k6eAd1	mnohem
více	hodně	k6eAd2	hodně
hodil	hodit	k5eAaImAgMnS	hodit
na	na	k7c4	na
psaní	psaní	k1gNnSc4	psaní
než	než	k8xS	než
tkanina	tkanina	k1gFnSc1	tkanina
z	z	k7c2	z
čistého	čistý	k2eAgNnSc2d1	čisté
hedvábí	hedvábí	k1gNnSc2	hedvábí
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
účelu	účel	k1gInSc3	účel
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
užívala	užívat	k5eAaImAgFnS	užívat
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
výroba	výroba	k1gFnSc1	výroba
byla	být	k5eAaImAgFnS	být
méně	málo	k6eAd2	málo
nákladná	nákladný	k2eAgFnSc1d1	nákladná
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
byl	být	k5eAaImAgMnS	být
z	z	k7c2	z
objevu	objev	k1gInSc2	objev
nadšen	nadšen	k2eAgMnSc1d1	nadšen
a	a	k8xC	a
Cchaj	Cchaj	k1gInSc1	Cchaj
Lun	luna	k1gFnPc2	luna
za	za	k7c4	za
odměnu	odměna	k1gFnSc4	odměna
získal	získat	k5eAaPmAgMnS	získat
šlechtický	šlechtický	k2eAgInSc4d1	šlechtický
titul	titul	k1gInSc4	titul
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
však	však	k9	však
stal	stát	k5eAaPmAgMnS	stát
obětí	oběť	k1gFnSc7	oběť
palácových	palácový	k2eAgFnPc2d1	palácová
intrik	intrika	k1gFnPc2	intrika
a	a	k8xC	a
spáchal	spáchat	k5eAaPmAgInS	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
vypitím	vypití	k1gNnSc7	vypití
číše	číš	k1gFnSc2	číš
jedu	jed	k1gInSc2	jed
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
nicméně	nicméně	k8xC	nicméně
autoři	autor	k1gMnPc1	autor
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
upozorňují	upozorňovat	k5eAaImIp3nP	upozorňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
papír	papír	k1gInSc1	papír
patrně	patrně	k6eAd1	patrně
vynalezl	vynaleznout	k5eAaPmAgInS	vynaleznout
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
někdo	někdo	k3yInSc1	někdo
z	z	k7c2	z
poddaných	poddaný	k1gMnPc2	poddaný
a	a	k8xC	a
vynález	vynález	k1gInSc4	vynález
byl	být	k5eAaImAgMnS	být
Cchaj	Cchaj	k1gFnSc4	Cchaj
Lunovi	Luna	k1gMnSc3	Luna
jako	jako	k8xC	jako
vysokému	vysoký	k2eAgMnSc3d1	vysoký
úředníkovi	úředník	k1gMnSc3	úředník
jen	jen	k9	jen
připsán	připsat	k5eAaPmNgMnS	připsat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
obvyklé	obvyklý	k2eAgNnSc1d1	obvyklé
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Způsob	způsob	k1gInSc1	způsob
výroby	výroba	k1gFnSc2	výroba
papíru	papír	k1gInSc2	papír
prý	prý	k9	prý
zdokonalil	zdokonalit	k5eAaPmAgMnS	zdokonalit
Cchajův	Cchajův	k2eAgMnSc1d1	Cchajův
žák	žák	k1gMnSc1	žák
Cuo	Cuo	k1gFnSc2	Cuo
Po	Po	kA	Po
a	a	k8xC	a
papír	papír	k1gInSc1	papír
se	se	k3xPyFc4	se
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
době	doba	k1gFnSc6	doba
rychle	rychle	k6eAd1	rychle
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
Číně	Čína	k1gFnSc6	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
užíval	užívat	k5eAaImAgInS	užívat
již	již	k6eAd1	již
běžně	běžně	k6eAd1	běžně
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
pronikl	proniknout	k5eAaPmAgMnS	proniknout
do	do	k7c2	do
Koreje	Korea	k1gFnSc2	Korea
a	a	k8xC	a
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
ve	v	k7c6	v
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
Cchajovo	Cchajův	k2eAgNnSc1d1	Cchajův
jméno	jméno	k1gNnSc1	jméno
zde	zde	k6eAd1	zde
však	však	k9	však
zůstávalo	zůstávat	k5eAaImAgNnS	zůstávat
dlouho	dlouho	k6eAd1	dlouho
neznámé	známý	k2eNgNnSc1d1	neznámé
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Cchaj	Cchaj	k1gInSc1	Cchaj
Lun	luna	k1gFnPc2	luna
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Heslo	heslo	k1gNnSc1	heslo
v	v	k7c6	v
encyklopedii	encyklopedie	k1gFnSc6	encyklopedie
Britannica	Britannic	k1gInSc2	Britannic
</s>
</p>
<p>
<s>
Heslo	heslo	k1gNnSc4	heslo
na	na	k7c4	na
Biography	Biographa	k1gFnPc4	Biographa
Online	Onlin	k1gMnSc5	Onlin
</s>
</p>
<p>
<s>
Portrét	portrét	k1gInSc1	portrét
na	na	k7c4	na
China	China	k1gFnSc1	China
Culture	Cultur	k1gMnSc5	Cultur
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
</s>
</p>
<p>
<s>
Informace	informace	k1gFnPc1	informace
o	o	k7c6	o
vynálezu	vynález	k1gInSc6	vynález
papíru	papír	k1gInSc2	papír
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Kultura	kultura	k1gFnSc1	kultura
21	[number]	k4	21
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
