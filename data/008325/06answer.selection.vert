<s>
Jan	Jan	k1gMnSc1	Jan
či	či	k8xC	či
Johánek	Johánek	k1gMnSc1	Johánek
z	z	k7c2	z
Pomuka	Pomuk	k1gMnSc2	Pomuk
(	(	kIx(	(
<g/>
mezi	mezi	k7c7	mezi
1340	[number]	k4	1340
a	a	k8xC	a
1350	[number]	k4	1350
v	v	k7c6	v
Pomuku	Pomuk	k1gInSc6	Pomuk
<g/>
,	,	kIx,	,
dnešním	dnešní	k2eAgInSc6d1	dnešní
Nepomuku	Nepomuk	k1gInSc6	Nepomuk
–	–	k?	–
20	[number]	k4	20
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1393	[number]	k4	1393
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
generální	generální	k2eAgMnSc1d1	generální
vikář	vikář	k1gMnSc1	vikář
pražského	pražský	k2eAgMnSc2d1	pražský
arcibiskupa	arcibiskup	k1gMnSc2	arcibiskup
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
uctívaný	uctívaný	k2eAgMnSc1d1	uctívaný
jako	jako	k8xC	jako
svatý	svatý	k2eAgMnSc1d1	svatý
Jan	Jan	k1gMnSc1	Jan
Nepomucký	Nepomucký	k1gMnSc1	Nepomucký
<g/>
,	,	kIx,	,
mučedník	mučedník	k1gMnSc1	mučedník
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
českých	český	k2eAgMnPc2d1	český
zemských	zemský	k2eAgMnPc2d1	zemský
patronů	patron	k1gMnPc2	patron
<g/>
.	.	kIx.	.
</s>
