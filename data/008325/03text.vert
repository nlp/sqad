<p>
<s>
Jan	Jan	k1gMnSc1	Jan
či	či	k8xC	či
Johánek	Johánek	k1gMnSc1	Johánek
z	z	k7c2	z
Pomuka	Pomuk	k1gMnSc2	Pomuk
(	(	kIx(	(
<g/>
mezi	mezi	k7c7	mezi
1340	[number]	k4	1340
a	a	k8xC	a
1350	[number]	k4	1350
v	v	k7c6	v
Pomuku	Pomuk	k1gInSc6	Pomuk
<g/>
,	,	kIx,	,
dnešním	dnešní	k2eAgInSc6d1	dnešní
Nepomuku	Nepomuk	k1gInSc6	Nepomuk
–	–	k?	–
20	[number]	k4	20
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1393	[number]	k4	1393
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
generální	generální	k2eAgMnSc1d1	generální
vikář	vikář	k1gMnSc1	vikář
pražského	pražský	k2eAgMnSc2d1	pražský
arcibiskupa	arcibiskup	k1gMnSc2	arcibiskup
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
uctívaný	uctívaný	k2eAgMnSc1d1	uctívaný
jako	jako	k8xS	jako
svatý	svatý	k2eAgMnSc1d1	svatý
Jan	Jan	k1gMnSc1	Jan
Nepomucký	Nepomucký	k1gMnSc1	Nepomucký
<g/>
,	,	kIx,	,
mučedník	mučedník	k1gMnSc1	mučedník
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
českých	český	k2eAgMnPc2d1	český
zemských	zemský	k2eAgMnPc2d1	zemský
patronů	patron	k1gMnPc2	patron
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Mládí	mládí	k1gNnSc1	mládí
===	===	k?	===
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
z	z	k7c2	z
Pomuku	Pomuk	k1gInSc2	Pomuk
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
někdy	někdy	k6eAd1	někdy
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1340	[number]	k4	1340
v	v	k7c6	v
Pomuku	Pomuk	k1gInSc6	Pomuk
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
náležel	náležet	k5eAaImAgInS	náležet
nedalekému	daleký	k2eNgInSc3d1	nedaleký
cisterciáckému	cisterciácký	k2eAgInSc3d1	cisterciácký
klášteru	klášter	k1gInSc3	klášter
pod	pod	k7c7	pod
Zelenou	zelený	k2eAgFnSc7d1	zelená
Horou	hora	k1gFnSc7	hora
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dnes	dnes	k6eAd1	dnes
stojí	stát	k5eAaImIp3nS	stát
barokní	barokní	k2eAgInSc1d1	barokní
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
<g/>
,	,	kIx,	,
stál	stát	k5eAaImAgMnS	stát
podle	podle	k7c2	podle
ústního	ústní	k2eAgNnSc2d1	ústní
podání	podání	k1gNnSc2	podání
domek	domek	k1gInSc1	domek
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
Jan	Jan	k1gMnSc1	Jan
narodil	narodit	k5eAaPmAgMnS	narodit
<g/>
.	.	kIx.	.
</s>
<s>
Janův	Janův	k2eAgMnSc1d1	Janův
otec	otec	k1gMnSc1	otec
Velfín	Velfín	k1gMnSc1	Velfín
(	(	kIx(	(
<g/>
či	či	k8xC	či
Welfin	Welfin	k1gMnSc1	Welfin
<g/>
,	,	kIx,	,
Iohannes	Iohannes	k1gMnSc1	Iohannes
natus	natus	k1gMnSc1	natus
quondam	quondam	k1gInSc4	quondam
Welflini	Welflin	k2eAgMnPc1d1	Welflin
de	de	k?	de
Pomuk	Pomuk	k1gInSc1	Pomuk
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Pomuku	Pomuk	k1gInSc6	Pomuk
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1355	[number]	k4	1355
až	až	k9	až
1367	[number]	k4	1367
rychtářem	rychtář	k1gMnSc7	rychtář
<g/>
,	,	kIx,	,
o	o	k7c6	o
matce	matka	k1gFnSc6	matka
není	být	k5eNaImIp3nS	být
nic	nic	k3yNnSc1	nic
známo	znám	k2eAgNnSc1d1	známo
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgNnSc1d1	základní
vzdělání	vzdělání	k1gNnSc4	vzdělání
získal	získat	k5eAaPmAgInS	získat
zřejmě	zřejmě	k6eAd1	zřejmě
ve	v	k7c6	v
škole	škola	k1gFnSc6	škola
zřízené	zřízený	k2eAgFnPc1d1	zřízená
roku	rok	k1gInSc2	rok
1344	[number]	k4	1344
při	při	k7c6	při
farním	farní	k2eAgInSc6d1	farní
kostele	kostel	k1gInSc6	kostel
sv.	sv.	kA	sv.
Jakuba	Jakub	k1gMnSc2	Jakub
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnPc1d1	další
studia	studio	k1gNnPc1	studio
nejsou	být	k5eNaImIp3nP	být
s	s	k7c7	s
jistotou	jistota	k1gFnSc7	jistota
doložena	doložit	k5eAaPmNgFnS	doložit
<g/>
,	,	kIx,	,
barokní	barokní	k2eAgFnSc2d1	barokní
legendy	legenda	k1gFnSc2	legenda
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
studoval	studovat	k5eAaImAgMnS	studovat
u	u	k7c2	u
cisterciáků	cisterciák	k1gMnPc2	cisterciák
v	v	k7c6	v
blízkém	blízký	k2eAgInSc6d1	blízký
klášteře	klášter	k1gInSc6	klášter
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c6	na
pražské	pražský	k2eAgFnSc6d1	Pražská
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Studia	studio	k1gNnSc2	studio
===	===	k?	===
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1369	[number]	k4	1369
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
veřejný	veřejný	k2eAgMnSc1d1	veřejný
notář	notář	k1gMnSc1	notář
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
působil	působit	k5eAaImAgMnS	působit
jako	jako	k8xS	jako
hlavní	hlavní	k2eAgMnSc1d1	hlavní
písař	písař	k1gMnSc1	písař
u	u	k7c2	u
právě	právě	k6eAd1	právě
vytvořených	vytvořený	k2eAgNnPc2d1	vytvořené
soudních	soudní	k2eAgNnPc2d1	soudní
akt	akta	k1gNnPc2	akta
generálních	generální	k2eAgMnPc2d1	generální
vikářů	vikář	k1gMnPc2	vikář
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1373	[number]	k4	1373
<g/>
–	–	k?	–
<g/>
1383	[number]	k4	1383
požíval	požívat	k5eAaImAgInS	požívat
titulů	titul	k1gInPc2	titul
notarius	notarius	k1gMnSc1	notarius
<g/>
,	,	kIx,	,
prothonotarius	prothonotarius	k1gMnSc1	prothonotarius
cancellarie	cancellarie	k1gFnSc2	cancellarie
a	a	k8xC	a
domesticus	domesticus	k1gInSc1	domesticus
commensalis	commensalis	k1gFnSc2	commensalis
arcibiskupa	arcibiskup	k1gMnSc2	arcibiskup
Jana	Jan	k1gMnSc2	Jan
Očka	očko	k1gNnSc2	očko
z	z	k7c2	z
Vlašimi	Vlašim	k1gFnSc2	Vlašim
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1380	[number]	k4	1380
byl	být	k5eAaImAgInS	být
oltářníkem	oltářník	k1gMnSc7	oltářník
u	u	k7c2	u
sv.	sv.	kA	sv.
Erharda	Erharda	k1gFnSc1	Erharda
a	a	k8xC	a
sv.	sv.	kA	sv.
Otilie	Otilie	k1gFnSc2	Otilie
ve	v	k7c6	v
svatovítském	svatovítský	k2eAgInSc6d1	svatovítský
kostele	kostel	k1gInSc6	kostel
a	a	k8xC	a
farář	farář	k1gMnSc1	farář
u	u	k7c2	u
sv.	sv.	kA	sv.
Havla	Havel	k1gMnSc2	Havel
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1383	[number]	k4	1383
a	a	k8xC	a
1387	[number]	k4	1387
studoval	studovat	k5eAaImAgInS	studovat
církevní	církevní	k2eAgNnSc4d1	církevní
právo	právo	k1gNnSc4	právo
na	na	k7c6	na
Padovské	padovský	k2eAgFnSc6d1	Padovská
univerzitě	univerzita	k1gFnSc6	univerzita
a	a	k8xC	a
studium	studium	k1gNnSc4	studium
ukončil	ukončit	k5eAaPmAgMnS	ukončit
jako	jako	k9	jako
doktor	doktor	k1gMnSc1	doktor
dekretů	dekret	k1gInPc2	dekret
<g/>
.	.	kIx.	.
</s>
<s>
Vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1387	[number]	k4	1387
a	a	k8xC	a
vyměnil	vyměnit	k5eAaPmAgMnS	vyměnit
svoje	svůj	k3xOyFgNnPc4	svůj
oltářnictví	oltářnictví	k1gNnPc4	oltářnictví
za	za	k7c4	za
místo	místo	k1gNnSc4	místo
kanovníka	kanovník	k1gMnSc2	kanovník
u	u	k7c2	u
sv.	sv.	kA	sv.
Jiljí	Jiljí	k1gMnSc2	Jiljí
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
kanovníkem	kanovník	k1gMnSc7	kanovník
a	a	k8xC	a
advokátem	advokát	k1gMnSc7	advokát
vyšehradské	vyšehradský	k2eAgFnSc2d1	Vyšehradská
kapituly	kapitula	k1gFnSc2	kapitula
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Generální	generální	k2eAgMnSc1d1	generální
vikář	vikář	k1gMnSc1	vikář
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
1389	[number]	k4	1389
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
generálním	generální	k2eAgMnSc7d1	generální
vikářem	vikář	k1gMnSc7	vikář
pražského	pražský	k2eAgMnSc2d1	pražský
arcibiskupa	arcibiskup	k1gMnSc2	arcibiskup
po	po	k7c6	po
Kuneši	Kuneš	k1gMnSc6	Kuneš
z	z	k7c2	z
Třebovle	Třebovle	k1gNnSc2	Třebovle
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
Janova	Janův	k2eAgInSc2d1	Janův
vikariátu	vikariát	k1gInSc2	vikariát
měl	mít	k5eAaImAgMnS	mít
pražský	pražský	k2eAgMnSc1d1	pražský
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
pouze	pouze	k6eAd1	pouze
dva	dva	k4xCgInPc1	dva
generální	generální	k2eAgInPc1d1	generální
vikáře	vikář	k1gMnSc2	vikář
(	(	kIx(	(
<g/>
oba	dva	k4xCgMnPc1	dva
in	in	k?	in
spiritualibus	spiritualibus	k1gInSc4	spiritualibus
<g/>
,	,	kIx,	,
ve	v	k7c6	v
věcech	věc	k1gFnPc6	věc
duchovní	duchovní	k2eAgFnSc2d1	duchovní
správy	správa	k1gFnSc2	správa
<g/>
;	;	kIx,	;
zástupce	zástupce	k1gMnSc1	zástupce
in	in	k?	in
temporalibus	temporalibus	k1gMnSc1	temporalibus
se	se	k3xPyFc4	se
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
vlastně	vlastně	k9	vlastně
ani	ani	k9	ani
nevyvinul	vyvinout	k5eNaPmAgMnS	vyvinout
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgInSc7	druhý
byl	být	k5eAaImAgMnS	být
M.	M.	kA	M.
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
Puchník	Puchník	k1gMnSc1	Puchník
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
také	také	k9	také
zastával	zastávat	k5eAaImAgMnS	zastávat
úřad	úřad	k1gInSc4	úřad
oficiála	oficiál	k1gMnSc2	oficiál
<g/>
.	.	kIx.	.
</s>
<s>
Následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
vyměnil	vyměnit	k5eAaPmAgMnS	vyměnit
Jan	Jan	k1gMnSc1	Jan
svou	svůj	k3xOyFgFnSc4	svůj
farnost	farnost	k1gFnSc4	farnost
u	u	k7c2	u
sv.	sv.	kA	sv.
Havla	Havla	k1gFnSc1	Havla
za	za	k7c4	za
úřad	úřad	k1gInSc4	úřad
žateckého	žatecký	k2eAgMnSc2d1	žatecký
arcijáhna	arcijáhen	k1gMnSc2	arcijáhen
<g/>
.	.	kIx.	.
<g/>
Jan	Jan	k1gMnSc1	Jan
zastával	zastávat	k5eAaImAgMnS	zastávat
úřad	úřad	k1gInSc4	úřad
generálního	generální	k2eAgMnSc2d1	generální
vikáře	vikář	k1gMnSc2	vikář
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
již	již	k6eAd1	již
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
díky	díky	k7c3	díky
svým	svůj	k3xOyFgFnPc3	svůj
pravomocím	pravomoc	k1gFnPc3	pravomoc
hrál	hrát	k5eAaImAgMnS	hrát
podstatnou	podstatný	k2eAgFnSc4d1	podstatná
roli	role	k1gFnSc4	role
ve	v	k7c6	v
správě	správa	k1gFnSc6	správa
arcibiskupství	arcibiskupství	k1gNnSc2	arcibiskupství
<g/>
,	,	kIx,	,
v	v	k7c6	v
neklidné	klidný	k2eNgFnSc6d1	neklidná
době	doba	k1gFnSc6	doba
sporu	spor	k1gInSc2	spor
mezi	mezi	k7c7	mezi
arcibiskupem	arcibiskup	k1gMnSc7	arcibiskup
a	a	k8xC	a
králem	král	k1gMnSc7	král
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
velkému	velký	k2eAgNnSc3d1	velké
západnímu	západní	k2eAgNnSc3d1	západní
schizmatu	schizma	k1gNnSc3	schizma
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
nepřátelství	nepřátelství	k1gNnSc1	nepřátelství
mezi	mezi	k7c7	mezi
českým	český	k2eAgMnSc7d1	český
králem	král	k1gMnSc7	král
Václavem	Václav	k1gMnSc7	Václav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
a	a	k8xC	a
arcibiskupem	arcibiskup	k1gMnSc7	arcibiskup
Jenem	Jan	k1gMnSc7	Jan
z	z	k7c2	z
Jenštejna	Jenštejn	k1gInSc2	Jenštejn
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
spor	spor	k1gInSc1	spor
byl	být	k5eAaImAgInS	být
také	také	k9	také
sporem	spor	k1gInSc7	spor
o	o	k7c4	o
jmenování	jmenování	k1gNnSc4	jmenování
biskupů	biskup	k1gMnPc2	biskup
a	a	k8xC	a
vůbec	vůbec	k9	vůbec
vysokých	vysoký	k2eAgInPc2d1	vysoký
církevních	církevní	k2eAgInPc2d1	církevní
úřadů	úřad	k1gInPc2	úřad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Spor	spor	k1gInSc1	spor
o	o	k7c4	o
Kladruby	Kladruby	k1gInPc4	Kladruby
===	===	k?	===
</s>
</p>
<p>
<s>
K	k	k7c3	k
zásadnímu	zásadní	k2eAgInSc3d1	zásadní
vývoji	vývoj	k1gInSc3	vývoj
ve	v	k7c6	v
sporech	spor	k1gInPc6	spor
o	o	k7c6	o
obsazování	obsazování	k1gNnSc6	obsazování
úřadů	úřad	k1gInPc2	úřad
došlo	dojít	k5eAaPmAgNnS	dojít
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Racka	racek	k1gMnSc2	racek
<g/>
,	,	kIx,	,
opata	opat	k1gMnSc2	opat
kláštera	klášter	k1gInSc2	klášter
v	v	k7c6	v
Kladrubech	Kladruby	k1gInPc6	Kladruby
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
totiž	totiž	k9	totiž
mínil	mínit	k5eAaImAgMnS	mínit
vytvořit	vytvořit	k5eAaPmF	vytvořit
z	z	k7c2	z
kláštera	klášter	k1gInSc2	klášter
nové	nový	k2eAgNnSc1d1	nové
biskupství	biskupství	k1gNnSc1	biskupství
obsazené	obsazený	k2eAgNnSc1d1	obsazené
sobě	se	k3xPyFc3	se
věrným	věrný	k2eAgMnSc7d1	věrný
biskupem	biskup	k1gMnSc7	biskup
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
zmenšit	zmenšit	k5eAaPmF	zmenšit
arcibiskupovu	arcibiskupův	k2eAgFnSc4d1	arcibiskupova
moc	moc	k1gFnSc4	moc
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
však	však	k9	však
10	[number]	k4	10
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1393	[number]	k4	1393
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
nového	nový	k2eAgMnSc4d1	nový
opata	opat	k1gMnSc4	opat
<g/>
,	,	kIx,	,
zástupci	zástupce	k1gMnPc1	zástupce
kláštera	klášter	k1gInSc2	klášter
navrženého	navržený	k2eAgNnSc2d1	navržené
Olena	Oleno	k1gNnSc2	Oleno
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
tehdy	tehdy	k6eAd1	tehdy
pobýval	pobývat	k5eAaImAgMnS	pobývat
na	na	k7c6	na
Křivoklátě	Křivoklát	k1gInSc6	Křivoklát
a	a	k8xC	a
nestihl	stihnout	k5eNaPmAgMnS	stihnout
proti	proti	k7c3	proti
volbě	volba	k1gFnSc3	volba
včas	včas	k6eAd1	včas
podat	podat	k5eAaPmF	podat
námitky	námitka	k1gFnPc4	námitka
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
delikátní	delikátní	k2eAgFnSc4d1	delikátní
věc	věc	k1gFnSc4	věc
a	a	k8xC	a
opatrná	opatrný	k2eAgFnSc1d1	opatrná
formulace	formulace	k1gFnSc1	formulace
potvrzovací	potvrzovací	k2eAgFnSc2d1	potvrzovací
listiny	listina	k1gFnSc2	listina
dokazuje	dokazovat	k5eAaImIp3nS	dokazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
generální	generální	k2eAgMnSc1d1	generální
vikář	vikář	k1gMnSc1	vikář
Jan	Jan	k1gMnSc1	Jan
o	o	k7c6	o
tom	ten	k3xDgInSc6	ten
věděl	vědět	k5eAaImAgMnS	vědět
<g/>
.	.	kIx.	.
</s>
<s>
Spor	spor	k1gInSc1	spor
o	o	k7c4	o
Kladruby	Kladruby	k1gInPc4	Kladruby
a	a	k8xC	a
potvrzení	potvrzení	k1gNnSc4	potvrzení
listiny	listina	k1gFnSc2	listina
bylo	být	k5eAaImAgNnS	být
nejspíše	nejspíše	k9	nejspíše
hlavním	hlavní	k2eAgInSc7d1	hlavní
důvodem	důvod	k1gInSc7	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
byl	být	k5eAaImAgMnS	být
Jan	Jan	k1gMnSc1	Jan
zajat	zajat	k2eAgMnSc1d1	zajat
a	a	k8xC	a
mučen	mučen	k2eAgMnSc1d1	mučen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Historicky	historicky	k6eAd1	historicky
nedoložená	doložený	k2eNgFnSc1d1	nedoložená
a	a	k8xC	a
nepravděpodobná	pravděpodobný	k2eNgFnSc1d1	nepravděpodobná
je	být	k5eAaImIp3nS	být
role	role	k1gFnSc1	role
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
jako	jako	k8xS	jako
důvěrníka	důvěrník	k1gMnSc2	důvěrník
či	či	k8xC	či
zpovědníka	zpovědník	k1gMnSc2	zpovědník
královny	královna	k1gFnSc2	královna
Žofie	Žofie	k1gFnSc2	Žofie
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
nechce	chtít	k5eNaImIp3nS	chtít
prozradit	prozradit	k5eAaPmF	prozradit
zpovědní	zpovědní	k2eAgNnSc4d1	zpovědní
tajemství	tajemství	k1gNnSc4	tajemství
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
známá	známá	k1gFnSc1	známá
např.	např.	kA	např.
z	z	k7c2	z
Hájkovy	Hájkův	k2eAgFnSc2d1	Hájkova
kroniky	kronika	k1gFnSc2	kronika
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
důvěrná	důvěrný	k2eAgFnSc1d1	důvěrná
dvorská	dvorský	k2eAgFnSc1d1	dvorská
funkce	funkce	k1gFnSc1	funkce
je	být	k5eAaImIp3nS	být
nepravděpodobná	pravděpodobný	k2eNgFnSc1d1	nepravděpodobná
již	již	k6eAd1	již
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
Nepomukovy	Nepomukův	k2eAgFnSc2d1	Nepomukova
příslušnosti	příslušnost	k1gFnSc2	příslušnost
ke	k	k7c3	k
královi	král	k1gMnSc3	král
znepřátelené	znepřátelený	k2eAgFnSc2d1	znepřátelená
skupině	skupina	k1gFnSc3	skupina
okolo	okolo	k7c2	okolo
arcibiskupa	arcibiskup	k1gMnSc2	arcibiskup
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Mučení	mučení	k1gNnPc4	mučení
a	a	k8xC	a
smrt	smrt	k1gFnSc4	smrt
===	===	k?	===
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
20	[number]	k4	20
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1393	[number]	k4	1393
se	se	k3xPyFc4	se
brzo	brzo	k6eAd1	brzo
ráno	ráno	k6eAd1	ráno
sešel	sejít	k5eAaPmAgMnS	sejít
král	král	k1gMnSc1	král
Václav	Václav	k1gMnSc1	Václav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
s	s	k7c7	s
Janem	Jan	k1gMnSc7	Jan
z	z	k7c2	z
Jenštejna	Jenštejn	k1gInSc2	Jenštejn
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
krátké	krátký	k2eAgFnSc6d1	krátká
hádce	hádka	k1gFnSc6	hádka
nechal	nechat	k5eAaPmAgMnS	nechat
král	král	k1gMnSc1	král
zajmout	zajmout	k5eAaPmF	zajmout
čtyři	čtyři	k4xCgMnPc4	čtyři
muže	muž	k1gMnPc4	muž
z	z	k7c2	z
arcibiskupova	arcibiskupův	k2eAgInSc2d1	arcibiskupův
doprovodu	doprovod	k1gInSc2	doprovod
<g/>
,	,	kIx,	,
Jana	Jana	k1gFnSc1	Jana
z	z	k7c2	z
Pomuka	Pomuk	k1gMnSc2	Pomuk
<g/>
,	,	kIx,	,
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
Puchníka	Puchník	k1gMnSc2	Puchník
<g/>
,	,	kIx,	,
míšeňského	míšeňský	k2eAgMnSc4d1	míšeňský
probošta	probošt	k1gMnSc4	probošt
Václava	Václav	k1gMnSc2	Václav
Knoblocha	Knobloch	k1gMnSc2	Knobloch
a	a	k8xC	a
laika	laik	k1gMnSc2	laik
arcibiskupského	arcibiskupský	k2eAgMnSc2d1	arcibiskupský
hofmistra	hofmistr	k1gMnSc2	hofmistr
Něpra	Něpr	k1gMnSc2	Něpr
z	z	k7c2	z
Roupova	roupův	k2eAgNnSc2d1	roupův
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
byli	být	k5eAaImAgMnP	být
vyslýcháni	vyslýchán	k2eAgMnPc1d1	vyslýchán
na	na	k7c6	na
Pražském	pražský	k2eAgInSc6d1	pražský
hradě	hrad	k1gInSc6	hrad
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
na	na	k7c6	na
Staroměstské	staroměstský	k2eAgFnSc6d1	Staroměstská
radnici	radnice	k1gFnSc6	radnice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
posléze	posléze	k6eAd1	posléze
držen	držen	k2eAgInSc1d1	držen
Něpr	Něpr	k1gInSc1	Něpr
a	a	k8xC	a
ostatní	ostatní	k2eAgMnPc1d1	ostatní
byli	být	k5eAaImAgMnP	být
přesunuti	přesunout	k5eAaPmNgMnP	přesunout
do	do	k7c2	do
staroměstské	staroměstský	k2eAgFnSc2d1	Staroměstská
rychty	rychta	k1gFnSc2	rychta
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
dům	dům	k1gInSc1	dům
č.	č.	k?	č.
12	[number]	k4	12
na	na	k7c6	na
rohu	roh	k1gInSc6	roh
ulic	ulice	k1gFnPc2	ulice
Rytířské	rytířský	k2eAgFnSc2d1	rytířská
a	a	k8xC	a
Na	na	k7c6	na
Můstku	můstek	k1gInSc6	můstek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byli	být	k5eAaImAgMnP	být
mučeni	mučit	k5eAaImNgMnP	mučit
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
zemřel	zemřít	k5eAaPmAgMnS	zemřít
během	během	k7c2	během
tohoto	tento	k3xDgNnSc2	tento
mučení	mučení	k1gNnSc2	mučení
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgMnPc1d1	ostatní
byli	být	k5eAaImAgMnP	být
propuštěni	propustit	k5eAaPmNgMnP	propustit
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
deváté	devátý	k4xOgFnSc2	devátý
večer	večer	k6eAd1	večer
pak	pak	k6eAd1	pak
bylo	být	k5eAaImAgNnS	být
jeho	jeho	k3xOp3gNnSc1	jeho
tělo	tělo	k1gNnSc1	tělo
svrženo	svrhnout	k5eAaPmNgNnS	svrhnout
z	z	k7c2	z
dnešního	dnešní	k2eAgInSc2d1	dnešní
Karlova	Karlův	k2eAgInSc2d1	Karlův
mostu	most	k1gInSc2	most
do	do	k7c2	do
Vltavy	Vltava	k1gFnSc2	Vltava
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
do	do	k7c2	do
průzkumů	průzkum	k1gInPc2	průzkum
tělesných	tělesný	k2eAgInPc2d1	tělesný
pozůstatků	pozůstatek	k1gInPc2	pozůstatek
antropologem	antropolog	k1gMnSc7	antropolog
Emanuelem	Emanuel	k1gMnSc7	Emanuel
Vlčkem	Vlček	k1gMnSc7	Vlček
se	se	k3xPyFc4	se
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Jan	Jan	k1gMnSc1	Jan
zemřel	zemřít	k5eAaPmAgMnS	zemřít
utonutím	utonutí	k1gNnSc7	utonutí
<g/>
,	,	kIx,	,
Vlček	vlček	k1gInSc1	vlček
objevil	objevit	k5eAaPmAgInS	objevit
poranění	poranění	k1gNnSc4	poranění
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
při	při	k7c6	při
mučení	mučení	k1gNnSc6	mučení
a	a	k8xC	a
vedla	vést	k5eAaImAgFnS	vést
ke	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
ještě	ještě	k9	ještě
před	před	k7c7	před
svržením	svržení	k1gNnSc7	svržení
do	do	k7c2	do
řeky	řeka	k1gFnSc2	řeka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Pohřbení	pohřbení	k1gNnSc2	pohřbení
===	===	k?	===
</s>
</p>
<p>
<s>
17	[number]	k4	17
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
našli	najít	k5eAaPmAgMnP	najít
Janovo	Janův	k2eAgNnSc4d1	Janovo
tělo	tělo	k1gNnSc4	tělo
zachycené	zachycený	k2eAgNnSc4d1	zachycené
na	na	k7c6	na
pravém	pravý	k2eAgInSc6d1	pravý
břehu	břeh	k1gInSc6	břeh
Vltavy	Vltava	k1gFnSc2	Vltava
<g/>
,	,	kIx,	,
nedaleko	nedaleko	k7c2	nedaleko
svého	svůj	k3xOyFgInSc2	svůj
kláštera	klášter	k1gInSc2	klášter
(	(	kIx(	(
<g/>
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dnes	dnes	k6eAd1	dnes
stojí	stát	k5eAaImIp3nS	stát
Základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
a	a	k8xC	a
Gymnázium	gymnázium	k1gNnSc1	gymnázium
Dušní	dušný	k2eAgMnPc1d1	dušný
ulice	ulice	k1gFnPc1	ulice
<g/>
)	)	kIx)	)
rybáři	rybář	k1gMnPc1	rybář
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
nich	on	k3xPp3gMnPc2	on
jeho	jeho	k3xOp3gNnSc4	jeho
tělo	tělo	k1gNnSc4	tělo
převzali	převzít	k5eAaPmAgMnP	převzít
bratři	bratr	k1gMnPc1	bratr
Křižovníci	křižovník	k1gMnPc1	křižovník
s	s	k7c7	s
červeným	červený	k2eAgNnSc7d1	červené
srdcem	srdce	k1gNnSc7	srdce
–	–	k?	–
cyriaci	cyriace	k1gFnSc4	cyriace
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc4	jejichž
novicové	novic	k1gMnPc1	novic
jej	on	k3xPp3gMnSc4	on
uctivě	uctivě	k6eAd1	uctivě
vyzvedli	vyzvednout	k5eAaPmAgMnP	vyzvednout
a	a	k8xC	a
s	s	k7c7	s
úctou	úcta	k1gFnSc7	úcta
odpovídající	odpovídající	k2eAgFnSc4d1	odpovídající
jeho	jeho	k3xOp3gFnSc3	jeho
hodnosti	hodnost	k1gFnSc3	hodnost
jej	on	k3xPp3gMnSc4	on
pochovali	pochovat	k5eAaPmAgMnP	pochovat
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
sv.	sv.	kA	sv.
Kříže	kříž	k1gInSc2	kříž
většího	veliký	k2eAgInSc2d2	veliký
při	při	k7c6	při
svém	svůj	k3xOyFgInSc6	svůj
klášteře	klášter	k1gInSc6	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Generálním	generální	k2eAgMnSc7d1	generální
převorem	převor	k1gMnSc7	převor
byl	být	k5eAaImAgMnS	být
tehdy	tehdy	k6eAd1	tehdy
bratr	bratr	k1gMnSc1	bratr
Jan	Jan	k1gMnSc1	Jan
(	(	kIx(	(
<g/>
1373	[number]	k4	1373
<g/>
–	–	k?	–
<g/>
1403	[number]	k4	1403
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgMnSc1	třetí
generální	generální	k2eAgMnSc1d1	generální
převor	převor	k1gMnSc1	převor
řádu	řád	k1gInSc2	řád
<g/>
,	,	kIx,	,
od	od	k7c2	od
přenesení	přenesení	k1gNnSc2	přenesení
sídla	sídlo	k1gNnSc2	sídlo
řádu	řád	k1gInSc2	řád
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Legenda	legenda	k1gFnSc1	legenda
==	==	k?	==
</s>
</p>
<p>
<s>
Osudy	osud	k1gInPc1	osud
Jana	Jan	k1gMnSc2	Jan
z	z	k7c2	z
Pomuku	Pomuk	k1gInSc2	Pomuk
nekončí	končit	k5eNaImIp3nS	končit
jeho	jeho	k3xOp3gFnSc7	jeho
smrtí	smrt	k1gFnSc7	smrt
–	–	k?	–
legenda	legenda	k1gFnSc1	legenda
o	o	k7c6	o
jeho	jeho	k3xOp3gInSc6	jeho
životě	život	k1gInSc6	život
a	a	k8xC	a
především	především	k6eAd1	především
smrti	smrt	k1gFnSc3	smrt
prochází	procházet	k5eAaImIp3nS	procházet
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
dalších	další	k2eAgFnPc2d1	další
českých	český	k2eAgFnPc2d1	Česká
dějin	dějiny	k1gFnPc2	dějiny
velmi	velmi	k6eAd1	velmi
zajímavými	zajímavý	k2eAgFnPc7d1	zajímavá
a	a	k8xC	a
vrtkavými	vrtkavý	k2eAgFnPc7d1	vrtkavá
peripetiemi	peripetie	k1gFnPc7	peripetie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Raní	raný	k2eAgMnPc1d1	raný
životopisci	životopisec	k1gMnPc1	životopisec
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
Janově	Janův	k2eAgFnSc6d1	Janova
smrti	smrt	k1gFnSc6	smrt
podává	podávat	k5eAaImIp3nS	podávat
Jan	Jan	k1gMnSc1	Jan
z	z	k7c2	z
Jenštejna	Jenštejn	k1gMnSc2	Jenštejn
k	k	k7c3	k
římské	římský	k2eAgFnSc3d1	římská
kurii	kurie	k1gFnSc3	kurie
stížné	stížný	k2eAgFnSc2d1	stížný
podání	podání	k1gNnSc4	podání
na	na	k7c4	na
Václava	Václav	k1gMnSc4	Václav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgNnSc6	jenž
již	již	k6eAd1	již
Jana	Jana	k1gFnSc1	Jana
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k8xC	jako
mučedníka	mučedník	k1gMnSc2	mučedník
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
(	(	kIx(	(
<g/>
1401	[number]	k4	1401
<g/>
)	)	kIx)	)
životopisec	životopisec	k1gMnSc1	životopisec
Jana	Jan	k1gMnSc2	Jan
z	z	k7c2	z
Jenštejna	Jenštejn	k1gInSc2	Jenštejn
uvádí	uvádět	k5eAaImIp3nS	uvádět
zázračné	zázračný	k2eAgFnPc4d1	zázračná
okolnosti	okolnost	k1gFnPc4	okolnost
nalezení	nalezení	k1gNnSc2	nalezení
jeho	on	k3xPp3gNnSc2	on
těla	tělo	k1gNnSc2	tělo
a	a	k8xC	a
opět	opět	k6eAd1	opět
jej	on	k3xPp3gMnSc4	on
označuje	označovat	k5eAaImIp3nS	označovat
za	za	k7c2	za
mučedníka	mučedník	k1gMnSc2	mučedník
<g/>
.	.	kIx.	.
</s>
<s>
Augustiniánský	augustiniánský	k2eAgMnSc1d1	augustiniánský
opat	opat	k1gMnSc1	opat
Ludolf	Ludolf	k1gMnSc1	Ludolf
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1398	[number]	k4	1398
označuje	označovat	k5eAaImIp3nS	označovat
Jana	Jana	k1gFnSc1	Jana
z	z	k7c2	z
Pomuku	Pomuk	k1gInSc2	Pomuk
za	za	k7c4	za
"	"	kIx"	"
<g/>
ctihodného	ctihodný	k2eAgMnSc4d1	ctihodný
onoho	onen	k3xDgMnSc2	onen
muže	muž	k1gMnSc2	muž
<g/>
,	,	kIx,	,
milého	milý	k2eAgMnSc4d1	milý
Bohu	bůh	k1gMnSc3	bůh
i	i	k8xC	i
lidem	člověk	k1gMnPc3	člověk
<g/>
,	,	kIx,	,
Čechům	Čech	k1gMnPc3	Čech
i	i	k8xC	i
Němcům	Němec	k1gMnPc3	Němec
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1416	[number]	k4	1416
bylo	být	k5eAaImAgNnS	být
tělo	tělo	k1gNnSc1	tělo
bývalého	bývalý	k2eAgMnSc2d1	bývalý
generálního	generální	k2eAgMnSc2d1	generální
vikáře	vikář	k1gMnSc2	vikář
pohřbeno	pohřbít	k5eAaPmNgNnS	pohřbít
v	v	k7c6	v
katedrále	katedrála	k1gFnSc6	katedrála
sv.	sv.	kA	sv.
Víta	Vít	k1gMnSc2	Vít
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1449	[number]	k4	1449
zveřejňuje	zveřejňovat	k5eAaImIp3nS	zveřejňovat
Tomáš	Tomáš	k1gMnSc1	Tomáš
Ebendorfer	Ebendorfer	k1gMnSc1	Ebendorfer
z	z	k7c2	z
Haselbachu	Haselbach	k1gInSc2	Haselbach
domněnku	domněnka	k1gFnSc4	domněnka
<g/>
,	,	kIx,	,
že	že	k8xS	že
Jan	Jan	k1gMnSc1	Jan
byl	být	k5eAaImAgMnS	být
zabit	zabít	k5eAaPmNgMnS	zabít
kvůli	kvůli	k7c3	kvůli
zpovědnímu	zpovědní	k2eAgNnSc3d1	zpovědní
tajemství	tajemství	k1gNnSc3	tajemství
<g/>
.	.	kIx.	.
</s>
<s>
Mistr	mistr	k1gMnSc1	mistr
Pavel	Pavel	k1gMnSc1	Pavel
Žídek	Žídek	k1gMnSc1	Žídek
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
Spravovně	Spravovna	k1gFnSc6	Spravovna
uvádí	uvádět	k5eAaImIp3nS	uvádět
roku	rok	k1gInSc2	rok
1471	[number]	k4	1471
příběh	příběh	k1gInSc4	příběh
o	o	k7c6	o
doktoru	doktor	k1gMnSc6	doktor
Johánkovi	Johánek	k1gMnSc6	Johánek
<g/>
,	,	kIx,	,
zpovědníku	zpovědník	k1gMnSc3	zpovědník
královny	královna	k1gFnSc2	královna
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1480	[number]	k4	1480
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
inventář	inventář	k1gInSc4	inventář
svatovítského	svatovítský	k2eAgInSc2d1	svatovítský
pokladu	poklad	k1gInSc2	poklad
votivní	votivní	k2eAgInSc4d1	votivní
dar	dar	k1gInSc4	dar
ke	k	k7c3	k
cti	čest	k1gFnSc3	čest
"	"	kIx"	"
<g/>
blahoslaveného	blahoslavený	k2eAgMnSc4d1	blahoslavený
Jana	Jan	k1gMnSc4	Jan
z	z	k7c2	z
Pomuku	Pomuk	k1gInSc2	Pomuk
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
z	z	k7c2	z
Krumlova	Krumlov	k1gInSc2	Krumlov
<g/>
,	,	kIx,	,
děkan	děkan	k1gMnSc1	děkan
svatovítské	svatovítský	k2eAgFnSc2d1	Svatovítská
kapituly	kapitula	k1gFnSc2	kapitula
zaznamenává	zaznamenávat	k5eAaImIp3nS	zaznamenávat
roku	rok	k1gInSc2	rok
1483	[number]	k4	1483
omylem	omylem	k6eAd1	omylem
nesprávné	správný	k2eNgNnSc1d1	nesprávné
datum	datum	k1gNnSc1	datum
smrti	smrt	k1gFnSc2	smrt
Jana	Jan	k1gMnSc2	Jan
z	z	k7c2	z
Pomuka	Pomuk	k1gMnSc2	Pomuk
–	–	k?	–
1383	[number]	k4	1383
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
svědectví	svědectví	k1gNnPc1	svědectví
promlouvají	promlouvat	k5eAaImIp3nP	promlouvat
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
vzpomínka	vzpomínka	k1gFnSc1	vzpomínka
na	na	k7c4	na
Jana	Jan	k1gMnSc4	Jan
z	z	k7c2	z
Pomuku	Pomuk	k1gInSc2	Pomuk
zůstávala	zůstávat	k5eAaImAgFnS	zůstávat
přes	přes	k7c4	přes
husitské	husitský	k2eAgNnSc4d1	husitské
hnutí	hnutí	k1gNnSc4	hnutí
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
živá	živý	k2eAgFnSc1d1	živá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Václav	Václav	k1gMnSc1	Václav
Hájek	Hájek	k1gMnSc1	Hájek
z	z	k7c2	z
Libočan	Libočan	k1gMnSc1	Libočan
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
Kronice	kronika	k1gFnSc6	kronika
české	český	k2eAgNnSc1d1	české
(	(	kIx(	(
<g/>
1541	[number]	k4	1541
<g/>
)	)	kIx)	)
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
dva	dva	k4xCgMnPc4	dva
Jany	Jan	k1gMnPc4	Jan
<g/>
,	,	kIx,	,
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
a	a	k8xC	a
doktora	doktor	k1gMnSc2	doktor
Jana	Jan	k1gMnSc2	Jan
z	z	k7c2	z
Pomuku	Pomuk	k1gInSc2	Pomuk
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc4	on
byli	být	k5eAaImAgMnP	být
utopeni	utopen	k2eAgMnPc1d1	utopen
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
však	však	k9	však
roku	rok	k1gInSc2	rok
1383	[number]	k4	1383
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc1	druhý
roku	rok	k1gInSc2	rok
1393	[number]	k4	1393
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
názor	názor	k1gInSc4	názor
u	u	k7c2	u
velmi	velmi	k6eAd1	velmi
populární	populární	k2eAgFnSc2d1	populární
kroniky	kronika	k1gFnSc2	kronika
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
nakonec	nakonec	k6eAd1	nakonec
v	v	k7c6	v
obecném	obecný	k2eAgNnSc6d1	obecné
mínění	mínění	k1gNnSc6	mínění
převládl	převládnout	k5eAaPmAgMnS	převládnout
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1599	[number]	k4	1599
však	však	k9	však
zařazuje	zařazovat	k5eAaImIp3nS	zařazovat
Jiří	Jiří	k1gMnSc1	Jiří
Barthold	Barthold	k1gMnSc1	Barthold
Pontanus	Pontanus	k1gMnSc1	Pontanus
z	z	k7c2	z
Breitenberka	Breitenberka	k1gFnSc1	Breitenberka
"	"	kIx"	"
<g/>
Jana	Jana	k1gFnSc1	Jana
Zpovědníka	zpovědník	k1gMnSc2	zpovědník
<g/>
"	"	kIx"	"
mezi	mezi	k7c4	mezi
české	český	k2eAgFnPc4d1	Česká
zemské	zemský	k2eAgFnPc4d1	zemská
patrony	patrona	k1gFnPc4	patrona
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
potvrzuje	potvrzovat	k5eAaImIp3nS	potvrzovat
domněnku	domněnka	k1gFnSc4	domněnka
Pekařovu	Pekařův	k2eAgFnSc4d1	Pekařova
<g/>
,	,	kIx,	,
že	že	k8xS	že
kult	kult	k1gInSc1	kult
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
nejspíše	nejspíše	k9	nejspíše
prosadil	prosadit	k5eAaPmAgInS	prosadit
<g/>
,	,	kIx,	,
i	i	k8xC	i
kdyby	kdyby	kYmCp3nS	kdyby
nebylo	být	k5eNaImAgNnS	být
Bílé	bílý	k2eAgFnPc4d1	bílá
hory	hora	k1gFnPc4	hora
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vznik	vznik	k1gInSc1	vznik
kultu	kult	k1gInSc2	kult
===	===	k?	===
</s>
</p>
<p>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
polovina	polovina	k1gFnSc1	polovina
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
obdobím	období	k1gNnSc7	období
výrazného	výrazný	k2eAgNnSc2d1	výrazné
znovuobjevení	znovuobjevení	k1gNnSc2	znovuobjevení
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
<g/>
.	.	kIx.	.
</s>
<s>
Centrem	centr	k1gInSc7	centr
úcty	úcta	k1gFnSc2	úcta
byl	být	k5eAaImAgInS	být
oltář	oltář	k1gInSc1	oltář
ve	v	k7c6	v
Svatovítské	svatovítský	k2eAgFnSc6d1	Svatovítská
katedrále	katedrála	k1gFnSc6	katedrála
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1700	[number]	k4	1700
doplněn	doplnit	k5eAaPmNgMnS	doplnit
o	o	k7c6	o
stříbrné	stříbrná	k1gFnSc6	stříbrná
antependium	antependium	k1gNnSc1	antependium
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
vyšly	vyjít	k5eAaPmAgFnP	vyjít
v	v	k7c6	v
latině	latina	k1gFnSc6	latina
životopisy	životopis	k1gInPc7	životopis
od	od	k7c2	od
svatovítského	svatovítský	k2eAgMnSc2d1	svatovítský
kanovníka	kanovník	k1gMnSc2	kanovník
Jana	Jan	k1gMnSc2	Jan
Ignáce	Ignác	k1gMnSc2	Ignác
Dlouhoveského	Dlouhoveský	k2eAgMnSc2d1	Dlouhoveský
(	(	kIx(	(
<g/>
1668	[number]	k4	1668
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
především	především	k9	především
od	od	k7c2	od
jezuity	jezuita	k1gMnSc2	jezuita
Bohuslava	Bohuslava	k1gFnSc1	Bohuslava
Balbína	Balbína	k1gFnSc1	Balbína
roku	rok	k1gInSc2	rok
1680	[number]	k4	1680
<g/>
,	,	kIx,	,
vytištěný	vytištěný	k2eAgInSc1d1	vytištěný
i	i	k9	i
v	v	k7c6	v
antverpské	antverpský	k2eAgFnSc6d1	Antverpská
edici	edice	k1gFnSc6	edice
Acta	Act	k1gInSc2	Act
sanctorum	sanctorum	k1gInSc1	sanctorum
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1683	[number]	k4	1683
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
pražském	pražský	k2eAgInSc6d1	pražský
mostě	most	k1gInSc6	most
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
Karlově	Karlův	k2eAgFnSc6d1	Karlova
<g/>
,	,	kIx,	,
vztyčena	vztyčen	k2eAgFnSc1d1	vztyčena
bronzová	bronzový	k2eAgFnSc1d1	bronzová
socha	socha	k1gFnSc1	socha
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
od	od	k7c2	od
Jana	Jan	k1gMnSc2	Jan
Brokofa	Brokof	k1gMnSc2	Brokof
podle	podle	k7c2	podle
modelu	model	k1gInSc2	model
Matthiase	Matthiasa	k1gFnSc3	Matthiasa
Rauchmüllera	Rauchmüller	k1gMnSc2	Rauchmüller
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1691	[number]	k4	1691
byla	být	k5eAaImAgFnS	být
vystavěna	vystavěn	k2eAgFnSc1d1	vystavěna
dřevěná	dřevěný	k2eAgFnSc1d1	dřevěná
kaple	kaple	k1gFnSc1	kaple
zasvěcená	zasvěcený	k2eAgFnSc1d1	zasvěcená
Janu	Jan	k1gMnSc3	Jan
Nepomuckému	Nepomucký	k1gMnSc3	Nepomucký
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
dnešního	dnešní	k2eAgInSc2d1	dnešní
kostela	kostel	k1gInSc2	kostel
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
na	na	k7c6	na
Skalce	skalka	k1gFnSc6	skalka
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
při	při	k7c6	při
ní	on	k3xPp3gFnSc6	on
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
první	první	k4xOgNnSc1	první
náboženské	náboženský	k2eAgNnSc1d1	náboženské
bratrstvo	bratrstvo	k1gNnSc1	bratrstvo
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1708	[number]	k4	1708
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Hradci	Hradec	k1gInSc6	Hradec
Králové	Králová	k1gFnSc2	Králová
sv.	sv.	kA	sv.
Janu	Jan	k1gMnSc3	Jan
Nepomuckému	Nepomucký	k1gMnSc3	Nepomucký
oficiálně	oficiálně	k6eAd1	oficiálně
zasvěcen	zasvětit	k5eAaPmNgInS	zasvětit
první	první	k4xOgInSc1	první
kostel	kostel	k1gInSc1	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1639	[number]	k4	1639
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Nepomuku	Nepomuk	k1gInSc6	Nepomuk
<g/>
,	,	kIx,	,
Janově	Janův	k2eAgNnSc6d1	Janovo
rodišti	rodiště	k1gNnSc6	rodiště
<g/>
,	,	kIx,	,
započato	započnout	k5eAaPmNgNnS	započnout
na	na	k7c6	na
údajném	údajný	k2eAgNnSc6d1	údajné
místě	místo	k1gNnSc6	místo
jeho	jeho	k3xOp3gInSc2	jeho
rodného	rodný	k2eAgInSc2d1	rodný
domu	dům	k1gInSc2	dům
se	s	k7c7	s
stavbou	stavba	k1gFnSc7	stavba
chrámu	chrám	k1gInSc2	chrám
<g/>
,	,	kIx,	,
zasvěceného	zasvěcený	k2eAgInSc2d1	zasvěcený
ovšem	ovšem	k9	ovšem
Janu	Jan	k1gMnSc3	Jan
Křtiteli	křtitel	k1gMnSc3	křtitel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1641	[number]	k4	1641
vychází	vycházet	k5eAaImIp3nS	vycházet
česko-německo-latinský	českoěmeckoatinský	k2eAgInSc1d1	česko-německo-latinský
spisek	spisek	k1gInSc1	spisek
Fama	Fama	k?	Fama
Posthuma	posthuma	k1gFnSc1	posthuma
s	s	k7c7	s
ilustracemi	ilustrace	k1gFnPc7	ilustrace
Karla	Karel	k1gMnSc2	Karel
Škréty	Škréta	k1gMnSc2	Škréta
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
jako	jako	k9	jako
první	první	k4xOgFnSc7	první
literárně	literárně	k6eAd1	literárně
zpracovává	zpracovávat	k5eAaImIp3nS	zpracovávat
svatojánskou	svatojánský	k2eAgFnSc4d1	Svatojánská
legendu	legenda	k1gFnSc4	legenda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kanonizace	kanonizace	k1gFnSc2	kanonizace
===	===	k?	===
</s>
</p>
<p>
<s>
Snaha	snaha	k1gFnSc1	snaha
o	o	k7c6	o
kanonizaci	kanonizace	k1gFnSc6	kanonizace
Jana	Jan	k1gMnSc2	Jan
z	z	k7c2	z
Pomuku	Pomuk	k1gInSc2	Pomuk
začíná	začínat	k5eAaImIp3nS	začínat
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
spojena	spojit	k5eAaPmNgFnS	spojit
s	s	k7c7	s
pouze	pouze	k6eAd1	pouze
částečně	částečně	k6eAd1	částečně
přesným	přesný	k2eAgInSc7d1	přesný
Balbínovým	Balbínův	k2eAgInSc7d1	Balbínův
životopisem	životopis	k1gInSc7	životopis
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
Tomáše	Tomáš	k1gMnSc2	Tomáš
Pešiny	Pešina	k1gMnSc2	Pešina
z	z	k7c2	z
Čechorodu	Čechorod	k1gInSc2	Čechorod
a	a	k8xC	a
Jana	Jan	k1gMnSc2	Jan
Ignáce	Ignác	k1gMnSc2	Ignác
Dlouhoveského	Dlouhoveský	k2eAgMnSc2d1	Dlouhoveský
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
josefinské	josefinský	k2eAgFnSc6d1	josefinská
době	doba	k1gFnSc6	doba
a	a	k8xC	a
ve	v	k7c6	v
větší	veliký	k2eAgFnSc6d2	veliký
míře	míra	k1gFnSc6	míra
od	od	k7c2	od
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgInP	začít
šířit	šířit	k5eAaImF	šířit
dohady	dohad	k1gInPc1	dohad
<g/>
,	,	kIx,	,
že	že	k8xS	že
kult	kult	k1gInSc1	kult
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
uměle	uměle	k6eAd1	uměle
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
jezuité	jezuita	k1gMnPc1	jezuita
jako	jako	k8xC	jako
nástroj	nástroj	k1gInSc1	nástroj
protireformace	protireformace	k1gFnSc2	protireformace
<g/>
.	.	kIx.	.
</s>
<s>
Vyvracel	vyvracet	k5eAaImAgMnS	vyvracet
je	být	k5eAaImIp3nS	být
fakt	fakt	k1gInSc4	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
už	už	k6eAd1	už
bylo	být	k5eAaImAgNnS	být
po	po	k7c6	po
katolické	katolický	k2eAgFnSc6d1	katolická
reformaci	reformace	k1gFnSc6	reformace
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
někteří	některý	k3yIgMnPc1	některý
jezuité	jezuita	k1gMnPc1	jezuita
již	již	k6eAd1	již
v	v	k7c6	v
Balbínově	Balbínův	k2eAgFnSc6d1	Balbínova
době	doba	k1gFnSc6	doba
věrohodnost	věrohodnost	k1gFnSc4	věrohodnost
tohoto	tento	k3xDgInSc2	tento
životopisu	životopis	k1gInSc2	životopis
napadli	napadnout	k5eAaPmAgMnP	napadnout
<g/>
.	.	kIx.	.
</s>
<s>
Pekař	Pekař	k1gMnSc1	Pekař
dodává	dodávat	k5eAaImIp3nS	dodávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Balbín	Balbín	k1gMnSc1	Balbín
přibásnil	přibásnit	k5eAaPmAgMnS	přibásnit
životopis	životopis	k1gInSc4	životopis
ne	ne	k9	ne
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
jezuita	jezuita	k1gMnSc1	jezuita
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
protože	protože	k8xS	protože
byl	být	k5eAaImAgMnS	být
vlastenec	vlastenec	k1gMnSc1	vlastenec
<g/>
.	.	kIx.	.
<g/>
Řím	Řím	k1gInSc1	Řím
se	se	k3xPyFc4	se
ke	k	k7c3	k
snahám	snaha	k1gFnPc3	snaha
o	o	k7c6	o
kanonizaci	kanonizace	k1gFnSc6	kanonizace
stavěl	stavět	k5eAaImAgMnS	stavět
zpočátku	zpočátku	k6eAd1	zpočátku
skepticky	skepticky	k6eAd1	skepticky
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
15	[number]	k4	15
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
roku	rok	k1gInSc2	rok
1715	[number]	k4	1715
pražský	pražský	k2eAgMnSc1d1	pražský
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
František	František	k1gMnSc1	František
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Khünburg	Khünburg	k1gMnSc1	Khünburg
úspěšně	úspěšně	k6eAd1	úspěšně
zahájil	zahájit	k5eAaPmAgMnS	zahájit
beatifikační	beatifikační	k2eAgInSc4d1	beatifikační
proces	proces	k1gInSc4	proces
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
tento	tento	k3xDgInSc4	tento
den	den	k1gInSc4	den
se	se	k3xPyFc4	se
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
poprvé	poprvé	k6eAd1	poprvé
konaly	konat	k5eAaImAgFnP	konat
vodní	vodní	k2eAgFnPc1d1	vodní
slavnosti	slavnost	k1gFnPc1	slavnost
Navalis	Navalis	k1gFnSc2	Navalis
k	k	k7c3	k
poctě	pocta	k1gFnSc3	pocta
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
<g/>
.	.	kIx.	.
31	[number]	k4	31
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1721	[number]	k4	1721
papež	papež	k1gMnSc1	papež
Inocenc	Inocenc	k1gMnSc1	Inocenc
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1721	[number]	k4	1721
<g/>
–	–	k?	–
<g/>
1724	[number]	k4	1724
<g/>
)	)	kIx)	)
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
Jana	Jan	k1gMnSc4	Jan
za	za	k7c4	za
blahoslaveného	blahoslavený	k2eAgMnSc4d1	blahoslavený
<g/>
.	.	kIx.	.
</s>
<s>
Následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
byl	být	k5eAaImAgInS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
kanonizační	kanonizační	k2eAgInSc1d1	kanonizační
proces	proces	k1gInSc1	proces
<g/>
.	.	kIx.	.
</s>
<s>
Kanonizační	kanonizační	k2eAgFnSc1d1	kanonizační
komise	komise	k1gFnSc1	komise
uznala	uznat	k5eAaPmAgFnS	uznat
čtyři	čtyři	k4xCgInPc4	čtyři
zázraky	zázrak	k1gInPc4	zázrak
<g/>
,	,	kIx,	,
zachování	zachování	k1gNnSc4	zachování
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc1	jeho
naběhnutí	naběhnutí	k1gNnSc1	naběhnutí
a	a	k8xC	a
zčervenání	zčervenání	k1gNnSc1	zčervenání
<g/>
,	,	kIx,	,
zachránění	zachránění	k1gNnSc1	zachránění
Rozálie	Rozálie	k1gFnSc2	Rozálie
Hodánkové	Hodánková	k1gFnSc2	Hodánková
a	a	k8xC	a
uzdravení	uzdravení	k1gNnSc2	uzdravení
Terezie	Terezie	k1gFnSc2	Terezie
Veroniky	Veronika	k1gFnSc2	Veronika
Krebsové	Krebsová	k1gFnSc2	Krebsová
<g/>
.	.	kIx.	.
</s>
<s>
Benedikt	Benedikt	k1gMnSc1	Benedikt
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
dne	den	k1gInSc2	den
19	[number]	k4	19
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1729	[number]	k4	1729
kanonizační	kanonizační	k2eAgFnSc7d1	kanonizační
bulou	bula	k1gFnSc7	bula
Christus	Christus	k1gMnSc1	Christus
Dominus	Dominus	k1gMnSc1	Dominus
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
Jana	Jan	k1gMnSc4	Jan
za	za	k7c4	za
svatého	svatý	k2eAgMnSc4d1	svatý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Hrob	hrob	k1gInSc1	hrob
a	a	k8xC	a
náhrobek	náhrobek	k1gInSc1	náhrobek
===	===	k?	===
</s>
</p>
<p>
<s>
Místo	místo	k7c2	místo
Janova	Janův	k2eAgInSc2d1	Janův
pohřbu	pohřeb	k1gInSc2	pohřeb
v	v	k7c6	v
jižním	jižní	k2eAgInSc6d1	jižní
ochozu	ochoz	k1gInSc6	ochoz
chóru	chór	k1gInSc6	chór
katedrály	katedrála	k1gFnSc2	katedrála
před	před	k7c7	před
Vlašimskou	vlašimský	k2eAgFnSc7d1	Vlašimská
kaplí	kaple	k1gFnSc7	kaple
bylo	být	k5eAaImAgNnS	být
vyznačeno	vyznačit	k5eAaPmNgNnS	vyznačit
kamennou	kamenný	k2eAgFnSc7d1	kamenná
deskou	deska	k1gFnSc7	deska
se	se	k3xPyFc4	se
jménem	jméno	k1gNnSc7	jméno
v	v	k7c6	v
dlažbě	dlažba	k1gFnSc6	dlažba
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
ní	on	k3xPp3gFnSc2	on
byla	být	k5eAaImAgFnS	být
podle	podle	k7c2	podle
svědectví	svědectví	k1gNnSc2	svědectví
Žitavské	žitavský	k2eAgFnSc2d1	Žitavská
kroniky	kronika	k1gFnSc2	kronika
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
umístěna	umístit	k5eAaPmNgFnS	umístit
mříž	mříž	k1gFnSc1	mříž
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc4d1	další
mříž	mříž	k1gFnSc4	mříž
dal	dát	k5eAaPmAgMnS	dát
zřídit	zřídit	k5eAaPmF	zřídit
svatovítský	svatovítský	k2eAgMnSc1d1	svatovítský
děkan	děkan	k1gMnSc1	děkan
Václav	Václav	k1gMnSc1	Václav
z	z	k7c2	z
Wolfenburka	Wolfenburek	k1gMnSc2	Wolfenburek
roku	rok	k1gInSc2	rok
1530	[number]	k4	1530
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc4d1	další
úpravy	úprava	k1gFnPc4	úprava
zaznamenávají	zaznamenávat	k5eAaImIp3nP	zaznamenávat
analisté	analista	k1gMnPc1	analista
k	k	k7c3	k
roku	rok	k1gInSc3	rok
1595	[number]	k4	1595
<g/>
,	,	kIx,	,
1621	[number]	k4	1621
a	a	k8xC	a
1679	[number]	k4	1679
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
Jan	Jan	k1gMnSc1	Jan
Bedřich	Bedřich	k1gMnSc1	Bedřich
z	z	k7c2	z
Valdštejna	Valdštejn	k1gInSc2	Valdštejn
před	před	k7c7	před
oltářem	oltář	k1gInSc7	oltář
sloužil	sloužit	k5eAaImAgMnS	sloužit
bohoslužbu	bohoslužba	k1gFnSc4	bohoslužba
<g/>
.	.	kIx.	.
</s>
<s>
Rytina	rytina	k1gFnSc1	rytina
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1712	[number]	k4	1712
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
oltářní	oltářní	k2eAgInSc4d1	oltářní
stůl	stůl	k1gInSc4	stůl
s	s	k7c7	s
rakví	rakev	k1gFnSc7	rakev
a	a	k8xC	a
tabernákl	tabernákl	k1gInSc1	tabernákl
<g/>
.	.	kIx.	.
</s>
<s>
Propagační	propagační	k2eAgFnPc1d1	propagační
rytiny	rytina	k1gFnPc1	rytina
kanonizace	kanonizace	k1gFnSc2	kanonizace
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1729	[number]	k4	1729
zachycují	zachycovat	k5eAaImIp3nP	zachycovat
stav	stav	k1gInSc4	stav
dosavadní	dosavadní	k2eAgFnSc2d1	dosavadní
výzdoby	výzdoba	k1gFnSc2	výzdoba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podnět	podnět	k1gInSc1	podnět
ke	k	k7c3	k
zbudování	zbudování	k1gNnSc3	zbudování
nového	nový	k2eAgInSc2d1	nový
pražského	pražský	k2eAgInSc2d1	pražský
náhrobku	náhrobek	k1gInSc2	náhrobek
podala	podat	k5eAaPmAgFnS	podat
hraběnka	hraběnka	k1gFnSc1	hraběnka
Kinská	Kinská	k1gFnSc1	Kinská
<g/>
,	,	kIx,	,
rozená	rozený	k2eAgFnSc1d1	rozená
z	z	k7c2	z
Fünfkirchu	Fünfkirch	k1gInSc2	Fünfkirch
(	(	kIx(	(
<g/>
†	†	k?	†
1729	[number]	k4	1729
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
její	její	k3xOp3gInSc1	její
finanční	finanční	k2eAgInSc1d1	finanční
dar	dar	k1gInSc1	dar
se	se	k3xPyFc4	se
spojil	spojit	k5eAaPmAgInS	spojit
s	s	k7c7	s
dlouhodobou	dlouhodobý	k2eAgFnSc7d1	dlouhodobá
veřejnou	veřejný	k2eAgFnSc7d1	veřejná
kanonizační	kanonizační	k2eAgFnSc7d1	kanonizační
sbírkou	sbírka	k1gFnSc7	sbírka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
skončila	skončit	k5eAaPmAgFnS	skončit
přebytkem	přebytek	k1gInSc7	přebytek
<g/>
.	.	kIx.	.
</s>
<s>
Prostředníky	prostředník	k1gInPc7	prostředník
realizace	realizace	k1gFnSc2	realizace
velkolepého	velkolepý	k2eAgInSc2d1	velkolepý
stříbrného	stříbrný	k2eAgInSc2d1	stříbrný
pomníku	pomník	k1gInSc2	pomník
byli	být	k5eAaImAgMnP	být
kardinál	kardinál	k1gMnSc1	kardinál
Bedřich	Bedřich	k1gMnSc1	Bedřich
z	z	k7c2	z
Althanu	Althan	k1gInSc2	Althan
a	a	k8xC	a
tajný	tajný	k2eAgMnSc1d1	tajný
rada	rada	k1gMnSc1	rada
Gundakar	Gundakar	k1gMnSc1	Gundakar
z	z	k7c2	z
Althanu	Althan	k1gInSc2	Althan
<g/>
,	,	kIx,	,
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
císaře	císař	k1gMnSc2	císař
Karla	Karel	k1gMnSc2	Karel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
padla	padnout	k5eAaPmAgFnS	padnout
volba	volba	k1gFnSc1	volba
na	na	k7c4	na
dvorní	dvorní	k2eAgMnPc4d1	dvorní
umělce	umělec	k1gMnPc4	umělec
<g/>
:	:	kIx,	:
Podle	podle	k7c2	podle
náčrtu	náčrt	k1gInSc2	náčrt
Josefa	Josef	k1gMnSc2	Josef
Emanuela	Emanuel	k1gMnSc2	Emanuel
Fischera	Fischer	k1gMnSc2	Fischer
z	z	k7c2	z
Erlachu	Erlach	k1gInSc2	Erlach
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1733	[number]	k4	1733
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
italský	italský	k2eAgMnSc1d1	italský
sochař	sochař	k1gMnSc1	sochař
Antonio	Antonio	k1gMnSc1	Antonio
Corradini	Corradin	k2eAgMnPc1d1	Corradin
dřevěný	dřevěný	k2eAgInSc4d1	dřevěný
model	model	k1gInSc4	model
figury	figura	k1gFnSc2	figura
světce	světec	k1gMnSc2	světec
<g/>
,	,	kIx,	,
klečícího	klečící	k2eAgMnSc2d1	klečící
na	na	k7c6	na
sarkofágu	sarkofág	k1gInSc6	sarkofág
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgInPc7	dva
anděly	anděl	k1gMnPc7	anděl
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
po	po	k7c6	po
schválení	schválení	k1gNnSc6	schválení
císařem	císař	k1gMnSc7	císař
roku	rok	k1gInSc2	rok
1735	[number]	k4	1735
převedl	převést	k5eAaPmAgMnS	převést
do	do	k7c2	do
monumentálního	monumentální	k2eAgNnSc2d1	monumentální
měřítka	měřítko	k1gNnSc2	měřítko
<g/>
.	.	kIx.	.
</s>
<s>
Dřevěné	dřevěný	k2eAgInPc1d1	dřevěný
kadluby	kadlub	k1gInPc1	kadlub
v	v	k7c6	v
letech	let	k1gInPc6	let
1735	[number]	k4	1735
<g/>
–	–	k?	–
<g/>
1736	[number]	k4	1736
do	do	k7c2	do
stříbra	stříbro	k1gNnSc2	stříbro
odlil	odlít	k5eAaPmAgMnS	odlít
a	a	k8xC	a
vytepal	vytepat	k5eAaPmAgMnS	vytepat
vídeňský	vídeňský	k2eAgMnSc1d1	vídeňský
stříbrník	stříbrník	k1gMnSc1	stříbrník
Johann	Johann	k1gMnSc1	Johann
Joseph	Joseph	k1gMnSc1	Joseph
Würth	Würth	k1gMnSc1	Würth
s	s	k7c7	s
pěti	pět	k4xCc7	pět
tovaryši	tovaryš	k1gMnPc7	tovaryš
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
s	s	k7c7	s
Raffaelem	Raffael	k1gMnSc7	Raffael
Donnerem	Donner	k1gMnSc7	Donner
a	a	k8xC	a
Kašparem	Kašpar	k1gMnSc7	Kašpar
Gschwandtnerem	Gschwandtner	k1gMnSc7	Gschwandtner
<g/>
.	.	kIx.	.
</s>
<s>
Stříbrné	stříbrný	k2eAgNnSc1d1	stříbrné
mauzoleum	mauzoleum	k1gNnSc1	mauzoleum
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
pak	pak	k6eAd1	pak
smontovali	smontovat	k5eAaPmAgMnP	smontovat
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
dosavadního	dosavadní	k2eAgInSc2d1	dosavadní
hrobu	hrob	k1gInSc2	hrob
a	a	k8xC	a
oltáře	oltář	k1gInSc2	oltář
v	v	k7c6	v
chóru	chór	k1gInSc6	chór
Svatovítské	svatovítský	k2eAgFnSc2d1	Svatovítská
katedrály	katedrála	k1gFnSc2	katedrála
<g/>
.	.	kIx.	.
</s>
<s>
Sochy	socha	k1gFnPc1	socha
letících	letící	k2eAgMnPc2d1	letící
andílků	andílek	k1gMnPc2	andílek
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1771	[number]	k4	1771
<g/>
,	,	kIx,	,
baldachýn	baldachýn	k1gInSc1	baldachýn
na	na	k7c4	na
mauzoleum	mauzoleum	k1gNnSc4	mauzoleum
objednala	objednat	k5eAaPmAgFnS	objednat
císařovna	císařovna	k1gFnSc1	císařovna
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
<g/>
.	.	kIx.	.
</s>
<s>
Mramorovou	mramorový	k2eAgFnSc4d1	mramorová
balustrádu	balustráda	k1gFnSc4	balustráda
objednal	objednat	k5eAaPmAgMnS	objednat
pražský	pražský	k2eAgMnSc1d1	pražský
biskup	biskup	k1gMnSc1	biskup
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Jiřík	Jiřík	k1gMnSc1	Jiřík
Chřepický	Chřepický	k2eAgMnSc1d1	Chřepický
z	z	k7c2	z
Modlíškovic	Modlíškovice	k1gFnPc2	Modlíškovice
roku	rok	k1gInSc2	rok
1746	[number]	k4	1746
<g/>
.	.	kIx.	.
</s>
<s>
Šestici	šestice	k1gFnSc4	šestice
ženských	ženský	k2eAgFnPc2d1	ženská
postav	postava	k1gFnPc2	postava
na	na	k7c6	na
nárožích	nároží	k1gNnPc6	nároží
-	-	kIx~	-
alegorie	alegorie	k1gFnSc1	alegorie
Křesťanských	křesťanský	k2eAgFnPc2d1	křesťanská
ctností	ctnost	k1gFnPc2	ctnost
-	-	kIx~	-
podle	podle	k7c2	podle
modelů	model	k1gInPc2	model
sochaře	sochař	k1gMnSc2	sochař
Platzera	Platzer	k1gMnSc2	Platzer
dodal	dodat	k5eAaPmAgMnS	dodat
pražský	pražský	k2eAgMnSc1d1	pražský
stříbrník	stříbrník	k1gMnSc1	stříbrník
Josef	Josef	k1gMnSc1	Josef
Seitz	Seitz	k1gMnSc1	Seitz
až	až	k9	až
roku	rok	k1gInSc2	rok
1749	[number]	k4	1749
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Spor	spor	k1gInSc1	spor
o	o	k7c4	o
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
==	==	k?	==
</s>
</p>
<p>
<s>
Historikové	historik	k1gMnPc1	historik
se	se	k3xPyFc4	se
shodují	shodovat	k5eAaImIp3nP	shodovat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
při	při	k7c6	při
kanonizaci	kanonizace	k1gFnSc6	kanonizace
se	s	k7c7	s
základem	základ	k1gInSc7	základ
obrazu	obraz	k1gInSc2	obraz
světce	světec	k1gMnSc2	světec
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnPc2	jehož
se	se	k3xPyFc4	se
kongregace	kongregace	k1gFnSc1	kongregace
sv.	sv.	kA	sv.
ritu	rit	k1gInSc2	rit
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
kanonizovat	kanonizovat	k5eAaBmF	kanonizovat
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
Johánek	Johánek	k1gMnSc1	Johánek
z	z	k7c2	z
Pomuka	Pomuk	k1gMnSc2	Pomuk
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
byl	být	k5eAaImAgMnS	být
utopen	utopit	k5eAaPmNgMnS	utopit
roku	rok	k1gInSc2	rok
1383	[number]	k4	1383
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
nejspíše	nejspíše	k9	nejspíše
došlo	dojít	k5eAaPmAgNnS	dojít
díky	díky	k7c3	díky
kronikáři	kronikář	k1gMnSc3	kronikář
Hájkovi	Hájek	k1gMnSc3	Hájek
z	z	k7c2	z
Libočan	Libočan	k1gMnSc1	Libočan
<g/>
,	,	kIx,	,
dějepisci	dějepisec	k1gMnSc3	dějepisec
Balbínovi	Balbín	k1gMnSc3	Balbín
a	a	k8xC	a
zmatenosti	zmatenost	k1gFnSc3	zmatenost
pramenů	pramen	k1gInPc2	pramen
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
obraz	obraz	k1gInSc1	obraz
však	však	k9	však
vypovídá	vypovídat	k5eAaImIp3nS	vypovídat
i	i	k9	i
o	o	k7c6	o
Janu	Jan	k1gMnSc6	Jan
Nepomuckém	Nepomucký	k1gMnSc6	Nepomucký
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
pouze	pouze	k6eAd1	pouze
částečně	částečně	k6eAd1	částečně
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
chybí	chybit	k5eAaPmIp3nS	chybit
druhá	druhý	k4xOgFnSc1	druhý
část	část	k1gFnSc1	část
postavy	postava	k1gFnSc2	postava
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
mučedník	mučedník	k1gMnSc1	mučedník
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1393	[number]	k4	1393
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
vylíčenou	vylíčený	k2eAgFnSc7d1	vylíčená
totožná	totožný	k2eAgNnPc1d1	totožné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1747	[number]	k4	1747
Eliáš	Eliáš	k1gMnSc1	Eliáš
Sandrich	Sandrich	k1gMnSc1	Sandrich
vyvrací	vyvracet	k5eAaImIp3nS	vyvracet
Hájkův	Hájkův	k2eAgInSc4d1	Hájkův
omyl	omyl	k1gInSc4	omyl
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
rozdělil	rozdělit	k5eAaPmAgMnS	rozdělit
Jana	Jan	k1gMnSc4	Jan
do	do	k7c2	do
dvou	dva	k4xCgFnPc2	dva
postav	postava	k1gFnPc2	postava
a	a	k8xC	a
datuje	datovat	k5eAaImIp3nS	datovat
Janovu	Janův	k2eAgFnSc4d1	Janova
smrt	smrt	k1gFnSc4	smrt
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1393	[number]	k4	1393
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
dostává	dostávat	k5eAaImIp3nS	dostávat
text	text	k1gInSc1	text
Jenštejnovy	Jenštejnův	k2eAgFnSc2d1	Jenštejnova
žaloby	žaloba	k1gFnSc2	žaloba
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
těsně	těsně	k6eAd1	těsně
po	po	k7c6	po
Janově	Janův	k2eAgFnSc6d1	Janova
smrti	smrt	k1gFnSc6	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Gelasius	Gelasius	k1gInSc1	Gelasius
Dobner	Dobnra	k1gFnPc2	Dobnra
roku	rok	k1gInSc2	rok
1784	[number]	k4	1784
kritickým	kritický	k2eAgNnSc7d1	kritické
zkoumáním	zkoumání	k1gNnSc7	zkoumání
pramenů	pramen	k1gInPc2	pramen
na	na	k7c6	na
konci	konec	k1gInSc6	konec
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
potvrzuje	potvrzovat	k5eAaImIp3nS	potvrzovat
nespolehlivost	nespolehlivost	k1gFnSc4	nespolehlivost
Hájkovy	Hájkův	k2eAgFnSc2d1	Hájkova
kroniky	kronika	k1gFnSc2	kronika
a	a	k8xC	a
konkrétně	konkrétně	k6eAd1	konkrétně
odhalil	odhalit	k5eAaPmAgMnS	odhalit
také	také	k9	také
Hájkovo	Hájkův	k2eAgNnSc4d1	Hájkovo
chybné	chybný	k2eAgNnSc4d1	chybné
čtení	čtení	k1gNnSc4	čtení
dobových	dobový	k2eAgInPc2d1	dobový
pramenů	pramen	k1gInPc2	pramen
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
historický	historický	k2eAgMnSc1d1	historický
vikář	vikář	k1gMnSc1	vikář
Jan	Jan	k1gMnSc1	Jan
z	z	k7c2	z
Pomuku	Pomuk	k1gInSc2	Pomuk
totožný	totožný	k2eAgMnSc1d1	totožný
s	s	k7c7	s
legendárním	legendární	k2eAgMnSc7d1	legendární
mučedníkem	mučedník	k1gMnSc7	mučedník
zpovědního	zpovědní	k2eAgNnSc2d1	zpovědní
tajemství	tajemství	k1gNnSc2	tajemství
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Dobnerem	Dobner	k1gMnSc7	Dobner
polemizuje	polemizovat	k5eAaImIp3nS	polemizovat
Josef	Josef	k1gMnSc1	Josef
Dobrovský	Dobrovský	k1gMnSc1	Dobrovský
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vylučuje	vylučovat	k5eAaImIp3nS	vylučovat
zpověď	zpověď	k1gFnSc4	zpověď
jako	jako	k8xS	jako
důvod	důvod	k1gInSc4	důvod
Janovy	Janův	k2eAgFnSc2d1	Janova
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Dobnerova	Dobnerův	k2eAgInSc2d1	Dobnerův
názoru	názor	k1gInSc2	názor
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
přidrží	přidržet	k5eAaPmIp3nS	přidržet
např.	např.	kA	např.
František	František	k1gMnSc1	František
Palacký	Palacký	k1gMnSc1	Palacký
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1849	[number]	k4	1849
publikuje	publikovat	k5eAaBmIp3nS	publikovat
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Břetislav	Břetislav	k1gMnSc1	Břetislav
Mikovec	Mikovec	k1gMnSc1	Mikovec
teorii	teorie	k1gFnSc4	teorie
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
níž	nízce	k6eAd2	nízce
pravým	pravý	k2eAgInSc7d1	pravý
důvodem	důvod	k1gInSc7	důvod
"	"	kIx"	"
<g/>
umělého	umělý	k2eAgNnSc2d1	umělé
vytvoření	vytvoření	k1gNnSc2	vytvoření
<g/>
"	"	kIx"	"
postavy	postava	k1gFnSc2	postava
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
kanonizace	kanonizace	k1gFnSc1	kanonizace
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
podporovaného	podporovaný	k2eAgInSc2d1	podporovaný
kultu	kult	k1gInSc2	kult
byla	být	k5eAaImAgFnS	být
snaha	snaha	k1gFnSc1	snaha
zastínit	zastínit	k5eAaPmF	zastínit
kult	kult	k1gInSc4	kult
Mistra	mistr	k1gMnSc2	mistr
Jana	Jan	k1gMnSc2	Jan
Husa	Hus	k1gMnSc2	Hus
<g/>
.	.	kIx.	.
</s>
<s>
Základem	základ	k1gInSc7	základ
argumentace	argumentace	k1gFnSc2	argumentace
odpůrců	odpůrce	k1gMnPc2	odpůrce
kultu	kult	k1gInSc2	kult
sv.	sv.	kA	sv.
Jana	Jana	k1gFnSc1	Jana
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
roku	rok	k1gInSc2	rok
1855	[number]	k4	1855
kniha	kniha	k1gFnSc1	kniha
Otto	Otto	k1gMnSc1	Otto
Abela	Abela	k1gMnSc1	Abela
Legenda	legenda	k1gFnSc1	legenda
o	o	k7c6	o
svatém	svatý	k2eAgMnSc6d1	svatý
Janu	Jan	k1gMnSc6	Jan
Nepomuckém	Nepomucký	k1gMnSc6	Nepomucký
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
Československé	československý	k2eAgFnSc2d1	Československá
republiky	republika	k1gFnSc2	republika
docházelo	docházet	k5eAaImAgNnS	docházet
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1919	[number]	k4	1919
až	až	k9	až
1920	[number]	k4	1920
k	k	k7c3	k
vlně	vlna	k1gFnSc3	vlna
ničení	ničení	k1gNnSc2	ničení
nepomucenských	pomucenský	k2eNgFnPc2d1	pomucenský
památek	památka	k1gFnPc2	památka
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
pozdější	pozdní	k2eAgFnSc2d2	pozdější
doby	doba	k1gFnSc2	doba
bylo	být	k5eAaImAgNnS	být
prokázáno	prokázat	k5eAaPmNgNnS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
svatý	svatý	k2eAgMnSc1d1	svatý
Jan	Jan	k1gMnSc1	Jan
Nepomucký	Nepomucký	k1gMnSc1	Nepomucký
je	být	k5eAaImIp3nS	být
Johánek	Johánek	k1gMnSc1	Johánek
z	z	k7c2	z
Pomuku	Pomuk	k1gInSc2	Pomuk
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
vyvrácen	vyvrátit	k5eAaPmNgInS	vyvrátit
omyl	omyl	k1gInSc4	omyl
Václava	Václav	k1gMnSc2	Václav
Hájka	Hájek	k1gMnSc2	Hájek
z	z	k7c2	z
Libočan	Libočan	k1gMnSc1	Libočan
(	(	kIx(	(
<g/>
ovšem	ovšem	k9	ovšem
ne	ne	k9	ne
jediný	jediný	k2eAgMnSc1d1	jediný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
ideovém	ideový	k2eAgNnSc6d1	ideové
prostředí	prostředí	k1gNnSc6	prostředí
první	první	k4xOgFnSc2	první
republiky	republika	k1gFnSc2	republika
se	se	k3xPyFc4	se
Jan	Jan	k1gMnSc1	Jan
Nepomucký	Nepomucký	k1gMnSc1	Nepomucký
stal	stát	k5eAaPmAgMnS	stát
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
témat	téma	k1gNnPc2	téma
tzv.	tzv.	kA	tzv.
sporu	spora	k1gFnSc4	spora
o	o	k7c4	o
smysl	smysl	k1gInSc4	smysl
českých	český	k2eAgFnPc2d1	Česká
dějin	dějiny	k1gFnPc2	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1921	[number]	k4	1921
k	k	k7c3	k
diskuzi	diskuze	k1gFnSc3	diskuze
v	v	k7c6	v
Národních	národní	k2eAgInPc6d1	národní
listech	list	k1gInPc6	list
mezi	mezi	k7c7	mezi
Janem	Jan	k1gMnSc7	Jan
Herbenem	Herben	k1gMnSc7	Herben
a	a	k8xC	a
Josefem	Josef	k1gMnSc7	Josef
Pekařem	Pekař	k1gMnSc7	Pekař
<g/>
.	.	kIx.	.
</s>
<s>
Pekařovy	Pekařův	k2eAgInPc1d1	Pekařův
články	článek	k1gInPc1	článek
byly	být	k5eAaImAgInP	být
shrnuty	shrnout	k5eAaPmNgInP	shrnout
a	a	k8xC	a
vydány	vydat	k5eAaPmNgInP	vydat
jako	jako	k9	jako
Tři	tři	k4xCgFnPc1	tři
kapitoly	kapitola	k1gFnPc1	kapitola
z	z	k7c2	z
boje	boj	k1gInSc2	boj
o	o	k7c6	o
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
brání	bránit	k5eAaImIp3nP	bránit
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
osoba	osoba	k1gFnSc1	osoba
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
stavěla	stavět	k5eAaImAgFnS	stavět
proti	proti	k7c3	proti
osobě	osoba	k1gFnSc3	osoba
Husově	Husův	k2eAgFnSc3d1	Husova
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Johánek	Johánek	k1gMnSc1	Johánek
z	z	k7c2	z
Pomuku	Pomuk	k1gInSc2	Pomuk
byl	být	k5eAaImAgInS	být
již	již	k6eAd1	již
16	[number]	k4	16
let	léto	k1gNnPc2	léto
mrtvý	mrtvý	k2eAgMnSc1d1	mrtvý
<g/>
,	,	kIx,	,
když	když	k8xS	když
probíhala	probíhat	k5eAaImAgFnS	probíhat
kauza	kauza	k1gFnSc1	kauza
Mistra	mistr	k1gMnSc2	mistr
Jana	Jan	k1gMnSc2	Jan
Husa	Hus	k1gMnSc2	Hus
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
na	na	k7c6	na
základě	základ	k1gInSc6	základ
pomluvy	pomluva	k1gFnSc2	pomluva
faráře	farář	k1gMnSc2	farář
od	od	k7c2	od
sv.	sv.	kA	sv.
Klimenta	Kliment	k1gMnSc2	Kliment
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
věci	věc	k1gFnSc6	věc
roku	rok	k1gInSc2	rok
1409	[number]	k4	1409
rozhodoval	rozhodovat	k5eAaImAgMnS	rozhodovat
Mařík	Mařík	k1gMnSc1	Mařík
(	(	kIx(	(
<g/>
Mauricius	Mauricius	k1gMnSc1	Mauricius
<g/>
)	)	kIx)	)
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
generální	generální	k2eAgMnSc1d1	generální
převor	převor	k1gMnSc1	převor
řádu	řád	k1gInSc2	řád
křižovníků	křižovník	k1gMnPc2	křižovník
s	s	k7c7	s
červeným	červený	k2eAgNnSc7d1	červené
srdcem	srdce	k1gNnSc7	srdce
-	-	kIx~	-
Cyriaků	Cyriak	k1gMnPc2	Cyriak
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
převor	převor	k1gMnSc1	převor
od	od	k7c2	od
přenesení	přenesení	k1gNnSc2	přenesení
sídla	sídlo	k1gNnSc2	sídlo
řádu	řád	k1gInSc2	řád
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
řečený	řečený	k2eAgInSc1d1	řečený
Rvačka	rvačka	k1gFnSc1	rvačka
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
umístění	umístění	k1gNnSc2	umístění
Cyriaků	Cyriak	k1gInPc2	Cyriak
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
řečeném	řečený	k2eAgNnSc6d1	řečené
Na	na	k7c6	na
Rvačkách	rvačka	k1gFnPc6	rvačka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
bohosloví	bohosloví	k1gNnSc2	bohosloví
a	a	k8xC	a
inkvizitor	inkvizitor	k1gMnSc1	inkvizitor
kacířství	kacířství	k1gNnSc2	kacířství
<g/>
.	.	kIx.	.
</s>
<s>
Mařík	Mařík	k1gMnSc1	Mařík
působil	působit	k5eAaImAgMnS	působit
i	i	k9	i
na	na	k7c6	na
kostnickém	kostnický	k2eAgInSc6d1	kostnický
koncilu	koncil	k1gInSc6	koncil
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1925	[number]	k4	1925
zrušila	zrušit	k5eAaPmAgFnS	zrušit
československá	československý	k2eAgFnSc1d1	Československá
vláda	vláda	k1gFnSc1	vláda
svátek	svátek	k1gInSc1	svátek
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
(	(	kIx(	(
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
květen	květen	k1gInSc4	květen
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
den	den	k1gInSc4	den
pracovního	pracovní	k2eAgInSc2d1	pracovní
klidu	klid	k1gInSc2	klid
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
byl	být	k5eAaImAgInS	být
jeho	jeho	k3xOp3gInSc1	jeho
svátek	svátek	k1gInSc1	svátek
vymazán	vymazat	k5eAaPmNgInS	vymazat
z	z	k7c2	z
občanského	občanský	k2eAgInSc2d1	občanský
kalendáře	kalendář	k1gInSc2	kalendář
zcela	zcela	k6eAd1	zcela
a	a	k8xC	a
nahrazen	nahrazen	k2eAgInSc4d1	nahrazen
Přemyslem	Přemysl	k1gMnSc7	Přemysl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
byl	být	k5eAaImAgInS	být
proveden	provést	k5eAaPmNgInS	provést
antropologický	antropologický	k2eAgInSc1d1	antropologický
průzkum	průzkum	k1gInSc1	průzkum
ostatků	ostatek	k1gInPc2	ostatek
Jana	Jan	k1gMnSc2	Jan
z	z	k7c2	z
Pomuka	Pomuk	k1gMnSc2	Pomuk
(	(	kIx(	(
<g/>
Emanuel	Emanuel	k1gMnSc1	Emanuel
Vlček	Vlček	k1gMnSc1	Vlček
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
potvrzuje	potvrzovat	k5eAaImIp3nS	potvrzovat
jejich	jejich	k3xOp3gFnSc4	jejich
autenticitu	autenticita	k1gFnSc4	autenticita
<g/>
.	.	kIx.	.
</s>
<s>
Tentýž	týž	k3xTgInSc4	týž
výzkum	výzkum	k1gInSc4	výzkum
však	však	k9	však
zároveň	zároveň	k6eAd1	zároveň
potvrzuje	potvrzovat	k5eAaImIp3nS	potvrzovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
domnělý	domnělý	k2eAgMnSc1d1	domnělý
zázračně	zázračně	k6eAd1	zázračně
dochovaný	dochovaný	k2eAgInSc1d1	dochovaný
světcův	světcův	k2eAgInSc1d1	světcův
jazyk	jazyk	k1gInSc1	jazyk
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
pozůstatkem	pozůstatek	k1gInSc7	pozůstatek
mumifikované	mumifikovaný	k2eAgFnSc2d1	mumifikovaná
mozkové	mozkový	k2eAgFnSc2d1	mozková
tkáně	tkáň	k1gFnSc2	tkáň
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
pozůstatek	pozůstatek	k1gInSc1	pozůstatek
Janova	Janův	k2eAgNnSc2d1	Janovo
těla	tělo	k1gNnSc2	tělo
ovšem	ovšem	k9	ovšem
hrál	hrát	k5eAaImAgMnS	hrát
klíčovou	klíčový	k2eAgFnSc4d1	klíčová
roli	role	k1gFnSc4	role
při	při	k7c6	při
kanonizačním	kanonizační	k2eAgInSc6d1	kanonizační
procesu	proces	k1gInSc6	proces
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
(	(	kIx(	(
<g/>
domnělé	domnělý	k2eAgFnSc3d1	domnělá
<g/>
)	)	kIx)	)
dochování	dochování	k1gNnSc1	dochování
jazyka	jazyk	k1gInSc2	jazyk
bylo	být	k5eAaImAgNnS	být
uznáno	uznat	k5eAaPmNgNnS	uznat
jako	jako	k9	jako
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
čtyř	čtyři	k4xCgFnPc2	čtyři
zázraků	zázrak	k1gInPc2	zázrak
potvrzujících	potvrzující	k2eAgFnPc2d1	potvrzující
Janovu	Janův	k2eAgFnSc4d1	Janova
svatost	svatost	k1gFnSc4	svatost
<g/>
.	.	kIx.	.
</s>
<s>
František	František	k1gMnSc1	František
Tomášek	Tomášek	k1gMnSc1	Tomášek
<g/>
,	,	kIx,	,
pozdější	pozdní	k2eAgMnSc1d2	pozdější
pražský	pražský	k2eAgMnSc1d1	pražský
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
se	se	k3xPyFc4	se
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
vyjádřil	vyjádřit	k5eAaPmAgInS	vyjádřit
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
Moderní	moderní	k2eAgFnSc1d1	moderní
věda	věda	k1gFnSc1	věda
zjistila	zjistit	k5eAaPmAgFnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejde	jít	k5eNaImIp3nS	jít
o	o	k7c4	o
zbytky	zbytek	k1gInPc4	zbytek
jazyka	jazyk	k1gInSc2	jazyk
svatého	svatý	k2eAgMnSc2d1	svatý
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
o	o	k7c4	o
zbytek	zbytek	k1gInSc4	zbytek
jeho	jeho	k3xOp3gFnSc2	jeho
mozkové	mozkový	k2eAgFnSc2d1	mozková
tkáně	tkáň	k1gFnSc2	tkáň
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
význam	význam	k1gInSc4	význam
relikvie	relikvie	k1gFnSc1	relikvie
ještě	ještě	k9	ještě
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jazyk	jazyk	k1gInSc1	jazyk
je	být	k5eAaImIp3nS	být
podřízen	podřídit	k5eAaPmNgInS	podřídit
mozku	mozek	k1gInSc2	mozek
člověka	člověk	k1gMnSc4	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Úcta	úcta	k1gFnSc1	úcta
ke	k	k7c3	k
sv.	sv.	kA	sv.
Janu	Jan	k1gMnSc6	Jan
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgMnSc1	první
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
začal	začít	k5eAaPmAgMnS	začít
pěstovat	pěstovat	k5eAaImF	pěstovat
úctu	úcta	k1gFnSc4	úcta
ke	k	k7c3	k
svatému	svatý	k2eAgMnSc3d1	svatý
Janu	Jan	k1gMnSc3	Jan
Nepomuckému	Nepomucký	k1gMnSc3	Nepomucký
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
vynesení	vynesení	k1gNnSc6	vynesení
z	z	k7c2	z
Vltavy	Vltava	k1gFnSc2	Vltava
14	[number]	k4	14
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1393	[number]	k4	1393
Křižovníci	křižovník	k1gMnPc1	křižovník
s	s	k7c7	s
červeným	červený	k2eAgNnSc7d1	červené
srdcem	srdce	k1gNnSc7	srdce
–	–	k?	–
cyriaci	cyriace	k1gFnSc6	cyriace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byl	být	k5eAaImAgInS	být
jejich	jejich	k3xOp3gMnSc7	jejich
generálním	generální	k2eAgMnSc7d1	generální
převorem	převor	k1gMnSc7	převor
Jan	Jan	k1gMnSc1	Jan
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
generální	generální	k2eAgMnSc1d1	generální
převor	převor	k1gMnSc1	převor
od	od	k7c2	od
přenesení	přenesení	k1gNnSc2	přenesení
sídla	sídlo	k1gNnSc2	sídlo
řádu	řád	k1gInSc2	řád
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
–	–	k?	–
generálním	generální	k2eAgMnSc7d1	generální
převorem	převor	k1gMnSc7	převor
1373	[number]	k4	1373
<g/>
–	–	k?	–
<g/>
1403	[number]	k4	1403
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Uložili	uložit	k5eAaPmAgMnP	uložit
Nepomukovo	Nepomukův	k2eAgNnSc4d1	Nepomukův
tělo	tělo	k1gNnSc4	tělo
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
kostele	kostel	k1gInSc6	kostel
sv.	sv.	kA	sv.
Kříže	Kříž	k1gMnSc2	Kříž
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
místě	místo	k1gNnSc6	místo
prvního	první	k4xOgNnSc2	první
pohřbení	pohřbení	k1gNnSc2	pohřbení
později	pozdě	k6eAd2	pozdě
zbudovali	zbudovat	k5eAaPmAgMnP	zbudovat
chrámovou	chrámový	k2eAgFnSc4d1	chrámová
kapli	kaple	k1gFnSc4	kaple
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
úctě	úcta	k1gFnSc3	úcta
<g/>
.	.	kIx.	.
</s>
<s>
Vikář	vikář	k1gMnSc1	vikář
cyriaků	cyriak	k1gMnPc2	cyriak
Kolumbán	Kolumbán	k2eAgInSc4d1	Kolumbán
Grummelmeyer	Grummelmeyer	k1gInSc4	Grummelmeyer
(	(	kIx(	(
<g/>
vikářem	vikář	k1gMnSc7	vikář
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1709	[number]	k4	1709
<g/>
–	–	k?	–
<g/>
1717	[number]	k4	1717
<g/>
)	)	kIx)	)
tuto	tento	k3xDgFnSc4	tento
úctu	úcta	k1gFnSc4	úcta
dále	daleko	k6eAd2	daleko
rozvíjel	rozvíjet	k5eAaImAgInS	rozvíjet
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
nástupce	nástupce	k1gMnSc1	nástupce
Jan	Jan	k1gMnSc1	Jan
Mändl	Mändl	k1gMnSc1	Mändl
ze	z	k7c2	z
Steinfelsu	Steinfels	k1gInSc2	Steinfels
(	(	kIx(	(
<g/>
1717	[number]	k4	1717
<g/>
–	–	k?	–
<g/>
1730	[number]	k4	1730
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgInS	účastnit
příprav	příprava	k1gFnPc2	příprava
svatořečení	svatořečení	k1gNnSc2	svatořečení
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
<g/>
.	.	kIx.	.
</s>
<s>
Probošt	probošt	k1gMnSc1	probošt
Jan	Jan	k1gMnSc1	Jan
M.	M.	kA	M.
Heinfeld	Heinfeld	k1gMnSc1	Heinfeld
dal	dát	k5eAaPmAgMnS	dát
v	v	k7c6	v
Brokofově	Brokofův	k2eAgFnSc6d1	Brokofova
dílně	dílna	k1gFnSc6	dílna
vytesat	vytesat	k5eAaPmF	vytesat
sochu	socha	k1gFnSc4	socha
Jana	Jan	k1gMnSc2	Jan
Nepomuka	Nepomuk	k1gMnSc2	Nepomuk
jako	jako	k8xC	jako
almužníka	almužník	k1gMnSc2	almužník
<g/>
;	;	kIx,	;
socha	socha	k1gFnSc1	socha
stála	stát	k5eAaImAgFnS	stát
nejdříve	dříve	k6eAd3	dříve
na	na	k7c6	na
Svatojanském	svatojanský	k2eAgNnSc6d1	svatojanské
náměstí	náměstí	k1gNnSc6	náměstí
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Náměstí	náměstí	k1gNnSc1	náměstí
Curieových	Curieův	k2eAgFnPc2d1	Curieova
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
po	po	k7c6	po
asanaci	asanace	k1gFnSc6	asanace
Židovského	židovský	k2eAgNnSc2d1	Židovské
Města	město	k1gNnSc2	město
byla	být	k5eAaImAgFnS	být
přenesena	přenést	k5eAaPmNgFnS	přenést
před	před	k7c4	před
kostel	kostel	k1gInSc4	kostel
sv.	sv.	kA	sv.
Ducha	duch	k1gMnSc2	duch
v	v	k7c6	v
Dušní	dušný	k2eAgMnPc1d1	dušný
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Cyriaci	Cyriace	k1gFnSc4	Cyriace
společně	společně	k6eAd1	společně
s	s	k7c7	s
jezuity	jezuita	k1gMnPc7	jezuita
a	a	k8xC	a
s	s	k7c7	s
křižovníky	křižovník	k1gMnPc7	křižovník
s	s	k7c7	s
červeným	červený	k2eAgInSc7d1	červený
křížem	kříž	k1gInSc7	kříž
pořádali	pořádat	k5eAaImAgMnP	pořádat
každoročně	každoročně	k6eAd1	každoročně
večer	večer	k6eAd1	večer
před	před	k7c7	před
Janovým	Janův	k2eAgInSc7d1	Janův
svátkem	svátek	k1gInSc7	svátek
až	až	k6eAd1	až
do	do	k7c2	do
15	[number]	k4	15
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1783	[number]	k4	1783
slavný	slavný	k2eAgInSc1d1	slavný
průvod	průvod	k1gInSc1	průvod
lodí	loď	k1gFnPc2	loď
na	na	k7c6	na
Vltavě	Vltava	k1gFnSc6	Vltava
až	až	k9	až
ke	k	k7c3	k
Karlovu	Karlův	k2eAgInSc3d1	Karlův
mostu	most	k1gInSc3	most
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
u	u	k7c2	u
jeho	jeho	k3xOp3gFnSc2	jeho
sochy	socha	k1gFnSc2	socha
modlili	modlit	k5eAaImAgMnP	modlit
litanie	litanie	k1gFnSc2	litanie
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
těmto	tento	k3xDgFnPc3	tento
oslavám	oslava	k1gFnPc3	oslava
byly	být	k5eAaImAgFnP	být
zkomponovány	zkomponován	k2eAgFnPc1d1	zkomponována
hudební	hudební	k2eAgFnPc1d1	hudební
skladby	skladba	k1gFnPc1	skladba
význačných	význačný	k2eAgMnPc2d1	význačný
pražských	pražský	k2eAgMnPc2d1	pražský
skladatelů	skladatel	k1gMnPc2	skladatel
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Františka	František	k1gMnSc2	František
Xavera	Xaver	k1gMnSc2	Xaver
Brixího	Brixí	k1gMnSc2	Brixí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Slavnosti	slavnost	k1gFnPc1	slavnost
byly	být	k5eAaImAgFnP	být
zakázány	zakázat	k5eAaPmNgFnP	zakázat
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
všechny	všechen	k3xTgFnPc1	všechen
ostatní	ostatní	k2eAgFnPc1d1	ostatní
katolické	katolický	k2eAgFnPc1d1	katolická
pouti	pouť	k1gFnPc1	pouť
dekretem	dekret	k1gInSc7	dekret
císaře	císař	k1gMnSc2	císař
Josefa	Josef	k1gMnSc2	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1783	[number]	k4	1783
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
navazují	navazovat	k5eAaImIp3nP	navazovat
současné	současný	k2eAgFnPc1d1	současná
Svatojánské	svatojánský	k2eAgFnPc1d1	Svatojánská
slavnosti	slavnost	k1gFnPc1	slavnost
Navalis	Navalis	k1gFnSc2	Navalis
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kult	kult	k1gInSc1	kult
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
je	být	k5eAaImIp3nS	být
nerozlučně	rozlučně	k6eNd1	rozlučně
spojen	spojit	k5eAaPmNgInS	spojit
s	s	k7c7	s
českým	český	k2eAgInSc7d1	český
barokem	barok	k1gInSc7	barok
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Jan	Jan	k1gMnSc1	Jan
patřil	patřit	k5eAaImAgMnS	patřit
k	k	k7c3	k
nejvýznamnějším	významný	k2eAgMnPc3d3	nejvýznamnější
světcům	světec	k1gMnPc3	světec
rakouské	rakouský	k2eAgFnSc2d1	rakouská
monarchie	monarchie	k1gFnSc2	monarchie
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zemským	zemský	k2eAgMnSc7d1	zemský
patronem	patron	k1gMnSc7	patron
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
Bavorska	Bavorsko	k1gNnSc2	Bavorsko
<g/>
.	.	kIx.	.
</s>
<s>
Populární	populární	k2eAgMnSc1d1	populární
byl	být	k5eAaImAgInS	být
již	již	k6eAd1	již
od	od	k7c2	od
baroka	baroko	k1gNnSc2	baroko
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
katolické	katolický	k2eAgFnSc6d1	katolická
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
i	i	k8xC	i
v	v	k7c6	v
Belgii	Belgie	k1gFnSc6	Belgie
a	a	k8xC	a
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
Marii	Maria	k1gFnSc3	Maria
Leszczyńské	Leszczyńská	k1gFnSc2	Leszczyńská
také	také	k6eAd1	také
na	na	k7c6	na
francouzském	francouzský	k2eAgInSc6d1	francouzský
královském	královský	k2eAgInSc6d1	královský
dvoře	dvůr	k1gInSc6	dvůr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sv.	sv.	kA	sv.
Jan	Jan	k1gMnSc1	Jan
Nepomucký	Nepomucký	k1gMnSc1	Nepomucký
získal	získat	k5eAaPmAgMnS	získat
díky	díky	k7c3	díky
jezuitským	jezuitský	k2eAgFnPc3d1	jezuitská
misiím	misie	k1gFnPc3	misie
velký	velký	k2eAgInSc4d1	velký
věhlas	věhlas	k1gInSc4	věhlas
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
také	také	k9	také
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
Latinské	latinský	k2eAgFnSc2d1	Latinská
Ameriky	Amerika	k1gFnSc2	Amerika
a	a	k8xC	a
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
uctíván	uctívat	k5eAaImNgMnS	uctívat
jako	jako	k8xS	jako
mučedník	mučedník	k1gMnSc1	mučedník
zpovědního	zpovědní	k2eAgNnSc2d1	zpovědní
tajemství	tajemství	k1gNnSc2	tajemství
a	a	k8xC	a
patron	patrona	k1gFnPc2	patrona
při	při	k7c6	při
přírodních	přírodní	k2eAgFnPc6d1	přírodní
pohromách	pohroma	k1gFnPc6	pohroma
a	a	k8xC	a
povodních	povodeň	k1gFnPc6	povodeň
(	(	kIx(	(
<g/>
kvůli	kvůli	k7c3	kvůli
způsobu	způsob	k1gInSc3	způsob
své	svůj	k3xOyFgFnSc3	svůj
smrti	smrt	k1gFnSc3	smrt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
vyobrazení	vyobrazený	k2eAgMnPc1d1	vyobrazený
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaPmF	nalézt
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
místech	místo	k1gNnPc6	místo
v	v	k7c6	v
kostelech	kostel	k1gInPc6	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Svatojánské	svatojánský	k2eAgFnPc1d1	Svatojánská
sochy	socha	k1gFnPc1	socha
patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
typickým	typický	k2eAgInPc3d1	typický
atributům	atribut	k1gInPc3	atribut
české	český	k2eAgFnSc2d1	Česká
krajiny	krajina	k1gFnSc2	krajina
<g/>
,	,	kIx,	,
především	především	k9	především
na	na	k7c6	na
mostech	most	k1gInPc6	most
(	(	kIx(	(
<g/>
sv.	sv.	kA	sv.
Jan	Jan	k1gMnSc1	Jan
jako	jako	k8xS	jako
patron	patron	k1gMnSc1	patron
vod	voda	k1gFnPc2	voda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
bylo	být	k5eAaImAgNnS	být
roku	rok	k1gInSc3	rok
1929	[number]	k4	1929
pojmenováno	pojmenovat	k5eAaPmNgNnS	pojmenovat
Nepomucenum	Nepomucenum	k1gNnSc1	Nepomucenum
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
papežská	papežský	k2eAgFnSc1d1	Papežská
kolej	kolej	k1gFnSc1	kolej
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
bylo	být	k5eAaImAgNnS	být
znovuotevřeno	znovuotevřít	k5eAaPmNgNnS	znovuotevřít
Svatojánské	svatojánský	k2eAgNnSc1d1	svatojánské
muzeum	muzeum	k1gNnSc1	muzeum
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
rodiště	rodiště	k1gNnSc2	rodiště
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc4	Jan
Nepomuckého	Nepomucký	k1gMnSc4	Nepomucký
<g/>
,	,	kIx,	,
o	o	k7c4	o
jehož	jehož	k3xOyRp3gFnSc4	jehož
obnovu	obnova	k1gFnSc4	obnova
se	se	k3xPyFc4	se
zasloužili	zasloužit	k5eAaPmAgMnP	zasloužit
především	především	k9	především
P.	P.	kA	P.
František	František	k1gMnSc1	František
Cibuzar	Cibuzar	k1gMnSc1	Cibuzar
a	a	k8xC	a
P.	P.	kA	P.
Vítězslav	Vítězslav	k1gMnSc1	Vítězslav
Holý	Holý	k1gMnSc1	Holý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Antropologové	antropolog	k1gMnPc1	antropolog
představili	představit	k5eAaPmAgMnP	představit
19	[number]	k4	19
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2019	[number]	k4	2019
ve	v	k7c6	v
Svatojánském	svatojánský	k2eAgNnSc6d1	svatojánské
muzeu	muzeum	k1gNnSc6	muzeum
v	v	k7c6	v
Nepomuku	Nepomuk	k1gInSc6	Nepomuk
bronzovou	bronzový	k2eAgFnSc4d1	bronzová
plastiku	plastika	k1gFnSc4	plastika
zobrazující	zobrazující	k2eAgFnSc4d1	zobrazující
pravděpodobnou	pravděpodobný	k2eAgFnSc4d1	pravděpodobná
tvář	tvář	k1gFnSc4	tvář
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
<g/>
.	.	kIx.	.
</s>
<s>
Antropologové	antropolog	k1gMnPc1	antropolog
ji	on	k3xPp3gFnSc4	on
rekonstruovali	rekonstruovat	k5eAaBmAgMnP	rekonstruovat
podle	podle	k7c2	podle
modelu	model	k1gInSc2	model
jeho	jeho	k3xOp3gFnSc2	jeho
lebky	lebka	k1gFnSc2	lebka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ikonografie	ikonografie	k1gFnSc2	ikonografie
==	==	k?	==
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
křesťanské	křesťanský	k2eAgFnSc6d1	křesťanská
ikonografii	ikonografie	k1gFnSc6	ikonografie
(	(	kIx(	(
<g/>
tak	tak	k6eAd1	tak
ho	on	k3xPp3gMnSc4	on
zobrazil	zobrazit	k5eAaPmAgInS	zobrazit
poprvé	poprvé	k6eAd1	poprvé
již	již	k9	již
Brokoff	Brokoff	k1gInSc1	Brokoff
<g/>
)	)	kIx)	)
zobrazován	zobrazován	k2eAgMnSc1d1	zobrazován
jako	jako	k8xC	jako
kanovník	kanovník	k1gMnSc1	kanovník
v	v	k7c6	v
chórovém	chórový	k2eAgInSc6d1	chórový
oděvu	oděv	k1gInSc6	oděv
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
v	v	k7c6	v
klerice	klerika	k1gFnSc6	klerika
<g/>
,	,	kIx,	,
rochetě	rocheta	k1gFnSc6	rocheta
<g/>
,	,	kIx,	,
almuci	almuce	k1gFnSc6	almuce
a	a	k8xC	a
s	s	k7c7	s
biretem	biret	k1gInSc7	biret
<g/>
.	.	kIx.	.
</s>
<s>
Janova	Janův	k2eAgFnSc1d1	Janova
svatozář	svatozář	k1gFnSc1	svatozář
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
doplněna	doplnit	k5eAaPmNgFnS	doplnit
pěti	pět	k4xCc7	pět
hvězdami	hvězda	k1gFnPc7	hvězda
<g/>
,	,	kIx,	,
v	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
drží	držet	k5eAaImIp3nS	držet
kříž	kříž	k1gInSc4	kříž
a	a	k8xC	a
též	též	k9	též
palmovou	palmový	k2eAgFnSc4d1	Palmová
ratolest	ratolest	k1gFnSc4	ratolest
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
také	také	k9	také
knihu	kniha	k1gFnSc4	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Palma	palma	k1gFnSc1	palma
je	být	k5eAaImIp3nS	být
symbolem	symbol	k1gInSc7	symbol
vítězství	vítězství	k1gNnSc1	vítězství
<g/>
,	,	kIx,	,
hvězdy	hvězda	k1gFnPc1	hvězda
zastupují	zastupovat	k5eAaImIp3nP	zastupovat
legendu	legenda	k1gFnSc4	legenda
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
níž	jenž	k3xRgFnSc2	jenž
vltavští	vltavský	k2eAgMnPc1d1	vltavský
rybáři	rybář	k1gMnPc1	rybář
nalezli	naleznout	k5eAaPmAgMnP	naleznout
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Janovo	Janův	k2eAgNnSc4d1	Janovo
tělo	tělo	k1gNnSc4	tělo
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
pět	pět	k4xCc4	pět
ran	rána	k1gFnPc2	rána
Kristových	Kristův	k2eAgFnPc2d1	Kristova
či	či	k8xC	či
písmena	písmeno	k1gNnPc4	písmeno
latinského	latinský	k2eAgNnSc2d1	latinské
tacui	tacui	k1gNnSc2	tacui
(	(	kIx(	(
<g/>
mlčel	mlčet	k5eAaImAgMnS	mlčet
jsem	být	k5eAaImIp1nS	být
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nevyzrazení	nevyzrazení	k1gNnSc1	nevyzrazení
zpovědního	zpovědní	k2eAgNnSc2d1	zpovědní
tajemství	tajemství	k1gNnSc2	tajemství
je	být	k5eAaImIp3nS	být
symbolizováno	symbolizovat	k5eAaImNgNnS	symbolizovat
prstem	prst	k1gInSc7	prst
na	na	k7c6	na
ústech	ústa	k1gNnPc6	ústa
<g/>
.	.	kIx.	.
</s>
<s>
Hvězdy	hvězda	k1gFnPc1	hvězda
kolem	kolem	k7c2	kolem
hlavy	hlava	k1gFnSc2	hlava
jsou	být	k5eAaImIp3nP	být
atribut	atribut	k1gInSc4	atribut
mezi	mezi	k7c7	mezi
světci	světec	k1gMnPc7	světec
velmi	velmi	k6eAd1	velmi
zvláštní	zvláštní	k2eAgInSc1d1	zvláštní
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
s	s	k7c7	s
hvězdami	hvězda	k1gFnPc7	hvězda
kolem	kolem	k7c2	kolem
hlavy	hlava	k1gFnSc2	hlava
je	být	k5eAaImIp3nS	být
zobrazována	zobrazovat	k5eAaImNgFnS	zobrazovat
pouze	pouze	k6eAd1	pouze
Panna	Panna	k1gFnSc1	Panna
Mari	Mar	k1gFnSc2	Mar
<g/>
.	.	kIx.	.
</s>
<s>
Jiným	jiný	k2eAgInSc7d1	jiný
atributem	atribut	k1gInSc7	atribut
je	být	k5eAaImIp3nS	být
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
nalezen	nalézt	k5eAaBmNgInS	nalézt
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
Janově	Janův	k2eAgFnSc6d1	Janova
lebce	lebka	k1gFnSc6	lebka
neporušený	porušený	k2eNgInSc1d1	neporušený
<g/>
;	;	kIx,	;
podle	podle	k7c2	podle
nových	nový	k2eAgInPc2d1	nový
výzkumů	výzkum	k1gInPc2	výzkum
se	se	k3xPyFc4	se
však	však	k9	však
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
mozkovou	mozkový	k2eAgFnSc4d1	mozková
tkáň	tkáň	k1gFnSc4	tkáň
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgInPc7d1	další
atributy	atribut	k1gInPc7	atribut
jsou	být	k5eAaImIp3nP	být
Palladium	palladium	k1gNnSc4	palladium
země	zem	k1gFnSc2	zem
české	český	k2eAgFnSc2d1	Česká
ve	v	k7c6	v
Staré	Staré	k2eAgFnSc6d1	Staré
Boleslavi	Boleslaev	k1gFnSc6	Boleslaev
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
barokní	barokní	k2eAgFnSc2d1	barokní
legendy	legenda	k1gFnSc2	legenda
sem	sem	k6eAd1	sem
sv.	sv.	kA	sv.
Jan	Jan	k1gMnSc1	Jan
putoval	putovat	k5eAaImAgMnS	putovat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
most	most	k1gInSc1	most
<g/>
,	,	kIx,	,
kotva	kotva	k1gFnSc1	kotva
či	či	k8xC	či
chuďas	chuďas	k1gMnSc1	chuďas
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgMnSc3	jenž
Jan	Jan	k1gMnSc1	Jan
udílí	udílet	k5eAaImIp3nP	udílet
almužnu	almužna	k1gFnSc4	almužna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hudba	hudba	k1gFnSc1	hudba
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
každoročních	každoroční	k2eAgFnPc2d1	každoroční
oslav	oslava	k1gFnPc2	oslava
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
se	se	k3xPyFc4	se
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
pořádá	pořádat	k5eAaImIp3nS	pořádat
vodní	vodní	k2eAgInSc4d1	vodní
koncert	koncert	k1gInSc4	koncert
barokní	barokní	k2eAgFnSc2d1	barokní
hudby	hudba	k1gFnSc2	hudba
Svatojánské	svatojánský	k2eAgFnSc2d1	Svatojánská
navalis	navalis	k1gFnSc2	navalis
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Gerald	Gerald	k1gMnSc1	Gerald
Spitzner	Spitzner	k1gMnSc1	Spitzner
<g/>
:	:	kIx,	:
Johannes	Johannes	k1gMnSc1	Johannes
Nepomuk	Nepomuk	k1gMnSc1	Nepomuk
Messe	Messe	k1gFnSc2	Messe
in	in	k?	in
G-Major	G-Major	k1gMnSc1	G-Major
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Václav	Václav	k1gMnSc1	Václav
Hájek	Hájek	k1gMnSc1	Hájek
z	z	k7c2	z
Libočan	Libočan	k1gMnSc1	Libočan
<g/>
.	.	kIx.	.
</s>
<s>
Kronika	kronika	k1gFnSc1	kronika
česká	český	k2eAgFnSc1d1	Česká
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1541	[number]	k4	1541
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
BUBEN	buben	k1gInSc1	buben
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
M.	M.	kA	M.
<g/>
;	;	kIx,	;
KUČERA	Kučera	k1gMnSc1	Kučera
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
<g/>
;	;	kIx,	;
KUKLA	Kukla	k1gMnSc1	Kukla
<g/>
,	,	kIx,	,
Otakar	Otakar	k1gMnSc1	Otakar
A.	A.	kA	A.
Svatí	svatý	k1gMnPc1	svatý
spojují	spojovat	k5eAaImIp3nP	spojovat
národy	národ	k1gInPc4	národ
:	:	kIx,	:
portréty	portrét	k1gInPc4	portrét
evropských	evropský	k2eAgMnPc2d1	evropský
světců	světec	k1gMnPc2	světec
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
rozš	rozš	k1gInSc1	rozš
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Panevropa	Panevropa	k1gFnSc1	Panevropa
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
195	[number]	k4	195
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85846	[number]	k4	85846
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PEKAŘ	Pekař	k1gMnSc1	Pekař
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
smyslu	smysl	k1gInSc6	smysl
českých	český	k2eAgFnPc2d1	Česká
dějin	dějiny	k1gFnPc2	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Rozmluvy	rozmluva	k1gFnSc2	rozmluva
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
946352	[number]	k4	946352
<g/>
-	-	kIx~	-
<g/>
70	[number]	k4	70
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
Tři	tři	k4xCgFnPc4	tři
kapitoly	kapitola	k1gFnPc1	kapitola
z	z	k7c2	z
boje	boj	k1gInSc2	boj
o	o	k7c6	o
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
POLC	POLC	kA	POLC
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
V.	V.	kA	V.
Svatý	svatý	k1gMnSc1	svatý
Jan	Jan	k1gMnSc1	Jan
Nepomucký	Nepomucký	k1gMnSc1	Nepomucký
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Zvon	zvon	k1gInSc1	zvon
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7113	[number]	k4	7113
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
71	[number]	k4	71
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
STEJSKAL	Stejskal	k1gMnSc1	Stejskal
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
X.	X.	kA	X.
<g/>
,	,	kIx,	,
Svatý	svatý	k2eAgMnSc1d1	svatý
Jan	Jan	k1gMnSc1	Jan
Nepomucký	Nepomucký	k1gMnSc1	Nepomucký
<g/>
.	.	kIx.	.
2	[number]	k4	2
sv.	sv.	kA	sv.
Praha	Praha	k1gFnSc1	Praha
1921	[number]	k4	1921
<g/>
-	-	kIx~	-
<g/>
22	[number]	k4	22
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VLNAS	VLNAS	kA	VLNAS
<g/>
,	,	kIx,	,
Vít	Vít	k1gMnSc1	Vít
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Nepomucký	Nepomucký	k1gMnSc1	Nepomucký
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
legenda	legenda	k1gFnSc1	legenda
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
282	[number]	k4	282
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
204	[number]	k4	204
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
358	[number]	k4	358
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VACEK	Vacek	k1gMnSc1	Vacek
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Nepomucký	Nepomucký	k1gMnSc1	Nepomucký
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
Ottův	Ottův	k2eAgInSc4d1	Ottův
slovník	slovník	k1gInSc4	slovník
naučný	naučný	k2eAgInSc4d1	naučný
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
J.	J.	kA	J.
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
1897	[number]	k4	1897
<g/>
.	.	kIx.	.
</s>
<s>
Svazek	svazek	k1gInSc1	svazek
XII	XII	kA	XII
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
1055	[number]	k4	1055
<g/>
–	–	k?	–
<g/>
1056	[number]	k4	1056
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
BLAŽÍČEK	Blažíček	k1gMnSc1	Blažíček
<g/>
,	,	kIx,	,
Oldřich	Oldřich	k1gMnSc1	Oldřich
J.	J.	kA	J.
Náhrobek	náhrobek	k1gInSc1	náhrobek
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
<g/>
.	.	kIx.	.
</s>
<s>
Svazek	svazek	k1gInSc1	svazek
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
,	,	kIx,	,
1940	[number]	k4	1940
<g/>
.	.	kIx.	.
13	[number]	k4	13
s.	s.	k?	s.
(	(	kIx(	(
<g/>
Poklady	poklad	k1gInPc1	poklad
národního	národní	k2eAgNnSc2d1	národní
umění	umění	k1gNnSc2	umění
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Nepomuk	Nepomuk	k1gInSc1	Nepomuk
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Sarkander	Sarkander	k1gMnSc1	Sarkander
</s>
</p>
<p>
<s>
seznam	seznam	k1gInSc1	seznam
kostelů	kostel	k1gInPc2	kostel
zasvěcených	zasvěcený	k2eAgInPc2d1	zasvěcený
Janu	Jana	k1gFnSc4	Jana
Nepomuckému	Nepomucký	k2eAgMnSc3d1	Nepomucký
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Nepomuk	Nepomuk	k1gMnSc1	Nepomuk
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Jan	Jan	k1gMnSc1	Jan
Nepomucký	Nepomucký	k2eAgMnSc1d1	Nepomucký
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Jan	Jan	k1gMnSc1	Jan
Nepomucký	Nepomucký	k2eAgMnSc1d1	Nepomucký
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Jan	Jan	k1gMnSc1	Jan
Nepomucký	Nepomucký	k1gMnSc1	Nepomucký
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Jan	Jan	k1gMnSc1	Jan
Nepomucký	Nepomucký	k1gMnSc1	Nepomucký
</s>
</p>
<p>
<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Jan	Jan	k1gMnSc1	Jan
Nepomucký	Nepomucký	k1gMnSc1	Nepomucký
</s>
</p>
<p>
<s>
Literatura	literatura	k1gFnSc1	literatura
o	o	k7c4	o
sv.	sv.	kA	sv.
Janu	Jana	k1gFnSc4	Jana
-	-	kIx~	-
Městská	městský	k2eAgFnSc1d1	městská
knihovna	knihovna	k1gFnSc1	knihovna
Praha	Praha	k1gFnSc1	Praha
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Nepomucký	Nepomucký	k1gMnSc1	Nepomucký
</s>
</p>
<p>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
Jan	Jan	k1gMnSc1	Jan
Nepomucký	Nepomucký	k1gMnSc1	Nepomucký
kvůli	kvůli	k7c3	kvůli
zpovědnímu	zpovědní	k2eAgNnSc3d1	zpovědní
tajemství	tajemství	k1gNnSc3	tajemství
<g/>
?	?	kIx.	?
</s>
</p>
