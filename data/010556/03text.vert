<p>
<s>
Quesito	Quesit	k2eAgNnSc1d1	Quesit
(	(	kIx(	(
<g/>
výslovnost	výslovnost	k1gFnSc1	výslovnost
/	/	kIx~	/
<g/>
kesito	kesit	k2eAgNnSc1d1	kesit
<g/>
/	/	kIx~	/
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
sladké	sladký	k2eAgNnSc4d1	sladké
sýrové	sýrový	k2eAgNnSc4d1	sýrové
pečivo	pečivo	k1gNnSc4	pečivo
z	z	k7c2	z
Portorika	Portorico	k1gNnSc2	Portorico
<g/>
.	.	kIx.	.
</s>
<s>
Základem	základ	k1gInSc7	základ
je	být	k5eAaImIp3nS	být
sladké	sladký	k2eAgNnSc4d1	sladké
listové	listový	k2eAgNnSc4d1	listové
těsto	těsto	k1gNnSc4	těsto
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
něho	on	k3xPp3gNnSc2	on
se	se	k3xPyFc4	se
vytvarují	vytvarovat	k5eAaPmIp3nP	vytvarovat
bochánky	bochánek	k1gInPc1	bochánek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
naplněny	naplněn	k2eAgInPc1d1	naplněn
sýrem	sýr	k1gInSc7	sýr
smíchaným	smíchaný	k2eAgFnPc3d1	smíchaná
s	s	k7c7	s
vanilkou	vanilka	k1gFnSc7	vanilka
<g/>
,	,	kIx,	,
cukrem	cukr	k1gInSc7	cukr
a	a	k8xC	a
vejci	vejce	k1gNnPc7	vejce
<g/>
.	.	kIx.	.
</s>
<s>
Sýrová	sýrový	k2eAgFnSc1d1	sýrová
směs	směs	k1gFnSc1	směs
též	též	k9	též
může	moct	k5eAaImIp3nS	moct
obsahovat	obsahovat	k5eAaImF	obsahovat
tropické	tropický	k2eAgNnSc4d1	tropické
ovoce	ovoce	k1gNnSc4	ovoce
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
kvajávu	kvajávat	k5eAaPmIp1nS	kvajávat
a	a	k8xC	a
papáju	papája	k1gFnSc4	papája
<g/>
.	.	kIx.	.
</s>
<s>
Pečivo	pečivo	k1gNnSc1	pečivo
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
obaleno	obalit	k5eAaPmNgNnS	obalit
v	v	k7c6	v
karamelu	karamel	k1gInSc6	karamel
a	a	k8xC	a
pečeno	pečen	k2eAgNnSc1d1	pečeno
<g/>
.	.	kIx.	.
</s>
<s>
Quesita	Quesitum	k1gNnPc1	Quesitum
jsou	být	k5eAaImIp3nP	být
nejčastěji	často	k6eAd3	často
prodávána	prodávat	k5eAaImNgFnS	prodávat
v	v	k7c6	v
pekařstvích	pekařství	k1gNnPc6	pekařství
a	a	k8xC	a
ve	v	k7c6	v
speciálních	speciální	k2eAgInPc6d1	speciální
obchodech	obchod	k1gInPc6	obchod
nazývaných	nazývaný	k2eAgInPc6d1	nazývaný
"	"	kIx"	"
<g/>
bomboneras	bombonerasa	k1gFnPc2	bombonerasa
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Quesito	Quesit	k2eAgNnSc1d1	Quesit
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
