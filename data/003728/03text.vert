<s>
Robert	Robert	k1gMnSc1	Robert
Fico	Fico	k1gMnSc1	Fico
(	(	kIx(	(
<g/>
*	*	kIx~	*
15	[number]	k4	15
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1964	[number]	k4	1964
Topoľčany	Topoľčan	k1gMnPc4	Topoľčan
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
slovenský	slovenský	k2eAgMnSc1d1	slovenský
politik	politik	k1gMnSc1	politik
a	a	k8xC	a
současný	současný	k2eAgMnSc1d1	současný
předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
SR	SR	kA	SR
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
funkci	funkce	k1gFnSc4	funkce
již	již	k6eAd1	již
vykonával	vykonávat	k5eAaImAgMnS	vykonávat
v	v	k7c6	v
letech	let	k1gInPc6	let
2006	[number]	k4	2006
<g/>
–	–	k?	–
<g/>
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zakladatelem	zakladatel	k1gMnSc7	zakladatel
strany	strana	k1gFnSc2	strana
SMER	SMER	kA	SMER
–	–	k?	–
sociálna	sociálno	k1gNnSc2	sociálno
demokracia	demokracia	k1gFnSc1	demokracia
a	a	k8xC	a
jejím	její	k3xOp3gNnSc7	její
prvním	první	k4xOgMnSc7	první
předsedou	předseda	k1gMnSc7	předseda
<g/>
.	.	kIx.	.
</s>
<s>
Působil	působit	k5eAaImAgMnS	působit
i	i	k9	i
jako	jako	k9	jako
vysokoškolský	vysokoškolský	k2eAgMnSc1d1	vysokoškolský
učitel	učitel	k1gMnSc1	učitel
na	na	k7c6	na
Právnické	právnický	k2eAgFnSc6d1	právnická
fakultě	fakulta	k1gFnSc6	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Komenského	Komenský	k1gMnSc2	Komenský
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
habilitoval	habilitovat	k5eAaBmAgMnS	habilitovat
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
docenturu	docentura	k1gFnSc4	docentura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
oznámil	oznámit	k5eAaPmAgMnS	oznámit
kandidaturu	kandidatura	k1gFnSc4	kandidatura
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
2014	[number]	k4	2014
byl	být	k5eAaImAgMnS	být
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
kole	kolo	k1gNnSc6	kolo
poražen	poražen	k2eAgInSc4d1	poražen
Andrejem	Andrej	k1gMnSc7	Andrej
Kiskou	Kiska	k1gMnSc7	Kiska
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2014	[number]	k4	2014
mu	on	k3xPp3gMnSc3	on
český	český	k2eAgMnSc1d1	český
prezident	prezident	k1gMnSc1	prezident
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
udělil	udělit	k5eAaPmAgMnS	udělit
Řád	řád	k1gInSc4	řád
Bílého	bílý	k2eAgInSc2d1	bílý
lva	lev	k1gInSc2	lev
civilní	civilní	k2eAgFnSc2d1	civilní
skupiny	skupina	k1gFnSc2	skupina
I.	I.	kA	I.
třídy	třída	k1gFnSc2	třída
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Topoľčanech	Topoľčan	k1gMnPc6	Topoľčan
jako	jako	k8xS	jako
druhý	druhý	k4xOgMnSc1	druhý
syn	syn	k1gMnSc1	syn
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
Ľudovíta	Ľudovít	k1gMnSc2	Ľudovít
Fica	Fico	k1gMnSc2	Fico
<g/>
,	,	kIx,	,
dělníka	dělník	k1gMnSc2	dělník
na	na	k7c6	na
vysokozdvižném	vysokozdvižný	k2eAgInSc6d1	vysokozdvižný
vozíku	vozík	k1gInSc6	vozík
<g/>
,	,	kIx,	,
a	a	k8xC	a
matky	matka	k1gFnPc1	matka
Emílie	Emílie	k1gFnSc2	Emílie
Ficové	Ficová	k1gFnSc2	Ficová
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
pracovala	pracovat	k5eAaImAgFnS	pracovat
jako	jako	k9	jako
prodavačka	prodavačka	k1gFnSc1	prodavačka
obuvi	obuv	k1gFnSc2	obuv
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
staršího	starý	k2eAgMnSc4d2	starší
bratra	bratr	k1gMnSc4	bratr
Ing.	ing.	kA	ing.
Ladislava	Ladislav	k1gMnSc4	Ladislav
Fica	Fico	k1gMnSc4	Fico
<g/>
,	,	kIx,	,
podnikatele	podnikatel	k1gMnSc4	podnikatel
ve	v	k7c6	v
stavebnictví	stavebnictví	k1gNnSc6	stavebnictví
<g/>
,	,	kIx,	,
a	a	k8xC	a
o	o	k7c4	o
čtrnáct	čtrnáct	k4xCc4	čtrnáct
let	léto	k1gNnPc2	léto
mladší	mladý	k2eAgFnSc4d2	mladší
sestru	sestra	k1gFnSc4	sestra
Lucii	Lucie	k1gFnSc4	Lucie
Chabadovou	Chabadový	k2eAgFnSc7d1	Chabadová
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
působí	působit	k5eAaImIp3nS	působit
jako	jako	k9	jako
právnička	právnička	k1gFnSc1	právnička
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
věku	věk	k1gInSc6	věk
šesti	šest	k4xCc2	šest
let	léto	k1gNnPc2	léto
žil	žít	k5eAaImAgMnS	žít
i	i	k9	i
s	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Hrušovany	Hrušovany	k1gInPc4	Hrušovany
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
přestěhovali	přestěhovat	k5eAaPmAgMnP	přestěhovat
do	do	k7c2	do
Topoľčan	Topoľčan	k1gMnSc1	Topoľčan
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
manželkou	manželka	k1gFnSc7	manželka
Svetlanou	Svetlaný	k2eAgFnSc7d1	Svetlaný
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
právnička	právnička	k1gFnSc1	právnička
a	a	k8xC	a
vysokoškolská	vysokoškolský	k2eAgFnSc1d1	vysokoškolská
pedagožka	pedagožka	k1gFnSc1	pedagožka
(	(	kIx(	(
<g/>
profesorka	profesorka	k1gFnSc1	profesorka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
během	během	k7c2	během
studií	studie	k1gFnPc2	studie
práv	právo	k1gNnPc2	právo
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
spolu	spolu	k6eAd1	spolu
syna	syn	k1gMnSc4	syn
jménem	jméno	k1gNnSc7	jméno
Michal	Michal	k1gMnSc1	Michal
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
Právnickou	právnický	k2eAgFnSc4d1	právnická
fakultu	fakulta	k1gFnSc4	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Komenského	Komenský	k1gMnSc2	Komenský
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
titul	titul	k1gInSc4	titul
JUDr.	JUDr.	kA	JUDr.
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
získal	získat	k5eAaPmAgInS	získat
titul	titul	k1gInSc1	titul
CSc.	CSc.	kA	CSc.
Od	od	k7c2	od
absolvování	absolvování	k1gNnSc2	absolvování
školy	škola	k1gFnSc2	škola
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
pracoval	pracovat	k5eAaImAgMnS	pracovat
na	na	k7c6	na
Právnickém	právnický	k2eAgInSc6d1	právnický
institutu	institut	k1gInSc6	institut
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
SR	SR	kA	SR
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1994-2000	[number]	k4	1994-2000
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
agent	agent	k1gMnSc1	agent
pro	pro	k7c4	pro
zastupování	zastupování	k1gNnSc4	zastupování
SR	SR	kA	SR
v	v	k7c6	v
řízení	řízení	k1gNnSc6	řízení
před	před	k7c7	před
Evropskou	evropský	k2eAgFnSc7d1	Evropská
komisí	komise	k1gFnSc7	komise
pro	pro	k7c4	pro
lidská	lidský	k2eAgNnPc4d1	lidské
práva	právo	k1gNnPc4	právo
a	a	k8xC	a
Evropským	evropský	k2eAgInSc7d1	evropský
soudem	soud	k1gInSc7	soud
pro	pro	k7c4	pro
lidská	lidský	k2eAgNnPc4d1	lidské
práva	právo	k1gNnPc4	právo
<g/>
.	.	kIx.	.
14	[number]	k4	14
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1987	[number]	k4	1987
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
Komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
Slovenska	Slovensko	k1gNnSc2	Slovensko
(	(	kIx(	(
<g/>
KSS	KSS	kA	KSS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yRgFnSc2	který
byl	být	k5eAaImAgInS	být
vybrán	vybrat	k5eAaPmNgMnS	vybrat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
výborných	výborný	k2eAgInPc2d1	výborný
studijních	studijní	k2eAgInPc2d1	studijní
výsledků	výsledek	k1gInPc2	výsledek
<g/>
,	,	kIx,	,
ambicí	ambice	k1gFnPc2	ambice
i	i	k8xC	i
vhodného	vhodný	k2eAgInSc2d1	vhodný
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
byl	být	k5eAaImAgMnS	být
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
zvolen	zvolit	k5eAaPmNgInS	zvolit
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
poslance	poslanec	k1gMnSc2	poslanec
za	za	k7c4	za
Stranu	strana	k1gFnSc4	strana
demokratické	demokratický	k2eAgFnSc2d1	demokratická
levice	levice	k1gFnSc2	levice
(	(	kIx(	(
<g/>
SDĽ	SDĽ	kA	SDĽ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
po	po	k7c6	po
listopadu	listopad	k1gInSc6	listopad
1989	[number]	k4	1989
přejmenováním	přejmenování	k1gNnSc7	přejmenování
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
KSS	KSS	kA	KSS
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
byl	být	k5eAaImAgInS	být
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
poslancem	poslanec	k1gMnSc7	poslanec
Národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgInPc6	který
SDĽ	SDĽ	kA	SDĽ
ztratila	ztratit	k5eAaPmAgFnS	ztratit
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
podle	podle	k7c2	podle
průzkumů	průzkum	k1gInPc2	průzkum
očekávaných	očekávaný	k2eAgInPc2d1	očekávaný
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
odstoupil	odstoupit	k5eAaPmAgMnS	odstoupit
z	z	k7c2	z
postu	post	k1gInSc2	post
předsedy	předseda	k1gMnSc2	předseda
strany	strana	k1gFnSc2	strana
Peter	Peter	k1gMnSc1	Peter
Weiss	Weiss	k1gMnSc1	Weiss
a	a	k8xC	a
za	za	k7c4	za
svého	svůj	k3xOyFgMnSc4	svůj
nástupce	nástupce	k1gMnSc4	nástupce
následně	následně	k6eAd1	následně
označil	označit	k5eAaPmAgMnS	označit
Roberta	Robert	k1gMnSc4	Robert
Fica	Fico	k1gMnSc4	Fico
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
hodin	hodina	k1gFnPc2	hodina
před	před	k7c7	před
začátkem	začátek	k1gInSc7	začátek
sjezdu	sjezd	k1gInSc2	sjezd
však	však	k9	však
Fico	Fico	k1gMnSc1	Fico
kandidaturu	kandidatura	k1gFnSc4	kandidatura
stáhl	stáhnout	k5eAaPmAgMnS	stáhnout
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
Ľubomíra	Ľubomír	k1gMnSc2	Ľubomír
Fogaše	Fogaše	k1gFnSc2	Fogaše
<g/>
,	,	kIx,	,
jeho	on	k3xPp3gMnSc2	on
bývalého	bývalý	k2eAgMnSc2d1	bývalý
kolegy	kolega	k1gMnSc2	kolega
z	z	k7c2	z
Právnické	právnický	k2eAgFnSc2d1	právnická
fakulty	fakulta	k1gFnSc2	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Komenského	Komenský	k1gMnSc2	Komenský
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
vznikala	vznikat	k5eAaImAgFnS	vznikat
široká	široký	k2eAgFnSc1d1	široká
koalice	koalice	k1gFnSc1	koalice
stran	strana	k1gFnPc2	strana
SDK	SDK	kA	SDK
<g/>
,	,	kIx,	,
SDĽ	SDĽ	kA	SDĽ
<g/>
,	,	kIx,	,
SMK	SMK	kA	SMK
a	a	k8xC	a
SOP	sopit	k5eAaImRp2nS	sopit
<g/>
.	.	kIx.	.
</s>
<s>
Robert	Robert	k1gMnSc1	Robert
Fico	Fico	k1gMnSc1	Fico
<g/>
,	,	kIx,	,
jakožto	jakožto	k8xS	jakožto
člen	člen	k1gInSc1	člen
vládní	vládní	k2eAgInSc1d1	vládní
SDĽ	SDĽ	kA	SDĽ
začal	začít	k5eAaPmAgInS	začít
proti	proti	k7c3	proti
této	tento	k3xDgFnSc3	tento
koalici	koalice	k1gFnSc3	koalice
vystupovat	vystupovat	k5eAaImF	vystupovat
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
útočil	útočit	k5eAaImAgMnS	útočit
na	na	k7c6	na
spolupráci	spolupráce	k1gFnSc6	spolupráce
se	s	k7c7	s
Stranou	strana	k1gFnSc7	strana
maďarské	maďarský	k2eAgFnSc2d1	maďarská
koalice	koalice	k1gFnSc2	koalice
(	(	kIx(	(
<g/>
SMK	SMK	kA	SMK
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
když	když	k8xS	když
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
strana	strana	k1gFnSc1	strana
pokouší	pokoušet	k5eAaImIp3nS	pokoušet
otvírat	otvírat	k5eAaImF	otvírat
tzv.	tzv.	kA	tzv.
Benešovy	Benešův	k2eAgInPc4d1	Benešův
dekrety	dekret	k1gInPc4	dekret
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byl	být	k5eAaImAgMnS	být
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
straně	strana	k1gFnSc6	strana
nejpopulárnějším	populární	k2eAgNnSc7d3	nejpopulárnější
politikem	politikum	k1gNnSc7	politikum
a	a	k8xC	a
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
získal	získat	k5eAaPmAgInS	získat
rovněž	rovněž	k6eAd1	rovněž
nejvíce	hodně	k6eAd3	hodně
preferenčních	preferenční	k2eAgInPc2d1	preferenční
hlasů	hlas	k1gInPc2	hlas
z	z	k7c2	z
politiků	politik	k1gMnPc2	politik
SDĽ	SDĽ	kA	SDĽ
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
neshodám	neshoda	k1gFnPc3	neshoda
stranu	strana	k1gFnSc4	strana
však	však	k9	však
následně	následně	k6eAd1	následně
opustil	opustit	k5eAaPmAgMnS	opustit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1999	[number]	k4	1999
založil	založit	k5eAaPmAgMnS	založit
novou	nový	k2eAgFnSc4d1	nová
politickou	politický	k2eAgFnSc4d1	politická
stranu	strana	k1gFnSc4	strana
s	s	k7c7	s
názvem	název	k1gInSc7	název
SMER	SMER	kA	SMER
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
SMĚR	směr	k1gInSc1	směr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
založené	založený	k2eAgInPc1d1	založený
začal	začít	k5eAaPmAgMnS	začít
SMER	SMER	kA	SMER
působit	působit	k5eAaImF	působit
jako	jako	k9	jako
alternativa	alternativa	k1gFnSc1	alternativa
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
vůči	vůči	k7c3	vůči
tehdejší	tehdejší	k2eAgFnSc3d1	tehdejší
vládní	vládní	k2eAgFnSc3d1	vládní
koalici	koalice	k1gFnSc3	koalice
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
Dzurindy	Dzurinda	k1gFnSc2	Dzurinda
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
vůči	vůči	k7c3	vůči
opozici	opozice	k1gFnSc3	opozice
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
v	v	k7c6	v
témže	týž	k3xTgNnSc6	týž
volebním	volební	k2eAgNnSc6d1	volební
období	období	k1gNnSc6	období
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
-	-	kIx~	-
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
přijala	přijmout	k5eAaPmAgFnS	přijmout
strana	strana	k1gFnSc1	strana
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
názvu	název	k1gInSc2	název
přívlastek	přívlastek	k1gInSc1	přívlastek
"	"	kIx"	"
<g/>
třetí	třetí	k4xOgFnSc1	třetí
cesta	cesta	k1gFnSc1	cesta
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tvrzení	tvrzení	k1gNnPc2	tvrzení
představitelů	představitel	k1gMnPc2	představitel
strany	strana	k1gFnSc2	strana
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
definovala	definovat	k5eAaBmAgFnS	definovat
jako	jako	k8xS	jako
strana	strana	k1gFnSc1	strana
moderního	moderní	k2eAgInSc2d1	moderní
progresivního	progresivní	k2eAgInSc2d1	progresivní
středolevého	středolevý	k2eAgInSc2d1	středolevý
politického	politický	k2eAgInSc2d1	politický
proudu	proud	k1gInSc2	proud
typu	typ	k1gInSc2	typ
britských	britský	k2eAgMnPc2d1	britský
labouristů	labourista	k1gMnPc2	labourista
nebo	nebo	k8xC	nebo
německé	německý	k2eAgFnSc2d1	německá
SPD	SPD	kA	SPD
<g/>
.	.	kIx.	.
</s>
<s>
Preference	preference	k1gFnSc1	preference
strany	strana	k1gFnSc2	strana
postupně	postupně	k6eAd1	postupně
rostly	růst	k5eAaImAgFnP	růst
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
preference	preference	k1gFnPc1	preference
SDĽ	SDĽ	kA	SDĽ
klesaly	klesat	k5eAaImAgFnP	klesat
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
vládní	vládní	k2eAgFnSc1d1	vládní
strana	strana	k1gFnSc1	strana
SDĽ	SDĽ	kA	SDĽ
se	se	k3xPyFc4	se
v	v	k7c6	v
následujících	následující	k2eAgFnPc6d1	následující
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
parlamentu	parlament	k1gInSc2	parlament
nedostala	dostat	k5eNaPmAgFnS	dostat
<g/>
,	,	kIx,	,
a	a	k8xC	a
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc6	leden
2005	[number]	k4	2005
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
<g/>
,	,	kIx,	,
když	když	k8xS	když
byla	být	k5eAaImAgFnS	být
pohlcena	pohltit	k5eAaPmNgFnS	pohltit
právě	právě	k9	právě
SMERem	SMER	k1gInSc7	SMER
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
volbách	volba	k1gFnPc6	volba
byl	být	k5eAaImAgMnS	být
Fico	Fico	k1gMnSc1	Fico
opět	opět	k6eAd1	opět
zvolen	zvolit	k5eAaPmNgMnS	zvolit
poslancem	poslanec	k1gMnSc7	poslanec
NR	NR	kA	NR
SR	SR	kA	SR
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
členem	člen	k1gMnSc7	člen
Výboru	výbor	k1gInSc2	výbor
NR	NR	kA	NR
SR	SR	kA	SR
pro	pro	k7c4	pro
lidská	lidský	k2eAgNnPc4d1	lidské
práva	právo	k1gNnPc4	právo
<g/>
,	,	kIx,	,
národnosti	národnost	k1gFnPc4	národnost
a	a	k8xC	a
postavení	postavení	k1gNnSc4	postavení
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Strana	strana	k1gFnSc1	strana
SMER	SMER	kA	SMER
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
se	s	k7c7	s
ziskem	zisk	k1gInSc7	zisk
13,6	[number]	k4	13,6
<g/>
%	%	kIx~	%
voličských	voličský	k2eAgInPc2d1	voličský
hlasů	hlas	k1gInPc2	hlas
třetí	třetí	k4xOgFnSc7	třetí
nejsilnější	silný	k2eAgFnSc7d3	nejsilnější
politickou	politický	k2eAgFnSc7d1	politická
stranou	strana	k1gFnSc7	strana
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
,	,	kIx,	,
za	za	k7c4	za
ĽS-HZDS	ĽS-HZDS	k1gFnSc4	ĽS-HZDS
a	a	k8xC	a
SDKÚ-DS	SDKÚ-DS	k1gFnSc4	SDKÚ-DS
a	a	k8xC	a
obsadila	obsadit	k5eAaPmAgFnS	obsadit
tak	tak	k6eAd1	tak
v	v	k7c6	v
Národní	národní	k2eAgFnSc6d1	národní
radě	rada	k1gFnSc3	rada
25	[number]	k4	25
poslaneckých	poslanecký	k2eAgNnPc2d1	poslanecké
křesel	křeslo	k1gNnPc2	křeslo
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
průzkumy	průzkum	k1gInPc1	průzkum
veřejného	veřejný	k2eAgNnSc2d1	veřejné
mínění	mínění	k1gNnSc2	mínění
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
předpovídaly	předpovídat	k5eAaImAgFnP	předpovídat
pro	pro	k7c4	pro
stranu	strana	k1gFnSc4	strana
lepší	dobrý	k2eAgInSc4d2	lepší
výsledek	výsledek	k1gInSc4	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
SMERu	SMERu	k5eAaPmIp1nS	SMERu
se	se	k3xPyFc4	se
po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
vlády	vláda	k1gFnSc2	vláda
dostat	dostat	k5eAaPmF	dostat
nepodařilo	podařit	k5eNaPmAgNnS	podařit
<g/>
,	,	kIx,	,
tu	tu	k6eAd1	tu
nakonec	nakonec	k6eAd1	nakonec
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
opětovně	opětovně	k6eAd1	opětovně
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
Dzurinda	Dzurind	k1gMnSc2	Dzurind
z	z	k7c2	z
SDKÚ-DS	SDKÚ-DS	k1gFnPc2	SDKÚ-DS
(	(	kIx(	(
<g/>
druhá	druhý	k4xOgFnSc1	druhý
vláda	vláda	k1gFnSc1	vláda
Mikuláše	mikuláš	k1gInSc2	mikuláš
Dzurindy	Dzurinda	k1gFnSc2	Dzurinda
<g/>
)	)	kIx)	)
a	a	k8xC	a
SMER	SMER	kA	SMER
zůstal	zůstat	k5eAaPmAgInS	zůstat
v	v	k7c6	v
opozici	opozice	k1gFnSc6	opozice
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
celého	celý	k2eAgNnSc2d1	celé
volebního	volební	k2eAgNnSc2d1	volební
období	období	k1gNnSc2	období
soupeřil	soupeřit	k5eAaImAgInS	soupeřit
SMER	SMER	kA	SMER
s	s	k7c7	s
ĽS-HZDS	ĽS-HZDS	k1gFnSc7	ĽS-HZDS
o	o	k7c4	o
vedoucí	vedoucí	k2eAgNnSc4d1	vedoucí
postavení	postavení	k1gNnSc4	postavení
v	v	k7c6	v
opozici	opozice	k1gFnSc6	opozice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průzkumech	průzkum	k1gInPc6	průzkum
veřejného	veřejný	k2eAgNnSc2d1	veřejné
mínění	mínění	k1gNnSc2	mínění
následně	následně	k6eAd1	následně
SMER	SMER	kA	SMER
dosud	dosud	k6eAd1	dosud
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
nejpopulárnější	populární	k2eAgFnSc4d3	nejpopulárnější
stranu	strana	k1gFnSc4	strana
ĽS-HZDS	ĽS-HZDS	k1gFnSc2	ĽS-HZDS
předstihl	předstihnout	k5eAaPmAgMnS	předstihnout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
se	se	k3xPyFc4	se
Ficovi	Fic	k1gMnSc3	Fic
podařilo	podařit	k5eAaPmAgNnS	podařit
uskutečnit	uskutečnit	k5eAaPmF	uskutečnit
projekt	projekt	k1gInSc4	projekt
sjednocení	sjednocení	k1gNnSc2	sjednocení
levicových	levicový	k2eAgFnPc2d1	levicová
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgFnPc1	tři
levicové	levicový	k2eAgFnPc1d1	levicová
strany	strana	k1gFnPc1	strana
s	s	k7c7	s
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
zanedbatelnými	zanedbatelný	k2eAgFnPc7d1	zanedbatelná
volebními	volební	k2eAgFnPc7d1	volební
preferencemi	preference	k1gFnPc7	preference
Strana	strana	k1gFnSc1	strana
demokratické	demokratický	k2eAgFnSc2d1	demokratická
levice	levice	k1gFnSc2	levice
(	(	kIx(	(
<g/>
SDĽ	SDĽ	kA	SDĽ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Sociálně	sociálně	k6eAd1	sociálně
demokratická	demokratický	k2eAgFnSc1d1	demokratická
alternativa	alternativa	k1gFnSc1	alternativa
(	(	kIx(	(
<g/>
SDA	SDA	kA	SDA
<g/>
)	)	kIx)	)
a	a	k8xC	a
Sociálně	sociálně	k6eAd1	sociálně
demokratická	demokratický	k2eAgFnSc1d1	demokratická
strana	strana	k1gFnSc1	strana
Slovenska	Slovensko	k1gNnSc2	Slovensko
(	(	kIx(	(
<g/>
SDSS	SDSS	kA	SDSS
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
dohodly	dohodnout	k5eAaPmAgFnP	dohodnout
na	na	k7c4	na
společné	společný	k2eAgInPc4d1	společný
integrací	integrace	k1gFnSc7	integrace
se	s	k7c7	s
SMERem	SMER	k1gInSc7	SMER
<g/>
.	.	kIx.	.
</s>
<s>
Integraci	integrace	k1gFnSc3	integrace
schválily	schválit	k5eAaPmAgInP	schválit
sněmy	sněm	k1gInPc1	sněm
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
stran	strana	k1gFnPc2	strana
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
a	a	k8xC	a
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2005	[number]	k4	2005
strany	strana	k1gFnSc2	strana
SDĽ	SDĽ	kA	SDĽ
<g/>
,	,	kIx,	,
SDA	SDA	kA	SDA
a	a	k8xC	a
SDSS	SDSS	kA	SDSS
zanikly	zaniknout	k5eAaPmAgFnP	zaniknout
<g/>
.	.	kIx.	.
</s>
<s>
SMER	SMER	kA	SMER
si	se	k3xPyFc3	se
následně	následně	k6eAd1	následně
změnil	změnit	k5eAaPmAgMnS	změnit
název	název	k1gInSc4	název
na	na	k7c4	na
současný	současný	k2eAgInSc4d1	současný
"	"	kIx"	"
<g/>
SMER	SMER	kA	SMER
-	-	kIx~	-
sociálna	sociálno	k1gNnSc2	sociálno
demokracia	demokracium	k1gNnSc2	demokracium
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
"	"	kIx"	"
<g/>
SMĚR	směr	k1gInSc1	směr
-	-	kIx~	-
sociální	sociální	k2eAgFnSc1d1	sociální
demokracie	demokracie	k1gFnSc1	demokracie
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Významný	významný	k2eAgInSc1d1	významný
politický	politický	k2eAgInSc1d1	politický
zisk	zisk	k1gInSc1	zisk
pro	pro	k7c4	pro
SMER	SMER	kA	SMER
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
integrace	integrace	k1gFnSc2	integrace
byl	být	k5eAaImAgInS	být
zisku	zisk	k1gInSc2	zisk
nových	nový	k2eAgMnPc2d1	nový
voličů	volič	k1gMnPc2	volič
a	a	k8xC	a
získání	získání	k1gNnSc4	získání
značky	značka	k1gFnSc2	značka
sociální	sociální	k2eAgFnSc2d1	sociální
demokracie	demokracie	k1gFnSc2	demokracie
<g/>
.	.	kIx.	.
</s>
<s>
Strana	strana	k1gFnSc1	strana
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
stala	stát	k5eAaPmAgFnS	stát
členem	člen	k1gMnSc7	člen
Strany	strana	k1gFnSc2	strana
evropských	evropský	k2eAgInPc2d1	evropský
socialistů	socialist	k1gMnPc2	socialist
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Parlamentní	parlamentní	k2eAgFnSc2d1	parlamentní
volby	volba	k1gFnSc2	volba
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
2006	[number]	k4	2006
a	a	k8xC	a
První	první	k4xOgFnSc1	první
vláda	vláda	k1gFnSc1	vláda
Roberta	Robert	k1gMnSc4	Robert
Fica	Fico	k1gMnSc4	Fico
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2006	[number]	k4	2006
SMER-SD	SMER-SD	k1gMnSc1	SMER-SD
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
parlamentní	parlamentní	k2eAgFnPc4d1	parlamentní
volby	volba	k1gFnPc4	volba
se	s	k7c7	s
ziskem	zisk	k1gInSc7	zisk
29,1	[number]	k4	29,1
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
a	a	k8xC	a
utvořil	utvořit	k5eAaPmAgMnS	utvořit
vládní	vládní	k2eAgFnSc4d1	vládní
koalici	koalice	k1gFnSc4	koalice
s	s	k7c7	s
ĽS-HZDS	ĽS-HZDS	k1gFnSc7	ĽS-HZDS
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Mečiara	Mečiar	k1gMnSc2	Mečiar
a	a	k8xC	a
SNS	SNS	kA	SNS
Jána	Ján	k1gMnSc2	Ján
Sloty	slota	k1gFnSc2	slota
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
zmiňované	zmiňovaný	k2eAgFnPc1d1	zmiňovaná
strany	strana	k1gFnPc1	strana
tvořily	tvořit	k5eAaImAgFnP	tvořit
v	v	k7c6	v
období	období	k1gNnSc6	období
1994	[number]	k4	1994
<g/>
–	–	k?	–
<g/>
1998	[number]	k4	1998
koaliční	koaliční	k2eAgFnSc4d1	koaliční
vládu	vláda	k1gFnSc4	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
měla	mít	k5eAaImAgFnS	mít
16	[number]	k4	16
členů	člen	k1gMnPc2	člen
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
11	[number]	k4	11
získal	získat	k5eAaPmAgMnS	získat
SMER-SD	SMER-SD	k1gMnSc1	SMER-SD
<g/>
,	,	kIx,	,
3	[number]	k4	3
SNS	SNS	kA	SNS
a	a	k8xC	a
2	[number]	k4	2
ĽS-HZDS	ĽS-HZDS	k1gFnPc2	ĽS-HZDS
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vládě	vláda	k1gFnSc6	vláda
zastával	zastávat	k5eAaImAgMnS	zastávat
post	post	k1gInSc4	post
jejího	její	k3xOp3gMnSc2	její
předsedy	předseda	k1gMnSc2	předseda
a	a	k8xC	a
v	v	k7c6	v
krátkém	krátký	k2eAgNnSc6d1	krátké
období	období	k1gNnSc6	období
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
června	červen	k1gInSc2	červen
a	a	k8xC	a
července	červenec	k1gInSc2	červenec
2009	[number]	k4	2009
byl	být	k5eAaImAgInS	být
rovněž	rovněž	k9	rovněž
dočasně	dočasně	k6eAd1	dočasně
pověřeným	pověřený	k2eAgMnSc7d1	pověřený
ministrem	ministr	k1gMnSc7	ministr
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Parlamentní	parlamentní	k2eAgFnSc2d1	parlamentní
volby	volba	k1gFnSc2	volba
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2010	[number]	k4	2010
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
strana	strana	k1gFnSc1	strana
SMER-SD	SMER-SD	k1gFnSc1	SMER-SD
se	s	k7c7	s
ziskem	zisk	k1gInSc7	zisk
34,79	[number]	k4	34,79
%	%	kIx~	%
(	(	kIx(	(
<g/>
oproti	oproti	k7c3	oproti
minulým	minulý	k2eAgFnPc3d1	minulá
volbám	volba	k1gFnPc3	volba
si	se	k3xPyFc3	se
tak	tak	k8xS	tak
strana	strana	k1gFnSc1	strana
polepšila	polepšit	k5eAaPmAgFnS	polepšit
o	o	k7c4	o
necelých	celý	k2eNgInPc2d1	necelý
6	[number]	k4	6
%	%	kIx~	%
a	a	k8xC	a
získala	získat	k5eAaPmAgFnS	získat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvojnásobný	dvojnásobný	k2eAgInSc1d1	dvojnásobný
počet	počet	k1gInSc1	počet
hlasů	hlas	k1gInPc2	hlas
než	než	k8xS	než
druhá	druhý	k4xOgFnSc1	druhý
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
SDKÚ-DS	SDKÚ-DS	k1gFnSc2	SDKÚ-DS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebyl	být	k5eNaImAgMnS	být
Fico	Fico	k1gMnSc1	Fico
schopen	schopen	k2eAgMnSc1d1	schopen
sestavit	sestavit	k5eAaPmF	sestavit
koaliční	koaliční	k2eAgFnSc4d1	koaliční
vládu	vláda	k1gFnSc4	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
Ivan	Ivan	k1gMnSc1	Ivan
Gašparovič	Gašparovič	k1gMnSc1	Gašparovič
následně	následně	k6eAd1	následně
sestavením	sestavení	k1gNnSc7	sestavení
vlády	vláda	k1gFnSc2	vláda
pověřil	pověřit	k5eAaPmAgMnS	pověřit
Ivetu	Iveta	k1gFnSc4	Iveta
Radičovou	radičův	k2eAgFnSc7d1	radičův
z	z	k7c2	z
SDKÚ-DS	SDKÚ-DS	k1gFnSc2	SDKÚ-DS
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
strana	strana	k1gFnSc1	strana
skončila	skončit	k5eAaPmAgFnS	skončit
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
druhá	druhý	k4xOgFnSc1	druhý
se	se	k3xPyFc4	se
ziskem	zisk	k1gInSc7	zisk
15,42	[number]	k4	15,42
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Opakovala	opakovat	k5eAaImAgFnS	opakovat
se	se	k3xPyFc4	se
tak	tak	k9	tak
situace	situace	k1gFnSc1	situace
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
již	již	k6eAd1	již
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
nastala	nastat	k5eAaPmAgFnS	nastat
po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
1998	[number]	k4	1998
a	a	k8xC	a
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
HZDS	HZDS	kA	HZDS
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
Vladimír	Vladimír	k1gMnSc1	Vladimír
Mečiar	Mečiar	k1gMnSc1	Mečiar
nebyl	být	k5eNaImAgMnS	být
schopen	schopen	k2eAgMnSc1d1	schopen
vládu	vláda	k1gFnSc4	vláda
sestavit	sestavit	k5eAaPmF	sestavit
<g/>
.	.	kIx.	.
</s>
<s>
Pověřen	pověřen	k2eAgMnSc1d1	pověřen
tak	tak	k9	tak
byl	být	k5eAaImAgMnS	být
vždy	vždy	k6eAd1	vždy
předseda	předseda	k1gMnSc1	předseda
druhé	druhý	k4xOgFnSc2	druhý
nejsilnější	silný	k2eAgFnSc2d3	nejsilnější
strany	strana	k1gFnSc2	strana
(	(	kIx(	(
<g/>
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
případech	případ	k1gInPc6	případ
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
Dzurinda	Dzurind	k1gMnSc2	Dzurind
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Parlamentní	parlamentní	k2eAgFnSc2d1	parlamentní
volby	volba	k1gFnSc2	volba
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
2012	[number]	k4	2012
a	a	k8xC	a
Druhá	druhý	k4xOgFnSc1	druhý
vláda	vláda	k1gFnSc1	vláda
Roberta	Robert	k1gMnSc4	Robert
Fica	Fico	k1gMnSc4	Fico
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
předčasných	předčasný	k2eAgFnPc6d1	předčasná
parlamentních	parlamentní	k2eAgFnPc6d1	parlamentní
volbách	volba	k1gFnPc6	volba
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgFnPc6	který
strana	strana	k1gFnSc1	strana
SMER-SD	SMER-SD	k1gFnSc1	SMER-SD
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
ještě	ještě	k6eAd1	ještě
s	s	k7c7	s
výraznějším	výrazný	k2eAgInSc7d2	výraznější
odstupem	odstup	k1gInSc7	odstup
od	od	k7c2	od
ostatních	ostatní	k2eAgFnPc2d1	ostatní
stran	strana	k1gFnPc2	strana
než	než	k8xS	než
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
minulých	minulý	k2eAgMnPc2d1	minulý
<g/>
,	,	kIx,	,
sestavila	sestavit	k5eAaPmAgFnS	sestavit
vládu	vláda	k1gFnSc4	vláda
opět	opět	k6eAd1	opět
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Robertem	Robert	k1gMnSc7	Robert
Ficem	Fic	k1gMnSc7	Fic
<g/>
.	.	kIx.	.
</s>
<s>
Robertu	Robert	k1gMnSc3	Robert
Ficovi	Fic	k1gMnSc3	Fic
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
podařilo	podařit	k5eAaPmAgNnS	podařit
vytvořit	vytvořit	k5eAaPmF	vytvořit
vládu	vláda	k1gFnSc4	vláda
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yRgInPc4	který
zasedli	zasednout	k5eAaPmAgMnP	zasednout
pouze	pouze	k6eAd1	pouze
členové	člen	k1gMnPc1	člen
SMERu	SMERus	k1gInSc2	SMERus
a	a	k8xC	a
nestraníci	nestraník	k1gMnPc1	nestraník
za	za	k7c4	za
tuto	tento	k3xDgFnSc4	tento
stranu	strana	k1gFnSc4	strana
nominovaní	nominovaný	k2eAgMnPc1d1	nominovaný
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
projevu	projev	k1gInSc2	projev
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
18	[number]	k4	18
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2013	[number]	k4	2013
před	před	k7c7	před
Národní	národní	k2eAgFnSc7d1	národní
radou	rada	k1gFnSc7	rada
hodnotil	hodnotit	k5eAaImAgMnS	hodnotit
působení	působení	k1gNnSc4	působení
své	svůj	k3xOyFgFnSc2	svůj
druhé	druhý	k4xOgFnSc2	druhý
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
oznámil	oznámit	k5eAaPmAgMnS	oznámit
kandidaturu	kandidatura	k1gFnSc4	kandidatura
na	na	k7c4	na
post	post	k1gInSc4	post
prezidenta	prezident	k1gMnSc2	prezident
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
republiky	republika	k1gFnSc2	republika
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
o	o	k7c6	o
které	který	k3yQgFnSc6	který
se	se	k3xPyFc4	se
spekulovalo	spekulovat	k5eAaImAgNnS	spekulovat
již	již	k9	již
několik	několik	k4yIc1	několik
měsíců	měsíc	k1gInPc2	měsíc
předtím	předtím	k6eAd1	předtím
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc4	jeho
kandidaturu	kandidatura	k1gFnSc4	kandidatura
už	už	k6eAd1	už
předtím	předtím	k6eAd1	předtím
schválilo	schválit	k5eAaPmAgNnS	schválit
vedení	vedení	k1gNnSc1	vedení
strany	strana	k1gFnSc2	strana
SMER	SMER	kA	SMER
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
voleb	volba	k1gFnPc2	volba
<g/>
,	,	kIx,	,
konaném	konaný	k2eAgInSc6d1	konaný
15	[number]	k4	15
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgInS	získat
28	[number]	k4	28
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
a	a	k8xC	a
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
<g/>
,	,	kIx,	,
postoupil	postoupit	k5eAaPmAgMnS	postoupit
do	do	k7c2	do
druhého	druhý	k4xOgNnSc2	druhý
kola	kolo	k1gNnSc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
kole	kolo	k1gNnSc6	kolo
<g/>
,	,	kIx,	,
konaném	konaný	k2eAgInSc6d1	konaný
29	[number]	k4	29
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
však	však	k9	však
byl	být	k5eAaImAgInS	být
poražen	porazit	k5eAaPmNgInS	porazit
kandidátem	kandidát	k1gMnSc7	kandidát
Andrejem	Andrej	k1gMnSc7	Andrej
Kiskou	Kiska	k1gMnSc7	Kiska
<g/>
,	,	kIx,	,
když	když	k8xS	když
získal	získat	k5eAaPmAgMnS	získat
pouze	pouze	k6eAd1	pouze
40	[number]	k4	40
%	%	kIx~	%
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
Kiska	Kiska	k1gMnSc1	Kiska
získal	získat	k5eAaPmAgMnS	získat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
59	[number]	k4	59
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
