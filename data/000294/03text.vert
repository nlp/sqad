<s>
Sázavafest	Sázavafest	k1gFnSc1	Sázavafest
je	být	k5eAaImIp3nS	být
multikulturní	multikulturní	k2eAgFnSc1d1	multikulturní
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
hudební	hudební	k2eAgInSc1d1	hudební
festival	festival	k1gInSc1	festival
pořádaný	pořádaný	k2eAgInSc1d1	pořádaný
každý	každý	k3xTgInSc1	každý
rok	rok	k1gInSc1	rok
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
řeky	řeka	k1gFnSc2	řeka
Sázavy	Sázava	k1gFnSc2	Sázava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2008	[number]	k4	2008
se	se	k3xPyFc4	se
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
již	již	k6eAd1	již
osmý	osmý	k4xOgInSc4	osmý
ročník	ročník	k1gInSc4	ročník
<g/>
.	.	kIx.	.
</s>
<s>
Festival	festival	k1gInSc1	festival
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Sázava	Sázava	k1gFnSc1	Sázava
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
je	být	k5eAaImIp3nS	být
pořádán	pořádán	k2eAgInSc1d1	pořádán
v	v	k7c6	v
Kácově	Kácov	k1gInSc6	Kácov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
se	se	k3xPyFc4	se
přesunul	přesunout	k5eAaPmAgMnS	přesunout
do	do	k7c2	do
Benešova	Benešov	k1gInSc2	Benešov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
opět	opět	k6eAd1	opět
změnil	změnit	k5eAaPmAgInS	změnit
místo	místo	k1gNnSc4	místo
pořádání	pořádání	k1gNnSc2	pořádání
a	a	k8xC	a
koná	konat	k5eAaImIp3nS	konat
se	se	k3xPyFc4	se
ve	v	k7c4	v
Světlé	světlý	k2eAgNnSc4d1	světlé
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
ročník	ročník	k1gInSc1	ročník
festivalu	festival	k1gInSc2	festival
se	se	k3xPyFc4	se
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
na	na	k7c6	na
jediném	jediný	k2eAgNnSc6d1	jediné
pódiu	pódium	k1gNnSc6	pódium
<g/>
.	.	kIx.	.
</s>
<s>
Postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
se	se	k3xPyFc4	se
akce	akce	k1gFnSc1	akce
rozrostla	rozrůst	k5eAaPmAgFnS	rozrůst
a	a	k8xC	a
na	na	k7c6	na
šestém	šestý	k4xOgInSc6	šestý
ročníku	ročník	k1gInSc6	ročník
vystoupilo	vystoupit	k5eAaPmAgNnS	vystoupit
na	na	k7c6	na
sedmi	sedm	k4xCc6	sedm
jevištích	jeviště	k1gNnPc6	jeviště
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
známých	známý	k2eAgMnPc2d1	známý
interpretů	interpret	k1gMnPc2	interpret
české	český	k2eAgFnSc2d1	Česká
hudební	hudební	k2eAgFnSc2d1	hudební
scény	scéna	k1gFnSc2	scéna
a	a	k8xC	a
také	také	k9	také
mnoho	mnoho	k4c1	mnoho
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
hostů	host	k1gMnPc2	host
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
hudební	hudební	k2eAgFnSc2d1	hudební
produkce	produkce	k1gFnSc2	produkce
se	se	k3xPyFc4	se
na	na	k7c6	na
Sázavafestu	Sázavafest	k1gInSc6	Sázavafest
uskutečnily	uskutečnit	k5eAaPmAgFnP	uskutečnit
i	i	k9	i
autorská	autorský	k2eAgNnPc1d1	autorské
čtení	čtení	k1gNnPc1	čtení
například	například	k6eAd1	například
Michala	Michal	k1gMnSc2	Michal
Viewegha	Viewegh	k1gMnSc2	Viewegh
či	či	k8xC	či
Mardoši	Mardoš	k1gMnPc1	Mardoš
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
hlavním	hlavní	k2eAgMnSc7d1	hlavní
investorem	investor	k1gMnSc7	investor
jihlavská	jihlavský	k2eAgFnSc1d1	Jihlavská
společnost	společnost	k1gFnSc1	společnost
XANADU-Catering	XANADU-Catering	k1gInSc1	XANADU-Catering
a	a	k8xC	a
festival	festival	k1gInSc1	festival
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
počet	počet	k1gInSc1	počet
vystupujících	vystupující	k2eAgFnPc2d1	vystupující
kapel	kapela	k1gFnPc2	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
konání	konání	k1gNnSc2	konání
<g/>
:	:	kIx,	:
Světlá	světlat	k5eAaImIp3nS	světlat
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
<g/>
,	,	kIx,	,
areál	areál	k1gInSc1	areál
zámku	zámek	k1gInSc2	zámek
Světlá	světlat	k5eAaImIp3nS	světlat
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
Místo	místo	k7c2	místo
konání	konání	k1gNnSc2	konání
<g/>
:	:	kIx,	:
Světlá	světlat	k5eAaImIp3nS	světlat
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
<g/>
,	,	kIx,	,
areál	areál	k1gInSc1	areál
zámku	zámek	k1gInSc2	zámek
Světlá	světlat	k5eAaImIp3nS	světlat
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
15	[number]	k4	15
<g/>
.	.	kIx.	.
ročník	ročník	k1gInSc1	ročník
festivalu	festival	k1gInSc2	festival
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
proběhne	proběhnout	k5eAaPmIp3nS	proběhnout
během	během	k7c2	během
3	[number]	k4	3
dnů	den	k1gInPc2	den
(	(	kIx(	(
<g/>
od	od	k7c2	od
čtvrtka	čtvrtek	k1gInSc2	čtvrtek
30.7	[number]	k4	30.7
<g/>
.2015	.2015	k4	.2015
do	do	k7c2	do
sobotního	sobotní	k2eAgInSc2d1	sobotní
večera	večer	k1gInSc2	večer
1.8	[number]	k4	1.8
<g/>
.2015	.2015	k4	.2015
<g/>
)	)	kIx)	)
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
zámeckého	zámecký	k2eAgInSc2d1	zámecký
lesoparku	lesopark	k1gInSc2	lesopark
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
festivalu	festival	k1gInSc6	festival
vystoupí	vystoupit	k5eAaPmIp3nS	vystoupit
<g/>
:	:	kIx,	:
Tomáš	Tomáš	k1gMnSc1	Tomáš
Klus	klus	k1gInSc1	klus
<g/>
,	,	kIx,	,
Alphaville	Alphaville	k1gInSc1	Alphaville
<g/>
,	,	kIx,	,
Mig	mig	k1gInSc1	mig
21	[number]	k4	21
<g/>
,	,	kIx,	,
Tata	tata	k1gMnSc1	tata
Bojs	Bojsa	k1gFnPc2	Bojsa
<g/>
,	,	kIx,	,
Škwor	Škwora	k1gFnPc2	Škwora
<g/>
,	,	kIx,	,
Vypsaná	vypsaný	k2eAgNnPc1d1	vypsané
Fixa	fixum	k1gNnPc1	fixum
<g/>
,	,	kIx,	,
Michal	Michal	k1gMnSc1	Michal
Hrůza	Hrůza	k1gMnSc1	Hrůza
<g/>
,	,	kIx,	,
Wohnout	Wohnout	k1gMnSc1	Wohnout
<g/>
,	,	kIx,	,
Xindl	Xindl	k1gMnSc1	Xindl
X	X	kA	X
<g/>
,	,	kIx,	,
Michal	Michal	k1gMnSc1	Michal
Viewegh	Viewegh	k1gMnSc1	Viewegh
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
konání	konání	k1gNnSc2	konání
<g/>
:	:	kIx,	:
Světlá	světlat	k5eAaImIp3nS	světlat
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
<g/>
,	,	kIx,	,
areál	areál	k1gInSc1	areál
zámku	zámek	k1gInSc2	zámek
Světlá	světlat	k5eAaImIp3nS	světlat
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
Na	na	k7c6	na
festivalu	festival	k1gInSc6	festival
ve	v	k7c6	v
3	[number]	k4	3
dnech	den	k1gInPc6	den
na	na	k7c4	na
4	[number]	k4	4
scénách	scéna	k1gFnPc6	scéna
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
<g/>
:	:	kIx,	:
Dan	Dan	k1gMnSc1	Dan
Bárta	Bárta	k1gMnSc1	Bárta
&	&	k?	&
Illustratosphere	Illustratospher	k1gMnSc5	Illustratospher
<g/>
,	,	kIx,	,
Monkey	Monkeum	k1gNnPc7	Monkeum
Business	business	k1gInSc1	business
<g/>
,	,	kIx,	,
Sto	sto	k4xCgNnSc4	sto
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
Support	support	k1gInSc1	support
Lesbiens	Lesbiens	k1gInSc1	Lesbiens
<g/>
,	,	kIx,	,
Škwor	Škwor	k1gMnSc1	Škwor
<g/>
,	,	kIx,	,
Bart	Bart	k1gMnSc1	Bart
<g/>
&	&	k?	&
<g/>
Baker	Baker	k1gMnSc1	Baker
<g/>
,	,	kIx,	,
Mňága	Mňága	k1gFnSc1	Mňága
a	a	k8xC	a
Žďorp	Žďorp	k1gMnSc1	Žďorp
<g/>
,	,	kIx,	,
Wohnout	Wohnout	k1gMnSc1	Wohnout
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Beth	Beth	k1gMnSc1	Beth
Edges	Edges	k1gMnSc1	Edges
<g/>
,	,	kIx,	,
Mr	Mr	k1gMnSc1	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Žarko	Žarko	k6eAd1	Žarko
<g/>
,	,	kIx,	,
Na	na	k7c4	na
Stojáka	stoják	k1gMnSc4	stoják
<g/>
,	,	kIx,	,
Tonya	Tonya	k1gMnSc1	Tonya
Graves	Graves	k1gMnSc1	Graves
a	a	k8xC	a
Cartonnage	Cartonnage	k1gFnSc1	Cartonnage
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
konání	konání	k1gNnSc2	konání
<g/>
:	:	kIx,	:
Světlá	světlat	k5eAaImIp3nS	světlat
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
<g/>
,	,	kIx,	,
areál	areál	k1gInSc1	areál
zámku	zámek	k1gInSc2	zámek
Světlá	světlat	k5eAaImIp3nS	světlat
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
se	se	k3xPyFc4	se
festival	festival	k1gInSc1	festival
odehrával	odehrávat	k5eAaImAgInS	odehrávat
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
3	[number]	k4	3
dnů	den	k1gInPc2	den
(	(	kIx(	(
<g/>
od	od	k7c2	od
čtvrtka	čtvrtek	k1gInSc2	čtvrtek
1.8	[number]	k4	1.8
<g/>
.2013	.2013	k4	.2013
do	do	k7c2	do
soboty	sobota	k1gFnSc2	sobota
3.8	[number]	k4	3.8
<g/>
.2013	.2013	k4	.2013
<g/>
)	)	kIx)	)
ve	v	k7c4	v
Světlé	světlý	k2eAgNnSc4d1	světlé
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
<g/>
,	,	kIx,	,
v	v	k7c6	v
lesoparku	lesopark	k1gInSc6	lesopark
místního	místní	k2eAgInSc2d1	místní
zámku	zámek	k1gInSc2	zámek
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
již	již	k6eAd1	již
tradičně	tradičně	k6eAd1	tradičně
<g/>
,	,	kIx,	,
součástí	součást	k1gFnSc7	součást
areálu	areál	k1gInSc2	areál
bylo	být	k5eAaImAgNnS	být
i	i	k9	i
stanové	stanový	k2eAgNnSc1d1	stanové
městečko	městečko	k1gNnSc1	městečko
<g/>
.	.	kIx.	.
</s>
<s>
Headlinery	Headliner	k1gMnPc4	Headliner
tohoto	tento	k3xDgInSc2	tento
ročníku	ročník	k1gInSc2	ročník
byli	být	k5eAaImAgMnP	být
<g/>
:	:	kIx,	:
Hadouken	Hadouken	k1gInSc1	Hadouken
<g/>
!	!	kIx.	!
</s>
<s>
z	z	k7c2	z
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
,	,	kIx,	,
Dub	Dub	k1gMnSc1	Dub
FX	FX	kA	FX
(	(	kIx(	(
<g/>
AUS	AUS	kA	AUS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
senzace	senzace	k1gFnSc1	senzace
Lyre	Lyr	k1gFnSc2	Lyr
le	le	k?	le
Temps	Temps	k1gInSc1	Temps
<g/>
,	,	kIx,	,
Celeste	Celest	k1gInSc5	Celest
Buckingham	Buckingham	k1gInSc1	Buckingham
(	(	kIx(	(
<g/>
SK	Sk	kA	Sk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
No	no	k9	no
Name	Name	k1gInSc1	Name
(	(	kIx(	(
<g/>
SK	Sk	kA	Sk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kryštof	Kryštof	k1gMnSc1	Kryštof
<g/>
,	,	kIx,	,
Monarchy	monarcha	k1gMnPc7	monarcha
(	(	kIx(	(
<g/>
UK	UK	kA	UK
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Falco	Falco	k1gMnSc1	Falco
T.B.	T.B.	k1gMnSc1	T.B.
(	(	kIx(	(
<g/>
AT	AT	kA	AT
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
například	například	k6eAd1	například
Mandrage	Mandrage	k1gNnSc1	Mandrage
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
konání	konání	k1gNnSc2	konání
<g/>
:	:	kIx,	:
Benešov	Benešov	k1gInSc1	Benešov
<g/>
,	,	kIx,	,
areál	areál	k1gInSc1	areál
Táborských	Táborských	k2eAgFnPc2d1	Táborských
kasáren	kasárny	k1gFnPc2	kasárny
Festival	festival	k1gInSc1	festival
se	se	k3xPyFc4	se
již	již	k6eAd1	již
podruhé	podruhé	k6eAd1	podruhé
konal	konat	k5eAaImAgInS	konat
ve	v	k7c6	v
středočeském	středočeský	k2eAgInSc6d1	středočeský
Benešově	Benešov	k1gInSc6	Benešov
<g/>
,	,	kIx,	,
opět	opět	k6eAd1	opět
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
Táborských	Táborských	k2eAgFnPc2d1	Táborských
kasáren	kasárny	k1gFnPc2	kasárny
<g/>
.	.	kIx.	.
</s>
<s>
Nedlouho	dlouho	k6eNd1	dlouho
před	před	k7c7	před
zahájením	zahájení	k1gNnSc7	zahájení
festivalu	festival	k1gInSc2	festival
se	se	k3xPyFc4	se
odhlásili	odhlásit	k5eAaPmAgMnP	odhlásit
plánovaní	plánovaný	k2eAgMnPc1d1	plánovaný
headlineři	headliner	k1gMnPc1	headliner
<g/>
:	:	kIx,	:
Kaiser	Kaiser	k1gInSc1	Kaiser
Chiefs	Chiefsa	k1gFnPc2	Chiefsa
<g/>
.	.	kIx.	.
</s>
<s>
Návštěvníkům	návštěvník	k1gMnPc3	návštěvník
toto	tento	k3xDgNnSc4	tento
bylo	být	k5eAaImAgNnS	být
kompenzováno	kompenzován	k2eAgNnSc1d1	kompenzováno
slevou	sleva	k1gFnSc7	sleva
200	[number]	k4	200
<g/>
,	,	kIx,	,
<g/>
-	-	kIx~	-
na	na	k7c4	na
příští	příští	k2eAgInSc4d1	příští
ročník	ročník	k1gInSc4	ročník
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
festivalu	festival	k1gInSc6	festival
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
<g/>
:	:	kIx,	:
Paul	Paul	k1gMnSc1	Paul
Oakenfold	Oakenfold	k1gMnSc1	Oakenfold
<g/>
,	,	kIx,	,
Sak	sak	k1gInSc1	sak
Noel	Noel	k1gInSc1	Noel
<g/>
,	,	kIx,	,
Mike	Mike	k1gInSc1	Mike
Candys	Candys	k1gInSc1	Candys
<g/>
,	,	kIx,	,
Fun	Fun	k1gMnSc1	Fun
Lovin	Lovin	k1gMnSc1	Lovin
<g/>
'	'	kIx"	'
Criminals	Criminals	k1gInSc1	Criminals
<g/>
,	,	kIx,	,
Lyre	Lyre	k1gInSc1	Lyre
le	le	k?	le
Temps	Temps	k1gInSc1	Temps
<g/>
,	,	kIx,	,
Gabriela	Gabriela	k1gFnSc1	Gabriela
Gunčíková	Gunčíková	k1gFnSc1	Gunčíková
<g/>
,	,	kIx,	,
Katarína	Katarína	k1gFnSc1	Katarína
Knechtová	Knechtová	k1gFnSc1	Knechtová
a	a	k8xC	a
třeba	třeba	k6eAd1	třeba
také	také	k9	také
čeští	český	k2eAgMnPc1d1	český
Nightwork	Nightwork	k1gInSc1	Nightwork
a	a	k8xC	a
Tomáš	Tomáš	k1gMnSc1	Tomáš
Klus	klus	k1gInSc1	klus
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
konání	konání	k1gNnSc2	konání
<g/>
:	:	kIx,	:
Benešov	Benešov	k1gInSc1	Benešov
<g/>
,	,	kIx,	,
areál	areál	k1gInSc1	areál
Táborských	Táborských	k2eAgFnPc2d1	Táborských
kasáren	kasárny	k1gFnPc2	kasárny
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
se	se	k3xPyFc4	se
SázavaFest	SázavaFest	k1gInSc1	SázavaFest
poprvé	poprvé	k6eAd1	poprvé
konal	konat	k5eAaImAgInS	konat
v	v	k7c6	v
Benešově	Benešov	k1gInSc6	Benešov
u	u	k7c2	u
Prahy	Praha	k1gFnSc2	Praha
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
Táborských	Táborských	k2eAgFnPc2d1	Táborských
kasáren	kasárny	k1gFnPc2	kasárny
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
festivalu	festival	k1gInSc6	festival
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
<g/>
:	:	kIx,	:
Hurts	Hurts	k1gInSc1	Hurts
<g/>
,	,	kIx,	,
Kosheen	Kosheen	k1gInSc1	Kosheen
<g/>
,	,	kIx,	,
Alphaville	Alphaville	k1gFnSc1	Alphaville
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Subways	Subways	k1gInSc1	Subways
<g/>
,	,	kIx,	,
Nouvelle	Nouvelle	k1gFnSc1	Nouvelle
Vague	Vague	k1gFnSc1	Vague
<g/>
,	,	kIx,	,
Charlie	Charlie	k1gMnSc1	Charlie
Straight	Straight	k1gMnSc1	Straight
<g/>
,	,	kIx,	,
Chinaski	Chinask	k1gMnPc1	Chinask
<g/>
,	,	kIx,	,
Tomáš	Tomáš	k1gMnSc1	Tomáš
Klus	klus	k1gInSc1	klus
<g/>
,	,	kIx,	,
Toxique	Toxique	k1gInSc1	Toxique
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
například	například	k6eAd1	například
Věra	Věra	k1gFnSc1	Věra
Bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
konání	konání	k1gNnSc2	konání
<g/>
:	:	kIx,	:
areál	areál	k1gInSc1	areál
Malá	malý	k2eAgFnSc1d1	malá
Strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
Kácov	Kácov	k1gInSc1	Kácov
Místo	místo	k7c2	místo
konání	konání	k1gNnSc2	konání
<g/>
:	:	kIx,	:
Kácov	Kácov	k1gInSc1	Kácov
Místo	místo	k7c2	místo
konání	konání	k1gNnSc2	konání
<g/>
:	:	kIx,	:
areál	areál	k1gInSc1	areál
Malá	malý	k2eAgFnSc1d1	malá
Strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
Kácov	Kácov	k1gInSc1	Kácov
Místo	místo	k7c2	místo
konání	konání	k1gNnSc2	konání
<g/>
:	:	kIx,	:
areál	areál	k1gInSc1	areál
Malá	malý	k2eAgFnSc1d1	malá
Strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
Kácov	Kácov	k1gInSc1	Kácov
Místo	místo	k7c2	místo
konání	konání	k1gNnSc2	konání
<g/>
:	:	kIx,	:
areál	areál	k1gInSc1	areál
Ostrov	ostrov	k1gInSc1	ostrov
a	a	k8xC	a
okolí	okolí	k1gNnSc1	okolí
<g/>
,	,	kIx,	,
Sázava	Sázava	k1gFnSc1	Sázava
Místo	místo	k7c2	místo
konání	konání	k1gNnSc2	konání
<g/>
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s>
areál	areál	k1gInSc1	areál
Ostrov	ostrov	k1gInSc1	ostrov
a	a	k8xC	a
okolí	okolí	k1gNnSc1	okolí
<g/>
,	,	kIx,	,
Sázava	Sázava	k1gFnSc1	Sázava
Místo	místo	k7c2	místo
konání	konání	k1gNnSc2	konání
<g/>
:	:	kIx,	:
areál	areál	k1gInSc1	areál
Ostrov	ostrov	k1gInSc1	ostrov
a	a	k8xC	a
okolí	okolí	k1gNnSc1	okolí
<g/>
,	,	kIx,	,
Sázava	Sázava	k1gFnSc1	Sázava
Místo	místo	k7c2	místo
konání	konání	k1gNnSc2	konání
<g/>
:	:	kIx,	:
areál	areál	k1gInSc1	areál
Ostrov	ostrov	k1gInSc1	ostrov
<g/>
,	,	kIx,	,
Sázava	Sázava	k1gFnSc1	Sázava
Místo	místo	k7c2	místo
konání	konání	k1gNnSc2	konání
<g/>
:	:	kIx,	:
areál	areál	k1gInSc1	areál
Ostrov	ostrov	k1gInSc1	ostrov
<g/>
,	,	kIx,	,
Sázava	Sázava	k1gFnSc1	Sázava
Místo	místo	k7c2	místo
konání	konání	k1gNnSc2	konání
<g/>
:	:	kIx,	:
Zahrada	zahrada	k1gFnSc1	zahrada
KD	KD	kA	KD
Metaz	Metaz	k1gInSc1	Metaz
<g/>
,	,	kIx,	,
Týnec	Týnec	k1gInSc1	Týnec
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Sázavafest	Sázavafest	k1gInSc4	Sázavafest
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
Informace	informace	k1gFnSc1	informace
o	o	k7c6	o
Sázavafestu	Sázavafest	k1gInSc6	Sázavafest
2010	[number]	k4	2010
Program	program	k1gInSc1	program
Sázavafestu	Sázavafest	k1gInSc2	Sázavafest
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
</s>
