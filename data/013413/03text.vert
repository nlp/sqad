<p>
<s>
Robert	Robert	k1gMnSc1	Robert
Wiene	Wien	k1gInSc5	Wien
(	(	kIx(	(
<g/>
27	[number]	k4	27
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1873	[number]	k4	1873
Vratislav	Vratislav	k1gMnSc1	Vratislav
–	–	k?	–
17	[number]	k4	17
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1938	[number]	k4	1938
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
německý	německý	k2eAgMnSc1d1	německý
filmový	filmový	k2eAgMnSc1d1	filmový
režisér	režisér	k1gMnSc1	režisér
a	a	k8xC	a
herec	herec	k1gMnSc1	herec
židovského	židovský	k2eAgInSc2d1	židovský
původu	původ	k1gInSc2	původ
narozený	narozený	k2eAgMnSc1d1	narozený
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Proslavil	proslavit	k5eAaPmAgMnS	proslavit
se	se	k3xPyFc4	se
filmovým	filmový	k2eAgInSc7d1	filmový
hororem	horor	k1gInSc7	horor
Kabinet	kabinet	k1gInSc1	kabinet
doktora	doktor	k1gMnSc2	doktor
Caligariho	Caligari	k1gMnSc2	Caligari
(	(	kIx(	(
<g/>
Das	Das	k1gMnSc1	Das
Kabinett	Kabinett	k1gMnSc1	Kabinett
des	des	k1gNnSc2	des
Doktor	doktor	k1gMnSc1	doktor
Caligari	Caligar	k1gFnSc2	Caligar
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
natočil	natočit	k5eAaBmAgMnS	natočit
roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
<g/>
,	,	kIx,	,
a	a	k8xC	a
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
zakladatelské	zakladatelský	k2eAgNnSc4d1	zakladatelské
dílo	dílo	k1gNnSc4	dílo
filmového	filmový	k2eAgInSc2d1	filmový
expresionismu	expresionismus	k1gInSc2	expresionismus
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dalších	další	k2eAgInPc2d1	další
snímků	snímek	k1gInPc2	snímek
zaujala	zaujmout	k5eAaPmAgFnS	zaujmout
jeho	jeho	k3xOp3gFnSc1	jeho
adaptace	adaptace	k1gFnSc1	adaptace
Zločinu	zločin	k1gInSc2	zločin
a	a	k8xC	a
trestu	trest	k1gInSc2	trest
Fjodora	Fjodor	k1gMnSc2	Fjodor
Michajloviče	Michajlovič	k1gMnSc2	Michajlovič
Dostojevského	Dostojevský	k2eAgMnSc2d1	Dostojevský
nazvaná	nazvaný	k2eAgFnSc1d1	nazvaná
Raskolnikow	Raskolnikow	k1gFnPc1	Raskolnikow
(	(	kIx(	(
<g/>
1923	[number]	k4	1923
<g/>
)	)	kIx)	)
či	či	k8xC	či
horor	horor	k1gInSc1	horor
Orlakovy	Orlakův	k2eAgFnPc1d1	Orlakův
ruce	ruka	k1gFnPc1	ruka
(	(	kIx(	(
<g/>
1924	[number]	k4	1924
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
pádem	pád	k1gInSc7	pád
expresionismu	expresionismus	k1gInSc2	expresionismus
a	a	k8xC	a
koncem	koncem	k7c2	koncem
němého	němý	k2eAgInSc2d1	němý
filmu	film	k1gInSc2	film
na	na	k7c6	na
konci	konec	k1gInSc6	konec
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
jeho	jeho	k3xOp3gFnSc1	jeho
sláva	sláva	k1gFnSc1	sláva
pohasla	pohasnout	k5eAaPmAgFnS	pohasnout
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
nacismu	nacismus	k1gInSc2	nacismus
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
odešel	odejít	k5eAaPmAgInS	odejít
do	do	k7c2	do
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
spolupracoval	spolupracovat	k5eAaImAgInS	spolupracovat
mj.	mj.	kA	mj.
s	s	k7c7	s
Jean	Jean	k1gMnSc1	Jean
Cocteauem	Cocteau	k1gMnSc7	Cocteau
a	a	k8xC	a
kde	kde	k6eAd1	kde
před	před	k7c7	před
začátkem	začátek	k1gInSc7	začátek
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
také	také	k9	také
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
Carl	Carl	k1gMnSc1	Carl
Wiene	Wien	k1gInSc5	Wien
a	a	k8xC	a
bratr	bratr	k1gMnSc1	bratr
Conrad	Conrada	k1gFnPc2	Conrada
Wiene	Wien	k1gInSc5	Wien
byli	být	k5eAaImAgMnP	být
známí	známý	k2eAgMnPc1d1	známý
herci	herec	k1gMnPc1	herec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fotografie	fotografia	k1gFnPc4	fotografia
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Robert	Robert	k1gMnSc1	Robert
Wiene	Wien	k1gInSc5	Wien
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Robert	Robert	k1gMnSc1	Robert
Wiene	Wien	k1gInSc5	Wien
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
