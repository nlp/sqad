<s>
Lamanšský	lamanšský	k2eAgInSc1d1	lamanšský
průliv	průliv	k1gInSc1	průliv
neboli	neboli	k8xC	neboli
kanál	kanál	k1gInSc1	kanál
La	la	k1gNnSc2	la
Manche	Manch	k1gInSc2	Manch
(	(	kIx(	(
<g/>
též	též	k9	též
Anglický	anglický	k2eAgInSc1d1	anglický
kanál	kanál	k1gInSc1	kanál
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
také	také	k9	také
Britský	britský	k2eAgInSc1d1	britský
průplav	průplav	k1gInSc1	průplav
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
část	část	k1gFnSc1	část
Atlantského	atlantský	k2eAgInSc2d1	atlantský
oceánu	oceán	k1gInSc2	oceán
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
ostrov	ostrov	k1gInSc4	ostrov
Velká	velká	k1gFnSc1	velká
Británie	Británie	k1gFnSc1	Británie
a	a	k8xC	a
severní	severní	k2eAgFnSc6d1	severní
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Spojuje	spojovat	k5eAaImIp3nS	spojovat
Keltské	keltský	k2eAgNnSc4d1	keltské
moře	moře	k1gNnSc4	moře
a	a	k8xC	a
Severní	severní	k2eAgNnSc4d1	severní
moře	moře	k1gNnSc4	moře
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
nejrušnější	rušný	k2eAgNnSc1d3	nejrušnější
lodní	lodní	k2eAgFnSc7d1	lodní
dopravní	dopravní	k2eAgFnSc7d1	dopravní
cestou	cesta	k1gFnSc7	cesta
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Průliv	průliv	k1gInSc1	průliv
je	být	k5eAaImIp3nS	být
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
asi	asi	k9	asi
350	[number]	k4	350
km	km	kA	km
<g/>
,	,	kIx,	,
šířka	šířka	k1gFnSc1	šířka
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
od	od	k7c2	od
240	[number]	k4	240
po	po	k7c4	po
34	[number]	k4	34
km	km	kA	km
v	v	k7c6	v
nejužším	úzký	k2eAgNnSc6d3	nejužší
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
takzvané	takzvaný	k2eAgFnSc3d1	takzvaná
Doverské	Doverský	k2eAgFnSc3d1	Doverská
úžině	úžina	k1gFnSc3	úžina
mezi	mezi	k7c7	mezi
městy	město	k1gNnPc7	město
Dover	Dover	k1gMnSc1	Dover
a	a	k8xC	a
Calais	Calais	k1gNnPc2	Calais
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
jasného	jasný	k2eAgNnSc2d1	jasné
počasí	počasí	k1gNnSc2	počasí
tak	tak	k9	tak
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
mysu	mys	k1gInSc2	mys
Gris-Nez	Gris-Neza	k1gFnPc2	Gris-Neza
na	na	k7c6	na
francouzské	francouzský	k2eAgFnSc6d1	francouzská
straně	strana	k1gFnSc6	strana
vidět	vidět	k5eAaImF	vidět
křídové	křídový	k2eAgInPc4d1	křídový
Bílé	bílý	k2eAgInPc4d1	bílý
útesy	útes	k1gInPc4	útes
doverské	doverský	k2eAgInPc4d1	doverský
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
je	být	k5eAaImIp3nS	být
průliv	průliv	k1gInSc4	průliv
nazýván	nazýván	k2eAgMnSc1d1	nazýván
The	The	k1gMnSc1	The
Channel	Channel	k1gMnSc1	Channel
nebo	nebo	k8xC	nebo
The	The	k1gMnSc1	The
English	English	k1gMnSc1	English
Channel	Channel	k1gMnSc1	Channel
<g/>
,	,	kIx,	,
La	la	k1gNnSc1	la
Manche	Manch	k1gFnSc2	Manch
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
pojmenování	pojmenování	k1gNnSc1	pojmenování
francouzské	francouzský	k2eAgNnSc1d1	francouzské
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
geologického	geologický	k2eAgInSc2d1	geologický
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
Lamanšský	lamanšský	k2eAgInSc4d1	lamanšský
průliv	průliv	k1gInSc4	průliv
velice	velice	k6eAd1	velice
nedávno	nedávno	k6eAd1	nedávno
<g/>
,	,	kIx,	,
zhruba	zhruba	k6eAd1	zhruba
před	před	k7c7	před
10	[number]	k4	10
000	[number]	k4	000
lety	léto	k1gNnPc7	léto
<g/>
,	,	kIx,	,
teprve	teprve	k6eAd1	teprve
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
poslední	poslední	k2eAgFnSc2d1	poslední
doby	doba	k1gFnSc2	doba
ledové	ledový	k2eAgFnSc2d1	ledová
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
tající	tající	k2eAgInPc1d1	tající
ledovce	ledovec	k1gInPc1	ledovec
zvýšily	zvýšit	k5eAaPmAgInP	zvýšit
hladinu	hladina	k1gFnSc4	hladina
moří	moře	k1gNnPc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
byla	být	k5eAaImAgFnS	být
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
spojena	spojit	k5eAaPmNgFnS	spojit
s	s	k7c7	s
evropskou	evropský	k2eAgFnSc7d1	Evropská
pevninou	pevnina	k1gFnSc7	pevnina
<g/>
.	.	kIx.	.
</s>
<s>
Vzdušnou	vzdušný	k2eAgFnSc7d1	vzdušná
cestou	cesta	k1gFnSc7	cesta
byl	být	k5eAaImAgInS	být
Lamanšský	lamanšský	k2eAgInSc1d1	lamanšský
průliv	průliv	k1gInSc1	průliv
poprvé	poprvé	k6eAd1	poprvé
překonán	překonat	k5eAaPmNgInS	překonat
7	[number]	k4	7
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1785	[number]	k4	1785
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Francouz	Francouz	k1gMnSc1	Francouz
Jean-Pierre	Jean-Pierr	k1gInSc5	Jean-Pierr
Blanchard	Blanchard	k1gMnSc1	Blanchard
a	a	k8xC	a
Američan	Američan	k1gMnSc1	Američan
John	John	k1gMnSc1	John
Jeffries	Jeffries	k1gMnSc1	Jeffries
přeletěli	přeletět	k5eAaPmAgMnP	přeletět
v	v	k7c6	v
balonu	balon	k1gInSc6	balon
mezi	mezi	k7c7	mezi
Doverem	Dovero	k1gNnSc7	Dovero
a	a	k8xC	a
Calais	Calais	k1gNnSc7	Calais
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
letadlem	letadlo	k1gNnSc7	letadlo
těžším	těžký	k2eAgNnPc3d2	těžší
vzduchu	vzduch	k1gInSc3	vzduch
uspěl	uspět	k5eAaPmAgMnS	uspět
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
Francouz	Francouz	k1gMnSc1	Francouz
Louis	Louis	k1gMnSc1	Louis
Blériot	Blériot	k1gMnSc1	Blériot
25	[number]	k4	25
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1909	[number]	k4	1909
(	(	kIx(	(
<g/>
směrem	směr	k1gInSc7	směr
z	z	k7c2	z
Francie	Francie	k1gFnSc2	Francie
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
plavcem	plavec	k1gMnSc7	plavec
přes	přes	k7c4	přes
kanál	kanál	k1gInSc4	kanál
La	la	k1gNnSc2	la
Manche	Manch	k1gFnSc2	Manch
(	(	kIx(	(
<g/>
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
36	[number]	k4	36
km	km	kA	km
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
Angličan	Angličan	k1gMnSc1	Angličan
Matthew	Matthew	k1gMnSc1	Matthew
Webb	Webb	k1gMnSc1	Webb
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1875	[number]	k4	1875
<g/>
.	.	kIx.	.
</s>
<s>
Překonání	překonání	k1gNnSc1	překonání
průlivu	průliv	k1gInSc2	průliv
z	z	k7c2	z
Doveru	Dover	k1gInSc2	Dover
do	do	k7c2	do
Calais	Calais	k1gNnSc2	Calais
mu	on	k3xPp3gMnSc3	on
trvalo	trvat	k5eAaImAgNnS	trvat
21	[number]	k4	21
hodin	hodina	k1gFnPc2	hodina
a	a	k8xC	a
45	[number]	k4	45
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1926	[number]	k4	1926
se	se	k3xPyFc4	se
Američanka	Američanka	k1gFnSc1	Američanka
Gertrude	Gertrud	k1gInSc5	Gertrud
Ederleová	Ederleová	k1gFnSc1	Ederleová
vydala	vydat	k5eAaPmAgFnS	vydat
opačným	opačný	k2eAgInSc7d1	opačný
směrem	směr	k1gInSc7	směr
a	a	k8xC	a
po	po	k7c6	po
14	[number]	k4	14
hodinách	hodina	k1gFnPc6	hodina
a	a	k8xC	a
39	[number]	k4	39
minutách	minuta	k1gFnPc6	minuta
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
z	z	k7c2	z
vody	voda	k1gFnSc2	voda
jako	jako	k8xC	jako
první	první	k4xOgFnSc1	první
žena	žena	k1gFnSc1	žena
<g/>
.	.	kIx.	.
</s>
<s>
Nejrychleji	rychle	k6eAd3	rychle
přeplaval	přeplavat	k5eAaPmAgMnS	přeplavat
kanál	kanál	k1gInSc4	kanál
australský	australský	k2eAgMnSc1d1	australský
plavec	plavec	k1gMnSc1	plavec
Trent	Trenta	k1gFnPc2	Trenta
Grimsey	Grimsea	k1gFnSc2	Grimsea
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
to	ten	k3xDgNnSc1	ten
zvládl	zvládnout	k5eAaPmAgInS	zvládnout
za	za	k7c4	za
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
55	[number]	k4	55
<g/>
.	.	kIx.	.
</s>
<s>
Překonal	překonat	k5eAaPmAgMnS	překonat
tak	tak	k9	tak
čas	čas	k1gInSc4	čas
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
57	[number]	k4	57
bulharského	bulharský	k2eAgMnSc2d1	bulharský
plavce	plavec	k1gMnSc2	plavec
Petara	Petar	k1gMnSc2	Petar
Stojčeva	Stojčev	k1gMnSc2	Stojčev
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
Nejrychlejší	rychlý	k2eAgFnSc7d3	nejrychlejší
ženou	žena	k1gFnSc7	žena
je	být	k5eAaImIp3nS	být
naše	náš	k3xOp1gFnSc1	náš
Yvetta	Yvetta	k1gFnSc1	Yvetta
Hlaváčová	Hlaváčová	k1gFnSc1	Hlaváčová
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
:	:	kIx,	:
<g/>
25	[number]	k4	25
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
překonala	překonat	k5eAaPmAgFnS	překonat
kanál	kanál	k1gInSc4	kanál
britka	britka	k1gFnSc1	britka
Alison	Alison	k1gInSc1	Alison
Streeter	Streeter	k1gInSc1	Streeter
(	(	kIx(	(
<g/>
43	[number]	k4	43
<g/>
×	×	k?	×
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
historii	historie	k1gFnSc6	historie
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
několik	několik	k4yIc4	několik
projektů	projekt	k1gInPc2	projekt
spojení	spojení	k1gNnSc2	spojení
pro	pro	k7c4	pro
pozemní	pozemní	k2eAgInPc4d1	pozemní
dopravní	dopravní	k2eAgInPc4d1	dopravní
prostředky	prostředek	k1gInPc4	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
tento	tento	k3xDgInSc4	tento
účel	účel	k1gInSc4	účel
plní	plnit	k5eAaImIp3nP	plnit
trajekty	trajekt	k1gInPc1	trajekt
<g/>
,	,	kIx,	,
na	na	k7c6	na
konci	konec	k1gInSc6	konec
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
ale	ale	k8xC	ale
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
podmořský	podmořský	k2eAgInSc1d1	podmořský
Eurotunel	Eurotunel	k1gInSc1	Eurotunel
<g/>
,	,	kIx,	,
umožňující	umožňující	k2eAgNnSc4d1	umožňující
přímé	přímý	k2eAgNnSc4d1	přímé
vlakové	vlakový	k2eAgNnSc4d1	vlakové
spojení	spojení	k1gNnSc4	spojení
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
železniční	železniční	k2eAgFnSc2d1	železniční
přepravy	přeprava	k1gFnSc2	přeprava
automobilů	automobil	k1gInPc2	automobil
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lamanšský	lamanšský	k2eAgInSc1d1	lamanšský
průliv	průliv	k1gInSc1	průliv
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
významnou	významný	k2eAgFnSc7d1	významná
strategickou	strategický	k2eAgFnSc7d1	strategická
přírodní	přírodní	k2eAgFnSc7d1	přírodní
překážkou	překážka	k1gFnSc7	překážka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
boje	boj	k1gInPc4	boj
mezi	mezi	k7c7	mezi
Anglií	Anglie	k1gFnSc7	Anglie
a	a	k8xC	a
evropskými	evropský	k2eAgFnPc7d1	Evropská
zeměmi	zem	k1gFnPc7	zem
<g/>
.	.	kIx.	.
</s>
<s>
Kanál	kanál	k1gInSc1	kanál
sloužil	sloužit	k5eAaImAgInS	sloužit
například	například	k6eAd1	například
k	k	k7c3	k
obraně	obrana	k1gFnSc3	obrana
proti	proti	k7c3	proti
Napoleonovi	Napoleon	k1gMnSc3	Napoleon
i	i	k8xC	i
nacistickému	nacistický	k2eAgNnSc3d1	nacistické
Německu	Německo	k1gNnSc3	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgInS	být
zaminován	zaminovat	k5eAaPmNgInS	zaminovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
ztížily	ztížit	k5eAaPmAgFnP	ztížit
operace	operace	k1gFnPc1	operace
německých	německý	k2eAgFnPc2d1	německá
ponorek	ponorka	k1gFnPc2	ponorka
<g/>
.	.	kIx.	.
</s>
<s>
Lamanšský	lamanšský	k2eAgInSc1d1	lamanšský
průliv	průliv	k1gInSc1	průliv
je	být	k5eAaImIp3nS	být
oblíbeným	oblíbený	k2eAgInSc7d1	oblíbený
cílem	cíl	k1gInSc7	cíl
vytrvalostních	vytrvalostní	k2eAgMnPc2d1	vytrvalostní
plavců	plavec	k1gMnPc2	plavec
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
jejich	jejich	k3xOp3gInPc1	jejich
výkony	výkon	k1gInPc1	výkon
někdy	někdy	k6eAd1	někdy
kolidují	kolidovat	k5eAaImIp3nP	kolidovat
s	s	k7c7	s
lodní	lodní	k2eAgFnSc7d1	lodní
dopravou	doprava	k1gFnSc7	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Normanské	normanský	k2eAgInPc1d1	normanský
ostrovy	ostrov	k1gInPc1	ostrov
Eurotunel	Eurotunlo	k1gNnPc2	Eurotunlo
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Lamanšský	lamanšský	k2eAgInSc4d1	lamanšský
průliv	průliv	k1gInSc4	průliv
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc4d1	slovníkové
heslo	heslo	k1gNnSc4	heslo
Lamanšský	lamanšský	k2eAgInSc1d1	lamanšský
průliv	průliv	k1gInSc1	průliv
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
datovaný	datovaný	k2eAgInSc1d1	datovaný
autogram	autogram	k1gInSc1	autogram
J.	J.	kA	J.
<g/>
Novák	Novák	k1gMnSc1	Novák
Další	další	k2eAgFnPc4d1	další
fotky	fotka	k1gFnPc4	fotka
autogramy	autogram	k1gInPc1	autogram
dalších	další	k2eAgMnPc2d1	další
pokořitelů	pokořitel	k1gMnPc2	pokořitel
Video	video	k1gNnSc1	video
První	první	k4xOgFnSc1	první
žena	žena	k1gFnSc1	žena
překonala	překonat	k5eAaPmAgFnS	překonat
Lamanšský	lamanšský	k2eAgInSc4d1	lamanšský
průliv	průliv	k1gInSc4	průliv
na	na	k7c4	na
Stream	Stream	k1gInSc4	Stream
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
