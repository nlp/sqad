<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
část	část	k1gFnSc1	část
Atlantského	atlantský	k2eAgInSc2d1	atlantský
oceánu	oceán	k1gInSc2	oceán
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
ostrov	ostrov	k1gInSc4	ostrov
Velká	velká	k1gFnSc1	velká
Británie	Británie	k1gFnSc1	Británie
a	a	k8xC	a
severní	severní	k2eAgFnSc6d1	severní
Franii	Franie	k1gFnSc6	Franie
<g/>
?	?	kIx.	?
</s>
