<s>
Pussycat	Pussycat	k5eAaPmF
Dolls	Dolls	k1gInSc4
</s>
<s>
The	The	k?
Pussycat	Pussycat	k1gFnPc1
Dolls	Dollsa	k1gFnPc2
</s>
<s>
Základní	základní	k2eAgFnSc2d1
informace	informace	k1gFnSc1
</s>
<s>
Jinak	jinak	k6eAd1
zvaný	zvaný	k2eAgInSc1d1
<g/>
/	/	kIx~
<g/>
á	á	k0
PCD	PCD	kA
</s>
<s>
Původ	původ	k1gInSc1
Los	los	k1gMnSc1
Angeles	Angeles	k1gMnSc1
<g/>
,	,	kIx,
USA	USA	kA
Žánry	žánr	k1gInPc7
</s>
<s>
Pop	pop	k1gInSc1
R	R	kA
<g/>
&	&	k?
<g/>
B	B	kA
Dance	Danka	k1gFnSc6
Aktivní	aktivní	k2eAgInPc4d1
roky	rok	k1gInPc4
</s>
<s>
2003	#num#	k4
–	–	k?
<g/>
2010	#num#	k4
a	a	k8xC
2019	#num#	k4
-	-	kIx~
dosud	dosud	k6eAd1
Vydavatelé	vydavatel	k1gMnPc1
</s>
<s>
Interscope	Interscop	k1gMnSc5
<g/>
,	,	kIx,
A	a	k9
<g/>
&	&	k?
<g/>
M	M	kA
Ocenění	ocenění	k1gNnSc1
</s>
<s>
Mnet	Mnet	k2eAgMnSc1d1
Asian	Asian	k1gMnSc1
Music	Music	k1gMnSc1
Award	Award	k1gMnSc1
for	forum	k1gNnPc2
Best	Best	k1gMnSc1
International	International	k1gMnSc1
Artist	Artist	k1gMnSc1
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
<g/>
MTV	MTV	kA
Video	video	k1gNnSc1
Music	Music	k1gMnSc1
Award	Award	k1gMnSc1
for	forum	k1gNnPc2
Best	Best	k1gMnSc1
Dance	Danka	k1gFnSc6
Video	video	k1gNnSc1
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
<g/>
MTV	MTV	kA
Video	video	k1gNnSc1
Music	Music	k1gMnSc1
Award	Award	k1gMnSc1
for	forum	k1gNnPc2
Best	Best	k1gMnSc1
Dance	Danka	k1gFnSc6
Video	video	k1gNnSc1
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
Web	web	k1gInSc1
</s>
<s>
www.pcdmusic.com	www.pcdmusic.com	k1gInSc1
Některá	některý	k3yIgNnPc4
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
The	The	k?
Pussycat	Pussycat	k1gFnPc1
Dolls	Dollsa	k1gFnPc2
jsou	být	k5eAaImIp3nP
americká	americký	k2eAgFnSc1d1
dívčí	dívčí	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
založená	založený	k2eAgFnSc1d1
choreografkou	choreografka	k1gFnSc7
Robin	robin	k2eAgInSc4d1
Antinovou	Antinová	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Vznikly	vzniknout	k5eAaPmAgInP
v	v	k7c6
roce	rok	k1gInSc6
1995	#num#	k4
jako	jako	k8xC,k8xS
čistě	čistě	k6eAd1
taneční	taneční	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
od	od	k7c2
roku	rok	k1gInSc2
2003	#num#	k4
v	v	k7c6
sestavě	sestava	k1gFnSc6
Nicole	Nicole	k1gFnSc2
Scherzinger	Scherzinger	k1gInSc1
<g/>
,	,	kIx,
Melody	Meloda	k1gFnPc1
Thornton	Thornton	k1gInSc1
<g/>
,	,	kIx,
Carmit	Carmit	k1gInSc1
Bachar	Bachara	k1gFnPc2
<g/>
,	,	kIx,
Jessica	Jessic	k2eAgNnPc1d1
Sutta	Sutto	k1gNnPc1
<g/>
,	,	kIx,
Kimberly	Kimberl	k1gInPc1
Wyatt	Wyatta	k1gFnPc2
a	a	k8xC
Ashley	Ashlea	k1gFnSc2
Roberts	Robertsa	k1gFnPc2
působila	působit	k5eAaImAgFnS
i	i	k9
jako	jako	k9
skupina	skupina	k1gFnSc1
pěvecká	pěvecký	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
se	se	k3xPyFc4
k	k	k7c3
ní	on	k3xPp3gFnSc3
měla	mít	k5eAaImAgFnS
připojit	připojit	k5eAaPmF
Asia	Asia	k1gFnSc1
Nitollan	Nitollany	k1gInPc2
<g/>
,	,	kIx,
ta	ten	k3xDgFnSc1
se	se	k3xPyFc4
ale	ale	k8xC
pak	pak	k6eAd1
rozhodla	rozhodnout	k5eAaPmAgFnS
pro	pro	k7c4
sólovou	sólový	k2eAgFnSc4d1
dráhu	dráha	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
skupinu	skupina	k1gFnSc4
opustila	opustit	k5eAaPmAgFnS
Bachar	Bachar	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skupina	skupina	k1gFnSc1
fungovala	fungovat	k5eAaImAgFnS
dál	daleko	k6eAd2
i	i	k9
bez	bez	k7c2
ní	on	k3xPp3gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skupinu	skupina	k1gFnSc4
postupně	postupně	k6eAd1
opustily	opustit	k5eAaPmAgInP
všechny	všechen	k3xTgFnPc4
členky	členka	k1gFnPc4
a	a	k8xC
zbyla	zbýt	k5eAaPmAgFnS
v	v	k7c6
ní	on	k3xPp3gFnSc6
jenom	jenom	k9
Scherzinger	Scherzingra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Robin	Robina	k1gFnPc2
k	k	k7c3
ní	on	k3xPp3gFnSc3
tedy	tedy	k8xC
přidala	přidat	k5eAaPmAgFnS
čtyři	čtyři	k4xCgFnPc4
nové	nový	k2eAgFnPc4d1
členky	členka	k1gFnPc4
<g/>
,	,	kIx,
ale	ale	k8xC
pak	pak	k6eAd1
skupinu	skupina	k1gFnSc4
opustila	opustit	k5eAaPmAgFnS
i	i	k9
Scherzinger	Scherzinger	k1gInSc1
a	a	k8xC
skupina	skupina	k1gFnSc1
se	se	k3xPyFc4
rozpadla	rozpadnout	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stalo	stát	k5eAaPmAgNnS
se	se	k3xPyFc4
tak	tak	k6eAd1
roku	rok	k1gInSc2
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Robin	robin	k2eAgInSc1d1
teda	teda	k?
začala	začít	k5eAaPmAgFnS
hledat	hledat	k5eAaImF
další	další	k2eAgFnPc4d1
nové	nový	k2eAgFnPc4d1
členky	členka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vystřídalo	vystřídat	k5eAaPmAgNnS
se	se	k3xPyFc4
jich	on	k3xPp3gMnPc2
několik	několik	k4yIc4
<g/>
,	,	kIx,
ale	ale	k8xC
koncem	koncem	k7c2
roku	rok	k1gInSc2
2012	#num#	k4
už	už	k9
byla	být	k5eAaImAgFnS
známa	znám	k2eAgFnSc1d1
finální	finální	k2eAgFnSc1d1
formace	formace	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
Pussycat	Pussycat	k1gFnSc1
Dolls	Dollsa	k1gFnPc2
fungovala	fungovat	k5eAaImAgFnS
do	do	k7c2
začátku	začátek	k1gInSc2
roku	rok	k1gInSc2
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Potom	potom	k6eAd1
jí	jíst	k5eAaImIp3nS
Robin	robin	k2eAgInSc1d1
změnila	změnit	k5eAaPmAgFnS
jméno	jméno	k1gNnSc4
z	z	k7c2
důvodu	důvod	k1gInSc2
možného	možný	k2eAgInSc2d1
návratu	návrat	k1gInSc2
původní	původní	k2eAgFnSc2d1
formace	formace	k1gFnSc2
a	a	k8xC
funguje	fungovat	k5eAaImIp3nS
jako	jako	k9
úplně	úplně	k6eAd1
nová	nový	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Dne	den	k1gInSc2
21	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2009	#num#	k4
vystoupila	vystoupit	k5eAaPmAgFnS
skupina	skupina	k1gFnSc1
v	v	k7c6
rámci	rámec	k1gInSc6
Doll	Doll	k1gInSc1
Domination	Domination	k1gInSc1
World	World	k1gInSc1
Tour	Tour	k1gInSc1
v	v	k7c6
pražské	pražský	k2eAgFnSc6d1
Tesla	Tesla	k1gFnSc1
Aréně	aréna	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
o	o	k7c4
jejich	jejich	k3xOp3gNnSc4
vůbec	vůbec	k9
první	první	k4xOgNnSc4
vystoupení	vystoupení	k1gNnSc4
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pussycat	Pussycat	k5eAaPmF
dolls	dolls	k6eAd1
se	se	k3xPyFc4
vrátily	vrátit	k5eAaPmAgFnP
v	v	k7c6
roce	rok	k1gInSc6
2019	#num#	k4
kdy	kdy	k6eAd1
oznámily	oznámit	k5eAaPmAgInP
comeback	comeback	k6eAd1
<g/>
,	,	kIx,
vydali	vydat	k5eAaPmAgMnP
svůj	svůj	k3xOyFgInSc4
první	první	k4xOgInSc4
singl	singl	k1gInSc4
po	po	k7c6
10	#num#	k4
letech	léto	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s>
Do	do	k7c2
skupiny	skupina	k1gFnSc2
se	se	k3xPyFc4
vrátily	vrátit	k5eAaPmAgFnP
všechny	všechen	k3xTgFnPc1
členky	členka	k1gFnPc1
kromě	kromě	k7c2
Melody	Meloda	k1gFnSc2
Thornton	Thornton	k1gInSc1
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
oznámila	oznámit	k5eAaPmAgFnS
comeback	comeback	k1gInSc4
jako	jako	k8xC,k8xS
noční	noční	k2eAgFnSc4d1
můru	můra	k1gFnSc4
ostatní	ostatní	k2eAgFnSc2d1
panenky	panenka	k1gFnSc2
věří	věřit	k5eAaImIp3nS
že	že	k8xS
se	se	k3xPyFc4
Melody	Meloda	k1gFnPc1
k	k	k7c3
nim	on	k3xPp3gInPc3
z	z	k7c2
později	pozdě	k6eAd2
připojí	připojit	k5eAaPmIp3nS
a	a	k8xC
tak	tak	k6eAd1
ji	on	k3xPp3gFnSc4
nechávají	nechávat	k5eAaImIp3nP
otevřené	otevřený	k2eAgFnPc4d1
dveře	dveře	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Videoklipy	videoklip	k1gInPc1
</s>
<s>
Z	z	k7c2
alba	album	k1gNnSc2
PCD	PCD	kA
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
</s>
<s>
Don	Don	k1gMnSc1
<g/>
'	'	kIx"
<g/>
t	t	k?
Cha	cha	k0
(	(	kIx(
<g/>
feat	feata	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Busta	busta	k1gFnSc1
Rhymes	Rhymesa	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
Beep	Beep	k1gMnSc1
(	(	kIx(
<g/>
feat	feat	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Will	Will	k1gInSc1
<g/>
.	.	kIx.
<g/>
I.	I.	kA
<g/>
Am	Am	k1gFnSc6
<g/>
)	)	kIx)
</s>
<s>
Wait	Wait	k1gInSc1
a	a	k8xC
Minute	Minut	k1gInSc5
(	(	kIx(
<g/>
feat	feata	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Timbaland	Timbaland	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Stickwitu	Stickwita	k1gFnSc4
</s>
<s>
Buttons	Buttons	k1gInSc1
(	(	kIx(
<g/>
feat	feat	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Snoop	Snoop	k1gInSc1
Dogg	Dogg	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
I	i	k9
Don	Don	k1gMnSc1
<g/>
'	'	kIx"
<g/>
t	t	k?
Need	Need	k1gMnSc1
a	a	k8xC
Man	Man	k1gMnSc1
</s>
<s>
Sway	Swaa	k1gFnPc1
</s>
<s>
Z	z	k7c2
alba	album	k1gNnSc2
Doll	Dolla	k1gFnPc2
Domination	Domination	k1gInSc1
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
</s>
<s>
When	When	k1gMnSc1
I	i	k8xC
Grow	Grow	k1gMnSc1
Up	Up	k1gMnSc1
</s>
<s>
Bottle	Bottlat	k5eAaPmIp3nS
Pop	pop	k1gInSc1
(	(	kIx(
<g/>
feat	feat	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Snoop	Snoop	k1gInSc1
Dogg	Dogg	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
Whatcha	Whatcha	k1gMnSc1
Think	Think	k1gMnSc1
About	About	k1gMnSc1
That	That	k1gMnSc1
(	(	kIx(
<g/>
feat	feat	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Missy	Missa	k1gFnSc2
Elliott	Elliotta	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
I	i	k9
Hate	Hate	k1gNnSc1
This	Thisa	k1gFnPc2
Part	parta	k1gFnPc2
</s>
<s>
Jai	Jai	k?
Ho	on	k3xPp3gMnSc2
[	[	kIx(
<g/>
You	You	k1gMnSc2
Are	ar	k1gInSc5
My	my	k3xPp1nPc1
Destiny	Destin	k1gInPc4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
feat	feat	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
A.	A.	kA
R.	R.	kA
Rahman	Rahman	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Hush	Hush	k1gMnSc1
Hush	Hush	k1gMnSc1
(	(	kIx(
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
2009	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
React	React	k1gInSc1
-	-	kIx~
2020	#num#	k4
</s>
<s>
Diskografie	diskografie	k1gFnSc1
</s>
<s>
Studiová	studiový	k2eAgNnPc1d1
alba	album	k1gNnPc1
</s>
<s>
2005	#num#	k4
<g/>
:	:	kIx,
PCD	PCD	kA
</s>
<s>
2008	#num#	k4
<g/>
:	:	kIx,
Doll	Doll	k1gInSc1
Domination	Domination	k1gInSc1
</s>
<s>
2009	#num#	k4
<g/>
:	:	kIx,
Doll	Doll	k1gInSc1
Domination	Domination	k1gInSc1
2.0	2.0	k4
</s>
<s>
2009	#num#	k4
Doll	Doll	k1gInSc1
Domination	Domination	k1gInSc4
<g/>
:	:	kIx,
The	The	k1gMnSc2
Mini	mini	k2eAgInSc1d1
Collection	Collection	k1gInSc1
</s>
<s>
2009	#num#	k4
Doll	Doll	k1gMnSc1
Domination	Domination	k1gInSc4
3.0	3.0	k4
</s>
<s>
DVD	DVD	kA
</s>
<s>
2006	#num#	k4
<g/>
:	:	kIx,
Live	Liv	k1gInSc2
from	from	k1gMnSc1
London	London	k1gMnSc1
</s>
<s>
Singly	singl	k1gInPc1
</s>
<s>
2004	#num#	k4
<g/>
:	:	kIx,
„	„	k?
<g/>
Sway	Swaa	k1gFnSc2
<g/>
“	“	k?
</s>
<s>
2005	#num#	k4
<g/>
:	:	kIx,
„	„	k?
<g/>
Don	Don	k1gMnSc1
<g/>
'	'	kIx"
<g/>
t	t	k?
Cha	cha	k0
<g/>
“	“	k?
</s>
<s>
2005	#num#	k4
<g/>
:	:	kIx,
„	„	k?
<g/>
Stickwitu	Stickwit	k1gInSc2
<g/>
“	“	k?
</s>
<s>
2006	#num#	k4
<g/>
:	:	kIx,
„	„	k?
<g/>
Beep	Beep	k1gInSc1
<g/>
“	“	k?
</s>
<s>
2006	#num#	k4
<g/>
:	:	kIx,
„	„	k?
<g/>
Buttons	Buttons	k1gInSc1
<g/>
“	“	k?
</s>
<s>
2006	#num#	k4
<g/>
:	:	kIx,
„	„	k?
<g/>
I	i	k8xC
Don	Don	k1gMnSc1
<g/>
'	'	kIx"
<g/>
t	t	k?
Need	Need	k1gMnSc1
A	a	k8xC
Man	Man	k1gMnSc1
<g/>
“	“	k?
</s>
<s>
2007	#num#	k4
<g/>
:	:	kIx,
„	„	k?
<g/>
Wait	Wait	k1gInSc1
a	a	k8xC
Minute	Minut	k1gInSc5
<g/>
“	“	k?
</s>
<s>
2008	#num#	k4
<g/>
:	:	kIx,
„	„	k?
<g/>
When	When	k1gMnSc1
I	i	k8xC
Grow	Grow	k1gMnSc1
Up	Up	k1gFnSc2
<g/>
“	“	k?
</s>
<s>
2008	#num#	k4
<g/>
:	:	kIx,
„	„	k?
<g/>
I	i	k9
Hate	Hate	k1gNnSc1
This	Thisa	k1gFnPc2
Part	parta	k1gFnPc2
<g/>
“	“	k?
</s>
<s>
2008	#num#	k4
<g/>
:	:	kIx,
„	„	k?
<g/>
Bottle	Bottle	k1gFnSc1
Pop	pop	k1gInSc1
<g/>
“	“	k?
</s>
<s>
2008	#num#	k4
<g/>
:	:	kIx,
„	„	k?
<g/>
Magic	Magic	k1gMnSc1
<g/>
“	“	k?
</s>
<s>
2008	#num#	k4
<g/>
:	:	kIx,
„	„	k?
<g/>
Hush	Hush	k1gMnSc1
Hush	Hush	k1gMnSc1
<g/>
“	“	k?
</s>
<s>
2008	#num#	k4
<g/>
:	:	kIx,
„	„	k?
<g/>
Halo	halo	k1gNnSc4
<g/>
“	“	k?
</s>
<s>
2008	#num#	k4
<g/>
:	:	kIx,
„	„	k?
<g/>
Whatcha	Whatcha	k1gMnSc1
Think	Think	k1gMnSc1
About	About	k1gMnSc1
That	That	k1gMnSc1
<g/>
“	“	k?
</s>
<s>
2008	#num#	k4
<g/>
:	:	kIx,
„	„	k?
<g/>
Takin	Takin	k1gInSc1
<g/>
'	'	kIx"
Over	Over	k1gInSc1
the	the	k?
World	World	k1gInSc1
<g/>
“	“	k?
</s>
<s>
2008	#num#	k4
<g/>
:	:	kIx,
„	„	k?
<g/>
Whatchamacalit	Whatchamacalit	k1gInSc1
<g/>
“	“	k?
</s>
<s>
2008	#num#	k4
<g/>
:	:	kIx,
„	„	k?
<g/>
Elevator	Elevator	k1gInSc1
<g/>
“	“	k?
</s>
<s>
2008	#num#	k4
<g/>
:	:	kIx,
„	„	k?
<g/>
Happily	Happily	k1gMnSc1
Never	Never	k1gMnSc1
After	After	k1gMnSc1
<g/>
“	“	k?
</s>
<s>
2008	#num#	k4
<g/>
:	:	kIx,
„	„	k?
<g/>
Perhaps	Perhaps	k1gInSc1
Perhaps	Perhaps	k1gInSc1
Perhaps	Perhaps	k1gInSc1
<g/>
“	“	k?
</s>
<s>
2008	#num#	k4
<g/>
:	:	kIx,
„	„	k?
<g/>
I	I	kA
<g/>
'	'	kIx"
<g/>
m	m	kA
Done	Don	k1gMnSc5
<g/>
“	“	k?
</s>
<s>
2008	#num#	k4
<g/>
:	:	kIx,
„	„	k?
<g/>
Love	lov	k1gInSc5
the	the	k?
Way	Way	k1gMnSc5
You	You	k1gMnSc5
Love	lov	k1gInSc5
Me	Me	k1gFnPc3
<g/>
“	“	k?
</s>
<s>
2008	#num#	k4
<g/>
:	:	kIx,
„	„	k?
<g/>
Out	Out	k1gMnSc1
of	of	k?
This	This	k1gInSc1
Club	club	k1gInSc1
<g/>
“	“	k?
</s>
<s>
2008	#num#	k4
<g/>
:	:	kIx,
„	„	k?
<g/>
Who	Who	k1gFnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Gonna	Gonna	k1gFnSc1
Love	lov	k1gInSc5
You	You	k1gMnPc6
<g/>
“	“	k?
</s>
<s>
2008	#num#	k4
<g/>
:	:	kIx,
„	„	k?
<g/>
In	In	k1gFnSc2
Person	persona	k1gFnPc2
<g/>
“	“	k?
</s>
<s>
2009	#num#	k4
<g/>
:	:	kIx,
„	„	k?
<g/>
Bad	Bad	k1gFnSc1
Girl	girl	k1gFnSc1
<g/>
“	“	k?
</s>
<s>
2009	#num#	k4
<g/>
:	:	kIx,
„	„	k?
<g/>
Top	topit	k5eAaImRp2nS
of	of	k?
the	the	k?
World	World	k1gInSc1
<g/>
“	“	k?
</s>
<s>
2009	#num#	k4
<g/>
:	:	kIx,
„	„	k?
<g/>
Jai	Jai	k1gFnSc1
Ho	on	k3xPp3gMnSc2
(	(	kIx(
<g/>
You	You	k1gMnSc2
Are	ar	k1gInSc5
My	my	k3xPp1nPc1
Destiny	Destin	k2eAgInPc4d1
<g/>
)	)	kIx)
<g/>
“	“	k?
</s>
<s>
2009	#num#	k4
<g/>
:	:	kIx,
Painted	Painted	k1gInSc1
windows	windows	k6eAd1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Pussycat	Pussycat	k1gFnSc2
Dolls	Dollsa	k1gFnPc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
http://musicserver.cz/clanek/24964/Pussycat-Dolls-Victoria-Tesla-Arena-Praha-21-2-2009/	http://musicserver.cz/clanek/24964/Pussycat-Dolls-Victoria-Tesla-Arena-Praha-21-2-2009/	k4
</s>
<s>
Související	související	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Dívčí	dívčí	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Pussycat	Pussycat	k1gFnSc2
Dolls	Dolls	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Galerie	galerie	k1gFnSc1
Pussycat	Pussycat	k1gMnPc2
Dolls	Dollsa	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
internetové	internetový	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
Pussycat	Pussycat	k1gFnSc2
Dolls	Dollsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Hudba	hudba	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
xx	xx	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
53713	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
10338946-5	10338946-5	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
0180	#num#	k4
2687	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
2005052283	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
151671171	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
2005052283	#num#	k4
</s>
