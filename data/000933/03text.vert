<s>
Hliník	hliník	k1gInSc1	hliník
(	(	kIx(	(
<g/>
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Al	ala	k1gFnPc2	ala
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Aluminium	aluminium	k1gNnSc1	aluminium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
lehký	lehký	k2eAgInSc1d1	lehký
kov	kov	k1gInSc1	kov
bělavě	bělavě	k6eAd1	bělavě
šedé	šedý	k2eAgFnSc2d1	šedá
barvy	barva	k1gFnSc2	barva
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
dobrý	dobrý	k2eAgInSc1d1	dobrý
vodič	vodič	k1gInSc1	vodič
elektrického	elektrický	k2eAgInSc2d1	elektrický
proudu	proud	k1gInSc2	proud
<g/>
,	,	kIx,	,
široce	široko	k6eAd1	široko
používaný	používaný	k2eAgInSc1d1	používaný
v	v	k7c6	v
elektrotechnice	elektrotechnika	k1gFnSc6	elektrotechnika
a	a	k8xC	a
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
slitin	slitina	k1gFnPc2	slitina
v	v	k7c6	v
leteckém	letecký	k2eAgInSc6d1	letecký
průmyslu	průmysl	k1gInSc6	průmysl
a	a	k8xC	a
mnoha	mnoho	k4c6	mnoho
dalších	další	k2eAgFnPc6d1	další
aplikacích	aplikace	k1gFnPc6	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
Neušlechtilý	ušlechtilý	k2eNgMnSc1d1	neušlechtilý
stříbřitě	stříbřitě	k6eAd1	stříbřitě
šedý	šedý	k2eAgInSc4d1	šedý
<g/>
,	,	kIx,	,
nestálý	stálý	k2eNgInSc4d1	nestálý
<g/>
,	,	kIx,	,
kujný	kujný	k2eAgInSc4d1	kujný
kov	kov	k1gInSc4	kov
<g/>
,	,	kIx,	,
elektricky	elektricky	k6eAd1	elektricky
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
vodivý	vodivý	k2eAgInSc1d1	vodivý
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
teplotách	teplota	k1gFnPc6	teplota
pod	pod	k7c4	pod
1,18	[number]	k4	1,18
K	K	kA	K
je	být	k5eAaImIp3nS	být
supravodivý	supravodivý	k2eAgInSc1d1	supravodivý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
zejména	zejména	k9	zejména
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
,	,	kIx,	,
nejznámější	známý	k2eAgFnSc7d3	nejznámější
rudou	ruda	k1gFnSc7	ruda
je	být	k5eAaImIp3nS	být
bauxit	bauxit	k1gInSc4	bauxit
Al	ala	k1gFnPc2	ala
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
3	[number]	k4	3
.	.	kIx.	.
2	[number]	k4	2
H2O	H2O	k1gFnPc2	H2O
(	(	kIx(	(
<g/>
dihydrát	dihydrát	k1gInSc1	dihydrát
oxidu	oxid	k1gInSc2	oxid
hlinitého	hlinitý	k2eAgInSc2d1	hlinitý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vytváří	vytvářit	k5eAaPmIp3nP	vytvářit
sloučeniny	sloučenina	k1gFnPc1	sloučenina
v	v	k7c6	v
oxidačních	oxidační	k2eAgNnPc6d1	oxidační
číslech	číslo	k1gNnPc6	číslo
+	+	kIx~	+
<g/>
I	i	k8xC	i
až	až	k9	až
+	+	kIx~	+
<g/>
III	III	kA	III
<g/>
,	,	kIx,	,
nejběžnější	běžný	k2eAgFnPc1d3	nejběžnější
a	a	k8xC	a
nejstabilnější	stabilní	k2eAgFnPc1d3	nejstabilnější
jsou	být	k5eAaImIp3nP	být
sloučeniny	sloučenina	k1gFnPc1	sloučenina
hlinité	hlinitý	k2eAgFnPc1d1	hlinitá
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kyselém	kyselý	k2eAgNnSc6d1	kyselé
prostředí	prostředí	k1gNnSc6	prostředí
tvoří	tvořit	k5eAaImIp3nS	tvořit
ve	v	k7c6	v
vodném	vodné	k1gNnSc6	vodné
roztoku	roztok	k1gInSc2	roztok
hlinitý	hlinitý	k2eAgInSc4d1	hlinitý
kation	kation	k1gInSc4	kation
<g/>
,	,	kIx,	,
v	v	k7c6	v
alkalickém	alkalický	k2eAgNnSc6d1	alkalické
prostředí	prostředí	k1gNnSc6	prostředí
pak	pak	k6eAd1	pak
hlinitanový	hlinitanový	k2eAgInSc4d1	hlinitanový
anion	anion	k1gInSc4	anion
[	[	kIx(	[
<g/>
AlO	ala	k1gFnSc5	ala
<g/>
2	[number]	k4	2
<g/>
]	]	kIx)	]
<g/>
-	-	kIx~	-
<g/>
.	.	kIx.	.
</s>
<s>
Hliník	hliník	k1gInSc1	hliník
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
čistém	čistý	k2eAgInSc6d1	čistý
stavu	stav	k1gInSc6	stav
velmi	velmi	k6eAd1	velmi
reaktivní	reaktivní	k2eAgInSc1d1	reaktivní
<g/>
,	,	kIx,	,
na	na	k7c6	na
vzduchu	vzduch	k1gInSc6	vzduch
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
pokryje	pokrýt	k5eAaPmIp3nS	pokrýt
tenkou	tenký	k2eAgFnSc7d1	tenká
vrstvičkou	vrstvička	k1gFnSc7	vrstvička
oxidu	oxid	k1gInSc2	oxid
Al	ala	k1gFnPc2	ala
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
chrání	chránit	k5eAaImIp3nS	chránit
kov	kov	k1gInSc4	kov
před	před	k7c7	před
další	další	k2eAgFnSc7d1	další
oxidací	oxidace	k1gFnSc7	oxidace
<g/>
.	.	kIx.	.
</s>
<s>
Hliník	hliník	k1gInSc1	hliník
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
rozpustný	rozpustný	k2eAgInSc1d1	rozpustný
ve	v	k7c6	v
zředěných	zředěný	k2eAgFnPc6d1	zředěná
kyselinách	kyselina	k1gFnPc6	kyselina
<g/>
,	,	kIx,	,
koncentrovaná	koncentrovaný	k2eAgFnSc1d1	koncentrovaná
kyselina	kyselina	k1gFnSc1	kyselina
dusičná	dusičný	k2eAgFnSc1d1	dusičná
jej	on	k3xPp3gMnSc4	on
však	však	k9	však
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
vzdušný	vzdušný	k2eAgInSc1d1	vzdušný
kyslík	kyslík	k1gInSc1	kyslík
pokryje	pokrýt	k5eAaPmIp3nS	pokrýt
pasivační	pasivační	k2eAgFnSc7d1	pasivační
vrstvou	vrstva	k1gFnSc7	vrstva
oxidu	oxid	k1gInSc2	oxid
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
hydroxidy	hydroxid	k1gInPc1	hydroxid
alkalických	alkalický	k2eAgInPc2d1	alkalický
kovů	kov	k1gInPc2	kov
snadno	snadno	k6eAd1	snadno
rozpouštějí	rozpouštět	k5eAaImIp3nP	rozpouštět
kovový	kovový	k2eAgInSc4d1	kovový
hliník	hliník	k1gInSc4	hliník
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
hlinitanů	hlinitan	k1gInPc2	hlinitan
(	(	kIx(	(
<g/>
AlO	ala	k1gFnSc5	ala
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
-	-	kIx~	-
<g/>
.	.	kIx.	.
</s>
<s>
Hliník	hliník	k1gInSc1	hliník
a	a	k8xC	a
slitiny	slitina	k1gFnPc1	slitina
hliníku	hliník	k1gInSc2	hliník
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
svařitelné	svařitelný	k2eAgNnSc1d1	svařitelné
téměř	téměř	k6eAd1	téměř
všemi	všecek	k3xTgFnPc7	všecek
metodami	metoda	k1gFnPc7	metoda
svařování	svařování	k1gNnSc2	svařování
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
je	být	k5eAaImIp3nS	být
slitina	slitina	k1gFnSc1	slitina
dural	dural	k1gInSc1	dural
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
svařitelná	svařitelný	k2eAgNnPc4d1	svařitelné
obtížně	obtížně	k6eAd1	obtížně
<g/>
.	.	kIx.	.
</s>
<s>
Hliník	hliník	k1gInSc1	hliník
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
kovové	kovový	k2eAgFnSc6d1	kovová
formě	forma	k1gFnSc6	forma
izolován	izolován	k2eAgInSc1d1	izolován
roku	rok	k1gInSc2	rok
1825	[number]	k4	1825
dánským	dánský	k2eAgMnSc7d1	dánský
fyzikem	fyzik	k1gMnSc7	fyzik
Hansem	Hans	k1gMnSc7	Hans
Christianem	Christian	k1gMnSc7	Christian
Ø	Ø	k1gMnSc7	Ø
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
velké	velký	k2eAgFnSc3d1	velká
reaktivitě	reaktivita	k1gFnSc3	reaktivita
hliníku	hliník	k1gInSc2	hliník
se	se	k3xPyFc4	se
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
setkáváme	setkávat	k5eAaImIp1nP	setkávat
prakticky	prakticky	k6eAd1	prakticky
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
jeho	jeho	k3xOp3gFnPc7	jeho
sloučeninami	sloučenina	k1gFnPc7	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Hliník	hliník	k1gInSc1	hliník
je	být	k5eAaImIp3nS	být
třetím	třetí	k4xOgFnPc3	třetí
nejvíce	hodně	k6eAd3	hodně
zastoupeným	zastoupený	k2eAgInSc7d1	zastoupený
prvkem	prvek	k1gInSc7	prvek
v	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
kůře	kůra	k1gFnSc6	kůra
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
posledních	poslední	k2eAgInPc2d1	poslední
dostupných	dostupný	k2eAgInPc2d1	dostupný
údajů	údaj	k1gInPc2	údaj
tvoří	tvořit	k5eAaImIp3nS	tvořit
hliník	hliník	k1gInSc1	hliník
7,5	[number]	k4	7,5
<g/>
-	-	kIx~	-
<g/>
8,3	[number]	k4	8,3
%	%	kIx~	%
zemské	zemský	k2eAgFnSc2d1	zemská
kůry	kůra	k1gFnSc2	kůra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mořské	mořský	k2eAgFnSc6d1	mořská
vodě	voda	k1gFnSc6	voda
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
koncentrace	koncentrace	k1gFnSc1	koncentrace
velmi	velmi	k6eAd1	velmi
nízká	nízký	k2eAgFnSc1d1	nízká
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
0,01	[number]	k4	0,01
mg	mg	kA	mg
Al	ala	k1gFnPc2	ala
<g/>
/	/	kIx~	/
<g/>
l	l	kA	l
a	a	k8xC	a
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
připadá	připadat	k5eAaImIp3nS	připadat
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
atom	atom	k1gInSc4	atom
hliníku	hliník	k1gInSc2	hliník
přibližně	přibližně	k6eAd1	přibližně
půl	půl	k1xP	půl
milionu	milion	k4xCgInSc2	milion
atomů	atom	k1gInPc2	atom
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Nejběžnější	běžný	k2eAgFnSc7d3	nejběžnější
horninou	hornina	k1gFnSc7	hornina
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
hliníku	hliník	k1gInSc2	hliník
je	být	k5eAaImIp3nS	být
bauxit	bauxit	k1gInSc1	bauxit
<g/>
,	,	kIx,	,
Al	ala	k1gFnPc2	ala
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
3	[number]	k4	3
·	·	k?	·
2	[number]	k4	2
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O.	O.	kA	O.
Obvykle	obvykle	k6eAd1	obvykle
bývá	bývat	k5eAaImIp3nS	bývat
doprovázen	doprovázet	k5eAaImNgInS	doprovázet
dalšími	další	k2eAgFnPc7d1	další
příměsemi	příměse	k1gFnPc7	příměse
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
oxidů	oxid	k1gInPc2	oxid
křemíku	křemík	k1gInSc2	křemík
<g/>
,	,	kIx,	,
titanu	titan	k1gInSc2	titan
<g/>
,	,	kIx,	,
železa	železo	k1gNnSc2	železo
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Jiným	jiný	k2eAgInSc7d1	jiný
významným	významný	k2eAgInSc7d1	významný
minerálem	minerál	k1gInSc7	minerál
je	být	k5eAaImIp3nS	být
kryolit	kryolit	k1gInSc4	kryolit
<g/>
,	,	kIx,	,
hexafluorohlinitan	hexafluorohlinitan	k1gInSc4	hexafluorohlinitan
sodný	sodný	k2eAgInSc4d1	sodný
Na	na	k7c4	na
<g/>
3	[number]	k4	3
<g/>
AlF	alfa	k1gFnPc2	alfa
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
používaný	používaný	k2eAgInSc1d1	používaný
především	především	k9	především
jako	jako	k9	jako
tavidlo	tavidlo	k1gNnSc4	tavidlo
pro	pro	k7c4	pro
snížení	snížení	k1gNnSc4	snížení
teploty	teplota	k1gFnSc2	teplota
tání	tání	k1gNnSc1	tání
oxidu	oxid	k1gInSc2	oxid
hlinitého	hlinitý	k2eAgInSc2d1	hlinitý
při	při	k7c6	při
elektrolytické	elektrolytický	k2eAgFnSc6d1	elektrolytická
výrobě	výroba	k1gFnSc6	výroba
hliníku	hliník	k1gInSc2	hliník
<g/>
.	.	kIx.	.
</s>
<s>
Minerály	minerál	k1gInPc1	minerál
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
oxidu	oxid	k1gInSc2	oxid
hlinitého	hlinitý	k2eAgInSc2d1	hlinitý
Al	ala	k1gFnPc2	ala
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
3	[number]	k4	3
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
velmi	velmi	k6eAd1	velmi
významné	významný	k2eAgInPc4d1	významný
i	i	k8xC	i
ceněné	ceněný	k2eAgInPc4d1	ceněný
<g/>
.	.	kIx.	.
</s>
<s>
Korund	korund	k1gInSc1	korund
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
9	[number]	k4	9
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
Mohsovy	Mohsův	k2eAgFnSc2d1	Mohsova
stupnice	stupnice	k1gFnSc2	stupnice
tvrdosti	tvrdost	k1gFnSc2	tvrdost
<g/>
.	.	kIx.	.
</s>
<s>
Technický	technický	k2eAgInSc1d1	technický
oxid	oxid	k1gInSc1	oxid
hlinitý	hlinitý	k2eAgInSc1d1	hlinitý
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
také	také	k9	také
elektrit	elektrit	k1gInSc4	elektrit
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
hojně	hojně	k6eAd1	hojně
využíván	využívat	k5eAaPmNgInS	využívat
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
brusného	brusný	k2eAgInSc2d1	brusný
papíru	papír	k1gInSc2	papír
<g/>
.	.	kIx.	.
</s>
<s>
Drahé	drahý	k2eAgInPc4d1	drahý
kameny	kámen	k1gInPc4	kámen
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
základním	základní	k2eAgInSc7d1	základní
materiálem	materiál	k1gInSc7	materiál
je	být	k5eAaImIp3nS	být
oxid	oxid	k1gInSc4	oxid
hlinitý	hlinitý	k2eAgInSc4d1	hlinitý
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
příměsí	příměs	k1gFnSc7	příměs
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
jejich	jejich	k3xOp3gFnSc4	jejich
charakteristické	charakteristický	k2eAgNnSc1d1	charakteristické
zbarvení	zbarvení	k1gNnSc1	zbarvení
<g/>
.	.	kIx.	.
</s>
<s>
Červený	červený	k2eAgInSc1d1	červený
rubín	rubín	k1gInSc1	rubín
je	být	k5eAaImIp3nS	být
zbarven	zbarvit	k5eAaPmNgInS	zbarvit
příměsí	příměs	k1gFnSc7	příměs
oxidu	oxid	k1gInSc2	oxid
chromu	chrom	k1gInSc2	chrom
<g/>
,	,	kIx,	,
modrý	modrý	k2eAgInSc1d1	modrý
safír	safír	k1gInSc1	safír
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
především	především	k9	především
stopová	stopová	k1gFnSc1	stopová
množství	množství	k1gNnSc2	množství
oxidů	oxid	k1gInPc2	oxid
titanu	titan	k1gInSc2	titan
a	a	k8xC	a
železa	železo	k1gNnSc2	železo
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
zmíněné	zmíněný	k2eAgFnPc1d1	zmíněná
formy	forma	k1gFnPc1	forma
korundu	korund	k1gInSc2	korund
patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
nejvíce	hodně	k6eAd3	hodně
ceněným	ceněný	k2eAgInPc3d1	ceněný
drahým	drahý	k2eAgInPc3d1	drahý
kamenům	kámen	k1gInPc3	kámen
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mají	mít	k5eAaImIp3nP	mít
i	i	k9	i
významné	významný	k2eAgNnSc4d1	významné
využití	využití	k1gNnSc4	využití
v	v	k7c6	v
technice	technika	k1gFnSc6	technika
<g/>
.	.	kIx.	.
</s>
<s>
Safírové	safírový	k2eAgInPc1d1	safírový
hroty	hrot	k1gInPc1	hrot
vynikají	vynikat	k5eAaImIp3nP	vynikat
svou	svůj	k3xOyFgFnSc7	svůj
tvrdostí	tvrdost	k1gFnSc7	tvrdost
a	a	k8xC	a
odolností	odolnost	k1gFnSc7	odolnost
a	a	k8xC	a
vybavují	vybavovat	k5eAaImIp3nP	vybavovat
se	se	k3xPyFc4	se
jimi	on	k3xPp3gFnPc7	on
špičkové	špičkový	k2eAgInPc4d1	špičkový
vědecké	vědecký	k2eAgInPc4d1	vědecký
měřicí	měřicí	k2eAgInPc4d1	měřicí
přístroje	přístroj	k1gInPc4	přístroj
<g/>
.	.	kIx.	.
</s>
<s>
Rubín	rubín	k1gInSc1	rubín
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
jako	jako	k8xC	jako
materiál	materiál	k1gInSc1	materiál
pro	pro	k7c4	pro
konstrukci	konstrukce	k1gFnSc4	konstrukce
prvního	první	k4xOgInSc2	první
laseru	laser	k1gInSc2	laser
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Titan-safírový	Titanafírový	k2eAgInSc1d1	Titan-safírový
laser	laser	k1gInSc1	laser
vyniká	vynikat	k5eAaImIp3nS	vynikat
extrémně	extrémně	k6eAd1	extrémně
krátkými	krátký	k2eAgInPc7d1	krátký
pulsy	puls	k1gInPc7	puls
(	(	kIx(	(
<g/>
<	<	kIx(	<
50	[number]	k4	50
fs	fs	k?	fs
<g/>
)	)	kIx)	)
Přestože	přestože	k8xS	přestože
hliník	hliník	k1gInSc1	hliník
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c7	mezi
prvky	prvek	k1gInPc7	prvek
nejvíce	nejvíce	k6eAd1	nejvíce
zastoupené	zastoupený	k2eAgNnSc1d1	zastoupené
v	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
kůře	kůra	k1gFnSc6	kůra
<g/>
,	,	kIx,	,
patřila	patřit	k5eAaImAgFnS	patřit
jeho	jeho	k3xOp3gFnSc1	jeho
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
výroba	výroba	k1gFnSc1	výroba
do	do	k7c2	do
ještě	ještě	k6eAd1	ještě
poměrně	poměrně	k6eAd1	poměrně
nedávné	dávný	k2eNgFnSc2d1	nedávná
doby	doba	k1gFnSc2	doba
k	k	k7c3	k
velmi	velmi	k6eAd1	velmi
obtížným	obtížný	k2eAgMnPc3d1	obtížný
procesům	proces	k1gInPc3	proces
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
především	především	k9	především
z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
<g/>
,	,	kIx,	,
že	že	k8xS	že
elementární	elementární	k2eAgInSc1d1	elementární
hliník	hliník	k1gInSc1	hliník
nelze	lze	k6eNd1	lze
jednoduše	jednoduše	k6eAd1	jednoduše
metalurgicky	metalurgicky	k6eAd1	metalurgicky
vyredukovat	vyredukovat	k5eAaPmF	vyredukovat
z	z	k7c2	z
jeho	jeho	k3xOp3gFnSc2	jeho
rudy	ruda	k1gFnSc2	ruda
jako	jako	k8xS	jako
např.	např.	kA	např.
železo	železo	k1gNnSc1	železo
koksem	koks	k1gInSc7	koks
ve	v	k7c6	v
vysoké	vysoký	k2eAgFnSc6d1	vysoká
peci	pec	k1gFnSc6	pec
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
zvládnutí	zvládnutí	k1gNnSc1	zvládnutí
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
elektrolýzy	elektrolýza	k1gFnSc2	elektrolýza
taveniny	tavenina	k1gFnSc2	tavenina
kovových	kovový	k2eAgFnPc2d1	kovová
rud	ruda	k1gFnPc2	ruda
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
současnou	současný	k2eAgFnSc4d1	současná
mnohasettunovou	mnohasettunový	k2eAgFnSc4d1	mnohasettunový
roční	roční	k2eAgFnSc4d1	roční
produkci	produkce	k1gFnSc4	produkce
čistého	čistý	k2eAgInSc2d1	čistý
hliníku	hliník	k1gInSc2	hliník
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
elektrolýze	elektrolýza	k1gFnSc6	elektrolýza
se	se	k3xPyFc4	se
z	z	k7c2	z
taveniny	tavenina	k1gFnSc2	tavenina
směsi	směs	k1gFnSc2	směs
předem	předem	k6eAd1	předem
přečištěného	přečištěný	k2eAgInSc2d1	přečištěný
bauxitu	bauxit	k1gInSc2	bauxit
a	a	k8xC	a
kryolitu	kryolit	k1gInSc2	kryolit
o	o	k7c6	o
teplotě	teplota	k1gFnSc6	teplota
asi	asi	k9	asi
950	[number]	k4	950
°	°	k?	°
<g/>
C	C	kA	C
na	na	k7c6	na
katodě	katoda	k1gFnSc6	katoda
vylučuje	vylučovat	k5eAaImIp3nS	vylučovat
elementární	elementární	k2eAgInSc4d1	elementární
hliník	hliník	k1gInSc4	hliník
<g/>
,	,	kIx,	,
na	na	k7c6	na
grafitové	grafitový	k2eAgFnSc6d1	grafitová
anodě	anoda	k1gFnSc6	anoda
vzniká	vznikat	k5eAaImIp3nS	vznikat
kyslík	kyslík	k1gInSc4	kyslík
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
ihned	ihned	k6eAd1	ihned
reaguje	reagovat	k5eAaBmIp3nS	reagovat
s	s	k7c7	s
materiálem	materiál	k1gInSc7	materiál
elektrody	elektroda	k1gFnPc4	elektroda
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
toxického	toxický	k2eAgInSc2d1	toxický
plynného	plynný	k2eAgInSc2d1	plynný
oxidu	oxid	k1gInSc2	oxid
uhelnatého	uhelnatý	k2eAgInSc2d1	uhelnatý
<g/>
,	,	kIx,	,
CO	co	k9	co
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
někdejšího	někdejší	k2eAgNnSc2d1	někdejší
Československa	Československo	k1gNnSc2	Československo
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
1933	[number]	k4	1933
zahájena	zahájen	k2eAgFnSc1d1	zahájena
výroba	výroba	k1gFnSc1	výroba
hliníkových	hliníkový	k2eAgInPc2d1	hliníkový
plechů	plech	k1gInPc2	plech
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
roku	rok	k1gInSc2	rok
1954	[number]	k4	1954
výroba	výroba	k1gFnSc1	výroba
spotřebního	spotřební	k2eAgNnSc2d1	spotřební
zboží	zboží	k1gNnSc2	zboží
z	z	k7c2	z
hliníkových	hliníkový	k2eAgFnPc2d1	hliníková
fólií	fólie	k1gFnPc2	fólie
v	v	k7c4	v
Břidličné	břidličný	k2eAgNnSc4d1	Břidličné
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
ve	v	k7c6	v
výrobě	výroba	k1gFnSc6	výroba
nadále	nadále	k6eAd1	nadále
setrvává	setrvávat	k5eAaImIp3nS	setrvávat
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
roku	rok	k1gInSc2	rok
1953	[number]	k4	1953
započala	započnout	k5eAaPmAgFnS	započnout
výroba	výroba	k1gFnSc1	výroba
hliníku	hliník	k1gInSc2	hliník
ve	v	k7c6	v
slovenském	slovenský	k2eAgInSc6d1	slovenský
Žiaru	Žiar	k1gInSc6	Žiar
nad	nad	k7c4	nad
Hronom	Hronom	k1gInSc4	Hronom
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
převážná	převážný	k2eAgFnSc1d1	převážná
většina	většina	k1gFnSc1	většina
bauxitu	bauxit	k1gInSc2	bauxit
dovážela	dovážet	k5eAaImAgFnS	dovážet
z	z	k7c2	z
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
<g/>
.	.	kIx.	.
</s>
<s>
Výroba	výroba	k1gFnSc1	výroba
hliníku	hliník	k1gInSc2	hliník
Söderbergovou	Söderbergův	k2eAgFnSc7d1	Söderbergův
technologií	technologie	k1gFnSc7	technologie
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
ukončena	ukončit	k5eAaPmNgFnS	ukončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
Největšími	veliký	k2eAgMnPc7d3	veliký
světovými	světový	k2eAgMnPc7d1	světový
producenty	producent	k1gMnPc7	producent
hliníku	hliník	k1gInSc2	hliník
jsou	být	k5eAaImIp3nP	být
firmy	firma	k1gFnPc1	firma
Rio	Rio	k1gFnSc2	Rio
Tinto	tinta	k1gFnSc5	tinta
Alcan	Alcan	k1gInSc1	Alcan
<g/>
,	,	kIx,	,
Rusal	Rusal	k1gInSc1	Rusal
a	a	k8xC	a
Aluminum	Aluminum	k1gInSc1	Aluminum
Company	Compana	k1gFnSc2	Compana
of	of	k?	of
America	America	k1gMnSc1	America
<g/>
.	.	kIx.	.
</s>
<s>
Hliník	hliník	k1gInSc1	hliník
nalézá	nalézat	k5eAaImIp3nS	nalézat
uplatnění	uplatnění	k1gNnSc4	uplatnění
především	především	k9	především
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
poměrně	poměrně	k6eAd1	poměrně
značné	značný	k2eAgFnSc3d1	značná
chemické	chemický	k2eAgFnSc3d1	chemická
odolnosti	odolnost	k1gFnSc3	odolnost
a	a	k8xC	a
nízké	nízký	k2eAgFnSc3d1	nízká
hmotnosti	hmotnost	k1gFnSc3	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
slitin	slitina	k1gFnPc2	slitina
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
například	například	k6eAd1	například
některé	některý	k3yIgFnPc4	některý
drobné	drobný	k2eAgFnPc4d1	drobná
mince	mince	k1gFnPc4	mince
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
běžné	běžný	k2eAgFnPc4d1	běžná
kuchyňské	kuchyňská	k1gFnPc4	kuchyňská
nádobí	nádobí	k1gNnSc2	nádobí
a	a	k8xC	a
příbory	příbor	k1gInPc7	příbor
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vyválcování	vyválcování	k1gNnSc6	vyválcování
do	do	k7c2	do
tenké	tenký	k2eAgFnSc2d1	tenká
folie	folie	k1gFnSc2	folie
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
setkáme	setkat	k5eAaPmIp1nP	setkat
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
alobal	alobal	k1gInSc4	alobal
při	při	k7c6	při
tepelné	tepelný	k2eAgFnSc6d1	tepelná
úpravě	úprava	k1gFnSc6	úprava
pokrmů	pokrm	k1gInPc2	pokrm
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
ochranného	ochranný	k2eAgInSc2d1	ochranný
obalového	obalový	k2eAgInSc2d1	obalový
materiálu	materiál	k1gInSc2	materiál
pro	pro	k7c4	pro
nejrůznější	různý	k2eAgFnPc4d3	nejrůznější
aplikace	aplikace	k1gFnPc4	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stavebnictví	stavebnictví	k1gNnSc6	stavebnictví
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
lisované	lisovaný	k2eAgInPc1d1	lisovaný
hliníkové	hliníkový	k2eAgInPc1d1	hliníkový
profily	profil	k1gInPc1	profil
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yIgFnPc2	který
se	se	k3xPyFc4	se
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
např.	např.	kA	např.
okna	okno	k1gNnSc2	okno
a	a	k8xC	a
dveře	dveře	k1gFnPc1	dveře
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
se	se	k3xPyFc4	se
stříbrem	stříbro	k1gNnSc7	stříbro
slouží	sloužit	k5eAaImIp3nS	sloužit
hliník	hliník	k1gInSc1	hliník
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
velmi	velmi	k6eAd1	velmi
tenké	tenký	k2eAgFnPc4d1	tenká
folie	folie	k1gFnPc4	folie
jako	jako	k8xS	jako
záznamové	záznamový	k2eAgNnSc4d1	záznamové
médium	médium	k1gNnSc4	médium
v	v	k7c6	v
kompaktních	kompaktní	k2eAgInPc6d1	kompaktní
discích	disk	k1gInPc6	disk
(	(	kIx(	(
<g/>
CD	CD	kA	CD
<g/>
)	)	kIx)	)
ať	ať	k8xS	ať
již	již	k6eAd1	již
pro	pro	k7c4	pro
záznam	záznam	k1gInSc4	záznam
zvuku	zvuk	k1gInSc2	zvuk
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
paměťové	paměťový	k2eAgNnSc4d1	paměťové
médium	médium	k1gNnSc4	médium
ve	v	k7c6	v
výpočetní	výpočetní	k2eAgFnSc6d1	výpočetní
technice	technika	k1gFnSc6	technika
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vrstva	vrstva	k1gFnSc1	vrstva
se	se	k3xPyFc4	se
na	na	k7c4	na
plastový	plastový	k2eAgInSc4d1	plastový
podklad	podklad	k1gInSc4	podklad
obvykle	obvykle	k6eAd1	obvykle
napařuje	napařovat	k5eAaImIp3nS	napařovat
tichým	tichý	k2eAgInSc7d1	tichý
elektrickým	elektrický	k2eAgInSc7d1	elektrický
výbojem	výboj	k1gInSc7	výboj
ve	v	k7c6	v
vakuu	vakuum	k1gNnSc6	vakuum
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
poměrně	poměrně	k6eAd1	poměrně
dobré	dobrý	k2eAgFnSc3d1	dobrá
elektrické	elektrický	k2eAgFnSc3d1	elektrická
vodivosti	vodivost	k1gFnSc3	vodivost
se	se	k3xPyFc4	se
hliníku	hliník	k1gInSc3	hliník
užívá	užívat	k5eAaImIp3nS	užívat
jako	jako	k9	jako
materiálu	materiál	k1gInSc2	materiál
pro	pro	k7c4	pro
elektrické	elektrický	k2eAgInPc4d1	elektrický
vodiče	vodič	k1gInPc4	vodič
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
použití	použití	k1gNnSc3	použití
mědi	měď	k1gFnSc2	měď
má	mít	k5eAaImIp3nS	mít
ovšem	ovšem	k9	ovšem
některé	některý	k3yIgFnPc4	některý
nevýhody	nevýhoda	k1gFnPc4	nevýhoda
<g/>
:	:	kIx,	:
Hliník	hliník	k1gInSc1	hliník
je	být	k5eAaImIp3nS	být
křehčí	křehký	k2eAgMnSc1d2	křehčí
<g/>
,	,	kIx,	,
vodič	vodič	k1gMnSc1	vodič
se	se	k3xPyFc4	se
např.	např.	kA	např.
opakovaným	opakovaný	k2eAgInSc7d1	opakovaný
ohybem	ohyb	k1gInSc7	ohyb
snadno	snadno	k6eAd1	snadno
zlomí	zlomit	k5eAaPmIp3nP	zlomit
<g/>
.	.	kIx.	.
</s>
<s>
Průchodem	průchod	k1gInSc7	průchod
proudu	proud	k1gInSc2	proud
se	se	k3xPyFc4	se
zahřívá	zahřívat	k5eAaImIp3nS	zahřívat
a	a	k8xC	a
zvětšuje	zvětšovat	k5eAaImIp3nS	zvětšovat
svůj	svůj	k3xOyFgInSc4	svůj
objem	objem	k1gInSc4	objem
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
hliníkový	hliníkový	k2eAgInSc1d1	hliníkový
vodič	vodič	k1gInSc1	vodič
spojen	spojit	k5eAaPmNgInS	spojit
mechanicky	mechanicky	k6eAd1	mechanicky
s	s	k7c7	s
jiným	jiný	k2eAgInSc7d1	jiný
vodičem	vodič	k1gInSc7	vodič
kupříkladu	kupříkladu	k6eAd1	kupříkladu
pomocí	pomocí	k7c2	pomocí
šroubu	šroub	k1gInSc2	šroub
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
toto	tento	k3xDgNnSc4	tento
roztažení	roztažení	k1gNnSc4	roztažení
nemůže	moct	k5eNaImIp3nS	moct
probíhat	probíhat	k5eAaImF	probíhat
všemi	všecek	k3xTgInPc7	všecek
směry	směr	k1gInPc7	směr
stejně	stejně	k6eAd1	stejně
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
<g/>
-li	i	k?	-li
spoj	spoj	k1gInSc1	spoj
optimálně	optimálně	k6eAd1	optimálně
navržen	navrhnout	k5eAaPmNgInS	navrhnout
<g/>
,	,	kIx,	,
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
plastické	plastický	k2eAgFnSc3d1	plastická
deformaci	deformace	k1gFnSc3	deformace
měkkého	měkký	k2eAgInSc2d1	měkký
hliníku	hliník	k1gInSc2	hliník
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
ochlazení	ochlazení	k1gNnSc6	ochlazení
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
když	když	k8xS	když
proud	proud	k1gInSc1	proud
přestane	přestat	k5eAaPmIp3nS	přestat
vodičem	vodič	k1gInSc7	vodič
protékat	protékat	k5eAaImF	protékat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
naopak	naopak	k6eAd1	naopak
smrští	smrštit	k5eAaPmIp3nS	smrštit
rovnoměrně	rovnoměrně	k6eAd1	rovnoměrně
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
směrech	směr	k1gInPc6	směr
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
způsobí	způsobit	k5eAaPmIp3nS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
šroubované	šroubovaný	k2eAgInPc1d1	šroubovaný
kontakty	kontakt	k1gInPc1	kontakt
poněkud	poněkud	k6eAd1	poněkud
uvolní	uvolnit	k5eAaPmIp3nP	uvolnit
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
zvýší	zvýšit	k5eAaPmIp3nP	zvýšit
jejich	jejich	k3xOp3gInSc4	jejich
přechodový	přechodový	k2eAgInSc4d1	přechodový
odpor	odpor	k1gInSc4	odpor
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
následně	následně	k6eAd1	následně
vede	vést	k5eAaImIp3nS	vést
ke	k	k7c3	k
zvýšenému	zvýšený	k2eAgNnSc3d1	zvýšené
zahřívání	zahřívání	k1gNnSc3	zahřívání
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
se	se	k3xPyFc4	se
hliníkový	hliníkový	k2eAgInSc1d1	hliníkový
vodič	vodič	k1gInSc1	vodič
vlivem	vlivem	k7c2	vlivem
působení	působení	k1gNnSc2	působení
vzdušného	vzdušný	k2eAgInSc2d1	vzdušný
kyslíku	kyslík	k1gInSc2	kyslík
potahuje	potahovat	k5eAaImIp3nS	potahovat
vrstvičkou	vrstvička	k1gFnSc7	vrstvička
nevodivého	vodivý	k2eNgInSc2d1	nevodivý
Al	ala	k1gFnPc2	ala
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
3	[number]	k4	3
a	a	k8xC	a
vinou	vinou	k7c2	vinou
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
přechodový	přechodový	k2eAgInSc4d1	přechodový
odpor	odpor	k1gInSc4	odpor
mezi	mezi	k7c7	mezi
vodičem	vodič	k1gInSc7	vodič
a	a	k8xC	a
svorkovnicí	svorkovnice	k1gFnSc7	svorkovnice
dále	daleko	k6eAd2	daleko
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
<g/>
.	.	kIx.	.
</s>
<s>
Hliníkové	hliníkový	k2eAgInPc1d1	hliníkový
kontakty	kontakt	k1gInPc1	kontakt
mají	mít	k5eAaImIp3nP	mít
být	být	k5eAaImF	být
proto	proto	k8xC	proto
pravidelně	pravidelně	k6eAd1	pravidelně
dotahovány	dotahován	k2eAgFnPc1d1	dotahována
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
zmenšilo	zmenšit	k5eAaPmAgNnS	zmenšit
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
vzniku	vznik	k1gInSc2	vznik
požáru	požár	k1gInSc2	požár
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
vlastnosti	vlastnost	k1gFnPc1	vlastnost
vedly	vést	k5eAaImAgFnP	vést
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
k	k	k7c3	k
omezení	omezení	k1gNnSc3	omezení
používání	používání	k1gNnSc2	používání
hliníku	hliník	k1gInSc2	hliník
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
mědi	měď	k1gFnSc2	měď
zejména	zejména	k9	zejména
v	v	k7c6	v
domovních	domovní	k2eAgInPc6d1	domovní
rozvodech	rozvod	k1gInPc6	rozvod
<g/>
.	.	kIx.	.
</s>
<s>
Nadále	nadále	k6eAd1	nadále
se	se	k3xPyFc4	se
hliník	hliník	k1gInSc1	hliník
jako	jako	k8xC	jako
vodič	vodič	k1gInSc1	vodič
běžně	běžně	k6eAd1	běžně
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
dálkových	dálkový	k2eAgInPc6d1	dálkový
rozvodech	rozvod	k1gInPc6	rozvod
a	a	k8xC	a
průmyslových	průmyslový	k2eAgFnPc6d1	průmyslová
aplikacích	aplikace	k1gFnPc6	aplikace
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
pod	pod	k7c7	pod
profesionálním	profesionální	k2eAgInSc7d1	profesionální
dohledem	dohled	k1gInSc7	dohled
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Aluminotermie	Aluminotermie	k1gFnSc2	Aluminotermie
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
elektropozitivitě	elektropozitivita	k1gFnSc3	elektropozitivita
má	mít	k5eAaImIp3nS	mít
hliník	hliník	k1gInSc1	hliník
značnou	značný	k2eAgFnSc4d1	značná
afinitu	afinita	k1gFnSc4	afinita
ke	k	k7c3	k
kyslíku	kyslík	k1gInSc3	kyslík
a	a	k8xC	a
ochotně	ochotně	k6eAd1	ochotně
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
reaguje	reagovat	k5eAaBmIp3nS	reagovat
<g/>
.	.	kIx.	.
</s>
<s>
Této	tento	k3xDgFnSc3	tento
vlastnosti	vlastnost	k1gFnSc3	vlastnost
využívá	využívat	k5eAaImIp3nS	využívat
aluminotermie	aluminotermie	k1gFnSc1	aluminotermie
-	-	kIx~	-
metoda	metoda	k1gFnSc1	metoda
výroby	výroba	k1gFnSc2	výroba
některých	některý	k3yIgInPc2	některý
kovů	kov	k1gInPc2	kov
z	z	k7c2	z
jejich	jejich	k3xOp3gInPc2	jejich
oxidů	oxid	k1gInPc2	oxid
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
hliníku	hliník	k1gInSc2	hliník
jako	jako	k8xC	jako
redukčního	redukční	k2eAgNnSc2d1	redukční
činidla	činidlo	k1gNnSc2	činidlo
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
uvedené	uvedený	k2eAgFnSc6d1	uvedená
reakci	reakce	k1gFnSc6	reakce
se	se	k3xPyFc4	se
také	také	k9	také
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
značné	značný	k2eAgNnSc1d1	značné
množství	množství	k1gNnSc1	množství
tepla	teplo	k1gNnSc2	teplo
a	a	k8xC	a
teplota	teplota	k1gFnSc1	teplota
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
dostatečných	dostatečný	k2eAgFnPc2d1	dostatečná
hodnot	hodnota	k1gFnPc2	hodnota
pro	pro	k7c4	pro
roztavení	roztavení	k1gNnSc4	roztavení
např.	např.	kA	např.
železa	železo	k1gNnSc2	železo
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgFnPc1d1	následující
reakce	reakce	k1gFnPc1	reakce
práškového	práškový	k2eAgInSc2d1	práškový
hliníku	hliník	k1gInSc2	hliník
s	s	k7c7	s
oxidem	oxid	k1gInSc7	oxid
železitým	železitý	k2eAgInSc7d1	železitý
se	se	k3xPyFc4	se
dříve	dříve	k6eAd2	dříve
často	často	k6eAd1	často
používalo	používat	k5eAaImAgNnS	používat
ke	k	k7c3	k
spojování	spojování	k1gNnSc3	spojování
železných	železný	k2eAgFnPc2d1	železná
kolejnic	kolejnice	k1gFnPc2	kolejnice
vzniklým	vzniklý	k2eAgNnSc7d1	vzniklé
roztaveným	roztavený	k2eAgNnSc7d1	roztavené
železem	železo	k1gNnSc7	železo
<g/>
.	.	kIx.	.
2	[number]	k4	2
Al	ala	k1gFnPc2	ala
+	+	kIx~	+
Fe	Fe	k1gFnSc1	Fe
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
3	[number]	k4	3
→	→	k?	→
Al	ala	k1gFnPc2	ala
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
3	[number]	k4	3
+	+	kIx~	+
2	[number]	k4	2
Fe	Fe	k1gFnSc4	Fe
Aluminotermická	Aluminotermický	k2eAgFnSc1d1	Aluminotermická
metoda	metoda	k1gFnSc1	metoda
se	se	k3xPyFc4	se
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
využívá	využívat	k5eAaImIp3nS	využívat
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
kovů	kov	k1gInPc2	kov
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
nelze	lze	k6eNd1	lze
redukovat	redukovat	k5eAaBmF	redukovat
uhlíkem	uhlík	k1gInSc7	uhlík
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
tyto	tento	k3xDgInPc4	tento
kovy	kov	k1gInPc4	kov
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
molybden	molybden	k1gInSc1	molybden
<g/>
,	,	kIx,	,
mangan	mangan	k1gInSc1	mangan
<g/>
,	,	kIx,	,
chrom	chrom	k1gInSc1	chrom
a	a	k8xC	a
vanad	vanad	k1gInSc1	vanad
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
reakci	reakce	k1gFnSc6	reakce
vzniká	vznikat	k5eAaImIp3nS	vznikat
oxid	oxid	k1gInSc4	oxid
hlinitý	hlinitý	k2eAgInSc4d1	hlinitý
a	a	k8xC	a
příslušný	příslušný	k2eAgInSc4d1	příslušný
kov	kov	k1gInSc4	kov
<g/>
:	:	kIx,	:
Cr	cr	k0	cr
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
3	[number]	k4	3
+	+	kIx~	+
2	[number]	k4	2
Al	ala	k1gFnPc2	ala
→	→	k?	→
2	[number]	k4	2
Cr	cr	k0	cr
+	+	kIx~	+
Al	ala	k1gFnPc2	ala
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
3	[number]	k4	3
Práškový	práškový	k2eAgInSc1d1	práškový
hliník	hliník	k1gInSc1	hliník
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
také	také	k9	také
jako	jako	k9	jako
složka	složka	k1gFnSc1	složka
některých	některý	k3yIgFnPc2	některý
trhavin	trhavina	k1gFnPc2	trhavina
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
svoji	svůj	k3xOyFgFnSc4	svůj
přítomností	přítomnost	k1gFnSc7	přítomnost
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
teplotu	teplota	k1gFnSc4	teplota
exploze	exploze	k1gFnSc2	exploze
i	i	k9	i
brizanci	brizance	k1gFnSc4	brizance
výbušniny	výbušnina	k1gFnSc2	výbušnina
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Slitiny	slitina	k1gFnSc2	slitina
hliníku	hliník	k1gInSc2	hliník
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitější	důležitý	k2eAgNnSc1d3	nejdůležitější
je	být	k5eAaImIp3nS	být
však	však	k9	však
uplatnění	uplatnění	k1gNnSc4	uplatnění
hliníku	hliník	k1gInSc2	hliník
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
slitin	slitina	k1gFnPc2	slitina
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
bezesporu	bezesporu	k9	bezesporu
nejznámější	známý	k2eAgFnSc1d3	nejznámější
je	být	k5eAaImIp3nS	být
slitina	slitina	k1gFnSc1	slitina
s	s	k7c7	s
hořčíkem	hořčík	k1gInSc7	hořčík
<g/>
,	,	kIx,	,
mědí	měď	k1gFnSc7	měď
a	a	k8xC	a
manganem	mangan	k1gInSc7	mangan
<g/>
,	,	kIx,	,
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xS	jako
dural	dural	k1gInSc1	dural
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
materiál	materiál	k1gInSc1	materiál
má	mít	k5eAaImIp3nS	mít
oproti	oproti	k7c3	oproti
samotnému	samotný	k2eAgInSc3d1	samotný
hliníku	hliník	k1gInSc3	hliník
mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgFnSc1d2	veliký
pevnost	pevnost	k1gFnSc1	pevnost
a	a	k8xC	a
tvrdost	tvrdost	k1gFnSc1	tvrdost
při	při	k7c6	při
zachování	zachování	k1gNnSc6	zachování
velmi	velmi	k6eAd1	velmi
malé	malý	k2eAgFnSc2d1	malá
hustoty	hustota	k1gFnSc2	hustota
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
značně	značně	k6eAd1	značně
odolné	odolný	k2eAgFnPc1d1	odolná
vůči	vůči	k7c3	vůči
korozi	koroze	k1gFnSc3	koroze
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
uvedené	uvedený	k2eAgFnPc1d1	uvedená
vlastnosti	vlastnost	k1gFnPc1	vlastnost
předurčují	předurčovat	k5eAaImIp3nP	předurčovat
dural	dural	k1gInSc4	dural
jako	jako	k8xS	jako
ideální	ideální	k2eAgInSc4d1	ideální
materiál	materiál	k1gInSc4	materiál
pro	pro	k7c4	pro
letecký	letecký	k2eAgInSc4d1	letecký
a	a	k8xC	a
automobilový	automobilový	k2eAgInSc4d1	automobilový
průmysl	průmysl	k1gInSc4	průmysl
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
setkáme	setkat	k5eAaPmIp1nP	setkat
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
výtahů	výtah	k1gInPc2	výtah
<g/>
,	,	kIx,	,
jízdních	jízdní	k2eAgNnPc2d1	jízdní
kol	kolo	k1gNnPc2	kolo
<g/>
,	,	kIx,	,
lehkých	lehký	k2eAgInPc2d1	lehký
žebříků	žebřík	k1gInPc2	žebřík
a	a	k8xC	a
podobných	podobný	k2eAgFnPc6d1	podobná
aplikacích	aplikace	k1gFnPc6	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
Bezesporu	bezesporu	k9	bezesporu
nejvýznamnější	významný	k2eAgFnSc7d3	nejvýznamnější
sloučeninou	sloučenina	k1gFnSc7	sloučenina
hliníku	hliník	k1gInSc2	hliník
je	být	k5eAaImIp3nS	být
oxid	oxid	k1gInSc1	oxid
hlinitý	hlinitý	k2eAgInSc1d1	hlinitý
<g/>
,	,	kIx,	,
Al	ala	k1gFnPc2	ala
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
látka	látka	k1gFnSc1	látka
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
modifikací	modifikace	k1gFnPc2	modifikace
se	s	k7c7	s
zcela	zcela	k6eAd1	zcela
odlišnými	odlišný	k2eAgFnPc7d1	odlišná
fyzikálně-chemickými	fyzikálněhemický	k2eAgFnPc7d1	fyzikálně-chemická
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Krystalický	krystalický	k2eAgInSc1d1	krystalický
Al	ala	k1gFnPc2	ala
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
3	[number]	k4	3
má	mít	k5eAaImIp3nS	mít
název	název	k1gInSc1	název
korund	korund	k1gInSc1	korund
a	a	k8xC	a
k	k	k7c3	k
jeho	jeho	k3xOp3gFnPc3	jeho
základním	základní	k2eAgFnPc3d1	základní
vlastnostem	vlastnost	k1gFnPc3	vlastnost
patří	patřit	k5eAaImIp3nS	patřit
mimořádná	mimořádný	k2eAgFnSc1d1	mimořádná
tvrdost	tvrdost	k1gFnSc1	tvrdost
a	a	k8xC	a
chemická	chemický	k2eAgFnSc1d1	chemická
odolnost	odolnost	k1gFnSc1	odolnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
různých	různý	k2eAgFnPc2d1	různá
modifikací	modifikace	k1gFnPc2	modifikace
<g/>
,	,	kIx,	,
drahokamy	drahokam	k1gInPc1	drahokam
safír	safír	k1gInSc1	safír
a	a	k8xC	a
rubín	rubín	k1gInSc1	rubín
jsou	být	k5eAaImIp3nP	být
zmíněny	zmínit	k5eAaPmNgInP	zmínit
v	v	k7c6	v
předchozí	předchozí	k2eAgFnSc6d1	předchozí
kapitole	kapitola	k1gFnSc6	kapitola
<g/>
.	.	kIx.	.
</s>
<s>
Uměle	uměle	k6eAd1	uměle
vyráběný	vyráběný	k2eAgInSc1d1	vyráběný
korund	korund	k1gInSc1	korund
nalézá	nalézat	k5eAaImIp3nS	nalézat
řadu	řada	k1gFnSc4	řada
praktických	praktický	k2eAgNnPc2d1	praktické
uplatnění	uplatnění	k1gNnPc2	uplatnění
<g/>
,	,	kIx,	,
od	od	k7c2	od
výroby	výroba	k1gFnSc2	výroba
laserů	laser	k1gInPc2	laser
po	po	k7c4	po
osazování	osazování	k1gNnSc4	osazování
hlavic	hlavice	k1gFnPc2	hlavice
geologických	geologický	k2eAgFnPc2d1	geologická
vrtných	vrtný	k2eAgFnPc2d1	vrtná
souprav	souprava	k1gFnPc2	souprava
a	a	k8xC	a
kovoobráběcích	kovoobráběcí	k2eAgInPc2d1	kovoobráběcí
nástrojů	nástroj	k1gInPc2	nástroj
pro	pro	k7c4	pro
práci	práce	k1gFnSc4	práce
s	s	k7c7	s
mimořádně	mimořádně	k6eAd1	mimořádně
odolnými	odolný	k2eAgInPc7d1	odolný
materiály	materiál	k1gInPc7	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Chemicky	chemicky	k6eAd1	chemicky
připravený	připravený	k2eAgInSc4d1	připravený
oxid	oxid	k1gInSc4	oxid
hlinitý	hlinitý	k2eAgInSc4d1	hlinitý
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
názvem	název	k1gInSc7	název
alumina	alumin	k2eAgNnSc2d1	alumin
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
podmínek	podmínka	k1gFnPc2	podmínka
výroby	výroba	k1gFnSc2	výroba
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
tento	tento	k3xDgInSc1	tento
materiál	materiál	k1gInSc1	materiál
různé	různý	k2eAgFnSc2d1	různá
fyzikální	fyzikální	k2eAgFnSc2d1	fyzikální
vlastnosti	vlastnost	k1gFnSc2	vlastnost
<g/>
,	,	kIx,	,
základní	základní	k2eAgInPc4d1	základní
typy	typ	k1gInPc4	typ
aluminy	alumin	k2eAgInPc4d1	alumin
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k8xC	jako
alfa	alfa	k1gNnSc1	alfa
<g/>
,	,	kIx,	,
beta	beta	k1gNnSc1	beta
a	a	k8xC	a
gama	gama	k1gNnSc1	gama
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgNnSc1d3	nejvýznamnější
uplatnění	uplatnění	k1gNnSc1	uplatnění
nalézá	nalézat	k5eAaImIp3nS	nalézat
alumina	alumin	k2eAgFnSc1d1	alumina
v	v	k7c6	v
chemickém	chemický	k2eAgInSc6d1	chemický
průmyslu	průmysl	k1gInSc6	průmysl
jako	jako	k9	jako
inertní	inertní	k2eAgInSc4d1	inertní
nosič	nosič	k1gInSc4	nosič
katalyzátorů	katalyzátor	k1gInPc2	katalyzátor
v	v	k7c6	v
organické	organický	k2eAgFnSc6d1	organická
i	i	k8xC	i
anorganické	anorganický	k2eAgFnSc6d1	anorganická
syntéze	syntéza	k1gFnSc6	syntéza
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
hydrogenační	hydrogenační	k2eAgInPc4d1	hydrogenační
katalyzátory	katalyzátor	k1gInPc4	katalyzátor
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
elementární	elementární	k2eAgFnSc2d1	elementární
platiny	platina	k1gFnSc2	platina
<g/>
,	,	kIx,	,
pracující	pracující	k2eAgMnSc1d1	pracující
za	za	k7c2	za
teplot	teplota	k1gFnPc2	teplota
přes	přes	k7c4	přes
300	[number]	k4	300
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
tlaků	tlak	k1gInPc2	tlak
desítek	desítka	k1gFnPc2	desítka
atmosfér	atmosféra	k1gFnPc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
za	za	k7c2	za
těchto	tento	k3xDgFnPc2	tento
extrémních	extrémní	k2eAgFnPc2d1	extrémní
podmínek	podmínka	k1gFnPc2	podmínka
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
životnost	životnost	k1gFnSc1	životnost
těchto	tento	k3xDgInPc2	tento
katalytických	katalytický	k2eAgInPc2d1	katalytický
systémů	systém	k1gInPc2	systém
stovek	stovka	k1gFnPc2	stovka
až	až	k9	až
tisíců	tisíc	k4xCgInPc2	tisíc
pracovních	pracovní	k2eAgFnPc2d1	pracovní
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Speciálně	speciálně	k6eAd1	speciálně
upravená	upravený	k2eAgFnSc1d1	upravená
alumina	alumin	k2eAgFnSc1d1	alumina
nanesená	nanesený	k2eAgFnSc1d1	nanesená
v	v	k7c6	v
tenké	tenký	k2eAgFnSc6d1	tenká
vrstvě	vrstva	k1gFnSc6	vrstva
na	na	k7c6	na
inertním	inertní	k2eAgInSc6d1	inertní
nosiči	nosič	k1gInSc6	nosič
slouží	sloužit	k5eAaImIp3nS	sloužit
pro	pro	k7c4	pro
separaci	separace	k1gFnSc4	separace
organických	organický	k2eAgFnPc2d1	organická
sloučenin	sloučenina	k1gFnPc2	sloučenina
chromatografií	chromatografie	k1gFnPc2	chromatografie
na	na	k7c6	na
tenké	tenký	k2eAgFnSc6d1	tenká
vrstvě	vrstva	k1gFnSc6	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
analytická	analytický	k2eAgFnSc1d1	analytická
technika	technika	k1gFnSc1	technika
je	být	k5eAaImIp3nS	být
ekonomicky	ekonomicky	k6eAd1	ekonomicky
velmi	velmi	k6eAd1	velmi
nenáročná	náročný	k2eNgFnSc1d1	nenáročná
a	a	k8xC	a
nalézá	nalézat	k5eAaImIp3nS	nalézat
uplatnění	uplatnění	k1gNnSc4	uplatnění
např.	např.	kA	např.
v	v	k7c6	v
kontrole	kontrola	k1gFnSc6	kontrola
průmyslového	průmyslový	k2eAgNnSc2d1	průmyslové
dělení	dělení	k1gNnSc2	dělení
směsí	směs	k1gFnPc2	směs
přírodních	přírodní	k2eAgNnPc2d1	přírodní
barviv	barvivo	k1gNnPc2	barvivo
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
typů	typ	k1gInPc2	typ
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Chlorid	chlorid	k1gInSc1	chlorid
hlinitý	hlinitý	k2eAgInSc1d1	hlinitý
<g/>
,	,	kIx,	,
AlCl	AlCl	k1gInSc1	AlCl
<g/>
3	[number]	k4	3
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
významný	významný	k2eAgInSc1d1	významný
průmyslový	průmyslový	k2eAgInSc1d1	průmyslový
katalyzátor	katalyzátor	k1gInSc1	katalyzátor
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
organické	organický	k2eAgFnSc2d1	organická
syntézy	syntéza	k1gFnSc2	syntéza
<g/>
.	.	kIx.	.
</s>
<s>
Uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
jako	jako	k9	jako
Lewisovská	Lewisovský	k2eAgFnSc1d1	Lewisovský
kyselina	kyselina	k1gFnSc1	kyselina
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
působením	působení	k1gNnSc7	působení
dochází	docházet	k5eAaImIp3nS	docházet
vnášení	vnášení	k1gNnSc1	vnášení
alkylových	alkylový	k2eAgFnPc2d1	alkylová
skupin	skupina	k1gFnPc2	skupina
na	na	k7c4	na
aromatické	aromatický	k2eAgNnSc4d1	aromatické
jádro	jádro	k1gNnSc4	jádro
nebo	nebo	k8xC	nebo
halogenaci	halogenace	k1gFnSc4	halogenace
uhlovodíků	uhlovodík	k1gInPc2	uhlovodík
do	do	k7c2	do
předem	předem	k6eAd1	předem
zvolené	zvolený	k2eAgFnSc2d1	zvolená
polohy	poloha	k1gFnSc2	poloha
<g/>
.	.	kIx.	.
</s>
<s>
Reakce	reakce	k1gFnSc1	reakce
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
jsou	být	k5eAaImIp3nP	být
souborně	souborně	k6eAd1	souborně
označovány	označován	k2eAgFnPc1d1	označována
termínem	termín	k1gInSc7	termín
Friedel-Craftsovy	Friedel-Craftsův	k2eAgFnPc1d1	Friedel-Craftsův
reakce	reakce	k1gFnPc1	reakce
<g/>
,	,	kIx,	,
klasickým	klasický	k2eAgInSc7d1	klasický
příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
výroba	výroba	k1gFnSc1	výroba
toluenu	toluen	k1gInSc2	toluen
reakcí	reakce	k1gFnPc2	reakce
chloroformu	chloroform	k1gInSc2	chloroform
s	s	k7c7	s
benzenem	benzen	k1gInSc7	benzen
nebo	nebo	k8xC	nebo
syntéza	syntéza	k1gFnSc1	syntéza
styrenu	styren	k1gInSc2	styren
z	z	k7c2	z
ethenu	ethen	k1gInSc2	ethen
a	a	k8xC	a
benzenu	benzen	k1gInSc2	benzen
<g/>
.	.	kIx.	.
</s>
<s>
Fluorid	fluorid	k1gInSc4	fluorid
hlinitý	hlinitý	k2eAgInSc4d1	hlinitý
<g/>
,	,	kIx,	,
AlF	alfa	k1gFnPc2	alfa
<g/>
3	[number]	k4	3
a	a	k8xC	a
fosforečnan	fosforečnan	k1gInSc4	fosforečnan
hlinitý	hlinitý	k2eAgInSc4d1	hlinitý
AlPO	alpa	k1gFnSc5	alpa
<g/>
4	[number]	k4	4
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c7	mezi
velmi	velmi	k6eAd1	velmi
málo	málo	k6eAd1	málo
rozpustné	rozpustný	k2eAgFnPc1d1	rozpustná
sloučeniny	sloučenina	k1gFnPc1	sloučenina
hliníku	hliník	k1gInSc2	hliník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
analytické	analytický	k2eAgFnSc6d1	analytická
chemii	chemie	k1gFnSc6	chemie
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
vážkovému	vážkový	k2eAgMnSc3d1	vážkový
(	(	kIx(	(
<g/>
gravimetrickému	gravimetrický	k2eAgMnSc3d1	gravimetrický
<g/>
)	)	kIx)	)
stanovení	stanovení	k1gNnSc3	stanovení
obsahu	obsah	k1gInSc2	obsah
hliníku	hliník	k1gInSc2	hliník
v	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
a	a	k8xC	a
fosfátový	fosfátový	k2eAgInSc1d1	fosfátový
ion	ion	k1gInSc1	ion
může	moct	k5eAaImIp3nS	moct
odstraňovat	odstraňovat	k5eAaImF	odstraňovat
i	i	k8xC	i
malá	malý	k2eAgNnPc4d1	malé
množství	množství	k1gNnSc4	množství
hliníku	hliník	k1gInSc2	hliník
z	z	k7c2	z
odpadních	odpadní	k2eAgFnPc2d1	odpadní
a	a	k8xC	a
průmyslových	průmyslový	k2eAgFnPc2d1	průmyslová
vod	voda	k1gFnPc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Octan	octan	k1gInSc4	octan
hlinitý	hlinitý	k2eAgInSc4d1	hlinitý
<g/>
,	,	kIx,	,
Al	ala	k1gFnPc2	ala
<g/>
(	(	kIx(	(
<g/>
CH	Ch	kA	Ch
<g/>
3	[number]	k4	3
<g/>
COO	COO	kA	COO
<g/>
)	)	kIx)	)
<g/>
3	[number]	k4	3
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
lékařství	lékařství	k1gNnSc1	lékařství
jako	jako	k8xC	jako
účinná	účinný	k2eAgFnSc1d1	účinná
látka	látka	k1gFnSc1	látka
v	v	k7c6	v
mastech	mast	k1gFnPc6	mast
proti	proti	k7c3	proti
otokům	otok	k1gInPc3	otok
<g/>
.	.	kIx.	.
</s>
<s>
Hlinité	hlinitý	k2eAgFnPc1d1	hlinitá
sloučeniny	sloučenina	k1gFnPc1	sloučenina
se	se	k3xPyFc4	se
špatně	špatně	k6eAd1	špatně
rozpouštějí	rozpouštět	k5eAaImIp3nP	rozpouštět
(	(	kIx(	(
<g/>
příliš	příliš	k6eAd1	příliš
toxické	toxický	k2eAgNnSc1d1	toxické
není	být	k5eNaImIp3nS	být
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hliník	hliník	k1gInSc1	hliník
se	se	k3xPyFc4	se
prakticky	prakticky	k6eAd1	prakticky
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
téměř	téměř	k6eAd1	téměř
jako	jako	k9	jako
jediný	jediný	k2eAgMnSc1d1	jediný
z	z	k7c2	z
prvků	prvek	k1gInPc2	prvek
I.	I.	kA	I.
<g/>
-	-	kIx~	-
<g/>
III	III	kA	III
<g/>
.	.	kIx.	.
základní	základní	k2eAgFnSc2d1	základní
skupiny	skupina	k1gFnSc2	skupina
prvků	prvek	k1gInPc2	prvek
periodické	periodický	k2eAgFnSc2d1	periodická
soustavy	soustava	k1gFnSc2	soustava
v	v	k7c6	v
žádné	žádný	k3yNgFnSc6	žádný
živé	živý	k2eAgFnSc6d1	živá
tkáni	tkáň	k1gFnSc6	tkáň
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
již	již	k6eAd1	již
rostlinné	rostlinný	k2eAgInPc4d1	rostlinný
nebo	nebo	k8xC	nebo
živočišné	živočišný	k2eAgInPc4d1	živočišný
<g/>
,	,	kIx,	,
navzdory	navzdory	k7c3	navzdory
jeho	jeho	k3xOp3gNnSc3	jeho
značnému	značný	k2eAgNnSc3d1	značné
zastoupení	zastoupení	k1gNnSc3	zastoupení
v	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
kůře	kůra	k1gFnSc6	kůra
<g/>
.	.	kIx.	.
</s>
<s>
Dlouho	dlouho	k6eAd1	dlouho
dobu	doba	k1gFnSc4	doba
existuje	existovat	k5eAaImIp3nS	existovat
podezření	podezření	k1gNnSc4	podezření
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
vedena	veden	k2eAgFnSc1d1	vedena
debata	debata	k1gFnSc1	debata
<g/>
,	,	kIx,	,
že	že	k8xS	že
případný	případný	k2eAgInSc1d1	případný
zvýšený	zvýšený	k2eAgInSc1d1	zvýšený
výskyt	výskyt	k1gInSc1	výskyt
hliníku	hliník	k1gInSc2	hliník
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
příčinou	příčina	k1gFnSc7	příčina
vzniku	vznik	k1gInSc2	vznik
Alzheimerovy	Alzheimerův	k2eAgFnSc2d1	Alzheimerova
choroby	choroba	k1gFnSc2	choroba
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
likviduje	likvidovat	k5eAaBmIp3nS	likvidovat
mozkové	mozkový	k2eAgFnPc4d1	mozková
a	a	k8xC	a
nervové	nervový	k2eAgFnPc4d1	nervová
buňky	buňka	k1gFnPc4	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Světová	světový	k2eAgFnSc1d1	světová
zdravotnická	zdravotnický	k2eAgFnSc1d1	zdravotnická
organizace	organizace	k1gFnSc1	organizace
(	(	kIx(	(
<g/>
WHO	WHO	kA	WHO
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
uvádí	uvádět	k5eAaImIp3nS	uvádět
existenci	existence	k1gFnSc4	existence
spojení	spojení	k1gNnSc2	spojení
mezi	mezi	k7c7	mezi
vystavením	vystavení	k1gNnSc7	vystavení
působení	působení	k1gNnSc2	působení
hliníku	hliník	k1gInSc2	hliník
a	a	k8xC	a
Alzheimerovou	Alzheimerův	k2eAgFnSc7d1	Alzheimerova
chorobou	choroba	k1gFnSc7	choroba
<g/>
.	.	kIx.	.
</s>
<s>
Vyšší	vysoký	k2eAgFnSc1d2	vyšší
frekvence	frekvence	k1gFnSc1	frekvence
používání	používání	k1gNnSc2	používání
antiperspirantů	antiperspirant	k1gMnPc2	antiperspirant
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
vyšším	vysoký	k2eAgMnSc6d2	vyšší
rizikům	riziko	k1gNnPc3	riziko
vzniku	vznik	k1gInSc3	vznik
Alzheimerovy	Alzheimerův	k2eAgFnSc2d1	Alzheimerova
choroby	choroba	k1gFnSc2	choroba
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
provedených	provedený	k2eAgInPc2d1	provedený
výzkumů	výzkum	k1gInPc2	výzkum
byly	být	k5eAaImAgFnP	být
zjištěny	zjistit	k5eAaPmNgFnP	zjistit
nadměrné	nadměrný	k2eAgFnPc1d1	nadměrná
koncentrace	koncentrace	k1gFnPc1	koncentrace
hliníku	hliník	k1gInSc2	hliník
v	v	k7c6	v
mozku	mozek	k1gInSc6	mozek
lidí	člověk	k1gMnPc2	člověk
s	s	k7c7	s
Alzheimerovou	Alzheimerův	k2eAgFnSc7d1	Alzheimerova
chorobou	choroba	k1gFnSc7	choroba
<g/>
.	.	kIx.	.
</s>
<s>
Výzkum	výzkum	k1gInSc1	výzkum
ověřil	ověřit	k5eAaPmAgInS	ověřit
tuto	tento	k3xDgFnSc4	tento
teorii	teorie	k1gFnSc4	teorie
laboratorními	laboratorní	k2eAgInPc7d1	laboratorní
testy	test	k1gInPc7	test
na	na	k7c6	na
zvířatech	zvíře	k1gNnPc6	zvíře
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pokud	pokud	k8xS	pokud
byl	být	k5eAaImAgInS	být
hliník	hliník	k1gInSc1	hliník
vpíchnut	vpíchnut	k2eAgInSc1d1	vpíchnut
do	do	k7c2	do
mozku	mozek	k1gInSc2	mozek
laboratorních	laboratorní	k2eAgNnPc2d1	laboratorní
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
u	u	k7c2	u
nich	on	k3xPp3gFnPc2	on
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
neurologická	neurologický	k2eAgFnSc1d1	neurologická
choroba	choroba	k1gFnSc1	choroba
podobná	podobný	k2eAgFnSc1d1	podobná
Alzheimeru	Alzheimer	k1gInSc2	Alzheimer
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
studie	studie	k1gFnPc1	studie
oponují	oponovat	k5eAaImIp3nP	oponovat
<g/>
,	,	kIx,	,
<g/>
že	že	k8xS	že
se	se	k3xPyFc4	se
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
nepodařilo	podařit	k5eNaPmAgNnS	podařit
prokázat	prokázat	k5eAaPmF	prokázat
<g/>
.	.	kIx.	.
</s>
<s>
Výzkumy	výzkum	k1gInPc1	výzkum
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
kterým	který	k3yIgMnPc3	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
tento	tento	k3xDgInSc1	tento
předpoklad	předpoklad	k1gInSc1	předpoklad
<g/>
,	,	kIx,	,
mohly	moct	k5eAaImAgInP	moct
být	být	k5eAaImF	být
ovlivněny	ovlivnit	k5eAaPmNgInP	ovlivnit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
část	část	k1gFnSc1	část
lidí	člověk	k1gMnPc2	člověk
setkávala	setkávat	k5eAaImAgFnS	setkávat
poměrně	poměrně	k6eAd1	poměrně
často	často	k6eAd1	často
s	s	k7c7	s
hliníkovým	hliníkový	k2eAgInSc7d1	hliníkový
příborem	příbor	k1gInSc7	příbor
a	a	k8xC	a
nádobím	nádobí	k1gNnSc7	nádobí
<g/>
,	,	kIx,	,
a	a	k8xC	a
měli	mít	k5eAaImAgMnP	mít
tedy	tedy	k9	tedy
zvýšenou	zvýšený	k2eAgFnSc4d1	zvýšená
hladinu	hladina	k1gFnSc4	hladina
hliníku	hliník	k1gInSc2	hliník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
však	však	k9	však
na	na	k7c4	na
obsah	obsah	k1gInSc4	obsah
hliníku	hliník	k1gInSc2	hliník
velmi	velmi	k6eAd1	velmi
pečlivě	pečlivě	k6eAd1	pečlivě
testována	testovat	k5eAaImNgFnS	testovat
především	především	k9	především
krevní	krevní	k2eAgFnSc1d1	krevní
plazma	plazma	k1gFnSc1	plazma
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
by	by	kYmCp3nS	by
při	při	k7c6	při
pravidelných	pravidelný	k2eAgFnPc6d1	pravidelná
krevních	krevní	k2eAgFnPc6d1	krevní
transfuzích	transfuze	k1gFnPc6	transfuze
mohla	moct	k5eAaImAgFnS	moct
zvýšit	zvýšit	k5eAaPmF	zvýšit
hladinu	hladina	k1gFnSc4	hladina
hliníku	hliník	k1gInSc2	hliník
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
pacienta	pacient	k1gMnSc4	pacient
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Obdobná	obdobný	k2eAgNnPc1d1	obdobné
pravidla	pravidlo	k1gNnPc1	pravidlo
platí	platit	k5eAaImIp3nP	platit
pro	pro	k7c4	pro
všechny	všechen	k3xTgInPc4	všechen
dialyzační	dialyzační	k2eAgInPc4d1	dialyzační
roztoky	roztok	k1gInPc4	roztok
<g/>
,	,	kIx,	,
používané	používaný	k2eAgInPc4d1	používaný
při	při	k7c6	při
chronickém	chronický	k2eAgNnSc6d1	chronické
selhání	selhání	k1gNnSc6	selhání
ledvin	ledvina	k1gFnPc2	ledvina
<g/>
.	.	kIx.	.
</s>
<s>
Poměrně	poměrně	k6eAd1	poměrně
diskutovaným	diskutovaný	k2eAgInSc7d1	diskutovaný
problémem	problém	k1gInSc7	problém
je	být	k5eAaImIp3nS	být
riziko	riziko	k1gNnSc1	riziko
používání	používání	k1gNnSc2	používání
hliníkového	hliníkový	k2eAgNnSc2d1	hliníkové
nádobí	nádobí	k1gNnSc2	nádobí
a	a	k8xC	a
příborů	příbor	k1gInPc2	příbor
při	při	k7c6	při
přípravě	příprava	k1gFnSc6	příprava
a	a	k8xC	a
konzumaci	konzumace	k1gFnSc6	konzumace
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pravda	pravda	k1gFnSc1	pravda
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
podmínkách	podmínka	k1gFnPc6	podmínka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
potraviny	potravina	k1gFnPc1	potravina
běžně	běžně	k6eAd1	běžně
tepelně	tepelně	k6eAd1	tepelně
upravují	upravovat	k5eAaImIp3nP	upravovat
i	i	k8xC	i
konzumují	konzumovat	k5eAaBmIp3nP	konzumovat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
hliník	hliník	k1gInSc1	hliník
nejstálejší	stálý	k2eAgMnSc1d3	nejstálejší
a	a	k8xC	a
prakticky	prakticky	k6eAd1	prakticky
nerozpustný	rozpustný	k2eNgInSc1d1	nerozpustný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
neutrálním	neutrální	k2eAgNnSc6d1	neutrální
prostředí	prostředí	k1gNnSc6	prostředí
běžné	běžný	k2eAgFnSc2d1	běžná
pitné	pitný	k2eAgFnSc2d1	pitná
vody	voda	k1gFnSc2	voda
o	o	k7c4	o
pH	ph	kA	ph
=	=	kIx~	=
7	[number]	k4	7
je	být	k5eAaImIp3nS	být
hliníkový	hliníkový	k2eAgInSc4d1	hliníkový
povrch	povrch	k1gInSc4	povrch
perfektně	perfektně	k6eAd1	perfektně
stabilní	stabilní	k2eAgInSc4d1	stabilní
a	a	k8xC	a
bezpečný	bezpečný	k2eAgInSc4d1	bezpečný
<g/>
.	.	kIx.	.
</s>
<s>
Problém	problém	k1gInSc1	problém
nastává	nastávat	k5eAaImIp3nS	nastávat
<g/>
,	,	kIx,	,
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
vařený	vařený	k2eAgInSc1d1	vařený
pokrm	pokrm	k1gInSc1	pokrm
okyselen	okyselit	k5eAaPmNgInS	okyselit
například	například	k6eAd1	například
octem	ocet	k1gInSc7	ocet
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
stále	stále	k6eAd1	stále
mírně	mírně	k6eAd1	mírně
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
kyselost	kyselost	k1gFnSc4	kyselost
pitné	pitný	k2eAgFnSc2d1	pitná
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
kyselých	kyselý	k2eAgInPc2d1	kyselý
dešťů	dešť	k1gInPc2	dešť
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
může	moct	k5eAaImIp3nS	moct
skutečně	skutečně	k6eAd1	skutečně
nastat	nastat	k5eAaPmF	nastat
situace	situace	k1gFnPc4	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
z	z	k7c2	z
hliníkových	hliníkový	k2eAgFnPc2d1	hliníková
nádob	nádoba	k1gFnPc2	nádoba
bude	být	k5eAaImBp3nS	být
uvolňovat	uvolňovat	k5eAaImF	uvolňovat
hliník	hliník	k1gInSc1	hliník
při	při	k7c6	při
každém	každý	k3xTgNnSc6	každý
použití	použití	k1gNnSc6	použití
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
je	být	k5eAaImIp3nS	být
organizmus	organizmus	k1gInSc1	organizmus
vybaven	vybavit	k5eAaPmNgInS	vybavit
řadou	řada	k1gFnSc7	řada
bariér	bariéra	k1gFnPc2	bariéra
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
brání	bránit	k5eAaImIp3nP	bránit
pronikání	pronikání	k1gNnSc4	pronikání
sloučenin	sloučenina	k1gFnPc2	sloučenina
hliníku	hliník	k1gInSc2	hliník
do	do	k7c2	do
tělesných	tělesný	k2eAgFnPc2d1	tělesná
tekutin	tekutina	k1gFnPc2	tekutina
a	a	k8xC	a
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Vdechování	vdechování	k1gNnSc1	vdechování
jemných	jemný	k2eAgInPc2d1	jemný
prachů	prach	k1gInPc2	prach
hlinitých	hlinitý	k2eAgFnPc2d1	hlinitá
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
oxid	oxid	k1gInSc1	oxid
hlinitý	hlinitý	k2eAgInSc1d1	hlinitý
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
vyvolat	vyvolat	k5eAaPmF	vyvolat
onemocnění	onemocnění	k1gNnSc4	onemocnění
plic	plíce	k1gFnPc2	plíce
<g/>
.	.	kIx.	.
</s>
<s>
Cotton	Cotton	k1gInSc1	Cotton
F.	F.	kA	F.
<g/>
A.	A.	kA	A.
<g/>
,	,	kIx,	,
Wilkinson	Wilkinson	k1gMnSc1	Wilkinson
J.	J.	kA	J.
<g/>
:	:	kIx,	:
<g/>
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
souborné	souborný	k2eAgNnSc1d1	souborné
zpracování	zpracování	k1gNnSc1	zpracování
pro	pro	k7c4	pro
pokročilé	pokročilý	k1gMnPc4	pokročilý
<g/>
,	,	kIx,	,
ACADEMIA	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1973	[number]	k4	1973
Holzbecher	Holzbechra	k1gFnPc2	Holzbechra
Z.	Z.	kA	Z.
<g/>
:	:	kIx,	:
<g/>
Analytická	analytický	k2eAgFnSc1d1	analytická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
SNTL	SNTL	kA	SNTL
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1974	[number]	k4	1974
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Heinrich	Heinrich	k1gMnSc1	Heinrich
Remy	remy	k1gNnSc2	remy
<g/>
,	,	kIx,	,
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1961	[number]	k4	1961
N.	N.	kA	N.
N.	N.	kA	N.
Greenwood	Greenwooda	k1gFnPc2	Greenwooda
-	-	kIx~	-
A.	A.	kA	A.
Earnshaw	Earnshaw	k1gFnSc1	Earnshaw
<g/>
,	,	kIx,	,
Chemie	chemie	k1gFnSc1	chemie
prvků	prvek	k1gInPc2	prvek
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1993	[number]	k4	1993
ISBN	ISBN	kA	ISBN
80-85427-38-9	[number]	k4	80-85427-38-9
JAREŠ	JAREŠ	kA	JAREŠ
<g/>
,	,	kIx,	,
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
<g/>
.	.	kIx.	.
</s>
<s>
Lehké	Lehké	k2eAgInPc1d1	Lehké
kovy	kov	k1gInPc1	kov
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Česká	český	k2eAgFnSc1d1	Česká
matice	matice	k1gFnSc1	matice
technická	technický	k2eAgFnSc1d1	technická
<g/>
,	,	kIx,	,
1944	[number]	k4	1944
<g/>
.	.	kIx.	.
163	[number]	k4	163
s.	s.	k?	s.
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
hliník	hliník	k1gInSc1	hliník
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
hliník	hliník	k1gInSc1	hliník
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Chemický	chemický	k2eAgInSc1d1	chemický
vzdělávací	vzdělávací	k2eAgInSc1d1	vzdělávací
portál	portál	k1gInSc1	portál
</s>
