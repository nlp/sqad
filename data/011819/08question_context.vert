<s>
Ekonomicko-správní	ekonomickoprávní	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
Masarykovy	Masarykův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
(	(	kIx(
<g/>
ECON	ECON	kA
MUNI	MUNI	k?
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
fakulta	fakulta	k1gFnSc1
zaměřena	zaměřit	k5eAaPmNgFnS
na	na	k7c4
ekonomii	ekonomie	k1gFnSc4
<g/>
,	,	kIx,
ekonomické	ekonomický	k2eAgInPc4d1
informační	informační	k2eAgInPc4d1
systémy	systém	k1gInPc4
<g/>
,	,	kIx,
finanční	finanční	k2eAgNnSc4d1
podnikání	podnikání	k1gNnSc4
<g/>
,	,	kIx,
hospodářskou	hospodářský	k2eAgFnSc4d1
politiku	politika	k1gFnSc4
a	a	k8xC
management	management	k1gInSc4
<g/>
,	,	kIx,
národní	národní	k2eAgNnSc4d1
a	a	k8xC
podnikové	podnikový	k2eAgNnSc4d1
hospodářství	hospodářství	k1gNnSc4
<g/>
,	,	kIx,
regionální	regionální	k2eAgInSc4d1
rozvoj	rozvoj	k1gInSc4
a	a	k8xC
správu	správa	k1gFnSc4
<g/>
,	,	kIx,
součást	součást	k1gFnSc4
Masarykovy	Masarykův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
<g/>
.	.	kIx.
</s>