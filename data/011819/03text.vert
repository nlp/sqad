<p>
<s>
Ekonomicko-správní	ekonomickoprávní	k2eAgFnSc1d1	ekonomicko-správní
fakulta	fakulta	k1gFnSc1	fakulta
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
(	(	kIx(	(
<g/>
ECON	ECON	kA	ECON
MUNI	MUNI	k?	MUNI
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
fakulta	fakulta	k1gFnSc1	fakulta
zaměřena	zaměřit	k5eAaPmNgFnS	zaměřit
na	na	k7c4	na
ekonomii	ekonomie	k1gFnSc4	ekonomie
<g/>
,	,	kIx,	,
ekonomické	ekonomický	k2eAgInPc4d1	ekonomický
informační	informační	k2eAgInPc4d1	informační
systémy	systém	k1gInPc4	systém
<g/>
,	,	kIx,	,
finanční	finanční	k2eAgNnSc4d1	finanční
podnikání	podnikání	k1gNnSc4	podnikání
<g/>
,	,	kIx,	,
hospodářskou	hospodářský	k2eAgFnSc4d1	hospodářská
politiku	politika	k1gFnSc4	politika
a	a	k8xC	a
management	management	k1gInSc4	management
<g/>
,	,	kIx,	,
národní	národní	k2eAgNnSc4d1	národní
a	a	k8xC	a
podnikové	podnikový	k2eAgNnSc4d1	podnikové
hospodářství	hospodářství	k1gNnSc4	hospodářství
<g/>
,	,	kIx,	,
regionální	regionální	k2eAgInSc4d1	regionální
rozvoj	rozvoj	k1gInSc4	rozvoj
a	a	k8xC	a
správu	správa	k1gFnSc4	správa
<g/>
,	,	kIx,	,
součást	součást	k1gFnSc4	součást
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Fakulta	fakulta	k1gFnSc1	fakulta
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
roku	rok	k1gInSc6	rok
1990	[number]	k4	1990
jako	jako	k8xS	jako
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
šestá	šestý	k4xOgFnSc1	šestý
fakulta	fakulta	k1gFnSc1	fakulta
MU	MU	kA	MU
a	a	k8xC	a
první	první	k4xOgFnSc4	první
polistopadová	polistopadový	k2eAgFnSc1d1	polistopadová
<g/>
.	.	kIx.	.
</s>
<s>
Výuka	výuka	k1gFnSc1	výuka
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
v	v	k7c6	v
září	září	k1gNnSc6	září
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
sedmi	sedm	k4xCc7	sedm
katedrami	katedra	k1gFnPc7	katedra
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
bakalářské	bakalářský	k2eAgInPc1d1	bakalářský
<g/>
,	,	kIx,	,
magisterské	magisterský	k2eAgInPc1d1	magisterský
i	i	k8xC	i
navazující	navazující	k2eAgInPc1d1	navazující
magisterské	magisterský	k2eAgInPc1d1	magisterský
a	a	k8xC	a
doktorské	doktorský	k2eAgInPc1d1	doktorský
studijní	studijní	k2eAgInPc1d1	studijní
programy	program	k1gInPc1	program
v	v	k7c6	v
prezenční	prezenční	k2eAgFnSc6d1	prezenční
formě	forma	k1gFnSc6	forma
<g/>
.	.	kIx.	.
</s>
<s>
Fakulta	fakulta	k1gFnSc1	fakulta
nabízí	nabízet	k5eAaImIp3nS	nabízet
řadu	řada	k1gFnSc4	řada
studijních	studijní	k2eAgInPc2d1	studijní
oborů	obor	k1gInPc2	obor
také	také	k9	také
v	v	k7c6	v
kominované	kominovaný	k2eAgFnSc6d1	kominovaný
formě	forma	k1gFnSc6	forma
a	a	k8xC	a
program	program	k1gInSc4	program
celoživotního	celoživotní	k2eAgNnSc2d1	celoživotní
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
několika	několik	k4yIc6	několik
stěhováních	stěhování	k1gNnPc6	stěhování
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
sídlí	sídlet	k5eAaImIp3nS	sídlet
ve	v	k7c6	v
vlastní	vlastní	k2eAgFnSc6d1	vlastní
budově	budova	k1gFnSc6	budova
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
v	v	k7c6	v
brněnských	brněnský	k2eAgFnPc6d1	brněnská
Pisárkách	Pisárka	k1gFnPc6	Pisárka
na	na	k7c6	na
Lipové	lipový	k2eAgFnSc6d1	Lipová
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Budova	budova	k1gFnSc1	budova
==	==	k?	==
</s>
</p>
<p>
<s>
Původní	původní	k2eAgNnSc4d1	původní
sídlo	sídlo	k1gNnSc4	sídlo
měla	mít	k5eAaImAgFnS	mít
fakulta	fakulta	k1gFnSc1	fakulta
na	na	k7c6	na
Zelném	zelný	k2eAgInSc6d1	zelný
trhu	trh	k1gInSc6	trh
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
Brna	Brno	k1gNnSc2	Brno
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
bývalé	bývalý	k2eAgFnSc2d1	bývalá
Cyrilometodějské	cyrilometodějský	k2eAgFnSc2d1	Cyrilometodějská
záložny	záložna	k1gFnSc2	záložna
<g/>
,	,	kIx,	,
postavené	postavený	k2eAgInPc1d1	postavený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1882	[number]	k4	1882
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
budově	budova	k1gFnSc6	budova
sídlila	sídlit	k5eAaImAgFnS	sídlit
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
Právnická	právnický	k2eAgFnSc1d1	právnická
fakulta	fakulta	k1gFnSc1	fakulta
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
původní	původní	k2eAgFnSc2d1	původní
budovy	budova	k1gFnSc2	budova
na	na	k7c6	na
ulici	ulice	k1gFnSc6	ulice
Veveří	veveří	k2eAgNnSc4d1	veveří
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
kapacita	kapacita	k1gFnSc1	kapacita
budovy	budova	k1gFnSc2	budova
nedostačovala	dostačovat	k5eNaImAgFnS	dostačovat
<g/>
,	,	kIx,	,
fakulta	fakulta	k1gFnSc1	fakulta
přesídlila	přesídlit	k5eAaPmAgFnS	přesídlit
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
do	do	k7c2	do
pronajaté	pronajatý	k2eAgFnSc2d1	pronajatá
budovy	budova	k1gFnSc2	budova
Vysokého	vysoký	k2eAgNnSc2d1	vysoké
učení	učení	k1gNnSc2	učení
technického	technický	k2eAgNnSc2d1	technické
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
na	na	k7c6	na
Antonínské	Antonínský	k2eAgFnSc6d1	Antonínská
ulici	ulice	k1gFnSc6	ulice
také	také	k9	také
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
využívá	využívat	k5eAaPmIp3nS	využívat
fakulta	fakulta	k1gFnSc1	fakulta
budovu	budova	k1gFnSc4	budova
na	na	k7c6	na
rohu	roh	k1gInSc6	roh
ulic	ulice	k1gFnPc2	ulice
Lipová	lipový	k2eAgFnSc1d1	Lipová
a	a	k8xC	a
Vinařská	vinařský	k2eAgFnSc1d1	vinařská
za	za	k7c7	za
brněnským	brněnský	k2eAgNnSc7d1	brněnské
výstavištěm	výstaviště	k1gNnSc7	výstaviště
<g/>
,	,	kIx,	,
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
rozsáhlých	rozsáhlý	k2eAgFnPc2d1	rozsáhlá
kolejí	kolej	k1gFnPc2	kolej
a	a	k8xC	a
menzy	menza	k1gFnPc1	menza
Vinařská	vinařský	k2eAgFnSc1d1	vinařská
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
sídlí	sídlet	k5eAaImIp3nS	sídlet
také	také	k9	také
Správa	správa	k1gFnSc1	správa
kolejí	kolej	k1gFnPc2	kolej
a	a	k8xC	a
menz	menza	k1gFnPc2	menza
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Základní	základní	k2eAgInSc1d1	základní
kámen	kámen	k1gInSc1	kámen
nové	nový	k2eAgFnSc2d1	nová
budovy	budova	k1gFnSc2	budova
byl	být	k5eAaImAgInS	být
položen	položit	k5eAaPmNgInS	položit
14	[number]	k4	14
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1996	[number]	k4	1996
za	za	k7c2	za
přítomnosti	přítomnost	k1gFnSc2	přítomnost
tehdejšího	tehdejší	k2eAgMnSc2d1	tehdejší
ministra	ministr	k1gMnSc2	ministr
školství	školství	k1gNnSc2	školství
Ivana	Ivan	k1gMnSc2	Ivan
Pilipa	Pilip	k1gMnSc2	Pilip
a	a	k8xC	a
rektora	rektor	k1gMnSc2	rektor
Eduarda	Eduard	k1gMnSc2	Eduard
Schmidta	Schmidt	k1gMnSc2	Schmidt
<g/>
.	.	kIx.	.
</s>
<s>
Slavnostně	slavnostně	k6eAd1	slavnostně
byla	být	k5eAaImAgFnS	být
fakulta	fakulta	k1gFnSc1	fakulta
otevřena	otevřen	k2eAgFnSc1d1	otevřena
18	[number]	k4	18
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
budovou	budova	k1gFnSc7	budova
stojí	stát	k5eAaImIp3nS	stát
Plastika	plastika	k1gFnSc1	plastika
pro	pro	k7c4	pro
bazén	bazén	k1gInSc4	bazén
podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
Vincence	Vincenc	k1gMnSc2	Vincenc
Makovského	Makovský	k1gMnSc2	Makovský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Součástí	součást	k1gFnSc7	součást
budovy	budova	k1gFnSc2	budova
je	být	k5eAaImIp3nS	být
také	také	k9	také
Středisko	středisko	k1gNnSc1	středisko
vědeckých	vědecký	k2eAgFnPc2d1	vědecká
informací	informace	k1gFnPc2	informace
s	s	k7c7	s
rozsáhlým	rozsáhlý	k2eAgInSc7d1	rozsáhlý
knižním	knižní	k2eAgInSc7d1	knižní
fondem	fond	k1gInSc7	fond
a	a	k8xC	a
přístupem	přístup	k1gInSc7	přístup
do	do	k7c2	do
řady	řada	k1gFnSc2	řada
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
databází	databáze	k1gFnPc2	databáze
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Děkanát	děkanát	k1gInSc4	děkanát
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
fakulty	fakulta	k1gFnSc2	fakulta
stojí	stát	k5eAaImIp3nS	stát
děkan	děkan	k1gMnSc1	děkan
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
jejím	její	k3xOp3gNnSc7	její
jménem	jméno	k1gNnSc7	jméno
jedná	jednat	k5eAaImIp3nS	jednat
a	a	k8xC	a
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
<g/>
.	.	kIx.	.
</s>
<s>
Děkana	děkan	k1gMnSc4	děkan
na	na	k7c4	na
návrh	návrh	k1gInSc4	návrh
fakultního	fakultní	k2eAgInSc2d1	fakultní
akademického	akademický	k2eAgInSc2d1	akademický
senátu	senát	k1gInSc2	senát
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
a	a	k8xC	a
odvolává	odvolávat	k5eAaImIp3nS	odvolávat
rektor	rektor	k1gMnSc1	rektor
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Funkční	funkční	k2eAgNnSc1d1	funkční
období	období	k1gNnSc1	období
děkana	děkan	k1gMnSc2	děkan
je	být	k5eAaImIp3nS	být
čtyřleté	čtyřletý	k2eAgNnSc1d1	čtyřleté
a	a	k8xC	a
stejná	stejný	k2eAgFnSc1d1	stejná
osoba	osoba	k1gFnSc1	osoba
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
děkanem	děkan	k1gMnSc7	děkan
nejvýše	vysoce	k6eAd3	vysoce
dvakrát	dvakrát	k6eAd1	dvakrát
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
<g/>
.	.	kIx.	.
</s>
<s>
Děkan	děkan	k1gMnSc1	děkan
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
a	a	k8xC	a
odvolává	odvolávat	k5eAaImIp3nS	odvolávat
proděkany	proděkan	k1gMnPc4	proděkan
a	a	k8xC	a
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
o	o	k7c6	o
jejich	jejich	k3xOp3gFnSc6	jejich
činnosti	činnost	k1gFnSc6	činnost
a	a	k8xC	a
počtu	počet	k1gInSc6	počet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Symboly	symbol	k1gInPc1	symbol
fakulty	fakulta	k1gFnSc2	fakulta
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Logo	logo	k1gNnSc1	logo
a	a	k8xC	a
barva	barva	k1gFnSc1	barva
===	===	k?	===
</s>
</p>
<p>
<s>
Znak	znak	k1gInSc1	znak
fakulty	fakulta	k1gFnSc2	fakulta
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
symboly	symbol	k1gInPc4	symbol
hospodářství	hospodářství	k1gNnSc2	hospodářství
<g/>
,	,	kIx,	,
obchodu	obchod	k1gInSc2	obchod
a	a	k8xC	a
správy	správa	k1gFnSc2	správa
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středu	střed	k1gInSc6	střed
znaku	znak	k1gInSc2	znak
je	být	k5eAaImIp3nS	být
Merkurova	Merkurův	k2eAgFnSc1d1	Merkurova
hůl	hůl	k1gFnSc1	hůl
<g/>
,	,	kIx,	,
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
Caduceus	Caduceus	k1gInSc1	Caduceus
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
ní	on	k3xPp3gFnSc2	on
se	se	k3xPyFc4	se
obtáčejí	obtáčet	k5eAaImIp3nP	obtáčet
dva	dva	k4xCgMnPc1	dva
hadi	had	k1gMnPc1	had
<g/>
.	.	kIx.	.
</s>
<s>
Merkur	Merkur	k1gMnSc1	Merkur
<g/>
,	,	kIx,	,
římský	římský	k2eAgMnSc1d1	římský
bůh	bůh	k1gMnSc1	bůh
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
patronem	patron	k1gMnSc7	patron
obchodu	obchod	k1gInSc2	obchod
<g/>
,	,	kIx,	,
obchodníků	obchodník	k1gMnPc2	obchodník
a	a	k8xC	a
cest	cesta	k1gFnPc2	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Hůl	hůl	k1gFnSc1	hůl
s	s	k7c7	s
hady	had	k1gMnPc7	had
kříží	křížit	k5eAaImIp3nS	křížit
maršálská	maršálský	k2eAgFnSc1d1	maršálská
hůl	hůl	k1gFnSc1	hůl
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
řízení	řízení	k1gNnSc4	řízení
a	a	k8xC	a
správu	správa	k1gFnSc4	správa
světských	světský	k2eAgFnPc2d1	světská
věcí	věc	k1gFnPc2	věc
<g/>
.	.	kIx.	.
</s>
<s>
Dominantou	dominanta	k1gFnSc7	dominanta
loga	logo	k1gNnSc2	logo
je	být	k5eAaImIp3nS	být
průčelí	průčelí	k1gNnSc4	průčelí
antického	antický	k2eAgInSc2d1	antický
chrámu	chrám	k1gInSc2	chrám
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
význam	význam	k1gInSc1	význam
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
různý	různý	k2eAgInSc4d1	různý
–	–	k?	–
jako	jako	k8xS	jako
znak	znak	k1gInSc1	znak
vzdělanosti	vzdělanost	k1gFnSc2	vzdělanost
podle	podle	k7c2	podle
symbolu	symbol	k1gInSc2	symbol
chrámu	chrám	k1gInSc2	chrám
Athény	Athéna	k1gFnSc2	Athéna
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
symbol	symbol	k1gInSc1	symbol
instituce	instituce	k1gFnSc2	instituce
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jako	jako	k8xC	jako
symbol	symbol	k1gInSc1	symbol
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
vědy	věda	k1gFnSc2	věda
obecně	obecně	k6eAd1	obecně
<g/>
.	.	kIx.	.
</s>
<s>
Barva	barva	k1gFnSc1	barva
fakulty	fakulta	k1gFnSc2	fakulta
byla	být	k5eAaImAgFnS	být
hnědá	hnědý	k2eAgFnSc1d1	hnědá
(	(	kIx(	(
<g/>
Pantone	Panton	k1gInSc5	Panton
1815	[number]	k4	1815
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2018	[number]	k4	2018
fakulta	fakulta	k1gFnSc1	fakulta
užívá	užívat	k5eAaImIp3nS	užívat
nové	nový	k2eAgNnSc4d1	nové
logo	logo	k1gNnSc4	logo
dle	dle	k7c2	dle
nového	nový	k2eAgInSc2d1	nový
vizuálního	vizuální	k2eAgInSc2d1	vizuální
stylu	styl	k1gInSc2	styl
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
vytvořeným	vytvořený	k2eAgNnSc7d1	vytvořené
studiem	studio	k1gNnSc7	studio
Najbrt	Najbrta	k1gFnPc2	Najbrta
<g/>
.	.	kIx.	.
</s>
<s>
Barvou	barva	k1gFnSc7	barva
fakulty	fakulta	k1gFnSc2	fakulta
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stala	stát	k5eAaPmAgFnS	stát
růžová	růžový	k2eAgFnSc1d1	růžová
(	(	kIx(	(
<g/>
Pantone	Panton	k1gInSc5	Panton
226	[number]	k4	226
C	C	kA	C
<g/>
/	/	kIx~	/
<g/>
U	u	k7c2	u
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
s	s	k7c7	s
přijetím	přijetí	k1gNnSc7	přijetí
nového	nový	k2eAgNnSc2d1	nové
loga	logo	k1gNnSc2	logo
fakulta	fakulta	k1gFnSc1	fakulta
změnila	změnit	k5eAaPmAgFnS	změnit
svou	svůj	k3xOyFgFnSc4	svůj
zkratku	zkratka	k1gFnSc4	zkratka
na	na	k7c6	na
ECON	ECON	kA	ECON
MUNI	MUNI	k?	MUNI
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Insignie	insignie	k1gFnPc4	insignie
===	===	k?	===
</s>
</p>
<p>
<s>
Ekonomicko-správní	ekonomickoprávní	k2eAgFnSc1d1	ekonomicko-správní
fakulta	fakulta	k1gFnSc1	fakulta
používá	používat	k5eAaImIp3nS	používat
při	při	k7c6	při
významných	významný	k2eAgFnPc6d1	významná
událostech	událost	k1gFnPc6	událost
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
slavnostní	slavnostní	k2eAgFnPc1d1	slavnostní
promoce	promoce	k1gFnPc1	promoce
<g/>
,	,	kIx,	,
insignie	insignie	k1gFnPc1	insignie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
medaili	medaile	k1gFnSc6	medaile
má	mít	k5eAaImIp3nS	mít
portrét	portrét	k1gInSc1	portrét
prvního	první	k4xOgMnSc2	první
rektora	rektor	k1gMnSc2	rektor
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
Karla	Karel	k1gMnSc2	Karel
Engliše	Engliš	k1gMnSc2	Engliš
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
je	být	k5eAaImIp3nS	být
motto	motto	k1gNnSc1	motto
"	"	kIx"	"
<g/>
Výkonnost	výkonnost	k1gFnSc1	výkonnost
<g/>
,	,	kIx,	,
hospodárnost	hospodárnost	k1gFnSc1	hospodárnost
<g/>
,	,	kIx,	,
solidnost	solidnost	k1gFnSc1	solidnost
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Hlavici	hlavice	k1gFnSc4	hlavice
žezla	žezlo	k1gNnSc2	žezlo
tvoří	tvořit	k5eAaImIp3nS	tvořit
motiv	motiv	k1gInSc4	motiv
okřídlené	okřídlený	k2eAgFnSc2d1	okřídlená
přilby	přilba	k1gFnSc2	přilba
<g/>
.	.	kIx.	.
</s>
<s>
Návrh	návrh	k1gInSc1	návrh
řetězu	řetěz	k1gInSc2	řetěz
i	i	k8xC	i
žezla	žezlo	k1gNnSc2	žezlo
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
sochař	sochař	k1gMnSc1	sochař
Michal	Michal	k1gMnSc1	Michal
Vitanovský	Vitanovský	k2eAgMnSc1d1	Vitanovský
<g/>
.	.	kIx.	.
</s>
<s>
Žezlo	žezlo	k1gNnSc1	žezlo
podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
Vitanovského	Vitanovský	k2eAgInSc2d1	Vitanovský
zhotovil	zhotovit	k5eAaPmAgInS	zhotovit
sochař	sochař	k1gMnSc1	sochař
Pavel	Pavel	k1gMnSc1	Pavel
Filip	Filip	k1gMnSc1	Filip
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgFnPc4	svůj
insignie	insignie	k1gFnPc4	insignie
užívá	užívat	k5eAaImIp3nS	užívat
fakulta	fakulta	k1gFnSc1	fakulta
od	od	k7c2	od
listopadu	listopad	k1gInSc2	listopad
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Katedry	katedra	k1gFnSc2	katedra
==	==	k?	==
</s>
</p>
<p>
<s>
Katedra	katedra	k1gFnSc1	katedra
ekonomie	ekonomie	k1gFnSc2	ekonomie
</s>
</p>
<p>
<s>
Katedra	katedra	k1gFnSc1	katedra
financí	finance	k1gFnPc2	finance
</s>
</p>
<p>
<s>
Katedra	katedra	k1gFnSc1	katedra
podnikového	podnikový	k2eAgNnSc2d1	podnikové
hospodářství	hospodářství	k1gNnSc2	hospodářství
</s>
</p>
<p>
<s>
Katedra	katedra	k1gFnSc1	katedra
regionální	regionální	k2eAgFnSc2d1	regionální
ekonomie	ekonomie	k1gFnSc2	ekonomie
a	a	k8xC	a
správy	správa	k1gFnSc2	správa
</s>
</p>
<p>
<s>
Katedra	katedra	k1gFnSc1	katedra
veřejné	veřejný	k2eAgFnSc2d1	veřejná
ekonomie	ekonomie	k1gFnSc2	ekonomie
</s>
</p>
<p>
<s>
Katedra	katedra	k1gFnSc1	katedra
aplikované	aplikovaný	k2eAgFnSc2d1	aplikovaná
matematiky	matematika	k1gFnSc2	matematika
a	a	k8xC	a
informatiky	informatika	k1gFnSc2	informatika
</s>
</p>
<p>
<s>
Katedra	katedra	k1gFnSc1	katedra
práva	právo	k1gNnSc2	právo
</s>
</p>
<p>
<s>
Oddělení	oddělení	k1gNnSc1	oddělení
jazyků	jazyk	k1gInPc2	jazyk
na	na	k7c6	na
ECON	ECON	kA	ECON
MUNI	MUNI	k?	MUNI
</s>
</p>
<p>
<s>
==	==	k?	==
Výzkum	výzkum	k1gInSc1	výzkum
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Výzkumná	výzkumný	k2eAgNnPc1d1	výzkumné
centra	centrum	k1gNnPc1	centrum
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
fakultě	fakulta	k1gFnSc6	fakulta
působí	působit	k5eAaImIp3nS	působit
několik	několik	k4yIc1	několik
vědeckých	vědecký	k2eAgInPc2d1	vědecký
institutů	institut	k1gInPc2	institut
-	-	kIx~	-
Institute	institut	k1gInSc5	institut
of	of	k?	of
Transport	transport	k1gInSc1	transport
Economics	Economicsa	k1gFnPc2	Economicsa
<g/>
,	,	kIx,	,
Geography	Geographa	k1gFnSc2	Geographa
and	and	k?	and
Policy	Polica	k1gFnSc2	Polica
<g/>
,	,	kIx,	,
Centrum	centrum	k1gNnSc1	centrum
pro	pro	k7c4	pro
výzkum	výzkum	k1gInSc4	výzkum
neziskového	ziskový	k2eNgInSc2d1	neziskový
sektoru	sektor	k1gInSc2	sektor
a	a	k8xC	a
Institut	institut	k1gInSc1	institut
cestovního	cestovní	k2eAgInSc2d1	cestovní
ruchu	ruch	k1gInSc2	ruch
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
na	na	k7c6	na
fakultě	fakulta	k1gFnSc6	fakulta
působí	působit	k5eAaImIp3nS	působit
také	také	k9	také
Centrum	centrum	k1gNnSc1	centrum
výzkumu	výzkum	k1gInSc2	výzkum
konkurenční	konkurenční	k2eAgFnSc2d1	konkurenční
schopnosti	schopnost	k1gFnSc2	schopnost
české	český	k2eAgFnSc2d1	Česká
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
koncipováno	koncipován	k2eAgNnSc4d1	koncipováno
jako	jako	k8xC	jako
analytické	analytický	k2eAgNnSc4d1	analytické
pracoviště	pracoviště	k1gNnSc4	pracoviště
zaměřené	zaměřený	k2eAgNnSc4d1	zaměřené
na	na	k7c4	na
aplikaci	aplikace	k1gFnSc4	aplikace
sociálněvědních	sociálněvědní	k2eAgInPc2d1	sociálněvědní
poznatků	poznatek	k1gInPc2	poznatek
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
konkurenční	konkurenční	k2eAgFnSc2d1	konkurenční
schopnosti	schopnost	k1gFnSc2	schopnost
při	při	k7c6	při
hodnocení	hodnocení	k1gNnSc6	hodnocení
jejích	její	k3xOp3gInPc2	její
předpokladů	předpoklad	k1gInPc2	předpoklad
a	a	k8xC	a
výsledků	výsledek	k1gInPc2	výsledek
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
a	a	k8xC	a
jejích	její	k3xOp3gFnPc2	její
hospodářskopolitických	hospodářskopolitický	k2eAgFnPc2d1	hospodářskopolitická
implikací	implikace	k1gFnPc2	implikace
v	v	k7c6	v
návaznosti	návaznost	k1gFnSc6	návaznost
na	na	k7c6	na
plnění	plnění	k1gNnSc6	plnění
cílů	cíl	k1gInPc2	cíl
Lisabonské	lisabonský	k2eAgFnSc2d1	Lisabonská
strategie	strategie	k1gFnSc2	strategie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Konference	konference	k1gFnSc1	konference
===	===	k?	===
</s>
</p>
<p>
<s>
Fakulta	fakulta	k1gFnSc1	fakulta
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
své	svůj	k3xOyFgNnSc4	svůj
vědecké	vědecký	k2eAgNnSc4d1	vědecké
působení	působení	k1gNnSc4	působení
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
organizování	organizování	k1gNnSc2	organizování
konferencí	konference	k1gFnPc2	konference
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
International	Internationat	k5eAaPmAgMnS	Internationat
PhD	PhD	k1gFnSc4	PhD
Students	Studentsa	k1gFnPc2	Studentsa
Conference	Conference	k1gFnSc2	Conference
–	–	k?	–
New	New	k1gMnSc1	New
Economic	Economic	k1gMnSc1	Economic
Challenges	Challenges	k1gMnSc1	Challenges
</s>
</p>
<p>
<s>
Mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
kolokvium	kolokvium	k1gNnSc1	kolokvium
o	o	k7c6	o
regionálních	regionální	k2eAgFnPc6d1	regionální
vědách	věda	k1gFnPc6	věda
</s>
</p>
<p>
<s>
Mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
kolokvium	kolokvium	k1gNnSc1	kolokvium
o	o	k7c6	o
cestovním	cestovní	k2eAgInSc6d1	cestovní
ruchu	ruch	k1gInSc6	ruch
</s>
</p>
<p>
<s>
Evropské	evropský	k2eAgInPc1d1	evropský
finanční	finanční	k2eAgInPc1d1	finanční
systémy	systém	k1gInPc1	systém
</s>
</p>
<p>
<s>
Seminář	seminář	k1gInSc1	seminář
Šlapanice	Šlapanice	k1gFnSc2	Šlapanice
–	–	k?	–
Current	Current	k1gInSc1	Current
Trends	Trends	k1gInSc1	Trends
in	in	k?	in
Public	publicum	k1gNnPc2	publicum
Sector	Sector	k1gMnSc1	Sector
Research	Research	k1gMnSc1	Research
</s>
</p>
<p>
<s>
Seminář	seminář	k1gInSc1	seminář
Telč	Telč	k1gFnSc1	Telč
–	–	k?	–
Železniční	železniční	k2eAgFnSc1d1	železniční
doprava	doprava	k1gFnSc1	doprava
</s>
</p>
<p>
<s>
===	===	k?	===
Publikace	publikace	k1gFnSc1	publikace
===	===	k?	===
</s>
</p>
<p>
<s>
Fakulta	fakulta	k1gFnSc1	fakulta
zaštiťuje	zaštiťovat	k5eAaImIp3nS	zaštiťovat
vydávání	vydávání	k1gNnSc2	vydávání
tří	tři	k4xCgNnPc2	tři
odborných	odborný	k2eAgNnPc2d1	odborné
periodik	periodikum	k1gNnPc2	periodikum
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Národohospodářský	národohospodářský	k2eAgInSc1d1	národohospodářský
obzor	obzor	k1gInSc1	obzor
je	být	k5eAaImIp3nS	být
recenzovaný	recenzovaný	k2eAgInSc1d1	recenzovaný
odborný	odborný	k2eAgInSc1d1	odborný
časopis	časopis	k1gInSc1	časopis
schválený	schválený	k2eAgInSc1d1	schválený
Radou	rada	k1gFnSc7	rada
pro	pro	k7c4	pro
výzkum	výzkum	k1gInSc4	výzkum
a	a	k8xC	a
vývoj	vývoj	k1gInSc4	vývoj
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
přinášet	přinášet	k5eAaImF	přinášet
nové	nový	k2eAgInPc4d1	nový
poznatky	poznatek	k1gInPc4	poznatek
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
ekonomie	ekonomie	k1gFnSc2	ekonomie
<g/>
,	,	kIx,	,
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
politiky	politika	k1gFnSc2	politika
a	a	k8xC	a
ekonometrie	ekonometrie	k1gFnSc2	ekonometrie
<g/>
.	.	kIx.	.
</s>
<s>
Časopis	časopis	k1gInSc1	časopis
vycházející	vycházející	k2eAgInSc1d1	vycházející
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
na	na	k7c6	na
Masarykově	Masarykův	k2eAgFnSc6d1	Masarykova
univerzitě	univerzita	k1gFnSc6	univerzita
volně	volně	k6eAd1	volně
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
tradici	tradice	k1gFnSc4	tradice
Obzoru	obzor	k1gInSc2	obzor
národohospodářského	národohospodářský	k2eAgInSc2d1	národohospodářský
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgNnSc4	jenž
první	první	k4xOgNnSc4	první
číslo	číslo	k1gNnSc4	číslo
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1895	[number]	k4	1895
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
jsou	být	k5eAaImIp3nP	být
publikovány	publikován	k2eAgFnPc4d1	publikována
původní	původní	k2eAgFnPc4d1	původní
vědecké	vědecký	k2eAgFnPc4d1	vědecká
stati	stať	k1gFnPc4	stať
a	a	k8xC	a
přehledy	přehled	k1gInPc4	přehled
v	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
jazyce	jazyk	k1gInSc6	jazyk
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
prošly	projít	k5eAaPmAgFnP	projít
oboustranně	oboustranně	k6eAd1	oboustranně
anonymním	anonymní	k2eAgNnSc7d1	anonymní
recenzním	recenzní	k2eAgNnSc7d1	recenzní
řízením	řízení	k1gNnSc7	řízení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Financial	Financial	k1gInSc1	Financial
Assets	Assets	k1gInSc1	Assets
and	and	k?	and
Investing	Investing	k1gInSc1	Investing
je	být	k5eAaImIp3nS	být
odborný	odborný	k2eAgInSc1d1	odborný
recenzovaný	recenzovaný	k2eAgInSc1d1	recenzovaný
časopis	časopis	k1gInSc1	časopis
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
na	na	k7c4	na
finanční	finanční	k2eAgInPc4d1	finanční
trhy	trh	k1gInPc4	trh
<g/>
,	,	kIx,	,
bankovnictví	bankovnictví	k1gNnSc4	bankovnictví
<g/>
,	,	kIx,	,
pojištění	pojištění	k1gNnSc1	pojištění
apod.	apod.	kA	apod.
</s>
</p>
<p>
<s>
Czech	Czech	k1gMnSc1	Czech
Journal	Journal	k1gMnSc1	Journal
of	of	k?	of
Tourism	Tourism	k1gInSc1	Tourism
je	být	k5eAaImIp3nS	být
vědecký	vědecký	k2eAgInSc1d1	vědecký
časopis	časopis	k1gInSc1	časopis
publikující	publikující	k2eAgInSc1d1	publikující
především	především	k9	především
recenzované	recenzovaný	k2eAgFnPc4d1	recenzovaná
vědecké	vědecký	k2eAgFnPc4d1	vědecká
práce	práce	k1gFnPc4	práce
a	a	k8xC	a
studie	studie	k1gFnPc4	studie
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
vycházejí	vycházet	k5eAaImIp3nP	vycházet
z	z	k7c2	z
teoretických	teoretický	k2eAgInPc2d1	teoretický
poznatků	poznatek	k1gInPc2	poznatek
a	a	k8xC	a
vlastního	vlastní	k2eAgInSc2d1	vlastní
výzkumu	výzkum	k1gInSc2	výzkum
autorů	autor	k1gMnPc2	autor
<g/>
.	.	kIx.	.
</s>
<s>
Časopis	časopis	k1gInSc1	časopis
je	být	k5eAaImIp3nS	být
zaměřen	zaměřit	k5eAaPmNgInS	zaměřit
na	na	k7c4	na
cestovní	cestovní	k2eAgInSc4d1	cestovní
ruch	ruch	k1gInSc4	ruch
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
jeho	jeho	k3xOp3gInSc4	jeho
interdisciplinární	interdisciplinární	k2eAgInSc4d1	interdisciplinární
charakter	charakter	k1gInSc4	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Věnuje	věnovat	k5eAaPmIp3nS	věnovat
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
<g/>
,	,	kIx,	,
environmentální	environmentální	k2eAgFnSc2d1	environmentální
<g/>
,	,	kIx,	,
etické	etický	k2eAgFnSc2d1	etická
<g/>
,	,	kIx,	,
kognitivní	kognitivní	k2eAgFnSc2d1	kognitivní
<g/>
,	,	kIx,	,
manažerské	manažerský	k2eAgFnSc2d1	manažerská
<g/>
,	,	kIx,	,
politické	politický	k2eAgFnSc3d1	politická
a	a	k8xC	a
prostorové	prostorový	k2eAgFnSc3d1	prostorová
stránce	stránka	k1gFnSc3	stránka
cestovního	cestovní	k2eAgInSc2d1	cestovní
ruchu	ruch	k1gInSc2	ruch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Ekonomicko-správní	ekonomickoprávní	k2eAgFnSc1d1	ekonomicko-správní
fakulta	fakulta	k1gFnSc1	fakulta
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
fakulty	fakulta	k1gFnSc2	fakulta
</s>
</p>
<p>
<s>
Aktuality	aktualita	k1gFnPc1	aktualita
z	z	k7c2	z
ekonomicko-správní	ekonomickoprávní	k2eAgFnSc2d1	ekonomicko-správní
fakulty	fakulta	k1gFnSc2	fakulta
na	na	k7c6	na
zpravodajském	zpravodajský	k2eAgInSc6d1	zpravodajský
portálu	portál	k1gInSc6	portál
MU	MU	kA	MU
</s>
</p>
<p>
<s>
Ekonomicko-správní	ekonomickoprávní	k2eAgFnSc1d1	ekonomicko-správní
fakulta	fakulta	k1gFnSc1	fakulta
MU	MU	kA	MU
na	na	k7c6	na
Facebooku	Facebook	k1gInSc6	Facebook
</s>
</p>
