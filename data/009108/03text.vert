<p>
<s>
Korint	Korint	k1gInSc1	Korint
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
Κ	Κ	k?	Κ
<g/>
,	,	kIx,	,
Kórinthos	Kórinthos	k1gInSc1	Kórinthos
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
starověký	starověký	k2eAgInSc1d1	starověký
řecký	řecký	k2eAgInSc1d1	řecký
městský	městský	k2eAgInSc1d1	městský
stát	stát	k1gInSc1	stát
a	a	k8xC	a
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
řecké	řecký	k2eAgNnSc1d1	řecké
město	město	k1gNnSc1	město
ležící	ležící	k2eAgNnSc1d1	ležící
při	při	k7c6	při
Korintské	korintský	k2eAgFnSc6d1	Korintská
šíji	šíj	k1gFnSc6	šíj
(	(	kIx(	(
<g/>
Isthmos	Isthmos	k1gInSc1	Isthmos
<g/>
)	)	kIx)	)
–	–	k?	–
úzkém	úzký	k2eAgInSc6d1	úzký
pásu	pás	k1gInSc6	pás
země	zem	k1gFnSc2	zem
spojujícím	spojující	k2eAgNnSc7d1	spojující
Peloponéský	peloponéský	k2eAgInSc1d1	peloponéský
poloostrov	poloostrov	k1gInSc4	poloostrov
s	s	k7c7	s
řeckou	řecký	k2eAgFnSc7d1	řecká
pevninou	pevnina	k1gFnSc7	pevnina
<g/>
.	.	kIx.	.
</s>
<s>
Isthmos	Isthmos	k1gInSc1	Isthmos
je	být	k5eAaImIp3nS	být
sevřen	sevřít	k5eAaPmNgInS	sevřít
mezi	mezi	k7c7	mezi
Korintským	korintský	k2eAgInSc7d1	korintský
zálivem	záliv	k1gInSc7	záliv
na	na	k7c6	na
západě	západ	k1gInSc6	západ
a	a	k8xC	a
Sarónským	Sarónský	k2eAgInSc7d1	Sarónský
zálivem	záliv	k1gInSc7	záliv
na	na	k7c6	na
východě	východ	k1gInSc6	východ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
antických	antický	k2eAgFnPc6d1	antická
dobách	doba	k1gFnPc6	doba
byl	být	k5eAaImAgInS	být
zdoláván	zdolávat	k5eAaImNgInS	zdolávat
tažením	tažení	k1gNnSc7	tažení
lodí	loď	k1gFnSc7	loď
na	na	k7c6	na
saních	saně	k1gFnPc6	saně
po	po	k7c6	po
dlážděné	dlážděný	k2eAgFnSc6d1	dlážděná
cestě	cesta	k1gFnSc6	cesta
(	(	kIx(	(
<g/>
diolkos	diolkos	k1gInSc1	diolkos
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
od	od	k7c2	od
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
je	být	k5eAaImIp3nS	být
přeťat	přetít	k5eAaPmNgInS	přetít
Korintským	korintský	k2eAgInSc7d1	korintský
kanálem	kanál	k1gInSc7	kanál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dnešní	dnešní	k2eAgInSc1d1	dnešní
Korint	Korint	k1gInSc1	Korint
je	být	k5eAaImIp3nS	být
správním	správní	k2eAgNnSc7d1	správní
centrem	centrum	k1gNnSc7	centrum
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
řecké	řecký	k2eAgFnSc2d1	řecká
prefektury	prefektura	k1gFnSc2	prefektura
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zhruba	zhruba	k6eAd1	zhruba
80	[number]	k4	80
kilometrů	kilometr	k1gInPc2	kilometr
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
Athén	Athéna	k1gFnPc2	Athéna
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
pobřeží	pobřeží	k1gNnSc4	pobřeží
obklopeno	obklopen	k2eAgNnSc4d1	obklopeno
vesnicemi	vesnice	k1gFnPc7	vesnice
Lechaion	Lechaion	k1gInSc1	Lechaion
<g/>
,	,	kIx,	,
Ishtmia	Ishtmia	k1gFnSc1	Ishtmia
a	a	k8xC	a
Kenchreie	Kenchreie	k1gFnSc1	Kenchreie
<g/>
.	.	kIx.	.
</s>
<s>
Směrem	směr	k1gInSc7	směr
k	k	k7c3	k
Peloponésu	Peloponés	k1gInSc3	Peloponés
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
obec	obec	k1gFnSc1	obec
Examilia	Examilia	k1gFnSc1	Examilia
a	a	k8xC	a
místo	místo	k1gNnSc1	místo
archeologického	archeologický	k2eAgNnSc2d1	Archeologické
naleziště	naleziště	k1gNnSc2	naleziště
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
Korintu	Korint	k1gInSc2	Korint
podle	podle	k7c2	podle
údajů	údaj	k1gInPc2	údaj
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
činí	činit	k5eAaImIp3nS	činit
36	[number]	k4	36
555	[number]	k4	555
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Prehistorie	prehistorie	k1gFnSc2	prehistorie
a	a	k8xC	a
mytologie	mytologie	k1gFnSc2	mytologie
===	===	k?	===
</s>
</p>
<p>
<s>
První	první	k4xOgNnSc1	první
rané	raný	k2eAgNnSc1d1	rané
sídlo	sídlo	k1gNnSc1	sídlo
zde	zde	k6eAd1	zde
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
zřejmě	zřejmě	k6eAd1	zřejmě
někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
mladší	mladý	k2eAgFnSc6d2	mladší
době	doba	k1gFnSc6	doba
kamenné	kamenný	k2eAgInPc1d1	kamenný
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
dvě	dva	k4xCgFnPc1	dva
legendy	legenda	k1gFnPc1	legenda
ohledně	ohledně	k7c2	ohledně
vzniku	vznik	k1gInSc2	vznik
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
první	první	k4xOgMnSc1	první
založil	založit	k5eAaPmAgMnS	založit
město	město	k1gNnSc4	město
Korinthos	Korinthos	k1gMnSc1	Korinthos
<g/>
,	,	kIx,	,
potomek	potomek	k1gMnSc1	potomek
boha	bůh	k1gMnSc2	bůh
Hélia	hélium	k1gNnSc2	hélium
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
druhá	druhý	k4xOgFnSc1	druhý
praví	pravit	k5eAaImIp3nS	pravit
<g/>
,	,	kIx,	,
že	že	k8xS	že
zakladatelem	zakladatel	k1gMnSc7	zakladatel
města	město	k1gNnSc2	město
byla	být	k5eAaImAgFnS	být
bohyně	bohyně	k1gFnSc1	bohyně
Efyra	Efyra	k1gFnSc1	Efyra
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
titána	titán	k1gMnSc2	titán
Okeana	Okean	k1gMnSc2	Okean
(	(	kIx(	(
<g/>
odtud	odtud	k6eAd1	odtud
také	také	k9	také
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
antických	antický	k2eAgNnPc2d1	antické
jmen	jméno	k1gNnPc2	jméno
Korintu	Korint	k1gInSc2	Korint
–	–	k?	–
Efyra	Efyra	k1gMnSc1	Efyra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
druhého	druhý	k4xOgNnSc2	druhý
a	a	k8xC	a
třetího	třetí	k4xOgNnSc2	třetí
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
mělo	mít	k5eAaImAgNnS	mít
zdejší	zdejší	k2eAgNnSc1d1	zdejší
osídlení	osídlení	k1gNnSc1	osídlení
přechodně	přechodně	k6eAd1	přechodně
zaniknout	zaniknout	k5eAaPmF	zaniknout
<g/>
.	.	kIx.	.
</s>
<s>
Trvalo	trvalo	k1gNnSc1	trvalo
dalších	další	k2eAgInPc2d1	další
téměř	téměř	k6eAd1	téměř
1000	[number]	k4	1000
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
na	na	k7c6	na
konci	konec	k1gInSc6	konec
mykénského	mykénský	k2eAgNnSc2d1	mykénské
období	období	k1gNnSc2	období
usadili	usadit	k5eAaPmAgMnP	usadit
Dórové	Dór	k1gMnPc1	Dór
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Název	název	k1gInSc1	název
tohoto	tento	k3xDgNnSc2	tento
místa	místo	k1gNnSc2	místo
–	–	k?	–
Korinthos	Korinthosa	k1gFnPc2	Korinthosa
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
z	z	k7c2	z
jazyka	jazyk	k1gInSc2	jazyk
Pelasgů	Pelasg	k1gInPc2	Pelasg
<g/>
,	,	kIx,	,
národa	národ	k1gInSc2	národ
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
obýval	obývat	k5eAaImAgMnS	obývat
území	území	k1gNnSc4	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Řecka	Řecko	k1gNnSc2	Řecko
v	v	k7c6	v
dobách	doba	k1gFnPc6	doba
před	před	k7c7	před
příchodem	příchod	k1gInSc7	příchod
řeckých	řecký	k2eAgInPc2d1	řecký
kmenů	kmen	k1gInPc2	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Korint	Korint	k1gInSc1	Korint
byl	být	k5eAaImAgInS	být
zřejmě	zřejmě	k6eAd1	zřejmě
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
Mykény	Mykény	k1gFnPc1	Mykény
<g/>
,	,	kIx,	,
Tíryns	Tíryns	k1gInSc1	Tíryns
nebo	nebo	k8xC	nebo
Pylos	Pylos	k1gInSc1	Pylos
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
palácových	palácový	k2eAgNnPc2d1	palácové
center	centrum	k1gNnPc2	centrum
v	v	k7c6	v
době	doba	k1gFnSc6	doba
mykénské	mykénský	k2eAgFnSc2d1	mykénská
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
mýtů	mýtus	k1gInPc2	mýtus
byl	být	k5eAaImAgInS	být
předkem	předek	k1gMnSc7	předek
korintských	korintský	k2eAgMnPc2d1	korintský
králů	král	k1gMnPc2	král
bájný	bájný	k2eAgMnSc1d1	bájný
Sisyfos	Sisyfos	k1gMnSc1	Sisyfos
<g/>
.	.	kIx.	.
</s>
<s>
Korint	Korint	k1gMnSc1	Korint
byl	být	k5eAaImAgMnS	být
rovněž	rovněž	k9	rovněž
místem	místo	k1gNnSc7	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
měl	mít	k5eAaImAgMnS	mít
Iásón	Iásón	k1gMnSc1	Iásón
<g/>
,	,	kIx,	,
vůdce	vůdce	k1gMnSc1	vůdce
legendárních	legendární	k2eAgMnPc2d1	legendární
Argonautů	argonaut	k1gMnPc2	argonaut
<g/>
,	,	kIx,	,
opustit	opustit	k5eAaPmF	opustit
svoji	svůj	k3xOyFgFnSc4	svůj
manželku	manželka	k1gFnSc4	manželka
Médeu	Médea	k1gFnSc4	Médea
<g/>
.	.	kIx.	.
</s>
<s>
Korinťané	Korinťan	k1gMnPc1	Korinťan
jsou	být	k5eAaImIp3nP	být
zmiňováni	zmiňován	k2eAgMnPc1d1	zmiňován
i	i	k8xC	i
Homérem	Homér	k1gMnSc7	Homér
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
trójskou	trójský	k2eAgFnSc7d1	Trójská
válkou	válka	k1gFnSc7	válka
<g/>
,	,	kIx,	,
které	který	k3yRgFnSc2	který
se	se	k3xPyFc4	se
měli	mít	k5eAaImAgMnP	mít
zúčastnit	zúčastnit	k5eAaPmF	zúčastnit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Klasické	klasický	k2eAgNnSc4d1	klasické
období	období	k1gNnSc4	období
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
klasické	klasický	k2eAgFnSc2d1	klasická
antiky	antika	k1gFnSc2	antika
soupeřil	soupeřit	k5eAaImAgInS	soupeřit
Korint	Korint	k1gInSc1	Korint
s	s	k7c7	s
Athénami	Athéna	k1gFnPc7	Athéna
a	a	k8xC	a
Megarou	Megara	k1gFnSc7	Megara
<g/>
.	.	kIx.	.
</s>
<s>
Moc	moc	k1gFnSc1	moc
tohoto	tento	k3xDgNnSc2	tento
města	město	k1gNnSc2	město
pocházela	pocházet	k5eAaImAgFnS	pocházet
z	z	k7c2	z
bohatství	bohatství	k1gNnSc2	bohatství
plynoucího	plynoucí	k2eAgInSc2d1	plynoucí
z	z	k7c2	z
obchodu	obchod	k1gInSc2	obchod
a	a	k8xC	a
z	z	k7c2	z
poplatků	poplatek	k1gInPc2	poplatek
za	za	k7c4	za
přepravu	přeprava	k1gFnSc4	přeprava
přes	přes	k7c4	přes
Isthmos	Isthmos	k1gInSc4	Isthmos
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
byl	být	k5eAaImAgMnS	být
Korint	Korint	k1gMnSc1	Korint
hlavním	hlavní	k2eAgMnSc7d1	hlavní
vývozcem	vývozce	k1gMnSc7	vývozce
keramiky	keramika	k1gFnSc2	keramika
s	s	k7c7	s
černými	černý	k2eAgFnPc7d1	černá
postavami	postava	k1gFnPc7	postava
do	do	k7c2	do
téměř	téměř	k6eAd1	téměř
všech	všecek	k3xTgFnPc2	všecek
obcí	obec	k1gFnPc2	obec
starověkého	starověký	k2eAgInSc2d1	starověký
řeckého	řecký	k2eAgInSc2d1	řecký
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
však	však	k9	však
začalo	začít	k5eAaPmAgNnS	začít
obchodu	obchod	k1gInSc3	obchod
s	s	k7c7	s
keramikou	keramika	k1gFnSc7	keramika
dominovat	dominovat	k5eAaImF	dominovat
zboží	zboží	k1gNnSc4	zboží
produkované	produkovaný	k2eAgNnSc4d1	produkované
v	v	k7c6	v
Athénách	Athéna	k1gFnPc6	Athéna
<g/>
.	.	kIx.	.
</s>
<s>
Korintský	korintský	k2eAgInSc1d1	korintský
chrám	chrám	k1gInSc1	chrám
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
starověkého	starověký	k2eAgInSc2d1	starověký
akropole	akropole	k1gFnSc1	akropole
(	(	kIx(	(
<g/>
Akrokorint	Akrokorint	k1gInSc1	Akrokorint
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
zasvěcen	zasvěcen	k2eAgInSc1d1	zasvěcen
bohyni	bohyně	k1gFnSc4	bohyně
lásky	láska	k1gFnSc2	láska
<g/>
,	,	kIx,	,
Afrodíté	Afrodítý	k2eAgInPc1d1	Afrodítý
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
většiny	většina	k1gFnSc2	většina
antických	antický	k2eAgInPc2d1	antický
pramenů	pramen	k1gInPc2	pramen
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
chrámu	chrám	k1gInSc6	chrám
působilo	působit	k5eAaImAgNnS	působit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
tisíc	tisíc	k4xCgInSc4	tisíc
chrámových	chrámový	k2eAgFnPc2d1	chrámová
kněžek	kněžka	k1gFnPc2	kněžka
–	–	k?	–
prostitutek	prostitutka	k1gFnPc2	prostitutka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Korintu	Korint	k1gInSc6	Korint
se	se	k3xPyFc4	se
pravidelně	pravidelně	k6eAd1	pravidelně
konaly	konat	k5eAaImAgFnP	konat
rovněž	rovněž	k9	rovněž
isthmické	isthmický	k2eAgFnPc1d1	isthmický
hry	hra	k1gFnPc1	hra
<g/>
,	,	kIx,	,
vedle	vedle	k7c2	vedle
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
jedny	jeden	k4xCgFnPc1	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
panhelénských	panhelénský	k2eAgFnPc2d1	panhelénský
kulturních	kulturní	k2eAgFnPc2d1	kulturní
a	a	k8xC	a
sportovních	sportovní	k2eAgFnPc2d1	sportovní
slavností	slavnost	k1gFnPc2	slavnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
tyranů	tyran	k1gMnPc2	tyran
Kypsela	Kypsela	k1gFnSc1	Kypsela
(	(	kIx(	(
<g/>
657	[number]	k4	657
až	až	k9	až
627	[number]	k4	627
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeho	on	k3xPp3gMnSc2	on
syna	syn	k1gMnSc2	syn
Periandra	Periandr	k1gMnSc2	Periandr
(	(	kIx(	(
<g/>
627	[number]	k4	627
až	až	k9	až
585	[number]	k4	585
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
mnoho	mnoho	k4c1	mnoho
korintských	korintský	k2eAgMnPc2d1	korintský
občanů	občan	k1gMnPc2	občan
vysláno	vyslán	k2eAgNnSc1d1	vysláno
do	do	k7c2	do
zámoří	zámoří	k1gNnSc2	zámoří
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
četné	četný	k2eAgFnPc1d1	četná
kolonie	kolonie	k1gFnPc1	kolonie
jako	jako	k9	jako
například	například	k6eAd1	například
Epidamnos	Epidamnosa	k1gFnPc2	Epidamnosa
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgFnSc1d1	dnešní
Drač	Drač	k1gFnSc1	Drač
<g/>
,	,	kIx,	,
Albánie	Albánie	k1gFnSc1	Albánie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Syrakusy	Syrakusy	k1gFnPc4	Syrakusy
<g/>
,	,	kIx,	,
Ambrakia	Ambrakia	k1gFnSc1	Ambrakia
a	a	k8xC	a
Kerkýra	Kerkýra	k1gFnSc1	Kerkýra
<g/>
.	.	kIx.	.
</s>
<s>
Periandros	Periandrosa	k1gFnPc2	Periandrosa
dal	dát	k5eAaPmAgMnS	dát
založit	založit	k5eAaPmF	založit
také	také	k9	také
Apollónii	Apollónie	k1gFnSc4	Apollónie
(	(	kIx(	(
<g/>
Fier	Fier	k1gInSc1	Fier
<g/>
,	,	kIx,	,
Albánie	Albánie	k1gFnSc1	Albánie
<g/>
)	)	kIx)	)
a	a	k8xC	a
Poteidaiu	Poteidaius	k1gMnSc3	Poteidaius
(	(	kIx(	(
<g/>
na	na	k7c6	na
poloostrově	poloostrov	k1gInSc6	poloostrov
Chalkidiké	Chalkidiká	k1gFnSc2	Chalkidiká
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Korint	Korint	k1gInSc1	Korint
byl	být	k5eAaImAgInS	být
i	i	k9	i
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
devíti	devět	k4xCc2	devět
řeckých	řecký	k2eAgFnPc2d1	řecká
obcí	obec	k1gFnPc2	obec
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
podílely	podílet	k5eAaImAgFnP	podílet
na	na	k7c6	na
založení	založení	k1gNnSc6	založení
kolonie	kolonie	k1gFnSc2	kolonie
Naukratis	Naukratis	k1gFnSc2	Naukratis
ve	v	k7c6	v
starověkém	starověký	k2eAgInSc6d1	starověký
Egyptě	Egypt	k1gInSc6	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Vybudování	vybudování	k1gNnSc1	vybudování
Naukratidy	Naukratida	k1gFnSc2	Naukratida
mělo	mít	k5eAaImAgNnS	mít
přispět	přispět	k5eAaPmF	přispět
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
vzájemného	vzájemný	k2eAgInSc2d1	vzájemný
obchodu	obchod	k1gInSc2	obchod
mezi	mezi	k7c7	mezi
řeckými	řecký	k2eAgFnPc7d1	řecká
obcemi	obec	k1gFnPc7	obec
a	a	k8xC	a
Egyptem	Egypt	k1gInSc7	Egypt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Periandros	Periandrosa	k1gFnPc2	Periandrosa
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
řazen	řazen	k2eAgInSc1d1	řazen
mezi	mezi	k7c7	mezi
tzv.	tzv.	kA	tzv.
sedm	sedm	k4xCc1	sedm
řeckých	řecký	k2eAgMnPc2d1	řecký
mudrců	mudrc	k1gMnPc2	mudrc
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
Korintu	Korint	k1gInSc6	Korint
raženy	ražen	k2eAgFnPc4d1	ražena
první	první	k4xOgFnPc4	první
mince	mince	k1gFnPc4	mince
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
první	první	k4xOgMnSc1	první
také	také	k9	také
pojal	pojmout	k5eAaPmAgMnS	pojmout
úmysl	úmysl	k1gInSc4	úmysl
prokopat	prokopat	k5eAaPmF	prokopat
Isthmos	Isthmos	k1gInSc4	Isthmos
a	a	k8xC	a
vytvořit	vytvořit	k5eAaPmF	vytvořit
tak	tak	k9	tak
kanál	kanál	k1gInSc1	kanál
spojující	spojující	k2eAgInSc1d1	spojující
Korintský	korintský	k2eAgInSc1d1	korintský
a	a	k8xC	a
Sarónský	Sarónský	k2eAgInSc1d1	Sarónský
záliv	záliv	k1gInSc1	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Narazil	narazit	k5eAaPmAgMnS	narazit
však	však	k9	však
na	na	k7c4	na
nepřekonatelné	překonatelný	k2eNgFnPc4d1	nepřekonatelná
technické	technický	k2eAgFnPc4d1	technická
překážky	překážka	k1gFnPc4	překážka
a	a	k8xC	a
od	od	k7c2	od
svého	svůj	k3xOyFgInSc2	svůj
záměru	záměr	k1gInSc2	záměr
proto	proto	k8xC	proto
upustil	upustit	k5eAaPmAgMnS	upustit
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
však	však	k9	však
nechal	nechat	k5eAaPmAgMnS	nechat
vybudovat	vybudovat	k5eAaPmF	vybudovat
diolkos	diolkos	k1gInSc4	diolkos
<g/>
.	.	kIx.	.
</s>
<s>
Období	období	k1gNnSc1	období
vlády	vláda	k1gFnSc2	vláda
Kypselidů	Kypselid	k1gInPc2	Kypselid
skončilo	skončit	k5eAaPmAgNnS	skončit
za	za	k7c4	za
Periandrova	Periandrův	k2eAgMnSc4d1	Periandrův
synovce	synovec	k1gMnSc4	synovec
Psammeticha	Psammetich	k1gMnSc4	Psammetich
<g/>
,	,	kIx,	,
pojmenovaného	pojmenovaný	k2eAgMnSc4d1	pojmenovaný
podle	podle	k7c2	podle
egyptského	egyptský	k2eAgMnSc2d1	egyptský
faraóna	faraón	k1gMnSc2	faraón
a	a	k8xC	a
nadšeného	nadšený	k2eAgMnSc2d1	nadšený
helénofila	helénofil	k1gMnSc2	helénofil
Psammetika	Psammetik	k1gMnSc2	Psammetik
I.	I.	kA	I.
Korint	Korint	k1gMnSc1	Korint
zažíval	zažívat	k5eAaImAgMnS	zažívat
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
své	svůj	k3xOyFgNnSc4	svůj
vrcholné	vrcholný	k2eAgNnSc4d1	vrcholné
období	období	k1gNnSc4	období
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tehdy	tehdy	k6eAd1	tehdy
byl	být	k5eAaImAgInS	být
vyvinut	vyvinout	k5eAaPmNgInS	vyvinout
korintský	korintský	k2eAgInSc1d1	korintský
řád	řád	k1gInSc1	řád
<g/>
,	,	kIx,	,
po	po	k7c6	po
iónském	iónský	k2eAgInSc6d1	iónský
a	a	k8xC	a
dórském	dórský	k2eAgInSc6d1	dórský
řádu	řád	k1gInSc6	řád
třetí	třetí	k4xOgFnPc1	třetí
sloh	sloha	k1gFnPc2	sloha
klasické	klasický	k2eAgFnSc2d1	klasická
antické	antický	k2eAgFnSc2d1	antická
architektury	architektura	k1gFnSc2	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Hlavice	hlavice	k1gFnSc1	hlavice
korintského	korintský	k2eAgInSc2d1	korintský
sloupu	sloup	k1gInSc2	sloup
byla	být	k5eAaImAgFnS	být
nejsložitější	složitý	k2eAgFnSc1d3	nejsložitější
a	a	k8xC	a
nejpropracovanější	propracovaný	k2eAgFnSc1d3	nejpropracovanější
z	z	k7c2	z
oněch	onen	k3xDgFnPc2	onen
tří	tři	k4xCgFnPc2	tři
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
symbolizovala	symbolizovat	k5eAaImAgFnS	symbolizovat
bohatý	bohatý	k2eAgInSc4d1	bohatý
a	a	k8xC	a
luxusní	luxusní	k2eAgInSc4d1	luxusní
životní	životní	k2eAgInSc4d1	životní
styl	styl	k1gInSc4	styl
tohoto	tento	k3xDgInSc2	tento
antického	antický	k2eAgInSc2d1	antický
městského	městský	k2eAgInSc2d1	městský
státu	stát	k1gInSc2	stát
(	(	kIx(	(
<g/>
polis	polis	k1gInSc1	polis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
dórský	dórský	k2eAgInSc4d1	dórský
řád	řád	k1gInSc4	řád
znázorňoval	znázorňovat	k5eAaImAgInS	znázorňovat
přísný	přísný	k2eAgInSc1d1	přísný
a	a	k8xC	a
prostý	prostý	k2eAgInSc1d1	prostý
život	život	k1gInSc1	život
starých	starý	k2eAgMnPc2d1	starý
Dórů	Dór	k1gMnPc2	Dór
–	–	k?	–
obyvatel	obyvatel	k1gMnSc1	obyvatel
Sparty	Sparta	k1gFnSc2	Sparta
<g/>
.	.	kIx.	.
</s>
<s>
Iónský	iónský	k2eAgInSc1d1	iónský
byl	být	k5eAaImAgInS	být
vedle	vedle	k7c2	vedle
těchto	tento	k3xDgFnPc2	tento
dvou	dva	k4xCgFnPc2	dva
příkladem	příklad	k1gInSc7	příklad
harmonie	harmonie	k1gFnSc2	harmonie
<g/>
,	,	kIx,	,
vyváženosti	vyváženost	k1gFnSc2	vyváženost
a	a	k8xC	a
filozofie	filozofie	k1gFnSc2	filozofie
Iónů	Ión	k1gMnPc2	Ión
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
něž	jenž	k3xRgMnPc4	jenž
se	se	k3xPyFc4	se
řadili	řadit	k5eAaImAgMnP	řadit
Athéňané	Athéňan	k1gMnPc1	Athéňan
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
obyvatelé	obyvatel	k1gMnPc1	obyvatel
řeckých	řecký	k2eAgNnPc2d1	řecké
měst	město	k1gNnPc2	město
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
Malé	Malé	k2eAgFnSc2d1	Malé
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
slavného	slavný	k2eAgNnSc2d1	slavné
období	období	k1gNnSc2	období
Korintu	Korint	k1gInSc2	Korint
pochází	pocházet	k5eAaImIp3nS	pocházet
věta	věta	k1gFnSc1	věta
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Ou	ou	k0	ou
pantos	pantos	k1gInSc4	pantos
plein	plein	k1gInSc1	plein
es	es	k1gNnSc1	es
Korinthon	Korinthon	k1gInSc1	Korinthon
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
lze	lze	k6eAd1	lze
volně	volně	k6eAd1	volně
přeložit	přeložit	k5eAaPmF	přeložit
jako	jako	k8xS	jako
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Ne	ne	k9	ne
každý	každý	k3xTgMnSc1	každý
může	moct	k5eAaImIp3nS	moct
jít	jít	k5eAaImF	jít
do	do	k7c2	do
Korintu	Korint	k1gInSc2	Korint
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
je	být	k5eAaImIp3nS	být
připomínán	připomínán	k2eAgInSc4d1	připomínán
zdejší	zdejší	k2eAgInSc4d1	zdejší
nákladný	nákladný	k2eAgInSc4d1	nákladný
životní	životní	k2eAgInSc4d1	životní
styl	styl	k1gInSc4	styl
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
proslulé	proslulý	k2eAgNnSc1d1	proslulé
svými	svůj	k3xOyFgFnPc7	svůj
kněžkami	kněžka	k1gFnPc7	kněžka
–	–	k?	–
prostitutkami	prostitutka	k1gFnPc7	prostitutka
z	z	k7c2	z
Afroditina	Afroditin	k2eAgInSc2d1	Afroditin
chrámu	chrám	k1gInSc2	chrám
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
dopřávaly	dopřávat	k5eAaImAgFnP	dopřávat
potěšení	potěšení	k1gNnSc4	potěšení
bohatým	bohatý	k2eAgMnPc3d1	bohatý
obchodníkům	obchodník	k1gMnPc3	obchodník
a	a	k8xC	a
mocným	mocný	k2eAgMnPc3d1	mocný
politikům	politik	k1gMnPc3	politik
během	během	k7c2	během
jejich	jejich	k3xOp3gFnPc2	jejich
cest	cesta	k1gFnPc2	cesta
do	do	k7c2	do
Korintu	Korint	k1gInSc2	Korint
<g/>
.	.	kIx.	.
</s>
<s>
Říká	říkat	k5eAaImIp3nS	říkat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejslavnější	slavný	k2eAgMnSc1d3	nejslavnější
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
,	,	kIx,	,
Lais	Laisa	k1gFnPc2	Laisa
<g/>
,	,	kIx,	,
získávala	získávat	k5eAaImAgFnS	získávat
od	od	k7c2	od
svých	svůj	k3xOyFgMnPc2	svůj
milenců	milenec	k1gMnPc2	milenec
za	za	k7c4	za
své	svůj	k3xOyFgFnPc4	svůj
mimořádné	mimořádný	k2eAgFnPc4d1	mimořádná
služby	služba	k1gFnPc4	služba
neuvěřitelné	uvěřitelný	k2eNgFnPc4d1	neuvěřitelná
sumy	suma	k1gFnPc4	suma
peněz	peníze	k1gInPc2	peníze
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Město	město	k1gNnSc1	město
disponovalo	disponovat	k5eAaBmAgNnS	disponovat
dvěma	dva	k4xCgInPc7	dva
přístavy	přístav	k1gInPc7	přístav
<g/>
,	,	kIx,	,
jedním	jeden	k4xCgMnSc7	jeden
v	v	k7c6	v
Korintském	korintský	k2eAgMnSc6d1	korintský
a	a	k8xC	a
druhým	druhý	k4xOgNnSc7	druhý
v	v	k7c6	v
Sarónském	Sarónský	k2eAgInSc6d1	Sarónský
zálivu	záliv	k1gInSc6	záliv
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
městu	město	k1gNnSc3	město
umožňovaly	umožňovat	k5eAaImAgFnP	umožňovat
snadno	snadno	k6eAd1	snadno
obchodovat	obchodovat	k5eAaImF	obchodovat
s	s	k7c7	s
oblastmi	oblast	k1gFnPc7	oblast
východního	východní	k2eAgInSc2d1	východní
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
i	i	k9	i
západního	západní	k2eAgNnSc2d1	západní
Středomoří	středomoří	k1gNnSc2	středomoří
<g/>
.	.	kIx.	.
</s>
<s>
Lechaion	Lechaion	k1gInSc1	Lechaion
v	v	k7c6	v
Korintském	korintský	k2eAgInSc6d1	korintský
zálivu	záliv	k1gInSc6	záliv
zajišťoval	zajišťovat	k5eAaImAgMnS	zajišťovat
městu	město	k1gNnSc3	město
spojení	spojení	k1gNnSc1	spojení
s	s	k7c7	s
jeho	jeho	k3xOp3gFnPc7	jeho
koloniemi	kolonie	k1gFnPc7	kolonie
na	na	k7c6	na
západě	západ	k1gInSc6	západ
a	a	k8xC	a
s	s	k7c7	s
obcemi	obec	k1gFnPc7	obec
Velkého	velký	k2eAgNnSc2d1	velké
Řecka	Řecko	k1gNnSc2	Řecko
(	(	kIx(	(
<g/>
Magna	Magen	k2eAgFnSc1d1	Magna
Graecia	Graecia	k1gFnSc1	Graecia
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Kenchreie	Kenchreie	k1gFnPc1	Kenchreie
sloužily	sloužit	k5eAaImAgFnP	sloužit
lodím	loď	k1gFnPc3	loď
plujícím	plující	k2eAgInSc6d1	plující
z	z	k7c2	z
Athén	Athéna	k1gFnPc2	Athéna
<g/>
,	,	kIx,	,
Iónie	Iónie	k1gFnSc2	Iónie
<g/>
,	,	kIx,	,
Kypru	Kypr	k1gInSc2	Kypr
a	a	k8xC	a
Fénicie	Fénicie	k1gFnSc2	Fénicie
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
přístavy	přístav	k1gInPc1	přístav
byly	být	k5eAaImAgInP	být
rovněž	rovněž	k9	rovněž
vybaveny	vybaven	k2eAgInPc1d1	vybaven
rozlehlými	rozlehlý	k2eAgInPc7d1	rozlehlý
doky	dok	k1gInPc7	dok
pro	pro	k7c4	pro
ukotvení	ukotvení	k1gNnSc4	ukotvení
válečného	válečný	k2eAgNnSc2d1	válečné
loďstva	loďstvo	k1gNnSc2	loďstvo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
řecko-perských	řeckoerský	k2eAgFnPc2d1	řecko-perský
válek	válka	k1gFnPc2	válka
poskytl	poskytnout	k5eAaPmAgInS	poskytnout
Korint	Korint	k1gInSc1	Korint
40	[number]	k4	40
lodí	loď	k1gFnPc2	loď
do	do	k7c2	do
bitvy	bitva	k1gFnSc2	bitva
u	u	k7c2	u
Salamíny	Salamína	k1gFnSc2	Salamína
(	(	kIx(	(
<g/>
480	[number]	k4	480
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
velel	velet	k5eAaImAgMnS	velet
admirál	admirál	k1gMnSc1	admirál
Adeimantos	Adeimantos	k1gMnSc1	Adeimantos
<g/>
,	,	kIx,	,
a	a	k8xC	a
5	[number]	k4	5
000	[number]	k4	000
hoplítů	hoplít	k1gInPc2	hoplít
(	(	kIx(	(
<g/>
nesoucích	nesoucí	k2eAgFnPc2d1	nesoucí
charakteristické	charakteristický	k2eAgFnPc4d1	charakteristická
korintské	korintský	k2eAgFnPc4d1	Korintská
helmice	helmice	k1gFnPc4	helmice
<g/>
)	)	kIx)	)
v	v	k7c6	v
brzy	brzy	k6eAd1	brzy
následující	následující	k2eAgFnSc6d1	následující
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Platají	Platají	k1gFnSc2	Platají
(	(	kIx(	(
<g/>
479	[number]	k4	479
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
celé	celá	k1gFnSc6	celá
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
byl	být	k5eAaImAgMnS	být
Korint	Korint	k1gMnSc1	Korint
často	často	k6eAd1	často
spojencem	spojenec	k1gMnSc7	spojenec
Sparty	Sparta	k1gFnSc2	Sparta
a	a	k8xC	a
nepřítelem	nepřítel	k1gMnSc7	nepřítel
Athén	Athéna	k1gFnPc2	Athéna
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
zřejmě	zřejmě	k6eAd1	zřejmě
také	také	k9	také
důsledkem	důsledek	k1gInSc7	důsledek
vzájemného	vzájemný	k2eAgNnSc2d1	vzájemné
tradičního	tradiční	k2eAgNnSc2d1	tradiční
obchodního	obchodní	k2eAgNnSc2d1	obchodní
soupeření	soupeření	k1gNnSc2	soupeření
obou	dva	k4xCgFnPc2	dva
těchto	tento	k3xDgNnPc2	tento
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Otevřené	otevřený	k2eAgNnSc4d1	otevřené
nepřátelství	nepřátelství	k1gNnSc4	nepřátelství
vůči	vůči	k7c3	vůči
Athénám	Athéna	k1gFnPc3	Athéna
se	se	k3xPyFc4	se
rozhořelo	rozhořet	k5eAaPmAgNnS	rozhořet
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
Athéňan	Athéňan	k1gMnSc1	Athéňan
Kimón	Kimón	k1gMnSc1	Kimón
v	v	k7c6	v
roce	rok	k1gInSc6	rok
462	[number]	k4	462
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
pronikl	proniknout	k5eAaPmAgMnS	proniknout
se	s	k7c7	s
svým	svůj	k3xOyFgNnSc7	svůj
vojskem	vojsko	k1gNnSc7	vojsko
bez	bez	k7c2	bez
dovolení	dovolení	k1gNnSc2	dovolení
na	na	k7c4	na
korintské	korintský	k2eAgNnSc4d1	korintské
území	území	k1gNnSc4	území
<g/>
.	.	kIx.	.
</s>
<s>
Došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
válce	válka	k1gFnSc3	válka
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
Korint	Korint	k1gMnSc1	Korint
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
spojencem	spojenec	k1gMnSc7	spojenec
Epidaurem	Epidaur	k1gMnSc7	Epidaur
Athény	Athéna	k1gFnSc2	Athéna
nejprve	nejprve	k6eAd1	nejprve
porazil	porazit	k5eAaPmAgMnS	porazit
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
však	však	k9	však
Athénám	Athéna	k1gFnPc3	Athéna
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
v	v	k7c6	v
námořní	námořní	k2eAgFnSc6d1	námořní
bitvě	bitva	k1gFnSc6	bitva
v	v	k7c6	v
Sarónském	Sarónský	k2eAgInSc6d1	Sarónský
zálivu	záliv	k1gInSc6	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
451	[number]	k4	451
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
bylo	být	k5eAaImAgNnS	být
uzavřeno	uzavřít	k5eAaPmNgNnS	uzavřít
příměří	příměří	k1gNnSc1	příměří
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
faktorů	faktor	k1gInPc2	faktor
vedoucích	vedoucí	k2eAgInPc2d1	vedoucí
k	k	k7c3	k
vypuknutí	vypuknutí	k1gNnSc3	vypuknutí
peloponéské	peloponéský	k2eAgFnSc2d1	Peloponéská
války	válka	k1gFnSc2	válka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
431	[number]	k4	431
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
byl	být	k5eAaImAgMnS	být
spor	spor	k1gInSc4	spor
mezi	mezi	k7c7	mezi
Korintem	Korint	k1gInSc7	Korint
a	a	k8xC	a
Athénami	Athéna	k1gFnPc7	Athéna
týkající	týkající	k2eAgFnSc2d1	týkající
se	se	k3xPyFc4	se
korintské	korintský	k2eAgFnSc2d1	Korintská
kolonie	kolonie	k1gFnSc2	kolonie
Kerkýry	Kerkýr	k1gInPc1	Kerkýr
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
peloponéské	peloponéský	k2eAgFnSc2d1	Peloponéská
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
někdejší	někdejší	k2eAgMnPc1d1	někdejší
spojenci	spojenec	k1gMnPc1	spojenec
Sparty	Sparta	k1gFnSc2	Sparta
z	z	k7c2	z
peloponéského	peloponéský	k2eAgInSc2d1	peloponéský
spolku	spolek	k1gInSc2	spolek
<g/>
,	,	kIx,	,
Korint	Korinta	k1gFnPc2	Korinta
a	a	k8xC	a
Théby	Théby	k1gFnPc4	Théby
<g/>
,	,	kIx,	,
cítili	cítit	k5eAaImAgMnP	cítit
podvedeni	podvést	k5eAaPmNgMnP	podvést
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc4	jejich
narůstající	narůstající	k2eAgFnSc4d1	narůstající
nespokojenost	nespokojenost	k1gFnSc4	nespokojenost
s	s	k7c7	s
hegemonním	hegemonní	k2eAgNnSc7d1	hegemonní
postavením	postavení	k1gNnSc7	postavení
Sparty	Sparta	k1gFnSc2	Sparta
vyústila	vyústit	k5eAaPmAgFnS	vyústit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
394	[number]	k4	394
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
v	v	k7c4	v
korintskou	korintský	k2eAgFnSc4d1	Korintská
válku	válka	k1gFnSc4	válka
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yQgFnPc4	který
Korint	Korint	k1gMnSc1	Korint
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Athénami	Athéna	k1gFnPc7	Athéna
<g/>
,	,	kIx,	,
Thébami	Théby	k1gFnPc7	Théby
a	a	k8xC	a
Argem	Argos	k1gInSc7	Argos
bojoval	bojovat	k5eAaImAgMnS	bojovat
proti	proti	k7c3	proti
Spartě	Sparta	k1gFnSc3	Sparta
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
provedli	provést	k5eAaPmAgMnP	provést
demokraté	demokrat	k1gMnPc1	demokrat
v	v	k7c6	v
Korintu	Korint	k1gInSc6	Korint
převrat	převrat	k1gInSc1	převrat
a	a	k8xC	a
výsledkem	výsledek	k1gInSc7	výsledek
toho	ten	k3xDgNnSc2	ten
bylo	být	k5eAaImAgNnS	být
dočasné	dočasný	k2eAgNnSc4d1	dočasné
politické	politický	k2eAgNnSc4d1	politické
sjednocení	sjednocení	k1gNnSc4	sjednocení
s	s	k7c7	s
Argem	Argos	k1gInSc7	Argos
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
386	[number]	k4	386
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
však	však	k9	však
byli	být	k5eAaImAgMnP	být
Korinťané	Korinťan	k1gMnPc1	Korinťan
přinuceni	přinucen	k2eAgMnPc1d1	přinucen
akceptovat	akceptovat	k5eAaBmF	akceptovat
podmínky	podmínka	k1gFnPc4	podmínka
královského	královský	k2eAgInSc2d1	královský
míru	mír	k1gInSc2	mír
<g/>
.	.	kIx.	.
</s>
<s>
Argos	Argos	k1gMnSc1	Argos
se	se	k3xPyFc4	se
musel	muset	k5eAaImAgMnS	muset
stáhnout	stáhnout	k5eAaPmF	stáhnout
z	z	k7c2	z
Korintu	Korint	k1gInSc2	Korint
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
se	se	k3xPyFc4	se
moci	moc	k1gFnSc3	moc
chopili	chopit	k5eAaPmAgMnP	chopit
aristokraté	aristokrat	k1gMnPc1	aristokrat
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
přiklonili	přiklonit	k5eAaPmAgMnP	přiklonit
ke	k	k7c3	k
Spartě	Sparta	k1gFnSc3	Sparta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Makedonská	makedonský	k2eAgFnSc1d1	makedonská
a	a	k8xC	a
římská	římský	k2eAgFnSc1d1	římská
nadvláda	nadvláda	k1gFnSc1	nadvláda
===	===	k?	===
</s>
</p>
<p>
<s>
Slabost	slabost	k1gFnSc1	slabost
řeckých	řecký	k2eAgFnPc2d1	řecká
obcí	obec	k1gFnPc2	obec
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
nepřetržitých	přetržitý	k2eNgInPc2d1	nepřetržitý
bratrovražedných	bratrovražedný	k2eAgInPc2d1	bratrovražedný
konfliktů	konflikt	k1gInPc2	konflikt
usnadnila	usnadnit	k5eAaPmAgFnS	usnadnit
vzestup	vzestup	k1gInSc4	vzestup
Makedonie	Makedonie	k1gFnSc2	Makedonie
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Po	po	k7c6	po
vítězství	vítězství	k1gNnSc6	vítězství
Filipa	Filip	k1gMnSc2	Filip
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Makedonského	makedonský	k2eAgInSc2d1	makedonský
nad	nad	k7c7	nad
Helény	Helén	k1gMnPc7	Helén
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Chairóneie	Chairóneie	k1gFnSc2	Chairóneie
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
korintský	korintský	k2eAgInSc1d1	korintský
spolek	spolek	k1gInSc1	spolek
namířený	namířený	k2eAgInSc1d1	namířený
proti	proti	k7c3	proti
perské	perský	k2eAgFnSc3d1	perská
říši	říš	k1gFnSc3	říš
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
řecké	řecký	k2eAgFnPc1d1	řecká
obce	obec	k1gFnPc1	obec
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Korintu	Korint	k1gInSc2	Korint
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
staly	stát	k5eAaPmAgFnP	stát
poddanými	poddaná	k1gFnPc7	poddaná
makedonského	makedonský	k2eAgMnSc2d1	makedonský
krále	král	k1gMnSc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Filipově	Filipův	k2eAgNnSc6d1	Filipovo
zavraždění	zavraždění	k1gNnSc6	zavraždění
v	v	k7c6	v
roce	rok	k1gInSc6	rok
336	[number]	k4	336
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
spolkový	spolkový	k2eAgInSc4d1	spolkový
sněm	sněm	k1gInSc4	sněm
(	(	kIx(	(
<g/>
synedrion	synedrion	k1gNnSc4	synedrion
<g/>
)	)	kIx)	)
Filipova	Filipův	k2eAgMnSc2d1	Filipův
syna	syn	k1gMnSc2	syn
Alexandra	Alexandr	k1gMnSc2	Alexandr
Makedonského	makedonský	k2eAgMnSc2d1	makedonský
v	v	k7c6	v
pozici	pozice	k1gFnSc6	pozice
hegemona	hegemon	k1gMnSc2	hegemon
spolku	spolek	k1gInSc2	spolek
pro	pro	k7c4	pro
plánované	plánovaný	k2eAgNnSc4d1	plánované
tažení	tažení	k1gNnSc4	tažení
na	na	k7c4	na
východ	východ	k1gInSc4	východ
<g/>
.	.	kIx.	.
</s>
<s>
Korint	Korint	k1gMnSc1	Korint
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
časech	čas	k1gInPc6	čas
domovem	domov	k1gInSc7	domov
kynického	kynický	k2eAgMnSc2d1	kynický
filozofa	filozof	k1gMnSc2	filozof
Diogena	Diogenes	k1gMnSc2	Diogenes
ze	z	k7c2	z
Sinópé	Sinópý	k2eAgFnSc2d1	Sinópý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
desetiletích	desetiletí	k1gNnPc6	desetiletí
představoval	představovat	k5eAaImAgInS	představovat
Korint	Korint	k1gInSc1	Korint
pevnou	pevný	k2eAgFnSc4d1	pevná
základnu	základna	k1gFnSc4	základna
makedonské	makedonský	k2eAgFnSc2d1	makedonská
moci	moc	k1gFnSc2	moc
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Démétrios	Démétrios	k1gInSc1	Démétrios
Poliorkétés	Poliorkétésa	k1gFnPc2	Poliorkétésa
a	a	k8xC	a
po	po	k7c6	po
něm	on	k3xPp3gMnSc6	on
i	i	k9	i
další	další	k2eAgMnPc1d1	další
makedonští	makedonský	k2eAgMnPc1d1	makedonský
vojevůdci	vojevůdce	k1gMnPc1	vojevůdce
se	se	k3xPyFc4	se
zmocnili	zmocnit	k5eAaPmAgMnP	zmocnit
města	město	k1gNnSc2	město
a	a	k8xC	a
umístili	umístit	k5eAaPmAgMnP	umístit
své	svůj	k3xOyFgMnPc4	svůj
vojáky	voják	k1gMnPc4	voják
do	do	k7c2	do
strategicky	strategicky	k6eAd1	strategicky
důležité	důležitý	k2eAgFnSc2d1	důležitá
pevnosti	pevnost	k1gFnSc2	pevnost
Akrokorint	Akrokorinta	k1gFnPc2	Akrokorinta
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
ztrátě	ztráta	k1gFnSc3	ztráta
své	svůj	k3xOyFgFnSc2	svůj
politické	politický	k2eAgFnSc2d1	politická
nezávislosti	nezávislost	k1gFnSc2	nezávislost
zažíval	zažívat	k5eAaImAgInS	zažívat
Korint	Korint	k1gInSc1	Korint
po	po	k7c6	po
celé	celá	k1gFnSc6	celá
3	[number]	k4	3
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
výrazný	výrazný	k2eAgInSc1d1	výrazný
ekonomický	ekonomický	k2eAgInSc1d1	ekonomický
rozmach	rozmach	k1gInSc1	rozmach
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
z	z	k7c2	z
něho	on	k3xPp3gMnSc2	on
opět	opět	k6eAd1	opět
učinil	učinit	k5eAaPmAgMnS	učinit
jedno	jeden	k4xCgNnSc4	jeden
z	z	k7c2	z
nejbohatších	bohatý	k2eAgNnPc2d3	nejbohatší
měst	město	k1gNnPc2	město
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
bylo	být	k5eAaImAgNnS	být
díky	díky	k7c3	díky
svému	svůj	k3xOyFgNnSc3	svůj
kvetoucímu	kvetoucí	k2eAgNnSc3d1	kvetoucí
hospodářství	hospodářství	k1gNnSc3	hospodářství
a	a	k8xC	a
kulturnímu	kulturní	k2eAgInSc3d1	kulturní
životu	život	k1gInSc3	život
známo	znám	k2eAgNnSc1d1	známo
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
helénistickém	helénistický	k2eAgInSc6d1	helénistický
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
243	[number]	k4	243
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
ukončil	ukončit	k5eAaPmAgMnS	ukončit
makedonskou	makedonský	k2eAgFnSc4d1	makedonská
vládu	vláda	k1gFnSc4	vláda
stratég	stratég	k1gMnSc1	stratég
achajského	achajský	k2eAgInSc2d1	achajský
spolku	spolek	k1gInSc2	spolek
<g/>
,	,	kIx,	,
Arátos	Arátos	k1gInSc1	Arátos
ze	z	k7c2	z
Sikyónu	Sikyón	k1gInSc2	Sikyón
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
město	město	k1gNnSc4	město
přepadl	přepadnout	k5eAaPmAgMnS	přepadnout
a	a	k8xC	a
obsadil	obsadit	k5eAaPmAgMnS	obsadit
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
tohoto	tento	k3xDgMnSc2	tento
významného	významný	k2eAgMnSc2d1	významný
státníka	státník	k1gMnSc2	státník
se	se	k3xPyFc4	se
Korint	Korint	k1gMnSc1	Korint
připojil	připojit	k5eAaPmAgMnS	připojit
k	k	k7c3	k
achajskému	achajský	k2eAgInSc3d1	achajský
spolku	spolek	k1gInSc3	spolek
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
ale	ale	k9	ale
nespokojení	spokojený	k2eNgMnPc1d1	nespokojený
obyvatelé	obyvatel	k1gMnPc1	obyvatel
města	město	k1gNnSc2	město
obrátili	obrátit	k5eAaPmAgMnP	obrátit
s	s	k7c7	s
žádostí	žádost	k1gFnSc7	žádost
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
na	na	k7c4	na
spartského	spartský	k2eAgMnSc4d1	spartský
krále	král	k1gMnSc4	král
Kleomena	Kleomen	k2eAgFnSc1d1	Kleomena
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
přenechal	přenechat	k5eAaPmAgMnS	přenechat
Arátos	Arátos	k1gMnSc1	Arátos
vládu	vláda	k1gFnSc4	vláda
nad	nad	k7c7	nad
Korintem	Korint	k1gInSc7	Korint
v	v	k7c6	v
roce	rok	k1gInSc6	rok
224	[number]	k4	224
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
makedonskému	makedonský	k2eAgInSc3d1	makedonský
králi	král	k1gMnPc7	král
Antigonu	Antigon	k1gInSc2	Antigon
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Dósónovi	Dósónův	k2eAgMnPc1d1	Dósónův
<g/>
.	.	kIx.	.
</s>
<s>
Vítězství	vítězství	k1gNnSc1	vítězství
Římanů	Říman	k1gMnPc2	Říman
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Kynoskefal	Kynoskefal	k1gFnSc2	Kynoskefal
v	v	k7c6	v
roce	rok	k1gInSc6	rok
197	[number]	k4	197
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
přineslo	přinést	k5eAaPmAgNnS	přinést
Korinťanům	Korinťan	k1gMnPc3	Korinťan
vysvobození	vysvobození	k1gNnSc4	vysvobození
z	z	k7c2	z
makedonské	makedonský	k2eAgFnSc2d1	makedonská
nadvlády	nadvláda	k1gFnSc2	nadvláda
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
sídlem	sídlo	k1gNnSc7	sídlo
achajského	achajský	k2eAgInSc2d1	achajský
spolku	spolek	k1gInSc2	spolek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
od	od	k7c2	od
nynějška	nynějšek	k1gInSc2	nynějšek
zastával	zastávat	k5eAaImAgMnS	zastávat
ostře	ostro	k6eAd1	ostro
protiřímskou	protiřímský	k2eAgFnSc4d1	protiřímský
politiku	politika	k1gFnSc4	politika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
146	[number]	k4	146
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
achajský	achajský	k2eAgInSc1d1	achajský
spolek	spolek	k1gInSc1	spolek
válku	válka	k1gFnSc4	válka
Spartě	Sparta	k1gFnSc3	Sparta
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
ale	ale	k9	ale
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
konfliktu	konflikt	k1gInSc2	konflikt
s	s	k7c7	s
Římany	Říman	k1gMnPc7	Říman
<g/>
.	.	kIx.	.
</s>
<s>
Konzul	konzul	k1gMnSc1	konzul
Lucius	Lucius	k1gMnSc1	Lucius
Mummius	Mummius	k1gMnSc1	Mummius
Achaje	achat	k5eAaImSgInS	achat
porazil	porazit	k5eAaPmAgInS	porazit
a	a	k8xC	a
po	po	k7c4	po
dobytí	dobytí	k1gNnSc4	dobytí
Korintu	Korint	k1gInSc2	Korint
v	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
dal	dát	k5eAaPmAgMnS	dát
toto	tento	k3xDgNnSc4	tento
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
centrem	centr	k1gInSc7	centr
odporu	odpor	k1gInSc2	odpor
proti	proti	k7c3	proti
Římanům	Říman	k1gMnPc3	Říman
<g/>
,	,	kIx,	,
srovnat	srovnat	k5eAaPmF	srovnat
se	se	k3xPyFc4	se
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
ale	ale	k9	ale
Mummius	Mummius	k1gInSc1	Mummius
nechal	nechat	k5eAaPmAgInS	nechat
popravit	popravit	k5eAaPmF	popravit
všechny	všechen	k3xTgMnPc4	všechen
muže	muž	k1gMnPc4	muž
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
ostatní	ostatní	k2eAgNnSc1d1	ostatní
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
bylo	být	k5eAaImAgNnS	být
prodáno	prodat	k5eAaPmNgNnS	prodat
do	do	k7c2	do
otroctví	otroctví	k1gNnSc2	otroctví
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
tyto	tento	k3xDgFnPc4	tento
činy	čina	k1gFnPc4	čina
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
římským	římský	k2eAgInSc7d1	římský
senátem	senát	k1gInSc7	senát
jakožto	jakožto	k8xS	jakožto
přemožiteli	přemožitel	k1gMnPc7	přemožitel
Achajů	Achaj	k1gInPc2	Achaj
uděleno	udělen	k2eAgNnSc4d1	uděleno
cognomen	cognomen	k2eAgInSc4d1	cognomen
Achaicus	Achaicus	k1gInSc4	Achaicus
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
následujících	následující	k2eAgNnPc2d1	následující
let	léto	k1gNnPc2	léto
existuje	existovat	k5eAaImIp3nS	existovat
jen	jen	k9	jen
nepatrné	patrný	k2eNgNnSc1d1	nepatrné
množství	množství	k1gNnSc1	množství
důkazů	důkaz	k1gInPc2	důkaz
o	o	k7c6	o
jakémkoliv	jakýkoliv	k3yIgNnSc6	jakýkoliv
osídlení	osídlení	k1gNnSc6	osídlení
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
44	[number]	k4	44
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
znovu	znovu	k6eAd1	znovu
založeno	založit	k5eAaPmNgNnS	založit
Gaiem	Gai	k1gInSc7	Gai
Juliem	Julius	k1gMnSc7	Julius
Caesarem	Caesar	k1gMnSc7	Caesar
jako	jako	k8xC	jako
římská	římský	k2eAgFnSc1d1	římská
kolonie	kolonie	k1gFnSc1	kolonie
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Colonia	Colonium	k1gNnSc2	Colonium
Laus	Laus	k1gInSc1	Laus
Iulia	Iulius	k1gMnSc4	Iulius
Corinthiensis	Corinthiensis	k1gFnSc2	Corinthiensis
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
historika	historik	k1gMnSc2	historik
Appiána	Appián	k1gMnSc2	Appián
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
noví	nový	k2eAgMnPc1d1	nový
osadníci	osadník	k1gMnPc1	osadník
vybráni	vybrat	k5eAaPmNgMnP	vybrat
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
římských	římský	k2eAgMnPc2d1	římský
propuštěnců	propuštěnec	k1gMnPc2	propuštěnec
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Římanů	Říman	k1gMnPc2	Říman
se	se	k3xPyFc4	se
Korint	Korint	k1gInSc1	Korint
stal	stát	k5eAaPmAgInS	stát
sídlem	sídlo	k1gNnSc7	sídlo
správy	správa	k1gFnSc2	správa
provincie	provincie	k1gFnSc2	provincie
Achaia	Achaia	k1gFnSc1	Achaia
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
se	se	k3xPyFc4	se
skládalo	skládat	k5eAaImAgNnS	skládat
z	z	k7c2	z
Římanů	Říman	k1gMnPc2	Říman
<g/>
,	,	kIx,	,
Řeků	Řek	k1gMnPc2	Řek
a	a	k8xC	a
Židů	Žid	k1gMnPc2	Žid
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
známo	znám	k2eAgNnSc1d1	známo
pro	pro	k7c4	pro
své	své	k1gNnSc4	své
bohatství	bohatství	k1gNnSc2	bohatství
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
také	také	k9	také
pro	pro	k7c4	pro
svoji	svůj	k3xOyFgFnSc4	svůj
rozmařilost	rozmařilost	k1gFnSc4	rozmařilost
<g/>
,	,	kIx,	,
nemorálnost	nemorálnost	k1gFnSc4	nemorálnost
a	a	k8xC	a
zkaženost	zkaženost	k1gFnSc4	zkaženost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
prokonzulem	prokonzul	k1gMnSc7	prokonzul
Achaje	achat	k5eAaImSgInS	achat
Gallio	Gallio	k1gMnSc1	Gallio
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
51	[number]	k4	51
nebo	nebo	k8xC	nebo
52	[number]	k4	52
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bratr	bratr	k1gMnSc1	bratr
Senecy	Seneca	k1gMnSc2	Seneca
<g/>
,	,	kIx,	,
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Korint	Korint	k1gMnSc1	Korint
poprvé	poprvé	k6eAd1	poprvé
apoštol	apoštol	k1gMnSc1	apoštol
Pavel	Pavel	k1gMnSc1	Pavel
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
Pavlova	Pavlův	k2eAgInSc2d1	Pavlův
druhého	druhý	k4xOgInSc2	druhý
pobytu	pobyt	k1gInSc2	pobyt
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
roku	rok	k1gInSc2	rok
58	[number]	k4	58
zde	zde	k6eAd1	zde
zřejmě	zřejmě	k6eAd1	zřejmě
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
Listy	lista	k1gFnPc1	lista
Římanům	Říman	k1gMnPc3	Říman
<g/>
.	.	kIx.	.
</s>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
napsal	napsat	k5eAaPmAgMnS	napsat
také	také	k9	také
Listy	lista	k1gFnPc4	lista
Korintským	korintský	k2eAgFnPc3d1	Korintská
(	(	kIx(	(
<g/>
První	první	k4xOgInSc1	první
list	list	k1gInSc1	list
Korintským	korintský	k2eAgFnPc3d1	Korintská
a	a	k8xC	a
Druhý	druhý	k4xOgInSc1	druhý
list	list	k1gInSc1	list
Korintským	korintský	k2eAgMnPc3d1	korintský
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
adresovány	adresovat	k5eAaBmNgFnP	adresovat
místní	místní	k2eAgFnSc6d1	místní
křesťanské	křesťanský	k2eAgFnSc6d1	křesťanská
komunitě	komunita	k1gFnSc6	komunita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Další	další	k2eAgFnPc1d1	další
dějiny	dějiny	k1gFnPc1	dějiny
města	město	k1gNnSc2	město
===	===	k?	===
</s>
</p>
<p>
<s>
Již	již	k6eAd1	již
ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
sídlil	sídlit	k5eAaImAgMnS	sídlit
v	v	k7c6	v
Korintu	Korint	k1gInSc6	Korint
biskup	biskup	k1gInSc1	biskup
a	a	k8xC	a
nejpozději	pozdě	k6eAd3	pozdě
ve	v	k7c6	v
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
dokonce	dokonce	k9	dokonce
metropolita	metropolita	k1gMnSc1	metropolita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
267	[number]	k4	267
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
těžce	těžce	k6eAd1	těžce
poničeno	poničit	k5eAaPmNgNnS	poničit
při	při	k7c6	při
vpádu	vpád	k1gInSc6	vpád
Gótů	Gót	k1gMnPc2	Gót
a	a	k8xC	a
Herulů	Herul	k1gMnPc2	Herul
<g/>
,	,	kIx,	,
brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
však	však	k9	však
vzpamatovalo	vzpamatovat	k5eAaPmAgNnS	vzpamatovat
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
svého	svůj	k3xOyFgInSc2	svůj
někdejšího	někdejší	k2eAgInSc2d1	někdejší
lesku	lesk	k1gInSc2	lesk
a	a	k8xC	a
slávy	sláva	k1gFnSc2	sláva
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
těžkým	těžký	k2eAgInSc7d1	těžký
úderem	úder	k1gInSc7	úder
bylo	být	k5eAaImAgNnS	být
pro	pro	k7c4	pro
Korint	Korint	k1gInSc4	Korint
ničivé	ničivý	k2eAgNnSc1d1	ničivé
zemětřesení	zemětřesení	k1gNnSc1	zemětřesení
v	v	k7c6	v
roce	rok	k1gInSc6	rok
375	[number]	k4	375
následované	následovaný	k2eAgNnSc1d1	následované
Alarichovou	Alarichův	k2eAgFnSc7d1	Alarichova
invazí	invaze	k1gFnSc7	invaze
do	do	k7c2	do
Řecka	Řecko	k1gNnSc2	Řecko
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
395	[number]	k4	395
až	až	k9	až
396	[number]	k4	396
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
dokonale	dokonale	k6eAd1	dokonale
vydrancováno	vydrancovat	k5eAaPmNgNnS	vydrancovat
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
jeho	jeho	k3xOp3gMnPc2	jeho
občanů	občan	k1gMnPc2	občan
upadlo	upadnout	k5eAaPmAgNnS	upadnout
do	do	k7c2	do
otroctví	otroctví	k1gNnSc2	otroctví
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
521	[number]	k4	521
postihlo	postihnout	k5eAaPmAgNnS	postihnout
město	město	k1gNnSc1	město
nové	nový	k2eAgNnSc1d1	nové
katastrofální	katastrofální	k2eAgNnSc1d1	katastrofální
zemětřesení	zemětřesení	k1gNnSc1	zemětřesení
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
úplně	úplně	k6eAd1	úplně
zničilo	zničit	k5eAaPmAgNnS	zničit
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
císař	císař	k1gMnSc1	císař
Justinus	Justinus	k1gMnSc1	Justinus
I.	I.	kA	I.
je	on	k3xPp3gNnSc4	on
nechal	nechat	k5eAaPmAgMnS	nechat
obnovit	obnovit	k5eAaPmF	obnovit
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
císaře	císař	k1gMnSc2	císař
Justiniána	Justinián	k1gMnSc2	Justinián
I.	I.	kA	I.
byla	být	k5eAaImAgFnS	být
vystavěna	vystavěn	k2eAgFnSc1d1	vystavěna
mohutná	mohutný	k2eAgFnSc1d1	mohutná
kamenná	kamenný	k2eAgFnSc1d1	kamenná
hradba	hradba	k1gFnSc1	hradba
napříč	napříč	k7c7	napříč
Korintskou	korintský	k2eAgFnSc7d1	Korintská
šíjí	šíj	k1gFnSc7	šíj
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
měla	mít	k5eAaImAgFnS	mít
chránit	chránit	k5eAaImF	chránit
Peloponéský	peloponéský	k2eAgInSc4d1	peloponéský
poloostrov	poloostrov	k1gInSc4	poloostrov
před	před	k7c7	před
barbary	barbar	k1gMnPc7	barbar
pronikajícími	pronikající	k2eAgMnPc7d1	pronikající
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
<g/>
.	.	kIx.	.
</s>
<s>
Hradba	hradba	k1gFnSc1	hradba
byla	být	k5eAaImAgFnS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
kolem	kolem	k7c2	kolem
šesti	šest	k4xCc2	šest
mil	míle	k1gFnPc2	míle
(	(	kIx(	(
<g/>
10	[number]	k4	10
km	km	kA	km
<g/>
)	)	kIx)	)
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
Hexamilion	Hexamilion	k1gInSc1	Hexamilion
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
šest	šest	k4xCc1	šest
mílí	míle	k1gFnSc7	míle
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Definitivní	definitivní	k2eAgFnSc4d1	definitivní
tečku	tečka	k1gFnSc4	tečka
za	za	k7c7	za
antickou	antický	k2eAgFnSc7d1	antická
slávou	sláva	k1gFnSc7	sláva
města	město	k1gNnSc2	město
znamenal	znamenat	k5eAaImAgInS	znamenat
vpád	vpád	k1gInSc4	vpád
Slovanů	Slovan	k1gMnPc2	Slovan
na	na	k7c4	na
Peloponés	Peloponés	k1gInSc4	Peloponés
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
byzantské	byzantský	k2eAgFnSc2d1	byzantská
říše	říš	k1gFnSc2	říš
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
Korint	Korint	k1gInSc1	Korint
střediskem	středisko	k1gNnSc7	středisko
thematu	thema	k1gNnSc2	thema
Hellas	Hellas	k1gFnSc1	Hellas
(	(	kIx(	(
<g/>
zahrnujícím	zahrnující	k2eAgNnSc7d1	zahrnující
velkou	velká	k1gFnSc4	velká
část	část	k1gFnSc1	část
území	území	k1gNnSc2	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
za	za	k7c4	za
panování	panování	k1gNnSc4	panování
dynastie	dynastie	k1gFnSc2	dynastie
Komnenovců	Komnenovec	k1gMnPc2	Komnenovec
<g/>
)	)	kIx)	)
upoutalo	upoutat	k5eAaPmAgNnS	upoutat
tehdejší	tehdejší	k2eAgNnSc1d1	tehdejší
bohatství	bohatství	k1gNnSc1	bohatství
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
pocházející	pocházející	k2eAgFnSc2d1	pocházející
z	z	k7c2	z
obchodu	obchod	k1gInSc2	obchod
s	s	k7c7	s
hedvábím	hedvábí	k1gNnSc7	hedvábí
do	do	k7c2	do
západní	západní	k2eAgFnSc2d1	západní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
pozornost	pozornost	k1gFnSc4	pozornost
normanského	normanský	k2eAgMnSc2d1	normanský
krále	král	k1gMnSc2	král
Rogera	Rogero	k1gNnSc2	Rogero
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Sicilského	sicilský	k2eAgMnSc4d1	sicilský
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
si	se	k3xPyFc3	se
město	město	k1gNnSc4	město
podmanil	podmanit	k5eAaPmAgMnS	podmanit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1147	[number]	k4	1147
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
dobytí	dobytí	k1gNnSc6	dobytí
Konstantinopole	Konstantinopol	k1gInSc2	Konstantinopol
během	během	k7c2	během
čtvrté	čtvrtá	k1gFnSc2	čtvrtá
křížové	křížový	k2eAgFnSc2d1	křížová
výpravy	výprava	k1gFnSc2	výprava
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1204	[number]	k4	1204
byl	být	k5eAaImAgMnS	být
Geoffrey	Geoffrey	k1gInPc4	Geoffrey
I.	I.	kA	I.
de	de	k?	de
Villehardouin	Villehardouin	k1gMnSc1	Villehardouin
<g/>
,	,	kIx,	,
synovec	synovec	k1gMnSc1	synovec
stejnojmenného	stejnojmenný	k2eAgMnSc2d1	stejnojmenný
slavného	slavný	k2eAgMnSc2d1	slavný
historika	historik	k1gMnSc2	historik
<g/>
,	,	kIx,	,
ustaven	ustaven	k2eAgInSc1d1	ustaven
pánem	pán	k1gMnSc7	pán
Korintu	Korint	k1gInSc2	Korint
jako	jako	k8xS	jako
vládce	vládce	k1gMnSc2	vládce
achajského	achajský	k2eAgNnSc2d1	achajské
knížectví	knížectví	k1gNnSc2	knížectví
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1205	[number]	k4	1205
až	až	k9	až
1208	[number]	k4	1208
kladli	klást	k5eAaImAgMnP	klást
Korinťané	Korinťan	k1gMnPc1	Korinťan
latinským	latinský	k2eAgMnPc3d1	latinský
dobyvatelům	dobyvatel	k1gMnPc3	dobyvatel
tuhý	tuhý	k2eAgInSc4d1	tuhý
odpor	odpor	k1gInSc4	odpor
z	z	k7c2	z
pevnosti	pevnost	k1gFnSc2	pevnost
na	na	k7c4	na
Akrokorintu	Akrokorinta	k1gFnSc4	Akrokorinta
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
byzantského	byzantský	k2eAgMnSc2d1	byzantský
generála	generál	k1gMnSc2	generál
Leona	Leo	k1gMnSc2	Leo
Sgura	Sgur	k1gMnSc2	Sgur
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
sice	sice	k8xC	sice
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1208	[number]	k4	1208
spáchal	spáchat	k5eAaPmAgInS	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
skokem	skokem	k6eAd1	skokem
z	z	k7c2	z
vrcholu	vrchol	k1gInSc2	vrchol
Akrokorintu	Akrokorint	k1gInSc2	Akrokorint
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
Byzantinci	Byzantinec	k1gMnPc1	Byzantinec
vytrvali	vytrvat	k5eAaPmAgMnP	vytrvat
v	v	k7c6	v
odboji	odboj	k1gInSc6	odboj
proti	proti	k7c3	proti
cizím	cizí	k2eAgMnPc3d1	cizí
útočníkům	útočník	k1gMnPc3	útočník
ještě	ještě	k6eAd1	ještě
další	další	k2eAgInPc4d1	další
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1458	[number]	k4	1458
<g/>
,	,	kIx,	,
pět	pět	k4xCc1	pět
let	léto	k1gNnPc2	léto
po	po	k7c6	po
pádu	pád	k1gInSc6	pád
Konstantinopole	Konstantinopol	k1gInSc2	Konstantinopol
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
osmanští	osmanský	k2eAgMnPc1d1	osmanský
Turci	Turek	k1gMnPc1	Turek
zmocnili	zmocnit	k5eAaPmAgMnP	zmocnit
města	město	k1gNnSc2	město
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc2	jeho
mocného	mocný	k2eAgInSc2d1	mocný
hradu	hrad	k1gInSc2	hrad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
řecké	řecký	k2eAgFnSc2d1	řecká
války	válka	k1gFnSc2	válka
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1821	[number]	k4	1821
až	až	k9	až
1830	[number]	k4	1830
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
kompletně	kompletně	k6eAd1	kompletně
zničeno	zničit	k5eAaPmNgNnS	zničit
tureckými	turecký	k2eAgInPc7d1	turecký
vojsky	vojsky	k6eAd1	vojsky
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
nezávislosti	nezávislost	k1gFnSc2	nezávislost
Řecka	Řecko	k1gNnSc2	Řecko
patřil	patřit	k5eAaImAgInS	patřit
Korint	Korint	k1gInSc1	Korint
díky	díky	k7c3	díky
svému	svůj	k3xOyFgInSc3	svůj
historickému	historický	k2eAgInSc3d1	historický
významu	význam	k1gInSc3	význam
a	a	k8xC	a
strategické	strategický	k2eAgFnSc6d1	strategická
pozici	pozice	k1gFnSc6	pozice
mezi	mezi	k7c7	mezi
kandidáty	kandidát	k1gMnPc7	kandidát
nového	nový	k2eAgNnSc2d1	nové
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
právě	právě	k6eAd1	právě
založeného	založený	k2eAgNnSc2d1	založené
řeckého	řecký	k2eAgNnSc2d1	řecké
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
Korintu	Korint	k1gInSc2	Korint
však	však	k9	však
byly	být	k5eAaImAgInP	být
nakonec	nakonec	k6eAd1	nakonec
vybrány	vybrán	k2eAgFnPc1d1	vybrána
Athény	Athéna	k1gFnPc1	Athéna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1858	[number]	k4	1858
byl	být	k5eAaImAgInS	být
starý	starý	k2eAgInSc1d1	starý
Korint	Korint	k1gInSc1	Korint
zcela	zcela	k6eAd1	zcela
zničen	zničit	k5eAaPmNgInS	zničit
zemětřesením	zemětřesení	k1gNnSc7	zemětřesení
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgNnSc1d1	Nové
město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
vystavěno	vystavět	k5eAaPmNgNnS	vystavět
o	o	k7c4	o
šest	šest	k4xCc4	šest
kilometrů	kilometr	k1gInPc2	kilometr
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
severovýchod	severovýchod	k1gInSc4	severovýchod
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
Korintského	korintský	k2eAgInSc2d1	korintský
zálivu	záliv	k1gInSc2	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
konečně	konečně	k6eAd1	konečně
realizován	realizovat	k5eAaBmNgInS	realizovat
plán	plán	k1gInSc1	plán
přetnutí	přetnutí	k1gNnSc2	přetnutí
Korintské	korintský	k2eAgFnSc2d1	Korintská
šíje	šíj	k1gFnSc2	šíj
kanálem	kanál	k1gInSc7	kanál
propojujícím	propojující	k2eAgInSc7d1	propojující
Korintský	korintský	k2eAgInSc1d1	korintský
a	a	k8xC	a
Sarónský	Sarónský	k2eAgInSc1d1	Sarónský
záliv	záliv	k1gInSc1	záliv
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
OLIVA	Oliva	k1gMnSc1	Oliva
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
<g/>
,	,	kIx,	,
Kolébka	kolébka	k1gFnSc1	kolébka
demokracie	demokracie	k1gFnSc1	demokracie
<g/>
:	:	kIx,	:
dějiny	dějiny	k1gFnPc1	dějiny
a	a	k8xC	a
kultura	kultura	k1gFnSc1	kultura
klasického	klasický	k2eAgNnSc2d1	klasické
Řecka	Řecko	k1gNnSc2	Řecko
5	[number]	k4	5
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př.n.l.	př.n.l.	k?	př.n.l.
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Arista	Arista	k1gFnSc1	Arista
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-86410-04-8	[number]	k4	80-86410-04-8
</s>
</p>
<p>
<s>
ZAMAROVSKÝ	ZAMAROVSKÝ	kA	ZAMAROVSKÝ
<g/>
,	,	kIx,	,
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
<g/>
,	,	kIx,	,
Řecký	řecký	k2eAgInSc1d1	řecký
zázrak	zázrak	k1gInSc1	zázrak
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Euromedia	Euromedium	k1gNnSc2	Euromedium
Group	Group	k1gInSc1	Group
–	–	k?	–
Knižní	knižní	k2eAgInSc1d1	knižní
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
Erika	Erika	k1gFnSc1	Erika
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-242-0403-7	[number]	k4	80-242-0403-7
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Starověké	starověký	k2eAgNnSc1d1	starověké
Řecko	Řecko	k1gNnSc1	Řecko
</s>
</p>
<p>
<s>
Achajský	Achajský	k2eAgInSc1d1	Achajský
spolek	spolek	k1gInSc1	spolek
</s>
</p>
<p>
<s>
Korintský	korintský	k2eAgInSc1d1	korintský
spolek	spolek	k1gInSc1	spolek
</s>
</p>
<p>
<s>
Korintská	korintský	k2eAgFnSc1d1	Korintská
šíje	šíje	k1gFnSc1	šíje
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Korint	Korinta	k1gFnPc2	Korinta
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
