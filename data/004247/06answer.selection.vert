<s>
V	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
mnoho	mnoho	k4c1	mnoho
parků	park	k1gInPc2	park
různých	různý	k2eAgInPc2d1	různý
velikostí	velikost	k1gFnSc7	velikost
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
nejbohatší	bohatý	k2eAgNnSc4d3	nejbohatší
svým	svůj	k3xOyFgNnSc7	svůj
vybavením	vybavení	k1gNnSc7	vybavení
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
největší	veliký	k2eAgInSc1d3	veliký
patří	patřit	k5eAaImIp3nS	patřit
park	park	k1gInSc1	park
Lužánky	Lužánka	k1gFnSc2	Lužánka
<g/>
,	,	kIx,	,
otevřený	otevřený	k2eAgInSc1d1	otevřený
veřejnosti	veřejnost	k1gFnSc3	veřejnost
roku	rok	k1gInSc2	rok
1786	[number]	k4	1786
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
první	první	k4xOgInSc1	první
veřejný	veřejný	k2eAgInSc1d1	veřejný
park	park	k1gInSc1	park
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
a	a	k8xC	a
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
kopcový	kopcový	k2eAgInSc1d1	kopcový
park	park	k1gInSc1	park
Špilberk	Špilberk	k1gInSc1	Špilberk
pod	pod	k7c7	pod
stejnojmenným	stejnojmenný	k2eAgInSc7d1	stejnojmenný
hradem	hrad	k1gInSc7	hrad
či	či	k8xC	či
rozsáhlý	rozsáhlý	k2eAgInSc4d1	rozsáhlý
lesopark	lesopark	k1gInSc4	lesopark
Wilsonův	Wilsonův	k2eAgInSc4d1	Wilsonův
les	les	k1gInSc4	les
<g/>
.	.	kIx.	.
</s>
