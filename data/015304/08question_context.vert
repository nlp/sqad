<s>
Siderit	siderit	k1gInSc1
je	být	k5eAaImIp3nS
druh	druh	k1gInSc4
meteoritu	meteorit	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
složen	složit	k5eAaPmNgInS
především	především	k6eAd1
ze	z	k7c2
slitiny	slitina	k1gFnSc2
železa	železo	k1gNnSc2
a	a	k8xC
niklu	nikl	k1gInSc2
s	s	k7c7
menším	malý	k2eAgNnSc7d2
množstvím	množství	k1gNnSc7
minerálů	minerál	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Někdy	někdy	k6eAd1
se	se	k3xPyFc4
označuje	označovat	k5eAaImIp3nS
také	také	k9
jako	jako	k9
meteorické	meteorický	k2eAgNnSc4d1
železo	železo	k1gNnSc4
<g/>
.	.	kIx.
</s>