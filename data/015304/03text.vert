<s>
Siderit	siderit	k1gInSc1
(	(	kIx(
<g/>
meteorit	meteorit	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Železný	železný	k2eAgInSc1d1
meteorit	meteorit	k1gInSc1
z	z	k7c2
lokality	lokalita	k1gFnSc2
Campo	Campa	k1gFnSc5
del	del	k?
Cielo	Cielo	k1gNnSc1
v	v	k7c6
Argentině	Argentina	k1gFnSc6
</s>
<s>
Siderit	siderit	k1gInSc1
je	být	k5eAaImIp3nS
druh	druh	k1gInSc4
meteoritu	meteorit	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
složen	složit	k5eAaPmNgInS
především	především	k6eAd1
ze	z	k7c2
slitiny	slitina	k1gFnSc2
železa	železo	k1gNnSc2
a	a	k8xC
niklu	nikl	k1gInSc2
s	s	k7c7
menším	malý	k2eAgNnSc7d2
množstvím	množství	k1gNnSc7
minerálů	minerál	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Někdy	někdy	k6eAd1
se	se	k3xPyFc4
označuje	označovat	k5eAaImIp3nS
také	také	k9
jako	jako	k9
meteorické	meteorický	k2eAgNnSc4d1
železo	železo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tvoří	tvořit	k5eAaImIp3nS
asi	asi	k9
18	#num#	k4
%	%	kIx~
všech	všecek	k3xTgInPc2
nalezených	nalezený	k2eAgInPc2d1
meteorů	meteor	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
častějších	častý	k2eAgInPc2d2
typů	typ	k1gInPc2
meteoritů	meteorit	k1gInPc2
–	–	k?
chondritů	chondrit	k1gInPc2
–	–	k?
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
mají	mít	k5eAaImIp3nP
podobné	podobný	k2eAgNnSc4d1
složení	složení	k1gNnSc4
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
měla	mít	k5eAaImAgFnS
sluneční	sluneční	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
několik	několik	k4yIc4
miliónů	milión	k4xCgInPc2
let	léto	k1gNnPc2
po	po	k7c6
svém	svůj	k3xOyFgInSc6
zrodu	zrod	k1gInSc6
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
prošly	projít	k5eAaPmAgInP
siderity	siderit	k1gInPc1
geologickým	geologický	k2eAgInSc7d1
horotvorným	horotvorný	k2eAgInSc7d1
procesem	proces	k1gInSc7
–	–	k?
pocházejí	pocházet	k5eAaImIp3nP
pravděpodobně	pravděpodobně	k6eAd1
z	z	k7c2
jader	jádro	k1gNnPc2
rozbitých	rozbitý	k2eAgFnPc2d1
planetek	planetka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Největší	veliký	k2eAgInSc1d3
nalezený	nalezený	k2eAgInSc1d1
siderit	siderit	k1gInSc1
váží	vážit	k5eAaImIp3nS
šedesát	šedesát	k4xCc4
tun	tuna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Leží	ležet	k5eAaImIp3nS
v	v	k7c6
Namibii	Namibie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
stupně	stupeň	k1gInSc2
koroze	koroze	k1gFnSc2
se	se	k3xPyFc4
odhaduje	odhadovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
původně	původně	k6eAd1
měl	mít	k5eAaImAgInS
asi	asi	k9
100	#num#	k4
tun	tuna	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Poznámka	poznámka	k1gFnSc1
</s>
<s>
Meteority	meteorit	k1gInPc1
siderity	siderit	k1gInPc7
se	s	k7c7
svým	svůj	k3xOyFgNnSc7
složením	složení	k1gNnSc7
a	a	k8xC
strukturou	struktura	k1gFnSc7
nepodobají	podobat	k5eNaImIp3nP
minerálu	minerál	k1gInSc2
siderit	siderit	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Společné	společný	k2eAgInPc1d1
s	s	k7c7
ním	on	k3xPp3gMnSc7
mají	mít	k5eAaImIp3nP
jen	jen	k9
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
obsahují	obsahovat	k5eAaImIp3nP
železo	železo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
</s>
<s>
Astronomia	Astronomia	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pedagogická	pedagogický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
Západočeské	západočeský	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
<g/>
,	,	kIx,
2007	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Meteoroidy	Meteoroida	k1gFnSc2
a	a	k8xC
meteority	meteorit	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Astronomický	astronomický	k2eAgInSc1d1
slovníček	slovníček	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
astronomická	astronomický	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
chondrit	chondrita	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
DUŠEK	Dušek	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Návod	návod	k1gInSc1
na	na	k7c4
použití	použití	k1gNnSc4
vesmíru	vesmír	k1gInSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hvězdárna	hvězdárna	k1gFnSc1
a	a	k8xC
planetárium	planetárium	k1gNnSc1
Mikuláše	Mikuláš	k1gMnSc2
Koperníka	Koperník	k1gMnSc2
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
,	,	kIx,
2004	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Jak	jak	k6eAd1
se	se	k3xPyFc4
pozná	poznat	k5eAaPmIp3nS
meteorit	meteorit	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
MATYÁŠEK	MATYÁŠEK	kA
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
;	;	kIx,
SUK	Suk	k1gMnSc1
<g/>
,	,	kIx,
Miloš	Miloš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přehled	přehled	k1gInSc4
minerálů	minerál	k1gInPc2
a	a	k8xC
hornin	hornina	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pedagogická	pedagogický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
<g/>
,	,	kIx,
Masarykova	Masarykův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
<g/>
,	,	kIx,
2007	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Meteority	meteorit	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85084318	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85084318	#num#	k4
</s>
