<p>
<s>
Francis	Francis	k1gFnSc1	Francis
Seymour-Conway	Seymour-Conwaa	k1gFnSc2	Seymour-Conwaa
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
markýz	markýza	k1gFnPc2	markýza
z	z	k7c2	z
Hertfordu	Hertford	k1gInSc2	Hertford
(	(	kIx(	(
<g/>
Francis	Francis	k1gFnSc1	Francis
Seymour-Conway	Seymour-Conwaa	k1gFnSc2	Seymour-Conwaa
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
st	st	kA	st
Marquess	Marquessa	k1gFnPc2	Marquessa
of	of	k?	of
Hertford	Hertforda	k1gFnPc2	Hertforda
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
st	st	kA	st
Earl	earl	k1gMnSc1	earl
of	of	k?	of
Hertford	Hertford	k1gMnSc1	Hertford
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
st	st	kA	st
Earl	earl	k1gMnSc1	earl
of	of	k?	of
Yarmouth	Yarmouth	k1gMnSc1	Yarmouth
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
st	st	kA	st
Viscount	Viscount	k1gMnSc1	Viscount
Beauchamp	Beauchamp	k1gMnSc1	Beauchamp
of	of	k?	of
Hache	Hache	k1gInSc1	Hache
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
nd	nd	k?	nd
Baron	baron	k1gMnSc1	baron
Conway	Conwaa	k1gFnSc2	Conwaa
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1718	[number]	k4	1718
<g/>
,	,	kIx,	,
Londýn	Londýn	k1gInSc1	Londýn
<g/>
,	,	kIx,	,
Anglie	Anglie	k1gFnSc1	Anglie
–	–	k?	–
14	[number]	k4	14
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1794	[number]	k4	1794
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
britský	britský	k2eAgMnSc1d1	britský
diplomat	diplomat	k1gMnSc1	diplomat
<g/>
,	,	kIx,	,
dvořan	dvořan	k1gMnSc1	dvořan
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
ze	z	k7c2	z
šlechtického	šlechtický	k2eAgInSc2d1	šlechtický
rodu	rod	k1gInSc2	rod
Seymourů	Seymour	k1gInPc2	Seymour
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
kariéry	kariéra	k1gFnSc2	kariéra
byl	být	k5eAaImAgInS	být
vyslancem	vyslanec	k1gMnSc7	vyslanec
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
místokrálem	místokrál	k1gMnSc7	místokrál
v	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
a	a	k8xC	a
dlouholetým	dlouholetý	k2eAgMnSc7d1	dlouholetý
nejvyšším	vysoký	k2eAgMnSc7d3	nejvyšší
komořím	komoří	k1gMnSc7	komoří
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1756	[number]	k4	1756
obdržel	obdržet	k5eAaPmAgInS	obdržet
Podvazkový	podvazkový	k2eAgInSc1d1	podvazkový
řád	řád	k1gInSc1	řád
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1793	[number]	k4	1793
získal	získat	k5eAaPmAgMnS	získat
titul	titul	k1gInSc4	titul
markýze	markýz	k1gMnSc2	markýz
z	z	k7c2	z
Hertfordu	Hertford	k1gInSc2	Hertford
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
synové	syn	k1gMnPc1	syn
se	se	k3xPyFc4	se
uplatnili	uplatnit	k5eAaPmAgMnP	uplatnit
u	u	k7c2	u
dvora	dvůr	k1gInSc2	dvůr
<g/>
,	,	kIx,	,
v	v	k7c6	v
diplomacii	diplomacie	k1gFnSc6	diplomacie
<g/>
,	,	kIx,	,
armádě	armáda	k1gFnSc6	armáda
a	a	k8xC	a
námořnictvu	námořnictvo	k1gNnSc6	námořnictvo
<g/>
,	,	kIx,	,
zasedali	zasedat	k5eAaImAgMnP	zasedat
také	také	k9	také
v	v	k7c6	v
Dolní	dolní	k2eAgFnSc6d1	dolní
sněmovně	sněmovna	k1gFnSc6	sněmovna
<g/>
.	.	kIx.	.
</s>
<s>
Vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
rekord	rekord	k1gInSc4	rekord
v	v	k7c6	v
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
historii	historie	k1gFnSc6	historie
britského	britský	k2eAgInSc2d1	britský
parlamentu	parlament	k1gInSc2	parlament
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
žádné	žádný	k3yNgFnSc3	žádný
jiné	jiný	k2eAgFnSc3d1	jiná
rodině	rodina	k1gFnSc3	rodina
se	se	k3xPyFc4	se
nepodařilo	podařit	k5eNaPmAgNnS	podařit
<g/>
,	,	kIx,	,
aby	aby	k9	aby
šest	šest	k4xCc1	šest
sourozenců	sourozenec	k1gMnPc2	sourozenec
zasedalo	zasedat	k5eAaImAgNnS	zasedat
v	v	k7c6	v
Dolní	dolní	k2eAgFnSc6d1	dolní
sněmovně	sněmovna	k1gFnSc6	sněmovna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
Pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
vedlejší	vedlejší	k2eAgFnSc2d1	vedlejší
linie	linie	k1gFnSc2	linie
vévodů	vévoda	k1gMnPc2	vévoda
ze	z	k7c2	z
Somersetu	Somerset	k1gInSc2	Somerset
<g/>
,	,	kIx,	,
narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
jako	jako	k8xC	jako
syn	syn	k1gMnSc1	syn
Francise	Francise	k1gFnSc2	Francise
Seymoura	Seymoura	k1gFnSc1	Seymoura
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
barona	baron	k1gMnSc4	baron
Conwaye	Conway	k1gMnSc4	Conway
(	(	kIx(	(
<g/>
1679	[number]	k4	1679
<g/>
-	-	kIx~	-
<g/>
1731	[number]	k4	1731
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1699	[number]	k4	1699
přijal	přijmout	k5eAaPmAgInS	přijmout
jméno	jméno	k1gNnSc4	jméno
Seymour-Conway	Seymour-Conwaa	k1gFnSc2	Seymour-Conwaa
<g/>
,	,	kIx,	,
po	po	k7c6	po
matce	matka	k1gFnSc6	matka
Charlotte	Charlott	k1gInSc5	Charlott
Shorter	Shorter	k1gMnSc1	Shorter
byl	být	k5eAaImAgMnS	být
synovcem	synovec	k1gMnSc7	synovec
dlouholetého	dlouholetý	k2eAgMnSc2d1	dlouholetý
premiéra	premiér	k1gMnSc2	premiér
Roberta	Robert	k1gMnSc2	Robert
Walpola	Walpola	k1gFnSc1	Walpola
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
otci	otec	k1gMnSc6	otec
zdědil	zdědit	k5eAaPmAgInS	zdědit
titul	titul	k1gInSc1	titul
barona	baron	k1gMnSc2	baron
(	(	kIx(	(
<g/>
1731	[number]	k4	1731
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
kavalírskou	kavalírský	k2eAgFnSc4d1	kavalírská
cestu	cesta	k1gFnSc4	cesta
po	po	k7c6	po
Francii	Francie	k1gFnSc6	Francie
a	a	k8xC	a
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
do	do	k7c2	do
Sněmovny	sněmovna	k1gFnSc2	sněmovna
lordů	lord	k1gMnPc2	lord
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
po	po	k7c6	po
dosažení	dosažení	k1gNnSc6	dosažení
zletilosti	zletilost	k1gFnSc2	zletilost
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1739	[number]	k4	1739
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1750	[number]	k4	1750
byl	být	k5eAaImAgMnS	být
povýšen	povýšit	k5eAaPmNgMnS	povýšit
na	na	k7c4	na
hraběte	hrabě	k1gMnSc4	hrabě
z	z	k7c2	z
Hertfordu	Hertford	k1gInSc2	Hertford
(	(	kIx(	(
<g/>
tento	tento	k3xDgInSc1	tento
titul	titul	k1gInSc1	titul
užíval	užívat	k5eAaImAgInS	užívat
již	již	k6eAd1	již
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Edward	Edward	k1gMnSc1	Edward
Seymour	Seymour	k1gMnSc1	Seymour
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vévoda	vévoda	k1gMnSc1	vévoda
ze	z	k7c2	z
Somersetu	Somerset	k1gInSc2	Somerset
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1751-1766	[number]	k4	1751-1766
byl	být	k5eAaImAgInS	být
lordem	lord	k1gMnSc7	lord
komořím	komoří	k1gMnPc3	komoří
Jiřího	Jiří	k1gMnSc2	Jiří
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
Jiřího	Jiří	k1gMnSc4	Jiří
III	III	kA	III
<g/>
..	..	k?	..
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1755	[number]	k4	1755
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
stát	stát	k5eAaImF	stát
vyslancem	vyslanec	k1gMnSc7	vyslanec
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kvůli	kvůli	k7c3	kvůli
neznalosti	neznalost	k1gFnSc3	neznalost
francouzštiny	francouzština	k1gFnSc2	francouzština
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
jmenování	jmenování	k1gNnSc2	jmenování
sešlo	sejít	k5eAaPmAgNnS	sejít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1756	[number]	k4	1756
získal	získat	k5eAaPmAgInS	získat
Podvazkový	podvazkový	k2eAgInSc1d1	podvazkový
řád	řád	k1gInSc1	řád
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
vyslancem	vyslanec	k1gMnSc7	vyslanec
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
stal	stát	k5eAaPmAgMnS	stát
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
sedmileté	sedmiletý	k2eAgFnSc2d1	sedmiletá
války	válka	k1gFnSc2	válka
(	(	kIx(	(
<g/>
1763	[number]	k4	1763
<g/>
-	-	kIx~	-
<g/>
1765	[number]	k4	1765
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1763	[number]	k4	1763
byl	být	k5eAaImAgInS	být
zároveň	zároveň	k6eAd1	zároveň
členem	člen	k1gMnSc7	člen
Tajné	tajný	k2eAgFnSc2d1	tajná
rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1765-1766	[number]	k4	1765-1766
byl	být	k5eAaImAgInS	být
místokrálem	místokrál	k1gMnSc7	místokrál
v	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vlastnil	vlastnit	k5eAaImAgMnS	vlastnit
statky	statek	k1gInPc7	statek
v	v	k7c6	v
Ulsteru	Ulster	k1gInSc6	Ulster
jako	jako	k8xS	jako
dědictví	dědictví	k1gNnSc1	dědictví
po	po	k7c6	po
rodu	rod	k1gInSc6	rod
Conway	Conwaa	k1gFnSc2	Conwaa
a	a	k8xC	a
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1750	[number]	k4	1750
byl	být	k5eAaImAgMnS	být
členem	člen	k1gMnSc7	člen
irské	irský	k2eAgFnSc2d1	irská
Tajné	tajný	k2eAgFnSc2d1	tajná
rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
z	z	k7c2	z
Irska	Irsko	k1gNnSc2	Irsko
zastával	zastávat	k5eAaImAgMnS	zastávat
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
nejvyšší	vysoký	k2eAgInPc4d3	Nejvyšší
posty	post	k1gInPc4	post
u	u	k7c2	u
dvora	dvůr	k1gInSc2	dvůr
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
nejvyšším	vysoký	k2eAgMnSc7d3	nejvyšší
štolbou	štolba	k1gMnSc7	štolba
(	(	kIx(	(
<g/>
1766	[number]	k4	1766
<g/>
)	)	kIx)	)
a	a	k8xC	a
nejvyšším	vysoký	k2eAgMnPc3d3	nejvyšší
komořím	komoří	k1gMnPc3	komoří
(	(	kIx(	(
<g/>
1766	[number]	k4	1766
<g/>
-	-	kIx~	-
<g/>
1782	[number]	k4	1782
a	a	k8xC	a
1783	[number]	k4	1783
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
Pittovy	Pittův	k2eAgFnSc2d1	Pittova
vlády	vláda	k1gFnSc2	vláda
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
ústraní	ústraní	k1gNnSc2	ústraní
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1793	[number]	k4	1793
získal	získat	k5eAaPmAgMnS	získat
titul	titul	k1gInSc4	titul
markýze	markýz	k1gMnSc2	markýz
z	z	k7c2	z
Hertfordu	Hertford	k1gInSc2	Hertford
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rodina	rodina	k1gFnSc1	rodina
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1741	[number]	k4	1741
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Isabelou	Isabela	k1gFnSc7	Isabela
Fitzroy	Fitzroa	k1gFnSc2	Fitzroa
(	(	kIx(	(
<g/>
1726	[number]	k4	1726
<g/>
-	-	kIx~	-
<g/>
1782	[number]	k4	1782
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dcerou	dcera	k1gFnSc7	dcera
2	[number]	k4	2
<g/>
.	.	kIx.	.
vévody	vévoda	k1gMnPc4	vévoda
z	z	k7c2	z
Graftonu	Grafton	k1gInSc2	Grafton
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jejich	jejich	k3xOp3gNnSc2	jejich
manželství	manželství	k1gNnSc2	manželství
pocházelo	pocházet	k5eAaImAgNnS	pocházet
třináct	třináct	k4xCc1	třináct
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
sedm	sedm	k4xCc4	sedm
synů	syn	k1gMnPc2	syn
a	a	k8xC	a
šest	šest	k4xCc4	šest
dcer	dcera	k1gFnPc2	dcera
<g/>
.	.	kIx.	.
</s>
<s>
Dcery	dcera	k1gFnPc1	dcera
Anne	Anne	k1gFnPc2	Anne
(	(	kIx(	(
<g/>
1744	[number]	k4	1744
<g/>
-	-	kIx~	-
<g/>
1784	[number]	k4	1784
<g/>
)	)	kIx)	)
a	a	k8xC	a
Sarah	Sarah	k1gFnSc1	Sarah
(	(	kIx(	(
<g/>
1747	[number]	k4	1747
<g/>
-	-	kIx~	-
<g/>
1770	[number]	k4	1770
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
provdaly	provdat	k5eAaPmAgFnP	provdat
do	do	k7c2	do
přední	přední	k2eAgFnSc2d1	přední
irské	irský	k2eAgFnSc2d1	irská
šlechty	šlechta	k1gFnSc2	šlechta
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
markýz	markýza	k1gFnPc2	markýza
z	z	k7c2	z
Droghedy	Drogheda	k1gMnSc2	Drogheda
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
markýz	markýza	k1gFnPc2	markýza
z	z	k7c2	z
Londonderry	Londonderra	k1gFnSc2	Londonderra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
další	další	k2eAgFnSc1d1	další
dcera	dcera	k1gFnSc1	dcera
Frances	Frances	k1gMnSc1	Frances
(	(	kIx(	(
<g/>
1751	[number]	k4	1751
<g/>
-	-	kIx~	-
<g/>
1820	[number]	k4	1820
<g/>
)	)	kIx)	)
měla	mít	k5eAaImAgFnS	mít
za	za	k7c4	za
manžela	manžel	k1gMnSc4	manžel
předčasně	předčasně	k6eAd1	předčasně
zemřelého	zemřelý	k2eAgMnSc4d1	zemřelý
dědice	dědic	k1gMnSc4	dědic
vévody	vévoda	k1gMnSc2	vévoda
z	z	k7c2	z
Newcastle	Newcastle	k1gMnSc2	Newcastle
<g/>
,	,	kIx,	,
Henryho	Henry	k1gMnSc2	Henry
Pelham-Clintona	Pelham-Clinton	k1gMnSc4	Pelham-Clinton
<g/>
,	,	kIx,	,
hraběte	hrabě	k1gMnSc4	hrabě
z	z	k7c2	z
Lincolnu	Lincoln	k1gMnSc6	Lincoln
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
synů	syn	k1gMnPc2	syn
jediný	jediný	k2eAgMnSc1d1	jediný
Edward	Edward	k1gMnSc1	Edward
(	(	kIx(	(
<g/>
1752	[number]	k4	1752
<g/>
-	-	kIx~	-
<g/>
1785	[number]	k4	1785
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
duchovním	duchovnět	k5eAaImIp1nS	duchovnět
<g/>
,	,	kIx,	,
dalších	další	k2eAgMnPc2d1	další
šest	šest	k4xCc1	šest
synů	syn	k1gMnPc2	syn
našlo	najít	k5eAaPmAgNnS	najít
uplatnění	uplatnění	k1gNnSc4	uplatnění
u	u	k7c2	u
dvora	dvůr	k1gInSc2	dvůr
<g/>
,	,	kIx,	,
v	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
a	a	k8xC	a
námořnictvu	námořnictvo	k1gNnSc6	námořnictvo
<g/>
,	,	kIx,	,
všichni	všechen	k3xTgMnPc1	všechen
také	také	k9	také
zasedali	zasedat	k5eAaImAgMnP	zasedat
v	v	k7c6	v
Dolní	dolní	k2eAgFnSc6d1	dolní
sněmovně	sněmovna	k1gFnSc6	sněmovna
(	(	kIx(	(
<g/>
tři	tři	k4xCgMnPc1	tři
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
byli	být	k5eAaImAgMnP	být
i	i	k9	i
poslanci	poslanec	k1gMnPc1	poslanec
irského	irský	k2eAgInSc2d1	irský
parlamentu	parlament	k1gInSc2	parlament
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Francis	Francis	k1gFnSc1	Francis
Seymour-Conway	Seymour-Conwaa	k1gFnSc2	Seymour-Conwaa
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
markýz	markýza	k1gFnPc2	markýza
z	z	k7c2	z
Hertfordu	Hertford	k1gInSc2	Hertford
(	(	kIx(	(
<g/>
1743	[number]	k4	1743
<g/>
-	-	kIx~	-
<g/>
1822	[number]	k4	1822
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dvořan	dvořan	k1gMnSc1	dvořan
a	a	k8xC	a
diplomat	diplomat	k1gMnSc1	diplomat
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
Dolní	dolní	k2eAgFnSc2d1	dolní
sněmovny	sněmovna	k1gFnSc2	sněmovna
1766	[number]	k4	1766
<g/>
-	-	kIx~	-
<g/>
1794	[number]	k4	1794
<g/>
Lord	lord	k1gMnSc1	lord
Henry	Henry	k1gMnSc1	Henry
Seymour-Conway	Seymour-Conwaa	k1gFnSc2	Seymour-Conwaa
(	(	kIx(	(
<g/>
1746	[number]	k4	1746
<g/>
-	-	kIx~	-
<g/>
1830	[number]	k4	1830
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dvořan	dvořan	k1gMnSc1	dvořan
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
Dolní	dolní	k2eAgFnSc2d1	dolní
sněmovny	sněmovna	k1gFnSc2	sněmovna
1766	[number]	k4	1766
<g/>
-	-	kIx~	-
<g/>
1784	[number]	k4	1784
<g/>
Lord	lord	k1gMnSc1	lord
Robert	Robert	k1gMnSc1	Robert
Seymour-Conway	Seymour-Conwaa	k1gFnSc2	Seymour-Conwaa
(	(	kIx(	(
<g/>
1748	[number]	k4	1748
<g/>
-	-	kIx~	-
<g/>
1831	[number]	k4	1831
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
důstojník	důstojník	k1gMnSc1	důstojník
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
Dolní	dolní	k2eAgFnSc2d1	dolní
sněmovny	sněmovna	k1gFnSc2	sněmovna
1771-1790	[number]	k4	1771-1790
a	a	k8xC	a
1794	[number]	k4	1794
<g/>
-	-	kIx~	-
<g/>
1820	[number]	k4	1820
<g/>
Lord	lord	k1gMnSc1	lord
William	William	k1gInSc4	William
Seymour-Conway	Seymour-Conwaa	k1gFnSc2	Seymour-Conwaa
(	(	kIx(	(
<g/>
1759	[number]	k4	1759
<g/>
-	-	kIx~	-
<g/>
1837	[number]	k4	1837
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
Dolní	dolní	k2eAgFnSc2d1	dolní
sněmovny	sněmovna	k1gFnSc2	sněmovna
1783	[number]	k4	1783
<g/>
-	-	kIx~	-
<g/>
1796	[number]	k4	1796
<g/>
Lord	lord	k1gMnSc1	lord
Hugh	Hugh	k1gMnSc1	Hugh
Seymour-Conway	Seymour-Conwaa	k1gFnSc2	Seymour-Conwaa
(	(	kIx(	(
<g/>
1759	[number]	k4	1759
<g/>
-	-	kIx~	-
<g/>
1801	[number]	k4	1801
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
admirál	admirál	k1gMnSc1	admirál
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
Dolní	dolní	k2eAgFnSc2d1	dolní
sněmovny	sněmovna	k1gFnSc2	sněmovna
1796	[number]	k4	1796
<g/>
-	-	kIx~	-
<g/>
1801	[number]	k4	1801
<g/>
Lord	lord	k1gMnSc1	lord
George	George	k1gFnSc4	George
Seymour-Conway	Seymour-Conwaa	k1gFnSc2	Seymour-Conwaa
(	(	kIx(	(
<g/>
1763	[number]	k4	1763
<g/>
-	-	kIx~	-
<g/>
1848	[number]	k4	1848
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
důstojník	důstojník	k1gMnSc1	důstojník
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
Dolní	dolní	k2eAgFnSc2d1	dolní
sněmovny	sněmovna	k1gFnSc2	sněmovna
1784-1790	[number]	k4	1784-1790
a	a	k8xC	a
1796-1801	[number]	k4	1796-1801
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Rodokmen	rodokmen	k1gInSc1	rodokmen
markýzů	markýz	k1gMnPc2	markýz
z	z	k7c2	z
Hertfordu	Hertford	k1gInSc2	Hertford
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
markýz	markýza	k1gFnPc2	markýza
z	z	k7c2	z
Hertfordu	Hertford	k1gInSc2	Hertford
na	na	k7c6	na
webu	web	k1gInSc6	web
thepeerage	thepeerag	k1gFnSc2	thepeerag
</s>
</p>
