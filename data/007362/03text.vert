<s>
Slovanské	slovanský	k2eAgInPc1d1	slovanský
jazyky	jazyk	k1gInPc1	jazyk
představují	představovat	k5eAaImIp3nP	představovat
jednu	jeden	k4xCgFnSc4	jeden
ze	z	k7c2	z
skupin	skupina	k1gFnPc2	skupina
indoevropských	indoevropský	k2eAgInPc2d1	indoevropský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Studiem	studio	k1gNnSc7	studio
těchto	tento	k3xDgInPc2	tento
jazyků	jazyk	k1gInPc2	jazyk
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
slavistika	slavistika	k1gFnSc1	slavistika
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
slovanské	slovanský	k2eAgInPc1d1	slovanský
jazyky	jazyk	k1gInPc1	jazyk
začínaly	začínat	k5eAaImAgInP	začínat
jako	jako	k9	jako
indoevropský	indoevropský	k2eAgInSc4d1	indoevropský
prajazyk	prajazyk	k1gInSc4	prajazyk
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yRgInSc2	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
zřejmě	zřejmě	k6eAd1	zřejmě
oddělil	oddělit	k5eAaPmAgMnS	oddělit
baltsko-slovanský	baltskolovanský	k2eAgInSc4d1	baltsko-slovanský
prajazyk	prajazyk	k1gInSc4	prajazyk
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
rozdělil	rozdělit	k5eAaPmAgInS	rozdělit
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
jazyky	jazyk	k1gInPc4	jazyk
<g/>
:	:	kIx,	:
prabaltštinu	prabaltština	k1gFnSc4	prabaltština
a	a	k8xC	a
praslovanštinu	praslovanština	k1gFnSc4	praslovanština
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
východiskem	východisko	k1gNnSc7	východisko
pro	pro	k7c4	pro
všechny	všechen	k3xTgInPc4	všechen
slovanské	slovanský	k2eAgInPc4d1	slovanský
jazyky	jazyk	k1gInPc4	jazyk
<g/>
.	.	kIx.	.
západoslovanské	západoslovanský	k2eAgInPc1d1	západoslovanský
jazyky	jazyk	k1gInPc1	jazyk
česko-slovenské	českolovenský	k2eAgInPc1d1	česko-slovenský
jazyky	jazyk	k1gInPc4	jazyk
čeština	čeština	k1gFnSc1	čeština
slovenština	slovenština	k1gFnSc1	slovenština
lužickosrbské	lužickosrbský	k2eAgMnPc4d1	lužickosrbský
jazyky	jazyk	k1gMnPc4	jazyk
hornolužická	hornolužický	k2eAgFnSc1d1	Hornolužická
srbština	srbština	k1gFnSc1	srbština
dolnolužická	dolnolužický	k2eAgFnSc1d1	dolnolužická
srbština	srbština	k1gFnSc1	srbština
lechické	lechický	k2eAgFnSc2d1	lechický
jazyky	jazyk	k1gInPc4	jazyk
polština	polština	k1gFnSc1	polština
pomořanština	pomořanština	k1gFnSc1	pomořanština
kašubština	kašubština	k1gFnSc1	kašubština
severní	severní	k2eAgFnSc1d1	severní
slovinština	slovinština	k1gFnSc1	slovinština
–	–	k?	–
mrtvý	mrtvý	k2eAgInSc1d1	mrtvý
jazyk	jazyk	k1gInSc1	jazyk
polabština	polabština	k1gFnSc1	polabština
–	–	k?	–
mrtvý	mrtvý	k2eAgInSc1d1	mrtvý
jazyk	jazyk	k1gInSc1	jazyk
slezština	slezština	k1gFnSc1	slezština
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
někdy	někdy	k6eAd1	někdy
pokládána	pokládán	k2eAgFnSc1d1	pokládána
pouze	pouze	k6eAd1	pouze
za	za	k7c4	za
nářečí	nářečí	k1gNnSc4	nářečí
polštiny	polština	k1gFnSc2	polština
východoslovanské	východoslovanský	k2eAgInPc4d1	východoslovanský
jazyky	jazyk	k1gInPc4	jazyk
běloruština	běloruština	k1gFnSc1	běloruština
ukrajinština	ukrajinština	k1gFnSc1	ukrajinština
ruština	ruština	k1gFnSc1	ruština
rusínština	rusínština	k1gFnSc1	rusínština
smíšené	smíšený	k2eAgMnPc4d1	smíšený
jazyky	jazyk	k1gMnPc4	jazyk
suržyk	suržyk	k6eAd1	suržyk
(	(	kIx(	(
<g/>
směs	směs	k1gFnSc1	směs
ukrajinštiny	ukrajinština	k1gFnSc2	ukrajinština
a	a	k8xC	a
ruštiny	ruština	k1gFnSc2	ruština
<g/>
)	)	kIx)	)
trasjanka	trasjanka	k1gFnSc1	trasjanka
(	(	kIx(	(
<g/>
směs	směs	k1gFnSc1	směs
běloruštiny	běloruština	k1gFnSc2	běloruština
a	a	k8xC	a
ruštiny	ruština	k1gFnSc2	ruština
<g/>
)	)	kIx)	)
russenorsk	russenorsk	k1gInSc1	russenorsk
(	(	kIx(	(
<g/>
směs	směs	k1gFnSc1	směs
ruštiny	ruština	k1gFnSc2	ruština
a	a	k8xC	a
norštiny	norština	k1gFnSc2	norština
<g/>
)	)	kIx)	)
jihoslovanské	jihoslovanský	k2eAgInPc1d1	jihoslovanský
jazyky	jazyk	k1gInPc1	jazyk
západní	západní	k2eAgFnSc4d1	západní
větev	větev	k1gFnSc4	větev
chorvatština	chorvatština	k1gFnSc1	chorvatština
srbština	srbština	k1gFnSc1	srbština
bosenština	bosenština	k1gFnSc1	bosenština
černohorština	černohorština	k1gFnSc1	černohorština
<g />
.	.	kIx.	.
</s>
<s>
slovinština	slovinština	k1gFnSc1	slovinština
zámuřština	zámuřština	k1gFnSc1	zámuřština
východní	východní	k2eAgFnSc1d1	východní
větev	větev	k1gFnSc1	větev
makedonština	makedonština	k1gFnSc1	makedonština
bulharština	bulharština	k1gFnSc1	bulharština
staroslověnština	staroslověnština	k1gFnSc1	staroslověnština
–	–	k?	–
mrtvý	mrtvý	k2eAgInSc4d1	mrtvý
jazyk	jazyk	k1gInSc4	jazyk
církevní	církevní	k2eAgFnSc1d1	církevní
slovanština	slovanština	k1gFnSc1	slovanština
–	–	k?	–
liturgický	liturgický	k2eAgInSc4d1	liturgický
jazyk	jazyk	k1gInSc4	jazyk
pravoslavných	pravoslavný	k2eAgFnPc2d1	pravoslavná
církví	církev	k1gFnPc2	církev
slovio	slovio	k6eAd1	slovio
slovianski	slovianski	k6eAd1	slovianski
mezislovanský	mezislovanský	k2eAgMnSc1d1	mezislovanský
glagolica	glagolica	k1gMnSc1	glagolica
proslava	proslava	k1gFnSc1	proslava
ruslavsk	ruslavsk	k1gInSc1	ruslavsk
(	(	kIx(	(
<g/>
varianta	varianta	k1gFnSc1	varianta
slovia	slovium	k1gNnSc2	slovium
<g/>
)	)	kIx)	)
novoslovienskij	novoslovienskít	k5eAaPmRp2nS	novoslovienskít
jazyk	jazyk	k1gInSc4	jazyk
Existuje	existovat	k5eAaImIp3nS	existovat
domnělá	domnělý	k2eAgFnSc1d1	domnělá
větev	větev	k1gFnSc1	větev
tzv.	tzv.	kA	tzv.
severoslovanských	severoslovanský	k2eAgInPc2d1	severoslovanský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
usuzuje	usuzovat	k5eAaImIp3nS	usuzovat
kvůli	kvůli	k7c3	kvůli
nalezení	nalezení	k1gNnSc3	nalezení
záznamů	záznam	k1gInPc2	záznam
staronovgorodštiny	staronovgorodština	k1gFnSc2	staronovgorodština
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
značně	značně	k6eAd1	značně
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
od	od	k7c2	od
soudobých	soudobý	k2eAgInPc2d1	soudobý
slovanských	slovanský	k2eAgInPc2d1	slovanský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
slovanské	slovanský	k2eAgInPc4d1	slovanský
jazyky	jazyk	k1gInPc4	jazyk
se	se	k3xPyFc4	se
odkazují	odkazovat	k5eAaImIp3nP	odkazovat
i	i	k9	i
konstruované	konstruovaný	k2eAgInPc4d1	konstruovaný
jazyky	jazyk	k1gInPc4	jazyk
některých	některý	k3yIgMnPc2	některý
nadšenců	nadšenec	k1gMnPc2	nadšenec
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
inspirované	inspirovaný	k2eAgFnPc4d1	inspirovaná
převážně	převážně	k6eAd1	převážně
absencí	absence	k1gFnSc7	absence
živých	živý	k2eAgInPc2d1	živý
severoslovanských	severoslovanský	k2eAgInPc2d1	severoslovanský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
:	:	kIx,	:
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
Ill	Ill	k1gFnSc1	Ill
Bethisad	Bethisad	k1gInSc1	Bethisad
vozgičtina	vozgičtina	k1gFnSc1	vozgičtina
(	(	kIx(	(
<g/>
vuozgašu	vuozgasat	k5eAaPmIp1nS	vuozgasat
<g/>
)	)	kIx)	)
skuodština	skuodština	k1gFnSc1	skuodština
ostatní	ostatní	k2eAgMnPc4d1	ostatní
severoslovanské	severoslovanský	k2eAgMnPc4d1	severoslovanský
jazyky	jazyk	k1gMnPc4	jazyk
lydnevi	lydnevit	k5eAaPmRp2nS	lydnevit
sievrosku	sievroska	k1gFnSc4	sievroska
(	(	kIx(	(
<g/>
sevorian	sevorian	k1gInSc4	sevorian
<g/>
)	)	kIx)	)
slaveni	slaven	k2eAgMnPc1d1	slaven
slavisk	slavisk	k1gInSc4	slavisk
Slovanské	slovanský	k2eAgInPc1d1	slovanský
jazyky	jazyk	k1gInPc1	jazyk
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
používají	používat	k5eAaImIp3nP	používat
několik	několik	k4yIc4	několik
variant	varianta	k1gFnPc2	varianta
latinky	latinka	k1gFnSc2	latinka
<g/>
:	:	kIx,	:
západoslovanské	západoslovanský	k2eAgInPc1d1	západoslovanský
jazyky	jazyk	k1gInPc1	jazyk
a	a	k8xC	a
západní	západní	k2eAgInPc1d1	západní
jihoslovanské	jihoslovanský	k2eAgInPc1d1	jihoslovanský
jazyky	jazyk	k1gInPc1	jazyk
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
srbštiny	srbština	k1gFnSc2	srbština
<g/>
)	)	kIx)	)
a	a	k8xC	a
několik	několik	k4yIc1	několik
variant	varianta	k1gFnPc2	varianta
cyrilice	cyrilice	k1gFnSc2	cyrilice
<g/>
:	:	kIx,	:
východoslovanské	východoslovanský	k2eAgInPc1d1	východoslovanský
jazyky	jazyk	k1gInPc1	jazyk
<g/>
,	,	kIx,	,
východní	východní	k2eAgInPc1d1	východní
jihoslovanské	jihoslovanský	k2eAgInPc1d1	jihoslovanský
jazyky	jazyk	k1gInPc1	jazyk
a	a	k8xC	a
srbština	srbština	k1gFnSc1	srbština
<g/>
.	.	kIx.	.
</s>
<s>
Historické	historický	k2eAgNnSc1d1	historické
je	být	k5eAaImIp3nS	být
především	především	k9	především
nejstarší	starý	k2eAgNnSc4d3	nejstarší
slovanské	slovanský	k2eAgNnSc4d1	slovanské
písmo	písmo	k1gNnSc4	písmo
hlaholice	hlaholice	k1gFnSc2	hlaholice
<g/>
,	,	kIx,	,
patrně	patrně	k6eAd1	patrně
autorství	autorství	k1gNnSc2	autorství
věrozvěstů	věrozvěst	k1gMnPc2	věrozvěst
Cyrila	Cyril	k1gMnSc2	Cyril
a	a	k8xC	a
Metoděje	Metoděj	k1gMnSc2	Metoděj
a	a	k8xC	a
užívané	užívaný	k2eAgFnSc2d1	užívaná
pro	pro	k7c4	pro
církevní	církevní	k2eAgInPc4d1	církevní
účely	účel	k1gInPc4	účel
dosud	dosud	k6eAd1	dosud
<g/>
.	.	kIx.	.
</s>
<s>
Historickou	historický	k2eAgFnSc7d1	historická
výjimkou	výjimka	k1gFnSc7	výjimka
byly	být	k5eAaImAgFnP	být
středověké	středověký	k2eAgFnPc1d1	středověká
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
staročeské	staročeský	k2eAgFnPc1d1	staročeská
<g/>
,	,	kIx,	,
glosy	glosa	k1gFnPc1	glosa
v	v	k7c6	v
židovských	židovský	k2eAgInPc6d1	židovský
spisech	spis	k1gInPc6	spis
<g/>
,	,	kIx,	,
psané	psaný	k2eAgNnSc1d1	psané
hebrejským	hebrejský	k2eAgNnSc7d1	hebrejské
písmem	písmo	k1gNnSc7	písmo
<g/>
,	,	kIx,	,
zvané	zvaný	k2eAgNnSc1d1	zvané
lešon	lešon	k1gNnSc1	lešon
Kenaan	Kenaan	k1gInSc1	Kenaan
(	(	kIx(	(
<g/>
kenánský	kenánský	k2eAgInSc1d1	kenánský
jazyk	jazyk	k1gInSc1	jazyk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
