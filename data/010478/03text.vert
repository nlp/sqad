<p>
<s>
Jeremy	Jerem	k1gInPc1	Jerem
Reich	Reich	k?	Reich
(	(	kIx(	(
<g/>
*	*	kIx~	*
11	[number]	k4	11
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1979	[number]	k4	1979
<g/>
,	,	kIx,	,
Craik	Craik	k1gInSc1	Craik
<g/>
,	,	kIx,	,
Saskatchewan	Saskatchewan	k1gInSc1	Saskatchewan
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
kanadský	kanadský	k2eAgMnSc1d1	kanadský
profesionální	profesionální	k2eAgMnSc1d1	profesionální
hokejista	hokejista	k1gMnSc1	hokejista
momentálně	momentálně	k6eAd1	momentálně
hrající	hrající	k2eAgMnSc1d1	hrající
za	za	k7c2	za
ERC	ERC	kA	ERC
Ingolstadt	Ingolstadt	k1gInSc1	Ingolstadt
v	v	k7c6	v
DEL	DEL	kA	DEL
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
grinder	grinder	k1gMnSc1	grinder
<g/>
,	,	kIx,	,
hráč	hráč	k1gMnSc1	hráč
který	který	k3yQgMnSc1	který
vyniká	vynikat	k5eAaImIp3nS	vynikat
ve	v	k7c6	v
fyzické	fyzický	k2eAgFnSc6d1	fyzická
hře	hra	k1gFnSc6	hra
a	a	k8xC	a
neváhá	váhat	k5eNaImIp3nS	váhat
se	se	k3xPyFc4	se
porvat	porvat	k5eAaPmF	porvat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hráčská	hráčský	k2eAgFnSc1d1	hráčská
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
kariéře	kariéra	k1gFnSc6	kariéra
ve	v	k7c6	v
vedlejší	vedlejší	k2eAgFnSc6d1	vedlejší
juniorské	juniorský	k2eAgFnSc6d1	juniorská
soutěži	soutěž	k1gFnSc6	soutěž
Saskatchewan	Saskatchewan	k1gMnSc1	Saskatchewan
Midget	Midgeta	k1gFnPc2	Midgeta
AAA	AAA	kA	AAA
Hockey	Hockea	k1gMnSc2	Hockea
League	Leagu	k1gMnSc2	Leagu
za	za	k7c4	za
tým	tým	k1gInSc4	tým
Saskatchewan	Saskatchewan	k1gInSc1	Saskatchewan
Contacts	Contacts	k1gInSc1	Contacts
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgInS	vydat
na	na	k7c4	na
dráhu	dráha	k1gFnSc4	dráha
hlavních	hlavní	k2eAgFnPc2d1	hlavní
juniorských	juniorský	k2eAgFnPc2d1	juniorská
lig	liga	k1gFnPc2	liga
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995-96	[number]	k4	1995-96
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
ve	v	k7c6	v
WHL	WHL	kA	WHL
za	za	k7c4	za
tým	tým	k1gInSc4	tým
Seattle	Seattle	k1gFnSc2	Seattle
Thunderbirds	Thunderbirdsa	k1gFnPc2	Thunderbirdsa
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
své	svůj	k3xOyFgFnSc6	svůj
druhé	druhý	k4xOgFnSc6	druhý
sezoně	sezona	k1gFnSc6	sezona
v	v	k7c6	v
týmu	tým	k1gInSc6	tým
ho	on	k3xPp3gMnSc4	on
na	na	k7c6	na
draftu	draft	k1gInSc6	draft
NHL	NHL	kA	NHL
1997	[number]	k4	1997
draftovali	draftovat	k5eAaImAgMnP	draftovat
jako	jako	k8xC	jako
devětatřicátého	devětatřicátý	k4xOgMnSc4	devětatřicátý
celkově	celkově	k6eAd1	celkově
Chicago	Chicago	k1gNnSc4	Chicago
Blackhawks	Blackhawksa	k1gFnPc2	Blackhawksa
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
zůstal	zůstat	k5eAaPmAgInS	zůstat
ve	v	k7c6	v
WHL	WHL	kA	WHL
a	a	k8xC	a
v	v	k7c6	v
sezoně	sezona	k1gFnSc6	sezona
1997-98	[number]	k4	1997-98
byl	být	k5eAaImAgInS	být
vyměněn	vyměnit	k5eAaPmNgInS	vyměnit
do	do	k7c2	do
Swift	Swifta	k1gFnPc2	Swifta
Current	Current	k1gMnSc1	Current
Broncos	Broncos	k1gMnSc1	Broncos
za	za	k7c2	za
Jeffreyho	Jeffrey	k1gMnSc2	Jeffrey
Beatche	Beatch	k1gMnSc2	Beatch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sezona	sezona	k1gFnSc1	sezona
1999-00	[number]	k4	1999-00
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gFnSc1	jeho
poslední	poslední	k2eAgFnSc1d1	poslední
v	v	k7c6	v
juniorské	juniorský	k2eAgFnSc6d1	juniorská
kategorii	kategorie	k1gFnSc6	kategorie
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
profesionálem	profesionál	k1gMnSc7	profesionál
ve	v	k7c4	v
farmářské	farmářský	k2eAgInPc4d1	farmářský
American	American	k1gInSc4	American
Hockey	Hockea	k1gFnSc2	Hockea
League	Leagu	k1gFnSc2	Leagu
<g/>
,	,	kIx,	,
za	za	k7c4	za
tým	tým	k1gInSc4	tým
Syracuse	Syracuse	k1gFnSc2	Syracuse
Crunch	Cruncha	k1gFnPc2	Cruncha
<g/>
.	.	kIx.	.
</s>
<s>
Debut	debut	k1gInSc1	debut
v	v	k7c6	v
NHL	NHL	kA	NHL
si	se	k3xPyFc3	se
odbyl	odbýt	k5eAaPmAgInS	odbýt
v	v	k7c6	v
sezoně	sezona	k1gFnSc6	sezona
2003	[number]	k4	2003
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
si	se	k3xPyFc3	se
za	za	k7c4	za
Columbus	Columbus	k1gInSc4	Columbus
Blue	Blue	k1gInSc1	Blue
Jackets	Jacketsa	k1gFnPc2	Jacketsa
zahrál	zahrát	k5eAaPmAgInS	zahrát
v	v	k7c6	v
devíti	devět	k4xCc6	devět
zápasech	zápas	k1gInPc6	zápas
a	a	k8xC	a
připsal	připsat	k5eAaPmAgMnS	připsat
si	se	k3xPyFc3	se
jednu	jeden	k4xCgFnSc4	jeden
asistenci	asistence	k1gFnSc4	asistence
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezoně	sezona	k1gFnSc6	sezona
2004	[number]	k4	2004
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
hráč	hráč	k1gMnSc1	hráč
Crunch	Crunch	k1gMnSc1	Crunch
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
vyměněn	vyměnit	k5eAaPmNgInS	vyměnit
do	do	k7c2	do
Houston	Houston	k1gInSc4	Houston
Aeros	Aeros	k1gInSc4	Aeros
za	za	k7c4	za
hostování	hostování	k1gNnSc4	hostování
Jasona	Jason	k1gMnSc2	Jason
Becketta	Beckett	k1gMnSc2	Beckett
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
té	ten	k3xDgFnSc6	ten
sezoně	sezona	k1gFnSc6	sezona
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
podepsal	podepsat	k5eAaPmAgMnS	podepsat
jako	jako	k9	jako
volný	volný	k2eAgMnSc1d1	volný
hráč	hráč	k1gMnSc1	hráč
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
Boston	Boston	k1gInSc1	Boston
Bruins	Bruinsa	k1gFnPc2	Bruinsa
a	a	k8xC	a
začínal	začínat	k5eAaImAgMnS	začínat
na	na	k7c6	na
jejich	jejich	k3xOp3gFnSc6	jejich
farmě	farma	k1gFnSc6	farma
v	v	k7c6	v
AHL	AHL	kA	AHL
<g/>
,	,	kIx,	,
Providence	providence	k1gFnSc2	providence
Bruins	Bruinsa	k1gFnPc2	Bruinsa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezoně	sezona	k1gFnSc6	sezona
2007-08	[number]	k4	2007-08
vstřelil	vstřelit	k5eAaPmAgMnS	vstřelit
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
gól	gól	k1gInSc4	gól
v	v	k7c6	v
NHL	NHL	kA	NHL
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
proti	proti	k7c3	proti
svému	svůj	k3xOyFgInSc3	svůj
bývalému	bývalý	k2eAgInSc3d1	bývalý
týmu	tým	k1gInSc3	tým
<g/>
,	,	kIx,	,
Columbus	Columbus	k1gInSc4	Columbus
Blue	Blu	k1gFnSc2	Blu
Jackets	Jacketsa	k1gFnPc2	Jacketsa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
opět	opět	k6eAd1	opět
jako	jako	k9	jako
volný	volný	k2eAgMnSc1d1	volný
hráč	hráč	k1gMnSc1	hráč
<g/>
,	,	kIx,	,
podepsal	podepsat	k5eAaPmAgMnS	podepsat
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
New	New	k1gFnSc7	New
York	York	k1gInSc1	York
Islanders	Islanders	k1gInSc1	Islanders
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
farmářský	farmářský	k2eAgInSc4d1	farmářský
tým	tým	k1gInSc4	tým
Bridgeport	Bridgeport	k1gInSc1	Bridgeport
Sound	Sound	k1gInSc1	Sound
Tigers	Tigers	k1gInSc1	Tigers
si	se	k3xPyFc3	se
připsal	připsat	k5eAaPmAgInS	připsat
12	[number]	k4	12
gólů	gól	k1gInPc2	gól
ve	v	k7c6	v
33	[number]	k4	33
zápasech	zápas	k1gInPc6	zápas
<g/>
,	,	kIx,	,
sezonu	sezona	k1gFnSc4	sezona
mu	on	k3xPp3gMnSc3	on
zkrátilo	zkrátit	k5eAaPmAgNnS	zkrátit
zranění	zranění	k1gNnSc4	zranění
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2010	[number]	k4	2010
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
stal	stát	k5eAaPmAgInS	stát
volným	volný	k2eAgMnSc7d1	volný
hráčem	hráč	k1gMnSc7	hráč
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgInS	vrátit
do	do	k7c2	do
Bruins	Bruinsa	k1gFnPc2	Bruinsa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
podepsal	podepsat	k5eAaPmAgMnS	podepsat
jednoletou	jednoletý	k2eAgFnSc4d1	jednoletá
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Přestupy	přestup	k1gInPc4	přestup
===	===	k?	===
</s>
</p>
<p>
<s>
20	[number]	k4	20
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1997	[number]	k4	1997
-	-	kIx~	-
Seattle	Seattle	k1gFnSc1	Seattle
Thunderbirds	Thunderbirdsa	k1gFnPc2	Thunderbirdsa
→	→	k?	→
Swift	Swift	k1gMnSc1	Swift
Current	Current	k1gMnSc1	Current
Broncos	Broncos	k1gMnSc1	Broncos
-	-	kIx~	-
za	za	k7c4	za
Jeffreyho	Jeffrey	k1gMnSc4	Jeffrey
Betche	Betch	k1gMnSc4	Betch
</s>
</p>
<p>
<s>
10	[number]	k4	10
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2005	[number]	k4	2005
-	-	kIx~	-
Syracuse	Syracus	k1gInSc6	Syracus
Crunch	Crunch	k1gInSc4	Crunch
na	na	k7c4	na
hostování	hostování	k1gNnSc4	hostování
→	→	k?	→
Houston	Houston	k1gInSc1	Houston
Aeros	Aeros	k1gInSc1	Aeros
-	-	kIx~	-
za	za	k7c4	za
hostování	hostování	k1gNnSc4	hostování
Jasona	Jason	k1gMnSc2	Jason
Becketta	Beckett	k1gMnSc2	Beckett
</s>
</p>
<p>
<s>
7	[number]	k4	7
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2005	[number]	k4	2005
-	-	kIx~	-
volný	volný	k2eAgMnSc1d1	volný
hráč	hráč	k1gMnSc1	hráč
→	→	k?	→
Boston	Boston	k1gInSc1	Boston
Bruins	Bruins	k1gInSc1	Bruins
</s>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2009	[number]	k4	2009
-	-	kIx~	-
volný	volný	k2eAgMnSc1d1	volný
hráč	hráč	k1gMnSc1	hráč
→	→	k?	→
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
Islanders	Islanders	k1gInSc1	Islanders
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2010	[number]	k4	2010
-	-	kIx~	-
volný	volný	k2eAgMnSc1d1	volný
hráč	hráč	k1gMnSc1	hráč
→	→	k?	→
Boston	Boston	k1gInSc1	Boston
Bruins	Bruins	k1gInSc1	Bruins
</s>
</p>
<p>
<s>
==	==	k?	==
Klubové	klubový	k2eAgFnPc1d1	klubová
statistiky	statistika	k1gFnPc1	statistika
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Jeremy	Jerema	k1gFnSc2	Jerema
Reich	Reich	k?	Reich
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Jeremy	Jerema	k1gFnSc2	Jerema
Reich	Reich	k?	Reich
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Jeremy	Jerema	k1gFnPc1	Jerema
Reich	Reich	k?	Reich
–	–	k?	–
statistiky	statistika	k1gFnSc2	statistika
na	na	k7c4	na
Hockeydb	Hockeydb	k1gInSc4	Hockeydb
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jeremy	Jerema	k1gFnPc1	Jerema
Reich	Reich	k?	Reich
–	–	k?	–
statistiky	statistika	k1gFnSc2	statistika
na	na	k7c4	na
Eliteprospects	Eliteprospects	k1gInSc4	Eliteprospects
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jeremy	Jerema	k1gFnPc1	Jerema
Reich	Reich	k?	Reich
–	–	k?	–
statistiky	statistika	k1gFnSc2	statistika
na	na	k7c4	na
NHL	NHL	kA	NHL
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
