<s>
Bangkok	Bangkok	k1gInSc1	Bangkok
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Thajska	Thajsko	k1gNnSc2	Thajsko
a	a	k8xC	a
nejlidnatější	lidnatý	k2eAgNnSc4d3	nejlidnatější
město	město	k1gNnSc4	město
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Thajci	Thajce	k1gMnPc1	Thajce
ho	on	k3xPp3gNnSc4	on
znají	znát	k5eAaImIp3nP	znát
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Krung	Krung	k1gMnSc1	Krung
Thep	Thep	k1gMnSc1	Thep
Maha	Maha	k1gMnSc1	Maha
Nakhon	Nakhon	k1gMnSc1	Nakhon
nebo	nebo	k8xC	nebo
zjednodušeně	zjednodušeně	k6eAd1	zjednodušeně
Krung	Krung	k1gMnSc1	Krung
Thep	Thep	k1gMnSc1	Thep
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
1	[number]	k4	1
568	[number]	k4	568
km2	km2	k4	km2
v	v	k7c6	v
deltě	delta	k1gFnSc6	delta
řeky	řeka	k1gFnSc2	řeka
Čao-Praja	Čao-Praj	k1gInSc2	Čao-Praj
ve	v	k7c6	v
středním	střední	k2eAgNnSc6d1	střední
Thajsku	Thajsko	k1gNnSc6	Thajsko
a	a	k8xC	a
žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
více	hodně	k6eAd2	hodně
než	než	k8xS	než
8	[number]	k4	8
milionů	milion	k4xCgInPc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
12,6	[number]	k4	12,6
%	%	kIx~	%
všech	všecek	k3xTgMnPc2	všecek
obyvatel	obyvatel	k1gMnPc2	obyvatel
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
14	[number]	k4	14
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
(	(	kIx(	(
<g/>
22,2	[number]	k4	22,2
%	%	kIx~	%
<g/>
)	)	kIx)	)
žijících	žijící	k2eAgInPc2d1	žijící
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
metropolitní	metropolitní	k2eAgFnSc2d1	metropolitní
oblasti	oblast	k1gFnSc2	oblast
Bangkoku	Bangkok	k1gInSc2	Bangkok
<g/>
,	,	kIx,	,
dělá	dělat	k5eAaImIp3nS	dělat
Bangkok	Bangkok	k1gInSc1	Bangkok
světovým	světový	k2eAgNnSc7d1	světové
velkoměstem	velkoměsto	k1gNnSc7	velkoměsto
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
zmínky	zmínka	k1gFnPc1	zmínka
o	o	k7c6	o
Bangkoku	Bangkok	k1gInSc6	Bangkok
nalezneme	nalézt	k5eAaBmIp1nP	nalézt
v	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
plnil	plnit	k5eAaImAgInS	plnit
úlohu	úloha	k1gFnSc4	úloha
malé	malý	k2eAgFnSc2d1	malá
obchodní	obchodní	k2eAgFnSc2d1	obchodní
stanice	stanice	k1gFnSc2	stanice
<g/>
,	,	kIx,	,
během	během	k7c2	během
království	království	k1gNnSc2	království
Ajutthaja	Ajutthaj	k1gInSc2	Ajutthaj
<g/>
.	.	kIx.	.
</s>
<s>
Nezadržitelný	zadržitelný	k2eNgInSc1d1	nezadržitelný
růst	růst	k1gInSc1	růst
však	však	k9	však
znamenal	znamenat	k5eAaImAgInS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
místem	místo	k1gNnSc7	místo
dvou	dva	k4xCgNnPc2	dva
hlavních	hlavní	k2eAgNnPc2d1	hlavní
měst	město	k1gNnPc2	město
<g/>
:	:	kIx,	:
Thonburi	Thonbur	k1gFnSc2	Thonbur
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1768	[number]	k4	1768
a	a	k8xC	a
Rattanakosin	Rattanakosin	k1gMnSc1	Rattanakosin
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1782	[number]	k4	1782
<g/>
.	.	kIx.	.
</s>
<s>
Bangkok	Bangkok	k1gInSc4	Bangkok
byl	být	k5eAaImAgMnS	být
srdcem	srdce	k1gNnSc7	srdce
Siamské	siamský	k2eAgFnSc2d1	siamská
modernizace	modernizace	k1gFnSc2	modernizace
na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
pod	pod	k7c4	pod
tlak	tlak	k1gInSc4	tlak
ze	z	k7c2	z
západu	západ	k1gInSc2	západ
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
centrem	centrum	k1gNnSc7	centrum
politických	politický	k2eAgInPc2d1	politický
bojů	boj	k1gInPc2	boj
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
když	když	k8xS	když
byla	být	k5eAaImAgFnS	být
zrušena	zrušit	k5eAaPmNgFnS	zrušit
absolutistická	absolutistický	k2eAgFnSc1d1	absolutistická
monarchie	monarchie	k1gFnSc1	monarchie
a	a	k8xC	a
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
mnoha	mnoho	k4c3	mnoho
převratům	převrat	k1gInPc3	převrat
a	a	k8xC	a
vzpourám	vzpoura	k1gFnPc3	vzpoura
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
rychle	rychle	k6eAd1	rychle
rostlo	růst	k5eAaImAgNnS	růst
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
působí	působit	k5eAaImIp3nS	působit
veliký	veliký	k2eAgInSc4d1	veliký
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
politiku	politika	k1gFnSc4	politika
<g/>
,	,	kIx,	,
hospodářství	hospodářství	k1gNnSc4	hospodářství
<g/>
,	,	kIx,	,
vzdělávání	vzdělávání	k1gNnSc4	vzdělávání
<g/>
,	,	kIx,	,
média	médium	k1gNnPc1	médium
a	a	k8xC	a
moderní	moderní	k2eAgFnSc1d1	moderní
společnost	společnost	k1gFnSc1	společnost
Thajska	Thajsko	k1gNnSc2	Thajsko
<g/>
.	.	kIx.	.
</s>
<s>
Asijský	asijský	k2eAgInSc1d1	asijský
investiční	investiční	k2eAgInSc1d1	investiční
boom	boom	k1gInSc1	boom
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
a	a	k8xC	a
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
vedl	vést	k5eAaImAgInS	vést
mnoho	mnoho	k4c4	mnoho
nadnárodních	nadnárodní	k2eAgFnPc2d1	nadnárodní
společností	společnost	k1gFnPc2	společnost
k	k	k7c3	k
založení	založení	k1gNnSc3	založení
regionálního	regionální	k2eAgNnSc2d1	regionální
zastoupení	zastoupení	k1gNnSc2	zastoupení
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
hlavní	hlavní	k2eAgFnSc7d1	hlavní
regionální	regionální	k2eAgFnSc7d1	regionální
silou	síla	k1gFnSc7	síla
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
financí	finance	k1gFnPc2	finance
a	a	k8xC	a
obchodu	obchod	k1gInSc2	obchod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
samotném	samotný	k2eAgNnSc6d1	samotné
městě	město	k1gNnSc6	město
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
vidění	vidění	k1gNnSc3	vidění
rušný	rušný	k2eAgInSc4d1	rušný
pouliční	pouliční	k2eAgInSc4d1	pouliční
život	život	k1gInSc4	život
a	a	k8xC	a
mnoho	mnoho	k4c4	mnoho
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
<g/>
.	.	kIx.	.
</s>
<s>
Notoricky	notoricky	k6eAd1	notoricky
známé	známý	k2eAgNnSc1d1	známé
red-light	redight	k5eAaPmF	red-light
districts	districts	k6eAd1	districts
<g/>
,	,	kIx,	,
dodaly	dodat	k5eAaPmAgFnP	dodat
městu	město	k1gNnSc3	město
exotický	exotický	k2eAgInSc4d1	exotický
půvab	půvab	k1gInSc4	půvab
<g/>
.	.	kIx.	.
</s>
<s>
Historický	historický	k2eAgMnSc1d1	historický
Grand	grand	k1gMnSc1	grand
Palace	Palace	k1gFnSc2	Palace
a	a	k8xC	a
buddhistické	buddhistický	k2eAgInPc1d1	buddhistický
chrámy	chrám	k1gInPc1	chrám
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Wat	Wat	k1gFnSc2	Wat
Arun	Arun	k1gMnSc1	Arun
a	a	k8xC	a
Wat	Wat	k1gMnSc1	Wat
Pho	Pho	k1gMnSc1	Pho
<g/>
,	,	kIx,	,
stojí	stát	k5eAaImIp3nS	stát
v	v	k7c6	v
kontrastu	kontrast	k1gInSc6	kontrast
s	s	k7c7	s
jinými	jiný	k2eAgFnPc7d1	jiná
turistickými	turistický	k2eAgFnPc7d1	turistická
atrakcemi	atrakce	k1gFnPc7	atrakce
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
noční	noční	k2eAgFnPc1d1	noční
scény	scéna	k1gFnPc1	scéna
Khaosan	Khaosany	k1gInPc2	Khaosany
a	a	k8xC	a
Patpong	Patponga	k1gFnPc2	Patponga
<g/>
.	.	kIx.	.
</s>
<s>
Bangkok	Bangkok	k1gInSc1	Bangkok
je	být	k5eAaImIp3nS	být
mezi	mezi	k7c7	mezi
světovými	světový	k2eAgFnPc7d1	světová
nejnavštěvovanějšími	navštěvovaný	k2eAgFnPc7d3	nejnavštěvovanější
turistickými	turistický	k2eAgFnPc7d1	turistická
destinacemi	destinace	k1gFnPc7	destinace
hned	hned	k6eAd1	hned
po	po	k7c6	po
Londýně	Londýn	k1gInSc6	Londýn
a	a	k8xC	a
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Časopisem	časopis	k1gInSc7	časopis
Travel	Travlo	k1gNnPc2	Travlo
+	+	kIx~	+
Leisure	Leisur	k1gMnSc5	Leisur
byl	být	k5eAaImAgInS	být
třikrát	třikrát	k6eAd1	třikrát
po	po	k7c6	po
sobě	sebe	k3xPyFc6	sebe
jmenován	jmenován	k2eAgInSc1d1	jmenován
nejlepším	dobrý	k2eAgNnSc7d3	nejlepší
městem	město	k1gNnSc7	město
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
svému	svůj	k3xOyFgInSc3	svůj
rychlému	rychlý	k2eAgInSc3d1	rychlý
růstu	růst	k1gInSc3	růst
má	mít	k5eAaImIp3nS	mít
Bangkok	Bangkok	k1gInSc1	Bangkok
problém	problém	k1gInSc1	problém
s	s	k7c7	s
dopravou	doprava	k1gFnSc7	doprava
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
přes	přes	k7c4	přes
rozsáhlé	rozsáhlý	k2eAgFnPc4d1	rozsáhlá
rychlostní	rychlostní	k2eAgFnPc4d1	rychlostní
sítě	síť	k1gFnPc4	síť
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
navyšováním	navyšování	k1gNnSc7	navyšování
soukromých	soukromý	k2eAgInPc2d1	soukromý
automobilů	automobil	k1gInPc2	automobil
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
ochromující	ochromující	k2eAgFnPc4d1	ochromující
dopravní	dopravní	k2eAgFnSc4d1	dopravní
zácpy	zácpa	k1gFnPc4	zácpa
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
zapříčinilo	zapříčinit	k5eAaPmAgNnS	zapříčinit
závažné	závažný	k2eAgNnSc1d1	závažné
znečištění	znečištění	k1gNnSc1	znečištění
ovzduší	ovzduší	k1gNnSc2	ovzduší
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
obrátilo	obrátit	k5eAaPmAgNnS	obrátit
k	k	k7c3	k
veřejné	veřejný	k2eAgFnSc3d1	veřejná
dopravě	doprava	k1gFnSc3	doprava
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
vyřešit	vyřešit	k5eAaPmF	vyřešit
tento	tento	k3xDgInSc1	tento
závažný	závažný	k2eAgInSc1d1	závažný
problém	problém	k1gInSc1	problém
<g/>
.	.	kIx.	.
</s>
<s>
Čtyři	čtyři	k4xCgFnPc1	čtyři
linky	linka	k1gFnPc1	linka
rychlé	rychlý	k2eAgFnSc2d1	rychlá
přepravy	přeprava	k1gFnSc2	přeprava
jsou	být	k5eAaImIp3nP	být
nyní	nyní	k6eAd1	nyní
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
výstavbě	výstavba	k1gFnSc6	výstavba
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
Bangkok	Bangkok	k1gInSc1	Bangkok
představoval	představovat	k5eAaImAgInS	představovat
jen	jen	k9	jen
malý	malý	k2eAgInSc1d1	malý
přístav	přístav	k1gInSc1	přístav
a	a	k8xC	a
obchodní	obchodní	k2eAgNnSc1d1	obchodní
centrum	centrum	k1gNnSc1	centrum
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
však	však	k9	však
změnilo	změnit	k5eAaPmAgNnS	změnit
s	s	k7c7	s
příchodem	příchod	k1gInSc7	příchod
dynastie	dynastie	k1gFnSc2	dynastie
Chakri	Chakri	k1gNnSc2	Chakri
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
vládne	vládnout	k5eAaImIp3nS	vládnout
ještě	ještě	k9	ještě
dnes	dnes	k6eAd1	dnes
<g/>
.	.	kIx.	.
</s>
<s>
Tu	tu	k6eAd1	tu
před	před	k7c7	před
více	hodně	k6eAd2	hodně
než	než	k8xS	než
200	[number]	k4	200
lety	léto	k1gNnPc7	léto
založil	založit	k5eAaPmAgMnS	založit
Rama	Rama	k1gMnSc1	Rama
I.	I.	kA	I.
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1782	[number]	k4	1782
korunován	korunován	k2eAgInSc4d1	korunován
a	a	k8xC	a
nařídil	nařídit	k5eAaPmAgMnS	nařídit
vybudování	vybudování	k1gNnSc4	vybudování
nového	nový	k2eAgNnSc2d1	nové
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
Bangkok	Bangkok	k1gInSc1	Bangkok
a	a	k8xC	a
nahradil	nahradit	k5eAaPmAgInS	nahradit
město	město	k1gNnSc4	město
Thonburi	Thonburi	k1gNnSc2	Thonburi
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
leželo	ležet	k5eAaImAgNnS	ležet
na	na	k7c6	na
levém	levý	k2eAgInSc6d1	levý
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
Chao	Chao	k1gMnSc1	Chao
Phraya	Phraya	k1gMnSc1	Phraya
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
něho	on	k3xPp3gNnSc2	on
čtvrť	čtvrť	k1gFnSc4	čtvrť
v	v	k7c6	v
Bangkoku	Bangkok	k1gInSc6	Bangkok
<g/>
.	.	kIx.	.
</s>
<s>
Bangkok	Bangkok	k1gInSc1	Bangkok
a	a	k8xC	a
Thonburi	Thonbur	k1gFnPc1	Thonbur
byly	být	k5eAaImAgFnP	být
oddělovány	oddělovat	k5eAaImNgFnP	oddělovat
řekou	řeka	k1gFnSc7	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
Taksin	Taksin	k1gMnSc1	Taksin
však	však	k9	však
zvolil	zvolit	k5eAaPmAgMnS	zvolit
za	za	k7c4	za
své	svůj	k3xOyFgNnSc4	svůj
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Thonburi	Thonbur	k1gFnSc2	Thonbur
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
většinu	většina	k1gFnSc4	většina
hlavních	hlavní	k2eAgFnPc2d1	hlavní
staveb	stavba	k1gFnPc2	stavba
dal	dát	k5eAaPmAgInS	dát
postavit	postavit	k5eAaPmF	postavit
na	na	k7c6	na
pravém	pravý	k2eAgInSc6d1	pravý
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
v	v	k7c6	v
Bangkoku	Bangkok	k1gInSc6	Bangkok
<g/>
.	.	kIx.	.
</s>
<s>
Taksin	Taksin	k1gInSc1	Taksin
<g/>
,	,	kIx,	,
poučen	poučen	k2eAgInSc1d1	poučen
tragédií	tragédie	k1gFnSc7	tragédie
po	po	k7c6	po
útoku	útok	k1gInSc6	útok
barmských	barmský	k2eAgFnPc2d1	barmská
armád	armáda	k1gFnPc2	armáda
na	na	k7c4	na
Ayutthayu	Ayutthaya	k1gFnSc4	Ayutthaya
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1767	[number]	k4	1767
<g/>
,	,	kIx,	,
chtěl	chtít	k5eAaImAgMnS	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
řeka	řeka	k1gFnSc1	řeka
protékala	protékat	k5eAaImAgFnS	protékat
městem	město	k1gNnSc7	město
a	a	k8xC	a
dala	dát	k5eAaPmAgFnS	dát
se	se	k3xPyFc4	se
využít	využít	k5eAaPmF	využít
jako	jako	k9	jako
úniková	únikový	k2eAgFnSc1d1	úniková
cesta	cesta	k1gFnSc1	cesta
do	do	k7c2	do
pevnosti	pevnost	k1gFnSc2	pevnost
v	v	k7c6	v
Chantaburi	Chantabur	k1gFnSc6	Chantabur
na	na	k7c6	na
východním	východní	k2eAgNnSc6d1	východní
pobřeží	pobřeží	k1gNnSc6	pobřeží
poblíž	poblíž	k6eAd1	poblíž
dnešní	dnešní	k2eAgFnSc6d1	dnešní
Kambodži	Kambodža	k1gFnSc6	Kambodža
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
Chao	Chao	k6eAd1	Chao
Phaya	Phaya	k1gMnSc1	Phaya
Chakri	Chakr	k1gFnSc2	Chakr
stal	stát	k5eAaPmAgMnS	stát
Králem	Král	k1gMnSc7	Král
Siamu	Siam	k1gInSc2	Siam
a	a	k8xC	a
barmská	barmský	k2eAgFnSc1d1	barmská
hrozba	hrozba	k1gFnSc1	hrozba
už	už	k6eAd1	už
nebyla	být	k5eNaImAgFnS	být
tak	tak	k6eAd1	tak
významná	významný	k2eAgFnSc1d1	významná
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
byl	být	k5eAaImAgInS	být
Siam	Siam	k1gInSc1	Siam
znovu	znovu	k6eAd1	znovu
silný	silný	k2eAgInSc1d1	silný
<g/>
.	.	kIx.	.
</s>
<s>
Rama	Rama	k6eAd1	Rama
I.	I.	kA	I.
za	za	k7c4	za
žádnou	žádný	k3yNgFnSc4	žádný
cenu	cena	k1gFnSc4	cena
nechtěl	chtít	k5eNaImAgMnS	chtít
opustit	opustit	k5eAaPmF	opustit
město	město	k1gNnSc4	město
a	a	k8xC	a
tak	tak	k6eAd1	tak
změnil	změnit	k5eAaPmAgMnS	změnit
taktiku	taktika	k1gFnSc4	taktika
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
únikových	únikový	k2eAgFnPc2d1	úniková
cest	cesta	k1gFnPc2	cesta
<g/>
,	,	kIx,	,
budoval	budovat	k5eAaImAgMnS	budovat
opevnění	opevnění	k1gNnSc4	opevnění
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
zanedbávat	zanedbávat	k5eAaImF	zanedbávat
západní	západní	k2eAgFnSc4d1	západní
stranu	strana	k1gFnSc4	strana
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nacházelo	nacházet	k5eAaImAgNnS	nacházet
Thonburi	Thonburi	k1gNnSc1	Thonburi
a	a	k8xC	a
veškeré	veškerý	k3xTgFnPc4	veškerý
stavby	stavba	k1gFnPc4	stavba
soustředil	soustředit	k5eAaPmAgMnS	soustředit
do	do	k7c2	do
Bangkoku	Bangkok	k1gInSc2	Bangkok
<g/>
.	.	kIx.	.
</s>
<s>
Prvně	Prvně	k?	Prvně
nechal	nechat	k5eAaPmAgInS	nechat
vystavět	vystavět	k5eAaPmF	vystavět
svůj	svůj	k3xOyFgInSc4	svůj
vlastní	vlastní	k2eAgInSc4d1	vlastní
palác	palác	k1gInSc4	palác
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nP	aby
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
musel	muset	k5eAaImAgMnS	muset
vysídlit	vysídlit	k5eAaPmF	vysídlit
obyvatele	obyvatel	k1gMnSc4	obyvatel
čínské	čínský	k2eAgFnSc2d1	čínská
osady	osada	k1gFnSc2	osada
asi	asi	k9	asi
3	[number]	k4	3
km	km	kA	km
jižněji	jižně	k6eAd2	jižně
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Číňané	Číňan	k1gMnPc1	Číňan
stále	stále	k6eAd1	stále
žijí	žít	k5eAaImIp3nP	žít
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
Sampheng	Sampheng	k1gInSc4	Sampheng
Road	Roada	k1gFnPc2	Roada
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
slavná	slavný	k2eAgFnSc1d1	slavná
čínská	čínský	k2eAgFnSc1d1	čínská
nákupní	nákupní	k2eAgFnSc1d1	nákupní
čtvrť	čtvrť	k1gFnSc1	čtvrť
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
palác	palác	k1gInSc1	palác
a	a	k8xC	a
Chrám	chrám	k1gInSc1	chrám
smaragdového	smaragdový	k2eAgMnSc2d1	smaragdový
Budhy	Budha	k1gMnSc2	Budha
byly	být	k5eAaImAgFnP	být
dokončeny	dokončen	k2eAgFnPc1d1	dokončena
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1785	[number]	k4	1785
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgNnSc1d1	Nové
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
teď	teď	k6eAd1	teď
rozkládalo	rozkládat	k5eAaImAgNnS	rozkládat
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
straně	strana	k1gFnSc6	strana
řeky	řeka	k1gFnSc2	řeka
a	a	k8xC	a
začalo	začít	k5eAaPmAgNnS	začít
používat	používat	k5eAaImF	používat
nové	nový	k2eAgNnSc1d1	nové
jméno	jméno	k1gNnSc1	jméno
<g/>
..	..	k?	..
Originální	originální	k2eAgInSc1d1	originální
název	název	k1gInSc1	název
města	město	k1gNnSc2	město
zní	znět	k5eAaImIp3nS	znět
<g/>
:	:	kIx,	:
Krung	Krung	k1gMnSc1	Krung
Thep	Thep	k1gMnSc1	Thep
Maha	Maha	k1gMnSc1	Maha
Nakhon	Nakhon	k1gMnSc1	Nakhon
Amorn	Amorn	k1gMnSc1	Amorn
Rattanakosindra	Rattanakosindra	k1gFnSc1	Rattanakosindra
Mahindrayutthaya	Mahindrayutthaya	k1gFnSc1	Mahindrayutthaya
Mahadilokpop	Mahadilokpop	k1gInSc1	Mahadilokpop
Noparattana	Noparattana	k1gFnSc1	Noparattana
Radchhani	Radchhaň	k1gFnSc6	Radchhaň
Burirom	Burirom	k1gInSc1	Burirom
Udom	Udoma	k1gFnPc2	Udoma
Rachnivet	Rachniveta	k1gFnPc2	Rachniveta
Mahastan	Mahastan	k1gInSc1	Mahastan
Amorn	Amorn	k1gMnSc1	Amorn
Pimarn	Pimarn	k1gMnSc1	Pimarn
Avatarn	Avatarn	k1gMnSc1	Avatarn
Satit	Satit	k1gMnSc1	Satit
Sakututtiya	Sakututtiya	k1gMnSc1	Sakututtiya
Vishnukarm	Vishnukarm	k1gInSc4	Vishnukarm
Prasit	prasit	k5eAaImF	prasit
<g/>
.	.	kIx.	.
</s>
<s>
Přeloženo	přeložit	k5eAaPmNgNnS	přeložit
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Město	město	k1gNnSc1	město
andělů	anděl	k1gMnPc2	anděl
<g/>
,	,	kIx,	,
velké	velký	k2eAgNnSc1d1	velké
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
věčné	věčný	k2eAgNnSc1d1	věčné
a	a	k8xC	a
nádherné	nádherný	k2eAgNnSc1d1	nádherné
sídlo	sídlo	k1gNnSc1	sídlo
hinduistického	hinduistický	k2eAgMnSc2d1	hinduistický
boha	bůh	k1gMnSc2	bůh
Indry	Indra	k1gMnSc2	Indra
<g/>
,	,	kIx,	,
vznešené	vznešený	k2eAgNnSc1d1	vznešené
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
uloženo	uložit	k5eAaPmNgNnS	uložit
devět	devět	k4xCc4	devět
skvostných	skvostný	k2eAgInPc2d1	skvostný
drahokamů	drahokam	k1gInPc2	drahokam
(	(	kIx(	(
<g/>
diamant	diamant	k1gInSc1	diamant
<g/>
,	,	kIx,	,
rubín	rubín	k1gInSc1	rubín
<g/>
,	,	kIx,	,
smaragd	smaragd	k1gInSc1	smaragd
<g/>
,	,	kIx,	,
žlutý	žlutý	k2eAgInSc1d1	žlutý
safír	safír	k1gInSc1	safír
<g/>
,	,	kIx,	,
granát	granát	k1gInSc1	granát
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
modrý	modrý	k2eAgInSc1d1	modrý
safír	safír	k1gInSc1	safír
<g/>
,	,	kIx,	,
měsíční	měsíční	k2eAgInSc1d1	měsíční
kámen	kámen	k1gInSc1	kámen
<g/>
,	,	kIx,	,
zirkon	zirkon	k1gInSc1	zirkon
<g/>
,	,	kIx,	,
kočičí	kočičí	k2eAgNnSc1d1	kočičí
oko	oko	k1gNnSc1	oko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
královské	královský	k2eAgNnSc1d1	královské
město	město	k1gNnSc1	město
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
velkolepými	velkolepý	k2eAgInPc7d1	velkolepý
paláci	palác	k1gInPc7	palác
a	a	k8xC	a
stavbami	stavba	k1gFnPc7	stavba
<g/>
,	,	kIx,	,
věčné	věčný	k2eAgNnSc1d1	věčné
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
sídlí	sídlet	k5eAaImIp3nP	sídlet
bohové	bůh	k1gMnPc1	bůh
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
svůj	svůj	k3xOyFgInSc4	svůj
domov	domov	k1gInSc4	domov
hinduistický	hinduistický	k2eAgMnSc1d1	hinduistický
bůh	bůh	k1gMnSc1	bůh
Krišna	Krišna	k1gMnSc1	Krišna
<g/>
,	,	kIx,	,
město	město	k1gNnSc1	město
stvořené	stvořený	k2eAgNnSc1d1	stvořené
bohem	bůh	k1gMnSc7	bůh
Višnou	Višna	k1gMnSc7	Višna
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Takto	takto	k6eAd1	takto
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
název	název	k1gInSc4	název
by	by	kYmCp3nP	by
si	se	k3xPyFc3	se
jistě	jistě	k6eAd1	jistě
skoro	skoro	k6eAd1	skoro
nikdo	nikdo	k3yNnSc1	nikdo
nezapamatoval	zapamatovat	k5eNaPmAgMnS	zapamatovat
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
zkráceně	zkráceně	k6eAd1	zkráceně
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1782	[number]	k4	1782
říká	říkat	k5eAaImIp3nS	říkat
Krung	Krung	k1gMnSc1	Krung
Thep	Thep	k1gMnSc1	Thep
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
Město	město	k1gNnSc1	město
andělů	anděl	k1gMnPc2	anděl
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
ho	on	k3xPp3gInSc4	on
celý	celý	k2eAgInSc4d1	celý
zbytek	zbytek	k1gInSc4	zbytek
světa	svět	k1gInSc2	svět
zná	znát	k5eAaImIp3nS	znát
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Bangkok	Bangkok	k1gInSc1	Bangkok
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byl	být	k5eAaImAgInS	být
název	název	k1gInSc4	název
původní	původní	k2eAgFnSc2d1	původní
osady	osada	k1gFnSc2	osada
<g/>
,	,	kIx,	,
a	a	k8xC	a
znamená	znamenat	k5eAaImIp3nS	znamenat
to	ten	k3xDgNnSc1	ten
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
město	město	k1gNnSc1	město
oliv	oliva	k1gFnPc2	oliva
a	a	k8xC	a
v	v	k7c6	v
samotném	samotný	k2eAgNnSc6d1	samotné
Thajsku	Thajsko	k1gNnSc6	Thajsko
bylo	být	k5eAaImAgNnS	být
používané	používaný	k2eAgNnSc1d1	používané
před	před	k7c7	před
200	[number]	k4	200
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
24	[number]	k4	24
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1932	[number]	k4	1932
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
skupina	skupina	k1gFnSc1	skupina
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Evropě	Evropa	k1gFnSc6	Evropa
vzdělaných	vzdělaný	k2eAgMnPc2d1	vzdělaný
důstojníků	důstojník	k1gMnPc2	důstojník
a	a	k8xC	a
členů	člen	k1gMnPc2	člen
vlády	vláda	k1gFnSc2	vláda
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
mohutný	mohutný	k2eAgInSc4d1	mohutný
útok	útok	k1gInSc4	útok
proti	proti	k7c3	proti
absolutní	absolutní	k2eAgFnSc3d1	absolutní
monarchii	monarchie	k1gFnSc3	monarchie
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Thajsko	Thajsko	k1gNnSc1	Thajsko
konstituční	konstituční	k2eAgFnSc7d1	konstituční
monarchií	monarchie	k1gFnSc7	monarchie
a	a	k8xC	a
v	v	k7c6	v
jejím	její	k3xOp3gNnSc6	její
čele	čelo	k1gNnSc6	čelo
stojí	stát	k5eAaImIp3nS	stát
král	král	k1gMnSc1	král
Bhumiphol	Bhumiphol	k1gInSc4	Bhumiphol
Adulyadej	Adulyadej	k1gFnSc2	Adulyadej
jako	jako	k8xC	jako
hlava	hlava	k1gFnSc1	hlava
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Změna	změna	k1gFnSc1	změna
od	od	k7c2	od
absolutní	absolutní	k2eAgFnSc2d1	absolutní
ke	k	k7c3	k
konstituční	konstituční	k2eAgFnSc3d1	konstituční
monarchii	monarchie	k1gFnSc3	monarchie
byla	být	k5eAaImAgFnS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nejdalekosáhlejší	dalekosáhlý	k2eAgInSc4d3	dalekosáhlý
posun	posun	k1gInSc4	posun
v	v	k7c6	v
thajské	thajský	k2eAgFnSc6d1	thajská
historii	historie	k1gFnSc6	historie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nebyla	být	k5eNaImAgFnS	být
to	ten	k3xDgNnSc1	ten
první	první	k4xOgFnSc1	první
změna	změna	k1gFnSc1	změna
ve	v	k7c6	v
struktuře	struktura	k1gFnSc6	struktura
monarchie	monarchie	k1gFnSc2	monarchie
a	a	k8xC	a
jistě	jistě	k9	jistě
to	ten	k3xDgNnSc1	ten
nebyl	být	k5eNaImAgInS	být
také	také	k9	také
nejnásilnější	násilný	k2eAgInSc1d3	nejnásilnější
a	a	k8xC	a
nejkrvavější	krvavý	k2eAgInSc1d3	nejkrvavější
přechod	přechod	k1gInSc1	přechod
<g/>
.	.	kIx.	.
</s>
<s>
Thajská	thajský	k2eAgFnSc1d1	thajská
monarchie	monarchie	k1gFnSc1	monarchie
má	mít	k5eAaImIp3nS	mít
původ	původ	k1gInSc4	původ
a	a	k8xC	a
také	také	k9	také
se	se	k3xPyFc4	se
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
z	z	k7c2	z
principů	princip	k1gInPc2	princip
domorodého	domorodý	k2eAgNnSc2d1	domorodé
vedení	vedení	k1gNnSc2	vedení
<g/>
.	.	kIx.	.
</s>
<s>
Způsob	způsob	k1gInSc1	způsob
vlády	vláda	k1gFnSc2	vláda
domorodých	domorodý	k2eAgMnPc2d1	domorodý
vůdců	vůdce	k1gMnPc2	vůdce
nebyl	být	k5eNaImAgInS	být
ovšem	ovšem	k9	ovšem
tak	tak	k6eAd1	tak
neomezený	omezený	k2eNgInSc1d1	neomezený
jako	jako	k8xC	jako
u	u	k7c2	u
králů	král	k1gMnPc2	král
v	v	k7c6	v
rozvinutějších	rozvinutý	k2eAgFnPc6d2	rozvinutější
společnostech	společnost	k1gFnPc6	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Thajská	thajský	k2eAgFnSc1d1	thajská
monarchie	monarchie	k1gFnSc1	monarchie
odstartovala	odstartovat	k5eAaPmAgFnS	odstartovat
v	v	k7c6	v
Sukhothaii	Sukhothaie	k1gFnSc6	Sukhothaie
s	s	k7c7	s
množstvím	množství	k1gNnSc7	množství
králů	král	k1gMnPc2	král
kteří	který	k3yQgMnPc1	který
byli	být	k5eAaImAgMnP	být
velmi	velmi	k6eAd1	velmi
blízko	blízko	k6eAd1	blízko
k	k	k7c3	k
lidem	člověk	k1gMnPc3	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Pojetí	pojetí	k1gNnSc1	pojetí
absolutní	absolutní	k2eAgFnSc2d1	absolutní
monarchie	monarchie	k1gFnSc2	monarchie
bylo	být	k5eAaImAgNnS	být
adoptováno	adoptovat	k5eAaPmNgNnS	adoptovat
z	z	k7c2	z
Khmerské	khmerský	k2eAgFnSc2d1	khmerská
říše	říš	k1gFnSc2	říš
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
Ayutthayské	Ayutthayský	k2eAgFnSc2d1	Ayutthayský
éry	éra	k1gFnSc2	éra
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Sukhothaiské	Sukhothaiský	k2eAgFnSc2d1	Sukhothaiský
éry	éra	k1gFnSc2	éra
až	až	k9	až
dosud	dosud	k6eAd1	dosud
vládlo	vládnout	k5eAaImAgNnS	vládnout
zemi	zem	k1gFnSc4	zem
osm	osm	k4xCc1	osm
dynastií	dynastie	k1gFnPc2	dynastie
a	a	k8xC	a
přechody	přechod	k1gInPc4	přechod
od	od	k7c2	od
jedné	jeden	k4xCgFnSc2	jeden
k	k	k7c3	k
druhé	druhý	k4xOgFnSc6	druhý
byly	být	k5eAaImAgInP	být
obvykle	obvykle	k6eAd1	obvykle
velice	velice	k6eAd1	velice
krvavé	krvavý	k2eAgNnSc1d1	krvavé
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
Bangkok	Bangkok	k1gInSc1	Bangkok
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
1568	[number]	k4	1568
km2	km2	k4	km2
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
asi	asi	k9	asi
700	[number]	k4	700
km2	km2	k4	km2
tvoří	tvořit	k5eAaImIp3nP	tvořit
zastavěné	zastavěný	k2eAgFnPc1d1	zastavěná
městské	městský	k2eAgFnPc1d1	městská
oblasti	oblast	k1gFnPc1	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
hraničí	hraničit	k5eAaImIp3nS	hraničit
s	s	k7c7	s
dalšími	další	k2eAgNnPc7d1	další
šesti	šest	k4xCc7	šest
provinciemi	provincie	k1gFnPc7	provincie
<g/>
:	:	kIx,	:
Nonthaburi	Nonthabur	k1gFnPc1	Nonthabur
<g/>
,	,	kIx,	,
Pathum	Pathum	k1gInSc1	Pathum
Thani	Than	k1gMnPc1	Than
<g/>
,	,	kIx,	,
Chachoengsao	Chachoengsao	k1gMnSc1	Chachoengsao
<g/>
,	,	kIx,	,
Samut	Samut	k1gMnSc1	Samut
Prakan	Prakan	k1gMnSc1	Prakan
<g/>
,	,	kIx,	,
Samut	Samut	k1gMnSc1	Samut
Sakhon	Sakhon	k1gMnSc1	Sakhon
a	a	k8xC	a
Nakhon	Nakhon	k1gMnSc1	Nakhon
Pathom	Pathom	k1gInSc1	Pathom
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Chachoengsao	Chachoengsao	k1gMnSc1	Chachoengsao
<g/>
,	,	kIx,	,
tato	tento	k3xDgFnSc1	tento
provincie	provincie	k1gFnSc1	provincie
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Bangkokem	Bangkok	k1gInSc7	Bangkok
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nP	tvořit
větší	veliký	k2eAgFnSc4d2	veliký
metropolitní	metropolitní	k2eAgFnSc4d1	metropolitní
oblast	oblast	k1gFnSc4	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Bangkok	Bangkok	k1gInSc1	Bangkok
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
deltě	delta	k1gFnSc6	delta
řeky	řeka	k1gFnSc2	řeka
Menam-Čao-Praja	Menam-Čao-Prajum	k1gNnSc2	Menam-Čao-Prajum
v	v	k7c6	v
centrálních	centrální	k2eAgFnPc6d1	centrální
pláních	pláň	k1gFnPc6	pláň
Thajska	Thajsko	k1gNnSc2	Thajsko
<g/>
.	.	kIx.	.
</s>
<s>
Řeka	řeka	k1gFnSc1	řeka
se	se	k3xPyFc4	se
vine	vinout	k5eAaImIp3nS	vinout
městem	město	k1gNnSc7	město
v	v	k7c6	v
jižním	jižní	k2eAgInSc6d1	jižní
směru	směr	k1gInSc6	směr
a	a	k8xC	a
vlévá	vlévat	k5eAaImIp3nS	vlévat
se	se	k3xPyFc4	se
do	do	k7c2	do
Thajského	thajský	k2eAgInSc2d1	thajský
zálivu	záliv	k1gInSc2	záliv
přibližně	přibližně	k6eAd1	přibližně
25	[number]	k4	25
km	km	kA	km
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
oblast	oblast	k1gFnSc1	oblast
je	být	k5eAaImIp3nS	být
plochá	plochý	k2eAgFnSc1d1	plochá
a	a	k8xC	a
nízko	nízko	k6eAd1	nízko
položená	položený	k2eAgFnSc1d1	položená
<g/>
,	,	kIx,	,
s	s	k7c7	s
průměrnou	průměrný	k2eAgFnSc7d1	průměrná
výškou	výška	k1gFnSc7	výška
1,5	[number]	k4	1,5
m	m	kA	m
nad	nad	k7c7	nad
hladinou	hladina	k1gFnSc7	hladina
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
zde	zde	k6eAd1	zde
byly	být	k5eAaImAgFnP	být
bažiny	bažina	k1gFnPc1	bažina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
postupně	postupně	k6eAd1	postupně
byly	být	k5eAaImAgInP	být
odčerpány	odčerpat	k5eAaPmNgInP	odčerpat
a	a	k8xC	a
pro	pro	k7c4	pro
potřeby	potřeba	k1gFnPc4	potřeba
zemědělců	zemědělec	k1gMnPc2	zemědělec
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
budovaly	budovat	k5eAaImAgInP	budovat
kanály	kanál	k1gInPc1	kanál
od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
až	až	k9	až
po	po	k7c4	po
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
spletitá	spletitý	k2eAgFnSc1d1	spletitá
vodní	vodní	k2eAgFnSc1d1	vodní
síť	síť	k1gFnSc1	síť
sloužila	sloužit	k5eAaImAgFnS	sloužit
jako	jako	k9	jako
primární	primární	k2eAgInSc4d1	primární
způsob	způsob	k1gInSc4	způsob
dopravy	doprava	k1gFnSc2	doprava
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
stavět	stavět	k5eAaImF	stavět
moderní	moderní	k2eAgFnPc1d1	moderní
silnice	silnice	k1gFnPc1	silnice
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnSc1	obyvatel
žila	žít	k5eAaImAgFnS	žít
na	na	k7c6	na
vodě	voda	k1gFnSc6	voda
a	a	k8xC	a
nebo	nebo	k8xC	nebo
v	v	k7c6	v
dosahu	dosah	k1gInSc6	dosah
její	její	k3xOp3gFnSc2	její
blízkosti	blízkost	k1gFnSc2	blízkost
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
si	se	k3xPyFc3	se
město	město	k1gNnSc4	město
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
vysloužilo	vysloužit	k5eAaPmAgNnS	vysloužit
název	název	k1gInSc4	název
"	"	kIx"	"
<g/>
Benátky	Benátky	k1gFnPc1	Benátky
východu	východ	k1gInSc2	východ
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
kanálů	kanál	k1gInPc2	kanál
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
některé	některý	k3yIgInPc4	některý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
stále	stále	k6eAd1	stále
křižují	křižovat	k5eAaImIp3nP	křižovat
město	město	k1gNnSc4	město
a	a	k8xC	a
slouží	sloužit	k5eAaImIp3nP	sloužit
jako	jako	k9	jako
odvodňovací	odvodňovací	k2eAgInPc1d1	odvodňovací
kanály	kanál	k1gInPc1	kanál
nebo	nebo	k8xC	nebo
dopravní	dopravní	k2eAgFnSc2d1	dopravní
cesty	cesta	k1gFnSc2	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Problém	problém	k1gInSc1	problém
je	být	k5eAaImIp3nS	být
však	však	k9	však
veliké	veliký	k2eAgNnSc4d1	veliké
znečištění	znečištění	k1gNnSc4	znečištění
<g/>
.	.	kIx.	.
</s>
<s>
Povrch	povrch	k1gInSc1	povrch
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yRgNnSc6	který
Bangkok	Bangkok	k1gInSc1	Bangkok
leží	ležet	k5eAaImIp3nS	ležet
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
z	z	k7c2	z
měkkého	měkký	k2eAgInSc2d1	měkký
mořského	mořský	k2eAgInSc2d1	mořský
jílu	jíl	k1gInSc2	jíl
<g/>
.	.	kIx.	.
</s>
<s>
Studie	studie	k1gFnPc1	studie
prokázaly	prokázat	k5eAaPmAgFnP	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
propadá	propadat	k5eAaImIp3nS	propadat
o	o	k7c4	o
10	[number]	k4	10
až	až	k9	až
30	[number]	k4	30
mm	mm	kA	mm
ročně	ročně	k6eAd1	ročně
a	a	k8xC	a
části	část	k1gFnPc1	část
města	město	k1gNnSc2	město
jsou	být	k5eAaImIp3nP	být
již	již	k9	již
někde	někde	k6eAd1	někde
i	i	k9	i
metr	metr	k1gInSc4	metr
pod	pod	k7c7	pod
hladinou	hladina	k1gFnSc7	hladina
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
obavy	obava	k1gFnPc1	obava
<g/>
,	,	kIx,	,
že	že	k8xS	že
město	město	k1gNnSc1	město
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
ponořené	ponořený	k2eAgInPc4d1	ponořený
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2030	[number]	k4	2030
<g/>
.	.	kIx.	.
</s>
<s>
Hrozí	hrozit	k5eAaImIp3nS	hrozit
zde	zde	k6eAd1	zde
stále	stále	k6eAd1	stále
větší	veliký	k2eAgFnPc1d2	veliký
povodně	povodeň	k1gFnPc1	povodeň
a	a	k8xC	a
vláda	vláda	k1gFnSc1	vláda
staví	stavit	k5eAaImIp3nS	stavit
nové	nový	k2eAgFnPc4d1	nová
a	a	k8xC	a
nové	nový	k2eAgFnPc4d1	nová
povodňové	povodňový	k2eAgFnPc4d1	povodňová
bariéry	bariéra	k1gFnPc4	bariéra
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
extrémním	extrémní	k2eAgFnPc3d1	extrémní
záplavám	záplava	k1gFnPc3	záplava
zde	zde	k6eAd1	zde
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1995	[number]	k4	1995
a	a	k8xC	a
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
většina	většina	k1gFnSc1	většina
Thajska	Thajsko	k1gNnSc2	Thajsko
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
Bangkok	Bangkok	k1gInSc1	Bangkok
tropické	tropický	k2eAgNnSc4d1	tropické
<g/>
,	,	kIx,	,
vlhké	vlhký	k2eAgNnSc4d1	vlhké
a	a	k8xC	a
suché	suchý	k2eAgNnSc4d1	suché
klima	klima	k1gNnSc4	klima
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
monzunového	monzunový	k2eAgInSc2d1	monzunový
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Prakticky	prakticky	k6eAd1	prakticky
zde	zde	k6eAd1	zde
platí	platit	k5eAaImIp3nS	platit
jen	jen	k9	jen
2	[number]	k4	2
roční	roční	k2eAgInSc4d1	roční
období	období	k1gNnSc4	období
<g/>
.	.	kIx.	.
</s>
<s>
Období	období	k1gNnSc1	období
dešťů	dešť	k1gInPc2	dešť
začíná	začínat	k5eAaImIp3nS	začínat
s	s	k7c7	s
příchodem	příchod	k1gInSc7	příchod
jihozápadního	jihozápadní	k2eAgInSc2d1	jihozápadní
monzunu	monzun	k1gInSc2	monzun
kolem	kolem	k7c2	kolem
poloviny	polovina	k1gFnSc2	polovina
května	květen	k1gInSc2	květen
<g/>
.	.	kIx.	.
</s>
<s>
Září	září	k1gNnSc1	září
je	být	k5eAaImIp3nS	být
nejdeštivější	deštivý	k2eAgInSc4d3	nejdeštivější
měsíc	měsíc	k1gInSc4	měsíc
<g/>
,	,	kIx,	,
s	s	k7c7	s
průměrnými	průměrný	k2eAgFnPc7d1	průměrná
srážkami	srážka	k1gFnPc7	srážka
344	[number]	k4	344
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Deštivá	deštivý	k2eAgFnSc1d1	deštivá
sezóna	sezóna	k1gFnSc1	sezóna
trvá	trvat	k5eAaImIp3nS	trvat
až	až	k9	až
do	do	k7c2	do
října	říjen	k1gInSc2	říjen
<g/>
.	.	kIx.	.
</s>
<s>
Horké	Horké	k2eAgNnSc1d1	Horké
období	období	k1gNnSc1	období
je	být	k5eAaImIp3nS	být
obecně	obecně	k6eAd1	obecně
suché	suchý	k2eAgNnSc1d1	suché
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
se	se	k3xPyFc4	se
vidí	vidět	k5eAaImIp3nS	vidět
příležitostné	příležitostný	k2eAgFnSc2d1	příležitostná
letní	letní	k2eAgFnSc2d1	letní
bouře	bouř	k1gFnSc2	bouř
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
zaznamenaná	zaznamenaný	k2eAgFnSc1d1	zaznamenaná
teplota	teplota	k1gFnSc1	teplota
v	v	k7c6	v
Bangkoku	Bangkok	k1gInSc6	Bangkok
je	být	k5eAaImIp3nS	být
40,8	[number]	k4	40,8
°	°	k?	°
<g/>
C	C	kA	C
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1983	[number]	k4	1983
a	a	k8xC	a
nejnižší	nízký	k2eAgFnSc1d3	nejnižší
zaznamenaná	zaznamenaný	k2eAgFnSc1d1	zaznamenaná
teplota	teplota	k1gFnSc1	teplota
je	být	k5eAaImIp3nS	být
9,9	[number]	k4	9,9
°	°	k?	°
<g/>
C	C	kA	C
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1955	[number]	k4	1955
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
více	hodně	k6eAd2	hodně
než	než	k8xS	než
200	[number]	k4	200
akrů	akr	k1gInPc2	akr
veliký	veliký	k2eAgInSc4d1	veliký
park	park	k1gInSc4	park
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
otevřen	otevřen	k2eAgInSc1d1	otevřen
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
60	[number]	k4	60
<g/>
.	.	kIx.	.
narozenin	narozeniny	k1gFnPc2	narozeniny
krále	král	k1gMnSc2	král
Ramy	Rama	k1gFnSc2	Rama
IX	IX	kA	IX
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
největší	veliký	k2eAgFnSc1d3	veliký
botanická	botanický	k2eAgFnSc1d1	botanická
zahrada	zahrada	k1gFnSc1	zahrada
ve	v	k7c4	v
města	město	k1gNnPc4	město
a	a	k8xC	a
8	[number]	k4	8
metrový	metrový	k2eAgInSc4d1	metrový
uměle	uměle	k6eAd1	uměle
vytvořený	vytvořený	k2eAgInSc4d1	vytvořený
vodopád	vodopád	k1gInSc4	vodopád
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
místě	místo	k1gNnSc6	místo
byla	být	k5eAaImAgFnS	být
kdysi	kdysi	k6eAd1	kdysi
královská	královský	k2eAgFnSc1d1	královská
botanická	botanický	k2eAgFnSc1d1	botanická
zahrada	zahrada	k1gFnSc1	zahrada
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
součástí	součást	k1gFnSc7	součást
královského	královský	k2eAgInSc2d1	královský
paláce	palác	k1gInSc2	palác
Dusit	dusit	k5eAaImF	dusit
<g/>
.	.	kIx.	.
</s>
<s>
Zoologickou	zoologický	k2eAgFnSc7d1	zoologická
zahradou	zahrada	k1gFnSc7	zahrada
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
až	až	k6eAd1	až
roku	rok	k1gInSc2	rok
1954	[number]	k4	1954
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
je	být	k5eAaImIp3nS	být
velká	velká	k1gFnSc1	velká
necelých	celý	k2eNgInPc2d1	necelý
50	[number]	k4	50
akrů	akr	k1gInPc2	akr
a	a	k8xC	a
ani	ani	k8xC	ani
expozicemi	expozice	k1gFnPc7	expozice
moc	moc	k6eAd1	moc
nevyniká	vynikat	k5eNaImIp3nS	vynikat
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přesto	přesto	k8xC	přesto
je	být	k5eAaImIp3nS	být
zastávkou	zastávka	k1gFnSc7	zastávka
mnoha	mnoho	k4c2	mnoho
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
zde	zde	k6eAd1	zde
relaxují	relaxovat	k5eAaBmIp3nP	relaxovat
a	a	k8xC	a
hledají	hledat	k5eAaImIp3nP	hledat
odpočinek	odpočinek	k1gInSc4	odpočinek
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
park	park	k1gInSc1	park
je	být	k5eAaImIp3nS	být
dominantou	dominanta	k1gFnSc7	dominanta
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Nalezneme	naleznout	k5eAaPmIp1nP	naleznout
ho	on	k3xPp3gMnSc4	on
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
Si	se	k3xPyFc3	se
Lom	lom	k1gInSc4	lom
-	-	kIx~	-
Sathon	Sathon	k1gMnSc1	Sathon
v	v	k7c6	v
obchodní	obchodní	k2eAgFnSc6d1	obchodní
čtvrti	čtvrt	k1gFnSc6	čtvrt
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
57,2	[number]	k4	57,2
ha	ha	kA	ha
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Thajce	Thajce	k1gMnPc4	Thajce
je	být	k5eAaImIp3nS	být
park	park	k1gInSc1	park
místem	místo	k1gNnSc7	místo
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
schovat	schovat	k5eAaPmF	schovat
před	před	k7c7	před
rušným	rušný	k2eAgInSc7d1	rušný
a	a	k8xC	a
uspěchaným	uspěchaný	k2eAgInSc7d1	uspěchaný
životem	život	k1gInSc7	život
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Ráno	ráno	k6eAd1	ráno
park	park	k1gInSc1	park
může	moct	k5eAaImIp3nS	moct
sloužit	sloužit	k5eAaImF	sloužit
jako	jako	k9	jako
sportovní	sportovní	k2eAgFnSc1d1	sportovní
aréna	aréna	k1gFnSc1	aréna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
využít	využít	k5eAaPmF	využít
2,5	[number]	k4	2,5
km	km	kA	km
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
běžeckou	běžecký	k2eAgFnSc4d1	běžecká
trať	trať	k1gFnSc4	trať
nebo	nebo	k8xC	nebo
večer	večer	k1gInSc4	večer
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
romantické	romantický	k2eAgNnSc4d1	romantické
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
můžete	moct	k5eAaImIp2nP	moct
pronajmout	pronajmout	k5eAaPmF	pronajmout
loďku	loďka	k1gFnSc4	loďka
a	a	k8xC	a
plout	plout	k5eAaImF	plout
na	na	k7c6	na
umělém	umělý	k2eAgNnSc6d1	umělé
jezeře	jezero	k1gNnSc6	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
park	park	k1gInSc1	park
navštívíte	navštívit	k5eAaPmIp2nP	navštívit
<g/>
,	,	kIx,	,
můžete	moct	k5eAaImIp2nP	moct
se	se	k3xPyFc4	se
zastavit	zastavit	k5eAaPmF	zastavit
i	i	k9	i
v	v	k7c6	v
hadí	hadí	k2eAgFnSc6d1	hadí
farmě	farma	k1gFnSc6	farma
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
Chulalongkornovy	Chulalongkornův	k2eAgFnSc2d1	Chulalongkornův
nemocnice	nemocnice	k1gFnSc2	nemocnice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
předvádí	předvádět	k5eAaImIp3nS	předvádět
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
odebírá	odebírat	k5eAaImIp3nS	odebírat
hadům	had	k1gMnPc3	had
jed	jed	k1gInSc4	jed
a	a	k8xC	a
jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
zacházet	zacházet	k5eAaImF	zacházet
<g/>
.	.	kIx.	.
</s>
<s>
Hřiště	hřiště	k1gNnSc1	hřiště
Bangkoku	Bangkok	k1gInSc2	Bangkok
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
nedaleko	nedaleko	k7c2	nedaleko
letiště	letiště	k1gNnSc2	letiště
Don	Don	k1gMnSc1	Don
Muang	Muang	k1gMnSc1	Muang
a	a	k8xC	a
zabírá	zabírat	k5eAaImIp3nS	zabírat
plochu	plocha	k1gFnSc4	plocha
70	[number]	k4	70
akrů	akr	k1gInPc2	akr
<g/>
.	.	kIx.	.
</s>
<s>
Najdete	najít	k5eAaPmIp2nP	najít
tu	tu	k6eAd1	tu
prakticky	prakticky	k6eAd1	prakticky
vše	všechen	k3xTgNnSc1	všechen
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
k	k	k7c3	k
zábavnímu	zábavní	k2eAgInSc3d1	zábavní
parku	park	k1gInSc3	park
patří	patřit	k5eAaImIp3nP	patřit
<g/>
.	.	kIx.	.
</s>
<s>
Horské	Horské	k2eAgFnPc1d1	Horské
dráhy	dráha	k1gFnPc1	dráha
<g/>
,	,	kIx,	,
miniatury	miniatura	k1gFnPc1	miniatura
slavných	slavný	k2eAgFnPc2d1	slavná
budov	budova	k1gFnPc2	budova
<g/>
,	,	kIx,	,
umělý	umělý	k2eAgInSc4d1	umělý
sníh	sníh	k1gInSc4	sníh
a	a	k8xC	a
mnoho	mnoho	k4c4	mnoho
dalšího	další	k2eAgNnSc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Připravte	připravit	k5eAaPmRp2nP	připravit
si	se	k3xPyFc3	se
však	však	k9	však
hodně	hodně	k6eAd1	hodně
peněz	peníze	k1gInPc2	peníze
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
řadě	řada	k1gFnSc3	řada
atrakcí	atrakce	k1gFnPc2	atrakce
prostě	prostě	k6eAd1	prostě
neodoláte	odolat	k5eNaPmIp2nP	odolat
<g/>
.	.	kIx.	.
</s>
<s>
Vstupné	vstupné	k1gNnSc1	vstupné
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
za	za	k7c2	za
případného	případný	k2eAgInSc2d1	případný
deště	dešť	k1gInSc2	dešť
delšího	dlouhý	k2eAgNnSc2d2	delší
než	než	k8xS	než
2	[number]	k4	2
hodiny	hodina	k1gFnPc4	hodina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Bangkoku	Bangkok	k1gInSc6	Bangkok
žije	žít	k5eAaImIp3nS	žít
8280925	[number]	k4	8280925
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
množství	množství	k1gNnSc1	množství
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
dojíždějí	dojíždět	k5eAaImIp3nP	dojíždět
do	do	k7c2	do
města	město	k1gNnSc2	město
ze	z	k7c2	z
sousedních	sousední	k2eAgFnPc2d1	sousední
provincií	provincie	k1gFnPc2	provincie
<g/>
,	,	kIx,	,
zvýší	zvýšit	k5eAaPmIp3nS	zvýšit
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
na	na	k7c4	na
14	[number]	k4	14
565	[number]	k4	565
547	[number]	k4	547
<g/>
.	.	kIx.	.
</s>
<s>
Bangkok	Bangkok	k1gInSc1	Bangkok
je	být	k5eAaImIp3nS	být
kosmopolitní	kosmopolitní	k2eAgNnSc1d1	kosmopolitní
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
sčítání	sčítání	k1gNnSc1	sčítání
lidu	lid	k1gInSc2	lid
prokázalo	prokázat	k5eAaPmAgNnS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
zde	zde	k6eAd1	zde
žije	žít	k5eAaImIp3nS	žít
81	[number]	k4	81
570	[number]	k4	570
Japonců	Japonec	k1gMnPc2	Japonec
a	a	k8xC	a
55	[number]	k4	55
893	[number]	k4	893
čínských	čínský	k2eAgMnPc2d1	čínský
státních	státní	k2eAgMnPc2d1	státní
příslušníků	příslušník	k1gMnPc2	příslušník
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
117	[number]	k4	117
071	[number]	k4	071
cizích	cizí	k2eAgMnPc2d1	cizí
státních	státní	k2eAgMnPc2d1	státní
příslušníků	příslušník	k1gMnPc2	příslušník
z	z	k7c2	z
jiných	jiný	k2eAgFnPc2d1	jiná
asijských	asijský	k2eAgFnPc2d1	asijská
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
48	[number]	k4	48
341	[number]	k4	341
z	z	k7c2	z
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
23	[number]	k4	23
418	[number]	k4	418
z	z	k7c2	z
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
5	[number]	k4	5
289	[number]	k4	289
z	z	k7c2	z
Austrálie	Austrálie	k1gFnSc2	Austrálie
a	a	k8xC	a
3	[number]	k4	3
022	[number]	k4	022
z	z	k7c2	z
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
přistěhovalců	přistěhovalec	k1gMnPc2	přistěhovalec
ze	z	k7c2	z
sousedních	sousední	k2eAgFnPc2d1	sousední
zemí	zem	k1gFnPc2	zem
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
303	[number]	k4	303
595	[number]	k4	595
z	z	k7c2	z
Barmy	Barma	k1gFnSc2	Barma
<g/>
,	,	kIx,	,
63	[number]	k4	63
438	[number]	k4	438
z	z	k7c2	z
Kambodži	Kambodža	k1gFnSc6	Kambodža
a	a	k8xC	a
18	[number]	k4	18
126	[number]	k4	126
obyvatel	obyvatel	k1gMnPc2	obyvatel
z	z	k7c2	z
Laosu	Laos	k1gInSc2	Laos
<g/>
.	.	kIx.	.
</s>
<s>
Sport	sport	k1gInSc1	sport
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
Thajce	Thajce	k1gMnSc4	Thajce
neodmyslitelnou	odmyslitelný	k2eNgFnSc7d1	neodmyslitelná
součástí	součást	k1gFnSc7	součást
každodenního	každodenní	k2eAgInSc2d1	každodenní
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Provozuje	provozovat	k5eAaImIp3nS	provozovat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
sportů	sport	k1gInPc2	sport
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
nejznámější	známý	k2eAgInSc1d3	nejznámější
je	být	k5eAaImIp3nS	být
zajisté	zajisté	k9	zajisté
thajský	thajský	k2eAgInSc4d1	thajský
box	box	k1gInSc4	box
<g/>
.	.	kIx.	.
</s>
<s>
Zápasy	zápas	k1gInPc1	zápas
se	se	k3xPyFc4	se
odehrávají	odehrávat	k5eAaImIp3nP	odehrávat
hlavně	hlavně	k9	hlavně
na	na	k7c6	na
stadionech	stadion	k1gInPc6	stadion
Rajadamnern	Rajadamnern	k1gMnSc1	Rajadamnern
a	a	k8xC	a
Lumpini	Lumpin	k2eAgMnPc1d1	Lumpin
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
pravidelně	pravidelně	k6eAd1	pravidelně
vysílány	vysílat	k5eAaImNgInP	vysílat
v	v	k7c6	v
televizi	televize	k1gFnSc6	televize
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
sportem	sport	k1gInSc7	sport
je	být	k5eAaImIp3nS	být
golf	golf	k1gInSc4	golf
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Thajsku	Thajsko	k1gNnSc6	Thajsko
je	být	k5eAaImIp3nS	být
několik	několik	k4yIc1	několik
profesionálních	profesionální	k2eAgNnPc2d1	profesionální
hřišť	hřiště	k1gNnPc2	hřiště
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
vznikají	vznikat	k5eAaImIp3nP	vznikat
nová	nový	k2eAgNnPc4d1	nové
a	a	k8xC	a
nová	nový	k2eAgNnPc4d1	nové
hřiště	hřiště	k1gNnPc4	hřiště
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
tradičním	tradiční	k2eAgInSc7d1	tradiční
sportem	sport	k1gInSc7	sport
je	být	k5eAaImIp3nS	být
pouštění	pouštění	k1gNnSc1	pouštění
draků	drak	k1gInPc2	drak
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc1	ten
má	mít	k5eAaImIp3nS	mít
svá	svůj	k3xOyFgNnPc4	svůj
pevné	pevný	k2eAgFnSc3d1	pevná
pravidla	pravidlo	k1gNnPc4	pravidlo
a	a	k8xC	a
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
spatřit	spatřit	k5eAaPmF	spatřit
neuvěřitelné	uvěřitelný	k2eNgFnPc4d1	neuvěřitelná
barvy	barva	k1gFnPc4	barva
a	a	k8xC	a
tvary	tvar	k1gInPc4	tvar
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
draků	drak	k1gInPc2	drak
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
v	v	k7c6	v
Thajsku	Thajsko	k1gNnSc6	Thajsko
velmi	velmi	k6eAd1	velmi
populárním	populární	k2eAgInSc7d1	populární
sportem	sport	k1gInSc7	sport
také	také	k9	také
fotbal	fotbal	k1gInSc4	fotbal
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc4d1	velká
sledovanost	sledovanost	k1gFnSc4	sledovanost
zde	zde	k6eAd1	zde
má	mít	k5eAaImIp3nS	mít
anglická	anglický	k2eAgFnSc1d1	anglická
fotbalová	fotbalový	k2eAgFnSc1d1	fotbalová
liga	liga	k1gFnSc1	liga
Premier	Premira	k1gFnPc2	Premira
League	Leagu	k1gInSc2	Leagu
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
12	[number]	k4	12
miliony	milion	k4xCgInPc7	milion
návštěvníky	návštěvník	k1gMnPc7	návštěvník
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Bangkok	Bangkok	k1gInSc1	Bangkok
3	[number]	k4	3
<g/>
.	.	kIx.	.
nejnavštěvovanějším	navštěvovaný	k2eAgNnSc7d3	nejnavštěvovanější
městem	město	k1gNnSc7	město
na	na	k7c6	na
světe	svět	k1gInSc5	svět
<g/>
.	.	kIx.	.
</s>
<s>
Domácí	domácí	k2eAgInSc1d1	domácí
cestovní	cestovní	k2eAgInSc1d1	cestovní
ruch	ruch	k1gInSc1	ruch
je	být	k5eAaImIp3nS	být
také	také	k9	také
prominentní	prominentní	k2eAgMnSc1d1	prominentní
<g/>
.	.	kIx.	.
</s>
<s>
Odbor	odbor	k1gInSc1	odbor
cestovního	cestovní	k2eAgInSc2d1	cestovní
ruchu	ruch	k1gInSc2	ruch
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
26	[number]	k4	26
861	[number]	k4	861
095	[number]	k4	095
thajských	thajský	k2eAgMnPc2d1	thajský
a	a	k8xC	a
11	[number]	k4	11
361	[number]	k4	361
808	[number]	k4	808
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
návštěvníků	návštěvník	k1gMnPc2	návštěvník
Bangkoku	Bangkok	k1gInSc2	Bangkok
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Bangkok	Bangkok	k1gInSc1	Bangkok
je	být	k5eAaImIp3nS	být
plný	plný	k2eAgInSc1d1	plný
památek	památka	k1gFnPc2	památka
<g/>
,	,	kIx,	,
zajímavostí	zajímavost	k1gFnPc2	zajímavost
a	a	k8xC	a
městský	městský	k2eAgInSc1d1	městský
život	život	k1gInSc1	život
se	se	k3xPyFc4	se
líbí	líbit	k5eAaImIp3nS	líbit
různým	různý	k2eAgFnPc3d1	různá
skupinám	skupina	k1gFnPc3	skupina
turistů	turist	k1gMnPc2	turist
<g/>
.	.	kIx.	.
</s>
<s>
Královské	královský	k2eAgInPc1d1	královský
paláce	palác	k1gInPc1	palác
a	a	k8xC	a
chrámy	chrám	k1gInPc1	chrám
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
několik	několik	k4yIc4	několik
muzeí	muzeum	k1gNnPc2	muzeum
představují	představovat	k5eAaImIp3nP	představovat
své	svůj	k3xOyFgFnPc4	svůj
významné	významný	k2eAgFnPc4d1	významná
historické	historický	k2eAgFnPc4d1	historická
a	a	k8xC	a
kulturní	kulturní	k2eAgFnPc4d1	kulturní
turistické	turistický	k2eAgFnPc4d1	turistická
atrakce	atrakce	k1gFnPc4	atrakce
<g/>
.	.	kIx.	.
</s>
<s>
Nákupní	nákupní	k2eAgInPc1d1	nákupní
a	a	k8xC	a
jídelní	jídelní	k2eAgInPc1d1	jídelní
zážitky	zážitek	k1gInPc1	zážitek
nabízejí	nabízet	k5eAaImIp3nP	nabízet
širokou	široký	k2eAgFnSc4d1	široká
škálu	škála	k1gFnSc4	škála
možností	možnost	k1gFnPc2	možnost
a	a	k8xC	a
cen	cena	k1gFnPc2	cena
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
také	také	k9	také
známé	známá	k1gFnPc4	známá
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
divoký	divoký	k2eAgInSc4d1	divoký
noční	noční	k2eAgInSc4d1	noční
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Bangkok	Bangkok	k1gInSc1	Bangkok
je	být	k5eAaImIp3nS	být
ekonomické	ekonomický	k2eAgNnSc4d1	ekonomické
centrum	centrum	k1gNnSc4	centrum
Thajska	Thajsko	k1gNnSc2	Thajsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
mělo	mít	k5eAaImAgNnS	mít
město	město	k1gNnSc1	město
ekonomický	ekonomický	k2eAgInSc1d1	ekonomický
výkon	výkon	k1gInSc4	výkon
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
1,3	[number]	k4	1,3
bilionu	bilion	k4xCgInSc2	bilion
bahtů	baht	k1gInPc2	baht
(	(	kIx(	(
<g/>
824	[number]	k4	824
mld.	mld.	k?	mld.
Kč	Kč	kA	Kč
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
činí	činit	k5eAaImIp3nS	činit
29,1	[number]	k4	29,1
<g/>
%	%	kIx~	%
z	z	k7c2	z
celého	celý	k2eAgNnSc2d1	celé
HDP	HDP	kA	HDP
Thajska	Thajsko	k1gNnSc2	Thajsko
<g/>
.	.	kIx.	.
</s>
<s>
HDP	HDP	kA	HDP
na	na	k7c4	na
jednoho	jeden	k4xCgMnSc4	jeden
obyvatele	obyvatel	k1gMnSc4	obyvatel
tedy	tedy	k9	tedy
bylo	být	k5eAaImAgNnS	být
160	[number]	k4	160
556	[number]	k4	556
bahtů	baht	k1gInPc2	baht
(	(	kIx(	(
<g/>
101	[number]	k4	101
868	[number]	k4	868
Kč	Kč	kA	Kč
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
trojnásobek	trojnásobek	k1gInSc4	trojnásobek
celostátního	celostátní	k2eAgInSc2d1	celostátní
průměru	průměr	k1gInSc2	průměr
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
převážně	převážně	k6eAd1	převážně
automobilové	automobilový	k2eAgFnSc2d1	automobilová
součástky	součástka	k1gFnSc2	součástka
<g/>
,	,	kIx,	,
zbraně	zbraň	k1gFnPc4	zbraň
a	a	k8xC	a
bytové	bytový	k2eAgInPc4d1	bytový
doplňky	doplněk	k1gInPc4	doplněk
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInPc1d1	hlavní
příjmy	příjem	k1gInPc1	příjem
však	však	k9	však
tvoří	tvořit	k5eAaImIp3nP	tvořit
cestovní	cestovní	k2eAgInSc4d1	cestovní
ruch	ruch	k1gInSc4	ruch
s	s	k7c7	s
rostoucím	rostoucí	k2eAgInSc7d1	rostoucí
počtem	počet	k1gInSc7	počet
návštěvníků	návštěvník	k1gMnPc2	návštěvník
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
třetina	třetina	k1gFnSc1	třetina
všech	všecek	k3xTgFnPc2	všecek
bank	banka	k1gFnPc2	banka
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
Bangkoku	Bangkok	k1gInSc6	Bangkok
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
bangkokské	bangkokský	k2eAgInPc1d1	bangkokský
kanály	kanál	k1gInPc1	kanál
dříve	dříve	k6eAd2	dříve
sloužily	sloužit	k5eAaImAgInP	sloužit
jako	jako	k8xS	jako
hlavní	hlavní	k2eAgInPc1d1	hlavní
způsob	způsob	k1gInSc1	způsob
dopravy	doprava	k1gFnSc2	doprava
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
jsou	být	k5eAaImIp3nP	být
už	už	k6eAd1	už
dávno	dávno	k6eAd1	dávno
překonány	překonat	k5eAaPmNgInP	překonat
v	v	k7c6	v
důležitosti	důležitost	k1gFnSc6	důležitost
provozu	provoz	k1gInSc2	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Charoen	Charoen	k2eAgMnSc1d1	Charoen
Krung	Krung	k1gMnSc1	Krung
Road	Road	k1gMnSc1	Road
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
silnice	silnice	k1gFnSc1	silnice
západního	západní	k2eAgInSc2d1	západní
typu	typ	k1gInSc2	typ
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
dokončena	dokončen	k2eAgFnSc1d1	dokončena
roku	rok	k1gInSc2	rok
1864	[number]	k4	1864
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
silniční	silniční	k2eAgFnSc1d1	silniční
síť	síť	k1gFnSc1	síť
velice	velice	k6eAd1	velice
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
silnice	silnice	k1gFnPc1	silnice
nestačily	stačit	k5eNaBmAgFnP	stačit
na	na	k7c4	na
rychlý	rychlý	k2eAgInSc4d1	rychlý
růst	růst	k1gInSc4	růst
města	město	k1gNnSc2	město
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
sužují	sužovat	k5eAaImIp3nP	sužovat
město	město	k1gNnSc4	město
dopravní	dopravní	k2eAgFnSc2d1	dopravní
zácpy	zácpa	k1gFnSc2	zácpa
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
železniční	železniční	k2eAgFnSc1d1	železniční
doprava	doprava	k1gFnSc1	doprava
byla	být	k5eAaImAgFnS	být
zavedena	zavést	k5eAaPmNgFnS	zavést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1893	[number]	k4	1893
a	a	k8xC	a
elektrické	elektrický	k2eAgFnPc1d1	elektrická
tramvaje	tramvaj	k1gFnPc1	tramvaj
sloužily	sloužit	k5eAaImAgFnP	sloužit
městu	město	k1gNnSc3	město
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1894	[number]	k4	1894
<g/>
-	-	kIx~	-
<g/>
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgInS	začít
první	první	k4xOgInSc1	první
systém	systém	k1gInSc1	systém
rychlé	rychlý	k2eAgFnSc2d1	rychlá
přepravy	přeprava	k1gFnSc2	přeprava
fungovat	fungovat	k5eAaImF	fungovat
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
Starší	starý	k2eAgInPc1d2	starší
systémy	systém	k1gInPc1	systém
veřejné	veřejný	k2eAgFnSc2d1	veřejná
dopravy	doprava	k1gFnSc2	doprava
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
autobusovou	autobusový	k2eAgFnSc4d1	autobusová
síť	síť	k1gFnSc4	síť
a	a	k8xC	a
lodní	lodní	k2eAgFnPc4d1	lodní
služby	služba	k1gFnPc4	služba
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
stále	stále	k6eAd1	stále
fungují	fungovat	k5eAaImIp3nP	fungovat
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Menam-Čao-Praja	Menam-Čao-Praj	k1gInSc2	Menam-Čao-Praj
<g/>
.	.	kIx.	.
</s>
<s>
Taxi	taxi	k1gNnSc1	taxi
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
automobilů	automobil	k1gInPc2	automobil
<g/>
,	,	kIx,	,
motocyklů	motocykl	k1gInPc2	motocykl
<g/>
,	,	kIx,	,
a	a	k8xC	a
tuk-tuků	tukuk	k1gInPc2	tuk-tuk
<g/>
.	.	kIx.	.
</s>
<s>
Bangkok	Bangkok	k1gInSc1	Bangkok
je	být	k5eAaImIp3nS	být
spojen	spojit	k5eAaPmNgInS	spojit
se	s	k7c7	s
zbytkem	zbytek	k1gInSc7	zbytek
země	zem	k1gFnSc2	zem
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
národních	národní	k2eAgInPc2d1	národní
dálnici	dálnice	k1gFnSc4	dálnice
a	a	k8xC	a
železniční	železniční	k2eAgFnPc4d1	železniční
sítě	síť	k1gFnPc4	síť
<g/>
,	,	kIx,	,
či	či	k8xC	či
vnitrostátními	vnitrostátní	k2eAgNnPc7d1	vnitrostátní
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Staletá	staletý	k2eAgFnSc1d1	staletá
námořní	námořní	k2eAgFnSc1d1	námořní
doprava	doprava	k1gFnSc1	doprava
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
v	v	k7c6	v
přístavu	přístav	k1gInSc6	přístav
Khlong	Khlonga	k1gFnPc2	Khlonga
Toei	Toe	k1gFnSc2	Toe
<g/>
.	.	kIx.	.
</s>
<s>
Čtyřicet	čtyřicet	k4xCc4	čtyřicet
osm	osm	k4xCc4	osm
hlavních	hlavní	k2eAgFnPc2d1	hlavní
silnic	silnice	k1gFnPc2	silnice
spojuje	spojovat	k5eAaImIp3nS	spojovat
různé	různý	k2eAgFnPc4d1	různá
oblasti	oblast	k1gFnPc4	oblast
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
břehy	břeh	k1gInPc1	břeh
spoje	spoj	k1gInSc2	spoj
jedenáct	jedenáct	k4xCc4	jedenáct
mostů	most	k1gInPc2	most
přes	přes	k7c4	přes
řeku	řeka	k1gFnSc4	řeka
Čao-Praja	Čao-Praj	k1gInSc2	Čao-Praj
<g/>
.	.	kIx.	.
</s>
<s>
Rychlý	rychlý	k2eAgInSc1d1	rychlý
růst	růst	k1gInSc1	růst
města	město	k1gNnSc2	město
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
vyústil	vyústit	k5eAaPmAgMnS	vyústit
ke	k	k7c3	k
zvýšení	zvýšení	k1gNnSc3	zvýšení
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
osobních	osobní	k2eAgNnPc2d1	osobní
vozidel	vozidlo	k1gNnPc2	vozidlo
a	a	k8xC	a
dopravní	dopravní	k2eAgFnSc2d1	dopravní
poptávky	poptávka	k1gFnSc2	poptávka
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
ochromující	ochromující	k2eAgFnSc2d1	ochromující
dopravní	dopravní	k2eAgFnSc2d1	dopravní
zácpy	zácpa	k1gFnSc2	zácpa
<g/>
,	,	kIx,	,
evidentní	evidentní	k2eAgFnSc1d1	evidentní
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Situace	situace	k1gFnSc1	situace
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
vážná	vážný	k2eAgFnSc1d1	vážná
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
důstojníci	důstojník	k1gMnPc1	důstojník
dopravní	dopravní	k2eAgFnSc2d1	dopravní
policie	policie	k1gFnSc2	policie
cvičili	cvičit	k5eAaImAgMnP	cvičit
v	v	k7c6	v
základech	základ	k1gInPc6	základ
porodnictví	porodnictví	k1gNnPc2	porodnictví
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
vystavěli	vystavět	k5eAaPmAgMnP	vystavět
230	[number]	k4	230
km	km	kA	km
cyklostezek	cyklostezka	k1gFnPc2	cyklostezka
podél	podél	k7c2	podél
silnic	silnice	k1gFnPc2	silnice
<g/>
,	,	kIx,	,
kolo	kolo	k1gNnSc1	kolo
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
dost	dost	k6eAd1	dost
nepraktické	praktický	k2eNgNnSc1d1	nepraktické
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ji	on	k3xPp3gFnSc4	on
většinou	většinou	k6eAd1	většinou
sdílí	sdílet	k5eAaImIp3nS	sdílet
s	s	k7c7	s
chodci	chodec	k1gMnPc7	chodec
<g/>
.	.	kIx.	.
</s>
<s>
Bangkok	Bangkok	k1gInSc1	Bangkok
má	mít	k5eAaImIp3nS	mít
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
autobusovou	autobusový	k2eAgFnSc4d1	autobusová
síť	síť	k1gFnSc4	síť
poskytující	poskytující	k2eAgFnSc2d1	poskytující
místní	místní	k2eAgFnSc2d1	místní
tranzitní	tranzitní	k2eAgFnSc2d1	tranzitní
služby	služba	k1gFnSc2	služba
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Velkého	velký	k2eAgInSc2d1	velký
Bangkoku	Bangkok	k1gInSc2	Bangkok
<g/>
.	.	kIx.	.
</s>
<s>
Bangkok	Bangkok	k1gInSc1	Bangkok
Mass	Mass	k1gInSc1	Mass
Transit	transit	k1gInSc4	transit
Authority	Authorita	k1gFnSc2	Authorita
(	(	kIx(	(
<g/>
BMTA	BMTA	kA	BMTA
<g/>
)	)	kIx)	)
provozuje	provozovat	k5eAaImIp3nS	provozovat
monopol	monopol	k1gInSc4	monopol
na	na	k7c4	na
autobusovou	autobusový	k2eAgFnSc4d1	autobusová
dopravu	doprava	k1gFnSc4	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Dohromady	dohromady	k6eAd1	dohromady
má	mít	k5eAaImIp3nS	mít
BMTA	BMTA	kA	BMTA
3	[number]	k4	3
506	[number]	k4	506
autobusů	autobus	k1gInPc2	autobus
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ostatními	ostatní	k2eAgInPc7d1	ostatní
soukromými	soukromý	k2eAgInPc7d1	soukromý
autobusy	autobus	k1gInPc7	autobus
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Bangkoku	Bangkok	k1gInSc6	Bangkok
celkem	celkem	k6eAd1	celkem
16	[number]	k4	16
321	[number]	k4	321
autobusů	autobus	k1gInPc2	autobus
<g/>
.	.	kIx.	.
</s>
<s>
Působí	působit	k5eAaImIp3nS	působit
na	na	k7c6	na
470	[number]	k4	470
linkách	linka	k1gFnPc6	linka
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
regionu	region	k1gInSc6	region
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
stále	stále	k6eAd1	stále
spousty	spousta	k1gFnPc1	spousta
lidí	člověk	k1gMnPc2	člověk
denně	denně	k6eAd1	denně
dojíždějí	dojíždět	k5eAaImIp3nP	dojíždět
<g/>
,	,	kIx,	,
počet	počet	k1gInSc1	počet
cestujících	cestující	k1gMnPc2	cestující
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
bylo	být	k5eAaImAgNnS	být
hlášeno	hlásit	k5eAaImNgNnS	hlásit
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
1	[number]	k4	1
milion	milion	k4xCgInSc4	milion
přepravených	přepravený	k2eAgMnPc2d1	přepravený
lidí	člověk	k1gMnPc2	člověk
denně	denně	k6eAd1	denně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
čtvrtina	čtvrtina	k1gFnSc1	čtvrtina
z	z	k7c2	z
počtu	počet	k1gInSc2	počet
přepravených	přepravený	k2eAgMnPc2d1	přepravený
lidí	člověk	k1gMnPc2	člověk
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Taxíky	taxík	k1gInPc1	taxík
jsou	být	k5eAaImIp3nP	být
všudypřítomné	všudypřítomný	k2eAgInPc1d1	všudypřítomný
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
populární	populární	k2eAgInSc1d1	populární
způsob	způsob	k1gInSc1	způsob
přepravy	přeprava	k1gFnSc2	přeprava
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
srpna	srpen	k1gInSc2	srpen
2012	[number]	k4	2012
je	být	k5eAaImIp3nS	být
106	[number]	k4	106
050	[number]	k4	050
vozů	vůz	k1gInPc2	vůz
<g/>
,	,	kIx,	,
58	[number]	k4	58
276	[number]	k4	276
motocyklů	motocykl	k1gInPc2	motocykl
a	a	k8xC	a
8	[number]	k4	8
996	[number]	k4	996
tuk-tuků	tukuk	k1gInPc2	tuk-tuk
(	(	kIx(	(
<g/>
motorizované	motorizovaný	k2eAgFnPc1d1	motorizovaná
tříkolky	tříkolka	k1gFnPc1	tříkolka
<g/>
)	)	kIx)	)
registrovaných	registrovaný	k2eAgFnPc2d1	registrovaná
pro	pro	k7c4	pro
použití	použití	k1gNnSc4	použití
jako	jako	k8xC	jako
taxi	taxi	k1gNnSc4	taxi
<g/>
.	.	kIx.	.
</s>
<s>
Motocykly	motocykl	k1gInPc1	motocykl
jsou	být	k5eAaImIp3nP	být
převážně	převážně	k6eAd1	převážně
používány	používat	k5eAaImNgFnP	používat
na	na	k7c4	na
kratší	krátký	k2eAgFnPc4d2	kratší
trasy	trasa	k1gFnPc4	trasa
a	a	k8xC	a
tuk-tuky	tukuk	k1gInPc4	tuk-tuk
jako	jako	k8xS	jako
atrakce	atrakce	k1gFnPc4	atrakce
pro	pro	k7c4	pro
turisty	turist	k1gMnPc4	turist
<g/>
.	.	kIx.	.
</s>
<s>
Bangkok	Bangkok	k1gInSc1	Bangkok
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejrušnějších	rušný	k2eAgInPc2d3	nejrušnější
uzlů	uzel	k1gInPc2	uzel
asijských	asijský	k2eAgInPc2d1	asijský
letecké	letecký	k2eAgFnPc4d1	letecká
dopravy	doprava	k1gFnSc2	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgNnPc4	dva
komerční	komerční	k2eAgNnPc4d1	komerční
letiště	letiště	k1gNnPc4	letiště
slouží	sloužit	k5eAaImIp3nP	sloužit
městu	město	k1gNnSc3	město
<g/>
,	,	kIx,	,
starší	starý	k2eAgMnSc1d2	starší
Don	Don	k1gMnSc1	Don
Mueang	Mueang	k1gMnSc1	Mueang
International	International	k1gMnSc1	International
Airport	Airport	k1gInSc1	Airport
a	a	k8xC	a
nové	nový	k2eAgInPc1d1	nový
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
letiště	letiště	k1gNnSc2	letiště
<g/>
,	,	kIx,	,
obyčejně	obyčejně	k6eAd1	obyčejně
známé	známý	k2eAgInPc1d1	známý
jako	jako	k9	jako
Suvarnabhumi	Suvarnabhu	k1gFnPc7	Suvarnabhu
<g/>
.	.	kIx.	.
</s>
<s>
Letiště	letiště	k1gNnSc1	letiště
Suvarnabhumi	Suvarnabhu	k1gFnPc7	Suvarnabhu
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
nahradilo	nahradit	k5eAaPmAgNnS	nahradit
Don	Don	k1gInSc4	Don
Mueang	Mueanga	k1gFnPc2	Mueanga
jako	jako	k8xS	jako
hlavní	hlavní	k2eAgNnSc4d1	hlavní
letiště	letiště	k1gNnSc4	letiště
v	v	k7c6	v
Bangkoku	Bangkok	k1gInSc6	Bangkok
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
odbavilo	odbavit	k5eAaPmAgNnS	odbavit
47	[number]	k4	47
910	[number]	k4	910
744	[number]	k4	744
cestujících	cestující	k1gMnPc2	cestující
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
jej	on	k3xPp3gMnSc4	on
dělá	dělat	k5eAaImIp3nS	dělat
16	[number]	k4	16
<g/>
.	.	kIx.	.
nejzaměstanějším	zaměstaný	k2eAgNnSc7d3	zaměstaný
letištěm	letiště	k1gNnSc7	letiště
na	na	k7c6	na
světě	svět	k1gInSc6	svět
a	a	k8xC	a
5	[number]	k4	5
<g/>
.	.	kIx.	.
v	v	k7c6	v
asijsko-pacifickém	asijskoacifický	k2eAgInSc6d1	asijsko-pacifický
regionu	region	k1gInSc6	region
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
<g/>
,	,	kIx,	,
toto	tento	k3xDgNnSc1	tento
množství	množství	k1gNnSc1	množství
provozu	provoz	k1gInSc2	provoz
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
nad	nad	k7c7	nad
jeho	jeho	k3xOp3gFnSc7	jeho
projektovanou	projektovaný	k2eAgFnSc7d1	projektovaná
kapacitou	kapacita	k1gFnSc7	kapacita
45	[number]	k4	45
milionů	milion	k4xCgInPc2	milion
cestujících	cestující	k1gMnPc2	cestující
<g/>
.	.	kIx.	.
</s>
<s>
Don	Don	k1gMnSc1	Don
Mueang	Mueang	k1gMnSc1	Mueang
byl	být	k5eAaImAgMnS	být
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
znovu	znovu	k6eAd1	znovu
otevřen	otevřít	k5eAaPmNgInS	otevřít
pro	pro	k7c4	pro
vnitrostátní	vnitrostátní	k2eAgInPc4d1	vnitrostátní
lety	let	k1gInPc4	let
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
Suvarnabhumi	Suvarnabhu	k1gFnPc7	Suvarnabhu
prochází	procházet	k5eAaImIp3nS	procházet
expanzí	expanze	k1gFnSc7	expanze
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
zvýšit	zvýšit	k5eAaPmF	zvýšit
jeho	jeho	k3xOp3gFnSc4	jeho
kapacitu	kapacita	k1gFnSc4	kapacita
na	na	k7c4	na
60	[number]	k4	60
milionů	milion	k4xCgInPc2	milion
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
očekává	očekávat	k5eAaImIp3nS	očekávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
dokončeno	dokončit	k5eAaPmNgNnS	dokončit
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
BTS	BTS	kA	BTS
Skytrain	Skytrain	k1gInSc1	Skytrain
je	být	k5eAaImIp3nS	být
nejjednodušší	jednoduchý	k2eAgInSc1d3	nejjednodušší
a	a	k8xC	a
také	také	k9	také
nejrychlejší	rychlý	k2eAgInSc4d3	nejrychlejší
dopravní	dopravní	k2eAgInSc4d1	dopravní
prostředek	prostředek	k1gInSc4	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Nadzemní	nadzemní	k2eAgFnSc1d1	nadzemní
rychlodráha	rychlodráha	k1gFnSc1	rychlodráha
byla	být	k5eAaImAgFnS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
jsou	být	k5eAaImIp3nP	být
2	[number]	k4	2
linky	linka	k1gFnPc1	linka
a	a	k8xC	a
přestoupit	přestoupit	k5eAaPmF	přestoupit
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
ve	v	k7c6	v
stanici	stanice	k1gFnSc6	stanice
Siam	Siam	k1gInSc4	Siam
<g/>
.	.	kIx.	.
</s>
<s>
Provozní	provozní	k2eAgFnSc1d1	provozní
doba	doba	k1gFnSc1	doba
je	být	k5eAaImIp3nS	být
denně	denně	k6eAd1	denně
od	od	k7c2	od
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
do	do	k7c2	do
24	[number]	k4	24
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivou	jednotlivý	k2eAgFnSc4d1	jednotlivá
jízdenku	jízdenka	k1gFnSc4	jízdenka
si	se	k3xPyFc3	se
můžete	moct	k5eAaImIp2nP	moct
koupit	koupit	k5eAaPmF	koupit
v	v	k7c6	v
některém	některý	k3yIgInSc6	některý
z	z	k7c2	z
automatů	automat	k1gInPc2	automat
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
jízdenka	jízdenka	k1gFnSc1	jízdenka
je	být	k5eAaImIp3nS	být
platná	platný	k2eAgFnSc1d1	platná
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
jednu	jeden	k4xCgFnSc4	jeden
cestu	cesta	k1gFnSc4	cesta
s	s	k7c7	s
jízdným	jízdné	k1gNnSc7	jízdné
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
si	se	k3xPyFc3	se
zvolíte	zvolit	k5eAaPmIp2nP	zvolit
<g/>
.	.	kIx.	.
</s>
<s>
Cena	cena	k1gFnSc1	cena
jednotlivé	jednotlivý	k2eAgFnSc2d1	jednotlivá
jízdy	jízda	k1gFnSc2	jízda
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
od	od	k7c2	od
15	[number]	k4	15
do	do	k7c2	do
40	[number]	k4	40
THB	THB	kA	THB
<g/>
.	.	kIx.	.
</s>
<s>
Bangkok	Bangkok	k1gInSc1	Bangkok
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
centrem	centrum	k1gNnSc7	centrum
moderního	moderní	k2eAgNnSc2d1	moderní
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
v	v	k7c6	v
Thajsku	Thajsko	k1gNnSc6	Thajsko
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
školy	škola	k1gFnPc1	škola
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
byly	být	k5eAaImAgFnP	být
založeny	založit	k5eAaPmNgFnP	založit
zde	zde	k6eAd1	zde
na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
má	mít	k5eAaImIp3nS	mít
5	[number]	k4	5
nejstarších	starý	k2eAgFnPc2d3	nejstarší
univerzit	univerzita	k1gFnPc2	univerzita
<g/>
:	:	kIx,	:
Chulalongkorn	Chulalongkorna	k1gFnPc2	Chulalongkorna
<g/>
,	,	kIx,	,
Thammasat	Thammasat	k1gFnPc2	Thammasat
<g/>
,	,	kIx,	,
Kasetsart	Kasetsarta	k1gFnPc2	Kasetsarta
<g/>
,	,	kIx,	,
Mahidol	Mahidola	k1gFnPc2	Mahidola
a	a	k8xC	a
Silpakorn	Silpakorna	k1gFnPc2	Silpakorna
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
založeny	založit	k5eAaPmNgFnP	založit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1917	[number]	k4	1917
až	až	k9	až
1943	[number]	k4	1943
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
dominanci	dominance	k1gFnSc6	dominance
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
vysokoškolského	vysokoškolský	k2eAgNnSc2d1	vysokoškolské
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
univerzit	univerzita	k1gFnPc2	univerzita
<g/>
,	,	kIx,	,
veřejných	veřejný	k2eAgFnPc2d1	veřejná
i	i	k8xC	i
soukromých	soukromý	k2eAgFnPc2d1	soukromá
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
Bangkoku	Bangkok	k1gInSc6	Bangkok
nebo	nebo	k8xC	nebo
v	v	k7c6	v
metropolitní	metropolitní	k2eAgFnSc6d1	metropolitní
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Chulalongkorn	Chulalongkorn	k1gInSc1	Chulalongkorn
a	a	k8xC	a
Mahidol	Mahidol	k1gInSc1	Mahidol
jsou	být	k5eAaImIp3nP	být
pouze	pouze	k6eAd1	pouze
thajské	thajský	k2eAgFnPc1d1	thajská
univerzity	univerzita	k1gFnPc1	univerzita
a	a	k8xC	a
objevily	objevit	k5eAaPmAgFnP	objevit
se	se	k3xPyFc4	se
v	v	k7c6	v
top	topit	k5eAaImRp2nS	topit
500	[number]	k4	500
Times	Timesa	k1gFnPc2	Timesa
Higher	Highra	k1gFnPc2	Highra
Education	Education	k1gInSc1	Education
a	a	k8xC	a
QS	QS	kA	QS
World	World	k1gInSc4	World
University	universita	k1gFnSc2	universita
Rankings	Rankingsa	k1gFnPc2	Rankingsa
<g/>
.	.	kIx.	.
</s>
<s>
Poptávka	poptávka	k1gFnSc1	poptávka
po	po	k7c6	po
vysokoškolském	vysokoškolský	k2eAgNnSc6d1	vysokoškolské
vzdělávání	vzdělávání	k1gNnSc6	vzdělávání
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
založení	založení	k1gNnSc2	založení
mnoha	mnoho	k4c2	mnoho
dalších	další	k2eAgFnPc2d1	další
univerzit	univerzita	k1gFnPc2	univerzita
a	a	k8xC	a
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
veřejných	veřejný	k2eAgFnPc6d1	veřejná
i	i	k8xC	i
soukromých	soukromý	k2eAgFnPc6d1	soukromá
<g/>
.	.	kIx.	.
</s>
<s>
Hodně	hodně	k6eAd1	hodně
zdravotnických	zdravotnický	k2eAgInPc2d1	zdravotnický
prostředků	prostředek	k1gInPc2	prostředek
v	v	k7c6	v
Thajsku	Thajsko	k1gNnSc6	Thajsko
je	být	k5eAaImIp3nS	být
neúměrně	úměrně	k6eNd1	úměrně
soustředěno	soustředit	k5eAaPmNgNnS	soustředit
v	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
má	mít	k5eAaImIp3nS	mít
42	[number]	k4	42
veřejných	veřejný	k2eAgFnPc2d1	veřejná
nemocnic	nemocnice	k1gFnPc2	nemocnice
<g/>
,	,	kIx,	,
5	[number]	k4	5
univerzitních	univerzitní	k2eAgFnPc2d1	univerzitní
nemocnic	nemocnice	k1gFnPc2	nemocnice
<g/>
,	,	kIx,	,
98	[number]	k4	98
soukromých	soukromý	k2eAgFnPc2d1	soukromá
nemocnic	nemocnice	k1gFnPc2	nemocnice
a	a	k8xC	a
4	[number]	k4	4
063	[number]	k4	063
registrovaných	registrovaný	k2eAgFnPc2d1	registrovaná
klinik	klinika	k1gFnPc2	klinika
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
nemocnic	nemocnice	k1gFnPc2	nemocnice
v	v	k7c6	v
Bangkoku	Bangkok	k1gInSc6	Bangkok
jezdí	jezdit	k5eAaImIp3nS	jezdit
mnoho	mnoho	k4c4	mnoho
cizinců	cizinec	k1gMnPc2	cizinec
kvůli	kvůli	k7c3	kvůli
levnějším	levný	k2eAgFnPc3d2	levnější
službám	služba	k1gFnPc3	služba
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
navštívilo	navštívit	k5eAaPmAgNnS	navštívit
Bangkok	Bangkok	k1gInSc4	Bangkok
200	[number]	k4	200
000	[number]	k4	000
zdravotních	zdravotní	k2eAgMnPc2d1	zdravotní
turistů	turist	k1gMnPc2	turist
<g/>
.	.	kIx.	.
</s>
<s>
Bangkok	Bangkok	k1gInSc1	Bangkok
má	mít	k5eAaImIp3nS	mít
relativně	relativně	k6eAd1	relativně
mírnou	mírný	k2eAgFnSc4d1	mírná
kriminalitu	kriminalita	k1gFnSc4	kriminalita
<g/>
,	,	kIx,	,
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
městskými	městský	k2eAgInPc7d1	městský
protějšky	protějšek	k1gInPc7	protějšek
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Dopravní	dopravní	k2eAgFnPc1d1	dopravní
nehody	nehoda	k1gFnPc1	nehoda
jsou	být	k5eAaImIp3nP	být
hlavní	hlavní	k2eAgNnSc4d1	hlavní
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
přírodní	přírodní	k2eAgFnPc1d1	přírodní
katastrofy	katastrofa	k1gFnPc1	katastrofa
jsou	být	k5eAaImIp3nP	být
vzácné	vzácný	k2eAgInPc1d1	vzácný
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
hrozba	hrozba	k1gFnSc1	hrozba
nebezpečí	nebezpečí	k1gNnSc2	nebezpečí
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Bangkoku	Bangkok	k1gInSc6	Bangkok
poměrně	poměrně	k6eAd1	poměrně
nízká	nízký	k2eAgFnSc1d1	nízká
<g/>
,	,	kIx,	,
lidé	člověk	k1gMnPc1	člověk
si	se	k3xPyFc3	se
musí	muset	k5eAaImIp3nP	muset
dávat	dávat	k5eAaImF	dávat
pozor	pozor	k1gInSc4	pozor
na	na	k7c4	na
kapsáře	kapsář	k1gInPc4	kapsář
a	a	k8xC	a
podvody	podvod	k1gInPc4	podvod
s	s	k7c7	s
kreditními	kreditní	k2eAgFnPc7d1	kreditní
kartami	karta	k1gFnPc7	karta
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Bangkoku	Bangkok	k1gInSc6	Bangkok
kriminalita	kriminalita	k1gFnSc1	kriminalita
zhruba	zhruba	k6eAd1	zhruba
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
ve	v	k7c6	v
zbytku	zbytek	k1gInSc6	zbytek
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
vzrostly	vzrůst	k5eAaPmAgFnP	vzrůst
krádeže	krádež	k1gFnPc1	krádež
motorových	motorový	k2eAgNnPc2d1	motorové
vozidel	vozidlo	k1gNnPc2	vozidlo
a	a	k8xC	a
organizované	organizovaný	k2eAgFnSc2d1	organizovaná
trestné	trestný	k2eAgFnSc2d1	trestná
činnosti	činnost	k1gFnSc2	činnost
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
od	od	k7c2	od
zahraničních	zahraniční	k2eAgInPc2d1	zahraniční
gangů	gang	k1gInPc2	gang
<g/>
.	.	kIx.	.
</s>
<s>
Obchod	obchod	k1gInSc1	obchod
s	s	k7c7	s
drogami	droga	k1gFnPc7	droga
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
s	s	k7c7	s
metamfetaminovými	metamfetaminův	k2eAgFnPc7d1	metamfetaminův
pilulkami	pilulka	k1gFnPc7	pilulka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
také	také	k9	také
chronický	chronický	k2eAgInSc1d1	chronický
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastější	častý	k2eAgInSc1d3	nejčastější
případ	případ	k1gInSc1	případ
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
hlášený	hlášený	k2eAgInSc1d1	hlášený
policii	policie	k1gFnSc4	policie
bylo	být	k5eAaImAgNnS	být
vloupání	vloupání	k1gNnSc4	vloupání
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ve	v	k7c4	v
12	[number]	k4	12
347	[number]	k4	347
případech	případ	k1gInPc6	případ
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
krádeže	krádež	k1gFnPc1	krádež
motocyklů	motocykl	k1gInPc2	motocykl
s	s	k7c7	s
5	[number]	k4	5
504	[number]	k4	504
případy	případ	k1gInPc7	případ
<g/>
,	,	kIx,	,
3	[number]	k4	3
694	[number]	k4	694
případů	případ	k1gInPc2	případ
napadení	napadení	k1gNnPc2	napadení
a	a	k8xC	a
2	[number]	k4	2
836	[number]	k4	836
případů	případ	k1gInPc2	případ
zpronevěry	zpronevěra	k1gFnSc2	zpronevěra
<g/>
.	.	kIx.	.
</s>
<s>
Závažné	závažný	k2eAgInPc1d1	závažný
trestné	trestný	k2eAgInPc1d1	trestný
činy	čin	k1gInPc1	čin
<g/>
:	:	kIx,	:
183	[number]	k4	183
vražd	vražda	k1gFnPc2	vražda
<g/>
,	,	kIx,	,
81	[number]	k4	81
loupeží	loupež	k1gFnPc2	loupež
gangu	gang	k1gInSc2	gang
<g/>
,	,	kIx,	,
265	[number]	k4	265
loupeží	loupež	k1gFnPc2	loupež
<g/>
,	,	kIx,	,
1	[number]	k4	1
únos	únos	k1gInSc4	únos
a	a	k8xC	a
9	[number]	k4	9
případů	případ	k1gInPc2	případ
žhářství	žhářství	k1gNnSc2	žhářství
<g/>
.	.	kIx.	.
</s>
<s>
Trestné	trestný	k2eAgInPc1d1	trestný
činy	čin	k1gInPc1	čin
proti	proti	k7c3	proti
státu	stát	k1gInSc3	stát
byly	být	k5eAaImAgInP	být
mnohem	mnohem	k6eAd1	mnohem
častější	častý	k2eAgInPc1d2	častější
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
54	[number]	k4	54
068	[number]	k4	068
případů	případ	k1gInPc2	případ
souvisejících	související	k2eAgInPc2d1	související
s	s	k7c7	s
drogami	droga	k1gFnPc7	droga
<g/>
,	,	kIx,	,
17	[number]	k4	17
239	[number]	k4	239
případů	případ	k1gInPc2	případ
zahrnujících	zahrnující	k2eAgInPc2d1	zahrnující
prostituci	prostituce	k1gFnSc4	prostituce
a	a	k8xC	a
8	[number]	k4	8
634	[number]	k4	634
týkající	týkající	k2eAgNnPc1d1	týkající
se	se	k3xPyFc4	se
hazardních	hazardní	k2eAgFnPc2d1	hazardní
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Statistika	statistika	k1gFnSc1	statistika
dopravních	dopravní	k2eAgFnPc2d1	dopravní
nehod	nehoda	k1gFnPc2	nehoda
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
<g/>
:	:	kIx,	:
37	[number]	k4	37
985	[number]	k4	985
nehod	nehoda	k1gFnPc2	nehoda
<g/>
,	,	kIx,	,
16	[number]	k4	16
602	[number]	k4	602
zraněných	zraněný	k2eAgMnPc2d1	zraněný
a	a	k8xC	a
456	[number]	k4	456
úmrtí	úmrtí	k1gNnPc2	úmrtí
<g/>
.	.	kIx.	.
</s>
<s>
Celkové	celkový	k2eAgFnPc1d1	celková
škody	škoda	k1gFnPc1	škoda
byly	být	k5eAaImAgFnP	být
426	[number]	k4	426
420	[number]	k4	420
000	[number]	k4	000
bathů	bath	k1gInPc2	bath
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
<g/>
,	,	kIx,	,
míra	míra	k1gFnSc1	míra
smrtelných	smrtelný	k2eAgInPc2d1	smrtelný
úrazů	úraz	k1gInPc2	úraz
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
nižší	nízký	k2eAgFnSc1d2	nižší
<g/>
,	,	kIx,	,
než	než	k8xS	než
ve	v	k7c6	v
zbytku	zbytek	k1gInSc6	zbytek
Thajska	Thajsko	k1gNnSc2	Thajsko
<g/>
.	.	kIx.	.
</s>
<s>
Západ	západ	k1gInSc1	západ
ho	on	k3xPp3gMnSc4	on
zná	znát	k5eAaImIp3nS	znát
jako	jako	k9	jako
Hotel	hotel	k1gInSc1	hotel
Bangkok	Bangkok	k1gInSc1	Bangkok
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejhorší	zlý	k2eAgNnSc4d3	nejhorší
vězení	vězení	k1gNnSc4	vězení
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
a	a	k8xC	a
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
ocitnete	ocitnout	k5eAaPmIp2nP	ocitnout
<g/>
,	,	kIx,	,
znamená	znamenat	k5eAaImIp3nS	znamenat
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
zde	zde	k6eAd1	zde
budete	být	k5eAaImBp2nP	být
více	hodně	k6eAd2	hodně
jak	jak	k8xC	jak
25	[number]	k4	25
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
počet	počet	k1gInSc1	počet
vězňů	vězeň	k1gMnPc2	vězeň
téměř	téměř	k6eAd1	téměř
ztrojnásobil	ztrojnásobit	k5eAaPmAgInS	ztrojnásobit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
vláda	vláda	k1gFnSc1	vláda
tvrdě	tvrdě	k6eAd1	tvrdě
zakročila	zakročit	k5eAaPmAgFnS	zakročit
proti	proti	k7c3	proti
obchodníkům	obchodník	k1gMnPc3	obchodník
s	s	k7c7	s
drogami	droga	k1gFnPc7	droga
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
si	se	k3xPyFc3	se
nyní	nyní	k6eAd1	nyní
odpykává	odpykávat	k5eAaImIp3nS	odpykávat
trest	trest	k1gInSc4	trest
8	[number]	k4	8
000	[number]	k4	000
trestanců	trestanec	k1gMnPc2	trestanec
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
dvakrát	dvakrát	k6eAd1	dvakrát
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
původní	původní	k2eAgFnSc1d1	původní
kapacita	kapacita	k1gFnSc1	kapacita
<g/>
.	.	kIx.	.
</s>
<s>
Vězni	vězeň	k1gMnPc1	vězeň
tráví	trávit	k5eAaImIp3nP	trávit
v	v	k7c6	v
celách	cela	k1gFnPc6	cela
15	[number]	k4	15
hodin	hodina	k1gFnPc2	hodina
denně	denně	k6eAd1	denně
a	a	k8xC	a
světlo	světlo	k1gNnSc1	světlo
na	na	k7c6	na
celách	cela	k1gFnPc6	cela
svítí	svítit	k5eAaImIp3nS	svítit
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
denně	denně	k6eAd1	denně
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
3	[number]	k4	3
měsíce	měsíc	k1gInSc2	měsíc
musíte	muset	k5eAaImIp2nP	muset
nosit	nosit	k5eAaImF	nosit
železné	železný	k2eAgInPc1d1	železný
okovy	okov	k1gInPc1	okov
na	na	k7c6	na
nohou	noha	k1gFnPc6	noha
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
desátý	desátý	k4xOgMnSc1	desátý
odsouzený	odsouzený	k1gMnSc1	odsouzený
se	se	k3xPyFc4	se
pokusí	pokusit	k5eAaPmIp3nS	pokusit
spáchat	spáchat	k5eAaPmF	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Podává	podávat	k5eAaImIp3nS	podávat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
jedno	jeden	k4xCgNnSc4	jeden
jídlo	jídlo	k1gNnSc4	jídlo
denně	denně	k6eAd1	denně
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
většinou	většina	k1gFnSc7	většina
rýže	rýže	k1gFnSc2	rýže
v	v	k7c6	v
masové	masový	k2eAgFnSc6d1	masová
omáčce	omáčka	k1gFnSc6	omáčka
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
vězeň	vězeň	k1gMnSc1	vězeň
přežil	přežít	k5eAaPmAgMnS	přežít
<g/>
,	,	kIx,	,
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
dostávat	dostávat	k5eAaImF	dostávat
peníze	peníz	k1gInPc4	peníz
od	od	k7c2	od
rodiny	rodina	k1gFnSc2	rodina
nebo	nebo	k8xC	nebo
známých	známá	k1gFnPc2	známá
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
minimálně	minimálně	k6eAd1	minimálně
3	[number]	k4	3
000	[number]	k4	000
Kč	Kč	kA	Kč
měsíčně	měsíčně	k6eAd1	měsíčně
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
za	za	k7c7	za
vámi	vy	k3xPp2nPc7	vy
přijde	přijít	k5eAaPmIp3nS	přijít
vaše	váš	k3xOp2gFnSc1	váš
rodina	rodina	k1gFnSc1	rodina
<g/>
,	,	kIx,	,
zavedou	zavést	k5eAaPmIp3nP	zavést
vás	vy	k3xPp2nPc4	vy
do	do	k7c2	do
velké	velký	k2eAgFnSc2d1	velká
návštěvní	návštěvní	k2eAgFnSc2d1	návštěvní
místnosti	místnost	k1gFnSc2	místnost
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
navzájem	navzájem	k6eAd1	navzájem
překřikuje	překřikovat	k5eAaImIp3nS	překřikovat
minimálně	minimálně	k6eAd1	minimálně
dalších	další	k2eAgMnPc2d1	další
třicet	třicet	k4xCc1	třicet
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
přišli	přijít	k5eAaPmAgMnP	přijít
za	za	k7c7	za
vašimi	váš	k3xOp2gMnPc7	váš
spoluvězni	spoluvězeň	k1gMnPc7	spoluvězeň
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
se	se	k3xPyFc4	se
šíří	šířit	k5eAaImIp3nS	šířit
mnoho	mnoho	k4c1	mnoho
chorob	choroba	k1gFnPc2	choroba
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
žloutenka	žloutenka	k1gFnSc1	žloutenka
<g/>
,	,	kIx,	,
HIV	HIV	kA	HIV
nebo	nebo	k8xC	nebo
tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
onemocníte	onemocnět	k5eAaPmIp2nP	onemocnět
<g/>
,	,	kIx,	,
musíte	muset	k5eAaImIp2nP	muset
si	se	k3xPyFc3	se
výdaje	výdaj	k1gInPc4	výdaj
s	s	k7c7	s
léčením	léčení	k1gNnSc7	léčení
platit	platit	k5eAaImF	platit
sami	sám	k3xTgMnPc1	sám
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
mnoho	mnoho	k4c1	mnoho
chudých	chudý	k2eAgMnPc2d1	chudý
vězňů	vězeň	k1gMnPc2	vězeň
umírá	umírat	k5eAaImIp3nS	umírat
i	i	k9	i
na	na	k7c4	na
méně	málo	k6eAd2	málo
závažné	závažný	k2eAgFnPc4d1	závažná
nemoci	nemoc	k1gFnPc4	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Vězení	vězení	k1gNnSc4	vězení
nalezneme	nalézt	k5eAaBmIp1nP	nalézt
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Nonthaburi	Nonthabur	k1gFnSc2	Nonthabur
u	u	k7c2	u
břehu	břeh	k1gInSc2	břeh
řeky	řeka	k1gFnSc2	řeka
Chao	Chao	k6eAd1	Chao
Phraya	Phray	k1gInSc2	Phray
asi	asi	k9	asi
11	[number]	k4	11
km	km	kA	km
severně	severně	k6eAd1	severně
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
Bangkoku	Bangkok	k1gInSc2	Bangkok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Bangkoku	Bangkok	k1gInSc6	Bangkok
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
neuvěřitelné	uvěřitelný	k2eNgNnSc1d1	neuvěřitelné
množství	množství	k1gNnSc1	množství
chrámů	chrám	k1gInPc2	chrám
<g/>
.	.	kIx.	.
</s>
<s>
Uvádí	uvádět	k5eAaImIp3nS	uvádět
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
jich	on	k3xPp3gMnPc2	on
zde	zde	k6eAd1	zde
nalezneme	naleznout	k5eAaPmIp1nP	naleznout
kolem	kolem	k7c2	kolem
500	[number]	k4	500
<g/>
.	.	kIx.	.
</s>
<s>
Chrámy	chrám	k1gInPc1	chrám
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
názvy	název	k1gInPc1	název
začínají	začínat	k5eAaImIp3nP	začínat
na	na	k7c4	na
"	"	kIx"	"
<g/>
Rat-	Rat-	k1gMnSc1	Rat-
<g/>
,	,	kIx,	,
Racha	Racha	k1gMnSc1	Racha
(	(	kIx(	(
<g/>
čti	číst	k5eAaImRp2nS	číst
<g/>
:	:	kIx,	:
rača-	rača-	k?	rača-
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Maha-	Maha-	k1gMnSc1	Maha-
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
založeny	založit	k5eAaPmNgInP	založit
členy	člen	k1gInPc1	člen
královské	královský	k2eAgFnSc2d1	královská
rodiny	rodina	k1gFnSc2	rodina
nebo	nebo	k8xC	nebo
opatrují	opatrovat	k5eAaImIp3nP	opatrovat
nesmírně	smírně	k6eNd1	smírně
uctívané	uctívaný	k2eAgInPc4d1	uctívaný
předměty	předmět	k1gInPc4	předmět
(	(	kIx(	(
<g/>
před	před	k7c7	před
jménem	jméno	k1gNnSc7	jméno
mají	mít	k5eAaImIp3nP	mít
často	často	k6eAd1	často
slovo	slovo	k1gNnSc4	slovo
Phra	Phr	k1gInSc2	Phr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Takových	takový	k3xDgInPc2	takový
významných	významný	k2eAgInPc2d1	významný
chrámů	chrám	k1gInPc2	chrám
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Thajsku	Thajsko	k1gNnSc6	Thajsko
kolem	kolem	k7c2	kolem
180	[number]	k4	180
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
rozhodnete	rozhodnout	k5eAaPmIp2nP	rozhodnout
pro	pro	k7c4	pro
návštěvu	návštěva	k1gFnSc4	návštěva
nějakého	nějaký	k3yIgInSc2	nějaký
chrámu	chrám	k1gInSc2	chrám
<g/>
,	,	kIx,	,
pamatujte	pamatovat	k5eAaImRp2nP	pamatovat
vždy	vždy	k6eAd1	vždy
na	na	k7c4	na
slušné	slušný	k2eAgNnSc4d1	slušné
a	a	k8xC	a
vhodné	vhodný	k2eAgNnSc4d1	vhodné
oblečení	oblečení	k1gNnSc4	oblečení
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
některých	některý	k3yIgInPc2	některý
chrámů	chrám	k1gInPc2	chrám
vás	vy	k3xPp2nPc2	vy
bez	bez	k7c2	bez
toho	ten	k3xDgNnSc2	ten
ani	ani	k9	ani
nevpustí	vpustit	k5eNaPmIp3nS	vpustit
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
městská	městský	k2eAgFnSc1d1	městská
památka	památka	k1gFnSc1	památka
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
prvním	první	k4xOgNnSc7	první
místem	místo	k1gNnSc7	místo
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
turista	turista	k1gMnSc1	turista
zamíří	zamířit	k5eAaPmIp3nS	zamířit
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
palác	palác	k1gInSc1	palác
<g/>
,	,	kIx,	,
také	také	k9	také
často	často	k6eAd1	často
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
jako	jako	k8xS	jako
Královský	královský	k2eAgInSc1d1	královský
palác	palác	k1gInSc1	palác
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
obrovská	obrovský	k2eAgFnSc1d1	obrovská
stavba	stavba	k1gFnSc1	stavba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Thanon	Thanona	k1gFnPc2	Thanona
Na	na	k7c6	na
Phra	Phr	k1gInSc2	Phr
Lan	lano	k1gNnPc2	lano
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
paláce	palác	k1gInSc2	palác
začala	začít	k5eAaPmAgFnS	začít
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Ramy	Rama	k1gFnSc2	Rama
I.	I.	kA	I.
roku	rok	k1gInSc2	rok
1782	[number]	k4	1782
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
zde	zde	k6eAd1	zde
měla	mít	k5eAaImAgFnS	mít
sídlo	sídlo	k1gNnSc4	sídlo
královská	královský	k2eAgFnSc1d1	královská
rodina	rodina	k1gFnSc1	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
komplex	komplex	k1gInSc1	komplex
je	být	k5eAaImIp3nS	být
obehnán	obehnat	k5eAaPmNgInS	obehnat
vysokou	vysoký	k2eAgFnSc7d1	vysoká
bílou	bílý	k2eAgFnSc7d1	bílá
zdí	zeď	k1gFnSc7	zeď
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
do	do	k7c2	do
několika	několik	k4yIc2	několik
částí	část	k1gFnPc2	část
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc7d3	veliký
stavbou	stavba	k1gFnSc7	stavba
je	být	k5eAaImIp3nS	být
bezesporu	bezesporu	k9	bezesporu
Chakri	Chakri	k1gNnSc1	Chakri
Maha	Mah	k1gInSc2	Mah
Prasad	Prasad	k1gInSc1	Prasad
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Trůnní	trůnní	k2eAgInSc1d1	trůnní
sál	sál	k1gInSc1	sál
Chakri	Chakri	k1gNnSc2	Chakri
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
slavnostně	slavnostně	k6eAd1	slavnostně
otevřel	otevřít	k5eAaPmAgMnS	otevřít
Rama	Rama	k1gMnSc1	Rama
V.	V.	kA	V.
roku	rok	k1gInSc2	rok
1882	[number]	k4	1882
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
paláce	palác	k1gInSc2	palác
je	být	k5eAaImIp3nS	být
i	i	k9	i
chrám	chrám	k1gInSc1	chrám
Wat	Wat	k1gMnSc1	Wat
Phra	Phra	k1gMnSc1	Phra
Kaeo	Kaeo	k1gMnSc1	Kaeo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
smaragdová	smaragdový	k2eAgFnSc1d1	Smaragdová
soška	soška	k1gFnSc1	soška
Buddhy	Buddha	k1gMnSc2	Buddha
<g/>
.	.	kIx.	.
</s>
<s>
Areál	areál	k1gInSc1	areál
je	být	k5eAaImIp3nS	být
otevřen	otevřít	k5eAaPmNgInS	otevřít
denně	denně	k6eAd1	denně
od	od	k7c2	od
8	[number]	k4	8
<g/>
:	:	kIx,	:
<g/>
30	[number]	k4	30
do	do	k7c2	do
15	[number]	k4	15
<g/>
:	:	kIx,	:
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
</s>
<s>
Vstupné	vstupné	k1gNnSc1	vstupné
(	(	kIx(	(
<g/>
březen	březen	k1gInSc1	březen
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
500	[number]	k4	500
THB	THB	kA	THB
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
vstupenky	vstupenka	k1gFnSc2	vstupenka
na	na	k7c4	na
Vimanmek	Vimanmek	k1gInSc4	Vimanmek
Royal	Royal	k1gInSc1	Royal
Mansion	Mansion	k1gInSc1	Mansion
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Wat	Wat	k1gMnSc1	Wat
Phra	Phra	k1gMnSc1	Phra
Kaeo	Kaeo	k1gMnSc1	Kaeo
<g/>
.	.	kIx.	.
</s>
<s>
Chrám	chrám	k1gInSc1	chrám
smaragdového	smaragdový	k2eAgMnSc2d1	smaragdový
Buddhy	Buddha	k1gMnSc2	Buddha
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
nejposvátnější	posvátný	k2eAgInSc4d3	nejposvátnější
buddhistický	buddhistický	k2eAgInSc4d1	buddhistický
chrám	chrám	k1gInSc4	chrám
v	v	k7c6	v
Thajsku	Thajsko	k1gNnSc6	Thajsko
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
silný	silný	k2eAgInSc1d1	silný
nábožensko-politický	náboženskoolitický	k2eAgInSc1d1	nábožensko-politický
symbol	symbol	k1gInSc1	symbol
thajské	thajský	k2eAgFnSc2d1	thajská
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
historickém	historický	k2eAgNnSc6d1	historické
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
Královského	královský	k2eAgInSc2d1	královský
paláce	palác	k1gInSc2	palác
<g/>
,	,	kIx,	,
na	na	k7c6	na
jehož	jehož	k3xOyRp3gFnSc6	jehož
horní	horní	k2eAgFnSc6d1	horní
terase	terasa	k1gFnSc6	terasa
byl	být	k5eAaImAgInS	být
postaven	postaven	k2eAgInSc1d1	postaven
roku	rok	k1gInSc2	rok
1784	[number]	k4	1784
<g/>
.	.	kIx.	.
</s>
<s>
Branou	brána	k1gFnSc7	brána
byl	být	k5eAaImAgInS	být
přímo	přímo	k6eAd1	přímo
spojen	spojit	k5eAaPmNgInS	spojit
s	s	k7c7	s
královskou	královský	k2eAgFnSc7d1	královská
residencí	residence	k1gFnSc7	residence
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
hlavní	hlavní	k2eAgFnSc6d1	hlavní
budově	budova	k1gFnSc6	budova
je	být	k5eAaImIp3nS	být
vysoko	vysoko	k6eAd1	vysoko
na	na	k7c6	na
zlatém	zlatý	k2eAgInSc6d1	zlatý
oltáři	oltář	k1gInSc6	oltář
uložen	uložit	k5eAaPmNgInS	uložit
nejposvátnější	posvátný	k2eAgInSc1d3	nejposvátnější
předmět	předmět	k1gInSc1	předmět
Thajska	Thajsko	k1gNnSc2	Thajsko
-	-	kIx~	-
soška	soška	k1gFnSc1	soška
Buddhy	Buddha	k1gMnSc2	Buddha
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
však	však	k9	však
vytesána	vytesán	k2eAgFnSc1d1	vytesána
ze	z	k7c2	z
smaragdu	smaragd	k1gInSc2	smaragd
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ze	z	k7c2	z
zeleného	zelený	k2eAgInSc2d1	zelený
jadeitu	jadeit	k1gInSc2	jadeit
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
vysoká	vysoká	k1gFnSc1	vysoká
66	[number]	k4	66
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
ní	on	k3xPp3gFnSc6	on
pochází	pocházet	k5eAaImIp3nS	pocházet
již	již	k6eAd1	již
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1434	[number]	k4	1434
<g/>
.	.	kIx.	.
</s>
<s>
Interiér	interiér	k1gInSc1	interiér
chrámu	chrámat	k5eAaImIp1nS	chrámat
kromě	kromě	k7c2	kromě
ní	on	k3xPp3gFnSc2	on
vyplňují	vyplňovat	k5eAaImIp3nP	vyplňovat
další	další	k2eAgFnPc1d1	další
sochy	socha	k1gFnPc1	socha
i	i	k8xC	i
nástěnné	nástěnný	k2eAgFnPc1d1	nástěnná
malby	malba	k1gFnPc1	malba
s	s	k7c7	s
motivy	motiv	k1gInPc7	motiv
z	z	k7c2	z
Buddhova	Buddhův	k2eAgInSc2d1	Buddhův
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Dveře	dveře	k1gFnPc1	dveře
zdobí	zdobit	k5eAaImIp3nP	zdobit
scény	scéna	k1gFnPc4	scéna
z	z	k7c2	z
eposu	epos	k1gInSc2	epos
Rámájana	Rámájana	k1gFnSc1	Rámájana
<g/>
,	,	kIx,	,
doplněné	doplněný	k2eAgInPc1d1	doplněný
perleťovými	perleťový	k2eAgInPc7d1	perleťový
ornamenty	ornament	k1gInPc7	ornament
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sousedství	sousedství	k1gNnSc6	sousedství
hlavní	hlavní	k2eAgFnSc2d1	hlavní
svatyně	svatyně	k1gFnSc2	svatyně
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
soukromá	soukromý	k2eAgFnSc1d1	soukromá
kaple	kaple	k1gFnSc1	kaple
pro	pro	k7c4	pro
panovníka	panovník	k1gMnSc4	panovník
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
palácového	palácový	k2eAgInSc2d1	palácový
komplexu	komplex	k1gInSc2	komplex
je	být	k5eAaImIp3nS	být
také	také	k9	také
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
k	k	k7c3	k
vidění	vidění	k1gNnSc3	vidění
všechny	všechen	k3xTgInPc4	všechen
kostýmy	kostým	k1gInPc4	kostým
<g/>
,	,	kIx,	,
do	do	k7c2	do
nichž	jenž	k3xRgInPc2	jenž
bývá	bývat	k5eAaImIp3nS	bývat
socha	socha	k1gFnSc1	socha
smaragdového	smaragdový	k2eAgMnSc2d1	smaragdový
Buddhy	Buddha	k1gMnSc2	Buddha
oblékána	oblékán	k2eAgFnSc1d1	oblékána
<g/>
.	.	kIx.	.
</s>
<s>
Vstupné	vstupné	k1gNnSc1	vstupné
do	do	k7c2	do
chrámu	chrám	k1gInSc2	chrám
je	být	k5eAaImIp3nS	být
společné	společný	k2eAgNnSc1d1	společné
se	s	k7c7	s
vstupem	vstup	k1gInSc7	vstup
do	do	k7c2	do
Královského	královský	k2eAgInSc2d1	královský
paláce	palác	k1gInSc2	palác
<g/>
.	.	kIx.	.
</s>
<s>
Tomuto	tento	k3xDgInSc3	tento
buddhistickému	buddhistický	k2eAgInSc3d1	buddhistický
chrámu	chrám	k1gInSc3	chrám
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
Chrám	chrám	k1gInSc1	chrám
úsvitu	úsvit	k1gInSc2	úsvit
a	a	k8xC	a
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgFnPc4d3	nejznámější
památky	památka	k1gFnPc4	památka
Thajska	Thajsko	k1gNnSc2	Thajsko
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
historie	historie	k1gFnSc1	historie
sahá	sahat	k5eAaImIp3nS	sahat
minimálně	minimálně	k6eAd1	minimálně
do	do	k7c2	do
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
do	do	k7c2	do
dnešní	dnešní	k2eAgFnSc2d1	dnešní
podoby	podoba	k1gFnSc2	podoba
byl	být	k5eAaImAgInS	být
uveden	uvést	k5eAaPmNgInS	uvést
až	až	k9	až
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
umístěn	umístit	k5eAaPmNgInS	umístit
na	na	k7c6	na
západním	západní	k2eAgInSc6d1	západní
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
Čao	čao	k0	čao
Praja	Praj	k2eAgMnSc4d1	Praj
ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
Yai	Yai	k1gFnSc2	Yai
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1785	[number]	k4	1785
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
zmiňovaná	zmiňovaný	k2eAgFnSc1d1	zmiňovaná
soška	soška	k1gFnSc1	soška
smaragdového	smaragdový	k2eAgMnSc2d1	smaragdový
Buddhy	Buddha	k1gMnSc2	Buddha
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
než	než	k8xS	než
byla	být	k5eAaImAgFnS	být
přemístěna	přemístit	k5eAaPmNgFnS	přemístit
do	do	k7c2	do
chrámu	chrám	k1gInSc2	chrám
Wat	Wat	k1gMnSc1	Wat
Phra	Phra	k1gMnSc1	Phra
Kaeo	Kaeo	k1gMnSc1	Kaeo
<g/>
.	.	kIx.	.
</s>
<s>
Dominantou	dominanta	k1gFnSc7	dominanta
chrámu	chrám	k1gInSc2	chrám
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
82	[number]	k4	82
metrů	metr	k1gInPc2	metr
vysoká	vysoký	k2eAgFnSc1d1	vysoká
věž	věž	k1gFnSc1	věž
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yIgFnSc2	který
je	být	k5eAaImIp3nS	být
krásný	krásný	k2eAgInSc1d1	krásný
výhled	výhled	k1gInSc1	výhled
na	na	k7c4	na
celou	celý	k2eAgFnSc4d1	celá
čtvrť	čtvrť	k1gFnSc4	čtvrť
<g/>
.	.	kIx.	.
</s>
<s>
Chrám	chrám	k1gInSc1	chrám
je	být	k5eAaImIp3nS	být
otevřen	otevřít	k5eAaPmNgInS	otevřít
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
denně	denně	k6eAd1	denně
od	od	k7c2	od
8	[number]	k4	8
<g/>
:	:	kIx,	:
<g/>
30	[number]	k4	30
do	do	k7c2	do
17	[number]	k4	17
<g/>
:	:	kIx,	:
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
</s>
<s>
Vstupné	vstupné	k1gNnSc1	vstupné
je	být	k5eAaImIp3nS	být
30	[number]	k4	30
THB	THB	kA	THB
pro	pro	k7c4	pro
cizince	cizinec	k1gMnPc4	cizinec
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Thajce	Thajce	k1gMnPc4	Thajce
je	být	k5eAaImIp3nS	být
vstup	vstup	k1gInSc1	vstup
zdarma	zdarma	k6eAd1	zdarma
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nejmladší	mladý	k2eAgInSc4d3	nejmladší
chrám	chrám	k1gInSc4	chrám
v	v	k7c6	v
Bangkoku	Bangkok	k1gInSc6	Bangkok
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
z	z	k7c2	z
italského	italský	k2eAgInSc2d1	italský
mramoru	mramor	k1gInSc2	mramor
roku	rok	k1gInSc2	rok
1899	[number]	k4	1899
na	na	k7c4	na
příkaz	příkaz	k1gInSc4	příkaz
krále	král	k1gMnSc2	král
Chulalongkorna	Chulalongkorno	k1gNnSc2	Chulalongkorno
(	(	kIx(	(
<g/>
Ramy	Rama	k1gFnSc2	Rama
V.	V.	kA	V.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
i	i	k9	i
pochován	pochován	k2eAgMnSc1d1	pochován
<g/>
.	.	kIx.	.
</s>
<s>
Mramorový	mramorový	k2eAgInSc1d1	mramorový
chrám	chrám	k1gInSc1	chrám
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
také	také	k9	také
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
Dusit	dusit	k5eAaImF	dusit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
nádvoří	nádvoří	k1gNnSc6	nádvoří
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
k	k	k7c3	k
vidění	vidění	k1gNnSc3	vidění
53	[number]	k4	53
Buddhových	Buddhův	k2eAgFnPc2d1	Buddhova
soch	socha	k1gFnPc2	socha
z	z	k7c2	z
celé	celý	k2eAgFnSc2d1	celá
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Otevřeno	otevřít	k5eAaPmNgNnS	otevřít
je	být	k5eAaImIp3nS	být
denně	denně	k6eAd1	denně
od	od	k7c2	od
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
do	do	k7c2	do
18	[number]	k4	18
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
a	a	k8xC	a
vstupné	vstupné	k1gNnSc1	vstupné
činí	činit	k5eAaImIp3nS	činit
20	[number]	k4	20
THB	THB	kA	THB
(	(	kIx(	(
<g/>
březen	březen	k1gInSc1	březen
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
velký	velký	k2eAgInSc1d1	velký
a	a	k8xC	a
rozsáhlý	rozsáhlý	k2eAgInSc1d1	rozsáhlý
chrám	chrám	k1gInSc1	chrám
sousedí	sousedit	k5eAaImIp3nS	sousedit
s	s	k7c7	s
Královským	královský	k2eAgInSc7d1	královský
palácem	palác	k1gInSc7	palác
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
nejstarším	starý	k2eAgInSc7d3	nejstarší
chrámem	chrám	k1gInSc7	chrám
v	v	k7c6	v
Thajsku	Thajsko	k1gNnSc6	Thajsko
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc7d3	veliký
atrakcí	atrakce	k1gFnSc7	atrakce
je	být	k5eAaImIp3nS	být
zajisté	zajisté	k9	zajisté
obrovská	obrovský	k2eAgFnSc1d1	obrovská
46	[number]	k4	46
metrů	metr	k1gInPc2	metr
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
a	a	k8xC	a
15	[number]	k4	15
metrů	metr	k1gInPc2	metr
vysoká	vysoký	k2eAgFnSc1d1	vysoká
socha	socha	k1gFnSc1	socha
ležícího	ležící	k2eAgMnSc2d1	ležící
Buddhy	Buddha	k1gMnSc2	Buddha
<g/>
,	,	kIx,	,
znázorňující	znázorňující	k2eAgFnSc4d1	znázorňující
umírajícího	umírající	k2eAgMnSc4d1	umírající
Buddhu	Buddha	k1gMnSc4	Buddha
v	v	k7c6	v
pozici	pozice	k1gFnSc6	pozice
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
nirvány	nirvána	k1gFnPc4	nirvána
<g/>
.	.	kIx.	.
</s>
<s>
Chrám	chrám	k1gInSc1	chrám
je	být	k5eAaImIp3nS	být
také	také	k9	také
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
první	první	k4xOgNnSc4	první
centrum	centrum	k1gNnSc4	centrum
veřejného	veřejný	k2eAgNnSc2d1	veřejné
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
nazýván	nazývat	k5eAaImNgInS	nazývat
první	první	k4xOgFnSc7	první
thajskou	thajský	k2eAgFnSc7d1	thajská
univerzitou	univerzita	k1gFnSc7	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Otevírací	otevírací	k2eAgFnSc1d1	otevírací
doba	doba	k1gFnSc1	doba
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
8	[number]	k4	8
<g/>
:	:	kIx,	:
<g/>
30	[number]	k4	30
do	do	k7c2	do
18	[number]	k4	18
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
a	a	k8xC	a
vstupné	vstupné	k1gNnSc1	vstupné
je	být	k5eAaImIp3nS	být
100	[number]	k4	100
THB	THB	kA	THB
(	(	kIx(	(
<g/>
březen	březen	k1gInSc1	březen
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Palác	palác	k1gInSc1	palác
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
v	v	k7c6	v
italském	italský	k2eAgInSc6d1	italský
renesančním	renesanční	k2eAgInSc6d1	renesanční
a	a	k8xC	a
neoklasicistním	neoklasicistní	k2eAgInSc6d1	neoklasicistní
stylu	styl	k1gInSc6	styl
<g/>
.	.	kIx.	.
</s>
<s>
Stavět	stavět	k5eAaImF	stavět
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1907	[number]	k4	1907
a	a	k8xC	a
stavba	stavba	k1gFnSc1	stavba
byla	být	k5eAaImAgFnS	být
dokončena	dokončen	k2eAgFnSc1d1	dokončena
roku	rok	k1gInSc2	rok
1915	[number]	k4	1915
<g/>
.	.	kIx.	.
</s>
<s>
Kopule	kopule	k1gFnSc1	kopule
paláce	palác	k1gInSc2	palác
je	být	k5eAaImIp3nS	být
kopií	kopie	k1gFnSc7	kopie
Dómu	dóm	k1gInSc2	dóm
Sv.	sv.	kA	sv.
Petra	Petr	k1gMnSc2	Petr
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Vstupné	vstupné	k1gNnSc1	vstupné
činí	činit	k5eAaImIp3nS	činit
50	[number]	k4	50
THB	THB	kA	THB
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
fanoušky	fanoušek	k1gMnPc4	fanoušek
astronomie	astronomie	k1gFnSc2	astronomie
a	a	k8xC	a
biologie	biologie	k1gFnSc2	biologie
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nachází	nacházet	k5eAaImIp3nS	nacházet
ráj	ráj	k1gInSc4	ráj
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
muzeu	muzeum	k1gNnSc3	muzeum
se	se	k3xPyFc4	se
snadno	snadno	k6eAd1	snadno
dostanete	dostat	k5eAaPmIp2nP	dostat
ze	z	k7c2	z
stanice	stanice	k1gFnSc2	stanice
BTS	BTS	kA	BTS
Ekkamai	Ekkama	k1gFnSc2	Ekkama
<g/>
.	.	kIx.	.
</s>
<s>
Vstupné	vstupné	k1gNnSc1	vstupné
pak	pak	k6eAd1	pak
činí	činit	k5eAaImIp3nS	činit
30	[number]	k4	30
THB	THB	kA	THB
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
vystaven	vystaven	k2eAgInSc1d1	vystaven
roku	rok	k1gInSc2	rok
1940	[number]	k4	1940
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
art	art	k?	art
deco	deco	k1gMnSc1	deco
<g/>
,	,	kIx,	,
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
státního	státní	k2eAgInSc2d1	státní
převratu	převrat	k1gInSc2	převrat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
<g/>
.	.	kIx.	.
</s>
<s>
Památník	památník	k1gInSc1	památník
je	být	k5eAaImIp3nS	být
vysoký	vysoký	k2eAgInSc1d1	vysoký
24	[number]	k4	24
m	m	kA	m
a	a	k8xC	a
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
nedaleko	daleko	k6eNd1	daleko
od	od	k7c2	od
Khao	Khao	k6eAd1	Khao
San	San	k1gMnPc2	San
Road	Roada	k1gFnPc2	Roada
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
památník	památník	k1gInSc1	památník
byl	být	k5eAaImAgInS	být
slavnostně	slavnostně	k6eAd1	slavnostně
otevřen	otevřen	k2eAgInSc1d1	otevřen
roku	rok	k1gInSc3	rok
1940	[number]	k4	1940
<g/>
,	,	kIx,	,
k	k	k7c3	k
uctění	uctění	k1gNnSc3	uctění
hrdinských	hrdinský	k2eAgInPc2d1	hrdinský
činů	čin	k1gInPc2	čin
a	a	k8xC	a
padlých	padlý	k1gMnPc2	padlý
při	při	k7c6	při
konfliktu	konflikt	k1gInSc6	konflikt
mezi	mezi	k7c7	mezi
Thajskem	Thajsko	k1gNnSc7	Thajsko
a	a	k8xC	a
Francií	Francie	k1gFnSc7	Francie
na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
mezi	mezi	k7c7	mezi
Thajskem	Thajsko	k1gNnSc7	Thajsko
a	a	k8xC	a
Indočínou	Indočína	k1gFnSc7	Indočína
roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
<g/>
.	.	kIx.	.
</s>
<s>
Tyčí	tyčit	k5eAaImIp3nS	tyčit
se	se	k3xPyFc4	se
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
50	[number]	k4	50
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Krásný	krásný	k2eAgInSc1d1	krásný
pohled	pohled	k1gInSc1	pohled
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
nadzemní	nadzemní	k2eAgFnSc2d1	nadzemní
rychlodráhy	rychlodráha	k1gFnSc2	rychlodráha
BTS	BTS	kA	BTS
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ho	on	k3xPp3gMnSc4	on
obkružuje	obkružovat	k5eAaImIp3nS	obkružovat
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnPc1d1	hlavní
atrakce	atrakce	k1gFnPc1	atrakce
v	v	k7c6	v
Bangkoku	Bangkok	k1gInSc6	Bangkok
je	být	k5eAaImIp3nS	být
nakupování	nakupování	k1gNnSc1	nakupování
<g/>
.	.	kIx.	.
</s>
<s>
Tomu	ten	k3xDgNnSc3	ten
nahrávají	nahrávat	k5eAaImIp3nP	nahrávat
velmi	velmi	k6eAd1	velmi
nízké	nízký	k2eAgFnPc1d1	nízká
ceny	cena	k1gFnPc1	cena
zboží	zboží	k1gNnSc2	zboží
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zvykem	zvyk	k1gInSc7	zvyk
o	o	k7c6	o
konečné	konečný	k2eAgFnSc6d1	konečná
ceně	cena	k1gFnSc6	cena
smlouvat	smlouvat	k5eAaImF	smlouvat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgInPc6d1	poslední
letech	let	k1gInPc6	let
se	se	k3xPyFc4	se
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
počet	počet	k1gInSc1	počet
supermarketů	supermarket	k1gInPc2	supermarket
<g/>
,	,	kIx,	,
kterých	který	k3yQgInPc2	který
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
ke	k	k7c3	k
stovce	stovka	k1gFnSc3	stovka
<g/>
.	.	kIx.	.
</s>
<s>
Čínská	čínský	k2eAgFnSc1d1	čínská
čtvrť	čtvrť	k1gFnSc1	čtvrť
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgFnPc2d3	nejstarší
oblastí	oblast	k1gFnPc2	oblast
Bangkoku	Bangkok	k1gInSc2	Bangkok
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
byli	být	k5eAaImAgMnP	být
Číňané	Číňan	k1gMnPc1	Číňan
přemístěni	přemístit	k5eAaPmNgMnP	přemístit
v	v	k7c4	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
na	na	k7c4	na
příkaz	příkaz	k1gInSc4	příkaz
krále	král	k1gMnSc2	král
Ramy	Rama	k1gFnSc2	Rama
I.	I.	kA	I.
Do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
1891	[number]	k4	1891
nechal	nechat	k5eAaPmAgMnS	nechat
král	král	k1gMnSc1	král
Rama	Ramum	k1gNnSc2	Ramum
V.	V.	kA	V.
postavit	postavit	k5eAaPmF	postavit
mnoho	mnoho	k4c1	mnoho
silnic	silnice	k1gFnPc2	silnice
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Yaowarat	Yaowarat	k1gInSc4	Yaowarat
Road	Roada	k1gFnPc2	Roada
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
známé	známý	k2eAgFnPc1d1	známá
ulice	ulice	k1gFnPc1	ulice
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
Charoen	Charoen	k2eAgInSc4d1	Charoen
Krung	Krung	k1gInSc4	Krung
Road	Road	k1gMnSc1	Road
nebo	nebo	k8xC	nebo
Mungkorn	Mungkorn	k1gMnSc1	Mungkorn
Road	Road	k1gMnSc1	Road
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
zde	zde	k6eAd1	zde
mnoho	mnoho	k4c1	mnoho
obchodů	obchod	k1gInPc2	obchod
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
mání	mání	k1gNnSc3	mání
zlato	zlato	k1gNnSc1	zlato
<g/>
,	,	kIx,	,
oděvy	oděv	k1gInPc1	oděv
<g/>
,	,	kIx,	,
elektronika	elektronika	k1gFnSc1	elektronika
<g/>
,	,	kIx,	,
místní	místní	k2eAgFnPc4d1	místní
pochoutky	pochoutka	k1gFnPc4	pochoutka
a	a	k8xC	a
mnoho	mnoho	k4c4	mnoho
dalšího	další	k2eAgNnSc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Ceny	cena	k1gFnPc1	cena
pozemků	pozemek	k1gInPc2	pozemek
kolem	kolem	k7c2	kolem
ulice	ulice	k1gFnSc2	ulice
Yaowarat	Yaowarat	k1gInSc1	Yaowarat
jsou	být	k5eAaImIp3nP	být
jedny	jeden	k4xCgFnPc1	jeden
z	z	k7c2	z
nejdražších	drahý	k2eAgFnPc2d3	nejdražší
v	v	k7c6	v
Bangkoku	Bangkok	k1gInSc6	Bangkok
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
nedostatku	nedostatek	k1gInSc3	nedostatek
místa	místo	k1gNnSc2	místo
pro	pro	k7c4	pro
další	další	k2eAgFnSc4d1	další
stavbu	stavba	k1gFnSc4	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
chcete	chtít	k5eAaImIp2nP	chtít
poznat	poznat	k5eAaPmF	poznat
čínskou	čínský	k2eAgFnSc4d1	čínská
kulturu	kultura	k1gFnSc4	kultura
naplno	naplno	k6eAd1	naplno
<g/>
,	,	kIx,	,
navštivte	navštívit	k5eAaPmRp2nP	navštívit
tuto	tento	k3xDgFnSc4	tento
čtvrť	čtvrť	k1gFnSc4	čtvrť
v	v	k7c6	v
období	období	k1gNnSc6	období
některého	některý	k3yIgMnSc2	některý
z	z	k7c2	z
čínských	čínský	k2eAgInPc2d1	čínský
svátků	svátek	k1gInPc2	svátek
a	a	k8xC	a
vychutnejte	vychutnat	k5eAaPmRp2nP	vychutnat
si	se	k3xPyFc3	se
tu	ten	k3xDgFnSc4	ten
nezapomenutelnou	zapomenutelný	k2eNgFnSc4d1	nezapomenutelná
atmosféru	atmosféra	k1gFnSc4	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
je	být	k5eAaImIp3nS	být
prostituce	prostituce	k1gFnSc1	prostituce
v	v	k7c6	v
Thajsku	Thajsko	k1gNnSc6	Thajsko
nelegální	legální	k2eNgInSc4d1	nelegální
<g/>
,	,	kIx,	,
ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
Patpong	Patponga	k1gFnPc2	Patponga
tento	tento	k3xDgInSc4	tento
zákon	zákon	k1gInSc4	zákon
neplatí	platit	k5eNaImIp3nP	platit
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
si	se	k3xPyFc3	se
představujete	představovat	k5eAaImIp2nP	představovat
Thajsko	Thajsko	k1gNnSc1	Thajsko
jako	jako	k8xS	jako
sexuální	sexuální	k2eAgInSc1d1	sexuální
ráj	ráj	k1gInSc1	ráj
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
právě	právě	k9	právě
kvůli	kvůli	k7c3	kvůli
tomuto	tento	k3xDgNnSc3	tento
místu	místo	k1gNnSc3	místo
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgFnPc1d3	nejznámější
ulice	ulice	k1gFnPc1	ulice
se	se	k3xPyFc4	se
jmenují	jmenovat	k5eAaImIp3nP	jmenovat
Soi	Soi	k1gMnSc2	Soi
Cowboy	Cowboa	k1gMnSc2	Cowboa
a	a	k8xC	a
Nana	Nanus	k1gMnSc2	Nanus
Plaza	plaz	k1gMnSc2	plaz
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yIgNnPc6	který
nalezneme	nalézt	k5eAaBmIp1nP	nalézt
několik	několik	k4yIc4	několik
slavných	slavný	k2eAgInPc2d1	slavný
go-go	goo	k6eAd1	go-go
barů	bar	k1gInPc2	bar
<g/>
.	.	kIx.	.
</s>
<s>
Historie	historie	k1gFnSc1	historie
této	tento	k3xDgFnSc2	tento
proslulé	proslulý	k2eAgFnSc2d1	proslulá
čtvrti	čtvrt	k1gFnSc2	čtvrt
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
psát	psát	k5eAaImF	psát
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nahromadilo	nahromadit	k5eAaPmAgNnS	nahromadit
několik	několik	k4yIc1	několik
hospůdek	hospůdka	k1gFnPc2	hospůdka
v	v	k7c6	v
těsném	těsný	k2eAgNnSc6d1	těsné
sousedství	sousedství	k1gNnSc6	sousedství
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
války	válka	k1gFnSc2	válka
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
se	se	k3xPyFc4	se
Patpong	Patpong	k1gInSc1	Patpong
stal	stát	k5eAaPmAgInS	stát
útočištěm	útočiště	k1gNnSc7	útočiště
amerických	americký	k2eAgMnPc2d1	americký
vojáků	voják	k1gMnPc2	voják
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
zde	zde	k6eAd1	zde
hledali	hledat	k5eAaImAgMnP	hledat
zábavu	zábava	k1gFnSc4	zábava
a	a	k8xC	a
relaxaci	relaxace	k1gFnSc4	relaxace
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
den	den	k1gInSc4	den
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
konají	konat	k5eAaImIp3nP	konat
veliké	veliký	k2eAgInPc1d1	veliký
trhy	trh	k1gInPc1	trh
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
musíte	muset	k5eAaImIp2nP	muset
počítat	počítat	k5eAaImF	počítat
s	s	k7c7	s
vyšší	vysoký	k2eAgFnSc7d2	vyšší
cenou	cena	k1gFnSc7	cena
<g/>
,	,	kIx,	,
než	než	k8xS	než
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
rozhodnete	rozhodnout	k5eAaPmIp2nP	rozhodnout
využít	využít	k5eAaPmF	využít
z	z	k7c2	z
některých	některý	k3yIgFnPc2	některý
erotických	erotický	k2eAgFnPc2d1	erotická
služeb	služba	k1gFnPc2	služba
<g/>
,	,	kIx,	,
pamatujte	pamatovat	k5eAaImRp2nP	pamatovat
vždy	vždy	k6eAd1	vždy
na	na	k7c4	na
rizika	riziko	k1gNnPc4	riziko
spojená	spojený	k2eAgNnPc4d1	spojené
s	s	k7c7	s
pohlavními	pohlavní	k2eAgFnPc7d1	pohlavní
nemocemi	nemoc	k1gFnPc7	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Bangkoku	Bangkok	k1gInSc6	Bangkok
nalezneme	nalézt	k5eAaBmIp1nP	nalézt
nespočet	nespočet	k1gInSc4	nespočet
tržišť	tržiště	k1gNnPc2	tržiště
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
koupit	koupit	k5eAaPmF	koupit
čerstvé	čerstvý	k2eAgFnPc4d1	čerstvá
potraviny	potravina	k1gFnPc4	potravina
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
největší	veliký	k2eAgFnPc1d3	veliký
jsou	být	k5eAaImIp3nP	být
zachycené	zachycený	k2eAgFnPc1d1	zachycená
na	na	k7c6	na
mapách	mapa	k1gFnPc6	mapa
<g/>
.	.	kIx.	.
</s>
<s>
Snad	snad	k9	snad
nejlepší	dobrý	k2eAgNnSc1d3	nejlepší
tržiště	tržiště	k1gNnSc1	tržiště
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
u	u	k7c2	u
hotelu	hotel	k1gInSc2	hotel
Shangri-La	Shangri-L	k1gInSc2	Shangri-L
a	a	k8xC	a
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
se	se	k3xPyFc4	se
Bang	Bang	k1gMnSc1	Bang
Rak	rak	k1gMnSc1	rak
Market	market	k1gInSc4	market
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
mrakodrap	mrakodrap	k1gInSc1	mrakodrap
má	mít	k5eAaImIp3nS	mít
85	[number]	k4	85
podlaží	podlaží	k1gNnPc2	podlaží
a	a	k8xC	a
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
měří	měřit	k5eAaImIp3nS	měřit
304	[number]	k4	304
m.	m.	k?	m.
Nalezneme	nalézt	k5eAaBmIp1nP	nalézt
ho	on	k3xPp3gMnSc4	on
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
222	[number]	k4	222
Ratchaprarop	Ratchaprarop	k1gInSc1	Ratchaprarop
Road	Road	k1gInSc4	Road
ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
Ratchathewi	Ratchathew	k1gFnSc2	Ratchathew
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
budova	budova	k1gFnSc1	budova
ve	v	k7c6	v
městě	město	k1gNnSc6	město
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
hotel	hotel	k1gInSc1	hotel
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
jména	jméno	k1gNnSc2	jméno
stavitele	stavitel	k1gMnSc2	stavitel
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
byl	být	k5eAaImAgMnS	být
Panlert	Panlert	k1gMnSc1	Panlert
Baiyoke	Baiyok	k1gInSc2	Baiyok
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc4	projekt
vypracovala	vypracovat	k5eAaPmAgFnS	vypracovat
firma	firma	k1gFnSc1	firma
Plan	plan	k1gInSc4	plan
Architects	Architects	k1gInSc1	Architects
Company	Compana	k1gFnSc2	Compana
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
a	a	k8xC	a
rozpočet	rozpočet	k1gInSc1	rozpočet
činil	činit	k5eAaImAgInS	činit
3,8	[number]	k4	3,8
miliard	miliarda	k4xCgFnPc2	miliarda
THB	THB	kA	THB
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
dokončena	dokončen	k2eAgFnSc1d1	dokončena
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
a	a	k8xC	a
s	s	k7c7	s
přidáním	přidání	k1gNnSc7	přidání
antény	anténa	k1gFnSc2	anténa
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
výška	výška	k1gFnSc1	výška
budovy	budova	k1gFnSc2	budova
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
na	na	k7c4	na
328	[number]	k4	328
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
77	[number]	k4	77
<g/>
.	.	kIx.	.
patře	patro	k1gNnSc6	patro
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
veřejná	veřejný	k2eAgFnSc1d1	veřejná
hvězdárna	hvězdárna	k1gFnSc1	hvězdárna
<g/>
,	,	kIx,	,
na	na	k7c4	na
83	[number]	k4	83
<g/>
.	.	kIx.	.
patře	patro	k1gNnSc6	patro
střešní	střešní	k2eAgInSc1d1	střešní
bar	bar	k1gInSc1	bar
nazvaný	nazvaný	k2eAgInSc1d1	nazvaný
"	"	kIx"	"
<g/>
Roof	Roof	k1gInSc1	Roof
Top	topit	k5eAaImRp2nS	topit
Bar	bar	k1gInSc1	bar
&	&	k?	&
Music	Musice	k1gInPc2	Musice
Lounge	Loung	k1gInSc2	Loung
<g/>
"	"	kIx"	"
a	a	k8xC	a
v	v	k7c6	v
84	[number]	k4	84
<g/>
.	.	kIx.	.
podlaží	podlaží	k1gNnSc2	podlaží
najdeme	najít	k5eAaPmIp1nP	najít
krásnou	krásný	k2eAgFnSc4d1	krásná
otočnou	otočný	k2eAgFnSc4d1	otočná
vyhlídku	vyhlídka	k1gFnSc4	vyhlídka
s	s	k7c7	s
rozsahem	rozsah	k1gInSc7	rozsah
360	[number]	k4	360
<g/>
°	°	k?	°
<g/>
,	,	kIx,	,
nazývanou	nazývaný	k2eAgFnSc4d1	nazývaná
Nebeská	nebeský	k2eAgFnSc1d1	nebeská
cesta	cesta	k1gFnSc1	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
Baioke	Baioke	k1gInSc1	Baioke
Sky	Sky	k1gFnPc2	Sky
Hotel	hotel	k1gInSc1	hotel
<g/>
,	,	kIx,	,
zaujímající	zaujímající	k2eAgMnSc1d1	zaujímající
22	[number]	k4	22
<g/>
.	.	kIx.	.
-	-	kIx~	-
74	[number]	k4	74
<g/>
.	.	kIx.	.
patro	patro	k1gNnSc1	patro
<g/>
,	,	kIx,	,
nabízí	nabízet	k5eAaImIp3nS	nabízet
673	[number]	k4	673
pokojů	pokoj	k1gInPc2	pokoj
v	v	k7c6	v
dostupné	dostupný	k2eAgFnSc6d1	dostupná
cenové	cenový	k2eAgFnSc6d1	cenová
kategorii	kategorie	k1gFnSc6	kategorie
<g/>
.	.	kIx.	.
</s>
<s>
Vstupné	vstupné	k1gNnSc1	vstupné
do	do	k7c2	do
hvězdárny	hvězdárna	k1gFnSc2	hvězdárna
a	a	k8xC	a
na	na	k7c4	na
vyhlídku	vyhlídka	k1gFnSc4	vyhlídka
činí	činit	k5eAaImIp3nS	činit
300	[number]	k4	300
THB	THB	kA	THB
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jste	být	k5eAaImIp2nP	být
v	v	k7c6	v
hotelu	hotel	k1gInSc6	hotel
ubytovaní	ubytovaný	k2eAgMnPc1d1	ubytovaný
<g/>
,	,	kIx,	,
máte	mít	k5eAaImIp2nP	mít
to	ten	k3xDgNnSc1	ten
zdarma	zdarma	k6eAd1	zdarma
<g/>
.	.	kIx.	.
</s>
<s>
Jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
se	se	k3xPyFc4	se
Sukhumit	Sukhumit	k1gInSc1	Sukhumit
Road	Roada	k1gFnPc2	Roada
a	a	k8xC	a
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejmodernější	moderní	k2eAgFnPc4d3	nejmodernější
oblasti	oblast	k1gFnPc4	oblast
v	v	k7c6	v
Bangkoku	Bangkok	k1gInSc6	Bangkok
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
ulice	ulice	k1gFnSc1	ulice
je	být	k5eAaImIp3nS	být
úctyhodných	úctyhodný	k2eAgInPc2d1	úctyhodný
400	[number]	k4	400
km	km	kA	km
a	a	k8xC	a
táhne	táhnout	k5eAaImIp3nS	táhnout
se	se	k3xPyFc4	se
podél	podél	k7c2	podél
východního	východní	k2eAgNnSc2d1	východní
pobřeží	pobřeží	k1gNnSc2	pobřeží
Thajska	Thajsko	k1gNnSc2	Thajsko
<g/>
.	.	kIx.	.
</s>
<s>
Prochází	procházet	k5eAaImIp3nS	procházet
přímořským	přímořský	k2eAgNnSc7d1	přímořské
letoviskem	letovisko	k1gNnSc7	letovisko
Pattayou	Pattaya	k1gFnSc7	Pattaya
a	a	k8xC	a
končí	končit	k5eAaImIp3nS	končit
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Trat	trata	k1gFnPc2	trata
u	u	k7c2	u
hranic	hranice	k1gFnPc2	hranice
s	s	k7c7	s
Kambodžou	Kambodža	k1gFnSc7	Kambodža
<g/>
.	.	kIx.	.
</s>
<s>
Charakteristickým	charakteristický	k2eAgNnSc7d1	charakteristické
poznávacím	poznávací	k2eAgNnSc7d1	poznávací
znamením	znamení	k1gNnSc7	znamení
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
linií	linie	k1gFnPc2	linie
nadzemní	nadzemní	k2eAgFnSc2d1	nadzemní
rychlodráhy	rychlodráha	k1gFnSc2	rychlodráha
BTS	BTS	kA	BTS
<g/>
.	.	kIx.	.
</s>
<s>
Historie	historie	k1gFnSc1	historie
tohoto	tento	k3xDgInSc2	tento
luxusního	luxusní	k2eAgInSc2d1	luxusní
hotelu	hotel	k1gInSc2	hotel
sahá	sahat	k5eAaImIp3nS	sahat
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1876	[number]	k4	1876
<g/>
.	.	kIx.	.
</s>
<s>
Bohužel	bohužel	k9	bohužel
se	se	k3xPyFc4	se
nám	my	k3xPp1nPc3	my
dnes	dnes	k6eAd1	dnes
zachovala	zachovat	k5eAaPmAgFnS	zachovat
jen	jen	k9	jen
nepatrná	patrný	k2eNgFnSc1d1	patrný
část	část	k1gFnSc1	část
z	z	k7c2	z
původního	původní	k2eAgInSc2d1	původní
hotelu	hotel	k1gInSc2	hotel
<g/>
.	.	kIx.	.
</s>
<s>
Hotel	hotel	k1gInSc1	hotel
získal	získat	k5eAaPmAgInS	získat
několikrát	několikrát	k6eAd1	několikrát
po	po	k7c6	po
sobě	sebe	k3xPyFc6	sebe
titul	titul	k1gInSc4	titul
nejlepšího	dobrý	k2eAgInSc2d3	nejlepší
hotelu	hotel	k1gInSc2	hotel
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
součástí	součást	k1gFnSc7	součást
jsou	být	k5eAaImIp3nP	být
i	i	k8xC	i
jedny	jeden	k4xCgFnPc1	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
lázní	lázeň	k1gFnPc2	lázeň
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Této	tento	k3xDgFnSc3	tento
kategorii	kategorie	k1gFnSc3	kategorie
samozřejmě	samozřejmě	k6eAd1	samozřejmě
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
i	i	k9	i
restaurace	restaurace	k1gFnPc4	restaurace
a	a	k8xC	a
bary	bar	k1gInPc4	bar
<g/>
.	.	kIx.	.
</s>
<s>
Ceny	cena	k1gFnPc1	cena
začínají	začínat	k5eAaImIp3nP	začínat
na	na	k7c4	na
8	[number]	k4	8
000	[number]	k4	000
Kč	Kč	kA	Kč
za	za	k7c4	za
noc	noc	k1gFnSc4	noc
a	a	k8xC	a
cena	cena	k1gFnSc1	cena
prezidentského	prezidentský	k2eAgNnSc2d1	prezidentské
apartmá	apartmá	k1gNnSc2	apartmá
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
mimochodem	mimochodem	k9	mimochodem
zabírá	zabírat	k5eAaImIp3nS	zabírat
jedno	jeden	k4xCgNnSc1	jeden
celé	celý	k2eAgNnSc1d1	celé
patro	patro	k1gNnSc1	patro
<g/>
,	,	kIx,	,
činí	činit	k5eAaImIp3nS	činit
pak	pak	k6eAd1	pak
90	[number]	k4	90
000	[number]	k4	000
Kč	Kč	kA	Kč
za	za	k7c4	za
jedno	jeden	k4xCgNnSc4	jeden
vyspání	vyspání	k1gNnSc4	vyspání
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
pravidelné	pravidelný	k2eAgMnPc4d1	pravidelný
návštěvníky	návštěvník	k1gMnPc4	návštěvník
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
Tom	Tom	k1gMnSc1	Tom
Cruise	Cruise	k1gFnSc2	Cruise
<g/>
.	.	kIx.	.
</s>
<s>
Ankara	Ankara	k1gFnSc1	Ankara
<g/>
,	,	kIx,	,
Turecko	Turecko	k1gNnSc1	Turecko
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
Astana	Astana	k1gFnSc1	Astana
<g/>
,	,	kIx,	,
Kazachstán	Kazachstán	k1gInSc1	Kazachstán
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
Brisbane	Brisban	k1gMnSc5	Brisban
<g/>
,	,	kIx,	,
Austrálie	Austrálie	k1gFnSc2	Austrálie
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
Budapešť	Budapešť	k1gFnSc1	Budapešť
<g/>
,	,	kIx,	,
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
Čchao-čou	Čchao-ča	k1gFnSc7	Čchao-ča
<g/>
,	,	kIx,	,
ČLR	ČLR	kA	ČLR
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
Čchung-čching	Čchung-čching	k1gInSc1	Čchung-čching
<g/>
,	,	kIx,	,
ČLR	ČLR	kA	ČLR
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Fukuoka	Fukuoka	k1gFnSc1	Fukuoka
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc1	Japonsko
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
George	Georg	k1gInSc2	Georg
Town	Town	k1gInSc1	Town
<g/>
,	,	kIx,	,
Malajsie	Malajsie	k1gFnSc1	Malajsie
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
Hanoj	Hanoj	k1gFnSc1	Hanoj
<g/>
,	,	kIx,	,
Vietnam	Vietnam	k1gInSc1	Vietnam
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
Jakarta	Jakarta	k1gFnSc1	Jakarta
<g/>
,	,	kIx,	,
Indonésie	Indonésie	k1gFnSc1	Indonésie
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
Kanton	Kanton	k1gInSc1	Kanton
<g/>
,	,	kIx,	,
ČLR	ČLR	kA	ČLR
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Lausanne	Lausanne	k1gNnSc1	Lausanne
<g/>
,	,	kIx,	,
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
Manila	Manila	k1gFnSc1	Manila
<g/>
,	,	kIx,	,
Filipíny	Filipíny	k1gFnPc1	Filipíny
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
Moskva	Moskva	k1gFnSc1	Moskva
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
Peking	Peking	k1gInSc1	Peking
<g/>
,	,	kIx,	,
ČLR	ČLR	kA	ČLR
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
Petrohrad	Petrohrad	k1gInSc1	Petrohrad
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
Phnom	Phnom	k1gInSc1	Phnom
Penh	Penh	k1gInSc1	Penh
<g/>
,	,	kIx,	,
Kambodža	Kambodža	k1gFnSc1	Kambodža
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
Prefektura	prefektura	k1gFnSc1	prefektura
Aiči	Aič	k1gFnSc2	Aič
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc1	Japonsko
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
Pusan	Pusan	k1gInSc1	Pusan
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc1d1	jižní
Korea	Korea	k1gFnSc1	Korea
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Soul	Soul	k1gInSc1	Soul
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc1d1	jižní
Korea	Korea	k1gFnSc1	Korea
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
Tchien-ťin	Tchien-ťin	k1gInSc1	Tchien-ťin
<g/>
,	,	kIx,	,
ČLR	ČLR	kA	ČLR
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
Vientiane	Vientian	k1gMnSc5	Vientian
<g/>
,	,	kIx,	,
Laos	Laos	k1gInSc1	Laos
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
Washington	Washington	k1gInSc1	Washington
D.C.	D.C.	k1gFnSc2	D.C.
<g/>
,	,	kIx,	,
USA	USA	kA	USA
(	(	kIx(	(
<g/>
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Bangkok	Bangkok	k1gInSc1	Bangkok
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Bangkok	Bangkok	k1gInSc1	Bangkok
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
