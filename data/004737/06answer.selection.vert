<s>
Kuzmányho	Kuzmányze	k6eAd1	Kuzmányze
kruh	kruh	k1gInSc1	kruh
evanjelických	evanjelický	k2eAgFnPc2d1	evanjelická
akademikov	akademikov	k1gInSc4	akademikov
byl	být	k5eAaImAgInS	být
slovenský	slovenský	k2eAgInSc1d1	slovenský
univerzitní	univerzitní	k2eAgInSc1d1	univerzitní
spolek	spolek	k1gInSc1	spolek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
založen	založen	k2eAgInSc1d1	založen
roku	rok	k1gInSc2	rok
1927	[number]	k4	1927
z	z	k7c2	z
iniciativy	iniciativa	k1gFnSc2	iniciativa
profesora	profesor	k1gMnSc2	profesor
Samuele	Samuela	k1gFnSc6	Samuela
Štefana	Štefan	k1gMnSc4	Štefan
Osuského	Osuský	k1gMnSc4	Osuský
<g/>
.	.	kIx.	.
</s>
