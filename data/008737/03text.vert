<p>
<s>
Srdce	srdce	k1gNnSc1	srdce
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
cor	cor	k?	cor
nebo	nebo	k8xC	nebo
cardia	cardium	k1gNnSc2	cardium
-	-	kIx~	-
z	z	k7c2	z
řec.	řec.	k?	řec.
κ	κ	k?	κ
<g/>
,	,	kIx,	,
kardia	kardium	k1gNnSc2	kardium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
dutý	dutý	k2eAgInSc1d1	dutý
svalový	svalový	k2eAgInSc1d1	svalový
orgán	orgán	k1gInSc1	orgán
živočichů	živočich	k1gMnPc2	živočich
s	s	k7c7	s
oběhovým	oběhový	k2eAgInSc7d1	oběhový
systémem	systém	k1gInSc7	systém
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
všech	všecek	k3xTgMnPc2	všecek
obratlovců	obratlovec	k1gMnPc2	obratlovec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
uložený	uložený	k2eAgInSc1d1	uložený
v	v	k7c6	v
hrudníku	hrudník	k1gInSc6	hrudník
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
svými	svůj	k3xOyFgInPc7	svůj
pravidelnými	pravidelný	k2eAgInPc7d1	pravidelný
stahy	stah	k1gInPc7	stah
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
oběh	oběh	k1gInSc4	oběh
hemolymfy	hemolymf	k1gInPc4	hemolymf
nebo	nebo	k8xC	nebo
krve	krev	k1gFnSc2	krev
tělem	tělo	k1gNnSc7	tělo
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
přenos	přenos	k1gInSc1	přenos
dýchacích	dýchací	k2eAgInPc2d1	dýchací
plynů	plyn	k1gInPc2	plyn
<g/>
,	,	kIx,	,
živin	živina	k1gFnPc2	živina
<g/>
,	,	kIx,	,
odpadních	odpadní	k2eAgFnPc2d1	odpadní
látek	látka	k1gFnPc2	látka
a	a	k8xC	a
další	další	k2eAgFnSc2d1	další
funkce	funkce	k1gFnSc2	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
srdce	srdce	k1gNnSc2	srdce
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
různých	různý	k2eAgFnPc2d1	různá
skupin	skupina	k1gFnPc2	skupina
živočichů	živočich	k1gMnPc2	živočich
odlišná	odlišný	k2eAgFnSc1d1	odlišná
<g/>
.	.	kIx.	.
</s>
<s>
Nejjednodušší	jednoduchý	k2eAgNnPc4d3	nejjednodušší
srdce	srdce	k1gNnPc4	srdce
mají	mít	k5eAaImIp3nP	mít
někteří	některý	k3yIgMnPc1	některý
bezobratlí	bezobratlí	k1gMnPc1	bezobratlí
<g/>
,	,	kIx,	,
u	u	k7c2	u
nichž	jenž	k3xRgInPc2	jenž
rozšířený	rozšířený	k2eAgInSc1d1	rozšířený
pulsující	pulsující	k2eAgInSc4d1	pulsující
úsek	úsek	k1gInSc4	úsek
cévy	céva	k1gFnSc2	céva
funguje	fungovat	k5eAaImIp3nS	fungovat
jako	jako	k9	jako
peristaltické	peristaltický	k2eAgNnSc4d1	peristaltické
čerpadlo	čerpadlo	k1gNnSc4	čerpadlo
<g/>
.	.	kIx.	.
</s>
<s>
Srdce	srdce	k1gNnSc1	srdce
ptáků	pták	k1gMnPc2	pták
a	a	k8xC	a
savců	savec	k1gMnPc2	savec
má	mít	k5eAaImIp3nS	mít
čtyři	čtyři	k4xCgInPc4	čtyři
oddíly	oddíl	k1gInPc4	oddíl
s	s	k7c7	s
chlopněmi	chlopeň	k1gFnPc7	chlopeň
(	(	kIx(	(
<g/>
které	který	k3yRgNnSc4	který
fungují	fungovat	k5eAaImIp3nP	fungovat
jako	jako	k8xC	jako
ventily	ventil	k1gInPc1	ventil
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dva	dva	k4xCgMnPc1	dva
pro	pro	k7c4	pro
plicní	plicní	k2eAgInSc4d1	plicní
oběh	oběh	k1gInSc4	oběh
(	(	kIx(	(
<g/>
zajišťující	zajišťující	k2eAgInSc4d1	zajišťující
průtok	průtok	k1gInSc4	průtok
krve	krev	k1gFnPc4	krev
plícemi	plíce	k1gFnPc7	plíce
a	a	k8xC	a
její	její	k3xOp3gNnSc4	její
okysličování	okysličování	k1gNnSc4	okysličování
<g/>
)	)	kIx)	)
a	a	k8xC	a
dva	dva	k4xCgMnPc1	dva
pro	pro	k7c4	pro
oběh	oběh	k1gInSc4	oběh
hlavní	hlavní	k2eAgInSc4d1	hlavní
(	(	kIx(	(
<g/>
zajišťující	zajišťující	k2eAgInSc4d1	zajišťující
průtok	průtok	k1gInSc4	průtok
okysličené	okysličený	k2eAgFnSc2d1	okysličená
krve	krev	k1gFnSc2	krev
tělem	tělo	k1gNnSc7	tělo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stavbou	stavba	k1gFnSc7	stavba
<g/>
,	,	kIx,	,
funkcí	funkce	k1gFnSc7	funkce
a	a	k8xC	a
chorobami	choroba	k1gFnPc7	choroba
lidského	lidský	k2eAgNnSc2d1	lidské
srdce	srdce	k1gNnSc2	srdce
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
kardiologie	kardiologie	k1gFnSc2	kardiologie
<g/>
,	,	kIx,	,
srdeční	srdeční	k2eAgFnSc2d1	srdeční
choroby	choroba	k1gFnSc2	choroba
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejčastější	častý	k2eAgFnPc4d3	nejčastější
příčiny	příčina	k1gFnPc4	příčina
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
bohatých	bohatý	k2eAgFnPc6d1	bohatá
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Uložení	uložení	k1gNnSc1	uložení
srdce	srdce	k1gNnSc2	srdce
==	==	k?	==
</s>
</p>
<p>
<s>
U	u	k7c2	u
savců	savec	k1gMnPc2	savec
je	být	k5eAaImIp3nS	být
srdce	srdce	k1gNnSc1	srdce
uloženo	uložit	k5eAaPmNgNnS	uložit
v	v	k7c6	v
středohrudí	středohrudí	k1gNnSc6	středohrudí
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
v	v	k7c6	v
hrudníku	hrudník	k1gInSc6	hrudník
mezi	mezi	k7c7	mezi
plícemi	plíce	k1gFnPc7	plíce
(	(	kIx(	(
<g/>
pulmo	pulmo	k6eAd1	pulmo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hrudní	hrudní	k2eAgMnPc1d1	hrudní
kostí	kost	k1gFnPc2	kost
(	(	kIx(	(
<g/>
sternum	sternum	k1gNnSc1	sternum
<g/>
)	)	kIx)	)
a	a	k8xC	a
bránicí	bránice	k1gFnSc7	bránice
(	(	kIx(	(
<g/>
diaphragma	diaphragma	k1gFnSc1	diaphragma
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zevně	zevně	k6eAd1	zevně
je	být	k5eAaImIp3nS	být
kryto	krýt	k5eAaImNgNnS	krýt
vazivovým	vazivový	k2eAgInSc7d1	vazivový
obalem	obal	k1gInSc7	obal
<g/>
,	,	kIx,	,
osrdečníkem	osrdečník	k1gInSc7	osrdečník
(	(	kIx(	(
<g/>
perikard	perikard	k1gInSc1	perikard
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
duplikatura	duplikatura	k1gFnSc1	duplikatura
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
povázky	povázka	k1gFnSc2	povázka
hrudní	hrudní	k2eAgFnSc2d1	hrudní
dutiny	dutina	k1gFnSc2	dutina
<g/>
,	,	kIx,	,
pokrývající	pokrývající	k2eAgNnSc1d1	pokrývající
prakticky	prakticky	k6eAd1	prakticky
celé	celý	k2eAgNnSc1d1	celé
srdce	srdce	k1gNnSc1	srdce
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k7c2	uvnitř
perikardu	perikard	k1gInSc2	perikard
je	být	k5eAaImIp3nS	být
tenká	tenký	k2eAgFnSc1d1	tenká
dvojitá	dvojitý	k2eAgFnSc1d1	dvojitá
membrána	membrána	k1gFnSc1	membrána
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
úzkém	úzký	k2eAgInSc6d1	úzký
dvojitém	dvojitý	k2eAgInSc6d1	dvojitý
vaku	vak	k1gInSc6	vak
je	být	k5eAaImIp3nS	být
perikardiální	perikardiální	k2eAgInSc4d1	perikardiální
prostor	prostor	k1gInSc4	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
malá	malý	k2eAgFnSc1d1	malá
vrstva	vrstva	k1gFnSc1	vrstva
tekutiny	tekutina	k1gFnSc2	tekutina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
chrání	chránit	k5eAaImIp3nS	chránit
srdce	srdce	k1gNnSc4	srdce
před	před	k7c7	před
třením	tření	k1gNnSc7	tření
a	a	k8xC	a
nárazy	náraz	k1gInPc7	náraz
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k6eAd1	uvnitř
je	být	k5eAaImIp3nS	být
srdce	srdce	k1gNnSc1	srdce
vystláno	vystlat	k5eAaPmNgNnS	vystlat
další	další	k2eAgFnSc7d1	další
vrstvou	vrstva	k1gFnSc7	vrstva
–	–	k?	–
endokardem	endokard	k1gInSc7	endokard
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
celé	celý	k2eAgNnSc4d1	celé
srdce	srdce	k1gNnPc4	srdce
hydraulicky	hydraulicky	k6eAd1	hydraulicky
utěsňuje	utěsňovat	k5eAaImIp3nS	utěsňovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c7	mezi
endokardem	endokard	k1gInSc7	endokard
a	a	k8xC	a
perikardem	perikard	k1gInSc7	perikard
je	být	k5eAaImIp3nS	být
vlastní	vlastní	k2eAgInSc1d1	vlastní
zdroj	zdroj	k1gInSc1	zdroj
síly	síla	k1gFnSc2	síla
srdce	srdce	k1gNnSc2	srdce
–	–	k?	–
myokard	myokard	k1gInSc1	myokard
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
příčně	příčně	k6eAd1	příčně
pruhovaným	pruhovaný	k2eAgNnSc7d1	pruhované
svalstvem	svalstvo	k1gNnSc7	svalstvo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
není	být	k5eNaImIp3nS	být
ovlivňováno	ovlivňován	k2eAgNnSc4d1	ovlivňováno
vůlí	vůle	k1gFnSc7	vůle
<g/>
.	.	kIx.	.
</s>
<s>
Stěny	stěna	k1gFnPc1	stěna
síní	síň	k1gFnPc2	síň
mají	mít	k5eAaImIp3nP	mít
méně	málo	k6eAd2	málo
svaloviny	svalovina	k1gFnPc4	svalovina
než	než	k8xS	než
stěny	stěna	k1gFnPc4	stěna
komor	komora	k1gFnPc2	komora
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
vykonávají	vykonávat	k5eAaImIp3nP	vykonávat
menší	malý	k2eAgFnSc4d2	menší
činnost	činnost	k1gFnSc4	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
nervy	nerv	k1gInPc4	nerv
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
určují	určovat	k5eAaImIp3nP	určovat
rytmus	rytmus	k1gInSc4	rytmus
srdečního	srdeční	k2eAgInSc2d1	srdeční
tepu	tep	k1gInSc2	tep
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
středohrudí	středohrudí	k1gNnSc6	středohrudí
je	být	k5eAaImIp3nS	být
srdce	srdce	k1gNnSc1	srdce
fixováno	fixován	k2eAgNnSc1d1	fixováno
hlavně	hlavně	k9	hlavně
pomocí	pomocí	k7c2	pomocí
velkých	velký	k2eAgFnPc2d1	velká
cév	céva	k1gFnPc2	céva
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vycházejí	vycházet	k5eAaImIp3nP	vycházet
z	z	k7c2	z
jeho	jeho	k3xOp3gFnSc2	jeho
základny	základna	k1gFnSc2	základna
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
hlavně	hlavně	k9	hlavně
obloukem	oblouk	k1gInSc7	oblouk
aorty	aorta	k1gFnSc2	aorta
<g/>
,	,	kIx,	,
plicním	plicní	k2eAgInSc7d1	plicní
kmenem	kmen	k1gInSc7	kmen
a	a	k8xC	a
plicními	plicní	k2eAgFnPc7d1	plicní
žilami	žíla	k1gFnPc7	žíla
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
z	z	k7c2	z
perikardu	perikard	k1gInSc2	perikard
vychází	vycházet	k5eAaImIp3nS	vycházet
vaz	vaz	k1gInSc1	vaz
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
spojuje	spojovat	k5eAaImIp3nS	spojovat
srdce	srdce	k1gNnSc4	srdce
s	s	k7c7	s
hrudní	hrudní	k2eAgFnSc7d1	hrudní
kostí	kost	k1gFnSc7	kost
(	(	kIx(	(
<g/>
lig	liga	k1gFnPc2	liga
<g/>
.	.	kIx.	.
sternopericardiacum	sternopericardiacum	k1gInSc1	sternopericardiacum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
kopytníků	kopytník	k1gInPc2	kopytník
a	a	k8xC	a
velkých	velký	k2eAgNnPc2d1	velké
plemen	plemeno	k1gNnPc2	plemeno
psů	pes	k1gMnPc2	pes
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
malých	malý	k2eAgNnPc2d1	malé
zvířat	zvíře	k1gNnPc2	zvíře
(	(	kIx(	(
<g/>
kočky	kočka	k1gFnPc1	kočka
<g/>
,	,	kIx,	,
malí	malý	k2eAgMnPc1d1	malý
psi	pes	k1gMnPc1	pes
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
srdce	srdce	k1gNnSc1	srdce
spojeno	spojit	k5eAaPmNgNnS	spojit
vazem	vaz	k1gInSc7	vaz
s	s	k7c7	s
bránicí	bránice	k1gFnSc7	bránice
(	(	kIx(	(
<g/>
lig	liga	k1gFnPc2	liga
<g/>
.	.	kIx.	.
phrenicopericardiacum	phrenicopericardiacum	k1gInSc1	phrenicopericardiacum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
U	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
se	se	k3xPyFc4	se
srdce	srdce	k1gNnSc2	srdce
nachází	nacházet	k5eAaImIp3nS	nacházet
asi	asi	k9	asi
ze	z	k7c2	z
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
vlevo	vlevo	k6eAd1	vlevo
<g/>
,	,	kIx,	,
u	u	k7c2	u
zvířat	zvíře	k1gNnPc2	zvíře
je	být	k5eAaImIp3nS	být
srdce	srdce	k1gNnSc1	srdce
více	hodně	k6eAd2	hodně
uprostřed	uprostřed	k6eAd1	uprostřed
<g/>
,	,	kIx,	,
např.	např.	kA	např.
u	u	k7c2	u
psa	pes	k1gMnSc2	pes
je	být	k5eAaImIp3nS	být
srdce	srdce	k1gNnSc4	srdce
vlevo	vlevo	k6eAd1	vlevo
ze	z	k7c2	z
4	[number]	k4	4
<g/>
/	/	kIx~	/
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
u	u	k7c2	u
skotu	skot	k1gInSc2	skot
a	a	k8xC	a
u	u	k7c2	u
koně	kůň	k1gMnSc2	kůň
jsou	být	k5eAaImIp3nP	být
vlevo	vlevo	k6eAd1	vlevo
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
5	[number]	k4	5
srdce	srdce	k1gNnSc2	srdce
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Vlevo	vlevo	k6eAd1	vlevo
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
vpravo	vpravo	k6eAd1	vpravo
<g/>
"	"	kIx"	"
nikoli	nikoli	k9	nikoli
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
pozorovatele	pozorovatel	k1gMnSc2	pozorovatel
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
nositele	nositel	k1gMnSc2	nositel
srdce	srdce	k1gNnSc2	srdce
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc1	popis
srdce	srdce	k1gNnSc2	srdce
==	==	k?	==
</s>
</p>
<p>
<s>
Srdce	srdce	k1gNnSc1	srdce
má	mít	k5eAaImIp3nS	mít
tvar	tvar	k1gInSc4	tvar
kužele	kužel	k1gInSc2	kužel
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
hrot	hrot	k1gInSc1	hrot
(	(	kIx(	(
<g/>
apex	apex	k1gInSc1	apex
<g/>
)	)	kIx)	)
směřuje	směřovat	k5eAaImIp3nS	směřovat
doleva	doleva	k6eAd1	doleva
a	a	k8xC	a
dolů	dolů	k6eAd1	dolů
<g/>
.	.	kIx.	.
</s>
<s>
Srdeční	srdeční	k2eAgFnSc1d1	srdeční
základna	základna	k1gFnSc1	základna
je	být	k5eAaImIp3nS	být
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
ústí	ústit	k5eAaImIp3nS	ústit
cévy	céva	k1gFnPc4	céva
vstupující	vstupující	k2eAgFnPc4d1	vstupující
a	a	k8xC	a
vystupující	vystupující	k2eAgFnPc4d1	vystupující
do	do	k7c2	do
srdce	srdce	k1gNnSc2	srdce
<g/>
,	,	kIx,	,
podkladem	podklad	k1gInSc7	podklad
hrotu	hrot	k1gInSc2	hrot
je	být	k5eAaImIp3nS	být
hlavně	hlavně	k9	hlavně
levá	levý	k2eAgFnSc1d1	levá
komora	komora	k1gFnSc1	komora
<g/>
.	.	kIx.	.
</s>
<s>
Levá	levý	k2eAgFnSc1d1	levá
plocha	plocha	k1gFnSc1	plocha
srdce	srdce	k1gNnSc2	srdce
je	být	k5eAaImIp3nS	být
přivrácená	přivrácený	k2eAgFnSc1d1	přivrácená
k	k	k7c3	k
hrudní	hrudní	k2eAgFnSc3d1	hrudní
kosti	kost	k1gFnSc3	kost
a	a	k8xC	a
k	k	k7c3	k
žebrům	žebr	k1gInPc3	žebr
(	(	kIx(	(
<g/>
facies	facies	k1gFnSc2	facies
sternocostalis	sternocostalis	k1gFnSc2	sternocostalis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pravá	pravý	k2eAgFnSc1d1	pravá
plocha	plocha	k1gFnSc1	plocha
srdce	srdce	k1gNnSc2	srdce
je	být	k5eAaImIp3nS	být
přivrácená	přivrácený	k2eAgFnSc1d1	přivrácená
k	k	k7c3	k
bránici	bránice	k1gFnSc3	bránice
(	(	kIx(	(
<g/>
facies	facies	k1gFnSc1	facies
diaphragmatica	diaphragmatica	k1gMnSc1	diaphragmatica
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
veterinární	veterinární	k2eAgFnSc6d1	veterinární
anatomii	anatomie	k1gFnSc6	anatomie
se	se	k3xPyFc4	se
plocha	plocha	k1gFnSc1	plocha
srdce	srdce	k1gNnSc2	srdce
směřující	směřující	k2eAgFnSc7d1	směřující
k	k	k7c3	k
levému	levý	k2eAgInSc3d1	levý
boku	bok	k1gInSc3	bok
nazývá	nazývat	k5eAaImIp3nS	nazývat
strana	strana	k1gFnSc1	strana
oušková	ouškový	k2eAgFnSc1d1	ouškový
(	(	kIx(	(
<g/>
facies	facies	k1gFnSc1	facies
auricularis	auricularis	k1gFnSc2	auricularis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pravá	pravý	k2eAgFnSc1d1	pravá
plocha	plocha	k1gFnSc1	plocha
srdce	srdce	k1gNnSc2	srdce
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
plocha	plocha	k1gFnSc1	plocha
síňová	síňový	k2eAgFnSc1d1	síňová
(	(	kIx(	(
<g/>
facies	facies	k1gFnSc1	facies
atrialis	atrialis	k1gFnSc2	atrialis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Srdce	srdce	k1gNnSc1	srdce
savců	savec	k1gMnPc2	savec
je	být	k5eAaImIp3nS	být
uvnitř	uvnitř	k6eAd1	uvnitř
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
na	na	k7c4	na
4	[number]	k4	4
samostatné	samostatný	k2eAgFnPc4d1	samostatná
dutiny	dutina	k1gFnPc4	dutina
<g/>
.	.	kIx.	.
</s>
<s>
Přepážky	přepážka	k1gFnPc1	přepážka
mezi	mezi	k7c7	mezi
dutinami	dutina	k1gFnPc7	dutina
jsou	být	k5eAaImIp3nP	být
zevně	zevně	k6eAd1	zevně
naznačeny	naznačen	k2eAgInPc4d1	naznačen
mělkými	mělký	k2eAgInPc7d1	mělký
zářezy	zářez	k1gInPc7	zářez
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
srdce	srdce	k1gNnSc2	srdce
<g/>
.	.	kIx.	.
</s>
<s>
Srdce	srdce	k1gNnSc1	srdce
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
základny	základna	k1gFnSc2	základna
<g/>
,	,	kIx,	,
obaleno	obalen	k2eAgNnSc1d1	obaleno
funkčním	funkční	k2eAgInSc7d1	funkční
tukem	tuk	k1gInSc7	tuk
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vyrovnává	vyrovnávat	k5eAaImIp3nS	vyrovnávat
nerovnosti	nerovnost	k1gFnSc2	nerovnost
srdečního	srdeční	k2eAgInSc2d1	srdeční
povrchu	povrch	k1gInSc2	povrch
a	a	k8xC	a
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
tak	tak	k6eAd1	tak
jeho	jeho	k3xOp3gNnSc4	jeho
klouzání	klouzání	k1gNnSc4	klouzání
v	v	k7c6	v
dutině	dutina	k1gFnSc6	dutina
osrdečníku	osrdečník	k1gInSc2	osrdečník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Dutiny	dutina	k1gFnPc1	dutina
srdce	srdce	k1gNnSc2	srdce
===	===	k?	===
</s>
</p>
<p>
<s>
Krev	krev	k1gFnSc1	krev
při	při	k7c6	při
průchodu	průchod	k1gInSc6	průchod
srdcem	srdce	k1gNnSc7	srdce
protéká	protékat	k5eAaImIp3nS	protékat
dutinami	dutina	k1gFnPc7	dutina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
navzájem	navzájem	k6eAd1	navzájem
odděleny	oddělen	k2eAgFnPc1d1	oddělena
chlopněmi	chlopeň	k1gFnPc7	chlopeň
<g/>
,	,	kIx,	,
zabraňujícími	zabraňující	k2eAgFnPc7d1	zabraňující
zpětnému	zpětný	k2eAgInSc3d1	zpětný
toku	tok	k1gInSc3	tok
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Náraz	náraz	k1gInSc4	náraz
krve	krev	k1gFnSc2	krev
na	na	k7c4	na
uzavřené	uzavřený	k2eAgFnPc4d1	uzavřená
chlopně	chlopeň	k1gFnPc4	chlopeň
při	při	k7c6	při
systole	systola	k1gFnSc6	systola
slyšíme	slyšet	k5eAaImIp1nP	slyšet
jako	jako	k9	jako
srdeční	srdeční	k2eAgInPc4d1	srdeční
ozvy	ozev	k1gInPc4	ozev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Neokysličená	okysličený	k2eNgFnSc1d1	neokysličená
krev	krev	k1gFnSc1	krev
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
srdce	srdce	k1gNnSc2	srdce
přiváděna	přiváděn	k2eAgFnSc1d1	přiváděna
dutými	dutý	k2eAgFnPc7d1	dutá
žilami	žíla	k1gFnPc7	žíla
(	(	kIx(	(
<g/>
venae	venae	k1gInSc1	venae
cavae	cava	k1gInSc2	cava
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ty	ten	k3xDgFnPc1	ten
jsou	být	k5eAaImIp3nP	být
dvě	dva	k4xCgFnPc1	dva
<g/>
:	:	kIx,	:
horní	horní	k2eAgFnSc1d1	horní
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
přivádí	přivádět	k5eAaImIp3nS	přivádět
krev	krev	k1gFnSc4	krev
z	z	k7c2	z
horní	horní	k2eAgFnSc2d1	horní
části	část	k1gFnSc2	část
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
a	a	k8xC	a
dolní	dolní	k2eAgFnSc1d1	dolní
<g/>
.	.	kIx.	.
</s>
<s>
Duté	dutý	k2eAgFnPc1d1	dutá
žíly	žíla	k1gFnPc1	žíla
se	se	k3xPyFc4	se
před	před	k7c7	před
srdcem	srdce	k1gNnSc7	srdce
slévají	slévat	k5eAaImIp3nP	slévat
v	v	k7c6	v
žilném	žilný	k2eAgInSc6d1	žilný
splavu	splav	k1gInSc6	splav
(	(	kIx(	(
<g/>
sinus	sinus	k1gInSc1	sinus
venarum	venarum	k1gNnSc1	venarum
cavarum	cavarum	k1gInSc1	cavarum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Pravá	pravý	k2eAgFnSc1d1	pravá
síň	síň	k1gFnSc1	síň
====	====	k?	====
</s>
</p>
<p>
<s>
Z	z	k7c2	z
žilného	žilný	k2eAgInSc2d1	žilný
splavu	splav	k1gInSc2	splav
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
horní	horní	k2eAgFnPc4d1	horní
a	a	k8xC	a
dolní	dolní	k2eAgFnSc7d1	dolní
dutou	dutý	k2eAgFnSc7d1	dutá
žilou	žíla	k1gFnSc7	žíla
<g/>
,	,	kIx,	,
krev	krev	k1gFnSc1	krev
odtéká	odtékat	k5eAaImIp3nS	odtékat
do	do	k7c2	do
pravé	pravý	k2eAgFnSc2d1	pravá
síně	síň	k1gFnSc2	síň
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
atrium	atrium	k1gNnSc1	atrium
dextrum	dextrum	k1gNnSc1	dextrum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pravá	pravý	k2eAgFnSc1d1	pravá
síň	síň	k1gFnSc1	síň
tvoří	tvořit	k5eAaImIp3nS	tvořit
pravou	pravý	k2eAgFnSc4d1	pravá
polovinu	polovina	k1gFnSc4	polovina
srdeční	srdeční	k2eAgFnSc2d1	srdeční
základny	základna	k1gFnSc2	základna
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
relativně	relativně	k6eAd1	relativně
tenkou	tenký	k2eAgFnSc4d1	tenká
svalovou	svalový	k2eAgFnSc4d1	svalová
stěnu	stěna	k1gFnSc4	stěna
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
odvádí	odvádět	k5eAaImIp3nS	odvádět
menší	malý	k2eAgFnSc1d2	menší
část	část	k1gFnSc1	část
práce	práce	k1gFnSc1	práce
než	než	k8xS	než
levá	levý	k2eAgFnSc1d1	levá
polovina	polovina	k1gFnSc1	polovina
<g/>
,	,	kIx,	,
a	a	k8xC	a
vybíhá	vybíhat	k5eAaImIp3nS	vybíhat
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
srdce	srdce	k1gNnSc2	srdce
v	v	k7c4	v
jakýsi	jakýsi	k3yIgInSc4	jakýsi
svalový	svalový	k2eAgInSc4d1	svalový
vak	vak	k1gInSc4	vak
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
ouško	ouško	k1gNnSc1	ouško
(	(	kIx(	(
<g/>
auricula	auricula	k1gFnSc1	auricula
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
plocha	plocha	k1gFnSc1	plocha
síní	síň	k1gFnPc2	síň
není	být	k5eNaImIp3nS	být
hladká	hladký	k2eAgFnSc1d1	hladká
<g/>
,	,	kIx,	,
vybíhá	vybíhat	k5eAaImIp3nS	vybíhat
ve	v	k7c4	v
svalové	svalový	k2eAgInPc4d1	svalový
trámce	trámec	k1gInPc4	trámec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přepážce	přepážka	k1gFnSc6	přepážka
mezi	mezi	k7c7	mezi
pravou	pravý	k2eAgFnSc7d1	pravá
a	a	k8xC	a
levou	levý	k2eAgFnSc7d1	levá
síní	síň	k1gFnSc7	síň
je	být	k5eAaImIp3nS	být
místo	místo	k1gNnSc1	místo
se	s	k7c7	s
zúženou	zúžený	k2eAgFnSc7d1	zúžená
stěnou	stěna	k1gFnSc7	stěna
<g/>
,	,	kIx,	,
pozůstatek	pozůstatek	k1gInSc1	pozůstatek
po	po	k7c4	po
propojení	propojení	k1gNnSc4	propojení
síní	síň	k1gFnPc2	síň
u	u	k7c2	u
plodu	plod	k1gInSc2	plod
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
nevzdušné	vzdušný	k2eNgFnPc4d1	nevzdušná
plíce	plíce	k1gFnPc4	plíce
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
porodu	porod	k1gInSc6	porod
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
rychle	rychle	k6eAd1	rychle
uzavírá	uzavírat	k5eAaImIp3nS	uzavírat
<g/>
,	,	kIx,	,
jestliže	jestliže	k8xS	jestliže
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
nestane	stanout	k5eNaPmIp3nS	stanout
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
otvor	otvor	k1gInSc4	otvor
uzavřít	uzavřít	k5eAaPmF	uzavřít
chirurgicky	chirurgicky	k6eAd1	chirurgicky
<g/>
.	.	kIx.	.
</s>
<s>
Řadí	řadit	k5eAaImIp3nS	řadit
se	se	k3xPyFc4	se
mezi	mezi	k7c4	mezi
lehčí	lehký	k2eAgFnPc4d2	lehčí
srdeční	srdeční	k2eAgFnPc4d1	srdeční
vady	vada	k1gFnPc4	vada
(	(	kIx(	(
<g/>
foramen	foramen	k2eAgInSc1d1	foramen
ovale	ovale	k1gInSc1	ovale
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pravá	pravý	k2eAgFnSc1d1	pravá
síň	síň	k1gFnSc1	síň
je	být	k5eAaImIp3nS	být
oddělena	oddělit	k5eAaPmNgFnS	oddělit
od	od	k7c2	od
pravé	pravý	k2eAgFnSc2d1	pravá
komory	komora	k1gFnSc2	komora
(	(	kIx(	(
<g/>
ventriculus	ventriculus	k1gInSc1	ventriculus
dexter	dextra	k1gFnPc2	dextra
<g/>
)	)	kIx)	)
síňokomorovou	síňokomorův	k2eAgFnSc7d1	síňokomorův
přepážkou	přepážka	k1gFnSc7	přepážka
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
je	být	k5eAaImIp3nS	být
otvor	otvor	k1gInSc1	otvor
opatřený	opatřený	k2eAgInSc1d1	opatřený
trojcípou	trojcípý	k2eAgFnSc7d1	trojcípá
chlopní	chlopeň	k1gFnSc7	chlopeň
(	(	kIx(	(
<g/>
valva	valva	k1gFnSc1	valva
atrioventricularis	atrioventricularis	k1gFnSc1	atrioventricularis
dextra	dextra	k1gFnSc1	dextra
<g/>
,	,	kIx,	,
tricuspidalis	tricuspidalis	k1gFnSc1	tricuspidalis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Trojcípá	trojcípý	k2eAgFnSc1d1	trojcípá
(	(	kIx(	(
<g/>
trikuspidální	trikuspidální	k2eAgFnSc1d1	trikuspidální
<g/>
)	)	kIx)	)
chlopeň	chlopeň	k1gFnSc1	chlopeň
střeží	střežit	k5eAaImIp3nS	střežit
vstup	vstup	k1gInSc4	vstup
do	do	k7c2	do
pravé	pravý	k2eAgFnSc2d1	pravá
komory	komora	k1gFnSc2	komora
a	a	k8xC	a
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
při	při	k7c6	při
stahu	stah	k1gInSc6	stah
(	(	kIx(	(
<g/>
systole	systola	k1gFnSc6	systola
<g/>
)	)	kIx)	)
krev	krev	k1gFnSc1	krev
z	z	k7c2	z
komory	komora	k1gFnSc2	komora
nemohla	moct	k5eNaImAgFnS	moct
vracet	vracet	k5eAaImF	vracet
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
síně	síň	k1gFnSc2	síň
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgInPc4	tři
cípy	cíp	k1gInPc4	cíp
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
název	název	k1gInSc4	název
trojcípá	trojcípý	k2eAgFnSc1d1	trojcípá
<g/>
.	.	kIx.	.
</s>
<s>
Pracuje	pracovat	k5eAaImIp3nS	pracovat
jednoduchým	jednoduchý	k2eAgInSc7d1	jednoduchý
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
chlopeň	chlopeň	k1gFnSc1	chlopeň
mitrální	mitrální	k2eAgFnSc1d1	mitrální
v	v	k7c6	v
levé	levý	k2eAgFnSc6d1	levá
komoře	komora	k1gFnSc6	komora
<g/>
.	.	kIx.	.
</s>
<s>
Svalové	svalový	k2eAgNnSc1d1	svalové
napětí	napětí	k1gNnSc1	napětí
při	při	k7c6	při
stahu	stah	k1gInSc6	stah
(	(	kIx(	(
<g/>
kontrakci	kontrakce	k1gFnSc4	kontrakce
<g/>
)	)	kIx)	)
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
v	v	k7c6	v
komorách	komora	k1gFnPc6	komora
tlak	tlak	k1gInSc1	tlak
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
stlačuje	stlačovat	k5eAaImIp3nS	stlačovat
chlopenné	chlopenný	k2eAgInPc4d1	chlopenný
cípy	cíp	k1gInPc4	cíp
k	k	k7c3	k
sobě	se	k3xPyFc3	se
a	a	k8xC	a
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
tak	tak	k6eAd1	tak
těsný	těsný	k2eAgInSc1d1	těsný
uzávěr	uzávěr	k1gInSc1	uzávěr
<g/>
.	.	kIx.	.
</s>
<s>
Otevírání	otevírání	k1gNnSc1	otevírání
a	a	k8xC	a
uzavírání	uzavírání	k1gNnSc1	uzavírání
srdečních	srdeční	k2eAgFnPc2d1	srdeční
chlopní	chlopeň	k1gFnPc2	chlopeň
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
"	"	kIx"	"
<g/>
tlukot	tlukot	k1gInSc1	tlukot
srdce	srdce	k1gNnSc2	srdce
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
poslouchat	poslouchat	k5eAaImF	poslouchat
stetoskopem	stetoskop	k1gInSc7	stetoskop
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Pravá	pravý	k2eAgFnSc1d1	pravá
komora	komora	k1gFnSc1	komora
====	====	k?	====
</s>
</p>
<p>
<s>
Pravá	pravý	k2eAgFnSc1d1	pravá
komora	komora	k1gFnSc1	komora
leží	ležet	k5eAaImIp3nS	ležet
pod	pod	k7c7	pod
pravou	pravý	k2eAgFnSc7d1	pravá
síní	síň	k1gFnSc7	síň
<g/>
,	,	kIx,	,
nedosahuje	dosahovat	k5eNaImIp3nS	dosahovat
ale	ale	k9	ale
až	až	k9	až
do	do	k7c2	do
srdečního	srdeční	k2eAgInSc2d1	srdeční
hrotu	hrot	k1gInSc2	hrot
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
tenčí	tenký	k2eAgFnSc4d2	tenčí
stěnu	stěna	k1gFnSc4	stěna
než	než	k8xS	než
levá	levý	k2eAgFnSc1d1	levá
komora	komora	k1gFnSc1	komora
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
silnější	silný	k2eAgMnSc1d2	silnější
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
stěna	stěna	k1gFnSc1	stěna
síní	síň	k1gFnPc2	síň
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
je	být	k5eAaImIp3nS	být
stěna	stěna	k1gFnSc1	stěna
asi	asi	k9	asi
0,5	[number]	k4	0,5
cm	cm	kA	cm
tlustá	tlustý	k2eAgFnSc1d1	tlustá
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k7c2	uvnitř
komory	komora	k1gFnSc2	komora
myokard	myokard	k1gInSc1	myokard
vybíhá	vybíhat	k5eAaImIp3nS	vybíhat
do	do	k7c2	do
prostoru	prostor	k1gInSc2	prostor
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nP	tvořit
bradavkovité	bradavkovitý	k2eAgInPc4d1	bradavkovitý
svaly	sval	k1gInPc4	sval
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yRgMnPc4	který
se	se	k3xPyFc4	se
upínají	upínat	k5eAaImIp3nP	upínat
šlašinky	šlašinka	k1gFnPc1	šlašinka
<g/>
,	,	kIx,	,
vazivové	vazivový	k2eAgFnPc1d1	vazivová
struny	struna	k1gFnPc1	struna
rozepjaté	rozepjatý	k2eAgFnPc1d1	rozepjatá
mezi	mezi	k7c7	mezi
stěnou	stěna	k1gFnSc7	stěna
komory	komora	k1gFnPc1	komora
a	a	k8xC	a
cípy	cíp	k1gInPc1	cíp
trojcípé	trojcípý	k2eAgFnSc2d1	trojcípá
chlopně	chlopeň	k1gFnSc2	chlopeň
<g/>
.	.	kIx.	.
</s>
<s>
Šlašinky	Šlašink	k1gInPc1	Šlašink
zabraňují	zabraňovat	k5eAaImIp3nP	zabraňovat
vyvrácení	vyvrácení	k1gNnSc4	vyvrácení
chlopně	chlopeň	k1gFnSc2	chlopeň
při	při	k7c6	při
stahu	stah	k1gInSc6	stah
komory	komora	k1gFnSc2	komora
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pravá	pravý	k2eAgFnSc1d1	pravá
komora	komora	k1gFnSc1	komora
vyúsťuje	vyúsťovat	k5eAaImIp3nS	vyúsťovat
do	do	k7c2	do
plicního	plicní	k2eAgInSc2d1	plicní
kmene	kmen	k1gInSc2	kmen
(	(	kIx(	(
<g/>
truncus	truncus	k1gInSc1	truncus
pulmonalis	pulmonalis	k1gFnSc2	pulmonalis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
otvor	otvor	k1gInSc1	otvor
uzavírá	uzavírat	k5eAaPmIp3nS	uzavírat
poloměsíčitou	poloměsíčitý	k2eAgFnSc7d1	poloměsíčitá
(	(	kIx(	(
<g/>
semilunární	semilunární	k2eAgFnSc7d1	semilunární
<g/>
)	)	kIx)	)
chlopní	chlopeň	k1gFnSc7	chlopeň
(	(	kIx(	(
<g/>
valva	valva	k6eAd1	valva
trunci	trunec	k1gInSc3	trunec
pulmonalis	pulmonalis	k1gFnSc2	pulmonalis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
chlopeň	chlopeň	k1gFnSc1	chlopeň
dostala	dostat	k5eAaPmAgFnS	dostat
své	svůj	k3xOyFgNnSc4	svůj
jméno	jméno	k1gNnSc4	jméno
podle	podle	k7c2	podle
podobnosti	podobnost	k1gFnSc2	podobnost
s	s	k7c7	s
půlměsícem	půlměsíc	k1gInSc7	půlměsíc
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nS	tvořit
jakési	jakýsi	k3yIgFnSc2	jakýsi
kapsy	kapsa	k1gFnSc2	kapsa
<g/>
.	.	kIx.	.
</s>
<s>
Plicní	plicní	k2eAgFnSc1d1	plicní
chlopeň	chlopeň	k1gFnSc1	chlopeň
dovoluje	dovolovat	k5eAaImIp3nS	dovolovat
krvi	krev	k1gFnSc3	krev
protékat	protékat	k5eAaImF	protékat
z	z	k7c2	z
pravé	pravý	k2eAgFnSc2d1	pravá
komory	komora	k1gFnSc2	komora
do	do	k7c2	do
hlavní	hlavní	k2eAgFnSc2d1	hlavní
tepny	tepna	k1gFnSc2	tepna
plicního	plicní	k2eAgInSc2d1	plicní
oběhu	oběh	k1gInSc2	oběh
(	(	kIx(	(
<g/>
plicnice	plicnice	k1gFnSc1	plicnice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
kontrakci	kontrakce	k1gFnSc6	kontrakce
komor	komora	k1gFnPc2	komora
se	se	k3xPyFc4	se
dostává	dostávat	k5eAaImIp3nS	dostávat
neokysličená	okysličený	k2eNgFnSc1d1	neokysličená
krev	krev	k1gFnSc1	krev
přes	přes	k7c4	přes
plicní	plicní	k2eAgFnSc4d1	plicní
chlopeň	chlopeň	k1gFnSc4	chlopeň
do	do	k7c2	do
plicnice	plicnice	k1gFnSc2	plicnice
a	a	k8xC	a
do	do	k7c2	do
plic	plíce	k1gFnPc2	plíce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Levá	levý	k2eAgFnSc1d1	levá
síň	síň	k1gFnSc1	síň
====	====	k?	====
</s>
</p>
<p>
<s>
Z	z	k7c2	z
plic	plíce	k1gFnPc2	plíce
přitéká	přitékat	k5eAaImIp3nS	přitékat
okysličená	okysličený	k2eAgFnSc1d1	okysličená
krev	krev	k1gFnSc1	krev
plicními	plicní	k2eAgFnPc7d1	plicní
žilami	žíla	k1gFnPc7	žíla
do	do	k7c2	do
levé	levý	k2eAgFnSc2d1	levá
síně	síň	k1gFnSc2	síň
<g/>
.	.	kIx.	.
</s>
<s>
Levá	levý	k2eAgFnSc1d1	levá
síň	síň	k1gFnSc1	síň
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
atrium	atrium	k1gNnSc1	atrium
sinistre	sinistr	k1gInSc5	sinistr
<g/>
)	)	kIx)	)
tvoří	tvořit	k5eAaImIp3nP	tvořit
levou	levý	k2eAgFnSc4d1	levá
polovinu	polovina	k1gFnSc4	polovina
srdeční	srdeční	k2eAgFnSc2d1	srdeční
základny	základna	k1gFnSc2	základna
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
pravá	pravý	k2eAgFnSc1d1	pravá
síň	síň	k1gFnSc1	síň
má	mít	k5eAaImIp3nS	mít
tenkou	tenký	k2eAgFnSc4d1	tenká
svalovou	svalový	k2eAgFnSc4d1	svalová
stěnu	stěna	k1gFnSc4	stěna
a	a	k8xC	a
vybíhá	vybíhat	k5eAaImIp3nS	vybíhat
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
srdce	srdce	k1gNnSc1	srdce
jako	jako	k8xS	jako
ouško	ouško	k1gNnSc1	ouško
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
síňokomorové	síňokomorový	k2eAgFnSc6d1	síňokomorový
přepážce	přepážka	k1gFnSc6	přepážka
je	být	k5eAaImIp3nS	být
otvor	otvor	k1gInSc1	otvor
opatřený	opatřený	k2eAgInSc1d1	opatřený
dvojcípou	dvojcípý	k2eAgFnSc7d1	dvojcípá
chlopní	chlopeň	k1gFnSc7	chlopeň
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
také	také	k6eAd1	také
nazývá	nazývat	k5eAaImIp3nS	nazývat
chlopeň	chlopeň	k1gFnSc1	chlopeň
mitrální	mitrální	k2eAgFnSc1d1	mitrální
(	(	kIx(	(
<g/>
valva	valva	k1gFnSc1	valva
atrioventricularis	atrioventricularis	k1gFnSc1	atrioventricularis
sinistra	sinistra	k1gFnSc1	sinistra
<g/>
,	,	kIx,	,
bicuspidalis	bicuspidalis	k1gFnSc1	bicuspidalis
<g/>
,	,	kIx,	,
mitralis	mitralis	k1gFnSc1	mitralis
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
podobnost	podobnost	k1gFnSc4	podobnost
s	s	k7c7	s
tvarem	tvar	k1gInSc7	tvar
biskupské	biskupský	k2eAgFnSc2d1	biskupská
mitry	mitra	k1gFnSc2	mitra
<g/>
.	.	kIx.	.
</s>
<s>
Pracuje	pracovat	k5eAaImIp3nS	pracovat
souběžně	souběžně	k6eAd1	souběžně
a	a	k8xC	a
stejným	stejný	k2eAgInSc7d1	stejný
způsobem	způsob	k1gInSc7	způsob
jako	jako	k8xC	jako
atriventriokulární	atriventriokulární	k2eAgFnPc1d1	atriventriokulární
chlopeň	chlopeň	k1gFnSc1	chlopeň
v	v	k7c6	v
pravém	pravý	k2eAgNnSc6d1	pravé
srdci	srdce	k1gNnSc6	srdce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Levá	levý	k2eAgFnSc1d1	levá
komora	komora	k1gFnSc1	komora
====	====	k?	====
</s>
</p>
<p>
<s>
Levá	levý	k2eAgFnSc1d1	levá
komora	komora	k1gFnSc1	komora
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
ventriculus	ventriculus	k1gMnSc1	ventriculus
sinister	sinister	k1gMnSc1	sinister
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
srdečních	srdeční	k2eAgFnPc2d1	srdeční
dutin	dutina	k1gFnPc2	dutina
nejtlustší	tlustý	k2eAgFnSc4d3	nejtlustší
stěnu	stěna	k1gFnSc4	stěna
<g/>
,	,	kIx,	,
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
je	být	k5eAaImIp3nS	být
až	až	k9	až
1,5	[number]	k4	1,5
cm	cm	kA	cm
tlustá	tlustý	k2eAgFnSc1d1	tlustá
<g/>
.	.	kIx.	.
</s>
<s>
Zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
až	až	k9	až
do	do	k7c2	do
srdečního	srdeční	k2eAgInSc2d1	srdeční
hrotu	hrot	k1gInSc2	hrot
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k6eAd1	uvnitř
jsou	být	k5eAaImIp3nP	být
bradavkovité	bradavkovitý	k2eAgInPc4d1	bradavkovitý
svaly	sval	k1gInPc4	sval
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yRgMnPc4	který
se	se	k3xPyFc4	se
upínají	upínat	k5eAaImIp3nP	upínat
šlašinky	šlašinka	k1gFnPc1	šlašinka
<g/>
,	,	kIx,	,
a	a	k8xC	a
srdeční	srdeční	k2eAgFnPc4d1	srdeční
struny	struna	k1gFnPc4	struna
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
převodního	převodní	k2eAgInSc2d1	převodní
systému	systém	k1gInSc2	systém
srdečního	srdeční	k2eAgInSc2d1	srdeční
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stahem	stah	k1gInSc7	stah
levé	levý	k2eAgFnSc2d1	levá
komory	komora	k1gFnSc2	komora
je	být	k5eAaImIp3nS	být
krev	krev	k1gFnSc1	krev
vypuzována	vypuzován	k2eAgFnSc1d1	vypuzována
do	do	k7c2	do
aorty	aorta	k1gFnSc2	aorta
<g/>
,	,	kIx,	,
otvor	otvor	k1gInSc1	otvor
uzavírá	uzavírat	k5eAaImIp3nS	uzavírat
aortální	aortální	k2eAgFnSc4d1	aortální
chlopeň	chlopeň	k1gFnSc4	chlopeň
(	(	kIx(	(
<g/>
valva	valva	k6eAd1	valva
aortae	aortae	k6eAd1	aortae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Aortální	aortální	k2eAgFnSc1d1	aortální
chlopeň	chlopeň	k1gFnSc1	chlopeň
pracuje	pracovat	k5eAaImIp3nS	pracovat
stejně	stejně	k6eAd1	stejně
a	a	k8xC	a
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
rytmu	rytmus	k1gInSc6	rytmus
jako	jako	k9	jako
chlopeň	chlopeň	k1gFnSc1	chlopeň
plicní	plicní	k2eAgFnSc1d1	plicní
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
kontrakci	kontrakce	k1gFnSc6	kontrakce
komor	komora	k1gFnPc2	komora
se	se	k3xPyFc4	se
dostává	dostávat	k5eAaImIp3nS	dostávat
okysličená	okysličený	k2eAgFnSc1d1	okysličená
krev	krev	k1gFnSc1	krev
přes	přes	k7c4	přes
aortální	aortální	k2eAgFnSc4d1	aortální
chlopeň	chlopeň	k1gFnSc4	chlopeň
do	do	k7c2	do
hlavního	hlavní	k2eAgInSc2d1	hlavní
tělního	tělní	k2eAgInSc2d1	tělní
oběhu	oběh	k1gInSc2	oběh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Typický	typický	k2eAgInSc1d1	typický
zvuk	zvuk	k1gInSc1	zvuk
srdeční	srdeční	k2eAgFnSc2d1	srdeční
činnosti	činnost	k1gFnSc2	činnost
"	"	kIx"	"
<g/>
lub-dub	lubub	k1gInSc1	lub-dub
<g/>
"	"	kIx"	"
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
střídání	střídání	k1gNnSc1	střídání
fází	fáze	k1gFnPc2	fáze
systoly	systola	k1gFnSc2	systola
a	a	k8xC	a
diastoly	diastola	k1gFnSc2	diastola
<g/>
.	.	kIx.	.
</s>
<s>
Systola	systola	k1gFnSc1	systola
je	být	k5eAaImIp3nS	být
fáze	fáze	k1gFnPc4	fáze
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
krev	krev	k1gFnSc1	krev
tryská	tryskat	k5eAaImIp3nS	tryskat
ze	z	k7c2	z
srdečních	srdeční	k2eAgFnPc2d1	srdeční
komor	komora	k1gFnPc2	komora
do	do	k7c2	do
tepen	tepna	k1gFnPc2	tepna
<g/>
.	.	kIx.	.
</s>
<s>
Relaxační	relaxační	k2eAgFnSc1d1	relaxační
fáze	fáze	k1gFnSc1	fáze
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
srdce	srdce	k1gNnSc1	srdce
odpočívá	odpočívat	k5eAaImIp3nS	odpočívat
a	a	k8xC	a
plní	plnit	k5eAaImIp3nS	plnit
se	se	k3xPyFc4	se
krví	krev	k1gFnSc7	krev
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
diastola	diastola	k1gFnSc1	diastola
<g/>
.	.	kIx.	.
</s>
<s>
Síně	síň	k1gFnPc1	síň
začínají	začínat	k5eAaImIp3nP	začínat
svůj	svůj	k3xOyFgInSc4	svůj
stah	stah	k1gInSc4	stah
odshora	odshora	k6eAd1	odshora
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
připomíná	připomínat	k5eAaImIp3nS	připomínat
ždímání	ždímání	k1gNnSc4	ždímání
<g/>
.	.	kIx.	.
</s>
<s>
Síně	síň	k1gFnPc1	síň
musí	muset	k5eAaImIp3nP	muset
krev	krev	k1gFnSc4	krev
dopravit	dopravit	k5eAaPmF	dopravit
do	do	k7c2	do
komor	komora	k1gFnPc2	komora
pod	pod	k7c7	pod
nimi	on	k3xPp3gFnPc7	on
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Stavba	stavba	k1gFnSc1	stavba
srdeční	srdeční	k2eAgFnSc2d1	srdeční
stěny	stěna	k1gFnSc2	stěna
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
je	být	k5eAaImIp3nS	být
srdce	srdce	k1gNnSc1	srdce
kryto	krýt	k5eAaImNgNnS	krýt
perikardem	perikard	k1gInSc7	perikard
<g/>
,	,	kIx,	,
vazivovou	vazivový	k2eAgFnSc7d1	vazivová
blankou	blanka	k1gFnSc7	blanka
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yIgFnPc4	který
probíhají	probíhat	k5eAaImIp3nP	probíhat
tepny	tepna	k1gFnPc4	tepna
a	a	k8xC	a
žíly	žíla	k1gFnPc4	žíla
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
také	také	k9	také
tukovou	tukový	k2eAgFnSc4d1	tuková
tkáň	tkáň	k1gFnSc4	tkáň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejsilnější	silný	k2eAgFnSc1d3	nejsilnější
část	část	k1gFnSc1	část
stěny	stěna	k1gFnSc2	stěna
je	být	k5eAaImIp3nS	být
myokard	myokard	k1gInSc1	myokard
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
srdeční	srdeční	k2eAgFnSc7d1	srdeční
svalovinou	svalovina	k1gFnSc7	svalovina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
síních	síň	k1gFnPc6	síň
je	být	k5eAaImIp3nS	být
dvouvrstevný	dvouvrstevný	k2eAgInSc1d1	dvouvrstevný
<g/>
,	,	kIx,	,
ve	v	k7c6	v
stěnách	stěna	k1gFnPc6	stěna
komor	komora	k1gFnPc2	komora
je	být	k5eAaImIp3nS	být
trojvrstevný	trojvrstevný	k2eAgInSc1d1	trojvrstevný
<g/>
.	.	kIx.	.
</s>
<s>
Vlákna	vlákna	k1gFnSc1	vlákna
srdečního	srdeční	k2eAgInSc2d1	srdeční
svalu	sval	k1gInSc2	sval
jsou	být	k5eAaImIp3nP	být
složitě	složitě	k6eAd1	složitě
propletena	propleten	k2eAgNnPc1d1	propleteno
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nP	tvořit
architektoniku	architektonika	k1gFnSc4	architektonika
srdečního	srdeční	k2eAgInSc2d1	srdeční
svalu	sval	k1gInSc2	sval
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
stěna	stěna	k1gFnSc1	stěna
<g/>
,	,	kIx,	,
obrácená	obrácený	k2eAgFnSc1d1	obrácená
do	do	k7c2	do
srdečních	srdeční	k2eAgFnPc2d1	srdeční
dutin	dutina	k1gFnPc2	dutina
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
endokard	endokard	k1gInSc1	endokard
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
vazivová	vazivový	k2eAgFnSc1d1	vazivová
blanka	blanka	k1gFnSc1	blanka
<g/>
,	,	kIx,	,
směrem	směr	k1gInSc7	směr
do	do	k7c2	do
dutin	dutina	k1gFnPc2	dutina
pokryta	pokrýt	k5eAaPmNgFnS	pokrýt
endotelem	endotel	k1gInSc7	endotel
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
výstelkou	výstelka	k1gFnSc7	výstelka
cév	céva	k1gFnPc2	céva
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
endokardem	endokard	k1gInSc7	endokard
a	a	k8xC	a
myokardem	myokard	k1gInSc7	myokard
probíhají	probíhat	k5eAaImIp3nP	probíhat
Purkyňova	Purkyňův	k2eAgNnPc1d1	Purkyňovo
vlákna	vlákno	k1gNnPc1	vlákno
<g/>
,	,	kIx,	,
součást	součást	k1gFnSc1	součást
převodního	převodní	k2eAgInSc2d1	převodní
systému	systém	k1gInSc2	systém
srdečního	srdeční	k2eAgInSc2d1	srdeční
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Srdeční	srdeční	k2eAgInSc1d1	srdeční
skelet	skelet	k1gInSc1	skelet
===	===	k?	===
</s>
</p>
<p>
<s>
Mohlo	moct	k5eAaImAgNnS	moct
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
zdát	zdát	k5eAaImF	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
všechny	všechen	k3xTgFnPc1	všechen
chlopně	chlopeň	k1gFnPc1	chlopeň
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
rovině	rovina	k1gFnSc6	rovina
v	v	k7c6	v
úrovni	úroveň	k1gFnSc6	úroveň
srdeční	srdeční	k2eAgFnSc2d1	srdeční
báze	báze	k1gFnSc2	báze
<g/>
.	.	kIx.	.
</s>
<s>
Preparace	preparace	k1gFnSc1	preparace
srdeční	srdeční	k2eAgFnSc2d1	srdeční
stěny	stěna	k1gFnSc2	stěna
však	však	k9	však
ukáže	ukázat	k5eAaPmIp3nS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
čtyři	čtyři	k4xCgInPc1	čtyři
vazivové	vazivový	k2eAgInPc1d1	vazivový
prstence	prstenec	k1gInPc1	prstenec
chlopní	chlopeň	k1gFnPc2	chlopeň
(	(	kIx(	(
<g/>
anuli	anulit	k5eAaPmRp2nS	anulit
fibrosi	fibros	k1gMnSc5	fibros
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
základní	základní	k2eAgFnSc4d1	základní
část	část	k1gFnSc4	část
srdečního	srdeční	k2eAgInSc2d1	srdeční
skeletu	skelet	k1gInSc2	skelet
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nS	tvořit
vzájemně	vzájemně	k6eAd1	vzájemně
nakloněné	nakloněný	k2eAgFnPc4d1	nakloněná
roviny	rovina	k1gFnPc4	rovina
<g/>
.	.	kIx.	.
</s>
<s>
Jednu	jeden	k4xCgFnSc4	jeden
rovinu	rovina	k1gFnSc4	rovina
tvoří	tvořit	k5eAaImIp3nP	tvořit
prstence	prstenec	k1gInPc1	prstenec
trojcípé	trojcípý	k2eAgFnSc2d1	trojcípá
a	a	k8xC	a
dvojcípé	dvojcípý	k2eAgFnSc2d1	dvojcípá
chlopně	chlopeň	k1gFnSc2	chlopeň
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc4d1	další
rovinu	rovina	k1gFnSc4	rovina
odkloněnou	odkloněný	k2eAgFnSc4d1	odkloněná
dopředu	dopředu	k6eAd1	dopředu
doprava	doprava	k6eAd1	doprava
tvoří	tvořit	k5eAaImIp3nP	tvořit
prstenec	prstenec	k1gInSc4	prstenec
aorty	aorta	k1gFnSc2	aorta
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
nakonec	nakonec	k6eAd1	nakonec
třetí	třetí	k4xOgFnSc1	třetí
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
nejvíce	hodně	k6eAd3	hodně
vzadu	vzadu	k6eAd1	vzadu
vlevo	vlevo	k6eAd1	vlevo
odkloněná	odkloněný	k2eAgFnSc1d1	odkloněná
rovina	rovina	k1gFnSc1	rovina
prstence	prstenec	k1gInSc2	prstenec
chlopní	chlopeň	k1gFnPc2	chlopeň
plicních	plicní	k2eAgFnPc2d1	plicní
tepen	tepna	k1gFnPc2	tepna
malého	malý	k2eAgInSc2d1	malý
srdečního	srdeční	k2eAgInSc2d1	srdeční
oběhu	oběh	k1gInSc2	oběh
<g/>
.	.	kIx.	.
</s>
<s>
Okraje	okraj	k1gInPc1	okraj
otvorů	otvor	k1gInPc2	otvor
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
prstence	prstenec	k1gInPc1	prstenec
uzavírají	uzavírat	k5eAaImIp3nP	uzavírat
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
vyztužené	vyztužený	k2eAgInPc1d1	vyztužený
vazivovými	vazivový	k2eAgInPc7d1	vazivový
pruhy	pruh	k1gInPc7	pruh
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
i	i	k9	i
srdeční	srdeční	k2eAgFnSc1d1	srdeční
stěna	stěna	k1gFnSc1	stěna
je	být	k5eAaImIp3nS	být
zesílena	zesílit	k5eAaPmNgFnS	zesílit
vazivem	vazivo	k1gNnSc7	vazivo
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
skotu	skot	k1gInSc2	skot
a	a	k8xC	a
některých	některý	k3yIgMnPc2	některý
jiných	jiný	k2eAgMnPc2d1	jiný
savců	savec	k1gMnPc2	savec
dokonce	dokonce	k9	dokonce
vazivo	vazivo	k1gNnSc4	vazivo
osifikuje	osifikovat	k5eAaBmIp3nS	osifikovat
a	a	k8xC	a
vznikají	vznikat	k5eAaImIp3nP	vznikat
skutečné	skutečný	k2eAgFnPc1d1	skutečná
kosti	kost	k1gFnPc1	kost
trojúhelníkového	trojúhelníkový	k2eAgInSc2d1	trojúhelníkový
tvaru	tvar	k1gInSc2	tvar
(	(	kIx(	(
<g/>
ossa	oss	k2eAgFnSc1d1	ossa
cordis	cordis	k1gFnSc1	cordis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
mohou	moct	k5eAaImIp3nP	moct
srdeční	srdeční	k2eAgNnPc4d1	srdeční
vaziva	vazivo	k1gNnPc4	vazivo
vzácně	vzácně	k6eAd1	vzácně
kalcifikovat	kalcifikovat	k5eAaBmF	kalcifikovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Nutritivní	nutritivní	k2eAgInSc4d1	nutritivní
oběh	oběh	k1gInSc4	oběh
srdce	srdce	k1gNnSc2	srdce
===	===	k?	===
</s>
</p>
<p>
<s>
I	i	k9	i
srdeční	srdeční	k2eAgInSc4d1	srdeční
sval	sval	k1gInSc4	sval
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
kyslík	kyslík	k1gInSc1	kyslík
a	a	k8xC	a
živiny	živina	k1gFnPc1	živina
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgInS	moct
pracovat	pracovat	k5eAaImF	pracovat
<g/>
.	.	kIx.	.
</s>
<s>
Srdce	srdce	k1gNnSc1	srdce
ale	ale	k9	ale
nemůže	moct	k5eNaImIp3nS	moct
přijímat	přijímat	k5eAaImF	přijímat
živiny	živina	k1gFnPc4	živina
z	z	k7c2	z
velkého	velký	k2eAgNnSc2d1	velké
množství	množství	k1gNnSc2	množství
krve	krev	k1gFnSc2	krev
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
protéká	protékat	k5eAaImIp3nS	protékat
srdcem	srdce	k1gNnSc7	srdce
<g/>
.	.	kIx.	.
</s>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
toku	tok	k1gInSc2	tok
i	i	k9	i
vnitřní	vnitřní	k2eAgInSc1d1	vnitřní
tlak	tlak	k1gInSc1	tlak
je	být	k5eAaImIp3nS	být
příliš	příliš	k6eAd1	příliš
vysoký	vysoký	k2eAgInSc1d1	vysoký
a	a	k8xC	a
mohl	moct	k5eAaImAgInS	moct
by	by	kYmCp3nS	by
potrhat	potrhat	k5eAaPmF	potrhat
jemnou	jemný	k2eAgFnSc4d1	jemná
síť	síť	k1gFnSc4	síť
kapilár	kapilára	k1gFnPc2	kapilára
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
srdce	srdce	k1gNnSc1	srdce
zásobováno	zásobovat	k5eAaImNgNnS	zásobovat
zvenku	zvenku	k6eAd1	zvenku
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
aorty	aorta	k1gFnSc2	aorta
<g/>
,	,	kIx,	,
těsně	těsně	k6eAd1	těsně
nad	nad	k7c7	nad
aortální	aortální	k2eAgFnSc7d1	aortální
chlopní	chlopeň	k1gFnSc7	chlopeň
–	–	k?	–
otvorem	otvor	k1gInSc7	otvor
ne	ne	k9	ne
větším	většit	k5eAaImIp1nS	většit
než	než	k8xS	než
brčko	brčko	k1gNnSc1	brčko
na	na	k7c6	na
pití	pití	k1gNnSc6	pití
–	–	k?	–
odstupují	odstupovat	k5eAaImIp3nP	odstupovat
dvě	dva	k4xCgFnPc4	dva
věnčité	věnčitý	k2eAgFnPc4d1	věnčitá
(	(	kIx(	(
<g/>
koronární	koronární	k2eAgFnPc4d1	koronární
<g/>
)	)	kIx)	)
tepny	tepna	k1gFnPc4	tepna
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
vlastní	vlastní	k2eAgNnSc4d1	vlastní
zásobování	zásobování	k1gNnSc4	zásobování
srdce	srdce	k1gNnSc2	srdce
<g/>
.	.	kIx.	.
</s>
<s>
Vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
jemnou	jemný	k2eAgFnSc4d1	jemná
krajkovou	krajkový	k2eAgFnSc4d1	krajková
síť	síť	k1gFnSc4	síť
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
obkružuje	obkružovat	k5eAaImIp3nS	obkružovat
celé	celý	k2eAgNnSc4d1	celé
srdce	srdce	k1gNnSc4	srdce
a	a	k8xC	a
svým	svůj	k3xOyFgInSc7	svůj
tvarem	tvar	k1gInSc7	tvar
připomíná	připomínat	k5eAaImIp3nS	připomínat
věnec	věnec	k1gInSc4	věnec
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
pochází	pocházet	k5eAaImIp3nS	pocházet
název	název	k1gInSc1	název
věnčité	věnčitý	k2eAgFnSc2d1	věnčitá
tepny	tepna	k1gFnSc2	tepna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Věnčité	věnčitý	k2eAgFnPc1d1	věnčitá
tepny	tepna	k1gFnPc1	tepna
jsou	být	k5eAaImIp3nP	být
dvě	dva	k4xCgFnPc1	dva
–	–	k?	–
pravá	pravý	k2eAgFnSc1d1	pravá
a	a	k8xC	a
levá	levý	k2eAgFnSc1d1	levá
–	–	k?	–
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
jediné	jediný	k2eAgFnPc1d1	jediná
tepny	tepna	k1gFnPc1	tepna
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
ze	z	k7c2	z
vzestupné	vzestupný	k2eAgFnSc2d1	vzestupná
aorty	aorta	k1gFnSc2	aorta
hned	hned	k6eAd1	hned
za	za	k7c7	za
srdcem	srdce	k1gNnSc7	srdce
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
při	při	k7c6	při
systole	systola	k1gFnSc6	systola
je	být	k5eAaImIp3nS	být
tlak	tlak	k1gInSc4	tlak
v	v	k7c6	v
aortě	aorta	k1gFnSc6	aorta
příliš	příliš	k6eAd1	příliš
velký	velký	k2eAgInSc1d1	velký
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
také	také	k9	také
jediné	jediný	k2eAgFnPc1d1	jediná
tepny	tepna	k1gFnPc1	tepna
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
plní	plnit	k5eAaImIp3nP	plnit
při	při	k7c6	při
diastole	diastola	k1gFnSc6	diastola
<g/>
.	.	kIx.	.
</s>
<s>
Levá	levý	k2eAgFnSc1d1	levá
věnčitá	věnčitý	k2eAgFnSc1d1	věnčitá
tepna	tepna	k1gFnSc1	tepna
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
levou	levý	k2eAgFnSc4d1	levá
sestupnou	sestupný	k2eAgFnSc4d1	sestupná
větev	větev	k1gFnSc4	větev
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vede	vést	k5eAaImIp3nS	vést
krev	krev	k1gFnSc4	krev
k	k	k7c3	k
oběma	dva	k4xCgFnPc3	dva
komorám	komora	k1gFnPc3	komora
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c4	na
ramus	ramus	k1gInSc4	ramus
circumflexus	circumflexus	k1gMnSc1	circumflexus
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
otáčí	otáčet	k5eAaImIp3nS	otáčet
dozadu	dozadu	k6eAd1	dozadu
a	a	k8xC	a
zásobuje	zásobovat	k5eAaImIp3nS	zásobovat
krví	krvit	k5eAaImIp3nS	krvit
levou	levý	k2eAgFnSc4d1	levá
komoru	komora	k1gFnSc4	komora
a	a	k8xC	a
síň	síň	k1gFnSc4	síň
<g/>
.	.	kIx.	.
</s>
<s>
Zrcadlově	zrcadlově	k6eAd1	zrcadlově
je	být	k5eAaImIp3nS	být
obrazem	obraz	k1gInSc7	obraz
koronárních	koronární	k2eAgFnPc2d1	koronární
tepen	tepna	k1gFnPc2	tepna
systém	systém	k1gInSc4	systém
srdečních	srdeční	k2eAgFnPc2d1	srdeční
žil	žíla	k1gFnPc2	žíla
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
mají	mít	k5eAaImIp3nP	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
odvádět	odvádět	k5eAaImF	odvádět
krev	krev	k1gFnSc4	krev
ze	z	k7c2	z
srdečního	srdeční	k2eAgInSc2d1	srdeční
svalu	sval	k1gInSc2	sval
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
žíly	žíla	k1gFnPc1	žíla
jdou	jít	k5eAaImIp3nP	jít
paralelně	paralelně	k6eAd1	paralelně
a	a	k8xC	a
vyprazdňují	vyprazdňovat	k5eAaImIp3nP	vyprazdňovat
se	se	k3xPyFc4	se
do	do	k7c2	do
pravé	pravý	k2eAgFnSc2d1	pravá
síně	síň	k1gFnSc2	síň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
terminální	terminální	k2eAgFnPc4d1	terminální
arterie	arterie	k1gFnPc4	arterie
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
netvoří	tvořit	k5eNaImIp3nP	tvořit
žádné	žádný	k3yNgFnPc4	žádný
spojky	spojka	k1gFnPc4	spojka
s	s	k7c7	s
ostatními	ostatní	k2eAgFnPc7d1	ostatní
tepnami	tepna	k1gFnPc7	tepna
a	a	k8xC	a
pokud	pokud	k8xS	pokud
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
ucpání	ucpání	k1gNnSc3	ucpání
tepny	tepna	k1gFnSc2	tepna
<g/>
,	,	kIx,	,
okrsek	okrsek	k1gInSc4	okrsek
tkáně	tkáň	k1gFnSc2	tkáň
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
tato	tento	k3xDgFnSc1	tento
tepna	tepna	k1gFnSc1	tepna
zásobuje	zásobovat	k5eAaImIp3nS	zásobovat
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
nedostává	dostávat	k5eNaImIp3nS	dostávat
živiny	živina	k1gFnPc4	živina
ani	ani	k8xC	ani
kyslík	kyslík	k1gInSc4	kyslík
a	a	k8xC	a
odumře	odumřít	k5eAaPmIp3nS	odumřít
(	(	kIx(	(
<g/>
ischemie	ischemie	k1gFnSc1	ischemie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ischemická	ischemický	k2eAgFnSc1d1	ischemická
choroba	choroba	k1gFnSc1	choroba
srdeční	srdeční	k2eAgFnSc1d1	srdeční
je	být	k5eAaImIp3nS	být
stav	stav	k1gInSc4	stav
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zúžená	zúžený	k2eAgFnSc1d1	zúžená
věnčitá	věnčitý	k2eAgFnSc1d1	věnčitá
tepna	tepna	k1gFnSc1	tepna
nezvládá	zvládat	k5eNaImIp3nS	zvládat
dostatečně	dostatečně	k6eAd1	dostatečně
zásobovat	zásobovat	k5eAaImF	zásobovat
srdeční	srdeční	k2eAgInSc4d1	srdeční
sval	sval	k1gInSc4	sval
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
úplném	úplný	k2eAgInSc6d1	úplný
uzávěru	uzávěr	k1gInSc6	uzávěr
tepny	tepna	k1gFnSc2	tepna
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
infarktu	infarkt	k1gInSc3	infarkt
myokardu	myokard	k1gInSc2	myokard
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
velmi	velmi	k6eAd1	velmi
vážný	vážný	k2eAgInSc1d1	vážný
stav	stav	k1gInSc1	stav
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
může	moct	k5eAaImIp3nS	moct
vyústit	vyústit	k5eAaPmF	vyústit
až	až	k9	až
do	do	k7c2	do
srdeční	srdeční	k2eAgFnSc2d1	srdeční
zástavy	zástava	k1gFnSc2	zástava
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
odumření	odumření	k1gNnSc3	odumření
srdeční	srdeční	k2eAgFnSc2d1	srdeční
svaloviny	svalovina	k1gFnSc2	svalovina
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nevratný	vratný	k2eNgInSc1d1	nevratný
stav	stav	k1gInSc1	stav
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
srdce	srdce	k1gNnSc1	srdce
se	se	k3xPyFc4	se
dokáže	dokázat	k5eAaPmIp3nS	dokázat
hojit	hojit	k5eAaImF	hojit
jen	jen	k9	jen
vazivovou	vazivový	k2eAgFnSc7d1	vazivová
jizvou	jizva	k1gFnSc7	jizva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Velikost	velikost	k1gFnSc1	velikost
srdce	srdce	k1gNnSc2	srdce
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
uložení	uložení	k1gNnSc4	uložení
==	==	k?	==
</s>
</p>
<p>
<s>
U	u	k7c2	u
dospělého	dospělý	k2eAgMnSc2d1	dospělý
člověka	člověk	k1gMnSc2	člověk
je	být	k5eAaImIp3nS	být
srdce	srdce	k1gNnSc4	srdce
asi	asi	k9	asi
12	[number]	k4	12
cm	cm	kA	cm
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
a	a	k8xC	a
8	[number]	k4	8
<g/>
–	–	k?	–
<g/>
9	[number]	k4	9
cm	cm	kA	cm
široké	široký	k2eAgNnSc1d1	široké
<g/>
.	.	kIx.	.
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
srdce	srdce	k1gNnSc2	srdce
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
podle	podle	k7c2	podle
pohlaví	pohlaví	k1gNnSc2	pohlaví
<g/>
,	,	kIx,	,
u	u	k7c2	u
muže	muž	k1gMnSc2	muž
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
kolem	kolem	k7c2	kolem
280-340	[number]	k4	280-340
g	g	kA	g
<g/>
,	,	kIx,	,
u	u	k7c2	u
žen	žena	k1gFnPc2	žena
kolem	kolem	k7c2	kolem
230-280	[number]	k4	230-280
gramů	gram	k1gInPc2	gram
<g/>
.	.	kIx.	.
</s>
<s>
Průměrně	průměrně	k6eAd1	průměrně
srdce	srdce	k1gNnSc1	srdce
udělá	udělat	k5eAaPmIp3nS	udělat
okolo	okolo	k7c2	okolo
100	[number]	k4	100
000	[number]	k4	000
úderů	úder	k1gInPc2	úder
každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Srdeční	srdeční	k2eAgFnSc1d1	srdeční
základna	základna	k1gFnSc1	základna
leží	ležet	k5eAaImIp3nS	ležet
pod	pod	k7c7	pod
pátým	pátý	k4xOgInSc7	pátý
až	až	k8xS	až
sedmým	sedmý	k4xOgInSc7	sedmý
hrudním	hrudní	k2eAgInSc7d1	hrudní
obratlem	obratel	k1gInSc7	obratel
<g/>
,	,	kIx,	,
hrot	hrot	k1gInSc1	hrot
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
do	do	k7c2	do
pátého	pátý	k4xOgNnSc2	pátý
mezižebří	mezižebří	k1gNnSc2	mezižebří
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
U	u	k7c2	u
domácích	domácí	k2eAgMnPc2d1	domácí
savců	savec	k1gMnPc2	savec
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
U	u	k7c2	u
dinosaurů	dinosaurus	k1gMnPc2	dinosaurus
===	===	k?	===
</s>
</p>
<p>
<s>
Velcí	velký	k2eAgMnPc1d1	velký
sauropodní	sauropodní	k2eAgMnPc1d1	sauropodní
dinosauři	dinosaurus	k1gMnPc1	dinosaurus
jako	jako	k8xS	jako
byl	být	k5eAaImAgMnS	být
Giraffatitan	Giraffatitan	k1gInSc4	Giraffatitan
měli	mít	k5eAaImAgMnP	mít
obří	obří	k2eAgNnSc4d1	obří
srdce	srdce	k1gNnSc4	srdce
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
hmotnost	hmotnost	k1gFnSc1	hmotnost
zřejmě	zřejmě	k6eAd1	zřejmě
dosahovala	dosahovat	k5eAaImAgFnS	dosahovat
až	až	k6eAd1	až
kolem	kolem	k7c2	kolem
200	[number]	k4	200
kg	kg	kA	kg
a	a	k8xC	a
průměr	průměr	k1gInSc4	průměr
kolem	kolem	k7c2	kolem
1,5	[number]	k4	1,5
metru	metr	k1gInSc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Objem	objem	k1gInSc1	objem
krve	krev	k1gFnSc2	krev
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
u	u	k7c2	u
těchto	tento	k3xDgMnPc2	tento
dinosaurů	dinosaurus	k1gMnPc2	dinosaurus
mohl	moct	k5eAaImAgInS	moct
dosahovat	dosahovat	k5eAaImF	dosahovat
až	až	k9	až
kolem	kolem	k7c2	kolem
3000	[number]	k4	3000
litrů	litr	k1gInPc2	litr
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
známého	známý	k2eAgMnSc2d1	známý
teropoda	teropod	k1gMnSc2	teropod
rodu	rod	k1gInSc2	rod
Tyrannosaurus	Tyrannosaurus	k1gMnSc1	Tyrannosaurus
zase	zase	k9	zase
srdce	srdce	k1gNnSc1	srdce
dosahovalo	dosahovat	k5eAaImAgNnS	dosahovat
velikosti	velikost	k1gFnPc4	velikost
mikrovlnné	mikrovlnný	k2eAgFnSc2d1	mikrovlnná
trouby	trouba	k1gFnSc2	trouba
(	(	kIx(	(
<g/>
asi	asi	k9	asi
75	[number]	k4	75
x	x	k?	x
50	[number]	k4	50
cm	cm	kA	cm
<g/>
)	)	kIx)	)
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
tak	tak	k6eAd1	tak
zhruba	zhruba	k6eAd1	zhruba
100	[number]	k4	100
-	-	kIx~	-
150	[number]	k4	150
<g/>
krát	krát	k6eAd1	krát
větší	veliký	k2eAgNnSc4d2	veliký
než	než	k8xS	než
lidské	lidský	k2eAgNnSc4d1	lidské
srdce	srdce	k1gNnSc4	srdce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Práce	práce	k1gFnSc1	práce
srdce	srdce	k1gNnSc2	srdce
==	==	k?	==
</s>
</p>
<p>
<s>
Srdeční	srdeční	k2eAgInSc1d1	srdeční
cyklus	cyklus	k1gInSc1	cyklus
se	se	k3xPyFc4	se
rozpadá	rozpadat	k5eAaPmIp3nS	rozpadat
do	do	k7c2	do
dvou	dva	k4xCgFnPc2	dva
hlavních	hlavní	k2eAgFnPc2d1	hlavní
fází	fáze	k1gFnPc2	fáze
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Systola	systola	k1gFnSc1	systola
je	být	k5eAaImIp3nS	být
koordinovaný	koordinovaný	k2eAgInSc4d1	koordinovaný
stah	stah	k1gInSc4	stah
srdeční	srdeční	k2eAgFnSc2d1	srdeční
svaloviny	svalovina	k1gFnSc2	svalovina
síní	síň	k1gFnPc2	síň
nebo	nebo	k8xC	nebo
komor	komora	k1gFnPc2	komora
</s>
</p>
<p>
<s>
Diastola	diastola	k1gFnSc1	diastola
je	být	k5eAaImIp3nS	být
uvolnění	uvolnění	k1gNnSc4	uvolnění
(	(	kIx(	(
<g/>
relaxace	relaxace	k1gFnSc2	relaxace
<g/>
)	)	kIx)	)
srdečního	srdeční	k2eAgInSc2d1	srdeční
svaluPři	svaluPř	k1gFnSc3	svaluPř
diastole	diastola	k1gFnSc3	diastola
síní	síň	k1gFnPc2	síň
(	(	kIx(	(
<g/>
za	za	k7c2	za
současné	současný	k2eAgFnSc2d1	současná
systoly	systola	k1gFnSc2	systola
komor	komora	k1gFnPc2	komora
<g/>
)	)	kIx)	)
přitéká	přitékat	k5eAaImIp3nS	přitékat
do	do	k7c2	do
pravé	pravý	k2eAgFnSc2d1	pravá
síně	síň	k1gFnSc2	síň
oběma	dva	k4xCgFnPc7	dva
dutými	dutý	k2eAgFnPc7d1	dutá
žilami	žíla	k1gFnPc7	žíla
krev	krev	k1gFnSc4	krev
z	z	k7c2	z
velkého	velký	k2eAgInSc2d1	velký
tělního	tělní	k2eAgInSc2d1	tělní
oběhu	oběh	k1gInSc2	oběh
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
do	do	k7c2	do
levé	levý	k2eAgFnSc2d1	levá
síně	síň	k1gFnSc2	síň
přitéká	přitékat	k5eAaImIp3nS	přitékat
krev	krev	k1gFnSc4	krev
z	z	k7c2	z
plicních	plicní	k2eAgFnPc2d1	plicní
žil	žíla	k1gFnPc2	žíla
<g/>
.	.	kIx.	.
</s>
<s>
Následuje	následovat	k5eAaImIp3nS	následovat
systola	systola	k1gFnSc1	systola
obou	dva	k4xCgFnPc2	dva
síní	síň	k1gFnPc2	síň
(	(	kIx(	(
<g/>
současně	současně	k6eAd1	současně
s	s	k7c7	s
diastolou	diastola	k1gFnSc7	diastola
obou	dva	k4xCgFnPc2	dva
komor	komora	k1gFnPc2	komora
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yIgFnSc6	který
je	být	k5eAaImIp3nS	být
krev	krev	k1gFnSc4	krev
ze	z	k7c2	z
síní	síň	k1gFnPc2	síň
vypuzena	vypuzen	k2eAgFnSc1d1	vypuzena
do	do	k7c2	do
komor	komora	k1gFnPc2	komora
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
nedocházelo	docházet	k5eNaImAgNnS	docházet
ke	k	k7c3	k
zpětnému	zpětný	k2eAgInSc3d1	zpětný
toku	tok	k1gInSc3	tok
krve	krev	k1gFnSc2	krev
z	z	k7c2	z
komor	komora	k1gFnPc2	komora
do	do	k7c2	do
síní	síň	k1gFnPc2	síň
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
mezi	mezi	k7c7	mezi
pravou	pravý	k2eAgFnSc7d1	pravá
síní	síň	k1gFnSc7	síň
a	a	k8xC	a
komorou	komora	k1gFnSc7	komora
trojcípá	trojcípý	k2eAgFnSc1d1	trojcípá
chlopeň	chlopeň	k1gFnSc1	chlopeň
a	a	k8xC	a
mezi	mezi	k7c7	mezi
levou	levý	k2eAgFnSc7d1	levá
síní	síň	k1gFnSc7	síň
a	a	k8xC	a
komorou	komora	k1gFnSc7	komora
chlopeň	chlopeň	k1gFnSc1	chlopeň
dvojcípá	dvojcípý	k2eAgFnSc1d1	dvojcípá
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
výše	výše	k1gFnSc2	výše
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
chlopně	chlopeň	k1gFnPc1	chlopeň
se	se	k3xPyFc4	se
při	při	k7c6	při
následné	následný	k2eAgFnSc6d1	následná
systole	systola	k1gFnSc6	systola
komor	komora	k1gFnPc2	komora
uzavřou	uzavřít	k5eAaPmIp3nP	uzavřít
a	a	k8xC	a
krev	krev	k1gFnSc4	krev
z	z	k7c2	z
komor	komora	k1gFnPc2	komora
je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
vypuzena	vypuzen	k2eAgFnSc1d1	vypuzena
do	do	k7c2	do
plicního	plicní	k2eAgInSc2d1	plicní
kmene	kmen	k1gInSc2	kmen
a	a	k8xC	a
do	do	k7c2	do
aorty	aorta	k1gFnSc2	aorta
<g/>
.	.	kIx.	.
</s>
<s>
Zpětnému	zpětný	k2eAgInSc3d1	zpětný
toku	tok	k1gInSc3	tok
krve	krev	k1gFnSc2	krev
do	do	k7c2	do
komor	komora	k1gFnPc2	komora
brání	bránit	k5eAaImIp3nP	bránit
poloměsíčité	poloměsíčitý	k2eAgFnPc1d1	poloměsíčitá
chlopně	chlopeň	k1gFnPc1	chlopeň
uzavírající	uzavírající	k2eAgFnPc1d1	uzavírající
jak	jak	k8xC	jak
plicní	plicní	k2eAgInSc1d1	plicní
kmen	kmen	k1gInSc1	kmen
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
aortu	aorta	k1gFnSc4	aorta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
dvě	dva	k4xCgFnPc4	dva
fáze	fáze	k1gFnPc4	fáze
systoly	systola	k1gFnSc2	systola
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
izovolumická	izovolumický	k2eAgFnSc1d1	izovolumický
kontrakce	kontrakce	k1gFnSc1	kontrakce
-	-	kIx~	-
roste	růst	k5eAaImIp3nS	růst
tlak	tlak	k1gInSc1	tlak
<g/>
,	,	kIx,	,
objem	objem	k1gInSc1	objem
se	se	k3xPyFc4	se
nemění	měnit	k5eNaImIp3nS	měnit
</s>
</p>
<p>
<s>
ejekční	ejekční	k2eAgFnSc1d1	ejekční
fáze	fáze	k1gFnSc1	fáze
-	-	kIx~	-
objem	objem	k1gInSc1	objem
se	se	k3xPyFc4	se
zmenšuje	zmenšovat	k5eAaImIp3nS	zmenšovat
<g/>
,	,	kIx,	,
tlak	tlak	k1gInSc1	tlak
se	se	k3xPyFc4	se
neměníStejně	měníStejně	k6eNd1	měníStejně
tak	tak	k6eAd1	tak
rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
dvě	dva	k4xCgFnPc1	dva
fáze	fáze	k1gFnPc1	fáze
diastoly	diastola	k1gFnSc2	diastola
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
izovolumická	izovolumický	k2eAgFnSc1d1	izovolumický
relaxace	relaxace	k1gFnSc1	relaxace
-	-	kIx~	-
tlak	tlak	k1gInSc1	tlak
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
,	,	kIx,	,
objem	objem	k1gInSc1	objem
se	se	k3xPyFc4	se
nemění	měnit	k5eNaImIp3nS	měnit
</s>
</p>
<p>
<s>
plnicí	plnicí	k2eAgFnSc1d1	plnicí
fáze	fáze	k1gFnSc1	fáze
-	-	kIx~	-
objem	objem	k1gInSc1	objem
komor	komora	k1gFnPc2	komora
roste	růst	k5eAaImIp3nS	růst
<g/>
,	,	kIx,	,
tlak	tlak	k1gInSc1	tlak
se	se	k3xPyFc4	se
neměníKaždou	neměníKažda	k1gFnSc7	neměníKažda
systolou	systola	k1gFnSc7	systola
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
ze	z	k7c2	z
srdce	srdce	k1gNnSc2	srdce
vypuzeno	vypuzen	k2eAgNnSc1d1	vypuzeno
průměrně	průměrně	k6eAd1	průměrně
asi	asi	k9	asi
70	[number]	k4	70
ml	ml	kA	ml
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
krve	krev	k1gFnSc2	krev
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
jedna	jeden	k4xCgFnSc1	jeden
komora	komora	k1gFnSc1	komora
přečerpá	přečerpat	k5eAaPmIp3nS	přečerpat
za	za	k7c4	za
minutu	minuta	k1gFnSc4	minuta
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
minutový	minutový	k2eAgInSc1d1	minutový
srdeční	srdeční	k2eAgInSc1d1	srdeční
výdej	výdej	k1gInSc1	výdej
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Minutový	minutový	k2eAgInSc4d1	minutový
výdej	výdej	k1gInSc4	výdej
=	=	kIx~	=
tepový	tepový	k2eAgInSc4d1	tepový
objem	objem	k1gInSc4	objem
×	×	k?	×
srdeční	srdeční	k2eAgFnSc1d1	srdeční
frekvence	frekvence	k1gFnSc1	frekvence
</s>
</p>
<p>
<s>
U	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
v	v	k7c6	v
klidu	klid	k1gInSc6	klid
je	být	k5eAaImIp3nS	být
srdeční	srdeční	k2eAgFnSc1d1	srdeční
frekvence	frekvence	k1gFnSc1	frekvence
70-80	[number]	k4	70-80
stahů	stah	k1gInPc2	stah
za	za	k7c4	za
minutu	minuta	k1gFnSc4	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Klidový	klidový	k2eAgInSc1d1	klidový
minutový	minutový	k2eAgInSc1d1	minutový
srdeční	srdeční	k2eAgInSc1d1	srdeční
výdej	výdej	k1gInSc1	výdej
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
5-6	[number]	k4	5-6
l	l	kA	l
<g/>
/	/	kIx~	/
<g/>
min	mina	k1gFnPc2	mina
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
zhruba	zhruba	k6eAd1	zhruba
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
celkovému	celkový	k2eAgNnSc3d1	celkové
množství	množství	k1gNnSc3	množství
krve	krev	k1gFnSc2	krev
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
potřeby	potřeba	k1gFnSc2	potřeba
se	se	k3xPyFc4	se
ale	ale	k9	ale
dokáže	dokázat	k5eAaPmIp3nS	dokázat
zvýšit	zvýšit	k5eAaPmF	zvýšit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
pětkrát	pětkrát	k6eAd1	pětkrát
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
hlavě	hlava	k1gFnSc6	hlava
zrychlením	zrychlení	k1gNnSc7	zrychlení
srdeční	srdeční	k2eAgFnSc2d1	srdeční
frekvence	frekvence	k1gFnSc2	frekvence
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
člověka	člověk	k1gMnSc2	člověk
udělá	udělat	k5eAaPmIp3nS	udělat
srdce	srdce	k1gNnSc4	srdce
zhruba	zhruba	k6eAd1	zhruba
2,5	[number]	k4	2,5
miliardy	miliarda	k4xCgFnSc2	miliarda
stahů	stah	k1gInPc2	stah
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Proudění	proudění	k1gNnSc1	proudění
krve	krev	k1gFnSc2	krev
v	v	k7c6	v
krevním	krevní	k2eAgNnSc6d1	krevní
řečišti	řečiště	k1gNnSc6	řečiště
se	se	k3xPyFc4	se
řídí	řídit	k5eAaImIp3nS	řídit
tlakovým	tlakový	k2eAgInSc7d1	tlakový
spádem	spád	k1gInSc7	spád
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
hnací	hnací	k2eAgFnSc7d1	hnací
silou	síla	k1gFnSc7	síla
oběhu	oběh	k1gInSc2	oběh
krve	krev	k1gFnSc2	krev
jsou	být	k5eAaImIp3nP	být
tlakové	tlakový	k2eAgInPc1d1	tlakový
rozdíly	rozdíl	k1gInPc1	rozdíl
mezi	mezi	k7c7	mezi
tepennou	tepenný	k2eAgFnSc7d1	tepenná
a	a	k8xC	a
žilní	žilní	k2eAgFnSc7d1	žilní
částí	část	k1gFnSc7	část
oběhové	oběhový	k2eAgFnSc2d1	oběhová
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
krevního	krevní	k2eAgInSc2d1	krevní
tlaku	tlak	k1gInSc2	tlak
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
částech	část	k1gFnPc6	část
oběhové	oběhový	k2eAgFnSc2d1	oběhová
soustavy	soustava	k1gFnSc2	soustava
dána	dán	k2eAgFnSc1d1	dána
jednak	jednak	k8xC	jednak
činností	činnost	k1gFnSc7	činnost
srdce	srdce	k1gNnSc2	srdce
a	a	k8xC	a
jednak	jednak	k8xC	jednak
odporem	odpor	k1gInSc7	odpor
cesty	cesta	k1gFnSc2	cesta
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
klesá	klesat	k5eAaImIp3nS	klesat
s	s	k7c7	s
druhou	druhý	k4xOgFnSc7	druhý
mocninou	mocnina	k1gFnSc7	mocnina
poloměru	poloměr	k1gInSc2	poloměr
a	a	k8xC	a
roste	růst	k5eAaImIp3nS	růst
s	s	k7c7	s
délkou	délka	k1gFnSc7	délka
cévy	céva	k1gFnSc2	céva
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
směrem	směr	k1gInSc7	směr
od	od	k7c2	od
srdce	srdce	k1gNnSc2	srdce
krevní	krevní	k2eAgInSc4d1	krevní
tlak	tlak	k1gInSc4	tlak
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
,	,	kIx,	,
za	za	k7c2	za
současného	současný	k2eAgInSc2d1	současný
poklesu	pokles	k1gInSc2	pokles
rychlosti	rychlost	k1gFnSc2	rychlost
proudění	proudění	k1gNnSc2	proudění
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Zpomalení	zpomalení	k1gNnSc1	zpomalení
toku	tok	k1gInSc2	tok
krve	krev	k1gFnSc2	krev
ve	v	k7c6	v
vlásečnicích	vlásečnice	k1gFnPc6	vlásečnice
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
důležité	důležitý	k2eAgNnSc1d1	důležité
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
zde	zde	k6eAd1	zde
probíhá	probíhat	k5eAaImIp3nS	probíhat
vlastní	vlastní	k2eAgNnSc4d1	vlastní
předávání	předávání	k1gNnSc4	předávání
živin	živina	k1gFnPc2	živina
a	a	k8xC	a
kyslíku	kyslík	k1gInSc2	kyslík
tkáním	tkáň	k1gFnPc3	tkáň
a	a	k8xC	a
odvádění	odvádění	k1gNnSc6	odvádění
produktů	produkt	k1gInPc2	produkt
metabolismu	metabolismus	k1gInSc2	metabolismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Řízení	řízení	k1gNnSc1	řízení
srdeční	srdeční	k2eAgFnSc2d1	srdeční
činnosti	činnost	k1gFnSc2	činnost
==	==	k?	==
</s>
</p>
<p>
<s>
Srdce	srdce	k1gNnSc1	srdce
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
autonomní	autonomní	k2eAgInSc4d1	autonomní
orgán	orgán	k1gInSc4	orgán
<g/>
,	,	kIx,	,
podněty	podnět	k1gInPc4	podnět
ke	k	k7c3	k
kontrakci	kontrakce	k1gFnSc3	kontrakce
myokardu	myokard	k1gInSc2	myokard
vznikají	vznikat	k5eAaImIp3nP	vznikat
přímo	přímo	k6eAd1	přímo
ve	v	k7c6	v
vlastní	vlastní	k2eAgFnSc6d1	vlastní
svalovině	svalovina	k1gFnSc6	svalovina
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
v	v	k7c6	v
modifikovaných	modifikovaný	k2eAgInPc6d1	modifikovaný
kardiomyocytech	kardiomyocyt	k1gInPc6	kardiomyocyt
tvořících	tvořící	k2eAgFnPc2d1	tvořící
převodní	převodní	k2eAgFnSc4d1	převodní
soustavu	soustava	k1gFnSc4	soustava
srdeční	srdeční	k2eAgFnSc4d1	srdeční
<g/>
.	.	kIx.	.
</s>
<s>
Této	tento	k3xDgFnSc3	tento
vlastnosti	vlastnost	k1gFnSc3	vlastnost
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
automacie	automacie	k1gFnSc1	automacie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
kosterního	kosterní	k2eAgInSc2d1	kosterní
svalu	sval	k1gInSc2	sval
se	se	k3xPyFc4	se
membránový	membránový	k2eAgInSc4d1	membránový
potenciál	potenciál	k1gInSc4	potenciál
po	po	k7c6	po
depolarizaci	depolarizace	k1gFnSc6	depolarizace
nevrací	vracet	k5eNaImIp3nS	vracet
rychle	rychle	k6eAd1	rychle
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
původní	původní	k2eAgFnSc4d1	původní
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
po	po	k7c4	po
asi	asi	k9	asi
200-350	[number]	k4	200-350
ms	ms	k?	ms
ve	v	k7c6	v
fázi	fáze	k1gFnSc6	fáze
plató	plató	k1gNnSc2	plató
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
membránový	membránový	k2eAgInSc1d1	membránový
potenciál	potenciál	k1gInSc1	potenciál
kladný	kladný	k2eAgInSc1d1	kladný
a	a	k8xC	a
buňka	buňka	k1gFnSc1	buňka
nereaguje	reagovat	k5eNaBmIp3nS	reagovat
na	na	k7c4	na
další	další	k2eAgInSc4d1	další
vzruch	vzruch	k1gInSc4	vzruch
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
pak	pak	k6eAd1	pak
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
repolarizaci	repolarizace	k1gFnSc3	repolarizace
a	a	k8xC	a
buňka	buňka	k1gFnSc1	buňka
je	být	k5eAaImIp3nS	být
schopna	schopen	k2eAgFnSc1d1	schopna
další	další	k2eAgFnSc1d1	další
kontrakce	kontrakce	k1gFnSc1	kontrakce
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
nemůže	moct	k5eNaImIp3nS	moct
v	v	k7c6	v
srdeční	srdeční	k2eAgFnSc6d1	srdeční
svalovině	svalovina	k1gFnSc6	svalovina
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
tetanické	tetanický	k2eAgFnSc3d1	tetanický
křeči	křeč	k1gFnSc3	křeč
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
délce	délka	k1gFnSc3	délka
fáze	fáze	k1gFnSc2	fáze
plató	plató	k1gNnSc2	plató
také	také	k6eAd1	také
maximální	maximální	k2eAgFnSc1d1	maximální
tepová	tepový	k2eAgFnSc1d1	tepová
frekvence	frekvence	k1gFnSc1	frekvence
nemůže	moct	k5eNaImIp3nS	moct
překročit	překročit	k5eAaPmF	překročit
200	[number]	k4	200
-	-	kIx~	-
210	[number]	k4	210
tepů	tep	k1gInPc2	tep
za	za	k7c4	za
minutu	minuta	k1gFnSc4	minuta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Převodní	převodní	k2eAgInSc1d1	převodní
systém	systém	k1gInSc1	systém
srdeční	srdeční	k2eAgInSc1d1	srdeční
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
membránách	membrána	k1gFnPc6	membrána
buněk	buňka	k1gFnPc2	buňka
převodního	převodní	k2eAgInSc2d1	převodní
systému	systém	k1gInSc2	systém
se	se	k3xPyFc4	se
neustále	neustále	k6eAd1	neustále
spontánně	spontánně	k6eAd1	spontánně
mění	měnit	k5eAaImIp3nS	měnit
membránový	membránový	k2eAgInSc4d1	membránový
potenciál	potenciál	k1gInSc4	potenciál
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dosažení	dosažení	k1gNnSc6	dosažení
spouštěcí	spouštěcí	k2eAgFnSc2d1	spouštěcí
úrovně	úroveň	k1gFnSc2	úroveň
(	(	kIx(	(
<g/>
spontánní	spontánní	k2eAgFnSc1d1	spontánní
diastolická	diastolický	k2eAgFnSc1d1	diastolická
repolarizace	repolarizace	k1gFnSc1	repolarizace
<g/>
)	)	kIx)	)
vzniká	vznikat	k5eAaImIp3nS	vznikat
akční	akční	k2eAgInSc4d1	akční
potenciál	potenciál	k1gInSc4	potenciál
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
šíří	šířit	k5eAaImIp3nS	šířit
do	do	k7c2	do
pracovního	pracovní	k2eAgInSc2d1	pracovní
myokardu	myokard	k1gInSc2	myokard
a	a	k8xC	a
způsobí	způsobit	k5eAaPmIp3nS	způsobit
jeho	jeho	k3xOp3gFnSc4	jeho
kontrakci	kontrakce	k1gFnSc4	kontrakce
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
akčního	akční	k2eAgInSc2d1	akční
potenciálu	potenciál	k1gInSc2	potenciál
se	se	k3xPyFc4	se
na	na	k7c6	na
membránách	membrána	k1gFnPc6	membrána
opět	opět	k6eAd1	opět
začne	začít	k5eAaPmIp3nS	začít
tvořit	tvořit	k5eAaImF	tvořit
nový	nový	k2eAgInSc4d1	nový
akční	akční	k2eAgInSc4d1	akční
potenciál	potenciál	k1gInSc4	potenciál
<g/>
.	.	kIx.	.
</s>
<s>
Buňky	buňka	k1gFnPc1	buňka
samotné	samotný	k2eAgFnPc1d1	samotná
jsou	být	k5eAaImIp3nP	být
tedy	tedy	k9	tedy
zdroj	zdroj	k1gInSc4	zdroj
vzruchů	vzruch	k1gInPc2	vzruch
<g/>
.	.	kIx.	.
</s>
<s>
Akční	akční	k2eAgInPc1d1	akční
potenciály	potenciál	k1gInPc1	potenciál
vznikají	vznikat	k5eAaImIp3nP	vznikat
pravidelně	pravidelně	k6eAd1	pravidelně
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
příčinou	příčina	k1gFnSc7	příčina
rytmické	rytmický	k2eAgFnSc2d1	rytmická
práce	práce	k1gFnSc2	práce
srdce	srdce	k1gNnSc2	srdce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
zdrojem	zdroj	k1gInSc7	zdroj
vzruchů	vzruch	k1gInPc2	vzruch
u	u	k7c2	u
savců	savec	k1gMnPc2	savec
je	být	k5eAaImIp3nS	být
sinoatriální	sinoatriální	k2eAgInSc1d1	sinoatriální
uzel	uzel	k1gInSc1	uzel
(	(	kIx(	(
<g/>
SA	SA	kA	SA
uzel	uzel	k1gInSc1	uzel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
shluk	shluk	k1gInSc1	shluk
buněk	buňka	k1gFnPc2	buňka
převodního	převodní	k2eAgInSc2d1	převodní
systému	systém	k1gInSc2	systém
srdečního	srdeční	k2eAgInSc2d1	srdeční
ve	v	k7c6	v
stěně	stěna	k1gFnSc6	stěna
pravé	pravý	k2eAgFnSc2d1	pravá
síně	síň	k1gFnSc2	síň
blízko	blízko	k7c2	blízko
žilního	žilní	k2eAgInSc2d1	žilní
splavu	splav	k1gInSc2	splav
<g/>
.	.	kIx.	.
</s>
<s>
Tady	tady	k6eAd1	tady
je	být	k5eAaImIp3nS	být
spontánní	spontánní	k2eAgFnSc1d1	spontánní
depolarizace	depolarizace	k1gFnSc1	depolarizace
nejrychlejší	rychlý	k2eAgFnSc1d3	nejrychlejší
<g/>
,	,	kIx,	,
SA	SA	kA	SA
uzel	uzel	k1gInSc1	uzel
proto	proto	k8xC	proto
generuje	generovat	k5eAaImIp3nS	generovat
vzruchy	vzruch	k1gInPc4	vzruch
i	i	k9	i
pro	pro	k7c4	pro
ostatní	ostatní	k2eAgFnPc4d1	ostatní
části	část	k1gFnPc4	část
převodního	převodní	k2eAgInSc2d1	převodní
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Rytmus	rytmus	k1gInSc1	rytmus
srdečních	srdeční	k2eAgFnPc2d1	srdeční
frekvencí	frekvence	k1gFnPc2	frekvence
určuje	určovat	k5eAaImIp3nS	určovat
SA	SA	kA	SA
uzel	uzel	k1gInSc1	uzel
na	na	k7c4	na
počet	počet	k1gInSc4	počet
70	[number]	k4	70
tepů	tep	k1gInPc2	tep
za	za	k7c4	za
minutu	minuta	k1gFnSc4	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgInSc1	sám
uzel	uzel	k1gInSc1	uzel
je	být	k5eAaImIp3nS	být
regulován	regulovat	k5eAaImNgInS	regulovat
pokyny	pokyn	k1gInPc7	pokyn
z	z	k7c2	z
autonomního	autonomní	k2eAgNnSc2d1	autonomní
kardioregulačního	kardioregulační	k2eAgNnSc2d1	kardioregulační
centra	centrum	k1gNnSc2	centrum
v	v	k7c6	v
mozkovém	mozkový	k2eAgInSc6d1	mozkový
kmeni	kmen	k1gInSc6	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Centrum	centrum	k1gNnSc1	centrum
řízení	řízení	k1gNnSc2	řízení
srdeční	srdeční	k2eAgFnSc2d1	srdeční
činnosti	činnost	k1gFnSc2	činnost
je	být	k5eAaImIp3nS	být
umístěno	umístit	k5eAaPmNgNnS	umístit
v	v	k7c6	v
prodloužené	prodloužený	k2eAgFnSc6d1	prodloužená
míše	mícha	k1gFnSc6	mícha
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
určitých	určitý	k2eAgFnPc2d1	určitá
okolností	okolnost	k1gFnPc2	okolnost
ale	ale	k8xC	ale
může	moct	k5eAaImIp3nS	moct
vzruch	vzruch	k1gInSc4	vzruch
vznikat	vznikat	k5eAaImF	vznikat
i	i	k9	i
jinde	jinde	k6eAd1	jinde
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
projeví	projevit	k5eAaPmIp3nS	projevit
změnou	změna	k1gFnSc7	změna
frekvence	frekvence	k1gFnSc2	frekvence
tvorby	tvorba	k1gFnSc2	tvorba
vzruchů	vzruch	k1gInPc2	vzruch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
přepážce	přepážka	k1gFnSc6	přepážka
mezi	mezi	k7c7	mezi
síněmi	síň	k1gFnPc7	síň
a	a	k8xC	a
komorami	komora	k1gFnPc7	komora
je	být	k5eAaImIp3nS	být
atrioventrikulární	atrioventrikulární	k2eAgInSc1d1	atrioventrikulární
uzel	uzel	k1gInSc1	uzel
(	(	kIx(	(
<g/>
AV	AV	kA	AV
uzel	uzel	k1gInSc1	uzel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
běžných	běžný	k2eAgFnPc2d1	běžná
okolností	okolnost	k1gFnPc2	okolnost
pouze	pouze	k6eAd1	pouze
převádí	převádět	k5eAaImIp3nS	převádět
vzruch	vzruch	k1gInSc1	vzruch
z	z	k7c2	z
SA	SA	kA	SA
uzlu	uzel	k1gInSc2	uzel
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
ale	ale	k9	ale
generovat	generovat	k5eAaImF	generovat
vzruch	vzruch	k1gInSc4	vzruch
pro	pro	k7c4	pro
celé	celý	k2eAgNnSc4d1	celé
srdce	srdce	k1gNnSc4	srdce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
AV	AV	kA	AV
uzlu	uzel	k1gInSc2	uzel
vychází	vycházet	k5eAaImIp3nS	vycházet
Hissův	Hissův	k2eAgInSc4d1	Hissův
svazek	svazek	k1gInSc4	svazek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
mezikomorové	mezikomorový	k2eAgFnSc6d1	mezikomorový
přepážce	přepážka	k1gFnSc6	přepážka
rozdělí	rozdělit	k5eAaPmIp3nS	rozdělit
na	na	k7c4	na
dvě	dva	k4xCgNnPc4	dva
Tawarova	Tawarův	k2eAgNnPc4d1	Tawarův
raménka	raménko	k1gNnPc4	raménko
<g/>
,	,	kIx,	,
pravé	pravý	k2eAgFnSc6d1	pravá
a	a	k8xC	a
levé	levý	k2eAgFnSc6d1	levá
<g/>
.	.	kIx.	.
</s>
<s>
Každé	každý	k3xTgNnSc1	každý
raménko	raménko	k1gNnSc1	raménko
míří	mířit	k5eAaImIp3nS	mířit
k	k	k7c3	k
pracovnímu	pracovní	k2eAgInSc3d1	pracovní
myokardu	myokard	k1gInSc3	myokard
komor	komora	k1gFnPc2	komora
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
větví	větvit	k5eAaImIp3nS	větvit
na	na	k7c4	na
Purkyňova	Purkyňův	k2eAgNnPc4d1	Purkyňovo
vlákna	vlákno	k1gNnPc4	vlákno
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
probíhají	probíhat	k5eAaImIp3nP	probíhat
pod	pod	k7c7	pod
endokardem	endokard	k1gInSc7	endokard
a	a	k8xC	a
šíří	šířit	k5eAaImIp3nS	šířit
vzruch	vzruch	k1gInSc1	vzruch
do	do	k7c2	do
stěny	stěna	k1gFnSc2	stěna
komor	komora	k1gFnPc2	komora
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
SA	SA	kA	SA
uzlu	uzel	k1gInSc2	uzel
a	a	k8xC	a
AV	AV	kA	AV
uzlu	uzel	k1gInSc2	uzel
je	být	k5eAaImIp3nS	být
rychlost	rychlost	k1gFnSc1	rychlost
šíření	šíření	k1gNnSc2	šíření
vzruchu	vzruch	k1gInSc2	vzruch
0,02	[number]	k4	0,02
<g/>
-	-	kIx~	-
<g/>
0,1	[number]	k4	0,1
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
,	,	kIx,	,
ve	v	k7c6	v
zbytku	zbytek	k1gInSc6	zbytek
převodního	převodní	k2eAgInSc2d1	převodní
systému	systém	k1gInSc2	systém
se	se	k3xPyFc4	se
vzruch	vzruch	k1gInSc1	vzruch
šíří	šířit	k5eAaImIp3nS	šířit
rychlostí	rychlost	k1gFnSc7	rychlost
až	až	k9	až
4	[number]	k4	4
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Mezi	mezi	k7c7	mezi
buňkami	buňka	k1gFnPc7	buňka
pracovního	pracovní	k2eAgInSc2d1	pracovní
myokardu	myokard	k1gInSc2	myokard
je	být	k5eAaImIp3nS	být
šíření	šíření	k1gNnSc4	šíření
vzruchu	vzruch	k1gInSc2	vzruch
pomalejší	pomalý	k2eAgFnSc1d2	pomalejší
<g/>
,	,	kIx,	,
do	do	k7c2	do
1	[number]	k4	1
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
U	u	k7c2	u
zdravého	zdravý	k2eAgNnSc2d1	zdravé
srdce	srdce	k1gNnSc2	srdce
je	být	k5eAaImIp3nS	být
směr	směr	k1gInSc1	směr
šíření	šíření	k1gNnSc2	šíření
vzruchů	vzruch	k1gInPc2	vzruch
v	v	k7c6	v
určitém	určitý	k2eAgInSc6d1	určitý
okamžiku	okamžik	k1gInSc6	okamžik
vždy	vždy	k6eAd1	vždy
stejný	stejný	k2eAgInSc1d1	stejný
<g/>
.	.	kIx.	.
</s>
<s>
Výsledné	výsledný	k2eAgInPc1d1	výsledný
vektory	vektor	k1gInPc1	vektor
vzruchu	vzruch	k1gInSc2	vzruch
můžeme	moct	k5eAaImIp1nP	moct
snímat	snímat	k5eAaImF	snímat
pomocí	pomocí	k7c2	pomocí
EKG	EKG	kA	EKG
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Řízení	řízení	k1gNnSc1	řízení
srdeční	srdeční	k2eAgFnSc2d1	srdeční
frekvence	frekvence	k1gFnSc2	frekvence
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Nervové	nervový	k2eAgNnSc1d1	nervové
řízení	řízení	k1gNnSc1	řízení
====	====	k?	====
</s>
</p>
<p>
<s>
Nejvýznamnější	významný	k2eAgNnSc1d3	nejvýznamnější
řízení	řízení	k1gNnSc1	řízení
frekvence	frekvence	k1gFnSc2	frekvence
je	být	k5eAaImIp3nS	být
řízení	řízení	k1gNnSc1	řízení
nervové	nervový	k2eAgNnSc1d1	nervové
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
autonomních	autonomní	k2eAgInPc2d1	autonomní
nervů	nerv	k1gInPc2	nerv
<g/>
.	.	kIx.	.
</s>
<s>
Sinoatriální	Sinoatriální	k2eAgInSc1d1	Sinoatriální
uzel	uzel	k1gInSc1	uzel
je	být	k5eAaImIp3nS	být
pod	pod	k7c7	pod
tonickým	tonický	k2eAgInSc7d1	tonický
vlivem	vliv	k1gInSc7	vliv
vegetativního	vegetativní	k2eAgInSc2d1	vegetativní
nervového	nervový	k2eAgInSc2d1	nervový
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
tak	tak	k6eAd1	tak
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
rychlost	rychlost	k1gFnSc1	rychlost
tvorby	tvorba	k1gFnSc2	tvorba
vzruchů	vzruch	k1gInPc2	vzruch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Parasympatická	parasympatický	k2eAgFnSc1d1	parasympatická
nervová	nervový	k2eAgFnSc1d1	nervová
vlákna	vlákna	k1gFnSc1	vlákna
<g/>
,	,	kIx,	,
nn	nn	k?	nn
<g/>
.	.	kIx.	.
retardantes	retardantes	k1gMnSc1	retardantes
<g/>
,	,	kIx,	,
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
bloudivého	bloudivý	k2eAgInSc2d1	bloudivý
nervu	nerv	k1gInSc2	nerv
<g/>
.	.	kIx.	.
</s>
<s>
Působí	působit	k5eAaImIp3nS	působit
na	na	k7c4	na
srdeční	srdeční	k2eAgFnSc4d1	srdeční
činnost	činnost	k1gFnSc4	činnost
zpomalením	zpomalení	k1gNnSc7	zpomalení
srdeční	srdeční	k2eAgFnSc2d1	srdeční
frekvence	frekvence	k1gFnSc2	frekvence
<g/>
,	,	kIx,	,
snížením	snížení	k1gNnSc7	snížení
síly	síla	k1gFnSc2	síla
kontrakce	kontrakce	k1gFnSc2	kontrakce
a	a	k8xC	a
snížením	snížení	k1gNnSc7	snížení
vzrušivosti	vzrušivost	k1gFnSc2	vzrušivost
myokardu	myokard	k1gInSc2	myokard
<g/>
.	.	kIx.	.
</s>
<s>
Účinek	účinek	k1gInSc1	účinek
parasympatiku	parasympatikus	k1gInSc2	parasympatikus
na	na	k7c4	na
srdeční	srdeční	k2eAgInSc4d1	srdeční
sval	sval	k1gInSc4	sval
je	být	k5eAaImIp3nS	být
zprostředkován	zprostředkovat	k5eAaPmNgInS	zprostředkovat
mediátorem	mediátor	k1gInSc7	mediátor
acetylcholinem	acetylcholin	k1gInSc7	acetylcholin
<g/>
,	,	kIx,	,
receptory	receptor	k1gInPc7	receptor
v	v	k7c6	v
srdeční	srdeční	k2eAgFnSc6d1	srdeční
tkáni	tkáň	k1gFnSc6	tkáň
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
zablokovány	zablokovat	k5eAaPmNgInP	zablokovat
atropinem	atropin	k1gInSc7	atropin
(	(	kIx(	(
<g/>
proto	proto	k8xC	proto
atropin	atropin	k1gInSc1	atropin
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
srdeční	srdeční	k2eAgFnSc4d1	srdeční
frekvenci	frekvence	k1gFnSc4	frekvence
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sympatická	sympatický	k2eAgFnSc1d1	sympatická
nervová	nervový	k2eAgFnSc1d1	nervová
vlákna	vlákna	k1gFnSc1	vlákna
<g/>
,	,	kIx,	,
nn	nn	k?	nn
<g/>
.	.	kIx.	.
accelerantes	accelerantes	k1gMnSc1	accelerantes
<g/>
,	,	kIx,	,
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
hrudních	hrudní	k2eAgFnPc2d1	hrudní
sympatických	sympatický	k2eAgFnPc2d1	sympatická
ganglií	ganglie	k1gFnPc2	ganglie
<g/>
.	.	kIx.	.
</s>
<s>
Působí	působit	k5eAaImIp3nS	působit
na	na	k7c4	na
srdeční	srdeční	k2eAgFnSc4d1	srdeční
činnost	činnost	k1gFnSc4	činnost
zrychlením	zrychlení	k1gNnSc7	zrychlení
srdeční	srdeční	k2eAgFnSc2d1	srdeční
frekvence	frekvence	k1gFnSc2	frekvence
<g/>
,	,	kIx,	,
zvýšením	zvýšení	k1gNnSc7	zvýšení
síly	síla	k1gFnSc2	síla
kontrakce	kontrakce	k1gFnSc2	kontrakce
a	a	k8xC	a
zvýšením	zvýšení	k1gNnSc7	zvýšení
vzrušivosti	vzrušivost	k1gFnSc2	vzrušivost
myokardu	myokard	k1gInSc2	myokard
<g/>
.	.	kIx.	.
</s>
<s>
Mediátorem	Mediátor	k1gInSc7	Mediátor
sympatiku	sympatik	k1gInSc2	sympatik
je	být	k5eAaImIp3nS	být
noradrenalin	noradrenalin	k1gInSc1	noradrenalin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
=====	=====	k?	=====
Baroreceptorové	Baroreceptorový	k2eAgInPc1d1	Baroreceptorový
reflexy	reflex	k1gInPc1	reflex
=====	=====	k?	=====
</s>
</p>
<p>
<s>
V	v	k7c6	v
oblouku	oblouk	k1gInSc6	oblouk
aorty	aorta	k1gFnSc2	aorta
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
baroreceptory	baroreceptor	k1gInPc1	baroreceptor
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
snímají	snímat	k5eAaImIp3nP	snímat
krevní	krevní	k2eAgInSc4d1	krevní
tlak	tlak	k1gInSc4	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zvýšení	zvýšení	k1gNnSc6	zvýšení
tlaku	tlak	k1gInSc2	tlak
krve	krev	k1gFnSc2	krev
je	být	k5eAaImIp3nS	být
utlumen	utlumit	k5eAaPmNgInS	utlumit
sympatikus	sympatikus	k1gInSc1	sympatikus
<g/>
,	,	kIx,	,
srdeční	srdeční	k2eAgFnSc1d1	srdeční
frekvence	frekvence	k1gFnSc1	frekvence
se	se	k3xPyFc4	se
sníží	snížit	k5eAaPmIp3nS	snížit
a	a	k8xC	a
tlak	tlak	k1gInSc1	tlak
krve	krev	k1gFnSc2	krev
poklesne	poklesnout	k5eAaPmIp3nS	poklesnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Humorální	humorální	k2eAgNnSc1d1	humorální
řízení	řízení	k1gNnSc1	řízení
====	====	k?	====
</s>
</p>
<p>
<s>
Katecholaminy	Katecholamin	k1gInPc1	Katecholamin
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
adrenalin	adrenalin	k1gInSc1	adrenalin
a	a	k8xC	a
noradrenalin	noradrenalin	k1gInSc1	noradrenalin
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
na	na	k7c4	na
srdce	srdce	k1gNnPc4	srdce
stejný	stejný	k2eAgInSc4d1	stejný
účinek	účinek	k1gInSc4	účinek
jako	jako	k8xS	jako
sympatikus	sympatikus	k1gInSc4	sympatikus
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
acetylcholin	acetylcholin	k1gInSc1	acetylcholin
působí	působit	k5eAaImIp3nS	působit
na	na	k7c4	na
srdce	srdce	k1gNnSc4	srdce
jako	jako	k8xS	jako
parasympatikus	parasympatikus	k1gInSc4	parasympatikus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Glukagon	Glukagon	k1gInSc1	Glukagon
zrychluje	zrychlovat	k5eAaImIp3nS	zrychlovat
srdeční	srdeční	k2eAgFnSc4d1	srdeční
frekvenci	frekvence	k1gFnSc4	frekvence
a	a	k8xC	a
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
sílu	síla	k1gFnSc4	síla
kontrakcí	kontrakce	k1gFnPc2	kontrakce
<g/>
,	,	kIx,	,
inzulín	inzulín	k1gInSc1	inzulín
také	také	k9	také
zrychluje	zrychlovat	k5eAaImIp3nS	zrychlovat
srdeční	srdeční	k2eAgFnSc4d1	srdeční
činnost	činnost	k1gFnSc4	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Progesteron	progesteron	k1gInSc1	progesteron
naopak	naopak	k6eAd1	naopak
srdeční	srdeční	k2eAgFnSc4d1	srdeční
frekvenci	frekvence	k1gFnSc4	frekvence
zpomaluje	zpomalovat	k5eAaImIp3nS	zpomalovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Vliv	vliv	k1gInSc4	vliv
teploty	teplota	k1gFnSc2	teplota
na	na	k7c4	na
tepovou	tepový	k2eAgFnSc4d1	tepová
frekvenci	frekvence	k1gFnSc4	frekvence
====	====	k?	====
</s>
</p>
<p>
<s>
Frekvence	frekvence	k1gFnSc1	frekvence
s	s	k7c7	s
jakou	jaký	k3yRgFnSc7	jaký
sinoatriální	sinoatriální	k2eAgInSc1d1	sinoatriální
uzel	uzel	k1gInSc1	uzel
generuje	generovat	k5eAaImIp3nS	generovat
vzruchy	vzruch	k1gInPc4	vzruch
závisí	záviset	k5eAaImIp3nS	záviset
i	i	k9	i
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
teplotě	teplota	k1gFnSc6	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Třetím	třetí	k4xOgInSc7	třetí
faktorem	faktor	k1gInSc7	faktor
ovlivňujícím	ovlivňující	k2eAgInSc7d1	ovlivňující
srdeční	srdeční	k2eAgInSc1d1	srdeční
frekvenci	frekvence	k1gFnSc4	frekvence
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
tělesná	tělesný	k2eAgFnSc1d1	tělesná
teplota	teplota	k1gFnSc1	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
zvýšení	zvýšení	k1gNnSc1	zvýšení
teploty	teplota	k1gFnSc2	teplota
o	o	k7c4	o
1	[number]	k4	1
°	°	k?	°
<g/>
C	C	kA	C
se	se	k3xPyFc4	se
projeví	projevit	k5eAaPmIp3nS	projevit
vzestupem	vzestup	k1gInSc7	vzestup
frekvence	frekvence	k1gFnSc2	frekvence
srdečního	srdeční	k2eAgInSc2d1	srdeční
tepu	tep	k1gInSc2	tep
asi	asi	k9	asi
o	o	k7c4	o
10	[number]	k4	10
úderů	úder	k1gInPc2	úder
za	za	k7c4	za
minutu	minuta	k1gFnSc4	minuta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Srdce	srdce	k1gNnPc1	srdce
jako	jako	k8xS	jako
endokrinní	endokrinní	k2eAgFnSc1d1	endokrinní
žláza	žláza	k1gFnSc1	žláza
==	==	k?	==
</s>
</p>
<p>
<s>
Srdce	srdce	k1gNnSc1	srdce
produkuje	produkovat	k5eAaImIp3nS	produkovat
hormon	hormon	k1gInSc1	hormon
<g/>
,	,	kIx,	,
atriový	atriový	k2eAgInSc1d1	atriový
natriuretický	natriuretický	k2eAgInSc1d1	natriuretický
peptid	peptid	k1gInSc1	peptid
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
má	mít	k5eAaImIp3nS	mít
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
krevní	krevní	k2eAgFnPc4d1	krevní
cévy	céva	k1gFnPc4	céva
<g/>
,	,	kIx,	,
ledviny	ledvina	k1gFnPc4	ledvina
a	a	k8xC	a
nadledviny	nadledvina	k1gFnPc4	nadledvina
a	a	k8xC	a
podílí	podílet	k5eAaImIp3nS	podílet
se	se	k3xPyFc4	se
na	na	k7c4	na
regulaci	regulace	k1gFnSc4	regulace
krevního	krevní	k2eAgInSc2d1	krevní
tlaku	tlak	k1gInSc2	tlak
a	a	k8xC	a
krevního	krevní	k2eAgInSc2d1	krevní
objemu	objem	k1gInSc2	objem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vývoj	vývoj	k1gInSc1	vývoj
srdce	srdce	k1gNnSc2	srdce
==	==	k?	==
</s>
</p>
<p>
<s>
Srdce	srdce	k1gNnSc1	srdce
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
orgánů	orgán	k1gInPc2	orgán
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
u	u	k7c2	u
embrya	embryo	k1gNnSc2	embryo
začnou	začít	k5eAaPmIp3nP	začít
vyvíjet	vyvíjet	k5eAaImF	vyvíjet
<g/>
.	.	kIx.	.
</s>
<s>
Začíná	začínat	k5eAaImIp3nS	začínat
bít	bít	k5eAaImF	bít
zhruba	zhruba	k6eAd1	zhruba
pět	pět	k4xCc4	pět
týdnů	týden	k1gInPc2	týden
po	po	k7c6	po
poslední	poslední	k2eAgFnSc6d1	poslední
normální	normální	k2eAgFnSc6d1	normální
menstruaci	menstruace	k1gFnSc6	menstruace
(	(	kIx(	(
<g/>
tři	tři	k4xCgInPc4	tři
týdny	týden	k1gInPc4	týden
po	po	k7c6	po
početí	početí	k1gNnSc6	početí
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
tep	tep	k1gInSc1	tep
se	se	k3xPyFc4	se
zpočátku	zpočátku	k6eAd1	zpočátku
plynule	plynule	k6eAd1	plynule
zrychluje	zrychlovat	k5eAaImIp3nS	zrychlovat
až	až	k9	až
na	na	k7c4	na
hodnoty	hodnota	k1gFnPc4	hodnota
kolem	kolem	k7c2	kolem
280	[number]	k4	280
tepů	tep	k1gInPc2	tep
za	za	k7c4	za
minutu	minuta	k1gFnSc4	minuta
v	v	k7c6	v
7	[number]	k4	7
<g/>
.	.	kIx.	.
až	až	k9	až
9	[number]	k4	9
<g/>
.	.	kIx.	.
týdnu	týden	k1gInSc6	týden
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
frekvence	frekvence	k1gFnSc1	frekvence
opět	opět	k6eAd1	opět
klesá	klesat	k5eAaImIp3nS	klesat
až	až	k9	až
na	na	k7c4	na
150	[number]	k4	150
tepů	tep	k1gInPc2	tep
v	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
týdnu	týden	k1gInSc6	týden
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
porodu	porod	k1gInSc6	porod
se	se	k3xPyFc4	se
funkce	funkce	k1gFnSc1	funkce
srdce	srdce	k1gNnSc2	srdce
prudce	prudko	k6eAd1	prudko
změní	změnit	k5eAaPmIp3nS	změnit
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
dítě	dítě	k1gNnSc1	dítě
odpojí	odpojit	k5eAaPmIp3nS	odpojit
od	od	k7c2	od
matčina	matčin	k2eAgInSc2d1	matčin
krevního	krevní	k2eAgInSc2d1	krevní
oběhu	oběh	k1gInSc2	oběh
a	a	k8xC	a
začne	začít	k5eAaPmIp3nS	začít
dýchat	dýchat	k5eAaImF	dýchat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Srdce	srdce	k1gNnPc1	srdce
u	u	k7c2	u
různých	různý	k2eAgMnPc2d1	různý
živočichů	živočich	k1gMnPc2	živočich
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Srdeční	srdeční	k2eAgFnSc1d1	srdeční
frekvence	frekvence	k1gFnSc1	frekvence
===	===	k?	===
</s>
</p>
<p>
<s>
Čím	co	k3yRnSc7	co
menší	malý	k2eAgMnSc1d2	menší
živočich	živočich	k1gMnSc1	živočich
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
srdeční	srdeční	k2eAgFnSc1d1	srdeční
frekvence	frekvence	k1gFnSc1	frekvence
rychlejší	rychlý	k2eAgFnSc1d2	rychlejší
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
mláďat	mládě	k1gNnPc2	mládě
bije	bít	k5eAaImIp3nS	bít
srdce	srdce	k1gNnSc1	srdce
rychleji	rychle	k6eAd2	rychle
než	než	k8xS	než
u	u	k7c2	u
dospělých	dospělý	k2eAgMnPc2d1	dospělý
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Srdeční	srdeční	k2eAgFnSc1d1	srdeční
frekvence	frekvence	k1gFnSc1	frekvence
plejtvákovce	plejtvákovec	k1gInSc2	plejtvákovec
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
9	[number]	k4	9
úderů	úder	k1gInPc2	úder
za	za	k7c4	za
minutu	minuta	k1gFnSc4	minuta
<g/>
,	,	kIx,	,
slona	slon	k1gMnSc4	slon
25	[number]	k4	25
stahů	stah	k1gInPc2	stah
za	za	k7c4	za
minutu	minuta	k1gFnSc4	minuta
<g/>
,	,	kIx,	,
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
70	[number]	k4	70
stahů	stah	k1gInPc2	stah
za	za	k7c4	za
minutu	minuta	k1gFnSc4	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Vrabec	Vrabec	k1gMnSc1	Vrabec
má	mít	k5eAaImIp3nS	mít
tepovou	tepový	k2eAgFnSc4d1	tepová
frekvenci	frekvence	k1gFnSc4	frekvence
500	[number]	k4	500
stahů	stah	k1gInPc2	stah
za	za	k7c4	za
minutu	minuta	k1gFnSc4	minuta
<g/>
,	,	kIx,	,
rejsek	rejsek	k1gInSc4	rejsek
600	[number]	k4	600
stahů	stah	k1gInPc2	stah
za	za	k7c4	za
minutu	minuta	k1gFnSc4	minuta
<g/>
,	,	kIx,	,
letící	letící	k2eAgMnSc1d1	letící
kolibřík	kolibřík	k1gMnSc1	kolibřík
dokonce	dokonce	k9	dokonce
1200	[number]	k4	1200
stahů	stah	k1gInPc2	stah
za	za	k7c4	za
minutu	minuta	k1gFnSc4	minuta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Srdce	srdce	k1gNnSc1	srdce
ptáků	pták	k1gMnPc2	pták
===	===	k?	===
</s>
</p>
<p>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
u	u	k7c2	u
savců	savec	k1gMnPc2	savec
je	být	k5eAaImIp3nS	být
srdce	srdce	k1gNnSc1	srdce
ptáků	pták	k1gMnPc2	pták
čtyřkomorové	čtyřkomorový	k2eAgInPc1d1	čtyřkomorový
<g/>
,	,	kIx,	,
s	s	k7c7	s
dokonale	dokonale	k6eAd1	dokonale
oddělenými	oddělený	k2eAgFnPc7d1	oddělená
dutinami	dutina	k1gFnPc7	dutina
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
velikosti	velikost	k1gFnSc3	velikost
těla	tělo	k1gNnSc2	tělo
je	být	k5eAaImIp3nS	být
velké	velký	k2eAgNnSc1d1	velké
<g/>
,	,	kIx,	,
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
tělní	tělní	k2eAgFnSc6d1	tělní
dutině	dutina	k1gFnSc6	dutina
na	na	k7c6	na
hrudní	hrudní	k2eAgFnSc6d1	hrudní
kosti	kost	k1gFnSc6	kost
mezi	mezi	k7c7	mezi
plícemi	plíce	k1gFnPc7	plíce
a	a	k8xC	a
žaludkem	žaludek	k1gInSc7	žaludek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Srdce	srdce	k1gNnSc1	srdce
ostatních	ostatní	k2eAgMnPc2d1	ostatní
obratlovců	obratlovec	k1gMnPc2	obratlovec
===	===	k?	===
</s>
</p>
<p>
<s>
Nejjednodušší	jednoduchý	k2eAgNnPc1d3	nejjednodušší
srdce	srdce	k1gNnPc1	srdce
mají	mít	k5eAaImIp3nP	mít
ryby	ryba	k1gFnPc1	ryba
a	a	k8xC	a
paryby	paryba	k1gFnPc1	paryba
<g/>
;	;	kIx,	;
funguje	fungovat	k5eAaImIp3nS	fungovat
jako	jako	k9	jako
čerpadlo	čerpadlo	k1gNnSc4	čerpadlo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
žene	hnát	k5eAaImIp3nS	hnát
krev	krev	k1gFnSc1	krev
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
žábrám	žábry	k1gFnPc3	žábry
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
okysličována	okysličován	k2eAgFnSc1d1	okysličována
a	a	k8xC	a
pak	pak	k6eAd1	pak
rozváděna	rozváděn	k2eAgFnSc1d1	rozváděna
po	po	k7c6	po
těle	tělo	k1gNnSc6	tělo
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
vracela	vracet	k5eAaImAgFnS	vracet
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
srdce	srdce	k1gNnSc2	srdce
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nS	tvořit
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
síň	síň	k1gFnSc1	síň
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
přitéká	přitékat	k5eAaImIp3nS	přitékat
neokysličená	okysličený	k2eNgFnSc1d1	neokysličená
krev	krev	k1gFnSc1	krev
<g/>
,	,	kIx,	,
a	a	k8xC	a
jedna	jeden	k4xCgFnSc1	jeden
komora	komora	k1gFnSc1	komora
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vypuzuje	vypuzovat	k5eAaImIp3nS	vypuzovat
krev	krev	k1gFnSc4	krev
do	do	k7c2	do
žáber	žábra	k1gFnPc2	žábra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
U	u	k7c2	u
dospělých	dospělý	k2eAgMnPc2d1	dospělý
obojživelníků	obojživelník	k1gMnPc2	obojživelník
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
rozdělení	rozdělení	k1gNnSc3	rozdělení
síní	síň	k1gFnPc2	síň
<g/>
,	,	kIx,	,
srdce	srdce	k1gNnSc1	srdce
má	mít	k5eAaImIp3nS	mít
tedy	tedy	k9	tedy
jednu	jeden	k4xCgFnSc4	jeden
komoru	komora	k1gFnSc4	komora
a	a	k8xC	a
dvě	dva	k4xCgFnPc4	dva
síně	síň	k1gFnPc4	síň
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
primitivní	primitivní	k2eAgInSc4d1	primitivní
velký	velký	k2eAgInSc4d1	velký
a	a	k8xC	a
malý	malý	k2eAgInSc4d1	malý
krevní	krevní	k2eAgInSc4d1	krevní
oběh	oběh	k1gInSc4	oběh
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
míšení	míšení	k1gNnSc3	míšení
okysličené	okysličený	k2eAgFnSc2d1	okysličená
a	a	k8xC	a
neokysličené	okysličený	k2eNgFnSc2d1	neokysličená
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
U	u	k7c2	u
plazů	plaz	k1gMnPc2	plaz
se	se	k3xPyFc4	se
začíná	začínat	k5eAaImIp3nS	začínat
objevovat	objevovat	k5eAaImF	objevovat
náznak	náznak	k1gInSc4	náznak
mezikomorové	mezikomorový	k2eAgFnSc2d1	mezikomorový
přepážky	přepážka	k1gFnSc2	přepážka
<g/>
,	,	kIx,	,
u	u	k7c2	u
krokodýlů	krokodýl	k1gMnPc2	krokodýl
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
kompletní	kompletní	k2eAgFnSc1d1	kompletní
<g/>
,	,	kIx,	,
srdce	srdce	k1gNnSc1	srdce
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
čtyřkomorové	čtyřkomorový	k2eAgNnSc1d1	čtyřkomorové
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
u	u	k7c2	u
savců	savec	k1gMnPc2	savec
a	a	k8xC	a
ptáků	pták	k1gMnPc2	pták
a	a	k8xC	a
už	už	k6eAd1	už
nedochází	docházet	k5eNaImIp3nS	docházet
k	k	k7c3	k
míšení	míšení	k1gNnSc3	míšení
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Srdce	srdce	k1gNnSc1	srdce
bezobratlých	bezobratlí	k1gMnPc2	bezobratlí
===	===	k?	===
</s>
</p>
<p>
<s>
U	u	k7c2	u
bezobratlých	bezobratlý	k2eAgMnPc2d1	bezobratlý
zastává	zastávat	k5eAaImIp3nS	zastávat
funkci	funkce	k1gFnSc4	funkce
primitivního	primitivní	k2eAgNnSc2d1	primitivní
srdce	srdce	k1gNnSc2	srdce
většinou	většinou	k6eAd1	většinou
rozšířený	rozšířený	k2eAgInSc1d1	rozšířený
pulsující	pulsující	k2eAgInSc1d1	pulsující
úsek	úsek	k1gInSc1	úsek
cévy	céva	k1gFnSc2	céva
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
funguje	fungovat	k5eAaImIp3nS	fungovat
jako	jako	k9	jako
peristaltické	peristaltický	k2eAgNnSc4d1	peristaltické
čerpadlo	čerpadlo	k1gNnSc4	čerpadlo
<g/>
.	.	kIx.	.
</s>
<s>
Cirkulace	cirkulace	k1gFnSc1	cirkulace
tělních	tělní	k2eAgFnPc2d1	tělní
tekutin	tekutina	k1gFnPc2	tekutina
(	(	kIx(	(
<g/>
hemolymfy	hemolymf	k1gInPc1	hemolymf
<g/>
,	,	kIx,	,
krve	krev	k1gFnPc1	krev
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zajištěna	zajistit	k5eAaPmNgFnS	zajistit
peristaltickými	peristaltický	k2eAgInPc7d1	peristaltický
stahy	stah	k1gInPc7	stah
hladké	hladký	k2eAgFnSc2d1	hladká
svaloviny	svalovina	k1gFnSc2	svalovina
pulsující	pulsující	k2eAgFnSc2d1	pulsující
cévy	céva	k1gFnSc2	céva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Technické	technický	k2eAgFnPc1d1	technická
analogie	analogie	k1gFnPc1	analogie
srdce	srdce	k1gNnSc2	srdce
==	==	k?	==
</s>
</p>
<p>
<s>
Hledáme	hledat	k5eAaImIp1nP	hledat
<g/>
-li	i	k?	-li
technické	technický	k2eAgFnPc4d1	technická
analogie	analogie	k1gFnPc4	analogie
srdce	srdce	k1gNnSc2	srdce
a	a	k8xC	a
potažmo	potažmo	k6eAd1	potažmo
celé	celý	k2eAgFnSc2d1	celá
oběhové	oběhový	k2eAgFnSc2d1	oběhová
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
musíme	muset	k5eAaImIp1nP	muset
na	na	k7c6	na
prvním	první	k4xOgNnSc6	první
místě	místo	k1gNnSc6	místo
jmenovat	jmenovat	k5eAaImF	jmenovat
ty	ten	k3xDgFnPc1	ten
nejdokonalejší	dokonalý	k2eAgFnPc1d3	nejdokonalejší
-	-	kIx~	-
umělé	umělý	k2eAgNnSc4d1	umělé
srdce	srdce	k1gNnSc4	srdce
a	a	k8xC	a
funkční	funkční	k2eAgFnSc2d1	funkční
podpory	podpora	k1gFnSc2	podpora
srdce	srdce	k1gNnSc1	srdce
(	(	kIx(	(
<g/>
kardiostimulátory	kardiostimulátor	k1gInPc1	kardiostimulátor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lékařství	lékařství	k1gNnSc6	lékařství
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
úspěšně	úspěšně	k6eAd1	úspěšně
používány	používán	k2eAgInPc1d1	používán
nejrůznější	různý	k2eAgInPc1d3	nejrůznější
implantáty	implantát	k1gInPc1	implantát
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
umělé	umělý	k2eAgFnPc4d1	umělá
cévy	céva	k1gFnPc4	céva
či	či	k8xC	či
srdeční	srdeční	k2eAgFnPc4d1	srdeční
chlopně	chlopeň	k1gFnPc4	chlopeň
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
technického	technický	k2eAgNnSc2d1	technické
hlediska	hledisko	k1gNnSc2	hledisko
představuje	představovat	k5eAaImIp3nS	představovat
jistou	jistý	k2eAgFnSc4d1	jistá
funkční	funkční	k2eAgFnSc4d1	funkční
analogii	analogie	k1gFnSc4	analogie
srdce	srdce	k1gNnSc2	srdce
také	také	k9	také
např.	např.	kA	např.
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
pumpa	pumpa	k1gFnSc1	pumpa
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInPc1	jejíž
zpětné	zpětný	k2eAgInPc1d1	zpětný
ventily	ventil	k1gInPc1	ventil
plní	plnit	k5eAaImIp3nP	plnit
stejnou	stejný	k2eAgFnSc4d1	stejná
funkci	funkce	k1gFnSc4	funkce
jako	jako	k9	jako
u	u	k7c2	u
srdce	srdce	k1gNnSc2	srdce
chlopně	chlopeň	k1gFnSc2	chlopeň
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
pumpa	pumpa	k1gFnSc1	pumpa
nebyla	být	k5eNaImAgFnS	být
zkonstruována	zkonstruovat	k5eAaPmNgFnS	zkonstruovat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
studia	studio	k1gNnSc2	studio
funkce	funkce	k1gFnSc2	funkce
srdce	srdce	k1gNnSc2	srdce
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc7	jeho
chlopní	chlopeň	k1gFnSc7	chlopeň
<g/>
.	.	kIx.	.
</s>
<s>
Jistou	jistý	k2eAgFnSc4d1	jistá
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
velmi	velmi	k6eAd1	velmi
vzdálenou	vzdálený	k2eAgFnSc4d1	vzdálená
<g/>
,	,	kIx,	,
analogii	analogie	k1gFnSc4	analogie
celé	celý	k2eAgFnSc2d1	celá
oběhové	oběhový	k2eAgFnSc2d1	oběhová
soustavy	soustava	k1gFnSc2	soustava
představují	představovat	k5eAaImIp3nP	představovat
také	také	k9	také
hydraulické	hydraulický	k2eAgInPc4d1	hydraulický
a	a	k8xC	a
pneumatické	pneumatický	k2eAgInPc4d1	pneumatický
systémy	systém	k1gInPc4	systém
<g/>
,	,	kIx,	,
používané	používaný	k2eAgFnPc4d1	používaná
např.	např.	kA	např.
v	v	k7c6	v
robotice	robotika	k1gFnSc6	robotika
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
používají	používat	k5eAaImIp3nP	používat
jako	jako	k9	jako
ústřední	ústřední	k2eAgFnSc4d1	ústřední
jednotku	jednotka	k1gFnSc4	jednotka
hydraulický	hydraulický	k2eAgInSc4d1	hydraulický
či	či	k8xC	či
pneumatický	pneumatický	k2eAgInSc4d1	pneumatický
motor	motor	k1gInSc4	motor
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
funkce	funkce	k1gFnSc1	funkce
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
dala	dát	k5eAaPmAgFnS	dát
přirovnat	přirovnat	k5eAaPmF	přirovnat
k	k	k7c3	k
funkci	funkce	k1gFnSc3	funkce
srdce	srdce	k1gNnSc2	srdce
<g/>
,	,	kIx,	,
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
soustavu	soustava	k1gFnSc4	soustava
rozvodných	rozvodný	k2eAgFnPc2d1	rozvodná
hadic	hadice	k1gFnPc2	hadice
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
by	by	kYmCp3nS	by
snesla	snést	k5eAaPmAgFnS	snést
srovnání	srovnání	k1gNnSc4	srovnání
s	s	k7c7	s
cévní	cévní	k2eAgFnSc7d1	cévní
soustavou	soustava	k1gFnSc7	soustava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Choroby	choroba	k1gFnPc1	choroba
srdce	srdce	k1gNnSc2	srdce
==	==	k?	==
</s>
</p>
<p>
<s>
Stavbou	stavba	k1gFnSc7	stavba
<g/>
,	,	kIx,	,
funkcí	funkce	k1gFnSc7	funkce
a	a	k8xC	a
chorobami	choroba	k1gFnPc7	choroba
lidského	lidský	k2eAgNnSc2d1	lidské
srdce	srdce	k1gNnSc2	srdce
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
kardiologie	kardiologie	k1gFnSc2	kardiologie
<g/>
,	,	kIx,	,
srdeční	srdeční	k2eAgFnSc2d1	srdeční
choroby	choroba	k1gFnSc2	choroba
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejčastější	častý	k2eAgFnPc4d3	nejčastější
příčiny	příčina	k1gFnPc4	příčina
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
bohatých	bohatý	k2eAgFnPc6d1	bohatá
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
jsou	být	k5eAaImIp3nP	být
nejčastější	častý	k2eAgFnSc7d3	nejčastější
příčinou	příčina	k1gFnSc7	příčina
smrti	smrt	k1gFnSc2	smrt
a	a	k8xC	a
umírá	umírat	k5eAaImIp3nS	umírat
na	na	k7c4	na
ně	on	k3xPp3gNnSc4	on
asi	asi	k9	asi
50	[number]	k4	50
%	%	kIx~	%
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastější	častý	k2eAgFnSc7d3	nejčastější
příčinou	příčina	k1gFnSc7	příčina
úmrtí	úmrtí	k1gNnSc2	úmrtí
z	z	k7c2	z
nemocí	nemoc	k1gFnPc2	nemoc
srdce	srdce	k1gNnSc2	srdce
je	být	k5eAaImIp3nS	být
ischemická	ischemický	k2eAgFnSc1d1	ischemická
choroba	choroba	k1gFnSc1	choroba
srdeční	srdeční	k2eAgFnSc1d1	srdeční
<g/>
.	.	kIx.	.
</s>
<s>
Ischemické	ischemický	k2eAgFnPc1d1	ischemická
choroby	choroba	k1gFnPc1	choroba
srdce	srdce	k1gNnSc2	srdce
jsou	být	k5eAaImIp3nP	být
nejčastější	častý	k2eAgFnSc7d3	nejčastější
příčinou	příčina	k1gFnSc7	příčina
hospitalizace	hospitalizace	k1gFnSc2	hospitalizace
i	i	k8xC	i
úmrtí	úmrtí	k1gNnSc2	úmrtí
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
roční	roční	k2eAgFnSc6d1	roční
úmrtnosti	úmrtnost	k1gFnSc6	úmrtnost
se	se	k3xPyFc4	se
podílejí	podílet	k5eAaImIp3nP	podílet
přibližně	přibližně	k6eAd1	přibližně
čtvrtinou	čtvrtina	k1gFnSc7	čtvrtina
<g/>
.	.	kIx.	.
</s>
<s>
Statistika	statistika	k1gFnSc1	statistika
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
uvádí	uvádět	k5eAaImIp3nS	uvádět
75.499	[number]	k4	75.499
případů	případ	k1gInPc2	případ
hospitalizace	hospitalizace	k1gFnSc2	hospitalizace
pro	pro	k7c4	pro
ischemické	ischemický	k2eAgFnPc4d1	ischemická
choroby	choroba	k1gFnPc4	choroba
srdce	srdce	k1gNnSc2	srdce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ČR	ČR	kA	ČR
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
roce	rok	k1gInSc6	rok
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
na	na	k7c4	na
tyto	tento	k3xDgFnPc4	tento
choroby	choroba	k1gFnPc4	choroba
25.178	[number]	k4	25.178
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
na	na	k7c4	na
infarkt	infarkt	k1gInSc4	infarkt
myokardu	myokard	k1gInSc2	myokard
6439	[number]	k4	6439
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ischemická	ischemický	k2eAgFnSc1d1	ischemická
choroba	choroba	k1gFnSc1	choroba
srdeční	srdeční	k2eAgFnSc2d1	srdeční
</s>
</p>
<p>
<s>
Infarkt	infarkt	k1gInSc1	infarkt
myokardu	myokard	k1gInSc2	myokard
</s>
</p>
<p>
<s>
Chlopenní	chlopenní	k2eAgFnPc1d1	chlopenní
vady	vada	k1gFnPc1	vada
</s>
</p>
<p>
<s>
Infekční	infekční	k2eAgFnSc1d1	infekční
endokarditida	endokarditida	k1gFnSc1	endokarditida
</s>
</p>
<p>
<s>
Srdeční	srdeční	k2eAgFnSc1d1	srdeční
arytmie	arytmie	k1gFnSc1	arytmie
</s>
</p>
<p>
<s>
Vrozené	vrozený	k2eAgFnPc4d1	vrozená
srdeční	srdeční	k2eAgFnPc4d1	srdeční
vady	vada	k1gFnPc4	vada
</s>
</p>
<p>
<s>
==	==	k?	==
Srdce	srdce	k1gNnSc1	srdce
v	v	k7c6	v
přeneseném	přenesený	k2eAgInSc6d1	přenesený
významu	význam	k1gInSc6	význam
==	==	k?	==
</s>
</p>
<p>
<s>
Srdce	srdce	k1gNnSc1	srdce
se	se	k3xPyFc4	se
ale	ale	k9	ale
v	v	k7c6	v
hovorové	hovorový	k2eAgFnSc6d1	hovorová
i	i	k8xC	i
technické	technický	k2eAgFnSc3d1	technická
mluvě	mluva	k1gFnSc3	mluva
užívá	užívat	k5eAaImIp3nS	užívat
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
přenesených	přenesený	k2eAgFnPc6d1	přenesená
i	i	k8xC	i
naprosto	naprosto	k6eAd1	naprosto
konkrétních	konkrétní	k2eAgInPc6d1	konkrétní
významech	význam	k1gInPc6	význam
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Srdce	srdce	k1gNnSc1	srdce
je	být	k5eAaImIp3nS	být
všeobecně	všeobecně	k6eAd1	všeobecně
symbolem	symbol	k1gInSc7	symbol
lásky	láska	k1gFnSc2	láska
<g/>
,	,	kIx,	,
náklonnosti	náklonnost	k1gFnSc2	náklonnost
i	i	k8xC	i
přátelství	přátelství	k1gNnSc2	přátelství
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
rčení	rčení	k1gNnSc4	rčení
láska	láska	k1gFnSc1	láska
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
srdci	srdce	k1gNnSc6	srdce
aneb	aneb	k?	aneb
láska	láska	k1gFnSc1	láska
prochází	procházet	k5eAaImIp3nS	procházet
srdcem	srdce	k1gNnSc7	srdce
či	či	k8xC	či
nechal	nechat	k5eAaPmAgMnS	nechat
u	u	k7c2	u
ní	on	k3xPp3gFnSc2	on
své	svůj	k3xOyFgFnSc2	svůj
srdce	srdce	k1gNnSc4	srdce
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Naši	náš	k3xOp1gMnPc1	náš
předkové	předek	k1gMnPc1	předek
se	se	k3xPyFc4	se
domnívali	domnívat	k5eAaImAgMnP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
srdce	srdce	k1gNnSc1	srdce
je	být	k5eAaImIp3nS	být
sídlem	sídlo	k1gNnSc7	sídlo
citu	cit	k1gInSc2	cit
<g/>
,	,	kIx,	,
myšlení	myšlení	k1gNnSc2	myšlení
a	a	k8xC	a
duše	duše	k1gFnSc2	duše
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ale	ale	k9	ale
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
díky	díky	k7c3	díky
prokázanému	prokázaný	k2eAgInSc3d1	prokázaný
životu	život	k1gInSc3	život
s	s	k7c7	s
umělým	umělý	k2eAgNnSc7d1	umělé
srdcem	srdce	k1gNnSc7	srdce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Symbol	symbol	k1gInSc1	symbol
srdce	srdce	k1gNnSc2	srdce
může	moct	k5eAaImIp3nS	moct
sloužit	sloužit	k5eAaImF	sloužit
jako	jako	k9	jako
ozdoba	ozdoba	k1gFnSc1	ozdoba
či	či	k8xC	či
upomínka	upomínka	k1gFnSc1	upomínka
na	na	k7c4	na
nějakou	nějaký	k3yIgFnSc4	nějaký
osobu	osoba	k1gFnSc4	osoba
(	(	kIx(	(
<g/>
třeba	třeba	k6eAd1	třeba
perníkové	perníkový	k2eAgNnSc1d1	perníkové
či	či	k8xC	či
jiné	jiný	k2eAgNnSc1d1	jiné
ozdobné	ozdobný	k2eAgNnSc1d1	ozdobné
srdce	srdce	k1gNnSc1	srdce
<g/>
,	,	kIx,	,
srdíčko	srdíčko	k1gNnSc1	srdíčko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Může	moct	k5eAaImIp3nS	moct
však	však	k9	však
být	být	k5eAaImF	být
také	také	k9	také
symbolem	symbol	k1gInSc7	symbol
odvahy	odvaha	k1gFnSc2	odvaha
a	a	k8xC	a
energie	energie	k1gFnSc2	energie
<g/>
:	:	kIx,	:
bojovat	bojovat	k5eAaImF	bojovat
srdcem	srdce	k1gNnSc7	srdce
nebo	nebo	k8xC	nebo
srdnatě	srdnatě	k6eAd1	srdnatě
přeneseně	přeneseně	k6eAd1	přeneseně
znamená	znamenat	k5eAaImIp3nS	znamenat
odvážný	odvážný	k2eAgInSc1d1	odvážný
čin	čin	k1gInSc1	čin
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yQgNnSc2	který
člověk	člověk	k1gMnSc1	člověk
vložil	vložit	k5eAaPmAgMnS	vložit
svoji	svůj	k3xOyFgFnSc4	svůj
veškerou	veškerý	k3xTgFnSc4	veškerý
fyzickou	fyzický	k2eAgFnSc4d1	fyzická
a	a	k8xC	a
psychickou	psychický	k2eAgFnSc4d1	psychická
energii	energie	k1gFnSc4	energie
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
rčení	rčení	k1gNnPc2	rčení
nechali	nechat	k5eAaPmAgMnP	nechat
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
kus	kus	k1gInSc4	kus
srdce	srdce	k1gNnSc1	srdce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Srdeční	srdeční	k2eAgFnSc1d1	srdeční
záležitost	záležitost	k1gFnSc1	záležitost
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
týká	týkat	k5eAaImIp3nS	týkat
mimořádně	mimořádně	k6eAd1	mimořádně
blízké	blízký	k2eAgFnSc2d1	blízká
osoby	osoba	k1gFnSc2	osoba
či	či	k8xC	či
zvláště	zvláště	k6eAd1	zvláště
milé	milý	k2eAgFnPc4d1	Milá
věci	věc	k1gFnPc4	věc
(	(	kIx(	(
<g/>
záležitosti	záležitost	k1gFnPc4	záležitost
<g/>
,	,	kIx,	,
události	událost	k1gFnPc4	událost
<g/>
,	,	kIx,	,
předmětu	předmět	k1gInSc2	předmět
<g/>
,	,	kIx,	,
vlastnosti	vlastnost	k1gFnSc2	vlastnost
<g/>
,	,	kIx,	,
vztahu	vztah	k1gInSc2	vztah
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
nějakou	nějaký	k3yIgFnSc4	nějaký
konkrétní	konkrétní	k2eAgFnSc4d1	konkrétní
osobu	osoba	k1gFnSc4	osoba
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Nepokojné	pokojný	k2eNgNnSc4d1	nepokojné
srdce	srdce	k1gNnSc4	srdce
<g/>
"	"	kIx"	"
prý	prý	k9	prý
mívají	mívat	k5eAaImIp3nP	mívat
osoby	osoba	k1gFnPc4	osoba
neklidné	klidný	k2eNgFnPc4d1	neklidná
<g/>
,	,	kIx,	,
těkavé	těkavý	k2eAgFnPc4d1	těkavá
<g/>
,	,	kIx,	,
nestálé	stálý	k2eNgFnPc4d1	nestálá
<g/>
,	,	kIx,	,
hyperaktivní	hyperaktivní	k2eAgFnPc4d1	hyperaktivní
apod.	apod.	kA	apod.
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Srdce	srdce	k1gNnSc1	srdce
<g/>
"	"	kIx"	"
může	moct	k5eAaImIp3nS	moct
označovat	označovat	k5eAaImF	označovat
tu	ten	k3xDgFnSc4	ten
nejdůležitější	důležitý	k2eAgFnSc4d3	nejdůležitější
<g/>
,	,	kIx,	,
centrální	centrální	k2eAgFnSc4d1	centrální
část	část	k1gFnSc4	část
nějakého	nějaký	k3yIgInSc2	nějaký
většího	veliký	k2eAgInSc2d2	veliký
technického	technický	k2eAgInSc2d1	technický
či	či	k8xC	či
společenského	společenský	k2eAgInSc2d1	společenský
systému	systém	k1gInSc2	systém
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
termín	termín	k1gInSc1	termín
"	"	kIx"	"
<g/>
srdce	srdce	k1gNnSc1	srdce
stroje	stroj	k1gInSc2	stroj
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
srdce	srdce	k1gNnSc2	srdce
elektrárny	elektrárna	k1gFnSc2	elektrárna
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
srdce	srdce	k1gNnSc2	srdce
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
"	"	kIx"	"
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Srdce	srdce	k1gNnSc1	srdce
zvonu	zvon	k1gInSc2	zvon
je	být	k5eAaImIp3nS	být
volně	volně	k6eAd1	volně
zavěšené	zavěšený	k2eAgNnSc1d1	zavěšené
závaží	závaží	k1gNnSc1	závaží
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
"	"	kIx"	"
<g/>
bije	bít	k5eAaImIp3nS	bít
<g/>
"	"	kIx"	"
do	do	k7c2	do
zvonu	zvon	k1gInSc2	zvon
(	(	kIx(	(
<g/>
srovnej	srovnat	k5eAaPmRp2nS	srovnat
rčení	rčení	k1gNnSc4	rčení
má	mít	k5eAaImIp3nS	mít
srdce	srdce	k1gNnSc4	srdce
jako	jako	k8xC	jako
zvon	zvon	k1gInSc4	zvon
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Srdce	srdce	k1gNnSc1	srdce
je	být	k5eAaImIp3nS	být
také	také	k9	také
červená	červený	k2eAgFnSc1d1	červená
barva	barva	k1gFnSc1	barva
v	v	k7c6	v
karetních	karetní	k2eAgFnPc6d1	karetní
hrách	hra	k1gFnPc6	hra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Oběhová	oběhový	k2eAgFnSc1d1	oběhová
soustava	soustava	k1gFnSc1	soustava
</s>
</p>
<p>
<s>
Elektrokardiogram	elektrokardiogram	k1gInSc1	elektrokardiogram
</s>
</p>
<p>
<s>
Wiggersův	Wiggersův	k2eAgInSc1d1	Wiggersův
diagram	diagram	k1gInSc1	diagram
</s>
</p>
<p>
<s>
Transplantace	transplantace	k1gFnSc1	transplantace
srdce	srdce	k1gNnSc2	srdce
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Silbernagl	Silbernagl	k1gInSc1	Silbernagl
S.	S.	kA	S.
<g/>
,	,	kIx,	,
Despopoulos	Despopoulos	k1gMnSc1	Despopoulos
A.	A.	kA	A.
<g/>
:	:	kIx,	:
Atlas	Atlas	k1gInSc1	Atlas
fyziologie	fyziologie	k1gFnSc2	fyziologie
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Grada	Grada	k1gFnSc1	Grada
Avicenum	Avicenum	k1gInSc1	Avicenum
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
</s>
</p>
<p>
<s>
Stanislav	Stanislav	k1gMnSc1	Stanislav
Trojan	Trojan	k1gMnSc1	Trojan
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Lékařská	lékařský	k2eAgFnSc1d1	lékařská
fyziologie	fyziologie	k1gFnSc1	fyziologie
<g/>
.	.	kIx.	.
</s>
<s>
Grada	Grada	k1gFnSc1	Grada
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
</s>
</p>
<p>
<s>
Kolektiv	kolektiv	k1gInSc1	kolektiv
autorů	autor	k1gMnPc2	autor
<g/>
,	,	kIx,	,
Koldův	Koldův	k2eAgInSc4d1	Koldův
atlas	atlas	k1gInSc4	atlas
veterinární	veterinární	k2eAgFnSc2d1	veterinární
anatomie	anatomie	k1gFnSc2	anatomie
<g/>
.	.	kIx.	.
</s>
<s>
Grada	Grada	k1gFnSc1	Grada
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
</s>
</p>
<p>
<s>
Hugo	Hugo	k1gMnSc1	Hugo
Černý	Černý	k1gMnSc1	Černý
<g/>
,	,	kIx,	,
Veterinární	veterinární	k2eAgFnSc1d1	veterinární
anatomie	anatomie	k1gFnSc1	anatomie
<g/>
,	,	kIx,	,
Novico	Novico	k1gNnSc1	Novico
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
</s>
</p>
<p>
<s>
Ivan	Ivan	k1gMnSc1	Ivan
Novotný	Novotný	k1gMnSc1	Novotný
<g/>
,	,	kIx,	,
Michal	Michal	k1gMnSc1	Michal
Hruška	Hruška	k1gMnSc1	Hruška
<g/>
:	:	kIx,	:
Biologie	biologie	k1gFnSc1	biologie
člověka	člověk	k1gMnSc2	člověk
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
srdce	srdce	k1gNnSc2	srdce
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
srdce	srdce	k1gNnSc2	srdce
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Téma	téma	k1gNnSc1	téma
Srdce	srdce	k1gNnSc2	srdce
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
HeartWorks	HeartWorks	k1gInSc4	HeartWorks
-	-	kIx~	-
realistický	realistický	k2eAgInSc4d1	realistický
3D	[number]	k4	3D
model	model	k1gInSc4	model
srdce	srdce	k1gNnPc4	srdce
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Srdce	srdce	k1gNnPc1	srdce
a	a	k8xC	a
krení	krení	k2eAgInSc1d1	krení
oběh	oběh	k1gInSc1	oběh
(	(	kIx(	(
<g/>
animace	animace	k1gFnSc1	animace
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Srdce	srdce	k1gNnPc1	srdce
a	a	k8xC	a
krevní	krevní	k2eAgInSc1d1	krevní
oběh	oběh	k1gInSc1	oběh
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Srdeční	srdeční	k2eAgFnSc2d1	srdeční
choroby	choroba	k1gFnSc2	choroba
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
eMedicine	eMedicinout	k5eAaPmIp3nS	eMedicinout
<g/>
:	:	kIx,	:
Anatomie	anatomie	k1gFnSc1	anatomie
srdce	srdce	k1gNnSc2	srdce
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Portál	portál	k1gInSc1	portál
srdce	srdce	k1gNnSc2	srdce
</s>
</p>
