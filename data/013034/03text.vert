<p>
<s>
Tatra	Tatra	k1gFnSc1	Tatra
T5B6	T5B6	k1gFnSc2	T5B6
je	být	k5eAaImIp3nS	být
československá	československý	k2eAgFnSc1d1	Československá
tramvaj	tramvaj	k1gFnSc1	tramvaj
vyrobená	vyrobený	k2eAgFnSc1d1	vyrobená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
dvou	dva	k4xCgInPc2	dva
kusů	kus	k1gInPc2	kus
podnikem	podnik	k1gInSc7	podnik
ČKD	ČKD	kA	ČKD
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
závod	závod	k1gInSc1	závod
Tatra	Tatra	k1gFnSc1	Tatra
Smíchov	Smíchov	k1gInSc1	Smíchov
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
typ	typ	k1gInSc1	typ
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
původně	původně	k6eAd1	původně
určen	určit	k5eAaPmNgInS	určit
jako	jako	k9	jako
nástupce	nástupce	k1gMnSc4	nástupce
vozů	vůz	k1gInPc2	vůz
T3SU	T3SU	k1gFnPc2	T3SU
exportovaných	exportovaný	k2eAgInPc2d1	exportovaný
do	do	k7c2	do
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
,	,	kIx,	,
ve	v	k7c6	v
zkušebním	zkušební	k2eAgInSc6d1	zkušební
provozu	provoz	k1gInSc6	provoz
se	se	k3xPyFc4	se
ale	ale	k9	ale
neosvědčil	osvědčit	k5eNaPmAgInS	osvědčit
a	a	k8xC	a
oba	dva	k4xCgInPc1	dva
prototypy	prototyp	k1gInPc1	prototyp
nakonec	nakonec	k6eAd1	nakonec
skončily	skončit	k5eAaPmAgInP	skončit
v	v	k7c6	v
Mostě	most	k1gInSc6	most
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
tramvají	tramvaj	k1gFnPc2	tramvaj
zachována	zachován	k2eAgFnSc1d1	zachována
jako	jako	k8xS	jako
historická	historický	k2eAgFnSc1d1	historická
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc1	druhý
vůz	vůz	k1gInSc1	vůz
byl	být	k5eAaImAgInS	být
rozebrán	rozebrat	k5eAaPmNgInS	rozebrat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historické	historický	k2eAgNnSc4d1	historické
pozadí	pozadí	k1gNnSc4	pozadí
==	==	k?	==
</s>
</p>
<p>
<s>
Prototyp	prototyp	k1gInSc1	prototyp
"	"	kIx"	"
<g/>
klasického	klasický	k2eAgInSc2d1	klasický
<g/>
"	"	kIx"	"
typu	typ	k1gInSc2	typ
Tatra	Tatra	k1gFnSc1	Tatra
T3	T3	k1gFnSc1	T3
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1961	[number]	k4	1961
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
výrobu	výroba	k1gFnSc4	výroba
navázala	navázat	k5eAaPmAgFnS	navázat
sériová	sériový	k2eAgFnSc1d1	sériová
produkce	produkce	k1gFnSc1	produkce
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
začaly	začít	k5eAaPmAgInP	začít
být	být	k5eAaImF	být
vozy	vůz	k1gInPc1	vůz
v	v	k7c6	v
exportním	exportní	k2eAgNnSc6d1	exportní
provedení	provedení	k1gNnSc6	provedení
T3SU	T3SU	k1gFnSc2	T3SU
dodávány	dodávat	k5eAaImNgFnP	dodávat
i	i	k8xC	i
do	do	k7c2	do
měst	město	k1gNnPc2	město
tehdejšího	tehdejší	k2eAgInSc2d1	tehdejší
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
nástupce	nástupce	k1gMnSc1	nástupce
typu	typ	k1gInSc2	typ
T3	T3	k1gMnSc1	T3
vznikal	vznikat	k5eAaImAgMnS	vznikat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
projekt	projekt	k1gInSc1	projekt
nového	nový	k2eAgInSc2d1	nový
modelu	model	k1gInSc2	model
T	T	kA	T
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
ale	ale	k9	ale
byl	být	k5eAaImAgInS	být
poznamenán	poznamenat	k5eAaPmNgInS	poznamenat
odlišnými	odlišný	k2eAgInPc7d1	odlišný
názory	názor	k1gInPc7	názor
konstruktérů	konstruktér	k1gMnPc2	konstruktér
<g/>
,	,	kIx,	,
výtvarníků	výtvarník	k1gMnPc2	výtvarník
a	a	k8xC	a
vedení	vedení	k1gNnSc2	vedení
podniku	podnik	k1gInSc2	podnik
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dalších	další	k2eAgFnPc6d1	další
úpravách	úprava	k1gFnPc6	úprava
a	a	k8xC	a
vylepšeních	vylepšení	k1gNnPc6	vylepšení
došlo	dojít	k5eAaPmAgNnS	dojít
nakonec	nakonec	k6eAd1	nakonec
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
dvou	dva	k4xCgNnPc2	dva
prototypů	prototyp	k1gInPc2	prototyp
tramvaje	tramvaj	k1gFnSc2	tramvaj
T	T	kA	T
<g/>
5	[number]	k4	5
<g/>
B	B	kA	B
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
podle	podle	k7c2	podle
plánů	plán	k1gInPc2	plán
určena	určen	k2eAgFnSc1d1	určena
na	na	k7c4	na
export	export	k1gInSc4	export
do	do	k7c2	do
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Konstrukce	konstrukce	k1gFnSc2	konstrukce
==	==	k?	==
</s>
</p>
<p>
<s>
Tatra	Tatra	k1gFnSc1	Tatra
T5B6	T5B6	k1gFnSc2	T5B6
je	být	k5eAaImIp3nS	být
standardní	standardní	k2eAgInSc1d1	standardní
čtyřnápravový	čtyřnápravový	k2eAgInSc1d1	čtyřnápravový
tramvajový	tramvajový	k2eAgInSc1d1	tramvajový
jednosměrný	jednosměrný	k2eAgInSc1d1	jednosměrný
vůz	vůz	k1gInSc1	vůz
<g/>
.	.	kIx.	.
</s>
<s>
Vozová	vozový	k2eAgFnSc1d1	vozová
skříň	skříň	k1gFnSc1	skříň
je	být	k5eAaImIp3nS	být
usazena	usadit	k5eAaPmNgFnS	usadit
na	na	k7c6	na
dvou	dva	k4xCgInPc6	dva
dvounápravových	dvounápravový	k2eAgInPc6d1	dvounápravový
podvozcích	podvozek	k1gInPc6	podvozek
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
všechny	všechen	k3xTgFnPc1	všechen
nápravy	náprava	k1gFnPc1	náprava
jsou	být	k5eAaImIp3nP	být
hnací	hnací	k2eAgFnPc1d1	hnací
<g/>
.	.	kIx.	.
</s>
<s>
Každou	každý	k3xTgFnSc4	každý
nápravu	náprava	k1gFnSc4	náprava
pohání	pohánět	k5eAaImIp3nS	pohánět
jeden	jeden	k4xCgInSc1	jeden
kompaudní	kompaudní	k2eAgInSc1d1	kompaudní
trakční	trakční	k2eAgInSc1d1	trakční
motor	motor	k1gInSc1	motor
TE	TE	kA	TE
023	[number]	k4	023
A1	A1	k1gFnPc1	A1
s	s	k7c7	s
výkonem	výkon	k1gInSc7	výkon
77	[number]	k4	77
kW	kW	kA	kW
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
jiného	jiný	k2eAgInSc2d1	jiný
zdroje	zdroj	k1gInSc2	zdroj
motor	motor	k1gInSc1	motor
TE	TE	kA	TE
022	[number]	k4	022
H	H	kA	H
o	o	k7c6	o
výkonu	výkon	k1gInSc6	výkon
44,5	[number]	k4	44,5
kW	kW	kA	kW
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dva	dva	k4xCgInPc4	dva
motory	motor	k1gInPc4	motor
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
podvozku	podvozek	k1gInSc6	podvozek
jsou	být	k5eAaImIp3nP	být
spojeny	spojit	k5eAaPmNgInP	spojit
paralelně	paralelně	k6eAd1	paralelně
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
obě	dva	k4xCgFnPc1	dva
motorové	motorový	k2eAgFnPc1d1	motorová
skupiny	skupina	k1gFnPc1	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Motory	motor	k1gInPc1	motor
jsou	být	k5eAaImIp3nP	být
napájeny	napájet	k5eAaImNgInP	napájet
ze	z	k7c2	z
samostatného	samostatný	k2eAgInSc2d1	samostatný
dvoufázového	dvoufázový	k2eAgInSc2d1	dvoufázový
pulzního	pulzní	k2eAgInSc2d1	pulzní
měniče	měnič	k1gInSc2	měnič
<g/>
.	.	kIx.	.
</s>
<s>
Tramvaj	tramvaj	k1gFnSc1	tramvaj
je	být	k5eAaImIp3nS	být
vybavena	vybavit	k5eAaPmNgFnS	vybavit
tyristorovou	tyristorový	k2eAgFnSc7d1	tyristorová
elektrickou	elektrický	k2eAgFnSc7d1	elektrická
výzbrojí	výzbroj	k1gFnSc7	výzbroj
typu	typ	k1gInSc2	typ
TV	TV	kA	TV
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
rekuperační	rekuperační	k2eAgNnSc4d1	rekuperační
brzdění	brzdění	k1gNnSc4	brzdění
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
doplněno	doplnit	k5eAaPmNgNnS	doplnit
pulzně	pulzna	k1gFnSc3	pulzna
řízenou	řízený	k2eAgFnSc7d1	řízená
záskokovou	záskokový	k2eAgFnSc7d1	záskoková
odporovou	odporový	k2eAgFnSc7d1	odporová
brzdou	brzda	k1gFnSc7	brzda
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInPc1	jejíž
odporníky	odporník	k1gInPc1	odporník
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
na	na	k7c6	na
střeše	střecha	k1gFnSc6	střecha
vozidla	vozidlo	k1gNnSc2	vozidlo
<g/>
.	.	kIx.	.
</s>
<s>
Proud	proud	k1gInSc1	proud
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
trolejového	trolejový	k2eAgNnSc2d1	trolejové
vedení	vedení	k1gNnSc2	vedení
odebírán	odebírán	k2eAgInSc1d1	odebírán
běžným	běžný	k2eAgInSc7d1	běžný
pantografem	pantograf	k1gInSc7	pantograf
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vozová	vozový	k2eAgFnSc1d1	vozová
skříň	skříň	k1gFnSc1	skříň
má	mít	k5eAaImIp3nS	mít
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
svého	svůj	k3xOyFgNnSc2	svůj
předchůdce	předchůdce	k1gMnSc1	předchůdce
T	T	kA	T
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
hranaté	hranatý	k2eAgInPc4d1	hranatý
tvary	tvar	k1gInPc4	tvar
<g/>
,	,	kIx,	,
design	design	k1gInSc4	design
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
akademický	akademický	k2eAgMnSc1d1	akademický
sochař	sochař	k1gMnSc1	sochař
Ivan	Ivan	k1gMnSc1	Ivan
Linhart	Linhart	k1gMnSc1	Linhart
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pravé	pravý	k2eAgFnSc6d1	pravá
bočnici	bočnice	k1gFnSc6	bočnice
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
troje	troje	k4xRgFnPc4	troje
čtyřkřídlé	čtyřkřídlý	k2eAgFnPc4d1	čtyřkřídlá
skládací	skládací	k2eAgFnPc4d1	skládací
dveře	dveře	k1gFnPc4	dveře
<g/>
.	.	kIx.	.
</s>
<s>
Řidičova	řidičův	k2eAgFnSc1d1	Řidičova
kabina	kabina	k1gFnSc1	kabina
je	být	k5eAaImIp3nS	být
uzavřená	uzavřený	k2eAgFnSc1d1	uzavřená
a	a	k8xC	a
oddělená	oddělený	k2eAgFnSc1d1	oddělená
od	od	k7c2	od
salónu	salón	k1gInSc2	salón
pro	pro	k7c4	pro
cestující	cestující	k1gMnPc4	cestující
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něm	on	k3xPp3gInSc6	on
jsou	být	k5eAaImIp3nP	být
koženkové	koženkový	k2eAgFnPc1d1	koženková
sedačky	sedačka	k1gFnPc1	sedačka
rozmístěny	rozmístit	k5eAaPmNgFnP	rozmístit
v	v	k7c4	v
uspořádání	uspořádání	k1gNnSc4	uspořádání
1	[number]	k4	1
<g/>
+	+	kIx~	+
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dodávky	dodávka	k1gFnPc1	dodávka
a	a	k8xC	a
provoz	provoz	k1gInSc1	provoz
tramvají	tramvaj	k1gFnPc2	tramvaj
Tatra	Tatra	k1gFnSc1	Tatra
T5B6	T5B6	k1gFnSc1	T5B6
==	==	k?	==
</s>
</p>
<p>
<s>
Dva	dva	k4xCgInPc4	dva
prototypy	prototyp	k1gInPc4	prototyp
tramvaje	tramvaj	k1gFnSc2	tramvaj
T5B6	T5B6	k1gFnSc2	T5B6
(	(	kIx(	(
<g/>
zároveň	zároveň	k6eAd1	zároveň
také	také	k6eAd1	také
jediné	jediný	k2eAgInPc1d1	jediný
dva	dva	k4xCgInPc1	dva
vyrobené	vyrobený	k2eAgInPc1d1	vyrobený
vozy	vůz	k1gInPc1	vůz
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgFnP	být
dokončeny	dokončit	k5eAaPmNgFnP	dokončit
roku	rok	k1gInSc3	rok
1976	[number]	k4	1976
<g/>
.	.	kIx.	.
</s>
<s>
Označeny	označen	k2eAgFnPc1d1	označena
byly	být	k5eAaImAgFnP	být
evidenčními	evidenční	k2eAgNnPc7d1	evidenční
čísly	číslo	k1gNnPc7	číslo
výrobce	výrobce	k1gMnSc1	výrobce
8009	[number]	k4	8009
a	a	k8xC	a
8010	[number]	k4	8010
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
zapadaly	zapadat	k5eAaImAgFnP	zapadat
do	do	k7c2	do
řady	řada	k1gFnSc2	řada
prototypů	prototyp	k1gInPc2	prototyp
<g/>
.	.	kIx.	.
</s>
<s>
Tramvaje	tramvaj	k1gFnPc1	tramvaj
byly	být	k5eAaImAgFnP	být
zpočátku	zpočátku	k6eAd1	zpočátku
zkoušeny	zkoušet	k5eAaImNgFnP	zkoušet
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
širší	široký	k2eAgFnSc3d2	širší
skříni	skříň	k1gFnSc3	skříň
(	(	kIx(	(
<g/>
2	[number]	k4	2
600	[number]	k4	600
mm	mm	kA	mm
oproti	oproti	k7c3	oproti
běžným	běžný	k2eAgFnPc3d1	běžná
2	[number]	k4	2
500	[number]	k4	500
mm	mm	kA	mm
<g/>
)	)	kIx)	)
ale	ale	k8xC	ale
nemohly	moct	k5eNaImAgFnP	moct
být	být	k5eAaImF	být
zkoušky	zkouška	k1gFnPc1	zkouška
zcela	zcela	k6eAd1	zcela
dokončeny	dokončit	k5eAaPmNgFnP	dokončit
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
byly	být	k5eAaImAgInP	být
přepraveny	přepravit	k5eAaPmNgInP	přepravit
na	na	k7c4	na
mostecko-litvínovskou	mosteckoitvínovský	k2eAgFnSc4d1	mostecko-litvínovský
rychlodráhu	rychlodráha	k1gFnSc4	rychlodráha
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
článek	článek	k1gInSc1	článek
Tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
Mostě	most	k1gInSc6	most
a	a	k8xC	a
v	v	k7c6	v
Litvínově	Litvínov	k1gInSc6	Litvínov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zkušební	zkušební	k2eAgFnSc2d1	zkušební
jízdy	jízda	k1gFnSc2	jízda
dále	daleko	k6eAd2	daleko
pokračovaly	pokračovat	k5eAaImAgFnP	pokračovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
byly	být	k5eAaImAgInP	být
oba	dva	k4xCgInPc1	dva
prototypy	prototyp	k1gInPc1	prototyp
zapůjčeny	zapůjčen	k2eAgInPc1d1	zapůjčen
do	do	k7c2	do
Tveru	Tver	k1gInSc2	Tver
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
Kalinin	Kalinin	k2eAgInSc1d1	Kalinin
<g/>
)	)	kIx)	)
v	v	k7c6	v
dnešním	dnešní	k2eAgNnSc6d1	dnešní
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
ale	ale	k9	ale
neosvědčily	osvědčit	k5eNaPmAgFnP	osvědčit
a	a	k8xC	a
proto	proto	k6eAd1	proto
byly	být	k5eAaImAgFnP	být
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
vráceny	vrátit	k5eAaPmNgInP	vrátit
zpět	zpět	k6eAd1	zpět
výrobci	výrobce	k1gMnPc1	výrobce
<g/>
.	.	kIx.	.
<g/>
Zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
jejich	jejich	k3xOp3gInSc4	jejich
provoz	provoz	k1gInSc4	provoz
ale	ale	k8xC	ale
měl	mít	k5eAaImAgInS	mít
Dopravní	dopravní	k2eAgInSc1d1	dopravní
podnik	podnik	k1gInSc1	podnik
měst	město	k1gNnPc2	město
Mostu	most	k1gInSc2	most
a	a	k8xC	a
Litvínova	Litvínov	k1gInSc2	Litvínov
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
byl	být	k5eAaImAgInS	být
s	s	k7c7	s
tramvajemi	tramvaj	k1gFnPc7	tramvaj
při	při	k7c6	při
zkušebních	zkušební	k2eAgFnPc6d1	zkušební
jízdách	jízda	k1gFnPc6	jízda
spokojen	spokojen	k2eAgInSc1d1	spokojen
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
vozy	vůz	k1gInPc1	vůz
proto	proto	k8xC	proto
ještě	ještě	k9	ještě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
odkoupil	odkoupit	k5eAaPmAgInS	odkoupit
a	a	k8xC	a
počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
je	být	k5eAaImIp3nS	být
zařadil	zařadit	k5eAaPmAgInS	zařadit
do	do	k7c2	do
běžného	běžný	k2eAgInSc2d1	běžný
provozu	provoz	k1gInSc2	provoz
s	s	k7c7	s
vlastními	vlastní	k2eAgInPc7d1	vlastní
evidenčními	evidenční	k2eAgInPc7d1	evidenční
čísly	čísnout	k5eAaPmAgFnP	čísnout
272	[number]	k4	272
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
č.	č.	k?	č.
8009	[number]	k4	8009
<g/>
)	)	kIx)	)
a	a	k8xC	a
273	[number]	k4	273
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
č.	č.	k?	č.
8010	[number]	k4	8010
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jediným	jediný	k2eAgInSc7d1	jediný
negativem	negativ	k1gInSc7	negativ
na	na	k7c6	na
jejich	jejich	k3xOp3gInSc6	jejich
provozu	provoz	k1gInSc6	provoz
byla	být	k5eAaImAgNnP	být
bezpečnostní	bezpečnostní	k2eAgNnPc1d1	bezpečnostní
rizika	riziko	k1gNnPc1	riziko
při	při	k7c6	při
míjení	míjení	k1gNnSc6	míjení
na	na	k7c6	na
meziměstské	meziměstský	k2eAgFnSc6d1	meziměstská
trati	trať	k1gFnSc6	trať
Most	most	k1gInSc1	most
–	–	k?	–
Litvínov	Litvínov	k1gInSc1	Litvínov
<g/>
,	,	kIx,	,
tramvaje	tramvaj	k1gFnPc1	tramvaj
T5B6	T5B6	k1gMnPc2	T5B6
tedy	tedy	k8xC	tedy
byly	být	k5eAaImAgInP	být
vypravovány	vypravovat	k5eAaImNgInP	vypravovat
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
mosteckou	mostecký	k2eAgFnSc4d1	Mostecká
městskou	městský	k2eAgFnSc4d1	městská
linku	linka	k1gFnSc4	linka
č.	č.	k?	č.
2	[number]	k4	2
mezi	mezi	k7c7	mezi
hlavním	hlavní	k2eAgNnSc7d1	hlavní
nádražím	nádraží	k1gNnSc7	nádraží
a	a	k8xC	a
Velebudicemi	Velebudice	k1gFnPc7	Velebudice
<g/>
.	.	kIx.	.
<g/>
Provoz	provoz	k1gInSc1	provoz
vozu	vůz	k1gInSc2	vůz
č.	č.	k?	č.
272	[number]	k4	272
byl	být	k5eAaImAgInS	být
ukončen	ukončen	k2eAgInSc1d1	ukončen
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
byl	být	k5eAaImAgInS	být
využit	využít	k5eAaPmNgInS	využít
jako	jako	k9	jako
zdroj	zdroj	k1gInSc1	zdroj
náhradních	náhradní	k2eAgInPc2d1	náhradní
dílů	díl	k1gInPc2	díl
pro	pro	k7c4	pro
druhou	druhý	k4xOgFnSc4	druhý
tramvaj	tramvaj	k1gFnSc4	tramvaj
a	a	k8xC	a
poté	poté	k6eAd1	poté
byl	být	k5eAaImAgInS	být
sešrotován	sešrotován	k2eAgInSc1d1	sešrotován
<g/>
.	.	kIx.	.
</s>
<s>
Vůz	vůz	k1gInSc1	vůz
č.	č.	k?	č.
273	[number]	k4	273
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
atypičnosti	atypičnost	k1gFnSc3	atypičnost
a	a	k8xC	a
historické	historický	k2eAgFnSc3d1	historická
hodnotě	hodnota	k1gFnSc3	hodnota
zůstal	zůstat	k5eAaPmAgInS	zůstat
zachován	zachovat	k5eAaPmNgInS	zachovat
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
nejmladší	mladý	k2eAgFnSc1d3	nejmladší
historická	historický	k2eAgFnSc1d1	historická
tramvaj	tramvaj	k1gFnSc1	tramvaj
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
je	být	k5eAaImIp3nS	být
vůz	vůz	k1gInSc1	vůz
využíván	využívat	k5eAaImNgInS	využívat
při	při	k7c6	při
různých	různý	k2eAgNnPc6d1	různé
výročích	výročí	k1gNnPc6	výročí
MHD	MHD	kA	MHD
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
pro	pro	k7c4	pro
objednané	objednaný	k2eAgFnPc4d1	objednaná
příležitostné	příležitostný	k2eAgFnPc4d1	příležitostná
jízdy	jízda	k1gFnPc4	jízda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
tramvaj	tramvaj	k1gFnSc1	tramvaj
podstoupila	podstoupit	k5eAaPmAgFnS	podstoupit
střední	střední	k2eAgFnSc4d1	střední
prohlídku	prohlídka	k1gFnSc4	prohlídka
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yRnSc3	což
získala	získat	k5eAaPmAgFnS	získat
trvalý	trvalý	k2eAgInSc4d1	trvalý
průkaz	průkaz	k1gInSc4	průkaz
způsobilosti	způsobilost	k1gFnSc2	způsobilost
historického	historický	k2eAgNnSc2d1	historické
drážního	drážní	k2eAgNnSc2d1	drážní
vozidla	vozidlo	k1gNnSc2	vozidlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historické	historický	k2eAgInPc1d1	historický
vozy	vůz	k1gInPc1	vůz
==	==	k?	==
</s>
</p>
<p>
<s>
Most	most	k1gInSc1	most
a	a	k8xC	a
Litvínov	Litvínov	k1gInSc1	Litvínov
(	(	kIx(	(
<g/>
vůz	vůz	k1gInSc1	vůz
ev.	ev.	k?	ev.
č.	č.	k?	č.
273	[number]	k4	273
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Tatra	Tatra	k1gFnSc1	Tatra
T5B6	T5B6	k1gMnPc2	T5B6
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
30	[number]	k4	30
let	let	k1gInSc4	let
mostecké	mostecký	k2eAgFnSc2d1	Mostecká
tramvaje	tramvaj	k1gFnSc2	tramvaj
T	T	kA	T
<g/>
5	[number]	k4	5
<g/>
B	B	kA	B
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
prazsketramvaje	prazsketramvat	k5eAaImSgMnS	prazsketramvat
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
