<s>
Charles	Charles	k1gMnSc1	Charles
Bukowski	Bukowsk	k1gFnSc2	Bukowsk
(	(	kIx(	(
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1920	[number]	k4	1920
<g/>
,	,	kIx,	,
Andernach	Andernach	k1gInSc1	Andernach
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
–	–	k?	–
9	[number]	k4	9
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
San	San	k1gFnPc1	San
Pedro	Pedro	k1gNnSc1	Pedro
<g/>
,	,	kIx,	,
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
celým	celý	k2eAgNnSc7d1	celé
jménem	jméno	k1gNnSc7	jméno
Henry	Henry	k1gMnSc1	Henry
Charles	Charles	k1gMnSc1	Charles
Bukowski	Bukowsk	k1gMnPc1	Bukowsk
<g/>
,	,	kIx,	,
přezdívaný	přezdívaný	k2eAgMnSc1d1	přezdívaný
Hank	Hank	k1gMnSc1	Hank
nebo	nebo	k8xC	nebo
Buk	buk	k1gInSc1	buk
<g/>
,	,	kIx,	,
původním	původní	k2eAgNnSc7d1	původní
německým	německý	k2eAgNnSc7d1	německé
jménem	jméno	k1gNnSc7	jméno
Heinrich	Heinrich	k1gMnSc1	Heinrich
Karl	Karl	k1gMnSc1	Karl
Bukowski	Bukowske	k1gFnSc4	Bukowske
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgMnS	proslavit
nekonvenčními	konvenční	k2eNgFnPc7d1	nekonvenční
autobiografickými	autobiografický	k2eAgFnPc7d1	autobiografická
prózami	próza	k1gFnPc7	próza
a	a	k8xC	a
vulgárním	vulgární	k2eAgInSc7d1	vulgární
stylem	styl	k1gInSc7	styl
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
svému	svůj	k3xOyFgInSc3	svůj
stylu	styl	k1gInSc3	styl
bývá	bývat	k5eAaImIp3nS	bývat
často	často	k6eAd1	často
přiřazován	přiřazovat	k5eAaImNgInS	přiřazovat
ke	k	k7c3	k
spisovatelům	spisovatel	k1gMnPc3	spisovatel
poněkud	poněkud	k6eAd1	poněkud
vágní	vágní	k2eAgFnSc2d1	vágní
skupiny	skupina	k1gFnSc2	skupina
beat	beat	k1gInSc1	beat
generation	generation	k1gInSc1	generation
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
patří	patřit	k5eAaImIp3nS	patřit
zejména	zejména	k9	zejména
Jack	Jack	k1gMnSc1	Jack
Kerouac	Kerouac	k1gInSc1	Kerouac
nebo	nebo	k8xC	nebo
John	John	k1gMnSc1	John
Clellon	Clellon	k1gInSc4	Clellon
Holmes	Holmesa	k1gFnPc2	Holmesa
<g/>
.	.	kIx.	.
</s>
<s>
Osobně	osobně	k6eAd1	osobně
se	se	k3xPyFc4	se
však	však	k9	však
tomuto	tento	k3xDgNnSc3	tento
zařazení	zařazení	k1gNnSc3	zařazení
vždy	vždy	k6eAd1	vždy
velmi	velmi	k6eAd1	velmi
silně	silně	k6eAd1	silně
bránil	bránit	k5eAaImAgMnS	bránit
<g/>
.	.	kIx.	.
</s>
<s>
Historky	historka	k1gFnPc1	historka
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
nechtěným	chtěný	k2eNgNnSc7d1	nechtěné
nemanželským	manželský	k2eNgNnSc7d1	nemanželské
dítětem	dítě	k1gNnSc7	dítě
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
o	o	k7c6	o
sobě	se	k3xPyFc3	se
sám	sám	k3xTgMnSc1	sám
šířil	šířit	k5eAaImAgMnS	šířit
<g/>
,	,	kIx,	,
nejsou	být	k5eNaImIp3nP	být
pravdivé	pravdivý	k2eAgFnPc1d1	pravdivá
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	s	k7c7	s
16	[number]	k4	16
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
1920	[number]	k4	1920
v	v	k7c6	v
německém	německý	k2eAgInSc6d1	německý
Andernachu	Andernach	k1gInSc6	Andernach
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
Heinrich	Heinrich	k1gMnSc1	Heinrich
Bukowski	Bukowske	k1gFnSc4	Bukowske
tam	tam	k6eAd1	tam
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
sloužil	sloužit	k5eAaImAgInS	sloužit
v	v	k7c6	v
místní	místní	k2eAgFnSc6d1	místní
posádce	posádka	k1gFnSc6	posádka
americké	americký	k2eAgFnSc2d1	americká
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
zamiloval	zamilovat	k5eAaPmAgMnS	zamilovat
se	se	k3xPyFc4	se
do	do	k7c2	do
osmnáctileté	osmnáctiletý	k2eAgFnSc2d1	osmnáctiletá
švadleny	švadlena	k1gFnSc2	švadlena
Kathariny	Katharin	k1gInPc1	Katharin
Fett	Fetta	k1gFnPc2	Fetta
a	a	k8xC	a
vzal	vzít	k5eAaPmAgMnS	vzít
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
se	se	k3xPyFc4	se
rodina	rodina	k1gFnSc1	rodina
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
do	do	k7c2	do
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Charles	Charles	k1gMnSc1	Charles
Bukowski	Bukowsk	k1gFnSc2	Bukowsk
strávil	strávit	k5eAaPmAgMnS	strávit
bezmála	bezmála	k6eAd1	bezmála
50	[number]	k4	50
let	léto	k1gNnPc2	léto
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgMnSc1d1	český
překladatel	překladatel	k1gMnSc1	překladatel
jeho	jeho	k3xOp3gNnSc2	jeho
díla	dílo	k1gNnSc2	dílo
Robert	Robert	k1gMnSc1	Robert
Hýsek	Hýsek	k1gMnSc1	Hýsek
v	v	k7c6	v
pořadu	pořad	k1gInSc6	pořad
Schůzky	schůzka	k1gFnSc2	schůzka
s	s	k7c7	s
literaturou	literatura	k1gFnSc7	literatura
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
vysílala	vysílat	k5eAaImAgFnS	vysílat
stanice	stanice	k1gFnSc1	stanice
ČRo	ČRo	k1gFnSc1	ČRo
Vltava	Vltava	k1gFnSc1	Vltava
v	v	k7c6	v
neděli	neděle	k1gFnSc6	neděle
15	[number]	k4	15
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2016	[number]	k4	2016
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Charles	Charles	k1gMnSc1	Charles
Bukowski	Bukowsk	k1gFnSc2	Bukowsk
<g/>
,	,	kIx,	,
starý	starý	k2eAgMnSc1d1	starý
chachar	chachar	k1gMnSc1	chachar
ostravski	ostravsk	k1gFnSc2	ostravsk
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Bukowski	Bukowski	k1gNnSc2	Bukowski
narodil	narodit	k5eAaPmAgMnS	narodit
ve	v	k7c6	v
vesnici	vesnice	k1gFnSc6	vesnice
Pazderna	pazderna	k1gFnSc1	pazderna
na	na	k7c6	na
Frýdecko-Místecku	Frýdecko-Místecko	k1gNnSc6	Frýdecko-Místecko
a	a	k8xC	a
po	po	k7c6	po
emigraci	emigrace	k1gFnSc6	emigrace
do	do	k7c2	do
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
svou	svůj	k3xOyFgFnSc4	svůj
skutečnou	skutečný	k2eAgFnSc4d1	skutečná
identitu	identita	k1gFnSc4	identita
úspěšně	úspěšně	k6eAd1	úspěšně
zatajil	zatajit	k5eAaPmAgMnS	zatajit
<g/>
.	.	kIx.	.
</s>
<s>
Nemluvil	mluvit	k5eNaImAgMnS	mluvit
dobře	dobře	k6eAd1	dobře
anglicky	anglicky	k6eAd1	anglicky
a	a	k8xC	a
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
německý	německý	k2eAgInSc4d1	německý
původ	původ	k1gInSc4	původ
byl	být	k5eAaImAgInS	být
často	často	k6eAd1	často
terčem	terč	k1gInSc7	terč
posměchu	posměch	k1gInSc2	posměch
ostatních	ostatní	k2eAgFnPc2d1	ostatní
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
trávil	trávit	k5eAaImAgMnS	trávit
čas	čas	k1gInSc4	čas
většinou	většinou	k6eAd1	většinou
o	o	k7c6	o
samotě	samota	k1gFnSc6	samota
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
izolaci	izolace	k1gFnSc3	izolace
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
knihovnu	knihovna	k1gFnSc4	knihovna
a	a	k8xC	a
hodně	hodně	k6eAd1	hodně
četl	číst	k5eAaImAgMnS	číst
<g/>
.	.	kIx.	.
</s>
<s>
John	John	k1gMnSc1	John
Fante	fant	k1gInSc5	fant
<g/>
,	,	kIx,	,
Sherwood	Sherwood	k1gInSc4	Sherwood
Anderson	Anderson	k1gMnSc1	Anderson
<g/>
,	,	kIx,	,
Ernest	Ernest	k1gMnSc1	Ernest
Hemingway	Hemingwaa	k1gFnSc2	Hemingwaa
<g/>
,	,	kIx,	,
Robinson	Robinson	k1gMnSc1	Robinson
Jeffers	Jeffers	k1gInSc1	Jeffers
<g/>
,	,	kIx,	,
Edward	Edward	k1gMnSc1	Edward
Estlin	Estlina	k1gFnPc2	Estlina
Cummings	Cummings	k1gInSc1	Cummings
<g/>
,	,	kIx,	,
James	James	k1gMnSc1	James
Thurber	Thurber	k1gMnSc1	Thurber
<g/>
,	,	kIx,	,
Ezra	Ezra	k1gMnSc1	Ezra
Pound	pound	k1gInSc1	pound
<g/>
,	,	kIx,	,
D.	D.	kA	D.
H.	H.	kA	H.
Lawrence	Lawrence	k1gFnSc1	Lawrence
<g/>
,	,	kIx,	,
Fjodor	Fjodor	k1gMnSc1	Fjodor
Michajlovič	Michajlovič	k1gMnSc1	Michajlovič
Dostojevskij	Dostojevskij	k1gMnSc1	Dostojevskij
<g/>
,	,	kIx,	,
Ivan	Ivan	k1gMnSc1	Ivan
Sergejevič	Sergejevič	k1gMnSc1	Sergejevič
Turgeněv	Turgeněv	k1gMnSc1	Turgeněv
<g/>
,	,	kIx,	,
Louis-Ferdinand	Louis-Ferdinand	k1gInSc1	Louis-Ferdinand
Céline	Célin	k1gInSc5	Célin
<g/>
,	,	kIx,	,
Antonin	Antonin	k1gInSc4	Antonin
Artaud	Artauda	k1gFnPc2	Artauda
<g/>
,	,	kIx,	,
Knut	knuta	k1gFnPc2	knuta
Hamsun	Hamsuna	k1gFnPc2	Hamsuna
<g/>
,	,	kIx,	,
Franz	Franz	k1gMnSc1	Franz
Kafka	Kafka	k1gMnSc1	Kafka
<g/>
,	,	kIx,	,
Albert	Albert	k1gMnSc1	Albert
Camus	Camus	k1gMnSc1	Camus
a	a	k8xC	a
další	další	k2eAgFnSc1d1	další
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc4	ten
byli	být	k5eAaImAgMnP	být
autoři	autor	k1gMnPc1	autor
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
jej	on	k3xPp3gMnSc4	on
významně	významně	k6eAd1	významně
ovlivnili	ovlivnit	k5eAaPmAgMnP	ovlivnit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
na	na	k7c4	na
vyšší	vysoký	k2eAgFnSc4d2	vyšší
střední	střední	k2eAgFnSc4d1	střední
školu	škola	k1gFnSc4	škola
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
chodili	chodit	k5eAaImAgMnP	chodit
potomci	potomek	k1gMnPc1	potomek
rodičů	rodič	k1gMnPc2	rodič
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
netrpěli	trpět	k5eNaImAgMnP	trpět
finanční	finanční	k2eAgFnSc7d1	finanční
nouzí	nouze	k1gFnSc7	nouze
<g/>
,	,	kIx,	,
i	i	k8xC	i
proto	proto	k8xC	proto
špatně	špatně	k6eAd1	špatně
nesl	nést	k5eAaImAgMnS	nést
svou	svůj	k3xOyFgFnSc4	svůj
chudobu	chudoba	k1gFnSc4	chudoba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
probíhala	probíhat	k5eAaImAgFnS	probíhat
hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
krize	krize	k1gFnSc1	krize
<g/>
,	,	kIx,	,
odstartovaná	odstartovaný	k2eAgFnSc1d1	odstartovaná
pádem	pád	k1gInSc7	pád
burzy	burza	k1gFnSc2	burza
<g/>
.	.	kIx.	.
</s>
<s>
Respekt	respekt	k1gInSc4	respekt
si	se	k3xPyFc3	se
však	však	k9	však
dokázal	dokázat	k5eAaPmAgMnS	dokázat
částečně	částečně	k6eAd1	částečně
zjednat	zjednat	k5eAaPmF	zjednat
svými	svůj	k3xOyFgFnPc7	svůj
pěstmi	pěst	k1gFnPc7	pěst
a	a	k8xC	a
rvačky	rvačka	k1gFnPc1	rvačka
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
dál	daleko	k6eAd2	daleko
táhly	táhnout	k5eAaImAgInP	táhnout
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Navazuje	navazovat	k5eAaImIp3nS	navazovat
první	první	k4xOgNnSc4	první
přátelství	přátelství	k1gNnSc4	přátelství
s	s	k7c7	s
podobně	podobně	k6eAd1	podobně
sociálně	sociálně	k6eAd1	sociálně
vyloučenými	vyloučený	k2eAgMnPc7d1	vyloučený
chlapci	chlapec	k1gMnPc7	chlapec
a	a	k8xC	a
seznamuje	seznamovat	k5eAaImIp3nS	seznamovat
se	se	k3xPyFc4	se
s	s	k7c7	s
alkoholem	alkohol	k1gInSc7	alkohol
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc2	který
se	se	k3xPyFc4	se
již	již	k6eAd1	již
nikdy	nikdy	k6eAd1	nikdy
nevzdá	vzdát	k5eNaPmIp3nS	vzdát
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
dojem	dojem	k1gInSc4	dojem
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jediná	jediný	k2eAgFnSc1d1	jediná
věc	věc	k1gFnSc1	věc
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
dokáže	dokázat	k5eAaPmIp3nS	dokázat
alespoň	alespoň	k9	alespoň
trochu	trochu	k6eAd1	trochu
přebít	přebít	k5eAaPmF	přebít
každodenní	každodenní	k2eAgInSc4d1	každodenní
zmar	zmar	k1gInSc4	zmar
a	a	k8xC	a
zoufalství	zoufalství	k1gNnSc4	zoufalství
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
dívkami	dívka	k1gFnPc7	dívka
v	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
nehovořil	hovořit	k5eNaImAgMnS	hovořit
<g/>
,	,	kIx,	,
neuměl	umět	k5eNaImAgMnS	umět
dát	dát	k5eAaPmF	dát
záminku	záminka	k1gFnSc4	záminka
ke	k	k7c3	k
sblížení	sblížení	k1gNnSc3	sblížení
a	a	k8xC	a
přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
líbily	líbit	k5eAaImAgFnP	líbit
<g/>
,	,	kIx,	,
pohrdal	pohrdat	k5eAaImAgMnS	pohrdat
jimi	on	k3xPp3gMnPc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc4	jeho
izolaci	izolace	k1gFnSc4	izolace
napomohlo	napomoct	k5eAaPmAgNnS	napomoct
i	i	k9	i
ohyzdné	ohyzdný	k2eAgFnSc2d1	ohyzdná
akné	akné	k1gFnSc2	akné
(	(	kIx(	(
<g/>
acne	acnat	k5eAaPmIp3nS	acnat
vulgaris	vulgaris	k1gFnSc1	vulgaris
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
mu	on	k3xPp3gMnSc3	on
vyrazilo	vyrazit	k5eAaPmAgNnS	vyrazit
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
a	a	k8xC	a
trupu	trup	k1gInSc6	trup
<g/>
.	.	kIx.	.
</s>
<s>
Nebyly	být	k5eNaImAgFnP	být
to	ten	k3xDgNnSc1	ten
pouhé	pouhý	k2eAgFnPc1d1	pouhá
uhry	uhra	k1gFnPc1	uhra
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
celá	celý	k2eAgNnPc4d1	celé
ložiska	ložisko	k1gNnPc4	ložisko
obrovských	obrovský	k2eAgInPc2d1	obrovský
vředů	vřed	k1gInPc2	vřed
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
doma	doma	k6eAd1	doma
se	se	k3xPyFc4	se
necítil	cítit	k5eNaImAgMnS	cítit
dobře	dobře	k6eAd1	dobře
<g/>
:	:	kIx,	:
otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
přísný	přísný	k2eAgInSc4d1	přísný
a	a	k8xC	a
krutě	krutě	k6eAd1	krutě
ho	on	k3xPp3gMnSc4	on
trestal	trestat	k5eAaImAgMnS	trestat
za	za	k7c4	za
sebemenší	sebemenší	k2eAgFnSc4d1	sebemenší
maličkost	maličkost	k1gFnSc4	maličkost
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
se	se	k3xPyFc4	se
jej	on	k3xPp3gMnSc4	on
nedokázala	dokázat	k5eNaPmAgFnS	dokázat
před	před	k7c7	před
otcem	otec	k1gMnSc7	otec
zastat	zastat	k5eAaPmF	zastat
<g/>
.	.	kIx.	.
</s>
<s>
Mladý	mladý	k2eAgMnSc1d1	mladý
Charles	Charles	k1gMnSc1	Charles
autoritu	autorita	k1gFnSc4	autorita
rodičů	rodič	k1gMnPc2	rodič
nepřijímal	přijímat	k5eNaImAgInS	přijímat
<g/>
,	,	kIx,	,
naprosto	naprosto	k6eAd1	naprosto
k	k	k7c3	k
nim	on	k3xPp3gMnPc3	on
ztratil	ztratit	k5eAaPmAgMnS	ztratit
úctu	úcta	k1gFnSc4	úcta
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
je	být	k5eAaImIp3nS	být
nenávidět	nenávidět	k5eAaImF	nenávidět
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
přihlásil	přihlásit	k5eAaPmAgMnS	přihlásit
na	na	k7c4	na
vysokou	vysoký	k2eAgFnSc4d1	vysoká
školu	škola	k1gFnSc4	škola
-	-	kIx~	-
Los	los	k1gInSc1	los
Angeles	Angelesa	k1gFnPc2	Angelesa
City	City	k1gFnSc2	City
College	Colleg	k1gFnSc2	Colleg
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
2	[number]	k4	2
roky	rok	k1gInPc7	rok
studoval	studovat	k5eAaImAgMnS	studovat
literaturu	literatura	k1gFnSc4	literatura
a	a	k8xC	a
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgInS	mít
neurčitou	určitý	k2eNgFnSc4d1	neurčitá
představu	představa	k1gFnSc4	představa
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
stát	stát	k5eAaPmF	stát
novinářem	novinář	k1gMnSc7	novinář
<g/>
.	.	kIx.	.
</s>
<s>
Tady	tady	k6eAd1	tady
se	se	k3xPyFc4	se
-	-	kIx~	-
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
odlišit	odlišit	k5eAaPmF	odlišit
se	se	k3xPyFc4	se
od	od	k7c2	od
svých	svůj	k3xOyFgMnPc2	svůj
spolužáků	spolužák	k1gMnPc2	spolužák
-	-	kIx~	-
začal	začít	k5eAaPmAgInS	začít
angažovat	angažovat	k5eAaBmF	angažovat
pro	pro	k7c4	pro
Hitlera	Hitler	k1gMnSc4	Hitler
a	a	k8xC	a
fašismus	fašismus	k1gInSc4	fašismus
a	a	k8xC	a
zveřejňoval	zveřejňovat	k5eAaImAgMnS	zveřejňovat
své	svůj	k3xOyFgInPc4	svůj
extremistické	extremistický	k2eAgInPc4d1	extremistický
názory	názor	k1gInPc4	názor
<g/>
.	.	kIx.	.
</s>
<s>
Bukowského	Bukowského	k2eAgFnSc1d1	Bukowského
politika	politika	k1gFnSc1	politika
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
vůbec	vůbec	k9	vůbec
nezajímala	zajímat	k5eNaImAgFnS	zajímat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rád	rád	k6eAd1	rád
dělal	dělat	k5eAaImAgMnS	dělat
věci	věc	k1gFnPc4	věc
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
pohoršovaly	pohoršovat	k5eAaImAgFnP	pohoršovat
a	a	k8xC	a
šokovaly	šokovat	k5eAaBmAgFnP	šokovat
ostatní	ostatní	k2eAgFnPc1d1	ostatní
<g/>
.	.	kIx.	.
</s>
<s>
Považoval	považovat	k5eAaImAgMnS	považovat
se	se	k3xPyFc4	se
za	za	k7c4	za
Němce	Němec	k1gMnSc4	Němec
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
přirozenou	přirozený	k2eAgFnSc4d1	přirozená
loajalitu	loajalita	k1gFnSc4	loajalita
k	k	k7c3	k
vlasti	vlast	k1gFnSc3	vlast
a	a	k8xC	a
nelíbil	líbit	k5eNaImAgMnS	líbit
se	se	k3xPyFc4	se
mu	on	k3xPp3gNnSc3	on
médii	médium	k1gNnPc7	médium
vykreslovaný	vykreslovaný	k2eAgInSc4d1	vykreslovaný
obraz	obraz	k1gInSc4	obraz
a	a	k8xC	a
zesměšňování	zesměšňování	k1gNnSc4	zesměšňování
německého	německý	k2eAgInSc2d1	německý
národa	národ	k1gInSc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
také	také	k9	také
<g/>
:	:	kIx,	:
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
proamerický	proamerický	k2eAgMnSc1d1	proamerický
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnPc2	jeho
učitelé	učitel	k1gMnPc1	učitel
byli	být	k5eAaImAgMnP	být
protiněmečtí	protiněmecký	k2eAgMnPc1d1	protiněmecký
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
on	on	k3xPp3gMnSc1	on
byl	být	k5eAaImAgMnS	být
proněmecký	proněmecký	k2eAgMnSc1d1	proněmecký
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
otec	otec	k1gMnSc1	otec
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
psacím	psací	k2eAgInSc6d1	psací
stroji	stroj	k1gInSc6	stroj
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
mu	on	k3xPp3gMnSc3	on
koupil	koupit	k5eAaPmAgMnS	koupit
kvůli	kvůli	k7c3	kvůli
psaní	psaní	k1gNnSc3	psaní
prací	práce	k1gFnPc2	práce
do	do	k7c2	do
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
píše	psát	k5eAaImIp3nS	psát
kontroverzní	kontroverzní	k2eAgFnPc4d1	kontroverzní
povídky	povídka	k1gFnPc4	povídka
<g/>
,	,	kIx,	,
vyhodil	vyhodit	k5eAaPmAgMnS	vyhodit
mu	on	k3xPp3gMnSc3	on
je	být	k5eAaImIp3nS	být
i	i	k9	i
se	s	k7c7	s
strojem	stroj	k1gInSc7	stroj
a	a	k8xC	a
oblečením	oblečení	k1gNnSc7	oblečení
před	před	k7c4	před
dům	dům	k1gInSc4	dům
<g/>
.	.	kIx.	.
</s>
<s>
Bukowski	Bukowski	k6eAd1	Bukowski
si	se	k3xPyFc3	se
vzal	vzít	k5eAaPmAgMnS	vzít
nějaké	nějaký	k3yIgInPc4	nějaký
peníze	peníz	k1gInPc4	peníz
od	od	k7c2	od
matky	matka	k1gFnSc2	matka
a	a	k8xC	a
odstěhoval	odstěhovat	k5eAaPmAgInS	odstěhovat
se	se	k3xPyFc4	se
do	do	k7c2	do
podnájmu	podnájem	k1gInSc2	podnájem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
1941	[number]	k4	1941
skončil	skončit	k5eAaPmAgInS	skončit
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
školou	škola	k1gFnSc7	škola
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jednadvaceti	jednadvacet	k4xCc6	jednadvacet
odjel	odjet	k5eAaPmAgMnS	odjet
do	do	k7c2	do
New	New	k1gMnSc2	New
Orleans	Orleans	k1gInSc4	Orleans
<g/>
,	,	kIx,	,
následovala	následovat	k5eAaImAgFnS	následovat
Atlanta	Atlanta	k1gFnSc1	Atlanta
v	v	k7c6	v
Georgii	Georgie	k1gFnSc6	Georgie
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
San	San	k1gMnSc1	San
Francisco	Francisco	k1gMnSc1	Francisco
(	(	kIx(	(
<g/>
zde	zde	k6eAd1	zde
pracoval	pracovat	k5eAaImAgMnS	pracovat
několik	několik	k4yIc1	několik
týdnů	týden	k1gInPc2	týden
jako	jako	k8xS	jako
řidič	řidič	k1gMnSc1	řidič
Červeného	Červeného	k2eAgInSc2d1	Červeného
kříže	kříž	k1gInSc2	kříž
-	-	kIx~	-
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
mála	málo	k1gNnSc2	málo
přijatelných	přijatelný	k2eAgNnPc2d1	přijatelné
zaměstnání	zaměstnání	k1gNnPc2	zaměstnání
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc4	který
vykonával	vykonávat	k5eAaImAgMnS	vykonávat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Cítil	cítit	k5eAaImAgMnS	cítit
se	se	k3xPyFc4	se
být	být	k5eAaImF	být
vyděděným	vyděděný	k2eAgMnPc3d1	vyděděný
<g/>
,	,	kIx,	,
časem	časem	k6eAd1	časem
si	se	k3xPyFc3	se
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
pocit	pocit	k1gInSc4	pocit
zvykl	zvyknout	k5eAaPmAgMnS	zvyknout
a	a	k8xC	a
svým	svůj	k3xOyFgInSc7	svůj
způsobem	způsob	k1gInSc7	způsob
si	se	k3xPyFc3	se
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
začal	začít	k5eAaPmAgMnS	začít
libovat	libovat	k5eAaImF	libovat
<g/>
.	.	kIx.	.
22.6	[number]	k4	22.6
<g/>
.1944	.1944	k4	.1944
byl	být	k5eAaImAgMnS	být
Bukowski	Bukowsk	k1gMnSc3	Bukowsk
<g/>
,	,	kIx,	,
kterému	který	k3yRgMnSc3	který
bylo	být	k5eAaImAgNnS	být
24	[number]	k4	24
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Filadelfii	Filadelfie	k1gFnSc6	Filadelfie
zadržen	zadržet	k5eAaPmNgMnS	zadržet
FBI	FBI	kA	FBI
pro	pro	k7c4	pro
podezření	podezření	k1gNnSc4	podezření
z	z	k7c2	z
vyhýbání	vyhýbání	k1gNnSc2	vyhýbání
se	se	k3xPyFc4	se
vojenskému	vojenský	k2eAgInSc3d1	vojenský
odvodu	odvod	k1gInSc3	odvod
<g/>
.	.	kIx.	.
17	[number]	k4	17
dní	den	k1gInPc2	den
strávil	strávit	k5eAaPmAgMnS	strávit
v	v	k7c6	v
místním	místní	k2eAgNnSc6d1	místní
vězení	vězení	k1gNnSc6	vězení
Moyamensing	Moyamensing	k1gInSc4	Moyamensing
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
byl	být	k5eAaImAgMnS	být
propuštěn	propustit	k5eAaPmNgMnS	propustit
a	a	k8xC	a
shledán	shledat	k5eAaPmNgMnS	shledat
neschopným	schopný	k2eNgNnSc7d1	neschopné
pro	pro	k7c4	pro
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
službu	služba	k1gFnSc4	služba
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
Charles	Charles	k1gMnSc1	Charles
Bukowski	Bukowsk	k1gFnSc2	Bukowsk
literárně	literárně	k6eAd1	literárně
debutuje	debutovat	k5eAaBmIp3nS	debutovat
<g/>
,	,	kIx,	,
Whit	Whit	k2eAgInSc1d1	Whit
Burnett	Burnett	k1gInSc1	Burnett
a	a	k8xC	a
Martha	Martha	k1gFnSc1	Martha
Foleyová	Foleyová	k1gFnSc1	Foleyová
mu	on	k3xPp3gMnSc3	on
otisknou	otisknout	k5eAaPmIp3nP	otisknout
povídku	povídka	k1gFnSc4	povídka
Důsledky	důsledek	k1gInPc1	důsledek
málo	málo	k1gNnSc1	málo
strohého	strohý	k2eAgNnSc2d1	strohé
odmítnutí	odmítnutí	k1gNnSc2	odmítnutí
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
Aftermath	Aftermath	k1gInSc1	Aftermath
of	of	k?	of
a	a	k8xC	a
Lengthy	Lengtha	k1gFnSc2	Lengtha
Rejection	Rejection	k1gInSc1	Rejection
Slip	slip	k1gInSc1	slip
<g/>
)	)	kIx)	)
v	v	k7c6	v
prestižním	prestižní	k2eAgInSc6d1	prestižní
časopise	časopis	k1gInSc6	časopis
Story	story	k1gFnSc2	story
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
mu	on	k3xPp3gMnSc3	on
Caresse	Caresse	k1gFnSc1	Caresse
Crosbyová	Crosbyový	k2eAgFnSc1d1	Crosbyový
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
s	s	k7c7	s
manželem	manžel	k1gMnSc7	manžel
vlastnila	vlastnit	k5eAaImAgFnS	vlastnit
nakladatelství	nakladatelství	k1gNnSc4	nakladatelství
Black	Blacko	k1gNnPc2	Blacko
Sun	suna	k1gFnPc2	suna
Press	Pressa	k1gFnPc2	Pressa
publikovala	publikovat	k5eAaBmAgFnS	publikovat
povídku	povídka	k1gFnSc4	povídka
20	[number]	k4	20
tanků	tank	k1gInPc2	tank
z	z	k7c2	z
Kasseldownu	Kasseldown	k1gInSc2	Kasseldown
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
20	[number]	k4	20
Tanks	Tanksa	k1gFnPc2	Tanksa
from	from	k1gMnSc1	from
Kasseldown	Kasseldown	k1gMnSc1	Kasseldown
<g/>
)	)	kIx)	)
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Portfolio	portfolio	k1gNnSc4	portfolio
o	o	k7c6	o
muži	muž	k1gMnSc6	muž
čekajícím	čekající	k2eAgMnPc3d1	čekající
na	na	k7c6	na
popravu	poprava	k1gFnSc4	poprava
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
už	už	k6eAd1	už
výrazněji	výrazně	k6eAd2	výrazně
nedařilo	dařit	k5eNaImAgNnS	dařit
se	se	k3xPyFc4	se
literárně	literárně	k6eAd1	literárně
prosadit	prosadit	k5eAaPmF	prosadit
<g/>
,	,	kIx,	,
následuje	následovat	k5eAaImIp3nS	následovat
útlum	útlum	k1gInSc1	útlum
v	v	k7c6	v
tvorbě	tvorba	k1gFnSc6	tvorba
a	a	k8xC	a
začíná	začínat	k5eAaImIp3nS	začínat
období	období	k1gNnSc4	období
toulání	toulání	k1gNnSc2	toulání
<g/>
,	,	kIx,	,
rvaček	rvačka	k1gFnPc2	rvačka
<g/>
,	,	kIx,	,
nezřízeného	zřízený	k2eNgNnSc2d1	nezřízené
pití	pití	k1gNnSc2	pití
<g/>
,	,	kIx,	,
střídání	střídání	k1gNnSc2	střídání
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
podnájmů	podnájem	k1gInPc2	podnájem
a	a	k8xC	a
podřadných	podřadný	k2eAgNnPc2d1	podřadné
zaměstnání	zaměstnání	k1gNnPc2	zaměstnání
<g/>
,	,	kIx,	,
období	období	k1gNnSc1	období
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
sám	sám	k3xTgMnSc1	sám
autor	autor	k1gMnSc1	autor
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
desetiletý	desetiletý	k2eAgInSc4d1	desetiletý
flám	flám	k1gInSc4	flám
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
chvíli	chvíle	k1gFnSc6	chvíle
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Los	los	k1gInSc1	los
Angeles	Angeles	k1gInSc1	Angeles
k	k	k7c3	k
rodičům	rodič	k1gMnPc3	rodič
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
když	když	k8xS	když
se	se	k3xPyFc4	se
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
o	o	k7c6	o
otcově	otcův	k2eAgNnSc6d1	otcovo
povýšení	povýšení	k1gNnSc6	povýšení
v	v	k7c6	v
zaměstnání	zaměstnání	k1gNnSc6	zaměstnání
(	(	kIx(	(
<g/>
dělal	dělat	k5eAaImAgInS	dělat
hlídače	hlídač	k1gMnPc4	hlídač
v	v	k7c6	v
muzeu	muzeum	k1gNnSc6	muzeum
<g/>
)	)	kIx)	)
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
vydával	vydávat	k5eAaImAgInS	vydávat
za	za	k7c4	za
autora	autor	k1gMnSc4	autor
jeho	jeho	k3xOp3gFnSc2	jeho
otištěné	otištěný	k2eAgFnSc2d1	otištěná
povídky	povídka	k1gFnSc2	povídka
(	(	kIx(	(
<g/>
podvod	podvod	k1gInSc1	podvod
mu	on	k3xPp3gMnSc3	on
usnadnilo	usnadnit	k5eAaPmAgNnS	usnadnit
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
oba	dva	k4xCgMnPc1	dva
měli	mít	k5eAaImAgMnP	mít
stejné	stejný	k2eAgNnSc4d1	stejné
jméno	jméno	k1gNnSc4	jméno
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
znechutilo	znechutit	k5eAaPmAgNnS	znechutit
jej	on	k3xPp3gMnSc4	on
to	ten	k3xDgNnSc1	ten
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
odstěhoval	odstěhovat	k5eAaPmAgMnS	odstěhovat
do	do	k7c2	do
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
blízko	blízko	k7c2	blízko
Alvarado	Alvarada	k1gFnSc5	Alvarada
Street	Street	k1gInSc4	Street
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
baru	bar	k1gInSc6	bar
Glenview	Glenview	k1gFnSc2	Glenview
potkal	potkat	k5eAaPmAgInS	potkat
Jane	Jan	k1gMnSc5	Jan
Cooney	Coone	k1gMnPc4	Coone
Bakerovou	Bakerová	k1gFnSc4	Bakerová
<g/>
,	,	kIx,	,
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
vážnou	vážný	k2eAgFnSc4d1	vážná
známost	známost	k1gFnSc4	známost
<g/>
.	.	kIx.	.
</s>
<s>
Jane	Jan	k1gMnSc5	Jan
byla	být	k5eAaImAgFnS	být
o	o	k7c4	o
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
starší	starý	k2eAgFnSc1d2	starší
alkoholička	alkoholička	k1gFnSc1	alkoholička
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
inspirací	inspirace	k1gFnPc2	inspirace
pro	pro	k7c4	pro
mnoho	mnoho	k6eAd1	mnoho
jeho	jeho	k3xOp3gNnPc2	jeho
nejlepších	dobrý	k2eAgNnPc2d3	nejlepší
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
<s>
Objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
v	v	k7c6	v
básnické	básnický	k2eAgFnSc6d1	básnická
sbírce	sbírka	k1gFnSc6	sbírka
The	The	k1gMnSc1	The
Days	Daysa	k1gFnPc2	Daysa
Run	Runa	k1gFnPc2	Runa
Away	Awaa	k1gMnSc2	Awaa
Like	Like	k1gNnSc2	Like
Wild	Wild	k1gMnSc1	Wild
Horses	Horses	k1gMnSc1	Horses
Over	Over	k1gMnSc1	Over
the	the	k?	the
Hills	Hills	k1gInSc1	Hills
<g/>
,	,	kIx,	,
v	v	k7c6	v
románu	román	k1gInSc6	román
Poštovní	poštovní	k2eAgInSc1d1	poštovní
úřad	úřad	k1gInSc1	úřad
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Betty	Betty	k1gFnSc2	Betty
<g/>
,	,	kIx,	,
v	v	k7c6	v
románu	román	k1gInSc2	román
Faktótum	faktótum	k1gNnSc1	faktótum
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
Laura	Laura	k1gFnSc1	Laura
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
filmu	film	k1gInSc6	film
Štamgast	štamgast	k1gMnSc1	štamgast
režiséra	režisér	k1gMnSc2	režisér
Barbeta	Barbet	k2eAgFnSc1d1	Barbet
Schroedera	Schroedera	k1gFnSc1	Schroedera
ji	on	k3xPp3gFnSc4	on
představuje	představovat	k5eAaImIp3nS	představovat
Wanda	Wanda	k1gFnSc1	Wanda
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgFnSc1d1	hlavní
ženská	ženský	k2eAgFnSc1d1	ženská
postava	postava	k1gFnSc1	postava
v	v	k7c6	v
podání	podání	k1gNnSc6	podání
herečky	herečka	k1gFnSc2	herečka
Faye	Fay	k1gFnSc2	Fay
Dunaway	Dunawaa	k1gFnSc2	Dunawaa
<g/>
.	.	kIx.	.
</s>
<s>
Bukowski	Bukowski	k6eAd1	Bukowski
se	se	k3xPyFc4	se
do	do	k7c2	do
Jane	Jan	k1gMnSc5	Jan
zamiloval	zamilovat	k5eAaPmAgMnS	zamilovat
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
první	první	k4xOgFnSc1	první
žena	žena	k1gFnSc1	žena
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
mu	on	k3xPp3gMnSc3	on
věnovala	věnovat	k5eAaImAgFnS	věnovat
pozornost	pozornost	k1gFnSc4	pozornost
a	a	k8xC	a
také	také	k9	také
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mají	mít	k5eAaImIp3nP	mít
mnoho	mnoho	k6eAd1	mnoho
společného	společný	k2eAgMnSc4d1	společný
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
negativní	negativní	k2eAgNnSc4d1	negativní
vnímání	vnímání	k1gNnSc4	vnímání
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Jane	Jan	k1gMnSc5	Jan
vzala	vzít	k5eAaPmAgFnS	vzít
Charlese	Charles	k1gMnSc4	Charles
na	na	k7c4	na
dostihy	dostih	k1gInPc4	dostih
a	a	k8xC	a
ukázala	ukázat	k5eAaPmAgFnS	ukázat
mu	on	k3xPp3gMnSc3	on
systém	systém	k1gInSc1	systém
sázek	sázka	k1gFnPc2	sázka
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
jeho	jeho	k3xOp3gFnSc1	jeho
celoživotní	celoživotní	k2eAgFnSc1d1	celoživotní
vášeň	vášeň	k1gFnSc1	vášeň
k	k	k7c3	k
dostihovým	dostihový	k2eAgInPc3d1	dostihový
závodům	závod	k1gInPc3	závod
<g/>
.	.	kIx.	.
</s>
<s>
Prožili	prožít	k5eAaPmAgMnP	prožít
společně	společně	k6eAd1	společně
spoustu	spousta	k1gFnSc4	spousta
pitek	pitka	k1gFnPc2	pitka
<g/>
,	,	kIx,	,
hádek	hádka	k1gFnPc2	hádka
<g/>
,	,	kIx,	,
rozchodů	rozchod	k1gInPc2	rozchod
a	a	k8xC	a
návratů	návrat	k1gInPc2	návrat
<g/>
.	.	kIx.	.
</s>
<s>
Střídali	střídat	k5eAaImAgMnP	střídat
podnájmy	podnájem	k1gInPc4	podnájem
a	a	k8xC	a
často	často	k6eAd1	často
neměli	mít	k5eNaImAgMnP	mít
peníze	peníz	k1gInPc4	peníz
ani	ani	k8xC	ani
na	na	k7c4	na
jídlo	jídlo	k1gNnSc4	jídlo
<g/>
.	.	kIx.	.
</s>
<s>
Sousedé	soused	k1gMnPc1	soused
je	on	k3xPp3gFnPc4	on
nenáviděli	návidět	k5eNaImAgMnP	návidět
kvůli	kvůli	k7c3	kvůli
ustavičnému	ustavičný	k2eAgInSc3d1	ustavičný
hluku	hluk	k1gInSc3	hluk
<g/>
,	,	kIx,	,
rvačkám	rvačka	k1gFnPc3	rvačka
a	a	k8xC	a
nočnímu	noční	k2eAgInSc3d1	noční
křiku	křik	k1gInSc3	křik
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Jane	Jan	k1gMnSc5	Jan
měla	mít	k5eAaImAgFnS	mít
velmi	velmi	k6eAd1	velmi
uvolněnou	uvolněný	k2eAgFnSc4d1	uvolněná
morálku	morálka	k1gFnSc4	morálka
a	a	k8xC	a
nedělalo	dělat	k5eNaImAgNnS	dělat
jí	on	k3xPp3gFnSc3	on
problém	problém	k1gInSc4	problém
nechat	nechat	k5eAaPmF	nechat
se	se	k3xPyFc4	se
pozvat	pozvat	k5eAaPmF	pozvat
kýmkoli	kdokoli	k3yInSc7	kdokoli
na	na	k7c4	na
panáka	panák	k1gMnSc4	panák
<g/>
,	,	kIx,	,
Bukowski	Bukowski	k1gNnPc7	Bukowski
silně	silně	k6eAd1	silně
žárlil	žárlit	k5eAaImAgMnS	žárlit
a	a	k8xC	a
po	po	k7c6	po
rozchodu	rozchod	k1gInSc6	rozchod
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
měl	mít	k5eAaImAgMnS	mít
o	o	k7c6	o
ženách	žena	k1gFnPc6	žena
velmi	velmi	k6eAd1	velmi
špatné	špatný	k2eAgNnSc4d1	špatné
mínění	mínění	k1gNnSc4	mínění
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
si	se	k3xPyFc3	se
přivydělávala	přivydělávat	k5eAaImAgFnS	přivydělávat
na	na	k7c4	na
alkohol	alkohol	k1gInSc4	alkohol
i	i	k8xC	i
příležitostnou	příležitostný	k2eAgFnSc7d1	příležitostná
prostitucí	prostituce	k1gFnSc7	prostituce
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
období	období	k1gNnSc1	období
bylo	být	k5eAaImAgNnS	být
výživným	výživný	k2eAgNnSc7d1	výživné
podhoubím	podhoubí	k1gNnSc7	podhoubí
nejlepších	dobrý	k2eAgInPc2d3	nejlepší
portrétů	portrét	k1gInPc2	portrét
Bukowského	Bukowského	k2eAgInPc2d1	Bukowského
na	na	k7c6	na
dně	dno	k1gNnSc6	dno
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
právě	právě	k6eAd1	právě
nyní	nyní	k6eAd1	nyní
sbírá	sbírat	k5eAaImIp3nS	sbírat
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
zážitků	zážitek	k1gInPc2	zážitek
a	a	k8xC	a
spoustu	spousta	k1gFnSc4	spousta
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
pak	pak	k6eAd1	pak
dokáže	dokázat	k5eAaPmIp3nS	dokázat
přenést	přenést	k5eAaPmF	přenést
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
Charles	Charles	k1gMnSc1	Charles
Bukowski	Bukowski	k1gNnSc4	Bukowski
do	do	k7c2	do
služeb	služba	k1gFnPc2	služba
americké	americký	k2eAgFnSc2d1	americká
pošty	pošta	k1gFnSc2	pošta
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
necelých	celý	k2eNgNnPc6d1	necelé
třech	tři	k4xCgNnPc6	tři
letech	léto	k1gNnPc6	léto
odchází	odcházet	k5eAaImIp3nS	odcházet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1955	[number]	k4	1955
odvezli	odvézt	k5eAaPmAgMnP	odvézt
Bukowského	Bukowský	k1gMnSc4	Bukowský
na	na	k7c4	na
chudinské	chudinský	k2eAgNnSc4d1	chudinské
oddělení	oddělení	k1gNnSc4	oddělení
místní	místní	k2eAgFnSc2d1	místní
nemocnice	nemocnice	k1gFnSc2	nemocnice
s	s	k7c7	s
krvácejícím	krvácející	k2eAgInSc7d1	krvácející
žaludečním	žaludeční	k2eAgInSc7d1	žaludeční
vředem	vřed	k1gInSc7	vřed
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
si	se	k3xPyFc3	se
přivodil	přivodit	k5eAaPmAgInS	přivodit
nadměrnou	nadměrný	k2eAgFnSc7d1	nadměrná
konzumací	konzumace	k1gFnSc7	konzumace
alkoholu	alkohol	k1gInSc2	alkohol
<g/>
.	.	kIx.	.
</s>
<s>
Potřeboval	potřebovat	k5eAaImAgMnS	potřebovat
transfuzi	transfuze	k1gFnSc4	transfuze
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jelikož	jelikož	k8xS	jelikož
předtím	předtím	k6eAd1	předtím
nikdy	nikdy	k6eAd1	nikdy
neuložil	uložit	k5eNaPmAgMnS	uložit
do	do	k7c2	do
nemocnice	nemocnice	k1gFnSc2	nemocnice
krev	krev	k1gFnSc4	krev
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
mu	on	k3xPp3gNnSc3	on
odepřena	odepřen	k2eAgNnPc4d1	odepřeno
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
ležel	ležet	k5eAaImAgMnS	ležet
na	na	k7c6	na
vozíku	vozík	k1gInSc6	vozík
neschopen	schopit	k5eNaPmNgInS	schopit
pohybu	pohyb	k1gInSc3	pohyb
<g/>
,	,	kIx,	,
myslel	myslet	k5eAaImAgInS	myslet
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
tam	tam	k6eAd1	tam
zemře	zemřít	k5eAaPmIp3nS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Ironií	ironie	k1gFnSc7	ironie
osudu	osud	k1gInSc2	osud
jediný	jediný	k2eAgMnSc1d1	jediný
člen	člen	k1gMnSc1	člen
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
měl	mít	k5eAaImAgInS	mít
krevní	krevní	k2eAgInSc4d1	krevní
kredit	kredit	k1gInSc4	kredit
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
a	a	k8xC	a
právě	právě	k9	právě
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
nakonec	nakonec	k6eAd1	nakonec
transfuzi	transfuze	k1gFnSc4	transfuze
dostal	dostat	k5eAaPmAgMnS	dostat
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
životní	životní	k2eAgFnSc4d1	životní
zkušenost	zkušenost	k1gFnSc4	zkušenost
promítl	promítnout	k5eAaPmAgMnS	promítnout
do	do	k7c2	do
povídky	povídka	k1gFnSc2	povídka
Život	život	k1gInSc1	život
a	a	k8xC	a
smrt	smrt	k1gFnSc1	smrt
na	na	k7c6	na
sociálním	sociální	k2eAgNnSc6d1	sociální
oddělení	oddělení	k1gNnSc6	oddělení
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
Life	Life	k1gInSc1	Life
and	and	k?	and
Death	Death	k1gInSc1	Death
in	in	k?	in
the	the	k?	the
Charity	charita	k1gFnSc2	charita
Ward	Warda	k1gFnPc2	Warda
ve	v	k7c6	v
sbírce	sbírka	k1gFnSc6	sbírka
Nejkrásnější	krásný	k2eAgFnSc1d3	nejkrásnější
ženská	ženská	k1gFnSc1	ženská
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
angl.	angl.	k?	angl.
The	The	k1gFnSc1	The
most	most	k1gInSc1	most
Beautiful	Beautiful	k1gInSc1	Beautiful
Woman	Woman	k1gInSc1	Woman
in	in	k?	in
Town	Town	k1gMnSc1	Town
&	&	k?	&
Other	Other	k1gMnSc1	Other
Stories	Stories	k1gMnSc1	Stories
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přežil	přežít	k5eAaPmAgMnS	přežít
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
měl	mít	k5eAaImAgMnS	mít
namále	namále	k6eAd1	namále
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
s	s	k7c7	s
pitím	pití	k1gNnSc7	pití
přes	přes	k7c4	přes
varování	varování	k1gNnSc4	varování
lékařů	lékař	k1gMnPc2	lékař
přestat	přestat	k5eAaPmF	přestat
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
věnoval	věnovat	k5eAaImAgMnS	věnovat
psaní	psaní	k1gNnSc4	psaní
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgInS	začít
psát	psát	k5eAaImF	psát
převážně	převážně	k6eAd1	převážně
poezii	poezie	k1gFnSc3	poezie
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1958	[number]	k4	1958
mu	on	k3xPp3gMnSc3	on
zemřel	zemřít	k5eAaPmAgMnS	zemřít
otec	otec	k1gMnSc1	otec
na	na	k7c4	na
infarkt	infarkt	k1gInSc4	infarkt
(	(	kIx(	(
<g/>
matka	matka	k1gFnSc1	matka
již	již	k6eAd1	již
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
dříve	dříve	k6eAd2	dříve
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
příliš	příliš	k6eAd1	příliš
truchlit	truchlit	k5eAaImF	truchlit
nad	nad	k7c7	nad
smrtí	smrt	k1gFnSc7	smrt
matky	matka	k1gFnSc2	matka
<g/>
,	,	kIx,	,
skon	skon	k1gInSc4	skon
otce	otec	k1gMnSc2	otec
byl	být	k5eAaImAgMnS	být
pro	pro	k7c4	pro
něj	on	k3xPp3gInSc4	on
důvod	důvod	k1gInSc4	důvod
k	k	k7c3	k
oslavě	oslava	k1gFnSc3	oslava
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
...	...	k?	...
<g/>
díky	díky	k7c3	díky
bohu	bůh	k1gMnSc3	bůh
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
něm	on	k3xPp3gNnSc6	on
po	po	k7c6	po
něm	on	k3xPp3gNnSc6	on
po	po	k7c6	po
něm	on	k3xPp3gMnSc6	on
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
napsal	napsat	k5eAaBmAgMnS	napsat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pohřbu	pohřeb	k1gInSc6	pohřeb
rozdal	rozdat	k5eAaPmAgMnS	rozdat
většinu	většina	k1gFnSc4	většina
majetku	majetek	k1gInSc2	majetek
rodičů	rodič	k1gMnPc2	rodič
<g/>
,	,	kIx,	,
obrazy	obraz	k1gInPc1	obraz
<g/>
,	,	kIx,	,
příbory	příbor	k1gInPc1	příbor
<g/>
,	,	kIx,	,
koberce	koberec	k1gInPc1	koberec
atd.	atd.	kA	atd.
jejich	jejich	k3xOp3gMnPc3	jejich
přátelům	přítel	k1gMnPc3	přítel
a	a	k8xC	a
známým	známý	k1gMnPc3	známý
(	(	kIx(	(
<g/>
scénku	scénka	k1gFnSc4	scénka
popisuje	popisovat	k5eAaImIp3nS	popisovat
v	v	k7c6	v
povídce	povídka	k1gFnSc6	povídka
Smrt	smrt	k1gFnSc1	smrt
mého	můj	k1gMnSc2	můj
otce	otec	k1gMnSc2	otec
2	[number]	k4	2
ve	v	k7c6	v
sbírce	sbírka	k1gFnSc6	sbírka
Těžký	těžký	k2eAgInSc4d1	těžký
časy	čas	k1gInPc4	čas
<g/>
,	,	kIx,	,
angl.	angl.	k?	angl.
Hot	hot	k0	hot
Water	Watra	k1gFnPc2	Watra
Music	Musice	k1gFnPc2	Musice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
Bukowski	Bukowske	k1gFnSc4	Bukowske
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
celé	celý	k2eAgNnSc1d1	celé
dědictví	dědictví	k1gNnSc1	dědictví
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
činilo	činit	k5eAaImAgNnS	činit
15	[number]	k4	15
000	[number]	k4	000
dolarů	dolar	k1gInPc2	dolar
propil	propít	k5eAaPmAgInS	propít
a	a	k8xC	a
prohrál	prohrát	k5eAaPmAgInS	prohrát
<g/>
,	,	kIx,	,
nebylo	být	k5eNaImAgNnS	být
tomu	ten	k3xDgNnSc3	ten
tak	tak	k6eAd1	tak
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
přátelé	přítel	k1gMnPc1	přítel
vzpomínají	vzpomínat	k5eAaImIp3nP	vzpomínat
<g/>
,	,	kIx,	,
že	že	k8xS	že
měl	mít	k5eAaImAgMnS	mít
nemalé	malý	k2eNgFnPc4d1	nemalá
úspory	úspora	k1gFnPc4	úspora
v	v	k7c6	v
bance	banka	k1gFnSc6	banka
ještě	ještě	k6eAd1	ještě
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
po	po	k7c6	po
otcově	otcův	k2eAgFnSc6d1	otcova
smrti	smrt	k1gFnSc6	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Pravda	pravda	k1gFnSc1	pravda
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
taková	takový	k3xDgFnSc1	takový
<g/>
,	,	kIx,	,
že	že	k8xS	že
od	od	k7c2	od
tohoto	tento	k3xDgInSc2	tento
okamžiku	okamžik	k1gInSc2	okamžik
začal	začít	k5eAaPmAgInS	začít
být	být	k5eAaImF	být
na	na	k7c4	na
peníze	peníz	k1gInPc4	peníz
velmi	velmi	k6eAd1	velmi
opatrný	opatrný	k2eAgInSc1d1	opatrný
-	-	kIx~	-
přes	přes	k7c4	přes
všechny	všechen	k3xTgFnPc4	všechen
své	svůj	k3xOyFgFnPc4	svůj
publikované	publikovaný	k2eAgFnPc4d1	publikovaná
deklarace	deklarace	k1gFnPc4	deklarace
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1957	[number]	k4	1957
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
mladou	mladá	k1gFnSc7	mladá
23	[number]	k4	23
<g/>
letou	letý	k2eAgFnSc7d1	letá
básnířkou	básnířka	k1gFnSc7	básnířka
Barbarou	Barbara	k1gFnSc7	Barbara
Frye	Frye	k1gFnSc7	Frye
z	z	k7c2	z
malého	malý	k2eAgNnSc2d1	malé
texaského	texaský	k2eAgNnSc2d1	texaské
městečka	městečko	k1gNnSc2	městečko
Wheeler	Wheeler	k1gInSc1	Wheeler
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
manželství	manželství	k1gNnSc2	manželství
trvalo	trvat	k5eAaImAgNnS	trvat
necelé	celý	k2eNgInPc4d1	necelý
2	[number]	k4	2
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Bukowski	Bukowski	k6eAd1	Bukowski
ke	k	k7c3	k
svatbě	svatba	k1gFnSc3	svatba
připojuje	připojovat	k5eAaImIp3nS	připojovat
tuto	tento	k3xDgFnSc4	tento
historku	historka	k1gFnSc4	historka
<g/>
:	:	kIx,	:
Barbara	Barbara	k1gFnSc1	Barbara
Frye	Frye	k1gFnSc1	Frye
<g/>
,	,	kIx,	,
editorka	editorka	k1gFnSc1	editorka
časopisu	časopis	k1gInSc2	časopis
Harlequin	Harlequin	k1gInSc4	Harlequin
<g/>
,	,	kIx,	,
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
přijímá	přijímat	k5eAaImIp3nS	přijímat
básně	báseň	k1gFnPc4	báseň
od	od	k7c2	od
začínajících	začínající	k2eAgMnPc2d1	začínající
autorů	autor	k1gMnPc2	autor
<g/>
.	.	kIx.	.
</s>
<s>
Bukowski	Bukowski	k6eAd1	Bukowski
nelenil	lenit	k5eNaImAgMnS	lenit
a	a	k8xC	a
poslal	poslat	k5eAaPmAgMnS	poslat
jí	jíst	k5eAaImIp3nS	jíst
hromadu	hromada	k1gFnSc4	hromada
svých	svůj	k3xOyFgFnPc2	svůj
věcí	věc	k1gFnPc2	věc
a	a	k8xC	a
podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
ji	on	k3xPp3gFnSc4	on
zaujmout	zaujmout	k5eAaPmF	zaujmout
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
otiskla	otisknout	k5eAaPmAgFnS	otisknout
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
si	se	k3xPyFc3	se
začali	začít	k5eAaPmAgMnP	začít
dopisovat	dopisovat	k5eAaImF	dopisovat
a	a	k8xC	a
Barbara	Barbara	k1gFnSc1	Barbara
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
svěřila	svěřit	k5eAaPmAgFnS	svěřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
svobodná	svobodný	k2eAgFnSc1d1	svobodná
a	a	k8xC	a
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
lehkou	lehký	k2eAgFnSc4d1	lehká
tělesnou	tělesný	k2eAgFnSc4d1	tělesná
vadu	vada	k1gFnSc4	vada
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
níž	jenž	k3xRgFnSc3	jenž
má	mít	k5eAaImIp3nS	mít
strach	strach	k1gInSc1	strach
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
neprovdá	provdat	k5eNaPmIp3nS	provdat
<g/>
.	.	kIx.	.
</s>
<s>
Bukowski	Bukowski	k6eAd1	Bukowski
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
soucítil	soucítit	k5eAaImAgMnS	soucítit
<g/>
,	,	kIx,	,
vliv	vliv	k1gInSc1	vliv
mělo	mít	k5eAaImAgNnS	mít
i	i	k9	i
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
dopisech	dopis	k1gInPc6	dopis
milá	milá	k1gFnSc1	milá
a	a	k8xC	a
odepsal	odepsat	k5eAaPmAgMnS	odepsat
jí	on	k3xPp3gFnSc3	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
vezme	vzít	k5eAaPmIp3nS	vzít
<g/>
.	.	kIx.	.
</s>
<s>
Barbaře	Barbara	k1gFnSc3	Barbara
chyběly	chybět	k5eAaImAgInP	chybět
dva	dva	k4xCgInPc1	dva
krční	krční	k2eAgInPc1d1	krční
obratle	obratel	k1gInPc1	obratel
a	a	k8xC	a
díky	díky	k7c3	díky
tomu	ten	k3xDgMnSc3	ten
vypadala	vypadat	k5eAaPmAgFnS	vypadat
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
by	by	kYmCp3nP	by
neustále	neustále	k6eAd1	neustále
hrbila	hrbit	k5eAaImAgNnP	hrbit
ramena	rameno	k1gNnPc1	rameno
<g/>
.	.	kIx.	.
</s>
<s>
Nemohla	moct	k5eNaImAgFnS	moct
ani	ani	k8xC	ani
otočit	otočit	k5eAaPmF	otočit
hlavu	hlava	k1gFnSc4	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Bukowskému	Bukowský	k1gMnSc3	Bukowský
to	ten	k3xDgNnSc1	ten
nakonec	nakonec	k9	nakonec
nepřipadlo	připadnout	k5eNaPmAgNnS	připadnout
nijak	nijak	k6eAd1	nijak
zvlášť	zvlášť	k6eAd1	zvlášť
omezující	omezující	k2eAgFnPc1d1	omezující
pro	pro	k7c4	pro
vztah	vztah	k1gInSc4	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
tento	tento	k3xDgInSc1	tento
příběh	příběh	k1gInSc1	příběh
je	být	k5eAaImIp3nS	být
částečnou	částečný	k2eAgFnSc7d1	částečná
fikcí	fikce	k1gFnSc7	fikce
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
autorova	autorův	k2eAgFnSc1d1	autorova
poezie	poezie	k1gFnSc1	poezie
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
Harlequinu	Harlequin	k1gInSc6	Harlequin
až	až	k6eAd1	až
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
Bukowski	Bukowsk	k1gFnSc2	Bukowsk
stal	stát	k5eAaPmAgMnS	stát
spoluvydavatelem	spoluvydavatel	k1gMnSc7	spoluvydavatel
časopisu	časopis	k1gInSc2	časopis
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
po	po	k7c6	po
svatbě	svatba	k1gFnSc6	svatba
<g/>
.	.	kIx.	.
</s>
<s>
Zbytek	zbytek	k1gInSc1	zbytek
je	být	k5eAaImIp3nS	být
pravdivý	pravdivý	k2eAgInSc1d1	pravdivý
<g/>
,	,	kIx,	,
intimní	intimní	k2eAgFnSc2d1	intimní
korespondence	korespondence	k1gFnSc2	korespondence
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
probíhala	probíhat	k5eAaImAgFnS	probíhat
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
došlo	dojít	k5eAaPmAgNnS	dojít
i	i	k9	i
na	na	k7c4	na
výměnu	výměna	k1gFnSc4	výměna
fotografií	fotografia	k1gFnPc2	fotografia
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
jí	jíst	k5eAaImIp3nS	jíst
opravdu	opravdu	k6eAd1	opravdu
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
sňatek	sňatek	k1gInSc4	sňatek
i	i	k9	i
přes	přes	k7c4	přes
její	její	k3xOp3gInSc4	její
handicap	handicap	k1gInSc4	handicap
<g/>
.	.	kIx.	.
</s>
<s>
Novomanželé	novomanžel	k1gMnPc1	novomanžel
žili	žít	k5eAaImAgMnP	žít
v	v	k7c6	v
Texasu	Texas	k1gInSc6	Texas
pouze	pouze	k6eAd1	pouze
několik	několik	k4yIc4	několik
týdnů	týden	k1gInPc2	týden
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
přestěhovali	přestěhovat	k5eAaPmAgMnP	přestěhovat
do	do	k7c2	do
L.	L.	kA	L.
A.	A.	kA	A.
do	do	k7c2	do
kopcovité	kopcovitý	k2eAgFnSc2d1	kopcovitá
čtvrti	čtvrt	k1gFnSc2	čtvrt
Echo	echo	k1gNnSc1	echo
Park	park	k1gInSc1	park
ležící	ležící	k2eAgFnSc1d1	ležící
mezi	mezi	k7c4	mezi
West	West	k2eAgInSc4d1	West
Lake	Lake	k1gInSc4	Lake
a	a	k8xC	a
Elysian	Elysian	k1gInSc4	Elysian
Parkem	park	k1gInSc7	park
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
jako	jako	k9	jako
spoluvydavatel	spoluvydavatel	k1gMnSc1	spoluvydavatel
Harlequinu	Harlequin	k1gInSc2	Harlequin
se	se	k3xPyFc4	se
Bukowski	Bukowske	k1gFnSc4	Bukowske
pustil	pustit	k5eAaPmAgMnS	pustit
do	do	k7c2	do
všech	všecek	k3xTgMnPc2	všecek
básníků	básník	k1gMnPc2	básník
<g/>
,	,	kIx,	,
které	který	k3yQgMnPc4	který
neměl	mít	k5eNaImAgInS	mít
rád	rád	k6eAd1	rád
a	a	k8xC	a
vydavatelů	vydavatel	k1gMnPc2	vydavatel
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
jej	on	k3xPp3gMnSc4	on
dříve	dříve	k6eAd2	dříve
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
začal	začít	k5eAaPmAgInS	začít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
psát	psát	k5eAaImF	psát
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
román	román	k1gInSc4	román
Místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
přespat	přespat	k5eAaPmF	přespat
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
ale	ale	k8xC	ale
vydán	vydán	k2eAgMnSc1d1	vydán
nebyl	být	k5eNaImAgInS	být
<g/>
.	.	kIx.	.
</s>
<s>
Rodina	rodina	k1gFnSc1	rodina
Barbary	Barbara	k1gFnSc2	Barbara
netrpěla	trpět	k5eNaImAgFnS	trpět
finanční	finanční	k2eAgFnSc7d1	finanční
nouzí	nouze	k1gFnSc7	nouze
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
během	během	k7c2	během
vztahu	vztah	k1gInSc2	vztah
Barbara	Barbara	k1gFnSc1	Barbara
nedisponovala	disponovat	k5eNaBmAgFnS	disponovat
větší	veliký	k2eAgFnSc7d2	veliký
sumou	suma	k1gFnSc7	suma
peněz	peníze	k1gInPc2	peníze
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
členové	člen	k1gMnPc1	člen
rodiny	rodina	k1gFnSc2	rodina
svatbu	svatba	k1gFnSc4	svatba
neschvalovali	schvalovat	k5eNaImAgMnP	schvalovat
<g/>
.	.	kIx.	.
</s>
<s>
Barbara	Barbara	k1gFnSc1	Barbara
byla	být	k5eAaImAgFnS	být
umělecky	umělecky	k6eAd1	umělecky
založená	založený	k2eAgFnSc1d1	založená
a	a	k8xC	a
kromě	kromě	k7c2	kromě
psaní	psaní	k1gNnSc2	psaní
poezie	poezie	k1gFnSc2	poezie
(	(	kIx(	(
<g/>
kterou	který	k3yRgFnSc4	který
Bukowski	Bukowske	k1gFnSc4	Bukowske
pokládal	pokládat	k5eAaImAgMnS	pokládat
za	za	k7c4	za
"	"	kIx"	"
<g/>
dost	dost	k6eAd1	dost
dobrou	dobrý	k2eAgFnSc4d1	dobrá
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
pokoušela	pokoušet	k5eAaImAgFnS	pokoušet
malovat	malovat	k5eAaImF	malovat
<g/>
.	.	kIx.	.
</s>
<s>
Zapsala	zapsat	k5eAaPmAgFnS	zapsat
se	se	k3xPyFc4	se
do	do	k7c2	do
kurzu	kurz	k1gInSc2	kurz
výtvarného	výtvarný	k2eAgNnSc2d1	výtvarné
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
Hank	Hank	k1gInSc1	Hank
ji	on	k3xPp3gFnSc4	on
tam	tam	k6eAd1	tam
chodil	chodit	k5eAaImAgMnS	chodit
navštěvovat	navštěvovat	k5eAaImF	navštěvovat
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jednou	k6eAd1	jednou
ho	on	k3xPp3gMnSc4	on
lektor	lektor	k1gMnSc1	lektor
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jen	jen	k9	jen
tak	tak	k6eAd1	tak
neseděl	sedět	k5eNaImAgMnS	sedět
v	v	k7c6	v
rohu	roh	k1gInSc6	roh
<g/>
,	,	kIx,	,
připojil	připojit	k5eAaPmAgMnS	připojit
se	se	k3xPyFc4	se
k	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
a	a	k8xC	a
zkusil	zkusit	k5eAaPmAgMnS	zkusit
také	také	k9	také
něco	něco	k3yInSc1	něco
namalovat	namalovat	k5eAaPmF	namalovat
<g/>
.	.	kIx.	.
</s>
<s>
Bukowski	Bukowski	k6eAd1	Bukowski
výzvu	výzva	k1gFnSc4	výzva
přijal	přijmout	k5eAaPmAgMnS	přijmout
<g/>
,	,	kIx,	,
v	v	k7c6	v
rychlosti	rychlost	k1gFnSc6	rychlost
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
obrázek	obrázek	k1gInSc1	obrázek
vázy	váza	k1gFnSc2	váza
a	a	k8xC	a
odešel	odejít	k5eAaPmAgMnS	odejít
na	na	k7c4	na
kávu	káva	k1gFnSc4	káva
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
ostatní	ostatní	k2eAgMnPc1d1	ostatní
ještě	ještě	k6eAd1	ještě
pracovali	pracovat	k5eAaImAgMnP	pracovat
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
<g/>
,	,	kIx,	,
učitel	učitel	k1gMnSc1	učitel
vzal	vzít	k5eAaPmAgMnS	vzít
jeho	on	k3xPp3gInSc4	on
obraz	obraz	k1gInSc4	obraz
a	a	k8xC	a
vystavil	vystavit	k5eAaPmAgInS	vystavit
ho	on	k3xPp3gMnSc4	on
s	s	k7c7	s
pochvalnými	pochvalný	k2eAgNnPc7d1	pochvalné
slovy	slovo	k1gNnPc7	slovo
před	před	k7c7	před
celou	celý	k2eAgFnSc7d1	celá
třídou	třída	k1gFnSc7	třída
<g/>
.	.	kIx.	.
</s>
<s>
Bukowski	Bukowski	k6eAd1	Bukowski
pak	pak	k6eAd1	pak
namaloval	namalovat	k5eAaPmAgMnS	namalovat
ještě	ještě	k9	ještě
několik	několik	k4yIc4	několik
obrazů	obraz	k1gInPc2	obraz
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
chtěl	chtít	k5eAaImAgMnS	chtít
učitel	učitel	k1gMnSc1	učitel
použít	použít	k5eAaPmF	použít
na	na	k7c6	na
zamýšlené	zamýšlený	k2eAgFnSc6d1	zamýšlená
výstavě	výstava	k1gFnSc6	výstava
<g/>
.	.	kIx.	.
</s>
<s>
Barbara	Barbara	k1gFnSc1	Barbara
se	se	k3xPyFc4	se
cítila	cítit	k5eAaImAgFnS	cítit
nedoceněná	doceněný	k2eNgFnSc1d1	nedoceněná
a	a	k8xC	a
špatně	špatně	k6eAd1	špatně
to	ten	k3xDgNnSc4	ten
nesla	nést	k5eAaImAgFnS	nést
<g/>
.	.	kIx.	.
</s>
<s>
Manželství	manželství	k1gNnSc1	manželství
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
sestupnou	sestupný	k2eAgFnSc7d1	sestupná
křivkou	křivka	k1gFnSc7	křivka
<g/>
.	.	kIx.	.
</s>
<s>
Bukowski	Bukowsk	k1gMnSc3	Bukowsk
Barbaru	barbar	k1gMnSc3	barbar
nemiloval	milovat	k5eNaImAgMnS	milovat
a	a	k8xC	a
jako	jako	k8xC	jako
manžel	manžel	k1gMnSc1	manžel
jí	jíst	k5eAaImIp3nS	jíst
toho	ten	k3xDgMnSc4	ten
mohl	moct	k5eAaImAgMnS	moct
nabídnout	nabídnout	k5eAaPmF	nabídnout
velice	velice	k6eAd1	velice
málo	málo	k6eAd1	málo
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
jí	on	k3xPp3gFnSc3	on
začal	začít	k5eAaPmAgInS	začít
vadit	vadit	k5eAaImF	vadit
jeho	jeho	k3xOp3gInSc1	jeho
povalečský	povalečský	k2eAgInSc1d1	povalečský
způsob	způsob	k1gInSc1	způsob
života	život	k1gInSc2	život
-	-	kIx~	-
lenošení	lenošení	k1gNnSc2	lenošení
<g/>
,	,	kIx,	,
popíjení	popíjení	k1gNnSc2	popíjení
piva	pivo	k1gNnSc2	pivo
a	a	k8xC	a
studování	studování	k1gNnSc2	studování
dostihových	dostihový	k2eAgInPc2d1	dostihový
programů	program	k1gInPc2	program
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
pracoval	pracovat	k5eAaImAgMnS	pracovat
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
špatně	špatně	k6eAd1	špatně
placené	placený	k2eAgNnSc1d1	placené
zaměstnání	zaměstnání	k1gNnSc1	zaměstnání
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
návratu	návrat	k1gInSc2	návrat
do	do	k7c2	do
služeb	služba	k1gFnPc2	služba
pošty	pošta	k1gFnSc2	pošta
<g/>
.	.	kIx.	.
</s>
<s>
Charlese	Charles	k1gMnSc2	Charles
zase	zase	k9	zase
dráždila	dráždit	k5eAaImAgFnS	dráždit
její	její	k3xOp3gFnSc4	její
faleš	faleš	k1gFnSc4	faleš
a	a	k8xC	a
měl	mít	k5eAaImAgInS	mít
ji	on	k3xPp3gFnSc4	on
rád	rád	k6eAd1	rád
čím	co	k3yInSc7	co
dál	daleko	k6eAd2	daleko
méně	málo	k6eAd2	málo
<g/>
.	.	kIx.	.
</s>
<s>
Rozvod	rozvod	k1gInSc1	rozvod
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
18	[number]	k4	18
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1958	[number]	k4	1958
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
se	se	k3xPyFc4	se
autor	autor	k1gMnSc1	autor
přestěhoval	přestěhovat	k5eAaPmAgMnS	přestěhovat
do	do	k7c2	do
svého	svůj	k3xOyFgNnSc2	svůj
osvědčeného	osvědčený	k2eAgNnSc2d1	osvědčené
prostředí	prostředí	k1gNnSc2	prostředí
staromládeneckých	staromládenecký	k2eAgInPc2d1	staromládenecký
podnájmů	podnájem	k1gInPc2	podnájem
<g/>
,	,	kIx,	,
neměl	mít	k5eNaImAgInS	mít
potřebu	potřeba	k1gFnSc4	potřeba
zůstávat	zůstávat	k5eAaImF	zůstávat
v	v	k7c6	v
domě	dům	k1gInSc6	dům
se	s	k7c7	s
zahradou	zahrada	k1gFnSc7	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
novou	nový	k2eAgFnSc7d1	nová
adresou	adresa	k1gFnSc7	adresa
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
omšelý	omšelý	k2eAgInSc1d1	omšelý
pokoj	pokoj	k1gInSc1	pokoj
ve	v	k7c6	v
čtvrté	čtvrtý	k4xOgFnSc6	čtvrtý
etáži	etáž	k1gFnSc6	etáž
činžáku	činžák	k1gInSc2	činžák
ve	v	k7c6	v
východním	východní	k2eAgInSc6d1	východní
Hollywoodu	Hollywood	k1gInSc6	Hollywood
na	na	k7c4	na
North	North	k1gInSc4	North
Mariposa	Mariposa	k1gFnSc1	Mariposa
Avenue	avenue	k1gFnSc1	avenue
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
mezi	mezi	k7c4	mezi
Hollywood	Hollywood	k1gInSc4	Hollywood
Boulevard	Boulevard	k1gMnSc1	Boulevard
a	a	k8xC	a
Sunset	Sunset	k1gMnSc1	Sunset
Boulevard	Boulevard	k1gMnSc1	Boulevard
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k8xC	i
Barbaře	Barbara	k1gFnSc3	Barbara
věnoval	věnovat	k5eAaPmAgMnS	věnovat
autor	autor	k1gMnSc1	autor
několik	několik	k4yIc4	několik
básní	báseň	k1gFnPc2	báseň
a	a	k8xC	a
v	v	k7c6	v
próze	próza	k1gFnSc6	próza
se	se	k3xPyFc4	se
nejvýrazněji	výrazně	k6eAd3	výrazně
objevuje	objevovat	k5eAaImIp3nS	objevovat
jako	jako	k9	jako
postava	postava	k1gFnSc1	postava
Joyce	Joyec	k1gInSc2	Joyec
v	v	k7c6	v
románu	román	k1gInSc6	román
Poštovní	poštovní	k2eAgInSc4d1	poštovní
úřad	úřad	k1gInSc4	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
1962	[number]	k4	1962
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
spisovatele	spisovatel	k1gMnPc4	spisovatel
traumatizující	traumatizující	k2eAgFnSc1d1	traumatizující
<g/>
,	,	kIx,	,
22	[number]	k4	22
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
zemřela	zemřít	k5eAaPmAgFnS	zemřít
totiž	totiž	k9	totiž
Jane	Jan	k1gMnSc5	Jan
Cooney	Coonea	k1gFnPc1	Coonea
Bakerová	Bakerová	k1gFnSc1	Bakerová
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
první	první	k4xOgFnSc1	první
láska	láska	k1gFnSc1	láska
a	a	k8xC	a
studnice	studnice	k1gFnSc1	studnice
inspirace	inspirace	k1gFnSc1	inspirace
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
pocit	pocit	k1gInSc4	pocit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ztratil	ztratit	k5eAaPmAgInS	ztratit
někoho	někdo	k3yInSc4	někdo
blízkého	blízký	k2eAgNnSc2d1	blízké
<g/>
,	,	kIx,	,
jediného	jediný	k2eAgMnSc2d1	jediný
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
mu	on	k3xPp3gMnSc3	on
rozuměl	rozumět	k5eAaImAgMnS	rozumět
<g/>
.	.	kIx.	.
</s>
<s>
Bukowski	Bukowski	k6eAd1	Bukowski
toto	tento	k3xDgNnSc4	tento
těžké	těžký	k2eAgNnSc4d1	těžké
období	období	k1gNnSc4	období
přetavil	přetavit	k5eAaPmAgInS	přetavit
do	do	k7c2	do
mnoha	mnoho	k4c2	mnoho
básní	báseň	k1gFnPc2	báseň
a	a	k8xC	a
povídek	povídka	k1gFnPc2	povídka
<g/>
.	.	kIx.	.
</s>
<s>
Psaní	psaní	k1gNnPc1	psaní
a	a	k8xC	a
pití	pití	k1gNnPc1	pití
byly	být	k5eAaImAgFnP	být
jediné	jediný	k2eAgFnPc4d1	jediná
věci	věc	k1gFnPc4	věc
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
ho	on	k3xPp3gMnSc2	on
držely	držet	k5eAaImAgInP	držet
při	při	k7c6	při
životě	život	k1gInSc6	život
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
ho	on	k3xPp3gMnSc4	on
myšlenky	myšlenka	k1gFnPc4	myšlenka
na	na	k7c4	na
sebevraždu	sebevražda	k1gFnSc4	sebevražda
neopouštěly	opouštět	k5eNaImAgInP	opouštět
<g/>
.	.	kIx.	.
</s>
<s>
Posílal	posílat	k5eAaImAgMnS	posílat
stovky	stovka	k1gFnPc4	stovka
příspěvků	příspěvek	k1gInPc2	příspěvek
do	do	k7c2	do
všemožných	všemožný	k2eAgInPc2d1	všemožný
časopisů	časopis	k1gInPc2	časopis
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
i	i	k8xC	i
novináři	novinář	k1gMnSc6	novinář
Johnu	John	k1gMnSc6	John
Bryanovi	Bryan	k1gMnSc6	Bryan
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
mu	on	k3xPp3gMnSc3	on
vydal	vydat	k5eAaPmAgMnS	vydat
několik	několik	k4yIc4	několik
básní	báseň	k1gFnPc2	báseň
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Renaissance	Renaissance	k1gFnSc2	Renaissance
<g/>
.	.	kIx.	.
</s>
<s>
Tou	ten	k3xDgFnSc7	ten
dobou	doba	k1gFnSc7	doba
měl	mít	k5eAaImAgMnS	mít
autor	autor	k1gMnSc1	autor
i	i	k8xC	i
časté	častý	k2eAgInPc1d1	častý
spory	spor	k1gInPc1	spor
se	s	k7c7	s
sousedy	soused	k1gMnPc7	soused
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
psal	psát	k5eAaImAgInS	psát
až	až	k6eAd1	až
do	do	k7c2	do
noci	noc	k1gFnSc2	noc
a	a	k8xC	a
psací	psací	k2eAgInSc1d1	psací
stroj	stroj	k1gInSc1	stroj
dělal	dělat	k5eAaImAgInS	dělat
velký	velký	k2eAgInSc1d1	velký
rámus	rámus	k1gInSc1	rámus
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
rok	rok	k1gInSc4	rok
poté	poté	k6eAd1	poté
(	(	kIx(	(
<g/>
jaro	jaro	k1gNnSc4	jaro
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
Frances	Frances	k1gInSc4	Frances
Smithovou	Smithová	k1gFnSc4	Smithová
(	(	kIx(	(
<g/>
rozenou	rozený	k2eAgFnSc4d1	rozená
Frances	Francesa	k1gFnPc2	Francesa
Elizabeth	Elizabeth	k1gFnSc1	Elizabeth
Deanovou	Deanová	k1gFnSc4	Deanová
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
si	se	k3xPyFc3	se
změnila	změnit	k5eAaPmAgFnS	změnit
jméno	jméno	k1gNnSc4	jméno
na	na	k7c4	na
FrancEyE	FrancEyE	k1gFnSc4	FrancEyE
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Začali	začít	k5eAaPmAgMnP	začít
se	se	k3xPyFc4	se
vídat	vídat	k5eAaImF	vídat
pravidelně	pravidelně	k6eAd1	pravidelně
a	a	k8xC	a
FrancEyE	FrancEyE	k1gFnSc1	FrancEyE
se	se	k3xPyFc4	se
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
do	do	k7c2	do
L.A.	L.A.	k1gFnSc2	L.A.
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
nablízku	nablízku	k6eAd1	nablízku
<g/>
.	.	kIx.	.
</s>
<s>
Vzpomíná	vzpomínat	k5eAaImIp3nS	vzpomínat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Bukowski	Bukowski	k1gNnSc1	Bukowski
během	během	k7c2	během
týdne	týden	k1gInSc2	týden
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
musel	muset	k5eAaImAgMnS	muset
do	do	k7c2	do
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
pití	pití	k1gNnSc4	pití
jakžtakž	jakžtakž	k6eAd1	jakžtakž
pod	pod	k7c7	pod
kontrolou	kontrola	k1gFnSc7	kontrola
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
o	o	k7c6	o
víkendech	víkend	k1gInPc6	víkend
flámoval	flámovat	k5eAaImAgMnS	flámovat
tak	tak	k9	tak
intenzivně	intenzivně	k6eAd1	intenzivně
<g/>
,	,	kIx,	,
že	že	k8xS	že
často	často	k6eAd1	často
skončil	skončit	k5eAaPmAgMnS	skončit
na	na	k7c6	na
záchytce	záchytka	k1gFnSc6	záchytka
či	či	k8xC	či
ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
láska	láska	k1gFnSc1	láska
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
později	pozdě	k6eAd2	pozdě
mu	on	k3xPp3gMnSc3	on
FrancEyE	FrancEyE	k1gMnSc3	FrancEyE
začala	začít	k5eAaPmAgFnS	začít
být	být	k5eAaImF	být
lhostejná	lhostejný	k2eAgFnSc1d1	lhostejná
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
tvorbě	tvorba	k1gFnSc6	tvorba
jí	jíst	k5eAaImIp3nS	jíst
nevěnoval	věnovat	k5eNaImAgMnS	věnovat
příliš	příliš	k6eAd1	příliš
prostoru	prostor	k1gInSc2	prostor
<g/>
,	,	kIx,	,
nejvíce	hodně	k6eAd3	hodně
se	se	k3xPyFc4	se
o	o	k7c6	o
ní	on	k3xPp3gFnSc6	on
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
v	v	k7c6	v
Básni	báseň	k1gFnSc6	báseň
pro	pro	k7c4	pro
křivozubou	křivozubý	k2eAgFnSc4d1	křivozubý
bábu	bába	k1gFnSc4	bába
napsanou	napsaný	k2eAgFnSc4d1	napsaná
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
po	po	k7c6	po
rozchodu	rozchod	k1gInSc6	rozchod
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
podpora	podpora	k1gFnSc1	podpora
FrancEyE	FrancEyE	k1gFnSc2	FrancEyE
a	a	k8xC	a
přátelství	přátelství	k1gNnSc4	přátelství
lidí	člověk	k1gMnPc2	člověk
jako	jako	k9	jako
Sam	Sam	k1gMnSc1	Sam
Cherry	Cherra	k1gFnSc2	Cherra
a	a	k8xC	a
Jon	Jon	k1gFnSc2	Jon
a	a	k8xC	a
Gypsy	gyps	k1gInPc4	gyps
Lou	Lou	k1gFnSc2	Lou
Webbovi	Webba	k1gMnSc3	Webba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ho	on	k3xPp3gMnSc4	on
dostala	dostat	k5eAaPmAgFnS	dostat
z	z	k7c2	z
dlouhého	dlouhý	k2eAgNnSc2d1	dlouhé
období	období	k1gNnSc2	období
depresí	deprese	k1gFnPc2	deprese
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
ovšem	ovšem	k9	ovšem
byly	být	k5eAaImAgFnP	být
se	s	k7c7	s
spisovatelovým	spisovatelův	k2eAgInSc7d1	spisovatelův
životem	život	k1gInSc7	život
nerozlučně	rozlučně	k6eNd1	rozlučně
spjaty	spjat	k2eAgInPc1d1	spjat
<g/>
.	.	kIx.	.
</s>
<s>
Psaní	psaní	k1gNnSc1	psaní
mu	on	k3xPp3gMnSc3	on
také	také	k9	také
pomáhalo	pomáhat	k5eAaImAgNnS	pomáhat
<g/>
,	,	kIx,	,
nejen	nejen	k6eAd1	nejen
tvorba	tvorba	k1gFnSc1	tvorba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
korespondence	korespondence	k1gFnSc1	korespondence
s	s	k7c7	s
lidmi	člověk	k1gMnPc7	člověk
<g/>
,	,	kIx,	,
kterým	který	k3yQgMnPc3	který
v	v	k7c6	v
dopisech	dopis	k1gInPc6	dopis
líčil	líčit	k5eAaImAgMnS	líčit
nebezpečné	bezpečný	k2eNgInPc4d1	nebezpečný
stavy	stav	k1gInPc4	stav
své	svůj	k3xOyFgFnSc2	svůj
psychiky	psychika	k1gFnSc2	psychika
<g/>
.	.	kIx.	.
</s>
<s>
Sebevražedné	sebevražedný	k2eAgFnSc2d1	sebevražedná
tendence	tendence	k1gFnSc2	tendence
jej	on	k3xPp3gNnSc4	on
ničily	ničit	k5eAaImAgFnP	ničit
<g/>
,	,	kIx,	,
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
ho	on	k3xPp3gInSc2	on
přítomnost	přítomnost	k1gFnSc1	přítomnost
smrti	smrt	k1gFnSc2	smrt
vzrušovala	vzrušovat	k5eAaImAgFnS	vzrušovat
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
ležel	ležet	k5eAaImAgMnS	ležet
v	v	k7c6	v
době	doba	k1gFnSc6	doba
oběda	oběd	k1gInSc2	oběd
v	v	k7c6	v
uličce	ulička	k1gFnSc6	ulička
za	za	k7c7	za
barem	bar	k1gInSc7	bar
ve	v	k7c6	v
Filadelfii	Filadelfie	k1gFnSc6	Filadelfie
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
existovala	existovat	k5eAaImAgFnS	existovat
možnost	možnost	k1gFnSc4	možnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gMnSc4	on
přejede	přejet	k5eAaPmIp3nS	přejet
couvající	couvající	k2eAgInSc1d1	couvající
náklaďák	náklaďák	k1gInSc1	náklaďák
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
měl	mít	k5eAaImAgInS	mít
vytipovanou	vytipovaný	k2eAgFnSc4d1	vytipovaná
silnici	silnice	k1gFnSc4	silnice
v	v	k7c6	v
L.A.	L.A.	k1gFnSc6	L.A.
bez	bez	k7c2	bez
pouličního	pouliční	k2eAgNnSc2d1	pouliční
osvětlení	osvětlení	k1gNnSc2	osvětlení
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
uléhal	uléhat	k5eAaImAgMnS	uléhat
v	v	k7c6	v
opilosti	opilost	k1gFnSc6	opilost
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
nejlépe	dobře	k6eAd3	dobře
postihuje	postihovat	k5eAaImIp3nS	postihovat
jeho	jeho	k3xOp3gInSc4	jeho
ambivalentní	ambivalentní	k2eAgInSc4d1	ambivalentní
citát	citát	k1gInSc4	citát
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nejlíp	dobře	k6eAd3	dobře
mi	já	k3xPp1nSc3	já
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
když	když	k8xS	když
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
cítil	cítit	k5eAaImAgMnS	cítit
nejhůř	zle	k6eAd3	zle
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Také	také	k9	také
mu	on	k3xPp3gNnSc3	on
pomohlo	pomoct	k5eAaPmAgNnS	pomoct
vydání	vydání	k1gNnSc1	vydání
básnické	básnický	k2eAgFnSc2d1	básnická
sbírky	sbírka	k1gFnSc2	sbírka
Chytá	chytat	k5eAaImIp3nS	chytat
mé	můj	k3xOp1gNnSc1	můj
srdce	srdce	k1gNnSc1	srdce
do	do	k7c2	do
dlaní	dlaň	k1gFnPc2	dlaň
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
It	It	k1gMnSc1	It
Catches	Catches	k1gMnSc1	Catches
My	my	k3xPp1nPc1	my
Heart	Heart	k1gInSc4	Heart
in	in	k?	in
Its	Its	k1gFnSc2	Its
Hands	Handsa	k1gFnPc2	Handsa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc1	jejíž
titul	titul	k1gInSc1	titul
je	být	k5eAaImIp3nS	být
převzat	převzít	k5eAaPmNgInS	převzít
z	z	k7c2	z
verše	verš	k1gInSc2	verš
básně	báseň	k1gFnSc2	báseň
od	od	k7c2	od
Robinsona	Robinson	k1gMnSc2	Robinson
Jefferse	Jefferse	k1gFnSc2	Jefferse
<g/>
.	.	kIx.	.
</s>
<s>
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
manželů	manžel	k1gMnPc2	manžel
Webbových	Webbových	k2eAgNnSc1d1	Webbových
Loujon	Loujon	k1gNnSc1	Loujon
Press	Pressa	k1gFnPc2	Pressa
ji	on	k3xPp3gFnSc4	on
vydalo	vydat	k5eAaPmAgNnS	vydat
jako	jako	k9	jako
luxusní	luxusní	k2eAgFnSc4d1	luxusní
edici	edice	k1gFnSc4	edice
na	na	k7c6	na
drahém	drahý	k2eAgInSc6d1	drahý
papíře	papír	k1gInSc6	papír
a	a	k8xC	a
v	v	k7c6	v
korkové	korkový	k2eAgFnSc6d1	korková
vazbě	vazba	k1gFnSc6	vazba
<g/>
.	.	kIx.	.
</s>
<s>
Bukowski	Bukowski	k6eAd1	Bukowski
navíc	navíc	k6eAd1	navíc
podepsal	podepsat	k5eAaPmAgMnS	podepsat
každý	každý	k3xTgInSc4	každý
výtisk	výtisk	k1gInSc4	výtisk
(	(	kIx(	(
<g/>
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
povídce	povídka	k1gFnSc6	povídka
popisuje	popisovat	k5eAaImIp3nS	popisovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
stránek	stránka	k1gFnPc2	stránka
bylo	být	k5eAaImAgNnS	být
tolik	tolik	k4yIc1	tolik
<g/>
,	,	kIx,	,
až	až	k9	až
začal	začít	k5eAaPmAgInS	začít
svůj	svůj	k3xOyFgInSc4	svůj
podpis	podpis	k1gInSc4	podpis
nenávidět	nenávidět	k5eAaImF	nenávidět
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
narodila	narodit	k5eAaPmAgFnS	narodit
dcera	dcera	k1gFnSc1	dcera
Marina	Marina	k1gFnSc1	Marina
Louise	Louis	k1gMnSc2	Louis
Bukowski	Bukowsk	k1gFnSc2	Bukowsk
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
Marina	Marina	k1gFnSc1	Marina
vybrala	vybrat	k5eAaPmAgFnS	vybrat
FrancEyE	FrancEyE	k1gFnSc4	FrancEyE
podle	podle	k7c2	podle
jedné	jeden	k4xCgFnSc2	jeden
kurtizány	kurtizána	k1gFnSc2	kurtizána
<g/>
,	,	kIx,	,
o	o	k7c6	o
níž	jenž	k3xRgFnSc6	jenž
zrovna	zrovna	k6eAd1	zrovna
četla	číst	k5eAaImAgFnS	číst
a	a	k8xC	a
Louise	Louis	k1gMnSc2	Louis
dostala	dostat	k5eAaPmAgFnS	dostat
na	na	k7c6	na
počest	počest	k1gFnSc6	počest
Gypsy	gyps	k1gInPc4	gyps
Lou	Lou	k1gFnSc2	Lou
Webbové	Webbová	k1gFnSc3	Webbová
<g/>
.	.	kIx.	.
</s>
<s>
Marina	Marina	k1gFnSc1	Marina
byla	být	k5eAaImAgFnS	být
jediným	jediný	k2eAgNnSc7d1	jediné
dítětem	dítě	k1gNnSc7	dítě
Charlese	Charles	k1gMnSc2	Charles
Bukowského	Bukowský	k1gMnSc2	Bukowský
a	a	k8xC	a
ten	ten	k3xDgMnSc1	ten
jí	jíst	k5eAaImIp3nS	jíst
dokázal	dokázat	k5eAaPmAgMnS	dokázat
věnovat	věnovat	k5eAaImF	věnovat
velkou	velký	k2eAgFnSc4d1	velká
péči	péče	k1gFnSc4	péče
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
později	pozdě	k6eAd2	pozdě
Marina	Marina	k1gFnSc1	Marina
vzpomíná	vzpomínat	k5eAaImIp3nS	vzpomínat
<g/>
,	,	kIx,	,
vždy	vždy	k6eAd1	vždy
se	se	k3xPyFc4	se
mohla	moct	k5eAaImAgFnS	moct
na	na	k7c4	na
svého	svůj	k3xOyFgMnSc4	svůj
otce	otec	k1gMnSc4	otec
obrátit	obrátit	k5eAaPmF	obrátit
s	s	k7c7	s
jistotou	jistota	k1gFnSc7	jistota
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
neodmítne	odmítnout	k5eNaPmIp3nS	odmítnout
a	a	k8xC	a
postará	postarat	k5eAaPmIp3nS	postarat
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Dával	dávat	k5eAaImAgMnS	dávat
jí	jíst	k5eAaImIp3nS	jíst
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
má	mít	k5eAaImIp3nS	mít
rád	rád	k2eAgMnSc1d1	rád
<g/>
,	,	kIx,	,
a	a	k8xC	a
díky	díky	k7c3	díky
tomuto	tento	k3xDgInSc3	tento
pocitu	pocit	k1gInSc3	pocit
nezažila	zažít	k5eNaPmAgFnS	zažít
podobnou	podobný	k2eAgFnSc4d1	podobná
hrůzu	hrůza	k1gFnSc4	hrůza
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
si	se	k3xPyFc3	se
prošel	projít	k5eAaPmAgMnS	projít
její	její	k3xOp3gMnSc1	její
otec	otec	k1gMnSc1	otec
<g/>
.	.	kIx.	.
</s>
<s>
Charles	Charles	k1gMnSc1	Charles
požádal	požádat	k5eAaPmAgMnS	požádat
matku	matka	k1gFnSc4	matka
své	svůj	k3xOyFgFnSc2	svůj
dcery	dcera	k1gFnSc2	dcera
o	o	k7c4	o
ruku	ruka	k1gFnSc4	ruka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ta	ten	k3xDgFnSc1	ten
ho	on	k3xPp3gNnSc4	on
se	s	k7c7	s
zdůvodněním	zdůvodnění	k1gNnSc7	zdůvodnění
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
už	už	k9	už
nechce	chtít	k5eNaImIp3nS	chtít
nikoho	nikdo	k3yNnSc4	nikdo
brát	brát	k5eAaImF	brát
<g/>
,	,	kIx,	,
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Domluvili	domluvit	k5eAaPmAgMnP	domluvit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
budou	být	k5eAaImBp3nP	být
žít	žít	k5eAaImF	žít
jako	jako	k9	jako
rodina	rodina	k1gFnSc1	rodina
a	a	k8xC	a
Bukowski	Bukowski	k1gNnSc1	Bukowski
bude	být	k5eAaImBp3nS	být
platit	platit	k5eAaImF	platit
účty	účet	k1gInPc4	účet
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
vztah	vztah	k1gInSc1	vztah
se	se	k3xPyFc4	se
nevyvíjel	vyvíjet	k5eNaImAgInS	vyvíjet
<g/>
.	.	kIx.	.
</s>
<s>
Bukowski	Bukowski	k6eAd1	Bukowski
opovrhoval	opovrhovat	k5eAaImAgMnS	opovrhovat
přáteli	přítel	k1gMnPc7	přítel
Frances	Francesa	k1gFnPc2	Francesa
z	z	k7c2	z
básnického	básnický	k2eAgInSc2d1	básnický
semináře	seminář	k1gInSc2	seminář
<g/>
,	,	kIx,	,
dost	dost	k6eAd1	dost
pil	pít	k5eAaImAgMnS	pít
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
jeho	jeho	k3xOp3gFnPc1	jeho
noční	noční	k2eAgFnPc1d1	noční
směny	směna	k1gFnPc1	směna
narušovaly	narušovat	k5eAaImAgFnP	narušovat
soužití	soužití	k1gNnSc4	soužití
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
se	se	k3xPyFc4	se
rozešli	rozejít	k5eAaPmAgMnP	rozejít
<g/>
,	,	kIx,	,
Bukowski	Bukowske	k1gFnSc4	Bukowske
pomohl	pomoct	k5eAaPmAgMnS	pomoct
sehnat	sehnat	k5eAaPmF	sehnat
podnájem	podnájem	k1gInSc4	podnájem
a	a	k8xC	a
do	do	k7c2	do
budoucna	budoucno	k1gNnSc2	budoucno
pomáhal	pomáhat	k5eAaImAgInS	pomáhat
i	i	k9	i
finančně	finančně	k6eAd1	finančně
<g/>
.	.	kIx.	.
</s>
<s>
Johnu	John	k1gMnSc3	John
Martinovi	Martin	k1gMnSc3	Martin
Bukowského	Bukowského	k2eAgFnSc2d1	Bukowského
poezie	poezie	k1gFnSc2	poezie
doslova	doslova	k6eAd1	doslova
změnila	změnit	k5eAaPmAgFnS	změnit
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svojí	svůj	k3xOyFgFnSc7	svůj
ženou	žena	k1gFnSc7	žena
Barbarou	Barbara	k1gFnSc7	Barbara
založili	založit	k5eAaPmAgMnP	založit
nakladatelství	nakladatelství	k1gNnSc4	nakladatelství
Black	Blacka	k1gFnPc2	Blacka
Sparrow	Sparrow	k1gMnPc2	Sparrow
Press	Pressa	k1gFnPc2	Pressa
a	a	k8xC	a
začali	začít	k5eAaPmAgMnP	začít
tisknout	tisknout	k5eAaImF	tisknout
opomíjené	opomíjený	k2eAgMnPc4d1	opomíjený
spisovatele	spisovatel	k1gMnPc4	spisovatel
<g/>
,	,	kIx,	,
Bukowského	Bukowského	k2eAgNnPc4d1	Bukowského
obzvlášť	obzvlášť	k6eAd1	obzvlášť
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Johna	John	k1gMnSc4	John
Martina	Martin	k1gMnSc4	Martin
byl	být	k5eAaImAgMnS	být
snad	snad	k9	snad
nejoriginálnějším	originální	k2eAgMnSc7d3	nejoriginálnější
básníkem	básník	k1gMnSc7	básník
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jeho	jeho	k3xOp3gInPc1	jeho
verše	verš	k1gInPc1	verš
byly	být	k5eAaImAgInP	být
zcela	zcela	k6eAd1	zcela
jasné	jasný	k2eAgNnSc1d1	jasné
a	a	k8xC	a
přímočaré	přímočarý	k2eAgNnSc1d1	přímočaré
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
některých	některý	k3yIgMnPc2	některý
symbolistů	symbolista	k1gMnPc2	symbolista
či	či	k8xC	či
surrealistů	surrealista	k1gMnPc2	surrealista
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
také	také	k6eAd1	také
vydával	vydávat	k5eAaPmAgInS	vydávat
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
věděl	vědět	k5eAaImAgMnS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
Bukowski	Bukowski	k1gNnSc1	Bukowski
rád	rád	k6eAd1	rád
kreslí	kreslit	k5eAaImIp3nS	kreslit
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
jeho	jeho	k3xOp3gFnPc7	jeho
ilustracemi	ilustrace	k1gFnPc7	ilustrace
doplňoval	doplňovat	k5eAaImAgInS	doplňovat
básnické	básnický	k2eAgFnPc4d1	básnická
knížečky	knížečka	k1gFnPc4	knížečka
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	on	k3xPp3gMnPc4	on
zhodnocovalo	zhodnocovat	k5eAaImAgNnS	zhodnocovat
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
knihou	kniha	k1gFnSc7	kniha
vydanou	vydaný	k2eAgFnSc7d1	vydaná
v	v	k7c4	v
Black	Black	k1gInSc4	Black
Sparrow	Sparrow	k1gFnSc1	Sparrow
Press	Pressa	k1gFnPc2	Pressa
byla	být	k5eAaImAgFnS	být
sbírka	sbírka	k1gFnSc1	sbírka
At	At	k1gMnSc1	At
Terror	Terror	k1gMnSc1	Terror
Street	Street	k1gMnSc1	Street
and	and	k?	and
Agony	agon	k1gInPc1	agon
Way	Way	k1gFnSc1	Way
<g/>
.	.	kIx.	.
</s>
<s>
Bukowski	Bukowski	k6eAd1	Bukowski
prodělal	prodělat	k5eAaPmAgMnS	prodělat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
operaci	operace	k1gFnSc4	operace
hemoroidů	hemoroidy	k1gInPc2	hemoroidy
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
popsal	popsat	k5eAaPmAgMnS	popsat
v	v	k7c6	v
povídce	povídka	k1gFnSc6	povídka
Všechny	všechen	k3xTgFnPc1	všechen
řitě	řiť	k1gFnPc1	řiť
světa	svět	k1gInSc2	svět
i	i	k8xC	i
ta	ten	k3xDgFnSc1	ten
má	mít	k5eAaImIp3nS	mít
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
All	All	k1gMnSc1	All
the	the	k?	the
Assholes	Assholes	k1gMnSc1	Assholes
in	in	k?	in
the	the	k?	the
World	World	k1gInSc1	World
and	and	k?	and
Mine	minout	k5eAaImIp3nS	minout
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
se	se	k3xPyFc4	se
zmínil	zmínit	k5eAaPmAgInS	zmínit
Dougu	Doug	k1gMnSc3	Doug
Blazekovi	Blazeek	k1gMnSc3	Blazeek
<g/>
,	,	kIx,	,
že	že	k8xS	že
pracuje	pracovat	k5eAaImIp3nS	pracovat
na	na	k7c6	na
románu	román	k1gInSc6	román
Jak	jak	k6eAd1	jak
milují	milovat	k5eAaImIp3nP	milovat
mrtví	mrtvý	k2eAgMnPc1d1	mrtvý
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc4	jehož
časovou	časový	k2eAgFnSc4d1	časová
osu	osa	k1gFnSc4	osa
tvoří	tvořit	k5eAaImIp3nP	tvořit
období	období	k1gNnSc4	období
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
padesátých	padesátý	k4xOgNnPc2	padesátý
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bydlel	bydlet	k5eAaImAgMnS	bydlet
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
zchátralém	zchátralý	k2eAgInSc6d1	zchátralý
hotelu	hotel	k1gInSc6	hotel
<g/>
.	.	kIx.	.
</s>
<s>
Obsažená	obsažený	k2eAgNnPc4d1	obsažené
témata	téma	k1gNnPc4	téma
nebyla	být	k5eNaImAgFnS	být
překvapující	překvapující	k2eAgFnSc1d1	překvapující
-	-	kIx~	-
hladovění	hladovění	k1gNnPc1	hladovění
<g/>
,	,	kIx,	,
opíjení	opíjení	k1gNnPc1	opíjení
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
souložení	souložení	k1gNnSc4	souložení
a	a	k8xC	a
vždypřítomné	vždypřítomný	k2eAgNnSc4d1	vždypřítomný
šílenství	šílenství	k1gNnSc4	šílenství
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
nevyšel	vyjít	k5eNaPmAgInS	vyjít
a	a	k8xC	a
většinu	většina	k1gFnSc4	většina
materiálu	materiál	k1gInSc2	materiál
Bukowski	Bukowsk	k1gFnSc2	Bukowsk
použil	použít	k5eAaPmAgMnS	použít
jinde	jinde	k6eAd1	jinde
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
undergroundové	undergroundový	k2eAgFnPc4d1	undergroundová
noviny	novina	k1gFnPc4	novina
Open	Open	k1gInSc4	Open
City	city	k1gNnSc1	city
založené	založený	k2eAgNnSc1d1	založené
Johnem	John	k1gMnSc7	John
Bryanem	Bryan	k1gMnSc7	Bryan
psal	psát	k5eAaImAgMnS	psát
každý	každý	k3xTgInSc4	každý
týden	týden	k1gInSc4	týden
sloupek	sloupek	k1gInSc1	sloupek
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
Zápisky	zápiska	k1gFnSc2	zápiska
starého	starý	k2eAgMnSc2d1	starý
prasáka	prasák	k1gMnSc2	prasák
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
knižně	knižně	k6eAd1	knižně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dostal	dostat	k5eAaPmAgMnS	dostat
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
absolutní	absolutní	k2eAgFnSc1d1	absolutní
volnost	volnost	k1gFnSc1	volnost
a	a	k8xC	a
záhy	záhy	k6eAd1	záhy
se	se	k3xPyFc4	se
Zápisky	zápiska	k1gFnPc1	zápiska
staly	stát	k5eAaPmAgFnP	stát
velmi	velmi	k6eAd1	velmi
oblíbenými	oblíbený	k2eAgFnPc7d1	oblíbená
po	po	k7c6	po
celém	celý	k2eAgNnSc6d1	celé
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Díky	dík	k1gInPc1	dík
Open	Opena	k1gFnPc2	Opena
City	city	k1gNnSc2	city
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
i	i	k9	i
s	s	k7c7	s
řadou	řada	k1gFnSc7	řada
beatniků	beatnik	k1gMnPc2	beatnik
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
nejvíc	nejvíc	k6eAd1	nejvíc
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
zapůsobil	zapůsobit	k5eAaPmAgInS	zapůsobit
Neal	Neal	k1gInSc1	Neal
Cassady	Cassada	k1gFnSc2	Cassada
<g/>
.	.	kIx.	.
</s>
<s>
Open	Open	k1gNnSc1	Open
City	City	k1gFnSc2	City
zaniklo	zaniknout	k5eAaPmAgNnS	zaniknout
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
byl	být	k5eAaImAgMnS	být
John	John	k1gMnSc1	John
Bryan	Bryan	k1gMnSc1	Bryan
zatčen	zatknout	k5eAaPmNgMnS	zatknout
pro	pro	k7c4	pro
obvinění	obvinění	k1gNnSc4	obvinění
z	z	k7c2	z
obscénnosti	obscénnost	k1gFnSc2	obscénnost
kvůli	kvůli	k7c3	kvůli
zveřejnění	zveřejnění	k1gNnSc3	zveřejnění
povídky	povídka	k1gFnSc2	povídka
Jacka	Jacek	k1gMnSc2	Jacek
Michelinea	Michelineus	k1gMnSc2	Michelineus
Kostnatej	Kostnatej	k?	Kostnatej
Dynamit	dynamit	k1gInSc1	dynamit
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
Skinny	Skinna	k1gFnSc2	Skinna
Dynamite	dynamit	k1gInSc5	dynamit
<g/>
)	)	kIx)	)
o	o	k7c6	o
sexuálních	sexuální	k2eAgFnPc6d1	sexuální
radovánkách	radovánka	k1gFnPc6	radovánka
nezletilé	nezletilé	k2eAgFnPc1d1	nezletilé
holčičky	holčička	k1gFnPc1	holčička
<g/>
;	;	kIx,	;
povídka	povídka	k1gFnSc1	povídka
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
literární	literární	k2eAgFnSc6d1	literární
příloze	příloha	k1gFnSc6	příloha
editované	editovaný	k2eAgInPc1d1	editovaný
právě	právě	k6eAd1	právě
Bukowským	Bukowský	k1gMnSc7	Bukowský
<g/>
.	.	kIx.	.
</s>
<s>
Obvinění	obvinění	k1gNnSc1	obvinění
bylo	být	k5eAaImAgNnS	být
nakonec	nakonec	k6eAd1	nakonec
staženo	stáhnout	k5eAaPmNgNnS	stáhnout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zániku	zánik	k1gInSc2	zánik
už	už	k6eAd1	už
nešlo	jít	k5eNaImAgNnS	jít
zabránit	zabránit	k5eAaPmF	zabránit
<g/>
.	.	kIx.	.
</s>
<s>
Bukowski	Bukowski	k6eAd1	Bukowski
tomu	ten	k3xDgNnSc3	ten
navíc	navíc	k6eAd1	navíc
nasadil	nasadit	k5eAaPmAgMnS	nasadit
korunu	koruna	k1gFnSc4	koruna
<g/>
,	,	kIx,	,
když	když	k8xS	když
o	o	k7c6	o
konci	konec	k1gInSc6	konec
novin	novina	k1gFnPc2	novina
napsal	napsat	k5eAaPmAgInS	napsat
nevybíravou	vybíravý	k2eNgFnSc4d1	nevybíravá
parodii	parodie	k1gFnSc4	parodie
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
otiskl	otisknout	k5eAaPmAgInS	otisknout
časopis	časopis	k1gInSc1	časopis
Evergreen	evergreen	k1gInSc1	evergreen
(	(	kIx(	(
<g/>
parodie	parodie	k1gFnSc1	parodie
má	mít	k5eAaImIp3nS	mít
název	název	k1gInSc1	název
Zrod	zrod	k1gInSc1	zrod
<g/>
,	,	kIx,	,
život	život	k1gInSc1	život
a	a	k8xC	a
smrt	smrt	k1gFnSc1	smrt
undergroundového	undergroundový	k2eAgInSc2d1	undergroundový
plátku	plátek	k1gInSc2	plátek
<g/>
,	,	kIx,	,
angl.	angl.	k?	angl.
The	The	k1gMnSc1	The
Birth	Birth	k1gMnSc1	Birth
<g/>
,	,	kIx,	,
Life	Life	k1gFnSc1	Life
and	and	k?	and
Death	Death	k1gInSc1	Death
of	of	k?	of
an	an	k?	an
Underground	underground	k1gInSc1	underground
Newspaper	Newspapra	k1gFnPc2	Newspapra
a	a	k8xC	a
lze	lze	k6eAd1	lze
ji	on	k3xPp3gFnSc4	on
nalézt	nalézt	k5eAaPmF	nalézt
ve	v	k7c6	v
sbírce	sbírka	k1gFnSc6	sbírka
Nejkrásnější	krásný	k2eAgFnSc1d3	nejkrásnější
ženská	ženská	k1gFnSc1	ženská
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
angl.	angl.	k?	angl.
The	The	k1gFnSc1	The
most	most	k1gInSc1	most
Beautiful	Beautiful	k1gInSc1	Beautiful
Woman	Woman	k1gInSc1	Woman
in	in	k?	in
Town	Town	k1gMnSc1	Town
&	&	k?	&
Other	Other	k1gMnSc1	Other
Stories	Stories	k1gMnSc1	Stories
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
John	John	k1gMnSc1	John
Bryan	Bryan	k1gMnSc1	Bryan
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
později	pozdě	k6eAd2	pozdě
napsal	napsat	k5eAaBmAgMnS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nejvíc	nejvíc	k6eAd1	nejvíc
mě	já	k3xPp1nSc4	já
zranilo	zranit	k5eAaPmAgNnS	zranit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsem	být	k5eAaImIp1nS	být
Bukowského	Bukowský	k1gMnSc4	Bukowský
pokládal	pokládat	k5eAaImAgMnS	pokládat
za	za	k7c4	za
přítele	přítel	k1gMnSc4	přítel
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jsem	být	k5eAaImIp1nS	být
bojoval	bojovat	k5eAaImAgMnS	bojovat
o	o	k7c4	o
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
s	s	k7c7	s
čím	čí	k3xOyRgNnSc7	čí
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
s	s	k7c7	s
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
důvěrou	důvěra	k1gFnSc7	důvěra
svěřil	svěřit	k5eAaPmAgInS	svěřit
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
ta	ten	k3xDgNnPc4	ten
nejstrašnější	strašný	k2eAgNnPc4d3	nejstrašnější
tajemství	tajemství	k1gNnPc4	tajemství
a	a	k8xC	a
mé	můj	k3xOp1gFnSc2	můj
nejčernější	černý	k2eAgFnSc2d3	nejčernější
noční	noční	k2eAgFnSc2d1	noční
můry	můra	k1gFnSc2	můra
<g/>
.	.	kIx.	.
</s>
<s>
Potřeboval	potřebovat	k5eAaImAgMnS	potřebovat
jsem	být	k5eAaImIp1nS	být
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
jsem	být	k5eAaImIp1nS	být
na	na	k7c6	na
pokraji	pokraj	k1gInSc6	pokraj
sebezničení	sebezničení	k1gNnSc2	sebezničení
<g/>
.	.	kIx.	.
</s>
<s>
Bukowski	Bukowski	k6eAd1	Bukowski
se	se	k3xPyFc4	se
smál	smát	k5eAaImAgMnS	smát
a	a	k8xC	a
poškleboval	pošklebovat	k5eAaImAgMnS	pošklebovat
a	a	k8xC	a
vytroubil	vytroubit	k5eAaPmAgMnS	vytroubit
to	ten	k3xDgNnSc4	ten
do	do	k7c2	do
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Dospěl	dochvít	k5eAaPmAgMnS	dochvít
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
Bukowski	Bukowski	k1gNnSc1	Bukowski
byl	být	k5eAaImAgMnS	být
vynikající	vynikající	k2eAgMnSc1d1	vynikající
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
všivá	všivý	k2eAgFnSc1d1	všivá
lidská	lidský	k2eAgFnSc1d1	lidská
bytost	bytost	k1gFnSc1	bytost
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Charles	Charles	k1gMnSc1	Charles
Bukowski	Bukowsk	k1gFnSc2	Bukowsk
měl	mít	k5eAaImAgMnS	mít
zkrátka	zkrátka	k6eAd1	zkrátka
talent	talent	k1gInSc4	talent
na	na	k7c4	na
likvidování	likvidování	k1gNnSc4	likvidování
přátelství	přátelství	k1gNnSc2	přátelství
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ve	v	k7c4	v
spojení	spojení	k1gNnSc4	spojení
s	s	k7c7	s
jeho	jeho	k3xOp3gFnPc7	jeho
literárními	literární	k2eAgFnPc7d1	literární
schopnostmi	schopnost	k1gFnPc7	schopnost
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
často	často	k6eAd1	často
dělal	dělat	k5eAaImAgInS	dělat
nepřístupného	přístupný	k2eNgMnSc4d1	nepřístupný
a	a	k8xC	a
mstivého	mstivý	k2eAgMnSc4d1	mstivý
člověka	člověk	k1gMnSc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
své	svůj	k3xOyFgFnSc3	svůj
minimální	minimální	k2eAgFnSc3d1	minimální
empatické	empatický	k2eAgFnSc3d1	empatická
schopnosti	schopnost	k1gFnSc3	schopnost
nechápal	chápat	k5eNaImAgMnS	chápat
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
lidí	člověk	k1gMnPc2	člověk
tolik	tolik	k6eAd1	tolik
dotýká	dotýkat	k5eAaImIp3nS	dotýkat
<g/>
,	,	kIx,	,
když	když	k8xS	když
píše	psát	k5eAaImIp3nS	psát
podobné	podobný	k2eAgFnPc4d1	podobná
věci	věc	k1gFnPc4	věc
i	i	k9	i
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Absolutně	absolutně	k6eAd1	absolutně
si	se	k3xPyFc3	se
neuvědomoval	uvědomovat	k5eNaImAgMnS	uvědomovat
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
znamená	znamenat	k5eAaImIp3nS	znamenat
soukromí	soukromí	k1gNnSc1	soukromí
<g/>
.	.	kIx.	.
</s>
<s>
Skutečně	skutečně	k6eAd1	skutečně
velký	velký	k2eAgInSc4d1	velký
úspěch	úspěch	k1gInSc4	úspěch
mu	on	k3xPp3gMnSc3	on
zaručil	zaručit	k5eAaPmAgMnS	zaručit
až	až	k6eAd1	až
první	první	k4xOgInSc4	první
román	román	k1gInSc4	román
napsaný	napsaný	k2eAgInSc4d1	napsaný
během	během	k7c2	během
20	[number]	k4	20
dní	den	k1gInPc2	den
-	-	kIx~	-
Poštovní	poštovní	k2eAgInSc1d1	poštovní
úřad	úřad	k1gInSc1	úřad
o	o	k7c6	o
zážitcích	zážitek	k1gInPc6	zážitek
z	z	k7c2	z
protivného	protivný	k2eAgNnSc2d1	protivné
zaměstnání	zaměstnání	k1gNnSc2	zaměstnání
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
dočkal	dočkat	k5eAaPmAgMnS	dočkat
příznivého	příznivý	k2eAgInSc2d1	příznivý
ohlasu	ohlas	k1gInSc2	ohlas
<g/>
.	.	kIx.	.
</s>
<s>
Předcházela	předcházet	k5eAaImAgFnS	předcházet
mu	on	k3xPp3gNnSc3	on
dohoda	dohoda	k1gFnSc1	dohoda
s	s	k7c7	s
Johnem	John	k1gMnSc7	John
Martinem	Martin	k1gMnSc7	Martin
<g/>
,	,	kIx,	,
vydavatelem	vydavatel	k1gMnSc7	vydavatel
Black	Blacka	k1gFnPc2	Blacka
Sparrow	Sparrow	k1gMnSc7	Sparrow
Press	Press	k1gInSc4	Press
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
bude	být	k5eAaImBp3nS	být
vyplácet	vyplácet	k5eAaImF	vyplácet
měsíčně	měsíčně	k6eAd1	měsíčně
100	[number]	k4	100
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
pokryjí	pokrýt	k5eAaPmIp3nP	pokrýt
Bukowského	Bukowského	k2eAgInPc1d1	Bukowského
náklady	náklad	k1gInPc1	náklad
na	na	k7c4	na
živobytí	živobytí	k1gNnSc4	živobytí
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
bude	být	k5eAaImBp3nS	být
psát	psát	k5eAaImF	psát
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rostoucí	rostoucí	k2eAgFnSc7d1	rostoucí
prodejností	prodejnost	k1gFnSc7	prodejnost
jeho	jeho	k3xOp3gFnPc2	jeho
knih	kniha	k1gFnPc2	kniha
tento	tento	k3xDgInSc4	tento
honorář	honorář	k1gInSc4	honorář
stoupal	stoupat	k5eAaImAgInS	stoupat
<g/>
.	.	kIx.	.
</s>
<s>
Bukowského	Bukowského	k2eAgFnSc1d1	Bukowského
verze	verze	k1gFnSc1	verze
byla	být	k5eAaImAgFnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
pošty	pošta	k1gFnSc2	pošta
odešel	odejít	k5eAaPmAgMnS	odejít
sám	sám	k3xTgMnSc1	sám
(	(	kIx(	(
<g/>
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
podle	podle	k7c2	podle
poštovních	poštovní	k2eAgInPc2d1	poštovní
záznamů	záznam	k1gInPc2	záznam
mu	on	k3xPp3gMnSc3	on
hrozila	hrozit	k5eAaImAgFnS	hrozit
výpověď	výpověď	k1gFnSc4	výpověď
kvůli	kvůli	k7c3	kvůli
častým	častý	k2eAgFnPc3d1	častá
absencím	absence	k1gFnPc3	absence
<g/>
.	.	kIx.	.
</s>
<s>
Jisté	jistý	k2eAgNnSc1d1	jisté
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
od	od	k7c2	od
tohoto	tento	k3xDgInSc2	tento
okamžiku	okamžik	k1gInSc2	okamžik
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
živit	živit	k5eAaImF	živit
psaním	psaní	k1gNnSc7	psaní
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
konečně	konečně	k6eAd1	konečně
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
po	po	k7c6	po
čem	co	k3yQnSc6	co
toužil	toužit	k5eAaImAgMnS	toužit
<g/>
.	.	kIx.	.
</s>
<s>
Poštovní	poštovní	k2eAgInSc1d1	poštovní
úřad	úřad	k1gInSc1	úřad
je	být	k5eAaImIp3nS	být
napsán	napsat	k5eAaBmNgInS	napsat
přímým	přímý	k2eAgInSc7d1	přímý
stylem	styl	k1gInSc7	styl
(	(	kIx(	(
<g/>
po	po	k7c6	po
vzoru	vzor	k1gInSc6	vzor
Fanteho	Fante	k1gMnSc2	Fante
a	a	k8xC	a
Hemingwaye	Hemingway	k1gMnSc2	Hemingway
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Hemingwayových	Hemingwayův	k2eAgFnPc2d1	Hemingwayova
knih	kniha	k1gFnPc2	kniha
je	být	k5eAaImIp3nS	být
okořeněn	okořenit	k5eAaPmNgInS	okořenit
i	i	k9	i
humorem	humor	k1gInSc7	humor
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
němuž	jenž	k3xRgMnSc3	jenž
dokáže	dokázat	k5eAaPmIp3nS	dokázat
autor	autor	k1gMnSc1	autor
zlehčit	zlehčit	k5eAaPmF	zlehčit
vážnou	vážný	k2eAgFnSc4d1	vážná
situaci	situace	k1gFnSc4	situace
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
na	na	k7c4	na
čtenáře	čtenář	k1gMnPc4	čtenář
platilo	platit	k5eAaImAgNnS	platit
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
dokazují	dokazovat	k5eAaImIp3nP	dokazovat
úspěchy	úspěch	k1gInPc4	úspěch
tohoto	tento	k3xDgInSc2	tento
i	i	k8xC	i
dalších	další	k2eAgInPc2d1	další
románů	román	k1gInPc2	román
<g/>
.	.	kIx.	.
</s>
<s>
Básník	básník	k1gMnSc1	básník
a	a	k8xC	a
zakladatel	zakladatel	k1gMnSc1	zakladatel
City	City	k1gFnSc2	City
Lights	Lightsa	k1gFnPc2	Lightsa
Press	Pressa	k1gFnPc2	Pressa
Lawrence	Lawrenec	k1gMnSc2	Lawrenec
Ferlinghetti	Ferlinghetti	k1gNnSc2	Ferlinghetti
měl	mít	k5eAaImAgMnS	mít
také	také	k9	také
zájem	zájem	k1gInSc4	zájem
vydávat	vydávat	k5eAaPmF	vydávat
Bukowského	Bukowského	k2eAgFnPc4d1	Bukowského
knihy	kniha	k1gFnPc4	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Chystal	chystat	k5eAaImAgInS	chystat
se	se	k3xPyFc4	se
Bukowského	Bukowský	k1gMnSc2	Bukowský
oslovit	oslovit	k5eAaPmF	oslovit
právě	právě	k9	právě
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
do	do	k7c2	do
hry	hra	k1gFnSc2	hra
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
John	John	k1gMnSc1	John
Martin	Martin	k1gMnSc1	Martin
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
Black	Black	k1gMnSc1	Black
Sparrow	Sparrow	k1gFnPc2	Sparrow
Press	Pressa	k1gFnPc2	Pressa
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
Johnu	John	k1gMnSc3	John
Martinovi	Martin	k1gMnSc3	Martin
se	se	k3xPyFc4	se
některé	některý	k3yIgNnSc1	některý
příliš	příliš	k6eAd1	příliš
provokativní	provokativní	k2eAgInPc1d1	provokativní
texty	text	k1gInPc1	text
nezamlouvaly	zamlouvat	k5eNaImAgInP	zamlouvat
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
i	i	k9	i
něco	něco	k3yInSc4	něco
pro	pro	k7c4	pro
City	City	k1gFnSc4	City
Lights	Lightsa	k1gFnPc2	Lightsa
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
autor	autor	k1gMnSc1	autor
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
poznamenává	poznamenávat	k5eAaImIp3nS	poznamenávat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Všechny	všechen	k3xTgFnPc1	všechen
moje	můj	k3xOp1gFnPc1	můj
knížky	knížka	k1gFnPc1	knížka
patří	patřit	k5eAaImIp3nP	patřit
Black	Black	k1gInSc4	Black
Sparrow	Sparrow	k1gMnPc3	Sparrow
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
jenom	jenom	k9	jenom
občas	občas	k6eAd1	občas
<g/>
,	,	kIx,	,
když	když	k8xS	když
Martin	Martin	k1gMnSc1	Martin
ztratí	ztratit	k5eAaPmIp3nS	ztratit
soudnost	soudnost	k1gFnSc4	soudnost
<g/>
,	,	kIx,	,
usměje	usmát	k5eAaPmIp3nS	usmát
se	se	k3xPyFc4	se
štěstí	štěstí	k1gNnSc4	štěstí
na	na	k7c6	na
City	city	k1gNnSc6	city
Lights	Lightsa	k1gFnPc2	Lightsa
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
r.	r.	kA	r.
1972	[number]	k4	1972
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
City	City	k1gFnSc6	City
Lights	Lightsa	k1gFnPc2	Lightsa
Press	Pressa	k1gFnPc2	Pressa
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
kniha	kniha	k1gFnSc1	kniha
povídek	povídka	k1gFnPc2	povídka
Erection	Erection	k1gInSc1	Erection
<g/>
,	,	kIx,	,
Ejaculation	Ejaculation	k1gInSc1	Ejaculation
<g/>
,	,	kIx,	,
Exhibition	Exhibition	k1gInSc1	Exhibition
and	and	k?	and
General	General	k1gFnSc2	General
Tales	Tales	k1gMnSc1	Tales
of	of	k?	of
Ordinary	Ordinara	k1gFnSc2	Ordinara
Madness	Madnessa	k1gFnPc2	Madnessa
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
do	do	k7c2	do
dvou	dva	k4xCgInPc2	dva
titulů	titul	k1gInPc2	titul
-	-	kIx~	-
viz	vidět	k5eAaImRp2nS	vidět
kapitola	kapitola	k1gFnSc1	kapitola
Dílo	dílo	k1gNnSc4	dílo
-	-	kIx~	-
Povídkové	povídkový	k2eAgFnSc2d1	povídková
sbírky	sbírka	k1gFnSc2	sbírka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ferlinghetti	Ferlinghetti	k1gNnSc1	Ferlinghetti
pozval	pozvat	k5eAaPmAgInS	pozvat
v	v	k7c6	v
r.	r.	kA	r.
1973	[number]	k4	1973
Bukowského	Bukowského	k2eAgInSc4d1	Bukowského
na	na	k7c4	na
čtení	čtení	k1gNnSc4	čtení
poezie	poezie	k1gFnSc2	poezie
do	do	k7c2	do
San	San	k1gFnSc2	San
Francisca	Francisc	k1gInSc2	Francisc
<g/>
.	.	kIx.	.
</s>
<s>
Bukowski	Bukowski	k6eAd1	Bukowski
začíná	začínat	k5eAaImIp3nS	začínat
s	s	k7c7	s
recitací	recitace	k1gFnSc7	recitace
poezie	poezie	k1gFnSc2	poezie
na	na	k7c6	na
univerzitách	univerzita	k1gFnPc6	univerzita
a	a	k8xC	a
klubech	klub	k1gInPc6	klub
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
mu	on	k3xPp3gMnSc3	on
také	také	k9	také
vynáší	vynášet	k5eAaImIp3nS	vynášet
peníze	peníz	k1gInPc4	peníz
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
býval	bývat	k5eAaImAgInS	bývat
velmi	velmi	k6eAd1	velmi
nervózní	nervózní	k2eAgMnSc1d1	nervózní
a	a	k8xC	a
zakoktával	zakoktávat	k5eAaImAgMnS	zakoktávat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
tuto	tento	k3xDgFnSc4	tento
trému	tréma	k1gFnSc4	tréma
zaháněl	zahánět	k5eAaImAgMnS	zahánět
jak	jak	k8xS	jak
jinak	jinak	k6eAd1	jinak
než	než	k8xS	než
posílením	posílení	k1gNnSc7	posílení
se	se	k3xPyFc4	se
alkoholem	alkohol	k1gInSc7	alkohol
před	před	k7c7	před
vystoupením	vystoupení	k1gNnSc7	vystoupení
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
psal	psát	k5eAaImAgMnS	psát
pornografické	pornografický	k2eAgFnPc4d1	pornografická
povídky	povídka	k1gFnPc4	povídka
pro	pro	k7c4	pro
erotické	erotický	k2eAgInPc4d1	erotický
časopisy	časopis	k1gInPc4	časopis
Adam	Adam	k1gMnSc1	Adam
<g/>
,	,	kIx,	,
Screw	Screw	k1gMnSc1	Screw
<g/>
,	,	kIx,	,
Fling	Fling	k1gMnSc1	Fling
<g/>
,	,	kIx,	,
Hustler	Hustler	k1gMnSc1	Hustler
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ty	ten	k3xDgFnPc1	ten
většinou	většinou	k6eAd1	většinou
neměly	mít	k5eNaImAgFnP	mít
valnou	valný	k2eAgFnSc4d1	valná
kvalitu	kvalita	k1gFnSc4	kvalita
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
občas	občas	k6eAd1	občas
některá	některý	k3yIgNnPc4	některý
vyčnívala	vyčnívat	k5eAaImAgNnP	vyčnívat
<g/>
.	.	kIx.	.
</s>
<s>
Nejzvrácenější	zvrácený	k2eAgFnSc1d3	nejzvrácenější
povídka	povídka	k1gFnSc1	povídka
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
Úchyl	Úchyl	k1gInSc1	Úchyl
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
The	The	k1gMnSc1	The
Fiend	Fiend	k1gMnSc1	Fiend
<g/>
)	)	kIx)	)
o	o	k7c6	o
znásilnění	znásilnění	k1gNnSc6	znásilnění
malé	malý	k2eAgFnSc2d1	malá
holčičky	holčička	k1gFnSc2	holčička
(	(	kIx(	(
<g/>
vyšla	vyjít	k5eAaPmAgFnS	vyjít
ve	v	k7c6	v
sbírce	sbírka	k1gFnSc6	sbírka
Nejkrásnější	krásný	k2eAgFnSc1d3	nejkrásnější
ženská	ženská	k1gFnSc1	ženská
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
angl.	angl.	k?	angl.
The	The	k1gFnSc1	The
most	most	k1gInSc1	most
Beautiful	Beautiful	k1gInSc1	Beautiful
Woman	Woman	k1gInSc1	Woman
in	in	k?	in
Town	Town	k1gMnSc1	Town
&	&	k?	&
Other	Other	k1gMnSc1	Other
Stories	Stories	k1gMnSc1	Stories
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Jane	Jan	k1gMnSc5	Jan
Cooney	Coonea	k1gFnSc2	Coonea
Bakerové	Bakerové	k2eAgFnSc1d1	Bakerové
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
Bukowského	Bukowského	k2eAgFnSc7d1	Bukowského
další	další	k2eAgFnSc7d1	další
velkou	velký	k2eAgFnSc7d1	velká
láskou	láska	k1gFnSc7	láska
sochařka	sochařka	k1gFnSc1	sochařka
a	a	k8xC	a
příležitostná	příležitostný	k2eAgFnSc1d1	příležitostná
básnířka	básnířka	k1gFnSc1	básnířka
Linda	Linda	k1gFnSc1	Linda
Kingová	Kingový	k2eAgFnSc1d1	Kingová
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
ji	on	k3xPp3gFnSc4	on
zpočátku	zpočátku	k6eAd1	zpočátku
nepřitahoval	přitahovat	k5eNaImAgMnS	přitahovat
<g/>
,	,	kIx,	,
z	z	k7c2	z
občasných	občasný	k2eAgFnPc2d1	občasná
schůzek	schůzka	k1gFnPc2	schůzka
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
vztah	vztah	k1gInSc1	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Linda	Linda	k1gFnSc1	Linda
věnovala	věnovat	k5eAaImAgFnS	věnovat
Charlesovi	Charles	k1gMnSc3	Charles
bustu	busta	k1gFnSc4	busta
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
neopomněl	opomnět	k5eNaPmAgMnS	opomnět
zmínit	zmínit	k5eAaPmF	zmínit
v	v	k7c6	v
některé	některý	k3yIgFnSc6	některý
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
povídek	povídka	k1gFnPc2	povídka
<g/>
.	.	kIx.	.
</s>
<s>
Linda	Linda	k1gFnSc1	Linda
jej	on	k3xPp3gMnSc4	on
přinutila	přinutit	k5eAaPmAgFnS	přinutit
omezit	omezit	k5eAaPmF	omezit
pití	pití	k1gNnSc4	pití
<g/>
,	,	kIx,	,
přestat	přestat	k5eAaPmF	přestat
užívat	užívat	k5eAaImF	užívat
valium	valium	k1gNnSc4	valium
(	(	kIx(	(
<g/>
které	který	k3yIgNnSc1	který
bral	brát	k5eAaImAgInS	brát
na	na	k7c4	na
uklidnění	uklidnění	k1gNnSc4	uklidnění
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
nemohl	moct	k5eNaImAgMnS	moct
spát	spát	k5eAaImF	spát
<g/>
)	)	kIx)	)
a	a	k8xC	a
zavedla	zavést	k5eAaPmAgFnS	zavést
mu	on	k3xPp3gMnSc3	on
dietu	dieta	k1gFnSc4	dieta
a	a	k8xC	a
pravidelné	pravidelný	k2eAgNnSc4d1	pravidelné
cvičení	cvičení	k1gNnSc4	cvičení
<g/>
.	.	kIx.	.
</s>
<s>
Bukowski	Bukowski	k6eAd1	Bukowski
po	po	k7c6	po
této	tento	k3xDgFnSc6	tento
kúře	kúra	k1gFnSc6	kúra
zhubnul	zhubnout	k5eAaPmAgMnS	zhubnout
ze	z	k7c2	z
110	[number]	k4	110
kg	kg	kA	kg
až	až	k6eAd1	až
na	na	k7c4	na
72	[number]	k4	72
kg	kg	kA	kg
<g/>
,	,	kIx,	,
nechal	nechat	k5eAaPmAgMnS	nechat
si	se	k3xPyFc3	se
narůst	narůst	k5eAaPmF	narůst
vousy	vous	k1gInPc4	vous
a	a	k8xC	a
delší	dlouhý	k2eAgInPc4d2	delší
vlasy	vlas	k1gInPc4	vlas
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
módě	móda	k1gFnSc6	móda
<g/>
,	,	kIx,	,
a	a	k8xC	a
koupil	koupit	k5eAaPmAgMnS	koupit
si	se	k3xPyFc3	se
lepší	dobrý	k2eAgNnSc4d2	lepší
oblečení	oblečení	k1gNnSc4	oblečení
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
přeměny	přeměna	k1gFnSc2	přeměna
Linda	Linda	k1gFnSc1	Linda
vzpomíná	vzpomínat	k5eAaImIp3nS	vzpomínat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
spisovatel	spisovatel	k1gMnSc1	spisovatel
intenzivně	intenzivně	k6eAd1	intenzivně
zamiloval	zamilovat	k5eAaPmAgMnS	zamilovat
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
údajně	údajně	k6eAd1	údajně
až	až	k9	až
posedlost	posedlost	k1gFnSc1	posedlost
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
šílenou	šílený	k2eAgFnSc7d1	šílená
láskou	láska	k1gFnSc7	láska
se	se	k3xPyFc4	se
dostavila	dostavit	k5eAaPmAgFnS	dostavit
samozřejmě	samozřejmě	k6eAd1	samozřejmě
i	i	k9	i
žárlivost	žárlivost	k1gFnSc4	žárlivost
<g/>
,	,	kIx,	,
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
se	se	k3xPyFc4	se
vyrovnat	vyrovnat	k5eAaBmF	vyrovnat
s	s	k7c7	s
koketerií	koketerie	k1gFnSc7	koketerie
své	svůj	k3xOyFgFnPc4	svůj
mladé	mladý	k2eAgFnPc4d1	mladá
přítelkyně	přítelkyně	k1gFnPc4	přítelkyně
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
zavdávalo	zavdávat	k5eAaImAgNnS	zavdávat
časté	častý	k2eAgFnPc4d1	častá
záminky	záminka	k1gFnPc4	záminka
k	k	k7c3	k
vulgárním	vulgární	k2eAgFnPc3d1	vulgární
hádkám	hádka	k1gFnPc3	hádka
a	a	k8xC	a
častým	častý	k2eAgInPc3d1	častý
rozchodům	rozchod	k1gInPc3	rozchod
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
však	však	k9	však
nebyl	být	k5eNaImAgMnS	být
svatý	svatý	k2eAgMnSc1d1	svatý
<g/>
,	,	kIx,	,
neodmítal	odmítat	k5eNaImAgMnS	odmítat
nabídky	nabídka	k1gFnPc4	nabídka
jiných	jiný	k2eAgFnPc2d1	jiná
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnPc3	který
imponovala	imponovat	k5eAaImAgFnS	imponovat
spíše	spíše	k9	spíše
jeho	jeho	k3xOp3gFnSc1	jeho
rostoucí	rostoucí	k2eAgFnSc1d1	rostoucí
sláva	sláva	k1gFnSc1	sláva
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
ale	ale	k9	ale
Linda	Linda	k1gFnSc1	Linda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
jej	on	k3xPp3gMnSc4	on
zasvětila	zasvětit	k5eAaPmAgFnS	zasvětit
do	do	k7c2	do
tajů	taj	k1gInPc2	taj
milování	milování	k1gNnSc2	milování
a	a	k8xC	a
naučila	naučit	k5eAaPmAgFnS	naučit
jej	on	k3xPp3gInSc4	on
spoustu	spoustu	k6eAd1	spoustu
věcí	věc	k1gFnSc7	věc
v	v	k7c6	v
posteli	postel	k1gFnSc6	postel
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
v	v	k7c6	v
Bukowském	Bukowské	k1gNnSc6	Bukowské
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
něco	něco	k6eAd1	něco
z	z	k7c2	z
puritánství	puritánství	k1gNnSc2	puritánství
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ovšem	ovšem	k9	ovšem
nebylo	být	k5eNaImAgNnS	být
vysloveně	vysloveně	k6eAd1	vysloveně
špatné	špatný	k2eAgNnSc1d1	špatné
<g/>
.	.	kIx.	.
</s>
<s>
Linda	Linda	k1gFnSc1	Linda
Kingová	Kingový	k2eAgFnSc1d1	Kingová
jej	on	k3xPp3gMnSc4	on
obviňovala	obviňovat	k5eAaImAgFnS	obviňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
schválně	schválně	k6eAd1	schválně
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
zlou	zlý	k2eAgFnSc4d1	zlá
krev	krev	k1gFnSc4	krev
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
měl	mít	k5eAaImAgInS	mít
náměty	námět	k1gInPc4	námět
pro	pro	k7c4	pro
své	svůj	k3xOyFgNnSc4	svůj
psaní	psaní	k1gNnSc4	psaní
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
ní	on	k3xPp3gFnSc2	on
si	se	k3xPyFc3	se
vždycky	vždycky	k6eAd1	vždycky
něco	něco	k3yInSc1	něco
našel	najít	k5eAaPmAgMnS	najít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
nespokojený	spokojený	k2eNgInSc1d1	nespokojený
<g/>
,	,	kIx,	,
i	i	k8xC	i
kdyby	kdyby	kYmCp3nS	kdyby
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
jako	jako	k9	jako
v	v	k7c6	v
bavlnce	bavlnka	k1gFnSc6	bavlnka
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
ji	on	k3xPp3gFnSc4	on
miloval	milovat	k5eAaImAgInS	milovat
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
zároveň	zároveň	k6eAd1	zároveň
i	i	k9	i
nenávist	nenávist	k1gFnSc1	nenávist
<g/>
.	.	kIx.	.
</s>
<s>
Nedokázal	dokázat	k5eNaPmAgMnS	dokázat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
podezřívavosti	podezřívavost	k1gFnSc6	podezřívavost
rozpoznat	rozpoznat	k5eAaPmF	rozpoznat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
jsou	být	k5eAaImIp3nP	být
její	její	k3xOp3gFnPc4	její
city	city	k1gFnPc4	city
upřímné	upřímný	k2eAgFnPc4d1	upřímná
<g/>
.	.	kIx.	.
</s>
<s>
Zažil	zažít	k5eAaPmAgMnS	zažít
už	už	k9	už
tolik	tolik	k4yIc4	tolik
zklamání	zklamání	k1gNnPc2	zklamání
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jen	jen	k9	jen
stěží	stěží	k6eAd1	stěží
dokázal	dokázat	k5eAaPmAgInS	dokázat
otevřít	otevřít	k5eAaPmF	otevřít
naplno	naplno	k6eAd1	naplno
a	a	k8xC	a
snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
za	za	k7c4	za
každou	každý	k3xTgFnSc4	každý
cenu	cena	k1gFnSc4	cena
bránit	bránit	k5eAaImF	bránit
se	se	k3xPyFc4	se
dalšímu	další	k2eAgNnSc3d1	další
citovému	citový	k2eAgNnSc3d1	citové
zranění	zranění	k1gNnSc3	zranění
<g/>
.	.	kIx.	.
</s>
<s>
Linda	Linda	k1gFnSc1	Linda
dále	daleko	k6eAd2	daleko
vypovídá	vypovídat	k5eAaPmIp3nS	vypovídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Bukowski	Bukowski	k1gNnSc7	Bukowski
byl	být	k5eAaImAgInS	být
zpočátku	zpočátku	k6eAd1	zpočátku
za	za	k7c4	za
střízliva	střízliv	k2eAgNnPc4d1	střízliv
zdrženlivý	zdrženlivý	k2eAgMnSc1d1	zdrženlivý
až	až	k9	až
téměř	téměř	k6eAd1	téměř
plachý	plachý	k2eAgMnSc1d1	plachý
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jakmile	jakmile	k8xS	jakmile
si	se	k3xPyFc3	se
vypil	vypít	k5eAaPmAgMnS	vypít
<g/>
,	,	kIx,	,
stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gNnSc2	on
zvíře	zvíře	k1gNnSc1	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
absolutně	absolutně	k6eAd1	absolutně
nepříčetný	příčetný	k2eNgInSc1d1	nepříčetný
a	a	k8xC	a
nejednou	jednou	k6eNd1	jednou
zdemoloval	zdemolovat	k5eAaPmAgInS	zdemolovat
interiér	interiér	k1gInSc1	interiér
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgNnSc6	který
se	se	k3xPyFc4	se
zrovna	zrovna	k6eAd1	zrovna
nacházel	nacházet	k5eAaImAgMnS	nacházet
<g/>
.	.	kIx.	.
</s>
<s>
Lindu	Linda	k1gFnSc4	Linda
Kingovou	Kingový	k2eAgFnSc4d1	Kingová
popsal	popsat	k5eAaPmAgMnS	popsat
Bukowski	Bukowske	k1gFnSc4	Bukowske
jako	jako	k9	jako
Lýdii	Lýdia	k1gFnSc4	Lýdia
Vanceovou	Vanceův	k2eAgFnSc7d1	Vanceův
v	v	k7c6	v
románu	román	k1gInSc2	román
Ženy	žena	k1gFnPc1	žena
<g/>
.	.	kIx.	.
</s>
<s>
Uvádí	uvádět	k5eAaImIp3nS	uvádět
zde	zde	k6eAd1	zde
věčné	věčný	k2eAgFnPc4d1	věčná
hádky	hádka	k1gFnPc4	hádka
a	a	k8xC	a
spory	spor	k1gInPc4	spor
pramenící	pramenící	k2eAgInPc4d1	pramenící
z	z	k7c2	z
její	její	k3xOp3gFnSc2	její
impulzivity	impulzivita	k1gFnSc2	impulzivita
a	a	k8xC	a
výměnu	výměna	k1gFnSc4	výměna
jeho	jeho	k3xOp3gFnSc2	jeho
busty	busta	k1gFnSc2	busta
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
po	po	k7c6	po
každém	každý	k3xTgInSc6	každý
rozchodu	rozchod	k1gInSc6	rozchod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
si	se	k3xPyFc3	se
Bukowski	Bukowski	k1gNnSc4	Bukowski
dopisoval	dopisovat	k5eAaImAgMnS	dopisovat
s	s	k7c7	s
Williamem	William	k1gInSc7	William
Wantlingem	Wantling	k1gInSc7	Wantling
<g/>
,	,	kIx,	,
bývalým	bývalý	k2eAgMnSc7d1	bývalý
příslušníkem	příslušník	k1gMnSc7	příslušník
námořní	námořní	k2eAgFnSc2d1	námořní
pěchoty	pěchota	k1gFnSc2	pěchota
<g/>
.	.	kIx.	.
</s>
<s>
Wantling	Wantling	k1gInSc1	Wantling
učil	učít	k5eAaPmAgInS	učít
angličtinu	angličtina	k1gFnSc4	angličtina
na	na	k7c6	na
Illinoiské	Illinoiský	k2eAgFnSc6d1	Illinoiská
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
psal	psát	k5eAaImAgInS	psát
také	také	k9	také
poezii	poezie	k1gFnSc4	poezie
(	(	kIx(	(
<g/>
kterou	který	k3yRgFnSc4	který
Bukowski	Bukowske	k1gFnSc4	Bukowske
uznával	uznávat	k5eAaImAgInS	uznávat
<g/>
)	)	kIx)	)
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
závislý	závislý	k2eAgInSc1d1	závislý
na	na	k7c6	na
drogách	droga	k1gFnPc6	droga
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
strohé	strohý	k2eAgFnPc1d1	strohá
básně	báseň	k1gFnPc1	báseň
vypovídaly	vypovídat	k5eAaPmAgFnP	vypovídat
o	o	k7c6	o
mnohem	mnohem	k6eAd1	mnohem
těžším	těžký	k2eAgInSc6d2	těžší
životě	život	k1gInSc6	život
<g/>
,	,	kIx,	,
než	než	k8xS	než
jaký	jaký	k3yIgInSc1	jaký
Bukowski	Bukowske	k1gFnSc4	Bukowske
zakusil	zakusit	k5eAaPmAgInS	zakusit
-	-	kIx~	-
o	o	k7c4	o
prodávání	prodávání	k1gNnSc4	prodávání
heroinu	heroin	k1gInSc2	heroin
<g/>
,	,	kIx,	,
o	o	k7c6	o
válce	válka	k1gFnSc6	válka
v	v	k7c6	v
Koreji	Korea	k1gFnSc6	Korea
a	a	k8xC	a
rozbíjení	rozbíjení	k1gNnSc6	rozbíjení
lebek	lebka	k1gFnPc2	lebka
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
a	a	k8xC	a
když	když	k8xS	když
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
odrazit	odrazit	k5eAaPmF	odrazit
se	se	k3xPyFc4	se
ode	ode	k7c2	ode
dna	dno	k1gNnSc2	dno
a	a	k8xC	a
dostal	dostat	k5eAaPmAgMnS	dostat
nabídku	nabídka	k1gFnSc4	nabídka
učit	učit	k5eAaImF	učit
poezii	poezie	k1gFnSc4	poezie
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
,	,	kIx,	,
Bukowski	Bukowsk	k1gInSc6	Bukowsk
jej	on	k3xPp3gInSc4	on
obvinil	obvinit	k5eAaPmAgMnS	obvinit
(	(	kIx(	(
<g/>
jak	jak	k8xS	jak
měl	mít	k5eAaImAgMnS	mít
v	v	k7c6	v
povaze	povaha	k1gFnSc6	povaha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
zaprodává	zaprodávat	k5eAaImIp3nS	zaprodávat
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
dalo	dát	k5eAaPmAgNnS	dát
nazvat	nazvat	k5eAaBmF	nazvat
pokrytectvím	pokrytectví	k1gNnSc7	pokrytectví
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
vezmeme	vzít	k5eAaPmIp1nP	vzít
v	v	k7c4	v
potaz	potaz	k1gInSc4	potaz
Bukowského	Bukowského	k2eAgFnSc2d1	Bukowského
rozčilené	rozčilený	k2eAgFnSc2d1	rozčilená
reakce	reakce	k1gFnSc2	reakce
na	na	k7c4	na
čtenáře	čtenář	k1gMnPc4	čtenář
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
jej	on	k3xPp3gMnSc4	on
žádali	žádat	k5eAaImAgMnP	žádat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
psal	psát	k5eAaImAgInS	psát
nadále	nadále	k6eAd1	nadále
o	o	k7c6	o
kurvách	kurva	k1gFnPc6	kurva
a	a	k8xC	a
o	o	k7c6	o
barech	bar	k1gInPc6	bar
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
tento	tento	k3xDgInSc4	tento
způsob	způsob	k1gInSc4	způsob
života	život	k1gInSc2	život
opustil	opustit	k5eAaPmAgInS	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Bill	Bill	k1gMnSc1	Bill
Wantling	Wantling	k1gInSc4	Wantling
neprožíval	prožívat	k5eNaImAgMnS	prožívat
zrovna	zrovna	k6eAd1	zrovna
šťastné	šťastný	k2eAgFnPc4d1	šťastná
chvilky	chvilka	k1gFnPc4	chvilka
<g/>
,	,	kIx,	,
rozpadalo	rozpadat	k5eAaImAgNnS	rozpadat
se	se	k3xPyFc4	se
mu	on	k3xPp3gNnSc3	on
manželství	manželství	k1gNnSc3	manželství
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
své	svůj	k3xOyFgFnSc3	svůj
závislosti	závislost	k1gFnSc3	závislost
měl	mít	k5eAaImAgMnS	mít
problémy	problém	k1gInPc4	problém
s	s	k7c7	s
policií	policie	k1gFnSc7	policie
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
nervově	nervově	k6eAd1	nervově
labilní	labilní	k2eAgInSc1d1	labilní
<g/>
.	.	kIx.	.
</s>
<s>
Zorganizoval	zorganizovat	k5eAaPmAgMnS	zorganizovat
pro	pro	k7c4	pro
Bukowského	Bukowského	k2eAgNnSc4d1	Bukowského
čtení	čtení	k1gNnSc4	čtení
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
a	a	k8xC	a
těšil	těšit	k5eAaImAgMnS	těšit
se	se	k3xPyFc4	se
na	na	k7c6	na
setkání	setkání	k1gNnSc6	setkání
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
oblíbeným	oblíbený	k2eAgMnSc7d1	oblíbený
autorem	autor	k1gMnSc7	autor
<g/>
.	.	kIx.	.
</s>
<s>
Bukowski	Bukowski	k6eAd1	Bukowski
přiletěl	přiletět	k5eAaPmAgMnS	přiletět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vystoupení	vystoupení	k1gNnSc1	vystoupení
skončilo	skončit	k5eAaPmAgNnS	skončit
fiaskem	fiasko	k1gNnSc7	fiasko
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
byl	být	k5eAaImAgMnS	být
dost	dost	k6eAd1	dost
opilý	opilý	k2eAgMnSc1d1	opilý
a	a	k8xC	a
s	s	k7c7	s
Wantlingem	Wantling	k1gInSc7	Wantling
se	se	k3xPyFc4	se
skoro	skoro	k6eAd1	skoro
nebavil	bavit	k5eNaImAgMnS	bavit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
L.	L.	kA	L.
A.	A.	kA	A.
sedl	sednout	k5eAaPmAgInS	sednout
ke	k	k7c3	k
stroji	stroj	k1gInSc3	stroj
<g/>
,	,	kIx,	,
a	a	k8xC	a
jak	jak	k6eAd1	jak
měl	mít	k5eAaImAgMnS	mít
ve	v	k7c6	v
zvyku	zvyk	k1gInSc6	zvyk
<g/>
,	,	kIx,	,
napsal	napsat	k5eAaPmAgMnS	napsat
jedovatý	jedovatý	k2eAgInSc4d1	jedovatý
článek	článek	k1gInSc4	článek
o	o	k7c6	o
svém	svůj	k3xOyFgInSc6	svůj
výletu	výlet	k1gInSc6	výlet
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
Wantlinga	Wantlinga	k1gFnSc1	Wantlinga
(	(	kIx(	(
<g/>
ačkoliv	ačkoliv	k8xS	ačkoliv
použil	použít	k5eAaPmAgInS	použít
pseudonym	pseudonym	k1gInSc1	pseudonym
<g/>
)	)	kIx)	)
sepsul	sepsul	k1gInSc1	sepsul
<g/>
.	.	kIx.	.
</s>
<s>
William	William	k6eAd1	William
Wantling	Wantling	k1gInSc1	Wantling
byl	být	k5eAaImAgInS	být
hluboce	hluboko	k6eAd1	hluboko
zklamaný	zklamaný	k2eAgMnSc1d1	zklamaný
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgInS	začít
intenzivně	intenzivně	k6eAd1	intenzivně
pít	pít	k5eAaImF	pít
a	a	k8xC	a
propukla	propuknout	k5eAaPmAgFnS	propuknout
u	u	k7c2	u
něj	on	k3xPp3gMnSc2	on
psychóza	psychóza	k1gFnSc1	psychóza
<g/>
.	.	kIx.	.
</s>
<s>
A.D.	A.D.	k?	A.D.
Winansovi	Winans	k1gMnSc3	Winans
<g/>
,	,	kIx,	,
regionálnímu	regionální	k2eAgMnSc3d1	regionální
nakladateli	nakladatel	k1gMnSc3	nakladatel
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
svěřil	svěřit	k5eAaPmAgMnS	svěřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
touží	toužit	k5eAaImIp3nP	toužit
upít	upít	k5eAaPmF	upít
k	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
také	také	k9	také
podařilo	podařit	k5eAaPmAgNnS	podařit
<g/>
.	.	kIx.	.
</s>
<s>
Pil	pít	k5eAaImAgMnS	pít
celé	celý	k2eAgInPc4d1	celý
dny	den	k1gInPc4	den
a	a	k8xC	a
noci	noc	k1gFnPc4	noc
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
dostal	dostat	k5eAaPmAgMnS	dostat
infarkt	infarkt	k1gInSc4	infarkt
a	a	k8xC	a
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Bukowski	Bukowski	k6eAd1	Bukowski
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
cítil	cítit	k5eAaImAgMnS	cítit
provinile	provinile	k6eAd1	provinile
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ne	ne	k9	ne
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
nepokusil	pokusit	k5eNaPmAgMnS	pokusit
svést	svést	k5eAaPmF	svést
vdovu	vdova	k1gFnSc4	vdova
Ruth	Ruth	k1gFnSc2	Ruth
Wantlingovou	Wantlingový	k2eAgFnSc4d1	Wantlingový
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
otřesný	otřesný	k2eAgInSc1d1	otřesný
zážitek	zážitek	k1gInSc1	zážitek
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
Bukowski	Bukowske	k1gFnSc4	Bukowske
projevil	projevit	k5eAaPmAgMnS	projevit
jako	jako	k8xC	jako
nechutný	chutný	k2eNgMnSc1d1	nechutný
mizogyn	mizogyn	k1gMnSc1	mizogyn
a	a	k8xC	a
toto	tento	k3xDgNnSc4	tento
stanovisko	stanovisko	k1gNnSc4	stanovisko
sdílela	sdílet	k5eAaImAgFnS	sdílet
většina	většina	k1gFnSc1	většina
lidí	člověk	k1gMnPc2	člověk
včetně	včetně	k7c2	včetně
některých	některý	k3yIgMnPc2	některý
jeho	jeho	k3xOp3gMnPc2	jeho
přátel	přítel	k1gMnPc2	přítel
<g/>
.	.	kIx.	.
</s>
<s>
William	William	k1gInSc1	William
a	a	k8xC	a
Ruth	Ruth	k1gFnSc1	Ruth
Wantlingovi	Wantlingův	k2eAgMnPc1d1	Wantlingův
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
v	v	k7c6	v
románu	román	k1gInSc6	román
Ženy	žena	k1gFnSc2	žena
pod	pod	k7c7	pod
pseudonymy	pseudonym	k1gInPc7	pseudonym
Bill	Bill	k1gMnSc1	Bill
a	a	k8xC	a
Cecílie	Cecílie	k1gFnSc1	Cecílie
Keesingovi	Keesingův	k2eAgMnPc1d1	Keesingův
<g/>
.	.	kIx.	.
</s>
<s>
Bukowski	Bukowski	k6eAd1	Bukowski
se	se	k3xPyFc4	se
neobtěžoval	obtěžovat	k5eNaImAgMnS	obtěžovat
uvést	uvést	k5eAaPmF	uvést
okolnosti	okolnost	k1gFnPc4	okolnost
Wantlingovy	Wantlingův	k2eAgFnSc2d1	Wantlingův
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Lizou	Lizý	k2eAgFnSc7d1	Lizý
Williamsovou	Williamsová	k1gFnSc7	Williamsová
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
stýkat	stýkat	k5eAaImF	stýkat
krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
na	na	k7c4	na
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
rozešel	rozejít	k5eAaPmAgMnS	rozejít
s	s	k7c7	s
Lindou	Linda	k1gFnSc7	Linda
Kingovou	Kingův	k2eAgFnSc7d1	Kingova
<g/>
.	.	kIx.	.
</s>
<s>
Znali	znát	k5eAaImAgMnP	znát
se	se	k3xPyFc4	se
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
společně	společně	k6eAd1	společně
psali	psát	k5eAaImAgMnP	psát
pro	pro	k7c4	pro
Bryanovy	Bryanův	k2eAgFnPc4d1	Bryanova
undergroundové	undergroundový	k2eAgFnPc4d1	undergroundová
noviny	novina	k1gFnPc4	novina
Open	Open	k1gInSc4	Open
City	city	k1gNnSc1	city
<g/>
.	.	kIx.	.
</s>
<s>
Liza	Liza	k1gFnSc1	Liza
byla	být	k5eAaImAgFnS	být
zhruba	zhruba	k6eAd1	zhruba
stejně	stejně	k6eAd1	stejně
stará	starat	k5eAaImIp3nS	starat
jako	jako	k9	jako
Bukowski	Bukowske	k1gFnSc4	Bukowske
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
věděl	vědět	k5eAaImAgMnS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
Linda	Linda	k1gFnSc1	Linda
žárlí	žárlit	k5eAaImIp3nS	žárlit
<g/>
.	.	kIx.	.
</s>
<s>
Liza	Liza	k1gFnSc1	Liza
pracovala	pracovat	k5eAaImAgFnS	pracovat
ve	v	k7c6	v
vedení	vedení	k1gNnSc6	vedení
nahrávací	nahrávací	k2eAgFnSc2d1	nahrávací
společnosti	společnost	k1gFnSc2	společnost
Island	Island	k1gInSc1	Island
Records	Records	k1gInSc1	Records
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
Bukowskim	Bukowski	k1gNnSc7	Bukowski
odletěli	odletět	k5eAaPmAgMnP	odletět
na	na	k7c4	na
výlet	výlet	k1gInSc4	výlet
na	na	k7c4	na
ostrov	ostrov	k1gInSc4	ostrov
Santa	Santo	k1gNnSc2	Santo
Catalina	Catalin	k2eAgNnSc2d1	Catalino
u	u	k7c2	u
pobřeží	pobřeží	k1gNnSc2	pobřeží
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
ubytovali	ubytovat	k5eAaPmAgMnP	ubytovat
v	v	k7c6	v
hotelu	hotel	k1gInSc6	hotel
Monterey	Monterea	k1gFnSc2	Monterea
v	v	k7c6	v
přístavním	přístavní	k2eAgNnSc6d1	přístavní
městě	město	k1gNnSc6	město
Avalon	Avalon	k1gInSc1	Avalon
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
ani	ani	k8xC	ani
tady	tady	k6eAd1	tady
se	se	k3xPyFc4	se
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
Bukowski	Bukowski	k1gNnSc7	Bukowski
rozptýlit	rozptýlit	k5eAaPmF	rozptýlit
<g/>
,	,	kIx,	,
seděl	sedět	k5eAaImAgMnS	sedět
v	v	k7c6	v
hotelovém	hotelový	k2eAgInSc6d1	hotelový
pokoji	pokoj	k1gInSc6	pokoj
<g/>
,	,	kIx,	,
chlastal	chlastat	k5eAaImAgMnS	chlastat
pivo	pivo	k1gNnSc4	pivo
a	a	k8xC	a
víno	víno	k1gNnSc4	víno
a	a	k8xC	a
semtam	semtam	k6eAd1	semtam
napsal	napsat	k5eAaBmAgMnS	napsat
báseň	báseň	k1gFnSc4	báseň
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Liza	Liza	k1gFnSc1	Liza
chodila	chodit	k5eAaImAgFnS	chodit
se	s	k7c7	s
známými	známý	k1gMnPc7	známý
po	po	k7c6	po
nákupech	nákup	k1gInPc6	nákup
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
mu	on	k3xPp3gMnSc3	on
koupila	koupit	k5eAaPmAgFnS	koupit
kanára	kanár	k1gMnSc4	kanár
v	v	k7c6	v
kleci	klec	k1gFnSc6	klec
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
na	na	k7c6	na
pokoji	pokoj	k1gInSc6	pokoj
necítil	cítit	k5eNaImAgMnS	cítit
tak	tak	k6eAd1	tak
sám	sám	k3xTgMnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
L.A.	L.A.	k1gFnSc2	L.A.
začal	začít	k5eAaPmAgMnS	začít
Bukowski	Bukowske	k1gFnSc4	Bukowske
trávit	trávit	k5eAaImF	trávit
hodně	hodně	k6eAd1	hodně
času	čas	k1gInSc2	čas
v	v	k7c6	v
jejím	její	k3xOp3gInSc6	její
domě	dům	k1gInSc6	dům
na	na	k7c4	na
Tuxedo	Tuxedo	k1gNnSc4	Tuxedo
Terrace	Terrace	k1gFnSc2	Terrace
<g/>
.	.	kIx.	.
</s>
<s>
Chodili	chodit	k5eAaImAgMnP	chodit
spolu	spolu	k6eAd1	spolu
na	na	k7c4	na
rockové	rockový	k2eAgInPc4d1	rockový
koncerty	koncert	k1gInPc4	koncert
<g/>
,	,	kIx,	,
večeře	večeře	k1gFnPc4	večeře
<g/>
,	,	kIx,	,
doma	doma	k6eAd1	doma
pořádala	pořádat	k5eAaImAgFnS	pořádat
Liza	Liza	k1gFnSc1	Liza
večírky	večírek	k1gInPc4	večírek
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgInPc6	jenž
se	se	k3xPyFc4	se
Charles	Charles	k1gMnSc1	Charles
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
zajímavými	zajímavý	k2eAgMnPc7d1	zajímavý
hudebníky	hudebník	k1gMnPc7	hudebník
<g/>
,	,	kIx,	,
malíři	malíř	k1gMnPc7	malíř
a	a	k8xC	a
televizními	televizní	k2eAgMnPc7d1	televizní
tvůrci	tvůrce	k1gMnPc7	tvůrce
<g/>
,	,	kIx,	,
např.	např.	kA	např.
s	s	k7c7	s
Robertem	Robert	k1gMnSc7	Robert
Crumbem	Crumb	k1gMnSc7	Crumb
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
později	pozdě	k6eAd2	pozdě
ilustroval	ilustrovat	k5eAaBmAgMnS	ilustrovat
jeho	jeho	k3xOp3gFnSc4	jeho
sbírku	sbírka	k1gFnSc4	sbírka
Bring	Bring	k1gMnSc1	Bring
Me	Me	k1gMnSc1	Me
Your	Your	k1gMnSc1	Your
Love	lov	k1gInSc5	lov
a	a	k8xC	a
deník	deník	k1gInSc1	deník
Kapitán	kapitán	k1gMnSc1	kapitán
odešel	odejít	k5eAaPmAgMnS	odejít
na	na	k7c4	na
oběd	oběd	k1gInSc4	oběd
a	a	k8xC	a
námořníci	námořník	k1gMnPc1	námořník
převzali	převzít	k5eAaPmAgMnP	převzít
velení	velený	k2eAgMnPc1d1	velený
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
The	The	k1gMnSc1	The
Captain	Captain	k1gMnSc1	Captain
Is	Is	k1gMnSc1	Is
Out	Out	k1gMnSc1	Out
to	ten	k3xDgNnSc1	ten
Lunch	lunch	k1gInSc1	lunch
and	and	k?	and
the	the	k?	the
Sailors	Sailors	k1gInSc1	Sailors
Have	Have	k1gNnSc1	Have
Taken	Taken	k1gInSc1	Taken
Over	Over	k1gMnSc1	Over
the	the	k?	the
Ship	Ship	k1gMnSc1	Ship
<g/>
)	)	kIx)	)
a	a	k8xC	a
Taylorem	Taylor	k1gInSc7	Taylor
Hackfordem	Hackford	k1gInSc7	Hackford
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
o	o	k7c6	o
Bukowském	Bukowské	k1gNnSc6	Bukowské
natočil	natočit	k5eAaBmAgInS	natočit
dokument	dokument	k1gInSc1	dokument
pro	pro	k7c4	pro
místní	místní	k2eAgFnSc4d1	místní
stanici	stanice	k1gFnSc4	stanice
KCET	KCET	kA	KCET
<g/>
.	.	kIx.	.
</s>
<s>
Liza	Liza	k6eAd1	Liza
se	se	k3xPyFc4	se
do	do	k7c2	do
Bukowského	Bukowský	k1gMnSc2	Bukowský
záhy	záhy	k6eAd1	záhy
zamilovala	zamilovat	k5eAaPmAgFnS	zamilovat
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
on	on	k3xPp3gMnSc1	on
stále	stále	k6eAd1	stále
myslel	myslet	k5eAaImAgMnS	myslet
na	na	k7c4	na
Lindu	Linda	k1gFnSc4	Linda
Kingovou	Kingův	k2eAgFnSc7d1	Kingova
a	a	k8xC	a
po	po	k7c6	po
častých	častý	k2eAgInPc6d1	častý
telefonátech	telefonát	k1gInPc6	telefonát
se	se	k3xPyFc4	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
zase	zase	k9	zase
dal	dát	k5eAaPmAgInS	dát
dohromady	dohromady	k6eAd1	dohromady
a	a	k8xC	a
Lizu	liz	k1gInSc2	liz
opustil	opustit	k5eAaPmAgInS	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
pokračovaly	pokračovat	k5eAaImAgFnP	pokračovat
hádky	hádka	k1gFnPc1	hádka
na	na	k7c4	na
ostří	ostří	k1gNnSc4	ostří
nože	nůž	k1gInSc2	nůž
a	a	k8xC	a
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
Linda	Linda	k1gFnSc1	Linda
Kingová	Kingový	k2eAgFnSc1d1	Kingová
otěhotněla	otěhotnět	k5eAaPmAgFnS	otěhotnět
(	(	kIx(	(
<g/>
a	a	k8xC	a
následně	následně	k6eAd1	následně
potratila	potratit	k5eAaPmAgFnS	potratit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
definitivně	definitivně	k6eAd1	definitivně
ukončí	ukončit	k5eAaPmIp3nS	ukončit
<g/>
.	.	kIx.	.
</s>
<s>
Liza	Liz	k2eAgFnSc1d1	Liza
Williamsová	Williamsová	k1gFnSc1	Williamsová
je	být	k5eAaImIp3nS	být
ztvárněna	ztvárnit	k5eAaPmNgFnS	ztvárnit
v	v	k7c6	v
postavě	postava	k1gFnSc6	postava
Dee	Dee	k1gFnSc2	Dee
Dee	Dee	k1gFnSc2	Dee
Bronsonové	Bronsonová	k1gFnSc2	Bronsonová
v	v	k7c6	v
románu	román	k1gInSc6	román
Ženy	žena	k1gFnSc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Pamelou	Pamela	k1gFnSc7	Pamela
Millerovou	Millerův	k2eAgFnSc7d1	Millerova
se	se	k3xPyFc4	se
Bukowski	Bukowske	k1gFnSc4	Bukowske
seznámil	seznámit	k5eAaPmAgMnS	seznámit
<g/>
,	,	kIx,	,
když	když	k8xS	když
jí	on	k3xPp3gFnSc3	on
bylo	být	k5eAaImAgNnS	být
23	[number]	k4	23
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
jemu	on	k3xPp3gMnSc3	on
samotnému	samotný	k2eAgNnSc3d1	samotné
bylo	být	k5eAaImAgNnS	být
dávno	dávno	k6eAd1	dávno
přes	přes	k7c4	přes
padesát	padesát	k4xCc4	padesát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
mu	on	k3xPp3gMnSc3	on
Pamela	Pamela	k1gFnSc1	Pamela
zavolala	zavolat	k5eAaPmAgFnS	zavolat
a	a	k8xC	a
zeptala	zeptat	k5eAaPmAgFnS	zeptat
se	se	k3xPyFc4	se
jej	on	k3xPp3gNnSc2	on
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
by	by	kYmCp3nS	by
jej	on	k3xPp3gMnSc4	on
mohla	moct	k5eAaImAgFnS	moct
navštívit	navštívit	k5eAaPmF	navštívit
i	i	k9	i
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
kamarádkou	kamarádka	k1gFnSc7	kamarádka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
obdivovatelkou	obdivovatelka	k1gFnSc7	obdivovatelka
jeho	on	k3xPp3gNnSc2	on
díla	dílo	k1gNnSc2	dílo
a	a	k8xC	a
právě	právě	k6eAd1	právě
slaví	slavit	k5eAaImIp3nP	slavit
třicátiny	třicátin	k2eAgFnPc1d1	třicátin
<g/>
.	.	kIx.	.
</s>
<s>
Bukowski	Bukowski	k6eAd1	Bukowski
souhlasil	souhlasit	k5eAaImAgMnS	souhlasit
pod	pod	k7c7	pod
podmínkou	podmínka	k1gFnSc7	podmínka
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
sebou	se	k3xPyFc7	se
vezmou	vzít	k5eAaPmIp3nP	vzít
nějaká	nějaký	k3yIgNnPc4	nějaký
piva	pivo	k1gNnPc4	pivo
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
dívky	dívka	k1gFnPc1	dívka
se	se	k3xPyFc4	se
za	za	k7c4	za
chvíli	chvíle	k1gFnSc4	chvíle
dostavily	dostavit	k5eAaPmAgInP	dostavit
i	i	k9	i
s	s	k7c7	s
pivem	pivo	k1gNnSc7	pivo
a	a	k8xC	a
zdrogované	zdrogovaný	k2eAgMnPc4d1	zdrogovaný
<g/>
.	.	kIx.	.
</s>
<s>
Oslavenkyně	oslavenkyně	k1gFnSc1	oslavenkyně
<g/>
,	,	kIx,	,
štíhlá	štíhlý	k2eAgFnSc1d1	štíhlá
brunetka	brunetka	k1gFnSc1	brunetka
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
jmenovala	jmenovat	k5eAaBmAgFnS	jmenovat
Georgia	Georgia	k1gFnSc1	Georgia
Peckhamová-Krellnerová	Peckhamová-Krellnerová	k1gFnSc1	Peckhamová-Krellnerová
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zachycená	zachycený	k2eAgFnSc1d1	zachycená
na	na	k7c6	na
nezapomenutelné	zapomenutelný	k2eNgFnSc6d1	nezapomenutelná
fotografii	fotografia	k1gFnSc6	fotografia
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
pózuje	pózovat	k5eAaImIp3nS	pózovat
s	s	k7c7	s
Bukowskim	Bukowski	k1gNnSc7	Bukowski
před	před	k7c7	před
ledničkou	lednička	k1gFnSc7	lednička
a	a	k8xC	a
drží	držet	k5eAaImIp3nS	držet
jej	on	k3xPp3gInSc4	on
kolem	kolem	k7c2	kolem
krku	krk	k1gInSc2	krk
<g/>
.	.	kIx.	.
</s>
<s>
Pamela	Pamela	k1gFnSc1	Pamela
Millerová	Millerová	k1gFnSc1	Millerová
byla	být	k5eAaImAgFnS	být
zrzka	zrzka	k1gFnSc1	zrzka
s	s	k7c7	s
bujným	bujný	k2eAgNnSc7d1	bujné
poprsím	poprsí	k1gNnSc7	poprsí
<g/>
,	,	kIx,	,
přesně	přesně	k6eAd1	přesně
ten	ten	k3xDgInSc4	ten
typ	typ	k1gInSc4	typ
<g/>
,	,	kIx,	,
za	za	k7c7	za
nímž	jenž	k3xRgInSc7	jenž
se	se	k3xPyFc4	se
otočí	otočit	k5eAaPmIp3nS	otočit
každý	každý	k3xTgMnSc1	každý
chlap	chlap	k1gMnSc1	chlap
-	-	kIx~	-
a	a	k8xC	a
Bukowski	Bukowske	k1gFnSc4	Bukowske
nebyl	být	k5eNaImAgMnS	být
výjimkou	výjimka	k1gFnSc7	výjimka
<g/>
.	.	kIx.	.
</s>
<s>
Bláznivě	bláznivě	k6eAd1	bláznivě
se	se	k3xPyFc4	se
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
zamiloval	zamilovat	k5eAaPmAgMnS	zamilovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Pamela	Pamela	k1gFnSc1	Pamela
byla	být	k5eAaImAgFnS	být
příliš	příliš	k6eAd1	příliš
mladá	mladý	k2eAgFnSc1d1	mladá
a	a	k8xC	a
nedokázala	dokázat	k5eNaPmAgFnS	dokázat
jeho	jeho	k3xOp3gInPc4	jeho
city	cit	k1gInPc4	cit
opětovat	opětovat	k5eAaImF	opětovat
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
byla	být	k5eAaImAgFnS	být
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c6	na
drogách	droga	k1gFnPc6	droga
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
sama	sám	k3xTgMnSc4	sám
přiznává	přiznávat	k5eAaImIp3nS	přiznávat
<g/>
,	,	kIx,	,
nebrala	brát	k5eNaImAgFnS	brát
jeho	jeho	k3xOp3gInPc4	jeho
city	cit	k1gInPc4	cit
vážně	vážně	k6eAd1	vážně
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
bezstarostnou	bezstarostný	k2eAgFnSc7d1	bezstarostná
nestálou	stálý	k2eNgFnSc7d1	nestálá
holkou	holka	k1gFnSc7	holka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
dovádí	dovádět	k5eAaImIp3nS	dovádět
chlapy	chlap	k1gMnPc4	chlap
k	k	k7c3	k
šílenství	šílenství	k1gNnSc3	šílenství
<g/>
.	.	kIx.	.
</s>
<s>
Charles	Charles	k1gMnSc1	Charles
Bukowski	Bukowsk	k1gFnSc2	Bukowsk
brával	brávat	k5eAaImAgMnS	brávat
svou	svůj	k3xOyFgFnSc4	svůj
atraktivní	atraktivní	k2eAgFnSc4d1	atraktivní
přítelkyni	přítelkyně	k1gFnSc4	přítelkyně
na	na	k7c6	na
čtení	čtení	k1gNnSc6	čtení
poezie	poezie	k1gFnSc2	poezie
<g/>
,	,	kIx,	,
na	na	k7c4	na
dostihy	dostih	k1gInPc4	dostih
<g/>
,	,	kIx,	,
kupoval	kupovat	k5eAaImAgMnS	kupovat
jí	on	k3xPp3gFnSc3	on
dárky	dárek	k1gInPc4	dárek
a	a	k8xC	a
věnoval	věnovat	k5eAaPmAgMnS	věnovat
jí	jíst	k5eAaImIp3nS	jíst
mnoho	mnoho	k4c4	mnoho
básní	báseň	k1gFnPc2	báseň
(	(	kIx(	(
<g/>
např.	např.	kA	např.
sbírka	sbírka	k1gFnSc1	sbírka
Scarlet	Scarlet	k1gInSc1	Scarlet
je	být	k5eAaImIp3nS	být
celá	celý	k2eAgFnSc1d1	celá
o	o	k7c6	o
Pamele	Pamela	k1gFnSc6	Pamela
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Svých	svůj	k3xOyFgInPc2	svůj
záletů	zálet	k1gInPc2	zálet
se	se	k3xPyFc4	se
však	však	k9	však
nehodlal	hodlat	k5eNaImAgMnS	hodlat
vzdát	vzdát	k5eAaPmF	vzdát
<g/>
.	.	kIx.	.
</s>
<s>
Přijímal	přijímat	k5eAaImAgMnS	přijímat
své	svůj	k3xOyFgFnSc2	svůj
fanynky	fanynka	k1gFnSc2	fanynka
a	a	k8xC	a
neodmítal	odmítat	k5eNaImAgMnS	odmítat
ani	ani	k9	ani
ženy	žena	k1gFnPc4	žena
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
odvahu	odvaha	k1gFnSc4	odvaha
mu	on	k3xPp3gMnSc3	on
zavolat	zavolat	k5eAaPmF	zavolat
či	či	k8xC	či
napsat	napsat	k5eAaPmF	napsat
dopis	dopis	k1gInSc4	dopis
<g/>
.	.	kIx.	.
</s>
<s>
Pamela	Pamela	k1gFnSc1	Pamela
si	se	k3xPyFc3	se
začala	začít	k5eAaPmAgFnS	začít
se	s	k7c7	s
studentem	student	k1gMnSc7	student
zubního	zubní	k2eAgNnSc2d1	zubní
lékařství	lékařství	k1gNnSc2	lékařství
a	a	k8xC	a
nenamáhala	namáhat	k5eNaImAgFnS	namáhat
se	se	k3xPyFc4	se
ani	ani	k8xC	ani
rozloučit	rozloučit	k5eAaPmF	rozloučit
<g/>
.	.	kIx.	.
</s>
<s>
Bukowski	Bukowski	k6eAd1	Bukowski
měl	mít	k5eAaImAgInS	mít
opět	opět	k6eAd1	opět
pocítit	pocítit	k5eAaPmF	pocítit
nevýslovné	výslovný	k2eNgNnSc4d1	nevýslovné
utrpení	utrpení	k1gNnSc4	utrpení
nešťastné	šťastný	k2eNgFnSc2d1	nešťastná
lásky	láska	k1gFnSc2	láska
<g/>
.	.	kIx.	.
</s>
<s>
Pamela	Pamela	k1gFnSc1	Pamela
Millerová	Millerová	k1gFnSc1	Millerová
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
v	v	k7c6	v
románu	román	k1gInSc6	román
Ženy	žena	k1gFnSc2	žena
jako	jako	k8xC	jako
Tammie	Tammie	k1gFnSc2	Tammie
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Lindou	Linda	k1gFnSc7	Linda
Lee	Lea	k1gFnSc3	Lea
Beighle	Beighle	k1gFnPc3	Beighle
(	(	kIx(	(
<g/>
narozená	narozený	k2eAgFnSc1d1	narozená
roku	rok	k1gInSc2	rok
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
majitelkou	majitelka	k1gFnSc7	majitelka
restaurace	restaurace	k1gFnSc2	restaurace
se	s	k7c7	s
zdravou	zdravý	k2eAgFnSc7d1	zdravá
výživou	výživa	k1gFnSc7	výživa
a	a	k8xC	a
stoupenkyní	stoupenkyně	k1gFnSc7	stoupenkyně
Meher	Mehra	k1gFnPc2	Mehra
Baby	baba	k1gFnSc2	baba
<g/>
,	,	kIx,	,
indického	indický	k2eAgMnSc2d1	indický
náboženského	náboženský	k2eAgMnSc2d1	náboženský
vůdce	vůdce	k1gMnSc2	vůdce
se	se	k3xPyFc4	se
Charles	Charles	k1gMnSc1	Charles
Bukowski	Bukowsk	k1gFnSc2	Bukowsk
seznámil	seznámit	k5eAaPmAgMnS	seznámit
na	na	k7c4	na
čtení	čtení	k1gNnSc4	čtení
poezie	poezie	k1gFnSc2	poezie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
<g/>
.	.	kIx.	.
</s>
<s>
Linda	Linda	k1gFnSc1	Linda
Lee	Lea	k1gFnSc6	Lea
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
po	po	k7c6	po
přednášce	přednáška	k1gFnSc6	přednáška
představila	představit	k5eAaPmAgFnS	představit
a	a	k8xC	a
získala	získat	k5eAaPmAgFnS	získat
jeho	jeho	k3xOp3gNnSc4	jeho
telefonní	telefonní	k2eAgNnSc4d1	telefonní
číslo	číslo	k1gNnSc4	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Pamela	Pamela	k1gFnSc1	Pamela
Millerová	Millerová	k1gFnSc1	Millerová
byla	být	k5eAaImAgFnS	být
soustavně	soustavně	k6eAd1	soustavně
mimo	mimo	k7c4	mimo
dosah	dosah	k1gInSc4	dosah
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
se	se	k3xPyFc4	se
Charles	Charles	k1gMnSc1	Charles
vypravil	vypravit	k5eAaPmAgMnS	vypravit
do	do	k7c2	do
Lindiny	Lindin	k2eAgFnSc2d1	Lindina
restaurace	restaurace	k1gFnSc2	restaurace
Dew	Dew	k1gMnSc1	Dew
Drop	drop	k1gMnSc1	drop
Inn	Inn	k1gMnSc1	Inn
a	a	k8xC	a
tak	tak	k6eAd1	tak
začal	začít	k5eAaPmAgInS	začít
jejich	jejich	k3xOp3gInSc4	jejich
vztah	vztah	k1gInSc4	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Linda	Linda	k1gFnSc1	Linda
Lee	Lea	k1gFnSc6	Lea
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
začala	začít	k5eAaPmAgFnS	začít
pociťovat	pociťovat	k5eAaImF	pociťovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
náklonnost	náklonnost	k1gFnSc1	náklonnost
a	a	k8xC	a
přestože	přestože	k8xS	přestože
Bukowski	Bukowske	k1gFnSc4	Bukowske
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
v	v	k7c6	v
"	"	kIx"	"
<g/>
průzkumu	průzkum	k1gInSc6	průzkum
ženských	ženský	k2eAgFnPc2d1	ženská
duší	duše	k1gFnPc2	duše
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
začala	začít	k5eAaPmAgFnS	začít
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
trávit	trávit	k5eAaImF	trávit
čas	čas	k1gInSc4	čas
<g/>
.	.	kIx.	.
</s>
<s>
Starala	starat	k5eAaImAgFnS	starat
se	se	k3xPyFc4	se
o	o	k7c4	o
jeho	jeho	k3xOp3gFnSc4	jeho
životosprávu	životospráva	k1gFnSc4	životospráva
<g/>
,	,	kIx,	,
hlídala	hlídat	k5eAaImAgFnS	hlídat
jeho	jeho	k3xOp3gNnSc4	jeho
pití	pití	k1gNnSc4	pití
a	a	k8xC	a
brzy	brzy	k6eAd1	brzy
šedesátiletý	šedesátiletý	k2eAgMnSc1d1	šedesátiletý
muž	muž	k1gMnSc1	muž
si	se	k3xPyFc3	se
začínal	začínat	k5eAaImAgMnS	začínat
uvědomovat	uvědomovat	k5eAaImF	uvědomovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
výborně	výborně	k6eAd1	výborně
rozumí	rozumět	k5eAaImIp3nS	rozumět
a	a	k8xC	a
dokážou	dokázat	k5eAaPmIp3nP	dokázat
se	se	k3xPyFc4	se
společně	společně	k6eAd1	společně
bavit	bavit	k5eAaImF	bavit
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
připraven	připraven	k2eAgInSc1d1	připraven
změnit	změnit	k5eAaPmF	změnit
své	svůj	k3xOyFgInPc4	svůj
návyky	návyk	k1gInPc4	návyk
a	a	k8xC	a
začít	začít	k5eAaPmF	začít
nový	nový	k2eAgInSc4d1	nový
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Linda	Linda	k1gFnSc1	Linda
Lee	Lea	k1gFnSc6	Lea
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
dodává	dodávat	k5eAaImIp3nS	dodávat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Potom	potom	k6eAd1	potom
dokončil	dokončit	k5eAaPmAgMnS	dokončit
knihu	kniha	k1gFnSc4	kniha
(	(	kIx(	(
<g/>
Ženy	žena	k1gFnPc1	žena
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
skončil	skončit	k5eAaPmAgInS	skončit
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
a	a	k8xC	a
já	já	k3xPp1nSc1	já
zůstala	zůstat	k5eAaPmAgFnS	zůstat
<g/>
.	.	kIx.	.
</s>
<s>
Já	já	k3xPp1nSc1	já
byla	být	k5eAaImAgFnS	být
jediná	jediný	k2eAgNnPc1d1	jediné
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
měla	mít	k5eAaImAgNnP	mít
dost	dost	k6eAd1	dost
odvahy	odvaha	k1gFnSc2	odvaha
<g/>
,	,	kIx,	,
kuráže	kuráže	k?	kuráže
a	a	k8xC	a
humoru	humor	k1gInSc2	humor
zůstat	zůstat	k5eAaPmF	zůstat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Linda	Linda	k1gFnSc1	Linda
Lee	Lea	k1gFnSc3	Lea
věděla	vědět	k5eAaImAgFnS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
spolu	spolu	k6eAd1	spolu
nebudou	být	k5eNaImBp3nP	být
mít	mít	k5eAaImF	mít
děti	dítě	k1gFnPc1	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Bukowski	Bukowski	k6eAd1	Bukowski
neměl	mít	k5eNaImAgInS	mít
chuť	chuť	k1gFnSc4	chuť
projít	projít	k5eAaPmF	projít
si	se	k3xPyFc3	se
po	po	k7c6	po
šedesátce	šedesátka	k1gFnSc6	šedesátka
otcovstvím	otcovství	k1gNnSc7	otcovství
<g/>
.	.	kIx.	.
</s>
<s>
Musela	muset	k5eAaImAgFnS	muset
se	se	k3xPyFc4	se
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
-	-	kIx~	-
buď	buď	k8xC	buď
si	se	k3xPyFc3	se
jej	on	k3xPp3gMnSc4	on
vezme	vzít	k5eAaPmIp3nS	vzít
bez	bez	k7c2	bez
šance	šance	k1gFnSc2	šance
na	na	k7c4	na
potomka	potomek	k1gMnSc4	potomek
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
odejde	odejít	k5eAaPmIp3nS	odejít
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
se	se	k3xPyFc4	se
zůstat	zůstat	k5eAaPmF	zůstat
<g/>
.	.	kIx.	.
</s>
<s>
Začali	začít	k5eAaPmAgMnP	začít
žít	žít	k5eAaImF	žít
usedlým	usedlý	k2eAgInSc7d1	usedlý
životem	život	k1gInSc7	život
<g/>
.	.	kIx.	.
</s>
<s>
Linda	Linda	k1gFnSc1	Linda
Lee	Lea	k1gFnSc3	Lea
tolerovala	tolerovat	k5eAaImAgFnS	tolerovat
jeho	jeho	k3xOp3gInPc4	jeho
zažité	zažitý	k2eAgInPc4d1	zažitý
zvyky	zvyk	k1gInPc4	zvyk
-	-	kIx~	-
psaní	psaní	k1gNnSc1	psaní
do	do	k7c2	do
noci	noc	k1gFnSc2	noc
<g/>
,	,	kIx,	,
pozdní	pozdní	k2eAgNnSc1d1	pozdní
vstávání	vstávání	k1gNnSc1	vstávání
<g/>
,	,	kIx,	,
odpoledne	odpoledne	k6eAd1	odpoledne
trávená	trávený	k2eAgFnSc1d1	trávená
na	na	k7c6	na
dostizích	dostih	k1gInPc6	dostih
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jakmile	jakmile	k8xS	jakmile
sama	sám	k3xTgFnSc1	sám
začala	začít	k5eAaPmAgFnS	začít
navštěvovat	navštěvovat	k5eAaImF	navštěvovat
své	svůj	k3xOyFgMnPc4	svůj
přátele	přítel	k1gMnPc4	přítel
<g/>
,	,	kIx,	,
v	v	k7c6	v
Bukowském	Bukowské	k1gNnSc6	Bukowské
se	se	k3xPyFc4	se
ihned	ihned	k6eAd1	ihned
vznítila	vznítit	k5eAaPmAgFnS	vznítit
žárlivost	žárlivost	k1gFnSc4	žárlivost
<g/>
.	.	kIx.	.
</s>
<s>
Opět	opět	k6eAd1	opět
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
probudil	probudit	k5eAaPmAgMnS	probudit
vypěstovaný	vypěstovaný	k2eAgMnSc1d1	vypěstovaný
mizogyn	mizogyn	k1gMnSc1	mizogyn
<g/>
,	,	kIx,	,
vulgárně	vulgárně	k6eAd1	vulgárně
ji	on	k3xPp3gFnSc4	on
urážel	urážet	k5eAaPmAgMnS	urážet
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
ji	on	k3xPp3gFnSc4	on
fyzicky	fyzicky	k6eAd1	fyzicky
napadl	napadnout	k5eAaPmAgMnS	napadnout
během	během	k7c2	během
filmování	filmování	k1gNnSc2	filmování
dokumentu	dokument	k1gInSc2	dokument
The	The	k1gMnPc2	The
Charles	Charles	k1gMnSc1	Charles
Bukowski	Bukowsk	k1gFnSc2	Bukowsk
Tapes	Tapes	k1gMnSc1	Tapes
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
snímku	snímek	k1gInSc6	snímek
se	se	k3xPyFc4	se
odhalují	odhalovat	k5eAaImIp3nP	odhalovat
všechny	všechen	k3xTgFnPc1	všechen
jeho	jeho	k3xOp3gFnPc1	jeho
pokřivené	pokřivený	k2eAgFnPc1d1	pokřivená
patriarchální	patriarchální	k2eAgFnPc1d1	patriarchální
hodnoty	hodnota	k1gFnPc1	hodnota
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc4	jenž
zdědil	zdědit	k5eAaPmAgMnS	zdědit
po	po	k7c6	po
svém	svůj	k3xOyFgMnSc6	svůj
otci	otec	k1gMnSc6	otec
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
družka	družka	k1gFnSc1	družka
však	však	k9	však
po	po	k7c6	po
tolika	tolik	k4xDc6	tolik
letech	léto	k1gNnPc6	léto
již	již	k6eAd1	již
věděla	vědět	k5eAaImAgFnS	vědět
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
platí	platit	k5eAaImIp3nS	platit
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
krizí	krize	k1gFnSc7	krize
si	se	k3xPyFc3	se
prošli	projít	k5eAaPmAgMnP	projít
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1982	[number]	k4	1982
<g/>
-	-	kIx~	-
<g/>
1983	[number]	k4	1983
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
Linda	Linda	k1gFnSc1	Linda
Lee	Lea	k1gFnSc6	Lea
dokonce	dokonce	k9	dokonce
na	na	k7c4	na
čas	čas	k1gInSc4	čas
od	od	k7c2	od
partnera	partner	k1gMnSc2	partner
odstěhovala	odstěhovat	k5eAaPmAgFnS	odstěhovat
<g/>
.	.	kIx.	.
</s>
<s>
Vzali	vzít	k5eAaPmAgMnP	vzít
se	se	k3xPyFc4	se
v	v	k7c6	v
neděli	neděle	k1gFnSc6	neděle
18	[number]	k4	18
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1985	[number]	k4	1985
v	v	k7c6	v
evangelickém	evangelický	k2eAgInSc6d1	evangelický
kostele	kostel	k1gInSc6	kostel
v	v	k7c4	v
Los	los	k1gInSc4	los
Feliz	Feliz	k1gInSc4	Feliz
(	(	kIx(	(
<g/>
východně	východně	k6eAd1	východně
od	od	k7c2	od
Hollywoodu	Hollywood	k1gInSc2	Hollywood
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
přestože	přestože	k8xS	přestože
jejich	jejich	k3xOp3gNnSc1	jejich
soužití	soužití	k1gNnSc1	soužití
nebylo	být	k5eNaImAgNnS	být
zdaleka	zdaleka	k6eAd1	zdaleka
bezproblémové	bezproblémový	k2eAgNnSc1d1	bezproblémové
<g/>
,	,	kIx,	,
vydrželo	vydržet	k5eAaPmAgNnS	vydržet
až	až	k9	až
do	do	k7c2	do
Bukowského	Bukowského	k2eAgFnSc2d1	Bukowského
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Bukowski	Bukowski	k6eAd1	Bukowski
několikrát	několikrát	k6eAd1	několikrát
měnil	měnit	k5eAaImAgMnS	měnit
závěť	závěť	k1gFnSc4	závěť
<g/>
,	,	kIx,	,
až	až	k9	až
nakonec	nakonec	k6eAd1	nakonec
po	po	k7c6	po
sňatku	sňatek	k1gInSc6	sňatek
přenechal	přenechat	k5eAaPmAgMnS	přenechat
všechno	všechen	k3xTgNnSc4	všechen
Lindě	Linda	k1gFnSc3	Linda
Lee	Lea	k1gFnSc3	Lea
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
informoval	informovat	k5eAaBmAgMnS	informovat
svou	svůj	k3xOyFgFnSc4	svůj
dceru	dcera	k1gFnSc4	dcera
Marinu	Marina	k1gFnSc4	Marina
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
dům	dům	k1gInSc1	dům
v	v	k7c4	v
San	San	k1gMnSc4	San
Pedru	Pedra	k1gMnSc4	Pedra
prodělal	prodělat	k5eAaPmAgMnS	prodělat
několik	několik	k4yIc4	několik
změn	změna	k1gFnPc2	změna
<g/>
,	,	kIx,	,
postavil	postavit	k5eAaPmAgInS	postavit
se	se	k3xPyFc4	se
nový	nový	k2eAgInSc1d1	nový
plot	plot	k1gInSc1	plot
<g/>
,	,	kIx,	,
koupelna	koupelna	k1gFnSc1	koupelna
na	na	k7c6	na
zahradě	zahrada	k1gFnSc6	zahrada
a	a	k8xC	a
přístavek	přístavek	k1gInSc1	přístavek
pro	pro	k7c4	pro
Lindinu	Lindin	k2eAgFnSc4d1	Lindina
matku	matka	k1gFnSc4	matka
Honoru	honora	k1gFnSc4	honora
Beighlovou	Beighlová	k1gFnSc4	Beighlová
<g/>
.	.	kIx.	.
</s>
<s>
Bukowski	Bukowski	k6eAd1	Bukowski
cítil	cítit	k5eAaImAgMnS	cítit
<g/>
,	,	kIx,	,
že	že	k8xS	že
dokud	dokud	k8xS	dokud
bude	být	k5eAaImBp3nS	být
stará	starý	k2eAgFnSc1d1	stará
žena	žena	k1gFnSc1	žena
nechápavě	chápavě	k6eNd1	chápavě
kroutit	kroutit	k5eAaImF	kroutit
hlavou	hlava	k1gFnSc7	hlava
nad	nad	k7c7	nad
jeho	jeho	k3xOp3gFnPc7	jeho
básněmi	báseň	k1gFnPc7	báseň
a	a	k8xC	a
prózou	próza	k1gFnSc7	próza
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
tvorbou	tvorba	k1gFnSc7	tvorba
všechno	všechen	k3xTgNnSc1	všechen
v	v	k7c6	v
pořádku	pořádek	k1gInSc6	pořádek
<g/>
.	.	kIx.	.
</s>
<s>
Linda	Linda	k1gFnSc1	Linda
Lee	Lea	k1gFnSc3	Lea
Beighle	Beighle	k1gFnSc4	Beighle
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
stala	stát	k5eAaPmAgFnS	stát
předlohou	předloha	k1gFnSc7	předloha
k	k	k7c3	k
postavě	postava	k1gFnSc3	postava
Sáry	Sára	k1gFnSc2	Sára
v	v	k7c6	v
románech	román	k1gInPc6	román
Ženy	žena	k1gFnPc1	žena
a	a	k8xC	a
Hollywood	Hollywood	k1gInSc1	Hollywood
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1978	[number]	k4	1978
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Bukowski	Bukowske	k1gFnSc4	Bukowske
s	s	k7c7	s
Lindou	Linda	k1gFnSc7	Linda
Lee	Lea	k1gFnSc3	Lea
Evropu	Evropa	k1gFnSc4	Evropa
(	(	kIx(	(
<g/>
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgMnS	být
autor	autor	k1gMnSc1	autor
o	o	k7c4	o
poznání	poznání	k1gNnSc4	poznání
populárnější	populární	k2eAgFnSc1d2	populárnější
než	než	k8xS	než
v	v	k7c6	v
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
fotografem	fotograf	k1gMnSc7	fotograf
Michaelem	Michael	k1gMnSc7	Michael
Montfortem	Montfort	k1gInSc7	Montfort
jej	on	k3xPp3gMnSc4	on
doprovázel	doprovázet	k5eAaImAgMnS	doprovázet
i	i	k9	i
Carl	Carl	k1gMnSc1	Carl
Weissner	Weissner	k1gMnSc1	Weissner
-	-	kIx~	-
jeho	jeho	k3xOp3gMnSc1	jeho
přítel	přítel	k1gMnSc1	přítel
a	a	k8xC	a
německý	německý	k2eAgMnSc1d1	německý
překladatel	překladatel	k1gMnSc1	překladatel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hamburském	hamburský	k2eAgNnSc6d1	hamburské
zastřešeném	zastřešený	k2eAgNnSc6d1	zastřešené
tržišti	tržiště	k1gNnSc6	tržiště
Markthalle	Markthalle	k1gNnSc2	Markthalle
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
čtení	čtení	k1gNnSc3	čtení
poezie	poezie	k1gFnSc2	poezie
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgInS	zúčastnit
obrovský	obrovský	k2eAgInSc1d1	obrovský
počet	počet	k1gInSc1	počet
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
a	a	k8xC	a
odezva	odezva	k1gFnSc1	odezva
byla	být	k5eAaImAgFnS	být
pozitivní	pozitivní	k2eAgFnSc1d1	pozitivní
<g/>
,	,	kIx,	,
Bukowski	Bukowski	k1gNnSc7	Bukowski
už	už	k6eAd1	už
se	se	k3xPyFc4	se
vypracoval	vypracovat	k5eAaPmAgMnS	vypracovat
ve	v	k7c4	v
zkušeného	zkušený	k2eAgMnSc4d1	zkušený
recitátora	recitátor	k1gMnSc4	recitátor
a	a	k8xC	a
své	svůj	k3xOyFgInPc4	svůj
přednesy	přednes	k1gInPc4	přednes
obohacoval	obohacovat	k5eAaImAgMnS	obohacovat
různými	různý	k2eAgInPc7d1	různý
vtípky	vtípek	k1gInPc7	vtípek
a	a	k8xC	a
poznámkami	poznámka	k1gFnPc7	poznámka
<g/>
.	.	kIx.	.
</s>
<s>
Lidem	lid	k1gInSc7	lid
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
líbilo	líbit	k5eAaImAgNnS	líbit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
čtení	čtení	k1gNnSc6	čtení
navštívil	navštívit	k5eAaPmAgMnS	navštívit
spisovatel	spisovatel	k1gMnSc1	spisovatel
své	svůj	k3xOyFgNnSc4	svůj
rodné	rodný	k2eAgNnSc4d1	rodné
město	město	k1gNnSc4	město
Andernach	Andernacha	k1gFnPc2	Andernacha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
devadesátiletým	devadesátiletý	k2eAgMnSc7d1	devadesátiletý
strýcem	strýc	k1gMnSc7	strýc
Heinrichem	Heinrich	k1gMnSc7	Heinrich
Fettem	Fett	k1gMnSc7	Fett
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgNnSc7	jenž
si	se	k3xPyFc3	se
dopisoval	dopisovat	k5eAaImAgMnS	dopisovat
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
dojemné	dojemný	k2eAgNnSc1d1	dojemné
setkání	setkání	k1gNnSc1	setkání
<g/>
,	,	kIx,	,
starý	starý	k2eAgMnSc1d1	starý
pán	pán	k1gMnSc1	pán
vzal	vzít	k5eAaPmAgMnS	vzít
svého	svůj	k3xOyFgMnSc4	svůj
synovce	synovec	k1gMnSc4	synovec
na	na	k7c4	na
procházku	procházka	k1gFnSc4	procházka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mu	on	k3xPp3gMnSc3	on
ukázal	ukázat	k5eAaPmAgInS	ukázat
rodný	rodný	k2eAgInSc1d1	rodný
dům	dům	k1gInSc1	dům
<g/>
.	.	kIx.	.
</s>
<s>
Hanka	Hanka	k1gFnSc1	Hanka
Bukowského	Bukowské	k1gNnSc2	Bukowské
potěšilo	potěšit	k5eAaPmAgNnS	potěšit
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
donedávna	donedávna	k6eAd1	donedávna
v	v	k7c6	v
domě	dům	k1gInSc6	dům
sídlil	sídlit	k5eAaImAgInS	sídlit
nevěstinec	nevěstinec	k1gInSc1	nevěstinec
<g/>
.	.	kIx.	.
</s>
<s>
Večer	večer	k6eAd1	večer
se	se	k3xPyFc4	se
Charles	Charles	k1gMnSc1	Charles
odebral	odebrat	k5eAaPmAgMnS	odebrat
do	do	k7c2	do
hotelového	hotelový	k2eAgInSc2d1	hotelový
baru	bar	k1gInSc2	bar
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
popíjel	popíjet	k5eAaImAgMnS	popíjet
i	i	k9	i
Thomas	Thomas	k1gMnSc1	Thomas
Schmitt	Schmitt	k1gMnSc1	Schmitt
<g/>
,	,	kIx,	,
filmový	filmový	k2eAgMnSc1d1	filmový
režisér	režisér	k1gMnSc1	režisér
<g/>
.	.	kIx.	.
</s>
<s>
Zábava	zábava	k1gFnSc1	zábava
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
v	v	k7c6	v
hotelovém	hotelový	k2eAgInSc6d1	hotelový
pokoji	pokoj	k1gInSc6	pokoj
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
neobešla	obešnout	k5eNaPmAgFnS	obešnout
bez	bez	k7c2	bez
výtržností	výtržnost	k1gFnPc2	výtržnost
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
tyto	tento	k3xDgFnPc1	tento
události	událost	k1gFnPc1	událost
jsou	být	k5eAaImIp3nP	být
popsané	popsaný	k2eAgInPc1d1	popsaný
v	v	k7c6	v
cestopise	cestopis	k1gInSc6	cestopis
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
Never	Never	k1gMnSc1	Never
Did	Did	k1gMnSc1	Did
This	This	k1gInSc4	This
doplněné	doplněná	k1gFnSc3	doplněná
fotografiemi	fotografia	k1gFnPc7	fotografia
od	od	k7c2	od
Michaela	Michael	k1gMnSc4	Michael
Montforta	Montfort	k1gMnSc2	Montfort
<g/>
.	.	kIx.	.
</s>
<s>
Příští	příští	k2eAgFnSc4d1	příští
cestu	cesta	k1gFnSc4	cesta
podnikl	podniknout	k5eAaPmAgMnS	podniknout
Charles	Charles	k1gMnSc1	Charles
Bukowski	Bukowsk	k1gFnSc2	Bukowsk
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
byl	být	k5eAaImAgInS	být
pozván	pozvat	k5eAaPmNgInS	pozvat
Bernardem	Bernard	k1gMnSc7	Bernard
Pivotem	pivot	k1gMnSc7	pivot
<g/>
,	,	kIx,	,
moderátorem	moderátor	k1gMnSc7	moderátor
populárního	populární	k2eAgInSc2d1	populární
pořadu	pořad	k1gInSc2	pořad
Apostrophe	Apostroph	k1gFnSc2	Apostroph
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
svým	svůj	k3xOyFgNnSc7	svůj
nevybíravým	vybíravý	k2eNgNnSc7d1	nevybíravé
chováním	chování	k1gNnSc7	chování
způsobil	způsobit	k5eAaPmAgInS	způsobit
faux	faux	k1gInSc1	faux
pas	pas	k1gInSc4	pas
<g/>
.	.	kIx.	.
</s>
<s>
Hosty	host	k1gMnPc4	host
pořadu	pořad	k1gInSc6	pořad
byli	být	k5eAaImAgMnP	být
kromě	kromě	k7c2	kromě
něj	on	k3xPp3gMnSc4	on
význačný	význačný	k2eAgMnSc1d1	význačný
psychiatr	psychiatr	k1gMnSc1	psychiatr
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
kdysi	kdysi	k6eAd1	kdysi
léčil	léčit	k5eAaImAgMnS	léčit
Antonina	Antonin	k2eAgMnSc4d1	Antonin
Artauda	Artaud	k1gMnSc4	Artaud
<g/>
,	,	kIx,	,
a	a	k8xC	a
atraktivní	atraktivní	k2eAgFnSc1d1	atraktivní
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
<g/>
.	.	kIx.	.
</s>
<s>
Bukowski	Bukowski	k6eAd1	Bukowski
byl	být	k5eAaImAgInS	být
namol	namol	k?	namol
a	a	k8xC	a
pletl	plést	k5eAaImAgMnS	plést
se	se	k3xPyFc4	se
mu	on	k3xPp3gNnSc3	on
jazyk	jazyk	k1gInSc1	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
se	se	k3xPyFc4	se
moderátor	moderátor	k1gInSc1	moderátor
věnoval	věnovat	k5eAaImAgInS	věnovat
rozhovoru	rozhovor	k1gInSc2	rozhovor
s	s	k7c7	s
přítomnou	přítomný	k2eAgFnSc7d1	přítomná
spisovatelkou	spisovatelka	k1gFnSc7	spisovatelka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
přerušil	přerušit	k5eAaPmAgMnS	přerušit
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
rád	rád	k2eAgMnSc1d1	rád
viděl	vidět	k5eAaImAgMnS	vidět
více	hodně	k6eAd2	hodně
z	z	k7c2	z
jejích	její	k3xOp3gFnPc2	její
nohou	noha	k1gFnPc2	noha
<g/>
.	.	kIx.	.
</s>
<s>
Pivot	pivot	k1gInSc1	pivot
ho	on	k3xPp3gMnSc4	on
zpražil	zpražit	k5eAaPmAgInS	zpražit
pohledem	pohled	k1gInSc7	pohled
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Bukowski	Bukowsk	k1gMnPc7	Bukowsk
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
<g/>
,	,	kIx,	,
nazval	nazvat	k5eAaBmAgInS	nazvat
jej	on	k3xPp3gInSc4	on
"	"	kIx"	"
<g/>
fucking	fucking	k1gInSc4	fucking
son	son	k1gInSc1	son
of	of	k?	of
a	a	k8xC	a
fucking	fucking	k1gInSc1	fucking
bitch	bitcha	k1gFnPc2	bitcha
asshole	asshole	k1gFnSc2	asshole
<g/>
"	"	kIx"	"
v	v	k7c6	v
živém	živý	k2eAgNnSc6d1	živé
vysílání	vysílání	k1gNnSc6	vysílání
<g/>
,	,	kIx,	,
strhl	strhnout	k5eAaPmAgMnS	strhnout
si	se	k3xPyFc3	se
z	z	k7c2	z
ucha	ucho	k1gNnSc2	ucho
překladatelské	překladatelský	k2eAgNnSc1d1	překladatelské
sluchátko	sluchátko	k1gNnSc1	sluchátko
a	a	k8xC	a
odvrávoral	odvrávorat	k5eAaPmAgMnS	odvrávorat
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
musel	muset	k5eAaImAgMnS	muset
přidržet	přidržet	k5eAaPmF	přidržet
o	o	k7c4	o
hlavu	hlava	k1gFnSc4	hlava
hosta	host	k1gMnSc4	host
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
udržel	udržet	k5eAaPmAgInS	udržet
na	na	k7c6	na
nohou	noha	k1gFnPc6	noha
<g/>
.	.	kIx.	.
</s>
<s>
Publikum	publikum	k1gNnSc1	publikum
se	se	k3xPyFc4	se
válelo	válet	k5eAaImAgNnS	válet
smíchy	smíchy	k6eAd1	smíchy
<g/>
.	.	kIx.	.
</s>
<s>
Hank	Hank	k1gMnSc1	Hank
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
početným	početný	k2eAgInSc7d1	početný
hulvátským	hulvátský	k2eAgInSc7d1	hulvátský
doprovodem	doprovod	k1gInSc7	doprovod
odcházel	odcházet	k5eAaImAgMnS	odcházet
<g/>
,	,	kIx,	,
a	a	k8xC	a
když	když	k8xS	když
u	u	k7c2	u
dveří	dveře	k1gFnPc2	dveře
zaregistroval	zaregistrovat	k5eAaPmAgInS	zaregistrovat
ozbrojenou	ozbrojený	k2eAgFnSc4d1	ozbrojená
ochranku	ochranka	k1gFnSc4	ochranka
<g/>
,	,	kIx,	,
vytasil	vytasit	k5eAaPmAgMnS	vytasit
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
komicky	komicky	k6eAd1	komicky
nůž	nůž	k1gInSc4	nůž
<g/>
.	.	kIx.	.
</s>
<s>
Ochranka	ochranka	k1gFnSc1	ochranka
jej	on	k3xPp3gInSc4	on
rutinně	rutinně	k6eAd1	rutinně
vyhodila	vyhodit	k5eAaPmAgFnS	vyhodit
na	na	k7c6	na
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Nešetřilo	šetřit	k5eNaImAgNnS	šetřit
se	se	k3xPyFc4	se
fotografiemi	fotografia	k1gFnPc7	fotografia
a	a	k8xC	a
Bukowski	Bukowski	k1gNnSc7	Bukowski
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
přes	přes	k7c4	přes
noc	noc	k1gFnSc4	noc
slavným	slavný	k2eAgInSc7d1	slavný
<g/>
.	.	kIx.	.
</s>
<s>
Lidem	člověk	k1gMnPc3	člověk
se	se	k3xPyFc4	se
líbilo	líbit	k5eAaImAgNnS	líbit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Hank	Hank	k1gMnSc1	Hank
narušil	narušit	k5eAaPmAgMnS	narušit
elitářskou	elitářský	k2eAgFnSc4d1	elitářská
atmosféru	atmosféra	k1gFnSc4	atmosféra
pořadu	pořad	k1gInSc2	pořad
Apostrophe	Apostroph	k1gFnSc2	Apostroph
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc4d1	další
den	den	k1gInSc4	den
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gNnSc2	jeho
díla	dílo	k1gNnSc2	dílo
vyprodána	vyprodat	k5eAaPmNgFnS	vyprodat
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
francouzských	francouzský	k2eAgInPc2d1	francouzský
knihkupectvích	knihkupectví	k1gNnPc6	knihkupectví
<g/>
.	.	kIx.	.
</s>
<s>
Charles	Charles	k1gMnSc1	Charles
Bukowski	Bukowsk	k1gFnSc2	Bukowsk
se	se	k3xPyFc4	se
sešel	sejít	k5eAaPmAgMnS	sejít
i	i	k9	i
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
idolem	idol	k1gInSc7	idol
Johnem	John	k1gMnSc7	John
Fantem	fant	k1gInSc7	fant
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnPc4	jehož
knihy	kniha	k1gFnPc4	kniha
v	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
tolik	tolik	k6eAd1	tolik
obdivoval	obdivovat	k5eAaImAgMnS	obdivovat
<g/>
.	.	kIx.	.
</s>
<s>
John	John	k1gMnSc1	John
Fante	fant	k1gInSc5	fant
už	už	k6eAd1	už
řadu	řada	k1gFnSc4	řada
let	léto	k1gNnPc2	léto
trpěl	trpět	k5eAaImAgMnS	trpět
cukrovkou	cukrovka	k1gFnSc7	cukrovka
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
(	(	kIx(	(
<g/>
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
69	[number]	k4	69
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
oslepl	oslepnout	k5eAaPmAgInS	oslepnout
<g/>
.	.	kIx.	.
</s>
<s>
Prodělal	prodělat	k5eAaPmAgMnS	prodělat
i	i	k9	i
amputaci	amputace	k1gFnSc4	amputace
obou	dva	k4xCgFnPc2	dva
nohou	noha	k1gFnPc2	noha
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
v	v	k7c6	v
sobě	se	k3xPyFc3	se
ještě	ještě	k9	ještě
dokázal	dokázat	k5eAaPmAgMnS	dokázat
najít	najít	k5eAaPmF	najít
sílu	síla	k1gFnSc4	síla
bojovat	bojovat	k5eAaImF	bojovat
se	se	k3xPyFc4	se
životem	život	k1gInSc7	život
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
nezdolnost	nezdolnost	k1gFnSc4	nezdolnost
Bukowski	Bukowsk	k1gFnSc2	Bukowsk
velmi	velmi	k6eAd1	velmi
oceňoval	oceňovat	k5eAaImAgMnS	oceňovat
a	a	k8xC	a
zařídil	zařídit	k5eAaPmAgMnS	zařídit
Fantemu	Fantemo	k1gNnSc6	Fantemo
nová	nový	k2eAgNnPc1d1	nové
vydání	vydání	k1gNnPc1	vydání
jeho	jeho	k3xOp3gFnPc2	jeho
knih	kniha	k1gFnPc2	kniha
u	u	k7c2	u
svého	svůj	k3xOyFgMnSc2	svůj
vydavatele	vydavatel	k1gMnSc2	vydavatel
Black	Black	k1gMnSc1	Black
Sparrow	Sparrow	k1gMnSc1	Sparrow
Press	Press	k1gInSc4	Press
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
oběma	dva	k4xCgMnPc7	dva
spisovateli	spisovatel	k1gMnPc7	spisovatel
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
silné	silný	k2eAgNnSc4d1	silné
přátelské	přátelský	k2eAgNnSc4d1	přátelské
pouto	pouto	k1gNnSc4	pouto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
se	se	k3xPyFc4	se
s	s	k7c7	s
Lindou	Linda	k1gFnSc7	Linda
Lee	Lea	k1gFnSc6	Lea
Bukowski	Bukowsk	k1gFnSc3	Bukowsk
přestěhoval	přestěhovat	k5eAaPmAgInS	přestěhovat
do	do	k7c2	do
staršího	starý	k2eAgInSc2d2	starší
dvouposchoďového	dvouposchoďový	k2eAgInSc2d1	dvouposchoďový
domu	dům	k1gInSc2	dům
v	v	k7c6	v
San	San	k1gFnSc6	San
Pedru	Pedro	k1gNnSc6	Pedro
<g/>
,	,	kIx,	,
přístavním	přístavní	k2eAgNnSc6d1	přístavní
městě	město	k1gNnSc6	město
na	na	k7c6	na
konci	konec	k1gInSc6	konec
megapole	megapole	k1gFnSc2	megapole
Los	los	k1gMnSc1	los
Angeles	Angeles	k1gMnSc1	Angeles
a	a	k8xC	a
koupil	koupit	k5eAaPmAgMnS	koupit
si	se	k3xPyFc3	se
bazén	bazén	k1gInSc4	bazén
a	a	k8xC	a
nový	nový	k2eAgInSc4d1	nový
automobil	automobil	k1gInSc4	automobil
<g/>
,	,	kIx,	,
černé	černá	k1gFnPc4	černá
BMW	BMW	kA	BMW
<g/>
.	.	kIx.	.
</s>
<s>
Definitivně	definitivně	k6eAd1	definitivně
opustil	opustit	k5eAaPmAgInS	opustit
svět	svět	k1gInSc1	svět
chudých	chudý	k1gMnPc2	chudý
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
mu	on	k3xPp3gMnSc3	on
zajistil	zajistit	k5eAaPmAgMnS	zajistit
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
proslulost	proslulost	k1gFnSc4	proslulost
<g/>
.	.	kIx.	.
</s>
<s>
Tou	ten	k3xDgFnSc7	ten
dobou	doba	k1gFnSc7	doba
jej	on	k3xPp3gMnSc4	on
kontaktoval	kontaktovat	k5eAaImAgInS	kontaktovat
francouzský	francouzský	k2eAgMnSc1d1	francouzský
filmový	filmový	k2eAgMnSc1d1	filmový
režisér	režisér	k1gMnSc1	režisér
íránského	íránský	k2eAgInSc2d1	íránský
původu	původ	k1gInSc2	původ
Barbet	Barbet	k1gMnSc1	Barbet
Schroeder	Schroeder	k1gMnSc1	Schroeder
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
chtěl	chtít	k5eAaImAgMnS	chtít
natočit	natočit	k5eAaBmF	natočit
film	film	k1gInSc4	film
na	na	k7c4	na
námět	námět	k1gInSc4	námět
některé	některý	k3yIgFnPc4	některý
z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
povídek	povídka	k1gFnPc2	povídka
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
Bukowski	Bukowski	k1gNnSc4	Bukowski
souhlasil	souhlasit	k5eAaImAgMnS	souhlasit
s	s	k7c7	s
napsáním	napsání	k1gNnSc7	napsání
scénáře	scénář	k1gInSc2	scénář
k	k	k7c3	k
filmu	film	k1gInSc3	film
Štamgast	štamgast	k1gMnSc1	štamgast
<g/>
.	.	kIx.	.
</s>
<s>
Snímku	snímek	k1gInSc3	snímek
předcházel	předcházet	k5eAaImAgInS	předcházet
Schroederův	Schroederův	k2eAgInSc1d1	Schroederův
dokumentární	dokumentární	k2eAgInSc1d1	dokumentární
film	film	k1gInSc1	film
(	(	kIx(	(
<g/>
240	[number]	k4	240
min	mina	k1gFnPc2	mina
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Charles	Charles	k1gMnSc1	Charles
Bukowski	Bukowsk	k1gFnSc2	Bukowsk
Tapes	Tapes	k1gInSc1	Tapes
o	o	k7c6	o
spisovateli	spisovatel	k1gMnSc6	spisovatel
hovořícím	hovořící	k2eAgMnSc6d1	hovořící
o	o	k7c6	o
svém	svůj	k3xOyFgInSc6	svůj
životě	život	k1gInSc6	život
a	a	k8xC	a
práci	práce	k1gFnSc6	práce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
získal	získat	k5eAaPmAgInS	získat
práva	právo	k1gNnPc4	právo
na	na	k7c4	na
několik	několik	k4yIc4	několik
povídek	povídka	k1gFnPc2	povídka
italský	italský	k2eAgMnSc1d1	italský
režisér	režisér	k1gMnSc1	režisér
Marco	Marco	k1gMnSc1	Marco
Ferreri	Ferreri	k1gNnSc4	Ferreri
a	a	k8xC	a
natočil	natočit	k5eAaBmAgMnS	natočit
film	film	k1gInSc4	film
Erekce	erekce	k1gFnSc2	erekce
<g/>
,	,	kIx,	,
ejakulace	ejakulace	k1gFnSc2	ejakulace
<g/>
,	,	kIx,	,
exhibice	exhibice	k1gFnPc4	exhibice
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
příběhy	příběh	k1gInPc4	příběh
obyčejného	obyčejný	k2eAgNnSc2d1	obyčejné
šílenství	šílenství	k1gNnSc2	šílenství
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
Tales	Tales	k1gInSc1	Tales
of	of	k?	of
Ordinary	Ordinara	k1gFnSc2	Ordinara
Madness	Madnessa	k1gFnPc2	Madnessa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
čerpá	čerpat	k5eAaImIp3nS	čerpat
především	především	k9	především
z	z	k7c2	z
povídky	povídka	k1gFnSc2	povídka
Nejkrásnější	krásný	k2eAgFnSc1d3	nejkrásnější
ženská	ženská	k1gFnSc1	ženská
ve	v	k7c6	v
městě	město	k1gNnSc6	město
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
The	The	k1gFnSc1	The
most	most	k1gInSc1	most
Beautiful	Beautiful	k1gInSc1	Beautiful
Woman	Woman	k1gInSc1	Woman
in	in	k?	in
Town	Town	k1gInSc1	Town
<g/>
)	)	kIx)	)
o	o	k7c6	o
prostitutce	prostitutka	k1gFnSc6	prostitutka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
zohaví	zohavit	k5eAaPmIp3nS	zohavit
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
spáchá	spáchat	k5eAaPmIp3nS	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
evropským	evropský	k2eAgInSc7d1	evropský
filmem	film	k1gInSc7	film
těžícím	těžící	k2eAgInSc7d1	těžící
z	z	k7c2	z
Bukowského	Bukowského	k2eAgFnPc2d1	Bukowského
povídek	povídka	k1gFnPc2	povídka
byl	být	k5eAaImAgInS	být
snímek	snímek	k1gInSc1	snímek
Crazy	Craza	k1gFnSc2	Craza
Love	lov	k1gInSc5	lov
(	(	kIx(	(
<g/>
jiný	jiný	k2eAgInSc4d1	jiný
název	název	k1gInSc4	název
Love	lov	k1gInSc5	lov
Is	Is	k1gFnPc2	Is
a	a	k8xC	a
Dog	doga	k1gFnPc2	doga
From	From	k1gMnSc1	From
Hell	Hell	k1gMnSc1	Hell
<g/>
)	)	kIx)	)
belgického	belgický	k2eAgMnSc2d1	belgický
režiséra	režisér	k1gMnSc2	režisér
Dominiqua	Dominiquus	k1gMnSc2	Dominiquus
Deruddera	Derudder	k1gMnSc2	Derudder
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
spisovatel	spisovatel	k1gMnSc1	spisovatel
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
nejzdařilejší	zdařilý	k2eAgFnSc4d3	nejzdařilejší
adaptaci	adaptace	k1gFnSc4	adaptace
svého	svůj	k3xOyFgNnSc2	svůj
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Vychází	vycházet	k5eAaImIp3nP	vycházet
sbírky	sbírka	k1gFnPc1	sbírka
básní	báseň	k1gFnPc2	báseň
Hrej	hrát	k5eAaImRp2nS	hrát
opile	opile	k6eAd1	opile
na	na	k7c6	na
piano	piano	k6eAd1	piano
<g/>
/	/	kIx~	/
jak	jak	k8xS	jak
na	na	k7c4	na
bicí	bicí	k2eAgMnPc4d1	bicí
<g/>
/	/	kIx~	/
dokud	dokud	k8xS	dokud
ti	ten	k3xDgMnPc1	ten
z	z	k7c2	z
prstů	prst	k1gInPc2	prst
neucákne	ucáknout	k5eNaPmIp3nS	ucáknout
krev	krev	k1gFnSc1	krev
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
Play	play	k0	play
the	the	k?	the
Piano	piano	k1gNnSc1	piano
Drunk	Drunk	k1gMnSc1	Drunk
<g/>
/	/	kIx~	/
Like	Like	k1gInSc1	Like
a	a	k8xC	a
Percussion	Percussion	k1gInSc1	Percussion
Instrument	instrument	k1gInSc1	instrument
<g/>
/	/	kIx~	/
Until	Until	k1gInSc1	Until
the	the	k?	the
Fingers	Fingers	k1gInSc1	Fingers
Begin	Begin	k1gInSc1	Begin
to	ten	k3xDgNnSc1	ten
Bleed	Bleed	k1gMnSc1	Bleed
a	a	k8xC	a
Bit	bit	k2eAgMnSc1d1	bit
<g/>
)	)	kIx)	)
a	a	k8xC	a
Válka	válka	k1gFnSc1	válka
bez	bez	k7c2	bez
konce	konec	k1gInSc2	konec
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
War	War	k1gMnSc1	War
All	All	k1gMnSc1	All
the	the	k?	the
Time	Time	k1gInSc1	Time
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
autobiografický	autobiografický	k2eAgInSc1d1	autobiografický
román	román	k1gInSc1	román
o	o	k7c6	o
špatném	špatný	k2eAgNnSc6d1	špatné
dětství	dětství	k1gNnSc6	dětství
Šunkový	šunkový	k2eAgInSc1d1	šunkový
nářez	nářez	k1gInSc1	nářez
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
Ham	ham	k0	ham
on	on	k3xPp3gMnSc1	on
rye	rye	k?	rye
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sbírka	sbírka	k1gFnSc1	sbírka
povídek	povídka	k1gFnPc2	povídka
Těžký	těžký	k2eAgInSc1d1	těžký
časy	čas	k1gInPc1	čas
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
Hot	hot	k0	hot
Water	Watra	k1gFnPc2	Watra
Music	Musice	k1gFnPc2	Musice
<g/>
)	)	kIx)	)
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Bukowski	Bukowski	k6eAd1	Bukowski
má	mít	k5eAaImIp3nS	mít
spoustu	spousta	k1gFnSc4	spousta
času	čas	k1gInSc2	čas
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
tvorbu	tvorba	k1gFnSc4	tvorba
<g/>
,	,	kIx,	,
jediné	jediné	k1gNnSc4	jediné
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
jej	on	k3xPp3gMnSc4	on
dokáže	dokázat	k5eAaPmIp3nS	dokázat
od	od	k7c2	od
ní	on	k3xPp3gFnSc2	on
odtrhnout	odtrhnout	k5eAaPmF	odtrhnout
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
oblíbené	oblíbený	k2eAgInPc1d1	oblíbený
dostihové	dostihový	k2eAgInPc1d1	dostihový
závody	závod	k1gInPc1	závod
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
uvedení	uvedení	k1gNnSc6	uvedení
filmu	film	k1gInSc2	film
Štamgast	štamgast	k1gMnSc1	štamgast
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
i	i	k9	i
s	s	k7c7	s
hollywoodskou	hollywoodský	k2eAgFnSc7d1	hollywoodská
filmovou	filmový	k2eAgFnSc7d1	filmová
garniturou	garnitura	k1gFnSc7	garnitura
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
své	svůj	k3xOyFgMnPc4	svůj
přátele	přítel	k1gMnPc4	přítel
řadil	řadit	k5eAaImAgMnS	řadit
Seanna	Seann	k1gInSc2	Seann
Penna	Penn	k1gMnSc2	Penn
i	i	k8xC	i
Mickeyho	Mickey	k1gMnSc2	Mickey
Rourka	rourka	k1gFnSc1	rourka
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
hrál	hrát	k5eAaImAgInS	hrát
ve	v	k7c6	v
filmu	film	k1gInSc6	film
postavu	postav	k1gInSc2	postav
Henryho	Henry	k1gMnSc2	Henry
Chinaskiho	Chinaski	k1gMnSc2	Chinaski
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgInSc1	sám
Bukowski	Bukowske	k1gFnSc4	Bukowske
neměl	mít	k5eNaImAgInS	mít
kinematografii	kinematografie	k1gFnSc4	kinematografie
příliš	příliš	k6eAd1	příliš
v	v	k7c6	v
oblibě	obliba	k1gFnSc6	obliba
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
několik	několik	k4yIc4	několik
málo	málo	k4c4	málo
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
dokázal	dokázat	k5eAaPmAgMnS	dokázat
ocenit	ocenit	k5eAaPmF	ocenit
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
Kdo	kdo	k3yQnSc1	kdo
se	se	k3xPyFc4	se
bojí	bát	k5eAaImIp3nP	bát
Virginie	Virginie	k1gFnPc1	Virginie
Woolfové	Woolfový	k2eAgFnPc1d1	Woolfová
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
,	,	kIx,	,
Na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
frontě	fronta	k1gFnSc6	fronta
klid	klid	k1gInSc4	klid
a	a	k8xC	a
zejména	zejména	k9	zejména
Mazací	mazací	k2eAgFnSc1d1	mazací
hlava	hlava	k1gFnSc1	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
1983	[number]	k4	1983
Bukowski	Bukowski	k1gNnPc2	Bukowski
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
z	z	k7c2	z
hollywoodských	hollywoodský	k2eAgInPc2d1	hollywoodský
večírků	večírek	k1gInPc2	večírek
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
popíjel	popíjet	k5eAaImAgMnS	popíjet
s	s	k7c7	s
Gundolfem	Gundolf	k1gMnSc7	Gundolf
Freyermuthem	Freyermuth	k1gInSc7	Freyermuth
<g/>
,	,	kIx,	,
spatřil	spatřit	k5eAaPmAgMnS	spatřit
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
dění	dění	k1gNnSc2	dění
Arnolda	Arnold	k1gMnSc2	Arnold
Schwarzeneggera	Schwarzenegger	k1gMnSc2	Schwarzenegger
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
nevěstou	nevěsta	k1gFnSc7	nevěsta
Marií	Maria	k1gFnSc7	Maria
Shriverovou	Shriverová	k1gFnSc7	Shriverová
<g/>
,	,	kIx,	,
zvedl	zvednout	k5eAaPmAgMnS	zvednout
se	se	k3xPyFc4	se
ze	z	k7c2	z
židle	židle	k1gFnSc2	židle
a	a	k8xC	a
zamířil	zamířit	k5eAaPmAgMnS	zamířit
k	k	k7c3	k
jejich	jejich	k3xOp3gInSc3	jejich
stolu	stol	k1gInSc3	stol
<g/>
.	.	kIx.	.
</s>
<s>
Spustil	spustit	k5eAaPmAgMnS	spustit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Ty	ten	k3xDgFnPc1	ten
blbečku	blbeček	k1gMnSc3	blbeček
<g/>
!	!	kIx.	!
</s>
<s>
Co	co	k3yQnSc1	co
si	se	k3xPyFc3	se
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
myslíš	myslet	k5eAaImIp2nS	myslet
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
svým	svůj	k3xOyFgMnSc7	svůj
připitomělým	připitomělý	k2eAgInSc7d1	připitomělý
tlustým	tlustý	k2eAgInSc7d1	tlustý
doutníkem	doutník	k1gInSc7	doutník
<g/>
?	?	kIx.	?
</s>
<s>
Na	na	k7c6	na
tobě	ty	k3xPp2nSc6	ty
přece	přece	k9	přece
není	být	k5eNaImIp3nS	být
nic	nic	k6eAd1	nic
výjimečnýho	výjimečnýho	k?	výjimečnýho
jenom	jenom	k6eAd1	jenom
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
točíš	točit	k5eAaImIp2nS	točit
ty	ten	k3xDgInPc4	ten
ubohoučký	ubohoučký	k2eAgInSc4d1	ubohoučký
filmečky	filmeček	k1gInPc4	filmeček
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Schwarzenegger	Schwarzenegger	k1gMnSc1	Schwarzenegger
se	se	k3xPyFc4	se
nenechal	nechat	k5eNaPmAgMnS	nechat
vytočit	vytočit	k5eAaPmF	vytočit
a	a	k8xC	a
zůstal	zůstat	k5eAaPmAgMnS	zůstat
v	v	k7c6	v
klidu	klid	k1gInSc6	klid
<g/>
.	.	kIx.	.
</s>
<s>
Bukowski	Bukowski	k6eAd1	Bukowski
zřejmě	zřejmě	k6eAd1	zřejmě
netušil	tušit	k5eNaImAgMnS	tušit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Arnold	Arnold	k1gMnSc1	Arnold
-	-	kIx~	-
bývalý	bývalý	k2eAgMnSc1d1	bývalý
Mr	Mr	k1gMnSc1	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Universe	Universe	k1gFnSc1	Universe
-	-	kIx~	-
byl	být	k5eAaImAgInS	být
přítelem	přítel	k1gMnSc7	přítel
Harolda	Harold	k1gMnSc2	Harold
Norseho	Norse	k1gMnSc2	Norse
a	a	k8xC	a
také	také	k9	také
náruživým	náruživý	k2eAgMnSc7d1	náruživý
čtenářem	čtenář	k1gMnSc7	čtenář
moderní	moderní	k2eAgFnSc2d1	moderní
poezie	poezie	k1gFnSc2	poezie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
se	se	k3xPyFc4	se
Bukowskému	Bukowský	k1gMnSc3	Bukowský
začal	začít	k5eAaPmAgInS	začít
zhoršovat	zhoršovat	k5eAaImF	zhoršovat
zdravotní	zdravotní	k2eAgInSc1d1	zdravotní
stav	stav	k1gInSc1	stav
<g/>
.	.	kIx.	.
</s>
<s>
Zjistilo	zjistit	k5eAaPmAgNnS	zjistit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
TBC	TBC	kA	TBC
<g/>
.	.	kIx.	.
</s>
<s>
Musel	muset	k5eAaImAgInS	muset
brát	brát	k5eAaImF	brát
antibiotika	antibiotikum	k1gNnPc4	antibiotikum
a	a	k8xC	a
hodně	hodně	k6eAd1	hodně
ztratil	ztratit	k5eAaPmAgMnS	ztratit
na	na	k7c6	na
váze	váha	k1gFnSc6	váha
<g/>
.	.	kIx.	.
</s>
<s>
Díky	dík	k1gInPc1	dík
nemoci	nemoc	k1gFnSc2	nemoc
však	však	k9	však
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
dokáže	dokázat	k5eAaPmIp3nS	dokázat
obejít	obejít	k5eAaPmF	obejít
bez	bez	k7c2	bez
alkoholu	alkohol	k1gInSc2	alkohol
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
psát	psát	k5eAaImF	psát
co	co	k9	co
nejsevřenější	sevřený	k2eAgInPc4d3	sevřený
texty	text	k1gInPc4	text
<g/>
,	,	kIx,	,
s	s	k7c7	s
minimem	minimum	k1gNnSc7	minimum
fabulace	fabulace	k1gFnSc2	fabulace
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
manželky	manželka	k1gFnSc2	manželka
Lindy	Linda	k1gFnSc2	Linda
Lee	Lea	k1gFnSc3	Lea
a	a	k8xC	a
dcery	dcera	k1gFnSc2	dcera
Mariny	Marina	k1gFnSc2	Marina
dostal	dostat	k5eAaPmAgMnS	dostat
k	k	k7c3	k
Vánocům	Vánoce	k1gFnPc3	Vánoce
počítač	počítač	k1gInSc4	počítač
<g/>
,	,	kIx,	,
a	a	k8xC	a
jakmile	jakmile	k8xS	jakmile
si	se	k3xPyFc3	se
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
zvykl	zvyknout	k5eAaPmAgMnS	zvyknout
<g/>
,	,	kIx,	,
nemohl	moct	k5eNaImAgMnS	moct
si	se	k3xPyFc3	se
jej	on	k3xPp3gInSc4	on
vynachválit	vynachválit	k5eAaPmF	vynachválit
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
němu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgMnS	být
schopen	schopen	k2eAgMnSc1d1	schopen
znásobit	znásobit	k5eAaPmF	znásobit
svou	svůj	k3xOyFgFnSc4	svůj
literární	literární	k2eAgFnSc4d1	literární
produktivitu	produktivita	k1gFnSc4	produktivita
<g/>
.	.	kIx.	.
</s>
<s>
Vznikly	vzniknout	k5eAaPmAgInP	vzniknout
romány	román	k1gInPc1	román
Hollywood	Hollywood	k1gInSc1	Hollywood
a	a	k8xC	a
Škvár	škvár	k1gInSc1	škvár
(	(	kIx(	(
<g/>
jenž	jenž	k3xRgMnSc1	jenž
dokončil	dokončit	k5eAaPmAgInS	dokončit
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
svou	svůj	k3xOyFgFnSc7	svůj
smrtí	smrt	k1gFnSc7	smrt
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kniha	kniha	k1gFnSc1	kniha
básní	báseň	k1gFnPc2	báseň
a	a	k8xC	a
povídek	povídka	k1gFnPc2	povídka
Sedmdesátiletý	sedmdesátiletý	k2eAgInSc4d1	sedmdesátiletý
guláš	guláš	k1gInSc4	guláš
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
Septuagenarian	Septuagenarian	k1gMnSc1	Septuagenarian
Stew	Stew	k1gMnSc1	Stew
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
deník	deník	k1gInSc1	deník
Kapitán	kapitán	k1gMnSc1	kapitán
odešel	odejít	k5eAaPmAgMnS	odejít
na	na	k7c4	na
oběd	oběd	k1gInSc4	oběd
a	a	k8xC	a
námořníci	námořník	k1gMnPc1	námořník
převzali	převzít	k5eAaPmAgMnP	převzít
velení	velený	k2eAgMnPc1d1	velený
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
The	The	k1gMnSc1	The
Captain	Captain	k1gMnSc1	Captain
Is	Is	k1gMnSc1	Is
Out	Out	k1gMnSc1	Out
to	ten	k3xDgNnSc1	ten
Lunch	lunch	k1gInSc1	lunch
and	and	k?	and
the	the	k?	the
Sailors	Sailors	k1gInSc1	Sailors
Have	Have	k1gNnSc1	Have
Taken	Taken	k1gInSc1	Taken
Over	Over	k1gMnSc1	Over
the	the	k?	the
Ship	Ship	k1gMnSc1	Ship
<g/>
)	)	kIx)	)
a	a	k8xC	a
mnoho	mnoho	k4c4	mnoho
básní	báseň	k1gFnPc2	báseň
vydaných	vydaný	k2eAgFnPc2d1	vydaná
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
sbírkách	sbírka	k1gFnPc6	sbírka
<g/>
.	.	kIx.	.
</s>
<s>
Popularita	popularita	k1gFnSc1	popularita
měla	mít	k5eAaImAgFnS	mít
i	i	k9	i
svou	svůj	k3xOyFgFnSc4	svůj
stinnou	stinný	k2eAgFnSc4d1	stinná
stránku	stránka	k1gFnSc4	stránka
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
neměl	mít	k5eNaImAgInS	mít
zájem	zájem	k1gInSc1	zájem
přijímat	přijímat	k5eAaImF	přijímat
návštěvy	návštěva	k1gFnPc4	návštěva
a	a	k8xC	a
fanoušky	fanoušek	k1gMnPc4	fanoušek
a	a	k8xC	a
pořádat	pořádat	k5eAaImF	pořádat
divoké	divoký	k2eAgInPc4d1	divoký
mejdany	mejdan	k1gInPc4	mejdan
jako	jako	k8xS	jako
dřív	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
<s>
Jediné	jediné	k1gNnSc1	jediné
<g/>
,	,	kIx,	,
po	po	k7c6	po
čem	co	k3yInSc6	co
toužil	toužit	k5eAaImAgMnS	toužit
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
klid	klid	k1gInSc1	klid
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stáří	stáří	k1gNnSc6	stáří
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnPc1	jeho
potřeby	potřeba	k1gFnPc1	potřeba
přirozeně	přirozeně	k6eAd1	přirozeně
změnily	změnit	k5eAaPmAgFnP	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
zůstával	zůstávat	k5eAaImAgInS	zůstávat
existencialistou	existencialista	k1gMnSc7	existencialista
a	a	k8xC	a
pozorovatelem	pozorovatel	k1gMnSc7	pozorovatel
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
se	se	k3xPyFc4	se
cítil	cítit	k5eAaImAgMnS	cítit
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
ztuhlý	ztuhlý	k2eAgMnSc1d1	ztuhlý
muž	muž	k1gMnSc1	muž
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přítomnost	přítomnost	k1gFnSc1	přítomnost
konce	konec	k1gInSc2	konec
dodávala	dodávat	k5eAaImAgFnS	dodávat
jeho	jeho	k3xOp3gFnSc3	jeho
tvorbě	tvorba	k1gFnSc3	tvorba
nádech	nádech	k1gInSc4	nádech
filozofična	filozofično	k1gNnSc2	filozofično
a	a	k8xC	a
čehosi	cosi	k3yInSc2	cosi
nepostižitelného	postižitelný	k2eNgNnSc2d1	nepostižitelné
<g/>
,	,	kIx,	,
jemných	jemný	k2eAgFnPc2d1	jemná
nuancí	nuance	k1gFnPc2	nuance
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
bychom	by	kYmCp1nP	by
dříve	dříve	k6eAd2	dříve
marně	marně	k6eAd1	marně
hledali	hledat	k5eAaImAgMnP	hledat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1993	[number]	k4	1993
opět	opět	k6eAd1	opět
onemocněl	onemocnět	k5eAaPmAgInS	onemocnět
-	-	kIx~	-
tentokrát	tentokrát	k6eAd1	tentokrát
myelogenní	myelogenní	k2eAgFnSc7d1	myelogenní
leukémií	leukémie	k1gFnSc7	leukémie
<g/>
.	.	kIx.	.
</s>
<s>
Podstoupil	podstoupit	k5eAaPmAgInS	podstoupit
chemoterapii	chemoterapie	k1gFnSc4	chemoterapie
a	a	k8xC	a
nemoc	nemoc	k1gFnSc4	nemoc
trochu	trochu	k6eAd1	trochu
ustoupila	ustoupit	k5eAaPmAgFnS	ustoupit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
1993	[number]	k4	1993
se	se	k3xPyFc4	se
vrátila	vrátit	k5eAaPmAgFnS	vrátit
a	a	k8xC	a
sebrala	sebrat	k5eAaPmAgFnS	sebrat
Bukowskému	Bukowský	k2eAgInSc3d1	Bukowský
sílu	síl	k1gInSc3	síl
dále	daleko	k6eAd2	daleko
tvořit	tvořit	k5eAaImF	tvořit
<g/>
.	.	kIx.	.
</s>
<s>
Chemoterapie	chemoterapie	k1gFnSc1	chemoterapie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
bojovala	bojovat	k5eAaImAgFnS	bojovat
s	s	k7c7	s
narušenými	narušený	k2eAgFnPc7d1	narušená
krvinkami	krvinka	k1gFnPc7	krvinka
<g/>
,	,	kIx,	,
působila	působit	k5eAaImAgFnS	působit
zhoubně	zhoubně	k6eAd1	zhoubně
i	i	k8xC	i
na	na	k7c4	na
jeho	on	k3xPp3gInSc4	on
imunitní	imunitní	k2eAgInSc4d1	imunitní
systém	systém	k1gInSc4	systém
<g/>
.	.	kIx.	.
</s>
<s>
Charles	Charles	k1gMnSc1	Charles
Bukowski	Bukowsk	k1gFnSc2	Bukowsk
umírá	umírat	k5eAaImIp3nS	umírat
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
9	[number]	k4	9
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
1994	[number]	k4	1994
v	v	k7c6	v
San	San	k1gFnSc6	San
Pedru	Pedr	k1gInSc2	Pedr
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
73	[number]	k4	73
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Pohřeb	pohřeb	k1gInSc1	pohřeb
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
dne	den	k1gInSc2	den
14	[number]	k4	14
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
1994	[number]	k4	1994
na	na	k7c6	na
hřbitově	hřbitov	k1gInSc6	hřbitov
Green	Green	k2eAgInSc1d1	Green
Hills	Hills	k1gInSc1	Hills
Memorial	Memorial	k1gMnSc1	Memorial
Park	park	k1gInSc1	park
<g/>
.	.	kIx.	.
</s>
<s>
Nápis	nápis	k1gInSc1	nápis
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
náhrobku	náhrobek	k1gInSc6	náhrobek
sděluje	sdělovat	k5eAaImIp3nS	sdělovat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nesnaž	snažit	k5eNaImRp2nS	snažit
se	se	k3xPyFc4	se
(	(	kIx(	(
<g/>
Don	Don	k1gMnSc1	Don
<g/>
́	́	k?	́
<g/>
t	t	k?	t
Try	Try	k1gFnSc1	Try
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tohle	tenhle	k3xDgNnSc4	tenhle
motto	motto	k1gNnSc4	motto
použil	použít	k5eAaPmAgMnS	použít
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
básni	báseň	k1gFnSc6	báseň
a	a	k8xC	a
objasňuje	objasňovat	k5eAaImIp3nS	objasňovat
jej	on	k3xPp3gMnSc4	on
Johnu	John	k1gMnSc3	John
Williamu	William	k1gInSc2	William
Corringtonovi	Corrington	k1gMnSc3	Corrington
v	v	k7c6	v
dopise	dopis	k1gInSc6	dopis
<g/>
.	.	kIx.	.
</s>
<s>
Týká	týkat	k5eAaImIp3nS	týkat
se	se	k3xPyFc4	se
inspirace	inspirace	k1gFnSc1	inspirace
při	při	k7c6	při
psaní	psaní	k1gNnSc6	psaní
<g/>
.	.	kIx.	.
</s>
<s>
Bukowski	Bukowski	k6eAd1	Bukowski
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nemůžeš	moct	k5eNaImIp2nS	moct
se	se	k3xPyFc4	se
pokoušet	pokoušet	k5eAaImF	pokoušet
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
důležité	důležitý	k2eAgNnSc1d1	důležité
<g/>
,	,	kIx,	,
nesnažit	snažit	k5eNaImF	snažit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
čekat	čekat	k5eAaImF	čekat
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
když	když	k8xS	když
se	se	k3xPyFc4	se
nic	nic	k3yNnSc1	nic
neděje	dít	k5eNaImIp3nS	dít
<g/>
,	,	kIx,	,
čekej	čekat	k5eAaImRp2nS	čekat
dál	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jako	jako	k9	jako
štěnice	štěnice	k1gFnSc1	štěnice
vysoko	vysoko	k6eAd1	vysoko
na	na	k7c6	na
stěně	stěna	k1gFnSc6	stěna
<g/>
.	.	kIx.	.
</s>
<s>
Čekáš	čekat	k5eAaImIp2nS	čekat
<g/>
,	,	kIx,	,
až	až	k8xS	až
přijde	přijít	k5eAaPmIp3nS	přijít
k	k	k7c3	k
tobě	ty	k3xPp2nSc3	ty
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
když	když	k8xS	když
se	se	k3xPyFc4	se
přiblíží	přiblížit	k5eAaPmIp3nS	přiblížit
dost	dost	k6eAd1	dost
blízko	blízko	k6eAd1	blízko
<g/>
,	,	kIx,	,
plácneš	plácnout	k5eAaPmIp2nS	plácnout
rukou	ruka	k1gFnSc7	ruka
a	a	k8xC	a
máš	mít	k5eAaImIp2nS	mít
ji	on	k3xPp3gFnSc4	on
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Charles	Charles	k1gMnSc1	Charles
Bukowski	Bukowsk	k1gFnSc2	Bukowsk
tvořil	tvořit	k5eAaImAgMnS	tvořit
raději	rád	k6eAd2	rád
básně	báseň	k1gFnPc4	báseň
než	než	k8xS	než
prózu	próza	k1gFnSc4	próza
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Proč	proč	k6eAd1	proč
psát	psát	k5eAaImF	psát
román	román	k1gInSc4	román
<g/>
,	,	kIx,	,
když	když	k8xS	když
můžu	můžu	k?	můžu
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
cítím	cítit	k5eAaImIp1nS	cítit
pár	pár	k1gInSc4	pár
slovy	slovo	k1gNnPc7	slovo
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgInS	mít
blízký	blízký	k2eAgInSc1d1	blízký
vztah	vztah	k1gInSc1	vztah
k	k	k7c3	k
vážné	vážný	k2eAgFnSc3d1	vážná
hudbě	hudba	k1gFnSc3	hudba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
svých	svůj	k3xOyFgFnPc2	svůj
toulek	toulka	k1gFnPc2	toulka
Amerikou	Amerika	k1gFnSc7	Amerika
bydlel	bydlet	k5eAaImAgMnS	bydlet
chvíli	chvíle	k1gFnSc4	chvíle
v	v	k7c6	v
San	San	k1gFnSc6	San
Franciscu	Franciscus	k1gInSc2	Franciscus
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mu	on	k3xPp3gMnSc3	on
milá	milá	k1gFnSc1	milá
osmdesátiletá	osmdesátiletý	k2eAgFnSc1d1	osmdesátiletá
paní	paní	k1gFnSc1	paní
domácí	domácí	k1gFnSc1	domácí
věnovala	věnovat	k5eAaPmAgFnS	věnovat
starý	starý	k2eAgInSc4d1	starý
gramofon	gramofon	k1gInSc4	gramofon
značky	značka	k1gFnSc2	značka
Victrola	Victrola	k1gFnSc1	Victrola
a	a	k8xC	a
tak	tak	k6eAd1	tak
mladý	mladý	k2eAgMnSc1d1	mladý
Bukowski	Bukowsk	k1gFnSc3	Bukowsk
objevil	objevit	k5eAaPmAgInS	objevit
svět	svět	k1gInSc1	svět
nádherných	nádherný	k2eAgFnPc2d1	nádherná
melodií	melodie	k1gFnPc2	melodie
v	v	k7c6	v
podání	podání	k1gNnSc6	podání
L.	L.	kA	L.
van	van	k1gInSc1	van
Beethovena	Beethoven	k1gMnSc2	Beethoven
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Brahmse	Brahms	k1gMnSc2	Brahms
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozdějším	pozdní	k2eAgInSc6d2	pozdější
věku	věk	k1gInSc6	věk
mu	on	k3xPp3gMnSc3	on
stačilo	stačit	k5eAaBmAgNnS	stačit
k	k	k7c3	k
tvorbě	tvorba	k1gFnSc3	tvorba
zatáhnout	zatáhnout	k5eAaPmF	zatáhnout
rolety	roleta	k1gFnPc4	roleta
v	v	k7c6	v
pokojíku	pokojík	k1gInSc6	pokojík
<g/>
,	,	kIx,	,
otevřít	otevřít	k5eAaPmF	otevřít
pivo	pivo	k1gNnSc4	pivo
<g/>
,	,	kIx,	,
zapálit	zapálit	k5eAaPmF	zapálit
si	se	k3xPyFc3	se
a	a	k8xC	a
naladit	naladit	k5eAaPmF	naladit
FM	FM	kA	FM
stanici	stanice	k1gFnSc4	stanice
s	s	k7c7	s
klasickou	klasický	k2eAgFnSc7d1	klasická
hudbou	hudba	k1gFnSc7	hudba
a	a	k8xC	a
slova	slovo	k1gNnSc2	slovo
si	se	k3xPyFc3	se
sama	sám	k3xTgFnSc1	sám
nacházela	nacházet	k5eAaImAgFnS	nacházet
cestu	cesta	k1gFnSc4	cesta
na	na	k7c4	na
papír	papír	k1gInSc4	papír
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
psaní	psaní	k1gNnSc2	psaní
a	a	k8xC	a
vážné	vážný	k2eAgFnSc2d1	vážná
hudby	hudba	k1gFnSc2	hudba
miloval	milovat	k5eAaImAgMnS	milovat
dostihy	dostih	k1gInPc4	dostih
<g/>
,	,	kIx,	,
kočky	kočka	k1gFnPc4	kočka
a	a	k8xC	a
rád	rád	k6eAd1	rád
si	se	k3xPyFc3	se
zašel	zajít	k5eAaPmAgMnS	zajít
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
oblíbené	oblíbený	k2eAgFnSc2d1	oblíbená
restaurace	restaurace	k1gFnSc2	restaurace
The	The	k1gFnSc2	The
Musso	Mussa	k1gFnSc5	Mussa
&	&	k?	&
Frank	Frank	k1gMnSc1	Frank
Grill	Grill	k1gMnSc1	Grill
<g/>
,	,	kIx,	,
starého	starý	k2eAgInSc2d1	starý
hollywoodského	hollywoodský	k2eAgInSc2d1	hollywoodský
podniku	podnik	k1gInSc2	podnik
-	-	kIx~	-
jak	jak	k8xS	jak
Bukowski	Bukowske	k1gFnSc4	Bukowske
občas	občas	k6eAd1	občas
zdůrazňoval	zdůrazňovat	k5eAaImAgMnS	zdůrazňovat
"	"	kIx"	"
<g/>
staršího	starší	k1gMnSc4	starší
než	než	k8xS	než
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgInSc4	sám
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Charles	Charles	k1gMnSc1	Charles
Bukowski	Bukowsk	k1gFnSc2	Bukowsk
byl	být	k5eAaImAgMnS	být
natolik	natolik	k6eAd1	natolik
plodným	plodný	k2eAgMnSc7d1	plodný
autorem	autor	k1gMnSc7	autor
<g/>
,	,	kIx,	,
že	že	k8xS	že
dodnes	dodnes	k6eAd1	dodnes
neexistuje	existovat	k5eNaImIp3nS	existovat
uspokojivá	uspokojivý	k2eAgFnSc1d1	uspokojivá
kompletní	kompletní	k2eAgFnSc1d1	kompletní
bibliografie	bibliografie	k1gFnSc1	bibliografie
jeho	on	k3xPp3gNnSc2	on
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Uvést	uvést	k5eAaPmF	uvést
většinu	většina	k1gFnSc4	většina
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
Bukowski	Bukowski	k1gNnSc1	Bukowski
napsal	napsat	k5eAaBmAgMnS	napsat
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
formátech	formát	k1gInPc6	formát
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
vydalo	vydat	k5eAaPmAgNnS	vydat
na	na	k7c4	na
celou	celý	k2eAgFnSc4d1	celá
knihu	kniha	k1gFnSc4	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc4	několik
pokusů	pokus	k1gInPc2	pokus
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
např.	např.	kA	např.
A	a	k8xC	a
Bibliography	Bibliographa	k1gFnSc2	Bibliographa
of	of	k?	of
Charles	Charles	k1gMnSc1	Charles
Bukowski	Bukowsk	k1gFnSc2	Bukowsk
od	od	k7c2	od
Sandorfa	Sandorf	k1gMnSc2	Sandorf
Dorbina	Dorbin	k2eAgFnSc1d1	Dorbin
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
spisovatelovy	spisovatelův	k2eAgFnSc2d1	spisovatelova
tvorby	tvorba	k1gFnSc2	tvorba
je	být	k5eAaImIp3nS	být
roztroušena	roztroušen	k2eAgFnSc1d1	roztroušena
po	po	k7c6	po
mnoha	mnoho	k4c6	mnoho
téměř	téměř	k6eAd1	téměř
neznámých	známý	k2eNgFnPc6d1	neznámá
publikacích	publikace	k1gFnPc6	publikace
a	a	k8xC	a
část	část	k1gFnSc4	část
je	být	k5eAaImIp3nS	být
ztracena	ztratit	k5eAaPmNgFnS	ztratit
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Bukowski	Bukowske	k1gFnSc4	Bukowske
rozesílal	rozesílat	k5eAaImAgMnS	rozesílat
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
raném	raný	k2eAgNnSc6d1	rané
literárním	literární	k2eAgNnSc6d1	literární
období	období	k1gNnSc6	období
své	svůj	k3xOyFgInPc4	svůj
příspěvky	příspěvek	k1gInPc4	příspěvek
do	do	k7c2	do
časopisů	časopis	k1gInPc2	časopis
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
si	se	k3xPyFc3	se
dělal	dělat	k5eAaImAgMnS	dělat
kopie	kopie	k1gFnPc4	kopie
<g/>
,	,	kIx,	,
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
nevrátila	vrátit	k5eNaPmAgFnS	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Bukowského	Bukowského	k2eAgInPc6d1	Bukowského
prozaických	prozaický	k2eAgInPc6d1	prozaický
dílech	díl	k1gInPc6	díl
často	často	k6eAd1	často
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
postava	postava	k1gFnSc1	postava
jménem	jméno	k1gNnSc7	jméno
Henry	henry	k1gInSc2	henry
Chinaski	Chinask	k1gFnSc2	Chinask
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgNnSc1d1	hlavní
spisovatelovo	spisovatelův	k2eAgNnSc1d1	spisovatelovo
alter	alter	k1gInSc4	alter
ego	ego	k1gNnSc3	ego
<g/>
.	.	kIx.	.
</s>
<s>
Black	Black	k6eAd1	Black
Sparrow	Sparrow	k1gMnPc1	Sparrow
Press	Pressa	k1gFnPc2	Pressa
dále	daleko	k6eAd2	daleko
publikuje	publikovat	k5eAaBmIp3nS	publikovat
dosud	dosud	k6eAd1	dosud
nevydané	vydaný	k2eNgFnPc4d1	nevydaná
básně	báseň	k1gFnPc4	báseň
a	a	k8xC	a
prózu	próza	k1gFnSc4	próza
<g/>
.	.	kIx.	.
</s>
<s>
Poštovní	poštovní	k2eAgInSc1d1	poštovní
úřad	úřad	k1gInSc1	úřad
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
Post	post	k1gInSc1	post
Office	Office	kA	Office
<g/>
,	,	kIx,	,
Black	Black	k1gMnSc1	Black
Sparrow	Sparrow	k1gFnSc2	Sparrow
Press	Press	k1gInSc1	Press
1971	[number]	k4	1971
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
Faktótum	faktótum	k1gNnSc1	faktótum
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
Factotum	Factotum	k1gNnSc1	Factotum
<g/>
,	,	kIx,	,
Black	Black	k1gMnSc1	Black
Sparrow	Sparrow	k1gFnSc2	Sparrow
Press	Press	k1gInSc1	Press
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
Ženy	žena	k1gFnPc1	žena
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
Women	Women	k1gInSc1	Women
<g/>
,	,	kIx,	,
Black	Black	k1gMnSc1	Black
Sparrow	Sparrow	k1gFnSc2	Sparrow
Press	Press	k1gInSc1	Press
1978	[number]	k4	1978
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
<g />
.	.	kIx.	.
</s>
<s>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
Šunkový	šunkový	k2eAgInSc1d1	šunkový
nářez	nářez	k1gInSc1	nářez
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
Ham	ham	k0	ham
On	on	k3xPp3gMnSc1	on
Rye	Rye	k1gMnSc1	Rye
<g/>
,	,	kIx,	,
Black	Black	k1gMnSc1	Black
Sparrow	Sparrow	k1gFnSc2	Sparrow
Press	Press	k1gInSc1	Press
1982	[number]	k4	1982
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
Hollywood	Hollywood	k1gInSc1	Hollywood
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
Hollywood	Hollywood	k1gInSc1	Hollywood
<g/>
,	,	kIx,	,
Black	Black	k1gMnSc1	Black
Sparrow	Sparrow	k1gFnSc2	Sparrow
Press	Press	k1gInSc1	Press
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
Škvár	škvára	k1gFnPc2	škvára
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
Pulp	pulpa	k1gFnPc2	pulpa
<g/>
,	,	kIx,	,
Black	Black	k1gMnSc1	Black
Sparrow	Sparrow	k1gFnSc2	Sparrow
Press	Press	k1gInSc1	Press
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
Confessions	Confessionsa	k1gFnPc2	Confessionsa
of	of	k?	of
a	a	k8xC	a
Man	Man	k1gMnSc1	Man
Insane	Insan	k1gMnSc5	Insan
Enough	Enough	k1gInSc4	Enough
to	ten	k3xDgNnSc1	ten
Live	Live	k1gNnSc1	Live
with	witha	k1gFnPc2	witha
Beasts	Beastsa	k1gFnPc2	Beastsa
(	(	kIx(	(
<g/>
Mimeo	Mimeo	k6eAd1	Mimeo
Press	Press	k1gInSc1	Press
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
-	-	kIx~	-
raná	raný	k2eAgFnSc1d1	raná
sbírka	sbírka	k1gFnSc1	sbírka
vydaná	vydaný	k2eAgFnSc1d1	vydaná
v	v	k7c6	v
malém	malý	k2eAgInSc6d1	malý
nákladu	náklad	k1gInSc6	náklad
<g/>
.	.	kIx.	.
</s>
<s>
All	All	k?	All
the	the	k?	the
Assholes	Assholes	k1gInSc1	Assholes
in	in	k?	in
the	the	k?	the
World	World	k1gInSc1	World
and	and	k?	and
Mine	minout	k5eAaImIp3nS	minout
(	(	kIx(	(
<g/>
Open	Open	k1gMnSc1	Open
Skull	Skull	k1gMnSc1	Skull
Press	Press	k1gInSc4	Press
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
-	-	kIx~	-
další	další	k2eAgFnSc1d1	další
raná	raný	k2eAgFnSc1d1	raná
sbírka	sbírka	k1gFnSc1	sbírka
v	v	k7c6	v
malém	malý	k2eAgInSc6d1	malý
nákladu	náklad	k1gInSc6	náklad
<g/>
.	.	kIx.	.
</s>
<s>
Zápisky	zápisek	k1gInPc1	zápisek
starého	starý	k2eAgMnSc2d1	starý
prasáka	prasák	k1gMnSc2	prasák
/	/	kIx~	/
Paměti	paměť	k1gFnPc1	paměť
starého	starý	k2eAgMnSc2d1	starý
chlapáka	chlapák	k1gMnSc2	chlapák
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
Notes	notes	k1gInSc1	notes
of	of	k?	of
a	a	k8xC	a
Dirty	Dirta	k1gMnSc2	Dirta
Old	Olda	k1gFnPc2	Olda
Man	Man	k1gMnSc1	Man
<g/>
,	,	kIx,	,
Essex	Essex	k1gInSc1	Essex
House	house	k1gNnSc1	house
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
-	-	kIx~	-
sbírka	sbírka	k1gFnSc1	sbírka
povídek	povídka	k1gFnPc2	povídka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc4	jenž
Bukowski	Bukowsk	k1gFnPc4	Bukowsk
psal	psát	k5eAaImAgMnS	psát
jako	jako	k9	jako
týdenní	týdenní	k2eAgInSc4d1	týdenní
sloupky	sloupek	k1gInPc4	sloupek
do	do	k7c2	do
undergroundového	undergroundový	k2eAgInSc2d1	undergroundový
časopisu	časopis	k1gInSc2	časopis
Open	Open	k1gNnSc1	Open
City	city	k1gNnSc1	city
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgNnSc1d1	Nové
vydání	vydání	k1gNnSc1	vydání
připravilo	připravit	k5eAaPmAgNnS	připravit
City	city	k1gNnSc4	city
Lights	Lightsa	k1gFnPc2	Lightsa
Books	Booksa	k1gFnPc2	Booksa
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
Bukowski	Bukowsk	k1gFnPc1	Bukowsk
Sampler	Sampler	k1gInSc1	Sampler
(	(	kIx(	(
<g/>
Quixote	Quixot	k1gInSc5	Quixot
Press	Press	k1gInSc1	Press
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
Erection	Erection	k1gInSc1	Erection
<g/>
,	,	kIx,	,
Ejaculation	Ejaculation	k1gInSc1	Ejaculation
<g/>
,	,	kIx,	,
Exhibition	Exhibition	k1gInSc1	Exhibition
and	and	k?	and
General	General	k1gFnSc2	General
Tales	Tales	k1gMnSc1	Tales
of	of	k?	of
Ordinary	Ordinar	k1gInPc1	Ordinar
Madness	Madness	k1gInSc1	Madness
(	(	kIx(	(
<g/>
City	City	k1gFnSc1	City
Lights	Lights	k1gInSc4	Lights
Books	Books	k1gInSc1	Books
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
-	-	kIx~	-
tato	tento	k3xDgFnSc1	tento
velmi	velmi	k6eAd1	velmi
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
kniha	kniha	k1gFnSc1	kniha
byla	být	k5eAaImAgFnS	být
vydaná	vydaný	k2eAgFnSc1d1	vydaná
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
svazcích	svazek	k1gInPc6	svazek
-	-	kIx~	-
Tales	Tales	k1gMnSc1	Tales
of	of	k?	of
Ordinary	Ordinara	k1gFnSc2	Ordinara
Madness	Madnessa	k1gFnPc2	Madnessa
a	a	k8xC	a
The	The	k1gMnPc2	The
most	most	k1gInSc1	most
Beautiful	Beautifula	k1gFnPc2	Beautifula
Woman	Woman	k1gMnSc1	Woman
in	in	k?	in
Town	Town	k1gMnSc1	Town
&	&	k?	&
Other	Other	k1gMnSc1	Other
Stories	Stories	k1gMnSc1	Stories
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
řitě	řiť	k1gFnPc1	řiť
světa	svět	k1gInSc2	svět
i	i	k8xC	i
ta	ten	k3xDgFnSc1	ten
má	mít	k5eAaImIp3nS	mít
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
South	South	k1gMnSc1	South
of	of	k?	of
No	no	k9	no
North	North	k1gMnSc1	North
<g/>
,	,	kIx,	,
Black	Black	k1gMnSc1	Black
Sparrow	Sparrow	k1gFnSc2	Sparrow
Press	Press	k1gInSc1	Press
1973	[number]	k4	1973
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
Bring	Bring	k1gMnSc1	Bring
Me	Me	k1gMnSc1	Me
Your	Your	k1gMnSc1	Your
Love	lov	k1gInSc5	lov
(	(	kIx(	(
<g/>
Black	Black	k1gMnSc1	Black
Sparrow	Sparrow	k1gMnSc1	Sparrow
Press	Press	k1gInSc4	Press
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
-	-	kIx~	-
zvláštní	zvláštní	k2eAgNnSc4d1	zvláštní
vydání	vydání	k1gNnSc4	vydání
jediné	jediný	k2eAgFnSc2d1	jediná
povídky	povídka	k1gFnSc2	povídka
ilustrované	ilustrovaný	k2eAgFnSc2d1	ilustrovaná
R.	R.	kA	R.
<g/>
Crumbem	Crumb	k1gInSc7	Crumb
<g/>
.	.	kIx.	.
</s>
<s>
Těžký	těžký	k2eAgInSc4d1	těžký
časy	čas	k1gInPc4	čas
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
Hot	hot	k0	hot
Water	Water	k1gMnSc1	Water
Music	Music	k1gMnSc1	Music
<g/>
,	,	kIx,	,
Black	Black	k1gMnSc1	Black
Sparrow	Sparrow	k1gFnSc2	Sparrow
Press	Press	k1gInSc1	Press
1983	[number]	k4	1983
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
Erekce	erekce	k1gFnPc4	erekce
<g/>
,	,	kIx,	,
ejakulace	ejakulace	k1gFnPc4	ejakulace
<g/>
,	,	kIx,	,
exhibice	exhibice	k1gFnPc4	exhibice
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
příběhy	příběh	k1gInPc4	příběh
obyčejného	obyčejný	k2eAgNnSc2d1	obyčejné
šílenství	šílenství	k1gNnSc2	šílenství
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
Tales	Tales	k1gInSc1	Tales
of	of	k?	of
Ordinary	Ordinara	k1gFnSc2	Ordinara
Madness	Madnessa	k1gFnPc2	Madnessa
<g/>
,	,	kIx,	,
1983	[number]	k4	1983
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Pragma	Pragma	k1gFnSc1	Pragma
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
-	-	kIx~	-
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
vyšla	vyjít	k5eAaPmAgFnS	vyjít
i	i	k9	i
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Příběhy	příběh	k1gInPc4	příběh
obyčejného	obyčejný	k2eAgNnSc2d1	obyčejné
šílenství	šílenství	k1gNnSc2	šílenství
(	(	kIx(	(
<g/>
Argo	Argo	k6eAd1	Argo
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejkrásnější	krásný	k2eAgFnSc1d3	nejkrásnější
ženská	ženská	k1gFnSc1	ženská
ve	v	k7c6	v
městě	město	k1gNnSc6	město
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
The	The	k1gFnSc1	The
most	most	k1gInSc1	most
Beautiful	Beautiful	k1gInSc1	Beautiful
Woman	Woman	k1gInSc1	Woman
in	in	k?	in
Town	Town	k1gMnSc1	Town
&	&	k?	&
Other	Other	k1gMnSc1	Other
Stories	Stories	k1gMnSc1	Stories
<g/>
,	,	kIx,	,
1983	[number]	k4	1983
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Argo	Argo	k6eAd1	Argo
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
There	Ther	k1gInSc5	Ther
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
No	no	k9	no
Business	business	k1gInSc1	business
(	(	kIx(	(
<g/>
Black	Black	k1gMnSc1	Black
Sparrow	Sparrow	k1gMnSc1	Sparrow
Press	Press	k1gInSc4	Press
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
-	-	kIx~	-
další	další	k2eAgNnSc4d1	další
zvláštní	zvláštní	k2eAgNnSc4d1	zvláštní
vydání	vydání	k1gNnSc4	vydání
jediné	jediný	k2eAgFnSc2d1	jediná
povídky	povídka	k1gFnSc2	povídka
ilustrované	ilustrovaný	k2eAgFnSc2d1	ilustrovaná
R.	R.	kA	R.
<g/>
Crumbem	Crumb	k1gInSc7	Crumb
<g/>
.	.	kIx.	.
</s>
<s>
Confession	Confession	k1gInSc1	Confession
of	of	k?	of
a	a	k8xC	a
Coward	Coward	k1gMnSc1	Coward
(	(	kIx(	(
<g/>
Black	Black	k1gMnSc1	Black
Sparrow	Sparrow	k1gMnSc1	Sparrow
Press	Press	k1gInSc4	Press
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
Pobryndané	pobryndaný	k2eAgInPc4d1	pobryndaný
spisy	spis	k1gInPc4	spis
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
Portions	Portions	k1gInSc1	Portions
From	From	k1gMnSc1	From
a	a	k8xC	a
Wine-Stained	Wine-Stained	k1gMnSc1	Wine-Stained
Notebook	notebook	k1gInSc1	notebook
<g/>
:	:	kIx,	:
Uncollected	Uncollected	k1gInSc1	Uncollected
Stories	Stories	k1gInSc1	Stories
and	and	k?	and
Essays	Essays	k1gInSc1	Essays
1944	[number]	k4	1944
<g/>
-	-	kIx~	-
<g/>
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
City	City	k1gFnSc1	City
Lights	Lights	k1gInSc1	Lights
Books	Books	k1gInSc1	Books
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Argo	Argo	k6eAd1	Argo
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
Absence	absence	k1gFnSc1	absence
hrdiny	hrdina	k1gMnSc2	hrdina
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
angl.	angl.	k?	angl.
Absence	absence	k1gFnSc1	absence
of	of	k?	of
the	the	k?	the
Hero	Hero	k1gNnSc1	Hero
<g/>
,	,	kIx,	,
City	city	k1gNnSc1	city
Lights	Lightsa	k1gFnPc2	Lightsa
Books	Booksa	k1gFnPc2	Booksa
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Argo	Argo	k6eAd1	Argo
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
Další	další	k2eAgInPc1d1	další
zápisky	zápisek	k1gInPc1	zápisek
starého	starý	k2eAgMnSc2d1	starý
prasáka	prasák	k1gMnSc2	prasák
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
More	mor	k1gInSc5	mor
Notes	notes	k1gInSc4	notes
of	of	k?	of
a	a	k8xC	a
Dirty	Dirta	k1gMnSc2	Dirta
Old	Olda	k1gFnPc2	Olda
Man	Man	k1gMnSc1	Man
<g/>
,	,	kIx,	,
City	City	k1gFnSc1	City
Lights	Lights	k1gInSc1	Lights
Books	Books	k1gInSc1	Books
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Argo	Argo	k6eAd1	Argo
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
Blonďatá	blonďatý	k2eAgFnSc1d1	blonďatá
píča	píča	k1gFnSc1	píča
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Argo	Argo	k6eAd1	Argo
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
-	-	kIx~	-
dvojjazyčné	dvojjazyčný	k2eAgNnSc1d1	dvojjazyčné
vydání	vydání	k1gNnSc1	vydání
(	(	kIx(	(
<g/>
čeština	čeština	k1gFnSc1	čeština
+	+	kIx~	+
angličtina	angličtina	k1gFnSc1	angličtina
<g/>
)	)	kIx)	)
trojice	trojice	k1gFnSc1	trojice
povídek	povídka	k1gFnPc2	povídka
<g/>
:	:	kIx,	:
Odbarvená	odbarvený	k2eAgFnSc1d1	odbarvená
píča	píča	k1gFnSc1	píča
<g/>
,	,	kIx,	,
Bílej	Bílej	k?	Bílej
plnovous	plnovous	k1gInSc1	plnovous
<g/>
,	,	kIx,	,
Vražda	vražda	k1gFnSc1	vražda
Ramóna	Ramóna	k1gFnSc1	Ramóna
Vásqueze	Vásqueze	k1gFnSc1	Vásqueze
<g/>
.	.	kIx.	.
</s>
<s>
Horsemeat	Horsemeat	k1gInSc1	Horsemeat
(	(	kIx(	(
<g/>
Black	Black	k1gMnSc1	Black
Sparrow	Sparrow	k1gMnSc1	Sparrow
Press	Press	k1gInSc4	Press
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
-	-	kIx~	-
vydání	vydání	k1gNnSc1	vydání
v	v	k7c6	v
omezeném	omezený	k2eAgInSc6d1	omezený
nákladu	náklad	k1gInSc6	náklad
s	s	k7c7	s
fotografiemi	fotografia	k1gFnPc7	fotografia
Bukowského	Bukowského	k2eAgFnPc7d1	Bukowského
na	na	k7c6	na
dostizích	dostih	k1gInPc6	dostih
od	od	k7c2	od
Michaela	Michael	k1gMnSc2	Michael
Montforta	Montfort	k1gMnSc2	Montfort
<g/>
.	.	kIx.	.
</s>
<s>
Septuagenarian	Septuagenarian	k1gMnSc1	Septuagenarian
Stew	Stew	k1gMnSc1	Stew
<g/>
:	:	kIx,	:
Stories	Stories	k1gMnSc1	Stories
and	and	k?	and
Poems	Poems	k1gInSc1	Poems
(	(	kIx(	(
<g/>
Black	Black	k1gMnSc1	Black
Sparrow	Sparrow	k1gMnSc1	Sparrow
Press	Press	k1gInSc4	Press
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
Kam	kam	k6eAd1	kam
zmizela	zmizet	k5eAaPmAgFnS	zmizet
ta	ten	k3xDgFnSc1	ten
roztomilá	roztomilý	k2eAgFnSc1d1	roztomilá
rozesmátá	rozesmátý	k2eAgFnSc1d1	rozesmátá
holka	holka	k1gFnSc1	holka
v	v	k7c6	v
květovaných	květovaný	k2eAgInPc6d1	květovaný
šatech	šat	k1gInPc6	šat
<g/>
:	:	kIx,	:
Básně	báseň	k1gFnPc1	báseň
a	a	k8xC	a
povídky	povídka	k1gFnPc1	povídka
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
Betting	Betting	k1gInSc1	Betting
on	on	k3xPp3gMnSc1	on
the	the	k?	the
Muse	Musa	k1gFnSc3	Musa
<g/>
:	:	kIx,	:
Poems	Poemsa	k1gFnPc2	Poemsa
and	and	k?	and
Stories	Stories	k1gMnSc1	Stories
<g/>
,	,	kIx,	,
Black	Black	k1gMnSc1	Black
Sparrow	Sparrow	k1gFnSc2	Sparrow
Press	Press	k1gInSc1	Press
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
<g />
.	.	kIx.	.
</s>
<s>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
Flower	Flower	k1gMnSc1	Flower
<g/>
,	,	kIx,	,
Fist	Fist	k1gMnSc1	Fist
and	and	k?	and
Bestial	Bestial	k1gMnSc1	Bestial
Wail	Wail	k1gMnSc1	Wail
(	(	kIx(	(
<g/>
Hearse	Hearse	k1gFnSc1	Hearse
Press	Press	k1gInSc1	Press
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
Longshot	Longshot	k1gInSc4	Longshot
Poems	Poems	k1gInSc4	Poems
for	forum	k1gNnPc2	forum
Broke	Brok	k1gMnSc2	Brok
Players	Playersa	k1gFnPc2	Playersa
(	(	kIx(	(
<g/>
7	[number]	k4	7
Poets	Poets	k1gInSc1	Poets
Press	Press	k1gInSc1	Press
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
Run	run	k1gInSc1	run
with	with	k1gInSc1	with
the	the	k?	the
Hunted	Hunted	k1gInSc1	Hunted
(	(	kIx(	(
<g/>
Midwest	Midwest	k1gFnSc1	Midwest
Press	Press	k1gInSc1	Press
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
It	It	k1gMnSc1	It
Catches	Catches	k1gMnSc1	Catches
My	my	k3xPp1nPc1	my
Heart	Heart	k1gInSc4	Heart
in	in	k?	in
Its	Its	k1gFnSc2	Its
Hands	Handsa	k1gFnPc2	Handsa
(	(	kIx(	(
<g/>
Loujon	Loujon	k1gInSc1	Loujon
Press	Press	k1gInSc1	Press
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
-	-	kIx~	-
první	první	k4xOgFnSc1	první
kniha	kniha	k1gFnSc1	kniha
Ch	Ch	kA	Ch
<g/>
.	.	kIx.	.
Bukowského	Bukowského	k2eAgFnSc1d1	Bukowského
vydaná	vydaný	k2eAgFnSc1d1	vydaná
Jonem	Jonum	k1gNnSc7	Jonum
a	a	k8xC	a
Gypsy	gyps	k1gInPc7	gyps
Lou	Lou	k1gFnPc2	Lou
Webbovými	Webbová	k1gFnPc7	Webbová
v	v	k7c6	v
New	New	k1gFnSc6	New
Orleans	Orleans	k1gInSc4	Orleans
<g/>
.	.	kIx.	.
</s>
<s>
Crucifix	Crucifix	k1gInSc1	Crucifix
in	in	k?	in
a	a	k8xC	a
Deathhand	Deathhand	k1gInSc1	Deathhand
(	(	kIx(	(
<g/>
Loujon	Loujon	k1gInSc1	Loujon
Press	Press	k1gInSc1	Press
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
Cold	Cold	k1gInSc1	Cold
Dogs	Dogs	k1gInSc1	Dogs
in	in	k?	in
the	the	k?	the
Courtyard	Courtyard	k1gInSc4	Courtyard
(	(	kIx(	(
<g/>
Literary	Literar	k1gInPc4	Literar
Times-Cyfoeth	Times-Cyfoeth	k1gInSc1	Times-Cyfoeth
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Genius	genius	k1gMnSc1	genius
of	of	k?	of
the	the	k?	the
Crowd	Crowd	k1gInSc1	Crowd
(	(	kIx(	(
<g/>
7	[number]	k4	7
Flowers	Flowers	k1gInSc1	Flowers
Press	Press	k1gInSc1	Press
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
2	[number]	k4	2
Poems	Poems	k1gInSc1	Poems
(	(	kIx(	(
<g/>
Black	Black	k1gMnSc1	Black
Sparrow	Sparrow	k1gMnSc1	Sparrow
Press	Press	k1gInSc4	Press
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Curtains	Curtainsa	k1gFnPc2	Curtainsa
are	ar	k1gInSc5	ar
Waving	Waving	k1gInSc4	Waving
<g />
.	.	kIx.	.
</s>
<s>
and	and	k?	and
People	People	k1gMnSc1	People
Walk	Walk	k1gMnSc1	Walk
Through	Through	k1gMnSc1	Through
<g/>
/	/	kIx~	/
The	The	k1gMnSc1	The
Afternoon	Afternoon	k1gMnSc1	Afternoon
<g/>
/	/	kIx~	/
Here	Here	k1gFnSc1	Here
and	and	k?	and
in	in	k?	in
Berlin	berlina	k1gFnPc2	berlina
and	and	k?	and
in	in	k?	in
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
City	City	k1gFnSc2	City
and	and	k?	and
in	in	k?	in
Mexico	Mexico	k1gMnSc1	Mexico
(	(	kIx(	(
<g/>
Black	Black	k1gMnSc1	Black
Sparrow	Sparrow	k1gMnSc1	Sparrow
Press	Press	k1gInSc4	Press
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
At	At	k1gMnSc1	At
Terror	Terror	k1gMnSc1	Terror
Street	Street	k1gMnSc1	Street
and	and	k?	and
Agony	agon	k1gInPc1	agon
Way	Way	k1gFnSc1	Way
(	(	kIx(	(
<g/>
Black	Black	k1gMnSc1	Black
Sparrow	Sparrow	k1gMnSc1	Sparrow
Press	Press	k1gInSc4	Press
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
Poems	Poems	k1gInSc4	Poems
Written	Written	k2eAgInSc4d1	Written
before	befor	k1gInSc5	befor
Jumping	jumping	k1gInSc1	jumping
out	out	k?	out
of	of	k?	of
an	an	k?	an
8	[number]	k4	8
<g/>
-Storey	-Storea	k1gMnSc2	-Storea
Window	Window	k1gMnSc2	Window
(	(	kIx(	(
<g/>
Poetry	Poetr	k1gInPc1	Poetr
X	X	kA	X
<g/>
/	/	kIx~	/
<g/>
Change	change	k1gFnSc1	change
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
The	The	k1gFnSc6	The
Days	Daysa	k1gFnPc2	Daysa
Run	Runa	k1gFnPc2	Runa
Away	Awaa	k1gMnSc2	Awaa
Like	Like	k1gNnSc2	Like
Wild	Wild	k1gMnSc1	Wild
Horses	Horses	k1gMnSc1	Horses
Over	Over	k1gMnSc1	Over
the	the	k?	the
Hills	Hills	k1gInSc1	Hills
(	(	kIx(	(
<g/>
Black	Black	k1gMnSc1	Black
Sparrow	Sparrow	k1gMnSc1	Sparrow
Press	Press	k1gInSc4	Press
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
-	-	kIx~	-
první	první	k4xOgFnSc1	první
větší	veliký	k2eAgFnSc1d2	veliký
antologie	antologie	k1gFnSc1	antologie
od	od	k7c2	od
Black	Blacka	k1gFnPc2	Blacka
Sparrow	Sparrow	k1gFnSc2	Sparrow
Press	Pressa	k1gFnPc2	Pressa
<g/>
.	.	kIx.	.
</s>
<s>
Penguin	Penguin	k2eAgInSc1d1	Penguin
Modern	Modern	k1gInSc1	Modern
Poets	Poets	k1gInSc1	Poets
13	[number]	k4	13
-	-	kIx~	-
Charles	Charles	k1gMnSc1	Charles
Bukowski	Bukowsk	k1gFnSc2	Bukowsk
<g/>
/	/	kIx~	/
Philip	Philip	k1gMnSc1	Philip
Lamantia	Lamantius	k1gMnSc2	Lamantius
<g/>
/	/	kIx~	/
Harold	Harold	k1gMnSc1	Harold
Norse	Nors	k1gMnSc2	Nors
(	(	kIx(	(
<g/>
Penguin	Penguin	k2eAgInSc1d1	Penguin
Books	Books	k1gInSc1	Books
<g/>
,	,	kIx,	,
London	London	k1gMnSc1	London
<g/>
,	,	kIx,	,
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
-	-	kIx~	-
jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
edic	edice	k1gFnPc2	edice
If	If	k1gMnSc2	If
We	We	k1gMnSc2	We
Take	Tak	k1gMnSc2	Tak
(	(	kIx(	(
<g/>
Black	Black	k1gMnSc1	Black
Sparrow	Sparrow	k1gMnSc1	Sparrow
Press	Press	k1gInSc4	Press
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
-	-	kIx~	-
kniha	kniha	k1gFnSc1	kniha
byla	být	k5eAaImAgFnS	být
vydána	vydat	k5eAaPmNgFnS	vydat
jako	jako	k8xS	jako
novoroční	novoroční	k2eAgInSc4d1	novoroční
dárek	dárek	k1gInSc4	dárek
<g/>
.	.	kIx.	.
</s>
<s>
Fire	Fire	k1gInSc1	Fire
Station	station	k1gInSc1	station
(	(	kIx(	(
<g/>
Capricorn	Capricorn	k1gInSc1	Capricorn
Press	Press	k1gInSc1	Press
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
-	-	kIx~	-
vydání	vydání	k1gNnSc1	vydání
jediné	jediný	k2eAgFnSc2d1	jediná
básně	báseň	k1gFnSc2	báseň
v	v	k7c6	v
omezeném	omezený	k2eAgInSc6d1	omezený
nákladu	náklad	k1gInSc6	náklad
<g/>
.	.	kIx.	.
</s>
<s>
Me	Me	k?	Me
and	and	k?	and
Your	Your	k1gMnSc1	Your
Sometimes	Sometimes	k1gMnSc1	Sometimes
Love	lov	k1gInSc5	lov
Poems	Poems	k1gInSc1	Poems
(	(	kIx(	(
<g/>
Kisskill	Kisskill	k1gInSc1	Kisskill
Press	Press	k1gInSc1	Press
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
-	-	kIx~	-
básnická	básnický	k2eAgFnSc1d1	básnická
sbírka	sbírka	k1gFnSc1	sbírka
vydaná	vydaný	k2eAgNnPc4d1	vydané
vlastním	vlastní	k2eAgInSc7d1	vlastní
nákladem	náklad	k1gInSc7	náklad
Ch	Ch	kA	Ch
<g/>
.	.	kIx.	.
<g/>
Bukowskim	Bukowskim	k1gInSc1	Bukowskim
a	a	k8xC	a
Lindou	Linda	k1gFnSc7	Linda
Kingovou	Kingův	k2eAgFnSc7d1	Kingova
o	o	k7c6	o
jejich	jejich	k3xOp3gInSc6	jejich
vztahu	vztah	k1gInSc6	vztah
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
ji	on	k3xPp3gFnSc4	on
znovu	znovu	k6eAd1	znovu
vydala	vydat	k5eAaPmAgFnS	vydat
Linda	Linda	k1gFnSc1	Linda
Kingová	Kingový	k2eAgFnSc1d1	Kingová
a	a	k8xC	a
přidala	přidat	k5eAaPmAgFnS	přidat
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
žertovné	žertovný	k2eAgInPc4d1	žertovný
Bukowského	Bukowského	k2eAgInPc4d1	Bukowského
obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
měly	mít	k5eAaImAgInP	mít
být	být	k5eAaImF	být
již	již	k6eAd1	již
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
vydání	vydání	k1gNnSc6	vydání
<g/>
.	.	kIx.	.
</s>
<s>
Ptáčku	Ptáček	k1gMnSc5	Ptáček
posměváčku	posměváček	k1gMnSc5	posměváček
<g/>
,	,	kIx,	,
přej	přát	k5eAaImRp2nS	přát
mi	já	k3xPp1nSc3	já
štěstí	štěstí	k1gNnSc4	štěstí
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
Mockingbird	Mockingbird	k1gMnSc1	Mockingbird
Wish	Wish	k1gMnSc1	Wish
Me	Me	k1gMnSc1	Me
Luck	Luck	k1gMnSc1	Luck
<g/>
,	,	kIx,	,
Black	Black	k1gMnSc1	Black
Sparrow	Sparrow	k1gFnSc2	Sparrow
Press	Press	k1gInSc1	Press
1972	[number]	k4	1972
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
Burning	Burning	k1gInSc1	Burning
in	in	k?	in
Water	Water	k1gInSc1	Water
<g/>
,	,	kIx,	,
Drowning	Drowning	k1gInSc1	Drowning
in	in	k?	in
Flame	Flam	k1gInSc5	Flam
<g/>
:	:	kIx,	:
Poems	Poems	k1gInSc1	Poems
1955	[number]	k4	1955
-1973	-1973	k4	-1973
(	(	kIx(	(
<g/>
Black	Black	k1gMnSc1	Black
Sparrow	Sparrow	k1gMnSc1	Sparrow
Press	Press	k1gInSc4	Press
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
-	-	kIx~	-
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
antologie	antologie	k1gFnSc2	antologie
jsou	být	k5eAaImIp3nP	být
sebrány	sebrán	k2eAgFnPc4d1	sebrána
básně	báseň	k1gFnPc4	báseň
z	z	k7c2	z
obou	dva	k4xCgFnPc2	dva
loujonských	loujonský	k2eAgFnPc2d1	loujonský
vydání	vydání	k1gNnSc4	vydání
společně	společně	k6eAd1	společně
s	s	k7c7	s
pozdějšími	pozdní	k2eAgInPc7d2	pozdější
texty	text	k1gInPc7	text
<g/>
.	.	kIx.	.
</s>
<s>
Tough	Tough	k1gInSc1	Tough
Company	Compana	k1gFnSc2	Compana
(	(	kIx(	(
<g/>
Black	Black	k1gMnSc1	Black
Sparrow	Sparrow	k1gMnSc1	Sparrow
Press	Press	k1gInSc4	Press
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
-	-	kIx~	-
vyšla	vyjít	k5eAaPmAgFnS	vyjít
jako	jako	k9	jako
novoroční	novoroční	k2eAgInSc4d1	novoroční
dárek	dárek	k1gInSc4	dárek
<g/>
.	.	kIx.	.
</s>
<s>
Scarlet	Scarlet	k1gInSc1	Scarlet
(	(	kIx(	(
<g/>
Black	Black	k1gMnSc1	Black
Sparrow	Sparrow	k1gMnSc1	Sparrow
Press	Press	k1gInSc4	Press
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
-	-	kIx~	-
limitovaná	limitovaný	k2eAgFnSc1d1	limitovaná
edice	edice	k1gFnSc1	edice
básní	báseň	k1gFnPc2	báseň
věnovaných	věnovaný	k2eAgFnPc2d1	věnovaná
Bukowského	Bukowského	k2eAgFnSc4d1	Bukowského
přítelkyni	přítelkyně	k1gFnSc4	přítelkyně
Pamele	Pamela	k1gFnSc3	Pamela
Millerové	Millerové	k2eAgFnSc3d1	Millerové
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
básně	báseň	k1gFnPc1	báseň
byly	být	k5eAaImAgFnP	být
později	pozdě	k6eAd2	pozdě
znovu	znovu	k6eAd1	znovu
otištěny	otisknout	k5eAaPmNgInP	otisknout
ve	v	k7c6	v
sbírce	sbírka	k1gFnSc6	sbírka
Love	lov	k1gInSc5	lov
is	is	k?	is
a	a	k8xC	a
Dog	doga	k1gFnPc2	doga
from	from	k1gMnSc1	from
Hell	Hell	k1gMnSc1	Hell
<g/>
:	:	kIx,	:
Poems	Poems	k1gInSc1	Poems
1974	[number]	k4	1974
<g/>
-	-	kIx~	-
<g/>
1977	[number]	k4	1977
<g/>
.	.	kIx.	.
</s>
<s>
Art	Art	k?	Art
(	(	kIx(	(
<g/>
Black	Black	k1gMnSc1	Black
Sparrow	Sparrow	k1gMnSc1	Sparrow
Press	Press	k1gInSc4	Press
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
-	-	kIx~	-
jediná	jediný	k2eAgFnSc1d1	jediná
báseň	báseň	k1gFnSc1	báseň
vydaná	vydaný	k2eAgFnSc1d1	vydaná
v	v	k7c6	v
uměleckém	umělecký	k2eAgNnSc6d1	umělecké
provedení	provedení	k1gNnSc6	provedení
jako	jako	k8xC	jako
novoroční	novoroční	k2eAgInSc4d1	novoroční
dárek	dárek	k1gInSc4	dárek
pro	pro	k7c4	pro
přátele	přítel	k1gMnPc4	přítel
Black	Blacka	k1gFnPc2	Blacka
Sparrow	Sparrow	k1gFnSc7	Sparrow
Press	Pressa	k1gFnPc2	Pressa
<g/>
.	.	kIx.	.
</s>
<s>
Láska	láska	k1gFnSc1	láska
je	být	k5eAaImIp3nS	být
pes	pes	k1gMnSc1	pes
<g/>
:	:	kIx,	:
Básně	báseň	k1gFnSc2	báseň
1974-1978	[number]	k4	1974-1978
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
Love	lov	k1gInSc5	lov
is	is	k?	is
a	a	k8xC	a
Dog	doga	k1gFnPc2	doga
from	from	k1gMnSc1	from
Hell	Hell	k1gMnSc1	Hell
<g/>
:	:	kIx,	:
Poems	Poems	k1gInSc1	Poems
1974	[number]	k4	1974
<g/>
-	-	kIx~	-
<g/>
1977	[number]	k4	1977
<g/>
;	;	kIx,	;
Black	Black	k1gMnSc1	Black
Sparrow	Sparrow	k1gMnSc1	Sparrow
Press	Press	k1gInSc4	Press
1977	[number]	k4	1977
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Pragma	Pragma	k1gFnSc1	Pragma
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
-	-	kIx~	-
snad	snad	k9	snad
nejpopulárnější	populární	k2eAgFnSc1d3	nejpopulárnější
básnická	básnický	k2eAgFnSc1d1	básnická
sbírka	sbírka	k1gFnSc1	sbírka
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
Love	lov	k1gInSc5	lov
Poem	poema	k1gFnPc2	poema
(	(	kIx(	(
<g/>
Black	Black	k1gMnSc1	Black
Sparrow	Sparrow	k1gMnSc1	Sparrow
Press	Press	k1gInSc4	Press
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
-	-	kIx~	-
vyšla	vyjít	k5eAaPmAgFnS	vyjít
jako	jako	k9	jako
novoroční	novoroční	k2eAgInSc4d1	novoroční
dárek	dárek	k1gInSc4	dárek
<g/>
.	.	kIx.	.
</s>
<s>
Play	play	k0	play
the	the	k?	the
Piano	piano	k1gNnSc1	piano
Drunk	Drunk	k1gMnSc1	Drunk
<g/>
/	/	kIx~	/
Like	Like	k1gInSc1	Like
a	a	k8xC	a
Percussion	Percussion	k1gInSc1	Percussion
Instrument	instrument	k1gInSc1	instrument
<g/>
/	/	kIx~	/
Until	Until	k1gInSc1	Until
the	the	k?	the
Fingers	Fingers	k1gInSc1	Fingers
Begin	Begin	k1gInSc1	Begin
to	ten	k3xDgNnSc1	ten
Bleed	Bleed	k1gMnSc1	Bleed
a	a	k8xC	a
Bit	bit	k2eAgMnSc1d1	bit
(	(	kIx(	(
<g/>
Black	Black	k1gMnSc1	Black
Sparrow	Sparrow	k1gMnSc1	Sparrow
Press	Press	k1gInSc4	Press
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
-	-	kIx~	-
tato	tento	k3xDgFnSc1	tento
sbírka	sbírka	k1gFnSc1	sbírka
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
básní	báseň	k1gFnPc2	báseň
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
byly	být	k5eAaImAgFnP	být
původně	původně	k6eAd1	původně
otiskovány	otiskovat	k5eAaImNgFnP	otiskovat
v	v	k7c6	v
měsíčníku	měsíčník	k1gInSc6	měsíčník
Sparrow	Sparrow	k1gFnSc2	Sparrow
<g/>
.	.	kIx.	.
</s>
<s>
Dangling	Dangling	k1gInSc1	Dangling
in	in	k?	in
the	the	k?	the
Tournefortia	Tournefortia	k1gFnSc1	Tournefortia
(	(	kIx(	(
<g/>
Black	Black	k1gMnSc1	Black
Sparrow	Sparrow	k1gMnSc1	Sparrow
Press	Press	k1gInSc4	Press
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
War	War	k1gFnSc1	War
All	All	k1gFnSc2	All
the	the	k?	the
Time	Time	k1gInSc1	Time
<g/>
:	:	kIx,	:
Poems	Poems	k1gInSc1	Poems
1981	[number]	k4	1981
-	-	kIx~	-
1984	[number]	k4	1984
(	(	kIx(	(
<g/>
Black	Black	k1gMnSc1	Black
Sparrow	Sparrow	k1gMnSc1	Sparrow
Press	Press	k1gInSc4	Press
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
The	The	k1gFnSc7	The
Last	Last	k2eAgInSc4d1	Last
Generation	Generation	k1gInSc4	Generation
(	(	kIx(	(
<g/>
Black	Black	k1gMnSc1	Black
Sparrow	Sparrow	k1gMnSc1	Sparrow
Press	Press	k1gInSc4	Press
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
-	-	kIx~	-
vyšla	vyjít	k5eAaPmAgFnS	vyjít
jako	jako	k9	jako
novoroční	novoroční	k2eAgInSc4d1	novoroční
dárek	dárek	k1gInSc4	dárek
<g/>
.	.	kIx.	.
</s>
<s>
Sparks	Sparks	k1gInSc1	Sparks
(	(	kIx(	(
<g/>
Black	Black	k1gMnSc1	Black
Sparrow	Sparrow	k1gMnSc1	Sparrow
Press	Press	k1gInSc4	Press
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
-	-	kIx~	-
vyšla	vyjít	k5eAaPmAgFnS	vyjít
jako	jako	k9	jako
novoroční	novoroční	k2eAgInSc4d1	novoroční
dárek	dárek	k1gInSc4	dárek
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
jste	být	k5eAaImIp2nP	být
tak	tak	k9	tak
sami	sám	k3xTgMnPc1	sám
<g/>
,	,	kIx,	,
až	až	k9	až
to	ten	k3xDgNnSc1	ten
prostě	prostě	k9	prostě
dává	dávat	k5eAaImIp3nS	dávat
smysl	smysl	k1gInSc4	smysl
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
You	You	k1gMnSc1	You
Get	Get	k1gMnSc1	Get
So	So	kA	So
Alone	Alon	k1gInSc5	Alon
at	at	k?	at
Times	Times	k1gMnSc1	Times
That	That	k1gMnSc1	That
It	It	k1gMnSc1	It
Just	just	k6eAd1	just
Makes	Makes	k1gInSc4	Makes
Sense	Sense	k1gFnSc2	Sense
<g/>
,	,	kIx,	,
Black	Black	k1gMnSc1	Black
Sparrow	Sparrow	k1gFnSc2	Sparrow
Press	Press	k1gInSc1	Press
1986	[number]	k4	1986
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
-	-	kIx~	-
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
rozsáhlých	rozsáhlý	k2eAgFnPc2d1	rozsáhlá
pozdních	pozdní	k2eAgFnPc2d1	pozdní
sbírek	sbírka	k1gFnPc2	sbírka
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Roominghouse	Roominghouse	k1gFnSc1	Roominghouse
Madrigals	Madrigals	k1gInSc1	Madrigals
<g/>
:	:	kIx,	:
Early	earl	k1gMnPc4	earl
Selected	Selected	k1gInSc4	Selected
Poems	Poems	k1gInSc1	Poems
1946	[number]	k4	1946
<g/>
–	–	k?	–
<g/>
1966	[number]	k4	1966
(	(	kIx(	(
<g/>
Black	Black	k1gMnSc1	Black
Sparrow	Sparrow	k1gMnSc1	Sparrow
Press	Press	k1gInSc4	Press
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
-	-	kIx~	-
John	John	k1gMnSc1	John
Martin	Martin	k1gMnSc1	Martin
prošel	projít	k5eAaPmAgMnS	projít
malé	malý	k2eAgInPc4d1	malý
literární	literární	k2eAgInPc4d1	literární
časopisy	časopis	k1gInPc4	časopis
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
sestavil	sestavit	k5eAaPmAgMnS	sestavit
tuto	tento	k3xDgFnSc4	tento
sbírku	sbírka	k1gFnSc4	sbírka
rané	raný	k2eAgFnSc2d1	raná
básnické	básnický	k2eAgFnSc2d1	básnická
tvorby	tvorba	k1gFnSc2	tvorba
Ch	Ch	kA	Ch
<g/>
.	.	kIx.	.
Bukowského	Bukowský	k1gMnSc2	Bukowský
<g/>
.	.	kIx.	.
</s>
<s>
Básne	Básnout	k5eAaPmIp3nS	Básnout
napísané	napísaný	k2eAgInPc1d1	napísaný
pred	pred	k6eAd1	pred
skokom	skokom	k1gInSc4	skokom
z	z	k7c2	z
8	[number]	k4	8
<g/>
.	.	kIx.	.
poschodia	poschodium	k1gNnSc2	poschodium
-	-	kIx~	-
básnický	básnický	k2eAgInSc1d1	básnický
výbor	výbor	k1gInSc1	výbor
z	z	k7c2	z
několika	několik	k4yIc2	několik
sbírek	sbírka	k1gFnPc2	sbírka
vydaný	vydaný	k2eAgMnSc1d1	vydaný
nakladatelstvím	nakladatelství	k1gNnPc3	nakladatelství
Slovenský	slovenský	k2eAgInSc1d1	slovenský
spisovateľ	spisovateľ	k?	spisovateľ
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
a	a	k8xC	a
2001	[number]	k4	2001
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
the	the	k?	the
Shadow	Shadow	k1gFnSc2	Shadow
of	of	k?	of
the	the	k?	the
Rose	Rose	k1gMnSc1	Rose
(	(	kIx(	(
<g/>
Black	Black	k1gMnSc1	Black
Sparrow	Sparrow	k1gMnSc1	Sparrow
Press	Press	k1gInSc4	Press
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
-	-	kIx~	-
omezené	omezený	k2eAgNnSc1d1	omezené
vydání	vydání	k1gNnSc1	vydání
věnované	věnovaný	k2eAgNnSc1d1	věnované
Seanu	Seana	k1gFnSc4	Seana
Pennovi	Penn	k1gMnSc3	Penn
<g/>
,	,	kIx,	,
příteli	přítel	k1gMnSc3	přítel
Ch	Ch	kA	Ch
<g/>
.	.	kIx.	.
Bukowského	Bukowský	k1gMnSc2	Bukowský
<g/>
.	.	kIx.	.
</s>
<s>
Three	Three	k6eAd1	Three
by	by	kYmCp3nS	by
Bukowski	Bukowski	k1gNnSc1	Bukowski
(	(	kIx(	(
<g/>
Black	Black	k1gMnSc1	Black
Sparrow	Sparrow	k1gMnSc1	Sparrow
Press	Press	k1gInSc4	Press
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
-	-	kIx~	-
vydání	vydání	k1gNnSc1	vydání
v	v	k7c6	v
omezeném	omezený	k2eAgInSc6d1	omezený
nákladu	náklad	k1gInSc6	náklad
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Last	Last	k2eAgMnSc1d1	Last
Night	Night	k1gMnSc1	Night
of	of	k?	of
the	the	k?	the
Earth	Earth	k1gInSc1	Earth
Poems	Poems	k1gInSc1	Poems
(	(	kIx(	(
<g/>
Black	Black	k1gMnSc1	Black
Sparrow	Sparrow	k1gMnSc1	Sparrow
Press	Press	k1gInSc4	Press
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
-	-	kIx~	-
častými	častý	k2eAgNnPc7d1	časté
tématy	téma	k1gNnPc7	téma
básní	básnit	k5eAaImIp3nS	básnit
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
sbírce	sbírka	k1gFnSc6	sbírka
jsou	být	k5eAaImIp3nP	být
stáří	stáří	k1gNnSc4	stáří
a	a	k8xC	a
smrt	smrt	k1gFnSc4	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Those	Those	k6eAd1	Those
Marvelous	Marvelous	k1gMnSc1	Marvelous
Lunches	Lunches	k1gMnSc1	Lunches
(	(	kIx(	(
<g/>
Black	Black	k1gMnSc1	Black
Sparrow	Sparrow	k1gMnSc1	Sparrow
Press	Press	k1gInSc4	Press
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
-	-	kIx~	-
novoroční	novoroční	k2eAgInSc4d1	novoroční
dárek	dárek	k1gInSc4	dárek
pro	pro	k7c4	pro
přátele	přítel	k1gMnPc4	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Heat	Heat	k2eAgInSc1d1	Heat
Wave	Wave	k1gInSc1	Wave
(	(	kIx(	(
<g/>
Black	Black	k1gMnSc1	Black
Sparrow	Sparrow	k1gMnSc1	Sparrow
Graphic	Graphic	k1gMnSc1	Graphic
Art	Art	k1gMnSc1	Art
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
-	-	kIx~	-
omezené	omezený	k2eAgNnSc1d1	omezené
bibliofilské	bibliofilský	k2eAgNnSc1d1	bibliofilské
vydání	vydání	k1gNnSc1	vydání
ilustrované	ilustrovaný	k2eAgNnSc1d1	ilustrované
výtvarníkem	výtvarník	k1gMnSc7	výtvarník
Kenem	Ken	k1gMnSc7	Ken
Pricem	Price	k1gMnSc7	Price
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
vyšla	vyjít	k5eAaPmAgFnS	vyjít
s	s	k7c7	s
CD	CD	kA	CD
přílohou	příloha	k1gFnSc7	příloha
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
edicích	edice	k1gFnPc6	edice
<g/>
:	:	kIx,	:
obyčejné	obyčejný	k2eAgInPc4d1	obyčejný
a	a	k8xC	a
luxusní	luxusní	k2eAgInPc4d1	luxusní
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
New	New	k1gMnSc1	New
War	War	k1gMnSc1	War
(	(	kIx(	(
<g/>
Black	Black	k1gMnSc1	Black
Sparrow	Sparrow	k1gMnSc1	Sparrow
Press	Press	k1gInSc4	Press
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
-	-	kIx~	-
knížečka	knížečka	k1gFnSc1	knížečka
pěti	pět	k4xCc2	pět
básní	báseň	k1gFnPc2	báseň
pro	pro	k7c4	pro
přátele	přítel	k1gMnPc4	přítel
nakladatelství	nakladatelství	k1gNnSc2	nakladatelství
<g/>
.	.	kIx.	.
</s>
<s>
Bone	bon	k1gInSc5	bon
Palace	Palace	k1gFnPc4	Palace
Ballet	Ballet	k1gInSc1	Ballet
<g/>
:	:	kIx,	:
New	New	k1gFnSc1	New
Poems	Poems	k1gInSc1	Poems
(	(	kIx(	(
<g/>
Black	Black	k1gMnSc1	Black
Sparrow	Sparrow	k1gMnSc1	Sparrow
Press	Press	k1gInSc4	Press
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
-	-	kIx~	-
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
antologie	antologie	k1gFnSc1	antologie
dosud	dosud	k6eAd1	dosud
nepublikovaných	publikovaný	k2eNgFnPc2d1	nepublikovaná
básní	báseň	k1gFnPc2	báseň
vybraných	vybraný	k2eAgFnPc2d1	vybraná
Johnem	John	k1gMnSc7	John
Martinem	Martin	k1gMnSc7	Martin
<g/>
,	,	kIx,	,
vydaná	vydaný	k2eAgNnPc1d1	vydané
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
autora	autor	k1gMnSc2	autor
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc4	název
zvolil	zvolit	k5eAaPmAgMnS	zvolit
John	John	k1gMnSc1	John
Martin	Martin	k1gMnSc1	Martin
<g/>
.	.	kIx.	.
</s>
<s>
What	What	k2eAgInSc1d1	What
matters	matters	k1gInSc1	matters
most	most	k1gInSc1	most
is	is	k?	is
how	how	k?	how
well	well	k1gMnSc1	well
you	you	k?	you
walk	walk	k1gMnSc1	walk
through	through	k1gMnSc1	through
the	the	k?	the
fire	fire	k1gInSc1	fire
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
Open	Open	k1gMnSc1	Open
All	All	k1gMnSc1	All
Night	Night	k1gMnSc1	Night
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
The	The	k1gFnSc6	The
Night	Night	k2eAgMnSc1d1	Night
Torn	Torn	k1gMnSc1	Torn
Mad	Mad	k1gFnSc2	Mad
with	with	k1gMnSc1	with
Footsteps	Footsteps	k1gInSc1	Footsteps
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
Sifting	Sifting	k1gInSc1	Sifting
Through	Through	k1gInSc1	Through
the	the	k?	the
Madness	Madnessa	k1gFnPc2	Madnessa
for	forum	k1gNnPc2	forum
the	the	k?	the
Word	Word	kA	Word
<g/>
,	,	kIx,	,
the	the	k?	the
Line	linout	k5eAaImIp3nS	linout
<g/>
,	,	kIx,	,
the	the	k?	the
Way	Way	k1gFnSc1	Way
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
As	as	k9	as
Buddha	Buddha	k1gMnSc1	Buddha
smiles	smiles	k1gMnSc1	smiles
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Flash	Flash	k1gMnSc1	Flash
of	of	k?	of
the	the	k?	the
Lightning	Lightning	k1gInSc1	Lightning
Behind	Behind	k1gMnSc1	Behind
the	the	k?	the
Mountain	Mountain	k1gMnSc1	Mountain
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
Slouching	Slouching	k1gInSc1	Slouching
Toward	Toward	k1gInSc1	Toward
Nirvana	Nirvan	k1gMnSc2	Nirvan
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
Come	Com	k1gInSc2	Com
On	on	k3xPp3gMnSc1	on
In	In	k1gMnSc1	In
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
Příliš	příliš	k6eAd1	příliš
blízko	blízko	k7c2	blízko
jatek	jatka	k1gFnPc2	jatka
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
The	The	k1gMnSc1	The
People	People	k1gMnSc1	People
Look	Look	k1gMnSc1	Look
Like	Like	k1gFnPc2	Like
Flowers	Flowers	k1gInSc4	Flowers
at	at	k?	at
Last	Last	k1gInSc1	Last
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Argo	Argo	k6eAd1	Argo
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Pleasures	Pleasures	k1gMnSc1	Pleasures
of	of	k?	of
the	the	k?	the
Damned	Damned	k1gInSc1	Damned
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
Tvrdej	Tvrdej	k?	Tvrdej
chleba	chléb	k1gInSc2	chléb
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
The	The	k1gFnSc1	The
Continual	Continual	k1gMnSc1	Continual
Condition	Condition	k1gInSc1	Condition
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Argo	Argo	k6eAd1	Argo
2011	[number]	k4	2011
)	)	kIx)	)
Existuje	existovat	k5eAaImIp3nS	existovat
spousta	spousta	k1gFnSc1	spousta
kuriozit	kuriozita	k1gFnPc2	kuriozita
<g/>
,	,	kIx,	,
např.	např.	kA	např.
všelijaká	všelijaký	k3yIgNnPc4	všelijaký
ilustrovaná	ilustrovaný	k2eAgNnPc4d1	ilustrované
vydání	vydání	k1gNnPc4	vydání
v	v	k7c6	v
omezeném	omezený	k2eAgInSc6d1	omezený
nákladu	náklad	k1gInSc6	náklad
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
kniha	kniha	k1gFnSc1	kniha
navíc	navíc	k6eAd1	navíc
podepsaná	podepsaný	k2eAgFnSc1d1	podepsaná
autorem	autor	k1gMnSc7	autor
<g/>
,	,	kIx,	,
její	její	k3xOp3gInSc4	její
cena	cena	k1gFnSc1	cena
jde	jít	k5eAaImIp3nS	jít
do	do	k7c2	do
tisíců	tisíc	k4xCgInPc2	tisíc
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Dear	Dear	k1gInSc1	Dear
Mr	Mr	k1gFnSc2	Mr
Bukowski	Bukowsk	k1gFnSc2	Bukowsk
(	(	kIx(	(
<g/>
Garage	Garage	k1gFnSc1	Garage
Graphics	Graphics	k1gInSc1	Graphics
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
-	-	kIx~	-
série	série	k1gFnSc1	série
Bukowského	Bukowského	k2eAgFnPc2d1	Bukowského
karikatur	karikatura	k1gFnPc2	karikatura
s	s	k7c7	s
krátkým	krátký	k2eAgInSc7d1	krátký
textem	text	k1gInSc7	text
<g/>
,	,	kIx,	,
vytištěných	vytištěný	k2eAgInPc2d1	vytištěný
na	na	k7c6	na
kartičkách	kartička	k1gFnPc6	kartička
<g/>
.	.	kIx.	.
</s>
<s>
Určeno	určen	k2eAgNnSc1d1	určeno
pro	pro	k7c4	pro
sběratele	sběratel	k1gMnPc4	sběratel
<g/>
,	,	kIx,	,
náklad	náklad	k1gInSc1	náklad
50	[number]	k4	50
exemplářů	exemplář	k1gInPc2	exemplář
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Day	Day	k1gMnSc1	Day
It	It	k1gMnSc1	It
Snowed	Snowed	k1gMnSc1	Snowed
in	in	k?	in
LA	la	k1gNnSc4	la
(	(	kIx(	(
<g/>
Paget	Paget	k1gInSc1	Paget
Press	Press	k1gInSc1	Press
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
-	-	kIx~	-
obrázkový	obrázkový	k2eAgInSc1d1	obrázkový
seriál	seriál	k1gInSc1	seriál
s	s	k7c7	s
krátkým	krátký	k2eAgInSc7d1	krátký
textem	text	k1gInSc7	text
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Adventures	Adventuresa	k1gFnPc2	Adventuresa
of	of	k?	of
Clarence	Clarenec	k1gInSc2	Clarenec
Hiram	Hiram	k1gInSc1	Hiram
Sweetmeat	Sweetmeat	k1gInSc1	Sweetmeat
<g/>
"	"	kIx"	"
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
začalo	začít	k5eAaPmAgNnS	začít
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angeles	k1gInSc4	Angeles
sněžit	sněžit	k5eAaImF	sněžit
a	a	k8xC	a
každý	každý	k3xTgMnSc1	každý
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
šílel	šílet	k5eAaImAgMnS	šílet
<g/>
.	.	kIx.	.
</s>
<s>
Clarencin	Clarencin	k1gMnSc1	Clarencin
otec	otec	k1gMnSc1	otec
se	se	k3xPyFc4	se
usměje	usmát	k5eAaPmIp3nS	usmát
po	po	k7c6	po
několika	několik	k4yIc6	několik
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Wedding	Wedding	k1gInSc1	Wedding
(	(	kIx(	(
<g/>
Brown	Brown	k1gMnSc1	Brown
Buddha	Buddha	k1gMnSc1	Buddha
Books	Books	k1gInSc4	Books
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
-	-	kIx~	-
velmi	velmi	k6eAd1	velmi
vzácné	vzácný	k2eAgNnSc1d1	vzácné
vydání	vydání	k1gNnSc1	vydání
v	v	k7c6	v
omezeném	omezený	k2eAgInSc6d1	omezený
nákladu	náklad	k1gInSc6	náklad
<g/>
,	,	kIx,	,
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
svatby	svatba	k1gFnSc2	svatba
Ch	Ch	kA	Ch
<g/>
.	.	kIx.	.
Bukowského	Bukowský	k1gMnSc4	Bukowský
s	s	k7c7	s
Lindou	Linda	k1gFnSc7	Linda
Lee	Lea	k1gFnSc3	Lea
Beighle	Beighle	k1gMnPc3	Beighle
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
</s>
<s>
Text	text	k1gInSc4	text
napsal	napsat	k5eAaPmAgMnS	napsat
spisovatel	spisovatel	k1gMnSc1	spisovatel
sám	sám	k3xTgMnSc1	sám
a	a	k8xC	a
fotografie	fotografia	k1gFnPc4	fotografia
obstaral	obstarat	k5eAaPmAgMnS	obstarat
Michael	Michael	k1gMnSc1	Michael
Montfort	Montfort	k1gInSc4	Montfort
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Movie	Movie	k1gFnSc1	Movie
<g/>
:	:	kIx,	:
Barfly	Barfly	k1gMnSc1	Barfly
(	(	kIx(	(
<g/>
Black	Black	k1gMnSc1	Black
Sparrow	Sparrow	k1gMnSc1	Sparrow
Press	Press	k1gInSc4	Press
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
-	-	kIx~	-
scénář	scénář	k1gInSc1	scénář
k	k	k7c3	k
filmu	film	k1gInSc3	film
Štamgast	štamgast	k1gMnSc1	štamgast
doplněný	doplněný	k2eAgInSc1d1	doplněný
fotografiemi	fotografia	k1gFnPc7	fotografia
<g/>
.	.	kIx.	.
</s>
<s>
Run	run	k1gInSc1	run
with	with	k1gInSc1	with
the	the	k?	the
Hunted	Hunted	k1gInSc1	Hunted
<g/>
:	:	kIx,	:
A	a	k8xC	a
Charles	Charles	k1gMnSc1	Charles
Bukowski	Bukowsk	k1gFnSc2	Bukowsk
Reader	Reader	k1gMnSc1	Reader
(	(	kIx(	(
<g/>
HarperCollins	HarperCollins	k1gInSc1	HarperCollins
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
-	-	kIx~	-
čítanka	čítanka	k1gFnSc1	čítanka
redigovaná	redigovaný	k2eAgFnSc1d1	redigovaná
Johnem	John	k1gMnSc7	John
Martinem	Martin	k1gMnSc7	Martin
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zhruba	zhruba	k6eAd1	zhruba
chronologicky	chronologicky	k6eAd1	chronologicky
podávala	podávat	k5eAaImAgFnS	podávat
příběh	příběh	k1gInSc4	příběh
Henryho	Henry	k1gMnSc2	Henry
Chinaskiho	Chinaski	k1gMnSc2	Chinaski
<g/>
.	.	kIx.	.
</s>
<s>
Neplést	plést	k5eNaImF	plést
se	se	k3xPyFc4	se
stejnojmennou	stejnojmenný	k2eAgFnSc7d1	stejnojmenná
básnickou	básnický	k2eAgFnSc7d1	básnická
sbírkou	sbírka	k1gFnSc7	sbírka
z	z	k7c2	z
r.	r.	kA	r.
1962	[number]	k4	1962
<g/>
.	.	kIx.	.
</s>
<s>
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
Never	Never	k1gMnSc1	Never
Did	Did	k1gMnSc1	Did
This	This	k1gInSc1	This
(	(	kIx(	(
<g/>
City	City	k1gFnSc1	City
Lights	Lights	k1gInSc4	Lights
Books	Books	k1gInSc1	Books
1979	[number]	k4	1979
<g/>
,	,	kIx,	,
Black	Black	k1gMnSc1	Black
Sparrow	Sparrow	k1gFnSc2	Sparrow
Press	Press	k1gInSc1	Press
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
-	-	kIx~	-
cestopis	cestopis	k1gInSc1	cestopis
popisující	popisující	k2eAgInPc1d1	popisující
dvě	dva	k4xCgFnPc1	dva
autorovy	autorův	k2eAgFnPc1d1	autorova
cesty	cesta	k1gFnPc1	cesta
po	po	k7c6	po
Evropě	Evropa	k1gFnSc6	Evropa
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
ilustrovaný	ilustrovaný	k2eAgInSc1d1	ilustrovaný
černobílými	černobílý	k2eAgInPc7d1	černobílý
fotografiemi	fotografia	k1gFnPc7	fotografia
Michaela	Michael	k1gMnSc2	Michael
Montforta	Montfort	k1gMnSc2	Montfort
<g/>
.	.	kIx.	.
</s>
<s>
Próza	próza	k1gFnSc1	próza
doplněná	doplněná	k1gFnSc1	doplněná
básněmi	báseň	k1gFnPc7	báseň
<g/>
.	.	kIx.	.
</s>
<s>
Kapitán	kapitán	k1gMnSc1	kapitán
odešel	odejít	k5eAaPmAgMnS	odejít
na	na	k7c4	na
oběd	oběd	k1gInSc4	oběd
a	a	k8xC	a
námořníci	námořník	k1gMnPc1	námořník
převzali	převzít	k5eAaPmAgMnP	převzít
velení	velený	k2eAgMnPc1d1	velený
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
The	The	k1gMnSc1	The
Captain	Captain	k1gMnSc1	Captain
Is	Is	k1gMnSc1	Is
Out	Out	k1gMnSc1	Out
to	ten	k3xDgNnSc1	ten
Lunch	lunch	k1gInSc1	lunch
and	and	k?	and
the	the	k?	the
Sailors	Sailors	k1gInSc1	Sailors
Have	Have	k1gNnSc1	Have
Taken	Taken	k1gInSc1	Taken
Over	Over	k1gMnSc1	Over
the	the	k?	the
Ship	Ship	k1gMnSc1	Ship
<g/>
,	,	kIx,	,
Black	Black	k1gMnSc1	Black
Sparrow	Sparrow	k1gFnSc2	Sparrow
Graphic	Graphic	k1gMnSc1	Graphic
Art	Art	k1gMnSc1	Art
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
-	-	kIx~	-
deník	deník	k1gInSc1	deník
z	z	k7c2	z
posledních	poslední	k2eAgNnPc2d1	poslední
let	léto	k1gNnPc2	léto
spisovatelova	spisovatelův	k2eAgInSc2d1	spisovatelův
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
ilustroval	ilustrovat	k5eAaBmAgMnS	ilustrovat
R.	R.	kA	R.
Crumb	Crumb	k1gMnSc1	Crumb
<g/>
.	.	kIx.	.
</s>
<s>
Bukowski	Bukowski	k6eAd1	Bukowski
byl	být	k5eAaImAgInS	být
vášnivým	vášnivý	k2eAgMnSc7d1	vášnivý
dopisovatelem	dopisovatel	k1gMnSc7	dopisovatel
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
zbývá	zbývat	k5eAaImIp3nS	zbývat
dost	dost	k6eAd1	dost
nevydané	vydaný	k2eNgFnSc2d1	nevydaná
korespondence	korespondence	k1gFnSc2	korespondence
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Bukowski	Bukowski	k1gNnSc1	Bukowski
<g/>
/	/	kIx~	/
<g/>
Purdy	Purd	k1gInPc1	Purd
Letters	Lettersa	k1gFnPc2	Lettersa
<g/>
:	:	kIx,	:
1964	[number]	k4	1964
-	-	kIx~	-
1974	[number]	k4	1974
(	(	kIx(	(
<g/>
Paget	Paget	k1gInSc1	Paget
Press	Press	k1gInSc1	Press
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
-	-	kIx~	-
korespondence	korespondence	k1gFnSc1	korespondence
mezi	mezi	k7c7	mezi
Bukowským	Bukowský	k1gMnSc7	Bukowský
a	a	k8xC	a
kanadským	kanadský	k2eAgMnSc7d1	kanadský
básníkem	básník	k1gMnSc7	básník
Alem	alma	k1gFnPc2	alma
Purdym	Purdym	k1gInSc1	Purdym
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnPc7	jehož
Bukowski	Bukowsk	k1gMnPc7	Bukowsk
velmi	velmi	k6eAd1	velmi
obdivoval	obdivovat	k5eAaImAgMnS	obdivovat
<g/>
.	.	kIx.	.
</s>
<s>
Nikdy	nikdy	k6eAd1	nikdy
se	se	k3xPyFc4	se
nesetkali	setkat	k5eNaPmAgMnP	setkat
osobně	osobně	k6eAd1	osobně
<g/>
.	.	kIx.	.
</s>
<s>
Screams	Screams	k6eAd1	Screams
from	from	k1gInSc1	from
the	the	k?	the
Balcony	Balcona	k1gFnSc2	Balcona
<g/>
:	:	kIx,	:
Selected	Selected	k1gInSc1	Selected
Letters	Letters	k1gInSc1	Letters
1960	[number]	k4	1960
-	-	kIx~	-
1970	[number]	k4	1970
(	(	kIx(	(
<g/>
Black	Black	k1gMnSc1	Black
Sparrow	Sparrow	k1gMnSc1	Sparrow
Press	Press	k1gInSc4	Press
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
Living	Living	k1gInSc4	Living
on	on	k3xPp3gMnSc1	on
Luck	Luck	k1gMnSc1	Luck
<g/>
:	:	kIx,	:
Selected	Selected	k1gInSc1	Selected
Letters	Lettersa	k1gFnPc2	Lettersa
<g/>
,	,	kIx,	,
Volume	volum	k1gInSc5	volum
2	[number]	k4	2
(	(	kIx(	(
<g/>
Black	Black	k1gMnSc1	Black
Sparrow	Sparrow	k1gMnSc1	Sparrow
Press	Press	k1gInSc4	Press
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
Reach	Reacha	k1gFnPc2	Reacha
for	forum	k1gNnPc2	forum
the	the	k?	the
Sun	Sun	kA	Sun
<g/>
:	:	kIx,	:
Selected	Selected	k1gInSc1	Selected
Letters	Lettersa	k1gFnPc2	Lettersa
<g/>
,	,	kIx,	,
Volume	volum	k1gInSc5	volum
3	[number]	k4	3
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
Beerspit	Beerspit	k1gMnSc1	Beerspit
Night	Night	k1gMnSc1	Night
and	and	k?	and
Cursing	Cursing	k1gInSc1	Cursing
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Correspondense	Correspondense	k1gFnSc2	Correspondense
of	of	k?	of
Charles	Charles	k1gMnSc1	Charles
Bukowski	Bukowski	k1gNnSc2	Bukowski
and	and	k?	and
Sheri	Sher	k1gMnSc3	Sher
Martinelli	Martinell	k1gMnSc3	Martinell
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
Existuje	existovat	k5eAaImIp3nS	existovat
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
televizních	televizní	k2eAgInPc2d1	televizní
dokumentů	dokument	k1gInPc2	dokument
o	o	k7c6	o
Ch	Ch	kA	Ch
<g/>
.	.	kIx.	.
Bukowském	Bukowský	k2eAgInSc6d1	Bukowský
a	a	k8xC	a
několik	několik	k4yIc1	několik
celovečerních	celovečerní	k2eAgInPc2d1	celovečerní
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
z	z	k7c2	z
námětů	námět	k1gInPc2	námět
jeho	jeho	k3xOp3gFnPc2	jeho
knih	kniha	k1gFnPc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgInSc1	sám
Bukowski	Bukowske	k1gFnSc4	Bukowske
napsal	napsat	k5eAaBmAgInS	napsat
scénář	scénář	k1gInSc1	scénář
pouze	pouze	k6eAd1	pouze
k	k	k7c3	k
filmu	film	k1gInSc2	film
Štamgast	štamgast	k1gMnSc1	štamgast
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
Barfly	Barfly	k1gFnSc1	Barfly
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bukowski	Bukowski	k6eAd1	Bukowski
at	at	k?	at
Bellevue	bellevue	k1gFnSc1	bellevue
(	(	kIx(	(
<g/>
Black	Black	k1gInSc1	Black
Sparrow	Sparrow	k1gMnSc1	Sparrow
Press	Press	k1gInSc1	Press
<g/>
/	/	kIx~	/
<g/>
Visionary	Visionara	k1gFnPc1	Visionara
<g/>
)	)	kIx)	)
-	-	kIx~	-
studentský	studentský	k2eAgInSc1d1	studentský
videofilm	videofilm	k1gInSc1	videofilm
z	z	k7c2	z
Bellevue	bellevue	k1gFnSc2	bellevue
College	Colleg	k1gFnSc2	Colleg
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Washington	Washington	k1gInSc4	Washington
z	z	k7c2	z
r.	r.	kA	r.
1970	[number]	k4	1970
<g/>
,	,	kIx,	,
zachycující	zachycující	k2eAgInSc1d1	zachycující
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
prvních	první	k4xOgFnPc2	první
veřejných	veřejný	k2eAgFnPc2d1	veřejná
čtení	čtení	k1gNnPc2	čtení
<g/>
.	.	kIx.	.
</s>
<s>
Nevalná	valný	k2eNgFnSc1d1	nevalná
kvalita	kvalita	k1gFnSc1	kvalita
záznamu	záznam	k1gInSc2	záznam
<g/>
.	.	kIx.	.
</s>
<s>
Bukowski	Bukowski	k1gNnSc1	Bukowski
(	(	kIx(	(
<g/>
KCET	KCET	kA	KCET
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
-	-	kIx~	-
černobílý	černobílý	k2eAgInSc1d1	černobílý
dokument	dokument	k1gInSc1	dokument
režírovaný	režírovaný	k2eAgInSc1d1	režírovaný
Taylorem	Taylor	k1gMnSc7	Taylor
Hackfordem	Hackford	k1gInSc7	Hackford
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
Bukowského	Bukowského	k2eAgNnSc4d1	Bukowského
čtení	čtení	k1gNnSc4	čtení
v	v	k7c6	v
San	San	k1gFnSc6	San
Franciscu	Franciscus	k1gInSc2	Franciscus
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
i	i	k8xC	i
záběry	záběr	k1gInPc1	záběr
jeho	jeho	k3xOp3gFnPc2	jeho
soupeřek	soupeřka	k1gFnPc2	soupeřka
v	v	k7c6	v
lásce	láska	k1gFnSc6	láska
-	-	kIx~	-
Lindu	Linda	k1gFnSc4	Linda
Kingovou	Kingový	k2eAgFnSc4d1	Kingová
a	a	k8xC	a
Lizu	liz	k1gInSc6	liz
Williamsovou	Williamsová	k1gFnSc4	Williamsová
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
byl	být	k5eAaImAgInS	být
vyroben	vyrobit	k5eAaPmNgInS	vyrobit
na	na	k7c4	na
zakázku	zakázka	k1gFnSc4	zakázka
losangeleské	losangeleský	k2eAgFnSc2d1	losangeleská
televizní	televizní	k2eAgFnSc2d1	televizní
stanice	stanice	k1gFnSc2	stanice
KCET	KCET	kA	KCET
<g/>
.	.	kIx.	.
</s>
<s>
Charles	Charles	k1gMnSc1	Charles
Bukowski	Bukowski	k1gNnSc2	Bukowski
-	-	kIx~	-
East	East	k2eAgInSc1d1	East
Hollywood	Hollywood	k1gInSc1	Hollywood
(	(	kIx(	(
<g/>
Thomas	Thomas	k1gMnSc1	Thomas
Schnitt	Schnitt	k1gMnSc1	Schnitt
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
-	-	kIx~	-
černobílý	černobílý	k2eAgInSc4d1	černobílý
dokumentární	dokumentární	k2eAgInSc4d1	dokumentární
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
vzácný	vzácný	k2eAgInSc4d1	vzácný
snímek	snímek	k1gInSc4	snímek
zachycující	zachycující	k2eAgMnPc4d1	zachycující
spisovatele	spisovatel	k1gMnPc4	spisovatel
s	s	k7c7	s
Pamelou	Pamela	k1gFnSc7	Pamela
Millerovou	Millerův	k2eAgFnSc7d1	Millerova
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
chodil	chodit	k5eAaImAgMnS	chodit
po	po	k7c6	po
rozchodu	rozchod	k1gInSc6	rozchod
s	s	k7c7	s
Lindou	Linda	k1gFnSc7	Linda
Kingovou	Kingův	k2eAgFnSc7d1	Kingova
<g/>
.	.	kIx.	.
</s>
<s>
Erekce	erekce	k1gFnPc4	erekce
<g/>
,	,	kIx,	,
ejakulace	ejakulace	k1gFnPc4	ejakulace
<g/>
,	,	kIx,	,
exhibice	exhibice	k1gFnPc4	exhibice
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
příběhy	příběh	k1gInPc4	příběh
obyčejného	obyčejný	k2eAgNnSc2d1	obyčejné
šílenství	šílenství	k1gNnSc2	šílenství
(	(	kIx(	(
<g/>
ital	itala	k1gFnPc2	itala
<g/>
.	.	kIx.	.
</s>
<s>
Storie	Storie	k1gFnSc1	Storie
di	di	k?	di
ordinaria	ordinarius	k1gMnSc2	ordinarius
follia	follius	k1gMnSc2	follius
<g/>
,	,	kIx,	,
angl.	angl.	k?	angl.
Tales	Tales	k1gInSc1	Tales
of	of	k?	of
Ordinary	Ordinara	k1gFnSc2	Ordinara
Madness	Madnessa	k1gFnPc2	Madnessa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
italsko-francouzský	italskorancouzský	k2eAgInSc1d1	italsko-francouzský
film	film	k1gInSc1	film
z	z	k7c2	z
r.	r.	kA	r.
1981	[number]	k4	1981
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Marco	Marco	k6eAd1	Marco
Ferreri	Ferreri	k1gNnSc3	Ferreri
<g/>
,	,	kIx,	,
hrají	hrát	k5eAaImIp3nP	hrát
Ben	Ben	k1gInSc4	Ben
Gazzara	Gazzara	k1gFnSc1	Gazzara
<g/>
,	,	kIx,	,
Ornella	Ornella	k1gFnSc1	Ornella
Muti	Muti	k1gNnSc2	Muti
<g/>
,	,	kIx,	,
Susan	Susan	k1gMnSc1	Susan
Tyrrell	Tyrrell	k1gMnSc1	Tyrrell
,	,	kIx,	,
<g/>
...	...	k?	...
Bukowski	Bukowsk	k1gFnPc1	Bukowsk
se	se	k3xPyFc4	se
natáčení	natáčení	k1gNnSc2	natáčení
neúčastnil	účastnit	k5eNaImAgMnS	účastnit
a	a	k8xC	a
film	film	k1gInSc1	film
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
nelíbil	líbit	k5eNaImAgMnS	líbit
<g/>
.	.	kIx.	.
</s>
<s>
Crazy	Craza	k1gFnPc1	Craza
Love	lov	k1gInSc5	lov
(	(	kIx(	(
<g/>
Mainline	Mainlin	k1gInSc5	Mainlin
Pictures	Pictures	k1gInSc1	Pictures
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
-	-	kIx~	-
film	film	k1gInSc1	film
belgické	belgický	k2eAgFnSc2d1	belgická
televize	televize	k1gFnSc2	televize
režírovaný	režírovaný	k2eAgInSc4d1	režírovaný
Dominiquem	Dominiqu	k1gMnSc7	Dominiqu
Derudderem	Derudder	k1gMnSc7	Derudder
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
použil	použít	k5eAaPmAgMnS	použít
jako	jako	k8xS	jako
předlohu	předloha	k1gFnSc4	předloha
tři	tři	k4xCgFnPc4	tři
Bukowského	Bukowského	k2eAgFnPc4d1	Bukowského
povídky	povídka	k1gFnPc4	povídka
<g/>
.	.	kIx.	.
</s>
<s>
Spisovatel	spisovatel	k1gMnSc1	spisovatel
se	se	k3xPyFc4	se
nepodílel	podílet	k5eNaImAgMnS	podílet
na	na	k7c6	na
natáčení	natáčení	k1gNnSc6	natáčení
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
toto	tento	k3xDgNnSc4	tento
dílo	dílo	k1gNnSc4	dílo
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
filmovou	filmový	k2eAgFnSc4d1	filmová
adaptaci	adaptace	k1gFnSc4	adaptace
své	svůj	k3xOyFgFnSc2	svůj
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Hrají	hrát	k5eAaImIp3nP	hrát
<g/>
:	:	kIx,	:
Josse	Joss	k1gMnPc4	Joss
De	De	k?	De
Pauw	Pauw	k1gFnSc1	Pauw
<g/>
,	,	kIx,	,
Geert	Geert	k1gInSc1	Geert
Hunaerts	Hunaerts	k1gInSc1	Hunaerts
<g/>
,	,	kIx,	,
Michael	Michael	k1gMnSc1	Michael
Pas	pas	k1gNnSc2	pas
<g/>
,	,	kIx,	,
Gene	gen	k1gInSc5	gen
Bervoets	Bervoets	k1gInSc1	Bervoets
<g/>
,	,	kIx,	,
Amid	amid	k1gInSc1	amid
Chakir	Chakira	k1gFnPc2	Chakira
<g/>
,	,	kIx,	,
Florence	Florenc	k1gFnPc1	Florenc
Béliard	Béliard	k1gInSc1	Béliard
<g/>
,	,	kIx,	,
Karen	Karen	k2eAgInSc1d1	Karen
van	van	k1gInSc1	van
Parijs	Parijsa	k1gFnPc2	Parijsa
<g/>
,	,	kIx,	,
Carmela	Carmela	k1gFnSc1	Carmela
Locantore	Locantor	k1gMnSc5	Locantor
<g/>
,	,	kIx,	,
An	An	k1gFnPc3	An
Van	vana	k1gFnPc2	vana
Essche	Essche	k1gFnPc2	Essche
<g/>
,	,	kIx,	,
Doriane	Dorian	k1gMnSc5	Dorian
Moretus	Moretus	k1gInSc4	Moretus
<g/>
,	,	kIx,	,
...	...	k?	...
The	The	k1gMnSc1	The
Charles	Charles	k1gMnSc1	Charles
Bukowski	Bukowsk	k1gFnSc2	Bukowsk
Tapes	Tapes	k1gMnSc1	Tapes
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
-	-	kIx~	-
dokumentární	dokumentární	k2eAgInSc1d1	dokumentární
film	film	k1gInSc1	film
Barbeta	Barbet	k1gMnSc2	Barbet
Schroedera	Schroeder	k1gMnSc2	Schroeder
o	o	k7c6	o
Charlesi	Charles	k1gMnSc6	Charles
Bukowském	Bukowský	k1gMnSc6	Bukowský
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
videodokumentu	videodokument	k1gInSc6	videodokument
Bukowski	Bukowsk	k1gFnSc2	Bukowsk
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
svém	svůj	k3xOyFgInSc6	svůj
životě	život	k1gInSc6	život
a	a	k8xC	a
díle	dílo	k1gNnSc6	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
52	[number]	k4	52
epizod	epizoda	k1gFnPc2	epizoda
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
240	[number]	k4	240
min	mina	k1gFnPc2	mina
.	.	kIx.	.
Snímku	snímka	k1gFnSc4	snímka
předcházel	předcházet	k5eAaImAgInS	předcházet
obdobně	obdobně	k6eAd1	obdobně
zpracovaný	zpracovaný	k2eAgInSc1d1	zpracovaný
dokument	dokument	k1gInSc1	dokument
s	s	k7c7	s
názvem	název	k1gInSc7	název
Idi	Idi	k1gMnSc2	Idi
Amin	amin	k1gInSc4	amin
Dada	dada	k1gNnSc2	dada
o	o	k7c6	o
ugandském	ugandský	k2eAgMnSc6d1	ugandský
krutovládci	krutovládce	k1gMnSc6	krutovládce
Idi	Idi	k1gMnSc6	Idi
Aminovi	Amin	k1gMnSc6	Amin
<g/>
.	.	kIx.	.
</s>
<s>
Štamgast	štamgast	k1gMnSc1	štamgast
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
Barfly	Barfly	k1gFnSc1	Barfly
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
americký	americký	k2eAgInSc1d1	americký
film	film	k1gInSc1	film
z	z	k7c2	z
r.	r.	kA	r.
1987	[number]	k4	1987
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Barbet	Barbet	k1gMnSc1	Barbet
Schroeder	Schroeder	k1gMnSc1	Schroeder
<g/>
,	,	kIx,	,
hrají	hrát	k5eAaImIp3nP	hrát
Mickey	Micke	k2eAgFnPc4d1	Micke
Rourke	Rourk	k1gFnPc4	Rourk
<g/>
,	,	kIx,	,
Faye	Fay	k1gFnPc4	Fay
Dunaway	Dunawaa	k1gFnSc2	Dunawaa
<g/>
,	,	kIx,	,
Alice	Alice	k1gFnSc1	Alice
Krige	Krige	k1gFnSc1	Krige
<g/>
,	,	kIx,	,
Jack	Jack	k1gInSc1	Jack
Nance	Nanka	k1gFnSc3	Nanka
<g/>
,	,	kIx,	,
Frank	frank	k1gInSc4	frank
Stallone	Stallon	k1gInSc5	Stallon
<g/>
,	,	kIx,	,
Pruitt	Pruitt	k2eAgMnSc1d1	Pruitt
Taylor	Taylor	k1gMnSc1	Taylor
Vince	Vince	k?	Vince
<g/>
,	,	kIx,	,
Leonard	Leonard	k1gMnSc1	Leonard
Termo	Terma	k1gFnSc5	Terma
<g/>
,	,	kIx,	,
Charles	Charles	k1gMnSc1	Charles
Bukowski	Bukowsk	k1gFnSc2	Bukowsk
<g/>
,	,	kIx,	,
...	...	k?	...
I	i	k9	i
<g/>
́	́	k?	́
<g/>
m	m	kA	m
Still	Still	k1gInSc1	Still
Here	Here	k1gInSc1	Here
(	(	kIx(	(
<g/>
Tag	tag	k1gInSc1	tag
<g/>
/	/	kIx~	/
<g/>
Trum	Trum	k1gInSc1	Trum
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
-	-	kIx~	-
německý	německý	k2eAgInSc1d1	německý
barevný	barevný	k2eAgInSc1d1	barevný
dokument	dokument	k1gInSc1	dokument
natočený	natočený	k2eAgInSc1d1	natočený
Thomasem	Thomas	k1gMnSc7	Thomas
Schnittem	Schnitt	k1gMnSc7	Schnitt
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
záběry	záběr	k1gInPc4	záběr
Charlese	Charles	k1gMnSc2	Charles
Bukowského	Bukowský	k1gMnSc2	Bukowský
doma	doma	k6eAd1	doma
v	v	k7c6	v
San	San	k1gFnSc6	San
Pedru	Pedr	k1gInSc2	Pedr
po	po	k7c6	po
vypuknutí	vypuknutí	k1gNnSc6	vypuknutí
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Ordinary	Ordinara	k1gFnPc1	Ordinara
Madness	Madnessa	k1gFnPc2	Madnessa
of	of	k?	of
Charles	Charles	k1gMnSc1	Charles
Bukowski	Bukowsk	k1gFnSc2	Bukowsk
(	(	kIx(	(
<g/>
BBC	BBC	kA	BBC
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
-	-	kIx~	-
díl	díl	k1gInSc1	díl
seriálu	seriál	k1gInSc2	seriál
britské	britský	k2eAgFnSc2d1	britská
televize	televize	k1gFnSc2	televize
BBC	BBC	kA	BBC
Bookmarks	Bookmarks	k1gInSc1	Bookmarks
o	o	k7c6	o
současných	současný	k2eAgMnPc6d1	současný
autorech	autor	k1gMnPc6	autor
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
vysílán	vysílat	k5eAaImNgInS	vysílat
po	po	k7c6	po
spisovatelově	spisovatelův	k2eAgFnSc6d1	spisovatelova
smrti	smrt	k1gFnSc6	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Bukowski	Bukowski	k1gNnSc1	Bukowski
<g/>
:	:	kIx,	:
Born	Born	k1gInSc1	Born
Into	Into	k1gNnSc1	Into
This	This	k1gInSc1	This
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
-	-	kIx~	-
dokument	dokument	k1gInSc1	dokument
Faktótum	faktótum	k1gNnSc1	faktótum
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
Factotum	Factotum	k1gNnSc4	Factotum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
film	film	k1gInSc1	film
USA	USA	kA	USA
<g/>
/	/	kIx~	/
<g/>
Norsko	Norsko	k1gNnSc1	Norsko
<g/>
/	/	kIx~	/
<g/>
Francie	Francie	k1gFnSc1	Francie
<g/>
/	/	kIx~	/
<g/>
Německo	Německo	k1gNnSc1	Německo
z	z	k7c2	z
r.	r.	kA	r.
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Bent	Benta	k1gFnPc2	Benta
Hamer	Hamry	k1gInPc2	Hamry
,	,	kIx,	,
hrají	hrát	k5eAaImIp3nP	hrát
Matt	Matt	k2eAgInSc4d1	Matt
Dillon	Dillon	k1gInSc4	Dillon
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Lili	Lili	k1gFnSc3	Lili
Taylor	Taylor	k1gMnSc1	Taylor
<g/>
,	,	kIx,	,
Marisa	Marisa	k1gFnSc1	Marisa
Tomei	Tome	k1gFnSc2	Tome
<g/>
,	,	kIx,	,
Jim	on	k3xPp3gMnPc3	on
Brockhohn	Brockhohn	k1gNnSc1	Brockhohn
<g/>
,	,	kIx,	,
Chris	Chris	k1gFnSc1	Chris
Carlson	Carlson	k1gMnSc1	Carlson
<g/>
,	,	kIx,	,
Joseph	Joseph	k1gMnSc1	Joseph
Courtemanche	Courtemanche	k1gFnPc2	Courtemanche
<g/>
,	,	kIx,	,
Fisher	Fishra	k1gFnPc2	Fishra
Stevens	Stevensa	k1gFnPc2	Stevensa
<g/>
,	,	kIx,	,
Didier	Didira	k1gFnPc2	Didira
Flamand	Flamanda	k1gFnPc2	Flamanda
<g/>
,	,	kIx,	,
Adrienne	Adrienn	k1gInSc5	Adrienn
Shelly	Shell	k1gMnPc7	Shell
<g/>
,	,	kIx,	,
<g/>
...	...	k?	...
There	Ther	k1gInSc5	Ther
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Gonna	Gonn	k1gInSc2	Gonn
Be	Be	k1gFnSc2	Be
a	a	k8xC	a
God	God	k1gFnSc2	God
Damn	Damn	k1gMnSc1	Damn
Riot	Riot	k1gMnSc1	Riot
in	in	k?	in
Here	Here	k1gInSc1	Here
-	-	kIx~	-
zfilmováno	zfilmovat	k5eAaPmNgNnS	zfilmovat
r.	r.	kA	r.
1979	[number]	k4	1979
<g/>
,	,	kIx,	,
vydáno	vydat	k5eAaPmNgNnS	vydat
na	na	k7c6	na
DVD	DVD	kA	DVD
r.	r.	kA	r.
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Last	Last	k1gInSc4	Last
Straw	Straw	k1gMnSc4	Straw
-	-	kIx~	-
dokumentární	dokumentární	k2eAgInSc4d1	dokumentární
film	film	k1gInSc4	film
zobrazující	zobrazující	k2eAgNnSc1d1	zobrazující
čtení	čtení	k1gNnSc1	čtení
pozdní	pozdní	k2eAgFnSc2d1	pozdní
poezie	poezie	k1gFnSc2	poezie
Charlese	Charles	k1gMnSc2	Charles
Bukowského	Bukowský	k1gMnSc2	Bukowský
v	v	k7c6	v
The	The	k1gFnSc6	The
Sweetwater	Sweetwatra	k1gFnPc2	Sweetwatra
<g/>
,	,	kIx,	,
hudebním	hudební	k2eAgInSc6d1	hudební
klubu	klub	k1gInSc6	klub
v	v	k7c4	v
Redondo	Redondo	k1gNnSc4	Redondo
Beach	Beacha	k1gFnPc2	Beacha
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
dne	den	k1gInSc2	den
31.3	[number]	k4	31.3
<g/>
.1980	.1980	k4	.1980
<g/>
.	.	kIx.	.
</s>
<s>
Režie	režie	k1gFnSc1	režie
Jon	Jon	k1gMnSc2	Jon
Monday	Mondaa	k1gMnSc2	Mondaa
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Suicide	Suicid	k1gMnSc5	Suicid
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
-	-	kIx~	-
krátký	krátký	k2eAgInSc4d1	krátký
15	[number]	k4	15
<g/>
-minutový	inutový	k2eAgInSc4d1	-minutový
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
předlohou	předloha	k1gFnSc7	předloha
byla	být	k5eAaImAgFnS	být
povídka	povídka	k1gFnSc1	povídka
Sebevražda	sebevražda	k1gFnSc1	sebevražda
ze	z	k7c2	z
sbírky	sbírka	k1gFnSc2	sbírka
Kam	kam	k6eAd1	kam
zmizela	zmizet	k5eAaPmAgFnS	zmizet
ta	ten	k3xDgFnSc1	ten
roztomilá	roztomilý	k2eAgFnSc1d1	roztomilá
rozesmátá	rozesmátý	k2eAgFnSc1d1	rozesmátá
holka	holka	k1gFnSc1	holka
v	v	k7c6	v
květovaných	květovaný	k2eAgInPc6d1	květovaný
šatech	šat	k1gInPc6	šat
<g/>
:	:	kIx,	:
Básně	báseň	k1gFnPc1	báseň
a	a	k8xC	a
povídky	povídka	k1gFnPc1	povídka
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
Betting	Betting	k1gInSc1	Betting
on	on	k3xPp3gMnSc1	on
the	the	k?	the
Muse	Musa	k1gFnSc3	Musa
<g/>
:	:	kIx,	:
Poems	Poems	k1gInSc1	Poems
and	and	k?	and
Stories	Stories	k1gInSc1	Stories
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Motto	motto	k1gNnSc1	motto
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Skok	skok	k1gInSc1	skok
z	z	k7c2	z
mostu	most	k1gInSc2	most
je	být	k5eAaImIp3nS	být
možným	možný	k2eAgNnSc7d1	možné
řešením	řešení	k1gNnSc7	řešení
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Režie	režie	k1gFnSc1	režie
a	a	k8xC	a
scénář	scénář	k1gInSc1	scénář
<g/>
:	:	kIx,	:
Jeff	Jeff	k1gInSc1	Jeff
Markey	Markea	k1gFnSc2	Markea
<g/>
,	,	kIx,	,
hrají	hrát	k5eAaImIp3nP	hrát
<g/>
:	:	kIx,	:
Jeff	Jeff	k1gInSc1	Jeff
Markey	Markea	k1gFnSc2	Markea
<g/>
,	,	kIx,	,
Kinna	Kinen	k2eAgFnSc1d1	Kinen
McInroe	McInroe	k1gFnSc1	McInroe
<g/>
,	,	kIx,	,
Jeff	Jeff	k1gInSc1	Jeff
Marchelletta	Marchelletto	k1gNnSc2	Marchelletto
<g/>
,	,	kIx,	,
Chase	chasa	k1gFnSc3	chasa
Kim	Kim	k1gFnSc2	Kim
<g/>
,	,	kIx,	,
Ricardo	Ricardo	k1gNnSc1	Ricardo
Mamood-Vega	Mamood-Vega	k1gFnSc1	Mamood-Vega
<g/>
,	,	kIx,	,
Hardia	Hardium	k1gNnSc2	Hardium
Madden	Maddna	k1gFnPc2	Maddna
<g/>
,	,	kIx,	,
Luz	luza	k1gFnPc2	luza
Minerva	Minerva	k1gFnSc1	Minerva
<g/>
,	,	kIx,	,
Bonnie	Bonnie	k1gFnSc1	Bonnie
Kostecka	Kostecka	k1gFnSc1	Kostecka
<g/>
,	,	kIx,	,
Thomas	Thomas	k1gMnSc1	Thomas
James	James	k1gMnSc1	James
Markey	Markea	k1gFnSc2	Markea
<g/>
,	,	kIx,	,
Dan	Dan	k1gMnSc1	Dan
Gilvary	Gilvar	k1gInPc4	Gilvar
<g/>
,	,	kIx,	,
Sckljork	Sckljork	k1gInSc1	Sckljork
Wyckejam	Wyckejam	k1gInSc1	Wyckejam
<g/>
.	.	kIx.	.
</s>
<s>
Charles	Charles	k1gMnSc1	Charles
Bukowski	Bukowsk	k1gFnSc2	Bukowsk
<g/>
:	:	kIx,	:
One	One	k1gFnSc1	One
Tough	Tougha	k1gFnPc2	Tougha
Mother	Mothra	k1gFnPc2	Mothra
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
-	-	kIx~	-
sada	sada	k1gFnSc1	sada
2	[number]	k4	2
DVD	DVD	kA	DVD
předchozích	předchozí	k2eAgNnPc2d1	předchozí
vydání	vydání	k1gNnSc6	vydání
There	Ther	k1gInSc5	Ther
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Gonna	Gonn	k1gInSc2	Gonn
Be	Be	k1gFnSc2	Be
a	a	k8xC	a
God	God	k1gFnSc2	God
Damn	Damn	k1gMnSc1	Damn
Riot	Riot	k1gMnSc1	Riot
in	in	k?	in
Here	Her	k1gFnSc2	Her
a	a	k8xC	a
The	The	k1gFnSc2	The
Last	Last	k1gMnSc1	Last
Straw	Straw	k1gMnSc1	Straw
<g/>
.	.	kIx.	.
</s>
<s>
Sandorf	Sandorf	k1gInSc1	Sandorf
Dorbin	Dorbin	k2eAgInSc1d1	Dorbin
<g/>
,	,	kIx,	,
A	a	k8xC	a
Bibliography	Bibliographa	k1gFnPc1	Bibliographa
of	of	k?	of
Charles	Charles	k1gMnSc1	Charles
Bukowski	Bukowsk	k1gFnSc2	Bukowsk
(	(	kIx(	(
<g/>
Black	Black	k1gMnSc1	Black
Sparrow	Sparrow	k1gMnSc1	Sparrow
Press	Press	k1gInSc4	Press
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
Hugh	Hugha	k1gFnPc2	Hugha
Fox	fox	k1gInSc1	fox
<g/>
,	,	kIx,	,
Charles	Charles	k1gMnSc1	Charles
Bukowski	Bukowsk	k1gFnSc2	Bukowsk
<g/>
:	:	kIx,	:
A	a	k9	a
Critical	Critical	k1gFnSc1	Critical
and	and	k?	and
Bibliographical	Bibliographical	k1gFnSc1	Bibliographical
Study	stud	k1gInPc4	stud
(	(	kIx(	(
<g/>
Abyss	Abyss	k1gInSc4	Abyss
Publications	Publicationsa	k1gFnPc2	Publicationsa
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
Jory	Jora	k1gFnSc2	Jora
Sherman	Sherman	k1gMnSc1	Sherman
<g/>
,	,	kIx,	,
Bukowski	Bukowsk	k1gMnPc1	Bukowsk
<g/>
:	:	kIx,	:
Friendship	Friendship	k1gInSc1	Friendship
<g/>
,	,	kIx,	,
Fame	Fame	k1gInSc1	Fame
and	and	k?	and
Bestial	Bestial	k1gInSc1	Bestial
Myth	Myth	k1gInSc1	Myth
(	(	kIx(	(
<g/>
Blue	Blu	k1gInPc1	Blu
Horse	Horse	k1gFnSc1	Horse
Publications	Publications	k1gInSc1	Publications
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
Loss	Loss	k1gInSc4	Loss
Pequen	Pequen	k2eAgInSc4d1	Pequen
Glazier	Glazier	k1gInSc4	Glazier
<g/>
,	,	kIx,	,
All	All	k1gFnSc4	All
<g/>
́	́	k?	́
<g/>
s	s	k7c7	s
Normal	Normal	k1gMnSc1	Normal
Here	Here	k1gInSc1	Here
(	(	kIx(	(
<g/>
Ruddy	Rudda	k1gFnSc2	Rudda
Duck	Duck	k1gInSc1	Duck
Press	Press	k1gInSc1	Press
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
-	-	kIx~	-
sbírka	sbírka	k1gFnSc1	sbírka
textů	text	k1gInPc2	text
o	o	k7c6	o
Bukowském	Bukowské	k1gNnSc6	Bukowské
napsaná	napsaný	k2eAgFnSc1d1	napsaná
jeho	jeho	k3xOp3gMnSc7	jeho
přáteli	přítel	k1gMnPc7	přítel
<g/>
.	.	kIx.	.
</s>
<s>
David	David	k1gMnSc1	David
Barker	Barker	k1gMnSc1	Barker
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
King	King	k1gMnSc1	King
of	of	k?	of
San	San	k1gFnSc1	San
Pedro	Pedro	k1gNnSc4	Pedro
(	(	kIx(	(
<g/>
Richard	Richard	k1gMnSc1	Richard
<g/>
.	.	kIx.	.
</s>
<s>
G.	G.	kA	G.
Wrong	Wrong	k1gMnSc1	Wrong
and	and	k?	and
Co	co	k9	co
<g/>
.	.	kIx.	.
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
-	-	kIx~	-
kuriózní	kuriózní	k2eAgNnSc1d1	kuriózní
minivydání	minivydání	k1gNnSc1	minivydání
obsahující	obsahující	k2eAgInPc1d1	obsahující
základní	základní	k2eAgInPc1d1	základní
údaje	údaj	k1gInPc1	údaj
o	o	k7c6	o
životě	život	k1gInSc6	život
spisovatele	spisovatel	k1gMnSc2	spisovatel
<g/>
.	.	kIx.	.
</s>
<s>
Michael	Michael	k1gMnSc1	Michael
Montfort	Montfort	k1gInSc1	Montfort
<g/>
,	,	kIx,	,
Bukowski	Bukowski	k1gNnSc1	Bukowski
(	(	kIx(	(
<g/>
Photographs	Photographs	k1gInSc1	Photographs
1977	[number]	k4	1977
<g/>
-	-	kIx~	-
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Graham	graham	k1gInSc1	graham
Mackintosh	Mackintosh	k1gInSc1	Mackintosh
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
Ed	Ed	k1gMnSc1	Ed
Smith	Smith	k1gMnSc1	Smith
<g/>
,	,	kIx,	,
Sure	Sure	k1gFnSc1	Sure
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
-	-	kIx~	-
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
-	-	kIx~	-
10	[number]	k4	10
čísel	číslo	k1gNnPc2	číslo
bulletinu	bulletin	k1gInSc2	bulletin
obsahující	obsahující	k2eAgNnSc4d1	obsahující
bohaté	bohatý	k2eAgNnSc4d1	bohaté
množství	množství	k1gNnSc4	množství
informací	informace	k1gFnPc2	informace
<g/>
.	.	kIx.	.
</s>
<s>
Neeli	Neel	k1gMnPc1	Neel
Cherkovski	Cherkovsk	k1gFnSc2	Cherkovsk
<g/>
,	,	kIx,	,
Hank	Hank	k1gMnSc1	Hank
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Life	Lif	k1gFnSc2	Lif
of	of	k?	of
Charles	Charles	k1gMnSc1	Charles
Bukowski	Bukowsk	k1gFnSc2	Bukowsk
(	(	kIx(	(
<g/>
Random	Random	k1gInSc1	Random
House	house	k1gNnSc1	house
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
Michael	Michaela	k1gFnPc2	Michaela
Montfort	Montfort	k1gInSc1	Montfort
<g/>
,	,	kIx,	,
Bukowski	Bukowski	k1gNnSc1	Bukowski
-	-	kIx~	-
limitovaná	limitovaný	k2eAgFnSc1d1	limitovaná
edice	edice	k1gFnSc1	edice
fotografií	fotografia	k1gFnPc2	fotografia
<g/>
.	.	kIx.	.
</s>
<s>
Russell	Russell	k1gMnSc1	Russell
Harrison	Harrison	k1gMnSc1	Harrison
<g/>
,	,	kIx,	,
Against	Against	k1gMnSc1	Against
the	the	k?	the
American	American	k1gInSc1	American
Dream	Dream	k1gInSc4	Dream
<g/>
:	:	kIx,	:
Essays	Essays	k1gInSc4	Essays
on	on	k3xPp3gMnSc1	on
Charles	Charles	k1gMnSc1	Charles
Bukowski	Bukowsk	k1gFnSc2	Bukowsk
(	(	kIx(	(
<g/>
Black	Black	k1gMnSc1	Black
Sparrow	Sparrow	k1gMnSc1	Sparrow
Press	Press	k1gInSc4	Press
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
-	-	kIx~	-
kritická	kritický	k2eAgFnSc1d1	kritická
studie	studie	k1gFnSc1	studie
Bukowského	Bukowský	k2eAgNnSc2d1	Bukowský
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Amber	ambra	k1gFnPc2	ambra
O	o	k7c6	o
<g/>
́	́	k?	́
<g/>
Neil	Neil	k1gMnSc1	Neil
-	-	kIx~	-
Blowing	Blowing	k1gInSc1	Blowing
My	my	k3xPp1nPc1	my
Hero	Hero	k1gNnSc1	Hero
(	(	kIx(	(
<g/>
Amber	ambra	k1gFnPc2	ambra
O	o	k7c6	o
<g/>
́	́	k?	́
<g/>
Neil	Neil	k1gInSc1	Neil
Productions	Productions	k1gInSc1	Productions
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
-	-	kIx~	-
memoáry	memoáry	k1gInPc1	memoáry
jedné	jeden	k4xCgFnSc3	jeden
z	z	k7c2	z
autorových	autorův	k2eAgFnPc2d1	autorova
mladých	mladý	k2eAgFnPc2d1	mladá
milenek	milenka	k1gFnPc2	milenka
<g/>
.	.	kIx.	.
</s>
<s>
Gundolf	Gundolf	k1gMnSc1	Gundolf
S.	S.	kA	S.
Fryermuth	Fryermuth	k1gMnSc1	Fryermuth
<g/>
,	,	kIx,	,
Das	Das	k1gMnSc1	Das
war	war	k?	war
<g/>
́	́	k?	́
<g/>
s	s	k7c7	s
(	(	kIx(	(
<g/>
Rasch	Rasch	k1gMnSc1	Rasch
und	und	k?	und
Röhring	Röhring	k1gInSc1	Röhring
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
-	-	kIx~	-
kniha	kniha	k1gFnSc1	kniha
vydaná	vydaný	k2eAgFnSc1d1	vydaná
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
fotografiemi	fotografia	k1gFnPc7	fotografia
Michaela	Michael	k1gMnSc2	Michael
Montforta	Montfort	k1gMnSc2	Montfort
<g/>
.	.	kIx.	.
</s>
<s>
Steve	Steve	k1gMnSc1	Steve
Richmond	Richmond	k1gMnSc1	Richmond
<g/>
,	,	kIx,	,
Vzpomínky	vzpomínka	k1gFnPc1	vzpomínka
na	na	k7c4	na
Bukowského	Bukowského	k2eAgMnPc4d1	Bukowského
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
Spinning	Spinning	k1gInSc1	Spinning
Off	Off	k1gFnSc2	Off
Bukowski	Bukowsk	k1gFnSc2	Bukowsk
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Sun	Sun	kA	Sun
Dog	doga	k1gFnPc2	doga
Press	Pressa	k1gFnPc2	Pressa
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Pragma	Pragma	k1gFnSc1	Pragma
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
-	-	kIx~	-
memoáry	memoáry	k1gInPc1	memoáry
jeho	on	k3xPp3gMnSc2	on
dlouholetého	dlouholetý	k2eAgMnSc2d1	dlouholetý
přítele	přítel	k1gMnSc2	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Gerald	Gerald	k6eAd1	Gerald
Locklin	Locklin	k1gInSc1	Locklin
<g/>
,	,	kIx,	,
Bukowski	Bukowski	k1gNnSc1	Bukowski
<g/>
:	:	kIx,	:
Stoprocentní	stoprocentní	k2eAgInSc1d1	stoprocentní
tip	tip	k1gInSc1	tip
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
Charles	Charles	k1gMnSc1	Charles
Bukowski	Bukowsk	k1gFnSc2	Bukowsk
<g/>
:	:	kIx,	:
A	a	k9	a
Sure	Sure	k1gFnSc1	Sure
Bet	Bet	k1gFnSc1	Bet
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Water	Water	k1gMnSc1	Water
Row	Row	k1gMnSc1	Row
Press	Press	k1gInSc4	Press
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Votobia	Votobia	k1gFnSc1	Votobia
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
-	-	kIx~	-
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
i	i	k9	i
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Bukowski	Bukowski	k1gNnSc2	Bukowski
<g/>
:	:	kIx,	:
Zaručený	zaručený	k2eAgInSc1d1	zaručený
tip	tip	k1gInSc1	tip
od	od	k7c2	od
Volvox	Volvox	k1gInSc1	Volvox
Globator	Globator	k1gInSc1	Globator
v	v	k7c6	v
r.	r.	kA	r.
<g/>
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
Tvrdí	tvrdý	k2eAgMnPc1d1	tvrdý
hoši	hoch	k1gMnPc1	hoch
píšou	psát	k5eAaImIp3nP	psát
básně	báseň	k1gFnPc4	báseň
-	-	kIx~	-
tři	tři	k4xCgInPc4	tři
rozhovory	rozhovor	k1gInPc4	rozhovor
<g/>
,	,	kIx,	,
vydané	vydaný	k2eAgNnSc4d1	vydané
nakladatelstvím	nakladatelství	k1gNnSc7	nakladatelství
Votobia	Votobium	k1gNnSc2	Votobium
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
A.	A.	kA	A.
D.	D.	kA	D.
Winans	Winans	k1gInSc1	Winans
<g/>
,	,	kIx,	,
Charles	Charles	k1gMnSc1	Charles
Bukowski	Bukowsk	k1gFnSc2	Bukowsk
<g/>
/	/	kIx~	/
<g/>
Second	Second	k1gInSc1	Second
Coming	Coming	k1gInSc1	Coming
Years	Years	k1gInSc4	Years
(	(	kIx(	(
<g/>
The	The	k1gMnSc5	The
Bet	Bet	k1gMnSc5	Bet
Scene	Scen	k1gMnSc5	Scen
Press	Press	k1gInSc1	Press
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
Jim	on	k3xPp3gMnPc3	on
Christy	Christ	k1gInPc1	Christ
<g/>
,	,	kIx,	,
Buk	buk	k1gInSc1	buk
Book	Book	k1gInSc1	Book
<g/>
:	:	kIx,	:
Och	och	k0	och
Charles	Charles	k1gMnSc1	Charles
Bukowski	Bukowsk	k1gFnSc2	Bukowsk
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
The	The	k1gFnSc1	The
Buk	buk	k1gInSc1	buk
Book	Book	k1gInSc1	Book
<g/>
:	:	kIx,	:
musings	musings	k6eAd1	musings
of	of	k?	of
Charles	Charles	k1gMnSc1	Charles
Bukowski	Bukowsk	k1gFnSc2	Bukowsk
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
ECW	ECW	kA	ECW
Press	Press	k1gInSc4	Press
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Pragma	Pragma	k1gFnSc1	Pragma
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
-	-	kIx~	-
zajímavý	zajímavý	k2eAgInSc1d1	zajímavý
životopis	životopis	k1gInSc1	životopis
Charlese	Charles	k1gMnSc2	Charles
Bukowského	Bukowský	k1gMnSc2	Bukowský
vydaný	vydaný	k2eAgInSc4d1	vydaný
v	v	k7c6	v
malé	malý	k2eAgFnSc6d1	malá
brožuře	brožura	k1gFnSc6	brožura
<g/>
.	.	kIx.	.
</s>
<s>
Shrnuje	shrnovat	k5eAaImIp3nS	shrnovat
zásadní	zásadní	k2eAgNnSc1d1	zásadní
období	období	k1gNnSc1	období
autorova	autorův	k2eAgInSc2d1	autorův
života	život	k1gInSc2	život
a	a	k8xC	a
uvádí	uvádět	k5eAaImIp3nS	uvádět
mnoho	mnoho	k4c1	mnoho
pikantních	pikantní	k2eAgFnPc2d1	pikantní
historek	historka	k1gFnPc2	historka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
doplněn	doplnit	k5eAaPmNgInS	doplnit
fotografiemi	fotografia	k1gFnPc7	fotografia
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
pořídil	pořídit	k5eAaPmAgMnS	pořídit
Bukowského	Bukowského	k2eAgMnSc1d1	Bukowského
přítel	přítel	k1gMnSc1	přítel
Claude	Claud	k1gInSc5	Claud
Powell	Powell	k1gMnSc1	Powell
<g/>
.	.	kIx.	.
</s>
<s>
Gay	gay	k1gMnSc1	gay
Brewer	Brewer	k1gMnSc1	Brewer
<g/>
,	,	kIx,	,
Charles	Charles	k1gMnSc1	Charles
Bukowski	Bukowsk	k1gFnSc2	Bukowsk
(	(	kIx(	(
<g/>
Twaynes	Twaynes	k1gInSc1	Twaynes
United	United	k1gInSc1	United
States	States	k1gInSc1	States
Authors	Authors	k1gInSc1	Authors
Series	Series	k1gInSc1	Series
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
-	-	kIx~	-
kritická	kritický	k2eAgFnSc1d1	kritická
studie	studie	k1gFnSc1	studie
autorova	autorův	k2eAgNnSc2d1	autorovo
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Philomene	Philomenout	k5eAaPmIp3nS	Philomenout
Longová	Longový	k2eAgFnSc1d1	Longová
<g/>
,	,	kIx,	,
Bukowski	Bukowski	k1gNnSc1	Bukowski
ve	v	k7c6	v
vaně	vana	k1gFnSc6	vana
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
Bukowski	Bukowsk	k1gFnSc2	Bukowsk
in	in	k?	in
the	the	k?	the
Bathtub	Bathtub	k1gInSc1	Bathtub
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Raven	Raven	k1gInSc1	Raven
of	of	k?	of
Temple	templ	k1gInSc5	templ
of	of	k?	of
Man	mana	k1gFnPc2	mana
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Volvox	Volvox	k1gInSc1	Volvox
Globator	Globator	k1gInSc1	Globator
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
-	-	kIx~	-
přepis	přepis	k1gInSc4	přepis
rozhovorů	rozhovor	k1gInPc2	rozhovor
mezi	mezi	k7c7	mezi
Bukowským	Bukowský	k1gMnSc7	Bukowský
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc7	jeho
přítelem	přítel	k1gMnSc7	přítel
Johnem	John	k1gMnSc7	John
Thomasem	Thomas	k1gMnSc7	Thomas
<g/>
,	,	kIx,	,
manželem	manžel	k1gMnSc7	manžel
Philomeny	Philomen	k2eAgInPc4d1	Philomen
Longové	Longový	k2eAgInPc4d1	Longový
<g/>
.	.	kIx.	.
</s>
<s>
Howard	Howard	k1gMnSc1	Howard
Sounes	Sounes	k1gMnSc1	Sounes
<g/>
,	,	kIx,	,
Bláznivý	bláznivý	k2eAgInSc1d1	bláznivý
život	život	k1gInSc1	život
Charlese	Charles	k1gMnSc2	Charles
Bukowského	Bukowský	k1gMnSc2	Bukowský
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
Locked	Locked	k1gInSc1	Locked
in	in	k?	in
the	the	k?	the
Arms	Arms	k1gInSc1	Arms
of	of	k?	of
a	a	k8xC	a
Crazy	Craza	k1gFnSc2	Craza
Life	Lif	k1gInSc2	Lif
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Grove	Groev	k1gFnSc2	Groev
Press	Press	k1gInSc1	Press
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Pragma	Pragma	k1gFnSc1	Pragma
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
-	-	kIx~	-
rozsáhlý	rozsáhlý	k2eAgInSc1d1	rozsáhlý
životopis	životopis	k1gInSc1	životopis
Charlese	Charles	k1gMnSc2	Charles
Bukowského	Bukowské	k1gNnSc2	Bukowské
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
čerpal	čerpat	k5eAaImAgMnS	čerpat
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
rozhovorů	rozhovor	k1gInPc2	rozhovor
s	s	k7c7	s
přáteli	přítel	k1gMnPc7	přítel
<g/>
,	,	kIx,	,
příbuznými	příbuzný	k1gMnPc7	příbuzný
i	i	k8xC	i
milenkami	milenka	k1gFnPc7	milenka
kultovního	kultovní	k2eAgMnSc2d1	kultovní
amerického	americký	k2eAgMnSc2d1	americký
spisovatele	spisovatel	k1gMnSc2	spisovatel
<g/>
,	,	kIx,	,
z	z	k7c2	z
korespondence	korespondence	k1gFnSc2	korespondence
a	a	k8xC	a
nevydané	vydaný	k2eNgFnSc2d1	nevydaná
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
i	i	k9	i
Bukowského	Bukowského	k2eAgFnPc4d1	Bukowského
kresby	kresba	k1gFnPc4	kresba
a	a	k8xC	a
raritní	raritní	k2eAgFnPc4d1	raritní
fotografie	fotografia	k1gFnPc4	fotografia
<g/>
.	.	kIx.	.
</s>
<s>
Barry	Barra	k1gFnPc1	Barra
Miles	Milesa	k1gFnPc2	Milesa
<g/>
,	,	kIx,	,
Charles	Charles	k1gMnSc1	Charles
Bukowski	Bukowsk	k1gFnSc2	Bukowsk
(	(	kIx(	(
<g/>
Virgin	Virgin	k2eAgInSc1d1	Virgin
Books	Books	k1gInSc1	Books
Ltd	ltd	kA	ltd
<g/>
.	.	kIx.	.
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Volvox	Volvox	k1gInSc1	Volvox
Globator	Globator	k1gInSc1	Globator
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
-	-	kIx~	-
Bukowského	Bukowského	k2eAgFnSc1d1	Bukowského
biografie	biografie	k1gFnSc1	biografie
v	v	k7c6	v
podání	podání	k1gNnSc6	podání
anglického	anglický	k2eAgMnSc2d1	anglický
životopisce	životopisec	k1gMnSc2	životopisec
Barryho	Barry	k1gMnSc2	Barry
Milese	Milese	k1gFnSc2	Milese
<g/>
.	.	kIx.	.
</s>
<s>
Miles	Miles	k1gInSc1	Miles
se	se	k3xPyFc4	se
s	s	k7c7	s
Bukowskim	Bukowski	k1gNnSc7	Bukowski
seznámil	seznámit	k5eAaPmAgMnS	seznámit
jako	jako	k9	jako
s	s	k7c7	s
autorem	autor	k1gMnSc7	autor
z	z	k7c2	z
cyklostylovaných	cyklostylovaný	k2eAgInPc2d1	cyklostylovaný
amatérských	amatérský	k2eAgInPc2d1	amatérský
časopisů	časopis	k1gInPc2	časopis
<g/>
,	,	kIx,	,
které	který	k3yQgMnPc4	který
prodával	prodávat	k5eAaImAgInS	prodávat
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
londýnském	londýnský	k2eAgNnSc6d1	Londýnské
knihkupectví	knihkupectví	k1gNnSc6	knihkupectví
Indica	Indicum	k1gNnSc2	Indicum
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
setkali	setkat	k5eAaPmAgMnP	setkat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Miles	Miles	k1gMnSc1	Miles
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
projektu	projekt	k1gInSc2	projekt
Zapple	Zapple	k1gFnSc2	Zapple
natočil	natočit	k5eAaBmAgMnS	natočit
Bukowského	Bukowského	k2eAgMnSc1d1	Bukowského
první	první	k4xOgNnPc1	první
autorská	autorský	k2eAgNnPc1d1	autorské
čtení	čtení	k1gNnPc1	čtení
<g/>
.	.	kIx.	.
</s>
