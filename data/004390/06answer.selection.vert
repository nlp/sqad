<s>
Charles	Charles	k1gMnSc1	Charles
Bukowski	Bukowsk	k1gFnSc2	Bukowsk
(	(	kIx(	(
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1920	[number]	k4	1920
<g/>
,	,	kIx,	,
Andernach	Andernach	k1gInSc1	Andernach
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
–	–	k?	–
9	[number]	k4	9
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
San	San	k1gFnPc1	San
Pedro	Pedro	k1gNnSc1	Pedro
<g/>
,	,	kIx,	,
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
celým	celý	k2eAgNnSc7d1	celé
jménem	jméno	k1gNnSc7	jméno
Henry	Henry	k1gMnSc1	Henry
Charles	Charles	k1gMnSc1	Charles
Bukowski	Bukowsk	k1gMnPc1	Bukowsk
<g/>
,	,	kIx,	,
přezdívaný	přezdívaný	k2eAgMnSc1d1	přezdívaný
Hank	Hank	k1gMnSc1	Hank
nebo	nebo	k8xC	nebo
Buk	buk	k1gInSc1	buk
<g/>
,	,	kIx,	,
původním	původní	k2eAgNnSc7d1	původní
německým	německý	k2eAgNnSc7d1	německé
jménem	jméno	k1gNnSc7	jméno
Heinrich	Heinrich	k1gMnSc1	Heinrich
Karl	Karl	k1gMnSc1	Karl
Bukowski	Bukowske	k1gFnSc4	Bukowske
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgMnS	proslavit
nekonvenčními	konvenční	k2eNgFnPc7d1	nekonvenční
autobiografickými	autobiografický	k2eAgFnPc7d1	autobiografická
prózami	próza	k1gFnPc7	próza
a	a	k8xC	a
vulgárním	vulgární	k2eAgInSc7d1	vulgární
stylem	styl	k1gInSc7	styl
<g/>
.	.	kIx.	.
</s>
