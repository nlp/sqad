<s>
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
DNES	dnes	k6eAd1	dnes
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
psáno	psát	k5eAaImNgNnS	psát
také	také	k9	také
Mladá	mladá	k1gFnSc1	mladá
fronta	fronta	k1gFnSc1	fronta
Dnes	dnes	k6eAd1	dnes
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
MF	MF	kA	MF
DNES	dnes	k6eAd1	dnes
<g/>
,	,	kIx,	,
MF	MF	kA	MF
Dnes	dnes	k6eAd1	dnes
nebo	nebo	k8xC	nebo
MfD	MfD	k1gFnSc1	MfD
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nejprodávanějších	prodávaný	k2eAgInPc2d3	nejprodávanější
českých	český	k2eAgInPc2d1	český
deníků	deník	k1gInPc2	deník
<g/>
.	.	kIx.	.
</s>
<s>
Noviny	novina	k1gFnPc1	novina
jsou	být	k5eAaImIp3nP	být
registrované	registrovaný	k2eAgNnSc4d1	registrované
pod	pod	k7c7	pod
kódem	kód	k1gInSc7	kód
ISSN	ISSN	kA	ISSN
1210	[number]	k4	1210
<g/>
-	-	kIx~	-
<g/>
1168	[number]	k4	1168
<g/>
.	.	kIx.	.
</s>
<s>
Začaly	začít	k5eAaPmAgFnP	začít
vycházet	vycházet	k5eAaImF	vycházet
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
zvolením	zvolení	k1gNnSc7	zvolení
podobného	podobný	k2eAgInSc2d1	podobný
názvu	název	k1gInSc2	název
navázaly	navázat	k5eAaPmAgInP	navázat
na	na	k7c4	na
list	list	k1gInSc4	list
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgInS	být
před	před	k7c7	před
sametovou	sametový	k2eAgFnSc7d1	sametová
revolucí	revoluce	k1gFnSc7	revoluce
byl	být	k5eAaImAgMnS	být
orgánem	orgán	k1gInSc7	orgán
Ústředního	ústřední	k2eAgInSc2d1	ústřední
výboru	výbor	k1gInSc2	výbor
Socialistického	socialistický	k2eAgInSc2d1	socialistický
svazu	svaz	k1gInSc2	svaz
mládeže	mládež	k1gFnSc2	mládež
<g/>
,	,	kIx,	,
a	a	k8xC	a
převzaly	převzít	k5eAaPmAgFnP	převzít
i	i	k8xC	i
jeho	jeho	k3xOp3gMnPc4	jeho
redaktory	redaktor	k1gMnPc4	redaktor
<g/>
,	,	kIx,	,
know-how	knowow	k?	know-how
a	a	k8xC	a
čtenářskou	čtenářský	k2eAgFnSc4d1	čtenářská
základnu	základna	k1gFnSc4	základna
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
z	z	k7c2	z
právního	právní	k2eAgNnSc2d1	právní
hlediska	hledisko	k1gNnSc2	hledisko
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
zcela	zcela	k6eAd1	zcela
jiný	jiný	k2eAgInSc4d1	jiný
deník	deník	k1gInSc4	deník
<g/>
.	.	kIx.	.
</s>
<s>
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
DNES	dnes	k6eAd1	dnes
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
revoltou	revolta	k1gFnSc7	revolta
kolektivu	kolektiv	k1gInSc2	kolektiv
redaktorů	redaktor	k1gMnPc2	redaktor
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
k	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
vydávání	vydávání	k1gNnSc3	vydávání
založili	založit	k5eAaPmAgMnP	založit
společnost	společnost	k1gFnSc4	společnost
M	M	kA	M
a	a	k8xC	a
F	F	kA	F
a.s.	a.s.	k?	a.s.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
společně	společně	k6eAd1	společně
s	s	k7c7	s
francouzskou	francouzský	k2eAgFnSc7d1	francouzská
firmou	firma	k1gFnSc7	firma
Socpresse	Socpresse	k1gFnSc2	Socpresse
společnost	společnost	k1gFnSc1	společnost
MaFra	MaFra	k1gFnSc1	MaFra
a.s.	a.s.	k?	a.s.
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
kterou	který	k3yIgFnSc7	který
deník	deník	k1gInSc1	deník
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
původně	původně	k6eAd1	původně
vyvlastněn	vyvlastnit	k5eAaPmNgInS	vyvlastnit
jako	jako	k9	jako
majetek	majetek	k1gInSc1	majetek
SSM	SSM	kA	SSM
<g/>
,	,	kIx,	,
přešel	přejít	k5eAaPmAgInS	přejít
a	a	k8xC	a
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
plně	plně	k6eAd1	plně
vlastněna	vlastnit	k5eAaImNgFnS	vlastnit
německým	německý	k2eAgInSc7d1	německý
koncernem	koncern	k1gInSc7	koncern
Rheinisch-Bergische	Rheinisch-Bergisch	k1gMnSc2	Rheinisch-Bergisch
Druckerei-	Druckerei-	k1gMnSc2	Druckerei-
und	und	k?	und
Verlagsgesellschaft	Verlagsgesellschaft	k1gInSc1	Verlagsgesellschaft
mbH	mbH	k?	mbH
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
RBDV	RBDV	kA	RBDV
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
koupil	koupit	k5eAaPmAgMnS	koupit
vydavatelství	vydavatelství	k1gNnSc4	vydavatelství
Andrej	Andrej	k1gMnSc1	Andrej
Babiš	Babiš	k1gMnSc1	Babiš
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
jeho	jeho	k3xOp3gFnSc1	jeho
firma	firma	k1gFnSc1	firma
Agrofert	Agrofert	k1gInSc4	Agrofert
<g/>
.	.	kIx.	.
</s>
<s>
Redakci	redakce	k1gFnSc3	redakce
tvoří	tvořit	k5eAaImIp3nS	tvořit
centrální	centrální	k2eAgFnSc1d1	centrální
redakce	redakce	k1gFnSc1	redakce
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
14	[number]	k4	14
regionálních	regionální	k2eAgFnPc2d1	regionální
redakcí	redakce	k1gFnPc2	redakce
<g/>
.	.	kIx.	.
</s>
<s>
Šéfredaktorem	šéfredaktor	k1gMnSc7	šéfredaktor
deníku	deník	k1gInSc2	deník
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
července	červenec	k1gInSc2	červenec
2014	[number]	k4	2014
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Plesl	Plesl	k1gInSc1	Plesl
<g/>
.	.	kIx.	.
</s>
<s>
Komentátorem	komentátor	k1gMnSc7	komentátor
MF	MF	kA	MF
DNES	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
Miroslav	Miroslav	k1gMnSc1	Miroslav
Korecký	Korecký	k2eAgMnSc1d1	Korecký
nebo	nebo	k8xC	nebo
byl	být	k5eAaImAgMnS	být
Teodor	Teodor	k1gMnSc1	Teodor
Marjanovič	Marjanovič	k1gMnSc1	Marjanovič
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Bývalý	bývalý	k2eAgMnSc1d1	bývalý
komentátor	komentátor	k1gMnSc1	komentátor
Martin	Martin	k1gMnSc1	Martin
Komárek	Komárek	k1gMnSc1	Komárek
odešel	odejít	k5eAaPmAgMnS	odejít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
do	do	k7c2	do
vrcholné	vrcholný	k2eAgFnSc2d1	vrcholná
politiky	politika	k1gFnSc2	politika
do	do	k7c2	do
hnutí	hnutí	k1gNnSc2	hnutí
ANO	ano	k9	ano
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
vede	vést	k5eAaImIp3nS	vést
vlastník	vlastník	k1gMnSc1	vlastník
MF	MF	kA	MF
DNES	dnes	k6eAd1	dnes
<g/>
.	.	kIx.	.
</s>
<s>
Noviny	novina	k1gFnPc1	novina
se	se	k3xPyFc4	se
zaměřují	zaměřovat	k5eAaImIp3nP	zaměřovat
převážně	převážně	k6eAd1	převážně
na	na	k7c4	na
komentované	komentovaný	k2eAgNnSc4d1	komentované
zpravodajství	zpravodajství	k1gNnSc4	zpravodajství
a	a	k8xC	a
tematické	tematický	k2eAgFnSc2d1	tematická
přílohy	příloha	k1gFnSc2	příloha
<g/>
.	.	kIx.	.
</s>
<s>
Noviny	novina	k1gFnPc1	novina
MF	MF	kA	MF
DNES	dnes	k6eAd1	dnes
jsou	být	k5eAaImIp3nP	být
vydávané	vydávaný	k2eAgFnPc1d1	vydávaná
denně	denně	k6eAd1	denně
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
neděle	neděle	k1gFnSc2	neděle
ve	v	k7c6	v
formátu	formát	k1gInSc6	formát
310	[number]	k4	310
×	×	k?	×
230	[number]	k4	230
mm	mm	kA	mm
s	s	k7c7	s
přílohami	příloha	k1gFnPc7	příloha
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
formátu	formát	k1gInSc6	formát
nebo	nebo	k8xC	nebo
v	v	k7c6	v
časopiseckém	časopisecký	k2eAgInSc6d1	časopisecký
formátu	formát	k1gInSc6	formát
A	a	k9	a
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Vydání	vydání	k1gNnSc1	vydání
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
regionální	regionální	k2eAgFnSc4d1	regionální
přílohu	příloha	k1gFnSc4	příloha
příslušného	příslušný	k2eAgInSc2d1	příslušný
kraje	kraj	k1gInSc2	kraj
nebo	nebo	k8xC	nebo
srovnatelné	srovnatelný	k2eAgFnSc2d1	srovnatelná
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Tištěný	tištěný	k2eAgInSc1d1	tištěný
náklad	náklad	k1gInSc1	náklad
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
podle	podle	k7c2	podle
dne	den	k1gInSc2	den
vydání	vydání	k1gNnSc2	vydání
od	od	k7c2	od
134,3	[number]	k4	134,3
tisíc	tisíc	k4xCgInPc2	tisíc
do	do	k7c2	do
287,2	[number]	k4	287,2
tisíc	tisíc	k4xCgInPc2	tisíc
výtisků	výtisk	k1gInPc2	výtisk
<g/>
.	.	kIx.	.
</s>
<s>
Denně	denně	k6eAd1	denně
je	být	k5eAaImIp3nS	být
prodáno	prodat	k5eAaPmNgNnS	prodat
přibližně	přibližně	k6eAd1	přibližně
144	[number]	k4	144
tisíc	tisíc	k4xCgInPc2	tisíc
výtisků	výtisk	k1gInPc2	výtisk
(	(	kIx(	(
<g/>
k	k	k7c3	k
dubnu	duben	k1gInSc3	duben
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
necelých	celý	k2eNgNnPc2d1	necelé
48	[number]	k4	48
tisíc	tisíc	k4xCgInSc4	tisíc
v	v	k7c6	v
pultovém	pultový	k2eAgInSc6d1	pultový
prodeji	prodej	k1gInSc6	prodej
a	a	k8xC	a
přes	přes	k7c4	přes
89	[number]	k4	89
tisíc	tisíc	k4xCgInPc2	tisíc
formou	forma	k1gFnSc7	forma
předplatného	předplatné	k1gNnSc2	předplatné
<g/>
.	.	kIx.	.
</s>
<s>
Týž	týž	k3xTgMnSc1	týž
vydavatel	vydavatel	k1gMnSc1	vydavatel
provozuje	provozovat	k5eAaImIp3nS	provozovat
také	také	k9	také
internetový	internetový	k2eAgInSc1d1	internetový
portál	portál	k1gInSc1	portál
iDNES	iDNES	k?	iDNES
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
jsou	být	k5eAaImIp3nP	být
publikovány	publikovat	k5eAaBmNgInP	publikovat
některé	některý	k3yIgInPc1	některý
texty	text	k1gInPc1	text
z	z	k7c2	z
tištěného	tištěný	k2eAgInSc2d1	tištěný
deníku	deník	k1gInSc2	deník
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
<g/>
.	.	kIx.	.
</s>
<s>
Redaktoři	redaktor	k1gMnPc1	redaktor
Mladé	mladý	k2eAgFnSc2d1	mladá
fronty	fronta	k1gFnSc2	fronta
si	se	k3xPyFc3	se
vymohli	vymoct	k5eAaPmAgMnP	vymoct
<g/>
,	,	kIx,	,
že	že	k8xS	že
již	již	k6eAd1	již
od	od	k7c2	od
pondělí	pondělí	k1gNnSc2	pondělí
20	[number]	k4	20
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1989	[number]	k4	1989
poskytovali	poskytovat	k5eAaImAgMnP	poskytovat
relativně	relativně	k6eAd1	relativně
pravdivé	pravdivý	k2eAgFnPc4d1	pravdivá
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
událostech	událost	k1gFnPc6	událost
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
některá	některý	k3yIgNnPc1	některý
jiná	jiný	k2eAgNnPc1d1	jiné
média	médium	k1gNnPc1	médium
situaci	situace	k1gFnSc6	situace
stále	stále	k6eAd1	stále
ignorovala	ignorovat	k5eAaImAgFnS	ignorovat
nebo	nebo	k8xC	nebo
bagatelizovala	bagatelizovat	k5eAaImAgFnS	bagatelizovat
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
stále	stále	k6eAd1	stále
ještě	ještě	k9	ještě
mnoho	mnoho	k4c4	mnoho
materiálů	materiál	k1gInPc2	materiál
šéfredaktorka	šéfredaktorka	k1gFnSc1	šéfredaktorka
Miloslava	Miloslava	k1gFnSc1	Miloslava
Moučková	Moučková	k1gFnSc1	Moučková
a	a	k8xC	a
její	její	k3xOp3gMnPc1	její
zástupci	zástupce	k1gMnPc1	zástupce
zcenzurovali	zcenzurovat	k5eAaPmAgMnP	zcenzurovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
revoluční	revoluční	k2eAgFnSc6d1	revoluční
atmosféře	atmosféra	k1gFnSc6	atmosféra
se	s	k7c7	s
20	[number]	k4	20
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1989	[number]	k4	1989
stal	stát	k5eAaPmAgInS	stát
neformálně	formálně	k6eNd1	formálně
šéfredaktorem	šéfredaktor	k1gMnSc7	šéfredaktor
Libor	Libor	k1gMnSc1	Libor
Ševčík	Ševčík	k1gMnSc1	Ševčík
a	a	k8xC	a
podtitul	podtitul	k1gInSc1	podtitul
listu	list	k1gInSc2	list
se	se	k3xPyFc4	se
změnil	změnit	k5eAaPmAgMnS	změnit
na	na	k7c4	na
"	"	kIx"	"
<g/>
deník	deník	k1gInSc4	deník
Československé	československý	k2eAgFnSc2d1	Československá
mládeže	mládež	k1gFnSc2	mládež
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Vydávání	vydávání	k1gNnSc1	vydávání
deníku	deník	k1gInSc2	deník
Mladá	mladá	k1gFnSc1	mladá
fronta	fronta	k1gFnSc1	fronta
bylo	být	k5eAaImAgNnS	být
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
vlastníka	vlastník	k1gMnSc2	vlastník
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1990	[number]	k4	1990
převedeno	převést	k5eAaPmNgNnS	převést
z	z	k7c2	z
ÚV	ÚV	kA	ÚV
SSM	SSM	kA	SSM
na	na	k7c6	na
vydavatelství	vydavatelství	k1gNnSc6	vydavatelství
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1990	[number]	k4	1990
ještě	ještě	k9	ještě
existovaly	existovat	k5eAaImAgInP	existovat
představy	představa	k1gFnSc2	představa
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
vývoj	vývoj	k1gInSc1	vývoj
směřuje	směřovat	k5eAaImIp3nS	směřovat
k	k	k7c3	k
"	"	kIx"	"
<g/>
socialismu	socialismus	k1gInSc3	socialismus
s	s	k7c7	s
lidskou	lidský	k2eAgFnSc7d1	lidská
tváří	tvář	k1gFnSc7	tvář
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
této	tento	k3xDgFnSc2	tento
představy	představa	k1gFnSc2	představa
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
usilovalo	usilovat	k5eAaImAgNnS	usilovat
například	například	k6eAd1	například
o	o	k7c4	o
svoji	svůj	k3xOyFgFnSc4	svůj
změnu	změna	k1gFnSc4	změna
na	na	k7c4	na
státní	státní	k2eAgInSc4d1	státní
podnik	podnik	k1gInSc4	podnik
<g/>
.	.	kIx.	.
</s>
<s>
SSM	SSM	kA	SSM
přejmenovaný	přejmenovaný	k2eAgInSc1d1	přejmenovaný
na	na	k7c4	na
Svaz	svaz	k1gInSc4	svaz
mladých	mladý	k1gMnPc2	mladý
však	však	k9	však
usiloval	usilovat	k5eAaImAgMnS	usilovat
o	o	k7c4	o
udržení	udržení	k1gNnSc4	udržení
tohoto	tento	k3xDgInSc2	tento
majetku	majetek	k1gInSc2	majetek
<g/>
.	.	kIx.	.
</s>
<s>
Deník	deník	k1gInSc1	deník
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
ziskový	ziskový	k2eAgMnSc1d1	ziskový
i	i	k8xC	i
vlivný	vlivný	k2eAgMnSc1d1	vlivný
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	s	k7c7	s
šéfredaktorem	šéfredaktor	k1gMnSc7	šéfredaktor
stal	stát	k5eAaPmAgMnS	stát
Petr	Petr	k1gMnSc1	Petr
Šabata	Šabata	k1gMnSc1	Šabata
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
Hvížďala	Hvížďal	k1gMnSc2	Hvížďal
<g/>
,	,	kIx,	,
od	od	k7c2	od
května	květen	k1gInSc2	květen
1990	[number]	k4	1990
vedoucí	vedoucí	k1gMnSc1	vedoucí
zpravodajského	zpravodajský	k2eAgNnSc2d1	zpravodajské
oddělení	oddělení	k1gNnSc2	oddělení
<g/>
,	,	kIx,	,
přišel	přijít	k5eAaPmAgMnS	přijít
s	s	k7c7	s
myšlenkou	myšlenka	k1gFnSc7	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
alespoň	alespoň	k9	alespoň
jedny	jeden	k4xCgFnPc1	jeden
noviny	novina	k1gFnPc1	novina
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
nezávislé	závislý	k2eNgInPc1d1	nezávislý
na	na	k7c6	na
státu	stát	k1gInSc3	stát
<g/>
,	,	kIx,	,
k	k	k7c3	k
čemuž	což	k3yRnSc3	což
je	být	k5eAaImIp3nS	být
nezbytný	zbytný	k2eNgInSc1d1	zbytný
zahraniční	zahraniční	k2eAgInSc1d1	zahraniční
kapitál	kapitál	k1gInSc1	kapitál
a	a	k8xC	a
privatizace	privatizace	k1gFnSc1	privatizace
<g/>
.	.	kIx.	.
</s>
<s>
Redakce	redakce	k1gFnSc1	redakce
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
vlastnila	vlastnit	k5eAaImAgFnS	vlastnit
jen	jen	k9	jen
jeden	jeden	k4xCgInSc4	jeden
fax	fax	k1gInSc4	fax
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
žádné	žádný	k3yNgInPc4	žádný
počítače	počítač	k1gInPc4	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
se	se	k3xPyFc4	se
uvažovalo	uvažovat	k5eAaImAgNnS	uvažovat
o	o	k7c6	o
společném	společný	k2eAgInSc6d1	společný
projektu	projekt	k1gInSc6	projekt
s	s	k7c7	s
inzertními	inzertní	k2eAgFnPc7d1	inzertní
novinami	novina	k1gFnPc7	novina
Annonce	Annonec	k1gMnSc2	Annonec
a	a	k8xC	a
Josefem	Josef	k1gMnSc7	Josef
Kudláčkem	Kudláček	k1gMnSc7	Kudláček
<g/>
.	.	kIx.	.
</s>
<s>
Annonce	Annonec	k1gMnPc4	Annonec
skutečně	skutečně	k6eAd1	skutečně
jednou	jeden	k4xCgFnSc7	jeden
nebo	nebo	k8xC	nebo
dvakrát	dvakrát	k6eAd1	dvakrát
jako	jako	k9	jako
příloha	příloha	k1gFnSc1	příloha
Mladé	mladý	k2eAgFnSc2d1	mladá
fronty	fronta	k1gFnSc2	fronta
vyšla	vyjít	k5eAaPmAgFnS	vyjít
<g/>
.	.	kIx.	.
18	[number]	k4	18
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1990	[number]	k4	1990
předsednictvo	předsednictvo	k1gNnSc1	předsednictvo
Federálního	federální	k2eAgNnSc2d1	federální
shromáždění	shromáždění	k1gNnSc2	shromáždění
ČSFR	ČSFR	kA	ČSFR
přijalo	přijmout	k5eAaPmAgNnS	přijmout
zákonné	zákonný	k2eAgNnSc1d1	zákonné
opatření	opatření	k1gNnSc1	opatření
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
něhož	jenž	k3xRgNnSc2	jenž
byla	být	k5eAaImAgFnS	být
zakázána	zakázán	k2eAgFnSc1d1	zakázána
manipulace	manipulace	k1gFnSc1	manipulace
s	s	k7c7	s
majetkem	majetek	k1gInSc7	majetek
KSČ	KSČ	kA	KSČ
a	a	k8xC	a
SSM	SSM	kA	SSM
<g/>
.	.	kIx.	.
</s>
<s>
Ústavní	ústavní	k2eAgInSc1d1	ústavní
zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
497	[number]	k4	497
<g/>
/	/	kIx~	/
<g/>
1990	[number]	k4	1990
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c4	o
navrácení	navrácení	k1gNnSc4	navrácení
majetku	majetek	k1gInSc2	majetek
Socialistického	socialistický	k2eAgInSc2d1	socialistický
svazu	svaz	k1gInSc2	svaz
mládeže	mládež	k1gFnSc2	mládež
lidu	lid	k1gInSc2	lid
České	český	k2eAgFnSc2d1	Česká
a	a	k8xC	a
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
Federativní	federativní	k2eAgFnSc2d1	federativní
Republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
s	s	k7c7	s
účinností	účinnost	k1gFnSc7	účinnost
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1991	[number]	k4	1991
veškeré	veškerý	k3xTgNnSc1	veškerý
vlastnictví	vlastnictví	k1gNnSc1	vlastnictví
SSM	SSM	kA	SSM
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
deníku	deník	k1gInSc2	deník
Mladé	mladý	k2eAgFnSc2d1	mladá
Fronty	fronta	k1gFnSc2	fronta
<g/>
,	,	kIx,	,
převedl	převést	k5eAaPmAgMnS	převést
do	do	k7c2	do
správy	správa	k1gFnSc2	správa
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Stát	stát	k1gInSc1	stát
měl	mít	k5eAaImAgInS	mít
dohlédnout	dohlédnout	k5eAaPmF	dohlédnout
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
majetek	majetek	k1gInSc4	majetek
SSM	SSM	kA	SSM
byl	být	k5eAaImAgMnS	být
navrácen	navrácen	k2eAgMnSc1d1	navrácen
mládežnickým	mládežnický	k2eAgFnPc3d1	mládežnická
organizacím	organizace	k1gFnPc3	organizace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
1993	[number]	k4	1993
byl	být	k5eAaImAgInS	být
Poslaneckou	poslanecký	k2eAgFnSc7d1	Poslanecká
sněmovnou	sněmovna	k1gFnSc7	sněmovna
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
využívání	využívání	k1gNnSc1	využívání
majetku	majetek	k1gInSc2	majetek
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
mládeže	mládež	k1gFnSc2	mládež
založen	založit	k5eAaPmNgInS	založit
Fond	fond	k1gInSc1	fond
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
mládeže	mládež	k1gFnSc2	mládež
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
bývalého	bývalý	k2eAgInSc2d1	bývalý
majetku	majetek	k1gInSc2	majetek
SSM	SSM	kA	SSM
však	však	k9	však
tomuto	tento	k3xDgInSc3	tento
fondu	fond	k1gInSc3	fond
dlouho	dlouho	k6eAd1	dlouho
nebyla	být	k5eNaImAgFnS	být
předána	předat	k5eAaPmNgFnS	předat
<g/>
.	.	kIx.	.
</s>
<s>
Činnost	činnost	k1gFnSc1	činnost
fondu	fond	k1gInSc2	fond
vzbuzovala	vzbuzovat	k5eAaImAgFnS	vzbuzovat
pochybnosti	pochybnost	k1gFnPc4	pochybnost
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
několikrát	několikrát	k6eAd1	několikrát
prošetřována	prošetřovat	k5eAaImNgFnS	prošetřovat
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
kontrolním	kontrolní	k2eAgInSc7d1	kontrolní
úřadem	úřad	k1gInSc7	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
po	po	k7c6	po
přijetí	přijetí	k1gNnSc6	přijetí
novelizujícího	novelizující	k2eAgInSc2d1	novelizující
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
307	[number]	k4	307
<g/>
/	/	kIx~	/
<g/>
1997	[number]	k4	1997
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
majetek	majetek	k1gInSc1	majetek
SSM	SSM	kA	SSM
vypořádán	vypořádat	k5eAaPmNgInS	vypořádat
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
27	[number]	k4	27
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1990	[number]	k4	1990
byla	být	k5eAaImAgFnS	být
zřízena	zřízen	k2eAgFnSc1d1	zřízena
společnost	společnost	k1gFnSc1	společnost
M	M	kA	M
a	a	k8xC	a
F	F	kA	F
a.s.	a.s.	k?	a.s.
se	s	k7c7	s
základním	základní	k2eAgNnSc7d1	základní
jměním	jmění	k1gNnSc7	jmění
195	[number]	k4	195
000	[number]	k4	000
Kčs	Kčs	kA	Kčs
<g/>
.	.	kIx.	.
</s>
<s>
M	M	kA	M
a	a	k8xC	a
F	F	kA	F
založilo	založit	k5eAaPmAgNnS	založit
64	[number]	k4	64
píšících	píšící	k2eAgMnPc2d1	píšící
redaktorů	redaktor	k1gMnPc2	redaktor
a	a	k8xC	a
jedna	jeden	k4xCgFnSc1	jeden
sekretářka	sekretářka	k1gFnSc1	sekretářka
redakce	redakce	k1gFnSc2	redakce
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
každý	každý	k3xTgMnSc1	každý
vložil	vložit	k5eAaPmAgInS	vložit
jmění	jmění	k1gNnSc4	jmění
3	[number]	k4	3
000	[number]	k4	000
Kčs	Kčs	kA	Kčs
(	(	kIx(	(
<g/>
to	ten	k3xDgNnSc1	ten
tehdy	tehdy	k6eAd1	tehdy
odpovídalo	odpovídat	k5eAaImAgNnS	odpovídat
průměrnému	průměrný	k2eAgInSc3d1	průměrný
měsíčnímu	měsíční	k2eAgInSc3d1	měsíční
platu	plat	k1gInSc3	plat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vlastnické	vlastnický	k2eAgInPc1d1	vlastnický
podíly	podíl	k1gInPc1	podíl
místo	místo	k7c2	místo
honoráře	honorář	k1gInSc2	honorář
dostali	dostat	k5eAaPmAgMnP	dostat
i	i	k9	i
právníci	právník	k1gMnPc1	právník
z	z	k7c2	z
poradních	poradní	k2eAgFnPc2d1	poradní
firem	firma	k1gFnPc2	firma
<g/>
.	.	kIx.	.
</s>
<s>
Akcionáři	akcionář	k1gMnPc1	akcionář
zažádali	zažádat	k5eAaPmAgMnP	zažádat
o	o	k7c4	o
zápis	zápis	k1gInSc4	zápis
společnosti	společnost	k1gFnSc2	společnost
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
2	[number]	k4	2
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1990	[number]	k4	1990
oznámili	oznámit	k5eAaPmAgMnP	oznámit
ředitelce	ředitelka	k1gFnSc6	ředitelka
vydavatelství	vydavatelství	k1gNnSc2	vydavatelství
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
Koškové	Koškové	k2eAgFnSc1d1	Koškové
<g/>
,	,	kIx,	,
že	že	k8xS	že
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
začnou	začít	k5eAaPmIp3nP	začít
vydávat	vydávat	k5eAaImF	vydávat
nový	nový	k2eAgInSc4d1	nový
deník	deník	k1gInSc4	deník
a	a	k8xC	a
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
nový	nový	k2eAgInSc4d1	nový
týdeník	týdeník	k1gInSc4	týdeník
s	s	k7c7	s
odlišnými	odlišný	k2eAgInPc7d1	odlišný
názvy	název	k1gInPc7	název
<g/>
,	,	kIx,	,
že	že	k8xS	že
všichni	všechen	k3xTgMnPc1	všechen
pracovníci	pracovník	k1gMnPc1	pracovník
požádají	požádat	k5eAaPmIp3nP	požádat
o	o	k7c4	o
rozvázaní	rozvázaný	k2eAgMnPc1d1	rozvázaný
pracovního	pracovní	k2eAgInSc2d1	pracovní
poměru	poměr	k1gInSc6	poměr
dohodou	dohoda	k1gFnSc7	dohoda
a	a	k8xC	a
že	že	k8xS	že
nová	nový	k2eAgFnSc1d1	nová
společnost	společnost	k1gFnSc1	společnost
má	mít	k5eAaImIp3nS	mít
zájem	zájem	k1gInSc4	zájem
využívat	využívat	k5eAaImF	využívat
všechny	všechen	k3xTgFnPc4	všechen
služby	služba	k1gFnPc4	služba
stávajícího	stávající	k2eAgNnSc2d1	stávající
nakladatelství	nakladatelství	k1gNnSc2	nakladatelství
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
postup	postup	k1gInSc1	postup
byl	být	k5eAaImAgInS	být
nutný	nutný	k2eAgMnSc1d1	nutný
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
převedení	převedení	k1gNnSc1	převedení
stávajících	stávající	k2eAgFnPc2d1	stávající
tiskovin	tiskovina	k1gFnPc2	tiskovina
SSM	SSM	kA	SSM
do	do	k7c2	do
soukromého	soukromý	k2eAgNnSc2d1	soukromé
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
bylo	být	k5eAaImAgNnS	být
blokované	blokovaný	k2eAgFnSc2d1	blokovaná
zákonem	zákon	k1gInSc7	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Distributorovi	distributorův	k2eAgMnPc1d1	distributorův
<g/>
,	,	kIx,	,
Poštovní	poštovní	k2eAgFnSc3d1	poštovní
novinové	novinový	k2eAgFnSc3d1	novinová
službě	služba	k1gFnSc3	služba
(	(	kIx(	(
<g/>
PNS	PNS	kA	PNS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
redaktoři	redaktor	k1gMnPc1	redaktor
oznámili	oznámit	k5eAaPmAgMnP	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
přestane	přestat	k5eAaPmIp3nS	přestat
vycházet	vycházet	k5eAaImF	vycházet
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
a	a	k8xC	a
že	že	k8xS	že
mohou	moct	k5eAaImIp3nP	moct
předplatitelům	předplatitel	k1gMnPc3	předplatitel
nabídnout	nabídnout	k5eAaPmF	nabídnout
nové	nový	k2eAgFnPc4d1	nová
noviny	novina	k1gFnPc4	novina
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
Dnes	dnes	k6eAd1	dnes
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
probíhala	probíhat	k5eAaImAgFnS	probíhat
i	i	k8xC	i
jednání	jednání	k1gNnPc1	jednání
s	s	k7c7	s
tiskárnami	tiskárna	k1gFnPc7	tiskárna
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
si	se	k3xPyFc3	se
privatizátoři	privatizátor	k1gMnPc1	privatizátor
zajistili	zajistit	k5eAaPmAgMnP	zajistit
právní	právní	k2eAgFnSc4d1	právní
ochranu	ochrana	k1gFnSc4	ochrana
názvů	název	k1gInPc2	název
a	a	k8xC	a
značek	značka	k1gFnPc2	značka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
týdnech	týden	k1gInPc6	týden
však	však	k9	však
nebylo	být	k5eNaImAgNnS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
Svazu	svaz	k1gInSc2	svaz
mládeže	mládež	k1gFnSc2	mládež
nepodaří	podařit	k5eNaPmIp3nS	podařit
sehnat	sehnat	k5eAaPmF	sehnat
novou	nový	k2eAgFnSc4d1	nová
redakci	redakce	k1gFnSc4	redakce
a	a	k8xC	a
ve	v	k7c6	v
vydávání	vydávání	k1gNnSc6	vydávání
původních	původní	k2eAgFnPc2d1	původní
novin	novina	k1gFnPc2	novina
a	a	k8xC	a
časopisu	časopis	k1gInSc2	časopis
pokračovat	pokračovat	k5eAaImF	pokračovat
<g/>
.	.	kIx.	.
</s>
<s>
Předseda	předseda	k1gMnSc1	předseda
SM	SM	kA	SM
Martin	Martin	k1gMnSc1	Martin
Ulčák	Ulčák	k1gMnSc1	Ulčák
nabízel	nabízet	k5eAaImAgMnS	nabízet
až	až	k6eAd1	až
trojnásobné	trojnásobný	k2eAgInPc4d1	trojnásobný
platy	plat	k1gInPc4	plat
oproti	oproti	k7c3	oproti
obvyklým	obvyklý	k2eAgFnPc3d1	obvyklá
<g/>
.	.	kIx.	.
</s>
<s>
Šéfredaktoři	šéfredaktor	k1gMnPc1	šéfredaktor
Mladé	mladý	k2eAgFnSc2d1	mladá
fronty	fronta	k1gFnSc2	fronta
a	a	k8xC	a
Mladého	mladý	k2eAgInSc2d1	mladý
světa	svět	k1gInSc2	svět
byli	být	k5eAaImAgMnP	být
oprávněni	oprávněn	k2eAgMnPc1d1	oprávněn
přijímat	přijímat	k5eAaImF	přijímat
výpovědi	výpověď	k1gFnPc4	výpověď
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
srpna	srpen	k1gInSc2	srpen
1990	[number]	k4	1990
uzavřeli	uzavřít	k5eAaPmAgMnP	uzavřít
dohodu	dohoda	k1gFnSc4	dohoda
o	o	k7c4	o
rozvázání	rozvázání	k1gNnSc4	rozvázání
pracovního	pracovní	k2eAgInSc2d1	pracovní
poměru	poměr	k1gInSc2	poměr
se	s	k7c7	s
všemi	všecek	k3xTgMnPc7	všecek
podřízenými	podřízený	k2eAgMnPc7d1	podřízený
redaktory	redaktor	k1gMnPc7	redaktor
a	a	k8xC	a
sami	sám	k3xTgMnPc1	sám
podali	podat	k5eAaPmAgMnP	podat
výpovědi	výpověď	k1gFnSc3	výpověď
paní	paní	k1gFnSc3	paní
Koškové	Košková	k1gFnSc3	Košková
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
nakonec	nakonec	k6eAd1	nakonec
přijala	přijmout	k5eAaPmAgFnS	přijmout
<g/>
.	.	kIx.	.
22	[number]	k4	22
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1990	[number]	k4	1990
byla	být	k5eAaImAgFnS	být
společnost	společnost	k1gFnSc1	společnost
M	M	kA	M
a	a	k8xC	a
F	F	kA	F
a.s.	a.s.	k?	a.s.
zapsána	zapsat	k5eAaPmNgFnS	zapsat
do	do	k7c2	do
obchodního	obchodní	k2eAgInSc2d1	obchodní
rejstříku	rejstřík	k1gInSc2	rejstřík
a	a	k8xC	a
tentýž	týž	k3xTgInSc1	týž
den	den	k1gInSc1	den
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
kultury	kultura	k1gFnSc2	kultura
zaregistrovalo	zaregistrovat	k5eAaPmAgNnS	zaregistrovat
nový	nový	k2eAgInSc4d1	nový
deník	deník	k1gInSc4	deník
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
Dnes	dnes	k6eAd1	dnes
<g/>
.	.	kIx.	.
</s>
<s>
Jednání	jednání	k1gNnSc1	jednání
mezi	mezi	k7c7	mezi
SM	SM	kA	SM
<g/>
,	,	kIx,	,
nakladatelstvím	nakladatelství	k1gNnSc7	nakladatelství
a	a	k8xC	a
novou	nova	k1gFnSc7	nova
a.s.	a.s.	k?	a.s.
však	však	k9	však
byla	být	k5eAaImAgFnS	být
dramatická	dramatický	k2eAgFnSc1d1	dramatická
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
SM	SM	kA	SM
Ulčák	Ulčák	k1gMnSc1	Ulčák
naposledy	naposledy	k6eAd1	naposledy
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
předat	předat	k5eAaPmF	předat
nové	nový	k2eAgFnSc3d1	nová
redakci	redakce	k1gFnSc3	redakce
oprávnění	oprávnění	k1gNnSc2	oprávnění
k	k	k7c3	k
vydávání	vydávání	k1gNnSc3	vydávání
tiskovin	tiskovina	k1gFnPc2	tiskovina
pod	pod	k7c7	pod
původními	původní	k2eAgInPc7d1	původní
názvy	název	k1gInPc7	název
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
srpna	srpen	k1gInSc2	srpen
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
redaktoři	redaktor	k1gMnPc1	redaktor
obávali	obávat	k5eAaImAgMnP	obávat
násilného	násilný	k2eAgNnSc2d1	násilné
obsazení	obsazení	k1gNnSc2	obsazení
redakce	redakce	k1gFnSc2	redakce
Svazem	svaz	k1gInSc7	svaz
mládeže	mládež	k1gFnSc2	mládež
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
sjednali	sjednat	k5eAaPmAgMnP	sjednat
soukromě	soukromě	k6eAd1	soukromě
ochranu	ochrana	k1gFnSc4	ochrana
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
příslušníků	příslušník	k1gMnPc2	příslušník
Útvaru	útvar	k1gInSc2	útvar
rychlého	rychlý	k2eAgNnSc2d1	rychlé
nasazení	nasazení	k1gNnSc2	nasazení
(	(	kIx(	(
<g/>
URNA	urna	k1gFnSc1	urna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
v	v	k7c6	v
kritických	kritický	k2eAgInPc6d1	kritický
dnech	den	k1gInPc6	den
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
osobním	osobní	k2eAgNnSc6d1	osobní
volnu	volno	k1gNnSc6	volno
v	v	k7c6	v
redakci	redakce	k1gFnSc6	redakce
i	i	k9	i
přespávali	přespávat	k5eAaImAgMnP	přespávat
<g/>
.	.	kIx.	.
</s>
<s>
Ulčák	Ulčák	k1gInSc1	Ulčák
nakonec	nakonec	k6eAd1	nakonec
29	[number]	k4	29
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
s	s	k7c7	s
převodem	převod	k1gInSc7	převod
práv	právo	k1gNnPc2	právo
k	k	k7c3	k
vydávání	vydávání	k1gNnSc3	vydávání
souhlasil	souhlasit	k5eAaImAgMnS	souhlasit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
souhlas	souhlas	k1gInSc1	souhlas
ho	on	k3xPp3gNnSc4	on
donutilo	donutit	k5eAaPmAgNnS	donutit
zrušit	zrušit	k5eAaPmF	zrušit
zamítavé	zamítavý	k2eAgNnSc4d1	zamítavé
stanovisko	stanovisko	k1gNnSc4	stanovisko
Národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
mládeže	mládež	k1gFnSc2	mládež
a	a	k8xC	a
Majetko-právní	Majetkorávní	k2eAgFnSc2d1	Majetko-právní
unie	unie	k1gFnSc2	unie
ČSFR	ČSFR	kA	ČSFR
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
29	[number]	k4	29
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
ještě	ještě	k9	ještě
SM	SM	kA	SM
psalo	psát	k5eAaImAgNnS	psát
vydavatelství	vydavatelství	k1gNnSc1	vydavatelství
dopis	dopis	k1gInSc1	dopis
<g/>
,	,	kIx,	,
že	že	k8xS	že
redaktoři	redaktor	k1gMnPc1	redaktor
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
podali	podat	k5eAaPmAgMnP	podat
výpověď	výpověď	k1gFnSc4	výpověď
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
vrátit	vrátit	k5eAaPmF	vrátit
klíče	klíč	k1gInPc4	klíč
<g/>
.	.	kIx.	.
30	[number]	k4	30
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
se	se	k3xPyFc4	se
však	však	k9	však
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
řešit	řešit	k5eAaImF	řešit
vše	všechen	k3xTgNnSc4	všechen
smírnou	smírný	k2eAgFnSc7d1	smírná
cestou	cesta	k1gFnSc7	cesta
<g/>
.	.	kIx.	.
31	[number]	k4	31
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
jednání	jednání	k1gNnSc1	jednání
čtyř	čtyři	k4xCgFnPc2	čtyři
stran	strana	k1gFnPc2	strana
(	(	kIx(	(
<g/>
Svaz	svaz	k1gInSc1	svaz
mládeže	mládež	k1gFnSc2	mládež
<g/>
,	,	kIx,	,
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
nová	nový	k2eAgFnSc1d1	nová
akciová	akciový	k2eAgFnSc1d1	akciová
společnost	společnost	k1gFnSc1	společnost
a	a	k8xC	a
zástupci	zástupce	k1gMnPc1	zástupce
původní	původní	k2eAgFnSc2d1	původní
redakce	redakce	k1gFnSc2	redakce
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
u	u	k7c2	u
předsedy	předseda	k1gMnSc2	předseda
vlády	vláda	k1gFnSc2	vláda
Petra	Petr	k1gMnSc2	Petr
Pitharta	Pithart	k1gMnSc2	Pithart
<g/>
.	.	kIx.	.
</s>
<s>
Pithart	Pithart	k1gInSc1	Pithart
podpořil	podpořit	k5eAaPmAgInS	podpořit
privatizující	privatizující	k2eAgMnPc4d1	privatizující
redaktory	redaktor	k1gMnPc4	redaktor
a	a	k8xC	a
v	v	k7c4	v
16	[number]	k4	16
hodin	hodina	k1gFnPc2	hodina
večer	večer	k6eAd1	večer
před	před	k7c7	před
vydáním	vydání	k1gNnSc7	vydání
první	první	k4xOgFnSc2	první
Mladé	mladý	k2eAgFnSc2d1	mladá
fronty	fronta	k1gFnSc2	fronta
Dnes	dnes	k6eAd1	dnes
byla	být	k5eAaImAgFnS	být
podepsána	podepsat	k5eAaPmNgFnS	podepsat
oboustranně	oboustranně	k6eAd1	oboustranně
výhodná	výhodný	k2eAgFnSc1d1	výhodná
hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
smlouva	smlouva	k1gFnSc1	smlouva
mezi	mezi	k7c7	mezi
M	M	kA	M
a	a	k8xC	a
F	F	kA	F
a.s.	a.s.	k?	a.s.
a	a	k8xC	a
vydavatelstvím	vydavatelství	k1gNnSc7	vydavatelství
Mladá	mladá	k1gFnSc1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
názvu	název	k1gInSc2	název
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
Martin	Martin	k1gMnSc1	Martin
Komárek	Komárek	k1gMnSc1	Komárek
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
kritici	kritik	k1gMnPc1	kritik
této	tento	k3xDgFnSc2	tento
formy	forma	k1gFnSc2	forma
privatizace	privatizace	k1gFnSc2	privatizace
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
hodnoty	hodnota	k1gFnPc1	hodnota
<g/>
,	,	kIx,	,
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
již	již	k6eAd1	již
podle	podle	k7c2	podle
zákona	zákon	k1gInSc2	zákon
státní	státní	k2eAgFnSc2d1	státní
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
si	se	k3xPyFc3	se
kolektiv	kolektiv	k1gInSc1	kolektiv
redaktorů	redaktor	k1gMnPc2	redaktor
přisvojil	přisvojit	k5eAaPmAgInS	přisvojit
(	(	kIx(	(
<g/>
pozice	pozice	k1gFnSc1	pozice
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
<g/>
,	,	kIx,	,
seznam	seznam	k1gInSc1	seznam
předplatitelů	předplatitel	k1gMnPc2	předplatitel
<g/>
,	,	kIx,	,
renomé	renomé	k1gNnSc7	renomé
listu	list	k1gInSc2	list
<g/>
)	)	kIx)	)
mohly	moct	k5eAaImAgFnP	moct
mít	mít	k5eAaImF	mít
hodnotu	hodnota	k1gFnSc4	hodnota
až	až	k9	až
kolem	kolem	k7c2	kolem
2	[number]	k4	2
miliard	miliarda	k4xCgFnPc2	miliarda
Kčs	Kčs	kA	Kčs
a	a	k8xC	a
že	že	k8xS	že
toto	tento	k3xDgNnSc1	tento
"	"	kIx"	"
<g/>
vytunelování	vytunelování	k1gNnSc1	vytunelování
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
vzorem	vzor	k1gInSc7	vzor
i	i	k9	i
pro	pro	k7c4	pro
mnohé	mnohý	k2eAgInPc4d1	mnohý
další	další	k2eAgInPc4d1	další
podniky	podnik	k1gInPc4	podnik
<g/>
.	.	kIx.	.
</s>
<s>
Vlastníkem	vlastník	k1gMnSc7	vlastník
kmene	kmen	k1gInSc2	kmen
předplatitelů	předplatitel	k1gMnPc2	předplatitel
však	však	k9	však
původně	původně	k6eAd1	původně
byla	být	k5eAaImAgFnS	být
Poštovní	poštovní	k2eAgFnSc1d1	poštovní
novinová	novinový	k2eAgFnSc1d1	novinová
služba	služba	k1gFnSc1	služba
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
vydavatelství	vydavatelství	k1gNnPc1	vydavatelství
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
Hvížďala	Hvížďala	k1gFnSc1	Hvížďala
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
Zempliner	Zempliner	k1gMnSc1	Zempliner
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
Šabata	Šabata	k1gMnSc1	Šabata
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
Komárek	Komárek	k1gMnSc1	Komárek
<g/>
,	,	kIx,	,
Ondřej	Ondřej	k1gMnSc1	Ondřej
Neff	Neff	k1gMnSc1	Neff
<g/>
,	,	kIx,	,
Alexandr	Alexandr	k1gMnSc1	Alexandr
Ferra	Ferro	k1gNnSc2	Ferro
<g/>
,	,	kIx,	,
Marcela	Marcela	k1gFnSc1	Marcela
Pecháčková	Pecháčková	k1gFnSc1	Pecháčková
<g/>
,	,	kIx,	,
Libor	Libor	k1gMnSc1	Libor
Ševčík	Ševčík	k1gMnSc1	Ševčík
<g/>
,	,	kIx,	,
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Trapl	Trapl	k1gMnSc1	Trapl
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Beránek	Beránek	k1gMnSc1	Beránek
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Matějovský	Matějovský	k1gMnSc1	Matějovský
<g/>
,	,	kIx,	,
Bořek	Bořek	k1gMnSc1	Bořek
Hanuš	Hanuš	k1gMnSc1	Hanuš
<g/>
,	,	kIx,	,
Bohumil	Bohumil	k1gMnSc1	Bohumil
Židek	Židek	k1gMnSc1	Židek
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Pacner	Pacner	k1gMnSc1	Pacner
<g/>
,	,	kIx,	,
Tomáš	Tomáš	k1gMnSc1	Tomáš
Marek	Marek	k1gMnSc1	Marek
<g/>
,	,	kIx,	,
Viliam	Viliam	k1gMnSc1	Viliam
Buchert	Buchert	k1gMnSc1	Buchert
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Leschtina	Leschtina	k1gFnSc1	Leschtina
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Čech	Čech	k1gMnSc1	Čech
<g/>
,	,	kIx,	,
Jindřich	Jindřich	k1gMnSc1	Jindřich
Štěpánek	Štěpánek	k1gMnSc1	Štěpánek
Do	do	k7c2	do
října	říjen	k1gInSc2	říjen
1992	[number]	k4	1992
bylo	být	k5eAaImAgNnS	být
základní	základní	k2eAgNnSc1d1	základní
jmění	jmění	k1gNnSc1	jmění
M	M	kA	M
a	a	k8xC	a
F	F	kA	F
a.s.	a.s.	k?	a.s.
navýšeno	navýšit	k5eAaPmNgNnS	navýšit
přibližně	přibližně	k6eAd1	přibližně
na	na	k7c4	na
padesátinásobek	padesátinásobek	k1gInSc4	padesátinásobek
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
11	[number]	k4	11
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1991	[number]	k4	1991
se	se	k3xPyFc4	se
spojilo	spojit	k5eAaPmAgNnS	spojit
vydavatelství	vydavatelství	k1gNnSc1	vydavatelství
M	M	kA	M
a	a	k8xC	a
F	F	kA	F
a.s.	a.s.	k?	a.s.
se	se	k3xPyFc4	se
Socpresse	Socpresse	k1gFnSc1	Socpresse
<g/>
,	,	kIx,	,
patřící	patřící	k2eAgFnSc1d1	patřící
do	do	k7c2	do
francouzské	francouzský	k2eAgFnSc2d1	francouzská
tiskové	tiskový	k2eAgFnSc2d1	tisková
skupiny	skupina	k1gFnSc2	skupina
Hersant	Hersanta	k1gFnPc2	Hersanta
<g/>
,	,	kIx,	,
a	a	k8xC	a
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
nová	nový	k2eAgFnSc1d1	nová
a.s.	a.s.	k?	a.s.
MaFra	MaFra	k1gFnSc1	MaFra
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
M	M	kA	M
a	a	k8xC	a
F	F	kA	F
vlastnil	vlastnit	k5eAaImAgInS	vlastnit
52	[number]	k4	52
%	%	kIx~	%
akcií	akcie	k1gFnPc2	akcie
a	a	k8xC	a
Socpresse	Socpresse	k1gFnSc1	Socpresse
48	[number]	k4	48
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
Francouzi	Francouz	k1gMnPc1	Francouz
zvýšili	zvýšit	k5eAaPmAgMnP	zvýšit
svůj	svůj	k3xOyFgInSc4	svůj
podíl	podíl	k1gInSc4	podíl
na	na	k7c4	na
52	[number]	k4	52
%	%	kIx~	%
akcií	akcie	k1gFnPc2	akcie
a	a	k8xC	a
ten	ten	k3xDgInSc4	ten
prodali	prodat	k5eAaPmAgMnP	prodat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
německému	německý	k2eAgInSc3d1	německý
koncernu	koncern	k1gInSc2	koncern
Rheinisch-Bergische	Rheinisch-Bergische	k1gFnPc2	Rheinisch-Bergische
Druckerei-	Druckerei-	k1gMnSc1	Druckerei-
und	und	k?	und
Verlagsgesellschaft	Verlagsgesellschaft	k1gMnSc1	Verlagsgesellschaft
(	(	kIx(	(
<g/>
RBVG	RBVG	kA	RBVG
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
koupil	koupit	k5eAaPmAgInS	koupit
i	i	k9	i
menšinové	menšinový	k2eAgInPc4d1	menšinový
podíly	podíl	k1gInPc4	podíl
českých	český	k2eAgMnPc2d1	český
majitelů	majitel	k1gMnPc2	majitel
<g/>
.	.	kIx.	.
</s>
<s>
RBVG	RBVG	kA	RBVG
byla	být	k5eAaImAgFnS	být
vlastníkem	vlastník	k1gMnSc7	vlastník
společnosti	společnost	k1gFnSc2	společnost
MaFra	MaFr	k1gInSc2	MaFr
až	až	k6eAd1	až
do	do	k7c2	do
26	[number]	k4	26
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ji	on	k3xPp3gFnSc4	on
(	(	kIx(	(
<g/>
stejně	stejně	k9	stejně
jako	jako	k9	jako
konkurenční	konkurenční	k2eAgNnSc1d1	konkurenční
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stejným	stejný	k2eAgMnSc7d1	stejný
vlastníkem	vlastník	k1gMnSc7	vlastník
vlastněné	vlastněný	k2eAgFnSc2d1	vlastněná
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
)	)	kIx)	)
koupil	koupit	k5eAaPmAgMnS	koupit
majitel	majitel	k1gMnSc1	majitel
společnosti	společnost	k1gFnSc2	společnost
Agrofert	Agrofert	k1gMnSc1	Agrofert
a	a	k8xC	a
z	z	k7c2	z
tehdejšího	tehdejší	k2eAgInSc2d1	tehdejší
pohledu	pohled	k1gInSc2	pohled
potenciální	potenciální	k2eAgMnSc1d1	potenciální
politik	politik	k1gMnSc1	politik
Andrej	Andrej	k1gMnSc1	Andrej
Babiš	Babiš	k1gMnSc1	Babiš
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
toho	ten	k3xDgNnSc2	ten
podalo	podat	k5eAaPmAgNnS	podat
několik	několik	k4yIc1	několik
pracovníků	pracovník	k1gMnPc2	pracovník
obou	dva	k4xCgInPc2	dva
deníků	deník	k1gInPc2	deník
výpověď	výpověď	k1gFnSc4	výpověď
(	(	kIx(	(
<g/>
z	z	k7c2	z
MF	MF	kA	MF
například	například	k6eAd1	například
její	její	k3xOp3gNnSc4	její
šéfredaktor	šéfredaktor	k1gMnSc1	šéfredaktor
Robert	Robert	k1gMnSc1	Robert
Čásenský	Čásenský	k2eAgMnSc1d1	Čásenský
nebo	nebo	k8xC	nebo
investigativní	investigativní	k2eAgMnSc1d1	investigativní
reportér	reportér	k1gMnSc1	reportér
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Kmenta	Kment	k1gMnSc2	Kment
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
Babiše	Babiš	k1gMnSc4	Babiš
označil	označit	k5eAaPmAgMnS	označit
jako	jako	k9	jako
kmotra	kmotr	k1gMnSc4	kmotr
<g/>
,	,	kIx,	,
z	z	k7c2	z
LN	LN	kA	LN
například	například	k6eAd1	například
jejich	jejich	k3xOp3gNnSc4	jejich
šéfredaktor	šéfredaktor	k1gMnSc1	šéfredaktor
Dalibor	Dalibor	k1gMnSc1	Dalibor
Balšínek	Balšínek	k1gMnSc1	Balšínek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1998	[number]	k4	1998
podal	podat	k5eAaPmAgMnS	podat
Ladislav	Ladislav	k1gMnSc1	Ladislav
Froněk	Froňka	k1gFnPc2	Froňka
<g/>
,	,	kIx,	,
vydavatel	vydavatel	k1gMnSc1	vydavatel
Deníku	deník	k1gInSc2	deník
Špígl	špígl	k1gInSc1	špígl
<g/>
,	,	kIx,	,
trestní	trestní	k2eAgNnSc4d1	trestní
oznámení	oznámení	k1gNnSc4	oznámení
Nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
státní	státní	k2eAgFnSc4d1	státní
zástupkyni	zástupkyně	k1gFnSc4	zástupkyně
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
návrh	návrh	k1gInSc4	návrh
zaslala	zaslat	k5eAaPmAgFnS	zaslat
Obvodnímu	obvodní	k2eAgNnSc3d1	obvodní
státnímu	státní	k2eAgNnSc3d1	státní
zastupitelství	zastupitelství	k1gNnSc3	zastupitelství
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
1	[number]	k4	1
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
spis	spis	k1gInSc4	spis
zaslalo	zaslat	k5eAaPmAgNnS	zaslat
policejnímu	policejní	k2eAgNnSc3d1	policejní
oddělení	oddělení	k1gNnSc3	oddělení
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Řešením	řešení	k1gNnSc7	řešení
případu	případ	k1gInSc2	případ
byla	být	k5eAaImAgFnS	být
pověřena	pověřen	k2eAgFnSc1d1	pověřena
začínající	začínající	k2eAgFnSc1d1	začínající
policistka	policistka	k1gFnSc1	policistka
místního	místní	k2eAgNnSc2d1	místní
oddělení	oddělení	k1gNnSc2	oddělení
v	v	k7c6	v
Hybernské	hybernský	k2eAgFnSc6d1	Hybernská
ulici	ulice	k1gFnSc6	ulice
<g/>
,	,	kIx,	,
pprap.	pprap.	k?	pprap.
Pavlína	Pavlína	k1gFnSc1	Pavlína
Vaňková	Vaňková	k1gFnSc1	Vaňková
<g/>
,	,	kIx,	,
policejní	policejní	k2eAgFnSc1d1	policejní
inspektorka	inspektorka	k1gFnSc1	inspektorka
<g/>
.	.	kIx.	.
</s>
<s>
Ladislavu	Ladislav	k1gMnSc3	Ladislav
Froňkovi	Froňek	k1gMnSc3	Froňek
byla	být	k5eAaImAgFnS	být
zaslána	zaslat	k5eAaPmNgFnS	zaslat
kopie	kopie	k1gFnSc1	kopie
usnesení	usnesení	k1gNnSc2	usnesení
o	o	k7c4	o
odložení	odložení	k1gNnSc4	odložení
oznámení	oznámení	k1gNnSc2	oznámení
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
podepsal	podepsat	k5eAaPmAgMnS	podepsat
kpt.	kpt.	k?	kpt.
Jan	Jan	k1gMnSc1	Jan
Pavlík	Pavlík	k1gMnSc1	Pavlík
<g/>
,	,	kIx,	,
vedoucí	vedoucí	k1gMnSc1	vedoucí
oddělení	oddělení	k1gNnSc2	oddělení
Policie	policie	k1gFnSc2	policie
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
MO	MO	kA	MO
Hybernská	hybernský	k2eAgFnSc1d1	Hybernská
ul	ul	kA	ul
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Froněk	Froněk	k1gInSc1	Froněk
se	se	k3xPyFc4	se
odvolal	odvolat	k5eAaPmAgInS	odvolat
a	a	k8xC	a
Obvodní	obvodní	k2eAgNnSc1d1	obvodní
státní	státní	k2eAgNnSc1d1	státní
zastupitelství	zastupitelství	k1gNnSc1	zastupitelství
pro	pro	k7c4	pro
Prahu	Praha	k1gFnSc4	Praha
1	[number]	k4	1
stížnost	stížnost	k1gFnSc1	stížnost
vyhodnotilo	vyhodnotit	k5eAaPmAgNnS	vyhodnotit
jako	jako	k9	jako
oprávněnou	oprávněný	k2eAgFnSc4d1	oprávněná
a	a	k8xC	a
6	[number]	k4	6
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1998	[number]	k4	1998
uložilo	uložit	k5eAaPmAgNnS	uložit
Policii	policie	k1gFnSc4	policie
případ	případ	k1gInSc4	případ
znovu	znovu	k6eAd1	znovu
otevřít	otevřít	k5eAaPmF	otevřít
<g/>
.	.	kIx.	.
</s>
<s>
JUDr.	JUDr.	kA	JUDr.
Gisela	Gisela	k1gFnSc1	Gisela
Vojtěchová	Vojtěchová	k1gFnSc1	Vojtěchová
<g/>
,	,	kIx,	,
státní	státní	k2eAgFnSc1d1	státní
zástupkyně	zástupkyně	k1gFnSc1	zástupkyně
Obvodního	obvodní	k2eAgNnSc2d1	obvodní
zastupitelství	zastupitelství	k1gNnSc2	zastupitelství
pro	pro	k7c4	pro
Prahu	Praha	k1gFnSc4	Praha
1	[number]	k4	1
<g/>
,	,	kIx,	,
došla	dojít	k5eAaPmAgFnS	dojít
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
celou	celý	k2eAgFnSc4d1	celá
záležitost	záležitost	k1gFnSc4	záležitost
nelze	lze	k6eNd1	lze
posoudit	posoudit	k5eAaPmF	posoudit
a	a	k8xC	a
učinit	učinit	k5eAaPmF	učinit
závěr	závěr	k1gInSc4	závěr
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
se	se	k3xPyFc4	se
nezajistí	zajistit	k5eNaPmIp3nS	zajistit
listinný	listinný	k2eAgInSc1d1	listinný
důkaz	důkaz	k1gInSc1	důkaz
<g/>
,	,	kIx,	,
kterým	který	k3yRgFnPc3	který
bylo	být	k5eAaImAgNnS	být
povoleno	povolit	k5eAaPmNgNnS	povolit
vynětí	vynětí	k1gNnSc1	vynětí
movitého	movitý	k2eAgInSc2d1	movitý
majetku	majetek	k1gInSc2	majetek
bývalého	bývalý	k2eAgMnSc2d1	bývalý
ÚV	ÚV	kA	ÚV
SSM	SSM	kA	SSM
z	z	k7c2	z
celku	celek	k1gInSc2	celek
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
převedení	převedení	k1gNnSc1	převedení
<g />
.	.	kIx.	.
</s>
<s>
na	na	k7c4	na
65	[number]	k4	65
redaktorů	redaktor	k1gMnPc2	redaktor
(	(	kIx(	(
<g/>
mělo	mít	k5eAaImAgNnS	mít
by	by	kYmCp3nP	by
to	ten	k3xDgNnSc1	ten
být	být	k5eAaImF	být
povolení	povolení	k1gNnSc4	povolení
Federálního	federální	k2eAgNnSc2d1	federální
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
financí	finance	k1gFnPc2	finance
<g/>
)	)	kIx)	)
a	a	k8xC	a
dokud	dokud	k6eAd1	dokud
se	se	k3xPyFc4	se
nezjistí	zjistit	k5eNaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
o	o	k7c6	o
oddělení	oddělení	k1gNnSc6	oddělení
Mladé	mladý	k2eAgFnSc2d1	mladá
Fronty	fronta	k1gFnSc2	fronta
do	do	k7c2	do
privátních	privátní	k2eAgFnPc2d1	privátní
rukou	ruka	k1gFnPc2	ruka
a	a	k8xC	a
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
a	a	k8xC	a
vydavatelství	vydavatelství	k1gNnSc1	vydavatelství
poskytne	poskytnout	k5eAaPmIp3nS	poskytnout
veškerou	veškerý	k3xTgFnSc4	veškerý
pomoc	pomoc	k1gFnSc4	pomoc
na	na	k7c6	na
základě	základ	k1gInSc6	základ
hospodářských	hospodářský	k2eAgFnPc2d1	hospodářská
smluv	smlouva	k1gFnPc2	smlouva
M	M	kA	M
a	a	k8xC	a
F	F	kA	F
a.s.	a.s.	k?	a.s.
Proti	proti	k7c3	proti
rozhodnutí	rozhodnutí	k1gNnSc3	rozhodnutí
nebyla	být	k5eNaImAgFnS	být
žádná	žádný	k3yNgFnSc1	žádný
stížnost	stížnost	k1gFnSc1	stížnost
přípustná	přípustný	k2eAgFnSc1d1	přípustná
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
vyšetřování	vyšetřování	k1gNnSc1	vyšetřování
skončilo	skončit	k5eAaPmAgNnS	skončit
<g/>
.	.	kIx.	.
</s>
<s>
Trestní	trestní	k2eAgNnSc1d1	trestní
oznámení	oznámení	k1gNnSc1	oznámení
bylo	být	k5eAaImAgNnS	být
podáno	podat	k5eAaPmNgNnS	podat
i	i	k9	i
na	na	k7c4	na
Marii	Maria	k1gFnSc4	Maria
Koškovou	Košková	k1gFnSc4	Košková
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
jednala	jednat	k5eAaImAgFnS	jednat
jménem	jméno	k1gNnSc7	jméno
vydavatelství	vydavatelství	k1gNnSc2	vydavatelství
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nedošlo	dojít	k5eNaPmAgNnS	dojít
k	k	k7c3	k
převodu	převod	k1gInSc3	převod
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
na	na	k7c4	na
M	M	kA	M
a	a	k8xC	a
F	F	kA	F
a.s.	a.s.	k?	a.s.
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jen	jen	k9	jen
k	k	k7c3	k
časově	časově	k6eAd1	časově
omezenému	omezený	k2eAgInSc3d1	omezený
pronájmu	pronájem	k1gInSc3	pronájem
<g/>
,	,	kIx,	,
k	k	k7c3	k
němuž	jenž	k3xRgInSc3	jenž
nebylo	být	k5eNaImAgNnS	být
nutné	nutný	k2eAgNnSc1d1	nutné
svolení	svolení	k1gNnSc1	svolení
Federálního	federální	k2eAgNnSc2d1	federální
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
financí	finance	k1gFnPc2	finance
<g/>
,	,	kIx,	,
případ	případ	k1gInSc1	případ
byl	být	k5eAaImAgInS	být
odložen	odložit	k5eAaPmNgInS	odložit
<g/>
.	.	kIx.	.
</s>
<s>
Kontrolní	kontrolní	k2eAgFnSc1d1	kontrolní
skupina	skupina	k1gFnSc1	skupina
Federálního	federální	k2eAgNnSc2d1	federální
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
kontroly	kontrola	k1gFnSc2	kontrola
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
a	a	k8xC	a
1992	[number]	k4	1992
a	a	k8xC	a
skupina	skupina	k1gFnSc1	skupina
finančního	finanční	k2eAgNnSc2d1	finanční
ředitelství	ředitelství	k1gNnSc2	ředitelství
Magistrátu	magistrát	k1gInSc2	magistrát
hl.	hl.	k?	hl.
m.	m.	k?	m.
Prahy	Praha	k1gFnSc2	Praha
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
se	se	k3xPyFc4	se
zabývaly	zabývat	k5eAaImAgFnP	zabývat
hospodářskými	hospodářský	k2eAgFnPc7d1	hospodářská
smlouvami	smlouva	k1gFnPc7	smlouva
mezi	mezi	k7c7	mezi
M	M	kA	M
a	a	k8xC	a
F	F	kA	F
a.s.	a.s.	k?	a.s.
a	a	k8xC	a
podnikem	podnik	k1gInSc7	podnik
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
.	.	kIx.	.
</s>
<s>
Závěrečné	závěrečný	k2eAgFnPc4d1	závěrečná
zprávy	zpráva	k1gFnPc4	zpráva
žádné	žádný	k3yNgNnSc4	žádný
porušení	porušení	k1gNnSc4	porušení
zákona	zákon	k1gInSc2	zákon
nekonstatovaly	konstatovat	k5eNaBmAgFnP	konstatovat
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc4d1	český
klub	klub	k1gInSc4	klub
skeptiků	skeptik	k1gMnPc2	skeptik
Sisyfos	Sisyfos	k1gMnSc1	Sisyfos
udělil	udělit	k5eAaPmAgMnS	udělit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
příloze	příloha	k1gFnSc6	příloha
deníku	deník	k1gInSc2	deník
MF	MF	kA	MF
Dnes	dnes	k6eAd1	dnes
Zdraví	zdraví	k1gNnSc1	zdraví
a	a	k8xC	a
životní	životní	k2eAgInSc1d1	životní
styl	styl	k1gInSc1	styl
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
deníkem	deník	k1gInSc7	deník
ROVNOST	rovnost	k1gFnSc4	rovnost
anticenu	anticen	k2eAgFnSc4d1	anticena
zlatý	zlatý	k2eAgInSc4d1	zlatý
Bludný	bludný	k2eAgInSc4d1	bludný
balvan	balvan	k1gInSc4	balvan
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
družstev	družstvo	k1gNnPc2	družstvo
za	za	k7c4	za
"	"	kIx"	"
<g/>
nekritickou	kritický	k2eNgFnSc4d1	nekritická
propagaci	propagace	k1gFnSc4	propagace
alternativní	alternativní	k2eAgFnSc2d1	alternativní
medicíny	medicína	k1gFnSc2	medicína
a	a	k8xC	a
za	za	k7c4	za
šíření	šíření	k1gNnSc4	šíření
neověřených	ověřený	k2eNgFnPc2d1	neověřená
zpráv	zpráva	k1gFnPc2	zpráva
a	a	k8xC	a
dezinformací	dezinformace	k1gFnPc2	dezinformace
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Zbyněk	Zbyněk	k1gMnSc1	Zbyněk
Zeman	Zeman	k1gMnSc1	Zeman
(	(	kIx(	(
<g/>
oxfordský	oxfordský	k2eAgMnSc1d1	oxfordský
historik	historik	k1gMnSc1	historik
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Vznik	vznik	k1gInSc1	vznik
Mladé	mladý	k2eAgFnSc2d1	mladá
fronty	fronta	k1gFnSc2	fronta
Dnes	dnes	k6eAd1	dnes
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
iDNES	iDNES	k?	iDNES
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
DNES	dnes	k6eAd1	dnes
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
DNES	dnes	k6eAd1	dnes
na	na	k7c6	na
Facebooku	Facebook	k1gInSc6	Facebook
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
DNES	dnes	k6eAd1	dnes
na	na	k7c6	na
Twitteru	Twitter	k1gInSc6	Twitter
<g />
.	.	kIx.	.
</s>
<s>
Titulní	titulní	k2eAgFnSc1d1	titulní
strana	strana	k1gFnSc1	strana
novin	novina	k1gFnPc2	novina
Marta	Marta	k1gFnSc1	Marta
Vildová	Vildová	k1gFnSc1	Vildová
<g/>
:	:	kIx,	:
O	o	k7c4	o
privatizaci	privatizace	k1gFnSc4	privatizace
deníku	deník	k1gInSc2	deník
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
(	(	kIx(	(
<g/>
bakalářská	bakalářský	k2eAgFnSc1d1	Bakalářská
práce	práce	k1gFnSc1	práce
<g/>
,	,	kIx,	,
Fakulta	fakulta	k1gFnSc1	fakulta
humanitních	humanitní	k2eAgFnPc2d1	humanitní
studií	studie	k1gFnPc2	studie
UK	UK	kA	UK
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
květen	květen	k1gInSc4	květen
2005	[number]	k4	2005
<g/>
;	;	kIx,	;
Britské	britský	k2eAgInPc1d1	britský
listy	list	k1gInPc1	list
<g/>
,	,	kIx,	,
16.6	[number]	k4	16.6
<g/>
.2005	.2005	k4	.2005
<g/>
)	)	kIx)	)
Jak	jak	k8xC	jak
se	se	k3xPyFc4	se
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
změnila	změnit	k5eAaPmAgFnS	změnit
po	po	k7c6	po
Babišově	Babišův	k2eAgInSc6d1	Babišův
vstupu	vstup	k1gInSc6	vstup
do	do	k7c2	do
Mafry	Mafra	k1gFnSc2	Mafra
-	-	kIx~	-
článek	článek	k1gInSc1	článek
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Přítomnost	přítomnost	k1gFnSc1	přítomnost
</s>
