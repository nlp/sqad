<p>
<s>
Panda	panda	k1gFnSc1	panda
červená	červená	k1gFnSc1	červená
(	(	kIx(	(
<g/>
Ailurus	Ailurus	k1gInSc1	Ailurus
fulgens	fulgens	k1gInSc1	fulgens
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
též	též	k9	též
panda	panda	k1gFnSc1	panda
malá	malý	k2eAgFnSc1d1	malá
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
menší	malý	k2eAgFnSc1d2	menší
stromová	stromový	k2eAgFnSc1d1	stromová
šelma	šelma	k1gFnSc1	šelma
<g/>
,	,	kIx,	,
jediný	jediný	k2eAgMnSc1d1	jediný
zástupce	zástupce	k1gMnSc1	zástupce
monotypické	monotypický	k2eAgFnSc2d1	monotypický
čeledi	čeleď	k1gFnSc2	čeleď
pandy	panda	k1gFnSc2	panda
malé	malý	k2eAgFnSc2d1	malá
(	(	kIx(	(
<g/>
Ailuridae	Ailurida	k1gFnSc2	Ailurida
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pandě	panda	k1gFnSc3	panda
velké	velká	k1gFnSc3	velká
není	být	k5eNaImIp3nS	být
panda	panda	k1gFnSc1	panda
červená	červený	k2eAgFnSc1d1	červená
blízce	blízce	k6eAd1	blízce
příbuzná	příbuzný	k2eAgFnSc1d1	příbuzná
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
pouze	pouze	k6eAd1	pouze
podobný	podobný	k2eAgInSc1d1	podobný
způsob	způsob	k1gInSc1	způsob
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jsou	být	k5eAaImIp3nP	být
uznávany	uznávany	k?	uznávany
dva	dva	k4xCgInPc4	dva
poddruhy	poddruh	k1gInPc4	poddruh
<g/>
.	.	kIx.	.
<g/>
Tato	tento	k3xDgFnSc1	tento
panda	panda	k1gFnSc1	panda
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
něco	něco	k3yInSc4	něco
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
kočka	kočka	k1gFnSc1	kočka
domácí	domácí	k2eAgFnSc1d1	domácí
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
rudo-hnědou	rudonědý	k2eAgFnSc4d1	rudo-hnědý
srst	srst	k1gFnSc4	srst
a	a	k8xC	a
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
ocas	ocas	k1gInSc4	ocas
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
pandám	panda	k1gFnPc3	panda
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
udržovat	udržovat	k5eAaImF	udržovat
rovnováhu	rovnováha	k1gFnSc4	rovnováha
na	na	k7c6	na
stromech	strom	k1gInPc6	strom
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
samcem	samec	k1gInSc7	samec
a	a	k8xC	a
samicí	samice	k1gFnSc7	samice
není	být	k5eNaImIp3nS	být
téměř	téměř	k6eAd1	téměř
žádný	žádný	k3yNgInSc1	žádný
rozdíl	rozdíl	k1gInSc1	rozdíl
<g/>
,	,	kIx,	,
mláďata	mládě	k1gNnPc1	mládě
se	se	k3xPyFc4	se
podobají	podobat	k5eAaImIp3nP	podobat
dospělým	dospělý	k2eAgMnPc3d1	dospělý
jedincům	jedinec	k1gMnPc3	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
panda	panda	k1gFnSc1	panda
červená	červený	k2eAgFnSc1d1	červená
průměrně	průměrně	k6eAd1	průměrně
dožívá	dožívat	k5eAaImIp3nS	dožívat
8	[number]	k4	8
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
<g/>
Obývá	obývat	k5eAaImIp3nS	obývat
horské	horský	k2eAgInPc4d1	horský
lesy	les	k1gInPc4	les
jižní	jižní	k2eAgFnSc2d1	jižní
Číny	Čína	k1gFnSc2	Čína
<g/>
,	,	kIx,	,
Nepálu	Nepál	k1gInSc2	Nepál
<g/>
,	,	kIx,	,
Bhútánu	Bhútán	k1gInSc2	Bhútán
<g/>
,	,	kIx,	,
Indie	Indie	k1gFnSc2	Indie
a	a	k8xC	a
Myanmaru	Myanmar	k1gInSc2	Myanmar
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejvýše	nejvýše	k6eAd1	nejvýše
žijící	žijící	k2eAgNnPc4d1	žijící
zvířata	zvíře	k1gNnPc4	zvíře
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
i	i	k9	i
ve	v	k7c6	v
více	hodně	k6eAd2	hodně
než	než	k8xS	než
4	[number]	k4	4
000	[number]	k4	000
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Její	její	k3xOp3gFnSc4	její
potravu	potrava	k1gFnSc4	potrava
tvoří	tvořit	k5eAaImIp3nS	tvořit
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
většiny	většina	k1gFnSc2	většina
bambus	bambus	k1gInSc4	bambus
<g/>
,	,	kIx,	,
jídelníček	jídelníček	k1gInSc4	jídelníček
si	se	k3xPyFc3	se
občas	občas	k6eAd1	občas
zpestří	zpestřet	k5eAaPmIp3nS	zpestřet
bobulemi	bobule	k1gFnPc7	bobule
<g/>
,	,	kIx,	,
vejci	vejce	k1gNnPc7	vejce
nebo	nebo	k8xC	nebo
menšími	malý	k2eAgNnPc7d2	menší
zvířaty	zvíře	k1gNnPc7	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
samotářské	samotářský	k2eAgNnSc4d1	samotářské
zvíře	zvíře	k1gNnSc4	zvíře
<g/>
,	,	kIx,	,
aktivní	aktivní	k2eAgInSc4d1	aktivní
hlavně	hlavně	k9	hlavně
za	za	k7c2	za
úsvitu	úsvit	k1gInSc2	úsvit
a	a	k8xC	a
soumraku	soumrak	k1gInSc2	soumrak
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
březosti	březost	k1gFnSc6	březost
trvající	trvající	k2eAgFnSc1d1	trvající
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
měsíce	měsíc	k1gInSc2	měsíc
samice	samice	k1gFnSc2	samice
rodí	rodit	k5eAaImIp3nS	rodit
obvykle	obvykle	k6eAd1	obvykle
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
mláďata	mládě	k1gNnPc4	mládě
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
jsou	být	k5eAaImIp3nP	být
při	při	k7c6	při
narození	narození	k1gNnSc6	narození
úplně	úplně	k6eAd1	úplně
bezmocná	bezmocný	k2eAgFnSc1d1	bezmocná
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
se	se	k3xPyFc4	se
o	o	k7c4	o
ně	on	k3xPp3gInPc4	on
stará	starat	k5eAaImIp3nS	starat
zhruba	zhruba	k6eAd1	zhruba
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
IUCN	IUCN	kA	IUCN
ji	on	k3xPp3gFnSc4	on
zařazuje	zařazovat	k5eAaImIp3nS	zařazovat
mezi	mezi	k7c4	mezi
ohrožené	ohrožený	k2eAgInPc4d1	ohrožený
druhy	druh	k1gInPc4	druh
<g/>
,	,	kIx,	,
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
žije	žít	k5eAaImIp3nS	žít
asi	asi	k9	asi
jen	jen	k9	jen
16	[number]	k4	16
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
–	–	k?	–
<g/>
20	[number]	k4	20
000	[number]	k4	000
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Ohrožuje	ohrožovat	k5eAaImIp3nS	ohrožovat
ji	on	k3xPp3gFnSc4	on
především	především	k9	především
ztráta	ztráta	k1gFnSc1	ztráta
přirozeného	přirozený	k2eAgNnSc2d1	přirozené
prostředí	prostředí	k1gNnSc2	prostředí
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
fragmentace	fragmentace	k1gFnSc2	fragmentace
<g/>
,	,	kIx,	,
také	také	k9	také
je	být	k5eAaImIp3nS	být
občas	občas	k6eAd1	občas
lovena	lovit	k5eAaImNgFnS	lovit
pro	pro	k7c4	pro
kožešinu	kožešina	k1gFnSc4	kožešina
<g/>
.	.	kIx.	.
</s>
<s>
Pandy	panda	k1gFnPc1	panda
červené	červená	k1gFnSc2	červená
jsou	být	k5eAaImIp3nP	být
s	s	k7c7	s
oblibou	obliba	k1gFnSc7	obliba
chovány	chovat	k5eAaImNgInP	chovat
v	v	k7c6	v
zoologických	zoologický	k2eAgFnPc6d1	zoologická
zahradách	zahrada	k1gFnPc6	zahrada
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
vzhled	vzhled	k1gInSc4	vzhled
a	a	k8xC	a
menší	malý	k2eAgFnSc1d2	menší
náročnost	náročnost	k1gFnSc1	náročnost
než	než	k8xS	než
panda	panda	k1gFnSc1	panda
velká	velký	k2eAgFnSc1d1	velká
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Etymologie	etymologie	k1gFnSc2	etymologie
==	==	k?	==
</s>
</p>
<p>
<s>
Její	její	k3xOp3gFnSc1	její
západní	západní	k2eAgFnSc1d1	západní
(	(	kIx(	(
<g/>
i	i	k9	i
české	český	k2eAgFnSc3d1	Česká
<g/>
)	)	kIx)	)
jméno	jméno	k1gNnSc1	jméno
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
zřejmě	zřejmě	k6eAd1	zřejmě
z	z	k7c2	z
francouzštinou	francouzština	k1gFnSc7	francouzština
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
jiného	jiný	k2eAgInSc2d1	jiný
zdroje	zdroj	k1gInSc2	zdroj
angličtinou	angličtina	k1gFnSc7	angličtina
<g/>
)	)	kIx)	)
adaptovaného	adaptovaný	k2eAgNnSc2d1	adaptované
nepálského	nepálský	k2eAgNnSc2d1	nepálské
slova	slovo	k1gNnSc2	slovo
poonya	poonyum	k1gNnSc2	poonyum
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
dalo	dát	k5eAaPmAgNnS	dát
přeložit	přeložit	k5eAaPmF	přeložit
jako	jako	k9	jako
"	"	kIx"	"
<g/>
jedlík	jedlík	k1gMnSc1	jedlík
bambusu	bambus	k1gInSc2	bambus
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
i	i	k8xC	i
dalších	další	k2eAgInPc6d1	další
jazycích	jazyk	k1gInPc6	jazyk
je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
také	také	k9	také
jako	jako	k8xC	jako
malá	malý	k2eAgFnSc1d1	malá
panda	panda	k1gFnSc1	panda
<g/>
,	,	kIx,	,
menší	malý	k2eAgFnSc1d2	menší
panda	panda	k1gFnSc1	panda
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
i	i	k9	i
ohnivá	ohnivý	k2eAgFnSc1d1	ohnivá
liška	liška	k1gFnSc1	liška
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
panda	panda	k1gFnSc1	panda
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
označován	označovat	k5eAaImNgInS	označovat
právě	právě	k9	právě
tento	tento	k3xDgInSc4	tento
druh	druh	k1gInSc4	druh
<g/>
,	,	kIx,	,
mnohem	mnohem	k6eAd1	mnohem
později	pozdě	k6eAd2	pozdě
bylo	být	k5eAaImAgNnS	být
toto	tento	k3xDgNnSc1	tento
jméno	jméno	k1gNnSc1	jméno
přeneseno	přenést	k5eAaPmNgNnS	přenést
na	na	k7c4	na
pandu	panda	k1gFnSc4	panda
velkou	velký	k2eAgFnSc4d1	velká
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Taxonomie	taxonomie	k1gFnSc2	taxonomie
==	==	k?	==
</s>
</p>
<p>
<s>
Taxonomické	taxonomický	k2eAgNnSc1d1	taxonomické
postavení	postavení	k1gNnSc1	postavení
pandy	panda	k1gFnSc2	panda
červené	červený	k2eAgFnSc2d1	červená
i	i	k8xC	i
pandy	panda	k1gFnSc2	panda
velké	velký	k2eAgFnSc2d1	velká
bylo	být	k5eAaImAgNnS	být
dlouho	dlouho	k6eAd1	dlouho
nejasné	jasný	k2eNgNnSc1d1	nejasné
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
obě	dva	k4xCgFnPc1	dva
pandy	panda	k1gFnPc1	panda
mají	mít	k5eAaImIp3nP	mít
vlastnosti	vlastnost	k1gFnPc4	vlastnost
medvídkovitých	medvídkovitý	k2eAgFnPc2d1	medvídkovitá
i	i	k8xC	i
medvědovitých	medvědovitý	k2eAgFnPc2d1	medvědovitá
šelem	šelma	k1gFnPc2	šelma
<g/>
,	,	kIx,	,
bývaly	bývat	k5eAaImAgFnP	bývat
společně	společně	k6eAd1	společně
řazeny	řadit	k5eAaImNgFnP	řadit
do	do	k7c2	do
některé	některý	k3yIgFnPc4	některý
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
čeledí	čeleď	k1gFnPc2	čeleď
nebo	nebo	k8xC	nebo
do	do	k7c2	do
samostatné	samostatný	k2eAgFnSc2d1	samostatná
čeledi	čeleď	k1gFnSc2	čeleď
Ailuridae	Ailurida	k1gFnSc2	Ailurida
<g/>
.	.	kIx.	.
</s>
<s>
Jednoznačnou	jednoznačný	k2eAgFnSc4d1	jednoznačná
odpověď	odpověď	k1gFnSc4	odpověď
ohledně	ohledně	k7c2	ohledně
taxonomického	taxonomický	k2eAgNnSc2d1	taxonomické
zařazení	zařazení	k1gNnSc2	zařazení
obou	dva	k4xCgFnPc2	dva
pand	panda	k1gFnPc2	panda
přinesly	přinést	k5eAaPmAgInP	přinést
až	až	k9	až
podrobné	podrobný	k2eAgInPc1d1	podrobný
fylogenetické	fylogenetický	k2eAgInPc1d1	fylogenetický
výzkumy	výzkum	k1gInPc1	výzkum
včetně	včetně	k7c2	včetně
analýzy	analýza	k1gFnSc2	analýza
DNA	DNA	kA	DNA
<g/>
.	.	kIx.	.
<g/>
Panda	panda	k1gFnSc1	panda
červená	červená	k1gFnSc1	červená
je	být	k5eAaImIp3nS	být
jediný	jediný	k2eAgInSc1d1	jediný
recentní	recentní	k2eAgInSc1d1	recentní
druh	druh	k1gInSc1	druh
čeledi	čeleď	k1gFnSc2	čeleď
pandy	panda	k1gFnSc2	panda
malé	malý	k2eAgFnSc2d1	malá
(	(	kIx(	(
<g/>
Ailuridae	Ailurida	k1gFnSc2	Ailurida
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
zástupci	zástupce	k1gMnPc1	zástupce
byli	být	k5eAaImAgMnP	být
kdysi	kdysi	k6eAd1	kdysi
rozšíření	rozšíření	k1gNnSc4	rozšíření
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
Eurasii	Eurasie	k1gFnSc6	Eurasie
i	i	k8xC	i
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
současně	současně	k6eAd1	současně
jediným	jediný	k2eAgMnSc7d1	jediný
recentním	recentní	k2eAgMnSc7d1	recentní
zástupcem	zástupce	k1gMnSc7	zástupce
rodu	rod	k1gInSc2	rod
Ailurus	Ailurus	k1gInSc1	Ailurus
<g/>
.	.	kIx.	.
</s>
<s>
Nejbližší	blízký	k2eAgFnPc1d3	nejbližší
recentní	recentní	k2eAgFnPc1d1	recentní
příbuzné	příbuzný	k2eAgFnPc1d1	příbuzná
pandy	panda	k1gFnPc1	panda
červené	červená	k1gFnSc2	červená
bychom	by	kYmCp1nP	by
našli	najít	k5eAaPmAgMnP	najít
mezi	mezi	k7c7	mezi
lasicovitými	lasicovitý	k2eAgFnPc7d1	lasicovitá
<g/>
,	,	kIx,	,
medvídkovitými	medvídkovitý	k2eAgFnPc7d1	medvídkovitá
a	a	k8xC	a
skunkovitými	skunkovitý	k2eAgFnPc7d1	skunkovitý
šelmami	šelma	k1gFnPc7	šelma
<g/>
,	,	kIx,	,
s	s	k7c7	s
nimiž	jenž	k3xRgMnPc7	jenž
čeleď	čeleď	k1gFnSc1	čeleď
pandy	panda	k1gFnSc2	panda
malé	malá	k1gFnSc2	malá
tvoří	tvořit	k5eAaImIp3nS	tvořit
společnou	společný	k2eAgFnSc4d1	společná
nadčeleď	nadčeleď	k1gFnSc4	nadčeleď
Musteloidea	Musteloide	k1gInSc2	Musteloide
<g/>
.	.	kIx.	.
</s>
<s>
Panda	panda	k1gFnSc1	panda
velká	velká	k1gFnSc1	velká
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
řazena	řadit	k5eAaImNgFnS	řadit
přímo	přímo	k6eAd1	přímo
mezi	mezi	k7c4	mezi
medvědovité	medvědovitý	k2eAgInPc4d1	medvědovitý
a	a	k8xC	a
pandě	panda	k1gFnSc6	panda
červené	červený	k2eAgNnSc1d1	červené
tedy	tedy	k8xC	tedy
není	být	k5eNaImIp3nS	být
blízce	blízce	k6eAd1	blízce
příbuzná	příbuzný	k2eAgFnSc1d1	příbuzná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Poddruhy	poddruh	k1gInPc4	poddruh
===	===	k?	===
</s>
</p>
<p>
<s>
Areál	areál	k1gInSc1	areál
rozšíření	rozšíření	k1gNnSc2	rozšíření
pandy	panda	k1gFnSc2	panda
červené	červená	k1gFnSc2	červená
není	být	k5eNaImIp3nS	být
souvislý	souvislý	k2eAgMnSc1d1	souvislý
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
do	do	k7c2	do
dvou	dva	k4xCgFnPc2	dva
částí	část	k1gFnPc2	část
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ailurus	Ailurus	k1gInSc1	Ailurus
fulgens	fulgens	k1gInSc1	fulgens
fulgens	fulgens	k1gInSc1	fulgens
(	(	kIx(	(
<g/>
Cuvier	Cuvier	k1gInSc1	Cuvier
<g/>
,	,	kIx,	,
1825	[number]	k4	1825
<g/>
)	)	kIx)	)
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
oblasti	oblast	k1gFnSc6	oblast
rozšíření	rozšíření	k1gNnSc2	rozšíření
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
Nepálu	Nepál	k1gInSc6	Nepál
<g/>
,	,	kIx,	,
Bhútánu	Bhútán	k1gInSc6	Bhútán
<g/>
,	,	kIx,	,
Ásámu	Ásám	k1gInSc6	Ásám
a	a	k8xC	a
Sikkimu	Sikkim	k1gInSc6	Sikkim
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ailurus	Ailurus	k1gInSc1	Ailurus
fulgens	fulgensa	k1gFnPc2	fulgensa
styani	styan	k1gMnPc1	styan
(	(	kIx(	(
<g/>
Thomas	Thomas	k1gMnSc1	Thomas
<g/>
,	,	kIx,	,
1902	[number]	k4	1902
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
též	též	k9	též
Ailurus	Ailurus	k1gInSc1	Ailurus
fulgens	fulgensa	k1gFnPc2	fulgensa
refulgens	refulgens	k1gInSc1	refulgens
(	(	kIx(	(
<g/>
Milne-Edwards	Milne-Edwards	k1gInSc1	Milne-Edwards
<g/>
,	,	kIx,	,
1874	[number]	k4	1874
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
oblasti	oblast	k1gFnSc6	oblast
rozšíření	rozšíření	k1gNnSc2	rozšíření
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
severním	severní	k2eAgInSc6d1	severní
Myanmaru	Myanmar	k1gInSc6	Myanmar
a	a	k8xC	a
jižní	jižní	k2eAgFnSc6d1	jižní
Číně	Čína	k1gFnSc6	Čína
<g/>
.	.	kIx.	.
<g/>
Přirozenou	přirozený	k2eAgFnSc4d1	přirozená
hranici	hranice	k1gFnSc4	hranice
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgInPc7	dva
poddruhy	poddruh	k1gInPc7	poddruh
tvoří	tvořit	k5eAaImIp3nS	tvořit
přibližně	přibližně	k6eAd1	přibližně
řeka	řeka	k1gFnSc1	řeka
Brahmaputra	Brahmaputra	k1gFnSc1	Brahmaputra
<g/>
.	.	kIx.	.
</s>
<s>
Poddruhy	poddruh	k1gInPc1	poddruh
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
dají	dát	k5eAaPmIp3nP	dát
rozlišit	rozlišit	k5eAaPmF	rozlišit
podle	podle	k7c2	podle
větší	veliký	k2eAgFnSc2d2	veliký
lebky	lebka	k1gFnSc2	lebka
<g/>
,	,	kIx,	,
robustnějších	robustní	k2eAgInPc2d2	robustnější
zubů	zub	k1gInPc2	zub
a	a	k8xC	a
delší	dlouhý	k2eAgFnSc2d2	delší
zimní	zimní	k2eAgFnSc2d1	zimní
srsti	srst	k1gFnSc2	srst
u	u	k7c2	u
poddruhu	poddruh	k1gInSc2	poddruh
A.	A.	kA	A.
f.	f.	k?	f.
styani	styaň	k1gFnSc6	styaň
<g/>
.	.	kIx.	.
</s>
<s>
Poddruh	poddruh	k1gInSc1	poddruh
A.	A.	kA	A.
f.	f.	k?	f.
styani	styaň	k1gFnSc6	styaň
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
tmavší	tmavý	k2eAgNnSc1d2	tmavší
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgInPc7	dva
poddruhy	poddruh	k1gInPc7	poddruh
existuje	existovat	k5eAaImIp3nS	existovat
velká	velký	k2eAgFnSc1d1	velká
variabilita	variabilita	k1gFnSc1	variabilita
ve	v	k7c6	v
zbarvení	zbarvení	k1gNnSc6	zbarvení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Délka	délka	k1gFnSc1	délka
těla	tělo	k1gNnSc2	tělo
pandy	panda	k1gFnSc2	panda
červené	červený	k2eAgFnSc2d1	červená
je	být	k5eAaImIp3nS	být
50	[number]	k4	50
až	až	k9	až
64	[number]	k4	64
cm	cm	kA	cm
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
ocasu	ocas	k1gInSc2	ocas
28	[number]	k4	28
až	až	k9	až
59	[number]	k4	59
cm	cm	kA	cm
a	a	k8xC	a
hmotnost	hmotnost	k1gFnSc1	hmotnost
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
3	[number]	k4	3
až	až	k9	až
6	[number]	k4	6
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
<g/>
,	,	kIx,	,
hustou	hustý	k2eAgFnSc4d1	hustá
srst	srst	k1gFnSc4	srst
<g/>
,	,	kIx,	,
svrchu	svrchu	k6eAd1	svrchu
rudohnědou	rudohnědý	k2eAgFnSc4d1	rudohnědá
nebo	nebo	k8xC	nebo
kaštanovou	kaštanový	k2eAgFnSc4d1	Kaštanová
<g/>
,	,	kIx,	,
na	na	k7c6	na
spodní	spodní	k2eAgFnSc6d1	spodní
části	část	k1gFnSc6	část
těla	tělo	k1gNnSc2	tělo
a	a	k8xC	a
na	na	k7c6	na
končetinách	končetina	k1gFnPc6	končetina
leskle	leskle	k6eAd1	leskle
černou	černá	k1gFnSc4	černá
<g/>
.	.	kIx.	.
</s>
<s>
Okraje	okraj	k1gInPc1	okraj
uší	ucho	k1gNnPc2	ucho
jsou	být	k5eAaImIp3nP	být
bělavé	bělavý	k2eAgInPc1d1	bělavý
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
obličejová	obličejový	k2eAgFnSc1d1	obličejová
část	část	k1gFnSc1	část
s	s	k7c7	s
hnědými	hnědý	k2eAgFnPc7d1	hnědá
obličejovými	obličejový	k2eAgFnPc7d1	obličejová
tvářemi	tvář	k1gFnPc7	tvář
a	a	k8xC	a
skvrnami	skvrna	k1gFnPc7	skvrna
pod	pod	k7c7	pod
očima	oko	k1gNnPc7	oko
<g/>
,	,	kIx,	,
zvanými	zvaný	k2eAgNnPc7d1	zvané
slzy	slza	k1gFnPc5	slza
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
kulatá	kulatý	k2eAgFnSc1d1	kulatá
hlava	hlava	k1gFnSc1	hlava
připomíná	připomínat	k5eAaImIp3nS	připomínat
kočičí	kočičí	k2eAgFnSc1d1	kočičí
<g/>
.	.	kIx.	.
</s>
<s>
Lebka	lebka	k1gFnSc1	lebka
je	být	k5eAaImIp3nS	být
mohutná	mohutný	k2eAgFnSc1d1	mohutná
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnPc1	její
uši	ucho	k1gNnPc1	ucho
jsou	být	k5eAaImIp3nP	být
středně	středně	k6eAd1	středně
velké	velký	k2eAgInPc1d1	velký
a	a	k8xC	a
vzpřímené	vzpřímený	k2eAgInPc1d1	vzpřímený
<g/>
,	,	kIx,	,
čenich	čenich	k1gInSc1	čenich
černý	černý	k2eAgInSc1d1	černý
a	a	k8xC	a
oči	oko	k1gNnPc4	oko
velmi	velmi	k6eAd1	velmi
tmavé	tmavý	k2eAgFnPc1d1	tmavá
<g/>
.	.	kIx.	.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
vzhled	vzhled	k1gInSc1	vzhled
pand	panda	k1gFnPc2	panda
doplňuje	doplňovat	k5eAaImIp3nS	doplňovat
ještě	ještě	k6eAd1	ještě
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
<g/>
,	,	kIx,	,
huňatý	huňatý	k2eAgInSc4d1	huňatý
a	a	k8xC	a
široký	široký	k2eAgInSc4d1	široký
ocas	ocas	k1gInSc4	ocas
se	s	k7c7	s
střídajícími	střídající	k2eAgFnPc7d1	střídající
se	se	k3xPyFc4	se
žlutými	žlutý	k2eAgFnPc7d1	žlutá
a	a	k8xC	a
červenými	červený	k2eAgInPc7d1	červený
okrovými	okrový	k2eAgInPc7d1	okrový
kroužky	kroužek	k1gInPc7	kroužek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ocas	ocas	k1gInSc1	ocas
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
pandám	panda	k1gFnPc3	panda
udržovat	udržovat	k5eAaImF	udržovat
rovnováhu	rovnováha	k1gFnSc4	rovnováha
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
vynikajícím	vynikající	k2eAgNnSc7d1	vynikající
maskováním	maskování	k1gNnSc7	maskování
ve	v	k7c6	v
stromech	strom	k1gInPc6	strom
<g/>
.	.	kIx.	.
</s>
<s>
Nohy	noha	k1gFnPc1	noha
jsou	být	k5eAaImIp3nP	být
černé	černý	k2eAgFnPc1d1	černá
a	a	k8xC	a
krátké	krátký	k2eAgFnPc1d1	krátká
<g/>
,	,	kIx,	,
s	s	k7c7	s
hustě	hustě	k6eAd1	hustě
osrstěnými	osrstěný	k2eAgNnPc7d1	osrstěné
chodidly	chodidlo	k1gNnPc7	chodidlo
<g/>
.	.	kIx.	.
</s>
<s>
Kožešina	kožešina	k1gFnSc1	kožešina
na	na	k7c6	na
chodidlech	chodidlo	k1gNnPc6	chodidlo
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
tepelná	tepelný	k2eAgFnSc1d1	tepelná
izolace	izolace	k1gFnSc1	izolace
na	na	k7c6	na
ledových	ledový	k2eAgFnPc6d1	ledová
a	a	k8xC	a
zasněžených	zasněžený	k2eAgFnPc6d1	zasněžená
plochách	plocha	k1gFnPc6	plocha
a	a	k8xC	a
také	také	k9	také
skrývá	skrývat	k5eAaImIp3nS	skrývat
pachové	pachový	k2eAgFnPc4d1	pachová
žlázy	žláza	k1gFnPc4	žláza
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
umístěny	umístit	k5eAaPmNgFnP	umístit
i	i	k9	i
na	na	k7c6	na
řiti	řiť	k1gFnSc6	řiť
<g/>
.	.	kIx.	.
</s>
<s>
Panda	panda	k1gFnSc1	panda
červená	červená	k1gFnSc1	červená
připomíná	připomínat	k5eAaImIp3nS	připomínat
menší	malý	k2eAgFnSc7d2	menší
postavou	postava	k1gFnSc7	postava
na	na	k7c6	na
krátkých	krátký	k2eAgFnPc6d1	krátká
nohách	noha	k1gFnPc6	noha
a	a	k8xC	a
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
ocasem	ocas	k1gInSc7	ocas
spíše	spíše	k9	spíše
mývala	mýval	k1gMnSc4	mýval
než	než	k8xS	než
medvěda	medvěd	k1gMnSc4	medvěd
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
menším	malý	k2eAgInSc7d2	menší
počtem	počet	k1gInSc7	počet
zubů	zub	k1gInPc2	zub
(	(	kIx(	(
<g/>
její	její	k3xOp3gInSc1	její
zubní	zubní	k2eAgInSc1d1	zubní
vzorec	vzorec	k1gInSc1	vzorec
je	být	k5eAaImIp3nS	být
3	[number]	k4	3
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
,	,	kIx,	,
2	[number]	k4	2
=	=	kIx~	=
38	[number]	k4	38
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bližší	blízký	k2eAgMnSc1d2	bližší
šelmám	šelma	k1gFnPc3	šelma
medvídkovitým	medvídkovitý	k2eAgFnPc3d1	medvídkovitá
<g/>
.	.	kIx.	.
</s>
<s>
Prsty	prst	k1gInPc4	prst
má	mít	k5eAaImIp3nS	mít
poměrně	poměrně	k6eAd1	poměrně
krátké	krátký	k2eAgNnSc1d1	krátké
<g/>
,	,	kIx,	,
opatřené	opatřený	k2eAgInPc1d1	opatřený
polozatažitelnými	polozatažitelný	k2eAgInPc7d1	polozatažitelný
drápy	dráp	k1gInPc7	dráp
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
panda	panda	k1gFnSc1	panda
velká	velká	k1gFnSc1	velká
našlapuje	našlapovat	k5eAaImIp3nS	našlapovat
na	na	k7c4	na
celá	celý	k2eAgNnPc4d1	celé
chodidla	chodidlo	k1gNnPc4	chodidlo
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
pandy	panda	k1gFnSc2	panda
červené	červená	k1gFnSc2	červená
se	se	k3xPyFc4	se
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
"	"	kIx"	"
<g/>
šestý	šestý	k4xOgInSc1	šestý
prst	prst	k1gInSc1	prst
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
palec	palec	k1gInSc4	palec
navíc	navíc	k6eAd1	navíc
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
výrůstkem	výrůstek	k1gInSc7	výrůstek
zápěstní	zápěstní	k2eAgFnPc4d1	zápěstní
kosti	kost	k1gFnPc4	kost
<g/>
.	.	kIx.	.
</s>
<s>
Můžeme	moct	k5eAaImIp1nP	moct
jej	on	k3xPp3gMnSc4	on
najít	najít	k5eAaPmF	najít
i	i	k9	i
u	u	k7c2	u
pandy	panda	k1gFnSc2	panda
velké	velký	k2eAgFnSc2d1	velká
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
prst	prst	k1gInSc1	prst
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
lepší	dobrý	k2eAgNnSc4d2	lepší
uchopování	uchopování	k1gNnSc4	uchopování
větví	větev	k1gFnPc2	větev
(	(	kIx(	(
<g/>
hlavně	hlavně	k6eAd1	hlavně
bambusu	bambus	k1gInSc3	bambus
<g/>
)	)	kIx)	)
a	a	k8xC	a
větší	veliký	k2eAgInSc1d2	veliký
stisk	stisk	k1gInSc1	stisk
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
samcem	samec	k1gInSc7	samec
a	a	k8xC	a
samicí	samice	k1gFnSc7	samice
není	být	k5eNaImIp3nS	být
žádný	žádný	k3yNgInSc1	žádný
viditelný	viditelný	k2eAgInSc1d1	viditelný
rozdíl	rozdíl	k1gInSc1	rozdíl
<g/>
,	,	kIx,	,
mláďata	mládě	k1gNnPc1	mládě
se	se	k3xPyFc4	se
podobají	podobat	k5eAaImIp3nP	podobat
dospělým	dospělý	k2eAgMnPc3d1	dospělý
jedincům	jedinec	k1gMnPc3	jedinec
<g/>
,	,	kIx,	,
jen	jen	k9	jen
jsou	být	k5eAaImIp3nP	být
menší	malý	k2eAgFnPc1d2	menší
<g/>
.	.	kIx.	.
</s>
<s>
Průměrně	průměrně	k6eAd1	průměrně
se	se	k3xPyFc4	se
dožívá	dožívat	k5eAaImIp3nS	dožívat
8	[number]	k4	8
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
až	až	k9	až
14	[number]	k4	14
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Rekordní	rekordní	k2eAgInSc1d1	rekordní
věk	věk	k1gInSc1	věk
<g/>
,	,	kIx,	,
zaznamenaný	zaznamenaný	k2eAgInSc1d1	zaznamenaný
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
23	[number]	k4	23
let	léto	k1gNnPc2	léto
a	a	k8xC	a
4	[number]	k4	4
měsíce	měsíc	k1gInSc2	měsíc
u	u	k7c2	u
samce	samec	k1gInSc2	samec
pojmenovaného	pojmenovaný	k2eAgInSc2d1	pojmenovaný
Firecracker	Firecracker	k1gInSc1	Firecracker
<g/>
.	.	kIx.	.
<g/>
Panda	panda	k1gFnSc1	panda
červená	červený	k2eAgFnSc1d1	červená
je	být	k5eAaImIp3nS	být
většinou	většina	k1gFnSc7	většina
tichá	tichý	k2eAgFnSc1d1	tichá
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
jedny	jeden	k4xCgInPc4	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
zvuků	zvuk	k1gInPc2	zvuk
patří	patřit	k5eAaImIp3nS	patřit
pískání	pískání	k1gNnSc1	pískání
při	při	k7c6	při
vzájemné	vzájemný	k2eAgFnSc6d1	vzájemná
komunikaci	komunikace	k1gFnSc6	komunikace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Jejím	její	k3xOp3gInSc7	její
domovem	domov	k1gInSc7	domov
jsou	být	k5eAaImIp3nP	být
horské	horský	k2eAgInPc1d1	horský
lesy	les	k1gInPc1	les
a	a	k8xC	a
džungle	džungle	k1gFnPc1	džungle
v	v	k7c6	v
podhůří	podhůří	k1gNnSc6	podhůří
Himálaje	Himálaj	k1gFnSc2	Himálaj
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Číně	Čína	k1gFnSc6	Čína
<g/>
,	,	kIx,	,
Myanmaru	Myanmar	k1gInSc6	Myanmar
<g/>
,	,	kIx,	,
Nepálu	Nepál	k1gInSc6	Nepál
<g/>
,	,	kIx,	,
Bhútánu	Bhútán	k1gInSc6	Bhútán
a	a	k8xC	a
severovýchodní	severovýchodní	k2eAgFnSc6d1	severovýchodní
Indii	Indie	k1gFnSc6	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Rozšíření	rozšíření	k1gNnSc1	rozšíření
v	v	k7c6	v
severním	severní	k2eAgInSc6d1	severní
Laosu	Laos	k1gInSc6	Laos
není	být	k5eNaImIp3nS	být
potvrzeno	potvrdit	k5eAaPmNgNnS	potvrdit
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
spíše	spíše	k9	spíše
nepravděpodobné	pravděpodobný	k2eNgNnSc1d1	nepravděpodobné
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
v	v	k7c6	v
záznamech	záznam	k1gInPc6	záznam
o	o	k7c6	o
jejím	její	k3xOp3gInSc6	její
tamním	tamní	k2eAgInSc6d1	tamní
výskytu	výskyt	k1gInSc6	výskyt
se	se	k3xPyFc4	se
našlo	najít	k5eAaPmAgNnS	najít
mnoho	mnoho	k4c1	mnoho
nesrovnalostí	nesrovnalost	k1gFnPc2	nesrovnalost
<g/>
.	.	kIx.	.
</s>
<s>
Nejzápadněji	západně	k6eAd3	západně
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Annapurny	Annapurna	k1gFnSc2	Annapurna
v	v	k7c6	v
Nepálu	Nepál	k1gInSc6	Nepál
<g/>
,	,	kIx,	,
nejvýchodněji	východně	k6eAd3	východně
v	v	k7c6	v
pohoří	pohoří	k1gNnSc6	pohoří
Čchin-ling	Čchining	k1gInSc1	Čchin-ling
v	v	k7c6	v
čínské	čínský	k2eAgFnSc6d1	čínská
provincii	provincie	k1gFnSc6	provincie
Šen-si	Šene	k1gFnSc4	Šen-se
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
rozšíření	rozšíření	k1gNnSc1	rozšíření
je	být	k5eAaImIp3nS	být
spíše	spíše	k9	spíše
rozdělené	rozdělená	k1gFnPc4	rozdělená
do	do	k7c2	do
subpopulací	subpopulace	k1gFnPc2	subpopulace
než	než	k8xS	než
souvislé	souvislý	k2eAgFnPc1d1	souvislá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Panda	panda	k1gFnSc1	panda
červená	červený	k2eAgFnSc1d1	červená
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejvýše	nejvýše	k6eAd1	nejvýše
žijící	žijící	k2eAgNnPc4d1	žijící
zvířata	zvíře	k1gNnPc4	zvíře
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
nadmořských	nadmořský	k2eAgFnPc6d1	nadmořská
výškách	výška	k1gFnPc6	výška
od	od	k7c2	od
2	[number]	k4	2
000	[number]	k4	000
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
kolem	kolem	k7c2	kolem
4	[number]	k4	4
000	[number]	k4	000
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
,	,	kIx,	,
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
může	moct	k5eAaImIp3nS	moct
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
i	i	k9	i
sněžné	sněžný	k2eAgFnPc4d1	sněžná
čáry	čára	k1gFnPc4	čára
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
5	[number]	k4	5
000	[number]	k4	000
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
V	v	k7c6	v
severovýchodní	severovýchodní	k2eAgFnSc6d1	severovýchodní
Indii	Indie	k1gFnSc6	Indie
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
níže	nízce	k6eAd2	nízce
<g/>
,	,	kIx,	,
v	v	k7c6	v
tropických	tropický	k2eAgInPc6d1	tropický
lesích	les	k1gInPc6	les
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
1	[number]	k4	1
400	[number]	k4	400
a	a	k8xC	a
1	[number]	k4	1
700	[number]	k4	700
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
V	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
výskytu	výskyt	k1gInSc2	výskyt
pandy	panda	k1gFnSc2	panda
červené	červený	k2eAgFnSc2d1	červená
teploty	teplota	k1gFnSc2	teplota
průměrně	průměrně	k6eAd1	průměrně
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
10	[number]	k4	10
až	až	k9	až
25	[number]	k4	25
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
srážky	srážka	k1gFnPc4	srážka
3	[number]	k4	3
500	[number]	k4	500
mm	mm	kA	mm
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
klima	klima	k1gNnSc4	klima
je	být	k5eAaImIp3nS	být
stabilní	stabilní	k2eAgFnSc1d1	stabilní
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
úzce	úzko	k6eAd1	úzko
spjata	spjat	k2eAgFnSc1d1	spjata
se	s	k7c7	s
smíšenými	smíšený	k2eAgInPc7d1	smíšený
lesy	les	k1gInPc7	les
s	s	k7c7	s
opadavými	opadavý	k2eAgInPc7d1	opadavý
i	i	k8xC	i
jehličnatými	jehličnatý	k2eAgInPc7d1	jehličnatý
stromy	strom	k1gInPc7	strom
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
se	s	k7c7	s
starými	starý	k2eAgInPc7d1	starý
stromy	strom	k1gInPc7	strom
a	a	k8xC	a
porosty	porost	k1gInPc7	porost
bambusu	bambus	k1gInSc2	bambus
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
většina	většina	k1gFnSc1	většina
pozorování	pozorování	k1gNnPc2	pozorování
pandy	panda	k1gFnSc2	panda
červené	červená	k1gFnSc2	červená
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
do	do	k7c2	do
100	[number]	k4	100
metrů	metr	k1gInPc2	metr
od	od	k7c2	od
vodní	vodní	k2eAgFnSc2d1	vodní
plochy	plocha	k1gFnSc2	plocha
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
přítomnost	přítomnost	k1gFnSc1	přítomnost
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
důležitým	důležitý	k2eAgInSc7d1	důležitý
předpokladem	předpoklad	k1gInSc7	předpoklad
pro	pro	k7c4	pro
místo	místo	k1gNnSc4	místo
výskytu	výskyt	k1gInSc2	výskyt
pandy	panda	k1gFnSc2	panda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Etologie	etologie	k1gFnSc2	etologie
==	==	k?	==
</s>
</p>
<p>
<s>
Panda	panda	k1gFnSc1	panda
červená	červený	k2eAgFnSc1d1	červená
je	být	k5eAaImIp3nS	být
většinou	většina	k1gFnSc7	většina
nejaktivnější	aktivní	k2eAgFnSc7d3	nejaktivnější
za	za	k7c2	za
svítání	svítání	k1gNnSc2	svítání
a	a	k8xC	a
soumraku	soumrak	k1gInSc2	soumrak
<g/>
,	,	kIx,	,
celkově	celkově	k6eAd1	celkově
je	být	k5eAaImIp3nS	být
spíše	spíše	k9	spíše
nočním	noční	k2eAgNnSc7d1	noční
zvířetem	zvíře	k1gNnSc7	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
aktivní	aktivní	k2eAgFnSc1d1	aktivní
asi	asi	k9	asi
56	[number]	k4	56
%	%	kIx~	%
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
den	den	k1gInSc4	den
většinou	většina	k1gFnSc7	většina
spí	spát	k5eAaImIp3nS	spát
stočená	stočený	k2eAgFnSc1d1	stočená
na	na	k7c6	na
větvi	větev	k1gFnSc6	větev
s	s	k7c7	s
ocasem	ocas	k1gInSc7	ocas
přetaženým	přetažený	k2eAgInSc7d1	přetažený
přes	přes	k7c4	přes
obličej	obličej	k1gInSc4	obličej
nebo	nebo	k8xC	nebo
s	s	k7c7	s
hlavou	hlava	k1gFnSc7	hlava
zabořenou	zabořený	k2eAgFnSc7d1	zabořená
pod	pod	k7c7	pod
hrudí	hruď	k1gFnSc7	hruď
<g/>
.	.	kIx.	.
</s>
<s>
Výborně	výborně	k6eAd1	výborně
šplhá	šplhat	k5eAaImIp3nS	šplhat
a	a	k8xC	a
často	často	k6eAd1	často
ji	on	k3xPp3gFnSc4	on
lze	lze	k6eAd1	lze
zastihnout	zastihnout	k5eAaPmF	zastihnout
na	na	k7c6	na
stromech	strom	k1gInPc6	strom
<g/>
,	,	kIx,	,
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
se	se	k3xPyFc4	se
zdržuje	zdržovat	k5eAaImIp3nS	zdržovat
méně	málo	k6eAd2	málo
<g/>
.	.	kIx.	.
</s>
<s>
Pandy	panda	k1gFnPc1	panda
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
málo	málo	k6eAd1	málo
<g/>
,	,	kIx,	,
spíše	spíše	k9	spíše
více	hodně	k6eAd2	hodně
spí	spát	k5eAaImIp3nS	spát
a	a	k8xC	a
jí	jíst	k5eAaImIp3nS	jíst
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
ušetřily	ušetřit	k5eAaPmAgInP	ušetřit
energii	energie	k1gFnSc4	energie
pracně	pracně	k6eAd1	pracně
získanou	získaný	k2eAgFnSc4d1	získaná
z	z	k7c2	z
trávení	trávení	k1gNnSc2	trávení
bambusu	bambus	k1gInSc2	bambus
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
samotářská	samotářský	k2eAgNnPc4d1	samotářské
zvířata	zvíře	k1gNnPc4	zvíře
<g/>
,	,	kIx,	,
setkávají	setkávat	k5eAaImIp3nP	setkávat
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
období	období	k1gNnSc6	období
páření	páření	k1gNnSc2	páření
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
mají	mít	k5eAaImIp3nP	mít
teritorium	teritorium	k1gNnSc4	teritorium
větší	veliký	k2eAgNnSc4d2	veliký
<g/>
,	,	kIx,	,
překrývá	překrývat	k5eAaImIp3nS	překrývat
se	se	k3xPyFc4	se
s	s	k7c7	s
teritorii	teritorium	k1gNnPc7	teritorium
několika	několik	k4yIc3	několik
samic	samice	k1gFnPc2	samice
<g/>
.	.	kIx.	.
</s>
<s>
Pandy	panda	k1gFnPc1	panda
červené	červený	k2eAgFnPc1d1	červená
se	se	k3xPyFc4	se
o	o	k7c4	o
sobě	se	k3xPyFc3	se
před	před	k7c7	před
pářením	páření	k1gNnSc7	páření
i	i	k9	i
jindy	jindy	k6eAd1	jindy
dozvídají	dozvídat	k5eAaImIp3nP	dozvídat
pískáním	pískání	k1gNnSc7	pískání
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
teritorium	teritorium	k1gNnSc4	teritorium
si	se	k3xPyFc3	se
značkuje	značkovat	k5eAaImIp3nS	značkovat
trusem	trus	k1gInSc7	trus
<g/>
,	,	kIx,	,
močí	moč	k1gFnSc7	moč
a	a	k8xC	a
pižmově	pižmově	k6eAd1	pižmově
páchnoucím	páchnoucí	k2eAgInSc7d1	páchnoucí
výměškem	výměšek	k1gInSc7	výměšek
pachových	pachový	k2eAgFnPc2d1	pachová
žláz	žláza	k1gFnPc2	žláza
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Predátoři	predátor	k1gMnPc5	predátor
===	===	k?	===
</s>
</p>
<p>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
vysokohorskému	vysokohorský	k2eAgNnSc3d1	vysokohorské
prostředí	prostředí	k1gNnSc3	prostředí
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
žije	žít	k5eAaImIp3nS	žít
<g/>
,	,	kIx,	,
má	můj	k3xOp1gFnSc1	můj
panda	panda	k1gFnSc1	panda
červená	červenat	k5eAaImIp3nS	červenat
méně	málo	k6eAd2	málo
predátorů	predátor	k1gMnPc2	predátor
<g/>
,	,	kIx,	,
než	než	k8xS	než
kdyby	kdyby	kYmCp3nS	kdyby
žila	žít	k5eAaImAgFnS	žít
níže	nízce	k6eAd2	nízce
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nepřátele	nepřítel	k1gMnPc4	nepřítel
pandy	panda	k1gFnSc2	panda
červené	červená	k1gFnSc2	červená
patří	patřit	k5eAaImIp3nS	patřit
levhart	levhart	k1gMnSc1	levhart
sněžný	sněžný	k2eAgMnSc1d1	sněžný
a	a	k8xC	a
lasicovití	lasicovitý	k2eAgMnPc1d1	lasicovitý
<g/>
,	,	kIx,	,
mláďata	mládě	k1gNnPc1	mládě
loví	lovit	k5eAaImIp3nP	lovit
i	i	k9	i
menší	malý	k2eAgFnPc1d2	menší
šelmy	šelma	k1gFnPc1	šelma
a	a	k8xC	a
draví	dravý	k2eAgMnPc1d1	dravý
ptáci	pták	k1gMnPc1	pták
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
ohrožení	ohrožení	k1gNnSc6	ohrožení
predátorem	predátor	k1gMnSc7	predátor
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
utéct	utéct	k5eAaPmF	utéct
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
to	ten	k3xDgNnSc1	ten
nepovede	povést	k5eNaPmIp3nS	povést
<g/>
,	,	kIx,	,
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
zadních	zadní	k2eAgFnPc6d1	zadní
nohách	noha	k1gFnPc6	noha
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
vypadaly	vypadat	k5eAaImAgFnP	vypadat
větší	veliký	k2eAgFnPc1d2	veliký
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
brání	bránit	k5eAaImIp3nS	bránit
svými	svůj	k3xOyFgInPc7	svůj
ostrými	ostrý	k2eAgInPc7d1	ostrý
drápy	dráp	k1gInPc7	dráp
<g/>
.	.	kIx.	.
</s>
<s>
Panda	panda	k1gFnSc1	panda
červená	červený	k2eAgFnSc1d1	červená
vydrží	vydržet	k5eAaPmIp3nS	vydržet
stát	stát	k5eAaImF	stát
na	na	k7c6	na
zadních	zadní	k2eAgFnPc6d1	zadní
nohách	noha	k1gFnPc6	noha
až	až	k9	až
30	[number]	k4	30
sekund	sekunda	k1gFnPc2	sekunda
a	a	k8xC	a
některé	některý	k3yIgFnPc1	některý
pandy	panda	k1gFnPc1	panda
takto	takto	k6eAd1	takto
dokáží	dokázat	k5eAaPmIp3nP	dokázat
urazit	urazit	k5eAaPmF	urazit
několik	několik	k4yIc4	několik
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
==	==	k?	==
</s>
</p>
<p>
<s>
Pandy	panda	k1gFnPc1	panda
nevytvářejí	vytvářet	k5eNaImIp3nP	vytvářet
stálé	stálý	k2eAgFnPc1d1	stálá
páry	pára	k1gFnPc1	pára
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
samotáři	samotář	k1gMnPc1	samotář
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
úmyslně	úmyslně	k6eAd1	úmyslně
se	se	k3xPyFc4	se
setkávají	setkávat	k5eAaImIp3nP	setkávat
v	v	k7c6	v
době	doba	k1gFnSc6	doba
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
mezi	mezi	k7c7	mezi
lednem	leden	k1gInSc7	leden
a	a	k8xC	a
začátkem	začátek	k1gInSc7	začátek
března	březen	k1gInSc2	březen
<g/>
.	.	kIx.	.
</s>
<s>
Vyhledávají	vyhledávat	k5eAaImIp3nP	vyhledávat
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
pachu	pach	k1gInSc2	pach
a	a	k8xC	a
pískavých	pískavý	k2eAgInPc2d1	pískavý
zvuků	zvuk	k1gInPc2	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Páření	páření	k1gNnSc1	páření
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
a	a	k8xC	a
pár	pár	k4xCyI	pár
se	se	k3xPyFc4	se
po	po	k7c6	po
něm	on	k3xPp3gMnSc6	on
okamžitě	okamžitě	k6eAd1	okamžitě
rozchází	rozcházet	k5eAaImIp3nS	rozcházet
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgNnPc1	dva
pohlaví	pohlaví	k1gNnPc2	pohlaví
se	se	k3xPyFc4	se
často	často	k6eAd1	často
páří	pářit	k5eAaImIp3nS	pářit
s	s	k7c7	s
více	hodně	k6eAd2	hodně
jedinci	jedinec	k1gMnPc7	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnSc1	samice
dokáže	dokázat	k5eAaPmIp3nS	dokázat
odložit	odložit	k5eAaPmF	odložit
porod	porod	k1gInSc4	porod
mláďat	mládě	k1gNnPc2	mládě
do	do	k7c2	do
vhodné	vhodný	k2eAgFnSc2d1	vhodná
situace	situace	k1gFnSc2	situace
až	až	k9	až
o	o	k7c4	o
týden	týden	k1gInSc4	týden
<g/>
,	,	kIx,	,
možná	možná	k9	možná
i	i	k9	i
více	hodně	k6eAd2	hodně
(	(	kIx(	(
<g/>
odložená	odložený	k2eAgFnSc1d1	odložená
nidace	nidace	k1gFnSc1	nidace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
114	[number]	k4	114
až	až	k9	až
145	[number]	k4	145
dnech	den	k1gInPc6	den
březosti	březost	k1gFnSc2	březost
se	se	k3xPyFc4	se
samici	samice	k1gFnSc3	samice
rodí	rodit	k5eAaImIp3nS	rodit
ve	v	k7c6	v
hnízdě	hnízdo	k1gNnSc6	hnízdo
v	v	k7c6	v
dutině	dutina	k1gFnSc6	dutina
stromu	strom	k1gInSc2	strom
1	[number]	k4	1
až	až	k9	až
2	[number]	k4	2
mláďata	mládě	k1gNnPc4	mládě
<g/>
,	,	kIx,	,
výjimečně	výjimečně	k6eAd1	výjimečně
až	až	k9	až
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
váží	vážit	k5eAaImIp3nS	vážit
při	při	k7c6	při
narození	narození	k1gNnSc6	narození
asi	asi	k9	asi
100	[number]	k4	100
g	g	kA	g
a	a	k8xC	a
dva	dva	k4xCgInPc4	dva
týdny	týden	k1gInPc4	týden
jsou	být	k5eAaImIp3nP	být
úplně	úplně	k6eAd1	úplně
holá	holý	k2eAgNnPc1d1	holé
<g/>
,	,	kIx,	,
slepá	slepý	k2eAgNnPc1d1	slepé
<g/>
,	,	kIx,	,
a	a	k8xC	a
bezmocná	bezmocný	k2eAgFnSc1d1	bezmocná
<g/>
,	,	kIx,	,
oči	oko	k1gNnPc1	oko
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
otevírají	otevírat	k5eAaImIp3nP	otevírat
po	po	k7c6	po
18	[number]	k4	18
dnech	den	k1gInPc6	den
<g/>
.	.	kIx.	.
</s>
<s>
Mládě	mládě	k1gNnSc1	mládě
pandy	panda	k1gFnSc2	panda
svou	svůj	k3xOyFgFnSc4	svůj
matku	matka	k1gFnSc4	matka
opravdu	opravdu	k6eAd1	opravdu
zaměstnává	zaměstnávat	k5eAaImIp3nS	zaměstnávat
–	–	k?	–
mládě	mládě	k1gNnSc1	mládě
saje	sát	k5eAaImIp3nS	sát
mléko	mléko	k1gNnSc4	mléko
až	až	k6eAd1	až
14	[number]	k4	14
<g/>
krát	krát	k6eAd1	krát
za	za	k7c4	za
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
první	první	k4xOgInSc4	první
týden	týden	k1gInSc4	týden
tráví	trávit	k5eAaImIp3nS	trávit
matka	matka	k1gFnSc1	matka
s	s	k7c7	s
mláďaty	mládě	k1gNnPc7	mládě
60	[number]	k4	60
<g/>
–	–	k?	–
<g/>
90	[number]	k4	90
%	%	kIx~	%
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
vydává	vydávat	k5eAaImIp3nS	vydávat
častěji	často	k6eAd2	často
mimo	mimo	k7c4	mimo
hnízdo	hnízdo	k1gNnSc4	hnízdo
a	a	k8xC	a
vrací	vracet	k5eAaImIp3nS	vracet
se	se	k3xPyFc4	se
mláďata	mládě	k1gNnPc4	mládě
čistit	čistit	k5eAaImF	čistit
a	a	k8xC	a
krmit	krmit	k5eAaImF	krmit
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
srst	srst	k1gFnSc1	srst
je	být	k5eAaImIp3nS	být
zpočátku	zpočátku	k6eAd1	zpočátku
šedo-hnědá	šedonědý	k2eAgFnSc1d1	šedo-hnědá
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
získává	získávat	k5eAaImIp3nS	získávat
barvu	barva	k1gFnSc4	barva
dospělých	dospělý	k2eAgMnPc2d1	dospělý
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
týdnech	týden	k1gInPc6	týden
se	se	k3xPyFc4	se
mláďata	mládě	k1gNnPc1	mládě
vydávají	vydávat	k5eAaPmIp3nP	vydávat
na	na	k7c4	na
první	první	k4xOgInPc4	první
průzkumy	průzkum	k1gInPc4	průzkum
okolí	okolí	k1gNnSc2	okolí
<g/>
,	,	kIx,	,
pohlavně	pohlavně	k6eAd1	pohlavně
dospívají	dospívat	k5eAaImIp3nP	dospívat
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
18	[number]	k4	18
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
jejich	jejich	k3xOp3gInSc4	jejich
pomalý	pomalý	k2eAgInSc4d1	pomalý
vývoj	vývoj	k1gInSc4	vývoj
se	se	k3xPyFc4	se
o	o	k7c4	o
ně	on	k3xPp3gMnPc4	on
matka	matka	k1gFnSc1	matka
stará	starat	k5eAaImIp3nS	starat
až	až	k9	až
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Úmrtnost	úmrtnost	k1gFnSc1	úmrtnost
mláďat	mládě	k1gNnPc2	mládě
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
je	být	k5eAaImIp3nS	být
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
,	,	kIx,	,
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
až	až	k9	až
80	[number]	k4	80
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Potrava	potrava	k1gFnSc1	potrava
==	==	k?	==
</s>
</p>
<p>
<s>
Panda	panda	k1gFnSc1	panda
červená	červená	k1gFnSc1	červená
má	mít	k5eAaImIp3nS	mít
trávicí	trávicí	k2eAgInSc4d1	trávicí
systém	systém	k1gInSc4	systém
přizpůsobený	přizpůsobený	k2eAgInSc4d1	přizpůsobený
k	k	k7c3	k
přijímání	přijímání	k1gNnSc3	přijímání
masité	masitý	k2eAgFnSc2d1	masitá
potravy	potrava	k1gFnSc2	potrava
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mezi	mezi	k7c4	mezi
její	její	k3xOp3gInPc4	její
hlavní	hlavní	k2eAgInPc4d1	hlavní
zdroje	zdroj	k1gInPc4	zdroj
energie	energie	k1gFnSc2	energie
patří	patřit	k5eAaImIp3nP	patřit
bambusové	bambusový	k2eAgInPc1d1	bambusový
výhonky	výhonek	k1gInPc1	výhonek
a	a	k8xC	a
listy	list	k1gInPc1	list
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
neumí	umět	k5eNaImIp3nS	umět
strávit	strávit	k5eAaPmF	strávit
celulózu	celulóza	k1gFnSc4	celulóza
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgFnSc7d1	hlavní
složkou	složka	k1gFnSc7	složka
buněčné	buněčný	k2eAgFnSc2d1	buněčná
stěny	stěna	k1gFnSc2	stěna
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nP	muset
spotřebovat	spotřebovat	k5eAaPmF	spotřebovat
bambusu	bambus	k1gInSc3	bambus
velké	velká	k1gFnSc2	velká
množství	množství	k1gNnSc2	množství
<g/>
,	,	kIx,	,
až	až	k9	až
1,5	[number]	k4	1,5
kg	kg	kA	kg
bambusových	bambusový	k2eAgInPc2d1	bambusový
listů	list	k1gInPc2	list
a	a	k8xC	a
4	[number]	k4	4
kg	kg	kA	kg
bambusových	bambusový	k2eAgInPc2d1	bambusový
výhonků	výhonek	k1gInPc2	výhonek
denně	denně	k6eAd1	denně
<g/>
.	.	kIx.	.
</s>
<s>
Bambusové	bambusový	k2eAgInPc1d1	bambusový
výhonky	výhonek	k1gInPc1	výhonek
jsou	být	k5eAaImIp3nP	být
výživnější	výživný	k2eAgInPc1d2	výživnější
než	než	k8xS	než
listy	list	k1gInPc1	list
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	on	k3xPp3gFnPc4	on
pandy	panda	k1gFnPc4	panda
požírají	požírat	k5eAaImIp3nP	požírat
častěji	často	k6eAd2	často
<g/>
.	.	kIx.	.
</s>
<s>
Výhonky	výhonek	k1gInPc1	výhonek
jsou	být	k5eAaImIp3nP	být
nejvýživnější	výživný	k2eAgInPc1d3	nejvýživnější
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
a	a	k8xC	a
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
<g/>
,	,	kIx,	,
nejméně	málo	k6eAd3	málo
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
není	být	k5eNaImIp3nS	být
po	po	k7c6	po
ruce	ruka	k1gFnSc6	ruka
bambus	bambus	k1gInSc4	bambus
<g/>
,	,	kIx,	,
nepohrdne	pohrdnout	k5eNaPmIp3nS	pohrdnout
ani	ani	k8xC	ani
ovocem	ovoce	k1gNnSc7	ovoce
<g/>
,	,	kIx,	,
plody	plod	k1gInPc7	plod
<g/>
,	,	kIx,	,
kořínky	kořínek	k1gInPc7	kořínek
<g/>
,	,	kIx,	,
žaludy	žalud	k1gInPc7	žalud
<g/>
,	,	kIx,	,
různými	různý	k2eAgInPc7d1	různý
druhy	druh	k1gInPc7	druh
trav	tráva	k1gFnPc2	tráva
a	a	k8xC	a
lišejníků	lišejník	k1gInPc2	lišejník
<g/>
,	,	kIx,	,
a	a	k8xC	a
masitou	masitý	k2eAgFnSc4d1	masitá
stravu	strava	k1gFnSc4	strava
doplňuje	doplňovat	k5eAaImIp3nS	doplňovat
mladými	mladý	k2eAgMnPc7d1	mladý
ptáky	pták	k1gMnPc7	pták
<g/>
,	,	kIx,	,
vejci	vejce	k1gNnSc3	vejce
<g/>
,	,	kIx,	,
obratlovci	obratlovec	k1gMnPc7	obratlovec
(	(	kIx(	(
<g/>
myšmi	myš	k1gFnPc7	myš
<g/>
,	,	kIx,	,
menšími	malý	k2eAgMnPc7d2	menší
hlodavci	hlodavec	k1gMnPc7	hlodavec
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
ještěrkami	ještěrka	k1gFnPc7	ještěrka
<g/>
)	)	kIx)	)
a	a	k8xC	a
larvami	larva	k1gFnPc7	larva
hmyzu	hmyz	k1gInSc2	hmyz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
pandy	panda	k1gFnSc2	panda
červené	červená	k1gFnSc2	červená
dostávají	dostávat	k5eAaImIp3nP	dostávat
i	i	k9	i
kuřecí	kuřecí	k2eAgNnSc4d1	kuřecí
maso	maso	k1gNnSc4	maso
<g/>
.	.	kIx.	.
</s>
<s>
Potrava	potrava	k1gFnSc1	potrava
prochází	procházet	k5eAaImIp3nS	procházet
trávicí	trávicí	k2eAgFnSc7d1	trávicí
soustavou	soustava	k1gFnSc7	soustava
rychle	rychle	k6eAd1	rychle
<g/>
,	,	kIx,	,
za	za	k7c4	za
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
hodiny	hodina	k1gFnSc2	hodina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ohrožení	ohrožení	k1gNnSc1	ohrožení
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
klasifikace	klasifikace	k1gFnSc2	klasifikace
IUCN	IUCN	kA	IUCN
patří	patřit	k5eAaImIp3nS	patřit
panda	panda	k1gFnSc1	panda
červená	červený	k2eAgFnSc1d1	červená
mezi	mezi	k7c4	mezi
ohrožené	ohrožený	k2eAgInPc4d1	ohrožený
druhy	druh	k1gInPc4	druh
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
při	při	k7c6	při
nezměněných	změněný	k2eNgFnPc6d1	nezměněná
podmínkách	podmínka	k1gFnPc6	podmínka
hrozí	hrozit	k5eAaImIp3nS	hrozit
vysoké	vysoký	k2eAgNnSc4d1	vysoké
riziko	riziko	k1gNnSc4	riziko
vyhynutí	vyhynutí	k1gNnSc2	vyhynutí
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
vzestup	vzestup	k1gInSc4	vzestup
stupně	stupeň	k1gInSc2	stupeň
ohrožení	ohrožení	k1gNnSc2	ohrožení
oproti	oproti	k7c3	oproti
předchozí	předchozí	k2eAgFnSc3d1	předchozí
klasifikaci	klasifikace	k1gFnSc3	klasifikace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
druh	druh	k1gInSc1	druh
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
zranitelný	zranitelný	k2eAgInSc4d1	zranitelný
<g/>
.	.	kIx.	.
</s>
<s>
Odhady	odhad	k1gInPc1	odhad
o	o	k7c6	o
počtu	počet	k1gInSc6	počet
pand	panda	k1gFnPc2	panda
ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
různí	různit	k5eAaImIp3nP	různit
<g/>
,	,	kIx,	,
uvádí	uvádět	k5eAaImIp3nS	uvádět
se	se	k3xPyFc4	se
čísla	číslo	k1gNnSc2	číslo
od	od	k7c2	od
2	[number]	k4	2
500	[number]	k4	500
až	až	k6eAd1	až
po	po	k7c6	po
16	[number]	k4	16
<g/>
–	–	k?	–
<g/>
20	[number]	k4	20
000	[number]	k4	000
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
zvířat	zvíře	k1gNnPc2	zvíře
ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
ale	ale	k8xC	ale
prudce	prudko	k6eAd1	prudko
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
,	,	kIx,	,
jen	jen	k9	jen
za	za	k7c4	za
poslední	poslední	k2eAgFnPc4d1	poslední
tři	tři	k4xCgFnPc4	tři
generace	generace	k1gFnPc4	generace
(	(	kIx(	(
<g/>
to	ten	k3xDgNnSc1	ten
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
přibližně	přibližně	k6eAd1	přibližně
18	[number]	k4	18
letům	let	k1gInPc3	let
<g/>
)	)	kIx)	)
klesl	klesnout	k5eAaPmAgMnS	klesnout
počet	počet	k1gInSc4	počet
pand	panda	k1gFnPc2	panda
o	o	k7c4	o
50	[number]	k4	50
%	%	kIx~	%
a	a	k8xC	a
minimálně	minimálně	k6eAd1	minimálně
o	o	k7c4	o
50	[number]	k4	50
%	%	kIx~	%
má	mít	k5eAaImIp3nS	mít
populace	populace	k1gFnSc1	populace
klesat	klesat	k5eAaImF	klesat
i	i	k9	i
v	v	k7c6	v
příštích	příští	k2eAgFnPc6d1	příští
třech	tři	k4xCgFnPc6	tři
generacích	generace	k1gFnPc6	generace
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
početnost	početnost	k1gFnSc1	početnost
klesá	klesat	k5eAaImIp3nS	klesat
zvláště	zvláště	k6eAd1	zvláště
u	u	k7c2	u
východního	východní	k2eAgInSc2d1	východní
poddruhu	poddruh	k1gInSc2	poddruh
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
–	–	k?	–
počet	počet	k1gInSc1	počet
pand	panda	k1gFnPc2	panda
zde	zde	k6eAd1	zde
za	za	k7c4	za
posledních	poslední	k2eAgNnPc2d1	poslední
50	[number]	k4	50
let	léto	k1gNnPc2	léto
klesl	klesnout	k5eAaPmAgInS	klesnout
o	o	k7c4	o
40	[number]	k4	40
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Populace	populace	k1gFnSc1	populace
pand	panda	k1gFnPc2	panda
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
čítala	čítat	k5eAaImAgFnS	čítat
podle	podle	k7c2	podle
odhadu	odhad	k1gInSc2	odhad
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
3	[number]	k4	3
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
000	[number]	k4	000
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
žilo	žít	k5eAaImAgNnS	žít
podle	podle	k7c2	podle
odhadu	odhad	k1gInSc2	odhad
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
5	[number]	k4	5
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
000	[number]	k4	000
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
,	,	kIx,	,
v	v	k7c6	v
Nepálu	Nepál	k1gInSc6	Nepál
žilo	žít	k5eAaImAgNnS	žít
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2006	[number]	k4	2006
jen	jen	k9	jen
několik	několik	k4yIc4	několik
stovek	stovka	k1gFnPc2	stovka
pand	panda	k1gFnPc2	panda
<g/>
.	.	kIx.	.
</s>
<s>
Panda	panda	k1gFnSc1	panda
je	být	k5eAaImIp3nS	být
ohrožena	ohrozit	k5eAaPmNgFnS	ohrozit
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
ztráty	ztráta	k1gFnSc2	ztráta
přirozeného	přirozený	k2eAgNnSc2d1	přirozené
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
lidé	člověk	k1gMnPc1	člověk
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
sousedních	sousední	k2eAgFnPc6d1	sousední
zemch	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
rozšiřují	rozšiřovat	k5eAaImIp3nP	rozšiřovat
své	svůj	k3xOyFgNnSc4	svůj
území	území	k1gNnSc4	území
a	a	k8xC	a
kácí	kácet	k5eAaImIp3nP	kácet
lesy	les	k1gInPc1	les
–	–	k?	–
mnoho	mnoho	k4c1	mnoho
pand	panda	k1gFnPc2	panda
ročně	ročně	k6eAd1	ročně
zemře	zemřít	k5eAaPmIp3nS	zemřít
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
odlesňování	odlesňování	k1gNnSc2	odlesňování
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
zintezivňuje	zintezivňovat	k5eAaImIp3nS	zintezivňovat
<g/>
.	.	kIx.	.
</s>
<s>
Panda	panda	k1gFnSc1	panda
se	se	k3xPyFc4	se
kvůli	kvůli	k7c3	kvůli
své	svůj	k3xOyFgFnSc3	svůj
převážně	převážně	k6eAd1	převážně
bambusové	bambusový	k2eAgFnSc3d1	bambusová
stravě	strava	k1gFnSc3	strava
totiž	totiž	k9	totiž
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
přizpůsobit	přizpůsobit	k5eAaPmF	přizpůsobit
kulturní	kulturní	k2eAgFnSc3d1	kulturní
krajině	krajina	k1gFnSc3	krajina
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
zákaz	zákaz	k1gInSc4	zákaz
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
zabíjeny	zabíjet	k5eAaImNgInP	zabíjet
pro	pro	k7c4	pro
kožešinu	kožešina	k1gFnSc4	kožešina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
byl	být	k5eAaImAgInS	být
objeven	objevit	k5eAaPmNgInS	objevit
i	i	k9	i
problém	problém	k1gInSc1	problém
se	s	k7c7	s
psinkou	psinka	k1gFnSc7	psinka
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
na	na	k7c4	na
pandy	panda	k1gFnPc4	panda
přenáší	přenášet	k5eAaImIp3nP	přenášet
zdivočelí	zdivočelý	k2eAgMnPc1d1	zdivočelý
psi	pes	k1gMnPc1	pes
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
vše	všechen	k3xTgNnSc4	všechen
navazuje	navazovat	k5eAaImIp3nS	navazovat
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
rozmnožovací	rozmnožovací	k2eAgInSc1d1	rozmnožovací
cyklus	cyklus	k1gInSc1	cyklus
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
pomalé	pomalý	k2eAgNnSc4d1	pomalé
obnovování	obnovování	k1gNnSc4	obnovování
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ochrana	ochrana	k1gFnSc1	ochrana
===	===	k?	===
</s>
</p>
<p>
<s>
Panda	panda	k1gFnSc1	panda
červená	červený	k2eAgFnSc1d1	červená
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
CITES	CITES	kA	CITES
<g/>
,	,	kIx,	,
příloze	příloha	k1gFnSc6	příloha
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
zoologických	zoologický	k2eAgFnPc6d1	zoologická
zahradách	zahrada	k1gFnPc6	zahrada
probíhají	probíhat	k5eAaImIp3nP	probíhat
záchranné	záchranný	k2eAgInPc1d1	záchranný
programy	program	k1gInPc1	program
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
mají	mít	k5eAaImIp3nP	mít
zajistit	zajistit	k5eAaPmF	zajistit
budoucnost	budoucnost	k1gFnSc4	budoucnost
pandy	panda	k1gFnSc2	panda
červené	červený	k2eAgFnSc2d1	červená
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jejím	její	k3xOp3gInSc6	její
přirozeném	přirozený	k2eAgInSc6d1	přirozený
areálu	areál	k1gInSc6	areál
jsou	být	k5eAaImIp3nP	být
zřizovány	zřizován	k2eAgFnPc1d1	zřizována
rezervace	rezervace	k1gFnPc1	rezervace
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
mají	mít	k5eAaImIp3nP	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
chránit	chránit	k5eAaImF	chránit
jejich	jejich	k3xOp3gFnSc4	jejich
populaci	populace	k1gFnSc4	populace
<g/>
.	.	kIx.	.
</s>
<s>
Pandy	panda	k1gFnPc1	panda
červené	červený	k2eAgFnPc1d1	červená
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
35	[number]	k4	35
chráněných	chráněný	k2eAgFnPc6d1	chráněná
oblastech	oblast	k1gFnPc6	oblast
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
,	,	kIx,	,
ve	v	k7c6	v
20	[number]	k4	20
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
<g/>
,	,	kIx,	,
v	v	k7c6	v
7	[number]	k4	7
v	v	k7c6	v
Nepálu	Nepál	k1gInSc6	Nepál
<g/>
,	,	kIx,	,
ve	v	k7c6	v
2	[number]	k4	2
v	v	k7c6	v
Bhútánu	Bhútán	k1gInSc6	Bhútán
a	a	k8xC	a
minimálně	minimálně	k6eAd1	minimálně
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
v	v	k7c4	v
Myanmaru	Myanmara	k1gFnSc4	Myanmara
<g/>
.	.	kIx.	.
</s>
<s>
Panda	panda	k1gFnSc1	panda
červená	červený	k2eAgFnSc1d1	červená
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
svého	svůj	k3xOyFgInSc2	svůj
výskytu	výskyt	k1gInSc2	výskyt
chráněna	chráněn	k2eAgNnPc1d1	chráněno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
se	se	k3xPyFc4	se
o	o	k7c4	o
ochranu	ochrana	k1gFnSc4	ochrana
stará	starat	k5eAaImIp3nS	starat
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
ochraně	ochrana	k1gFnSc6	ochrana
přírody	příroda	k1gFnSc2	příroda
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
zařazena	zařadit	k5eAaPmNgFnS	zařadit
v	v	k7c6	v
nejvyšší	vysoký	k2eAgFnSc6d3	nejvyšší
kategorii	kategorie	k1gFnSc6	kategorie
ochrany	ochrana	k1gFnSc2	ochrana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Myanmaru	Myanmar	k1gInSc6	Myanmar
je	být	k5eAaImIp3nS	být
chráněna	chránit	k5eAaImNgFnS	chránit
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
je	být	k5eAaImIp3nS	být
zařazena	zařadit	k5eAaPmNgFnS	zařadit
do	do	k7c2	do
druhé	druhý	k4xOgFnSc2	druhý
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
kategorie	kategorie	k1gFnSc2	kategorie
ochrany	ochrana	k1gFnSc2	ochrana
pod	pod	k7c7	pod
zákonem	zákon	k1gInSc7	zákon
o	o	k7c6	o
ochraně	ochrana	k1gFnSc6	ochrana
divokých	divoký	k2eAgNnPc2d1	divoké
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
v	v	k7c6	v
Červeném	červený	k2eAgInSc6d1	červený
seznamu	seznam	k1gInSc6	seznam
Číny	Čína	k1gFnSc2	Čína
je	být	k5eAaImIp3nS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
jako	jako	k9	jako
zranitelná	zranitelný	k2eAgFnSc1d1	zranitelná
<g/>
.	.	kIx.	.
</s>
<s>
Zákonem	zákon	k1gInSc7	zákon
je	být	k5eAaImIp3nS	být
chráněna	chránit	k5eAaImNgFnS	chránit
i	i	k9	i
ve	v	k7c6	v
zbytku	zbytek	k1gInSc6	zbytek
svého	svůj	k3xOyFgNnSc2	svůj
rozšíření	rozšíření	k1gNnSc2	rozšíření
<g/>
,	,	kIx,	,
v	v	k7c6	v
Nepálu	Nepál	k1gInSc6	Nepál
a	a	k8xC	a
Bhútánu	Bhútán	k1gInSc6	Bhútán
<g/>
.	.	kIx.	.
<g/>
Anwaruddin	Anwaruddin	k2eAgMnSc1d1	Anwaruddin
Choudhury	Choudhura	k1gFnPc4	Choudhura
uvedl	uvést	k5eAaPmAgMnS	uvést
podle	podle	k7c2	podle
něj	on	k3xPp3gNnSc2	on
6	[number]	k4	6
zásadních	zásadní	k2eAgInPc2d1	zásadní
bodů	bod	k1gInPc2	bod
pro	pro	k7c4	pro
záchranu	záchrana	k1gFnSc4	záchrana
pandy	panda	k1gFnSc2	panda
červené	červená	k1gFnSc2	červená
<g/>
:	:	kIx,	:
rozšiřování	rozšiřování	k1gNnSc1	rozšiřování
chráněných	chráněný	k2eAgNnPc2d1	chráněné
území	území	k1gNnPc2	území
<g/>
,	,	kIx,	,
zabránění	zabránění	k1gNnSc4	zabránění
ilegálního	ilegální	k2eAgInSc2d1	ilegální
lovu	lov	k1gInSc2	lov
<g/>
,	,	kIx,	,
kontrola	kontrola	k1gFnSc1	kontrola
nadměrného	nadměrný	k2eAgNnSc2d1	nadměrné
vypalování	vypalování	k1gNnSc2	vypalování
lesů	les	k1gInPc2	les
<g/>
,	,	kIx,	,
regulace	regulace	k1gFnSc2	regulace
turismu	turismus	k1gInSc2	turismus
<g/>
,	,	kIx,	,
seznámení	seznámení	k1gNnSc4	seznámení
veřejnosti	veřejnost	k1gFnSc2	veřejnost
s	s	k7c7	s
ohrožením	ohrožení	k1gNnSc7	ohrožení
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
<g/>
,	,	kIx,	,
a	a	k8xC	a
trestání	trestání	k1gNnSc6	trestání
porušování	porušování	k1gNnSc2	porušování
stávajících	stávající	k2eAgInPc2d1	stávající
ochranných	ochranný	k2eAgInPc2d1	ochranný
zákonů	zákon	k1gInPc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
východním	východní	k2eAgInSc6d1	východní
Nepálu	Nepál	k1gInSc6	Nepál
se	se	k3xPyFc4	se
osvědčila	osvědčit	k5eAaPmAgFnS	osvědčit
ochrana	ochrana	k1gFnSc1	ochrana
prostředí	prostředí	k1gNnSc2	prostředí
pandy	panda	k1gFnSc2	panda
přímo	přímo	k6eAd1	přímo
obyvateli	obyvatel	k1gMnPc7	obyvatel
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
získávají	získávat	k5eAaImIp3nP	získávat
příjem	příjem	k1gInSc4	příjem
od	od	k7c2	od
turistů	turist	k1gMnPc2	turist
pozorujících	pozorující	k2eAgMnPc2d1	pozorující
tata	tata	k0	tata
zvířata	zvíře	k1gNnPc4	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
vesničané	vesničan	k1gMnPc1	vesničan
ve	v	k7c6	v
vysoko	vysoko	k6eAd1	vysoko
položených	položený	k2eAgFnPc6d1	položená
oblastech	oblast	k1gFnPc6	oblast
státu	stát	k1gInSc2	stát
Arunáčalpradéš	Arunáčalpradéš	k1gInSc4	Arunáčalpradéš
též	též	k9	též
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
skupinu	skupina	k1gFnSc4	skupina
pro	pro	k7c4	pro
ochranu	ochrana	k1gFnSc4	ochrana
pandy	panda	k1gFnSc2	panda
červené	červený	k2eAgFnSc2d1	červená
a	a	k8xC	a
chrání	chránit	k5eAaImIp3nS	chránit
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
200	[number]	k4	200
km	km	kA	km
<g/>
2	[number]	k4	2
lesa	les	k1gInSc2	les
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Chov	chov	k1gInSc1	chov
v	v	k7c6	v
zoo	zoo	k1gFnSc6	zoo
==	==	k?	==
</s>
</p>
<p>
<s>
Panda	panda	k1gFnSc1	panda
červená	červený	k2eAgFnSc1d1	červená
patří	patřit	k5eAaImIp3nS	patřit
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
k	k	k7c3	k
relativně	relativně	k6eAd1	relativně
často	často	k6eAd1	často
chovaným	chovaný	k2eAgMnPc3d1	chovaný
druhům	druh	k1gInPc3	druh
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
byla	být	k5eAaImAgFnS	být
chována	chovat	k5eAaImNgFnS	chovat
přibližně	přibližně	k6eAd1	přibližně
ve	v	k7c6	v
170	[number]	k4	170
evropských	evropský	k2eAgFnPc6d1	Evropská
zoo	zoo	k1gFnPc6	zoo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Také	také	k9	také
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Česka	Česko	k1gNnSc2	Česko
ji	on	k3xPp3gFnSc4	on
lze	lze	k6eAd1	lze
vidět	vidět	k5eAaImF	vidět
stále	stále	k6eAd1	stále
častěji	často	k6eAd2	často
–	–	k?	–
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
hned	hned	k6eAd1	hned
v	v	k7c6	v
devíti	devět	k4xCc6	devět
z	z	k7c2	z
15	[number]	k4	15
tradičních	tradiční	k2eAgFnPc2d1	tradiční
českých	český	k2eAgFnPc2d1	Česká
zoo	zoo	k1gFnPc2	zoo
sdružených	sdružený	k2eAgFnPc2d1	sdružená
v	v	k7c6	v
Unii	unie	k1gFnSc6	unie
českých	český	k2eAgFnPc2d1	Česká
a	a	k8xC	a
slovenských	slovenský	k2eAgFnPc2d1	slovenská
zoologických	zoologický	k2eAgFnPc2d1	zoologická
zahrad	zahrada	k1gFnPc2	zahrada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
tato	tento	k3xDgNnPc4	tento
zařízení	zařízení	k1gNnPc4	zařízení
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Zoo	zoo	k1gFnSc1	zoo
Brno	Brno	k1gNnSc1	Brno
</s>
</p>
<p>
<s>
Zoopark	zoopark	k1gInSc1	zoopark
Chomutov	Chomutov	k1gInSc1	Chomutov
</s>
</p>
<p>
<s>
Zoo	zoo	k1gFnSc1	zoo
Jihlava	Jihlava	k1gFnSc1	Jihlava
</s>
</p>
<p>
<s>
Zoo	zoo	k1gFnSc1	zoo
Liberec	Liberec	k1gInSc1	Liberec
</s>
</p>
<p>
<s>
Zoo	zoo	k1gFnSc1	zoo
Ostrava	Ostrava	k1gFnSc1	Ostrava
</s>
</p>
<p>
<s>
Zoo	zoo	k1gFnSc1	zoo
Plzeň	Plzeň	k1gFnSc1	Plzeň
</s>
</p>
<p>
<s>
Zoo	zoo	k1gFnSc1	zoo
Praha	Praha	k1gFnSc1	Praha
</s>
</p>
<p>
<s>
Zoo	zoo	k1gFnSc1	zoo
Ústí	ústí	k1gNnSc2	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
</s>
</p>
<p>
<s>
Zoo	zoo	k1gFnSc1	zoo
ZlínNa	ZlínN	k1gInSc2	ZlínN
Slovensku	Slovensko	k1gNnSc3	Slovensko
je	být	k5eAaImIp3nS	být
chována	chován	k2eAgFnSc1d1	chována
v	v	k7c4	v
Zoo	zoo	k1gFnSc4	zoo
Bojnice	Bojnice	k1gFnPc1	Bojnice
a	a	k8xC	a
Zoo	zoo	k1gFnSc1	zoo
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
zoologických	zoologický	k2eAgFnPc6d1	zoologická
zahradách	zahrada	k1gFnPc6	zahrada
ji	on	k3xPp3gFnSc4	on
lze	lze	k6eAd1	lze
najít	najít	k5eAaPmF	najít
častěji	často	k6eAd2	často
než	než	k8xS	než
pandu	panda	k1gFnSc4	panda
velkou	velký	k2eAgFnSc4d1	velká
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
medvědovitých	medvědovitý	k2eAgMnPc2d1	medvědovitý
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
ní	on	k3xPp3gFnSc2	on
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
živit	živit	k5eAaImF	živit
i	i	k9	i
náhradním	náhradní	k2eAgInSc7d1	náhradní
jídelníčkem	jídelníček	k1gInSc7	jídelníček
a	a	k8xC	a
netrvá	trvat	k5eNaImIp3nS	trvat
jen	jen	k9	jen
na	na	k7c6	na
bambusových	bambusový	k2eAgInPc6d1	bambusový
výhoncích	výhonek	k1gInPc6	výhonek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
chováno	chovat	k5eAaImNgNnS	chovat
nejméně	málo	k6eAd3	málo
817	[number]	k4	817
jedinců	jedinec	k1gMnPc2	jedinec
pandy	panda	k1gFnSc2	panda
červené	červený	k2eAgFnSc2d1	červená
ve	v	k7c6	v
254	[number]	k4	254
institucích	instituce	k1gFnPc6	instituce
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
511	[number]	k4	511
jedinců	jedinec	k1gMnPc2	jedinec
patřilo	patřit	k5eAaImAgNnS	patřit
k	k	k7c3	k
poddruhu	poddruh	k1gInSc3	poddruh
A.	A.	kA	A.
f.	f.	k?	f.
fulgens	fulgens	k6eAd1	fulgens
a	a	k8xC	a
306	[number]	k4	306
k	k	k7c3	k
poddruhu	poddruh	k1gInSc3	poddruh
A.	A.	kA	A.
f.	f.	k?	f.
styani	styaň	k1gFnSc6	styaň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zoo	zoo	k1gNnSc6	zoo
se	se	k3xPyFc4	se
většinou	většina	k1gFnSc7	většina
chovají	chovat	k5eAaImIp3nP	chovat
v	v	k7c6	v
otevřených	otevřený	k2eAgInPc6d1	otevřený
výbězích	výběh	k1gInPc6	výběh
se	se	k3xPyFc4	se
stromy	strom	k1gInPc1	strom
k	k	k7c3	k
odpočinku	odpočinek	k1gInSc3	odpočinek
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
bambusu	bambus	k1gInSc2	bambus
v	v	k7c6	v
zoo	zoo	k1gFnSc6	zoo
dostávají	dostávat	k5eAaImIp3nP	dostávat
i	i	k9	i
ovoce	ovoce	k1gNnSc4	ovoce
<g/>
,	,	kIx,	,
zeleninu	zelenina	k1gFnSc4	zelenina
<g/>
,	,	kIx,	,
trávu	tráva	k1gFnSc4	tráva
a	a	k8xC	a
vejce	vejce	k1gNnSc4	vejce
<g/>
,	,	kIx,	,
občas	občas	k6eAd1	občas
i	i	k9	i
kvalitní	kvalitní	k2eAgNnSc4d1	kvalitní
maso	maso	k1gNnSc4	maso
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
kuřecí	kuřecí	k2eAgNnSc1d1	kuřecí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
panda	panda	k1gFnSc1	panda
červená	červená	k1gFnSc1	červená
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
atraktivní	atraktivní	k2eAgNnSc4d1	atraktivní
zvíře	zvíře	k1gNnSc4	zvíře
<g/>
,	,	kIx,	,
podobné	podobný	k2eAgFnPc4d1	podobná
velikosti	velikost	k1gFnPc4	velikost
jako	jako	k8xC	jako
kočka	kočka	k1gFnSc1	kočka
domácí	domácí	k2eAgFnSc1d1	domácí
<g/>
,	,	kIx,	,
zdála	zdát	k5eAaImAgFnS	zdát
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
být	být	k5eAaImF	být
ideální	ideální	k2eAgFnPc1d1	ideální
jako	jako	k8xS	jako
domácí	domácí	k2eAgMnSc1d1	domácí
mazlíček	mazlíček	k1gMnSc1	mazlíček
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
nikdy	nikdy	k6eAd1	nikdy
nebylo	být	k5eNaImAgNnS	být
zaznamenáno	zaznamenán	k2eAgNnSc1d1	zaznamenáno
široké	široký	k2eAgNnSc1d1	široké
rozšíření	rozšíření	k1gNnSc1	rozšíření
chovu	chov	k1gInSc2	chov
pandy	panda	k1gFnSc2	panda
červené	červená	k1gFnSc2	červená
jako	jako	k8xC	jako
domácího	domácí	k2eAgMnSc2d1	domácí
mazlíčka	mazlíček	k1gMnSc2	mazlíček
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Chov	chov	k1gInSc1	chov
v	v	k7c6	v
Zoo	zoo	k1gFnSc6	zoo
Praha	Praha	k1gFnSc1	Praha
===	===	k?	===
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
panda	panda	k1gFnSc1	panda
červená	červený	k2eAgFnSc1d1	červená
přišla	přijít	k5eAaPmAgFnS	přijít
do	do	k7c2	do
Zoo	zoo	k1gFnSc2	zoo
Praha	Praha	k1gFnSc1	Praha
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1955	[number]	k4	1955
<g/>
.	.	kIx.	.
</s>
<s>
Systematický	systematický	k2eAgInSc1d1	systematický
chov	chov	k1gInSc1	chov
probíhá	probíhat	k5eAaImIp3nS	probíhat
od	od	k7c2	od
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
odchov	odchov	k1gInSc4	odchov
se	se	k3xPyFc4	se
však	však	k9	však
podařil	podařit	k5eAaPmAgInS	podařit
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
(	(	kIx(	(
<g/>
konkrétně	konkrétně	k6eAd1	konkrétně
25	[number]	k4	25
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sameček	sameček	k1gMnSc1	sameček
pojmenovaný	pojmenovaný	k2eAgMnSc1d1	pojmenovaný
Akim	Akim	k1gMnSc1	Akim
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
páru	pára	k1gFnSc4	pára
Pat	pata	k1gFnPc2	pata
(	(	kIx(	(
<g/>
narozen	narozen	k2eAgInSc1d1	narozen
2007	[number]	k4	2007
v	v	k7c6	v
Zoo	zoo	k1gFnSc6	zoo
Ústí	ústí	k1gNnSc2	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
)	)	kIx)	)
a	a	k8xC	a
Maria	Maria	k1gFnSc1	Maria
<g/>
.	.	kIx.	.
</s>
<s>
Maria	Maria	k1gFnSc1	Maria
uhynula	uhynout	k5eAaPmAgFnS	uhynout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
došlo	dojít	k5eAaPmAgNnS	dojít
na	na	k7c6	na
doporučení	doporučení	k1gNnSc6	doporučení
koordinátora	koordinátor	k1gMnSc2	koordinátor
Evropského	evropský	k2eAgInSc2d1	evropský
záchovného	záchovný	k2eAgInSc2d1	záchovný
programu	program	k1gInSc2	program
(	(	kIx(	(
<g/>
EEP	EEP	kA	EEP
<g/>
)	)	kIx)	)
k	k	k7c3	k
transportu	transport	k1gInSc3	transport
nové	nový	k2eAgFnSc2d1	nová
samičky	samička	k1gFnSc2	samička
Wilmy	Wilma	k1gFnSc2	Wilma
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
Zoo	zoo	k1gFnSc2	zoo
Eskilstuna	Eskilstuna	k1gFnSc1	Eskilstuna
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
Patovi	Pata	k1gMnSc3	Pata
a	a	k8xC	a
Wilmě	Wilma	k1gFnSc3	Wilma
se	se	k3xPyFc4	se
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2018	[number]	k4	2018
narodilo	narodit	k5eAaPmAgNnS	narodit
mládě	mládě	k1gNnSc1	mládě
<g/>
,	,	kIx,	,
v	v	k7c6	v
Zoo	zoo	k1gNnSc6	zoo
Praha	Praha	k1gFnSc1	Praha
historicky	historicky	k6eAd1	historicky
druhé	druhý	k4xOgNnSc1	druhý
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
samce	samec	k1gMnSc4	samec
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
slavnostnímu	slavnostní	k2eAgInSc3d1	slavnostní
křtu	křest	k1gInSc3	křest
došlo	dojít	k5eAaPmAgNnS	dojít
2	[number]	k4	2
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2018	[number]	k4	2018
<g/>
;	;	kIx,	;
kmotrem	kmotr	k1gMnSc7	kmotr
mláděte	mládě	k1gNnSc2	mládě
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
herec	herec	k1gMnSc1	herec
Miroslav	Miroslav	k1gMnSc1	Miroslav
Donutil	donutit	k5eAaPmAgMnS	donutit
<g/>
.	.	kIx.	.
</s>
<s>
Dostal	dostat	k5eAaPmAgMnS	dostat
jméno	jméno	k1gNnSc4	jméno
Flin	Flina	k1gFnPc2	Flina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2019	[number]	k4	2019
odjel	odjet	k5eAaPmAgMnS	odjet
mladý	mladý	k2eAgMnSc1d1	mladý
samec	samec	k1gMnSc1	samec
do	do	k7c2	do
německé	německý	k2eAgFnSc2d1	německá
Zoo	zoo	k1gFnSc2	zoo
Hannover	Hannover	k1gInSc1	Hannover
<g/>
.	.	kIx.	.
<g/>
Pandy	panda	k1gFnPc1	panda
obývají	obývat	k5eAaImIp3nP	obývat
výběh	výběh	k1gInSc4	výběh
hned	hned	k6eAd1	hned
za	za	k7c7	za
hlavním	hlavní	k2eAgInSc7d1	hlavní
vchodem	vchod	k1gInSc7	vchod
do	do	k7c2	do
zoo	zoo	k1gFnSc2	zoo
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c6	o
vůbec	vůbec	k9	vůbec
nejstarší	starý	k2eAgFnSc6d3	nejstarší
expozici	expozice	k1gFnSc6	expozice
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
patřila	patřit	k5eAaImAgFnS	patřit
vlkům	vlk	k1gMnPc3	vlk
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
slavné	slavný	k2eAgFnSc2d1	slavná
vlčice	vlčice	k1gFnSc2	vlčice
Lotty	Lotta	k1gFnSc2	Lotta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kultura	kultura	k1gFnSc1	kultura
==	==	k?	==
</s>
</p>
<p>
<s>
Překlad	překlad	k1gInSc1	překlad
čínského	čínský	k2eAgInSc2d1	čínský
názvu	název	k1gInSc2	název
do	do	k7c2	do
angličtiny	angličtina	k1gFnSc2	angličtina
se	se	k3xPyFc4	se
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
stal	stát	k5eAaPmAgInS	stát
podkladem	podklad	k1gInSc7	podklad
pro	pro	k7c4	pro
pojmenování	pojmenování	k1gNnSc4	pojmenování
webového	webový	k2eAgMnSc2d1	webový
prohlížeče	prohlížeč	k1gMnSc2	prohlížeč
Mozilla	Mozilla	k1gFnSc1	Mozilla
Firefox	Firefox	k1gInSc1	Firefox
(	(	kIx(	(
<g/>
firefox	firefox	k1gInSc1	firefox
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
překladu	překlad	k1gInSc6	překlad
"	"	kIx"	"
<g/>
ohnivá	ohnivý	k2eAgFnSc1d1	ohnivá
liška	liška	k1gFnSc1	liška
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
doslovný	doslovný	k2eAgInSc1d1	doslovný
překlad	překlad	k1gInSc1	překlad
čínského	čínský	k2eAgNnSc2d1	čínské
pojmenování	pojmenování	k1gNnSc2	pojmenování
pandy	panda	k1gFnSc2	panda
červené	červený	k2eAgFnSc2d1	červená
hun	hun	k?	hun
ho	on	k3xPp3gMnSc4	on
(	(	kIx(	(
<g/>
chun	chun	k1gNnSc4	chun
cho	cho	k0	cho
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Panda	panda	k1gFnSc1	panda
červená	červený	k2eAgFnSc1d1	červená
byla	být	k5eAaImAgFnS	být
vyobrazena	vyobrazit	k5eAaPmNgFnS	vyobrazit
v	v	k7c6	v
původním	původní	k2eAgInSc6d1	původní
návrhu	návrh	k1gInSc6	návrh
loga	logo	k1gNnSc2	logo
Mozilly	Mozilla	k1gFnSc2	Mozilla
Firefox	Firefox	k1gInSc1	Firefox
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
nejznámějším	známý	k2eAgMnSc7d3	nejznámější
jedincem	jedinec	k1gMnSc7	jedinec
pandy	panda	k1gFnSc2	panda
červené	červená	k1gFnSc2	červená
je	být	k5eAaImIp3nS	být
Babu	baba	k1gFnSc4	baba
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
utekl	utéct	k5eAaPmAgMnS	utéct
ze	z	k7c2	z
zoo	zoo	k1gFnSc2	zoo
v	v	k7c6	v
Birminghamu	Birmingham	k1gInSc6	Birmingham
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
mediální	mediální	k2eAgFnSc7d1	mediální
hvězdou	hvězda	k1gFnSc7	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
státní	státní	k2eAgNnSc4d1	státní
zvíře	zvíře	k1gNnSc4	zvíře
indického	indický	k2eAgInSc2d1	indický
státu	stát	k1gInSc2	stát
Sikkim	Sikkima	k1gFnPc2	Sikkima
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Red	Red	k1gFnSc1	Red
panda	panda	k1gFnSc1	panda
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
KOŘÍNEK	Kořínek	k1gMnSc1	Kořínek
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
<g/>
.	.	kIx.	.
</s>
<s>
Zoologická	zoologický	k2eAgFnSc1d1	zoologická
zahrada	zahrada	k1gFnSc1	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
:	:	kIx,	:
Rubico	Rubico	k1gNnSc1	Rubico
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
326	[number]	k4	326
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85839	[number]	k4	85839
<g/>
-	-	kIx~	-
<g/>
29	[number]	k4	29
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ANDĚRA	ANDĚRA	kA	ANDĚRA
<g/>
,	,	kIx,	,
Miloš	Miloš	k1gMnSc1	Miloš
<g/>
.	.	kIx.	.
</s>
<s>
Svět	svět	k1gInSc1	svět
zvířat	zvíře	k1gNnPc2	zvíře
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Savci	savec	k1gMnPc1	savec
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Albatros	albatros	k1gMnSc1	albatros
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
147	[number]	k4	147
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
677	[number]	k4	677
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ANDĚROVÁ	ANDĚROVÁ	kA	ANDĚROVÁ
<g/>
,	,	kIx,	,
Romana	Romana	k1gFnSc1	Romana
<g/>
.	.	kIx.	.
</s>
<s>
Lexikon	lexikon	k1gInSc4	lexikon
zvířat	zvíře	k1gNnPc2	zvíře
od	od	k7c2	od
A	a	k8xC	a
do	do	k7c2	do
Z.	Z.	kA	Z.
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Zoologická	zoologický	k2eAgFnSc1d1	zoologická
zahrada	zahrada	k1gFnSc1	zahrada
hlavního	hlavní	k2eAgNnSc2d1	hlavní
město	město	k1gNnSc1	město
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
278	[number]	k4	278
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85126	[number]	k4	85126
<g/>
-	-	kIx~	-
<g/>
30	[number]	k4	30
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
CHOUDHURY	CHOUDHURY	kA	CHOUDHURY
<g/>
,	,	kIx,	,
Anwaruddin	Anwaruddin	k2eAgInSc1d1	Anwaruddin
<g/>
.	.	kIx.	.
</s>
<s>
An	An	k?	An
overview	overview	k?	overview
of	of	k?	of
the	the	k?	the
status	status	k1gInSc4	status
and	and	k?	and
conservation	conservation	k1gInSc1	conservation
of	of	k?	of
the	the	k?	the
red	red	k?	red
panda	panda	k1gFnSc1	panda
Ailurus	Ailurus	k1gMnSc1	Ailurus
fulgens	fulgens	k1gInSc1	fulgens
in	in	k?	in
India	indium	k1gNnSc2	indium
<g/>
,	,	kIx,	,
with	with	k1gMnSc1	with
reference	reference	k1gFnSc2	reference
to	ten	k3xDgNnSc4	ten
its	its	k?	its
global	globat	k5eAaImAgMnS	globat
status	status	k1gInSc4	status
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
Oryx	Oryx	k1gInSc1	Oryx
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
DOI	DOI	kA	DOI
<g/>
:	:	kIx,	:
<g/>
10.104	[number]	k4	10.104
<g/>
6	[number]	k4	6
<g/>
/	/	kIx~	/
<g/>
j	j	k?	j
<g/>
.1365	.1365	k4	.1365
<g/>
-	-	kIx~	-
<g/>
3008.2001	[number]	k4	3008.2001
<g/>
.00181	.00181	k4	.00181
<g/>
.	.	kIx.	.
<g/>
x.	x.	k?	x.
</s>
</p>
<p>
<s>
MILES	MILES	kA	MILES
<g/>
,	,	kIx,	,
Roberts	Roberts	k1gInSc1	Roberts
<g/>
;	;	kIx,	;
GITTLEMAN	GITTLEMAN	kA	GITTLEMAN
<g/>
,	,	kIx,	,
J.	J.	kA	J.
L.	L.	kA	L.
Mammalian	Mammaliana	k1gFnPc2	Mammaliana
species	species	k1gFnPc2	species
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
n.	n.	k?	n.
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
</s>
<s>
DOI	DOI	kA	DOI
<g/>
:	:	kIx,	:
<g/>
10.230	[number]	k4	10.230
<g/>
7	[number]	k4	7
<g/>
/	/	kIx~	/
<g/>
3503840	[number]	k4	3503840
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
POCOCK	POCOCK	kA	POCOCK
<g/>
,	,	kIx,	,
Reginald	Reginald	k1gMnSc1	Reginald
Innes	Innes	k1gMnSc1	Innes
<g/>
.	.	kIx.	.
</s>
<s>
Fauna	fauna	k1gFnSc1	fauna
of	of	k?	of
British	British	k1gInSc1	British
India	indium	k1gNnSc2	indium
<g/>
,	,	kIx,	,
including	including	k1gInSc1	including
Ceylon	Ceylon	k1gInSc1	Ceylon
and	and	k?	and
Burma	Burma	k1gFnSc1	Burma
<g/>
.	.	kIx.	.
</s>
<s>
Londýn	Londýn	k1gInSc1	Londýn
<g/>
:	:	kIx,	:
Taylor	Taylor	k1gInSc1	Taylor
and	and	k?	and
Francis	Francis	k1gInSc1	Francis
<g/>
,	,	kIx,	,
1941	[number]	k4	1941
<g/>
.	.	kIx.	.
617	[number]	k4	617
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
9781179240268	[number]	k4	9781179240268
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Ailurus	Ailurus	k1gInSc1	Ailurus
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
panda	panda	k1gFnSc1	panda
červená	červenat	k5eAaImIp3nS	červenat
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Ailurus	Ailurus	k1gInSc1	Ailurus
fulgens	fulgens	k1gInSc1	fulgens
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc4d1	slovníkové
heslo	heslo	k1gNnSc4	heslo
panda	panda	k1gFnSc1	panda
červená	červený	k2eAgFnSc1d1	červená
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Zoo	zoo	k1gFnSc1	zoo
Praha	Praha	k1gFnSc1	Praha
</s>
</p>
<p>
<s>
Zoo	zoo	k1gFnSc1	zoo
Zlín	Zlín	k1gInSc1	Zlín
</s>
</p>
<p>
<s>
Zoo	zoo	k1gFnSc1	zoo
Liberec	Liberec	k1gInSc1	Liberec
</s>
</p>
<p>
<s>
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
o	o	k7c6	o
pandě	panda	k1gFnSc6	panda
červené	červený	k2eAgFnSc2d1	červená
</s>
</p>
<p>
<s>
Panda	panda	k1gFnSc1	panda
červená	červený	k2eAgFnSc1d1	červená
na	na	k7c6	na
Biolibu	Biolib	k1gInSc6	Biolib
</s>
</p>
<p>
<s>
Panda	panda	k1gFnSc1	panda
červená	červenat	k5eAaImIp3nS	červenat
na	na	k7c4	na
Encyclopedia	Encyclopedium	k1gNnPc4	Encyclopedium
of	of	k?	of
Life	Life	k1gNnSc5	Life
</s>
</p>
<p>
<s>
Červený	červený	k2eAgInSc1d1	červený
seznam	seznam	k1gInSc1	seznam
IUCN	IUCN	kA	IUCN
</s>
</p>
<p>
<s>
Článek	článek	k1gInSc1	článek
o	o	k7c6	o
pandě	panda	k1gFnSc6	panda
červené	červený	k2eAgFnSc6d1	červená
na	na	k7c6	na
webu	web	k1gInSc6	web
časopisu	časopis	k1gInSc2	časopis
100	[number]	k4	100
<g/>
+	+	kIx~	+
<g/>
1	[number]	k4	1
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
