<s>
F-Droid	F-Droid	k1gInSc1
</s>
<s>
F-Droid	F-Droid	k1gInSc1
</s>
<s>
Screenshot	Screenshot	k1gInSc1
F-Droid	F-Droid	k1gInSc1
0.100	0.100	k4
<g/>
.1	.1	k4
na	na	k7c6
Androidu	android	k1gInSc6
ukazující	ukazující	k2eAgFnSc2d1
klienta	klient	k1gMnSc4
s	s	k7c7
několika	několik	k4yIc2
nainstalovanými	nainstalovaný	k2eAgFnPc7d1
aplikacemi	aplikace	k1gFnPc7
<g/>
.	.	kIx.
<g/>
Vývojář	vývojář	k1gMnSc1
</s>
<s>
Ciaran	Ciaran	k1gInSc1
Gultnieks	Gultnieks	k1gInSc4
První	první	k4xOgNnPc4
vydání	vydání	k1gNnPc4
</s>
<s>
2010-9-29	2010-9-29	k4
Aktuální	aktuální	k2eAgFnSc1d1
verze	verze	k1gFnSc1
</s>
<s>
1.11	1.11	k4
Operační	operační	k2eAgInSc1d1
systém	systém	k1gInSc1
</s>
<s>
GNU	gnu	k1gNnSc1
<g/>
/	/	kIx~
<g/>
Linux	linux	k1gInSc1
(	(	kIx(
<g/>
server	server	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Android	android	k1gInSc1
(	(	kIx(
<g/>
klient	klient	k1gMnSc1
<g/>
)	)	kIx)
Vyvíjeno	vyvíjen	k2eAgNnSc1d1
v	v	k7c6
</s>
<s>
Python	Python	k1gMnSc1
(	(	kIx(
<g/>
server	server	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
PHP	PHP	kA
(	(	kIx(
<g/>
stránky	stránka	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Java	Java	k1gMnSc1
(	(	kIx(
<g/>
klient	klient	k1gMnSc1
<g/>
)	)	kIx)
Typ	typ	k1gInSc1
softwaru	software	k1gInSc2
</s>
<s>
Digitální	digitální	k2eAgFnSc1d1
distribuce	distribuce	k1gFnSc1
svobodného	svobodný	k2eAgInSc2d1
softwaru	software	k1gInSc2
<g/>
,	,	kIx,
Softwarový	softwarový	k2eAgInSc1d1
repozitář	repozitář	k1gInSc1
Licence	licence	k1gFnSc2
</s>
<s>
GNU	gnu	k1gMnSc1
GPLv	GPLv	k1gMnSc1
<g/>
3	#num#	k4
<g/>
+	+	kIx~
Web	web	k1gInSc1
</s>
<s>
https://f-droid.org/	https://f-droid.org/	k?
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
F-Droid	F-Droid	k1gInSc1
je	být	k5eAaImIp3nS
softwarový	softwarový	k2eAgInSc1d1
repozitář	repozitář	k1gInSc1
(	(	kIx(
<g/>
nebo	nebo	k8xC
„	„	k?
<g/>
app	app	k?
store	stor	k1gInSc5
<g/>
“	“	k?
<g/>
)	)	kIx)
pro	pro	k7c4
operační	operační	k2eAgInSc4d1
systém	systém	k1gInSc4
Android	android	k1gInSc4
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
Google	Google	k1gFnSc1
Play	play	k0
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgInSc1d1
repozitář	repozitář	k1gInSc1
<g/>
,	,	kIx,
poskytovaný	poskytovaný	k2eAgMnSc1d1
vývojáři	vývojář	k1gMnPc1
<g/>
,	,	kIx,
obsahuje	obsahovat	k5eAaImIp3nS
jen	jen	k9
svobodný	svobodný	k2eAgInSc1d1
software	software	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aplikace	aplikace	k1gFnPc1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
prohledávány	prohledávat	k5eAaImNgInP
a	a	k8xC
instalovány	instalovat	k5eAaBmNgInP
na	na	k7c6
webu	web	k1gInSc6
F-Droidu	F-Droid	k1gInSc2
nebo	nebo	k8xC
pomocí	pomocí	k7c2
klientské	klientský	k2eAgFnSc2d1
aplikace	aplikace	k1gFnSc2
bez	bez	k7c2
nutnosti	nutnost	k1gFnSc2
registrace	registrace	k1gFnSc2
<g/>
.	.	kIx.
„	„	k?
<g/>
Anti-vlastnosti	Anti-vlastnost	k1gFnSc2
<g/>
“	“	k?
jako	jako	k8xS,k8xC
reklamy	reklama	k1gFnSc2
<g/>
,	,	kIx,
sledování	sledování	k1gNnSc1
uživatelů	uživatel	k1gMnPc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
závislost	závislost	k1gFnSc4
na	na	k7c6
nesvobodném	svobodný	k2eNgInSc6d1
softwaru	software	k1gInSc6
jsou	být	k5eAaImIp3nP
označeny	označit	k5eAaPmNgInP
v	v	k7c6
popisu	popis	k1gInSc6
aplikací	aplikace	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Webové	webový	k2eAgFnSc2d1
stránky	stránka	k1gFnSc2
nabízejí	nabízet	k5eAaImIp3nP
také	také	k9
zdrojový	zdrojový	k2eAgInSc4d1
kód	kód	k1gInSc4
aplikací	aplikace	k1gFnPc2
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
hostí	hostit	k5eAaImIp3nS
i	i	k9
softwaru	software	k1gInSc6
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
pohání	pohánět	k5eAaImIp3nS
server	server	k1gInSc4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
umožňuje	umožňovat	k5eAaImIp3nS
komukoliv	kdokoliv	k3yInSc3
vytvořit	vytvořit	k5eAaPmF
si	se	k3xPyFc3
vlastní	vlastní	k2eAgInSc4d1
repozitář	repozitář	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
F-Droid	F-Droid	k1gInSc1
byl	být	k5eAaImAgInS
založen	založit	k5eAaPmNgInS
Ciaran	Ciaran	k1gInSc1
Gultnieksem	Gultnieks	k1gInSc7
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klient	klient	k1gMnSc1
byl	být	k5eAaImAgMnS
odvozen	odvodit	k5eAaPmNgMnS
ze	z	k7c2
zdrojového	zdrojový	k2eAgInSc2d1
kódu	kód	k1gInSc2
Aptoide	Aptoid	k1gInSc5
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Projekt	projekt	k1gInSc1
nyní	nyní	k6eAd1
zpravuje	zpravovat	k5eAaImIp3nS
anglická	anglický	k2eAgFnSc1d1
nezisková	ziskový	k2eNgFnSc1d1
organisace	organisace	k1gFnSc1
F-Droid	F-Droid	k1gInSc1
Limited	limited	k2eAgMnPc2d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Replicant	Replicant	k1gInSc1
<g/>
,	,	kIx,
zcela	zcela	k6eAd1
svobodná	svobodný	k2eAgFnSc1d1
verze	verze	k1gFnSc1
operačního	operační	k2eAgInSc2d1
systému	systém	k1gInSc2
Android	android	k1gInSc1
<g/>
,	,	kIx,
používá	používat	k5eAaImIp3nS
F-Droid	F-Droid	k1gInSc4
jako	jako	k8xC,k8xS
svůj	svůj	k3xOyFgInSc4
základní	základní	k2eAgInSc4d1
doporučený	doporučený	k2eAgInSc4d1
repozitář	repozitář	k1gInSc4
aplikací	aplikace	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
The	The	k1gMnSc1
Guardian	Guardian	k1gMnSc1
Project	Project	k1gMnSc1
<g/>
,	,	kIx,
sada	sada	k1gFnSc1
svobodných	svobodný	k2eAgFnPc2d1
a	a	k8xC
bezpečných	bezpečný	k2eAgFnPc2d1
aplikací	aplikace	k1gFnPc2
pro	pro	k7c4
Android	android	k1gInSc4
<g/>
,	,	kIx,
rozběhla	rozběhnout	k5eAaPmAgFnS
vlastní	vlastní	k2eAgInSc4d1
repozitář	repozitář	k1gInSc4
pro	pro	k7c4
F-Droid	F-Droid	k1gInSc4
na	na	k7c6
začátku	začátek	k1gInSc6
roku	rok	k1gInSc2
2012	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
Free	Fre	k1gInSc2
Software	software	k1gInSc1
Foundation	Foundation	k1gInSc1
Europe	Europ	k1gInSc5
uvedlo	uvést	k5eAaPmAgNnS
F-Droid	F-Droid	k1gInSc4
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
kampani	kampaň	k1gFnSc6
Free	Fre	k1gInSc2
Your	Your	k1gInSc4
Android	android	k1gInSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
na	na	k7c4
zvýšení	zvýšení	k1gNnSc4
povědomí	povědomí	k1gNnSc2
o	o	k7c6
ochraně	ochrana	k1gFnSc6
osobních	osobní	k2eAgInPc2d1
údajů	údaj	k1gInPc2
a	a	k8xC
bezpečnostních	bezpečnostní	k2eAgFnPc2d1
rizicích	rizicí	k2eAgFnPc2d1
spojených	spojený	k2eAgFnPc2d1
s	s	k7c7
proprietary	proprietar	k1gInPc4
software	software	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
F-Droid	F-Droid	k1gInSc1
byl	být	k5eAaImAgInS
vybrán	vybrán	k2eAgInSc1d1
GNU	gnu	k1gNnSc2
Projectem	Projecto	k1gNnSc7
během	během	k7c2
jeho	jeho	k3xOp3gNnPc2
30	#num#	k4
<g/>
.	.	kIx.
výročí	výročí	k1gNnSc1
jako	jako	k8xS,k8xC
část	část	k1gFnSc1
iniciativy	iniciativa	k1gFnSc2
GNU	gnu	k1gNnSc1
a	a	k8xC
Day	Day	k1gFnSc1
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
měl	mít	k5eAaImAgInS
podpořit	podpořit	k5eAaPmF
větší	veliký	k2eAgNnSc4d2
využití	využití	k1gNnSc4
svobodného	svobodný	k2eAgInSc2d1
softwaru	software	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
březnu	březen	k1gInSc6
2016	#num#	k4
F-Droid	F-Droid	k1gInSc1
začal	začít	k5eAaPmAgInS
spolupracovat	spolupracovat	k5eAaImF
s	s	k7c7
The	The	k1gFnSc7
Guardian	Guardian	k1gMnSc1
Project	Project	k1gMnSc1
a	a	k8xC
CopperheadOS	CopperheadOS	k1gMnSc1
s	s	k7c7
cílem	cíl	k1gInSc7
vytvořit	vytvořit	k5eAaPmF
„	„	k?
<g/>
řešení	řešení	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
ověřitelně	ověřitelně	k6eAd1
důvěryhodné	důvěryhodný	k2eAgFnPc1d1
od	od	k7c2
operačního	operační	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
,	,	kIx,
přes	přes	k7c4
síťové	síťový	k2eAgFnPc4d1
služby	služba	k1gFnPc4
a	a	k8xC
síť	síť	k1gFnSc4
<g/>
,	,	kIx,
až	až	k9
po	po	k7c6
obchodů	obchod	k1gInPc2
s	s	k7c7
aplikacemi	aplikace	k1gFnPc7
a	a	k8xC
aplikace	aplikace	k1gFnPc1
sami	sám	k3xTgMnPc1
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rozsah	rozsah	k1gInSc1
projektu	projekt	k1gInSc2
</s>
<s>
Databáze	databáze	k1gFnSc1
F-Droid	F-Droid	k1gInSc1
obsahuje	obsahovat	k5eAaImIp3nS
rostoucí	rostoucí	k2eAgInSc1d1
počet	počet	k1gInSc1
více	hodně	k6eAd2
než	než	k8xS
2300	#num#	k4
aplikací	aplikace	k1gFnPc2
<g/>
,	,	kIx,
ve	v	k7c6
srovnání	srovnání	k1gNnSc6
s	s	k7c7
více	hodně	k6eAd2
než	než	k8xS
1,43	1,43	k4
milionu	milion	k4xCgInSc2
na	na	k7c4
Google	Google	k1gNnSc4
Play	play	k0
<g/>
.	.	kIx.
</s>
<s desamb="1">
Projekt	projekt	k1gInSc1
zahrnuje	zahrnovat	k5eAaImIp3nS
několik	několik	k4yIc4
softwarových	softwarový	k2eAgInPc2d1
podprojektů	podprojekt	k1gInPc2
<g/>
:	:	kIx,
</s>
<s>
Klient	klient	k1gMnSc1
pro	pro	k7c4
vyhledávání	vyhledávání	k1gNnSc4
<g/>
,	,	kIx,
stahování	stahování	k1gNnSc4
<g/>
,	,	kIx,
ověřování	ověřování	k1gNnSc4
a	a	k8xC
aktualizaci	aktualizace	k1gFnSc4
aplikací	aplikace	k1gFnPc2
pro	pro	k7c4
Android	android	k1gInSc4
z	z	k7c2
F-Droidu	F-Droid	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
fdroidserver	fdroidserver	k1gInSc1
–	–	k?
nástroj	nástroj	k1gInSc1
pro	pro	k7c4
správu	správa	k1gFnSc4
stávajících	stávající	k2eAgInPc2d1
a	a	k8xC
vytváření	vytváření	k1gNnSc3
nových	nový	k2eAgInPc2d1
repozitářů	repozitář	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
WordPressu	WordPress	k1gInSc6
založený	založený	k2eAgInSc1d1
web	web	k1gInSc1
front	front	k1gInSc1
end	end	k?
k	k	k7c3
repozitáři	repozitář	k1gInSc3
<g/>
.	.	kIx.
</s>
<s>
F-Droid	F-Droid	k1gInSc1
kompiluje	kompilovat	k5eAaImIp3nS
aplikace	aplikace	k1gFnPc4
z	z	k7c2
veřejně	veřejně	k6eAd1
dostupných	dostupný	k2eAgFnPc2d1
a	a	k8xC
svobodně	svobodně	k6eAd1
licencovaných	licencovaný	k2eAgInPc2d1
zdrojových	zdrojový	k2eAgInPc2d1
kódů	kód	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Projekt	projekt	k1gInSc1
je	být	k5eAaImIp3nS
realizován	realizovat	k5eAaBmNgInS
výhradně	výhradně	k6eAd1
dobrovolníky	dobrovolník	k1gMnPc4
a	a	k8xC
nemá	mít	k5eNaImIp3nS
žádný	žádný	k3yNgInSc1
formální	formální	k2eAgInSc1d1
proces	proces	k1gInSc1
kontroly	kontrola	k1gFnSc2
aplikací	aplikace	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
Nové	Nové	k2eAgFnPc1d1
aplikace	aplikace	k1gFnPc1
jsou	být	k5eAaImIp3nP
přidávány	přidávat	k5eAaImNgFnP
na	na	k7c6
základě	základ	k1gInSc6
příspěvků	příspěvek	k1gInPc2
od	od	k7c2
uživatelů	uživatel	k1gMnPc2
nebo	nebo	k8xC
samotných	samotný	k2eAgMnPc2d1
vývojářů	vývojář	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jediným	jediný	k2eAgInSc7d1
požadavkem	požadavek	k1gInSc7
je	být	k5eAaImIp3nS
aby	aby	kYmCp3nP
aplikace	aplikace	k1gFnPc4
byly	být	k5eAaImAgFnP
výhradně	výhradně	k6eAd1
bez	bez	k7c2
proprietárního	proprietární	k2eAgMnSc2d1
software	software	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Klient	klient	k1gMnSc1
</s>
<s>
logo	logo	k1gNnSc4
Get	Get	k1gFnSc2
it	it	k?
on	on	k3xPp3gMnSc1
F-Droid	F-Droid	k1gInSc4
</s>
<s>
Pro	pro	k7c4
nainstalování	nainstalování	k1gNnSc4
klienta	klient	k1gMnSc2
F-Droid	F-Droid	k1gInSc4
musí	muset	k5eAaImIp3nS
uživatel	uživatel	k1gMnSc1
povolit	povolit	k5eAaPmF
instalaci	instalace	k1gFnSc4
z	z	k7c2
„	„	k?
<g/>
Neznámých	známý	k2eNgInPc2d1
zdrojů	zdroj	k1gInPc2
<g/>
“	“	k?
v	v	k7c6
nastavení	nastavení	k1gNnSc6
Androidu	android	k1gInSc2
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
získat	získat	k5eAaPmF
APK	APK	kA
(	(	kIx(
<g/>
instalační	instalační	k2eAgInSc4d1
soubor	soubor	k1gInSc4
<g/>
)	)	kIx)
z	z	k7c2
oficiálních	oficiální	k2eAgFnPc2d1
stránek	stránka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klienta	klient	k1gMnSc4
nelze	lze	k6eNd1
nainstalovat	nainstalovat	k5eAaPmF
skrze	skrze	k?
Google	Google	k1gFnSc2
Play	play	k0
store	stor	k1gInSc5
kvůli	kvůli	k7c3
konkurenční	konkurenční	k2eAgFnSc3d1
doložce	doložka	k1gFnSc3
v	v	k7c6
Google	Googl	k1gInSc6
Play	play	k0
Developer	developer	k1gMnSc1
Distribution	Distribution	k1gInSc4
Agreement	Agreement	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Klient	klient	k1gMnSc1
byl	být	k5eAaImAgMnS
navržen	navrhnout	k5eAaPmNgMnS
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
byl	být	k5eAaImAgInS
odolný	odolný	k2eAgMnSc1d1
proti	proti	k7c3
sledování	sledování	k1gNnSc3
<g/>
,	,	kIx,
cenzuře	cenzura	k1gFnSc3
a	a	k8xC
nedůvěryhodnému	důvěryhodný	k2eNgNnSc3d1
připojení	připojení	k1gNnSc3
k	k	k7c3
Internetu	Internet	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
podpoře	podpora	k1gFnSc3
anonymity	anonymita	k1gFnSc2
podporuje	podporovat	k5eAaImIp3nS
HTTP	HTTP	kA
proxy	prox	k1gInPc4
a	a	k8xC
repozitáře	repozitář	k1gInPc4
hostované	hostovaný	k2eAgInPc4d1
na	na	k7c6
Tor	Tor	k1gMnSc1
hidden	hiddna	k1gFnPc2
services	services	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klientská	klientský	k2eAgNnPc1d1
zařízení	zařízení	k1gNnPc1
mohou	moct	k5eAaImIp3nP
sloužit	sloužit	k5eAaImF
jako	jako	k9
improvizované	improvizovaný	k2eAgInPc4d1
„	„	k?
<g/>
app	app	k?
story	story	k1gFnSc2
<g/>
“	“	k?
a	a	k8xC
distribuovat	distribuovat	k5eAaBmF
stažené	stažený	k2eAgFnPc4d1
aplikace	aplikace	k1gFnPc4
pře	pře	k1gFnSc2
místní	místní	k2eAgFnSc2d1
Wi-Fi	Wi-F	k1gFnSc2
<g/>
,	,	kIx,
Bluetooth	Bluetooth	k1gInSc4
<g/>
,	,	kIx,
nebo	nebo	k8xC
Android	android	k1gInSc1
Beam	Beama	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
Klient	klient	k1gMnSc1
F-Droid	F-Droid	k1gInSc4
automaticky	automaticky	k6eAd1
nabízí	nabízet	k5eAaImIp3nS
aktualizace	aktualizace	k1gFnSc1
aplikací	aplikace	k1gFnPc2
přes	přes	k7c4
něj	on	k3xPp3gMnSc4
nainstalovaných	nainstalovaný	k2eAgMnPc6d1
<g/>
.	.	kIx.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1
repozitář	repozitář	k1gInSc1
F-Droid	F-Droid	k1gInSc1
používá	používat	k5eAaImIp3nS
k	k	k7c3
podepisování	podepisování	k1gNnSc3
aplikací	aplikace	k1gFnPc2
svůj	svůj	k3xOyFgInSc4
vlastní	vlastní	k2eAgInSc4d1
klíč	klíč	k1gInSc4
<g/>
,	,	kIx,
takže	takže	k8xS
aplikace	aplikace	k1gFnPc4
naiunstalované	naiunstalovaný	k2eAgFnPc4d1
z	z	k7c2
jiných	jiný	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
muesjí	muesjet	k5eAaImIp3nS,k5eAaPmIp3nS
být	být	k5eAaImF
nejdříve	dříve	k6eAd3
přeinstalovány	přeinstalován	k2eAgFnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kritika	kritika	k1gFnSc1
</s>
<s>
F-Droid	F-Droid	k1gInSc1
byl	být	k5eAaImAgMnS
kritizován	kritizovat	k5eAaImNgMnS
pro	pro	k7c4
distribuci	distribuce	k1gFnSc4
zastaralých	zastaralý	k2eAgFnPc2d1
verzí	verze	k1gFnPc2
oficiálních	oficiální	k2eAgFnPc2d1
aplikací	aplikace	k1gFnPc2
a	a	k8xC
za	za	k7c4
svůj	svůj	k3xOyFgInSc4
přístup	přístup	k1gInSc4
k	k	k7c3
podepisování	podepisování	k1gNnSc3
aplikací	aplikace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Zastaralé	zastaralý	k2eAgFnPc1d1
verze	verze	k1gFnPc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
bezpečnostní	bezpečnostní	k2eAgMnPc1d1
výzkumník	výzkumník	k1gMnSc1
a	a	k8xC
vývojář	vývojář	k1gMnSc1
Moxie	Moxie	k1gFnSc2
Marlinspike	Marlinspik	k1gFnSc2
kritizoval	kritizovat	k5eAaImAgInS
F-Droid	F-Droid	k1gInSc1
za	za	k7c4
distribuci	distribuce	k1gFnSc4
zastaralé	zastaralý	k2eAgFnSc2d1
verze	verze	k1gFnSc2
TextSecure	TextSecur	k1gMnSc5
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
obsahovala	obsahovat	k5eAaImAgFnS
známou	známý	k2eAgFnSc4d1
chybu	chyba	k1gFnSc4
opravenou	opravený	k2eAgFnSc4d1
v	v	k7c6
oficiální	oficiální	k2eAgFnSc6d1
verzi	verze	k1gFnSc6
na	na	k7c6
Google	Googla	k1gFnSc6
Play	play	k0
<g/>
.	.	kIx.
</s>
<s desamb="1">
F-Droid	F-Droid	k1gInSc1
aplikaci	aplikace	k1gFnSc4
na	na	k7c6
základě	základ	k1gInSc6
žádosti	žádost	k1gFnSc2
Marlinspikse	Marlinspikse	k1gFnSc2
odstranil	odstranit	k5eAaPmAgInS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
Marlinspikes	Marlinspikes	k1gInSc1
<g/>
,	,	kIx,
poté	poté	k6eAd1
co	co	k3yRnSc4,k3yInSc4,k3yQnSc4
obdržel	obdržet	k5eAaPmAgInS
e-mail	e-mail	k1gInSc1
od	od	k7c2
uživatelů	uživatel	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
byli	být	k5eAaImAgMnP
uvedeni	uvést	k5eAaPmNgMnP
v	v	k7c6
omyl	omyl	k1gInSc1
oznámením	oznámení	k1gNnSc7
F-Droidu	F-Droid	k1gInSc2
<g/>
,	,	kIx,
kritizoval	kritizovat	k5eAaImAgMnS
způsob	způsob	k1gInSc4
<g/>
,	,	kIx,
jakým	jaký	k3yIgInSc7,k3yRgInSc7,k3yQgInSc7
projekt	projekt	k1gInSc1
k	k	k7c3
problému	problém	k1gInSc3
přistoupil	přistoupit	k5eAaPmAgMnS
<g/>
,	,	kIx,
prohlásil	prohlásit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
projekt	projekt	k1gInSc1
„	„	k?
<g/>
nesprávně	správně	k6eNd1
popsal	popsat	k5eAaPmAgMnS
rozsah	rozsah	k1gInSc4
chyby	chyba	k1gFnSc2
<g/>
“	“	k?
a	a	k8xC
byl	být	k5eAaImAgInS
„	„	k?
<g/>
neuvěřitelně	uvěřitelně	k6eNd1
nezralý	zralý	k2eNgInSc4d1
<g/>
“	“	k?
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
zprávě	zpráva	k1gFnSc6
oznamující	oznamující	k2eAgNnSc1d1
odstranění	odstranění	k1gNnSc1
aplikace	aplikace	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Správa	správa	k1gFnSc1
klíčů	klíč	k1gInPc2
</s>
<s>
Marlinspike	Marlinspike	k1gInSc1
byl	být	k5eAaImAgInS
k	k	k7c3
F-Droidu	F-Droid	k1gInSc3
kritický	kritický	k2eAgInSc1d1
také	také	k9
kvůli	kvůli	k7c3
přístupu	přístup	k1gInSc3
k	k	k7c3
podepisování	podepisování	k1gNnSc3
aplikací	aplikace	k1gFnPc2
v	v	k7c6
hlavním	hlavní	k2eAgInSc6d1
repozitáři	repozitář	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
Aplikace	aplikace	k1gFnPc4
distribuované	distribuovaný	k2eAgFnPc4d1
skrze	skrze	k?
Google	Google	k1gFnPc4
Play	play	k0
jsou	být	k5eAaImIp3nP
podepsané	podepsaný	k2eAgFnSc3d1
vývojáři	vývojář	k1gMnPc1
aplikace	aplikace	k1gFnPc4
a	a	k8xC
Android	android	k1gInSc4
kontroluje	kontrolovat	k5eAaImIp3nS
<g/>
,	,	kIx,
zda	zda	k8xS
jsou	být	k5eAaImIp3nP
aplikace	aplikace	k1gFnPc4
při	při	k7c6
aktualizaci	aktualizace	k1gFnSc6
podepsány	podepsán	k2eAgMnPc4d1
stejným	stejný	k2eAgInSc7d1
klíčem	klíč	k1gInSc7
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
zabraňuje	zabraňovat	k5eAaImIp3nS
ostatním	ostatní	k1gNnSc7
šířit	šířit	k5eAaImF
aktualizace	aktualizace	k1gFnPc4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
které	který	k3yIgInPc4,k3yRgInPc4,k3yQgInPc4
vývojář	vývojář	k1gMnSc1
nepodepsal	podepsat	k5eNaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
F-Droid	F-Droid	k1gInSc1
rozvrací	rozvracet	k5eAaImIp3nS
tento	tento	k3xDgInSc1
model	model	k1gInSc1
zabezpečení	zabezpečení	k1gNnSc2
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
sestavuje	sestavovat	k5eAaImIp3nS
a	a	k8xC
podepisuje	podepisovat	k5eAaImIp3nS
balíčky	balíček	k1gInPc4
sám	sám	k3xTgMnSc1
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
správce	správce	k1gMnSc1
nebo	nebo	k8xC
někdo	někdo	k3yInSc1
jiný	jiný	k2eAgMnSc1d1
s	s	k7c7
přístupem	přístup	k1gInSc7
k	k	k7c3
F-Droidu	F-Droid	k1gInSc3
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
nucen	nutit	k5eAaImNgMnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
vydal	vydat	k5eAaPmAgMnS
škodlivé	škodlivý	k2eAgFnPc4d1
aktualizace	aktualizace	k1gFnPc4
pro	pro	k7c4
libovolnou	libovolný	k2eAgFnSc4d1
aplikaci	aplikace	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
F-Droid	F-Droid	k1gInSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Client	Client	k1gMnSc1
0.54	0.54	k4
released	released	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
F-droid	F-droid	k1gInSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
<g/>
,	,	kIx,
5	#num#	k4
November	November	k1gInSc1
2013	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
26	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
2015	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
F-Droid	F-Droid	k1gInSc1
is	is	k?
the	the	k?
FOSS	FOSS	kA
application	application	k1gInSc1
store	stor	k1gInSc5
for	forum	k1gNnPc2
your	your	k1gInSc4
Android	android	k1gInSc1
phone	phonout	k5eAaImIp3nS,k5eAaPmIp3nS
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
27	#num#	k4
November	November	k1gInSc1
2012	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Tom	Tom	k1gMnSc1
Nardi	Nard	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
F-Droid	F-Droid	k1gInSc1
<g/>
:	:	kIx,
The	The	k1gFnSc1
Android	android	k1gInSc4
Market	market	k1gInSc1
That	Thata	k1gFnPc2
Respects	Respects	k1gInSc1
Your	Your	k1gMnSc1
Rights	Rights	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
August	August	k1gMnSc1
27	#num#	k4
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
3	#num#	k4
December	December	k1gInSc1
2013	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
F-Droid	F-Droid	k1gInSc1
Server	server	k1gInSc1
Manual	Manual	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
F-Droid	F-Droid	k1gInSc1
initial	initial	k1gMnSc1
source	source	k1gMnSc1
code	codat	k5eAaPmIp3nS
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
19	#num#	k4
October	October	k1gInSc1
2010	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
10	#num#	k4
December	December	k1gInSc1
2014	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
F	F	kA
Droid	Droid	k1gInSc1
About	About	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
FDroid	FDroid	k1gInSc1
<g/>
:	:	kIx,
a	a	k8xC
free	freat	k5eAaPmIp3nS
software	software	k1gInSc1
alternative	alternativ	k1gInSc5
to	ten	k3xDgNnSc4
Google	Google	k1gNnSc4
Market	market	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Replicant	Replicant	k1gMnSc1
Project	Project	k1gMnSc1
<g/>
,	,	kIx,
26	#num#	k4
November	November	k1gInSc1
2010	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
FDroid	FDroid	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Our	Our	k1gMnSc1
New	New	k1gMnSc1
F-Droid	F-Droid	k1gInSc4
App	App	k1gFnSc2
Repository	Repositor	k1gInPc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Guardian	Guardian	k1gMnSc1
Project	Project	k1gMnSc1
<g/>
,	,	kIx,
2012-03-15	2012-03-15	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
WALKER-MORGAN	WALKER-MORGAN	k1gMnSc1
<g/>
,	,	kIx,
Dj	Dj	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
FSFE	FSFE	kA
launches	launchesa	k1gFnPc2
"	"	kIx"
<g/>
Free	Free	k1gFnSc1
Your	Your	k1gMnSc1
Android	android	k1gInSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
"	"	kIx"
campaign	campaign	k1gInSc1
<g/>
.	.	kIx.
www.h-online.com	www.h-online.com	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
H-online	H-onlin	k1gInSc5
<g/>
,	,	kIx,
28	#num#	k4
February	Februara	k1gFnSc2
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
27	#num#	k4
July	Jula	k1gFnSc2
2014	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Liberate	Liberat	k1gInSc5
Your	Your	k1gInSc1
Device	device	k1gInSc6
<g/>
!	!	kIx.
</s>
<s desamb="1">
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Free	Fre	k1gInSc2
Software	software	k1gInSc1
Foundation	Foundation	k1gInSc1
Europe	Europ	k1gInSc5
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
GNU-a-Day	GNU-a-Daa	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
GNU	gnu	k1gMnSc1
Project	Project	k1gMnSc1
<g/>
,	,	kIx,
Free	Free	k1gFnSc1
Software	software	k1gInSc1
Foundation	Foundation	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Copperhead	Copperhead	k1gInSc1
<g/>
,	,	kIx,
Guardian	Guardian	k1gInSc1
Project	Project	k2eAgInSc1d1
and	and	k?
F-Droid	F-Droid	k1gInSc1
Partner	partner	k1gMnSc1
to	ten	k3xDgNnSc4
Build	Build	k1gMnSc1
Open	Open	k1gMnSc1
<g/>
,	,	kIx,
Verifiably	Verifiably	k1gMnSc5
Secure	Secur	k1gMnSc5
Mobile	mobile	k1gNnSc2
Ecosystem	Ecosyst	k1gInSc7
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
CopperheadOS	CopperheadOS	k1gMnPc1
wants	wants	k6eAd1
to	ten	k3xDgNnSc4
bring	bring	k1gMnSc1
better	better	k1gMnSc1
security	securita	k1gFnSc2
to	ten	k3xDgNnSc4
Android	android	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Contribute	Contribut	k1gMnSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Inclusion	Inclusion	k1gInSc1
Policy	Polica	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
4	#num#	k4
April	April	k1gInSc1
2014	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Android	android	k1gInSc1
Open	Open	k1gMnSc1
Distribution	Distribution	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2012-10-31	2012-10-31	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Google	Google	k1gInSc1
Play	play	k0
Developer	developer	k1gMnSc1
Distribution	Distribution	k1gInSc4
Agreement	Agreement	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2012-10-31	2012-10-31	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Client	Client	k1gMnSc1
0.76	0.76	k4
Released	Released	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
14	#num#	k4
October	October	k1gInSc1
2014	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Russell	Russell	k1gInSc1
Brandom	Brandom	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Your	Your	k1gInSc1
survival	survivat	k5eAaPmAgInS,k5eAaBmAgInS,k5eAaImAgInS
guide	guide	k6eAd1
for	forum	k1gNnPc2
an	an	k?
internet	internet	k1gInSc4
blackout	blackout	k5eAaPmF,k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Verge	Verge	k1gFnSc1
<g/>
.	.	kIx.
10	#num#	k4
June	jun	k1gMnSc5
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2	#num#	k4
August	August	k1gMnSc1
2014	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Release	Releasa	k1gFnSc3
Channels	Channelsa	k1gFnPc2
and	and	k?
Signing	Signing	k1gInSc1
Keys	Keys	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
12	#num#	k4
August	August	k1gMnSc1
2014	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Security	Securita	k1gFnSc2
Notice	Notice	k?
–	–	k?
TextSecure	TextSecur	k1gMnSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
F-Droid	F-Droid	k1gInSc1
<g/>
,	,	kIx,
2012-08-23	2012-08-23	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Moxie	Moxius	k1gMnPc4
Marlinspike	Marlinspik	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
SMS	SMS	kA
Plain	Plain	k2eAgInSc4d1
text	text	k1gInSc4
leak	leak	k6eAd1
via	via	k7c4
LogCat	LogCat	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2012-08-24	2012-08-24	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
3	#num#	k4
moxie	moxie	k1gFnSc1
<g/>
0	#num#	k4
commented	commented	k1gMnSc1
Feb	Feb	k1gMnSc1
12	#num#	k4
<g/>
,	,	kIx,
2013	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Signing	Signing	k1gInSc1
Your	Your	k1gMnSc1
Applications	Applications	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Google	Google	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
AMADEO	AMADEO	kA
<g/>
,	,	kIx,
Ron	ron	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnPc2
great	great	k2eAgMnSc1d1
Ars	Ars	k1gMnSc1
experiment	experiment	k1gInSc1
<g/>
—	—	k?
<g/>
free	free	k1gInSc1
and	and	k?
open	open	k1gInSc1
source	source	k1gMnSc1
software	software	k1gInSc1
on	on	k3xPp3gMnSc1
a	a	k8xC
smartphone	smartphon	k1gInSc5
<g/>
?!	?!	k?
<g/>
.	.	kIx.
arstechnica	arstechnica	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ars	Ars	k1gMnSc1
Technica	Technica	k1gMnSc1
<g/>
,	,	kIx,
29	#num#	k4
July	Jula	k1gFnSc2
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
29	#num#	k4
July	Jula	k1gFnSc2
2014	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
F-Droid	F-Droid	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
Klient	klient	k1gMnSc1
v	v	k7c6
repozitáři	repozitář	k1gInSc6
F-Droid	F-Droid	k1gInSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Svobodný	svobodný	k2eAgInSc1d1
software	software	k1gInSc1
</s>
