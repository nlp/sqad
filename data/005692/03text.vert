<s>
Kipu	Kipu	k6eAd1	Kipu
je	být	k5eAaImIp3nS	být
uzlové	uzlový	k2eAgNnSc4d1	uzlové
písmo	písmo	k1gNnSc4	písmo
Inků	Ink	k1gMnPc2	Ink
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
mělo	mít	k5eAaImAgNnS	mít
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
obsahem	obsah	k1gInSc7	obsah
jsou	být	k5eAaImIp3nP	být
pouze	pouze	k6eAd1	pouze
statistické	statistický	k2eAgInPc1d1	statistický
záznamy	záznam	k1gInPc1	záznam
<g/>
.	.	kIx.	.
</s>
<s>
Nově	nově	k6eAd1	nově
se	se	k3xPyFc4	se
ale	ale	k9	ale
zjišťuje	zjišťovat	k5eAaImIp3nS	zjišťovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgInPc1	tento
záznamy	záznam	k1gInPc1	záznam
byly	být	k5eAaImAgInP	být
pouze	pouze	k6eAd1	pouze
výchozí	výchozí	k2eAgFnSc4d1	výchozí
formou	forma	k1gFnSc7	forma
a	a	k8xC	a
písmo	písmo	k1gNnSc4	písmo
vývojem	vývoj	k1gInSc7	vývoj
sloužilo	sloužit	k5eAaImAgNnS	sloužit
i	i	k9	i
pro	pro	k7c4	pro
záznam	záznam	k1gInSc4	záznam
textů	text	k1gInPc2	text
<g/>
.	.	kIx.	.
</s>
<s>
William	William	k1gInSc1	William
J.	J.	kA	J.
Conklin	Conklin	k1gInSc1	Conklin
z	z	k7c2	z
washingtonského	washingtonský	k2eAgInSc2d1	washingtonský
Textile	textil	k1gInSc5	textil
Museum	museum	k1gNnSc1	museum
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
začal	začít	k5eAaPmAgMnS	začít
zabývat	zabývat	k5eAaImF	zabývat
kipu	kipa	k1gFnSc4	kipa
<g/>
,	,	kIx,	,
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejde	jít	k5eNaImIp3nS	jít
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
uzly	uzel	k1gInPc4	uzel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
o	o	k7c4	o
rozdíly	rozdíl	k1gInPc4	rozdíl
ve	v	k7c6	v
spřádání	spřádání	k1gNnSc6	spřádání
šňůr	šňůra	k1gFnPc2	šňůra
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc2	jejich
barvení	barvení	k1gNnSc2	barvení
<g/>
.	.	kIx.	.
</s>
<s>
Gary	Gara	k1gFnPc1	Gara
Urton	Urtona	k1gFnPc2	Urtona
dále	daleko	k6eAd2	daleko
vypracoval	vypracovat	k5eAaPmAgMnS	vypracovat
teorii	teorie	k1gFnSc4	teorie
<g/>
,	,	kIx,	,
že	že	k8xS	že
kipu	kipu	k6eAd1	kipu
je	být	k5eAaImIp3nS	být
záznamem	záznam	k1gInSc7	záznam
v	v	k7c6	v
binárním	binární	k2eAgInSc6d1	binární
kódu	kód	k1gInSc6	kód
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
identifikovat	identifikovat	k5eAaBmF	identifikovat
první	první	k4xOgInSc4	první
nenumerický	numerický	k2eNgInSc4d1	nenumerický
element	element	k1gInSc4	element
Puruchuco	Puruchuco	k6eAd1	Puruchuco
jako	jako	k8xC	jako
název	název	k1gInSc1	název
města	město	k1gNnSc2	město
blízko	blízko	k7c2	blízko
Limy	Lima	k1gFnSc2	Lima
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Kipu	Kipus	k1gInSc2	Kipus
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
The	The	k1gMnSc3	The
Khipu	Khipa	k1gFnSc4	Khipa
Database	Databasa	k1gFnSc3	Databasa
Project	Projecta	k1gFnPc2	Projecta
at	at	k?	at
Harvard	Harvard	k1gInSc1	Harvard
University	universita	k1gFnSc2	universita
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Quipu	Quip	k1gInSc2	Quip
<g/>
,	,	kIx,	,
an	an	k?	an
Incan	Incan	k1gInSc1	Incan
Data	datum	k1gNnSc2	datum
Structure	Structur	k1gMnSc5	Structur
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Quipu	Quip	k1gInSc2	Quip
<g/>
:	:	kIx,	:
A	a	k9	a
Modern	Modern	k1gInSc1	Modern
Mystery	Myster	k1gInPc1	Myster
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
University	universita	k1gFnPc1	universita
of	of	k?	of
Wisconsin	Wisconsin	k1gInSc1	Wisconsin
<g/>
,	,	kIx,	,
Department	department	k1gInSc1	department
of	of	k?	of
Anthropology	Anthropolog	k1gMnPc7	Anthropolog
-	-	kIx~	-
How	How	k1gFnPc7	How
do	do	k7c2	do
quipus	quipus	k1gInSc1	quipus
record	record	k1gMnSc1	record
information	information	k1gInSc1	information
<g/>
?	?	kIx.	?
</s>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Geometry	geometr	k1gInPc1	geometr
from	froma	k1gFnPc2	froma
the	the	k?	the
land	land	k1gMnSc1	land
of	of	k?	of
the	the	k?	the
Incas	Incas	k1gInSc1	Incas
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Experts	Expertsa	k1gFnPc2	Expertsa
'	'	kIx"	'
<g/>
decipher	deciphra	k1gFnPc2	deciphra
<g/>
'	'	kIx"	'
Inca	Inca	k1gFnSc1	Inca
strings	strings	k1gInSc1	strings
-	-	kIx~	-
BBC	BBC	kA	BBC
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Peruvian	Peruvian	k1gInSc1	Peruvian
'	'	kIx"	'
<g/>
writing	writing	k1gInSc1	writing
<g/>
'	'	kIx"	'
system	syst	k1gInSc7	syst
goes	goes	k6eAd1	goes
back	back	k6eAd1	back
5,000	[number]	k4	5,000
years	yearsa	k1gFnPc2	yearsa
-	-	kIx~	-
MSNBC	MSNBC	kA	MSNBC
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Kipu	Kip	k2eAgFnSc4d1	Kipa
</s>
