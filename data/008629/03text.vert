<p>
<s>
Australský	australský	k2eAgMnSc1d1	australský
teriér	teriér	k1gMnSc1	teriér
byl	být	k5eAaImAgMnS	být
používán	používat	k5eAaImNgMnS	používat
jako	jako	k8xC	jako
lovec	lovec	k1gMnSc1	lovec
škodné	škodná	k1gFnSc2	škodná
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
se	se	k3xPyFc4	se
chová	chovat	k5eAaImIp3nS	chovat
především	především	k9	především
jako	jako	k9	jako
společenské	společenský	k2eAgNnSc4d1	společenské
plemeno	plemeno	k1gNnSc4	plemeno
psa	pes	k1gMnSc2	pes
<g/>
.	.	kIx.	.
</s>
<s>
Chová	chovat	k5eAaImIp3nS	chovat
se	se	k3xPyFc4	se
téměř	téměř	k6eAd1	téměř
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
anglicky	anglicky	k6eAd1	anglicky
mluvících	mluvící	k2eAgFnPc6d1	mluvící
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
nejhojnější	hojný	k2eAgInSc1d3	nejhojnější
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
toto	tento	k3xDgNnSc1	tento
plemeno	plemeno	k1gNnSc1	plemeno
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c6	na
blízkém	blízký	k2eAgInSc6d1	blízký
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Australský	australský	k2eAgMnSc1d1	australský
teriér	teriér	k1gMnSc1	teriér
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
nové	nový	k2eAgNnSc1d1	nové
plemeno	plemeno	k1gNnSc1	plemeno
vyšlechtěné	vyšlechtěný	k2eAgNnSc1d1	vyšlechtěné
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Vyšlechtilo	vyšlechtit	k5eAaPmAgNnS	vyšlechtit
se	se	k3xPyFc4	se
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
náhodným	náhodný	k2eAgNnSc7d1	náhodné
křížením	křížení	k1gNnSc7	křížení
bez	bez	k7c2	bez
zásahu	zásah	k1gInSc2	zásah
člověka	člověk	k1gMnSc2	člověk
–	–	k?	–
proto	proto	k8xC	proto
nejsou	být	k5eNaImIp3nP	být
žádné	žádný	k3yNgInPc1	žádný
dochované	dochovaný	k2eAgInPc1d1	dochovaný
záznamy	záznam	k1gInPc1	záznam
o	o	k7c6	o
jeho	jeho	k3xOp3gInSc6	jeho
vývoji	vývoj	k1gInSc6	vývoj
nebo	nebo	k8xC	nebo
snad	snad	k9	snad
předcích	předek	k1gInPc6	předek
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
společných	společný	k2eAgInPc2d1	společný
genů	gen	k1gInPc2	gen
se	se	k3xPyFc4	se
za	za	k7c4	za
ně	on	k3xPp3gInPc4	on
ale	ale	k8xC	ale
dá	dát	k5eAaPmIp3nS	dát
považovat	považovat	k5eAaImF	považovat
black	black	k1gInSc4	black
and	and	k?	and
tan	tan	k?	tan
teriér	teriér	k1gMnSc1	teriér
<g/>
,	,	kIx,	,
sky	sky	k?	sky
teriér	teriér	k1gMnSc1	teriér
a	a	k8xC	a
nejspíše	nejspíše	k9	nejspíše
i	i	k9	i
jorkšírský	jorkšírský	k2eAgMnSc1d1	jorkšírský
teriér	teriér	k1gMnSc1	teriér
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
tomuto	tento	k3xDgNnSc3	tento
velmi	velmi	k6eAd1	velmi
podobný	podobný	k2eAgInSc1d1	podobný
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
všech	všecek	k3xTgInPc6	všecek
svých	svůj	k3xOyFgInPc6	svůj
předcích	předek	k1gInPc6	předek
zdědil	zdědit	k5eAaPmAgInS	zdědit
správnou	správný	k2eAgFnSc4d1	správná
"	"	kIx"	"
<g/>
teriérskou	teriérský	k2eAgFnSc4d1	teriérská
<g/>
"	"	kIx"	"
povahu	povaha	k1gFnSc4	povaha
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
výborným	výborný	k2eAgInSc7d1	výborný
příkladem	příklad	k1gInSc7	příklad
teriérů	teriér	k1gMnPc2	teriér
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1870	[number]	k4	1870
se	se	k3xPyFc4	se
už	už	k6eAd1	už
jako	jako	k8xS	jako
poměrně	poměrně	k6eAd1	poměrně
běžný	běžný	k2eAgInSc4d1	běžný
typ	typ	k1gInSc4	typ
objevovalo	objevovat	k5eAaImAgNnS	objevovat
na	na	k7c6	na
výstavách	výstava	k1gFnPc6	výstava
ještě	ještě	k9	ještě
jako	jako	k9	jako
"	"	kIx"	"
<g/>
broken-haired	brokenaired	k1gMnSc1	broken-haired
teriér	teriér	k1gMnSc1	teriér
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
"	"	kIx"	"
<g/>
hrubosrstý	hrubosrstý	k2eAgMnSc1d1	hrubosrstý
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
australský	australský	k2eAgMnSc1d1	australský
teriér	teriér	k1gMnSc1	teriér
je	být	k5eAaImIp3nS	být
vystavován	vystavovat	k5eAaImNgMnS	vystavovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1899	[number]	k4	1899
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
standard	standard	k1gInSc1	standard
byl	být	k5eAaImAgInS	být
ustaven	ustavit	k5eAaPmNgInS	ustavit
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1896	[number]	k4	1896
o	o	k7c4	o
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
dříve	dříve	k6eAd2	dříve
již	již	k6eAd1	již
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
první	první	k4xOgInSc1	první
chovatelský	chovatelský	k2eAgInSc1d1	chovatelský
klub	klub	k1gInSc1	klub
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1933	[number]	k4	1933
byl	být	k5eAaImAgInS	být
zapsán	zapsat	k5eAaPmNgInS	zapsat
jako	jako	k8xC	jako
samostatné	samostatný	k2eAgNnSc1d1	samostatné
plemeno	plemeno	k1gNnSc1	plemeno
i	i	k9	i
v	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
Kennel	Kennel	k1gInSc1	Kennel
Clubu	club	k1gInSc6	club
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2014	[number]	k4	2014
<g/>
:	:	kIx,	:
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
se	se	k3xPyFc4	se
toto	tento	k3xDgNnSc1	tento
plemeno	plemeno	k1gNnSc1	plemeno
ještě	ještě	k9	ještě
moc	moc	k6eAd1	moc
neuchytilo	uchytit	k5eNaPmAgNnS	uchytit
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
lidí	člověk	k1gMnPc2	člověk
jej	on	k3xPp3gMnSc4	on
nezná	neznat	k5eAaImIp3nS	neznat
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
několik	několik	k4yIc1	několik
chovatelských	chovatelský	k2eAgFnPc2d1	chovatelská
stanic	stanice	k1gFnPc2	stanice
zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
spočítali	spočítat	k5eAaPmAgMnP	spočítat
byste	by	kYmCp2nP	by
je	on	k3xPp3gNnPc4	on
jen	jen	k6eAd1	jen
na	na	k7c6	na
prstech	prst	k1gInPc6	prst
jedné	jeden	k4xCgFnSc2	jeden
ruky	ruka	k1gFnSc2	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
toto	tento	k3xDgNnSc4	tento
plemeno	plemeno	k1gNnSc4	plemeno
není	být	k5eNaImIp3nS	být
moc	moc	k6eAd1	moc
oblíbené	oblíbený	k2eAgNnSc1d1	oblíbené
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
západní	západní	k2eAgFnSc6d1	západní
Evropě	Evropa	k1gFnSc6	Evropa
již	již	k6eAd1	již
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
zemi	zem	k1gFnSc6	zem
je	být	k5eAaImIp3nS	být
několik	několik	k4yIc1	několik
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vzhled	vzhled	k1gInSc4	vzhled
==	==	k?	==
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
kompaktně	kompaktně	k6eAd1	kompaktně
stavěný	stavěný	k2eAgMnSc1d1	stavěný
pes	pes	k1gMnSc1	pes
<g/>
,	,	kIx,	,
lehké	lehký	k2eAgFnPc1d1	lehká
konstrukce	konstrukce	k1gFnPc1	konstrukce
s	s	k7c7	s
jemným	jemný	k2eAgInSc7d1	jemný
vzhledem	vzhled	k1gInSc7	vzhled
ale	ale	k8xC	ale
povahou	povaha	k1gFnSc7	povaha
pravého	pravý	k2eAgMnSc4d1	pravý
teriéra	teriér	k1gMnSc4	teriér
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
obdélníkovitého	obdélníkovitý	k2eAgInSc2d1	obdélníkovitý
formátu	formát	k1gInSc2	formát
<g/>
.	.	kIx.	.
</s>
<s>
Srst	srst	k1gFnSc1	srst
je	být	k5eAaImIp3nS	být
kratší	krátký	k2eAgFnSc1d2	kratší
<g/>
,	,	kIx,	,
na	na	k7c4	na
dotek	dotek	k1gInSc4	dotek
hrubá	hrubý	k2eAgFnSc1d1	hrubá
a	a	k8xC	a
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
celé	celý	k2eAgNnSc4d1	celé
tělo	tělo	k1gNnSc4	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Nevytváří	vytvářet	k5eNaImIp3nS	vytvářet
na	na	k7c6	na
krku	krk	k1gInSc6	krk
límec	límec	k1gInSc1	límec
ani	ani	k8xC	ani
prapor	prapor	k1gInSc1	prapor
na	na	k7c6	na
ocasu	ocas	k1gInSc6	ocas
<g/>
.	.	kIx.	.
</s>
<s>
Hlava	hlava	k1gFnSc1	hlava
je	být	k5eAaImIp3nS	být
plochá	plochý	k2eAgFnSc1d1	plochá
<g/>
,	,	kIx,	,
suchá	suchý	k2eAgFnSc1d1	suchá
a	a	k8xC	a
jemnými	jemný	k2eAgFnPc7d1	jemná
liniemi	linie	k1gFnPc7	linie
<g/>
.	.	kIx.	.
</s>
<s>
Vyvážená	vyvážený	k2eAgFnSc1d1	vyvážená
<g/>
.	.	kIx.	.
</s>
<s>
Čumák	čumák	k1gInSc1	čumák
je	být	k5eAaImIp3nS	být
krátký	krátký	k2eAgInSc1d1	krátký
a	a	k8xC	a
úzký	úzký	k2eAgInSc1d1	úzký
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
malé	malý	k2eAgNnSc1d1	malé
tmavě	tmavě	k6eAd1	tmavě
hnědé	hnědý	k2eAgNnSc4d1	hnědé
oči	oko	k1gNnPc4	oko
<g/>
,	,	kIx,	,
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
plochou	plochý	k2eAgFnSc4d1	plochá
lebku	lebka	k1gFnSc4	lebka
<g/>
.	.	kIx.	.
</s>
<s>
Uši	ucho	k1gNnPc1	ucho
jsou	být	k5eAaImIp3nP	být
nízko	nízko	k6eAd1	nízko
nasazené	nasazený	k2eAgInPc1d1	nasazený
<g/>
,	,	kIx,	,
do	do	k7c2	do
trojúhelníku	trojúhelník	k1gInSc2	trojúhelník
a	a	k8xC	a
středně	středně	k6eAd1	středně
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
<g/>
.	.	kIx.	.
</s>
<s>
Vztyčené	vztyčený	k2eAgFnPc1d1	vztyčená
a	a	k8xC	a
natočené	natočený	k2eAgFnPc1d1	natočená
dopředu	dopředu	k6eAd1	dopředu
<g/>
.	.	kIx.	.
</s>
<s>
Stop	stop	k1gInSc1	stop
je	být	k5eAaImIp3nS	být
dobře	dobře	k6eAd1	dobře
viditelný	viditelný	k2eAgInSc1d1	viditelný
<g/>
.	.	kIx.	.
</s>
<s>
Krk	krk	k1gInSc1	krk
je	být	k5eAaImIp3nS	být
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
<g/>
,	,	kIx,	,
rovný	rovný	k2eAgInSc4d1	rovný
a	a	k8xC	a
osrstěný	osrstěný	k2eAgInSc4d1	osrstěný
<g/>
.	.	kIx.	.
</s>
<s>
Srst	srst	k1gFnSc1	srst
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
nevytváří	vytvářit	k5eNaPmIp3nS	vytvářit
límec	límec	k1gInSc4	límec
<g/>
.	.	kIx.	.
</s>
<s>
Hřbet	hřbet	k1gInSc1	hřbet
je	být	k5eAaImIp3nS	být
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
<g/>
,	,	kIx,	,
rovný	rovný	k2eAgInSc4d1	rovný
a	a	k8xC	a
úzký	úzký	k2eAgInSc4d1	úzký
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
srst	srst	k1gFnSc1	srst
na	na	k7c6	na
něm	on	k3xPp3gMnSc6	on
může	moct	k5eAaImIp3nS	moct
dodávat	dodávat	k5eAaImF	dodávat
pocit	pocit	k1gInSc4	pocit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
široký	široký	k2eAgInSc1d1	široký
<g/>
.	.	kIx.	.
</s>
<s>
Ocas	ocas	k1gInSc1	ocas
je	být	k5eAaImIp3nS	být
krátký	krátký	k2eAgInSc1d1	krátký
a	a	k8xC	a
jen	jen	k9	jen
málo	málo	k6eAd1	málo
osrstěný	osrstěný	k2eAgMnSc1d1	osrstěný
<g/>
,	,	kIx,	,
zahnutý	zahnutý	k2eAgMnSc1d1	zahnutý
do	do	k7c2	do
šavle	šavle	k1gFnSc2	šavle
a	a	k8xC	a
nesen	nesen	k2eAgMnSc1d1	nesen
i	i	k9	i
nad	nad	k7c7	nad
hřbetní	hřbetní	k2eAgFnSc7d1	hřbetní
linií	linie	k1gFnSc7	linie
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
<g/>
,	,	kIx,	,
přípustnější	přípustný	k2eAgNnSc1d2	přípustnější
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
není	být	k5eNaImIp3nS	být
nad	nad	k7c7	nad
hřbetem	hřbet	k1gInSc7	hřbet
<g/>
.	.	kIx.	.
</s>
<s>
Nohy	noha	k1gFnPc1	noha
jsou	být	k5eAaImIp3nP	být
krátké	krátký	k2eAgFnPc1d1	krátká
<g/>
,	,	kIx,	,
ne	ne	k9	ne
moc	moc	k6eAd1	moc
dobře	dobře	k6eAd1	dobře
osvalené	osvalený	k2eAgNnSc1d1	osvalené
<g/>
.	.	kIx.	.
</s>
<s>
Tlapky	tlapka	k1gFnPc1	tlapka
australského	australský	k2eAgMnSc2d1	australský
teriéra	teriér	k1gMnSc2	teriér
jsou	být	k5eAaImIp3nP	být
pevné	pevný	k2eAgFnPc4d1	pevná
a	a	k8xC	a
povětšinou	povětšinou	k6eAd1	povětšinou
mají	mít	k5eAaImIp3nP	mít
černé	černý	k2eAgInPc1d1	černý
drápky	drápek	k1gInPc1	drápek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Váží	vážit	k5eAaImIp3nS	vážit
okolo	okolo	k7c2	okolo
6	[number]	k4	6
kg	kg	kA	kg
a	a	k8xC	a
výška	výška	k1gFnSc1	výška
v	v	k7c6	v
kohoutku	kohoutek	k1gInSc6	kohoutek
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
25	[number]	k4	25
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Povaha	povaha	k1gFnSc1	povaha
==	==	k?	==
</s>
</p>
<p>
<s>
Tento	tento	k3xDgMnSc1	tento
pes	pes	k1gMnSc1	pes
má	mít	k5eAaImIp3nS	mít
pravou	pravý	k2eAgFnSc4d1	pravá
teriérskou	teriérský	k2eAgFnSc4d1	teriérská
povahu	povaha	k1gFnSc4	povaha
-	-	kIx~	-
aktivní	aktivní	k2eAgInSc4d1	aktivní
<g/>
,	,	kIx,	,
oddaný	oddaný	k2eAgInSc4d1	oddaný
<g/>
,	,	kIx,	,
hlasitý	hlasitý	k2eAgInSc4d1	hlasitý
a	a	k8xC	a
milý	milý	k2eAgInSc4d1	milý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
bystrý	bystrý	k2eAgMnSc1d1	bystrý
a	a	k8xC	a
inteligentní	inteligentní	k2eAgMnSc1d1	inteligentní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
má	mít	k5eAaImIp3nS	mít
krátkou	krátký	k2eAgFnSc4d1	krátká
paměť	paměť	k1gFnSc4	paměť
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
i	i	k9	i
tvrdohlavý	tvrdohlavý	k2eAgMnSc1d1	tvrdohlavý
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
sklony	sklon	k1gInPc4	sklon
k	k	k7c3	k
uštěkanosti	uštěkanost	k1gFnSc3	uštěkanost
jako	jako	k9	jako
třeba	třeba	k6eAd1	třeba
pudl	pudl	k1gMnSc1	pudl
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
takto	takto	k6eAd1	takto
malý	malý	k2eAgInSc1d1	malý
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
dobrý	dobrý	k2eAgInSc1d1	dobrý
hlídač	hlídač	k1gInSc1	hlídač
–	–	k?	–
na	na	k7c4	na
každého	každý	k3xTgMnSc4	každý
nezvaného	nezvaný	k2eAgMnSc4d1	nezvaný
hosta	host	k1gMnSc4	host
hlasitě	hlasitě	k6eAd1	hlasitě
upozorní	upozornit	k5eAaPmIp3nS	upozornit
a	a	k8xC	a
svoji	svůj	k3xOyFgFnSc4	svůj
rodinu	rodina	k1gFnSc4	rodina
je	být	k5eAaImIp3nS	být
schopný	schopný	k2eAgInSc1d1	schopný
bránit	bránit	k5eAaImF	bránit
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
rodině	rodina	k1gFnSc3	rodina
je	být	k5eAaImIp3nS	být
loajálním	loajální	k2eAgMnSc7d1	loajální
<g/>
,	,	kIx,	,
věrným	věrný	k2eAgMnSc7d1	věrný
a	a	k8xC	a
milým	milý	k2eAgMnSc7d1	milý
přítelem	přítel	k1gMnSc7	přítel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Děti	dítě	k1gFnPc4	dítě
má	mít	k5eAaImIp3nS	mít
rád	rád	k2eAgMnSc1d1	rád
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
skvělý	skvělý	k2eAgInSc4d1	skvělý
vztah	vztah	k1gInSc4	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
se	s	k7c7	s
zvířaty	zvíře	k1gNnPc7	zvíře
úmyslně	úmyslně	k6eAd1	úmyslně
vyhledává	vyhledávat	k5eAaImIp3nS	vyhledávat
potyčky	potyčka	k1gFnPc4	potyčka
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
k	k	k7c3	k
nim	on	k3xPp3gMnPc3	on
chovat	chovat	k5eAaImF	chovat
dominantně	dominantně	k6eAd1	dominantně
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
psi	pes	k1gMnPc1	pes
(	(	kIx(	(
<g/>
samci	samec	k1gMnPc1	samec
<g/>
)	)	kIx)	)
tohoto	tento	k3xDgNnSc2	tento
plemene	plemeno	k1gNnSc2	plemeno
mohou	moct	k5eAaImIp3nP	moct
projevovat	projevovat	k5eAaImF	projevovat
dominantní	dominantní	k2eAgNnSc4d1	dominantní
chování	chování	k1gNnSc4	chování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
cizím	cizí	k2eAgMnPc3d1	cizí
se	se	k3xPyFc4	se
chová	chovat	k5eAaImIp3nS	chovat
odtažitě	odtažitě	k6eAd1	odtažitě
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ne	ne	k9	ne
agresivně	agresivně	k6eAd1	agresivně
<g/>
.	.	kIx.	.
</s>
<s>
Platí	platit	k5eAaImIp3nS	platit
u	u	k7c2	u
něj	on	k3xPp3gNnSc2	on
i	i	k9	i
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
k	k	k7c3	k
hostům	host	k1gMnPc3	host
chová	chovat	k5eAaImIp3nS	chovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
to	ten	k3xDgNnSc1	ten
"	"	kIx"	"
<g/>
odkouká	odkoukat	k5eAaPmIp3nS	odkoukat
<g/>
"	"	kIx"	"
od	od	k7c2	od
vás	vy	k3xPp2nPc2	vy
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Péče	péče	k1gFnSc1	péče
==	==	k?	==
</s>
</p>
<p>
<s>
Srst	srst	k1gFnSc1	srst
nepotřebuje	potřebovat	k5eNaImIp3nS	potřebovat
zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
péči	péče	k1gFnSc4	péče
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
není	být	k5eNaImIp3nS	být
dobré	dobrý	k2eAgNnSc1d1	dobré
ji	on	k3xPp3gFnSc4	on
často	často	k6eAd1	často
mýt	mýt	k5eAaImF	mýt
šamponem	šampon	k1gInSc7	šampon
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
by	by	kYmCp3nS	by
ztratila	ztratit	k5eAaPmAgFnS	ztratit
svoji	svůj	k3xOyFgFnSc4	svůj
přirozenou	přirozený	k2eAgFnSc4d1	přirozená
mastnotu	mastnota	k1gFnSc4	mastnota
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
by	by	kYmCp3nP	by
vznikaly	vznikat	k5eAaImAgInP	vznikat
problémy	problém	k1gInPc1	problém
s	s	k7c7	s
kůží	kůže	k1gFnSc7	kůže
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
šampon	šampon	k1gInSc4	šampon
při	při	k7c6	při
koupání	koupání	k1gNnSc6	koupání
nedostal	dostat	k5eNaPmAgMnS	dostat
do	do	k7c2	do
očí	oko	k1gNnPc2	oko
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ty	ten	k3xDgMnPc4	ten
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
citlivé	citlivý	k2eAgInPc1d1	citlivý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Může	moct	k5eAaImIp3nS	moct
žít	žít	k5eAaImF	žít
trvale	trvale	k6eAd1	trvale
venku	venku	k6eAd1	venku
v	v	k7c6	v
domě	dům	k1gInSc6	dům
či	či	k8xC	či
bytě	byt	k1gInSc6	byt
se	s	k7c7	s
svojí	svůj	k3xOyFgFnSc7	svůj
rodinou	rodina	k1gFnSc7	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
dobré	dobrý	k2eAgInPc4d1	dobrý
předpoklady	předpoklad	k1gInPc4	předpoklad
žít	žít	k5eAaImF	žít
ve	v	k7c6	v
městě	město	k1gNnSc6	město
a	a	k8xC	a
být	být	k5eAaImF	být
dobrým	dobrý	k2eAgMnSc7d1	dobrý
společníkem	společník	k1gMnSc7	společník
především	především	k9	především
pro	pro	k7c4	pro
starší	starý	k2eAgMnPc4d2	starší
lidi	člověk	k1gMnPc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Dožívá	dožívat	k5eAaImIp3nS	dožívat
se	se	k3xPyFc4	se
až	až	k9	až
14	[number]	k4	14
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
<g/>
Vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
svému	svůj	k3xOyFgMnSc3	svůj
australskému	australský	k2eAgMnSc3d1	australský
původu	původ	k1gMnSc3	původ
dobře	dobře	k6eAd1	dobře
snáší	snášet	k5eAaImIp3nS	snášet
vysoké	vysoký	k2eAgFnPc4d1	vysoká
teploty	teplota	k1gFnPc4	teplota
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výcvik	výcvik	k1gInSc1	výcvik
není	být	k5eNaImIp3nS	být
nutný	nutný	k2eAgInSc1d1	nutný
<g/>
,	,	kIx,	,
stačí	stačit	k5eAaBmIp3nS	stačit
aby	aby	k9	aby
uměl	umět	k5eAaImAgMnS	umět
přivolání	přivolání	k1gNnSc4	přivolání
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
výchova	výchova	k1gFnSc1	výchova
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
důležitá	důležitý	k2eAgFnSc1d1	důležitá
<g/>
!	!	kIx.	!
</s>
<s>
Ve	v	k7c6	v
štěněcím	štěněcí	k2eAgInSc6d1	štěněcí
věku	věk	k1gInSc6	věk
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
dobře	dobře	k6eAd1	dobře
socializovat	socializovat	k5eAaBmF	socializovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
později	pozdě	k6eAd2	pozdě
neměl	mít	k5eNaImAgMnS	mít
problémy	problém	k1gInPc4	problém
s	s	k7c7	s
jinými	jiný	k2eAgNnPc7d1	jiné
zvířaty	zvíře	k1gNnPc7	zvíře
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
lidmi	člověk	k1gMnPc7	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Čistotnosti	čistotnost	k1gFnPc1	čistotnost
se	se	k3xPyFc4	se
také	také	k9	také
nenaučí	naučit	k5eNaPmIp3nS	naučit
sám	sám	k3xTgInSc4	sám
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
mu	on	k3xPp3gMnSc3	on
ukázat	ukázat	k5eAaPmF	ukázat
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
jít	jít	k5eAaImF	jít
<g/>
,	,	kIx,	,
když	když	k8xS	když
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
vykonat	vykonat	k5eAaPmF	vykonat
potřebu	potřeba	k1gFnSc4	potřeba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Australský	australský	k2eAgMnSc1d1	australský
teriér	teriér	k1gMnSc1	teriér
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgInSc1d1	oficiální
standard	standard	k1gInSc1	standard
plemene	plemeno	k1gNnSc2	plemeno
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
ČMKU	ČMKU	kA	ČMKU
</s>
</p>
