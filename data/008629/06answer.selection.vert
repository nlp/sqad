<s>
Chová	chovat	k5eAaImIp3nS	chovat
se	se	k3xPyFc4	se
téměř	téměř	k6eAd1	téměř
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
anglicky	anglicky	k6eAd1	anglicky
mluvících	mluvící	k2eAgFnPc6d1	mluvící
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
nejhojnější	hojný	k2eAgInSc1d3	nejhojnější
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
toto	tento	k3xDgNnSc1	tento
plemeno	plemeno	k1gNnSc1	plemeno
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c6	na
blízkém	blízký	k2eAgInSc6d1	blízký
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
<g/>
.	.	kIx.	.
</s>
