<p>
<s>
Československo	Československo	k1gNnSc1	Československo
zaniklo	zaniknout	k5eAaPmAgNnS	zaniknout
po	po	k7c6	po
74	[number]	k4	74
letech	léto	k1gNnPc6	léto
od	od	k7c2	od
svého	svůj	k3xOyFgNnSc2	svůj
založení	založení	k1gNnSc2	založení
uplynutím	uplynutí	k1gNnSc7	uplynutí
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
Česká	český	k2eAgFnSc1d1	Česká
a	a	k8xC	a
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
Federativní	federativní	k2eAgFnSc1d1	federativní
Republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
a	a	k8xC	a
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
republika	republika	k1gFnSc1	republika
se	se	k3xPyFc4	se
tak	tak	k9	tak
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1993	[number]	k4	1993
staly	stát	k5eAaPmAgFnP	stát
jejími	její	k3xOp3gInPc7	její
nástupnickými	nástupnický	k2eAgInPc7d1	nástupnický
státy	stát	k1gInPc7	stát
a	a	k8xC	a
samostatnými	samostatný	k2eAgInPc7d1	samostatný
subjekty	subjekt	k1gInPc7	subjekt
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
práva	právo	k1gNnSc2	právo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Československo	Československo	k1gNnSc1	Československo
již	již	k9	již
během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
historie	historie	k1gFnSc2	historie
jednou	jednou	k6eAd1	jednou
přestalo	přestat	k5eAaPmAgNnS	přestat
de	de	k?	de
facto	facto	k1gNnSc1	facto
existovat	existovat	k5eAaImF	existovat
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
14	[number]	k4	14
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1939	[number]	k4	1939
okupací	okupace	k1gFnPc2	okupace
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
Moravy	Morava	k1gFnSc2	Morava
a	a	k8xC	a
Slezska	Slezsko	k1gNnSc2	Slezsko
nacistickým	nacistický	k2eAgNnSc7d1	nacistické
Německem	Německo	k1gNnSc7	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgInS	být
československý	československý	k2eAgInSc1d1	československý
stát	stát	k1gInSc1	stát
obnoven	obnoven	k2eAgInSc1d1	obnoven
s	s	k7c7	s
plnou	plný	k2eAgFnSc7d1	plná
právní	právní	k2eAgFnSc7d1	právní
kontinuitou	kontinuita	k1gFnSc7	kontinuita
na	na	k7c4	na
první	první	k4xOgFnSc4	první
republiku	republika	k1gFnSc4	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zánik	zánik	k1gInSc1	zánik
druhé	druhý	k4xOgFnSc2	druhý
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
německá	německý	k2eAgFnSc1d1	německá
okupace	okupace	k1gFnSc1	okupace
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
14	[number]	k4	14
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1939	[number]	k4	1939
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
Snem	sen	k1gInSc7	sen
Slovenskej	Slovenskej	k?	Slovenskej
krajiny	krajina	k1gFnSc2	krajina
samostatný	samostatný	k2eAgInSc4d1	samostatný
slovenský	slovenský	k2eAgInSc4d1	slovenský
stát	stát	k1gInSc4	stát
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
druhá	druhý	k4xOgFnSc1	druhý
republika	republika	k1gFnSc1	republika
rozpadla	rozpadnout	k5eAaPmAgFnS	rozpadnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
15	[number]	k4	15
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1939	[number]	k4	1939
byly	být	k5eAaImAgFnP	být
české	český	k2eAgFnPc1d1	Česká
země	zem	k1gFnPc1	zem
obsazeny	obsadit	k5eAaPmNgFnP	obsadit
vojsky	vojsky	k6eAd1	vojsky
Nacistického	nacistický	k2eAgNnSc2d1	nacistické
Německa	Německo	k1gNnSc2	Německo
</s>
</p>
<p>
<s>
16	[number]	k4	16
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1939	[number]	k4	1939
bylo	být	k5eAaImAgNnS	být
obsazené	obsazený	k2eAgNnSc1d1	obsazené
území	území	k1gNnSc1	území
připojeno	připojen	k2eAgNnSc1d1	připojeno
k	k	k7c3	k
Německu	Německo	k1gNnSc3	Německo
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
Protektorát	protektorát	k1gInSc1	protektorát
Čechy	Čechy	k1gFnPc1	Čechy
a	a	k8xC	a
Morava	Morava	k1gFnSc1	Morava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zánik	zánik	k1gInSc1	zánik
České	český	k2eAgFnSc2d1	Česká
a	a	k8xC	a
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
Federativní	federativní	k2eAgFnSc2d1	federativní
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
/	/	kIx~	/
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Politický	politický	k2eAgInSc1d1	politický
vývoj	vývoj	k1gInSc1	vývoj
===	===	k?	===
</s>
</p>
<p>
<s>
Slovensko	Slovensko	k1gNnSc4	Slovensko
si	se	k3xPyFc3	se
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
a	a	k8xC	a
po	po	k7c6	po
zrušení	zrušení	k1gNnSc6	zrušení
zemského	zemský	k2eAgNnSc2d1	zemské
zřízení	zřízení	k1gNnSc2	zřízení
asymetricky	asymetricky	k6eAd1	asymetricky
podrželo	podržet	k5eAaPmAgNnS	podržet
jistou	jistý	k2eAgFnSc4d1	jistá
míru	míra	k1gFnSc4	míra
autonomie	autonomie	k1gFnSc2	autonomie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
zde	zde	k6eAd1	zde
jako	jako	k8xC	jako
zákonodárný	zákonodárný	k2eAgInSc4d1	zákonodárný
sbor	sbor	k1gInSc4	sbor
působila	působit	k5eAaImAgFnS	působit
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
národní	národní	k2eAgFnSc1d1	národní
rada	rada	k1gFnSc1	rada
<g/>
,	,	kIx,	,
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
i	i	k8xC	i
její	její	k3xOp3gInSc1	její
výkonný	výkonný	k2eAgInSc1d1	výkonný
orgán	orgán	k1gInSc1	orgán
Sbor	sbor	k1gInSc1	sbor
pověřenců	pověřenec	k1gMnPc2	pověřenec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc7	první
fází	fáze	k1gFnSc7	fáze
rozdělení	rozdělení	k1gNnSc2	rozdělení
Československa	Československo	k1gNnSc2	Československo
byla	být	k5eAaImAgFnS	být
federalizace	federalizace	k1gFnSc1	federalizace
Československa	Československo	k1gNnSc2	Československo
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
dosud	dosud	k6eAd1	dosud
unitární	unitární	k2eAgFnSc1d1	unitární
Československá	československý	k2eAgFnSc1d1	Československá
socialistická	socialistický	k2eAgFnSc1d1	socialistická
republika	republika	k1gFnSc1	republika
přeměnila	přeměnit	k5eAaPmAgFnS	přeměnit
ve	v	k7c6	v
federaci	federace	k1gFnSc6	federace
dvou	dva	k4xCgMnPc2	dva
podle	podle	k7c2	podle
ústavního	ústavní	k2eAgInSc2d1	ústavní
zákona	zákon	k1gInSc2	zákon
suverénních	suverénní	k2eAgInPc2d1	suverénní
národních	národní	k2eAgInPc2d1	národní
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
České	český	k2eAgFnSc2d1	Česká
socialistické	socialistický	k2eAgFnSc2d1	socialistická
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
socialistické	socialistický	k2eAgFnSc2d1	socialistická
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Federalizace	federalizace	k1gFnSc1	federalizace
byla	být	k5eAaImAgFnS	být
vyústěním	vyústění	k1gNnSc7	vyústění
politických	politický	k2eAgFnPc2d1	politická
tendencí	tendence	k1gFnPc2	tendence
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
při	při	k7c6	při
politickém	politický	k2eAgNnSc6d1	politické
uvolnění	uvolnění	k1gNnSc6	uvolnění
začátkem	začátkem	k7c2	začátkem
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
vystupňovaly	vystupňovat	k5eAaPmAgInP	vystupňovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Národnostní	národnostní	k2eAgInPc1d1	národnostní
spory	spor	k1gInPc1	spor
se	se	k3xPyFc4	se
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
znovu	znovu	k6eAd1	znovu
vyhrotily	vyhrotit	k5eAaPmAgInP	vyhrotit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
například	například	k6eAd1	například
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
tzv.	tzv.	kA	tzv.
Pomlčkové	pomlčkový	k2eAgFnSc2d1	pomlčková
války	válka	k1gFnSc2	válka
o	o	k7c4	o
název	název	k1gInSc4	název
federace	federace	k1gFnSc2	federace
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
přičítá	přičítat	k5eAaImIp3nS	přičítat
vyhrocení	vyhrocení	k1gNnSc4	vyhrocení
národnostních	národnostní	k2eAgInPc2d1	národnostní
vztahů	vztah	k1gInPc2	vztah
též	též	k9	též
lidem	člověk	k1gMnPc3	člověk
z	z	k7c2	z
okolí	okolí	k1gNnSc2	okolí
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
Klause	Klaus	k1gMnSc2	Klaus
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
zasadili	zasadit	k5eAaPmAgMnP	zasadit
o	o	k7c4	o
významnou	významný	k2eAgFnSc4d1	významná
redukci	redukce	k1gFnSc4	redukce
zbrojního	zbrojní	k2eAgInSc2d1	zbrojní
průmyslu	průmysl	k1gInSc2	průmysl
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
po	po	k7c6	po
více	hodně	k6eAd2	hodně
než	než	k8xS	než
21	[number]	k4	21
letech	let	k1gInPc6	let
formální	formální	k2eAgFnSc2d1	formální
existence	existence	k1gFnSc2	existence
federace	federace	k1gFnSc2	federace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
oba	dva	k4xCgInPc1	dva
národní	národní	k2eAgInPc1d1	národní
<g />
.	.	kIx.	.
</s>
<s>
státy	stát	k1gInPc1	stát
neměly	mít	k5eNaImAgInP	mít
ani	ani	k9	ani
své	svůj	k3xOyFgInPc4	svůj
vlastní	vlastní	k2eAgInPc4d1	vlastní
státní	státní	k2eAgInPc4d1	státní
symboly	symbol	k1gInPc4	symbol
<g/>
,	,	kIx,	,
přijala	přijmout	k5eAaPmAgFnS	přijmout
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
národní	národní	k2eAgFnSc1d1	národní
rada	rada	k1gFnSc1	rada
ústavním	ústavní	k2eAgInSc7d1	ústavní
zákonem	zákon	k1gInSc7	zákon
č.	č.	k?	č.
50	[number]	k4	50
<g/>
/	/	kIx~	/
<g/>
1990	[number]	k4	1990
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
účinnost	účinnost	k1gFnSc1	účinnost
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
a	a	k8xC	a
Česká	český	k2eAgFnSc1d1	Česká
národní	národní	k2eAgFnSc1d1	národní
rada	rada	k1gFnSc1	rada
ústavní	ústavní	k2eAgFnSc1d1	ústavní
zákon	zákon	k1gInSc4	zákon
č.	č.	k?	č.
67	[number]	k4	67
<g/>
/	/	kIx~	/
<g/>
1990	[number]	k4	1990
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
účinnost	účinnost	k1gFnSc1	účinnost
ode	ode	k7c2	ode
dne	den	k1gInSc2	den
vyhlášení	vyhlášení	k1gNnSc4	vyhlášení
13	[number]	k4	13
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
zavedly	zavést	k5eAaPmAgFnP	zavést
státní	státní	k2eAgInPc4d1	státní
symboly	symbol	k1gInPc4	symbol
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
slovenská	slovenský	k2eAgFnSc1d1	slovenská
vláda	vláda	k1gFnSc1	vláda
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
mezinárodních	mezinárodní	k2eAgInPc2d1	mezinárodní
vztahů	vztah	k1gInPc2	vztah
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
čele	čelo	k1gNnSc6	čelo
Milan	Milan	k1gMnSc1	Milan
Kňažko	Kňažka	k1gFnSc5	Kňažka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
česká	český	k2eAgFnSc1d1	Česká
vláda	vláda	k1gFnSc1	vláda
zřídila	zřídit	k5eAaPmAgFnS	zřídit
Ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
mezinárodních	mezinárodní	k2eAgInPc2d1	mezinárodní
vztahů	vztah	k1gInPc2	vztah
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
ministrem	ministr	k1gMnSc7	ministr
byl	být	k5eAaImAgMnS	být
Josef	Josef	k1gMnSc1	Josef
Zieleniec	Zieleniec	k1gMnSc1	Zieleniec
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Mečiara	Mečiar	k1gMnSc2	Mečiar
měla	mít	k5eAaImAgFnS	mít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
česká	český	k2eAgFnSc1d1	Česká
vláda	vláda	k1gFnSc1	vláda
také	také	k9	také
připravený	připravený	k2eAgInSc4d1	připravený
plán	plán	k1gInSc4	plán
jednostranného	jednostranný	k2eAgNnSc2d1	jednostranné
vystoupení	vystoupení	k1gNnSc2	vystoupení
z	z	k7c2	z
federace	federace	k1gFnSc2	federace
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterém	který	k3yQgInSc6	který
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
nikdo	nikdo	k3yNnSc1	nikdo
nevěděl	vědět	k5eNaImAgMnS	vědět
<g/>
.	.	kIx.	.
<g/>
Napětí	napětí	k1gNnSc4	napětí
vzbuzovala	vzbuzovat	k5eAaImAgFnS	vzbuzovat
i	i	k9	i
asymetrie	asymetrie	k1gFnSc1	asymetrie
mnoha	mnoho	k4c2	mnoho
institucí	instituce	k1gFnPc2	instituce
–	–	k?	–
například	například	k6eAd1	například
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
působila	působit	k5eAaImAgFnS	působit
Československá	československý	k2eAgFnSc1d1	Československá
televize	televize	k1gFnSc1	televize
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
televízia	televízia	k1gFnSc1	televízia
<g/>
,	,	kIx,	,
souběžně	souběžně	k6eAd1	souběžně
vznikaly	vznikat	k5eAaImAgInP	vznikat
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
tendence	tendence	k1gFnSc2	tendence
ke	k	k7c3	k
zřízení	zřízení	k1gNnSc3	zřízení
slovenské	slovenský	k2eAgFnSc2d1	slovenská
policie	policie	k1gFnSc2	policie
<g/>
,	,	kIx,	,
slovenské	slovenský	k2eAgFnSc2d1	slovenská
armády	armáda	k1gFnSc2	armáda
i	i	k8xC	i
slovenské	slovenský	k2eAgFnSc2d1	slovenská
národní	národní	k2eAgFnSc2d1	národní
banky	banka	k1gFnSc2	banka
<g/>
.	.	kIx.	.
</s>
<s>
Parlamentnímu	parlamentní	k2eAgNnSc3d1	parlamentní
projednávání	projednávání	k1gNnSc3	projednávání
zákonů	zákon	k1gInPc2	zákon
dělal	dělat	k5eAaImAgInS	dělat
vážný	vážný	k2eAgInSc1d1	vážný
problém	problém	k1gInSc1	problém
zákaz	zákaz	k1gInSc4	zákaz
majorizace	majorizace	k1gFnSc2	majorizace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Český	český	k2eAgMnSc1d1	český
premiér	premiér	k1gMnSc1	premiér
Petr	Petr	k1gMnSc1	Petr
Pithart	Pithart	k1gInSc4	Pithart
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
zmínil	zmínit	k5eAaPmAgMnS	zmínit
vizi	vize	k1gFnSc4	vize
dvojdomku	dvojdomek	k1gInSc2	dvojdomek
<g/>
,	,	kIx,	,
de	de	k?	de
facto	facto	k1gNnSc4	facto
konfederace	konfederace	k1gFnSc2	konfederace
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
Občanské	občanský	k2eAgNnSc1d1	občanské
hnutí	hnutí	k1gNnSc1	hnutí
následně	následně	k6eAd1	následně
propadlo	propadnout	k5eAaPmAgNnS	propadnout
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
později	pozdě	k6eAd2	pozdě
přičítal	přičítat	k5eAaImAgMnS	přičítat
vinu	vina	k1gFnSc4	vina
za	za	k7c4	za
rozdělení	rozdělení	k1gNnSc4	rozdělení
jak	jak	k8xC	jak
vstřícnosti	vstřícnost	k1gFnPc4	vstřícnost
Petra	Petr	k1gMnSc2	Petr
Pitharta	Pithart	k1gMnSc2	Pithart
vůči	vůči	k7c3	vůči
slovenským	slovenský	k2eAgInPc3d1	slovenský
požadavkům	požadavek	k1gInPc3	požadavek
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
údajně	údajně	k6eAd1	údajně
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
vyřazení	vyřazení	k1gNnSc3	vyřazení
federálních	federální	k2eAgInPc2d1	federální
orgánů	orgán	k1gInPc2	orgán
ze	z	k7c2	z
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
tak	tak	k9	tak
údajné	údajný	k2eAgFnSc6d1	údajná
zásluze	zásluha	k1gFnSc6	zásluha
prezidenta	prezident	k1gMnSc2	prezident
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
o	o	k7c4	o
dočasné	dočasný	k2eAgNnSc4d1	dočasné
sesazení	sesazení	k1gNnSc4	sesazení
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Mečiara	Mečiar	k1gMnSc2	Mečiar
z	z	k7c2	z
funkce	funkce	k1gFnSc2	funkce
slovenského	slovenský	k2eAgMnSc2d1	slovenský
premiéra	premiér	k1gMnSc2	premiér
<g/>
.	.	kIx.	.
<g/>
Ján	Ján	k1gMnSc1	Ján
Čarnogurský	Čarnogurský	k2eAgMnSc1d1	Čarnogurský
chtěl	chtít	k5eAaImAgMnS	chtít
vyhlášení	vyhlášení	k1gNnPc4	vyhlášení
samostatnosti	samostatnost	k1gFnSc2	samostatnost
Slovenska	Slovensko	k1gNnSc2	Slovensko
odložit	odložit	k5eAaPmF	odložit
až	až	k9	až
na	na	k7c4	na
dobu	doba	k1gFnSc4	doba
vstupu	vstup	k1gInSc2	vstup
ČSFR	ČSFR	kA	ČSFR
do	do	k7c2	do
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
způsobil	způsobit	k5eAaPmAgInS	způsobit
propad	propad	k1gInSc4	propad
své	svůj	k3xOyFgFnSc2	svůj
politické	politický	k2eAgFnSc2d1	politická
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
opětovné	opětovný	k2eAgNnSc1d1	opětovné
vítězství	vítězství	k1gNnSc1	vítězství
HZDS	HZDS	kA	HZDS
a	a	k8xC	a
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Mečiara	Mečiar	k1gMnSc2	Mečiar
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozdělení	rozdělení	k1gNnSc1	rozdělení
Československa	Československo	k1gNnSc2	Československo
podporovaly	podporovat	k5eAaImAgFnP	podporovat
všechny	všechen	k3xTgFnPc1	všechen
významné	významný	k2eAgFnPc1d1	významná
slovenské	slovenský	k2eAgFnPc1d1	slovenská
politické	politický	k2eAgFnPc1d1	politická
strany	strana	k1gFnPc1	strana
a	a	k8xC	a
špičkoví	špičkový	k2eAgMnPc1d1	špičkový
politici	politik	k1gMnPc1	politik
<g/>
.	.	kIx.	.
</s>
<s>
Demokratická	demokratický	k2eAgFnSc1d1	demokratická
strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
zachování	zachování	k1gNnSc4	zachování
společného	společný	k2eAgInSc2d1	společný
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
u	u	k7c2	u
voleb	volba	k1gFnPc2	volba
neuspěla	uspět	k5eNaPmAgFnS	uspět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
ČR	ČR	kA	ČR
uspěla	uspět	k5eAaPmAgFnS	uspět
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
ODS	ODS	kA	ODS
s	s	k7c7	s
heslem	heslo	k1gNnSc7	heslo
"	"	kIx"	"
<g/>
společný	společný	k2eAgInSc4d1	společný
stát	stát	k1gInSc4	stát
nebo	nebo	k8xC	nebo
rozdělení	rozdělení	k1gNnSc4	rozdělení
<g/>
"	"	kIx"	"
a	a	k8xC	a
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
HZDS	HZDS	kA	HZDS
s	s	k7c7	s
požadavkem	požadavek	k1gInSc7	požadavek
"	"	kIx"	"
<g/>
konfederace	konfederace	k1gFnSc2	konfederace
nebo	nebo	k8xC	nebo
rozdělení	rozdělení	k1gNnSc2	rozdělení
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Úplné	úplný	k2eAgNnSc1d1	úplné
rozdělení	rozdělení	k1gNnSc1	rozdělení
federace	federace	k1gFnSc2	federace
bylo	být	k5eAaImAgNnS	být
tedy	tedy	k8xC	tedy
průnikem	průnik	k1gInSc7	průnik
programů	program	k1gInPc2	program
vítězných	vítězný	k2eAgInPc2d1	vítězný
stran	stran	k7c2	stran
obou	dva	k4xCgInPc2	dva
národních	národní	k2eAgInPc2d1	národní
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
třebaže	třebaže	k8xS	třebaže
jejich	jejich	k3xOp3gInSc4	jejich
mandát	mandát	k1gInSc4	mandát
rozdělit	rozdělit	k5eAaPmF	rozdělit
federaci	federace	k1gFnSc4	federace
byl	být	k5eAaImAgInS	být
později	pozdě	k6eAd2	pozdě
mnohokrát	mnohokrát	k6eAd1	mnohokrát
zpochybňován	zpochybňován	k2eAgInSc1d1	zpochybňován
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
již	již	k6eAd1	již
18	[number]	k4	18
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1991	[number]	k4	1991
byl	být	k5eAaImAgInS	být
federálním	federální	k2eAgNnSc7d1	federální
shromážděním	shromáždění	k1gNnSc7	shromáždění
přijat	přijmout	k5eAaPmNgInS	přijmout
ústavní	ústavní	k2eAgInSc1d1	ústavní
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
referendu	referendum	k1gNnSc6	referendum
č.	č.	k?	č.
327	[number]	k4	327
<g/>
/	/	kIx~	/
<g/>
1991	[number]	k4	1991
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
nebylo	být	k5eNaImAgNnS	být
pořádání	pořádání	k1gNnSc1	pořádání
referenda	referendum	k1gNnSc2	referendum
o	o	k7c4	o
rozdělení	rozdělení	k1gNnSc4	rozdělení
federace	federace	k1gFnSc2	federace
podle	podle	k7c2	podle
tohoto	tento	k3xDgInSc2	tento
zákona	zákon	k1gInSc2	zákon
nikdy	nikdy	k6eAd1	nikdy
vážně	vážně	k6eAd1	vážně
navrženo	navržen	k2eAgNnSc4d1	navrženo
–	–	k?	–
zřejmě	zřejmě	k6eAd1	zřejmě
i	i	k9	i
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
jeho	on	k3xPp3gInSc4	on
případný	případný	k2eAgInSc4d1	případný
zamítavý	zamítavý	k2eAgInSc4d1	zamítavý
výsledek	výsledek	k1gInSc4	výsledek
situaci	situace	k1gFnSc4	situace
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nevyřešil	vyřešit	k5eNaPmAgMnS	vyřešit
<g/>
.	.	kIx.	.
</s>
<s>
Ústavní	ústavní	k2eAgInSc1d1	ústavní
zákon	zákon	k1gInSc1	zákon
sice	sice	k8xC	sice
přikazoval	přikazovat	k5eAaImAgMnS	přikazovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
o	o	k7c6	o
návrhu	návrh	k1gInSc6	návrh
na	na	k7c4	na
vystoupení	vystoupení	k1gNnSc4	vystoupení
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
republik	republika	k1gFnPc2	republika
z	z	k7c2	z
federace	federace	k1gFnSc2	federace
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
referendem	referendum	k1gNnSc7	referendum
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
rozdělení	rozdělení	k1gNnSc1	rozdělení
Československa	Československo	k1gNnSc2	Československo
bylo	být	k5eAaImAgNnS	být
zkonstruováno	zkonstruovat	k5eAaPmNgNnS	zkonstruovat
jako	jako	k8xS	jako
zánik	zánik	k1gInSc1	zánik
federace	federace	k1gFnSc2	federace
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
jako	jako	k8xS	jako
vystoupení	vystoupení	k1gNnSc4	vystoupení
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
republik	republika	k1gFnPc2	republika
<g/>
.17	.17	k4	.17
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1992	[number]	k4	1992
přijala	přijmout	k5eAaPmAgFnS	přijmout
a	a	k8xC	a
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
národní	národní	k2eAgFnSc1d1	národní
rada	rada	k1gFnSc1	rada
dokument	dokument	k1gInSc4	dokument
Deklarace	deklarace	k1gFnSc2	deklarace
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
o	o	k7c6	o
svrchovanosti	svrchovanost	k1gFnSc6	svrchovanost
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
požadavek	požadavek	k1gInSc4	požadavek
samostatnosti	samostatnost	k1gFnSc2	samostatnost
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
20	[number]	k4	20
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1992	[number]	k4	1992
odstoupil	odstoupit	k5eAaPmAgMnS	odstoupit
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
funkce	funkce	k1gFnSc2	funkce
prezident	prezident	k1gMnSc1	prezident
ČSFR	ČSFR	kA	ČSFR
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
zbylou	zbylý	k2eAgFnSc4d1	zbylá
dobu	doba	k1gFnSc4	doba
své	svůj	k3xOyFgFnSc2	svůj
existence	existence	k1gFnSc2	existence
byla	být	k5eAaImAgFnS	být
federace	federace	k1gFnSc1	federace
bez	bez	k7c2	bez
prezidenta	prezident	k1gMnSc2	prezident
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozdělení	rozdělení	k1gNnSc1	rozdělení
republiky	republika	k1gFnSc2	republika
je	být	k5eAaImIp3nS	být
spojeno	spojit	k5eAaPmNgNnS	spojit
s	s	k7c7	s
jednáními	jednání	k1gNnPc7	jednání
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc4	který
vedl	vést	k5eAaImAgMnS	vést
předseda	předseda	k1gMnSc1	předseda
české	český	k2eAgFnSc2d1	Česká
vlády	vláda	k1gFnSc2	vláda
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
a	a	k8xC	a
předseda	předseda	k1gMnSc1	předseda
slovenské	slovenský	k2eAgFnSc2d1	slovenská
vlády	vláda	k1gFnSc2	vláda
Vladimír	Vladimír	k1gMnSc1	Vladimír
Mečiar	Mečiar	k1gMnSc1	Mečiar
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
se	se	k3xPyFc4	se
setkali	setkat	k5eAaPmAgMnP	setkat
již	již	k6eAd1	již
8	[number]	k4	8
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1992	[number]	k4	1992
v	v	k7c6	v
brněnské	brněnský	k2eAgFnSc6d1	brněnská
vile	vila	k1gFnSc6	vila
Tugendhat	Tugendhat	k1gFnSc2	Tugendhat
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
dohodli	dohodnout	k5eAaPmAgMnP	dohodnout
na	na	k7c4	na
rozdělení	rozdělení	k1gNnSc4	rozdělení
federace	federace	k1gFnSc2	federace
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
písemné	písemný	k2eAgFnSc6d1	písemná
podobě	podoba	k1gFnSc6	podoba
tuto	tento	k3xDgFnSc4	tento
dohodu	dohoda	k1gFnSc4	dohoda
podepsali	podepsat	k5eAaPmAgMnP	podepsat
při	při	k7c6	při
další	další	k2eAgFnSc6d1	další
schůzce	schůzka	k1gFnSc6	schůzka
tamtéž	tamtéž	k6eAd1	tamtéž
dne	den	k1gInSc2	den
26	[number]	k4	26
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
urychlení	urychlení	k1gNnSc3	urychlení
rozdělení	rozdělení	k1gNnSc2	rozdělení
republiky	republika	k1gFnSc2	republika
přispěla	přispět	k5eAaPmAgFnS	přispět
neslučitelnost	neslučitelnost	k1gFnSc1	neslučitelnost
vítězných	vítězný	k2eAgFnPc2d1	vítězná
politických	politický	k2eAgFnPc2d1	politická
stran	strana	k1gFnPc2	strana
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
státech	stát	k1gInPc6	stát
–	–	k?	–
ODS	ODS	kA	ODS
a	a	k8xC	a
HZDS	HZDS	kA	HZDS
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
13	[number]	k4	13
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1992	[number]	k4	1992
přijalo	přijmout	k5eAaPmAgNnS	přijmout
Federální	federální	k2eAgNnSc1d1	federální
shromáždění	shromáždění	k1gNnSc1	shromáždění
ústavní	ústavní	k2eAgFnSc2d1	ústavní
zákon	zákon	k1gInSc4	zákon
č.	č.	k?	č.
541	[number]	k4	541
<g/>
/	/	kIx~	/
<g/>
1992	[number]	k4	1992
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c4	o
dělení	dělení	k1gNnSc4	dělení
majetku	majetek	k1gInSc2	majetek
České	český	k2eAgFnSc2d1	Česká
a	a	k8xC	a
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
Federativní	federativní	k2eAgFnSc2d1	federativní
Republiky	republika	k1gFnSc2	republika
mezi	mezi	k7c4	mezi
Českou	český	k2eAgFnSc4d1	Česká
republiku	republika	k1gFnSc4	republika
a	a	k8xC	a
Slovenskou	slovenský	k2eAgFnSc4d1	slovenská
republiku	republika	k1gFnSc4	republika
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc6	jeho
přechodu	přechod	k1gInSc6	přechod
na	na	k7c4	na
Českou	český	k2eAgFnSc4d1	Česká
republiku	republika	k1gFnSc4	republika
a	a	k8xC	a
Slovenskou	slovenský	k2eAgFnSc4d1	slovenská
republiku	republika	k1gFnSc4	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
25	[number]	k4	25
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1992	[number]	k4	1992
přijalo	přijmout	k5eAaPmAgNnS	přijmout
Federální	federální	k2eAgNnSc1d1	federální
shromáždění	shromáždění	k1gNnSc1	shromáždění
ústavní	ústavní	k2eAgFnSc2d1	ústavní
zákon	zákon	k1gInSc4	zákon
č.	č.	k?	č.
542	[number]	k4	542
<g/>
/	/	kIx~	/
<g/>
1992	[number]	k4	1992
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
o	o	k7c6	o
zániku	zánik	k1gInSc6	zánik
ČSFR	ČSFR	kA	ČSFR
k	k	k7c3	k
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosinci	prosinec	k1gInSc3	prosinec
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
15	[number]	k4	15
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1992	[number]	k4	1992
přijala	přijmout	k5eAaPmAgFnS	přijmout
Česká	český	k2eAgFnSc1d1	Česká
národní	národní	k2eAgFnSc1d1	národní
rada	rada	k1gFnSc1	rada
ústavní	ústavní	k2eAgFnSc1d1	ústavní
zákon	zákon	k1gInSc4	zákon
č.	č.	k?	č.
4	[number]	k4	4
<g/>
/	/	kIx~	/
<g/>
1993	[number]	k4	1993
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
opatřeních	opatření	k1gNnPc6	opatření
souvisejících	související	k2eAgFnPc2d1	související
se	s	k7c7	s
zánikem	zánik	k1gInSc7	zánik
České	český	k2eAgFnSc2d1	Česká
a	a	k8xC	a
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
Federativní	federativní	k2eAgFnSc2d1	federativní
Republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
stanovoval	stanovovat	k5eAaImAgInS	stanovovat
převzetí	převzetí	k1gNnSc4	převzetí
právního	právní	k2eAgInSc2d1	právní
řádu	řád	k1gInSc2	řád
a	a	k8xC	a
kompetencí	kompetence	k1gFnSc7	kompetence
ČSFR	ČSFR	kA	ČSFR
Českou	český	k2eAgFnSc7d1	Česká
republikou	republika	k1gFnSc7	republika
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
přijala	přijmout	k5eAaPmAgFnS	přijmout
ČNR	ČNR	kA	ČNR
usnesení	usnesení	k1gNnPc2	usnesení
č.	č.	k?	č.
5	[number]	k4	5
<g/>
/	/	kIx~	/
<g/>
1993	[number]	k4	1993
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
vyjádřila	vyjádřit	k5eAaPmAgFnS	vyjádřit
předpoklad	předpoklad	k1gInSc4	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
žádný	žádný	k3yNgInSc4	žádný
výklad	výklad	k1gInSc4	výklad
stávajících	stávající	k2eAgFnPc2d1	stávající
právních	právní	k2eAgFnPc2d1	právní
norem	norma	k1gFnPc2	norma
nepřipouští	připouštět	k5eNaImIp3nS	připouštět
zpochybnění	zpochybnění	k1gNnSc1	zpochybnění
kontinuity	kontinuita	k1gFnSc2	kontinuita
zákonodárné	zákonodárný	k2eAgFnSc2d1	zákonodárná
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
nositelkou	nositelka	k1gFnSc7	nositelka
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
ústavního	ústavní	k2eAgInSc2d1	ústavní
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
4	[number]	k4	4
<g/>
/	/	kIx~	/
<g/>
1993	[number]	k4	1993
Sb	sb	kA	sb
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
národní	národní	k2eAgFnSc1d1	národní
rada	rada	k1gFnSc1	rada
<g/>
.	.	kIx.	.
22	[number]	k4	22
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1992	[number]	k4	1992
přijala	přijmout	k5eAaPmAgFnS	přijmout
ČNR	ČNR	kA	ČNR
ještě	ještě	k6eAd1	ještě
ústavní	ústavní	k2eAgInSc4d1	ústavní
zákon	zákon	k1gInSc4	zákon
č.	č.	k?	č.
29	[number]	k4	29
<g/>
/	/	kIx~	/
<g/>
1993	[number]	k4	1993
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
dalších	další	k2eAgNnPc6d1	další
opatřeních	opatření	k1gNnPc6	opatření
souvisejících	související	k2eAgInPc2d1	související
se	s	k7c7	s
zánikem	zánik	k1gInSc7	zánik
ČSFR	ČSFR	kA	ČSFR
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
řešil	řešit	k5eAaImAgInS	řešit
otázku	otázka	k1gFnSc4	otázka
soudců	soudce	k1gMnPc2	soudce
<g/>
,	,	kIx,	,
vyšetřovatelů	vyšetřovatel	k1gMnPc2	vyšetřovatel
<g/>
,	,	kIx,	,
prokurátorů	prokurátor	k1gMnPc2	prokurátor
a	a	k8xC	a
některých	některý	k3yIgMnPc2	některý
soudních	soudní	k2eAgMnPc2d1	soudní
pracovníků	pracovník	k1gMnPc2	pracovník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
16	[number]	k4	16
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1992	[number]	k4	1992
schválila	schválit	k5eAaPmAgFnS	schválit
Česká	český	k2eAgFnSc1d1	Česká
národní	národní	k2eAgFnSc1d1	národní
rada	rada	k1gFnSc1	rada
novou	nový	k2eAgFnSc4d1	nová
Ústavu	ústava	k1gFnSc4	ústava
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
předtím	předtím	k6eAd1	předtím
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
federace	federace	k1gFnSc2	federace
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
vlastní	vlastní	k2eAgFnSc4d1	vlastní
ústavu	ústava	k1gFnSc4	ústava
neměla	mít	k5eNaImAgFnS	mít
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Též	též	k9	též
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
národní	národní	k2eAgFnSc1d1	národní
rada	rada	k1gFnSc1	rada
přijala	přijmout	k5eAaPmAgFnS	přijmout
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
zánikem	zánik	k1gInSc7	zánik
federace	federace	k1gFnSc2	federace
ústavu	ústav	k1gInSc2	ústav
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Funkci	funkce	k1gFnSc4	funkce
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
Parlamentu	parlament	k1gInSc2	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
převzala	převzít	k5eAaPmAgFnS	převzít
dosavadní	dosavadní	k2eAgFnSc1d1	dosavadní
Česká	český	k2eAgFnSc1d1	Česká
národní	národní	k2eAgFnSc1d1	národní
rada	rada	k1gFnSc1	rada
<g/>
.	.	kIx.	.
</s>
<s>
Též	též	k9	též
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
národní	národní	k2eAgFnSc1d1	národní
rada	rada	k1gFnSc1	rada
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
mandátu	mandát	k1gInSc6	mandát
i	i	k9	i
po	po	k7c6	po
zániku	zánik	k1gInSc6	zánik
federace	federace	k1gFnSc2	federace
jako	jako	k8xS	jako
slovenský	slovenský	k2eAgInSc1d1	slovenský
parlament	parlament	k1gInSc1	parlament
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
26	[number]	k4	26
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1993	[number]	k4	1993
zvolila	zvolit	k5eAaPmAgFnS	zvolit
Poslanecká	poslanecký	k2eAgFnSc1d1	Poslanecká
sněmovna	sněmovna	k1gFnSc1	sněmovna
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
prvním	první	k4xOgMnSc6	první
prezidentem	prezident	k1gMnSc7	prezident
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
Slovensko	Slovensko	k1gNnSc1	Slovensko
se	se	k3xPyFc4	se
na	na	k7c6	na
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
scéně	scéna	k1gFnSc6	scéna
zřetelně	zřetelně	k6eAd1	zřetelně
etablovaly	etablovat	k5eAaBmAgInP	etablovat
jako	jako	k8xS	jako
nástupnické	nástupnický	k2eAgInPc1d1	nástupnický
státy	stát	k1gInPc1	stát
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
tyto	tento	k3xDgInPc1	tento
osamostatněné	osamostatněný	k2eAgInPc1d1	osamostatněný
státy	stát	k1gInPc1	stát
podepisovaly	podepisovat	k5eAaImAgInP	podepisovat
desítky	desítka	k1gFnPc4	desítka
dodatkových	dodatkový	k2eAgFnPc2d1	dodatková
dohod	dohoda	k1gFnPc2	dohoda
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
potvrzovaly	potvrzovat	k5eAaImAgFnP	potvrzovat
platnost	platnost	k1gFnSc4	platnost
dřívějších	dřívější	k2eAgFnPc2d1	dřívější
dvoustranných	dvoustranný	k2eAgFnPc2d1	dvoustranná
smluv	smlouva	k1gFnPc2	smlouva
uzavřených	uzavřený	k2eAgFnPc2d1	uzavřená
federací	federace	k1gFnPc2	federace
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
státy	stát	k1gInPc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
podepsaly	podepsat	k5eAaPmAgInP	podepsat
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
a	a	k8xC	a
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
republika	republika	k1gFnSc1	republika
protokol	protokol	k1gInSc1	protokol
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vymezoval	vymezovat	k5eAaImAgInS	vymezovat
rozdělení	rozdělení	k1gNnSc4	rozdělení
povinností	povinnost	k1gFnPc2	povinnost
vyplývajících	vyplývající	k2eAgFnPc2d1	vyplývající
z	z	k7c2	z
mnohostranných	mnohostranný	k2eAgFnPc2d1	mnohostranná
smluv	smlouva	k1gFnPc2	smlouva
uzavřených	uzavřený	k2eAgInPc2d1	uzavřený
bývalým	bývalý	k2eAgNnSc7d1	bývalé
Československem	Československo	k1gNnSc7	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Češtině	čeština	k1gFnSc3	čeština
přiznává	přiznávat	k5eAaImIp3nS	přiznávat
slovenský	slovenský	k2eAgInSc1d1	slovenský
jazykový	jazykový	k2eAgInSc1d1	jazykový
zákon	zákon	k1gInSc1	zákon
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
postavení	postavení	k1gNnSc1	postavení
jazyka	jazyk	k1gInSc2	jazyk
splňujícího	splňující	k2eAgInSc2d1	splňující
požadavek	požadavek	k1gInSc1	požadavek
základní	základní	k2eAgFnSc2d1	základní
srozumitelnosti	srozumitelnost	k1gFnSc2	srozumitelnost
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
státního	státní	k2eAgInSc2d1	státní
jazyka	jazyk	k1gInSc2	jazyk
a	a	k8xC	a
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
případech	případ	k1gInPc6	případ
používat	používat	k5eAaImF	používat
rovnocenně	rovnocenně	k6eAd1	rovnocenně
ke	k	k7c3	k
slovenštině	slovenština	k1gFnSc3	slovenština
<g/>
;	;	kIx,	;
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
zase	zase	k9	zase
požívá	požívat	k5eAaImIp3nS	požívat
obdobného	obdobný	k2eAgNnSc2d1	obdobné
postavení	postavení	k1gNnSc2	postavení
slovenština	slovenština	k1gFnSc1	slovenština
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
20	[number]	k4	20
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc3	výročí
ustavení	ustavení	k1gNnSc2	ustavení
nových	nový	k2eAgFnPc2d1	nová
republik	republika	k1gFnPc2	republika
se	se	k3xPyFc4	se
český	český	k2eAgInSc1d1	český
i	i	k8xC	i
slovenský	slovenský	k2eAgMnSc1d1	slovenský
prezident	prezident	k1gMnSc1	prezident
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
vyhlásit	vyhlásit	k5eAaPmF	vyhlásit
k	k	k7c3	k
2	[number]	k4	2
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc3	leden
2013	[number]	k4	2013
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
zemích	zem	k1gFnPc6	zem
dílčí	dílčí	k2eAgFnSc4d1	dílčí
amnestii	amnestie	k1gFnSc4	amnestie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rozdělení	rozdělení	k1gNnSc1	rozdělení
státního	státní	k2eAgInSc2d1	státní
majetku	majetek	k1gInSc2	majetek
<g/>
,	,	kIx,	,
závazků	závazek	k1gInPc2	závazek
a	a	k8xC	a
kompetencí	kompetence	k1gFnPc2	kompetence
===	===	k?	===
</s>
</p>
<p>
<s>
Nemovitý	movitý	k2eNgInSc4d1	nemovitý
státní	státní	k2eAgInSc4d1	státní
majetek	majetek	k1gInSc4	majetek
byl	být	k5eAaImAgMnS	být
rozdělený	rozdělený	k2eAgMnSc1d1	rozdělený
podle	podle	k7c2	podle
místa	místo	k1gNnSc2	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nacházel	nacházet	k5eAaImAgMnS	nacházet
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
podle	podle	k7c2	podle
sídla	sídlo	k1gNnSc2	sídlo
právnické	právnický	k2eAgFnSc2d1	právnická
osoby	osoba	k1gFnSc2	osoba
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
vlastnická	vlastnický	k2eAgNnPc1d1	vlastnické
práva	právo	k1gNnPc1	právo
vykonávala	vykonávat	k5eAaImAgNnP	vykonávat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Principem	princip	k1gInSc7	princip
dělení	dělení	k1gNnSc2	dělení
movitého	movitý	k2eAgInSc2d1	movitý
a	a	k8xC	a
zahraničního	zahraniční	k2eAgInSc2d1	zahraniční
majetku	majetek	k1gInSc2	majetek
byl	být	k5eAaImAgInS	být
poměr	poměr	k1gInSc1	poměr
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
29	[number]	k4	29
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1992	[number]	k4	1992
federální	federální	k2eAgMnSc1d1	federální
ministr	ministr	k1gMnSc1	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
Moravčík	Moravčík	k1gMnSc1	Moravčík
podepsal	podepsat	k5eAaPmAgMnS	podepsat
s	s	k7c7	s
oběma	dva	k4xCgMnPc7	dva
národními	národní	k2eAgMnPc7d1	národní
ministry	ministr	k1gMnPc7	ministr
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
(	(	kIx(	(
<g/>
Kňažkem	Kňažek	k1gMnSc7	Kňažek
a	a	k8xC	a
Zielencem	Zielenec	k1gMnSc7	Zielenec
<g/>
)	)	kIx)	)
dohodu	dohoda	k1gFnSc4	dohoda
o	o	k7c4	o
rozdělení	rozdělení	k1gNnSc4	rozdělení
majetku	majetek	k1gInSc2	majetek
federálního	federální	k2eAgNnSc2d1	federální
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
700	[number]	k4	700
z	z	k7c2	z
necelých	celý	k2eNgInPc2d1	necelý
800	[number]	k4	800
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
federálního	federální	k2eAgNnSc2d1	federální
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
přešlo	přejít	k5eAaPmAgNnS	přejít
do	do	k7c2	do
českého	český	k2eAgNnSc2d1	české
nástupnického	nástupnický	k2eAgNnSc2d1	nástupnické
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
dělení	dělení	k1gNnSc6	dělení
zahraničních	zahraniční	k2eAgNnPc2d1	zahraniční
zastupitelství	zastupitelství	k1gNnPc2	zastupitelství
bylo	být	k5eAaImAgNnS	být
využito	využít	k5eAaPmNgNnS	využít
skutečnosti	skutečnost	k1gFnSc3	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
zemích	zem	k1gFnPc6	zem
mělo	mít	k5eAaImAgNnS	mít
československé	československý	k2eAgNnSc1d1	Československé
zastoupení	zastoupení	k1gNnSc1	zastoupení
více	hodně	k6eAd2	hodně
budov	budova	k1gFnPc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
si	se	k3xPyFc3	se
nástupnické	nástupnický	k2eAgInPc1d1	nástupnický
státy	stát	k1gInPc1	stát
musely	muset	k5eAaImAgInP	muset
hledat	hledat	k5eAaImF	hledat
nové	nový	k2eAgFnSc2d1	nová
budovy	budova	k1gFnSc2	budova
–	–	k?	–
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
v	v	k7c6	v
Ottawě	Ottawa	k1gFnSc6	Ottawa
<g/>
,	,	kIx,	,
Římě	Řím	k1gInSc6	Řím
a	a	k8xC	a
Soulu	Soul	k1gInSc6	Soul
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Československé	československý	k2eAgFnPc1d1	Československá
státní	státní	k2eAgFnPc1d1	státní
dráhy	dráha	k1gFnPc1	dráha
byly	být	k5eAaImAgFnP	být
rozděleny	rozdělit	k5eAaPmNgFnP	rozdělit
na	na	k7c4	na
České	český	k2eAgFnPc4d1	Česká
dráhy	dráha	k1gFnPc4	dráha
a	a	k8xC	a
Železnice	železnice	k1gFnPc4	železnice
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Vozový	vozový	k2eAgInSc1d1	vozový
park	park	k1gInSc1	park
byl	být	k5eAaImAgInS	být
dělen	dělit	k5eAaImNgInS	dělit
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ústavní	ústavní	k2eAgInSc1d1	ústavní
zákon	zákon	k1gInSc1	zákon
ČSFR	ČSFR	kA	ČSFR
č.	č.	k?	č.
624	[number]	k4	624
<g/>
/	/	kIx~	/
<g/>
1992	[number]	k4	1992
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
zániku	zánik	k1gInSc6	zánik
funkce	funkce	k1gFnSc2	funkce
soudců	soudce	k1gMnPc2	soudce
a	a	k8xC	a
o	o	k7c6	o
skončení	skončení	k1gNnSc6	skončení
pracovních	pracovní	k2eAgInPc2d1	pracovní
a	a	k8xC	a
služebních	služební	k2eAgInPc2d1	služební
poměrů	poměr	k1gInPc2	poměr
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
se	s	k7c7	s
zánikem	zánik	k1gInSc7	zánik
České	český	k2eAgFnSc2d1	Česká
a	a	k8xC	a
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
Federativní	federativní	k2eAgFnSc2d1	federativní
Republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
ze	z	k7c2	z
dne	den	k1gInSc2	den
17	[number]	k4	17
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1992	[number]	k4	1992
se	se	k3xPyFc4	se
vypořádal	vypořádat	k5eAaPmAgMnS	vypořádat
s	s	k7c7	s
některými	některý	k3yIgMnPc7	některý
pracovníky	pracovník	k1gMnPc7	pracovník
a	a	k8xC	a
funkcionáři	funkcionář	k1gMnPc7	funkcionář
orgánů	orgán	k1gInPc2	orgán
federace	federace	k1gFnSc2	federace
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
17	[number]	k4	17
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
přijalo	přijmout	k5eAaPmAgNnS	přijmout
Federální	federální	k2eAgNnSc1d1	federální
shromáždění	shromáždění	k1gNnSc1	shromáždění
usnesení	usnesení	k1gNnSc2	usnesení
č.	č.	k?	č.
US	US	kA	US
<g/>
72	[number]	k4	72
<g/>
/	/	kIx~	/
<g/>
1992	[number]	k4	1992
o	o	k7c6	o
ústavnosti	ústavnost	k1gFnSc6	ústavnost
procesu	proces	k1gInSc2	proces
zániku	zánik	k1gInSc2	zánik
České	český	k2eAgFnSc2d1	Česká
a	a	k8xC	a
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
Federativní	federativní	k2eAgFnSc2d1	federativní
Republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
vzniku	vznik	k1gInSc2	vznik
nástupnických	nástupnický	k2eAgInPc2d1	nástupnický
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Revize	revize	k1gFnPc4	revize
státní	státní	k2eAgFnSc2d1	státní
hranice	hranice	k1gFnSc2	hranice
===	===	k?	===
</s>
</p>
<p>
<s>
Při	při	k7c6	při
zániku	zánik	k1gInSc6	zánik
Československa	Československo	k1gNnSc2	Československo
byla	být	k5eAaImAgFnS	být
podle	podle	k7c2	podle
Generelní	generelní	k2eAgFnSc2d1	generelní
smlouvy	smlouva	k1gFnSc2	smlouva
(	(	kIx(	(
<g/>
Smlouva	smlouva	k1gFnSc1	smlouva
mezi	mezi	k7c7	mezi
ČR	ČR	kA	ČR
a	a	k8xC	a
SR	SR	kA	SR
o	o	k7c6	o
generelním	generelní	k2eAgNnSc6d1	generelní
vymezení	vymezení	k1gNnSc6	vymezení
společných	společný	k2eAgFnPc2d1	společná
státních	státní	k2eAgFnPc2d1	státní
hranic	hranice	k1gFnPc2	hranice
<g/>
,	,	kIx,	,
podepsaná	podepsaný	k2eAgFnSc1d1	podepsaná
29	[number]	k4	29
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1992	[number]	k4	1992
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
č.	č.	k?	č.
229	[number]	k4	229
<g/>
/	/	kIx~	/
<g/>
1993	[number]	k4	1993
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
ČR	ČR	kA	ČR
<g/>
)	)	kIx)	)
ustanovena	ustanoven	k2eAgFnSc1d1	ustanovena
Společná	společný	k2eAgFnSc1d1	společná
česko-slovenská	českolovenský	k2eAgFnSc1d1	česko-slovenská
rozhraničovací	rozhraničovací	k2eAgFnSc1d1	rozhraničovací
komise	komise	k1gFnSc1	komise
(	(	kIx(	(
<g/>
ČSRK	ČSRK	kA	ČSRK
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozpadem	rozpad	k1gInSc7	rozpad
federace	federace	k1gFnSc2	federace
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
pohraničních	pohraniční	k2eAgFnPc6d1	pohraniční
oblastech	oblast	k1gFnPc6	oblast
obyvatelům	obyvatel	k1gMnPc3	obyvatel
problémy	problém	k1gInPc4	problém
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
předchozí	předchozí	k2eAgFnPc1d1	předchozí
hranice	hranice	k1gFnPc1	hranice
národních	národní	k2eAgFnPc2d1	národní
republik	republika	k1gFnPc2	republika
nebyly	být	k5eNaImAgFnP	být
vytyčené	vytyčený	k2eAgFnPc1d1	vytyčená
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
dostupnost	dostupnost	k1gFnSc4	dostupnost
sídel	sídlo	k1gNnPc2	sídlo
z	z	k7c2	z
příslušné	příslušný	k2eAgFnSc2d1	příslušná
strany	strana	k1gFnSc2	strana
hranice	hranice	k1gFnSc2	hranice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Osada	osada	k1gFnSc1	osada
U	u	k7c2	u
Sabotů	Sabot	k1gInPc2	Sabot
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
jejíž	jejíž	k3xOyRp3gNnSc4	jejíž
označení	označení	k1gNnSc4	označení
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
začal	začít	k5eAaPmAgInS	začít
používat	používat	k5eAaImF	používat
historický	historický	k2eAgInSc1d1	historický
název	název	k1gInSc1	název
Šance	šance	k1gFnSc1	šance
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
kvůli	kvůli	k7c3	kvůli
potřebám	potřeba	k1gFnPc3	potřeba
jejích	její	k3xOp3gMnPc2	její
obyvatel	obyvatel	k1gMnPc2	obyvatel
z	z	k7c2	z
moravské	moravský	k2eAgFnSc2d1	Moravská
obce	obec	k1gFnSc2	obec
Javorník	Javorník	k1gInSc1	Javorník
od	od	k7c2	od
25	[number]	k4	25
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1997	[number]	k4	1997
přičleněna	přičlenit	k5eAaPmNgFnS	přičlenit
ke	k	k7c3	k
slovenské	slovenský	k2eAgFnSc3d1	slovenská
obci	obec	k1gFnSc3	obec
Vrbovce	vrbovka	k1gFnSc3	vrbovka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Osada	osada	k1gFnSc1	osada
Sidónia	Sidónium	k1gNnSc2	Sidónium
(	(	kIx(	(
<g/>
nově	nově	k6eAd1	nově
Sidonie	Sidonie	k1gFnSc1	Sidonie
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
ze	z	k7c2	z
slovenské	slovenský	k2eAgFnSc2d1	slovenská
obce	obec	k1gFnSc2	obec
Horné	horný	k1gMnPc4	horný
Srnie	Srnie	k1gFnSc2	Srnie
od	od	k7c2	od
25	[number]	k4	25
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1997	[number]	k4	1997
přičleněna	přičleněn	k2eAgFnSc1d1	přičleněna
k	k	k7c3	k
moravskému	moravský	k2eAgNnSc3d1	Moravské
městu	město	k1gNnSc3	město
Brumov-Bylnice	Brumov-Bylnice	k1gFnSc2	Brumov-Bylnice
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c4	za
osadu	osada	k1gFnSc4	osada
U	u	k7c2	u
Sabotů	Sabot	k1gInPc2	Sabot
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rekreační	rekreační	k2eAgFnSc1d1	rekreační
oblast	oblast	k1gFnSc1	oblast
Kasárna	kasárna	k1gNnPc4	kasárna
(	(	kIx(	(
<g/>
Kasárňa	Kasárňa	k1gFnSc1	Kasárňa
<g/>
)	)	kIx)	)
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
asi	asi	k9	asi
35	[number]	k4	35
hektarů	hektar	k1gInPc2	hektar
byla	být	k5eAaImAgFnS	být
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
slovenské	slovenský	k2eAgFnSc2d1	slovenská
obce	obec	k1gFnSc2	obec
Makov	Makov	k1gInSc1	Makov
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
ji	on	k3xPp3gFnSc4	on
užívali	užívat	k5eAaImAgMnP	užívat
především	především	k9	především
Češi	Čech	k1gMnPc1	Čech
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
vláda	vláda	k1gFnSc1	vláda
se	se	k3xPyFc4	se
v	v	k7c6	v
letech	let	k1gInPc6	let
1994	[number]	k4	1994
<g/>
–	–	k?	–
<g/>
2001	[number]	k4	2001
snažila	snažit	k5eAaImAgFnS	snažit
tuto	tento	k3xDgFnSc4	tento
oblast	oblast	k1gFnSc4	oblast
získat	získat	k5eAaPmF	získat
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c4	za
jinou	jiný	k2eAgFnSc4d1	jiná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Došlo	dojít	k5eAaPmAgNnS	dojít
zde	zde	k6eAd1	zde
ke	k	k7c3	k
komplikacím	komplikace	k1gFnPc3	komplikace
<g/>
,	,	kIx,	,
když	když	k8xS	když
majitelům	majitel	k1gMnPc3	majitel
nemovitostí	nemovitost	k1gFnPc2	nemovitost
bylo	být	k5eAaImAgNnS	být
bráněno	bránit	k5eAaImNgNnS	bránit
v	v	k7c6	v
užívání	užívání	k1gNnSc6	užívání
dosavadní	dosavadní	k2eAgFnSc2d1	dosavadní
příjezdové	příjezdový	k2eAgFnSc2d1	příjezdová
komunikace	komunikace	k1gFnSc2	komunikace
z	z	k7c2	z
české	český	k2eAgFnSc2d1	Česká
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Pozemky	pozemka	k1gFnPc1	pozemka
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgFnPc6	jenž
stojí	stát	k5eAaImIp3nS	stát
nemovitosti	nemovitost	k1gFnPc4	nemovitost
českých	český	k2eAgMnPc2d1	český
sportovních	sportovní	k2eAgMnPc2d1	sportovní
klubů	klub	k1gInPc2	klub
a	a	k8xC	a
chatařů	chatař	k1gMnPc2	chatař
<g/>
,	,	kIx,	,
prodala	prodat	k5eAaPmAgFnS	prodat
obec	obec	k1gFnSc1	obec
Makov	Makov	k1gInSc1	Makov
dvěma	dva	k4xCgMnPc3	dva
slovenským	slovenský	k2eAgMnPc3d1	slovenský
podnikatelům	podnikatel	k1gMnPc3	podnikatel
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
je	být	k5eAaImIp3nS	být
předtím	předtím	k6eAd1	předtím
nabídla	nabídnout	k5eAaPmAgFnS	nabídnout
vlastníkům	vlastník	k1gMnPc3	vlastník
nemovitostí	nemovitost	k1gFnPc2	nemovitost
na	na	k7c6	na
nich	on	k3xPp3gInPc6	on
postavených	postavený	k2eAgInPc6d1	postavený
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2001	[number]	k4	2001
se	se	k3xPyFc4	se
premiéři	premiér	k1gMnPc1	premiér
obou	dva	k4xCgFnPc2	dva
zemí	zem	k1gFnPc2	zem
od	od	k7c2	od
řešení	řešení	k1gNnSc2	řešení
distancovali	distancovat	k5eAaBmAgMnP	distancovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Smlouva	smlouva	k1gFnSc1	smlouva
mezi	mezi	k7c7	mezi
ČR	ČR	kA	ČR
a	a	k8xC	a
SR	SR	kA	SR
o	o	k7c6	o
společných	společný	k2eAgFnPc6d1	společná
státních	státní	k2eAgFnPc6d1	státní
hranicích	hranice	k1gFnPc6	hranice
byla	být	k5eAaImAgFnS	být
podepsána	podepsán	k2eAgFnSc1d1	podepsána
v	v	k7c6	v
Židlochovicích	Židlochovice	k1gFnPc6	Židlochovice
4	[number]	k4	4
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1996	[number]	k4	1996
s	s	k7c7	s
platností	platnost	k1gFnSc7	platnost
od	od	k7c2	od
25	[number]	k4	25
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
Výsledek	výsledek	k1gInSc1	výsledek
smlouvy	smlouva	k1gFnSc2	smlouva
byl	být	k5eAaImAgInS	být
schválen	schválit	k5eAaPmNgInS	schválit
jako	jako	k8xS	jako
</s>
</p>
<p>
<s>
Ústavní	ústavní	k2eAgInSc1d1	ústavní
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
změnách	změna	k1gFnPc6	změna
státních	státní	k2eAgFnPc2d1	státní
hranic	hranice	k1gFnPc2	hranice
se	s	k7c7	s
Slovenskou	slovenský	k2eAgFnSc7d1	slovenská
republikou	republika	k1gFnSc7	republika
<g/>
,	,	kIx,	,
74	[number]	k4	74
<g/>
/	/	kIx~	/
<g/>
1997	[number]	k4	1997
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
Uzavřena	uzavřen	k2eAgFnSc1d1	uzavřena
byla	být	k5eAaImAgFnS	být
i	i	k9	i
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgFnPc2d1	další
dvoustranných	dvoustranný	k2eAgFnPc2d1	dvoustranná
smluv	smlouva	k1gFnPc2	smlouva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Občanství	občanství	k1gNnSc2	občanství
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
rozdělení	rozdělení	k1gNnSc6	rozdělení
Československa	Československo	k1gNnSc2	Československo
byla	být	k5eAaImAgNnP	být
komplikací	komplikace	k1gFnSc7	komplikace
nutnost	nutnost	k1gFnSc1	nutnost
určit	určit	k5eAaPmF	určit
občanství	občanství	k1gNnSc3	občanství
dosavadních	dosavadní	k2eAgMnPc2d1	dosavadní
československých	československý	k2eAgMnPc2d1	československý
občanů	občan	k1gMnPc2	občan
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgNnSc1d1	státní
občanství	občanství	k1gNnSc1	občanství
nově	nově	k6eAd1	nově
samostatných	samostatný	k2eAgInPc2d1	samostatný
států	stát	k1gInPc2	stát
se	se	k3xPyFc4	se
složitě	složitě	k6eAd1	složitě
určovalo	určovat	k5eAaImAgNnS	určovat
nejen	nejen	k6eAd1	nejen
z	z	k7c2	z
okamžitého	okamžitý	k2eAgNnSc2d1	okamžité
místa	místo	k1gNnSc2	místo
trvalého	trvalý	k2eAgInSc2d1	trvalý
pobytu	pobyt	k1gInSc2	pobyt
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
též	též	k9	též
z	z	k7c2	z
místa	místo	k1gNnSc2	místo
narození	narození	k1gNnSc2	narození
nebo	nebo	k8xC	nebo
občanství	občanství	k1gNnSc4	občanství
rodičů	rodič	k1gMnPc2	rodič
atd.	atd.	kA	atd.
Při	při	k7c6	při
nejbližších	blízký	k2eAgFnPc6d3	nejbližší
volbách	volba	k1gFnPc6	volba
následujících	následující	k2eAgInPc2d1	následující
po	po	k7c6	po
rozdělení	rozdělení	k1gNnSc6	rozdělení
Československa	Československo	k1gNnSc2	Československo
musel	muset	k5eAaImAgInS	muset
mít	mít	k5eAaImF	mít
každý	každý	k3xTgInSc1	každý
volič	volič	k1gInSc1	volič
v	v	k7c6	v
občanském	občanský	k2eAgInSc6d1	občanský
průkaze	průkaz	k1gInSc6	průkaz
potvrzené	potvrzená	k1gFnSc2	potvrzená
občanství	občanství	k1gNnSc4	občanství
příslušné	příslušný	k2eAgFnSc2d1	příslušná
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
se	se	k3xPyFc4	se
existence	existence	k1gFnSc1	existence
českého	český	k2eAgNnSc2d1	české
občanství	občanství	k1gNnSc2	občanství
posuzovala	posuzovat	k5eAaImAgFnS	posuzovat
podle	podle	k7c2	podle
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
39	[number]	k4	39
<g/>
/	/	kIx~	/
<g/>
1969	[number]	k4	1969
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
nabývání	nabývání	k1gNnSc6	nabývání
a	a	k8xC	a
pozbývání	pozbývání	k1gNnSc6	pozbývání
státního	státní	k2eAgNnSc2d1	státní
občanství	občanství	k1gNnSc2	občanství
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Fyzická	fyzický	k2eAgFnSc1d1	fyzická
osoba	osoba	k1gFnSc1	osoba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
k	k	k7c3	k
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosinci	prosinec	k1gInSc3	prosinec
1992	[number]	k4	1992
státním	státní	k2eAgMnSc7d1	státní
občanem	občan	k1gMnSc7	občan
České	český	k2eAgFnSc2d1	Česká
a	a	k8xC	a
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
Federativní	federativní	k2eAgFnSc2d1	federativní
Republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neměla	mít	k5eNaImAgFnS	mít
ani	ani	k9	ani
státní	státní	k2eAgNnSc4d1	státní
občanství	občanství	k1gNnSc4	občanství
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
ani	ani	k8xC	ani
státní	státní	k2eAgNnSc1d1	státní
občanství	občanství	k1gNnSc1	občanství
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
mohla	moct	k5eAaImAgFnS	moct
zvolit	zvolit	k5eAaPmF	zvolit
státní	státní	k2eAgNnSc4d1	státní
občanství	občanství	k1gNnSc4	občanství
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
prohlášením	prohlášení	k1gNnSc7	prohlášení
podle	podle	k7c2	podle
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
40	[number]	k4	40
<g/>
/	/	kIx~	/
<g/>
1993	[number]	k4	1993
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
nabývání	nabývání	k1gNnSc6	nabývání
a	a	k8xC	a
pozbývání	pozbývání	k1gNnSc6	pozbývání
státního	státní	k2eAgNnSc2d1	státní
občanství	občanství	k1gNnSc2	občanství
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Někteří	některý	k3yIgMnPc1	některý
státní	státní	k2eAgMnPc1d1	státní
občané	občan	k1gMnPc1	občan
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
republiky	republika	k1gFnSc2	republika
mohli	moct	k5eAaImAgMnP	moct
nabýt	nabýt	k5eAaPmF	nabýt
státní	státní	k2eAgNnSc4d1	státní
občanství	občanství	k1gNnSc4	občanství
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
zjednodušeným	zjednodušený	k2eAgInSc7d1	zjednodušený
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
prohlášením	prohlášení	k1gNnSc7	prohlášení
podle	podle	k7c2	podle
ustanovení	ustanovení	k1gNnSc2	ustanovení
§	§	k?	§
18	[number]	k4	18
<g/>
a	a	k8xC	a
(	(	kIx(	(
<g/>
osoba	osoba	k1gFnSc1	osoba
od	od	k7c2	od
doby	doba	k1gFnSc2	doba
rozdělení	rozdělení	k1gNnSc2	rozdělení
federace	federace	k1gFnSc2	federace
trvale	trvale	k6eAd1	trvale
žijící	žijící	k2eAgMnPc1d1	žijící
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
,	,	kIx,	,
a	a	k8xC	a
nezletilé	nezletilé	k2eAgNnSc4d1	nezletilé
dítě	dítě	k1gNnSc4	dítě
takové	takový	k3xDgFnSc2	takový
osoby	osoba	k1gFnSc2	osoba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
§	§	k?	§
18	[number]	k4	18
<g/>
b	b	k?	b
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
osoba	osoba	k1gFnSc1	osoba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
slovenského	slovenský	k2eAgMnSc2d1	slovenský
občanství	občanství	k1gNnSc4	občanství
nabyla	nabýt	k5eAaPmAgFnS	nabýt
udělením	udělení	k1gNnSc7	udělení
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
§	§	k?	§
18	[number]	k4	18
<g/>
c	c	k0	c
(	(	kIx(	(
<g/>
osoby	osoba	k1gFnPc1	osoba
ze	z	k7c2	z
smíšených	smíšený	k2eAgFnPc2d1	smíšená
rodin	rodina	k1gFnPc2	rodina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
v	v	k7c6	v
době	doba	k1gFnSc6	doba
rozdělení	rozdělení	k1gNnSc2	rozdělení
federace	federace	k1gFnSc2	federace
byly	být	k5eAaImAgInP	být
nezletilé	nezletilá	k1gFnSc2	nezletilá
<g/>
)	)	kIx)	)
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
40	[number]	k4	40
<g/>
/	/	kIx~	/
<g/>
1993	[number]	k4	1993
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
nabývání	nabývání	k1gNnSc6	nabývání
a	a	k8xC	a
pozbývání	pozbývání	k1gNnSc6	pozbývání
státního	státní	k2eAgNnSc2d1	státní
občanství	občanství	k1gNnSc2	občanství
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
slovenské	slovenský	k2eAgNnSc4d1	slovenské
státní	státní	k2eAgNnSc4d1	státní
občanství	občanství	k1gNnSc4	občanství
jim	on	k3xPp3gMnPc3	on
může	moct	k5eAaImIp3nS	moct
zůstat	zůstat	k5eAaPmF	zůstat
zachováno	zachovat	k5eAaPmNgNnS	zachovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ohlasy	ohlas	k1gInPc1	ohlas
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Postoje	postoj	k1gInPc1	postoj
českých	český	k2eAgMnPc2d1	český
obyvatel	obyvatel	k1gMnPc2	obyvatel
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
provedlo	provést	k5eAaPmAgNnS	provést
Centrum	centrum	k1gNnSc1	centrum
pro	pro	k7c4	pro
výzkum	výzkum	k1gInSc4	výzkum
veřejného	veřejný	k2eAgNnSc2d1	veřejné
mínění	mínění	k1gNnSc2	mínění
výzkum	výzkum	k1gInSc1	výzkum
o	o	k7c6	o
postojích	postoj	k1gInPc6	postoj
obyvatel	obyvatel	k1gMnPc2	obyvatel
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
starších	starý	k2eAgFnPc2d2	starší
15	[number]	k4	15
let	léto	k1gNnPc2	léto
k	k	k7c3	k
rozdělení	rozdělení	k1gNnSc3	rozdělení
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
rozdělování	rozdělování	k1gNnSc2	rozdělování
republiky	republika	k1gFnSc2	republika
bylo	být	k5eAaImAgNnS	být
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
zpětného	zpětný	k2eAgNnSc2d1	zpětné
sebehodnocení	sebehodnocení	k1gNnSc2	sebehodnocení
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
rozdělení	rozdělení	k1gNnSc4	rozdělení
22	[number]	k4	22
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
6	[number]	k4	6
%	%	kIx~	%
spíše	spíše	k9	spíše
a	a	k8xC	a
16	[number]	k4	16
%	%	kIx~	%
rozhodně	rozhodně	k6eAd1	rozhodně
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
rozdělení	rozdělení	k1gNnSc3	rozdělení
60	[number]	k4	60
%	%	kIx~	%
(	(	kIx(	(
<g/>
32	[number]	k4	32
%	%	kIx~	%
spíše	spíše	k9	spíše
a	a	k8xC	a
28	[number]	k4	28
%	%	kIx~	%
rozhodně	rozhodně	k6eAd1	rozhodně
<g/>
)	)	kIx)	)
a	a	k8xC	a
18	[number]	k4	18
%	%	kIx~	%
nevědělo	vědět	k5eNaImAgNnS	vědět
nebo	nebo	k8xC	nebo
zaujalo	zaujmout	k5eAaPmAgNnS	zaujmout
neutrální	neutrální	k2eAgInSc4d1	neutrální
postoj	postoj	k1gInSc4	postoj
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
rozdělení	rozdělení	k1gNnSc3	rozdělení
byli	být	k5eAaImAgMnP	být
častěji	často	k6eAd2	často
starší	starý	k2eAgMnPc1d2	starší
lidé	člověk	k1gMnPc1	člověk
a	a	k8xC	a
voliči	volič	k1gMnPc1	volič
levicových	levicový	k2eAgFnPc2d1	levicová
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
názor	názor	k1gInSc4	názor
pro	pro	k7c4	pro
rozdělení	rozdělení	k1gNnSc4	rozdělení
byl	být	k5eAaImAgInS	být
častější	častý	k2eAgInSc1d2	častější
mezi	mezi	k7c7	mezi
vzdělanějšími	vzdělaný	k2eAgMnPc7d2	vzdělanější
lidmi	člověk	k1gMnPc7	člověk
<g/>
,	,	kIx,	,
podnikateli	podnikatel	k1gMnPc7	podnikatel
a	a	k8xC	a
voliči	volič	k1gMnPc1	volič
ODS	ODS	kA	ODS
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
považovalo	považovat	k5eAaImAgNnS	považovat
tehdejší	tehdejší	k2eAgNnSc1d1	tehdejší
rozdělení	rozdělení	k1gNnSc1	rozdělení
za	za	k7c4	za
správné	správný	k2eAgNnSc4d1	správné
43	[number]	k4	43
%	%	kIx~	%
(	(	kIx(	(
<g/>
30	[number]	k4	30
%	%	kIx~	%
spíše	spíše	k9	spíše
a	a	k8xC	a
13	[number]	k4	13
%	%	kIx~	%
rozhodně	rozhodně	k6eAd1	rozhodně
<g/>
)	)	kIx)	)
a	a	k8xC	a
za	za	k7c4	za
špatné	špatný	k2eAgNnSc4d1	špatné
46	[number]	k4	46
%	%	kIx~	%
(	(	kIx(	(
<g/>
28	[number]	k4	28
%	%	kIx~	%
spíše	spíše	k9	spíše
a	a	k8xC	a
18	[number]	k4	18
%	%	kIx~	%
rozhodně	rozhodně	k6eAd1	rozhodně
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
11	[number]	k4	11
%	%	kIx~	%
nevědělo	vědět	k5eNaImAgNnS	vědět
<g/>
.	.	kIx.	.
</s>
<s>
Věkové	věkový	k2eAgFnPc1d1	věková
a	a	k8xC	a
politické	politický	k2eAgFnPc1d1	politická
korelace	korelace	k1gFnPc1	korelace
byly	být	k5eAaImAgFnP	být
podobné	podobný	k2eAgFnPc1d1	podobná
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
asi	asi	k9	asi
1	[number]	k4	1
%	%	kIx~	%
respondentů	respondent	k1gMnPc2	respondent
změnilo	změnit	k5eAaPmAgNnS	změnit
svůj	svůj	k3xOyFgInSc4	svůj
názor	názor	k1gInSc4	názor
na	na	k7c4	na
rozdělení	rozdělení	k1gNnSc4	rozdělení
k	k	k7c3	k
horšímu	zlý	k2eAgMnSc3d2	horší
(	(	kIx(	(
<g/>
z	z	k7c2	z
pozitivního	pozitivní	k2eAgInSc2d1	pozitivní
postoje	postoj	k1gInSc2	postoj
k	k	k7c3	k
negativnímu	negativní	k2eAgInSc3d1	negativní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
15	[number]	k4	15
%	%	kIx~	%
respondentů	respondent	k1gMnPc2	respondent
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
původně	původně	k6eAd1	původně
byla	být	k5eAaImAgFnS	být
proti	proti	k7c3	proti
rozdělení	rozdělení	k1gNnSc3	rozdělení
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
po	po	k7c6	po
deseti	deset	k4xCc6	deset
letech	let	k1gInPc6	let
již	již	k9	již
s	s	k7c7	s
rozdělením	rozdělení	k1gNnSc7	rozdělení
souhlasila	souhlasit	k5eAaImAgFnS	souhlasit
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
průzkumu	průzkum	k1gInSc2	průzkum
agentury	agentura	k1gFnSc2	agentura
STEM	sto	k4xCgNnSc7	sto
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
rozdělení	rozdělení	k1gNnSc2	rozdělení
jako	jako	k8xC	jako
správný	správný	k2eAgInSc4d1	správný
krok	krok	k1gInSc4	krok
hodnotilo	hodnotit	k5eAaImAgNnS	hodnotit
37	[number]	k4	37
procent	procento	k1gNnPc2	procento
Čechů	Čech	k1gMnPc2	Čech
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
špatný	špatný	k2eAgInSc1d1	špatný
36	[number]	k4	36
procent	procento	k1gNnPc2	procento
a	a	k8xC	a
27	[number]	k4	27
procent	procento	k1gNnPc2	procento
lidí	člověk	k1gMnPc2	člověk
nevědělo	vědět	k5eNaImAgNnS	vědět
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
do	do	k7c2	do
29	[number]	k4	29
let	léto	k1gNnPc2	léto
a	a	k8xC	a
pak	pak	k6eAd1	pak
mezi	mezi	k7c7	mezi
30	[number]	k4	30
a	a	k8xC	a
44	[number]	k4	44
lety	léto	k1gNnPc7	léto
výrazně	výrazně	k6eAd1	výrazně
převažovalo	převažovat	k5eAaImAgNnS	převažovat
kladné	kladný	k2eAgNnSc1d1	kladné
hodnocení	hodnocení	k1gNnSc1	hodnocení
vzniku	vznik	k1gInSc2	vznik
dvou	dva	k4xCgInPc2	dva
samostatných	samostatný	k2eAgInPc2d1	samostatný
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
věkové	věkový	k2eAgFnSc6d1	věková
skupině	skupina	k1gFnSc6	skupina
mezi	mezi	k7c7	mezi
45	[number]	k4	45
až	až	k9	až
59	[number]	k4	59
lety	léto	k1gNnPc7	léto
už	už	k6eAd1	už
mírně	mírně	k6eAd1	mírně
převažoval	převažovat	k5eAaImAgMnS	převažovat
negativní	negativní	k2eAgInSc4d1	negativní
názor	názor	k1gInSc4	názor
a	a	k8xC	a
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
starších	starý	k2eAgNnPc2d2	starší
60	[number]	k4	60
let	léto	k1gNnPc2	léto
bylo	být	k5eAaImAgNnS	být
mínění	mínění	k1gNnSc1	mínění
o	o	k7c6	o
rozpadu	rozpad	k1gInSc6	rozpad
federace	federace	k1gFnSc2	federace
většinově	většinově	k6eAd1	většinově
negativní	negativní	k2eAgMnSc1d1	negativní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
průzkumu	průzkum	k1gInSc2	průzkum
převažovalo	převažovat	k5eAaImAgNnS	převažovat
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
se	s	k7c7	s
základním	základní	k2eAgNnSc7d1	základní
vzděláním	vzdělání	k1gNnSc7	vzdělání
přesvědčení	přesvědčení	k1gNnSc2	přesvědčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
rozpad	rozpad	k1gInSc1	rozpad
Československa	Československo	k1gNnSc2	Československo
byl	být	k5eAaImAgInS	být
špatným	špatný	k2eAgInSc7d1	špatný
krokem	krok	k1gInSc7	krok
a	a	k8xC	a
s	s	k7c7	s
postupně	postupně	k6eAd1	postupně
zvyšujícím	zvyšující	k2eAgMnSc7d1	zvyšující
se	se	k3xPyFc4	se
vzděláním	vzdělání	k1gNnSc7	vzdělání
přibývalo	přibývat	k5eAaImAgNnS	přibývat
kladné	kladný	k2eAgNnSc1d1	kladné
hodnocení	hodnocení	k1gNnSc1	hodnocení
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
dobrý	dobrý	k2eAgInSc4d1	dobrý
krok	krok	k1gInSc4	krok
hodnotilo	hodnotit	k5eAaImAgNnS	hodnotit
rozštěpení	rozštěpení	k1gNnSc1	rozštěpení
federace	federace	k1gFnSc2	federace
51	[number]	k4	51
procent	procento	k1gNnPc2	procento
stoupenců	stoupenec	k1gMnPc2	stoupenec
ODS	ODS	kA	ODS
<g/>
,	,	kIx,	,
52	[number]	k4	52
procent	procento	k1gNnPc2	procento
voličů	volič	k1gMnPc2	volič
TOP	topit	k5eAaImRp2nS	topit
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
,	,	kIx,	,
35	[number]	k4	35
procent	procento	k1gNnPc2	procento
příznivců	příznivec	k1gMnPc2	příznivec
ČSSD	ČSSD	kA	ČSSD
a	a	k8xC	a
24	[number]	k4	24
procent	procento	k1gNnPc2	procento
stoupenců	stoupenec	k1gMnPc2	stoupenec
KSČM	KSČM	kA	KSČM
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
byla	být	k5eAaImAgFnS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
motivem	motiv	k1gInSc7	motiv
vzniku	vznik	k1gInSc2	vznik
společného	společný	k2eAgInSc2d1	společný
státu	stát	k1gInSc2	stát
Čechů	Čech	k1gMnPc2	Čech
a	a	k8xC	a
Slováků	Slovák	k1gMnPc2	Slovák
a	a	k8xC	a
ideje	idea	k1gFnSc2	idea
čechoslovakismu	čechoslovakismus	k1gInSc2	čechoslovakismus
snaha	snaha	k1gFnSc1	snaha
získat	získat	k5eAaPmF	získat
převahu	převaha	k1gFnSc4	převaha
nad	nad	k7c7	nad
německým	německý	k2eAgMnSc7d1	německý
a	a	k8xC	a
maďarským	maďarský	k2eAgNnSc7d1	Maďarské
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vysídlení	vysídlení	k1gNnSc6	vysídlení
Němců	Němec	k1gMnPc2	Němec
z	z	k7c2	z
Československa	Československo	k1gNnSc2	Československo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc1	tento
původní	původní	k2eAgInSc1d1	původní
motiv	motiv	k1gInSc1	motiv
oslaben	oslabit	k5eAaPmNgInS	oslabit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
Češi	Čech	k1gMnPc1	Čech
se	s	k7c7	s
společným	společný	k2eAgInSc7d1	společný
státem	stát	k1gInSc7	stát
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
jeho	jeho	k3xOp3gNnSc2	jeho
trvání	trvání	k1gNnSc2	trvání
identifikovali	identifikovat	k5eAaBmAgMnP	identifikovat
podstatně	podstatně	k6eAd1	podstatně
více	hodně	k6eAd2	hodně
než	než	k8xS	než
Slováci	Slovák	k1gMnPc1	Slovák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2012	[number]	k4	2012
spustilo	spustit	k5eAaPmAgNnS	spustit
sdružení	sdružení	k1gNnSc1	sdružení
Edukace	edukace	k1gFnSc1	edukace
<g/>
@	@	kIx~	@
<g/>
Internet	Internet	k1gInSc1	Internet
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
Centrem	centrum	k1gNnSc7	centrum
jazykového	jazykový	k2eAgNnSc2d1	jazykové
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
českou	český	k2eAgFnSc4d1	Česká
verzi	verze	k1gFnSc4	verze
bezplatného	bezplatný	k2eAgInSc2d1	bezplatný
internetového	internetový	k2eAgInSc2d1	internetový
kurzu	kurz	k1gInSc2	kurz
slovenštiny	slovenština	k1gFnSc2	slovenština
Slovake	Slovake	k1gFnPc2	Slovake
<g/>
.	.	kIx.	.
<g/>
eu	eu	k?	eu
<g/>
.	.	kIx.	.
</s>
<s>
Autoři	autor	k1gMnPc1	autor
to	ten	k3xDgNnSc4	ten
odůvodnili	odůvodnit	k5eAaPmAgMnP	odůvodnit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
mladí	mladý	k2eAgMnPc1d1	mladý
Češi	Čech	k1gMnPc1	Čech
už	už	k6eAd1	už
mají	mít	k5eAaImIp3nP	mít
potíže	potíž	k1gFnPc4	potíž
Slovákům	Slovák	k1gMnPc3	Slovák
rozumět	rozumět	k5eAaImF	rozumět
<g/>
.	.	kIx.	.
</s>
<s>
Průzkum	průzkum	k1gInSc1	průzkum
provedený	provedený	k2eAgInSc1d1	provedený
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
a	a	k8xC	a
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
ukázal	ukázat	k5eAaPmAgInS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
porozumění	porozumění	k1gNnSc1	porozumění
slovenštině	slovenština	k1gFnSc6	slovenština
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
výrazně	výrazně	k6eAd1	výrazně
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
věku	věk	k1gInSc6	věk
respondenta	respondent	k1gMnSc2	respondent
<g/>
.	.	kIx.	.
</s>
<s>
Jazyku	jazyk	k1gInSc6	jazyk
druhého	druhý	k4xOgInSc2	druhý
národa	národ	k1gInSc2	národ
podle	podle	k7c2	podle
výsledků	výsledek	k1gInPc2	výsledek
bez	bez	k7c2	bez
problémů	problém	k1gInPc2	problém
rozumí	rozumět	k5eAaImIp3nS	rozumět
73	[number]	k4	73
%	%	kIx~	%
Slováků	Slovák	k1gMnPc2	Slovák
a	a	k8xC	a
55	[number]	k4	55
%	%	kIx~	%
Čechů	Čech	k1gMnPc2	Čech
<g/>
,	,	kIx,	,
ve	v	k7c6	v
věkové	věkový	k2eAgFnSc6d1	věková
skupině	skupina	k1gFnSc6	skupina
18	[number]	k4	18
<g/>
–	–	k?	–
<g/>
30	[number]	k4	30
let	léto	k1gNnPc2	léto
však	však	k9	však
už	už	k6eAd1	už
slovenštině	slovenština	k1gFnSc3	slovenština
rozumí	rozumět	k5eAaImIp3nS	rozumět
pouze	pouze	k6eAd1	pouze
čtvrtina	čtvrtina	k1gFnSc1	čtvrtina
dotázaných	dotázaný	k2eAgMnPc2d1	dotázaný
Čechů	Čech	k1gMnPc2	Čech
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Postoje	postoj	k1gInPc1	postoj
slovenských	slovenský	k2eAgMnPc2d1	slovenský
obyvatel	obyvatel	k1gMnPc2	obyvatel
====	====	k?	====
</s>
</p>
<p>
<s>
Na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
byla	být	k5eAaImAgFnS	být
snaha	snaha	k1gFnSc1	snaha
se	se	k3xPyFc4	se
osamostatnit	osamostatnit	k5eAaPmF	osamostatnit
politicky	politicky	k6eAd1	politicky
trvale	trvale	k6eAd1	trvale
významným	významný	k2eAgInSc7d1	významný
motivem	motiv	k1gInSc7	motiv
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
projevilo	projevit	k5eAaPmAgNnS	projevit
jak	jak	k6eAd1	jak
vyhlášením	vyhlášení	k1gNnSc7	vyhlášení
samostatného	samostatný	k2eAgInSc2d1	samostatný
státu	stát	k1gInSc2	stát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
federalizací	federalizace	k1gFnSc7	federalizace
Československa	Československo	k1gNnSc2	Československo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
a	a	k8xC	a
výrazným	výrazný	k2eAgInSc7d1	výrazný
úspěchem	úspěch	k1gInSc7	úspěch
politických	politický	k2eAgFnPc2d1	politická
stran	strana	k1gFnPc2	strana
požadujících	požadující	k2eAgNnPc2d1	požadující
osamostatnění	osamostatnění	k1gNnPc2	osamostatnění
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
Menší	malý	k2eAgFnSc4d2	menší
podporu	podpora	k1gFnSc4	podpora
nebo	nebo	k8xC	nebo
žádnou	žádný	k3yNgFnSc4	žádný
mělo	mít	k5eAaImAgNnS	mít
rozdělení	rozdělení	k1gNnSc1	rozdělení
Československa	Československo	k1gNnSc2	Československo
u	u	k7c2	u
slovenských	slovenský	k2eAgMnPc2d1	slovenský
Maďarů	Maďar	k1gMnPc2	Maďar
<g/>
,	,	kIx,	,
Rusínů	Rusín	k1gMnPc2	Rusín
<g/>
,	,	kIx,	,
u	u	k7c2	u
Čechů	Čech	k1gMnPc2	Čech
žijících	žijící	k2eAgMnPc2d1	žijící
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
a	a	k8xC	a
u	u	k7c2	u
Slováků	Slovák	k1gMnPc2	Slovák
žijících	žijící	k2eAgMnPc2d1	žijící
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
smíšených	smíšený	k2eAgFnPc6d1	smíšená
rodinách	rodina	k1gFnPc6	rodina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Postoje	postoj	k1gInPc1	postoj
českých	český	k2eAgMnPc2d1	český
a	a	k8xC	a
slovenských	slovenský	k2eAgMnPc2d1	slovenský
politiků	politik	k1gMnPc2	politik
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2017	[number]	k4	2017
<g/>
,	,	kIx,	,
necelých	celý	k2eNgNnPc2d1	necelé
25	[number]	k4	25
let	léto	k1gNnPc2	léto
od	od	k7c2	od
rozpadu	rozpad	k1gInSc2	rozpad
<g/>
,	,	kIx,	,
oba	dva	k4xCgMnPc1	dva
hlavní	hlavní	k2eAgMnPc1d1	hlavní
aktéři	aktér	k1gMnPc1	aktér
rozpadu	rozpad	k1gInSc2	rozpad
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
a	a	k8xC	a
Vladimír	Vladimír	k1gMnSc1	Vladimír
Mečiar	Mečiar	k1gMnSc1	Mečiar
<g/>
,	,	kIx,	,
prohlásili	prohlásit	k5eAaPmAgMnP	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
rozdělení	rozdělení	k1gNnSc1	rozdělení
Československa	Československo	k1gNnSc2	Československo
bylo	být	k5eAaImAgNnS	být
správným	správný	k2eAgMnSc7d1	správný
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
nezbytným	zbytný	k2eNgNnSc7d1	nezbytné
řešením	řešení	k1gNnSc7	řešení
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Mečiara	Mečiar	k1gMnSc2	Mečiar
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
přestaly	přestat	k5eAaPmAgInP	přestat
federální	federální	k2eAgInPc1d1	federální
orgány	orgán	k1gInPc1	orgán
fungovat	fungovat	k5eAaImF	fungovat
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
Klause	Klaus	k1gMnSc2	Klaus
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
postoje	postoj	k1gInPc1	postoj
Čechů	Čech	k1gMnPc2	Čech
a	a	k8xC	a
Slováků	Slovák	k1gMnPc2	Slovák
ohledně	ohledně	k7c2	ohledně
dalšího	další	k2eAgNnSc2d1	další
uspořádání	uspořádání	k1gNnSc2	uspořádání
státu	stát	k1gInSc2	stát
byly	být	k5eAaImAgInP	být
příliš	příliš	k6eAd1	příliš
odlišné	odlišný	k2eAgInPc1d1	odlišný
<g/>
.	.	kIx.	.
</s>
<s>
Zvolené	zvolený	k2eAgNnSc1d1	zvolené
řešení	řešení	k1gNnSc1	řešení
problému	problém	k1gInSc2	problém
podle	podle	k7c2	podle
Klause	Klaus	k1gMnSc2	Klaus
předešlo	předejít	k5eAaPmAgNnS	předejít
případnému	případný	k2eAgInSc3d1	případný
chaosu	chaos	k1gInSc3	chaos
obdobnému	obdobný	k2eAgInSc3d1	obdobný
stavu	stav	k1gInSc3	stav
v	v	k7c6	v
rozpadající	rozpadající	k2eAgFnSc6d1	rozpadající
se	se	k3xPyFc4	se
Jugoslávii	Jugoslávie	k1gFnSc6	Jugoslávie
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Mečiara	Mečiar	k1gMnSc2	Mečiar
rozpad	rozpad	k1gInSc4	rozpad
státu	stát	k1gInSc2	stát
zlepšil	zlepšit	k5eAaPmAgMnS	zlepšit
vzájemné	vzájemný	k2eAgInPc4d1	vzájemný
vztahy	vztah	k1gInPc4	vztah
mezi	mezi	k7c4	mezi
Čechy	Čech	k1gMnPc4	Čech
a	a	k8xC	a
Slováky	Slovák	k1gMnPc4	Slovák
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
tehdejšího	tehdejší	k2eAgMnSc2d1	tehdejší
premiéra	premiér	k1gMnSc2	premiér
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
Petra	Petr	k1gMnSc2	Petr
Pitharta	Pithart	k1gMnSc2	Pithart
<g/>
,	,	kIx,	,
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
k	k	k7c3	k
otázce	otázka	k1gFnSc3	otázka
rozpadu	rozpad	k1gInSc2	rozpad
federace	federace	k1gFnSc2	federace
vyhlášeno	vyhlásit	k5eAaPmNgNnS	vyhlásit
referendum	referendum	k1gNnSc1	referendum
<g/>
.	.	kIx.	.
</s>
<s>
Plán	plán	k1gInSc1	plán
rozdělit	rozdělit	k5eAaPmF	rozdělit
stát	stát	k1gInSc4	stát
totiž	totiž	k9	totiž
měla	mít	k5eAaImAgFnS	mít
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
předvolebním	předvolební	k2eAgInSc6d1	předvolební
programu	program	k1gInSc6	program
jedna	jeden	k4xCgFnSc1	jeden
jediná	jediný	k2eAgFnSc1d1	jediná
strana	strana	k1gFnSc1	strana
<g/>
.	.	kIx.	.
</s>
<s>
Návrh	návrh	k1gInSc1	návrh
na	na	k7c4	na
rozdělení	rozdělení	k1gNnSc4	rozdělení
státu	stát	k1gInSc2	stát
byl	být	k5eAaImAgInS	být
tedy	tedy	k9	tedy
podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
"	"	kIx"	"
<g/>
nesmyslný	smyslný	k2eNgInSc4d1	nesmyslný
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
je	být	k5eAaImIp3nS	být
otázka	otázka	k1gFnSc1	otázka
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
se	se	k3xPyFc4	se
vůbec	vůbec	k9	vůbec
o	o	k7c6	o
[	[	kIx(	[
<g/>
něm	on	k3xPp3gMnSc6	on
<g/>
]	]	kIx)	]
mělo	mít	k5eAaImAgNnS	mít
jednat	jednat	k5eAaImF	jednat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Zánik	zánik	k1gInSc1	zánik
státu	stát	k1gInSc2	stát
považoval	považovat	k5eAaImAgInS	považovat
za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
osobní	osobní	k2eAgFnSc4d1	osobní
prohru	prohra	k1gFnSc4	prohra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
k	k	k7c3	k
25	[number]	k4	25
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc3	výročí
rozpadu	rozpad	k1gInSc2	rozpad
společného	společný	k2eAgInSc2d1	společný
státu	stát	k1gInSc2	stát
však	však	k9	však
již	již	k6eAd1	již
našel	najít	k5eAaPmAgMnS	najít
v	v	k7c6	v
existenci	existence	k1gFnSc6	existence
dvou	dva	k4xCgInPc2	dva
oddělených	oddělený	k2eAgInPc2d1	oddělený
státu	stát	k1gInSc2	stát
i	i	k9	i
pozitiva	pozitivum	k1gNnPc4	pozitivum
<g/>
:	:	kIx,	:
došlo	dojít	k5eAaPmAgNnS	dojít
např.	např.	kA	např.
ke	k	k7c3	k
zlepšení	zlepšení	k1gNnSc3	zlepšení
vztahů	vztah	k1gInPc2	vztah
mezi	mezi	k7c4	mezi
Čechy	Čech	k1gMnPc4	Čech
a	a	k8xC	a
Slováky	Slovák	k1gMnPc4	Slovák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Zahraniční	zahraniční	k2eAgInPc1d1	zahraniční
ohlasy	ohlas	k1gInPc1	ohlas
====	====	k?	====
</s>
</p>
<p>
<s>
Rozdělení	rozdělení	k1gNnSc1	rozdělení
Československa	Československo	k1gNnSc2	Československo
<g/>
,	,	kIx,	,
v	v	k7c6	v
anglických	anglický	k2eAgNnPc6d1	anglické
médiích	médium	k1gNnPc6	médium
známé	známá	k1gFnSc2	známá
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
Sametový	sametový	k2eAgInSc1d1	sametový
rozvod	rozvod	k1gInSc1	rozvod
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Velvet	Velvet	k1gInSc1	Velvet
Divorce	Divorka	k1gFnSc3	Divorka
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bývalo	bývat	k5eAaImAgNnS	bývat
separatisty	separatista	k1gMnPc7	separatista
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
zemích	zem	k1gFnPc6	zem
uváděno	uvádět	k5eAaImNgNnS	uvádět
jako	jako	k8xS	jako
pozitivní	pozitivní	k2eAgInSc1d1	pozitivní
příklad	příklad	k1gInSc1	příklad
pokojného	pokojný	k2eAgNnSc2d1	pokojné
rozdělení	rozdělení	k1gNnSc2	rozdělení
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Zmiňováno	zmiňován	k2eAgNnSc1d1	zmiňováno
bývá	bývat	k5eAaImIp3nS	bývat
například	například	k6eAd1	například
v	v	k7c6	v
Belgii	Belgie	k1gFnSc6	Belgie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
existuje	existovat	k5eAaImIp3nS	existovat
napětí	napětí	k1gNnSc4	napětí
mezi	mezi	k7c7	mezi
vlámskou	vlámský	k2eAgFnSc7d1	vlámská
částí	část	k1gFnSc7	část
(	(	kIx(	(
<g/>
Flandry	Flandry	k1gInPc7	Flandry
<g/>
)	)	kIx)	)
a	a	k8xC	a
valonskou	valonský	k2eAgFnSc4d1	valonská
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
(	(	kIx(	(
<g/>
Québec	Québec	k1gInSc1	Québec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
RYCHLÍK	Rychlík	k1gMnSc1	Rychlík
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Češi	Čech	k1gMnPc1	Čech
a	a	k8xC	a
Slováci	Slovák	k1gMnPc1	Slovák
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
:	:	kIx,	:
spolupráce	spolupráce	k1gFnSc1	spolupráce
a	a	k8xC	a
konflikty	konflikt	k1gInPc1	konflikt
1914	[number]	k4	1914
<g/>
–	–	k?	–
<g/>
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
688	[number]	k4	688
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7429	[number]	k4	7429
<g/>
-	-	kIx~	-
<g/>
133	[number]	k4	133
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Eric	Eric	k1gFnSc1	Eric
Stein	Stein	k1gMnSc1	Stein
<g/>
:	:	kIx,	:
Česko	Česko	k1gNnSc1	Česko
–	–	k?	–
Slovensko	Slovensko	k1gNnSc1	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Anglicky	anglicky	k6eAd1	anglicky
vydal	vydat	k5eAaPmAgMnS	vydat
The	The	k1gMnSc1	The
University	universita	k1gFnSc2	universita
of	of	k?	of
Michigan	Michigan	k1gInSc1	Michigan
Press	Press	k1gInSc1	Press
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ústava	ústava	k1gFnSc1	ústava
a	a	k8xC	a
ústavní	ústavní	k2eAgInSc1d1	ústavní
řád	řád	k1gInSc1	řád
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
2	[number]	k4	2
díly	díl	k1gInPc1	díl
<g/>
,	,	kIx,	,
nakl	nakl	k1gInSc1	nakl
<g/>
.	.	kIx.	.
</s>
<s>
Linde	Linde	k6eAd1	Linde
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
26	[number]	k4	26
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Den	den	k1gInSc1	den
obnovy	obnova	k1gFnSc2	obnova
samostatného	samostatný	k2eAgInSc2d1	samostatný
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
</s>
</p>
<p>
<s>
Vznik	vznik	k1gInSc1	vznik
Československa	Československo	k1gNnSc2	Československo
</s>
</p>
<p>
<s>
Federalizace	federalizace	k1gFnSc1	federalizace
Československa	Československo	k1gNnSc2	Československo
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc4	dílo
Ústavní	ústavní	k2eAgInSc1d1	ústavní
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
zániku	zánik	k1gInSc6	zánik
České	český	k2eAgFnSc2d1	Česká
a	a	k8xC	a
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
Federativní	federativní	k2eAgFnSc2d1	federativní
Republiky	republika	k1gFnSc2	republika
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Bachora	bachor	k1gMnSc2	bachor
<g/>
,	,	kIx,	,
Miloslav	Miloslav	k1gMnSc1	Miloslav
Bednář	Bednář	k1gMnSc1	Bednář
<g/>
,	,	kIx,	,
Jindřich	Jindřich	k1gMnSc1	Jindřich
Dejmek	Dejmek	k1gMnSc1	Dejmek
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
Hájek	Hájek	k1gMnSc1	Hájek
a	a	k8xC	a
další	další	k2eAgNnSc4d1	další
<g/>
:	:	kIx,	:
Rozdělení	rozdělení	k1gNnSc4	rozdělení
Československa	Československo	k1gNnSc2	Československo
očima	oko	k1gNnPc7	oko
dneška	dnešek	k1gInSc2	dnešek
<g/>
,	,	kIx,	,
Newsletter	Newsletter	k1gInSc1	Newsletter
–	–	k?	–
Centrum	centrum	k1gNnSc1	centrum
pro	pro	k7c4	pro
ekonomiku	ekonomika	k1gFnSc4	ekonomika
a	a	k8xC	a
politiku	politika	k1gFnSc4	politika
<g/>
,	,	kIx,	,
leden	leden	k1gInSc4	leden
2003	[number]	k4	2003
</s>
</p>
<p>
<s>
Peter	Peter	k1gMnSc1	Peter
Just	just	k6eAd1	just
<g/>
:	:	kIx,	:
Československo	Československo	k1gNnSc1	Československo
<g/>
:	:	kIx,	:
obhajoba	obhajoba	k1gFnSc1	obhajoba
společného	společný	k2eAgInSc2d1	společný
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
web	web	k1gInSc1	web
Czech-Slovak	Czech-Slovak	k1gInSc1	Czech-Slovak
Political	Political	k1gMnSc2	Political
Science	Science	k1gFnSc1	Science
Students	Students	k1gInSc1	Students
<g/>
'	'	kIx"	'
Union	union	k1gInSc1	union
</s>
</p>
<p>
<s>
Dvadsať	Dvadsatit	k5eAaPmRp2nS	Dvadsatit
rokov	rokov	k1gInSc1	rokov
od	od	k7c2	od
rozdelenia	rozdelenium	k1gNnSc2	rozdelenium
–	–	k?	–
výskumy	výskum	k1gInPc1	výskum
verejnej	verejnat	k5eAaPmRp2nS	verejnat
mienky	mienka	k1gMnSc2	mienka
v	v	k7c6	v
Českej	Českej	k?	Českej
republike	republike	k1gFnPc6	republike
a	a	k8xC	a
Slovensku	Slovensko	k1gNnSc6	Slovensko
</s>
</p>
<p>
<s>
Speciály	speciál	k1gInPc1	speciál
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
<g/>
:	:	kIx,	:
25	[number]	k4	25
let	léto	k1gNnPc2	léto
od	od	k7c2	od
rozdělení	rozdělení	k1gNnSc2	rozdělení
Československa	Československo	k1gNnSc2	Československo
(	(	kIx(	(
<g/>
Studio	studio	k1gNnSc1	studio
ČT	ČT	kA	ČT
<g/>
24	[number]	k4	24
<g/>
,	,	kIx,	,
Události	událost	k1gFnPc1	událost
<g/>
,	,	kIx,	,
komentáře	komentář	k1gInPc1	komentář
<g/>
,	,	kIx,	,
90	[number]	k4	90
<g/>
'	'	kIx"	'
ČT	ČT	kA	ČT
<g/>
24	[number]	k4	24
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2017	[number]	k4	2017
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
</s>
</p>
