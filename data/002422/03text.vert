<s>
Neuróza	neuróza	k1gFnSc1	neuróza
je	být	k5eAaImIp3nS	být
duševní	duševní	k2eAgFnSc1d1	duševní
porucha	porucha	k1gFnSc1	porucha
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yRgFnSc6	který
pacient	pacient	k1gMnSc1	pacient
trpí	trpět	k5eAaImIp3nS	trpět
stavy	stav	k1gInPc4	stav
úzkosti	úzkost	k1gFnSc2	úzkost
<g/>
,	,	kIx,	,
emoční	emoční	k2eAgFnSc2d1	emoční
tísně	tíseň	k1gFnSc2	tíseň
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
psychologii	psychologie	k1gFnSc6	psychologie
je	být	k5eAaImIp3nS	být
neuróza	neuróza	k1gFnSc1	neuróza
převážně	převážně	k6eAd1	převážně
vykládána	vykládán	k2eAgFnSc1d1	vykládána
jako	jako	k8xS	jako
duševní	duševní	k2eAgFnSc1d1	duševní
nerovnováha	nerovnováha	k1gFnSc1	nerovnováha
<g/>
,	,	kIx,	,
způsobující	způsobující	k2eAgInSc4d1	způsobující
psychický	psychický	k2eAgInSc4d1	psychický
stres	stres	k1gInSc4	stres
neboli	neboli	k8xC	neboli
duševní	duševní	k2eAgFnSc4d1	duševní
tíseň	tíseň	k1gFnSc4	tíseň
<g/>
.	.	kIx.	.
</s>
<s>
Emoční	emoční	k2eAgFnSc1d1	emoční
tíseň	tíseň	k1gFnSc1	tíseň
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
ve	v	k7c6	v
fyziologické	fyziologický	k2eAgFnSc6d1	fyziologická
a	a	k8xC	a
duševní	duševní	k2eAgFnSc6d1	duševní
nerovnováze	nerovnováha	k1gFnSc6	nerovnováha
(	(	kIx(	(
<g/>
fobie	fobie	k1gFnSc1	fobie
<g/>
,	,	kIx,	,
úzkost	úzkost	k1gFnSc1	úzkost
<g/>
,	,	kIx,	,
somatické	somatický	k2eAgInPc1d1	somatický
projevy	projev	k1gInPc1	projev
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
kulturách	kultura	k1gFnPc6	kultura
západního	západní	k2eAgInSc2d1	západní
typu	typ	k1gInSc2	typ
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc4	tento
pojem	pojem	k1gInSc4	pojem
nepoužívá	používat	k5eNaImIp3nS	používat
i	i	k9	i
všeobecně	všeobecně	k6eAd1	všeobecně
nezná	znát	k5eNaImIp3nS	znát
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Velmi	velmi	k6eAd1	velmi
zjednodušeně	zjednodušeně	k6eAd1	zjednodušeně
je	být	k5eAaImIp3nS	být
definována	definován	k2eAgFnSc1d1	definována
jako	jako	k8xC	jako
trvalejší	trvalý	k2eAgFnSc1d2	trvalejší
neschopnost	neschopnost	k1gFnSc1	neschopnost
přizpůsobit	přizpůsobit	k5eAaPmF	přizpůsobit
se	se	k3xPyFc4	se
prostředí	prostředí	k1gNnSc2	prostředí
(	(	kIx(	(
<g/>
vyrovnat	vyrovnat	k5eAaPmF	vyrovnat
se	se	k3xPyFc4	se
se	s	k7c7	s
vzniklými	vzniklý	k2eAgInPc7d1	vzniklý
stresory	stresor	k1gInPc7	stresor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
změnit	změnit	k5eAaPmF	změnit
životní	životní	k2eAgInPc4d1	životní
návyky	návyk	k1gInPc4	návyk
a	a	k8xC	a
neschopnost	neschopnost	k1gFnSc4	neschopnost
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
svou	svůj	k3xOyFgFnSc4	svůj
osobnost	osobnost	k1gFnSc4	osobnost
ve	v	k7c6	v
vzniklé	vzniklý	k2eAgFnSc6d1	vzniklá
situaci	situace	k1gFnSc6	situace
v	v	k7c4	v
uspokojivější	uspokojivý	k2eAgInSc4d2	uspokojivější
komplexnější	komplexní	k2eAgInSc4d2	komplexnější
celek	celek	k1gInSc4	celek
<g/>
.	.	kIx.	.
</s>
<s>
Sigmund	Sigmund	k1gMnSc1	Sigmund
Freud	Freud	k1gMnSc1	Freud
popisuje	popisovat	k5eAaImIp3nS	popisovat
neurózu	neuróza	k1gFnSc4	neuróza
jako	jako	k8xS	jako
konflikt	konflikt	k1gInSc4	konflikt
mezi	mezi	k7c4	mezi
id	idy	k1gFnPc2	idy
a	a	k8xC	a
superegem	superego	k1gNnSc7	superego
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
pojem	pojem	k1gInSc1	pojem
duševní	duševní	k2eAgFnSc2d1	duševní
poruchy	porucha	k1gFnSc2	porucha
má	mít	k5eAaImIp3nS	mít
často	často	k6eAd1	často
politický	politický	k2eAgInSc1d1	politický
problém	problém	k1gInSc1	problém
s	s	k7c7	s
uznáním	uznání	k1gNnSc7	uznání
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
ekonomicky	ekonomicky	k6eAd1	ekonomicky
a	a	k8xC	a
politicky	politicky	k6eAd1	politicky
nežádoucí	žádoucí	k2eNgFnPc1d1	nežádoucí
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Všichni	všechen	k3xTgMnPc1	všechen
jsou	být	k5eAaImIp3nP	být
šťastní	šťastný	k2eAgMnPc1d1	šťastný
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
všechno	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
každý	každý	k3xTgInSc1	každý
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
vše	všechen	k3xTgNnSc4	všechen
potřebné	potřebné	k1gNnSc4	potřebné
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Podle	podle	k7c2	podle
S.	S.	kA	S.
Pfeifera	Pfeifera	k1gFnSc1	Pfeifera
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
neurotické	neurotický	k2eAgFnPc1d1	neurotická
potíže	potíž	k1gFnPc1	potíž
nejčetnější	četný	k2eAgFnSc1d3	nejčetnější
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
mezi	mezi	k7c7	mezi
25	[number]	k4	25
<g/>
.	.	kIx.	.
a	a	k8xC	a
40	[number]	k4	40
<g/>
.	.	kIx.	.
rokem	rok	k1gInSc7	rok
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
člověk	člověk	k1gMnSc1	člověk
musí	muset	k5eAaImIp3nS	muset
vyrovnávat	vyrovnávat	k5eAaImF	vyrovnávat
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
nároky	nárok	k1gInPc7	nárok
dospělosti	dospělost	k1gFnSc2	dospělost
<g/>
,	,	kIx,	,
nových	nový	k2eAgFnPc2d1	nová
rolí	role	k1gFnPc2	role
a	a	k8xC	a
s	s	k7c7	s
odpovědností	odpovědnost	k1gFnSc7	odpovědnost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
si	se	k3xPyFc3	se
hledá	hledat	k5eAaImIp3nS	hledat
manželského	manželský	k2eAgMnSc4d1	manželský
partnera	partner	k1gMnSc4	partner
<g/>
,	,	kIx,	,
zakládá	zakládat	k5eAaImIp3nS	zakládat
rodinu	rodina	k1gFnSc4	rodina
a	a	k8xC	a
buduje	budovat	k5eAaImIp3nS	budovat
si	se	k3xPyFc3	se
svou	svůj	k3xOyFgFnSc4	svůj
profesní	profesní	k2eAgFnSc4d1	profesní
roli	role	k1gFnSc4	role
<g/>
.	.	kIx.	.
</s>
<s>
Nároky	nárok	k1gInPc1	nárok
takových	takový	k3xDgFnPc2	takový
změn	změna	k1gFnPc2	změna
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
pro	pro	k7c4	pro
disponované	disponovaný	k2eAgMnPc4d1	disponovaný
jedince	jedinec	k1gMnPc4	jedinec
příliš	příliš	k6eAd1	příliš
velké	velký	k2eAgFnPc1d1	velká
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
vést	vést	k5eAaImF	vést
k	k	k7c3	k
neurotickým	neurotický	k2eAgFnPc3d1	neurotická
poruchám	porucha	k1gFnPc3	porucha
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc7	jejich
důsledkem	důsledek	k1gInSc7	důsledek
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
snížená	snížený	k2eAgFnSc1d1	snížená
výkonnost	výkonnost	k1gFnSc1	výkonnost
i	i	k8xC	i
životní	životní	k2eAgFnSc1d1	životní
energie	energie	k1gFnSc1	energie
a	a	k8xC	a
omezená	omezený	k2eAgFnSc1d1	omezená
radost	radost	k1gFnSc1	radost
ze	z	k7c2	z
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Příčinou	příčina	k1gFnSc7	příčina
neuróz	neuróza	k1gFnPc2	neuróza
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
silný	silný	k2eAgInSc4d1	silný
otřes	otřes	k1gInSc4	otřes
<g/>
,	,	kIx,	,
silný	silný	k2eAgInSc4d1	silný
dlouhodobý	dlouhodobý	k2eAgInSc4d1	dlouhodobý
stres	stres	k1gInSc4	stres
(	(	kIx(	(
<g/>
úmrtí	úmrtí	k1gNnSc2	úmrtí
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
<g/>
,	,	kIx,	,
šikana	šikana	k1gFnSc1	šikana
<g/>
,	,	kIx,	,
znásilnění	znásilnění	k1gNnSc1	znásilnění
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
frustrace	frustrace	k1gFnSc1	frustrace
i	i	k8xC	i
deprivace	deprivace	k1gFnSc1	deprivace
<g/>
,	,	kIx,	,
dlouhodobé	dlouhodobý	k2eAgNnSc1d1	dlouhodobé
působení	působení	k1gNnSc1	působení
traumatických	traumatický	k2eAgInPc2d1	traumatický
vlivů	vliv	k1gInPc2	vliv
(	(	kIx(	(
<g/>
špatná	špatný	k2eAgFnSc1d1	špatná
výchova	výchova	k1gFnSc1	výchova
-	-	kIx~	-
nedostatek	nedostatek	k1gInSc1	nedostatek
citu	cit	k1gInSc2	cit
<g/>
,	,	kIx,	,
přehnané	přehnaný	k2eAgInPc1d1	přehnaný
požadavky	požadavek	k1gInPc1	požadavek
rodičů	rodič	k1gMnPc2	rodič
<g/>
,	,	kIx,	,
nesoulad	nesoulad	k1gInSc1	nesoulad
mezi	mezi	k7c7	mezi
rodiči	rodič	k1gMnPc7	rodič
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
třeba	třeba	k6eAd1	třeba
nevhodné	vhodný	k2eNgNnSc1d1	nevhodné
a	a	k8xC	a
hlavně	hlavně	k9	hlavně
tvrdé	tvrdý	k2eAgNnSc4d1	tvrdé
životní	životní	k2eAgNnSc4d1	životní
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
,	,	kIx,	,
podmínky	podmínka	k1gFnPc4	podmínka
v	v	k7c6	v
práci	práce	k1gFnSc6	práce
<g/>
,	,	kIx,	,
rodině	rodina	k1gFnSc6	rodina
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
Neurotické	neurotický	k2eAgInPc1d1	neurotický
příznaky	příznak	k1gInPc1	příznak
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Psychické	psychický	k2eAgInPc1d1	psychický
příznaky	příznak	k1gInPc1	příznak
<g/>
:	:	kIx,	:
Poruchy	poruch	k1gInPc1	poruch
emocí	emoce	k1gFnPc2	emoce
-	-	kIx~	-
strach	strach	k1gInSc1	strach
<g/>
,	,	kIx,	,
event.	event.	k?	event.
až	až	k8xS	až
fobie	fobie	k1gFnSc1	fobie
<g/>
,	,	kIx,	,
úzkost	úzkost	k1gFnSc1	úzkost
<g/>
,	,	kIx,	,
deprese	deprese	k1gFnSc1	deprese
Poruchy	porucha	k1gFnSc2	porucha
myšlení	myšlení	k1gNnSc2	myšlení
-	-	kIx~	-
obsese	obsese	k1gFnPc1	obsese
(	(	kIx(	(
<g/>
vtíravé	vtíravý	k2eAgFnPc1d1	vtíravá
myšlenky	myšlenka	k1gFnPc1	myšlenka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poruchy	porucha	k1gFnPc4	porucha
hodnocení	hodnocení	k1gNnSc2	hodnocení
a	a	k8xC	a
sebehodnocení	sebehodnocení	k1gNnSc2	sebehodnocení
Poruchy	porucha	k1gFnSc2	porucha
paměti	paměť	k1gFnSc2	paměť
(	(	kIx(	(
<g/>
amnézie	amnézie	k1gFnSc1	amnézie
<g/>
)	)	kIx)	)
Poruchy	poruch	k1gInPc1	poruch
pozornosti	pozornost	k1gFnSc2	pozornost
-	-	kIx~	-
porucha	porucha	k1gFnSc1	porucha
koncentrace	koncentrace	k1gFnSc1	koncentrace
<g/>
,	,	kIx,	,
snadná	snadný	k2eAgFnSc1d1	snadná
unavitelnost	unavitelnost	k1gFnSc1	unavitelnost
Poruchy	porucha	k1gFnSc2	porucha
autoregulace	autoregulace	k1gFnSc2	autoregulace
-	-	kIx~	-
kompulze	kompulze	k1gFnPc1	kompulze
(	(	kIx(	(
<g/>
nutkavá	nutkavý	k2eAgNnPc1d1	nutkavé
jednání	jednání	k1gNnPc1	jednání
<g/>
)	)	kIx)	)
Poruchy	poruch	k1gInPc1	poruch
spánku	spánek	k1gInSc2	spánek
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Somatické	somatický	k2eAgInPc4d1	somatický
(	(	kIx(	(
<g/>
vegetativní	vegetativní	k2eAgInPc4d1	vegetativní
<g/>
)	)	kIx)	)
příznaky	příznak	k1gInPc4	příznak
<g/>
:	:	kIx,	:
Třes	třes	k1gInSc1	třes
<g/>
,	,	kIx,	,
bolesti	bolest	k1gFnPc1	bolest
<g/>
,	,	kIx,	,
poruchy	porucha	k1gFnPc1	porucha
zažívání	zažívání	k1gNnSc2	zažívání
či	či	k8xC	či
vyměšování	vyměšování	k1gNnSc2	vyměšování
aj.	aj.	kA	aj.
Emoční	emoční	k2eAgNnSc1d1	emoční
prožívání	prožívání	k1gNnSc1	prožívání
člověka	člověk	k1gMnSc2	člověk
trpícího	trpící	k2eAgMnSc2d1	trpící
neurózou	neuróza	k1gFnSc7	neuróza
je	být	k5eAaImIp3nS	být
převážně	převážně	k6eAd1	převážně
negativní	negativní	k2eAgMnSc1d1	negativní
<g/>
,	,	kIx,	,
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
subjektivně	subjektivně	k6eAd1	subjektivně
nepříjemné	příjemný	k2eNgNnSc1d1	nepříjemné
<g/>
.	.	kIx.	.
</s>
<s>
Bývá	bývat	k5eAaImIp3nS	bývat
pro	pro	k7c4	pro
ně	on	k3xPp3gFnPc4	on
typická	typický	k2eAgFnSc1d1	typická
zvýšená	zvýšený	k2eAgFnSc1d1	zvýšená
úzkost	úzkost	k1gFnSc1	úzkost
a	a	k8xC	a
strach	strach	k1gInSc1	strach
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
přinášejí	přinášet	k5eAaImIp3nP	přinášet
jedinci	jedinec	k1gMnPc7	jedinec
značné	značný	k2eAgFnSc2d1	značná
utrpení	utrpení	k1gNnSc3	utrpení
a	a	k8xC	a
komplikují	komplikovat	k5eAaBmIp3nP	komplikovat
život	život	k1gInSc1	život
jemu	on	k3xPp3gMnSc3	on
i	i	k9	i
jeho	jeho	k3xOp3gNnSc2	jeho
okolí	okolí	k1gNnSc2	okolí
Neurózy	neuróza	k1gFnSc2	neuróza
se	se	k3xPyFc4	se
projevují	projevovat	k5eAaImIp3nP	projevovat
často	často	k6eAd1	často
jako	jako	k8xC	jako
zlozvyky	zlozvyk	k1gInPc1	zlozvyk
-	-	kIx~	-
okusování	okusování	k1gNnSc1	okusování
nehtů	nehet	k1gInPc2	nehet
<g/>
,	,	kIx,	,
stereotypní	stereotypní	k2eAgInPc1d1	stereotypní
pohyby	pohyb	k1gInPc1	pohyb
<g/>
,	,	kIx,	,
tiky	tik	k1gInPc1	tik
<g/>
,	,	kIx,	,
grimasy	grimasa	k1gFnPc1	grimasa
<g/>
,	,	kIx,	,
koktavost	koktavost	k1gFnSc1	koktavost
<g/>
,	,	kIx,	,
noční	noční	k2eAgNnSc1d1	noční
pomočování	pomočování	k1gNnSc1	pomočování
<g/>
,	,	kIx,	,
bruxismus	bruxismus	k1gInSc1	bruxismus
<g/>
,	,	kIx,	,
bolest	bolest	k1gFnSc1	bolest
hlavy	hlava	k1gFnSc2	hlava
či	či	k8xC	či
zad	záda	k1gNnPc2	záda
<g/>
,	,	kIx,	,
podrážděnost	podrážděnost	k1gFnSc1	podrážděnost
<g/>
,	,	kIx,	,
nerozhodnost	nerozhodnost	k1gFnSc1	nerozhodnost
<g/>
,	,	kIx,	,
agresivita	agresivita	k1gFnSc1	agresivita
<g/>
,	,	kIx,	,
zvýšená	zvýšený	k2eAgFnSc1d1	zvýšená
meteosenzitivita	meteosenzitivita	k1gFnSc1	meteosenzitivita
<g/>
...	...	k?	...
Chování	chování	k1gNnSc1	chování
člověka	člověk	k1gMnSc2	člověk
trpícího	trpící	k2eAgMnSc2d1	trpící
neurotickou	neurotický	k2eAgFnSc7d1	neurotická
poruchou	porucha	k1gFnSc7	porucha
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
nápadné	nápadný	k2eAgNnSc1d1	nápadné
a	a	k8xC	a
neobvyklé	obvyklý	k2eNgNnSc1d1	neobvyklé
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neporušuje	porušovat	k5eNaImIp3nS	porušovat
základní	základní	k2eAgFnPc4d1	základní
sociální	sociální	k2eAgFnPc4d1	sociální
normy	norma	k1gFnPc4	norma
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
hodnocení	hodnocení	k1gNnSc1	hodnocení
reality	realita	k1gFnSc2	realita
není	být	k5eNaImIp3nS	být
zásadním	zásadní	k2eAgInSc7d1	zásadní
způsobem	způsob	k1gInSc7	způsob
narušeno	narušen	k2eAgNnSc1d1	narušeno
<g/>
.	.	kIx.	.
</s>
<s>
Postižený	postižený	k2eAgMnSc1d1	postižený
člověk	člověk	k1gMnSc1	člověk
si	se	k3xPyFc3	se
zpravidla	zpravidla	k6eAd1	zpravidla
často	často	k6eAd1	často
odlišnost	odlišnost	k1gFnSc1	odlišnost
svých	svůj	k3xOyFgInPc2	svůj
projevů	projev	k1gInPc2	projev
uvědomuje	uvědomovat	k5eAaImIp3nS	uvědomovat
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
i	i	k9	i
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gNnSc4	jeho
nejbližší	blízký	k2eAgNnSc4d3	nejbližší
okolí	okolí	k1gNnSc4	okolí
obtěžující	obtěžující	k2eAgNnSc4d1	obtěžující
<g/>
,	,	kIx,	,
málokdy	málokdy	k6eAd1	málokdy
však	však	k9	však
zná	znát	k5eAaImIp3nS	znát
jeho	jeho	k3xOp3gFnPc4	jeho
příčiny	příčina	k1gFnPc4	příčina
<g/>
.	.	kIx.	.
</s>
<s>
Interakce	interakce	k1gFnSc1	interakce
s	s	k7c7	s
neurotiky	neurotik	k1gMnPc7	neurotik
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
daleko	daleko	k6eAd1	daleko
obtížnější	obtížný	k2eAgMnSc1d2	obtížnější
<g/>
.	.	kIx.	.
</s>
<s>
Vážnější	vážní	k2eAgFnPc1d2	vážnější
neurózy	neuróza	k1gFnPc1	neuróza
se	se	k3xPyFc4	se
léčí	léčit	k5eAaImIp3nP	léčit
obvykle	obvykle	k6eAd1	obvykle
psychoterapií	psychoterapie	k1gFnPc2	psychoterapie
<g/>
.	.	kIx.	.
</s>
<s>
Fobické	fobický	k2eAgInPc1d1	fobický
úzkostné	úzkostný	k2eAgInPc1d1	úzkostný
poruchy	poruch	k1gInPc1	poruch
<g/>
:	:	kIx,	:
Agorafobie	agorafobie	k1gFnSc1	agorafobie
Sociální	sociální	k2eAgFnSc1d1	sociální
fobie	fobie	k1gFnSc1	fobie
Specifické	specifický	k2eAgFnSc2d1	specifická
fobie	fobie	k1gFnSc2	fobie
Ostatní	ostatní	k2eAgFnSc2d1	ostatní
úzkostné	úzkostný	k2eAgFnSc2d1	úzkostná
poruchy	porucha	k1gFnSc2	porucha
<g/>
:	:	kIx,	:
Panická	panický	k2eAgFnSc1d1	panická
porucha	porucha	k1gFnSc1	porucha
-	-	kIx~	-
Opakované	opakovaný	k2eAgInPc1d1	opakovaný
záchvaty	záchvat	k1gInPc1	záchvat
náhle	náhle	k6eAd1	náhle
vzniklého	vzniklý	k2eAgInSc2d1	vzniklý
pocitu	pocit	k1gInSc2	pocit
strachu	strach	k1gInSc2	strach
a	a	k8xC	a
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
nepohody	nepohoda	k1gFnSc2	nepohoda
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
bez	bez	k7c2	bez
zjevné	zjevný	k2eAgFnSc2d1	zjevná
příčiny	příčina	k1gFnSc2	příčina
<g/>
.	.	kIx.	.
</s>
<s>
Objevují	objevovat	k5eAaImIp3nP	objevovat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
vegetativní	vegetativní	k2eAgInPc1d1	vegetativní
projevy	projev	k1gInPc1	projev
-	-	kIx~	-
hyperventilace	hyperventilace	k1gFnSc1	hyperventilace
<g/>
,	,	kIx,	,
pocení	pocení	k1gNnSc1	pocení
<g/>
,	,	kIx,	,
pocit	pocit	k1gInSc1	pocit
tíhy	tíha	k1gFnSc2	tíha
na	na	k7c6	na
prsou	prsa	k1gNnPc6	prsa
<g/>
,	,	kIx,	,
pocity	pocit	k1gInPc4	pocit
na	na	k7c4	na
omdlení	omdlení	k1gNnSc4	omdlení
<g/>
,	,	kIx,	,
nevolnosti	nevolnost	k1gFnSc6	nevolnost
atp.	atp.	kA	atp.
Generalizovaná	generalizovaný	k2eAgFnSc1d1	generalizovaná
úzkostná	úzkostný	k2eAgFnSc1d1	úzkostná
porucha	porucha	k1gFnSc1	porucha
Smíšená	smíšený	k2eAgFnSc1d1	smíšená
úzkostně	úzkostně	k6eAd1	úzkostně
depresivní	depresivní	k2eAgFnSc1d1	depresivní
porucha	porucha	k1gFnSc1	porucha
Obsedantně	obsedantně	k6eAd1	obsedantně
kompulzivní	kompulzivní	k2eAgFnSc1d1	kompulzivní
porucha	porucha	k1gFnSc1	porucha
Akutní	akutní	k2eAgFnSc2d1	akutní
reakce	reakce	k1gFnSc2	reakce
na	na	k7c4	na
stres	stres	k1gInSc4	stres
Posttraumatická	posttraumatický	k2eAgFnSc1d1	posttraumatická
stresová	stresový	k2eAgFnSc1d1	stresová
porucha	porucha	k1gFnSc1	porucha
Porucha	porucha	k1gFnSc1	porucha
přizpůsobení	přizpůsobení	k1gNnSc2	přizpůsobení
Disociativní	Disociativní	k2eAgFnSc2d1	Disociativní
poruchy	porucha	k1gFnSc2	porucha
Somatoformní	Somatoformní	k2eAgFnSc2d1	Somatoformní
poruchy	porucha	k1gFnSc2	porucha
<g/>
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s>
Somatizační	Somatizační	k2eAgFnSc1d1	Somatizační
porucha	porucha	k1gFnSc1	porucha
Hypochondrická	hypochondrický	k2eAgFnSc1d1	hypochondrická
porucha	porucha	k1gFnSc1	porucha
Dysmorfofobie	Dysmorfofobie	k1gFnSc2	Dysmorfofobie
Somatoformní	Somatoformní	k2eAgFnSc1d1	Somatoformní
vegetativní	vegetativní	k2eAgFnSc1d1	vegetativní
dysfunkce	dysfunkce	k1gFnSc1	dysfunkce
Jiné	jiný	k2eAgFnSc2d1	jiná
neurotické	neurotický	k2eAgFnSc2d1	neurotická
poruchy	porucha	k1gFnSc2	porucha
<g/>
:	:	kIx,	:
Neurastenie	neurastenie	k1gFnSc1	neurastenie
Fobie	fobie	k1gFnSc1	fobie
-	-	kIx~	-
neodůvodněný	odůvodněný	k2eNgInSc1d1	neodůvodněný
strach	strach	k1gInSc1	strach
Hypochondrie	hypochondrie	k1gFnSc1	hypochondrie
-	-	kIx~	-
terapie	terapie	k1gFnSc1	terapie
placebo	placebo	k1gNnSc1	placebo
Mentální	mentální	k2eAgFnSc2d1	mentální
anorexie	anorexie	k1gFnSc2	anorexie
-	-	kIx~	-
odmítá	odmítat	k5eAaImIp3nS	odmítat
přijmout	přijmout	k5eAaPmF	přijmout
sám	sám	k3xTgMnSc1	sám
sebe	sebe	k3xPyFc4	sebe
(	(	kIx(	(
<g/>
ženskou	ženský	k2eAgFnSc4d1	ženská
roli	role	k1gFnSc4	role
<g/>
)	)	kIx)	)
Hysterie	hysterie	k1gFnSc1	hysterie
-	-	kIx~	-
citová	citový	k2eAgFnSc1d1	citová
labilita	labilita	k1gFnSc1	labilita
se	s	k7c7	s
sklonem	sklon	k1gInSc7	sklon
k	k	k7c3	k
afektům	afekt	k1gInPc3	afekt
Úzkostné	úzkostný	k2eAgFnSc2d1	úzkostná
poruchy	porucha	k1gFnSc2	porucha
Úzkostná	úzkostný	k2eAgFnSc1d1	úzkostná
porucha	porucha	k1gFnSc1	porucha
Obsedantně	obsedantně	k6eAd1	obsedantně
kompulzivní	kompulzivní	k2eAgFnSc1d1	kompulzivní
porucha	porucha	k1gFnSc1	porucha
Obsedantní	obsedantní	k2eAgFnSc1d1	obsedantní
neuróza	neuróza	k1gFnSc1	neuróza
Generalizovaná	generalizovaný	k2eAgFnSc1d1	generalizovaná
úzkostná	úzkostný	k2eAgFnSc1d1	úzkostná
porucha	porucha	k1gFnSc1	porucha
Posttraumatická	posttraumatický	k2eAgFnSc1d1	posttraumatická
stresová	stresový	k2eAgFnSc1d1	stresová
porucha	porucha	k1gFnSc1	porucha
Agorafobie	agorafobie	k1gFnSc2	agorafobie
Specifické	specifický	k2eAgFnSc2d1	specifická
fobie	fobie	k1gFnSc2	fobie
Depersonalizace	depersonalizace	k1gFnSc2	depersonalizace
</s>
