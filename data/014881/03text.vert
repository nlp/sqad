<s>
Ústní	ústní	k2eAgNnSc1d1
podání	podání	k1gNnSc1
</s>
<s>
Možná	možná	k9
hledáte	hledat	k5eAaImIp2nP
<g/>
:	:	kIx,
Perorální	perorální	k2eAgNnSc1d1
podání	podání	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Ústní	ústní	k2eAgNnSc1d1
podání	podání	k1gNnSc1
<g/>
,	,	kIx,
projev	projev	k1gInSc1
nebo	nebo	k8xC
sdělení	sdělení	k1gNnSc1
je	být	k5eAaImIp3nS
předání	předání	k1gNnSc4
informace	informace	k1gFnSc2
případně	případně	k6eAd1
emoce	emoce	k1gFnSc1
mezi	mezi	k7c7
lidmi	člověk	k1gMnPc7
prostřednictvím	prostřednictvím	k7c2
ústní	ústní	k2eAgFnSc2d1
komunikace	komunikace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
stejně	stejně	k6eAd1
jednoduché	jednoduchý	k2eAgNnSc1d1
jako	jako	k8xS,k8xC
říct	říct	k5eAaPmF
někomu	někdo	k3yInSc3
kolik	kolik	k4yQc4,k4yIc4,k4yRc4
je	být	k5eAaImIp3nS
hodin	hodina	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Příkladem	příklad	k1gInSc7
ústního	ústní	k2eAgInSc2d1
projevu	projev	k1gInSc2
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
řeč	řeč	k1gFnSc1
<g/>
,	,	kIx,
monolog	monolog	k1gInSc1
<g/>
,	,	kIx,
dialog	dialog	k1gInSc1
<g/>
,	,	kIx,
disputace	disputace	k1gFnSc1
<g/>
,	,	kIx,
diskuse	diskuse	k1gFnSc1
nebo	nebo	k8xC
polemika	polemika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Běžnou	běžný	k2eAgFnSc7d1
formou	forma	k1gFnSc7
ústní	ústní	k2eAgFnSc2d1
komunikace	komunikace	k1gFnSc2
je	být	k5eAaImIp3nS
vyprávění	vyprávění	k1gNnSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
jedna	jeden	k4xCgFnSc1
osoba	osoba	k1gFnSc1
vypráví	vyprávět	k5eAaImIp3nS
ostatním	ostatní	k2eAgMnPc3d1
příběh	příběh	k1gInSc1
o	o	k7c6
skutečné	skutečný	k2eAgFnSc6d1
události	událost	k1gFnSc6
nebo	nebo	k8xC
o	o	k7c6
něčem	něco	k3yInSc6
vymyšleném	vymyšlený	k2eAgNnSc6d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ústní	ústní	k2eAgFnPc4d1
tradice	tradice	k1gFnPc4
obsahuje	obsahovat	k5eAaImIp3nS
kulturní	kulturní	k2eAgInSc1d1
materiál	materiál	k1gInSc1
<g/>
,	,	kIx,
zvyky	zvyk	k1gInPc1
a	a	k8xC
způsob	způsob	k1gInSc1
myšlení	myšlení	k1gNnSc2
předávané	předávaný	k2eAgNnSc4d1
ústně	ústně	k6eAd1
na	na	k7c4
další	další	k2eAgFnPc4d1
generace	generace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyprávění	vyprávění	k1gNnPc1
a	a	k8xC
ústní	ústní	k2eAgFnPc1d1
tradice	tradice	k1gFnPc1
jsou	být	k5eAaImIp3nP
formy	forma	k1gFnPc4
ústního	ústní	k2eAgInSc2d1
projevu	projev	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
hrají	hrát	k5eAaImIp3nP
důležitou	důležitý	k2eAgFnSc4d1
roli	role	k1gFnSc4
ve	v	k7c6
folklóru	folklór	k1gInSc6
a	a	k8xC
mytologii	mytologie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalším	další	k2eAgInSc7d1
příkladem	příklad	k1gInSc7
ústní	ústní	k2eAgFnSc2d1
komunikace	komunikace	k1gFnSc2
je	být	k5eAaImIp3nS
orální	orální	k2eAgFnSc1d1
historie	historie	k1gFnSc1
-	-	kIx~
zaznamenávání	zaznamenávání	k1gNnSc1
<g/>
,	,	kIx,
uchovávání	uchovávání	k1gNnSc1
a	a	k8xC
interpretace	interpretace	k1gFnSc1
historických	historický	k2eAgFnPc2d1
informací	informace	k1gFnPc2
na	na	k7c6
základě	základ	k1gInSc6
osobních	osobní	k2eAgFnPc2d1
zkušeností	zkušenost	k1gFnPc2
a	a	k8xC
názorů	názor	k1gInPc2
mluvčího	mluvčí	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ochrana	ochrana	k1gFnSc1
orální	orální	k2eAgFnSc2d1
historie	historie	k1gFnSc2
je	být	k5eAaImIp3nS
obor	obor	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
se	se	k3xPyFc4
zabývá	zabývat	k5eAaImIp3nS
péčí	péče	k1gFnSc7
a	a	k8xC
údržbou	údržba	k1gFnSc7
materiálů	materiál	k1gInPc2
obsahujících	obsahující	k2eAgFnPc2d1
orální	orální	k2eAgFnSc4d1
historii	historie	k1gFnSc4
v	v	k7c6
jakémkoli	jakýkoli	k3yIgInSc6
formátu	formát	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Vyprávění	vyprávění	k1gNnSc1
</s>
<s>
Vyprávění	vyprávění	k1gNnSc1
často	často	k6eAd1
zahrnuje	zahrnovat	k5eAaImIp3nS
improvizaci	improvizace	k1gFnSc4
nebo	nebo	k8xC
přibarvování	přibarvování	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příběhy	příběh	k1gInPc1
nebo	nebo	k8xC
vyprávění	vyprávění	k1gNnSc4
byly	být	k5eAaImAgFnP
sdíleny	sdílen	k2eAgFnPc1d1
v	v	k7c6
každé	každý	k3xTgFnSc6
kultuře	kultura	k1gFnSc6
jako	jako	k8xC,k8xS
prostředek	prostředek	k1gInSc4
zábavy	zábava	k1gFnSc2
<g/>
,	,	kIx,
vzdělávání	vzdělávání	k1gNnSc2
<g/>
,	,	kIx,
zachování	zachování	k1gNnSc2
kulturní	kulturní	k2eAgFnSc2d1
tradice	tradice	k1gFnSc2
i	i	k9
pro	pro	k7c4
vštěpování	vštěpování	k1gNnSc4
morálních	morální	k2eAgFnPc2d1
hodnot	hodnota	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Nejstarší	starý	k2eAgFnSc7d3
formou	forma	k1gFnSc7
vyprávění	vyprávění	k1gNnSc2
byla	být	k5eAaImAgNnP
ústní	ústní	k2eAgNnPc1d1
sdělení	sdělení	k1gNnPc1
spolu	spolu	k6eAd1
se	s	k7c7
znázorněním	znázornění	k1gNnSc7
gesty	gesto	k1gNnPc7
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
bylo	být	k5eAaImAgNnS
běžné	běžný	k2eAgFnPc4d1
pro	pro	k7c4
mnoho	mnoho	k4c4
starověkých	starověký	k2eAgFnPc2d1
kultur	kultura	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Australští	australský	k2eAgMnPc1d1
domorodci	domorodec	k1gMnPc1
malovali	malovat	k5eAaImAgMnP
symboly	symbol	k1gInPc4
z	z	k7c2
příběhů	příběh	k1gInPc2
na	na	k7c6
stěnách	stěna	k1gFnPc6
jeskyní	jeskyně	k1gFnPc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
pomohli	pomoct	k5eAaPmAgMnP
vypravěči	vypravěč	k1gMnPc1
zapamatovat	zapamatovat	k5eAaPmF
si	se	k3xPyFc3
příběh	příběh	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příběh	příběh	k1gInSc1
byl	být	k5eAaImAgInS
poté	poté	k6eAd1
vyprávěn	vyprávět	k5eAaImNgInS
pomocí	pomocí	k7c2
kombinace	kombinace	k1gFnSc2
ústního	ústní	k2eAgNnSc2d1
vyprávění	vyprávění	k1gNnSc2
<g/>
,	,	kIx,
hudby	hudba	k1gFnSc2
<g/>
,	,	kIx,
jeskyních	jeskyně	k1gFnPc6
maleb	malba	k1gFnPc2
a	a	k8xC
tance	tanec	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Původně	původně	k6eAd1
byly	být	k5eAaImAgInP
příběhy	příběh	k1gInPc1
uloženy	uložen	k2eAgInPc1d1
do	do	k7c2
paměti	paměť	k1gFnSc2
a	a	k8xC
poté	poté	k6eAd1
předávány	předávat	k5eAaImNgFnP
z	z	k7c2
generace	generace	k1gFnSc2
na	na	k7c4
generaci	generace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
moderní	moderní	k2eAgFnSc6d1
společnosti	společnost	k1gFnSc6
však	však	k9
tuto	tento	k3xDgFnSc4
formu	forma	k1gFnSc4
předávání	předávání	k1gNnSc2
místní	místní	k2eAgFnSc2d1
<g/>
,	,	kIx,
rodinné	rodinný	k2eAgFnSc2d1
a	a	k8xC
kulturní	kulturní	k2eAgFnSc2d1
historie	historie	k1gFnSc2
nahradila	nahradit	k5eAaPmAgNnP
masmédia	masmédium	k1gNnPc1
<g/>
,	,	kIx,
hlavně	hlavně	k6eAd1
tisk	tisk	k1gInSc1
a	a	k8xC
televize	televize	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
některých	některý	k3yIgFnPc6
zemích	zem	k1gFnPc6
s	s	k7c7
nízkou	nízký	k2eAgFnSc7d1
mírou	míra	k1gFnSc7
gramotnosti	gramotnost	k1gFnSc2
však	však	k9
zůstávají	zůstávat	k5eAaImIp3nP
příběhy	příběh	k1gInPc1
a	a	k8xC
vyprávění	vyprávění	k1gNnSc1
převažujícím	převažující	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
učení	učení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Ústní	ústní	k2eAgFnSc1d1
tradice	tradice	k1gFnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Ústní	ústní	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
slovesnost	slovesnost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Ústní	ústní	k2eAgFnSc1d1
tradice	tradice	k1gFnSc1
(	(	kIx(
<g/>
někdy	někdy	k6eAd1
označovaná	označovaný	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
lidová	lidový	k2eAgFnSc1d1
tradice	tradice	k1gFnSc1
<g/>
,	,	kIx,
slovesnost	slovesnost	k1gFnSc1
nebo	nebo	k8xC
folklór	folklór	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
kulturní	kulturní	k2eAgInSc1d1
materiál	materiál	k1gInSc1
a	a	k8xC
tradice	tradice	k1gFnSc1
přenášené	přenášený	k2eAgNnSc4d1
ústně	ústně	k6eAd1
z	z	k7c2
jedné	jeden	k4xCgFnSc2
generace	generace	k1gFnSc2
na	na	k7c4
druhou	druhý	k4xOgFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zprávy	zpráva	k1gFnPc1
nebo	nebo	k8xC
svědectví	svědectví	k1gNnPc1
jsou	být	k5eAaImIp3nP
ústně	ústně	k6eAd1
přenášeny	přenášen	k2eAgFnPc1d1
řečí	řeč	k1gFnSc7
nebo	nebo	k8xC
zpěvem	zpěv	k1gInSc7
a	a	k8xC
mohou	moct	k5eAaImIp3nP
mít	mít	k5eAaImF
podobu	podoba	k1gFnSc4
například	například	k6eAd1
pohádek	pohádka	k1gFnPc2
<g/>
,	,	kIx,
legend	legenda	k1gFnPc2
<g/>
,	,	kIx,
pověstí	pověst	k1gFnPc2
<g/>
,	,	kIx,
hádanek	hádanka	k1gFnPc2
<g/>
,	,	kIx,
rčení	rčení	k1gNnPc2
<g/>
,	,	kIx,
balad	balada	k1gFnPc2
<g/>
,	,	kIx,
písní	píseň	k1gFnPc2
<g/>
,	,	kIx,
obřadů	obřad	k1gInPc2
nebo	nebo	k8xC
her	hra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nově	nově	k6eAd1
ji	on	k3xPp3gFnSc4
doplnily	doplnit	k5eAaPmAgFnP
například	například	k6eAd1
vtipy	vtip	k1gInPc4
<g/>
,	,	kIx,
virály	virála	k1gFnPc4
nebo	nebo	k8xC
videa	video	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tímto	tento	k3xDgInSc7
způsobem	způsob	k1gInSc7
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
ve	v	k7c6
společnosti	společnost	k1gFnSc6
předávala	předávat	k5eAaImAgFnS
jistá	jistý	k2eAgFnSc1d1
omezená	omezený	k2eAgFnSc1d1
znalost	znalost	k1gFnSc1
historie	historie	k1gFnSc2
<g/>
,	,	kIx,
slovesnosti	slovesnost	k1gFnSc2
<g/>
,	,	kIx,
právo	právo	k1gNnSc4
a	a	k8xC
další	další	k2eAgInPc4d1
poznatky	poznatek	k1gInPc4
napříč	napříč	k7c7
generacemi	generace	k1gFnPc7
bez	bez	k7c2
použití	použití	k1gNnSc2
písma	písmo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Sociologové	sociolog	k1gMnPc1
zdůrazňují	zdůrazňovat	k5eAaImIp3nP
požadavek	požadavek	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
materiál	materiál	k1gInSc1
byl	být	k5eAaImAgInS
udržován	udržovat	k5eAaImNgInS
skupinou	skupina	k1gFnSc7
lidí	člověk	k1gMnPc2
po	po	k7c4
několik	několik	k4yIc4
generací	generace	k1gFnPc2
<g/>
,	,	kIx,
a	a	k8xC
chtějí	chtít	k5eAaImIp3nP
tak	tak	k6eAd1
odlišit	odlišit	k5eAaPmF
ústní	ústní	k2eAgFnSc4d1
tradici	tradice	k1gFnSc4
od	od	k7c2
svědectví	svědectví	k1gNnSc2
nebo	nebo	k8xC
orální	orální	k2eAgFnSc2d1
historie	historie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Studium	studium	k1gNnSc1
ústní	ústní	k2eAgFnSc2d1
tradice	tradice	k1gFnSc2
je	být	k5eAaImIp3nS
odlišné	odlišný	k2eAgNnSc1d1
od	od	k7c2
akademické	akademický	k2eAgFnSc2d1
disciplíny	disciplína	k1gFnSc2
orální	orální	k2eAgFnSc2d1
historie	historie	k1gFnSc2
popsané	popsaný	k2eAgFnSc2d1
dále	daleko	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
také	také	k9
odlišné	odlišný	k2eAgNnSc1d1
od	od	k7c2
studia	studio	k1gNnSc2
orality	oralita	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
definována	definovat	k5eAaBmNgFnS
jako	jako	k9
myšlenka	myšlenka	k1gFnSc1
a	a	k8xC
její	její	k3xOp3gNnSc1
slovní	slovní	k2eAgNnSc1d1
vyjádření	vyjádření	k1gNnSc1
ve	v	k7c6
společnostech	společnost	k1gFnPc6
kde	kde	k6eAd1
technologie	technologie	k1gFnSc2
gramotnosti	gramotnost	k1gFnSc2
(	(	kIx(
<g/>
obzvláště	obzvláště	k6eAd1
psaní	psaní	k1gNnSc1
a	a	k8xC
tisk	tisk	k1gInSc1
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
neznámé	neznámá	k1gFnPc1
pro	pro	k7c4
většinu	většina	k1gFnSc4
populace	populace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Orální	orální	k2eAgFnSc1d1
historie	historie	k1gFnSc1
</s>
<s>
Orální	orální	k2eAgFnSc1d1
historie	historie	k1gFnSc1
je	být	k5eAaImIp3nS
zaznamenávání	zaznamenávání	k1gNnSc4
osobních	osobní	k2eAgFnPc2d1
vzpomínek	vzpomínka	k1gFnPc2
a	a	k8xC
osudů	osud	k1gInPc2
těch	ten	k3xDgMnPc2
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
zažili	zažít	k5eAaPmAgMnP
historické	historický	k2eAgFnPc4d1
éry	éra	k1gFnPc4
nebo	nebo	k8xC
události	událost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
metoda	metoda	k1gFnSc1
historické	historický	k2eAgFnSc2d1
dokumentace	dokumentace	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
rozhovory	rozhovor	k1gInPc4
s	s	k7c7
pamětníky	pamětník	k1gMnPc7
období	období	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
je	být	k5eAaImIp3nS
předmětem	předmět	k1gInSc7
zkoumání	zkoumání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Orální	orální	k2eAgFnSc1d1
historie	historie	k1gFnSc1
se	se	k3xPyFc4
často	často	k6eAd1
dotýká	dotýkat	k5eAaImIp3nS
témat	téma	k1gNnPc2
<g/>
,	,	kIx,
kterých	který	k3yRgFnPc2,k3yQgFnPc2,k3yIgFnPc2
se	se	k3xPyFc4
písemné	písemný	k2eAgInPc1d1
dokumenty	dokument	k1gInPc1
téměř	téměř	k6eAd1
nedotýkají	dotýkat	k5eNaImIp3nP
<g/>
,	,	kIx,
a	a	k8xC
tím	ten	k3xDgNnSc7
vyplňuje	vyplňovat	k5eAaImIp3nS
mezery	mezera	k1gFnPc4
v	v	k7c6
záznamech	záznam	k1gInPc6
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
tvoří	tvořit	k5eAaImIp3nP
rané	raný	k2eAgInPc4d1
historické	historický	k2eAgInPc4d1
dokumenty	dokument	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ochrana	ochrana	k1gFnSc1
orální	orální	k2eAgFnSc2d1
historie	historie	k1gFnSc2
je	být	k5eAaImIp3nS
obor	obor	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
se	se	k3xPyFc4
zabývá	zabývat	k5eAaImIp3nS
péčí	péče	k1gFnSc7
a	a	k8xC
údržbou	údržba	k1gFnSc7
materiálů	materiál	k1gInPc2
orální	orální	k2eAgFnSc2d1
historie	historie	k1gFnSc2
bez	bez	k7c2
ohledu	ohled	k1gInSc2
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
v	v	k7c6
jakém	jaký	k3yQgInSc6,k3yRgInSc6,k3yIgInSc6
formátu	formát	k1gInSc6
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
Tradiční	tradiční	k2eAgFnPc1d1
formy	forma	k1gFnPc1
ústního	ústní	k2eAgNnSc2d1
podání	podání	k1gNnSc2
</s>
<s>
Mezi	mezi	k7c4
zavedené	zavedený	k2eAgInPc4d1
systémy	systém	k1gInPc4
používající	používající	k2eAgInPc4d1
ústní	ústní	k2eAgInPc4d1
podání	podání	k1gNnSc6
patří	patřit	k5eAaImIp3nP
kromě	kromě	k7c2
dříve	dříve	k6eAd2
uvedených	uvedený	k2eAgFnPc2d1
také	také	k9
<g/>
:	:	kIx,
</s>
<s>
šeptanda	šeptanda	k1gFnSc1
<g/>
,	,	kIx,
fáma	fáma	k1gFnSc1
<g/>
,	,	kIx,
drbyPoslouchat	drbyPoslouchat	k5eAaPmF,k5eAaImF
šeptandu	šeptanda	k1gFnSc4
znamená	znamenat	k5eAaImIp3nS
něco	něco	k3yInSc1
tajného	tajný	k2eAgNnSc2d1
se	se	k3xPyFc4
dozvědět	dozvědět	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
pomluva	pomluva	k1gFnSc1
<g/>
,	,	kIx,
urážka	urážka	k1gFnSc1
na	na	k7c4
ctiPomluva	ctiPomluva	k1gFnSc1
nebo	nebo	k8xC
jiný	jiný	k2eAgInSc1d1
zásah	zásah	k1gInSc1
do	do	k7c2
vážnosti	vážnost	k1gFnSc2
člověka	člověk	k1gMnSc2
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
posouzen	posoudit	k5eAaPmNgInS
jako	jako	k9
přestupek	přestupek	k1gInSc4
urážky	urážka	k1gFnSc2
na	na	k7c6
cti	čest	k1gFnSc6
nebo	nebo	k8xC
i	i	k9
trestný	trestný	k2eAgInSc4d1
čin	čin	k1gInSc4
křivého	křivý	k2eAgNnSc2d1
obvinění	obvinění	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
salónek	salónek	k1gInSc4
<g/>
,	,	kIx,
kroužekLiterární	kroužekLiterární	k2eAgInPc4d1
salóny	salón	k1gInPc4
byla	být	k5eAaImAgFnS
shromáždění	shromáždění	k1gNnPc4
příznivců	příznivec	k1gMnPc2
určitého	určitý	k2eAgInSc2d1
žánru	žánr	k1gInSc2
<g/>
,	,	kIx,
autora	autor	k1gMnSc2
nebo	nebo	k8xC
skupiny	skupina	k1gFnSc2
autorů	autor	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobně	podobně	k6eAd1
se	se	k3xPyFc4
organizují	organizovat	k5eAaBmIp3nP
další	další	k2eAgFnPc1d1
zájmové	zájmový	k2eAgFnPc1d1
skupiny	skupina	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
přednáška	přednáška	k1gFnSc1
<g/>
,	,	kIx,
projev	projev	k1gInSc1
<g/>
,	,	kIx,
prohlášeníVeřejně	prohlášeníVeřejně	k6eAd1
pronesené	pronesený	k2eAgNnSc4d1
sdělení	sdělení	k1gNnSc4
<g/>
,	,	kIx,
prohlášení	prohlášení	k1gNnSc4
před	před	k7c7
svědky	svědek	k1gMnPc7
nebo	nebo	k8xC
uznávanou	uznávaný	k2eAgFnSc7d1
autoritou	autorita	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
je	být	k5eAaImIp3nS
ústní	ústní	k2eAgNnSc1d1
sdělení	sdělení	k1gNnSc1
závazné	závazný	k2eAgNnSc1d1
<g/>
,	,	kIx,
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
právním	právní	k2eAgInSc7d1
aktem	akt	k1gInSc7
<g/>
,	,	kIx,
například	například	k6eAd1
prohlášení	prohlášení	k1gNnSc1
učiněné	učiněný	k2eAgNnSc1d1
do	do	k7c2
protokolu	protokol	k1gInSc2
pro	pro	k7c4
odmítnutí	odmítnutí	k1gNnSc4
dědictví	dědictví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
kázání	kázání	k1gNnSc1
</s>
<s>
divadelní	divadelní	k2eAgNnSc4d1
<g/>
,	,	kIx,
hudební	hudební	k2eAgNnSc4d1
představení	představení	k1gNnSc4
</s>
<s>
a	a	k8xC
další	další	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Word	Word	kA
of	of	k?
mouth	mouth	k1gMnSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
ústním	ústní	k2eAgNnSc7d1
podáním	podání	k1gNnSc7
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Cajete	Caje	k1gNnSc2
<g/>
,	,	kIx,
Gregory	Gregor	k1gMnPc4
<g/>
,	,	kIx,
Donna	donna	k1gFnSc1
Eder	Eder	k1gMnSc1
and	and	k?
Regina	Regina	k1gFnSc1
Holyan	Holyan	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Životní	životní	k2eAgFnSc1d1
lekce	lekce	k1gFnSc1
jako	jako	k8xS,k8xC
vyprávění	vyprávění	k1gNnSc1
<g/>
:	:	kIx,
zkoumání	zkoumání	k1gNnSc4
morálky	morálka	k1gFnSc2
dětmi	dítě	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bloomington	Bloomington	k1gInSc1
<g/>
:	:	kIx,
Indiana	Indiana	k1gFnSc1
UP	UP	kA
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
0253222443	#num#	k4
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Komunikace	komunikace	k1gFnSc1
</s>
<s>
Interpersonální	interpersonální	k2eAgFnSc1d1
komunikace	komunikace	k1gFnSc1
</s>
<s>
Telekomunikace	telekomunikace	k1gFnSc1
</s>
