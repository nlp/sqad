<s>
Národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
Podyjí	Podyjí	k1gNnSc2	Podyjí
je	být	k5eAaImIp3nS	být
dosud	dosud	k6eAd1	dosud
jediným	jediný	k2eAgInSc7d1	jediný
moravským	moravský	k2eAgInSc7d1	moravský
národním	národní	k2eAgInSc7d1	národní
parkem	park	k1gInSc7	park
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
ke	k	k7c3	k
dni	den	k1gInSc3	den
20	[number]	k4	20
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1991	[number]	k4	1991
nařízením	nařízení	k1gNnSc7	nařízení
vlády	vláda	k1gFnSc2	vláda
ČR	ČR	kA	ČR
č.	č.	k?	č.
164	[number]	k4	164
<g/>
/	/	kIx~	/
<g/>
1991	[number]	k4	1991
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
v	v	k7c6	v
nejcennějších	cenný	k2eAgFnPc6d3	nejcennější
oblastech	oblast	k1gFnPc6	oblast
bývalé	bývalý	k2eAgFnSc2d1	bývalá
CHKO	CHKO	kA	CHKO
Podyjí	Podyjí	k1gNnSc2	Podyjí
(	(	kIx(	(
<g/>
tato	tento	k3xDgFnSc1	tento
CHKO	CHKO	kA	CHKO
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlášen	k2eAgFnSc1d1	vyhlášena
roku	rok	k1gInSc2	rok
1978	[number]	k4	1978
na	na	k7c6	na
rozloze	rozloha	k1gFnSc6	rozloha
103	[number]	k4	103
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
Podyjí	Podyjí	k1gNnSc2	Podyjí
je	být	k5eAaImIp3nS	být
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
rozlohou	rozloha	k1gFnSc7	rozloha
nejmenším	malý	k2eAgInSc7d3	nejmenší
národním	národní	k2eAgInSc7d1	národní
parkem	park	k1gInSc7	park
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
dolnorakouské	dolnorakouský	k2eAgFnSc6d1	Dolnorakouská
straně	strana	k1gFnSc6	strana
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
navazuje	navazovat	k5eAaImIp3nS	navazovat
Národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
Thayatal	Thayatal	k1gMnSc1	Thayatal
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
celková	celkový	k2eAgFnSc1d1	celková
rozloha	rozloha	k1gFnSc1	rozloha
je	být	k5eAaImIp3nS	být
63	[number]	k4	63
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
bod	bod	k1gInSc1	bod
má	mít	k5eAaImIp3nS	mít
nadmořskou	nadmořský	k2eAgFnSc4d1	nadmořská
výšku	výška	k1gFnSc4	výška
536	[number]	k4	536
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Nejnižším	nízký	k2eAgInSc7d3	nejnižší
bodem	bod	k1gInSc7	bod
je	být	k5eAaImIp3nS	být
207	[number]	k4	207
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Park	park	k1gInSc1	park
je	být	k5eAaImIp3nS	být
zalesněn	zalesnit	k5eAaPmNgInS	zalesnit
z	z	k7c2	z
84	[number]	k4	84
%	%	kIx~	%
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
běžných	běžný	k2eAgInPc2d1	běžný
lesních	lesní	k2eAgInPc2d1	lesní
porostů	porost	k1gInPc2	porost
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
mnohé	mnohý	k2eAgFnPc4d1	mnohá
vzácné	vzácný	k2eAgFnPc4d1	vzácná
dřeviny	dřevina	k1gFnPc4	dřevina
a	a	k8xC	a
hlavně	hlavně	k9	hlavně
endemický	endemický	k2eAgMnSc1d1	endemický
jeřáb	jeřáb	k1gMnSc1	jeřáb
muk	muka	k1gFnPc2	muka
hardeggský	hardeggský	k1gMnSc1	hardeggský
<g/>
.	.	kIx.	.
</s>
<s>
NP	NP	kA	NP
Podyjí	Podyjí	k1gNnSc1	Podyjí
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
podél	podél	k7c2	podél
řeky	řeka	k1gFnSc2	řeka
Dyje	Dyje	k1gFnSc2	Dyje
protékající	protékající	k2eAgFnSc2d1	protékající
40	[number]	k4	40
km	km	kA	km
dlouhým	dlouhý	k2eAgMnSc7d1	dlouhý
<g/>
,	,	kIx,	,
hlubokým	hluboký	k2eAgNnSc7d1	hluboké
meandrovitým	meandrovitý	k2eAgNnSc7d1	meandrovité
údolím	údolí	k1gNnSc7	údolí
mezi	mezi	k7c7	mezi
Vranovem	Vranov	k1gInSc7	Vranov
nad	nad	k7c7	nad
Dyjí	Dyje	k1gFnSc7	Dyje
a	a	k8xC	a
Znojmem	Znojmo	k1gNnSc7	Znojmo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihovýchodním	jihovýchodní	k2eAgInSc6d1	jihovýchodní
okraji	okraj	k1gInSc6	okraj
NP	NP	kA	NP
se	se	k3xPyFc4	se
na	na	k7c4	na
spojnici	spojnice	k1gFnSc4	spojnice
Znojmo	Znojmo	k1gNnSc1	Znojmo
-	-	kIx~	-
Hnanice	Hnanice	k1gFnSc1	Hnanice
nalézá	nalézat	k5eAaImIp3nS	nalézat
unikátní	unikátní	k2eAgNnSc1d1	unikátní
pásmo	pásmo	k1gNnSc1	pásmo
stepních	stepní	k2eAgNnPc2d1	stepní
vřesovištních	vřesovištní	k2eAgNnPc2d1	vřesovištní
lad	lado	k1gNnPc2	lado
<g/>
.	.	kIx.	.
</s>
<s>
Údolí	údolí	k1gNnSc1	údolí
řeky	řeka	k1gFnSc2	řeka
Dyje	Dyje	k1gFnSc2	Dyje
má	mít	k5eAaImIp3nS	mít
kaňonovitý	kaňonovitý	k2eAgInSc1d1	kaňonovitý
charakter	charakter	k1gInSc1	charakter
a	a	k8xC	a
Dyje	Dyje	k1gFnSc1	Dyje
zde	zde	k6eAd1	zde
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
mnoho	mnoho	k4c4	mnoho
meandrů	meandr	k1gInPc2	meandr
lemovaných	lemovaný	k2eAgFnPc2d1	lemovaná
skalními	skalní	k2eAgInPc7d1	skalní
útvary	útvar	k1gInPc7	útvar
a	a	k8xC	a
kamennými	kamenný	k2eAgNnPc7d1	kamenné
moři	moře	k1gNnPc7	moře
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
již	již	k6eAd1	již
vyhlášený	vyhlášený	k2eAgInSc4d1	vyhlášený
Národní	národní	k2eAgInSc4d1	národní
park	park	k1gInSc4	park
Podyjí	Podyjí	k1gNnSc1	Podyjí
navázalo	navázat	k5eAaPmAgNnS	navázat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
vyhlášení	vyhlášení	k1gNnSc4	vyhlášení
Národního	národní	k2eAgInSc2d1	národní
parku	park	k1gInSc2	park
Thayatal	Thayatal	k1gMnSc1	Thayatal
na	na	k7c6	na
rakouské	rakouský	k2eAgFnSc6d1	rakouská
straně	strana	k1gFnSc6	strana
hranice	hranice	k1gFnSc2	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
NP	NP	kA	NP
Thayatal	Thayatal	k1gMnSc1	Thayatal
navazuje	navazovat	k5eAaImIp3nS	navazovat
bezprostředně	bezprostředně	k6eAd1	bezprostředně
na	na	k7c4	na
území	území	k1gNnSc4	území
NP	NP	kA	NP
Podyjí	Podyjí	k1gNnSc4	Podyjí
na	na	k7c6	na
moravske	moravske	k1gFnSc6	moravske
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
Hodnoty	hodnota	k1gFnPc1	hodnota
parku	park	k1gInSc2	park
spočívají	spočívat	k5eAaImIp3nP	spočívat
v	v	k7c6	v
jedinečné	jedinečný	k2eAgFnSc6d1	jedinečná
geomorfologii	geomorfologie	k1gFnSc6	geomorfologie
a	a	k8xC	a
celkové	celkový	k2eAgFnSc3d1	celková
zachovalosti	zachovalost	k1gFnSc3	zachovalost
dyjského	dyjský	k2eAgInSc2d1	dyjský
kaňonu	kaňon	k1gInSc2	kaňon
<g/>
.	.	kIx.	.
</s>
<s>
Najdeme	najít	k5eAaPmIp1nP	najít
zde	zde	k6eAd1	zde
42	[number]	k4	42
km	km	kA	km
téměř	téměř	k6eAd1	téměř
neobydleného	obydlený	k2eNgNnSc2d1	neobydlené
říčního	říční	k2eAgNnSc2d1	říční
údolí	údolí	k1gNnSc2	údolí
–	–	k?	–
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nejzachovalejší	zachovalý	k2eAgNnSc1d3	nejzachovalejší
a	a	k8xC	a
nejméně	málo	k6eAd3	málo
dotčené	dotčený	k2eAgNnSc4d1	dotčené
říční	říční	k2eAgNnSc4d1	říční
údolí	údolí	k1gNnSc4	údolí
v	v	k7c6	v
Česke	Česke	k1gFnSc6	Česke
Republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
národního	národní	k2eAgInSc2d1	národní
parku	park	k1gInSc2	park
se	se	k3xPyFc4	se
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
nachází	nacházet	k5eAaImIp3nS	nacházet
vodní	vodní	k2eAgFnSc1d1	vodní
nádrž	nádrž	k1gFnSc1	nádrž
Znojmo	Znojmo	k1gNnSc4	Znojmo
<g/>
.	.	kIx.	.
</s>
<s>
Obdobná	obdobný	k2eAgNnPc1d1	obdobné
údolí	údolí	k1gNnPc1	údolí
byla	být	k5eAaImAgNnP	být
totiž	totiž	k9	totiž
zničena	zničen	k2eAgNnPc1d1	zničeno
např.	např.	kA	např.
výstavbou	výstavba	k1gFnSc7	výstavba
přehrad	přehrada	k1gFnPc2	přehrada
nebo	nebo	k8xC	nebo
chatovou	chatový	k2eAgFnSc7d1	chatová
zástavbou	zástavba	k1gFnSc7	zástavba
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
zachovalost	zachovalost	k1gFnSc4	zachovalost
Podyjí	Podyjí	k1gNnSc2	Podyjí
vděčí	vděčit	k5eAaImIp3nS	vděčit
především	především	k6eAd1	především
terénu	terén	k1gInSc2	terén
a	a	k8xC	a
poloze	poloha	k1gFnSc6	poloha
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
a	a	k8xC	a
také	také	k9	také
existenci	existence	k1gFnSc4	existence
hraničního	hraniční	k2eAgNnSc2d1	hraniční
pásma	pásmo	k1gNnSc2	pásmo
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Neexistují	existovat	k5eNaImIp3nP	existovat
zde	zde	k6eAd1	zde
žádné	žádný	k3yNgFnPc1	žádný
funkční	funkční	k2eAgFnPc1d1	funkční
stavby	stavba	k1gFnPc1	stavba
–	–	k?	–
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
rakouské	rakouský	k2eAgFnSc6d1	rakouská
straně	strana	k1gFnSc6	strana
malé	malý	k2eAgNnSc1d1	malé
městečko	městečko	k1gNnSc1	městečko
Hardegg	Hardegga	k1gFnPc2	Hardegga
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zároveň	zároveň	k6eAd1	zároveň
sídlí	sídlet	k5eAaImIp3nS	sídlet
správa	správa	k1gFnSc1	správa
rakouského	rakouský	k2eAgInSc2d1	rakouský
parku	park	k1gInSc2	park
<g/>
.	.	kIx.	.
</s>
<s>
Podloží	podloží	k1gNnSc1	podloží
parku	park	k1gInSc2	park
je	být	k5eAaImIp3nS	být
převážně	převážně	k6eAd1	převážně
tvořeno	tvořit	k5eAaImNgNnS	tvořit
Českým	český	k2eAgInSc7d1	český
masivem	masiv	k1gInSc7	masiv
–	–	k?	–
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
pevné	pevný	k2eAgNnSc1d1	pevné
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
staré	starý	k2eAgNnSc4d1	staré
podloží	podloží	k1gNnSc4	podloží
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc4	jeho
východní	východní	k2eAgInSc4d1	východní
okraj	okraj	k1gInSc4	okraj
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
do	do	k7c2	do
Dyjsko-svrateckého	dyjskovratecký	k2eAgInSc2d1	dyjsko-svratecký
úvalu	úval	k1gInSc2	úval
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějším	významný	k2eAgInSc7d3	nejvýznamnější
procesem	proces	k1gInSc7	proces
ve	v	k7c6	v
formování	formování	k1gNnSc6	formování
krajiny	krajina	k1gFnSc2	krajina
bylo	být	k5eAaImAgNnS	být
rychlé	rychlý	k2eAgNnSc1d1	rychlé
zařezávání	zařezávání	k1gNnSc1	zařezávání
Dyje	Dyje	k1gFnSc2	Dyje
do	do	k7c2	do
krajiny	krajina	k1gFnSc2	krajina
v	v	k7c6	v
třetihorách	třetihory	k1gFnPc6	třetihory
<g/>
,	,	kIx,	,
do	do	k7c2	do
hloubky	hloubka	k1gFnSc2	hloubka
100	[number]	k4	100
<g/>
–	–	k?	–
<g/>
200	[number]	k4	200
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
a	a	k8xC	a
mrazová	mrazový	k2eAgFnSc1d1	mrazová
modelace	modelace	k1gFnSc1	modelace
krajiny	krajina	k1gFnSc2	krajina
v	v	k7c6	v
blízké	blízký	k2eAgFnSc6d1	blízká
geologické	geologický	k2eAgFnSc6d1	geologická
minulosti	minulost	k1gFnSc6	minulost
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
skalní	skalní	k2eAgInPc1d1	skalní
amfiteátry	amfiteátr	k1gInPc1	amfiteátr
<g/>
,	,	kIx,	,
srázné	srázný	k2eAgFnPc1d1	Srázná
stěny	stěna	k1gFnPc1	stěna
<g/>
,	,	kIx,	,
meandry	meandr	k1gInPc1	meandr
i	i	k8xC	i
pseudokrasové	pseudokrasové	k2eAgFnSc1d1	pseudokrasové
jeskyně	jeskyně	k1gFnSc1	jeskyně
<g/>
.	.	kIx.	.
</s>
<s>
Typické	typický	k2eAgFnPc1d1	typická
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
Podyjí	Podyjí	k1gNnPc4	Podyjí
rozsáhlá	rozsáhlý	k2eAgNnPc4d1	rozsáhlé
kamenná	kamenný	k2eAgNnPc4d1	kamenné
a	a	k8xC	a
balvanitá	balvanitý	k2eAgNnPc4d1	balvanité
moře	moře	k1gNnPc4	moře
s	s	k7c7	s
typickou	typický	k2eAgFnSc7d1	typická
faunou	fauna	k1gFnSc7	fauna
a	a	k8xC	a
florou	flora	k1gFnSc7	flora
<g/>
.	.	kIx.	.
</s>
<s>
NP	NP	kA	NP
Podyjí	Podyjí	k1gNnSc1	Podyjí
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
druhově	druhově	k6eAd1	druhově
nejbohatší	bohatý	k2eAgNnPc4d3	nejbohatší
velkoplošná	velkoplošný	k2eAgNnPc4d1	velkoplošné
chráněná	chráněný	k2eAgNnPc4d1	chráněné
území	území	k1gNnPc4	území
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Krajinné	krajinný	k2eAgFnPc1d1	krajinná
podmínky	podmínka	k1gFnPc1	podmínka
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
růst	růst	k1gInSc4	růst
jak	jak	k8xS	jak
rostlin	rostlina	k1gFnPc2	rostlina
chladno-	chladno-	k?	chladno-
a	a	k8xC	a
stínomilných	stínomilný	k2eAgInPc2d1	stínomilný
v	v	k7c6	v
hlubokých	hluboký	k2eAgNnPc6d1	hluboké
údolích	údolí	k1gNnPc6	údolí
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
teplomilných	teplomilný	k2eAgFnPc2d1	teplomilná
na	na	k7c6	na
výslunných	výslunný	k2eAgFnPc6d1	výslunná
stráních	stráň	k1gFnPc6	stráň
<g/>
.	.	kIx.	.
</s>
<s>
Nepůvodní	původní	k2eNgInPc1d1	nepůvodní
rostlinné	rostlinný	k2eAgInPc1d1	rostlinný
druhy	druh	k1gInPc1	druh
můžeme	moct	k5eAaImIp1nP	moct
nalézt	nalézt	k5eAaPmF	nalézt
jen	jen	k9	jen
v	v	k7c6	v
okrajových	okrajový	k2eAgFnPc6d1	okrajová
částech	část	k1gFnPc6	část
parku	park	k1gInSc2	park
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
rozšiřování	rozšiřování	k1gNnSc4	rozšiřování
akátu	akát	k1gInSc2	akát
ve	v	k7c6	v
východních	východní	k2eAgFnPc6d1	východní
částech	část	k1gFnPc6	část
parku	park	k1gInSc2	park
je	být	k5eAaImIp3nS	být
závažnějším	závažný	k2eAgInSc7d2	závažnější
problémem	problém	k1gInSc7	problém
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
77	[number]	k4	77
druhů	druh	k1gInPc2	druh
chráněných	chráněný	k2eAgFnPc2d1	chráněná
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
kýchavice	kýchavice	k1gFnSc1	kýchavice
černá	černý	k2eAgFnSc1d1	černá
<g/>
,	,	kIx,	,
měsíčnice	měsíčnice	k1gFnSc1	měsíčnice
vytrvalá	vytrvalý	k2eAgFnSc1d1	vytrvalá
<g/>
,	,	kIx,	,
brambořík	brambořík	k1gInSc1	brambořík
nachový	nachový	k2eAgInSc1d1	nachový
<g/>
,	,	kIx,	,
divizna	divizna	k1gFnSc1	divizna
nádherná	nádherný	k2eAgFnSc1d1	nádherná
<g/>
,	,	kIx,	,
koniklec	koniklec	k1gInSc1	koniklec
velkokvětý	velkokvětý	k2eAgInSc1d1	velkokvětý
<g/>
,	,	kIx,	,
kosatec	kosatec	k1gInSc1	kosatec
dvoubarvý	dvoubarvý	k2eAgInSc1d1	dvoubarvý
<g/>
,	,	kIx,	,
volovec	volovec	k1gInSc1	volovec
vrbolistý	vrbolistý	k2eAgInSc1d1	vrbolistý
<g/>
,	,	kIx,	,
18	[number]	k4	18
druhů	druh	k1gInPc2	druh
orchidejí	orchidea	k1gFnPc2	orchidea
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
zajímavá	zajímavý	k2eAgNnPc4d1	zajímavé
společenství	společenství	k1gNnPc4	společenství
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
suťová	suťový	k2eAgFnSc1d1	suťová
pole	pole	k1gFnSc1	pole
<g/>
,	,	kIx,	,
porostlá	porostlý	k2eAgFnSc1d1	porostlá
mechy	mech	k1gInPc7	mech
a	a	k8xC	a
lišejníky	lišejník	k1gInPc1	lišejník
<g/>
,	,	kIx,	,
bohaté	bohatý	k2eAgFnPc1d1	bohatá
mokřadní	mokřadní	k2eAgFnPc1d1	mokřadní
i	i	k8xC	i
suché	suchý	k2eAgFnPc1d1	suchá
louky	louka	k1gFnPc1	louka
<g/>
,	,	kIx,	,
skály	skála	k1gFnPc1	skála
<g/>
,	,	kIx,	,
skalní	skalní	k2eAgFnPc1d1	skalní
stepi	step	k1gFnPc1	step
<g/>
,	,	kIx,	,
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
unikátní	unikátní	k2eAgFnSc2d1	unikátní
plochy	plocha	k1gFnSc2	plocha
suchých	suchý	k2eAgNnPc2d1	suché
a	a	k8xC	a
teplých	teplý	k2eAgNnPc2d1	teplé
vřesovišť	vřesoviště	k1gNnPc2	vřesoviště
a	a	k8xC	a
stepních	stepní	k2eAgNnPc2d1	stepní
lad	lado	k1gNnPc2	lado
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
dlouhodobou	dlouhodobý	k2eAgFnSc7d1	dlouhodobá
pastvou	pastva	k1gFnSc7	pastva
dobytka	dobytek	k1gInSc2	dobytek
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
východním	východní	k2eAgInSc6d1	východní
okraji	okraj	k1gInSc6	okraj
NP	NP	kA	NP
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nP	nacházet
zbytky	zbytek	k1gInPc7	zbytek
rozvolněných	rozvolněný	k2eAgFnPc2d1	rozvolněná
doubrav	doubrava	k1gFnPc2	doubrava
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
vykáceny	vykácet	k5eAaPmNgInP	vykácet
a	a	k8xC	a
přeměněny	přeměnit	k5eAaPmNgInP	přeměnit
na	na	k7c4	na
pastviny	pastvina	k1gFnPc4	pastvina
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgInPc6	jenž
se	se	k3xPyFc4	se
časem	časem	k6eAd1	časem
vlivem	vliv	k1gInSc7	vliv
pastvy	pastva	k1gFnSc2	pastva
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
unikátní	unikátní	k2eAgFnSc1d1	unikátní
náhradní	náhradní	k2eAgFnSc1d1	náhradní
společenstva	společenstvo	k1gNnPc1	společenstvo
s	s	k7c7	s
převládajícím	převládající	k2eAgInSc7d1	převládající
vřesem	vřes	k1gInSc7	vřes
obecným	obecný	k2eAgInSc7d1	obecný
<g/>
.	.	kIx.	.
</s>
<s>
Pásmo	pásmo	k1gNnSc1	pásmo
tzv.	tzv.	kA	tzv.
znojemských	znojemský	k2eAgNnPc2d1	Znojemské
vřesovišť	vřesoviště	k1gNnPc2	vřesoviště
se	se	k3xPyFc4	se
táhne	táhnout	k5eAaImIp3nS	táhnout
od	od	k7c2	od
Znojma	Znojmo	k1gNnSc2	Znojmo
až	až	k9	až
daleko	daleko	k6eAd1	daleko
do	do	k7c2	do
Rakouska	Rakousko	k1gNnSc2	Rakousko
za	za	k7c4	za
Retz	Retz	k1gInSc4	Retz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
národním	národní	k2eAgInSc6d1	národní
parku	park	k1gInSc6	park
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
řada	řada	k1gFnSc1	řada
vzácných	vzácný	k2eAgInPc2d1	vzácný
a	a	k8xC	a
chráněných	chráněný	k2eAgInPc2d1	chráněný
druhů	druh	k1gInPc2	druh
makromycetů	makromycet	k1gInPc2	makromycet
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
druhů	druh	k1gInPc2	druh
uvedených	uvedený	k2eAgInPc2d1	uvedený
v	v	k7c6	v
Červeném	červený	k2eAgInSc6d1	červený
seznamu	seznam	k1gInSc6	seznam
hub	houba	k1gFnPc2	houba
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
především	především	k9	především
baňka	baňka	k1gFnSc1	baňka
velkokališná	velkokališný	k2eAgFnSc1d1	velkokališná
(	(	kIx(	(
<g/>
Sarcosphaera	Sarcosphaera	k1gFnSc1	Sarcosphaera
coronaria	coronarium	k1gNnSc2	coronarium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bělochoroš	bělochoroš	k1gMnSc1	bělochoroš
lužní	lužní	k2eAgMnSc1d1	lužní
(	(	kIx(	(
<g/>
Postia	Postius	k1gMnSc4	Postius
subcaesia	subcaesius	k1gMnSc4	subcaesius
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bránovitka	bránovitka	k1gFnSc1	bránovitka
přezkatá	přezkatý	k2eAgFnSc1d1	přezkatý
(	(	kIx(	(
<g/>
Steccherinum	Steccherinum	k1gInSc1	Steccherinum
oreophilum	oreophilum	k1gInSc1	oreophilum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
černorosol	černorosol	k1gInSc1	černorosol
bělavý	bělavý	k2eAgInSc1d1	bělavý
(	(	kIx(	(
<g/>
Exidia	Exidium	k1gNnSc2	Exidium
thuretiana	thuretiana	k1gFnSc1	thuretiana
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
helmovka	helmovka	k1gFnSc1	helmovka
dojvonná	dojvonný	k2eAgFnSc1d1	dojvonný
(	(	kIx(	(
<g/>
Mycena	Mycen	k2eAgFnSc1d1	Mycena
diosma	diosma	k1gFnSc1	diosma
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hlíva	hlíva	k1gFnSc1	hlíva
čepičkatá	čepičkatý	k2eAgFnSc1d1	čepičkatý
(	(	kIx(	(
<g/>
Pleurotus	Pleurotus	k1gMnSc1	Pleurotus
calyptratus	calyptratus	k1gMnSc1	calyptratus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hlívička	hlívička	k1gFnSc1	hlívička
stopkatá	stopkatý	k2eAgFnSc1d1	stopkatá
(	(	kIx(	(
<g/>
Hohenbuehelia	Hohenbuehelia	k1gFnSc1	Hohenbuehelia
auriscalpium	auriscalpium	k1gNnSc4	auriscalpium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
holubinka	holubinka	k1gFnSc1	holubinka
Raoultova	Raoultův	k2eAgFnSc1d1	Raoultův
(	(	kIx(	(
<g/>
Russula	Russula	k1gFnSc1	Russula
raoultii	raoultie	k1gFnSc4	raoultie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
holubinka	holubinka	k1gFnSc1	holubinka
sluneční	sluneční	k2eAgFnSc1d1	sluneční
(	(	kIx(	(
<g/>
Russula	Russula	k1gFnSc1	Russula
solaris	solaris	k1gFnSc1	solaris
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
houžovec	houžovec	k1gMnSc1	houžovec
bobří	bobří	k2eAgMnSc1d1	bobří
(	(	kIx(	(
<g/>
Lentinellus	Lentinellus	k1gMnSc1	Lentinellus
castoreus	castoreus	k1gMnSc1	castoreus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
houžovec	houžovec	k1gMnSc1	houžovec
medvědí	medvědí	k2eAgMnSc1d1	medvědí
(	(	kIx(	(
<g/>
Lentinellus	Lentinellus	k1gMnSc1	Lentinellus
ursinus	ursinus	k1gMnSc1	ursinus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
choroš	choroš	k1gInSc1	choroš
oříš	oříš	k1gInSc1	oříš
(	(	kIx(	(
<g/>
Polyporus	Polyporus	k1gMnSc1	Polyporus
umbellatus	umbellatus	k1gMnSc1	umbellatus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
káčovka	káčovka	k1gFnSc1	káčovka
jeřábová	jeřábový	k2eAgFnSc1d1	jeřábová
(	(	kIx(	(
<g/>
Biscogniauxia	Biscogniauxia	k1gFnSc1	Biscogniauxia
repanda	repanda	k1gFnSc1	repanda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
káčovka	káčovka	k1gFnSc1	káčovka
ploská	ploský	k2eAgFnSc1d1	Ploská
(	(	kIx(	(
<g/>
Biscogniauxia	Biscogniauxia	k1gFnSc1	Biscogniauxia
simplicior	simplicior	k1gMnSc1	simplicior
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
korálovec	korálovec	k1gMnSc1	korálovec
ježatý	ježatý	k2eAgMnSc1d1	ježatý
(	(	kIx(	(
<g/>
Hericium	Hericium	k1gNnSc1	Hericium
erinaceus	erinaceus	k1gInSc1	erinaceus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kržatka	kržatka	k1gFnSc1	kržatka
ostnitá	ostnitý	k2eAgFnSc1d1	ostnitá
(	(	kIx(	(
<g/>
Flamulaster	Flamulaster	k1gMnSc1	Flamulaster
muricatus	muricatus	k1gMnSc1	muricatus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kuřátka	kuřátko	k1gNnPc1	kuřátko
křehká	křehký	k2eAgNnPc1d1	křehké
(	(	kIx(	(
<g/>
Ramaria	Ramarium	k1gNnPc1	Ramarium
gracilis	gracilis	k1gFnPc2	gracilis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kyjanka	kyjanka	k1gFnSc1	kyjanka
zakouřená	zakouřený	k2eAgFnSc1d1	zakouřená
(	(	kIx(	(
<g/>
Clavaria	Clavarium	k1gNnSc2	Clavarium
fumosa	fumosa	k1gFnSc1	fumosa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
límcovka	límcovka	k1gFnSc1	límcovka
natřená	natřený	k2eAgFnSc1d1	natřená
(	(	kIx(	(
<g/>
Stropharia	Stropharium	k1gNnSc2	Stropharium
inuncta	inunct	k1gInSc2	inunct
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
mozkovka	mozkovka	k1gFnSc1	mozkovka
rosolovitá	rosolovitý	k2eAgFnSc1d1	rosolovitá
(	(	kIx(	(
<g/>
Ascotremella	Ascotremella	k1gFnSc1	Ascotremella
faginea	faginea	k1gMnSc1	faginea
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pavučinec	pavučinec	k1gInSc1	pavučinec
plyšový	plyšový	k2eAgInSc1d1	plyšový
(	(	kIx(	(
<g/>
Cortinarius	Cortinarius	k1gInSc1	Cortinarius
orellanus	orellanus	k1gInSc1	orellanus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pečárka	pečárka	k1gFnSc1	pečárka
Benešova	Benešov	k1gInSc2	Benešov
(	(	kIx(	(
<g/>
Agaricus	Agaricus	k1gInSc1	Agaricus
benesii	benesie	k1gFnSc4	benesie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pórnatka	pórnatka	k1gFnSc1	pórnatka
klamná	klamný	k2eAgFnSc1d1	klamná
(	(	kIx(	(
<g/>
Ceriporiopsis	Ceriporiopsis	k1gFnSc1	Ceriporiopsis
aneirina	aneirina	k1gMnSc1	aneirina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
prášivka	prášivka	k1gFnSc1	prášivka
polní	polní	k2eAgFnSc1d1	polní
(	(	kIx(	(
<g/>
Bovista	Bovista	k1gMnSc1	Bovista
graveolens	graveolensa	k1gFnPc2	graveolensa
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
pýchavka	pýchavka	k1gFnSc1	pýchavka
loupavá	loupavý	k2eAgFnSc1d1	loupavá
(	(	kIx(	(
<g/>
Lycoperdon	Lycoperdon	k1gInSc1	Lycoperdon
marginatum	marginatum	k1gNnSc1	marginatum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
řasnatka	řasnatka	k1gFnSc1	řasnatka
modromléčná	modromléčný	k2eAgFnSc1d1	modromléčný
(	(	kIx(	(
<g/>
Peziza	Peziza	k1gFnSc1	Peziza
saniosa	saniosa	k1gFnSc1	saniosa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sametovka	sametovka	k1gFnSc1	sametovka
pochybná	pochybný	k2eAgFnSc1d1	pochybná
(	(	kIx(	(
<g/>
Conocybe	Conocyb	k1gInSc5	Conocyb
ambigua	ambiguum	k1gNnPc1	ambiguum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
strmělka	strmělka	k1gFnSc1	strmělka
Houghtonova	Houghtonův	k2eAgFnSc1d1	Houghtonův
(	(	kIx(	(
<g/>
Clitocybe	Clitocyb	k1gInSc5	Clitocyb
houghtonii	houghtonie	k1gFnSc4	houghtonie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
šťavnatka	šťavnatka	k1gFnSc1	šťavnatka
holubinková	holubinkový	k2eAgFnSc1d1	holubinkový
(	(	kIx(	(
<g/>
Hygrophorus	Hygrophorus	k1gMnSc1	Hygrophorus
russula	russul	k1gMnSc2	russul
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
šťavnatka	šťavnatka	k1gFnSc1	šťavnatka
zlatá	zlatá	k1gFnSc1	zlatá
(	(	kIx(	(
<g/>
Hygrophorus	Hygrophorus	k1gMnSc1	Hygrophorus
aureus	aureus	k1gMnSc1	aureus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
štítovka	štítovka	k1gFnSc1	štítovka
stinná	stinný	k2eAgFnSc1d1	stinná
(	(	kIx(	(
<g/>
Pluteus	pluteus	k1gMnSc1	pluteus
umbrosus	umbrosus	k1gMnSc1	umbrosus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
štítovka	štítovka	k1gFnSc1	štítovka
Thomsonova	Thomsonův	k2eAgFnSc1d1	Thomsonova
(	(	kIx(	(
<g/>
Pluteus	pluteus	k1gMnSc1	pluteus
thomsonii	thomsonie	k1gFnSc4	thomsonie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tlustěnka	tlustěnka	k1gFnSc1	tlustěnka
mléčná	mléčný	k2eAgFnSc1d1	mléčná
(	(	kIx(	(
<g/>
Scytinostroma	Scytinostroma	k1gFnSc1	Scytinostroma
galactinum	galactinum	k1gInSc1	galactinum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
třepenitka	třepenitka	k1gFnSc1	třepenitka
vlhkožijná	vlhkožijný	k2eAgFnSc1d1	vlhkožijný
(	(	kIx(	(
<g/>
Hypholoma	Hypholoma	k1gFnSc1	Hypholoma
subericeum	subericeum	k1gInSc1	subericeum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
voskovička	voskovička	k1gFnSc1	voskovička
černavá	černavý	k2eAgFnSc1d1	černavá
(	(	kIx(	(
<g/>
Holwaya	Holway	k2eAgFnSc1d1	Holway
mucida	mucida	k1gFnSc1	mucida
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
závojenka	závojenka	k1gFnSc1	závojenka
modrá	modrý	k2eAgFnSc1d1	modrá
(	(	kIx(	(
<g/>
Entoloma	Entoloma	k1gFnSc1	Entoloma
euchroum	euchroum	k1gInSc1	euchroum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
závojenka	závojenka	k1gFnSc1	závojenka
sítinová	sítinový	k2eAgFnSc1d1	sítinový
(	(	kIx(	(
<g/>
Entoloma	Entoloma	k1gFnSc1	Entoloma
juncinum	juncinum	k1gInSc1	juncinum
<g/>
)	)	kIx)	)
a	a	k8xC	a
žilnatka	žilnatka	k1gFnSc1	žilnatka
bledá	bledý	k2eAgFnSc1d1	bledá
(	(	kIx(	(
<g/>
Phlebia	Phlebia	k1gFnSc1	Phlebia
centrifuga	centrifuga	k1gFnSc1	centrifuga
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
bohatě	bohatě	k6eAd1	bohatě
zastoupených	zastoupený	k2eAgMnPc2d1	zastoupený
živočichů	živočich	k1gMnPc2	živočich
žije	žít	k5eAaImIp3nS	žít
na	na	k7c6	na
území	území	k1gNnSc6	území
parku	park	k1gInSc2	park
vydra	vydra	k1gFnSc1	vydra
říční	říční	k2eAgFnSc2d1	říční
<g/>
,	,	kIx,	,
ve	v	k7c6	v
stepních	stepní	k2eAgFnPc6d1	stepní
partiích	partie	k1gFnPc6	partie
i	i	k8xC	i
na	na	k7c6	na
vřesovištích	vřesoviště	k1gNnPc6	vřesoviště
sysel	sysel	k1gMnSc1	sysel
obecný	obecný	k2eAgInSc1d1	obecný
<g/>
.	.	kIx.	.
</s>
<s>
Vzácné	vzácný	k2eAgMnPc4d1	vzácný
ptáky	pták	k1gMnPc4	pták
zastupuje	zastupovat	k5eAaImIp3nS	zastupovat
dudek	dudek	k1gMnSc1	dudek
chocholatý	chocholatý	k2eAgMnSc1d1	chocholatý
<g/>
,	,	kIx,	,
čáp	čáp	k1gMnSc1	čáp
černý	černý	k1gMnSc1	černý
<g/>
,	,	kIx,	,
výr	výr	k1gMnSc1	výr
velký	velký	k2eAgMnSc1d1	velký
<g/>
,	,	kIx,	,
skorec	skorec	k1gMnSc1	skorec
vodní	vodní	k2eAgMnPc1d1	vodní
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
plazů	plaz	k1gMnPc2	plaz
se	se	k3xPyFc4	se
v	v	k7c6	v
NP	NP	kA	NP
Podyjí	Podyjí	k1gNnSc4	Podyjí
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
všechny	všechen	k3xTgInPc1	všechen
druhy	druh	k1gInPc1	druh
užovek	užovka	k1gFnPc2	užovka
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
hmyz	hmyz	k1gInSc4	hmyz
je	být	k5eAaImIp3nS	být
zastoupen	zastoupit	k5eAaPmNgMnS	zastoupit
mnoha	mnoho	k4c3	mnoho
jinde	jinde	k6eAd1	jinde
vzácnými	vzácný	k2eAgInPc7d1	vzácný
druhy	druh	k1gInPc7	druh
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
roháč	roháč	k1gInSc1	roháč
obecný	obecný	k2eAgInSc1d1	obecný
<g/>
,	,	kIx,	,
kudlanka	kudlanka	k1gFnSc1	kudlanka
nábožná	nábožný	k2eAgFnSc1d1	nábožná
<g/>
,	,	kIx,	,
tesařík	tesařík	k1gMnSc1	tesařík
obrovský	obrovský	k2eAgMnSc1d1	obrovský
<g/>
,	,	kIx,	,
nosorožík	nosorožík	k1gMnSc1	nosorožík
kapucínek	kapucínek	k1gMnSc1	kapucínek
<g/>
,	,	kIx,	,
otakárek	otakárek	k1gMnSc1	otakárek
ovocný	ovocný	k2eAgMnSc1d1	ovocný
<g/>
,	,	kIx,	,
jasoň	jasoň	k1gMnSc1	jasoň
dymnivkový	dymnivkový	k2eAgMnSc1d1	dymnivkový
a	a	k8xC	a
jiné	jiný	k2eAgInPc1d1	jiný
<g/>
.	.	kIx.	.
</s>
<s>
Negativní	negativní	k2eAgInSc1d1	negativní
vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
faunu	fauna	k1gFnSc4	fauna
v	v	k7c6	v
řece	řeka	k1gFnSc6	řeka
Dyji	Dyje	k1gFnSc6	Dyje
mají	mít	k5eAaImIp3nP	mít
dvě	dva	k4xCgFnPc4	dva
velké	velký	k2eAgFnPc4d1	velká
přehrady	přehrada	k1gFnPc4	přehrada
na	na	k7c6	na
okrajích	okraj	k1gInPc6	okraj
chráněné	chráněný	k2eAgFnSc2d1	chráněná
části	část	k1gFnSc2	část
jejího	její	k3xOp3gInSc2	její
toku	tok	k1gInSc2	tok
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
horním	horní	k2eAgInSc6d1	horní
okraji	okraj	k1gInSc6	okraj
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
Vranovská	vranovský	k2eAgFnSc1d1	Vranovská
přehrada	přehrada	k1gFnSc1	přehrada
a	a	k8xC	a
na	na	k7c6	na
dolním	dolní	k2eAgInSc6d1	dolní
Znojemská	znojemský	k2eAgNnPc5d1	Znojemské
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
stavby	stavba	k1gFnPc1	stavba
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
teplotní	teplotní	k2eAgInPc4d1	teplotní
poměry	poměr	k1gInPc4	poměr
v	v	k7c6	v
řece	řeka	k1gFnSc6	řeka
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
zároveň	zároveň	k6eAd1	zároveň
nepřekonatelnou	překonatelný	k2eNgFnSc7d1	nepřekonatelná
překážkou	překážka	k1gFnSc7	překážka
pro	pro	k7c4	pro
migrující	migrující	k2eAgMnPc4d1	migrující
živočichy	živočich	k1gMnPc4	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
chladnému	chladný	k2eAgInSc3d1	chladný
přítoku	přítok	k1gInSc3	přítok
vody	voda	k1gFnSc2	voda
z	z	k7c2	z
přehrady	přehrada	k1gFnSc2	přehrada
se	se	k3xPyFc4	se
změnil	změnit	k5eAaPmAgInS	změnit
tok	tok	k1gInSc1	tok
Dyje	Dyje	k1gFnSc2	Dyje
na	na	k7c4	na
pstruhové	pstruhový	k2eAgNnSc4d1	pstruhové
pásmo	pásmo	k1gNnSc4	pásmo
a	a	k8xC	a
pozdější	pozdní	k2eAgInSc4d2	pozdější
vznik	vznik	k1gInSc4	vznik
Znojemské	znojemský	k2eAgFnSc2d1	Znojemská
přehrady	přehrada	k1gFnSc2	přehrada
zcela	zcela	k6eAd1	zcela
vytěsnil	vytěsnit	k5eAaPmAgInS	vytěsnit
i	i	k9	i
typické	typický	k2eAgInPc4d1	typický
druhy	druh	k1gInPc4	druh
jako	jako	k8xS	jako
byla	být	k5eAaImAgFnS	být
ostroretka	ostroretka	k1gFnSc1	ostroretka
stěhovavá	stěhovavý	k2eAgFnSc1d1	stěhovavá
či	či	k8xC	či
ouklejka	ouklejka	k1gFnSc1	ouklejka
pruhovaná	pruhovaný	k2eAgFnSc1d1	pruhovaná
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
tak	tak	k6eAd1	tak
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
řece	řeka	k1gFnSc6	řeka
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
32	[number]	k4	32
druhů	druh	k1gInPc2	druh
ryb	ryba	k1gFnPc2	ryba
–	–	k?	–
především	především	k9	především
pstruh	pstruh	k1gMnSc1	pstruh
<g/>
,	,	kIx,	,
lipan	lipan	k1gMnSc1	lipan
a	a	k8xC	a
siven	siven	k1gMnSc1	siven
americký	americký	k2eAgMnSc1d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Mimořádně	mimořádně	k6eAd1	mimořádně
pestrý	pestrý	k2eAgInSc1d1	pestrý
je	být	k5eAaImIp3nS	být
i	i	k9	i
přehled	přehled	k1gInSc4	přehled
savců	savec	k1gMnPc2	savec
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
parku	park	k1gInSc2	park
je	být	k5eAaImIp3nS	být
jich	on	k3xPp3gInPc2	on
65	[number]	k4	65
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Druhů	druh	k1gInPc2	druh
ptáků	pták	k1gMnPc2	pták
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
152	[number]	k4	152
<g/>
,	,	kIx,	,
dvě	dva	k4xCgFnPc1	dva
třetiny	třetina	k1gFnPc1	třetina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
zde	zde	k6eAd1	zde
hnízdí	hnízdit	k5eAaImIp3nP	hnízdit
<g/>
;	;	kIx,	;
dále	daleko	k6eAd2	daleko
7	[number]	k4	7
druhů	druh	k1gInPc2	druh
plazů	plaz	k1gInPc2	plaz
a	a	k8xC	a
značné	značný	k2eAgNnSc1d1	značné
zastoupení	zastoupení	k1gNnSc1	zastoupení
obojživelníků	obojživelník	k1gMnPc2	obojživelník
<g/>
.	.	kIx.	.
</s>
<s>
Hmyz	hmyz	k1gInSc1	hmyz
je	být	k5eAaImIp3nS	být
tu	tu	k6eAd1	tu
zastoupen	zastoupit	k5eAaPmNgMnS	zastoupit
také	také	k9	také
velmi	velmi	k6eAd1	velmi
bohatě	bohatě	k6eAd1	bohatě
<g/>
,	,	kIx,	,
za	za	k7c4	za
zmínku	zmínka	k1gFnSc4	zmínka
stojí	stát	k5eAaImIp3nS	stát
alespoň	alespoň	k9	alespoň
12	[number]	k4	12
zvláště	zvláště	k6eAd1	zvláště
ohrožených	ohrožený	k2eAgInPc2d1	ohrožený
druhů	druh	k1gInPc2	druh
motýlů	motýl	k1gMnPc2	motýl
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
vysoká	vysoký	k2eAgFnSc1d1	vysoká
biodivezita	biodivezita	k1gFnSc1	biodivezita
je	být	k5eAaImIp3nS	být
způsobena	způsobit	k5eAaPmNgFnS	způsobit
stykem	styk	k1gInSc7	styk
dvou	dva	k4xCgFnPc2	dva
základních	základní	k2eAgFnPc2d1	základní
biogeografických	biogeografický	k2eAgFnPc2d1	biogeografická
oblastí	oblast	k1gFnPc2	oblast
střední	střední	k2eAgFnSc2d1	střední
Evropy	Evropa	k1gFnSc2	Evropa
–	–	k?	–
Hercynské	hercynský	k2eAgFnSc2d1	hercynská
a	a	k8xC	a
Panonské	panonský	k2eAgFnSc2d1	Panonská
krajiny	krajina	k1gFnSc2	krajina
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
s	s	k7c7	s
alpskými	alpský	k2eAgInPc7d1	alpský
i	i	k8xC	i
karpatskými	karpatský	k2eAgInPc7d1	karpatský
prvky	prvek	k1gInPc7	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
vhodným	vhodný	k2eAgFnPc3d1	vhodná
podmínkám	podmínka	k1gFnPc3	podmínka
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
projevuje	projevovat	k5eAaImIp3nS	projevovat
tzv.	tzv.	kA	tzv.
údolní	údolní	k2eAgInSc1d1	údolní
fenomén	fenomén	k1gInSc1	fenomén
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
důsledku	důsledek	k1gInSc6	důsledek
pronikají	pronikat	k5eAaImIp3nP	pronikat
západním	západní	k2eAgInSc7d1	západní
směrem	směr	k1gInSc7	směr
do	do	k7c2	do
údolí	údolí	k1gNnSc2	údolí
teplomilné	teplomilný	k2eAgInPc4d1	teplomilný
živočišné	živočišný	k2eAgInPc4d1	živočišný
a	a	k8xC	a
rostlinné	rostlinný	k2eAgInPc4d1	rostlinný
druhy	druh	k1gInPc4	druh
z	z	k7c2	z
jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
teplé	teplý	k2eAgFnSc2d1	teplá
panonské	panonský	k2eAgFnSc2d1	Panonská
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
ze	z	k7c2	z
západu	západ	k1gInSc2	západ
migrují	migrovat	k5eAaImIp3nP	migrovat
údolím	údolí	k1gNnSc7	údolí
druhy	druh	k1gInPc1	druh
podhorské	podhorský	k2eAgInPc1d1	podhorský
<g/>
,	,	kIx,	,
s	s	k7c7	s
kterými	který	k3yIgFnPc7	který
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
můžeme	moct	k5eAaImIp1nP	moct
setkat	setkat	k5eAaPmF	setkat
na	na	k7c6	na
chladnějších	chladný	k2eAgInPc6d2	chladnější
a	a	k8xC	a
stinných	stinný	k2eAgInPc6d1	stinný
severních	severní	k2eAgInPc6d1	severní
svazích	svah	k1gInPc6	svah
údolí	údolí	k1gNnSc2	údolí
<g/>
.	.	kIx.	.
</s>
<s>
Správa	správa	k1gFnSc1	správa
Národního	národní	k2eAgInSc2d1	národní
parku	park	k1gInSc2	park
Podyjí	Podyjí	k1gNnSc2	Podyjí
sídlí	sídlet	k5eAaImIp3nS	sídlet
na	na	k7c6	na
adrese	adresa	k1gFnSc6	adresa
<g/>
:	:	kIx,	:
Na	na	k7c6	na
vyhlídce	vyhlídka	k1gFnSc6	vyhlídka
5	[number]	k4	5
<g/>
,	,	kIx,	,
669	[number]	k4	669
03	[number]	k4	03
Znojmo	Znojmo	k1gNnSc4	Znojmo
<g/>
.	.	kIx.	.
</s>
<s>
Návštěvnické	návštěvnický	k2eAgNnSc1d1	návštěvnické
středisko	středisko	k1gNnSc1	středisko
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Čížov	Čížov	k1gInSc4	Čížov
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Národní	národní	k2eAgInSc4d1	národní
park	park	k1gInSc4	park
Podyjí	Podyjí	k1gNnSc2	Podyjí
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
Podyjí	Podyjí	k1gNnSc2	Podyjí
na	na	k7c4	na
OpenStreetMap	OpenStreetMap	k1gInSc4	OpenStreetMap
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
národního	národní	k2eAgInSc2d1	národní
parku	park	k1gInSc2	park
Podyjí	Podyjí	k1gNnSc2	Podyjí
Fotografie	fotografia	k1gFnSc2	fotografia
z	z	k7c2	z
NP	NP	kA	NP
Podyjí	Podyjí	k1gNnSc6	Podyjí
Turistická	turistický	k2eAgNnPc1d1	turistické
místa	místo	k1gNnPc1	místo
Znojemska	Znojemsko	k1gNnSc2	Znojemsko
a	a	k8xC	a
Podyjí	Podyjí	k1gNnSc2	Podyjí
Národní	národní	k2eAgInSc4d1	národní
park	park	k1gInSc4	park
Podyjí	Podyjí	k1gNnSc2	Podyjí
–	–	k?	–
video	video	k1gNnSc1	video
z	z	k7c2	z
cyklu	cyklus	k1gInSc2	cyklus
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
Návraty	návrat	k1gInPc1	návrat
k	k	k7c3	k
divočině	divočina	k1gFnSc3	divočina
</s>
