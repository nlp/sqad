<s>
Jukio	Jukio	k1gNnSc1	Jukio
Mišima	Mišimum	k1gNnSc2	Mišimum
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
三	三	k?	三
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Kimitake	Kimitak	k1gFnSc2	Kimitak
Hiraoka	Hiraoek	k1gInSc2	Hiraoek
<g/>
,	,	kIx,	,
japonsky	japonsky	k6eAd1	japonsky
平	平	k?	平
<g/>
,	,	kIx,	,
14	[number]	k4	14
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1925	[number]	k4	1925
-	-	kIx~	-
25	[number]	k4	25
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
japonský	japonský	k2eAgMnSc1d1	japonský
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
na	na	k7c6	na
Západě	západ	k1gInSc6	západ
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgMnS	proslavit
svou	svůj	k3xOyFgFnSc7	svůj
rituální	rituální	k2eAgFnSc7d1	rituální
sebevraždou	sebevražda	k1gFnSc7	sebevražda
seppuku	seppuk	k1gInSc2	seppuk
<g/>
.	.	kIx.	.
</s>
