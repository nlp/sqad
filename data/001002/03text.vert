<p>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Foglar	Foglar	k1gMnSc1	Foglar
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1907	[number]	k4	1907
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
23.	[number]	k4	23.
ledna	leden	k1gInSc2	leden
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
populární	populární	k2eAgMnSc1d1	populární
český	český	k2eAgMnSc1d1	český
spisovatel	spisovatel	k1gMnSc1	spisovatel
literatury	literatura	k1gFnSc2	literatura
pro	pro	k7c4	pro
mládež	mládež	k1gFnSc4	mládež
<g/>
,	,	kIx,	,
významná	významný	k2eAgFnSc1d1	významná
osobnost	osobnost	k1gFnSc1	osobnost
českého	český	k2eAgNnSc2d1	české
skautského	skautský	k2eAgNnSc2d1	skautské
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
,	,	kIx,	,
redaktor	redaktor	k1gMnSc1	redaktor
několika	několik	k4yIc2	několik
dětských	dětský	k2eAgInPc2d1	dětský
časopisů	časopis	k1gInPc2	časopis
a	a	k8xC	a
zážitkový	zážitkový	k2eAgMnSc1d1	zážitkový
vychovatel	vychovatel	k1gMnSc1	vychovatel
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
skautskou	skautský	k2eAgFnSc7d1	skautská
přezdívkou	přezdívka	k1gFnSc7	přezdívka
Jestřáb	jestřáb	k1gMnSc1	jestřáb
vedl	vést	k5eAaImAgInS	vést
po	po	k7c4	po
většinu	většina	k1gFnSc4	většina
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
chlapecký	chlapecký	k2eAgInSc1d1	chlapecký
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
klasický	klasický	k2eAgInSc1d1	klasický
skautský	skautský	k2eAgInSc1d1	skautský
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
jinak	jinak	k6eAd1	jinak
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
<g/>
)	)	kIx)	)
oddíl	oddíl	k1gInSc1	oddíl
<g/>
,	,	kIx,	,
Pražskou	pražský	k2eAgFnSc4d1	Pražská
Dvojku	dvojka	k1gFnSc4	dvojka
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
jeho	on	k3xPp3gNnSc2	on
díla	dílo	k1gNnSc2	dílo
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
příběhy	příběh	k1gInPc4	příběh
Rychlých	Rychlých	k2eAgInPc2d1	Rychlých
šípů	šíp	k1gInPc2	šíp
a	a	k8xC	a
Hochů	hoch	k1gMnPc2	hoch
od	od	k7c2	od
Bobří	bobří	k2eAgFnSc2d1	bobří
řeky	řeka	k1gFnSc2	řeka
<g/>
)	)	kIx)	)
přešla	přejít	k5eAaPmAgFnS	přejít
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
do	do	k7c2	do
obecného	obecný	k2eAgNnSc2d1	obecné
povědomí	povědomí	k1gNnSc2	povědomí
a	a	k8xC	a
některé	některý	k3yIgInPc1	některý
jeho	jeho	k3xOp3gInPc1	jeho
pojmy	pojem	k1gInPc1	pojem
a	a	k8xC	a
fráze	fráze	k1gFnPc1	fráze
zlidověly	zlidovět	k5eAaPmAgFnP	zlidovět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Životopis	životopis	k1gInSc4	životopis
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Dětství	dětství	k1gNnSc4	dětství
===	===	k?	===
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
na	na	k7c6	na
Novém	nový	k2eAgNnSc6d1	nové
Městě	město	k1gNnSc6	město
pražském	pražský	k2eAgNnSc6d1	Pražské
<g/>
,	,	kIx,	,
v	v	k7c6	v
Benátské	benátský	k2eAgFnSc6d1	Benátská
ulici	ulice	k1gFnSc6	ulice
číslo	číslo	k1gNnSc1	číslo
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Rodina	rodina	k1gFnSc1	rodina
se	se	k3xPyFc4	se
však	však	k9	však
krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
do	do	k7c2	do
Předlic	Předlice	k1gFnPc2	Předlice
(	(	kIx(	(
<g/>
Ústí	ústí	k1gNnSc1	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
)	)	kIx)	)
a	a	k8xC	a
následně	následně	k6eAd1	následně
do	do	k7c2	do
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byly	být	k5eAaImAgFnP	být
Foglarovi	Foglarův	k2eAgMnPc1d1	Foglarův
4	[number]	k4	4
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
17.	[number]	k4	17.
července	červenec	k1gInSc2	červenec
1911	[number]	k4	1911
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
srdeční	srdeční	k2eAgFnSc4d1	srdeční
nemoc	nemoc	k1gFnSc4	nemoc
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
Jindřich	Jindřich	k1gMnSc1	Jindřich
(	(	kIx(	(
<g/>
v	v	k7c6	v
Poděbradech	Poděbrady	k1gInPc6	Poděbrady
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
celá	celý	k2eAgFnSc1d1	celá
rodina	rodina	k1gFnSc1	rodina
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
kvůli	kvůli	k7c3	kvůli
léčbě	léčba	k1gFnSc3	léčba
<g/>
)	)	kIx)	)
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
39	[number]	k4	39
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
žil	žít	k5eAaImAgMnS	žít
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Foglar	Foglar	k1gMnSc1	Foglar
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
matkou	matka	k1gFnSc7	matka
a	a	k8xC	a
starším	starý	k2eAgMnSc7d2	starší
bratrem	bratr	k1gMnSc7	bratr
Zdeňkem	Zdeněk	k1gMnSc7	Zdeněk
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1914	[number]	k4	1914
se	se	k3xPyFc4	se
ovdovělá	ovdovělý	k2eAgFnSc1d1	ovdovělá
paní	paní	k1gFnSc1	paní
Foglarová	Foglarový	k2eAgFnSc1d1	Foglarový
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
dvěma	dva	k4xCgInPc7	dva
syny	syn	k1gMnPc7	syn
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
na	na	k7c4	na
Vinohrady	Vinohrady	k1gInPc4	Vinohrady
do	do	k7c2	do
Korunní	korunní	k2eAgFnSc2d1	korunní
třídy	třída	k1gFnSc2	třída
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
let	léto	k1gNnPc2	léto
1919/1920	[number]	k4	1919/1920
na	na	k7c4	na
výzvu	výzva	k1gFnSc4	výzva
svého	svůj	k3xOyFgMnSc2	svůj
kamaráda	kamarád	k1gMnSc2	kamarád
poprvé	poprvé	k6eAd1	poprvé
navštívil	navštívit	k5eAaPmAgMnS	navštívit
skautský	skautský	k2eAgInSc4d1	skautský
oddíl	oddíl	k1gInSc4	oddíl
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
mylně	mylně	k6eAd1	mylně
domnívali	domnívat	k5eAaImAgMnP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
mohou	moct	k5eAaImIp3nP	moct
zdarma	zdarma	k6eAd1	zdarma
cestovat	cestovat	k5eAaImF	cestovat
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Zjistili	zjistit	k5eAaPmAgMnP	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
cesta	cesta	k1gFnSc1	cesta
zdarma	zdarma	k6eAd1	zdarma
není	být	k5eNaImIp3nS	být
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
dostali	dostat	k5eAaPmAgMnP	dostat
pozvání	pozvání	k1gNnSc4	pozvání
do	do	k7c2	do
oddílu	oddíl	k1gInSc2	oddíl
<g/>
.	.	kIx.	.
</s>
<s>
Navštívili	navštívit	k5eAaPmAgMnP	navštívit
tedy	tedy	k9	tedy
ještě	ještě	k6eAd1	ještě
několik	několik	k4yIc4	několik
schůzek	schůzka	k1gFnPc2	schůzka
a	a	k8xC	a
výprav	výprava	k1gFnPc2	výprava
za	za	k7c4	za
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
dalších	další	k2eAgFnPc2d1	další
akcí	akce	k1gFnPc2	akce
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
v	v	k7c6	v
ulicích	ulice	k1gFnPc6	ulice
Prahy	Praha	k1gFnSc2	Praha
prodávali	prodávat	k5eAaImAgMnP	prodávat
losy	los	k1gMnPc7	los
Skautské	skautský	k2eAgFnSc2d1	skautská
loterie	loterie	k1gFnSc2	loterie
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nachladil	nachladit	k5eAaPmAgMnS	nachladit
<g/>
,	,	kIx,	,
a	a	k8xC	a
matka	matka	k1gFnSc1	matka
mu	on	k3xPp3gMnSc3	on
proto	proto	k6eAd1	proto
další	další	k2eAgNnSc4d1	další
skautování	skautování	k1gNnSc4	skautování
zakázala	zakázat	k5eAaPmAgFnS	zakázat
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
byl	být	k5eAaImAgInS	být
společně	společně	k6eAd1	společně
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
bratrem	bratr	k1gMnSc7	bratr
Zdeňkem	Zdeněk	k1gMnSc7	Zdeněk
přijat	přijmout	k5eAaPmNgMnS	přijmout
do	do	k7c2	do
48.	[number]	k4	48.
klubu	klub	k1gInSc2	klub
oldskautů	oldskaut	k1gInPc2	oldskaut
Jestřábi	jestřáb	k1gMnPc1	jestřáb
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterého	který	k3yQgMnSc2	který
později	pozdě	k6eAd2	pozdě
dostal	dostat	k5eAaPmAgMnS	dostat
i	i	k9	i
svoji	svůj	k3xOyFgFnSc4	svůj
přezdívku	přezdívka	k1gFnSc4	přezdívka
Jestřáb	jestřáb	k1gMnSc1	jestřáb
<g/>
.	.	kIx.	.
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
tehdy	tehdy	k6eAd1	tehdy
sice	sice	k8xC	sice
nesplňoval	splňovat	k5eNaImAgInS	splňovat
věk	věk	k1gInSc1	věk
pro	pro	k7c4	pro
oldskauty	oldskaut	k1gMnPc4	oldskaut
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
však	však	k9	však
přijat	přijmout	k5eAaPmNgInS	přijmout
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
z	z	k7c2	z
nouze	nouze	k1gFnSc2	nouze
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
měl	mít	k5eAaImAgInS	mít
klub	klub	k1gInSc1	klub
dostatečný	dostatečný	k2eAgInSc4d1	dostatečný
počet	počet	k1gInSc4	počet
členů	člen	k1gMnPc2	člen
pro	pro	k7c4	pro
schválení	schválení	k1gNnSc4	schválení
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
skautské	skautský	k2eAgFnSc2d1	skautská
organizace	organizace	k1gFnSc2	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Trampské	trampský	k2eAgNnSc1d1	trampské
toulání	toulání	k1gNnSc1	toulání
s	s	k7c7	s
oldskauty	oldskaut	k1gMnPc7	oldskaut
jej	on	k3xPp3gMnSc4	on
však	však	k9	však
nezaujalo	zaujmout	k5eNaPmAgNnS	zaujmout
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
téměř	téměř	k6eAd1	téměř
dospělý	dospělý	k1gMnSc1	dospělý
nakonec	nakonec	k6eAd1	nakonec
přijal	přijmout	k5eAaPmAgMnS	přijmout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
nabídku	nabídka	k1gFnSc4	nabídka
vstoupit	vstoupit	k5eAaPmF	vstoupit
do	do	k7c2	do
34.	[number]	k4	34.
pražského	pražský	k2eAgInSc2d1	pražský
oddílu	oddíl	k1gInSc2	oddíl
Ohnivci	ohnivec	k1gMnSc3	ohnivec
<g/>
.	.	kIx.	.
</s>
<s>
Ohnivci	ohnivec	k1gMnPc1	ohnivec
byli	být	k5eAaImAgMnP	být
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
sloučeni	sloučit	k5eAaPmNgMnP	sloučit
s	s	k7c7	s
legendárním	legendární	k2eAgInSc7d1	legendární
druhým	druhý	k4xOgInSc7	druhý
pražským	pražský	k2eAgInSc7d1	pražský
oddílem	oddíl	k1gInSc7	oddíl
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Dvojka	dvojka	k1gFnSc1	dvojka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Počátky	počátek	k1gInPc1	počátek
dospělosti	dospělost	k1gFnSc2	dospělost
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
skončil	skončit	k5eAaPmAgInS	skončit
obchodní	obchodní	k2eAgFnSc4d1	obchodní
školu	škola	k1gFnSc4	škola
a	a	k8xC	a
krátce	krátce	k6eAd1	krátce
pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
informační	informační	k2eAgFnSc6d1	informační
kanceláři	kancelář	k1gFnSc6	kancelář
firmy	firma	k1gFnSc2	firma
Wys	Wys	k1gFnSc2	Wys
Muller	Muller	k1gInSc1	Muller
&	&	k?	&
Co	co	k3yRnSc4	co
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
na	na	k7c4	na
třináct	třináct	k4xCc4	třináct
let	léto	k1gNnPc2	léto
přešel	přejít	k5eAaPmAgInS	přejít
k	k	k7c3	k
firmě	firma	k1gFnSc3	firma
Oskar	Oskar	k1gMnSc1	Oskar
Stein	Stein	k1gMnSc1	Stein
<g/>
,	,	kIx,	,
velkoobchod	velkoobchod	k1gInSc4	velkoobchod
papírem	papír	k1gInSc7	papír
<g/>
,	,	kIx,	,
u	u	k7c2	u
níž	jenž	k3xRgFnSc2	jenž
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
fakturant	fakturant	k1gMnSc1	fakturant
<g/>
.	.	kIx.	.
<g/>
O	o	k7c6	o
prázdninách	prázdniny	k1gFnPc6	prázdniny
roku	rok	k1gInSc2	rok
1925	[number]	k4	1925
vedl	vést	k5eAaImAgInS	vést
skautský	skautský	k2eAgInSc1d1	skautský
tábor	tábor	k1gInSc1	tábor
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgInSc6	jenž
poprvé	poprvé	k6eAd1	poprvé
zavítal	zavítat	k5eAaPmAgMnS	zavítat
do	do	k7c2	do
Sluneční	sluneční	k2eAgFnSc2d1	sluneční
zátoky	zátoka	k1gFnSc2	zátoka
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Sázavě	Sázava	k1gFnSc6	Sázava
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
z	z	k7c2	z
tábora	tábor	k1gInSc2	tábor
se	se	k3xPyFc4	se
z	z	k7c2	z
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
rady	rada	k1gMnSc2	rada
Junáka	junák	k1gMnSc2	junák
34.	[number]	k4	34.
pražský	pražský	k2eAgInSc1d1	pražský
oddíl	oddíl	k1gInSc1	oddíl
sloučil	sloučit	k5eAaPmAgInS	sloučit
se	s	k7c7	s
slavnou	slavný	k2eAgFnSc7d1	slavná
Dvojkou	dvojka	k1gFnSc7	dvojka
a	a	k8xC	a
Jestřáb	jestřáb	k1gMnSc1	jestřáb
přešel	přejít	k5eAaPmAgMnS	přejít
do	do	k7c2	do
jeho	on	k3xPp3gNnSc2	on
vedení	vedení	k1gNnSc2	vedení
jako	jako	k9	jako
rádce	rádce	k1gMnSc1	rádce
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
po	po	k7c6	po
dvou	dva	k4xCgNnPc6	dva
letech	léto	k1gNnPc6	léto
stal	stát	k5eAaPmAgMnS	stát
vůdcem	vůdce	k1gMnSc7	vůdce
oddílu	oddíl	k1gInSc2	oddíl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
funkci	funkce	k1gFnSc6	funkce
vydržel	vydržet	k5eAaPmAgInS	vydržet
plných	plný	k2eAgInPc2d1	plný
šedesát	šedesát	k4xCc4	šedesát
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
po	po	k7c6	po
60	[number]	k4	60
letech	let	k1gInPc6	let
nepřetržitého	přetržitý	k2eNgNnSc2d1	nepřetržité
vedení	vedení	k1gNnSc2	vedení
předal	předat	k5eAaPmAgMnS	předat
oddíl	oddíl	k1gInSc4	oddíl
svému	svůj	k3xOyFgMnSc3	svůj
nástupci	nástupce	k1gMnSc3	nástupce
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
zařadil	zařadit	k5eAaPmAgMnS	zařadit
mezi	mezi	k7c4	mezi
nejstarší	starý	k2eAgMnPc4d3	nejstarší
oddílové	oddílový	k2eAgMnPc4d1	oddílový
vedoucí	vedoucí	k1gMnPc4	vedoucí
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
První	první	k4xOgInSc4	první
publikační	publikační	k2eAgInSc4d1	publikační
úspěchy	úspěch	k1gInPc4	úspěch
===	===	k?	===
</s>
</p>
<p>
<s>
Dvě	dva	k4xCgFnPc4	dva
desítky	desítka	k1gFnPc4	desítka
let	léto	k1gNnPc2	léto
tradovanou	tradovaný	k2eAgFnSc4d1	tradovaná
legendu	legenda	k1gFnSc4	legenda
<g/>
,	,	kIx,	,
že	že	k8xS	že
jako	jako	k8xC	jako
třináctiletý	třináctiletý	k2eAgInSc4d1	třináctiletý
v	v	k7c6	v
novinách	novina	k1gFnPc6	novina
publikoval	publikovat	k5eAaBmAgMnS	publikovat
báseň	báseň	k1gFnSc4	báseň
Měsíční	měsíční	k2eAgFnSc2d1	měsíční
noci	noc	k1gFnSc2	noc
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
badatelé	badatel	k1gMnPc1	badatel
odmítají	odmítat	k5eAaImIp3nP	odmítat
<g/>
.	.	kIx.	.
</s>
<s>
Rukopis	rukopis	k1gInSc1	rukopis
ani	ani	k8xC	ani
dobový	dobový	k2eAgInSc1d1	dobový
výtisk	výtisk	k1gInSc1	výtisk
novin	novina	k1gFnPc2	novina
totiž	totiž	k9	totiž
doposud	doposud	k6eAd1	doposud
nikdo	nikdo	k3yNnSc1	nikdo
nedoložil	doložit	k5eNaPmAgMnS	doložit
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
patrně	patrně	k6eAd1	patrně
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
několik	několik	k4yIc4	několik
desetiletí	desetiletí	k1gNnPc2	desetiletí
starý	starý	k2eAgInSc4d1	starý
bibliografický	bibliografický	k2eAgInSc4d1	bibliografický
omyl	omyl	k1gInSc4	omyl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
bylo	být	k5eAaImAgNnS	být
osvětleno	osvětlit	k5eAaPmNgNnS	osvětlit
i	i	k9	i
pozadí	pozadí	k1gNnSc1	pozadí
popěvku	popěvek	k1gInSc2	popěvek
Biograf	biograf	k1gInSc1	biograf
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
rukopis	rukopis	k1gInSc1	rukopis
některé	některý	k3yIgMnPc4	některý
badatele	badatel	k1gMnPc4	badatel
mátl	mást	k5eAaImAgInS	mást
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
ukázala	ukázat	k5eAaPmAgFnS	ukázat
odborná	odborný	k2eAgFnSc1d1	odborná
rešerše	rešerše	k1gFnSc1	rešerše
agentury	agentura	k1gFnSc2	agentura
abcRedakce.cz	abcRedakce.cza	k1gFnPc2	abcRedakce.cza
<g/>
,	,	kIx,	,
říkačka	říkačka	k1gFnSc1	říkačka
Biograf	biograf	k1gInSc1	biograf
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
objevovala	objevovat	k5eAaImAgFnS	objevovat
například	například	k6eAd1	například
i	i	k9	i
v	v	k7c6	v
repertoáru	repertoár	k1gInSc6	repertoár
Osvobozeného	osvobozený	k2eAgNnSc2d1	osvobozené
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
první	první	k4xOgMnSc1	první
<g/>
,	,	kIx,	,
skutečně	skutečně	k6eAd1	skutečně
doloženou	doložený	k2eAgFnSc7d1	doložená
publikací	publikace	k1gFnSc7	publikace
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
povídka	povídka	k1gFnSc1	povídka
Vítězství	vítězství	k1gNnSc2	vítězství
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1923	[number]	k4	1923
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
svého	svůj	k3xOyFgNnSc2	svůj
působení	působení	k1gNnSc2	působení
mezi	mezi	k7c7	mezi
skauty	skaut	k1gMnPc7	skaut
pociťuje	pociťovat	k5eAaImIp3nS	pociťovat
zároveň	zároveň	k6eAd1	zároveň
touhu	touha	k1gFnSc4	touha
napsat	napsat	k5eAaBmF	napsat
nějakou	nějaký	k3yIgFnSc4	nějaký
jasnou	jasný	k2eAgFnSc4d1	jasná
a	a	k8xC	a
čtivou	čtivý	k2eAgFnSc4d1	čtivá
knihu	kniha	k1gFnSc4	kniha
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
by	by	kYmCp3nS	by
skauting	skauting	k1gInSc4	skauting
propagovala	propagovat	k5eAaImAgFnS	propagovat
<g/>
.	.	kIx.	.
</s>
<s>
Podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
ji	on	k3xPp3gFnSc4	on
však	však	k9	však
napsat	napsat	k5eAaBmF	napsat
až	až	k9	až
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
třicátých	třicátý	k4xOgNnPc2	třicátý
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
získával	získávat	k5eAaImAgInS	získávat
vůdcovské	vůdcovský	k2eAgFnPc4d1	vůdcovská
i	i	k8xC	i
textařské	textařský	k2eAgFnPc4d1	textařská
zkušenosti	zkušenost	k1gFnPc4	zkušenost
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1934	[number]	k4	1934
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
s	s	k7c7	s
knihou	kniha	k1gFnSc7	kniha
Modrý	modrý	k2eAgInSc1d1	modrý
život	život	k1gInSc1	život
Jiřího	Jiří	k1gMnSc2	Jiří
Dražana	Dražan	k1gMnSc2	Dražan
soutěže	soutěž	k1gFnSc2	soutěž
o	o	k7c4	o
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
knihu	kniha	k1gFnSc4	kniha
pro	pro	k7c4	pro
mládež	mládež	k1gFnSc4	mládež
<g/>
,	,	kIx,	,
vyhlášenou	vyhlášený	k2eAgFnSc4d1	vyhlášená
nakladatelstvím	nakladatelství	k1gNnSc7	nakladatelství
Melantrich	Melantrich	k1gMnSc1	Melantrich
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
dvěma	dva	k4xCgMnPc7	dva
autory	autor	k1gMnPc7	autor
získal	získat	k5eAaPmAgMnS	získat
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
vítězství	vítězství	k1gNnSc1	vítězství
bylo	být	k5eAaImAgNnS	být
i	i	k9	i
vydání	vydání	k1gNnSc1	vydání
knihy	kniha	k1gFnSc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
novým	nový	k2eAgInSc7d1	nový
názvem	název	k1gInSc7	název
Přístav	přístav	k1gInSc1	přístav
volá	volat	k5eAaImIp3nS	volat
tak	tak	k8xC	tak
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
jeho	jeho	k3xOp3gNnSc1	jeho
první	první	k4xOgNnSc1	první
dílo	dílo	k1gNnSc1	dílo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
stále	stále	k6eAd1	stále
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
úředník	úředník	k1gMnSc1	úředník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
přesvědčil	přesvědčit	k5eAaPmAgMnS	přesvědčit
nakladatelství	nakladatelství	k1gNnSc4	nakladatelství
Melantrich	Melantrich	k1gInSc1	Melantrich
o	o	k7c6	o
koncepci	koncepce	k1gFnSc6	koncepce
nového	nový	k2eAgInSc2d1	nový
časopisu	časopis	k1gInSc2	časopis
pro	pro	k7c4	pro
mládež	mládež	k1gFnSc4	mládež
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
nulté	nultý	k4xOgNnSc1	nultý
číslo	číslo	k1gNnSc1	číslo
časopisu	časopis	k1gInSc2	časopis
Malý	Malý	k1gMnSc1	Malý
hlasatel	hlasatel	k1gMnSc1	hlasatel
s	s	k7c7	s
odpovědným	odpovědný	k2eAgMnSc7d1	odpovědný
redaktorem	redaktor	k1gMnSc7	redaktor
dr.	dr.	kA	dr.
Břetislavem	Břetislav	k1gMnSc7	Břetislav
Mencákem	Mencák	k1gMnSc7	Mencák
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
byl	být	k5eAaImAgInS	být
brzy	brzy	k6eAd1	brzy
změněn	změnit	k5eAaPmNgInS	změnit
na	na	k7c6	na
Mladý	mladý	k1gMnSc1	mladý
hlasatel	hlasatel	k1gMnSc1	hlasatel
<g/>
.	.	kIx.	.
</s>
<s>
Foglar	Foglar	k1gInSc1	Foglar
s	s	k7c7	s
časopisem	časopis	k1gInSc7	časopis
začal	začít	k5eAaPmAgInS	začít
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
jako	jako	k9	jako
externí	externí	k2eAgMnSc1d1	externí
redaktor	redaktor	k1gMnSc1	redaktor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Období	období	k1gNnSc4	období
jeho	jeho	k3xOp3gNnSc2	jeho
redaktorského	redaktorský	k2eAgNnSc2d1	redaktorské
působení	působení	k1gNnSc2	působení
v	v	k7c6	v
Melantrichu	Melantrich	k1gInSc6	Melantrich
přišlo	přijít	k5eAaPmAgNnS	přijít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zde	zde	k6eAd1	zde
začal	začít	k5eAaPmAgInS	začít
působit	působit	k5eAaImF	působit
jako	jako	k9	jako
pracovník	pracovník	k1gMnSc1	pracovník
propagačního	propagační	k2eAgNnSc2d1	propagační
oddělení	oddělení	k1gNnSc2	oddělení
<g/>
.	.	kIx.	.
</s>
<s>
8.	[number]	k4	8.
května	květen	k1gInSc2	květen
1937	[number]	k4	1937
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
Mladém	mladý	k2eAgMnSc6d1	mladý
hlasateli	hlasatel	k1gMnSc6	hlasatel
na	na	k7c4	na
jeho	on	k3xPp3gInSc4	on
popud	popud	k1gInSc4	popud
výzva	výzva	k1gFnSc1	výzva
k	k	k7c3	k
zakládání	zakládání	k1gNnSc3	zakládání
čtenářských	čtenářský	k2eAgInPc2d1	čtenářský
klubů	klub	k1gInPc2	klub
Mladého	mladý	k1gMnSc2	mladý
hlasatele	hlasatel	k1gMnSc2	hlasatel
a	a	k8xC	a
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
první	první	k4xOgNnSc1	první
vydání	vydání	k1gNnSc1	vydání
knihy	kniha	k1gFnSc2	kniha
Hoši	hoch	k1gMnPc1	hoch
od	od	k7c2	od
Bobří	bobří	k2eAgFnSc2d1	bobří
řeky	řeka	k1gFnSc2	řeka
s	s	k7c7	s
ilustracemi	ilustrace	k1gFnPc7	ilustrace
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Buriana	Burian	k1gMnSc2	Burian
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
listopadu	listopad	k1gInSc2	listopad
1938	[number]	k4	1938
se	se	k3xPyFc4	se
ve	v	k7c6	v
vedení	vedení	k1gNnSc6	vedení
redakce	redakce	k1gFnSc2	redakce
objevil	objevit	k5eAaPmAgInS	objevit
společně	společně	k6eAd1	společně
s	s	k7c7	s
Karlem	Karel	k1gMnSc7	Karel
Burešem	Bureš	k1gMnSc7	Bureš
a	a	k8xC	a
od	od	k7c2	od
17.	[number]	k4	17.
prosince	prosinec	k1gInSc2	prosinec
začal	začít	k5eAaPmAgMnS	začít
vycházet	vycházet	k5eAaImF	vycházet
na	na	k7c4	na
pokračování	pokračování	k1gNnSc4	pokračování
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
legendární	legendární	k2eAgInSc4d1	legendární
seriál	seriál	k1gInSc4	seriál
–	–	k?	–
Rychlé	Rychlé	k2eAgInPc4d1	Rychlé
šípy	šíp	k1gInPc4	šíp
<g/>
,	,	kIx,	,
kreslený	kreslený	k2eAgInSc1d1	kreslený
dr.	dr.	kA	dr.
Janem	Jan	k1gMnSc7	Jan
Fischerem	Fischer	k1gMnSc7	Fischer
<g/>
.	.	kIx.	.
</s>
<s>
Rozborem	rozbor	k1gInSc7	rozbor
jeho	jeho	k3xOp3gInPc2	jeho
předválečných	předválečný	k2eAgInPc2d1	předválečný
inspiračních	inspirační	k2eAgInPc2d1	inspirační
zdrojů	zdroj	k1gInPc2	zdroj
se	se	k3xPyFc4	se
zřejmě	zřejmě	k6eAd1	zřejmě
nejvýrazněji	výrazně	k6eAd3	výrazně
zabývala	zabývat	k5eAaImAgFnS	zabývat
kniha	kniha	k1gFnSc1	kniha
Miloše	Miloš	k1gMnSc2	Miloš
Dvorského	Dvorský	k1gMnSc2	Dvorský
Mýtus	mýtus	k1gInSc1	mýtus
zvaný	zvaný	k2eAgInSc1d1	zvaný
Stínadla	stínadlo	k1gNnPc1	stínadlo
<g/>
,	,	kIx,	,
vydaná	vydaný	k2eAgFnSc1d1	vydaná
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
(	(	kIx(	(
<g/>
druhé	druhý	k4xOgNnSc1	druhý
vydání	vydání	k1gNnSc1	vydání
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgFnSc1	třetí
výrazně	výrazně	k6eAd1	výrazně
rozšířené	rozšířený	k2eAgFnPc1d1	rozšířená
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2018	[number]	k4	2018
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Činnost	činnost	k1gFnSc1	činnost
v	v	k7c6	v
období	období	k1gNnSc6	období
Protektorátu	protektorát	k1gInSc2	protektorát
===	===	k?	===
</s>
</p>
<p>
<s>
S	s	k7c7	s
počátkem	počátek	k1gInSc7	počátek
okupace	okupace	k1gFnSc2	okupace
a	a	k8xC	a
Protektorátu	protektorát	k1gInSc2	protektorát
jsou	být	k5eAaImIp3nP	být
paradoxně	paradoxně	k6eAd1	paradoxně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jen	jen	k9	jen
shodou	shoda	k1gFnSc7	shoda
náhod	náhoda	k1gFnPc2	náhoda
<g/>
,	,	kIx,	,
spojeny	spojen	k2eAgInPc1d1	spojen
jedny	jeden	k4xCgFnPc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
Foglarových	Foglarová	k1gFnPc2	Foglarová
redaktorských	redaktorský	k2eAgInPc2d1	redaktorský
úspěchů	úspěch	k1gInPc2	úspěch
<g/>
.	.	kIx.	.
–	–	k?	–
Vrcholné	vrcholný	k2eAgNnSc4d1	vrcholné
období	období	k1gNnSc4	období
časopisu	časopis	k1gInSc2	časopis
Mladý	mladý	k1gMnSc1	mladý
hlasatel	hlasatel	k1gMnSc1	hlasatel
s	s	k7c7	s
komiksem	komiks	k1gInSc7	komiks
Rychlé	Rychlé	k2eAgInPc4d1	Rychlé
šípy	šíp	k1gInPc4	šíp
a	a	k8xC	a
také	také	k9	také
největší	veliký	k2eAgInSc1d3	veliký
rozmach	rozmach	k1gInSc1	rozmach
jeho	jeho	k3xOp3gInPc2	jeho
čtenářských	čtenářský	k2eAgInPc2d1	čtenářský
klubů	klub	k1gInPc2	klub
(	(	kIx(	(
<g/>
do	do	k7c2	do
jara	jaro	k1gNnSc2	jaro
1941	[number]	k4	1941
jich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
registrováno	registrovat	k5eAaBmNgNnS	registrovat
téměř	téměř	k6eAd1	téměř
25.	[number]	k4	25.
<g/>
000	[number]	k4	000
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
roku	rok	k1gInSc2	rok
1941	[number]	k4	1941
byl	být	k5eAaImAgInS	být
však	však	k9	však
Mladý	mladý	k2eAgMnSc1d1	mladý
hlasatel	hlasatel	k1gMnSc1	hlasatel
úředně	úředně	k6eAd1	úředně
zastaven	zastavit	k5eAaPmNgMnS	zastavit
<g/>
.	.	kIx.	.
</s>
<s>
Foglar	Foglar	k1gMnSc1	Foglar
sice	sice	k8xC	sice
mohl	moct	k5eAaImAgMnS	moct
v	v	k7c6	v
Melantrichu	Melantrich	k1gInSc6	Melantrich
zůstat	zůstat	k5eAaPmF	zůstat
na	na	k7c4	na
pozici	pozice	k1gFnSc4	pozice
technického	technický	k2eAgMnSc2d1	technický
redaktora	redaktor	k1gMnSc2	redaktor
<g/>
,	,	kIx,	,
tato	tento	k3xDgFnSc1	tento
práce	práce	k1gFnSc1	práce
ho	on	k3xPp3gMnSc4	on
ale	ale	k9	ale
neuspokojovala	uspokojovat	k5eNaImAgFnS	uspokojovat
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
nepříjemné	příjemný	k2eNgNnSc1d1	nepříjemné
nechávat	nechávat	k5eAaImF	nechávat
zcela	zcela	k6eAd1	zcela
bez	bez	k7c2	bez
podpory	podpora	k1gFnSc2	podpora
početný	početný	k2eAgInSc4d1	početný
zástup	zástup	k1gInSc4	zástup
svých	svůj	k3xOyFgInPc2	svůj
čtenářských	čtenářský	k2eAgInPc2d1	čtenářský
klubů	klub	k1gInPc2	klub
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
zastavení	zastavení	k1gNnSc6	zastavení
Mladého	mladý	k1gMnSc2	mladý
hlasatele	hlasatel	k1gMnSc2	hlasatel
proto	proto	k8xC	proto
přijal	přijmout	k5eAaPmAgMnS	přijmout
nabídku	nabídka	k1gFnSc4	nabídka
na	na	k7c6	na
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
rozhlasem	rozhlas	k1gInSc7	rozhlas
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
od	od	k7c2	od
konce	konec	k1gInSc2	konec
léta	léto	k1gNnSc2	léto
začal	začít	k5eAaPmAgInS	začít
v	v	k7c6	v
časopisu	časopis	k1gInSc6	časopis
Rozhlas	rozhlas	k1gInSc1	rozhlas
mladých	mladý	k1gMnPc2	mladý
řídit	řídit	k5eAaImF	řídit
klubovou	klubový	k2eAgFnSc4d1	klubová
stránku	stránka	k1gFnSc4	stránka
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
zde	zde	k6eAd1	zde
založil	založit	k5eAaPmAgMnS	založit
<g/>
.	.	kIx.	.
</s>
<s>
Hlavně	hlavně	k9	hlavně
ale	ale	k9	ale
začal	začít	k5eAaPmAgMnS	začít
psát	psát	k5eAaImF	psát
scénáře	scénář	k1gInPc4	scénář
pro	pro	k7c4	pro
rozhlasový	rozhlasový	k2eAgInSc4d1	rozhlasový
pořad	pořad	k1gInSc4	pořad
Klub	klub	k1gInSc1	klub
zvídavých	zvídavý	k2eAgFnPc2d1	zvídavá
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
15	[number]	k4	15
obsáhlých	obsáhlý	k2eAgInPc2d1	obsáhlý
scénářů	scénář	k1gInPc2	scénář
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
dohromady	dohromady	k6eAd1	dohromady
tvoří	tvořit	k5eAaImIp3nP	tvořit
uzavřený	uzavřený	k2eAgInSc4d1	uzavřený
příběh	příběh	k1gInSc4	příběh
šestice	šestice	k1gFnSc2	šestice
pražských	pražský	k2eAgFnPc2d1	Pražská
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
<g/>
První	první	k4xOgFnPc1	první
větší	veliký	k2eAgFnPc1d2	veliký
komplikace	komplikace	k1gFnPc1	komplikace
souvisely	souviset	k5eAaImAgFnP	souviset
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
oddílovou	oddílový	k2eAgFnSc7d1	oddílová
činností	činnost	k1gFnSc7	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Organizace	organizace	k1gFnSc1	organizace
Junák	junák	k1gMnSc1	junák
<g/>
,	,	kIx,	,
největší	veliký	k2eAgFnSc1d3	veliký
prvorepubliková	prvorepublikový	k2eAgFnSc1d1	prvorepubliková
skautská	skautský	k2eAgFnSc1d1	skautská
organizace	organizace	k1gFnSc1	organizace
<g/>
,	,	kIx,	,
ke	k	k7c3	k
které	který	k3yRgFnSc3	který
náležela	náležet	k5eAaImAgFnS	náležet
i	i	k9	i
Foglarova	Foglarův	k2eAgFnSc1d1	Foglarova
Dvojka	dvojka	k1gFnSc1	dvojka
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ocitl	ocitnout	k5eAaPmAgMnS	ocitnout
ve	v	k7c6	v
vnitřní	vnitřní	k2eAgFnSc6d1	vnitřní
krizi	krize	k1gFnSc6	krize
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
Druhé	druhý	k4xOgFnSc2	druhý
republiky	republika	k1gFnSc2	republika
souhlasil	souhlasit	k5eAaImAgMnS	souhlasit
s	s	k7c7	s
řadou	řada	k1gFnSc7	řada
politických	politický	k2eAgInPc2d1	politický
ústupků	ústupek	k1gInPc2	ústupek
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
programu	program	k1gInSc6	program
(	(	kIx(	(
<g/>
sloučení	sloučení	k1gNnSc4	sloučení
s	s	k7c7	s
jinými	jiný	k2eAgFnPc7d1	jiná
a	a	k8xC	a
názorově	názorově	k6eAd1	názorově
odlišnými	odlišný	k2eAgFnPc7d1	odlišná
skautskými	skautský	k2eAgFnPc7d1	skautská
organizacemi	organizace	k1gFnPc7	organizace
<g/>
,	,	kIx,	,
změna	změna	k1gFnSc1	změna
krojů	kroj	k1gInPc2	kroj
<g/>
,	,	kIx,	,
politizace	politizace	k1gFnSc2	politizace
činnosti	činnost	k1gFnSc2	činnost
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
pokračovaly	pokračovat	k5eAaImAgFnP	pokračovat
i	i	k9	i
s	s	k7c7	s
příchodem	příchod	k1gInSc7	příchod
okupantů	okupant	k1gMnPc2	okupant
a	a	k8xC	a
vznikem	vznik	k1gInSc7	vznik
Protektorátu	protektorát	k1gInSc2	protektorát
<g/>
.	.	kIx.	.
</s>
<s>
Okupační	okupační	k2eAgInPc1d1	okupační
orgány	orgán	k1gInPc1	orgán
přesto	přesto	k8xC	přesto
všechno	všechen	k3xTgNnSc1	všechen
považovaly	považovat	k5eAaImAgFnP	považovat
Junáka	junák	k1gMnSc2	junák
za	za	k7c4	za
nebezpečnou	bezpečný	k2eNgFnSc4d1	nebezpečná
opoziční	opoziční	k2eAgFnSc4d1	opoziční
organizaci	organizace	k1gFnSc4	organizace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
roku	rok	k1gInSc2	rok
1940	[number]	k4	1940
byly	být	k5eAaImAgInP	být
některé	některý	k3yIgInPc1	některý
junácké	junácký	k2eAgInPc1d1	junácký
tábory	tábor	k1gInPc1	tábor
nečekaně	nečekaně	k6eAd1	nečekaně
rozehnány	rozehnán	k2eAgInPc1d1	rozehnán
bezpečnostními	bezpečnostní	k2eAgInPc7d1	bezpečnostní
nacistickými	nacistický	k2eAgInPc7d1	nacistický
oddíly	oddíl	k1gInPc7	oddíl
<g/>
.	.	kIx.	.
</s>
<s>
Táboru	tábor	k1gMnSc3	tábor
Foglarovy	Foglarův	k2eAgFnSc2d1	Foglarova
Dvojky	dvojka	k1gFnSc2	dvojka
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
zátah	zátah	k1gInSc1	zátah
sice	sice	k8xC	sice
vyhnul	vyhnout	k5eAaPmAgMnS	vyhnout
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
mohl	moct	k5eAaImAgMnS	moct
pokračovat	pokračovat	k5eAaImF	pokračovat
podle	podle	k7c2	podle
plánu	plán	k1gInSc2	plán
až	až	k6eAd1	až
téměř	téměř	k6eAd1	téměř
do	do	k7c2	do
konce	konec	k1gInSc2	konec
připraveného	připravený	k2eAgInSc2d1	připravený
programu	program	k1gInSc2	program
(	(	kIx(	(
<g/>
navzdory	navzdory	k7c3	navzdory
tradovaným	tradovaný	k2eAgFnPc3d1	tradovaná
legendám	legenda	k1gFnPc3	legenda
o	o	k7c4	o
ukončení	ukončení	k1gNnSc4	ukončení
po	po	k7c6	po
několika	několik	k4yIc6	několik
dnech	den	k1gInPc6	den
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
však	však	k9	však
byla	být	k5eAaImAgNnP	být
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1940	[number]	k4	1940
organizace	organizace	k1gFnSc1	organizace
Junák	junák	k1gMnSc1	junák
úředně	úředně	k6eAd1	úředně
zastavena	zastavit	k5eAaPmNgFnS	zastavit
<g/>
,	,	kIx,	,
musel	muset	k5eAaImAgInS	muset
i	i	k9	i
Foglar	Foglar	k1gInSc1	Foglar
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
oddílem	oddíl	k1gInSc7	oddíl
hledat	hledat	k5eAaImF	hledat
nouzové	nouzový	k2eAgNnSc4d1	nouzové
řešení	řešení	k1gNnSc4	řešení
pro	pro	k7c4	pro
pokračující	pokračující	k2eAgFnSc4d1	pokračující
oddílovou	oddílový	k2eAgFnSc4d1	oddílová
činnost	činnost	k1gFnSc4	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
zřejmě	zřejmě	k6eAd1	zřejmě
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
se	se	k3xPyFc4	se
s	s	k7c7	s
oddílem	oddíl	k1gInSc7	oddíl
přihlásil	přihlásit	k5eAaPmAgMnS	přihlásit
do	do	k7c2	do
organizace	organizace	k1gFnSc2	organizace
Dorostu	dorost	k1gInSc2	dorost
Klubu	klub	k1gInSc2	klub
českých	český	k2eAgMnPc2d1	český
turistů	turist	k1gMnPc2	turist
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
Káčata	káče	k1gNnPc1	káče
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
s	s	k7c7	s
oddílem	oddíl	k1gInSc7	oddíl
setrval	setrvat	k5eAaPmAgInS	setrvat
až	až	k9	až
téměř	téměř	k6eAd1	téměř
do	do	k7c2	do
konce	konec	k1gInSc2	konec
okupace	okupace	k1gFnSc2	okupace
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1942	[number]	k4	1942
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
Kuratorium	kuratorium	k1gNnSc1	kuratorium
pro	pro	k7c4	pro
výchovu	výchova	k1gFnSc4	výchova
mládeže	mládež	k1gFnSc2	mládež
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
a	a	k8xC	a
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
následně	následně	k6eAd1	následně
začalo	začít	k5eAaPmAgNnS	začít
ovlivňovat	ovlivňovat	k5eAaImF	ovlivňovat
veškerou	veškerý	k3xTgFnSc4	veškerý
mimoškolní	mimoškolní	k2eAgFnSc4d1	mimoškolní
organizovanou	organizovaný	k2eAgFnSc4d1	organizovaná
činnost	činnost	k1gFnSc4	činnost
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
mládeže	mládež	k1gFnSc2	mládež
<g/>
.	.	kIx.	.
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Foglar	Foglar	k1gMnSc1	Foglar
přešel	přejít	k5eAaPmAgMnS	přejít
během	během	k7c2	během
léta	léto	k1gNnSc2	léto
roku	rok	k1gInSc2	rok
1943	[number]	k4	1943
z	z	k7c2	z
rozhlasu	rozhlas	k1gInSc2	rozhlas
do	do	k7c2	do
externí	externí	k2eAgFnSc2d1	externí
spolupráce	spolupráce	k1gFnSc2	spolupráce
s	s	k7c7	s
kuratorním	kuratorní	k2eAgInSc7d1	kuratorní
časopisem	časopis	k1gInSc7	časopis
Správný	správný	k2eAgMnSc1d1	správný
kluk	kluk	k1gMnSc1	kluk
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jak	jak	k6eAd1	jak
doufal	doufat	k5eAaImAgMnS	doufat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podaří	podařit	k5eAaPmIp3nS	podařit
navázat	navázat	k5eAaPmF	navázat
na	na	k7c4	na
dřívější	dřívější	k2eAgFnSc4d1	dřívější
činnost	činnost	k1gFnSc4	činnost
Mladého	mladý	k1gMnSc2	mladý
hlasatele	hlasatel	k1gMnSc2	hlasatel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rozhlasu	rozhlas	k1gInSc2	rozhlas
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc1	ten
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
důvodů	důvod	k1gInPc2	důvod
nepodařilo	podařit	k5eNaPmAgNnS	podařit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
Kuratoriu	kuratorium	k1gNnSc6	kuratorium
zpočátku	zpočátku	k6eAd1	zpočátku
spatřoval	spatřovat	k5eAaImAgInS	spatřovat
slibný	slibný	k2eAgInSc1d1	slibný
potenciál	potenciál	k1gInSc1	potenciál
<g/>
.	.	kIx.	.
</s>
<s>
Přál	přát	k5eAaImAgMnS	přát
si	se	k3xPyFc3	se
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
v	v	k7c6	v
redakci	redakce	k1gFnSc6	redakce
Správného	správný	k2eAgMnSc2d1	správný
kluka	kluk	k1gMnSc2	kluk
mohl	moct	k5eAaImAgMnS	moct
získat	získat	k5eAaPmF	získat
stálý	stálý	k2eAgInSc4d1	stálý
úvazek	úvazek	k1gInSc4	úvazek
<g/>
.	.	kIx.	.
</s>
<s>
Vedl	vést	k5eAaImAgInS	vést
zde	zde	k6eAd1	zde
opět	opět	k6eAd1	opět
čtenářské	čtenářský	k2eAgInPc4d1	čtenářský
kluby	klub	k1gInPc4	klub
<g/>
,	,	kIx,	,
psal	psát	k5eAaImAgMnS	psát
povídky	povídka	k1gFnSc2	povídka
a	a	k8xC	a
také	také	k9	také
scénáře	scénář	k1gInPc1	scénář
pro	pro	k7c4	pro
Svorné	svorný	k2eAgInPc4d1	svorný
gambusíny	gambusín	k1gInPc4	gambusín
<g/>
.	.	kIx.	.
</s>
<s>
Skončilo	skončit	k5eAaPmAgNnS	skončit
to	ten	k3xDgNnSc1	ten
ale	ale	k9	ale
přesně	přesně	k6eAd1	přesně
opačně	opačně	k6eAd1	opačně
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
vlastně	vlastně	k9	vlastně
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
neprojevoval	projevovat	k5eNaImAgMnS	projevovat
pro	pro	k7c4	pro
oslavu	oslava	k1gFnSc4	oslava
protektorátního	protektorátní	k2eAgInSc2d1	protektorátní
vlastenectví	vlastenectví	k1gNnPc1	vlastenectví
žádné	žádný	k3yNgNnSc4	žádný
pochopení	pochopení	k1gNnSc4	pochopení
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
začátkem	začátkem	k7c2	začátkem
roku	rok	k1gInSc2	rok
1944	[number]	k4	1944
z	z	k7c2	z
redakce	redakce	k1gFnSc2	redakce
časopisu	časopis	k1gInSc2	časopis
vyhozen	vyhodit	k5eAaPmNgInS	vyhodit
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
Protektorátu	protektorát	k1gInSc2	protektorát
sice	sice	k8xC	sice
na	na	k7c4	na
jednu	jeden	k4xCgFnSc4	jeden
stranu	strana	k1gFnSc4	strana
dál	daleko	k6eAd2	daleko
vedl	vést	k5eAaImAgInS	vést
svůj	svůj	k3xOyFgInSc4	svůj
oddíl	oddíl	k1gInSc4	oddíl
a	a	k8xC	a
příležitostně	příležitostně	k6eAd1	příležitostně
publikoval	publikovat	k5eAaBmAgMnS	publikovat
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
však	však	k9	však
rostl	růst	k5eAaImAgInS	růst
jeho	jeho	k3xOp3gInSc1	jeho
strach	strach	k1gInSc1	strach
z	z	k7c2	z
možného	možný	k2eAgNnSc2d1	možné
zatčení	zatčení	k1gNnSc2	zatčení
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
z	z	k7c2	z
nuceného	nucený	k2eAgNnSc2d1	nucené
totálního	totální	k2eAgNnSc2d1	totální
nasazení	nasazení	k1gNnSc2	nasazení
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
ani	ani	k8xC	ani
na	na	k7c6	na
konci	konec	k1gInSc6	konec
války	válka	k1gFnSc2	válka
neodkláněl	odklánět	k5eNaImAgInS	odklánět
hlavu	hlava	k1gFnSc4	hlava
od	od	k7c2	od
možnosti	možnost	k1gFnSc2	možnost
být	být	k5eAaImF	být
prospěšný	prospěšný	k2eAgMnSc1d1	prospěšný
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
okolí	okolí	k1gNnSc6	okolí
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
například	například	k6eAd1	například
po	po	k7c6	po
únorovém	únorový	k2eAgInSc6d1	únorový
náletu	nálet	k1gInSc6	nálet
na	na	k7c4	na
Prahu	Praha	k1gFnSc4	Praha
ochotně	ochotně	k6eAd1	ochotně
vypomáhal	vypomáhat	k5eAaImAgMnS	vypomáhat
s	s	k7c7	s
několika	několik	k4yIc7	několik
chlapci	chlapec	k1gMnPc7	chlapec
z	z	k7c2	z
oddílu	oddíl	k1gInSc2	oddíl
při	při	k7c6	při
odklízení	odklízení	k1gNnSc6	odklízení
sutin	sutina	k1gFnPc2	sutina
na	na	k7c6	na
pražských	pražský	k2eAgInPc6d1	pražský
Vinohradech	Vinohrady	k1gInPc6	Vinohrady
<g/>
.	.	kIx.	.
<g/>
Činnosti	činnost	k1gFnPc1	činnost
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Foglara	Foglar	k1gMnSc2	Foglar
mezi	mezi	k7c7	mezi
roky	rok	k1gInPc7	rok
1939	[number]	k4	1939
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
se	se	k3xPyFc4	se
komplexně	komplexně	k6eAd1	komplexně
doposud	doposud	k6eAd1	doposud
jako	jako	k9	jako
jediná	jediný	k2eAgFnSc1d1	jediná
<g/>
,	,	kIx,	,
zato	zato	k6eAd1	zato
však	však	k9	však
velice	velice	k6eAd1	velice
erudovaně	erudovaně	k6eAd1	erudovaně
a	a	k8xC	a
čtivě	čtivě	k6eAd1	čtivě
<g/>
,	,	kIx,	,
věnovala	věnovat	k5eAaPmAgFnS	věnovat
obsáhlá	obsáhlý	k2eAgFnSc1d1	obsáhlá
publikace	publikace	k1gFnSc1	publikace
Klub	klub	k1gInSc1	klub
zvídavých	zvídavý	k2eAgFnPc2d1	zvídavá
dětí	dítě	k1gFnPc2	dítě
•	•	k?	•
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Foglar	Foglar	k1gMnSc1	Foglar
a	a	k8xC	a
Protektorát	protektorát	k1gInSc1	protektorát
<g/>
,	,	kIx,	,
vydaná	vydaný	k2eAgFnSc1d1	vydaná
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2018.	[number]	k4	2018.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Třetí	třetí	k4xOgFnSc1	třetí
republika	republika	k1gFnSc1	republika
===	===	k?	===
</s>
</p>
<p>
<s>
Těsně	těsně	k6eAd1	těsně
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
redigoval	redigovat	k5eAaImAgMnS	redigovat
krátkou	krátký	k2eAgFnSc4d1	krátká
dobu	doba	k1gFnSc4	doba
časopis	časopis	k1gInSc1	časopis
Junák	junák	k1gMnSc1	junák
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
názorové	názorový	k2eAgFnPc4d1	názorová
neshody	neshoda	k1gFnPc4	neshoda
však	však	k9	však
odtud	odtud	k6eAd1	odtud
brzy	brzy	k6eAd1	brzy
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
neskautského	skautský	k2eNgNnSc2d1	skautský
<g/>
,	,	kIx,	,
obecně	obecně	k6eAd1	obecně
mládežnického	mládežnický	k2eAgInSc2d1	mládežnický
časopisu	časopis	k1gInSc2	časopis
Vpřed	vpřed	k6eAd1	vpřed
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
"	"	kIx"	"
<g/>
foglarovské	foglarovský	k2eAgNnSc1d1	foglarovské
<g/>
"	"	kIx"	"
číslo	číslo	k1gNnSc1	číslo
časopisu	časopis	k1gInSc2	časopis
Vpřed	vpřed	k6eAd1	vpřed
(	(	kIx(	(
<g/>
č	č	k0	č
<g/>
.	.	kIx.	.
18	[number]	k4	18
<g/>
)	)	kIx)	)
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
9.	[number]	k4	9.
dubna	duben	k1gInSc2	duben
1946	[number]	k4	1946
s	s	k7c7	s
legendární	legendární	k2eAgFnSc7d1	legendární
kresbou	kresba	k1gFnSc7	kresba
Rychlých	Rychlých	k2eAgInPc2d1	Rychlých
šípů	šíp	k1gInPc2	šíp
na	na	k7c6	na
titulní	titulní	k2eAgFnSc6d1	titulní
straně	strana	k1gFnSc6	strana
(	(	kIx(	(
<g/>
autor	autor	k1gMnSc1	autor
Bohumil	Bohumil	k1gMnSc1	Bohumil
Konečný	Konečný	k1gMnSc1	Konečný
–	–	k?	–
Bimba	Bimba	k1gMnSc1	Bimba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
časopis	časopis	k1gInSc1	časopis
ukončil	ukončit	k5eAaPmAgInS	ukončit
svoji	svůj	k3xOyFgFnSc4	svůj
činnost	činnost	k1gFnSc4	činnost
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948.	[number]	k4	1948.
</s>
</p>
<p>
<s>
Foglar	Foglar	k1gInSc1	Foglar
při	při	k7c6	při
odchodu	odchod	k1gInSc6	odchod
dostal	dostat	k5eAaPmAgInS	dostat
od	od	k7c2	od
vydavatele	vydavatel	k1gMnSc4	vydavatel
nabídku	nabídka	k1gFnSc4	nabídka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
redaktorem	redaktor	k1gMnSc7	redaktor
časopisu	časopis	k1gInSc2	časopis
Mladý	mladý	k2eAgMnSc1d1	mladý
technik	technik	k1gMnSc1	technik
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
ale	ale	k9	ale
Foglar	Foglar	k1gMnSc1	Foglar
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
se	s	k7c7	s
slovy	slovo	k1gNnPc7	slovo
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Technika	technika	k1gFnSc1	technika
není	být	k5eNaImIp3nS	být
mým	můj	k3xOp1gInSc7	můj
oborem	obor	k1gInSc7	obor
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Časopis	časopis	k1gInSc1	časopis
v	v	k7c6	v
technických	technický	k2eAgInPc6d1	technický
oborech	obor	k1gInPc6	obor
sice	sice	k8xC	sice
navazoval	navazovat	k5eAaImAgInS	navazovat
na	na	k7c4	na
Mladého	mladý	k1gMnSc4	mladý
hlasatele	hlasatel	k1gMnSc4	hlasatel
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
však	však	k9	však
orientován	orientovat	k5eAaBmNgInS	orientovat
výhradně	výhradně	k6eAd1	výhradně
na	na	k7c4	na
techniku	technika	k1gFnSc4	technika
<g/>
.	.	kIx.	.
</s>
<s>
To	to	k9	to
Foglara	Foglara	k1gFnSc1	Foglara
odpuzovalo	odpuzovat	k5eAaImAgNnS	odpuzovat
a	a	k8xC	a
zřejmě	zřejmě	k6eAd1	zřejmě
ani	ani	k8xC	ani
nevěřil	věřit	k5eNaImAgMnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
zde	zde	k6eAd1	zde
mohl	moct	k5eAaImAgInS	moct
své	svůj	k3xOyFgFnPc4	svůj
romantické	romantický	k2eAgFnPc4d1	romantická
vize	vize	k1gFnPc4	vize
nějak	nějak	k6eAd1	nějak
smysluplně	smysluplně	k6eAd1	smysluplně
uplatnit	uplatnit	k5eAaPmF	uplatnit
<g/>
.	.	kIx.	.
</s>
<s>
Odmítnutím	odmítnutí	k1gNnSc7	odmítnutí
nabízené	nabízený	k2eAgFnSc2d1	nabízená
redaktorské	redaktorský	k2eAgFnPc4d1	redaktorská
práce	práce	k1gFnPc4	práce
mu	on	k3xPp3gMnSc3	on
přitom	přitom	k6eAd1	přitom
propadlo	propadnout	k5eAaPmAgNnS	propadnout
i	i	k9	i
členství	členství	k1gNnSc4	členství
v	v	k7c6	v
Syndikátu	syndikát	k1gInSc6	syndikát
novinářů	novinář	k1gMnPc2	novinář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Potíže	potíž	k1gFnPc1	potíž
v	v	k7c6	v
období	období	k1gNnSc6	období
komunistických	komunistický	k2eAgFnPc2d1	komunistická
vlád	vláda	k1gFnPc2	vláda
===	===	k?	===
</s>
</p>
<p>
<s>
Protože	protože	k8xS	protože
byl	být	k5eAaImAgInS	být
díky	díky	k7c3	díky
značným	značný	k2eAgInPc3d1	značný
honorářům	honorář	k1gInPc3	honorář
za	za	k7c2	za
knihy	kniha	k1gFnSc2	kniha
a	a	k8xC	a
redakční	redakční	k2eAgInPc4d1	redakční
nápady	nápad	k1gInPc4	nápad
velice	velice	k6eAd1	velice
dobře	dobře	k6eAd1	dobře
finančně	finančně	k6eAd1	finančně
zaopatřen	zaopatřen	k2eAgMnSc1d1	zaopatřen
<g/>
,	,	kIx,	,
chtěl	chtít	k5eAaImAgMnS	chtít
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
zůstat	zůstat	k5eAaPmF	zůstat
na	na	k7c6	na
volné	volný	k2eAgFnSc6d1	volná
noze	noha	k1gFnSc6	noha
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
nátlak	nátlak	k1gInSc4	nátlak
komunistické	komunistický	k2eAgFnSc2d1	komunistická
kontrarozvědky	kontrarozvědka	k1gFnSc2	kontrarozvědka
byl	být	k5eAaImAgInS	být
však	však	k9	však
nucen	nucen	k2eAgInSc1d1	nucen
na	na	k7c4	na
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
přijmout	přijmout	k5eAaPmF	přijmout
místo	místo	k7c2	místo
vychovatele	vychovatel	k1gMnSc2	vychovatel
učňovské	učňovský	k2eAgFnSc2d1	učňovská
a	a	k8xC	a
středoškolské	středoškolský	k2eAgFnSc2d1	středoškolská
mládeže	mládež	k1gFnSc2	mládež
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomuto	tento	k3xDgNnSc3	tento
přizpůsobení	přizpůsobení	k1gNnSc1	přizpůsobení
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
díky	díky	k7c3	díky
vytěžování	vytěžování	k1gNnSc3	vytěžování
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
StB	StB	k1gFnSc2	StB
<g/>
,	,	kIx,	,
mohl	moct	k5eAaImAgMnS	moct
dál	daleko	k6eAd2	daleko
vést	vést	k5eAaImF	vést
svůj	svůj	k3xOyFgInSc4	svůj
oddíl	oddíl	k1gInSc4	oddíl
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
pod	pod	k7c7	pod
hlavičkou	hlavička	k1gFnSc7	hlavička
turistického	turistický	k2eAgInSc2d1	turistický
oddílu	oddíl	k1gInSc2	oddíl
(	(	kIx(	(
<g/>
skauting	skauting	k1gInSc1	skauting
byl	být	k5eAaImAgInS	být
opět	opět	k6eAd1	opět
rozpuštěn	rozpustit	k5eAaPmNgInS	rozpustit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Prvnímu	první	k4xOgInSc3	první
výslechu	výslech	k1gInSc6	výslech
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
Státní	státní	k2eAgFnSc2d1	státní
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
byl	být	k5eAaImAgInS	být
Foglar	Foglar	k1gInSc1	Foglar
podroben	podrobit	k5eAaPmNgInS	podrobit
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
roku	rok	k1gInSc2	rok
1954	[number]	k4	1954
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
1955	[number]	k4	1955
s	s	k7c7	s
StB	StB	k1gMnSc7	StB
podepsal	podepsat	k5eAaPmAgInS	podepsat
tzv.	tzv.	kA	tzv.
vázací	vázací	k2eAgInSc4d1	vázací
akt	akt	k1gInSc4	akt
spolupráce	spolupráce	k1gFnSc2	spolupráce
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
veden	vést	k5eAaImNgInS	vést
pod	pod	k7c7	pod
krycím	krycí	k2eAgNnSc7d1	krycí
jménem	jméno	k1gNnSc7	jméno
"	"	kIx"	"
<g/>
Šípek	Šípek	k1gMnSc1	Šípek
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
tohoto	tento	k3xDgNnSc2	tento
uvázání	uvázání	k1gNnSc2	uvázání
byl	být	k5eAaImAgMnS	být
přinucen	přinutit	k5eAaPmNgMnS	přinutit
podat	podat	k5eAaPmF	podat
několik	několik	k4yIc1	několik
písemných	písemný	k2eAgNnPc2d1	písemné
hlášení	hlášení	k1gNnPc2	hlášení
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgNnPc7	jenž
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
nikomu	nikdo	k3yNnSc3	nikdo
neublížit	ublížit	k5eNaPmF	ublížit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
několika	několik	k4yIc6	několik
měsících	měsíc	k1gInPc6	měsíc
<g/>
,	,	kIx,	,
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1956	[number]	k4	1956
napsal	napsat	k5eAaBmAgMnS	napsat
"	"	kIx"	"
<g/>
řídícímu	řídící	k2eAgMnSc3d1	řídící
důstojníkovi	důstojník	k1gMnSc3	důstojník
<g/>
"	"	kIx"	"
dopis	dopis	k1gInSc4	dopis
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
zoufale	zoufale	k6eAd1	zoufale
psal	psát	k5eAaImAgMnS	psát
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
spolupráci	spolupráce	k1gFnSc4	spolupráce
časově	časově	k6eAd1	časově
ani	ani	k8xC	ani
nervově	nervově	k6eAd1	nervově
nestačí	stačit	k5eNaBmIp3nS	stačit
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
StB	StB	k1gFnSc1	StB
překvapivě	překvapivě	k6eAd1	překvapivě
jeho	on	k3xPp3gInSc4	on
spis	spis	k1gInSc4	spis
uzavřela	uzavřít	k5eAaPmAgFnS	uzavřít
a	a	k8xC	a
odložila	odložit	k5eAaPmAgFnS	odložit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vyjma	vyjma	k7c2	vyjma
jedné	jeden	k4xCgFnSc2	jeden
krátké	krátký	k2eAgFnSc2d1	krátká
epizody	epizoda	k1gFnSc2	epizoda
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1957	[number]	k4	1957
nebyl	být	k5eNaImAgMnS	být
v	v	k7c6	v
letech	let	k1gInPc6	let
1948	[number]	k4	1948
<g/>
–	–	k?	–
<g/>
1962	[number]	k4	1962
publikačně	publikačně	k6eAd1	publikačně
nijak	nijak	k6eAd1	nijak
činný	činný	k2eAgMnSc1d1	činný
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
začátkem	začátkem	k7c2	začátkem
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
začal	začít	k5eAaPmAgInS	začít
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
s	s	k7c7	s
pionýrským	pionýrský	k2eAgInSc7d1	pionýrský
časopisem	časopis	k1gInSc7	časopis
ABC	ABC	kA	ABC
mladých	mladý	k2eAgMnPc2d1	mladý
techniků	technik	k1gMnPc2	technik
a	a	k8xC	a
přírodovědců	přírodovědec	k1gMnPc2	přírodovědec
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc4	jehož
redakci	redakce	k1gFnSc4	redakce
vedl	vést	k5eAaImAgMnS	vést
Vlastislav	Vlastislav	k1gMnSc1	Vlastislav
Toman	Toman	k1gMnSc1	Toman
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1963	[number]	k4	1963
<g/>
–	–	k?	–
<g/>
1966	[number]	k4	1966
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
časopis	časopis	k1gInSc4	časopis
vytvářel	vytvářet	k5eAaImAgMnS	vytvářet
komiks	komiks	k1gInSc4	komiks
Kulišáci	Kulišák	k1gMnPc1	Kulišák
kreslený	kreslený	k2eAgInSc1d1	kreslený
Jiřím	Jiří	k1gMnSc7	Jiří
Kráslem	Krásl	k1gMnSc7	Krásl
a	a	k8xC	a
redigoval	redigovat	k5eAaImAgInS	redigovat
rubriku	rubrika	k1gFnSc4	rubrika
Kompas	kompas	k1gInSc1	kompas
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalšímu	další	k2eAgInSc3d1	další
vývoji	vývoj	k1gInSc3	vývoj
došlo	dojít	k5eAaPmAgNnS	dojít
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	s	k7c7	s
spisovatelovým	spisovatelův	k2eAgInSc7d1	spisovatelův
souhlasem	souhlas	k1gInSc7	souhlas
rozvířil	rozvířit	k5eAaPmAgMnS	rozvířit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
diskusi	diskuse	k1gFnSc4	diskuse
nad	nad	k7c7	nad
foglarovkami	foglarovka	k1gFnPc7	foglarovka
Petr	Petr	k1gMnSc1	Petr
Sadecký	Sadecký	k2eAgInSc1d1	Sadecký
<g/>
.	.	kIx.	.
</s>
<s>
Znovu	znovu	k6eAd1	znovu
začal	začít	k5eAaPmAgInS	začít
publikovat	publikovat	k5eAaBmF	publikovat
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
spisovatelem	spisovatel	k1gMnSc7	spisovatel
na	na	k7c6	na
volné	volný	k2eAgFnSc6d1	volná
noze	noha	k1gFnSc6	noha
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
delší	dlouhý	k2eAgFnSc6d2	delší
odmlce	odmlka	k1gFnSc6	odmlka
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
objevil	objevit	k5eAaPmAgInS	objevit
román	román	k1gInSc1	román
Tajemná	tajemný	k2eAgFnSc1d1	tajemná
Řásnovka	Řásnovka	k1gFnSc1	Řásnovka
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
začal	začít	k5eAaPmAgInS	začít
psát	psát	k5eAaImF	psát
už	už	k6eAd1	už
za	za	k7c2	za
Protektorátu	protektorát	k1gInSc2	protektorát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Ostravském	ostravský	k2eAgInSc6d1	ostravský
kulturním	kulturní	k2eAgInSc6d1	kulturní
zpravodaji	zpravodaj	k1gInSc6	zpravodaj
začaly	začít	k5eAaPmAgInP	začít
znovu	znovu	k6eAd1	znovu
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
vycházet	vycházet	k5eAaImF	vycházet
Rychlé	Rychlé	k2eAgInPc4d1	Rychlé
šípy	šíp	k1gInPc4	šíp
i	i	k9	i
s	s	k7c7	s
novými	nový	k2eAgInPc7d1	nový
příběhy	příběh	k1gInPc7	příběh
<g/>
,	,	kIx,	,
kreslenými	kreslený	k2eAgInPc7d1	kreslený
Marko	Marko	k1gMnSc1	Marko
Čermákem	Čermák	k1gMnSc7	Čermák
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
mohl	moct	k5eAaImAgInS	moct
opětovně	opětovně	k6eAd1	opětovně
vést	vést	k5eAaImF	vést
svůj	svůj	k3xOyFgInSc4	svůj
oddíl	oddíl	k1gInSc4	oddíl
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
začal	začít	k5eAaPmAgInS	začít
psát	psát	k5eAaImF	psát
i	i	k9	i
další	další	k2eAgFnPc1d1	další
knihy	kniha	k1gFnPc1	kniha
včetně	včetně	k7c2	včetně
dalšího	další	k2eAgMnSc2d1	další
(	(	kIx(	(
<g/>
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
již	již	k6eAd1	již
třetího	třetí	k4xOgNnSc2	třetí
<g/>
)	)	kIx)	)
dílu	dílo	k1gNnSc3	dílo
o	o	k7c6	o
Rychlých	Rychlých	k2eAgInPc6d1	Rychlých
šípech	šíp	k1gInPc6	šíp
ve	v	k7c6	v
Stínadlech	stínadlo	k1gNnPc6	stínadlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Okupace	okupace	k1gFnSc1	okupace
Československa	Československo	k1gNnSc2	Československo
vojsky	vojsky	k6eAd1	vojsky
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
a	a	k8xC	a
nastolení	nastolení	k1gNnSc2	nastolení
socialistické	socialistický	k2eAgFnSc2d1	socialistická
normalizace	normalizace	k1gFnSc2	normalizace
však	však	k9	však
znamenaly	znamenat	k5eAaImAgFnP	znamenat
nové	nový	k2eAgNnSc4d1	nové
umlčení	umlčení	k1gNnSc4	umlčení
jeho	jeho	k3xOp3gFnSc2	jeho
veřejné	veřejný	k2eAgFnSc2d1	veřejná
činnosti	činnost	k1gFnSc2	činnost
i	i	k8xC	i
skautské	skautský	k2eAgFnSc2d1	skautská
činnosti	činnost	k1gFnSc2	činnost
Pražské	pražský	k2eAgFnSc2d1	Pražská
dvojky	dvojka	k1gFnSc2	dvojka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sedmdesátých	sedmdesátý	k4xOgNnPc6	sedmdesátý
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
proto	proto	k6eAd1	proto
věnoval	věnovat	k5eAaImAgMnS	věnovat
převážně	převážně	k6eAd1	převážně
práci	práce	k1gFnSc4	práce
s	s	k7c7	s
oddílem	oddíl	k1gInSc7	oddíl
(	(	kIx(	(
<g/>
opětovně	opětovně	k6eAd1	opětovně
přejmenovaným	přejmenovaný	k2eAgInSc7d1	přejmenovaný
jako	jako	k8xS	jako
turistický	turistický	k2eAgInSc4d1	turistický
oddíl	oddíl	k1gInSc4	oddíl
"	"	kIx"	"
<g/>
Hoší	Hoší	k1gFnSc4	Hoší
od	od	k7c2	od
bobří	bobří	k2eAgFnSc2d1	bobří
řeky	řeka	k1gFnSc2	řeka
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
publikoval	publikovat	k5eAaBmAgMnS	publikovat
příležitostně	příležitostně	k6eAd1	příležitostně
a	a	k8xC	a
pouze	pouze	k6eAd1	pouze
časopisecky	časopisecky	k6eAd1	časopisecky
<g/>
.	.	kIx.	.
</s>
<s>
Situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
pozvolna	pozvolna	k6eAd1	pozvolna
měnit	měnit	k5eAaImF	měnit
až	až	k6eAd1	až
koncem	koncem	k7c2	koncem
osmdesátých	osmdesátý	k4xOgNnPc2	osmdesátý
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Výraznou	výrazný	k2eAgFnSc7d1	výrazná
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gMnPc3	jeho
spolupráce	spolupráce	k1gFnSc1	spolupráce
s	s	k7c7	s
výtvarníkem	výtvarník	k1gMnSc7	výtvarník
Kájou	Kájou	k?	Kájou
Saudkem	Saudek	k1gMnSc7	Saudek
na	na	k7c6	na
komiksových	komiksový	k2eAgInPc6d1	komiksový
seriálech	seriál	k1gInPc6	seriál
pro	pro	k7c4	pro
Českou	český	k2eAgFnSc4d1	Česká
speleologickou	speleologický	k2eAgFnSc4d1	speleologická
společnost	společnost	k1gFnSc4	společnost
pod	pod	k7c7	pod
názvy	název	k1gInPc7	název
Modrá	modrat	k5eAaImIp3nS	modrat
rokle	rokle	k1gFnSc1	rokle
<g/>
,	,	kIx,	,
Ztracený	ztracený	k2eAgMnSc1d1	ztracený
kamarád	kamarád	k1gMnSc1	kamarád
a	a	k8xC	a
Jeskyně	jeskyně	k1gFnSc1	jeskyně
Saturn	Saturn	k1gInSc1	Saturn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Po	po	k7c6	po
revoluci	revoluce	k1gFnSc6	revoluce
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
měl	mít	k5eAaImAgMnS	mít
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Foglar	Foglar	k1gMnSc1	Foglar
plně	plně	k6eAd1	plně
otevřenu	otevřen	k2eAgFnSc4d1	otevřena
cestu	cesta	k1gFnSc4	cesta
ke	k	k7c3	k
čtenářské	čtenářský	k2eAgFnSc3d1	čtenářská
veřejnosti	veřejnost	k1gFnSc3	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
nakladatelstvím	nakladatelství	k1gNnSc7	nakladatelství
Olympia	Olympia	k1gFnSc1	Olympia
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgNnSc7	jenž
měl	mít	k5eAaImAgMnS	mít
dobrou	dobrý	k2eAgFnSc4d1	dobrá
zkušenost	zkušenost	k1gFnSc4	zkušenost
už	už	k6eAd1	už
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1968/1969	[number]	k4	1968/1969
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
mu	on	k3xPp3gMnSc3	on
v	v	k7c6	v
Olympii	Olympia	k1gFnSc6	Olympia
vyšly	vyjít	k5eAaPmAgFnP	vyjít
4	[number]	k4	4
knihy	kniha	k1gFnPc1	kniha
<g/>
)	)	kIx)	)
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
exkluzivní	exkluzivní	k2eAgFnSc4d1	exkluzivní
smlouvu	smlouva	k1gFnSc4	smlouva
na	na	k7c4	na
vydávání	vydávání	k1gNnSc4	vydávání
Sebraných	sebraný	k2eAgInPc2d1	sebraný
spisů	spis	k1gInPc2	spis
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Foglara	Foglar	k1gMnSc2	Foglar
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
tak	tak	k6eAd1	tak
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
všech	všecek	k3xTgInPc2	všecek
26	[number]	k4	26
jeho	jeho	k3xOp3gFnPc2	jeho
knih	kniha	k1gFnPc2	kniha
(	(	kIx(	(
<g/>
s	s	k7c7	s
edičními	ediční	k2eAgFnPc7d1	ediční
poznámkami	poznámka	k1gFnPc7	poznámka
Václava	Václav	k1gMnSc2	Václav
Noska-Windyho	Noska-Windy	k1gMnSc2	Noska-Windy
a	a	k8xC	a
posledních	poslední	k2eAgInPc2d1	poslední
pět	pět	k4xCc4	pět
svazků	svazek	k1gInPc2	svazek
<g/>
,	,	kIx,	,
výpisy	výpis	k1gInPc4	výpis
z	z	k7c2	z
kronik	kronika	k1gFnPc2	kronika
<g/>
,	,	kIx,	,
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
editorem	editor	k1gMnSc7	editor
Milošem	Miloš	k1gMnSc7	Miloš
Zapletalem	Zapletal	k1gMnSc7	Zapletal
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
svazky	svazek	k1gInPc1	svazek
edice	edice	k1gFnSc2	edice
byly	být	k5eAaImAgInP	být
se	s	k7c7	s
souhlasem	souhlas	k1gInSc7	souhlas
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Foglara	Foglar	k1gMnSc2	Foglar
podrobeny	podroben	k2eAgInPc1d1	podroben
citlivé	citlivý	k2eAgFnSc6d1	citlivá
redakční	redakční	k2eAgFnSc6d1	redakční
a	a	k8xC	a
jazykové	jazykový	k2eAgFnSc6d1	jazyková
úpravě	úprava	k1gFnSc6	úprava
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
byly	být	k5eAaImAgFnP	být
blíže	blízce	k6eAd2	blízce
současnému	současný	k2eAgNnSc3d1	současné
mladému	mladé	k1gNnSc3	mladé
čtenáři	čtenář	k1gMnPc1	čtenář
(	(	kIx(	(
<g/>
podíleli	podílet	k5eAaImAgMnP	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
nich	on	k3xPp3gMnPc6	on
zejména	zejména	k9	zejména
šéfredaktor	šéfredaktor	k1gMnSc1	šéfredaktor
a	a	k8xC	a
iniciátor	iniciátor	k1gMnSc1	iniciátor
edice	edice	k1gFnSc2	edice
Vladimír	Vladimír	k1gMnSc1	Vladimír
Dobrovodský	Dobrovodský	k2eAgMnSc1d1	Dobrovodský
a	a	k8xC	a
redaktorka	redaktorka	k1gFnSc1	redaktorka
dr.	dr.	kA	dr.
Jaroslava	Jaroslava	k1gFnSc1	Jaroslava
Poberová	Poberová	k1gFnSc1	Poberová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
k	k	k7c3	k
Foglarovým	Foglarův	k2eAgFnPc3d1	Foglarova
devadesátinám	devadesátina	k1gFnPc3	devadesátina
jubilejní	jubilejní	k2eAgFnSc1d1	jubilejní
<g/>
,	,	kIx,	,
patnácté	patnáctý	k4xOgNnSc1	patnáctý
vydání	vydání	k1gNnSc1	vydání
Hochů	hoch	k1gMnPc2	hoch
od	od	k7c2	od
Bobří	bobří	k2eAgFnSc2d1	bobří
řeky	řeka	k1gFnSc2	řeka
s	s	k7c7	s
pamětním	pamětní	k2eAgInSc7d1	pamětní
listem	list	k1gInSc7	list
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
na	na	k7c6	na
sklonku	sklonek	k1gInSc6	sklonek
Foglarova	Foglarův	k2eAgInSc2d1	Foglarův
života	život	k1gInSc2	život
splnilo	splnit	k5eAaPmAgNnS	splnit
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Olympia	Olympia	k1gFnSc1	Olympia
jeho	jeho	k3xOp3gNnSc4	jeho
největší	veliký	k2eAgNnSc4d3	veliký
přání	přání	k1gNnSc4	přání
a	a	k8xC	a
vydalo	vydat	k5eAaPmAgNnS	vydat
v	v	k7c6	v
samostatné	samostatný	k2eAgFnSc6d1	samostatná
knize	kniha	k1gFnSc6	kniha
všechny	všechen	k3xTgInPc1	všechen
kreslené	kreslený	k2eAgInPc1d1	kreslený
seriálové	seriálový	k2eAgInPc1d1	seriálový
příběhy	příběh	k1gInPc1	příběh
Rychlé	Rychlé	k2eAgInPc1d1	Rychlé
šípy	šíp	k1gInPc1	šíp
a	a	k8xC	a
poté	poté	k6eAd1	poté
rovněž	rovněž	k9	rovněž
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
knize	kniha	k1gFnSc6	kniha
kreslené	kreslený	k2eAgInPc4d1	kreslený
seriály	seriál	k1gInPc4	seriál
Svorní	svorný	k2eAgMnPc1d1	svorný
gambusíni	gambusín	k1gMnPc1	gambusín
a	a	k8xC	a
jiné	jiný	k2eAgInPc1d1	jiný
příběhy	příběh	k1gInPc1	příběh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bezprostřední	bezprostřední	k2eAgFnSc1d1	bezprostřední
porevoluční	porevoluční	k2eAgFnSc1d1	porevoluční
atmosféra	atmosféra	k1gFnSc1	atmosféra
představovala	představovat	k5eAaImAgFnS	představovat
mimořádné	mimořádný	k2eAgNnSc4d1	mimořádné
oživení	oživení	k1gNnSc4	oživení
zájmu	zájem	k1gInSc2	zájem
o	o	k7c4	o
Foglarovo	Foglarův	k2eAgNnSc4d1	Foglarovo
dílo	dílo	k1gNnSc4	dílo
<g/>
,	,	kIx,	,
doložené	doložený	k2eAgInPc1d1	doložený
vysokými	vysoký	k2eAgInPc7d1	vysoký
náklady	náklad	k1gInPc7	náklad
jeho	jeho	k3xOp3gFnPc2	jeho
knih	kniha	k1gFnPc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
i	i	k9	i
jistý	jistý	k2eAgInSc4d1	jistý
projev	projev	k1gInSc4	projev
občanské	občanský	k2eAgFnSc2d1	občanská
satisfakce	satisfakce	k1gFnSc2	satisfakce
za	za	k7c4	za
všechna	všechen	k3xTgNnPc4	všechen
minulá	minulý	k2eAgNnPc4d1	Minulé
příkoří	příkoří	k1gNnPc4	příkoří
<g/>
.	.	kIx.	.
</s>
<s>
Ne	ne	k9	ne
všechno	všechen	k3xTgNnSc1	všechen
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
pořádku	pořádek	k1gInSc6	pořádek
<g/>
.	.	kIx.	.
18.	[number]	k4	18.
listopadu	listopad	k1gInSc2	listopad
1992	[number]	k4	1992
byl	být	k5eAaImAgInS	být
přepaden	přepadnout	k5eAaPmNgMnS	přepadnout
dvěma	dva	k4xCgMnPc7	dva
učni	učeň	k1gMnPc7	učeň
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
ho	on	k3xPp3gMnSc4	on
chtěli	chtít	k5eAaImAgMnP	chtít
okrást	okrást	k5eAaPmF	okrást
o	o	k7c4	o
peníze	peníz	k1gInPc4	peníz
<g/>
.	.	kIx.	.
</s>
<s>
Dovolal	dovolat	k5eAaPmAgInS	dovolat
se	se	k3xPyFc4	se
však	však	k9	však
pomoci	pomoc	k1gFnSc3	pomoc
sousedů	soused	k1gMnPc2	soused
<g/>
.	.	kIx.	.
</s>
<s>
Sdružení	sdružení	k1gNnSc4	sdružení
přátel	přítel	k1gMnPc2	přítel
Jaroslava	Jaroslav	k1gMnSc4	Jaroslav
Foglara	Foglar	k1gMnSc4	Foglar
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
tehdy	tehdy	k6eAd1	tehdy
zareagovalo	zareagovat	k5eAaPmAgNnS	zareagovat
seminářem	seminář	k1gInSc7	seminář
Hledáme	hledat	k5eAaImIp1nP	hledat
lék	lék	k1gInSc4	lék
na	na	k7c4	na
dětskou	dětský	k2eAgFnSc4d1	dětská
kriminalitu	kriminalita	k1gFnSc4	kriminalita
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
rámci	rámec	k1gInSc6	rámec
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
letech	let	k1gInPc6	let
konány	konán	k2eAgInPc4d1	konán
přednáškové	přednáškový	k2eAgInPc4d1	přednáškový
cykly	cyklus	k1gInPc4	cyklus
<g/>
,	,	kIx,	,
natáčeny	natáčen	k2eAgInPc4d1	natáčen
filmy	film	k1gInPc4	film
a	a	k8xC	a
vydávány	vydáván	k2eAgInPc4d1	vydáván
sborníky	sborník	k1gInPc4	sborník
přednášek	přednáška	k1gFnPc2	přednáška
<g/>
.	.	kIx.	.
</s>
<s>
Sama	sám	k3xTgFnSc1	sám
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
dva	dva	k4xCgMnPc1	dva
mladí	mladý	k2eAgMnPc1d1	mladý
chlapci	chlapec	k1gMnPc1	chlapec
jdou	jít	k5eAaImIp3nP	jít
násilně	násilně	k6eAd1	násilně
přepadnout	přepadnout	k5eAaPmF	přepadnout
starého	starý	k1gMnSc2	starý
spisovatele	spisovatel	k1gMnSc2	spisovatel
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
výchově	výchova	k1gFnSc6	výchova
mládeže	mládež	k1gFnSc2	mládež
věnoval	věnovat	k5eAaImAgMnS	věnovat
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
působila	působit	k5eAaImAgFnS	působit
jako	jako	k9	jako
šok	šok	k1gInSc4	šok
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
šok	šok	k1gInSc1	šok
přišel	přijít	k5eAaPmAgInS	přijít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
jara	jaro	k1gNnSc2	jaro
byl	být	k5eAaImAgInS	být
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
hospitalizován	hospitalizován	k2eAgInSc1d1	hospitalizován
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
podle	podle	k7c2	podle
možností	možnost	k1gFnPc2	možnost
dál	daleko	k6eAd2	daleko
vyjížděl	vyjíždět	k5eAaImAgMnS	vyjíždět
na	na	k7c4	na
besedy	beseda	k1gFnPc4	beseda
se	s	k7c7	s
čtenáři	čtenář	k1gMnPc7	čtenář
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jeho	jeho	k3xOp3gFnPc3	jeho
devadesátinám	devadesátina	k1gFnPc3	devadesátina
a	a	k8xC	a
taktéž	taktéž	k?	taktéž
k	k	k7c3	k
nedožitým	dožitý	k2eNgFnPc3d1	dožitý
devadesátinám	devadesátina	k1gFnPc3	devadesátina
ilustrátora	ilustrátor	k1gMnSc2	ilustrátor
Rychlých	Rychlých	k2eAgInPc2d1	Rychlých
šípů	šíp	k1gInPc2	šíp
dr.	dr.	kA	dr.
Jana	Jan	k1gMnSc2	Jan
Fischera	Fischer	k1gMnSc2	Fischer
uspořádalo	uspořádat	k5eAaPmAgNnS	uspořádat
Muzeum	muzeum	k1gNnSc1	muzeum
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
výstavu	výstava	k1gFnSc4	výstava
s	s	k7c7	s
názvem	název	k1gInSc7	název
Po	po	k7c6	po
stopách	stopa	k1gFnPc6	stopa
Rychlých	Rychlých	k2eAgInPc2d1	Rychlých
šípů	šíp	k1gInPc2	šíp
<g/>
.	.	kIx.	.
</s>
<s>
Neznámí	známý	k2eNgMnPc1d1	neznámý
zloději	zloděj	k1gMnPc1	zloděj
však	však	k9	však
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
výstavě	výstava	k1gFnSc6	výstava
ukradli	ukradnout	k5eAaPmAgMnP	ukradnout
jeho	jeho	k3xOp3gInSc1	jeho
první	první	k4xOgInSc1	první
deník	deník	k1gInSc1	deník
a	a	k8xC	a
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
kovových	kovový	k2eAgMnPc2d1	kovový
ježků	ježek	k1gMnPc2	ježek
v	v	k7c6	v
kleci	klec	k1gFnSc6	klec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
závěr	závěr	k1gInSc1	závěr
jeho	jeho	k3xOp3gInSc2	jeho
života	život	k1gInSc2	život
byl	být	k5eAaImAgInS	být
spojen	spojen	k2eAgInSc1d1	spojen
s	s	k7c7	s
horečnými	horečný	k2eAgFnPc7d1	horečná
aktivitami	aktivita	k1gFnPc7	aktivita
některých	některý	k3yIgMnPc2	některý
jeho	jeho	k3xOp3gMnPc2	jeho
příznivců	příznivec	k1gMnPc2	příznivec
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
byl	být	k5eAaImAgInS	být
poznamenán	poznamenat	k5eAaPmNgInS	poznamenat
mediálně	mediálně	k6eAd1	mediálně
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
sledovaným	sledovaný	k2eAgNnSc7d1	sledované
hašteřením	hašteření	k1gNnSc7	hašteření
různých	různý	k2eAgFnPc2d1	různá
skupin	skupina	k1gFnPc2	skupina
jeho	jeho	k3xOp3gMnPc2	jeho
příznivců	příznivec	k1gMnPc2	příznivec
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
diametrálně	diametrálně	k6eAd1	diametrálně
odlišný	odlišný	k2eAgInSc4d1	odlišný
názor	názor	k1gInSc4	názor
na	na	k7c4	na
využití	využití	k1gNnSc4	využití
jeho	jeho	k3xOp3gInSc2	jeho
odkazu	odkaz	k1gInSc2	odkaz
a	a	k8xC	a
především	především	k9	především
jeho	jeho	k3xOp3gFnPc4	jeho
finanční	finanční	k2eAgFnPc4d1	finanční
pozůstalosti	pozůstalost	k1gFnPc4	pozůstalost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Foglar	Foglar	k1gMnSc1	Foglar
zemřel	zemřít	k5eAaPmAgMnS	zemřít
23.	[number]	k4	23.
ledna	leden	k1gInSc2	leden
1999	[number]	k4	1999
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
Thomayerově	Thomayerův	k2eAgFnSc6d1	Thomayerova
nemocnici	nemocnice	k1gFnSc6	nemocnice
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
91	[number]	k4	91
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pochován	pochován	k2eAgMnSc1d1	pochován
na	na	k7c6	na
Vinohradském	vinohradský	k2eAgInSc6d1	vinohradský
hřbitově	hřbitov	k1gInSc6	hřbitov
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
28.	[number]	k4	28.
října	říjen	k1gInSc2	říjen
2017	[number]	k4	2017
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
z	z	k7c2	z
rukou	ruka	k1gFnPc2	ruka
českého	český	k2eAgMnSc2d1	český
prezidenta	prezident	k1gMnSc2	prezident
Miloše	Miloš	k1gMnSc2	Miloš
Zemana	Zeman	k1gMnSc2	Zeman
udělena	udělen	k2eAgFnSc1d1	udělena
medaile	medaile	k1gFnSc1	medaile
Za	za	k7c4	za
zásluhy	zásluha	k1gFnPc4	zásluha
(	(	kIx(	(
<g/>
in	in	k?	in
memoriam	memoriam	k1gInSc1	memoriam
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Současnost	současnost	k1gFnSc4	současnost
===	===	k?	===
</s>
</p>
<p>
<s>
K	k	k7c3	k
výročí	výročí	k1gNnSc3	výročí
110	[number]	k4	110
let	léto	k1gNnPc2	léto
od	od	k7c2	od
narození	narození	k1gNnSc2	narození
pana	pan	k1gMnSc2	pan
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Foglara	Foglar	k1gMnSc2	Foglar
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
<g/>
1907	[number]	k4	1907
<g/>
)	)	kIx)	)
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
první	první	k4xOgNnSc1	první
stálé	stálý	k2eAgNnSc1d1	stálé
muzeum	muzeum	k1gNnSc1	muzeum
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Foglara	Foglar	k1gMnSc2	Foglar
v	v	k7c6	v
prostorách	prostora	k1gFnPc6	prostora
hradu	hrad	k1gInSc2	hrad
Ledeč	Ledeč	k1gFnSc1	Ledeč
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
oznámilo	oznámit	k5eAaPmAgNnS	oznámit
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Albatros	albatros	k1gMnSc1	albatros
<g/>
,	,	kIx,	,
že	že	k8xS	že
znovu	znovu	k6eAd1	znovu
vydá	vydat	k5eAaPmIp3nS	vydat
zásadní	zásadní	k2eAgFnPc4d1	zásadní
díla	dílo	k1gNnPc4	dílo
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Foglara	Foglar	k1gMnSc2	Foglar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Ohlasy	ohlas	k1gInPc1	ohlas
na	na	k7c4	na
dílo	dílo	k1gNnSc4	dílo
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Foglara	Foglar	k1gMnSc2	Foglar
==	==	k?	==
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
dílo	dílo	k1gNnSc1	dílo
ovlivnilo	ovlivnit	k5eAaPmAgNnS	ovlivnit
významnou	významný	k2eAgFnSc4d1	významná
část	část	k1gFnSc4	část
populace	populace	k1gFnSc2	populace
Československa	Československo	k1gNnSc2	Československo
a	a	k8xC	a
dalo	dát	k5eAaPmAgNnS	dát
vzniknout	vzniknout	k5eAaPmF	vzniknout
určité	určitý	k2eAgFnSc3d1	určitá
subkultuře	subkultura	k1gFnSc3	subkultura
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
všech	všecek	k3xTgInPc2	všecek
politických	politický	k2eAgInPc2d1	politický
režimů	režim	k1gInPc2	režim
měl	mít	k5eAaImAgMnS	mít
mnoho	mnoho	k6eAd1	mnoho
příznivců	příznivec	k1gMnPc2	příznivec
a	a	k8xC	a
přátel	přítel	k1gMnPc2	přítel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
mnoho	mnoho	k4c1	mnoho
kritiků	kritik	k1gMnPc2	kritik
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
nepřátel	nepřítel	k1gMnPc2	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
měl	mít	k5eAaImAgMnS	mít
mnoho	mnoho	k4c4	mnoho
napodobitelů	napodobitel	k1gMnPc2	napodobitel
i	i	k8xC	i
několik	několik	k4yIc1	několik
epigonů	epigon	k1gMnPc2	epigon
<g/>
.	.	kIx.	.
</s>
<s>
Svým	svůj	k3xOyFgNnSc7	svůj
celoživotním	celoživotní	k2eAgNnSc7d1	celoživotní
zaměřením	zaměření	k1gNnSc7	zaměření
na	na	k7c4	na
zážitkovou	zážitkový	k2eAgFnSc4d1	zážitková
výchovu	výchova	k1gFnSc4	výchova
mladých	mladý	k1gMnPc2	mladý
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
mnoho	mnoho	k4c4	mnoho
lidí	člověk	k1gMnPc2	člověk
velkým	velký	k2eAgMnSc7d1	velký
inspirátorem	inspirátor	k1gMnSc7	inspirátor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
dílo	dílo	k1gNnSc4	dílo
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
stínadelskou	stínadelský	k2eAgFnSc4d1	stínadelská
trilogii	trilogie	k1gFnSc4	trilogie
o	o	k7c6	o
Rychlých	Rychlých	k2eAgInPc6d1	Rychlých
šípech	šíp	k1gInPc6	šíp
<g/>
,	,	kIx,	,
navazuje	navazovat	k5eAaImIp3nS	navazovat
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgMnPc2d1	další
autorů	autor	k1gMnPc2	autor
-	-	kIx~	-
například	například	k6eAd1	například
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Hrnčíř	Hrnčíř	k1gMnSc1	Hrnčíř
s	s	k7c7	s
několika	několik	k4yIc7	několik
díly	dílo	k1gNnPc7	dílo
Ostrov	ostrov	k1gInSc1	ostrov
Uctívačů	uctívač	k1gMnPc2	uctívač
ginga	ging	k1gMnSc2	ging
nebo	nebo	k8xC	nebo
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Velinský	velinský	k2eAgMnSc1d1	velinský
(	(	kIx(	(
<g/>
Kapitán	kapitán	k1gMnSc1	kapitán
Kid	Kid	k1gMnSc1	Kid
<g/>
)	)	kIx)	)
s	s	k7c7	s
dílem	díl	k1gInSc7	díl
Poslední	poslední	k2eAgNnSc4d1	poslední
tajemství	tajemství	k1gNnSc4	tajemství
Jana	Jan	k1gMnSc2	Jan
T.	T.	kA	T.
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Už	už	k6eAd1	už
v	v	k7c6	v
60.	[number]	k4	60.
letech	let	k1gInPc6	let
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
okruhu	okruh	k1gInSc6	okruh
sběratelů	sběratel	k1gMnPc2	sběratel
Klub	klub	k1gInSc1	klub
přátel	přítel	k1gMnPc2	přítel
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Foglara	Foglar	k1gMnSc2	Foglar
<g/>
,	,	kIx,	,
po	po	k7c6	po
sametové	sametový	k2eAgFnSc6d1	sametová
revoluci	revoluce	k1gFnSc6	revoluce
pak	pak	k6eAd1	pak
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
navázalo	navázat	k5eAaPmAgNnS	navázat
Sdružení	sdružení	k1gNnSc1	sdružení
přátel	přítel	k1gMnPc2	přítel
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Foglara	Foglar	k1gMnSc2	Foglar
<g/>
,	,	kIx,	,
fungující	fungující	k2eAgInPc1d1	fungující
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
sběratelský	sběratelský	k2eAgInSc1d1	sběratelský
kroužek	kroužek	k1gInSc1	kroužek
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
propagovat	propagovat	k5eAaImF	propagovat
Foglarovo	Foglarův	k2eAgNnSc4d1	Foglarovo
dílo	dílo	k1gNnSc4	dílo
mezi	mezi	k7c7	mezi
současnou	současný	k2eAgFnSc7d1	současná
mládeží	mládež	k1gFnSc7	mládež
a	a	k8xC	a
funguje	fungovat	k5eAaImIp3nS	fungovat
jako	jako	k9	jako
běžné	běžný	k2eAgNnSc1d1	běžné
sdružení	sdružení	k1gNnSc1	sdružení
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
mládeže	mládež	k1gFnSc2	mládež
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
možností	možnost	k1gFnPc2	možnost
se	se	k3xPyFc4	se
také	také	k9	také
snaží	snažit	k5eAaImIp3nS	snažit
navázat	navázat	k5eAaPmF	navázat
i	i	k9	i
na	na	k7c4	na
myšlenku	myšlenka	k1gFnSc4	myšlenka
dětských	dětský	k2eAgInPc2d1	dětský
čtenářských	čtenářský	k2eAgInPc2d1	čtenářský
klubů	klub	k1gInPc2	klub
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
v	v	k7c6	v
šedesátých	šedesátý	k4xOgNnPc6	šedesátý
letech	léto	k1gNnPc6	léto
20.	[number]	k4	20.
století	století	k1gNnSc2	století
rozvíjel	rozvíjet	k5eAaImAgInS	rozvíjet
časopis	časopis	k1gInSc1	časopis
ABC	ABC	kA	ABC
mladých	mladý	k2eAgMnPc2d1	mladý
techniků	technik	k1gMnPc2	technik
a	a	k8xC	a
přírodovědců	přírodovědec	k1gMnPc2	přírodovědec
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
Miloš	Miloš	k1gMnSc1	Miloš
Dvorský	Dvorský	k1gMnSc1	Dvorský
na	na	k7c6	na
přehledné	přehledný	k2eAgFnSc6d1	přehledná
analýze	analýza	k1gFnSc6	analýza
statistik	statistika	k1gFnPc2	statistika
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
není	být	k5eNaImIp3nS	být
nijak	nijak	k6eAd1	nijak
snadné	snadný	k2eAgNnSc1d1	snadné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Spolupráce	spolupráce	k1gFnSc1	spolupráce
s	s	k7c7	s
StB	StB	k1gFnSc7	StB
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Foglar	Foglar	k1gMnSc1	Foglar
byl	být	k5eAaImAgMnS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
veřejných	veřejný	k2eAgFnPc2d1	veřejná
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
ke	k	k7c3	k
svým	svůj	k3xOyFgInPc3	svůj
stykům	styk	k1gInPc3	styk
se	se	k3xPyFc4	se
Státní	státní	k2eAgFnSc7d1	státní
bezpečností	bezpečnost	k1gFnSc7	bezpečnost
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
velmi	velmi	k6eAd1	velmi
krátkým	krátká	k1gFnPc3	krátká
<g/>
,	,	kIx,	,
samy	sám	k3xTgFnPc1	sám
veřejně	veřejně	k6eAd1	veřejně
přiznaly	přiznat	k5eAaPmAgFnP	přiznat
dávno	dávno	k6eAd1	dávno
před	před	k7c7	před
zveřejněním	zveřejnění	k1gNnSc7	zveřejnění
porevolučních	porevoluční	k2eAgInPc2d1	porevoluční
seznamů	seznam	k1gInPc2	seznam
agentů	agent	k1gMnPc2	agent
StB	StB	k1gFnSc2	StB
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Cibulkovy	Cibulkův	k2eAgInPc1d1	Cibulkův
seznamy	seznam	k1gInPc1	seznam
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
podobné	podobný	k2eAgFnPc1d1	podobná
publikace	publikace	k1gFnPc1	publikace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
jej	on	k3xPp3gMnSc4	on
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Hanzel	Hanzel	k1gMnSc1	Hanzel
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
spisovatele	spisovatel	k1gMnSc2	spisovatel
na	na	k7c6	na
sklonku	sklonek	k1gInSc6	sklonek
socialismu	socialismus	k1gInSc2	socialismus
krátce	krátce	k6eAd1	krátce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
intenzivně	intenzivně	k6eAd1	intenzivně
propagoval	propagovat	k5eAaImAgMnS	propagovat
<g/>
,	,	kIx,	,
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
najednou	najednou	k6eAd1	najednou
veřejně	veřejně	k6eAd1	veřejně
v	v	k7c6	v
týdeníku	týdeník	k1gInSc6	týdeník
Reflex	reflex	k1gInSc1	reflex
obvinil	obvinit	k5eAaPmAgInS	obvinit
z	z	k7c2	z
udavačství	udavačství	k1gNnSc2	udavačství
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Mediálně	mediálně	k6eAd1	mediálně
vděčná	vděčný	k2eAgFnSc1d1	vděčná
kauza	kauza	k1gFnSc1	kauza
měla	mít	k5eAaImAgFnS	mít
zásadní	zásadní	k2eAgFnSc4d1	zásadní
vadu	vada	k1gFnSc4	vada
–	–	k?	–
Hanzel	Hanzel	k1gFnSc1	Hanzel
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
kdysi	kdysi	k6eAd1	kdysi
ve	v	k7c6	v
zlém	zlé	k1gNnSc6	zlé
rozešel	rozejít	k5eAaPmAgMnS	rozejít
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
vůči	vůči	k7c3	vůči
němu	on	k3xPp3gMnSc3	on
silně	silně	k6eAd1	silně
zaujatý	zaujatý	k2eAgInSc1d1	zaujatý
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Především	především	k9	především
však	však	k9	však
své	svůj	k3xOyFgNnSc4	svůj
tvrzení	tvrzení	k1gNnSc4	tvrzení
o	o	k7c6	o
jeho	jeho	k3xOp3gNnSc6	jeho
udavačství	udavačství	k1gNnSc6	udavačství
opíral	opírat	k5eAaImAgMnS	opírat
o	o	k7c4	o
neupřesněné	upřesněný	k2eNgNnSc4d1	neupřesněné
svědectví	svědectví	k1gNnSc4	svědectví
z	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
ruky	ruka	k1gFnSc2	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Pravdou	pravda	k1gFnSc7	pravda
navíc	navíc	k6eAd1	navíc
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
StB	StB	k1gFnSc1	StB
o	o	k7c4	o
něj	on	k3xPp3gMnSc4	on
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
jeho	jeho	k3xOp3gFnSc2	jeho
apolitičnosti	apolitičnost	k1gFnSc2	apolitičnost
přestala	přestat	k5eAaPmAgFnS	přestat
jevit	jevit	k5eAaImF	jevit
v	v	k7c6	v
padesátých	padesátý	k4xOgNnPc6	padesátý
letech	léto	k1gNnPc6	léto
už	už	k6eAd1	už
po	po	k7c4	po
půl	půl	k1xP	půl
roce	rok	k1gInSc6	rok
zájem	zájem	k1gInSc1	zájem
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
byl	být	k5eAaImAgInS	být
kontrarozvědkou	kontrarozvědka	k1gFnSc7	kontrarozvědka
dokonce	dokonce	k9	dokonce
krátce	krátce	k6eAd1	krátce
evidován	evidován	k2eAgInSc1d1	evidován
pod	pod	k7c7	pod
krycím	krycí	k2eAgNnSc7d1	krycí
jménem	jméno	k1gNnSc7	jméno
"	"	kIx"	"
<g/>
Jestřáb	jestřáb	k1gMnSc1	jestřáb
<g/>
"	"	kIx"	"
jako	jako	k8xS	jako
potenciální	potenciální	k2eAgMnSc1d1	potenciální
nepřítel	nepřítel	k1gMnSc1	nepřítel
socialistického	socialistický	k2eAgInSc2d1	socialistický
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Spekulace	spekulace	k1gFnPc1	spekulace
o	o	k7c4	o
sexuální	sexuální	k2eAgFnSc4d1	sexuální
orientaci	orientace	k1gFnSc4	orientace
===	===	k?	===
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
líčení	líčení	k1gNnSc1	líčení
přátelských	přátelský	k2eAgInPc2d1	přátelský
vztahů	vztah	k1gInPc2	vztah
mezi	mezi	k7c7	mezi
chlapci	chlapec	k1gMnPc7	chlapec
i	i	k8xC	i
literární	literární	k2eAgNnPc1d1	literární
opěvování	opěvování	k1gNnPc1	opěvování
chlapeckých	chlapecký	k2eAgFnPc2d1	chlapecká
povah	povaha	k1gFnPc2	povaha
i	i	k8xC	i
opálených	opálený	k2eAgNnPc2d1	opálené
těl	tělo	k1gNnPc2	tělo
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc4	jeho
celoživotní	celoživotní	k2eAgNnSc4d1	celoživotní
působení	působení	k1gNnSc4	působení
mezi	mezi	k7c7	mezi
chlapeckou	chlapecký	k2eAgFnSc7d1	chlapecká
mládeží	mládež	k1gFnSc7	mládež
i	i	k8xC	i
staromládenecký	staromládenecký	k2eAgInSc1d1	staromládenecký
život	život	k1gInSc1	život
s	s	k7c7	s
matkou	matka	k1gFnSc7	matka
zavdávaly	zavdávat	k5eAaImAgFnP	zavdávat
příčinu	příčina	k1gFnSc4	příčina
ke	k	k7c3	k
spekulacím	spekulace	k1gFnPc3	spekulace
o	o	k7c4	o
jeho	jeho	k3xOp3gFnSc4	jeho
sexuální	sexuální	k2eAgFnSc4d1	sexuální
orientaci	orientace	k1gFnSc4	orientace
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýrazněji	výrazně	k6eAd3	výrazně
toto	tento	k3xDgNnSc4	tento
obvinění	obvinění	k1gNnSc4	obvinění
rozvířil	rozvířit	k5eAaPmAgMnS	rozvířit
již	již	k9	již
zmíněný	zmíněný	k2eAgMnSc1d1	zmíněný
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Hanzel	Hanzel	k1gMnSc1	Hanzel
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
zapojil	zapojit	k5eAaPmAgMnS	zapojit
do	do	k7c2	do
soudního	soudní	k2eAgInSc2d1	soudní
sporu	spor	k1gInSc2	spor
o	o	k7c4	o
dědictví	dědictví	k1gNnPc4	dědictví
<g/>
,	,	kIx,	,
vedeného	vedený	k2eAgMnSc2d1	vedený
mezi	mezi	k7c7	mezi
Foglarovým	Foglarův	k2eAgMnSc7d1	Foglarův
synovcem	synovec	k1gMnSc7	synovec
a	a	k8xC	a
Nadací	nadace	k1gFnSc7	nadace
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Foglara	Foglar	k1gMnSc2	Foglar
<g/>
.	.	kIx.	.
</s>
<s>
Redaktor	redaktor	k1gMnSc1	redaktor
Reflexu	reflex	k1gInSc2	reflex
Jan	Jan	k1gMnSc1	Jan
Potůček	Potůček	k1gMnSc1	Potůček
dostal	dostat	k5eAaPmAgMnS	dostat
od	od	k7c2	od
Hanzela	Hanzela	k1gMnSc2	Hanzela
někdy	někdy	k6eAd1	někdy
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
cyklostylovanou	cyklostylovaný	k2eAgFnSc4d1	cyklostylovaná
"	"	kIx"	"
<g/>
lékařskou	lékařský	k2eAgFnSc4d1	lékařská
zprávu	zpráva	k1gFnSc4	zpráva
<g/>
"	"	kIx"	"
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
údajné	údajný	k2eAgFnSc6d1	údajná
homosexualitě	homosexualita	k1gFnSc6	homosexualita
<g/>
.	.	kIx.	.
</s>
<s>
Potůček	Potůček	k1gMnSc1	Potůček
ji	on	k3xPp3gFnSc4	on
vyhodnotil	vyhodnotit	k5eAaPmAgMnS	vyhodnotit
jako	jako	k9	jako
velmi	velmi	k6eAd1	velmi
amatérskou	amatérský	k2eAgFnSc4d1	amatérská
analýzu	analýza	k1gFnSc4	analýza
osobnosti	osobnost	k1gFnSc2	osobnost
<g/>
,	,	kIx,	,
založenou	založený	k2eAgFnSc7d1	založená
na	na	k7c6	na
citátech	citát	k1gInPc6	citát
z	z	k7c2	z
foglarovek	foglarovka	k1gFnPc2	foglarovka
<g/>
,	,	kIx,	,
deníků	deník	k1gInPc2	deník
a	a	k8xC	a
vyjádření	vyjádření	k1gNnPc2	vyjádření
bývalých	bývalý	k2eAgMnPc2d1	bývalý
členů	člen	k1gMnPc2	člen
Foglarova	Foglarův	k2eAgInSc2d1	Foglarův
oddílu	oddíl	k1gInSc2	oddíl
<g/>
.	.	kIx.	.
</s>
<s>
Byť	byť	k8xS	byť
Potůček	Potůček	k1gMnSc1	Potůček
zprávu	zpráva	k1gFnSc4	zpráva
nikde	nikde	k6eAd1	nikde
na	na	k7c4	na
Hanzelovu	Hanzelův	k2eAgFnSc4d1	Hanzelův
žádost	žádost	k1gFnSc4	žádost
nezveřejnil	zveřejnit	k5eNaPmAgMnS	zveřejnit
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
její	její	k3xOp3gFnSc4	její
existenci	existence	k1gFnSc4	existence
potvrdil	potvrdit	k5eAaPmAgInS	potvrdit
(	(	kIx(	(
<g/>
Hanzel	Hanzel	k1gFnSc1	Hanzel
ji	on	k3xPp3gFnSc4	on
naopak	naopak	k6eAd1	naopak
popřel	popřít	k5eAaPmAgMnS	popřít
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
mezitím	mezitím	k6eAd1	mezitím
dostala	dostat	k5eAaPmAgFnS	dostat
i	i	k9	i
do	do	k7c2	do
dalších	další	k2eAgFnPc2d1	další
rukou	ruka	k1gFnPc2	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Hanzel	Hanzet	k5eAaImAgMnS	Hanzet
nicméně	nicméně	k8xC	nicméně
na	na	k7c6	na
Foglarově	Foglarův	k2eAgNnSc6d1	Foglarovo
homoerotickém	homoerotický	k2eAgNnSc6d1	homoerotické
cítění	cítění	k1gNnSc6	cítění
trval	trvat	k5eAaImAgInS	trvat
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
dokládal	dokládat	k5eAaImAgInS	dokládat
opět	opět	k6eAd1	opět
některými	některý	k3yIgNnPc7	některý
neověřitelnými	ověřitelný	k2eNgNnPc7d1	neověřitelné
svědectvími	svědectví	k1gNnPc7	svědectví
z	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
či	či	k8xC	či
třetí	třetí	k4xOgFnSc2	třetí
ruky	ruka	k1gFnSc2	ruka
<g/>
.	.	kIx.	.
<g/>
Provokativní	provokativní	k2eAgFnSc1d1	provokativní
představa	představa	k1gFnSc1	představa
chlapeckého	chlapecký	k2eAgMnSc2d1	chlapecký
spisovatele	spisovatel	k1gMnSc2	spisovatel
jako	jako	k8xS	jako
gaye	gay	k1gMnSc2	gay
vyvolala	vyvolat	k5eAaPmAgFnS	vyvolat
řadu	řada	k1gFnSc4	řada
polemik	polemika	k1gFnPc2	polemika
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
autoři	autor	k1gMnPc1	autor
najednou	najednou	k6eAd1	najednou
začali	začít	k5eAaPmAgMnP	začít
s	s	k7c7	s
naprostou	naprostý	k2eAgFnSc7d1	naprostá
samozřejmostí	samozřejmost	k1gFnSc7	samozřejmost
psát	psát	k5eAaImF	psát
o	o	k7c4	o
množství	množství	k1gNnSc4	množství
sexuality	sexualita	k1gFnSc2	sexualita
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
jeho	jeho	k3xOp3gNnPc2	jeho
děl	dělo	k1gNnPc2	dělo
údajně	údajně	k6eAd1	údajně
objevuje	objevovat	k5eAaImIp3nS	objevovat
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Zikmund-Lender	Zikmund-Lender	k1gInSc1	Zikmund-Lender
dokonce	dokonce	k9	dokonce
provedl	provést	k5eAaPmAgInS	provést
výzkum	výzkum	k1gInSc1	výzkum
mezi	mezi	k7c7	mezi
gayi	gay	k1gMnPc7	gay
nad	nad	k7c4	nad
30	[number]	k4	30
let	léto	k1gNnPc2	léto
a	a	k8xC	a
zjišťoval	zjišťovat	k5eAaImAgInS	zjišťovat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
vnímali	vnímat	k5eAaImAgMnP	vnímat
Foglarovo	Foglarův	k2eAgNnSc4d1	Foglarovo
dílo	dílo	k1gNnSc4	dílo
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
vyjadřovala	vyjadřovat	k5eAaImAgFnS	vyjadřovat
<g />
.	.	kIx.	.
</s>
<s>
k	k	k7c3	k
homoerotickému	homoerotický	k2eAgInSc3d1	homoerotický
výkladu	výklad	k1gInSc3	výklad
díla	dílo	k1gNnSc2	dílo
Jaroslava	Jaroslav	k1gMnSc4	Jaroslav
Foglara	Foglar	k1gMnSc4	Foglar
spíše	spíše	k9	spíše
odpor	odpor	k1gInSc4	odpor
a	a	k8xC	a
hodnotili	hodnotit	k5eAaImAgMnP	hodnotit
čistotu	čistota	k1gFnSc4	čistota
a	a	k8xC	a
ideál	ideál	k1gInSc4	ideál
přátelství	přátelství	k1gNnSc2	přátelství
<g/>
,	,	kIx,	,
zobrazené	zobrazený	k2eAgFnSc2d1	zobrazená
v	v	k7c6	v
knihách	kniha	k1gFnPc6	kniha
<g/>
,	,	kIx,	,
vesměs	vesměs	k6eAd1	vesměs
jako	jako	k9	jako
neerotické	erotický	k2eNgFnPc1d1	neerotická
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Byť	byť	k8xS	byť
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
úrovni	úroveň	k1gFnSc6	úroveň
snažili	snažit	k5eAaImAgMnP	snažit
někteří	některý	k3yIgMnPc1	některý
Foglara	Foglara	k1gFnSc1	Foglara
hájit	hájit	k5eAaImF	hájit
<g/>
,	,	kIx,	,
laická	laický	k2eAgFnSc1d1	laická
veřejnost	veřejnost	k1gFnSc1	veřejnost
většinou	většinou	k6eAd1	většinou
postřehla	postřehnout	k5eAaPmAgFnS	postřehnout
jen	jen	k9	jen
pomyslnou	pomyslný	k2eAgFnSc4d1	pomyslná
provokativní	provokativní	k2eAgFnSc4d1	provokativní
mlhu	mlha	k1gFnSc4	mlha
o	o	k7c6	o
Foglarově	Foglarův	k2eAgFnSc6d1	Foglarova
sexualitě	sexualita	k1gFnSc6	sexualita
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
tohoto	tento	k3xDgInSc2	tento
kontextu	kontext	k1gInSc2	kontext
hledání	hledání	k1gNnSc1	hledání
sexuality	sexualita	k1gFnSc2	sexualita
zapadá	zapadat	k5eAaPmIp3nS	zapadat
i	i	k9	i
názor	názor	k1gInSc1	názor
sexuologa	sexuolog	k1gMnSc2	sexuolog
Petra	Petr	k1gMnSc2	Petr
Weisse	Weiss	k1gMnSc2	Weiss
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
něhož	jenž	k3xRgInSc2	jenž
Hans	Hans	k1gMnSc1	Hans
Christian	Christian	k1gMnSc1	Christian
Andersen	Andersen	k1gMnSc1	Andersen
<g/>
,	,	kIx,	,
Lewis	Lewis	k1gFnSc1	Lewis
Caroll	Caroll	k1gMnSc1	Caroll
i	i	k8xC	i
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Foglar	Foglar	k1gMnSc1	Foglar
byli	být	k5eAaImAgMnP	být
pedofily	pedofil	k1gMnPc4	pedofil
<g/>
.	.	kIx.	.
<g/>
Znalci	znalec	k1gMnPc7	znalec
Foglarova	Foglarův	k2eAgNnSc2d1	Foglarovo
díla	dílo	k1gNnSc2	dílo
vesměs	vesměs	k6eAd1	vesměs
jakékoli	jakýkoli	k3yIgFnPc4	jakýkoli
spekulace	spekulace	k1gFnPc4	spekulace
o	o	k7c6	o
spisovatelově	spisovatelův	k2eAgFnSc6d1	spisovatelova
sexuální	sexuální	k2eAgFnSc6d1	sexuální
orientaci	orientace	k1gFnSc6	orientace
odmítali	odmítat	k5eAaImAgMnP	odmítat
<g/>
.	.	kIx.	.
</s>
<s>
Souhlasí	souhlasit	k5eAaImIp3nS	souhlasit
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
dílech	dílo	k1gNnPc6	dílo
používal	používat	k5eAaImAgMnS	používat
téměř	téměř	k6eAd1	téměř
výhradně	výhradně	k6eAd1	výhradně
chlapecké	chlapecký	k2eAgFnPc4d1	chlapecká
postavy	postava	k1gFnPc4	postava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Chatě	chata	k1gFnSc6	chata
v	v	k7c6	v
Jezerní	jezerní	k2eAgFnSc6d1	jezerní
kotlině	kotlina	k1gFnSc6	kotlina
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
Pavel	Pavel	k1gMnSc1	Pavel
Zeman	Zeman	k1gMnSc1	Zeman
a	a	k8xC	a
Ludva	Ludva	k1gMnSc1	Ludva
Grygar	Grygar	k1gMnSc1	Grygar
<g/>
,	,	kIx,	,
Přístav	přístav	k1gInSc1	přístav
volá	volat	k5eAaImIp3nS	volat
je	být	k5eAaImIp3nS	být
příběhem	příběh	k1gInSc7	příběh
Jiřího	Jiří	k1gMnSc2	Jiří
Dražana	Dražan	k1gMnSc2	Dražan
a	a	k8xC	a
Ládi	Láďa	k1gMnSc2	Láďa
Vilemína	Vilemína	k1gFnSc1	Vilemína
<g/>
,	,	kIx,	,
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Když	když	k8xS	když
duben	duben	k1gInSc1	duben
přichází	přicházet	k5eAaImIp3nS	přicházet
jsou	být	k5eAaImIp3nP	být
hlavními	hlavní	k2eAgMnPc7d1	hlavní
hrdiny	hrdina	k1gMnPc7	hrdina
Lubor	Lubor	k1gMnSc1	Lubor
Klement	Klement	k1gMnSc1	Klement
a	a	k8xC	a
Vojta	Vojta	k1gMnSc1	Vojta
Řezina	řezina	k1gFnSc1	řezina
<g/>
.	.	kIx.	.
</s>
<s>
Znalci	znalec	k1gMnPc1	znalec
to	ten	k3xDgNnSc4	ten
zdůvodňují	zdůvodňovat	k5eAaImIp3nP	zdůvodňovat
především	především	k9	především
dříve	dříve	k6eAd2	dříve
běžným	běžný	k2eAgNnSc7d1	běžné
pojetím	pojetí	k1gNnSc7	pojetí
oddělené	oddělený	k2eAgFnSc2d1	oddělená
výchovy	výchova	k1gFnSc2	výchova
chlapců	chlapec	k1gMnPc2	chlapec
a	a	k8xC	a
děvčat	děvče	k1gNnPc2	děvče
<g/>
.	.	kIx.	.
</s>
<s>
Miloš	Miloš	k1gMnSc1	Miloš
Zapletal	Zapletal	k1gMnSc1	Zapletal
souhlasí	souhlasit	k5eAaImIp3nS	souhlasit
<g/>
,	,	kIx,	,
že	že	k8xS	že
žádný	žádný	k3yNgMnSc1	žádný
z	z	k7c2	z
hrdinů	hrdina	k1gMnPc2	hrdina
jeho	jeho	k3xOp3gInPc2	jeho
chlapeckých	chlapecký	k2eAgInPc2d1	chlapecký
románů	román	k1gInPc2	román
nepokukuje	pokukovat	k5eNaImIp3nS	pokukovat
po	po	k7c6	po
děvčatech	děvče	k1gNnPc6	děvče
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
Foglar	Foglar	k1gMnSc1	Foglar
sám	sám	k3xTgMnSc1	sám
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
neoženil	oženit	k5eNaPmAgMnS	oženit
a	a	k8xC	a
skoro	skoro	k6eAd1	skoro
všechen	všechen	k3xTgInSc1	všechen
volný	volný	k2eAgInSc1d1	volný
čas	čas	k1gInSc1	čas
věnoval	věnovat	k5eAaImAgInS	věnovat
vedení	vedení	k1gNnSc4	vedení
chlapeckého	chlapecký	k2eAgInSc2d1	chlapecký
skautského	skautský	k2eAgInSc2d1	skautský
oddílu	oddíl	k1gInSc2	oddíl
<g/>
.	.	kIx.	.
</s>
<s>
Zapletal	Zapletal	k1gMnSc1	Zapletal
uzavírá	uzavírat	k5eAaPmIp3nS	uzavírat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
smyslu	smysl	k1gInSc6	smysl
zcela	zcela	k6eAd1	zcela
určitě	určitě	k6eAd1	určitě
"	"	kIx"	"
<g/>
normální	normální	k2eAgFnSc4d1	normální
<g/>
"	"	kIx"	"
nebyl	být	k5eNaImAgInS	být
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
"	"	kIx"	"
<g/>
s	s	k7c7	s
jistotou	jistota	k1gFnSc7	jistota
prohlašuje	prohlašovat	k5eAaImIp3nS	prohlašovat
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc1	jeho
nesmírně	smírně	k6eNd1	smírně
silná	silný	k2eAgFnSc1d1	silná
motivace	motivace	k1gFnSc1	motivace
neměla	mít	k5eNaImAgFnS	mít
nic	nic	k3yNnSc1	nic
společného	společný	k2eAgNnSc2d1	společné
se	s	k7c7	s
sexuální	sexuální	k2eAgFnSc7d1	sexuální
orientací	orientace	k1gFnSc7	orientace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pečlivá	pečlivý	k2eAgFnSc1d1	pečlivá
analýza	analýza	k1gFnSc1	analýza
jeho	jeho	k3xOp3gNnPc2	jeho
děl	dělo	k1gNnPc2	dělo
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
především	především	k9	především
v	v	k7c6	v
počátcích	počátek	k1gInPc6	počátek
své	svůj	k3xOyFgFnSc2	svůj
tvorby	tvorba	k1gFnSc2	tvorba
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
v	v	k7c6	v
jejím	její	k3xOp3gInSc6	její
závěru	závěr	k1gInSc6	závěr
se	se	k3xPyFc4	se
kladné	kladný	k2eAgInPc1d1	kladný
postavy	postava	k1gFnPc4	postava
děvčat	děvče	k1gNnPc2	děvče
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
díle	díl	k1gInSc6	díl
objevovaly	objevovat	k5eAaImAgFnP	objevovat
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
sice	sice	k8xC	sice
jako	jako	k9	jako
postavy	postava	k1gFnPc4	postava
vedlejší	vedlejší	k2eAgFnPc4d1	vedlejší
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
ne	ne	k9	ne
vždy	vždy	k6eAd1	vždy
<g/>
.	.	kIx.	.
</s>
<s>
Jiřka	Jiřka	k1gFnSc1	Jiřka
<g/>
,	,	kIx,	,
Lenka	Lenka	k1gFnSc1	Lenka
a	a	k8xC	a
Martina	Martina	k1gFnSc1	Martina
z	z	k7c2	z
Historie	historie	k1gFnSc2	historie
Svorné	svorný	k2eAgFnSc2d1	svorná
sedmy	sedma	k1gFnSc2	sedma
a	a	k8xC	a
především	především	k9	především
Vlasta	Vlasta	k1gMnSc1	Vlasta
objevující	objevující	k2eAgMnSc1d1	objevující
se	se	k3xPyFc4	se
v	v	k7c6	v
několika	několik	k4yIc6	několik
příbězích	příběh	k1gInPc6	příběh
Rychlých	Rychlých	k2eAgInPc2d1	Rychlých
šípů	šíp	k1gInPc2	šíp
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
postavy	postava	k1gFnPc4	postava
dějově	dějově	k6eAd1	dějově
poměrně	poměrně	k6eAd1	poměrně
zásadní	zásadní	k2eAgInSc1d1	zásadní
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
jednoznačně	jednoznačně	k6eAd1	jednoznačně
heterosexuální	heterosexuální	k2eAgFnSc6d1	heterosexuální
orientaci	orientace	k1gFnSc6	orientace
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Foglara	Foglar	k1gMnSc2	Foglar
svědčí	svědčit	k5eAaImIp3nS	svědčit
také	také	k9	také
spisovatelovy	spisovatelův	k2eAgInPc4d1	spisovatelův
deníky	deník	k1gInPc4	deník
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
dochované	dochovaný	k2eAgFnPc4d1	dochovaná
archiválie	archiválie	k1gFnPc4	archiválie
<g/>
.	.	kIx.	.
<g/>
Dalším	další	k2eAgInSc7d1	další
dokladem	doklad	k1gInSc7	doklad
jeho	jeho	k3xOp3gFnSc2	jeho
heterosexuality	heterosexualita	k1gFnSc2	heterosexualita
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
Miloše	Miloš	k1gMnSc2	Miloš
Zapletala	Zapletal	k1gMnSc2	Zapletal
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
blízkosti	blízkost	k1gFnSc6	blízkost
během	během	k7c2	během
života	život	k1gInSc2	život
objevila	objevit	k5eAaPmAgFnS	objevit
řada	řada	k1gFnSc1	řada
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
o	o	k7c4	o
které	který	k3yQgFnPc4	který
buď	buď	k8xC	buď
projevil	projevit	k5eAaPmAgInS	projevit
zájem	zájem	k1gInSc1	zájem
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
do	do	k7c2	do
něj	on	k3xPp3gMnSc2	on
zamilovaly	zamilovat	k5eAaPmAgFnP	zamilovat
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
první	první	k4xOgFnSc7	první
velkou	velký	k2eAgFnSc7d1	velká
láskou	láska	k1gFnSc7	láska
byla	být	k5eAaImAgFnS	být
během	během	k7c2	během
studia	studio	k1gNnSc2	studio
na	na	k7c6	na
obchodní	obchodní	k2eAgFnSc6d1	obchodní
škole	škola	k1gFnSc6	škola
Božena	Božena	k1gFnSc1	Božena
Voldřichová	Voldřichová	k1gFnSc1	Voldřichová
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1927	[number]	k4	1927
byl	být	k5eAaImAgMnS	být
tak	tak	k6eAd1	tak
okouzlen	okouzlit	k5eAaPmNgMnS	okouzlit
Jiřinou	Jiřina	k1gFnSc7	Jiřina
Kalousovou	Kalousův	k2eAgFnSc7d1	Kalousova
<g/>
,	,	kIx,	,
že	že	k8xS	že
jí	on	k3xPp3gFnSc3	on
umožnil	umožnit	k5eAaPmAgInS	umožnit
účast	účast	k1gFnSc4	účast
na	na	k7c6	na
táborovém	táborový	k2eAgInSc6d1	táborový
programu	program	k1gInSc6	program
a	a	k8xC	a
věnoval	věnovat	k5eAaPmAgMnS	věnovat
jí	jíst	k5eAaImIp3nS	jíst
pak	pak	k6eAd1	pak
vlastnoruční	vlastnoruční	k2eAgInSc1d1	vlastnoruční
opis	opis	k1gInSc1	opis
táborové	táborový	k2eAgFnSc2d1	táborová
kroniky	kronika	k1gFnSc2	kronika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
si	se	k3xPyFc3	se
do	do	k7c2	do
deníku	deník	k1gInSc2	deník
napsal	napsat	k5eAaPmAgMnS	napsat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
ožení	oženit	k5eAaPmIp3nS	oženit
s	s	k7c7	s
M.	M.	kA	M.
M.	M.	kA	M.
<g/>
,	,	kIx,	,
o	o	k7c4	o
18	[number]	k4	18
let	léto	k1gNnPc2	léto
mladší	mladý	k2eAgFnSc1d2	mladší
spolupracovnicí	spolupracovnice	k1gFnSc7	spolupracovnice
z	z	k7c2	z
časopisu	časopis	k1gInSc2	časopis
Vpřed	vpřed	k6eAd1	vpřed
a	a	k8xC	a
úspěšnou	úspěšný	k2eAgFnSc7d1	úspěšná
skautskou	skautský	k2eAgFnSc7d1	skautská
vedoucí	vedoucí	k1gFnSc7	vedoucí
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
jej	on	k3xPp3gMnSc4	on
obdivovala	obdivovat	k5eAaImAgFnS	obdivovat
jako	jako	k8xC	jako
svůj	svůj	k3xOyFgInSc4	svůj
vzor	vzor	k1gInSc4	vzor
–	–	k?	–
vztah	vztah	k1gInSc1	vztah
trval	trvat	k5eAaImAgInS	trvat
rok	rok	k1gInSc4	rok
a	a	k8xC	a
půl	půl	k1xP	půl
<g/>
.	.	kIx.	.
</s>
<s>
Neboť	neboť	k8xC	neboť
vztah	vztah	k1gInSc1	vztah
k	k	k7c3	k
oddílu	oddíl	k1gInSc3	oddíl
byl	být	k5eAaImAgMnS	být
podle	podle	k7c2	podle
Zapletala	Zapletal	k1gMnSc2	Zapletal
Foglarovou	Foglarová	k1gFnSc4	Foglarová
největší	veliký	k2eAgFnSc7d3	veliký
a	a	k8xC	a
nejstálejší	stálý	k2eAgFnSc7d3	nejstálejší
láskou	láska	k1gFnSc7	láska
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
opakovaně	opakovaně	k6eAd1	opakovaně
litoval	litovat	k5eAaImAgMnS	litovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nezaložil	založit	k5eNaPmAgInS	založit
vlastní	vlastní	k2eAgFnSc4d1	vlastní
rodinu	rodina	k1gFnSc4	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Vynahrazoval	vynahrazovat	k5eAaImAgMnS	vynahrazovat
si	se	k3xPyFc3	se
to	ten	k3xDgNnSc1	ten
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
rodinu	rodina	k1gFnSc4	rodina
svého	svůj	k3xOyFgMnSc2	svůj
bratra	bratr	k1gMnSc2	bratr
a	a	k8xC	a
dlouhé	dlouhý	k2eAgFnPc4d1	dlouhá
hodiny	hodina	k1gFnPc4	hodina
si	se	k3xPyFc3	se
hrál	hrát	k5eAaImAgInS	hrát
s	s	k7c7	s
jeho	jeho	k3xOp3gFnPc7	jeho
dětmi	dítě	k1gFnPc7	dítě
<g/>
;	;	kIx,	;
synovec	synovec	k1gMnSc1	synovec
Petr	Petr	k1gMnSc1	Petr
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stal	stát	k5eAaPmAgInS	stát
i	i	k9	i
členem	člen	k1gMnSc7	člen
jeho	on	k3xPp3gInSc2	on
oddílu	oddíl	k1gInSc2	oddíl
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
přesvědčivý	přesvědčivý	k2eAgInSc4d1	přesvědčivý
důkaz	důkaz	k1gInSc4	důkaz
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
měl	mít	k5eAaImAgMnS	mít
k	k	k7c3	k
chlapcům	chlapec	k1gMnPc3	chlapec
vztah	vztah	k1gInSc4	vztah
výhradně	výhradně	k6eAd1	výhradně
výchovný	výchovný	k2eAgInSc4d1	výchovný
a	a	k8xC	a
pedagogický	pedagogický	k2eAgInSc4d1	pedagogický
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
sexuální	sexuální	k2eAgFnPc1d1	sexuální
<g/>
,	,	kIx,	,
považuje	považovat	k5eAaImIp3nS	považovat
Miloš	Miloš	k1gMnSc1	Miloš
Zapletal	Zapletal	k1gMnSc1	Zapletal
skutečnost	skutečnost	k1gFnSc4	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
ze	z	k7c2	z
stovek	stovka	k1gFnPc2	stovka
chlapců	chlapec	k1gMnPc2	chlapec
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
po	po	k7c6	po
několik	několik	k4yIc4	několik
desetiletí	desetiletí	k1gNnPc2	desetiletí
vedl	vést	k5eAaImAgInS	vést
<g/>
,	,	kIx,	,
nikdy	nikdy	k6eAd1	nikdy
nikdo	nikdo	k3yNnSc1	nikdo
nestěžoval	stěžovat	k5eNaImAgMnS	stěžovat
na	na	k7c4	na
nějaké	nějaký	k3yIgNnSc4	nějaký
obtěžování	obtěžování	k1gNnSc4	obtěžování
nebo	nebo	k8xC	nebo
zneužívání	zneužívání	k1gNnSc1	zneužívání
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
že	že	k8xS	že
nic	nic	k3yNnSc1	nic
takového	takový	k3xDgMnSc4	takový
proti	proti	k7c3	proti
němu	on	k3xPp3gNnSc3	on
nikdy	nikdy	k6eAd1	nikdy
nezneužili	zneužít	k5eNaPmAgMnP	zneužít
ani	ani	k8xC	ani
jeho	jeho	k3xOp3gMnPc1	jeho
osobní	osobní	k2eAgMnSc1d1	osobní
nepřátelé	nepřítel	k1gMnPc1	nepřítel
<g/>
,	,	kIx,	,
kterých	který	k3yRgMnPc2	který
měl	mít	k5eAaImAgMnS	mít
vždycky	vždycky	k6eAd1	vždycky
dost	dost	k6eAd1	dost
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
Státní	státní	k2eAgFnSc1d1	státní
bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
neodvážila	odvážit	k5eNaPmAgFnS	odvážit
nic	nic	k3yNnSc1	nic
podobného	podobný	k2eAgNnSc2d1	podobné
ani	ani	k8xC	ani
naznačit	naznačit	k5eAaPmF	naznačit
<g/>
.	.	kIx.	.
<g/>
Pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
jím	on	k3xPp3gInSc7	on
celoživotně	celoživotně	k6eAd1	celoživotně
propagovaný	propagovaný	k2eAgInSc1d1	propagovaný
kult	kult	k1gInSc1	kult
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
někteří	některý	k3yIgMnPc1	některý
jeho	jeho	k3xOp3gMnPc1	jeho
odpůrci	odpůrce	k1gMnPc1	odpůrce
v	v	k7c6	v
posledních	poslední	k2eAgInPc6d1	poslední
letech	let	k1gInPc6	let
rovněž	rovněž	k9	rovněž
označovali	označovat	k5eAaImAgMnP	označovat
jako	jako	k9	jako
úchylku	úchylka	k1gFnSc4	úchylka
<g/>
,	,	kIx,	,
Miloš	Miloš	k1gMnSc1	Miloš
Dvorský	Dvorský	k1gMnSc1	Dvorský
opakovaně	opakovaně	k6eAd1	opakovaně
zdůrazňuje	zdůrazňovat	k5eAaImIp3nS	zdůrazňovat
jeho	jeho	k3xOp3gInPc4	jeho
vzory	vzor	k1gInPc4	vzor
z	z	k7c2	z
dětství	dětství	k1gNnSc2	dětství
(	(	kIx(	(
<g/>
např.	např.	kA	např.
film	film	k1gInSc1	film
Zorro	Zorro	k1gNnSc1	Zorro
mstitel	mstitel	k1gMnSc1	mstitel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
vliv	vliv	k1gInSc1	vliv
organizace	organizace	k1gFnSc2	organizace
Wandervogel	Wandervogela	k1gFnPc2	Wandervogela
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Dvorského	Dvorský	k1gMnSc2	Dvorský
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
díle	dílo	k1gNnSc6	dílo
vidět	vidět	k5eAaImF	vidět
nic	nic	k3yNnSc1	nic
víc	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
než	než	k8xS	než
ideály	ideál	k1gInPc1	ideál
antické	antický	k2eAgFnSc2d1	antická
kalokagathie	kalokagathie	k1gFnSc2	kalokagathie
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
filozofie	filozofie	k1gFnSc1	filozofie
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20.	[number]	k4	20.
století	století	k1gNnPc2	století
popularizována	popularizovat	k5eAaImNgFnS	popularizovat
skrze	skrze	k?	skrze
organizace	organizace	k1gFnSc1	organizace
typu	typ	k1gInSc2	typ
Wandervogel	Wandervogela	k1gFnPc2	Wandervogela
<g/>
,	,	kIx,	,
Junák	junák	k1gMnSc1	junák
<g/>
,	,	kIx,	,
Sokol	Sokol	k1gMnSc1	Sokol
a	a	k8xC	a
jiné	jiný	k2eAgNnSc1d1	jiné
podobné	podobný	k2eAgInPc1d1	podobný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Charakter	charakter	k1gInSc1	charakter
tvorby	tvorba	k1gFnSc2	tvorba
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Motivy	motiv	k1gInPc1	motiv
===	===	k?	===
</s>
</p>
<p>
<s>
Typickým	typický	k2eAgMnSc7d1	typický
Foglarovým	Foglarův	k2eAgMnSc7d1	Foglarův
hrdinou	hrdina	k1gMnSc7	hrdina
je	být	k5eAaImIp3nS	být
chlapec	chlapec	k1gMnSc1	chlapec
z	z	k7c2	z
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
na	na	k7c6	na
prahu	práh	k1gInSc6	práh
dospívání	dospívání	k1gNnSc2	dospívání
<g/>
.	.	kIx.	.
</s>
<s>
Dívky	dívka	k1gFnPc1	dívka
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
jen	jen	k9	jen
okrajově	okrajově	k6eAd1	okrajově
a	a	k8xC	a
v	v	k7c6	v
podružné	podružný	k2eAgFnSc6d1	podružná
roli	role	k1gFnSc6	role
<g/>
,	,	kIx,	,
výjimkou	výjimka	k1gFnSc7	výjimka
je	být	k5eAaImIp3nS	být
Historie	historie	k1gFnSc1	historie
Svorné	svorný	k2eAgFnSc2d1	svorná
sedmy	sedma	k1gFnSc2	sedma
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
ústřední	ústřední	k2eAgInSc1d1	ústřední
klub	klub	k1gInSc1	klub
složen	složit	k5eAaPmNgInS	složit
z	z	k7c2	z
hochů	hoch	k1gMnPc2	hoch
i	i	k8xC	i
děvčat	děvče	k1gNnPc2	děvče
<g/>
.	.	kIx.	.
</s>
<s>
Milostné	milostný	k2eAgInPc1d1	milostný
nebo	nebo	k8xC	nebo
sexuální	sexuální	k2eAgInPc1d1	sexuální
vztahy	vztah	k1gInPc1	vztah
se	se	k3xPyFc4	se
ve	v	k7c6	v
Foglarových	Foglarův	k2eAgInPc6d1	Foglarův
příbězích	příběh	k1gInPc6	příběh
každopádně	každopádně	k6eAd1	každopádně
nenacházejí	nacházet	k5eNaImIp3nP	nacházet
<g/>
.	.	kIx.	.
</s>
<s>
Dospělí	dospělí	k1gMnPc1	dospělí
se	se	k3xPyFc4	se
také	také	k9	také
objevují	objevovat	k5eAaImIp3nP	objevovat
jen	jen	k9	jen
okrajově	okrajově	k6eAd1	okrajově
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
jako	jako	k9	jako
rodiče	rodič	k1gMnPc1	rodič
<g/>
,	,	kIx,	,
učitelé	učitel	k1gMnPc1	učitel
nebo	nebo	k8xC	nebo
bezejmenné	bezejmenný	k2eAgFnPc1d1	bezejmenná
postavy	postava	k1gFnPc1	postava
ze	z	k7c2	z
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
tomu	ten	k3xDgNnSc3	ten
chlapeckému	chlapecký	k2eAgNnSc3d1	chlapecké
nerozumí	rozumět	k5eNaImIp3nS	rozumět
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
jsou	být	k5eAaImIp3nP	být
mladí	mladý	k2eAgMnPc1d1	mladý
skautští	skautský	k2eAgMnPc1d1	skautský
vedoucí	vedoucí	k1gMnPc1	vedoucí
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
osoba	osoba	k1gFnSc1	osoba
jejich	jejich	k3xOp3gInSc2	jejich
typu	typ	k1gInSc2	typ
(	(	kIx(	(
<g/>
Rikitan	Rikitan	k1gInSc1	Rikitan
<g/>
;	;	kIx,	;
učitel	učitel	k1gMnSc1	učitel
Kovář	Kovář	k1gMnSc1	Kovář
z	z	k7c2	z
Když	když	k8xS	když
Duben	duben	k1gInSc1	duben
Přichází	přicházet	k5eAaImIp3nS	přicházet
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlavním	hlavní	k2eAgMnSc7d1	hlavní
hrdinou	hrdina	k1gMnSc7	hrdina
románu	román	k1gInSc2	román
bývá	bývat	k5eAaImIp3nS	bývat
jednotlivec	jednotlivec	k1gMnSc1	jednotlivec
nebo	nebo	k8xC	nebo
dvojice	dvojice	k1gFnSc1	dvojice
či	či	k8xC	či
větší	veliký	k2eAgFnSc1d2	veliký
skupina	skupina	k1gFnSc1	skupina
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
ale	ale	k8xC	ale
vždy	vždy	k6eAd1	vždy
existuje	existovat	k5eAaImIp3nS	existovat
jedna	jeden	k4xCgFnSc1	jeden
postava	postava	k1gFnSc1	postava
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInPc1	jejíž
osudy	osud	k1gInPc1	osud
a	a	k8xC	a
myšlenkové	myšlenkový	k2eAgInPc1d1	myšlenkový
pochody	pochod	k1gInPc1	pochod
především	především	k6eAd1	především
sledujeme	sledovat	k5eAaImIp1nP	sledovat
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
v	v	k7c6	v
Pokladu	poklad	k1gInSc6	poklad
Černého	Černý	k1gMnSc2	Černý
delfína	delfín	k1gMnSc2	delfín
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
Standa	Standa	k1gMnSc1	Standa
Melichar	Melichar	k1gMnSc1	Melichar
z	z	k7c2	z
dvojice	dvojice	k1gFnSc2	dvojice
s	s	k7c7	s
Lojzkem	Lojzek	k1gMnSc7	Lojzek
Noskem	Nosek	k1gMnSc7	Nosek
<g/>
,	,	kIx,	,
v	v	k7c6	v
Chatě	chata	k1gFnSc6	chata
v	v	k7c6	v
Jezerní	jezerní	k2eAgFnSc6d1	jezerní
kotlině	kotlina	k1gFnSc6	kotlina
Pavel	Pavel	k1gMnSc1	Pavel
Zeman	Zeman	k1gMnSc1	Zeman
z	z	k7c2	z
dvojice	dvojice	k1gFnSc2	dvojice
s	s	k7c7	s
Ludvou	Ludva	k1gMnSc7	Ludva
Grygarem	Grygar	k1gMnSc7	Grygar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
řadě	řada	k1gFnSc6	řada
románů	román	k1gInPc2	román
je	být	k5eAaImIp3nS	být
hrdinou	hrdina	k1gMnSc7	hrdina
citlivý	citlivý	k2eAgMnSc1d1	citlivý
a	a	k8xC	a
čestný	čestný	k2eAgMnSc1d1	čestný
chlapec	chlapec	k1gMnSc1	chlapec
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
trpí	trpět	k5eAaImIp3nS	trpět
nepochopením	nepochopení	k1gNnSc7	nepochopení
okolí	okolí	k1gNnSc2	okolí
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
rodičů	rodič	k1gMnPc2	rodič
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
při	při	k7c6	při
hledání	hledání	k1gNnSc6	hledání
dobrodružství	dobrodružství	k1gNnSc2	dobrodružství
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
zaplete	zaplést	k5eAaPmIp3nS	zaplést
s	s	k7c7	s
pochybnými	pochybný	k2eAgInPc7d1	pochybný
kumpány	kumpány	k?	kumpány
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
nachází	nacházet	k5eAaImIp3nS	nacházet
naplnění	naplnění	k1gNnSc4	naplnění
svých	svůj	k3xOyFgFnPc2	svůj
potřeb	potřeba	k1gFnPc2	potřeba
v	v	k7c6	v
ušlechtilém	ušlechtilý	k2eAgNnSc6d1	ušlechtilé
přátelství	přátelství	k1gNnSc6	přátelství
s	s	k7c7	s
vrstevníkem	vrstevník	k1gMnSc7	vrstevník
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
skautském	skautský	k2eAgInSc6d1	skautský
oddíle	oddíl	k1gInSc6	oddíl
či	či	k8xC	či
podobném	podobný	k2eAgInSc6d1	podobný
spolku	spolek	k1gInSc6	spolek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Opakovaným	opakovaný	k2eAgInSc7d1	opakovaný
motivem	motiv	k1gInSc7	motiv
je	být	k5eAaImIp3nS	být
liduprázdná	liduprázdný	k2eAgFnSc1d1	liduprázdná
divoká	divoký	k2eAgFnSc1d1	divoká
krajina	krajina	k1gFnSc1	krajina
"	"	kIx"	"
<g/>
za	za	k7c7	za
městem	město	k1gNnSc7	město
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
je	být	k5eAaImIp3nS	být
obtížný	obtížný	k2eAgInSc4d1	obtížný
přístup	přístup	k1gInSc4	přístup
a	a	k8xC	a
kde	kde	k6eAd1	kde
chlapečtí	chlapecký	k2eAgMnPc1d1	chlapecký
hrdinové	hrdina	k1gMnPc1	hrdina
svobodně	svobodně	k6eAd1	svobodně
tráví	trávit	k5eAaImIp3nP	trávit
čas	čas	k1gInSc4	čas
a	a	k8xC	a
zažívají	zažívat	k5eAaImIp3nP	zažívat
dobrodružné	dobrodružný	k2eAgFnPc4d1	dobrodružná
příhody	příhoda	k1gFnPc4	příhoda
(	(	kIx(	(
<g/>
Chata	chata	k1gFnSc1	chata
v	v	k7c6	v
Jezerní	jezerní	k2eAgFnSc6d1	jezerní
kotlině	kotlina	k1gFnSc6	kotlina
<g/>
,	,	kIx,	,
Dobrodružství	dobrodružství	k1gNnSc1	dobrodružství
v	v	k7c6	v
Zemi	zem	k1gFnSc6	zem
nikoho	nikdo	k3yNnSc4	nikdo
<g/>
,	,	kIx,	,
Modrá	modrý	k2eAgFnSc1d1	modrá
rokle	rokle	k1gFnSc1	rokle
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
podobných	podobný	k2eAgFnPc6d1	podobná
lokalitách	lokalita	k1gFnPc6	lokalita
vrcholí	vrcholit	k5eAaImIp3nP	vrcholit
i	i	k9	i
romány	román	k1gInPc1	román
ze	z	k7c2	z
skautského	skautský	k2eAgNnSc2d1	skautské
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Foglar	Foglar	k1gInSc1	Foglar
v	v	k7c6	v
příbězích	příběh	k1gInPc6	příběh
nešetří	šetřit	k5eNaImIp3nS	šetřit
tragédiemi	tragédie	k1gFnPc7	tragédie
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
vážná	vážný	k2eAgNnPc4d1	vážné
zranění	zranění	k1gNnPc4	zranění
<g/>
,	,	kIx,	,
onemocnění	onemocnění	k1gNnPc4	onemocnění
nebo	nebo	k8xC	nebo
i	i	k9	i
smrt	smrt	k1gFnSc1	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Zpravidla	zpravidla	k6eAd1	zpravidla
však	však	k9	však
nepostihují	postihovat	k5eNaImIp3nP	postihovat
hlavní	hlavní	k2eAgFnPc4d1	hlavní
hrdiny	hrdina	k1gMnPc7	hrdina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gNnSc4	jejich
okolí	okolí	k1gNnSc4	okolí
<g/>
,	,	kIx,	,
a	a	k8xC	a
mívají	mívat	k5eAaImIp3nP	mívat
výchovný	výchovný	k2eAgInSc4d1	výchovný
podtext	podtext	k1gInSc4	podtext
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jsou	být	k5eAaImIp3nP	být
způsobeny	způsobit	k5eAaPmNgInP	způsobit
nezodpovědným	zodpovědný	k2eNgNnSc7d1	nezodpovědné
či	či	k8xC	či
nebezpečným	bezpečný	k2eNgNnSc7d1	nebezpečné
chováním	chování	k1gNnSc7	chování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Autorský	autorský	k2eAgInSc4d1	autorský
styl	styl	k1gInSc4	styl
===	===	k?	===
</s>
</p>
<p>
<s>
Foglarův	Foglarův	k2eAgInSc4d1	Foglarův
literární	literární	k2eAgInSc4d1	literární
styl	styl	k1gInSc4	styl
má	mít	k5eAaImIp3nS	mít
několik	několik	k4yIc4	několik
typických	typický	k2eAgInPc2d1	typický
rysů	rys	k1gInPc2	rys
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
časté	častý	k2eAgNnSc1d1	časté
používání	používání	k1gNnSc1	používání
nezvyklých	zvyklý	k2eNgNnPc2d1	nezvyklé
<g/>
,	,	kIx,	,
až	až	k9	až
bizarních	bizarní	k2eAgNnPc2d1	bizarní
jmen	jméno	k1gNnPc2	jméno
vedlejších	vedlejší	k2eAgFnPc2d1	vedlejší
postav	postava	k1gFnPc2	postava
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnSc4	jejich
příjmení	příjmení	k1gNnSc4	příjmení
mnohdy	mnohdy	k6eAd1	mnohdy
nemají	mít	k5eNaImIp3nP	mít
žádný	žádný	k3yNgInSc4	žádný
reálný	reálný	k2eAgInSc4d1	reálný
předobraz	předobraz	k1gInSc4	předobraz
(	(	kIx(	(
<g/>
Dabinel	Dabinel	k1gMnSc1	Dabinel
<g/>
,	,	kIx,	,
Hačiřík	Hačiřík	k1gMnSc1	Hačiřík
<g/>
,	,	kIx,	,
Komour	Komour	k1gMnSc1	Komour
<g/>
,	,	kIx,	,
Losna	Losna	k1gFnSc1	Losna
<g/>
,	,	kIx,	,
Mažňák	Mažňák	k1gMnSc1	Mažňák
<g/>
,	,	kIx,	,
Macíř	Macíř	k1gMnSc1	Macíř
<g/>
,	,	kIx,	,
Mencíř	Mencíř	k1gMnSc1	Mencíř
<g/>
,	,	kIx,	,
Murkač	Murkač	k1gMnSc1	Murkač
<g/>
,	,	kIx,	,
Široko-Širokko	Široko-Širokka	k1gFnSc5	Široko-Širokka
<g/>
,	,	kIx,	,
Šprundibour	Šprundibour	k1gMnSc1	Šprundibour
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vymyšlené	vymyšlený	k2eAgNnSc1d1	vymyšlené
je	být	k5eAaImIp3nS	být
ostatně	ostatně	k6eAd1	ostatně
i	i	k9	i
příjmení	příjmení	k1gNnSc4	příjmení
Tleskač	tleskač	k1gMnSc1	tleskač
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
přítomen	přítomen	k2eAgInSc4d1	přítomen
svérázný	svérázný	k2eAgInSc4d1	svérázný
humor	humor	k1gInSc4	humor
<g/>
,	,	kIx,	,
dialogický	dialogický	k2eAgInSc4d1	dialogický
nebo	nebo	k8xC	nebo
situační	situační	k2eAgInSc4d1	situační
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
jeho	jeho	k3xOp3gNnPc1	jeho
díla	dílo	k1gNnPc1	dílo
jsou	být	k5eAaImIp3nP	být
vysloveně	vysloveně	k6eAd1	vysloveně
humoristická	humoristický	k2eAgNnPc1d1	humoristické
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
Tábor	Tábor	k1gInSc1	Tábor
smůly	smůla	k1gFnSc2	smůla
<g/>
,	,	kIx,	,
Nováček	Nováček	k1gMnSc1	Nováček
Bubáček	bubáček	k1gMnSc1	bubáček
píše	psát	k5eAaImIp3nS	psát
deník	deník	k1gInSc1	deník
nebo	nebo	k8xC	nebo
řada	řada	k1gFnSc1	řada
dílů	díl	k1gInPc2	díl
komiksu	komiks	k1gInSc2	komiks
o	o	k7c6	o
Rychlých	Rychlých	k2eAgInPc6d1	Rychlých
šípech	šíp	k1gInPc6	šíp
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výchovná	výchovný	k2eAgFnSc1d1	výchovná
a	a	k8xC	a
poučná	poučný	k2eAgFnSc1d1	poučná
stránka	stránka	k1gFnSc1	stránka
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
Foglara	Foglar	k1gMnSc2	Foglar
někdy	někdy	k6eAd1	někdy
tak	tak	k6eAd1	tak
zdůrazněna	zdůraznit	k5eAaPmNgFnS	zdůraznit
<g/>
,	,	kIx,	,
že	že	k8xS	že
některé	některý	k3yIgFnPc1	některý
pasáže	pasáž	k1gFnPc1	pasáž
mají	mít	k5eAaImIp3nP	mít
až	až	k9	až
charakter	charakter	k1gInSc4	charakter
ilustrované	ilustrovaný	k2eAgFnSc2d1	ilustrovaná
příručky	příručka	k1gFnSc2	příručka
(	(	kIx(	(
<g/>
např.	např.	kA	např.
román	román	k1gInSc4	román
Hoši	hoch	k1gMnPc1	hoch
od	od	k7c2	od
Bobří	bobří	k2eAgFnSc2d1	bobří
řeky	řeka	k1gFnSc2	řeka
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
přesné	přesný	k2eAgInPc4d1	přesný
návody	návod	k1gInPc4	návod
jak	jak	k8xC	jak
lovit	lovit	k5eAaImF	lovit
bobříky	bobřík	k1gMnPc4	bobřík
<g/>
,	,	kIx,	,
jinde	jinde	k6eAd1	jinde
jsou	být	k5eAaImIp3nP	být
podrobně	podrobně	k6eAd1	podrobně
popsána	popsán	k2eAgNnPc1d1	popsáno
pravidla	pravidlo	k1gNnPc1	pravidlo
her	hra	k1gFnPc2	hra
vhodných	vhodný	k2eAgFnPc2d1	vhodná
pro	pro	k7c4	pro
tábor	tábor	k1gInSc4	tábor
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
díly	díl	k1gInPc1	díl
Rychlých	Rychlých	k2eAgInPc2d1	Rychlých
šípů	šíp	k1gInPc2	šíp
učí	učit	k5eAaImIp3nS	učit
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
skákat	skákat	k5eAaImF	skákat
do	do	k7c2	do
záchranné	záchranný	k2eAgFnSc2d1	záchranná
plachty	plachta	k1gFnSc2	plachta
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
ze	z	k7c2	z
čtyř	čtyři	k4xCgFnPc2	čtyři
rukou	ruka	k1gFnPc2	ruka
vyrobit	vyrobit	k5eAaPmF	vyrobit
nosítka	nosítko	k1gNnSc2	nosítko
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
mezi	mezi	k7c7	mezi
kapitoly	kapitola	k1gFnSc2	kapitola
vložen	vložit	k5eAaPmNgInS	vložit
přímo	přímo	k6eAd1	přímo
seznam	seznam	k1gInSc1	seznam
zásad	zásada	k1gFnPc2	zásada
správného	správný	k2eAgNnSc2d1	správné
chování	chování	k1gNnSc2	chování
<g/>
.	.	kIx.	.
</s>
<s>
Nebývá	bývat	k5eNaImIp3nS	bývat
to	ten	k3xDgNnSc1	ten
ale	ale	k9	ale
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
čtivosti	čtivost	k1gFnSc2	čtivost
celku	celek	k1gInSc2	celek
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgInPc1d1	mnohý
romány	román	k1gInPc1	román
nejsou	být	k5eNaImIp3nP	být
prosty	prost	k2eAgInPc1d1	prost
upřímného	upřímný	k2eAgInSc2d1	upřímný
patosu	patos	k1gInSc2	patos
<g/>
,	,	kIx,	,
opěvujícího	opěvující	k2eAgInSc2d1	opěvující
zpravidla	zpravidla	k6eAd1	zpravidla
krásy	krása	k1gFnPc4	krása
nevinného	vinný	k2eNgNnSc2d1	nevinné
chlapectví	chlapectví	k1gNnSc2	chlapectví
<g/>
,	,	kIx,	,
přátelství	přátelství	k1gNnSc2	přátelství
<g/>
,	,	kIx,	,
jara	jaro	k1gNnSc2	jaro
a	a	k8xC	a
přírody	příroda	k1gFnSc2	příroda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Foglar	Foglar	k1gInSc1	Foglar
používá	používat	k5eAaImIp3nS	používat
poutavý	poutavý	k2eAgInSc1d1	poutavý
jazyk	jazyk	k1gInSc1	jazyk
blízký	blízký	k2eAgInSc1d1	blízký
duši	duše	k1gFnSc4	duše
mladých	mladý	k2eAgMnPc2d1	mladý
hochů	hoch	k1gMnPc2	hoch
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
dívek	dívka	k1gFnPc2	dívka
<g/>
.	.	kIx.	.
</s>
<s>
Mluva	mluva	k1gFnSc1	mluva
jeho	jeho	k3xOp3gMnPc2	jeho
chlapeckých	chlapecký	k2eAgMnPc2d1	chlapecký
hrdinů	hrdina	k1gMnPc2	hrdina
ve	v	k7c6	v
starších	starý	k2eAgNnPc6d2	starší
dílech	dílo	k1gNnPc6	dílo
působí	působit	k5eAaImIp3nS	působit
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
poněkud	poněkud	k6eAd1	poněkud
archaicky	archaicky	k6eAd1	archaicky
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
víceméně	víceméně	k9	víceméně
koresponduje	korespondovat	k5eAaImIp3nS	korespondovat
s	s	k7c7	s
celkovou	celkový	k2eAgFnSc7d1	celková
atmosférou	atmosféra	k1gFnSc7	atmosféra
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
<s>
Posun	posun	k1gInSc4	posun
k	k	k7c3	k
současnějšímu	současný	k2eAgInSc3d2	současnější
jazyku	jazyk	k1gInSc3	jazyk
je	být	k5eAaImIp3nS	být
však	však	k9	však
patrný	patrný	k2eAgMnSc1d1	patrný
například	například	k6eAd1	například
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
posledním	poslední	k2eAgInSc6d1	poslední
románu	román	k1gInSc6	román
Modrá	modrý	k2eAgFnSc1d1	modrá
rokle	rokle	k1gFnSc1	rokle
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mají	mít	k5eAaImIp3nP	mít
také	také	k9	také
některé	některý	k3yIgFnPc1	některý
postavy	postava	k1gFnPc1	postava
moderně	moderně	k6eAd1	moderně
znějící	znějící	k2eAgNnPc1d1	znějící
jména	jméno	k1gNnPc1	jméno
(	(	kIx(	(
<g/>
Denny	Denn	k1gInPc1	Denn
<g/>
,	,	kIx,	,
Altar	Altar	k1gInSc1	Altar
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výrazný	výrazný	k2eAgInSc1d1	výrazný
posun	posun	k1gInSc1	posun
v	v	k7c6	v
přizpůsobení	přizpůsobení	k1gNnSc6	přizpůsobení
Foglarova	Foglarův	k2eAgInSc2d1	Foglarův
původního	původní	k2eAgInSc2d1	původní
jazyka	jazyk	k1gInSc2	jazyk
moderní	moderní	k2eAgFnSc3d1	moderní
češtině	čeština	k1gFnSc3	čeština
je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
zřejmý	zřejmý	k2eAgInSc1d1	zřejmý
teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
nových	nový	k2eAgNnPc6d1	nové
vydáních	vydání	k1gNnPc6	vydání
Foglarových	Foglarová	k1gFnPc2	Foglarová
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
edice	edice	k1gFnSc1	edice
Sebraných	sebraný	k2eAgInPc2d1	sebraný
spisů	spis	k1gInPc2	spis
J.	J.	kA	J.
F.	F.	kA	F.
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
vycházely	vycházet	k5eAaImAgFnP	vycházet
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
v	v	k7c6	v
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
Olympia	Olympia	k1gFnSc1	Olympia
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
s	s	k7c7	s
pochopením	pochopení	k1gNnSc7	pochopení
akceptoval	akceptovat	k5eAaBmAgInS	akceptovat
redakční	redakční	k2eAgFnPc4d1	redakční
a	a	k8xC	a
jazykové	jazykový	k2eAgFnPc4d1	jazyková
úpravy	úprava	k1gFnPc4	úprava
svých	svůj	k3xOyFgNnPc2	svůj
děl	dělo	k1gNnPc2	dělo
a	a	k8xC	a
bez	bez	k7c2	bez
výhrady	výhrada	k1gFnSc2	výhrada
je	on	k3xPp3gInPc4	on
přijal	přijmout	k5eAaPmAgMnS	přijmout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Všechna	všechen	k3xTgNnPc1	všechen
Foglarova	Foglarův	k2eAgNnPc1d1	Foglarovo
díla	dílo	k1gNnPc1	dílo
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
výpravných	výpravný	k2eAgNnPc2d1	výpravné
samostatných	samostatný	k2eAgNnPc2d1	samostatné
vydání	vydání	k1gNnPc2	vydání
komiksových	komiksový	k2eAgInPc2d1	komiksový
Rychlých	Rychlých	k2eAgInPc2d1	Rychlých
šípů	šíp	k1gInPc2	šíp
a	a	k8xC	a
Svorných	svorný	k2eAgInPc2d1	svorný
gambusínů	gambusín	k1gInPc2	gambusín
a	a	k8xC	a
též	tenž	k3xDgFnSc2	tenž
trilogie	trilogie	k1gFnSc2	trilogie
Rychlé	Rychlé	k2eAgInPc1d1	Rychlé
šípy	šíp	k1gInPc1	šíp
ve	v	k7c6	v
Stínadlech	stínadlo	k1gNnPc6	stínadlo
<g/>
)	)	kIx)	)
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
knižní	knižní	k2eAgFnSc6d1	knižní
edici	edice	k1gFnSc6	edice
nakladatelství	nakladatelství	k1gNnPc2	nakladatelství
Olympia	Olympia	k1gFnSc1	Olympia
Sebrané	sebraný	k2eAgInPc4d1	sebraný
spisy	spis	k1gInPc4	spis
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Foglara	Foglar	k1gMnSc2	Foglar
<g/>
.	.	kIx.	.
</s>
<s>
Detailní	detailní	k2eAgInSc1d1	detailní
přehled	přehled	k1gInSc1	přehled
vydaných	vydaný	k2eAgNnPc2d1	vydané
i	i	k8xC	i
nevydaných	vydaný	k2eNgNnPc2d1	nevydané
děl	dělo	k1gNnPc2	dělo
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Foglara	Foglar	k1gMnSc2	Foglar
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
na	na	k7c6	na
badatelském	badatelský	k2eAgInSc6d1	badatelský
webu	web	k1gInSc6	web
Vontové	Vontová	k1gFnSc2	Vontová
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
<g/>
.	.	kIx.	.
<g/>
Majitel	majitel	k1gMnSc1	majitel
knižních	knižní	k2eAgNnPc2d1	knižní
práv	právo	k1gNnPc2	právo
–	–	k?	–
Skautská	skautský	k2eAgFnSc1d1	skautská
nadace	nadace	k1gFnSc1	nadace
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Foglara	Foglar	k1gMnSc2	Foglar
–	–	k?	–
se	se	k3xPyFc4	se
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2018	[number]	k4	2018
dohodla	dohodnout	k5eAaPmAgFnS	dohodnout
s	s	k7c7	s
nakladatelstvím	nakladatelství	k1gNnSc7	nakladatelství
Albatros	albatros	k1gMnSc1	albatros
<g/>
,	,	kIx,	,
že	že	k8xS	že
vydá	vydat	k5eAaPmIp3nS	vydat
dílo	dílo	k1gNnSc1	dílo
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Foglara	Foglar	k1gMnSc2	Foglar
v	v	k7c6	v
novém	nový	k2eAgNnSc6d1	nové
vydání	vydání	k1gNnSc6	vydání
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
první	první	k4xOgFnPc1	první
přijdou	přijít	k5eAaPmIp3nP	přijít
na	na	k7c4	na
řadu	řada	k1gFnSc4	řada
v	v	k7c4	v
září	září	k1gNnSc4	září
2018	[number]	k4	2018
knihy	kniha	k1gFnSc2	kniha
Hoši	hoch	k1gMnPc1	hoch
od	od	k7c2	od
Bobří	bobří	k2eAgFnSc2d1	bobří
řeky	řeka	k1gFnSc2	řeka
a	a	k8xC	a
Chata	chata	k1gFnSc1	chata
v	v	k7c6	v
Jezerní	jezerní	k2eAgFnSc6d1	jezerní
kotlině	kotlina	k1gFnSc6	kotlina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Knihy	kniha	k1gFnSc2	kniha
===	===	k?	===
</s>
</p>
<p>
<s>
Přístav	přístav	k1gInSc1	přístav
volá	volat	k5eAaImIp3nS	volat
(	(	kIx(	(
<g/>
pův	pův	k?	pův
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc1	vydání
1934	[number]	k4	1934
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
o	o	k7c4	o
přátelství	přátelství	k1gNnSc4	přátelství
Jirky	Jirka	k1gMnSc2	Jirka
Dražana	Dražan	k1gMnSc2	Dražan
a	a	k8xC	a
Ládi	Láďa	k1gMnSc2	Láďa
Vilemína	Vilemína	k1gFnSc1	Vilemína
<g/>
,	,	kIx,	,
román	román	k1gInSc1	román
seznamuje	seznamovat	k5eAaImIp3nS	seznamovat
s	s	k7c7	s
Modrým	modrý	k2eAgInSc7d1	modrý
životem	život	k1gInSc7	život
</s>
</p>
<p>
<s>
Boj	boj	k1gInSc1	boj
o	o	k7c4	o
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
(	(	kIx(	(
<g/>
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
o	o	k7c6	o
cestě	cesta	k1gFnSc6	cesta
Petra	Petr	k1gMnSc2	Petr
Solnara	Solnar	k1gMnSc2	Solnar
k	k	k7c3	k
naplnění	naplnění	k1gNnSc3	naplnění
svých	svůj	k3xOyFgFnPc2	svůj
tužeb	tužba	k1gFnPc2	tužba
ve	v	k7c6	v
skautském	skautský	k2eAgInSc6d1	skautský
oddíle	oddíl	k1gInSc6	oddíl
</s>
</p>
<p>
<s>
Hoši	hoch	k1gMnPc1	hoch
od	od	k7c2	od
Bobří	bobří	k2eAgFnSc2d1	bobří
řeky	řeka	k1gFnSc2	řeka
(	(	kIx(	(
<g/>
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
klasické	klasický	k2eAgNnSc4d1	klasické
dílo	dílo	k1gNnSc4	dílo
o	o	k7c6	o
skupině	skupina	k1gFnSc6	skupina
12	[number]	k4	12
chlapců	chlapec	k1gMnPc2	chlapec
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
vede	vést	k5eAaImIp3nS	vést
mladý	mladý	k2eAgMnSc1d1	mladý
muž	muž	k1gMnSc1	muž
Rikitan	Rikitan	k1gInSc4	Rikitan
k	k	k7c3	k
ušlechtilému	ušlechtilý	k2eAgInSc3d1	ušlechtilý
životu	život	k1gInSc3	život
podle	podle	k7c2	podle
skautských	skautský	k2eAgFnPc2d1	skautská
zásad	zásada	k1gFnPc2	zásada
<g/>
,	,	kIx,	,
seznamuje	seznamovat	k5eAaImIp3nS	seznamovat
s	s	k7c7	s
lovem	lov	k1gInSc7	lov
bobříků	bobřík	k1gMnPc2	bobřík
</s>
</p>
<p>
<s>
Tábor	Tábor	k1gInSc1	Tábor
smůly	smůla	k1gFnSc2	smůla
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
humoristická	humoristický	k2eAgFnSc1d1	humoristická
sbírka	sbírka	k1gFnSc1	sbírka
zážitků	zážitek	k1gInPc2	zážitek
z	z	k7c2	z
Foglarova	Foglarův	k2eAgNnSc2d1	Foglarovo
dětství	dětství	k1gNnSc2	dětství
a	a	k8xC	a
vedení	vedení	k1gNnSc2	vedení
skautských	skautský	k2eAgInPc2d1	skautský
táborů	tábor	k1gInPc2	tábor
</s>
</p>
<p>
<s>
Chata	chata	k1gFnSc1	chata
v	v	k7c6	v
Jezerní	jezerní	k2eAgFnSc6d1	jezerní
kotlině	kotlina	k1gFnSc6	kotlina
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
o	o	k7c4	o
přátelství	přátelství	k1gNnSc4	přátelství
Pavla	Pavel	k1gMnSc2	Pavel
Zemana	Zeman	k1gMnSc2	Zeman
a	a	k8xC	a
Ludvy	Ludva	k1gMnSc2	Ludva
Grygara	Grygar	k1gMnSc2	Grygar
<g/>
,	,	kIx,	,
román	román	k1gInSc1	román
silně	silně	k6eAd1	silně
akcentuje	akcentovat	k5eAaImIp3nS	akcentovat
škodlivost	škodlivost	k1gFnSc4	škodlivost
kouření	kouření	k1gNnSc3	kouření
</s>
</p>
<p>
<s>
Historie	historie	k1gFnSc1	historie
Svorné	svorný	k2eAgFnSc2d1	svorná
sedmy	sedma	k1gFnSc2	sedma
(	(	kIx(	(
<g/>
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
o	o	k7c6	o
partě	parta	k1gFnSc6	parta
dětí	dítě	k1gFnPc2	dítě
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
činžáku	činžák	k1gInSc2	činžák
a	a	k8xC	a
jejích	její	k3xOp3gFnPc6	její
příhodách	příhoda	k1gFnPc6	příhoda
</s>
</p>
<p>
<s>
Pod	pod	k7c7	pod
junáckou	junácký	k2eAgFnSc7d1	Junácká
vlajkou	vlajka	k1gFnSc7	vlajka
(	(	kIx(	(
<g/>
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
příběh	příběh	k1gInSc4	příběh
Mirka	Mirek	k1gMnSc2	Mirek
Trojana	Trojan	k1gMnSc2	Trojan
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
za	za	k7c7	za
nepoctivým	poctivý	k2eNgInSc7d1	nepoctivý
účelem	účel	k1gInSc7	účel
vstoupí	vstoupit	k5eAaPmIp3nP	vstoupit
do	do	k7c2	do
skautského	skautský	k2eAgInSc2d1	skautský
oddílu	oddíl	k1gInSc2	oddíl
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc1	jehož
prostředí	prostředí	k1gNnSc1	prostředí
ho	on	k3xPp3gMnSc4	on
napraví	napravit	k5eAaPmIp3nS	napravit
</s>
</p>
<p>
<s>
Záhada	záhada	k1gFnSc1	záhada
hlavolamu	hlavolam	k1gInSc2	hlavolam
(	(	kIx(	(
<g/>
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejslavnějších	slavný	k2eAgInPc2d3	nejslavnější
románů	román	k1gInPc2	román
<g/>
,	,	kIx,	,
o	o	k7c4	o
dobrodružství	dobrodružství	k1gNnSc4	dobrodružství
Rychlých	Rychlých	k2eAgInPc2d1	Rychlých
šípů	šíp	k1gInPc2	šíp
ve	v	k7c6	v
Stínadlech	stínadlo	k1gNnPc6	stínadlo
a	a	k8xC	a
hledání	hledání	k1gNnSc1	hledání
ježka	ježek	k1gMnSc2	ježek
v	v	k7c6	v
kleci	klec	k1gFnSc6	klec
<g/>
,	,	kIx,	,
zpracováno	zpracován	k2eAgNnSc1d1	zpracováno
i	i	k8xC	i
jako	jako	k9	jako
televizní	televizní	k2eAgInSc4d1	televizní
seriál	seriál	k1gInSc4	seriál
a	a	k8xC	a
celovečerní	celovečerní	k2eAgInSc4d1	celovečerní
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
první	první	k4xOgInSc4	první
díl	díl	k1gInSc4	díl
tzv.	tzv.	kA	tzv.
Stínadelské	Stínadelský	k2eAgFnSc2d1	Stínadelská
trilogie	trilogie	k1gFnSc2	trilogie
</s>
</p>
<p>
<s>
Když	když	k8xS	když
Duben	duben	k1gInSc1	duben
přichází	přicházet	k5eAaImIp3nS	přicházet
(	(	kIx(	(
<g/>
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
o	o	k7c6	o
několika	několik	k4yIc6	několik
spolužácích	spolužák	k1gMnPc6	spolužák
ze	z	k7c2	z
znesvářené	znesvářený	k2eAgFnSc2d1	znesvářená
školní	školní	k2eAgFnSc2d1	školní
třídy	třída	k1gFnSc2	třída
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
k	k	k7c3	k
sobě	se	k3xPyFc3	se
hledají	hledat	k5eAaImIp3nP	hledat
cestu	cesta	k1gFnSc4	cesta
</s>
</p>
<p>
<s>
Stínadla	stínadlo	k1gNnPc1	stínadlo
se	se	k3xPyFc4	se
bouří	bouřit	k5eAaImIp3nP	bouřit
(	(	kIx(	(
<g/>
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
Záhadu	záhada	k1gFnSc4	záhada
hlavolamu	hlavolam	k1gInSc2	hlavolam
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc4	druhý
díl	díl	k1gInSc4	díl
tzv.	tzv.	kA	tzv.
Stínadelské	Stínadelský	k2eAgFnSc2d1	Stínadelská
trilogie	trilogie	k1gFnSc2	trilogie
</s>
</p>
<p>
<s>
Tajemná	tajemný	k2eAgFnSc1d1	tajemná
Řásnovka	Řásnovka	k1gFnSc1	Řásnovka
(	(	kIx(	(
<g/>
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
o	o	k7c6	o
peripetiích	peripetie	k1gFnPc6	peripetie
chlapeckého	chlapecký	k2eAgInSc2d1	chlapecký
klubu	klub	k1gInSc2	klub
na	na	k7c6	na
pražské	pražský	k2eAgFnSc6d1	Pražská
Řásnovce	Řásnovka	k1gFnSc6	Řásnovka
</s>
</p>
<p>
<s>
Poklad	poklad	k1gInSc1	poklad
Černého	Černý	k1gMnSc2	Černý
delfína	delfín	k1gMnSc2	delfín
(	(	kIx(	(
<g/>
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
o	o	k7c6	o
dvou	dva	k4xCgInPc6	dva
pražských	pražský	k2eAgInPc6d1	pražský
klucích	kluk	k1gMnPc6	kluk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
vstoupí	vstoupit	k5eAaPmIp3nP	vstoupit
do	do	k7c2	do
skautského	skautský	k2eAgInSc2d1	skautský
oddílu	oddíl	k1gInSc2	oddíl
<g/>
,	,	kIx,	,
děj	děj	k1gInSc1	děj
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
konkrétně	konkrétně	k6eAd1	konkrétně
lokalizován	lokalizovat	k5eAaBmNgInS	lokalizovat
do	do	k7c2	do
ulic	ulice	k1gFnPc2	ulice
Starého	Starého	k2eAgNnSc2d1	Starého
Města	město	k1gNnSc2	město
</s>
</p>
<p>
<s>
Kronika	kronika	k1gFnSc1	kronika
Ztracené	ztracený	k2eAgFnSc2d1	ztracená
stopy	stopa	k1gFnSc2	stopa
(	(	kIx(	(
<g/>
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rady	rada	k1gFnPc4	rada
a	a	k8xC	a
návody	návod	k1gInPc4	návod
pro	pro	k7c4	pro
činnost	činnost	k1gFnSc4	činnost
klubů	klub	k1gInPc2	klub
</s>
</p>
<p>
<s>
Dobrodružství	dobrodružství	k1gNnSc1	dobrodružství
v	v	k7c6	v
Zemi	zem	k1gFnSc6	zem
nikoho	nikdo	k3yNnSc4	nikdo
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
příběh	příběh	k1gInSc4	příběh
dvou	dva	k4xCgMnPc2	dva
kamarádů	kamarád	k1gMnPc2	kamarád
Vládi	Vláďa	k1gFnSc2	Vláďa
a	a	k8xC	a
Vincka	Vincek	k1gMnSc4	Vincek
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
zapletou	zaplést	k5eAaPmIp3nP	zaplést
do	do	k7c2	do
hledání	hledání	k1gNnSc2	hledání
vzácných	vzácný	k2eAgFnPc2d1	vzácná
zkamenělin	zkamenělina	k1gFnPc2	zkamenělina
v	v	k7c6	v
opuštěné	opuštěný	k2eAgFnSc6d1	opuštěná
lomové	lomový	k2eAgFnSc6d1	Lomová
krajině	krajina	k1gFnSc6	krajina
</s>
</p>
<p>
<s>
Devadesátka	devadesátka	k1gFnSc1	devadesátka
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
knihu	kniha	k1gFnSc4	kniha
Pod	pod	k7c7	pod
junáckou	junácký	k2eAgFnSc7d1	Junácká
vlajkou	vlajka	k1gFnSc7	vlajka
</s>
</p>
<p>
<s>
Tajemství	tajemství	k1gNnSc1	tajemství
Velkého	velký	k2eAgInSc2d1	velký
Vonta	Vont	k1gInSc2	Vont
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
české	český	k2eAgNnSc1d1	české
vydání	vydání	k1gNnSc1	vydání
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
Stínadla	stínadlo	k1gNnPc4	stínadlo
se	se	k3xPyFc4	se
bouří	bouřit	k5eAaImIp3nS	bouřit
a	a	k8xC	a
uzavírá	uzavírat	k5eAaImIp3nS	uzavírat
tzv.	tzv.	kA	tzv.
Stínadelskou	Stínadelský	k2eAgFnSc4d1	Stínadelská
trilogii	trilogie	k1gFnSc4	trilogie
<g/>
,	,	kIx,	,
vydanou	vydaný	k2eAgFnSc4d1	vydaná
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
svazku	svazek	k1gInSc6	svazek
jako	jako	k9	jako
Rychlé	Rychlé	k2eAgInPc4d1	Rychlé
šípy	šíp	k1gInPc4	šíp
ve	v	k7c6	v
Stínadlech	stínadlo	k1gNnPc6	stínadlo
</s>
</p>
<p>
<s>
Dobrodružství	dobrodružství	k1gNnSc1	dobrodružství
v	v	k7c6	v
temných	temný	k2eAgFnPc6d1	temná
uličkách	ulička	k1gFnPc6	ulička
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
Záhadu	záhada	k1gFnSc4	záhada
hlavolamu	hlavolam	k1gInSc2	hlavolam
<g/>
,	,	kIx,	,
Stínadla	stínadlo	k1gNnPc1	stínadlo
se	se	k3xPyFc4	se
bouří	bouřit	k5eAaImIp3nP	bouřit
a	a	k8xC	a
první	první	k4xOgNnSc4	první
vydání	vydání	k1gNnSc4	vydání
Tajemství	tajemství	k1gNnSc4	tajemství
Velkého	velký	k2eAgInSc2d1	velký
Vonta	Vont	k1gInSc2	Vont
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Tajemství	tajemství	k1gNnSc1	tajemství
Velkého	velký	k2eAgInSc2d1	velký
Vonta	Vont	k1gInSc2	Vont
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
první	první	k4xOgNnSc1	první
samostatné	samostatný	k2eAgNnSc1d1	samostatné
vydání	vydání	k1gNnSc1	vydání
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Život	život	k1gInSc1	život
v	v	k7c6	v
poklusu	poklus	k1gInSc6	poklus
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
/	/	kIx~	/
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
autobiografie	autobiografie	k1gFnSc1	autobiografie
</s>
</p>
<p>
<s>
Nováček	Nováček	k1gMnSc1	Nováček
Bubáček	bubáček	k1gMnSc1	bubáček
píše	psát	k5eAaImIp3nS	psát
deník	deník	k1gInSc1	deník
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
humornou	humorný	k2eAgFnSc7d1	humorná
nadsázkou	nadsázka	k1gFnSc7	nadsázka
podané	podaný	k2eAgInPc1d1	podaný
zážitky	zážitek	k1gInPc1	zážitek
skautského	skautský	k2eAgMnSc2d1	skautský
nováčka	nováček	k1gMnSc2	nováček
</s>
</p>
<p>
<s>
Jestřábe	jestřáb	k1gMnSc5	jestřáb
<g/>
,	,	kIx,	,
vypravuj	vypravovat	k5eAaImRp2nS	vypravovat
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sbírka	sbírka	k1gFnSc1	sbírka
krátkých	krátký	k2eAgFnPc2d1	krátká
povídek	povídka	k1gFnPc2	povídka
z	z	k7c2	z
rozličných	rozličný	k2eAgNnPc2d1	rozličné
prostředí	prostředí	k1gNnSc2	prostředí
</s>
</p>
<p>
<s>
Strach	strach	k1gInSc1	strach
nad	nad	k7c7	nad
Bobří	bobří	k2eAgFnSc7d1	bobří
řekou	řeka	k1gFnSc7	řeka
(	(	kIx(	(
<g/>
knižně	knižně	k6eAd1	knižně
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
napsáno	napsán	k2eAgNnSc1d1	napsáno
ale	ale	k9	ale
o	o	k7c4	o
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
dříve	dříve	k6eAd2	dříve
<g/>
)	)	kIx)	)
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
Hochy	hoch	k1gMnPc4	hoch
od	od	k7c2	od
Bobří	bobří	k2eAgFnSc2d1	bobří
řeky	řeka	k1gFnSc2	řeka
a	a	k8xC	a
zřetelně	zřetelně	k6eAd1	zřetelně
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
nevysloveně	vysloveně	k6eNd1	vysloveně
<g/>
,	,	kIx,	,
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
k	k	k7c3	k
období	období	k1gNnSc3	období
nacistické	nacistický	k2eAgFnSc2d1	nacistická
okupace	okupace	k1gFnSc2	okupace
</s>
</p>
<p>
<s>
Náš	náš	k3xOp1gInSc1	náš
oddíl	oddíl	k1gInSc1	oddíl
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
příručka	příručka	k1gFnSc1	příručka
pro	pro	k7c4	pro
práci	práce	k1gFnSc4	práce
ve	v	k7c6	v
skautském	skautský	k2eAgInSc6d1	skautský
oddíle	oddíl	k1gInSc6	oddíl
</s>
</p>
<p>
<s>
Modrá	modrý	k2eAgFnSc1d1	modrá
rokle	rokle	k1gFnSc1	rokle
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgInSc1d1	poslední
Foglarův	Foglarův	k2eAgInSc1d1	Foglarův
román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
zpracovaný	zpracovaný	k2eAgInSc1d1	zpracovaný
původně	původně	k6eAd1	původně
jako	jako	k8xC	jako
komiks	komiks	k1gInSc4	komiks
<g/>
,	,	kIx,	,
o	o	k7c6	o
chlapeckém	chlapecký	k2eAgInSc6d1	chlapecký
klubu	klub	k1gInSc6	klub
z	z	k7c2	z
nejmenovaného	jmenovaný	k2eNgNnSc2d1	nejmenované
velkoměsta	velkoměsto	k1gNnSc2	velkoměsto
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
zažívají	zažívat	k5eAaImIp3nP	zažívat
dobrodružství	dobrodružství	k1gNnSc4	dobrodružství
v	v	k7c6	v
opuštěném	opuštěný	k2eAgInSc6d1	opuštěný
krasu	kras	k1gInSc6	kras
</s>
</p>
<p>
<s>
Kronika	kronika	k1gFnSc1	kronika
Hochů	hoch	k1gMnPc2	hoch
od	od	k7c2	od
Bobří	bobří	k2eAgFnSc2d1	bobří
řeky	řeka	k1gFnSc2	řeka
–	–	k?	–
I.	I.	kA	I.
díl	díl	k1gInSc4	díl
<g/>
:	:	kIx,	:
Skautský	skautský	k2eAgInSc4d1	skautský
rok	rok	k1gInSc4	rok
a	a	k8xC	a
II	II	kA	II
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
:	:	kIx,	:
Tábory	Tábor	k1gInPc1	Tábor
ve	v	k7c6	v
Sluneční	sluneční	k2eAgFnSc6d1	sluneční
zátoce	zátoka	k1gFnSc6	zátoka
a	a	k8xC	a
na	na	k7c6	na
Zelené	Zelené	k2eAgFnSc6d1	Zelené
říčce	říčka	k1gFnSc6	říčka
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
výpisy	výpis	k1gInPc1	výpis
z	z	k7c2	z
kronik	kronika	k1gFnPc2	kronika
<g/>
,	,	kIx,	,
editor	editor	k1gInSc1	editor
Miloš	Miloš	k1gMnSc1	Miloš
Zapletal	Zapletal	k1gMnSc1	Zapletal
</s>
</p>
<p>
<s>
Rychlé	Rychlé	k2eAgInPc1d1	Rychlé
šípy	šíp	k1gInPc1	šíp
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
souborné	souborný	k2eAgNnSc1d1	souborné
knižní	knižní	k2eAgNnSc1d1	knižní
vydání	vydání	k1gNnSc1	vydání
kresleného	kreslený	k2eAgInSc2d1	kreslený
seriálu	seriál	k1gInSc2	seriál
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Svorní	svorný	k2eAgMnPc1d1	svorný
gambusíni	gambusín	k1gMnPc1	gambusín
a	a	k8xC	a
jiné	jiný	k2eAgInPc1d1	jiný
příběhy	příběh	k1gInPc1	příběh
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
knižní	knižní	k2eAgNnSc1d1	knižní
vydání	vydání	k1gNnSc1	vydání
dalších	další	k2eAgInPc2d1	další
kreslených	kreslený	k2eAgInPc2d1	kreslený
seriálů	seriál	k1gInPc2	seriál
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Z	z	k7c2	z
Bobří	bobří	k2eAgFnSc2d1	bobří
hráze	hráz	k1gFnSc2	hráz
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
výpisy	výpis	k1gInPc1	výpis
z	z	k7c2	z
kronik	kronika	k1gFnPc2	kronika
<g/>
,	,	kIx,	,
editor	editor	k1gInSc1	editor
Miloš	Miloš	k1gMnSc1	Miloš
Zapletal	Zapletal	k1gMnSc1	Zapletal
</s>
</p>
<p>
<s>
Hry	hra	k1gFnPc1	hra
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Foglara	Foglar	k1gMnSc2	Foglar
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
výpisy	výpis	k1gInPc1	výpis
z	z	k7c2	z
kronik	kronika	k1gFnPc2	kronika
<g/>
,	,	kIx,	,
editor	editor	k1gInSc1	editor
Miloš	Miloš	k1gMnSc1	Miloš
Zapletal	Zapletal	k1gMnSc1	Zapletal
</s>
</p>
<p>
<s>
Rychlé	Rychlé	k2eAgInPc4d1	Rychlé
šípy	šíp	k1gInPc4	šíp
ve	v	k7c6	v
Stínadlech	stínadlo	k1gNnPc6	stínadlo
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
soubor	soubor	k1gInSc1	soubor
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
Záhadu	záhada	k1gFnSc4	záhada
hlavolamu	hlavolam	k1gInSc2	hlavolam
<g/>
,	,	kIx,	,
Stínadla	stínadlo	k1gNnPc1	stínadlo
se	se	k3xPyFc4	se
bouří	bouřit	k5eAaImIp3nP	bouřit
a	a	k8xC	a
Tajemství	tajemství	k1gNnSc1	tajemství
Velkého	velký	k2eAgInSc2d1	velký
Vonta	Vont	k1gInSc2	Vont
<g/>
,	,	kIx,	,
ilustrace	ilustrace	k1gFnPc1	ilustrace
Milan	Milan	k1gMnSc1	Milan
Teslevič	Teslevič	k1gMnSc1	Teslevič
</s>
</p>
<p>
<s>
Tábor	Tábor	k1gInSc1	Tábor
Zelené	Zelené	k2eAgFnSc2d1	Zelené
příšery	příšera	k1gFnSc2	příšera
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
výpisy	výpis	k1gInPc1	výpis
z	z	k7c2	z
kronik	kronika	k1gFnPc2	kronika
<g/>
,	,	kIx,	,
editor	editor	k1gInSc1	editor
Miloš	Miloš	k1gMnSc1	Miloš
Zapletal	Zapletal	k1gMnSc1	Zapletal
</s>
</p>
<p>
<s>
Tábor	Tábor	k1gInSc1	Tábor
ve	v	k7c6	v
Sluneční	sluneční	k2eAgFnSc6d1	sluneční
zátoce	zátoka	k1gFnSc6	zátoka
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
libreto	libreto	k1gNnSc1	libreto
zmíněné	zmíněný	k2eAgFnSc2d1	zmíněná
divadelní	divadelní	k2eAgFnSc2d1	divadelní
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
přehled	přehled	k1gInSc4	přehled
táborů	tábor	k1gInPc2	tábor
Jaroslava	Jaroslav	k1gMnSc4	Jaroslav
Foglara	Foglar	k1gMnSc4	Foglar
a	a	k8xC	a
příručku	příručka	k1gFnSc4	příručka
Tábornická	tábornický	k2eAgFnSc1d1	tábornická
moudrost	moudrost	k1gFnSc1	moudrost
<g/>
,	,	kIx,	,
aneb	aneb	k?	aneb
Tábor	Tábor	k1gInSc1	Tábor
–	–	k?	–
podnik	podnik	k1gInSc1	podnik
pro	pro	k7c4	pro
celé	celý	k2eAgMnPc4d1	celý
muže	muž	k1gMnPc4	muž
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Příručky	příručka	k1gFnSc2	příručka
pro	pro	k7c4	pro
práci	práce	k1gFnSc4	práce
s	s	k7c7	s
mládeží	mládež	k1gFnSc7	mládež
===	===	k?	===
</s>
</p>
<p>
<s>
Zápisník	zápisník	k1gInSc1	zápisník
13	[number]	k4	13
bobříků	bobřík	k1gMnPc2	bobřík
(	(	kIx(	(
<g/>
s	s	k7c7	s
Dr.	dr.	kA	dr.
Karlem	Karel	k1gMnSc7	Karel
Burešem	Bureš	k1gMnSc7	Bureš
<g/>
,	,	kIx,	,
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Můj	můj	k3xOp1gInSc1	můj
turistický	turistický	k2eAgInSc1d1	turistický
zápisník	zápisník	k1gInSc1	zápisník
(	(	kIx(	(
<g/>
s	s	k7c7	s
Kamilem	Kamil	k1gMnSc7	Kamil
Buderou	Budera	k1gMnSc7	Budera
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Skautské	skautský	k2eAgFnPc1d1	skautská
hry	hra	k1gFnPc1	hra
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
(	(	kIx(	(
<g/>
s	s	k7c7	s
Milošem	Miloš	k1gMnSc7	Miloš
Zapletalem	Zapletal	k1gMnSc7	Zapletal
+	+	kIx~	+
autoři	autor	k1gMnPc1	autor
<g/>
:	:	kIx,	:
Robert	Robert	k1gMnSc1	Robert
Baden-Powell	Baden-Powell	k1gMnSc1	Baden-Powell
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
Benjamin	Benjamin	k1gMnSc1	Benjamin
Svojsík	Svojsík	k1gMnSc1	Svojsík
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Skautské	skautský	k2eAgFnPc1d1	skautská
hry	hra	k1gFnPc1	hra
v	v	k7c6	v
klubovně	klubovna	k1gFnSc6	klubovna
(	(	kIx(	(
<g/>
s	s	k7c7	s
Milošem	Miloš	k1gMnSc7	Miloš
Zapletalem	Zapletal	k1gMnSc7	Zapletal
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Skautský	skautský	k2eAgInSc1d1	skautský
tábor	tábor	k1gInSc1	tábor
(	(	kIx(	(
<g/>
s	s	k7c7	s
Milošem	Miloš	k1gMnSc7	Miloš
Zapletalem	Zapletal	k1gMnSc7	Zapletal
<g/>
,	,	kIx,	,
Jiřím	Jiří	k1gMnSc7	Jiří
Herzánem	Herzán	k1gMnSc7	Herzán
<g/>
,	,	kIx,	,
Jiřím	Jiří	k1gMnSc7	Jiří
Lasovským	Lasovský	k1gMnSc7	Lasovský
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Kreslené	kreslený	k2eAgInPc1d1	kreslený
seriály	seriál	k1gInPc1	seriál
===	===	k?	===
</s>
</p>
<p>
<s>
Rychlé	Rychlé	k2eAgInPc1d1	Rychlé
šípy	šíp	k1gInPc1	šíp
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
–	–	k?	–
<g/>
1941	[number]	k4	1941
<g/>
,	,	kIx,	,
1946	[number]	k4	1946
<g/>
–	–	k?	–
<g/>
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
1966	[number]	k4	1966
<g/>
–	–	k?	–
<g/>
1971	[number]	k4	1971
<g/>
,	,	kIx,	,
1986	[number]	k4	1986
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
souborné	souborný	k2eAgNnSc4d1	souborné
knižní	knižní	k2eAgNnSc4d1	knižní
vydání	vydání	k1gNnSc4	vydání
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
</s>
</p>
<p>
<s>
Svorní	svorný	k2eAgMnPc1d1	svorný
gambusíni	gambusín	k1gMnPc1	gambusín
(	(	kIx(	(
<g/>
1943	[number]	k4	1943
<g/>
–	–	k?	–
<g/>
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Příběhy	příběh	k1gInPc1	příběh
Medvědí	medvědí	k2eAgFnSc2d1	medvědí
družiny	družina	k1gFnSc2	družina
(	(	kIx(	(
<g/>
1945	[number]	k4	1945
<g/>
/	/	kIx~	/
<g/>
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pim	Pim	k?	Pim
a	a	k8xC	a
Red	Red	k1gMnSc1	Red
(	(	kIx(	(
<g/>
1947	[number]	k4	1947
<g/>
–	–	k?	–
<g/>
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Za	za	k7c4	za
poklady	poklad	k1gInPc4	poklad
starých	starý	k2eAgMnPc2d1	starý
Inků	Ink	k1gMnPc2	Ink
(	(	kIx(	(
<g/>
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
K	k	k7c3	k
pramenům	pramen	k1gInPc3	pramen
neznámé	známý	k2eNgFnSc2d1	neznámá
řeky	řeka	k1gFnSc2	řeka
(	(	kIx(	(
<g/>
1964	[number]	k4	1964
<g/>
–	–	k?	–
<g/>
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kulišáci	Kulišák	k1gMnPc1	Kulišák
(	(	kIx(	(
<g/>
1963	[number]	k4	1963
<g/>
–	–	k?	–
<g/>
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Chata	chata	k1gFnSc1	chata
v	v	k7c6	v
Jezerní	jezerní	k2eAgFnSc6d1	jezerní
kotlině	kotlina	k1gFnSc6	kotlina
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
/	/	kIx~	/
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Záhada	záhada	k1gFnSc1	záhada
hlavolamu	hlavolam	k1gInSc2	hlavolam
(	(	kIx(	(
<g/>
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Stínadla	stínadlo	k1gNnPc1	stínadlo
se	se	k3xPyFc4	se
bouří	bouřit	k5eAaImIp3nP	bouřit
(	(	kIx(	(
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Tajemství	tajemství	k1gNnSc1	tajemství
Velkého	velký	k2eAgInSc2d1	velký
Vonta	Vont	k1gInSc2	Vont
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Modrá	modrý	k2eAgFnSc1d1	modrá
rokle	rokle	k1gFnSc1	rokle
(	(	kIx(	(
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
komiks	komiks	k1gInSc1	komiks
Káji	Káji	k?	Káji
Saudka	Saudek	k1gMnSc2	Saudek
</s>
</p>
<p>
<s>
Ztracený	ztracený	k2eAgMnSc1d1	ztracený
kamarád	kamarád	k1gMnSc1	kamarád
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
volné	volný	k2eAgNnSc4d1	volné
pokračování	pokračování	k1gNnSc4	pokračování
Modré	modrý	k2eAgFnSc2d1	modrá
rokle	rokle	k1gFnSc2	rokle
<g/>
,	,	kIx,	,
komiks	komiks	k1gInSc1	komiks
Káji	Káji	k?	Káji
Saudka	Saudek	k1gMnSc2	Saudek
</s>
</p>
<p>
<s>
Jeskyně	jeskyně	k1gFnSc1	jeskyně
Saturn	Saturn	k1gMnSc1	Saturn
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
/	/	kIx~	/
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
další	další	k2eAgNnSc4d1	další
volné	volný	k2eAgNnSc4d1	volné
pokračování	pokračování	k1gNnSc4	pokračování
Modré	modrý	k2eAgFnSc2d1	modrá
rokle	rokle	k1gFnSc2	rokle
<g/>
,	,	kIx,	,
komiks	komiks	k1gInSc1	komiks
Káji	Káji	k?	Káji
Saudka	Saudek	k1gMnSc2	Saudek
</s>
</p>
<p>
<s>
Kreslené	kreslený	k2eAgInPc1d1	kreslený
seriály	seriál	k1gInPc1	seriál
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Foglara	Foglar	k1gMnSc2	Foglar
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
výběr	výběr	k1gInSc4	výběr
sestavil	sestavit	k5eAaPmAgMnS	sestavit
Ivan	Ivan	k1gMnSc1	Ivan
Vápenka	vápenka	k1gFnSc1	vápenka
</s>
</p>
<p>
<s>
Expedice	expedice	k1gFnSc1	expedice
Borneo	Borneo	k1gNnSc4	Borneo
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Hoši	hoch	k1gMnPc1	hoch
od	od	k7c2	od
Bobří	bobří	k2eAgFnSc2d1	bobří
řeky	řeka	k1gFnSc2	řeka
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
komiks	komiks	k1gInSc1	komiks
Ivany	Ivana	k1gFnSc2	Ivana
Peroutkové	Peroutková	k1gFnSc2	Peroutková
a	a	k8xC	a
Milana	Milan	k1gMnSc2	Milan
Tesleviče	Teslevič	k1gMnSc2	Teslevič
</s>
</p>
<p>
<s>
Modrý	modrý	k2eAgInSc1d1	modrý
život	život	k1gInSc1	život
Jiřího	Jiří	k1gMnSc2	Jiří
Dražana	Dražan	k1gMnSc2	Dražan
aneb	aneb	k?	aneb
Přístav	přístav	k1gInSc1	přístav
volá	volat	k5eAaImIp3nS	volat
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
komiks	komiks	k1gInSc4	komiks
Milana	Milan	k1gMnSc2	Milan
Tesleviče	Teslevič	k1gMnSc2	Teslevič
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Divadelní	divadelní	k2eAgFnSc2d1	divadelní
hry	hra	k1gFnSc2	hra
===	===	k?	===
</s>
</p>
<p>
<s>
Tábor	Tábor	k1gInSc1	Tábor
ve	v	k7c6	v
Sluneční	sluneční	k2eAgFnSc6d1	sluneční
zátoce	zátoka	k1gFnSc6	zátoka
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Tajemství	tajemství	k1gNnSc1	tajemství
Jezerní	jezerní	k2eAgFnSc2d1	jezerní
kotliny	kotlina	k1gFnSc2	kotlina
(	(	kIx(	(
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Filmy	film	k1gInPc1	film
a	a	k8xC	a
seriály	seriál	k1gInPc1	seriál
podle	podle	k7c2	podle
námětu	námět	k1gInSc2	námět
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Foglara	Foglar	k1gMnSc2	Foglar
===	===	k?	===
</s>
</p>
<p>
<s>
Záhada	záhada	k1gFnSc1	záhada
hlavolamu	hlavolam	k1gInSc2	hlavolam
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
–	–	k?	–
film	film	k1gInSc1	film
</s>
</p>
<p>
<s>
Záhada	záhada	k1gFnSc1	záhada
hlavolamu	hlavolam	k1gInSc2	hlavolam
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
–	–	k?	–
televizní	televizní	k2eAgInSc4d1	televizní
seriál	seriál	k1gInSc4	seriál
</s>
</p>
<p>
<s>
Stínadla	stínadlo	k1gNnPc1	stínadlo
se	se	k3xPyFc4	se
bouří	bouřit	k5eAaImIp3nP	bouřit
–	–	k?	–
rozhlasový	rozhlasový	k2eAgInSc1d1	rozhlasový
seriál	seriál	k1gInSc1	seriál
</s>
</p>
<p>
<s>
Když	když	k8xS	když
Duben	duben	k1gInSc1	duben
přichází	přicházet	k5eAaImIp3nS	přicházet
-	-	kIx~	-
rozhlasový	rozhlasový	k2eAgInSc4d1	rozhlasový
seriál	seriál	k1gInSc4	seriál
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Nevydaná	vydaný	k2eNgNnPc1d1	nevydané
díla	dílo	k1gNnPc1	dílo
(	(	kIx(	(
<g/>
stav	stav	k1gInSc1	stav
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2018	[number]	k4	2018
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Klub	klub	k1gInSc1	klub
zvídavých	zvídavý	k2eAgFnPc2d1	zvídavá
dětí	dítě	k1gFnPc2	dítě
–	–	k?	–
rozsáhlý	rozsáhlý	k2eAgInSc4d1	rozsáhlý
text	text	k1gInSc4	text
s	s	k7c7	s
příhodami	příhoda	k1gFnPc7	příhoda
šestice	šestice	k1gFnSc2	šestice
pražských	pražský	k2eAgFnPc2d1	Pražská
dětí	dítě	k1gFnPc2	dítě
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
o	o	k7c6	o
Foglarovi	Foglar	k1gMnSc6	Foglar
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Knihy	kniha	k1gFnSc2	kniha
===	===	k?	===
</s>
</p>
<p>
<s>
DVORSKÝ	Dvorský	k1gMnSc1	Dvorský
<g/>
,	,	kIx,	,
Miloš	Miloš	k1gMnSc1	Miloš
<g/>
:	:	kIx,	:
Mýtus	mýtus	k1gInSc1	mýtus
zvaný	zvaný	k2eAgInSc1d1	zvaný
Stínadla	stínadlo	k1gNnPc1	stínadlo
<g/>
.	.	kIx.	.
</s>
<s>
NZB	NZB	kA	NZB
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-904272-2-8	[number]	k4	978-80-904272-2-8
<g/>
,	,	kIx,	,
Druhé	druhý	k4xOgInPc4	druhý
<g/>
,	,	kIx,	,
rozšířené	rozšířený	k2eAgNnSc4d1	rozšířené
vydání	vydání	k1gNnSc4	vydání
<g/>
.	.	kIx.	.
</s>
<s>
NZB	NZB	kA	NZB
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-904272-5-9	[number]	k4	978-80-904272-5-9
</s>
</p>
<p>
<s>
DVORSKÝ	Dvorský	k1gMnSc1	Dvorský
<g/>
,	,	kIx,	,
Miloš	Miloš	k1gMnSc1	Miloš
<g/>
.	.	kIx.	.
</s>
<s>
Mýtus	mýtus	k1gInSc1	mýtus
zvaný	zvaný	k2eAgInSc1d1	zvaný
Stínadla	stínadlo	k1gNnSc2	stínadlo
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
NZB	NZB	kA	NZB
<g/>
,	,	kIx,	,
2018.	[number]	k4	2018.
376	[number]	k4	376
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
978-80-906755-1-3	[number]	k4	978-80-906755-1-3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
FORST	FORST	kA	FORST
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Lexikon	lexikon	k1gInSc1	lexikon
české	český	k2eAgFnSc2d1	Česká
literatury	literatura	k1gFnSc2	literatura
:	:	kIx,	:
osobnosti	osobnost	k1gFnPc1	osobnost
<g/>
,	,	kIx,	,
díla	dílo	k1gNnPc1	dílo
<g/>
,	,	kIx,	,
instituce	instituce	k1gFnPc1	instituce
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
A	A	kA	A
<g/>
-	-	kIx~	-
<g/>
G.	G.	kA	G.
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
1985.	[number]	k4	1985.
900	[number]	k4	900
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
80-200-0797-0	[number]	k4	80-200-0797-0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HAMANOVÁ	Hamanová	k1gFnSc1	Hamanová
<g/>
,	,	kIx,	,
Růžena	Růžena	k1gFnSc1	Růžena
–	–	k?	–
LÁBUSOVÁ	LÁBUSOVÁ	kA	LÁBUSOVÁ
<g/>
,	,	kIx,	,
Dorota	Dorota	k1gFnSc1	Dorota
(	(	kIx(	(
<g/>
eds	eds	k?	eds
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
K	k	k7c3	k
fenoménu	fenomén	k1gInSc3	fenomén
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Foglar	Foglar	k1gMnSc1	Foglar
:	:	kIx,	:
sborník	sborník	k1gInSc1	sborník
příspěvků	příspěvek	k1gInPc2	příspěvek
z	z	k7c2	z
konference	konference	k1gFnSc2	konference
Fenomén	fenomén	k1gMnSc1	fenomén
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Foglar	Foglar	k1gMnSc1	Foglar
<g/>
.	.	kIx.	.
</s>
<s>
Památník	památník	k1gInSc1	památník
národního	národní	k2eAgNnSc2d1	národní
písemnictví	písemnictví	k1gNnSc2	písemnictví
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978-80-85085-88-4	[number]	k4	978-80-85085-88-4
</s>
</p>
<p>
<s>
HOJER	HOJER	kA	HOJER
<g/>
,	,	kIx,	,
Jindřich	Jindřich	k1gMnSc1	Jindřich
<g/>
,	,	kIx,	,
ČERNÝ	Černý	k1gMnSc1	Černý
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Jestřábe	jestřáb	k1gMnSc5	jestřáb
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
–	–	k?	–
Jaroslavu	Jaroslava	k1gFnSc4	Jaroslava
Foglarovi	Foglarův	k2eAgMnPc1d1	Foglarův
Hoši	hoch	k1gMnPc1	hoch
od	od	k7c2	od
Bobří	bobří	k2eAgFnSc2d1	bobří
řeky	řeka	k1gFnSc2	řeka
a	a	k8xC	a
přátelé	přítel	k1gMnPc1	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-86289-12-5	[number]	k4	80-86289-12-5
</s>
</p>
<p>
<s>
JIRÁSEK	Jirásek	k1gMnSc1	Jirásek
<g/>
,	,	kIx,	,
Ivo	Ivo	k1gMnSc1	Ivo
(	(	kIx(	(
<g/>
ed	ed	k?	ed
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Fenomén	fenomén	k1gMnSc1	fenomén
Foglar	Foglar	k1gMnSc1	Foglar
<g/>
.	.	kIx.	.
</s>
<s>
Prázdninová	prázdninový	k2eAgFnSc1d1	prázdninová
škola	škola	k1gFnSc1	škola
Lipnice	Lipnice	k1gFnSc2	Lipnice
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978-80-239-9736-1	[number]	k4	978-80-239-9736-1
</s>
</p>
<p>
<s>
KOL	kol	k6eAd1	kol
<g/>
.	.	kIx.	.
</s>
<s>
Klub	klub	k1gInSc1	klub
zvídavých	zvídavý	k2eAgFnPc2d1	zvídavá
dětí	dítě	k1gFnPc2	dítě
•	•	k?	•
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Foglar	Foglar	k1gMnSc1	Foglar
a	a	k8xC	a
Protektorát	protektorát	k1gInSc1	protektorát
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
NZB	NZB	kA	NZB
<g/>
,	,	kIx,	,
2018.	[number]	k4	2018.
412	[number]	k4	412
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
978-80-906755-6-8	[number]	k4	978-80-906755-6-8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MIKULA	Mikula	k1gMnSc1	Mikula
<g/>
,	,	kIx,	,
Lumír	Lumír	k1gMnSc1	Lumír
<g/>
:	:	kIx,	:
Causa	causa	k1gFnSc1	causa
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Foglar	Foglar	k1gMnSc1	Foglar
<g/>
.	.	kIx.	.
</s>
<s>
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
,	,	kIx,	,
1991.	[number]	k4	1991.
</s>
</p>
<p>
<s>
MRVA	Mrva	k1gMnSc1	Mrva
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Foglar	Foglar	k1gMnSc1	Foglar
<g/>
.	.	kIx.	.
1.	[number]	k4	1.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Kroměříž	Kroměříž	k1gFnSc1	Kroměříž
<g/>
:	:	kIx,	:
Okresní	okresní	k2eAgFnSc1d1	okresní
knihovna	knihovna	k1gFnSc1	knihovna
<g/>
,	,	kIx,	,
1990.	[number]	k4	1990.
66	[number]	k4	66
s	s	k7c7	s
<g/>
.	.	kIx.	.
Předmluva	předmluva	k1gFnSc1	předmluva
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Foglara	Foglar	k1gMnSc2	Foglar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
NOSEK	Nosek	k1gMnSc1	Nosek
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
<g/>
:	:	kIx,	:
Jestřábí	jestřábí	k2eAgFnSc2d1	jestřábí
perutě	peruť	k1gFnSc2	peruť
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
</s>
</p>
<p>
<s>
PÍREK	pírko	k1gNnPc2	pírko
<g/>
,	,	kIx,	,
Zdeněk	Zdeňka	k1gFnPc2	Zdeňka
<g/>
:	:	kIx,	:
Čtenářské	čtenářský	k2eAgInPc1d1	čtenářský
kluby	klub	k1gInPc1	klub
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Foglara	Foglar	k1gMnSc2	Foglar
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
1990.	[number]	k4	1990.
</s>
</p>
<p>
<s>
POLÁK	Polák	k1gMnSc1	Polák
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
:	:	kIx,	:
Poselství	poselství	k1gNnSc1	poselství
žlutého	žlutý	k2eAgInSc2d1	žlutý
kvítku	kvítek	k1gInSc2	kvítek
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
</s>
</p>
<p>
<s>
RABA	rab	k1gMnSc4	rab
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
:	:	kIx,	:
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Foglar	Foglar	k1gMnSc1	Foglar
v	v	k7c6	v
hádankách	hádanka	k1gFnPc6	hádanka
a	a	k8xC	a
vzpomínkách	vzpomínka	k1gFnPc6	vzpomínka
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
</s>
</p>
<p>
<s>
SOHR	SOHR	kA	SOHR
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
<g/>
:	:	kIx,	:
Zase	zase	k9	zase
zní	znět	k5eAaImIp3nS	znět
píseň	píseň	k1gFnSc4	píseň
úplňku	úplněk	k1gInSc2	úplněk
<g/>
.	.	kIx.	.
</s>
<s>
Karviná	Karviná	k1gFnSc1	Karviná
<g/>
,	,	kIx,	,
1968	[number]	k4	1968
</s>
</p>
<p>
<s>
VUČKA	VUČKA	kA	VUČKA
<g/>
,	,	kIx,	,
Tomáš	Tomáš	k1gMnSc1	Tomáš
<g/>
:	:	kIx,	:
Cesta	cesta	k1gFnSc1	cesta
za	za	k7c7	za
modrým	modrý	k2eAgNnSc7d1	modré
světlem	světlo	k1gNnSc7	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Meditace	meditace	k1gFnSc1	meditace
nad	nad	k7c7	nad
texty	text	k1gInPc7	text
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Foglara	Foglar	k1gMnSc2	Foglar
<g/>
.	.	kIx.	.
</s>
<s>
Příbram	Příbram	k1gFnSc1	Příbram
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978-80-87855-26-3	[number]	k4	978-80-87855-26-3
</s>
</p>
<p>
<s>
ZACHARIÁŠ	ZACHARIÁŠ	kA	ZACHARIÁŠ
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
:	:	kIx,	:
Stoletý	stoletý	k2eAgMnSc1d1	stoletý
hoch	hoch	k1gMnSc1	hoch
od	od	k7c2	od
Bobří	bobří	k2eAgFnSc2d1	bobří
řeky	řeka	k1gFnSc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Ostrov	ostrov	k1gInSc1	ostrov
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
</s>
</p>
<p>
<s>
ZAPLETAL	Zapletal	k1gMnSc1	Zapletal
<g/>
,	,	kIx,	,
Miloš	Miloš	k1gMnSc1	Miloš
<g/>
:	:	kIx,	:
Záhady	záhada	k1gFnPc1	záhada
a	a	k8xC	a
tajemství	tajemství	k1gNnSc1	tajemství
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Foglara	Foglar	k1gMnSc2	Foglar
<g/>
.	.	kIx.	.
</s>
<s>
Knižní	knižní	k2eAgInSc1d1	knižní
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978-80-242-1902-8	[number]	k4	978-80-242-1902-8
</s>
</p>
<p>
<s>
HOŠEK	Hošek	k1gMnSc1	Hošek
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
<g/>
:	:	kIx,	:
Evangelium	evangelium	k1gNnSc1	evangelium
podle	podle	k7c2	podle
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Foglara	Foglar	k1gMnSc2	Foglar
<g/>
.	.	kIx.	.
</s>
<s>
CDK	CDK	kA	CDK
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978-80-7325-427-8	[number]	k4	978-80-7325-427-8
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
HORÁČKOVÁ	Horáčková	k1gFnSc1	Horáčková
<g/>
,	,	kIx,	,
Alice	Alice	k1gFnSc1	Alice
<g/>
:	:	kIx,	:
Foglar	Foglar	k1gInSc1	Foglar
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
mě	já	k3xPp1nSc4	já
polobůh	polobůh	k1gMnSc1	polobůh
<g/>
,	,	kIx,	,
rozhovor	rozhovor	k1gInSc1	rozhovor
s	s	k7c7	s
Milošem	Miloš	k1gMnSc7	Miloš
Zapletalem	Zapletal	k1gMnSc7	Zapletal
<g/>
,	,	kIx,	,
MF	MF	kA	MF
DNES	dnes	k6eAd1	dnes
18.	[number]	k4	18.
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
2007	[number]	k4	2007
<g/>
;	;	kIx,	;
na	na	k7c4	na
iDNES	iDNES	k?	iDNES
jako	jako	k8xS	jako
Foglar	Foglar	k1gMnSc1	Foglar
byl	být	k5eAaImAgMnS	být
polobůh	polobůh	k1gMnSc1	polobůh
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nikoli	nikoli	k9	nikoli
dokonalý	dokonalý	k2eAgInSc1d1	dokonalý
</s>
</p>
<p>
<s>
HORÁČKOVÁ	Horáčková	k1gFnSc1	Horáčková
<g/>
,	,	kIx,	,
Alice	Alice	k1gFnSc1	Alice
<g/>
:	:	kIx,	:
Muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
nechtěl	chtít	k5eNaImAgMnS	chtít
dospět	dospět	k5eAaPmF	dospět
<g/>
,	,	kIx,	,
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
DNES	dnes	k6eAd1	dnes
7.	[number]	k4	7.
7.	[number]	k4	7.
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
iDNES	iDNES	k?	iDNES
jako	jako	k8xC	jako
Stoletý	stoletý	k2eAgMnSc1d1	stoletý
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Foglar	Foglar	k1gMnSc1	Foglar
nechtěl	chtít	k5eNaImAgMnS	chtít
dospět	dospět	k5eAaPmF	dospět
</s>
</p>
<p>
<s>
JANDOUREK	JANDOUREK	kA	JANDOUREK
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
:	:	kIx,	:
Dokonalí	dokonalit	k5eAaImIp3nP	dokonalit
hoši	hoch	k1gMnPc1	hoch
<g/>
.	.	kIx.	.
</s>
<s>
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
DNES	dnes	k6eAd1	dnes
3.	[number]	k4	3.
3.	[number]	k4	3.
2001	[number]	k4	2001
</s>
</p>
<p>
<s>
JANDOUREK	JANDOUREK	kA	JANDOUREK
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
:	:	kIx,	:
Skutečný	skutečný	k2eAgMnSc1d1	skutečný
fenomén	fenomén	k1gMnSc1	fenomén
–	–	k?	–
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Foglar	Foglar	k1gMnSc1	Foglar
<g/>
,	,	kIx,	,
MF	MF	kA	MF
DNES	dnes	k6eAd1	dnes
20.	[number]	k4	20.
10.	[number]	k4	10.
2007	[number]	k4	2007
</s>
</p>
<p>
<s>
KOMÁREK	Komárek	k1gMnSc1	Komárek
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
<g/>
:	:	kIx,	:
Jestřáb	jestřáb	k1gMnSc1	jestřáb
a	a	k8xC	a
kuřátka	kuřátko	k1gNnPc1	kuřátko
<g/>
.	.	kIx.	.
</s>
<s>
Právo	právo	k1gNnSc1	právo
21.	[number]	k4	21.
6.	[number]	k4	6.
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
Salon	salon	k1gInSc1	salon
Práva	právo	k1gNnSc2	právo
<g/>
,	,	kIx,	,
str.	str.	kA	str.
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Mír	mír	k1gInSc4	mír
s	s	k7c7	s
mloky	mlok	k1gMnPc7	mlok
<g/>
,	,	kIx,	,
Petrov	Petrov	k1gInSc1	Petrov
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7227-167-9	[number]	k4	80-7227-167-9
<g/>
,	,	kIx,	,
ukázka	ukázka	k1gFnSc1	ukázka
včetně	včetně	k7c2	včetně
Jestřáb	jestřáb	k1gMnSc1	jestřáb
a	a	k8xC	a
kuřátka	kuřátko	k1gNnPc1	kuřátko
</s>
</p>
<p>
<s>
SADECKÝ	SADECKÝ	kA	SADECKÝ
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
:	:	kIx,	:
Proč	proč	k6eAd1	proč
mlčí	mlčet	k5eAaImIp3nS	mlčet
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Foglar	Foglar	k1gMnSc1	Foglar
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
,	,	kIx,	,
Zlatý	zlatý	k2eAgInSc1d1	zlatý
máj	máj	k1gInSc1	máj
č	č	k0	č
<g/>
.	.	kIx.	.
3/1964	[number]	k4	3/1964
<g/>
,	,	kIx,	,
str.	str.	kA	str.
105	[number]	k4	105
<g/>
–	–	k?	–
<g/>
108	[number]	k4	108
<g/>
,	,	kIx,	,
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
číslech	číslo	k1gNnPc6	číslo
(	(	kIx(	(
<g/>
cca	cca	kA	cca
4	[number]	k4	4
<g/>
–	–	k?	–
<g/>
11	[number]	k4	11
<g/>
)	)	kIx)	)
v	v	k7c6	v
diskusi	diskuse	k1gFnSc6	diskuse
reagují	reagovat	k5eAaBmIp3nP	reagovat
Vladimír	Vladimír	k1gMnSc1	Vladimír
Kovářík	kovářík	k1gMnSc1	kovářík
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Foglar	Foglar	k1gMnSc1	Foglar
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
Kovářík	kovářík	k1gMnSc1	kovářík
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Stejskal	Stejskal	k1gMnSc1	Stejskal
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Vavřík	Vavřík	k1gMnSc1	Vavřík
<g/>
,	,	kIx,	,
Bedřich	Bedřich	k1gMnSc1	Bedřich
Bösser	Bösser	k1gMnSc1	Bösser
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VACKE	VACKE	kA	VACKE
<g/>
,	,	kIx,	,
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
<g/>
:	:	kIx,	:
Svět	svět	k1gInSc1	svět
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
chlapce	chlapec	k1gMnPc4	chlapec
<g/>
?	?	kIx.	?
</s>
<s>
Kmen	kmen	k1gInSc1	kmen
(	(	kIx(	(
<g/>
časopis	časopis	k1gInSc1	časopis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
1988	[number]	k4	1988
<g/>
,	,	kIx,	,
č	č	k0	č
<g/>
.	.	kIx.	.
40	[number]	k4	40
<g/>
,	,	kIx,	,
str.	str.	kA	str.
1	[number]	k4	1
</s>
</p>
<p>
<s>
VALÁŠEK	Valášek	k1gMnSc1	Valášek
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Jandourek	Jandourek	k1gMnSc1	Jandourek
<g/>
:	:	kIx,	:
Svět	svět	k1gInSc1	svět
Foglarových	Foglarův	k2eAgNnPc2d1	Foglarovo
Stínadel	stínadlo	k1gNnPc2	stínadlo
<g/>
,	,	kIx,	,
Souvislosti	souvislost	k1gFnPc1	souvislost
–	–	k?	–
revue	revue	k1gFnSc2	revue
pro	pro	k7c4	pro
literaturu	literatura	k1gFnSc4	literatura
a	a	k8xC	a
kulturu	kultura	k1gFnSc4	kultura
<g/>
,	,	kIx,	,
č	č	k0	č
<g/>
.	.	kIx.	.
30	[number]	k4	30
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
/	/	kIx~	/
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
téma	téma	k1gNnSc4	téma
čísla	číslo	k1gNnSc2	číslo
Dětství	dětství	k1gNnSc2	dětství
<g/>
,	,	kIx,	,
str.	str.	kA	str.
69	[number]	k4	69
<g/>
–	–	k?	–
<g/>
85	[number]	k4	85
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
část	část	k1gFnSc1	část
č	č	k0	č
<g/>
.	.	kIx.	.
4/1996	[number]	k4	4/1996
jako	jako	k8xC	jako
Portable	portable	k1gInSc1	portable
Document	Document	k1gInSc1	Document
Format	Format	k1gInSc1	Format
400	[number]	k4	400
kB	kb	kA	kb
<g/>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Zajímavost	zajímavost	k1gFnSc4	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
rozhodlo	rozhodnout	k5eAaPmAgNnS	rozhodnout
zastupitelstvo	zastupitelstvo	k1gNnSc1	zastupitelstvo
města	město	k1gNnSc2	město
Dobříš	dobřit	k5eAaImIp2nS	dobřit
o	o	k7c4	o
pojmenování	pojmenování	k1gNnSc4	pojmenování
několika	několik	k4yIc2	několik
nových	nový	k2eAgFnPc2d1	nová
ulic	ulice	k1gFnPc2	ulice
na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
díla	dílo	k1gNnSc2	dílo
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Foglara	Foglar	k1gMnSc2	Foglar
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
původně	původně	k6eAd1	původně
navržené	navržený	k2eAgFnSc3d1	navržená
ulici	ulice	k1gFnSc3	ulice
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Foglara	Foglar	k1gMnSc2	Foglar
přibyly	přibýt	k5eAaPmAgFnP	přibýt
ještě	ještě	k6eAd1	ještě
ulice	ulice	k1gFnPc1	ulice
Rychlých	Rychlých	k2eAgInPc2d1	Rychlých
šípů	šíp	k1gInPc2	šíp
<g/>
,	,	kIx,	,
Ke	k	k7c3	k
Stínadlům	stínadlo	k1gNnPc3	stínadlo
<g/>
,	,	kIx,	,
Jana	Jan	k1gMnSc2	Jan
Tleskače	tleskač	k1gMnSc2	tleskač
a	a	k8xC	a
Velkého	velký	k2eAgNnSc2d1	velké
Vonta	Vonto	k1gNnSc2	Vonto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Už	už	k6eAd1	už
dříve	dříve	k6eAd2	dříve
přitom	přitom	k6eAd1	přitom
k	k	k7c3	k
podobnému	podobný	k2eAgInSc3d1	podobný
kroku	krok	k1gInSc3	krok
sáhli	sáhnout	k5eAaPmAgMnP	sáhnout
v	v	k7c6	v
Kuřimi	Kuři	k1gFnPc7	Kuři
na	na	k7c6	na
Brněnsku	Brněnsko	k1gNnSc6	Brněnsko
–	–	k?	–
v	v	k7c6	v
nové	nový	k2eAgFnSc6d1	nová
obytné	obytný	k2eAgFnSc6d1	obytná
lokalitě	lokalita	k1gFnSc6	lokalita
jsou	být	k5eAaImIp3nP	být
ulice	ulice	k1gFnPc1	ulice
Foglarova	Foglarův	k2eAgFnSc1d1	Foglarova
a	a	k8xC	a
Jestřábova	jestřábův	k2eAgFnSc1d1	Jestřábova
<g/>
,	,	kIx,	,
Dušínova	Dušínův	k2eAgFnSc1d1	Dušínova
<g/>
,	,	kIx,	,
Metelkova	Metelkův	k2eAgFnSc1d1	Metelkova
<g/>
,	,	kIx,	,
Hojerova	Hojerův	k2eAgFnSc1d1	Hojerova
<g/>
,	,	kIx,	,
Červenáčkova	Červenáčkův	k2eAgFnSc1d1	Červenáčkova
a	a	k8xC	a
Rychlonožkova	Rychlonožkův	k2eAgFnSc1d1	Rychlonožkova
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
Bohoušova	Bohoušův	k2eAgNnPc1d1	Bohoušovo
<g/>
,	,	kIx,	,
Losnova	Losnův	k2eAgNnPc1d1	Losnův
<g/>
,	,	kIx,	,
Rozdělovací	rozdělovací	k2eAgNnPc1d1	rozdělovací
<g/>
,	,	kIx,	,
Tleskačova	tleskačův	k2eAgNnPc1d1	Tleskačovo
a	a	k8xC	a
Vontská	Vontský	k2eAgNnPc1d1	Vontský
<g/>
.	.	kIx.	.
</s>
<s>
Smutným	smutný	k2eAgInSc7d1	smutný
paradoxem	paradox	k1gInSc7	paradox
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
právě	právě	k9	právě
v	v	k7c6	v
Dušínově	Dušínův	k2eAgFnSc6d1	Dušínova
ulici	ulice	k1gFnSc6	ulice
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
týrání	týrání	k1gNnSc3	týrání
dvou	dva	k4xCgMnPc2	dva
malých	malý	k2eAgMnPc2d1	malý
chlapců	chlapec	k1gMnPc2	chlapec
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
Kuřimské	kuřimský	k2eAgFnSc6d1	Kuřimská
kauze	kauza	k1gFnSc6	kauza
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Rychlé	Rychlé	k2eAgInPc4d1	Rychlé
šípy	šíp	k1gInPc4	šíp
</s>
</p>
<p>
<s>
Klub	klub	k1gInSc1	klub
zvídavých	zvídavý	k2eAgFnPc2d1	zvídavá
dětí	dítě	k1gFnPc2	dítě
</s>
</p>
<p>
<s>
Kulišáci	Kulišák	k1gMnPc1	Kulišák
</s>
</p>
<p>
<s>
Svorní	svorný	k2eAgMnPc1d1	svorný
gambusíni	gambusín	k1gMnPc1	gambusín
</s>
</p>
<p>
<s>
Mirek	Mirek	k1gMnSc1	Mirek
Dušín	Dušín	k1gMnSc1	Dušín
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Tleskač	tleskač	k1gMnSc1	tleskač
</s>
</p>
<p>
<s>
Ježek	Ježek	k1gMnSc1	Ježek
v	v	k7c6	v
kleci	klec	k1gFnSc6	klec
</s>
</p>
<p>
<s>
Literatura	literatura	k1gFnSc1	literatura
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Foglar	Foglara	k1gFnPc2	Foglara
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Foglar	Foglar	k1gMnSc1	Foglar
ve	v	k7c6	v
Slovníku	slovník	k1gInSc6	slovník
české	český	k2eAgFnSc2d1	Česká
literatury	literatura	k1gFnSc2	literatura
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
(	(	kIx(	(
<g/>
projekt	projekt	k1gInSc1	projekt
ÚČL	ÚČL	kA	ÚČL
AV	AV	kA	AV
ČR	ČR	kA	ČR
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Oddíl	oddíl	k1gInSc1	oddíl
Pražská	pražský	k2eAgFnSc1d1	Pražská
Dvojka	dvojka	k1gFnSc1	dvojka
</s>
</p>
<p>
<s>
Bohoušek	Bohoušek	k1gMnSc1	Bohoušek
–	–	k?	–
Foglarovský	Foglarovský	k2eAgInSc4d1	Foglarovský
magazín	magazín	k1gInSc4	magazín
</s>
</p>
<p>
<s>
iKlubovna	iKlubovna	k1gFnSc1	iKlubovna
<g/>
,	,	kIx,	,
klubový	klubový	k2eAgInSc1d1	klubový
web	web	k1gInSc1	web
Sdružení	sdružení	k1gNnSc2	sdružení
přátel	přítel	k1gMnPc2	přítel
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Foglara	Foglar	k1gMnSc2	Foglar
(	(	kIx(	(
<g/>
prezentace	prezentace	k1gFnSc1	prezentace
sdružení	sdružení	k1gNnSc2	sdružení
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
na	na	k7c4	na
http://www.spjf.cz	[url]	k1gInSc4	http://www.spjf.cz
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Foglar	Foglar	k1gMnSc1	Foglar
–	–	k?	–
Jestřáb	jestřáb	k1gMnSc1	jestřáb
<g/>
,	,	kIx,	,
neoficiální	oficiální	k2eNgFnSc1d1	neoficiální
stránka	stránka	k1gFnSc1	stránka
<g/>
,	,	kIx,	,
Michal	Michal	k1gMnSc1	Michal
Ambrož	Ambrož	k1gMnSc1	Ambrož
</s>
</p>
<p>
<s>
Foglarweb	Foglarwba	k1gFnPc2	Foglarwba
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Novotný	Novotný	k1gMnSc1	Novotný
</s>
</p>
<p>
<s>
Vontové	Vontová	k1gFnPc1	Vontová
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
</s>
</p>
<p>
<s>
Muzeum	muzeum	k1gNnSc1	muzeum
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Foglara	Foglar	k1gMnSc2	Foglar
-	-	kIx~	-
první	první	k4xOgNnSc1	první
muzeum	muzeum	k1gNnSc1	muzeum
věnované	věnovaný	k2eAgFnSc2d1	věnovaná
našemu	náš	k3xOp1gMnSc3	náš
významnému	významný	k2eAgMnSc3d1	významný
českému	český	k2eAgMnSc3d1	český
spisovateli	spisovatel	k1gMnSc3	spisovatel
</s>
</p>
