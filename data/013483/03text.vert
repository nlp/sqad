<s>
Layla	Layla	k1gFnSc1
(	(	kIx(
<g/>
zpěvačka	zpěvačka	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
LaylaZákladní	LaylaZákladný	k2eAgMnPc1d1
informace	informace	k1gFnSc2
Rodné	rodný	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
</s>
<s>
Kristína	Kristína	k1gFnSc1
Tranová	Tranový	k2eAgFnSc1d1
Narození	narození	k1gNnPc2
</s>
<s>
1985	#num#	k4
Žánry	žánr	k1gInPc4
</s>
<s>
soul	soul	k1gInSc1
<g/>
,	,	kIx,
R	R	kA
<g/>
&	&	k?
<g/>
B	B	kA
Web	web	k1gInSc1
</s>
<s>
http://laylaworld.com/	http://laylaworld.com/	k?
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Layla	Layla	k1gFnSc1
(	(	kIx(
<g/>
*	*	kIx~
<g/>
1985	#num#	k4
Bratislava	Bratislava	k1gFnSc1
<g/>
,	,	kIx,
Československo	Československo	k1gNnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
umělecké	umělecký	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
slovenské	slovenský	k2eAgFnSc2d1
zpěvačky	zpěvačka	k1gFnSc2
Kristíny	Kristína	k1gFnSc2
Tranové	Tranová	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Layla	Layla	k1gMnSc1
zpívá	zpívat	k5eAaImIp3nS
v	v	k7c6
soulových	soulový	k2eAgFnPc6d1
a	a	k8xC
R	R	kA
<g/>
&	&	k?
<g/>
B	B	kA
písních	píseň	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
slovensko-vietnamského	slovensko-vietnamský	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Singly	singl	k1gInPc1
</s>
<s>
Nestrácaj	Nestrácaj	k1gInSc1
dych	dych	k1gInSc1
(	(	kIx(
<g/>
s	s	k7c7
Majkem	Majek	k1gInSc7
Spiritem	Spiritem	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
</s>
<s>
Áno	Áno	k?
či	či	k8xC
nie	nie	k?
<g/>
,	,	kIx,
</s>
<s>
I	i	k9
feel	feel	k1gInSc1
(	(	kIx(
<g/>
s	s	k7c7
Billy	Bill	k1gMnPc7
Hollywoodem	Hollywood	k1gInSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Speváčka	Speváčka	k1gFnSc1
Layla	Layla	k1gFnSc1
bola	bola	k1gFnSc1
online	onlinout	k5eAaPmIp3nS
<g/>
!	!	kIx.
</s>
<s desamb="1">
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
topky	topky	k6eAd1
<g/>
.	.	kIx.
<g/>
sk	sk	k?
<g/>
,	,	kIx,
2010-04-11	2010-04-11	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Slovenská	slovenský	k2eAgFnSc1d1
zpěvačka	zpěvačka	k1gFnSc1
má	mít	k5eAaImIp3nS
světový	světový	k2eAgInSc4d1
videoklip	videoklip	k1gInSc4
<g/>
!	!	kIx.
</s>
<s desamb="1">
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bravo	bravo	k1gMnSc1
web	web	k1gInSc1
<g/>
,	,	kIx,
2014-01-27	2014-01-27	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
