<s desamb="1">
Všechny	všechen	k3xTgMnPc4
tři	tři	k4xCgMnPc4
zpěváky	zpěvák	k1gMnPc4
doprovázela	doprovázet	k5eAaImAgFnS
Rytmická	rytmický	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
divadla	divadlo	k1gNnSc2
Semafor	Semafor	k1gInSc1
ve	v	k7c6
složení	složení	k1gNnSc6
Jiří	Jiří	k1gMnSc1
Bažant	Bažant	k1gMnSc1
(	(	kIx(
<g/>
klavír	klavír	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
Turnovský	turnovský	k1gMnSc1
(	(	kIx(
<g/>
bicí	bicí	k2eAgMnSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Ladislav	Ladislav	k1gMnSc1
Štaidl	Štaidl	k1gMnSc1
(	(	kIx(
<g/>
kytara	kytara	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
Stanislav	Stanislav	k1gMnSc1
Zeman	Zeman	k1gMnSc1
(	(	kIx(
<g/>
kontrabas	kontrabas	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>