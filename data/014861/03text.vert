<s>
Recital	Recital	k1gMnSc1
64	#num#	k4
</s>
<s>
Recital	Recitat	k5eAaImAgMnS,k5eAaPmAgMnS
64	#num#	k4
Autor	autor	k1gMnSc1
</s>
<s>
Jiří	Jiří	k1gMnSc1
Suchý	Suchý	k1gMnSc1
Žánr	žánr	k1gInSc4
</s>
<s>
recitalpásmo	recitalpásmo	k6eAd1
písniček	písnička	k1gFnPc2
Premiéra	premiér	k1gMnSc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1964	#num#	k4
Místo	místo	k7c2
premiéry	premiéra	k1gFnSc2
</s>
<s>
Městská	městský	k2eAgFnSc1d1
knihovna	knihovna	k1gFnSc1
v	v	k7c6
Praze	Praha	k1gFnSc6
Soubor	soubor	k1gInSc1
</s>
<s>
Semafor	Semafor	k1gInSc4
Počet	počet	k1gInSc1
představení	představení	k1gNnSc1
</s>
<s>
44	#num#	k4
Štáb	štáb	k1gInSc1
Režie	režie	k1gFnSc2
</s>
<s>
Jiří	Jiří	k1gMnSc1
SuchýJiří	SuchýJiří	k1gNnSc2
Šlitr	Šlitr	k1gMnSc1
Hudba	hudba	k1gFnSc1
</s>
<s>
Jiří	Jiří	k1gMnSc1
Šlituke	Šlituk	k1gInSc2
EllingtonRay	EllingtonRaa	k1gFnSc2
HendersonBurton	HendersonBurton	k1gInSc1
LaneFrank	LaneFrank	k1gInSc1
PerkinsHank	PerkinsHanka	k1gFnPc2
WilliamsJiří	WilliamsJiří	k2eAgMnSc1d1
BažantJiří	BažantJiří	k2eAgMnSc1d1
Suchý	Suchý	k1gMnSc1
Obsazení	obsazení	k1gNnSc2
</s>
<s>
Jiří	Jiří	k1gMnSc1
SuchýJiří	SuchýJiří	k1gNnSc2
ŠlitrVlasta	ŠlitrVlast	k1gMnSc2
Kahovcová	Kahovcový	k2eAgFnSc1d1
Profesionální	profesionální	k2eAgFnSc1d1
kritika	kritika	k1gFnSc1
</s>
<s>
Gm	Gm	k?
<g/>
,	,	kIx,
Lidová	lidový	k2eAgFnSc1d1
demokracie	demokracie	k1gFnSc1
3	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
1964	#num#	k4
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1
Přikryl	Přikryl	k1gMnSc1
<g/>
,	,	kIx,
Večerní	večerní	k2eAgFnSc1d1
Praha	Praha	k1gFnSc1
3	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
1964	#num#	k4
</s>
<s>
Alena	Alena	k1gFnSc1
Stránská	Stránská	k1gFnSc1
<g/>
,	,	kIx,
Divadelní	divadelní	k2eAgFnPc1d1
noviny	novina	k1gFnPc1
22	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
1964	#num#	k4
</s>
<s>
Sergej	Sergej	k1gMnSc1
Machonin	Machonina	k1gFnPc2
<g/>
,	,	kIx,
Literární	literární	k2eAgFnPc1d1
noviny	novina	k1gFnPc1
25	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
1964	#num#	k4
</s>
<s>
Semafor	Semafor	k1gInSc1
chronologicky	chronologicky	k6eAd1
Ondráš	Ondráš	k1gMnSc3
podotýká	podotýkat	k5eAaImIp3nS
(	(	kIx(
<g/>
1963	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Dvacet	dvacet	k4xCc1
(	(	kIx(
<g/>
koncert	koncert	k1gInSc1
orchestru	orchestr	k1gInSc2
Ferdinanda	Ferdinand	k1gMnSc2
Havlíka	Havlík	k1gMnSc2
<g/>
)	)	kIx)
(	(	kIx(
<g/>
1964	#num#	k4
<g/>
)	)	kIx)
Jiří	Jiří	k1gMnSc1
Suchý	Suchý	k1gMnSc1
chronologicky	chronologicky	k6eAd1
Zuzana	Zuzana	k1gFnSc1
není	být	k5eNaImIp3nS
pro	pro	k7c4
nikoho	nikdo	k3yNnSc4
doma	doma	k6eAd1
(	(	kIx(
<g/>
1963	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Sekta	sekta	k1gFnSc1
(	(	kIx(
<g/>
1965	#num#	k4
<g/>
)	)	kIx)
Údaje	údaj	k1gInSc2
v	v	k7c6
infoboxu	infobox	k1gInSc6
aktuální	aktuální	k2eAgInPc1d1
k	k	k7c3
2	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Recital	Recital	k1gMnSc1
64	#num#	k4
(	(	kIx(
<g/>
1964	#num#	k4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
pásmo	pásmo	k1gNnSc4
písniček	písnička	k1gFnPc2
Jiřího	Jiří	k1gMnSc2
Suchého	Suchý	k1gMnSc2
a	a	k8xC
Jiřího	Jiří	k1gMnSc2
Šlitra	Šlitr	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
nová	nový	k2eAgFnSc1d1
zpěvačka	zpěvačka	k1gFnSc1
Semaforu	Semafor	k1gInSc2
se	se	k3xPyFc4
zde	zde	k6eAd1
představila	představit	k5eAaPmAgFnS
Vlasta	Vlasta	k1gFnSc1
Kahovcová	Kahovcový	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgMnPc4
tři	tři	k4xCgMnPc4
zpěváky	zpěvák	k1gMnPc4
doprovázela	doprovázet	k5eAaImAgFnS
Rytmická	rytmický	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
divadla	divadlo	k1gNnSc2
Semafor	Semafor	k1gInSc1
ve	v	k7c6
složení	složení	k1gNnSc6
Jiří	Jiří	k1gMnSc1
Bažant	Bažant	k1gMnSc1
(	(	kIx(
<g/>
klavír	klavír	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
Turnovský	turnovský	k1gMnSc1
(	(	kIx(
<g/>
bicí	bicí	k2eAgMnSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Ladislav	Ladislav	k1gMnSc1
Štaidl	Štaidl	k1gMnSc1
(	(	kIx(
<g/>
kytara	kytara	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
Stanislav	Stanislav	k1gMnSc1
Zeman	Zeman	k1gMnSc1
(	(	kIx(
<g/>
kontrabas	kontrabas	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Pořad	pořad	k1gInSc4
obsahoval	obsahovat	k5eAaImAgInS
jednak	jednak	k8xC
nové	nový	k2eAgFnPc4d1
písně	píseň	k1gFnPc4
Suchého	Suchý	k1gMnSc2
a	a	k8xC
Šlitra	Šlitr	k1gMnSc2
<g/>
,	,	kIx,
jednak	jednak	k8xC
písně	píseň	k1gFnPc1
jiných	jiný	k2eAgMnPc2d1
autorů	autor	k1gMnPc2
(	(	kIx(
<g/>
např.	např.	kA
Voskovce	Voskovec	k1gMnSc2
a	a	k8xC
Wericha	Werich	k1gMnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dále	daleko	k6eAd2
několik	několik	k4yIc1
již	již	k6eAd1
známých	známý	k2eAgFnPc2d1
semaforských	semaforský	k2eAgFnPc2d1
písní	píseň	k1gFnPc2
napsaných	napsaný	k2eAgFnPc2d1
původně	původně	k6eAd1
pro	pro	k7c4
jiné	jiný	k2eAgMnPc4d1
zpěváky	zpěvák	k1gMnPc4
divadla	divadlo	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
v	v	k7c6
novém	nový	k2eAgNnSc6d1
podání	podání	k1gNnSc6
jejich	jejich	k3xOp3gInPc2
autoři	autor	k1gMnPc1
(	(	kIx(
<g/>
převážně	převážně	k6eAd1
Šlitr	Šlitr	k1gInSc1
<g/>
)	)	kIx)
parodovali	parodovat	k5eAaImAgMnP
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
např.	např.	kA
píseň	píseň	k1gFnSc4
Študent	Študent	k?
s	s	k7c7
rudýma	rudý	k2eAgNnPc7d1
ušima	ucho	k1gNnPc7
z	z	k7c2
repertoáru	repertoár	k1gInSc2
Hany	Hana	k1gFnSc2
Hegerové	Hegerová	k1gFnSc2
zpíval	zpívat	k5eAaImAgMnS
Šlitr	Šlitr	k1gMnSc1
s	s	k7c7
žárovkami	žárovka	k1gFnPc7
za	za	k7c7
ušima	ucho	k1gNnPc7
„	„	k?
<g/>
za	za	k7c2
mohutného	mohutný	k2eAgNnSc2d1
chechtání	chechtání	k1gNnSc2
publika	publikum	k1gNnSc2
včetně	včetně	k7c2
Hegerové	Hegerová	k1gFnSc2
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
či	či	k8xC
píseň	píseň	k1gFnSc4
Klokočí	klokočí	k1gNnSc2
ze	z	k7c2
hry	hra	k1gFnSc2
Taková	takový	k3xDgFnSc1
ztráta	ztráta	k1gFnSc1
krve	krev	k1gFnSc2
(	(	kIx(
<g/>
1960	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
Šlitr	Šlitr	k1gMnSc1
zpíval	zpívat	k5eAaImAgMnS
se	s	k7c7
zpožděním	zpoždění	k1gNnSc7
oproti	oproti	k7c3
doprovodu	doprovod	k1gInSc3
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
pořadu	pořad	k1gInSc6
také	také	k9
Jiří	Jiří	k1gMnSc1
Šlitr	Šlitr	k1gMnSc1
zpíval	zpívat	k5eAaImAgMnS
dvě	dva	k4xCgFnPc4
písně	píseň	k1gFnPc4
známé	známá	k1gFnSc2
ze	z	k7c2
hry	hra	k1gFnSc2
Jonáš	Jonáš	k1gMnSc1
a	a	k8xC
tingltangl	tingltangl	k1gInSc1
(	(	kIx(
<g/>
1962	#num#	k4
<g/>
)	)	kIx)
v	v	k7c6
„	„	k?
<g/>
ruském	ruský	k2eAgInSc6d1
<g/>
“	“	k?
překladu	překlad	k1gInSc6
Jiřího	Jiří	k1gMnSc4
Krampola	Krampola	k1gFnSc1
(	(	kIx(
<g/>
Tulipán	tulipán	k1gMnSc1
a	a	k8xC
Klementajn	Klementajn	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
průvodním	průvodní	k2eAgNnSc6d1
slově	slovo	k1gNnSc6
mezi	mezi	k7c7
písněmi	píseň	k1gFnPc7
dělal	dělat	k5eAaImAgMnS
Jiří	Jiří	k1gMnSc1
Šlitr	Šlitr	k1gMnSc1
stále	stále	k6eAd1
narážky	narážka	k1gFnSc2
na	na	k7c4
výsledky	výsledek	k1gInPc4
ankety	anketa	k1gFnSc2
Zlatý	zlatý	k2eAgInSc1d1
slavík	slavík	k1gInSc1
1963	#num#	k4
<g/>
,	,	kIx,
ve	v	k7c4
které	který	k3yIgMnPc4,k3yQgMnPc4,k3yRgMnPc4
skončil	skončit	k5eAaPmAgInS
na	na	k7c6
dvacátém	dvacátý	k4xOgNnSc6
místě	místo	k1gNnSc6
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
Jiří	Jiří	k1gMnSc1
Suchý	Suchý	k1gMnSc1
obsadil	obsadit	k5eAaPmAgMnS
čtvrtou	čtvrtý	k4xOgFnSc4
příčku	příčka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Jednoduchou	jednoduchý	k2eAgFnSc4d1
stavbu	stavba	k1gFnSc4
pořadu	pořad	k1gInSc2
vysvětlil	vysvětlit	k5eAaPmAgMnS
Jiří	Jiří	k1gMnSc1
Suchý	Suchý	k1gMnSc1
v	v	k7c6
programu	program	k1gInSc6
k	k	k7c3
představení	představení	k1gNnSc3
<g/>
,	,	kIx,
kde	kde	k6eAd1
píše	psát	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
uprostřed	uprostřed	k7c2
práce	práce	k1gFnSc2
na	na	k7c6
filmu	film	k1gInSc6
Kdyby	kdyby	k9
tisíc	tisíc	k4xCgInPc2
klarinetů	klarinet	k1gInPc2
(	(	kIx(
<g/>
1964	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
příprav	příprava	k1gFnPc2
jazzové	jazzový	k2eAgFnSc2d1
opery	opera	k1gFnSc2
Dobře	dobře	k6eAd1
placená	placený	k2eAgFnSc1d1
procházka	procházka	k1gFnSc1
(	(	kIx(
<g/>
1965	#num#	k4
<g/>
)	)	kIx)
by	by	kYmCp3nS
si	se	k3xPyFc3
Semafor	Semafor	k1gInSc1
nemohl	moct	k5eNaImAgInS
dovolit	dovolit	k5eAaPmF
další	další	k2eAgInSc1d1
náročný	náročný	k2eAgInSc1d1
program	program	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
formátu	formát	k1gInSc3
recitalu	recital	k1gMnSc3
inspirovala	inspirovat	k5eAaBmAgFnS
Suchého	Suchého	k2eAgFnSc1d1
návštěva	návštěva	k1gFnSc1
Paříže	Paříž	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
menší	malý	k2eAgFnSc6d2
roli	role	k1gFnSc6
klavíristy	klavírista	k1gMnSc2
ve	v	k7c6
hře	hra	k1gFnSc6
Jonáš	Jonáš	k1gMnSc1
a	a	k8xC
tingltangl	tingltangl	k1gInSc4
dospěl	dochvít	k5eAaPmAgMnS
Jiří	Jiří	k1gMnSc1
Šlitr	Šlitr	k1gMnSc1
v	v	k7c6
komika	komik	k1gMnSc2
právě	právě	k9
v	v	k7c6
Recitalu	Recital	k1gMnSc6
64	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Jiří	Jiří	k1gMnSc1
Suchý	Suchý	k1gMnSc1
se	se	k3xPyFc4
evidentně	evidentně	k6eAd1
stáhl	stáhnout	k5eAaPmAgInS
trochu	trochu	k6eAd1
do	do	k7c2
pozadí	pozadí	k1gNnSc2
<g/>
,	,	kIx,
naopak	naopak	k6eAd1
Šlitrovi	Šlitr	k1gMnSc3
napsal	napsat	k5eAaBmAgInS,k5eAaPmAgInS
do	do	k7c2
libreta	libreto	k1gNnSc2
mnohem	mnohem	k6eAd1
více	hodně	k6eAd2
vtipů	vtip	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Seznam	seznam	k1gInSc1
písniček	písnička	k1gFnPc2
</s>
<s>
Blues	blues	k1gNnSc1
touhy	touha	k1gFnSc2
po	po	k7c6
světle	světlo	k1gNnSc6
</s>
<s>
Dívka	dívka	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
zkazil	zkazit	k5eAaPmAgInS
svět	svět	k1gInSc1
</s>
<s>
Půl	půl	k6eAd1
párku	párek	k1gInSc2
</s>
<s>
Blues	blues	k1gNnSc1
o	o	k7c6
stabilitě	stabilita	k1gFnSc6
</s>
<s>
Klokočí	klokočí	k2eAgFnSc1d1
</s>
<s>
V	v	k7c6
kašně	kašna	k1gFnSc6
</s>
<s>
Černá	černý	k2eAgFnSc1d1
vrána	vrána	k1gFnSc1
</s>
<s>
Není	být	k5eNaImIp3nS
<g/>
-li	-li	k?
tu	tu	k6eAd1
ta	ten	k3xDgNnPc1
</s>
<s>
Množení	množení	k1gNnSc1
</s>
<s>
Študent	Študent	k?
s	s	k7c7
rudýma	rudý	k2eAgNnPc7d1
ušima	ucho	k1gNnPc7
</s>
<s>
Labutí	labutí	k2eAgFnSc1d1
píseň	píseň	k1gFnSc1
</s>
<s>
Modrá	modrý	k2eAgFnSc1d1
nálada	nálada	k1gFnSc1
</s>
<s>
Tento	tento	k3xDgInSc4
tón	tón	k1gInSc4
mám	mít	k5eAaImIp1nS
rád	rád	k6eAd1
</s>
<s>
Už	už	k6eAd1
dávno	dávno	k6eAd1
nejsem	být	k5eNaImIp1nS
dítě	dítě	k1gNnSc4
</s>
<s>
Pramínek	pramínek	k1gInSc1
vlasů	vlas	k1gInPc2
</s>
<s>
Oči	oko	k1gNnPc4
sněhem	sníh	k1gInSc7
zaváté	zavátý	k2eAgFnSc2d1
</s>
<s>
Bassin	Bassin	k2eAgMnSc1d1
Street	Street	k1gMnSc1
Blues	blues	k1gNnSc2
</s>
<s>
Anna	Anna	k1gFnSc1
von	von	k1gInSc1
Cléve	Cléev	k1gFnSc2
</s>
<s>
Kdybych	kdyby	kYmCp1nS
byla	být	k5eAaImAgFnS
andělem	anděl	k1gMnSc7
</s>
<s>
Zvuky	zvuk	k1gInPc1
slovenského	slovenský	k2eAgInSc2d1
pralesa	prales	k1gInSc2
</s>
<s>
Big	Big	k?
Bad	Bad	k1gMnSc1
John	John	k1gMnSc1
</s>
<s>
Len	len	k1gInSc1
túto	túto	k6eAd1
noc	noc	k1gFnSc4
</s>
<s>
Tulipán	tulipán	k1gMnSc1
</s>
<s>
Golem	Golem	k1gMnSc1
</s>
<s>
Morality	moralita	k1gFnPc1
</s>
<s>
Klíměntajn	Klíměntajn	k1gMnSc1
</s>
<s>
Záznamy	záznam	k1gInPc1
hry	hra	k1gFnSc2
</s>
<s>
Zvukové	zvukový	k2eAgNnSc1d1
</s>
<s>
nahrávka	nahrávka	k1gFnSc1
neznámého	známý	k2eNgNnSc2d1
data	datum	k1gNnSc2
</s>
<s>
Divadlo	divadlo	k1gNnSc1
Semafor	Semafor	k1gInSc1
1959	#num#	k4
<g/>
–	–	k?
<g/>
1969	#num#	k4
<g/>
,	,	kIx,
Supraphon	supraphon	k1gInSc1
<g/>
,	,	kIx,
1978	#num#	k4
–	–	k?
výběr	výběr	k1gInSc1
tří	tři	k4xCgFnPc2
písniček	písnička	k1gFnPc2
(	(	kIx(
<g/>
Tento	tento	k3xDgInSc1
tón	tón	k1gInSc1
mám	mít	k5eAaImIp1nS
rád	rád	k6eAd1
<g/>
,	,	kIx,
V	v	k7c6
kašně	kašna	k1gFnSc6
a	a	k8xC
Černá	černý	k2eAgFnSc1d1
vrána	vrána	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
To	ten	k3xDgNnSc4
byl	být	k5eAaImAgMnS
Jiří	Jiří	k1gMnSc1
Šlitr	Šlitr	k1gMnSc1
<g/>
,	,	kIx,
B	B	kA
<g/>
&	&	k?
<g/>
M	M	kA
Music	Music	k1gMnSc1
<g/>
,	,	kIx,
1999	#num#	k4
–	–	k?
výběr	výběr	k1gInSc1
9	#num#	k4
tracků	tracek	k1gMnPc2
</s>
<s>
Po	po	k7c6
babičce	babička	k1gFnSc6
klokočí	klokočí	k1gNnSc2
<g/>
,	,	kIx,
Multisonic	Multisonice	k1gFnPc2
<g/>
,	,	kIx,
2002	#num#	k4
–	–	k?
píseň	píseň	k1gFnSc4
Klokočí	klokočí	k2eAgFnSc2d1
</s>
<s>
Recital	Recital	k1gMnSc1
64	#num#	k4
<g/>
,	,	kIx,
And	Anda	k1gFnPc2
The	The	k1gFnSc2
End	End	k1gMnSc1
Record	Record	k1gMnSc1
<g/>
,	,	kIx,
2006	#num#	k4
–	–	k?
kompletní	kompletní	k2eAgInSc4d1
záznam	záznam	k1gInSc4
představení	představení	k1gNnSc2
</s>
<s>
Obrazové	obrazový	k2eAgNnSc1d1
</s>
<s>
Na	na	k7c6
základě	základ	k1gInSc6
představení	představení	k1gNnSc2
natočil	natočit	k5eAaBmAgInS
Ján	Ján	k1gMnSc1
Roháč	roháč	k1gMnSc1
s	s	k7c7
kameramanem	kameraman	k1gMnSc7
Stanislavem	Stanislav	k1gMnSc7
Milotou	milota	k1gFnSc7
televizní	televizní	k2eAgInSc4d1
film	film	k1gInSc4
Recital	Recital	k1gMnSc1
S	s	k7c7
+	+	kIx~
Š	Š	kA
(	(	kIx(
<g/>
1965	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
kopíruje	kopírovat	k5eAaImIp3nS
původní	původní	k2eAgMnSc1d1
Recital	Recital	k1gMnSc1
64	#num#	k4
a	a	k8xC
jeho	jeho	k3xOp3gNnSc1
obsazení	obsazení	k1gNnSc1
má	mít	k5eAaImIp3nS
stejné	stejný	k2eAgNnSc4d1
složení	složení	k1gNnSc4
jako	jako	k8xS,k8xC
na	na	k7c6
divadle	divadlo	k1gNnSc6
(	(	kIx(
<g/>
doplněné	doplněná	k1gFnSc6
baletem	balet	k1gInSc7
Divadla	divadlo	k1gNnSc2
ABC	ABC	kA
a	a	k8xC
Alhambra	Alhambro	k1gNnSc2
a	a	k8xC
dalšími	další	k2eAgInPc7d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původní	původní	k2eAgInSc1d1
představení	představení	k1gNnSc2
ale	ale	k8xC
muselo	muset	k5eAaImAgNnS
být	být	k5eAaImF
pro	pro	k7c4
potřeby	potřeba	k1gFnPc4
televize	televize	k1gFnSc2
zhruba	zhruba	k6eAd1
o	o	k7c4
polovinu	polovina	k1gFnSc4
zkráceno	zkrátit	k5eAaPmNgNnS
(	(	kIx(
<g/>
na	na	k7c4
45	#num#	k4
minut	minuta	k1gFnPc2
<g/>
)	)	kIx)
a	a	k8xC
tak	tak	k6eAd1
vypadly	vypadnout	k5eAaPmAgFnP
všechny	všechen	k3xTgFnPc1
předělávky	předělávka	k1gFnPc1
jiných	jiný	k2eAgMnPc2d1
autorů	autor	k1gMnPc2
a	a	k8xC
další	další	k2eAgFnPc4d1
písně	píseň	k1gFnPc4
(	(	kIx(
<g/>
V	v	k7c6
kašně	kašna	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naopak	naopak	k6eAd1
přibyl	přibýt	k5eAaPmAgInS
nový	nový	k2eAgInSc4d1
hit	hit	k1gInSc4
Krajina	Krajina	k1gFnSc1
posedlá	posedlý	k2eAgFnSc1d1
tmou	tma	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Film	film	k1gInSc1
vyšel	vyjít	k5eAaPmAgInS
na	na	k7c6
videokazetě	videokazeta	k1gFnSc6
Recital	Recital	k1gMnSc1
64	#num#	k4
+	+	kIx~
Zlatý	zlatý	k2eAgInSc1d1
zub	zub	k1gInSc1
(	(	kIx(
<g/>
Perplex	perplex	k2eAgFnSc1d1
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednotlivé	jednotlivý	k2eAgFnPc4d1
písničky	písnička	k1gFnPc4
z	z	k7c2
filmu	film	k1gInSc2
také	také	k9
vyšly	vyjít	k5eAaPmAgInP
na	na	k7c4
DVD	DVD	kA
<g/>
,	,	kIx,
ale	ale	k8xC
nesmyslně	smyslně	k6eNd1
roztroušené	roztroušený	k2eAgInPc1d1
na	na	k7c4
všechny	všechen	k3xTgInPc1
tři	tři	k4xCgInPc1
výběrové	výběrový	k2eAgInPc1d1
tituly	titul	k1gInPc1
Největší	veliký	k2eAgInPc1d3
hity	hit	k1gInPc1
(	(	kIx(
<g/>
Supraphon	supraphon	k1gInSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Největší	veliký	k2eAgInPc4d3
hity	hit	k1gInPc4
2	#num#	k4
(	(	kIx(
<g/>
Supraphon	supraphon	k1gInSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Unikáty	unikát	k1gInPc1
(	(	kIx(
<g/>
Supraphon	supraphon	k1gInSc1
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
po	po	k7c6
čtyřech	čtyři	k4xCgFnPc6
písničkách	písnička	k1gFnPc6
na	na	k7c6
každém	každý	k3xTgInSc6
z	z	k7c2
nich	on	k3xPp3gInPc2
<g/>
.	.	kIx.
</s>
<s>
Recitál	recitál	k1gInSc1
1964	#num#	k4
<g/>
/	/	kIx~
<g/>
2019	#num#	k4
</s>
<s>
Pod	pod	k7c7
jménem	jméno	k1gNnSc7
Recitál	recitál	k1gInSc1
1964	#num#	k4
<g/>
/	/	kIx~
<g/>
2019	#num#	k4
se	s	k7c7
19	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2019	#num#	k4
odehrál	odehrát	k5eAaPmAgInS
koncert	koncert	k1gInSc1
Jiřího	Jiří	k1gMnSc2
Suchého	Suchý	k1gMnSc2
v	v	k7c6
Dvořákově	Dvořákův	k2eAgFnSc6d1
síni	síň	k1gFnSc6
Rudolfina	Rudolfinum	k1gNnSc2
v	v	k7c6
rámci	rámec	k1gInSc6
festivalu	festival	k1gInSc2
Struny	struna	k1gFnSc2
podzimu	podzim	k1gInSc2
jako	jako	k8xS,k8xC
připomínka	připomínka	k1gFnSc1
55	#num#	k4
let	léto	k1gNnPc2
od	od	k7c2
uvedení	uvedení	k1gNnSc2
původního	původní	k2eAgInSc2d1
Recitalu	Recital	k1gMnSc3
64	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Písničky	písnička	k1gFnSc2
z	z	k7c2
Recitalu	Recital	k1gMnSc3
64	#num#	k4
zazněly	zaznít	k5eAaPmAgFnP,k5eAaImAgFnP
tři	tři	k4xCgFnPc1
<g/>
,	,	kIx,
jinak	jinak	k6eAd1
šlo	jít	k5eAaImAgNnS
hlavně	hlavně	k9
o	o	k7c4
průřez	průřez	k1gInSc4
tvorbou	tvorba	k1gFnSc7
60	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
<g/>
,	,	kIx,
vizuál	vizuál	k1gMnSc1
a	a	k8xC
filmová	filmový	k2eAgFnSc1d1
ukázka	ukázka	k1gFnSc1
odkazovaly	odkazovat	k5eAaImAgFnP
k	k	k7c3
televiznímu	televizní	k2eAgInSc3d1
záznamu	záznam	k1gInSc3
Recital	Recital	k1gMnSc1
S	s	k7c7
+	+	kIx~
Š.	Š.	kA
Dvě	dva	k4xCgFnPc1
písně	píseň	k1gFnPc1
se	se	k3xPyFc4
Suchým	Suchý	k1gMnPc3
zazpívala	zazpívat	k5eAaPmAgFnS
Jitka	Jitka	k1gFnSc1
Molavcová	Molavcový	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zazněly	zaznít	k5eAaPmAgFnP,k5eAaImAgFnP
tyto	tento	k3xDgFnPc1
písně	píseň	k1gFnPc1
<g/>
:	:	kIx,
Margareta	Margareta	k1gFnSc1
<g/>
,	,	kIx,
Bledá	bledý	k2eAgFnSc1d1
slečna	slečna	k1gFnSc1
<g/>
,	,	kIx,
Byl	být	k5eAaImAgMnS
jednou	jednou	k6eAd1
jeden	jeden	k4xCgMnSc1
král	král	k1gMnSc1
<g/>
,	,	kIx,
Labutí	labutí	k2eAgFnSc1d1
píseň	píseň	k1gFnSc1
<g/>
,	,	kIx,
Tento	tento	k3xDgInSc1
tón	tón	k1gInSc1
mám	mít	k5eAaImIp1nS
rád	rád	k2eAgMnSc1d1
<g/>
,	,	kIx,
Klokočí	klokočí	k1gNnSc1
<g/>
,	,	kIx,
Mississippi	Mississippi	k1gNnSc1
<g/>
,	,	kIx,
Tereza	Tereza	k1gFnSc1
<g/>
,	,	kIx,
Pramínek	pramínek	k1gInSc1
vlasů	vlas	k1gInPc2
<g/>
,	,	kIx,
Kamarádi	kamarád	k1gMnPc1
<g/>
,	,	kIx,
Blues	blues	k1gNnSc1
na	na	k7c4
cestu	cesta	k1gFnSc4
poslední	poslední	k2eAgFnSc1d1
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
Nevyplacený	vyplacený	k2eNgInSc1d1
blues	blues	k1gInSc1
<g/>
,	,	kIx,
Jó	jó	k0
<g/>
,	,	kIx,
to	ten	k3xDgNnSc1
jsem	být	k5eAaImIp1nS
ještě	ještě	k9
žil	žít	k5eAaImAgMnS
<g/>
,	,	kIx,
Blues	blues	k1gFnPc1
pro	pro	k7c4
tebe	ty	k3xPp2nSc4
<g/>
,	,	kIx,
Slečna	slečna	k1gFnSc1
v	v	k7c4
sedmý	sedmý	k4xOgInSc4
řadě	řada	k1gFnSc6
<g/>
,	,	kIx,
Kdykoliv	kdykoliv	k6eAd1
<g/>
,	,	kIx,
kdekoliv	kdekoliv	k6eAd1
<g/>
,	,	kIx,
Zlatý	zlatý	k2eAgInSc1d1
slunce	slunce	k1gNnSc2
nad	nad	k7c7
hlavou	hlava	k1gFnSc7
<g/>
,	,	kIx,
Na	na	k7c4
shledanou	shledaná	k1gFnSc4
a	a	k8xC
Hluboká	hluboký	k2eAgFnSc1d1
vráska	vráska	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Kompletní	kompletní	k2eAgInSc4d1
záznam	záznam	k1gInSc4
koncertu	koncert	k1gInSc2
odvysílala	odvysílat	k5eAaPmAgFnS
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
na	na	k7c6
programu	program	k1gInSc6
ČT	ČT	kA
art	art	k?
26	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2019	#num#	k4
na	na	k7c4
50	#num#	k4
<g/>
.	.	kIx.
výročí	výročí	k1gNnSc4
úmrtí	úmrtí	k1gNnSc2
Jiřího	Jiří	k1gMnSc2
Šlitra	Šlitr	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Stejný	stejný	k2eAgInSc1d1
koncert	koncert	k1gInSc1
je	být	k5eAaImIp3nS
plánován	plánovat	k5eAaImNgInS
i	i	k9
pro	pro	k7c4
Struny	struna	k1gFnPc4
podzimu	podzim	k1gInSc2
2020	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Knižní	knižní	k2eAgNnSc1d1
vydání	vydání	k1gNnSc1
textu	text	k1gInSc2
hry	hra	k1gFnSc2
</s>
<s>
Encyklopedie	encyklopedie	k1gFnSc1
Jiřího	Jiří	k1gMnSc2
Suchého	Suchý	k1gMnSc2
<g/>
,	,	kIx,
svazek	svazek	k1gInSc1
10	#num#	k4
<g/>
,	,	kIx,
divadlo	divadlo	k1gNnSc1
1963	#num#	k4
<g/>
–	–	k?
<g/>
1969	#num#	k4
<g/>
,	,	kIx,
Karolinum	Karolinum	k1gNnSc1
a	a	k8xC
Pražská	pražský	k2eAgFnSc1d1
imaginace	imaginace	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2002	#num#	k4
<g/>
:	:	kIx,
s.	s.	k?
33	#num#	k4
<g/>
–	–	k?
<g/>
42	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
ČERNÝ	Černý	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
:	:	kIx,
Sleeve-note	Sleeve-not	k1gInSc5
na	na	k7c6
LP	LP	kA
Ohlédnutí	ohlédnutí	k1gNnSc1
<g/>
,	,	kIx,
Supraphon	supraphon	k1gInSc1
<g/>
,	,	kIx,
1984	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
KOLÁŘ	Kolář	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
:	:	kIx,
Starý	starý	k2eAgInSc1d1
dobrý	dobrý	k2eAgInSc1d1
Semafor	Semafor	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Divadelní	divadelní	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
4	#num#	k4
<g/>
/	/	kIx~
<g/>
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnPc4d1
online	onlinout	k5eAaPmIp3nS
Archivováno	archivován	k2eAgNnSc4d1
21	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
2009	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
↑	↑	k?
BERNÝ	berný	k2eAgMnSc1d1
<g/>
,	,	kIx,
Lukáš	Lukáš	k1gMnSc1
<g/>
:	:	kIx,
Poznámka	poznámka	k1gFnSc1
editora	editor	k1gMnSc2
v	v	k7c6
bookletu	booklet	k1gInSc6
v	v	k7c6
albu	album	k1gNnSc6
Recital	Recital	k1gMnSc1
64	#num#	k4
<g/>
,	,	kIx,
And	Anda	k1gFnPc2
The	The	k1gFnSc2
End	End	k1gMnSc1
Record	Record	k1gMnSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
program	program	k1gInSc1
festivalu	festival	k1gInSc2
Struny	struna	k1gFnSc2
podzimu	podzim	k1gInSc2
<g/>
.	.	kIx.
strunypodzimu	strunypodzim	k1gInSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Recitál	recitál	k1gInSc1
1964	#num#	k4
<g/>
/	/	kIx~
<g/>
2019	#num#	k4
na	na	k7c6
webu	web	k1gInSc6
České	český	k2eAgFnSc2d1
televize	televize	k1gFnSc2
<g/>
↑	↑	k?
ČTK	ČTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
Strunách	struna	k1gFnPc6
podzimu	podzim	k1gInSc2
vystoupí	vystoupit	k5eAaPmIp3nS
Cohen	Cohen	k1gInSc1
<g/>
,	,	kIx,
Suchý	Suchý	k1gMnSc1
či	či	k8xC
Cantuária	Cantuárium	k1gNnPc1
<g/>
,	,	kIx,
Hope	Hope	k1gInSc1
zahraje	zahrát	k5eAaPmIp3nS
na	na	k7c6
Vltavě	Vltava	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-06-10	2020-06-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Hudba	hudba	k1gFnSc1
</s>
