<s>
Jan	Jan	k1gMnSc1	Jan
Novák	Novák	k1gMnSc1	Novák
(	(	kIx(	(
<g/>
*	*	kIx~	*
29	[number]	k4	29
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1975	[number]	k4	1975
Ostrava-Vítkovice	Ostrava-Vítkovice	k1gFnSc1	Ostrava-Vítkovice
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
politolog	politolog	k1gMnSc1	politolog
a	a	k8xC	a
kariérní	kariérní	k2eAgMnSc1d1	kariérní
státní	státní	k2eAgMnSc1d1	státní
úředník	úředník	k1gMnSc1	úředník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2006	[number]	k4	2006
až	až	k9	až
2010	[number]	k4	2010
působil	působit	k5eAaImAgInS	působit
jako	jako	k9	jako
vedoucí	vedoucí	k1gMnSc1	vedoucí
Úřadu	úřad	k1gInSc2	úřad
vlády	vláda	k1gFnSc2	vláda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
Kanceláři	kancelář	k1gFnSc6	kancelář
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
za	za	k7c4	za
Václava	Václav	k1gMnSc4	Václav
Havla	Havel	k1gMnSc4	Havel
a	a	k8xC	a
Václava	Václav	k1gMnSc4	Václav
Klause	Klaus	k1gMnSc4	Klaus
<g/>
.	.	kIx.	.
</s>
<s>
Působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
náměstek	náměstek	k1gMnSc1	náměstek
ředitele	ředitel	k1gMnSc2	ředitel
Národního	národní	k2eAgInSc2d1	národní
bezpečnostního	bezpečnostní	k2eAgInSc2d1	bezpečnostní
úřadu	úřad	k1gInSc2	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
je	být	k5eAaImIp3nS	být
ředitelem	ředitel	k1gMnSc7	ředitel
Sekce	sekce	k1gFnSc2	sekce
administrativní	administrativní	k2eAgFnPc1d1	administrativní
Kanceláře	kancelář	k1gFnPc1	kancelář
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Psalo	psát	k5eAaImAgNnS	psát
se	se	k3xPyFc4	se
o	o	k7c6	o
něm	on	k3xPp3gNnSc6	on
jako	jako	k8xS	jako
o	o	k7c6	o
příteli	přítel	k1gMnSc6	přítel
Mirka	Mirka	k1gFnSc1	Mirka
Topolánka	Topolánka	k1gFnSc1	Topolánka
a	a	k8xC	a
Marka	Marek	k1gMnSc4	Marek
Dalíka	Dalík	k1gMnSc4	Dalík
<g/>
.	.	kIx.	.
</s>
<s>
Janu	Jan	k1gMnSc3	Jan
Fischerovi	Fischer	k1gMnSc3	Fischer
vedl	vést	k5eAaImAgMnS	vést
prezidentskou	prezidentský	k2eAgFnSc4d1	prezidentská
kampaň	kampaň	k1gFnSc4	kampaň
<g/>
.	.	kIx.	.
</s>
<s>
Věnuje	věnovat	k5eAaImIp3nS	věnovat
se	se	k3xPyFc4	se
otázkám	otázka	k1gFnPc3	otázka
české	český	k2eAgFnSc2d1	Česká
státnosti	státnost	k1gFnSc2	státnost
<g/>
,	,	kIx,	,
státní	státní	k2eAgFnSc2d1	státní
symboliky	symbolika	k1gFnSc2	symbolika
a	a	k8xC	a
vyznamenání	vyznamenání	k1gNnSc2	vyznamenání
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
odborníkem	odborník	k1gMnSc7	odborník
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1994	[number]	k4	1994
<g/>
–	–	k?	–
<g/>
1995	[number]	k4	1995
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
stážista	stážista	k1gMnSc1	stážista
v	v	k7c6	v
Kanceláři	kancelář	k1gFnSc6	kancelář
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
zde	zde	k6eAd1	zde
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
sekretariátu	sekretariát	k1gInSc6	sekretariát
kancléře	kancléř	k1gMnSc2	kancléř
Ivana	Ivan	k1gMnSc2	Ivan
Medka	Medek	k1gMnSc2	Medek
jako	jako	k8xC	jako
specialista	specialista	k1gMnSc1	specialista
pro	pro	k7c4	pro
oblast	oblast	k1gFnSc4	oblast
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
a	a	k8xC	a
tajemník	tajemník	k1gMnSc1	tajemník
komise	komise	k1gFnSc2	komise
pro	pro	k7c4	pro
státní	státní	k2eAgNnSc4d1	státní
vyznamenání	vyznamenání	k1gNnSc4	vyznamenání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1999-2003	[number]	k4	1999-2003
pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
Politickém	politický	k2eAgInSc6d1	politický
odboru	odbor	k1gInSc6	odbor
KPR	KPR	kA	KPR
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
ředitelem	ředitel	k1gMnSc7	ředitel
Bezpečnostního	bezpečnostní	k2eAgInSc2d1	bezpečnostní
odboru	odbor	k1gInSc2	odbor
a	a	k8xC	a
bezpečnostním	bezpečnostní	k2eAgMnSc7d1	bezpečnostní
ředitelem	ředitel	k1gMnSc7	ředitel
Kanceláře	kancelář	k1gFnSc2	kancelář
prezidenta	prezident	k1gMnSc4	prezident
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
vedoucí	vedoucí	k1gMnSc1	vedoucí
Úřadu	úřad	k1gInSc2	úřad
vlády	vláda	k1gFnSc2	vláda
ČR	ČR	kA	ČR
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
5	[number]	k4	5
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2006	[number]	k4	2006
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Mirka	Mirek	k1gMnSc2	Mirek
Topolánka	Topolánek	k1gMnSc2	Topolánek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozici	pozice	k1gFnSc6	pozice
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
za	za	k7c2	za
vlád	vláda	k1gFnPc2	vláda
Jana	Jan	k1gMnSc2	Jan
Fischera	Fischer	k1gMnSc2	Fischer
a	a	k8xC	a
Petra	Petr	k1gMnSc2	Petr
Nečase	Nečas	k1gMnSc2	Nečas
<g/>
.	.	kIx.	.
</s>
<s>
Ukončil	ukončit	k5eAaPmAgMnS	ukončit
ji	on	k3xPp3gFnSc4	on
31	[number]	k4	31
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2010-2012	[number]	k4	2010-2012
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
náměstek	náměstek	k1gMnSc1	náměstek
ředitele	ředitel	k1gMnSc2	ředitel
Národního	národní	k2eAgInSc2d1	národní
bezpečnostního	bezpečnostní	k2eAgInSc2d1	bezpečnostní
úřadu	úřad	k1gInSc2	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
zodpovědný	zodpovědný	k2eAgInSc1d1	zodpovědný
za	za	k7c4	za
státní	státní	k2eAgInSc4d1	státní
dozor	dozor	k1gInSc4	dozor
<g/>
,	,	kIx,	,
oblast	oblast	k1gFnSc4	oblast
fyzické	fyzický	k2eAgFnSc2d1	fyzická
a	a	k8xC	a
administrativní	administrativní	k2eAgFnSc2d1	administrativní
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
a	a	k8xC	a
za	za	k7c4	za
kancelář	kancelář	k1gFnSc4	kancelář
ředitele	ředitel	k1gMnSc2	ředitel
NBÚ	NBÚ	kA	NBÚ
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
držitelem	držitel	k1gMnSc7	držitel
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
bezpečnostních	bezpečnostní	k2eAgFnPc2d1	bezpečnostní
prověrek	prověrka	k1gFnPc2	prověrka
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
NATO	NATO	kA	NATO
Vedl	vést	k5eAaImAgInS	vést
politickou	politický	k2eAgFnSc4d1	politická
kampaň	kampaň	k1gFnSc4	kampaň
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
ČR	ČR	kA	ČR
Janu	Jan	k1gMnSc3	Jan
Fischerovi	Fischer	k1gMnSc3	Fischer
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2014-2015	[number]	k4	2014-2015
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
externí	externí	k2eAgMnSc1d1	externí
poradce	poradce	k1gMnSc1	poradce
Kanceláře	kancelář	k1gFnSc2	kancelář
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
s	s	k7c7	s
analytickým	analytický	k2eAgInSc7d1	analytický
odborem	odbor	k1gInSc7	odbor
Pražského	pražský	k2eAgInSc2d1	pražský
hradu	hrad	k1gInSc2	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2015	[number]	k4	2015
je	být	k5eAaImIp3nS	být
ředitelem	ředitel	k1gMnSc7	ředitel
Sekce	sekce	k1gFnSc2	sekce
administrativní	administrativní	k2eAgFnPc1d1	administrativní
Kanceláře	kancelář	k1gFnPc1	kancelář
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Řídí	řídit	k5eAaImIp3nS	řídit
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgInPc4d1	jiný
útvary	útvar	k1gInPc4	útvar
pro	pro	k7c4	pro
styk	styk	k1gInSc4	styk
s	s	k7c7	s
veřejností	veřejnost	k1gFnSc7	veřejnost
a	a	k8xC	a
památkové	památkový	k2eAgFnSc2d1	památková
péče	péče	k1gFnSc2	péče
<g/>
,	,	kIx,	,
Archiv	archiv	k1gInSc1	archiv
Pražského	pražský	k2eAgInSc2d1	pražský
hradu	hrad	k1gInSc2	hrad
a	a	k8xC	a
Archiv	archiv	k1gInSc1	archiv
Kanceláře	kancelář	k1gFnSc2	kancelář
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
října	říjen	k1gInSc2	říjen
2015	[number]	k4	2015
přímo	přímo	k6eAd1	přímo
řídí	řídit	k5eAaImIp3nS	řídit
také	také	k9	také
Odbor	odbor	k1gInSc1	odbor
bezpečnostní	bezpečnostní	k2eAgFnSc2d1	bezpečnostní
Kanceláře	kancelář	k1gFnSc2	kancelář
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaPmIp3nS	věnovat
otázkám	otázka	k1gFnPc3	otázka
české	český	k2eAgFnSc2d1	Česká
státnosti	státnost	k1gFnSc2	státnost
<g/>
,	,	kIx,	,
státní	státní	k2eAgFnSc2d1	státní
symboliky	symbolika	k1gFnSc2	symbolika
a	a	k8xC	a
vyznamenání	vyznamenání	k1gNnSc2	vyznamenání
<g/>
:	:	kIx,	:
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2000-2001	[number]	k4	2000-2001
organizoval	organizovat	k5eAaBmAgMnS	organizovat
výstavu	výstava	k1gFnSc4	výstava
na	na	k7c6	na
Pražském	pražský	k2eAgInSc6d1	pražský
hradě	hrad	k1gInSc6	hrad
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
poprvé	poprvé	k6eAd1	poprvé
vystavena	vystavit	k5eAaPmNgFnS	vystavit
vyznamenání	vyznamenání	k1gNnSc1	vyznamenání
všech	všecek	k3xTgMnPc2	všecek
prezidentů	prezident	k1gMnPc2	prezident
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
a	a	k8xC	a
všechny	všechen	k3xTgFnPc4	všechen
historické	historický	k2eAgFnPc4d1	historická
podoby	podoba	k1gFnPc4	podoba
Řádu	řád	k1gInSc2	řád
Bílého	bílý	k2eAgInSc2d1	bílý
lva	lev	k1gInSc2	lev
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
byl	být	k5eAaImAgMnS	být
spoluautorem	spoluautor	k1gMnSc7	spoluautor
výstavy	výstava	k1gFnSc2	výstava
v	v	k7c6	v
Hrzánském	Hrzánský	k2eAgInSc6d1	Hrzánský
paláci	palác	k1gInSc6	palác
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
soustředila	soustředit	k5eAaPmAgFnS	soustředit
současná	současný	k2eAgFnSc1d1	současná
vyznamenání	vyznamenání	k1gNnPc2	vyznamenání
všech	všecek	k3xTgFnPc2	všecek
členských	členský	k2eAgFnPc2d1	členská
zemí	zem	k1gFnPc2	zem
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
předsedou	předseda	k1gMnSc7	předseda
správní	správní	k2eAgFnSc2d1	správní
rady	rada	k1gFnSc2	rada
obecně	obecně	k6eAd1	obecně
prospěšné	prospěšný	k2eAgFnSc2d1	prospěšná
společnosti	společnost	k1gFnSc2	společnost
Dny	dna	k1gFnSc2	dna
české	český	k2eAgFnSc2d1	Česká
státnosti	státnost	k1gFnSc2	státnost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
se	s	k7c7	s
státními	státní	k2eAgFnPc7d1	státní
institucemi	instituce	k1gFnPc7	instituce
<g/>
,	,	kIx,	,
médii	médium	k1gNnPc7	médium
a	a	k8xC	a
soukromými	soukromý	k2eAgInPc7d1	soukromý
subjekty	subjekt	k1gInPc7	subjekt
pořádá	pořádat	k5eAaImIp3nS	pořádat
oslavy	oslava	k1gFnSc2	oslava
státního	státní	k2eAgInSc2d1	státní
svátku	svátek	k1gInSc2	svátek
28	[number]	k4	28
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
vedoucí	vedoucí	k1gMnSc1	vedoucí
Úřadu	úřad	k1gInSc2	úřad
vlády	vláda	k1gFnSc2	vláda
inicioval	iniciovat	k5eAaBmAgInS	iniciovat
a	a	k8xC	a
v	v	k7c6	v
letech	let	k1gInPc6	let
2006	[number]	k4	2006
<g/>
–	–	k?	–
<g/>
2009	[number]	k4	2009
vedl	vést	k5eAaImAgMnS	vést
rekonstrukci	rekonstrukce	k1gFnSc4	rekonstrukce
vily	vila	k1gFnSc2	vila
Hany	Hana	k1gFnSc2	Hana
a	a	k8xC	a
Edvarda	Edvard	k1gMnSc2	Edvard
Benešových	Benešová	k1gFnPc2	Benešová
v	v	k7c6	v
Sezimově	Sezimův	k2eAgNnSc6d1	Sezimovo
Ústí	ústí	k1gNnSc6	ústí
do	do	k7c2	do
původní	původní	k2eAgFnSc2d1	původní
podoby	podoba	k1gFnSc2	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Vila	vila	k1gFnSc1	vila
byla	být	k5eAaImAgFnS	být
zpřístupněna	zpřístupněn	k2eAgFnSc1d1	zpřístupněna
veřejnosti	veřejnost	k1gFnSc2	veřejnost
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Jan	Jan	k1gMnSc1	Jan
Novák	Novák	k1gMnSc1	Novák
figuroval	figurovat	k5eAaImAgMnS	figurovat
jako	jako	k9	jako
svědek	svědek	k1gMnSc1	svědek
v	v	k7c6	v
kauze	kauza	k1gFnSc6	kauza
Promopro	Promopro	k1gNnSc1	Promopro
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
podepsal	podepsat	k5eAaPmAgMnS	podepsat
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
firmou	firma	k1gFnSc7	firma
Promopro	Promopro	k1gNnSc1	Promopro
za	za	k7c4	za
Úřad	úřad	k1gInSc4	úřad
vlády	vláda	k1gFnSc2	vláda
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
trval	trvat	k5eAaImAgInS	trvat
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
odpovědnost	odpovědnost	k1gFnSc1	odpovědnost
za	za	k7c4	za
výběr	výběr	k1gInSc4	výběr
dodavatele	dodavatel	k1gMnSc2	dodavatel
a	a	k8xC	a
kontrolu	kontrola	k1gFnSc4	kontrola
plnění	plnění	k1gNnSc2	plnění
nesla	nést	k5eAaImAgFnS	nést
v	v	k7c6	v
kauze	kauza	k1gFnSc6	kauza
Promopro	Promopro	k1gNnSc1	Promopro
podle	podle	k7c2	podle
organizačního	organizační	k2eAgInSc2d1	organizační
řádu	řád	k1gInSc2	řád
a	a	k8xC	a
platných	platný	k2eAgFnPc2d1	platná
směrnic	směrnice	k1gFnPc2	směrnice
Úřadu	úřad	k1gInSc2	úřad
vlády	vláda	k1gFnSc2	vláda
ČR	ČR	kA	ČR
sekce	sekce	k1gFnSc1	sekce
přímo	přímo	k6eAd1	přímo
řízená	řízený	k2eAgFnSc1d1	řízená
vicepremiérem	vicepremiér	k1gMnSc7	vicepremiér
Alexandrem	Alexandr	k1gMnSc7	Alexandr
Vondrou	Vondra	k1gMnSc7	Vondra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případu	případ	k1gInSc6	případ
nebyl	být	k5eNaImAgMnS	být
obviněn	obvinit	k5eAaPmNgMnS	obvinit
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
podle	podle	k7c2	podle
neprokázaného	prokázaný	k2eNgNnSc2d1	neprokázané
tvrzení	tvrzení	k1gNnSc2	tvrzení
Karla	Karel	k1gMnSc2	Karel
Randáka	Randák	k1gMnSc2	Randák
z	z	k7c2	z
období	období	k1gNnSc2	období
prezidentské	prezidentský	k2eAgFnSc2d1	prezidentská
kampaně	kampaň	k1gFnSc2	kampaň
figuruje	figurovat	k5eAaImIp3nS	figurovat
v	v	k7c6	v
kauze	kauza	k1gFnSc6	kauza
nákupů	nákup	k1gInPc2	nákup
obrněných	obrněný	k2eAgInPc2d1	obrněný
transportérů	transportér	k1gInPc2	transportér
Pandur	pandur	k1gMnSc1	pandur
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
měl	mít	k5eAaImAgMnS	mít
Dalíkovi	Dalík	k1gMnSc3	Dalík
o	o	k7c6	o
tomto	tento	k3xDgInSc6	tento
obchodu	obchod	k1gInSc6	obchod
sdělovat	sdělovat	k5eAaImF	sdělovat
utajené	utajený	k2eAgFnPc4d1	utajená
informace	informace	k1gFnPc4	informace
<g/>
.	.	kIx.	.
</s>
<s>
Novák	Novák	k1gMnSc1	Novák
to	ten	k3xDgNnSc4	ten
odmítá	odmítat	k5eAaImIp3nS	odmítat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případu	případ	k1gInSc6	případ
nebyl	být	k5eNaImAgMnS	být
vyšetřován	vyšetřovat	k5eAaImNgMnS	vyšetřovat
ani	ani	k8xC	ani
obviněn	obvinit	k5eAaPmNgMnS	obvinit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
média	médium	k1gNnSc2	médium
informovala	informovat	k5eAaBmAgFnS	informovat
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
registrovaném	registrovaný	k2eAgNnSc6d1	registrované
partnerství	partnerství	k1gNnSc6	partnerství
s	s	k7c7	s
Gustavem	Gustav	k1gMnSc7	Gustav
Slamečkou	Slamečka	k1gFnSc7	Slamečka
<g/>
.	.	kIx.	.
</s>
