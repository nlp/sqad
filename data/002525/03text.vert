<s>
Mekka	Mekka	k1gFnSc1	Mekka
(	(	kIx(	(
<g/>
arabsky	arabsky	k6eAd1	arabsky
م	م	k?	م
Makka	Makek	k1gInSc2	Makek
<g/>
,	,	kIx,	,
v	v	k7c6	v
novější	nový	k2eAgFnSc6d2	novější
době	doba	k1gFnSc6	doba
většinou	většinou	k6eAd1	většinou
م	م	k?	م
ا	ا	k?	ا
<g/>
ّ	ّ	k?	ّ
<g/>
م	م	k?	م
Makka	Makka	k1gFnSc1	Makka
al-Mukarrama	al-Mukarrama	k1gFnSc1	al-Mukarrama
<g/>
'	'	kIx"	'
Mekka	Mekka	k1gFnSc1	Mekka
<g/>
,	,	kIx,	,
ctihodná	ctihodný	k2eAgFnSc1d1	ctihodná
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
s	s	k7c7	s
2	[number]	k4	2
000	[number]	k4	000
000	[number]	k4	000
obyvateli	obyvatel	k1gMnPc7	obyvatel
(	(	kIx(	(
<g/>
stav	stav	k1gInSc4	stav
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
Saúdské	saúdský	k2eAgFnSc2d1	Saúdská
Arábie	Arábie	k1gFnSc2	Arábie
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
provincie	provincie	k1gFnSc2	provincie
Mekka	Mekka	k1gFnSc1	Mekka
v	v	k7c6	v
regionu	region	k1gInSc6	region
Hidžáz	Hidžáz	k1gInSc1	Hidžáz
<g/>
.	.	kIx.	.
</s>
<s>
Mekka	Mekka	k1gFnSc1	Mekka
je	být	k5eAaImIp3nS	být
rodištěm	rodiště	k1gNnSc7	rodiště
Mohameda	Mohamed	k1gMnSc2	Mohamed
<g/>
,	,	kIx,	,
proroka	prorok	k1gMnSc2	prorok
islámu	islám	k1gInSc2	islám
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
nejsvětějším	nejsvětější	k2eAgNnSc7d1	nejsvětější
městem	město	k1gNnSc7	město
islámu	islám	k1gInSc2	islám
<g/>
.	.	kIx.	.
</s>
<s>
Každým	každý	k3xTgInSc7	každý
rokem	rok	k1gInSc7	rok
putuje	putovat	k5eAaImIp3nS	putovat
na	na	k7c4	na
pouť	pouť	k1gFnSc4	pouť
během	během	k7c2	během
hadždže	hadždž	k1gFnSc2	hadždž
(	(	kIx(	(
<g/>
islámský	islámský	k2eAgInSc4d1	islámský
měsíc	měsíc	k1gInSc4	měsíc
dhú	dhú	k?	dhú
<g/>
'	'	kIx"	'
<g/>
l-hidždža	lidždža	k1gMnSc1	l-hidždža
<g/>
)	)	kIx)	)
do	do	k7c2	do
Mekky	Mekka	k1gFnSc2	Mekka
tři	tři	k4xCgInPc4	tři
milióny	milión	k4xCgInPc4	milión
muslimů	muslim	k1gMnPc2	muslim
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
nevěřícím	věřící	k2eNgInSc6d1	nevěřící
je	být	k5eAaImIp3nS	být
vstup	vstup	k1gInSc1	vstup
do	do	k7c2	do
města	město	k1gNnSc2	město
tradičně	tradičně	k6eAd1	tradičně
zakázán	zakázán	k2eAgInSc1d1	zakázán
<g/>
.	.	kIx.	.
</s>
<s>
Mekka	Mekka	k1gFnSc1	Mekka
leží	ležet	k5eAaImIp3nS	ležet
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
přibližně	přibližně	k6eAd1	přibližně
90	[number]	k4	90
km	km	kA	km
od	od	k7c2	od
Rudého	rudý	k2eAgNnSc2d1	Rudé
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
73	[number]	k4	73
km	km	kA	km
od	od	k7c2	od
Džiddy	Džidda	k1gFnSc2	Džidda
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
přímořskou	přímořský	k2eAgFnSc7d1	přímořská
nížinou	nížina	k1gFnSc7	nížina
a	a	k8xC	a
vysočinou	vysočina	k1gFnSc7	vysočina
v	v	k7c6	v
pánvi	pánev	k1gFnSc6	pánev
pouštního	pouštní	k2eAgInSc2d1	pouštní
charakteru	charakter	k1gInSc2	charakter
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgInPc7	dva
horskými	horský	k2eAgInPc7d1	horský
hřebeny	hřeben	k1gInPc7	hřeben
<g/>
.	.	kIx.	.
</s>
<s>
Níže	nízce	k6eAd2	nízce
položená	položený	k2eAgFnSc1d1	položená
část	část	k1gFnSc1	část
města	město	k1gNnSc2	město
kolem	kolem	k7c2	kolem
Káby	kába	k1gFnSc2	kába
je	být	k5eAaImIp3nS	být
staré	starý	k2eAgNnSc1d1	staré
jádro	jádro	k1gNnSc1	jádro
města	město	k1gNnSc2	město
<g/>
;	;	kIx,	;
výše	vysoce	k6eAd2	vysoce
položená	položený	k2eAgFnSc1d1	položená
část	část	k1gFnSc1	část
města	město	k1gNnSc2	město
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
mnoha	mnoho	k4c3	mnoho
kopcům	kopec	k1gInPc3	kopec
a	a	k8xC	a
výšinám	výšina	k1gFnPc3	výšina
na	na	k7c6	na
teritoriu	teritorium	k1gNnSc6	teritorium
města	město	k1gNnSc2	město
bylo	být	k5eAaImAgNnS	být
nutno	nutno	k6eAd1	nutno
postavit	postavit	k5eAaPmF	postavit
větší	veliký	k2eAgNnSc4d2	veliký
množství	množství	k1gNnSc4	množství
silničních	silniční	k2eAgInPc2d1	silniční
tunelů	tunel	k1gInPc2	tunel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
rostl	růst	k5eAaImAgMnS	růst
značně	značně	k6eAd1	značně
počet	počet	k1gInSc4	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
až	až	k9	až
o	o	k7c4	o
200	[number]	k4	200
tisíc	tisíc	k4xCgInPc2	tisíc
ročně	ročně	k6eAd1	ročně
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
spojená	spojený	k2eAgFnSc1d1	spojená
přestavba	přestavba	k1gFnSc1	přestavba
města	město	k1gNnSc2	město
okolo	okolo	k7c2	okolo
svatých	svatá	k1gFnPc2	svatá
míst	místo	k1gNnPc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Celé	celý	k2eAgFnPc1d1	celá
přízemní	přízemní	k2eAgFnPc1d1	přízemní
části	část	k1gFnPc1	část
města	město	k1gNnSc2	město
byly	být	k5eAaImAgFnP	být
zbořeny	zbořen	k2eAgFnPc1d1	zbořena
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
udělaly	udělat	k5eAaPmAgFnP	udělat
místo	místo	k1gNnSc4	místo
pro	pro	k7c4	pro
velkostavby	velkostavba	k1gFnPc4	velkostavba
<g/>
,	,	kIx,	,
především	především	k9	především
hotely	hotel	k1gInPc1	hotel
pro	pro	k7c4	pro
poutníky	poutník	k1gMnPc4	poutník
<g/>
.	.	kIx.	.
</s>
<s>
Jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Velké	velký	k2eAgFnSc2d1	velká
mešity	mešita	k1gFnSc2	mešita
stojí	stát	k5eAaImIp3nS	stát
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
hotelový	hotelový	k2eAgInSc1d1	hotelový
komplex	komplex	k1gInSc1	komplex
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
středu	střed	k1gInSc6	střed
stojí	stát	k5eAaImIp3nS	stát
600	[number]	k4	600
metrů	metr	k1gInPc2	metr
vysoká	vysoký	k2eAgFnSc1d1	vysoká
věž	věž	k1gFnSc1	věž
<g/>
,	,	kIx,	,
Abraj	Abraj	k1gFnSc1	Abraj
Al	ala	k1gFnPc2	ala
Bait	Bait	k2eAgInSc1d1	Bait
Towers	Towers	k1gInSc1	Towers
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
novou	nový	k2eAgFnSc4d1	nová
siluetu	silueta	k1gFnSc4	silueta
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Mekka	Mekka	k1gFnSc1	Mekka
má	mít	k5eAaImIp3nS	mít
pouze	pouze	k6eAd1	pouze
malé	malý	k2eAgNnSc1d1	malé
letiště	letiště	k1gNnSc1	letiště
bez	bez	k7c2	bez
pravidelné	pravidelný	k2eAgFnSc2d1	pravidelná
dopravy	doprava	k1gFnSc2	doprava
do	do	k7c2	do
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Jeddah	Jeddah	k1gMnSc1	Jeddah
Abdulaziz	Abdulaziz	k1gMnSc1	Abdulaziz
International	International	k1gMnSc1	International
Airport	Airport	k1gInSc1	Airport
a	a	k8xC	a
přístav	přístav	k1gInSc1	přístav
v	v	k7c6	v
Džiddě	Džidda	k1gFnSc6	Džidda
jsou	být	k5eAaImIp3nP	být
proto	proto	k8xC	proto
důležitou	důležitý	k2eAgFnSc4d1	důležitá
infrastrukturou	infrastruktura	k1gFnSc7	infrastruktura
pro	pro	k7c4	pro
poutníky	poutník	k1gMnPc4	poutník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
budoucnosti	budoucnost	k1gFnSc6	budoucnost
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
vybudováno	vybudován	k2eAgNnSc1d1	vybudováno
železniční	železniční	k2eAgNnSc1d1	železniční
spojení	spojení	k1gNnSc1	spojení
z	z	k7c2	z
Džiddy	Džidda	k1gFnSc2	Džidda
do	do	k7c2	do
Mekky	Mekka	k1gFnSc2	Mekka
španělským	španělský	k2eAgNnSc7d1	španělské
konsorciem	konsorcium	k1gNnSc7	konsorcium
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
plánovány	plánovat	k5eAaImNgFnP	plánovat
i	i	k9	i
linky	linka	k1gFnPc1	linka
metra	metro	k1gNnSc2	metro
<g/>
.	.	kIx.	.
</s>
<s>
Počátek	počátek	k1gInSc1	počátek
dějin	dějiny	k1gFnPc2	dějiny
Mekky	Mekka	k1gFnSc2	Mekka
není	být	k5eNaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
že	že	k8xS	že
již	již	k6eAd1	již
v	v	k7c6	v
předislámské	předislámský	k2eAgFnSc6d1	předislámská
době	doba	k1gFnSc6	doba
sloužila	sloužit	k5eAaImAgFnS	sloužit
jako	jako	k9	jako
poutní	poutní	k2eAgNnSc4d1	poutní
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
poutníků	poutník	k1gMnPc2	poutník
byla	být	k5eAaImAgFnS	být
Kába	kába	k1gFnSc1	kába
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k9	již
v	v	k7c6	v
předislámských	předislámský	k2eAgFnPc6d1	předislámská
dobách	doba	k1gFnPc6	doba
byla	být	k5eAaImAgFnS	být
budova	budova	k1gFnSc1	budova
arabskými	arabský	k2eAgInPc7d1	arabský
kmeny	kmen	k1gInPc7	kmen
využívána	využívat	k5eAaImNgFnS	využívat
jako	jako	k8xC	jako
svatyně	svatyně	k1gFnSc1	svatyně
k	k	k7c3	k
uctívání	uctívání	k1gNnSc3	uctívání
boha	bůh	k1gMnSc2	bůh
Hubala	Hubal	k1gMnSc2	Hubal
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
předislámské	předislámský	k2eAgFnSc3d1	předislámská
době	doba	k1gFnSc3	doba
sloužila	sloužit	k5eAaImAgFnS	sloužit
Kába	kába	k1gFnSc1	kába
mimo	mimo	k7c4	mimo
uctívání	uctívání	k1gNnSc4	uctívání
Alláha	Alláh	k1gMnSc2	Alláh
uctívání	uctívání	k1gNnSc2	uctívání
staroarabských	staroarabský	k2eAgFnPc2d1	staroarabský
bohyní	bohyně	k1gFnPc2	bohyně
al-Lát	al-Lát	k1gMnSc1	al-Lát
<g/>
,	,	kIx,	,
Manát	Manát	k1gMnSc1	Manát
a	a	k8xC	a
al-Uzzá	al-Uzzat	k5eAaImIp3nS	al-Uzzat
<g/>
.	.	kIx.	.
</s>
<s>
Islám	islám	k1gInSc4	islám
převzal	převzít	k5eAaPmAgInS	převzít
kult	kult	k1gInSc1	kult
uctívání	uctívání	k1gNnSc2	uctívání
černého	černý	k2eAgInSc2d1	černý
meteoritického	meteoritický	k2eAgInSc2d1	meteoritický
kamene	kámen	k1gInSc2	kámen
v	v	k7c6	v
Kábě	kába	k1gFnSc6	kába
z	z	k7c2	z
staroarabského	staroarabský	k2eAgNnSc2d1	staroarabský
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
pouť	pouť	k1gFnSc1	pouť
do	do	k7c2	do
Mekky	Mekka	k1gFnSc2	Mekka
<g/>
.	.	kIx.	.
</s>
<s>
Přikázání	přikázání	k1gNnSc1	přikázání
islámu	islám	k1gInSc2	islám
k	k	k7c3	k
rituální	rituální	k2eAgFnSc3d1	rituální
čistotě	čistota	k1gFnSc3	čistota
(	(	kIx(	(
<g/>
tahára	tahár	k1gMnSc2	tahár
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
původ	původ	k1gInSc4	původ
v	v	k7c6	v
přikázáních	přikázání	k1gNnPc6	přikázání
staroarabského	staroarabský	k2eAgNnSc2d1	staroarabský
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Značný	značný	k2eAgInSc4d1	značný
počet	počet	k1gInSc4	počet
poutníků	poutník	k1gMnPc2	poutník
byl	být	k5eAaImAgMnS	být
příčinou	příčina	k1gFnSc7	příčina
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Mekka	Mekka	k1gFnSc1	Mekka
stala	stát	k5eAaPmAgFnS	stát
obchodním	obchodní	k2eAgInSc7d1	obchodní
centrem	centr	k1gInSc7	centr
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
sama	sám	k3xTgFnSc1	sám
málo	málo	k6eAd1	málo
produkovala	produkovat	k5eAaImAgFnS	produkovat
a	a	k8xC	a
neležela	ležet	k5eNaImAgFnS	ležet
na	na	k7c6	na
strategickém	strategický	k2eAgNnSc6d1	strategické
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Mekka	Mekka	k1gFnSc1	Mekka
ležela	ležet	k5eAaImAgFnS	ležet
v	v	k7c6	v
suchém	suchý	k2eAgNnSc6d1	suché
a	a	k8xC	a
neúrodném	úrodný	k2eNgNnSc6d1	neúrodné
údolí	údolí	k1gNnSc6	údolí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
předislámské	předislámský	k2eAgFnSc6d1	předislámská
době	doba	k1gFnSc6	doba
byla	být	k5eAaImAgFnS	být
zcela	zcela	k6eAd1	zcela
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c6	na
potravinách	potravina	k1gFnPc6	potravina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
produkovaly	produkovat	k5eAaImAgFnP	produkovat
v	v	k7c6	v
Ta	ten	k3xDgFnSc1	ten
<g/>
'	'	kIx"	'
<g/>
if	if	k?	if
<g/>
.	.	kIx.	.
</s>
<s>
Určití	určitý	k2eAgMnPc1d1	určitý
historikové	historik	k1gMnPc1	historik
jsou	být	k5eAaImIp3nP	být
ale	ale	k9	ale
názoru	názor	k1gInSc3	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
Mekka	Mekka	k1gFnSc1	Mekka
získala	získat	k5eAaPmAgFnS	získat
své	svůj	k3xOyFgNnSc4	svůj
postavení	postavení	k1gNnSc4	postavení
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
poloze	poloha	k1gFnSc3	poloha
<g/>
.	.	kIx.	.
</s>
<s>
Ležela	ležet	k5eAaImAgFnS	ležet
na	na	k7c6	na
dvouměsíční	dvouměsíční	k2eAgFnSc6d1	dvouměsíční
cestě	cesta	k1gFnSc6	cesta
mezi	mezi	k7c7	mezi
Byzancí	Byzanc	k1gFnSc7	Byzanc
a	a	k8xC	a
jemenským	jemenský	k2eAgInSc7d1	jemenský
sabejským	sabejský	k2eAgInSc7d1	sabejský
<g/>
,	,	kIx,	,
ma	ma	k?	ma
<g/>
'	'	kIx"	'
<g/>
inskýn	inskýn	k1gMnSc1	inskýn
<g/>
,	,	kIx,	,
qatabanským	qatabanský	k2eAgMnSc7d1	qatabanský
<g/>
,	,	kIx,	,
asuánským	asuánský	k2eAgNnSc7d1	asuánský
a	a	k8xC	a
hadramautským	hadramautský	k2eAgNnSc7d1	hadramautský
královstvím	království	k1gNnSc7	království
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
úzké	úzký	k2eAgInPc4d1	úzký
obchodní	obchodní	k2eAgInPc4d1	obchodní
kontakty	kontakt	k1gInPc4	kontakt
s	s	k7c7	s
Indií	Indie	k1gFnSc7	Indie
a	a	k8xC	a
východní	východní	k2eAgFnSc7d1	východní
Afrikou	Afrika	k1gFnSc7	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Jakým	jaký	k3yQgInSc7	jaký
způsobem	způsob	k1gInSc7	způsob
Mekka	Mekka	k1gFnSc1	Mekka
profitovala	profitovat	k5eAaBmAgFnS	profitovat
z	z	k7c2	z
obchodu	obchod	k1gInSc2	obchod
s	s	k7c7	s
pryskyřičným	pryskyřičný	k2eAgNnSc7d1	pryskyřičné
kadidlem	kadidlo	k1gNnSc7	kadidlo
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
sporné	sporný	k2eAgNnSc1d1	sporné
<g/>
.	.	kIx.	.
</s>
<s>
Dosud	dosud	k6eAd1	dosud
není	být	k5eNaImIp3nS	být
jednotný	jednotný	k2eAgInSc4d1	jednotný
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
Mekka	Mekka	k1gFnSc1	Mekka
ležela	ležet	k5eAaImAgFnS	ležet
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
z	z	k7c2	z
jihu	jih	k1gInSc2	jih
do	do	k7c2	do
Egypta	Egypt	k1gInSc2	Egypt
na	na	k7c6	na
kadidlové	kadidlový	k2eAgFnSc6d1	Kadidlová
stezce	stezka	k1gFnSc6	stezka
<g/>
.	.	kIx.	.
</s>
<s>
Potvrzený	potvrzený	k2eAgInSc1d1	potvrzený
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
údaj	údaj	k1gInSc1	údaj
pro	pro	k7c4	pro
Medinu	Medina	k1gFnSc4	Medina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
převzali	převzít	k5eAaPmAgMnP	převzít
Kurajšovci	Kurajšovec	k1gMnPc1	Kurajšovec
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
Mekkou	Mekka	k1gFnSc7	Mekka
<g/>
.	.	kIx.	.
</s>
<s>
Prosadili	prosadit	k5eAaPmAgMnP	prosadit
se	se	k3xPyFc4	se
jako	jako	k9	jako
úspěšní	úspěšný	k2eAgMnPc1d1	úspěšný
obchodníci	obchodník	k1gMnPc1	obchodník
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
díky	díky	k7c3	díky
rivalitě	rivalita	k1gFnSc3	rivalita
uvnitř	uvnitř	k7c2	uvnitř
kmene	kmen	k1gInSc2	kmen
se	se	k3xPyFc4	se
rozpadli	rozpadnout	k5eAaPmAgMnP	rozpadnout
do	do	k7c2	do
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
frakcí	frakce	k1gFnPc2	frakce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
630	[number]	k4	630
si	se	k3xPyFc3	se
město	město	k1gNnSc4	město
podmanil	podmanit	k5eAaPmAgMnS	podmanit
Mohamed	Mohamed	k1gMnSc1	Mohamed
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
od	od	k7c2	od
roku	rok	k1gInSc2	rok
623	[number]	k4	623
<g/>
,	,	kIx,	,
od	od	k7c2	od
svého	svůj	k3xOyFgInSc2	svůj
odchodu	odchod	k1gInSc2	odchod
do	do	k7c2	do
Medíny	Medína	k1gFnSc2	Medína
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgMnS	mít
několik	několik	k4yIc4	několik
vojenských	vojenský	k2eAgInPc2d1	vojenský
konfliktů	konflikt	k1gInPc2	konflikt
s	s	k7c7	s
Mekkou	Mekka	k1gFnSc7	Mekka
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
příchod	příchod	k1gInSc1	příchod
do	do	k7c2	do
Mekky	Mekka	k1gFnSc2	Mekka
ale	ale	k8xC	ale
neproběhl	proběhnout	k5eNaPmAgMnS	proběhnout
vojensky	vojensky	k6eAd1	vojensky
a	a	k8xC	a
proběhl	proběhnout	k5eAaPmAgMnS	proběhnout
bez	bez	k7c2	bez
násilí	násilí	k1gNnSc2	násilí
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c6	na
základě	základ	k1gInSc6	základ
předchozího	předchozí	k2eAgInSc2d1	předchozí
roku	rok	k1gInSc2	rok
uzavřené	uzavřený	k2eAgFnSc2d1	uzavřená
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
doby	doba	k1gFnSc2	doba
proroka	prorok	k1gMnSc2	prorok
Mohameda	Mohamed	k1gMnSc2	Mohamed
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
několikrát	několikrát	k6eAd1	několikrát
obléháno	obléhat	k5eAaImNgNnS	obléhat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
město	město	k1gNnSc4	město
obsadili	obsadit	k5eAaPmAgMnP	obsadit
Egypťané	Egypťan	k1gMnPc1	Egypťan
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1517	[number]	k4	1517
byla	být	k5eAaImAgFnS	být
Mekka	Mekka	k1gFnSc1	Mekka
pod	pod	k7c7	pod
nadvládou	nadvláda	k1gFnSc7	nadvláda
Osmanů	Osman	k1gMnPc2	Osman
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
poprvé	poprvé	k6eAd1	poprvé
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
kolem	kolem	k7c2	kolem
Káby	kába	k1gFnSc2	kába
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1517	[number]	k4	1517
vládli	vládnout	k5eAaImAgMnP	vládnout
Osmanové	Osmanové	k?	Osmanové
současně	současně	k6eAd1	současně
jako	jako	k9	jako
kalifové	kalif	k1gMnPc1	kalif
<g/>
.	.	kIx.	.
</s>
<s>
Šarif	Šarif	k1gMnSc1	Šarif
Husajn	Husajn	k1gMnSc1	Husajn
ibn	ibn	k?	ibn
Alí	Alí	k1gFnSc2	Alí
al-Hášimí	al-Hášimí	k1gNnSc2	al-Hášimí
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
později	pozdě	k6eAd2	pozdě
králem	král	k1gMnSc7	král
Hidžázu	Hidžáz	k1gInSc2	Hidžáz
<g/>
,	,	kIx,	,
dostal	dostat	k5eAaPmAgMnS	dostat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1916	[number]	k4	1916
Mekku	Mekka	k1gFnSc4	Mekka
pod	pod	k7c4	pod
tureckou	turecký	k2eAgFnSc4d1	turecká
nadvládu	nadvláda	k1gFnSc4	nadvláda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
obsadil	obsadit	k5eAaPmAgMnS	obsadit
Abdul	Abdul	k1gMnSc1	Abdul
al-Azíz	al-Azíz	k1gMnSc1	al-Azíz
al-Saud	al-Saud	k1gMnSc1	al-Saud
<g/>
,	,	kIx,	,
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
sultán	sultán	k1gMnSc1	sultán
Nadždu	Nadžd	k1gInSc2	Nadžd
<g/>
,	,	kIx,	,
Mekku	Mekka	k1gFnSc4	Mekka
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitějším	důležitý	k2eAgNnSc7d3	nejdůležitější
poutním	poutní	k2eAgNnSc7d1	poutní
místem	místo	k1gNnSc7	místo
je	být	k5eAaImIp3nS	být
Kába	kába	k1gFnSc1	kába
<g/>
,	,	kIx,	,
kostkovitá	kostkovitý	k2eAgFnSc1d1	kostkovitý
budova	budova	k1gFnSc1	budova
bez	bez	k7c2	bez
oken	okno	k1gNnPc2	okno
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
dle	dle	k7c2	dle
islámského	islámský	k2eAgNnSc2d1	islámské
pojetí	pojetí	k1gNnSc2	pojetí
poprvé	poprvé	k6eAd1	poprvé
vybudována	vybudovat	k5eAaPmNgFnS	vybudovat
prorokem	prorok	k1gMnSc7	prorok
Adamem	Adam	k1gMnSc7	Adam
a	a	k8xC	a
poté	poté	k6eAd1	poté
znovu	znovu	k6eAd1	znovu
prorokem	prorok	k1gMnSc7	prorok
Abrahámem	Abrahám	k1gMnSc7	Abrahám
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
historicky	historicky	k6eAd1	historicky
potvrzeno	potvrdit	k5eAaPmNgNnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Kába	kába	k1gFnSc1	kába
již	již	k6eAd1	již
v	v	k7c6	v
předislámských	předislámský	k2eAgFnPc6d1	předislámská
dobách	doba	k1gFnPc6	doba
byla	být	k5eAaImAgFnS	být
centrální	centrální	k2eAgFnSc7d1	centrální
svatyní	svatyně	k1gFnSc7	svatyně
arabských	arabský	k2eAgInPc2d1	arabský
kmenů	kmen	k1gInPc2	kmen
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jejím	její	k3xOp3gInSc6	její
jihozápadním	jihozápadní	k2eAgInSc6d1	jihozápadní
rohu	roh	k1gInSc6	roh
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
černý	černý	k2eAgInSc1d1	černý
kámen	kámen	k1gInSc1	kámen
-	-	kIx~	-
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
meteorit	meteorit	k1gInSc4	meteorit
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
dle	dle	k7c2	dle
pojetí	pojetí	k1gNnSc2	pojetí
proroka	prorok	k1gMnSc2	prorok
Abrahama	Abraham	k1gMnSc2	Abraham
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
archanděla	archanděl	k1gMnSc2	archanděl
Gabriela	Gabriel	k1gMnSc2	Gabriel
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
islámských	islámský	k2eAgInPc2d1	islámský
pramenů	pramen	k1gInPc2	pramen
započalo	započnout	k5eAaPmAgNnS	započnout
osídlení	osídlení	k1gNnSc1	osídlení
Mekky	Mekka	k1gFnSc2	Mekka
<g/>
,	,	kIx,	,
když	když	k8xS	když
otec	otec	k1gMnSc1	otec
kmene	kmen	k1gInSc2	kmen
Abraham	Abraham	k1gMnSc1	Abraham
na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
místo	místo	k1gNnSc4	místo
přivedl	přivést	k5eAaPmAgMnS	přivést
svoji	svůj	k3xOyFgFnSc4	svůj
ženu	žena	k1gFnSc4	žena
Hagar	Hagara	k1gFnPc2	Hagara
a	a	k8xC	a
jejich	jejich	k3xOp3gMnSc2	jejich
syna	syn	k1gMnSc2	syn
Izmaela	Izmael	k1gMnSc2	Izmael
<g/>
.	.	kIx.	.
</s>
<s>
Prosil	prosít	k5eAaPmAgMnS	prosít
boha	bůh	k1gMnSc4	bůh
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zaopatřil	zaopatřit	k5eAaPmAgMnS	zaopatřit
jeho	jeho	k3xOp3gFnSc4	jeho
rodinu	rodina	k1gFnSc4	rodina
a	a	k8xC	a
srdce	srdce	k1gNnSc1	srdce
lidí	člověk	k1gMnPc2	člověk
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
přiblížila	přiblížit	k5eAaPmAgFnS	přiblížit
<g/>
:	:	kIx,	:
Pane	Pan	k1gMnSc5	Pan
můj	můj	k1gMnSc5	můj
<g/>
,	,	kIx,	,
usadil	usadit	k5eAaPmAgMnS	usadit
jsem	být	k5eAaImIp1nS	být
část	část	k1gFnSc4	část
potomstva	potomstvo	k1gNnSc2	potomstvo
svého	své	k1gNnSc2	své
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
bez	bez	k7c2	bez
polí	pole	k1gFnPc2	pole
obilných	obilný	k2eAgFnPc2d1	obilná
poblíže	poblíže	k7c2	poblíže
chrámu	chrám	k1gInSc2	chrám
Tvého	tvůj	k3xOp2gNnSc2	tvůj
posvátného	posvátný	k2eAgNnSc2d1	posvátné
<g/>
,	,	kIx,	,
Pane	Pan	k1gMnSc5	Pan
náš	náš	k3xOp1gInSc1	náš
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zde	zde	k6eAd1	zde
modlitbu	modlitba	k1gFnSc4	modlitba
konali	konat	k5eAaImAgMnP	konat
<g/>
.	.	kIx.	.
</s>
<s>
Učiň	učinit	k5eAaPmRp2nS	učinit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
srdce	srdce	k1gNnSc1	srdce
lidí	člověk	k1gMnPc2	člověk
se	se	k3xPyFc4	se
k	k	k7c3	k
nim	on	k3xPp3gMnPc3	on
přiblížila	přiblížit	k5eAaPmAgFnS	přiblížit
<g/>
,	,	kIx,	,
a	a	k8xC	a
uštědři	uštědřit	k5eAaPmRp2nS	uštědřit
jim	on	k3xPp3gMnPc3	on
plody	plod	k1gInPc4	plod
co	co	k8xS	co
obživu	obživa	k1gFnSc4	obživa
-	-	kIx~	-
snad	snad	k9	snad
budou	být	k5eAaImBp3nP	být
vděční	vděčný	k2eAgMnPc1d1	vděčný
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
Korán	korán	k1gInSc1	korán
<g/>
,	,	kIx,	,
súra	súra	k1gFnSc1	súra
14	[number]	k4	14
<g/>
,	,	kIx,	,
verš	verš	k1gInSc1	verš
37	[number]	k4	37
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
sága	sága	k1gFnSc1	sága
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Když	když	k8xS	když
docházely	docházet	k5eAaImAgFnP	docházet
zásoby	zásoba	k1gFnPc1	zásoba
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
běžela	běžet	k5eAaImAgFnS	běžet
Hagar	Hagar	k1gInSc4	Hagar
celkem	celkem	k6eAd1	celkem
sedmkrát	sedmkrát	k6eAd1	sedmkrát
mezi	mezi	k7c4	mezi
pahorky	pahorek	k1gInPc4	pahorek
Safá	Safá	k1gFnSc1	Safá
a	a	k8xC	a
Marwá	Marwá	k1gFnSc1	Marwá
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
hledala	hledat	k5eAaImAgFnS	hledat
vodu	voda	k1gFnSc4	voda
a	a	k8xC	a
vyhlížela	vyhlížet	k5eAaImAgFnS	vyhlížet
karavany	karavana	k1gFnPc4	karavana
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
vrátila	vrátit	k5eAaPmAgFnS	vrátit
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
stanu	stan	k1gInSc2	stan
<g/>
,	,	kIx,	,
našla	najít	k5eAaPmAgFnS	najít
vedle	vedle	k7c2	vedle
syna	syn	k1gMnSc2	syn
Izmaela	Izmael	k1gMnSc2	Izmael
zurčící	zurčící	k2eAgInSc4d1	zurčící
pramen	pramen	k1gInSc4	pramen
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
existuje	existovat	k5eAaImIp3nS	existovat
dodnes	dodnes	k6eAd1	dodnes
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
známý	známý	k2eAgMnSc1d1	známý
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Zamzam	Zamzam	k1gInSc1	Zamzam
<g/>
.	.	kIx.	.
</s>
<s>
Karavany	karavan	k1gInPc1	karavan
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
přichází	přicházet	k5eAaImIp3nS	přicházet
z	z	k7c2	z
Jemenu	Jemen	k1gInSc2	Jemen
<g/>
,	,	kIx,	,
věděly	vědět	k5eAaImAgFnP	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
údolí	údolí	k1gNnSc2	údolí
normálně	normálně	k6eAd1	normálně
voda	voda	k1gFnSc1	voda
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
a	a	k8xC	a
divily	divit	k5eAaImAgFnP	divit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
když	když	k8xS	když
viděly	vidět	k5eAaImAgInP	vidět
nad	nad	k7c7	nad
údolím	údolí	k1gNnSc7	údolí
kroužit	kroužit	k5eAaImF	kroužit
ptáky	pták	k1gMnPc4	pták
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
známka	známka	k1gFnSc1	známka
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
voda	voda	k1gFnSc1	voda
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
hledali	hledat	k5eAaImAgMnP	hledat
údolí	údolí	k1gNnPc4	údolí
a	a	k8xC	a
našli	najít	k5eAaPmAgMnP	najít
Hagar	Hagar	k1gInSc4	Hagar
a	a	k8xC	a
jejího	její	k3xOp3gMnSc2	její
syna	syn	k1gMnSc2	syn
Izmaela	Izmael	k1gMnSc2	Izmael
<g/>
.	.	kIx.	.
</s>
<s>
Prosili	prosit	k5eAaImAgMnP	prosit
o	o	k7c4	o
svolení	svolení	k1gNnSc4	svolení
usadit	usadit	k5eAaPmF	usadit
se	se	k3xPyFc4	se
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
osídlení	osídlení	k1gNnSc1	osídlení
Mekky	Mekka	k1gFnSc2	Mekka
<g/>
.	.	kIx.	.
</s>
<s>
Izmael	Izmael	k1gMnSc1	Izmael
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
oženil	oženit	k5eAaPmAgMnS	oženit
se	s	k7c7	s
ženou	žena	k1gFnSc7	žena
z	z	k7c2	z
jemenského	jemenský	k2eAgInSc2d1	jemenský
kmene	kmen	k1gInSc2	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
Abrahám	Abrahám	k1gMnSc1	Abrahám
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Mekky	Mekka	k1gFnSc2	Mekka
<g/>
,	,	kIx,	,
zřídil	zřídit	k5eAaPmAgMnS	zřídit
společně	společně	k6eAd1	společně
s	s	k7c7	s
Izmaelem	Izmael	k1gInSc7	Izmael
Kábu	kába	k1gFnSc4	kába
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
poutním	poutní	k2eAgNnSc7d1	poutní
místem	místo	k1gNnSc7	místo
pro	pro	k7c4	pro
milióny	milión	k4xCgInPc1	milión
lidí	člověk	k1gMnPc2	člověk
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Běh	běh	k1gInSc1	běh
mezi	mezi	k7c7	mezi
pahorky	pahorek	k1gInPc7	pahorek
Safa	Safum	k1gNnSc2	Safum
a	a	k8xC	a
Marwa	Marwum	k1gNnSc2	Marwum
je	být	k5eAaImIp3nS	být
nedílnou	dílný	k2eNgFnSc7d1	nedílná
součástí	součást	k1gFnSc7	součást
islámské	islámský	k2eAgFnSc2d1	islámská
poutě	pouť	k1gFnSc2	pouť
do	do	k7c2	do
Mekky	Mekka	k1gFnSc2	Mekka
<g/>
.	.	kIx.	.
</s>
<s>
Sága	sága	k1gFnSc1	sága
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
historickou	historický	k2eAgFnSc4d1	historická
věrohodnost	věrohodnost	k1gFnSc4	věrohodnost
nejde	jít	k5eNaImIp3nS	jít
ověřit	ověřit	k5eAaPmF	ověřit
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
etiologickou	etiologický	k2eAgFnSc4d1	etiologická
funkci	funkce	k1gFnSc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Nemuslimové	Nemuslim	k1gMnPc1	Nemuslim
mají	mít	k5eAaImIp3nP	mít
vstup	vstup	k1gInSc4	vstup
do	do	k7c2	do
Mekky	Mekka	k1gFnSc2	Mekka
zakázán	zakázat	k5eAaPmNgInS	zakázat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
neuposlechnutí	neuposlechnutí	k1gNnSc2	neuposlechnutí
hrozí	hrozit	k5eAaImIp3nS	hrozit
zadrženému	zadržený	k2eAgNnSc3d1	zadržené
vyhoštění	vyhoštění	k1gNnSc3	vyhoštění
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
i	i	k9	i
vězení	vězení	k1gNnSc2	vězení
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
původní	původní	k2eAgFnSc2d1	původní
obavy	obava	k1gFnSc2	obava
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
předislámský	předislámský	k2eAgInSc1d1	předislámský
polyteistický	polyteistický	k2eAgInSc1d1	polyteistický
kult	kult	k1gInSc1	kult
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
u	u	k7c2	u
Káby	kába	k1gFnSc2	kába
znovu	znovu	k6eAd1	znovu
oživen	oživen	k2eAgInSc1d1	oživen
<g/>
,	,	kIx,	,
vzešel	vzejít	k5eAaPmAgInS	vzejít
příkaz	příkaz	k1gInSc4	příkaz
v	v	k7c6	v
Koránu	korán	k1gInSc6	korán
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
tato	tento	k3xDgFnSc1	tento
svatá	svatá	k1gFnSc1	svatá
místa	místo	k1gNnSc2	místo
neměla	mít	k5eNaImAgFnS	mít
být	být	k5eAaImF	být
přístupná	přístupný	k2eAgFnSc1d1	přístupná
nevěřícím	věřící	k2eNgMnPc3d1	nevěřící
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
saudskoarabských	saudskoarabský	k2eAgInPc6d1	saudskoarabský
průkazech	průkaz	k1gInPc6	průkaz
totožnosti	totožnost	k1gFnSc2	totožnost
je	být	k5eAaImIp3nS	být
uvedeno	uveden	k2eAgNnSc1d1	uvedeno
vyznání	vyznání	k1gNnSc1	vyznání
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
může	moct	k5eAaImIp3nS	moct
v	v	k7c6	v
případě	případ	k1gInSc6	případ
pochybností	pochybnost	k1gFnPc2	pochybnost
policie	policie	k1gFnSc2	policie
zkontrolovat	zkontrolovat	k5eAaPmF	zkontrolovat
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
žádosti	žádost	k1gFnSc6	žádost
o	o	k7c4	o
vízum	vízum	k1gNnSc4	vízum
se	se	k3xPyFc4	se
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
vyplnit	vyplnit	k5eAaPmF	vyplnit
vyznání	vyznání	k1gNnSc4	vyznání
žadatele	žadatel	k1gMnSc2	žadatel
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
muslimů	muslim	k1gMnPc2	muslim
je	být	k5eAaImIp3nS	být
vyžadováno	vyžadován	k2eAgNnSc4d1	vyžadováno
potvrzení	potvrzení	k1gNnSc4	potvrzení
místní	místní	k2eAgFnSc2d1	místní
autority	autorita	k1gFnSc2	autorita
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
i	i	k9	i
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
některým	některý	k3yIgInPc3	některý
nemuslimům	nemuslim	k1gInPc3	nemuslim
daří	dařit	k5eAaImIp3nS	dařit
do	do	k7c2	do
Mekky	Mekka	k1gFnSc2	Mekka
dostat	dostat	k5eAaPmF	dostat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
několika	několik	k4yIc7	několik
evropských	evropský	k2eAgFnPc6d1	Evropská
cestovatelům	cestovatel	k1gMnPc3	cestovatel
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
byli	být	k5eAaImAgMnP	být
převlečení	převlečený	k2eAgMnPc1d1	převlečený
jako	jako	k9	jako
muslimové	muslim	k1gMnPc1	muslim
<g/>
,	,	kIx,	,
navštívit	navštívit	k5eAaPmF	navštívit
Mekku	Mekka	k1gFnSc4	Mekka
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nim	on	k3xPp3gMnPc3	on
patřil	patřit	k5eAaImAgInS	patřit
zotročený	zotročený	k2eAgMnSc1d1	zotročený
sluha	sluha	k1gMnSc1	sluha
Hans	Hans	k1gMnSc1	Hans
Wild	Wild	k1gMnSc1	Wild
(	(	kIx(	(
<g/>
mezi	mezi	k7c4	mezi
roky	rok	k1gInPc4	rok
1607	[number]	k4	1607
a	a	k8xC	a
1609	[number]	k4	1609
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
vědec	vědec	k1gMnSc1	vědec
a	a	k8xC	a
cestovatel	cestovatel	k1gMnSc1	cestovatel
Ulrich	Ulrich	k1gMnSc1	Ulrich
Jasper	Jasper	k1gMnSc1	Jasper
Seetzen	Seetzen	k2eAgMnSc1d1	Seetzen
(	(	kIx(	(
<g/>
1809	[number]	k4	1809
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
Basler	Basler	k1gMnSc1	Basler
Jean	Jean	k1gMnSc1	Jean
Louis	Louis	k1gMnSc1	Louis
Burckhardt	Burckhardt	k1gMnSc1	Burckhardt
(	(	kIx(	(
<g/>
1814	[number]	k4	1814
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
také	také	k6eAd1	také
známý	známý	k2eAgInSc1d1	známý
objevem	objev	k1gInSc7	objev
starověkého	starověký	k2eAgNnSc2d1	starověké
nabatského	nabatský	k2eAgNnSc2d1	nabatský
města	město	k1gNnSc2	město
Petra	Petr	k1gMnSc2	Petr
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
orientalista	orientalista	k1gMnSc1	orientalista
a	a	k8xC	a
objevitel	objevitel	k1gMnSc1	objevitel
Heinrich	Heinrich	k1gMnSc1	Heinrich
von	von	k1gInSc4	von
Maltzan	Maltzan	k1gMnSc1	Maltzan
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1860	[number]	k4	1860
převlečený	převlečený	k2eAgInSc4d1	převlečený
za	za	k7c4	za
muslima	muslim	k1gMnSc4	muslim
díky	díky	k7c3	díky
pasu	pas	k1gInSc3	pas
získanému	získaný	k2eAgInSc3d1	získaný
pomocí	pomocí	k7c2	pomocí
úplatku	úplatek	k1gInSc2	úplatek
od	od	k7c2	od
jistého	jistý	k2eAgMnSc2d1	jistý
Araba	Arab	k1gMnSc2	Arab
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Mekku	Mekka	k1gFnSc4	Mekka
<g/>
,	,	kIx,	,
o	o	k7c6	o
čem	co	k3yInSc6	co
píše	psát	k5eAaImIp3nS	psát
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
poprvé	poprvé	k6eAd1	poprvé
vydané	vydaný	k2eAgFnPc4d1	vydaná
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1865	[number]	k4	1865
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1853	[number]	k4	1853
popsal	popsat	k5eAaPmAgMnS	popsat
anglický	anglický	k2eAgMnSc1d1	anglický
dobrodruh	dobrodruh	k1gMnSc1	dobrodruh
Richard	Richard	k1gMnSc1	Richard
Francis	Francis	k1gFnSc2	Francis
Burton	Burton	k1gInSc1	Burton
detailní	detailní	k2eAgInSc1d1	detailní
popis	popis	k1gInSc1	popis
Mekky	Mekka	k1gFnSc2	Mekka
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
navštívil	navštívit	k5eAaPmAgMnS	navštívit
převlečený	převlečený	k2eAgMnSc1d1	převlečený
za	za	k7c4	za
derviše	derviš	k1gMnPc4	derviš
a	a	k8xC	a
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
se	se	k3xPyFc4	se
důležitých	důležitý	k2eAgFnPc2d1	důležitá
náboženských	náboženský	k2eAgFnPc2d1	náboženská
ceremonií	ceremonie	k1gFnPc2	ceremonie
<g/>
.	.	kIx.	.
</s>
<s>
Nizozemský	nizozemský	k2eAgMnSc1d1	nizozemský
vědec	vědec	k1gMnSc1	vědec
o	o	k7c6	o
islámu	islám	k1gInSc6	islám
Christiaan	Christiaan	k1gMnSc1	Christiaan
Snouck	Snouck	k1gMnSc1	Snouck
Hurgronje	Hurgronj	k1gMnSc4	Hurgronj
také	také	k9	také
navštívil	navštívit	k5eAaPmAgInS	navštívit
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
pod	pod	k7c7	pod
falešnou	falešný	k2eAgFnSc7d1	falešná
identitou	identita	k1gFnSc7	identita
Mekku	Mekka	k1gFnSc4	Mekka
<g/>
.	.	kIx.	.
</s>
<s>
Výsledek	výsledek	k1gInSc1	výsledek
jeho	jeho	k3xOp3gFnSc2	jeho
cesty	cesta	k1gFnSc2	cesta
bylo	být	k5eAaImAgNnS	být
dvojdílné	dvojdílný	k2eAgNnSc1d1	dvojdílné
dílo	dílo	k1gNnSc1	dílo
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Mekka	Mekka	k1gFnSc1	Mekka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1889	[number]	k4	1889
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
textové	textový	k2eAgFnSc6d1	textová
o	o	k7c4	o
obrazové	obrazový	k2eAgFnPc4d1	obrazová
části	část	k1gFnPc4	část
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
se	se	k3xPyFc4	se
do	do	k7c2	do
Mekky	Mekka	k1gFnSc2	Mekka
dostali	dostat	k5eAaPmAgMnP	dostat
členové	člen	k1gMnPc1	člen
francouzské	francouzský	k2eAgFnSc2d1	francouzská
Groupe	Group	k1gInSc5	Group
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Intervention	Intervention	k1gInSc1	Intervention
de	de	k?	de
la	la	k1gNnSc2	la
Gendarmerie	Gendarmerie	k1gFnSc1	Gendarmerie
Nationale	Nationale	k1gFnSc1	Nationale
(	(	kIx(	(
<g/>
Skupina	skupina	k1gFnSc1	skupina
speciálního	speciální	k2eAgNnSc2d1	speciální
určení	určení	k1gNnSc2	určení
francouzské	francouzský	k2eAgFnSc2d1	francouzská
policie	policie	k1gFnSc2	policie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
na	na	k7c6	na
základě	základ	k1gInSc6	základ
výjimky	výjimka	k1gFnSc2	výjimka
od	od	k7c2	od
saúdské	saúdský	k2eAgFnSc2d1	Saúdská
armády	armáda	k1gFnSc2	armáda
podíleli	podílet	k5eAaImAgMnP	podílet
na	na	k7c4	na
potlačení	potlačení	k1gNnSc4	potlačení
obsazení	obsazení	k1gNnSc2	obsazení
Velké	velký	k2eAgFnSc2d1	velká
mešity	mešita	k1gFnSc2	mešita
v	v	k7c6	v
Mekce	Mekka	k1gFnSc6	Mekka
<g/>
.	.	kIx.	.
</s>
