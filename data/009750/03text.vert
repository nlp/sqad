<p>
<s>
Muhammad	Muhammad	k1gInSc1	Muhammad
Nadžíb	Nadžíba	k1gFnPc2	Nadžíba
<g/>
,	,	kIx,	,
arabsky	arabsky	k6eAd1	arabsky
ج	ج	k?	ج
ن	ن	k?	ن
(	(	kIx(	(
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1901	[number]	k4	1901
<g/>
,	,	kIx,	,
Chartúm	Chartúm	k1gInSc1	Chartúm
–	–	k?	–
29	[number]	k4	29
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1984	[number]	k4	1984
<g/>
,	,	kIx,	,
Káhira	Káhira	k1gFnSc1	Káhira
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgMnSc7	první
egyptským	egyptský	k2eAgMnSc7d1	egyptský
prezidentem	prezident	k1gMnSc7	prezident
<g/>
,	,	kIx,	,
kterým	který	k3yQgMnSc7	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
po	po	k7c6	po
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
republiky	republika	k1gFnSc2	republika
18	[number]	k4	18
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1953	[number]	k4	1953
a	a	k8xC	a
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
setrval	setrvat	k5eAaPmAgMnS	setrvat
do	do	k7c2	do
14	[number]	k4	14
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1954	[number]	k4	1954
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
Gamálem	Gamál	k1gMnSc7	Gamál
Násirem	Násir	k1gMnSc7	Násir
byl	být	k5eAaImAgMnS	být
hlavním	hlavní	k2eAgMnSc7d1	hlavní
vůdcem	vůdce	k1gMnSc7	vůdce
egyptské	egyptský	k2eAgFnSc2d1	egyptská
revoluce	revoluce	k1gFnSc2	revoluce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
ukončení	ukončení	k1gNnSc4	ukončení
vlády	vláda	k1gFnSc2	vláda
dynastie	dynastie	k1gFnSc2	dynastie
Muhammada	Muhammada	k1gFnSc1	Muhammada
Alího	Alí	k1gMnSc2	Alí
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
a	a	k8xC	a
Súdánu	Súdán	k1gInSc6	Súdán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Mládí	mládí	k1gNnSc1	mládí
==	==	k?	==
</s>
</p>
<p>
<s>
Muhammad	Muhammad	k1gInSc1	Muhammad
Nadžíb	Nadžíb	k1gInSc1	Nadžíb
(	(	kIx(	(
<g/>
celým	celý	k2eAgNnSc7d1	celé
jménem	jméno	k1gNnSc7	jméno
Muhammad	Muhammad	k1gInSc1	Muhammad
Nadžíb	Nadžíb	k1gInSc1	Nadžíb
Yousef	Yousef	k1gInSc1	Yousef
Qotp	Qotp	k1gInSc1	Qotp
Elkashlan	Elkashlan	k1gInSc1	Elkashlan
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgInS	narodit
v	v	k7c6	v
súdánském	súdánský	k2eAgInSc6d1	súdánský
Chartúmu	Chartúm	k1gInSc6	Chartúm
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byl	být	k5eAaImAgInS	být
Súdán	Súdán	k1gInSc1	Súdán
spojený	spojený	k2eAgInSc1d1	spojený
s	s	k7c7	s
Egyptem	Egypt	k1gInSc7	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Muhammad	Muhammad	k1gInSc1	Muhammad
byl	být	k5eAaImAgInS	být
nejstarším	starý	k2eAgInSc7d3	nejstarší
z	z	k7c2	z
devíti	devět	k4xCc3	devět
dětí	dítě	k1gFnPc2	dítě
Egypťana	Egypťan	k1gMnSc2	Egypťan
Youssefa	Youssef	k1gMnSc2	Youssef
Nadžíba	Nadžíb	k1gMnSc2	Nadžíb
a	a	k8xC	a
súdánské	súdánský	k2eAgFnSc2d1	súdánská
matky	matka	k1gFnSc2	matka
Zohry	Zohra	k1gFnSc2	Zohra
Ahmed	Ahmed	k1gMnSc1	Ahmed
Othman	Othman	k1gMnSc1	Othman
<g/>
.	.	kIx.	.
</s>
<s>
Muhammad	Muhammad	k1gInSc1	Muhammad
pocházel	pocházet	k5eAaImAgInS	pocházet
z	z	k7c2	z
vojenské	vojenský	k2eAgFnSc2d1	vojenská
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
vojenským	vojenský	k2eAgMnSc7d1	vojenský
důstojníkem	důstojník	k1gMnSc7	důstojník
sloužícím	sloužící	k1gMnSc7	sloužící
v	v	k7c6	v
egyptské	egyptský	k2eAgFnSc6d1	egyptská
armádě	armáda	k1gFnSc6	armáda
v	v	k7c6	v
Súdánu	Súdán	k1gInSc6	Súdán
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
fakt	fakt	k1gInSc1	fakt
poznamenal	poznamenat	k5eAaPmAgInS	poznamenat
i	i	k9	i
jeho	jeho	k3xOp3gNnSc4	jeho
dětství	dětství	k1gNnSc4	dětství
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgMnSc7	svůj
mladším	mladý	k2eAgMnSc7d2	mladší
bratrem	bratr	k1gMnSc7	bratr
Alim	Alima	k1gFnPc2	Alima
si	se	k3xPyFc3	se
často	často	k6eAd1	často
hrávali	hrávat	k5eAaImAgMnP	hrávat
na	na	k7c4	na
vojáky	voják	k1gMnPc4	voják
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
svým	svůj	k3xOyFgInSc7	svůj
domem	dům	k1gInSc7	dům
si	se	k3xPyFc3	se
vybudovali	vybudovat	k5eAaPmAgMnP	vybudovat
pevnost	pevnost	k1gFnSc4	pevnost
s	s	k7c7	s
vojáčky	vojáček	k1gMnPc7	vojáček
a	a	k8xC	a
malý	malý	k2eAgInSc4d1	malý
Muhammad	Muhammad	k1gInSc4	Muhammad
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
trávil	trávit	k5eAaImAgMnS	trávit
spousty	spousta	k1gFnPc4	spousta
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přesto	přesto	k8xC	přesto
všechno	všechen	k3xTgNnSc1	všechen
nechtěl	chtít	k5eNaImAgMnS	chtít
Nadžíbův	Nadžíbův	k2eAgMnSc1d1	Nadžíbův
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ho	on	k3xPp3gNnSc4	on
syn	syn	k1gMnSc1	syn
následoval	následovat	k5eAaImAgMnS	následovat
a	a	k8xC	a
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Věřil	věřit	k5eAaImAgMnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
může	moct	k5eAaImIp3nS	moct
sloužit	sloužit	k5eAaImF	sloužit
Egyptu	Egypt	k1gInSc3	Egypt
lépe	dobře	k6eAd2	dobře
v	v	k7c6	v
civilním	civilní	k2eAgInSc6d1	civilní
sektoru	sektor	k1gInSc6	sektor
než	než	k8xS	než
jako	jako	k8xC	jako
voják	voják	k1gMnSc1	voják
ve	v	k7c6	v
službách	služba	k1gFnPc6	služba
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
začal	začít	k5eAaPmAgInS	začít
Muhammad	Muhammad	k1gInSc1	Muhammad
studovat	studovat	k5eAaImF	studovat
na	na	k7c4	na
překladatele	překladatel	k1gMnPc4	překladatel
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
získal	získat	k5eAaPmAgMnS	získat
vysokoškolské	vysokoškolský	k2eAgNnSc4d1	vysokoškolské
vzdělání	vzdělání	k1gNnSc4	vzdělání
v	v	k7c6	v
oborech	obor	k1gInPc6	obor
politologie	politologie	k1gFnSc2	politologie
a	a	k8xC	a
občanských	občanský	k2eAgNnPc2d1	občanské
práv	právo	k1gNnPc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
nikdy	nikdy	k6eAd1	nikdy
nezískal	získat	k5eNaPmAgMnS	získat
doktorát	doktorát	k1gInSc4	doktorát
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
navzdory	navzdory	k7c3	navzdory
přání	přání	k1gNnSc3	přání
svého	svůj	k1gMnSc2	svůj
otce	otec	k1gMnSc2	otec
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
nadále	nadále	k6eAd1	nadále
však	však	k9	však
zdokonaloval	zdokonalovat	k5eAaImAgMnS	zdokonalovat
své	svůj	k3xOyFgFnPc4	svůj
jazykové	jazykový	k2eAgFnPc4d1	jazyková
schopnosti	schopnost	k1gFnPc4	schopnost
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
získal	získat	k5eAaPmAgInS	získat
během	během	k7c2	během
studií	studio	k1gNnPc2	studio
jako	jako	k8xS	jako
překladatel	překladatel	k1gMnSc1	překladatel
<g/>
.	.	kIx.	.
</s>
<s>
Nadžíb	Nadžíb	k1gMnSc1	Nadžíb
hovořil	hovořit	k5eAaImAgMnS	hovořit
anglicky	anglicky	k6eAd1	anglicky
<g/>
,	,	kIx,	,
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
,	,	kIx,	,
italsky	italsky	k6eAd1	italsky
a	a	k8xC	a
německy	německy	k6eAd1	německy
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
padesátých	padesátý	k4xOgNnPc6	padesátý
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
začal	začít	k5eAaPmAgInS	začít
učit	učit	k5eAaImF	učit
hebrejštinu	hebrejština	k1gFnSc4	hebrejština
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
později	pozdě	k6eAd2	pozdě
zavedl	zavést	k5eAaPmAgInS	zavést
jako	jako	k9	jako
povinnou	povinný	k2eAgFnSc4d1	povinná
na	na	k7c6	na
vojenských	vojenský	k2eAgFnPc6d1	vojenská
školách	škola	k1gFnPc6	škola
v	v	k7c6	v
Káhiře	Káhira	k1gFnSc6	Káhira
a	a	k8xC	a
Alexandrii	Alexandrie	k1gFnSc6	Alexandrie
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
během	během	k7c2	během
arabsko-izraelské	arabskozraelský	k2eAgFnSc2d1	arabsko-izraelská
války	válka	k1gFnSc2	válka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
měla	mít	k5eAaImAgFnS	mít
egyptská	egyptský	k2eAgFnSc1d1	egyptská
armáda	armáda	k1gFnSc1	armáda
velmi	velmi	k6eAd1	velmi
málo	málo	k4c1	málo
vojáků	voják	k1gMnPc2	voják
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
mohli	moct	k5eAaImAgMnP	moct
porozumět	porozumět	k5eAaPmF	porozumět
izraelské	izraelský	k2eAgFnSc3d1	izraelská
komunikaci	komunikace	k1gFnSc3	komunikace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
studií	studio	k1gNnPc2	studio
v	v	k7c6	v
Chartúmu	Chartúm	k1gInSc6	Chartúm
byl	být	k5eAaImAgInS	být
Nadžíb	Nadžíb	k1gInSc1	Nadžíb
za	za	k7c4	za
kritizování	kritizování	k1gNnSc4	kritizování
britské	britský	k2eAgFnSc2d1	britská
okupace	okupace	k1gFnSc2	okupace
Egypta	Egypt	k1gInSc2	Egypt
a	a	k8xC	a
Súdánu	Súdán	k1gInSc2	Súdán
častokrát	častokrát	k6eAd1	častokrát
trestaný	trestaný	k2eAgInSc1d1	trestaný
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
i	i	k9	i
dokonce	dokonce	k9	dokonce
bičovaný	bičovaný	k2eAgMnSc1d1	bičovaný
svými	svůj	k3xOyFgMnPc7	svůj
britskými	britský	k2eAgMnPc7d1	britský
učiteli	učitel	k1gMnPc7	učitel
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
vzorem	vzor	k1gInSc7	vzor
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
Napoleon	napoleon	k1gInSc1	napoleon
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
on	on	k3xPp3gMnSc1	on
spával	spávat	k5eAaImAgMnS	spávat
na	na	k7c6	na
podlaze	podlaha	k1gFnSc6	podlaha
místo	místo	k7c2	místo
v	v	k7c6	v
posteli	postel	k1gFnSc6	postel
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
vzhlížel	vzhlížet	k5eAaImAgMnS	vzhlížet
k	k	k7c3	k
Mustafu	Mustaf	k1gMnSc3	Mustaf
Kamelovi	Kamel	k1gMnSc3	Kamel
<g/>
,	,	kIx,	,
zakladateli	zakladatel	k1gMnSc3	zakladatel
Národní	národní	k2eAgFnSc2d1	národní
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
a	a	k8xC	a
egyptskému	egyptský	k2eAgMnSc3d1	egyptský
revolucionáři	revolucionář	k1gMnSc3	revolucionář
a	a	k8xC	a
premiérovi	premiér	k1gMnSc3	premiér
Saadu	Saad	k1gMnSc3	Saad
Zaghlulovi	Zaghlul	k1gMnSc3	Zaghlul
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stáří	stáří	k1gNnSc6	stáří
obdivoval	obdivovat	k5eAaImAgMnS	obdivovat
indického	indický	k2eAgMnSc4d1	indický
duchovního	duchovní	k1gMnSc4	duchovní
vůdce	vůdce	k1gMnSc4	vůdce
Gándhího	Gándhí	k1gMnSc4	Gándhí
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1916	[number]	k4	1916
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
rodina	rodina	k1gFnSc1	rodina
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
do	do	k7c2	do
Káhiry	Káhira	k1gFnSc2	Káhira
<g/>
.	.	kIx.	.
</s>
<s>
Nadžíb	Nadžíb	k1gInSc1	Nadžíb
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
bratrem	bratr	k1gMnSc7	bratr
Alim	Alim	k1gInPc3	Alim
ještě	ještě	k6eAd1	ještě
zůstali	zůstat	k5eAaPmAgMnP	zůstat
v	v	k7c6	v
Súdánu	Súdán	k1gInSc6	Súdán
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dokončili	dokončit	k5eAaPmAgMnP	dokončit
svá	svůj	k3xOyFgNnPc4	svůj
studia	studio	k1gNnPc4	studio
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vojenská	vojenský	k2eAgFnSc1d1	vojenská
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
Nadžíb	Nadžíb	k1gInSc1	Nadžíb
začínal	začínat	k5eAaImAgInS	začínat
svoji	svůj	k3xOyFgFnSc4	svůj
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
kariéru	kariéra	k1gFnSc4	kariéra
u	u	k7c2	u
káhirské	káhirský	k2eAgFnSc2d1	Káhirská
stráže	stráž	k1gFnSc2	stráž
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
sloužil	sloužit	k5eAaImAgMnS	sloužit
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1927	[number]	k4	1927
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
<g/>
,	,	kIx,	,
započal	započnout	k5eAaPmAgMnS	započnout
svá	svůj	k3xOyFgNnPc4	svůj
další	další	k2eAgNnPc4d1	další
studia	studio	k1gNnPc4	studio
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
i	i	k9	i
nadále	nadále	k6eAd1	nadále
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
v	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1931	[number]	k4	1931
byl	být	k5eAaImAgMnS	být
rozhodnut	rozhodnout	k5eAaPmNgMnS	rozhodnout
armádu	armáda	k1gFnSc4	armáda
opustit	opustit	k5eAaPmF	opustit
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
díky	díky	k7c3	díky
povýšení	povýšení	k1gNnPc1	povýšení
své	svůj	k3xOyFgNnSc4	svůj
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
změnil	změnit	k5eAaPmAgInS	změnit
a	a	k8xC	a
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
vojenské	vojenský	k2eAgFnSc6d1	vojenská
kariéře	kariéra	k1gFnSc6	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
roku	rok	k1gInSc2	rok
1934	[number]	k4	1934
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
oženil	oženit	k5eAaPmAgMnS	oženit
a	a	k8xC	a
následně	následně	k6eAd1	následně
byl	být	k5eAaImAgInS	být
převelen	převelet	k5eAaPmNgInS	převelet
k	k	k7c3	k
pobřežní	pobřežní	k2eAgFnSc3d1	pobřežní
stráži	stráž	k1gFnSc3	stráž
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
na	na	k7c4	na
starost	starost	k1gFnSc4	starost
pronásledování	pronásledování	k1gNnSc2	pronásledování
pašeráků	pašerák	k1gMnPc2	pašerák
na	na	k7c6	na
Sinajském	sinajský	k2eAgInSc6d1	sinajský
poloostrově	poloostrov	k1gInSc6	poloostrov
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
pomáhali	pomáhat	k5eAaImAgMnP	pomáhat
místním	místní	k2eAgMnPc3d1	místní
beduínům	beduín	k1gMnPc3	beduín
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnPc3	který
poskytovali	poskytovat	k5eAaImAgMnP	poskytovat
i	i	k9	i
lékařskou	lékařský	k2eAgFnSc4d1	lékařská
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1940	[number]	k4	1940
byl	být	k5eAaImAgInS	být
znovu	znovu	k6eAd1	znovu
povýšen	povýšit	k5eAaPmNgInS	povýšit
a	a	k8xC	a
při	při	k7c6	při
té	ten	k3xDgFnSc6	ten
příležitosti	příležitost	k1gFnSc6	příležitost
znovu	znovu	k6eAd1	znovu
ukázal	ukázat	k5eAaPmAgInS	ukázat
svůj	svůj	k3xOyFgInSc4	svůj
postoj	postoj	k1gInSc4	postoj
k	k	k7c3	k
politickým	politický	k2eAgInPc3d1	politický
poměrům	poměr	k1gInPc3	poměr
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
poměrně	poměrně	k6eAd1	poměrně
dobré	dobrý	k2eAgInPc4d1	dobrý
vztahy	vztah	k1gInPc4	vztah
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
mezi	mezi	k7c7	mezi
ním	on	k3xPp3gMnSc7	on
a	a	k8xC	a
králem	král	k1gMnSc7	král
Farúkem	Farúek	k1gMnSc7	Farúek
panovaly	panovat	k5eAaImAgFnP	panovat
<g/>
,	,	kIx,	,
mu	on	k3xPp3gMnSc3	on
odmítl	odmítnout	k5eAaPmAgInS	odmítnout
políbit	políbit	k5eAaPmF	políbit
ruku	ruka	k1gFnSc4	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Nadžíb	Nadžíb	k1gMnSc1	Nadžíb
mu	on	k3xPp3gMnSc3	on
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
pouze	pouze	k6eAd1	pouze
podání	podání	k1gNnSc4	podání
ruky	ruka	k1gFnSc2	ruka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgFnSc1d1	další
iluze	iluze	k1gFnSc1	iluze
o	o	k7c6	o
Farúkově	Farúkův	k2eAgFnSc6d1	Farúkův
vládě	vláda	k1gFnSc6	vláda
ztratil	ztratit	k5eAaPmAgMnS	ztratit
Nadžíb	Nadžíb	k1gInSc4	Nadžíb
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1942	[number]	k4	1942
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
král	král	k1gMnSc1	král
ustoupil	ustoupit	k5eAaPmAgMnS	ustoupit
Britům	Brit	k1gMnPc3	Brit
a	a	k8xC	a
dovolil	dovolit	k5eAaPmAgMnS	dovolit
jim	on	k3xPp3gMnPc3	on
<g/>
,	,	kIx,	,
vybrat	vybrat	k5eAaPmF	vybrat
si	se	k3xPyFc3	se
premiéra	premiéra	k1gFnSc1	premiéra
podle	podle	k7c2	podle
svého	svůj	k3xOyFgNnSc2	svůj
uvážení	uvážení	k1gNnSc2	uvážení
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
protest	protest	k1gInSc4	protest
poslal	poslat	k5eAaPmAgMnS	poslat
Nadžíb	Nadžíb	k1gMnSc1	Nadžíb
Farúkovi	Farúek	k1gMnSc3	Farúek
svoji	svůj	k3xOyFgFnSc4	svůj
rezignaci	rezignace	k1gFnSc4	rezignace
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yRgFnPc4	který
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jelikož	jelikož	k8xS	jelikož
nebyla	být	k5eNaImAgFnS	být
armáda	armáda	k1gFnSc1	armáda
povolána	povolán	k2eAgFnSc1d1	povolána
bránit	bránit	k5eAaImF	bránit
Vaše	váš	k3xOp2gNnSc4	váš
veličenstvo	veličenstvo	k1gNnSc4	veličenstvo
<g/>
,	,	kIx,	,
stydím	stydět	k5eAaImIp1nS	stydět
se	se	k3xPyFc4	se
nosit	nosit	k5eAaImF	nosit
tuto	tento	k3xDgFnSc4	tento
uniformu	uniforma	k1gFnSc4	uniforma
a	a	k8xC	a
žádám	žádat	k5eAaImIp1nS	žádat
vás	vy	k3xPp2nPc4	vy
tímto	tento	k3xDgMnSc7	tento
o	o	k7c4	o
povolení	povolení	k1gNnPc4	povolení
k	k	k7c3	k
rezignaci	rezignace	k1gFnSc3	rezignace
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Farúk	Farúk	k1gMnSc1	Farúk
však	však	k9	však
jeho	jeho	k3xOp3gFnSc4	jeho
rezignaci	rezignace	k1gFnSc4	rezignace
nepřijal	přijmout	k5eNaPmAgMnS	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
Neúspěšně	úspěšně	k6eNd1	úspěšně
se	se	k3xPyFc4	se
pokoušel	pokoušet	k5eAaImAgInS	pokoušet
rezignovat	rezignovat	k5eAaBmF	rezignovat
i	i	k9	i
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
všemu	všecek	k3xTgNnSc3	všecek
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
Muhammad	Muhammad	k1gInSc4	Muhammad
v	v	k7c6	v
kariérním	kariérní	k2eAgInSc6d1	kariérní
postupu	postup	k1gInSc6	postup
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgMnS	účastnit
Palestinské	palestinský	k2eAgFnSc2d1	palestinská
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
Nadžíb	Nadžíba	k1gFnPc2	Nadžíba
každé	každý	k3xTgNnSc4	každý
ráno	ráno	k6eAd1	ráno
věnoval	věnovat	k5eAaImAgMnS	věnovat
třicetiminutovému	třicetiminutový	k2eAgNnSc3d1	třicetiminutové
čtení	čtení	k1gNnSc3	čtení
koránu	korán	k1gInSc2	korán
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byl	být	k5eAaImAgInS	být
jeho	jeho	k3xOp3gInSc1	jeho
zvyk	zvyk	k1gInSc1	zvyk
z	z	k7c2	z
dětství	dětství	k1gNnSc2	dětství
<g/>
,	,	kIx,	,
kterým	který	k3yIgNnSc7	který
posilňoval	posilňovat	k5eAaImAgInS	posilňovat
své	svůj	k3xOyFgNnSc4	svůj
odhodlání	odhodlání	k1gNnSc4	odhodlání
během	během	k7c2	během
špatných	špatný	k2eAgInPc2d1	špatný
časů	čas	k1gInPc2	čas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hnutí	hnutí	k1gNnPc2	hnutí
svobodných	svobodný	k2eAgMnPc2d1	svobodný
důstojníků	důstojník	k1gMnPc2	důstojník
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
se	se	k3xPyFc4	se
Nadžíb	Nadžíb	k1gMnSc1	Nadžíb
tajně	tajně	k6eAd1	tajně
připojil	připojit	k5eAaPmAgMnS	připojit
k	k	k7c3	k
hnutí	hnutí	k1gNnSc3	hnutí
svobodných	svobodný	k2eAgMnPc2d1	svobodný
důstojníků	důstojník	k1gMnPc2	důstojník
a	a	k8xC	a
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgMnS	být
povýšen	povýšit	k5eAaPmNgMnS	povýšit
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
generálmajora	generálmajor	k1gMnSc2	generálmajor
<g/>
.	.	kIx.	.
</s>
<s>
Nadžíb	Nadžíb	k1gInSc1	Nadžíb
byl	být	k5eAaImAgInS	být
obecně	obecně	k6eAd1	obecně
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
egyptských	egyptský	k2eAgMnPc2d1	egyptský
hrdinů	hrdina	k1gMnPc2	hrdina
Palestinské	palestinský	k2eAgFnSc2d1	palestinská
války	válka	k1gFnSc2	válka
a	a	k8xC	a
těšil	těšit	k5eAaImAgMnS	těšit
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
velkému	velký	k2eAgInSc3d1	velký
respektu	respekt	k1gInSc3	respekt
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Hnutí	hnutí	k1gNnSc1	hnutí
svobodných	svobodný	k2eAgMnPc2d1	svobodný
důstojníků	důstojník	k1gMnPc2	důstojník
<g/>
,	,	kIx,	,
vedené	vedený	k2eAgNnSc1d1	vedené
plukovníkem	plukovník	k1gMnSc7	plukovník
Gamálem	Gamál	k1gMnSc7	Gamál
Násirem	Násir	k1gMnSc7	Násir
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
spolek	spolek	k1gInSc1	spolek
mladých	mladý	k2eAgMnPc2d1	mladý
členů	člen	k1gMnPc2	člen
armády	armáda	k1gFnSc2	armáda
-	-	kIx~	-
všem	všecek	k3xTgFnPc3	všecek
bylo	být	k5eAaImAgNnS	být
pod	pod	k7c4	pod
pětatřicet	pětatřicet	k4xCc4	pětatřicet
a	a	k8xC	a
pocházeli	pocházet	k5eAaImAgMnP	pocházet
z	z	k7c2	z
nižší	nízký	k2eAgFnSc2d2	nižší
či	či	k8xC	či
střední	střední	k2eAgFnSc2d1	střední
vrstvy	vrstva	k1gFnSc2	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Násirovým	Násirův	k2eAgInSc7d1	Násirův
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
svržení	svržení	k1gNnSc1	svržení
krále	král	k1gMnSc2	král
Farúka	Farúek	k1gMnSc2	Farúek
a	a	k8xC	a
ukončení	ukončení	k1gNnSc6	ukončení
britské	britský	k2eAgFnSc2d1	britská
nadvlády	nadvláda	k1gFnSc2	nadvláda
nad	nad	k7c7	nad
Egyptem	Egypt	k1gInSc7	Egypt
a	a	k8xC	a
Súdánem	Súdán	k1gInSc7	Súdán
<g/>
.	.	kIx.	.
</s>
<s>
Vědom	vědom	k2eAgMnSc1d1	vědom
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
důstojníci	důstojník	k1gMnPc1	důstojník
tak	tak	k6eAd1	tak
mladého	mladý	k2eAgInSc2d1	mladý
věku	věk	k1gInSc2	věk
by	by	kYmCp3nP	by
nebyli	být	k5eNaImAgMnP	být
bráni	brát	k5eAaImNgMnP	brát
vážně	vážně	k6eAd1	vážně
<g/>
,	,	kIx,	,
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
místo	místo	k7c2	místo
vůdce	vůdce	k1gMnSc2	vůdce
hnutí	hnutí	k1gNnPc2	hnutí
právě	právě	k6eAd1	právě
Nadžíbovi	Nadžíbův	k2eAgMnPc1d1	Nadžíbův
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
tento	tento	k3xDgInSc1	tento
krok	krok	k1gInSc1	krok
vedl	vést	k5eAaImAgInS	vést
k	k	k7c3	k
úspěšnému	úspěšný	k2eAgNnSc3d1	úspěšné
posilnění	posilnění	k1gNnSc3	posilnění
celé	celý	k2eAgFnSc2d1	celá
organizace	organizace	k1gFnSc2	organizace
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
však	však	k9	však
také	také	k9	také
přinesl	přinést	k5eAaPmAgMnS	přinést
i	i	k9	i
velké	velký	k2eAgFnPc4d1	velká
neshody	neshoda	k1gFnPc4	neshoda
mezi	mezi	k7c7	mezi
Násirem	Násiro	k1gNnSc7	Násiro
a	a	k8xC	a
Nadžíbem	Nadžíb	k1gInSc7	Nadžíb
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
nesouhlas	nesouhlas	k1gInSc4	nesouhlas
svých	svůj	k3xOyFgMnPc2	svůj
kolegů	kolega	k1gMnPc2	kolega
zůstal	zůstat	k5eAaPmAgInS	zůstat
Muhammad	Muhammad	k1gInSc1	Muhammad
i	i	k9	i
nadále	nadále	k6eAd1	nadále
v	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
tohoto	tento	k3xDgNnSc2	tento
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
byl	být	k5eAaImAgInS	být
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
hnutí	hnutí	k1gNnSc1	hnutí
neztratilo	ztratit	k5eNaPmAgNnS	ztratit
svého	svůj	k3xOyFgMnSc4	svůj
nejvyššího	vysoký	k2eAgMnSc4d3	nejvyšší
důstojníka	důstojník	k1gMnSc4	důstojník
a	a	k8xC	a
nejvlivnějšího	vlivný	k2eAgMnSc4d3	nejvlivnější
člena	člen	k1gMnSc4	člen
<g/>
,	,	kIx,	,
kterým	který	k3yQgMnPc3	který
Nadžíb	Nadžíb	k1gInSc1	Nadžíb
byl	být	k5eAaImAgInS	být
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
mnozí	mnohý	k2eAgMnPc1d1	mnohý
domnívají	domnívat	k5eAaImIp3nP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Nadžíb	Nadžíb	k1gInSc1	Nadžíb
byl	být	k5eAaImAgInS	být
pouze	pouze	k6eAd1	pouze
loutkou	loutka	k1gFnSc7	loutka
v	v	k7c6	v
popředí	popředí	k1gNnSc6	popředí
celé	celý	k2eAgFnSc2d1	celá
organizace	organizace	k1gFnSc2	organizace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
6	[number]	k4	6
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1952	[number]	k4	1952
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
Nadžíb	Nadžíb	k1gMnSc1	Nadžíb
volby	volba	k1gFnSc2	volba
v	v	k7c6	v
armádním	armádní	k2eAgInSc6d1	armádní
důstojnickém	důstojnický	k2eAgInSc6d1	důstojnický
klubu	klub	k1gInSc6	klub
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
krok	krok	k1gInSc1	krok
byl	být	k5eAaImAgInS	být
téměř	téměř	k6eAd1	téměř
revoluční	revoluční	k2eAgInSc1d1	revoluční
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
královi	králův	k2eAgMnPc1d1	králův
přívrženci	přívrženec	k1gMnPc1	přívrženec
seděli	sedět	k5eAaImAgMnP	sedět
v	v	k7c6	v
klubu	klub	k1gInSc6	klub
na	na	k7c6	na
předních	přední	k2eAgNnPc6d1	přední
místech	místo	k1gNnPc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Nadžíbova	Nadžíbův	k2eAgFnSc1d1	Nadžíbův
reputace	reputace	k1gFnSc1	reputace
stále	stále	k6eAd1	stále
rostla	růst	k5eAaImAgFnS	růst
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
Egypt	Egypt	k1gInSc1	Egypt
ocitl	ocitnout	k5eAaPmAgInS	ocitnout
v	v	k7c6	v
chaosu	chaos	k1gInSc3	chaos
následovaným	následovaný	k2eAgInSc7d1	následovaný
káhirským	káhirský	k2eAgInSc7d1	káhirský
požárem	požár	k1gInSc7	požár
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vypukl	vypuknout	k5eAaPmAgInS	vypuknout
26	[number]	k4	26
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1952	[number]	k4	1952
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgInS	začít
Farúk	Farúk	k1gInSc1	Farúk
uvažovat	uvažovat	k5eAaImF	uvažovat
o	o	k7c4	o
odstranění	odstranění	k1gNnSc4	odstranění
Nadžíba	Nadžíb	k1gMnSc2	Nadžíb
z	z	k7c2	z
jeho	jeho	k3xOp3gFnSc2	jeho
pozice	pozice	k1gFnSc2	pozice
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
se	se	k3xPyFc4	se
zaměřil	zaměřit	k5eAaPmAgMnS	zaměřit
na	na	k7c4	na
hnutí	hnutí	k1gNnSc4	hnutí
svobodných	svobodný	k2eAgMnPc2d1	svobodný
důstojníků	důstojník	k1gMnPc2	důstojník
<g/>
.	.	kIx.	.
</s>
<s>
Vyšetřování	vyšetřování	k1gNnSc1	vyšetřování
odhalilo	odhalit	k5eAaPmAgNnS	odhalit
v	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
mnoho	mnoho	k6eAd1	mnoho
disidentů	disident	k1gMnPc2	disident
<g/>
.	.	kIx.	.
</s>
<s>
Výkonný	výkonný	k2eAgInSc1d1	výkonný
výbor	výbor	k1gInSc1	výbor
důstojnického	důstojnický	k2eAgInSc2d1	důstojnický
klubu	klub	k1gInSc2	klub
byl	být	k5eAaImAgInS	být
rozpuštěn	rozpuštěn	k2eAgInSc1d1	rozpuštěn
a	a	k8xC	a
Svobodní	svobodný	k2eAgMnPc1d1	svobodný
důstojníci	důstojník	k1gMnPc1	důstojník
přinesli	přinést	k5eAaPmAgMnP	přinést
plán	plán	k1gInSc4	plán
pro	pro	k7c4	pro
revoluci	revoluce	k1gFnSc4	revoluce
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
začít	začít	k5eAaPmF	začít
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1952	[number]	k4	1952
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Revoluce	revoluce	k1gFnSc1	revoluce
==	==	k?	==
</s>
</p>
<p>
<s>
23	[number]	k4	23
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1952	[number]	k4	1952
začalo	začít	k5eAaPmAgNnS	začít
hnutí	hnutí	k1gNnSc1	hnutí
svobodných	svobodný	k2eAgMnPc2d1	svobodný
důstojníků	důstojník	k1gMnPc2	důstojník
egyptskou	egyptský	k2eAgFnSc4d1	egyptská
revoluci	revoluce	k1gFnSc4	revoluce
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
měla	mít	k5eAaImAgFnS	mít
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
sesadil	sesadit	k5eAaPmAgMnS	sesadit
krále	král	k1gMnSc4	král
Farúka	Farúek	k1gMnSc4	Farúek
<g/>
.	.	kIx.	.
</s>
<s>
Nadžíb	Nadžíb	k1gMnSc1	Nadžíb
byl	být	k5eAaImAgMnS	být
nejprve	nejprve	k6eAd1	nejprve
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
vrchním	vrchní	k2eAgMnSc7d1	vrchní
velitelem	velitel	k1gMnSc7	velitel
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
v	v	k7c4	v
září	září	k1gNnSc4	září
egyptským	egyptský	k2eAgMnSc7d1	egyptský
premiérem	premiér	k1gMnSc7	premiér
<g/>
.	.	kIx.	.
</s>
<s>
Násir	Násir	k1gInSc1	Násir
byl	být	k5eAaImAgInS	být
dosazen	dosadit	k5eAaPmNgInS	dosadit
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
ministra	ministr	k1gMnSc2	ministr
vnitra	vnitro	k1gNnSc2	vnitro
<g/>
.	.	kIx.	.
</s>
<s>
Den	den	k1gInSc1	den
po	po	k7c6	po
začátku	začátek	k1gInSc6	začátek
revoluce	revoluce	k1gFnSc2	revoluce
se	se	k3xPyFc4	se
Nadžíb	Nadžíb	k1gMnSc1	Nadžíb
sešel	sejít	k5eAaPmAgMnS	sejít
s	s	k7c7	s
bývalým	bývalý	k2eAgMnSc7d1	bývalý
premiérem	premiér	k1gMnSc7	premiér
Ali	Ali	k1gMnSc7	Ali
Maherem	Maher	k1gMnSc7	Maher
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ho	on	k3xPp3gMnSc4	on
požádal	požádat	k5eAaPmAgMnS	požádat
o	o	k7c4	o
sestavení	sestavení	k1gNnSc4	sestavení
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
sdělil	sdělit	k5eAaPmAgMnS	sdělit
požadavky	požadavek	k1gInPc4	požadavek
revolucionářů	revolucionář	k1gMnPc2	revolucionář
králi	král	k1gMnSc6	král
Farúkovi	Farúek	k1gMnSc6	Farúek
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
nacházel	nacházet	k5eAaImAgMnS	nacházet
v	v	k7c6	v
Alexandrii	Alexandrie	k1gFnSc6	Alexandrie
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc4d1	následující
den	den	k1gInSc4	den
25	[number]	k4	25
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
se	se	k3xPyFc4	se
Nadžíb	Nadžíb	k1gMnSc1	Nadžíb
společně	společně	k6eAd1	společně
s	s	k7c7	s
radou	rada	k1gFnSc7	rada
vydal	vydat	k5eAaPmAgMnS	vydat
do	do	k7c2	do
Alexandrie	Alexandrie	k1gFnSc2	Alexandrie
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
dohlédl	dohlédnout	k5eAaPmAgMnS	dohlédnout
na	na	k7c4	na
odchod	odchod	k1gInSc4	odchod
krále	král	k1gMnSc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
rady	rada	k1gFnSc2	rada
však	však	k9	však
neměli	mít	k5eNaImAgMnP	mít
jednotný	jednotný	k2eAgInSc4d1	jednotný
názor	názor	k1gInSc4	názor
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
jaký	jaký	k3yRgInSc1	jaký
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
Farúkův	Farúkův	k2eAgInSc1d1	Farúkův
osud	osud	k1gInSc1	osud
<g/>
.	.	kIx.	.
</s>
<s>
Jedni	jeden	k4xCgMnPc1	jeden
ho	on	k3xPp3gMnSc4	on
chtěli	chtít	k5eAaImAgMnP	chtít
postavit	postavit	k5eAaPmF	postavit
před	před	k7c4	před
soud	soud	k1gInSc4	soud
<g/>
,	,	kIx,	,
druzí	druhý	k4xOgMnPc1	druhý
zase	zase	k9	zase
donutit	donutit	k5eAaPmF	donutit
abdikovat	abdikovat	k5eAaBmF	abdikovat
a	a	k8xC	a
poslat	poslat	k5eAaPmF	poslat
do	do	k7c2	do
exilu	exil	k1gInSc2	exil
<g/>
.	.	kIx.	.
</s>
<s>
Nadžíb	Nadžíb	k1gMnSc1	Nadžíb
a	a	k8xC	a
Násir	Násir	k1gMnSc1	Násir
podporovali	podporovat	k5eAaImAgMnP	podporovat
druhou	druhý	k4xOgFnSc4	druhý
možnost	možnost	k1gFnSc4	možnost
a	a	k8xC	a
po	po	k7c6	po
hlasování	hlasování	k1gNnSc6	hlasování
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
Farúk	Farúk	k1gInSc1	Farúk
abdikuje	abdikovat	k5eAaBmIp3nS	abdikovat
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
svého	svůj	k3xOyFgMnSc2	svůj
syna	syn	k1gMnSc2	syn
Ahmeda	Ahmed	k1gMnSc2	Ahmed
Fuada	Fuad	k1gMnSc2	Fuad
a	a	k8xC	a
odejde	odejít	k5eAaPmIp3nS	odejít
do	do	k7c2	do
exilu	exil	k1gInSc2	exil
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
26	[number]	k4	26
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1952	[number]	k4	1952
se	se	k3xPyFc4	se
přijel	přijet	k5eAaPmAgInS	přijet
Nadžíb	Nadžíb	k1gInSc1	Nadžíb
rozloučit	rozloučit	k5eAaPmF	rozloučit
s	s	k7c7	s
již	již	k6eAd1	již
bývalým	bývalý	k2eAgMnSc7d1	bývalý
králem	král	k1gMnSc7	král
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
dorazil	dorazit	k5eAaPmAgMnS	dorazit
pozdě	pozdě	k6eAd1	pozdě
a	a	k8xC	a
tak	tak	k6eAd1	tak
musel	muset	k5eAaImAgMnS	muset
Farúka	Farúka	k1gMnSc1	Farúka
dohnat	dohnat	k5eAaPmF	dohnat
lodí	loď	k1gFnSc7	loď
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
královské	královský	k2eAgFnSc2d1	královská
jachty	jachta	k1gFnSc2	jachta
El-Mahrousa	El-Mahrous	k1gMnSc2	El-Mahrous
připomněl	připomnět	k5eAaPmAgMnS	připomnět
Nadžíb	Nadžíb	k1gMnSc1	Nadžíb
Farúkovi	Farúek	k1gMnSc3	Farúek
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
loajální	loajální	k2eAgFnSc4d1	loajální
monarchii	monarchie	k1gFnSc4	monarchie
až	až	k9	až
do	do	k7c2	do
onoho	onen	k3xDgInSc2	onen
roku	rok	k1gInSc2	rok
1942	[number]	k4	1942
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
věci	věc	k1gFnPc1	věc
změnily	změnit	k5eAaPmAgFnP	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Nadžíb	Nadžíb	k1gMnSc1	Nadžíb
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Pane	Pan	k1gMnSc5	Pan
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
jsme	být	k5eAaImIp1nP	být
nuceni	nutit	k5eAaImNgMnP	nutit
udělat	udělat	k5eAaPmF	udělat
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
jsme	být	k5eAaImIp1nP	být
udělali	udělat	k5eAaPmAgMnP	udělat
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
na	na	k7c4	na
což	což	k3yRnSc4	což
Farúk	Farúk	k1gMnSc1	Farúk
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Ano	ano	k9	ano
<g/>
,	,	kIx,	,
já	já	k3xPp1nSc1	já
vím	vědět	k5eAaImIp1nS	vědět
<g/>
.	.	kIx.	.
</s>
<s>
Tvůj	tvůj	k3xOp2gInSc1	tvůj
úkol	úkol	k1gInSc1	úkol
je	být	k5eAaImIp3nS	být
složitý	složitý	k2eAgInSc1d1	složitý
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
víš	vědět	k5eAaImIp2nS	vědět
<g/>
,	,	kIx,	,
vládnout	vládnout	k5eAaImF	vládnout
Egyptu	Egypt	k1gInSc3	Egypt
není	být	k5eNaImIp3nS	být
snadný	snadný	k2eAgInSc4d1	snadný
úkol	úkol	k1gInSc4	úkol
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Nadžíb	Nadžíb	k1gMnSc1	Nadžíb
později	pozdě	k6eAd2	pozdě
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
necítil	cítit	k5eNaImAgMnS	cítit
radost	radost	k1gFnSc4	radost
z	z	k7c2	z
jeho	jeho	k3xOp3gFnSc2	jeho
porážky	porážka	k1gFnSc2	porážka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
nástupce	nástupce	k1gMnSc1	nástupce
Fuad	Fuad	k1gMnSc1	Fuad
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
kterému	který	k3yRgInSc3	který
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
době	doba	k1gFnSc6	doba
jmenování	jmenování	k1gNnSc6	jmenování
pouhých	pouhý	k2eAgNnPc2d1	pouhé
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Británie	Británie	k1gFnSc1	Británie
neměla	mít	k5eNaImAgFnS	mít
důvod	důvod	k1gInSc4	důvod
k	k	k7c3	k
intervenci	intervence	k1gFnSc3	intervence
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
fakt	fakt	k1gInSc1	fakt
také	také	k6eAd1	také
umožnil	umožnit	k5eAaPmAgInS	umožnit
revolucionářům	revolucionář	k1gMnPc3	revolucionář
tvrdit	tvrdit	k5eAaImF	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jim	on	k3xPp3gMnPc3	on
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
sesazení	sesazení	k1gNnSc4	sesazení
zkorumpované	zkorumpovaný	k2eAgFnSc2d1	zkorumpovaná
vlády	vláda	k1gFnSc2	vláda
Farúka	Farúek	k1gInSc2	Farúek
I.	I.	kA	I.
<g/>
,	,	kIx,	,
ne	ne	k9	ne
o	o	k7c4	o
monarchii	monarchie	k1gFnSc4	monarchie
samotnou	samotný	k2eAgFnSc4d1	samotná
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
po	po	k7c6	po
upevnění	upevnění	k1gNnSc6	upevnění
své	svůj	k3xOyFgFnSc2	svůj
moci	moc	k1gFnSc2	moc
uskutečnili	uskutečnit	k5eAaPmAgMnP	uskutečnit
své	svůj	k3xOyFgInPc4	svůj
dlouhodobé	dlouhodobý	k2eAgInPc4d1	dlouhodobý
plány	plán	k1gInPc4	plán
na	na	k7c4	na
zrušení	zrušení	k1gNnSc4	zrušení
monarchie	monarchie	k1gFnSc2	monarchie
a	a	k8xC	a
aristokracie	aristokracie	k1gFnSc2	aristokracie
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
Ali	Ali	k1gFnSc1	Ali
Mahera	Mahera	k1gFnSc1	Mahera
rezignovala	rezignovat	k5eAaBmAgFnS	rezignovat
17	[number]	k4	17
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1952	[number]	k4	1952
a	a	k8xC	a
Nadžíb	Nadžíb	k1gMnSc1	Nadžíb
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
novým	nový	k2eAgMnSc7d1	nový
premiérem	premiér	k1gMnSc7	premiér
<g/>
.	.	kIx.	.
18	[number]	k4	18
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1953	[number]	k4	1953
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
přesně	přesně	k6eAd1	přesně
jedenáct	jedenáct	k4xCc4	jedenáct
měsíců	měsíc	k1gInPc2	měsíc
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
,	,	kIx,	,
prohlásil	prohlásit	k5eAaPmAgInS	prohlásit
Nadžíb	Nadžíb	k1gInSc1	Nadžíb
konec	konec	k1gInSc1	konec
egyptsko-súdánské	egyptskoúdánský	k2eAgFnSc2d1	egyptsko-súdánský
monarchie	monarchie	k1gFnSc2	monarchie
a	a	k8xC	a
založil	založit	k5eAaPmAgMnS	založit
Egyptskou	egyptský	k2eAgFnSc4d1	egyptská
republiku	republika	k1gFnSc4	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Prezidentství	prezidentství	k1gNnSc2	prezidentství
==	==	k?	==
</s>
</p>
<p>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
deklarací	deklarace	k1gFnSc7	deklarace
republiky	republika	k1gFnSc2	republika
se	se	k3xPyFc4	se
Nadžíb	Nadžíb	k1gMnSc1	Nadžíb
stal	stát	k5eAaPmAgMnS	stát
prvním	první	k4xOgMnSc7	první
egyptským	egyptský	k2eAgMnSc7d1	egyptský
prezidentem	prezident	k1gMnSc7	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
této	tento	k3xDgFnSc2	tento
funkce	funkce	k1gFnSc2	funkce
zastával	zastávat	k5eAaImAgMnS	zastávat
i	i	k9	i
pozici	pozice	k1gFnSc4	pozice
premiéra	premiér	k1gMnSc2	premiér
<g/>
.	.	kIx.	.
</s>
<s>
Nově	nově	k6eAd1	nově
složená	složený	k2eAgFnSc1d1	složená
vláda	vláda	k1gFnSc1	vláda
se	se	k3xPyFc4	se
skládala	skládat	k5eAaImAgFnS	skládat
především	především	k9	především
z	z	k7c2	z
vojenských	vojenský	k2eAgMnPc2d1	vojenský
důstojníků	důstojník	k1gMnPc2	důstojník
<g/>
.	.	kIx.	.
</s>
<s>
Gamál	Gamál	k1gMnSc1	Gamál
Násir	Násir	k1gMnSc1	Násir
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
místopředsedou	místopředseda	k1gMnSc7	místopředseda
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Nadžíb	Nadžíb	k1gInSc1	Nadžíb
postupem	postupem	k7c2	postupem
času	čas	k1gInSc2	čas
začal	začít	k5eAaPmAgInS	začít
mít	mít	k5eAaImF	mít
rozepře	rozepře	k1gFnPc4	rozepře
se	s	k7c7	s
členy	člen	k1gMnPc7	člen
rady	rada	k1gFnSc2	rada
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgInP	mít
být	být	k5eAaImF	být
cíle	cíl	k1gInPc4	cíl
revoluce	revoluce	k1gFnSc2	revoluce
realizovány	realizován	k2eAgInPc4d1	realizován
<g/>
.	.	kIx.	.
</s>
<s>
Muhammad	Muhammad	k6eAd1	Muhammad
chtěl	chtít	k5eAaImAgMnS	chtít
ukončit	ukončit	k5eAaPmF	ukončit
politický	politický	k2eAgInSc4d1	politický
vliv	vliv	k1gInSc4	vliv
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
vrátit	vrátit	k5eAaPmF	vrátit
zemi	zem	k1gFnSc4	zem
civilní	civilní	k2eAgFnSc4d1	civilní
vládu	vláda	k1gFnSc4	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Věřil	věřit	k5eAaImAgMnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
úloha	úloha	k1gFnSc1	úloha
armády	armáda	k1gFnSc2	armáda
není	být	k5eNaImIp3nS	být
vládnout	vládnout	k5eAaImF	vládnout
zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
chránit	chránit	k5eAaImF	chránit
ji	on	k3xPp3gFnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Armáda	armáda	k1gFnSc1	armáda
měla	mít	k5eAaImAgFnS	mít
odstranit	odstranit	k5eAaPmF	odstranit
zbytky	zbytek	k1gInPc4	zbytek
zkorumpovaného	zkorumpovaný	k2eAgInSc2d1	zkorumpovaný
režimu	režim	k1gInSc2	režim
a	a	k8xC	a
poté	poté	k6eAd1	poté
odstoupit	odstoupit	k5eAaPmF	odstoupit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Později	pozdě	k6eAd2	pozdě
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
"	"	kIx"	"
<g/>
Byl	být	k5eAaImAgMnS	být
jsem	být	k5eAaImIp1nS	být
egyptský	egyptský	k2eAgMnSc1d1	egyptský
prezident	prezident	k1gMnSc1	prezident
<g/>
"	"	kIx"	"
napsal	napsat	k5eAaPmAgInS	napsat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Ve	v	k7c6	v
věku	věk	k1gInSc6	věk
36	[number]	k4	36
let	léto	k1gNnPc2	léto
Abdel	Abdel	k1gMnSc1	Abdel
Násir	Násir	k1gMnSc1	Násir
cítil	cítit	k5eAaImAgMnS	cítit
<g/>
,	,	kIx,	,
že	že	k8xS	že
bychom	by	kYmCp1nP	by
mohli	moct	k5eAaImAgMnP	moct
ignorovat	ignorovat	k5eAaImF	ignorovat
egyptskou	egyptský	k2eAgFnSc4d1	egyptská
veřejnost	veřejnost	k1gFnSc4	veřejnost
až	až	k9	až
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
než	než	k8xS	než
dosáhneme	dosáhnout	k5eAaPmIp1nP	dosáhnout
našich	náš	k3xOp1gInPc2	náš
cílů	cíl	k1gInPc2	cíl
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
s	s	k7c7	s
opatrností	opatrnost	k1gFnSc7	opatrnost
53	[number]	k4	53
<g/>
letého	letý	k2eAgInSc2d1	letý
jsem	být	k5eAaImIp1nS	být
věřil	věřit	k5eAaImAgMnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
potřebujeme	potřebovat	k5eAaImIp1nP	potřebovat
pro	pro	k7c4	pro
naše	náš	k3xOp1gInPc4	náš
plány	plán	k1gInPc4	plán
podporu	podpor	k1gInSc2	podpor
zdola	zdola	k6eAd1	zdola
<g/>
,	,	kIx,	,
i	i	k8xC	i
kdyby	kdyby	kYmCp3nS	kdyby
to	ten	k3xDgNnSc1	ten
znamenalo	znamenat	k5eAaImAgNnS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
musíme	muset	k5eAaImIp1nP	muset
některé	některý	k3yIgInPc4	některý
z	z	k7c2	z
našich	náš	k3xOp1gInPc2	náš
cílů	cíl	k1gInPc2	cíl
odložit	odložit	k5eAaPmF	odložit
<g/>
.	.	kIx.	.
</s>
<s>
Lišil	lišit	k5eAaImAgMnS	lišit
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
od	od	k7c2	od
mladších	mladý	k2eAgMnPc2d2	mladší
důstojníků	důstojník	k1gMnPc2	důstojník
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
našich	náš	k3xOp1gInPc2	náš
cílů	cíl	k1gInPc2	cíl
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
zásadami	zásada	k1gFnPc7	zásada
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Přestože	přestože	k8xS	přestože
byl	být	k5eAaImAgInS	být
Nadžíb	Nadžíb	k1gInSc1	Nadžíb
prezidentem	prezident	k1gMnSc7	prezident
a	a	k8xC	a
premiérem	premiér	k1gMnSc7	premiér
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc1	jeho
moc	moc	k6eAd1	moc
byla	být	k5eAaImAgFnS	být
omezená	omezený	k2eAgFnSc1d1	omezená
<g/>
.	.	kIx.	.
</s>
<s>
Každé	každý	k3xTgNnSc4	každý
jeho	jeho	k3xOp3gNnSc4	jeho
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
musela	muset	k5eAaImAgFnS	muset
schválit	schválit	k5eAaPmF	schválit
rada	rada	k1gFnSc1	rada
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
ho	on	k3xPp3gMnSc4	on
často	často	k6eAd1	často
ignorovala	ignorovat	k5eAaImAgNnP	ignorovat
<g/>
.	.	kIx.	.
</s>
<s>
Úřad	úřad	k1gInSc1	úřad
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zastával	zastávat	k5eAaImAgInS	zastávat
<g/>
,	,	kIx,	,
znamenal	znamenat	k5eAaImAgInS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
zodpovědný	zodpovědný	k2eAgMnSc1d1	zodpovědný
za	za	k7c4	za
všechna	všechen	k3xTgNnPc4	všechen
rozhodnutí	rozhodnutí	k1gNnPc4	rozhodnutí
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
jen	jen	k9	jen
zřídka	zřídka	k6eAd1	zřídka
souhlasil	souhlasit	k5eAaImAgMnS	souhlasit
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stával	stávat	k5eAaImAgInS	stávat
pouze	pouze	k6eAd1	pouze
jejich	jejich	k3xOp3gFnSc7	jejich
loutkou	loutka	k1gFnSc7	loutka
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
přišel	přijít	k5eAaPmAgInS	přijít
Nadžíb	Nadžíb	k1gInSc1	Nadžíb
za	za	k7c7	za
Násirem	Násir	k1gInSc7	Násir
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
měl	mít	k5eAaImAgInS	mít
v	v	k7c6	v
radě	rada	k1gFnSc6	rada
opravdovou	opravdový	k2eAgFnSc4d1	opravdová
moc	moc	k1gFnSc4	moc
<g/>
,	,	kIx,	,
s	s	k7c7	s
ultimátem	ultimátum	k1gNnSc7	ultimátum
<g/>
:	:	kIx,	:
buď	buď	k8xC	buď
dostane	dostat	k5eAaPmIp3nS	dostat
skutečnou	skutečný	k2eAgFnSc4d1	skutečná
moc	moc	k1gFnSc4	moc
nebo	nebo	k8xC	nebo
rezignuje	rezignovat	k5eAaBmIp3nS	rezignovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
1953	[number]	k4	1953
obvinil	obvinit	k5eAaPmAgMnS	obvinit
Násir	Násir	k1gMnSc1	Násir
Nadžíba	Nadžíba	k1gMnSc1	Nadžíba
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
podporuje	podporovat	k5eAaImIp3nS	podporovat
nedávno	nedávno	k6eAd1	nedávno
zakázané	zakázaný	k2eAgNnSc4d1	zakázané
Muslimské	muslimský	k2eAgNnSc4d1	muslimské
bratrstvo	bratrstvo	k1gNnSc4	bratrstvo
a	a	k8xC	a
přechovává	přechovávat	k5eAaImIp3nS	přechovávat
diktátorské	diktátorský	k2eAgFnPc4d1	diktátorská
ambice	ambice	k1gFnPc4	ambice
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
vypukl	vypuknout	k5eAaPmAgInS	vypuknout
boj	boj	k1gInSc4	boj
o	o	k7c4	o
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
armádou	armáda	k1gFnSc7	armáda
a	a	k8xC	a
Egyptem	Egypt	k1gInSc7	Egypt
samotným	samotný	k2eAgInSc7d1	samotný
<g/>
.	.	kIx.	.
25	[number]	k4	25
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1954	[number]	k4	1954
oznámila	oznámit	k5eAaPmAgFnS	oznámit
rada	rada	k1gFnSc1	rada
Nadžíbovu	Nadžíbův	k2eAgFnSc4d1	Nadžíbův
rezignaci	rezignace	k1gFnSc4	rezignace
na	na	k7c4	na
post	post	k1gInSc4	post
prezidenta	prezident	k1gMnSc2	prezident
a	a	k8xC	a
zdůvodnila	zdůvodnit	k5eAaPmAgFnS	zdůvodnit
to	ten	k3xDgNnSc4	ten
slovy	slovo	k1gNnPc7	slovo
<g/>
,	,	kIx,	,
že	že	k8xS	že
představuje	představovat	k5eAaImIp3nS	představovat
absolutní	absolutní	k2eAgFnSc4d1	absolutní
autoritu	autorita	k1gFnSc4	autorita
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
neakceptovatelná	akceptovatelný	k2eNgFnSc1d1	neakceptovatelná
<g/>
.	.	kIx.	.
</s>
<s>
Reakcí	reakce	k1gFnSc7	reakce
byla	být	k5eAaImAgFnS	být
velká	velký	k2eAgFnSc1d1	velká
nevole	nevole	k1gFnSc1	nevole
občanů	občan	k1gMnPc2	občan
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
vyšli	vyjít	k5eAaPmAgMnP	vyjít
na	na	k7c4	na
protest	protest	k1gInSc4	protest
do	do	k7c2	do
ulic	ulice	k1gFnPc2	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
protestům	protest	k1gInPc3	protest
se	se	k3xPyFc4	se
Nadžíb	Nadžíb	k1gInSc1	Nadžíb
následující	následující	k2eAgInSc1d1	následující
den	den	k1gInSc1	den
vrátil	vrátit	k5eAaPmAgInS	vrátit
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
prezidenta	prezident	k1gMnSc2	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
jeho	jeho	k3xOp3gInPc4	jeho
dny	den	k1gInPc4	den
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
funkci	funkce	k1gFnSc6	funkce
byly	být	k5eAaImAgInP	být
již	již	k6eAd1	již
sečtené	sečtený	k2eAgInPc1d1	sečtený
<g/>
.	.	kIx.	.
</s>
<s>
Gamál	Gamál	k1gMnSc1	Gamál
Násir	Násir	k1gMnSc1	Násir
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
novým	nový	k2eAgMnSc7d1	nový
premiérem	premiér	k1gMnSc7	premiér
a	a	k8xC	a
předsedou	předseda	k1gMnSc7	předseda
rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
</s>
<s>
Nadžíbova	Nadžíbův	k2eAgFnSc1d1	Nadžíbův
funkce	funkce	k1gFnSc1	funkce
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stala	stát	k5eAaPmAgFnS	stát
především	především	k6eAd1	především
symbolickou	symbolický	k2eAgFnSc4d1	symbolická
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
devět	devět	k4xCc4	devět
měsíců	měsíc	k1gInPc2	měsíc
později	pozdě	k6eAd2	pozdě
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
Nadžíb	Nadžíb	k1gMnSc1	Nadžíb
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
šarádě	šaráda	k1gFnSc6	šaráda
pokračovat	pokračovat	k5eAaImF	pokračovat
a	a	k8xC	a
14	[number]	k4	14
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1954	[number]	k4	1954
odstoupil	odstoupit	k5eAaPmAgMnS	odstoupit
z	z	k7c2	z
funkce	funkce	k1gFnSc2	funkce
prezidenta	prezident	k1gMnSc2	prezident
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
rezignaci	rezignace	k1gFnSc6	rezignace
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
novým	nový	k2eAgMnSc7d1	nový
prezidentem	prezident	k1gMnSc7	prezident
Gamálem	Gamál	k1gMnSc7	Gamál
Násirem	Násir	k1gMnSc7	Násir
nařízeno	nařídit	k5eAaPmNgNnS	nařídit
domácí	domácí	k2eAgNnSc1d1	domácí
vězení	vězení	k1gNnSc1	vězení
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
Nadžíb	Nadžíb	k1gMnSc1	Nadžíb
strávil	strávit	k5eAaPmAgMnS	strávit
ve	v	k7c6	v
vile	vila	k1gFnSc6	vila
Zienab	Zienab	k1gMnSc1	Zienab
Al-Wakil	Al-Wakil	k1gMnSc1	Al-Wakil
<g/>
,	,	kIx,	,
manželky	manželka	k1gFnPc1	manželka
bývalého	bývalý	k2eAgInSc2d1	bývalý
egyptského	egyptský	k2eAgInSc2d1	egyptský
premiéra	premiéra	k1gFnSc1	premiéra
Mustafy	Mustaf	k1gMnPc4	Mustaf
an-Nahhase	an-Nahhase	k6eAd1	an-Nahhase
Pašy	Paš	k2eAgFnPc1d1	Paš
<g/>
.	.	kIx.	.
</s>
<s>
Nadžíb	Nadžíb	k1gInSc1	Nadžíb
byl	být	k5eAaImAgInS	být
propuštěn	propustit	k5eAaPmNgInS	propustit
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
prezidentem	prezident	k1gMnSc7	prezident
Anvarem	Anvar	k1gMnSc7	Anvar
as-Sádátem	as-Sádát	k1gMnSc7	as-Sádát
<g/>
.	.	kIx.	.
</s>
<s>
Muhammad	Muhammad	k6eAd1	Muhammad
Nadžíb	Nadžíb	k1gMnSc1	Nadžíb
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
mu	on	k3xPp3gMnSc3	on
vystrojen	vystrojen	k2eAgInSc4d1	vystrojen
vojenský	vojenský	k2eAgInSc4d1	vojenský
pohřeb	pohřeb	k1gInSc4	pohřeb
<g/>
,	,	kIx,	,
na	na	k7c4	na
který	který	k3yQgInSc4	který
dorazil	dorazit	k5eAaPmAgMnS	dorazit
i	i	k8xC	i
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
prezident	prezident	k1gMnSc1	prezident
Husní	Husní	k1gMnSc1	Husní
Mubárak	Mubárak	k1gMnSc1	Mubárak
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
tentýž	týž	k3xTgInSc4	týž
rok	rok	k1gInSc4	rok
mu	on	k3xPp3gMnSc3	on
vyšly	vyjít	k5eAaPmAgFnP	vyjít
paměti	paměť	k1gFnPc1	paměť
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
publikované	publikovaný	k2eAgFnPc4d1	publikovaná
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Byl	být	k5eAaImAgMnS	být
jsem	být	k5eAaImIp1nS	být
egyptský	egyptský	k2eAgMnSc1d1	egyptský
prezident	prezident	k1gMnSc1	prezident
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
poctě	pocta	k1gFnSc3	pocta
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
pojmenovaná	pojmenovaný	k2eAgFnSc1d1	pojmenovaná
zastávka	zastávka	k1gFnSc1	zastávka
káhirského	káhirský	k2eAgNnSc2d1	Káhirské
metra	metro	k1gNnSc2	metro
a	a	k8xC	a
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
rodném	rodný	k2eAgInSc6d1	rodný
Chartúmu	Chartúm	k1gInSc6	Chartúm
hlavní	hlavní	k2eAgFnSc2d1	hlavní
silnice	silnice	k1gFnSc2	silnice
ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
Al	ala	k1gFnPc2	ala
Amarat	Amarat	k1gMnSc1	Amarat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Muhammad	Muhammad	k1gInSc1	Muhammad
Naguib	Naguib	k1gInSc1	Naguib
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
