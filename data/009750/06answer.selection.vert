<s>
Muhammad	Muhammad	k1gInSc1	Muhammad
Nadžíb	Nadžíba	k1gFnPc2	Nadžíba
<g/>
,	,	kIx,	,
arabsky	arabsky	k6eAd1	arabsky
ج	ج	k?	ج
ن	ن	k?	ن
(	(	kIx(	(
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1901	[number]	k4	1901
<g/>
,	,	kIx,	,
Chartúm	Chartúm	k1gInSc1	Chartúm
–	–	k?	–
29	[number]	k4	29
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1984	[number]	k4	1984
<g/>
,	,	kIx,	,
Káhira	Káhira	k1gFnSc1	Káhira
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgMnSc7	první
egyptským	egyptský	k2eAgMnSc7d1	egyptský
prezidentem	prezident	k1gMnSc7	prezident
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnSc7	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
po	po	k7c6	po
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
republiky	republika	k1gFnSc2	republika
18	[number]	k4	18
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1953	[number]	k4	1953
a	a	k8xC	a
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
setrval	setrvat	k5eAaPmAgMnS	setrvat
do	do	k7c2	do
14	[number]	k4	14
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1954	[number]	k4	1954
<g/>
.	.	kIx.	.
</s>
