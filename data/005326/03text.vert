<s>
Viceprezident	viceprezident	k1gMnSc1	viceprezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
je	být	k5eAaImIp3nS	být
druhým	druhý	k4xOgMnSc7	druhý
nejvyšším	vysoký	k2eAgMnSc7d3	nejvyšší
představitelem	představitel	k1gMnSc7	představitel
USA	USA	kA	USA
a	a	k8xC	a
podle	podle	k7c2	podle
ústavy	ústava	k1gFnSc2	ústava
USA	USA	kA	USA
je	být	k5eAaImIp3nS	být
předsedou	předseda	k1gMnSc7	předseda
ex	ex	k6eAd1	ex
officio	officio	k6eAd1	officio
Senátu	senát	k1gInSc2	senát
Kongresu	kongres	k1gInSc2	kongres
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Právo	právo	k1gNnSc1	právo
na	na	k7c6	na
hlasování	hlasování	k1gNnSc6	hlasování
má	mít	k5eAaImIp3nS	mít
však	však	k9	však
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
případě	případ	k1gInSc6	případ
rovnosti	rovnost	k1gFnSc2	rovnost
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
nástupnictví	nástupnictví	k1gNnSc2	nástupnictví
na	na	k7c4	na
úřad	úřad	k1gInSc4	úřad
prezidenta	prezident	k1gMnSc2	prezident
je	být	k5eAaImIp3nS	být
první	první	k4xOgMnSc1	první
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
by	by	kYmCp3nS	by
v	v	k7c6	v
případě	případ	k1gInSc6	případ
neschopnosti	neschopnost	k1gFnSc2	neschopnost
výkonu	výkon	k1gInSc2	výkon
funkce	funkce	k1gFnSc2	funkce
prezidenta	prezident	k1gMnSc2	prezident
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Nástupnictví	nástupnictví	k1gNnSc2	nástupnictví
na	na	k7c4	na
úřad	úřad	k1gInSc4	úřad
prezidenta	prezident	k1gMnSc2	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
.	.	kIx.	.
v	v	k7c6	v
případě	případ	k1gInSc6	případ
uvolnění	uvolnění	k1gNnSc4	uvolnění
úřadu	úřad	k1gInSc2	úřad
prezidenta	prezident	k1gMnSc2	prezident
USA	USA	kA	USA
se	se	k3xPyFc4	se
viceprezident	viceprezident	k1gMnSc1	viceprezident
stává	stávat	k5eAaImIp3nS	stávat
prezidentem	prezident	k1gMnSc7	prezident
do	do	k7c2	do
konce	konec	k1gInSc2	konec
funkčního	funkční	k2eAgNnSc2d1	funkční
období	období	k1gNnSc2	období
v	v	k7c6	v
případě	případ	k1gInSc6	případ
uvolnění	uvolnění	k1gNnSc4	uvolnění
úřadu	úřad	k1gInSc2	úřad
viceprezidenta	viceprezident	k1gMnSc2	viceprezident
USA	USA	kA	USA
následuje	následovat	k5eAaImIp3nS	následovat
v	v	k7c6	v
nástupnictví	nástupnictví	k1gNnSc6	nástupnictví
<g/>
:	:	kIx,	:
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1792	[number]	k4	1792
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1886	[number]	k4	1886
prozatímní	prozatímní	k2eAgMnSc1d1	prozatímní
předseda	předseda	k1gMnSc1	předseda
Senátu	senát	k1gInSc2	senát
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
řídí	řídit	k5eAaImIp3nS	řídit
schůzi	schůze	k1gFnSc4	schůze
Senátu	senát	k1gInSc2	senát
v	v	k7c6	v
nepřítomnosti	nepřítomnost	k1gFnSc6	nepřítomnost
Viceprezidenta	viceprezident	k1gMnSc2	viceprezident
<g/>
,	,	kIx,	,
dalším	další	k2eAgNnSc6d1	další
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
byl	být	k5eAaImAgMnS	být
předseda	předseda	k1gMnSc1	předseda
Sněmovny	sněmovna	k1gFnSc2	sněmovna
reprezentantů	reprezentant	k1gMnPc2	reprezentant
,	,	kIx,	,
<g/>
pak	pak	k6eAd1	pak
ministr	ministr	k1gMnSc1	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1947	[number]	k4	1947
byl	být	k5eAaImAgInS	být
zástupcem	zástupce	k1gMnSc7	zástupce
prezidenta	prezident	k1gMnSc2	prezident
ministr	ministr	k1gMnSc1	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
ministři	ministr	k1gMnPc1	ministr
dle	dle	k7c2	dle
vzniku	vznik	k1gInSc2	vznik
ministerstev	ministerstvo	k1gNnPc2	ministerstvo
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1947	[number]	k4	1947
je	být	k5eAaImIp3nS	být
zástupcem	zástupce	k1gMnSc7	zástupce
prezidenta	prezident	k1gMnSc2	prezident
předseda	předseda	k1gMnSc1	předseda
Sněmovny	sněmovna	k1gFnSc2	sněmovna
reprezentantů	reprezentant	k1gMnPc2	reprezentant
pak	pak	k6eAd1	pak
prozatímní	prozatímní	k2eAgMnSc1d1	prozatímní
předseda	předseda	k1gMnSc1	předseda
Senátu	senát	k1gInSc2	senát
dále	daleko	k6eAd2	daleko
<g />
.	.	kIx.	.
</s>
<s>
pak	pak	k6eAd1	pak
ministr	ministr	k1gMnSc1	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
a	a	k8xC	a
ministři	ministr	k1gMnPc1	ministr
dle	dle	k7c2	dle
vzniku	vznik	k1gInSc2	vznik
ministerstev	ministerstvo	k1gNnPc2	ministerstvo
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
25	[number]	k4	25
<g/>
.	.	kIx.	.
dodatkem	dodatek	k1gInSc7	dodatek
Ústavy	ústava	k1gFnSc2	ústava
prezident	prezident	k1gMnSc1	prezident
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
nového	nový	k2eAgMnSc4d1	nový
viceprezidenta	viceprezident	k1gMnSc4	viceprezident
se	s	k7c7	s
souhlasem	souhlas	k1gInSc7	souhlas
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
senátorů	senátor	k1gMnPc2	senátor
a	a	k8xC	a
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
členů	člen	k1gMnPc2	člen
Sněmovny	sněmovna	k1gFnSc2	sněmovna
reprezentantů	reprezentant	k1gMnPc2	reprezentant
Osoba	osoba	k1gFnSc1	osoba
vykonávající	vykonávající	k2eAgFnSc1d1	vykonávající
úřad	úřad	k1gInSc4	úřad
prezidenta	prezident	k1gMnSc2	prezident
či	či	k8xC	či
viceprezidenta	viceprezident	k1gMnSc2	viceprezident
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
naturalizovaný	naturalizovaný	k2eAgMnSc1d1	naturalizovaný
občan	občan	k1gMnSc1	občan
USA	USA	kA	USA
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
zde	zde	k6eAd1	zde
narozený	narozený	k2eAgMnSc1d1	narozený
<g/>
.	.	kIx.	.
</s>
<s>
Ministr	ministr	k1gMnSc1	ministr
vlády	vláda	k1gFnSc2	vláda
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
narozený	narozený	k2eAgMnSc1d1	narozený
v	v	k7c6	v
cizině	cizina	k1gFnSc6	cizina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tím	ten	k3xDgNnSc7	ten
mu	on	k3xPp3gMnSc3	on
zaniká	zanikat	k5eAaImIp3nS	zanikat
možnost	možnost	k1gFnSc4	možnost
vykonávat	vykonávat	k5eAaImF	vykonávat
úřad	úřad	k1gInSc4	úřad
prezidenta	prezident	k1gMnSc2	prezident
</s>
<s>
Úřad	úřad	k1gInSc1	úřad
viceprezidenta	viceprezident	k1gMnSc2	viceprezident
USA	USA	kA	USA
dosud	dosud	k6eAd1	dosud
zastávali	zastávat	k5eAaImAgMnP	zastávat
jen	jen	k9	jen
muži	muž	k1gMnPc1	muž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prezidentských	prezidentský	k2eAgFnPc6d1	prezidentská
volbách	volba	k1gFnPc6	volba
2008	[number]	k4	2008
kandidovala	kandidovat	k5eAaImAgFnS	kandidovat
na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
místo	místo	k1gNnSc4	místo
Sarah	Sarah	k1gFnSc1	Sarah
Palinová	Palinový	k2eAgFnSc1d1	Palinová
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
teprve	teprve	k6eAd1	teprve
o	o	k7c6	o
druhou	druhý	k4xOgFnSc7	druhý
ženou	žena	k1gFnSc7	žena
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
USA	USA	kA	USA
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
nominována	nominovat	k5eAaBmNgFnS	nominovat
na	na	k7c4	na
druhý	druhý	k4xOgInSc4	druhý
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
úřad	úřad	k1gInSc4	úřad
<g/>
,	,	kIx,	,
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
jedné	jeden	k4xCgFnSc2	jeden
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
největších	veliký	k2eAgFnPc2d3	veliký
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stalo	stát	k5eAaPmAgNnS	stát
v	v	k7c6	v
prezidentských	prezidentský	k2eAgFnPc6d1	prezidentská
volbách	volba	k1gFnPc6	volba
1984	[number]	k4	1984
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
za	za	k7c4	za
Demokratickou	demokratický	k2eAgFnSc4d1	demokratická
stranu	strana	k1gFnSc4	strana
neúspěšně	úspěšně	k6eNd1	úspěšně
kandidovala	kandidovat	k5eAaImAgFnS	kandidovat
na	na	k7c4	na
úřad	úřad	k1gInSc4	úřad
viceprezidentky	viceprezidentka	k1gFnSc2	viceprezidentka
USA	USA	kA	USA
newyorská	newyorský	k2eAgFnSc1d1	newyorská
kongresmanka	kongresmanka	k1gFnSc1	kongresmanka
Geraldine	Geraldin	k1gInSc5	Geraldin
Ferrarová	Ferrarový	k2eAgNnPc1d1	Ferrarový
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
prezidentským	prezidentský	k2eAgMnSc7d1	prezidentský
kandidátem	kandidát	k1gMnSc7	kandidát
Walterem	Walter	k1gMnSc7	Walter
Mondalem	Mondal	k1gMnSc7	Mondal
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
dodatek	dodatek	k1gInSc1	dodatek
Ústavy	ústava	k1gFnSc2	ústava
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
byl	být	k5eAaImAgInS	být
ratifikován	ratifikovat	k5eAaBmNgInS	ratifikovat
10	[number]	k4	10
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1967	[number]	k4	1967
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
4	[number]	k4	4
oddíly	oddíl	k1gInPc4	oddíl
a	a	k8xC	a
mj.	mj.	kA	mj.
upravuje	upravovat	k5eAaImIp3nS	upravovat
předání	předání	k1gNnSc1	předání
pravomoce	pravomoc	k1gFnSc2	pravomoc
prezidenta	prezident	k1gMnSc2	prezident
USA	USA	kA	USA
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
zbavení	zbavení	k1gNnSc2	zbavení
prezidenta	prezident	k1gMnSc2	prezident
úřadu	úřad	k1gInSc2	úřad
nebo	nebo	k8xC	nebo
v	v	k7c6	v
případě	případ	k1gInSc6	případ
jeho	jeho	k3xOp3gFnSc2	jeho
smrti	smrt	k1gFnSc2	smrt
nebo	nebo	k8xC	nebo
rezignace	rezignace	k1gFnSc2	rezignace
stane	stanout	k5eAaPmIp3nS	stanout
se	s	k7c7	s
prezidentem	prezident	k1gMnSc7	prezident
viceprezident	viceprezident	k1gMnSc1	viceprezident
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
XXV	XXV	kA	XXV
<g/>
.	.	kIx.	.
<g/>
dodatek	dodatek	k1gInSc1	dodatek
<g/>
,	,	kIx,	,
oddíl	oddíl	k1gInSc1	oddíl
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Kdykoli	kdykoli	k6eAd1	kdykoli
prezident	prezident	k1gMnSc1	prezident
postoupí	postoupit	k5eAaPmIp3nS	postoupit
prozatímnímu	prozatímní	k2eAgMnSc3d1	prozatímní
předsedovi	předseda	k1gMnSc3	předseda
Senátu	senát	k1gInSc2	senát
a	a	k8xC	a
předsedovi	předseda	k1gMnSc3	předseda
Sněmovny	sněmovna	k1gFnPc1	sněmovna
reprezentantů	reprezentant	k1gInPc2	reprezentant
písemné	písemný	k2eAgNnSc4d1	písemné
prohlášení	prohlášení	k1gNnSc4	prohlášení
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
způsobilý	způsobilý	k2eAgMnSc1d1	způsobilý
vykonávat	vykonávat	k5eAaImF	vykonávat
pravomoci	pravomoc	k1gFnPc4	pravomoc
a	a	k8xC	a
povinnosti	povinnost	k1gFnPc4	povinnost
svého	svůj	k3xOyFgInSc2	svůj
úřadu	úřad	k1gInSc2	úřad
<g/>
,	,	kIx,	,
a	a	k8xC	a
dokud	dokud	k8xS	dokud
jim	on	k3xPp3gMnPc3	on
nepředá	předat	k5eNaPmIp3nS	předat
písemné	písemný	k2eAgNnSc4d1	písemné
prohlášení	prohlášení	k1gNnSc4	prohlášení
o	o	k7c6	o
opaku	opak	k1gInSc6	opak
<g/>
,	,	kIx,	,
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
takové	takový	k3xDgFnPc4	takový
pravomoci	pravomoc	k1gFnPc4	pravomoc
a	a	k8xC	a
povinnosti	povinnost	k1gFnSc2	povinnost
viceprezident	viceprezident	k1gMnSc1	viceprezident
jako	jako	k8xS	jako
úřadující	úřadující	k2eAgMnSc1d1	úřadující
prezident	prezident	k1gMnSc1	prezident
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
XXV	XXV	kA	XXV
<g/>
.	.	kIx.	.
<g/>
dodatek	dodatek	k1gInSc1	dodatek
<g/>
,	,	kIx,	,
oddíl	oddíl	k1gInSc1	oddíl
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
K	k	k7c3	k
jeho	jeho	k3xOp3gMnPc3	jeho
využití	využití	k1gNnSc1	využití
došlo	dojít	k5eAaPmAgNnS	dojít
dosud	dosud	k6eAd1	dosud
šestkrát	šestkrát	k6eAd1	šestkrát
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
)	)	kIx)	)
12	[number]	k4	12
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1973	[number]	k4	1973
Nixon	Nixon	k1gMnSc1	Nixon
nominoval	nominovat	k5eAaBmAgMnS	nominovat
na	na	k7c4	na
viceprezidenta	viceprezident	k1gMnSc4	viceprezident
michiganského	michiganský	k2eAgMnSc2d1	michiganský
kongresmana	kongresman	k1gMnSc2	kongresman
Geralda	Gerald	k1gMnSc2	Gerald
Forda	ford	k1gMnSc2	ford
po	po	k7c6	po
rezignaci	rezignace	k1gFnSc6	rezignace
Spiro	Spiro	k1gNnSc1	Spiro
Agnewa	Agnewum	k1gNnSc2	Agnewum
<g/>
.2	.2	k4	.2
<g/>
)	)	kIx)	)
9	[number]	k4	9
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
<g />
.	.	kIx.	.
</s>
<s>
1974	[number]	k4	1974
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
Richard	Richard	k1gMnSc1	Richard
Nixon	Nixon	k1gMnSc1	Nixon
na	na	k7c4	na
post	post	k1gInSc4	post
prezidenta	prezident	k1gMnSc2	prezident
USA	USA	kA	USA
<g/>
.3	.3	k4	.3
<g/>
)	)	kIx)	)
20	[number]	k4	20
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1974	[number]	k4	1974
nový	nový	k2eAgInSc1d1	nový
prezident	prezident	k1gMnSc1	prezident
Ford	ford	k1gInSc4	ford
nominoval	nominovat	k5eAaBmAgMnS	nominovat
na	na	k7c4	na
post	post	k1gInSc4	post
viceprezidenta	viceprezident	k1gMnSc2	viceprezident
bývalého	bývalý	k2eAgMnSc2d1	bývalý
guvernéra	guvernér	k1gMnSc2	guvernér
New	New	k1gMnSc2	New
Yorku	York	k1gInSc2	York
Nelsona	Nelson	k1gMnSc2	Nelson
Rockefellera	Rockefeller	k1gMnSc2	Rockefeller
<g/>
.4	.4	k4	.4
<g/>
)	)	kIx)	)
12	[number]	k4	12
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1985	[number]	k4	1985
se	se	k3xPyFc4	se
na	na	k7c4	na
časový	časový	k2eAgInSc4d1	časový
úsek	úsek	k1gInSc4	úsek
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Ronald	Ronald	k1gMnSc1	Ronald
Reagan	Reagan	k1gMnSc1	Reagan
podstoupil	podstoupit	k5eAaPmAgMnS	podstoupit
kolonoskopii	kolonoskopie	k1gFnSc4	kolonoskopie
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgInS	stát
úřadujícím	úřadující	k2eAgMnSc7d1	úřadující
prezidentem	prezident	k1gMnSc7	prezident
USA	USA	kA	USA
G.	G.	kA	G.
Bush	Bush	k1gMnSc1	Bush
st.	st.	kA	st.
<g/>
.5	.5	k4	.5
<g/>
)	)	kIx)	)
29	[number]	k4	29
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2002	[number]	k4	2002
se	se	k3xPyFc4	se
na	na	k7c4	na
časový	časový	k2eAgInSc4d1	časový
úsek	úsek	k1gInSc4	úsek
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
-	-	kIx~	-
9	[number]	k4	9
<g/>
:	:	kIx,	:
<g/>
29	[number]	k4	29
hod	hod	k1gInSc1	hod
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
George	Georg	k1gFnSc2	Georg
W.	W.	kA	W.
Bush	Bush	k1gMnSc1	Bush
podstoupil	podstoupit	k5eAaPmAgMnS	podstoupit
kolonoskopii	kolonoskopie	k1gFnSc4	kolonoskopie
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
stal	stát	k5eAaPmAgMnS	stát
úřadujícím	úřadující	k2eAgMnSc7d1	úřadující
prezidentem	prezident	k1gMnSc7	prezident
USA	USA	kA	USA
R.	R.	kA	R.
Cheney	Chenea	k1gMnSc2	Chenea
<g/>
.6	.6	k4	.6
<g/>
)	)	kIx)	)
21	[number]	k4	21
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2007	[number]	k4	2007
se	se	k3xPyFc4	se
na	na	k7c4	na
časový	časový	k2eAgInSc4d1	časový
úsek	úsek	k1gInSc4	úsek
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
:	:	kIx,	:
<g/>
16	[number]	k4	16
-	-	kIx~	-
9	[number]	k4	9
<g/>
:	:	kIx,	:
<g/>
21	[number]	k4	21
hod	hod	k1gInSc1	hod
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
George	Georg	k1gFnSc2	Georg
W.	W.	kA	W.
Bush	Bush	k1gMnSc1	Bush
podstoupil	podstoupit	k5eAaPmAgMnS	podstoupit
kolonoskopii	kolonoskopie	k1gFnSc4	kolonoskopie
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
úřadujícím	úřadující	k2eAgMnSc7d1	úřadující
prezidentem	prezident	k1gMnSc7	prezident
USA	USA	kA	USA
R.	R.	kA	R.
Cheney	Chenea	k1gMnSc2	Chenea
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
právníků	právník	k1gMnPc2	právník
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
XXV	XXV	kA	XXV
<g/>
.	.	kIx.	.
dodatek	dodatek	k1gInSc1	dodatek
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
oddíl	oddíl	k1gInSc1	oddíl
<g/>
)	)	kIx)	)
naplněn	naplnit	k5eAaPmNgInS	naplnit
i	i	k9	i
v	v	k7c6	v
čase	čas	k1gInSc6	čas
atentátu	atentát	k1gInSc2	atentát
31	[number]	k4	31
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1981	[number]	k4	1981
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
Reagana	Reagan	k1gMnSc4	Reagan
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
de	de	k?	de
iure	iure	k1gInSc1	iure
úřadujícím	úřadující	k2eAgMnSc7d1	úřadující
prezidentem	prezident	k1gMnSc7	prezident
USA	USA	kA	USA
G.	G.	kA	G.
<g/>
Bush	Bush	k1gMnSc1	Bush
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
prezident	prezident	k1gMnSc1	prezident
Bill	Bill	k1gMnSc1	Bill
Clinton	Clinton	k1gMnSc1	Clinton
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
celkovou	celkový	k2eAgFnSc4d1	celková
anestézii	anestézie	k1gFnSc4	anestézie
při	při	k7c6	při
operačním	operační	k2eAgInSc6d1	operační
výkonu	výkon	k1gInSc6	výkon
přišití	přišití	k1gNnSc2	přišití
utrženého	utržený	k2eAgInSc2d1	utržený
čtyřhlavého	čtyřhlavý	k2eAgInSc2d1	čtyřhlavý
stehenního	stehenní	k2eAgInSc2d1	stehenní
svalu	sval	k1gInSc2	sval
právě	právě	k9	právě
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nemusel	muset	k5eNaImAgInS	muset
přenést	přenést	k5eAaPmF	přenést
své	svůj	k3xOyFgFnPc4	svůj
pravomoce	pravomoc	k1gFnPc4	pravomoc
na	na	k7c4	na
viceprezidenta	viceprezident	k1gMnSc4	viceprezident
Gorea	Goreus	k1gMnSc4	Goreus
<g/>
.	.	kIx.	.
</s>
<s>
Přísaha	přísaha	k1gFnSc1	přísaha
viceprezidenta	viceprezident	k1gMnSc2	viceprezident
USA	USA	kA	USA
I	I	kA	I
(	(	kIx(	(
<g/>
name	nam	k1gMnSc2	nam
<g/>
)	)	kIx)	)
do	do	k7c2	do
solemnly	solemnla	k1gFnSc2	solemnla
swear	swear	k1gInSc1	swear
(	(	kIx(	(
<g/>
or	or	k?	or
affirm	affirm	k1gInSc1	affirm
<g/>
)	)	kIx)	)
that	that	k1gInSc1	that
I	i	k9	i
will	wilnout	k5eAaPmAgInS	wilnout
support	support	k1gInSc1	support
and	and	k?	and
defend	defend	k1gInSc1	defend
the	the	k?	the
Constitution	Constitution	k1gInSc1	Constitution
of	of	k?	of
the	the	k?	the
United	United	k1gMnSc1	United
States	States	k1gMnSc1	States
against	against	k1gMnSc1	against
all	all	k?	all
enemies	enemies	k1gInSc1	enemies
<g/>
,	,	kIx,	,
foreign	foreign	k1gInSc1	foreign
and	and	k?	and
domestic	domestice	k1gFnPc2	domestice
<g/>
;	;	kIx,	;
that	that	k1gMnSc1	that
I	i	k9	i
will	wilnout	k5eAaPmAgMnS	wilnout
bear	bear	k1gMnSc1	bear
true	true	k1gNnSc2	true
faith	faith	k1gMnSc1	faith
and	and	k?	and
allegiance	allegiance	k1gFnSc2	allegiance
to	ten	k3xDgNnSc1	ten
the	the	k?	the
same	same	k1gInSc1	same
<g/>
;	;	kIx,	;
that	that	k1gInSc1	that
I	i	k9	i
take	takat	k5eAaPmIp3nS	takat
this	this	k6eAd1	this
obligation	obligation	k1gInSc4	obligation
freely	freela	k1gFnSc2	freela
<g/>
,	,	kIx,	,
without	without	k1gMnSc1	without
any	any	k?	any
mental	mentat	k5eAaPmAgMnS	mentat
reservation	reservation	k1gInSc4	reservation
or	or	k?	or
purpose	purposa	k1gFnSc3	purposa
of	of	k?	of
evasion	evasion	k1gInSc1	evasion
<g/>
;	;	kIx,	;
and	and	k?	and
that	that	k1gInSc1	that
I	i	k9	i
will	wilnout	k5eAaPmAgInS	wilnout
well	well	k1gInSc1	well
and	and	k?	and
faithfully	faithfulla	k1gMnSc2	faithfulla
discharge	discharg	k1gMnSc2	discharg
the	the	k?	the
duties	duties	k1gMnSc1	duties
of	of	k?	of
the	the	k?	the
office	office	k1gFnSc2	office
on	on	k3xPp3gMnSc1	on
which	which	k1gMnSc1	which
I	i	k9	i
am	am	k?	am
about	about	k1gInSc1	about
to	ten	k3xDgNnSc4	ten
enter	enter	k1gInSc1	enter
<g/>
.	.	kIx.	.
</s>
<s>
So	So	kA	So
help	help	k1gInSc1	help
me	me	k?	me
God	God	k1gFnSc2	God
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
překlad	překlad	k1gInSc1	překlad
<g/>
:	:	kIx,	:
Já	já	k3xPp1nSc1	já
___	___	k?	___
___	___	k?	___
<g/>
,	,	kIx,	,
slavnostně	slavnostně	k6eAd1	slavnostně
přísahám	přísahat	k5eAaImIp1nS	přísahat
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
prohlašuji	prohlašovat	k5eAaImIp1nS	prohlašovat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
že	že	k8xS	že
budu	být	k5eAaImBp1nS	být
podporovat	podporovat	k5eAaImF	podporovat
a	a	k8xC	a
bránit	bránit	k5eAaImF	bránit
Ústavu	ústava	k1gFnSc4	ústava
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
proti	proti	k7c3	proti
všem	všecek	k3xTgMnPc3	všecek
nepřátelům	nepřítel	k1gMnPc3	nepřítel
<g/>
,	,	kIx,	,
cizím	cizí	k2eAgMnPc3d1	cizí
a	a	k8xC	a
domácím	domácí	k2eAgMnPc3d1	domácí
<g/>
,	,	kIx,	,
že	že	k8xS	že
budu	být	k5eAaImBp1nS	být
mít	mít	k5eAaImF	mít
pravou	pravý	k2eAgFnSc4d1	pravá
víru	víra	k1gFnSc4	víra
a	a	k8xC	a
oddanost	oddanost	k1gFnSc4	oddanost
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsem	být	k5eAaImIp1nS	být
tuto	tento	k3xDgFnSc4	tento
povinnost	povinnost	k1gFnSc4	povinnost
přijal	přijmout	k5eAaPmAgMnS	přijmout
svobodně	svobodně	k6eAd1	svobodně
bez	bez	k7c2	bez
mentální	mentální	k2eAgFnSc2d1	mentální
výhrady	výhrada	k1gFnSc2	výhrada
nebo	nebo	k8xC	nebo
účelu	účel	k1gInSc3	účel
vyhnutí	vyhnutí	k1gNnSc2	vyhnutí
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
budu	být	k5eAaImBp1nS	být
dobře	dobře	k6eAd1	dobře
a	a	k8xC	a
věrně	věrně	k6eAd1	věrně
plnit	plnit	k5eAaImF	plnit
povinnosti	povinnost	k1gFnPc4	povinnost
úřadu	úřad	k1gInSc2	úřad
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yIgInSc2	který
se	se	k3xPyFc4	se
chystám	chystat	k5eAaImIp1nS	chystat
nastoupit	nastoupit	k5eAaPmF	nastoupit
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
mi	já	k3xPp1nSc3	já
dopomáhej	dopomáhat	k5eAaImRp2nS	dopomáhat
Bůh	bůh	k1gMnSc1	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1937	[number]	k4	1937
skládá	skládat	k5eAaImIp3nS	skládat
viceprezident	viceprezident	k1gMnSc1	viceprezident
přísahu	přísaha	k1gFnSc4	přísaha
krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
prezidentskou	prezidentský	k2eAgFnSc7d1	prezidentská
přísahou	přísaha	k1gFnSc7	přísaha
<g/>
.	.	kIx.	.
</s>
<s>
Dopravní	dopravní	k2eAgInPc1d1	dopravní
prostředky	prostředek	k1gInPc1	prostředek
k	k	k7c3	k
přepravě	přeprava	k1gFnSc3	přeprava
viceprezidenta	viceprezident	k1gMnSc2	viceprezident
USA	USA	kA	USA
<g/>
:	:	kIx,	:
Air	Air	k1gMnSc2	Air
Force	force	k1gFnSc1	force
Two	Two	k1gFnSc1	Two
(	(	kIx(	(
<g/>
Boeing	boeing	k1gInSc1	boeing
747	[number]	k4	747
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Marine	Marin	k1gMnSc5	Marin
Two	Two	k1gMnSc5	Two
(	(	kIx(	(
<g/>
helikoptéra	helikoptéra	k1gFnSc1	helikoptéra
<g/>
)	)	kIx)	)
a	a	k8xC	a
limuzína	limuzína	k1gFnSc1	limuzína
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1951	[number]	k4	1951
má	mít	k5eAaImIp3nS	mít
ochranu	ochrana	k1gFnSc4	ochrana
na	na	k7c4	na
starosti	starost	k1gFnPc4	starost
viceprezidenta	viceprezident	k1gMnSc2	viceprezident
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
rodiny	rodina	k1gFnSc2	rodina
Tajná	tajný	k2eAgFnSc1d1	tajná
služba	služba	k1gFnSc1	služba
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Oficiálním	oficiální	k2eAgNnSc7d1	oficiální
sídlem	sídlo	k1gNnSc7	sídlo
viceprezidenta	viceprezident	k1gMnSc2	viceprezident
je	být	k5eAaImIp3nS	být
Number	Number	k1gMnSc1	Number
One	One	k1gFnSc4	One
Observatory	Observator	k1gInPc4	Observator
Circle	Circle	k1gNnSc2	Circle
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
kancelář	kancelář	k1gFnSc1	kancelář
se	se	k3xPyFc4	se
v	v	k7c6	v
Bílém	bílý	k2eAgInSc6d1	bílý
domě	dům	k1gInSc6	dům
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
západním	západní	k2eAgNnSc6d1	západní
křídle	křídlo	k1gNnSc6	křídlo
a	a	k8xC	a
nazývá	nazývat	k5eAaImIp3nS	nazývat
se	se	k3xPyFc4	se
Ceremonial	Ceremonial	k1gInSc1	Ceremonial
Office	Office	kA	Office
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc4	seznam
viceprezidentů	viceprezident	k1gMnPc2	viceprezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
lednu	leden	k1gInSc3	leden
2017	[number]	k4	2017
žilo	žít	k5eAaImAgNnS	žít
sedm	sedm	k4xCc1	sedm
viceprezidentů	viceprezident	k1gMnPc2	viceprezident
<g/>
,	,	kIx,	,
úřadující	úřadující	k2eAgMnSc1d1	úřadující
a	a	k8xC	a
pět	pět	k4xCc1	pět
bývalých	bývalý	k2eAgFnPc2d1	bývalá
<g/>
:	:	kIx,	:
Seznam	seznam	k1gInSc1	seznam
viceprezidentů	viceprezident	k1gMnPc2	viceprezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
</s>
