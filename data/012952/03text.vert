<p>
<s>
Festival	festival	k1gInSc1	festival
Paris	Paris	k1gMnSc1	Paris
Cinéma	Cinéma	k1gNnSc4	Cinéma
je	být	k5eAaImIp3nS	být
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
filmový	filmový	k2eAgInSc1d1	filmový
festival	festival	k1gInSc1	festival
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
června	červen	k1gInSc2	červen
a	a	k8xC	a
července	červenec	k1gInSc2	červenec
<g/>
.	.	kIx.	.
</s>
<s>
Festival	festival	k1gInSc1	festival
má	mít	k5eAaImIp3nS	mít
projekce	projekce	k1gFnPc4	projekce
k	k	k7c3	k
poctám	pocta	k1gFnPc3	pocta
filmařů	filmař	k1gMnPc2	filmař
<g/>
,	,	kIx,	,
tematické	tematický	k2eAgMnPc4d1	tematický
sekce	sekce	k1gFnSc2	sekce
a	a	k8xC	a
retrospektivní	retrospektivní	k2eAgFnSc2d1	retrospektivní
přehlídky	přehlídka	k1gFnSc2	přehlídka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prezidentkou	prezidentka	k1gFnSc7	prezidentka
festivalu	festival	k1gInSc2	festival
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
britská	britský	k2eAgFnSc1d1	britská
herečka	herečka	k1gFnSc1	herečka
Charlotte	Charlott	k1gInSc5	Charlott
Rampling	Rampling	k1gInSc4	Rampling
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2003-2005	[number]	k4	2003-2005
tuto	tento	k3xDgFnSc4	tento
funkci	funkce	k1gFnSc4	funkce
zastával	zastávat	k5eAaImAgMnS	zastávat
řecký	řecký	k2eAgMnSc1d1	řecký
režisér	režisér	k1gMnSc1	režisér
Costa-Gavras	Costa-Gavras	k1gMnSc1	Costa-Gavras
<g/>
.	.	kIx.	.
</s>
<s>
Festival	festival	k1gInSc1	festival
se	se	k3xPyFc4	se
koná	konat	k5eAaImIp3nS	konat
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
Pařížské	pařížský	k2eAgFnSc2d1	Pařížská
radnice	radnice	k1gFnSc2	radnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ročníky	ročník	k1gInPc4	ročník
==	==	k?	==
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
ročníkTermín	ročníkTermína	k1gFnPc2	ročníkTermína
<g/>
:	:	kIx,	:
2	[number]	k4	2
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
-	-	kIx~	-
15	[number]	k4	15
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
20032	[number]	k4	20032
<g/>
.	.	kIx.	.
ročníkTermín	ročníkTermína	k1gFnPc2	ročníkTermína
<g/>
:	:	kIx,	:
28	[number]	k4	28
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
-	-	kIx~	-
13	[number]	k4	13
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2004	[number]	k4	2004
</s>
</p>
<p>
<s>
Oceněné	oceněný	k2eAgInPc1d1	oceněný
filmy	film	k1gInPc1	film
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Velká	velký	k2eAgFnSc1d1	velká
cena	cena	k1gFnSc1	cena
publika	publikum	k1gNnSc2	publikum
<g/>
:	:	kIx,	:
Quand	Quanda	k1gFnPc2	Quanda
la	la	k1gNnPc2	la
mer	mer	k?	mer
monte	monte	k5eAaPmIp2nP	monte
</s>
</p>
<p>
<s>
Cena	cena	k1gFnSc1	cena
ARTE	ARTE	kA	ARTE
<g/>
:	:	kIx,	:
In	In	k1gMnSc1	In
Your	Your	k1gMnSc1	Your
Hands	Hands	k1gInSc4	Hands
</s>
</p>
<p>
<s>
Cena	cena	k1gFnSc1	cena
novinářů	novinář	k1gMnPc2	novinář
<g/>
:	:	kIx,	:
South	South	k1gMnSc1	South
of	of	k?	of
the	the	k?	the
Clouds	Clouds	k1gInSc1	Clouds
</s>
</p>
<p>
<s>
Prix	Prix	k1gInSc1	Prix
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
avenir	avenir	k1gMnSc1	avenir
<g/>
:	:	kIx,	:
Días	Días	k1gInSc1	Días
de	de	k?	de
Santiago	Santiago	k1gNnSc1	Santiago
</s>
</p>
<p>
<s>
Pocta	pocta	k1gFnSc1	pocta
filmařům	filmař	k1gMnPc3	filmař
<g/>
:	:	kIx,	:
Karin	Karina	k1gFnPc2	Karina
Viardová	Viardová	k1gFnSc1	Viardová
<g/>
,	,	kIx,	,
Chung	Chung	k1gMnSc1	Chung
Chang-wha	Changha	k1gMnSc1	Chang-wha
<g/>
,	,	kIx,	,
BBC	BBC	kA	BBC
<g/>
,	,	kIx,	,
Laurent	Laurent	k1gMnSc1	Laurent
Cantet	Cantet	k1gMnSc1	Cantet
<g/>
,	,	kIx,	,
Walter	Walter	k1gMnSc1	Walter
Salles	Salles	k1gMnSc1	Salles
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
Winckler	Winckler	k1gMnSc1	Winckler
<g/>
,	,	kIx,	,
Jean-Paul	Jean-Paul	k1gInSc1	Jean-Paul
Belmondo	Belmondo	k6eAd1	Belmondo
<g/>
,	,	kIx,	,
Oliver	Oliver	k1gInSc1	Oliver
Stone	ston	k1gInSc5	ston
<g/>
,	,	kIx,	,
Fernando	Fernanda	k1gFnSc5	Fernanda
Solanas	Solanas	k1gInSc4	Solanas
</s>
</p>
<p>
<s>
Retrospektivy	retrospektiva	k1gFnPc1	retrospektiva
<g/>
:	:	kIx,	:
Claude	Claud	k1gInSc5	Claud
Sautet	Sautet	k1gInSc1	Sautet
<g/>
,	,	kIx,	,
Hongkongské	hongkongský	k2eAgNnSc1d1	hongkongské
filmové	filmový	k2eAgNnSc1d1	filmové
studio	studio	k1gNnSc1	studio
Shaw	Shaw	k1gFnSc2	Shaw
Brothers	Brothersa	k1gFnPc2	Brothersa
</s>
</p>
<p>
<s>
Tematické	tematický	k2eAgInPc1d1	tematický
programy	program	k1gInPc1	program
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Paříž	Paříž	k1gFnSc1	Paříž
-	-	kIx~	-
Londýn	Londýn	k1gInSc1	Londýn
a	a	k8xC	a
zpět	zpět	k6eAd1	zpět
</s>
</p>
<p>
<s>
Rock	rock	k1gInSc1	rock
a	a	k8xC	a
film	film	k1gInSc1	film
</s>
</p>
<p>
<s>
Muzikál	muzikál	k1gInSc1	muzikál
<g/>
:	:	kIx,	:
cesta	cesta	k1gFnSc1	cesta
kolem	kolem	k7c2	kolem
světa	svět	k1gInSc2	svět
</s>
</p>
<p>
<s>
Točili	točit	k5eAaImAgMnP	točit
také	také	k9	také
dokumenty	dokument	k1gInPc1	dokument
<g/>
...	...	k?	...
(	(	kIx(	(
<g/>
Agnè	Agnè	k1gMnSc1	Agnè
Varda	Varda	k1gMnSc1	Varda
<g/>
,	,	kIx,	,
Jean-Jacques	Jean-Jacques	k1gMnSc1	Jean-Jacques
Beineix	Beineix	k1gInSc1	Beineix
<g/>
,	,	kIx,	,
Al	ala	k1gFnPc2	ala
Pacino	Pacino	k1gNnSc1	Pacino
<g/>
,	,	kIx,	,
Krzysztof	Krzysztof	k1gInSc1	Krzysztof
Kieślowski	Kieślowsk	k1gFnSc2	Kieślowsk
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
French	French	k1gMnSc1	French
Classics	Classicsa	k1gFnPc2	Classicsa
with	with	k1gMnSc1	with
English	English	k1gMnSc1	English
Subtitles	Subtitles	k1gMnSc1	Subtitles
</s>
</p>
<p>
<s>
Paříž	Paříž	k1gFnSc1	Paříž
viděna	viděn	k2eAgFnSc1d1	viděna
<g/>
..	..	k?	..
<g/>
.3	.3	k4	.3
<g/>
.	.	kIx.	.
ročníkTermín	ročníkTermína	k1gFnPc2	ročníkTermína
<g/>
:	:	kIx,	:
28	[number]	k4	28
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
-	-	kIx~	-
12	[number]	k4	12
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2005	[number]	k4	2005
</s>
</p>
<p>
<s>
Oceněné	oceněný	k2eAgInPc1d1	oceněný
filmy	film	k1gInPc1	film
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Velká	velký	k2eAgFnSc1d1	velká
cena	cena	k1gFnSc1	cena
publika	publikum	k1gNnSc2	publikum
<g/>
:	:	kIx,	:
Watermarks	Watermarks	k1gInSc1	Watermarks
</s>
</p>
<p>
<s>
Cena	cena	k1gFnSc1	cena
novinářů	novinář	k1gMnPc2	novinář
<g/>
:	:	kIx,	:
How	How	k1gMnPc1	How
the	the	k?	the
Garcia	Garcium	k1gNnSc2	Garcium
Girls	girl	k1gFnPc2	girl
spent	spent	k1gMnSc1	spent
their	their	k1gMnSc1	their
Summer	Summer	k1gMnSc1	Summer
</s>
</p>
<p>
<s>
Prix	Prix	k1gInSc1	Prix
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
avenir	avenir	k1gMnSc1	avenir
<g/>
:	:	kIx,	:
Ronde	rond	k1gInSc5	rond
de	de	k?	de
Nuit	Nuit	k2eAgMnSc1d1	Nuit
</s>
</p>
<p>
<s>
Pocta	pocta	k1gFnSc1	pocta
filmařům	filmař	k1gMnPc3	filmař
<g/>
:	:	kIx,	:
Jeanne	Jeann	k1gInSc5	Jeann
Moreau	Moreaus	k1gInSc3	Moreaus
<g/>
,	,	kIx,	,
Jackie	Jackie	k1gFnSc1	Jackie
Chan	Chan	k1gMnSc1	Chan
<g/>
,	,	kIx,	,
Michael	Michael	k1gMnSc1	Michael
Cimino	Cimino	k1gNnSc1	Cimino
<g/>
,	,	kIx,	,
Moustapha	Moustapha	k1gFnSc1	Moustapha
Alassane	Alassan	k1gMnSc5	Alassan
<g/>
,	,	kIx,	,
Tim	Tim	k?	Tim
Burton	Burton	k1gInSc1	Burton
<g/>
,	,	kIx,	,
Javier	Javier	k1gMnSc1	Javier
Bardem	bard	k1gMnSc7	bard
<g/>
,	,	kIx,	,
Helma	helma	k1gFnSc1	helma
Sanders-Brahms	Sanders-Brahms	k1gInSc1	Sanders-Brahms
<g/>
,	,	kIx,	,
Arnaud	Arnaud	k1gInSc1	Arnaud
a	a	k8xC	a
Jean-Marie	Jean-Marie	k1gFnSc1	Jean-Marie
Larrieu	Larrieus	k1gInSc2	Larrieus
<g/>
,	,	kIx,	,
Stanley	Stanlea	k1gFnSc2	Stanlea
Kwan	Kwana	k1gFnPc2	Kwana
<g/>
,	,	kIx,	,
Lorenzo	Lorenza	k1gFnSc5	Lorenza
Recio	Recio	k1gNnSc4	Recio
</s>
</p>
<p>
<s>
Retrospektivy	retrospektiva	k1gFnPc1	retrospektiva
a	a	k8xC	a
tematické	tematický	k2eAgInPc1d1	tematický
programy	program	k1gInPc1	program
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Zázrak	zázrak	k1gInSc1	zázrak
brazilského	brazilský	k2eAgInSc2d1	brazilský
dokumentárního	dokumentární	k2eAgInSc2d1	dokumentární
filmu	film	k1gInSc2	film
</s>
</p>
<p>
<s>
Reggae	Reggae	k1gInSc1	Reggae
a	a	k8xC	a
film	film	k1gInSc1	film
</s>
</p>
<p>
<s>
Animovaná	animovaný	k2eAgFnSc1d1	animovaná
Afrika	Afrika	k1gFnSc1	Afrika
</s>
</p>
<p>
<s>
Unforgettable	Unforgettable	k6eAd1	Unforgettable
French	French	k1gInSc1	French
Films	Filmsa	k1gFnPc2	Filmsa
</s>
</p>
<p>
<s>
Experimentální	experimentální	k2eAgInSc1d1	experimentální
film	film	k1gInSc1	film
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
ročníkTermín	ročníkTermína	k1gFnPc2	ročníkTermína
<g/>
:	:	kIx,	:
27	[number]	k4	27
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
-	-	kIx~	-
11	[number]	k4	11
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2006	[number]	k4	2006
</s>
</p>
<p>
<s>
Oceněné	oceněný	k2eAgInPc1d1	oceněný
filmy	film	k1gInPc1	film
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Velká	velký	k2eAgFnSc1d1	velká
cena	cena	k1gFnSc1	cena
publika	publikum	k1gNnSc2	publikum
<g/>
:	:	kIx,	:
Bamako	Bamako	k1gNnSc1	Bamako
</s>
</p>
<p>
<s>
Cena	cena	k1gFnSc1	cena
novinářů	novinář	k1gMnPc2	novinář
<g/>
:	:	kIx,	:
Old	Olda	k1gFnPc2	Olda
Joy	Joy	k1gMnPc2	Joy
</s>
</p>
<p>
<s>
Prix	Prix	k1gInSc1	Prix
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
avenir	avenir	k1gMnSc1	avenir
<g/>
:	:	kIx,	:
C	C	kA	C
<g/>
'	'	kIx"	'
<g/>
est	est	k?	est
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
hiver	hiver	k1gMnSc1	hiver
</s>
</p>
<p>
<s>
Pocta	pocta	k1gFnSc1	pocta
filmařům	filmař	k1gMnPc3	filmař
<g/>
:	:	kIx,	:
Cyd	Cyd	k1gMnSc5	Cyd
Charisse	Chariss	k1gMnSc5	Chariss
<g/>
,	,	kIx,	,
Claude	Claud	k1gMnSc5	Claud
Chabrol	Chabrola	k1gFnPc2	Chabrola
<g/>
,	,	kIx,	,
Rossy	Rossa	k1gFnPc1	Rossa
de	de	k?	de
Palma	palma	k1gFnSc1	palma
</s>
</p>
<p>
<s>
Retrospektivy	retrospektiva	k1gFnPc1	retrospektiva
a	a	k8xC	a
tematické	tematický	k2eAgInPc1d1	tematický
programy	program	k1gInPc1	program
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Nové	Nové	k2eAgInPc1d1	Nové
korejské	korejský	k2eAgInPc1d1	korejský
filmy	film	k1gInPc1	film
</s>
</p>
<p>
<s>
Robert	Robert	k1gMnSc1	Robert
Wise	Wis	k1gFnSc2	Wis
Story	story	k1gFnSc2	story
</s>
</p>
<p>
<s>
Inferno	inferno	k1gNnSc1	inferno
<g/>
,	,	kIx,	,
pekelné	pekelný	k2eAgInPc1d1	pekelný
italské	italský	k2eAgInPc1d1	italský
kruhy	kruh	k1gInPc1	kruh
</s>
</p>
<p>
<s>
Electro	Electro	k1gNnSc1	Electro
a	a	k8xC	a
film	film	k1gInSc1	film
</s>
</p>
<p>
<s>
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
klasika	klasika	k1gFnSc1	klasika
s	s	k7c7	s
anglickými	anglický	k2eAgInPc7d1	anglický
titulky	titulek	k1gInPc7	titulek
</s>
</p>
<p>
<s>
Peter	Peter	k1gMnSc1	Peter
Fonda	Fonda	k1gMnSc1	Fonda
</s>
</p>
<p>
<s>
Marilyne	Marilynout	k5eAaPmIp3nS	Marilynout
Canto	canto	k1gNnSc1	canto
</s>
</p>
<p>
<s>
Nikolaus	Nikolaus	k1gMnSc1	Nikolaus
Geyrhalter	Geyrhalter	k1gMnSc1	Geyrhalter
</s>
</p>
<p>
<s>
Německé	německý	k2eAgNnSc1d1	německé
rozjasnění	rozjasnění	k1gNnSc1	rozjasnění
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
ročníkTermín	ročníkTermína	k1gFnPc2	ročníkTermína
<g/>
:	:	kIx,	:
3	[number]	k4	3
<g/>
.	.	kIx.	.
-	-	kIx~	-
14	[number]	k4	14
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2007	[number]	k4	2007
</s>
</p>
<p>
<s>
Oceněné	oceněný	k2eAgInPc1d1	oceněný
filmy	film	k1gInPc1	film
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Velká	velký	k2eAgFnSc1d1	velká
cena	cena	k1gFnSc1	cena
poroty	porota	k1gFnSc2	porota
<g/>
:	:	kIx,	:
This	This	k1gInSc1	This
is	is	k?	is
England	England	k1gInSc1	England
</s>
</p>
<p>
<s>
Velká	velký	k2eAgFnSc1d1	velká
cena	cena	k1gFnSc1	cena
publika	publikum	k1gNnSc2	publikum
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Cats	Catsa	k1gFnPc2	Catsa
of	of	k?	of
Mirikitani	Mirikitan	k1gMnPc1	Mirikitan
</s>
</p>
<p>
<s>
Prix	Prix	k1gInSc1	Prix
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
avenir	avenir	k1gMnSc1	avenir
<g/>
:	:	kIx,	:
Armin	Armin	k1gMnSc1	Armin
</s>
</p>
<p>
<s>
Pocta	pocta	k1gFnSc1	pocta
filmařům	filmař	k1gMnPc3	filmař
<g/>
:	:	kIx,	:
libanonský	libanonský	k2eAgInSc1d1	libanonský
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
Sandrine	Sandrin	k1gInSc5	Sandrin
Bonnaire	Bonnair	k1gInSc5	Bonnair
<g/>
,	,	kIx,	,
Christopher	Christophra	k1gFnPc2	Christophra
Doyle	Doyle	k1gFnSc2	Doyle
<g/>
,	,	kIx,	,
Francesco	Francesco	k1gMnSc1	Francesco
Rosi	Rosi	k1gMnSc1	Rosi
<g/>
,	,	kIx,	,
Robin	robin	k2eAgMnSc1d1	robin
Wright	Wright	k1gMnSc1	Wright
Penn	Penn	k1gMnSc1	Penn
<g/>
,	,	kIx,	,
Yasmin	Yasmin	k2eAgInSc1d1	Yasmin
Ahmad	Ahmad	k1gInSc1	Ahmad
<g/>
,	,	kIx,	,
Naomi	Nao	k1gFnPc7	Nao
Kawase	Kawasa	k1gFnSc6	Kawasa
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
ročníkTermín	ročníkTermína	k1gFnPc2	ročníkTermína
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
-	-	kIx~	-
12	[number]	k4	12
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2008	[number]	k4	2008
</s>
</p>
<p>
<s>
Oceněné	oceněný	k2eAgInPc1d1	oceněný
filmy	film	k1gInPc1	film
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Velká	velký	k2eAgFnSc1d1	velká
cena	cena	k1gFnSc1	cena
poroty	porota	k1gFnSc2	porota
<g/>
:	:	kIx,	:
Young	Young	k1gMnSc1	Young
<g/>
@	@	kIx~	@
<g/>
Heart	Heart	k1gInSc1	Heart
</s>
</p>
<p>
<s>
Velká	velký	k2eAgFnSc1d1	velká
cena	cena	k1gFnSc1	cena
publika	publikum	k1gNnSc2	publikum
<g/>
:	:	kIx,	:
Young	Young	k1gInSc1	Young
<g/>
@	@	kIx~	@
<g/>
Heart	Heart	k1gInSc1	Heart
</s>
</p>
<p>
<s>
Prix	Prix	k1gInSc1	Prix
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
avenir	avenir	k1gMnSc1	avenir
<g/>
:	:	kIx,	:
Tribu	Trib	k1gInSc2	Trib
</s>
</p>
<p>
<s>
Pocta	pocta	k1gFnSc1	pocta
filmařům	filmař	k1gMnPc3	filmař
<g/>
:	:	kIx,	:
filipínský	filipínský	k2eAgInSc1d1	filipínský
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
Nathalie	Nathalie	k1gFnSc1	Nathalie
Baye	Baye	k1gFnSc1	Baye
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
Cronenberg	Cronenberg	k1gMnSc1	Cronenberg
<g/>
,	,	kIx,	,
Aki	Aki	k1gMnSc5	Aki
Kaurismäki	Kaurismäk	k1gMnSc5	Kaurismäk
<g/>
,	,	kIx,	,
Jean-Claude	Jean-Claud	k1gMnSc5	Jean-Claud
Carriè	Carriè	k1gMnSc5	Carriè
<g/>
,	,	kIx,	,
Joseph	Joseph	k1gMnSc1	Joseph
Kuo	Kuo	k1gMnSc1	Kuo
<g/>
,	,	kIx,	,
Ronit	ronit	k5eAaImF	ronit
Elkabetz	Elkabetz	k1gInSc4	Elkabetz
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
ročníkTermín	ročníkTermína	k1gFnPc2	ročníkTermína
<g/>
:	:	kIx,	:
2	[number]	k4	2
<g/>
.	.	kIx.	.
-	-	kIx~	-
14	[number]	k4	14
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2009	[number]	k4	2009
</s>
</p>
<p>
<s>
Oceněné	oceněný	k2eAgInPc1d1	oceněný
filmy	film	k1gInPc1	film
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Velká	velký	k2eAgFnSc1d1	velká
cena	cena	k1gFnSc1	cena
poroty	porota	k1gFnSc2	porota
<g/>
:	:	kIx,	:
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Autre	Autr	k1gMnSc5	Autr
Rive	Rivus	k1gMnSc5	Rivus
</s>
</p>
<p>
<s>
Velká	velký	k2eAgFnSc1d1	velká
cena	cena	k1gFnSc1	cena
publika	publikum	k1gNnSc2	publikum
<g/>
:	:	kIx,	:
La	la	k1gNnSc1	la
Nana	Nan	k1gInSc2	Nan
(	(	kIx(	(
<g/>
dlouhometrážní	dlouhometrážní	k2eAgMnSc1d1	dlouhometrážní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Diplomacy	Diplomacy	k1gInPc1	Diplomacy
(	(	kIx(	(
<g/>
krátkometrážní	krátkometrážní	k2eAgMnSc1d1	krátkometrážní
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Prix	Prix	k1gInSc1	Prix
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
avenir	avenir	k1gMnSc1	avenir
<g/>
:	:	kIx,	:
Vegas	Vegas	k1gMnSc1	Vegas
(	(	kIx(	(
<g/>
dlouhometrážní	dlouhometrážní	k2eAgMnSc1d1	dlouhometrážní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Dehors	Dehors	k1gInSc1	Dehors
(	(	kIx(	(
<g/>
krátkometrážní	krátkometrážní	k2eAgFnSc1d1	krátkometrážní
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pocta	pocta	k1gFnSc1	pocta
filmařům	filmař	k1gMnPc3	filmař
<g/>
:	:	kIx,	:
turecký	turecký	k2eAgInSc1d1	turecký
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
Claudia	Claudia	k1gFnSc1	Claudia
Cardinalová	Cardinalová	k1gFnSc1	Cardinalová
<g/>
,	,	kIx,	,
Jean-Pierre	Jean-Pierr	k1gInSc5	Jean-Pierr
Léaud	Léaud	k1gInSc1	Léaud
<g/>
,	,	kIx,	,
Tsai	Tsai	k1gNnSc1	Tsai
Ming-liang	Mingianga	k1gFnPc2	Ming-lianga
<g/>
,	,	kIx,	,
Luis	Luisa	k1gFnPc2	Luisa
Miñ	Miñ	k1gFnPc2	Miñ
<g/>
,	,	kIx,	,
Naomi	Nao	k1gFnPc7	Nao
Kawase	Kawas	k1gInSc6	Kawas
</s>
</p>
<p>
<s>
Retrospektivy	retrospektiva	k1gFnPc1	retrospektiva
a	a	k8xC	a
tematické	tematický	k2eAgInPc1d1	tematický
programy	program	k1gInPc1	program
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Filmová	filmový	k2eAgFnSc1d1	filmová
noc	noc	k1gFnSc1	noc
</s>
</p>
<p>
<s>
Starožitný	starožitný	k2eAgInSc1d1	starožitný
film	film	k1gInSc1	film
</s>
</p>
<p>
<s>
Výstava	výstava	k1gFnSc1	výstava
fotografií	fotografia	k1gFnPc2	fotografia
pozvaných	pozvaný	k2eAgFnPc2d1	pozvaná
na	na	k7c4	na
festival	festival	k1gInSc4	festival
fotografa	fotograf	k1gMnSc2	fotograf
Jérôma	Jérôm	k1gMnSc2	Jérôm
Bonneta	Bonnet	k1gMnSc2	Bonnet
</s>
</p>
<p>
<s>
Adami	Ada	k1gFnPc7	Ada
(	(	kIx(	(
<g/>
Administration	Administration	k1gInSc1	Administration
des	des	k1gNnSc1	des
droits	droits	k6eAd1	droits
des	des	k1gNnSc7	des
artistes	artistesa	k1gFnPc2	artistesa
musiciens	musiciens	k1gInSc4	musiciens
et	et	k?	et
enterpretes	enterpretes	k1gInSc1	enterpretes
<g/>
)	)	kIx)	)
dělá	dělat	k5eAaImIp3nS	dělat
své	svůj	k3xOyFgInPc4	svůj
filmy	film	k1gInPc4	film
</s>
</p>
<p>
<s>
Filmový	filmový	k2eAgInSc1d1	filmový
koncert	koncert	k1gInSc1	koncert
Kenji	Kenj	k1gMnSc3	Kenj
Mizoguchi	Mizoguch	k1gMnSc3	Mizoguch
</s>
</p>
<p>
<s>
Kolem	kolem	k7c2	kolem
plamenů	plamen	k1gInPc2	plamen
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
ročníkTermín	ročníkTermína	k1gFnPc2	ročníkTermína
<g/>
:	:	kIx,	:
3	[number]	k4	3
<g/>
.	.	kIx.	.
-	-	kIx~	-
13	[number]	k4	13
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2010	[number]	k4	2010
</s>
</p>
<p>
<s>
Pocta	pocta	k1gFnSc1	pocta
filmařům	filmař	k1gMnPc3	filmař
<g/>
:	:	kIx,	:
japonský	japonský	k2eAgInSc1d1	japonský
film	film	k1gInSc1	film
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
ročníkTermín	ročníkTermína	k1gFnPc2	ročníkTermína
<g/>
:	:	kIx,	:
2	[number]	k4	2
<g/>
.	.	kIx.	.
-	-	kIx~	-
13	[number]	k4	13
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2011	[number]	k4	2011
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Festival	festival	k1gInSc1	festival
Paris	Paris	k1gMnSc1	Paris
Cinéma	Cinéma	k1gFnSc1	Cinéma
na	na	k7c6	na
francouzské	francouzský	k2eAgFnSc6d1	francouzská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Festival	festival	k1gInSc4	festival
Paris	Paris	k1gMnSc1	Paris
Cinéma	Ciném	k1gMnSc2	Ciném
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
festivalu	festival	k1gInSc2	festival
</s>
</p>
