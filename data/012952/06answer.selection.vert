<s>
Festival	festival	k1gInSc1	festival
Paris	Paris	k1gMnSc1	Paris
Cinéma	Cinéma	k1gNnSc4	Cinéma
je	být	k5eAaImIp3nS	být
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
filmový	filmový	k2eAgInSc1d1	filmový
festival	festival	k1gInSc1	festival
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
června	červen	k1gInSc2	červen
a	a	k8xC	a
července	červenec	k1gInSc2	červenec
<g/>
.	.	kIx.	.
</s>
