<s>
Kajman	kajman	k1gMnSc1
brýlový	brýlový	k2eAgMnSc1d1
</s>
<s>
Kajman	kajman	k1gMnSc1
brýlový	brýlový	k2eAgInSc1d1
Stupeň	stupeň	k1gInSc1
ohrožení	ohrožení	k1gNnSc2
podle	podle	k7c2
IUCN	IUCN	kA
</s>
<s>
málo	málo	k6eAd1
dotčený	dotčený	k2eAgMnSc1d1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc1
Říše	říš	k1gFnSc2
</s>
<s>
živočichové	živočich	k1gMnPc1
(	(	kIx(
<g/>
Animalia	Animalia	k1gFnSc1
<g/>
)	)	kIx)
Kmen	kmen	k1gInSc1
</s>
<s>
strunatci	strunatec	k1gMnPc1
(	(	kIx(
<g/>
Chordata	Chordata	k1gFnSc1
<g/>
)	)	kIx)
Podkmen	podkmen	k1gInSc1
</s>
<s>
obratlovci	obratlovec	k1gMnPc1
(	(	kIx(
<g/>
Vertebrata	Vertebrat	k2eAgFnSc1d1
<g/>
)	)	kIx)
Třída	třída	k1gFnSc1
</s>
<s>
plazi	plaz	k1gMnPc1
(	(	kIx(
<g/>
Reptilia	Reptilia	k1gFnSc1
<g/>
)	)	kIx)
Řád	řád	k1gInSc1
</s>
<s>
krokodýli	krokodýl	k1gMnPc1
(	(	kIx(
<g/>
Crocodilia	Crocodilia	k1gFnSc1
<g/>
)	)	kIx)
Čeleď	čeleď	k1gFnSc1
</s>
<s>
aligátorovití	aligátorovitý	k2eAgMnPc1d1
(	(	kIx(
<g/>
Alligatoridae	Alligatoridae	k1gNnSc7
<g/>
)	)	kIx)
Rod	rod	k1gInSc1
</s>
<s>
kajman	kajman	k1gMnSc1
(	(	kIx(
<g/>
Caiman	Caiman	k1gMnSc1
<g/>
)	)	kIx)
Binomické	binomický	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
</s>
<s>
Caiman	Caiman	k1gMnSc1
crocodilus	crocodilus	k1gMnSc1
<g/>
(	(	kIx(
<g/>
Carl	Carl	k1gMnSc1
Linné	Linná	k1gFnSc2
<g/>
,	,	kIx,
1758	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Rozšíření	rozšíření	k1gNnSc1
kajmana	kajman	k1gMnSc2
brýlového	brýlový	k2eAgMnSc2d1
<g/>
.	.	kIx.
</s>
<s>
Rozšíření	rozšíření	k1gNnSc1
kajmana	kajman	k1gMnSc2
brýlového	brýlový	k2eAgMnSc2d1
<g/>
.	.	kIx.
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Kajman	kajman	k1gMnSc1
brýlový	brýlový	k2eAgMnSc1d1
<g/>
,	,	kIx,
kajman	kajman	k1gMnSc1
obecný	obecný	k2eAgMnSc1d1
či	či	k8xC
kajman	kajman	k1gMnSc1
středoamerický	středoamerický	k2eAgMnSc1d1
(	(	kIx(
<g/>
Caiman	Caiman	k1gMnSc1
crocodilus	crocodilus	k1gMnSc1
známý	známý	k1gMnSc1
také	také	k9
pod	pod	k7c7
starším	starý	k2eAgNnSc7d2
jménem	jméno	k1gNnSc7
Caiman	Caiman	k1gMnSc1
sclerops	sclerops	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
poměrně	poměrně	k6eAd1
malý	malý	k2eAgInSc1d1
plaz	plaz	k1gInSc1
z	z	k7c2
čeledi	čeleď	k1gFnSc2
aligátorovitých	aligátorovitý	k2eAgFnPc2d1
obývající	obývající	k2eAgNnSc4d1
poměrně	poměrně	k6eAd1
rozsáhlé	rozsáhlý	k2eAgNnSc4d1
území	území	k1gNnSc4
Střední	střední	k2eAgFnSc2d1
a	a	k8xC
Jižní	jižní	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
v	v	k7c6
rozmezí	rozmezí	k1gNnSc6
od	od	k7c2
Venezuely	Venezuela	k1gFnSc2
až	až	k9
na	na	k7c4
jih	jih	k1gInSc4
Amazonské	amazonský	k2eAgFnSc2d1
pánve	pánev	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žije	žít	k5eAaImIp3nS
v	v	k7c6
pomalu	pomalu	k6eAd1
tekoucích	tekoucí	k2eAgFnPc6d1
vodách	voda	k1gFnPc6
<g/>
,	,	kIx,
nížinných	nížinný	k2eAgFnPc6d1
mokřinách	mokřina	k1gFnPc6
a	a	k8xC
jezerech	jezero	k1gNnPc6
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
schopen	schopen	k2eAgMnSc1d1
tolerovat	tolerovat	k5eAaImF
i	i	k9
slanou	slaný	k2eAgFnSc4d1
vodu	voda	k1gFnSc4
<g/>
;	;	kIx,
díky	díky	k7c3
této	tento	k3xDgFnSc3
adaptaci	adaptace	k1gFnSc3
je	být	k5eAaImIp3nS
také	také	k9
nejhojnějším	hojný	k2eAgMnSc7d3
zástupcem	zástupce	k1gMnSc7
své	svůj	k3xOyFgFnSc2
čeledi	čeleď	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Český	český	k2eAgInSc1d1
druhový	druhový	k2eAgInSc1d1
název	název	k1gInSc1
brýlový	brýlový	k2eAgInSc1d1
je	být	k5eAaImIp3nS
inspirován	inspirovat	k5eAaBmNgMnS
hřebenem	hřeben	k1gInSc7
mezi	mezi	k7c7
očima	oko	k1gNnPc7
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
slabě	slabě	k6eAd1
připomíná	připomínat	k5eAaImIp3nS
obroučku	obroučka	k1gFnSc4
brýlí	brýle	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zkostnatělá	zkostnatělý	k2eAgFnSc1d1
horní	horní	k2eAgFnSc1d1
víčka	víčko	k1gNnPc1
také	také	k9
poukazují	poukazovat	k5eAaImIp3nP
na	na	k7c4
možnou	možný	k2eAgFnSc4d1
podobnost	podobnost	k1gFnSc4
s	s	k7c7
dinosaury	dinosaurus	k1gMnPc7
z	z	k7c2
rodu	rod	k1gInSc2
Allosaurus	Allosaurus	k1gInSc1
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Několik	několik	k4yIc1
poddruhů	poddruh	k1gInPc2
tohoto	tento	k3xDgMnSc2
kajmana	kajman	k1gMnSc2
se	se	k3xPyFc4
liší	lišit	k5eAaImIp3nS
zbarvením	zbarvení	k1gNnSc7
<g/>
,	,	kIx,
velikostí	velikost	k1gFnSc7
a	a	k8xC
tvaru	tvar	k1gInSc3
lebky	lebka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samci	Samek	k1gMnPc1
dosahují	dosahovat	k5eAaImIp3nP
délky	délka	k1gFnPc4
od	od	k7c2
2	#num#	k4
m	m	kA
do	do	k7c2
2,5	2,5	k4
m	m	kA
<g/>
,	,	kIx,
maximálně	maximálně	k6eAd1
pak	pak	k6eAd1
až	až	k6eAd1
do	do	k7c2
3	#num#	k4
m.	m.	k?
Samice	samice	k1gFnPc1
jsou	být	k5eAaImIp3nP
znatelně	znatelně	k6eAd1
menší	malý	k2eAgFnPc1d2
a	a	k8xC
dosahují	dosahovat	k5eAaImIp3nP
maximální	maximální	k2eAgFnPc4d1
velikosti	velikost	k1gFnPc4
do	do	k7c2
1,4	1,4	k4
m.	m.	k?
Dospělá	dospělý	k2eAgNnPc1d1
zvířata	zvíře	k1gNnPc1
mají	mít	k5eAaImIp3nP
tlustou	tlustý	k2eAgFnSc4d1
kůži	kůže	k1gFnSc4
s	s	k7c7
olivovým	olivový	k2eAgInSc7d1
nádechem	nádech	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mláďata	mládě	k1gNnPc1
mají	mít	k5eAaImIp3nP
obvykle	obvykle	k6eAd1
nažloutlou	nažloutlý	k2eAgFnSc4d1
barvu	barva	k1gFnSc4
s	s	k7c7
černými	černý	k2eAgFnPc7d1
skvrnami	skvrna	k1gFnPc7
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
postupem	postupem	k7c2
času	čas	k1gInSc2
zcela	zcela	k6eAd1
ztrácí	ztrácet	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Potrava	potrava	k1gFnSc1
a	a	k8xC
rozmnožování	rozmnožování	k1gNnSc1
</s>
<s>
Mláďata	mládě	k1gNnPc1
se	se	k3xPyFc4
živí	živit	k5eAaImIp3nP
širokou	široký	k2eAgFnSc7d1
paletou	paleta	k1gFnSc7
vodních	vodní	k2eAgMnPc2d1
bezobratlých	bezobratlí	k1gMnPc2
<g/>
,	,	kIx,
převážně	převážně	k6eAd1
pak	pak	k8xC
larvami	larva	k1gFnPc7
hmyzu	hmyz	k1gInSc2
<g/>
,	,	kIx,
korýši	korýš	k1gMnPc1
a	a	k8xC
měkkýši	měkkýš	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postupem	postupem	k7c2
času	čas	k1gInSc2
začnou	začít	k5eAaPmIp3nP
do	do	k7c2
svého	svůj	k3xOyFgInSc2
jídelníčku	jídelníček	k1gInSc2
zahrnovat	zahrnovat	k5eAaImF
i	i	k9
různé	různý	k2eAgMnPc4d1
obratlovce	obratlovec	k1gMnPc4
<g/>
,	,	kIx,
převážně	převážně	k6eAd1
ryby	ryba	k1gFnPc4
<g/>
,	,	kIx,
obojživelníky	obojživelník	k1gMnPc4
<g/>
,	,	kIx,
plazy	plaz	k1gMnPc4
a	a	k8xC
vodní	vodní	k2eAgMnPc4d1
ptáky	pták	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dospělá	dospělý	k2eAgFnSc1d1
zvířata	zvíře	k1gNnPc1
si	se	k3xPyFc3
troufnou	troufnout	k5eAaPmIp3nP
i	i	k9
na	na	k7c4
větší	veliký	k2eAgMnPc4d2
savce	savec	k1gMnPc4
<g/>
,	,	kIx,
např.	např.	kA
divoká	divoký	k2eAgFnSc1d1
prasata	prase	k1gNnPc4
nebo	nebo	k8xC
kapybary	kapybara	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Období	období	k1gNnSc1
páření	páření	k1gNnSc2
trvá	trvat	k5eAaImIp3nS
u	u	k7c2
kajmanů	kajman	k1gMnPc2
brýlových	brýlový	k2eAgMnPc2d1
v	v	k7c6
rozmezí	rozmezí	k1gNnSc6
mezi	mezi	k7c7
květnem	květen	k1gInSc7
a	a	k8xC
srpnem	srpen	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samice	samice	k1gFnSc1
vybuduje	vybudovat	k5eAaPmIp3nS
hnízdo	hnízdo	k1gNnSc4
v	v	k7c6
podobě	podoba	k1gFnSc6
hromady	hromada	k1gFnSc2
různé	různý	k2eAgFnSc2d1
půdy	půda	k1gFnSc2
a	a	k8xC
rostlinné	rostlinný	k2eAgFnSc2d1
vegetace	vegetace	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
sem	sem	k6eAd1
nahrne	nahrnout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
období	období	k1gNnSc6
dešťů	dešť	k1gInPc2
<g/>
,	,	kIx,
mezi	mezi	k7c7
červencem	červenec	k1gInSc7
a	a	k8xC
srpnem	srpen	k1gInSc7
naklade	naklást	k5eAaPmIp3nS
14	#num#	k4
až	až	k9
40	#num#	k4
(	(	kIx(
<g/>
obvykle	obvykle	k6eAd1
kolem	kolem	k7c2
22	#num#	k4
<g/>
)	)	kIx)
vajec	vejce	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
po	po	k7c4
celou	celý	k2eAgFnSc4d1
inkubační	inkubační	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
trvá	trvat	k5eAaImIp3nS
zhruba	zhruba	k6eAd1
90	#num#	k4
dní	den	k1gInPc2
pečlivě	pečlivě	k6eAd1
střeží	střežit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samice	samice	k1gFnPc1
dosahují	dosahovat	k5eAaImIp3nP
pohlavní	pohlavní	k2eAgFnPc1d1
dospělosti	dospělost	k1gFnPc1
mezi	mezi	k7c7
4	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
7	#num#	k4
<g/>
.	.	kIx.
rokem	rok	k1gInSc7
<g/>
,	,	kIx,
samci	samec	k1gMnPc1
zhruba	zhruba	k6eAd1
ve	v	k7c6
stejném	stejný	k2eAgInSc6d1
věku	věk	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Kajman	kajman	k1gMnSc1
brýlový	brýlový	k2eAgMnSc1d1
je	být	k5eAaImIp3nS
loven	loven	k2eAgInSc1d1
pro	pro	k7c4
svou	svůj	k3xOyFgFnSc4
kůži	kůže	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
ohrožení	ohrožení	k1gNnSc1
pro	pro	k7c4
něj	on	k3xPp3gMnSc4
představuje	představovat	k5eAaImIp3nS
i	i	k9
odchyt	odchyt	k1gInSc1
mláďat	mládě	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yRgNnPc1,k3yQgNnPc1,k3yIgNnPc1
se	se	k3xPyFc4
následně	následně	k6eAd1
prodávají	prodávat	k5eAaImIp3nP
do	do	k7c2
domácností	domácnost	k1gFnPc2
buď	buď	k8xC
živí	živit	k5eAaImIp3nS
<g/>
,	,	kIx,
nebo	nebo	k8xC
jako	jako	k9
kuriózní	kuriózní	k2eAgFnPc4d1
vycpaniny	vycpanina	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Spectacled	Spectacled	k1gMnSc1
Caiman	Caiman	k1gMnSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
The	The	k1gFnSc1
IUCN	IUCN	kA
Red	Red	k1gFnSc1
List	list	k1gInSc1
of	of	k?
Threatened	Threatened	k1gInSc1
Species	species	k1gFnSc1
2021.1	2021.1	k4
<g/>
.	.	kIx.
25	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
http://www.flmnh.ufl.edu/cnhc/csp_ccro.htm	http://www.flmnh.ufl.edu/cnhc/csp_ccro.htm	k1gInSc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Igor	Igor	k1gMnSc1
J.	J.	kA
Roberto	Roberta	k1gFnSc5
<g/>
,	,	kIx,
Pedro	Pedra	k1gFnSc5
S.	S.	kA
Bittencourt	Bittencourt	k1gInSc1
<g/>
,	,	kIx,
Fabio	Fabio	k1gMnSc1
L.	L.	kA
Muniz	Muniz	k1gMnSc1
<g/>
,	,	kIx,
Sandra	Sandra	k1gFnSc1
M.	M.	kA
Hernández-Rangel	Hernández-Rangela	k1gFnPc2
<g/>
,	,	kIx,
Yhuri	Yhur	k1gFnPc1
C.	C.	kA
Nóbrega	Nóbreg	k1gMnSc4
<g/>
,	,	kIx,
Robson	Robson	k1gMnSc1
W.	W.	kA
Ávila	Ávila	k1gMnSc1
<g/>
,	,	kIx,
Bruno	Bruno	k1gMnSc1
C.	C.	kA
Souza	Souza	k1gFnSc1
<g/>
,	,	kIx,
Gustavo	Gustava	k1gFnSc5
Alvarez	Alvarez	k1gInSc4
<g/>
,	,	kIx,
Guido	Guido	k1gNnSc4
Miranda-Chumacero	Miranda-Chumacero	k1gNnSc1
<g/>
,	,	kIx,
Zilca	Zilca	k1gMnSc1
Campos	Campos	k1gMnSc1
<g/>
,	,	kIx,
Izeni	Izeen	k2eAgMnPc1d1
P.	P.	kA
Farias	Farias	k1gMnSc1
&	&	k?
Tomas	Tomas	k1gMnSc1
Hrbek	Hrbek	k1gMnSc1
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Unexpected	Unexpected	k1gInSc1
but	but	k?
unsurprising	unsurprising	k1gInSc1
lineage	lineagat	k5eAaPmIp3nS
diversity	diversit	k1gInPc4
within	withina	k1gFnPc2
the	the	k?
most	most	k1gInSc1
widespread	widespread	k1gInSc1
Neotropical	Neotropical	k1gFnSc1
crocodilian	crocodilian	k1gMnSc1
genus	genus	k1gMnSc1
Caiman	Caiman	k1gMnSc1
(	(	kIx(
<g/>
Crocodylia	Crocodylia	k1gFnSc1
<g/>
,	,	kIx,
Alligatoridae	Alligatoridae	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Systematics	Systematics	k1gInSc4
and	and	k?
Biodiversity	Biodiversit	k1gInPc4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
https://doi.org/10.1080/14772000.2020.1769222	https://doi.org/10.1080/14772000.2020.1769222	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
kajman	kajman	k1gMnSc1
brýlový	brýlový	k2eAgMnSc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Taxon	taxon	k1gInSc1
Caiman	Caiman	k1gMnSc1
crocodilus	crocodilus	k1gMnSc1
ve	v	k7c6
Wikidruzích	Wikidruze	k1gFnPc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Krokodýli	krokodýl	k1gMnPc1
(	(	kIx(
<g/>
Crocodilia	Crocodilia	k1gFnSc1
<g/>
)	)	kIx)
čeleď	čeleď	k1gFnSc1
gaviálovití	gaviálovitý	k2eAgMnPc1d1
(	(	kIx(
<g/>
Gavialidae	Gavialidae	k1gNnSc7
<g/>
)	)	kIx)
</s>
<s>
rod	rod	k1gInSc1
gaviál	gaviál	k1gMnSc1
(	(	kIx(
<g/>
Gavialis	Gavialis	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
druh	druh	k1gInSc1
<g/>
:	:	kIx,
gaviál	gaviál	k1gMnSc1
indický	indický	k2eAgMnSc1d1
(	(	kIx(
<g/>
Gavialis	Gavialis	k1gInSc1
gangeticus	gangeticus	k1gInSc1
<g/>
)	)	kIx)
rod	rod	k1gInSc1
tomistoma	tomistoma	k1gNnSc1
(	(	kIx(
<g/>
Tomistoma	Tomistoma	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
druh	druh	k1gInSc1
<g/>
:	:	kIx,
tomistoma	tomistoma	k1gFnSc1
úzkohlavá	úzkohlavý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Tomistoma	Tomistoma	k1gFnSc1
schlegelii	schlegelium	k1gNnPc7
<g/>
)	)	kIx)
</s>
<s>
čeleď	čeleď	k1gFnSc1
aligátorovití	aligátorovitý	k2eAgMnPc1d1
(	(	kIx(
<g/>
Alligatoridae	Alligatoridae	k1gNnSc7
<g/>
)	)	kIx)
</s>
<s>
rod	rod	k1gInSc1
aligátor	aligátor	k1gMnSc1
(	(	kIx(
<g/>
Alligator	Alligator	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
druhy	druh	k1gInPc1
<g/>
:	:	kIx,
aligátor	aligátor	k1gMnSc1
severoamerický	severoamerický	k2eAgMnSc1d1
(	(	kIx(
<g/>
Alligator	Alligator	k1gInSc1
mississippiensis	mississippiensis	k1gFnSc2
<g/>
)	)	kIx)
•	•	k?
aligátor	aligátor	k1gMnSc1
čínský	čínský	k2eAgMnSc1d1
(	(	kIx(
<g/>
Alligator	Alligator	k1gInSc1
sinensis	sinensis	k1gInSc1
<g/>
)	)	kIx)
rod	rod	k1gInSc1
Paleosuchus	Paleosuchus	k1gInSc1
</s>
<s>
druhy	druh	k1gInPc1
<g/>
:	:	kIx,
kajman	kajman	k1gMnSc1
trpasličí	trpasličí	k2eAgMnSc1d1
(	(	kIx(
<g/>
Paleosuchus	Paleosuchus	k1gMnSc1
palpebrosus	palpebrosus	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
kajman	kajman	k1gMnSc1
klínohlavý	klínohlavý	k2eAgMnSc1d1
(	(	kIx(
<g/>
Paleosuchus	Paleosuchus	k1gMnSc1
trigonatus	trigonatus	k1gMnSc1
<g/>
)	)	kIx)
rod	rod	k1gInSc1
kajman	kajman	k1gMnSc1
(	(	kIx(
<g/>
Caiman	Caiman	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
druhy	druh	k1gInPc1
<g/>
:	:	kIx,
kajman	kajman	k1gMnSc1
yakaré	yakarý	k2eAgFnPc1d1
(	(	kIx(
<g/>
Caiman	Caiman	k1gMnSc1
yacare	yacar	k1gMnSc5
<g/>
)	)	kIx)
•	•	k?
kajman	kajman	k1gMnSc1
brýlový	brýlový	k2eAgMnSc1d1
(	(	kIx(
<g/>
Caiman	Caiman	k1gMnSc1
crocodilus	crocodilus	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
kajman	kajman	k1gMnSc1
šíronosý	šíronosý	k2eAgMnSc1d1
(	(	kIx(
<g/>
Caiman	Caiman	k1gMnSc1
latirostris	latirostris	k1gFnSc2
<g/>
)	)	kIx)
rod	rod	k1gInSc1
Melanosuchus	Melanosuchus	k1gInSc1
</s>
<s>
druh	druh	k1gInSc1
<g/>
:	:	kIx,
kajman	kajman	k1gMnSc1
černý	černý	k1gMnSc1
(	(	kIx(
<g/>
Melanosuchus	Melanosuchus	k1gMnSc1
niger	niger	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
čeleď	čeleď	k1gFnSc1
krokodýlovití	krokodýlovitý	k2eAgMnPc1d1
(	(	kIx(
<g/>
Crocodylidae	Crocodylidae	k1gNnSc7
<g/>
)	)	kIx)
</s>
<s>
rod	rod	k1gInSc1
krokodýl	krokodýl	k1gMnSc1
(	(	kIx(
<g/>
Crocodylus	Crocodylus	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
druhy	druh	k1gInPc1
<g/>
:	:	kIx,
krokodýl	krokodýl	k1gMnSc1
americký	americký	k2eAgMnSc1d1
(	(	kIx(
<g/>
Crocodylus	Crocodylus	k1gMnSc1
acutus	acutus	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
krokodýl	krokodýl	k1gMnSc1
orinocký	orinocký	k2eAgMnSc1d1
(	(	kIx(
<g/>
Crocodylus	Crocodylus	k1gMnSc1
intermedius	intermedius	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
krokodýl	krokodýl	k1gMnSc1
Johnstonův	Johnstonův	k2eAgMnSc1d1
(	(	kIx(
<g/>
Crocodylus	Crocodylus	k1gInSc1
johnsoni	johnsoň	k1gFnSc3
<g/>
)	)	kIx)
•	•	k?
krokodýl	krokodýl	k1gMnSc1
filipínský	filipínský	k2eAgMnSc1d1
(	(	kIx(
<g/>
Crocodylus	Crocodylus	k1gInSc1
mindorensis	mindorensis	k1gFnSc2
<g/>
)	)	kIx)
•	•	k?
krokodýl	krokodýl	k1gMnSc1
Moreletův	Moreletův	k2eAgMnSc1d1
(	(	kIx(
<g/>
Crocodylus	Crocodylus	k1gMnSc1
moreletii	moreletie	k1gFnSc4
<g/>
)	)	kIx)
•	•	k?
krokodýl	krokodýl	k1gMnSc1
nilský	nilský	k2eAgMnSc1d1
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
Crocodylus	Crocodylus	k1gMnSc1
niloticus	niloticus	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
krokodýl	krokodýl	k1gMnSc1
západoafrický	západoafrický	k2eAgMnSc1d1
(	(	kIx(
<g/>
Crocodylus	Crocodylus	k1gMnSc1
suchus	suchus	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
krokodýl	krokodýl	k1gMnSc1
novoguinejský	novoguinejský	k2eAgMnSc1d1
(	(	kIx(
<g/>
Crocodylus	Crocodylus	k1gInSc1
novaeguineae	novaeguinea	k1gInSc2
<g/>
)	)	kIx)
•	•	k?
krokodýl	krokodýl	k1gMnSc1
bahenní	bahenní	k2eAgMnSc1d1
(	(	kIx(
<g/>
Crocodylus	Crocodylus	k1gInSc1
palustris	palustris	k1gFnSc2
<g/>
)	)	kIx)
•	•	k?
krokodýl	krokodýl	k1gMnSc1
mořský	mořský	k2eAgMnSc1d1
(	(	kIx(
<g/>
Crocodylus	Crocodylus	k1gMnSc1
porosus	porosus	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
krokodýl	krokodýl	k1gMnSc1
kubánský	kubánský	k2eAgMnSc1d1
(	(	kIx(
<g/>
Crocodylus	Crocodylus	k1gMnSc1
rhombifer	rhombifer	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
krokodýl	krokodýl	k1gMnSc1
siamský	siamský	k2eAgMnSc1d1
(	(	kIx(
<g/>
Crocodylus	Crocodylus	k1gInSc1
siamensis	siamensis	k1gInSc1
<g/>
)	)	kIx)
rod	rod	k1gInSc1
Mecistops	Mecistopsa	k1gFnPc2
</s>
<s>
druh	druh	k1gInSc1
<g/>
:	:	kIx,
krokodýl	krokodýl	k1gMnSc1
štítnatý	štítnatý	k2eAgMnSc1d1
(	(	kIx(
<g/>
Mecistops	Mecistops	k1gInSc1
cataphractus	cataphractus	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
krokodýl	krokodýl	k1gMnSc1
štíhlohlavý	štíhlohlavý	k2eAgMnSc1d1
(	(	kIx(
<g/>
Mecistops	Mecistops	k1gInSc1
leptorhynchus	leptorhynchus	k1gInSc1
<g/>
)	)	kIx)
rod	rod	k1gInSc1
Osteolaemus	Osteolaemus	k1gInSc1
</s>
<s>
druh	druh	k1gInSc1
<g/>
:	:	kIx,
krokodýl	krokodýl	k1gMnSc1
čelnatý	čelnatý	k2eAgMnSc1d1
(	(	kIx(
<g/>
Osteolaemus	Osteolaemus	k1gInSc1
tetraspis	tetraspis	k1gFnSc2
<g/>
)	)	kIx)
•	•	k?
krokodýl	krokodýl	k1gMnSc1
konžský	konžský	k2eAgMnSc1d1
(	(	kIx(
<g/>
Osteolaemus	Osteolaemus	k1gMnSc1
osborni	osbornit	k5eAaPmRp2nS
<g/>
)	)	kIx)
</s>
