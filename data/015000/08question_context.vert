<s>
Kajman	kajman	k1gMnSc1
brýlový	brýlový	k2eAgMnSc1d1
<g/>
,	,	kIx,
kajman	kajman	k1gMnSc1
obecný	obecný	k2eAgMnSc1d1
či	či	k8xC
kajman	kajman	k1gMnSc1
středoamerický	středoamerický	k2eAgMnSc1d1
(	(	kIx(
<g/>
Caiman	Caiman	k1gMnSc1
crocodilus	crocodilus	k1gMnSc1
známý	známý	k1gMnSc1
také	také	k9
pod	pod	k7c7
starším	starý	k2eAgNnSc7d2
jménem	jméno	k1gNnSc7
Caiman	Caiman	k1gMnSc1
sclerops	sclerops	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
poměrně	poměrně	k6eAd1
malý	malý	k2eAgInSc1d1
plaz	plaz	k1gInSc1
z	z	k7c2
čeledi	čeleď	k1gFnSc2
aligátorovitých	aligátorovitý	k2eAgFnPc2d1
obývající	obývající	k2eAgNnSc4d1
poměrně	poměrně	k6eAd1
rozsáhlé	rozsáhlý	k2eAgNnSc4d1
území	území	k1gNnSc4
Střední	střední	k2eAgFnSc2d1
a	a	k8xC
Jižní	jižní	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
v	v	k7c6
rozmezí	rozmezí	k1gNnSc6
od	od	k7c2
Venezuely	Venezuela	k1gFnSc2
až	až	k9
na	na	k7c4
jih	jih	k1gInSc4
Amazonské	amazonský	k2eAgFnSc2d1
pánve	pánev	k1gFnSc2
<g/>
.	.	kIx.
</s>