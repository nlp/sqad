<s>
Saluki	Saluki	k6eAd1	Saluki
<g/>
,	,	kIx,	,
saluka	saluk	k1gMnSc4	saluk
či	či	k8xC	či
perský	perský	k2eAgMnSc1d1	perský
chrt	chrt	k1gMnSc1	chrt
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
íránské	íránský	k2eAgNnSc1d1	íránské
psí	psí	k2eAgNnSc1d1	psí
plemeno	plemeno	k1gNnSc1	plemeno
vyšlechtěné	vyšlechtěný	k2eAgNnSc1d1	vyšlechtěné
především	především	k9	především
pro	pro	k7c4	pro
lov	lov	k1gInSc4	lov
gazel	gazela	k1gFnPc2	gazela
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
chována	chován	k2eAgFnSc1d1	chována
spíše	spíše	k9	spíše
jako	jako	k8xC	jako
společenský	společenský	k2eAgInSc1d1	společenský
nebo	nebo	k8xC	nebo
dostihový	dostihový	k2eAgMnSc1d1	dostihový
pes	pes	k1gMnSc1	pes
<g/>
.	.	kIx.	.
</s>
<s>
Saluki	Saluki	k6eAd1	Saluki
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
staré	starý	k2eAgNnSc4d1	staré
íránské	íránský	k2eAgNnSc4d1	íránské
plemeno	plemeno	k1gNnSc4	plemeno
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
zmínky	zmínka	k1gFnPc1	zmínka
o	o	k7c6	o
podobných	podobný	k2eAgFnPc6d1	podobná
psech	pes	k1gMnPc6	pes
nalezneme	nalézt	k5eAaBmIp1nP	nalézt
již	již	k6eAd1	již
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
4	[number]	k4	4
000	[number]	k4	000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Tyto	tento	k3xDgFnPc1	tento
informace	informace	k1gFnPc1	informace
pocházejí	pocházet	k5eAaImIp3nP	pocházet
hlavně	hlavně	k9	hlavně
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
dnešního	dnešní	k2eAgInSc2d1	dnešní
Egypta	Egypt	k1gInSc2	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Povětšinou	povětšinou	k6eAd1	povětšinou
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
nástěnné	nástěnný	k2eAgFnPc4d1	nástěnná
malby	malba	k1gFnPc4	malba
<g/>
.	.	kIx.	.
</s>
<s>
Psi	pes	k1gMnPc1	pes
velmi	velmi	k6eAd1	velmi
podobni	podoben	k2eAgMnPc1d1	podoben
dnešní	dnešní	k2eAgFnPc1d1	dnešní
saluce	saluce	k1gFnPc1	saluce
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
na	na	k7c6	na
nástěnných	nástěnný	k2eAgFnPc6d1	nástěnná
malbách	malba	k1gFnPc6	malba
v	v	k7c6	v
hrobce	hrobka	k1gFnSc6	hrobka
krále	král	k1gMnSc2	král
Thutmose	Thutmosa	k1gFnSc3	Thutmosa
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byl	být	k5eAaImAgMnS	být
panovník	panovník	k1gMnSc1	panovník
starověkého	starověký	k2eAgInSc2d1	starověký
Egypta	Egypt	k1gInSc2	Egypt
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgMnPc2d3	nejznámější
<g/>
.	.	kIx.	.
</s>
<s>
Islamisté	islamista	k1gMnPc1	islamista
věří	věřit	k5eAaImIp3nP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
psi	pes	k1gMnPc1	pes
a	a	k8xC	a
prasata	prase	k1gNnPc1	prase
jsou	být	k5eAaImIp3nP	být
nečistá	čistý	k2eNgNnPc1d1	nečisté
zvířata	zvíře	k1gNnPc1	zvíře
<g/>
,	,	kIx,	,
právě	právě	k6eAd1	právě
saluce	saluce	k1gFnSc1	saluce
ale	ale	k8xC	ale
byla	být	k5eAaImAgFnS	být
udělena	udělit	k5eAaPmNgFnS	udělit
zvláštní	zvláštní	k2eAgFnSc1d1	zvláštní
"	"	kIx"	"
<g/>
výjimka	výjimka	k1gFnSc1	výjimka
<g/>
"	"	kIx"	"
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
jako	jako	k8xS	jako
jediné	jediný	k2eAgNnSc1d1	jediné
psí	psí	k2eAgNnSc1d1	psí
plemeno	plemeno	k1gNnSc1	plemeno
může	moct	k5eAaImIp3nS	moct
skutečně	skutečně	k6eAd1	skutečně
chovat	chovat	k5eAaImF	chovat
i	i	k9	i
v	v	k7c6	v
muslimské	muslimský	k2eAgFnSc6d1	muslimská
rodině	rodina	k1gFnSc6	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
v	v	k7c6	v
době	doba	k1gFnSc6	doba
Thutmose	Thutmosa	k1gFnSc6	Thutmosa
III	III	kA	III
<g/>
.	.	kIx.	.
existovaly	existovat	k5eAaImAgInP	existovat
dva	dva	k4xCgInPc1	dva
typy	typ	k1gInPc1	typ
saluk	saluk	k6eAd1	saluk
–	–	k?	–
dlouhosrstá	dlouhosrstý	k2eAgFnSc1d1	dlouhosrstá
a	a	k8xC	a
krátkosrstá	krátkosrstý	k2eAgFnSc1d1	krátkosrstá
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
dvě	dva	k4xCgFnPc1	dva
varianty	varianta	k1gFnPc1	varianta
můžeme	moct	k5eAaImIp1nP	moct
nalézt	nalézt	k5eAaPmF	nalézt
dodnes	dodnes	k6eAd1	dodnes
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
rozšířenější	rozšířený	k2eAgFnSc1d2	rozšířenější
je	být	k5eAaImIp3nS	být
varianta	varianta	k1gFnSc1	varianta
dlouhosrstá	dlouhosrstý	k2eAgFnSc1d1	dlouhosrstá
<g/>
.	.	kIx.	.
</s>
<s>
Egypťané	Egypťan	k1gMnPc1	Egypťan
měli	mít	k5eAaImAgMnP	mít
saluky	saluk	k1gInPc4	saluk
velmi	velmi	k6eAd1	velmi
v	v	k7c6	v
lásce	láska	k1gFnSc6	láska
<g/>
,	,	kIx,	,
svědčí	svědčit	k5eAaImIp3nS	svědčit
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
i	i	k9	i
nástěnná	nástěnný	k2eAgFnSc1d1	nástěnná
kresba	kresba	k1gFnSc1	kresba
s	s	k7c7	s
faraónem	faraón	k1gMnSc7	faraón
Antefem	Antef	k1gMnSc7	Antef
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
žil	žít	k5eAaImAgMnS	žít
okolo	okolo	k7c2	okolo
3	[number]	k4	3
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Kresba	kresba	k1gFnSc1	kresba
znázorňuje	znázorňovat	k5eAaImIp3nS	znázorňovat
faraóna	faraón	k1gMnSc2	faraón
sedícího	sedící	k2eAgMnSc2d1	sedící
na	na	k7c6	na
zlatém	zlatý	k2eAgInSc6d1	zlatý
trůnu	trůn	k1gInSc6	trůn
<g/>
,	,	kIx,	,
u	u	k7c2	u
jehož	jehož	k3xOyRp3gFnPc2	jehož
nohou	noha	k1gFnPc2	noha
leží	ležet	k5eAaImIp3nS	ležet
dlouhosrstá	dlouhosrstý	k2eAgNnPc4d1	dlouhosrsté
saluki	saluki	k1gNnPc4	saluki
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
faraónově	faraónův	k2eAgFnSc6d1	faraónova
hrobce	hrobka	k1gFnSc6	hrobka
byl	být	k5eAaImAgInS	být
nalezen	nalézt	k5eAaBmNgInS	nalézt
papyrus	papyrus	k1gInSc1	papyrus
pojednávající	pojednávající	k2eAgInSc1d1	pojednávající
o	o	k7c6	o
faraónově	faraónův	k2eAgFnSc6d1	faraónova
oblíbené	oblíbený	k2eAgFnSc6d1	oblíbená
feně	fena	k1gFnSc6	fena
jménem	jméno	k1gNnSc7	jméno
"	"	kIx"	"
<g/>
Bílá	bílý	k2eAgFnSc1d1	bílá
Gazela	gazela	k1gFnSc1	gazela
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nejvíce	nejvíce	k6eAd1	nejvíce
připomíná	připomínat	k5eAaImIp3nS	připomínat
saluki	saluki	k6eAd1	saluki
nebo	nebo	k8xC	nebo
sloughi	sloughi	k6eAd1	sloughi
<g/>
.	.	kIx.	.
</s>
<s>
Bůh	bůh	k1gMnSc1	bůh
Anubis	Anubis	k1gFnSc2	Anubis
má	mít	k5eAaImIp3nS	mít
lidské	lidský	k2eAgNnSc4d1	lidské
tělo	tělo	k1gNnSc4	tělo
a	a	k8xC	a
hlavu	hlava	k1gFnSc4	hlava
chrta	chrt	k1gMnSc2	chrt
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nápadně	nápadně	k6eAd1	nápadně
připomíná	připomínat	k5eAaImIp3nS	připomínat
tu	tu	k6eAd1	tu
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
mají	mít	k5eAaImIp3nP	mít
saluky	saluk	k1gInPc1	saluk
<g/>
.	.	kIx.	.
</s>
<s>
Saluky	Saluk	k1gInPc1	Saluk
byly	být	k5eAaImAgInP	být
a	a	k8xC	a
pořád	pořád	k6eAd1	pořád
jsou	být	k5eAaImIp3nP	být
oblíbené	oblíbený	k2eAgFnPc1d1	oblíbená
i	i	k9	i
u	u	k7c2	u
Beduínů	Beduín	k1gMnPc2	Beduín
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
u	u	k7c2	u
kmene	kmen	k1gInSc2	kmen
Djafí	Djafí	k1gFnSc2	Djafí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
před	před	k7c7	před
naším	náš	k3xOp1gInSc7	náš
letopočtem	letopočet	k1gInSc7	letopočet
byla	být	k5eAaImAgFnS	být
dobrá	dobrá	k9	dobrá
saluka	saluk	k1gMnSc4	saluk
mnohem	mnohem	k6eAd1	mnohem
cennější	cenný	k2eAgMnSc1d2	cennější
než	než	k8xS	než
velbloud	velbloud	k1gMnSc1	velbloud
a	a	k8xC	a
lidé	člověk	k1gMnPc1	člověk
si	se	k3xPyFc3	se
jí	on	k3xPp3gFnSc7	on
velmi	velmi	k6eAd1	velmi
cenili	cenit	k5eAaImAgMnP	cenit
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
saluki	saluki	k1gNnSc1	saluki
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
ji	on	k3xPp3gFnSc4	on
dovezl	dovézt	k5eAaPmAgInS	dovézt
Hamilton	Hamilton	k1gInSc1	Hamilton
Smith	Smith	k1gInSc1	Smith
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
v	v	k7c4	v
1840	[number]	k4	1840
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
za	za	k7c4	za
první	první	k4xOgFnSc4	první
opravdovou	opravdový	k2eAgFnSc4d1	opravdová
chovatelku	chovatelka	k1gFnSc4	chovatelka
saluk	saluk	k6eAd1	saluk
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
Florence	Florenc	k1gFnPc1	Florenc
Amherst	Amherst	k1gFnSc4	Amherst
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
zabývat	zabývat	k5eAaImF	zabývat
chovem	chov	k1gInSc7	chov
tohoto	tento	k3xDgNnSc2	tento
plemene	plemeno	k1gNnSc2	plemeno
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1895	[number]	k4	1895
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
rok	rok	k1gInSc4	rok
dovezla	dovézt	k5eAaPmAgFnS	dovézt
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
Al	ala	k1gFnPc2	ala
Salihah	Salihaha	k1gFnPc2	Salihaha
pár	pár	k4xCyI	pár
saluk	saluka	k1gFnPc2	saluka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
se	se	k3xPyFc4	se
zase	zase	k9	zase
první	první	k4xOgInPc1	první
saluky	saluk	k1gInPc1	saluk
objevily	objevit	k5eAaPmAgInP	objevit
na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	on	k3xPp3gMnPc4	on
přivezl	přivézt	k5eAaPmAgMnS	přivézt
hrabě	hrabě	k1gMnSc1	hrabě
von	von	k1gInSc4	von
der	drát	k5eAaImRp2nS	drát
Schulenberg	Schulenberg	k1gInSc4	Schulenberg
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
saluki	saluki	k6eAd1	saluki
velmi	velmi	k6eAd1	velmi
oblíbená	oblíbený	k2eAgFnSc1d1	oblíbená
i	i	k9	i
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
několik	několik	k4yIc1	několik
chovných	chovný	k2eAgFnPc2d1	chovná
stanic	stanice	k1gFnPc2	stanice
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
původnímu	původní	k2eAgInSc3d1	původní
účelu	účel	k1gInSc3	účel
lovení	lovení	k1gNnSc2	lovení
gazel	gazel	k1gInSc1	gazel
ji	on	k3xPp3gFnSc4	on
používají	používat	k5eAaImIp3nP	používat
už	už	k6eAd1	už
jen	jen	k9	jen
Beduíni	Beduín	k1gMnPc1	Beduín
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
spíše	spíše	k9	spíše
dostihový	dostihový	k2eAgMnSc1d1	dostihový
pes	pes	k1gMnSc1	pes
a	a	k8xC	a
společník	společník	k1gMnSc1	společník
<g/>
.	.	kIx.	.
</s>
<s>
Saluki	Saluki	k6eAd1	Saluki
má	mít	k5eAaImIp3nS	mít
typickou	typický	k2eAgFnSc4d1	typická
chrtí	chrtí	k2eAgFnSc4d1	chrtí
postavu	postava	k1gFnSc4	postava
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
štíhlá	štíhlý	k2eAgFnSc1d1	štíhlá
<g/>
,	,	kIx,	,
elegantní	elegantní	k2eAgFnSc1d1	elegantní
<g/>
.	.	kIx.	.
</s>
<s>
Hlava	hlava	k1gFnSc1	hlava
je	být	k5eAaImIp3nS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
<g/>
,	,	kIx,	,
nešená	šenat	k5eNaPmIp3nS	šenat
vzpřímeně	vzpřímeně	k6eAd1	vzpřímeně
na	na	k7c6	na
dlouhém	dlouhý	k2eAgInSc6d1	dlouhý
krku	krk	k1gInSc6	krk
<g/>
;	;	kIx,	;
čelní	čelní	k2eAgInSc4d1	čelní
sklon	sklon	k1gInSc4	sklon
mírný	mírný	k2eAgInSc4d1	mírný
<g/>
,	,	kIx,	,
uši	ucho	k1gNnPc4	ucho
svěšené	svěšený	k2eAgFnPc1d1	svěšená
<g/>
.	.	kIx.	.
</s>
<s>
Hrudník	hrudník	k1gInSc1	hrudník
je	být	k5eAaImIp3nS	být
úzký	úzký	k2eAgInSc1d1	úzký
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
hluboký	hluboký	k2eAgInSc1d1	hluboký
<g/>
,	,	kIx,	,
elegantní	elegantní	k2eAgFnSc1d1	elegantní
křivkou	křivka	k1gFnSc7	křivka
přechází	přecházet	k5eAaImIp3nS	přecházet
ve	v	k7c4	v
vtažené	vtažený	k2eAgNnSc4d1	vtažené
břicho	břicho	k1gNnSc4	břicho
<g/>
.	.	kIx.	.
</s>
<s>
Ocas	ocas	k1gInSc1	ocas
je	být	k5eAaImIp3nS	být
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
<g/>
,	,	kIx,	,
nízko	nízko	k6eAd1	nízko
nasazený	nasazený	k2eAgMnSc1d1	nasazený
a	a	k8xC	a
svěšený	svěšený	k2eAgInSc1d1	svěšený
<g/>
.	.	kIx.	.
</s>
<s>
Kohoutková	kohoutkový	k2eAgFnSc1d1	kohoutková
výška	výška	k1gFnSc1	výška
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
60	[number]	k4	60
a	a	k8xC	a
70	[number]	k4	70
cm	cm	kA	cm
<g/>
,	,	kIx,	,
feny	fena	k1gFnPc1	fena
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
i	i	k9	i
menší	malý	k2eAgFnSc1d2	menší
<g/>
.	.	kIx.	.
</s>
<s>
Srst	srst	k1gFnSc1	srst
je	být	k5eAaImIp3nS	být
jemná	jemný	k2eAgFnSc1d1	jemná
a	a	k8xC	a
hladká	hladký	k2eAgFnSc1d1	hladká
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
dvě	dva	k4xCgFnPc1	dva
varianty	varianta	k1gFnPc1	varianta
<g/>
:	:	kIx,	:
první	první	k4xOgFnSc1	první
(	(	kIx(	(
<g/>
běžnější	běžný	k2eAgFnSc1d2	běžnější
<g/>
)	)	kIx)	)
s	s	k7c7	s
třásněmi	třáseň	k1gFnPc7	třáseň
na	na	k7c6	na
uších	ucho	k1gNnPc6	ucho
a	a	k8xC	a
opeřením	opeření	k1gNnSc7	opeření
na	na	k7c6	na
nohách	noha	k1gFnPc6	noha
a	a	k8xC	a
druhá	druhý	k4xOgFnSc1	druhý
zcela	zcela	k6eAd1	zcela
hladkosrstá	hladkosrstý	k2eAgFnSc1d1	hladkosrstá
<g/>
.	.	kIx.	.
</s>
<s>
Barevnost	barevnost	k1gFnSc1	barevnost
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
od	od	k7c2	od
bílé	bílý	k2eAgFnSc2d1	bílá
přes	přes	k7c4	přes
zlatou	zlatý	k2eAgFnSc4d1	zlatá
<g/>
,	,	kIx,	,
rezavou	rezavý	k2eAgFnSc4d1	rezavá
a	a	k8xC	a
černostříbrnou	černostříbrný	k2eAgFnSc4d1	černostříbrná
až	až	k6eAd1	až
k	k	k7c3	k
trikolorní	trikolorní	k2eAgFnSc3d1	trikolorní
(	(	kIx(	(
<g/>
černá	černý	k2eAgFnSc1d1	černá
<g/>
,	,	kIx,	,
bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
,	,	kIx,	,
tříslová	tříslový	k2eAgFnSc1d1	tříslová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oči	oko	k1gNnPc1	oko
a	a	k8xC	a
nos	nos	k1gInSc1	nos
jsou	být	k5eAaImIp3nP	být
tmavé	tmavý	k2eAgFnPc1d1	tmavá
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
ostatní	ostatní	k2eAgMnPc1d1	ostatní
chrti	chrt	k1gMnPc1	chrt
je	on	k3xPp3gFnPc4	on
saluki	saluki	k6eAd1	saluki
nezávislý	závislý	k2eNgMnSc1d1	nezávislý
<g/>
,	,	kIx,	,
aristokratický	aristokratický	k2eAgMnSc1d1	aristokratický
pes	pes	k1gMnSc1	pes
se	s	k7c7	s
silným	silný	k2eAgInSc7d1	silný
loveckým	lovecký	k2eAgInSc7d1	lovecký
instinktem	instinkt	k1gInSc7	instinkt
<g/>
.	.	kIx.	.
</s>
<s>
Doma	doma	k6eAd1	doma
je	být	k5eAaImIp3nS	být
klidná	klidný	k2eAgFnSc1d1	klidná
<g/>
,	,	kIx,	,
k	k	k7c3	k
cizím	cizí	k2eAgMnPc3d1	cizí
lidem	člověk	k1gMnPc3	člověk
se	se	k3xPyFc4	se
chová	chovat	k5eAaImIp3nS	chovat
odměřeně	odměřeně	k6eAd1	odměřeně
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
ní	on	k3xPp3gFnSc6	on
i	i	k8xC	i
o	o	k7c4	o
ostatních	ostatní	k2eAgInPc2d1	ostatní
chrtech	chrt	k1gMnPc6	chrt
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
loví	lovit	k5eAaImIp3nS	lovit
očima	oko	k1gNnPc7	oko
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
něco	něco	k3yInSc4	něco
uvidí	uvidět	k5eAaPmIp3nS	uvidět
<g/>
,	,	kIx,	,
rozběhne	rozběhnout	k5eAaPmIp3nS	rozběhnout
se	se	k3xPyFc4	se
za	za	k7c7	za
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
není	být	k5eNaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
pouštět	pouštět	k5eAaImF	pouštět
ji	on	k3xPp3gFnSc4	on
z	z	k7c2	z
vodítka	vodítko	k1gNnSc2	vodítko
–	–	k?	–
je	být	k5eAaImIp3nS	být
totiž	totiž	k9	totiž
velmi	velmi	k6eAd1	velmi
těžké	těžký	k2eAgNnSc1d1	těžké
ji	on	k3xPp3gFnSc4	on
přivolat	přivolat	k5eAaPmF	přivolat
<g/>
.	.	kIx.	.
</s>
<s>
Saluka	Saluk	k1gMnSc4	Saluk
nemá	mít	k5eNaImIp3nS	mít
ve	v	k7c6	v
zvyku	zvyk	k1gInSc6	zvyk
pána	pán	k1gMnSc4	pán
otrocky	otrocky	k6eAd1	otrocky
poslouchat	poslouchat	k5eAaImF	poslouchat
a	a	k8xC	a
neudělá	udělat	k5eNaPmIp3nS	udělat
vše	všechen	k3xTgNnSc1	všechen
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
mu	on	k3xPp3gMnSc3	on
na	na	k7c6	na
očích	oko	k1gNnPc6	oko
uvidí	uvidět	k5eAaPmIp3nP	uvidět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
lze	lze	k6eAd1	lze
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
vychovat	vychovat	k5eAaPmF	vychovat
alespoň	alespoň	k9	alespoň
do	do	k7c2	do
takové	takový	k3xDgFnSc2	takový
míry	míra	k1gFnSc2	míra
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
dobrou	dobrý	k2eAgFnSc7d1	dobrá
společnicí	společnice	k1gFnSc7	společnice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
jiných	jiný	k2eAgNnPc2d1	jiné
plemen	plemeno	k1gNnPc2	plemeno
psu	pes	k1gMnSc6	pes
je	on	k3xPp3gMnPc4	on
saluka	saluk	k1gMnSc2	saluk
svobodomyslná	svobodomyslný	k2eAgFnSc1d1	svobodomyslná
a	a	k8xC	a
nezávislá	závislý	k2eNgFnSc1d1	nezávislá
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
výcvik	výcvik	k1gInSc1	výcvik
ignoruje	ignorovat	k5eAaImIp3nS	ignorovat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vhodná	vhodný	k2eAgFnSc1d1	vhodná
k	k	k7c3	k
hodným	hodný	k2eAgFnPc3d1	hodná
dětem	dítě	k1gFnPc3	dítě
-	-	kIx~	-
děti	dítě	k1gFnPc4	dítě
má	mít	k5eAaImIp3nS	mít
moc	moc	k6eAd1	moc
ráda	rád	k2eAgFnSc1d1	ráda
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
však	však	k9	však
dominantní	dominantní	k2eAgNnSc1d1	dominantní
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
hrdá	hrdat	k5eAaImIp3nS	hrdat
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
nemá	mít	k5eNaImIp3nS	mít
ráda	rád	k2eAgFnSc1d1	ráda
necitlivé	citlivý	k2eNgFnPc4d1	necitlivá
dětské	dětský	k2eAgFnPc4d1	dětská
hry	hra	k1gFnPc4	hra
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
tahání	tahání	k1gNnSc1	tahání
za	za	k7c4	za
ocas	ocas	k1gInSc4	ocas
nebo	nebo	k8xC	nebo
za	za	k7c4	za
uši	ucho	k1gNnPc4	ucho
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgMnPc4d1	ostatní
psy	pes	k1gMnPc4	pes
ignoruje	ignorovat	k5eAaImIp3nS	ignorovat
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
hodí	hodit	k5eAaImIp3nS	hodit
k	k	k7c3	k
psím	psí	k2eAgInPc3d1	psí
dostihům	dostih	k1gInPc3	dostih
<g/>
.	.	kIx.	.
</s>
<s>
Nemá	mít	k5eNaImIp3nS	mít
s	s	k7c7	s
cizími	cizí	k2eAgMnPc7d1	cizí
psy	pes	k1gMnPc7	pes
problém	problém	k1gInSc4	problém
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přehnaně	přehnaně	k6eAd1	přehnaně
dominantní	dominantní	k2eAgMnSc1d1	dominantní
nebo	nebo	k8xC	nebo
povyšující	povyšující	k2eAgMnSc1d1	povyšující
se	se	k3xPyFc4	se
jedince	jedinko	k6eAd1	jedinko
nestrpí	strpět	k5eNaPmIp3nS	strpět
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vhodná	vhodný	k2eAgFnSc1d1	vhodná
k	k	k7c3	k
jiným	jiný	k2eAgNnPc3d1	jiné
domácím	domácí	k2eAgNnPc3d1	domácí
zvířatům	zvíře	k1gNnPc3	zvíře
-	-	kIx~	-
s	s	k7c7	s
kočkami	kočka	k1gFnPc7	kočka
vychází	vycházet	k5eAaImIp3nS	vycházet
velmi	velmi	k6eAd1	velmi
přátelsky	přátelsky	k6eAd1	přátelsky
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
cizím	cizí	k2eAgMnPc3d1	cizí
lidem	člověk	k1gMnPc3	člověk
se	se	k3xPyFc4	se
chová	chovat	k5eAaImIp3nS	chovat
odtažitě	odtažitě	k6eAd1	odtažitě
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ne	ne	k9	ne
bojácně	bojácně	k6eAd1	bojácně
nebo	nebo	k8xC	nebo
agresivně	agresivně	k6eAd1	agresivně
<g/>
.	.	kIx.	.
</s>
<s>
Neštěká	štěkat	k5eNaImIp3nS	štěkat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
nezávislá	závislý	k2eNgFnSc1d1	nezávislá
<g/>
,	,	kIx,	,
nehodí	hodit	k5eNaPmIp3nS	hodit
se	se	k3xPyFc4	se
na	na	k7c4	na
hlídání	hlídání	k1gNnSc4	hlídání
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Dlouhosrstá	dlouhosrstý	k2eAgFnSc1d1	dlouhosrstá
i	i	k8xC	i
krátkosrstá	krátkosrstý	k2eAgFnSc1d1	krátkosrstá
varianta	varianta	k1gFnSc1	varianta
líná	línat	k5eAaImIp3nS	línat
pravidelně	pravidelně	k6eAd1	pravidelně
2	[number]	k4	2
<g/>
x	x	k?	x
do	do	k7c2	do
roka	rok	k1gInSc2	rok
(	(	kIx(	(
<g/>
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
a	a	k8xC	a
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jen	jen	k9	jen
minimálně	minimálně	k6eAd1	minimálně
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhosrstá	dlouhosrstý	k2eAgFnSc1d1	dlouhosrstá
varianta	varianta	k1gFnSc1	varianta
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
občasné	občasný	k2eAgNnSc4d1	občasné
pročesávání	pročesávání	k1gNnSc4	pročesávání
a	a	k8xC	a
koupání	koupání	k1gNnSc4	koupání
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
srst	srst	k1gFnSc4	srst
krátkosrsté	krátkosrstý	k2eAgFnSc2d1	krátkosrstá
varianty	varianta	k1gFnSc2	varianta
nepotřebuje	potřebovat	k5eNaImIp3nS	potřebovat
skoro	skoro	k6eAd1	skoro
žádnou	žádný	k3yNgFnSc4	žádný
péči	péče	k1gFnSc4	péče
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
chrtů	chrt	k1gMnPc2	chrt
a	a	k8xC	a
tím	ten	k3xDgInSc7	ten
pádem	pád	k1gInSc7	pád
i	i	k8xC	i
saluky	saluk	k1gInPc1	saluk
nemají	mít	k5eNaImIp3nP	mít
rády	rád	k2eAgFnPc4d1	ráda
vodu	voda	k1gFnSc4	voda
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
ani	ani	k8xC	ani
koupání	koupání	k1gNnSc1	koupání
<g/>
.	.	kIx.	.
</s>
<s>
Vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
hodně	hodně	k6eAd1	hodně
pohybu	pohyb	k1gInSc2	pohyb
<g/>
,	,	kIx,	,
nejlépe	dobře	k6eAd3	dobře
volného	volný	k2eAgInSc2d1	volný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ten	ten	k3xDgInSc4	ten
je	být	k5eAaImIp3nS	být
těžké	těžký	k2eAgNnSc1d1	těžké
jí	jíst	k5eAaImIp3nS	jíst
dopřát	dopřát	k5eAaPmF	dopřát
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
hlavně	hlavně	k9	hlavně
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
často	často	k6eAd1	často
utíká	utíkat	k5eAaImIp3nS	utíkat
a	a	k8xC	a
přivolání	přivolání	k1gNnSc1	přivolání
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
těžké	těžký	k2eAgNnSc1d1	těžké
<g/>
.	.	kIx.	.
</s>
<s>
Takže	takže	k9	takže
se	se	k3xPyFc4	se
nedoporučuje	doporučovat	k5eNaImIp3nS	doporučovat
pouštět	pouštět	k5eAaImF	pouštět
jí	jíst	k5eAaImIp3nS	jíst
z	z	k7c2	z
vodítka	vodítko	k1gNnSc2	vodítko
<g/>
.	.	kIx.	.
</s>
<s>
Hodí	hodit	k5eAaImIp3nS	hodit
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
chůzi	chůze	k1gFnSc4	chůze
nebo	nebo	k8xC	nebo
běh	běh	k1gInSc4	běh
při	při	k7c6	při
kole	kolo	k1gNnSc6	kolo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ne	ne	k9	ne
pro	pro	k7c4	pro
plavání	plavání	k1gNnPc4	plavání
nebo	nebo	k8xC	nebo
horské	horský	k2eAgFnPc4d1	horská
túry	túra	k1gFnPc4	túra
<g/>
.	.	kIx.	.
</s>
<s>
Ja	Ja	k?	Ja
vhodná	vhodný	k2eAgNnPc1d1	vhodné
pro	pro	k7c4	pro
psí	psí	k2eAgInPc4d1	psí
dostihy	dostih	k1gInPc4	dostih
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Saluki	Saluk	k1gFnSc2	Saluk
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
http://saluki.name/	[url]	k?	http://saluki.name/
Czech	Czech	k1gMnSc1	Czech
Saluki	Saluk	k1gFnSc2	Saluk
Fans	Fans	k1gInSc1	Fans
http://www.saluki.cz/	[url]	k?	http://www.saluki.cz/
Saluki	Saluk	k1gFnSc2	Saluk
Klub	klub	k1gInSc1	klub
ČR	ČR	kA	ČR
</s>
