<s>
Nové	Nové	k2eAgNnSc1d1	Nové
Město	město	k1gNnSc1	město
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Neustadt	Neustadt	k2eAgInSc1d1	Neustadt
in	in	k?	in
Mähren	Mährno	k1gNnPc2	Mährno
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Žďár	Žďár	k1gInSc1	Žďár
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
v	v	k7c6	v
kraji	kraj	k1gInSc6	kraj
Vysočina	vysočina	k1gFnSc1	vysočina
<g/>
,	,	kIx,	,
10	[number]	k4	10
km	km	kA	km
východně	východně	k6eAd1	východně
od	od	k7c2	od
Žďáru	Žďár	k1gInSc2	Žďár
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
<g/>
,	,	kIx,	,
na	na	k7c6	na
jižním	jižní	k2eAgInSc6d1	jižní
okraji	okraj	k1gInSc6	okraj
Žďárských	Žďárských	k2eAgInPc2d1	Žďárských
vrchů	vrch	k1gInPc2	vrch
<g/>
.	.	kIx.	.
</s>
