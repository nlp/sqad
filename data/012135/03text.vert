<p>
<s>
Oldřich	Oldřich	k1gMnSc1	Oldřich
(	(	kIx(	(
<g/>
†	†	k?	†
9	[number]	k4	9
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1034	[number]	k4	1034
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
kníže	kníže	k1gMnSc1	kníže
(	(	kIx(	(
<g/>
1012	[number]	k4	1012
<g/>
–	–	k?	–
<g/>
1033	[number]	k4	1033
a	a	k8xC	a
1034	[number]	k4	1034
<g/>
)	)	kIx)	)
z	z	k7c2	z
dynastie	dynastie	k1gFnSc2	dynastie
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
synem	syn	k1gMnSc7	syn
Boleslava	Boleslav	k1gMnSc4	Boleslav
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
zřejmě	zřejmě	k6eAd1	zřejmě
jeho	jeho	k3xOp3gNnSc1	jeho
druhé	druhý	k4xOgNnSc1	druhý
<g/>
,	,	kIx,	,
neznámé	známý	k2eNgFnSc2d1	neznámá
ženy	žena	k1gFnSc2	žena
či	či	k8xC	či
teoreticky	teoreticky	k6eAd1	teoreticky
i	i	k9	i
jeho	jeho	k3xOp3gFnPc1	jeho
třetí	třetí	k4xOgFnPc1	třetí
manželky	manželka	k1gFnPc1	manželka
Emmy	Emma	k1gFnSc2	Emma
Franské	franský	k2eAgInPc1d1	franský
<g/>
.	.	kIx.	.
</s>
<s>
Oldřich	Oldřich	k1gMnSc1	Oldřich
byl	být	k5eAaImAgMnS	být
mladším	mladý	k2eAgMnSc7d2	mladší
bratrem	bratr	k1gMnSc7	bratr
knížat	kníže	k1gNnPc2	kníže
Boleslava	Boleslav	k1gMnSc2	Boleslav
III	III	kA	III
<g/>
.	.	kIx.	.
a	a	k8xC	a
Jaromíra	Jaromír	k1gMnSc4	Jaromír
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
první	první	k4xOgFnSc7	první
manželkou	manželka	k1gFnSc7	manželka
byla	být	k5eAaImAgFnS	být
zřejmě	zřejmě	k6eAd1	zřejmě
Juta	juta	k1gFnSc1	juta
(	(	kIx(	(
<g/>
byla	být	k5eAaImAgFnS	být
německého	německý	k2eAgInSc2d1	německý
původu	původ	k1gInSc2	původ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tou	ten	k3xDgFnSc7	ten
druhou	druhý	k4xOgFnSc7	druhý
byla	být	k5eAaImAgFnS	být
zřejmě	zřejmě	k6eAd1	zřejmě
vesnická	vesnický	k2eAgFnSc1d1	vesnická
žena	žena	k1gFnSc1	žena
Božena	Božena	k1gFnSc1	Božena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kníže	kníže	k1gMnSc1	kníže
Oldřich	Oldřich	k1gMnSc1	Oldřich
vyvedl	vyvést	k5eAaPmAgMnS	vyvést
zemi	zem	k1gFnSc4	zem
z	z	k7c2	z
krize	krize	k1gFnSc2	krize
způsobené	způsobený	k2eAgFnSc2d1	způsobená
boji	boj	k1gInSc6	boj
o	o	k7c4	o
moc	moc	k1gFnSc4	moc
mezi	mezi	k7c7	mezi
syny	syn	k1gMnPc7	syn
Boleslava	Boleslav	k1gMnSc2	Boleslav
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
k	k	k7c3	k
Čechám	Čechy	k1gFnPc3	Čechy
trvale	trvale	k6eAd1	trvale
připojil	připojit	k5eAaPmAgInS	připojit
Moravu	Morava	k1gFnSc4	Morava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Před	před	k7c7	před
nástupem	nástup	k1gInSc7	nástup
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
==	==	k?	==
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1001	[number]	k4	1001
uprchl	uprchnout	k5eAaPmAgInS	uprchnout
s	s	k7c7	s
Jaromírem	Jaromír	k1gMnSc7	Jaromír
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc1	jejich
(	(	kIx(	(
<g/>
nevlastní	vlastnit	k5eNaImIp3nS	vlastnit
<g/>
)	)	kIx)	)
matkou	matka	k1gFnSc7	matka
Emmou	Emma	k1gFnSc7	Emma
před	před	k7c7	před
terorem	teror	k1gInSc7	teror
Boleslava	Boleslav	k1gMnSc2	Boleslav
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
mínil	mínit	k5eAaImAgMnS	mínit
nechat	nechat	k5eAaPmF	nechat
Oldřicha	Oldřich	k1gMnSc4	Oldřich
<g/>
,	,	kIx,	,
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
už	už	k9	už
možná	možná	k9	možná
otce	otec	k1gMnSc4	otec
levobočného	levobočný	k2eAgMnSc4d1	levobočný
syna	syn	k1gMnSc4	syn
Břetislava	Břetislav	k1gMnSc4	Břetislav
<g/>
,	,	kIx,	,
zavraždit	zavraždit	k5eAaPmF	zavraždit
<g/>
.	.	kIx.	.
</s>
<s>
Oldřich	Oldřich	k1gMnSc1	Oldřich
sice	sice	k8xC	sice
byl	být	k5eAaImAgMnS	být
podle	podle	k7c2	podle
všeho	všecek	k3xTgInSc2	všecek
již	již	k6eAd1	již
ženatý	ženatý	k2eAgInSc1d1	ženatý
<g/>
,	,	kIx,	,
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
ale	ale	k8xC	ale
neměl	mít	k5eNaImAgMnS	mít
příliš	příliš	k6eAd1	příliš
dobré	dobrý	k2eAgFnPc4d1	dobrá
vyhlídky	vyhlídka	k1gFnPc4	vyhlídka
na	na	k7c4	na
knížecí	knížecí	k2eAgInSc4d1	knížecí
stolec	stolec	k1gInSc4	stolec
<g/>
.	.	kIx.	.
</s>
<s>
Nedalo	dát	k5eNaPmAgNnS	dát
se	se	k3xPyFc4	se
předpokládat	předpokládat	k5eAaImF	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gMnSc1	jeho
nemanželský	manželský	k2eNgMnSc1d1	nemanželský
syn	syn	k1gMnSc1	syn
v	v	k7c6	v
budoucnosti	budoucnost	k1gFnSc6	budoucnost
sehraje	sehrát	k5eAaPmIp3nS	sehrát
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
dějinách	dějiny	k1gFnPc6	dějiny
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Útočištěm	útočiště	k1gNnSc7	útočiště
se	se	k3xPyFc4	se
oběma	dva	k4xCgMnPc7	dva
bratrům	bratr	k1gMnPc3	bratr
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc3	jejich
matce	matka	k1gFnSc3	matka
stal	stát	k5eAaPmAgInS	stát
dvůr	dvůr	k1gInSc1	dvůr
bavorského	bavorský	k2eAgMnSc2d1	bavorský
vévody	vévoda	k1gMnSc2	vévoda
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
stal	stát	k5eAaPmAgMnS	stát
římskoněmeckým	římskoněmecký	k2eAgMnSc7d1	římskoněmecký
králem	král	k1gMnSc7	král
jako	jako	k8xC	jako
Jindřich	Jindřich	k1gMnSc1	Jindřich
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
dosazeného	dosazený	k2eAgMnSc2d1	dosazený
knížete	kníže	k1gMnSc2	kníže
Vladivoje	Vladivoj	k1gInSc2	Vladivoj
roku	rok	k1gInSc2	rok
1003	[number]	k4	1003
se	se	k3xPyFc4	se
krátce	krátce	k6eAd1	krátce
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
brzy	brzy	k6eAd1	brzy
však	však	k9	však
musel	muset	k5eAaImAgInS	muset
znovu	znovu	k6eAd1	znovu
prchnout	prchnout	k5eAaPmF	prchnout
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Jaromírem	Jaromír	k1gMnSc7	Jaromír
před	před	k7c7	před
Boleslavem	Boleslav	k1gMnSc7	Boleslav
III	III	kA	III
<g/>
.	.	kIx.	.
a	a	k8xC	a
Boleslavem	Boleslav	k1gMnSc7	Boleslav
Chrabrým	chrabrý	k2eAgMnSc7d1	chrabrý
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
1004	[number]	k4	1004
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
vrátit	vrátit	k5eAaPmF	vrátit
<g/>
,	,	kIx,	,
když	když	k8xS	když
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
Jindřicha	Jindřich	k1gMnSc2	Jindřich
II	II	kA	II
<g/>
.	.	kIx.	.
byli	být	k5eAaImAgMnP	být
z	z	k7c2	z
Čech	Čechy	k1gFnPc2	Čechy
vypuzeni	vypuzen	k2eAgMnPc1d1	vypuzen
Poláci	Polák	k1gMnPc1	Polák
<g/>
.	.	kIx.	.
</s>
<s>
Knížetem	kníže	k1gMnSc7	kníže
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Jaromír	Jaromír	k1gMnSc1	Jaromír
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Oldřich	Oldřich	k1gMnSc1	Oldřich
knížetem	kníže	k1gNnSc7wR	kníže
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1012	[number]	k4	1012
sesadil	sesadit	k5eAaPmAgMnS	sesadit
Oldřich	Oldřich	k1gMnSc1	Oldřich
svého	své	k1gNnSc2	své
bratra	bratr	k1gMnSc2	bratr
Jaromíra	Jaromír	k1gMnSc2	Jaromír
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
postavil	postavit	k5eAaPmAgMnS	postavit
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
povstání	povstání	k1gNnSc2	povstání
proti	proti	k7c3	proti
němu	on	k3xPp3gInSc3	on
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
knížetem	kníže	k1gMnSc7	kníže
a	a	k8xC	a
od	od	k7c2	od
krále	král	k1gMnSc2	král
Jindřicha	Jindřich	k1gMnSc2	Jindřich
II	II	kA	II
<g/>
.	.	kIx.	.
přijal	přijmout	k5eAaPmAgMnS	přijmout
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
Čechy	Čechy	k1gFnPc1	Čechy
v	v	k7c4	v
léno	léno	k1gNnSc4	léno
<g/>
.	.	kIx.	.
</s>
<s>
Projevil	projevit	k5eAaPmAgMnS	projevit
se	se	k3xPyFc4	se
jako	jako	k9	jako
energický	energický	k2eAgMnSc1d1	energický
a	a	k8xC	a
tvrdý	tvrdý	k2eAgMnSc1d1	tvrdý
panovník	panovník	k1gMnSc1	panovník
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1014	[number]	k4	1014
zlikvidoval	zlikvidovat	k5eAaPmAgMnS	zlikvidovat
opozici	opozice	k1gFnSc4	opozice
a	a	k8xC	a
Jaromírovy	Jaromírův	k2eAgMnPc4d1	Jaromírův
stoupence	stoupenec	k1gMnPc4	stoupenec
–	–	k?	–
tzv.	tzv.	kA	tzv.
druhé	druhý	k4xOgNnSc4	druhý
vraždění	vraždění	k1gNnSc4	vraždění
Vršovců	Vršovec	k1gInPc2	Vršovec
<g/>
.	.	kIx.	.
</s>
<s>
Údajně	údajně	k6eAd1	údajně
se	se	k3xPyFc4	se
tak	tak	k9	tak
stalo	stát	k5eAaPmAgNnS	stát
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
podporovali	podporovat	k5eAaImAgMnP	podporovat
jeho	jeho	k3xOp3gMnSc4	jeho
bratra	bratr	k1gMnSc4	bratr
Jaromíra	Jaromír	k1gMnSc4	Jaromír
<g/>
,	,	kIx,	,
tou	ten	k3xDgFnSc7	ten
dobou	doba	k1gFnSc7	doba
ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
v	v	k7c6	v
Utrechtu	Utrecht	k1gInSc6	Utrecht
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
neúspěšném	úspěšný	k2eNgInSc6d1	neúspěšný
Oldřichovu	Oldřichův	k2eAgFnSc4d1	Oldřichova
vojenském	vojenský	k2eAgNnSc6d1	vojenské
tažení	tažení	k1gNnSc6	tažení
na	na	k7c4	na
Moravu	Morava	k1gFnSc4	Morava
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1015	[number]	k4	1015
roku	rok	k1gInSc2	rok
1017	[number]	k4	1017
úspěšně	úspěšně	k6eAd1	úspěšně
odrazil	odrazit	k5eAaPmAgInS	odrazit
vojenský	vojenský	k2eAgInSc1d1	vojenský
vpád	vpád	k1gInSc1	vpád
Boleslava	Boleslav	k1gMnSc2	Boleslav
Chrabrého	chrabrý	k2eAgMnSc2d1	chrabrý
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1031	[number]	k4	1031
Oldřich	Oldřich	k1gMnSc1	Oldřich
postupně	postupně	k6eAd1	postupně
dobyl	dobýt	k5eAaPmAgMnS	dobýt
na	na	k7c4	na
Polácích	Polák	k1gMnPc6	Polák
Moravu	Morava	k1gFnSc4	Morava
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
správu	správa	k1gFnSc4	správa
svěřil	svěřit	k5eAaPmAgInS	svěřit
svému	svůj	k3xOyFgMnSc3	svůj
synovi	syn	k1gMnSc3	syn
Břetislavovi	Břetislav	k1gMnSc3	Břetislav
<g/>
.	.	kIx.	.
</s>
<s>
Stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
i	i	k9	i
díky	díky	k7c3	díky
spojenectví	spojenectví	k1gNnSc3	spojenectví
s	s	k7c7	s
rakouskými	rakouský	k2eAgInPc7d1	rakouský
Babenberky	Babenberk	k1gInPc7	Babenberk
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
bylo	být	k5eAaImAgNnS	být
nakonec	nakonec	k6eAd1	nakonec
stvrzeno	stvrdit	k5eAaPmNgNnS	stvrdit
sňatkem	sňatek	k1gInSc7	sňatek
Břetislava	Břetislav	k1gMnSc2	Břetislav
s	s	k7c7	s
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
příslušnic	příslušnice	k1gFnPc2	příslušnice
rodu	rod	k1gInSc2	rod
<g/>
,	,	kIx,	,
Jitkou	Jitka	k1gFnSc7	Jitka
ze	z	k7c2	z
Svinibrodu	Svinibrod	k1gInSc2	Svinibrod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
zahraniční	zahraniční	k2eAgFnSc6d1	zahraniční
politice	politika	k1gFnSc6	politika
byl	být	k5eAaImAgMnS	být
Oldřich	Oldřich	k1gMnSc1	Oldřich
spojencem	spojenec	k1gMnSc7	spojenec
Říše	říš	k1gFnSc2	říš
–	–	k?	–
Jindřicha	Jindřich	k1gMnSc2	Jindřich
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
Konráda	Konrád	k1gMnSc4	Konrád
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
od	od	k7c2	od
něj	on	k3xPp3gInSc2	on
přijal	přijmout	k5eAaPmAgInS	přijmout
r.	r.	kA	r.
1024	[number]	k4	1024
Čechy	Čechy	k1gFnPc4	Čechy
v	v	k7c4	v
léno	léno	k1gNnSc4	léno
<g/>
)	)	kIx)	)
a	a	k8xC	a
podporoval	podporovat	k5eAaImAgMnS	podporovat
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
bojích	boj	k1gInPc6	boj
s	s	k7c7	s
Poláky	Polák	k1gMnPc7	Polák
a	a	k8xC	a
Uhry	Uher	k1gMnPc7	Uher
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
si	se	k3xPyFc3	se
Oldřich	Oldřich	k1gMnSc1	Oldřich
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
směrech	směr	k1gInPc6	směr
počínal	počínat	k5eAaImAgMnS	počínat
nezávisleji	závisle	k6eNd2	závisle
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
líbilo	líbit	k5eAaImAgNnS	líbit
římskoněmeckým	římskoněmecký	k2eAgMnPc3d1	římskoněmecký
panovníkům	panovník	k1gMnPc3	panovník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1014	[number]	k4	1014
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc2	on
snažil	snažit	k5eAaImAgMnS	snažit
polský	polský	k2eAgMnSc1d1	polský
kníže	kníže	k1gMnSc1	kníže
Boleslav	Boleslav	k1gMnSc1	Boleslav
Chrabrý	chrabrý	k2eAgMnSc1d1	chrabrý
získat	získat	k5eAaPmF	získat
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
stranu	strana	k1gFnSc4	strana
<g/>
,	,	kIx,	,
Oldřich	Oldřich	k1gMnSc1	Oldřich
ale	ale	k8xC	ale
nechal	nechat	k5eAaPmAgMnS	nechat
vyslance	vyslanec	k1gMnSc4	vyslanec
na	na	k7c6	na
zpáteční	zpáteční	k2eAgFnSc6d1	zpáteční
cestě	cesta	k1gFnSc6	cesta
zajmout	zajmout	k5eAaPmF	zajmout
a	a	k8xC	a
některé	některý	k3yIgFnPc4	některý
zabít	zabít	k5eAaPmF	zabít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1032	[number]	k4	1032
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
Sázavský	sázavský	k2eAgInSc1d1	sázavský
klášter	klášter	k1gInSc1	klášter
<g/>
,	,	kIx,	,
opatem	opat	k1gMnSc7	opat
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
poustevník	poustevník	k1gMnSc1	poustevník
Prokop	Prokop	k1gMnSc1	Prokop
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
klášteře	klášter	k1gInSc6	klášter
se	se	k3xPyFc4	se
uplatňovala	uplatňovat	k5eAaImAgFnS	uplatňovat
slovanská	slovanský	k2eAgFnSc1d1	Slovanská
liturgie	liturgie	k1gFnSc1	liturgie
vycházející	vycházející	k2eAgFnSc1d1	vycházející
z	z	k7c2	z
kultury	kultura	k1gFnSc2	kultura
Velké	velký	k2eAgFnSc2d1	velká
Moravy	Morava	k1gFnSc2	Morava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajetí	zajetí	k1gNnSc6	zajetí
a	a	k8xC	a
druhá	druhý	k4xOgFnSc1	druhý
vláda	vláda	k1gFnSc1	vláda
==	==	k?	==
</s>
</p>
<p>
<s>
Počátkem	počátkem	k7c2	počátkem
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
však	však	k9	však
vztahy	vztah	k1gInPc1	vztah
s	s	k7c7	s
Říší	říš	k1gFnSc7	říš
rapidně	rapidně	k6eAd1	rapidně
zhoršily	zhoršit	k5eAaPmAgInP	zhoršit
<g/>
,	,	kIx,	,
když	když	k8xS	když
sesazený	sesazený	k2eAgMnSc1d1	sesazený
polský	polský	k2eAgMnSc1d1	polský
kníže	kníže	k1gMnSc1	kníže
Měšek	Měšek	k1gMnSc1	Měšek
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Lambert	Lambert	k1gMnSc1	Lambert
nalezl	naleznout	k5eAaPmAgMnS	naleznout
útočiště	útočiště	k1gNnSc4	útočiště
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
a	a	k8xC	a
Oldřich	Oldřich	k1gMnSc1	Oldřich
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
pomáhat	pomáhat	k5eAaImF	pomáhat
císaři	císař	k1gMnSc3	císař
v	v	k7c6	v
tažení	tažení	k1gNnSc6	tažení
do	do	k7c2	do
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
Konrád	Konrád	k1gMnSc1	Konrád
II	II	kA	II
<g/>
.	.	kIx.	.
si	se	k3xPyFc3	se
roku	rok	k1gInSc2	rok
1033	[number]	k4	1033
Oldřicha	Oldřich	k1gMnSc2	Oldřich
předvolal	předvolat	k5eAaPmAgMnS	předvolat
<g/>
,	,	kIx,	,
obvinil	obvinit	k5eAaPmAgMnS	obvinit
jej	on	k3xPp3gInSc4	on
z	z	k7c2	z
úkladů	úklad	k1gInPc2	úklad
<g/>
,	,	kIx,	,
sesadil	sesadit	k5eAaPmAgInS	sesadit
a	a	k8xC	a
odsoudil	odsoudit	k5eAaPmAgInS	odsoudit
do	do	k7c2	do
vyhnanství	vyhnanství	k1gNnSc2	vyhnanství
v	v	k7c6	v
Bavorsku	Bavorsko	k1gNnSc6	Bavorsko
<g/>
.	.	kIx.	.
</s>
<s>
Českým	český	k2eAgMnSc7d1	český
knížetem	kníže	k1gMnSc7	kníže
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
opět	opět	k6eAd1	opět
Jaromír	Jaromír	k1gMnSc1	Jaromír
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1034	[number]	k4	1034
však	však	k9	však
císař	císař	k1gMnSc1	císař
Oldřicha	Oldřich	k1gMnSc2	Oldřich
propustil	propustit	k5eAaPmAgInS	propustit
a	a	k8xC	a
vrátil	vrátit	k5eAaPmAgInS	vrátit
mu	on	k3xPp3gMnSc3	on
vládu	vláda	k1gFnSc4	vláda
v	v	k7c6	v
knížectví	knížectví	k1gNnSc6	knížectví
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Jaromír	Jaromír	k1gMnSc1	Jaromír
získá	získat	k5eAaPmIp3nS	získat
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
úděl	úděl	k1gInSc1	úděl
a	a	k8xC	a
Oldřichův	Oldřichův	k2eAgMnSc1d1	Oldřichův
syn	syn	k1gMnSc1	syn
Břetislav	Břetislav	k1gMnSc1	Břetislav
Moravu	Morava	k1gFnSc4	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Jaromíra	Jaromír	k1gMnSc4	Jaromír
dal	dát	k5eAaPmAgMnS	dát
ale	ale	k9	ale
jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
zajmout	zajmout	k5eAaPmF	zajmout
a	a	k8xC	a
oslepit	oslepit	k5eAaPmF	oslepit
a	a	k8xC	a
Břetislav	Břetislav	k1gMnSc1	Břetislav
utekl	utéct	k5eAaPmAgMnS	utéct
do	do	k7c2	do
ciziny	cizina	k1gFnSc2	cizina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Již	již	k6eAd1	již
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
však	však	k9	však
Oldřich	Oldřich	k1gMnSc1	Oldřich
při	při	k7c6	při
bohaté	bohatý	k2eAgFnSc6d1	bohatá
tabuli	tabule	k1gFnSc6	tabule
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
jejímž	jejíž	k3xOyRp3gInPc3	jejíž
vždy	vždy	k6eAd1	vždy
byl	být	k5eAaImAgMnS	být
ctitelem	ctitel	k1gMnSc7	ctitel
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
jídlem	jídlo	k1gNnSc7	jídlo
a	a	k8xC	a
pitím	pití	k1gNnSc7	pití
náhle	náhle	k6eAd1	náhle
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Bratr	bratr	k1gMnSc1	bratr
Jaromír	Jaromír	k1gMnSc1	Jaromír
ho	on	k3xPp3gMnSc4	on
dal	dát	k5eAaPmAgMnS	dát
slavnostně	slavnostně	k6eAd1	slavnostně
pohřbít	pohřbít	k5eAaPmF	pohřbít
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
Sv.	sv.	kA	sv.
Jiří	Jiří	k1gMnSc1	Jiří
na	na	k7c6	na
Pražském	pražský	k2eAgInSc6d1	pražský
hradě	hrad	k1gInSc6	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
žijící	žijící	k2eAgMnSc1d1	žijící
vykastrovaný	vykastrovaný	k2eAgMnSc1d1	vykastrovaný
a	a	k8xC	a
oslepený	oslepený	k2eAgMnSc1d1	oslepený
Jaromír	Jaromír	k1gMnSc1	Jaromír
se	se	k3xPyFc4	se
vzdal	vzdát	k5eAaPmAgMnS	vzdát
nároků	nárok	k1gInPc2	nárok
na	na	k7c4	na
vládu	vláda	k1gFnSc4	vláda
a	a	k8xC	a
moc	moc	k1gFnSc4	moc
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
získal	získat	k5eAaPmAgMnS	získat
Oldřichův	Oldřichův	k2eAgMnSc1d1	Oldřichův
syn	syn	k1gMnSc1	syn
Břetislav	Břetislav	k1gMnSc1	Břetislav
I.	I.	kA	I.
</s>
</p>
<p>
<s>
==	==	k?	==
Genealogie	genealogie	k1gFnSc2	genealogie
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
BLÁHOVÁ	Bláhová	k1gFnSc1	Bláhová
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
<g/>
;	;	kIx,	;
FROLÍK	FROLÍK	kA	FROLÍK
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
;	;	kIx,	;
PROFANTOVÁ	PROFANTOVÁ	kA	PROFANTOVÁ
<g/>
,	,	kIx,	,
Naďa	Naďa	k1gFnSc1	Naďa
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnPc1d1	velká
dějiny	dějiny	k1gFnPc1	dějiny
zemí	zem	k1gFnPc2	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
I.	I.	kA	I.
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1197	[number]	k4	1197
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
;	;	kIx,	;
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
800	[number]	k4	800
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
265	[number]	k4	265
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
CHARVÁT	Charvát	k1gMnSc1	Charvát
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Zrod	zrod	k1gInSc1	zrod
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
568	[number]	k4	568
<g/>
-	-	kIx~	-
<g/>
1055	[number]	k4	1055
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
263	[number]	k4	263
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7021	[number]	k4	7021
<g/>
-	-	kIx~	-
<g/>
845	[number]	k4	845
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
LUTOVSKÝ	LUTOVSKÝ	kA	LUTOVSKÝ
<g/>
,	,	kIx,	,
Michal	Michal	k1gMnSc1	Michal
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
stopách	stopa	k1gFnPc6	stopa
prvních	první	k4xOgInPc2	první
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Léta	léto	k1gNnPc1	léto
krize	krize	k1gFnSc2	krize
a	a	k8xC	a
obnovy	obnova	k1gFnSc2	obnova
972	[number]	k4	972
<g/>
-	-	kIx~	-
<g/>
1012	[number]	k4	1012
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Boleslava	Boleslav	k1gMnSc2	Boleslav
II	II	kA	II
<g/>
.	.	kIx.	.
po	po	k7c4	po
Jaromíra	Jaromír	k1gMnSc4	Jaromír
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
271	[number]	k4	271
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
231	[number]	k4	231
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
LUTOVSKÝ	LUTOVSKÝ	kA	LUTOVSKÝ
<g/>
,	,	kIx,	,
Michal	Michal	k1gMnSc1	Michal
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
stopách	stopa	k1gFnPc6	stopa
prvních	první	k4xOgInPc2	první
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Správa	správa	k1gFnSc1	správa
a	a	k8xC	a
obrana	obrana	k1gFnSc1	obrana
země	zem	k1gFnSc2	zem
(	(	kIx(	(
<g/>
1012	[number]	k4	1012
<g/>
-	-	kIx~	-
<g/>
1055	[number]	k4	1055
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Oldřicha	Oldřich	k1gMnSc2	Oldřich
po	po	k7c4	po
Břetislava	Břetislav	k1gMnSc4	Břetislav
I.	I.	kA	I.
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
282	[number]	k4	282
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
365	[number]	k4	365
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MATLA-KOZŁOWSKA	MATLA-KOZŁOWSKA	k?	MATLA-KOZŁOWSKA
<g/>
,	,	kIx,	,
Marzena	Marzen	k2eAgFnSc1d1	Marzena
<g/>
.	.	kIx.	.
</s>
<s>
Pierwsi	Pierwse	k1gFnSc4	Pierwse
Przemyślidzi	Przemyślidze	k1gFnSc4	Przemyślidze
i	i	k9	i
ich	ich	k?	ich
państwo	państwo	k6eAd1	państwo
(	(	kIx(	(
<g/>
od	od	k7c2	od
X	X	kA	X
do	do	k7c2	do
połowy	połowa	k1gFnSc2	połowa
XI	XI	kA	XI
wieku	wieku	k6eAd1	wieku
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ekspansja	Ekspansj	k2eAgFnSc1d1	Ekspansj
terytorialna	terytorialna	k1gFnSc1	terytorialna
i	i	k8xC	i
jej	on	k3xPp3gMnSc4	on
polityczne	politycznout	k5eAaPmIp3nS	politycznout
uwarunkowania	uwarunkowanium	k1gNnPc1	uwarunkowanium
<g/>
.	.	kIx.	.
</s>
<s>
Poznań	Poznań	k?	Poznań
<g/>
:	:	kIx,	:
Wydawnictwo	Wydawnictwo	k1gMnSc1	Wydawnictwo
Poznańskie	Poznańskie	k1gFnSc2	Poznańskie
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
576	[number]	k4	576
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
83	[number]	k4	83
<g/>
-	-	kIx~	-
<g/>
7177	[number]	k4	7177
<g/>
-	-	kIx~	-
<g/>
547	[number]	k4	547
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
NOVOTNÝ	Novotný	k1gMnSc1	Novotný
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgFnPc1d1	Česká
dějiny	dějiny	k1gFnPc1	dějiny
I.	I.	kA	I.
<g/>
/	/	kIx~	/
<g/>
I.	I.	kA	I.
Od	od	k7c2	od
nejstarších	starý	k2eAgFnPc2d3	nejstarší
dob	doba	k1gFnPc2	doba
do	do	k7c2	do
smrti	smrt	k1gFnSc2	smrt
knížete	kníže	k1gMnSc2	kníže
Oldřicha	Oldřich	k1gMnSc2	Oldřich
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Laichter	Laichter	k1gMnSc1	Laichter
<g/>
,	,	kIx,	,
1912	[number]	k4	1912
<g/>
.	.	kIx.	.
782	[number]	k4	782
s.	s.	k?	s.
</s>
</p>
<p>
<s>
SOMMER	Sommer	k1gMnSc1	Sommer
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
;	;	kIx,	;
TŘEŠTÍK	TŘEŠTÍK	kA	TŘEŠTÍK
<g/>
,	,	kIx,	,
Dušan	Dušan	k1gMnSc1	Dušan
<g/>
;	;	kIx,	;
ŽEMLIČKA	Žemlička	k1gMnSc1	Žemlička
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Přemyslovci	Přemyslovec	k1gMnPc1	Přemyslovec
<g/>
.	.	kIx.	.
</s>
<s>
Budování	budování	k1gNnSc4	budování
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
779	[number]	k4	779
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
352	[number]	k4	352
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
TŘEŠTÍK	TŘEŠTÍK	kA	TŘEŠTÍK
<g/>
,	,	kIx,	,
Dušan	Dušan	k1gMnSc1	Dušan
<g/>
;	;	kIx,	;
ČECHURA	Čechura	k1gMnSc1	Čechura
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
;	;	kIx,	;
PECHAR	PECHAR	kA	PECHAR
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
<g/>
.	.	kIx.	.
</s>
<s>
Králové	Král	k1gMnPc1	Král
a	a	k8xC	a
knížata	kníže	k1gMnPc1wR	kníže
zemí	zem	k1gFnPc2	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Rybka	Rybka	k1gMnSc1	Rybka
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
413	[number]	k4	413
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86182	[number]	k4	86182
<g/>
-	-	kIx~	-
<g/>
55	[number]	k4	55
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
</s>
</p>
<p>
<s>
WIHODA	WIHODA	kA	WIHODA
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
<g/>
.	.	kIx.	.
</s>
<s>
Morava	Morava	k1gFnSc1	Morava
v	v	k7c6	v
době	doba	k1gFnSc6	doba
knížecí	knížecí	k2eAgFnSc1d1	knížecí
906	[number]	k4	906
<g/>
–	–	k?	–
<g/>
1197	[number]	k4	1197
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
464	[number]	k4	464
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
563	[number]	k4	563
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ŽEMLIČKA	Žemlička	k1gMnSc1	Žemlička
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Přemyslovci	Přemyslovec	k1gMnPc1	Přemyslovec
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
žili	žít	k5eAaImAgMnP	žít
<g/>
,	,	kIx,	,
vládli	vládnout	k5eAaImAgMnP	vládnout
<g/>
,	,	kIx,	,
umírali	umírat	k5eAaImAgMnP	umírat
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
497	[number]	k4	497
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
759	[number]	k4	759
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Oldřich	Oldřich	k1gMnSc1	Oldřich
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oldřich	Oldřich	k1gMnSc1	Oldřich
ve	v	k7c6	v
Vlastenském	vlastenský	k2eAgInSc6d1	vlastenský
slovníku	slovník	k1gInSc6	slovník
historickém	historický	k2eAgInSc6d1	historický
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Heslo	heslo	k1gNnSc1	heslo
v	v	k7c6	v
encyklopedii	encyklopedie	k1gFnSc6	encyklopedie
KDO	kdo	k3yInSc1	kdo
BYL	být	k5eAaImAgMnS	být
KDO	kdo	k3yQnSc1	kdo
v	v	k7c6	v
našich	náš	k3xOp1gFnPc6	náš
dějinách	dějiny	k1gFnPc6	dějiny
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
</s>
</p>
<p>
<s>
Oldřich	Oldřich	k1gMnSc1	Oldřich
I.	I.	kA	I.
na	na	k7c6	na
e-stredovek	etredovka	k1gFnPc2	e-stredovka
</s>
</p>
