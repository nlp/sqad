<s>
Synagoga	synagoga	k1gFnSc1
v	v	k7c6
Českých	český	k2eAgInPc6d1
Budějovicích	Budějovice	k1gInPc6
</s>
<s>
Synagoga	synagoga	k1gFnSc1
v	v	k7c6
Českých	český	k2eAgInPc6d1
Budějovicích	Budějovice	k1gInPc6
Místo	místo	k6eAd1
Místo	místo	k7c2
</s>
<s>
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
<g/>
,	,	kIx,
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
48	#num#	k4
<g/>
°	°	k?
<g/>
58	#num#	k4
<g/>
′	′	k?
<g/>
14,56	14,56	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
14	#num#	k4
<g/>
°	°	k?
<g/>
28	#num#	k4
<g/>
′	′	k?
<g/>
21,38	21,38	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Základní	základní	k2eAgFnSc1d1
informace	informace	k1gFnSc1
Kraj	kraj	k1gInSc1
</s>
<s>
Jihočeský	jihočeský	k2eAgInSc1d1
Okres	okres	k1gInSc1
</s>
<s>
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
Architektonický	architektonický	k2eAgInSc1d1
popis	popis	k1gInSc4
Architekt	architekt	k1gMnSc1
</s>
<s>
Max	Max	k1gMnSc1
Fleischer	Fleischra	k1gFnPc2
Sloh	sloh	k1gInSc4
</s>
<s>
novogotický	novogotický	k2eAgInSc1d1
Výstavba	výstavba	k1gFnSc1
</s>
<s>
1887	#num#	k4
<g/>
–	–	k?
<g/>
1888	#num#	k4
Odkazy	odkaz	k1gInPc7
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Zaniklá	zaniklý	k2eAgFnSc1d1
synagoga	synagoga	k1gFnSc1
v	v	k7c6
Českých	český	k2eAgInPc6d1
Budějovicích	Budějovice	k1gInPc6
stávala	stávat	k5eAaImAgFnS
do	do	k7c2
roku	rok	k1gInSc2
1942	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
ji	on	k3xPp3gFnSc4
zničili	zničit	k5eAaPmAgMnP
nacisté	nacista	k1gMnPc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
v	v	k7c6
ulici	ulice	k1gFnSc6
F.	F.	kA
A.	A.	kA
Gerstnera	Gerstner	k1gMnSc2
<g/>
,	,	kIx,
vedle	vedle	k7c2
dnešního	dnešní	k2eAgInSc2d1
justičního	justiční	k2eAgInSc2d1
paláce	palác	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
místě	místo	k1gNnSc6
novogotické	novogotický	k2eAgFnSc2d1
synagogy	synagoga	k1gFnSc2
postavené	postavený	k2eAgFnSc2d1
v	v	k7c6
letech	let	k1gInPc6
1887	#num#	k4
<g/>
–	–	k?
<g/>
1888	#num#	k4
podle	podle	k7c2
plánů	plán	k1gInPc2
vídeňského	vídeňský	k2eAgMnSc2d1
architekta	architekt	k1gMnSc2
Maxe	Max	k1gMnSc2
Fleischera	Fleischer	k1gMnSc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
byl	být	k5eAaImAgInS
v	v	k7c6
roce	rok	k1gInSc6
1992	#num#	k4
odhalen	odhalit	k5eAaPmNgInS
pomník	pomník	k1gInSc1
obětem	oběť	k1gFnPc3
holokaustu	holokaust	k1gInSc2
–	–	k?
ruka	ruka	k1gFnSc1
ve	v	k7c6
tvaru	tvar	k1gInSc6
menory	menora	k1gFnSc2
s	s	k7c7
Davidovou	Davidův	k2eAgFnSc7d1
hvězdou	hvězda	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Pomník	pomník	k1gInSc1
stojí	stát	k5eAaImIp3nS
na	na	k7c6
travnaté	travnatý	k2eAgFnSc6d1
ploše	plocha	k1gFnSc6
mezi	mezi	k7c7
dvěma	dva	k4xCgNnPc7
parkovišti	parkoviště	k1gNnPc7
<g/>
,	,	kIx,
naproti	naproti	k7c3
zimnímu	zimní	k2eAgInSc3d1
stadionu	stadion	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Model	model	k1gInSc1
synagogy	synagoga	k1gFnSc2
je	být	k5eAaImIp3nS
k	k	k7c3
vidění	vidění	k1gNnSc3
ve	v	k7c6
vestibulu	vestibul	k1gInSc6
přes	přes	k7c4
ulici	ulice	k1gFnSc4
sídlící	sídlící	k2eAgFnSc2d1
energetické	energetický	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Varhany	varhany	k1gFnPc1
</s>
<s>
Při	při	k7c6
západní	západní	k2eAgFnSc6d1
stěně	stěna	k1gFnSc6
byl	být	k5eAaImAgInS
umístěn	umístěn	k2eAgInSc1d1
hudební	hudební	k2eAgInSc1d1
kůr	kůr	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
v	v	k7c6
roce	rok	k1gInSc6
1898	#num#	k4
doplnily	doplnit	k5eAaPmAgInP
dvoumanuálové	dvoumanuálový	k2eAgInPc1d1
varhany	varhany	k1gInPc1
s	s	k7c7
13	#num#	k4
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
1	#num#	k4
<g/>
]	]	kIx)
znějícími	znějící	k2eAgInPc7d1
rejstříky	rejstřík	k1gInPc7
od	od	k7c2
varhanáře	varhanář	k1gMnSc4
Karla	Karel	k1gMnSc2
Eisenhuta	Eisenhut	k1gMnSc2
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
I.	I.	kA
manuál	manuál	k1gInSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
Bourdon	bourdon	k1gInSc1
<g/>
16	#num#	k4
<g/>
′	′	k?
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
<g/>
Principal	Principal	k1gFnSc1
<g/>
8	#num#	k4
<g/>
′	′	k?
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
<g/>
Flöte	Flöt	k1gInSc5
<g/>
8	#num#	k4
<g/>
′	′	k?
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
<g/>
Gamba	gamba	k1gFnSc1
<g/>
8	#num#	k4
<g/>
′	′	k?
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
<g/>
Oktava	Oktava	k1gFnSc1
<g/>
4	#num#	k4
<g/>
′	′	k?
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
<g/>
Flöte	Flöt	k1gInSc5
<g/>
4	#num#	k4
<g/>
′	′	k?
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
<g/>
Mixtur	mixtura	k1gFnPc2
<g/>
22	#num#	k4
<g/>
/	/	kIx~
<g/>
3	#num#	k4
<g/>
′	′	k?
</s>
<s>
II	II	kA
<g/>
.	.	kIx.
manuál	manuál	k1gInSc1
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
<g/>
Salicional	Salicional	k1gFnSc1
<g/>
8	#num#	k4
<g/>
′	′	k?
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
<g/>
Gamba	gamba	k1gFnSc1
<g/>
8	#num#	k4
<g/>
′	′	k?
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
<g/>
Dolce	dolec	k1gInSc2
<g/>
4	#num#	k4
<g/>
′	′	k?
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
<g/>
Fugara	Fugara	k1gFnSc1
<g/>
4	#num#	k4
<g/>
′	′	k?
</s>
<s>
Pedál	pedál	k1gInSc1
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
<g/>
Subbass	Subbass	k1gInSc1
<g/>
16	#num#	k4
<g/>
′	′	k?
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
<g/>
Oktavbass	Oktavbass	k1gInSc1
<g/>
8	#num#	k4
<g/>
′	′	k?
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
<g/>
Oktav	Oktav	k1gInSc1
<g/>
4	#num#	k4
<g/>
′	′	k?
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
↑	↑	k?
Přestože	přestože	k8xS
zdroj	zdroj	k1gInSc1
zmiňuje	zmiňovat	k5eAaImIp3nS
explicitně	explicitně	k6eAd1
13	#num#	k4
rejstříků	rejstřík	k1gInPc2
<g/>
,	,	kIx,
ve	v	k7c6
výčtu	výčet	k1gInSc6
jich	on	k3xPp3gMnPc2
uvádí	uvádět	k5eAaImIp3nS
14	#num#	k4
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.holocaust.cz	www.holocaust.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Rozkošná	rozkošný	k2eAgFnSc1d1
<g/>
,	,	kIx,
Blanka	Blanka	k1gFnSc1
<g/>
,	,	kIx,
Jakubec	Jakubec	k1gMnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Židovské	židovský	k2eAgFnPc4d1
památky	památka	k1gFnPc4
Čech	Čechy	k1gFnPc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Era	Era	k1gMnSc1
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
480	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86517	#num#	k4
<g/>
-	-	kIx~
<g/>
64	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
↑	↑	k?
www.zanikleobce.cz	www.zanikleobce.cz	k1gMnSc1
<g/>
↑	↑	k?
SVOBODA	Svoboda	k1gMnSc1
<g/>
,	,	kIx,
Štěpán	Štěpán	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Varhany	varhany	k1gFnPc1
a	a	k8xC
varhanáři	varhanář	k1gMnPc1
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
varhany	varhany	k1gInPc1
<g/>
.	.	kIx.
<g/>
net	net	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
židovských	židovský	k2eAgFnPc2d1
památek	památka	k1gFnPc2
v	v	k7c6
Jihočeském	jihočeský	k2eAgInSc6d1
kraji	kraj	k1gInSc6
</s>
<s>
Seznam	seznam	k1gInSc1
synagog	synagoga	k1gFnPc2
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Synagogy	synagoga	k1gFnPc1
v	v	k7c6
Jihočeském	jihočeský	k2eAgInSc6d1
kraji	kraj	k1gInSc6
</s>
<s>
Babčice	Babčice	k1gFnSc1
</s>
<s>
Bechyně	Bechyně	k1gFnSc1
</s>
<s>
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
</s>
<s>
Český	český	k2eAgInSc1d1
Krumlov	Krumlov	k1gInSc1
</s>
<s>
Čkyně	Čkyně	k6eAd1
</s>
<s>
Dub	Dub	k1gMnSc1
</s>
<s>
Hluboká	Hluboká	k1gFnSc1
nad	nad	k7c7
Vltavou	Vltava	k1gFnSc7
</s>
<s>
Jindřichův	Jindřichův	k2eAgInSc1d1
Hradec	Hradec	k1gInSc1
</s>
<s>
Milevsko	Milevsko	k1gNnSc1
</s>
<s>
Nová	nový	k2eAgFnSc1d1
synagoga	synagoga	k1gFnSc1
</s>
<s>
Stará	starý	k2eAgFnSc1d1
synagoga	synagoga	k1gFnSc1
</s>
<s>
Písek	Písek	k1gInSc1
</s>
<s>
Radenín	Radenín	k1gMnSc1
</s>
<s>
Slavonice	Slavonice	k1gFnSc1
</s>
<s>
Soběslav	Soběslav	k1gMnSc1
</s>
<s>
Stádlec	Stádlec	k1gMnSc1
</s>
<s>
Stráž	stráž	k1gFnSc1
nad	nad	k7c7
Nežárkou	Nežárka	k1gFnSc7
</s>
<s>
Třeboň	Třeboň	k1gFnSc1
</s>
<s>
Tučapy	Tučapa	k1gFnPc1
</s>
<s>
Vlachovo	Vlachův	k2eAgNnSc1d1
Březí	březí	k1gNnSc1
</s>
<s>
Vodňany	Vodňan	k1gMnPc4
</s>
<s>
Volyně	Volyně	k1gFnSc1
viz	vidět	k5eAaImRp2nS
též	též	k9
Seznam	seznam	k1gInSc4
židovských	židovský	k2eAgFnPc2d1
památek	památka	k1gFnPc2
v	v	k7c6
Jihočeském	jihočeský	k2eAgInSc6d1
kraji	kraj	k1gInSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Hebraistika	hebraistika	k1gFnSc1
|	|	kIx~
Česko	Česko	k1gNnSc1
</s>
