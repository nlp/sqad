<s>
Zaniklá	zaniklý	k2eAgFnSc1d1
synagoga	synagoga	k1gFnSc1
v	v	k7c6
Českých	český	k2eAgInPc6d1
Budějovicích	Budějovice	k1gInPc6
stávala	stávat	k5eAaImAgFnS
do	do	k7c2
roku	rok	k1gInSc2
1942	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
ji	on	k3xPp3gFnSc4
zničili	zničit	k5eAaPmAgMnP
nacisté	nacista	k1gMnPc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
v	v	k7c6
ulici	ulice	k1gFnSc6
F.	F.	kA
A.	A.	kA
Gerstnera	Gerstner	k1gMnSc2
<g/>
,	,	kIx,
vedle	vedle	k7c2
dnešního	dnešní	k2eAgInSc2d1
justičního	justiční	k2eAgInSc2d1
paláce	palác	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>