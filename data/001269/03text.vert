<s>
Setkání	setkání	k1gNnSc1	setkání
s	s	k7c7	s
medúzou	medúza	k1gFnSc7	medúza
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
A	a	k9	a
Meeting	meeting	k1gInSc1	meeting
with	with	k1gInSc1	with
Medusa	Medusa	k1gFnSc1	Medusa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
sci-fi	scii	k1gNnSc1	sci-fi
novela	novela	k1gFnSc1	novela
spisovatele	spisovatel	k1gMnSc2	spisovatel
Arthura	Arthur	k1gMnSc2	Arthur
C.	C.	kA	C.
Clarka	Clarek	k1gMnSc2	Clarek
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
poprvé	poprvé	k6eAd1	poprvé
publikována	publikovat	k5eAaBmNgFnS	publikovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
a	a	k8xC	a
v	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
cenu	cena	k1gFnSc4	cena
Nebula	nebula	k1gFnSc1	nebula
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
"	"	kIx"	"
<g/>
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
novela	novela	k1gFnSc1	novela
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
vyšla	vyjít	k5eAaPmAgFnS	vyjít
mj.	mj.	kA	mj.
ve	v	k7c6	v
sbírce	sbírka	k1gFnSc6	sbírka
The	The	k1gMnSc1	The
Wind	Wind	k1gMnSc1	Wind
from	from	k1gMnSc1	from
the	the	k?	the
Sun	Sun	kA	Sun
<g/>
.	.	kIx.	.
</s>
<s>
Novela	novela	k1gFnSc1	novela
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
příběh	příběh	k1gInSc4	příběh
průkopníka	průkopník	k1gMnSc2	průkopník
Howarda	Howard	k1gMnSc2	Howard
Falcona	Falcon	k1gMnSc2	Falcon
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
přežije	přežít	k5eAaPmIp3nS	přežít
havárii	havárie	k1gFnSc4	havárie
vzducholodě	vzducholoď	k1gFnSc2	vzducholoď
a	a	k8xC	a
najde	najít	k5eAaPmIp3nS	najít
v	v	k7c6	v
sobě	sebe	k3xPyFc6	sebe
sílu	síla	k1gFnSc4	síla
čelit	čelit	k5eAaImF	čelit
další	další	k2eAgFnSc3d1	další
výzvě	výzva	k1gFnSc3	výzva
<g/>
:	:	kIx,	:
planetě	planeta	k1gFnSc3	planeta
Jupiter	Jupiter	k1gInSc4	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
Novela	novela	k1gFnSc1	novela
Setkání	setkání	k1gNnSc2	setkání
s	s	k7c7	s
medúzou	medúza	k1gFnSc7	medúza
je	být	k5eAaImIp3nS	být
rozčleněna	rozčlenit	k5eAaPmNgFnS	rozčlenit
do	do	k7c2	do
8	[number]	k4	8
kapitol	kapitola	k1gFnPc2	kapitola
<g/>
:	:	kIx,	:
Památný	památný	k2eAgInSc4d1	památný
den	den	k1gInSc4	den
Protože	protože	k8xS	protože
tady	tady	k6eAd1	tady
je	být	k5eAaImIp3nS	být
Svět	svět	k1gInSc1	svět
bohů	bůh	k1gMnPc2	bůh
Hlasy	hlas	k1gInPc1	hlas
z	z	k7c2	z
hlubiny	hlubina	k1gFnSc2	hlubina
Kola	kolo	k1gNnSc2	kolo
Poseidonova	Poseidonův	k2eAgFnSc1d1	Poseidonova
Medúza	Medúza	k1gFnSc1	Medúza
Základní	základní	k2eAgFnSc1d1	základní
ustanovení	ustanovení	k1gNnPc2	ustanovení
Mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgInPc7	dva
světy	svět	k1gInPc7	svět
Howard	Howard	k1gMnSc1	Howard
Falcon	Falcon	k1gMnSc1	Falcon
-	-	kIx~	-
hlavní	hlavní	k2eAgFnSc1d1	hlavní
postava	postava	k1gFnSc1	postava
<g/>
,	,	kIx,	,
kapitán	kapitán	k1gMnSc1	kapitán
přeživší	přeživší	k2eAgFnSc3d1	přeživší
havárii	havárie	k1gFnSc3	havárie
největší	veliký	k2eAgFnSc2d3	veliký
vzducholodě	vzducholoď	k1gFnSc2	vzducholoď
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
<g/>
,	,	kIx,	,
přijme	přijmout	k5eAaPmIp3nS	přijmout
výzvu	výzva	k1gFnSc4	výzva
sestoupit	sestoupit	k5eAaPmF	sestoupit
do	do	k7c2	do
atmosféry	atmosféra	k1gFnSc2	atmosféra
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
Webster	Webster	k1gInSc4	Webster
-	-	kIx~	-
vedoucí	vedoucí	k2eAgNnSc4d1	vedoucí
oddělení	oddělení	k1gNnSc4	oddělení
Plánování	plánování	k1gNnSc2	plánování
dálkových	dálkový	k2eAgInPc2d1	dálkový
letů	let	k1gInPc2	let
<g/>
.	.	kIx.	.
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Brenner	Brenner	k1gMnSc1	Brenner
-	-	kIx~	-
vedoucí	vedoucí	k1gFnSc1	vedoucí
Exobiologického	Exobiologický	k2eAgNnSc2d1	Exobiologický
oddělení	oddělení	k1gNnSc2	oddělení
<g/>
.	.	kIx.	.
</s>
<s>
Kapitán	kapitán	k1gMnSc1	kapitán
Howard	Howard	k1gMnSc1	Howard
Falcon	Falcon	k1gMnSc1	Falcon
velí	velet	k5eAaImIp3nS	velet
zkušebnímu	zkušební	k2eAgInSc3d1	zkušební
letu	let	k1gInSc3	let
největší	veliký	k2eAgFnSc2d3	veliký
vzducholodě	vzducholoď	k1gFnSc2	vzducholoď
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
-	-	kIx~	-
téměř	téměř	k6eAd1	téměř
500	[number]	k4	500
metrů	metr	k1gInPc2	metr
dlouhého	dlouhý	k2eAgNnSc2d1	dlouhé
plavidla	plavidlo	k1gNnSc2	plavidlo
pojmenovaného	pojmenovaný	k2eAgInSc2d1	pojmenovaný
"	"	kIx"	"
<g/>
Královna	královna	k1gFnSc1	královna
Alžběta	Alžběta	k1gFnSc1	Alžběta
IV	IV	kA	IV
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
bezproblémový	bezproblémový	k2eAgInSc4d1	bezproblémový
let	let	k1gInSc4	let
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
během	během	k7c2	během
přibližovacího	přibližovací	k2eAgInSc2d1	přibližovací
manévru	manévr	k1gInSc2	manévr
televizního	televizní	k2eAgNnSc2d1	televizní
vznášedla	vznášedlo	k1gNnSc2	vznášedlo
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
nehodě	nehoda	k1gFnSc3	nehoda
a	a	k8xC	a
vzducholoď	vzducholoď	k1gFnSc1	vzducholoď
utrpí	utrpět	k5eAaPmIp3nS	utrpět
vážné	vážný	k2eAgNnSc4d1	vážné
poškození	poškození	k1gNnSc4	poškození
<g/>
.	.	kIx.	.
</s>
<s>
Kapitán	kapitán	k1gMnSc1	kapitán
Falcon	Falcon	k1gMnSc1	Falcon
se	se	k3xPyFc4	se
ihned	ihned	k6eAd1	ihned
ujme	ujmout	k5eAaPmIp3nS	ujmout
řízení	řízení	k1gNnSc1	řízení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nepodaří	podařit	k5eNaPmIp3nS	podařit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
zabránit	zabránit	k5eAaPmF	zabránit
havárii	havárie	k1gFnSc4	havárie
<g/>
.	.	kIx.	.
</s>
<s>
Cestou	cesta	k1gFnSc7	cesta
na	na	k7c4	na
můstek	můstek	k1gInSc4	můstek
potká	potkat	k5eAaPmIp3nS	potkat
vyděšeného	vyděšený	k2eAgMnSc4d1	vyděšený
šimpanze	šimpanz	k1gMnSc4	šimpanz
<g/>
,	,	kIx,	,
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
těch	ten	k3xDgMnPc2	ten
drezúrovaných	drezúrovaný	k2eAgMnPc2d1	drezúrovaný
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
dokáží	dokázat	k5eAaPmIp3nP	dokázat
vykonávat	vykonávat	k5eAaImF	vykonávat
jednoduché	jednoduchý	k2eAgFnPc1d1	jednoduchá
pomocné	pomocný	k2eAgFnPc1d1	pomocná
práce	práce	k1gFnPc1	práce
<g/>
.	.	kIx.	.
</s>
<s>
Neví	vědět	k5eNaImIp3nS	vědět
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
zvíře	zvíře	k1gNnSc1	zvíře
zareaguje	zareagovat	k5eAaPmIp3nS	zareagovat
<g/>
.	.	kIx.	.
</s>
<s>
Podaří	podařit	k5eAaPmIp3nS	podařit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
jej	on	k3xPp3gMnSc4	on
uklidnit	uklidnit	k5eAaPmF	uklidnit
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
ztroskotání	ztroskotání	k1gNnSc6	ztroskotání
vzdušného	vzdušný	k2eAgNnSc2d1	vzdušné
plavidla	plavidlo	k1gNnSc2	plavidlo
zahyne	zahynout	k5eAaPmIp3nS	zahynout
několik	několik	k4yIc4	několik
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
Falcon	Falcona	k1gFnPc2	Falcona
utrpí	utrpět	k5eAaPmIp3nP	utrpět
velmi	velmi	k6eAd1	velmi
vážná	vážný	k2eAgNnPc4d1	vážné
zranění	zranění	k1gNnPc4	zranění
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
silné	silný	k2eAgFnSc3d1	silná
vůli	vůle	k1gFnSc3	vůle
a	a	k8xC	a
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
nejmodernější	moderní	k2eAgFnSc2d3	nejmodernější
lékařské	lékařský	k2eAgFnSc2d1	lékařská
technologie	technologie	k1gFnSc2	technologie
se	se	k3xPyFc4	se
zotaví	zotavit	k5eAaPmIp3nS	zotavit
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
to	ten	k3xDgNnSc1	ten
trvá	trvat	k5eAaImIp3nS	trvat
několik	několik	k4yIc1	několik
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
má	mít	k5eAaImIp3nS	mít
Howard	Howard	k1gMnSc1	Howard
Falcon	Falcon	k1gMnSc1	Falcon
nový	nový	k2eAgInSc4d1	nový
cíl	cíl	k1gInSc4	cíl
svých	svůj	k3xOyFgFnPc2	svůj
ambicí	ambice	k1gFnPc2	ambice
-	-	kIx~	-
plynného	plynný	k2eAgMnSc4d1	plynný
obra	obr	k1gMnSc4	obr
Jupiter	Jupiter	k1gMnSc1	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
Debatuje	debatovat	k5eAaImIp3nS	debatovat
o	o	k7c6	o
výpravě	výprava	k1gFnSc6	výprava
s	s	k7c7	s
Websterem	Webster	k1gMnSc7	Webster
<g/>
,	,	kIx,	,
vedoucím	vedoucí	k1gMnSc7	vedoucí
oddělení	oddělení	k1gNnSc2	oddělení
Plánování	plánování	k1gNnSc2	plánování
dálkových	dálkový	k2eAgInPc2d1	dálkový
letů	let	k1gInPc2	let
<g/>
.	.	kIx.	.
</s>
<s>
Jupiter	Jupiter	k1gMnSc1	Jupiter
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
obestřen	obestřít	k5eAaPmNgMnS	obestřít
záhadami	záhada	k1gFnPc7	záhada
a	a	k8xC	a
Howard	Howard	k1gMnSc1	Howard
je	být	k5eAaImIp3nS	být
přesvědčen	přesvědčit	k5eAaPmNgMnS	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
jediný	jediný	k2eAgMnSc1d1	jediný
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
zvládne	zvládnout	k5eAaPmIp3nS	zvládnout
náročný	náročný	k2eAgInSc1d1	náročný
sestup	sestup	k1gInSc1	sestup
do	do	k7c2	do
atmosféry	atmosféra	k1gFnSc2	atmosféra
obrovské	obrovský	k2eAgFnSc2d1	obrovská
planety	planeta	k1gFnSc2	planeta
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
gravitací	gravitace	k1gFnSc7	gravitace
<g/>
.	.	kIx.	.
</s>
<s>
Webster	Webster	k1gInSc1	Webster
navrhuje	navrhovat	k5eAaImIp3nS	navrhovat
méně	málo	k6eAd2	málo
hmotnou	hmotný	k2eAgFnSc4d1	hmotná
planetu	planeta	k1gFnSc4	planeta
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Uran	Uran	k1gInSc1	Uran
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Falcona	Falcona	k1gFnSc1	Falcona
nic	nic	k3yNnSc1	nic
menšího	malý	k2eAgMnSc4d2	menší
než	než	k8xS	než
král	král	k1gMnSc1	král
Sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
nezajímá	zajímat	k5eNaImIp3nS	zajímat
<g/>
.	.	kIx.	.
</s>
<s>
Dojde	dojít	k5eAaPmIp3nS	dojít
řeč	řeč	k1gFnSc4	řeč
i	i	k9	i
na	na	k7c6	na
havárii	havárie	k1gFnSc6	havárie
Královny	královna	k1gFnSc2	královna
Alžběty	Alžběta	k1gFnSc2	Alžběta
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
půlsekundové	půlsekundový	k2eAgNnSc1d1	půlsekundové
časové	časový	k2eAgNnSc1d1	časové
zpoždění	zpoždění	k1gNnSc1	zpoždění
při	při	k7c6	při
udílení	udílení	k1gNnSc6	udílení
povelů	povel	k1gInPc2	povel
<g/>
,	,	kIx,	,
okruh	okruh	k1gInSc1	okruh
dálkového	dálkový	k2eAgNnSc2d1	dálkové
řízení	řízení	k1gNnSc2	řízení
byl	být	k5eAaImAgMnS	být
zapojen	zapojit	k5eAaPmNgMnS	zapojit
přes	přes	k7c4	přes
družici	družice	k1gFnSc4	družice
<g/>
.	.	kIx.	.
</s>
<s>
Howard	Howard	k1gMnSc1	Howard
Falcon	Falcon	k1gMnSc1	Falcon
padá	padat	k5eAaImIp3nS	padat
se	s	k7c7	s
svým	svůj	k3xOyFgNnSc7	svůj
kosmickým	kosmický	k2eAgNnSc7d1	kosmické
plavidlem	plavidlo	k1gNnSc7	plavidlo
<g/>
,	,	kIx,	,
pojmenovaném	pojmenovaný	k2eAgInSc6d1	pojmenovaný
Kon-Tiki	Kon-Tik	k1gInSc6	Kon-Tik
<g/>
,	,	kIx,	,
do	do	k7c2	do
atmosféry	atmosféra	k1gFnSc2	atmosféra
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k1gNnSc1	místo
pro	pro	k7c4	pro
sestup	sestup	k1gInSc4	sestup
bylo	být	k5eAaImAgNnS	být
pečlivě	pečlivě	k6eAd1	pečlivě
vybráno	vybrat	k5eAaPmNgNnS	vybrat
<g/>
,	,	kIx,	,
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
uprostřed	uprostřed	k7c2	uprostřed
rovníkové	rovníkový	k2eAgFnSc2d1	Rovníková
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
zóna	zóna	k1gFnSc1	zóna
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
klidná	klidný	k2eAgFnSc1d1	klidná
<g/>
,	,	kIx,	,
neočekávají	očekávat	k5eNaImIp3nP	očekávat
se	se	k3xPyFc4	se
atmosférické	atmosférický	k2eAgFnSc2d1	atmosférická
víry	víra	k1gFnSc2	víra
či	či	k8xC	či
bouře	bouř	k1gFnSc2	bouř
s	s	k7c7	s
vichry	vichr	k1gInPc7	vichr
o	o	k7c6	o
rychlosti	rychlost	k1gFnSc6	rychlost
až	až	k9	až
1500	[number]	k4	1500
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
Velká	velký	k2eAgFnSc1d1	velká
rudá	rudý	k2eAgFnSc1d1	rudá
skvrna	skvrna	k1gFnSc1	skvrna
bude	být	k5eAaImBp3nS	být
muset	muset	k5eAaImF	muset
počkat	počkat	k5eAaPmF	počkat
na	na	k7c4	na
další	další	k2eAgFnPc4d1	další
expedice	expedice	k1gFnPc4	expedice
<g/>
.	.	kIx.	.
</s>
<s>
Kon-Tiki	Kon-Tiki	k6eAd1	Kon-Tiki
je	být	k5eAaImIp3nS	být
modul	modul	k1gInSc1	modul
s	s	k7c7	s
pomocným	pomocný	k2eAgInSc7d1	pomocný
balónem	balón	k1gInSc7	balón
plněným	plněný	k2eAgInSc7d1	plněný
horkým	horký	k2eAgInSc7d1	horký
vodíkem	vodík	k1gInSc7	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
pilot	pilot	k1gInSc1	pilot
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
svědkem	svědek	k1gMnSc7	svědek
neuvěřitelných	uvěřitelný	k2eNgFnPc2d1	neuvěřitelná
scenérií	scenérie	k1gFnPc2	scenérie
a	a	k8xC	a
úkazů	úkaz	k1gInPc2	úkaz
<g/>
.	.	kIx.	.
</s>
<s>
Pochopí	pochopit	k5eAaPmIp3nS	pochopit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
složité	složitý	k2eAgNnSc1d1	složité
odhadovat	odhadovat	k5eAaImF	odhadovat
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
<g/>
,	,	kIx,	,
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
obřím	obří	k2eAgInPc3d1	obří
rozměrům	rozměr	k1gInPc3	rozměr
planety	planeta	k1gFnSc2	planeta
je	být	k5eAaImIp3nS	být
horizont	horizont	k1gInSc1	horizont
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
jeho	jeho	k3xOp3gInSc2	jeho
odhadu	odhad	k1gInSc2	odhad
200	[number]	k4	200
km	km	kA	km
vzdálený	vzdálený	k2eAgMnSc1d1	vzdálený
<g/>
)	)	kIx)	)
2800	[number]	k4	2800
km	km	kA	km
daleko	daleko	k6eAd1	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
odhad	odhad	k1gInSc1	odhad
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
musí	muset	k5eAaImIp3nS	muset
násobit	násobit	k5eAaImF	násobit
minimálně	minimálně	k6eAd1	minimálně
deseti	deset	k4xCc7	deset
<g/>
.	.	kIx.	.
</s>
<s>
Obloha	obloha	k1gFnSc1	obloha
je	být	k5eAaImIp3nS	být
černá	černý	k2eAgFnSc1d1	černá
<g/>
,	,	kIx,	,
prohánějí	prohánět	k5eAaImIp3nP	prohánět
se	se	k3xPyFc4	se
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
amoniakové	amoniakový	k2eAgInPc1d1	amoniakový
cirry	cirr	k1gInPc4	cirr
<g/>
.	.	kIx.	.
</s>
<s>
Falcon	Falcon	k1gInSc1	Falcon
sbírá	sbírat	k5eAaImIp3nS	sbírat
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
přístrojů	přístroj	k1gInPc2	přístroj
lodi	loď	k1gFnSc6	loď
údaje	údaj	k1gInPc4	údaj
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
jsou	být	k5eAaImIp3nP	být
odesílány	odesílat	k5eAaImNgInP	odesílat
na	na	k7c4	na
řídící	řídící	k2eAgNnSc4d1	řídící
letové	letový	k2eAgNnSc4d1	letové
středisko	středisko	k1gNnSc4	středisko
na	na	k7c4	na
stanici	stanice	k1gFnSc4	stanice
Jupiter	Jupiter	k1gInSc1	Jupiter
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Taktéž	Taktéž	k?	Taktéž
vypustí	vypustit	k5eAaPmIp3nS	vypustit
sondu	sonda	k1gFnSc4	sonda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
chvíli	chvíle	k1gFnSc6	chvíle
jej	on	k3xPp3gMnSc4	on
rádiem	rádio	k1gNnSc7	rádio
kontaktuje	kontaktovat	k5eAaImIp3nS	kontaktovat
operátorka	operátorka	k1gFnSc1	operátorka
letového	letový	k2eAgNnSc2d1	letové
řízení	řízení	k1gNnSc2	řízení
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
se	se	k3xPyFc4	se
zaměří	zaměřit	k5eAaPmIp3nS	zaměřit
na	na	k7c4	na
kanál	kanál	k1gInSc4	kanál
46	[number]	k4	46
<g/>
.	.	kIx.	.
</s>
<s>
Howard	Howard	k1gInSc1	Howard
uslyší	uslyšet	k5eAaPmIp3nS	uslyšet
netypický	typický	k2eNgInSc1d1	netypický
zvuk	zvuk	k1gInSc1	zvuk
<g/>
,	,	kIx,	,
sílící	sílící	k2eAgNnSc1d1	sílící
dunivé	dunivý	k2eAgNnSc1d1	dunivé
chvění	chvění	k1gNnSc1	chvění
<g/>
.	.	kIx.	.
</s>
<s>
Dostane	dostat	k5eAaPmIp3nS	dostat
příkaz	příkaz	k1gInSc4	příkaz
vypustit	vypustit	k5eAaPmF	vypustit
další	další	k2eAgFnSc4d1	další
sondu	sonda	k1gFnSc4	sonda
<g/>
.	.	kIx.	.
</s>
<s>
Konzultuje	konzultovat	k5eAaImIp3nS	konzultovat
záhadný	záhadný	k2eAgInSc4d1	záhadný
zvuk	zvuk	k1gInSc4	zvuk
s	s	k7c7	s
vedoucím	vedoucí	k1gMnSc7	vedoucí
Exobiologického	Exobiologický	k2eAgNnSc2d1	Exobiologický
oddělení	oddělení	k1gNnSc2	oddělení
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Brennerem	Brenner	k1gMnSc7	Brenner
<g/>
.	.	kIx.	.
</s>
<s>
Brenner	Brenner	k1gInSc1	Brenner
mu	on	k3xPp3gMnSc3	on
sdělí	sdělit	k5eAaPmIp3nS	sdělit
<g/>
,	,	kIx,	,
že	že	k8xS	že
očekává	očekávat	k5eAaImIp3nS	očekávat
přítomnost	přítomnost	k1gFnSc4	přítomnost
mikroorgnismů	mikroorgnismus	k1gInPc2	mikroorgnismus
či	či	k8xC	či
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
žádný	žádný	k3yNgInSc1	žádný
větší	veliký	k2eAgInSc1d2	veliký
organismus	organismus	k1gInSc1	organismus
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
pro	pro	k7c4	pro
něj	on	k3xPp3gInSc4	on
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
energetická	energetický	k2eAgFnSc1d1	energetická
spotřeba	spotřeba	k1gFnSc1	spotřeba
příliš	příliš	k6eAd1	příliš
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
.	.	kIx.	.
</s>
<s>
Falcon	Falcon	k1gInSc1	Falcon
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
v	v	k7c6	v
plavbě	plavba	k1gFnSc6	plavba
atmosférou	atmosféra	k1gFnSc7	atmosféra
<g/>
,	,	kIx,	,
když	když	k8xS	když
zaregistruje	zaregistrovat	k5eAaPmIp3nS	zaregistrovat
několik	několik	k4yIc4	několik
kilometrů	kilometr	k1gInPc2	kilometr
velký	velký	k2eAgInSc4d1	velký
útvar	útvar	k1gInSc4	útvar
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
překvapením	překvapení	k1gNnSc7	překvapení
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
živý	živý	k2eAgMnSc1d1	živý
tvor	tvor	k1gMnSc1	tvor
<g/>
.	.	kIx.	.
</s>
<s>
Vypadá	vypadat	k5eAaImIp3nS	vypadat
jako	jako	k9	jako
pozemský	pozemský	k2eAgMnSc1d1	pozemský
rejnok	rejnok	k1gMnSc1	rejnok
v	v	k7c6	v
ohromujícím	ohromující	k2eAgNnSc6d1	ohromující
měřítku	měřítko	k1gNnSc6	měřítko
<g/>
.	.	kIx.	.
</s>
<s>
Falcon	Falcon	k1gMnSc1	Falcon
si	se	k3xPyFc3	se
povšimne	povšimnout	k5eAaPmIp3nS	povšimnout
dalších	další	k2eAgInPc2d1	další
a	a	k8xC	a
pozoruje	pozorovat	k5eAaImIp3nS	pozorovat
je	on	k3xPp3gNnSc4	on
<g/>
,	,	kIx,	,
rejnoci	rejnok	k1gMnPc1	rejnok
nejeví	jevit	k5eNaImIp3nP	jevit
o	o	k7c4	o
jeho	jeho	k3xOp3gNnSc4	jeho
plavidlo	plavidlo	k1gNnSc4	plavidlo
žádný	žádný	k3yNgInSc1	žádný
zájem	zájem	k1gInSc1	zájem
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
zpráva	zpráva	k1gFnSc1	zpráva
z	z	k7c2	z
letového	letový	k2eAgNnSc2d1	letové
střediska	středisko	k1gNnSc2	středisko
není	být	k5eNaImIp3nS	být
vůbec	vůbec	k9	vůbec
příjemná	příjemný	k2eAgFnSc1d1	příjemná
<g/>
.	.	kIx.	.
</s>
<s>
Zdroj	zdroj	k1gInSc1	zdroj
Beta	beta	k1gNnSc2	beta
se	se	k3xPyFc4	se
chystá	chystat	k5eAaImIp3nS	chystat
k	k	k7c3	k
výbuchu	výbuch	k1gInSc3	výbuch
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
planetě	planeta	k1gFnSc6	planeta
dochází	docházet	k5eAaImIp3nS	docházet
pravidelně	pravidelně	k6eAd1	pravidelně
k	k	k7c3	k
výtryskům	výtrysk	k1gInPc3	výtrysk
plynů	plyn	k1gInPc2	plyn
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
sebou	se	k3xPyFc7	se
nese	nést	k5eAaImIp3nS	nést
radiační	radiační	k2eAgFnSc4d1	radiační
a	a	k8xC	a
elektromagnetickou	elektromagnetický	k2eAgFnSc4d1	elektromagnetická
bouři	bouře	k1gFnSc4	bouře
<g/>
.	.	kIx.	.
</s>
<s>
Trocha	trocha	k1gFnSc1	trocha
se	se	k3xPyFc4	se
dostane	dostat	k5eAaPmIp3nS	dostat
i	i	k9	i
do	do	k7c2	do
ionosféry	ionosféra	k1gFnSc2	ionosféra
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
rozbouří	rozbouřit	k5eAaPmIp3nP	rozbouřit
radiační	radiační	k2eAgInPc1d1	radiační
pásy	pás	k1gInPc1	pás
obepínající	obepínající	k2eAgFnSc4d1	obepínající
planetu	planeta	k1gFnSc4	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Van	van	k1gInSc1	van
Allenovy	Allenův	k2eAgInPc1d1	Allenův
pásy	pás	k1gInPc1	pás
kolem	kolem	k7c2	kolem
Země	zem	k1gFnSc2	zem
jsou	být	k5eAaImIp3nP	být
oproti	oproti	k7c3	oproti
nim	on	k3xPp3gInPc3	on
jen	jen	k9	jen
slabým	slabý	k2eAgInSc7d1	slabý
odvarem	odvar	k1gInSc7	odvar
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Jupiteru	Jupiter	k1gInSc6	Jupiter
byla	být	k5eAaImAgFnS	být
identifikována	identifikován	k2eAgFnSc1d1	identifikována
4	[number]	k4	4
nejaktivnější	aktivní	k2eAgFnSc1d3	nejaktivnější
místa	místo	k1gNnPc1	místo
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
nejčastěji	často	k6eAd3	často
exploduje	explodovat	k5eAaBmIp3nS	explodovat
zdroj	zdroj	k1gInSc4	zdroj
Alfa	alfa	k1gFnSc1	alfa
<g/>
.	.	kIx.	.
</s>
<s>
Falcon	Falcon	k1gInSc1	Falcon
se	se	k3xPyFc4	se
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
modulem	modul	k1gInSc7	modul
nachází	nacházet	k5eAaImIp3nS	nacházet
nedaleko	daleko	k6eNd1	daleko
od	od	k7c2	od
zdroje	zdroj	k1gInSc2	zdroj
Beta	beta	k1gNnSc2	beta
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
právě	právě	k9	právě
chystá	chystat	k5eAaImIp3nS	chystat
vybuchnout	vybuchnout	k5eAaPmF	vybuchnout
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stane	stanout	k5eAaPmIp3nS	stanout
<g/>
,	,	kIx,	,
spatří	spatřit	k5eAaPmIp3nS	spatřit
Howard	Howard	k1gMnSc1	Howard
v	v	k7c6	v
dálce	dálka	k1gFnSc6	dálka
obrovitý	obrovitý	k2eAgInSc1d1	obrovitý
sloup	sloup	k1gInSc1	sloup
plynů	plyn	k1gInPc2	plyn
široký	široký	k2eAgMnSc1d1	široký
jako	jako	k8xC	jako
Tichý	tichý	k2eAgInSc1d1	tichý
oceán	oceán	k1gInSc1	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
něj	on	k3xPp3gInSc2	on
řádí	řádit	k5eAaImIp3nP	řádit
hromy	hrom	k1gInPc4	hrom
a	a	k8xC	a
blesky	blesk	k1gInPc4	blesk
a	a	k8xC	a
šíří	šířit	k5eAaImIp3nS	šířit
se	se	k3xPyFc4	se
tlaková	tlakový	k2eAgFnSc1d1	tlaková
vlna	vlna	k1gFnSc1	vlna
<g/>
.	.	kIx.	.
</s>
<s>
Astronaut	astronaut	k1gMnSc1	astronaut
zatahuje	zatahovat	k5eAaImIp3nS	zatahovat
všechny	všechen	k3xTgInPc4	všechen
nástavce	nástavec	k1gInPc4	nástavec
s	s	k7c7	s
přístroji	přístroj	k1gInPc7	přístroj
dovnitř	dovnitř	k6eAd1	dovnitř
a	a	k8xC	a
čeká	čekat	k5eAaImIp3nS	čekat
<g/>
,	,	kIx,	,
až	až	k8xS	až
přejde	přejít	k5eAaPmIp3nS	přejít
nejhorší	zlý	k2eAgInSc4d3	Nejhorší
nápor	nápor	k1gInSc4	nápor
<g/>
.	.	kIx.	.
</s>
<s>
Zachvátí	zachvátit	k5eAaPmIp3nS	zachvátit
jej	on	k3xPp3gNnSc4	on
jakási	jakýsi	k3yIgFnSc1	jakýsi
paralýza	paralýza	k1gFnSc1	paralýza
a	a	k8xC	a
pocítí	pocítit	k5eAaPmIp3nS	pocítit
palčivou	palčivý	k2eAgFnSc4d1	palčivá
bolest	bolest	k1gFnSc4	bolest
<g/>
.	.	kIx.	.
</s>
<s>
Elektrické	elektrický	k2eAgNnSc1d1	elektrické
pole	pole	k1gNnSc1	pole
zasáhne	zasáhnout	k5eAaPmIp3nS	zasáhnout
i	i	k9	i
kabinu	kabina	k1gFnSc4	kabina
<g/>
,	,	kIx,	,
přístrojová	přístrojový	k2eAgFnSc1d1	přístrojová
deska	deska	k1gFnSc1	deska
plápolá	plápolat	k5eAaImIp3nS	plápolat
barevnou	barevna	k1gFnSc7	barevna
září	září	k1gNnSc2	září
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
slyšet	slyšet	k5eAaImF	slyšet
praskání	praskání	k1gNnPc4	praskání
výbojů	výboj	k1gInPc2	výboj
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
chvíli	chvíle	k1gFnSc6	chvíle
naskočí	naskočit	k5eAaPmIp3nS	naskočit
světlo	světlo	k1gNnSc1	světlo
a	a	k8xC	a
systémy	systém	k1gInPc1	systém
lodi	loď	k1gFnSc2	loď
začnou	začít	k5eAaPmIp3nP	začít
opět	opět	k6eAd1	opět
pracovat	pracovat	k5eAaImF	pracovat
<g/>
.	.	kIx.	.
</s>
<s>
Spojení	spojení	k1gNnSc1	spojení
se	s	k7c7	s
střediskem	středisko	k1gNnSc7	středisko
je	být	k5eAaImIp3nS	být
opět	opět	k6eAd1	opět
navázáno	navázat	k5eAaPmNgNnS	navázat
<g/>
.	.	kIx.	.
</s>
<s>
Howard	Howard	k1gInSc1	Howard
letí	letět	k5eAaImIp3nS	letět
dál	daleko	k6eAd2	daleko
a	a	k8xC	a
před	před	k7c7	před
svítáním	svítání	k1gNnSc7	svítání
zaregistruje	zaregistrovat	k5eAaPmIp3nS	zaregistrovat
podivný	podivný	k2eAgInSc1d1	podivný
jev	jev	k1gInSc1	jev
zvaný	zvaný	k2eAgInSc1d1	zvaný
"	"	kIx"	"
<g/>
kola	kola	k1gFnSc1	kola
poseidonova	poseidonův	k2eAgFnSc1d1	poseidonův
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Zapne	zapnout	k5eAaPmIp3nS	zapnout
dosud	dosud	k6eAd1	dosud
nevyužitý	využitý	k2eNgInSc1d1	nevyužitý
horizontální	horizontální	k2eAgInSc1d1	horizontální
radar	radar	k1gInSc1	radar
(	(	kIx(	(
<g/>
zatím	zatím	k6eAd1	zatím
používal	používat	k5eAaImAgInS	používat
jen	jen	k9	jen
vertikální	vertikální	k2eAgNnSc4d1	vertikální
pro	pro	k7c4	pro
kontrolu	kontrola	k1gFnSc4	kontrola
letové	letový	k2eAgFnSc2d1	letová
hladiny	hladina	k1gFnSc2	hladina
<g/>
)	)	kIx)	)
a	a	k8xC	a
s	s	k7c7	s
údivem	údiv	k1gInSc7	údiv
zaznamená	zaznamenat	k5eAaPmIp3nS	zaznamenat
celé	celý	k2eAgInPc4d1	celý
tucty	tucet	k1gInPc4	tucet
radarových	radarový	k2eAgFnPc2d1	radarová
ozvěn	ozvěna	k1gFnPc2	ozvěna
před	před	k7c7	před
Kon-Tiki	Kon-Tiki	k1gNnSc7	Kon-Tiki
<g/>
.	.	kIx.	.
ještě	ještě	k6eAd1	ještě
musí	muset	k5eAaImIp3nS	muset
zabojovat	zabojovat	k5eAaPmF	zabojovat
s	s	k7c7	s
gigantickým	gigantický	k2eAgInSc7d1	gigantický
vzdušným	vzdušný	k2eAgInSc7d1	vzdušný
vírem	vír	k1gInSc7	vír
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
věnovat	věnovat	k5eAaImF	věnovat
analýze	analýza	k1gFnSc3	analýza
radarových	radarový	k2eAgInPc2d1	radarový
odrazů	odraz	k1gInPc2	odraz
<g/>
.	.	kIx.	.
</s>
<s>
Nejbližší	blízký	k2eAgMnSc1d3	nejbližší
se	se	k3xPyFc4	se
nyní	nyní	k6eAd1	nyní
nachází	nacházet	k5eAaImIp3nS	nacházet
asi	asi	k9	asi
40	[number]	k4	40
km	km	kA	km
daleko	daleko	k6eAd1	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Zamíří	zamířit	k5eAaPmIp3nS	zamířit
tím	ten	k3xDgInSc7	ten
směrem	směr	k1gInSc7	směr
teleskop	teleskop	k1gInSc1	teleskop
a	a	k8xC	a
zpozoruje	zpozorovat	k5eAaPmIp3nS	zpozorovat
zvláštní	zvláštní	k2eAgInSc1d1	zvláštní
skvrnitý	skvrnitý	k2eAgInSc1d1	skvrnitý
oblak	oblak	k1gInSc1	oblak
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
připomíná	připomínat	k5eAaImIp3nS	připomínat
chobotnici	chobotnice	k1gFnSc4	chobotnice
či	či	k8xC	či
medúzu	medúza	k1gFnSc4	medúza
<g/>
.	.	kIx.	.
</s>
<s>
Stane	stanout	k5eAaPmIp3nS	stanout
se	s	k7c7	s
svědkem	svědek	k1gMnSc7	svědek
jejího	její	k3xOp3gNnSc2	její
napadení	napadení	k1gNnSc2	napadení
rejnoky	rejnok	k1gMnPc4	rejnok
-	-	kIx~	-
jinou	jiný	k2eAgFnSc7d1	jiná
formou	forma	k1gFnSc7	forma
místního	místní	k2eAgInSc2d1	místní
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
medúza	medúza	k1gFnSc1	medúza
nemá	mít	k5eNaImIp3nS	mít
proti	proti	k7c3	proti
nim	on	k3xPp3gMnPc3	on
šanci	šance	k1gFnSc4	šance
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
ale	ale	k8xC	ale
vyšlehne	vyšlehnout	k5eAaPmIp3nS	vyšlehnout
oslepující	oslepující	k2eAgInSc4d1	oslepující
záblesk	záblesk	k1gInSc4	záblesk
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
útočníků	útočník	k1gMnPc2	útočník
se	se	k3xPyFc4	se
řítí	řítit	k5eAaImIp3nS	řítit
dolů	dolů	k6eAd1	dolů
a	a	k8xC	a
za	za	k7c7	za
ním	on	k3xPp3gMnSc7	on
se	se	k3xPyFc4	se
táhne	táhnout	k5eAaImIp3nS	táhnout
černý	černý	k2eAgInSc1d1	černý
dým	dým	k1gInSc1	dým
<g/>
.	.	kIx.	.
</s>
<s>
Falconovi	Falcon	k1gMnSc3	Falcon
to	ten	k3xDgNnSc1	ten
až	až	k9	až
zlověstně	zlověstně	k6eAd1	zlověstně
připomíná	připomínat	k5eAaImIp3nS	připomínat
sestřelený	sestřelený	k2eAgInSc1d1	sestřelený
letoun	letoun	k1gInSc1	letoun
z	z	k7c2	z
oblohy	obloha	k1gFnSc2	obloha
<g/>
.	.	kIx.	.
</s>
<s>
Rejnoci	rejnok	k1gMnPc1	rejnok
mizí	mizet	k5eAaImIp3nP	mizet
<g/>
.	.	kIx.	.
</s>
<s>
Pozoruje	pozorovat	k5eAaImIp3nS	pozorovat
dál	daleko	k6eAd2	daleko
medúzu	medúza	k1gFnSc4	medúza
a	a	k8xC	a
zaregistruje	zaregistrovat	k5eAaPmIp3nS	zaregistrovat
zajímavý	zajímavý	k2eAgInSc1d1	zajímavý
úkaz	úkaz	k1gInSc1	úkaz
na	na	k7c6	na
jejím	její	k3xOp3gNnSc6	její
těle	tělo	k1gNnSc6	tělo
-	-	kIx~	-
pravidelnou	pravidelný	k2eAgFnSc4d1	pravidelná
šachovnici	šachovnice	k1gFnSc4	šachovnice
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Brennerem	Brenner	k1gMnSc7	Brenner
se	se	k3xPyFc4	se
shodnou	shodnout	k5eAaPmIp3nP	shodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
anténu	anténa	k1gFnSc4	anténa
v	v	k7c6	v
pásmu	pásmo	k1gNnSc6	pásmo
metrových	metrový	k2eAgFnPc2d1	metrová
vln	vlna	k1gFnPc2	vlna
<g/>
.	.	kIx.	.
</s>
<s>
Zdejší	zdejší	k2eAgMnPc1d1	zdejší
tvorové	tvor	k1gMnPc1	tvor
jsou	být	k5eAaImIp3nP	být
uzpůsobeni	uzpůsobit	k5eAaPmNgMnP	uzpůsobit
k	k	k7c3	k
příjmu	příjem	k1gInSc3	příjem
a	a	k8xC	a
zužitkovávání	zužitkovávání	k1gNnSc6	zužitkovávání
rádiových	rádiový	k2eAgFnPc2d1	rádiová
vln	vlna	k1gFnPc2	vlna
<g/>
.	.	kIx.	.
</s>
<s>
Debatu	debata	k1gFnSc4	debata
přeruší	přerušit	k5eAaPmIp3nS	přerušit
velitel	velitel	k1gMnSc1	velitel
řídícího	řídící	k2eAgNnSc2d1	řídící
letového	letový	k2eAgNnSc2d1	letové
střediska	středisko	k1gNnSc2	středisko
a	a	k8xC	a
oznámí	oznámit	k5eAaPmIp3nS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
možnému	možný	k2eAgInSc3d1	možný
inteligentnímu	inteligentní	k2eAgInSc3d1	inteligentní
životu	život	k1gInSc3	život
na	na	k7c6	na
Jupiteru	Jupiter	k1gInSc6	Jupiter
vstupují	vstupovat	k5eAaImIp3nP	vstupovat
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
Základní	základní	k2eAgNnSc1d1	základní
ustanovení	ustanovení	k1gNnSc1	ustanovení
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc1	jehož
první	první	k4xOgNnSc1	první
pravidlo	pravidlo	k1gNnSc1	pravidlo
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
dalo	dát	k5eAaPmAgNnS	dát
stručně	stručně	k6eAd1	stručně
shrnout	shrnout	k5eAaPmF	shrnout
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
nepodnikej	podnikat	k5eNaImRp2nS	podnikat
pokusy	pokus	k1gInPc1	pokus
o	o	k7c4	o
přiblížení	přiblížení	k1gNnSc4	přiblížení
či	či	k8xC	či
navázání	navázání	k1gNnSc4	navázání
komunikace	komunikace	k1gFnSc2	komunikace
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
i	i	k8xC	i
"	"	kIx"	"
<g/>
oni	onen	k3xDgMnPc1	onen
<g/>
"	"	kIx"	"
měli	mít	k5eAaImAgMnP	mít
dostatek	dostatek	k1gInSc4	dostatek
času	čas	k1gInSc2	čas
na	na	k7c6	na
prozkoumání	prozkoumání	k1gNnSc6	prozkoumání
lidského	lidský	k2eAgMnSc2d1	lidský
zástupce	zástupce	k1gMnSc2	zástupce
<g/>
.	.	kIx.	.
</s>
<s>
Kolik	kolik	k4yIc4	kolik
času	čas	k1gInSc2	čas
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c4	na
posouzení	posouzení	k1gNnSc4	posouzení
přítomného	přítomný	k2eAgMnSc2d1	přítomný
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
zodpovědnost	zodpovědnost	k1gFnSc1	zodpovědnost
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
na	na	k7c6	na
Howardu	Howard	k1gMnSc6	Howard
Falconovi	Falcon	k1gMnSc6	Falcon
<g/>
.	.	kIx.	.
</s>
<s>
Astronaut	astronaut	k1gMnSc1	astronaut
podcení	podcenit	k5eAaPmIp3nS	podcenit
situaci	situace	k1gFnSc4	situace
a	a	k8xC	a
dostane	dostat	k5eAaPmIp3nS	dostat
se	se	k3xPyFc4	se
do	do	k7c2	do
kontaktu	kontakt	k1gInSc2	kontakt
s	s	k7c7	s
medúzou	medúza	k1gFnSc7	medúza
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
přiblížila	přiblížit	k5eAaPmAgFnS	přiblížit
shora	shora	k6eAd1	shora
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
útočí	útočit	k5eAaImIp3nS	útočit
<g/>
,	,	kIx,	,
či	či	k8xC	či
jen	jen	k9	jen
prozkoumává	prozkoumávat	k5eAaImIp3nS	prozkoumávat
ze	z	k7c2	z
zvědavosti	zvědavost	k1gFnSc2	zvědavost
záhadnou	záhadný	k2eAgFnSc4d1	záhadná
věc	věc	k1gFnSc4	věc
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pro	pro	k7c4	pro
astronauta	astronaut	k1gMnSc4	astronaut
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
ohrožení	ohrožení	k1gNnSc4	ohrožení
<g/>
.	.	kIx.	.
</s>
<s>
Odhazuje	odhazovat	k5eAaImIp3nS	odhazovat
balón	balón	k1gInSc1	balón
<g/>
,	,	kIx,	,
startuje	startovat	k5eAaBmIp3nS	startovat
nukleární	nukleární	k2eAgInPc4d1	nukleární
raketové	raketový	k2eAgInPc4d1	raketový
motory	motor	k1gInPc4	motor
a	a	k8xC	a
uniká	unikat	k5eAaImIp3nS	unikat
z	z	k7c2	z
dosahu	dosah	k1gInSc2	dosah
tvora	tvor	k1gMnSc2	tvor
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
určitou	určitý	k2eAgFnSc4d1	určitá
dobu	doba	k1gFnSc4	doba
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
oběžné	oběžný	k2eAgFnPc4d1	oběžná
dráhy	dráha	k1gFnPc4	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
je	být	k5eAaImIp3nS	být
Falcon	Falcon	k1gMnSc1	Falcon
hrdinou	hrdina	k1gMnSc7	hrdina
<g/>
.	.	kIx.	.
</s>
<s>
Webster	Webster	k1gInSc1	Webster
jej	on	k3xPp3gMnSc4	on
informuje	informovat	k5eAaBmIp3nS	informovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
společnost	společnost	k1gFnSc1	společnost
získá	získat	k5eAaPmIp3nS	získat
na	na	k7c6	na
novém	nový	k2eAgInSc6d1	nový
zájmu	zájem	k1gInSc6	zájem
o	o	k7c4	o
Jupiter	Jupiter	k1gInSc4	Jupiter
díky	díky	k7c3	díky
jeho	jeho	k3xOp3gFnSc3	jeho
úspěšné	úspěšný	k2eAgFnSc3d1	úspěšná
misi	mise	k1gFnSc3	mise
a	a	k8xC	a
předloží	předložit	k5eAaPmIp3nS	předložit
mu	on	k3xPp3gMnSc3	on
domněnku	domněnka	k1gFnSc4	domněnka
-	-	kIx~	-
myslí	myslet	k5eAaImIp3nS	myslet
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
medúza	medúza	k1gFnSc1	medúza
dobře	dobře	k6eAd1	dobře
věděla	vědět	k5eAaImAgFnS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
měl	mít	k5eAaImAgInS	mít
Falcon	Falcon	k1gInSc4	Falcon
nahoře	nahoře	k6eAd1	nahoře
zakrytý	zakrytý	k2eAgInSc4d1	zakrytý
výhled	výhled	k1gInSc4	výhled
<g/>
.	.	kIx.	.
</s>
<s>
Howard	Howard	k1gMnSc1	Howard
Falcon	Falcon	k1gMnSc1	Falcon
odjíždí	odjíždět	k5eAaImIp3nS	odjíždět
na	na	k7c6	na
svých	svůj	k3xOyFgFnPc6	svůj
pneumatikách	pneumatika	k1gFnPc6	pneumatika
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
o	o	k7c4	o
nějž	jenž	k3xRgMnSc4	jenž
přišel	přijít	k5eAaPmAgMnS	přijít
při	při	k7c6	při
havárii	havárie	k1gFnSc6	havárie
Královny	královna	k1gFnSc2	královna
Alžběty	Alžběta	k1gFnSc2	Alžběta
IV	IV	kA	IV
<g/>
.	.	kIx.	.
má	mít	k5eAaImIp3nS	mít
kovový	kovový	k2eAgInSc4d1	kovový
válec	válec	k1gInSc4	válec
s	s	k7c7	s
ukrytým	ukrytý	k2eAgInSc7d1	ukrytý
systémem	systém	k1gInSc7	systém
zabezpečujícím	zabezpečující	k2eAgInSc7d1	zabezpečující
jeho	jeho	k3xOp3gInPc4	jeho
životní	životní	k2eAgInPc4d1	životní
pochody	pochod	k1gInPc4	pochod
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
zdává	zdávat	k5eAaImIp3nS	zdávat
o	o	k7c6	o
šimpanzovi	šimpanz	k1gMnSc6	šimpanz
ze	z	k7c2	z
zřícené	zřícený	k2eAgFnSc2d1	zřícená
vzducholodi	vzducholoď	k1gFnSc2	vzducholoď
<g/>
.	.	kIx.	.
</s>
<s>
Kdo	kdo	k3yInSc1	kdo
jednou	jednou	k6eAd1	jednou
opanuje	opanovat	k5eAaPmIp3nS	opanovat
vesmír	vesmír	k1gInSc4	vesmír
<g/>
,	,	kIx,	,
nebude	být	k5eNaImBp3nS	být
mít	mít	k5eAaImF	mít
křehkou	křehký	k2eAgFnSc4d1	křehká
skořápku	skořápka	k1gFnSc4	skořápka
<g/>
.	.	kIx.	.
</s>
<s>
Nebude	být	k5eNaImBp3nS	být
to	ten	k3xDgNnSc1	ten
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stroje	stroj	k1gInPc1	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Falcon	Falcon	k1gInSc1	Falcon
již	již	k6eAd1	již
není	být	k5eNaImIp3nS	být
ani	ani	k9	ani
jedním	jeden	k4xCgInSc7	jeden
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
druhým	druhý	k4xOgInSc7	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
již	již	k6eAd1	již
smířil	smířit	k5eAaPmAgInS	smířit
s	s	k7c7	s
osudem	osud	k1gInSc7	osud
<g/>
,	,	kIx,	,
pociťuje	pociťovat	k5eAaImIp3nS	pociťovat
své	svůj	k3xOyFgNnSc4	svůj
výlučné	výlučný	k2eAgNnSc4d1	výlučné
osamění	osamění	k1gNnSc4	osamění
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zachycen	zachytit	k5eAaPmNgInS	zachytit
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgInPc7	dva
světy	svět	k1gInPc7	svět
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
ten	ten	k3xDgMnSc1	ten
nebohý	nebohý	k2eAgMnSc1d1	nebohý
šimpanz	šimpanz	k1gMnSc1	šimpanz
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
prostředníkem	prostředník	k1gInSc7	prostředník
mezi	mezi	k7c7	mezi
starým	starý	k2eAgInSc7d1	starý
a	a	k8xC	a
novým	nový	k2eAgInSc7d1	nový
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
tvory	tvor	k1gMnPc4	tvor
z	z	k7c2	z
uhlíku	uhlík	k1gInSc2	uhlík
a	a	k8xC	a
tvory	tvor	k1gMnPc7	tvor
z	z	k7c2	z
kovu	kov	k1gInSc2	kov
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
je	on	k3xPp3gMnPc4	on
jednou	jednou	k6eAd1	jednou
nahradí	nahradit	k5eAaPmIp3nP	nahradit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
příběhu	příběh	k1gInSc6	příběh
je	být	k5eAaImIp3nS	být
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
katastrofě	katastrofa	k1gFnSc6	katastrofa
německé	německý	k2eAgFnSc2d1	německá
vzducholodi	vzducholoď	k1gFnSc2	vzducholoď
LZ	LZ	kA	LZ
129	[number]	k4	129
Hindenburg	Hindenburg	k1gInSc1	Hindenburg
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
havarovala	havarovat	k5eAaPmAgFnS	havarovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1937	[number]	k4	1937
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
katastrofa	katastrofa	k1gFnSc1	katastrofa
předznamenala	předznamenat	k5eAaPmAgFnS	předznamenat
ústup	ústup	k1gInSc4	ústup
od	od	k7c2	od
používání	používání	k1gNnSc2	používání
vzducholodí	vzducholoď	k1gFnPc2	vzducholoď
<g/>
.	.	kIx.	.
</s>
<s>
Kosmické	kosmický	k2eAgNnSc1d1	kosmické
plavidlo	plavidlo	k1gNnSc1	plavidlo
hlavní	hlavní	k2eAgFnSc2d1	hlavní
postavy	postava	k1gFnSc2	postava
Howarda	Howard	k1gMnSc2	Howard
Falcona	Falcon	k1gMnSc2	Falcon
-	-	kIx~	-
modul	modul	k1gInSc4	modul
vybavený	vybavený	k2eAgInSc4d1	vybavený
balónem	balón	k1gInSc7	balón
s	s	k7c7	s
horkým	horký	k2eAgInSc7d1	horký
vodíkem	vodík	k1gInSc7	vodík
-	-	kIx~	-
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
Kon-Tiki	Kon-Tik	k1gFnSc2	Kon-Tik
<g/>
.	.	kIx.	.
</s>
<s>
Stejný	stejný	k2eAgInSc1d1	stejný
název	název	k1gInSc1	název
nesl	nést	k5eAaImAgInS	nést
i	i	k9	i
vor	vor	k1gInSc1	vor
expedice	expedice	k1gFnSc2	expedice
norského	norský	k2eAgMnSc2d1	norský
cestovatele	cestovatel	k1gMnSc2	cestovatel
Thora	Thor	k1gMnSc2	Thor
Heyerdahla	Heyerdahla	k1gMnSc2	Heyerdahla
<g/>
.	.	kIx.	.
</s>
<s>
Novela	novela	k1gFnSc1	novela
Setkání	setkání	k1gNnSc2	setkání
s	s	k7c7	s
medúzou	medúza	k1gFnSc7	medúza
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
inspirací	inspirace	k1gFnSc7	inspirace
pro	pro	k7c4	pro
román	román	k1gInSc4	román
amerického	americký	k2eAgMnSc2d1	americký
spisovatele	spisovatel	k1gMnSc2	spisovatel
žánru	žánr	k1gInSc2	žánr
sci-fi	scii	k1gNnSc2	sci-fi
Paula	Paul	k1gMnSc2	Paul
Preusse	Preuss	k1gMnSc2	Preuss
s	s	k7c7	s
názvem	název	k1gInSc7	název
Střetnutí	střetnutí	k1gNnSc1	střetnutí
s	s	k7c7	s
medúzou	medúza	k1gFnSc7	medúza
ze	z	k7c2	z
série	série	k1gFnSc2	série
Zlatý	zlatý	k2eAgInSc4d1	zlatý
věk	věk	k1gInSc4	věk
Venuše	Venuše	k1gFnSc2	Venuše
<g/>
.	.	kIx.	.
</s>
<s>
Česky	česky	k6eAd1	česky
vyšla	vyjít	k5eAaPmAgFnS	vyjít
povídka	povídka	k1gFnSc1	povídka
v	v	k7c6	v
následujících	následující	k2eAgFnPc6d1	následující
sbírkách	sbírka	k1gFnPc6	sbírka
nebo	nebo	k8xC	nebo
antologiích	antologie	k1gFnPc6	antologie
<g/>
:	:	kIx,	:
Zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
třetí	třetí	k4xOgFnSc6	třetí
planetě	planeta	k1gFnSc6	planeta
<g/>
,	,	kIx,	,
Práce	práce	k1gFnSc1	práce
1982	[number]	k4	1982
.	.	kIx.	.
</s>
<s>
Devět	devět	k4xCc1	devět
miliard	miliarda	k4xCgFnPc2	miliarda
božích	boží	k2eAgFnPc2d1	boží
jmen	jméno	k1gNnPc2	jméno
<g/>
,	,	kIx,	,
Baronet	baroneta	k1gFnPc2	baroneta
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7214	[number]	k4	7214
<g/>
-	-	kIx~	-
<g/>
455	[number]	k4	455
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Hlídka	hlídka	k1gFnSc1	hlídka
<g/>
,	,	kIx,	,
Knižní	knižní	k2eAgInSc1d1	knižní
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7176	[number]	k4	7176
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
60	[number]	k4	60
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Objevitelé	objevitel	k1gMnPc1	objevitel
<g/>
:	:	kIx,	:
Dobrodružná	dobrodružný	k2eAgFnSc1d1	dobrodružná
science	science	k1gFnSc1	science
fiction	fiction	k1gInSc4	fiction
ze	z	k7c2	z
vzdáleného	vzdálený	k2eAgInSc2d1	vzdálený
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
,	,	kIx,	,
Laser-books	Laserooks	k1gInSc1	Laser-books
(	(	kIx(	(
<g/>
edice	edice	k1gFnSc1	edice
SF	SF	kA	SF
č.	č.	k?	č.
<g/>
186	[number]	k4	186
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7193	[number]	k4	7193
<g/>
-	-	kIx~	-
<g/>
233	[number]	k4	233
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Velmistři	velmistr	k1gMnPc1	velmistr
SF	SF	kA	SF
2	[number]	k4	2
<g/>
,	,	kIx,	,
Baronet	baronet	k1gMnSc1	baronet
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7214	[number]	k4	7214
<g/>
-	-	kIx~	-
<g/>
514	[number]	k4	514
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
