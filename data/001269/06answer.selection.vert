<s>
Setkání	setkání	k1gNnSc1	setkání
s	s	k7c7	s
medúzou	medúza	k1gFnSc7	medúza
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
A	a	k9	a
Meeting	meeting	k1gInSc1	meeting
with	with	k1gInSc1	with
Medusa	Medusa	k1gFnSc1	Medusa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
sci-fi	scii	k1gNnSc1	sci-fi
novela	novela	k1gFnSc1	novela
spisovatele	spisovatel	k1gMnSc2	spisovatel
Arthura	Arthur	k1gMnSc2	Arthur
C.	C.	kA	C.
Clarka	Clarek	k1gMnSc2	Clarek
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
poprvé	poprvé	k6eAd1	poprvé
publikována	publikovat	k5eAaBmNgFnS	publikovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
a	a	k8xC	a
v	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
cenu	cena	k1gFnSc4	cena
Nebula	nebula	k1gFnSc1	nebula
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
"	"	kIx"	"
<g/>
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
novela	novela	k1gFnSc1	novela
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
