<s>
Univerzita	univerzita	k1gFnSc1
v	v	k7c6
Basileji	Basilej	k1gFnSc6
(	(	kIx(
<g/>
Universität	Universität	k2eAgInSc1d1
Basel	Basel	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
založená	založený	k2eAgFnSc1d1
v	v	k7c6
roce	rok	k1gInSc6
1460	#num#	k4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
nejstarší	starý	k2eAgFnSc1d3
a	a	k8xC
dodnes	dodnes	k6eAd1
jedna	jeden	k4xCgFnSc1
z	z	k7c2
nejvýznamnějších	významný	k2eAgFnPc2d3
univerzit	univerzita	k1gFnPc2
ve	v	k7c6
Švýcarsku	Švýcarsko	k1gNnSc6
<g/>
.	.	kIx.
</s>