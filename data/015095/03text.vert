<s>
Univerzita	univerzita	k1gFnSc1
v	v	k7c6
Basileji	Basilej	k1gFnSc6
</s>
<s>
Univerzita	univerzita	k1gFnSc1
v	v	k7c6
Basileji	Basilej	k1gFnSc6
Datum	datum	k1gNnSc1
založení	založení	k1gNnSc2
</s>
<s>
1460	#num#	k4
Počet	počet	k1gInSc4
studentů	student	k1gMnPc2
</s>
<s>
12	#num#	k4
852	#num#	k4
Další	další	k2eAgFnPc4d1
informace	informace	k1gFnPc4
Adresa	adresa	k1gFnSc1
</s>
<s>
Basilej	Basilej	k1gFnSc1
<g/>
,	,	kIx,
CH-	CH-	k1gFnSc1
<g/>
4003	#num#	k4
<g/>
,	,	kIx,
Švýcarsko	Švýcarsko	k1gNnSc1
Zeměpisné	zeměpisný	k2eAgFnSc2d1
souřadnice	souřadnice	k1gFnSc2
</s>
<s>
47	#num#	k4
<g/>
°	°	k?
<g/>
33	#num#	k4
<g/>
′	′	k?
<g/>
31	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
7	#num#	k4
<g/>
°	°	k?
<g/>
35	#num#	k4
<g/>
′	′	k?
<g/>
0	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Členství	členství	k1gNnSc1
</s>
<s>
Utrechtská	Utrechtský	k2eAgFnSc1d1
síť	síť	k1gFnSc1
<g/>
,	,	kIx,
IIIF	IIIF	kA
Consortium	Consortium	k1gNnSc1
<g/>
,	,	kIx,
Asociace	asociace	k1gFnSc1
evropských	evropský	k2eAgFnPc2d1
univerzit	univerzita	k1gFnPc2
a	a	k8xC
Informationsdienst	Informationsdienst	k1gMnSc1
Wissenschaft	Wissenschaft	k1gMnSc1
e.V.	e.V.	k?
</s>
<s>
www.unibas.ch	www.unibas.ch	k1gInSc1
Některá	některý	k3yIgNnPc4
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Univerzitní	univerzitní	k2eAgFnSc1d1
knihovna	knihovna	k1gFnSc1
</s>
<s>
Univerzita	univerzita	k1gFnSc1
v	v	k7c6
Basileji	Basilej	k1gFnSc6
(	(	kIx(
<g/>
Universität	Universität	k2eAgInSc1d1
Basel	Basel	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
založená	založený	k2eAgFnSc1d1
v	v	k7c6
roce	rok	k1gInSc6
1460	#num#	k4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
nejstarší	starý	k2eAgFnSc1d3
a	a	k8xC
dodnes	dodnes	k6eAd1
jedna	jeden	k4xCgFnSc1
z	z	k7c2
nejvýznamnějších	významný	k2eAgFnPc2d3
univerzit	univerzita	k1gFnPc2
ve	v	k7c6
Švýcarsku	Švýcarsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
světových	světový	k2eAgInPc6d1
žebříčcích	žebříček	k1gInPc6
hodnocení	hodnocení	k1gNnSc2
se	se	k3xPyFc4
umísťuje	umísťovat	k5eAaImIp3nS
kolem	kolem	k7c2
100	#num#	k4
<g/>
.	.	kIx.
místa	místo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
sedmi	sedm	k4xCc6
fakultách	fakulta	k1gFnPc6
a	a	k8xC
řadě	řada	k1gFnSc3
institutů	institut	k1gInPc2
studuje	studovat	k5eAaImIp3nS
přes	přes	k7c4
11	#num#	k4
000	#num#	k4
studentů	student	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svými	svůj	k3xOyFgInPc7
vědeckými	vědecký	k2eAgInPc7d1
výsledky	výsledek	k1gInPc7
se	se	k3xPyFc4
univerzita	univerzita	k1gFnSc1
proslavila	proslavit	k5eAaPmAgFnS
například	například	k6eAd1
v	v	k7c6
astronomii	astronomie	k1gFnSc6
<g/>
,	,	kIx,
jazykovědě	jazykověda	k1gFnSc6
a	a	k8xC
tropické	tropický	k2eAgFnSc6d1
medicíně	medicína	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc1
knihovna	knihovna	k1gFnSc1
je	být	k5eAaImIp3nS
největší	veliký	k2eAgFnSc1d3
ve	v	k7c6
Švýcarsku	Švýcarsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Univerzita	univerzita	k1gFnSc1
byla	být	k5eAaImAgFnS
založena	založit	k5eAaPmNgFnS
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
Basilejským	basilejský	k2eAgInSc7d1
koncilem	koncil	k1gInSc7
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
papežskou	papežský	k2eAgFnSc7d1
bulou	bula	k1gFnSc7
z	z	k7c2
roku	rok	k1gInSc2
1458	#num#	k4
<g/>
,	,	kIx,
slavnostní	slavnostní	k2eAgNnSc1d1
otevření	otevření	k1gNnSc1
bylo	být	k5eAaImAgNnS
4	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1460	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původně	původně	k6eAd1
měla	mít	k5eAaImAgFnS
čtyři	čtyři	k4xCgFnPc4
fakulty	fakulta	k1gFnPc4
(	(	kIx(
<g/>
artistickou	artistický	k2eAgFnSc4d1
<g/>
,	,	kIx,
teologickou	teologický	k2eAgFnSc4d1
<g/>
,	,	kIx,
právnickou	právnický	k2eAgFnSc4d1
a	a	k8xC
lékařskou	lékařský	k2eAgFnSc4d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
absolvování	absolvování	k1gNnSc6
artistické	artistický	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
bylo	být	k5eAaImAgNnS
až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
1818	#num#	k4
podmínkou	podmínka	k1gFnSc7
pro	pro	k7c4
vstup	vstup	k1gInSc4
na	na	k7c4
tři	tři	k4xCgMnPc4
ostatní	ostatní	k2eAgMnPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
univerzitou	univerzita	k1gFnSc7
se	se	k3xPyFc4
Basilej	Basilej	k1gFnSc1
stala	stát	k5eAaPmAgFnS
střediskem	středisko	k1gNnSc7
vzdělanosti	vzdělanost	k1gFnSc2
<g/>
,	,	kIx,
humanismu	humanismus	k1gInSc2
a	a	k8xC
také	také	k9
knihtisku	knihtisk	k1gInSc2
ve	v	k7c6
Švýcarsku	Švýcarsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
novější	nový	k2eAgFnSc6d2
době	doba	k1gFnSc6
přibyly	přibýt	k5eAaPmAgFnP
další	další	k2eAgInPc1d1
tři	tři	k4xCgFnPc1
fakulty	fakulta	k1gFnPc1
<g/>
,	,	kIx,
přírodovědecká	přírodovědecký	k2eAgFnSc1d1
<g/>
,	,	kIx,
ekonomická	ekonomický	k2eAgFnSc1d1
a	a	k8xC
psychologická	psychologický	k2eAgFnSc1d1
a	a	k8xC
řada	řada	k1gFnSc1
univerzitních	univerzitní	k2eAgMnPc2d1
institutů	institut	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Osobnosti	osobnost	k1gFnPc1
</s>
<s>
V	v	k7c6
průběhu	průběh	k1gInSc6
historie	historie	k1gFnSc2
zde	zde	k6eAd1
přednášeli	přednášet	k5eAaImAgMnP
například	například	k6eAd1
Erasmus	Erasmus	k1gMnSc1
Rotterdamský	rotterdamský	k2eAgMnSc1d1
<g/>
,	,	kIx,
Daniel	Daniel	k1gMnSc1
Bernoulli	Bernoulli	kA
<g/>
,	,	kIx,
Jacob	Jacoba	k1gFnPc2
Burckhardt	Burckhardta	k1gFnPc2
<g/>
,	,	kIx,
Leonhard	Leonhard	k1gMnSc1
Euler	Euler	k1gMnSc1
<g/>
,	,	kIx,
Friedrich	Friedrich	k1gMnSc1
Nietzsche	Nietzsch	k1gFnSc2
<g/>
,	,	kIx,
Fritz	Fritz	k1gMnSc1
Haber	Haber	k1gMnSc1
<g/>
,	,	kIx,
Adolf	Adolf	k1gMnSc1
Portmann	Portmann	k1gMnSc1
<g/>
,	,	kIx,
Carl	Carl	k1gMnSc1
Gustav	Gustav	k1gMnSc1
Jung	Jung	k1gMnSc1
<g/>
,	,	kIx,
Karl	Karl	k1gMnSc1
Barth	Barth	k1gMnSc1
<g/>
,	,	kIx,
Karl	Karl	k1gMnSc1
Jaspers	Jaspers	k1gInSc1
<g/>
,	,	kIx,
Walter	Walter	k1gMnSc1
Muschg	Muschg	k1gMnSc1
nebo	nebo	k8xC
Hans	Hans	k1gMnSc1
Urs	Urs	k1gFnSc2
von	von	k1gInSc1
Balthasar	Balthasar	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
profesoři	profesor	k1gMnPc1
zde	zde	k6eAd1
působili	působit	k5eAaImAgMnP
český	český	k2eAgMnSc1d1
historik	historik	k1gMnSc1
František	František	k1gMnSc1
Graus	Graus	k1gMnSc1
a	a	k8xC
protestantský	protestantský	k2eAgMnSc1d1
teolog	teolog	k1gMnSc1
Jan	Jan	k1gMnSc1
Milíč	Milíč	k1gMnSc1
Lochman	Lochman	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
byl	být	k5eAaImAgMnS
dvakrát	dvakrát	k6eAd1
zvolen	zvolit	k5eAaPmNgMnS
rektorem	rektor	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Basilej	Basilej	k1gFnSc1
</s>
<s>
Seznam	seznam	k1gInSc1
nejstarších	starý	k2eAgFnPc2d3
univerzit	univerzita	k1gFnPc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Univerzita	univerzita	k1gFnSc1
v	v	k7c6
Basileji	Basilej	k1gFnSc6
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
univerzity	univerzita	k1gFnSc2
v	v	k7c6
Basileji	Basilej	k1gFnSc6
</s>
<s>
Dějiny	dějiny	k1gFnPc1
univerzity	univerzita	k1gFnSc2
v	v	k7c6
Basileji	Basilej	k1gFnSc6
</s>
<s>
Švýcarský	švýcarský	k2eAgInSc1d1
institut	institut	k1gInSc1
tropické	tropický	k2eAgFnSc2d1
medicíny	medicína	k1gFnSc2
</s>
<s>
Praktické	praktický	k2eAgFnPc1d1
informace	informace	k1gFnPc1
o	o	k7c6
univerzitě	univerzita	k1gFnSc6
v	v	k7c6
Basileji	Basilej	k1gFnSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Švýcarsko	Švýcarsko	k1gNnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
kn	kn	k?
<g/>
20010710026	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
36146-X	36146-X	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0004	#num#	k4
1937	#num#	k4
0642	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
50079502	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
155492003	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
50079502	#num#	k4
</s>
