<p>
<s>
Ara	ara	k1gMnSc1	ara
kaninda	kanind	k1gMnSc2	kanind
(	(	kIx(	(
<g/>
Ara	ara	k1gMnSc1	ara
glaucogularis	glaucogularis	k1gFnSc2	glaucogularis
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
papoušek	papoušek	k1gMnSc1	papoušek
endemický	endemický	k2eAgInSc1d1	endemický
k	k	k7c3	k
malému	malý	k2eAgNnSc3d1	malé
území	území	k1gNnSc3	území
na	na	k7c4	na
hranici	hranice	k1gFnSc4	hranice
střední	střední	k2eAgFnSc2d1	střední
a	a	k8xC	a
severní	severní	k2eAgFnSc2d1	severní
Bolívie	Bolívie	k1gFnSc2	Bolívie
známém	známý	k1gMnSc6	známý
jako	jako	k8xS	jako
Los	los	k1gInSc4	los
Llanos	Llanos	k1gInPc2	Llanos
de	de	k?	de
Moxos	Moxosa	k1gFnPc2	Moxosa
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
nedávných	dávný	k2eNgInPc2d1	nedávný
průzkumů	průzkum	k1gInPc2	průzkum
čítá	čítat	k5eAaImIp3nS	čítat
populace	populace	k1gFnSc1	populace
tohoto	tento	k3xDgMnSc2	tento
ary	ara	k1gMnSc2	ara
pouze	pouze	k6eAd1	pouze
250	[number]	k4	250
<g/>
–	–	k?	–
<g/>
300	[number]	k4	300
volně	volně	k6eAd1	volně
žijících	žijící	k2eAgMnPc2d1	žijící
jedinců	jedinec	k1gMnPc2	jedinec
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
je	být	k5eAaImIp3nS	být
i	i	k9	i
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
kriticky	kriticky	k6eAd1	kriticky
ohrožený	ohrožený	k2eAgInSc4d1	ohrožený
druh	druh	k1gInSc4	druh
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
příčinou	příčina	k1gFnSc7	příčina
tohoto	tento	k3xDgInSc2	tento
statusu	status	k1gInSc2	status
je	být	k5eAaImIp3nS	být
přitom	přitom	k6eAd1	přitom
nelegální	legální	k2eNgInSc1d1	nelegální
lov	lov	k1gInSc1	lov
a	a	k8xC	a
odchyt	odchyt	k1gInSc1	odchyt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Tohoto	tento	k3xDgMnSc4	tento
papouška	papoušek	k1gMnSc4	papoušek
si	se	k3xPyFc3	se
lze	lze	k6eAd1	lze
snadno	snadno	k6eAd1	snadno
splést	splést	k5eAaPmF	splést
s	s	k7c7	s
arou	ara	k1gMnSc7	ara
araraunou	araraunout	k5eAaPmIp3nP	araraunout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
při	při	k7c6	při
bližším	blízký	k2eAgNnSc6d2	bližší
prozkoumání	prozkoumání	k1gNnSc6	prozkoumání
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
hrdle	hrdla	k1gFnSc6	hrdla
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
ary	ara	k1gMnSc2	ara
ararauny	ararauna	k1gFnSc2	ararauna
modrou	modrý	k2eAgFnSc4d1	modrá
barvu	barva	k1gFnSc4	barva
a	a	k8xC	a
jemnou	jemný	k2eAgFnSc4d1	jemná
hlavu	hlava	k1gFnSc4	hlava
zdobí	zdobit	k5eAaImIp3nP	zdobit
méně	málo	k6eAd2	málo
masivní	masivní	k2eAgInSc4d1	masivní
zobák	zobák	k1gInSc4	zobák
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgNnSc1d1	základní
zbarvení	zbarvení	k1gNnSc1	zbarvení
je	být	k5eAaImIp3nS	být
modré	modrý	k2eAgNnSc1d1	modré
a	a	k8xC	a
žlutooranžové	žlutooranžový	k2eAgNnSc1d1	žlutooranžové
<g/>
.	.	kIx.	.
</s>
<s>
Zobák	zobák	k1gInSc1	zobák
je	být	k5eAaImIp3nS	být
černý	černý	k2eAgMnSc1d1	černý
<g/>
,	,	kIx,	,
kulaté	kulatý	k2eAgNnSc1d1	kulaté
oko	oko	k1gNnSc1	oko
má	mít	k5eAaImIp3nS	mít
žlutavou	žlutavý	k2eAgFnSc4d1	žlutavá
duhovku	duhovka	k1gFnSc4	duhovka
<g/>
.	.	kIx.	.
</s>
<s>
Mladí	mladý	k2eAgMnPc1d1	mladý
ptáci	pták	k1gMnPc1	pták
ji	on	k3xPp3gFnSc4	on
však	však	k9	však
mají	mít	k5eAaImIp3nP	mít
do	do	k7c2	do
jednoho	jeden	k4xCgInSc2	jeden
roku	rok	k1gInSc2	rok
spíše	spíše	k9	spíše
šedou	šedý	k2eAgFnSc7d1	šedá
<g/>
.	.	kIx.	.
</s>
<s>
Částečně	částečně	k6eAd1	částečně
holá	holý	k2eAgFnSc1d1	holá
oblast	oblast	k1gFnSc1	oblast
obličeje	obličej	k1gInSc2	obličej
bývá	bývat	k5eAaImIp3nS	bývat
bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
pták	pták	k1gMnSc1	pták
rozrušený	rozrušený	k2eAgMnSc1d1	rozrušený
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
dostat	dostat	k5eAaPmF	dostat
nádech	nádech	k1gInSc1	nádech
do	do	k7c2	do
růžova	růžovo	k1gNnSc2	růžovo
<g/>
.	.	kIx.	.
</s>
<s>
Váží	vážit	k5eAaImIp3nS	vážit
obvykle	obvykle	k6eAd1	obvykle
kolem	kolem	k7c2	kolem
750	[number]	k4	750
gramů	gram	k1gInPc2	gram
a	a	k8xC	a
měří	měřit	k5eAaImIp3nS	měřit
88	[number]	k4	88
centimetrů	centimetr	k1gInPc2	centimetr
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
délky	délka	k1gFnSc2	délka
papouška	papoušek	k1gMnSc2	papoušek
tvoří	tvořit	k5eAaImIp3nS	tvořit
ocas	ocas	k1gInSc4	ocas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
ara	ara	k1gMnSc1	ara
kaninda	kanind	k1gMnSc2	kanind
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
ara	ara	k1gMnSc1	ara
kaninda	kanind	k1gMnSc2	kanind
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
