<s>
Mont	Mont	k1gMnSc1	Mont
Blanc	Blanc	k1gMnSc1	Blanc
(	(	kIx(	(
<g/>
italsky	italsky	k6eAd1	italsky
Monte	Mont	k1gInSc5	Mont
Bianco	bianco	k2eAgMnSc6d1	bianco
<g/>
,	,	kIx,	,
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
bílá	bílý	k2eAgFnSc1d1	bílá
hora	hora	k1gFnSc1	hora
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
Alp	Alpy	k1gFnPc2	Alpy
<g/>
,	,	kIx,	,
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
vypínající	vypínající	k2eAgFnSc2d1	vypínající
se	se	k3xPyFc4	se
v	v	k7c6	v
Montblanském	Montblanský	k2eAgInSc6d1	Montblanský
masivu	masiv	k1gInSc6	masiv
na	na	k7c6	na
francouzsko-italském	francouzskotalský	k2eAgNnSc6d1	francouzsko-italské
pomezí	pomezí	k1gNnSc6	pomezí
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
4809	[number]	k4	4809
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
</s>
<s>
Mont	Mont	k2eAgMnSc1d1	Mont
Blanc	Blanc	k1gMnSc1	Blanc
je	být	k5eAaImIp3nS	být
tradičně	tradičně	k6eAd1	tradičně
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
vrchol	vrchol	k1gInSc4	vrchol
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
nejednotné	jednotný	k2eNgFnSc2d1	nejednotná
metodiky	metodika	k1gFnSc2	metodika
pro	pro	k7c4	pro
přesné	přesný	k2eAgNnSc4d1	přesné
určení	určení	k1gNnSc4	určení
evropsko-asijské	evropskosijský	k2eAgFnSc2d1	evropsko-asijský
hranice	hranice	k1gFnSc2	hranice
bývá	bývat	k5eAaImIp3nS	bývat
tento	tento	k3xDgInSc1	tento
titul	titul	k1gInSc1	titul
připisován	připisovat	k5eAaImNgInS	připisovat
i	i	k8xC	i
kavkazské	kavkazský	k2eAgNnSc1d1	kavkazské
hoře	hoře	k1gNnSc1	hoře
Elbrus	Elbrus	k1gInSc1	Elbrus
(	(	kIx(	(
<g/>
5642	[number]	k4	5642
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
vrchol	vrchol	k1gInSc1	vrchol
hory	hora	k1gFnSc2	hora
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
či	či	k8xC	či
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
území	území	k1gNnSc6	území
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
panují	panovat	k5eAaImIp3nP	panovat
dosud	dosud	k6eAd1	dosud
spory	spor	k1gInPc1	spor
<g/>
.	.	kIx.	.
</s>
<s>
Mapa	mapa	k1gFnSc1	mapa
francouzského	francouzský	k2eAgMnSc2d1	francouzský
Institut	institut	k1gInSc1	institut
Géographique	Géographiqu	k1gMnSc2	Géographiqu
National	National	k1gMnSc2	National
de	de	k?	de
France	Franc	k1gMnSc2	Franc
nicméně	nicméně	k8xC	nicméně
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
vrchol	vrchol	k1gInSc4	vrchol
Mt	Mt	k1gFnSc2	Mt
<g/>
.	.	kIx.	.
</s>
<s>
Blancu	Blanc	k1gMnSc3	Blanc
zcela	zcela	k6eAd1	zcela
na	na	k7c4	na
území	území	k1gNnSc4	území
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
ostře	ostro	k6eAd1	ostro
brání	bránit	k5eAaImIp3nS	bránit
italská	italský	k2eAgFnSc1d1	italská
strana	strana	k1gFnSc1	strana
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
hranice	hranice	k1gFnSc1	hranice
procházející	procházející	k2eAgFnSc1d1	procházející
přímo	přímo	k6eAd1	přímo
vrcholem	vrchol	k1gInSc7	vrchol
Mt	Mt	k1gFnSc2	Mt
<g/>
.	.	kIx.	.
</s>
<s>
Blancu	Blanc	k1gMnSc3	Blanc
byla	být	k5eAaImAgNnP	být
výslovně	výslovně	k6eAd1	výslovně
stanovena	stanovit	k5eAaPmNgNnP	stanovit
stále	stále	k6eAd1	stále
platnou	platný	k2eAgFnSc7d1	platná
dvojstrannou	dvojstranný	k2eAgFnSc7d1	dvojstranná
smlouvou	smlouva	k1gFnSc7	smlouva
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1860	[number]	k4	1860
(	(	kIx(	(
<g/>
jíž	jenž	k3xRgFnSc2	jenž
bylo	být	k5eAaImAgNnS	být
postoupeno	postoupit	k5eAaPmNgNnS	postoupit
Savojsko	Savojsko	k1gNnSc1	Savojsko
Francii	Francie	k1gFnSc3	Francie
a	a	k8xC	a
Mt	Mt	k1gFnSc3	Mt
<g/>
.	.	kIx.	.
</s>
<s>
Blanc	Blanc	k1gMnSc1	Blanc
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
horou	hora	k1gFnSc7	hora
hraniční	hraniční	k2eAgFnSc2d1	hraniční
<g/>
)	)	kIx)	)
a	a	k8xC	a
potvrzena	potvrzen	k2eAgFnSc1d1	potvrzena
i	i	k9	i
Pařížskou	pařížský	k2eAgFnSc7d1	Pařížská
mírovou	mírový	k2eAgFnSc7d1	mírová
smlouvou	smlouva	k1gFnSc7	smlouva
z	z	k7c2	z
r.	r.	kA	r.
1947	[number]	k4	1947
(	(	kIx(	(
<g/>
jíž	jenž	k3xRgFnSc2	jenž
byly	být	k5eAaImAgFnP	být
částečně	částečně	k6eAd1	částečně
upraveny	upraven	k2eAgFnPc4d1	upravena
hranice	hranice	k1gFnPc4	hranice
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgFnPc7	dva
zeměmi	zem	k1gFnPc7	zem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
ovšem	ovšem	k9	ovšem
popírá	popírat	k5eAaImIp3nS	popírat
francouzská	francouzský	k2eAgFnSc1d1	francouzská
strana	strana	k1gFnSc1	strana
a	a	k8xC	a
výzva	výzva	k1gFnSc1	výzva
italských	italský	k2eAgMnPc2d1	italský
úřadů	úřad	k1gInPc2	úřad
o	o	k7c4	o
definitivní	definitivní	k2eAgNnSc4d1	definitivní
vyřešení	vyřešení	k1gNnSc4	vyřešení
hraničního	hraniční	k2eAgInSc2d1	hraniční
sporu	spor	k1gInSc2	spor
se	se	k3xPyFc4	se
setkala	setkat	k5eAaPmAgFnS	setkat
z	z	k7c2	z
francouzské	francouzský	k2eAgFnSc2d1	francouzská
strany	strana	k1gFnSc2	strana
s	s	k7c7	s
mlčením	mlčení	k1gNnSc7	mlčení
<g/>
.	.	kIx.	.
</s>
<s>
Hornina	hornina	k1gFnSc1	hornina
<g/>
,	,	kIx,	,
tyčící	tyčící	k2eAgFnSc1d1	tyčící
se	se	k3xPyFc4	se
do	do	k7c2	do
nadmořské	nadmořský	k2eAgFnSc2d1	nadmořská
výšky	výška	k1gFnSc2	výška
4792	[number]	k4	4792
m	m	kA	m
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
kryta	krýt	k5eAaImNgFnS	krýt
ledovým	ledový	k2eAgInSc7d1	ledový
příkrovem	příkrov	k1gInSc7	příkrov
o	o	k7c6	o
proměnlivé	proměnlivý	k2eAgFnSc6d1	proměnlivá
tloušťce	tloušťka	k1gFnSc6	tloušťka
14	[number]	k4	14
<g/>
-	-	kIx~	-
<g/>
23	[number]	k4	23
m.	m.	k?	m.
Při	při	k7c6	při
expertním	expertní	k2eAgNnSc6d1	expertní
měření	měření	k1gNnSc6	měření
<g/>
,	,	kIx,	,
provedeném	provedený	k2eAgNnSc6d1	provedené
v	v	k7c6	v
září	září	k1gNnSc6	září
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
výška	výška	k1gFnSc1	výška
hory	hora	k1gFnSc2	hora
stanovena	stanovit	k5eAaPmNgFnS	stanovit
na	na	k7c4	na
4810,45	[number]	k4	4810,45
metrů	metr	k1gInPc2	metr
nad	nad	k7c7	nad
hladinou	hladina	k1gFnSc7	hladina
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnPc1d1	další
měření	měření	k1gNnPc1	měření
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
výšku	výška	k1gFnSc4	výška
4810,02	[number]	k4	4810,02
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
a	a	k8xC	a
podle	podle	k7c2	podle
měření	měření	k1gNnSc2	měření
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
se	se	k3xPyFc4	se
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
bod	bod	k1gInSc1	bod
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
4808,73	[number]	k4	4808,73
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Mont	Mont	k1gMnSc1	Mont
Blanc	Blanc	k1gMnSc1	Blanc
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
horninami	hornina	k1gFnPc7	hornina
krystalinika	krystalinikum	k1gNnPc1	krystalinikum
(	(	kIx(	(
<g/>
žula	žula	k1gFnSc1	žula
<g/>
,	,	kIx,	,
rula	rula	k1gFnSc1	rula
a	a	k8xC	a
břidlice	břidlice	k1gFnSc1	břidlice
<g/>
)	)	kIx)	)
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
pokryt	pokrýt	k5eAaPmNgInS	pokrýt
mnoha	mnoho	k4c7	mnoho
ledovci	ledovec	k1gInPc7	ledovec
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgMnSc1d3	veliký
z	z	k7c2	z
nich	on	k3xPp3gNnPc2	on
nese	nést	k5eAaImIp3nS	nést
jméno	jméno	k1gNnSc4	jméno
Mer	Mer	k1gMnSc1	Mer
de	de	k?	de
Glace	Glace	k1gMnSc1	Glace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1957-1965	[number]	k4	1957-1965
byl	být	k5eAaImAgInS	být
pod	pod	k7c7	pod
Montblanským	Montblanský	k2eAgInSc7d1	Montblanský
masivem	masiv	k1gInSc7	masiv
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
11,6	[number]	k4	11,6
km	km	kA	km
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
Montblanský	Montblanský	k2eAgInSc1d1	Montblanský
tunel	tunel	k1gInSc1	tunel
<g/>
,	,	kIx,	,
spojující	spojující	k2eAgInSc1d1	spojující
města	město	k1gNnSc2	město
Chamonix	Chamonix	k1gNnSc1	Chamonix
(	(	kIx(	(
<g/>
Horní	horní	k2eAgNnSc1d1	horní
Savojsko	Savojsko	k1gNnSc1	Savojsko
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
)	)	kIx)	)
a	a	k8xC	a
Courmayeur	Courmayeur	k1gMnSc1	Courmayeur
(	(	kIx(	(
<g/>
Valle	Valle	k1gInSc1	Valle
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Aosta	Aosta	k1gMnSc1	Aosta
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
východním	východní	k2eAgNnSc6d1	východní
úbočí	úbočí	k1gNnSc6	úbočí
Mont	Mont	k2eAgInSc1d1	Mont
Blanku	Blanka	k1gFnSc4	Blanka
vede	vést	k5eAaImIp3nS	vést
visutá	visutý	k2eAgFnSc1d1	visutá
lanovka	lanovka	k1gFnSc1	lanovka
z	z	k7c2	z
francouzského	francouzský	k2eAgNnSc2d1	francouzské
Chamonix	Chamonix	k1gNnSc2	Chamonix
(	(	kIx(	(
<g/>
1030	[number]	k4	1030
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
do	do	k7c2	do
italského	italský	k2eAgInSc2d1	italský
Courmayeru	Courmayer	k1gInSc2	Courmayer
(	(	kIx(	(
<g/>
1370	[number]	k4	1370
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Překonává	překonávat	k5eAaImIp3nS	překonávat
horní	horní	k2eAgFnSc1d1	horní
část	část	k1gFnSc1	část
ledovce	ledovec	k1gInSc2	ledovec
Mer	Mer	k1gFnSc2	Mer
de	de	k?	de
Glace	Glace	k1gMnSc1	Glace
zvanou	zvaný	k2eAgFnSc4d1	zvaná
Vallée	Vallée	k1gFnSc4	Vallée
Blanche	Blanch	k1gInSc2	Blanch
celkem	celkem	k6eAd1	celkem
v	v	k7c6	v
šesti	šest	k4xCc6	šest
úsecích	úsek	k1gInPc6	úsek
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
přestupní	přestupní	k2eAgFnSc1d1	přestupní
stanice	stanice	k1gFnSc1	stanice
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
Aiguille	Aiguill	k1gInSc6	Aiguill
du	du	k?	du
Midi	Mid	k1gFnSc2	Mid
(	(	kIx(	(
<g/>
3842	[number]	k4	3842
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
Na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
poprvé	poprvé	k6eAd1	poprvé
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
Francouzi	Francouz	k1gMnPc1	Francouz
8	[number]	k4	8
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1786	[number]	k4	1786
Jacques	Jacques	k1gMnSc1	Jacques
Balmat	Balmat	k1gInSc1	Balmat
a	a	k8xC	a
Michel	Michel	k1gMnSc1	Michel
Paccard	Paccard	k1gMnSc1	Paccard
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
ženou	žena	k1gFnSc7	žena
<g/>
,	,	kIx,	,
zdolavší	zdolavší	k2eAgMnSc1d1	zdolavší
Mont	Mont	k1gMnSc1	Mont
Blanc	Blanc	k1gMnSc1	Blanc
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
14	[number]	k4	14
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1808	[number]	k4	1808
Marie	Maria	k1gFnSc2	Maria
Paradisová	Paradisový	k2eAgFnSc1d1	Paradisová
<g/>
.	.	kIx.	.
</s>
<s>
Datum	datum	k1gNnSc1	datum
jejího	její	k3xOp3gInSc2	její
výstupu	výstup	k1gInSc2	výstup
však	však	k9	však
není	být	k5eNaImIp3nS	být
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
:	:	kIx,	:
uvádí	uvádět	k5eAaImIp3nS	uvádět
se	se	k3xPyFc4	se
i	i	k9	i
léta	léto	k1gNnPc4	léto
1809	[number]	k4	1809
či	či	k8xC	či
1811	[number]	k4	1811
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
fotografie	fotografie	k1gFnSc1	fotografie
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
Mont	Monta	k1gFnPc2	Monta
Blancu	Blanc	k1gMnSc3	Blanc
pořídili	pořídit	k5eAaPmAgMnP	pořídit
bratři	bratr	k1gMnPc1	bratr
Louis-Auguste	Louis-Auguste	k1gMnSc1	Louis-Auguste
Bisson	Bisson	k1gMnSc1	Bisson
a	a	k8xC	a
Auguste-Rosalie	Auguste-Rosalie	k1gFnSc1	Auguste-Rosalie
Bisson	Bissona	k1gFnPc2	Bissona
(	(	kIx(	(
<g/>
1826	[number]	k4	1826
<g/>
-	-	kIx~	-
<g/>
1900	[number]	k4	1900
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1861	[number]	k4	1861
<g/>
.	.	kIx.	.
</s>
<s>
Putovalo	putovat	k5eAaImAgNnS	putovat
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
pětadvacet	pětadvacet	k4xCc1	pětadvacet
nosičů	nosič	k1gMnPc2	nosič
technického	technický	k2eAgNnSc2d1	technické
vybavení	vybavení	k1gNnSc2	vybavení
<g/>
.	.	kIx.	.
</s>
<s>
Museli	muset	k5eAaImAgMnP	muset
nahoru	nahoru	k6eAd1	nahoru
dopravit	dopravit	k5eAaPmF	dopravit
celou	celý	k2eAgFnSc4d1	celá
temnou	temný	k2eAgFnSc4d1	temná
komoru	komora	k1gFnSc4	komora
včetně	včetně	k7c2	včetně
kamínek	kamínka	k1gNnPc2	kamínka
na	na	k7c4	na
rozehřátí	rozehřátý	k2eAgMnPc1d1	rozehřátý
sněhu	sníh	k1gInSc2	sníh
<g/>
,	,	kIx,	,
vše	všechen	k3xTgNnSc1	všechen
okolo	okolo	k7c2	okolo
250	[number]	k4	250
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Fotografie	fotografia	k1gFnPc1	fotografia
byly	být	k5eAaImAgFnP	být
provedeny	provést	k5eAaPmNgFnP	provést
s	s	k7c7	s
využitím	využití	k1gNnSc7	využití
kolodiového	kolodiový	k2eAgInSc2d1	kolodiový
procesu	proces	k1gInSc2	proces
na	na	k7c4	na
velkoformátové	velkoformátový	k2eAgInPc4d1	velkoformátový
negativy	negativ	k1gInPc4	negativ
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
až	až	k9	až
30	[number]	k4	30
×	×	k?	×
40	[number]	k4	40
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
Mont	Mont	k2eAgInSc4d1	Mont
Blancu	Blanc	k1gMnSc6	Blanc
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
vystoupit	vystoupit	k5eAaPmF	vystoupit
čtyřmi	čtyři	k4xCgFnPc7	čtyři
výstupovými	výstupový	k2eAgFnPc7d1	výstupová
trasami	trasa	k1gFnPc7	trasa
nižší	nízký	k2eAgFnSc2d2	nižší
obtížnosti	obtížnost	k1gFnSc2	obtížnost
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
normálky	normálka	k1gFnSc2	normálka
<g/>
)	)	kIx)	)
a	a	k8xC	a
značným	značný	k2eAgInSc7d1	značný
množství	množství	k1gNnSc1	množství
horolezeckých	horolezecký	k2eAgInPc2d1	horolezecký
výstupů	výstup	k1gInPc2	výstup
vyšších	vysoký	k2eAgFnPc2d2	vyšší
obtížností	obtížnost	k1gFnPc2	obtížnost
<g/>
.	.	kIx.	.
</s>
<s>
Trasami	trasa	k1gFnPc7	trasa
nižší	nízký	k2eAgFnSc2d2	nižší
obtížnosti	obtížnost	k1gFnSc2	obtížnost
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Cesta	cesta	k1gFnSc1	cesta
přes	přes	k7c4	přes
Dôme	Dôme	k1gFnSc4	Dôme
du	du	k?	du
Goû	Goû	k1gFnSc1	Goû
(	(	kIx(	(
<g/>
obtížnost	obtížnost	k1gFnSc1	obtížnost
PD	PD	kA	PD
<g/>
-	-	kIx~	-
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
nejfrekventovanější	frekventovaný	k2eAgFnSc1d3	nejfrekventovanější
cesta	cesta	k1gFnSc1	cesta
výstupu	výstup	k1gInSc2	výstup
na	na	k7c4	na
Mont	Mont	k1gInSc4	Mont
Blanc	Blanc	k1gMnSc1	Blanc
<g/>
,	,	kIx,	,
nejčastější	častý	k2eAgFnSc1d3	nejčastější
cesta	cesta	k1gFnSc1	cesta
vůdců	vůdce	k1gMnPc2	vůdce
s	s	k7c7	s
klienty	klient	k1gMnPc7	klient
a	a	k8xC	a
zákazníky	zákazník	k1gMnPc7	zákazník
cestovních	cestovní	k2eAgFnPc2d1	cestovní
kanceláří	kancelář	k1gFnPc2	kancelář
<g/>
.	.	kIx.	.
</s>
<s>
Východiskem	východisko	k1gNnSc7	východisko
je	být	k5eAaImIp3nS	být
Chamonix	Chamonix	k1gNnSc4	Chamonix
nebo	nebo	k8xC	nebo
Saint-Gervais-les-Bains	Saint-Gervaises-Bains	k1gInSc4	Saint-Gervais-les-Bains
na	na	k7c6	na
francouzské	francouzský	k2eAgFnSc6d1	francouzská
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
vrcholový	vrcholový	k2eAgInSc4d1	vrcholový
den	den	k1gInSc4	den
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
vyráží	vyrážet	k5eAaImIp3nS	vyrážet
z	z	k7c2	z
chaty	chata	k1gFnSc2	chata
Gouter	Goutra	k1gFnPc2	Goutra
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
3817	[number]	k4	3817
m	m	kA	m
n.	n.	k?	n.
<g/>
m.	m.	k?	m.
Cesta	cesta	k1gFnSc1	cesta
přes	přes	k7c4	přes
Aiguille	Aiguille	k1gFnPc4	Aiguille
du	du	k?	du
Midi	Midi	k1gNnSc1	Midi
(	(	kIx(	(
<g/>
obtížnost	obtížnost	k1gFnSc1	obtížnost
PD	PD	kA	PD
<g/>
+	+	kIx~	+
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Též	též	k9	též
zvaná	zvaný	k2eAgNnPc1d1	zvané
cestou	cestou	k7c2	cestou
tří	tři	k4xCgFnPc2	tři
Mont	Monta	k1gFnPc2	Monta
Blanců	Blanc	k1gMnPc2	Blanc
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
sleduje	sledovat	k5eAaImIp3nS	sledovat
hřeben	hřeben	k1gInSc4	hřeben
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
několik	několik	k4yIc1	několik
dalších	další	k2eAgInPc2d1	další
významných	významný	k2eAgInPc2d1	významný
vrcholů	vrchol	k1gInPc2	vrchol
masivu	masiv	k1gInSc2	masiv
Mont	Mont	k1gMnSc1	Mont
Blancu	Blanc	k1gMnSc3	Blanc
<g/>
.	.	kIx.	.
</s>
<s>
Technicky	technicky	k6eAd1	technicky
nejnáročnější	náročný	k2eAgMnPc4d3	nejnáročnější
ze	z	k7c2	z
snadných	snadný	k2eAgInPc2d1	snadný
výstupů	výstup	k1gInPc2	výstup
<g/>
,	,	kIx,	,
při	při	k7c6	při
sestupu	sestup	k1gInSc6	sestup
se	se	k3xPyFc4	se
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
místě	místo	k1gNnSc6	místo
krátký	krátký	k2eAgInSc4d1	krátký
úsek	úsek	k1gInSc4	úsek
slaňuje	slaňovat	k5eAaImIp3nS	slaňovat
<g/>
.	.	kIx.	.
</s>
<s>
Východiskem	východisko	k1gNnSc7	východisko
je	být	k5eAaImIp3nS	být
Chamonix	Chamonix	k1gNnSc1	Chamonix
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
koncová	koncový	k2eAgFnSc1d1	koncová
stanice	stanice	k1gFnSc1	stanice
lanovky	lanovka	k1gFnSc2	lanovka
Aiguille	Aiguille	k1gFnSc2	Aiguille
du	du	k?	du
Midi	Mid	k1gFnSc2	Mid
v	v	k7c6	v
3842	[number]	k4	3842
m	m	kA	m
n.	n.	k?	n.
<g/>
m.	m.	k?	m.
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
vede	vést	k5eAaImIp3nS	vést
přes	přes	k7c4	přes
Mont	Mont	k1gInSc4	Mont
Blanc	Blanc	k1gMnSc1	Blanc
du	du	k?	du
Tacul	Tacul	k1gInSc1	Tacul
a	a	k8xC	a
Mont	Mont	k1gMnSc1	Mont
Maudit	Maudit	k1gMnSc1	Maudit
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
přes	přes	k7c4	přes
Grand	grand	k1gMnSc1	grand
Mulets	Mulets	k1gInSc1	Mulets
(	(	kIx(	(
<g/>
obtížnost	obtížnost	k1gFnSc1	obtížnost
F	F	kA	F
<g/>
/	/	kIx~	/
<g/>
PD	PD	kA	PD
<g/>
-	-	kIx~	-
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
technicky	technicky	k6eAd1	technicky
nejsnazším	snadný	k2eAgInSc7d3	nejsnazší
výstupem	výstup	k1gInSc7	výstup
a	a	k8xC	a
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
je	být	k5eAaImIp3nS	být
shodná	shodný	k2eAgFnSc1d1	shodná
s	s	k7c7	s
cestou	cesta	k1gFnSc7	cesta
prvovýstupců	prvovýstupec	k1gInPc2	prvovýstupec
<g/>
.	.	kIx.	.
</s>
<s>
Obtíže	obtíž	k1gFnPc1	obtíž
a	a	k8xC	a
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
cesty	cesta	k1gFnSc2	cesta
v	v	k7c6	v
letních	letní	k2eAgInPc6d1	letní
měsících	měsíc	k1gInPc6	měsíc
spočívají	spočívat	k5eAaImIp3nP	spočívat
v	v	k7c6	v
množství	množství	k1gNnSc6	množství
ledovcových	ledovcový	k2eAgFnPc2d1	ledovcová
trhlin	trhlina	k1gFnPc2	trhlina
a	a	k8xC	a
ohrožení	ohrožení	k1gNnSc4	ohrožení
pádem	pád	k1gInSc7	pád
seraků	serak	k1gInPc2	serak
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
přes	přes	k7c4	přes
Aiguilles	Aiguilles	k1gInSc4	Aiguilles
Grises	Grises	k1gInSc1	Grises
(	(	kIx(	(
<g/>
obtížnost	obtížnost	k1gFnSc1	obtížnost
PD	PD	kA	PD
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
standardní	standardní	k2eAgFnSc7d1	standardní
cestou	cesta	k1gFnSc7	cesta
z	z	k7c2	z
italské	italský	k2eAgFnSc2d1	italská
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
obtížnějších	obtížný	k2eAgInPc2d2	obtížnější
výstupů	výstup	k1gInPc2	výstup
jsou	být	k5eAaImIp3nP	být
populární	populární	k2eAgFnPc1d1	populární
ledové	ledový	k2eAgFnPc1d1	ledová
a	a	k8xC	a
kombinované	kombinovaný	k2eAgInPc1d1	kombinovaný
výstupy	výstup	k1gInPc1	výstup
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
stěně	stěna	k1gFnSc6	stěna
Brenva	Brenvo	k1gNnSc2	Brenvo
nad	nad	k7c7	nad
ledovcem	ledovec	k1gInSc7	ledovec
Brenva	Brenvo	k1gNnSc2	Brenvo
<g/>
.	.	kIx.	.
</s>
<s>
Zcela	zcela	k6eAd1	zcela
vpravo	vpravo	k6eAd1	vpravo
je	být	k5eAaImIp3nS	být
Ostruha	ostruha	k1gFnSc1	ostruha
Brenvy	Brenva	k1gFnSc2	Brenva
<g/>
,	,	kIx,	,
směrem	směr	k1gInSc7	směr
doleva	doleva	k6eAd1	doleva
ledovcová	ledovcový	k2eAgFnSc1d1	ledovcová
klasika	klasika	k1gFnSc1	klasika
Sentinelle	Sentinelle	k1gFnSc1	Sentinelle
Rouge	rouge	k1gFnSc1	rouge
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
Major	major	k1gMnSc1	major
a	a	k8xC	a
zcela	zcela	k6eAd1	zcela
vlevo	vlevo	k6eAd1	vlevo
Hruška	hruška	k1gFnSc1	hruška
-	-	kIx~	-
Poire	Poir	k1gInSc5	Poir
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
východní	východní	k2eAgFnSc4d1	východní
stěnu	stěna	k1gFnSc4	stěna
dále	daleko	k6eAd2	daleko
vlevo	vlevo	k6eAd1	vlevo
navazuje	navazovat	k5eAaImIp3nS	navazovat
Grand	grand	k1gMnSc1	grand
Pilier	Pilier	k1gMnSc1	Pilier
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Angle	Angl	k1gMnSc5	Angl
s	s	k7c7	s
řadou	řada	k1gFnSc7	řada
těžkých	těžký	k2eAgInPc2d1	těžký
výstupů	výstup	k1gInPc2	výstup
<g/>
,	,	kIx,	,
vyúsťujících	vyúsťující	k2eAgInPc2d1	vyúsťující
na	na	k7c4	na
předvrchol	předvrchol	k1gInSc4	předvrchol
Mont	Mont	k2eAgMnSc1d1	Mont
Blanc	Blanc	k1gMnSc1	Blanc
de	de	k?	de
Courmayeur	Courmayeur	k1gMnSc1	Courmayeur
<g/>
,	,	kIx,	,
do	do	k7c2	do
jehož	jehož	k3xOyRp3gFnPc2	jehož
stěn	stěna	k1gFnPc2	stěna
se	se	k3xPyFc4	se
zapsali	zapsat	k5eAaPmAgMnP	zapsat
i	i	k9	i
čeští	český	k2eAgMnPc1d1	český
a	a	k8xC	a
českoslovenští	československý	k2eAgMnPc1d1	československý
horolezci	horolezec	k1gMnPc1	horolezec
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
těžké	těžký	k2eAgInPc1d1	těžký
výstupy	výstup	k1gInPc1	výstup
vedou	vést	k5eAaImIp3nP	vést
pilíři	pilíř	k1gInSc3	pilíř
stěny	stěna	k1gFnSc2	stěna
Frê	Frê	k1gFnSc2	Frê
nad	nad	k7c7	nad
stejnojmenným	stejnojmenný	k2eAgInSc7d1	stejnojmenný
ledovcem	ledovec	k1gInSc7	ledovec
a	a	k8xC	a
pilíři	pilíř	k1gInSc6	pilíř
stěny	stěna	k1gFnSc2	stěna
Broulliard	Broulliarda	k1gFnPc2	Broulliarda
nad	nad	k7c7	nad
ledovcem	ledovec	k1gInSc7	ledovec
Brouillard	Brouillarda	k1gFnPc2	Brouillarda
<g/>
.	.	kIx.	.
</s>
<s>
Výstupy	výstup	k1gInPc1	výstup
těmito	tento	k3xDgFnPc7	tento
dvěma	dva	k4xCgFnPc7	dva
stěnami	stěna	k1gFnPc7	stěna
jsou	být	k5eAaImIp3nP	být
náročné	náročný	k2eAgInPc1d1	náročný
svou	svůj	k3xOyFgFnSc7	svůj
délkou	délka	k1gFnSc7	délka
a	a	k8xC	a
odlehlostí	odlehlost	k1gFnSc7	odlehlost
a	a	k8xC	a
také	také	k9	také
obtížným	obtížný	k2eAgInSc7d1	obtížný
návratem	návrat	k1gInSc7	návrat
při	při	k7c6	při
zhoršení	zhoršení	k1gNnSc6	zhoršení
počasí	počasí	k1gNnSc2	počasí
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
už	už	k6eAd1	už
jen	jen	k9	jen
přístup	přístup	k1gInSc4	přístup
ke	k	k7c3	k
stěnám	stěna	k1gFnPc3	stěna
Frê	Frê	k1gFnPc2	Frê
a	a	k8xC	a
Brouillard	Brouillard	k1gInSc1	Brouillard
by	by	kYmCp3nS	by
vydal	vydat	k5eAaPmAgInS	vydat
na	na	k7c4	na
ne	ne	k9	ne
zrovna	zrovna	k6eAd1	zrovna
lehké	lehký	k2eAgFnPc1d1	lehká
horolezecké	horolezecký	k2eAgFnPc1d1	horolezecká
túry	túra	k1gFnPc1	túra
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pokusu	pokus	k1gInSc6	pokus
o	o	k7c4	o
prvovýstup	prvovýstup	k1gInSc4	prvovýstup
Centrálním	centrální	k2eAgInSc7d1	centrální
pilířem	pilíř	k1gInSc7	pilíř
Frê	Frê	k1gFnSc2	Frê
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
náhodou	náhodou	k6eAd1	náhodou
sešli	sejít	k5eAaPmAgMnP	sejít
ve	v	k7c4	v
stejnou	stejný	k2eAgFnSc4d1	stejná
chvíli	chvíle	k1gFnSc4	chvíle
Italové	Ital	k1gMnPc1	Ital
Bonatti	Bonatti	k1gNnSc2	Bonatti
<g/>
,	,	kIx,	,
Galieni	Galien	k2eAgMnPc1d1	Galien
a	a	k8xC	a
Oggioni	Oggion	k1gMnPc1	Oggion
s	s	k7c7	s
francouzským	francouzský	k2eAgNnSc7d1	francouzské
družstvem	družstvo	k1gNnSc7	družstvo
Mazeaud	Mazeauda	k1gFnPc2	Mazeauda
<g/>
,	,	kIx,	,
Kohlmann	Kohlmanna	k1gFnPc2	Kohlmanna
<g/>
,	,	kIx,	,
Vieille	Vieille	k1gFnSc2	Vieille
a	a	k8xC	a
Guillaume	Guillaum	k1gInSc5	Guillaum
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
známé	známý	k2eAgFnSc3d1	známá
tragédii	tragédie	k1gFnSc3	tragédie
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
při	při	k7c6	při
několikadenní	několikadenní	k2eAgFnSc6d1	několikadenní
bouřce	bouřka	k1gFnSc6	bouřka
zahynuli	zahynout	k5eAaPmAgMnP	zahynout
čtyři	čtyři	k4xCgMnPc4	čtyři
špičkoví	špičkový	k2eAgMnPc1d1	špičkový
horolezci	horolezec	k1gMnPc1	horolezec
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
