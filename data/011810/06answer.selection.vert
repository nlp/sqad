<s>
Drak	drak	k1gInSc1	drak
je	být	k5eAaImIp3nS	být
mytické	mytický	k2eAgNnSc4d1	mytické
zvíře	zvíře	k1gNnSc4	zvíře
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
s	s	k7c7	s
hadími	hadí	k2eAgInPc7d1	hadí
nebo	nebo	k8xC	nebo
ještěřími	ještěří	k2eAgInPc7d1	ještěří
znaky	znak	k1gInPc7	znak
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
mýtech	mýtus	k1gInPc6	mýtus
<g/>
,	,	kIx,	,
pověstech	pověst	k1gFnPc6	pověst
<g/>
,	,	kIx,	,
pohádkách	pohádka	k1gFnPc6	pohádka
a	a	k8xC	a
lidových	lidový	k2eAgInPc6d1	lidový
příbězích	příběh	k1gInPc6	příběh
celé	celý	k2eAgFnSc2d1	celá
řady	řada	k1gFnSc2	řada
kultur	kultura	k1gFnPc2	kultura
<g/>
.	.	kIx.	.
</s>
