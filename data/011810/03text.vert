<p>
<s>
Drak	drak	k1gInSc1	drak
je	být	k5eAaImIp3nS	být
mytické	mytický	k2eAgNnSc4d1	mytické
zvíře	zvíře	k1gNnSc4	zvíře
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
s	s	k7c7	s
hadími	hadí	k2eAgInPc7d1	hadí
nebo	nebo	k8xC	nebo
ještěřími	ještěří	k2eAgInPc7d1	ještěří
znaky	znak	k1gInPc7	znak
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
mýtech	mýtus	k1gInPc6	mýtus
<g/>
,	,	kIx,	,
pověstech	pověst	k1gFnPc6	pověst
<g/>
,	,	kIx,	,
pohádkách	pohádka	k1gFnPc6	pohádka
a	a	k8xC	a
lidových	lidový	k2eAgInPc6d1	lidový
příbězích	příběh	k1gInPc6	příběh
celé	celý	k2eAgFnSc2d1	celá
řady	řada	k1gFnSc2	řada
kultur	kultura	k1gFnPc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
celosvětová	celosvětový	k2eAgFnSc1d1	celosvětová
rozšířenost	rozšířenost	k1gFnSc1	rozšířenost
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
značnou	značný	k2eAgFnSc4d1	značná
variabilitu	variabilita	k1gFnSc4	variabilita
jeho	on	k3xPp3gInSc2	on
vzhledu	vzhled	k1gInSc2	vzhled
<g/>
,	,	kIx,	,
velikosti	velikost	k1gFnSc2	velikost
a	a	k8xC	a
vlastností	vlastnost	k1gFnPc2	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Draci	drak	k1gMnPc1	drak
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
počtem	počet	k1gInSc7	počet
hlav	hlava	k1gFnPc2	hlava
–	–	k?	–
například	například	k6eAd1	například
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
je	být	k5eAaImIp3nS	být
zobrazován	zobrazován	k2eAgInSc1d1	zobrazován
zásadně	zásadně	k6eAd1	zásadně
jednohlavý	jednohlavý	k2eAgInSc1d1	jednohlavý
<g/>
,	,	kIx,	,
řecká	řecký	k2eAgFnSc1d1	řecká
mytologie	mytologie	k1gFnSc1	mytologie
znala	znát	k5eAaImAgFnS	znát
draka	drak	k1gMnSc4	drak
stohlavého	stohlavý	k2eAgMnSc4d1	stohlavý
<g/>
,	,	kIx,	,
v	v	k7c6	v
pohádkách	pohádka	k1gFnPc6	pohádka
mívá	mívat	k5eAaImIp3nS	mívat
zpravidla	zpravidla	k6eAd1	zpravidla
lichý	lichý	k2eAgInSc1d1	lichý
počet	počet	k1gInSc1	počet
hlav	hlava	k1gFnPc2	hlava
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
tři	tři	k4xCgNnPc1	tři
nebo	nebo	k8xC	nebo
sedm	sedm	k4xCc1	sedm
–	–	k?	–
i	i	k9	i
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
nohou	noha	k1gFnPc2	noha
(	(	kIx(	(
<g/>
nejčastěji	často	k6eAd3	často
dvě	dva	k4xCgFnPc4	dva
<g/>
,	,	kIx,	,
čtyři	čtyři	k4xCgFnPc4	čtyři
nebo	nebo	k8xC	nebo
žádné	žádný	k3yNgFnPc1	žádný
<g/>
)	)	kIx)	)
a	a	k8xC	a
také	také	k9	také
ve	v	k7c6	v
schopnosti	schopnost	k1gFnSc6	schopnost
létat	létat	k5eAaImF	létat
a	a	k8xC	a
dštít	dštít	k5eAaImF	dštít
oheň	oheň	k1gInSc4	oheň
(	(	kIx(	(
<g/>
případně	případně	k6eAd1	případně
mráz	mráz	k1gInSc1	mráz
nebo	nebo	k8xC	nebo
jed	jed	k1gInSc1	jed
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
jejich	jejich	k3xOp3gInSc4	jejich
vzhled	vzhled	k1gInSc4	vzhled
<g/>
,	,	kIx,	,
liší	lišit	k5eAaImIp3nS	lišit
se	se	k3xPyFc4	se
i	i	k9	i
jejich	jejich	k3xOp3gFnPc1	jejich
vlastnosti	vlastnost	k1gFnPc1	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Draci	drak	k1gMnPc1	drak
se	se	k3xPyFc4	se
v	v	k7c6	v
základním	základní	k2eAgNnSc6d1	základní
pojetí	pojetí	k1gNnSc6	pojetí
mohou	moct	k5eAaImIp3nP	moct
rozlišovat	rozlišovat	k5eAaImF	rozlišovat
na	na	k7c4	na
dobré	dobrý	k2eAgFnPc4d1	dobrá
a	a	k8xC	a
zlé	zlá	k1gFnPc4	zlá
<g/>
.	.	kIx.	.
</s>
<s>
Tradice	tradice	k1gFnSc1	tradice
prospěšných	prospěšný	k2eAgInPc2d1	prospěšný
draků	drak	k1gInPc2	drak
je	být	k5eAaImIp3nS	být
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
drak	drak	k1gInSc1	drak
symbolem	symbol	k1gInSc7	symbol
moci	moc	k1gFnSc2	moc
a	a	k8xC	a
deště	dešť	k1gInSc2	dešť
<g/>
.	.	kIx.	.
</s>
<s>
Evropští	evropský	k2eAgMnPc1d1	evropský
draci	drak	k1gMnPc1	drak
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
jsou	být	k5eAaImIp3nP	být
namísto	namísto	k7c2	namísto
toho	ten	k3xDgInSc2	ten
chápáni	chápat	k5eAaImNgMnP	chápat
jako	jako	k8xC	jako
symbol	symbol	k1gInSc1	symbol
ničení	ničení	k1gNnSc2	ničení
a	a	k8xC	a
chaosu	chaos	k1gInSc2	chaos
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
jsou	být	k5eAaImIp3nP	být
zobrazováni	zobrazován	k2eAgMnPc1d1	zobrazován
jako	jako	k8xS	jako
krvelačné	krvelačný	k2eAgFnPc1d1	krvelačná
bestie	bestie	k1gFnPc1	bestie
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
ničí	ničit	k5eAaImIp3nP	ničit
vše	všechen	k3xTgNnSc4	všechen
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
a	a	k8xC	a
pojídají	pojídat	k5eAaImIp3nP	pojídat
zvířata	zvíře	k1gNnPc4	zvíře
a	a	k8xC	a
lidi	člověk	k1gMnPc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Draci	drak	k1gMnPc1	drak
jsou	být	k5eAaImIp3nP	být
spojováni	spojován	k2eAgMnPc1d1	spojován
s	s	k7c7	s
bohatstvím	bohatství	k1gNnSc7	bohatství
a	a	k8xC	a
majetkem	majetek	k1gInSc7	majetek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
zpravidla	zpravidla	k6eAd1	zpravidla
ochraňují	ochraňovat	k5eAaImIp3nP	ochraňovat
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
slujích	sluj	k1gFnPc6	sluj
<g/>
.	.	kIx.	.
</s>
<s>
Představa	představa	k1gFnSc1	představa
draka	drak	k1gMnSc2	drak
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
stará	starý	k2eAgFnSc1d1	stará
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
odrážet	odrážet	k5eAaImF	odrážet
zkameněliny	zkamenělina	k1gFnPc4	zkamenělina
dinosaurů	dinosaurus	k1gMnPc2	dinosaurus
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
doklady	doklad	k1gInPc4	doklad
z	z	k7c2	z
antického	antický	k2eAgNnSc2d1	antické
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
drak	drak	k1gMnSc1	drak
hlídal	hlídat	k5eAaImAgMnS	hlídat
bájné	bájný	k2eAgNnSc4d1	bájné
zlaté	zlatý	k2eAgNnSc4d1	Zlaté
rouno	rouno	k1gNnSc4	rouno
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
i	i	k9	i
v	v	k7c6	v
akkadském	akkadský	k2eAgInSc6d1	akkadský
eposu	epos	k1gInSc6	epos
Enúma	Enúma	k1gFnSc1	Enúma
eliš	eliš	k1gInSc1	eliš
napsaného	napsaný	k2eAgInSc2d1	napsaný
mezi	mezi	k7c7	mezi
18	[number]	k4	18
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Zmiňují	zmiňovat	k5eAaImIp3nP	zmiňovat
je	on	k3xPp3gInPc4	on
i	i	k8xC	i
příběhy	příběh	k1gInPc4	příběh
ze	z	k7c2	z
starodávného	starodávný	k2eAgInSc2d1	starodávný
Egypta	Egypt	k1gInSc2	Egypt
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
setkání	setkání	k1gNnSc2	setkání
boha	bůh	k1gMnSc4	bůh
Ra	ra	k0	ra
s	s	k7c7	s
hadím	hadí	k2eAgMnSc7d1	hadí
drakem	drak	k1gMnSc7	drak
Apopem	Apop	k1gMnSc7	Apop
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
v	v	k7c6	v
germánské	germánský	k2eAgFnSc6d1	germánská
mytologii	mytologie	k1gFnSc6	mytologie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
s	s	k7c7	s
drakem	drak	k1gMnSc7	drak
utká	utkat	k5eAaPmIp3nS	utkat
Beowulf	Beowulf	k1gMnSc1	Beowulf
či	či	k8xC	či
Sigurd	Sigurd	k1gMnSc1	Sigurd
(	(	kIx(	(
<g/>
Siegfried	Siegfried	k1gMnSc1	Siegfried
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hrdinovi	Hrdina	k1gMnSc3	Hrdina
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
utkal	utkat	k5eAaPmAgMnS	utkat
s	s	k7c7	s
drakem	drak	k1gInSc7	drak
se	se	k3xPyFc4	se
obecně	obecně	k6eAd1	obecně
říká	říkat	k5eAaImIp3nS	říkat
drakobijce	drakobijce	k1gMnSc1	drakobijce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
kromě	kromě	k7c2	kromě
výrazu	výraz	k1gInSc2	výraz
drak	drak	k1gInSc1	drak
existuje	existovat	k5eAaImIp3nS	existovat
také	také	k9	také
označení	označení	k1gNnSc4	označení
saň	saň	k1gFnSc4	saň
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
heraldického	heraldický	k2eAgNnSc2d1	heraldické
hlediska	hledisko	k1gNnSc2	hledisko
je	být	k5eAaImIp3nS	být
drak	drak	k1gInSc1	drak
dvounohý	dvounohý	k2eAgMnSc1d1	dvounohý
tvor	tvor	k1gMnSc1	tvor
<g/>
,	,	kIx,	,
chrlící	chrlící	k2eAgInSc1d1	chrlící
oheň	oheň	k1gInSc1	oheň
<g/>
,	,	kIx,	,
a	a	k8xC	a
saň	saň	k1gFnSc4	saň
je	být	k5eAaImIp3nS	být
tvor	tvor	k1gMnSc1	tvor
čtyřnohý	čtyřnohý	k2eAgMnSc1d1	čtyřnohý
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
oheň	oheň	k1gInSc1	oheň
obvykle	obvykle	k6eAd1	obvykle
nechrlí	chrlit	k5eNaImIp3nS	chrlit
<g/>
,	,	kIx,	,
v	v	k7c6	v
obecné	obecný	k2eAgFnSc6d1	obecná
mluvě	mluva	k1gFnSc6	mluva
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
oba	dva	k4xCgInPc1	dva
výrazy	výraz	k1gInPc1	výraz
často	často	k6eAd1	často
chápány	chápán	k2eAgInPc1d1	chápán
jako	jako	k9	jako
synonyma	synonymum	k1gNnPc1	synonymum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Drak	drak	k1gMnSc1	drak
v	v	k7c6	v
mytologii	mytologie	k1gFnSc6	mytologie
==	==	k?	==
</s>
</p>
<p>
<s>
Draci	drak	k1gMnPc1	drak
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
od	od	k7c2	od
Japonska	Japonsko	k1gNnSc2	Japonsko
až	až	k9	až
po	po	k7c4	po
západ	západ	k1gInSc4	západ
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
jistým	jistý	k2eAgNnSc7d1	jisté
zjednodušením	zjednodušení	k1gNnSc7	zjednodušení
můžeme	moct	k5eAaImIp1nP	moct
rozlišit	rozlišit	k5eAaPmF	rozlišit
dva	dva	k4xCgInPc4	dva
základní	základní	k2eAgInPc4d1	základní
okruhy	okruh	k1gInPc4	okruh
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejichž	jejichž	k3xOyRp3gInSc6	jejichž
rámci	rámec	k1gInSc6	rámec
se	se	k3xPyFc4	se
představa	představa	k1gFnSc1	představa
o	o	k7c6	o
dracích	drak	k1gInPc6	drak
vyvíjela	vyvíjet	k5eAaImAgFnS	vyvíjet
<g/>
:	:	kIx,	:
dálněvýchodní	dálněvýchodní	k2eAgFnSc1d1	dálněvýchodní
a	a	k8xC	a
západní	západní	k2eAgFnSc1d1	západní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Západní	západní	k2eAgMnPc1d1	západní
draci	drak	k1gMnPc1	drak
===	===	k?	===
</s>
</p>
<p>
<s>
Představa	představa	k1gFnSc1	představa
draka	drak	k1gMnSc2	drak
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
kultuře	kultura	k1gFnSc6	kultura
vychází	vycházet	k5eAaImIp3nS	vycházet
původně	původně	k6eAd1	původně
především	především	k9	především
z	z	k7c2	z
hada	had	k1gMnSc2	had
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
podoba	podoba	k1gFnSc1	podoba
se	se	k3xPyFc4	se
s	s	k7c7	s
místem	místo	k1gNnSc7	místo
a	a	k8xC	a
časem	čas	k1gInSc7	čas
výrazně	výrazně	k6eAd1	výrazně
mění	měnit	k5eAaImIp3nS	měnit
<g/>
:	:	kIx,	:
získává	získávat	k5eAaImIp3nS	získávat
křídla	křídlo	k1gNnPc4	křídlo
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc4	jeden
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
dva	dva	k4xCgInPc4	dva
páry	pár	k1gInPc4	pár
nohou	noha	k1gFnPc2	noha
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
jednu	jeden	k4xCgFnSc4	jeden
<g/>
,	,	kIx,	,
dvě	dva	k4xCgFnPc4	dva
<g/>
,	,	kIx,	,
tři	tři	k4xCgMnPc1	tři
i	i	k9	i
více	hodně	k6eAd2	hodně
hlav	hlava	k1gFnPc2	hlava
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
chrlí	chrlit	k5eAaImIp3nP	chrlit
oheň	oheň	k1gInSc4	oheň
či	či	k8xC	či
má	mít	k5eAaImIp3nS	mít
jedovatý	jedovatý	k2eAgInSc4d1	jedovatý
dech	dech	k1gInSc4	dech
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
středověkých	středověký	k2eAgNnPc6d1	středověké
vyobrazeních	vyobrazení	k1gNnPc6	vyobrazení
převládá	převládat	k5eAaImIp3nS	převládat
drak	drak	k1gInSc1	drak
s	s	k7c7	s
hadím	hadí	k2eAgNnSc7d1	hadí
tělem	tělo	k1gNnSc7	tělo
a	a	k8xC	a
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
ocasem	ocas	k1gInSc7	ocas
<g/>
,	,	kIx,	,
ušatou	ušatý	k2eAgFnSc7d1	ušatá
hlavou	hlava	k1gFnSc7	hlava
připomínající	připomínající	k2eAgMnSc1d1	připomínající
psa	pes	k1gMnSc4	pes
<g/>
,	,	kIx,	,
jedním	jeden	k4xCgInSc7	jeden
párem	pár	k1gInSc7	pár
lvích	lví	k2eAgFnPc2d1	lví
nohou	noha	k1gFnPc2	noha
a	a	k8xC	a
ptačími	ptačí	k2eAgNnPc7d1	ptačí
křídly	křídlo	k1gNnPc7	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
přibližně	přibližně	k6eAd1	přibližně
od	od	k7c2	od
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
začíná	začínat	k5eAaImIp3nS	začínat
převládat	převládat	k5eAaImF	převládat
vyobrazováni	vyobrazován	k2eAgMnPc1d1	vyobrazován
s	s	k7c7	s
párem	pár	k1gInSc7	pár
blanitých	blanitý	k2eAgNnPc2d1	blanité
křídel	křídlo	k1gNnPc2	křídlo
<g/>
,	,	kIx,	,
připomínajících	připomínající	k2eAgFnPc2d1	připomínající
netopýra	netopýr	k1gMnSc4	netopýr
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
novověku	novověk	k1gInSc2	novověk
pak	pak	k6eAd1	pak
draci	drak	k1gMnPc1	drak
postupně	postupně	k6eAd1	postupně
získávají	získávat	k5eAaImIp3nP	získávat
stále	stále	k6eAd1	stále
častěji	často	k6eAd2	často
ještěří	ještěří	k2eAgInPc4d1	ještěří
atributy	atribut	k1gInPc4	atribut
<g/>
:	:	kIx,	:
dva	dva	k4xCgInPc1	dva
páry	pár	k1gInPc1	pár
nohou	noha	k1gFnSc7	noha
<g/>
,	,	kIx,	,
šupinatou	šupinatý	k2eAgFnSc4d1	šupinatá
kůži	kůže	k1gFnSc4	kůže
a	a	k8xC	a
rohy	roh	k1gInPc4	roh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Západní	západní	k2eAgMnPc1d1	západní
draci	drak	k1gMnPc1	drak
často	často	k6eAd1	často
bývají	bývat	k5eAaImIp3nP	bývat
spojování	spojování	k1gNnPc4	spojování
se	s	k7c7	s
silami	síla	k1gFnPc7	síla
zla	zlo	k1gNnSc2	zlo
a	a	k8xC	a
chaosu	chaos	k1gInSc2	chaos
<g/>
,	,	kIx,	,
výjimku	výjimka	k1gFnSc4	výjimka
tvoří	tvořit	k5eAaImIp3nP	tvořit
například	například	k6eAd1	například
velšské	velšský	k2eAgFnPc1d1	velšská
pověsti	pověst	k1gFnPc1	pověst
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Přední	přední	k2eAgInSc4d1	přední
východ	východ	k1gInSc4	východ
====	====	k?	====
</s>
</p>
<p>
<s>
Drak	drak	k1gInSc1	drak
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
již	již	k6eAd1	již
u	u	k7c2	u
Sumerů	Sumer	k1gInPc2	Sumer
a	a	k8xC	a
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
navazujících	navazující	k2eAgFnPc2d1	navazující
kultur	kultura	k1gFnPc2	kultura
<g/>
,	,	kIx,	,
např.	např.	kA	např.
nejvyššímu	vysoký	k2eAgMnSc3d3	nejvyšší
bohovi	bůh	k1gMnSc3	bůh
akkadské	akkadský	k2eAgFnSc2d1	akkadská
říše	říš	k1gFnSc2	říš
byl	být	k5eAaImAgInS	být
drak	drak	k1gInSc1	drak
přímo	přímo	k6eAd1	přímo
zasvěcen	zasvětit	k5eAaPmNgInS	zasvětit
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgMnSc1	tento
drak	drak	k1gMnSc1	drak
byl	být	k5eAaImAgMnS	být
jakýmsi	jakýsi	k3yIgInSc7	jakýsi
prototypem	prototyp	k1gInSc7	prototyp
tehdejších	tehdejší	k2eAgFnPc2d1	tehdejší
představ	představa	k1gFnPc2	představa
draka	drak	k1gMnSc2	drak
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
vlastně	vlastně	k9	vlastně
křížencem	kříženec	k1gMnSc7	kříženec
hada	had	k1gMnSc2	had
a	a	k8xC	a
ptáka	pták	k1gMnSc2	pták
Noha	noh	k1gMnSc2	noh
<g/>
.	.	kIx.	.
</s>
<s>
Tehdejší	tehdejší	k2eAgFnSc1d1	tehdejší
podoba	podoba	k1gFnSc1	podoba
draka	drak	k1gMnSc2	drak
je	být	k5eAaImIp3nS	být
dobře	dobře	k6eAd1	dobře
známa	znám	k2eAgFnSc1d1	známa
z	z	k7c2	z
archeologických	archeologický	k2eAgFnPc2d1	archeologická
vykopávek	vykopávka	k1gFnPc2	vykopávka
<g/>
:	:	kIx,	:
drak	drak	k1gInSc1	drak
má	mít	k5eAaImIp3nS	mít
hadí	hadí	k2eAgFnSc4d1	hadí
hlavu	hlava	k1gFnSc4	hlava
s	s	k7c7	s
velkýma	velký	k2eAgNnPc7d1	velké
očima	oko	k1gNnPc7	oko
<g/>
,	,	kIx,	,
rozeklaným	rozeklaný	k2eAgInSc7d1	rozeklaný
<g/>
,	,	kIx,	,
hadím	hadí	k2eAgInSc7d1	hadí
jazykem	jazyk	k1gInSc7	jazyk
a	a	k8xC	a
s	s	k7c7	s
rohem	roh	k1gInSc7	roh
na	na	k7c6	na
lebce	lebka	k1gFnSc6	lebka
<g/>
.	.	kIx.	.
</s>
<s>
Krk	krk	k1gInSc1	krk
má	mít	k5eAaImIp3nS	mít
velmi	velmi	k6eAd1	velmi
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
tělo	tělo	k1gNnSc4	tělo
je	být	k5eAaImIp3nS	být
šupinaté	šupinatý	k2eAgNnSc1d1	šupinaté
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
čtyři	čtyři	k4xCgFnPc4	čtyři
nohy	noha	k1gFnPc4	noha
<g/>
.	.	kIx.	.
</s>
<s>
Zadní	zadní	k2eAgFnPc1d1	zadní
tlapy	tlapa	k1gFnPc1	tlapa
mají	mít	k5eAaImIp3nP	mít
ptačí	ptačí	k2eAgInPc4d1	ptačí
drápy	dráp	k1gInPc4	dráp
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
badateli	badatel	k1gMnPc7	badatel
nepanuje	panovat	k5eNaImIp3nS	panovat
úplná	úplný	k2eAgFnSc1d1	úplná
jednota	jednota	k1gFnSc1	jednota
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
funkci	funkce	k1gFnSc6	funkce
<g/>
,	,	kIx,	,
schopnostech	schopnost	k1gFnPc6	schopnost
ani	ani	k8xC	ani
vlastnostech	vlastnost	k1gFnPc6	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
známým	známý	k2eAgMnSc7d1	známý
drakem	drak	k1gMnSc7	drak
sumersko-akkadské	sumerskokkadský	k2eAgFnSc2d1	sumersko-akkadský
mytologie	mytologie	k1gFnSc2	mytologie
je	být	k5eAaImIp3nS	být
Mušmachchu	Mušmachcha	k1gFnSc4	Mušmachcha
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byl	být	k5eAaImAgMnS	být
sedmihlavý	sedmihlavý	k2eAgInSc4d1	sedmihlavý
drak	drak	k1gInSc4	drak
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
had	had	k1gMnSc1	had
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poněkud	poněkud	k6eAd1	poněkud
atypickým	atypický	k2eAgInSc7d1	atypický
drakem	drak	k1gInSc7	drak
byla	být	k5eAaImAgFnS	být
Tiamat	Tiamat	k1gInSc4	Tiamat
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
spíše	spíše	k9	spíše
brala	brát	k5eAaImAgFnS	brát
podobu	podoba	k1gFnSc4	podoba
draka	drak	k1gMnSc2	drak
než	než	k8xS	než
by	by	kYmCp3nS	by
jím	on	k3xPp3gInSc7	on
sama	sám	k3xTgFnSc1	sám
byla	být	k5eAaImAgNnP	být
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
S	s	k7c7	s
chetitskými	chetitský	k2eAgMnPc7d1	chetitský
draky	drak	k1gMnPc7	drak
je	být	k5eAaImIp3nS	být
spojena	spojit	k5eAaPmNgFnS	spojit
jedna	jeden	k4xCgFnSc1	jeden
zajímavost	zajímavost	k1gFnSc1	zajímavost
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
drak	drak	k1gInSc4	drak
Illujanka	Illujanka	k1gFnSc1	Illujanka
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byl	být	k5eAaImAgMnS	být
had	had	k1gMnSc1	had
–	–	k?	–
drak	drak	k1gInSc1	drak
<g/>
,	,	kIx,	,
spojený	spojený	k2eAgInSc1d1	spojený
s	s	k7c7	s
mýtem	mýtus	k1gInSc7	mýtus
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
Inana	Inana	k1gFnSc1	Inana
draka	drak	k1gMnSc2	drak
opije	opít	k5eAaPmIp3nS	opít
a	a	k8xC	a
spoutá	spoutat	k5eAaPmIp3nS	spoutat
(	(	kIx(	(
<g/>
Mýtus	mýtus	k1gInSc1	mýtus
o	o	k7c6	o
Illujankovi	Illujanek	k1gMnSc6	Illujanek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavá	zajímavý	k2eAgFnSc1d1	zajímavá
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
je	být	k5eAaImIp3nS	být
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
drak	drak	k1gInSc1	drak
Illujanka	Illujanka	k1gFnSc1	Illujanka
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
první	první	k4xOgInSc4	první
drak	drak	k1gInSc4	drak
žijící	žijící	k2eAgMnPc4d1	žijící
v	v	k7c6	v
jeskyni	jeskyně	k1gFnSc6	jeskyně
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
nenasytný	nasytný	k2eNgInSc1d1	nenasytný
byl	být	k5eAaImAgInS	být
chetitský	chetitský	k2eAgInSc1d1	chetitský
mořský	mořský	k2eAgInSc1d1	mořský
drak	drak	k1gInSc1	drak
Chedammu	Chedamm	k1gInSc2	Chedamm
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
chetitsky	chetitsky	k6eAd1	chetitsky
psaného	psaný	k2eAgInSc2d1	psaný
mýtu	mýtus	k1gInSc2	mýtus
o	o	k7c4	o
Kumarbim	Kumarbim	k1gInSc4	Kumarbim
(	(	kIx(	(
<g/>
churritský	churritský	k2eAgMnSc1d1	churritský
otec	otec	k1gMnSc1	otec
bohů	bůh	k1gMnPc2	bůh
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rodiče	rodič	k1gMnPc1	rodič
tohoto	tento	k3xDgMnSc4	tento
draka	drak	k1gMnSc4	drak
jsou	být	k5eAaImIp3nP	být
Kumarbi	Kumarb	k1gFnPc1	Kumarb
a	a	k8xC	a
dcera	dcera	k1gFnSc1	dcera
Velkého	velký	k2eAgNnSc2d1	velké
moře	moře	k1gNnSc2	moře
Šertapšuchuri	Šertapšuchur	k1gFnSc2	Šertapšuchur
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
hlavní	hlavní	k2eAgFnSc7d1	hlavní
zbraní	zbraň	k1gFnSc7	zbraň
byla	být	k5eAaImAgFnS	být
žravost	žravost	k1gFnSc1	žravost
<g/>
.	.	kIx.	.
</s>
<s>
Ištar	Ištar	k1gMnSc1	Ištar
se	se	k3xPyFc4	se
jej	on	k3xPp3gMnSc4	on
na	na	k7c4	na
prosbu	prosba	k1gFnSc4	prosba
svého	svůj	k3xOyFgMnSc2	svůj
bratra	bratr	k1gMnSc2	bratr
(	(	kIx(	(
<g/>
Bůh	bůh	k1gMnSc1	bůh
bouře	bouř	k1gFnSc2	bouř
<g/>
)	)	kIx)	)
snažila	snažit	k5eAaImAgFnS	snažit
zneškodnit	zneškodnit	k5eAaPmF	zneškodnit
<g/>
,	,	kIx,	,
nejprve	nejprve	k6eAd1	nejprve
použila	použít	k5eAaPmAgFnS	použít
svoje	svůj	k3xOyFgFnPc4	svůj
ženské	ženský	k2eAgFnPc4d1	ženská
vnady	vnada	k1gFnPc4	vnada
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c4	na
draka	drak	k1gMnSc4	drak
nezapůsobila	zapůsobit	k5eNaPmAgFnS	zapůsobit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jeho	jeho	k3xOp3gFnSc1	jeho
jediná	jediný	k2eAgFnSc1d1	jediná
touha	touha	k1gFnSc1	touha
bylo	být	k5eAaImAgNnS	být
jídlo	jídlo	k1gNnSc1	jídlo
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
ho	on	k3xPp3gNnSc4	on
omámila	omámit	k5eAaPmAgFnS	omámit
omamnými	omamný	k2eAgFnPc7d1	omamná
látkami	látka	k1gFnPc7	látka
a	a	k8xC	a
vylákala	vylákat	k5eAaPmAgFnS	vylákat
ho	on	k3xPp3gMnSc4	on
na	na	k7c4	na
břeh	břeh	k1gInSc4	břeh
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ho	on	k3xPp3gNnSc4	on
Bůh	bůh	k1gMnSc1	bůh
bouře	bouř	k1gFnSc2	bouř
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
zabil	zabít	k5eAaPmAgInS	zabít
(	(	kIx(	(
<g/>
tato	tento	k3xDgFnSc1	tento
část	část	k1gFnSc1	část
díla	dílo	k1gNnSc2	dílo
se	se	k3xPyFc4	se
nezachovala	zachovat	k5eNaPmAgFnS	zachovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Egypt	Egypt	k1gInSc1	Egypt
====	====	k?	====
</s>
</p>
<p>
<s>
Egyptské	egyptský	k2eAgNnSc1d1	egyptské
náboženství	náboženství	k1gNnSc1	náboženství
přes	přes	k7c4	přes
obrovské	obrovský	k2eAgNnSc4d1	obrovské
množství	množství	k1gNnSc4	množství
podivných	podivný	k2eAgFnPc2d1	podivná
bytostí	bytost	k1gFnPc2	bytost
a	a	k8xC	a
monster	monstrum	k1gNnPc2	monstrum
nezná	neznat	k5eAaImIp3nS	neznat
klasického	klasický	k2eAgMnSc4d1	klasický
draka	drak	k1gMnSc4	drak
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
egyptské	egyptský	k2eAgFnSc2d1	egyptská
mytologie	mytologie	k1gFnSc2	mytologie
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
doby	doba	k1gFnSc2	doba
zasazeno	zasadit	k5eAaPmNgNnS	zasadit
několik	několik	k4yIc1	několik
draků	drak	k1gInPc2	drak
víceméně	víceméně	k9	víceméně
lokálního	lokální	k2eAgInSc2d1	lokální
významu	význam	k1gInSc2	význam
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
zase	zase	k9	zase
odešli	odejít	k5eAaPmAgMnP	odejít
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
významněji	významně	k6eAd2	významně
zasáhli	zasáhnout	k5eAaPmAgMnP	zasáhnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přesto	přesto	k8xC	přesto
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
mytologii	mytologie	k1gFnSc6	mytologie
dvě	dva	k4xCgFnPc4	dva
velmi	velmi	k6eAd1	velmi
významné	významný	k2eAgFnPc1d1	významná
bytosti	bytost	k1gFnPc1	bytost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
někdy	někdy	k6eAd1	někdy
označováni	označován	k2eAgMnPc1d1	označován
jako	jako	k9	jako
draci	drak	k1gMnPc1	drak
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
Amemait	Amemait	k1gInSc4	Amemait
<g/>
,	,	kIx,	,
velká	velký	k2eAgFnSc1d1	velká
požíračka	požíračka	k1gFnSc1	požíračka
<g/>
,	,	kIx,	,
nestvůra	nestvůra	k1gFnSc1	nestvůra
se	s	k7c7	s
lvím	lví	k2eAgNnSc7d1	lví
a	a	k8xC	a
hroším	hroší	k2eAgNnSc7d1	hroší
tělem	tělo	k1gNnSc7	tělo
a	a	k8xC	a
krokodýlí	krokodýlí	k2eAgFnSc7d1	krokodýlí
hlavou	hlava	k1gFnSc7	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Zúčastňovala	zúčastňovat	k5eAaImAgFnS	zúčastňovat
se	s	k7c7	s
"	"	kIx"	"
<g/>
vážení	vážení	k1gNnSc1	vážení
srdce	srdce	k1gNnSc2	srdce
<g/>
"	"	kIx"	"
u	u	k7c2	u
posledního	poslední	k2eAgInSc2d1	poslední
soudu	soud	k1gInSc2	soud
<g/>
,	,	kIx,	,
jestliže	jestliže	k8xS	jestliže
bylo	být	k5eAaImAgNnS	být
srdce	srdce	k1gNnSc1	srdce
těžší	těžký	k2eAgNnSc1d2	těžší
než	než	k8xS	než
pravda	pravda	k1gFnSc1	pravda
<g/>
,	,	kIx,	,
sežrala	sežrat	k5eAaPmAgFnS	sežrat
ho	on	k3xPp3gMnSc4	on
a	a	k8xC	a
duše	duše	k1gFnSc1	duše
přestala	přestat	k5eAaPmAgFnS	přestat
existovat	existovat	k5eAaImF	existovat
navěky	navěky	k6eAd1	navěky
<g/>
.	.	kIx.	.
</s>
<s>
Amemait	Amemait	k1gInSc1	Amemait
bývá	bývat	k5eAaImIp3nS	bývat
v	v	k7c6	v
některé	některý	k3yIgFnSc6	některý
literatuře	literatura	k1gFnSc6	literatura
označována	označován	k2eAgFnSc1d1	označována
jako	jako	k8xS	jako
drak	drak	k1gInSc1	drak
<g/>
,	,	kIx,	,
jinde	jinde	k6eAd1	jinde
nikoli	nikoli	k9	nikoli
<g/>
,	,	kIx,	,
každopádně	každopádně	k6eAd1	každopádně
s	s	k7c7	s
draky	drak	k1gMnPc7	drak
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
některé	některý	k3yIgInPc4	některý
shodné	shodný	k2eAgInPc4d1	shodný
znaky	znak	k1gInPc4	znak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Apop	Apop	k1gInSc1	Apop
je	být	k5eAaImIp3nS	být
spíše	spíše	k9	spíše
démon	démon	k1gMnSc1	démon
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
obrovského	obrovský	k2eAgMnSc2d1	obrovský
hada	had	k1gMnSc2	had
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nepřítel	nepřítel	k1gMnSc1	nepřítel
boha	bůh	k1gMnSc4	bůh
Re	re	k9	re
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
ztělesněním	ztělesnění	k1gNnSc7	ztělesnění
nepřátelských	přátelský	k2eNgFnPc2d1	nepřátelská
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
ohrožovaly	ohrožovat	k5eAaImAgFnP	ohrožovat
plavbu	plavba	k1gFnSc4	plavba
sluneční	sluneční	k2eAgFnSc2d1	sluneční
lodi	loď	k1gFnSc2	loď
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
příčinou	příčina	k1gFnSc7	příčina
stmívání	stmívání	k1gNnSc2	stmívání
<g/>
,	,	kIx,	,
přítmí	přítmí	k1gNnSc2	přítmí
<g/>
,	,	kIx,	,
temných	temný	k2eAgInPc2d1	temný
mraků	mrak	k1gInPc2	mrak
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
====	====	k?	====
Řecko	Řecko	k1gNnSc1	Řecko
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
řecké	řecký	k2eAgFnSc6d1	řecká
a	a	k8xC	a
následně	následně	k6eAd1	následně
i	i	k9	i
římské	římský	k2eAgFnSc3d1	římská
mytologii	mytologie	k1gFnSc3	mytologie
je	být	k5eAaImIp3nS	být
situace	situace	k1gFnSc1	situace
kolem	kolem	k7c2	kolem
draků	drak	k1gMnPc2	drak
velmi	velmi	k6eAd1	velmi
komplikovaná	komplikovaný	k2eAgFnSc1d1	komplikovaná
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
jich	on	k3xPp3gMnPc2	on
zde	zde	k6eAd1	zde
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
značné	značný	k2eAgNnSc1d1	značné
množství	množství	k1gNnSc1	množství
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
některé	některý	k3yIgInPc1	některý
příběhy	příběh	k1gInPc1	příběh
mají	mít	k5eAaImIp3nP	mít
více	hodně	k6eAd2	hodně
verzí	verze	k1gFnPc2	verze
a	a	k8xC	a
často	často	k6eAd1	často
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
některé	některý	k3yIgFnSc6	některý
verzi	verze	k1gFnSc6	verze
nějakou	nějaký	k3yIgFnSc4	nějaký
činnost	činnost	k1gFnSc4	činnost
dělá	dělat	k5eAaImIp3nS	dělat
drak	drak	k1gInSc1	drak
a	a	k8xC	a
v	v	k7c4	v
jiné	jiný	k2eAgMnPc4d1	jiný
nikoli	nikoli	k9	nikoli
<g/>
.	.	kIx.	.
</s>
<s>
Draci	drak	k1gMnPc1	drak
zde	zde	k6eAd1	zde
často	často	k6eAd1	často
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
jako	jako	k9	jako
představitelé	představitel	k1gMnPc1	představitel
tmy	tma	k1gFnSc2	tma
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
stávají	stávat	k5eAaImIp3nP	stávat
i	i	k9	i
jejím	její	k3xOp3gInSc7	její
symbolem	symbol	k1gInSc7	symbol
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jindy	jindy	k6eAd1	jindy
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
v	v	k7c6	v
pozici	pozice	k1gFnSc6	pozice
"	"	kIx"	"
<g/>
hlídacího	hlídací	k2eAgMnSc2d1	hlídací
psa	pes	k1gMnSc2	pes
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
i	i	k9	i
pojetí	pojetí	k1gNnSc1	pojetí
jakéhosi	jakýsi	k3yIgInSc2	jakýsi
"	"	kIx"	"
<g/>
posvátného	posvátný	k2eAgMnSc2d1	posvátný
<g/>
"	"	kIx"	"
zvířete	zvíře	k1gNnSc2	zvíře
a	a	k8xC	a
vykonavatele	vykonavatel	k1gMnSc2	vykonavatel
vůle	vůle	k1gFnSc2	vůle
bohů	bůh	k1gMnPc2	bůh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejznámějším	známý	k2eAgInSc7d3	nejznámější
řeckým	řecký	k2eAgInSc7d1	řecký
drakem	drak	k1gInSc7	drak
je	být	k5eAaImIp3nS	být
patrně	patrně	k6eAd1	patrně
drak	drak	k1gInSc1	drak
vystupující	vystupující	k2eAgInSc1d1	vystupující
v	v	k7c6	v
Pověsti	pověst	k1gFnSc6	pověst
o	o	k7c6	o
zlatém	zlatý	k2eAgNnSc6d1	Zlaté
rounu	rouno	k1gNnSc6	rouno
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Argonauté	argonaut	k1gMnPc1	argonaut
přijedou	přijet	k5eAaPmIp3nP	přijet
získat	získat	k5eAaPmF	získat
zlaté	zlatý	k2eAgNnSc4d1	Zlaté
rouno	rouno	k1gNnSc4	rouno
do	do	k7c2	do
Kolchidy	Kolchida	k1gFnSc2	Kolchida
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
nejprve	nejprve	k6eAd1	nejprve
Iásón	Iásón	k1gInSc1	Iásón
dostane	dostat	k5eAaPmIp3nS	dostat
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
osít	osít	k5eAaPmF	osít
pole	pole	k1gNnSc2	pole
dračími	dračí	k2eAgInPc7d1	dračí
zuby	zub	k1gInPc7	zub
(	(	kIx(	(
<g/>
tento	tento	k3xDgInSc4	tento
čin	čin	k1gInSc4	čin
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
základem	základ	k1gInSc7	základ
mnoha	mnoho	k4c2	mnoho
přísloví	přísloví	k1gNnPc2	přísloví
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
setby	setba	k1gFnSc2	setba
vyrostou	vyrůst	k5eAaPmIp3nP	vyrůst
vojáci	voják	k1gMnPc1	voják
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
pobijí	pobít	k5eAaPmIp3nP	pobít
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
Iásón	Iásón	k1gMnSc1	Iásón
splnil	splnit	k5eAaPmAgMnS	splnit
úkol	úkol	k1gInSc4	úkol
zlaté	zlatý	k2eAgNnSc4d1	Zlaté
rouno	rouno	k1gNnSc4	rouno
nedostane	dostat	k5eNaPmIp3nS	dostat
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc2	on
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
získat	získat	k5eAaPmF	získat
lstí	lest	k1gFnSc7	lest
v	v	k7c6	v
čemž	což	k3yRnSc6	což
mu	on	k3xPp3gMnSc3	on
pomůže	pomoct	k5eAaPmIp3nS	pomoct
Médeia	Médeium	k1gNnPc4	Médeium
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
uspí	uspat	k5eAaPmIp3nP	uspat
draka	drak	k1gMnSc4	drak
střežícího	střežící	k2eAgInSc2d1	střežící
toto	tento	k3xDgNnSc4	tento
rouno	rouno	k1gNnSc4	rouno
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
pověsti	pověst	k1gFnSc2	pověst
zasáhnou	zasáhnout	k5eAaPmIp3nP	zasáhnout
draci	drak	k1gMnPc1	drak
ještě	ještě	k6eAd1	ještě
jednou	jeden	k4xCgFnSc7	jeden
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
když	když	k8xS	když
Médeia	Médeia	k1gFnSc1	Médeia
prchá	prchat	k5eAaImIp3nS	prchat
na	na	k7c6	na
voze	vůz	k1gInSc6	vůz
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
tahnou	tahnout	k5eAaImIp3nP	tahnout
dva	dva	k4xCgMnPc1	dva
okřídlení	okřídlený	k2eAgMnPc1d1	okřídlený
draci	drak	k1gMnPc1	drak
boha	bůh	k1gMnSc2	bůh
slunce	slunce	k1gNnSc1	slunce
Helia	helium	k1gNnSc2	helium
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dalším	další	k2eAgMnSc7d1	další
velmi	velmi	k6eAd1	velmi
známým	známý	k2eAgMnSc7d1	známý
drakem	drak	k1gMnSc7	drak
je	být	k5eAaImIp3nS	být
drak	drak	k1gMnSc1	drak
zabitý	zabitý	k1gMnSc1	zabitý
Kadmem	Kadm	k1gMnSc7	Kadm
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
po	po	k7c6	po
objevení	objevení	k1gNnSc6	objevení
vhodného	vhodný	k2eAgNnSc2d1	vhodné
místa	místo	k1gNnSc2	místo
pro	pro	k7c4	pro
založení	založení	k1gNnSc4	založení
Théb	Théby	k1gFnPc2	Théby
narazí	narazit	k5eAaPmIp3nP	narazit
na	na	k7c4	na
draka	drak	k1gMnSc4	drak
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zabije	zabít	k5eAaPmIp3nS	zabít
jeho	jeho	k3xOp3gMnPc4	jeho
společníky	společník	k1gMnPc4	společník
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Kadmos	Kadmos	k1gMnSc1	Kadmos
ho	on	k3xPp3gMnSc4	on
nakonec	nakonec	k6eAd1	nakonec
zabije	zabít	k5eAaPmIp3nS	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc4	jeho
zuby	zub	k1gInPc4	zub
osel	osel	k1gMnSc1	osel
pole	pole	k1gFnSc2	pole
a	a	k8xC	a
vyrostlí	vyrostlý	k2eAgMnPc1d1	vyrostlý
vojáci	voják	k1gMnPc1	voják
mu	on	k3xPp3gMnSc3	on
pomohli	pomoct	k5eAaPmAgMnP	pomoct
založit	založit	k5eAaPmF	založit
město	město	k1gNnSc4	město
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jejich	jejich	k3xOp3gMnSc7	jejich
králem	král	k1gMnSc7	král
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
starý	starý	k1gMnSc1	starý
svého	své	k1gNnSc2	své
činu	čin	k1gInSc2	čin
lituje	litovat	k5eAaImIp3nS	litovat
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
sám	sám	k3xTgMnSc1	sám
promění	proměnit	k5eAaPmIp3nS	proměnit
ve	v	k7c4	v
velkého	velký	k2eAgMnSc4d1	velký
hada	had	k1gMnSc4	had
–	–	k?	–
draka	drak	k1gMnSc4	drak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stohlavý	stohlavý	k2eAgMnSc1d1	stohlavý
drak	drak	k1gMnSc1	drak
Ládón	Ládón	k1gMnSc1	Ládón
<g/>
,	,	kIx,	,
potomek	potomek	k1gMnSc1	potomek
nymfy	nymfa	k1gFnSc2	nymfa
Echidny	Echidna	k1gFnSc2	Echidna
a	a	k8xC	a
storukého	storuký	k2eAgMnSc2d1	storuký
obra	obr	k1gMnSc2	obr
Týfóna	Týfón	k1gMnSc2	Týfón
<g/>
,	,	kIx,	,
střeží	střežit	k5eAaImIp3nP	střežit
zlatá	zlatý	k2eAgNnPc1d1	Zlaté
jablka	jablko	k1gNnPc1	jablko
v	v	k7c6	v
zahradě	zahrada	k1gFnSc6	zahrada
Hesperidek	Hesperidka	k1gFnPc2	Hesperidka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Trochu	trochu	k6eAd1	trochu
atypickým	atypický	k2eAgMnSc7d1	atypický
drakem	drak	k1gMnSc7	drak
byl	být	k5eAaImAgMnS	být
Delfynes	Delfynes	k1gMnSc1	Delfynes
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
stvořila	stvořit	k5eAaPmAgFnS	stvořit
bohyně	bohyně	k1gFnSc1	bohyně
Gáia	Gáia	k1gFnSc1	Gáia
při	při	k7c6	při
válce	válka	k1gFnSc6	válka
s	s	k7c7	s
nebesy	nebesa	k1gNnPc7	nebesa
a	a	k8xC	a
kterého	který	k3yRgMnSc4	který
zabil	zabít	k5eAaPmAgMnS	zabít
Apollón	Apollón	k1gMnSc1	Apollón
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Latinské	latinský	k2eAgNnSc1d1	latinské
slovo	slovo	k1gNnSc1	slovo
draco	draco	k1gMnSc1	draco
<g/>
,	,	kIx,	,
–	–	k?	–
<g/>
ónis	ónis	k1gInSc1	ónis
<g/>
,	,	kIx,	,
znamená	znamenat	k5eAaImIp3nS	znamenat
jak	jak	k6eAd1	jak
"	"	kIx"	"
<g/>
drak	drak	k1gInSc1	drak
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
i	i	k9	i
"	"	kIx"	"
<g/>
had	had	k1gMnSc1	had
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
čehož	což	k3yRnSc2	což
byli	být	k5eAaImAgMnP	být
draci	drak	k1gMnPc1	drak
ještě	ještě	k6eAd1	ještě
dlouho	dlouho	k6eAd1	dlouho
zaměňováni	zaměňovat	k5eAaImNgMnP	zaměňovat
s	s	k7c7	s
hady	had	k1gMnPc7	had
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Bible	bible	k1gFnSc2	bible
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
Bible	bible	k1gFnSc2	bible
se	se	k3xPyFc4	se
drak	drak	k1gMnSc1	drak
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
pouze	pouze	k6eAd1	pouze
několikrát	několikrát	k6eAd1	několikrát
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
symbolickém	symbolický	k2eAgInSc6d1	symbolický
významu	význam	k1gInSc6	význam
v	v	k7c6	v
Novém	nový	k2eAgInSc6d1	nový
zákoně	zákon	k1gInSc6	zákon
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Zjevení	zjevení	k1gNnSc1	zjevení
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
drak	drak	k1gInSc1	drak
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
symbolické	symbolický	k2eAgNnSc1d1	symbolické
vykreslení	vykreslení	k1gNnSc1	vykreslení
Satana	Satan	k1gMnSc2	Satan
Ďábla	ďábel	k1gMnSc2	ďábel
<g/>
.	.	kIx.	.
</s>
<s>
Výjimečně	výjimečně	k6eAd1	výjimečně
je	být	k5eAaImIp3nS	být
za	za	k7c4	za
draka	drak	k1gMnSc4	drak
považován	považován	k2eAgMnSc1d1	považován
Behemot	behemot	k1gMnSc1	behemot
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
překládán	překládán	k2eAgMnSc1d1	překládán
jako	jako	k8xS	jako
Velké	velký	k2eAgNnSc1d1	velké
zvíře	zvíře	k1gNnSc1	zvíře
<g/>
,	,	kIx,	,
obluda	obluda	k1gFnSc1	obluda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
či	či	k8xC	či
Leviatan	leviatan	k1gMnSc1	leviatan
(	(	kIx(	(
<g/>
v	v	k7c6	v
kontextu	kontext	k1gInSc6	kontext
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
krokodýl	krokodýl	k1gMnSc1	krokodýl
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
prostě	prostě	k9	prostě
vodní	vodní	k2eAgFnSc1d1	vodní
obluda	obluda	k1gFnSc1	obluda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Naopak	naopak	k6eAd1	naopak
v	v	k7c6	v
apokryfních	apokryfní	k2eAgFnPc6d1	apokryfní
knihách	kniha	k1gFnPc6	kniha
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
zřejmě	zřejmě	k6eAd1	zřejmě
značně	značně	k6eAd1	značně
ovlivněny	ovlivnit	k5eAaPmNgInP	ovlivnit
myšlenkami	myšlenka	k1gFnPc7	myšlenka
národů	národ	k1gInPc2	národ
Předního	přední	k2eAgInSc2d1	přední
východu	východ	k1gInSc2	východ
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
s	s	k7c7	s
drakem	drak	k1gInSc7	drak
můžeme	moct	k5eAaImIp1nP	moct
setkat	setkat	k5eAaPmF	setkat
častěji	často	k6eAd2	často
<g/>
.	.	kIx.	.
</s>
<s>
Vykreslení	vykreslení	k1gNnSc1	vykreslení
těchto	tento	k3xDgFnPc2	tento
bytostí	bytost	k1gFnPc2	bytost
je	být	k5eAaImIp3nS	být
zhusta	zhusta	k6eAd1	zhusta
značně	značně	k6eAd1	značně
negativní	negativní	k2eAgMnSc1d1	negativní
<g/>
.	.	kIx.	.
</s>
<s>
Zlovolné	zlovolný	k2eAgFnPc1d1	zlovolná
<g/>
,	,	kIx,	,
nepojmenované	pojmenovaný	k2eNgFnPc1d1	nepojmenovaná
bytosti	bytost	k1gFnPc1	bytost
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
osud	osud	k1gInSc1	osud
není	být	k5eNaImIp3nS	být
nijak	nijak	k6eAd1	nijak
zkoumán	zkoumat	k5eAaImNgInS	zkoumat
<g/>
.	.	kIx.	.
</s>
<s>
Prostě	prostě	k6eAd1	prostě
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
jako	jako	k8xS	jako
součást	součást	k1gFnSc4	součást
nějakého	nějaký	k3yIgInSc2	nějaký
příběhu	příběh	k1gInSc2	příběh
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
nehrají	hrát	k5eNaImIp3nP	hrát
hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zajímavá	zajímavý	k2eAgFnSc1d1	zajímavá
je	být	k5eAaImIp3nS	být
Kniha	kniha	k1gFnSc1	kniha
Henochova	Henochův	k2eAgFnSc1d1	Henochova
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
přímo	přímo	k6eAd1	přímo
vysloveno	vyslovit	k5eAaPmNgNnS	vyslovit
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c4	v
den	den	k1gInSc4	den
Potopy	potopa	k1gFnSc2	potopa
světa	svět	k1gInSc2	svět
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
rozdělení	rozdělení	k1gNnSc3	rozdělení
dvou	dva	k4xCgMnPc2	dva
"	"	kIx"	"
<g/>
oblud	oblud	k1gInSc4	oblud
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
draků	drak	k1gMnPc2	drak
<g/>
)	)	kIx)	)
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
jedna	jeden	k4xCgFnSc1	jeden
bude	být	k5eAaImBp3nS	být
samice	samice	k1gFnSc1	samice
–	–	k?	–
Leviatan	leviatan	k1gInSc1	leviatan
(	(	kIx(	(
<g/>
mořská	mořský	k2eAgFnSc1d1	mořská
<g/>
)	)	kIx)	)
a	a	k8xC	a
druhá	druhý	k4xOgFnSc1	druhý
samec	samec	k1gMnSc1	samec
Behemot	behemot	k1gMnSc1	behemot
<g/>
.	.	kIx.	.
</s>
<s>
Velice	velice	k6eAd1	velice
zvláštní	zvláštní	k2eAgNnSc4d1	zvláštní
je	být	k5eAaImIp3nS	být
zabití	zabití	k1gNnSc4	zabití
babylonského	babylonský	k2eAgMnSc2d1	babylonský
draka	drak	k1gMnSc2	drak
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
prorok	prorok	k1gMnSc1	prorok
Daniel	Daniel	k1gMnSc1	Daniel
nakrmil	nakrmit	k5eAaPmAgMnS	nakrmit
šiškami	šiška	k1gFnPc7	šiška
z	z	k7c2	z
tuku	tuk	k1gInSc2	tuk
<g/>
,	,	kIx,	,
smoly	smola	k1gFnSc2	smola
a	a	k8xC	a
chlupů	chlup	k1gInPc2	chlup
a	a	k8xC	a
drak	drak	k1gInSc1	drak
poté	poté	k6eAd1	poté
pukl	puknout	k5eAaPmAgInS	puknout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Severská	severský	k2eAgFnSc1d1	severská
mytologie	mytologie	k1gFnSc1	mytologie
====	====	k?	====
</s>
</p>
<p>
<s>
Jörmungandr	Jörmungandr	k1gInSc1	Jörmungandr
(	(	kIx(	(
<g/>
Midgardsormr	Midgardsormr	k1gInSc1	Midgardsormr
<g/>
)	)	kIx)	)
vyskytující	vyskytující	k2eAgMnSc1d1	vyskytující
se	se	k3xPyFc4	se
v	v	k7c6	v
severské	severský	k2eAgFnSc6d1	severská
mytologii	mytologie	k1gFnSc6	mytologie
je	být	k5eAaImIp3nS	být
sice	sice	k8xC	sice
častěji	často	k6eAd2	často
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
mořského	mořský	k2eAgMnSc4d1	mořský
hada	had	k1gMnSc4	had
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
i	i	k9	i
drak	drak	k1gInSc4	drak
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Eddy	Edda	k1gFnSc2	Edda
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
hlavního	hlavní	k2eAgMnSc4d1	hlavní
protivníka	protivník	k1gMnSc4	protivník
boha	bůh	k1gMnSc4	bůh
hromu	hrom	k1gInSc2	hrom
Thóra	Thóra	k1gFnSc1	Thóra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Středověká	středověký	k2eAgFnSc1d1	středověká
Evropa	Evropa	k1gFnSc1	Evropa
====	====	k?	====
</s>
</p>
<p>
<s>
Draky	drak	k1gInPc1	drak
se	se	k3xPyFc4	se
zabývalo	zabývat	k5eAaImAgNnS	zabývat
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
odborné	odborný	k2eAgFnSc2d1	odborná
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Evropské	evropský	k2eAgNnSc1d1	Evropské
pojetí	pojetí	k1gNnSc1	pojetí
draka	drak	k1gMnSc2	drak
se	se	k3xPyFc4	se
vyvíjelo	vyvíjet	k5eAaImAgNnS	vyvíjet
poměrně	poměrně	k6eAd1	poměrně
složitou	složitý	k2eAgFnSc7d1	složitá
cestou	cesta	k1gFnSc7	cesta
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
zásadní	zásadní	k2eAgInSc4d1	zásadní
dopad	dopad	k1gInSc4	dopad
na	na	k7c4	na
evropské	evropský	k2eAgNnSc4d1	Evropské
pojetí	pojetí	k1gNnSc4	pojetí
draků	drak	k1gMnPc2	drak
měla	mít	k5eAaImAgFnS	mít
samozřejmě	samozřejmě	k6eAd1	samozřejmě
Bible	bible	k1gFnSc1	bible
<g/>
,	,	kIx,	,
z	z	k7c2	z
dalších	další	k2eAgMnPc2d1	další
významných	významný	k2eAgMnPc2d1	významný
myslitelů	myslitel	k1gMnPc2	myslitel
lze	lze	k6eAd1	lze
uvést	uvést	k5eAaPmF	uvést
svatého	svatý	k2eAgMnSc4d1	svatý
Augustina	Augustin	k1gMnSc4	Augustin
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
jako	jako	k9	jako
první	první	k4xOgMnPc1	první
uvádí	uvádět	k5eAaImIp3nP	uvádět
představu	představa	k1gFnSc4	představa
<g/>
,	,	kIx,	,
že	že	k8xS	že
dračí	dračí	k2eAgNnPc1d1	dračí
křídla	křídlo	k1gNnPc1	křídlo
jsou	být	k5eAaImIp3nP	být
podobná	podobný	k2eAgNnPc1d1	podobné
netopýřím	netopýří	k2eAgFnPc3d1	netopýří
(	(	kIx(	(
<g/>
tuto	tento	k3xDgFnSc4	tento
představu	představa	k1gFnSc4	představa
neopustila	opustit	k5eNaPmAgFnS	opustit
evropská	evropský	k2eAgFnSc1d1	Evropská
animace	animace	k1gFnSc1	animace
a	a	k8xC	a
film	film	k1gInSc1	film
dodnes	dodnes	k6eAd1	dodnes
<g/>
)	)	kIx)	)
a	a	k8xC	a
že	že	k8xS	že
vzduch	vzduch	k1gInSc1	vzduch
okolo	okolo	k7c2	okolo
draka	drak	k1gMnSc2	drak
je	být	k5eAaImIp3nS	být
zamořen	zamořit	k5eAaPmNgInS	zamořit
jeho	jeho	k3xOp3gInSc7	jeho
dechem	dech	k1gInSc7	dech
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
tvrzení	tvrzení	k1gNnSc1	tvrzení
je	být	k5eAaImIp3nS	být
zajímavé	zajímavý	k2eAgFnPc4d1	zajímavá
–	–	k?	–
dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
soudí	soudit	k5eAaImIp3nS	soudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
ze	z	k7c2	z
skutečnosti	skutečnost	k1gFnSc2	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
jeskyních	jeskyně	k1gFnPc6	jeskyně
(	(	kIx(	(
<g/>
kde	kde	k6eAd1	kde
draci	drak	k1gMnPc1	drak
měli	mít	k5eAaImAgMnP	mít
žít	žít	k5eAaImF	žít
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
zatuchlý	zatuchlý	k2eAgInSc4d1	zatuchlý
vzduch	vzduch	k1gInSc4	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
v	v	k7c6	v
době	doba	k1gFnSc6	doba
sv.	sv.	kA	sv.
Augustýna	Augustýn	k1gMnSc2	Augustýn
nebylo	být	k5eNaImAgNnS	být
zcela	zcela	k6eAd1	zcela
běžné	běžný	k2eAgNnSc1d1	běžné
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
drak	drak	k1gMnSc1	drak
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
jeskyni	jeskyně	k1gFnSc6	jeskyně
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
ještě	ještě	k9	ještě
mnoho	mnoho	k4c4	mnoho
let	léto	k1gNnPc2	léto
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
se	se	k3xPyFc4	se
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
draci	drak	k1gMnPc1	drak
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
Etiopii	Etiopie	k1gFnSc6	Etiopie
a	a	k8xC	a
v	v	k7c6	v
podobných	podobný	k2eAgFnPc6d1	podobná
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Středověk	středověk	k1gInSc1	středověk
rozeznával	rozeznávat	k5eAaImAgInS	rozeznávat
draky	drak	k1gMnPc4	drak
okřídlené	okřídlený	k2eAgNnSc1d1	okřídlené
a	a	k8xC	a
neokřídlené	okřídlený	k2eNgNnSc1d1	okřídlený
<g/>
.	.	kIx.	.
</s>
<s>
Nevěřit	věřit	k5eNaImF	věřit
v	v	k7c4	v
neokřídlené	okřídlený	k2eNgInPc4d1	okřídlený
draky	drak	k1gInPc4	drak
(	(	kIx(	(
<g/>
a	a	k8xC	a
draky	drak	k1gInPc1	drak
obecně	obecně	k6eAd1	obecně
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
těžkým	těžký	k2eAgInSc7d1	těžký
hříchem	hřích	k1gInSc7	hřích
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
pochybovač	pochybovač	k1gMnSc1	pochybovač
odporoval	odporovat	k5eAaImAgMnS	odporovat
Písmu	písmo	k1gNnSc3	písmo
svatému	svatý	k2eAgNnSc3d1	svaté
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
svatých	svatá	k1gFnPc2	svatá
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
zaznamenali	zaznamenat	k5eAaPmAgMnP	zaznamenat
jejich	jejich	k3xOp3gInPc4	jejich
biografové	biografový	k2eAgInPc4d1	biografový
draky	drak	k1gInPc4	drak
zabilo	zabít	k5eAaPmAgNnS	zabít
nebo	nebo	k8xC	nebo
zahnalo	zahnat	k5eAaPmAgNnS	zahnat
<g/>
.	.	kIx.	.
</s>
<s>
Draka	drak	k1gMnSc4	drak
jako	jako	k8xC	jako
atribut	atribut	k1gInSc4	atribut
má	mít	k5eAaImIp3nS	mít
hned	hned	k6eAd1	hned
několik	několik	k4yIc4	několik
křesťanských	křesťanský	k2eAgMnPc2d1	křesťanský
světců	světec	k1gMnPc2	světec
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc4	ten
například	například	k6eAd1	například
Benedikt	Benedikt	k1gMnSc1	Benedikt
z	z	k7c2	z
Nursie	Nursie	k1gFnSc2	Nursie
(	(	kIx(	(
<g/>
okřídlený	okřídlený	k2eAgMnSc1d1	okřídlený
dráček	dráček	k1gMnSc1	dráček
vylézá	vylézat	k5eAaImIp3nS	vylézat
z	z	k7c2	z
kalicha	kalich	k1gInSc2	kalich
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Gotthard	Gotthard	k1gMnSc1	Gotthard
(	(	kIx(	(
<g/>
zkrocený	zkrocený	k2eAgMnSc1d1	zkrocený
u	u	k7c2	u
nohou	noha	k1gFnPc2	noha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ignác	Ignác	k1gMnSc1	Ignác
z	z	k7c2	z
Loyoly	Loyola	k1gFnSc2	Loyola
(	(	kIx(	(
<g/>
přemáhá	přemáhat	k5eAaImIp3nS	přemáhat
ho	on	k3xPp3gNnSc4	on
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
svatý	svatý	k2eAgMnSc1d1	svatý
Jiří	Jiří	k1gMnSc1	Jiří
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
probodává	probodávat	k5eAaImIp3nS	probodávat
ho	on	k3xPp3gMnSc4	on
kopím	kopit	k5eAaImIp1nS	kopit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Lev	lev	k1gInSc1	lev
Veliký	veliký	k2eAgInSc1d1	veliký
(	(	kIx(	(
<g/>
drak	drak	k1gInSc1	drak
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
mocnosti	mocnost	k1gFnPc4	mocnost
temna	temno	k1gNnSc2	temno
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Markéta	Markéta	k1gFnSc1	Markéta
Antiochijská	antiochijský	k2eAgFnSc1d1	Antiochijská
(	(	kIx(	(
<g/>
u	u	k7c2	u
nohou	noha	k1gFnPc2	noha
<g/>
,	,	kIx,	,
na	na	k7c6	na
dlani	dlaň	k1gFnSc6	dlaň
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
ho	on	k3xPp3gMnSc4	on
drží	držet	k5eAaImIp3nP	držet
na	na	k7c6	na
provázku	provázek	k1gInSc6	provázek
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
zaměněn	zaměněn	k2eAgMnSc1d1	zaměněn
za	za	k7c4	za
hada	had	k1gMnSc4	had
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Marta	Marta	k1gFnSc1	Marta
<g/>
,	,	kIx,	,
Hilarius	Hilarius	k1gMnSc1	Hilarius
nebo	nebo	k8xC	nebo
archanděl	archanděl	k1gMnSc1	archanděl
Michael	Michael	k1gMnSc1	Michael
(	(	kIx(	(
<g/>
přemáhá	přemáhat	k5eAaImIp3nS	přemáhat
ďábla	ďábel	k1gMnSc4	ďábel
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
draka	drak	k1gMnSc4	drak
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
v	v	k7c6	v
době	doba	k1gFnSc6	doba
krále	král	k1gMnSc2	král
Artuše	Artuš	k1gMnSc2	Artuš
byli	být	k5eAaImAgMnP	být
draci	drak	k1gMnPc1	drak
různých	různý	k2eAgFnPc2d1	různá
barev	barva	k1gFnPc2	barva
symbolem	symbol	k1gInSc7	symbol
různých	různý	k2eAgFnPc2d1	různá
znepřátelených	znepřátelený	k2eAgFnPc2d1	znepřátelená
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
legendách	legenda	k1gFnPc6	legenda
o	o	k7c6	o
Merlinovi	Merlinův	k2eAgMnPc1d1	Merlinův
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
i	i	k9	i
živí	živý	k2eAgMnPc1d1	živý
draci	drak	k1gMnPc1	drak
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
dokládá	dokládat	k5eAaImIp3nS	dokládat
například	například	k6eAd1	například
pověst	pověst	k1gFnSc4	pověst
o	o	k7c6	o
souboji	souboj	k1gInSc6	souboj
červeného	červený	k2eAgMnSc2d1	červený
draka	drak	k1gMnSc2	drak
s	s	k7c7	s
bílým	bílý	k1gMnSc7	bílý
<g/>
.	.	kIx.	.
</s>
<s>
Wales	Wales	k1gInSc1	Wales
si	se	k3xPyFc3	se
červeného	červený	k2eAgMnSc4d1	červený
draka	drak	k1gMnSc4	drak
jako	jako	k8xS	jako
symbol	symbol	k1gInSc4	symbol
ponechal	ponechat	k5eAaPmAgMnS	ponechat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Francii	Francie	k1gFnSc6	Francie
je	být	k5eAaImIp3nS	být
drak	drak	k1gInSc1	drak
někdy	někdy	k6eAd1	někdy
nazýván	nazývat	k5eAaImNgInS	nazývat
guivre	guivr	k1gInSc5	guivr
(	(	kIx(	(
<g/>
či	či	k8xC	či
vouivre	vouivr	k1gInSc5	vouivr
<g/>
,	,	kIx,	,
názvy	název	k1gInPc4	název
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
francouzských	francouzský	k2eAgInPc2d1	francouzský
dialektů	dialekt	k1gInPc2	dialekt
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Franc-Comtois	Franc-Comtois	k1gInSc1	Franc-Comtois
nebo	nebo	k8xC	nebo
Ile-de-France	Ilee-France	k1gFnSc1	Ile-de-France
<g/>
)	)	kIx)	)
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
popisován	popisován	k2eAgMnSc1d1	popisován
jako	jako	k8xS	jako
příšera	příšera	k1gFnSc1	příšera
s	s	k7c7	s
dlouhým	dlouhý	k2eAgNnSc7d1	dlouhé
hadím	hadí	k2eAgNnSc7d1	hadí
tělem	tělo	k1gNnSc7	tělo
<g/>
,	,	kIx,	,
dračí	dračí	k2eAgFnSc7d1	dračí
hlavou	hlava	k1gFnSc7	hlava
a	a	k8xC	a
otráveným	otrávený	k2eAgInSc7d1	otrávený
dechem	dech	k1gInSc7	dech
<g/>
,	,	kIx,	,
občas	občas	k6eAd1	občas
rovněž	rovněž	k9	rovněž
mívá	mívat	k5eAaImIp3nS	mívat
rohy	roh	k1gInPc4	roh
na	na	k7c6	na
čele	čelo	k1gNnSc6	čelo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
některých	některý	k3yIgNnPc6	některý
místech	místo	k1gNnPc6	místo
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
velmi	velmi	k6eAd1	velmi
agresivní	agresivní	k2eAgFnSc4d1	agresivní
stvůru	stvůra	k1gFnSc4	stvůra
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
často	často	k6eAd1	často
napadá	napadat	k5eAaPmIp3nS	napadat
i	i	k9	i
bez	bez	k7c2	bez
zjevné	zjevný	k2eAgFnSc2d1	zjevná
příčiny	příčina	k1gFnSc2	příčina
či	či	k8xC	či
předchozího	předchozí	k2eAgNnSc2d1	předchozí
vyprovokování	vyprovokování	k1gNnSc2	vyprovokování
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bojí	bát	k5eAaImIp3nP	bát
se	se	k3xPyFc4	se
nahých	nahý	k2eAgInPc2d1	nahý
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
na	na	k7c4	na
ně	on	k3xPp3gNnSc4	on
se	se	k3xPyFc4	se
červená	červenat	k5eAaImIp3nS	červenat
a	a	k8xC	a
odvrací	odvracet	k5eAaImIp3nS	odvracet
pohled	pohled	k1gInSc4	pohled
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Samson	Samson	k1gMnSc1	Samson
z	z	k7c2	z
Dolu	dol	k1gInSc2	dol
byl	být	k5eAaImAgInS	být
prý	prý	k9	prý
svědkem	svědek	k1gMnSc7	svědek
setkání	setkání	k1gNnSc4	setkání
dráčka	dráček	k1gMnSc4	dráček
nazývaného	nazývaný	k2eAgInSc2d1	nazývaný
La	la	k0	la
Guivre	Guivr	k1gInSc5	Guivr
a	a	k8xC	a
mladého	mladý	k2eAgMnSc2d1	mladý
kněze	kněz	k1gMnSc2	kněz
<g/>
.	.	kIx.	.
</s>
<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Samson	Samson	k1gMnSc1	Samson
se	se	k3xPyFc4	se
s	s	k7c7	s
několika	několik	k4yIc7	několik
následovníky	následovník	k1gMnPc7	následovník
vypravil	vypravit	k5eAaPmAgMnS	vypravit
navštívit	navštívit	k5eAaPmF	navštívit
svatého	svatý	k2eAgMnSc4d1	svatý
Tysilia	Tysilius	k1gMnSc4	Tysilius
<g/>
,	,	kIx,	,
zakladatele	zakladatel	k1gMnSc2	zakladatel
kláštera	klášter	k1gInSc2	klášter
v	v	k7c6	v
Saint-Suliacu	Saint-Suliacus	k1gInSc6	Saint-Suliacus
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
byl	být	k5eAaImAgMnS	být
sice	sice	k8xC	sice
velmi	velmi	k6eAd1	velmi
chudý	chudý	k2eAgMnSc1d1	chudý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
poutníkům	poutník	k1gMnPc3	poutník
poskytnout	poskytnout	k5eAaPmF	poskytnout
co	co	k9	co
nejlepší	dobrý	k2eAgNnSc4d3	nejlepší
pohoštění	pohoštění	k1gNnSc4	pohoštění
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
mnich	mnich	k1gMnSc1	mnich
<g/>
,	,	kIx,	,
nespokojený	spokojený	k2eNgMnSc1d1	nespokojený
s	s	k7c7	s
kvalitou	kvalita	k1gFnSc7	kvalita
jídla	jídlo	k1gNnSc2	jídlo
<g/>
,	,	kIx,	,
schoval	schovat	k5eAaPmAgMnS	schovat
skývu	skýva	k1gFnSc4	skýva
chleba	chléb	k1gInSc2	chléb
pod	pod	k7c4	pod
plášť	plášť	k1gInSc4	plášť
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ji	on	k3xPp3gFnSc4	on
nemusel	muset	k5eNaImAgMnS	muset
snít	snít	k5eAaPmF	snít
či	či	k8xC	či
nechat	nechat	k5eAaPmF	nechat
na	na	k7c6	na
stole	stol	k1gInSc6	stol
<g/>
.	.	kIx.	.
</s>
<s>
Takřka	takřka	k6eAd1	takřka
okamžitě	okamžitě	k6eAd1	okamžitě
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
třást	třást	k5eAaImF	třást
a	a	k8xC	a
hlasitě	hlasitě	k6eAd1	hlasitě
křičet	křičet	k5eAaImF	křičet
<g/>
.	.	kIx.	.
</s>
<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Samson	Samson	k1gMnSc1	Samson
nevěděl	vědět	k5eNaImAgMnS	vědět
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
si	se	k3xPyFc3	se
počít	počít	k5eAaPmF	počít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
svatý	svatý	k2eAgMnSc1d1	svatý
Tysilio	Tysilio	k1gMnSc1	Tysilio
mnichovi	mnich	k1gMnSc3	mnich
ihned	ihned	k6eAd1	ihned
roztrhl	roztrhnout	k5eAaPmAgMnS	roztrhnout
plášť	plášť	k1gInSc4	plášť
a	a	k8xC	a
objevil	objevit	k5eAaPmAgMnS	objevit
hádě	hádě	k1gNnSc4	hádě
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
nebožákovi	nebožák	k1gMnSc3	nebožák
zahryzlo	zahryznout	k5eAaPmAgNnS	zahryznout
do	do	k7c2	do
prsou	prsa	k1gNnPc2	prsa
<g/>
.	.	kIx.	.
</s>
<s>
Napomenul	napomenout	k5eAaPmAgMnS	napomenout
kněze	kněz	k1gMnPc4	kněz
za	za	k7c4	za
jeho	jeho	k3xOp3gNnPc4	jeho
chování	chování	k1gNnSc4	chování
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
následně	následně	k6eAd1	následně
provedl	provést	k5eAaPmAgInS	provést
zaříkání	zaříkání	k1gNnSc4	zaříkání
a	a	k8xC	a
svrhl	svrhnout	k5eAaPmAgMnS	svrhnout
dráče	dráče	k1gNnSc4	dráče
ze	z	k7c2	z
skály	skála	k1gFnSc2	skála
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
The	The	k1gFnSc6	The
Drac	Drac	k1gFnSc1	Drac
<g/>
:	:	kIx,	:
French	French	k1gMnSc1	French
Tales	Tales	k1gMnSc1	Tales
of	of	k?	of
Dragons	Dragons	k1gInSc1	Dragons
and	and	k?	and
Demons	Demonsa	k1gFnPc2	Demonsa
<g/>
,	,	kIx,	,
francouzské	francouzský	k2eAgFnSc3d1	francouzská
knize	kniha	k1gFnSc3	kniha
o	o	k7c6	o
dracích	drak	k1gMnPc6	drak
a	a	k8xC	a
démonech	démon	k1gMnPc6	démon
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vouivre	vouivr	k1gInSc5	vouivr
zobrazována	zobrazován	k2eAgFnSc1d1	zobrazována
jako	jako	k8xS	jako
ženská	ženský	k2eAgFnSc1d1	ženská
příšera	příšera	k1gFnSc1	příšera
s	s	k7c7	s
oslnivými	oslnivý	k2eAgFnPc7d1	oslnivá
<g/>
,	,	kIx,	,
zelenými	zelený	k2eAgFnPc7d1	zelená
šupinami	šupina	k1gFnPc7	šupina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
zvuk	zvuk	k1gInSc4	zvuk
jako	jako	k8xS	jako
moucha	moucha	k1gFnSc1	moucha
<g/>
.	.	kIx.	.
</s>
<s>
Vouivre	Vouivr	k1gMnSc5	Vouivr
je	být	k5eAaImIp3nS	být
popisována	popisován	k2eAgFnSc1d1	popisována
jako	jako	k8xS	jako
chamtivá	chamtivý	k2eAgFnSc1d1	chamtivá
a	a	k8xC	a
chtivá	chtivý	k2eAgFnSc1d1	chtivá
s	s	k7c7	s
hlavou	hlava	k1gFnSc7	hlava
korunovanou	korunovaný	k2eAgFnSc4d1	korunovaná
perlami	perla	k1gFnPc7	perla
a	a	k8xC	a
zlatým	zlatý	k2eAgInSc7d1	zlatý
prstenem	prsten	k1gInSc7	prsten
na	na	k7c6	na
ocasu	ocas	k1gInSc6	ocas
<g/>
.	.	kIx.	.
</s>
<s>
Příšera	příšera	k1gFnSc1	příšera
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
většinu	většina	k1gFnSc4	většina
času	čas	k1gInSc2	čas
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
jeskyni	jeskyně	k1gFnSc6	jeskyně
<g/>
,	,	kIx,	,
odchází	odcházet	k5eAaImIp3nS	odcházet
jen	jen	k9	jen
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
vykoupala	vykoupat	k5eAaPmAgFnS	vykoupat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Konrad	Konrad	k1gInSc1	Konrad
Gesner	Gesner	k1gInSc1	Gesner
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
Historiae	Historia	k1gFnSc2	Historia
animalium	animalium	k1gNnSc4	animalium
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
slovo	slovo	k1gNnSc1	slovo
draco	draco	k6eAd1	draco
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
"	"	kIx"	"
<g/>
ostře	ostro	k6eAd1	ostro
vidět	vidět	k5eAaImF	vidět
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
ovlivnilo	ovlivnit	k5eAaPmAgNnS	ovlivnit
představy	představa	k1gFnPc4	představa
o	o	k7c6	o
dracích	drak	k1gInPc6	drak
na	na	k7c4	na
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
a	a	k8xC	a
lze	lze	k6eAd1	lze
tvrdit	tvrdit	k5eAaImF	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
dobrý	dobrý	k2eAgInSc1d1	dobrý
zrak	zrak	k1gInSc1	zrak
byl	být	k5eAaImAgInS	být
nahrazen	nahradit	k5eAaPmNgInS	nahradit
dobrým	dobrý	k2eAgInSc7d1	dobrý
čichem	čich	k1gInSc7	čich
až	až	k9	až
v	v	k7c6	v
pohádkách	pohádka	k1gFnPc6	pohádka
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Dobrý	dobrý	k2eAgInSc1d1	dobrý
zrak	zrak	k1gInSc1	zrak
měl	mít	k5eAaImAgInS	mít
své	svůj	k3xOyFgNnSc4	svůj
opodstatnění	opodstatnění	k1gNnSc4	opodstatnění
v	v	k7c6	v
úvaze	úvaha	k1gFnSc6	úvaha
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
drak	drak	k1gMnSc1	drak
letí	letět	k5eAaImIp3nS	letět
do	do	k7c2	do
velké	velký	k2eAgFnSc2d1	velká
výšky	výška	k1gFnSc2	výška
musí	muset	k5eAaImIp3nS	muset
nutně	nutně	k6eAd1	nutně
dobře	dobře	k6eAd1	dobře
vidět	vidět	k5eAaImF	vidět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dalším	další	k2eAgInSc7d1	další
zdrojem	zdroj	k1gInSc7	zdroj
informací	informace	k1gFnPc2	informace
o	o	k7c6	o
dracích	drak	k1gInPc6	drak
je	být	k5eAaImIp3nS	být
Mandevillův	Mandevillův	k2eAgInSc4d1	Mandevillův
cestopis	cestopis	k1gInSc4	cestopis
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
o	o	k7c6	o
několika	několik	k4yIc6	několik
dracích	drak	k1gInPc6	drak
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neposkytuje	poskytovat	k5eNaImIp3nS	poskytovat
nové	nový	k2eAgInPc4d1	nový
obecné	obecný	k2eAgInPc4d1	obecný
poznatky	poznatek	k1gInPc4	poznatek
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
nás	my	k3xPp1nPc4	my
seznamuje	seznamovat	k5eAaImIp3nS	seznamovat
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jaký	jaký	k3yRgInSc1	jaký
druh	druh	k1gInSc1	druh
draka	drak	k1gMnSc2	drak
žije	žít	k5eAaImIp3nS	žít
<g/>
.	.	kIx.	.
</s>
<s>
Jedinou	jediný	k2eAgFnSc7d1	jediná
novinkou	novinka	k1gFnSc7	novinka
tohoto	tento	k3xDgInSc2	tento
cestopisu	cestopis	k1gInSc2	cestopis
je	být	k5eAaImIp3nS	být
možnost	možnost	k1gFnSc4	možnost
začarování	začarování	k1gNnSc2	začarování
ženy	žena	k1gFnSc2	žena
v	v	k7c4	v
draka	drak	k1gMnSc4	drak
–	–	k?	–
Pověst	pověst	k1gFnSc1	pověst
o	o	k7c6	o
Ipokrasově	Ipokrasův	k2eAgFnSc6d1	Ipokrasův
dceři	dcera	k1gFnSc6	dcera
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Drak	drak	k1gInSc1	drak
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
také	také	k9	také
v	v	k7c6	v
proslulé	proslulý	k2eAgFnSc6d1	proslulá
středověké	středověký	k2eAgFnSc6d1	středověká
pověsti	pověst	k1gFnSc6	pověst
o	o	k7c6	o
víle	víla	k1gFnSc6	víla
Meluzíně	Meluzína	k1gFnSc6	Meluzína
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
každou	každý	k3xTgFnSc4	každý
sobotu	sobota	k1gFnSc4	sobota
změnily	změnit	k5eAaPmAgFnP	změnit
nohy	noha	k1gFnPc1	noha
na	na	k7c4	na
hadí	hadí	k2eAgInSc4d1	hadí
ocas	ocas	k1gInSc4	ocas
a	a	k8xC	a
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
byla	být	k5eAaImAgFnS	být
takto	takto	k6eAd1	takto
přes	přes	k7c4	přes
zákaz	zákaz	k1gInSc4	zákaz
spatřena	spatřit	k5eAaPmNgFnS	spatřit
svým	svůj	k1gMnSc7	svůj
manželem	manžel	k1gMnSc7	manžel
v	v	k7c6	v
lázni	lázeň	k1gFnSc6	lázeň
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
proměnila	proměnit	k5eAaPmAgFnS	proměnit
v	v	k7c4	v
draka	drak	k1gMnSc4	drak
(	(	kIx(	(
<g/>
okřídleného	okřídlený	k2eAgMnSc2d1	okřídlený
hada	had	k1gMnSc2	had
<g/>
)	)	kIx)	)
a	a	k8xC	a
zmizela	zmizet	k5eAaPmAgFnS	zmizet
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středověké	středověký	k2eAgFnSc6d1	středověká
heraldice	heraldika	k1gFnSc6	heraldika
byla	být	k5eAaImAgFnS	být
proto	proto	k8xC	proto
zobrazována	zobrazován	k2eAgFnSc1d1	zobrazována
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
draka	drak	k1gMnSc2	drak
v	v	k7c6	v
kádi	káď	k1gFnSc6	káď
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
později	pozdě	k6eAd2	pozdě
začala	začít	k5eAaPmAgFnS	začít
být	být	k5eAaImF	být
ztotožňována	ztotožňován	k2eAgFnSc1d1	ztotožňována
s	s	k7c7	s
mořskou	mořský	k2eAgFnSc7d1	mořská
pannou	panna	k1gFnSc7	panna
<g/>
.	.	kIx.	.
<g/>
Ve	v	k7c6	v
středověké	středověký	k2eAgFnSc6d1	středověká
Evropě	Evropa	k1gFnSc6	Evropa
bylo	být	k5eAaImAgNnS	být
mnoho	mnoho	k6eAd1	mnoho
"	"	kIx"	"
<g/>
draků	drak	k1gMnPc2	drak
<g/>
"	"	kIx"	"
vystaveno	vystavit	k5eAaPmNgNnS	vystavit
v	v	k7c6	v
tehdejších	tehdejší	k2eAgInPc6d1	tehdejší
kabinetech	kabinet	k1gInPc6	kabinet
kuriozit	kuriozita	k1gFnPc2	kuriozita
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastějším	častý	k2eAgMnSc7d3	nejčastější
takto	takto	k6eAd1	takto
vystavovaným	vystavovaný	k2eAgMnSc7d1	vystavovaný
drakem	drak	k1gMnSc7	drak
byl	být	k5eAaImAgMnS	být
buď	buď	k8xC	buď
krokodýl	krokodýl	k1gMnSc1	krokodýl
nebo	nebo	k8xC	nebo
rejnok	rejnok	k1gMnSc1	rejnok
<g/>
.	.	kIx.	.
</s>
<s>
Středověk	středověk	k1gInSc1	středověk
obecně	obecně	k6eAd1	obecně
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
ale	ale	k9	ale
proslul	proslout	k5eAaPmAgMnS	proslout
představou	představa	k1gFnSc7	představa
soubojů	souboj	k1gInPc2	souboj
s	s	k7c7	s
draky	drak	k1gInPc7	drak
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc7	jejich
postupnou	postupný	k2eAgFnSc7d1	postupná
likvidací	likvidace	k1gFnSc7	likvidace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
=====	=====	k?	=====
Zabití	zabití	k1gNnSc1	zabití
draka	drak	k1gMnSc2	drak
=====	=====	k?	=====
</s>
</p>
<p>
<s>
Motiv	motiv	k1gInSc1	motiv
zabití	zabití	k1gNnSc1	zabití
draka	drak	k1gMnSc2	drak
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
velice	velice	k6eAd1	velice
často	často	k6eAd1	často
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
mnohých	mnohý	k2eAgMnPc2d1	mnohý
etnografů	etnograf	k1gMnPc2	etnograf
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
skutečně	skutečně	k6eAd1	skutečně
hrdinských	hrdinský	k2eAgFnPc2d1	hrdinská
situací	situace	k1gFnPc2	situace
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
draka	drak	k1gMnSc4	drak
zpravidla	zpravidla	k6eAd1	zpravidla
zabíjeli	zabíjet	k5eAaImAgMnP	zabíjet
hrdinové	hrdina	k1gMnPc1	hrdina
případně	případně	k6eAd1	případně
sami	sám	k3xTgMnPc1	sám
bozi	bůh	k1gMnPc1	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
je	být	k5eAaImIp3nS	být
drak	drak	k1gInSc1	drak
zabíjen	zabíjen	k2eAgInSc1d1	zabíjen
jako	jako	k8xS	jako
temný	temný	k2eAgInSc1d1	temný
element	element	k1gInSc1	element
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc1	jeho
zabití	zabití	k1gNnSc1	zabití
pak	pak	k6eAd1	pak
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
vítězství	vítězství	k1gNnSc4	vítězství
světla	světlo	k1gNnSc2	světlo
a	a	k8xC	a
dobra	dobro	k1gNnSc2	dobro
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
je	být	k5eAaImIp3nS	být
zabití	zabití	k1gNnSc1	zabití
draka	drak	k1gMnSc2	drak
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
akt	akt	k1gInSc4	akt
zabití	zabití	k1gNnSc2	zabití
posvátného	posvátný	k2eAgNnSc2d1	posvátné
zvířete	zvíře	k1gNnSc2	zvíře
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
zabije	zabít	k5eAaPmIp3nS	zabít
člověk	člověk	k1gMnSc1	člověk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
tento	tento	k3xDgInSc4	tento
čin	čin	k1gInSc4	čin
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
tento	tento	k3xDgMnSc1	tento
člověk	člověk	k1gMnSc1	člověk
potrestán	potrestat	k5eAaPmNgMnS	potrestat
jako	jako	k9	jako
za	za	k7c4	za
zabití	zabití	k1gNnSc4	zabití
posvátného	posvátný	k2eAgNnSc2d1	posvátné
zvířete	zvíře	k1gNnSc2	zvíře
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
odpovědnost	odpovědnost	k1gFnSc4	odpovědnost
za	za	k7c4	za
trest	trest	k1gInSc4	trest
buď	buď	k8xC	buď
převezmou	převzít	k5eAaPmIp3nP	převzít
sami	sám	k3xTgMnPc1	sám
bozi	bůh	k1gMnPc1	bůh
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
jakémusi	jakýsi	k3yIgNnSc3	jakýsi
samovolnému	samovolný	k2eAgNnSc3d1	samovolné
<g/>
,	,	kIx,	,
nikým	nikdo	k3yNnSc7	nikdo
neřiditelnému	řiditelný	k2eNgInSc3d1	neřiditelný
trestu	trest	k1gInSc3	trest
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velmi	velmi	k6eAd1	velmi
zvláštní	zvláštní	k2eAgFnPc1d1	zvláštní
jsou	být	k5eAaImIp3nP	být
informace	informace	k1gFnPc1	informace
vyplývající	vyplývající	k2eAgFnSc1d1	vyplývající
z	z	k7c2	z
díla	dílo	k1gNnSc2	dílo
Gaia	Gaius	k1gMnSc2	Gaius
Plinia	Plinium	k1gNnSc2	Plinium
Secunda	Secund	k1gMnSc2	Secund
který	který	k3yRgMnSc1	který
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
drak	drak	k1gMnSc1	drak
loví	lovit	k5eAaImIp3nP	lovit
slony	slon	k1gMnPc7	slon
přičemž	přičemž	k6eAd1	přičemž
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
zabit	zabít	k5eAaPmNgMnS	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Zrovna	zrovna	k6eAd1	zrovna
tak	tak	k9	tak
sloni	slon	k1gMnPc1	slon
se	se	k3xPyFc4	se
pokoušejí	pokoušet	k5eAaImIp3nP	pokoušet
lovit	lovit	k5eAaImF	lovit
draky	drak	k1gMnPc4	drak
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
představ	představa	k1gFnPc2	představa
Plinia	Plinium	k1gNnSc2	Plinium
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
drak	drak	k1gMnSc1	drak
je	být	k5eAaImIp3nS	být
menší	malý	k2eAgMnSc1d2	menší
než	než	k8xS	než
slon	slon	k1gMnSc1	slon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
je	být	k5eAaImIp3nS	být
motiv	motiv	k1gInSc1	motiv
zabití	zabití	k1gNnSc1	zabití
draka	drak	k1gMnSc2	drak
ještě	ještě	k9	ještě
častější	častý	k2eAgInPc1d2	častější
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
středověká	středověký	k2eAgFnSc1d1	středověká
Evropa	Evropa	k1gFnSc1	Evropa
nezná	neznat	k5eAaImIp3nS	neznat
nesmrtelné	smrtelný	k2eNgInPc4d1	nesmrtelný
draky	drak	k1gInPc4	drak
a	a	k8xC	a
takový	takový	k3xDgInSc1	takový
čin	čin	k1gInSc1	čin
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
hodnocen	hodnotit	k5eAaImNgInS	hodnotit
kladně	kladně	k6eAd1	kladně
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgMnPc4	ten
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
draka	drak	k1gMnSc4	drak
zabili	zabít	k5eAaPmAgMnP	zabít
<g/>
,	,	kIx,	,
zve	zvát	k5eAaImIp3nS	zvát
drakobijci	drakobijce	k1gMnSc3	drakobijce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Slovanský	slovanský	k2eAgInSc1d1	slovanský
drak	drak	k1gInSc1	drak
====	====	k?	====
</s>
</p>
<p>
<s>
Zmej	Zmej	k1gFnSc1	Zmej
je	být	k5eAaImIp3nS	být
bájná	bájný	k2eAgFnSc1d1	bájná
bytost	bytost	k1gFnSc1	bytost
ve	v	k7c6	v
slovanské	slovanský	k2eAgFnSc6d1	Slovanská
mytologii	mytologie	k1gFnSc6	mytologie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ruské	ruský	k2eAgFnSc6d1	ruská
mytologii	mytologie	k1gFnSc6	mytologie
ho	on	k3xPp3gMnSc4	on
lidé	člověk	k1gMnPc1	člověk
nazývali	nazývat	k5eAaImAgMnP	nazývat
zmej	zmej	k1gFnSc4	zmej
garynyč	garynyč	k1gFnSc4	garynyč
<g/>
,	,	kIx,	,
v	v	k7c4	v
bulharské	bulharský	k2eAgInPc4d1	bulharský
zmej	zmej	k1gInSc4	zmej
<g/>
,	,	kIx,	,
ve	v	k7c6	v
staroslovanské	staroslovanský	k2eAgFnSc6d1	staroslovanská
a	a	k8xC	a
ukrajinské	ukrajinský	k2eAgFnSc6d1	ukrajinská
zmaj	zmaj	k1gMnSc1	zmaj
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
v	v	k7c6	v
srbské	srbský	k2eAgFnSc6d1	Srbská
<g/>
,	,	kIx,	,
chorvatské	chorvatský	k2eAgFnSc6d1	chorvatská
<g/>
,	,	kIx,	,
bosenské	bosenský	k2eAgFnSc6d1	bosenská
a	a	k8xC	a
slovinské	slovinský	k2eAgFnSc6d1	slovinská
<g/>
,	,	kIx,	,
a	a	k8xC	a
żmij	żmij	k1gFnSc1	żmij
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Základ	základ	k1gInSc1	základ
slova	slovo	k1gNnSc2	slovo
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
ruského	ruský	k2eAgMnSc2d1	ruský
"	"	kIx"	"
<g/>
zmeja	zmejum	k1gNnSc2	zmejum
<g/>
"	"	kIx"	"
–	–	k?	–
had	had	k1gMnSc1	had
<g/>
,	,	kIx,	,
zmije	zmije	k1gFnSc1	zmije
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
také	také	k9	také
pojem	pojem	k1gInSc1	pojem
smok	smok	k1gMnSc1	smok
(	(	kIx(	(
<g/>
šmok	šmok	k1gMnSc1	šmok
<g/>
,	,	kIx,	,
zmok	zmok	k1gMnSc1	zmok
či	či	k8xC	či
zmek	zmek	k1gMnSc1	zmek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
jakýsi	jakýsi	k3yIgInSc4	jakýsi
dračí	dračí	k2eAgInSc4d1	dračí
předstupeň	předstupeň	k1gInSc4	předstupeň
<g/>
,	,	kIx,	,
dráče	dráče	k1gNnSc4	dráče
<g/>
.	.	kIx.	.
</s>
<s>
Smok	smok	k1gMnSc1	smok
se	se	k3xPyFc4	se
prý	prý	k9	prý
vyvine	vyvinout	k5eAaPmIp3nS	vyvinout
z	z	k7c2	z
hada	had	k1gMnSc2	had
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
žádný	žádný	k1gMnSc1	žádný
člověk	člověk	k1gMnSc1	člověk
nespatřil	spatřit	k5eNaPmAgMnS	spatřit
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
sedmi	sedm	k4xCc2	sedm
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
slova	slovo	k1gNnSc2	slovo
zmaj	zmaj	k1gMnSc1	zmaj
i	i	k8xC	i
smok	smok	k1gMnSc1	smok
jsou	být	k5eAaImIp3nP	být
obecně	obecně	k6eAd1	obecně
slovanského	slovanský	k2eAgInSc2d1	slovanský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
a	a	k8xC	a
např.	např.	kA	např.
v	v	k7c6	v
polštině	polština	k1gFnSc6	polština
smok	smok	k1gMnSc1	smok
označuje	označovat	k5eAaImIp3nS	označovat
jakéhokoli	jakýkoli	k3yIgMnSc4	jakýkoli
draka	drak	k1gMnSc4	drak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
západoslovanském	západoslovanský	k2eAgInSc6d1	západoslovanský
okruhu	okruh	k1gInSc6	okruh
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
známé	známý	k2eAgFnPc4d1	známá
pověsti	pověst	k1gFnPc4	pověst
například	například	k6eAd1	například
ta	ten	k3xDgNnPc1	ten
o	o	k7c6	o
krakovském	krakovský	k2eAgMnSc6d1	krakovský
drakovi	drak	k1gMnSc6	drak
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
prostředí	prostředí	k1gNnSc6	prostředí
drak	drak	k1gMnSc1	drak
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
například	například	k6eAd1	například
v	v	k7c6	v
pověsti	pověst	k1gFnSc6	pověst
o	o	k7c6	o
Brunclíkovi	Brunclík	k1gMnSc6	Brunclík
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
drak	drak	k1gInSc4	drak
jménem	jméno	k1gNnSc7	jméno
Bazilišek	baziliška	k1gFnPc2	baziliška
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Dálný	dálný	k2eAgInSc4d1	dálný
východ	východ	k1gInSc4	východ
===	===	k?	===
</s>
</p>
<p>
<s>
Orientální	orientální	k2eAgInSc1d1	orientální
drak	drak	k1gInSc1	drak
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
zobrazován	zobrazován	k2eAgMnSc1d1	zobrazován
jako	jako	k8xC	jako
tvor	tvor	k1gMnSc1	tvor
s	s	k7c7	s
jednou	jeden	k4xCgFnSc7	jeden
velkou	velký	k2eAgFnSc7d1	velká
hlavou	hlava	k1gFnSc7	hlava
a	a	k8xC	a
protáhlým	protáhlý	k2eAgNnSc7d1	protáhlé
dlouhým	dlouhý	k2eAgNnSc7d1	dlouhé
tělem	tělo	k1gNnSc7	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
stavba	stavba	k1gFnSc1	stavba
těla	tělo	k1gNnSc2	tělo
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
hadů	had	k1gMnPc2	had
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
má	mít	k5eAaImIp3nS	mít
čtyři	čtyři	k4xCgFnPc4	čtyři
končetiny	končetina	k1gFnPc4	končetina
zakončené	zakončený	k2eAgFnPc4d1	zakončená
ostrými	ostrý	k2eAgInPc7d1	ostrý
pařáty	pařát	k1gInPc7	pařát
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
mu	on	k3xPp3gNnSc3	on
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
chůzi	chůze	k1gFnSc4	chůze
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
mají	mít	k5eAaImIp3nP	mít
asijští	asijský	k2eAgMnPc1d1	asijský
draci	drak	k1gMnPc1	drak
obývat	obývat	k5eAaImF	obývat
převážně	převážně	k6eAd1	převážně
vodní	vodní	k2eAgNnSc4d1	vodní
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
často	často	k6eAd1	často
zobrazován	zobrazován	k2eAgInSc1d1	zobrazován
ocas	ocas	k1gInSc1	ocas
zakončený	zakončený	k2eAgInSc1d1	zakončený
ploutví	ploutev	k1gFnSc7	ploutev
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
jim	on	k3xPp3gMnPc3	on
má	mít	k5eAaImIp3nS	mít
umožňovat	umožňovat	k5eAaImF	umožňovat
snazší	snadný	k2eAgInSc1d2	snadnější
pohyb	pohyb	k1gInSc1	pohyb
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
i	i	k9	i
tvar	tvar	k1gInSc4	tvar
jejich	jejich	k3xOp3gNnSc2	jejich
těla	tělo	k1gNnSc2	tělo
je	být	k5eAaImIp3nS	být
uzpůsoben	uzpůsoben	k2eAgInSc1d1	uzpůsoben
k	k	k7c3	k
životu	život	k1gInSc3	život
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
hlavou	hlava	k1gFnSc7	hlava
se	se	k3xPyFc4	se
občas	občas	k6eAd1	občas
nacházejí	nacházet	k5eAaImIp3nP	nacházet
malá	malý	k2eAgNnPc4d1	malé
zakrnělá	zakrnělý	k2eAgNnPc4d1	zakrnělé
křídla	křídlo	k1gNnPc4	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
zobrazených	zobrazený	k2eAgInPc2d1	zobrazený
draků	drak	k1gInPc2	drak
křídla	křídlo	k1gNnSc2	křídlo
vůbec	vůbec	k9	vůbec
nemá	mít	k5eNaImIp3nS	mít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
přesto	přesto	k8xC	přesto
dokáží	dokázat	k5eAaPmIp3nP	dokázat
létat	létat	k5eAaImF	létat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
draků	drak	k1gInPc2	drak
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
zobrazovány	zobrazován	k2eAgInPc4d1	zobrazován
blesky	blesk	k1gInPc4	blesk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dle	dle	k7c2	dle
pověstí	pověst	k1gFnPc2	pověst
jsou	být	k5eAaImIp3nP	být
draci	drak	k1gMnPc1	drak
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Asii	Asie	k1gFnSc6	Asie
spojeni	spojen	k2eAgMnPc1d1	spojen
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
a	a	k8xC	a
větrem	vítr	k1gInSc7	vítr
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
jejich	jejich	k3xOp3gMnPc1	jejich
evropští	evropský	k2eAgMnPc1d1	evropský
příbuzní	příbuzný	k1gMnPc1	příbuzný
mají	mít	k5eAaImIp3nP	mít
schopnost	schopnost	k1gFnSc4	schopnost
chrlit	chrlit	k5eAaImF	chrlit
oheň	oheň	k1gInSc4	oheň
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
využívána	využívat	k5eAaImNgFnS	využívat
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
zřídka	zřídka	k6eAd1	zřídka
<g/>
,	,	kIx,	,
namísto	namísto	k7c2	namísto
toho	ten	k3xDgNnSc2	ten
například	například	k6eAd1	například
přivolávají	přivolávat	k5eAaImIp3nP	přivolávat
déšť	déšť	k1gInSc4	déšť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Čína	Čína	k1gFnSc1	Čína
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
má	mít	k5eAaImIp3nS	mít
uctívání	uctívání	k1gNnSc1	uctívání
draků	drak	k1gInPc2	drak
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
tradici	tradice	k1gFnSc4	tradice
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
společnosti	společnost	k1gFnSc6	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Draci	drak	k1gMnPc1	drak
byli	být	k5eAaImAgMnP	být
spojováni	spojován	k2eAgMnPc1d1	spojován
s	s	k7c7	s
vodními	vodní	k2eAgInPc7d1	vodní
a	a	k8xC	a
vzdušnými	vzdušný	k2eAgInPc7d1	vzdušný
živly	živel	k1gInPc7	živel
<g/>
,	,	kIx,	,
s	s	k7c7	s
dobrem	dobro	k1gNnSc7	dobro
a	a	k8xC	a
s	s	k7c7	s
vrcholným	vrcholný	k2eAgMnSc7d1	vrcholný
představitelem	představitel	k1gMnSc7	představitel
řádu	řád	k1gInSc2	řád
<g/>
.	.	kIx.	.
</s>
<s>
Čínské	čínský	k2eAgFnPc1d1	čínská
vládnoucí	vládnoucí	k2eAgFnPc1d1	vládnoucí
dynastie	dynastie	k1gFnPc1	dynastie
se	se	k3xPyFc4	se
považovaly	považovat	k5eAaImAgFnP	považovat
za	za	k7c4	za
potomky	potomek	k1gMnPc4	potomek
nebeského	nebeský	k2eAgMnSc4d1	nebeský
draka	drak	k1gMnSc4	drak
a	a	k8xC	a
spojovaly	spojovat	k5eAaImAgInP	spojovat
s	s	k7c7	s
draky	drak	k1gInPc7	drak
svůj	svůj	k3xOyFgInSc4	svůj
vznik	vznik	k1gInSc4	vznik
<g/>
.	.	kIx.	.
</s>
<s>
Drak	drak	k1gMnSc1	drak
figuroval	figurovat	k5eAaImAgMnS	figurovat
jako	jako	k9	jako
ochránce	ochránce	k1gMnSc1	ochránce
rolníků	rolník	k1gMnPc2	rolník
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
věřilo	věřit	k5eAaImAgNnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
magickou	magický	k2eAgFnSc4d1	magická
sílu	síla	k1gFnSc4	síla
přivolávat	přivolávat	k5eAaImF	přivolávat
srážky	srážka	k1gFnPc4	srážka
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
bylo	být	k5eAaImAgNnS	být
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
Číně	Čína	k1gFnSc6	Čína
vybudováno	vybudovat	k5eAaPmNgNnS	vybudovat
nespočet	nespočet	k1gInSc1	nespočet
chrámů	chrám	k1gInPc2	chrám
se	s	k7c7	s
sochami	socha	k1gFnPc7	socha
draků	drak	k1gInPc2	drak
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yIgFnPc2	který
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
konaly	konat	k5eAaImAgFnP	konat
lidové	lidový	k2eAgInPc4d1	lidový
obřady	obřad	k1gInPc4	obřad
s	s	k7c7	s
oběťmi	oběť	k1gFnPc7	oběť
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
asijskou	asijský	k2eAgFnSc4d1	asijská
mytologii	mytologie	k1gFnSc4	mytologie
drak	drak	k1gMnSc1	drak
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
jaro	jaro	k1gNnSc4	jaro
a	a	k8xC	a
znamení	znamení	k1gNnSc4	znamení
zrodu	zrod	k1gInSc2	zrod
<g/>
.	.	kIx.	.
</s>
<s>
Spojován	spojován	k2eAgInSc1d1	spojován
je	být	k5eAaImIp3nS	být
se	se	k3xPyFc4	se
symbolem	symbol	k1gInSc7	symbol
jang	janga	k1gFnPc2	janga
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
dech	dech	k1gInSc1	dech
má	mít	k5eAaImIp3nS	mít
mít	mít	k5eAaImF	mít
léčitelské	léčitelský	k2eAgFnPc4d1	léčitelská
a	a	k8xC	a
oživovací	oživovací	k2eAgFnPc4d1	oživovací
schopnosti	schopnost	k1gFnPc4	schopnost
<g/>
.	.	kIx.	.
<g/>
Drak	drak	k1gMnSc1	drak
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgMnSc7	jeden
ze	z	k7c2	z
znamení	znamení	k1gNnSc2	znamení
čínského	čínský	k2eAgInSc2d1	čínský
zvěrokruhu	zvěrokruh	k1gInSc2	zvěrokruh
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
věří	věřit	k5eAaImIp3nS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
lidé	člověk	k1gMnPc1	člověk
narození	narození	k1gNnSc2	narození
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
znamení	znamení	k1gNnSc6	znamení
mají	mít	k5eAaImIp3nP	mít
vladařské	vladařský	k2eAgInPc1d1	vladařský
předpoklady	předpoklad	k1gInPc1	předpoklad
<g/>
.	.	kIx.	.
<g/>
Popisy	popis	k1gInPc1	popis
asijského	asijský	k2eAgMnSc2d1	asijský
draka	drak	k1gMnSc2	drak
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
tvoru	tvor	k1gMnSc6	tvor
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
má	mít	k5eAaImIp3nS	mít
obrovské	obrovský	k2eAgNnSc4d1	obrovské
tělo	tělo	k1gNnSc4	tělo
<g/>
,	,	kIx,	,
ocas	ocas	k1gInSc4	ocas
ještěrky	ještěrka	k1gFnPc4	ještěrka
a	a	k8xC	a
čtyři	čtyři	k4xCgFnPc4	čtyři
velké	velký	k2eAgFnPc4d1	velká
nohy	noha	k1gFnPc4	noha
s	s	k7c7	s
drápy	dráp	k1gInPc7	dráp
<g/>
.	.	kIx.	.
</s>
<s>
Drak	drak	k1gMnSc1	drak
asijský	asijský	k2eAgMnSc1d1	asijský
mívá	mívat	k5eAaImIp3nS	mívat
vyobrazená	vyobrazený	k2eAgFnSc1d1	vyobrazená
jen	jen	k9	jen
malá	malý	k2eAgNnPc4d1	malé
zakrnělá	zakrnělý	k2eAgNnPc4d1	zakrnělé
křídla	křídlo	k1gNnPc4	křídlo
<g/>
,	,	kIx,	,
či	či	k8xC	či
vůbec	vůbec	k9	vůbec
žádná	žádný	k3yNgFnSc1	žádný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
tak	tak	k9	tak
má	mít	k5eAaImIp3nS	mít
disponovat	disponovat	k5eAaBmF	disponovat
schopností	schopnost	k1gFnSc7	schopnost
létat	létat	k5eAaImF	létat
<g/>
,	,	kIx,	,
čehož	což	k3yRnSc2	což
má	mít	k5eAaImIp3nS	mít
docilovat	docilovat	k5eAaImF	docilovat
používáním	používání	k1gNnSc7	používání
záhadné	záhadný	k2eAgFnSc2d1	záhadná
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Vyjma	vyjma	k7c2	vyjma
schopnosti	schopnost	k1gFnSc2	schopnost
chrilt	chrilt	k2eAgInSc4d1	chrilt
oheň	oheň	k1gInSc4	oheň
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
využívá	využívat	k5eAaImIp3nS	využívat
jen	jen	k9	jen
zřídka	zřídka	k6eAd1	zřídka
<g/>
,	,	kIx,	,
umí	umět	k5eAaImIp3nS	umět
ovládat	ovládat	k5eAaImF	ovládat
déšť	déšť	k1gInSc4	déšť
a	a	k8xC	a
měnit	měnit	k5eAaImF	měnit
svoji	svůj	k3xOyFgFnSc4	svůj
velikost	velikost	k1gFnSc4	velikost
od	od	k7c2	od
velikosti	velikost	k1gFnSc2	velikost
ještěrky	ještěrka	k1gFnSc2	ještěrka
až	až	k9	až
po	po	k7c4	po
rozměry	rozměr	k1gInPc4	rozměr
hory	hora	k1gFnSc2	hora
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
je	být	k5eAaImIp3nS	být
také	také	k9	také
schopnost	schopnost	k1gFnSc4	schopnost
učinit	učinit	k5eAaImF	učinit
se	se	k3xPyFc4	se
neviditelným	viditelný	k2eNgMnSc7d1	Neviditelný
<g/>
,	,	kIx,	,
čehož	což	k3yRnSc2	což
draci	drak	k1gMnPc1	drak
údajně	údajně	k6eAd1	údajně
využívají	využívat	k5eAaPmIp3nP	využívat
pro	pro	k7c4	pro
kontrolu	kontrola	k1gFnSc4	kontrola
svých	svůj	k3xOyFgNnPc2	svůj
teritorií	teritorium	k1gNnPc2	teritorium
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
čínských	čínský	k2eAgFnPc2d1	čínská
hierarchií	hierarchie	k1gFnPc2	hierarchie
dělí	dělit	k5eAaImIp3nP	dělit
draky	drak	k1gInPc1	drak
do	do	k7c2	do
čtyř	čtyři	k4xCgFnPc2	čtyři
skupin	skupina	k1gFnPc2	skupina
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Nebeští	nebeský	k2eAgMnPc1d1	nebeský
draci	drak	k1gMnPc1	drak
(	(	kIx(	(
<g/>
天	天	k?	天
<g/>
,	,	kIx,	,
tchien-lung	tchienung	k1gInSc1	tchien-lung
<g/>
)	)	kIx)	)
–	–	k?	–
střeží	střežit	k5eAaImIp3nS	střežit
zemi	zem	k1gFnSc3	zem
bohů	bůh	k1gMnPc2	bůh
a	a	k8xC	a
slouží	sloužit	k5eAaImIp3nS	sloužit
jim	on	k3xPp3gMnPc3	on
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Draci	drak	k1gMnPc1	drak
duchové	duchový	k2eAgFnSc2d1	duchová
(	(	kIx(	(
<g/>
神	神	k?	神
<g/>
,	,	kIx,	,
šen-lung	šenung	k1gInSc1	šen-lung
<g/>
)	)	kIx)	)
–	–	k?	–
mají	mít	k5eAaImIp3nP	mít
schopnost	schopnost	k1gFnSc4	schopnost
vyvolat	vyvolat	k5eAaPmF	vyvolat
déšť	déšť	k1gInSc4	déšť
a	a	k8xC	a
vítr	vítr	k1gInSc4	vítr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pozemští	pozemský	k2eAgMnPc1d1	pozemský
draci	drak	k1gMnPc1	drak
(	(	kIx(	(
<g/>
地	地	k?	地
<g/>
,	,	kIx,	,
ti-lung	tiung	k1gInSc1	ti-lung
<g/>
)	)	kIx)	)
–	–	k?	–
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
mořích	moře	k1gNnPc6	moře
a	a	k8xC	a
řekách	řeka	k1gFnPc6	řeka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Draci	drak	k1gMnPc1	drak
strážci	strážce	k1gMnPc1	strážce
pokladů	poklad	k1gInPc2	poklad
(	(	kIx(	(
<g/>
伏	伏	k?	伏
<g/>
,	,	kIx,	,
fu-cchang-lung	fuchangung	k1gInSc1	fu-cchang-lung
<g/>
)	)	kIx)	)
–	–	k?	–
hlídají	hlídat	k5eAaImIp3nP	hlídat
poklady	poklad	k1gInPc1	poklad
<g/>
.	.	kIx.	.
<g/>
Draci	drak	k1gMnPc1	drak
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
hodní	hodný	k2eAgMnPc1d1	hodný
a	a	k8xC	a
přátelští	přátelský	k2eAgMnPc1d1	přátelský
<g/>
,	,	kIx,	,
dovedou	dovést	k5eAaPmIp3nP	dovést
se	se	k3xPyFc4	se
proměnit	proměnit	k5eAaPmF	proměnit
v	v	k7c4	v
lidi	člověk	k1gMnPc4	člověk
i	i	k8xC	i
jiná	jiný	k2eAgNnPc1d1	jiné
zvířata	zvíře	k1gNnPc1	zvíře
(	(	kIx(	(
<g/>
symbol	symbol	k1gInSc1	symbol
císařské	císařský	k2eAgFnSc2d1	císařská
moci	moc	k1gFnSc2	moc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
potah	potah	k1gInSc1	potah
bohů	bůh	k1gMnPc2	bůh
se	se	k3xPyFc4	se
nejvýrazněji	výrazně	k6eAd3	výrazně
projevili	projevit	k5eAaPmAgMnP	projevit
u	u	k7c2	u
Š-che	Šh	k1gFnSc2	Š-ch
–	–	k?	–
bohyně	bohyně	k1gFnSc2	bohyně
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vyjíždí	vyjíždět	k5eAaImIp3nS	vyjíždět
každé	každý	k3xTgNnSc1	každý
ráno	ráno	k6eAd1	ráno
na	na	k7c6	na
voze	vůz	k1gInSc6	vůz
taženém	tažený	k2eAgInSc6d1	tažený
šesti	šest	k4xCc7	šest
draky	drak	k1gInPc7	drak
a	a	k8xC	a
u	u	k7c2	u
západní	západní	k2eAgFnSc2d1	západní
hory	hora	k1gFnSc2	hora
sjede	sjet	k5eAaPmIp3nS	sjet
do	do	k7c2	do
propasti	propast	k1gFnSc2	propast
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
vládce	vládce	k1gMnSc2	vládce
jara	jaro	k1gNnSc2	jaro
Tchaj-che	Tchajh	k1gMnSc2	Tchaj-ch
(	(	kIx(	(
<g/>
východ	východ	k1gInSc1	východ
<g/>
)	)	kIx)	)
s	s	k7c7	s
bohem	bůh	k1gMnSc7	bůh
stromu	strom	k1gInSc2	strom
Ču	Ču	k1gFnSc1	Ču
mangem	mango	k1gNnSc7	mango
(	(	kIx(	(
<g/>
ptačí	ptačí	k2eAgNnSc1d1	ptačí
tělo	tělo	k1gNnSc1	tělo
<g/>
)	)	kIx)	)
létá	létat	k5eAaImIp3nS	létat
na	na	k7c6	na
dvou	dva	k4xCgInPc6	dva
dracích	drak	k1gInPc6	drak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velmi	velmi	k6eAd1	velmi
podobné	podobný	k2eAgFnPc4d1	podobná
představy	představa	k1gFnPc4	představa
o	o	k7c6	o
dracích	drak	k1gInPc6	drak
jsou	být	k5eAaImIp3nP	být
rozšířeny	rozšířit	k5eAaPmNgFnP	rozšířit
též	též	k9	též
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
a	a	k8xC	a
Koreji	Korea	k1gFnSc6	Korea
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Drak	drak	k1gInSc1	drak
jako	jako	k8xC	jako
symbol	symbol	k1gInSc1	symbol
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Heraldika	heraldika	k1gFnSc1	heraldika
===	===	k?	===
</s>
</p>
<p>
<s>
Mezi	mezi	k7c7	mezi
obecnými	obecný	k2eAgFnPc7d1	obecná
figurami	figura	k1gFnPc7	figura
se	se	k3xPyFc4	se
v	v	k7c6	v
heraldice	heraldika	k1gFnSc6	heraldika
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
především	především	k9	především
drak	drak	k1gInSc1	drak
a	a	k8xC	a
saň	saň	k1gFnSc1	saň
<g/>
.	.	kIx.	.
</s>
<s>
Drak	drak	k1gInSc1	drak
(	(	kIx(	(
<g/>
v	v	k7c6	v
anglosaském	anglosaský	k2eAgNnSc6d1	anglosaské
prostředí	prostředí	k1gNnSc6	prostředí
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
wyvern	wyvern	k1gInSc1	wyvern
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgFnPc4	dva
nohy	noha	k1gFnPc4	noha
<g/>
,	,	kIx,	,
pár	pár	k4xCyI	pár
křídel	křídlo	k1gNnPc2	křídlo
a	a	k8xC	a
obvykle	obvykle	k6eAd1	obvykle
chrlí	chrlit	k5eAaImIp3nP	chrlit
</s>
</p>
<p>
<s>
oheň	oheň	k1gInSc1	oheň
<g/>
.	.	kIx.	.
</s>
<s>
Saň	saň	k1gFnSc1	saň
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
dragon	dragon	k?	dragon
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
čtyři	čtyři	k4xCgFnPc4	čtyři
nohy	noha	k1gFnPc4	noha
<g/>
,	,	kIx,	,
pár	pár	k4xCyI	pár
křídel	křídlo	k1gNnPc2	křídlo
a	a	k8xC	a
obvykle	obvykle	k6eAd1	obvykle
oheň	oheň	k1gInSc1	oheň
nechrlí	chrlit	k5eNaImIp3nS	chrlit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
saně	saně	k1gFnSc2	saně
a	a	k8xC	a
draka	drak	k1gMnSc2	drak
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
ještě	ještě	k9	ještě
další	další	k2eAgMnPc1d1	další
tvorové	tvor	k1gMnPc1	tvor
<g/>
,	,	kIx,	,
sdílející	sdílející	k2eAgMnPc1d1	sdílející
s	s	k7c7	s
draky	drak	k1gMnPc7	drak
určité	určitý	k2eAgInPc4d1	určitý
rysy	rys	k1gInPc4	rys
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
kupříkladu	kupříkladu	k6eAd1	kupříkladu
bazilišek	bazilišek	k1gMnSc1	bazilišek
(	(	kIx(	(
<g/>
tvor	tvor	k1gMnSc1	tvor
s	s	k7c7	s
hadím	hadí	k2eAgNnSc7d1	hadí
tělem	tělo	k1gNnSc7	tělo
a	a	k8xC	a
ocasem	ocas	k1gInSc7	ocas
<g/>
,	,	kIx,	,
jedním	jeden	k4xCgInSc7	jeden
párem	pár	k1gInSc7	pár
nohou	noha	k1gFnPc2	noha
<g/>
,	,	kIx,	,
kohoutí	kohoutí	k2eAgFnSc7d1	kohoutí
hlavou	hlava	k1gFnSc7	hlava
a	a	k8xC	a
dračími	dračí	k2eAgNnPc7d1	dračí
křídly	křídlo	k1gNnPc7	křídlo
<g/>
,	,	kIx,	,
v	v	k7c6	v
anglosaské	anglosaský	k2eAgFnSc6d1	anglosaská
terminologie	terminologie	k1gFnSc2	terminologie
cockatirice	cockatirika	k1gFnSc6	cockatirika
<g/>
)	)	kIx)	)
či	či	k8xC	či
panter	panter	k1gInSc1	panter
(	(	kIx(	(
<g/>
zvíře	zvíře	k1gNnSc1	zvíře
chrlící	chrlící	k2eAgNnSc1d1	chrlící
oheň	oheň	k1gInSc4	oheň
a	a	k8xC	a
podobné	podobný	k2eAgInPc4d1	podobný
lvu	lev	k1gInSc6	lev
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Spíše	spíše	k9	spíše
mimo	mimo	k7c4	mimo
české	český	k2eAgNnSc4d1	české
prostředí	prostředí	k1gNnSc4	prostředí
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
i	i	k9	i
další	další	k2eAgFnPc1d1	další
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
kupříkladu	kupříkladu	k6eAd1	kupříkladu
amfisbéna	amfisbéna	k6eAd1	amfisbéna
(	(	kIx(	(
<g/>
drakovité	drakovitý	k2eAgNnSc1d1	drakovitý
stvoření	stvoření	k1gNnSc1	stvoření
s	s	k7c7	s
druhou	druhý	k4xOgFnSc7	druhý
hlavou	hlava	k1gFnSc7	hlava
na	na	k7c6	na
ocase	ocas	k1gInSc6	ocas
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
amfiptéra	amfiptér	k1gMnSc2	amfiptér
(	(	kIx(	(
<g/>
okřídlený	okřídlený	k2eAgMnSc1d1	okřídlený
had	had	k1gMnSc1	had
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hydra	hydra	k1gFnSc1	hydra
(	(	kIx(	(
<g/>
vícehlavá	vícehlavý	k2eAgFnSc1d1	vícehlavá
saň	saň	k1gFnSc1	saň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
lindworm	lindworm	k1gInSc1	lindworm
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
anglické	anglický	k2eAgNnSc1d1	anglické
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
draka	drak	k1gMnSc4	drak
bez	bez	k7c2	bez
křídel	křídlo	k1gNnPc2	křídlo
–	–	k?	–
nezaměňovat	zaměňovat	k5eNaImF	zaměňovat
s	s	k7c7	s
německým	německý	k2eAgInSc7d1	německý
"	"	kIx"	"
<g/>
lindwurm	lindwurm	k1gInSc1	lindwurm
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
drak	drak	k1gInSc4	drak
obecně	obecně	k6eAd1	obecně
<g/>
)	)	kIx)	)
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Saň	saň	k1gFnSc1	saň
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
ve	v	k7c6	v
státním	státní	k2eAgInSc6d1	státní
znaku	znak	k1gInSc6	znak
Ruské	ruský	k2eAgFnSc2d1	ruská
federace	federace	k1gFnSc2	federace
a	a	k8xC	a
Gruzie	Gruzie	k1gFnSc2	Gruzie
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
přemožený	přemožený	k2eAgMnSc1d1	přemožený
protivník	protivník	k1gMnSc1	protivník
sv.	sv.	kA	sv.
Jiřího	Jiří	k1gMnSc2	Jiří
<g/>
)	)	kIx)	)
a	a	k8xC	a
zejména	zejména	k9	zejména
ve	v	k7c6	v
znacích	znak	k1gInPc6	znak
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
znaku	znak	k1gInSc6	znak
má	mít	k5eAaImIp3nS	mít
saň	saň	k1gFnSc4	saň
slovinské	slovinský	k2eAgNnSc1d1	slovinské
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Lublaň	Lublaň	k1gFnSc1	Lublaň
<g/>
,	,	kIx,	,
z	z	k7c2	z
českých	český	k2eAgNnPc2d1	české
měst	město	k1gNnPc2	město
je	být	k5eAaImIp3nS	být
drak	drak	k1gInSc1	drak
znám	znát	k5eAaImIp1nS	znát
např.	např.	kA	např.
v	v	k7c6	v
městské	městský	k2eAgFnSc6d1	městská
pečeti	pečeť	k1gFnSc6	pečeť
Brna	Brno	k1gNnSc2	Brno
(	(	kIx(	(
<g/>
krokodýl	krokodýl	k1gMnSc1	krokodýl
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
v	v	k7c6	v
městských	městský	k2eAgInPc6d1	městský
znacích	znak	k1gInPc6	znak
Kolína	Kolín	k1gInSc2	Kolín
či	či	k8xC	či
Trutnova	Trutnov	k1gInSc2	Trutnov
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
také	také	k9	také
doprovázen	doprovázet	k5eAaImNgInS	doprovázet
postavou	postava	k1gFnSc7	postava
sv.	sv.	kA	sv.
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
např.	např.	kA	např.
u	u	k7c2	u
Vysokého	vysoký	k2eAgNnSc2d1	vysoké
Mýta	mýto	k1gNnSc2	mýto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vlajky	vlajka	k1gFnSc2	vlajka
===	===	k?	===
</s>
</p>
<p>
<s>
Symbol	symbol	k1gInSc1	symbol
draka	drak	k1gMnSc2	drak
či	či	k8xC	či
saně	saně	k1gFnSc2	saně
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
též	též	k9	též
na	na	k7c6	na
vlajkách	vlajka	k1gFnPc6	vlajka
Walesu	Wales	k1gInSc2	Wales
a	a	k8xC	a
Bhútánu	Bhútán	k1gInSc2	Bhútán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Drak	drak	k1gMnSc1	drak
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
kultuře	kultura	k1gFnSc6	kultura
==	==	k?	==
</s>
</p>
<p>
<s>
Drak	drak	k1gInSc1	drak
začal	začít	k5eAaPmAgInS	začít
pronikat	pronikat	k5eAaImF	pronikat
do	do	k7c2	do
literatury	literatura	k1gFnSc2	literatura
<g/>
,	,	kIx,	,
především	především	k9	především
fantasy	fantas	k1gInPc1	fantas
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
narušení	narušení	k1gNnSc3	narušení
pravidel	pravidlo	k1gNnPc2	pravidlo
rozlišování	rozlišování	k1gNnSc1	rozlišování
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
drak	drak	k1gInSc1	drak
může	moct	k5eAaImIp3nS	moct
a	a	k8xC	a
čeho	co	k3yInSc2	co
schopen	schopen	k2eAgMnSc1d1	schopen
není	být	k5eNaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Veškerá	veškerý	k3xTgNnPc1	veškerý
pravidla	pravidlo	k1gNnPc1	pravidlo
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
narušena	narušit	k5eAaPmNgFnS	narušit
filmem	film	k1gInSc7	film
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
drak	drak	k1gInSc1	drak
často	často	k6eAd1	často
nevystupuje	vystupovat	k5eNaImIp3nS	vystupovat
jako	jako	k8xS	jako
bájné	bájný	k2eAgNnSc1d1	bájné
zvíře	zvíře	k1gNnSc1	zvíře
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jako	jako	k8xS	jako
přihlouplý	přihlouplý	k2eAgMnSc1d1	přihlouplý
vraždící	vraždící	k2eAgMnSc1d1	vraždící
maniak	maniak	k1gMnSc1	maniak
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
pro	pro	k7c4	pro
své	svůj	k3xOyFgNnSc4	svůj
jednání	jednání	k1gNnSc4	jednání
zpravidla	zpravidla	k6eAd1	zpravidla
nemá	mít	k5eNaImIp3nS	mít
ani	ani	k8xC	ani
motiv	motiv	k1gInSc4	motiv
ani	ani	k8xC	ani
jasný	jasný	k2eAgInSc4d1	jasný
příkaz	příkaz	k1gInSc4	příkaz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c4	v
fantasy	fantas	k1gInPc4	fantas
zaměřené	zaměřený	k2eAgInPc4d1	zaměřený
spíše	spíše	k9	spíše
na	na	k7c4	na
magii	magie	k1gFnSc4	magie
bývají	bývat	k5eAaImIp3nP	bývat
naopak	naopak	k6eAd1	naopak
draci	drak	k1gMnPc1	drak
inteligentní	inteligentní	k2eAgMnPc1d1	inteligentní
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
inteligentnější	inteligentní	k2eAgMnPc1d2	inteligentnější
než	než	k8xS	než
člověk	člověk	k1gMnSc1	člověk
nebo	nebo	k8xC	nebo
alespoň	alespoň	k9	alespoň
těžící	těžící	k2eAgFnSc1d1	těžící
z	z	k7c2	z
tisíciletých	tisíciletý	k2eAgFnPc2d1	tisíciletá
životních	životní	k2eAgFnPc2d1	životní
zkušeností	zkušenost	k1gFnPc2	zkušenost
<g/>
.	.	kIx.	.
</s>
<s>
Ovládají	ovládat	k5eAaImIp3nP	ovládat
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
magii	magie	k1gFnSc4	magie
přirozenou	přirozený	k2eAgFnSc4d1	přirozená
(	(	kIx(	(
<g/>
konečně	konečně	k6eAd1	konečně
<g/>
,	,	kIx,	,
každý	každý	k3xTgMnSc1	každý
letecký	letecký	k2eAgMnSc1d1	letecký
inženýr	inženýr	k1gMnSc1	inženýr
by	by	kYmCp3nS	by
vám	vy	k3xPp2nPc3	vy
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
drak	drak	k1gInSc4	drak
létat	létat	k5eAaImF	létat
nemůže	moct	k5eNaImIp3nS	moct
–	–	k?	–
rozuměj	rozumět	k5eAaImRp2nS	rozumět
bez	bez	k7c2	bez
magie	magie	k1gFnSc2	magie
<g/>
)	)	kIx)	)
i	i	k8xC	i
naučenou	naučený	k2eAgFnSc4d1	naučená
(	(	kIx(	(
<g/>
tedy	tedy	k8xC	tedy
klasická	klasický	k2eAgNnPc1d1	klasické
zaklínadla	zaklínadlo	k1gNnPc1	zaklínadlo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejnebezpečnější	bezpečný	k2eNgMnPc4d3	nejnebezpečnější
protivníky	protivník	k1gMnPc4	protivník
které	který	k3yRgNnSc1	který
můžete	moct	k5eAaImIp2nP	moct
ve	v	k7c4	v
fantasy	fantas	k1gInPc4	fantas
potkat	potkat	k5eAaPmF	potkat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nebývají	bývat	k5eNaImIp3nP	bývat
vždy	vždy	k6eAd1	vždy
zlí	zlý	k2eAgMnPc1d1	zlý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
některých	některý	k3yIgMnPc6	některý
fantasy	fantas	k1gInPc4	fantas
světech	svět	k1gInPc6	svět
–	–	k?	–
například	například	k6eAd1	například
v	v	k7c6	v
Midkemii	Midkemie	k1gFnSc6	Midkemie
–	–	k?	–
na	na	k7c6	na
sebe	sebe	k3xPyFc4	sebe
draci	drak	k1gMnPc1	drak
mohou	moct	k5eAaImIp3nP	moct
vzít	vzít	k5eAaPmF	vzít
lidskou	lidský	k2eAgFnSc4d1	lidská
podobu	podoba	k1gFnSc4	podoba
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
už	už	k6eAd1	už
pomocí	pomocí	k7c2	pomocí
zaklínadla	zaklínadlo	k1gNnSc2	zaklínadlo
nebo	nebo	k8xC	nebo
díky	díky	k7c3	díky
přirozené	přirozený	k2eAgFnSc3d1	přirozená
schopnosti	schopnost	k1gFnSc3	schopnost
změny	změna	k1gFnSc2	změna
podoby	podoba	k1gFnSc2	podoba
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
lidské	lidský	k2eAgFnSc6d1	lidská
podobě	podoba	k1gFnSc6	podoba
si	se	k3xPyFc3	se
uchovávají	uchovávat	k5eAaImIp3nP	uchovávat
svou	svůj	k3xOyFgFnSc4	svůj
sílu	síla	k1gFnSc4	síla
<g/>
,	,	kIx,	,
rychlost	rychlost	k1gFnSc4	rychlost
a	a	k8xC	a
pochopitelně	pochopitelně	k6eAd1	pochopitelně
i	i	k9	i
magické	magický	k2eAgFnSc2d1	magická
schopnosti	schopnost	k1gFnSc2	schopnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
draků	drak	k1gInPc2	drak
dštících	dštící	k2eAgInPc2d1	dštící
oheň	oheň	k1gInSc4	oheň
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
draci	drak	k1gMnPc1	drak
ledoví	ledový	k2eAgMnPc1d1	ledový
(	(	kIx(	(
<g/>
bývají	bývat	k5eAaImIp3nP	bývat
modří	modrý	k2eAgMnPc1d1	modrý
<g/>
)	)	kIx)	)
a	a	k8xC	a
draci	drak	k1gMnPc1	drak
vydechující	vydechující	k2eAgMnSc1d1	vydechující
nebo	nebo	k8xC	nebo
vyplivující	vyplivující	k2eAgInSc1d1	vyplivující
jed	jed	k1gInSc1	jed
(	(	kIx(	(
<g/>
bývají	bývat	k5eAaImIp3nP	bývat
zelení	zelení	k1gNnSc4	zelení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Draci	drak	k1gMnPc1	drak
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
i	i	k9	i
ve	v	k7c6	v
sci-fi	scii	k1gFnSc6	sci-fi
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
knihách	kniha	k1gFnPc6	kniha
o	o	k7c4	o
Pernu	perna	k1gFnSc4	perna
(	(	kIx(	(
<g/>
Anne	Anne	k1gFnSc4	Anne
McCaffrey	McCaffrea	k1gFnSc2	McCaffrea
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
o	o	k7c4	o
původně	původně	k6eAd1	původně
malé	malý	k2eAgMnPc4d1	malý
tvory	tvor	k1gMnPc4	tvor
schopné	schopný	k2eAgNnSc1d1	schopné
plivat	plivat	k5eAaImF	plivat
oheň	oheň	k1gInSc4	oheň
po	po	k7c4	po
rozžvýkání	rozžvýkání	k1gNnSc4	rozžvýkání
jakéhosi	jakýsi	k3yIgInSc2	jakýsi
nerostu	nerost	k1gInSc2	nerost
<g/>
,	,	kIx,	,
létat	létat	k5eAaImF	létat
(	(	kIx(	(
<g/>
na	na	k7c6	na
netopýřích	netopýří	k2eAgNnPc6d1	netopýří
křídlech	křídlo	k1gNnPc6	křídlo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
telepatické	telepatický	k2eAgFnSc2d1	telepatická
komunikace	komunikace	k1gFnSc2	komunikace
a	a	k8xC	a
teleportace	teleportace	k1gFnSc2	teleportace
skrz	skrz	k7c4	skrz
prostor	prostor	k1gInSc4	prostor
i	i	k8xC	i
čas	čas	k1gInSc4	čas
(	(	kIx(	(
<g/>
přes	přes	k7c4	přes
něco	něco	k3yInSc4	něco
zvané	zvaný	k2eAgFnPc4d1	zvaná
mezimezí	mezimeze	k1gFnSc7	mezimeze
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
přistání	přistání	k1gNnSc6	přistání
lidských	lidský	k2eAgMnPc2d1	lidský
kolonistů	kolonista	k1gMnPc2	kolonista
byly	být	k5eAaImAgFnP	být
geneticky	geneticky	k6eAd1	geneticky
vylepšeni	vylepšit	k5eAaPmNgMnP	vylepšit
–	–	k?	–
zvětšeni	zvětšit	k5eAaPmNgMnP	zvětšit
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mohli	moct	k5eAaImAgMnP	moct
nést	nést	k5eAaImF	nést
lidského	lidský	k2eAgMnSc4d1	lidský
jezdce	jezdec	k1gMnSc4	jezdec
<g/>
,	,	kIx,	,
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
zvýšena	zvýšit	k5eAaPmNgFnS	zvýšit
jejich	jejich	k3xOp3gFnSc1	jejich
inteligence	inteligence	k1gFnSc1	inteligence
–	–	k?	–
aby	aby	kYmCp3nP	aby
mohli	moct	k5eAaImAgMnP	moct
být	být	k5eAaImF	být
použiti	použit	k2eAgMnPc1d1	použit
při	při	k7c6	při
obraně	obrana	k1gFnSc6	obrana
proti	proti	k7c3	proti
agresivní	agresivní	k2eAgFnSc3d1	agresivní
mimozemské	mimozemský	k2eAgFnSc3d1	mimozemská
(	(	kIx(	(
<g/>
a	a	k8xC	a
mimopernské	mimopernský	k2eAgFnSc6d1	mimopernský
<g/>
)	)	kIx)	)
formě	forma	k1gFnSc6	forma
života	život	k1gInSc2	život
zvané	zvaný	k2eAgFnSc2d1	zvaná
vlákna	vlákno	k1gNnPc1	vlákno
<g/>
.	.	kIx.	.
</s>
<s>
Draci	drak	k1gMnPc1	drak
z	z	k7c2	z
Pernu	perna	k1gFnSc4	perna
jsou	být	k5eAaImIp3nP	být
častěji	často	k6eAd2	často
samci	samec	k1gMnPc1	samec
<g/>
,	,	kIx,	,
samice	samice	k1gFnPc1	samice
kladou	klást	k5eAaImIp3nP	klást
větší	veliký	k2eAgInSc4d2	veliký
počet	počet	k1gInSc4	počet
vajec	vejce	k1gNnPc2	vejce
<g/>
.	.	kIx.	.
</s>
<s>
Svého	svůj	k3xOyFgInSc2	svůj
jezdce	jezdec	k1gInSc2	jezdec
si	se	k3xPyFc3	se
vybírají	vybírat	k5eAaImIp3nP	vybírat
(	(	kIx(	(
<g/>
z	z	k7c2	z
přivedených	přivedený	k2eAgMnPc2d1	přivedený
kandidátů	kandidát	k1gMnPc2	kandidát
<g/>
)	)	kIx)	)
hned	hned	k6eAd1	hned
po	po	k7c6	po
vylíhnutí	vylíhnutí	k1gNnSc6	vylíhnutí
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nS	tvořit
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
telepatické	telepatický	k2eAgNnSc1d1	telepatické
pouto	pouto	k1gNnSc1	pouto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
J.	J.	kA	J.
R.	R.	kA	R.
R.	R.	kA	R.
Tolkien	Tolkina	k1gFnPc2	Tolkina
<g/>
:	:	kIx,	:
Silmarillion	Silmarillion	k1gInSc1	Silmarillion
<g/>
,	,	kIx,	,
Hobit	hobit	k1gMnSc1	hobit
–	–	k?	–
ve	v	k7c6	v
Středozemi	Středozem	k1gFnSc6	Středozem
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
několik	několik	k4yIc4	několik
druhů	druh	k1gInPc2	druh
draků	drak	k1gInPc2	drak
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nP	objevit
draci	drak	k1gMnPc1	drak
<g/>
,	,	kIx,	,
vedení	vedený	k2eAgMnPc1d1	vedený
Glaurungem	Glaurung	k1gInSc7	Glaurung
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
nejvíce	nejvíce	k6eAd1	nejvíce
připomínají	připomínat	k5eAaImIp3nP	připomínat
hada	had	k1gMnSc4	had
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
pouze	pouze	k6eAd1	pouze
dvě	dva	k4xCgFnPc4	dva
přední	přední	k2eAgFnPc4d1	přední
nohy	noha	k1gFnPc4	noha
a	a	k8xC	a
nemohou	moct	k5eNaImIp3nP	moct
létat	létat	k5eAaImF	létat
<g/>
.	.	kIx.	.
</s>
<s>
Dokáže	dokázat	k5eAaPmIp3nS	dokázat
chrlit	chrlit	k5eAaImF	chrlit
oheň	oheň	k1gInSc4	oheň
a	a	k8xC	a
také	také	k9	také
má	mít	k5eAaImIp3nS	mít
schopnost	schopnost	k1gFnSc4	schopnost
zaklít	zaklít	k5eAaPmF	zaklít
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
se	se	k3xPyFc4	se
podívá	podívat	k5eAaImIp3nS	podívat
do	do	k7c2	do
jeho	jeho	k3xOp3gNnPc2	jeho
očí	oko	k1gNnPc2	oko
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
i	i	k9	i
plemeno	plemeno	k1gNnSc1	plemeno
šupinatých	šupinatý	k2eAgInPc2d1	šupinatý
létajících	létající	k2eAgInPc2d1	létající
draků	drak	k1gInPc2	drak
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgNnPc2	jenž
nejmohutnější	mohutný	k2eAgMnSc1d3	nejmohutnější
je	být	k5eAaImIp3nS	být
Ancalagon	Ancalagon	k1gMnSc1	Ancalagon
Černý	Černý	k1gMnSc1	Černý
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
svým	svůj	k3xOyFgInSc7	svůj
pádem	pád	k1gInSc7	pád
zničí	zničit	k5eAaPmIp3nP	zničit
pevnost	pevnost	k1gFnSc4	pevnost
Thangorodrim	Thangorodri	k1gNnSc7	Thangorodri
Temného	temný	k2eAgMnSc2d1	temný
pána	pán	k1gMnSc2	pán
Morgotha	Morgoth	k1gMnSc2	Morgoth
<g/>
.	.	kIx.	.
</s>
<s>
Stejného	stejný	k2eAgNnSc2d1	stejné
plemene	plemeno	k1gNnSc2	plemeno
je	být	k5eAaImIp3nS	být
i	i	k9	i
drak	drak	k1gInSc1	drak
Šmak	šmak	k1gInSc1	šmak
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vyžene	vyhnat	k5eAaPmIp3nS	vyhnat
trpaslíky	trpaslík	k1gMnPc4	trpaslík
z	z	k7c2	z
Osamělé	osamělý	k2eAgFnSc2d1	osamělá
hory	hora	k1gFnSc2	hora
a	a	k8xC	a
přivlastní	přivlastnit	k5eAaPmIp3nS	přivlastnit
si	se	k3xPyFc3	se
jejich	jejich	k3xOp3gInSc4	jejich
poklad	poklad	k1gInSc4	poklad
<g/>
.	.	kIx.	.
</s>
<s>
Zmíněn	zmíněn	k2eAgInSc1d1	zmíněn
je	být	k5eAaImIp3nS	být
také	také	k9	také
ledový	ledový	k2eAgInSc1d1	ledový
drak	drak	k1gInSc1	drak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rob	roba	k1gFnPc2	roba
Cohen	Cohna	k1gFnPc2	Cohna
<g/>
:	:	kIx,	:
Dračí	dračí	k2eAgNnSc1d1	dračí
srdce	srdce	k1gNnSc1	srdce
–	–	k?	–
filmový	filmový	k2eAgInSc1d1	filmový
příběh	příběh	k1gInSc1	příběh
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
je	být	k5eAaImIp3nS	být
výstižně	výstižně	k6eAd1	výstižně
zachycen	zachycen	k2eAgInSc1d1	zachycen
nejasný	jasný	k2eNgInSc1d1	nejasný
postoj	postoj	k1gInSc1	postoj
lidí	člověk	k1gMnPc2	člověk
k	k	k7c3	k
drakům	drak	k1gMnPc3	drak
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
netuší	tušit	k5eNaImIp3nP	tušit
<g/>
,	,	kIx,	,
že	že	k8xS	že
draci	drak	k1gMnPc1	drak
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
nejsou	být	k5eNaImIp3nP	být
zlí	zlý	k2eAgMnPc1d1	zlý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Cressida	Cressida	k1gFnSc1	Cressida
Cowellová	Cowellová	k1gFnSc1	Cowellová
<g/>
:	:	kIx,	:
Jak	jak	k8xS	jak
vycvičit	vycvičit	k5eAaPmF	vycvičit
draka	drak	k1gMnSc4	drak
–	–	k?	–
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
knize	kniha	k1gFnSc6	kniha
je	být	k5eAaImIp3nS	být
hodně	hodně	k6eAd1	hodně
druhů	druh	k1gInPc2	druh
draků	drak	k1gInPc2	drak
<g/>
;	;	kIx,	;
bývají	bývat	k5eAaImIp3nP	bývat
malí	malý	k2eAgMnPc1d1	malý
i	i	k8xC	i
obrovští	obrovský	k2eAgMnPc1d1	obrovský
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
<g/>
:	:	kIx,	:
děsovec	děsovec	k1gInSc1	děsovec
obludný	obludný	k2eAgInSc1d1	obludný
<g/>
,	,	kIx,	,
Garvan	Garvan	k1gMnSc1	Garvan
<g/>
,	,	kIx,	,
drak	drak	k1gMnSc1	drak
běžný	běžný	k2eAgMnSc1d1	běžný
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
zahradní	zahradní	k2eAgFnSc4d1	zahradní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hnědák	hnědák	k1gMnSc1	hnědák
obecný	obecný	k2eAgInSc4d1	obecný
</s>
</p>
<p>
<s>
Christopher	Christophra	k1gFnPc2	Christophra
Paolini	Paolin	k2eAgMnPc1d1	Paolin
<g/>
:	:	kIx,	:
Odkaz	odkaz	k1gInSc1	odkaz
Dračích	dračí	k2eAgMnPc2d1	dračí
jezdců	jezdec	k1gMnPc2	jezdec
–	–	k?	–
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
tetralogii	tetralogie	k1gFnSc6	tetralogie
je	být	k5eAaImIp3nS	být
jenom	jenom	k9	jenom
jeden	jeden	k4xCgInSc1	jeden
druh	druh	k1gInSc1	druh
draků	drak	k1gInPc2	drak
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
však	však	k9	však
jak	jak	k6eAd1	jak
divocí	divoký	k2eAgMnPc1d1	divoký
draci	drak	k1gMnPc1	drak
tak	tak	k6eAd1	tak
i	i	k9	i
draci	drak	k1gMnPc1	drak
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
svého	svůj	k3xOyFgMnSc4	svůj
jezdce	jezdec	k1gMnSc4	jezdec
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
přijali	přijmout	k5eAaPmAgMnP	přijmout
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
svého	svůj	k3xOyFgMnSc4	svůj
partnera	partner	k1gMnSc4	partner
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Terry	Terr	k1gInPc1	Terr
Pratchett	Pratchetta	k1gFnPc2	Pratchetta
<g/>
:	:	kIx,	:
Stráže	stráž	k1gFnSc2	stráž
<g/>
!	!	kIx.	!
</s>
<s>
Stráže	stráž	k1gFnPc1	stráž
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
,	,	kIx,	,
Poslední	poslední	k2eAgMnSc1d1	poslední
hrdina	hrdina	k1gMnSc1	hrdina
–	–	k?	–
V	v	k7c6	v
sérii	série	k1gFnSc6	série
Úžasná	úžasný	k2eAgFnSc1d1	úžasná
Zeměplocha	Zeměploch	k1gMnSc2	Zeměploch
jsou	být	k5eAaImIp3nP	být
jedinými	jediný	k2eAgInPc7d1	jediný
žijícími	žijící	k2eAgInPc7d1	žijící
draky	drak	k1gInPc7	drak
bahenní	bahenní	k2eAgMnPc1d1	bahenní
dráčci	dráček	k1gMnPc1	dráček
–	–	k?	–
malí	malý	k2eAgMnPc1d1	malý
ještěři	ještěr	k1gMnPc1	ještěr
se	s	k7c7	s
složitým	složitý	k2eAgNnSc7d1	složité
zažíváním	zažívání	k1gNnSc7	zažívání
<g/>
,	,	kIx,	,
schopní	schopný	k2eAgMnPc1d1	schopný
letu	let	k1gInSc3	let
a	a	k8xC	a
plivání	plivání	k1gNnSc3	plivání
ohně	oheň	k1gInSc2	oheň
a	a	k8xC	a
náchylní	náchylný	k2eAgMnPc1d1	náchylný
k	k	k7c3	k
výbuchům	výbuch	k1gInPc3	výbuch
v	v	k7c6	v
případě	případ	k1gInSc6	případ
nemoci	nemoc	k1gFnSc2	nemoc
či	či	k8xC	či
rozrušení	rozrušení	k1gNnSc2	rozrušení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
existovali	existovat	k5eAaImAgMnP	existovat
velcí	velký	k2eAgMnPc1d1	velký
draci	drak	k1gMnPc1	drak
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
využívali	využívat	k5eAaPmAgMnP	využívat
k	k	k7c3	k
životu	život	k1gInSc3	život
magii	magie	k1gFnSc3	magie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vzhledem	vzhled	k1gInSc7	vzhled
k	k	k7c3	k
jejímu	její	k3xOp3gInSc3	její
úbytku	úbytek	k1gInSc3	úbytek
postupně	postupně	k6eAd1	postupně
vyhynuli	vyhynout	k5eAaPmAgMnP	vyhynout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozdějších	pozdní	k2eAgFnPc6d2	pozdější
knihách	kniha	k1gFnPc6	kniha
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
měsíční	měsíční	k2eAgMnPc1d1	měsíční
draci	drak	k1gMnPc1	drak
se	s	k7c7	s
zakrslými	zakrslý	k2eAgNnPc7d1	zakrslé
křídly	křídlo	k1gNnPc7	křídlo
<g/>
,	,	kIx,	,
využívající	využívající	k2eAgNnSc1d1	využívající
chrlení	chrlení	k1gNnSc1	chrlení
ohně	oheň	k1gInSc2	oheň
k	k	k7c3	k
pohonu	pohon	k1gInSc3	pohon
</s>
</p>
<p>
<s>
Richard	Richard	k1gMnSc1	Richard
A.	A.	kA	A.
Knaak	Knaak	k1gMnSc1	Knaak
<g/>
:	:	kIx,	:
Dragonrealm	Dragonrealm	k1gMnSc1	Dragonrealm
–	–	k?	–
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
sérii	série	k1gFnSc4	série
knih	kniha	k1gFnPc2	kniha
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yRgFnPc4	který
draci	drak	k1gMnPc1	drak
hrají	hrát	k5eAaImIp3nP	hrát
ústřední	ústřední	k2eAgFnSc4d1	ústřední
roli	role	k1gFnSc4	role
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
zde	zde	k6eAd1	zde
mnoho	mnoho	k4c4	mnoho
druhů	druh	k1gInPc2	druh
draků	drak	k1gInPc2	drak
(	(	kIx(	(
<g/>
různé	různý	k2eAgFnPc4d1	různá
kasty	kasta	k1gFnPc4	kasta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
inteligentních	inteligentní	k2eAgInPc2d1	inteligentní
draků	drak	k1gInPc2	drak
–	–	k?	–
dračí	dračí	k2eAgFnPc1d1	dračí
králové	králová	k1gFnPc1	králová
<g/>
,	,	kIx,	,
vévodové	vévoda	k1gMnPc1	vévoda
<g/>
,	,	kIx,	,
válečníci	válečník	k1gMnPc1	válečník
a	a	k8xC	a
sluhové	sluha	k1gMnPc1	sluha
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
ohnivé	ohnivý	k2eAgInPc4d1	ohnivý
draky	drak	k1gInPc4	drak
chrlící	chrlící	k2eAgInSc1d1	chrlící
oheň	oheň	k1gInSc1	oheň
a	a	k8xC	a
vzdušné	vzdušný	k2eAgInPc1d1	vzdušný
draky	drak	k1gInPc1	drak
chrlící	chrlící	k2eAgInSc1d1	chrlící
jed	jed	k1gInSc4	jed
(	(	kIx(	(
<g/>
výjimku	výjimka	k1gFnSc4	výjimka
tvoří	tvořit	k5eAaImIp3nS	tvořit
Ledový	ledový	k2eAgInSc1d1	ledový
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
dýchá	dýchat	k5eAaImIp3nS	dýchat
mráz	mráz	k1gInSc1	mráz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
inteligentní	inteligentní	k2eAgMnPc1d1	inteligentní
draci	drak	k1gMnPc1	drak
umí	umět	k5eAaImIp3nP	umět
měnit	měnit	k5eAaImF	měnit
podobu	podoba	k1gFnSc4	podoba
v	v	k7c4	v
ozbrojeného	ozbrojený	k2eAgMnSc4d1	ozbrojený
válečníka	válečník	k1gMnSc4	válečník
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
se	se	k3xPyFc4	se
dovedou	dovést	k5eAaPmIp3nP	dovést
proměnit	proměnit	k5eAaPmF	proměnit
v	v	k7c4	v
ženy	žena	k1gFnPc4	žena
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
neinteligentních	inteligentní	k2eNgMnPc2d1	neinteligentní
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
nižší	nízký	k2eAgMnPc1d2	nižší
draci	drak	k1gMnPc1	drak
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
ohniví	ohnivý	k2eAgMnPc1d1	ohnivý
draci	drak	k1gMnPc1	drak
využívají	využívat	k5eAaImIp3nP	využívat
jako	jako	k9	jako
jízdní	jízdní	k2eAgNnPc1d1	jízdní
zvířata	zvíře	k1gNnPc1	zvíře
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
lidské	lidský	k2eAgFnSc6d1	lidská
podobě	podoba	k1gFnSc6	podoba
<g/>
,	,	kIx,	,
vyverny	vyverna	k1gFnPc1	vyverna
a	a	k8xC	a
bazilišci	bazilišek	k1gMnPc1	bazilišek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Guivre	Guivr	k1gInSc5	Guivr
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Brněnský	brněnský	k2eAgMnSc1d1	brněnský
drak	drak	k1gMnSc1	drak
</s>
</p>
<p>
<s>
Zmej	Zmej	k1gMnSc1	Zmej
–	–	k?	–
slovanský	slovanský	k2eAgMnSc1d1	slovanský
drak	drak	k1gMnSc1	drak
</s>
</p>
<p>
<s>
Draci	drak	k1gMnPc1	drak
(	(	kIx(	(
<g/>
Středozem	Středozem	k1gFnSc1	Středozem
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Drakobijce	drakobijce	k1gMnSc1	drakobijce
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
kniha	kniha	k1gFnSc1	kniha
o	o	k7c4	o
mytologii	mytologie	k1gFnSc4	mytologie
starých	starý	k2eAgInPc2d1	starý
Slovanů	Slovan	k1gInPc2	Slovan
<g/>
,	,	kIx,	,
o	o	k7c6	o
dracích	drak	k1gInPc6	drak
v	v	k7c6	v
kapitole	kapitola	k1gFnSc6	kapitola
2	[number]	k4	2
-	-	kIx~	-
HOSTINSKÝ	hostinský	k1gMnSc1	hostinský
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
Záboj	Záboj	k1gMnSc1	Záboj
<g/>
.	.	kIx.	.
</s>
<s>
Stará	Stará	k1gFnSc1	Stará
vieronauka	vieronauk	k1gMnSc2	vieronauk
slovenská	slovenský	k2eAgNnPc1d1	slovenské
:	:	kIx,	:
Vek	veka	k1gFnPc2	veka
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
kniha	kniha	k1gFnSc1	kniha
1	[number]	k4	1
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
<g/>
]	]	kIx)	]
Pešť	Pešť	k1gFnSc1	Pešť
<g/>
:	:	kIx,	:
Minerva	Minerva	k1gFnSc1	Minerva
<g/>
,	,	kIx,	,
1871	[number]	k4	1871
<g/>
.	.	kIx.	.
122	[number]	k4	122
s.	s.	k?	s.
-	-	kIx~	-
dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
v	v	k7c6	v
Digitální	digitální	k2eAgFnSc6d1	digitální
knihovně	knihovna	k1gFnSc6	knihovna
UKB	UKB	kA	UKB
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
drak	drak	k1gInSc1	drak
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
drak	drak	k1gInSc4	drak
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
drak	drak	k1gInSc1	drak
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
České	český	k2eAgFnSc2d1	Česká
dračí	dračí	k2eAgFnSc2d1	dračí
komunity	komunita	k1gFnSc2	komunita
</s>
</p>
