<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
den	den	k1gInSc1	den
míru	mír	k1gInSc2	mír
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
International	International	k1gMnSc1	International
Day	Day	k1gMnSc1	Day
of	of	k?	of
Peace	Peace	k1gMnSc1	Peace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
známý	známý	k2eAgInSc1d1	známý
i	i	k9	i
jako	jako	k8xS	jako
Světový	světový	k2eAgInSc4d1	světový
den	den	k1gInSc4	den
míru	mír	k1gInSc2	mír
(	(	kIx(	(
<g/>
World	World	k1gMnSc1	World
Peace	Peace	k1gMnSc1	Peace
Day	Day	k1gMnSc1	Day
<g/>
)	)	kIx)	)
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
slaví	slavit	k5eAaImIp3nS	slavit
každý	každý	k3xTgInSc1	každý
rok	rok	k1gInSc1	rok
21	[number]	k4	21
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
<g/>
.	.	kIx.	.
</s>
