<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
den	den	k1gInSc1	den
míru	mír	k1gInSc2	mír
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
International	International	k1gMnSc1	International
Day	Day	k1gMnSc1	Day
of	of	k?	of
Peace	Peace	k1gMnSc1	Peace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
známý	známý	k2eAgInSc1d1	známý
i	i	k9	i
jako	jako	k8xS	jako
Světový	světový	k2eAgInSc4d1	světový
den	den	k1gInSc4	den
míru	mír	k1gInSc2	mír
(	(	kIx(	(
<g/>
World	World	k1gMnSc1	World
Peace	Peace	k1gMnSc1	Peace
Day	Day	k1gMnSc1	Day
<g/>
)	)	kIx)	)
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
slaví	slavit	k5eAaImIp3nS	slavit
každý	každý	k3xTgInSc1	každý
rok	rok	k1gInSc1	rok
21	[number]	k4	21
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
<g/>
.	.	kIx.	.
</s>
<s>
Věnovaný	věnovaný	k2eAgMnSc1d1	věnovaný
je	být	k5eAaImIp3nS	být
míru	míra	k1gFnSc4	míra
a	a	k8xC	a
specificky	specificky	k6eAd1	specificky
absenci	absence	k1gFnSc4	absence
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Svátek	svátek	k1gInSc4	svátek
si	se	k3xPyFc3	se
připomíná	připomínat	k5eAaImIp3nS	připomínat
mnoho	mnoho	k4c4	mnoho
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
politických	politický	k2eAgFnPc2d1	politická
skupin	skupina	k1gFnPc2	skupina
<g/>
,	,	kIx,	,
vojenských	vojenský	k2eAgFnPc2d1	vojenská
skupin	skupina	k1gFnPc2	skupina
a	a	k8xC	a
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
den	den	k1gInSc1	den
slavil	slavit	k5eAaImAgInS	slavit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc1	tento
den	den	k1gInSc1	den
poprvé	poprvé	k6eAd1	poprvé
věnován	věnován	k2eAgInSc1d1	věnován
mírové	mírový	k2eAgFnSc3d1	mírová
edukaci	edukace	k1gFnSc3	edukace
a	a	k8xC	a
klíčovým	klíčový	k2eAgInPc3d1	klíčový
prostředkům	prostředek	k1gInPc3	prostředek
na	na	k7c6	na
trvalé	trvalá	k1gFnSc6	trvalá
snižování	snižování	k1gNnSc2	snižování
konfliktů	konflikt	k1gInPc2	konflikt
<g/>
.	.	kIx.	.
</s>
<s>
Tématem	téma	k1gNnSc7	téma
Mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
dne	den	k1gInSc2	den
míru	mír	k1gInSc2	mír
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
bylo	být	k5eAaImAgNnS	být
právo	právo	k1gNnSc1	právo
lidí	člověk	k1gMnPc2	člověk
na	na	k7c4	na
mír	mír	k1gInSc4	mír
<g/>
,	,	kIx,	,
podporující	podporující	k2eAgInSc4d1	podporující
slib	slib	k1gInSc4	slib
Spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
v	v	k7c4	v
Deklaraci	deklarace	k1gFnSc4	deklarace
OSN	OSN	kA	OSN
na	na	k7c4	na
právo	právo	k1gNnSc4	právo
národů	národ	k1gInPc2	národ
na	na	k7c4	na
mír	mír	k1gInSc4	mír
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
uznává	uznávat	k5eAaImIp3nS	uznávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
propagace	propagace	k1gFnSc1	propagace
míru	mír	k1gInSc2	mír
je	být	k5eAaImIp3nS	být
vitální	vitální	k2eAgNnSc1d1	vitální
pro	pro	k7c4	pro
celkové	celkový	k2eAgNnSc4d1	celkové
užívání	užívání	k1gNnSc4	užívání
všech	všecek	k3xTgNnPc2	všecek
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
připomenutí	připomenutí	k1gNnSc4	připomenutí
tohoto	tento	k3xDgInSc2	tento
dne	den	k1gInSc2	den
thinkPEACE	thinkPEACE	k?	thinkPEACE
Network	network	k1gInSc1	network
(	(	kIx(	(
<g/>
síť	síť	k1gFnSc1	síť
myslící	myslící	k2eAgFnSc1d1	myslící
na	na	k7c4	na
mír	mír	k1gInSc4	mír
<g/>
)	)	kIx)	)
uvedla	uvést	k5eAaPmAgFnS	uvést
zábavní	zábavní	k2eAgInSc4d1	zábavní
program	program	k1gInSc4	program
"	"	kIx"	"
<g/>
Sólo	sólo	k1gNnSc1	sólo
pro	pro	k7c4	pro
mír	mír	k1gInSc4	mír
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
ve	v	k7c6	v
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
50	[number]	k4	50
klubech	klub	k1gInPc6	klub
po	po	k7c6	po
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Globální	globální	k2eAgFnSc1d1	globální
iniciativa	iniciativa	k1gFnSc1	iniciativa
Vlny	vlna	k1gFnSc2	vlna
laskavosti	laskavost	k1gFnSc2	laskavost
oslavuje	oslavovat	k5eAaImIp3nS	oslavovat
tento	tento	k3xDgInSc4	tento
den	den	k1gInSc4	den
globálními	globální	k2eAgFnPc7d1	globální
meditačními	meditační	k2eAgFnPc7d1	meditační
akcemi	akce	k1gFnPc7	akce
<g/>
.	.	kIx.	.
</s>
