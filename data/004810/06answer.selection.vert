<s>
Ghost	Ghost	k1gFnSc1	Ghost
in	in	k?	in
the	the	k?	the
Shell	Shell	k1gInSc1	Shell
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
攻	攻	k?	攻
<g/>
,	,	kIx,	,
Kókaku	Kókak	k1gMnSc3	Kókak
kidótai	kidóta	k1gMnSc3	kidóta
<g/>
,	,	kIx,	,
ve	v	k7c6	v
volném	volný	k2eAgInSc6d1	volný
překladu	překlad	k1gInSc6	překlad
Zásahová	zásahový	k2eAgFnSc1d1	zásahová
obrněná	obrněný	k2eAgFnSc1d1	obrněná
pořádková	pořádkový	k2eAgFnSc1d1	pořádková
jednotka	jednotka	k1gFnSc1	jednotka
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
japonská	japonský	k2eAgFnSc1d1	japonská
post-kyberpunková	postyberpunkový	k2eAgFnSc1d1	post-kyberpunkový
série	série	k1gFnSc1	série
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
započala	započnout	k5eAaPmAgFnS	započnout
mangou	manga	k1gFnSc7	manga
vytvořenou	vytvořený	k2eAgFnSc7d1	vytvořená
Masamunem	Masamun	k1gInSc7	Masamun
Širóem	Širó	k1gInSc7	Širó
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
začala	začít	k5eAaPmAgFnS	začít
vycházet	vycházet	k5eAaImF	vycházet
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Šúkan	Šúkan	k1gMnSc1	Šúkan
Young	Young	k1gMnSc1	Young
Magazine	Magazin	k1gInSc5	Magazin
<g/>
.	.	kIx.	.
</s>
