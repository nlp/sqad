<s>
Horoskop	horoskop	k1gInSc1
</s>
<s>
Moderní	moderní	k2eAgInSc1d1
horoskop	horoskop	k1gInSc1
</s>
<s>
Horoskop	horoskop	k1gInSc1
je	být	k5eAaImIp3nS
astrologická	astrologický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
nebo	nebo	k8xC
diagram	diagram	k1gInSc1
(	(	kIx(
<g/>
obrazec	obrazec	k1gInSc1
<g/>
)	)	kIx)
zachycující	zachycující	k2eAgNnSc1d1
postavení	postavení	k1gNnSc1
nebeských	nebeský	k2eAgNnPc2d1
těles	těleso	k1gNnPc2
pro	pro	k7c4
daný	daný	k2eAgInSc4d1
okamžik	okamžik	k1gInSc4
a	a	k8xC
konkrétní	konkrétní	k2eAgNnSc4d1
místo	místo	k1gNnSc4
na	na	k7c6
Zemi	zem	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původ	původ	k1gInSc1
slova	slovo	k1gNnSc2
je	být	k5eAaImIp3nS
odvozován	odvozovat	k5eAaImNgInS
z	z	k7c2
řeckého	řecký	k2eAgInSc2d1
horoskopos	horoskopos	k1gInSc4
(	(	kIx(
<g/>
=	=	kIx~
znamení	znamení	k1gNnSc1
času	čas	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Historický	historický	k2eAgInSc1d1
horoskop	horoskop	k1gInSc1
z	z	k7c2
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
Babylónští	babylónský	k2eAgMnPc1d1
kněží-astrologové	kněží-astrolog	k1gMnPc1
jsou	být	k5eAaImIp3nP
považováni	považován	k2eAgMnPc1d1
za	za	k7c2
první	první	k4xOgFnSc2
<g/>
,	,	kIx,
kdo	kdo	k3yRnSc1,k3yQnSc1,k3yInSc1
rozdělili	rozdělit	k5eAaPmAgMnP
roční	roční	k2eAgFnSc4d1
pouť	pouť	k1gFnSc4
Slunce	slunce	k1gNnSc2
zvěrokruhem	zvěrokruh	k1gInSc7
mezi	mezi	k7c4
dvanáct	dvanáct	k4xCc4
souhvězdí	souhvězdí	k1gNnPc2
<g/>
,	,	kIx,
či	či	k8xC
spíše	spíše	k9
astrologická	astrologický	k2eAgNnPc4d1
znamení	znamení	k1gNnPc4
po	po	k7c6
30	#num#	k4
<g/>
°	°	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
jim	on	k3xPp3gFnPc3
připisováno	připisovat	k5eAaImNgNnS
také	také	k9
sestavení	sestavení	k1gNnSc4
prvních	první	k4xOgInPc2
horoskopů	horoskop	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejstarší	starý	k2eAgInSc1d3
dochovaný	dochovaný	k2eAgInSc1d1
horoskop	horoskop	k1gInSc1
je	být	k5eAaImIp3nS
diagram	diagram	k1gInSc4
z	z	k7c2
r.	r.	kA
410	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
horoskop	horoskop	k1gInSc4
syna	syn	k1gMnSc2
babylónského	babylónský	k2eAgMnSc2d1
panovníka	panovník	k1gMnSc2
Šuma	šuma	k1gFnSc1
Usa	Usa	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgMnS
29	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
420	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
</s>
<s>
Kolem	kolem	k7c2
roku	rok	k1gInSc2
200	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
se	se	k3xPyFc4
v	v	k7c6
helénské	helénský	k2eAgFnSc6d1
astrologii	astrologie	k1gFnSc6
náhle	náhle	k6eAd1
prosadil	prosadit	k5eAaPmAgInS
nový	nový	k2eAgInSc1d1
systém	systém	k1gInSc1
tvorby	tvorba	k1gFnSc2
horoskopů	horoskop	k1gInPc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
významným	významný	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
rozšířil	rozšířit	k5eAaPmAgInS
původní	původní	k2eAgInSc1d1
systém	systém	k1gInSc1
babylónský	babylónský	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nový	nový	k2eAgInSc1d1
systém	systém	k1gInSc1
se	se	k3xPyFc4
objevil	objevit	k5eAaPmAgInS
náhle	náhle	k6eAd1
a	a	k8xC
ustálil	ustálit	k5eAaPmAgMnS
se	se	k3xPyFc4
během	během	k7c2
půl	půl	k1xP
století	století	k1gNnSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
vede	vést	k5eAaImIp3nS
k	k	k7c3
závěrům	závěr	k1gInPc3
<g/>
,	,	kIx,
že	že	k8xS
byl	být	k5eAaImAgInS
pravděpodobně	pravděpodobně	k6eAd1
dílem	dílo	k1gNnSc7
jedince	jedinko	k6eAd1
(	(	kIx(
<g/>
za	za	k7c2
něhož	jenž	k3xRgNnSc2
lze	lze	k6eAd1
považovat	považovat	k5eAaImF
legendárního	legendární	k2eAgMnSc4d1
Herma	Hermes	k1gMnSc4
Trismegista	Trismegist	k1gMnSc4
<g/>
)	)	kIx)
či	či	k8xC
neznámé	známý	k2eNgFnSc2d1
malé	malý	k2eAgFnSc2d1
astrologické	astrologický	k2eAgFnSc2d1
školy	škola	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
systém	systém	k1gInSc1
ustálil	ustálit	k5eAaPmAgInS
sestavování	sestavování	k1gNnSc4
horoskopů	horoskop	k1gInPc2
prakticky	prakticky	k6eAd1
v	v	k7c6
podobě	podoba	k1gFnSc6
<g/>
,	,	kIx,
jaká	jaký	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
dochovala	dochovat	k5eAaPmAgFnS
dodnes	dodnes	k6eAd1
(	(	kIx(
<g/>
zavedla	zavést	k5eAaPmAgFnS
systém	systém	k1gInSc4
domů	dům	k1gInPc2
<g/>
,	,	kIx,
aspekty	aspekt	k1gInPc4
<g/>
,	,	kIx,
používala	používat	k5eAaImAgFnS
sedm	sedm	k4xCc4
klasických	klasický	k2eAgFnPc2d1
astrologických	astrologický	k2eAgFnPc2d1
planet	planeta	k1gFnPc2
apod.	apod.	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
v	v	k7c6
pozdější	pozdní	k2eAgFnSc6d2
arabské	arabský	k2eAgFnSc6d1
a	a	k8xC
západní	západní	k2eAgFnSc6d1
astrologické	astrologický	k2eAgFnSc6d1
tradici	tradice	k1gFnSc6
doznal	doznat	k5eAaPmAgMnS
výrazných	výrazný	k2eAgFnPc2d1
modifikací	modifikace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Prvky	prvek	k1gInPc1
horoskopu	horoskop	k1gInSc2
</s>
<s>
V	v	k7c6
západní	západní	k2eAgFnSc6d1
astrologické	astrologický	k2eAgFnSc6d1
tradici	tradice	k1gFnSc6
se	se	k3xPyFc4
pro	pro	k7c4
sestavení	sestavení	k1gNnSc4
horoskopu	horoskop	k1gInSc2
počítá	počítat	k5eAaImIp3nS
s	s	k7c7
různými	různý	k2eAgInPc7d1
prvky	prvek	k1gInPc7
<g/>
:	:	kIx,
</s>
<s>
astrologické	astrologický	k2eAgFnPc1d1
planety	planeta	k1gFnPc1
</s>
<s>
zvěrokruh	zvěrokruh	k1gInSc1
</s>
<s>
astrologické	astrologický	k2eAgInPc1d1
domy	dům	k1gInPc1
</s>
<s>
ascendent	ascendent	k1gMnSc1
a	a	k8xC
descendent	descendent	k1gMnSc1
</s>
<s>
aspekty	aspekt	k1gInPc1
(	(	kIx(
<g/>
vzájemné	vzájemný	k2eAgInPc1d1
úhly	úhel	k1gInPc1
mezi	mezi	k7c7
prvky	prvek	k1gInPc7
<g/>
)	)	kIx)
</s>
<s>
medium	medium	k1gNnSc1
coeli	coele	k1gFnSc3
a	a	k8xC
imum	imum	k6eAd1
coeli	coenout	k5eAaImAgMnP,k5eAaPmAgMnP
</s>
<s>
stálice	stálice	k1gFnPc1
(	(	kIx(
<g/>
fixní	fixní	k2eAgFnPc1d1
hvězdy	hvězda	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
asteroidy	asteroid	k1gInPc4
</s>
<s>
citlivé	citlivý	k2eAgInPc1d1
(	(	kIx(
<g/>
tzv.	tzv.	kA
Arabské	arabský	k2eAgInPc1d1
<g/>
)	)	kIx)
body	bod	k1gInPc1
</s>
<s>
další	další	k2eAgInPc4d1
astrologické	astrologický	k2eAgInPc4d1
body	bod	k1gInPc4
(	(	kIx(
<g/>
lunární	lunární	k2eAgInPc1d1
uzly	uzel	k1gInPc1
<g/>
,	,	kIx,
Černá	černý	k2eAgFnSc1d1
Luna	luna	k1gFnSc1
apod.	apod.	kA
<g/>
)	)	kIx)
</s>
<s>
Podmínky	podmínka	k1gFnPc1
pro	pro	k7c4
sestavení	sestavení	k1gNnSc4
horoskopu	horoskop	k1gInSc2
</s>
<s>
K	k	k7c3
sestavení	sestavení	k1gNnSc3
přesného	přesný	k2eAgInSc2d1
horoskopu	horoskop	k1gInSc2
události	událost	k1gFnSc2
se	se	k3xPyFc4
pracuje	pracovat	k5eAaImIp3nS
s	s	k7c7
postavením	postavení	k1gNnSc7
planet	planeta	k1gFnPc2
na	na	k7c6
obloze	obloha	k1gFnSc6
v	v	k7c6
příslušném	příslušný	k2eAgNnSc6d1
místě	místo	k1gNnSc6
a	a	k8xC
v	v	k7c6
příslušném	příslušný	k2eAgInSc6d1
času	čas	k1gInSc6
události	událost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
sestavení	sestavení	k1gNnSc4
horoskopu	horoskop	k1gInSc2
je	být	k5eAaImIp3nS
proto	proto	k8xC
nezbytné	nezbytný	k2eAgNnSc1d1,k2eNgNnSc1d1
znát	znát	k5eAaImF
<g/>
:	:	kIx,
</s>
<s>
přesné	přesný	k2eAgNnSc1d1
určení	určení	k1gNnSc1
místa	místo	k1gNnSc2
-	-	kIx~
zeměpisná	zeměpisný	k2eAgFnSc1d1
šířka	šířka	k1gFnSc1
a	a	k8xC
zeměpisná	zeměpisný	k2eAgFnSc1d1
délka	délka	k1gFnSc1
místa	místo	k1gNnSc2
události	událost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
mundánní	mundánní	k2eAgFnSc6d1
astrologii	astrologie	k1gFnSc6
se	se	k3xPyFc4
pro	pro	k7c4
výpočty	výpočet	k1gInPc4
horoskopů	horoskop	k1gInPc2
státních	státní	k2eAgInPc2d1
celků	celek	k1gInPc2
uvažuje	uvažovat	k5eAaImIp3nS
se	s	k7c7
souřadnicemi	souřadnice	k1gFnPc7
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
příslušného	příslušný	k2eAgInSc2d1
územního	územní	k2eAgInSc2d1
celku	celek	k1gInSc2
nezávisle	závisle	k6eNd1
na	na	k7c6
jeho	jeho	k3xOp3gFnSc6
poloze	poloha	k1gFnSc6
v	v	k7c6
rámci	rámec	k1gInSc6
jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
teritoria	teritorium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
co	co	k9
možná	možná	k9
nejpřesnější	přesný	k2eAgNnSc4d3
určení	určení	k1gNnSc4
času	čas	k1gInSc2
-	-	kIx~
pro	pro	k7c4
přepočet	přepočet	k1gInSc4
místního	místní	k2eAgInSc2d1
času	čas	k1gInSc2
na	na	k7c4
hvězdný	hvězdný	k2eAgInSc4d1
čas	čas	k1gInSc4
je	být	k5eAaImIp3nS
zapotřebí	zapotřebí	k6eAd1
znát	znát	k5eAaImF
také	také	k9
časový	časový	k2eAgInSc4d1
posun	posun	k1gInSc4
proti	proti	k7c3
univerzálnímu	univerzální	k2eAgInSc3d1
času	čas	k1gInSc3
v	v	k7c6
době	doba	k1gFnSc6
události	událost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čím	co	k3yRnSc7,k3yInSc7,k3yQnSc7
přesnější	přesný	k2eAgInSc1d2
časový	časový	k2eAgInSc1d1
údaj	údaj	k1gInSc1
je	být	k5eAaImIp3nS
při	při	k7c6
sestavování	sestavování	k1gNnSc6
horoskopu	horoskop	k1gInSc2
k	k	k7c3
dispozici	dispozice	k1gFnSc3
<g/>
,	,	kIx,
tím	ten	k3xDgNnSc7
přesnější	přesný	k2eAgInPc4d2
horoskop	horoskop	k1gInSc4
lze	lze	k6eAd1
sestavit	sestavit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Obě	dva	k4xCgFnPc1
tyto	tento	k3xDgFnPc1
veličiny	veličina	k1gFnPc1
se	se	k3xPyFc4
dosazují	dosazovat	k5eAaImIp3nP
do	do	k7c2
rovnic	rovnice	k1gFnPc2
pro	pro	k7c4
výpočet	výpočet	k1gInSc4
prvků	prvek	k1gInPc2
horoskopu	horoskop	k1gInSc2
(	(	kIx(
<g/>
postavení	postavení	k1gNnSc1
planet	planeta	k1gFnPc2
v	v	k7c6
horoskopu	horoskop	k1gInSc6
<g/>
,	,	kIx,
osy	osa	k1gFnPc4
horoskopu	horoskop	k1gInSc2
<g/>
,	,	kIx,
domy	dům	k1gInPc1
horoskopu	horoskop	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Není	být	k5eNaImIp3nS
<g/>
-li	-li	k?
některý	některý	k3yIgInSc4
z	z	k7c2
těchto	tento	k3xDgInPc2
údajů	údaj	k1gInPc2
znám	znát	k5eAaImIp1nS
<g/>
,	,	kIx,
používá	používat	k5eAaImIp3nS
astrologie	astrologie	k1gFnSc1
pro	pro	k7c4
jejich	jejich	k3xOp3gFnSc4
zpětné	zpětný	k2eAgNnSc1d1
dopočítání	dopočítání	k1gNnSc1
některé	některý	k3yIgFnSc2
rektifikační	rektifikační	k2eAgFnSc2d1
metody	metoda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rektifikace	rektifikace	k1gFnSc1
horoskopu	horoskop	k1gInSc2
se	se	k3xPyFc4
u	u	k7c2
různých	různý	k2eAgInPc2d1
astrologických	astrologický	k2eAgInPc2d1
směrů	směr	k1gInPc2
liší	lišit	k5eAaImIp3nP
a	a	k8xC
vždy	vždy	k6eAd1
závisí	záviset	k5eAaImIp3nS
na	na	k7c6
přesnosti	přesnost	k1gFnSc6
dalších	další	k2eAgInPc2d1
známých	známý	k2eAgInPc2d1
údajů	údaj	k1gInPc2
o	o	k7c6
zkoumaném	zkoumaný	k2eAgInSc6d1
subjektu	subjekt	k1gInSc6
(	(	kIx(
<g/>
údaje	údaj	k1gInSc2
ze	z	k7c2
života	život	k1gInSc2
a	a	k8xC
činnosti	činnost	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Planety	planeta	k1gFnPc1
</s>
<s>
Západní	západní	k2eAgInSc1d1
astrologický	astrologický	k2eAgInSc1d1
symbolismus	symbolismus	k1gInSc1
považuje	považovat	k5eAaImIp3nS
za	za	k7c4
astrologické	astrologický	k2eAgFnPc4d1
planety	planeta	k1gFnPc4
také	také	k9
Slunce	slunce	k1gNnSc1
a	a	k8xC
Měsíc	měsíc	k1gInSc1
(	(	kIx(
<g/>
v	v	k7c6
astrologii	astrologie	k1gFnSc6
častěji	často	k6eAd2
označován	označovat	k5eAaImNgInS
jako	jako	k8xC,k8xS
Luna	luna	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
novověkých	novověký	k2eAgInPc2d1
astronomických	astronomický	k2eAgInPc2d1
objevů	objev	k1gInPc2
nových	nový	k2eAgFnPc2d1
planet	planeta	k1gFnPc2
sluneční	sluneční	k2eAgFnSc2d1
soustavy	soustava	k1gFnSc2
(	(	kIx(
<g/>
Uran	Uran	k1gMnSc1
<g/>
,	,	kIx,
Neptun	Neptun	k1gMnSc1
<g/>
,	,	kIx,
Pluto	Pluto	k1gMnSc1
<g/>
)	)	kIx)
pracoval	pracovat	k5eAaImAgInS
astrologický	astrologický	k2eAgInSc1d1
systém	systém	k1gInSc1
se	s	k7c7
sedmi	sedm	k4xCc7
klasickými	klasický	k2eAgFnPc7d1
planetami	planeta	k1gFnPc7
(	(	kIx(
<g/>
Slunce	slunce	k1gNnSc1
<g/>
,	,	kIx,
Luna	luna	k1gFnSc1
<g/>
,	,	kIx,
Merkur	Merkur	k1gInSc1
<g/>
,	,	kIx,
Venuše	Venuše	k1gFnSc1
<g/>
,	,	kIx,
Mars	Mars	k1gInSc1
<g/>
,	,	kIx,
Jupiter	Jupiter	k1gMnSc1
a	a	k8xC
Saturn	Saturn	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
těchto	tento	k3xDgInPc6
objevech	objev	k1gInPc6
absorboval	absorbovat	k5eAaBmAgMnS
a	a	k8xC
zapracoval	zapracovat	k5eAaPmAgMnS
také	také	k9
tato	tento	k3xDgNnPc1
nová	nový	k2eAgNnPc1d1
tělesa	těleso	k1gNnPc1
<g/>
,	,	kIx,
označovaná	označovaný	k2eAgFnSc1d1
jako	jako	k8xS,k8xC
transsaturnská	transsaturnský	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Slunce	slunce	k1gNnSc1
</s>
<s>
Luna	luna	k1gFnSc1
</s>
<s>
Merkur	Merkur	k1gInSc1
</s>
<s>
Venuše	Venuše	k1gFnSc1
</s>
<s>
Mars	Mars	k1gInSc1
</s>
<s>
Jupiter	Jupiter	k1gMnSc1
</s>
<s>
Saturn	Saturn	k1gMnSc1
</s>
<s>
Uran	Uran	k1gInSc1
</s>
<s>
Neptun	Neptun	k1gMnSc1
</s>
<s>
Pluto	Pluto	k1gMnSc1
</s>
<s>
Zvěrokruh	zvěrokruh	k1gInSc1
</s>
<s>
Naprostá	naprostý	k2eAgFnSc1d1
většina	většina	k1gFnSc1
západních	západní	k2eAgMnPc2d1
astrologů	astrolog	k1gMnPc2
pracuje	pracovat	k5eAaImIp3nS
s	s	k7c7
tropickým	tropický	k2eAgInSc7d1
zodiakem	zodiak	k1gInSc7
rozděleným	rozdělený	k2eAgInSc7d1
na	na	k7c4
dvanáct	dvanáct	k4xCc4
stejně	stejně	k6eAd1
velkých	velký	k2eAgInPc2d1
úseků	úsek	k1gInPc2
(	(	kIx(
<g/>
astrologických	astrologický	k2eAgNnPc2d1
znamení	znamení	k1gNnPc2
<g/>
)	)	kIx)
po	po	k7c6
30	#num#	k4
<g/>
°	°	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výchozím	výchozí	k2eAgInSc7d1
bodem	bod	k1gInSc7
pro	pro	k7c4
první	první	k4xOgNnSc4
znamení	znamení	k1gNnSc4
astrologického	astrologický	k2eAgInSc2d1
zvěrokruhu	zvěrokruh	k1gInSc2
(	(	kIx(
<g/>
Berana	Beran	k1gMnSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
jarní	jarní	k2eAgInSc4d1
bod	bod	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
potřeby	potřeba	k1gFnPc4
interpretace	interpretace	k1gFnSc2
dělí	dělit	k5eAaImIp3nP
Západní	západní	k2eAgFnPc1d1
astrologie	astrologie	k1gFnPc1
v	v	k7c6
souladu	soulad	k1gInSc6
se	s	k7c7
svými	svůj	k3xOyFgInPc7
hermetickými	hermetický	k2eAgInPc7d1
kořeny	kořen	k1gInPc7
znamení	znamení	k1gNnSc2
zvěrokruhu	zvěrokruh	k1gInSc2
podle	podle	k7c2
několika	několik	k4yIc2
kritérií	kritérion	k1gNnPc2
<g/>
:	:	kIx,
</s>
<s>
dělení	dělení	k1gNnSc1
na	na	k7c4
mužská	mužský	k2eAgNnPc4d1
a	a	k8xC
ženská	ženský	k2eAgNnPc4d1
znamení	znamení	k1gNnPc4
</s>
<s>
dělení	dělení	k1gNnSc1
podle	podle	k7c2
4	#num#	k4
základních	základní	k2eAgInPc2d1
živlů	živel	k1gInPc2
na	na	k7c4
ohnivá	ohnivý	k2eAgNnPc4d1
<g/>
,	,	kIx,
zemská	zemský	k2eAgFnSc1d1
<g/>
,	,	kIx,
vzdušná	vzdušný	k2eAgFnSc1d1
a	a	k8xC
vodní	vodní	k2eAgNnSc1d1
znamení	znamení	k1gNnSc1
</s>
<s>
dělení	dělení	k1gNnSc1
na	na	k7c4
základní	základní	k2eAgFnSc4d1
(	(	kIx(
<g/>
kardinální	kardinální	k2eAgFnSc4d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
pevná	pevný	k2eAgFnSc1d1
(	(	kIx(
<g/>
fixní	fixní	k2eAgFnSc1d1
<g/>
)	)	kIx)
a	a	k8xC
pohyblivá	pohyblivý	k2eAgFnSc1d1
(	(	kIx(
<g/>
mutuální	mutuální	k2eAgNnSc1d1
<g/>
)	)	kIx)
znamení	znamení	k1gNnSc1
</s>
<s>
Ohnivá	ohnivý	k2eAgNnPc1d1
znamení	znamení	k1gNnPc1
</s>
<s>
Zemská	zemský	k2eAgNnPc1d1
znamení	znamení	k1gNnPc1
</s>
<s>
Vzdušná	vzdušný	k2eAgNnPc1d1
znamení	znamení	k1gNnPc1
</s>
<s>
Vodní	vodní	k2eAgNnSc1d1
znamení	znamení	k1gNnSc1
</s>
<s>
Beran	Beran	k1gMnSc1
</s>
<s>
Býk	býk	k1gMnSc1
</s>
<s>
Blíženci	blíženec	k1gMnPc1
</s>
<s>
Rak	rak	k1gMnSc1
</s>
<s>
Lev	Lev	k1gMnSc1
</s>
<s>
Panna	Panna	k1gFnSc1
</s>
<s>
Váhy	Váhy	k1gFnPc1
</s>
<s>
Štír	štír	k1gMnSc1
</s>
<s>
Střelec	Střelec	k1gMnSc1
</s>
<s>
Kozoroh	Kozoroh	k1gMnSc1
</s>
<s>
Vodnář	vodnář	k1gMnSc1
</s>
<s>
Ryby	Ryby	k1gFnPc1
</s>
<s>
Siderický	siderický	k2eAgInSc1d1
zvěrokruh	zvěrokruh	k1gInSc1
a	a	k8xC
reálné	reálný	k2eAgFnPc1d1
polohy	poloha	k1gFnPc1
planet	planeta	k1gFnPc2
</s>
<s>
Výchozím	výchozí	k2eAgInSc7d1
bodem	bod	k1gInSc7
tradičního	tradiční	k2eAgInSc2d1
zvěrokruhu	zvěrokruh	k1gInSc2
je	být	k5eAaImIp3nS
jarní	jarní	k2eAgInSc4d1
bod	bod	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
se	se	k3xPyFc4
však	však	k9
neustále	neustále	k6eAd1
posouvá	posouvat	k5eAaImIp3nS
oproti	oproti	k7c3
souhvězdím	souhvězdí	k1gNnSc7
díky	díky	k7c3
jevu	jev	k1gInSc3
zvanému	zvaný	k2eAgInSc3d1
precese	precese	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
tomu	ten	k3xDgNnSc3
je	být	k5eAaImIp3nS
zvěrokruh	zvěrokruh	k1gInSc1
oproti	oproti	k7c3
skutečným	skutečný	k2eAgFnPc3d1
polohám	poloha	k1gFnPc3
souhvězdí	souhvězdí	k1gNnSc2
na	na	k7c6
obloze	obloha	k1gFnSc6
posunut	posunout	k5eAaPmNgInS
o	o	k7c6
necelých	celý	k2eNgInPc6d1
30	#num#	k4
<g/>
°	°	k?
a	a	k8xC
tento	tento	k3xDgInSc1
posun	posun	k1gInSc1
se	se	k3xPyFc4
neustále	neustále	k6eAd1
zvětšuje	zvětšovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celý	celý	k2eAgInSc1d1
tento	tento	k3xDgInSc1
precesní	precesní	k2eAgInSc1d1
cyklus	cyklus	k1gInSc1
trvá	trvat	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
25	#num#	k4
725	#num#	k4
let	léto	k1gNnPc2
a	a	k8xC
říká	říkat	k5eAaImIp3nS
se	se	k3xPyFc4
mu	on	k3xPp3gNnSc3
platónský	platónský	k2eAgInSc4d1
rok	rok	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
tohoto	tento	k3xDgInSc2
faktu	fakt	k1gInSc2
vychází	vycházet	k5eAaImIp3nS
Siderický	siderický	k2eAgInSc1d1
zvěrokruh	zvěrokruh	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
definuje	definovat	k5eAaBmIp3nS
začátky	začátek	k1gInPc4
konce	konec	k1gInSc2
znamení	znamení	k1gNnSc2
podle	podle	k7c2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
jimi	on	k3xPp3gInPc7
prochází	procházet	k5eAaImIp3nP
Slunce	slunce	k1gNnSc1
na	na	k7c6
hvězdné	hvězdný	k2eAgFnSc6d1
obloze	obloha	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Takže	takže	k9
například	například	k6eAd1
pokud	pokud	k8xS
se	se	k3xPyFc4
narodíte	narodit	k5eAaPmIp2nP
1	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
a	a	k8xC
jste	být	k5eAaImIp2nP
tedy	tedy	k9
Beran	beran	k1gInSc4
<g/>
,	,	kIx,
tak	tak	k8xS,k8xC
Slunce	slunce	k1gNnSc1
ještě	ještě	k9
prochází	procházet	k5eAaImIp3nS
znamením	znamení	k1gNnSc7
Ryb	Ryby	k1gFnPc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
také	také	k9
odpovídá	odpovídat	k5eAaImIp3nS
Siderickému	siderický	k2eAgInSc3d1
zvěrokruhu	zvěrokruh	k1gInSc3
<g/>
,	,	kIx,
který	který	k3yQgInSc4,k3yRgInSc4,k3yIgInSc4
definuje	definovat	k5eAaBmIp3nS
Sideralistická	Sideralistický	k2eAgFnSc1d1
astrologie	astrologie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Domy	dům	k1gInPc1
horoskopu	horoskop	k1gInSc2
</s>
<s>
Domy	dům	k1gInPc4
je	být	k5eAaImIp3nS
v	v	k7c6
horoskopu	horoskop	k1gInSc6
nazýváno	nazývat	k5eAaImNgNnS
12	#num#	k4
úhlových	úhlový	k2eAgFnPc2d1
výsečí	výseč	k1gFnPc2
<g/>
,	,	kIx,
významově	významově	k6eAd1
přibližně	přibližně	k6eAd1
odpovídajících	odpovídající	k2eAgFnPc2d1
znamením	znamení	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Helénská	helénský	k2eAgFnSc1d1
astrologie	astrologie	k1gFnSc1
používala	používat	k5eAaImAgFnS
rovněž	rovněž	k9
dělení	dělení	k1gNnSc4
horoskopu	horoskop	k1gInSc2
na	na	k7c4
osm	osm	k4xCc4
domů	dům	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
bylo	být	k5eAaImAgNnS
později	pozdě	k6eAd2
opuštěno	opuštěn	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pravděpodobně	pravděpodobně	k6eAd1
bylo	být	k5eAaImAgNnS
odvozeno	odvodit	k5eAaPmNgNnS
z	z	k7c2
ještě	ještě	k6eAd1
staršího	starý	k2eAgInSc2d2
přístupu	přístup	k1gInSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byl	být	k5eAaImAgInS
obraz	obraz	k1gInSc1
nebeské	nebeský	k2eAgFnSc2d1
sféry	sféra	k1gFnSc2
dělen	dělit	k5eAaImNgInS
jen	jen	k9
na	na	k7c4
čtyři	čtyři	k4xCgInPc4
„	„	k?
<g/>
rohové	rohový	k2eAgInPc1d1
<g/>
“	“	k?
domy	dům	k1gInPc1
či	či	k8xC
podle	podle	k7c2
osmi	osm	k4xCc2
lunárních	lunární	k2eAgFnPc2d1
fází	fáze	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Dělení	dělení	k1gNnSc1
kruhu	kruh	k1gInSc2
</s>
<s>
Dělení	dělení	k1gNnSc1
kruhu	kruh	k1gInSc2
na	na	k7c4
8	#num#	k4
a	a	k8xC
12	#num#	k4
dílů	díl	k1gInPc2
má	mít	k5eAaImIp3nS
své	svůj	k3xOyFgNnSc4
jisté	jistý	k2eAgNnSc4d1
opodstatnění	opodstatnění	k1gNnSc4
ve	v	k7c6
funkci	funkce	k1gFnSc6
Sinus	sinus	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
sin	sin	kA
90	#num#	k4
<g/>
°	°	k?
<g/>
=	=	kIx~
1	#num#	k4
<g/>
/	/	kIx~
<g/>
√	√	k?
<g/>
1	#num#	k4
-	-	kIx~
4	#num#	k4
osy	osa	k1gFnSc2
</s>
<s>
sin	sin	kA
45	#num#	k4
<g/>
°	°	k?
<g/>
=	=	kIx~
1	#num#	k4
<g/>
/	/	kIx~
<g/>
√	√	k?
<g/>
2	#num#	k4
-	-	kIx~
8	#num#	k4
lunací	lunace	k1gFnPc2
(	(	kIx(
<g/>
vizuálně	vizuálně	k6eAd1
lehce	lehko	k6eAd1
rozlišitelné	rozlišitelný	k2eAgFnPc4d1
fáze	fáze	k1gFnPc4
Měsíce	měsíc	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
sin	sin	kA
30	#num#	k4
<g/>
°	°	k?
<g/>
=	=	kIx~
1	#num#	k4
<g/>
/	/	kIx~
<g/>
√	√	k?
<g/>
4	#num#	k4
-	-	kIx~
12	#num#	k4
novů	nov	k1gInPc2
do	do	k7c2
roka	rok	k1gInSc2
(	(	kIx(
<g/>
cyklus	cyklus	k1gInSc1
je	být	k5eAaImIp3nS
kratší	krátký	k2eAgInSc1d2
zhruba	zhruba	k6eAd1
o	o	k7c4
2	#num#	k4
dny	den	k1gInPc4
<g/>
)	)	kIx)
</s>
<s>
Žádné	žádný	k3yNgNnSc4
jiné	jiný	k2eAgNnSc4d1
celé	celý	k2eAgNnSc4d1
číslo	číslo	k1gNnSc4
pod	pod	k7c7
odmocninou	odmocnina	k1gFnSc7
nedělí	dělit	k5eNaImIp3nS
kruh	kruh	k1gInSc1
beze	beze	k7c2
zbytku	zbytek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Sumerové	Sumer	k1gMnPc1
kolem	kolem	k7c2
roku	rok	k1gInSc2
2000	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
zavedli	zavést	k5eAaPmAgMnP
šedesátkový	šedesátkový	k2eAgInSc4d1
systém	systém	k1gInSc4
kombinovaný	kombinovaný	k2eAgInSc4d1
s	s	k7c7
desítkovým	desítkový	k2eAgInSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozdělili	rozdělit	k5eAaPmAgMnP
kruh	kruh	k1gInSc4
na	na	k7c4
360	#num#	k4
<g/>
°	°	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
těch	ten	k3xDgFnPc2
dob	doba	k1gFnPc2
je	být	k5eAaImIp3nS
počítán	počítán	k2eAgInSc4d1
čas	čas	k1gInSc4
po	po	k7c6
šedesáti	šedesát	k4xCc6
jednotkách	jednotka	k1gFnPc6
seskupených	seskupený	k2eAgInPc2d1
do	do	k7c2
24	#num#	k4
hodin	hodina	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Zajímavě	zajímavě	k6eAd1
se	se	k3xPyFc4
tento	tento	k3xDgInSc1
systém	systém	k1gInSc1
kryje	krýt	k5eAaImIp3nS
s	s	k7c7
dělením	dělení	k1gNnSc7
kruhu	kruh	k1gInSc2
na	na	k7c4
8	#num#	k4
a	a	k8xC
12	#num#	k4
dílů	díl	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
díly	díl	k1gInPc4
mají	mít	k5eAaImIp3nP
společné	společný	k2eAgInPc4d1
body	bod	k1gInPc4
na	na	k7c6
polygonu	polygon	k1gInSc6
o	o	k7c6
24	#num#	k4
stranách	strana	k1gFnPc6
(	(	kIx(
<g/>
to	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
15	#num#	k4
<g/>
°	°	k?
–	–	k?
polovina	polovina	k1gFnSc1
znamení	znamení	k1gNnSc2
-	-	kIx~
1	#num#	k4
hodina	hodina	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zatímco	zatímco	k8xS
znamení	znamení	k1gNnSc4
jsou	být	k5eAaImIp3nP
členěním	členění	k1gNnSc7
na	na	k7c6
úrovni	úroveň	k1gFnSc6
oběžné	oběžný	k2eAgFnSc2d1
dráhy	dráha	k1gFnSc2
Země	zem	k1gFnSc2
respektive	respektive	k9
sklonu	sklona	k1gFnSc4
osy	osa	k1gFnSc2
rotace	rotace	k1gFnSc2
vzhledem	vzhled	k1gInSc7
k	k	k7c3
Slunci	slunce	k1gNnSc3
<g/>
,	,	kIx,
domy	dům	k1gInPc1
se	se	k3xPyFc4
vztahují	vztahovat	k5eAaImIp3nP
k	k	k7c3
otáčení	otáčení	k1gNnSc3
Země	zem	k1gFnSc2
kolem	kolem	k7c2
své	svůj	k3xOyFgFnSc2
osy	osa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozdělují	rozdělovat	k5eAaImIp3nP
horoskop	horoskop	k1gInSc4
na	na	k7c4
12	#num#	k4
úseků	úsek	k1gInPc2
<g/>
,	,	kIx,
počínaje	počínaje	k7c7
ascendentem	ascendent	k1gMnSc7
(	(	kIx(
<g/>
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
některých	některý	k3yIgFnPc2
metod	metoda	k1gFnPc2
konstrukce	konstrukce	k1gFnSc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
např.	např.	kA
metoda	metoda	k1gFnSc1
equální	equální	k2eAgFnSc1d1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
ascendent	ascendent	k1gMnSc1
středem	středem	k7c2
prvního	první	k4xOgInSc2
domu	dům	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
souladu	soulad	k1gInSc6
s	s	k7c7
tímto	tento	k3xDgInSc7
„	„	k?
<g/>
přiblížením	přiblížení	k1gNnPc3
<g/>
“	“	k?
k	k	k7c3
Zemi	zem	k1gFnSc3
jsou	být	k5eAaImIp3nP
domy	dům	k1gInPc1
základem	základ	k1gInSc7
pro	pro	k7c4
individuální	individuální	k2eAgInSc4d1
výklad	výklad	k1gInSc4
horoskopu	horoskop	k1gInSc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
postavení	postavení	k1gNnSc4
prvků	prvek	k1gInPc2
horoskopu	horoskop	k1gInSc2
ve	v	k7c6
znameních	znamení	k1gNnPc6
má	mít	k5eAaImIp3nS
v	v	k7c6
astrologii	astrologie	k1gFnSc6
spíše	spíše	k9
kolektivní	kolektivní	k2eAgInSc4d1
význam	význam	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
jsou	být	k5eAaImIp3nP
domy	dům	k1gInPc1
vztaženy	vztažen	k2eAgInPc1d1
k	k	k7c3
otáčení	otáčení	k1gNnSc3
Země	zem	k1gFnSc2
kolem	kolem	k7c2
vlastní	vlastní	k2eAgFnSc2d1
osy	osa	k1gFnSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
pro	pro	k7c4
jejich	jejich	k3xOp3gInSc4
výpočet	výpočet	k1gInSc4
rozhodující	rozhodující	k2eAgFnSc1d1
znalost	znalost	k1gFnSc1
přesného	přesný	k2eAgInSc2d1
času	čas	k1gInSc2
narození	narození	k1gNnSc2
(	(	kIx(
<g/>
nebo	nebo	k8xC
události	událost	k1gFnPc1
<g/>
,	,	kIx,
pro	pro	k7c4
kterou	který	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
se	se	k3xPyFc4
horoskop	horoskop	k1gInSc4
počítá	počítat	k5eAaImIp3nS
<g/>
)	)	kIx)
a	a	k8xC
souřadnice	souřadnice	k1gFnSc1
polohy	poloha	k1gFnSc2
místa	místo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Základem	základ	k1gInSc7
konstrukce	konstrukce	k1gFnSc2
domů	dům	k1gInPc2
je	být	k5eAaImIp3nS
rozdělení	rozdělení	k1gNnSc1
horoskopu	horoskop	k1gInSc2
osou	osa	k1gFnSc7
ascendent	ascendent	k1gMnSc1
-	-	kIx~
descendent	descendent	k1gMnSc1
a	a	k8xC
osou	osý	k2eAgFnSc4d1
Medium	medium	k1gNnSc4
Coeli	Coele	k1gFnSc4
-	-	kIx~
Imum	Imum	k1gInSc4
Coeli	Coele	k1gFnSc4
na	na	k7c4
čtyři	čtyři	k4xCgInPc4
kvadranty	kvadrant	k1gInPc4
<g/>
;	;	kIx,
každý	každý	k3xTgInSc1
kvadrant	kvadrant	k1gInSc1
se	se	k3xPyFc4
dělí	dělit	k5eAaImIp3nS
na	na	k7c4
tři	tři	k4xCgInPc4
astrologické	astrologický	k2eAgInPc4d1
domy	dům	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konstrukce	konstrukce	k1gFnSc1
domů	dům	k1gInPc2
je	být	k5eAaImIp3nS
jednou	jeden	k4xCgFnSc7
z	z	k7c2
nejproblematičtějších	problematický	k2eAgFnPc2d3
otázek	otázka	k1gFnPc2
západní	západní	k2eAgFnSc2d1
astrologie	astrologie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakreslený	nakreslený	k2eAgInSc1d1
horoskop	horoskop	k1gInSc1
je	být	k5eAaImIp3nS
totiž	totiž	k9
vlastně	vlastně	k9
projekcí	projekce	k1gFnSc7
prostorového	prostorový	k2eAgNnSc2d1
(	(	kIx(
<g/>
sférického	sférický	k2eAgNnSc2d1
<g/>
)	)	kIx)
uspořádání	uspořádání	k1gNnSc1
do	do	k7c2
roviny	rovina	k1gFnSc2
ekliptiky	ekliptika	k1gFnSc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
domy	dům	k1gInPc1
mají	mít	k5eAaImIp3nP
vztah	vztah	k1gInSc4
k	k	k7c3
rovině	rovina	k1gFnSc3
rovníku	rovník	k1gInSc2
<g/>
;	;	kIx,
je	být	k5eAaImIp3nS
tedy	tedy	k9
třeba	třeba	k6eAd1
promítnout	promítnout	k5eAaPmF
domy	dům	k1gInPc4
do	do	k7c2
roviny	rovina	k1gFnSc2
ekliptiky	ekliptika	k1gFnSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
lze	lze	k6eAd1
udělat	udělat	k5eAaPmF
více	hodně	k6eAd2
způsoby	způsob	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postupně	postupně	k6eAd1
se	se	k3xPyFc4
uplatnilo	uplatnit	k5eAaPmAgNnS
asi	asi	k9
26	#num#	k4
různých	různý	k2eAgFnPc2d1
metod	metoda	k1gFnPc2
konstrukce	konstrukce	k1gFnSc2
domů	dům	k1gInPc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
některé	některý	k3yIgFnPc4
nelze	lze	k6eNd1
použít	použít	k5eAaPmF
pro	pro	k7c4
oblasti	oblast	k1gFnPc4
blízko	blízko	k7c2
zemských	zemský	k2eAgInPc2d1
pólů	pól	k1gInPc2
a	a	k8xC
existuje	existovat	k5eAaImIp3nS
řada	řada	k1gFnSc1
dalších	další	k2eAgFnPc2d1
námitek	námitka	k1gFnPc2
proti	proti	k7c3
každé	každý	k3xTgFnSc3
z	z	k7c2
nich	on	k3xPp3gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hojně	hojně	k6eAd1
používanými	používaný	k2eAgFnPc7d1
jsou	být	k5eAaImIp3nP
dnes	dnes	k6eAd1
např.	např.	kA
metoda	metoda	k1gFnSc1
Placidova	Placidův	k2eAgFnSc1d1
nebo	nebo	k8xC
Kochova	Kochův	k2eAgFnSc1d1
<g/>
;	;	kIx,
transpersonální	transpersonální	k2eAgMnPc1d1
astrologové	astrolog	k1gMnPc1
mají	mít	k5eAaImIp3nP
v	v	k7c6
oblibě	obliba	k1gFnSc6
Campanův	Campanův	k2eAgInSc4d1
systém	systém	k1gInSc4
domů	dům	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Aspekty	aspekt	k1gInPc1
</s>
<s>
Ukázka	ukázka	k1gFnSc1
nativního	nativní	k2eAgInSc2d1
horoskopu	horoskop	k1gInSc2
s	s	k7c7
tabulkou	tabulka	k1gFnSc7
aspektů	aspekt	k1gInPc2
</s>
<s>
Jako	jako	k8xS,k8xC
aspekty	aspekt	k1gInPc1
se	se	k3xPyFc4
označují	označovat	k5eAaImIp3nP
vzájemné	vzájemný	k2eAgInPc1d1
úhly	úhel	k1gInPc1
mezi	mezi	k7c7
postaveními	postavení	k1gNnPc7
dvou	dva	k4xCgFnPc2
planet	planeta	k1gFnPc2
(	(	kIx(
<g/>
bodů	bod	k1gInPc2
<g/>
)	)	kIx)
v	v	k7c6
horoskopu	horoskop	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aspekty	aspekt	k1gInPc7
jsou	být	k5eAaImIp3nP
odvozovány	odvozován	k2eAgInPc1d1
z	z	k7c2
vepisování	vepisování	k1gNnSc2
mnohoúhelníků	mnohoúhelník	k1gInPc2
do	do	k7c2
kruhu	kruh	k1gInSc2
a	a	k8xC
tvoří	tvořit	k5eAaImIp3nP
několik	několik	k4yIc4
základních	základní	k2eAgFnPc2d1
řad	řada	k1gFnPc2
<g/>
:	:	kIx,
</s>
<s>
harmonická	harmonický	k2eAgFnSc1d1
(	(	kIx(
<g/>
dódekagonální	dódekagonální	k2eAgFnSc1d1
<g/>
)	)	kIx)
řada	řada	k1gFnSc1
-	-	kIx~
je	být	k5eAaImIp3nS
odvozena	odvodit	k5eAaPmNgFnS
z	z	k7c2
dvanáctiúhelníku	dvanáctiúhelník	k1gInSc2
a	a	k8xC
jejím	její	k3xOp3gInSc7
modulem	modul	k1gInSc7
je	být	k5eAaImIp3nS
úhel	úhel	k1gInSc1
30	#num#	k4
<g/>
°	°	k?
</s>
<s>
disharmonická	disharmonický	k2eAgFnSc1d1
(	(	kIx(
<g/>
oktagonální	oktagonální	k2eAgFnSc1d1
<g/>
)	)	kIx)
řada	řada	k1gFnSc1
-	-	kIx~
je	být	k5eAaImIp3nS
odvozena	odvodit	k5eAaPmNgFnS
z	z	k7c2
osmiúhelníku	osmiúhelník	k1gInSc2
a	a	k8xC
jejím	její	k3xOp3gInSc7
modulem	modul	k1gInSc7
je	být	k5eAaImIp3nS
úhel	úhel	k1gInSc1
45	#num#	k4
<g/>
°	°	k?
</s>
<s>
dynamická	dynamický	k2eAgFnSc1d1
(	(	kIx(
<g/>
dekagonální	dekagonální	k2eAgFnSc1d1
<g/>
)	)	kIx)
řada	řada	k1gFnSc1
-	-	kIx~
je	být	k5eAaImIp3nS
odvozena	odvodit	k5eAaPmNgFnS
z	z	k7c2
desetiúhelníku	desetiúhelník	k1gInSc2
a	a	k8xC
jejím	její	k3xOp3gInSc7
modulem	modul	k1gInSc7
je	být	k5eAaImIp3nS
úhel	úhel	k1gInSc1
36	#num#	k4
<g/>
°	°	k?
</s>
<s>
méně	málo	k6eAd2
často	často	k6eAd1
se	se	k3xPyFc4
používají	používat	k5eAaImIp3nP
i	i	k8xC
aspekty	aspekt	k1gInPc4
nonagonální	nonagonální	k2eAgFnSc2d1
řady	řada	k1gFnSc2
<g/>
,	,	kIx,
heptagonální	heptagonální	k2eAgFnSc2d1
řady	řada	k1gFnSc2
a	a	k8xC
undekagonální	undekagonální	k2eAgFnSc2d1
řady	řada	k1gFnSc2
</s>
<s>
V	v	k7c6
astrologické	astrologický	k2eAgFnSc6d1
praxi	praxe	k1gFnSc6
se	se	k3xPyFc4
nepracuje	pracovat	k5eNaImIp3nS
pouze	pouze	k6eAd1
s	s	k7c7
exaktními	exaktní	k2eAgInPc7d1
(	(	kIx(
<g/>
přesnými	přesný	k2eAgInPc7d1
<g/>
)	)	kIx)
aspekty	aspekt	k1gInPc7
<g/>
,	,	kIx,
ale	ale	k8xC
též	též	k9
s	s	k7c7
aspekty	aspekt	k1gInPc7
plaktickými	plaktický	k2eAgInPc7d1
(	(	kIx(
<g/>
nepřesnými	přesný	k2eNgInPc7d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Maximální	maximální	k2eAgFnSc1d1
nepřesnost	nepřesnost	k1gFnSc1
plaktického	plaktický	k2eAgInSc2d1
aspektu	aspekt	k1gInSc2
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
orbis	orbis	k1gInSc1
a	a	k8xC
závisí	záviset	k5eAaImIp3nS
na	na	k7c6
typu	typ	k1gInSc6
aspektu	aspekt	k1gInSc2
<g/>
,	,	kIx,
posuzovaných	posuzovaný	k2eAgFnPc6d1
planetách	planeta	k1gFnPc6
<g/>
,	,	kIx,
typu	typ	k1gInSc6
horoskopu	horoskop	k1gInSc6
a	a	k8xC
astrologické	astrologický	k2eAgFnSc3d1
škole	škola	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Hlavní	hlavní	k2eAgInPc1d1
aspekty	aspekt	k1gInPc1
</s>
<s>
Aspekt	aspekt	k1gInSc1
</s>
<s>
Úhel	úhel	k1gInSc1
</s>
<s>
Orbis	orbis	k1gInSc1
</s>
<s>
Konjunkce	konjunkce	k1gFnSc1
</s>
<s>
0	#num#	k4
<g/>
°	°	k?
</s>
<s>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
°	°	k?
</s>
<s>
Opozice	opozice	k1gFnSc1
</s>
<s>
180	#num#	k4
<g/>
°	°	k?
</s>
<s>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
°	°	k?
</s>
<s>
Trinus	Trinus	k1gInSc1
(	(	kIx(
<g/>
Trigon	trigon	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
120	#num#	k4
<g/>
°	°	k?
</s>
<s>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
°	°	k?
</s>
<s>
Kvadratura	kvadratura	k1gFnSc1
</s>
<s>
90	#num#	k4
<g/>
°	°	k?
</s>
<s>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
°	°	k?
</s>
<s>
Sextil	Sextit	k5eAaPmAgMnS,k5eAaImAgMnS
</s>
<s>
60	#num#	k4
<g/>
°	°	k?
</s>
<s>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
°	°	k?
</s>
<s>
Vedlejší	vedlejší	k2eAgInPc1d1
aspekty	aspekt	k1gInPc1
</s>
<s>
Aspekt	aspekt	k1gInSc1
</s>
<s>
Úhel	úhel	k1gInSc1
</s>
<s>
Orbis	orbis	k1gInSc1
</s>
<s>
Kvinkunux	Kvinkunux	k1gInSc1
</s>
<s>
150	#num#	k4
<g/>
°	°	k?
</s>
<s>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
°	°	k?
</s>
<s>
Semikvadratura	Semikvadratura	k1gFnSc1
</s>
<s>
45	#num#	k4
<g/>
°	°	k?
</s>
<s>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
°	°	k?
</s>
<s>
Seskvikvadratura	Seskvikvadratura	k1gFnSc1
</s>
<s>
135	#num#	k4
<g/>
°	°	k?
</s>
<s>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
°	°	k?
</s>
<s>
Semisextil	Semisextit	k5eAaImAgMnS,k5eAaPmAgMnS
</s>
<s>
30	#num#	k4
<g/>
°	°	k?
</s>
<s>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
°	°	k?
</s>
<s>
Kvintil	Kvintit	k5eAaPmAgMnS,k5eAaImAgMnS
</s>
<s>
72	#num#	k4
<g/>
°	°	k?
</s>
<s>
Bikvintil	Bikvintit	k5eAaPmAgMnS,k5eAaImAgMnS
</s>
<s>
144	#num#	k4
<g/>
°	°	k?
</s>
<s>
Nevědeckost	nevědeckost	k1gFnSc1
</s>
<s>
Astrologie	astrologie	k1gFnSc1
je	být	k5eAaImIp3nS
označována	označovat	k5eAaImNgFnS
jako	jako	k9
pseudověda	pseudověda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Běžný	běžný	k2eAgInSc1d1
horoskop	horoskop	k1gInSc1
nemůže	moct	k5eNaImIp3nS
vysvětlit	vysvětlit	k5eAaPmF
rozdílné	rozdílný	k2eAgInPc4d1
osudy	osud	k1gInPc4
dvojčat	dvojče	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
i	i	k9
u	u	k7c2
dvojčat	dvojče	k1gNnPc2
je	být	k5eAaImIp3nS
při	při	k7c6
narození	narození	k1gNnSc6
rozdílná	rozdílný	k2eAgFnSc1d1
konstelace	konstelace	k1gFnSc1
hvězdných	hvězdný	k2eAgInPc2d1
objektů	objekt	k1gInPc2
<g/>
,	,	kIx,
takže	takže	k8xS
to	ten	k3xDgNnSc1
zcela	zcela	k6eAd1
nevylučuje	vylučovat	k5eNaImIp3nS
vědeckost	vědeckost	k1gFnSc4
horoskopů	horoskop	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.astrohoroskop.cz	www.astrohoroskop.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
http://www.myty.info/rservice.php?akce=tisk&	http://www.myty.info/rservice.php?akce=tisk&	k?
<g/>
↑	↑	k?
I.	I.	kA
Astrologická	astrologický	k2eAgFnSc1d1
výstava	výstava	k1gFnSc1
<g/>
,	,	kIx,
Písek	Písek	k1gInSc1
<g/>
,	,	kIx,
Sladovna	sladovna	k1gFnSc1
2010	#num#	k4
<g/>
,	,	kIx,
http://www.astro-vystava.cz	http://www.astro-vystava.cz	k1gInSc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Astrologie	astrologie	k1gFnSc1
</s>
<s>
Západní	západní	k2eAgFnPc1d1
astrologie	astrologie	k1gFnPc1
</s>
<s>
Zvěrokruh	zvěrokruh	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Horoskop	horoskop	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
horoskop	horoskop	k1gInSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
PSH	PSH	kA
<g/>
:	:	kIx,
7960	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85062074	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85062074	#num#	k4
</s>
