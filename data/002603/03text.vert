<s>
Absint	absint	k1gInSc1	absint
(	(	kIx(	(
<g/>
též	též	k9	též
absinth	absinth	k1gInSc1	absinth
<g/>
;	;	kIx,	;
francouzsky	francouzsky	k6eAd1	francouzsky
absinthe	absinthe	k6eAd1	absinthe
<g/>
;	;	kIx,	;
z	z	k7c2	z
latinského	latinský	k2eAgInSc2d1	latinský
absinthium	absinthium	k1gNnSc4	absinthium
a	a	k8xC	a
řeckého	řecký	k2eAgNnSc2d1	řecké
ἀ	ἀ	k?	ἀ
apsinthion	apsinthion	k1gInSc1	apsinthion
<g/>
,	,	kIx,	,
pelyněk	pelyněk	k1gInSc1	pelyněk
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
tvrdý	tvrdý	k2eAgInSc4d1	tvrdý
alkoholický	alkoholický	k2eAgInSc4d1	alkoholický
nápoj	nápoj	k1gInSc4	nápoj
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
z	z	k7c2	z
pelyňku	pelyněk	k1gInSc2	pelyněk
<g/>
,	,	kIx,	,
anýzu	anýz	k1gInSc2	anýz
a	a	k8xC	a
fenyklu	fenykl	k1gInSc2	fenykl
(	(	kIx(	(
<g/>
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
tzv.	tzv.	kA	tzv.
svatou	svatý	k2eAgFnSc4d1	svatá
trojici	trojice	k1gFnSc4	trojice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
dochucení	dochucení	k1gNnSc4	dochucení
se	se	k3xPyFc4	se
často	často	k6eAd1	často
se	se	k3xPyFc4	se
využívají	využívat	k5eAaImIp3nP	využívat
další	další	k2eAgFnPc1d1	další
bylinky	bylinka	k1gFnPc1	bylinka
<g/>
,	,	kIx,	,
kupř	kupř	kA	kupř
<g/>
.	.	kIx.	.
yzop	yzop	k1gInSc1	yzop
lékařský	lékařský	k2eAgInSc1d1	lékařský
<g/>
,	,	kIx,	,
meduňka	meduňka	k1gFnSc1	meduňka
lékařská	lékařský	k2eAgFnSc1d1	lékařská
nebo	nebo	k8xC	nebo
badyán	badyán	k1gInSc1	badyán
<g/>
.	.	kIx.	.
</s>
<s>
Absint	absint	k1gInSc1	absint
má	mít	k5eAaImIp3nS	mít
přírodní	přírodní	k2eAgFnSc4d1	přírodní
zelenou	zelený	k2eAgFnSc4d1	zelená
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
taktéž	taktéž	k?	taktéž
bezbarvý	bezbarvý	k2eAgInSc4d1	bezbarvý
<g/>
.	.	kIx.	.
</s>
<s>
Nápoj	nápoj	k1gInSc1	nápoj
je	být	k5eAaImIp3nS	být
nezřídkakdy	zřídkakdy	k6eNd1	zřídkakdy
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
zelená	zelený	k2eAgFnSc1d1	zelená
víla	víla	k1gFnSc1	víla
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Pojmenování	pojmenování	k1gNnSc1	pojmenování
absint	absint	k1gInSc1	absint
je	být	k5eAaImIp3nS	být
kodifikováno	kodifikovat	k5eAaBmNgNnS	kodifikovat
v	v	k7c6	v
pravidlech	pravidlo	k1gNnPc6	pravidlo
českého	český	k2eAgInSc2d1	český
pravopisu	pravopis	k1gInSc2	pravopis
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
kvůli	kvůli	k7c3	kvůli
velkému	velký	k2eAgInSc3d1	velký
exportu	export	k1gInSc3	export
českých	český	k2eAgInPc2d1	český
absintů	absint	k1gInPc2	absint
jsou	být	k5eAaImIp3nP	být
výrobky	výrobek	k1gInPc1	výrobek
pojmenovány	pojmenován	k2eAgInPc1d1	pojmenován
absinth	absinth	k1gInSc1	absinth
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
absinthe	absinthe	k6eAd1	absinthe
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
mezinárodně	mezinárodně	k6eAd1	mezinárodně
uznávaný	uznávaný	k2eAgInSc4d1	uznávaný
název	název	k1gInSc4	název
tohoto	tento	k3xDgInSc2	tento
nápoje	nápoj	k1gInSc2	nápoj
<g/>
.	.	kIx.	.
</s>
<s>
Absint	absint	k1gInSc4	absint
lze	lze	k6eAd1	lze
dělit	dělit	k5eAaImF	dělit
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
základní	základní	k2eAgInPc4d1	základní
druhy	druh	k1gInPc4	druh
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
je	být	k5eAaImIp3nS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
označuje	označovat	k5eAaImIp3nS	označovat
přídomkem	přídomek	k1gInSc7	přídomek
pravý	pravý	k2eAgMnSc1d1	pravý
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgInSc7	druhý
typem	typ	k1gInSc7	typ
je	být	k5eAaImIp3nS	být
východoevropský	východoevropský	k2eAgInSc1d1	východoevropský
absint	absint	k1gInSc1	absint
<g/>
,	,	kIx,	,
titulovaný	titulovaný	k2eAgInSc1d1	titulovaný
přídomkem	přídomek	k1gInSc7	přídomek
český	český	k2eAgMnSc1d1	český
nebo	nebo	k8xC	nebo
bohémský	bohémský	k2eAgMnSc1d1	bohémský
<g/>
;	;	kIx,	;
často	často	k6eAd1	často
je	být	k5eAaImIp3nS	být
modravé	modravý	k2eAgFnPc4d1	modravá
barvy	barva	k1gFnPc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
rozdíl	rozdíl	k1gInSc1	rozdíl
netkví	tkvět	k5eNaImIp3nS	tkvět
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
vzniku	vznik	k1gInSc2	vznik
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
způsobu	způsob	k1gInSc6	způsob
výroby	výroba	k1gFnSc2	výroba
<g/>
.	.	kIx.	.
</s>
<s>
Pravé	pravý	k2eAgInPc1d1	pravý
absinty	absint	k1gInPc1	absint
se	se	k3xPyFc4	se
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
máčením	máčení	k1gNnSc7	máčení
výše	výše	k1gFnSc2	výše
zmíněných	zmíněný	k2eAgInPc2d1	zmíněný
tří	tři	k4xCgNnPc2	tři
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
bylin	bylina	k1gFnPc2	bylina
v	v	k7c6	v
alkoholu	alkohol	k1gInSc6	alkohol
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc3	jeho
následné	následný	k2eAgFnSc3d1	následná
destilaci	destilace	k1gFnSc3	destilace
a	a	k8xC	a
barvením	barvení	k1gNnPc3	barvení
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
české	český	k2eAgInPc1d1	český
absinty	absint	k1gInPc1	absint
zpravidla	zpravidla	k6eAd1	zpravidla
vynechávají	vynechávat	k5eAaImIp3nP	vynechávat
některé	některý	k3yIgFnPc4	některý
bylinné	bylinný	k2eAgFnPc4d1	bylinná
složky	složka	k1gFnPc4	složka
<g/>
,	,	kIx,	,
obcházejí	obcházet	k5eAaImIp3nP	obcházet
následnou	následný	k2eAgFnSc4d1	následná
destilaci	destilace	k1gFnSc4	destilace
a	a	k8xC	a
výsledný	výsledný	k2eAgInSc1d1	výsledný
produkt	produkt	k1gInSc1	produkt
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
filtruje	filtrovat	k5eAaImIp3nS	filtrovat
<g/>
,	,	kIx,	,
tudíž	tudíž	k8xC	tudíž
je	být	k5eAaImIp3nS	být
oproti	oproti	k7c3	oproti
pravým	pravý	k2eAgInPc3d1	pravý
absintům	absint	k1gInPc3	absint
velmi	velmi	k6eAd1	velmi
hořký	hořký	k2eAgMnSc1d1	hořký
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
zastánci	zastánce	k1gMnPc1	zastánce
pravého	pravý	k2eAgInSc2d1	pravý
absintu	absint	k1gInSc2	absint
tyto	tento	k3xDgFnPc1	tento
"	"	kIx"	"
<g/>
bohémské	bohémský	k2eAgInPc1d1	bohémský
<g/>
"	"	kIx"	"
absinty	absint	k1gInPc1	absint
za	za	k7c4	za
absint	absint	k1gInSc4	absint
nepovažují	považovat	k5eNaImIp3nP	považovat
a	a	k8xC	a
toto	tento	k3xDgNnSc4	tento
základní	základní	k2eAgNnSc4d1	základní
rozdělení	rozdělení	k1gNnSc4	rozdělení
popírají	popírat	k5eAaImIp3nP	popírat
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1915	[number]	k4	1915
byl	být	k5eAaImAgInS	být
absint	absint	k1gInSc1	absint
v	v	k7c6	v
USA	USA	kA	USA
a	a	k8xC	a
takřka	takřka	k6eAd1	takřka
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
<g/>
,	,	kIx,	,
Rakouska-Uherska	Rakouska-Uhersko	k1gNnSc2	Rakouska-Uhersko
<g/>
,	,	kIx,	,
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
,	,	kIx,	,
Švédska	Švédsko	k1gNnSc2	Švédsko
a	a	k8xC	a
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
)	)	kIx)	)
zakázán	zakázat	k5eAaPmNgInS	zakázat
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
konci	konec	k1gInSc6	konec
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
označen	označit	k5eAaPmNgMnS	označit
za	za	k7c2	za
zdraví	zdraví	k1gNnSc2	zdraví
škodlivý	škodlivý	k2eAgInSc4d1	škodlivý
<g/>
,	,	kIx,	,
do	do	k7c2	do
dnešního	dnešní	k2eAgInSc2d1	dnešní
dne	den	k1gInSc2	den
nebyl	být	k5eNaImAgInS	být
doložen	doložen	k2eAgInSc1d1	doložen
důkaz	důkaz	k1gInSc1	důkaz
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
škodlivější	škodlivý	k2eAgInSc4d2	škodlivější
produkt	produkt	k1gInSc4	produkt
<g/>
,	,	kIx,	,
než	než	k8xS	než
kterýkoliv	kterýkoliv	k3yIgInSc1	kterýkoliv
jiný	jiný	k2eAgInSc1d1	jiný
vysokoprocentní	vysokoprocentní	k2eAgInSc1d1	vysokoprocentní
alkoholický	alkoholický	k2eAgInSc1d1	alkoholický
nápoj	nápoj	k1gInSc1	nápoj
<g/>
.	.	kIx.	.
</s>
<s>
Psychotropní	psychotropní	k2eAgInPc1d1	psychotropní
účinky	účinek	k1gInPc1	účinek
absintu	absint	k1gInSc2	absint
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
dnes	dnes	k6eAd1	dnes
nikoli	nikoli	k9	nikoli
výjimečně	výjimečně	k6eAd1	výjimečně
přeceňovány	přeceňován	k2eAgFnPc1d1	přeceňována
<g/>
.	.	kIx.	.
</s>
<s>
Oživení	oživení	k1gNnSc1	oživení
absintu	absint	k1gInSc2	absint
přišlo	přijít	k5eAaPmAgNnS	přijít
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Evropská	evropský	k2eAgFnSc1d1	Evropská
unie	unie	k1gFnSc1	unie
znovu	znovu	k6eAd1	znovu
povolila	povolit	k5eAaPmAgFnS	povolit
jeho	jeho	k3xOp3gFnSc4	jeho
výrobu	výroba	k1gFnSc4	výroba
a	a	k8xC	a
prodej	prodej	k1gInSc4	prodej
<g/>
;	;	kIx,	;
bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc4	ten
však	však	k9	však
podmíněno	podmíněn	k2eAgNnSc1d1	podmíněno
regulacemi	regulace	k1gFnPc7	regulace
<g/>
,	,	kIx,	,
týkajícími	týkající	k2eAgFnPc7d1	týkající
se	se	k3xPyFc4	se
obsahu	obsah	k1gInSc2	obsah
thujonu	thujon	k1gMnSc3	thujon
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
se	se	k3xPyFc4	se
na	na	k7c6	na
světě	svět	k1gInSc6	svět
vyrábělo	vyrábět	k5eAaImAgNnS	vyrábět
kolem	kolem	k7c2	kolem
dvou	dva	k4xCgNnPc2	dva
set	sto	k4xCgNnPc2	sto
značek	značka	k1gFnPc2	značka
absintů	absint	k1gInPc2	absint
<g/>
;	;	kIx,	;
mezi	mezi	k7c7	mezi
zeměmi	zem	k1gFnPc7	zem
původu	původ	k1gInSc2	původ
své	svůj	k3xOyFgNnSc4	svůj
zastoupení	zastoupení	k1gNnSc4	zastoupení
měly	mít	k5eAaImAgInP	mít
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
Španělsko	Španělsko	k1gNnSc1	Španělsko
a	a	k8xC	a
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
většina	většina	k1gFnSc1	většina
zemí	zem	k1gFnPc2	zem
nemá	mít	k5eNaImIp3nS	mít
zákonnou	zákonný	k2eAgFnSc4d1	zákonná
definici	definice	k1gFnSc4	definice
absintu	absint	k1gInSc2	absint
(	(	kIx(	(
<g/>
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
skotské	skotský	k2eAgFnSc2d1	skotská
whisky	whisky	k1gFnSc2	whisky
<g/>
,	,	kIx,	,
brandy	brandy	k1gFnSc2	brandy
nebo	nebo	k8xC	nebo
ginu	gin	k1gInSc2	gin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tudíž	tudíž	k8xC	tudíž
výrobci	výrobce	k1gMnPc1	výrobce
mohou	moct	k5eAaImIp3nP	moct
svůj	svůj	k3xOyFgInSc4	svůj
produkt	produkt	k1gInSc4	produkt
nazvat	nazvat	k5eAaBmF	nazvat
absintem	absint	k1gInSc7	absint
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nP	by
museli	muset	k5eAaImAgMnP	muset
dodržet	dodržet	k5eAaPmF	dodržet
jakoukoliv	jakýkoliv	k3yIgFnSc4	jakýkoliv
normu	norma	k1gFnSc4	norma
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
je	být	k5eAaImIp3nS	být
kupř	kupř	kA	kupř
<g/>
.	.	kIx.	.
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
-	-	kIx~	-
tamější	tamější	k2eAgInSc1d1	tamější
absint	absint	k1gInSc1	absint
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
po	po	k7c6	po
maceraci	macerace	k1gFnSc6	macerace
destilován	destilován	k2eAgInSc1d1	destilován
a	a	k8xC	a
nesmí	smět	k5eNaImIp3nS	smět
být	být	k5eAaImF	být
uměle	uměle	k6eAd1	uměle
barven	barven	k2eAgMnSc1d1	barven
<g/>
.	.	kIx.	.
</s>
<s>
Bylinné	bylinný	k2eAgFnPc1d1	bylinná
přísady	přísada	k1gFnPc1	přísada
jsou	být	k5eAaImIp3nP	být
macerovány	macerovat	k5eAaImNgFnP	macerovat
v	v	k7c6	v
alkoholu	alkohol	k1gInSc6	alkohol
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
je	být	k5eAaImIp3nS	být
výsledný	výsledný	k2eAgInSc1d1	výsledný
macerát	macerát	k1gInSc1	macerát
destilován	destilován	k2eAgInSc1d1	destilován
<g/>
.	.	kIx.	.
</s>
<s>
Destilace	destilace	k1gFnSc1	destilace
je	být	k5eAaImIp3nS	být
dvou-	dvou-	k?	dvou-
nebo	nebo	k8xC	nebo
třífázová	třífázový	k2eAgFnSc1d1	třífázová
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
v	v	k7c6	v
určitých	určitý	k2eAgFnPc6d1	určitá
fázích	fáze	k1gFnPc6	fáze
se	se	k3xPyFc4	se
bylinky	bylinka	k1gFnPc1	bylinka
obměňují	obměňovat	k5eAaImIp3nP	obměňovat
a	a	k8xC	a
doplňují	doplňovat	k5eAaImIp3nP	doplňovat
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
finální	finální	k2eAgInSc1d1	finální
(	(	kIx(	(
<g/>
průtahový	průtahový	k2eAgInSc1d1	průtahový
<g/>
)	)	kIx)	)
destilát	destilát	k1gInSc1	destilát
je	být	k5eAaImIp3nS	být
naprosto	naprosto	k6eAd1	naprosto
bezbarvý	bezbarvý	k2eAgMnSc1d1	bezbarvý
a	a	k8xC	a
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
zhruba	zhruba	k6eAd1	zhruba
od	od	k7c2	od
65	[number]	k4	65
<g/>
%	%	kIx~	%
do	do	k7c2	do
80	[number]	k4	80
<g/>
%	%	kIx~	%
alkoholu	alkohol	k1gInSc2	alkohol
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
upravený	upravený	k2eAgInSc1d1	upravený
destilát	destilát	k1gInSc1	destilát
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
rovnou	rovnou	k6eAd1	rovnou
stáčen	stáčet	k5eAaImNgInS	stáčet
a	a	k8xC	a
prodáván	prodávat	k5eAaImNgInS	prodávat
jako	jako	k9	jako
bílý	bílý	k2eAgInSc1d1	bílý
absint	absint	k1gInSc1	absint
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
blanche	blanche	k1gInSc1	blanche
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
la	la	k1gNnSc1	la
Bleue	Bleue	k1gNnSc1	Bleue
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
destilát	destilát	k1gInSc1	destilát
dále	daleko	k6eAd2	daleko
barven	barven	k2eAgInSc1d1	barven
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
získal	získat	k5eAaPmAgMnS	získat
zelenou	zelený	k2eAgFnSc4d1	zelená
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
absint	absint	k1gInSc4	absint
verte	verte	k1gNnSc2	verte
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
zelený	zelený	k2eAgInSc1d1	zelený
<g/>
.	.	kIx.	.
</s>
<s>
Zelenou	zelený	k2eAgFnSc4d1	zelená
barvu	barva	k1gFnSc4	barva
dodává	dodávat	k5eAaImIp3nS	dodávat
absintu	absint	k1gInSc2	absint
chlorofyl	chlorofyl	k1gInSc4	chlorofyl
z	z	k7c2	z
bylinek	bylinka	k1gFnPc2	bylinka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
jsou	být	k5eAaImIp3nP	být
při	při	k7c6	při
barvení	barvení	k1gNnSc6	barvení
použity	použit	k2eAgInPc1d1	použit
<g/>
.	.	kIx.	.
</s>
<s>
Chlorofyl	chlorofyl	k1gInSc1	chlorofyl
postupem	postupem	k7c2	postupem
času	čas	k1gInSc2	čas
v	v	k7c6	v
lahvi	lahev	k1gFnSc6	lahev
hnědne	hnědnout	k5eAaImIp3nS	hnědnout
(	(	kIx(	(
<g/>
vizte	vidět	k5eAaImRp2nP	vidět
raritní	raritní	k2eAgInSc4d1	raritní
pre-ban	prean	k1gInSc4	pre-ban
absinty	absint	k1gInPc7	absint
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
koutech	kout	k1gInPc6	kout
světa	svět	k1gInSc2	svět
po	po	k7c4	po
sto	sto	k4xCgNnSc4	sto
letech	let	k1gInPc6	let
nalezeny	nalezen	k2eAgFnPc1d1	nalezena
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
technologického	technologický	k2eAgInSc2d1	technologický
postupu	postup	k1gInSc2	postup
výroby	výroba	k1gFnSc2	výroba
některých	některý	k3yIgFnPc2	některý
českých	český	k2eAgFnPc2d1	Česká
firem	firma	k1gFnPc2	firma
je	být	k5eAaImIp3nS	být
loužení	loužení	k1gNnSc1	loužení
pelyňku	pelyněk	k1gInSc2	pelyněk
v	v	k7c6	v
horké	horký	k2eAgFnSc6d1	horká
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
uvolní	uvolnit	k5eAaPmIp3nS	uvolnit
aroma	aroma	k1gNnSc1	aroma
<g/>
,	,	kIx,	,
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
i	i	k9	i
pro	pro	k7c4	pro
absint	absint	k1gInSc4	absint
příznačná	příznačný	k2eAgFnSc1d1	příznačná
barva	barva	k1gFnSc1	barva
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
firmy	firma	k1gFnPc1	firma
využívají	využívat	k5eAaImIp3nP	využívat
podobný	podobný	k2eAgInSc4d1	podobný
postup	postup	k1gInSc4	postup
jako	jako	k9	jako
u	u	k7c2	u
pravých	pravý	k2eAgInPc2d1	pravý
absintů	absint	k1gInPc2	absint
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
louží	loužit	k5eAaImIp3nP	loužit
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
macerují	macerovat	k5eAaImIp3nP	macerovat
v	v	k7c6	v
alkoholu	alkohol	k1gInSc6	alkohol
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
filtrují	filtrovat	k5eAaImIp3nP	filtrovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neprovádějí	provádět	k5eNaImIp3nP	provádět
finální	finální	k2eAgFnSc4d1	finální
destilaci	destilace	k1gFnSc4	destilace
<g/>
.	.	kIx.	.
</s>
<s>
Vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
absint	absint	k1gInSc1	absint
macerací	macerace	k1gFnPc2	macerace
v	v	k7c6	v
alkoholu	alkohol	k1gInSc6	alkohol
<g/>
,	,	kIx,	,
produkt	produkt	k1gInSc4	produkt
již	již	k6eAd1	již
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
macerace	macerace	k1gFnSc1	macerace
mu	on	k3xPp3gMnSc3	on
pro	pro	k7c4	pro
něj	on	k3xPp3gInSc4	on
typicky	typicky	k6eAd1	typicky
zelenou	zelený	k2eAgFnSc4d1	zelená
barvu	barva	k1gFnSc4	barva
poskytne	poskytnout	k5eAaPmIp3nS	poskytnout
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
dále	daleko	k6eAd2	daleko
barvit	barvit	k5eAaImF	barvit
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
postupu	postup	k1gInSc6	postup
loužení	loužení	k1gNnSc2	loužení
bylin	bylina	k1gFnPc2	bylina
v	v	k7c6	v
horké	horký	k2eAgFnSc6d1	horká
vodě	voda	k1gFnSc6	voda
se	se	k3xPyFc4	se
naopak	naopak	k6eAd1	naopak
zpravidla	zpravidla	k6eAd1	zpravidla
následně	následně	k6eAd1	následně
provádí	provádět	k5eAaImIp3nS	provádět
barvení	barvení	k1gNnSc1	barvení
pomocí	pomocí	k7c2	pomocí
umělých	umělý	k2eAgNnPc2d1	umělé
barviv	barvivo	k1gNnPc2	barvivo
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
nápoje	nápoj	k1gInPc1	nápoj
získávají	získávat	k5eAaImIp3nP	získávat
výrazně	výrazně	k6eAd1	výrazně
zelenou	zelený	k2eAgFnSc4d1	zelená
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
až	až	k9	až
fluorescenční	fluorescenční	k2eAgFnSc4d1	fluorescenční
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Absint	absint	k1gInSc1	absint
byl	být	k5eAaImAgInS	být
dlouho	dlouho	k6eAd1	dlouho
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
halucinogenní	halucinogenní	k2eAgInSc4d1	halucinogenní
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
domněnka	domněnka	k1gFnSc1	domněnka
byla	být	k5eAaImAgFnS	být
posílena	posílit	k5eAaPmNgFnS	posílit
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
zmínkou	zmínka	k1gFnSc7	zmínka
ve	v	k7c6	v
vědeckém	vědecký	k2eAgInSc6d1	vědecký
časopisu	časopis	k1gInSc6	časopis
<g/>
,	,	kIx,	,
že	že	k8xS	že
thujon	thujon	k1gInSc1	thujon
se	se	k3xPyFc4	se
podobá	podobat	k5eAaImIp3nS	podobat
THC	THC	kA	THC
-	-	kIx~	-
aktivní	aktivní	k2eAgFnSc6d1	aktivní
látce	látka	k1gFnSc6	látka
v	v	k7c6	v
konopí	konopí	k1gNnSc6	konopí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
francouzský	francouzský	k2eAgMnSc1d1	francouzský
doktor	doktor	k1gMnSc1	doktor
Magnan	Magnan	k1gMnSc1	Magnan
zkoumal	zkoumat	k5eAaImAgMnS	zkoumat
250	[number]	k4	250
případů	případ	k1gInPc2	případ
alkoholismu	alkoholismus	k1gInSc2	alkoholismus
a	a	k8xC	a
došel	dojít	k5eAaPmAgInS	dojít
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
absintoví	absintový	k2eAgMnPc1d1	absintový
pijáci	piják	k1gMnPc1	piják
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
byli	být	k5eAaImAgMnP	být
hůře	zle	k6eAd2	zle
než	než	k8xS	než
obyčejní	obyčejný	k2eAgMnPc1d1	obyčejný
alkoholici	alkoholik	k1gMnPc1	alkoholik
a	a	k8xC	a
že	že	k8xS	že
navíc	navíc	k6eAd1	navíc
trpěli	trpět	k5eAaImAgMnP	trpět
vážnými	vážný	k2eAgFnPc7d1	vážná
halucinacemi	halucinace	k1gFnPc7	halucinace
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
závěr	závěr	k1gInSc1	závěr
byl	být	k5eAaImAgInS	být
odpůrci	odpůrce	k1gMnPc7	odpůrce
absintu	absint	k1gInSc3	absint
přijat	přijat	k2eAgMnSc1d1	přijat
velice	velice	k6eAd1	velice
vlídně	vlídně	k6eAd1	vlídně
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
absintu	absint	k1gInSc2	absint
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
paradoxně	paradoxně	k6eAd1	paradoxně
přitížili	přitížit	k5eAaPmAgMnP	přitížit
bohémští	bohémský	k2eAgMnPc1d1	bohémský
umělci	umělec	k1gMnPc1	umělec
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
Oscar	Oscar	k1gInSc1	Oscar
Wilde	Wild	k1gInSc5	Wild
<g/>
,	,	kIx,	,
Paul	Paul	k1gMnSc1	Paul
Verlaine	Verlain	k1gInSc5	Verlain
<g/>
,	,	kIx,	,
Toulouse	Toulouse	k1gInSc7	Toulouse
Lautrec	Lautrec	k1gInSc1	Lautrec
nebo	nebo	k8xC	nebo
Vincent	Vincent	k1gMnSc1	Vincent
van	vana	k1gFnPc2	vana
Gogh	Gogh	k1gMnSc1	Gogh
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
si	se	k3xPyFc3	se
údajně	údajně	k6eAd1	údajně
pod	pod	k7c7	pod
jeho	jeho	k3xOp3gInSc7	jeho
vlivem	vliv	k1gInSc7	vliv
uřízl	uříznout	k5eAaPmAgInS	uříznout
ucho	ucho	k1gNnSc4	ucho
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
že	že	k8xS	že
absint	absint	k1gInSc1	absint
halucinace	halucinace	k1gFnSc2	halucinace
nezpůsobuje	způsobovat	k5eNaImIp3nS	způsobovat
<g/>
.	.	kIx.	.
</s>
<s>
Thujon	Thujon	k1gInSc1	Thujon
<g/>
,	,	kIx,	,
domnělý	domnělý	k2eAgInSc1d1	domnělý
aktivní	aktivní	k2eAgInSc4d1	aktivní
prvek	prvek	k1gInSc4	prvek
v	v	k7c6	v
absintu	absint	k1gInSc6	absint
<g/>
,	,	kIx,	,
sice	sice	k8xC	sice
ve	v	k7c6	v
velkých	velký	k2eAgFnPc6d1	velká
dávkách	dávka	k1gFnPc6	dávka
může	moct	k5eAaImIp3nS	moct
způsobit	způsobit	k5eAaPmF	způsobit
svalové	svalový	k2eAgFnPc4d1	svalová
křeče	křeč	k1gFnPc4	křeč
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
není	být	k5eNaImIp3nS	být
prokázáno	prokázat	k5eAaPmNgNnS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
způsoboval	způsobovat	k5eAaImAgMnS	způsobovat
halucinace	halucinace	k1gFnSc1	halucinace
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
hlášené	hlášený	k2eAgInPc1d1	hlášený
případy	případ	k1gInPc1	případ
halucinací	halucinace	k1gFnSc7	halucinace
z	z	k7c2	z
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byly	být	k5eAaImAgInP	být
způsobeny	způsobit	k5eAaPmNgInP	způsobit
jedovatými	jedovatý	k2eAgFnPc7d1	jedovatá
chemikáliemi	chemikálie	k1gFnPc7	chemikálie
<g/>
,	,	kIx,	,
kupř	kupř	kA	kupř
<g/>
.	.	kIx.	.
modrou	modrý	k2eAgFnSc7d1	modrá
skalicí	skalice	k1gFnSc7	skalice
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
přidávaly	přidávat	k5eAaImAgFnP	přidávat
do	do	k7c2	do
levných	levný	k2eAgInPc2d1	levný
absintů	absint	k1gInPc2	absint
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
docílilo	docílit	k5eAaPmAgNnS	docílit
zajímavější	zajímavý	k2eAgFnPc4d2	zajímavější
barvy	barva	k1gFnPc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
výše	výše	k1gFnPc4	výše
uvedené	uvedený	k2eAgFnPc4d1	uvedená
se	se	k3xPyFc4	se
i	i	k9	i
dnes	dnes	k6eAd1	dnes
vedou	vést	k5eAaImIp3nP	vést
diskuse	diskuse	k1gFnPc1	diskuse
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
má	mít	k5eAaImIp3nS	mít
absint	absint	k1gInSc1	absint
jiné	jiný	k2eAgInPc1d1	jiný
účinky	účinek	k1gInPc1	účinek
než	než	k8xS	než
alkoholové	alkoholový	k2eAgNnSc1d1	alkoholové
opojení	opojení	k1gNnSc1	opojení
<g/>
.	.	kIx.	.
</s>
<s>
Umělci	umělec	k1gMnPc1	umělec
popisovali	popisovat	k5eAaImAgMnP	popisovat
účinky	účinek	k1gInPc4	účinek
absintu	absint	k1gInSc2	absint
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
mysl	mysl	k1gFnSc1	mysl
otevírající	otevírající	k2eAgFnSc1d1	otevírající
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
obecně	obecně	k6eAd1	obecně
je	být	k5eAaImIp3nS	být
nejčastější	častý	k2eAgFnSc7d3	nejčastější
zkušeností	zkušenost	k1gFnSc7	zkušenost
jakási	jakýsi	k3yIgFnSc1	jakýsi
"	"	kIx"	"
<g/>
střízlivější	střízlivý	k2eAgFnSc1d2	střízlivější
opilost	opilost	k1gFnSc1	opilost
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
znalci	znalec	k1gMnPc1	znalec
absintu	absint	k1gInSc2	absint
tento	tento	k3xDgInSc4	tento
jev	jev	k1gInSc4	jev
vysvětlují	vysvětlovat	k5eAaImIp3nP	vysvětlovat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
některé	některý	k3yIgInPc4	některý
z	z	k7c2	z
široké	široký	k2eAgFnSc2d1	široká
škály	škála	k1gFnSc2	škála
bylinek	bylinka	k1gFnPc2	bylinka
působí	působit	k5eAaImIp3nS	působit
jako	jako	k9	jako
stimulanty	stimulant	k1gInPc4	stimulant
a	a	k8xC	a
některé	některý	k3yIgFnPc4	některý
naopak	naopak	k6eAd1	naopak
jako	jako	k9	jako
sedativa	sedativum	k1gNnSc2	sedativum
<g/>
.	.	kIx.	.
</s>
<s>
Domnělé	domnělý	k2eAgInPc1d1	domnělý
halucinogenní	halucinogenní	k2eAgInPc1d1	halucinogenní
účinky	účinek	k1gInPc1	účinek
thujonu	thujon	k1gInSc2	thujon
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
paradoxně	paradoxně	k6eAd1	paradoxně
marketingově	marketingově	k6eAd1	marketingově
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
některým	některý	k3yIgMnPc3	některý
výrobcům	výrobce	k1gMnPc3	výrobce
při	při	k7c6	při
propagaci	propagace	k1gFnSc6	propagace
jejich	jejich	k3xOp3gInPc2	jejich
výrobků	výrobek	k1gInPc2	výrobek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2008	[number]	k4	2008
byla	být	k5eAaImAgFnS	být
zveřejněna	zveřejnit	k5eAaPmNgFnS	zveřejnit
studie	studie	k1gFnSc1	studie
předních	přední	k2eAgMnPc2d1	přední
znalců	znalec	k1gMnPc2	znalec
absintu	absint	k1gInSc2	absint
zaměřená	zaměřený	k2eAgFnSc1d1	zaměřená
na	na	k7c4	na
analýzu	analýza	k1gFnSc4	analýza
"	"	kIx"	"
<g/>
starých	starý	k2eAgInPc2d1	starý
<g/>
"	"	kIx"	"
absintů	absint	k1gInPc2	absint
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc6	jejich
obsahu	obsah	k1gInSc6	obsah
thujonu	thujona	k1gFnSc4	thujona
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
vědecky	vědecky	k6eAd1	vědecky
dokázáno	dokázat	k5eAaPmNgNnS	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
absinty	absint	k1gInPc1	absint
z	z	k7c2	z
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
počátku	počátek	k1gInSc2	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
obsahovaly	obsahovat	k5eAaImAgInP	obsahovat
nanejvýš	nanejvýš	k6eAd1	nanejvýš
48,3	[number]	k4	48,3
mg	mg	kA	mg
<g/>
.	.	kIx.	.
<g/>
l-	l-	k?	l-
<g/>
1	[number]	k4	1
thujonu	thujon	k1gInSc2	thujon
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
bylo	být	k5eAaImAgNnS	být
popřeno	popřen	k2eAgNnSc4d1	popřeno
tvrzení	tvrzení	k1gNnSc4	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
původní	původní	k2eAgInPc4d1	původní
absinty	absint	k1gInPc4	absint
obsahovaly	obsahovat	k5eAaImAgFnP	obsahovat
několikanásobně	několikanásobně	k6eAd1	několikanásobně
větší	veliký	k2eAgNnSc4d2	veliký
množství	množství	k1gNnSc4	množství
thujonu	thujon	k1gInSc2	thujon
než	než	k8xS	než
absinty	absint	k1gInPc1	absint
dnešní	dnešní	k2eAgInPc1d1	dnešní
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
českých	český	k2eAgFnPc2d1	Česká
norem	norma	k1gFnPc2	norma
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
v	v	k7c6	v
nápoji	nápoj	k1gInSc6	nápoj
o	o	k7c6	o
obsahu	obsah	k1gInSc6	obsah
alkoholu	alkohol	k1gInSc2	alkohol
nejméně	málo	k6eAd3	málo
25	[number]	k4	25
%	%	kIx~	%
obsaženo	obsáhnout	k5eAaPmNgNnS	obsáhnout
více	hodně	k6eAd2	hodně
než	než	k8xS	než
35	[number]	k4	35
mg	mg	kA	mg
<g/>
.	.	kIx.	.
<g/>
l-	l-	k?	l-
<g/>
1	[number]	k4	1
alfa	alfa	k1gNnSc1	alfa
a	a	k8xC	a
beta	beta	k1gNnSc1	beta
thujonu	thujon	k1gInSc2	thujon
<g/>
.	.	kIx.	.
</s>
<s>
Přesný	přesný	k2eAgInSc1d1	přesný
původ	původ	k1gInSc1	původ
absintu	absint	k1gInSc2	absint
není	být	k5eNaImIp3nS	být
jasný	jasný	k2eAgInSc1d1	jasný
<g/>
.	.	kIx.	.
</s>
<s>
Lékařské	lékařský	k2eAgNnSc1d1	lékařské
využití	využití	k1gNnSc1	využití
pelyňku	pelyněk	k1gInSc2	pelyněk
je	být	k5eAaImIp3nS	být
zmíněno	zmínit	k5eAaPmNgNnS	zmínit
na	na	k7c6	na
starém	starý	k2eAgInSc6d1	starý
egyptském	egyptský	k2eAgInSc6d1	egyptský
papyru	papyr	k1gInSc6	papyr
z	z	k7c2	z
období	období	k1gNnSc2	období
roku	rok	k1gInSc2	rok
1550	[number]	k4	1550
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Výtažky	výtažek	k1gInPc4	výtažek
z	z	k7c2	z
pelyňku	pelyněk	k1gInSc2	pelyněk
byly	být	k5eAaImAgFnP	být
používány	používat	k5eAaImNgFnP	používat
jako	jako	k8xC	jako
lék	lék	k1gInSc1	lék
i	i	k9	i
ve	v	k7c6	v
starém	starý	k2eAgNnSc6d1	staré
Řecku	Řecko	k1gNnSc6	Řecko
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yQgInSc2	který
je	být	k5eAaImIp3nS	být
doložena	doložen	k2eAgFnSc1d1	doložena
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
pelyňkovém	pelyňkový	k2eAgNnSc6d1	pelyňkové
víně	víno	k1gNnSc6	víno
<g/>
,	,	kIx,	,
absinthites	absinthites	k1gMnSc1	absinthites
oinos	oinos	k1gMnSc1	oinos
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
ověřená	ověřený	k2eAgFnSc1d1	ověřená
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
absintu	absint	k1gInSc6	absint
jakožto	jakožto	k8xS	jakožto
destilátu	destilát	k1gInSc2	destilát
se	se	k3xPyFc4	se
datuje	datovat	k5eAaImIp3nS	datovat
až	až	k9	až
od	od	k7c2	od
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
legendy	legenda	k1gFnSc2	legenda
historie	historie	k1gFnSc2	historie
absintu	absint	k1gInSc2	absint
započala	započnout	k5eAaPmAgFnS	započnout
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1792	[number]	k4	1792
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
všeléku	všelék	k1gInSc2	všelék
od	od	k7c2	od
lékaře	lékař	k1gMnSc2	lékař
ze	z	k7c2	z
švýcarského	švýcarský	k2eAgInSc2d1	švýcarský
Couvetu	Couvet	k1gInSc2	Couvet
<g/>
,	,	kIx,	,
Pierra	Pierra	k1gMnSc1	Pierra
Ordinaira	Ordinaira	k1gMnSc1	Ordinaira
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
recept	recept	k1gInSc1	recept
byl	být	k5eAaImAgInS	být
poté	poté	k6eAd1	poté
předán	předat	k5eAaPmNgInS	předat
sestrám	sestra	k1gFnPc3	sestra
Henriodovým	Henriodový	k2eAgNnPc3d1	Henriodový
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
absint	absint	k1gInSc1	absint
prodávaly	prodávat	k5eAaImAgFnP	prodávat
jako	jako	k8xC	jako
léčivý	léčivý	k2eAgInSc4d1	léčivý
elixír	elixír	k1gInSc4	elixír
<g/>
.	.	kIx.	.
</s>
<s>
Jiná	jiný	k2eAgFnSc1d1	jiná
verze	verze	k1gFnSc1	verze
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
sestry	sestra	k1gFnPc1	sestra
absint	absint	k1gInSc4	absint
prodávaly	prodávat	k5eAaImAgFnP	prodávat
už	už	k6eAd1	už
před	před	k7c7	před
příchodem	příchod	k1gInSc7	příchod
doktora	doktor	k1gMnSc2	doktor
Ordinaira	Ordinair	k1gMnSc2	Ordinair
do	do	k7c2	do
Couvetu	Couvet	k1gInSc2	Couvet
<g/>
.	.	kIx.	.
</s>
<s>
Každopádně	každopádně	k6eAd1	každopádně
je	být	k5eAaImIp3nS	být
potvrzeno	potvrzen	k2eAgNnSc1d1	potvrzeno
<g/>
,	,	kIx,	,
že	že	k8xS	že
jistý	jistý	k2eAgMnSc1d1	jistý
major	major	k1gMnSc1	major
Dubied	Dubied	k1gMnSc1	Dubied
získal	získat	k5eAaPmAgMnS	získat
od	od	k7c2	od
sester	sestra	k1gFnPc2	sestra
recept	recept	k1gInSc4	recept
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1797	[number]	k4	1797
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgMnSc7	svůj
synem	syn	k1gMnSc7	syn
Marcellinem	Marcellin	k1gInSc7	Marcellin
a	a	k8xC	a
zetěm	zeť	k1gMnSc7	zeť
Henry-Louisem	Henry-Louis	k1gInSc7	Henry-Louis
Pernodem	Pernod	k1gInSc7	Pernod
založil	založit	k5eAaPmAgMnS	založit
v	v	k7c4	v
Couvetu	Couveta	k1gFnSc4	Couveta
první	první	k4xOgInSc1	první
absintový	absintový	k2eAgInSc1d1	absintový
lihovar	lihovar	k1gInSc1	lihovar
-	-	kIx~	-
Dubied	Dubied	k1gInSc1	Dubied
Pè	Pè	k1gFnSc2	Pè
et	et	k?	et
Fils	Fils	k1gInSc1	Fils
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1805	[number]	k4	1805
byl	být	k5eAaImAgMnS	být
ve	v	k7c6	v
francouzském	francouzský	k2eAgNnSc6d1	francouzské
městě	město	k1gNnSc6	město
Pontarlier	Pontarliero	k1gNnPc2	Pontarliero
uveden	uvést	k5eAaPmNgInS	uvést
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
druhý	druhý	k4xOgInSc1	druhý
lihovar	lihovar	k1gInSc1	lihovar
-	-	kIx~	-
nesl	nést	k5eAaImAgInS	nést
název	název	k1gInSc1	název
Maison	Maison	k1gNnSc1	Maison
Pernod	Pernoda	k1gFnPc2	Pernoda
Fils	Filsa	k1gFnPc2	Filsa
a	a	k8xC	a
absintovému	absintový	k2eAgInSc3d1	absintový
trhu	trh	k1gInSc3	trh
dominoval	dominovat	k5eAaImAgInS	dominovat
až	až	k6eAd1	až
do	do	k7c2	do
zákazu	zákaz	k1gInSc2	zákaz
absintu	absint	k1gInSc2	absint
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1915	[number]	k4	1915
<g/>
.	.	kIx.	.
</s>
<s>
Kořeny	kořen	k2eAgFnPc1d1	Kořena
oblíbenosti	oblíbenost	k1gFnPc1	oblíbenost
absintu	absint	k1gInSc2	absint
sahají	sahat	k5eAaImIp3nP	sahat
do	do	k7c2	do
40	[number]	k4	40
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
absint	absint	k1gInSc1	absint
podáván	podávat	k5eAaImNgInS	podávat
na	na	k7c4	na
příděl	příděl	k1gInSc4	příděl
francouzským	francouzský	k2eAgMnSc7d1	francouzský
legionářům	legionář	k1gMnPc3	legionář
jako	jako	k9	jako
lék	lék	k1gInSc1	lék
proti	proti	k7c3	proti
malárii	malárie	k1gFnSc3	malárie
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
vojáci	voják	k1gMnPc1	voják
vrátili	vrátit	k5eAaPmAgMnP	vrátit
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
přivezli	přivézt	k5eAaPmAgMnP	přivézt
s	s	k7c7	s
sebou	se	k3xPyFc7	se
i	i	k8xC	i
chuť	chuť	k1gFnSc4	chuť
na	na	k7c4	na
absint	absint	k1gInSc4	absint
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
absint	absint	k1gInSc1	absint
stal	stát	k5eAaPmAgInS	stát
již	již	k9	již
natolik	natolik	k6eAd1	natolik
populárním	populární	k2eAgMnSc6d1	populární
<g/>
,	,	kIx,	,
že	že	k8xS	že
páté	pátý	k4xOgFnSc3	pátý
hodině	hodina	k1gFnSc3	hodina
odpolední	odpolední	k1gNnSc2	odpolední
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
říkat	říkat	k5eAaImF	říkat
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
heure	heur	k1gInSc5	heur
verte	verte	k1gNnPc7	verte
-	-	kIx~	-
"	"	kIx"	"
<g/>
zelená	zelený	k2eAgFnSc1d1	zelená
hodinka	hodinka	k1gFnSc1	hodinka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Absint	absint	k1gInSc1	absint
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
těšit	těšit	k5eAaImF	těšit
oblibě	obliba	k1gFnSc3	obliba
všech	všecek	k3xTgFnPc2	všecek
společenských	společenský	k2eAgFnPc2d1	společenská
vrstev	vrstva	k1gFnPc2	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
Francii	Francie	k1gFnSc4	Francie
a	a	k8xC	a
Švýcarsko	Švýcarsko	k1gNnSc4	Švýcarsko
se	se	k3xPyFc4	se
absint	absint	k1gInSc1	absint
konzumoval	konzumovat	k5eAaBmAgInS	konzumovat
kupř	kupř	kA	kupř
<g/>
.	.	kIx.	.
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
,	,	kIx,	,
v	v	k7c6	v
USA	USA	kA	USA
(	(	kIx(	(
<g/>
existují	existovat	k5eAaImIp3nP	existovat
zmínky	zmínka	k1gFnPc4	zmínka
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
konzumaci	konzumace	k1gFnSc6	konzumace
v	v	k7c6	v
New	New	k1gFnSc6	New
Orleans	Orleans	k1gInSc1	Orleans
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
půdě	půda	k1gFnSc6	půda
dnešní	dnešní	k2eAgFnSc2d1	dnešní
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
tehdy	tehdy	k6eAd1	tehdy
byla	být	k5eAaImAgFnS	být
součástí	součást	k1gFnSc7	součást
Rakouska-Uherska	Rakouska-Uhersko	k1gNnSc2	Rakouska-Uhersko
<g/>
.	.	kIx.	.
</s>
<s>
Absint	absint	k1gInSc1	absint
se	se	k3xPyFc4	se
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
rozšířil	rozšířit	k5eAaPmAgMnS	rozšířit
v	v	k7c4	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
;	;	kIx,	;
to	ten	k3xDgNnSc1	ten
zvláště	zvláště	k6eAd1	zvláště
díky	díky	k7c3	díky
českým	český	k2eAgMnPc3d1	český
umělcům	umělec	k1gMnPc3	umělec
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Viktoru	Viktor	k1gMnSc3	Viktor
Olivovi	Oliva	k1gMnSc3	Oliva
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
zemí	zem	k1gFnPc2	zem
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
produkci	produkce	k1gFnSc4	produkce
absintu	absint	k1gInSc2	absint
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1915	[number]	k4	1915
nezakázaly	zakázat	k5eNaPmAgFnP	zakázat
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
právě	právě	k9	právě
Čechy	Čechy	k1gFnPc1	Čechy
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
absint	absint	k1gInSc1	absint
popíjel	popíjet	k5eAaImAgInS	popíjet
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
uměleckých	umělecký	k2eAgFnPc6d1	umělecká
kavárnách	kavárna	k1gFnPc6	kavárna
a	a	k8xC	a
vinárnách	vinárna	k1gFnPc6	vinárna
(	(	kIx(	(
<g/>
kupř	kupř	kA	kupř
<g/>
.	.	kIx.	.
v	v	k7c6	v
kavárně	kavárna	k1gFnSc6	kavárna
Slavii	slavie	k1gFnSc6	slavie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
byla	být	k5eAaImAgFnS	být
Albínem	Albín	k1gMnSc7	Albín
Hillem	Hill	k1gInSc7	Hill
založena	založen	k2eAgFnSc1d1	založena
likérka	likérka	k1gFnSc1	likérka
Hills	Hillsa	k1gFnPc2	Hillsa
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnPc4	jejíž
pokračovatelé	pokračovatel	k1gMnPc1	pokračovatel
v	v	k7c4	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
tvrdili	tvrdit	k5eAaImAgMnP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Hill	Hill	k1gMnSc1	Hill
absint	absint	k1gInSc4	absint
vyráběl	vyrábět	k5eAaImAgMnS	vyrábět
již	již	k9	již
za	za	k7c4	za
První	první	k4xOgFnPc4	první
republiky	republika	k1gFnPc4	republika
<g/>
.	.	kIx.	.
</s>
<s>
Hillův	Hillův	k2eAgInSc1d1	Hillův
absint	absint	k1gInSc1	absint
byl	být	k5eAaImAgInS	být
prý	prý	k9	prý
velmi	velmi	k6eAd1	velmi
populární	populární	k2eAgMnSc1d1	populární
zvláště	zvláště	k6eAd1	zvláště
během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
rozléval	rozlévat	k5eAaImAgMnS	rozlévat
na	na	k7c4	na
příděl	příděl	k1gInSc4	příděl
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
tomto	tento	k3xDgNnSc6	tento
tvrzení	tvrzení	k1gNnSc6	tvrzení
postavena	postaven	k2eAgFnSc1d1	postavena
česká	český	k2eAgFnSc1d1	Česká
absintová	absintový	k2eAgFnSc1d1	absintová
tradice	tradice	k1gFnSc1	tradice
<g/>
,	,	kIx,	,
nebyl	být	k5eNaImAgMnS	být
dosud	dosud	k6eAd1	dosud
veřejnosti	veřejnost	k1gFnSc6	veřejnost
předložen	předložit	k5eAaPmNgInS	předložit
jediný	jediný	k2eAgInSc1d1	jediný
důkaz	důkaz	k1gInSc1	důkaz
dokládající	dokládající	k2eAgFnSc4d1	dokládající
skutečnost	skutečnost	k1gFnSc4	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
likérka	likérka	k1gFnSc1	likérka
Hills	Hillsa	k1gFnPc2	Hillsa
předválečný	předválečný	k2eAgInSc4d1	předválečný
absint	absint	k1gInSc4	absint
skutečně	skutečně	k6eAd1	skutečně
vyráběla	vyrábět	k5eAaImAgFnS	vyrábět
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
argument	argument	k1gInSc1	argument
patří	patřit	k5eAaImIp3nS	patřit
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
zákazem	zákaz	k1gInSc7	zákaz
absintu	absint	k1gInSc2	absint
v	v	k7c6	v
protektorátním	protektorátní	k2eAgNnSc6d1	protektorátní
Československu	Československo	k1gNnSc6	Československo
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
přední	přední	k2eAgFnPc4d1	přední
zbraně	zbraň	k1gFnPc4	zbraň
jeho	jeho	k3xOp3gMnPc2	jeho
odpůrců	odpůrce	k1gMnPc2	odpůrce
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
absint	absint	k1gInSc1	absint
(	(	kIx(	(
<g/>
ať	ať	k9	ať
už	už	k6eAd1	už
pravý	pravý	k2eAgMnSc1d1	pravý
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
bohémský	bohémský	k2eAgInSc1d1	bohémský
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
v	v	k7c6	v
předválečném	předválečný	k2eAgNnSc6d1	předválečné
Československu	Československo	k1gNnSc6	Československo
nikdy	nikdy	k6eAd1	nikdy
nevyráběl	vyrábět	k5eNaImAgInS	vyrábět
<g/>
.	.	kIx.	.
</s>
<s>
Každopádně	každopádně	k6eAd1	každopádně
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
pravda	pravda	k1gFnSc1	pravda
<g/>
,	,	kIx,	,
že	že	k8xS	že
potomek	potomek	k1gMnSc1	potomek
zakladatele	zakladatel	k1gMnSc2	zakladatel
Albína	Albín	k1gMnSc2	Albín
Hilla	Hill	k1gMnSc2	Hill
<g/>
,	,	kIx,	,
Radomil	Radomila	k1gFnPc2	Radomila
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgInS	začít
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
na	na	k7c4	na
popud	popud	k1gInSc4	popud
zákazníka	zákazník	k1gMnSc4	zákazník
Ing.	ing.	kA	ing.
Boháče	Boháč	k1gMnSc4	Boháč
ze	z	k7c2	z
Slavonic	Slavonice	k1gFnPc2	Slavonice
vyrábět	vyrábět	k5eAaImF	vyrábět
bohémský	bohémský	k2eAgInSc4d1	bohémský
Hills	Hills	k1gInSc4	Hills
absinth	absintha	k1gFnPc2	absintha
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnPc1	jehož
první	první	k4xOgFnPc1	první
lahve	lahev	k1gFnPc1	lahev
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
dodávat	dodávat	k5eAaImF	dodávat
do	do	k7c2	do
restaurace	restaurace	k1gFnSc2	restaurace
Besídka	besídka	k1gFnSc1	besídka
ve	v	k7c6	v
Slavonicích	Slavonice	k1gFnPc6	Slavonice
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
do	do	k7c2	do
všech	všecek	k3xTgInPc2	všecek
koutů	kout	k1gInPc2	kout
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
do	do	k7c2	do
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Starorežná	Starorežný	k2eAgFnSc1d1	Starorežná
Prostějov	Prostějov	k1gInSc1	Prostějov
(	(	kIx(	(
<g/>
Pálírna	Pálírna	k1gFnSc1	Pálírna
u	u	k7c2	u
Zeleného	Zeleného	k2eAgInSc2d1	Zeleného
Stromu	strom	k1gInSc2	strom
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgMnPc2d3	veliký
producentů	producent	k1gMnPc2	producent
a	a	k8xC	a
vývozců	vývozce	k1gMnPc2	vývozce
absintu	absint	k1gInSc2	absint
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
;	;	kIx,	;
tuto	tento	k3xDgFnSc4	tento
lihovinu	lihovina	k1gFnSc4	lihovina
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
již	již	k6eAd1	již
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
;	;	kIx,	;
doložit	doložit	k5eAaPmF	doložit
to	ten	k3xDgNnSc1	ten
lze	lze	k6eAd1	lze
starými	starý	k2eAgFnPc7d1	stará
etiketami	etiketa	k1gFnPc7	etiketa
<g/>
,	,	kIx,	,
datujícími	datující	k2eAgMnPc7d1	datující
se	s	k7c7	s
právě	právě	k9	právě
do	do	k7c2	do
počátků	počátek	k1gInPc2	počátek
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
usilování	usilování	k1gNnSc3	usilování
vinařských	vinařský	k2eAgInPc2d1	vinařský
spolků	spolek	k1gInPc2	spolek
i	i	k8xC	i
různých	různý	k2eAgFnPc2d1	různá
jiných	jiný	k2eAgFnPc2d1	jiná
organizací	organizace	k1gFnPc2	organizace
o	o	k7c4	o
absintovou	absintový	k2eAgFnSc4d1	absintová
prohibici	prohibice	k1gFnSc4	prohibice
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
obecném	obecný	k2eAgNnSc6d1	obecné
povědomí	povědomí	k1gNnSc6	povědomí
vytvářeno	vytvářen	k2eAgNnSc4d1	vytvářeno
přesvědčení	přesvědčení	k1gNnSc4	přesvědčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
absint	absint	k1gInSc1	absint
je	být	k5eAaImIp3nS	být
spojen	spojit	k5eAaPmNgInS	spojit
s	s	k7c7	s
násilnými	násilný	k2eAgInPc7d1	násilný
zločiny	zločin	k1gInPc7	zločin
a	a	k8xC	a
společenským	společenský	k2eAgInSc7d1	společenský
nepokojem	nepokoj	k1gInSc7	nepokoj
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
roku	rok	k1gInSc2	rok
1905	[number]	k4	1905
Jean	Jean	k1gMnSc1	Jean
Lanfray	Lanfraa	k1gFnSc2	Lanfraa
zabil	zabít	k5eAaPmAgMnS	zabít
svou	svůj	k3xOyFgFnSc4	svůj
rodinu	rodina	k1gFnSc4	rodina
<g/>
,	,	kIx,	,
a	a	k8xC	a
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
zabít	zabít	k5eAaPmF	zabít
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Domněnka	domněnka	k1gFnSc1	domněnka
<g/>
,	,	kIx,	,
že	že	k8xS	že
Lanfray	Lanfra	k1gMnPc4	Lanfra
zabíjel	zabíjet	k5eAaImAgMnS	zabíjet
<g/>
,	,	kIx,	,
poněvadž	poněvadž	k8xS	poněvadž
vypil	vypít	k5eAaPmAgMnS	vypít
dvě	dva	k4xCgFnPc4	dva
sklenice	sklenice	k1gFnPc4	sklenice
absintu	absint	k1gInSc2	absint
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
šířila	šířit	k5eAaImAgFnS	šířit
<g/>
.	.	kIx.	.
</s>
<s>
Skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
Lanfray	Lanfra	k1gMnPc4	Lanfra
měl	mít	k5eAaImAgInS	mít
absint	absint	k1gInSc1	absint
pouze	pouze	k6eAd1	pouze
ráno	ráno	k6eAd1	ráno
a	a	k8xC	a
během	během	k7c2	během
dne	den	k1gInSc2	den
vypil	vypít	k5eAaPmAgMnS	vypít
sedm	sedm	k4xCc4	sedm
sklenic	sklenice	k1gFnPc2	sklenice
vína	víno	k1gNnSc2	víno
<g/>
,	,	kIx,	,
šest	šest	k4xCc1	šest
sklenic	sklenice	k1gFnPc2	sklenice
koňaku	koňak	k1gInSc2	koňak
a	a	k8xC	a
kávu	káva	k1gFnSc4	káva
s	s	k7c7	s
brandy	brandy	k1gFnSc7	brandy
<g/>
,	,	kIx,	,
přitom	přitom	k6eAd1	přitom
byly	být	k5eAaImAgFnP	být
bagatelizovány	bagatelizován	k2eAgFnPc1d1	bagatelizována
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
této	tento	k3xDgFnSc2	tento
události	událost	k1gFnSc2	událost
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
petice	petice	k1gFnSc1	petice
na	na	k7c4	na
absintovou	absintový	k2eAgFnSc4d1	absintová
prohibici	prohibice	k1gFnSc4	prohibice
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
po	po	k7c6	po
nasbírání	nasbírání	k1gNnSc6	nasbírání
82	[number]	k4	82
tis	tis	k1gInSc4	tis
<g/>
.	.	kIx.	.
podpisů	podpis	k1gInPc2	podpis
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1907	[number]	k4	1907
zákaz	zákaz	k1gInSc1	zákaz
absintu	absint	k1gInSc2	absint
zanesen	zanést	k5eAaPmNgInS	zanést
do	do	k7c2	do
švýcarské	švýcarský	k2eAgFnSc2d1	švýcarská
ústavy	ústava	k1gFnSc2	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
vůbec	vůbec	k9	vůbec
první	první	k4xOgInPc1	první
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
absint	absint	k1gInSc1	absint
zakázán	zakázán	k2eAgInSc1d1	zakázán
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
Svobodný	svobodný	k2eAgInSc1d1	svobodný
konžský	konžský	k2eAgInSc1d1	konžský
stát	stát	k1gInSc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
Tamější	tamější	k2eAgInSc1d1	tamější
zákaz	zákaz	k1gInSc1	zákaz
začal	začít	k5eAaPmAgInS	začít
platit	platit	k5eAaImF	platit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1898	[number]	k4	1898
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1906	[number]	k4	1906
byl	být	k5eAaImAgInS	být
absint	absint	k1gInSc1	absint
zakázán	zakázat	k5eAaPmNgInS	zakázat
v	v	k7c6	v
Belgii	Belgie	k1gFnSc6	Belgie
a	a	k8xC	a
Brazílii	Brazílie	k1gFnSc6	Brazílie
<g/>
.	.	kIx.	.
</s>
<s>
Nizozemí	Nizozemí	k1gNnSc1	Nizozemí
se	se	k3xPyFc4	se
přidalo	přidat	k5eAaPmAgNnS	přidat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1909	[number]	k4	1909
<g/>
,	,	kIx,	,
USA	USA	kA	USA
roku	rok	k1gInSc2	rok
1912	[number]	k4	1912
a	a	k8xC	a
Francie	Francie	k1gFnSc1	Francie
se	se	k3xPyFc4	se
zákazu	zákaz	k1gInSc2	zákaz
dočkala	dočkat	k5eAaPmAgFnS	dočkat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1915	[number]	k4	1915
<g/>
.	.	kIx.	.
</s>
<s>
Německo	Německo	k1gNnSc1	Německo
pokusům	pokus	k1gInPc3	pokus
o	o	k7c4	o
společenské	společenský	k2eAgInPc4d1	společenský
vytěsnění	vytěsnění	k1gNnSc1	vytěsnění
absintu	absint	k1gInSc2	absint
odolávalo	odolávat	k5eAaImAgNnS	odolávat
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1923	[number]	k4	1923
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jeho	jeho	k3xOp3gMnPc1	jeho
radikální	radikální	k2eAgMnPc1d1	radikální
odpůrci	odpůrce	k1gMnPc1	odpůrce
svého	svůj	k3xOyFgMnSc4	svůj
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
i	i	k9	i
tam	tam	k6eAd1	tam
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
byly	být	k5eAaImAgFnP	být
po	po	k7c4	po
vyhlášení	vyhlášení	k1gNnSc4	vyhlášení
protektorátu	protektorát	k1gInSc2	protektorát
vládním	vládní	k2eAgNnSc7d1	vládní
nařízením	nařízení	k1gNnSc7	nařízení
č.	č.	k?	č.
458	[number]	k4	458
<g/>
/	/	kIx~	/
<g/>
1941	[number]	k4	1941
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
obchodu	obchod	k1gInSc6	obchod
s	s	k7c7	s
absinthem	absinth	k1gInSc7	absinth
<g/>
,	,	kIx,	,
absinth	absinth	k1gInSc1	absinth
a	a	k8xC	a
"	"	kIx"	"
<g/>
veškeré	veškerý	k3xTgFnPc4	veškerý
kořalky	kořalka	k1gFnPc4	kořalka
<g/>
"	"	kIx"	"
obsahující	obsahující	k2eAgFnPc4d1	obsahující
byť	byť	k8xS	byť
malé	malý	k2eAgNnSc1d1	malé
množství	množství	k1gNnSc1	množství
vermutového	vermutový	k2eAgNnSc2d1	vermutový
koření	koření	k1gNnSc2	koření
a	a	k8xC	a
základních	základní	k2eAgFnPc2d1	základní
látek	látka	k1gFnPc2	látka
sloužících	sloužící	k2eAgFnPc2d1	sloužící
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
absintu	absint	k1gInSc2	absint
zakázány	zakázat	k5eAaPmNgFnP	zakázat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zákazu	zákaz	k1gInSc6	zákaz
absintu	absint	k1gInSc2	absint
se	se	k3xPyFc4	se
pozvolna	pozvolna	k6eAd1	pozvolna
rozvinula	rozvinout	k5eAaPmAgFnS	rozvinout
výroba	výroba	k1gFnSc1	výroba
podobného	podobný	k2eAgInSc2d1	podobný
anýzového	anýzový	k2eAgInSc2d1	anýzový
nápoje	nápoj	k1gInSc2	nápoj
se	s	k7c7	s
jménem	jméno	k1gNnSc7	jméno
pastis	pastis	k1gFnSc2	pastis
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
jehož	jehož	k3xOyRp3gFnSc4	jehož
výrobu	výroba	k1gFnSc4	výroba
se	se	k3xPyFc4	se
nevyužívalo	využívat	k5eNaImAgNnS	využívat
pelyňku	pelyňka	k1gFnSc4	pelyňka
<g/>
,	,	kIx,	,
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
se	se	k3xPyFc4	se
ho	on	k3xPp3gInSc4	on
zákaz	zákaz	k1gInSc4	zákaz
netýkal	týkat	k5eNaImAgMnS	týkat
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
Pernod	Pernoda	k1gFnPc2	Pernoda
se	se	k3xPyFc4	se
po	po	k7c6	po
zákazu	zákaz	k1gInSc6	zákaz
přesunula	přesunout	k5eAaPmAgFnS	přesunout
do	do	k7c2	do
katalánského	katalánský	k2eAgNnSc2d1	katalánské
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
absint	absint	k1gInSc1	absint
zakázán	zakázat	k5eAaPmNgInS	zakázat
nebyl	být	k5eNaImAgInS	být
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kvůli	kvůli	k7c3	kvůli
upadajícímu	upadající	k2eAgInSc3d1	upadající
zájmu	zájem	k1gInSc3	zájem
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
poslední	poslední	k2eAgFnSc1d1	poslední
lahev	lahev	k1gFnSc1	lahev
pravého	pravý	k2eAgInSc2d1	pravý
absintu	absint	k1gInSc2	absint
Pernod	Pernoda	k1gFnPc2	Pernoda
naplněna	naplněn	k2eAgFnSc1d1	naplněna
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
se	se	k3xPyFc4	se
absint	absint	k1gInSc1	absint
po	po	k7c6	po
zákazu	zákaz	k1gInSc6	zákaz
přesunul	přesunout	k5eAaPmAgInS	přesunout
do	do	k7c2	do
tajných	tajný	k2eAgFnPc2d1	tajná
<g/>
,	,	kIx,	,
ilegálních	ilegální	k2eAgFnPc2d1	ilegální
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
domácích	domácí	k2eAgFnPc2d1	domácí
destilérek	destilérka	k1gFnPc2	destilérka
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
absinty	absint	k1gInPc1	absint
se	se	k3xPyFc4	se
nazývaly	nazývat	k5eAaImAgInP	nazývat
Clandestine	Clandestin	k1gInSc5	Clandestin
a	a	k8xC	a
šlo	jít	k5eAaImAgNnS	jít
většinou	většinou	k6eAd1	většinou
jen	jen	k9	jen
o	o	k7c6	o
blanche	blanche	k1gFnSc6	blanche
absinty	absint	k1gInPc4	absint
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
barvení	barvení	k1gNnSc1	barvení
patřilo	patřit	k5eAaImAgNnS	patřit
mezi	mezi	k7c4	mezi
náročnější	náročný	k2eAgFnPc4d2	náročnější
části	část	k1gFnPc4	část
technologického	technologický	k2eAgInSc2d1	technologický
postupu	postup	k1gInSc2	postup
a	a	k8xC	a
absence	absence	k1gFnSc2	absence
charakteristické	charakteristický	k2eAgFnSc2d1	charakteristická
zelené	zelený	k2eAgFnPc4d1	zelená
barvy	barva	k1gFnPc4	barva
nahrávala	nahrávat	k5eAaImAgFnS	nahrávat
snahám	snaha	k1gFnPc3	snaha
o	o	k7c4	o
utajení	utajení	k1gNnSc4	utajení
před	před	k7c7	před
úřady	úřad	k1gInPc7	úřad
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
prvnímu	první	k4xOgNnSc3	první
uvolnění	uvolnění	k1gNnSc3	uvolnění
zákazu	zákaz	k1gInSc2	zákaz
došlo	dojít	k5eAaPmAgNnS	dojít
už	už	k9	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Podmínkou	podmínka	k1gFnSc7	podmínka
bylo	být	k5eAaImAgNnS	být
omezení	omezení	k1gNnSc1	omezení
thujonu	thujon	k1gInSc2	thujon
na	na	k7c4	na
10	[number]	k4	10
mg	mg	kA	mg
<g/>
.	.	kIx.	.
<g/>
l-	l-	k?	l-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
britský	britský	k2eAgMnSc1d1	britský
dovozce	dovozce	k1gMnSc1	dovozce
BBH	BBH	kA	BBH
Spirits	Spirits	k1gInSc1	Spirits
objevil	objevit	k5eAaPmAgInS	objevit
český	český	k2eAgInSc1d1	český
absint	absint	k1gInSc1	absint
Hills	Hillsa	k1gFnPc2	Hillsa
<g/>
,	,	kIx,	,
a	a	k8xC	a
protože	protože	k8xS	protože
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
absint	absint	k1gInSc4	absint
nikdy	nikdy	k6eAd1	nikdy
nezakázala	zakázat	k5eNaPmAgFnS	zakázat
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgMnS	začít
tento	tento	k3xDgInSc4	tento
nápoj	nápoj	k1gInSc4	nápoj
importovat	importovat	k5eAaBmF	importovat
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
;	;	kIx,	;
obliba	obliba	k1gFnSc1	obliba
absintu	absint	k1gInSc2	absint
od	od	k7c2	od
toho	ten	k3xDgInSc2	ten
okamžiku	okamžik	k1gInSc2	okamžik
rychle	rychle	k6eAd1	rychle
stoupala	stoupat	k5eAaImAgFnS	stoupat
<g/>
,	,	kIx,	,
a	a	k8xC	a
renesance	renesance	k1gFnSc1	renesance
absintu	absint	k1gInSc2	absint
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
se	se	k3xPyFc4	se
výroba	výroba	k1gFnSc1	výroba
pravého	pravý	k2eAgInSc2d1	pravý
absintu	absint	k1gInSc2	absint
vrátila	vrátit	k5eAaPmAgFnS	vrátit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
Francie	Francie	k1gFnSc1	Francie
je	být	k5eAaImIp3nS	být
zemí	zem	k1gFnSc7	zem
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yQgFnSc4	který
se	se	k3xPyFc4	se
absintový	absintový	k2eAgInSc1d1	absintový
zákaz	zákaz	k1gInSc1	zákaz
podepsal	podepsat	k5eAaPmAgInS	podepsat
regulací	regulace	k1gFnSc7	regulace
<g/>
,	,	kIx,	,
že	že	k8xS	že
absint	absint	k1gInSc1	absint
se	se	k3xPyFc4	se
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
může	moct	k5eAaImIp3nS	moct
vyrábět	vyrábět	k5eAaImF	vyrábět
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
vyvážet	vyvážet	k5eAaImF	vyvážet
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
jejím	její	k3xOp3gNnSc6	její
území	území	k1gNnSc6	území
smí	smět	k5eAaImIp3nS	smět
být	být	k5eAaImF	být
prodáván	prodávat	k5eAaImNgInS	prodávat
jen	jen	k6eAd1	jen
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
spiritueux	spiritueux	k1gInSc4	spiritueux
à	à	k?	à
base	basa	k1gFnSc3	basa
de	de	k?	de
plantes	plantes	k1gInSc1	plantes
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
absinthe	absinth	k1gMnSc2	absinth
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
alkohol	alkohol	k1gInSc1	alkohol
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
pelyňku	pelyněk	k1gInSc2	pelyněk
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
uvedeného	uvedený	k2eAgNnSc2d1	uvedené
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
absint	absint	k1gInSc1	absint
se	se	k3xPyFc4	se
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
pod	pod	k7c7	pod
svým	svůj	k3xOyFgNnSc7	svůj
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
(	(	kIx(	(
<g/>
absinthe	absinthe	k1gNnSc7	absinthe
<g/>
)	)	kIx)	)
prodávat	prodávat	k5eAaImF	prodávat
nesmí	smět	k5eNaImIp3nS	smět
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
milénia	milénium	k1gNnSc2	milénium
se	se	k3xPyFc4	se
zákaz	zákaz	k1gInSc1	zákaz
absintu	absint	k1gInSc2	absint
postupně	postupně	k6eAd1	postupně
zrušil	zrušit	k5eAaPmAgMnS	zrušit
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
zaveden	zavést	k5eAaPmNgInS	zavést
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
na	na	k7c4	na
regionální	regionální	k2eAgFnPc4d1	regionální
výjimky	výjimka	k1gFnPc4	výjimka
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
Mataura	Mataura	k1gFnSc1	Mataura
na	na	k7c6	na
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
absint	absint	k1gInSc1	absint
legálně	legálně	k6eAd1	legálně
konzumovat	konzumovat	k5eAaBmF	konzumovat
v	v	k7c6	v
kterékoli	kterýkoli	k3yIgFnSc6	kterýkoli
části	část	k1gFnSc6	část
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
celosvětově	celosvětově	k6eAd1	celosvětově
existují	existovat	k5eAaImIp3nP	existovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvě	dva	k4xCgFnPc4	dva
stovky	stovka	k1gFnPc4	stovka
značek	značka	k1gFnPc2	značka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
světě	svět	k1gInSc6	svět
je	být	k5eAaImIp3nS	být
rozšířeno	rozšířit	k5eAaPmNgNnS	rozšířit
několik	několik	k4yIc1	několik
způsobů	způsob	k1gInPc2	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
absint	absint	k1gInSc1	absint
pije	pít	k5eAaImIp3nS	pít
<g/>
.	.	kIx.	.
</s>
<s>
Těmi	ten	k3xDgInPc7	ten
částmi	část	k1gFnPc7	část
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
postupů	postup	k1gInPc2	postup
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
vzbuzují	vzbuzovat	k5eAaImIp3nP	vzbuzovat
největší	veliký	k2eAgFnSc4d3	veliký
pozornost	pozornost	k1gFnSc4	pozornost
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
metody	metoda	k1gFnPc1	metoda
míchání	míchání	k1gNnSc2	míchání
absintu	absint	k1gInSc2	absint
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
změnu	změna	k1gFnSc4	změna
jeho	jeho	k3xOp3gFnSc2	jeho
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Nejen	nejen	k6eAd1	nejen
Francouzi	Francouz	k1gMnPc1	Francouz
tento	tento	k3xDgInSc4	tento
proces	proces	k1gInSc4	proces
označují	označovat	k5eAaImIp3nP	označovat
slovem	slovem	k6eAd1	slovem
loucher	louchra	k1gFnPc2	louchra
nebo	nebo	k8xC	nebo
louche	louche	k1gFnPc2	louche
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
doslova	doslova	k6eAd1	doslova
znamená	znamenat	k5eAaImIp3nS	znamenat
kalit	kalit	k5eAaImF	kalit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
kalný	kalný	k2eAgInSc1d1	kalný
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
smíchá	smíchat	k5eAaPmIp3nS	smíchat
pravý	pravý	k2eAgInSc1d1	pravý
absint	absint	k1gInSc1	absint
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
mělo	mít	k5eAaImAgNnS	mít
by	by	kYmCp3nS	by
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
pozvolnému	pozvolný	k2eAgNnSc3d1	pozvolné
zakalení	zakalení	k1gNnSc3	zakalení
a	a	k8xC	a
změně	změna	k1gFnSc3	změna
barvy	barva	k1gFnSc2	barva
-	-	kIx~	-
ze	z	k7c2	z
smaragdově	smaragdově	k6eAd1	smaragdově
zelené	zelený	k2eAgInPc1d1	zelený
na	na	k7c4	na
mléčnou	mléčný	k2eAgFnSc4d1	mléčná
<g/>
.	.	kIx.	.
</s>
<s>
Změna	změna	k1gFnSc1	změna
barvy	barva	k1gFnSc2	barva
má	mít	k5eAaImIp3nS	mít
svou	svůj	k3xOyFgFnSc4	svůj
podstatu	podstata	k1gFnSc4	podstata
v	v	k7c6	v
silicových	silicův	k2eAgFnPc6d1	silicův
složkách	složka	k1gFnPc6	složka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
se	se	k3xPyFc4	se
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
začnou	začít	k5eAaPmIp3nP	začít
srážet	srážet	k5eAaImF	srážet
a	a	k8xC	a
odlučovat	odlučovat	k5eAaImF	odlučovat
se	se	k3xPyFc4	se
od	od	k7c2	od
alkoholové	alkoholový	k2eAgFnSc2d1	alkoholová
složky	složka	k1gFnSc2	složka
-	-	kIx~	-
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
srážení	srážení	k1gNnSc3	srážení
v	v	k7c6	v
koloidní	koloidní	k2eAgFnSc6d1	koloidní
suspenzi	suspenze	k1gFnSc6	suspenze
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
některých	některý	k3yIgFnPc2	některý
metod	metoda	k1gFnPc2	metoda
ředění	ředění	k1gNnSc2	ředění
absintu	absint	k1gInSc2	absint
vodou	voda	k1gFnSc7	voda
je	být	k5eAaImIp3nS	být
nalití	nalití	k1gNnSc1	nalití
vody	voda	k1gFnSc2	voda
do	do	k7c2	do
skleničky	sklenička	k1gFnSc2	sklenička
s	s	k7c7	s
absintem	absint	k1gInSc7	absint
přes	přes	k7c4	přes
kostku	kostka	k1gFnSc4	kostka
cukru	cukr	k1gInSc2	cukr
na	na	k7c6	na
lžičce	lžička	k1gFnSc6	lžička
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
rozpuštění	rozpuštění	k1gNnSc2	rozpuštění
cukru	cukr	k1gInSc2	cukr
a	a	k8xC	a
zjemnění	zjemnění	k1gNnSc2	zjemnění
chuti	chuť	k1gFnSc2	chuť
nápoje	nápoj	k1gInSc2	nápoj
<g/>
;	;	kIx,	;
tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
konzumace	konzumace	k1gFnSc2	konzumace
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Absint	absint	k1gInSc1	absint
se	se	k3xPyFc4	se
původně	původně	k6eAd1	původně
naléval	nalévat	k5eAaImAgInS	nalévat
do	do	k7c2	do
speciálních	speciální	k2eAgFnPc2d1	speciální
sklenic	sklenice	k1gFnPc2	sklenice
<g/>
,	,	kIx,	,
naprostá	naprostý	k2eAgFnSc1d1	naprostá
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
měla	mít	k5eAaImAgFnS	mít
rozšiřující	rozšiřující	k2eAgFnSc4d1	rozšiřující
se	se	k3xPyFc4	se
kulatou	kulatý	k2eAgFnSc4d1	kulatá
nožku	nožka	k1gFnSc4	nožka
tvaru	tvar	k1gInSc2	tvar
skleněné	skleněný	k2eAgFnSc2d1	skleněná
kuličky	kulička	k1gFnSc2	kulička
<g/>
;	;	kIx,	;
objem	objem	k1gInSc1	objem
sklenice	sklenice	k1gFnSc2	sklenice
se	se	k3xPyFc4	se
pohyboval	pohybovat	k5eAaImAgInS	pohybovat
okolo	okolo	k7c2	okolo
75	[number]	k4	75
ml.	ml.	kA	ml.
Přes	přes	k7c4	přes
sklenici	sklenice	k1gFnSc4	sklenice
byla	být	k5eAaImAgFnS	být
položena	položen	k2eAgFnSc1d1	položena
speciální	speciální	k2eAgFnSc1d1	speciální
děrovaná	děrovaný	k2eAgFnSc1d1	děrovaná
lžička	lžička	k1gFnSc1	lžička
<g/>
,	,	kIx,	,
na	na	k7c4	na
niž	jenž	k3xRgFnSc4	jenž
se	se	k3xPyFc4	se
pokládala	pokládat	k5eAaImAgFnS	pokládat
kostka	kostka	k1gFnSc1	kostka
cukru	cukr	k1gInSc2	cukr
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgMnPc1d1	původní
pijáci	piják	k1gMnPc1	piják
absintu	absint	k1gInSc2	absint
si	se	k3xPyFc3	se
vždy	vždy	k6eAd1	vždy
nápoj	nápoj	k1gInSc4	nápoj
připravovali	připravovat	k5eAaImAgMnP	připravovat
sami	sám	k3xTgMnPc1	sám
<g/>
;	;	kIx,	;
příprava	příprava	k1gFnSc1	příprava
nápoje	nápoj	k1gInSc2	nápoj
číšníkem	číšník	k1gMnSc7	číšník
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
urážkou	urážka	k1gFnSc7	urážka
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
tradiční	tradiční	k2eAgInSc1d1	tradiční
způsob	způsob	k1gInSc1	způsob
konzumace	konzumace	k1gFnSc2	konzumace
se	se	k3xPyFc4	se
společně	společně	k6eAd1	společně
s	s	k7c7	s
replikami	replika	k1gFnPc7	replika
absintového	absintový	k2eAgNnSc2d1	absintový
příslušenství	příslušenství	k1gNnSc2	příslušenství
pozvolna	pozvolna	k6eAd1	pozvolna
vrací	vracet	k5eAaImIp3nS	vracet
<g/>
.	.	kIx.	.
</s>
<s>
Ředění	ředění	k1gNnSc1	ředění
absintu	absint	k1gInSc2	absint
vodou	voda	k1gFnSc7	voda
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
až	až	k9	až
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Doporučení	doporučení	k1gNnSc1	doporučení
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
konzumované	konzumovaný	k2eAgFnSc6d1	konzumovaná
značce	značka	k1gFnSc6	značka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
české	český	k2eAgFnSc6d1	Česká
půdě	půda	k1gFnSc6	půda
se	se	k3xPyFc4	se
po	po	k7c6	po
revoluci	revoluce	k1gFnSc6	revoluce
objevil	objevit	k5eAaPmAgInS	objevit
nový	nový	k2eAgInSc1d1	nový
způsob	způsob	k1gInSc1	způsob
úpravy	úprava	k1gFnSc2	úprava
absintu	absint	k1gInSc2	absint
před	před	k7c7	před
konzumací	konzumace	k1gFnSc7	konzumace
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
úspěšným	úspěšný	k2eAgInSc7d1	úspěšný
marketingovým	marketingový	k2eAgInSc7d1	marketingový
tahem	tah	k1gInSc7	tah
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
lžičku	lžička	k1gFnSc4	lžička
se	se	k3xPyFc4	se
položí	položit	k5eAaPmIp3nS	položit
kostka	kostka	k1gFnSc1	kostka
cukru	cukr	k1gInSc2	cukr
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
namočí	namočit	k5eAaPmIp3nS	namočit
do	do	k7c2	do
sklenice	sklenice	k1gFnSc2	sklenice
s	s	k7c7	s
absintem	absint	k1gInSc7	absint
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
vysokého	vysoký	k2eAgInSc2d1	vysoký
podílu	podíl	k1gInSc2	podíl
alkoholu	alkohol	k1gInSc2	alkohol
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
kostku	kostka	k1gFnSc4	kostka
s	s	k7c7	s
absintem	absint	k1gInSc7	absint
zapálit	zapálit	k5eAaPmF	zapálit
a	a	k8xC	a
vznikající	vznikající	k2eAgInSc1d1	vznikající
karamel	karamel	k1gInSc1	karamel
nechat	nechat	k5eAaPmF	nechat
odkapávat	odkapávat	k5eAaImF	odkapávat
do	do	k7c2	do
sklenice	sklenice	k1gFnSc2	sklenice
<g/>
.	.	kIx.	.
</s>
<s>
Ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
dohoření	dohoření	k1gNnSc6	dohoření
se	se	k3xPyFc4	se
lžička	lžička	k1gFnSc1	lžička
s	s	k7c7	s
karamelem	karamel	k1gInSc7	karamel
ponoří	ponořit	k5eAaPmIp3nS	ponořit
do	do	k7c2	do
absintu	absint	k1gInSc2	absint
<g/>
,	,	kIx,	,
celá	celý	k2eAgFnSc1d1	celá
směs	směs	k1gFnSc1	směs
se	se	k3xPyFc4	se
rozmíchá	rozmíchat	k5eAaPmIp3nS	rozmíchat
a	a	k8xC	a
následně	následně	k6eAd1	následně
podává	podávat	k5eAaImIp3nS	podávat
ke	k	k7c3	k
konzumaci	konzumace	k1gFnSc3	konzumace
<g/>
.	.	kIx.	.
</s>
<s>
Procedura	procedura	k1gFnSc1	procedura
je	být	k5eAaImIp3nS	být
opticky	opticky	k6eAd1	opticky
velmi	velmi	k6eAd1	velmi
efektní	efektní	k2eAgMnSc1d1	efektní
a	a	k8xC	a
získává	získávat	k5eAaImIp3nS	získávat
si	se	k3xPyFc3	se
množství	množství	k1gNnSc4	množství
turistů	turist	k1gMnPc2	turist
a	a	k8xC	a
absintových	absintový	k2eAgMnPc2d1	absintový
nováčků	nováček	k1gMnPc2	nováček
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
rituál	rituál	k1gInSc1	rituál
je	být	k5eAaImIp3nS	být
vhodný	vhodný	k2eAgInSc1d1	vhodný
spíše	spíše	k9	spíše
pro	pro	k7c4	pro
bohémský	bohémský	k2eAgInSc4d1	bohémský
absint	absint	k1gInSc4	absint
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
je	být	k5eAaImIp3nS	být
rozšířeným	rozšířený	k2eAgInSc7d1	rozšířený
zvykem	zvyk	k1gInSc7	zvyk
podávat	podávat	k5eAaImF	podávat
absint	absint	k1gInSc4	absint
silně	silně	k6eAd1	silně
vychlazený	vychlazený	k2eAgMnSc1d1	vychlazený
a	a	k8xC	a
zapíjet	zapíjet	k5eAaImF	zapíjet
ho	on	k3xPp3gNnSc4	on
sklenicí	sklenice	k1gFnSc7	sklenice
vodky	vodka	k1gFnSc2	vodka
<g/>
.	.	kIx.	.
</s>
