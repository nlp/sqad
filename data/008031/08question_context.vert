<s>
Diabetes	diabetes	k1gInSc1
mellitus	mellitus	k1gInSc1
(	(	kIx(
<g/>
zkratka	zkratka	k1gFnSc1
DM	dm	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
česky	česky	k6eAd1
úplavice	úplavice	k1gFnSc1
cukrová	cukrový	k2eAgFnSc1d1
<g/>
,	,	kIx,
krátce	krátce	k6eAd1
cukrovka	cukrovka	k1gFnSc1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
souhrnný	souhrnný	k2eAgInSc1d1
název	název	k1gInSc1
pro	pro	k7c4
skupinu	skupina	k1gFnSc4
chronických	chronický	k2eAgNnPc2d1
onemocnění	onemocnění	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yIgNnPc1
se	se	k3xPyFc4
projevují	projevovat	k5eAaImIp3nP
poruchou	porucha	k1gFnSc7
metabolismu	metabolismus	k1gInSc2
sacharidů	sacharid	k1gInPc2
<g/>
.	.	kIx.
</s>