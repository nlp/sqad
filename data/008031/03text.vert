<s>
Diabetes	diabetes	k1gInSc1	diabetes
mellitus	mellitus	k1gInSc1	mellitus
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
DM	dm	kA	dm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
úplavice	úplavice	k1gFnSc1	úplavice
cukrová	cukrový	k2eAgFnSc1d1	cukrová
<g/>
,	,	kIx,	,
krátce	krátce	k6eAd1	krátce
cukrovka	cukrovka	k1gFnSc1	cukrovka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
souhrnný	souhrnný	k2eAgInSc1d1	souhrnný
název	název	k1gInSc1	název
pro	pro	k7c4	pro
skupinu	skupina	k1gFnSc4	skupina
chronických	chronický	k2eAgNnPc2d1	chronické
onemocnění	onemocnění	k1gNnPc2	onemocnění
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
se	se	k3xPyFc4	se
projevují	projevovat	k5eAaImIp3nP	projevovat
poruchou	porucha	k1gFnSc7	porucha
metabolismu	metabolismus	k1gInSc2	metabolismus
sacharidů	sacharid	k1gInPc2	sacharid
<g/>
.	.	kIx.	.
</s>
<s>
Rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
se	se	k3xPyFc4	se
dva	dva	k4xCgInPc4	dva
základní	základní	k2eAgInPc4d1	základní
typy	typ	k1gInPc4	typ
<g/>
:	:	kIx,	:
diabetes	diabetes	k1gInSc4	diabetes
I.	I.	kA	I.
typu	typ	k1gInSc2	typ
a	a	k8xC	a
diabetes	diabetes	k1gInSc1	diabetes
II	II	kA	II
<g/>
.	.	kIx.	.
typu	typ	k1gInSc2	typ
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
vznikají	vznikat	k5eAaImIp3nP	vznikat
důsledkem	důsledek	k1gInSc7	důsledek
absolutního	absolutní	k2eAgInSc2d1	absolutní
nebo	nebo	k8xC	nebo
relativního	relativní	k2eAgInSc2d1	relativní
nedostatku	nedostatek	k1gInSc2	nedostatek
inzulinu	inzulin	k1gInSc2	inzulin
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc4	dva
dvě	dva	k4xCgFnPc4	dva
nemoci	nemoc	k1gFnPc4	nemoc
mají	mít	k5eAaImIp3nP	mít
podobné	podobný	k2eAgInPc1d1	podobný
příznaky	příznak	k1gInPc1	příznak
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
odlišné	odlišný	k2eAgFnPc1d1	odlišná
příčiny	příčina	k1gFnPc1	příčina
vzniku	vznik	k1gInSc2	vznik
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvotních	prvotní	k2eAgNnPc6d1	prvotní
stadiích	stadion	k1gNnPc6	stadion
diabetu	diabetes	k1gInSc2	diabetes
I.	I.	kA	I.
typu	typ	k1gInSc2	typ
jsou	být	k5eAaImIp3nP	být
ničeny	ničit	k5eAaImNgFnP	ničit
buňky	buňka	k1gFnPc1	buňka
slinivky	slinivka	k1gFnSc2	slinivka
břišní	břišní	k2eAgFnPc1d1	břišní
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
produkují	produkovat	k5eAaImIp3nP	produkovat
hormon	hormon	k1gInSc4	hormon
inzulin	inzulin	k1gInSc1	inzulin
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgInSc7d1	vlastní
imunitním	imunitní	k2eAgInSc7d1	imunitní
systémem	systém	k1gInSc7	systém
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c4	mezi
autoimunitní	autoimunitní	k2eAgFnPc4d1	autoimunitní
choroby	choroba	k1gFnPc4	choroba
<g/>
.	.	kIx.	.
</s>
<s>
Diabetes	diabetes	k1gInSc1	diabetes
II	II	kA	II
<g/>
.	.	kIx.	.
typu	typ	k1gInSc2	typ
je	být	k5eAaImIp3nS	být
způsoben	způsobit	k5eAaPmNgInS	způsobit
sníženou	snížený	k2eAgFnSc7d1	snížená
citlivostí	citlivost	k1gFnSc7	citlivost
tkání	tkáň	k1gFnPc2	tkáň
vlastního	vlastní	k2eAgNnSc2d1	vlastní
těla	tělo	k1gNnSc2	tělo
k	k	k7c3	k
inzulinu	inzulin	k1gInSc3	inzulin
<g/>
.	.	kIx.	.
</s>
<s>
Následky	následek	k1gInPc1	následek
cukrovky	cukrovka	k1gFnSc2	cukrovka
se	se	k3xPyFc4	se
řeší	řešit	k5eAaImIp3nP	řešit
inzulinem	inzulin	k1gInSc7	inzulin
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
hormon	hormon	k1gInSc1	hormon
bílkovinné	bílkovinný	k2eAgFnSc2d1	bílkovinná
povahy	povaha	k1gFnSc2	povaha
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
glukóze	glukóza	k1gFnSc3	glukóza
obsažené	obsažený	k2eAgFnSc3d1	obsažená
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
vstup	vstup	k1gInSc4	vstup
do	do	k7c2	do
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
buňkách	buňka	k1gFnPc6	buňka
je	být	k5eAaImIp3nS	být
glukóza	glukóza	k1gFnSc1	glukóza
štěpena	štěpit	k5eAaImNgFnS	štěpit
na	na	k7c4	na
jednodušší	jednoduchý	k2eAgFnPc4d2	jednodušší
látky	látka	k1gFnPc4	látka
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
projevem	projev	k1gInSc7	projev
diabetu	diabetes	k1gInSc2	diabetes
zvýšená	zvýšený	k2eAgFnSc1d1	zvýšená
koncentrace	koncentrace	k1gFnSc1	koncentrace
glukózy	glukóza	k1gFnSc2	glukóza
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
a	a	k8xC	a
nedostatek	nedostatek	k1gInSc4	nedostatek
energie	energie	k1gFnSc2	energie
(	(	kIx(	(
<g/>
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
však	však	k9	však
nevyplývá	vyplývat	k5eNaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
diabetik	diabetik	k1gMnSc1	diabetik
měl	mít	k5eAaImAgMnS	mít
neustále	neustále	k6eAd1	neustále
zvýšenou	zvýšený	k2eAgFnSc4d1	zvýšená
glykemii	glykemie	k1gFnSc4	glykemie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Diabetes	diabetes	k1gInSc4	diabetes
je	být	k5eAaImIp3nS	být
řecké	řecký	k2eAgNnSc1d1	řecké
slovo	slovo	k1gNnSc1	slovo
znamenající	znamenající	k2eAgNnSc1d1	znamenající
uplynout	uplynout	k5eAaPmF	uplynout
<g/>
,	,	kIx,	,
odtékat	odtékat	k5eAaImF	odtékat
(	(	kIx(	(
<g/>
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
český	český	k2eAgInSc1d1	český
výraz	výraz	k1gInSc1	výraz
úplavice	úplavice	k1gFnSc2	úplavice
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
příčinu	příčina	k1gFnSc4	příčina
smrti	smrt	k1gFnSc2	smrt
neléčeného	léčený	k2eNgMnSc2d1	neléčený
diabetika	diabetik	k1gMnSc2	diabetik
-	-	kIx~	-
těžkou	těžký	k2eAgFnSc4d1	těžká
dehydrataci	dehydratace	k1gFnSc4	dehydratace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Latinské	latinský	k2eAgNnSc1d1	latinské
slovo	slovo	k1gNnSc1	slovo
mellitus	mellitus	k1gInSc1	mellitus
znamená	znamenat	k5eAaImIp3nS	znamenat
sladký	sladký	k2eAgInSc4d1	sladký
(	(	kIx(	(
<g/>
med	med	k1gInSc4	med
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
pojmenování	pojmenování	k1gNnSc1	pojmenování
očividně	očividně	k6eAd1	očividně
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
faktu	fakt	k1gInSc2	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
moč	moč	k1gFnSc1	moč
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
neléčení	léčený	k2eNgMnPc1d1	neléčený
diabetici	diabetik	k1gMnPc1	diabetik
vylučují	vylučovat	k5eAaImIp3nP	vylučovat
v	v	k7c6	v
nadměrném	nadměrný	k2eAgNnSc6d1	nadměrné
množství	množství	k1gNnSc6	množství
<g/>
,	,	kIx,	,
přitahovala	přitahovat	k5eAaImAgFnS	přitahovat
mouchy	moucha	k1gFnPc4	moucha
a	a	k8xC	a
včely	včela	k1gFnPc4	včela
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
taková	takový	k3xDgFnSc1	takový
moč	moč	k1gFnSc1	moč
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
glukózu	glukóza	k1gFnSc4	glukóza
<g/>
.	.	kIx.	.
</s>
<s>
Staří	starý	k2eAgMnPc1d1	starý
Číňané	Číňan	k1gMnPc1	Číňan
zjišťovali	zjišťovat	k5eAaImAgMnP	zjišťovat
projevy	projev	k1gInPc4	projev
cukrovky	cukrovka	k1gFnSc2	cukrovka
sledováním	sledování	k1gNnSc7	sledování
mravenců	mravenec	k1gMnPc2	mravenec
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
byli	být	k5eAaImAgMnP	být
přitahování	přitahování	k1gNnSc4	přitahování
k	k	k7c3	k
moči	moč	k1gFnSc3	moč
pacienta	pacient	k1gMnSc2	pacient
nebo	nebo	k8xC	nebo
ne	ne	k9	ne
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
evropští	evropský	k2eAgMnPc1d1	evropský
lékaři	lékař	k1gMnPc1	lékař
ochutnávali	ochutnávat	k5eAaImAgMnP	ochutnávat
moč	moč	k1gFnSc4	moč
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
občas	občas	k6eAd1	občas
vyobrazeno	vyobrazit	k5eAaPmNgNnS	vyobrazit
na	na	k7c6	na
gotických	gotický	k2eAgInPc6d1	gotický
reliéfech	reliéf	k1gInPc6	reliéf
<g/>
.	.	kIx.	.
</s>
<s>
Česky	česky	k6eAd1	česky
se	se	k3xPyFc4	se
cukrovka	cukrovka	k1gFnSc1	cukrovka
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
úplavice	úplavice	k1gFnSc1	úplavice
cukrová	cukrový	k2eAgFnSc1d1	cukrová
<g/>
,	,	kIx,	,
archaicky	archaicky	k6eAd1	archaicky
také	také	k9	také
průjem	průjem	k1gInSc1	průjem
cukrový	cukrový	k2eAgInSc1d1	cukrový
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
zmínky	zmínka	k1gFnPc1	zmínka
o	o	k7c6	o
diabetu	diabetes	k1gInSc6	diabetes
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
Egypta	Egypt	k1gInSc2	Egypt
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
se	se	k3xPyFc4	se
zachoval	zachovat	k5eAaPmAgInS	zachovat
Ebersův	Ebersův	k2eAgInSc1d1	Ebersův
papyrus	papyrus	k1gInSc1	papyrus
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1552	[number]	k4	1552
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Autor	autor	k1gMnSc1	autor
zde	zde	k6eAd1	zde
diabetes	diabetes	k1gInSc4	diabetes
popisuje	popisovat	k5eAaImIp3nS	popisovat
jako	jako	k9	jako
vzácnou	vzácný	k2eAgFnSc4d1	vzácná
nemoc	nemoc	k1gFnSc4	nemoc
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yQgFnSc6	který
nemocný	mocný	k2eNgMnSc1d1	nemocný
neustále	neustále	k6eAd1	neustále
pije	pít	k5eAaImIp3nS	pít
<g/>
,	,	kIx,	,
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
se	se	k3xPyFc4	se
a	a	k8xC	a
močí	moč	k1gFnPc2	moč
odchází	odcházet	k5eAaImIp3nS	odcházet
ven	ven	k6eAd1	ven
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
100	[number]	k4	100
n.	n.	k?	n.
l.	l.	k?	l.
Hippokratův	Hippokratův	k2eAgMnSc1d1	Hippokratův
žák	žák	k1gMnSc1	žák
Aretaus	Aretaus	k1gMnSc1	Aretaus
jako	jako	k8xC	jako
první	první	k4xOgFnSc1	první
užívá	užívat	k5eAaImIp3nS	užívat
termín	termín	k1gInSc1	termín
diabetes	diabetes	k1gInSc1	diabetes
(	(	kIx(	(
<g/>
z	z	k7c2	z
řeckého	řecký	k2eAgInSc2d1	řecký
δ	δ	k?	δ
-	-	kIx~	-
diabainó	diabainó	k?	diabainó
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
procházím	procházet	k5eAaImIp1nS	procházet
něčím	něco	k3yInSc7	něco
<g/>
"	"	kIx"	"
.	.	kIx.	.
</s>
<s>
Římský	římský	k2eAgMnSc1d1	římský
lékař	lékař	k1gMnSc1	lékař
Claudius	Claudius	k1gMnSc1	Claudius
Galén	Galén	k1gInSc1	Galén
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
129	[number]	k4	129
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
spisu	spis	k1gInSc6	spis
Corpus	corpus	k1gInPc2	corpus
Galenic	Galenice	k1gFnPc2	Galenice
popisuje	popisovat	k5eAaImIp3nS	popisovat
novou	nový	k2eAgFnSc4d1	nová
léčbu	léčba	k1gFnSc4	léčba
diabetu	diabetes	k1gInSc2	diabetes
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
podstatou	podstata	k1gFnSc7	podstata
je	být	k5eAaImIp3nS	být
dostatek	dostatek	k1gInSc1	dostatek
tělesného	tělesný	k2eAgInSc2d1	tělesný
pohybu	pohyb	k1gInSc2	pohyb
<g/>
,	,	kIx,	,
málo	málo	k6eAd1	málo
jídla	jídlo	k1gNnSc2	jídlo
a	a	k8xC	a
hydroterapie	hydroterapie	k1gFnSc2	hydroterapie
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
byl	být	k5eAaImAgInS	být
diabetes	diabetes	k1gInSc1	diabetes
popsán	popsat	k5eAaPmNgInS	popsat
pouze	pouze	k6eAd1	pouze
jako	jako	k8xS	jako
nemoc	nemoc	k1gFnSc4	nemoc
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
toto	tento	k3xDgNnSc4	tento
období	období	k1gNnSc4	období
účinnou	účinný	k2eAgFnSc4d1	účinná
léčbu	léčba	k1gFnSc4	léčba
diabetu	diabetes	k1gInSc2	diabetes
nepřineslo	přinést	k5eNaPmAgNnS	přinést
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
knihami	kniha	k1gFnPc7	kniha
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
stojí	stát	k5eAaImIp3nS	stát
za	za	k7c2	za
zmínění	zmínění	k1gNnSc2	zmínění
Kánon	kánon	k1gInSc4	kánon
lékařství	lékařství	k1gNnSc2	lékařství
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
perský	perský	k2eAgMnSc1d1	perský
učenec	učenec	k1gMnSc1	učenec
Ibn	Ibn	k1gFnSc2	Ibn
Síná	Síná	k1gFnSc1	Síná
zvaný	zvaný	k2eAgInSc1d1	zvaný
Avicenna	Avicenen	k2eAgMnSc4d1	Avicenen
shrnuje	shrnovat	k5eAaImIp3nS	shrnovat
dosavadní	dosavadní	k2eAgMnSc1d1	dosavadní
lékařské	lékařský	k2eAgInPc4d1	lékařský
poznatky	poznatek	k1gInPc4	poznatek
a	a	k8xC	a
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
popisuje	popisovat	k5eAaImIp3nS	popisovat
diabetickou	diabetický	k2eAgFnSc4d1	diabetická
sněť	sněť	k1gFnSc4	sněť
(	(	kIx(	(
<g/>
gangrénu	gangréna	k1gFnSc4	gangréna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
středověku	středověk	k1gInSc3	středověk
nastává	nastávat	k5eAaImIp3nS	nastávat
v	v	k7c6	v
novověku	novověk	k1gInSc6	novověk
obrovský	obrovský	k2eAgInSc4d1	obrovský
rozvoj	rozvoj	k1gInSc4	rozvoj
všech	všecek	k3xTgFnPc2	všecek
věd	věda	k1gFnPc2	věda
včetně	včetně	k7c2	včetně
medicíny	medicína	k1gFnSc2	medicína
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1674	[number]	k4	1674
si	se	k3xPyFc3	se
Angličan	Angličan	k1gMnSc1	Angličan
T.	T.	kA	T.
Willis	Willis	k1gInSc1	Willis
všiml	všimnout	k5eAaPmAgMnS	všimnout
sladké	sladký	k2eAgFnSc2d1	sladká
chuti	chuť	k1gFnSc2	chuť
diabetické	diabetický	k2eAgFnSc2d1	diabetická
moči	moč	k1gFnSc2	moč
<g/>
.	.	kIx.	.
</s>
<s>
Jedno	jeden	k4xCgNnSc1	jeden
století	století	k1gNnSc1	století
po	po	k7c6	po
Willisově	Willisův	k2eAgInSc6d1	Willisův
objevu	objev	k1gInSc6	objev
byla	být	k5eAaImAgFnS	být
vyvinuta	vyvinut	k2eAgFnSc1d1	vyvinuta
W.	W.	kA	W.
Dobsonem	Dobson	k1gInSc7	Dobson
chemická	chemický	k2eAgFnSc1d1	chemická
metoda	metoda	k1gFnSc1	metoda
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
lze	lze	k6eAd1	lze
cukr	cukr	k1gInSc4	cukr
v	v	k7c6	v
moči	moč	k1gFnSc6	moč
detekovat	detekovat	k5eAaImF	detekovat
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1787	[number]	k4	1787
přidává	přidávat	k5eAaImIp3nS	přidávat
W.	W.	kA	W.
Cullen	Cullen	k2eAgInSc4d1	Cullen
přívlastek	přívlastek	k1gInSc4	přívlastek
"	"	kIx"	"
<g/>
mellitus	mellitus	k1gInSc4	mellitus
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1869	[number]	k4	1869
Paul	Paul	k1gMnSc1	Paul
Langerhans	Langerhans	k1gInSc4	Langerhans
popsal	popsat	k5eAaPmAgMnS	popsat
nově	nově	k6eAd1	nově
objevené	objevený	k2eAgInPc4d1	objevený
ostrůvky	ostrůvek	k1gInPc4	ostrůvek
pankreatu	pankreas	k1gInSc2	pankreas
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
dizertační	dizertační	k2eAgFnSc6d1	dizertační
práci	práce	k1gFnSc6	práce
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
ostrůvky	ostrůvek	k1gInPc1	ostrůvek
jsou	být	k5eAaImIp3nP	být
po	po	k7c6	po
něm	on	k3xPp3gNnSc6	on
dodnes	dodnes	k6eAd1	dodnes
nazvané	nazvaný	k2eAgInPc1d1	nazvaný
jako	jako	k8xC	jako
Langerhansovy	Langerhansův	k2eAgInPc1d1	Langerhansův
ostrůvky	ostrůvek	k1gInPc1	ostrůvek
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1889	[number]	k4	1889
lékaři	lékař	k1gMnPc1	lékař
O.	O.	kA	O.
Minkowski	Minkowski	k1gNnSc2	Minkowski
a	a	k8xC	a
J.	J.	kA	J.
von	von	k1gInSc1	von
Mering	Mering	k1gInSc1	Mering
zjistili	zjistit	k5eAaPmAgMnP	zjistit
vztah	vztah	k1gInSc4	vztah
mezi	mezi	k7c7	mezi
pankreatem	pankreas	k1gInSc7	pankreas
a	a	k8xC	a
diabetem	diabetes	k1gInSc7	diabetes
<g/>
.	.	kIx.	.
</s>
<s>
Neúspěšně	úspěšně	k6eNd1	úspěšně
se	se	k3xPyFc4	se
pokoušeli	pokoušet	k5eAaImAgMnP	pokoušet
diabetes	diabetes	k1gInSc4	diabetes
léčit	léčit	k5eAaImF	léčit
orálně	orálně	k6eAd1	orálně
podávaným	podávaný	k2eAgInSc7d1	podávaný
pankreatinem	pankreatin	k1gInSc7	pankreatin
<g/>
,	,	kIx,	,
jejž	jenž	k3xRgInSc4	jenž
připravovali	připravovat	k5eAaImAgMnP	připravovat
sušením	sušení	k1gNnSc7	sušení
zvířecích	zvířecí	k2eAgMnPc2d1	zvířecí
pankreatů	pankreas	k1gInPc2	pankreas
<g/>
.	.	kIx.	.
</s>
<s>
Lékař	lékař	k1gMnSc1	lékař
M.	M.	kA	M.
A.	A.	kA	A.
Lane	Lane	k1gFnSc1	Lane
roku	rok	k1gInSc2	rok
1907	[number]	k4	1907
podrobněji	podrobně	k6eAd2	podrobně
zkoumal	zkoumat	k5eAaImAgInS	zkoumat
Langerhansovy	Langerhansův	k2eAgInPc4d1	Langerhansův
ostrůvky	ostrůvek	k1gInPc4	ostrůvek
a	a	k8xC	a
rozlišil	rozlišit	k5eAaPmAgMnS	rozlišit
v	v	k7c6	v
nich	on	k3xPp3gMnPc6	on
A	a	k9	a
buňky	buňka	k1gFnPc1	buňka
a	a	k8xC	a
B	B	kA	B
buňky	buňka	k1gFnPc4	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
zjištění	zjištění	k1gNnSc2	zjištění
J.	J.	kA	J.
de	de	k?	de
Meyer	Meyer	k1gMnSc1	Meyer
odvodil	odvodit	k5eAaPmAgMnS	odvodit
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
některé	některý	k3yIgFnPc1	některý
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
buněk	buňka	k1gFnPc2	buňka
mohly	moct	k5eAaImAgInP	moct
tvořit	tvořit	k5eAaImF	tvořit
hypotetický	hypotetický	k2eAgInSc4d1	hypotetický
hormon	hormon	k1gInSc4	hormon
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
snižuje	snižovat	k5eAaImIp3nS	snižovat
glykemii	glykemie	k1gFnSc4	glykemie
<g/>
,	,	kIx,	,
a	a	k8xC	a
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
ho	on	k3xPp3gInSc4	on
inzulin	inzulin	k1gInSc4	inzulin
(	(	kIx(	(
<g/>
z	z	k7c2	z
lat.	lat.	k?	lat.
insula	insula	k1gFnSc1	insula
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
znamená	znamenat	k5eAaImIp3nS	znamenat
ostrov	ostrov	k1gInSc1	ostrov
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Revoluci	revoluce	k1gFnSc4	revoluce
v	v	k7c6	v
léčbě	léčba	k1gFnSc6	léčba
diabetu	diabetes	k1gInSc2	diabetes
znamenal	znamenat	k5eAaImAgInS	znamenat
objev	objev	k1gInSc1	objev
inzulinu	inzulin	k1gInSc2	inzulin
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
uskutečněn	uskutečnit	k5eAaPmNgInS	uskutečnit
na	na	k7c6	na
lékařské	lékařský	k2eAgFnSc6d1	lékařská
fakultě	fakulta	k1gFnSc6	fakulta
University	universita	k1gFnSc2	universita
of	of	k?	of
Toronto	Toronto	k1gNnSc1	Toronto
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1921	[number]	k4	1921
kanadský	kanadský	k2eAgMnSc1d1	kanadský
chirurg	chirurg	k1gMnSc1	chirurg
Frederick	Frederick	k1gMnSc1	Frederick
Banting	Banting	k1gInSc1	Banting
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
spolupracovník	spolupracovník	k1gMnSc1	spolupracovník
a	a	k8xC	a
student	student	k1gMnSc1	student
medicíny	medicína	k1gFnSc2	medicína
Charles	Charles	k1gMnSc1	Charles
Herbert	Herbert	k1gMnSc1	Herbert
Best	Best	k1gMnSc1	Best
<g/>
,	,	kIx,	,
za	za	k7c2	za
finanční	finanční	k2eAgFnSc2d1	finanční
podpory	podpora	k1gFnSc2	podpora
profesora	profesor	k1gMnSc4	profesor
John	John	k1gMnSc1	John
James	James	k1gMnSc1	James
Richard	Richard	k1gMnSc1	Richard
Macleoda	Macleoda	k1gMnSc1	Macleoda
<g/>
,	,	kIx,	,
extrahovali	extrahovat	k5eAaBmAgMnP	extrahovat
inzulin	inzulin	k1gInSc4	inzulin
z	z	k7c2	z
psího	psí	k2eAgInSc2d1	psí
pankreatu	pankreas	k1gInSc2	pankreas
a	a	k8xC	a
injekčně	injekčně	k6eAd1	injekčně
jej	on	k3xPp3gMnSc4	on
vpravili	vpravit	k5eAaPmAgMnP	vpravit
do	do	k7c2	do
jiného	jiné	k1gNnSc2	jiné
pokusného	pokusný	k2eAgMnSc2d1	pokusný
psa	pes	k1gMnSc2	pes
<g/>
,	,	kIx,	,
u	u	k7c2	u
něhož	jenž	k3xRgNnSc2	jenž
se	se	k3xPyFc4	se
příznaky	příznak	k1gInPc7	příznak
diabetu	diabetes	k1gInSc2	diabetes
zmírnily	zmírnit	k5eAaPmAgFnP	zmírnit
<g/>
.	.	kIx.	.
</s>
<s>
Posléze	posléze	k6eAd1	posléze
pokus	pokus	k1gInSc4	pokus
zopakovali	zopakovat	k5eAaPmAgMnP	zopakovat
na	na	k7c6	na
diabetickém	diabetický	k2eAgNnSc6d1	diabetické
dítěti	dítě	k1gNnSc6	dítě
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stalo	stát	k5eAaPmAgNnS	stát
prvním	první	k4xOgMnSc6	první
úspěšně	úspěšně	k6eAd1	úspěšně
léčeným	léčený	k2eAgMnSc7d1	léčený
diabetikem	diabetik	k1gMnSc7	diabetik
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Československa	Československo	k1gNnSc2	Československo
došel	dojít	k5eAaPmAgInS	dojít
objev	objev	k1gInSc1	objev
inzulinu	inzulin	k1gInSc2	inzulin
až	až	k8xS	až
roku	rok	k1gInSc2	rok
1923	[number]	k4	1923
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
byla	být	k5eAaImAgFnS	být
započata	započat	k2eAgFnSc1d1	započata
éra	éra	k1gFnSc1	éra
české	český	k2eAgFnSc2d1	Česká
diabetologie	diabetologie	k1gFnSc2	diabetologie
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
byl	být	k5eAaImAgInS	být
Banting	Banting	k1gInSc1	Banting
a	a	k8xC	a
Macleod	Macleod	k1gInSc1	Macleod
oceněn	ocenit	k5eAaPmNgInS	ocenit
Nobelovou	Nobelův	k2eAgFnSc7d1	Nobelova
cenou	cena	k1gFnSc7	cena
za	za	k7c4	za
lékařství	lékařství	k1gNnSc4	lékařství
<g/>
.	.	kIx.	.
</s>
<s>
Diabetes	diabetes	k1gInSc4	diabetes
dělíme	dělit	k5eAaImIp1nP	dělit
na	na	k7c4	na
2	[number]	k4	2
základní	základní	k2eAgInPc4d1	základní
typy	typ	k1gInPc4	typ
<g/>
,	,	kIx,	,
u	u	k7c2	u
nichž	jenž	k3xRgFnPc2	jenž
rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
dvě	dva	k4xCgFnPc1	dva
situace	situace	k1gFnPc1	situace
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mohou	moct	k5eAaImIp3nP	moct
nastat	nastat	k5eAaPmF	nastat
<g/>
:	:	kIx,	:
absolutní	absolutní	k2eAgInSc4d1	absolutní
nedostatek	nedostatek	k1gInSc4	nedostatek
inzulinu	inzulin	k1gInSc2	inzulin
→	→	k?	→
diabetes	diabetes	k1gInSc1	diabetes
mellitus	mellitus	k1gInSc1	mellitus
I.	I.	kA	I.
typu	typ	k1gInSc2	typ
relativní	relativní	k2eAgInSc1d1	relativní
nedostatek	nedostatek	k1gInSc1	nedostatek
inzulinu	inzulin	k1gInSc2	inzulin
→	→	k?	→
diabetes	diabetes	k1gInSc1	diabetes
mellitus	mellitus	k1gMnSc1	mellitus
II	II	kA	II
<g/>
.	.	kIx.	.
typu	typ	k1gInSc2	typ
Kromě	kromě	k7c2	kromě
základních	základní	k2eAgInPc2d1	základní
dvou	dva	k4xCgInPc2	dva
typů	typ	k1gInPc2	typ
diabetu	diabetes	k1gInSc2	diabetes
existuje	existovat	k5eAaImIp3nS	existovat
gestační	gestační	k2eAgInSc1d1	gestační
diabetes	diabetes	k1gInSc1	diabetes
a	a	k8xC	a
ostatní	ostatní	k2eAgInPc1d1	ostatní
specifické	specifický	k2eAgInPc1d1	specifický
typy	typ	k1gInPc1	typ
diabetu	diabetes	k1gInSc2	diabetes
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
sekundární	sekundární	k2eAgInSc1d1	sekundární
diabetes	diabetes	k1gInSc1	diabetes
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
způsobeny	způsobit	k5eAaPmNgInP	způsobit
genetickým	genetický	k2eAgInSc7d1	genetický
defektem	defekt	k1gInSc7	defekt
B	B	kA	B
buněk	buňka	k1gFnPc2	buňka
a	a	k8xC	a
inzulinu	inzulin	k1gInSc2	inzulin
<g/>
,	,	kIx,	,
změnou	změna	k1gFnSc7	změna
glukotolerance	glukotolerance	k1gFnSc2	glukotolerance
<g/>
,	,	kIx,	,
onemocněním	onemocnění	k1gNnSc7	onemocnění
pankreatu	pankreas	k1gInSc2	pankreas
atd.	atd.	kA	atd.
Označovaný	označovaný	k2eAgInSc1d1	označovaný
také	také	k9	také
jako	jako	k8xC	jako
inzulin-dependentní	inzulinependentní	k2eAgInSc1d1	inzulin-dependentní
diabetes	diabetes	k1gInSc1	diabetes
mellitus	mellitus	k1gInSc1	mellitus
(	(	kIx(	(
<g/>
IDDM	IDDM	kA	IDDM
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Diabetes	diabetes	k1gInSc1	diabetes
mellitus	mellitus	k1gInSc1	mellitus
1	[number]	k4	1
<g/>
.	.	kIx.	.
typu	typ	k1gInSc2	typ
vzniká	vznikat	k5eAaImIp3nS	vznikat
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
selektivní	selektivní	k2eAgFnSc1d1	selektivní
destrukce	destrukce	k1gFnSc1	destrukce
B	B	kA	B
buněk	buňka	k1gFnPc2	buňka
vlastním	vlastnit	k5eAaImIp1nS	vlastnit
imunitním	imunitní	k2eAgInSc7d1	imunitní
systémem	systém	k1gInSc7	systém
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
absolutnímu	absolutní	k2eAgInSc3d1	absolutní
nedostatku	nedostatek	k1gInSc3	nedostatek
inzulinu	inzulin	k1gInSc2	inzulin
a	a	k8xC	a
doživotní	doživotní	k2eAgFnSc2d1	doživotní
závislosti	závislost	k1gFnSc2	závislost
na	na	k7c6	na
exogenní	exogenní	k2eAgFnSc6d1	exogenní
aplikaci	aplikace	k1gFnSc6	aplikace
inzulinu	inzulin	k1gInSc2	inzulin
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
zničení	zničení	k1gNnSc3	zničení
B	B	kA	B
buněk	buňka	k1gFnPc2	buňka
dochází	docházet	k5eAaImIp3nS	docházet
autoimunitním	autoimunitní	k2eAgInSc7d1	autoimunitní
procesem	proces	k1gInSc7	proces
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
je	být	k5eAaImIp3nS	být
zakódován	zakódovat	k5eAaPmNgInS	zakódovat
v	v	k7c4	v
genetické	genetický	k2eAgFnPc4d1	genetická
informaci	informace	k1gFnSc4	informace
diabetika	diabetik	k1gMnSc2	diabetik
<g/>
.	.	kIx.	.
</s>
<s>
Autoimunitní	autoimunitní	k2eAgInSc1d1	autoimunitní
proces	proces	k1gInSc1	proces
je	být	k5eAaImIp3nS	být
porucha	porucha	k1gFnSc1	porucha
imunitního	imunitní	k2eAgInSc2d1	imunitní
systému	systém	k1gInSc2	systém
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
toleranci	tolerance	k1gFnSc3	tolerance
vlastních	vlastní	k2eAgFnPc2d1	vlastní
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
nimž	jenž	k3xRgInPc3	jenž
vlastní	vlastnit	k5eAaImIp3nP	vlastnit
tělo	tělo	k1gNnSc4	tělo
vytváří	vytvářit	k5eAaPmIp3nP	vytvářit
autoprotilátky	autoprotilátko	k1gNnPc7	autoprotilátko
<g/>
.	.	kIx.	.
</s>
<s>
Imunita	imunita	k1gFnSc1	imunita
zjednodušeně	zjednodušeně	k6eAd1	zjednodušeně
funguje	fungovat	k5eAaImIp3nS	fungovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
cizorodá	cizorodý	k2eAgFnSc1d1	cizorodá
látka	látka	k1gFnSc1	látka
(	(	kIx(	(
<g/>
=	=	kIx~	=
antigen	antigen	k1gInSc4	antigen
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rozpoznána	rozpoznat	k5eAaPmNgFnS	rozpoznat
B-lymfocyty	Bymfocyt	k1gInPc7	B-lymfocyt
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
produkují	produkovat	k5eAaImIp3nP	produkovat
protilátky	protilátka	k1gFnPc4	protilátka
<g/>
.	.	kIx.	.
</s>
<s>
Antigen	antigen	k1gInSc1	antigen
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
označen	označen	k2eAgMnSc1d1	označen
<g/>
"	"	kIx"	"
touto	tento	k3xDgFnSc7	tento
protilátkou	protilátka	k1gFnSc7	protilátka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
naváže	navázat	k5eAaPmIp3nS	navázat
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
označená	označený	k2eAgFnSc1d1	označená
cizorodá	cizorodý	k2eAgFnSc1d1	cizorodá
látka	látka	k1gFnSc1	látka
je	být	k5eAaImIp3nS	být
zničena	zničit	k5eAaPmNgFnS	zničit
T-lymfocyty	Tymfocyt	k1gInPc7	T-lymfocyt
a	a	k8xC	a
makrofágy	makrofág	k1gInPc7	makrofág
<g/>
.	.	kIx.	.
</s>
<s>
Konkrétně	konkrétně	k6eAd1	konkrétně
u	u	k7c2	u
diabetu	diabetes	k1gInSc2	diabetes
<g/>
:	:	kIx,	:
B-lymfocyty	Bymfocyt	k1gInPc1	B-lymfocyt
označí	označit	k5eAaPmIp3nS	označit
svými	svůj	k3xOyFgFnPc7	svůj
protilátkami	protilátka	k1gFnPc7	protilátka
B	B	kA	B
buňky	buňka	k1gFnSc2	buňka
slinivky	slinivka	k1gFnSc2	slinivka
břišní	břišní	k2eAgFnSc2d1	břišní
jako	jako	k8xS	jako
cizorodou	cizorodý	k2eAgFnSc4d1	cizorodá
část	část	k1gFnSc4	část
těla	tělo	k1gNnSc2	tělo
(	(	kIx(	(
<g/>
=	=	kIx~	=
autoantigen	autoantigen	k1gInSc1	autoantigen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
je	být	k5eAaImIp3nS	být
nastartována	nastartován	k2eAgFnSc1d1	nastartována
imunitní	imunitní	k2eAgFnSc1d1	imunitní
reakce	reakce	k1gFnSc1	reakce
<g/>
.	.	kIx.	.
</s>
<s>
T-lymfocyty	Tymfocyt	k1gInPc1	T-lymfocyt
a	a	k8xC	a
makrofágy	makrofág	k1gInPc1	makrofág
takto	takto	k6eAd1	takto
označené	označený	k2eAgFnSc2d1	označená
buňky	buňka	k1gFnSc2	buňka
bezhlavě	bezhlavě	k6eAd1	bezhlavě
ničí	ničit	k5eAaImIp3nS	ničit
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
při	při	k7c6	při
tom	ten	k3xDgNnSc6	ten
dotkly	dotknout	k5eAaPmAgFnP	dotknout
A	A	kA	A
<g/>
,	,	kIx,	,
D	D	kA	D
nebo	nebo	k8xC	nebo
PP-buněk	PPuněk	k1gInSc1	PP-buněk
<g/>
.	.	kIx.	.
</s>
<s>
Zůstává	zůstávat	k5eAaImIp3nS	zůstávat
však	však	k9	však
dosud	dosud	k6eAd1	dosud
nezodpovězená	zodpovězený	k2eNgFnSc1d1	nezodpovězená
otázka	otázka	k1gFnSc1	otázka
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
poruše	porucha	k1gFnSc3	porucha
tolerance	tolerance	k1gFnSc2	tolerance
imunitního	imunitní	k2eAgInSc2d1	imunitní
systému	systém	k1gInSc2	systém
vůči	vůči	k7c3	vůči
buňkám	buňka	k1gFnPc3	buňka
vlastního	vlastní	k2eAgNnSc2d1	vlastní
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Tvorba	tvorba	k1gFnSc1	tvorba
protilátek	protilátka	k1gFnPc2	protilátka
vůči	vůči	k7c3	vůči
B	B	kA	B
buňkám	buňka	k1gFnPc3	buňka
je	být	k5eAaImIp3nS	být
zakódovaná	zakódovaný	k2eAgFnSc1d1	zakódovaná
v	v	k7c6	v
genetické	genetický	k2eAgFnSc6d1	genetická
informaci	informace	k1gFnSc6	informace
diabetika	diabetik	k1gMnSc2	diabetik
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
vlastní	vlastní	k2eAgFnSc7d1	vlastní
manifestací	manifestace	k1gFnSc7	manifestace
diabetu	diabetes	k1gInSc2	diabetes
<g/>
.	.	kIx.	.
</s>
<s>
Spouštěcím	spouštěcí	k2eAgInSc7d1	spouštěcí
mechanismem	mechanismus	k1gInSc7	mechanismus
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
autoimunitní	autoimunitní	k2eAgFnSc2d1	autoimunitní
reakce	reakce	k1gFnSc2	reakce
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
imunitní	imunitní	k2eAgFnSc4d1	imunitní
odpověď	odpověď	k1gFnSc4	odpověď
na	na	k7c4	na
mírnou	mírný	k2eAgFnSc4d1	mírná
virózu	viróza	k1gFnSc4	viróza
(	(	kIx(	(
<g/>
nachlazení	nachlazení	k1gNnSc4	nachlazení
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
jakéhokoliv	jakýkoliv	k3yIgNnSc2	jakýkoliv
jiného	jiný	k2eAgNnSc2d1	jiné
podráždění	podráždění	k1gNnSc2	podráždění
(	(	kIx(	(
<g/>
např.	např.	kA	např.
štípnutí	štípnutí	k1gNnSc1	štípnutí
komárem	komár	k1gMnSc7	komár
<g/>
,	,	kIx,	,
<g/>
angína	angína	k1gFnSc1	angína
atp.	atp.	kA	atp.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
nastartuje	nastartovat	k5eAaPmIp3nS	nastartovat
nezvratnou	zvratný	k2eNgFnSc4d1	nezvratná
tvorbu	tvorba	k1gFnSc4	tvorba
protilátek	protilátka	k1gFnPc2	protilátka
proti	proti	k7c3	proti
vlastním	vlastní	k2eAgFnPc3d1	vlastní
buňkám	buňka	k1gFnPc3	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
autoimunitní	autoimunitní	k2eAgFnSc3d1	autoimunitní
podstatě	podstata	k1gFnSc3	podstata
diabetu	diabetes	k1gInSc2	diabetes
byl	být	k5eAaImAgInS	být
též	též	k9	též
dokázán	dokázat	k5eAaPmNgInS	dokázat
i	i	k9	i
sklon	sklon	k1gInSc1	sklon
diabetiků	diabetik	k1gMnPc2	diabetik
k	k	k7c3	k
jiným	jiný	k2eAgFnPc3d1	jiná
autoimunitním	autoimunitní	k2eAgFnPc3d1	autoimunitní
nemocem	nemoc	k1gFnPc3	nemoc
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Hypotyreóza	Hypotyreóza	k1gFnSc1	Hypotyreóza
<g/>
,	,	kIx,	,
Diabetická	diabetický	k2eAgFnSc1d1	diabetická
neuropatie	neuropatie	k1gFnSc1	neuropatie
atp.	atp.	kA	atp.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Diabetes	diabetes	k1gInSc1	diabetes
mellitus	mellitus	k1gInSc1	mellitus
1	[number]	k4	1
<g/>
.	.	kIx.	.
typu	typ	k1gInSc2	typ
je	být	k5eAaImIp3nS	být
také	také	k9	také
někdy	někdy	k6eAd1	někdy
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k8xS	jako
juvenilní	juvenilní	k2eAgInSc1d1	juvenilní
diabetes	diabetes	k1gInSc1	diabetes
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
nejčastěji	často	k6eAd3	často
diagnostikován	diagnostikovat	k5eAaBmNgInS	diagnostikovat
kolem	kolem	k7c2	kolem
15	[number]	k4	15
<g/>
.	.	kIx.	.
roku	rok	k1gInSc2	rok
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Onemocnět	onemocnět	k5eAaPmF	onemocnět
jím	jíst	k5eAaImIp1nS	jíst
však	však	k9	však
mohou	moct	k5eAaImIp3nP	moct
jak	jak	k6eAd1	jak
novorozenci	novorozenec	k1gMnPc1	novorozenec
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
starší	starý	k2eAgMnPc1d2	starší
lidé	člověk	k1gMnPc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
teorií	teorie	k1gFnPc2	teorie
o	o	k7c6	o
původu	původ	k1gInSc6	původ
autoimunitních	autoimunitní	k2eAgNnPc2d1	autoimunitní
onemocnění	onemocnění	k1gNnPc2	onemocnění
je	být	k5eAaImIp3nS	být
teorie	teorie	k1gFnSc1	teorie
o	o	k7c6	o
vlivu	vliv	k1gInSc6	vliv
střevní	střevní	k2eAgFnSc2d1	střevní
mikroflóry	mikroflóra	k1gFnSc2	mikroflóra
na	na	k7c4	na
vznik	vznik	k1gInSc4	vznik
autoimunitních	autoimunitní	k2eAgNnPc2d1	autoimunitní
onemocnění	onemocnění	k1gNnPc2	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
této	tento	k3xDgFnSc2	tento
teorie	teorie	k1gFnSc2	teorie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
však	však	k9	však
zatím	zatím	k6eAd1	zatím
nebyla	být	k5eNaImAgFnS	být
plně	plně	k6eAd1	plně
experimentálně	experimentálně	k6eAd1	experimentálně
ověřena	ověřen	k2eAgFnSc1d1	ověřena
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
faktorů	faktor	k1gInPc2	faktor
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
rozvoj	rozvoj	k1gInSc4	rozvoj
diabetu	diabetes	k1gInSc2	diabetes
1	[number]	k4	1
<g/>
.	.	kIx.	.
typu	typ	k1gInSc2	typ
<g/>
,	,	kIx,	,
i	i	k8xC	i
složení	složení	k1gNnSc1	složení
střevní	střevní	k2eAgFnSc2d1	střevní
mikroflóry	mikroflóra	k1gFnSc2	mikroflóra
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Z	z	k7c2	z
genetického	genetický	k2eAgNnSc2d1	genetické
hlediska	hledisko	k1gNnSc2	hledisko
je	být	k5eAaImIp3nS	být
diabetes	diabetes	k1gInSc1	diabetes
1	[number]	k4	1
<g/>
.	.	kIx.	.
typu	typ	k1gInSc2	typ
velmi	velmi	k6eAd1	velmi
příbuzný	příbuzný	k2eAgMnSc1d1	příbuzný
hypertenzi	hypertenze	k1gFnSc3	hypertenze
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Diabetes	diabetes	k1gInSc1	diabetes
mellitus	mellitus	k1gInSc1	mellitus
2	[number]	k4	2
<g/>
.	.	kIx.	.
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Označovaný	označovaný	k2eAgMnSc1d1	označovaný
také	také	k9	také
jako	jako	k9	jako
non-inzulin-dependentní	nonnzulinependentní	k2eAgMnSc1d1	non-inzulin-dependentní
(	(	kIx(	(
<g/>
NIDDM	NIDDM	kA	NIDDM
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Diabetes	diabetes	k1gInSc1	diabetes
mellitus	mellitus	k1gInSc1	mellitus
2	[number]	k4	2
<g/>
.	.	kIx.	.
typu	typ	k1gInSc2	typ
je	být	k5eAaImIp3nS	být
onemocnění	onemocnění	k1gNnSc1	onemocnění
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
podmíněno	podmíněn	k2eAgNnSc1d1	podmíněno
nerovnováhou	nerovnováha	k1gFnSc7	nerovnováha
mezi	mezi	k7c7	mezi
sekrecí	sekrece	k1gFnSc7	sekrece
a	a	k8xC	a
účinkem	účinek	k1gInSc7	účinek
inzulinu	inzulin	k1gInSc2	inzulin
v	v	k7c6	v
metabolismu	metabolismus	k1gInSc6	metabolismus
glukózy	glukóza	k1gFnSc2	glukóza
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
slinivka	slinivka	k1gFnSc1	slinivka
diabetiků	diabetik	k1gMnPc2	diabetik
2	[number]	k4	2
<g/>
.	.	kIx.	.
typu	typ	k1gInSc2	typ
produkuje	produkovat	k5eAaImIp3nS	produkovat
nadbytek	nadbytek	k1gInSc1	nadbytek
inzulinu	inzulin	k1gInSc2	inzulin
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
jejich	jejich	k3xOp3gNnSc1	jejich
tělo	tělo	k1gNnSc1	tělo
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
inzulin	inzulin	k1gInSc4	inzulin
více	hodně	k6eAd2	hodně
či	či	k8xC	či
méně	málo	k6eAd2	málo
"	"	kIx"	"
<g/>
rezistentní	rezistentní	k2eAgMnSc1d1	rezistentní
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
relativní	relativní	k2eAgInSc1d1	relativní
nedostatek	nedostatek	k1gInSc1	nedostatek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Léčí	léčit	k5eAaImIp3nP	léčit
se	se	k3xPyFc4	se
perorálními	perorální	k2eAgMnPc7d1	perorální
antidiabetiky	antidiabetik	k1gMnPc7	antidiabetik
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jsou	být	k5eAaImIp3nP	být
ústy	ústa	k1gNnPc7	ústa
podávané	podávaný	k2eAgInPc4d1	podávaný
léky	lék	k1gInPc7	lék
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
citlivost	citlivost	k1gFnSc4	citlivost
k	k	k7c3	k
inzulinu	inzulin	k1gInSc3	inzulin
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
závažnosti	závažnost	k1gFnSc3	závažnost
onemocnění	onemocnění	k1gNnSc2	onemocnění
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
rozvinutá	rozvinutý	k2eAgFnSc1d1	rozvinutá
inzulínová	inzulínový	k2eAgFnSc1d1	inzulínová
rezistence	rezistence	k1gFnSc1	rezistence
léčena	léčit	k5eAaImNgFnS	léčit
kombinovanou	kombinovaný	k2eAgFnSc7d1	kombinovaná
terapií	terapie	k1gFnSc7	terapie
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
terapie	terapie	k1gFnSc1	terapie
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
podávání	podávání	k1gNnSc6	podávání
alespoň	alespoň	k9	alespoň
dvou	dva	k4xCgFnPc2	dva
perorálních	perorální	k2eAgFnPc2d1	perorální
antidiabetik	antidiabetika	k1gFnPc2	antidiabetika
a	a	k8xC	a
hormonu	hormon	k1gInSc2	hormon
inzulínu	inzulín	k1gInSc2	inzulín
v	v	k7c6	v
injekcích	injekce	k1gFnPc6	injekce
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
kroku	krok	k1gInSc3	krok
se	se	k3xPyFc4	se
uchyluje	uchylovat	k5eAaImIp3nS	uchylovat
především	především	k9	především
v	v	k7c6	v
momentě	moment	k1gInSc6	moment
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
existuje	existovat	k5eAaImIp3nS	existovat
zvýšené	zvýšený	k2eAgNnSc4d1	zvýšené
riziko	riziko	k1gNnSc4	riziko
rozvinutí	rozvinutí	k1gNnSc2	rozvinutí
DM	dm	kA	dm
1	[number]	k4	1
<g/>
.	.	kIx.	.
typu	typ	k1gInSc2	typ
na	na	k7c6	na
pokladě	poklad	k1gInSc6	poklad
DM	dm	kA	dm
2	[number]	k4	2
<g/>
.	.	kIx.	.
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
stav	stav	k1gInSc1	stav
nastává	nastávat	k5eAaImIp3nS	nastávat
obzvláště	obzvláště	k6eAd1	obzvláště
v	v	k7c6	v
případe	případ	k1gInSc5	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
buňky	buňka	k1gFnSc2	buňka
slinivky	slinivka	k1gFnSc2	slinivka
břišní	břišní	k2eAgFnSc2d1	břišní
jsou	být	k5eAaImIp3nP	být
již	již	k6eAd1	již
vyčerpány	vyčerpat	k5eAaPmNgFnP	vyčerpat
z	z	k7c2	z
nadměrné	nadměrný	k2eAgFnSc2d1	nadměrná
tvorby	tvorba	k1gFnSc2	tvorba
inzulínu	inzulín	k1gInSc2	inzulín
a	a	k8xC	a
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
jejich	jejich	k3xOp3gInSc3	jejich
zániku	zánik	k1gInSc3	zánik
<g/>
.	.	kIx.	.
</s>
<s>
Hladina	hladina	k1gFnSc1	hladina
inzulínu	inzulín	k1gInSc2	inzulín
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
poté	poté	k6eAd1	poté
prudce	prudko	k6eAd1	prudko
padá	padat	k5eAaImIp3nS	padat
<g/>
.	.	kIx.	.
</s>
<s>
Diabetes	diabetes	k1gInSc1	diabetes
2	[number]	k4	2
<g/>
.	.	kIx.	.
typu	typ	k1gInSc2	typ
je	být	k5eAaImIp3nS	být
označován	označovat	k5eAaImNgInS	označovat
diabetem	diabetes	k1gInSc7	diabetes
dospělých	dospělí	k1gMnPc2	dospělí
(	(	kIx(	(
<g/>
obvyklá	obvyklý	k2eAgFnSc1d1	obvyklá
manifestace	manifestace	k1gFnSc1	manifestace
po	po	k7c6	po
40	[number]	k4	40
<g/>
.	.	kIx.	.
roce	rok	k1gInSc6	rok
života	život	k1gInSc2	život
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
současnému	současný	k2eAgInSc3d1	současný
sedavému	sedavý	k2eAgInSc3d1	sedavý
způsobu	způsob	k1gInSc3	způsob
života	život	k1gInSc2	život
tuto	tento	k3xDgFnSc4	tento
nemoc	nemoc	k1gFnSc4	nemoc
mohou	moct	k5eAaImIp3nP	moct
dostat	dostat	k5eAaPmF	dostat
i	i	k9	i
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
jsou	být	k5eAaImIp3nP	být
vlohy	vloha	k1gFnPc4	vloha
k	k	k7c3	k
diabetu	diabetes	k1gInSc3	diabetes
2	[number]	k4	2
<g/>
.	.	kIx.	.
typu	typ	k1gInSc2	typ
dědičně	dědičně	k6eAd1	dědičně
přenášeny	přenášet	k5eAaImNgInP	přenášet
z	z	k7c2	z
generace	generace	k1gFnSc2	generace
na	na	k7c4	na
generaci	generace	k1gFnSc4	generace
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
nových	nový	k2eAgNnPc2d1	nové
studií	studio	k1gNnPc2	studio
z	z	k7c2	z
Newcastle	Newcastle	k1gFnSc2	Newcastle
University	universita	k1gFnSc2	universita
by	by	kYmCp3nP	by
nemoc	nemoc	k1gFnSc1	nemoc
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
léčena	léčit	k5eAaImNgFnS	léčit
přísnou	přísný	k2eAgFnSc7d1	přísná
nízkokalorickou	nízkokalorický	k2eAgFnSc7d1	nízkokalorická
dietou	dieta	k1gFnSc7	dieta
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgFnPc1d1	jiná
studie	studie	k1gFnPc1	studie
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
potenciál	potenciál	k1gInSc4	potenciál
v	v	k7c6	v
aloe	aloe	k1gNnSc6	aloe
obsažených	obsažený	k2eAgInPc2d1	obsažený
antidiabetických	antidiabetický	k2eAgInPc2d1	antidiabetický
fytosterolů	fytosterol	k1gInPc2	fytosterol
(	(	kIx(	(
<g/>
lophenol	lophenol	k1gInSc1	lophenol
<g/>
,	,	kIx,	,
cykloartanol	cykloartanol	k1gInSc1	cykloartanol
<g/>
)	)	kIx)	)
zmírňovat	zmírňovat	k5eAaImF	zmírňovat
poruchy	porucha	k1gFnPc4	porucha
metabolismu	metabolismus	k1gInSc2	metabolismus
glukózy	glukóza	k1gFnSc2	glukóza
a	a	k8xC	a
lipidů	lipid	k1gInPc2	lipid
u	u	k7c2	u
pacientů	pacient	k1gMnPc2	pacient
s	s	k7c7	s
prediabetem	prediabet	k1gInSc7	prediabet
a	a	k8xC	a
časným	časný	k2eAgInSc7d1	časný
neléčeným	léčený	k2eNgInSc7d1	neléčený
diabetem	diabetes	k1gInSc7	diabetes
.	.	kIx.	.
</s>
<s>
Označovaný	označovaný	k2eAgMnSc1d1	označovaný
také	také	k9	také
jako	jako	k9	jako
gestační	gestační	k2eAgInSc1d1	gestační
diabetes	diabetes	k1gInSc1	diabetes
mellitus	mellitus	k1gInSc1	mellitus
(	(	kIx(	(
<g/>
GDM	GDM	kA	GDM
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Gestační	Gestační	k2eAgInSc1d1	Gestační
diabetes	diabetes	k1gInSc1	diabetes
postihuje	postihovat	k5eAaImIp3nS	postihovat
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
použitých	použitý	k2eAgNnPc6d1	Použité
diagnostických	diagnostický	k2eAgNnPc6d1	diagnostické
kritériích	kritérion	k1gNnPc6	kritérion
až	až	k9	až
17	[number]	k4	17
%	%	kIx~	%
těhotných	těhotný	k2eAgFnPc2d1	těhotná
žen	žena	k1gFnPc2	žena
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
zachycen	zachycen	k2eAgInSc1d1	zachycen
při	při	k7c6	při
rutinních	rutinní	k2eAgInPc6d1	rutinní
krevních	krevní	k2eAgInPc6d1	krevní
testech	test	k1gInPc6	test
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
dělají	dělat	k5eAaImIp3nP	dělat
mezi	mezi	k7c7	mezi
24	[number]	k4	24
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
28	[number]	k4	28
<g/>
.	.	kIx.	.
týdnem	týden	k1gInSc7	týden
těhotenství	těhotenství	k1gNnSc2	těhotenství
<g/>
.	.	kIx.	.
</s>
<s>
Gestační	Gestační	k2eAgInSc1d1	Gestační
diabetes	diabetes	k1gInSc1	diabetes
postihuje	postihovat	k5eAaImIp3nS	postihovat
ženy	žena	k1gFnPc4	žena
s	s	k7c7	s
vrozenou	vrozený	k2eAgFnSc7d1	vrozená
dispozicí	dispozice	k1gFnSc7	dispozice
<g/>
,	,	kIx,	,
spouštěcím	spouštěcí	k2eAgInSc7d1	spouštěcí
faktorem	faktor	k1gInSc7	faktor
je	být	k5eAaImIp3nS	být
vliv	vliv	k1gInSc1	vliv
těhotenských	těhotenský	k2eAgInPc2d1	těhotenský
hormonů	hormon	k1gInPc2	hormon
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
hladina	hladina	k1gFnSc1	hladina
narůstá	narůstat	k5eAaImIp3nS	narůstat
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
těhotenství	těhotenství	k1gNnSc2	těhotenství
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
rizikové	rizikový	k2eAgInPc4d1	rizikový
faktory	faktor	k1gInPc4	faktor
rozvoje	rozvoj	k1gInSc2	rozvoj
GDM	GDM	kA	GDM
patří	patřit	k5eAaImIp3nS	patřit
výskyt	výskyt	k1gInSc1	výskyt
diabetu	diabetes	k1gInSc2	diabetes
2	[number]	k4	2
<g/>
.	.	kIx.	.
typu	typ	k1gInSc2	typ
v	v	k7c6	v
rodinné	rodinný	k2eAgFnSc6d1	rodinná
anamnéze	anamnéza	k1gFnSc6	anamnéza
<g/>
,	,	kIx,	,
věk	věk	k1gInSc4	věk
těhotné	těhotná	k1gFnSc2	těhotná
nad	nad	k7c4	nad
30	[number]	k4	30
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
obezita	obezita	k1gFnSc1	obezita
a	a	k8xC	a
nedostatek	nedostatek	k1gInSc1	nedostatek
fyzické	fyzický	k2eAgFnSc2d1	fyzická
aktivity	aktivita	k1gFnSc2	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Ženy	žena	k1gFnPc1	žena
se	s	k7c7	s
zvýšeným	zvýšený	k2eAgInSc7d1	zvýšený
krevním	krevní	k2eAgInSc7d1	krevní
tlakem	tlak	k1gInSc7	tlak
(	(	kIx(	(
<g/>
hypertenzí	hypertenze	k1gFnPc2	hypertenze
<g/>
)	)	kIx)	)
mají	mít	k5eAaImIp3nP	mít
2	[number]	k4	2
<g/>
x	x	k?	x
vyšší	vysoký	k2eAgNnSc4d2	vyšší
riziko	riziko	k1gNnSc4	riziko
GDM	GDM	kA	GDM
v	v	k7c6	v
těhotenství	těhotenství	k1gNnSc6	těhotenství
<g/>
.	.	kIx.	.
</s>
<s>
Nedostatečně	dostatečně	k6eNd1	dostatečně
léčený	léčený	k2eAgInSc4d1	léčený
GDM	GDM	kA	GDM
představuje	představovat	k5eAaImIp3nS	představovat
zvýšené	zvýšený	k2eAgNnSc4d1	zvýšené
riziko	riziko	k1gNnSc4	riziko
pro	pro	k7c4	pro
matku	matka	k1gFnSc4	matka
i	i	k8xC	i
plod	plod	k1gInSc4	plod
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
dobře	dobře	k6eAd1	dobře
léčeného	léčený	k2eAgMnSc2d1	léčený
GDM	GDM	kA	GDM
jsou	být	k5eAaImIp3nP	být
rizika	riziko	k1gNnPc4	riziko
nízká	nízký	k2eAgNnPc4d1	nízké
<g/>
,	,	kIx,	,
srovnatelná	srovnatelný	k2eAgNnPc4d1	srovnatelné
s	s	k7c7	s
fyziologicky	fyziologicky	k6eAd1	fyziologicky
probíhajícím	probíhající	k2eAgNnSc7d1	probíhající
těhotenstvím	těhotenství	k1gNnSc7	těhotenství
<g/>
.	.	kIx.	.
</s>
<s>
Glukóza	glukóza	k1gFnSc1	glukóza
cirkulující	cirkulující	k2eAgFnSc1d1	cirkulující
ve	v	k7c6	v
zvýšeném	zvýšený	k2eAgNnSc6d1	zvýšené
množství	množství	k1gNnSc6	množství
v	v	k7c6	v
matčině	matčin	k2eAgFnSc6d1	matčina
krvi	krev	k1gFnSc6	krev
přestupuje	přestupovat	k5eAaImIp3nS	přestupovat
přes	přes	k7c4	přes
placentu	placenta	k1gFnSc4	placenta
do	do	k7c2	do
plodu	plod	k1gInSc2	plod
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
zvýšenou	zvýšený	k2eAgFnSc7d1	zvýšená
tvorbou	tvorba	k1gFnSc7	tvorba
vlastního	vlastní	k2eAgInSc2d1	vlastní
inzulinu	inzulin	k1gInSc2	inzulin
<g/>
.	.	kIx.	.
</s>
<s>
Kombinace	kombinace	k1gFnPc1	kombinace
přemíry	přemíra	k1gFnSc2	přemíra
glukózy	glukóza	k1gFnSc2	glukóza
a	a	k8xC	a
přemíry	přemíra	k1gFnSc2	přemíra
inzulinu	inzulin	k1gInSc2	inzulin
se	se	k3xPyFc4	se
u	u	k7c2	u
plodu	plod	k1gInSc2	plod
projevuje	projevovat	k5eAaImIp3nS	projevovat
výskytem	výskyt	k1gInSc7	výskyt
tzv.	tzv.	kA	tzv.
diabetické	diabetický	k2eAgFnSc2d1	diabetická
fetopatie	fetopatie	k1gFnSc2	fetopatie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
vyšší	vysoký	k2eAgFnSc7d2	vyšší
porodní	porodní	k2eAgFnSc7d1	porodní
hmotností	hmotnost	k1gFnSc7	hmotnost
(	(	kIx(	(
<g/>
nad	nad	k7c4	nad
4000	[number]	k4	4000
g	g	kA	g
<g/>
)	)	kIx)	)
s	s	k7c7	s
rizikem	riziko	k1gNnSc7	riziko
porodního	porodní	k2eAgNnSc2d1	porodní
poranění	poranění	k1gNnSc2	poranění
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
narození	narození	k1gNnSc6	narození
je	být	k5eAaImIp3nS	být
novorozenec	novorozenec	k1gMnSc1	novorozenec
ohrožen	ohrozit	k5eAaPmNgInS	ohrozit
hypoglykemií	hypoglykemie	k1gFnSc7	hypoglykemie
<g/>
,	,	kIx,	,
poruchami	porucha	k1gFnPc7	porucha
dýchání	dýchání	k1gNnSc2	dýchání
a	a	k8xC	a
dalšími	další	k2eAgFnPc7d1	další
komplikacemi	komplikace	k1gFnPc7	komplikace
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
vyšší	vysoký	k2eAgFnSc7d2	vyšší
hladinou	hladina	k1gFnSc7	hladina
žlučového	žlučový	k2eAgNnSc2d1	žlučové
barviva	barvivo	k1gNnSc2	barvivo
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
k	k	k7c3	k
novorozenecké	novorozenecký	k2eAgFnSc3d1	novorozenecká
žloutence	žloutenka	k1gFnSc3	žloutenka
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
děti	dítě	k1gFnPc1	dítě
mají	mít	k5eAaImIp3nP	mít
od	od	k7c2	od
dětství	dětství	k1gNnSc2	dětství
větší	veliký	k2eAgInSc4d2	veliký
sklon	sklon	k1gInSc4	sklon
k	k	k7c3	k
nadváze	nadváha	k1gFnSc3	nadváha
a	a	k8xC	a
obezitě	obezita	k1gFnSc3	obezita
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
během	během	k7c2	během
života	život	k1gInSc2	život
vyšší	vysoký	k2eAgNnSc4d2	vyšší
riziko	riziko	k1gNnSc4	riziko
diabetu	diabetes	k1gInSc2	diabetes
2	[number]	k4	2
<g/>
.	.	kIx.	.
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Ženy	žena	k1gFnPc1	žena
s	s	k7c7	s
GDM	GDM	kA	GDM
mají	mít	k5eAaImIp3nP	mít
vyšší	vysoký	k2eAgNnSc4d2	vyšší
riziko	riziko	k1gNnSc4	riziko
hypertenze	hypertenze	k1gFnSc2	hypertenze
v	v	k7c6	v
těhotenství	těhotenství	k1gNnSc6	těhotenství
a	a	k8xC	a
preeklampsie	preeklampsie	k1gFnSc1	preeklampsie
<g/>
,	,	kIx,	,
riziko	riziko	k1gNnSc1	riziko
předčasného	předčasný	k2eAgInSc2d1	předčasný
porodu	porod	k1gInSc2	porod
a	a	k8xC	a
porodu	porod	k1gInSc2	porod
císařským	císařský	k2eAgInSc7d1	císařský
řezem	řez	k1gInSc7	řez
<g/>
.	.	kIx.	.
</s>
<s>
Gestační	Gestační	k2eAgInSc1d1	Gestační
diabetes	diabetes	k1gInSc1	diabetes
po	po	k7c6	po
porodu	porod	k1gInSc6	porod
zpravidla	zpravidla	k6eAd1	zpravidla
mizí	mizet	k5eAaImIp3nP	mizet
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
u	u	k7c2	u
těchto	tento	k3xDgFnPc2	tento
žen	žena	k1gFnPc2	žena
ověřuje	ověřovat	k5eAaImIp3nS	ověřovat
kontrolním	kontrolní	k2eAgInSc7d1	kontrolní
testem	test	k1gInSc7	test
za	za	k7c4	za
3	[number]	k4	3
až	až	k9	až
6	[number]	k4	6
měsíců	měsíc	k1gInPc2	měsíc
po	po	k7c6	po
porodu	porod	k1gInSc6	porod
<g/>
.	.	kIx.	.
</s>
<s>
Ženy	žena	k1gFnPc1	žena
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
gestační	gestační	k2eAgInSc4d1	gestační
diabetes	diabetes	k1gInSc4	diabetes
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
několikanásobně	několikanásobně	k6eAd1	několikanásobně
vyšší	vysoký	k2eAgNnSc4d2	vyšší
riziko	riziko	k1gNnSc4	riziko
diabetu	diabetes	k1gInSc2	diabetes
2	[number]	k4	2
<g/>
.	.	kIx.	.
typu	typ	k1gInSc2	typ
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
vzácné	vzácný	k2eAgInPc1d1	vzácný
typy	typ	k1gInPc1	typ
diabetu	diabetes	k1gInSc2	diabetes
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
způsobeny	způsobit	k5eAaPmNgInP	způsobit
genetickým	genetický	k2eAgInSc7d1	genetický
defektem	defekt	k1gInSc7	defekt
B	B	kA	B
buněk	buňka	k1gFnPc2	buňka
pankreatu	pankreas	k1gInSc2	pankreas
<g/>
,	,	kIx,	,
genetickým	genetický	k2eAgInSc7d1	genetický
defektem	defekt	k1gInSc7	defekt
struktury	struktura	k1gFnSc2	struktura
inzulinu	inzulin	k1gInSc2	inzulin
nebo	nebo	k8xC	nebo
chorobami	choroba	k1gFnPc7	choroba
pankreatu	pankreas	k1gInSc2	pankreas
(	(	kIx(	(
<g/>
onemocněním	onemocnění	k1gNnPc3	onemocnění
exokrinního	exokrinní	k2eAgInSc2d1	exokrinní
pankreatu	pankreas	k1gInSc2	pankreas
<g/>
,	,	kIx,	,
endokrinopatie	endokrinopatie	k1gFnPc1	endokrinopatie
<g/>
,	,	kIx,	,
chemikálie	chemikálie	k1gFnPc1	chemikálie
či	či	k8xC	či
léky	lék	k1gInPc1	lék
<g/>
,	,	kIx,	,
infekce	infekce	k1gFnPc1	infekce
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
Diabetes	diabetes	k1gInSc1	diabetes
mellitus	mellitus	k1gInSc1	mellitus
není	být	k5eNaImIp3nS	být
porucha	porucha	k1gFnSc1	porucha
jen	jen	k9	jen
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
i	i	k9	i
u	u	k7c2	u
jiných	jiný	k2eAgMnPc2d1	jiný
savců	savec	k1gMnPc2	savec
<g/>
:	:	kIx,	:
Zejména	zejména	k9	zejména
díky	díky	k7c3	díky
veterinářské	veterinářský	k2eAgFnSc3d1	veterinářská
praxi	praxe	k1gFnSc3	praxe
víme	vědět	k5eAaImIp1nP	vědět
o	o	k7c6	o
takových	takový	k3xDgFnPc6	takový
poruchách	porucha	k1gFnPc6	porucha
u	u	k7c2	u
koček	kočka	k1gFnPc2	kočka
a	a	k8xC	a
psů	pes	k1gMnPc2	pes
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gFnPc1	jejich
obdoby	obdoba	k1gFnPc1	obdoba
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
i	i	k9	i
u	u	k7c2	u
jiných	jiný	k2eAgInPc2d1	jiný
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
znám	znám	k2eAgInSc1d1	znám
je	být	k5eAaImIp3nS	být
přirozený	přirozený	k2eAgInSc1d1	přirozený
diabet	diabet	k1gInSc1	diabet
u	u	k7c2	u
osmáků	osmák	k1gMnPc2	osmák
degu	degus	k1gInSc2	degus
<g/>
.	.	kIx.	.
</s>
<s>
Nadměrné	nadměrný	k2eAgNnSc1d1	nadměrné
vylučování	vylučování	k1gNnSc1	vylučování
moči	moč	k1gFnSc2	moč
je	být	k5eAaImIp3nS	být
symptomem	symptom	k1gInSc7	symptom
pro	pro	k7c4	pro
více	hodně	k6eAd2	hodně
nemocí	nemoc	k1gFnPc2	nemoc
(	(	kIx(	(
<g/>
nejčastěji	často	k6eAd3	často
ledvin	ledvina	k1gFnPc2	ledvina
<g/>
)	)	kIx)	)
a	a	k8xC	a
slovo	slovo	k1gNnSc1	slovo
diabetes	diabetes	k1gInSc1	diabetes
často	často	k6eAd1	často
tvoří	tvořit	k5eAaImIp3nS	tvořit
jejich	jejich	k3xOp3gInSc4	jejich
název	název	k1gInSc4	název
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgMnPc1d3	nejznámější
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
výše	výše	k1gFnSc2	výše
popisovaného	popisovaný	k2eAgInSc2d1	popisovaný
diabetu	diabetes	k1gInSc2	diabetes
mellitu	mellit	k1gInSc2	mellit
<g/>
)	)	kIx)	)
i	i	k9	i
diabetes	diabetes	k1gInSc1	diabetes
insipidus	insipidus	k1gInSc1	insipidus
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tomto	tento	k3xDgInSc6	tento
typu	typ	k1gInSc6	typ
diabetu	diabetes	k1gInSc2	diabetes
není	být	k5eNaImIp3nS	být
narušena	narušit	k5eAaPmNgFnS	narušit
hormonální	hormonální	k2eAgFnSc1d1	hormonální
rovnováha	rovnováha	k1gFnSc1	rovnováha
hormonů	hormon	k1gInPc2	hormon
pankreatu	pankreas	k1gInSc2	pankreas
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vylučování	vylučování	k1gNnSc1	vylučování
antidiuretického	antidiuretický	k2eAgInSc2d1	antidiuretický
hormonu	hormon	k1gInSc2	hormon
(	(	kIx(	(
<g/>
ADH	ADH	kA	ADH
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
reguluje	regulovat	k5eAaImIp3nS	regulovat
tvorbu	tvorba	k1gFnSc4	tvorba
moči	moč	k1gFnSc2	moč
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
medicíny	medicína	k1gFnSc2	medicína
založené	založený	k2eAgFnSc2d1	založená
na	na	k7c6	na
důkazech	důkaz	k1gInPc6	důkaz
(	(	kIx(	(
<g/>
evidence-based	evidenceased	k1gMnSc1	evidence-based
medicine	medicinout	k5eAaPmIp3nS	medicinout
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
diabetes	diabetes	k1gInSc1	diabetes
mellitus	mellitus	k1gInSc4	mellitus
metabolickou	metabolický	k2eAgFnSc7d1	metabolická
nemocí	nemoc	k1gFnSc7	nemoc
charakterizovanou	charakterizovaný	k2eAgFnSc7d1	charakterizovaná
glykémií	glykémie	k1gFnSc7	glykémie
nalačno	nalačno	k6eAd1	nalačno
>	>	kIx)	>
<g/>
7	[number]	k4	7
mmol	mmolum	k1gNnPc2	mmolum
<g/>
/	/	kIx~	/
<g/>
l	l	kA	l
a	a	k8xC	a
glykémií	glykémie	k1gFnSc7	glykémie
za	za	k7c4	za
2	[number]	k4	2
hodiny	hodina	k1gFnSc2	hodina
>	>	kIx)	>
<g/>
11,1	[number]	k4	11,1
mmol	mmolum	k1gNnPc2	mmolum
<g/>
/	/	kIx~	/
<g/>
l	l	kA	l
při	při	k7c6	při
orálním	orální	k2eAgInSc6d1	orální
glukózovém	glukózový	k2eAgInSc6d1	glukózový
tolerančním	toleranční	k2eAgInSc6d1	toleranční
testu	test	k1gInSc6	test
(	(	kIx(	(
<g/>
oGTT	oGTT	k?	oGTT
<g/>
)	)	kIx)	)
s	s	k7c7	s
podáním	podání	k1gNnSc7	podání
75	[number]	k4	75
g	g	kA	g
glukózy	glukóza	k1gFnSc2	glukóza
<g/>
.	.	kIx.	.
</s>
<s>
Diabetes	diabetes	k1gInSc1	diabetes
mellitus	mellitus	k1gInSc4	mellitus
však	však	k9	však
není	být	k5eNaImIp3nS	být
jedinou	jediný	k2eAgFnSc7d1	jediná
poruchou	porucha	k1gFnSc7	porucha
glukózového	glukózový	k2eAgInSc2d1	glukózový
metabolismu	metabolismus	k1gInSc2	metabolismus
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
nezbytné	nezbytný	k2eAgNnSc1d1	nezbytný
ji	on	k3xPp3gFnSc4	on
odlišit	odlišit	k5eAaPmF	odlišit
od	od	k7c2	od
dalších	další	k2eAgFnPc2d1	další
poruch	porucha	k1gFnPc2	porucha
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mohou	moct	k5eAaImIp3nP	moct
znamenat	znamenat	k5eAaImF	znamenat
nejen	nejen	k6eAd1	nejen
zvýšené	zvýšený	k2eAgNnSc4d1	zvýšené
riziko	riziko	k1gNnSc4	riziko
manifestace	manifestace	k1gFnSc2	manifestace
diabetu	diabetes	k1gInSc2	diabetes
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
zvýšené	zvýšený	k2eAgNnSc4d1	zvýšené
riziko	riziko	k1gNnSc4	riziko
aterosklerotických	aterosklerotický	k2eAgFnPc2d1	aterosklerotický
cévních	cévní	k2eAgFnPc2d1	cévní
komplikací	komplikace	k1gFnPc2	komplikace
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
jedinci	jedinec	k1gMnPc7	jedinec
stejných	stejný	k2eAgFnPc2d1	stejná
věkových	věkový	k2eAgFnPc2d1	věková
skupin	skupina	k1gFnPc2	skupina
a	a	k8xC	a
normálními	normální	k2eAgFnPc7d1	normální
hladinami	hladina	k1gFnPc7	hladina
krevního	krevní	k2eAgInSc2d1	krevní
cukru	cukr	k1gInSc2	cukr
nalačno	nalačno	k6eAd1	nalačno
i	i	k9	i
po	po	k7c6	po
jídle	jídlo	k1gNnSc6	jídlo
<g/>
.	.	kIx.	.
</s>
<s>
Diabetes	diabetes	k1gInSc1	diabetes
mellitus	mellitus	k1gInSc1	mellitus
se	se	k3xPyFc4	se
ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
raných	raný	k2eAgNnPc6d1	rané
stádiích	stádium	k1gNnPc6	stádium
výrazně	výrazně	k6eAd1	výrazně
somaticky	somaticky	k6eAd1	somaticky
neprojevuje	projevovat	k5eNaImIp3nS	projevovat
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
DM1	DM1	k1gMnSc2	DM1
ničení	ničení	k1gNnSc2	ničení
B	B	kA	B
buněk	buňka	k1gFnPc2	buňka
probíhá	probíhat	k5eAaImIp3nS	probíhat
latentně	latentně	k6eAd1	latentně
po	po	k7c4	po
mnoho	mnoho	k4c4	mnoho
měsíců	měsíc	k1gInPc2	měsíc
až	až	k8xS	až
roků	rok	k1gInPc2	rok
do	do	k7c2	do
chvíle	chvíle	k1gFnSc2	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
zničeno	zničit	k5eAaPmNgNnS	zničit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
80	[number]	k4	80
%	%	kIx~	%
všech	všecek	k3xTgInPc2	všecek
B	B	kA	B
buněk	buňka	k1gFnPc2	buňka
a	a	k8xC	a
slinivka	slinivka	k1gFnSc1	slinivka
není	být	k5eNaImIp3nS	být
schopna	schopen	k2eAgFnSc1d1	schopna
zabezpečit	zabezpečit	k5eAaPmF	zabezpečit
spotřebu	spotřeba	k1gFnSc4	spotřeba
inzulinu	inzulin	k1gInSc2	inzulin
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Glykemie	Glykemie	k1gFnSc1	Glykemie
přestává	přestávat	k5eAaImIp3nS	přestávat
nabývat	nabývat	k5eAaImF	nabývat
fyziologických	fyziologický	k2eAgFnPc2d1	fyziologická
hodnot	hodnota	k1gFnPc2	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
chvíle	chvíle	k1gFnSc2	chvíle
lze	lze	k6eAd1	lze
jednoznačně	jednoznačně	k6eAd1	jednoznačně
prokázat	prokázat	k5eAaPmF	prokázat
diabetes	diabetes	k1gInSc4	diabetes
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
glukometr	glukometr	k1gInSc1	glukometr
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
by	by	kYmCp3nS	by
naměřil	naměřit	k5eAaBmAgInS	naměřit
budoucímu	budoucí	k2eAgMnSc3d1	budoucí
diabetikovi	diabetik	k1gMnSc3	diabetik
hodnotu	hodnota	k1gFnSc4	hodnota
glykemie	glykemie	k1gFnSc2	glykemie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
by	by	kYmCp3nS	by
neodpovídala	odpovídat	k5eNaImAgFnS	odpovídat
referenčním	referenční	k2eAgFnPc3d1	referenční
hodnotám	hodnota	k1gFnPc3	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
inzulin	inzulin	k1gInSc1	inzulin
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
vstup	vstup	k1gInSc4	vstup
glukózy	glukóza	k1gFnSc2	glukóza
do	do	k7c2	do
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
tělo	tělo	k1gNnSc4	tělo
neléčeného	léčený	k2eNgMnSc2d1	neléčený
diabetika	diabetik	k1gMnSc2	diabetik
trpí	trpět	k5eAaImIp3nS	trpět
nedostatkem	nedostatek	k1gInSc7	nedostatek
energie	energie	k1gFnSc2	energie
v	v	k7c6	v
buňkách	buňka	k1gFnPc6	buňka
<g/>
,	,	kIx,	,
a	a	k8xC	a
tu	tu	k6eAd1	tu
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
nahradit	nahradit	k5eAaPmF	nahradit
jiným	jiný	k2eAgInSc7d1	jiný
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
sekundární	sekundární	k2eAgInSc1d1	sekundární
zdroj	zdroj	k1gInSc1	zdroj
energie	energie	k1gFnSc2	energie
pro	pro	k7c4	pro
tělo	tělo	k1gNnSc4	tělo
je	být	k5eAaImIp3nS	být
energeticky	energeticky	k6eAd1	energeticky
nejvýhodnější	výhodný	k2eAgInPc4d3	nejvýhodnější
rozklad	rozklad	k1gInSc4	rozklad
tuků	tuk	k1gInPc2	tuk
<g/>
.	.	kIx.	.
</s>
<s>
Katabolismem	katabolismus	k1gInSc7	katabolismus
tuků	tuk	k1gInPc2	tuk
se	se	k3xPyFc4	se
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
požadovaná	požadovaný	k2eAgFnSc1d1	požadovaná
energie	energie	k1gFnSc1	energie
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
jako	jako	k9	jako
odpadní	odpadní	k2eAgFnPc4d1	odpadní
látky	látka	k1gFnPc4	látka
této	tento	k3xDgFnSc2	tento
reakce	reakce	k1gFnSc2	reakce
vznikají	vznikat	k5eAaImIp3nP	vznikat
ketolátky	ketolátko	k1gNnPc7	ketolátko
(	(	kIx(	(
<g/>
např.	např.	kA	např.
aceton	aceton	k1gInSc1	aceton
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
okyselují	okyselovat	k5eAaImIp3nP	okyselovat
vnitřní	vnitřní	k2eAgNnSc4d1	vnitřní
prostředí	prostředí	k1gNnSc4	prostředí
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
průběh	průběh	k1gInSc4	průběh
některých	některý	k3yIgFnPc2	některý
chemických	chemický	k2eAgFnPc2d1	chemická
reakcí	reakce	k1gFnPc2	reakce
v	v	k7c6	v
organismu	organismus	k1gInSc6	organismus
a	a	k8xC	a
také	také	k9	také
i	i	k9	i
jeho	jeho	k3xOp3gInSc1	jeho
metabolismus	metabolismus	k1gInSc1	metabolismus
-	-	kIx~	-
otrava	otrava	k1gFnSc1	otrava
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
tělo	tělo	k1gNnSc1	tělo
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Ledviny	ledvina	k1gFnPc1	ledvina
nedovedou	dovést	k5eNaPmIp3nP	dovést
molekuly	molekula	k1gFnPc1	molekula
glukózy	glukóza	k1gFnSc2	glukóza
při	při	k7c6	při
velké	velký	k2eAgFnSc6d1	velká
koncentraci	koncentrace	k1gFnSc6	koncentrace
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
udržet	udržet	k5eAaPmF	udržet
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
a	a	k8xC	a
propouští	propouštět	k5eAaImIp3nP	propouštět
ji	on	k3xPp3gFnSc4	on
do	do	k7c2	do
moči	moč	k1gFnSc2	moč
<g/>
.	.	kIx.	.
</s>
<s>
Přebytky	přebytek	k1gInPc1	přebytek
glukózy	glukóza	k1gFnSc2	glukóza
s	s	k7c7	s
sebou	se	k3xPyFc7	se
strhávají	strhávat	k5eAaImIp3nP	strhávat
molekuly	molekula	k1gFnPc1	molekula
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Čím	čí	k3xOyRgNnSc7	čí
vyšší	vysoký	k2eAgFnSc1d2	vyšší
glykemie	glykemie	k1gFnSc1	glykemie
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
větší	veliký	k2eAgNnSc1d2	veliký
množství	množství	k1gNnSc1	množství
moči	moč	k1gFnSc2	moč
<g/>
.	.	kIx.	.
</s>
<s>
Jediným	jediný	k2eAgNnSc7d1	jediné
východiskem	východisko	k1gNnSc7	východisko
je	být	k5eAaImIp3nS	být
vnést	vnést	k5eAaPmF	vnést
do	do	k7c2	do
metabolismu	metabolismus	k1gInSc2	metabolismus
znovu	znovu	k6eAd1	znovu
rovnováhu	rovnováha	k1gFnSc4	rovnováha
pomocí	pomocí	k7c2	pomocí
zevní	zevní	k2eAgFnSc2d1	zevní
aplikace	aplikace	k1gFnSc2	aplikace
inzulinu	inzulin	k1gInSc2	inzulin
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
si	se	k3xPyFc3	se
již	již	k9	již
tělo	tělo	k1gNnSc4	tělo
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
samo	sám	k3xTgNnSc4	sám
vyrobit	vyrobit	k5eAaPmF	vyrobit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zahájení	zahájení	k1gNnSc6	zahájení
léčby	léčba	k1gFnSc2	léčba
inzulinem	inzulin	k1gInSc7	inzulin
se	se	k3xPyFc4	se
částečně	částečně	k6eAd1	částečně
obnoví	obnovit	k5eAaPmIp3nS	obnovit
funkce	funkce	k1gFnSc1	funkce
zbývajících	zbývající	k2eAgInPc2d1	zbývající
B	B	kA	B
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
na	na	k7c4	na
určitou	určitý	k2eAgFnSc4d1	určitá
dobu	doba	k1gFnSc4	doba
obnoví	obnovit	k5eAaPmIp3nS	obnovit
vlastní	vlastní	k2eAgFnSc1d1	vlastní
sekrece	sekrece	k1gFnSc1	sekrece
inzulinu	inzulin	k1gInSc2	inzulin
<g/>
.	.	kIx.	.
</s>
<s>
Tomuto	tento	k3xDgNnSc3	tento
období	období	k1gNnSc3	období
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
remise	remise	k1gFnSc1	remise
a	a	k8xC	a
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
není	být	k5eNaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
pokrývat	pokrývat	k5eAaImF	pokrývat
celou	celý	k2eAgFnSc4d1	celá
spotřebu	spotřeba	k1gFnSc4	spotřeba
inzulinu	inzulin	k1gInSc2	inzulin
<g/>
.	.	kIx.	.
</s>
<s>
Remise	remise	k1gFnSc1	remise
má	mít	k5eAaImIp3nS	mít
přechodný	přechodný	k2eAgInSc4d1	přechodný
charakter	charakter	k1gInSc4	charakter
–	–	k?	–
trvá	trvat	k5eAaImIp3nS	trvat
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
jeden	jeden	k4xCgInSc4	jeden
rok	rok	k1gInSc4	rok
od	od	k7c2	od
manifestace	manifestace	k1gFnSc2	manifestace
diabetu	diabetes	k1gInSc2	diabetes
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgInPc1d1	základní
projevy	projev	k1gInPc1	projev
diabetu	diabetes	k1gInSc2	diabetes
vyplývající	vyplývající	k2eAgInSc4d1	vyplývající
z	z	k7c2	z
předchozího	předchozí	k2eAgInSc2d1	předchozí
textu	text	k1gInSc2	text
<g/>
:	:	kIx,	:
hyperglykémie	hyperglykémie	k1gFnSc1	hyperglykémie
žízeň	žízeň	k1gFnSc1	žízeň
časté	častý	k2eAgFnSc2d1	častá
a	a	k8xC	a
vydatné	vydatný	k2eAgNnSc4d1	vydatné
močení	močení	k1gNnSc4	močení
hubnutí	hubnutí	k1gNnSc6	hubnutí
únava	únava	k1gFnSc1	únava
poruchy	porucha	k1gFnSc2	porucha
vědomí	vědomí	k1gNnSc2	vědomí
diabetická	diabetický	k2eAgFnSc1d1	diabetická
ketoacidóza	ketoacidóza	k1gFnSc1	ketoacidóza
Cílem	cíl	k1gInSc7	cíl
léčby	léčba	k1gFnSc2	léčba
cukrovky	cukrovka	k1gFnSc2	cukrovka
je	být	k5eAaImIp3nS	být
umožnit	umožnit	k5eAaPmF	umožnit
nemocnému	nemocný	k2eAgMnSc3d1	nemocný
plnohodnotný	plnohodnotný	k2eAgInSc4d1	plnohodnotný
aktivní	aktivní	k2eAgInSc4d1	aktivní
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
kvalitativně	kvalitativně	k6eAd1	kvalitativně
blíží	blížit	k5eAaImIp3nS	blížit
co	co	k9	co
nejvíce	hodně	k6eAd3	hodně
normálu	normála	k1gFnSc4	normála
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgMnSc4	ten
lze	lze	k6eAd1	lze
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
dlouhodobou	dlouhodobý	k2eAgFnSc7d1	dlouhodobá
uspokojivou	uspokojivý	k2eAgFnSc7d1	uspokojivá
kompenzací	kompenzace	k1gFnSc7	kompenzace
diabetu	diabetes	k1gInSc2	diabetes
a	a	k8xC	a
prevencí	prevence	k1gFnPc2	prevence
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
kvalitní	kvalitní	k2eAgFnSc7d1	kvalitní
léčbou	léčba	k1gFnSc7	léčba
<g/>
,	,	kIx,	,
pozdních	pozdní	k2eAgFnPc2d1	pozdní
komplikací	komplikace	k1gFnPc2	komplikace
diabetu	diabetes	k1gInSc2	diabetes
<g/>
.	.	kIx.	.
</s>
<s>
Ukazatelem	ukazatel	k1gInSc7	ukazatel
dlouhodobé	dlouhodobý	k2eAgFnSc2d1	dlouhodobá
kompenzace	kompenzace	k1gFnSc2	kompenzace
diabetu	diabetes	k1gInSc2	diabetes
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
kontroluje	kontrolovat	k5eAaImIp3nS	kontrolovat
diabetolog	diabetolog	k1gMnSc1	diabetolog
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
výše	výše	k1gFnSc1	výše
glykovaného	glykovaný	k2eAgInSc2d1	glykovaný
hemoglobinu	hemoglobin	k1gInSc2	hemoglobin
<g/>
.	.	kIx.	.
</s>
<s>
Glykovaný	Glykovaný	k2eAgInSc1d1	Glykovaný
hemoglobin	hemoglobin	k1gInSc1	hemoglobin
vzniká	vznikat	k5eAaImIp3nS	vznikat
glykací	glykace	k1gFnSc7	glykace
bílkovinného	bílkovinný	k2eAgInSc2d1	bílkovinný
řetězce	řetězec	k1gInSc2	řetězec
hemoglobinu	hemoglobin	k1gInSc2	hemoglobin
(	(	kIx(	(
<g/>
červené	červený	k2eAgNnSc1d1	červené
krevní	krevní	k2eAgNnSc1d1	krevní
barvivo	barvivo	k1gNnSc1	barvivo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
intenzita	intenzita	k1gFnSc1	intenzita
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
koncentraci	koncentrace	k1gFnSc6	koncentrace
a	a	k8xC	a
době	doba	k1gFnSc6	doba
expozice	expozice	k1gFnSc2	expozice
glukózy	glukóza	k1gFnSc2	glukóza
<g/>
.	.	kIx.	.
</s>
<s>
Tudíž	tudíž	k8xC	tudíž
hodnota	hodnota	k1gFnSc1	hodnota
glykovaného	glykovaný	k2eAgInSc2d1	glykovaný
hemoglobinu	hemoglobin	k1gInSc2	hemoglobin
(	(	kIx(	(
<g/>
v	v	k7c6	v
%	%	kIx~	%
<g/>
)	)	kIx)	)
nese	nést	k5eAaImIp3nS	nést
informaci	informace	k1gFnSc4	informace
o	o	k7c4	o
kompenzaci	kompenzace	k1gFnSc4	kompenzace
diabetu	diabetes	k1gInSc2	diabetes
v	v	k7c6	v
časovém	časový	k2eAgNnSc6d1	časové
období	období	k1gNnSc6	období
3	[number]	k4	3
měsíce	měsíc	k1gInSc2	měsíc
před	před	k7c7	před
jejím	její	k3xOp3gNnSc7	její
stanovením	stanovení	k1gNnSc7	stanovení
(	(	kIx(	(
<g/>
erytrocyty	erytrocyt	k1gInPc7	erytrocyt
se	se	k3xPyFc4	se
obměňují	obměňovat	k5eAaImIp3nP	obměňovat
každé	každý	k3xTgFnSc6	každý
3	[number]	k4	3
měsíce	měsíc	k1gInSc2	měsíc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
terapie	terapie	k1gFnSc1	terapie
diabetu	diabetes	k1gInSc2	diabetes
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
třech	tři	k4xCgInPc6	tři
pilířích	pilíř	k1gInPc6	pilíř
<g/>
:	:	kIx,	:
inzulin	inzulin	k1gInSc1	inzulin
<g/>
,	,	kIx,	,
dieta	dieta	k1gFnSc1	dieta
a	a	k8xC	a
fyzická	fyzický	k2eAgFnSc1d1	fyzická
aktivita	aktivita	k1gFnSc1	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Přičemž	přičemž	k6eAd1	přičemž
inzulin	inzulin	k1gInSc1	inzulin
a	a	k8xC	a
fyzická	fyzický	k2eAgFnSc1d1	fyzická
aktivita	aktivita	k1gFnSc1	aktivita
glykemii	glykemie	k1gFnSc4	glykemie
snižují	snižovat	k5eAaImIp3nP	snižovat
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
tomu	ten	k3xDgNnSc3	ten
jídlo	jídlo	k1gNnSc1	jídlo
a	a	k8xC	a
antiregulační	antiregulační	k2eAgInPc1d1	antiregulační
hormony	hormon	k1gInPc1	hormon
glykemii	glykemie	k1gFnSc3	glykemie
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
neodhadnutí	neodhadnutí	k1gNnSc6	neodhadnutí
poměru	poměr	k1gInSc2	poměr
"	"	kIx"	"
<g/>
inzulin	inzulin	k1gInSc1	inzulin
:	:	kIx,	:
jídlo	jídlo	k1gNnSc1	jídlo
:	:	kIx,	:
pohyb	pohyb	k1gInSc1	pohyb
<g/>
"	"	kIx"	"
nastane	nastat	k5eAaPmIp3nS	nastat
výkyv	výkyv	k1gInSc1	výkyv
glykemie	glykemie	k1gFnSc2	glykemie
z	z	k7c2	z
normy	norma	k1gFnSc2	norma
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
akutních	akutní	k2eAgFnPc2d1	akutní
komplikací	komplikace	k1gFnPc2	komplikace
diabetu	diabetes	k1gInSc2	diabetes
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
špatné	špatný	k2eAgFnSc2d1	špatná
kompenzace	kompenzace	k1gFnSc2	kompenzace
diabetu	diabetes	k1gInSc2	diabetes
se	se	k3xPyFc4	se
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
výskytu	výskyt	k1gInSc2	výskyt
pozdních	pozdní	k2eAgFnPc2d1	pozdní
komplikací	komplikace	k1gFnPc2	komplikace
diabetu	diabetes	k1gInSc2	diabetes
<g/>
,	,	kIx,	,
např.	např.	kA	např.
diabetické	diabetický	k2eAgFnSc2d1	diabetická
vegetativní	vegetativní	k2eAgFnSc2d1	vegetativní
neuropatie	neuropatie	k1gFnSc2	neuropatie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
závažnými	závažný	k2eAgFnPc7d1	závažná
obtížemi	obtíž	k1gFnPc7	obtíž
charakterizovanými	charakterizovaný	k2eAgFnPc7d1	charakterizovaná
postižením	postižení	k1gNnSc7	postižení
nervově	nervově	k6eAd1	nervově
vegetativní	vegetativní	k2eAgFnSc2d1	vegetativní
regulace	regulace	k1gFnSc2	regulace
dalších	další	k2eAgInPc2d1	další
orgánů	orgán	k1gInPc2	orgán
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
poruchy	poruch	k1gInPc1	poruch
cirkulace	cirkulace	k1gFnSc2	cirkulace
(	(	kIx(	(
<g/>
řízení	řízení	k1gNnSc2	řízení
srdečního	srdeční	k2eAgInSc2d1	srdeční
rytmu	rytmus	k1gInSc2	rytmus
<g/>
,	,	kIx,	,
poruchy	porucha	k1gFnPc1	porucha
kapilárního	kapilární	k2eAgNnSc2d1	kapilární
prokrvení	prokrvení	k1gNnSc2	prokrvení
tkání	tkáň	k1gFnPc2	tkáň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
urologické	urologický	k2eAgFnPc1d1	urologická
poruchy	porucha	k1gFnPc1	porucha
při	při	k7c6	při
diabetu	diabetes	k1gInSc6	diabetes
(	(	kIx(	(
<g/>
sexuální	sexuální	k2eAgFnPc1d1	sexuální
dysfunkce	dysfunkce	k1gFnPc1	dysfunkce
<g/>
,	,	kIx,	,
impotence	impotence	k1gFnPc1	impotence
<g/>
,	,	kIx,	,
poruchy	porucha	k1gFnPc1	porucha
močení	močení	k1gNnSc2	močení
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
vegetativní	vegetativní	k2eAgFnPc1d1	vegetativní
poruchy	porucha	k1gFnPc1	porucha
funkce	funkce	k1gFnSc2	funkce
gastrointestinálního	gastrointestinální	k2eAgInSc2d1	gastrointestinální
traktu	trakt	k1gInSc2	trakt
při	při	k7c6	při
diabetu	diabetes	k1gInSc6	diabetes
(	(	kIx(	(
<g/>
diabetická	diabetický	k2eAgFnSc1d1	diabetická
enteropatie	enteropatie	k1gFnSc1	enteropatie
spojená	spojený	k2eAgFnSc1d1	spojená
s	s	k7c7	s
průjmy	průjem	k1gInPc7	průjem
nebo	nebo	k8xC	nebo
zácpou	zácpa	k1gFnSc7	zácpa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dosažení	dosažení	k1gNnSc3	dosažení
optimální	optimální	k2eAgFnSc2d1	optimální
kompenzace	kompenzace	k1gFnSc2	kompenzace
diabetu	diabetes	k1gInSc2	diabetes
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
podávat	podávat	k5eAaImF	podávat
inzulin	inzulin	k1gInSc4	inzulin
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
nejvíce	hodně	k6eAd3	hodně
napodobuje	napodobovat	k5eAaImIp3nS	napodobovat
přirozenou	přirozený	k2eAgFnSc4d1	přirozená
sekreci	sekrece	k1gFnSc4	sekrece
inzulinu	inzulin	k1gInSc2	inzulin
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
systematickou	systematický	k2eAgFnSc7d1	systematická
aplikací	aplikace	k1gFnSc7	aplikace
inzulinu	inzulin	k1gInSc2	inzulin
inzulinovými	inzulinový	k2eAgNnPc7d1	inzulinové
pery	pero	k1gNnPc7	pero
nebo	nebo	k8xC	nebo
inzulinovou	inzulinový	k2eAgFnSc7d1	inzulinová
pumpou	pumpa	k1gFnSc7	pumpa
<g/>
.	.	kIx.	.
</s>
<s>
Kontrolu	kontrola	k1gFnSc4	kontrola
kompenzace	kompenzace	k1gFnSc2	kompenzace
si	se	k3xPyFc3	se
provádí	provádět	k5eAaImIp3nS	provádět
sám	sám	k3xTgMnSc1	sám
diabetik	diabetik	k1gMnSc1	diabetik
pomocí	pomocí	k7c2	pomocí
přístroje	přístroj	k1gInSc2	přístroj
k	k	k7c3	k
měření	měření	k1gNnSc3	měření
hladiny	hladina	k1gFnSc2	hladina
glykemie	glykemie	k1gFnSc2	glykemie
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
glukometrem	glukometr	k1gInSc7	glukometr
<g/>
.	.	kIx.	.
</s>
<s>
Nedílnou	dílný	k2eNgFnSc7d1	nedílná
součástí	součást	k1gFnSc7	součást
léčby	léčba	k1gFnSc2	léčba
diabetu	diabetes	k1gInSc2	diabetes
je	být	k5eAaImIp3nS	být
kvalitní	kvalitní	k2eAgFnSc1d1	kvalitní
edukace	edukace	k1gFnSc1	edukace
diabetika	diabetik	k1gMnSc2	diabetik
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
níž	jenž	k3xRgFnSc2	jenž
by	by	kYmCp3nS	by
úspěch	úspěch	k1gInSc4	úspěch
nebyl	být	k5eNaImAgInS	být
možný	možný	k2eAgInSc1d1	možný
<g/>
.	.	kIx.	.
</s>
<s>
Experimentální	experimentální	k2eAgFnSc1d1	experimentální
terapie	terapie	k1gFnSc1	terapie
diabetu	diabetes	k1gInSc2	diabetes
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
typu	typ	k1gInSc2	typ
<g/>
:	:	kIx,	:
Ve	v	k7c6	v
fázi	fáze	k1gFnSc6	fáze
klinických	klinický	k2eAgFnPc2d1	klinická
zkoušek	zkouška	k1gFnPc2	zkouška
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
využití	využití	k1gNnSc1	využití
kmenových	kmenový	k2eAgFnPc2d1	kmenová
buněk	buňka	k1gFnPc2	buňka
z	z	k7c2	z
pupečníkové	pupečníkový	k2eAgFnSc2d1	pupečníková
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládaným	předpokládaný	k2eAgInSc7d1	předpokládaný
mechanismem	mechanismus	k1gInSc7	mechanismus
účinku	účinek	k1gInSc2	účinek
je	být	k5eAaImIp3nS	být
ovlivnění	ovlivnění	k1gNnSc4	ovlivnění
autoimunitního	autoimunitní	k2eAgInSc2d1	autoimunitní
procesu	proces	k1gInSc2	proces
a	a	k8xC	a
podpora	podpora	k1gFnSc1	podpora
regenerace	regenerace	k1gFnSc2	regenerace
beta-buněk	betauňka	k1gFnPc2	beta-buňka
Langerhansových	Langerhansův	k2eAgInPc2d1	Langerhansův
ostrůvků	ostrůvek	k1gInPc2	ostrůvek
<g/>
.	.	kIx.	.
glukometr	glukometr	k1gInSc1	glukometr
kontinuální	kontinuální	k2eAgInSc1d1	kontinuální
monitor	monitor	k1gInSc1	monitor
glykémie	glykémie	k1gFnSc2	glykémie
inzulinová	inzulinový	k2eAgFnSc1d1	inzulinová
pumpa	pumpa	k1gFnSc1	pumpa
inzulinové	inzulinový	k2eAgNnSc4d1	inzulinové
pero	pero	k1gNnSc4	pero
inzulin	inzulin	k1gInSc4	inzulin
GlucaGen	GlucaGen	k2eAgInSc4d1	GlucaGen
HypoKit	HypoKit	k2eAgInSc4d1	HypoKit
diabetický	diabetický	k2eAgInSc4d1	diabetický
deník	deník	k1gInSc4	deník
DiaPhan	DiaPhany	k1gInPc2	DiaPhany
diagnostické	diagnostický	k2eAgInPc4d1	diagnostický
proužky	proužek	k1gInPc4	proužek
Zásady	zásada	k1gFnPc1	zásada
diabetické	diabetický	k2eAgFnPc1d1	diabetická
diety	dieta	k1gFnPc1	dieta
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
totožné	totožný	k2eAgInPc1d1	totožný
s	s	k7c7	s
pravidly	pravidlo	k1gNnPc7	pravidlo
racionální	racionální	k2eAgMnPc1d1	racionální
výživy	výživa	k1gFnSc2	výživa
<g/>
.	.	kIx.	.
</s>
<s>
Tudíž	tudíž	k8xC	tudíž
rozdělení	rozdělení	k1gNnSc1	rozdělení
nutričních	nutriční	k2eAgFnPc2d1	nutriční
hodnot	hodnota	k1gFnPc2	hodnota
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
asi	asi	k9	asi
55	[number]	k4	55
%	%	kIx~	%
sacharidů	sacharid	k1gInPc2	sacharid
<g/>
,	,	kIx,	,
30	[number]	k4	30
%	%	kIx~	%
tuků	tuk	k1gInPc2	tuk
a	a	k8xC	a
15	[number]	k4	15
%	%	kIx~	%
bílkovin	bílkovina	k1gFnPc2	bílkovina
z	z	k7c2	z
celkového	celkový	k2eAgInSc2d1	celkový
kalorického	kalorický	k2eAgInSc2d1	kalorický
příjmu	příjem	k1gInSc2	příjem
<g/>
.	.	kIx.	.
</s>
<s>
Sacharidy	sacharid	k1gInPc1	sacharid
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgInP	mít
být	být	k5eAaImF	být
přijímány	přijímat	k5eAaImNgInP	přijímat
převážně	převážně	k6eAd1	převážně
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
škrobů	škrob	k1gInPc2	škrob
a	a	k8xC	a
nestravitelné	stravitelný	k2eNgFnPc4d1	nestravitelná
vlákniny	vláknina	k1gFnPc4	vláknina
<g/>
,	,	kIx,	,
lipidy	lipid	k1gInPc4	lipid
nejlépe	dobře	k6eAd3	dobře
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
nenasycených	nasycený	k2eNgInPc2d1	nenasycený
tuků	tuk	k1gInPc2	tuk
<g/>
.	.	kIx.	.
</s>
<s>
Kvalita	kvalita	k1gFnSc1	kvalita
i	i	k8xC	i
kvantita	kvantita	k1gFnSc1	kvantita
přijímané	přijímaný	k2eAgFnSc2d1	přijímaná
potravy	potrava	k1gFnSc2	potrava
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
taková	takový	k3xDgFnSc1	takový
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
diabetik	diabetik	k1gMnSc1	diabetik
netloustnul	tloustnout	k5eNaPmAgMnS	tloustnout
ani	ani	k8xC	ani
nehubnul	hubnout	k5eNaPmAgMnS	hubnout
<g/>
,	,	kIx,	,
a	a	k8xC	a
při	při	k7c6	při
tom	ten	k3xDgInSc6	ten
měl	mít	k5eAaImAgMnS	mít
glykemickou	glykemický	k2eAgFnSc4d1	glykemická
křivku	křivka	k1gFnSc4	křivka
v	v	k7c6	v
normě	norma	k1gFnSc6	norma
<g/>
.	.	kIx.	.
</s>
<s>
Složení	složení	k1gNnSc1	složení
potravy	potrava	k1gFnSc2	potrava
musí	muset	k5eAaImIp3nS	muset
také	také	k9	také
přispívat	přispívat	k5eAaImF	přispívat
k	k	k7c3	k
dosažení	dosažení	k1gNnSc3	dosažení
optimální	optimální	k2eAgFnSc2d1	optimální
hladiny	hladina	k1gFnSc2	hladina
krevních	krevní	k2eAgInPc2d1	krevní
tuků	tuk	k1gInPc2	tuk
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
(	(	kIx(	(
<g/>
HDL	HDL	kA	HDL
<g/>
,	,	kIx,	,
LDL	LDL	kA	LDL
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
nerovnováha	nerovnováha	k1gFnSc1	nerovnováha
přispívá	přispívat	k5eAaImIp3nS	přispívat
k	k	k7c3	k
ateroskleróze	ateroskleróza	k1gFnSc3	ateroskleróza
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
kornatění	kornatění	k1gNnSc1	kornatění
tepen	tepna	k1gFnPc2	tepna
způsobené	způsobený	k2eAgFnPc1d1	způsobená
ukládáním	ukládání	k1gNnSc7	ukládání
tuku	tuk	k1gInSc2	tuk
do	do	k7c2	do
jejich	jejich	k3xOp3gFnPc2	jejich
stěn	stěna	k1gFnPc2	stěna
<g/>
.	.	kIx.	.
</s>
<s>
Všeobecně	všeobecně	k6eAd1	všeobecně
převládá	převládat	k5eAaImIp3nS	převládat
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
diabetici	diabetik	k1gMnPc1	diabetik
nemohou	moct	k5eNaImIp3nP	moct
jíst	jíst	k5eAaImF	jíst
sladké	sladký	k2eAgFnPc1d1	sladká
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
léčbě	léčba	k1gFnSc6	léčba
DM	dm	kA	dm
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
toto	tento	k3xDgNnSc1	tento
tvrzení	tvrzení	k1gNnSc1	tvrzení
oprávněné	oprávněný	k2eAgNnSc1d1	oprávněné
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
DM	dm	kA	dm
2	[number]	k4	2
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
léčí	léčit	k5eAaImIp3nS	léčit
pouze	pouze	k6eAd1	pouze
dietou	dieta	k1gFnSc7	dieta
a	a	k8xC	a
perorálními	perorální	k2eAgMnPc7d1	perorální
antidiabetiky	antidiabetik	k1gMnPc7	antidiabetik
<g/>
,	,	kIx,	,
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
má	mít	k5eAaImIp3nS	mít
skladba	skladba	k1gFnSc1	skladba
jídla	jídlo	k1gNnSc2	jídlo
v	v	k7c6	v
léčbě	léčba	k1gFnSc6	léčba
obrovský	obrovský	k2eAgInSc4d1	obrovský
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
diabetici	diabetik	k1gMnPc1	diabetik
I.	I.	kA	I.
typu	typ	k1gInSc6	typ
jsou	být	k5eAaImIp3nP	být
vždy	vždy	k6eAd1	vždy
léčeni	léčit	k5eAaImNgMnP	léčit
inzulinem	inzulin	k1gInSc7	inzulin
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
diabetik	diabetik	k1gMnSc1	diabetik
I.	I.	kA	I.
typu	typ	k1gInSc2	typ
si	se	k3xPyFc3	se
může	moct	k5eAaImIp3nS	moct
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
diety	dieta	k1gFnSc2	dieta
zakomponovat	zakomponovat	k5eAaPmF	zakomponovat
i	i	k9	i
malé	malý	k2eAgNnSc4d1	malé
množství	množství	k1gNnSc4	množství
sladkého	sladký	k2eAgNnSc2d1	sladké
jídla	jídlo	k1gNnSc2	jídlo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
však	však	k9	však
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
náležitě	náležitě	k6eAd1	náležitě
kryto	krýt	k5eAaImNgNnS	krýt
inzulinem	inzulin	k1gInSc7	inzulin
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
diabetik	diabetik	k1gMnSc1	diabetik
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgMnS	mít
znát	znát	k5eAaImF	znát
glykemické	glykemický	k2eAgInPc4d1	glykemický
indexy	index	k1gInPc4	index
(	(	kIx(	(
<g/>
případně	případně	k6eAd1	případně
inzulinový	inzulinový	k2eAgInSc1d1	inzulinový
index	index	k1gInSc1	index
<g/>
)	)	kIx)	)
a	a	k8xC	a
počty	počet	k1gInPc1	počet
výměnných	výměnný	k2eAgFnPc2d1	výměnná
jednotek	jednotka	k1gFnPc2	jednotka
základních	základní	k2eAgFnPc2d1	základní
potravin	potravina	k1gFnPc2	potravina
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
měl	mít	k5eAaImAgInS	mít
přehled	přehled	k1gInSc1	přehled
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
bude	být	k5eAaImBp3nS	být
glykemie	glykemie	k1gFnSc1	glykemie
reagovat	reagovat	k5eAaBmF	reagovat
na	na	k7c4	na
potravinu	potravina	k1gFnSc4	potravina
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
hodlá	hodlat	k5eAaImIp3nS	hodlat
sníst	sníst	k5eAaPmF	sníst
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
zdrojem	zdroj	k1gInSc7	zdroj
energie	energie	k1gFnSc2	energie
pro	pro	k7c4	pro
pohyb	pohyb	k1gInSc4	pohyb
těla	tělo	k1gNnSc2	tělo
je	být	k5eAaImIp3nS	být
štěpení	štěpení	k1gNnSc4	štěpení
glukózy	glukóza	k1gFnSc2	glukóza
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvních	první	k4xOgFnPc6	první
minutách	minuta	k1gFnPc6	minuta
fyzické	fyzický	k2eAgFnSc2d1	fyzická
aktivity	aktivita	k1gFnSc2	aktivita
je	být	k5eAaImIp3nS	být
glukóza	glukóza	k1gFnSc1	glukóza
získávána	získáván	k2eAgFnSc1d1	získávána
ze	z	k7c2	z
svalového	svalový	k2eAgInSc2d1	svalový
glykogenu	glykogen	k1gInSc2	glykogen
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
odbourávání	odbourávání	k1gNnSc3	odbourávání
jaterního	jaterní	k2eAgInSc2d1	jaterní
glykogenu	glykogen	k1gInSc2	glykogen
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
znovu	znovu	k6eAd1	znovu
syntetizován	syntetizovat	k5eAaImNgMnS	syntetizovat
z	z	k7c2	z
glukózy	glukóza	k1gFnSc2	glukóza
v	v	k7c6	v
krevní	krevní	k2eAgFnSc6d1	krevní
plazmě	plazma	k1gFnSc6	plazma
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
poklesu	pokles	k1gInSc3	pokles
glykemie	glykemie	k1gFnSc2	glykemie
<g/>
.	.	kIx.	.
</s>
<s>
Klesá	klesat	k5eAaImIp3nS	klesat
spotřeba	spotřeba	k1gFnSc1	spotřeba
vnějšího	vnější	k2eAgInSc2d1	vnější
příjmu	příjem	k1gInSc2	příjem
inzulinu	inzulin	k1gInSc2	inzulin
a	a	k8xC	a
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
se	se	k3xPyFc4	se
vnímavost	vnímavost	k1gFnSc1	vnímavost
těla	tělo	k1gNnSc2	tělo
na	na	k7c4	na
inzulin	inzulin	k1gInSc4	inzulin
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
fyzické	fyzický	k2eAgFnSc6d1	fyzická
aktivitě	aktivita	k1gFnSc6	aktivita
tělo	tělo	k1gNnSc1	tělo
doplňuje	doplňovat	k5eAaImIp3nS	doplňovat
deficit	deficit	k1gInSc4	deficit
spotřebovaného	spotřebovaný	k2eAgInSc2d1	spotřebovaný
jaterního	jaterní	k2eAgInSc2d1	jaterní
glykogenu	glykogen	k1gInSc2	glykogen
během	během	k7c2	během
cvičení	cvičení	k1gNnPc2	cvičení
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
fyzická	fyzický	k2eAgFnSc1d1	fyzická
aktivita	aktivita	k1gFnSc1	aktivita
má	mít	k5eAaImIp3nS	mít
hypoglykemizující	hypoglykemizující	k2eAgInSc4d1	hypoglykemizující
vliv	vliv	k1gInSc4	vliv
ještě	ještě	k9	ještě
několik	několik	k4yIc4	několik
hodin	hodina	k1gFnPc2	hodina
po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
cvičení	cvičení	k1gNnPc2	cvičení
<g/>
.	.	kIx.	.
</s>
<s>
Ovlivnění	ovlivnění	k1gNnSc1	ovlivnění
glykémie	glykémie	k1gFnSc2	glykémie
pohybem	pohyb	k1gInSc7	pohyb
také	také	k9	také
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
intenzitě	intenzita	k1gFnSc6	intenzita
a	a	k8xC	a
délce	délka	k1gFnSc6	délka
trvání	trvání	k1gNnSc2	trvání
fyzické	fyzický	k2eAgFnSc2d1	fyzická
aktivity	aktivita	k1gFnSc2	aktivita
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
člověk	člověk	k1gMnSc1	člověk
spotřebuje	spotřebovat	k5eAaPmIp3nS	spotřebovat
více	hodně	k6eAd2	hodně
energie	energie	k1gFnSc2	energie
při	při	k7c6	při
dlouhodobějším	dlouhodobý	k2eAgInSc6d2	dlouhodobější
pohybu	pohyb	k1gInSc6	pohyb
s	s	k7c7	s
menší	malý	k2eAgFnSc7d2	menší
intenzitou	intenzita	k1gFnSc7	intenzita
než	než	k8xS	než
při	při	k7c6	při
několikaminutovém	několikaminutový	k2eAgInSc6d1	několikaminutový
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
intenzivním	intenzivní	k2eAgInSc6d1	intenzivní
pohybu	pohyb	k1gInSc6	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Fyzická	fyzický	k2eAgFnSc1d1	fyzická
aktivita	aktivita	k1gFnSc1	aktivita
je	být	k5eAaImIp3nS	být
nevhodná	vhodný	k2eNgFnSc1d1	nevhodná
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
případě	případ	k1gInSc6	případ
špatné	špatný	k2eAgFnPc4d1	špatná
kompenzace	kompenzace	k1gFnPc4	kompenzace
diabetu	diabetes	k1gInSc2	diabetes
(	(	kIx(	(
<g/>
kdy	kdy	k6eAd1	kdy
glykemie	glykemie	k1gFnSc1	glykemie
je	být	k5eAaImIp3nS	být
vyšší	vysoký	k2eAgFnSc1d2	vyšší
než	než	k8xS	než
17	[number]	k4	17
mmol	mmola	k1gFnPc2	mmola
<g/>
/	/	kIx~	/
<g/>
l	l	kA	l
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
deficitu	deficit	k1gInSc3	deficit
inzulinu	inzulin	k1gInSc2	inzulin
(	(	kIx(	(
<g/>
glukóza	glukóza	k1gFnSc1	glukóza
se	se	k3xPyFc4	se
nemůže	moct	k5eNaImIp3nS	moct
dostat	dostat	k5eAaPmF	dostat
do	do	k7c2	do
buňky	buňka	k1gFnSc2	buňka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
urychlují	urychlovat	k5eAaImIp3nP	urychlovat
katabolické	katabolický	k2eAgInPc1d1	katabolický
děje	děj	k1gInPc1	děj
<g/>
,	,	kIx,	,
při	při	k7c6	při
nichž	jenž	k3xRgInPc6	jenž
se	se	k3xPyFc4	se
rozkládají	rozkládat	k5eAaImIp3nP	rozkládat
tuky	tuk	k1gInPc1	tuk
na	na	k7c4	na
ketolátky	ketolátko	k1gNnPc7	ketolátko
<g/>
.	.	kIx.	.
</s>
<s>
Fyzická	fyzický	k2eAgFnSc1d1	fyzická
aktivita	aktivita	k1gFnSc1	aktivita
má	mít	k5eAaImIp3nS	mít
kromě	kromě	k7c2	kromě
zlepšení	zlepšení	k1gNnSc2	zlepšení
glykémie	glykémie	k1gFnSc2	glykémie
také	také	k9	také
pozitivní	pozitivní	k2eAgInSc4d1	pozitivní
dopad	dopad	k1gInSc4	dopad
na	na	k7c4	na
psychiku	psychika	k1gFnSc4	psychika
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
při	při	k7c6	při
léčbě	léčba	k1gFnSc6	léčba
jakékoliv	jakýkoliv	k3yIgFnSc2	jakýkoliv
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
zdravého	zdravý	k2eAgMnSc2d1	zdravý
dospělého	dospělý	k2eAgMnSc2d1	dospělý
diabetika	diabetik	k1gMnSc2	diabetik
není	být	k5eNaImIp3nS	být
důvod	důvod	k1gInSc4	důvod
k	k	k7c3	k
omezování	omezování	k1gNnSc3	omezování
sportovní	sportovní	k2eAgFnSc2d1	sportovní
aktivity	aktivita	k1gFnSc2	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
však	však	k9	však
nutno	nutno	k6eAd1	nutno
před	před	k7c7	před
každou	každý	k3xTgFnSc7	každý
větší	veliký	k2eAgFnSc7d2	veliký
fyzickou	fyzický	k2eAgFnSc7d1	fyzická
aktivitou	aktivita	k1gFnSc7	aktivita
změřit	změřit	k5eAaPmF	změřit
glykemii	glykemie	k1gFnSc4	glykemie
a	a	k8xC	a
podle	podle	k7c2	podle
její	její	k3xOp3gFnSc2	její
hladiny	hladina	k1gFnSc2	hladina
se	se	k3xPyFc4	se
dojíst	dojíst	k5eAaPmF	dojíst
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
ubrat	ubrat	k5eAaPmF	ubrat
inzulin	inzulin	k1gInSc4	inzulin
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
během	během	k7c2	během
pohybu	pohyb	k1gInSc2	pohyb
nedošlo	dojít	k5eNaPmAgNnS	dojít
k	k	k7c3	k
hypoglykemii	hypoglykemie	k1gFnSc3	hypoglykemie
<g/>
.	.	kIx.	.
</s>
<s>
Obezita	obezita	k1gFnSc1	obezita
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
rizikovým	rizikový	k2eAgInSc7d1	rizikový
faktorem	faktor	k1gInSc7	faktor
pro	pro	k7c4	pro
vznik	vznik	k1gInSc4	vznik
diabetu	diabetes	k1gInSc2	diabetes
2	[number]	k4	2
<g/>
.	.	kIx.	.
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Chirurgické	chirurgický	k2eAgNnSc1d1	chirurgické
řešení	řešení	k1gNnSc1	řešení
obezity	obezita	k1gFnSc2	obezita
při	při	k7c6	při
diabetu	diabetes	k1gInSc6	diabetes
2	[number]	k4	2
<g/>
.	.	kIx.	.
typu	typ	k1gInSc2	typ
je	být	k5eAaImIp3nS	být
nejúčinnější	účinný	k2eAgFnSc7d3	nejúčinnější
metodou	metoda	k1gFnSc7	metoda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vede	vést	k5eAaImIp3nS	vést
ke	k	k7c3	k
zlepšení	zlepšení	k1gNnSc3	zlepšení
kompenzace	kompenzace	k1gFnSc2	kompenzace
diabetu	diabetes	k1gInSc2	diabetes
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
cca	cca	kA	cca
80	[number]	k4	80
<g/>
%	%	kIx~	%
nemocných	nemocný	k1gMnPc2	nemocný
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
vymizení	vymizení	k1gNnSc3	vymizení
diabetu	diabetes	k1gInSc2	diabetes
a	a	k8xC	a
přechodu	přechod	k1gInSc2	přechod
do	do	k7c2	do
poruchy	porucha	k1gFnSc2	porucha
glukózové	glukózový	k2eAgFnSc2d1	glukózová
homeostázy	homeostáza	k1gFnSc2	homeostáza
či	či	k8xC	či
k	k	k7c3	k
úplné	úplný	k2eAgFnSc3d1	úplná
normalizaci	normalizace	k1gFnSc3	normalizace
tolerance	tolerance	k1gFnSc2	tolerance
glukózy	glukóza	k1gFnSc2	glukóza
<g/>
.	.	kIx.	.
</s>
<s>
Umožní	umožnit	k5eAaPmIp3nS	umožnit
obvykle	obvykle	k6eAd1	obvykle
vysazení	vysazení	k1gNnSc1	vysazení
inzulinu	inzulin	k1gInSc2	inzulin
a	a	k8xC	a
snížení	snížení	k1gNnSc2	snížení
dávek	dávka	k1gFnPc2	dávka
PAD	padnout	k5eAaPmDgMnS	padnout
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
BMI	BMI	kA	BMI
nad	nad	k7c4	nad
35	[number]	k4	35
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
jsou	být	k5eAaImIp3nP	být
<g/>
-li	i	k?	-li
přítomné	přítomný	k2eAgInPc4d1	přítomný
další	další	k2eAgInPc4d1	další
rizikové	rizikový	k2eAgInPc4d1	rizikový
faktory	faktor	k1gInPc4	faktor
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
by	by	kYmCp3nS	by
být	být	k5eAaImF	být
indikace	indikace	k1gFnPc4	indikace
k	k	k7c3	k
bariatrickému	bariatrický	k2eAgInSc3d1	bariatrický
výkonu	výkon	k1gInSc3	výkon
u	u	k7c2	u
obézního	obézní	k2eAgMnSc2d1	obézní
nemocného	nemocný	k1gMnSc2	nemocný
s	s	k7c7	s
diabetem	diabetes	k1gInSc7	diabetes
2	[number]	k4	2
<g/>
.	.	kIx.	.
typu	typ	k1gInSc2	typ
vždy	vždy	k6eAd1	vždy
uvážena	uvážen	k2eAgFnSc1d1	uvážen
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
akutní	akutní	k2eAgFnPc4d1	akutní
komplikace	komplikace	k1gFnPc4	komplikace
diabetu	diabetes	k1gInSc2	diabetes
(	(	kIx(	(
<g/>
AKD	AKD	kA	AKD
<g/>
)	)	kIx)	)
řadíme	řadit	k5eAaImIp1nP	řadit
hypoglykemii	hypoglykemie	k1gFnSc4	hypoglykemie
<g/>
,	,	kIx,	,
hyperglykemii	hyperglykemie	k1gFnSc4	hyperglykemie
a	a	k8xC	a
s	s	k7c7	s
hyperglykemií	hyperglykemie	k1gFnSc7	hyperglykemie
spojenou	spojený	k2eAgFnSc4d1	spojená
diabetickou	diabetický	k2eAgFnSc4d1	diabetická
ketoacidózu	ketoacidóza	k1gFnSc4	ketoacidóza
<g/>
.	.	kIx.	.
</s>
<s>
AKD	AKD	kA	AKD
lehčího	lehký	k2eAgInSc2d2	lehčí
rázu	ráz	k1gInSc2	ráz
prožívá	prožívat	k5eAaImIp3nS	prožívat
každý	každý	k3xTgMnSc1	každý
diabetik	diabetik	k1gMnSc1	diabetik
relativně	relativně	k6eAd1	relativně
často	často	k6eAd1	často
a	a	k8xC	a
nelze	lze	k6eNd1	lze
je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
eliminovat	eliminovat	k5eAaBmF	eliminovat
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
léčby	léčba	k1gFnSc2	léčba
DM	dm	kA	dm
1	[number]	k4	1
je	být	k5eAaImIp3nS	být
předcházet	předcházet	k5eAaImF	předcházet
AKD	AKD	kA	AKD
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
možností	možnost	k1gFnPc2	možnost
každého	každý	k3xTgMnSc2	každý
pacienta	pacient	k1gMnSc2	pacient
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
důležité	důležitý	k2eAgNnSc1d1	důležité
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
každý	každý	k3xTgMnSc1	každý
diabetik	diabetik	k1gMnSc1	diabetik
dovedl	dovést	k5eAaPmAgMnS	dovést
AKD	AKD	kA	AKD
rychle	rychle	k6eAd1	rychle
a	a	k8xC	a
účinně	účinně	k6eAd1	účinně
řešit	řešit	k5eAaImF	řešit
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
ho	on	k3xPp3gInSc4	on
naučí	naučit	k5eAaPmIp3nS	naučit
kvalitní	kvalitní	k2eAgFnSc1d1	kvalitní
edukace	edukace	k1gFnSc1	edukace
<g/>
.	.	kIx.	.
</s>
<s>
Hypoglykémie	hypoglykémie	k1gFnSc1	hypoglykémie
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
nízká	nízký	k2eAgFnSc1d1	nízká
hladina	hladina	k1gFnSc1	hladina
glukózy	glukóza	k1gFnSc2	glukóza
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
těžká	těžký	k2eAgFnSc1d1	těžká
a	a	k8xC	a
lehká	lehký	k2eAgFnSc1d1	lehká
<g/>
.	.	kIx.	.
lehká	lehký	k2eAgFnSc1d1	lehká
hypoglykémie	hypoglykémie	k1gFnSc1	hypoglykémie
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
diabetik	diabetik	k1gMnSc1	diabetik
má	mít	k5eAaImIp3nS	mít
hladinu	hladina	k1gFnSc4	hladina
krevního	krevní	k2eAgInSc2d1	krevní
"	"	kIx"	"
<g/>
cukru	cukr	k1gInSc2	cukr
<g/>
"	"	kIx"	"
pod	pod	k7c7	pod
hladinou	hladina	k1gFnSc7	hladina
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
si	se	k3xPyFc3	se
předem	předem	k6eAd1	předem
určí	určit	k5eAaPmIp3nS	určit
(	(	kIx(	(
<g/>
jde	jít	k5eAaImIp3nS	jít
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
někdo	někdo	k3yInSc1	někdo
se	se	k3xPyFc4	se
cítí	cítit	k5eAaImIp3nS	cítit
špatně	špatně	k6eAd1	špatně
při	při	k7c6	při
5	[number]	k4	5
mmol	mmola	k1gFnPc2	mmola
<g/>
/	/	kIx~	/
<g/>
l	l	kA	l
a	a	k8xC	a
druhý	druhý	k4xOgInSc1	druhý
zase	zase	k9	zase
až	až	k9	až
při	při	k7c6	při
3	[number]	k4	3
mmol	mmola	k1gFnPc2	mmola
<g/>
/	/	kIx~	/
<g/>
l	l	kA	l
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
se	se	k3xPyFc4	se
nají	najíst	k5eAaPmIp3nS	najíst
nějakého	nějaký	k3yIgInSc2	nějaký
jídla-cukru	jídlaukr	k1gInSc2	jídla-cukr
(	(	kIx(	(
<g/>
oficiálně	oficiálně	k6eAd1	oficiálně
má	mít	k5eAaImIp3nS	mít
diabetik	diabetik	k1gMnSc1	diabetik
hypoglykemii	hypoglykemie	k1gFnSc4	hypoglykemie
od	od	k7c2	od
3,3	[number]	k4	3,3
mmol	mmola	k1gFnPc2	mmola
<g/>
/	/	kIx~	/
<g/>
l	l	kA	l
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
už	už	k6eAd1	už
je	být	k5eAaImIp3nS	být
diabetik	diabetik	k1gMnSc1	diabetik
v	v	k7c6	v
bezvědomí	bezvědomí	k1gNnSc6	bezvědomí
,	,	kIx,	,
nazýváme	nazývat	k5eAaImIp1nP	nazývat
to	ten	k3xDgNnSc1	ten
těžká	těžký	k2eAgFnSc1d1	těžká
hypoglykémie	hypoglykémie	k1gFnSc1	hypoglykémie
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
diabetik	diabetik	k1gMnSc1	diabetik
nenají	najíst	k5eNaPmIp3nS	najíst
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
při	při	k7c6	při
nadměrném	nadměrný	k2eAgInSc6d1	nadměrný
pohybu	pohyb	k1gInSc6	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
diabetici	diabetik	k1gMnPc1	diabetik
před	před	k7c7	před
sportem	sport	k1gInSc7	sport
měřili	měřit	k5eAaImAgMnP	měřit
a	a	k8xC	a
dojídali	dojídat	k5eAaImAgMnP	dojídat
<g/>
.	.	kIx.	.
</s>
<s>
Hypoglykémie	hypoglykémie	k1gFnSc1	hypoglykémie
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
nebezpečnější	bezpečný	k2eNgFnSc1d2	nebezpečnější
než	než	k8xS	než
hyperglykemie	hyperglykemie	k1gFnSc1	hyperglykemie
<g/>
.	.	kIx.	.
</s>
<s>
Hypoglykemie	hypoglykemie	k1gFnSc1	hypoglykemie
je	být	k5eAaImIp3nS	být
také	také	k9	také
mnohem	mnohem	k6eAd1	mnohem
častější	častý	k2eAgNnSc1d2	častější
a	a	k8xC	a
rychlejší	rychlý	k2eAgMnSc1d2	rychlejší
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
účelem	účel	k1gInSc7	účel
léčby	léčba	k1gFnSc2	léčba
diabetu	diabetes	k1gInSc2	diabetes
je	být	k5eAaImIp3nS	být
předcházet	předcházet	k5eAaImF	předcházet
pozdním	pozdní	k2eAgFnPc3d1	pozdní
komplikacím	komplikace	k1gFnPc3	komplikace
diabetu	diabetes	k1gInSc2	diabetes
(	(	kIx(	(
<g/>
PKD	PKD	kA	PKD
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
docílení	docílení	k1gNnSc4	docílení
stejné	stejný	k2eAgFnSc2d1	stejná
kvality	kvalita	k1gFnSc2	kvalita
života	život	k1gInSc2	život
jako	jako	k8xC	jako
u	u	k7c2	u
nediabetické	diabetický	k2eNgFnSc2d1	diabetický
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
PKD	PKD	kA	PKD
podporuje	podporovat	k5eAaImIp3nS	podporovat
dlouhotrvající	dlouhotrvající	k2eAgFnSc1d1	dlouhotrvající
hyperglykemie	hyperglykemie	k1gFnSc1	hyperglykemie
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
genetickými	genetický	k2eAgFnPc7d1	genetická
predispozicemi	predispozice	k1gFnPc7	predispozice
<g/>
.	.	kIx.	.
</s>
<s>
Hyperglykemie	hyperglykemie	k1gFnSc1	hyperglykemie
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
tělo	tělo	k1gNnSc4	tělo
nepřirozená	přirozený	k2eNgFnSc1d1	nepřirozená
a	a	k8xC	a
tělesné	tělesný	k2eAgFnSc2d1	tělesná
tkáně	tkáň	k1gFnSc2	tkáň
nejsou	být	k5eNaImIp3nP	být
na	na	k7c4	na
vysoké	vysoký	k2eAgFnPc4d1	vysoká
koncentrace	koncentrace	k1gFnPc4	koncentrace
glukózy	glukóza	k1gFnSc2	glukóza
"	"	kIx"	"
<g/>
zvyklé	zvyklý	k2eAgFnPc1d1	zvyklá
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Glukóza	glukóza	k1gFnSc1	glukóza
je	být	k5eAaImIp3nS	být
za	za	k7c2	za
určitých	určitý	k2eAgFnPc2d1	určitá
podmínek	podmínka	k1gFnPc2	podmínka
velmi	velmi	k6eAd1	velmi
reaktivní	reaktivní	k2eAgInSc1d1	reaktivní
a	a	k8xC	a
snadno	snadno	k6eAd1	snadno
nekontrolovaně	kontrolovaně	k6eNd1	kontrolovaně
reaguje	reagovat	k5eAaBmIp3nS	reagovat
s	s	k7c7	s
molekulami	molekula	k1gFnPc7	molekula
bílkovin	bílkovina	k1gFnPc2	bílkovina
(	(	kIx(	(
<g/>
proces	proces	k1gInSc1	proces
neenzymové	enzymový	k2eNgFnSc2d1	enzymový
glykace	glykace	k1gFnSc2	glykace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Glykace	Glykace	k1gFnSc1	Glykace
dané	daný	k2eAgFnSc2d1	daná
bílkoviny	bílkovina	k1gFnSc2	bílkovina
změní	změnit	k5eAaPmIp3nS	změnit
její	její	k3xOp3gFnPc4	její
chemické	chemický	k2eAgFnPc4d1	chemická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
bílkovina	bílkovina	k1gFnSc1	bílkovina
nemůže	moct	k5eNaImIp3nS	moct
již	již	k6eAd1	již
plnit	plnit	k5eAaImF	plnit
svou	svůj	k3xOyFgFnSc4	svůj
původní	původní	k2eAgFnSc4d1	původní
funkci	funkce	k1gFnSc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Čím	co	k3yInSc7	co
častější	častý	k2eAgMnSc1d2	častější
a	a	k8xC	a
déletrvající	déletrvající	k2eAgMnSc1d1	déletrvající
jsou	být	k5eAaImIp3nP	být
hyperglykemie	hyperglykemie	k1gFnSc1	hyperglykemie
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
více	hodně	k6eAd2	hodně
proteinů	protein	k1gInPc2	protein
je	být	k5eAaImIp3nS	být
naglykováno	naglykován	k2eAgNnSc1d1	naglykován
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
při	při	k7c6	při
překročení	překročení	k1gNnSc6	překročení
určité	určitý	k2eAgFnSc2d1	určitá
kritické	kritický	k2eAgFnSc2d1	kritická
hodnoty	hodnota	k1gFnSc2	hodnota
naglykovaných	naglykovaný	k2eAgFnPc2d1	naglykovaný
bílkovin	bílkovina	k1gFnPc2	bílkovina
znamená	znamenat	k5eAaImIp3nS	znamenat
výrazné	výrazný	k2eAgNnSc4d1	výrazné
zvýšení	zvýšení	k1gNnSc4	zvýšení
rizika	riziko	k1gNnSc2	riziko
vzniku	vznik	k1gInSc2	vznik
nějaké	nějaký	k3yIgFnSc2	nějaký
konkrétní	konkrétní	k2eAgFnSc2d1	konkrétní
pozdní	pozdní	k2eAgFnSc2d1	pozdní
komplikace	komplikace	k1gFnSc2	komplikace
diabetu	diabetes	k1gInSc2	diabetes
<g/>
.	.	kIx.	.
</s>
<s>
Hyperglykemie	hyperglykemie	k1gFnSc1	hyperglykemie
také	také	k9	také
podporuje	podporovat	k5eAaImIp3nS	podporovat
oxidační	oxidační	k2eAgInSc4d1	oxidační
stres	stres	k1gInSc4	stres
organismu	organismus	k1gInSc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Hydroxylové	hydroxylový	k2eAgFnPc1d1	hydroxylová
skupiny	skupina	k1gFnPc1	skupina
glukózy	glukóza	k1gFnSc2	glukóza
se	se	k3xPyFc4	se
účastní	účastnit	k5eAaImIp3nS	účastnit
přenosu	přenos	k1gInSc2	přenos
elektronů	elektron	k1gInPc2	elektron
<g/>
,	,	kIx,	,
tím	ten	k3xDgInSc7	ten
podporují	podporovat	k5eAaImIp3nP	podporovat
vznik	vznik	k1gInSc4	vznik
reaktivních	reaktivní	k2eAgMnPc2d1	reaktivní
radikálů	radikál	k1gMnPc2	radikál
(	(	kIx(	(
<g/>
především	především	k9	především
reaktivní	reaktivní	k2eAgFnPc1d1	reaktivní
formy	forma	k1gFnPc1	forma
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
přímo	přímo	k6eAd1	přímo
reagují	reagovat	k5eAaBmIp3nP	reagovat
s	s	k7c7	s
buňkami	buňka	k1gFnPc7	buňka
a	a	k8xC	a
mění	měnit	k5eAaImIp3nP	měnit
jejich	jejich	k3xOp3gFnPc4	jejich
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Projev	projev	k1gInSc1	projev
oxidačního	oxidační	k2eAgInSc2d1	oxidační
stresu	stres	k1gInSc2	stres
můžeme	moct	k5eAaImIp1nP	moct
pozorovat	pozorovat	k5eAaImF	pozorovat
např.	např.	kA	např.
u	u	k7c2	u
procesu	proces	k1gInSc2	proces
geneze	geneze	k1gFnSc2	geneze
aterosklerotických	aterosklerotický	k2eAgInPc2d1	aterosklerotický
plátů	plát	k1gInPc2	plát
velkých	velký	k2eAgFnPc2d1	velká
cév	céva	k1gFnPc2	céva
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
relativně	relativně	k6eAd1	relativně
časté	častý	k2eAgNnSc1d1	časté
diabetické	diabetický	k2eAgNnSc1d1	diabetické
onemocnění	onemocnění	k1gNnSc1	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Oxidační	oxidační	k2eAgInSc1d1	oxidační
stres	stres	k1gInSc1	stres
stupňuje	stupňovat	k5eAaImIp3nS	stupňovat
rychlost	rychlost	k1gFnSc4	rychlost
glykace	glykace	k1gFnSc2	glykace
bílkovin	bílkovina	k1gFnPc2	bílkovina
a	a	k8xC	a
naglykované	naglykovaný	k2eAgFnPc1d1	naglykovaný
bílkoviny	bílkovina	k1gFnPc1	bílkovina
zase	zase	k9	zase
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
reaktivní	reaktivní	k2eAgInPc1d1	reaktivní
radikály	radikál	k1gInPc1	radikál
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
vzniká	vznikat	k5eAaImIp3nS	vznikat
"	"	kIx"	"
<g/>
začarovaný	začarovaný	k2eAgInSc1d1	začarovaný
kruh	kruh	k1gInSc1	kruh
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
PKD	PKD	kA	PKD
je	být	k5eAaImIp3nS	být
poškození	poškození	k1gNnSc1	poškození
povrchu	povrch	k1gInSc2	povrch
malých	malý	k2eAgFnPc2d1	malá
cév	céva	k1gFnPc2	céva
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stávají	stávat	k5eAaImIp3nP	stávat
náchylné	náchylný	k2eAgFnPc1d1	náchylná
k	k	k7c3	k
prasknutí	prasknutí	k1gNnSc3	prasknutí
<g/>
.	.	kIx.	.
</s>
<s>
Popraskání	popraskání	k1gNnSc1	popraskání
určitých	určitý	k2eAgFnPc2d1	určitá
cév	céva	k1gFnPc2	céva
v	v	k7c6	v
určitých	určitý	k2eAgInPc6d1	určitý
orgánech	orgán	k1gInPc6	orgán
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
jako	jako	k9	jako
pozdní	pozdní	k2eAgFnPc4d1	pozdní
komplikace	komplikace	k1gFnPc4	komplikace
diabetu	diabetes	k1gInSc2	diabetes
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
něž	jenž	k3xRgNnSc4	jenž
řadíme	řadit	k5eAaImIp1nP	řadit
onemocnění	onemocnění	k1gNnPc1	onemocnění
jako	jako	k8xC	jako
diabetická	diabetický	k2eAgFnSc1d1	diabetická
nefropatie	nefropatie	k1gFnSc1	nefropatie
(	(	kIx(	(
<g/>
poškození	poškození	k1gNnSc1	poškození
cév	céva	k1gFnPc2	céva
obalujících	obalující	k2eAgFnPc2d1	obalující
glomeruly	glomerul	k1gInPc4	glomerul
ledvin	ledvina	k1gFnPc2	ledvina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
diabetická	diabetický	k2eAgFnSc1d1	diabetická
retinopatie	retinopatie	k1gFnSc1	retinopatie
(	(	kIx(	(
<g/>
poškození	poškození	k1gNnSc1	poškození
cév	céva	k1gFnPc2	céva
vyživujících	vyživující	k2eAgFnPc2d1	vyživující
sítnici	sítnice	k1gFnSc4	sítnice
oka	oko	k1gNnSc2	oko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
diabetická	diabetický	k2eAgFnSc1d1	diabetická
neuropatie	neuropatie	k1gFnSc1	neuropatie
(	(	kIx(	(
<g/>
poškození	poškození	k1gNnSc3	poškození
funkce	funkce	k1gFnSc2	funkce
všech	všecek	k3xTgInPc2	všecek
druhů	druh	k1gInPc2	druh
nervů	nerv	k1gInPc2	nerv
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Diabetická	diabetický	k2eAgFnSc1d1	diabetická
neuropatie	neuropatie	k1gFnSc1	neuropatie
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
poruchy	porucha	k1gFnPc4	porucha
vnímání	vnímání	k1gNnSc2	vnímání
bolesti	bolest	k1gFnSc2	bolest
dolních	dolní	k2eAgFnPc2d1	dolní
končetin	končetina	k1gFnPc2	končetina
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
drobný	drobný	k2eAgInSc4d1	drobný
úraz	úraz	k1gInSc4	úraz
dolní	dolní	k2eAgFnSc2d1	dolní
končetiny	končetina	k1gFnSc2	končetina
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
není	být	k5eNaImIp3nS	být
brzy	brzy	k6eAd1	brzy
rozpoznán	rozpoznat	k5eAaPmNgInS	rozpoznat
a	a	k8xC	a
léčen	léčit	k5eAaImNgInS	léčit
diabetikem	diabetik	k1gMnSc7	diabetik
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
gangrény	gangréna	k1gFnSc2	gangréna
<g/>
,	,	kIx,	,
v	v	k7c6	v
nejhorším	zlý	k2eAgInSc6d3	Nejhorší
případě	případ	k1gInSc6	případ
i	i	k9	i
k	k	k7c3	k
amputaci	amputace	k1gFnSc3	amputace
dolní	dolní	k2eAgFnSc2d1	dolní
končetiny	končetina	k1gFnSc2	končetina
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
PKD	PKD	kA	PKD
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
syndrom	syndrom	k1gInSc1	syndrom
diabetické	diabetický	k2eAgFnSc2d1	diabetická
nohy	noha	k1gFnSc2	noha
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
pozdní	pozdní	k2eAgFnPc4d1	pozdní
komplikace	komplikace	k1gFnPc4	komplikace
diabetu	diabetes	k1gInSc2	diabetes
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
diabetická	diabetický	k2eAgFnSc1d1	diabetická
nefropatie	nefropatie	k1gFnSc1	nefropatie
diabetická	diabetický	k2eAgFnSc1d1	diabetická
retinopatie	retinopatie	k1gFnSc2	retinopatie
diabetická	diabetický	k2eAgFnSc1d1	diabetická
neuropatie	neuropatie	k1gFnSc1	neuropatie
diabetická	diabetický	k2eAgFnSc1d1	diabetická
makroangiopatie	makroangiopatie	k1gFnSc2	makroangiopatie
diabetická	diabetický	k2eAgFnSc1d1	diabetická
noha	noha	k1gFnSc1	noha
Onemocnění	onemocnění	k1gNnSc2	onemocnění
diabetu	diabetes	k1gInSc2	diabetes
I.	I.	kA	I.
typu	typ	k1gInSc2	typ
se	se	k3xPyFc4	se
nedědí	dědit	k5eNaImIp3nS	dědit
přímo	přímo	k6eAd1	přímo
<g/>
.	.	kIx.	.
</s>
<s>
Dědí	dědit	k5eAaImIp3nS	dědit
se	se	k3xPyFc4	se
jen	jen	k9	jen
určité	určitý	k2eAgFnPc4d1	určitá
vlohy	vloha	k1gFnPc4	vloha
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
při	při	k7c6	při
působení	působení	k1gNnSc6	působení
dalších	další	k2eAgInPc2d1	další
faktorů	faktor	k1gInPc2	faktor
vedou	vést	k5eAaImIp3nP	vést
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
této	tento	k3xDgFnSc2	tento
choroby	choroba	k1gFnSc2	choroba
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
statisticky	statisticky	k6eAd1	statisticky
dokázáno	dokázat	k5eAaPmNgNnS	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
sourozenci	sourozenec	k1gMnPc1	sourozenec
diabetiků	diabetik	k1gMnPc2	diabetik
I.	I.	kA	I.
typu	typ	k1gInSc6	typ
jsou	být	k5eAaImIp3nP	být
s	s	k7c7	s
10	[number]	k4	10
<g/>
%	%	kIx~	%
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
diabetici	diabetik	k1gMnPc1	diabetik
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
získání	získání	k1gNnSc2	získání
diabetu	diabetes	k1gInSc2	diabetes
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
jednovaječných	jednovaječný	k2eAgNnPc2d1	jednovaječné
dvojčat	dvojče	k1gNnPc2	dvojče
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
jedno	jeden	k4xCgNnSc1	jeden
už	už	k6eAd1	už
diabetem	diabetes	k1gInSc7	diabetes
trpí	trpět	k5eAaImIp3nS	trpět
(	(	kIx(	(
<g/>
shodná	shodný	k2eAgFnSc1d1	shodná
genetická	genetický	k2eAgFnSc1d1	genetická
informace	informace	k1gFnSc1	informace
u	u	k7c2	u
obou	dva	k4xCgNnPc2	dva
dvojčat	dvojče	k1gNnPc2	dvojče
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Děti	dítě	k1gFnPc1	dítě
obou	dva	k4xCgMnPc2	dva
diabetických	diabetický	k2eAgMnPc2d1	diabetický
rodičů	rodič	k1gMnPc2	rodič
mají	mít	k5eAaImIp3nP	mít
cca	cca	kA	cca
50	[number]	k4	50
<g/>
%	%	kIx~	%
pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
propuknutí	propuknutí	k1gNnSc2	propuknutí
této	tento	k3xDgFnSc2	tento
choroby	choroba	k1gFnSc2	choroba
během	během	k7c2	během
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
diabetu	diabetes	k1gInSc2	diabetes
II	II	kA	II
<g/>
.	.	kIx.	.
typu	typ	k1gInSc2	typ
jsou	být	k5eAaImIp3nP	být
dědičné	dědičný	k2eAgInPc1d1	dědičný
sklony	sklon	k1gInPc1	sklon
mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgInPc1d2	veliký
a	a	k8xC	a
příbuzných	příbuzný	k1gMnPc2	příbuzný
s	s	k7c7	s
cukrovkou	cukrovka	k1gFnSc7	cukrovka
přibývá	přibývat	k5eAaImIp3nS	přibývat
úměrně	úměrně	k6eAd1	úměrně
s	s	k7c7	s
věkem	věk	k1gInSc7	věk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
přes	přes	k7c4	přes
770	[number]	k4	770
tisíc	tisíc	k4xCgInPc2	tisíc
diabetiků	diabetik	k1gMnPc2	diabetik
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
přibližně	přibližně	k6eAd1	přibližně
55	[number]	k4	55
400	[number]	k4	400
diabetiků	diabetik	k1gMnPc2	diabetik
1	[number]	k4	1
<g/>
.	.	kIx.	.
typu	typ	k1gInSc2	typ
a	a	k8xC	a
717	[number]	k4	717
300	[number]	k4	300
diabetiků	diabetik	k1gMnPc2	diabetik
2	[number]	k4	2
<g/>
.	.	kIx.	.
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
je	být	k5eAaImIp3nS	být
cca	cca	kA	cca
215	[number]	k4	215
milionů	milion	k4xCgInPc2	milion
diabetiků	diabetik	k1gMnPc2	diabetik
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
množství	množství	k1gNnSc2	množství
připadá	připadat	k5eAaImIp3nS	připadat
pouze	pouze	k6eAd1	pouze
7,5	[number]	k4	7,5
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
%	%	kIx~	%
na	na	k7c4	na
diabetiky	diabetik	k1gMnPc4	diabetik
I.	I.	kA	I.
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Incidence	incidence	k1gFnSc1	incidence
i	i	k8xC	i
prevalance	prevalance	k1gFnSc1	prevalance
diabetu	diabetes	k1gInSc2	diabetes
celosvětově	celosvětově	k6eAd1	celosvětově
roste	růst	k5eAaImIp3nS	růst
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
zřejmě	zřejmě	k6eAd1	zřejmě
důsledkem	důsledek	k1gInSc7	důsledek
neustálého	neustálý	k2eAgNnSc2d1	neustálé
zlepšování	zlepšování	k1gNnSc2	zlepšování
zdravotní	zdravotní	k2eAgFnSc2d1	zdravotní
péče	péče	k1gFnSc2	péče
(	(	kIx(	(
<g/>
většina	většina	k1gFnSc1	většina
diabetiků	diabetik	k1gMnPc2	diabetik
se	se	k3xPyFc4	se
dožije	dožít	k5eAaPmIp3nS	dožít
reprodukčního	reprodukční	k2eAgInSc2d1	reprodukční
věku	věk	k1gInSc2	věk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
znečišťování	znečišťování	k1gNnSc4	znečišťování
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
změny	změna	k1gFnSc2	změna
způsobu	způsob	k1gInSc2	způsob
života	život	k1gInSc2	život
apod.	apod.	kA	apod.
Diabetický	diabetický	k2eAgInSc4d1	diabetický
syndrom	syndrom	k1gInSc4	syndrom
nebo	nebo	k8xC	nebo
porušenou	porušený	k2eAgFnSc4d1	porušená
glukózovou	glukózový	k2eAgFnSc4d1	glukózová
toleranci	tolerance	k1gFnSc4	tolerance
lze	lze	k6eAd1	lze
v	v	k7c6	v
rozvinutých	rozvinutý	k2eAgFnPc6d1	rozvinutá
zemích	zem	k1gFnPc6	zem
prokázat	prokázat	k5eAaPmF	prokázat
u	u	k7c2	u
5-6	[number]	k4	5-6
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíly	rozdíl	k1gInPc1	rozdíl
mezi	mezi	k7c7	mezi
populacemi	populace	k1gFnPc7	populace
v	v	k7c6	v
incidenci	incidence	k1gFnSc6	incidence
diabetu	diabetes	k1gInSc2	diabetes
jsou	být	k5eAaImIp3nP	být
zcela	zcela	k6eAd1	zcela
mimořádné	mimořádný	k2eAgFnSc2d1	mimořádná
<g/>
,	,	kIx,	,
např.	např.	kA	např.
nejnižší	nízký	k2eAgInSc1d3	nejnižší
výskyt	výskyt	k1gInSc1	výskyt
diabetu	diabetes	k1gInSc2	diabetes
u	u	k7c2	u
dětí	dítě	k1gFnPc2	dítě
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
(	(	kIx(	(
<g/>
pod	pod	k7c4	pod
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
100	[number]	k4	100
000	[number]	k4	000
osob	osoba	k1gFnPc2	osoba
<g/>
/	/	kIx~	/
<g/>
rok	rok	k1gInSc4	rok
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
pak	pak	k6eAd1	pak
ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
však	však	k9	však
najít	najít	k5eAaPmF	najít
rozdíly	rozdíl	k1gInPc4	rozdíl
i	i	k9	i
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
zemí	zem	k1gFnPc2	zem
a	a	k8xC	a
oblastí	oblast	k1gFnPc2	oblast
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
sociálním	sociální	k2eAgInSc6d1	sociální
statutu	statut	k1gInSc6	statut
a	a	k8xC	a
dalších	další	k2eAgInPc6d1	další
faktorech	faktor	k1gInPc6	faktor
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Pocket	Pocketa	k1gFnPc2	Pocketa
World	Worlda	k1gFnPc2	Worlda
in	in	k?	in
Figures	Figures	k1gInSc1	Figures
2008	[number]	k4	2008
vydaného	vydaný	k2eAgInSc2d1	vydaný
deníkem	deník	k1gInSc7	deník
The	The	k1gMnSc1	The
Economist	Economist	k1gMnSc1	Economist
<g/>
,	,	kIx,	,
zasáhl	zasáhnout	k5eAaPmAgInS	zasáhnout
diabetes	diabetes	k1gInSc1	diabetes
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2007	[number]	k4	2007
následující	následující	k2eAgInPc1d1	následující
podíly	podíl	k1gInPc1	podíl
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
států	stát	k1gInPc2	stát
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
20-79	[number]	k4	20-79
let	léto	k1gNnPc2	léto
<g/>
:	:	kIx,	:
Cílem	cíl	k1gInSc7	cíl
výzkumu	výzkum	k1gInSc2	výzkum
je	být	k5eAaImIp3nS	být
buď	buď	k8xC	buď
bezriziková	bezrizikový	k2eAgFnSc1d1	bezriziková
transplantace	transplantace	k1gFnSc1	transplantace
slinivky	slinivka	k1gFnSc2	slinivka
<g/>
,	,	kIx,	,
Langerhansových	Langerhansův	k2eAgInPc2d1	Langerhansův
ostrůvků	ostrůvek	k1gInPc2	ostrůvek
nebo	nebo	k8xC	nebo
jen	jen	k9	jen
samotných	samotný	k2eAgInPc2d1	samotný
β	β	k?	β
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
lze	lze	k6eAd1	lze
vyšlechtit	vyšlechtit	k5eAaPmF	vyšlechtit
z	z	k7c2	z
embryonálních	embryonální	k2eAgFnPc2d1	embryonální
kmenových	kmenový	k2eAgFnPc2d1	kmenová
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
zkonstruování	zkonstruování	k1gNnSc4	zkonstruování
a	a	k8xC	a
miniaturizaci	miniaturizace	k1gFnSc4	miniaturizace
stroje	stroj	k1gInSc2	stroj
-	-	kIx~	-
umělé	umělý	k2eAgFnSc2d1	umělá
B	B	kA	B
buňky	buňka	k1gFnSc2	buňka
-	-	kIx~	-
která	který	k3yQgFnSc1	který
by	by	kYmCp3nS	by
napodobovala	napodobovat	k5eAaImAgFnS	napodobovat
všechny	všechen	k3xTgFnPc4	všechen
funkce	funkce	k1gFnPc4	funkce
B	B	kA	B
buňky	buňka	k1gFnSc2	buňka
zdravého	zdravý	k2eAgInSc2d1	zdravý
organismu	organismus	k1gInSc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Úsilí	úsilí	k1gNnSc1	úsilí
výzkumu	výzkum	k1gInSc2	výzkum
však	však	k9	však
také	také	k9	také
směřuje	směřovat	k5eAaImIp3nS	směřovat
ke	k	k7c3	k
zdokonalení	zdokonalení	k1gNnSc3	zdokonalení
a	a	k8xC	a
zefektivnění	zefektivnění	k1gNnSc3	zefektivnění
současné	současný	k2eAgFnSc2d1	současná
konvenční	konvenční	k2eAgFnSc2d1	konvenční
léčby	léčba	k1gFnSc2	léčba
diabetu	diabetes	k1gInSc2	diabetes
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
transplantace	transplantace	k1gFnSc2	transplantace
je	být	k5eAaImIp3nS	být
zabezpečit	zabezpečit	k5eAaPmF	zabezpečit
aktuální	aktuální	k2eAgFnSc4d1	aktuální
potřebu	potřeba	k1gFnSc4	potřeba
inzulinu	inzulin	k1gInSc2	inzulin
organismu	organismus	k1gInSc2	organismus
bez	bez	k7c2	bez
nutnosti	nutnost	k1gFnSc2	nutnost
exogenní	exogenní	k2eAgFnSc2d1	exogenní
aplikace	aplikace	k1gFnSc2	aplikace
inzulinu	inzulin	k1gInSc2	inzulin
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
docílit	docílit	k5eAaPmF	docílit
trvalé	trvalý	k2eAgFnPc1d1	trvalá
normoglykemie	normoglykemie	k1gFnPc1	normoglykemie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
trvalé	trvalý	k2eAgFnSc2d1	trvalá
normoglykemie	normoglykemie	k1gFnSc2	normoglykemie
se	se	k3xPyFc4	se
dostává	dostávat	k5eAaImIp3nS	dostávat
hodnota	hodnota	k1gFnSc1	hodnota
glykovaného	glykovaný	k2eAgInSc2d1	glykovaný
hemoglobinu	hemoglobin	k1gInSc2	hemoglobin
do	do	k7c2	do
normálu	normál	k1gInSc2	normál
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
lze	lze	k6eAd1	lze
absolutně	absolutně	k6eAd1	absolutně
předcházet	předcházet	k5eAaImF	předcházet
pozdním	pozdní	k2eAgFnPc3d1	pozdní
komplikacím	komplikace	k1gFnPc3	komplikace
diabetu	diabetes	k1gInSc2	diabetes
<g/>
.	.	kIx.	.
</s>
<s>
Dárce	dárce	k1gMnSc1	dárce
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
příjemci	příjemce	k1gMnSc3	příjemce
vybírán	vybírán	k2eAgMnSc1d1	vybírán
podle	podle	k7c2	podle
shod	shoda	k1gFnPc2	shoda
v	v	k7c6	v
transplantačních	transplantační	k2eAgInPc6d1	transplantační
antigenech	antigen	k1gInPc6	antigen
HLA	HLA	kA	HLA
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
absolutní	absolutní	k2eAgFnSc3d1	absolutní
HLA	HLA	kA	HLA
kompatibilitě	kompatibilita	k1gFnSc3	kompatibilita
dárce	dárce	k1gMnSc1	dárce
s	s	k7c7	s
příjemcem	příjemce	k1gMnSc7	příjemce
však	však	k8xC	však
nikdy	nikdy	k6eAd1	nikdy
nedojde	dojít	k5eNaPmIp3nS	dojít
<g/>
,	,	kIx,	,
tudíž	tudíž	k8xC	tudíž
štěp	štěp	k1gInSc1	štěp
je	být	k5eAaImIp3nS	být
imunitním	imunitní	k2eAgInSc7d1	imunitní
systémem	systém	k1gInSc7	systém
příjemce	příjemce	k1gMnSc2	příjemce
vždy	vždy	k6eAd1	vždy
různě	různě	k6eAd1	různě
rychle	rychle	k6eAd1	rychle
odhojován	odhojován	k2eAgInSc1d1	odhojován
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
transplantovaný	transplantovaný	k2eAgInSc1d1	transplantovaný
orgán	orgán	k1gInSc1	orgán
vydržel	vydržet	k5eAaPmAgInS	vydržet
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
funkční	funkční	k2eAgFnSc4d1	funkční
co	co	k9	co
nejdelší	dlouhý	k2eAgFnSc4d3	nejdelší
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
aktivita	aktivita	k1gFnSc1	aktivita
imunitního	imunitní	k2eAgInSc2d1	imunitní
systému	systém	k1gInSc2	systém
příjemce	příjemce	k1gMnSc2	příjemce
potlačena	potlačen	k2eAgFnSc1d1	potlačena
doživotní	doživotní	k2eAgFnSc7d1	doživotní
imunosupresivní	imunosupresivní	k2eAgFnSc7d1	imunosupresivní
léčbou	léčba	k1gFnSc7	léčba
<g/>
.	.	kIx.	.
</s>
<s>
Transplantace	transplantace	k1gFnSc1	transplantace
je	být	k5eAaImIp3nS	být
vhodná	vhodný	k2eAgFnSc1d1	vhodná
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
diabetika	diabetik	k1gMnSc4	diabetik
<g/>
,	,	kIx,	,
u	u	k7c2	u
něhož	jenž	k3xRgInSc2	jenž
prospěch	prospěch	k1gInSc4	prospěch
náhradních	náhradní	k2eAgInPc2d1	náhradní
orgánů	orgán	k1gInPc2	orgán
jasně	jasně	k6eAd1	jasně
převyšuje	převyšovat	k5eAaImIp3nS	převyšovat
případná	případný	k2eAgNnPc4d1	případné
rizika	riziko	k1gNnPc4	riziko
dlouhodobého	dlouhodobý	k2eAgNnSc2d1	dlouhodobé
podávání	podávání	k1gNnSc2	podávání
imunosupresiv	imunosupresit	k5eAaPmDgInS	imunosupresit
a	a	k8xC	a
operace	operace	k1gFnPc4	operace
<g/>
.	.	kIx.	.
</s>
<s>
Nejvhodnější	vhodný	k2eAgMnPc1d3	nejvhodnější
dárci	dárce	k1gMnPc1	dárce
jsou	být	k5eAaImIp3nP	být
příbuzní	příbuzný	k1gMnPc1	příbuzný
příjemce	příjemce	k1gMnSc1	příjemce
(	(	kIx(	(
<g/>
kvůli	kvůli	k7c3	kvůli
genetické	genetický	k2eAgFnSc3d1	genetická
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
imunitní	imunitní	k2eAgFnPc4d1	imunitní
podobnosti	podobnost	k1gFnPc4	podobnost
příjemců	příjemce	k1gMnPc2	příjemce
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc2	jejich
příbuzných	příbuzný	k1gMnPc2	příbuzný
<g/>
)	)	kIx)	)
a	a	k8xC	a
nejlépe	dobře	k6eAd3	dobře
příjemce	příjemce	k1gMnSc1	příjemce
sám	sám	k3xTgMnSc1	sám
sobě	se	k3xPyFc3	se
<g/>
.	.	kIx.	.
</s>
<s>
Samotná	samotný	k2eAgFnSc1d1	samotná
transplantace	transplantace	k1gFnSc1	transplantace
pankreatu	pankreas	k1gInSc2	pankreas
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
u	u	k7c2	u
zdravých	zdravý	k2eAgMnPc2d1	zdravý
diabetiků	diabetik	k1gMnPc2	diabetik
I.	I.	kA	I.
typu	typ	k1gInSc2	typ
neprovádí	provádět	k5eNaImIp3nS	provádět
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
léčba	léčba	k1gFnSc1	léčba
imunosupresivy	imunosupresiva	k1gFnSc2	imunosupresiva
je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgFnSc7d2	veliký
zátěží	zátěž	k1gFnSc7	zátěž
pro	pro	k7c4	pro
organismus	organismus	k1gInSc4	organismus
než	než	k8xS	než
samotná	samotný	k2eAgFnSc1d1	samotná
léčba	léčba	k1gFnSc1	léčba
inzulinem	inzulin	k1gInSc7	inzulin
<g/>
.	.	kIx.	.
</s>
<s>
Transplantace	transplantace	k1gFnSc1	transplantace
pankreatu	pankreas	k1gInSc2	pankreas
se	se	k3xPyFc4	se
často	často	k6eAd1	často
spojuje	spojovat	k5eAaImIp3nS	spojovat
s	s	k7c7	s
transplantací	transplantace	k1gFnSc7	transplantace
ledvin	ledvina	k1gFnPc2	ledvina
postižených	postižený	k2eAgFnPc2d1	postižená
diabetickou	diabetický	k2eAgFnSc7d1	diabetická
nefropatií	nefropatie	k1gFnSc7	nefropatie
<g/>
.	.	kIx.	.
</s>
<s>
Transplantace	transplantace	k1gFnSc1	transplantace
ledvin	ledvina	k1gFnPc2	ledvina
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
nutná	nutný	k2eAgFnSc1d1	nutná
pro	pro	k7c4	pro
přežití	přežití	k1gNnSc4	přežití
pacienta	pacient	k1gMnSc2	pacient
a	a	k8xC	a
následná	následný	k2eAgFnSc1d1	následná
léčba	léčba	k1gFnSc1	léčba
imunosupresivy	imunosupresiva	k1gFnSc2	imunosupresiva
má	mít	k5eAaImIp3nS	mít
proto	proto	k8xC	proto
na	na	k7c4	na
organismus	organismus	k1gInSc4	organismus
zanedbatelný	zanedbatelný	k2eAgInSc1d1	zanedbatelný
dopad	dopad	k1gInSc1	dopad
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
takovýchto	takovýto	k3xDgMnPc2	takovýto
diabetických	diabetický	k2eAgMnPc2d1	diabetický
pacientů	pacient	k1gMnPc2	pacient
se	se	k3xPyFc4	se
nabízí	nabízet	k5eAaImIp3nS	nabízet
spojení	spojení	k1gNnSc1	spojení
transplantace	transplantace	k1gFnSc2	transplantace
ledvin	ledvina	k1gFnPc2	ledvina
a	a	k8xC	a
slinivky	slinivka	k1gFnSc2	slinivka
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
transplantace	transplantace	k1gFnSc1	transplantace
je	být	k5eAaImIp3nS	být
vhodná	vhodný	k2eAgFnSc1d1	vhodná
většinou	většinou	k6eAd1	většinou
pro	pro	k7c4	pro
starší	starý	k2eAgMnPc4d2	starší
diabetiky	diabetik	k1gMnPc4	diabetik
II	II	kA	II
<g/>
.	.	kIx.	.
typu	typ	k1gInSc2	typ
postižené	postižený	k2eAgNnSc1d1	postižené
diabetickou	diabetický	k2eAgFnSc7d1	diabetická
nefropatií	nefropatie	k1gFnSc7	nefropatie
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
jsou	být	k5eAaImIp3nP	být
doživotně	doživotně	k6eAd1	doživotně
závislí	závislý	k2eAgMnPc1d1	závislý
na	na	k7c6	na
dialýze	dialýza	k1gFnSc6	dialýza
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
globální	globální	k2eAgNnSc4d1	globální
vyléčení	vyléčení	k1gNnSc4	vyléčení
diabetu	diabetes	k1gInSc2	diabetes
I.	I.	kA	I.
typu	typ	k1gInSc2	typ
nemá	mít	k5eNaImIp3nS	mít
transplantace	transplantace	k1gFnSc1	transplantace
pankreatu	pankreas	k1gInSc2	pankreas
perspektivu	perspektiv	k1gInSc2	perspektiv
<g/>
,	,	kIx,	,
nehledě	hledět	k5eNaImSgMnS	hledět
na	na	k7c4	na
finanční	finanční	k2eAgFnSc4d1	finanční
náročnost	náročnost	k1gFnSc4	náročnost
úkonu	úkon	k1gInSc2	úkon
<g/>
,	,	kIx,	,
nedostatek	nedostatek	k1gInSc1	nedostatek
dárců	dárce	k1gMnPc2	dárce
a	a	k8xC	a
relativně	relativně	k6eAd1	relativně
krátkou	krátký	k2eAgFnSc4d1	krátká
životnost	životnost	k1gFnSc4	životnost
štěpu	štěp	k1gInSc2	štěp
(	(	kIx(	(
<g/>
cca	cca	kA	cca
5	[number]	k4	5
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Inzulinová	inzulinový	k2eAgFnSc1d1	inzulinová
pumpa	pumpa	k1gFnSc1	pumpa
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
diabetes	diabetes	k1gInSc4	diabetes
mellitus	mellitus	k1gInSc1	mellitus
typu	typ	k1gInSc2	typ
1	[number]	k4	1
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
autoimunitní	autoimunitní	k2eAgNnSc4d1	autoimunitní
onemocnění	onemocnění	k1gNnSc4	onemocnění
<g/>
,	,	kIx,	,
zvažuje	zvažovat	k5eAaImIp3nS	zvažovat
se	se	k3xPyFc4	se
i	i	k9	i
použití	použití	k1gNnSc1	použití
červů	červ	k1gMnPc2	červ
k	k	k7c3	k
potlačení	potlačení	k1gNnSc3	potlačení
imunitní	imunitní	k2eAgFnSc2d1	imunitní
reakce	reakce	k1gFnSc2	reakce
vůči	vůči	k7c3	vůči
B-buňkám	Buňka	k1gFnPc3	B-buňka
v	v	k7c6	v
pankreasu	pankreas	k1gInSc6	pankreas
<g/>
.	.	kIx.	.
</s>
<s>
Takzvaná	takzvaný	k2eAgFnSc1d1	takzvaná
helmitická	helmitický	k2eAgFnSc1d1	helmitický
terapie	terapie	k1gFnSc1	terapie
byla	být	k5eAaImAgFnS	být
testována	testovat	k5eAaImNgFnS	testovat
na	na	k7c6	na
zvířecích	zvířecí	k2eAgInPc6d1	zvířecí
modelech	model	k1gInPc6	model
s	s	k7c7	s
živými	živý	k2eAgMnPc7d1	živý
červy	červ	k1gMnPc7	červ
nebo	nebo	k8xC	nebo
jen	jen	k6eAd1	jen
jejich	jejich	k3xOp3gInPc4	jejich
proteiny	protein	k1gInPc4	protein
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
těchto	tento	k3xDgFnPc2	tento
studií	studie	k1gFnPc2	studie
ukázala	ukázat	k5eAaPmAgFnS	ukázat
terapeutický	terapeutický	k2eAgInSc4d1	terapeutický
efekt	efekt	k1gInSc4	efekt
helmintů	helmint	k1gMnPc2	helmint
nebo	nebo	k8xC	nebo
jejich	jejich	k3xOp3gFnPc2	jejich
molekul	molekula	k1gFnPc2	molekula
na	na	k7c6	na
DM	dm	kA	dm
typu	typ	k1gInSc2	typ
I.	I.	kA	I.
</s>
