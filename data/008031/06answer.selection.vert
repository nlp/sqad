<s>
Léčí	léčit	k5eAaImIp3nP
se	se	k3xPyFc4
perorálními	perorální	k2eAgMnPc7d1
antidiabetiky	antidiabetik	k1gMnPc7
<g/>
,	,	kIx,
což	což	k3yQnSc1
jsou	být	k5eAaImIp3nP
ústy	ústa	k1gNnPc7
podávané	podávaný	k2eAgInPc4d1
léky	lék	k1gInPc7
<g/>
,	,	kIx,
které	který	k3yIgInPc1
zvyšují	zvyšovat	k5eAaImIp3nP
citlivost	citlivost	k1gFnSc4
k	k	k7c3
inzulinu	inzulin	k1gInSc3
<g/>
.	.	kIx.
</s>