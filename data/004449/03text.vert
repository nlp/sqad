<s>
Exekutorská	exekutorský	k2eAgFnSc1d1	Exekutorská
komora	komora	k1gFnSc1	komora
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
je	být	k5eAaImIp3nS	být
stavovskou	stavovský	k2eAgFnSc7d1	stavovská
organizací	organizace	k1gFnSc7	organizace
profesní	profesní	k2eAgFnSc2d1	profesní
samosprávy	samospráva	k1gFnSc2	samospráva
sdružující	sdružující	k2eAgMnPc4d1	sdružující
soudní	soudní	k2eAgMnPc4d1	soudní
exekutory	exekutor	k1gMnPc4	exekutor
<g/>
.	.	kIx.	.
</s>
<s>
Členství	členství	k1gNnSc1	členství
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
všechny	všechen	k3xTgMnPc4	všechen
soudní	soudní	k2eAgMnPc4d1	soudní
exekutory	exekutor	k1gMnPc4	exekutor
jmenované	jmenovaná	k1gFnSc2	jmenovaná
ministrem	ministr	k1gMnSc7	ministr
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
povinné	povinný	k2eAgNnSc1d1	povinné
<g/>
.	.	kIx.	.
</s>
<s>
Vykonává	vykonávat	k5eAaImIp3nS	vykonávat
samosprávu	samospráva	k1gFnSc4	samospráva
soudních	soudní	k2eAgMnPc2d1	soudní
exekutorů	exekutor	k1gMnPc2	exekutor
a	a	k8xC	a
dohled	dohled	k1gInSc1	dohled
nad	nad	k7c7	nad
jejich	jejich	k3xOp3gFnSc7	jejich
úřední	úřední	k2eAgFnSc7d1	úřední
činností	činnost	k1gFnSc7	činnost
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
taková	takový	k3xDgFnSc1	takový
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
garantovat	garantovat	k5eAaBmF	garantovat
kvalitu	kvalita	k1gFnSc4	kvalita
exekucí	exekuce	k1gFnPc2	exekuce
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
právních	právní	k2eAgFnPc2d1	právní
služeb	služba	k1gFnPc2	služba
poskytovaných	poskytovaný	k2eAgFnPc2d1	poskytovaná
soudními	soudní	k2eAgMnPc7d1	soudní
exekutory	exekutor	k1gMnPc7	exekutor
<g/>
.	.	kIx.	.
</s>
<s>
Organizace	organizace	k1gFnSc1	organizace
Exekutorské	exekutorský	k2eAgFnSc2d1	Exekutorská
komory	komora	k1gFnSc2	komora
ČR	ČR	kA	ČR
je	být	k5eAaImIp3nS	být
upravena	upravit	k5eAaPmNgFnS	upravit
v	v	k7c6	v
exekučním	exekuční	k2eAgInSc6d1	exekuční
řádu	řád	k1gInSc6	řád
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
sídlila	sídlit	k5eAaImAgFnS	sídlit
jen	jen	k9	jen
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
od	od	k7c2	od
října	říjen	k1gInSc2	říjen
2012	[number]	k4	2012
měla	mít	k5eAaImAgFnS	mít
zřízenou	zřízený	k2eAgFnSc4d1	zřízená
pobočku	pobočka	k1gFnSc4	pobočka
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2013	[number]	k4	2013
se	se	k3xPyFc4	se
její	její	k3xOp3gNnSc1	její
sídlo	sídlo	k1gNnSc1	sídlo
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
novelizací	novelizace	k1gFnSc7	novelizace
exekučního	exekuční	k2eAgInSc2d1	exekuční
řádu	řád	k1gInSc2	řád
přesunulo	přesunout	k5eAaPmAgNnS	přesunout
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
úplně	úplně	k6eAd1	úplně
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
zůstala	zůstat	k5eAaPmAgFnS	zůstat
pobočka	pobočka	k1gFnSc1	pobočka
<g/>
.	.	kIx.	.
</s>
<s>
Exekutorská	exekutorský	k2eAgFnSc1d1	Exekutorská
komora	komora	k1gFnSc1	komora
ČR	ČR	kA	ČR
vydává	vydávat	k5eAaImIp3nS	vydávat
časopis	časopis	k1gInSc1	časopis
Komorní	komorní	k2eAgInPc1d1	komorní
listy	list	k1gInPc1	list
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
odborné	odborný	k2eAgNnSc4d1	odborné
periodikum	periodikum	k1gNnSc4	periodikum
vycházející	vycházející	k2eAgNnSc4d1	vycházející
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
ročně	ročně	k6eAd1	ročně
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
hlavní	hlavní	k2eAgFnSc7d1	hlavní
obsahovou	obsahový	k2eAgFnSc7d1	obsahová
náplní	náplň	k1gFnSc7	náplň
jsou	být	k5eAaImIp3nP	být
odborné	odborný	k2eAgInPc1d1	odborný
články	článek	k1gInPc1	článek
a	a	k8xC	a
příspěvky	příspěvek	k1gInPc1	příspěvek
zabývající	zabývající	k2eAgInPc1d1	zabývající
se	s	k7c7	s
problematikou	problematika	k1gFnSc7	problematika
exekuční	exekuční	k2eAgFnSc2d1	exekuční
činnosti	činnost	k1gFnSc2	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Komorní	komorní	k2eAgInPc1d1	komorní
listy	list	k1gInPc1	list
byly	být	k5eAaImAgInP	být
zapsány	zapsat	k5eAaPmNgInP	zapsat
na	na	k7c4	na
Seznam	seznam	k1gInSc4	seznam
recenzovaných	recenzovaný	k2eAgNnPc2d1	recenzované
neimpaktovaných	impaktovaný	k2eNgNnPc2d1	impaktovaný
periodik	periodikum	k1gNnPc2	periodikum
vydávaných	vydávaný	k2eAgMnPc2d1	vydávaný
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
sněm	sněm	k1gInSc1	sněm
prezidium	prezidium	k1gNnSc1	prezidium
prezident	prezident	k1gMnSc1	prezident
revizní	revizní	k2eAgFnSc2d1	revizní
komise	komise	k1gFnSc2	komise
zkušební	zkušební	k2eAgFnSc2d1	zkušební
komise	komise	k1gFnSc2	komise
kárná	kárný	k2eAgFnSc1d1	kárná
komise	komise	k1gFnSc1	komise
kontrolní	kontrolní	k2eAgFnSc1d1	kontrolní
komise	komise	k1gFnSc1	komise
Prezidentkou	prezidentka	k1gFnSc7	prezidentka
Exekutorské	exekutorský	k2eAgFnSc2d1	Exekutorská
komory	komora	k1gFnSc2	komora
ČR	ČR	kA	ČR
je	být	k5eAaImIp3nS	být
Mgr.	Mgr.	kA	Mgr.
Pavla	Pavla	k1gFnSc1	Pavla
Fučíková	Fučíková	k1gFnSc1	Fučíková
<g/>
,	,	kIx,	,
viceprezidentem	viceprezident	k1gMnSc7	viceprezident
JUDr.	JUDr.	kA	JUDr.
Vladimír	Vladimír	k1gMnSc1	Vladimír
Plášil	Plášil	k1gMnSc1	Plášil
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgMnPc7d1	další
členy	člen	k1gMnPc7	člen
prezidia	prezidium	k1gNnSc2	prezidium
jsou	být	k5eAaImIp3nP	být
Mgr.	Mgr.	kA	Mgr.
Petr	Petr	k1gMnSc1	Petr
Polanský	Polanský	k1gMnSc1	Polanský
<g/>
,	,	kIx,	,
Mgr.	Mgr.	kA	Mgr.
Martin	Martin	k2eAgInSc1d1	Martin
Tunkl	Tunkl	k1gInSc1	Tunkl
a	a	k8xC	a
Mgr.	Mgr.	kA	Mgr.
Jan	Jan	k1gMnSc1	Jan
Mlynarčík	Mlynarčík	k1gMnSc1	Mlynarčík
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
prezidentů	prezident	k1gMnPc2	prezident
Exekutorské	exekutorský	k2eAgFnSc2d1	Exekutorská
komory	komora	k1gFnSc2	komora
ČR	ČR	kA	ČR
JUDr.	JUDr.	kA	JUDr.
Juraj	Juraj	k1gMnSc1	Juraj
Podkonický	podkonický	k2eAgMnSc1d1	podkonický
<g/>
,	,	kIx,	,
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
–	–	k?	–
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
JUDr.	JUDr.	kA	JUDr.
Jana	Jana	k1gFnSc1	Jana
Tvrdková	Tvrdkový	k2eAgFnSc1d1	Tvrdková
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
–	–	k?	–
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
Mgr.	Mgr.	kA	Mgr.
Ing.	ing.	kA	ing.
Jiří	Jiří	k1gMnSc1	Jiří
Prošek	Prošek	k1gMnSc1	Prošek
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
–	–	k?	–
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
Mgr.	Mgr.	kA	Mgr.
<g />
.	.	kIx.	.
</s>
<s>
David	David	k1gMnSc1	David
Koncz	Koncz	k1gMnSc1	Koncz
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
–	–	k?	–
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
Mgr.	Mgr.	kA	Mgr.
Pavla	Pavla	k1gFnSc1	Pavla
Fučíková	Fučíková	k1gFnSc1	Fučíková
(	(	kIx(	(
<g/>
od	od	k7c2	od
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
Exekutorská	exekutorský	k2eAgFnSc1d1	Exekutorská
komora	komora	k1gFnSc1	komora
ČR	ČR	kA	ČR
dle	dle	k7c2	dle
ustanovení	ustanovení	k1gNnSc2	ustanovení
§	§	k?	§
7	[number]	k4	7
odst	odsta	k1gFnPc2	odsta
<g/>
.	.	kIx.	.
5	[number]	k4	5
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
120	[number]	k4	120
<g/>
/	/	kIx~	/
<g/>
2001	[number]	k4	2001
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
soudních	soudní	k2eAgMnPc6d1	soudní
exekutorech	exekutor	k1gMnPc6	exekutor
a	a	k8xC	a
exekuční	exekuční	k2eAgFnSc3d1	exekuční
činnosti	činnost	k1gFnSc3	činnost
(	(	kIx(	(
<g/>
exekuční	exekuční	k2eAgInSc1d1	exekuční
řád	řád	k1gInSc1	řád
<g/>
)	)	kIx)	)
a	a	k8xC	a
o	o	k7c6	o
změně	změna	k1gFnSc6	změna
dalších	další	k2eAgInPc2d1	další
zákonů	zákon	k1gInPc2	zákon
<g/>
,	,	kIx,	,
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
dohled	dohled	k1gInSc1	dohled
nad	nad	k7c7	nad
činností	činnost	k1gFnSc7	činnost
exekutora	exekutor	k1gMnSc2	exekutor
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
řízením	řízení	k1gNnSc7	řízení
činnosti	činnost	k1gFnSc2	činnost
exekutorského	exekutorský	k2eAgInSc2d1	exekutorský
úřadu	úřad	k1gInSc2	úřad
a	a	k8xC	a
nad	nad	k7c7	nad
dodržováním	dodržování	k1gNnSc7	dodržování
povinností	povinnost	k1gFnPc2	povinnost
stanovených	stanovený	k2eAgFnPc2d1	stanovená
exekutorovi	exekutor	k1gMnSc3	exekutor
zákonem	zákon	k1gInSc7	zákon
o	o	k7c4	o
některých	některý	k3yIgNnPc6	některý
opatřeních	opatření	k1gNnPc6	opatření
proti	proti	k7c3	proti
legalizaci	legalizace	k1gFnSc3	legalizace
výnosů	výnos	k1gInPc2	výnos
z	z	k7c2	z
trestné	trestný	k2eAgFnSc2d1	trestná
činnosti	činnost	k1gFnSc2	činnost
a	a	k8xC	a
financování	financování	k1gNnSc2	financování
terorismu	terorismus	k1gInSc2	terorismus
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
výkonu	výkon	k1gInSc6	výkon
dohledu	dohled	k1gInSc2	dohled
je	být	k5eAaImIp3nS	být
Exekutorská	exekutorský	k2eAgFnSc1d1	Exekutorská
komora	komora	k1gFnSc1	komora
ČR	ČR	kA	ČR
oprávněna	oprávněn	k2eAgFnSc1d1	oprávněna
<g/>
:	:	kIx,	:
provádět	provádět	k5eAaImF	provádět
pravidelné	pravidelný	k2eAgFnPc4d1	pravidelná
kontroly	kontrola	k1gFnPc4	kontrola
exekutorských	exekutorský	k2eAgInPc2d1	exekutorský
úřadů	úřad	k1gInPc2	úřad
<g/>
,	,	kIx,	,
nahlížet	nahlížet	k5eAaImF	nahlížet
do	do	k7c2	do
spisů	spis	k1gInPc2	spis
<g/>
,	,	kIx,	,
listin	listina	k1gFnPc2	listina
a	a	k8xC	a
evidenčních	evidenční	k2eAgFnPc2d1	evidenční
pomůcek	pomůcka	k1gFnPc2	pomůcka
exekutora	exekutor	k1gMnSc2	exekutor
<g/>
,	,	kIx,	,
pořizovat	pořizovat	k5eAaImF	pořizovat
si	se	k3xPyFc3	se
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
výpisy	výpis	k1gInPc1	výpis
a	a	k8xC	a
kopie	kopie	k1gFnPc1	kopie
<g/>
,	,	kIx,	,
požadovat	požadovat	k5eAaImF	požadovat
ve	v	k7c6	v
lhůtě	lhůta	k1gFnSc6	lhůta
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
stanoví	stanovit	k5eAaPmIp3nP	stanovit
<g/>
,	,	kIx,	,
písemné	písemný	k2eAgNnSc1d1	písemné
vyjádření	vyjádření	k1gNnSc1	vyjádření
exekutora	exekutor	k1gMnSc2	exekutor
<g />
.	.	kIx.	.
</s>
<s>
k	k	k7c3	k
věci	věc	k1gFnSc3	věc
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
předmětem	předmět	k1gInSc7	předmět
státního	státní	k2eAgInSc2d1	státní
dohledu	dohled	k1gInSc2	dohled
<g/>
,	,	kIx,	,
požadovat	požadovat	k5eAaImF	požadovat
ústní	ústní	k2eAgNnSc4d1	ústní
vysvětlení	vysvětlení	k1gNnSc4	vysvětlení
exekutora	exekutor	k1gMnSc2	exekutor
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
jeho	jeho	k3xOp3gMnPc4	jeho
zaměstnance	zaměstnanec	k1gMnPc4	zaměstnanec
k	k	k7c3	k
věci	věc	k1gFnSc3	věc
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
písemné	písemný	k2eAgNnSc4d1	písemné
vyjádření	vyjádření	k1gNnSc4	vyjádření
není	být	k5eNaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
nedostatečné	dostatečný	k2eNgNnSc1d1	nedostatečné
<g/>
,	,	kIx,	,
vstupovat	vstupovat	k5eAaImF	vstupovat
do	do	k7c2	do
prostor	prostora	k1gFnPc2	prostora
exekutorského	exekutorský	k2eAgInSc2d1	exekutorský
úřadu	úřad	k1gInSc2	úřad
po	po	k7c6	po
předchozím	předchozí	k2eAgNnSc6d1	předchozí
oznámení	oznámení	k1gNnSc6	oznámení
soudnímu	soudní	k2eAgNnSc3d1	soudní
exekutorovi	exekutor	k1gMnSc3	exekutor
nebo	nebo	k8xC	nebo
jeho	jeho	k3xOp3gMnSc3	jeho
zástupci	zástupce	k1gMnSc3	zástupce
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
vedením	vedení	k1gNnSc7	vedení
úřadu	úřad	k1gInSc2	úřad
pověřen	pověřit	k5eAaPmNgMnS	pověřit
<g/>
.	.	kIx.	.
</s>
<s>
Exekutorská	exekutorský	k2eAgFnSc1d1	Exekutorská
komora	komora	k1gFnSc1	komora
vede	vést	k5eAaImIp3nS	vést
bezplatné	bezplatný	k2eAgFnPc4d1	bezplatná
právní	právní	k2eAgFnPc4d1	právní
poradny	poradna	k1gFnPc4	poradna
týkající	týkající	k2eAgFnPc4d1	týkající
se	se	k3xPyFc4	se
provádění	provádění	k1gNnSc1	provádění
exekucí	exekuce	k1gFnPc2	exekuce
a	a	k8xC	a
výkonů	výkon	k1gInPc2	výkon
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc7	jejich
smyslem	smysl	k1gInSc7	smysl
je	být	k5eAaImIp3nS	být
snaha	snaha	k1gFnSc1	snaha
jednak	jednak	k8xC	jednak
pomoci	pomoct	k5eAaPmF	pomoct
i	i	k9	i
sociálně	sociálně	k6eAd1	sociálně
slabším	slabý	k2eAgMnPc3d2	slabší
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
informovat	informovat	k5eAaBmF	informovat
veřejnost	veřejnost	k1gFnSc4	veřejnost
o	o	k7c6	o
problematice	problematika	k1gFnSc6	problematika
exekučního	exekuční	k2eAgNnSc2d1	exekuční
řízení	řízení	k1gNnSc2	řízení
<g/>
.	.	kIx.	.
</s>
<s>
Podstatou	podstata	k1gFnSc7	podstata
takových	takový	k3xDgFnPc2	takový
poraden	poradna	k1gFnPc2	poradna
je	být	k5eAaImIp3nS	být
poskytnout	poskytnout	k5eAaPmF	poskytnout
pouze	pouze	k6eAd1	pouze
informativní	informativní	k2eAgFnSc4d1	informativní
poradu	porada	k1gFnSc4	porada
týkající	týkající	k2eAgFnSc2d1	týkající
se	se	k3xPyFc4	se
exekuce	exekuce	k1gFnSc2	exekuce
<g/>
.	.	kIx.	.
</s>
<s>
Bezplatné	bezplatný	k2eAgFnPc1d1	bezplatná
právní	právní	k2eAgFnPc1d1	právní
poradny	poradna	k1gFnPc1	poradna
fungují	fungovat	k5eAaImIp3nP	fungovat
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
2	[number]	k4	2
(	(	kIx(	(
<g/>
SOS	sos	k1gInSc1	sos
centrum	centrum	k1gNnSc1	centrum
střediska	středisko	k1gNnSc2	středisko
SKP	SKP	kA	SKP
–	–	k?	–
Diakonie	diakonie	k1gFnSc2	diakonie
Českobratrské	českobratrský	k2eAgFnSc2d1	Českobratrská
církve	církev	k1gFnSc2	církev
evangelické	evangelický	k2eAgFnSc2d1	evangelická
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Brně	Brno	k1gNnSc6	Brno
(	(	kIx(	(
<g/>
centrum	centrum	k1gNnSc1	centrum
Diakonie	diakonie	k1gFnSc2	diakonie
Českobratrské	českobratrský	k2eAgFnSc2d1	Českobratrská
církve	církev	k1gFnSc2	církev
evangelické	evangelický	k2eAgFnSc2d1	evangelická
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
(	(	kIx(	(
<g/>
organizace	organizace	k1gFnSc1	organizace
Archa	archa	k1gFnSc1	archa
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
v	v	k7c6	v
krizi	krize	k1gFnSc6	krize
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
navštívit	navštívit	k5eAaPmF	navštívit
bezplatné	bezplatný	k2eAgFnPc4d1	bezplatná
poradny	poradna	k1gFnPc4	poradna
některých	některý	k3yIgInPc2	některý
exekutorských	exekutorský	k2eAgInPc2d1	exekutorský
úřadů	úřad	k1gInPc2	úřad
(	(	kIx(	(
<g/>
např.	např.	kA	např.
soudního	soudní	k2eAgMnSc2d1	soudní
exekutora	exekutor	k1gMnSc2	exekutor
JUDr.	JUDr.	kA	JUDr.
Igora	Igor	k1gMnSc2	Igor
Ivanka	Ivanka	k1gFnSc1	Ivanka
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
6	[number]	k4	6
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Exekutorská	exekutorský	k2eAgFnSc1d1	Exekutorská
komora	komora	k1gFnSc1	komora
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
Exekutorská	exekutorský	k2eAgFnSc1d1	Exekutorská
komora	komora	k1gFnSc1	komora
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
–	–	k?	–
oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
Centrální	centrální	k2eAgInSc1d1	centrální
registr	registr	k1gInSc1	registr
exekucí	exekuce	k1gFnPc2	exekuce
Portál	portál	k1gInSc1	portál
dražeb	dražba	k1gFnPc2	dražba
Bezplatná	bezplatný	k2eAgFnSc1d1	bezplatná
poradna	poradna	k1gFnSc1	poradna
Exekutorské	exekutorský	k2eAgFnSc2d1	Exekutorská
komory	komora	k1gFnSc2	komora
v	v	k7c4	v
SOS	sos	k1gInSc4	sos
centru	centr	k1gInSc2	centr
Diakonie	diakonie	k1gFnSc2	diakonie
ČCE	ČCE	kA	ČCE
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
</s>
