<s>
Exekutorská	exekutorský	k2eAgFnSc1d1	Exekutorská
komora	komora	k1gFnSc1	komora
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
je	být	k5eAaImIp3nS	být
stavovskou	stavovský	k2eAgFnSc7d1	stavovská
organizací	organizace	k1gFnSc7	organizace
profesní	profesní	k2eAgFnSc2d1	profesní
samosprávy	samospráva	k1gFnSc2	samospráva
sdružující	sdružující	k2eAgMnPc4d1	sdružující
soudní	soudní	k2eAgMnPc4d1	soudní
exekutory	exekutor	k1gMnPc4	exekutor
<g/>
.	.	kIx.	.
</s>
