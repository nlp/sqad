<p>
<s>
Rusalka	rusalka	k1gFnSc1	rusalka
je	být	k5eAaImIp3nS	být
název	název	k1gInSc4	název
romantické	romantický	k2eAgFnSc2d1	romantická
pohádkové	pohádkový	k2eAgFnSc2d1	pohádková
opery	opera	k1gFnSc2	opera
Antonína	Antonín	k1gMnSc2	Antonín
Dvořáka	Dvořák	k1gMnSc2	Dvořák
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
libreta	libreto	k1gNnSc2	libreto
je	být	k5eAaImIp3nS	být
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Kvapil	Kvapil	k1gMnSc1	Kvapil
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
dokončena	dokončit	k5eAaPmNgFnS	dokončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1900	[number]	k4	1900
<g/>
,	,	kIx,	,
premiéru	premiér	k1gMnSc3	premiér
měla	mít	k5eAaImAgFnS	mít
31	[number]	k4	31
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1901	[number]	k4	1901
v	v	k7c6	v
Národním	národní	k2eAgNnSc6d1	národní
divadle	divadlo	k1gNnSc6	divadlo
<g/>
,	,	kIx,	,
roli	role	k1gFnSc6	role
Rusalky	rusalka	k1gFnSc2	rusalka
jako	jako	k8xS	jako
první	první	k4xOgFnSc1	první
zpívala	zpívat	k5eAaImAgFnS	zpívat
Růžena	Růžena	k1gFnSc1	Růžena
Maturová	Maturový	k2eAgFnSc1d1	Maturová
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgFnSc1d3	nejznámější
árie	árie	k1gFnSc1	árie
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
opery	opera	k1gFnSc2	opera
je	být	k5eAaImIp3nS	být
zpěv	zpěv	k1gInSc4	zpěv
Rusalky	rusalka	k1gFnSc2	rusalka
k	k	k7c3	k
Měsíci	měsíc	k1gInSc3	měsíc
<g/>
:	:	kIx,	:
Měsíčku	měsíček	k1gInSc2	měsíček
na	na	k7c6	na
nebi	nebe	k1gNnSc6	nebe
hlubokém	hluboký	k2eAgNnSc6d1	hluboké
<g/>
,	,	kIx,	,
nejznámějším	známý	k2eAgNnSc6d3	nejznámější
sborem	sborem	k6eAd1	sborem
pak	pak	k6eAd1	pak
píseň	píseň	k1gFnSc4	píseň
Květiny	květina	k1gFnSc2	květina
bílé	bílý	k2eAgFnSc2d1	bílá
po	po	k7c6	po
cestě	cesta	k1gFnSc6	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
známá	známý	k2eAgFnSc1d1	známá
je	být	k5eAaImIp3nS	být
též	též	k9	též
i	i	k9	i
árie	árie	k1gFnSc1	árie
vodníkova	vodníkův	k2eAgMnSc2d1	vodníkův
Celý	celý	k2eAgInSc4d1	celý
svět	svět	k1gInSc4	svět
nedá	dát	k5eNaPmIp3nS	dát
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
nedá	dát	k5eNaPmIp3nS	dát
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
árie	árie	k1gFnPc1	árie
ježibaby	ježibaba	k1gFnSc2	ježibaba
Čáry	čára	k1gFnSc2	čára
mury	mura	k1gFnSc2	mura
fuk	fuk	k6eAd1	fuk
a	a	k8xC	a
prince	princ	k1gMnSc2	princ
Vidino	vidina	k1gFnSc5	vidina
divná	divný	k2eAgNnPc1d1	divné
<g/>
,	,	kIx,	,
přesladká	přesladký	k2eAgNnPc1d1	přesladké
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Libreto	libreto	k1gNnSc1	libreto
==	==	k?	==
</s>
</p>
<p>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Kvapil	Kvapil	k1gMnSc1	Kvapil
už	už	k6eAd1	už
při	při	k7c6	při
psaní	psaní	k1gNnSc6	psaní
doufal	doufat	k5eAaImAgMnS	doufat
<g/>
,	,	kIx,	,
že	že	k8xS	že
libreto	libreto	k1gNnSc4	libreto
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgMnS	moct
zhudebnit	zhudebnit	k5eAaPmF	zhudebnit
Antonín	Antonín	k1gMnSc1	Antonín
Dvořák	Dvořák	k1gMnSc1	Dvořák
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yIgMnSc7	který
se	se	k3xPyFc4	se
dobře	dobře	k6eAd1	dobře
znal	znát	k5eAaImAgMnS	znát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
netroufal	troufat	k5eNaImAgMnS	troufat
si	se	k3xPyFc3	se
ho	on	k3xPp3gInSc4	on
sám	sám	k3xTgMnSc1	sám
oslovit	oslovit	k5eAaPmF	oslovit
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
jednou	jeden	k4xCgFnSc7	jeden
mu	on	k3xPp3gMnSc3	on
libreto	libreto	k1gNnSc4	libreto
nabídl	nabídnout	k5eAaPmAgInS	nabídnout
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
odmítnut	odmítnout	k5eAaPmNgInS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Přinesl	přinést	k5eAaPmAgMnS	přinést
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
ukázat	ukázat	k5eAaPmF	ukázat
Oskaru	Oskar	k1gMnSc3	Oskar
Nedbalovi	Nedbal	k1gMnSc3	Nedbal
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ho	on	k3xPp3gNnSc4	on
kdysi	kdysi	k6eAd1	kdysi
o	o	k7c4	o
libreto	libreto	k1gNnSc4	libreto
požádal	požádat	k5eAaPmAgMnS	požádat
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
ho	on	k3xPp3gNnSc4	on
však	však	k9	však
slušně	slušně	k6eAd1	slušně
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
právě	právě	k6eAd1	právě
pracuje	pracovat	k5eAaImIp3nS	pracovat
na	na	k7c6	na
něčem	něco	k3yInSc6	něco
jiném	jiný	k2eAgNnSc6d1	jiné
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
práce	práce	k1gFnSc2	práce
ještě	ještě	k9	ještě
Kvapil	Kvapil	k1gMnSc1	Kvapil
kontaktoval	kontaktovat	k5eAaImAgMnS	kontaktovat
J.	J.	kA	J.
B.	B.	kA	B.
Foerstera	Foerster	k1gMnSc2	Foerster
<g/>
,	,	kIx,	,
Karla	Karel	k1gMnSc2	Karel
Kovařovice	Kovařovice	k1gFnSc2	Kovařovice
a	a	k8xC	a
Josefa	Josef	k1gMnSc2	Josef
Suka	Suk	k1gMnSc2	Suk
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
se	s	k7c7	s
stejným	stejný	k2eAgInSc7d1	stejný
výsledkem	výsledek	k1gInSc7	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
Dvořák	Dvořák	k1gMnSc1	Dvořák
se	se	k3xPyFc4	se
však	však	k9	však
zrovna	zrovna	k6eAd1	zrovna
poohlížel	poohlížet	k5eAaImAgMnS	poohlížet
po	po	k7c6	po
novém	nový	k2eAgNnSc6d1	nové
libretu	libreto	k1gNnSc6	libreto
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
Kvapil	Kvapil	k1gMnSc1	Kvapil
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
z	z	k7c2	z
inzerátu	inzerát	k1gInSc2	inzerát
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
nechalo	nechat	k5eAaPmAgNnS	nechat
otisknout	otisknout	k5eAaPmF	otisknout
Národní	národní	k2eAgNnSc1d1	národní
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Osoby	osoba	k1gFnPc4	osoba
a	a	k8xC	a
první	první	k4xOgNnPc4	první
obsazení	obsazení	k1gNnPc4	obsazení
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Obsah	obsah	k1gInSc1	obsah
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
1	[number]	k4	1
<g/>
.	.	kIx.	.
jednání	jednání	k1gNnSc2	jednání
===	===	k?	===
</s>
</p>
<p>
<s>
Nad	nad	k7c7	nad
jezerem	jezero	k1gNnSc7	jezero
skrytým	skrytý	k2eAgNnSc7d1	skryté
v	v	k7c6	v
lesích	les	k1gInPc6	les
vychází	vycházet	k5eAaImIp3nS	vycházet
měsíc	měsíc	k1gInSc1	měsíc
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc1	jeho
paprsky	paprsek	k1gInPc1	paprsek
ozařují	ozařovat	k5eAaImIp3nP	ozařovat
třpytivou	třpytivý	k2eAgFnSc4d1	třpytivá
vodní	vodní	k2eAgFnSc4d1	vodní
hladinu	hladina	k1gFnSc4	hladina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
paloučku	palouček	k1gInSc6	palouček
rozpustile	rozpustile	k6eAd1	rozpustile
dovádějí	dovádět	k5eAaImIp3nP	dovádět
lesní	lesní	k2eAgFnPc4d1	lesní
žínky	žínka	k1gFnPc4	žínka
a	a	k8xC	a
laškují	laškovat	k5eAaImIp3nP	laškovat
s	s	k7c7	s
vodníkem	vodník	k1gMnSc7	vodník
<g/>
.	.	kIx.	.
</s>
<s>
Vodníkova	vodníkův	k2eAgFnSc1d1	Vodníkova
dcera	dcera	k1gFnSc1	dcera
Rusalka	rusalka	k1gFnSc1	rusalka
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
smutná	smutný	k2eAgFnSc1d1	smutná
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
zamilovala	zamilovat	k5eAaPmAgFnS	zamilovat
do	do	k7c2	do
prince	princ	k1gMnSc2	princ
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
často	často	k6eAd1	často
přichází	přicházet	k5eAaImIp3nS	přicházet
k	k	k7c3	k
jezeru	jezero	k1gNnSc3	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Rusalka	rusalka	k1gFnSc1	rusalka
se	se	k3xPyFc4	se
touží	toužit	k5eAaImIp3nS	toužit
stát	stát	k5eAaPmF	stát
člověkem	člověk	k1gMnSc7	člověk
a	a	k8xC	a
získat	získat	k5eAaPmF	získat
tak	tak	k6eAd1	tak
lidskou	lidský	k2eAgFnSc4d1	lidská
duši	duše	k1gFnSc4	duše
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
svému	svůj	k3xOyFgMnSc3	svůj
milému	milý	k1gMnSc3	milý
stále	stále	k6eAd1	stále
nablízku	nablízku	k6eAd1	nablízku
<g/>
.	.	kIx.	.
</s>
<s>
Vodník	vodník	k1gMnSc1	vodník
ji	on	k3xPp3gFnSc4	on
ale	ale	k8xC	ale
před	před	k7c7	před
světem	svět	k1gInSc7	svět
lidí	člověk	k1gMnPc2	člověk
varuje	varovat	k5eAaImIp3nS	varovat
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
totiž	totiž	k9	totiž
dobře	dobře	k6eAd1	dobře
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
lidská	lidský	k2eAgFnSc1d1	lidská
láska	láska	k1gFnSc1	láska
nestálá	stálý	k2eNgFnSc1d1	nestálá
<g/>
.	.	kIx.	.
</s>
<s>
Rusalka	rusalka	k1gFnSc1	rusalka
si	se	k3xPyFc3	se
ale	ale	k9	ale
své	svůj	k3xOyFgNnSc4	svůj
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
nedá	dát	k5eNaPmIp3nS	dát
vymluvit	vymluvit	k5eAaPmF	vymluvit
<g/>
,	,	kIx,	,
a	a	k8xC	a
vodník	vodník	k1gMnSc1	vodník
ji	on	k3xPp3gFnSc4	on
tedy	tedy	k8xC	tedy
pošle	poslat	k5eAaPmIp3nS	poslat
za	za	k7c7	za
ježibabou	ježibaba	k1gFnSc7	ježibaba
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ona	onen	k3xDgFnSc1	onen
je	být	k5eAaImIp3nS	být
jediná	jediný	k2eAgFnSc1d1	jediná
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
může	moct	k5eAaImIp3nS	moct
Rusalce	rusalka	k1gFnSc3	rusalka
pomoci	pomoc	k1gFnSc2	pomoc
<g/>
.	.	kIx.	.
</s>
<s>
Chvilkové	chvilkový	k2eAgNnSc1d1	chvilkové
váhání	váhání	k1gNnSc1	váhání
provází	provázet	k5eAaImIp3nS	provázet
Rusalčin	rusalčin	k2eAgInSc4d1	rusalčin
zpěv	zpěv	k1gInSc4	zpěv
Měsíčku	měsíček	k1gInSc2	měsíček
na	na	k7c6	na
nebi	nebe	k1gNnSc6	nebe
hlubokém	hluboký	k2eAgNnSc6d1	hluboké
<g/>
.	.	kIx.	.
</s>
<s>
Ježibaba	ježibaba	k1gFnSc1	ježibaba
souhlasí	souhlasit	k5eAaImIp3nS	souhlasit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Rusalku	rusalka	k1gFnSc4	rusalka
promění	proměnit	k5eAaPmIp3nS	proměnit
v	v	k7c4	v
lidskou	lidský	k2eAgFnSc4d1	lidská
bytost	bytost	k1gFnSc4	bytost
<g/>
,	,	kIx,	,
a	a	k8xC	a
žádá	žádat	k5eAaImIp3nS	žádat
si	se	k3xPyFc3	se
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
nemalou	malý	k2eNgFnSc4d1	nemalá
cenu	cena	k1gFnSc4	cena
–	–	k?	–
v	v	k7c6	v
lidské	lidský	k2eAgFnSc6d1	lidská
podobě	podoba	k1gFnSc6	podoba
bude	být	k5eAaImBp3nS	být
Rusalka	rusalka	k1gFnSc1	rusalka
němá	němý	k2eAgFnSc1d1	němá
<g/>
,	,	kIx,	,
a	a	k8xC	a
pokud	pokud	k8xS	pokud
si	se	k3xPyFc3	se
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
udržet	udržet	k5eAaPmF	udržet
princovu	princův	k2eAgFnSc4d1	princova
lásku	láska	k1gFnSc4	láska
<g/>
,	,	kIx,	,
stane	stanout	k5eAaPmIp3nS	stanout
se	se	k3xPyFc4	se
navěky	navěky	k6eAd1	navěky
bludičkou	bludička	k1gFnSc7	bludička
<g/>
.	.	kIx.	.
</s>
<s>
Princ	princ	k1gMnSc1	princ
na	na	k7c6	na
lovu	lov	k1gInSc6	lov
přijíždí	přijíždět	k5eAaImIp3nS	přijíždět
opět	opět	k6eAd1	opět
k	k	k7c3	k
jezeru	jezero	k1gNnSc3	jezero
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nachází	nacházet	k5eAaImIp3nS	nacházet
němou	němý	k2eAgFnSc4d1	němá
Rusalku	rusalka	k1gFnSc4	rusalka
a	a	k8xC	a
odvádí	odvádět	k5eAaImIp3nS	odvádět
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
zámek	zámek	k1gInSc4	zámek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
2	[number]	k4	2
<g/>
.	.	kIx.	.
jednání	jednání	k1gNnSc2	jednání
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
plném	plný	k2eAgInSc6d1	plný
proudu	proud	k1gInSc6	proud
přípravy	příprava	k1gFnSc2	příprava
na	na	k7c4	na
princovu	princův	k2eAgFnSc4d1	princova
svatbu	svatba	k1gFnSc4	svatba
s	s	k7c7	s
Rusalkou	rusalka	k1gFnSc7	rusalka
<g/>
.	.	kIx.	.
</s>
<s>
Přijíždí	přijíždět	k5eAaImIp3nS	přijíždět
i	i	k9	i
cizí	cizí	k2eAgFnSc1d1	cizí
kněžna	kněžna	k1gFnSc1	kněžna
<g/>
.	.	kIx.	.
</s>
<s>
Princ	princ	k1gMnSc1	princ
podlehne	podlehnout	k5eAaPmIp3nS	podlehnout
její	její	k3xOp3gFnPc4	její
vyzývavosti	vyzývavost	k1gFnPc4	vyzývavost
a	a	k8xC	a
smyslnosti	smyslnost	k1gFnPc4	smyslnost
<g/>
,	,	kIx,	,
tolik	tolik	k6eAd1	tolik
odlišné	odlišný	k2eAgInPc1d1	odlišný
od	od	k7c2	od
Rusalčiny	rusalčin	k2eAgFnSc2d1	Rusalčina
chladné	chladný	k2eAgFnSc2d1	chladná
krásy	krása	k1gFnSc2	krása
<g/>
,	,	kIx,	,
a	a	k8xC	a
začne	začít	k5eAaPmIp3nS	začít
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc2	on
dvořit	dvořit	k5eAaImF	dvořit
<g/>
.	.	kIx.	.
</s>
<s>
Vodník	vodník	k1gMnSc1	vodník
zpovzdálí	zpovzdálí	k6eAd1	zpovzdálí
sleduje	sledovat	k5eAaImIp3nS	sledovat
Rusalčino	rusalčin	k2eAgNnSc4d1	Rusalčino
trápení	trápení	k1gNnSc4	trápení
(	(	kIx(	(
<g/>
árie	árie	k1gFnSc2	árie
Celý	celý	k2eAgInSc1d1	celý
svět	svět	k1gInSc1	svět
nedá	dát	k5eNaPmIp3nS	dát
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
nedá	dát	k5eNaPmIp3nS	dát
<g/>
)	)	kIx)	)
i	i	k8xC	i
přípravy	příprava	k1gFnSc2	příprava
na	na	k7c4	na
svatební	svatební	k2eAgInSc4d1	svatební
obřad	obřad	k1gInSc4	obřad
(	(	kIx(	(
<g/>
polonéza	polonéza	k1gFnSc1	polonéza
a	a	k8xC	a
svatební	svatební	k2eAgInSc1d1	svatební
sbor	sbor	k1gInSc1	sbor
Květiny	květina	k1gFnSc2	květina
bílé	bílý	k2eAgFnSc2d1	bílá
po	po	k7c6	po
cestě	cesta	k1gFnSc6	cesta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rusalka	rusalka	k1gFnSc1	rusalka
se	se	k3xPyFc4	se
bojí	bát	k5eAaImIp3nS	bát
o	o	k7c4	o
svou	svůj	k3xOyFgFnSc4	svůj
lásku	láska	k1gFnSc4	láska
a	a	k8xC	a
utíká	utíkat	k5eAaImIp3nS	utíkat
pro	pro	k7c4	pro
radu	rada	k1gMnSc4	rada
k	k	k7c3	k
vodníkovi	vodník	k1gMnSc3	vodník
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
ji	on	k3xPp3gFnSc4	on
vybízí	vybízet	k5eAaImIp3nS	vybízet
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
o	o	k7c4	o
svou	svůj	k3xOyFgFnSc4	svůj
lásku	láska	k1gFnSc4	láska
bojovala	bojovat	k5eAaImAgFnS	bojovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
už	už	k6eAd1	už
je	být	k5eAaImIp3nS	být
pozdě	pozdě	k6eAd1	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Princ	princ	k1gMnSc1	princ
zcela	zcela	k6eAd1	zcela
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
kouzlu	kouzlo	k1gNnSc3	kouzlo
exotické	exotický	k2eAgFnSc2d1	exotická
kněžny	kněžna	k1gFnSc2	kněžna
a	a	k8xC	a
Rusalku	rusalka	k1gFnSc4	rusalka
zavrhuje	zavrhovat	k5eAaImIp3nS	zavrhovat
<g/>
.	.	kIx.	.
</s>
<s>
Vodník	vodník	k1gMnSc1	vodník
slibuje	slibovat	k5eAaImIp3nS	slibovat
pomstu	pomsta	k1gFnSc4	pomsta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
3	[number]	k4	3
<g/>
.	.	kIx.	.
jednání	jednání	k1gNnSc2	jednání
===	===	k?	===
</s>
</p>
<p>
<s>
Rusalka	rusalka	k1gFnSc1	rusalka
se	se	k3xPyFc4	se
vrátila	vrátit	k5eAaPmAgFnS	vrátit
ke	k	k7c3	k
svému	svůj	k3xOyFgNnSc3	svůj
jezeru	jezero	k1gNnSc3	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
teď	teď	k6eAd1	teď
bludičkou	bludička	k1gFnSc7	bludička
<g/>
,	,	kIx,	,
lákající	lákající	k2eAgMnPc4d1	lákající
poutníky	poutník	k1gMnPc4	poutník
do	do	k7c2	do
bažin	bažina	k1gFnPc2	bažina
<g/>
,	,	kIx,	,
a	a	k8xC	a
smutní	smutnit	k5eAaImIp3nP	smutnit
nad	nad	k7c7	nad
svým	svůj	k3xOyFgInSc7	svůj
osudem	osud	k1gInSc7	osud
(	(	kIx(	(
<g/>
árie	árie	k1gFnSc1	árie
Necitelná	citelný	k2eNgFnSc1d1	necitelná
vodní	vodní	k2eAgFnSc3d1	vodní
moci	moc	k1gFnSc3	moc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ježibaba	ježibaba	k1gFnSc1	ježibaba
jí	on	k3xPp3gFnSc3	on
nabízí	nabízet	k5eAaImIp3nS	nabízet
východisko	východisko	k1gNnSc1	východisko
–	–	k?	–
pokud	pokud	k8xS	pokud
zabije	zabít	k5eAaPmIp3nS	zabít
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
jí	jíst	k5eAaImIp3nS	jíst
ublížil	ublížit	k5eAaPmAgMnS	ublížit
<g/>
,	,	kIx,	,
stane	stanout	k5eAaPmIp3nS	stanout
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
vodní	vodní	k2eAgFnSc7d1	vodní
vílou	víla	k1gFnSc7	víla
<g/>
.	.	kIx.	.
</s>
<s>
Rusalka	rusalka	k1gFnSc1	rusalka
však	však	k9	však
odmítá	odmítat	k5eAaImIp3nS	odmítat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
prince	princ	k1gMnSc2	princ
stále	stále	k6eAd1	stále
miluje	milovat	k5eAaImIp3nS	milovat
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
ani	ani	k8xC	ani
princ	princ	k1gMnSc1	princ
není	být	k5eNaImIp3nS	být
šťastný	šťastný	k2eAgMnSc1d1	šťastný
<g/>
.	.	kIx.	.
</s>
<s>
Exotická	exotický	k2eAgFnSc1d1	exotická
kněžna	kněžna	k1gFnSc1	kněžna
ho	on	k3xPp3gInSc4	on
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
teď	teď	k6eAd1	teď
bloudí	bloudit	k5eAaImIp3nS	bloudit
po	po	k7c6	po
lese	les	k1gInSc6	les
a	a	k8xC	a
hledá	hledat	k5eAaImIp3nS	hledat
ztracenou	ztracený	k2eAgFnSc4d1	ztracená
lásku	láska	k1gFnSc4	láska
<g/>
.	.	kIx.	.
</s>
<s>
Vrací	vracet	k5eAaImIp3nS	vracet
se	se	k3xPyFc4	se
k	k	k7c3	k
jezeru	jezero	k1gNnSc3	jezero
a	a	k8xC	a
volá	volat	k5eAaImIp3nS	volat
Rusalku	rusalka	k1gFnSc4	rusalka
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
zjevuje	zjevovat	k5eAaImIp3nS	zjevovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
varuje	varovat	k5eAaImIp3nS	varovat
jej	on	k3xPp3gInSc4	on
–	–	k?	–
její	její	k3xOp3gInSc4	její
polibek	polibek	k1gInSc4	polibek
mu	on	k3xPp3gMnSc3	on
přinese	přinést	k5eAaPmIp3nS	přinést
smrt	smrt	k1gFnSc4	smrt
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
však	však	k9	však
nechce	chtít	k5eNaImIp3nS	chtít
bez	bez	k7c2	bez
ní	on	k3xPp3gFnSc2	on
dál	daleko	k6eAd2	daleko
žít	žít	k5eAaImF	žít
a	a	k8xC	a
vrhá	vrhat	k5eAaImIp3nS	vrhat
se	se	k3xPyFc4	se
v	v	k7c4	v
její	její	k3xOp3gNnSc4	její
smrtící	smrtící	k2eAgNnSc4d1	smrtící
objetí	objetí	k1gNnSc4	objetí
<g/>
.	.	kIx.	.
</s>
<s>
Vodník	vodník	k1gMnSc1	vodník
smutně	smutně	k6eAd1	smutně
oznamuje	oznamovat	k5eAaImIp3nS	oznamovat
Rusalce	rusalka	k1gFnSc3	rusalka
<g/>
,	,	kIx,	,
že	že	k8xS	že
oběť	oběť	k1gFnSc1	oběť
je	být	k5eAaImIp3nS	být
marná	marný	k2eAgFnSc1d1	marná
-	-	kIx~	-
Rusalka	rusalka	k1gFnSc1	rusalka
neprolila	prolít	k5eNaPmAgFnS	prolít
princovu	princův	k2eAgFnSc4d1	princova
krev	krev	k1gFnSc4	krev
a	a	k8xC	a
zůstane	zůstat	k5eAaPmIp3nS	zůstat
navždy	navždy	k6eAd1	navždy
bludičkou	bludička	k1gFnSc7	bludička
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
princovi	princ	k1gMnSc3	princ
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
slovech	slovo	k1gNnPc6	slovo
opery	opera	k1gFnSc2	opera
všechno	všechen	k3xTgNnSc4	všechen
za	za	k7c4	za
jeho	jeho	k3xOp3gFnSc4	jeho
lidskou	lidský	k2eAgFnSc4d1	lidská
lásku	láska	k1gFnSc4	láska
promíjí	promíjet	k5eAaImIp3nS	promíjet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nahrávky	nahrávka	k1gFnSc2	nahrávka
==	==	k?	==
</s>
</p>
<p>
<s>
1948	[number]	k4	1948
(	(	kIx(	(
<g/>
LP	LP	kA	LP
Urania	uranium	k1gNnPc4	uranium
219	[number]	k4	219
<g/>
;	;	kIx,	;
CD	CD	kA	CD
Relief	Relief	k1gMnSc1	Relief
CR	cr	k0	cr
1903	[number]	k4	1903
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
Cantus	Cantus	k1gInSc1	Cantus
Classics	Classics	k1gInSc1	Classics
5.004	[number]	k4	5.004
<g/>
31	[number]	k4	31
<g/>
/	/	kIx~	/
<g/>
Hänssler	Hänssler	k1gInSc1	Hänssler
Profil	profil	k1gInSc1	profil
<g/>
/	/	kIx~	/
<g/>
Naxos	Naxos	k1gInSc1	Naxos
PH	Ph	kA	Ph
0	[number]	k4	0
<g/>
6031	[number]	k4	6031
<g/>
,	,	kIx,	,
vydáno	vydat	k5eAaPmNgNnS	vydat
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
Zpívají	zpívat	k5eAaImIp3nP	zpívat
<g/>
:	:	kIx,	:
(	(	kIx(	(
<g/>
Rusalka	rusalka	k1gFnSc1	rusalka
<g/>
)	)	kIx)	)
Elfride	Elfrid	k1gInSc5	Elfrid
<g />
.	.	kIx.	.
</s>
<s>
Trötschel	Trötschet	k5eAaBmAgMnS	Trötschet
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
vodník	vodník	k1gMnSc1	vodník
<g/>
)	)	kIx)	)
Gottlob	Gottloba	k1gFnPc2	Gottloba
Frick	Frick	k1gInSc1	Frick
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
princ	princ	k1gMnSc1	princ
<g/>
)	)	kIx)	)
Helmut	Helmut	k1gMnSc1	Helmut
Schindler	Schindler	k1gMnSc1	Schindler
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
ježibaba	ježibaba	k1gFnSc1	ježibaba
<g/>
)	)	kIx)	)
Helena	Helena	k1gFnSc1	Helena
Rott	Rott	k1gMnSc1	Rott
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
cizí	cizí	k2eAgFnSc1d1	cizí
kněžna	kněžna	k1gFnSc1	kněžna
<g/>
)	)	kIx)	)
Ruth	Ruth	k1gFnSc1	Ruth
Lange	Lange	k1gFnSc1	Lange
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
lesní	lesní	k2eAgFnSc1d1	lesní
žínka	žínka	k1gFnSc1	žínka
<g/>
)	)	kIx)	)
Eva	Eva	k1gFnSc1	Eva
<g />
.	.	kIx.	.
</s>
<s>
Fleischhauer	Fleischhauer	k1gInSc1	Fleischhauer
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
lesní	lesní	k2eAgFnSc1d1	lesní
žínka	žínka	k1gFnSc1	žínka
<g/>
)	)	kIx)	)
Edith	Edith	k1gMnSc1	Edith
Hellriegel	Hellriegel	k1gMnSc1	Hellriegel
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
lesní	lesní	k2eAgFnSc1d1	lesní
žínka	žínka	k1gFnSc1	žínka
<g/>
)	)	kIx)	)
Käte	Käte	k1gFnSc1	Käte
Höfgen	Höfgen	k1gInSc1	Höfgen
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
hajný	hajný	k1gMnSc1	hajný
<g/>
)	)	kIx)	)
Erich	Erich	k1gMnSc1	Erich
Zimmermann	Zimmermann	k1gMnSc1	Zimmermann
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
kuchtík	kuchtík	k1gMnSc1	kuchtík
<g/>
)	)	kIx)	)
Lisa	Lisa	k1gFnSc1	Lisa
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
lovec	lovec	k1gMnSc1	lovec
<g/>
)	)	kIx)	)
Kurt	Kurt	k1gMnSc1	Kurt
Preuss	Preussa	k1gFnPc2	Preussa
<g/>
.	.	kIx.	.
</s>
<s>
Dresden	Dresdna	k1gFnPc2	Dresdna
Staatskapelle	Staatskapelle	k1gNnPc2	Staatskapelle
a	a	k8xC	a
Chor	Chora	k1gFnPc2	Chora
der	drát	k5eAaImRp2nS	drát
Dresdner	Dresdner	k1gMnSc1	Dresdner
Staatsoper	Staatsoper	k1gMnSc1	Staatsoper
řídí	řídit	k5eAaImIp3nS	řídit
Joseph	Joseph	k1gMnSc1	Joseph
Keilberth	Keilberth	k1gMnSc1	Keilberth
(	(	kIx(	(
<g/>
v	v	k7c6	v
němčině	němčina	k1gFnSc6	němčina
<g/>
,	,	kIx,	,
mono	mono	k6eAd1	mono
<g/>
,	,	kIx,	,
silně	silně	k6eAd1	silně
kráceno	krácen	k2eAgNnSc1d1	kráceno
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1952	[number]	k4	1952
(	(	kIx(	(
<g/>
Supraphon	supraphon	k1gInSc1	supraphon
<g/>
,	,	kIx,	,
LP	LP	kA	LP
1955	[number]	k4	1955
<g/>
,	,	kIx,	,
SLPV	SLPV	kA	SLPV
94	[number]	k4	94
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
CD	CD	kA	CD
vydáno	vydat	k5eAaPmNgNnS	vydat
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
SU	SU	k?	SU
3811-2	[number]	k4	3811-2
602	[number]	k4	602
<g/>
)	)	kIx)	)
Zpívají	zpívat	k5eAaImIp3nP	zpívat
<g/>
:	:	kIx,	:
(	(	kIx(	(
<g/>
Rusalka	rusalka	k1gFnSc1	rusalka
<g/>
)	)	kIx)	)
Ludmila	Ludmila	k1gFnSc1	Ludmila
Červinková	Červinková	k1gFnSc1	Červinková
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
vodník	vodník	k1gMnSc1	vodník
<g/>
)	)	kIx)	)
Eduard	Eduard	k1gMnSc1	Eduard
Haken	Haken	k1gMnSc1	Haken
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
princ	princ	k1gMnSc1	princ
<g/>
)	)	kIx)	)
Beno	Beno	k1gMnSc1	Beno
Blachut	Blachut	k1gMnSc1	Blachut
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
ježibaba	ježibaba	k1gFnSc1	ježibaba
<g/>
)	)	kIx)	)
Marta	Marta	k1gFnSc1	Marta
Krásová	Krásová	k1gFnSc1	Krásová
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
cizí	cizí	k2eAgFnSc1d1	cizí
kněžna	kněžna	k1gFnSc1	kněžna
<g/>
)	)	kIx)	)
Marie	Marie	k1gFnSc1	Marie
Podvalová	Podvalová	k1gFnSc1	Podvalová
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
lesní	lesní	k2eAgFnSc1d1	lesní
žínka	žínka	k1gFnSc1	žínka
<g/>
)	)	kIx)	)
Maria	Maria	k1gFnSc1	Maria
Tauberová	Tauberová	k1gFnSc1	Tauberová
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
lesní	lesní	k2eAgFnSc1d1	lesní
žínka	žínka	k1gFnSc1	žínka
<g/>
)	)	kIx)	)
Miloslava	Miloslava	k1gFnSc1	Miloslava
Fidlerová	Fidlerová	k1gFnSc1	Fidlerová
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
lesní	lesní	k2eAgFnSc1d1	lesní
žínka	žínka	k1gFnSc1	žínka
<g/>
)	)	kIx)	)
Věra	Věra	k1gFnSc1	Věra
Krilová	Krilová	k1gFnSc1	Krilová
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
hajný	hajný	k1gMnSc1	hajný
<g/>
)	)	kIx)	)
Jiří	Jiří	k1gMnSc1	Jiří
Joran	Joran	k1gMnSc1	Joran
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
kuchtík	kuchtík	k1gMnSc1	kuchtík
<g/>
)	)	kIx)	)
Ludmila	Ludmila	k1gFnSc1	Ludmila
Hanzalíková	Hanzalíková	k1gFnSc1	Hanzalíková
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
lovec	lovec	k1gMnSc1	lovec
<g/>
)	)	kIx)	)
Přemysl	Přemysl	k1gMnSc1	Přemysl
Kočí	Kočí	k1gMnSc1	Kočí
<g/>
.	.	kIx.	.
</s>
<s>
Orchestr	orchestr	k1gInSc1	orchestr
a	a	k8xC	a
sbor	sbor	k1gInSc1	sbor
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
řídí	řídit	k5eAaImIp3nS	řídit
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Krombholc	Krombholc	k1gFnSc4	Krombholc
</s>
</p>
<p>
<s>
1953	[number]	k4	1953
(	(	kIx(	(
<g/>
nevydáno	vydán	k2eNgNnSc1d1	nevydáno
<g/>
)	)	kIx)	)
Zpívají	zpívat	k5eAaImIp3nP	zpívat
<g/>
:	:	kIx,	:
(	(	kIx(	(
<g/>
Rusalka	rusalka	k1gFnSc1	rusalka
<g/>
)	)	kIx)	)
Maria	Maria	k1gFnSc1	Maria
Tauberová	Tauberová	k1gFnSc1	Tauberová
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
vodník	vodník	k1gMnSc1	vodník
<g/>
)	)	kIx)	)
Karel	Karel	k1gMnSc1	Karel
Kalaš	Kalaš	k1gMnSc1	Kalaš
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
princ	princ	k1gMnSc1	princ
<g/>
)	)	kIx)	)
Beno	Beno	k1gMnSc1	Beno
Blachut	Blachut	k1gMnSc1	Blachut
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
ježibaba	ježibaba	k1gFnSc1	ježibaba
<g/>
)	)	kIx)	)
Ludmila	Ludmila	k1gFnSc1	Ludmila
Hanzalíková	Hanzalíková	k1gFnSc1	Hanzalíková
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
lovec	lovec	k1gMnSc1	lovec
<g/>
)	)	kIx)	)
Bořek	Bořek	k1gMnSc1	Bořek
<g />
.	.	kIx.	.
</s>
<s>
Rujan	Rujan	k1gMnSc1	Rujan
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
kuchtík	kuchtík	k1gMnSc1	kuchtík
<g/>
)	)	kIx)	)
Štefa	Štef	k1gMnSc2	Štef
Petrová	Petrová	k1gFnSc1	Petrová
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
hajná	hajná	k1gFnSc1	hajná
<g/>
)	)	kIx)	)
Karel	Karel	k1gMnSc1	Karel
Leiss	Leissa	k1gFnPc2	Leissa
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
lesní	lesní	k2eAgFnSc1d1	lesní
žínka	žínka	k1gFnSc1	žínka
<g/>
)	)	kIx)	)
Milada	Milada	k1gFnSc1	Milada
Jirásková	Jirásková	k1gFnSc1	Jirásková
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
lesní	lesní	k2eAgFnSc1d1	lesní
žínka	žínka	k1gFnSc1	žínka
<g/>
)	)	kIx)	)
Gabriela	Gabriela	k1gFnSc1	Gabriela
Najmanová	Najmanová	k1gFnSc1	Najmanová
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
lesní	lesní	k2eAgFnSc1d1	lesní
žínka	žínka	k1gFnSc1	žínka
<g/>
)	)	kIx)	)
Dagmar	Dagmar	k1gFnSc1	Dagmar
Linhartová	Linhartová	k1gFnSc1	Linhartová
<g/>
.	.	kIx.	.
</s>
<s>
Pražský	pražský	k2eAgInSc1d1	pražský
rozhlasový	rozhlasový	k2eAgInSc1d1	rozhlasový
orchestr	orchestr	k1gInSc1	orchestr
a	a	k8xC	a
Pěvecký	pěvecký	k2eAgInSc1d1	pěvecký
sbor	sbor	k1gInSc1	sbor
Československého	československý	k2eAgInSc2d1	československý
rozhlasu	rozhlas	k1gInSc2	rozhlas
řídí	řídit	k5eAaImIp3nS	řídit
František	František	k1gMnSc1	František
Dyk	Dyk	k?	Dyk
(	(	kIx(	(
<g/>
průřez	průřez	k1gInSc1	průřez
<g/>
,	,	kIx,	,
v	v	k7c6	v
esperantu	esperanto	k1gNnSc6	esperanto
<g/>
,	,	kIx,	,
průvodní	průvodní	k2eAgNnSc1d1	průvodní
slovo	slovo	k1gNnSc1	slovo
Karel	Karel	k1gMnSc1	Karel
Höger	Höger	k1gMnSc1	Höger
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1954	[number]	k4	1954
(	(	kIx(	(
<g/>
Walhall	Walhall	k1gInSc1	Walhall
<g/>
)	)	kIx)	)
Zpívají	zpívat	k5eAaImIp3nP	zpívat
<g/>
:	:	kIx,	:
(	(	kIx(	(
<g/>
Rusalka	rusalka	k1gFnSc1	rusalka
<g/>
)	)	kIx)	)
E.	E.	kA	E.
Schneider	Schneider	k1gMnSc1	Schneider
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
vodník	vodník	k1gMnSc1	vodník
<g/>
)	)	kIx)	)
Walter	Walter	k1gMnSc1	Walter
Berry	Berra	k1gFnSc2	Berra
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
princ	princ	k1gMnSc1	princ
<g/>
)	)	kIx)	)
Waldemar	Waldemar	k1gMnSc1	Waldemar
Kmentt	Kmentt	k1gMnSc1	Kmentt
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
ježibaba	ježibaba	k1gFnSc1	ježibaba
<g/>
)	)	kIx)	)
Hilde	Hild	k1gInSc5	Hild
Rössl-Majdan	Rössl-Majdan	k1gMnSc1	Rössl-Majdan
aj.	aj.	kA	aj.
Wiener	Wiener	k1gMnSc1	Wiener
Rundfunkorchester	Rundfunkorchester	k1gMnSc1	Rundfunkorchester
a	a	k8xC	a
Wiener	Wiener	k1gMnSc1	Wiener
Rundfunkchor	Rundfunkchor	k1gMnSc1	Rundfunkchor
řídí	řídit	k5eAaImIp3nS	řídit
Felix	Felix	k1gMnSc1	Felix
Prohaska	Prohasko	k1gNnSc2	Prohasko
(	(	kIx(	(
<g/>
kráceno	krácen	k2eAgNnSc1d1	kráceno
<g/>
,	,	kIx,	,
v	v	k7c6	v
němčině	němčina	k1gFnSc6	němčina
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1961	[number]	k4	1961
(	(	kIx(	(
<g/>
Supraphon	supraphon	k1gInSc1	supraphon
<g/>
,	,	kIx,	,
LP	LP	kA	LP
SUA	SUA	kA	SUA
ST	St	kA	St
50440	[number]	k4	50440
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
CD	CD	kA	CD
vydáno	vydat	k5eAaPmNgNnS	vydat
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
SU	SU	k?	SU
0013-2	[number]	k4	0013-2
612	[number]	k4	612
<g/>
;	;	kIx,	;
Line	linout	k5eAaImIp3nS	linout
5.000	[number]	k4	5.000
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
)	)	kIx)	)
Zpívají	zpívat	k5eAaImIp3nP	zpívat
<g/>
:	:	kIx,	:
(	(	kIx(	(
<g/>
Rusalka	rusalka	k1gFnSc1	rusalka
<g/>
)	)	kIx)	)
Milada	Milada	k1gFnSc1	Milada
Šubrtová	Šubrtová	k1gFnSc1	Šubrtová
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
vodník	vodník	k1gMnSc1	vodník
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Eduard	Eduard	k1gMnSc1	Eduard
Haken	Haken	k1gMnSc1	Haken
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
princ	princ	k1gMnSc1	princ
<g/>
)	)	kIx)	)
Ivo	Ivo	k1gMnSc1	Ivo
Žídek	Žídek	k1gMnSc1	Žídek
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
ježibaba	ježibaba	k1gFnSc1	ježibaba
<g/>
)	)	kIx)	)
Marie	Marie	k1gFnSc1	Marie
Ovčačíková	Ovčačíková	k1gFnSc1	Ovčačíková
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
cizí	cizí	k2eAgFnSc1d1	cizí
kněžna	kněžna	k1gFnSc1	kněžna
<g/>
)	)	kIx)	)
Alena	Alena	k1gFnSc1	Alena
Míková	Míková	k1gFnSc1	Míková
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
lesní	lesní	k2eAgFnSc1d1	lesní
žínka	žínka	k1gFnSc1	žínka
<g/>
)	)	kIx)	)
Jadwiga	Jadwiga	k1gFnSc1	Jadwiga
Wysoczanská	Wysoczanský	k2eAgFnSc1d1	Wysoczanský
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
lesní	lesní	k2eAgFnSc1d1	lesní
žínka	žínka	k1gFnSc1	žínka
<g/>
)	)	kIx)	)
Eva	Eva	k1gFnSc1	Eva
Hlobilová	Hlobilová	k1gFnSc1	Hlobilová
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
lesní	lesní	k2eAgFnSc1d1	lesní
žínka	žínka	k1gFnSc1	žínka
<g/>
)	)	kIx)	)
Věra	Věra	k1gFnSc1	Věra
Krilová	Krilová	k1gFnSc1	Krilová
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
hajný	hajný	k1gMnSc1	hajný
<g/>
)	)	kIx)	)
Jiří	Jiří	k1gMnSc1	Jiří
Joran	Joran	k1gMnSc1	Joran
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
kuchtík	kuchtík	k1gMnSc1	kuchtík
<g/>
)	)	kIx)	)
Ivana	Ivana	k1gFnSc1	Ivana
Mixová	Mixová	k1gFnSc1	Mixová
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
lovec	lovec	k1gMnSc1	lovec
<g/>
)	)	kIx)	)
Václav	Václav	k1gMnSc1	Václav
Bednář	Bednář	k1gMnSc1	Bednář
<g/>
.	.	kIx.	.
</s>
<s>
Orchestr	orchestr	k1gInSc1	orchestr
a	a	k8xC	a
sbor	sbor	k1gInSc1	sbor
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
řídí	řídit	k5eAaImIp3nS	řídit
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Chalabala	Chalabal	k1gMnSc2	Chalabal
</s>
</p>
<p>
<s>
1971	[number]	k4	1971
(	(	kIx(	(
<g/>
Berlin	berlina	k1gFnPc2	berlina
Classics	Classics	k1gInSc1	Classics
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
20332	[number]	k4	20332
<g/>
BC	BC	kA	BC
<g/>
)	)	kIx)	)
Zpívají	zpívat	k5eAaImIp3nP	zpívat
<g/>
:	:	kIx,	:
(	(	kIx(	(
<g/>
Rusalka	rusalka	k1gFnSc1	rusalka
<g/>
)	)	kIx)	)
Elka	Elk	k2eAgFnSc1d1	Elka
Mitzewa	Mitzewa	k1gFnSc1	Mitzewa
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
vodník	vodník	k1gMnSc1	vodník
<g/>
)	)	kIx)	)
Theo	Thea	k1gFnSc5	Thea
Adam	Adam	k1gMnSc1	Adam
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
princ	princ	k1gMnSc1	princ
<g/>
)	)	kIx)	)
Peter	Peter	k1gMnSc1	Peter
Bindszus	Bindszus	k1gMnSc1	Bindszus
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
ježibaba	ježibaba	k1gFnSc1	ježibaba
<g/>
)	)	kIx)	)
Annelies	Annelies	k1gInSc1	Annelies
Burmeister	Burmeister	k1gInSc1	Burmeister
aj.	aj.	kA	aj.
Staatskapelle	Staatskapelle	k1gInSc4	Staatskapelle
Berlin	berlina	k1gFnPc2	berlina
a	a	k8xC	a
Chor	Chora	k1gFnPc2	Chora
der	drát	k5eAaImRp2nS	drát
Deutschen	Deutschen	k1gInSc4	Deutschen
Staatsoper	Staatsoper	k1gInSc1	Staatsoper
Berlin	berlina	k1gFnPc2	berlina
řídí	řídit	k5eAaImIp3nS	řídit
Arthur	Arthur	k1gMnSc1	Arthur
Apelt	Apelt	k1gMnSc1	Apelt
</s>
</p>
<p>
<s>
1975	[number]	k4	1975
(	(	kIx(	(
<g/>
Gala	gala	k2eAgFnSc1d1	gala
GL	GL	kA	GL
<g/>
100.762	[number]	k4	100.762
<g/>
)	)	kIx)	)
Zpívají	zpívat	k5eAaImIp3nP	zpívat
<g/>
:	:	kIx,	:
(	(	kIx(	(
<g/>
Rusalka	rusalka	k1gFnSc1	rusalka
<g/>
)	)	kIx)	)
Hildegard	Hildegard	k1gInSc1	Hildegard
Behrens	Behrensa	k1gFnPc2	Behrensa
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
vodník	vodník	k1gMnSc1	vodník
<g/>
)	)	kIx)	)
Malcolm	Malcolm	k1gMnSc1	Malcolm
Smith	Smith	k1gMnSc1	Smith
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
princ	princ	k1gMnSc1	princ
<g/>
)	)	kIx)	)
Werner	Werner	k1gMnSc1	Werner
Götz	Götz	k1gMnSc1	Götz
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
ježibaba	ježibaba	k1gFnSc1	ježibaba
<g/>
)	)	kIx)	)
Gwynn	Gwynn	k1gMnSc1	Gwynn
Cornell	Cornell	k1gMnSc1	Cornell
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
cizí	cizit	k5eAaImIp3nS	cizit
<g />
.	.	kIx.	.
</s>
<s>
kněžna	kněžna	k1gFnSc1	kněžna
<g/>
)	)	kIx)	)
Hana	Hana	k1gFnSc1	Hana
Svobodová-Janků	Svobodová-Janek	k1gInPc2	Svobodová-Janek
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
lesní	lesní	k2eAgFnSc1d1	lesní
žínka	žínka	k1gFnSc1	žínka
<g/>
)	)	kIx)	)
Reingard	Reingard	k1gMnSc1	Reingard
Didusch	Didusch	k1gMnSc1	Didusch
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
lesní	lesní	k2eAgFnSc1d1	lesní
žínka	žínka	k1gFnSc1	žínka
<g/>
)	)	kIx)	)
Patricia	Patricius	k1gMnSc2	Patricius
Parker	Parker	k1gInSc4	Parker
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
lesní	lesní	k2eAgFnSc1d1	lesní
žínka	žínka	k1gFnSc1	žínka
<g/>
)	)	kIx)	)
Keiko	Keiko	k1gNnSc1	Keiko
Yano	Yano	k6eAd1	Yano
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
hajný	hajný	k1gMnSc1	hajný
<g/>
)	)	kIx)	)
Robert	Robert	k1gMnSc1	Robert
Hoyem	Hoyem	k1gInSc4	Hoyem
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
kuchtík	kuchtík	k1gMnSc1	kuchtík
<g/>
)	)	kIx)	)
Brigitte	Brigitte	k1gFnSc1	Brigitte
Dürrier	Dürrier	k1gInSc1	Dürrier
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
lovec	lovec	k1gMnSc1	lovec
<g/>
)	)	kIx)	)
Peter-Christoph	Peter-Christoph	k1gMnSc1	Peter-Christoph
Runge	Rung	k1gFnSc2	Rung
<g/>
.	.	kIx.	.
</s>
<s>
Düsseldorfer	Düsseldorfer	k1gMnSc1	Düsseldorfer
Symphoniker	Symphoniker	k1gMnSc1	Symphoniker
a	a	k8xC	a
Chor	Chor	k1gMnSc1	Chor
der	drát	k5eAaImRp2nS	drát
Deutschen	Deutschna	k1gFnPc2	Deutschna
Oper	opera	k1gFnPc2	opera
am	am	k?	am
Rhein	Rhein	k1gMnSc1	Rhein
řídí	řídit	k5eAaImIp3nS	řídit
Peter	Peter	k1gMnSc1	Peter
Schneider	Schneider	k1gMnSc1	Schneider
(	(	kIx(	(
<g/>
živá	živý	k2eAgFnSc1d1	živá
nahrávka	nahrávka	k1gFnSc1	nahrávka
<g/>
,	,	kIx,	,
v	v	k7c6	v
němčině	němčina	k1gFnSc6	němčina
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1976	[number]	k4	1976
(	(	kIx(	(
<g/>
Opera	opera	k1gFnSc1	opera
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Oro	Oro	k1gMnSc1	Oro
<g/>
/	/	kIx~	/
<g/>
Bella	Bella	k1gMnSc1	Bella
Voce	Voc	k1gFnSc2	Voc
BLV	BLV	kA	BLV
107.237	[number]	k4	107.237
<g/>
)	)	kIx)	)
Zpívají	zpívat	k5eAaImIp3nP	zpívat
<g/>
:	:	kIx,	:
(	(	kIx(	(
<g/>
Rusalka	rusalka	k1gFnSc1	rusalka
<g/>
)	)	kIx)	)
Teresa	Teresa	k1gFnSc1	Teresa
Stratas	Stratas	k1gInSc1	Stratas
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
vodník	vodník	k1gMnSc1	vodník
<g/>
)	)	kIx)	)
Wilard	Wilard	k1gMnSc1	Wilard
White	Whit	k1gInSc5	Whit
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
princ	princ	k1gMnSc1	princ
<g/>
)	)	kIx)	)
Ivo	Ivo	k1gMnSc1	Ivo
Žídek	Žídek	k1gMnSc1	Žídek
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
ježibaba	ježibaba	k1gFnSc1	ježibaba
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Gwendolyn	Gwendolyn	k1gMnSc1	Gwendolyn
Killebrew	Killebrew	k1gMnSc1	Killebrew
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
cizí	cizí	k2eAgFnSc1d1	cizí
kněžna	kněžna	k1gFnSc1	kněžna
<g/>
)	)	kIx)	)
Gwendolyn	Gwendolyn	k1gMnSc1	Gwendolyn
Killebrew	Killebrew	k1gMnSc1	Killebrew
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
lesní	lesní	k2eAgFnSc1d1	lesní
žínka	žínka	k1gFnSc1	žínka
<g/>
)	)	kIx)	)
Ans	Ans	k1gFnSc1	Ans
Philippo	Philippa	k1gFnSc5	Philippa
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
lesní	lesní	k2eAgFnSc1d1	lesní
žínka	žínka	k1gFnSc1	žínka
<g/>
)	)	kIx)	)
Roberta	Roberta	k1gFnSc1	Roberta
Alxander	Alxander	k1gInSc1	Alxander
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
lesní	lesní	k2eAgFnSc1d1	lesní
žínka	žínka	k1gFnSc1	žínka
<g/>
)	)	kIx)	)
Marianne	Mariann	k1gInSc5	Mariann
Dieleman	Dieleman	k1gMnSc1	Dieleman
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
hajný	hajný	k1gMnSc1	hajný
<g/>
)	)	kIx)	)
Nico	Nico	k1gMnSc1	Nico
Boer	Boer	k1gMnSc1	Boer
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
kuchtík	kuchtík	k1gMnSc1	kuchtík
<g/>
)	)	kIx)	)
Angela	angel	k1gMnSc2	angel
Bello	Bello	k1gNnSc4	Bello
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
lovec	lovec	k1gMnSc1	lovec
<g/>
)	)	kIx)	)
Fons	Fons	k1gInSc1	Fons
van	vana	k1gFnPc2	vana
Zijl	Zijla	k1gFnPc2	Zijla
<g/>
.	.	kIx.	.
</s>
<s>
Het	Het	k?	Het
Omroeporkest	Omroeporkest	k1gMnSc1	Omroeporkest
a	a	k8xC	a
Het	Het	k1gMnSc1	Het
Nederlands	Nederlandsa	k1gFnPc2	Nederlandsa
Operakoor	Operakoor	k1gMnSc1	Operakoor
řídí	řídit	k5eAaImIp3nS	řídit
Bohumil	Bohumil	k1gMnSc1	Bohumil
Gregor	Gregor	k1gMnSc1	Gregor
(	(	kIx(	(
<g/>
živá	živý	k2eAgFnSc1d1	živá
nahrávka	nahrávka	k1gFnSc1	nahrávka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1982-83	[number]	k4	1982-83
(	(	kIx(	(
<g/>
Supraphon	supraphon	k1gInSc1	supraphon
<g/>
,	,	kIx,	,
LP	LP	kA	LP
1984	[number]	k4	1984
<g/>
,	,	kIx,	,
10	[number]	k4	10
3641-2	[number]	k4	3641-2
633	[number]	k4	633
<g/>
,	,	kIx,	,
CD	CD	kA	CD
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
SU	SU	k?	SU
3718-2	[number]	k4	3718-2
633	[number]	k4	633
<g/>
;	;	kIx,	;
Eurodisc	Eurodisc	k1gFnSc1	Eurodisc
880	[number]	k4	880
114	[number]	k4	114
<g/>
-	-	kIx~	-
<g/>
915	[number]	k4	915
<g/>
)	)	kIx)	)
Zpívají	zpívat	k5eAaImIp3nP	zpívat
<g/>
:	:	kIx,	:
(	(	kIx(	(
<g/>
Rusalka	rusalka	k1gFnSc1	rusalka
<g/>
)	)	kIx)	)
Gabriela	Gabriela	k1gFnSc1	Gabriela
Beňačková-Čápová	Beňačková-Čápová	k1gFnSc1	Beňačková-Čápová
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
vodník	vodník	k1gMnSc1	vodník
<g/>
)	)	kIx)	)
Richard	Richard	k1gMnSc1	Richard
<g />
.	.	kIx.	.
</s>
<s>
Novák	Novák	k1gMnSc1	Novák
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
princ	princ	k1gMnSc1	princ
<g/>
)	)	kIx)	)
Wiesław	Wiesław	k1gMnSc1	Wiesław
Ochman	Ochman	k1gMnSc1	Ochman
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
ježibaba	ježibaba	k1gFnSc1	ježibaba
<g/>
)	)	kIx)	)
Věra	Věra	k1gFnSc1	Věra
Soukupová	Soukupová	k1gFnSc1	Soukupová
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
cizí	cizí	k2eAgFnSc1d1	cizí
kněžna	kněžna	k1gFnSc1	kněžna
<g/>
)	)	kIx)	)
Drahomíra	Drahomíra	k1gFnSc1	Drahomíra
Drobková	Drobkový	k2eAgFnSc1d1	Drobková
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
lesní	lesní	k2eAgFnSc1d1	lesní
žínka	žínka	k1gFnSc1	žínka
<g/>
)	)	kIx)	)
Jana	Jana	k1gFnSc1	Jana
Jonášová	Jonášová	k1gFnSc1	Jonášová
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
lesní	lesní	k2eAgFnSc1d1	lesní
žínka	žínka	k1gFnSc1	žínka
<g/>
)	)	kIx)	)
Daniela	Daniela	k1gFnSc1	Daniela
Šounová-Brouková	Šounová-Broukový	k2eAgFnSc1d1	Šounová-Broukový
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
lesní	lesní	k2eAgFnSc1d1	lesní
žínka	žínka	k1gFnSc1	žínka
<g/>
)	)	kIx)	)
Anna	Anna	k1gFnSc1	Anna
Barová	barový	k2eAgFnSc1d1	barová
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
hajný	hajný	k1gMnSc1	hajný
<g/>
)	)	kIx)	)
René	René	k1gMnSc1	René
Tuček	Tuček	k1gMnSc1	Tuček
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
kuchtík	kuchtík	k1gMnSc1	kuchtík
<g/>
)	)	kIx)	)
Jiřina	Jiřina	k1gFnSc1	Jiřina
Marková	Marková	k1gFnSc1	Marková
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
lovec	lovec	k1gMnSc1	lovec
<g/>
)	)	kIx)	)
René	René	k1gMnSc1	René
Tuček	Tuček	k1gMnSc1	Tuček
<g/>
.	.	kIx.	.
</s>
<s>
Českou	český	k2eAgFnSc4d1	Česká
filharmonii	filharmonie	k1gFnSc4	filharmonie
a	a	k8xC	a
Pražský	pražský	k2eAgInSc4d1	pražský
filharmonický	filharmonický	k2eAgInSc4d1	filharmonický
sbor	sbor	k1gInSc4	sbor
řídí	řídit	k5eAaImIp3nS	řídit
Václav	Václav	k1gMnSc1	Václav
Neumann	Neumann	k1gMnSc1	Neumann
</s>
</p>
<p>
<s>
1987	[number]	k4	1987
(	(	kIx(	(
<g/>
Orfeo	Orfeo	k6eAd1	Orfeo
C	C	kA	C
638	[number]	k4	638
042	[number]	k4	042
I	I	kA	I
<g/>
)	)	kIx)	)
Zpívají	zpívat	k5eAaImIp3nP	zpívat
<g/>
:	:	kIx,	:
(	(	kIx(	(
<g/>
Rusalka	rusalka	k1gFnSc1	rusalka
<g/>
)	)	kIx)	)
Gabriela	Gabriela	k1gFnSc1	Gabriela
Beňačková-Čápová	Beňačková-Čápová	k1gFnSc1	Beňačková-Čápová
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
vodník	vodník	k1gMnSc1	vodník
<g/>
)	)	kIx)	)
Jevgenij	Jevgenij	k1gMnSc1	Jevgenij
Nesterenko	Nesterenka	k1gFnSc5	Nesterenka
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
princ	princ	k1gMnSc1	princ
<g/>
)	)	kIx)	)
Peter	Peter	k1gMnSc1	Peter
Dvorský	Dvorský	k1gMnSc1	Dvorský
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
ježibaba	ježibaba	k1gFnSc1	ježibaba
<g/>
)	)	kIx)	)
Eva	Eva	k1gFnSc1	Eva
Randová	Randová	k1gFnSc1	Randová
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
cizí	cizí	k2eAgFnSc1d1	cizí
kněžna	kněžna	k1gFnSc1	kněžna
<g/>
)	)	kIx)	)
Eva	Eva	k1gFnSc1	Eva
Randová	Randová	k1gFnSc1	Randová
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
lesní	lesní	k2eAgFnSc1d1	lesní
žínka	žínka	k1gFnSc1	žínka
<g/>
)	)	kIx)	)
Margaretha	Margaretha	k1gMnSc1	Margaretha
Hintermeier	Hintermeier	k1gMnSc1	Hintermeier
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
lesní	lesní	k2eAgFnSc1d1	lesní
žínka	žínka	k1gFnSc1	žínka
<g/>
)	)	kIx)	)
Noriko	Norika	k1gFnSc5	Norika
Sasaki	Sasak	k1gFnSc5	Sasak
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
lesní	lesní	k2eAgFnSc1d1	lesní
žínka	žínka	k1gFnSc1	žínka
<g/>
)	)	kIx)	)
Gabriele	Gabriela	k1gFnSc6	Gabriela
Sima	sima	k1gNnSc4	sima
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
lovec	lovec	k1gMnSc1	lovec
<g/>
)	)	kIx)	)
Alexander	Alexandra	k1gFnPc2	Alexandra
Malý	malý	k2eAgMnSc1d1	malý
<g/>
.	.	kIx.	.
</s>
<s>
Orchester	orchestra	k1gFnPc2	orchestra
und	und	k?	und
Chor	Chor	k1gMnSc1	Chor
der	drát	k5eAaImRp2nS	drát
Wiener	Wiener	k1gMnSc1	Wiener
Staatsoper	Staatsoper	k1gMnSc1	Staatsoper
řídí	řídit	k5eAaImIp3nS	řídit
Václav	Václav	k1gMnSc1	Václav
Neumann	Neumann	k1gMnSc1	Neumann
(	(	kIx(	(
<g/>
živá	živý	k2eAgFnSc1d1	živá
nahrávka	nahrávka	k1gFnSc1	nahrávka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1997	[number]	k4	1997
(	(	kIx(	(
<g/>
Koch	Koch	k1gMnSc1	Koch
920473	[number]	k4	920473
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
;	;	kIx,	;
Brilliant	Brilliant	k1gInSc1	Brilliant
Classics	Classics	k1gInSc1	Classics
<g/>
/	/	kIx~	/
<g/>
Discover	Discover	k1gInSc1	Discover
<g/>
)	)	kIx)	)
Zpívají	zpívat	k5eAaImIp3nP	zpívat
<g/>
:	:	kIx,	:
(	(	kIx(	(
<g/>
Rusalka	rusalka	k1gFnSc1	rusalka
<g/>
)	)	kIx)	)
Ursula	Ursula	k1gFnSc1	Ursula
Füri-Bernhard	Füri-Bernhard	k1gInSc1	Füri-Bernhard
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
vodník	vodník	k1gMnSc1	vodník
<g/>
)	)	kIx)	)
Marcello	Marcello	k1gNnSc1	Marcello
Rosca	Rosc	k1gInSc2	Rosc
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
princ	princ	k1gMnSc1	princ
<g/>
)	)	kIx)	)
Walter	Walter	k1gMnSc1	Walter
Coppola	Coppola	k1gFnSc1	Coppola
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
ježibaba	ježibaba	k1gFnSc1	ježibaba
<g/>
)	)	kIx)	)
Nelly	Nella	k1gFnPc1	Nella
Boschkowa	Boschkow	k2eAgFnSc1d1	Boschkow
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
cizí	cizí	k2eAgFnSc1d1	cizí
kněžna	kněžna	k1gFnSc1	kněžna
<g/>
)	)	kIx)	)
Tiziana	Tiziana	k1gFnSc1	Tiziana
Sojat	Sojat	k1gInSc1	Sojat
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
lesní	lesní	k2eAgFnSc1d1	lesní
žínka	žínka	k1gFnSc1	žínka
<g/>
)	)	kIx)	)
Tamara	Tamara	k1gFnSc1	Tamara
Felbinger	Felbinger	k1gInSc1	Felbinger
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
lesní	lesní	k2eAgFnSc1d1	lesní
žínka	žínka	k1gFnSc1	žínka
<g/>
)	)	kIx)	)
Vesna	Vesna	k1gFnSc1	Vesna
Odoran	Odoran	k1gInSc1	Odoran
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
lesní	lesní	k2eAgFnSc1d1	lesní
žínka	žínka	k1gFnSc1	žínka
<g/>
)	)	kIx)	)
Martina	Martina	k1gFnSc1	Martina
Gojceta	Gojceta	k1gFnSc1	Gojceta
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
hajný	hajný	k1gMnSc1	hajný
<g/>
)	)	kIx)	)
Zeljco	Zeljco	k1gMnSc1	Zeljco
Grofelnik	Grofelnik	k1gMnSc1	Grofelnik
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
kuchtík	kuchtík	k1gMnSc1	kuchtík
<g/>
)	)	kIx)	)
Martina	Martin	k1gMnSc2	Martin
Zadro	Zadro	k1gNnSc4	Zadro
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
lovec	lovec	k1gMnSc1	lovec
<g/>
)	)	kIx)	)
Vitomir	Vitomir	k1gMnSc1	Vitomir
Marof	Marof	k1gMnSc1	Marof
<g/>
.	.	kIx.	.
</s>
<s>
Záhřebskou	záhřebský	k2eAgFnSc4d1	Záhřebská
filharmonii	filharmonie	k1gFnSc4	filharmonie
a	a	k8xC	a
Akademický	akademický	k2eAgInSc1d1	akademický
sbor	sbor	k1gInSc1	sbor
"	"	kIx"	"
<g/>
Ivan	Ivan	k1gMnSc1	Ivan
Goran	Goran	k1gMnSc1	Goran
Kovačić	Kovačić	k1gMnSc1	Kovačić
<g/>
"	"	kIx"	"
řídí	řídit	k5eAaImIp3nS	řídit
Alexander	Alexandra	k1gFnPc2	Alexandra
Rahbari	Rahbar	k1gFnSc2	Rahbar
</s>
</p>
<p>
<s>
1998	[number]	k4	1998
(	(	kIx(	(
<g/>
Decca	Decc	k1gInSc2	Decc
460	[number]	k4	460
568	[number]	k4	568
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
Zpívají	zpívat	k5eAaImIp3nP	zpívat
<g/>
:	:	kIx,	:
(	(	kIx(	(
<g/>
Rusalka	rusalka	k1gFnSc1	rusalka
<g/>
)	)	kIx)	)
Renée	René	k1gMnPc4	René
Fleming	Fleming	k1gInSc4	Fleming
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
vodník	vodník	k1gMnSc1	vodník
<g/>
)	)	kIx)	)
Franz	Franz	k1gMnSc1	Franz
Hawlata	Hawle	k1gNnPc4	Hawle
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
princ	princ	k1gMnSc1	princ
<g/>
)	)	kIx)	)
Ben	Ben	k1gInSc1	Ben
Heppner	Heppnra	k1gFnPc2	Heppnra
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
ježibaba	ježibaba	k1gFnSc1	ježibaba
<g/>
)	)	kIx)	)
Dolora	Dolora	k1gFnSc1	Dolora
Zajick	Zajick	k1gInSc1	Zajick
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
cizí	cizí	k2eAgFnSc1d1	cizí
kněžna	kněžna	k1gFnSc1	kněžna
<g/>
)	)	kIx)	)
Eva	Eva	k1gFnSc1	Eva
Urbanová	Urbanová	k1gFnSc1	Urbanová
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
lesní	lesní	k2eAgFnSc1d1	lesní
žínka	žínka	k1gFnSc1	žínka
<g/>
)	)	kIx)	)
Lívia	Lívia	k1gFnSc1	Lívia
Ághová	Ághová	k1gFnSc1	Ághová
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
lesní	lesní	k2eAgFnSc1d1	lesní
žínka	žínka	k1gFnSc1	žínka
<g/>
)	)	kIx)	)
Dana	Dana	k1gFnSc1	Dana
Burešová	Burešová	k1gFnSc1	Burešová
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
lesní	lesní	k2eAgFnSc1d1	lesní
žínka	žínka	k1gFnSc1	žínka
<g/>
)	)	kIx)	)
Hana	Hana	k1gFnSc1	Hana
Minutillo	Minutillo	k1gNnSc1	Minutillo
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
hajný	hajný	k1gMnSc1	hajný
<g/>
)	)	kIx)	)
Ivan	Ivan	k1gMnSc1	Ivan
Kusnjer	Kusnjer	k1gMnSc1	Kusnjer
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
kuchtík	kuchtík	k1gMnSc1	kuchtík
<g/>
)	)	kIx)	)
Zdena	Zdena	k1gFnSc1	Zdena
Kloubová	kloubový	k2eAgFnSc1d1	kloubová
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
lovec	lovec	k1gMnSc1	lovec
<g/>
)	)	kIx)	)
Ivan	Ivan	k1gMnSc1	Ivan
Kusnjer	Kusnjer	k1gMnSc1	Kusnjer
<g/>
.	.	kIx.	.
</s>
<s>
Českou	český	k2eAgFnSc4d1	Česká
filharmonii	filharmonie	k1gFnSc4	filharmonie
a	a	k8xC	a
Kühnův	Kühnův	k2eAgInSc4d1	Kühnův
smíšený	smíšený	k2eAgInSc4d1	smíšený
sbor	sbor	k1gInSc4	sbor
řídí	řídit	k5eAaImIp3nS	řídit
Charles	Charles	k1gMnSc1	Charles
Mackerras	Mackerras	k1gMnSc1	Mackerras
</s>
</p>
<p>
<s>
2007	[number]	k4	2007
(	(	kIx(	(
<g/>
Chandos	Chandos	k1gInSc1	Chandos
CHAN	CHAN	kA	CHAN
10449	[number]	k4	10449
<g/>
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
))	))	k?	))
Zpívají	zpívat	k5eAaImIp3nP	zpívat
<g/>
:	:	kIx,	:
(	(	kIx(	(
<g/>
Rusalka	rusalka	k1gFnSc1	rusalka
<g/>
)	)	kIx)	)
Cheryl	Cheryl	k1gInSc1	Cheryl
Barker	Barkra	k1gFnPc2	Barkra
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
vodník	vodník	k1gMnSc1	vodník
<g/>
)	)	kIx)	)
Bruce	Bruce	k1gMnSc1	Bruce
Martin	Martin	k1gMnSc1	Martin
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
princ	princ	k1gMnSc1	princ
<g/>
)	)	kIx)	)
Rosario	Rosario	k1gMnSc1	Rosario
La	la	k1gNnSc2	la
Spina	spina	k1gFnSc1	spina
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
ježibaba	ježibaba	k1gFnSc1	ježibaba
<g/>
)	)	kIx)	)
Anne-Marie	Anne-Marie	k1gFnSc1	Anne-Marie
Owens	Owensa	k1gFnPc2	Owensa
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
(	(	kIx(	(
<g/>
cizí	cizí	k2eAgFnSc1d1	cizí
kněžna	kněžna	k1gFnSc1	kněžna
<g/>
)	)	kIx)	)
Elizabeth	Elizabeth	k1gFnSc1	Elizabeth
Whitehouse	Whitehouse	k1gFnSc1	Whitehouse
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
lesní	lesní	k2eAgFnSc1d1	lesní
žínka	žínka	k1gFnSc1	žínka
<g/>
)	)	kIx)	)
Sarah	Sarah	k1gFnSc1	Sarah
Crane	Cran	k1gInSc5	Cran
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
lesní	lesní	k2eAgFnSc1d1	lesní
žínka	žínka	k1gFnSc1	žínka
<g/>
)	)	kIx)	)
Taryn	Taryn	k1gMnSc1	Taryn
Fiebig	Fiebig	k1gMnSc1	Fiebig
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
lesní	lesní	k2eAgFnSc1d1	lesní
žínka	žínka	k1gFnSc1	žínka
<g/>
)	)	kIx)	)
Dominica	Dominica	k1gFnSc1	Dominica
Matthews	Matthews	k1gInSc1	Matthews
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
hajný	hajný	k1gMnSc1	hajný
<g/>
)	)	kIx)	)
Barry	Barra	k1gFnPc1	Barra
Ryan	Ryano	k1gNnPc2	Ryano
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
kuchtík	kuchtík	k1gMnSc1	kuchtík
<g/>
)	)	kIx)	)
Sian	Sian	k1gMnSc1	Sian
Pendry	Pendr	k1gInPc4	Pendr
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
lovec	lovec	k1gMnSc1	lovec
<g/>
)	)	kIx)	)
Barry	Barr	k1gInPc1	Barr
Ryan	Ryana	k1gFnPc2	Ryana
<g/>
.	.	kIx.	.
</s>
<s>
Australian	Australian	k1gInSc1	Australian
Opera	opera	k1gFnSc1	opera
and	and	k?	and
Ballet	Ballet	k1gInSc1	Ballet
Orchestra	orchestra	k1gFnSc1	orchestra
a	a	k8xC	a
Australian	Australian	k1gInSc4	Australian
Opera	opera	k1gFnSc1	opera
Chorus	chorus	k1gInSc1	chorus
řídí	řídit	k5eAaImIp3nS	řídit
Richard	Richard	k1gMnSc1	Richard
Hickox	Hickox	k1gInSc1	Hickox
</s>
</p>
<p>
<s>
2009	[number]	k4	2009
(	(	kIx(	(
<g/>
Glyndebourne	Glyndebourn	k1gInSc5	Glyndebourn
GFOCD	GFOCD	kA	GFOCD
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
)	)	kIx)	)
Zpívají	zpívat	k5eAaImIp3nP	zpívat
<g/>
:	:	kIx,	:
(	(	kIx(	(
<g/>
Rusalka	rusalka	k1gFnSc1	rusalka
<g/>
)	)	kIx)	)
Ana	Ana	k1gMnSc1	Ana
María	María	k1gMnSc1	María
Martínez	Martínez	k1gMnSc1	Martínez
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
vodník	vodník	k1gMnSc1	vodník
<g/>
)	)	kIx)	)
Mischa	Mischa	k1gMnSc1	Mischa
Schelomianski	Schelomiansk	k1gFnSc2	Schelomiansk
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
princ	princ	k1gMnSc1	princ
<g/>
)	)	kIx)	)
Brandon	Brandon	k1gMnSc1	Brandon
Jovanovich	Jovanovich	k1gMnSc1	Jovanovich
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
ježibaba	ježibaba	k1gFnSc1	ježibaba
<g/>
)	)	kIx)	)
Larissa	Larissa	k1gFnSc1	Larissa
Diadkova	Diadkův	k2eAgFnSc1d1	Diadkův
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
cizí	cizí	k2eAgFnSc1d1	cizí
kněžna	kněžna	k1gFnSc1	kněžna
<g/>
)	)	kIx)	)
Tatiana	Tatiana	k1gFnSc1	Tatiana
Pavlovskaya	Pavlovskaya	k1gFnSc1	Pavlovskaya
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
lesní	lesní	k2eAgFnSc1d1	lesní
žínka	žínka	k1gFnSc1	žínka
<g/>
)	)	kIx)	)
Natasha	Natasha	k1gMnSc1	Natasha
Jouhl	Jouhl	k1gMnSc1	Jouhl
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
lesní	lesní	k2eAgFnSc1d1	lesní
žínka	žínka	k1gFnSc1	žínka
<g/>
)	)	kIx)	)
Barbara	Barbara	k1gFnSc1	Barbara
Senator	Senator	k1gInSc1	Senator
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
lesní	lesní	k2eAgFnSc1d1	lesní
žínka	žínka	k1gFnSc1	žínka
<g/>
)	)	kIx)	)
Élodie	Élodie	k1gFnSc1	Élodie
Méchain	Méchain	k1gInSc1	Méchain
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
hajný	hajný	k1gMnSc1	hajný
<g/>
)	)	kIx)	)
Alasdair	Alasdair	k1gMnSc1	Alasdair
Elliott	Elliott	k1gMnSc1	Elliott
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
kuchtík	kuchtík	k1gMnSc1	kuchtík
<g/>
)	)	kIx)	)
Diana	Diana	k1gFnSc1	Diana
Axentii	Axentie	k1gFnSc4	Axentie
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
lovec	lovec	k1gMnSc1	lovec
<g/>
)	)	kIx)	)
John	John	k1gMnSc1	John
Mackenzie	Mackenzie	k1gFnSc2	Mackenzie
<g/>
.	.	kIx.	.
</s>
<s>
London	London	k1gMnSc1	London
Philharmonic	Philharmonice	k1gFnPc2	Philharmonice
Orchestra	orchestra	k1gFnSc1	orchestra
a	a	k8xC	a
The	The	k1gFnSc1	The
Glyndebourne	Glyndebourn	k1gInSc5	Glyndebourn
Chorus	chorus	k1gInSc4	chorus
řídí	řídit	k5eAaImIp3nS	řídit
Jiří	Jiří	k1gMnSc1	Jiří
Bělohlávek	Bělohlávek	k1gMnSc1	Bělohlávek
</s>
</p>
<p>
<s>
2011	[number]	k4	2011
(	(	kIx(	(
<g/>
Orfeo	Orfeo	k6eAd1	Orfeo
<g/>
)	)	kIx)	)
Zpívají	zpívat	k5eAaImIp3nP	zpívat
<g/>
:	:	kIx,	:
(	(	kIx(	(
<g/>
Rusalka	rusalka	k1gFnSc1	rusalka
<g/>
)	)	kIx)	)
Camilla	Camilla	k1gMnSc1	Camilla
Nylund	Nylund	k1gMnSc1	Nylund
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
vodník	vodník	k1gMnSc1	vodník
<g/>
)	)	kIx)	)
Alan	Alan	k1gMnSc1	Alan
Held	Held	k1gMnSc1	Held
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
princ	princ	k1gMnSc1	princ
<g/>
)	)	kIx)	)
Piotr	Piotr	k1gMnSc1	Piotr
Beczala	Beczala	k1gMnSc1	Beczala
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
ježibaba	ježibaba	k1gFnSc1	ježibaba
<g/>
)	)	kIx)	)
Birgit	Birgit	k2eAgInSc1d1	Birgit
Remmert	Remmert	k1gInSc1	Remmert
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
cizí	cizí	k2eAgFnSc1d1	cizí
kněžna	kněžna	k1gFnSc1	kněžna
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Emily	Emil	k1gMnPc7	Emil
Magee	Mage	k1gInSc2	Mage
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
lesní	lesní	k2eAgFnSc1d1	lesní
žínka	žínka	k1gFnSc1	žínka
<g/>
)	)	kIx)	)
Anna	Anna	k1gFnSc1	Anna
Prohaska	Prohaska	k1gFnSc1	Prohaska
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
lesní	lesní	k2eAgFnSc1d1	lesní
žínka	žínka	k1gFnSc1	žínka
<g/>
)	)	kIx)	)
Stephanie	Stephanie	k1gFnSc1	Stephanie
Atanasov	Atanasov	k1gInSc1	Atanasov
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
lesní	lesní	k2eAgFnSc1d1	lesní
žínka	žínka	k1gFnSc1	žínka
<g/>
)	)	kIx)	)
Hannah	Hannah	k1gInSc1	Hannah
Esther	Esthra	k1gFnPc2	Esthra
Minutillo	Minutillo	k1gNnSc1	Minutillo
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
hajný	hajný	k1gMnSc1	hajný
<g/>
)	)	kIx)	)
Adam	Adam	k1gMnSc1	Adam
Plachetka	plachetka	k1gFnSc1	plachetka
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
kuchtík	kuchtík	k1gMnSc1	kuchtík
<g/>
)	)	kIx)	)
Eva	Eva	k1gFnSc1	Eva
Liebau	Liebaus	k1gInSc2	Liebaus
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
lovec	lovec	k1gMnSc1	lovec
<g/>
)	)	kIx)	)
Daniel	Daniel	k1gMnSc1	Daniel
Schmutzhard	Schmutzhard	k1gMnSc1	Schmutzhard
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Cleveland	Cleveland	k1gInSc1	Cleveland
Orchestra	orchestra	k1gFnSc1	orchestra
a	a	k8xC	a
Chor	Chor	k1gInSc1	Chor
der	drát	k5eAaImRp2nS	drát
Wiener	Wiener	k1gMnSc1	Wiener
Staatsoper	Staatsoper	k1gMnSc1	Staatsoper
řídí	řídit	k5eAaImIp3nS	řídit
Franz	Franz	k1gMnSc1	Franz
Welser-Möst	Welser-Möst	k1gMnSc1	Welser-Möst
(	(	kIx(	(
<g/>
živá	živý	k2eAgFnSc1d1	živá
nahrávka	nahrávka	k1gFnSc1	nahrávka
ze	z	k7c2	z
Salcburského	salcburský	k2eAgInSc2d1	salcburský
festivalu	festival	k1gInSc2	festival
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
DVD	DVD	kA	DVD
a	a	k8xC	a
Blu-ray	Blua	k2eAgInPc1d1	Blu-ra
záznamy	záznam	k1gInPc1	záznam
==	==	k?	==
</s>
</p>
<p>
<s>
1982	[number]	k4	1982
<g/>
,	,	kIx,	,
Opéra	Opéra	k1gFnSc1	Opéra
de	de	k?	de
Marseilles	Marseilles	k1gInSc1	Marseilles
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Jacques	Jacques	k1gMnSc1	Jacques
Karpo	Karpa	k1gFnSc5	Karpa
<g/>
,	,	kIx,	,
diriguje	dirigovat	k5eAaImIp3nS	dirigovat
János	János	k1gMnSc1	János
Fürst	Fürst	k1gMnSc1	Fürst
<g/>
,	,	kIx,	,
zpívají	zpívat	k5eAaImIp3nP	zpívat
(	(	kIx(	(
<g/>
Rusalka	rusalka	k1gFnSc1	rusalka
<g/>
)	)	kIx)	)
Lilian	Liliana	k1gFnPc2	Liliana
Sukis	Sukis	k1gInSc1	Sukis
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
vodník	vodník	k1gMnSc1	vodník
<g/>
)	)	kIx)	)
Dimiter	Dimiter	k1gInSc1	Dimiter
Petkov	Petkov	k1gInSc1	Petkov
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
princ	princ	k1gMnSc1	princ
<g/>
)	)	kIx)	)
Vincenzo	Vincenza	k1gFnSc5	Vincenza
Manno	Manna	k1gFnSc5	Manna
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
ježibaba	ježibaba	k1gFnSc1	ježibaba
<g/>
)	)	kIx)	)
Nuala	Nuala	k1gFnSc1	Nuala
Willis	Willis	k1gFnSc1	Willis
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
cizí	cizí	k2eAgFnSc1d1	cizí
kněžna	kněžna	k1gFnSc1	kněžna
<g/>
)	)	kIx)	)
Livia	Livia	k1gFnSc1	Livia
Budai	Buda	k1gFnSc2	Buda
</s>
</p>
<p>
<s>
1986	[number]	k4	1986
<g/>
,	,	kIx,	,
English	English	k1gMnSc1	English
National	National	k1gFnSc2	National
Opera	opera	k1gFnSc1	opera
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
David	David	k1gMnSc1	David
Pountney	Pountnea	k1gFnPc4	Pountnea
<g/>
,	,	kIx,	,
diriguje	dirigovat	k5eAaImIp3nS	dirigovat
Mark	Mark	k1gMnSc1	Mark
Elder	Elder	k1gMnSc1	Elder
<g/>
,	,	kIx,	,
zpívají	zpívat	k5eAaImIp3nP	zpívat
(	(	kIx(	(
<g/>
Rusalka	rusalka	k1gFnSc1	rusalka
<g/>
)	)	kIx)	)
Eilene	Eilen	k1gInSc5	Eilen
Hannan	Hannany	k1gInPc2	Hannany
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
vodník	vodník	k1gMnSc1	vodník
<g/>
)	)	kIx)	)
Rodney	Rodnea	k1gFnPc1	Rodnea
Macann	Macanno	k1gNnPc2	Macanno
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
princ	princ	k1gMnSc1	princ
<g/>
)	)	kIx)	)
John	John	k1gMnSc1	John
Treleaven	Treleaven	k2eAgMnSc1d1	Treleaven
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
ježibaba	ježibaba	k1gFnSc1	ježibaba
<g/>
)	)	kIx)	)
Ann	Ann	k1gMnSc1	Ann
Howard	Howard	k1gMnSc1	Howard
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
cizí	cizí	k2eAgFnSc1d1	cizí
kněžna	kněžna	k1gFnSc1	kněžna
<g/>
)	)	kIx)	)
Phyllis	Phyllis	k1gFnSc1	Phyllis
Cannan	Cannan	k1gMnSc1	Cannan
(	(	kIx(	(
<g/>
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
Opéra	Opéra	k1gMnSc1	Opéra
national	nationat	k5eAaPmAgMnS	nationat
de	de	k?	de
Paris	Paris	k1gMnSc1	Paris
(	(	kIx(	(
<g/>
Bastille	Bastille	k1gInSc1	Bastille
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Robert	Robert	k1gMnSc1	Robert
Carsen	Carsen	k1gInSc1	Carsen
<g/>
,	,	kIx,	,
diriguje	dirigovat	k5eAaImIp3nS	dirigovat
James	James	k1gMnSc1	James
Conlon	Conlon	k1gInSc1	Conlon
<g/>
,	,	kIx,	,
zpívají	zpívat	k5eAaImIp3nP	zpívat
(	(	kIx(	(
<g/>
Rusalka	rusalka	k1gFnSc1	rusalka
<g/>
)	)	kIx)	)
Renée	René	k1gMnPc4	René
Flemingová	Flemingový	k2eAgFnSc1d1	Flemingová
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
vodník	vodník	k1gMnSc1	vodník
<g/>
)	)	kIx)	)
Franz	Franz	k1gMnSc1	Franz
Hawlata	Hawle	k1gNnPc4	Hawle
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
princ	princ	k1gMnSc1	princ
<g/>
)	)	kIx)	)
Sergej	Sergej	k1gMnSc1	Sergej
Larin	Larin	k1gMnSc1	Larin
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
ježibaba	ježibaba	k1gFnSc1	ježibaba
<g/>
)	)	kIx)	)
Larissa	Larissa	k1gFnSc1	Larissa
Diadkova	Diadkův	k2eAgFnSc1d1	Diadkův
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
cizí	cizí	k2eAgFnSc1d1	cizí
kněžna	kněžna	k1gFnSc1	kněžna
<g/>
)	)	kIx)	)
Eva	Eva	k1gFnSc1	Eva
Urbanová	Urbanová	k1gFnSc1	Urbanová
</s>
</p>
<p>
<s>
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
také	také	k9	také
Blu-ray	Blua	k2eAgFnPc1d1	Blu-ra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bayerische	Bayerische	k1gFnSc1	Bayerische
Staatsoper	Staatsoper	k1gMnSc1	Staatsoper
<g/>
,	,	kIx,	,
dirigent	dirigent	k1gMnSc1	dirigent
Tomáš	Tomáš	k1gMnSc1	Tomáš
Hanus	Hanus	k1gMnSc1	Hanus
<g/>
,	,	kIx,	,
zpívají	zpívat	k5eAaImIp3nP	zpívat
(	(	kIx(	(
<g/>
Rusalka	rusalka	k1gFnSc1	rusalka
<g/>
)	)	kIx)	)
Kristine	Kristin	k1gInSc5	Kristin
Opolais	Opolais	k1gFnSc5	Opolais
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
vodník	vodník	k1gMnSc1	vodník
<g/>
)	)	kIx)	)
Günther	Günthra	k1gFnPc2	Günthra
Groissböck	Groissböck	k1gInSc1	Groissböck
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
princ	princ	k1gMnSc1	princ
<g/>
)	)	kIx)	)
Klaus	Klaus	k1gMnSc1	Klaus
Vogt	Vogt	k1gMnSc1	Vogt
<g/>
,	,	kIx,	,
<g/>
(	(	kIx(	(
<g/>
ježibaba	ježibaba	k1gFnSc1	ježibaba
<g/>
)	)	kIx)	)
Janina	Janin	k2eAgFnSc1d1	Janina
Baechle	Baechle	k1gFnSc1	Baechle
<g/>
,	,	kIx,	,
<g/>
(	(	kIx(	(
<g/>
cizí	cizí	k2eAgFnSc1d1	cizí
kněžna	kněžna	k1gFnSc1	kněžna
<g/>
)	)	kIx)	)
Nadia	Nadia	k1gFnSc1	Nadia
Krasteva	Krasteva	k1gFnSc1	Krasteva
(	(	kIx(	(
<g/>
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
také	také	k9	také
Blu-ray	Blua	k2eAgFnPc1d1	Blu-ra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Metropolitan	metropolitan	k1gInSc1	metropolitan
Opera	opera	k1gFnSc1	opera
<g/>
,	,	kIx,	,
dirigent	dirigent	k1gMnSc1	dirigent
Yannick	Yannick	k1gMnSc1	Yannick
Nézet-Séguin	Nézet-Séguin	k1gMnSc1	Nézet-Séguin
<g/>
,	,	kIx,	,
zpívají	zpívat	k5eAaImIp3nP	zpívat
(	(	kIx(	(
<g/>
Rusalka	rusalka	k1gFnSc1	rusalka
<g/>
)	)	kIx)	)
Renée	René	k1gMnPc4	René
Fleming	Fleming	k1gInSc4	Fleming
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
vodník	vodník	k1gMnSc1	vodník
<g/>
)	)	kIx)	)
John	John	k1gMnSc1	John
Relyea	Relyea	k1gMnSc1	Relyea
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
princ	princ	k1gMnSc1	princ
<g/>
)	)	kIx)	)
Piotr	Piotr	k1gMnSc1	Piotr
Beczala	Beczala	k1gMnSc1	Beczala
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Ježibaba	ježibaba	k1gFnSc1	ježibaba
<g/>
)	)	kIx)	)
Dolora	Dolora	k1gFnSc1	Dolora
Zajick	Zajick	k1gInSc1	Zajick
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
hajný	hajný	k1gMnSc1	hajný
<g/>
)	)	kIx)	)
Vladimír	Vladimír	k1gMnSc1	Vladimír
Chmelo	Chmela	k1gFnSc5	Chmela
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
kněžna	kněžna	k1gFnSc1	kněžna
<g/>
)	)	kIx)	)
Emily	Emil	k1gMnPc7	Emil
Magée	Magé	k1gFnSc2	Magé
(	(	kIx(	(
<g/>
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Filmy	film	k1gInPc4	film
==	==	k?	==
</s>
</p>
<p>
<s>
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
Filmové	filmový	k2eAgNnSc1d1	filmové
studio	studio	k1gNnSc1	studio
Barrandov	Barrandov	k1gInSc1	Barrandov
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Václav	Václav	k1gMnSc1	Václav
Kašlík	kašlík	k1gInSc1	kašlík
<g/>
,	,	kIx,	,
hrají	hrát	k5eAaImIp3nP	hrát
Rusalka	rusalka	k1gFnSc1	rusalka
–	–	k?	–
Jana	Jana	k1gFnSc1	Jana
Andrsová	Andrsová	k1gFnSc1	Andrsová
(	(	kIx(	(
<g/>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Milada	Milada	k1gFnSc1	Milada
Šubrtová	Šubrtová	k1gFnSc1	Šubrtová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vodník	vodník	k1gMnSc1	vodník
–	–	k?	–
Josef	Josef	k1gMnSc1	Josef
Hlinomaz	hlinomaz	k1gMnSc1	hlinomaz
(	(	kIx(	(
<g/>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Ondrej	Ondrej	k1gMnSc1	Ondrej
Malachovský	Malachovský	k2eAgMnSc1d1	Malachovský
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Princ	princ	k1gMnSc1	princ
–	–	k?	–
Vladimír	Vladimír	k1gMnSc1	Vladimír
Ráž	Ráž	k1gMnSc1	Ráž
(	(	kIx(	(
<g/>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Švehla	Švehla	k1gMnSc1	Švehla
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ježibaba	ježibaba	k1gFnSc1	ježibaba
–	–	k?	–
Jiřina	Jiřina	k1gFnSc1	Jiřina
Šejbalová	Šejbalová	k1gFnSc1	Šejbalová
(	(	kIx(	(
<g/>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Věra	Věra	k1gFnSc1	Věra
Soukupová	Soukupová	k1gFnSc1	Soukupová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Cizí	cizí	k2eAgFnSc1d1	cizí
kněžna	kněžna	k1gFnSc1	kněžna
–	–	k?	–
Ivana	Ivana	k1gFnSc1	Ivana
Mixová	Mixová	k1gFnSc1	Mixová
</s>
</p>
<p>
<s>
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
Československá	československý	k2eAgFnSc1d1	Československá
televize	televize	k1gFnSc1	televize
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Bohumil	Bohumil	k1gMnSc1	Bohumil
Zoul	Zoul	k1gMnSc1	Zoul
<g/>
,	,	kIx,	,
diriguje	dirigovat	k5eAaImIp3nS	dirigovat
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Chalabala	Chalabal	k1gMnSc2	Chalabal
<g/>
,	,	kIx,	,
hrají	hrát	k5eAaImIp3nP	hrát
Rusalka	rusalka	k1gFnSc1	rusalka
–	–	k?	–
Kateřina	Kateřina	k1gFnSc1	Kateřina
Macháčková	Macháčková	k1gFnSc1	Macháčková
(	(	kIx(	(
<g/>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Milada	Milada	k1gFnSc1	Milada
Šubrtová	Šubrtová	k1gFnSc1	Šubrtová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vodník	vodník	k1gMnSc1	vodník
–	–	k?	–
Eduard	Eduard	k1gMnSc1	Eduard
Haken	Haken	k1gMnSc1	Haken
<g/>
,	,	kIx,	,
Princ	princ	k1gMnSc1	princ
–	–	k?	–
Miroslav	Miroslav	k1gMnSc1	Miroslav
Nohýnek	Nohýnka	k1gFnPc2	Nohýnka
(	(	kIx(	(
<g/>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Ivo	Ivo	k1gMnSc1	Ivo
Žídek	Žídek	k1gMnSc1	Žídek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ježibaba	ježibaba	k1gFnSc1	ježibaba
–	–	k?	–
Slávka	Slávka	k1gFnSc1	Slávka
Budínová	Budínová	k1gFnSc1	Budínová
(	(	kIx(	(
<g/>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Marie	Marie	k1gFnSc1	Marie
Ovčačíková	Ovčačíková	k1gFnSc1	Ovčačíková
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Cizí	cizí	k2eAgFnSc1d1	cizí
kněžna	kněžna	k1gFnSc1	kněžna
–	–	k?	–
Marie	Marie	k1gFnSc1	Marie
Málková	Málková	k1gFnSc1	Málková
(	(	kIx(	(
<g/>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Alena	Alena	k1gFnSc1	Alena
Míková	Míková	k1gFnSc1	Míková
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1977	[number]	k4	1977
<g/>
,	,	kIx,	,
Zweites	Zweites	k1gInSc1	Zweites
Deutsches	Deutschesa	k1gFnPc2	Deutschesa
Fernsehen	Fernsehen	k1gInSc1	Fernsehen
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Petr	Petr	k1gMnSc1	Petr
Weigl	Weigl	k1gMnSc1	Weigl
<g/>
,	,	kIx,	,
Bayerisches	Bayerisches	k1gMnSc1	Bayerisches
Rundfunkorchester	Rundfunkorchester	k1gMnSc1	Rundfunkorchester
diriguje	dirigovat	k5eAaImIp3nS	dirigovat
Marek	Marek	k1gMnSc1	Marek
Janowski	Janowske	k1gFnSc4	Janowske
<g/>
,	,	kIx,	,
hrají	hrát	k5eAaImIp3nP	hrát
Rusalka	rusalka	k1gFnSc1	rusalka
–	–	k?	–
Magda	Magda	k1gFnSc1	Magda
Vašáryová	Vašáryová	k1gFnSc1	Vašáryová
(	(	kIx(	(
<g/>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Gabriela	Gabriela	k1gFnSc1	Gabriela
Beňačková-Čápová	Beňačková-Čápová	k1gFnSc1	Beňačková-Čápová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vodník	vodník	k1gMnSc1	vodník
–	–	k?	–
Přemysl	Přemysl	k1gMnSc1	Přemysl
Kočí	Kočí	k1gMnSc1	Kočí
(	(	kIx(	(
<g/>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Ondrej	Ondrej	k1gMnSc1	Ondrej
Malachovský	Malachovský	k2eAgMnSc1d1	Malachovský
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Princ	princ	k1gMnSc1	princ
–	–	k?	–
Milan	Milan	k1gMnSc1	Milan
Kňažko	Kňažka	k1gFnSc5	Kňažka
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Peter	Peter	k1gMnSc1	Peter
Dvorský	Dvorský	k1gMnSc1	Dvorský
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ježibaba	ježibaba	k1gFnSc1	ježibaba
–	–	k?	–
Jaroslava	Jaroslava	k1gFnSc1	Jaroslava
Adamová	Adamová	k1gFnSc1	Adamová
(	(	kIx(	(
<g/>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Libuše	Libuše	k1gFnSc1	Libuše
Márová	Márová	k1gFnSc1	Márová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Cizí	cizí	k2eAgFnSc1d1	cizí
kněžna	kněžna	k1gFnSc1	kněžna
–	–	k?	–
Božidara	Božidar	k1gMnSc2	Božidar
Turzonovová	Turzonovová	k1gFnSc1	Turzonovová
(	(	kIx(	(
<g/>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Milada	Milada	k1gFnSc1	Milada
Šubrtová	Šubrtová	k1gFnSc1	Šubrtová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hajný	hajný	k1gMnSc1	hajný
–	–	k?	–
Josef	Josef	k1gMnSc1	Josef
Karlík	Karlík	k1gMnSc1	Karlík
(	(	kIx(	(
<g/>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Stříška	stříška	k1gFnSc1	stříška
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kuchtík	kuchtík	k1gMnSc1	kuchtík
–	–	k?	–
Dagmar	Dagmar	k1gFnSc1	Dagmar
Veškrnová	Veškrnová	k1gFnSc1	Veškrnová
(	(	kIx(	(
<g/>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Daniela	Daniela	k1gFnSc1	Daniela
Šounová	Šounová	k1gFnSc1	Šounová
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1978	[number]	k4	1978
<g/>
,	,	kIx,	,
Zweites	Zweites	k1gInSc1	Zweites
Deutsches	Deutschesa	k1gFnPc2	Deutschesa
Fernsehen	Fernsehen	k1gInSc1	Fernsehen
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Petr	Petr	k1gMnSc1	Petr
Weigl	Weigl	k1gMnSc1	Weigl
<g/>
,	,	kIx,	,
Bayerisches	Bayerisches	k1gMnSc1	Bayerisches
Rundfunkorchester	Rundfunkorchester	k1gMnSc1	Rundfunkorchester
diriguje	dirigovat	k5eAaImIp3nS	dirigovat
Marek	Marek	k1gMnSc1	Marek
Janowski	Janowske	k1gFnSc4	Janowske
<g/>
,	,	kIx,	,
hrají	hrát	k5eAaImIp3nP	hrát
Rusalka	rusalka	k1gFnSc1	rusalka
–	–	k?	–
Magda	Magda	k1gFnSc1	Magda
Vašáryová	Vašáryová	k1gFnSc1	Vašáryová
(	(	kIx(	(
<g/>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Lilian	Liliana	k1gFnPc2	Liliana
Sukis	Sukis	k1gFnSc2	Sukis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vodník	vodník	k1gMnSc1	vodník
–	–	k?	–
Přemysl	Přemysl	k1gMnSc1	Přemysl
Kočí	Kočí	k1gMnSc1	Kočí
(	(	kIx(	(
<g/>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Theo	Thea	k1gFnSc5	Thea
Adam	Adam	k1gMnSc1	Adam
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Princ	princ	k1gMnSc1	princ
–	–	k?	–
Milan	Milan	k1gMnSc1	Milan
Kňažko	Kňažka	k1gFnSc5	Kňažka
(	(	kIx(	(
<g/>
zpívá	zpívat	k5eAaImIp3nS	zpívat
P.	P.	kA	P.
Hofmann	Hofmann	k1gInSc1	Hofmann
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ježibaba	ježibaba	k1gFnSc1	ježibaba
–	–	k?	–
Jaroslava	Jaroslava	k1gFnSc1	Jaroslava
Adamová	Adamová	k1gFnSc1	Adamová
(	(	kIx(	(
<g/>
zpívá	zpívat	k5eAaImIp3nS	zpívat
R.	R.	kA	R.
Wagemann	Wagemann	k1gInSc1	Wagemann
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Cizí	cizí	k2eAgFnSc1d1	cizí
kněžna	kněžna	k1gFnSc1	kněžna
–	–	k?	–
Božidara	Božidar	k1gMnSc2	Božidar
Turzonovová	Turzonovová	k1gFnSc1	Turzonovová
(	(	kIx(	(
<g/>
zpívá	zpívat	k5eAaImIp3nS	zpívat
J.	J.	kA	J.
Beckmann	Beckmann	k1gInSc1	Beckmann
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Poznámka	poznámka	k1gFnSc1	poznámka
==	==	k?	==
</s>
</p>
<p>
<s>
Motivy	motiv	k1gInPc1	motiv
zakleté	zakletý	k2eAgFnSc2d1	zakletá
víly	víla	k1gFnSc2	víla
<g/>
/	/	kIx~	/
<g/>
dívky	dívka	k1gFnSc2	dívka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nesmí	smět	k5eNaImIp3nS	smět
s	s	k7c7	s
princem	princ	k1gMnSc7	princ
<g/>
,	,	kIx,	,
do	do	k7c2	do
něhož	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
zamilovala	zamilovat	k5eAaPmAgNnP	zamilovat
promluvit	promluvit	k5eAaPmF	promluvit
<g/>
,	,	kIx,	,
můžeme	moct	k5eAaImIp1nP	moct
najít	najít	k5eAaPmF	najít
například	například	k6eAd1	například
v	v	k7c6	v
pohádce	pohádka	k1gFnSc6	pohádka
Malá	malý	k2eAgFnSc1d1	malá
mořská	mořský	k2eAgFnSc1d1	mořská
víla	víla	k1gFnSc1	víla
Hanse	Hans	k1gMnSc2	Hans
Christiana	Christian	k1gMnSc2	Christian
Andersena	Andersen	k1gMnSc2	Andersen
nebo	nebo	k8xC	nebo
v	v	k7c6	v
příběhu	příběh	k1gInSc6	příběh
zakleté	zakletý	k2eAgFnSc2d1	zakletá
princezny	princezna	k1gFnSc2	princezna
Odetty	Odetta	k1gFnSc2	Odetta
v	v	k7c6	v
pohádkovém	pohádkový	k2eAgInSc6d1	pohádkový
baletu	balet	k1gInSc6	balet
Labutí	labutí	k2eAgNnSc1d1	labutí
jezero	jezero	k1gNnSc1	jezero
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
HOSTOMSKÁ	HOSTOMSKÁ	kA	HOSTOMSKÁ
<g/>
,	,	kIx,	,
Anna	Anna	k1gFnSc1	Anna
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Opera	opera	k1gFnSc1	opera
–	–	k?	–
Průvodce	průvodka	k1gFnSc6	průvodka
operní	operní	k2eAgFnSc7d1	operní
tvorbou	tvorba	k1gFnSc7	tvorba
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
NS	NS	kA	NS
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
1466	[number]	k4	1466
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
205	[number]	k4	205
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
637	[number]	k4	637
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
716	[number]	k4	716
<g/>
–	–	k?	–
<g/>
718	[number]	k4	718
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
WARRACK	WARRACK	kA	WARRACK
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
<g/>
;	;	kIx,	;
WEST	WEST	kA	WEST
<g/>
,	,	kIx,	,
Ewan	Ewan	k1gInSc1	Ewan
<g/>
.	.	kIx.	.
</s>
<s>
Oxfordský	oxfordský	k2eAgInSc1d1	oxfordský
slovník	slovník	k1gInSc1	slovník
opery	opera	k1gFnSc2	opera
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Holba	holba	k1gFnSc1	holba
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
IRIS	iris	k1gInSc1	iris
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85893	[number]	k4	85893
<g/>
-	-	kIx~	-
<g/>
14	[number]	k4	14
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ŠÍP	Šíp	k1gMnSc1	Šíp
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
opera	opera	k1gFnSc1	opera
a	a	k8xC	a
její	její	k3xOp3gMnPc1	její
tvůrci	tvůrce	k1gMnPc1	tvůrce
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Supraphon	supraphon	k1gInSc1	supraphon
<g/>
,	,	kIx,	,
n.	n.	k?	n.
p.	p.	k?	p.
<g/>
,	,	kIx,	,
1983	[number]	k4	1983
<g/>
.	.	kIx.	.
400	[number]	k4	400
s.	s.	k?	s.
Kapitola	kapitola	k1gFnSc1	kapitola
Antonín	Antonín	k1gMnSc1	Antonín
Dvořák	Dvořák	k1gMnSc1	Dvořák
<g/>
,	,	kIx,	,
s.	s.	k?	s.
95	[number]	k4	95
<g/>
-	-	kIx~	-
<g/>
99	[number]	k4	99
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ŠOUREK	Šourek	k1gMnSc1	Šourek
<g/>
,	,	kIx,	,
Otakar	Otakar	k1gMnSc1	Otakar
<g/>
.	.	kIx.	.
</s>
<s>
Antonín	Antonín	k1gMnSc1	Antonín
Dvořák	Dvořák	k1gMnSc1	Dvořák
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
HUTTER	HUTTER	kA	HUTTER
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
;	;	kIx,	;
CHALABALA	CHALABALA	kA	CHALABALA
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgNnSc1d1	české
umění	umění	k1gNnSc1	umění
dramatické	dramatický	k2eAgFnSc2d1	dramatická
II	II	kA	II
-	-	kIx~	-
Zpěvohra	zpěvohra	k1gFnSc1	zpěvohra
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Šolc	Šolc	k1gMnSc1	Šolc
a	a	k8xC	a
Šimáček	Šimáček	k1gMnSc1	Šimáček
<g/>
,	,	kIx,	,
společnost	společnost	k1gFnSc1	společnost
s	s	k7c7	s
r.	r.	kA	r.
o.	o.	k?	o.
<g/>
,	,	kIx,	,
1941	[number]	k4	1941
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
122	[number]	k4	122
<g/>
-	-	kIx~	-
<g/>
126	[number]	k4	126
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
WAGNER	Wagner	k1gMnSc1	Wagner
<g/>
,	,	kIx,	,
Heinz	Heinz	k1gMnSc1	Heinz
<g/>
.	.	kIx.	.
</s>
<s>
Das	Das	k?	Das
grosse	grosse	k1gFnSc1	grosse
Handbuch	Handbuch	k1gInSc1	Handbuch
der	drát	k5eAaImRp2nS	drát
Oper	opera	k1gFnPc2	opera
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Hamburg	Hamburg	k1gInSc1	Hamburg
<g/>
:	:	kIx,	:
Nikol	nikol	k1gInSc1	nikol
Verlagsgesellschaft	Verlagsgesellschaft	k1gInSc1	Verlagsgesellschaft
mbH	mbH	k?	mbH
&	&	k?	&
Co	co	k9	co
<g/>
.	.	kIx.	.
</s>
<s>
KG	kg	kA	kg
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
1470	[number]	k4	1470
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
937872	[number]	k4	937872
<g/>
-	-	kIx~	-
<g/>
38	[number]	k4	38
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
Dvořák	Dvořák	k1gMnSc1	Dvořák
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
-	-	kIx~	-
Rusalka	rusalka	k1gFnSc1	rusalka
<g/>
,	,	kIx,	,
s.	s.	k?	s.
332	[number]	k4	332
<g/>
-	-	kIx~	-
<g/>
333	[number]	k4	333
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
KAMINSKI	KAMINSKI	kA	KAMINSKI
<g/>
,	,	kIx,	,
Piotr	Piotr	k1gInSc1	Piotr
<g/>
.	.	kIx.	.
</s>
<s>
Mille	Mille	k1gFnSc1	Mille
et	et	k?	et
un	un	k?	un
opéras	opéras	k1gInSc1	opéras
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Paris	Paris	k1gMnSc1	Paris
<g/>
:	:	kIx,	:
Librairie	Librairie	k1gFnSc1	Librairie
Arthè	Arthè	k1gMnSc1	Arthè
Fayard	Fayard	k1gMnSc1	Fayard
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
1819	[number]	k4	1819
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
213	[number]	k4	213
<g/>
-	-	kIx~	-
<g/>
60017	[number]	k4	60017
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
Dvořák	Dvořák	k1gMnSc1	Dvořák
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
-	-	kIx~	-
Rusalka	rusalka	k1gFnSc1	rusalka
<g/>
,	,	kIx,	,
s.	s.	k?	s.
403	[number]	k4	403
<g/>
-	-	kIx~	-
<g/>
406	[number]	k4	406
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Rusalka	rusalka	k1gFnSc1	rusalka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Rusalka	rusalka	k1gFnSc1	rusalka
(	(	kIx(	(
<g/>
Dvořák	Dvořák	k1gMnSc1	Dvořák
<g/>
)	)	kIx)	)
na	na	k7c6	na
Kinoboxu	Kinobox	k1gInSc6	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
První	první	k4xOgNnSc1	první
uvedení	uvedení	k1gNnSc1	uvedení
Rusalky	rusalka	k1gFnSc2	rusalka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1901	[number]	k4	1901
</s>
</p>
<p>
<s>
Rusalka	rusalka	k1gFnSc1	rusalka
(	(	kIx(	(
<g/>
Dvořák	Dvořák	k1gMnSc1	Dvořák
<g/>
)	)	kIx)	)
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
</s>
</p>
<p>
<s>
Článek	článek	k1gInSc1	článek
Ondřeje	Ondřej	k1gMnSc2	Ondřej
Šupky	Šupky	k?	Šupky
na	na	k7c4	na
OperaPlus	OperaPlus	k1gInSc4	OperaPlus
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Stránky	stránka	k1gFnPc1	stránka
věnované	věnovaný	k2eAgFnPc1d1	věnovaná
Antonínu	Antonín	k1gMnSc3	Antonín
Dvořákovi	Dvořák	k1gMnSc3	Dvořák
-	-	kIx~	-
včetně	včetně	k7c2	včetně
libreta	libreto	k1gNnSc2	libreto
</s>
</p>
