<s>
Oldřich	Oldřich	k1gMnSc1	Oldřich
Albín	Albín	k1gMnSc1	Albín
Nový	Nový	k1gMnSc1	Nový
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1899	[number]	k4	1899
Žižkov	Žižkov	k1gInSc1	Žižkov
–	–	k?	–
15	[number]	k4	15
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1983	[number]	k4	1983
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
režisér	režisér	k1gMnSc1	režisér
<g/>
,	,	kIx,	,
scenárista	scenárista	k1gMnSc1	scenárista
<g/>
,	,	kIx,	,
dramaturg	dramaturg	k1gMnSc1	dramaturg
<g/>
,	,	kIx,	,
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
,	,	kIx,	,
divadelní	divadelní	k2eAgMnSc1d1	divadelní
ředitel	ředitel	k1gMnSc1	ředitel
a	a	k8xC	a
pedagog	pedagog	k1gMnSc1	pedagog
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
na	na	k7c6	na
pražském	pražský	k2eAgNnSc6d1	Pražské
předměstí	předměstí	k1gNnSc6	předměstí
Žižkově	Žižkov	k1gInSc6	Žižkov
jako	jako	k9	jako
syn	syn	k1gMnSc1	syn
cvičitele	cvičitel	k1gMnSc2	cvičitel
hasičského	hasičský	k2eAgInSc2d1	hasičský
sboru	sbor	k1gInSc2	sbor
Antonína	Antonín	k1gMnSc2	Antonín
Nového	Nový	k1gMnSc2	Nový
(	(	kIx(	(
<g/>
1860	[number]	k4	1860
<g/>
–	–	k?	–
<g/>
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
a	a	k8xC	a
Cecilie	Cecilie	k1gFnSc1	Cecilie
rozené	rozený	k2eAgFnSc2d1	rozená
Valentové	Valentová	k1gFnSc2	Valentová
(	(	kIx(	(
<g/>
1869	[number]	k4	1869
<g/>
–	–	k?	–
<g/>
1909	[number]	k4	1909
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
matky	matka	k1gFnSc2	matka
se	se	k3xPyFc4	se
otec	otec	k1gMnSc1	otec
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1910	[number]	k4	1910
znovu	znovu	k6eAd1	znovu
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Marií	Maria	k1gFnSc7	Maria
<g/>
,	,	kIx,	,
rozenou	rozený	k2eAgFnSc7d1	rozená
Zahrádkovou	Zahrádková	k1gFnSc7	Zahrádková
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
hraní	hraní	k1gNnSc3	hraní
jej	on	k3xPp3gInSc2	on
přivedl	přivést	k5eAaPmAgMnS	přivést
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
jako	jako	k8xS	jako
pražský	pražský	k2eAgMnSc1d1	pražský
vrchní	vrchní	k2eAgMnSc1d1	vrchní
hasič	hasič	k1gMnSc1	hasič
zajišťoval	zajišťovat	k5eAaImAgMnS	zajišťovat
požární	požární	k2eAgFnSc4d1	požární
hlídku	hlídka	k1gFnSc4	hlídka
ve	v	k7c6	v
všech	všecek	k3xTgNnPc6	všecek
tehdejších	tehdejší	k2eAgNnPc6d1	tehdejší
kinech	kino	k1gNnPc6	kino
<g/>
,	,	kIx,	,
a	a	k8xC	a
strýc	strýc	k1gMnSc1	strýc
Miloš	Miloš	k1gMnSc1	Miloš
Nový	Nový	k1gMnSc1	Nový
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
jej	on	k3xPp3gMnSc4	on
brával	brávat	k5eAaImAgMnS	brávat
s	s	k7c7	s
sebou	se	k3xPyFc7	se
do	do	k7c2	do
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hrál	hrát	k5eAaImAgMnS	hrát
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
otcovo	otcův	k2eAgNnSc4d1	otcovo
přání	přání	k1gNnSc4	přání
se	se	k3xPyFc4	se
vyučil	vyučit	k5eAaPmAgMnS	vyučit
sazečem	sazeč	k1gMnSc7	sazeč
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
měl	mít	k5eAaImAgInS	mít
jisté	jistý	k2eAgNnSc4d1	jisté
živobytí	živobytí	k1gNnSc4	živobytí
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
jeho	jeho	k3xOp3gInSc1	jeho
zájem	zájem	k1gInSc1	zájem
směřoval	směřovat	k5eAaImAgInS	směřovat
k	k	k7c3	k
divadlu	divadlo	k1gNnSc3	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
hrál	hrát	k5eAaImAgMnS	hrát
ochotnické	ochotnický	k2eAgNnSc4d1	ochotnické
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
dostal	dostat	k5eAaPmAgMnS	dostat
Oldřich	Oldřich	k1gMnSc1	Oldřich
Nový	nový	k2eAgMnSc1d1	nový
angažmá	angažmá	k1gNnSc4	angažmá
v	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
<g/>
,	,	kIx,	,
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
působil	působit	k5eAaImAgMnS	působit
nejdříve	dříve	k6eAd3	dříve
jako	jako	k8xS	jako
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
i	i	k9	i
režisér	režisér	k1gMnSc1	režisér
(	(	kIx(	(
<g/>
od	od	k7c2	od
1923	[number]	k4	1923
<g/>
)	)	kIx)	)
a	a	k8xC	a
šéf	šéf	k1gMnSc1	šéf
operety	opereta	k1gFnSc2	opereta
(	(	kIx(	(
<g/>
od	od	k7c2	od
sezony	sezona	k1gFnSc2	sezona
1925	[number]	k4	1925
<g/>
/	/	kIx~	/
<g/>
1926	[number]	k4	1926
<g/>
)	)	kIx)	)
tamního	tamní	k2eAgNnSc2d1	tamní
Národního	národní	k2eAgNnSc2d1	národní
(	(	kIx(	(
<g/>
Zemského	zemský	k2eAgNnSc2d1	zemské
<g/>
)	)	kIx)	)
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
přínosem	přínos	k1gInSc7	přínos
tomuto	tento	k3xDgInSc3	tento
žánru	žánr	k1gInSc3	žánr
bylo	být	k5eAaImAgNnS	být
hlavně	hlavně	k9	hlavně
zinteligentnění	zinteligentnění	k1gNnSc1	zinteligentnění
děje	děj	k1gInSc2	děj
<g/>
,	,	kIx,	,
odstranění	odstranění	k1gNnSc4	odstranění
pozlátka	pozlátko	k1gNnSc2	pozlátko
a	a	k8xC	a
přiblížení	přiblížení	k1gNnSc2	přiblížení
divákovi	divákův	k2eAgMnPc1d1	divákův
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
zvýšení	zvýšení	k1gNnSc4	zvýšení
jeho	jeho	k3xOp3gFnSc2	jeho
umělecké	umělecký	k2eAgFnSc2d1	umělecká
hodnoty	hodnota	k1gFnSc2	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
patnácti	patnáct	k4xCc6	patnáct
letech	léto	k1gNnPc6	léto
odsud	odsud	k6eAd1	odsud
pro	pro	k7c4	pro
neshody	neshoda	k1gFnPc4	neshoda
ve	v	k7c4	v
vedení	vedení	k1gNnSc4	vedení
a	a	k8xC	a
dramaturgování	dramaturgování	k1gNnSc4	dramaturgování
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
ředitelem	ředitel	k1gMnSc7	ředitel
Nového	Nového	k2eAgNnSc2d1	Nového
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
mohl	moct	k5eAaImAgInS	moct
konečně	konečně	k6eAd1	konečně
vyzkoušet	vyzkoušet	k5eAaPmF	vyzkoušet
žánr	žánr	k1gInSc4	žánr
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
neprosadil	prosadit	k5eNaPmAgMnS	prosadit
<g/>
:	:	kIx,	:
hudební	hudební	k2eAgFnSc4d1	hudební
komedii	komedie	k1gFnSc4	komedie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
operety	opereta	k1gFnSc2	opereta
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
nejdůležitější	důležitý	k2eAgFnPc4d3	nejdůležitější
písně	píseň	k1gFnPc4	píseň
a	a	k8xC	a
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
komické	komický	k2eAgInPc4d1	komický
výstupy	výstup	k1gInPc4	výstup
působí	působit	k5eAaImIp3nS	působit
spíše	spíše	k9	spíše
jako	jako	k8xC	jako
předehra	předehra	k1gFnSc1	předehra
k	k	k7c3	k
árii	árie	k1gFnSc3	árie
na	na	k7c4	na
aplaus	aplaus	k1gInSc4	aplaus
<g/>
,	,	kIx,	,
v	v	k7c6	v
hudební	hudební	k2eAgFnSc6d1	hudební
komedii	komedie	k1gFnSc6	komedie
(	(	kIx(	(
<g/>
tak	tak	k9	tak
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
ji	on	k3xPp3gFnSc4	on
Nový	Nový	k1gMnSc1	Nový
pojímal	pojímat	k5eAaImAgMnS	pojímat
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
píseň	píseň	k1gFnSc1	píseň
spíše	spíše	k9	spíše
pokračováním	pokračování	k1gNnSc7	pokračování
mluveného	mluvený	k2eAgInSc2d1	mluvený
dialogu	dialog	k1gInSc2	dialog
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
přejde	přejít	k5eAaPmIp3nS	přejít
do	do	k7c2	do
zpěvu	zpěv	k1gInSc2	zpěv
jen	jen	k9	jen
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
slovo	slovo	k1gNnSc1	slovo
nezpívané	zpívaný	k2eNgFnSc2d1	nezpívaná
již	již	k6eAd1	již
k	k	k7c3	k
vyjádření	vyjádření	k1gNnSc3	vyjádření
citů	cit	k1gInPc2	cit
nestačí	stačit	k5eNaBmIp3nS	stačit
<g/>
.	.	kIx.	.
</s>
<s>
Zahájil	zahájit	k5eAaPmAgMnS	zahájit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1934	[number]	k4	1934
komedií	komedie	k1gFnPc2	komedie
"	"	kIx"	"
<g/>
Štěstí	štěstí	k1gNnSc1	štěstí
do	do	k7c2	do
domu	dům	k1gInSc2	dům
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
55	[number]	k4	55
repríz	repríza	k1gFnPc2	repríza
místo	místo	k7c2	místo
plánovaných	plánovaný	k2eAgNnPc2d1	plánované
deseti	deset	k4xCc2	deset
<g/>
.	.	kIx.	.
</s>
<s>
Pokračoval	pokračovat	k5eAaImAgInS	pokračovat
řadou	řada	k1gFnSc7	řada
dalších	další	k2eAgInPc2d1	další
titulů	titul	k1gInPc2	titul
<g/>
,	,	kIx,	,
dosahujících	dosahující	k2eAgFnPc2d1	dosahující
okolo	okolo	k7c2	okolo
sta	sto	k4xCgNnPc4	sto
repríz	repríza	k1gFnPc2	repríza
a	a	k8xC	a
kritikou	kritika	k1gFnSc7	kritika
kladně	kladně	k6eAd1	kladně
hodnocených	hodnocený	k2eAgInPc2d1	hodnocený
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
filmových	filmový	k2eAgInPc6d1	filmový
úspěších	úspěch	k1gInPc6	úspěch
se	se	k3xPyFc4	se
Novému	Nový	k1gMnSc3	Nový
podařilo	podařit	k5eAaPmAgNnS	podařit
naplnit	naplnit	k5eAaPmF	naplnit
divadlo	divadlo	k1gNnSc1	divadlo
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
jedním	jeden	k4xCgInSc7	jeden
titulem	titul	k1gInSc7	titul
vydržel	vydržet	k5eAaPmAgMnS	vydržet
celou	celý	k2eAgFnSc4d1	celá
sezónu	sezóna	k1gFnSc4	sezóna
<g/>
.	.	kIx.	.
</s>
<s>
Eduard	Eduard	k1gMnSc1	Eduard
Bass	Bass	k1gMnSc1	Bass
nazval	nazvat	k5eAaPmAgMnS	nazvat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
představení	představení	k1gNnPc2	představení
Další	další	k2eAgMnSc1d1	další
<g/>
,	,	kIx,	,
prosím	prosit	k5eAaImIp1nS	prosit
"	"	kIx"	"
<g/>
opereta	opereta	k1gFnSc1	opereta
pro	pro	k7c4	pro
kulturní	kulturní	k2eAgMnPc4d1	kulturní
lidi	člověk	k1gMnPc4	člověk
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
okupace	okupace	k1gFnSc2	okupace
se	se	k3xPyFc4	se
Oldřich	Oldřich	k1gMnSc1	Oldřich
Nový	Nový	k1gMnSc1	Nový
nejvíc	nejvíc	k6eAd1	nejvíc
prosadil	prosadit	k5eAaPmAgMnS	prosadit
ve	v	k7c6	v
filmu	film	k1gInSc6	film
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
sice	sice	k8xC	sice
získal	získat	k5eAaPmAgMnS	získat
filmovou	filmový	k2eAgFnSc4d1	filmová
roli	role	k1gFnSc4	role
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1922	[number]	k4	1922
v	v	k7c6	v
němém	němý	k2eAgInSc6d1	němý
filmu	film	k1gInSc6	film
Přemysla	Přemysl	k1gMnSc2	Přemysl
Pražského	pražský	k2eAgInSc2d1	pražský
Neznámá	známý	k2eNgFnSc1d1	neznámá
kráska	kráska	k1gFnSc1	kráska
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
jen	jen	k9	jen
v	v	k7c6	v
malých	malý	k2eAgFnPc6d1	malá
roličkách	rolička	k1gFnPc6	rolička
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
přišel	přijít	k5eAaPmAgMnS	přijít
Mac	Mac	kA	Mac
Frič	Frič	k1gMnSc1	Frič
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
Kristián	Kristián	k1gMnSc1	Kristián
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
Nového	Nového	k2eAgInSc1d1	Nového
nejúspěšnější	úspěšný	k2eAgInSc1d3	nejúspěšnější
film	film	k1gInSc1	film
<g/>
.	.	kIx.	.
</s>
<s>
Následovalo	následovat	k5eAaImAgNnS	následovat
mnoho	mnoho	k4c1	mnoho
hudebních	hudební	k2eAgFnPc2d1	hudební
komedií	komedie	k1gFnPc2	komedie
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
11	[number]	k4	11
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1936	[number]	k4	1936
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
uměleckou	umělecký	k2eAgFnSc7d1	umělecká
fotografkou	fotografka	k1gFnSc7	fotografka
Alicí	Alice	k1gFnSc7	Alice
Wienerovou	Wienerová	k1gFnSc7	Wienerová
(	(	kIx(	(
<g/>
1902	[number]	k4	1902
<g/>
–	–	k?	–
<g/>
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dcerou	dcera	k1gFnSc7	dcera
zámožného	zámožný	k2eAgMnSc2d1	zámožný
ředitele	ředitel	k1gMnSc2	ředitel
pražské	pražský	k2eAgFnSc2d1	Pražská
banky	banka	k1gFnSc2	banka
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
vychovali	vychovat	k5eAaPmAgMnP	vychovat
adoptivní	adoptivní	k2eAgFnSc4d1	adoptivní
dceru	dcera	k1gFnSc4	dcera
Janu	Jana	k1gFnSc4	Jana
(	(	kIx(	(
<g/>
1937	[number]	k4	1937
<g/>
–	–	k?	–
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
provdanou	provdaný	k2eAgFnSc7d1	provdaná
Včelákovou	včelákův	k2eAgFnSc7d1	Včelákova
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
nacismu	nacismus	k1gInSc2	nacismus
hrozila	hrozit	k5eAaImAgFnS	hrozit
Alici	Alice	k1gFnSc4	Alice
deportace	deportace	k1gFnSc2	deportace
do	do	k7c2	do
koncentračního	koncentrační	k2eAgInSc2d1	koncentrační
tábora	tábor	k1gInSc2	tábor
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
jejímu	její	k3xOp3gInSc3	její
židovskému	židovský	k2eAgInSc3d1	židovský
původu	původ	k1gInSc3	původ
<g/>
.	.	kIx.	.
</s>
<s>
Nový	Nový	k1gMnSc1	Nový
ji	on	k3xPp3gFnSc4	on
bránil	bránit	k5eAaImAgMnS	bránit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
odmítal	odmítat	k5eAaImAgMnS	odmítat
rozvést	rozvést	k5eAaPmF	rozvést
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
manželé	manžel	k1gMnPc1	manžel
i	i	k9	i
divadlo	divadlo	k1gNnSc4	divadlo
trpěli	trpět	k5eAaImAgMnP	trpět
antisemitskými	antisemitský	k2eAgInPc7d1	antisemitský
útoky	útok	k1gInPc7	útok
v	v	k7c6	v
tisku	tisk	k1gInSc6	tisk
i	i	k8xC	i
na	na	k7c6	na
veřejnosti	veřejnost	k1gFnSc6	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Nový	Nový	k1gMnSc1	Nový
byl	být	k5eAaImAgMnS	být
deportován	deportovat	k5eAaBmNgMnS	deportovat
přes	přes	k7c4	přes
Hagiboru	Hagibora	k1gFnSc4	Hagibora
do	do	k7c2	do
Osterode	Osterod	k1gInSc5	Osterod
v	v	k7c6	v
Sasku	Sasko	k1gNnSc6	Sasko
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
manželka	manželka	k1gFnSc1	manželka
měla	mít	k5eAaImAgFnS	mít
jít	jít	k5eAaImF	jít
do	do	k7c2	do
Terezína	Terezín	k1gInSc2	Terezín
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dceru	dcera	k1gFnSc4	dcera
Janu	Jana	k1gFnSc4	Jana
se	se	k3xPyFc4	se
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
starali	starat	k5eAaImAgMnP	starat
manželé	manžel	k1gMnPc1	manžel
Lelkovi	lelkův	k2eAgMnPc1d1	lelkův
<g/>
,	,	kIx,	,
majitelé	majitel	k1gMnPc1	majitel
továrny	továrna	k1gFnSc2	továrna
na	na	k7c4	na
skelný	skelný	k2eAgInSc4d1	skelný
papír	papír	k1gInSc4	papír
v	v	k7c6	v
lázních	lázeň	k1gFnPc6	lázeň
Bělohrad	Bělohrad	k1gInSc1	Bělohrad
<g/>
.	.	kIx.	.
</s>
<s>
Žena	žena	k1gFnSc1	žena
Alice	Alice	k1gFnSc2	Alice
věznění	věznění	k1gNnSc2	věznění
přežila	přežít	k5eAaPmAgFnS	přežít
<g/>
,	,	kIx,	,
takové	takový	k3xDgNnSc4	takový
štěstí	štěstí	k1gNnSc4	štěstí
neměla	mít	k5eNaImAgFnS	mít
polovina	polovina	k1gFnSc1	polovina
její	její	k3xOp3gFnSc2	její
rodiny	rodina	k1gFnSc2	rodina
včetně	včetně	k7c2	včetně
otce	otec	k1gMnSc2	otec
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
ztrátou	ztráta	k1gFnSc7	ztráta
bližních	bližní	k1gMnPc2	bližní
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
nevyrovnala	vyrovnat	k5eNaPmAgFnS	vyrovnat
a	a	k8xC	a
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
u	u	k7c2	u
ní	on	k3xPp3gFnSc2	on
schizofrenie	schizofrenie	k1gFnSc2	schizofrenie
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
poválečný	poválečný	k2eAgInSc1d1	poválečný
filmový	filmový	k2eAgInSc1d1	filmový
úspěch	úspěch	k1gInSc1	úspěch
přišel	přijít	k5eAaPmAgInS	přijít
s	s	k7c7	s
rolí	role	k1gFnSc7	role
René	René	k1gFnSc2	René
Skalského	Skalského	k2eAgFnSc2d1	Skalského
v	v	k7c6	v
parodii	parodie	k1gFnSc6	parodie
na	na	k7c4	na
romantický	romantický	k2eAgInSc4d1	romantický
žánr	žánr	k1gInSc4	žánr
Pytlákova	pytlákův	k2eAgFnSc1d1	Pytlákova
schovanka	schovanka	k1gFnSc1	schovanka
<g/>
.	.	kIx.	.
</s>
<s>
Úspěch	úspěch	k1gInSc1	úspěch
se	se	k3xPyFc4	se
však	však	k9	však
dostavil	dostavit	k5eAaPmAgInS	dostavit
až	až	k6eAd1	až
později	pozdě	k6eAd2	pozdě
–	–	k?	–
dobové	dobový	k2eAgNnSc4d1	dobové
publikum	publikum	k1gNnSc4	publikum
parodii	parodie	k1gFnSc3	parodie
zčásti	zčásti	k6eAd1	zčásti
nepochopilo	pochopit	k5eNaPmAgNnS	pochopit
a	a	k8xC	a
komunistická	komunistický	k2eAgFnSc1d1	komunistická
kritika	kritika	k1gFnSc1	kritika
film	film	k1gInSc4	film
odvrhla	odvrhnout	k5eAaPmAgFnS	odvrhnout
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
bolo	bola	k1gFnSc5	bola
škoda	škoda	k1gFnSc1	škoda
filmovacieho	filmovacie	k1gMnSc2	filmovacie
materiálu	materiál	k1gInSc2	materiál
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Režisér	režisér	k1gMnSc1	režisér
Hynek	Hynek	k1gMnSc1	Hynek
Bočan	bočan	k1gMnSc1	bočan
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
popsal	popsat	k5eAaPmAgMnS	popsat
pochopení	pochopení	k1gNnSc4	pochopení
a	a	k8xC	a
nepochopení	nepochopení	k1gNnSc4	nepochopení
filmu	film	k1gInSc2	film
jako	jako	k8xS	jako
rozdíl	rozdíl	k1gInSc4	rozdíl
v	v	k7c6	v
chápání	chápání	k1gNnSc6	chápání
mladé	mladý	k2eAgFnSc2d1	mladá
a	a	k8xC	a
staré	starý	k2eAgFnSc2d1	stará
generace	generace	k1gFnSc2	generace
<g/>
.	.	kIx.	.
</s>
<s>
Pytlákovu	pytlákův	k2eAgFnSc4d1	Pytlákova
schovanku	schovanka	k1gFnSc4	schovanka
zhlédlo	zhlédnout	k5eAaPmAgNnS	zhlédnout
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1988	[number]	k4	1988
více	hodně	k6eAd2	hodně
než	než	k8xS	než
4	[number]	k4	4
milióny	milión	k4xCgInPc1	milión
diváků	divák	k1gMnPc2	divák
v	v	k7c6	v
21	[number]	k4	21
435	[number]	k4	435
představeních	představení	k1gNnPc6	představení
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
kin	kino	k1gNnPc2	kino
(	(	kIx(	(
<g/>
i	i	k9	i
když	když	k8xS	když
okrajových	okrajový	k2eAgMnPc2d1	okrajový
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
Pytlákova	pytlákův	k2eAgFnSc1d1	Pytlákova
schovanka	schovanka	k1gFnSc1	schovanka
nasazována	nasazovat	k5eAaImNgFnS	nasazovat
ještě	ještě	k9	ještě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1954	[number]	k4	1954
a	a	k8xC	a
v	v	k7c6	v
televizi	televize	k1gFnSc6	televize
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
předválečný	předválečný	k2eAgInSc4d1	předválečný
herecký	herecký	k2eAgInSc4d1	herecký
typ	typ	k1gInSc4	typ
Oldřicha	Oldřich	k1gMnSc2	Oldřich
Nového	Nový	k1gMnSc2	Nový
se	se	k3xPyFc4	se
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
hledalo	hledat	k5eAaImAgNnS	hledat
obtížně	obtížně	k6eAd1	obtížně
uplatnění	uplatnění	k1gNnSc1	uplatnění
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
situace	situace	k1gFnSc1	situace
lépe	dobře	k6eAd2	dobře
<g/>
,	,	kIx,	,
než	než	k8xS	než
pro	pro	k7c4	pro
řadu	řada	k1gFnSc4	řada
hereckých	herecký	k2eAgMnPc2d1	herecký
kolegů	kolega	k1gMnPc2	kolega
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
se	se	k3xPyFc4	se
přizpůsobil	přizpůsobit	k5eAaPmAgMnS	přizpůsobit
požadavkům	požadavek	k1gInPc3	požadavek
socialistického	socialistický	k2eAgInSc2d1	socialistický
realismu	realismus	k1gInSc2	realismus
<g/>
.	.	kIx.	.
</s>
<s>
Hrál	hrát	k5eAaImAgMnS	hrát
sice	sice	k8xC	sice
i	i	k9	i
nadále	nadále	k6eAd1	nadále
hlavní	hlavní	k2eAgFnPc4d1	hlavní
role	role	k1gFnPc4	role
<g/>
,	,	kIx,	,
nejedná	jednat	k5eNaImIp3nS	jednat
se	se	k3xPyFc4	se
však	však	k9	však
o	o	k7c4	o
významná	významný	k2eAgNnPc4d1	významné
díla	dílo	k1gNnPc4	dílo
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
filmografii	filmografie	k1gFnSc6	filmografie
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
stereotypu	stereotyp	k1gInSc2	stereotyp
vybočuje	vybočovat	k5eAaImIp3nS	vybočovat
například	například	k6eAd1	například
film	film	k1gInSc4	film
Fantom	fantom	k1gInSc1	fantom
Morrisvillu	Morrisvill	k1gInSc2	Morrisvill
a	a	k8xC	a
film	film	k1gInSc1	film
režiséra	režisér	k1gMnSc2	režisér
Podskalského	podskalský	k2eAgMnSc2d1	podskalský
Světáci	Světák	k1gMnPc1	Světák
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
energie	energie	k1gFnSc2	energie
zaměřil	zaměřit	k5eAaPmAgMnS	zaměřit
na	na	k7c4	na
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
,	,	kIx,	,
hostoval	hostovat	k5eAaImAgInS	hostovat
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
režíroval	režírovat	k5eAaImAgMnS	režírovat
několik	několik	k4yIc4	několik
úspěšných	úspěšný	k2eAgFnPc2d1	úspěšná
her	hra	k1gFnPc2	hra
i	i	k9	i
hrál	hrát	k5eAaImAgInS	hrát
mnoho	mnoho	k4c4	mnoho
úspěšných	úspěšný	k2eAgFnPc2d1	úspěšná
hlavních	hlavní	k2eAgFnPc2d1	hlavní
rolí	role	k1gFnPc2	role
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgMnPc4d1	jiný
v	v	k7c6	v
karlínském	karlínský	k2eAgNnSc6d1	Karlínské
hudebním	hudební	k2eAgNnSc6d1	hudební
divadle	divadlo	k1gNnSc6	divadlo
režíroval	režírovat	k5eAaImAgInS	režírovat
velmi	velmi	k6eAd1	velmi
oblíbenou	oblíbený	k2eAgFnSc4d1	oblíbená
operetu	opereta	k1gFnSc4	opereta
Mamzelle	Mamzell	k1gMnSc2	Mamzell
Nitouche	Nitouch	k1gMnSc2	Nitouch
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
zpíval	zpívat	k5eAaImAgMnS	zpívat
Célestina	Célestin	k1gMnSc4	Célestin
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Stanislavy	Stanislava	k1gFnSc2	Stanislava
Součkové	Součková	k1gFnSc2	Součková
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
zasloužilým	zasloužilý	k2eAgMnSc7d1	zasloužilý
umělcem	umělec	k1gMnSc7	umělec
,	,	kIx,	,
v	v	k7c6	v
letech	let	k1gInPc6	let
1954	[number]	k4	1954
<g/>
–	–	k?	–
<g/>
1959	[number]	k4	1959
byl	být	k5eAaImAgInS	být
uměleckým	umělecký	k2eAgMnSc7d1	umělecký
ředitelem	ředitel	k1gMnSc7	ředitel
Hudebního	hudební	k2eAgNnSc2d1	hudební
divadla	divadlo	k1gNnSc2	divadlo
v	v	k7c6	v
Karlíně	Karlín	k1gInSc6	Karlín
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1959	[number]	k4	1959
<g/>
–	–	k?	–
<g/>
1964	[number]	k4	1964
stál	stát	k5eAaImAgInS	stát
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
nově	nově	k6eAd1	nově
založeného	založený	k2eAgNnSc2d1	založené
oddělení	oddělení	k1gNnSc2	oddělení
hudební	hudební	k2eAgFnSc2d1	hudební
komedie	komedie	k1gFnSc2	komedie
na	na	k7c6	na
Pražské	pražský	k2eAgFnSc6d1	Pražská
konzervatoři	konzervatoř	k1gFnSc6	konzervatoř
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
příležitostí	příležitost	k1gFnPc2	příležitost
mu	on	k3xPp3gNnSc3	on
přinesla	přinést	k5eAaPmAgFnS	přinést
televize	televize	k1gFnSc1	televize
<g/>
.	.	kIx.	.
</s>
<s>
Zahrál	zahrát	k5eAaPmAgMnS	zahrát
si	se	k3xPyFc3	se
v	v	k7c6	v
bakalářské	bakalářský	k2eAgFnSc6d1	Bakalářská
povídce	povídka	k1gFnSc6	povídka
a	a	k8xC	a
především	především	k6eAd1	především
v	v	k7c6	v
osmidílném	osmidílný	k2eAgInSc6d1	osmidílný
seriálu	seriál	k1gInSc6	seriál
Fan	Fana	k1gFnPc2	Fana
Vavřincové	Vavřincové	k?	Vavřincové
a	a	k8xC	a
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Dudka	Dudek	k1gMnSc2	Dudek
Taková	takový	k3xDgFnSc1	takový
normální	normální	k2eAgFnSc1d1	normální
rodinka	rodinka	k1gFnSc1	rodinka
(	(	kIx(	(
<g/>
1967	[number]	k4	1967
<g/>
–	–	k?	–
<g/>
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ztvárnil	ztvárnit	k5eAaPmAgMnS	ztvárnit
neodolatelně	odolatelně	k6eNd1	odolatelně
komisního	komisní	k2eAgMnSc4d1	komisní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
lidsky	lidsky	k6eAd1	lidsky
půvabného	půvabný	k2eAgMnSc2d1	půvabný
účetního	účetní	k1gMnSc2	účetní
Jana	Jan	k1gMnSc2	Jan
Koníčka	Koníček	k1gMnSc2	Koníček
<g/>
.	.	kIx.	.
</s>
<s>
Posledních	poslední	k2eAgNnPc2d1	poslední
deset	deset	k4xCc1	deset
let	léto	k1gNnPc2	léto
života	život	k1gInSc2	život
téměř	téměř	k6eAd1	téměř
nevycházel	vycházet	k5eNaImAgMnS	vycházet
ze	z	k7c2	z
svého	svůj	k3xOyFgInSc2	svůj
bytu	byt	k1gInSc2	byt
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nechtěl	chtít	k5eNaImAgMnS	chtít
být	být	k5eAaImF	být
svými	svůj	k3xOyFgMnPc7	svůj
fanoušky	fanoušek	k1gMnPc4	fanoušek
poznán	poznán	k2eAgMnSc1d1	poznán
<g/>
.	.	kIx.	.
</s>
<s>
Chtěl	chtít	k5eAaImAgMnS	chtít
zůstat	zůstat	k5eAaPmF	zůstat
v	v	k7c6	v
paměti	paměť	k1gFnSc6	paměť
jako	jako	k8xC	jako
milovaný	milovaný	k2eAgMnSc1d1	milovaný
Oldřich	Oldřich	k1gMnSc1	Oldřich
Nový	Nový	k1gMnSc1	Nový
<g/>
,	,	kIx,	,
Kristian	Kristian	k1gMnSc1	Kristian
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc7	jeho
životním	životní	k2eAgNnSc7d1	životní
krédem	krédo	k1gNnSc7	krédo
bylo	být	k5eAaImAgNnS	být
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Smích	smích	k1gInSc1	smích
je	být	k5eAaImIp3nS	být
kořením	koření	k1gNnSc7	koření
života	život	k1gInSc2	život
a	a	k8xC	a
dokud	dokud	k8xS	dokud
se	se	k3xPyFc4	se
člověk	člověk	k1gMnSc1	člověk
dovede	dovést	k5eAaPmIp3nS	dovést
smát	smát	k5eAaImF	smát
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
živ	živ	k2eAgMnSc1d1	živ
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
se	se	k3xPyFc4	se
začnou	začít	k5eAaPmIp3nP	začít
všichni	všechen	k3xTgMnPc1	všechen
lidé	člověk	k1gMnPc1	člověk
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
usmívat	usmívat	k5eAaImF	usmívat
a	a	k8xC	a
přestanou	přestat	k5eAaPmIp3nP	přestat
si	se	k3xPyFc3	se
kopat	kopat	k5eAaImF	kopat
jámy	jáma	k1gFnSc2	jáma
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
bude	být	k5eAaImBp3nS	být
na	na	k7c6	na
světě	svět	k1gInSc6	svět
blaze	blaze	k6eAd1	blaze
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Podle	podle	k7c2	podle
něho	on	k3xPp3gInSc2	on
žil	žít	k5eAaImAgMnS	žít
<g/>
,	,	kIx,	,
tvořil	tvořit	k5eAaImAgInS	tvořit
i	i	k8xC	i
jednal	jednat	k5eAaImAgInS	jednat
<g/>
.	.	kIx.	.
</s>
<s>
Pohřben	pohřben	k2eAgMnSc1d1	pohřben
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
Olšanských	olšanský	k2eAgInPc6d1	olšanský
hřbitovech	hřbitov	k1gInPc6	hřbitov
(	(	kIx(	(
<g/>
II	II	kA	II
<g/>
.	.	kIx.	.
obecní	obecní	k2eAgInSc1d1	obecní
hřbitov	hřbitov	k1gInSc1	hřbitov
<g/>
,	,	kIx,	,
27	[number]	k4	27
<g/>
.	.	kIx.	.
oddělení	oddělení	k1gNnSc1	oddělení
<g/>
,	,	kIx,	,
8	[number]	k4	8
UH	uh	k0	uh
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byly	být	k5eAaImAgInP	být
později	pozdě	k6eAd2	pozdě
uloženy	uložen	k2eAgInPc1d1	uložen
ostatky	ostatek	k1gInPc1	ostatek
dcery	dcera	k1gFnSc2	dcera
Jany	Jana	k1gFnSc2	Jana
<g/>
.	.	kIx.	.
1935	[number]	k4	1935
Jean	Jean	k1gMnSc1	Jean
de	de	k?	de
Létraz	Létraz	k1gInSc1	Létraz
<g/>
,	,	kIx,	,
A.	A.	kA	A.
Steinbrecher	Steinbrechra	k1gFnPc2	Steinbrechra
<g/>
:	:	kIx,	:
Štěstí	štěstí	k1gNnSc1	štěstí
do	do	k7c2	do
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
André	André	k1gMnPc3	André
<g/>
,	,	kIx,	,
Nové	Nové	k2eAgNnSc4d1	Nové
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
,	,	kIx,	,
režie	režie	k1gFnPc4	režie
Oldřich	Oldřich	k1gMnSc1	Oldřich
Nový	nový	k2eAgInSc4d1	nový
1936	[number]	k4	1936
Ralph	Ralph	k1gInSc4	Ralph
Benatzky	Benatzka	k1gFnSc2	Benatzka
<g/>
:	:	kIx,	:
Okouzlující	okouzlující	k2eAgFnSc2d1	okouzlující
slečna	slečna	k1gFnSc1	slečna
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
<g/>
,	,	kIx,	,
Nové	Nové	k2eAgNnSc4d1	Nové
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
,	,	kIx,	,
režie	režie	k1gFnPc4	režie
Oldřich	Oldřich	k1gMnSc1	Oldřich
Nový	nový	k2eAgInSc4d1	nový
1936	[number]	k4	1936
O.	O.	kA	O.
Nový	nový	k2eAgInSc4d1	nový
<g/>
,	,	kIx,	,
R.	R.	kA	R.
Kvasnica	Kvasnicum	k1gNnSc2	Kvasnicum
<g/>
:	:	kIx,	:
Další	další	k2eAgFnSc7d1	další
<g/>
,	,	kIx,	,
prosím	prosit	k5eAaImIp1nS	prosit
<g/>
...	...	k?	...
<g/>
,	,	kIx,	,
Úředník	úředník	k1gMnSc1	úředník
<g/>
,	,	kIx,	,
Nové	Nové	k2eAgFnSc6d1	Nové
<g />
.	.	kIx.	.
</s>
<s>
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Oldřich	Oldřich	k1gMnSc1	Oldřich
Nový	nový	k2eAgMnSc1d1	nový
1937	[number]	k4	1937
A.	A.	kA	A.
Steinbrecher	Steinbrechra	k1gFnPc2	Steinbrechra
<g/>
:	:	kIx,	:
Krejčí	Krejčí	k1gMnSc1	Krejčí
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
<g/>
,	,	kIx,	,
Adolf	Adolf	k1gMnSc1	Adolf
Hortigan	Hortigan	k1gMnSc1	Hortigan
<g/>
,	,	kIx,	,
Nové	Nové	k2eAgNnSc1d1	Nové
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Oldřich	Oldřich	k1gMnSc1	Oldřich
Nový	nový	k2eAgMnSc1d1	nový
1938	[number]	k4	1938
Hudba	hudba	k1gFnSc1	hudba
Hervé	Hervá	k1gFnSc2	Hervá
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
a	a	k8xC	a
úprava	úprava	k1gFnSc1	úprava
Oldřich	Oldřich	k1gMnSc1	Oldřich
Nový	nový	k2eAgMnSc1d1	nový
<g/>
:	:	kIx,	:
Mam	mam	k1gInSc1	mam
<g/>
'	'	kIx"	'
<g/>
zelle	zelle	k1gInSc1	zelle
Nitouche	Nitouch	k1gInSc2	Nitouch
<g/>
,	,	kIx,	,
Nové	Nové	k2eAgNnSc1d1	Nové
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
role	role	k1gFnSc1	role
Célestina	Célestina	k1gFnSc1	Célestina
1939	[number]	k4	1939
Ján	Ján	k1gMnSc1	Ján
Móry	móra	k1gFnPc4	móra
<g/>
,	,	kIx,	,
O.	O.	kA	O.
Nový	nový	k2eAgInSc1d1	nový
<g/>
:	:	kIx,	:
Pro	pro	k7c4	pro
tebe	ty	k3xPp2nSc4	ty
všecko	všecek	k3xTgNnSc4	všecek
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Hodan	Hodan	k1gMnSc1	Hodan
<g/>
,	,	kIx,	,
Nové	Nové	k2eAgNnSc1d1	Nové
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Oldřich	Oldřich	k1gMnSc1	Oldřich
Nový	nový	k2eAgMnSc1d1	nový
1940	[number]	k4	1940
Jan	Jan	k1gMnSc1	Jan
Patrný	patrný	k2eAgMnSc1d1	patrný
<g/>
,	,	kIx,	,
S.	S.	kA	S.
E.	E.	kA	E.
Nováček	Nováček	k1gMnSc1	Nováček
<g/>
:	:	kIx,	:
Muži	muž	k1gMnPc1	muž
nestárnou	stárnout	k5eNaImIp3nP	stárnout
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
Janderka	Janderka	k1gFnSc1	Janderka
<g/>
,	,	kIx,	,
Nové	Nové	k2eAgNnSc1d1	Nové
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
režie	režie	k1gFnSc1	režie
Oldřich	Oldřich	k1gMnSc1	Oldřich
Nový	Nový	k1gMnSc1	Nový
1941	[number]	k4	1941
Ralph	Ralph	k1gInSc1	Ralph
Benatzky	Benatzka	k1gFnSc2	Benatzka
<g/>
,	,	kIx,	,
Karl	Karl	k1gMnSc1	Karl
Vollmöller	Vollmöller	k1gInSc1	Vollmöller
<g/>
:	:	kIx,	:
Karolina	Karolinum	k1gNnSc2	Karolinum
(	(	kIx(	(
<g/>
Coctail	Coctail	k1gInSc1	Coctail
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Fred	Fred	k1gMnSc1	Fred
<g/>
,	,	kIx,	,
Nové	Nové	k2eAgNnSc1d1	Nové
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Oldřich	Oldřich	k1gMnSc1	Oldřich
Nový	nový	k2eAgMnSc1d1	nový
1942	[number]	k4	1942
B.	B.	kA	B.
Corry	Corra	k1gFnSc2	Corra
<g/>
,	,	kIx,	,
G.	G.	kA	G.
Achille	Achilles	k1gMnSc5	Achilles
<g/>
,	,	kIx,	,
O.	O.	kA	O.
Nový	nový	k2eAgInSc1d1	nový
<g/>
:	:	kIx,	:
Jedenáctý	jedenáctý	k4xOgInSc4	jedenáctý
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
<g/>
,	,	kIx,	,
Štěpán	Štěpán	k1gMnSc1	Štěpán
Krapka	krapka	k1gFnSc1	krapka
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Nové	Nové	k2eAgNnSc1d1	Nové
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Oldřich	Oldřich	k1gMnSc1	Oldřich
Nový	nový	k2eAgMnSc1d1	nový
1943	[number]	k4	1943
František	František	k1gMnSc1	František
Kožík	Kožík	k1gMnSc1	Kožík
<g/>
,	,	kIx,	,
Julius	Julius	k1gMnSc1	Julius
Kalaš	Kalaš	k1gMnSc1	Kalaš
<g/>
:	:	kIx,	:
Komediant	komediant	k1gMnSc1	komediant
<g/>
,	,	kIx,	,
Bertrand	Bertrand	k1gInSc1	Bertrand
<g/>
,	,	kIx,	,
Nové	Nové	k2eAgNnSc1d1	Nové
Divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Oldřich	Oldřich	k1gMnSc1	Oldřich
Nový	nový	k2eAgMnSc1d1	nový
1944	[number]	k4	1944
B.	B.	kA	B.
Corry	Corra	k1gFnSc2	Corra
<g/>
,	,	kIx,	,
G.	G.	kA	G.
Achille	Achilles	k1gMnSc5	Achilles
<g/>
,	,	kIx,	,
O.	O.	kA	O.
Nový	nový	k2eAgInSc1d1	nový
<g/>
:	:	kIx,	:
Jedenáctý	jedenáctý	k4xOgInSc4	jedenáctý
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
<g/>
,	,	kIx,	,
Štěpán	Štěpán	k1gMnSc1	Štěpán
<g />
.	.	kIx.	.
</s>
<s>
Krapka	krapka	k1gFnSc1	krapka
<g/>
,	,	kIx,	,
Nové	Nové	k2eAgNnSc1d1	Nové
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Oldřich	Oldřich	k1gMnSc1	Oldřich
Nový	nový	k2eAgMnSc1d1	nový
1945	[number]	k4	1945
N.	N.	kA	N.
V.	V.	kA	V.
Gogol	Gogol	k1gMnSc1	Gogol
<g/>
,	,	kIx,	,
A.	A.	kA	A.
Jiránek	Jiránek	k1gMnSc1	Jiránek
<g/>
:	:	kIx,	:
Ženitba	ženitba	k1gFnSc1	ženitba
<g/>
,	,	kIx,	,
Podkolesin	Podkolesin	k1gInSc1	Podkolesin
<g/>
,	,	kIx,	,
Nové	Nové	k2eAgNnSc1d1	Nové
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Oldřich	Oldřich	k1gMnSc1	Oldřich
Nový	nový	k2eAgMnSc1d1	nový
1946	[number]	k4	1946
E.	E.	kA	E.
Pugliesse	Pugliesse	k1gFnSc2	Pugliesse
<g/>
,	,	kIx,	,
S.	S.	kA	S.
E.	E.	kA	E.
Nováček	Nováček	k1gMnSc1	Nováček
<g/>
:	:	kIx,	:
Chůdy	chůda	k1gFnSc2	chůda
pana	pan	k1gMnSc4	pan
Celestýna	Celestýn	k1gMnSc4	Celestýn
<g/>
,	,	kIx,	,
titul	titul	k1gInSc4	titul
<g/>
<g />
.	.	kIx.	.
</s>
<s>
role	role	k1gFnSc1	role
<g/>
,	,	kIx,	,
Nové	Nové	k2eAgNnSc1d1	Nové
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Oldřich	Oldřich	k1gMnSc1	Oldřich
Nový	Nový	k1gMnSc1	Nový
1947	[number]	k4	1947
Carlo	Carlo	k1gNnSc1	Carlo
Goldoni	Goldon	k1gMnPc1	Goldon
<g/>
:	:	kIx,	:
Lhář	lhář	k1gMnSc1	lhář
<g/>
,	,	kIx,	,
Lelio	Lelio	k1gMnSc1	Lelio
<g/>
,	,	kIx,	,
Nové	Nové	k2eAgNnSc1d1	Nové
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Oldřich	Oldřich	k1gMnSc1	Oldřich
Nový	Nový	k1gMnSc1	Nový
1949	[number]	k4	1949
Glenn	Glenn	k1gNnSc1	Glenn
Webster	Webstra	k1gFnPc2	Webstra
<g/>
,	,	kIx,	,
Harry	Harra	k1gFnSc2	Harra
Macourek	Macourek	k1gMnSc1	Macourek
<g/>
:	:	kIx,	:
Plukovník	plukovník	k1gMnSc1	plukovník
chce	chtít	k5eAaImIp3nS	chtít
spát	spát	k5eAaImF	spát
<g/>
,	,	kIx,	,
Tommy	Tommo	k1gNnPc7	Tommo
Garsson	Garsson	k1gInSc1	Garsson
<g/>
,	,	kIx,	,
plukovník	plukovník	k1gMnSc1	plukovník
letectva	letectvo	k1gNnSc2	letectvo
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Divadlo	divadlo	k1gNnSc1	divadlo
v	v	k7c6	v
Karlíně	Karlín	k1gInSc6	Karlín
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Oldřich	Oldřich	k1gMnSc1	Oldřich
Nový	nový	k2eAgMnSc1d1	nový
1953	[number]	k4	1953
J.	J.	kA	J.
K.	K.	kA	K.
Tyl	Tyl	k1gMnSc1	Tyl
<g/>
,	,	kIx,	,
F.	F.	kA	F.
Škroup	Škroup	k1gInSc1	Škroup
<g/>
:	:	kIx,	:
Fidlovačka	Fidlovačka	k1gFnSc1	Fidlovačka
<g/>
,	,	kIx,	,
Divadlo	divadlo	k1gNnSc1	divadlo
v	v	k7c6	v
Karlíně	Karlín	k1gInSc6	Karlín
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Oldřich	Oldřich	k1gMnSc1	Oldřich
Nový	nový	k2eAgMnSc1d1	nový
1954	[number]	k4	1954
Hudba	hudba	k1gFnSc1	hudba
Hervé	Hervá	k1gFnSc2	Hervá
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
a	a	k8xC	a
úprava	úprava	k1gFnSc1	úprava
Oldřich	Oldřich	k1gMnSc1	Oldřich
Nový	nový	k2eAgMnSc1d1	nový
<g/>
:	:	kIx,	:
Mam	mam	k1gInSc1	mam
<g/>
'	'	kIx"	'
<g/>
zelle	zelle	k1gFnSc1	zelle
Nitouche	Nitouche	k1gFnSc1	Nitouche
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Divadlo	divadlo	k1gNnSc1	divadlo
v	v	k7c6	v
Karlíně	Karlín	k1gInSc6	Karlín
<g/>
,	,	kIx,	,
role	role	k1gFnSc1	role
Célestina	Célestina	k1gFnSc1	Célestina
1955	[number]	k4	1955
Oskar	Oskar	k1gMnSc1	Oskar
Nedbal	Nedbal	k1gMnSc1	Nedbal
<g/>
,	,	kIx,	,
Leo	Leo	k1gMnSc1	Leo
Stein	Stein	k1gMnSc1	Stein
<g/>
:	:	kIx,	:
Polská	polský	k2eAgFnSc1d1	polská
krev	krev	k1gFnSc1	krev
<g/>
,	,	kIx,	,
Divadlo	divadlo	k1gNnSc1	divadlo
v	v	k7c6	v
Karlíně	Karlín	k1gInSc6	Karlín
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Oldřich	Oldřich	k1gMnSc1	Oldřich
Nový	nový	k2eAgMnSc1d1	nový
1956	[number]	k4	1956
J.	J.	kA	J.
N.	N.	kA	N.
Nestroy	Nestroa	k1gFnPc1	Nestroa
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Frýda	Frýda	k1gMnSc1	Frýda
<g/>
:	:	kIx,	:
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
Pán	pán	k1gMnSc1	pán
z	z	k7c2	z
Landeku	Landek	k1gInSc2	Landek
<g/>
,	,	kIx,	,
zvaný	zvaný	k2eAgInSc1d1	zvaný
<g />
.	.	kIx.	.
</s>
<s>
rozpolcený	rozpolcený	k2eAgMnSc1d1	rozpolcený
<g/>
,	,	kIx,	,
Divadlo	divadlo	k1gNnSc1	divadlo
v	v	k7c6	v
Karlíně	Karlín	k1gInSc6	Karlín
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Oldřich	Oldřich	k1gMnSc1	Oldřich
Nový	nový	k2eAgMnSc1d1	nový
1957	[number]	k4	1957
J.	J.	kA	J.
Strauss	Straussa	k1gFnPc2	Straussa
<g/>
,	,	kIx,	,
H.	H.	kA	H.
Meilhac	Meilhac	k1gInSc1	Meilhac
<g/>
,	,	kIx,	,
C.	C.	kA	C.
Haffner	Haffner	k1gInSc1	Haffner
<g/>
,	,	kIx,	,
E.	E.	kA	E.
Halévy	Haléva	k1gFnPc1	Haléva
<g/>
,	,	kIx,	,
R.	R.	kA	R.
Genée	Genée	k1gFnSc1	Genée
<g/>
:	:	kIx,	:
Netopýr	netopýr	k1gMnSc1	netopýr
<g/>
,	,	kIx,	,
Divadlo	divadlo	k1gNnSc1	divadlo
v	v	k7c6	v
Karlíně	Karlín	k1gInSc6	Karlín
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Oldřich	Oldřich	k1gMnSc1	Oldřich
Nový	nový	k2eAgMnSc1d1	nový
1964	[number]	k4	1964
Rudolf	Rudolf	k1gMnSc1	Rudolf
Friml	Friml	k1gMnSc1	Friml
<g/>
,	,	kIx,	,
H.	H.	kA	H.
Stothart	Stothart	k1gInSc1	Stothart
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
O.	O.	kA	O.
Harbach	Harbach	k1gInSc1	Harbach
<g/>
,	,	kIx,	,
O.	O.	kA	O.
Hammerstein	Hammerstein	k1gMnSc1	Hammerstein
<g/>
:	:	kIx,	:
Rose	Rose	k1gMnSc1	Rose
Marie	Marie	k1gFnSc1	Marie
<g/>
,	,	kIx,	,
Divadlo	divadlo	k1gNnSc1	divadlo
v	v	k7c6	v
Karlíně	Karlín	k1gInSc6	Karlín
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Oldřich	Oldřich	k1gMnSc1	Oldřich
Nový	nový	k2eAgMnSc1d1	nový
1969	[number]	k4	1969
<g/>
–	–	k?	–
<g/>
1973	[number]	k4	1973
Pavel	Pavel	k1gMnSc1	Pavel
Landovský	Landovský	k2eAgMnSc1d1	Landovský
<g/>
:	:	kIx,	:
Hodinový	hodinový	k2eAgMnSc1d1	hodinový
hoteliér	hoteliér	k1gMnSc1	hoteliér
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Evald	Evald	k1gMnSc1	Evald
Schorm	Schorm	k1gInSc1	Schorm
<g/>
,	,	kIx,	,
Činoherní	činoherní	k2eAgInSc1d1	činoherní
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
role	role	k1gFnPc1	role
Hanzla	Hanzl	k1gMnSc2	Hanzl
Praha	Praha	k1gFnSc1	Praha
1974	[number]	k4	1974
Bakaláři	bakalář	k1gMnPc1	bakalář
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
povídka	povídka	k1gFnSc1	povídka
"	"	kIx"	"
<g/>
Výlet	výlet	k1gInSc1	výlet
<g/>
"	"	kIx"	"
1971	[number]	k4	1971
Taková	takový	k3xDgFnSc1	takový
normální	normální	k2eAgFnSc1d1	normální
rodinka	rodinka	k1gFnSc1	rodinka
1970	[number]	k4	1970
Muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
rozdával	rozdávat	k5eAaImAgMnS	rozdávat
smích	smích	k1gInSc4	smích
1969	[number]	k4	1969
Světáci	Světák	k1gMnPc1	Světák
1966	[number]	k4	1966
Čertouská	Čertouský	k2eAgFnSc1d1	Čertouský
poudačka	poudačka	k1gFnSc1	poudačka
(	(	kIx(	(
<g/>
televizní	televizní	k2eAgInSc1d1	televizní
film	film	k1gInSc1	film
<g/>
)	)	kIx)	)
1966	[number]	k4	1966
Fantom	fantom	k1gInSc1	fantom
Morrisvillu	Morrisvill	k1gInSc2	Morrisvill
1965	[number]	k4	1965
Alibi	alibi	k1gNnSc2	alibi
na	na	k7c6	na
vodě	voda	k1gFnSc6	voda
1965	[number]	k4	1965
Káťa	Káťa	k1gFnSc1	Káťa
a	a	k8xC	a
krokodýl	krokodýl	k1gMnSc1	krokodýl
1962	[number]	k4	1962
Dva	dva	k4xCgMnPc1	dva
z	z	k7c2	z
onoho	onen	k3xDgInSc2	onen
světa	svět	k1gInSc2	svět
1960	[number]	k4	1960
Bílá	bílý	k2eAgFnSc1d1	bílá
spona	spona	k1gFnSc1	spona
1960	[number]	k4	1960
Kde	kde	k6eAd1	kde
alibi	alibi	k1gNnSc4	alibi
<g />
.	.	kIx.	.
</s>
<s>
nestačí	stačit	k5eNaBmIp3nS	stačit
1958	[number]	k4	1958
O	o	k7c6	o
věcech	věc	k1gFnPc6	věc
nadpřirozených	nadpřirozený	k2eAgFnPc2d1	nadpřirozená
1955	[number]	k4	1955
Nechte	nechat	k5eAaPmRp2nP	nechat
to	ten	k3xDgNnSc4	ten
na	na	k7c4	na
mně	já	k3xPp1nSc6	já
1954	[number]	k4	1954
Hudba	hudba	k1gFnSc1	hudba
z	z	k7c2	z
Marsu	Mars	k1gInSc2	Mars
1952	[number]	k4	1952
Slovo	slovo	k1gNnSc4	slovo
dělá	dělat	k5eAaImIp3nS	dělat
ženu	žena	k1gFnSc4	žena
1949	[number]	k4	1949
Pytlákova	pytlákův	k2eAgFnSc1d1	Pytlákova
schovanka	schovanka	k1gFnSc1	schovanka
1947	[number]	k4	1947
Parohy	paroh	k1gInPc7	paroh
1945	[number]	k4	1945
Jenom	jenom	k9	jenom
krok	krok	k1gInSc4	krok
1944	[number]	k4	1944
Paklíč	paklíč	k1gInSc1	paklíč
1944	[number]	k4	1944
Sobota	sobota	k1gFnSc1	sobota
1942	[number]	k4	1942
Valentin	Valentin	k1gMnSc1	Valentin
Dobrotivý	dobrotivý	k2eAgMnSc1d1	dobrotivý
1941	[number]	k4	1941
Hotel	hotel	k1gInSc1	hotel
Modrá	modrat	k5eAaImIp3nS	modrat
hvězda	hvězda	k1gFnSc1	hvězda
1941	[number]	k4	1941
Roztomilý	roztomilý	k2eAgMnSc1d1	roztomilý
člověk	člověk	k1gMnSc1	člověk
1940	[number]	k4	1940
Když	když	k8xS	když
Burian	Burian	k1gMnSc1	Burian
prášil	prášil	k1gMnSc1	prášil
/	/	kIx~	/
Baron	baron	k1gMnSc1	baron
Prášil	prášit	k5eAaImAgMnS	prášit
1940	[number]	k4	1940
Dívka	dívka	k1gFnSc1	dívka
v	v	k7c6	v
<g />
.	.	kIx.	.
</s>
<s>
modrém	modrý	k2eAgNnSc6d1	modré
1940	[number]	k4	1940
Život	život	k1gInSc1	život
je	být	k5eAaImIp3nS	být
krásný	krásný	k2eAgInSc1d1	krásný
1940	[number]	k4	1940
Přítelkyně	přítelkyně	k1gFnSc2	přítelkyně
pana	pan	k1gMnSc4	pan
ministra	ministr	k1gMnSc4	ministr
1939	[number]	k4	1939
Dědečkem	dědeček	k1gMnSc7	dědeček
proti	proti	k7c3	proti
své	svůj	k3xOyFgFnSc3	svůj
vůli	vůle	k1gFnSc3	vůle
1939	[number]	k4	1939
Eva	Eva	k1gFnSc1	Eva
tropí	tropit	k5eAaImIp3nS	tropit
hlouposti	hloupost	k1gFnSc2	hloupost
1939	[number]	k4	1939
Kristian	Kristian	k1gMnSc1	Kristian
1938	[number]	k4	1938
Třetí	třetí	k4xOgNnSc1	třetí
zvonění	zvonění	k1gNnSc1	zvonění
1937	[number]	k4	1937
Advokátka	advokátka	k1gFnSc1	advokátka
Věra	Věra	k1gFnSc1	Věra
1937	[number]	k4	1937
Důvod	důvod	k1gInSc1	důvod
k	k	k7c3	k
rozvodu	rozvod	k1gInSc3	rozvod
1937	[number]	k4	1937
Falešná	falešný	k2eAgFnSc1d1	falešná
kočička	kočička	k1gFnSc1	kočička
1936	[number]	k4	1936
Na	na	k7c4	na
tý	tý	k0	tý
louce	louka	k1gFnSc6	louka
zelený	zelený	k2eAgInSc1d1	zelený
1936	[number]	k4	1936
Rozkošný	rozkošný	k2eAgInSc1d1	rozkošný
příběh	příběh	k1gInSc1	příběh
1936	[number]	k4	1936
Uličnice	uličnice	k1gFnSc2	uličnice
1936	[number]	k4	1936
Velbloud	velbloud	k1gMnSc1	velbloud
uchem	ucho	k1gNnSc7	ucho
jehly	jehla	k1gFnSc2	jehla
1934	[number]	k4	1934
Rozpustilá	rozpustilý	k2eAgFnSc1d1	rozpustilá
noc	noc	k1gFnSc1	noc
<g />
.	.	kIx.	.
</s>
<s>
1922	[number]	k4	1922
Neznámá	známý	k2eNgFnSc1d1	neznámá
kráska	kráska	k1gFnSc1	kráska
Mezi	mezi	k7c7	mezi
mnoha	mnoho	k4c7	mnoho
rozhlasovými	rozhlasový	k2eAgFnPc7d1	rozhlasová
nahrávkami	nahrávka	k1gFnPc7	nahrávka
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgNnPc6	který
Oldřich	Oldřich	k1gMnSc1	Oldřich
Nový	nový	k2eAgMnSc1d1	nový
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
<g/>
,	,	kIx,	,
vyniká	vynikat	k5eAaImIp3nS	vynikat
rozhlasová	rozhlasový	k2eAgFnSc1d1	rozhlasová
nahrávka	nahrávka	k1gFnSc1	nahrávka
operety	opereta	k1gFnSc2	opereta
Mamzelle	Mamzelle	k1gFnSc1	Mamzelle
Nitouche	Nitouche	k1gFnSc1	Nitouche
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
na	na	k7c6	na
základě	základ	k1gInSc6	základ
představení	představení	k1gNnSc2	představení
Divadla	divadlo	k1gNnSc2	divadlo
v	v	k7c6	v
Karlíně	Karlín	k1gInSc6	Karlín
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
1953	[number]	k4	1953
<g/>
/	/	kIx~	/
<g/>
54	[number]	k4	54
(	(	kIx(	(
<g/>
překlad	překlad	k1gInSc1	překlad
<g/>
,	,	kIx,	,
úprava	úprava	k1gFnSc1	úprava
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
a	a	k8xC	a
role	role	k1gFnSc2	role
Célestina	Célestina	k1gFnSc1	Célestina
Oldřich	Oldřich	k1gMnSc1	Oldřich
Nový	nový	k2eAgMnSc1d1	nový
<g/>
)	)	kIx)	)
Oldřich	Oldřich	k1gMnSc1	Oldřich
Nový	Nový	k1gMnSc1	Nový
byl	být	k5eAaImAgMnS	být
autorem	autor	k1gMnSc7	autor
řady	řada	k1gFnSc2	řada
písňových	písňový	k2eAgInPc2d1	písňový
textů	text	k1gInPc2	text
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
pocházel	pocházet	k5eAaImAgMnS	pocházet
již	již	k6eAd1	již
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1928	[number]	k4	1928
</s>
