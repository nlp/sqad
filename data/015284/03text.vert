<s>
Slivenec	Slivenec	k1gMnSc1
</s>
<s>
Praha-Slivenec	Praha-Slivenec	k1gInSc1
Obecní	obecní	k2eAgInSc1d1
úřad	úřad	k1gInSc1
a	a	k8xC
pošta	pošta	k1gFnSc1
</s>
<s>
znakvlajka	znakvlajka	k1gFnSc1
Lokalita	lokalita	k1gFnSc1
Status	status	k1gInSc1
</s>
<s>
městská	městský	k2eAgFnSc1d1
část	část	k1gFnSc1
Správní	správní	k2eAgFnSc1d1
obvod	obvod	k1gInSc4
</s>
<s>
Praha	Praha	k1gFnSc1
5	#num#	k4
Statutární	statutární	k2eAgInSc4d1
město	město	k1gNnSc1
</s>
<s>
Praha	Praha	k1gFnSc1
Historická	historický	k2eAgFnSc1d1
země	země	k1gFnSc1
</s>
<s>
Čechy	Čechy	k1gFnPc1
Zeměpisné	zeměpisný	k2eAgFnSc2d1
souřadnice	souřadnice	k1gFnSc2
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
1	#num#	k4
<g/>
′	′	k?
<g/>
27	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
14	#num#	k4
<g/>
°	°	k?
<g/>
21	#num#	k4
<g/>
′	′	k?
<g/>
20	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Základní	základní	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Počet	počet	k1gInSc4
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
3	#num#	k4
106	#num#	k4
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Rozloha	rozloha	k1gFnSc1
</s>
<s>
7,59	7,59	k4
km²	km²	k?
PSČ	PSČ	kA
</s>
<s>
154	#num#	k4
00	#num#	k4
Počet	počet	k1gInSc1
domů	domů	k6eAd1
</s>
<s>
991	#num#	k4
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Počet	počet	k1gInSc1
částí	část	k1gFnPc2
obce	obec	k1gFnSc2
</s>
<s>
2	#num#	k4
Počet	počet	k1gInSc1
k.	k.	k?
ú.	ú.	k?
</s>
<s>
2	#num#	k4
Počet	počet	k1gInSc1
ZSJ	ZSJ	kA
</s>
<s>
9	#num#	k4
Kontakt	kontakt	k1gInSc1
Adresa	adresa	k1gFnSc1
úřadu	úřad	k1gInSc2
MČ	MČ	kA
</s>
<s>
K	k	k7c3
Lochkovu	Lochkov	k1gInSc3
6	#num#	k4
<g/>
Slivenec	Slivenec	k1gInSc1
-	-	kIx~
Praha	Praha	k1gFnSc1
5154	#num#	k4
00	#num#	k4
slivenec@praha-slivenec.cz	slivenec@praha-slivenec.cza	k1gFnPc2
Starostka	starostka	k1gFnSc1
</s>
<s>
Jana	Jana	k1gFnSc1
Plamínková	plamínkový	k2eAgFnSc1d1
(	(	kIx(
<g/>
STAN	stan	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
<g/>
:	:	kIx,
www.praha-slivenec.cz	www.praha-slivenec.cz	k1gInSc1
</s>
<s>
Praha-Slivenec	Praha-Slivenec	k1gMnSc1
</s>
<s>
Další	další	k2eAgInPc1d1
údaje	údaj	k1gInPc1
Kód	kód	k1gInSc4
MČ	MČ	kA
</s>
<s>
539678	#num#	k4
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Zdroje	zdroj	k1gInSc2
k	k	k7c3
infoboxu	infobox	k1gInSc3
a	a	k8xC
českým	český	k2eAgNnPc3d1
sídlům	sídlo	k1gNnPc3
<g/>
.	.	kIx.
<g/>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
SlivenecLokalita	SlivenecLokalita	k1gFnSc1
Městská	městský	k2eAgFnSc1d1
část	část	k1gFnSc1
</s>
<s>
Praha-Slivenec	Praha-Slivenec	k1gInSc1
Správní	správní	k2eAgInSc1d1
obvod	obvod	k1gInSc1
</s>
<s>
Praha	Praha	k1gFnSc1
5	#num#	k4
Obec	obec	k1gFnSc1
</s>
<s>
Praha	Praha	k1gFnSc1
Základní	základní	k2eAgFnSc2d1
informace	informace	k1gFnSc2
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
2	#num#	k4
655	#num#	k4
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
PSČ	PSČ	kA
</s>
<s>
154	#num#	k4
00	#num#	k4
Počet	počet	k1gInSc1
domů	domů	k6eAd1
</s>
<s>
841	#num#	k4
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Počet	počet	k1gInSc1
ZSJ	ZSJ	kA
</s>
<s>
6	#num#	k4
</s>
<s>
Další	další	k2eAgInPc1d1
údaje	údaj	k1gInPc1
Kód	kód	k1gInSc4
části	část	k1gFnPc1
obce	obec	k1gFnSc2
</s>
<s>
150592	#num#	k4
Kód	kód	k1gInSc1
k.	k.	k?
ú.	ú.	k?
</s>
<s>
750590	#num#	k4
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Zdroje	zdroj	k1gInSc2
k	k	k7c3
infoboxu	infobox	k1gInSc3
a	a	k8xC
českým	český	k2eAgNnPc3d1
sídlům	sídlo	k1gNnPc3
<g/>
.	.	kIx.
<g/>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Slivenec	Slivenec	k1gInSc1
(	(	kIx(
<g/>
německy	německy	k6eAd1
Sliwenetz	Sliwenetz	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
katastrální	katastrální	k2eAgNnSc4d1
území	území	k1gNnSc4
Prahy	Praha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
evidováno	evidovat	k5eAaImNgNnS
51	#num#	k4
ulic	ulice	k1gFnPc2
<g/>
,	,	kIx,
970	#num#	k4
adres	adresa	k1gFnPc2
a	a	k8xC
žijí	žít	k5eAaImIp3nP
zde	zde	k6eAd1
asi	asi	k9
3	#num#	k4
tisíce	tisíc	k4xCgInPc1
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
Praze	Praha	k1gFnSc3
byl	být	k5eAaImAgInS
připojen	připojen	k2eAgInSc1d1
roku	rok	k1gInSc2
1974	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Praha-Slivenec	Praha-Slivenec	k1gInSc1
je	být	k5eAaImIp3nS
od	od	k7c2
24	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1990	#num#	k4
městská	městský	k2eAgFnSc1d1
část	část	k1gFnSc1
na	na	k7c6
jihozápadě	jihozápad	k1gInSc6
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Prahy	Praha	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
městském	městský	k2eAgInSc6d1
obvodu	obvod	k1gInSc6
Praha	Praha	k1gFnSc1
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
3	#num#	k4
252	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
,	,	kIx,
rozloha	rozloha	k1gFnSc1
je	být	k5eAaImIp3nS
7,588	7,588	k4
<g/>
2	#num#	k4
km²	km²	k?
a	a	k8xC
zahrnuje	zahrnovat	k5eAaImIp3nS
celá	celý	k2eAgFnSc1d1
katastrální	katastrální	k2eAgFnSc1d1
území	území	k1gNnSc4
Slivenec	Slivenec	k1gInSc1
a	a	k8xC
Holyně	Holyně	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Menší	malý	k2eAgFnPc4d2
části	část	k1gFnPc4
území	území	k1gNnSc2
Holyně	Holyně	k1gFnSc2
byly	být	k5eAaImAgInP
k	k	k7c3
Praze	Praha	k1gFnSc6
připojeny	připojit	k5eAaPmNgFnP
již	již	k6eAd1
v	v	k7c6
letech	léto	k1gNnPc6
1960	#num#	k4
a	a	k8xC
1968	#num#	k4
<g/>
,	,	kIx,
hlavní	hlavní	k2eAgFnSc1d1
část	část	k1gFnSc1
Holyně	Holyně	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1974	#num#	k4
v	v	k7c6
rámci	rámec	k1gInSc6
obce	obec	k1gFnSc2
Slivenec	Slivenec	k1gInSc1
<g/>
,	,	kIx,
k	k	k7c3
níž	jenž	k3xRgFnSc3
Holyně	Holyně	k1gFnSc2
patřila	patřit	k5eAaImAgFnS
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
centru	centrum	k1gNnSc6
Slivence	Slivence	k1gFnSc2
se	se	k3xPyFc4
kolem	kolem	k7c2
náměstíčka	náměstíčko	k1gNnSc2
nachází	nacházet	k5eAaImIp3nS
kostel	kostel	k1gInSc4
Všech	všecek	k3xTgFnPc2
svatých	svatá	k1gFnPc2
a	a	k8xC
Křižovnický	Křižovnický	k2eAgInSc1d1
dvůr	dvůr	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bývalý	bývalý	k2eAgInSc1d1
statek	statek	k1gInSc1
JZD	JZD	kA
byl	být	k5eAaImAgInS
v	v	k7c6
roce	rok	k1gInSc6
2014	#num#	k4
stržen	strhnout	k5eAaPmNgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
území	území	k1gNnSc6
místní	místní	k2eAgFnSc2d1
části	část	k1gFnSc2
se	se	k3xPyFc4
stavějí	stavět	k5eAaImIp3nP
i	i	k9
nové	nový	k2eAgInPc4d1
domy	dům	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2015	#num#	k4
prošla	projít	k5eAaPmAgFnS
náves	náves	k1gFnSc4
celkovou	celkový	k2eAgFnSc7d1
rekonstrukcí	rekonstrukce	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
posledních	poslední	k2eAgNnPc6d1
letech	léto	k1gNnPc6
proběhla	proběhnout	k5eAaPmAgFnS
plynofikace	plynofikace	k1gFnSc1
celé	celý	k2eAgFnSc2d1
obce	obec	k1gFnSc2
a	a	k8xC
dokončuje	dokončovat	k5eAaImIp3nS
se	se	k3xPyFc4
také	také	k9
kanalizace	kanalizace	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každé	každý	k3xTgFnSc6
3	#num#	k4
měsíce	měsíc	k1gInSc2
vychází	vycházet	k5eAaImIp3nS
časopis	časopis	k1gInSc1
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
Slivenecký	slivenecký	k2eAgInSc4d1
mramor	mramor	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pamětihodnosti	pamětihodnost	k1gFnPc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Seznam	seznam	k1gInSc1
kulturních	kulturní	k2eAgFnPc2d1
památek	památka	k1gFnPc2
ve	v	k7c6
Slivenci	Slivence	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Kostel	kostel	k1gInSc1
Všech	všecek	k3xTgFnPc2
svatých	svatá	k1gFnPc2
</s>
<s>
Kostel	kostel	k1gInSc1
Všech	všecek	k3xTgFnPc2
svatých	svatá	k1gFnPc2
ve	v	k7c6
Slivenci	Slivence	k1gFnSc6
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Kostel	kostel	k1gInSc1
Všech	všecek	k3xTgFnPc2
svatých	svatá	k1gFnPc2
(	(	kIx(
<g/>
Slivenec	Slivenec	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Slivenecký	slivenecký	k2eAgInSc1d1
kostel	kostel	k1gInSc1
Všech	všecek	k3xTgFnPc2
svatých	svatá	k1gFnPc2
je	být	k5eAaImIp3nS
raně	raně	k6eAd1
gotická	gotický	k2eAgFnSc1d1
stavba	stavba	k1gFnSc1
z	z	k7c2
konce	konec	k1gInSc2
13	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc4
s	s	k7c7
přistavěnou	přistavěný	k2eAgFnSc7d1
předsíní	předsíň	k1gFnSc7
z	z	k7c2
roku	rok	k1gInSc2
1693	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1886	#num#	k4
byla	být	k5eAaImAgFnS
vestavěna	vestavěn	k2eAgFnSc1d1
kruchta	kruchta	k1gFnSc1
a	a	k8xC
roku	rok	k1gInSc2
1901	#num#	k4
byl	být	k5eAaImAgInS
kostel	kostel	k1gInSc4
opraven	opravna	k1gFnPc2
podle	podle	k7c2
návrhu	návrh	k1gInSc2
A.	A.	kA
Barvitia	Barvitius	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
obdélnou	obdélný	k2eAgFnSc4d1
loď	loď	k1gFnSc4
s	s	k7c7
věží	věž	k1gFnSc7
navazuje	navazovat	k5eAaImIp3nS
trojboce	trojboce	k6eAd1
zakončený	zakončený	k2eAgInSc1d1
presbytář	presbytář	k1gInSc1
s	s	k7c7
žebrovou	žebrový	k2eAgFnSc7d1
křížovou	křížový	k2eAgFnSc7d1
klenbou	klenba	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
presbytáři	presbytář	k1gInSc6
se	se	k3xPyFc4
zachovalo	zachovat	k5eAaPmAgNnS
prosté	prostý	k2eAgNnSc1d1
gotické	gotický	k2eAgNnSc1d1
sedile	sedile	k1gNnSc1
a	a	k8xC
sanktuárium	sanktuárium	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
lodi	loď	k1gFnSc6
jsou	být	k5eAaImIp3nP
lomená	lomený	k2eAgNnPc1d1
okna	okno	k1gNnPc1
s	s	k7c7
kopiemi	kopie	k1gFnPc7
původních	původní	k2eAgFnPc2d1
vitráží	vitráž	k1gFnPc2
ze	z	k7c2
14	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
(	(	kIx(
<g/>
originály	originál	k1gInPc1
jsou	být	k5eAaImIp3nP
v	v	k7c6
Uměleckoprůmyslovém	uměleckoprůmyslový	k2eAgNnSc6d1
muzeu	muzeum	k1gNnSc6
<g/>
)	)	kIx)
<g/>
;	;	kIx,
v	v	k7c6
českých	český	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
jsou	být	k5eAaImIp3nP
téměř	téměř	k6eAd1
unikátní	unikátní	k2eAgFnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Křižovnický	Křižovnický	k2eAgInSc1d1
dvůr	dvůr	k1gInSc1
</s>
<s>
Křižovnický	Křižovnický	k2eAgInSc1d1
dvůr	dvůr	k1gInSc1
ve	v	k7c6
Slivenci	Slivence	k1gFnSc6
</s>
<s>
Ves	ves	k1gFnSc1
Slivenec	Slivenec	k1gInSc1
věnoval	věnovat	k5eAaImAgMnS,k5eAaPmAgMnS
král	král	k1gMnSc1
Václav	Václav	k1gMnSc1
I.	I.	kA
listinou	listina	k1gFnSc7
ze	z	k7c2
6	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1253	#num#	k4
řádu	řád	k1gInSc2
křižovníků	křižovník	k1gMnPc2
s	s	k7c7
červenou	červený	k2eAgFnSc7d1
hvězdou	hvězda	k1gFnSc7
<g/>
,	,	kIx,
s	s	k7c7
čímž	což	k3yQnSc7,k3yRnSc7
byla	být	k5eAaImAgFnS
spojena	spojit	k5eAaPmNgFnS
povinnost	povinnost	k1gFnSc1
řádu	řád	k1gInSc2
pečovat	pečovat	k5eAaImF
o	o	k7c4
opravy	oprava	k1gFnPc4
Juditina	Juditin	k2eAgInSc2d1
mostu	most	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Bývalý	bývalý	k2eAgInSc1d1
majetek	majetek	k1gInSc1
křižovnického	křižovnický	k2eAgInSc2d1
řádu	řád	k1gInSc2
zablokovaný	zablokovaný	k2eAgInSc4d1
pro	pro	k7c4
restituce	restituce	k1gFnPc4
tvoří	tvořit	k5eAaImIp3nS
7	#num#	k4
%	%	kIx~
rozlohy	rozloha	k1gFnSc2
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
Praha-Slivenec	Praha-Slivenec	k1gMnSc1
<g/>
,	,	kIx,
dalšími	další	k2eAgMnPc7d1
3	#num#	k4
hektary	hektar	k1gInPc4
přesahuje	přesahovat	k5eAaImIp3nS
do	do	k7c2
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
Praha	Praha	k1gFnSc1
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
ve	v	k7c6
správě	správa	k1gFnSc6
Pozemkového	pozemkový	k2eAgInSc2d1
fondu	fond	k1gInSc2
<g/>
,	,	kIx,
většinou	většinou	k6eAd1
jde	jít	k5eAaImIp3nS
o	o	k7c4
ornou	orný	k2eAgFnSc4d1
půdu	půda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zemědělský	zemědělský	k2eAgInSc1d1
dvůr	dvůr	k1gInSc1
užíval	užívat	k5eAaImAgInS
za	za	k7c2
socialismu	socialismus	k1gInSc2
státní	státní	k2eAgInSc1d1
statek	statek	k1gInSc1
<g/>
,	,	kIx,
nyní	nyní	k6eAd1
ho	on	k3xPp3gMnSc4
má	mít	k5eAaImIp3nS
od	od	k7c2
Pozemkového	pozemkový	k2eAgInSc2d1
fondu	fond	k1gInSc2
pronajatý	pronajatý	k2eAgInSc1d1
soukromá	soukromý	k2eAgFnSc1d1
zemědělská	zemědělský	k2eAgFnSc1d1
firma	firma	k1gFnSc1
Pražská	pražský	k2eAgFnSc1d1
agrární	agrární	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
s.	s.	k?
<g/>
r.	r.	kA
<g/>
o.	o.	k?
Dvůr	Dvůr	k1gInSc1
chátrá	chátrat	k5eAaImIp3nS
<g/>
,	,	kIx,
správce	správce	k1gMnSc1
ani	ani	k8xC
nájemce	nájemce	k1gMnSc1
do	do	k7c2
něj	on	k3xPp3gMnSc2
dlouhodobě	dlouhodobě	k6eAd1
neinvestují	investovat	k5eNaBmIp3nP
<g/>
,	,	kIx,
teprve	teprve	k6eAd1
po	po	k7c6
medializaci	medializace	k1gFnSc6
tohoto	tento	k3xDgInSc2
stavu	stav	k1gInSc2
starostkou	starostka	k1gFnSc7
začal	začít	k5eAaPmAgInS
Pozemkový	pozemkový	k2eAgInSc1d1
fond	fond	k1gInSc1
vyměňovat	vyměňovat	k5eAaImF
střechu	střecha	k1gFnSc4
<g/>
,	,	kIx,
aniž	aniž	k8xS,k8xC
by	by	kYmCp3nS
vyměnil	vyměnit	k5eAaPmAgMnS
zpuchřelé	zpuchřelý	k2eAgInPc4d1
trámy	trám	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Městská	městský	k2eAgFnSc1d1
část	část	k1gFnSc1
dlouhodobě	dlouhodobě	k6eAd1
usiluje	usilovat	k5eAaImIp3nS
o	o	k7c4
navrácení	navrácení	k1gNnSc4
zdejšího	zdejší	k2eAgInSc2d1
majetku	majetek	k1gInSc2
řádu	řád	k1gInSc2
křižovníků	křižovník	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Těžba	těžba	k1gFnSc1
kamene	kámen	k1gInSc2
</s>
<s>
V	v	k7c6
lomu	lom	k1gInSc6
nad	nad	k7c7
samotou	samota	k1gFnSc7
Cikánka	cikánka	k1gFnSc1
se	se	k3xPyFc4
těží	těžet	k5eAaImIp3nS
červený	červený	k2eAgInSc1d1
vápenec	vápenec	k1gInSc1
<g/>
,	,	kIx,
tzv.	tzv.	kA
slivenecký	slivenecký	k2eAgInSc1d1
mramor	mramor	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejstarší	starý	k2eAgFnSc7d3
známou	známý	k2eAgFnSc7d1
dochovanou	dochovaný	k2eAgFnSc7d1
pražskou	pražský	k2eAgFnSc7d1
památkou	památka	k1gFnSc7
z	z	k7c2
červeného	červený	k2eAgInSc2d1
mramoru	mramor	k1gInSc2
<g/>
,	,	kIx,
pravděpodobně	pravděpodobně	k6eAd1
sliveneckého	slivenecký	k2eAgInSc2d1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
některé	některý	k3yIgFnPc4
desky	deska	k1gFnPc4
na	na	k7c6
hrobech	hrob	k1gInPc6
14	#num#	k4
biskupů	biskup	k1gMnPc2
v	v	k7c6
chóru	chór	k1gInSc6
chrámu	chrám	k1gInSc2
sv.	sv.	kA
Víta	Vít	k1gMnSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc4,k3yQgNnSc4,k3yRgNnSc4
nechal	nechat	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1374	#num#	k4
položit	položit	k5eAaPmF
Beneš	Beneš	k1gMnSc1
Krabice	krabice	k1gFnSc2
z	z	k7c2
Veitmile	Veitmila	k1gFnSc3
<g/>
,	,	kIx,
a	a	k8xC
pravděpodobně	pravděpodobně	k6eAd1
ze	z	k7c2
Slivence	Slivence	k1gFnSc2
pochází	pocházet	k5eAaImIp3nS
i	i	k9
mramor	mramor	k1gInSc1
řady	řada	k1gFnSc2
dalších	další	k2eAgInPc2d1
náhrobků	náhrobek	k1gInPc2
ze	z	k7c2
14	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
15	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
<g/>
,	,	kIx,
například	například	k6eAd1
svatojiřské	svatojiřský	k2eAgFnPc1d1
abatyše	abatyše	k1gFnPc1
v	v	k7c6
křížové	křížový	k2eAgFnSc6d1
chodbě	chodba	k1gFnSc6
Svatojiřského	svatojiřský	k2eAgInSc2d1
kláštera	klášter	k1gInSc2
na	na	k7c6
Pražském	pražský	k2eAgInSc6d1
hradě	hrad	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
a	a	k8xC
v	v	k7c6
období	období	k1gNnSc6
renesance	renesance	k1gFnSc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
hojně	hojně	k6eAd1
a	a	k8xC
všestranně	všestranně	k6eAd1
využívaným	využívaný	k2eAgInSc7d1
a	a	k8xC
žádaným	žádaný	k2eAgInSc7d1
kamenem	kámen	k1gInSc7
<g/>
,	,	kIx,
největším	veliký	k2eAgInSc7d3
objektem	objekt	k1gInSc7
byla	být	k5eAaImAgFnS
Krocínova	Krocínův	k2eAgFnSc1d1
kašna	kašna	k1gFnSc1
v	v	k7c6
Praze	Praha	k1gFnSc6
na	na	k7c6
Staroměstském	staroměstský	k2eAgNnSc6d1
náměstí	náměstí	k1gNnSc6
o	o	k7c6
výše	vysoce	k6eAd2
6	#num#	k4
metrů	metr	k1gInPc2
a	a	k8xC
stejném	stejný	k2eAgInSc6d1
průměru	průměr	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Nejstarší	starý	k2eAgFnSc1d3
písemná	písemný	k2eAgFnSc1d1
zmínka	zmínka	k1gFnSc1
o	o	k7c6
těžbě	těžba	k1gFnSc6
mramoru	mramor	k1gInSc2
ve	v	k7c6
Slivenci	Slivence	k1gFnSc6
je	být	k5eAaImIp3nS
z	z	k7c2
roku	rok	k1gInSc2
1515	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křižovníci	křižovník	k1gMnPc1
své	svůj	k3xOyFgInPc4
lomy	lom	k1gInPc4
pronajímali	pronajímat	k5eAaImAgMnP
různým	různý	k2eAgMnPc3d1
kameníkům	kameník	k1gMnPc3
a	a	k8xC
firmám	firma	k1gFnPc3
<g/>
.	.	kIx.
23	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1923	#num#	k4
křižovníci	křižovník	k1gMnPc1
lom	lom	k1gInSc4
prodali	prodat	k5eAaPmAgMnP
firmě	firma	k1gFnSc3
Spojené	spojený	k2eAgFnSc2d1
pražské	pražský	k2eAgFnSc2d1
továrny	továrna	k1gFnSc2
na	na	k7c4
stavivo	stavivo	k1gNnSc4
<g/>
,	,	kIx,
a.s.	a.s.	k?
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
jej	on	k3xPp3gMnSc4
měla	mít	k5eAaImAgFnS
v	v	k7c6
nájmu	nájem	k1gInSc6
již	již	k6eAd1
od	od	k7c2
roku	rok	k1gInSc2
1895	#num#	k4
(	(	kIx(
<g/>
původně	původně	k6eAd1
pod	pod	k7c7
jménem	jméno	k1gNnSc7
Barta	Barta	k1gMnSc1
&	&	k?
Tichý	Tichý	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1946	#num#	k4
byl	být	k5eAaImAgInS
lom	lom	k1gInSc1
znárodněn	znárodnit	k5eAaPmNgInS
<g/>
,	,	kIx,
poté	poté	k6eAd1
privatizován	privatizován	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Doprava	doprava	k1gFnSc1
</s>
<s>
Mezi	mezi	k7c7
Slivencem	Slivenec	k1gInSc7
a	a	k8xC
Holyní	Holyně	k1gFnSc7
prochází	procházet	k5eAaImIp3nS
páteřní	páteřní	k2eAgFnSc1d1
výpadovka	výpadovka	k1gFnSc1
<g/>
,	,	kIx,
ulice	ulice	k1gFnSc1
K	k	k7c3
Barrandovu	Barrandov	k1gInSc3
<g/>
,	,	kIx,
která	který	k3yIgNnPc1,k3yRgNnPc1,k3yQgNnPc1
je	být	k5eAaImIp3nS
radiálním	radiální	k2eAgInSc7d1
napaječem	napaječ	k1gInSc7
Pražského	pražský	k2eAgInSc2d1
okruhu	okruh	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přímo	přímo	k6eAd1
přes	přes	k7c4
Slivenec	Slivenec	k1gInSc4
vede	vést	k5eAaImIp3nS
stará	starý	k2eAgFnSc1d1
silnice	silnice	k1gFnSc1
od	od	k7c2
Barrandova	Barrandov	k1gInSc2
do	do	k7c2
Lochkova	Lochkov	k1gInSc2
a	a	k8xC
Radotína	Radotín	k1gInSc2
<g/>
,	,	kIx,
z	z	k7c2
níž	jenž	k3xRgFnSc2
ve	v	k7c6
Slivenci	Slivenec	k1gMnSc6
a	a	k8xC
v	v	k7c6
Lochkově	Lochkův	k2eAgInSc6d1
odbočují	odbočovat	k5eAaImIp3nP
silnice	silnice	k1gFnPc1
do	do	k7c2
Velké	velký	k2eAgFnSc2d1
Chuchle	Chuchle	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
sever	sever	k1gInSc4
vede	vést	k5eAaImIp3nS
ze	z	k7c2
Slivence	Slivence	k1gFnSc2
silnice	silnice	k1gFnPc1
do	do	k7c2
Holyně	Holyně	k1gFnSc2
<g/>
,	,	kIx,
na	na	k7c4
severozápad	severozápad	k1gInSc4
do	do	k7c2
Ořecha	Ořech	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Do	do	k7c2
Slivence	Slivence	k1gFnSc2
po	po	k7c4
léta	léto	k1gNnPc4
jezdily	jezdit	k5eAaImAgInP
městské	městský	k2eAgFnSc2d1
autobusové	autobusový	k2eAgFnSc2d1
linky	linka	k1gFnSc2
246	#num#	k4
<g/>
,	,	kIx,
247	#num#	k4
a	a	k8xC
248	#num#	k4
ze	z	k7c2
Smíchova	Smíchov	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
2	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2006	#num#	k4
byla	být	k5eAaImAgFnS
od	od	k7c2
Stodůlek	stodůlka	k1gFnPc2
a	a	k8xC
Řeporyj	Řeporyj	k1gInSc1
přes	přes	k7c4
Slivenec	Slivenec	k1gInSc4
na	na	k7c4
Barrandov	Barrandov	k1gInSc4
prodloužena	prodloužen	k2eAgFnSc1d1
v	v	k7c6
pracovních	pracovní	k2eAgInPc6d1
dnech	den	k1gInPc6
linka	linka	k1gFnSc1
230	#num#	k4
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
vytvořila	vytvořit	k5eAaPmAgFnS
zcela	zcela	k6eAd1
nové	nový	k2eAgNnSc4d1
spojení	spojení	k1gNnSc4
MHD	MHD	kA
<g/>
,	,	kIx,
později	pozdě	k6eAd2
byl	být	k5eAaImAgInS
provoz	provoz	k1gInSc1
v	v	k7c6
tomto	tento	k3xDgInSc6
úseku	úsek	k1gInSc6
omezen	omezit	k5eAaPmNgInS
jen	jen	k9
na	na	k7c4
dopravní	dopravní	k2eAgFnPc4d1
špičky	špička	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současnosti	současnost	k1gFnSc6
jezdí	jezdit	k5eAaImIp3nS
tímto	tento	k3xDgInSc7
směrem	směr	k1gInSc7
linka	linka	k1gFnSc1
130	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
rámci	rámec	k1gInSc6
rozsáhlých	rozsáhlý	k2eAgFnPc2d1
změn	změna	k1gFnPc2
1	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2012	#num#	k4
byly	být	k5eAaImAgFnP
dosavadní	dosavadní	k2eAgFnPc1d1
linky	linka	k1gFnPc1
246	#num#	k4
a	a	k8xC
247	#num#	k4
nahrazeny	nahradit	k5eAaPmNgInP
prodloužením	prodloužení	k1gNnSc7
linky	linka	k1gFnSc2
120	#num#	k4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
jezdí	jezdit	k5eAaImIp3nS
nyní	nyní	k6eAd1
ze	z	k7c2
Smíchova	Smíchov	k1gInSc2
(	(	kIx(
<g/>
Na	na	k7c4
Knížecí	knížecí	k2eAgInPc4d1
<g/>
)	)	kIx)
do	do	k7c2
Lochkova	Lochkov	k1gInSc2
<g/>
,	,	kIx,
část	část	k1gFnSc1
spojů	spoj	k1gInPc2
jezdí	jezdit	k5eAaImIp3nS
až	až	k9
do	do	k7c2
Radotína	Radotín	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Linka	linka	k1gFnSc1
248	#num#	k4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
jezdila	jezdit	k5eAaImAgFnS
přes	přes	k7c4
Slivenec	Slivenec	k1gInSc4
do	do	k7c2
Holyně	Holyně	k1gFnSc2
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
nahrazena	nahradit	k5eAaPmNgFnS
změněnou	změněný	k2eAgFnSc7d1
linkou	linka	k1gFnSc7
230	#num#	k4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
začala	začít	k5eAaPmAgFnS
zajíždět	zajíždět	k5eAaImF
místo	místo	k7c2
linky	linka	k1gFnSc2
248	#num#	k4
do	do	k7c2
Holyně	Holyně	k1gFnSc2
a	a	k8xC
místo	místo	k7c2
linky	linka	k1gFnSc2
120	#num#	k4
do	do	k7c2
Klukovic	Klukovice	k1gFnPc2
a	a	k8xC
na	na	k7c6
barrandovské	barrandovský	k2eAgFnSc6d1
straně	strana	k1gFnSc6
byla	být	k5eAaImAgFnS
prodloužena	prodloužit	k5eAaPmNgFnS
z	z	k7c2
Chaplinova	Chaplinův	k2eAgNnSc2d1
náměstí	náměstí	k1gNnSc2
k	k	k7c3
filmovým	filmový	k2eAgInPc3d1
ateliérům	ateliér	k1gInPc3
<g/>
.	.	kIx.
</s>
<s>
Zajímavosti	zajímavost	k1gFnPc1
</s>
<s>
Na	na	k7c6
fotbalovém	fotbalový	k2eAgNnSc6d1
hřišti	hřiště	k1gNnSc6
ve	v	k7c6
Slivenci	Slivence	k1gFnSc6
se	se	k3xPyFc4
točilo	točit	k5eAaImAgNnS
utkání	utkání	k1gNnSc1
ve	v	k7c6
fotbale	fotbal	k1gInSc6
v	v	k7c6
Kameňáku	Kameňáku	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
areálu	areál	k1gInSc6
Křižovnického	Křižovnický	k2eAgInSc2d1
dvora	dvůr	k1gInSc2
se	se	k3xPyFc4
točil	točit	k5eAaImAgMnS
film	film	k1gInSc4
Utrpení	utrpení	k1gNnSc2
mladého	mladý	k2eAgMnSc2d1
Boháčka	Boháček	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Fotogalerie	Fotogalerie	k1gFnSc1
</s>
<s>
Náves	náves	k1gFnSc1
s	s	k7c7
kostelem	kostel	k1gInSc7
</s>
<s>
Náves	náves	k1gFnSc1
s	s	k7c7
rybníkem	rybník	k1gInSc7
</s>
<s>
Náves	náves	k1gFnSc1
s	s	k7c7
domy	dům	k1gInPc7
u	u	k7c2
rybníka	rybník	k1gInSc2
</s>
<s>
Kostel	kostel	k1gInSc1
Všech	všecek	k3xTgFnPc2
svatých	svatá	k1gFnPc2
</s>
<s>
Křižovnický	Křižovnický	k2eAgInSc1d1
dvůr	dvůr	k1gInSc1
</s>
<s>
Budova	budova	k1gFnSc1
základní	základní	k2eAgFnSc2d1
a	a	k8xC
meteřské	meteřský	k2eAgFnSc2d1
školy	škola	k1gFnSc2
ve	v	k7c6
Slivenci	Slivence	k1gFnSc6
</s>
<s>
Sousední	sousední	k2eAgFnPc1d1
čtvrti	čtvrt	k1gFnPc1
</s>
<s>
Holyně	Holyně	k1gFnSc1
</s>
<s>
Lochkov	Lochkov	k1gInSc1
</s>
<s>
Barrandov	Barrandov	k1gInSc1
</s>
<s>
Velká	velký	k2eAgFnSc1d1
Chuchle	Chuchle	k1gFnSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Historický	historický	k2eAgInSc1d1
lexikon	lexikon	k1gInSc1
obcí	obec	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
–	–	k?
1869	#num#	k4
<g/>
–	–	k?
<g/>
2011	#num#	k4
<g/>
.	.	kIx.
21	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Územní	územní	k2eAgInSc4d1
plán	plán	k1gInSc4
a	a	k8xC
rozvoj	rozvoj	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úřad	úřad	k1gInSc1
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
Praha-Slivenec	Praha-Slivenec	k1gMnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
E.	E.	kA
Poche	Poch	k1gFnSc2
<g/>
,	,	kIx,
Umělecké	umělecký	k2eAgFnSc2d1
památky	památka	k1gFnSc2
Čech	Čechy	k1gFnPc2
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Str	str	kA
<g/>
.	.	kIx.
363	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Václav	Václav	k1gMnSc1
Rybařík	rybařík	k1gMnSc1
<g/>
:	:	kIx,
Z	z	k7c2
minulosti	minulost	k1gFnSc2
pražských	pražský	k2eAgInPc2d1
lomů	lom	k1gInPc2
(	(	kIx(
<g/>
4	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
revue	revue	k1gFnSc1
Kámen	kámen	k1gInSc1
<g/>
↑	↑	k?
Jana	Jana	k1gFnSc1
Plamínková	plamínkový	k2eAgFnSc1d1
<g/>
:	:	kIx,
Archivováno	archivovat	k5eAaBmNgNnS
24	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
2014	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gMnSc5
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
E.	E.	kA
Poche	Poche	k1gFnPc2
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Umělecké	umělecký	k2eAgFnSc2d1
památky	památka	k1gFnSc2
Čech	Čechy	k1gFnPc2
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
1980	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
ŠUBERT	Šubert	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
Adolf	Adolf	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čechy	Čech	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ilustrace	ilustrace	k1gFnSc1
Karel	Karla	k1gFnPc2
Liebscher	Liebschra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svazek	svazek	k1gInSc1
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Otto	Otto	k1gMnSc1
<g/>
,	,	kIx,
[	[	kIx(
<g/>
po	po	k7c6
r.	r.	kA
1880	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Slivenec	Slivenec	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
426	#num#	k4
<g/>
-	-	kIx~
<g/>
427	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
HANSL	HANSL	kA
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Smíchovsko	Smíchovsko	k1gNnSc1
a	a	k8xC
Zbraslavsko	Zbraslavsko	k1gNnSc1
:	:	kIx,
společnou	společný	k2eAgFnSc7d1
prací	práce	k1gFnSc7
učitelstva	učitelstvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Smíchov	Smíchov	k1gInSc1
<g/>
:	:	kIx,
vl	vl	k?
<g/>
.	.	kIx.
<g/>
n.	n.	k?
<g/>
,	,	kIx,
1899	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Slivenec	Slivenec	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
349	#num#	k4
<g/>
-	-	kIx~
<g/>
355	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Barrandien	barrandien	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Praha-Slivenec	Praha-Slivenec	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Katastrální	katastrální	k2eAgFnSc1d1
mapa	mapa	k1gFnSc1
Slivenec	Slivenec	k1gInSc1
na	na	k7c6
webu	web	k1gInSc6
ČÚZK	ČÚZK	kA
</s>
<s>
Archivní	archivní	k2eAgFnPc1d1
mapy	mapa	k1gFnPc1
na	na	k7c6
webu	web	k1gInSc6
ČÚZK	ČÚZK	kA
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Praha	Praha	k1gFnSc1
–	–	k?
hlavní	hlavní	k2eAgNnSc4d1
město	město	k1gNnSc4
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
městské	městský	k2eAgInPc1d1
obvody	obvod	k1gInPc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
–	–	k?
<g/>
10	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
správní	správní	k2eAgInPc1d1
obvody	obvod	k1gInPc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
–	–	k?
<g/>
22	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
(	(	kIx(
<g/>
57	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Praha	Praha	k1gFnSc1
1	#num#	k4
</s>
<s>
Praha	Praha	k1gFnSc1
1	#num#	k4
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
1	#num#	k4
<g/>
)	)	kIx)
Praha	Praha	k1gFnSc1
2	#num#	k4
</s>
<s>
Praha	Praha	k1gFnSc1
2	#num#	k4
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
2	#num#	k4
<g/>
)	)	kIx)
Praha	Praha	k1gFnSc1
3	#num#	k4
</s>
<s>
Praha	Praha	k1gFnSc1
3	#num#	k4
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
3	#num#	k4
<g/>
)	)	kIx)
Praha	Praha	k1gFnSc1
4	#num#	k4
</s>
<s>
Praha	Praha	k1gFnSc1
4	#num#	k4
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
4	#num#	k4
<g/>
,	,	kIx,
Praha-Kunratice	Praha-Kunratice	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
11	#num#	k4
(	(	kIx(
<g/>
část	část	k1gFnSc1
<g/>
:	:	kIx,
Praha	Praha	k1gFnSc1
11	#num#	k4
<g/>
,	,	kIx,
Praha-Šeberov	Praha-Šeberov	k1gInSc1
<g/>
,	,	kIx,
Praha-Újezd	Praha-Újezd	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
12	#num#	k4
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
12	#num#	k4
<g/>
,	,	kIx,
Praha-Libuš	Praha-Libuš	k1gInSc1
<g/>
)	)	kIx)
Praha	Praha	k1gFnSc1
5	#num#	k4
</s>
<s>
Praha	Praha	k1gFnSc1
5	#num#	k4
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
5	#num#	k4
<g/>
,	,	kIx,
Praha-Slivenec	Praha-Slivenec	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
13	#num#	k4
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
13	#num#	k4
<g/>
,	,	kIx,
Praha-Řeporyje	Praha-Řeporyje	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
16	#num#	k4
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
16	#num#	k4
<g/>
,	,	kIx,
Praha-Velká	Praha-Velký	k2eAgFnSc1d1
Chuchle	Chuchle	k1gFnSc1
<g/>
,	,	kIx,
Praha-Lochkov	Praha-Lochkov	k1gInSc1
<g/>
,	,	kIx,
Praha-Zbraslav	Praha-Zbraslav	k1gMnSc1
<g/>
,	,	kIx,
Praha-Lipence	Praha-Lipence	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
17	#num#	k4
(	(	kIx(
<g/>
část	část	k1gFnSc1
<g/>
:	:	kIx,
Praha-Zličín	Praha-Zličín	k1gInSc1
<g/>
)	)	kIx)
Praha	Praha	k1gFnSc1
6	#num#	k4
</s>
<s>
Praha	Praha	k1gFnSc1
6	#num#	k4
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
6	#num#	k4
<g/>
,	,	kIx,
Praha-Suchdol	Praha-Suchdol	k1gInSc1
<g/>
,	,	kIx,
Praha-Lysolaje	Praha-Lysolaje	k1gFnSc1
<g/>
,	,	kIx,
Praha-Nebušice	Praha-Nebušice	k1gFnSc1
<g/>
,	,	kIx,
Praha-Přední	Praha-Přední	k2eAgFnSc1d1
Kopanina	kopanina	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
17	#num#	k4
(	(	kIx(
<g/>
část	část	k1gFnSc1
<g/>
:	:	kIx,
Praha	Praha	k1gFnSc1
17	#num#	k4
<g/>
)	)	kIx)
Praha	Praha	k1gFnSc1
7	#num#	k4
</s>
<s>
Praha	Praha	k1gFnSc1
7	#num#	k4
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
7	#num#	k4
<g/>
,	,	kIx,
Praha-Troja	Praha-Troja	k1gFnSc1
<g/>
)	)	kIx)
Praha	Praha	k1gFnSc1
8	#num#	k4
</s>
<s>
Praha	Praha	k1gFnSc1
8	#num#	k4
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
8	#num#	k4
<g/>
,	,	kIx,
Praha-Ďáblice	Praha-Ďáblice	k1gFnSc1
<g/>
,	,	kIx,
Praha-Březiněves	Praha-Březiněves	k1gInSc1
<g/>
,	,	kIx,
Praha-Dolní	Praha-Dolní	k2eAgInPc1d1
Chabry	Chabr	k1gInPc1
<g/>
)	)	kIx)
Praha	Praha	k1gFnSc1
9	#num#	k4
</s>
<s>
Praha	Praha	k1gFnSc1
9	#num#	k4
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
9	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
14	#num#	k4
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
14	#num#	k4
<g/>
,	,	kIx,
Praha-Dolní	Praha-Dolní	k2eAgFnPc4d1
Počernice	Počernice	k1gFnPc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
18	#num#	k4
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
18	#num#	k4
<g/>
,	,	kIx,
Praha-Čakovice	Praha-Čakovice	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
19	#num#	k4
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
19	#num#	k4
<g/>
,	,	kIx,
Praha-Vinoř	Praha-Vinoř	k1gFnSc1
<g/>
,	,	kIx,
Praha-Satalice	Praha-Satalice	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
20	#num#	k4
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
20	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
21	#num#	k4
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
21	#num#	k4
<g/>
,	,	kIx,
Praha-Klánovice	Praha-Klánovice	k1gFnSc1
<g/>
,	,	kIx,
Praha-Koloděje	Praha-Koloděje	k1gFnSc1
<g/>
,	,	kIx,
Praha-Běchovice	Praha-Běchovice	k1gFnSc1
<g/>
)	)	kIx)
Praha	Praha	k1gFnSc1
10	#num#	k4
</s>
<s>
Praha	Praha	k1gFnSc1
10	#num#	k4
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
10	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
15	#num#	k4
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
15	#num#	k4
<g/>
,	,	kIx,
Praha-Dolní	Praha-Dolní	k2eAgInPc1d1
Měcholupy	Měcholup	k1gInPc1
<g/>
,	,	kIx,
Praha-Štěrboholy	Praha-Štěrbohol	k1gInPc1
<g/>
,	,	kIx,
Praha-Petrovice	Praha-Petrovice	k1gFnPc1
<g/>
,	,	kIx,
Praha-Dubeč	Praha-Dubeč	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
22	#num#	k4
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
22	#num#	k4
<g/>
,	,	kIx,
Praha-Královice	Praha-Královice	k1gFnSc1
<g/>
,	,	kIx,
Praha-Nedvězí	Praha-Nedvězí	k1gNnPc1
<g/>
,	,	kIx,
Praha-Kolovraty	Praha-Kolovrat	k1gInPc1
<g/>
,	,	kIx,
Praha-Benice	Praha-Benice	k1gFnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
11	#num#	k4
(	(	kIx(
<g/>
část	část	k1gFnSc1
<g/>
:	:	kIx,
Praha-Křeslice	Praha-Křeslice	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
katastrální	katastrální	k2eAgNnPc1d1
území	území	k1gNnPc1
a	a	k8xC
části	část	k1gFnPc1
obce	obec	k1gFnSc2
(	(	kIx(
<g/>
112	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Běchovice	Běchovice	k1gFnPc1
•	•	k?
Benice	Benice	k1gFnSc1
•	•	k?
Bohnice	Bohnice	k1gInPc1
•	•	k?
Braník	Braník	k1gInSc1
•	•	k?
Břevnov	Břevnov	k1gInSc1
•	•	k?
Březiněves	Březiněves	k1gInSc1
•	•	k?
Bubeneč	Bubeneč	k1gInSc1
•	•	k?
Čakovice	Čakovice	k1gFnSc2
•	•	k?
Černý	černý	k2eAgInSc1d1
Most	most	k1gInSc1
•	•	k?
Čimice	Čimice	k1gFnSc2
•	•	k?
Dejvice	Dejvice	k1gFnPc1
•	•	k?
Dolní	dolní	k2eAgFnPc1d1
Chabry	Chabra	k1gFnPc1
•	•	k?
Dolní	dolní	k2eAgFnPc1d1
Měcholupy	Měcholupa	k1gFnPc1
•	•	k?
Dolní	dolní	k2eAgFnPc1d1
Počernice	Počernice	k1gFnPc1
•	•	k?
Dubeč	Dubeč	k1gMnSc1
•	•	k?
Ďáblice	ďáblice	k1gFnSc2
•	•	k?
Háje	háj	k1gInSc2
•	•	k?
Hájek	Hájek	k1gMnSc1
u	u	k7c2
Uhříněvsi	Uhříněves	k1gFnSc2
(	(	kIx(
<g/>
dř	dř	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hájek	Hájek	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
Hloubětín	Hloubětín	k1gInSc1
•	•	k?
Hlubočepy	Hlubočepa	k1gFnSc2
•	•	k?
Hodkovičky	Hodkovička	k1gFnSc2
•	•	k?
Holešovice	Holešovice	k1gFnPc1
(	(	kIx(
<g/>
dř	dř	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Holešovice-Bubny	Holešovice-Bubna	k1gFnPc1
<g/>
)	)	kIx)
•	•	k?
Holyně	Holyně	k1gFnSc2
•	•	k?
Horní	horní	k2eAgFnSc2d1
Měcholupy	Měcholupa	k1gFnSc2
•	•	k?
Horní	horní	k2eAgFnPc1d1
Počernice	Počernice	k1gFnPc1
•	•	k?
Hostavice	Hostavice	k1gFnSc1
•	•	k?
Hostivař	Hostivař	k1gFnSc1
•	•	k?
Hradčany	Hradčany	k1gInPc1
•	•	k?
Hrdlořezy	Hrdlořezy	k1gInPc1
•	•	k?
Chodov	Chodov	k1gInSc1
•	•	k?
Cholupice	Cholupice	k1gFnSc2
•	•	k?
Jinonice	Jinonice	k1gFnPc4
•	•	k?
Josefov	Josefov	k1gInSc1
•	•	k?
Kamýk	Kamýk	k1gInSc1
•	•	k?
Karlín	Karlín	k1gInSc1
•	•	k?
Kbely	Kbely	k1gInPc1
•	•	k?
Klánovice	Klánovice	k1gFnPc4
•	•	k?
Kobylisy	Kobylisy	k1gInPc1
•	•	k?
Koloděje	koloděj	k1gMnSc2
•	•	k?
Kolovraty	kolovrat	k1gInPc1
•	•	k?
Komořany	Komořan	k1gMnPc4
•	•	k?
Košíře	Košíře	k1gInPc1
•	•	k?
Královice	královic	k1gMnSc2
•	•	k?
Krč	Krč	k1gFnSc4
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Křeslice	Křeslice	k1gFnSc1
•	•	k?
Kunratice	Kunratice	k1gFnPc4
•	•	k?
Kyje	kyje	k1gFnPc4
•	•	k?
Lahovice	Lahovice	k1gFnSc2
•	•	k?
Letňany	Letňan	k1gMnPc4
•	•	k?
Lhotka	Lhotka	k1gFnSc1
•	•	k?
Libeň	Libeň	k1gFnSc1
•	•	k?
Liboc	Liboc	k1gFnSc1
•	•	k?
Libuš	Libuš	k1gInSc1
•	•	k?
Lipany	lipan	k1gMnPc7
•	•	k?
Lipence	Lipenec	k1gInSc2
•	•	k?
Lochkov	Lochkov	k1gInSc1
•	•	k?
Lysolaje	Lysolaje	k1gFnPc4
•	•	k?
Malá	Malá	k1gFnSc1
Chuchle	chuchel	k1gInSc2
•	•	k?
Malá	malý	k2eAgFnSc1d1
Strana	strana	k1gFnSc1
•	•	k?
Malešice	Malešice	k1gFnPc4
•	•	k?
Michle	Michl	k1gMnSc5
•	•	k?
Miškovice	Miškovice	k1gFnSc1
•	•	k?
Modřany	Modřany	k1gInPc1
•	•	k?
Motol	Motol	k1gInSc1
•	•	k?
Nebušice	Nebušice	k1gFnSc1
•	•	k?
Nedvězí	Nedvěze	k1gFnPc2
u	u	k7c2
Říčan	Říčany	k1gInPc2
(	(	kIx(
<g/>
dř	dř	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nedvězí	Nedvězí	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
Nové	Nové	k2eAgNnSc1d1
Město	město	k1gNnSc1
•	•	k?
Nusle	Nusle	k1gFnPc4
•	•	k?
Petrovice	Petrovice	k1gFnSc2
•	•	k?
Písnice	písnice	k1gFnSc2
•	•	k?
Pitkovice	Pitkovice	k1gFnSc1
•	•	k?
Podolí	Podolí	k1gNnSc2
•	•	k?
Prosek	proséct	k5eAaPmDgInS
•	•	k?
Přední	přední	k2eAgFnSc1d1
Kopanina	kopanina	k1gFnSc1
•	•	k?
Radlice	radlice	k1gFnSc2
•	•	k?
Radotín	Radotín	k1gMnSc1
•	•	k?
Ruzyně	Ruzyně	k1gFnSc2
•	•	k?
Řeporyje	Řeporyje	k1gFnSc2
•	•	k?
Řepy	řepa	k1gFnSc2
•	•	k?
Satalice	Satalice	k1gFnSc2
•	•	k?
Sedlec	Sedlec	k1gInSc1
•	•	k?
Slivenec	Slivenec	k1gInSc1
•	•	k?
Smíchov	Smíchov	k1gInSc1
•	•	k?
Sobín	Sobín	k1gMnSc1
•	•	k?
Staré	Staré	k2eAgNnSc1d1
Město	město	k1gNnSc1
•	•	k?
Stodůlky	stodůlka	k1gFnPc4
•	•	k?
Strašnice	Strašnice	k1gFnPc4
•	•	k?
Střešovice	Střešovice	k1gFnSc2
•	•	k?
Střížkov	Střížkov	k1gInSc1
•	•	k?
Suchdol	Suchdol	k1gInSc1
•	•	k?
Šeberov	Šeberov	k1gInSc1
•	•	k?
Štěrboholy	Štěrbohola	k1gFnSc2
•	•	k?
Točná	točný	k2eAgFnSc1d1
•	•	k?
Troja	Troja	k1gFnSc1
•	•	k?
Třebonice	Třebonice	k1gFnSc2
•	•	k?
Třeboradice	Třeboradice	k1gFnSc2
•	•	k?
Uhříněves	Uhříněves	k1gFnSc1
•	•	k?
Újezd	Újezd	k1gInSc1
nad	nad	k7c7
Lesy	les	k1gInPc7
•	•	k?
Újezd	Újezd	k1gInSc1
u	u	k7c2
Průhonic	Průhonice	k1gFnPc2
(	(	kIx(
<g/>
dř	dř	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Újezd	Újezd	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Veleslavín	Veleslavín	k1gMnSc1
•	•	k?
Velká	velká	k1gFnSc1
Chuchle	Chuchle	k1gFnSc2
•	•	k?
Vinohrady	Vinohrady	k1gInPc4
(	(	kIx(
<g/>
dř	dř	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Královské	královský	k2eAgInPc1d1
Vinohrady	Vinohrady	k1gInPc1
<g/>
)	)	kIx)
•	•	k?
Vinoř	Vinoř	k1gFnSc1
•	•	k?
Vokovice	Vokovice	k1gFnPc4
•	•	k?
Vršovice	Vršovice	k1gFnPc4
•	•	k?
Vysočany	Vysočany	k1gInPc1
•	•	k?
Vyšehrad	Vyšehrad	k1gInSc1
•	•	k?
Záběhlice	Záběhlice	k1gFnPc4
•	•	k?
Zadní	zadní	k2eAgFnSc1d1
Kopanina	kopanina	k1gFnSc1
•	•	k?
Zbraslav	Zbraslav	k1gFnSc1
•	•	k?
Zličín	Zličín	k1gInSc1
•	•	k?
Žižkov	Žižkov	k1gInSc1
O	o	k7c4
členění	členění	k1gNnSc4
Prahy	Praha	k1gFnSc2
na	na	k7c4
obvody	obvod	k1gInPc4
<g/>
,	,	kIx,
správní	správní	k2eAgInPc1d1
obvody	obvod	k1gInPc1
<g/>
,	,	kIx,
městské	městský	k2eAgInPc1d1
části	část	k1gFnPc4
a	a	k8xC
katastrální	katastrální	k2eAgNnSc4d1
území	území	k1gNnSc4
pojednává	pojednávat	k5eAaImIp3nS
článek	článek	k1gInSc1
Části	část	k1gFnSc2
Prahy	Praha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
137122	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Praha	Praha	k1gFnSc1
</s>
