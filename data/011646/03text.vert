<p>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
farnost	farnost	k1gFnSc1	farnost
Nová	nový	k2eAgFnSc1d1	nová
Bystřice	Bystřice	k1gFnSc1	Bystřice
je	být	k5eAaImIp3nS	být
územním	územní	k2eAgNnSc7d1	územní
společenstvím	společenství	k1gNnSc7	společenství
římských	římský	k2eAgMnPc2d1	římský
katolíků	katolík	k1gMnPc2	katolík
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
jindřichohradeckého	jindřichohradecký	k2eAgInSc2d1	jindřichohradecký
vikariátu	vikariát	k1gInSc2	vikariát
českobudějovické	českobudějovický	k2eAgFnSc2d1	českobudějovická
diecéze	diecéze	k1gFnSc2	diecéze
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
O	o	k7c6	o
farnosti	farnost	k1gFnSc6	farnost
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Historie	historie	k1gFnSc1	historie
===	===	k?	===
</s>
</p>
<p>
<s>
Plebánie	plebánie	k1gFnSc1	plebánie
v	v	k7c6	v
Nové	Nové	k2eAgFnSc6d1	Nové
Bystřici	Bystřice	k1gFnSc6	Bystřice
byla	být	k5eAaImAgFnS	být
zřízena	zřídit	k5eAaPmNgFnS	zřídit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1355	[number]	k4	1355
a	a	k8xC	a
původně	původně	k6eAd1	původně
náležela	náležet	k5eAaImAgFnS	náležet
do	do	k7c2	do
pasovské	pasovský	k2eAgFnSc2d1	pasovská
diecéze	diecéze	k1gFnSc2	diecéze
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
diecéze	diecéze	k1gFnSc2	diecéze
českobudějovické	českobudějovický	k2eAgFnSc2d1	českobudějovická
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1940	[number]	k4	1940
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
byla	být	k5eAaImAgFnS	být
nuceně	nuceně	k6eAd1	nuceně
spravována	spravovat	k5eAaImNgFnS	spravovat
z	z	k7c2	z
diecéze	diecéze	k1gFnSc2	diecéze
Sankt	Sankt	k1gInSc1	Sankt
Pölten	Pölten	k2eAgInSc1d1	Pölten
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
byla	být	k5eAaImAgNnP	být
navrácena	navrátit	k5eAaPmNgNnP	navrátit
do	do	k7c2	do
českobudějovické	českobudějovický	k2eAgFnSc2d1	českobudějovická
diecéze	diecéze	k1gFnSc2	diecéze
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Současnost	současnost	k1gFnSc4	současnost
===	===	k?	===
</s>
</p>
<p>
<s>
Farnost	farnost	k1gFnSc1	farnost
v	v	k7c6	v
Nové	Nové	k2eAgFnSc6d1	Nové
Bystřici	Bystřice	k1gFnSc6	Bystřice
neměla	mít	k5eNaImAgNnP	mít
do	do	k7c2	do
září	září	k1gNnSc2	září
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
sídelního	sídelní	k2eAgMnSc2d1	sídelní
duchovního	duchovní	k2eAgMnSc2d1	duchovní
správce	správce	k1gMnSc2	správce
<g/>
,	,	kIx,	,
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
administrována	administrován	k2eAgFnSc1d1	administrována
ex	ex	k6eAd1	ex
currendo	currendo	k6eAd1	currendo
z	z	k7c2	z
proboštství	proboštství	k1gNnSc2	proboštství
v	v	k7c6	v
Jindřichově	Jindřichův	k2eAgInSc6d1	Jindřichův
Hradci	Hradec	k1gInSc6	Hradec
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
29	[number]	k4	29
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
2016	[number]	k4	2016
byl	být	k5eAaImAgInS	být
opět	opět	k6eAd1	opět
ustanoven	ustanoven	k2eAgMnSc1d1	ustanoven
kněz	kněz	k1gMnSc1	kněz
přímo	přímo	k6eAd1	přímo
do	do	k7c2	do
Nové	Nové	k2eAgFnSc2d1	Nové
Bystřice	Bystřice	k1gFnSc2	Bystřice
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
rovněž	rovněž	k9	rovněž
administruje	administrovat	k5eAaImIp3nS	administrovat
ex	ex	k6eAd1	ex
currendo	currendo	k1gNnSc1	currendo
farnosti	farnost	k1gFnSc2	farnost
Číměř	Číměř	k1gInSc1	Číměř
<g/>
,	,	kIx,	,
Hůrky	hůrka	k1gFnPc1	hůrka
<g/>
,	,	kIx,	,
Klášter	klášter	k1gInSc1	klášter
a	a	k8xC	a
Staré	Staré	k2eAgNnSc1d1	Staré
Město	město	k1gNnSc1	město
pod	pod	k7c7	pod
Landštejnem	Landštejn	k1gInSc7	Landštejn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Římskokatolická	římskokatolický	k2eAgNnPc4d1	římskokatolické
farnost-proboštství	farnostroboštství	k1gNnPc4	farnost-proboštství
Jindřichův	Jindřichův	k2eAgInSc4d1	Jindřichův
Hradec	Hradec	k1gInSc4	Hradec
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Profil	profil	k1gInSc1	profil
farnosti	farnost	k1gFnSc2	farnost
na	na	k7c6	na
webu	web	k1gInSc6	web
Českobudějovické	českobudějovický	k2eAgFnSc2d1	českobudějovická
diecéze	diecéze	k1gFnSc2	diecéze
</s>
</p>
