<p>
<s>
Claire	Clair	k1gMnSc5	Clair
Danesová	Danesový	k2eAgFnSc1d1	Danesová
<g/>
,	,	kIx,	,
rodným	rodný	k2eAgNnSc7d1	rodné
jménem	jméno	k1gNnSc7	jméno
Claire	Clair	k1gInSc5	Clair
Catherine	Catherin	k1gInSc5	Catherin
Danes	Danesa	k1gFnPc2	Danesa
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
*	*	kIx~	*
12	[number]	k4	12
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1979	[number]	k4	1979
Manhattan	Manhattan	k1gInSc1	Manhattan
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americká	americký	k2eAgFnSc1d1	americká
divadelní	divadelní	k2eAgFnSc1d1	divadelní
<g/>
,	,	kIx,	,
televizní	televizní	k2eAgFnSc1d1	televizní
a	a	k8xC	a
filmová	filmový	k2eAgFnSc1d1	filmová
herečka	herečka	k1gFnSc1	herečka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Životopis	životopis	k1gInSc4	životopis
==	==	k?	==
</s>
</p>
<p>
<s>
Narodila	narodit	k5eAaPmAgFnS	narodit
se	se	k3xPyFc4	se
ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
Manhattan	Manhattan	k1gInSc1	Manhattan
v	v	k7c4	v
New	New	k1gFnPc4	New
York	York	k1gInSc4	York
City	city	k1gNnSc1	city
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
matka	matka	k1gFnSc1	matka
Carla	Carla	k1gFnSc1	Carla
(	(	kIx(	(
<g/>
rozená	rozený	k2eAgFnSc1d1	rozená
Hallová	Hallová	k1gFnSc1	Hallová
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
poskytovatelka	poskytovatelka	k1gFnSc1	poskytovatelka
denní	denní	k2eAgFnSc2d1	denní
péče	péče	k1gFnSc2	péče
<g/>
,	,	kIx,	,
malířka	malířka	k1gFnSc1	malířka
a	a	k8xC	a
návrhářka	návrhářka	k1gFnSc1	návrhářka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stala	stát	k5eAaPmAgFnS	stát
manažerkou	manažerka	k1gFnSc7	manažerka
své	svůj	k3xOyFgFnSc2	svůj
dcery	dcera	k1gFnSc2	dcera
<g/>
,	,	kIx,	,
a	a	k8xC	a
její	její	k3xOp3gMnSc1	její
otec	otec	k1gMnSc1	otec
Christopher	Christophra	k1gFnPc2	Christophra
Danes	Danes	k1gMnSc1	Danes
(	(	kIx(	(
<g/>
narozený	narozený	k2eAgMnSc1d1	narozený
6	[number]	k4	6
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1944	[number]	k4	1944
v	v	k7c6	v
Austinu	Austin	k1gInSc6	Austin
v	v	k7c6	v
Texasu	Texas	k1gInSc6	Texas
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
počítačový	počítačový	k2eAgMnSc1d1	počítačový
odborník	odborník	k1gMnSc1	odborník
a	a	k8xC	a
bývalý	bývalý	k2eAgMnSc1d1	bývalý
fotograf	fotograf	k1gMnSc1	fotograf
architektury	architektura	k1gFnSc2	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnSc1	její
dědeček	dědeček	k1gMnSc1	dědeček
z	z	k7c2	z
otcovy	otcův	k2eAgFnSc2d1	otcova
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
Gibson	Gibson	k1gMnSc1	Gibson
Andrew	Andrew	k1gMnSc1	Andrew
Danes	Danes	k1gMnSc1	Danes
(	(	kIx(	(
<g/>
1910	[number]	k4	1910
<g/>
–	–	k?	–
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
děkan	děkan	k1gMnSc1	děkan
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
architektury	architektura	k1gFnSc2	architektura
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
Yale	Yal	k1gInSc2	Yal
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
staršího	starý	k2eAgMnSc4d2	starší
bratra	bratr	k1gMnSc4	bratr
<g/>
,	,	kIx,	,
Asu	as	k1gInSc2	as
(	(	kIx(	(
<g/>
narozený	narozený	k2eAgMnSc1d1	narozený
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Navštěvovala	navštěvovat	k5eAaImAgFnS	navštěvovat
Dalton	Dalton	k1gInSc4	Dalton
School	Schoola	k1gFnPc2	Schoola
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
Professional	Professional	k1gFnSc1	Professional
Performing	Performing	k1gInSc1	Performing
Arts	Arts	k1gInSc1	Arts
School	Schoola	k1gFnPc2	Schoola
a	a	k8xC	a
Lycée	Lycée	k1gFnPc2	Lycée
Français	Français	k1gFnSc2	Français
de	de	k?	de
Los	los	k1gInSc1	los
Angeles	Angelesa	k1gFnPc2	Angelesa
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
ji	on	k3xPp3gFnSc4	on
přijali	přijmout	k5eAaPmAgMnP	přijmout
na	na	k7c4	na
univerzitu	univerzita	k1gFnSc4	univerzita
Yale	Yal	k1gInSc2	Yal
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
chodil	chodit	k5eAaImAgMnS	chodit
i	i	k9	i
její	její	k3xOp3gMnSc1	její
otec	otec	k1gMnSc1	otec
<g/>
.	.	kIx.	.
</s>
<s>
Režisér	režisér	k1gMnSc1	režisér
Oliver	Oliver	k1gMnSc1	Oliver
Stone	ston	k1gInSc5	ston
napsal	napsat	k5eAaPmAgInS	napsat
její	její	k3xOp3gInSc1	její
doporučující	doporučující	k2eAgInSc1d1	doporučující
dopis	dopis	k1gInSc1	dopis
na	na	k7c4	na
Yale	Yale	k1gFnSc4	Yale
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dvou	dva	k4xCgNnPc6	dva
letech	léto	k1gNnPc6	léto
studií	studie	k1gFnPc2	studie
zaměřených	zaměřený	k2eAgFnPc2d1	zaměřená
na	na	k7c6	na
psychologii	psychologie	k1gFnSc6	psychologie
se	se	k3xPyFc4	se
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
z	z	k7c2	z
Yalu	Yalus	k1gInSc2	Yalus
odejít	odejít	k5eAaPmF	odejít
a	a	k8xC	a
soustředit	soustředit	k5eAaPmF	soustředit
se	se	k3xPyFc4	se
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
filmovou	filmový	k2eAgFnSc4d1	filmová
kariéru	kariéra	k1gFnSc4	kariéra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Televize	televize	k1gFnSc1	televize
==	==	k?	==
</s>
</p>
<p>
<s>
Její	její	k3xOp3gFnSc1	její
první	první	k4xOgFnSc1	první
role	role	k1gFnSc1	role
bylo	být	k5eAaImAgNnS	být
hostování	hostování	k1gNnSc1	hostování
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
Právo	právo	k1gNnSc4	právo
a	a	k8xC	a
pořádek	pořádek	k1gInSc4	pořádek
v	v	k7c6	v
epizodě	epizoda	k1gFnSc6	epizoda
"	"	kIx"	"
<g/>
Skin	skin	k1gMnSc1	skin
Deep	Deep	k1gMnSc1	Deep
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
v	v	k7c6	v
epizodě	epizoda	k1gFnSc6	epizoda
nazvané	nazvaný	k2eAgNnSc1d1	nazvané
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Coming	Coming	k1gInSc1	Coming
out	out	k?	out
of	of	k?	of
Heidi	Heid	k1gMnPc1	Heid
Leiter	Leiter	k1gMnSc1	Leiter
<g/>
"	"	kIx"	"
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
HBO	HBO	kA	HBO
<g/>
,	,	kIx,	,
Lifestories	Lifestories	k1gMnSc1	Lifestories
<g/>
:	:	kIx,	:
Families	Families	k1gMnSc1	Families
in	in	k?	in
Crisis	Crisis	k1gInSc1	Crisis
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
epizoda	epizoda	k1gFnSc1	epizoda
natáčela	natáčet	k5eAaImAgFnS	natáčet
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
Claire	Clair	k1gInSc5	Clair
třináct	třináct	k4xCc4	třináct
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vysílala	vysílat	k5eAaImAgFnS	vysílat
se	se	k3xPyFc4	se
až	až	k9	až
po	po	k7c6	po
téměř	téměř	k6eAd1	téměř
roce	rok	k1gInSc6	rok
a	a	k8xC	a
půl	půl	k1xP	půl
<g/>
.	.	kIx.	.
</s>
<s>
Zahrála	zahrát	k5eAaPmAgFnS	zahrát
si	se	k3xPyFc3	se
patnáctiletou	patnáctiletý	k2eAgFnSc4d1	patnáctiletá
Angelu	Angela	k1gFnSc4	Angela
Chase	chasa	k1gFnSc3	chasa
v	v	k7c6	v
televizním	televizní	k2eAgInSc6d1	televizní
dramatickém	dramatický	k2eAgInSc6d1	dramatický
seriálu	seriál	k1gInSc6	seriál
Tak	tak	k9	tak
tohle	tenhle	k3xDgNnSc1	tenhle
je	být	k5eAaImIp3nS	být
můj	můj	k3xOp1gInSc1	můj
život	život	k1gInSc1	život
<g/>
,	,	kIx,	,
za	za	k7c4	za
který	který	k3yRgInSc4	který
získala	získat	k5eAaPmAgFnS	získat
Zlatý	zlatý	k2eAgInSc4d1	zlatý
glóbus	glóbus	k1gInSc4	glóbus
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
nominována	nominovat	k5eAaBmNgFnS	nominovat
na	na	k7c4	na
cenu	cena	k1gFnSc4	cena
Emmy	Emma	k1gFnSc2	Emma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
si	se	k3xPyFc3	se
zahrála	zahrát	k5eAaPmAgFnS	zahrát
hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
v	v	k7c6	v
životopisném	životopisný	k2eAgInSc6d1	životopisný
filmu	film	k1gInSc6	film
z	z	k7c2	z
produkce	produkce	k1gFnSc2	produkce
HBO	HBO	kA	HBO
<g/>
,	,	kIx,	,
Temple	templ	k1gInSc5	templ
Grandinová	Grandinový	k2eAgFnSc1d1	Grandinová
<g/>
,	,	kIx,	,
založeném	založený	k2eAgInSc6d1	založený
na	na	k7c6	na
osudu	osud	k1gInSc6	osud
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
autistické	autistický	k2eAgFnSc2d1	autistická
ženy	žena	k1gFnSc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
tuto	tento	k3xDgFnSc4	tento
roli	role	k1gFnSc4	role
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
Cenu	cena	k1gFnSc4	cena
Emmy	Emma	k1gFnSc2	Emma
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
herečku	herečka	k1gFnSc4	herečka
v	v	k7c6	v
minisérii	minisérie	k1gFnSc6	minisérie
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
filmu	film	k1gInSc6	film
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
Zlatý	zlatý	k2eAgInSc4d1	zlatý
glóbus	glóbus	k1gInSc4	glóbus
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
ženský	ženský	k2eAgInSc4d1	ženský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
minisérii	minisérie	k1gFnSc6	minisérie
nebo	nebo	k8xC	nebo
TV	TV	kA	TV
filmu	film	k1gInSc2	film
a	a	k8xC	a
Screen	Screen	k2eAgInSc4d1	Screen
Actors	Actors	k1gInSc4	Actors
Guild	Guilda	k1gFnPc2	Guilda
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
ženský	ženský	k2eAgInSc4d1	ženský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
televizním	televizní	k2eAgInSc6d1	televizní
filmu	film	k1gInSc6	film
nebo	nebo	k8xC	nebo
minisérii	minisérie	k1gFnSc6	minisérie
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
přijat	přijmout	k5eAaPmNgMnS	přijmout
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
i	i	k8xC	i
sama	sám	k3xTgMnSc4	sám
Grandin	Grandin	k2eAgInSc1d1	Grandin
chválila	chválit	k5eAaImAgFnS	chválit
Danesin	Danesin	k2eAgInSc4d1	Danesin
výkon	výkon	k1gInSc4	výkon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
začala	začít	k5eAaPmAgFnS	začít
účinkovat	účinkovat	k5eAaImF	účinkovat
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
Ve	v	k7c6	v
jménu	jméno	k1gNnSc6	jméno
vlasti	vlast	k1gFnSc2	vlast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ztvárnila	ztvárnit	k5eAaPmAgFnS	ztvárnit
agentku	agentka	k1gFnSc4	agentka
CIA	CIA	kA	CIA
<g/>
,	,	kIx,	,
trpící	trpící	k2eAgFnSc1d1	trpící
bipolární	bipolární	k2eAgFnSc7d1	bipolární
afektivní	afektivní	k2eAgFnSc7d1	afektivní
poruchou	porucha	k1gFnSc7	porucha
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
věděl	vědět	k5eAaImAgMnS	vědět
zaměstnavatel	zaměstnavatel	k1gMnSc1	zaměstnavatel
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
postava	postava	k1gFnSc1	postava
podezřívá	podezřívat	k5eAaImIp3nS	podezřívat
z	z	k7c2	z
plánování	plánování	k1gNnSc2	plánování
teroristického	teroristický	k2eAgInSc2d1	teroristický
útoku	útok	k1gInSc2	útok
dva	dva	k4xCgInPc1	dva
bývalé	bývalý	k2eAgInPc1d1	bývalý
příslušníky	příslušník	k1gMnPc7	příslušník
námořních	námořní	k2eAgFnPc2d1	námořní
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
byli	být	k5eAaImAgMnP	být
zajati	zajmout	k5eAaPmNgMnP	zajmout
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
a	a	k8xC	a
po	po	k7c6	po
osmi	osm	k4xCc6	osm
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
objevili	objevit	k5eAaPmAgMnP	objevit
zpět	zpět	k6eAd1	zpět
na	na	k7c6	na
americkém	americký	k2eAgNnSc6d1	americké
území	území	k1gNnSc6	území
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
hlavní	hlavní	k2eAgFnPc1d1	hlavní
role	role	k1gFnPc1	role
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
ztvárnili	ztvárnit	k5eAaPmAgMnP	ztvárnit
Mandy	Manda	k1gFnPc4	Manda
Patinkin	Patinkina	k1gFnPc2	Patinkina
a	a	k8xC	a
Damian	Damiana	k1gFnPc2	Damiana
Lewis	Lewis	k1gFnSc2	Lewis
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
roli	role	k1gFnSc4	role
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
Zlatý	zlatý	k2eAgInSc4d1	zlatý
glóbus	glóbus	k1gInSc4	glóbus
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
herečka	herečka	k1gFnSc1	herečka
v	v	k7c6	v
televizním	televizní	k2eAgInSc6d1	televizní
seriálu	seriál	k1gInSc6	seriál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Film	film	k1gInSc1	film
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
seriálu	seriál	k1gInSc2	seriál
Tak	tak	k8xS	tak
tohle	tenhle	k3xDgNnSc1	tenhle
je	být	k5eAaImIp3nS	být
můj	můj	k3xOp1gInSc4	můj
život	život	k1gInSc4	život
se	se	k3xPyFc4	se
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
působit	působit	k5eAaImF	působit
ve	v	k7c6	v
filmu	film	k1gInSc6	film
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
si	se	k3xPyFc3	se
zahrála	zahrát	k5eAaPmAgFnS	zahrát
Elizabeth	Elizabeth	k1gFnSc4	Elizabeth
"	"	kIx"	"
<g/>
Beth	Beth	k1gInSc4	Beth
<g/>
"	"	kIx"	"
Marchovou	Marchová	k1gFnSc4	Marchová
v	v	k7c6	v
adaptaci	adaptace	k1gFnSc6	adaptace
filmu	film	k1gInSc2	film
Malé	Malé	k2eAgFnSc2d1	Malé
ženy	žena	k1gFnSc2	žena
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
s	s	k7c7	s
Winonou	Winona	k1gFnSc7	Winona
Ryderovou	Ryderová	k1gFnSc7	Ryderová
<g/>
,	,	kIx,	,
Kirsten	Kirsten	k2eAgInSc1d1	Kirsten
Dunstovou	Dunstová	k1gFnSc7	Dunstová
<g/>
,	,	kIx,	,
Samanthou	Samantha	k1gFnSc7	Samantha
Mathisovou	Mathisový	k2eAgFnSc7d1	Mathisová
<g/>
,	,	kIx,	,
Trini	Trin	k1gMnPc1	Trin
Alvaradovou	Alvaradový	k2eAgFnSc4d1	Alvaradová
<g/>
,	,	kIx,	,
Christianem	Christian	k1gMnSc7	Christian
Balem	bal	k1gInSc7	bal
<g/>
,	,	kIx,	,
Susan	Susan	k1gInSc4	Susan
Sarandonovou	Sarandonová	k1gFnSc7	Sarandonová
a	a	k8xC	a
Gabrielem	Gabriel	k1gMnSc7	Gabriel
Byrnem	Byrn	k1gMnSc7	Byrn
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
jako	jako	k9	jako
dcera	dcera	k1gFnSc1	dcera
Holly	Holla	k1gFnSc2	Holla
Hunterové	Hunterová	k1gFnSc2	Hunterová
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Jodie	Jodie	k1gFnSc2	Jodie
Fosterové	Fosterová	k1gFnSc2	Fosterová
<g/>
,	,	kIx,	,
Domů	domů	k6eAd1	domů
na	na	k7c4	na
svátky	svátek	k1gInPc4	svátek
<g/>
.	.	kIx.	.
</s>
<s>
Hrála	hrát	k5eAaImAgFnS	hrát
společně	společně	k6eAd1	společně
s	s	k7c7	s
francouzskou	francouzský	k2eAgFnSc7d1	francouzská
herečkou	herečka	k1gFnSc7	herečka
Jeanne	Jeann	k1gInSc5	Jeann
Moreau	Moreaus	k1gInSc6	Moreaus
a	a	k8xC	a
Judem	judo	k1gNnSc7	judo
Lawem	Lawem	k1gInSc1	Lawem
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Má	mít	k5eAaImIp3nS	mít
mě	já	k3xPp1nSc4	já
rád	rád	k6eAd1	rád
<g/>
,	,	kIx,	,
nemá	mít	k5eNaImIp3nS	mít
mě	já	k3xPp1nSc4	já
rád	rád	k6eAd1	rád
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
si	se	k3xPyFc3	se
zahrála	zahrát	k5eAaPmAgFnS	zahrát
roli	role	k1gFnSc4	role
Rachel	Rachela	k1gFnPc2	Rachela
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Nezvaný	zvaný	k2eNgMnSc1d1	zvaný
host	host	k1gMnSc1	host
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Její	její	k3xOp3gFnPc1	její
první	první	k4xOgFnPc1	první
hlavní	hlavní	k2eAgFnPc1d1	hlavní
role	role	k1gFnPc1	role
ve	v	k7c6	v
filmu	film	k1gInSc6	film
přišla	přijít	k5eAaPmAgFnS	přijít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
Hrála	hrát	k5eAaImAgFnS	hrát
Julii	Julie	k1gFnSc4	Julie
Kapuletovou	Kapuletový	k2eAgFnSc4d1	Kapuletová
ve	v	k7c6	v
filmové	filmový	k2eAgFnSc6d1	filmová
adaptaci	adaptace	k1gFnSc6	adaptace
Sheakesperova	Sheakesperův	k2eAgMnSc4d1	Sheakesperův
Romea	Romeo	k1gMnSc4	Romeo
a	a	k8xC	a
Julie	Julie	k1gFnPc4	Julie
a	a	k8xC	a
roli	role	k1gFnSc4	role
Romea	Romeo	k1gMnSc4	Romeo
Monteka	Monteek	k1gMnSc4	Monteek
hrál	hrát	k5eAaImAgMnS	hrát
Leonardo	Leonardo	k1gMnSc1	Leonardo
DiCaprio	DiCaprio	k1gMnSc1	DiCaprio
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
bylo	být	k5eAaImAgNnS	být
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
hlavní	hlavní	k2eAgFnSc4d1	hlavní
ženskou	ženský	k2eAgFnSc4d1	ženská
roli	role	k1gFnSc4	role
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Titanic	Titanic	k1gInSc1	Titanic
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
ona	onen	k3xDgFnSc1	onen
sama	sám	k3xTgMnSc4	sám
říkala	říkat	k5eAaImAgFnS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
na	na	k7c4	na
roli	role	k1gFnSc4	role
zvažována	zvažován	k2eAgFnSc1d1	zvažována
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tato	tento	k3xDgFnSc1	tento
role	role	k1gFnSc1	role
ji	on	k3xPp3gFnSc4	on
nikdy	nikdy	k6eAd1	nikdy
nabídnuta	nabídnout	k5eAaPmNgFnS	nabídnout
nebyla	být	k5eNaImAgFnS	být
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
pracovala	pracovat	k5eAaImAgFnS	pracovat
po	po	k7c6	po
boku	bok	k1gInSc6	bok
dvou	dva	k4xCgMnPc2	dva
velice	velice	k6eAd1	velice
uznávaných	uznávaný	k2eAgMnPc2d1	uznávaný
režisérů	režisér	k1gMnPc2	režisér
<g/>
.	.	kIx.	.
</s>
<s>
Zahrála	zahrát	k5eAaPmAgFnS	zahrát
si	se	k3xPyFc3	se
týranou	týraný	k2eAgFnSc4d1	týraná
manželku	manželka	k1gFnSc4	manželka
Kelly	Kella	k1gFnSc2	Kella
Rikerovou	Rikerová	k1gFnSc4	Rikerová
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Vyvolávač	vyvolávač	k1gMnSc1	vyvolávač
deště	dešť	k1gInSc2	dešť
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
režíroval	režírovat	k5eAaImAgInS	režírovat
Francis	Francis	k1gInSc1	Francis
Ford	ford	k1gInSc1	ford
Coppola	Coppola	k1gFnSc1	Coppola
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
roli	role	k1gFnSc4	role
kýčovité	kýčovitý	k2eAgFnSc2d1	kýčovitá
a	a	k8xC	a
hloupé	hloupý	k2eAgFnSc2d1	hloupá
Jenny	Jenna	k1gFnSc2	Jenna
ve	v	k7c6	v
filmu	film	k1gInSc6	film
režiséra	režisér	k1gMnSc2	režisér
Olivera	Oliver	k1gMnSc2	Oliver
Stona	Stona	k1gFnSc1	Stona
U-Turn	U-Turn	k1gInSc1	U-Turn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
si	se	k3xPyFc3	se
zahrála	zahrát	k5eAaPmAgFnS	zahrát
dvě	dva	k4xCgFnPc4	dva
velmi	velmi	k6eAd1	velmi
odlišné	odlišný	k2eAgFnPc4d1	odlišná
role	role	k1gFnPc4	role
<g/>
:	:	kIx,	:
Cosettu	Cosett	k1gInSc2	Cosett
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Bídníci	bídník	k1gMnPc1	bídník
režiséra	režisér	k1gMnSc2	režisér
Bille	Bill	k1gMnSc5	Bill
Augusta	Augusta	k1gMnSc1	Augusta
a	a	k8xC	a
těhotnou	těhotný	k2eAgFnSc4d1	těhotná
dceru	dcera	k1gFnSc4	dcera
polských	polský	k2eAgMnPc2d1	polský
přistěhovalců	přistěhovalec	k1gMnPc2	přistěhovalec
(	(	kIx(	(
<g/>
hráli	hrát	k5eAaImAgMnP	hrát
je	on	k3xPp3gFnPc4	on
Gabriel	Gabriel	k1gMnSc1	Gabriel
Byrne	Byrn	k1gInSc5	Byrn
a	a	k8xC	a
Lena	Lena	k1gFnSc1	Lena
Olinová	Olinová	k1gFnSc1	Olinová
<g/>
)	)	kIx)	)
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Polská	polský	k2eAgFnSc1d1	polská
svatba	svatba	k1gFnSc1	svatba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
dabovala	dabovat	k5eAaBmAgFnS	dabovat
anglickou	anglický	k2eAgFnSc4d1	anglická
verzi	verze	k1gFnSc4	verze
animovaného	animovaný	k2eAgInSc2d1	animovaný
filmu	film	k1gInSc2	film
Princezna	princezna	k1gFnSc1	princezna
Mononoke	Mononoke	k1gFnSc1	Mononoke
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
si	se	k3xPyFc3	se
zahrála	zahrát	k5eAaPmAgFnS	zahrát
roli	role	k1gFnSc4	role
Julie	Julie	k1gFnSc2	Julie
Barnesové	Barnesový	k2eAgFnSc2d1	Barnesová
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Parchanti	parchant	k1gMnPc1	parchant
nebo	nebo	k8xC	nebo
poldové	polda	k1gMnPc1	polda
<g/>
?	?	kIx.	?
</s>
<s>
a	a	k8xC	a
získala	získat	k5eAaPmAgFnS	získat
hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Téměř	téměř	k6eAd1	téměř
bez	bez	k7c2	bez
šance	šance	k1gFnSc2	šance
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
objevili	objevit	k5eAaPmAgMnP	objevit
Kate	kat	k1gInSc5	kat
Beckinsale	Beckinsala	k1gFnSc6	Beckinsala
a	a	k8xC	a
Bill	Bill	k1gMnSc1	Bill
Pullman	Pullman	k1gMnSc1	Pullman
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
po	po	k7c4	po
nějaký	nějaký	k3yIgInSc4	nějaký
čas	čas	k1gInSc4	čas
opustila	opustit	k5eAaPmAgFnS	opustit
svou	svůj	k3xOyFgFnSc4	svůj
filmovou	filmový	k2eAgFnSc4d1	filmová
kariéru	kariéra	k1gFnSc4	kariéra
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
mohla	moct	k5eAaImAgFnS	moct
věnovat	věnovat	k5eAaImF	věnovat
studiím	studie	k1gFnPc3	studie
na	na	k7c4	na
Yalu	Yala	k1gFnSc4	Yala
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
se	se	k3xPyFc4	se
vrátila	vrátit	k5eAaPmAgFnS	vrátit
na	na	k7c4	na
filmová	filmový	k2eAgNnPc4d1	filmové
plátna	plátno	k1gNnPc4	plátno
<g/>
.	.	kIx.	.
</s>
<s>
Hrála	hrát	k5eAaImAgFnS	hrát
spolu	spolu	k6eAd1	spolu
se	se	k3xPyFc4	se
Susan	Susan	k1gMnSc1	Susan
Sarandon	Sarandon	k1gMnSc1	Sarandon
<g/>
,	,	kIx,	,
Kieranem	Kieran	k1gInSc7	Kieran
Culkinem	Culkin	k1gInSc7	Culkin
a	a	k8xC	a
Billem	Bill	k1gMnSc7	Bill
Pullmanem	Pullman	k1gMnSc7	Pullman
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Igby	Igba	k1gFnSc2	Igba
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
si	se	k3xPyFc3	se
zahrála	zahrát	k5eAaPmAgFnS	zahrát
dceru	dcera	k1gFnSc4	dcera
Meryl	Meryl	k1gInSc1	Meryl
Streep	Streep	k1gMnSc1	Streep
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Hodiny	hodina	k1gFnSc2	hodina
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgInS	být
nominován	nominovat	k5eAaBmNgMnS	nominovat
na	na	k7c4	na
Oscara	Oscar	k1gMnSc4	Oscar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
roce	rok	k1gInSc6	rok
byla	být	k5eAaImAgFnS	být
obsazena	obsadit	k5eAaPmNgFnS	obsadit
do	do	k7c2	do
filmů	film	k1gInPc2	film
Terminátor	terminátor	k1gInSc1	terminátor
3	[number]	k4	3
<g/>
:	:	kIx,	:
Vzpoura	vzpoura	k1gFnSc1	vzpoura
strojů	stroj	k1gInPc2	stroj
a	a	k8xC	a
Krása	krása	k1gFnSc1	krása
na	na	k7c6	na
scéně	scéna	k1gFnSc6	scéna
<g/>
.	.	kIx.	.
</s>
<s>
Získala	získat	k5eAaPmAgFnS	získat
pozornost	pozornost	k1gFnSc1	pozornost
kritiků	kritik	k1gMnPc2	kritik
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
když	když	k8xS	když
hrála	hrát	k5eAaImAgFnS	hrát
ve	v	k7c6	v
filmech	film	k1gInPc6	film
Jeden	jeden	k4xCgInSc4	jeden
navíc	navíc	k6eAd1	navíc
a	a	k8xC	a
Základ	základ	k1gInSc4	základ
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
ve	v	k7c4	v
fantasy	fantas	k1gInPc4	fantas
filmu	film	k1gInSc2	film
Hvězdný	hvězdný	k2eAgInSc4d1	hvězdný
prach	prach	k1gInSc4	prach
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
popsala	popsat	k5eAaPmAgFnS	popsat
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
klasický	klasický	k2eAgInSc4d1	klasický
model	model	k1gInSc4	model
romantické	romantický	k2eAgFnSc2d1	romantická
komedie	komedie	k1gFnSc2	komedie
<g/>
"	"	kIx"	"
a	a	k8xC	a
hrála	hrát	k5eAaImAgFnS	hrát
zde	zde	k6eAd1	zde
společně	společně	k6eAd1	společně
s	s	k7c7	s
Charliem	Charlie	k1gMnSc7	Charlie
Coxem	Cox	k1gMnSc7	Cox
<g/>
,	,	kIx,	,
Michelle	Michelle	k1gNnSc1	Michelle
Pfeiffer	Pfeiffra	k1gFnPc2	Pfeiffra
<g/>
,	,	kIx,	,
Robertem	Robert	k1gMnSc7	Robert
De	De	k?	De
Niro	Niro	k6eAd1	Niro
a	a	k8xC	a
Siennou	Sienný	k2eAgFnSc4d1	Sienný
Miller	Miller	k1gMnSc1	Miller
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
si	se	k3xPyFc3	se
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
zahrála	zahrát	k5eAaPmAgFnS	zahrát
v	v	k7c6	v
dramatu	drama	k1gNnSc6	drama
Ten	ten	k3xDgInSc1	ten
večer	večer	k1gInSc1	večer
a	a	k8xC	a
Osobní	osobní	k2eAgFnSc1d1	osobní
spravedlnost	spravedlnost	k1gFnSc1	spravedlnost
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
boku	bok	k1gInSc6	bok
Zaca	Zacus	k1gMnSc2	Zacus
Efrona	Efron	k1gMnSc2	Efron
a	a	k8xC	a
Bena	Bena	k?	Bena
Chaplina	Chaplina	k1gFnSc1	Chaplina
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
v	v	k7c6	v
dramatu	drama	k1gNnSc6	drama
Já	já	k3xPp1nSc1	já
a	a	k8xC	a
Orson	Orson	k1gMnSc1	Orson
Welles	Welles	k1gMnSc1	Welles
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Divadlo	divadlo	k1gNnSc1	divadlo
==	==	k?	==
</s>
</p>
<p>
<s>
Její	její	k3xOp3gNnSc1	její
první	první	k4xOgNnSc1	první
divadelní	divadelní	k2eAgNnSc1d1	divadelní
představení	představení	k1gNnSc1	představení
v	v	k7c6	v
New	New	k1gMnSc6	New
Yorku	York	k1gInSc2	York
byly	být	k5eAaImAgFnP	být
Happiness	Happiness	k1gInSc4	Happiness
<g/>
,	,	kIx,	,
Punk	punk	k1gInSc1	punk
Ballet	Ballet	k1gInSc1	Ballet
a	a	k8xC	a
Kids	Kids	k1gInSc1	Kids
Onstage	Onstag	k1gInSc2	Onstag
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2000	[number]	k4	2000
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgNnP	objevit
mimo	mimo	k7c4	mimo
Broadway	Broadwaa	k1gFnPc4	Broadwaa
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
Monology	monolog	k1gInPc4	monolog
vagíny	vagína	k1gFnSc2	vagína
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
stejného	stejný	k2eAgInSc2d1	stejný
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
jako	jako	k9	jako
Emily	Emil	k1gMnPc4	Emil
Webb	Webb	k1gInSc1	Webb
ve	v	k7c6	v
čtení	čtení	k1gNnSc6	čtení
hry	hra	k1gFnSc2	hra
Thorntona	Thornton	k1gMnSc2	Thornton
Wildera	Wilder	k1gMnSc2	Wilder
Our	Our	k1gMnSc1	Our
Town	Town	k1gMnSc1	Town
v	v	k7c6	v
All	All	k1gFnSc6	All
Saint	Sainto	k1gNnPc2	Sainto
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Episcopal	Episcopal	k1gFnSc7	Episcopal
Church	Church	k1gInSc1	Church
v	v	k7c4	v
Beverly	Beverla	k1gFnPc4	Beverla
Hills	Hillsa	k1gFnPc2	Hillsa
<g/>
.	.	kIx.	.
</s>
<s>
Výrobu	výroba	k1gFnSc4	výroba
představení	představení	k1gNnSc2	představení
provedla	provést	k5eAaPmAgFnS	provést
Bess	Bess	k1gInSc4	Bess
Armstrongová	Armstrongová	k1gFnSc1	Armstrongová
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
hrála	hrát	k5eAaImAgFnS	hrát
její	její	k3xOp3gFnSc4	její
matku	matka	k1gFnSc4	matka
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
Tak	tak	k9	tak
tohle	tenhle	k3xDgNnSc1	tenhle
je	být	k5eAaImIp3nS	být
můj	můj	k3xOp1gInSc4	můj
život	život	k1gInSc4	život
a	a	k8xC	a
objevili	objevit	k5eAaPmAgMnP	objevit
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
někteří	některý	k3yIgMnPc1	některý
její	její	k3xOp3gMnPc1	její
kolegové	kolega	k1gMnPc1	kolega
ze	z	k7c2	z
seriálu	seriál	k1gInSc2	seriál
včetně	včetně	k7c2	včetně
Toma	Tom	k1gMnSc2	Tom
Irwina	Irwin	k1gMnSc2	Irwin
<g/>
,	,	kIx,	,
Devona	Devon	k1gMnSc2	Devon
Gummersalla	Gummersall	k1gMnSc2	Gummersall
a	a	k8xC	a
Paula	Paul	k1gMnSc2	Paul
Dooleye	Dooley	k1gMnSc2	Dooley
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
svůj	svůj	k3xOyFgInSc4	svůj
broadwayský	broadwayský	k2eAgInSc4d1	broadwayský
debut	debut	k1gInSc4	debut
v	v	k7c6	v
roli	role	k1gFnSc6	role
Elizy	Eliza	k1gFnSc2	Eliza
Doolittleové	Doolittleové	k2eAgFnSc2d1	Doolittleové
v	v	k7c6	v
Pygmalionu	Pygmalion	k1gInSc6	Pygmalion
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2012	[number]	k4	2012
Harvard	Harvard	k1gInSc1	Harvard
University	universita	k1gFnSc2	universita
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Hasty	Hast	k1gMnPc7	Hast
Pudding	Pudding	k1gInSc4	Pudding
Theatricals	Theatricals	k1gInSc4	Theatricals
oznámili	oznámit	k5eAaPmAgMnP	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
získá	získat	k5eAaPmIp3nS	získat
titul	titul	k1gInSc4	titul
žena	žena	k1gFnSc1	žena
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Další	další	k2eAgFnPc1d1	další
práce	práce	k1gFnPc1	práce
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
napsala	napsat	k5eAaBmAgFnS	napsat
úvod	úvod	k1gInSc4	úvod
knihy	kniha	k1gFnSc2	kniha
Neila	Neil	k1gMnSc2	Neil
Gaimana	Gaiman	k1gMnSc2	Gaiman
<g/>
,	,	kIx,	,
Death	Death	k1gInSc1	Death
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Time	Tim	k1gFnSc2	Tim
of	of	k?	of
Your	Your	k1gInSc1	Your
Life	Life	k1gInSc1	Life
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
namluvila	namluvit	k5eAaBmAgFnS	namluvit
audioknihu	audioknih	k1gInSc3	audioknih
od	od	k7c2	od
Margaret	Margareta	k1gFnPc2	Margareta
Atwoodové	Atwoodový	k2eAgNnSc1d1	Atwoodové
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Handmaid	Handmaida	k1gFnPc2	Handmaida
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Tale	Tale	k1gFnSc7	Tale
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
vydána	vydat	k5eAaPmNgFnS	vydat
přes	přes	k7c4	přes
Audible	Audible	k1gFnSc4	Audible
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
výkon	výkon	k1gInSc4	výkon
byla	být	k5eAaImAgFnS	být
nominována	nominovat	k5eAaBmNgFnS	nominovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
na	na	k7c4	na
cenu	cena	k1gFnSc4	cena
Audie	Audie	k1gFnSc2	Audie
Award	Awarda	k1gFnPc2	Awarda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Osobní	osobní	k2eAgInSc4d1	osobní
život	život	k1gInSc4	život
==	==	k?	==
</s>
</p>
<p>
<s>
Chodila	chodit	k5eAaImAgFnS	chodit
s	s	k7c7	s
hercem	herec	k1gMnSc7	herec
Billym	Billym	k1gInSc4	Billym
Crudupem	Crudup	k1gInSc7	Crudup
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
budilo	budit	k5eAaImAgNnS	budit
především	především	k9	především
negativní	negativní	k2eAgFnSc4d1	negativní
publicitu	publicita	k1gFnSc4	publicita
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
šířily	šířit	k5eAaImAgFnP	šířit
zvěsti	zvěst	k1gFnPc1	zvěst
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gInSc1	jejich
vztah	vztah	k1gInSc1	vztah
zapříčinil	zapříčinit	k5eAaPmAgInS	zapříčinit
konec	konec	k1gInSc4	konec
vztahu	vztah	k1gInSc2	vztah
mezi	mezi	k7c7	mezi
Crudupem	Crudup	k1gInSc7	Crudup
a	a	k8xC	a
tehdy	tehdy	k6eAd1	tehdy
těhotnou	těhotná	k1gFnSc4	těhotná
Mary-Louise	Mary-Louise	k1gFnSc2	Mary-Louise
Parkerovou	Parkerová	k1gFnSc7	Parkerová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c2	za
herce	herec	k1gMnSc2	herec
Hugha	Hugh	k1gMnSc2	Hugh
Dancyho	Dancy	k1gMnSc2	Dancy
se	se	k3xPyFc4	se
provdala	provdat	k5eAaPmAgNnP	provdat
v	v	k7c6	v
září	září	k1gNnSc6	září
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
4	[number]	k4	4
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2012	[number]	k4	2012
oznámili	oznámit	k5eAaPmAgMnP	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
spolu	spolu	k6eAd1	spolu
čekají	čekat	k5eAaImIp3nP	čekat
potomka	potomek	k1gMnSc4	potomek
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
syn	syn	k1gMnSc1	syn
Cyrus	Cyrus	k1gMnSc1	Cyrus
Michael	Michael	k1gMnSc1	Michael
Christopher	Christophra	k1gFnPc2	Christophra
Dancy	Danca	k1gFnSc2	Danca
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgInS	narodit
17	[number]	k4	17
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2012	[number]	k4	2012
a	a	k8xC	a
druhý	druhý	k4xOgInSc1	druhý
pak	pak	k9	pak
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
obdržela	obdržet	k5eAaPmAgFnS	obdržet
hvězdu	hvězda	k1gFnSc4	hvězda
na	na	k7c6	na
Hollywoodském	hollywoodský	k2eAgInSc6d1	hollywoodský
chodníku	chodník	k1gInSc6	chodník
slávy	sláva	k1gFnSc2	sláva
<g/>
.	.	kIx.	.
</s>
<s>
Přihlásila	přihlásit	k5eAaPmAgFnS	přihlásit
se	se	k3xPyFc4	se
k	k	k7c3	k
feminismu	feminismus	k1gInSc3	feminismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Filmografie	filmografie	k1gFnSc2	filmografie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Film	film	k1gInSc1	film
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Televize	televize	k1gFnSc1	televize
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Divadlo	divadlo	k1gNnSc1	divadlo
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Ocenění	ocenění	k1gNnSc1	ocenění
a	a	k8xC	a
nominace	nominace	k1gFnSc1	nominace
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Claire	Clair	k1gInSc5	Clair
Danes	Danesa	k1gFnPc2	Danesa
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Claire	Clair	k1gInSc5	Clair
Danesová	Danesový	k2eAgFnSc1d1	Danesová
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Claire	Clair	k1gInSc5	Clair
Danes	Danesa	k1gFnPc2	Danesa
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Claire	Clair	k1gMnSc5	Clair
Danesová	Danesový	k2eAgNnPc4d1	Danesový
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
<p>
<s>
Claire	Clair	k1gMnSc5	Clair
Danesová	Danesový	k2eAgFnSc1d1	Danesová
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Claire	Clair	k1gMnSc5	Clair
Danesová	Danesový	k2eAgNnPc1d1	Danesový
na	na	k7c4	na
People	People	k1gFnSc4	People
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Claire	Clair	k1gMnSc5	Clair
Danesová	Danesový	k2eAgFnSc1d1	Danesová
na	na	k7c4	na
Emmys	Emmys	k1gInSc4	Emmys
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
