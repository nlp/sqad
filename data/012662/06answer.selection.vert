<s>
Kód	kód	k1gInSc1	kód
amerického	americký	k2eAgInSc2d1	americký
dolaru	dolar	k1gInSc2	dolar
podle	podle	k7c2	podle
ISO	ISO	kA	ISO
4217	[number]	k4	4217
je	být	k5eAaImIp3nS	být
USD	USD	kA	USD
<g/>
;	;	kIx,	;
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
měnový	měnový	k2eAgInSc1d1	měnový
fond	fond	k1gInSc1	fond
také	také	k9	také
používá	používat	k5eAaImIp3nS	používat
označení	označení	k1gNnSc4	označení
US	US	kA	US
<g/>
$	$	kIx~	$
<g/>
.	.	kIx.	.
</s>
