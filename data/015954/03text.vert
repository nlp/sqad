<s>
Survival	Survivat	k5eAaImAgInS,k5eAaBmAgInS,k5eAaPmAgInS
horor	horor	k1gInSc1
</s>
<s>
Survival	survival	k2
horor	horor	k1gInSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
survival	survival	k2
horror	horror	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
žánr	žánr	k1gInSc4
videoher	videohra	k1gFnPc2
<g/>
/	/	kIx~
<g/>
počítačových	počítačový	k2eAgFnPc2d1
her	hra	k1gFnPc2
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
hráč	hráč	k1gMnSc1
musí	muset	k5eAaImIp3nS
čelit	čelit	k5eAaImF
útokům	útok	k1gInPc3
nadpřirozených	nadpřirozený	k2eAgMnPc2d1
nepřátel	nepřítel	k1gMnPc2
(	(	kIx(
<g/>
často	často	k6eAd1
nemrtvých	nemrtvý	k1gMnPc2
<g/>
,	,	kIx,
zombie	zombie	k1gFnPc1
apod.	apod.	kA
<g/>
)	)	kIx)
a	a	k8xC
přežít	přežít	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejčastěji	často	k6eAd3
se	se	k3xPyFc4
příběh	příběh	k1gInSc1
odehrává	odehrávat	k5eAaImIp3nS
v	v	k7c6
klaustrofobickém	klaustrofobický	k2eAgNnSc6d1
industrializovaném	industrializovaný	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
<g/>
,	,	kIx,
kterému	který	k3yRgNnSc3,k3yQgNnSc3,k3yIgNnSc3
dominuje	dominovat	k5eAaImIp3nS
znepokojivá	znepokojivý	k2eAgFnSc1d1
bizarní	bizarní	k2eAgFnSc1d1
stylizace	stylizace	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Hry	hra	k1gFnPc1
obvykle	obvykle	k6eAd1
bývají	bývat	k5eAaImIp3nP
ztvárněny	ztvárnit	k5eAaPmNgFnP
v	v	k7c6
žánru	žánr	k1gInSc6
3D	3D	k4
akčních	akční	k2eAgFnPc2d1
her	hra	k1gFnPc2
(	(	kIx(
<g/>
tzv.	tzv.	kA
third-person	third-person	k1gMnSc1
shooter	shooter	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ve	v	k7c6
kterých	který	k3yIgInPc6,k3yQgInPc6,k3yRgInPc6
se	se	k3xPyFc4
kromě	kromě	k7c2
bojů	boj	k1gInPc2
objevují	objevovat	k5eAaImIp3nP
také	také	k9
adventurní	adventurní	k2eAgFnPc1d1
sekvence	sekvence	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hudební	hudební	k2eAgFnSc7d1
složkou	složka	k1gFnSc7
bývá	bývat	k5eAaImIp3nS
lomozivá	lomozivý	k2eAgFnSc1d1
ambientní	ambientní	k2eAgFnSc1d1
hudba	hudba	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
spolu	spolu	k6eAd1
s	s	k7c7
děsivými	děsivý	k2eAgInPc7d1
zvuky	zvuk	k1gInPc7
a	a	k8xC
temnou	temný	k2eAgFnSc7d1
grafickou	grafický	k2eAgFnSc7d1
stránkou	stránka	k1gFnSc7
celé	celý	k2eAgFnSc2d1
hry	hra	k1gFnSc2
nabízí	nabízet	k5eAaImIp3nS
psychologicky	psychologicky	k6eAd1
stresující	stresující	k2eAgInSc1d1
zážitek	zážitek	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
bývá	bývat	k5eAaImIp3nS
ještě	ještě	k6eAd1
umocněn	umocněn	k2eAgMnSc1d1
(	(	kIx(
<g/>
např.	např.	kA
ve	v	k7c6
hrách	hra	k1gFnPc6
Silent	Silent	k1gMnSc1
Hill	Hill	k1gMnSc1
<g/>
)	)	kIx)
fyzicky	fyzicky	k6eAd1
slabší	slabý	k2eAgFnSc4d2
<g/>
,	,	kIx,
neobratnou	obratný	k2eNgFnSc4d1
<g/>
,	,	kIx,
militantně	militantně	k6eAd1
nezaloženou	založený	k2eNgFnSc7d1
hlavní	hlavní	k2eAgFnSc7d1
postavou	postava	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
neumí	umět	k5eNaImIp3nS
příliš	příliš	k6eAd1
dobře	dobře	k6eAd1
zacházet	zacházet	k5eAaImF
se	s	k7c7
zbraněmi	zbraň	k1gFnPc7
(	(	kIx(
<g/>
často	často	k6eAd1
netrefí	trefit	k5eNaPmIp3nS
cíl	cíl	k1gInSc4
<g/>
,	,	kIx,
nemotorně	motorně	k6eNd1,k6eAd1
manipuluje	manipulovat	k5eAaImIp3nS
se	s	k7c7
zbraněmi	zbraň	k1gFnPc7
na	na	k7c4
blízko	blízko	k1gNnSc4
<g/>
)	)	kIx)
a	a	k8xC
neholduje	holdovat	k5eNaImIp3nS
násilí	násilí	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvůli	kvůli	k7c3
těmto	tento	k3xDgFnPc3
indispozicím	indispozice	k1gFnPc3
pomalu	pomalu	k6eAd1
nabíjí	nabíjet	k5eAaImIp3nS
střelné	střelný	k2eAgFnPc4d1
zbraně	zbraň	k1gFnPc4
a	a	k8xC
zbraně	zbraň	k1gFnPc4
na	na	k7c4
blízko	blízko	k1gNnSc4
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
jsou	být	k5eAaImIp3nP
např.	např.	kA
palice	palice	k1gFnPc1
<g/>
,	,	kIx,
halapartna	halapartna	k1gFnSc1
<g/>
,	,	kIx,
větší	veliký	k2eAgFnSc1d2
sekera	sekera	k1gFnSc1
atp.	atp.	kA
<g/>
,	,	kIx,
táhne	táhnout	k5eAaImIp3nS
po	po	k7c6
zemi	zem	k1gFnSc6
za	za	k7c7
sebou	se	k3xPyFc7
<g/>
.	.	kIx.
</s>
<s>
Dalšími	další	k2eAgInPc7d1
stresujícími	stresující	k2eAgInPc7d1
faktory	faktor	k1gInPc7
snažící	snažící	k2eAgFnSc2d1
se	se	k3xPyFc4
u	u	k7c2
hráče	hráč	k1gMnSc2
vyvolat	vyvolat	k5eAaPmF
např.	např.	kA
pocity	pocit	k1gInPc1
opuštěnosti	opuštěnost	k1gFnSc2
<g/>
,	,	kIx,
nejistoty	nejistota	k1gFnSc2
a	a	k8xC
beznaděje	beznaděj	k1gFnSc2
jsou	být	k5eAaImIp3nP
minimum	minimum	k1gNnSc4
munice	munice	k1gFnSc2
<g/>
,	,	kIx,
možnost	možnost	k1gFnSc4
ukládat	ukládat	k5eAaImF
hru	hra	k1gFnSc4
jen	jen	k9
na	na	k7c6
určitých	určitý	k2eAgNnPc6d1
místech	místo	k1gNnPc6
a	a	k8xC
mnohdy	mnohdy	k6eAd1
i	i	k9
technické	technický	k2eAgInPc4d1
nedostatky	nedostatek	k1gInPc4
jako	jako	k8xC,k8xS
kamera	kamera	k1gFnSc1
hůře	zle	k6eAd2
snímající	snímající	k2eAgInSc4d1
hráčův	hráčův	k2eAgInSc4d1
postup	postup	k1gInSc4
prostředím	prostředí	k1gNnSc7
atd.	atd.	kA
</s>
<s>
Za	za	k7c4
průkopnický	průkopnický	k2eAgInSc4d1
titul	titul	k1gInSc4
survival	survivat	k5eAaBmAgMnS,k5eAaPmAgMnS,k5eAaImAgMnS
hororu	horor	k1gInSc3
je	být	k5eAaImIp3nS
považována	považován	k2eAgFnSc1d1
hra	hra	k1gFnSc1
Alone	Alon	k1gInSc5
In	In	k1gMnPc3
The	The	k1gFnPc2
Dark	Dark	k1gInSc4
z	z	k7c2
roku	rok	k1gInSc2
1992	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Označení	označení	k1gNnSc1
survival	survivat	k5eAaBmAgInS,k5eAaPmAgInS,k5eAaImAgInS
horor	horor	k1gInSc4
přišlo	přijít	k5eAaPmAgNnS
však	však	k9
až	až	k6eAd1
se	s	k7c7
hrou	hra	k1gFnSc7
Resident	resident	k1gMnSc1
Evil	Evil	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Kultovními	kultovní	k2eAgMnPc7d1
zástupci	zástupce	k1gMnPc7
survival	survivat	k5eAaPmAgMnS,k5eAaBmAgMnS,k5eAaImAgMnS
hororu	horor	k1gInSc2
jsou	být	k5eAaImIp3nP
herní	herní	k2eAgFnPc4d1
série	série	k1gFnPc4
Alone	Alon	k1gInSc5
In	In	k1gFnSc2
The	The	k1gMnSc1
Dark	Dark	k1gMnSc1
<g/>
,	,	kIx,
Resident	resident	k1gMnSc1
Evil	Evil	k1gMnSc1
<g/>
,	,	kIx,
Silent	Silent	k1gMnSc1
Hill	Hill	k1gMnSc1
<g/>
,	,	kIx,
Project	Project	k1gMnSc1
Zero	Zero	k1gMnSc1
<g/>
,	,	kIx,
Forbidden	Forbiddno	k1gNnPc2
Siren	Siren	k2eAgMnSc1d1
a	a	k8xC
další	další	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Mezi	mezi	k7c4
české	český	k2eAgInPc4d1
tituly	titul	k1gInPc4
tohoto	tento	k3xDgInSc2
žánru	žánr	k1gInSc2
lze	lze	k6eAd1
zařadit	zařadit	k5eAaPmF
Bloodline	Bloodlin	k1gInSc5
<g/>
,	,	kIx,
Silent	Silent	k1gMnSc1
Hill	Hill	k1gMnSc1
<g/>
:	:	kIx,
Downpour	Downpour	k1gMnSc1
nebo	nebo	k8xC
DayZ	DayZ	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Survival	Survivat	k5eAaPmAgMnS,k5eAaBmAgMnS,k5eAaImAgMnS
horory	horor	k1gInPc1
</s>
<s>
7	#num#	k4
Days	Days	k1gInSc1
to	ten	k3xDgNnSc1
Die	Die	k1gMnPc6
</s>
<s>
Alan	Alan	k1gMnSc1
Wake	Wak	k1gFnSc2
</s>
<s>
Alan	Alan	k1gMnSc1
Wake	Wak	k1gFnSc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
American	American	k1gInSc1
Nightmare	Nightmar	k1gMnSc5
</s>
<s>
Alone	Alonout	k5eAaImIp3nS,k5eAaPmIp3nS
in	in	k?
the	the	k?
Dark	Dark	k1gInSc1
</s>
<s>
Alone	Alonout	k5eAaImIp3nS,k5eAaPmIp3nS
in	in	k?
the	the	k?
Dark	Dark	k1gInSc1
2	#num#	k4
</s>
<s>
Alone	Alonout	k5eAaPmIp3nS,k5eAaImIp3nS
in	in	k?
the	the	k?
Dark	Dark	k1gInSc1
3	#num#	k4
</s>
<s>
Alone	Alonout	k5eAaImIp3nS,k5eAaPmIp3nS
in	in	k?
the	the	k?
Dark	Dark	k1gInSc4
<g/>
:	:	kIx,
The	The	k1gMnSc5
New	New	k1gMnSc5
Nightmare	Nightmar	k1gMnSc5
</s>
<s>
Amnézie	amnézie	k1gFnSc1
<g/>
:	:	kIx,
Pád	Pád	k1gInSc1
do	do	k7c2
temnoty	temnota	k1gFnSc2
</s>
<s>
Amnesia	Amnesia	k1gFnSc1
<g/>
:	:	kIx,
Machine	Machin	k1gInSc5
For	forum	k1gNnPc2
Pigs	Pigs	k1gInSc1
</s>
<s>
Dead	Dead	k1gMnSc1
space	space	k1gMnSc1
</s>
<s>
Dungeon	Dungeon	k1gMnSc1
Nightmares	Nightmares	k1gMnSc1
</s>
<s>
Outlast	Outlast	k1gInSc1
</s>
<s>
Outlast	Outlast	k1gInSc1
2	#num#	k4
</s>
<s>
Resident	resident	k1gMnSc1
Evil	Evil	k1gMnSc1
</s>
<s>
Resident	resident	k1gMnSc1
Evil	Evil	k1gMnSc1
2	#num#	k4
</s>
<s>
Resident	resident	k1gMnSc1
Evil	Evil	k1gMnSc1
3	#num#	k4
<g/>
:	:	kIx,
Nemesis	Nemesis	k1gFnSc1
</s>
<s>
Resident	resident	k1gMnSc1
Evil	Evil	k1gMnSc1
4	#num#	k4
</s>
<s>
Resident	resident	k1gMnSc1
Evil	Evil	k1gMnSc1
7	#num#	k4
</s>
<s>
Silent	Silent	k1gMnSc1
Hill	Hill	k1gMnSc1
</s>
<s>
Silent	Silent	k1gMnSc1
Hill	Hill	k1gMnSc1
2	#num#	k4
</s>
<s>
Silent	Silent	k1gMnSc1
Hill	Hill	k1gMnSc1
3	#num#	k4
</s>
<s>
Silent	Silent	k1gMnSc1
Hill	Hill	k1gMnSc1
4	#num#	k4
</s>
<s>
Penumbra	Penumbra	k1gFnSc1
(	(	kIx(
<g/>
herní	herní	k2eAgFnSc1d1
série	série	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Slender	Slender	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Eight	Eight	k1gMnSc1
Pages	Pages	k1gMnSc1
</s>
<s>
Slender	Slender	k1gInSc1
Mansion	Mansion	k1gInSc1
</s>
<s>
Slender	Slender	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Woods	Woods	k1gInSc1
</s>
<s>
Slender	Slender	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Arrival	Arrival	k1gMnSc1
</s>
<s>
Year	Year	k1gMnSc1
Walk	Walk	k1gMnSc1
</s>
<s>
Visage	Visage	k1gFnSc1
(	(	kIx(
<g/>
videohra	videohra	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gNnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc4
<g/>
}	}	kIx)
Žánry	žánr	k1gInPc4
videoher	videohra	k1gFnPc2
Akční	akční	k2eAgFnSc1d1
</s>
<s>
beat	beat	k1gInSc1
'	'	kIx"
<g/>
em	em	k?
up	up	k?
</s>
<s>
hack	hack	k1gInSc1
'	'	kIx"
<g/>
n	n	k0
<g/>
'	'	kIx"
slash	slash	k1gInSc1
</s>
<s>
bojová	bojový	k2eAgNnPc1d1
</s>
<s>
plošinovka	plošinovka	k1gFnSc1
</s>
<s>
střílečka	střílečka	k1gFnSc1
</s>
<s>
first-person	first-person	k1gMnSc1
</s>
<s>
third-person	third-person	k1gMnSc1
</s>
<s>
light	light	k1gInSc1
gun	gun	k?
</s>
<s>
shoot	shoot	k1gInSc1
'	'	kIx"
<g/>
em	em	k?
up	up	k?
</s>
<s>
taktická	taktický	k2eAgFnSc1d1
</s>
<s>
hra	hra	k1gFnSc1
o	o	k7c4
přežití	přežití	k1gNnSc4
</s>
<s>
battle	battlat	k5eAaPmIp3nS
royale	royale	k6eAd1
Akční	akční	k2eAgFnSc1d1
adventura	adventura	k1gFnSc1
</s>
<s>
klon	klon	k1gInSc1
Grand	grand	k1gMnSc1
Theft	Theft	k1gMnSc1
Auto	auto	k1gNnSc1
</s>
<s>
immersive	immersiev	k1gFnPc1
sim	sima	k1gFnPc2
</s>
<s>
metroidvania	metroidvanium	k1gNnPc1
</s>
<s>
stealth	stealth	k1gMnSc1
</s>
<s>
survival	survivat	k5eAaPmAgInS,k5eAaBmAgInS,k5eAaImAgInS
horor	horor	k1gInSc1
Adventura	adventura	k1gFnSc1
</s>
<s>
escape	escapat	k5eAaPmIp3nS
the	the	k?
room	room	k6eAd1
</s>
<s>
interaktivní	interaktivní	k2eAgFnSc1d1
videohra	videohra	k1gFnSc1
</s>
<s>
vizuální	vizuální	k2eAgInSc4d1
román	román	k1gInSc4
MMO	MMO	kA
</s>
<s>
MMOFPS	MMOFPS	kA
</s>
<s>
MMORPG	MMORPG	kA
</s>
<s>
MMORTS	MMORTS	kA
</s>
<s>
MUD	MUD	kA
Hra	hra	k1gFnSc1
na	na	k7c4
hrdiny	hrdina	k1gMnPc4
</s>
<s>
akční	akční	k2eAgFnSc1d1
RPG	RPG	kA
</s>
<s>
dungeon	dungeon	k1gMnSc1
crawl	crawl	k1gMnSc1
</s>
<s>
roguelike	roguelike	k6eAd1
</s>
<s>
taktické	taktický	k2eAgInPc1d1
RPG	RPG	kA
Simulátor	simulátor	k1gInSc4
</s>
<s>
stavební	stavební	k2eAgFnPc1d1
</s>
<s>
obchodu	obchod	k1gInSc2
</s>
<s>
budovatelská	budovatelský	k2eAgFnSc1d1
strategie	strategie	k1gFnSc1
</s>
<s>
politický	politický	k2eAgInSc1d1
</s>
<s>
života	život	k1gInSc2
</s>
<s>
rande	rande	k1gNnSc1
</s>
<s>
sportovní	sportovní	k2eAgFnSc1d1
Strategie	strategie	k1gFnSc1
</s>
<s>
4X	4X	k4
</s>
<s>
auto	auto	k1gNnSc1
battler	battler	k1gMnSc1
</s>
<s>
MOBA	MOBA	kA
</s>
<s>
realtimová	realtimový	k2eAgFnSc1d1
strategie	strategie	k1gFnSc1
</s>
<s>
realtimová	realtimový	k2eAgFnSc1d1
taktika	taktika	k1gFnSc1
</s>
<s>
tower	tower	k1gInSc1
defense	defense	k1gFnSc2
</s>
<s>
tahová	tahový	k2eAgFnSc1d1
strategie	strategie	k1gFnSc1
</s>
<s>
tahová	tahový	k2eAgFnSc1d1
taktika	taktika	k1gFnSc1
</s>
<s>
artillery	artiller	k1gInPc1
</s>
<s>
wargame	wargam	k1gInSc5
</s>
<s>
grand	grand	k1gMnSc1
strategy	stratega	k1gFnSc2
wargame	wargam	k1gInSc5
Dopravní	dopravní	k2eAgInPc1d1
simulátor	simulátor	k1gInSc1
</s>
<s>
letecký	letecký	k2eAgInSc1d1
</s>
<s>
vojenský	vojenský	k2eAgInSc1d1
</s>
<s>
vesmírný	vesmírný	k2eAgInSc1d1
</s>
<s>
závodní	závodní	k2eAgFnSc1d1
videohra	videohra	k1gFnSc1
</s>
<s>
kart	kart	k2eAgInSc1d1
racing	racing	k1gInSc1
</s>
<s>
závodní	závodní	k2eAgInSc1d1
simulátor	simulátor	k1gInSc1
</s>
<s>
ponorky	ponorka	k1gFnPc1
</s>
<s>
vlakový	vlakový	k2eAgInSc1d1
Další	další	k2eAgInSc1d1
žánry	žánr	k1gInPc7
</s>
<s>
klon	klon	k1gInSc1
Breakout	Breakout	k1gMnSc1
</s>
<s>
deck-building	deck-building	k1gInSc1
</s>
<s>
roguelike	roguelike	k6eAd1
deck-building	deck-building	k1gInSc1
</s>
<s>
digitální	digitální	k2eAgFnSc1d1
sběratelská	sběratelský	k2eAgFnSc1d1
karetní	karetní	k2eAgFnSc1d1
videohra	videohra	k1gFnSc1
</s>
<s>
eroge	eroge	k6eAd1
</s>
<s>
exergaming	exergaming	k1gInSc1
</s>
<s>
hororová	hororový	k2eAgFnSc1d1
</s>
<s>
klikací	klikací	k2eAgFnSc1d1
</s>
<s>
hudební	hudební	k2eAgMnSc1d1
</s>
<s>
rytmická	rytmický	k2eAgFnSc1d1
</s>
<s>
non-game	non-gam	k1gInSc5
</s>
<s>
párty	párty	k1gFnSc1
</s>
<s>
programovací	programovací	k2eAgInSc1d1
</s>
<s>
logická	logický	k2eAgFnSc1d1
</s>
<s>
sokoban	sokoban	k1gInSc4
Související	související	k2eAgNnPc4d1
témata	téma	k1gNnPc4
</s>
<s>
AAA	AAA	kA
videohra	videohra	k1gFnSc1
</s>
<s>
reklama	reklama	k1gFnSc1
ve	v	k7c6
videohrách	videohra	k1gFnPc6
</s>
<s>
advergaming	advergaming	k1gInSc1
</s>
<s>
in-game	in-gam	k1gInSc5
reklama	reklama	k1gFnSc1
</s>
<s>
arkáda	arkáda	k1gFnSc1
</s>
<s>
audiohra	audiohra	k1gFnSc1
</s>
<s>
casual	casual	k1gInSc1
videohra	videohra	k1gFnSc1
</s>
<s>
křesťanská	křesťanský	k2eAgFnSc1d1
videohra	videohra	k1gFnSc1
</s>
<s>
crossover	crossover	k1gInSc1
videohra	videohra	k1gFnSc1
</s>
<s>
kultovní	kultovní	k2eAgFnSc1d1
videohra	videohra	k1gFnSc1
</s>
<s>
vzdělávací	vzdělávací	k2eAgFnSc1d1
videohra	videohra	k1gFnSc1
</s>
<s>
FMV	FMV	kA
</s>
<s>
gamifikace	gamifikace	k1gFnSc1
</s>
<s>
nezávislá	závislý	k2eNgFnSc1d1
videohra	videohra	k1gFnSc1
</s>
<s>
multiplayer	multiplayer	k1gMnSc1
</s>
<s>
singleplayer	singleplayer	k1gMnSc1
</s>
<s>
kooperace	kooperace	k1gFnSc1
</s>
<s>
nelineární	lineární	k2eNgFnSc1d1
hratelnost	hratelnost	k1gFnSc1
</s>
<s>
open	open	k1gMnSc1
world	world	k1gMnSc1
</s>
<s>
sandboxová	sandboxový	k2eAgFnSc1d1
hra	hra	k1gFnSc1
</s>
<s>
nenásilná	násilný	k2eNgFnSc1d1
videohra	videohra	k1gFnSc1
</s>
<s>
online	onlinout	k5eAaPmIp3nS
hra	hra	k1gFnSc1
</s>
<s>
webová	webový	k2eAgFnSc1d1
hra	hra	k1gFnSc1
</s>
<s>
sázení	sázení	k1gNnSc1
po	po	k7c6
internetu	internet	k1gInSc6
</s>
<s>
hra	hra	k1gFnSc1
na	na	k7c6
sociální	sociální	k2eAgFnSc6d1
síti	síť	k1gFnSc6
</s>
<s>
vážná	vážný	k2eAgFnSc1d1
videohra	videohra	k1gFnSc1
</s>
<s>
toys-to-life	toys-to-lif	k1gMnSc5
</s>
<s>
videoherní	videoherní	k2eAgInSc1d1
klon	klon	k1gInSc1
kategorie	kategorie	k1gFnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Počítačové	počítačový	k2eAgFnPc1d1
hry	hra	k1gFnPc1
</s>
