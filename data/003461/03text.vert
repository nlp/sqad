<s>
Sophiina	Sophiin	k2eAgFnSc1d1	Sophiina
volba	volba	k1gFnSc1	volba
(	(	kIx(	(
<g/>
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
psychologický	psychologický	k2eAgInSc4d1	psychologický
román	román	k1gInSc4	román
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nejznámější	známý	k2eAgNnSc1d3	nejznámější
dílo	dílo	k1gNnSc1	dílo
Williama	William	k1gMnSc2	William
Styrona	Styron	k1gMnSc2	Styron
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
román	román	k1gInSc4	román
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zpracovává	zpracovávat	k5eAaImIp3nS	zpracovávat
složitým	složitý	k2eAgInSc7d1	složitý
a	a	k8xC	a
neobvyklým	obvyklý	k2eNgInSc7d1	neobvyklý
způsobem	způsob	k1gInSc7	způsob
téma	téma	k1gNnSc1	téma
vlivu	vliv	k1gInSc2	vliv
nacismu	nacismus	k1gInSc2	nacismus
na	na	k7c4	na
průměrně	průměrně	k6eAd1	průměrně
silného	silný	k2eAgMnSc4d1	silný
jedince	jedinec	k1gMnSc4	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc4d1	základní
dějovou	dějový	k2eAgFnSc4d1	dějová
osu	osa	k1gFnSc4	osa
příběhu	příběh	k1gInSc2	příběh
tvoří	tvořit	k5eAaImIp3nS	tvořit
výpověď	výpověď	k1gFnSc4	výpověď
vyprávěče	vyprávěč	k1gMnSc2	vyprávěč
Stinga	Sting	k1gMnSc2	Sting
(	(	kIx(	(
<g/>
W.	W.	kA	W.
Styron	Styron	k1gMnSc1	Styron
<g/>
)	)	kIx)	)
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
učil	učít	k5eAaPmAgMnS	učít
psát	psát	k5eAaImF	psát
<g/>
.	.	kIx.	.
</s>
<s>
Pásmo	pásmo	k1gNnSc1	pásmo
autorského	autorský	k2eAgNnSc2d1	autorské
vyprávění	vyprávění	k1gNnSc2	vyprávění
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
je	být	k5eAaImIp3nS	být
však	však	k9	však
brzy	brzy	k6eAd1	brzy
přerušeno	přerušit	k5eAaPmNgNnS	přerušit
líčením	líčení	k1gNnSc7	líčení
života	život	k1gInSc2	život
mladého	mladý	k2eAgInSc2d1	mladý
mileneckého	milenecký	k2eAgInSc2d1	milenecký
páru	pár	k1gInSc2	pár
<g/>
:	:	kIx,	:
Sophie	Sophie	k1gFnSc1	Sophie
Zawistowské	Zawistowská	k1gFnSc2	Zawistowská
a	a	k8xC	a
Nathana	Nathan	k1gMnSc2	Nathan
Landaua	Landauus	k1gMnSc2	Landauus
<g/>
,	,	kIx,	,
s	s	k7c7	s
kterými	který	k3yRgInPc7	který
se	se	k3xPyFc4	se
Stingo	Stingo	k1gMnSc1	Stingo
seznámil	seznámit	k5eAaPmAgMnS	seznámit
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
1947	[number]	k4	1947
v	v	k7c6	v
brooklynském	brooklynský	k2eAgInSc6d1	brooklynský
penziónu	penzión	k1gInSc6	penzión
Růžový	růžový	k2eAgInSc4d1	růžový
palác	palác	k1gInSc4	palác
<g/>
.	.	kIx.	.
</s>
<s>
Jinou	jiný	k2eAgFnSc4d1	jiná
dějovou	dějový	k2eAgFnSc4d1	dějová
linii	linie	k1gFnSc4	linie
tvoří	tvořit	k5eAaImIp3nS	tvořit
příběh	příběh	k1gInSc1	příběh
Stingova	Stingův	k2eAgNnSc2d1	Stingovo
pronikání	pronikání	k1gNnSc2	pronikání
do	do	k7c2	do
záhad	záhada	k1gFnPc2	záhada
tragického	tragický	k2eAgInSc2d1	tragický
až	až	k8xS	až
patologického	patologický	k2eAgInSc2d1	patologický
vztahu	vztah	k1gInSc2	vztah
milenců	milenec	k1gMnPc2	milenec
(	(	kIx(	(
<g/>
zjišťuje	zjišťovat	k5eAaImIp3nS	zjišťovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Nathan	Nathan	k1gMnSc1	Nathan
je	být	k5eAaImIp3nS	být
narkoman	narkoman	k1gMnSc1	narkoman
a	a	k8xC	a
schizofrenik	schizofrenik	k1gMnSc1	schizofrenik
<g/>
)	)	kIx)	)
a	a	k8xC	a
dál	daleko	k6eAd2	daleko
do	do	k7c2	do
minulosti	minulost	k1gFnSc2	minulost
a	a	k8xC	a
nejhlubších	hluboký	k2eAgNnPc2d3	nejhlubší
zákoutí	zákoutí	k1gNnPc2	zákoutí
psychiky	psychika	k1gFnSc2	psychika
ženy	žena	k1gFnSc2	žena
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ho	on	k3xPp3gMnSc4	on
přitahuje	přitahovat	k5eAaImIp3nS	přitahovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalším	další	k2eAgInSc6d1	další
dějovém	dějový	k2eAgInSc6d1	dějový
plánu	plán	k1gInSc6	plán
je	být	k5eAaImIp3nS	být
pásmo	pásmo	k1gNnSc1	pásmo
hrdinčiných	hrdinčin	k2eAgInPc2d1	hrdinčin
vzrušených	vzrušený	k2eAgInPc2d1	vzrušený
monologů	monolog	k1gInPc2	monolog
<g/>
,	,	kIx,	,
líčících	líčící	k2eAgMnPc2d1	líčící
její	její	k3xOp3gNnSc1	její
dětství	dětství	k1gNnSc4	dětství
a	a	k8xC	a
mládí	mládí	k1gNnSc4	mládí
v	v	k7c6	v
Krakově	Krakov	k1gInSc6	Krakov
<g/>
,	,	kIx,	,
její	její	k3xOp3gInPc4	její
osudy	osud	k1gInPc4	osud
roku	rok	k1gInSc2	rok
1943	[number]	k4	1943
ve	v	k7c6	v
Varšavě	Varšava	k1gFnSc6	Varšava
a	a	k8xC	a
v	v	k7c6	v
koncentračním	koncentrační	k2eAgInSc6d1	koncentrační
táboře	tábor	k1gInSc6	tábor
Osvětim	Osvětim	k1gFnSc4	Osvětim
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
zaujímají	zaujímat	k5eAaImIp3nP	zaujímat
klíčové	klíčový	k2eAgNnSc4d1	klíčové
místo	místo	k1gNnSc4	místo
Sophiininy	Sophiinin	k2eAgFnSc2d1	Sophiinin
volby	volba	k1gFnSc2	volba
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
románu	román	k1gInSc2	román
sice	sice	k8xC	sice
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
jednu	jeden	k4xCgFnSc4	jeden
klíčovou	klíčový	k2eAgFnSc4d1	klíčová
volbu	volba	k1gFnSc4	volba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nad	nad	k7c7	nad
závěrečnou	závěrečný	k2eAgFnSc7d1	závěrečná
tragickou	tragický	k2eAgFnSc7d1	tragická
Sophiinou	Sophiin	k2eAgFnSc7d1	Sophiina
volbou	volba	k1gFnSc7	volba
můžeme	moct	k5eAaImIp1nP	moct
jasně	jasně	k6eAd1	jasně
cítit	cítit	k5eAaImF	cítit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
pouhým	pouhý	k2eAgInSc7d1	pouhý
důsledkem	důsledek	k1gInSc7	důsledek
celého	celý	k2eAgInSc2d1	celý
řetězu	řetěz	k1gInSc2	řetěz
dalších	další	k2eAgFnPc2d1	další
osudových	osudový	k2eAgFnPc2d1	osudová
voleb	volba	k1gFnPc2	volba
<g/>
,	,	kIx,	,
před	před	k7c4	před
jaké	jaký	k3yIgFnPc4	jaký
stavěl	stavět	k5eAaImAgMnS	stavět
člověka	člověk	k1gMnSc4	člověk
nacismus	nacismus	k1gInSc1	nacismus
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
volbou	volba	k1gFnSc7	volba
je	být	k5eAaImIp3nS	být
Sophiin	Sophiin	k2eAgInSc1d1	Sophiin
pomýlený	pomýlený	k2eAgInSc1d1	pomýlený
závěr	závěr	k1gInSc1	závěr
neangažovat	angažovat	k5eNaBmF	angažovat
se	se	k3xPyFc4	se
v	v	k7c6	v
okupované	okupovaný	k2eAgFnSc6d1	okupovaná
Varšavě	Varšava	k1gFnSc6	Varšava
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
Sophii	Sophie	k1gFnSc4	Sophie
její	její	k3xOp3gFnSc1	její
varšavská	varšavský	k2eAgFnSc1d1	Varšavská
přítelkyně	přítelkyně	k1gFnSc1	přítelkyně
přesvědčuje	přesvědčovat	k5eAaImIp3nS	přesvědčovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
členkou	členka	k1gFnSc7	členka
organizovaného	organizovaný	k2eAgInSc2d1	organizovaný
odboje	odboj	k1gInSc2	odboj
<g/>
,	,	kIx,	,
Sophie	Sophie	k1gFnSc1	Sophie
odmítá	odmítat	k5eAaImIp3nS	odmítat
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
volba	volba	k1gFnSc1	volba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
pro	pro	k7c4	pro
hrdinčino	hrdinčin	k2eAgNnSc4d1	hrdinčino
celoživotní	celoživotní	k2eAgNnSc4d1	celoživotní
trauma	trauma	k1gNnSc4	trauma
klíčový	klíčový	k2eAgInSc1d1	klíčový
význam	význam	k1gInSc1	význam
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
volba	volba	k1gFnSc1	volba
<g/>
,	,	kIx,	,
před	před	k7c4	před
niž	jenž	k3xRgFnSc4	jenž
Sophii	Sophie	k1gFnSc4	Sophie
postaví	postavit	k5eAaPmIp3nS	postavit
sadistický	sadistický	k2eAgMnSc1d1	sadistický
německý	německý	k2eAgMnSc1d1	německý
lékař	lékař	k1gMnSc1	lékař
při	při	k7c6	při
jejím	její	k3xOp3gInSc6	její
příchodu	příchod	k1gInSc6	příchod
do	do	k7c2	do
koncentračního	koncentrační	k2eAgInSc2d1	koncentrační
tábora	tábor	k1gInSc2	tábor
<g/>
.	.	kIx.	.
</s>
<s>
Nechá	nechat	k5eAaPmIp3nS	nechat
ji	on	k3xPp3gFnSc4	on
vybrat	vybrat	k5eAaPmF	vybrat
si	se	k3xPyFc3	se
z	z	k7c2	z
jejich	jejich	k3xOp3gFnPc2	jejich
dvou	dva	k4xCgFnPc2	dva
dětí	dítě	k1gFnPc2	dítě
jedno	jeden	k4xCgNnSc1	jeden
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bude	být	k5eAaImBp3nS	být
dále	daleko	k6eAd2	daleko
žit	žít	k5eAaImNgInS	žít
v	v	k7c6	v
koncentračním	koncentrační	k2eAgInSc6d1	koncentrační
táboře	tábor	k1gInSc6	tábor
Osvětim	Osvětim	k1gFnSc4	Osvětim
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
druhé	druhý	k4xOgNnSc1	druhý
bude	být	k5eAaImBp3nS	být
odvezeno	odvézt	k5eAaPmNgNnS	odvézt
do	do	k7c2	do
nedalekého	daleký	k2eNgInSc2d1	nedaleký
tábora	tábor	k1gInSc2	tábor
<g/>
,	,	kIx,	,
určeného	určený	k2eAgInSc2d1	určený
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
likvidaci	likvidace	k1gFnSc4	likvidace
vězňů	vězeň	k1gMnPc2	vězeň
<g/>
.	.	kIx.	.
</s>
<s>
Sophie	Sophie	k1gFnSc1	Sophie
si	se	k3xPyFc3	se
vybere	vybrat	k5eAaPmIp3nS	vybrat
svého	svůj	k3xOyFgMnSc4	svůj
desetiletého	desetiletý	k2eAgMnSc4d1	desetiletý
syna	syn	k1gMnSc4	syn
Jana	Jan	k1gMnSc4	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Sama	sám	k3xTgFnSc1	sám
pak	pak	k6eAd1	pak
sleduje	sledovat	k5eAaImIp3nS	sledovat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
jí	on	k3xPp3gFnSc7	on
nacisté	nacista	k1gMnPc1	nacista
odvádějí	odvádět	k5eAaImIp3nP	odvádět
její	její	k3xOp3gFnSc4	její
malou	malý	k2eAgFnSc4d1	malá
dceru	dcera	k1gFnSc4	dcera
Evu	Eva	k1gFnSc4	Eva
k	k	k7c3	k
transportu	transport	k1gInSc3	transport
do	do	k7c2	do
Březinky	březinka	k1gFnSc2	březinka
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc1	třetí
volba	volba	k1gFnSc1	volba
je	být	k5eAaImIp3nS	být
Sophiino	Sophiin	k2eAgNnSc4d1	Sophiino
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
pokusit	pokusit	k5eAaPmF	pokusit
se	se	k3xPyFc4	se
svést	svést	k5eAaPmF	svést
Rudolfa	Rudolf	k1gMnSc2	Rudolf
Hösse	Höss	k1gMnSc2	Höss
<g/>
,	,	kIx,	,
vrchního	vrchní	k2eAgMnSc2d1	vrchní
velitele	velitel	k1gMnSc2	velitel
osvětimského	osvětimský	k2eAgInSc2d1	osvětimský
tábora	tábor	k1gInSc2	tábor
<g/>
.	.	kIx.	.
</s>
<s>
Volba	volba	k1gFnSc1	volba
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
krajního	krajní	k2eAgNnSc2d1	krajní
zoufalství	zoufalství	k1gNnSc2	zoufalství
matky	matka	k1gFnSc2	matka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
chce	chtít	k5eAaImIp3nS	chtít
zachránit	zachránit	k5eAaPmF	zachránit
svého	svůj	k3xOyFgMnSc4	svůj
potomka	potomek	k1gMnSc4	potomek
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
pokus	pokus	k1gInSc1	pokus
ztroskotává	ztroskotávat	k5eAaImIp3nS	ztroskotávat
a	a	k8xC	a
jen	jen	k9	jen
prohlubuje	prohlubovat	k5eAaImIp3nS	prohlubovat
bezmocnost	bezmocnost	k1gFnSc4	bezmocnost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
zakotvuje	zakotvovat	k5eAaImIp3nS	zakotvovat
níž	nízce	k6eAd2	nízce
a	a	k8xC	a
níž	nízce	k6eAd2	nízce
do	do	k7c2	do
Sophiiny	Sophiin	k2eAgFnSc2d1	Sophiina
duše	duše	k1gFnSc2	duše
<g/>
.	.	kIx.	.
</s>
<s>
Závěrečná	závěrečný	k2eAgFnSc1d1	závěrečná
hrdinčina	hrdinčin	k2eAgFnSc1d1	hrdinčina
volba	volba	k1gFnSc1	volba
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
z	z	k7c2	z
jejího	její	k3xOp3gNnSc2	její
konečného	konečný	k2eAgNnSc2d1	konečné
poznání	poznání	k1gNnSc2	poznání
<g/>
,	,	kIx,	,
že	že	k8xS	že
život	život	k1gInSc1	život
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
nelze	lze	k6eNd1	lze
obnovit	obnovit	k5eAaPmF	obnovit
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
jako	jako	k8xS	jako
se	se	k3xPyFc4	se
dalo	dát	k5eAaPmAgNnS	dát
díky	díky	k7c3	díky
Nathanově	Nathanův	k2eAgFnSc3d1	Nathanova
péči	péče	k1gFnSc3	péče
obnovit	obnovit	k5eAaPmF	obnovit
její	její	k3xOp3gNnSc4	její
fyzické	fyzický	k2eAgNnSc4d1	fyzické
zdraví	zdraví	k1gNnSc4	zdraví
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
jakékoliv	jakýkoliv	k3yIgInPc1	jakýkoliv
pokusy	pokus	k1gInPc1	pokus
zbavit	zbavit	k5eAaPmF	zbavit
se	se	k3xPyFc4	se
bolestných	bolestný	k2eAgFnPc2d1	bolestná
traumat	trauma	k1gNnPc2	trauma
minulosti	minulost	k1gFnSc2	minulost
a	a	k8xC	a
rozvinout	rozvinout	k5eAaPmF	rozvinout
vlastní	vlastní	k2eAgFnSc4d1	vlastní
šťastnou	šťastný	k2eAgFnSc4d1	šťastná
vyrovnanou	vyrovnaný	k2eAgFnSc4d1	vyrovnaná
osobnost	osobnost	k1gFnSc4	osobnost
jsou	být	k5eAaImIp3nP	být
marné	marný	k2eAgInPc1d1	marný
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
i	i	k9	i
její	její	k3xOp3gFnSc1	její
jediná	jediný	k2eAgFnSc1d1	jediná
naděje	naděje	k1gFnSc1	naděje
<g/>
,	,	kIx,	,
začít	začít	k5eAaPmF	začít
nový	nový	k2eAgInSc4d1	nový
život	život	k1gInSc4	život
s	s	k7c7	s
Nathanem	Nathan	k1gMnSc7	Nathan
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ukáže	ukázat	k5eAaPmIp3nS	ukázat
být	být	k5eAaImF	být
klamná	klamný	k2eAgFnSc1d1	klamná
a	a	k8xC	a
nerealizovatelná	realizovatelný	k2eNgFnSc1d1	nerealizovatelná
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
jí	on	k3xPp3gFnSc3	on
nezbývá	zbývat	k5eNaImIp3nS	zbývat
než	než	k8xS	než
navždy	navždy	k6eAd1	navždy
zhasit	zhasit	k5eAaPmF	zhasit
světlo	světlo	k1gNnSc4	světlo
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Sophině	Sophin	k2eAgFnSc6d1	Sophin
volbě	volba	k1gFnSc6	volba
se	se	k3xPyFc4	se
William	William	k1gInSc1	William
Styron	Styron	k1gInSc1	Styron
pokouší	pokoušet	k5eAaImIp3nS	pokoušet
o	o	k7c4	o
hloubkovou	hloubkový	k2eAgFnSc4d1	hloubková
analýzu	analýza	k1gFnSc4	analýza
nacismu	nacismus	k1gInSc2	nacismus
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc2	jeho
zhoubného	zhoubný	k2eAgInSc2d1	zhoubný
vlivu	vliv	k1gInSc2	vliv
na	na	k7c4	na
průměrně	průměrně	k6eAd1	průměrně
slabého	slabý	k2eAgNnSc2d1	slabé
nebo	nebo	k8xC	nebo
možná	možná	k9	možná
jen	jen	k9	jen
průměrně	průměrně	k6eAd1	průměrně
silného	silný	k2eAgMnSc2d1	silný
lidského	lidský	k2eAgMnSc2d1	lidský
jedince	jedinec	k1gMnSc2	jedinec
a	a	k8xC	a
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
nacismus	nacismus	k1gInSc1	nacismus
v	v	k7c6	v
osobních	osobní	k2eAgFnPc6d1	osobní
perspektivách	perspektiva	k1gFnPc6	perspektiva
<g/>
.	.	kIx.	.
</s>
<s>
Volí	volit	k5eAaImIp3nS	volit
proto	proto	k8xC	proto
hrdinku	hrdinka	k1gFnSc4	hrdinka
až	až	k9	až
absurdní	absurdní	k2eAgMnSc1d1	absurdní
svou	svůj	k3xOyFgFnSc7	svůj
nenápadností	nenápadnost	k1gFnSc7	nenápadnost
a	a	k8xC	a
svou	svůj	k3xOyFgFnSc7	svůj
neprovinilostí	neprovinilost	k1gFnSc7	neprovinilost
vůči	vůči	k7c3	vůči
ideálům	ideál	k1gInPc3	ideál
nacismu	nacismus	k1gInSc2	nacismus
<g/>
.	.	kIx.	.
</s>
<s>
Sophie	Sophie	k1gFnSc1	Sophie
není	být	k5eNaImIp3nS	být
židovka	židovka	k1gFnSc1	židovka
<g/>
,	,	kIx,	,
nemá	mít	k5eNaImIp3nS	mít
neárijské	árijský	k2eNgInPc4d1	árijský
rysy	rys	k1gInPc4	rys
<g/>
,	,	kIx,	,
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
splňuje	splňovat	k5eAaImIp3nS	splňovat
svým	svůj	k3xOyFgInSc7	svůj
zevnějškem	zevnějšek	k1gInSc7	zevnějšek
i	i	k9	i
představy	představa	k1gFnPc1	představa
nacistické	nacistický	k2eAgFnSc2d1	nacistická
estetiky	estetika	k1gFnSc2	estetika
<g/>
.	.	kIx.	.
</s>
<s>
Jestli	jestli	k8xS	jestli
je	být	k5eAaImIp3nS	být
Sophie	Sophie	k1gFnSc1	Sophie
něčím	něčí	k3xOyIgNnSc7	něčí
výjimečná	výjimečný	k2eAgFnSc1d1	výjimečná
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
pouze	pouze	k6eAd1	pouze
citlivostí	citlivost	k1gFnSc7	citlivost
<g/>
,	,	kIx,	,
vnitřní	vnitřní	k2eAgFnSc7d1	vnitřní
kultivovaností	kultivovanost	k1gFnSc7	kultivovanost
a	a	k8xC	a
inteligencí	inteligence	k1gFnSc7	inteligence
<g/>
.	.	kIx.	.
</s>
<s>
Styron	Styron	k1gInSc1	Styron
používá	používat	k5eAaImIp3nS	používat
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
vyprávění	vyprávění	k1gNnSc6	vyprávění
bohatě	bohatě	k6eAd1	bohatě
členěnou	členěný	k2eAgFnSc4d1	členěná
větu	věta	k1gFnSc4	věta
(	(	kIx(	(
<g/>
po	po	k7c6	po
vzoru	vzor	k1gInSc6	vzor
Faulknera	Faulkner	k1gMnSc2	Faulkner
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
si	se	k3xPyFc3	se
činí	činit	k5eAaImIp3nS	činit
neobyčejné	obyčejný	k2eNgInPc4d1	neobyčejný
nároky	nárok	k1gInPc4	nárok
na	na	k7c4	na
čtenářovu	čtenářův	k2eAgFnSc4d1	čtenářova
spolupráci	spolupráce	k1gFnSc4	spolupráce
a	a	k8xC	a
průběžné	průběžný	k2eAgNnSc4d1	průběžné
dešifrování	dešifrování	k1gNnSc4	dešifrování
skrytých	skrytý	k2eAgInPc2d1	skrytý
významů	význam	k1gInPc2	význam
a	a	k8xC	a
souvislostí	souvislost	k1gFnPc2	souvislost
<g/>
.	.	kIx.	.
</s>
<s>
Tvorba	tvorba	k1gFnSc1	tvorba
tohoto	tento	k3xDgNnSc2	tento
díla	dílo	k1gNnSc2	dílo
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
postupném	postupný	k2eAgNnSc6d1	postupné
odhalování	odhalování	k1gNnSc6	odhalování
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
nás	my	k3xPp1nPc2	my
do	do	k7c2	do
posledních	poslední	k2eAgFnPc2d1	poslední
stránek	stránka	k1gFnPc2	stránka
nad	nad	k7c7	nad
Sophiiným	Sophiin	k2eAgInSc7d1	Sophiin
osudem	osud	k1gInSc7	osud
udržuje	udržovat	k5eAaImIp3nS	udržovat
v	v	k7c6	v
napětí	napětí	k1gNnSc6	napětí
<g/>
.	.	kIx.	.
</s>
<s>
Sophiina	Sophiin	k2eAgFnSc1d1	Sophiina
volba	volba	k1gFnSc1	volba
–	–	k?	–
romantické	romantický	k2eAgNnSc4d1	romantické
drama	drama	k1gNnSc4	drama
režiséra	režisér	k1gMnSc2	režisér
Alana	Alan	k1gMnSc2	Alan
J.	J.	kA	J.
Pakuly	Pakula	k1gFnSc2	Pakula
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavních	hlavní	k2eAgFnPc6d1	hlavní
rolích	role	k1gFnPc6	role
Meryl	Meryl	k1gInSc4	Meryl
Streepová	Streepová	k1gFnSc1	Streepová
(	(	kIx(	(
<g/>
za	za	k7c4	za
roli	role	k1gFnSc4	role
Sophie	Sophie	k1gFnSc2	Sophie
získala	získat	k5eAaPmAgFnS	získat
Oscara	Oscara	k1gFnSc1	Oscara
a	a	k8xC	a
Zlatý	zlatý	k2eAgInSc1d1	zlatý
glóbus	glóbus	k1gInSc1	glóbus
<g/>
)	)	kIx)	)
,	,	kIx,	,
Kevin	Kevin	k1gMnSc1	Kevin
Kline	klinout	k5eAaImIp3nS	klinout
a	a	k8xC	a
Peter	Peter	k1gMnSc1	Peter
MacNicol	MacNicola	k1gFnPc2	MacNicola
<g/>
.	.	kIx.	.
</s>
