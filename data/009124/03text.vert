<p>
<s>
Theatre	Theatr	k1gMnSc5	Theatr
of	of	k?	of
Tragedy	Traged	k1gMnPc7	Traged
byla	být	k5eAaImAgFnS	být
norská	norský	k2eAgFnSc1d1	norská
metalová	metalový	k2eAgFnSc1d1	metalová
kapela	kapela	k1gFnSc1	kapela
založená	založený	k2eAgFnSc1d1	založená
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
během	během	k7c2	během
svého	svůj	k3xOyFgInSc2	svůj
vývoje	vývoj	k1gInSc2	vývoj
stihla	stihnout	k5eAaPmAgFnS	stihnout
projít	projít	k5eAaPmF	projít
hned	hned	k9	hned
několika	několik	k4yIc7	několik
hudebními	hudební	k2eAgInPc7d1	hudební
styly	styl	k1gInPc7	styl
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
se	se	k3xPyFc4	se
pohybovala	pohybovat	k5eAaImAgFnS	pohybovat
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
death-doom	deathoom	k1gInSc4	death-doom
<g/>
,	,	kIx,	,
po	po	k7c6	po
dvou	dva	k4xCgNnPc6	dva
řadových	řadový	k2eAgNnPc6d1	řadové
albech	album	k1gNnPc6	album
se	se	k3xPyFc4	se
posunula	posunout	k5eAaPmAgFnS	posunout
více	hodně	k6eAd2	hodně
k	k	k7c3	k
žánru	žánr	k1gInSc3	žánr
industrial	industriat	k5eAaBmAgMnS	industriat
metal	metal	k1gInSc4	metal
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
žánrový	žánrový	k2eAgInSc1d1	žánrový
posun	posun	k1gInSc1	posun
jim	on	k3xPp3gMnPc3	on
vydržel	vydržet	k5eAaPmAgInS	vydržet
opět	opět	k6eAd1	opět
jen	jen	k9	jen
dvě	dva	k4xCgNnPc4	dva
alba	album	k1gNnPc4	album
–	–	k?	–
v	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
kariéry	kariéra	k1gFnSc2	kariéra
už	už	k6eAd1	už
byla	být	k5eAaImAgFnS	být
tvorba	tvorba	k1gFnSc1	tvorba
kapely	kapela	k1gFnSc2	kapela
převážně	převážně	k6eAd1	převážně
gothic	gothice	k1gInPc2	gothice
metalová	metalový	k2eAgFnSc1d1	metalová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2010	[number]	k4	2010
vydala	vydat	k5eAaPmAgFnS	vydat
skupina	skupina	k1gFnSc1	skupina
prohlášení	prohlášení	k1gNnSc2	prohlášení
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
informuje	informovat	k5eAaBmIp3nS	informovat
fanoušky	fanoušek	k1gMnPc4	fanoušek
o	o	k7c6	o
svém	svůj	k3xOyFgNnSc6	svůj
rozhodnutí	rozhodnutí	k1gNnSc6	rozhodnutí
ukončit	ukončit	k5eAaPmF	ukončit
činnost	činnost	k1gFnSc4	činnost
Theatre	Theatr	k1gInSc5	Theatr
of	of	k?	of
Tragedy	Traged	k1gMnPc4	Traged
k	k	k7c3	k
datu	datum	k1gNnSc3	datum
2	[number]	k4	2
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
byla	být	k5eAaImAgFnS	být
touha	touha	k1gFnSc1	touha
trávit	trávit	k5eAaImF	trávit
více	hodně	k6eAd2	hodně
času	čas	k1gInSc2	čas
s	s	k7c7	s
rodinami	rodina	k1gFnPc7	rodina
a	a	k8xC	a
neschopnost	neschopnost	k1gFnSc1	neschopnost
dalšího	další	k2eAgNnSc2d1	další
pokračování	pokračování	k1gNnSc2	pokračování
v	v	k7c6	v
"	"	kIx"	"
<g/>
rokenrolovém	rokenrolový	k2eAgMnSc6d1	rokenrolový
<g/>
"	"	kIx"	"
životním	životní	k2eAgInSc6d1	životní
stylu	styl	k1gInSc6	styl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Členové	člen	k1gMnPc1	člen
kapely	kapela	k1gFnSc2	kapela
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poslední	poslední	k2eAgNnSc4d1	poslední
složení	složení	k1gNnSc4	složení
===	===	k?	===
</s>
</p>
<p>
<s>
Raymond	Raymond	k1gMnSc1	Raymond
István	István	k2eAgMnSc1d1	István
Rohonyi	Rohony	k1gFnPc4	Rohony
-	-	kIx~	-
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
texty	text	k1gInPc1	text
</s>
</p>
<p>
<s>
Nell	Nell	k1gInSc1	Nell
Sigland	Sigland	k1gInSc1	Sigland
-	-	kIx~	-
zpěv	zpěv	k1gInSc1	zpěv
</s>
</p>
<p>
<s>
Frank	Frank	k1gMnSc1	Frank
Claussen	Claussen	k1gInSc1	Claussen
-	-	kIx~	-
kytara	kytara	k1gFnSc1	kytara
</s>
</p>
<p>
<s>
Vegard	Vegard	k1gInSc1	Vegard
K.	K.	kA	K.
Thorsen	Thorsen	k1gInSc1	Thorsen
-	-	kIx~	-
kytara	kytara	k1gFnSc1	kytara
</s>
</p>
<p>
<s>
Lorentz	Lorentz	k1gInSc1	Lorentz
Aspen	Aspen	k1gInSc1	Aspen
-	-	kIx~	-
keyboard	keyboard	k1gInSc1	keyboard
</s>
</p>
<p>
<s>
Hein	Hein	k1gMnSc1	Hein
Frode	Frod	k1gInSc5	Frod
Hansen	Hansen	k2eAgInSc4d1	Hansen
-	-	kIx~	-
bicí	bicí	k2eAgInSc4d1	bicí
</s>
</p>
<p>
<s>
===	===	k?	===
Bývalí	bývalý	k2eAgMnPc1d1	bývalý
členové	člen	k1gMnPc1	člen
===	===	k?	===
</s>
</p>
<p>
<s>
Liv	Liv	k?	Liv
Kristine	Kristin	k1gMnSc5	Kristin
Espenæ	Espenæ	k1gMnSc5	Espenæ
Krull	Krull	k1gInSc1	Krull
-	-	kIx~	-
zpěv	zpěv	k1gInSc1	zpěv
</s>
</p>
<p>
<s>
Tommy	Tomm	k1gInPc1	Tomm
Lindal	Lindal	k1gFnSc2	Lindal
-	-	kIx~	-
kytara	kytara	k1gFnSc1	kytara
</s>
</p>
<p>
<s>
Tommy	Tomm	k1gInPc1	Tomm
Olsson	Olssona	k1gFnPc2	Olssona
-	-	kIx~	-
kytara	kytara	k1gFnSc1	kytara
</s>
</p>
<p>
<s>
På	På	k?	På
Bjå	Bjå	k1gFnSc1	Bjå
-	-	kIx~	-
kytara	kytara	k1gFnSc1	kytara
</s>
</p>
<p>
<s>
Geir	Geir	k1gInSc1	Geir
Flikkeid	Flikkeid	k1gInSc1	Flikkeid
-	-	kIx~	-
kytara	kytara	k1gFnSc1	kytara
</s>
</p>
<p>
<s>
Eirik	Eirik	k1gMnSc1	Eirik
T.	T.	kA	T.
Saltrø	Saltrø	k1gMnSc1	Saltrø
-	-	kIx~	-
baskytara	baskytara	k1gFnSc1	baskytara
</s>
</p>
<p>
<s>
==	==	k?	==
Diskografie	diskografie	k1gFnSc2	diskografie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Alba	album	k1gNnSc2	album
===	===	k?	===
</s>
</p>
<p>
<s>
Theatre	Theatr	k1gMnSc5	Theatr
of	of	k?	of
Tragedy	Traged	k1gMnPc7	Traged
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Velvet	Velvet	k1gInSc1	Velvet
Darkness	Darknessa	k1gFnPc2	Darknessa
They	Thea	k1gFnSc2	Thea
Fear	Fear	k1gMnSc1	Fear
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Aégis	Aégis	k1gFnSc1	Aégis
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Musique	Musique	k1gFnSc1	Musique
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Closure	Closur	k1gMnSc5	Closur
<g/>
:	:	kIx,	:
Live	Live	k1gNnPc6	Live
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Assembly	Assembnout	k5eAaPmAgFnP	Assembnout
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Storm	Storm	k1gInSc1	Storm
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Forever	Forever	k1gMnSc1	Forever
Is	Is	k1gMnSc1	Is
the	the	k?	the
World	World	k1gMnSc1	World
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Last	Last	k2eAgMnSc1d1	Last
Curtain	Curtain	k1gMnSc1	Curtain
Call	Call	k1gMnSc1	Call
(	(	kIx(	(
<g/>
Live-DVD	Live-DVD	k1gMnSc1	Live-DVD
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Singly	singl	k1gInPc4	singl
a	a	k8xC	a
EP	EP	kA	EP
===	===	k?	===
</s>
</p>
<p>
<s>
Theatre	Theatr	k1gMnSc5	Theatr
of	of	k?	of
Tragedy	Traged	k1gMnPc7	Traged
(	(	kIx(	(
<g/>
demo	demo	k2eAgNnSc2d1	demo
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Der	drát	k5eAaImRp2nS	drát
Tanz	Tanz	k1gInSc1	Tanz
Der	drát	k5eAaImRp2nS	drát
Schatten	Schattno	k1gNnPc2	Schattno
(	(	kIx(	(
<g/>
single	singl	k1gInSc5	singl
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
A	a	k9	a
Rose	Rose	k1gMnSc1	Rose
for	forum	k1gNnPc2	forum
the	the	k?	the
Dead	Dead	k1gMnSc1	Dead
(	(	kIx(	(
<g/>
EP	EP	kA	EP
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Cassandra	Cassandra	k1gFnSc1	Cassandra
(	(	kIx(	(
<g/>
single	singl	k1gInSc5	singl
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Virago	Virago	k1gMnSc1	Virago
(	(	kIx(	(
<g/>
EP	EP	kA	EP
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Image	image	k1gFnSc1	image
(	(	kIx(	(
<g/>
single	singl	k1gInSc5	singl
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Inperspective	Inperspectiv	k1gInSc5	Inperspectiv
(	(	kIx(	(
<g/>
EP	EP	kA	EP
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Machine	Machinout	k5eAaPmIp3nS	Machinout
(	(	kIx(	(
<g/>
single	singl	k1gInSc5	singl
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Envision	Envision	k1gInSc1	Envision
(	(	kIx(	(
<g/>
single	singl	k1gInSc5	singl
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Let	let	k1gInSc1	let
You	You	k1gMnSc1	You
Down	Down	k1gMnSc1	Down
(	(	kIx(	(
<g/>
single	singl	k1gInSc5	singl
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Storm	Storm	k1gInSc1	Storm
(	(	kIx(	(
<g/>
single	singl	k1gInSc5	singl
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Deadland	Deadland	k1gInSc1	Deadland
(	(	kIx(	(
<g/>
single	singl	k1gInSc5	singl
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Addenda	addendum	k1gNnPc1	addendum
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Theatre	Theatr	k1gInSc5	Theatr
of	of	k?	of
Tragedy	Trageda	k1gMnSc2	Trageda
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Obsáhlá	obsáhlý	k2eAgFnSc1d1	obsáhlá
biografie	biografie	k1gFnSc1	biografie
kapely	kapela	k1gFnSc2	kapela
-	-	kIx~	-
česky	česky	k6eAd1	česky
</s>
</p>
