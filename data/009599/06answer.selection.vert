<s>
Camembert	camembert	k1gInSc1	camembert
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
z	z	k7c2	z
nepasterovaného	pasterovaný	k2eNgNnSc2d1	nepasterované
kravského	kravský	k2eAgNnSc2d1	kravské
mléka	mléko	k1gNnSc2	mléko
<g/>
,	,	kIx,	,
zraje	zrát	k5eAaImIp3nS	zrát
ve	v	k7c6	v
formách	forma	k1gFnPc6	forma
za	za	k7c2	za
přítomnosti	přítomnost	k1gFnSc2	přítomnost
plísní	plísnit	k5eAaImIp3nS	plísnit
Penicillium	Penicillium	k1gNnSc1	Penicillium
camemberti	camembert	k1gMnPc1	camembert
(	(	kIx(	(
<g/>
synonymum	synonymum	k1gNnSc1	synonymum
Penicillium	Penicillium	k1gNnSc1	Penicillium
candidum	candidum	k1gInSc1	candidum
<g/>
)	)	kIx)	)
a	a	k8xC	a
nejméně	málo	k6eAd3	málo
tři	tři	k4xCgInPc4	tři
týdny	týden	k1gInPc4	týden
<g/>
.	.	kIx.	.
</s>
