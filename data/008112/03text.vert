<s>
Univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Cambridgi	Cambridge	k1gFnSc6	Cambridge
<g/>
,	,	kIx,	,
či	či	k8xC	či
Cambridgeská	cambridgeský	k2eAgFnSc1d1	Cambridgeská
univerzita	univerzita	k1gFnSc1	univerzita
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
University	universita	k1gFnPc1	universita
of	of	k?	of
Cambridge	Cambridge	k1gFnSc2	Cambridge
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
také	také	k9	také
Cambridge	Cambridge	k1gFnSc1	Cambridge
University	universita	k1gFnSc2	universita
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Universitas	Universitas	k1gMnSc1	Universitas
Cantabrigiensis	Cantabrigiensis	k1gFnSc2	Cantabrigiensis
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
druhá	druhý	k4xOgFnSc1	druhý
nejstarší	starý	k2eAgFnSc1d3	nejstarší
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
anglicky	anglicky	k6eAd1	anglicky
mluvící	mluvící	k2eAgFnSc6d1	mluvící
části	část	k1gFnSc6	část
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Univerzita	univerzita	k1gFnSc1	univerzita
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
anglickém	anglický	k2eAgNnSc6d1	anglické
městě	město	k1gNnSc6	město
Cambridge	Cambridge	k1gFnSc2	Cambridge
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
má	mít	k5eAaImIp3nS	mít
přibližně	přibližně	k6eAd1	přibližně
19	[number]	k4	19
000	[number]	k4	000
studentů	student	k1gMnPc2	student
rozdělených	rozdělený	k2eAgMnPc2d1	rozdělený
v	v	k7c4	v
31	[number]	k4	31
colleges	collegesa	k1gFnPc2	collegesa
(	(	kIx(	(
<g/>
kolejích	kolej	k1gFnPc6	kolej
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jsou	být	k5eAaImIp3nP	být
víceméně	víceméně	k9	víceméně
nezávislé	závislý	k2eNgFnPc1d1	nezávislá
instituce	instituce	k1gFnPc1	instituce
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mezi	mezi	k7c7	mezi
jiným	jiný	k2eAgInSc7d1	jiný
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
studentům	student	k1gMnPc3	student
i	i	k8xC	i
ubytování	ubytování	k1gNnSc2	ubytování
a	a	k8xC	a
stravu	strava	k1gFnSc4	strava
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
Oxfordskou	oxfordský	k2eAgFnSc7d1	Oxfordská
univerzitou	univerzita	k1gFnSc7	univerzita
jsou	být	k5eAaImIp3nP	být
označovány	označovat	k5eAaImNgInP	označovat
jako	jako	k9	jako
Oxbridge	Oxbridg	k1gInPc4	Oxbridg
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
známá	známý	k2eAgFnSc1d1	známá
je	být	k5eAaImIp3nS	být
také	také	k9	také
jejich	jejich	k3xOp3gFnSc1	jejich
rivalita	rivalita	k1gFnSc1	rivalita
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
táhne	táhnout	k5eAaImIp3nS	táhnout
už	už	k6eAd1	už
od	od	k7c2	od
založení	založení	k1gNnSc2	založení
Cambridgeské	cambridgeský	k2eAgFnSc2d1	Cambridgeská
university	universita	k1gFnSc2	universita
učenci	učenec	k1gMnPc7	učenec
prchajícími	prchající	k2eAgMnPc7d1	prchající
z	z	k7c2	z
Oxfordu	Oxford	k1gInSc2	Oxford
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
pověsti	pověst	k1gFnSc2	pověst
založili	založit	k5eAaPmAgMnP	založit
Univerzitu	univerzita	k1gFnSc4	univerzita
v	v	k7c6	v
Cambridgi	Cambridge	k1gFnSc6	Cambridge
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1209	[number]	k4	1209
akademici	akademik	k1gMnPc1	akademik
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
po	po	k7c6	po
neshodách	neshoda	k1gFnPc6	neshoda
na	na	k7c6	na
Oxfordské	oxfordský	k2eAgFnSc6d1	Oxfordská
univerzitě	univerzita	k1gFnSc6	univerzita
uprchli	uprchnout	k5eAaPmAgMnP	uprchnout
a	a	k8xC	a
založili	založit	k5eAaPmAgMnP	založit
si	se	k3xPyFc3	se
vlastní	vlastní	k2eAgFnSc4d1	vlastní
univerzitu	univerzita	k1gFnSc4	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgMnSc1d3	nejstarší
z	z	k7c2	z
kolejí	kolej	k1gFnPc2	kolej
je	být	k5eAaImIp3nS	být
Peterhouse	Peterhouse	k1gFnSc1	Peterhouse
<g/>
,	,	kIx,	,
založená	založený	k2eAgFnSc1d1	založená
roku	rok	k1gInSc2	rok
1284	[number]	k4	1284
biskupem	biskup	k1gMnSc7	biskup
z	z	k7c2	z
Ely	Ela	k1gFnSc2	Ela
Hugem	Hugo	k1gMnSc7	Hugo
de	de	k?	de
Balsahamem	Balsaham	k1gInSc7	Balsaham
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
cambridgeských	cambridgeský	k2eAgFnPc2d1	Cambridgeská
kolejí	kolej	k1gFnPc2	kolej
(	(	kIx(	(
<g/>
colleges	colleges	k1gInSc1	colleges
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
jenom	jenom	k9	jenom
tři	tři	k4xCgInPc4	tři
ženské	ženský	k2eAgInPc4d1	ženský
(	(	kIx(	(
<g/>
Murray	Murray	k1gInPc4	Murray
Edwards	Edwardsa	k1gFnPc2	Edwardsa
<g/>
,	,	kIx,	,
Newnham	Newnham	k1gInSc4	Newnham
a	a	k8xC	a
Lucy	Lucy	k1gInPc4	Lucy
Cavendish	Cavendisha	k1gFnPc2	Cavendisha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ostatních	ostatní	k2eAgNnPc2d1	ostatní
28	[number]	k4	28
je	být	k5eAaImIp3nS	být
smíšených	smíšený	k2eAgNnPc2d1	smíšené
<g/>
.	.	kIx.	.
</s>
<s>
Christ	Christ	k1gInSc1	Christ
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Churchill	Churchill	k1gInSc1	Churchill
Clare	Clar	k1gInSc5	Clar
Clare	Clar	k1gInSc5	Clar
Hall	Hallum	k1gNnPc2	Hallum
Corpus	corpus	k1gNnSc2	corpus
Christi	Christ	k1gMnPc1	Christ
Darwin	Darwin	k1gMnSc1	Darwin
Downing	Downing	k1gInSc1	Downing
Emmanuel	Emmanuel	k1gMnSc1	Emmanuel
Fitzwilliam	Fitzwilliam	k1gInSc1	Fitzwilliam
Girton	Girton	k1gInSc1	Girton
Gonville	Gonville	k1gInSc1	Gonville
and	and	k?	and
Caius	Caius	k1gInSc1	Caius
Homerton	Homerton	k1gInSc1	Homerton
Hughes	Hughesa	k1gFnPc2	Hughesa
Hall	Hall	k1gMnSc1	Hall
Jesus	Jesus	k1gMnSc1	Jesus
King	King	k1gMnSc1	King
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Lucy	Luc	k1gMnPc7	Luc
Cavendish	Cavendisha	k1gFnPc2	Cavendisha
Magdalene	Magdalen	k1gInSc5	Magdalen
Murray	Murray	k1gInPc4	Murray
Edwards	Edwards	k1gInSc1	Edwards
(	(	kIx(	(
<g/>
New	New	k1gMnSc1	New
Hall	Hall	k1gMnSc1	Hall
<g/>
)	)	kIx)	)
Newnham	Newnham	k1gInSc1	Newnham
Pembroke	Pembrok	k1gFnSc2	Pembrok
Peterhouse	Peterhouse	k1gFnSc2	Peterhouse
Queens	Queens	k1gInSc1	Queens
<g/>
'	'	kIx"	'
Robinson	Robinson	k1gMnSc1	Robinson
St	St	kA	St
Catharine	Catharin	k1gInSc5	Catharin
<g/>
'	'	kIx"	'
<g />
.	.	kIx.	.
</s>
<s>
<g/>
s	s	k7c7	s
St	St	kA	St
Edmund	Edmund	k1gMnSc1	Edmund
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
St	St	kA	St
John	John	k1gMnSc1	John
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Selwyn	Selwyn	k1gInSc1	Selwyn
Sidney	Sidne	k2eAgInPc1d1	Sidne
Sussex	Sussex	k1gInSc1	Sussex
Trinity	Trinita	k1gFnSc2	Trinita
Trinity	Trinita	k1gFnSc2	Trinita
Hall	Halla	k1gFnPc2	Halla
Wolfson	Wolfson	k1gInSc1	Wolfson
Každá	každý	k3xTgFnSc1	každý
kolej	kolej	k1gFnSc1	kolej
má	mít	k5eAaImIp3nS	mít
vlastní	vlastní	k2eAgFnSc4d1	vlastní
knihovnu	knihovna	k1gFnSc4	knihovna
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pro	pro	k7c4	pro
případ	případ	k1gInSc4	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
student	student	k1gMnSc1	student
nenajde	najít	k5eNaPmIp3nS	najít
potřebné	potřebný	k2eAgFnPc4d1	potřebná
informace	informace	k1gFnPc4	informace
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
<g/>
,	,	kIx,	,
funguje	fungovat	k5eAaImIp3nS	fungovat
zde	zde	k6eAd1	zde
pověstná	pověstný	k2eAgFnSc1d1	pověstná
centrální	centrální	k2eAgFnSc1d1	centrální
knihovna	knihovna	k1gFnSc1	knihovna
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c7	mezi
největší	veliký	k2eAgFnSc7d3	veliký
a	a	k8xC	a
nejlépe	dobře	k6eAd3	dobře
vybavené	vybavený	k2eAgFnPc4d1	vybavená
knihovny	knihovna	k1gFnPc4	knihovna
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Přístup	přístup	k1gInSc4	přístup
má	mít	k5eAaImIp3nS	mít
každý	každý	k3xTgMnSc1	každý
student	student	k1gMnSc1	student
zdarma	zdarma	k6eAd1	zdarma
<g/>
.	.	kIx.	.
</s>
<s>
Univerzita	univerzita	k1gFnSc1	univerzita
je	být	k5eAaImIp3nS	být
především	především	k6eAd1	především
známa	znám	k2eAgFnSc1d1	známa
svými	svůj	k3xOyFgMnPc7	svůj
skvělými	skvělý	k2eAgMnPc7d1	skvělý
vědci	vědec	k1gMnPc7	vědec
a	a	k8xC	a
matematiky	matematik	k1gMnPc7	matematik
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
Isaac	Isaac	k1gInSc4	Isaac
Newton	newton	k1gInSc1	newton
(	(	kIx(	(
<g/>
dynamika	dynamika	k1gFnSc1	dynamika
<g/>
,	,	kIx,	,
gravitace	gravitace	k1gFnSc1	gravitace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Charles	Charles	k1gMnSc1	Charles
Darwin	Darwin	k1gMnSc1	Darwin
(	(	kIx(	(
<g/>
teorie	teorie	k1gFnSc1	teorie
evoluce	evoluce	k1gFnSc2	evoluce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
William	William	k1gInSc1	William
Harvey	Harvea	k1gFnSc2	Harvea
(	(	kIx(	(
<g/>
krevní	krevní	k2eAgInSc4d1	krevní
oběh	oběh	k1gInSc4	oběh
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Paul	Paul	k1gMnSc1	Paul
Dirac	Dirac	k1gFnSc1	Dirac
<g/>
,	,	kIx,	,
J.	J.	kA	J.
<g />
.	.	kIx.	.
</s>
<s>
J.	J.	kA	J.
Thomson	Thomson	k1gMnSc1	Thomson
(	(	kIx(	(
<g/>
objev	objev	k1gInSc1	objev
elektronu	elektron	k1gInSc2	elektron
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ernest	Ernest	k1gMnSc1	Ernest
Rutherford	Rutherford	k1gMnSc1	Rutherford
(	(	kIx(	(
<g/>
struktura	struktura	k1gFnSc1	struktura
atomu	atom	k1gInSc2	atom
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jane	Jan	k1gMnSc5	Jan
Goodall	Goodall	k1gInSc1	Goodall
<g/>
,	,	kIx,	,
James	James	k1gInSc1	James
Clerk	Clerk	k1gInSc1	Clerk
Maxwell	maxwell	k1gInSc1	maxwell
<g/>
,	,	kIx,	,
Francis	Francis	k1gFnSc1	Francis
Crick	Crick	k1gMnSc1	Crick
<g/>
,	,	kIx,	,
Alan	Alan	k1gMnSc1	Alan
Turing	Turing	k1gInSc1	Turing
<g/>
,	,	kIx,	,
Stephen	Stephen	k2eAgInSc1d1	Stephen
Hawking	Hawking	k1gInSc1	Hawking
nebo	nebo	k8xC	nebo
Frederick	Frederick	k1gMnSc1	Frederick
Sanger	Sanger	k1gMnSc1	Sanger
<g/>
.	.	kIx.	.
83	[number]	k4	83
absolventů	absolvent	k1gMnPc2	absolvent
bylo	být	k5eAaImAgNnS	být
odměněno	odměnit	k5eAaPmNgNnS	odměnit
Nobelovou	Nobelův	k2eAgFnSc7d1	Nobelova
cenou	cena	k1gFnSc7	cena
<g/>
,	,	kIx,	,
University	universita	k1gFnPc1	universita
of	of	k?	of
Cambridge	Cambridge	k1gFnSc1	Cambridge
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
univerzitou	univerzita	k1gFnSc7	univerzita
s	s	k7c7	s
největším	veliký	k2eAgInSc7d3	veliký
počtem	počet	k1gInSc7	počet
laureátů	laureát	k1gMnPc2	laureát
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
(	(	kIx(	(
<g/>
druhá	druhý	k4xOgFnSc1	druhý
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
s	s	k7c7	s
počtem	počet	k1gInSc7	počet
laureátů	laureát	k1gMnPc2	laureát
82	[number]	k4	82
je	být	k5eAaImIp3nS	být
University	universita	k1gFnPc4	universita
of	of	k?	of
Chicago	Chicago	k1gNnSc1	Chicago
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Cambridge	Cambridge	k1gFnSc1	Cambridge
a	a	k8xC	a
Oxford	Oxford	k1gInSc1	Oxford
jsou	být	k5eAaImIp3nP	být
akademicky	akademicky	k6eAd1	akademicky
nejselektivnější	selektivní	k2eAgFnPc1d3	selektivní
univerzity	univerzita	k1gFnPc1	univerzita
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
<g/>
.	.	kIx.	.
</s>
<s>
Student	student	k1gMnSc1	student
si	se	k3xPyFc3	se
může	moct	k5eAaImIp3nS	moct
jeden	jeden	k4xCgInSc4	jeden
rok	rok	k1gInSc4	rok
podat	podat	k5eAaPmF	podat
přihlášku	přihláška	k1gFnSc4	přihláška
jen	jen	k9	jen
na	na	k7c4	na
jednu	jeden	k4xCgFnSc4	jeden
univerzitu	univerzita	k1gFnSc4	univerzita
z	z	k7c2	z
Oxbridge	Oxbridg	k1gInSc2	Oxbridg
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
od	od	k7c2	od
ostatních	ostatní	k2eAgFnPc2d1	ostatní
univerzit	univerzita	k1gFnPc2	univerzita
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
<g/>
.	.	kIx.	.
</s>
<s>
Univerzita	univerzita	k1gFnSc1	univerzita
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
tradičně	tradičně	k6eAd1	tradičně
mezi	mezi	k7c7	mezi
5	[number]	k4	5
nejlepšími	dobrý	k2eAgFnPc7d3	nejlepší
univerzitami	univerzita	k1gFnPc7	univerzita
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
