<p>
<s>
Zřícenina	zřícenina	k1gFnSc1	zřícenina
hradu	hrad	k1gInSc2	hrad
Bílkov	Bílkov	k1gInSc1	Bílkov
leží	ležet	k5eAaImIp3nS	ležet
ve	v	k7c6	v
stejnojmenné	stejnojmenný	k2eAgFnSc6d1	stejnojmenná
obci	obec	k1gFnSc6	obec
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
součástí	součást	k1gFnSc7	součást
města	město	k1gNnSc2	město
Dačice	Dačice	k1gFnPc1	Dačice
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Jindřichův	Jindřichův	k2eAgInSc1d1	Jindřichův
Hradec	Hradec	k1gInSc1	Hradec
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
jsou	být	k5eAaImIp3nP	být
zbytky	zbytek	k1gInPc1	zbytek
hradu	hrad	k1gInSc2	hrad
chráněny	chránit	k5eAaImNgInP	chránit
jako	jako	k8xC	jako
kulturní	kulturní	k2eAgFnSc1d1	kulturní
památka	památka	k1gFnSc1	památka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
Bílkově	bílkově	k6eAd1	bílkově
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
píše	psát	k5eAaImIp3nS	psát
roku	rok	k1gInSc2	rok
1238	[number]	k4	1238
Ratibor	Ratibora	k1gFnPc2	Ratibora
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1253	[number]	k4	1253
Smil	smil	k1gInSc1	smil
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
hrad	hrad	k1gInSc4	hrad
získali	získat	k5eAaPmAgMnP	získat
páni	pan	k1gMnPc1	pan
z	z	k7c2	z
Hradce	Hradec	k1gInSc2	Hradec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1399	[number]	k4	1399
získala	získat	k5eAaPmAgFnS	získat
hrad	hrad	k1gInSc4	hrad
Eliška	Eliška	k1gFnSc1	Eliška
z	z	k7c2	z
Hradce	Hradec	k1gInSc2	Hradec
<g/>
,	,	kIx,	,
provdaná	provdaný	k2eAgFnSc1d1	provdaná
za	za	k7c4	za
Jana	Jan	k1gMnSc4	Jan
z	z	k7c2	z
Kravař	Kravaře	k1gInPc2	Kravaře
<g/>
.	.	kIx.	.
</s>
<s>
Hrad	hrad	k1gInSc4	hrad
přestál	přestát	k5eAaPmAgMnS	přestát
husitské	husitský	k2eAgFnSc2d1	husitská
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
Bílkov	Bílkov	k1gInSc4	Bílkov
poté	poté	k6eAd1	poté
vyvstal	vyvstat	k5eAaPmAgInS	vyvstat
spor	spor	k1gInSc1	spor
<g/>
.	.	kIx.	.
</s>
<s>
Měla	mít	k5eAaImAgFnS	mít
jej	on	k3xPp3gInSc4	on
držet	držet	k5eAaImF	držet
jako	jako	k9	jako
věno	věno	k1gNnSc4	věno
Anna	Anna	k1gFnSc1	Anna
z	z	k7c2	z
Hradce	Hradec	k1gInSc2	Hradec
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
provdala	provdat	k5eAaPmAgFnS	provdat
za	za	k7c4	za
Hynce	Hynce	k1gMnSc4	Hynce
Ptáčka	Ptáček	k1gMnSc4	Ptáček
z	z	k7c2	z
Pirkštejna	Pirkštejno	k1gNnSc2	Pirkštejno
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
nárokoval	nárokovat	k5eAaImAgMnS	nárokovat
si	se	k3xPyFc3	se
jej	on	k3xPp3gInSc4	on
i	i	k8xC	i
její	její	k3xOp3gNnSc4	její
příbuzný	příbuzný	k1gMnSc1	příbuzný
Menhard	Menhard	k1gMnSc1	Menhard
z	z	k7c2	z
Hradce	Hradec	k1gInSc2	Hradec
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1444	[number]	k4	1444
při	při	k7c6	při
nočním	noční	k2eAgInSc6d1	noční
přepadu	přepad	k1gInSc6	přepad
Hynce	Hynce	k1gMnSc1	Hynce
Ptáček	Ptáček	k1gMnSc1	Ptáček
z	z	k7c2	z
Pirkštejna	Pirkštejn	k1gInSc2	Pirkštejn
Bílkov	Bílkov	k1gInSc1	Bílkov
obsadil	obsadit	k5eAaPmAgInS	obsadit
a	a	k8xC	a
poničil	poničit	k5eAaPmAgInS	poničit
<g/>
.	.	kIx.	.
</s>
<s>
Hrad	hrad	k1gInSc1	hrad
už	už	k6eAd1	už
nebyl	být	k5eNaImAgInS	být
obnoven	obnovit	k5eAaPmNgInS	obnovit
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1447	[number]	k4	1447
o	o	k7c4	o
něj	on	k3xPp3gMnSc4	on
Menhard	Menhard	k1gMnSc1	Menhard
vedl	vést	k5eAaImAgMnS	vést
s	s	k7c7	s
Annou	Anna	k1gFnSc7	Anna
soudní	soudní	k2eAgInSc1d1	soudní
spor	spor	k1gInSc1	spor
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
jej	on	k3xPp3gMnSc4	on
roku	rok	k1gInSc2	rok
1452	[number]	k4	1452
nechala	nechat	k5eAaPmAgFnS	nechat
za	za	k7c4	za
5	[number]	k4	5
000	[number]	k4	000
kop	kop	k1gInSc4	kop
grošů	groš	k1gInPc2	groš
zapsat	zapsat	k5eAaPmF	zapsat
svému	svůj	k3xOyFgMnSc3	svůj
novému	nový	k2eAgMnSc3d1	nový
choti	choť	k1gMnSc3	choť
Jindřichu	Jindřich	k1gMnSc3	Jindřich
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Kruhlatovi	Kruhlatův	k2eAgMnPc1d1	Kruhlatův
z	z	k7c2	z
Michalovic	Michalovice	k1gFnPc2	Michalovice
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
připadl	připadnout	k5eAaPmAgMnS	připadnout
Menhartovým	Menhartův	k2eAgMnSc7d1	Menhartův
dědicům	dědic	k1gMnPc3	dědic
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
17	[number]	k4	17
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1459	[number]	k4	1459
jej	on	k3xPp3gMnSc4	on
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
ze	z	k7c2	z
Šternberka	Šternberk	k1gInSc2	Šternberk
<g/>
,	,	kIx,	,
správce	správce	k1gMnSc1	správce
Menhartova	Menhartův	k2eAgNnSc2d1	Menhartův
dědictví	dědictví	k1gNnSc2	dědictví
<g/>
,	,	kIx,	,
prodal	prodat	k5eAaPmAgInS	prodat
Volfgangovi	Volfgang	k1gMnSc3	Volfgang
Krajíři	Krajíř	k1gMnSc3	Krajíř
z	z	k7c2	z
Krajku	krajek	k1gInSc2	krajek
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1459	[number]	k4	1459
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
jako	jako	k9	jako
zbořený	zbořený	k2eAgMnSc1d1	zbořený
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hrad	hrad	k1gInSc1	hrad
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
staletí	staletí	k1gNnSc6	staletí
rozebírán	rozebírán	k2eAgInSc1d1	rozebírán
na	na	k7c4	na
stavební	stavební	k2eAgInSc4d1	stavební
materiál	materiál	k1gInSc4	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
místě	místo	k1gNnSc6	místo
byly	být	k5eAaImAgInP	být
postaveny	postavit	k5eAaPmNgInP	postavit
rodinné	rodinný	k2eAgInPc1d1	rodinný
domy	dům	k1gInPc1	dům
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
prakticky	prakticky	k6eAd1	prakticky
zachovaly	zachovat	k5eAaPmAgInP	zachovat
jen	jen	k9	jen
zbytky	zbytek	k1gInPc1	zbytek
příkopů	příkop	k1gInPc2	příkop
a	a	k8xC	a
kousek	kousek	k1gInSc4	kousek
zdi	zeď	k1gFnSc2	zeď
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
PLAČEK	Plaček	k1gMnSc1	Plaček
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
<g/>
:	:	kIx,	:
Ilustrovaná	ilustrovaný	k2eAgFnSc1d1	ilustrovaná
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
moravských	moravský	k2eAgInPc2d1	moravský
hradů	hrad	k1gInPc2	hrad
<g/>
,	,	kIx,	,
hrádků	hrádek	k1gInPc2	hrádek
a	a	k8xC	a
tvrzí	tvrz	k1gFnPc2	tvrz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Hrad	hrad	k1gInSc1	hrad
Bílkov	Bílkov	k1gInSc1	Bílkov
na	na	k7c6	na
webu	web	k1gInSc6	web
Pruvodce	Pruvodce	k1gMnSc2	Pruvodce
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Panství	panství	k1gNnSc1	panství
Bílkov	Bílkov	k1gInSc1	Bílkov
<g/>
,	,	kIx,	,
Ranožírovci	Ranožírovec	k1gMnPc1	Ranožírovec
</s>
</p>
