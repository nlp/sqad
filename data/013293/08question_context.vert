<s>
Zřícenina	zřícenina	k1gFnSc1	zřícenina
hradu	hrad	k1gInSc2	hrad
Bílkov	Bílkov	k1gInSc1	Bílkov
leží	ležet	k5eAaImIp3nS	ležet
ve	v	k7c6	v
stejnojmenné	stejnojmenný	k2eAgFnSc6d1	stejnojmenná
obci	obec	k1gFnSc6	obec
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
součástí	součást	k1gFnSc7	součást
města	město	k1gNnSc2	město
Dačice	Dačice	k1gFnPc1	Dačice
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Jindřichův	Jindřichův	k2eAgInSc1d1	Jindřichův
Hradec	Hradec	k1gInSc1	Hradec
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
jsou	být	k5eAaImIp3nP	být
zbytky	zbytek	k1gInPc1	zbytek
hradu	hrad	k1gInSc2	hrad
chráněny	chránit	k5eAaImNgInP	chránit
jako	jako	k8xC	jako
kulturní	kulturní	k2eAgFnSc1d1	kulturní
památka	památka	k1gFnSc1	památka
<g/>
.	.	kIx.	.
</s>
