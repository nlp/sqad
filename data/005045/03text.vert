<s>
Mezopotámie	Mezopotámie	k1gFnSc1	Mezopotámie
(	(	kIx(	(
<g/>
z	z	k7c2	z
řeckého	řecký	k2eAgNnSc2d1	řecké
Μ	Μ	k?	Μ
<g/>
,	,	kIx,	,
Mesopotamia	Mesopotamia	k1gFnSc1	Mesopotamia
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
země	země	k1gFnSc1	země
mezi	mezi	k7c7	mezi
řekami	řeka	k1gFnPc7	řeka
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
meziříčí	meziříčí	k1gNnSc1	meziříčí
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
arabsky	arabsky	k6eAd1	arabsky
ب	ب	k?	ب
ا	ا	k?	ا
bilā	bilā	k?	bilā
al-rā	alā	k?	al-rā
<g/>
,	,	kIx,	,
syrsky	syrsky	k6eAd1	syrsky
ܒ	ܒ	k?	ܒ
ܢ	ܢ	k?	ܢ
beth	beth	k1gMnSc1	beth
nahrain	nahrain	k1gMnSc1	nahrain
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
země	země	k1gFnSc1	země
řek	řeka	k1gFnPc2	řeka
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
oblast	oblast	k1gFnSc4	oblast
mezi	mezi	k7c7	mezi
řekami	řeka	k1gFnPc7	řeka
Eufrat	Eufrat	k1gInSc1	Eufrat
a	a	k8xC	a
Tigris	Tigris	k1gInSc1	Tigris
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gNnSc1	jejíž
jádro	jádro	k1gNnSc1	jádro
tvoří	tvořit	k5eAaImIp3nS	tvořit
povodí	povodí	k1gNnSc4	povodí
středního	střední	k2eAgInSc2d1	střední
a	a	k8xC	a
dolního	dolní	k2eAgInSc2d1	dolní
toku	tok	k1gInSc2	tok
obou	dva	k4xCgFnPc2	dva
řek	řeka	k1gFnPc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Mezopotámie	Mezopotámie	k1gFnSc1	Mezopotámie
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
tzv.	tzv.	kA	tzv.
úrodného	úrodný	k2eAgInSc2d1	úrodný
půlměsíce	půlměsíc	k1gInSc2	půlměsíc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
její	její	k3xOp3gNnSc1	její
území	území	k1gNnSc1	území
zhruba	zhruba	k6eAd1	zhruba
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
Iráku	Irák	k1gInSc2	Irák
<g/>
,	,	kIx,	,
severovýchodní	severovýchodní	k2eAgFnSc4d1	severovýchodní
Sýrii	Sýrie	k1gFnSc4	Sýrie
<g/>
,	,	kIx,	,
jihovýchodnímu	jihovýchodní	k2eAgNnSc3d1	jihovýchodní
Turecku	Turecko	k1gNnSc3	Turecko
a	a	k8xC	a
jihozápadnímu	jihozápadní	k2eAgInSc3d1	jihozápadní
Íránu	Írán	k1gInSc3	Írán
<g/>
.	.	kIx.	.
</s>
<s>
Mezopotámie	Mezopotámie	k1gFnSc1	Mezopotámie
byla	být	k5eAaImAgFnS	být
kolébkou	kolébka	k1gFnSc7	kolébka
civilizace	civilizace	k1gFnSc2	civilizace
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
sumerské	sumerský	k2eAgNnSc1d1	sumerské
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
se	se	k3xPyFc4	se
v	v	k7c6	v
Mezopotámii	Mezopotámie	k1gFnSc6	Mezopotámie
objevilo	objevit	k5eAaPmAgNnS	objevit
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
4	[number]	k4	4
<g/>
.	.	kIx.	.
a	a	k8xC	a
3	[number]	k4	3
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
byla	být	k5eAaImAgNnP	být
rozdělena	rozdělit	k5eAaPmNgNnP	rozdělit
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
Asýrii	Asýrie	k1gFnSc6	Asýrie
a	a	k8xC	a
jižní	jižní	k2eAgFnSc6d1	jižní
Babylonii	Babylonie	k1gFnSc6	Babylonie
<g/>
.	.	kIx.	.
</s>
<s>
Horní	horní	k2eAgFnSc1d1	horní
část	část	k1gFnSc1	část
Babylonie	Babylonie	k1gFnSc1	Babylonie
se	se	k3xPyFc4	se
nazývala	nazývat	k5eAaImAgFnS	nazývat
Akkad	Akkad	k1gInSc4	Akkad
a	a	k8xC	a
dolní	dolní	k2eAgInSc4d1	dolní
Sumer	Sumer	k1gInSc4	Sumer
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
539	[number]	k4	539
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
do	do	k7c2	do
Babylónu	babylón	k1gInSc2	babylón
vnikli	vniknout	k5eAaPmAgMnP	vniknout
Peršané	Peršan	k1gMnPc1	Peršan
z	z	k7c2	z
dynastie	dynastie	k1gFnSc2	dynastie
Achaimenovců	Achaimenovec	k1gMnPc2	Achaimenovec
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
331	[number]	k4	331
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
pak	pak	k6eAd1	pak
Alexandr	Alexandr	k1gMnSc1	Alexandr
Veliký	veliký	k2eAgMnSc1d1	veliký
<g/>
,	,	kIx,	,
po	po	k7c6	po
jehož	jehož	k3xOyRp3gFnSc6	jehož
smrti	smrt	k1gFnSc6	smrt
se	se	k3xPyFc4	se
Babylón	Babylón	k1gInSc1	Babylón
stal	stát	k5eAaPmAgInS	stát
součástí	součást	k1gFnSc7	součást
Seleukovské	Seleukovský	k2eAgFnSc2d1	Seleukovská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
150	[number]	k4	150
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
Mezopotámie	Mezopotámie	k1gFnSc1	Mezopotámie
zmocnili	zmocnit	k5eAaPmAgMnP	zmocnit
Parthové	Parth	k1gMnPc1	Parth
<g/>
,	,	kIx,	,
ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
našeho	náš	k3xOp1gInSc2	náš
letopočtu	letopočet	k1gInSc2	letopočet
opět	opět	k6eAd1	opět
Peršané	Peršan	k1gMnPc1	Peršan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
637	[number]	k4	637
se	se	k3xPyFc4	se
vlády	vláda	k1gFnSc2	vláda
nad	nad	k7c7	nad
Mezopotámií	Mezopotámie	k1gFnSc7	Mezopotámie
ujali	ujmout	k5eAaPmAgMnP	ujmout
muslimští	muslimský	k2eAgMnPc1d1	muslimský
Arabové	Arab	k1gMnPc1	Arab
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
Mezopotámii	Mezopotámie	k1gFnSc3	Mezopotámie
říkalo	říkat	k5eAaImAgNnS	říkat
El	Ela	k1gFnPc2	Ela
Irák	Irák	k1gInSc1	Irák
el	ela	k1gFnPc2	ela
<g/>
'	'	kIx"	'
<g/>
Arabi	Arab	k1gMnPc1	Arab
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
ji	on	k3xPp3gFnSc4	on
Arabové	Arab	k1gMnPc1	Arab
nazvali	nazvat	k5eAaPmAgMnP	nazvat
<g/>
.	.	kIx.	.
</s>
<s>
Studiem	studio	k1gNnSc7	studio
starověké	starověký	k2eAgFnSc2d1	starověká
Mezopotámie	Mezopotámie	k1gFnSc2	Mezopotámie
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
asyriologie	asyriologie	k1gFnSc1	asyriologie
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
první	první	k4xOgMnPc1	první
archeologové	archeolog	k1gMnPc1	archeolog
se	se	k3xPyFc4	se
zajímali	zajímat	k5eAaImAgMnP	zajímat
o	o	k7c6	o
Asýrii	Asýrie	k1gFnSc6	Asýrie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byly	být	k5eAaImAgInP	být
zkoumané	zkoumaný	k2eAgInPc1d1	zkoumaný
materiály	materiál	k1gInPc1	materiál
dostupnější	dostupný	k2eAgInPc1d2	dostupnější
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějšími	známý	k2eAgInPc7d3	nejznámější
objekty	objekt	k1gInPc7	objekt
pocházejícími	pocházející	k2eAgInPc7d1	pocházející
z	z	k7c2	z
Mezopotámie	Mezopotámie	k1gFnSc2	Mezopotámie
jsou	být	k5eAaImIp3nP	být
Visuté	visutý	k2eAgFnPc4d1	visutá
zahrady	zahrada	k1gFnPc4	zahrada
Semiramidiny	Semiramidin	k2eAgFnPc4d1	Semiramidin
<g/>
,	,	kIx,	,
Chammurapiho	Chammurapi	k1gMnSc2	Chammurapi
zákoník	zákoník	k1gInSc4	zákoník
<g/>
,	,	kIx,	,
Epos	epos	k1gInSc4	epos
o	o	k7c6	o
Gilgamešovi	Gilgameš	k1gMnSc6	Gilgameš
a	a	k8xC	a
Ištařina	Ištařina	k1gFnSc1	Ištařina
brána	brána	k1gFnSc1	brána
<g/>
.	.	kIx.	.
</s>
<s>
Mezopotámie	Mezopotámie	k1gFnSc1	Mezopotámie
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
povodí	povodí	k1gNnSc6	povodí
řek	řeka	k1gFnPc2	řeka
Eufrat	Eufrat	k1gInSc1	Eufrat
a	a	k8xC	a
Tigris	Tigris	k1gInSc1	Tigris
<g/>
.	.	kIx.	.
</s>
<s>
Řeky	řeka	k1gFnPc1	řeka
pramení	pramenit	k5eAaImIp3nP	pramenit
v	v	k7c6	v
Arménské	arménský	k2eAgFnSc6d1	arménská
vysočině	vysočina	k1gFnSc6	vysočina
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Turecka	Turecko	k1gNnSc2	Turecko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
na	na	k7c6	na
jednom	jeden	k4xCgNnSc6	jeden
místě	místo	k1gNnSc6	místo
jejich	jejich	k3xOp3gInSc1	jejich
tok	tok	k1gInSc1	tok
přibližuje	přibližovat	k5eAaImIp3nS	přibližovat
na	na	k7c4	na
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
asi	asi	k9	asi
30	[number]	k4	30
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
Tigris	Tigris	k1gInSc1	Tigris
teče	téct	k5eAaImIp3nS	téct
rychle	rychle	k6eAd1	rychle
na	na	k7c4	na
východ	východ	k1gInSc4	východ
a	a	k8xC	a
pak	pak	k6eAd1	pak
na	na	k7c4	na
jihovýchod	jihovýchod	k1gInSc4	jihovýchod
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Eufrat	Eufrat	k1gInSc1	Eufrat
na	na	k7c4	na
jihozápad	jihozápad	k1gInSc4	jihozápad
a	a	k8xC	a
poté	poté	k6eAd1	poté
na	na	k7c4	na
jih	jih	k1gInSc4	jih
a	a	k8xC	a
jihovýchod	jihovýchod	k1gInSc4	jihovýchod
<g/>
.	.	kIx.	.
</s>
<s>
Tigris	Tigris	k1gInSc1	Tigris
má	mít	k5eAaImIp3nS	mít
více	hodně	k6eAd2	hodně
přítoků	přítok	k1gInPc2	přítok
než	než	k8xS	než
Eufrat	Eufrat	k1gInSc1	Eufrat
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
protéká	protékat	k5eAaImIp3nS	protékat
méně	málo	k6eAd2	málo
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
jehož	jehož	k3xOyRp3gInSc1	jehož
proud	proud	k1gInSc1	proud
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
mírnější	mírný	k2eAgFnSc1d2	mírnější
<g/>
.	.	kIx.	.
</s>
<s>
Podzimní	podzimní	k2eAgInPc1d1	podzimní
deště	dešť	k1gInPc1	dešť
zvedají	zvedat	k5eAaImIp3nP	zvedat
hladiny	hladina	k1gFnPc4	hladina
obou	dva	k4xCgFnPc2	dva
řek	řeka	k1gFnPc2	řeka
a	a	k8xC	a
záplavy	záplava	k1gFnPc1	záplava
vrcholí	vrcholit	k5eAaImIp3nP	vrcholit
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
a	a	k8xC	a
květnu	květen	k1gInSc6	květen
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
tomuto	tento	k3xDgInSc3	tento
přírodnímu	přírodní	k2eAgInSc3d1	přírodní
procesu	proces	k1gInSc3	proces
se	se	k3xPyFc4	se
v	v	k7c6	v
níže	nízce	k6eAd2	nízce
položených	položený	k2eAgFnPc6d1	položená
oblastech	oblast	k1gFnPc6	oblast
podél	podél	k7c2	podél
Eufratu	Eufrat	k1gInSc2	Eufrat
a	a	k8xC	a
Tigridu	Tigris	k1gInSc2	Tigris
nahromadily	nahromadit	k5eAaPmAgInP	nahromadit
nánosy	nános	k1gInPc1	nános
bahna	bahno	k1gNnSc2	bahno
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
kopcích	kopec	k1gInPc6	kopec
se	se	k3xPyFc4	se
vytvořila	vytvořit	k5eAaPmAgNnP	vytvořit
úrodná	úrodný	k2eAgNnPc1d1	úrodné
údolí	údolí	k1gNnPc1	údolí
<g/>
.	.	kIx.	.
</s>
<s>
Východní	východní	k2eAgInSc4d1	východní
okraj	okraj	k1gInSc4	okraj
Mezopotámie	Mezopotámie	k1gFnSc2	Mezopotámie
je	být	k5eAaImIp3nS	být
ostře	ostro	k6eAd1	ostro
ohraničen	ohraničen	k2eAgInSc1d1	ohraničen
pohořím	pohořet	k5eAaPmIp1nS	pohořet
Zagros	Zagrosa	k1gFnPc2	Zagrosa
<g/>
,	,	kIx,	,
součástí	součást	k1gFnSc7	součást
Íránské	íránský	k2eAgFnSc2d1	íránská
vysočiny	vysočina	k1gFnSc2	vysočina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
je	být	k5eAaImIp3nS	být
ohraničena	ohraničit	k5eAaPmNgFnS	ohraničit
Arménskou	arménský	k2eAgFnSc7d1	arménská
vysočinou	vysočina	k1gFnSc7	vysočina
<g/>
,	,	kIx,	,
na	na	k7c6	na
západě	západ	k1gInSc6	západ
pouštěmi	poušť	k1gFnPc7	poušť
Arabské	arabský	k2eAgFnSc2d1	arabská
platformy	platforma	k1gFnSc2	platforma
a	a	k8xC	a
na	na	k7c6	na
jihu	jih	k1gInSc3	jih
Perským	perský	k2eAgInSc7d1	perský
zálivem	záliv	k1gInSc7	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Mezopotámii	Mezopotámie	k1gFnSc4	Mezopotámie
tradičně	tradičně	k6eAd1	tradičně
dělíme	dělit	k5eAaImIp1nP	dělit
na	na	k7c6	na
Horní	horní	k2eAgFnSc6d1	horní
a	a	k8xC	a
Dolní	dolní	k2eAgFnSc6d1	dolní
<g/>
.	.	kIx.	.
</s>
<s>
Dolní	dolní	k2eAgFnSc4d1	dolní
Mezopotámii	Mezopotámie	k1gFnSc4	Mezopotámie
vyplňuje	vyplňovat	k5eAaImIp3nS	vyplňovat
Mezopotamská	mezopotamský	k2eAgFnSc1d1	mezopotamská
nížina	nížina	k1gFnSc1	nížina
<g/>
,	,	kIx,	,
Horní	horní	k2eAgFnSc1d1	horní
Mezopotámie	Mezopotámie	k1gFnSc1	Mezopotámie
je	být	k5eAaImIp3nS	být
také	také	k9	také
známa	znám	k2eAgFnSc1d1	známa
pod	pod	k7c7	pod
arabským	arabský	k2eAgInSc7d1	arabský
názvem	název	k1gInSc7	název
Džazíra	Džazír	k1gMnSc2	Džazír
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
ostrov	ostrov	k1gInSc4	ostrov
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc4	dějiny
Mezopotámie	Mezopotámie	k1gFnSc2	Mezopotámie
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgNnSc1d3	nejstarší
známé	známý	k2eAgNnSc1d1	známé
vesnické	vesnický	k2eAgNnSc1d1	vesnické
osídlení	osídlení	k1gNnSc1	osídlení
v	v	k7c6	v
Mezopotámii	Mezopotámie	k1gFnSc6	Mezopotámie
se	se	k3xPyFc4	se
datuje	datovat	k5eAaImIp3nS	datovat
do	do	k7c2	do
6	[number]	k4	6
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
</s>
<s>
První	první	k4xOgInSc4	první
byla	být	k5eAaImAgFnS	být
osídlena	osídlen	k2eAgFnSc1d1	osídlena
severní	severní	k2eAgFnSc1d1	severní
Mezopotámie	Mezopotámie	k1gFnSc1	Mezopotámie
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
lidé	člověk	k1gMnPc1	člověk
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
nedokázali	dokázat	k5eNaPmAgMnP	dokázat
bránit	bránit	k5eAaImF	bránit
záplavám	záplava	k1gFnPc3	záplava
Eufratu	Eufrat	k1gInSc2	Eufrat
a	a	k8xC	a
Tigridu	Tigris	k1gInSc2	Tigris
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
Eufratu	Eufrat	k1gInSc6	Eufrat
bylo	být	k5eAaImAgNnS	být
více	hodně	k6eAd2	hodně
měst	město	k1gNnPc2	město
než	než	k8xS	než
při	při	k7c6	při
Tigridu	Tigris	k1gInSc6	Tigris
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
podmínky	podmínka	k1gFnPc4	podmínka
osídlení	osídlení	k1gNnPc2	osídlení
nebyly	být	k5eNaImAgFnP	být
tak	tak	k6eAd1	tak
příznivé	příznivý	k2eAgFnPc1d1	příznivá
<g/>
.	.	kIx.	.
</s>
<s>
Zakládání	zakládání	k1gNnSc1	zakládání
a	a	k8xC	a
rozvoj	rozvoj	k1gInSc4	rozvoj
sídlišť	sídliště	k1gNnPc2	sídliště
vzdálených	vzdálený	k2eAgNnPc2d1	vzdálené
od	od	k7c2	od
hlavních	hlavní	k2eAgInPc2d1	hlavní
toků	tok	k1gInPc2	tok
umožňovala	umožňovat	k5eAaImAgFnS	umožňovat
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
síť	síť	k1gFnSc1	síť
kanálů	kanál	k1gInPc2	kanál
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
měly	mít	k5eAaImAgInP	mít
dva	dva	k4xCgInPc1	dva
účely	účel	k1gInPc1	účel
<g/>
:	:	kIx,	:
zajišťovaly	zajišťovat	k5eAaImAgInP	zajišťovat
vodu	voda	k1gFnSc4	voda
odlehlým	odlehlý	k2eAgFnPc3d1	odlehlá
zemědělským	zemědělský	k2eAgFnPc3d1	zemědělská
půdám	půda	k1gFnPc3	půda
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
odváděly	odvádět	k5eAaImAgFnP	odvádět
vodu	voda	k1gFnSc4	voda
z	z	k7c2	z
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jí	on	k3xPp3gFnSc3	on
byl	být	k5eAaImAgInS	být
přebytek	přebytek	k1gInSc1	přebytek
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
veletoků	veletok	k1gInPc2	veletok
při	při	k7c6	při
jarních	jarní	k2eAgFnPc6d1	jarní
záplavách	záplava	k1gFnPc6	záplava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
sumerské	sumerský	k2eAgNnSc1d1	sumerské
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
se	se	k3xPyFc4	se
v	v	k7c6	v
Mezopotámii	Mezopotámie	k1gFnSc6	Mezopotámie
objevilo	objevit	k5eAaPmAgNnS	objevit
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
4	[number]	k4	4
<g/>
.	.	kIx.	.
a	a	k8xC	a
3	[number]	k4	3
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1630	[number]	k4	1630
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
Babylonie	Babylonie	k1gFnSc1	Babylonie
zmocnili	zmocnit	k5eAaPmAgMnP	zmocnit
Chetité	Chetita	k1gMnPc1	Chetita
a	a	k8xC	a
asi	asi	k9	asi
o	o	k7c4	o
sto	sto	k4xCgNnSc4	sto
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
pak	pak	k6eAd1	pak
Kassité	Kassita	k1gMnPc1	Kassita
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
jí	on	k3xPp3gFnSc3	on
vládli	vládnout	k5eAaImAgMnP	vládnout
téměř	téměř	k6eAd1	téměř
pět	pět	k4xCc4	pět
století	století	k1gNnPc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
osudu	osud	k1gInSc2	osud
Asýrie	Asýrie	k1gFnSc2	Asýrie
od	od	k7c2	od
konce	konec	k1gInSc2	konec
2	[number]	k4	2
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
začali	začít	k5eAaPmAgMnP	začít
zasahovat	zasahovat	k5eAaImF	zasahovat
Aramejci	Aramejec	k1gMnPc1	Aramejec
a	a	k8xC	a
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
1	[number]	k4	1
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
lze	lze	k6eAd1	lze
již	již	k6eAd1	již
mluvit	mluvit	k5eAaImF	mluvit
o	o	k7c4	o
aramejizaci	aramejizace	k1gFnSc4	aramejizace
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
aramejština	aramejština	k1gFnSc1	aramejština
nakonec	nakonec	k6eAd1	nakonec
vytlačila	vytlačit	k5eAaPmAgFnS	vytlačit
akkadštinu	akkadština	k1gFnSc4	akkadština
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
7	[number]	k4	7
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
pány	pan	k1gMnPc7	pan
Mezopotámie	Mezopotámie	k1gFnPc1	Mezopotámie
stali	stát	k5eAaPmAgMnP	stát
Médové	Méd	k1gMnPc1	Méd
<g/>
.	.	kIx.	.
</s>
<s>
Babylón	Babylón	k1gInSc1	Babylón
poté	poté	k6eAd1	poté
zažíval	zažívat	k5eAaImAgInS	zažívat
krátké	krátký	k2eAgNnSc4d1	krátké
období	období	k1gNnSc4	období
renesance	renesance	k1gFnSc2	renesance
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
o	o	k7c4	o
století	století	k1gNnSc4	století
později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
539	[number]	k4	539
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
přerušil	přerušit	k5eAaPmAgMnS	přerušit
příchod	příchod	k1gInSc4	příchod
Peršanů	Peršan	k1gMnPc2	Peršan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
331	[number]	k4	331
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
dobyl	dobýt	k5eAaPmAgMnS	dobýt
Babylonii	Babylonie	k1gFnSc4	Babylonie
Alexandr	Alexandr	k1gMnSc1	Alexandr
Veliký	veliký	k2eAgMnSc1d1	veliký
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
Seleukovské	Seleukovský	k2eAgFnSc2d1	Seleukovská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
150	[number]	k4	150
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
území	území	k1gNnSc2	území
zmocnili	zmocnit	k5eAaPmAgMnP	zmocnit
Parthové	Parth	k1gMnPc1	Parth
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
půl	půl	k1xP	půl
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
podlehli	podlehnout	k5eAaPmAgMnP	podlehnout
Parthové	Parthové	k2eAgMnPc3d1	Parthové
Peršanům	Peršan	k1gMnPc3	Peršan
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
Babylonii	Babylonie	k1gFnSc4	Babylonie
připojili	připojit	k5eAaPmAgMnP	připojit
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
říši	říš	k1gFnSc3	říš
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
637	[number]	k4	637
se	se	k3xPyFc4	se
vlády	vláda	k1gFnSc2	vláda
nad	nad	k7c7	nad
Mezopotámií	Mezopotámie	k1gFnSc7	Mezopotámie
ujali	ujmout	k5eAaPmAgMnP	ujmout
muslimští	muslimský	k2eAgMnPc1d1	muslimský
Arabové	Arab	k1gMnPc1	Arab
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
762	[number]	k4	762
vybudovali	vybudovat	k5eAaPmAgMnP	vybudovat
novou	nový	k2eAgFnSc4d1	nová
metropoli	metropole	k1gFnSc4	metropole
Bagdád	Bagdád	k1gInSc1	Bagdád
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
Mezopotámii	Mezopotámie	k1gFnSc3	Mezopotámie
říkalo	říkat	k5eAaImAgNnS	říkat
El	Ela	k1gFnPc2	Ela
Irák	Irák	k1gInSc1	Irák
el	ela	k1gFnPc2	ela
<g/>
'	'	kIx"	'
<g/>
Arabi	Arab	k1gMnPc1	Arab
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
ji	on	k3xPp3gFnSc4	on
Arabové	Arab	k1gMnPc1	Arab
nazvali	nazvat	k5eAaPmAgMnP	nazvat
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgNnPc4d3	nejvýznamnější
města	město	k1gNnPc4	město
Mezopotámie	Mezopotámie	k1gFnPc1	Mezopotámie
patřily	patřit	k5eAaImAgFnP	patřit
Ur	Ur	k1gInSc4	Ur
<g/>
,	,	kIx,	,
Uruk	Uruk	k1gInSc1	Uruk
<g/>
,	,	kIx,	,
Babylón	Babylón	k1gInSc1	Babylón
<g/>
,	,	kIx,	,
Nippur	Nippur	k1gMnSc1	Nippur
<g/>
,	,	kIx,	,
Aššúr	Aššúr	k1gMnSc1	Aššúr
<g/>
,	,	kIx,	,
Ninive	Ninive	k1gNnSc1	Ninive
<g/>
,	,	kIx,	,
Kiš	kiš	k0	kiš
<g/>
,	,	kIx,	,
Akkad	Akkad	k1gInSc4	Akkad
<g/>
,	,	kIx,	,
Lagaš	Lagaš	k1gInSc4	Lagaš
a	a	k8xC	a
Eridu	Eris	k1gFnSc4	Eris
<g/>
.	.	kIx.	.
</s>
<s>
Mladší	mladý	k2eAgFnSc1d2	mladší
doba	doba	k1gFnSc1	doba
kamenná	kamenný	k2eAgFnSc1d1	kamenná
(	(	kIx(	(
<g/>
první	první	k4xOgNnPc4	první
osídlení	osídlení	k1gNnPc4	osídlení
<g/>
)	)	kIx)	)
Doba	doba	k1gFnSc1	doba
měděná	měděný	k2eAgFnSc1d1	měděná
obeidská	obeidský	k2eAgFnSc1d1	obeidský
kultura	kultura	k1gFnSc1	kultura
(	(	kIx(	(
<g/>
vznik	vznik	k1gInSc1	vznik
prvních	první	k4xOgNnPc2	první
měst	město	k1gNnPc2	město
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
urucká	urucký	k2eAgFnSc1d1	urucká
kultura	kultura	k1gFnSc1	kultura
(	(	kIx(	(
<g/>
vznik	vznik	k1gInSc1	vznik
písma	písmo	k1gNnSc2	písmo
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g />
.	.	kIx.	.
</s>
<s>
<g/>
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
<g/>
)	)	kIx)	)
gaurská	gaurský	k2eAgFnSc1d1	gaurský
kultura	kultura	k1gFnSc1	kultura
džemdet-nasrská	džemdetasrský	k2eAgFnSc1d1	džemdet-nasrský
kultura	kultura	k1gFnSc1	kultura
(	(	kIx(	(
<g/>
cca	cca	kA	cca
5	[number]	k4	5
000	[number]	k4	000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
Raná	raný	k2eAgFnSc1d1	raná
doba	doba	k1gFnSc1	doba
bronzová	bronzový	k2eAgFnSc1d1	bronzová
raně	raně	k6eAd1	raně
dynastické	dynastický	k2eAgNnSc4d1	dynastické
období	období	k1gNnSc4	období
sumerských	sumerský	k2eAgInPc2d1	sumerský
městských	městský	k2eAgInPc2d1	městský
států	stát	k1gInPc2	stát
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
polovina	polovina	k1gFnSc1	polovina
3	[number]	k4	3
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
<g/>
)	)	kIx)	)
Akkadská	Akkadský	k2eAgFnSc1d1	Akkadská
říše	říše	k1gFnSc1	říše
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
cca	cca	kA	cca
2350	[number]	k4	2350
<g/>
–	–	k?	–
<g/>
2193	[number]	k4	2193
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
Třetí	třetí	k4xOgFnSc1	třetí
dynastie	dynastie	k1gFnSc1	dynastie
z	z	k7c2	z
Uru	Ur	k1gInSc2	Ur
(	(	kIx(	(
<g/>
Sumerská	sumerský	k2eAgFnSc1d1	sumerská
renesance	renesance	k1gFnSc1	renesance
–	–	k?	–
cca	cca	kA	cca
2119	[number]	k4	2119
<g/>
–	–	k?	–
<g/>
2004	[number]	k4	2004
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
Střední	střední	k2eAgFnSc1d1	střední
doba	doba	k1gFnSc1	doba
bronzová	bronzový	k2eAgFnSc1d1	bronzová
Staroasyrské	Staroasyrský	k2eAgNnSc4d1	Staroasyrský
období	období	k1gNnSc4	období
(	(	kIx(	(
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
<g />
.	.	kIx.	.
</s>
<s>
18	[number]	k4	18
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
Starobabylonské	Starobabylonský	k2eAgNnSc1d1	Starobabylonský
období	období	k1gNnSc1	období
(	(	kIx(	(
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
Pozdní	pozdní	k2eAgFnSc1d1	pozdní
doba	doba	k1gFnSc1	doba
bronzová	bronzový	k2eAgFnSc1d1	bronzová
Kassitská	Kassitský	k2eAgFnSc1d1	Kassitský
dynastie	dynastie	k1gFnSc1	dynastie
<g/>
,	,	kIx,	,
Středoasyrské	Středoasyrský	k2eAgNnSc1d1	Středoasyrský
období	období	k1gNnSc1	období
(	(	kIx(	(
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
12	[number]	k4	12
<g />
.	.	kIx.	.
</s>
<s>
<g/>
stol.	stol.	k?	stol.
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
Doba	doba	k1gFnSc1	doba
železná	železný	k2eAgFnSc1d1	železná
Novochetitská	Novochetitský	k2eAgFnSc1d1	Novochetitský
říše	říše	k1gFnSc1	říše
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
Novoasyrská	Novoasyrský	k2eAgFnSc1d1	Novoasyrská
říše	říše	k1gFnSc1	říše
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
př	př	kA	př
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
Novobabylonská	Novobabylonský	k2eAgFnSc1d1	Novobabylonská
říše	říše	k1gFnSc1	říše
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
Klasický	klasický	k2eAgInSc1d1	klasický
starověk	starověk	k1gInSc1	starověk
Achaimenovská	Achaimenovský	k2eAgFnSc1d1	Achaimenovský
říše	říše	k1gFnSc1	říše
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
Seleukovská	Seleukovský	k2eAgFnSc1d1	Seleukovská
říše	říše	k1gFnSc1	říše
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
Parthská	Parthský	k2eAgFnSc1d1	Parthský
říše	říše	k1gFnSc1	říše
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
–	–	k?	–
3	[number]	k4	3
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
Sasánovská	Sasánovský	k2eAgFnSc1d1	Sasánovská
říše	říše	k1gFnSc1	říše
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
Islámské	islámský	k2eAgNnSc4d1	islámské
dobytí	dobytí	k1gNnSc4	dobytí
Mezopotámie	Mezopotámie	k1gFnSc2	Mezopotámie
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
Naplavenina	naplavenina	k1gFnSc1	naplavenina
složená	složený	k2eAgFnSc1d1	složená
z	z	k7c2	z
hlíny	hlína	k1gFnSc2	hlína
a	a	k8xC	a
jílu	jíl	k1gInSc2	jíl
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
přinášely	přinášet	k5eAaImAgFnP	přinášet
záplavy	záplava	k1gFnPc4	záplava
z	z	k7c2	z
Eufratu	Eufrat	k1gInSc2	Eufrat
a	a	k8xC	a
Tigridu	Tigris	k1gInSc2	Tigris
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
v	v	k7c6	v
Mezopotámii	Mezopotámie	k1gFnSc6	Mezopotámie
stala	stát	k5eAaPmAgFnS	stát
hlavní	hlavní	k2eAgFnSc7d1	hlavní
stavební	stavební	k2eAgFnSc7d1	stavební
surovinou	surovina	k1gFnSc7	surovina
<g/>
,	,	kIx,	,
z	z	k7c2	z
které	který	k3yQgFnSc2	který
se	se	k3xPyFc4	se
vytvářely	vytvářet	k5eAaImAgInP	vytvářet
např.	např.	kA	např.
cihly	cihla	k1gFnPc1	cihla
<g/>
,	,	kIx,	,
nádobí	nádobí	k1gNnSc1	nádobí
<g/>
,	,	kIx,	,
náčiní	náčiní	k1gNnSc1	náčiní
a	a	k8xC	a
sudy	sud	k1gInPc1	sud
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
hliněné	hliněný	k2eAgFnSc2d1	hliněná
tabulky	tabulka	k1gFnSc2	tabulka
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
písemné	písemný	k2eAgInPc1d1	písemný
doklady	doklad	k1gInPc1	doklad
(	(	kIx(	(
<g/>
psané	psaný	k2eAgFnPc1d1	psaná
sumersky	sumersky	k6eAd1	sumersky
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
napsané	napsaný	k2eAgInPc1d1	napsaný
právě	právě	k9	právě
na	na	k7c4	na
nich	on	k3xPp3gFnPc2	on
a	a	k8xC	a
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
3	[number]	k4	3
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgMnSc1d1	poslední
pak	pak	k6eAd1	pak
z	z	k7c2	z
konce	konec	k1gInSc2	konec
posledního	poslední	k2eAgNnSc2d1	poslední
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Hliněné	hliněný	k2eAgFnSc2d1	hliněná
tabulky	tabulka	k1gFnSc2	tabulka
byly	být	k5eAaImAgInP	být
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
zdaleka	zdaleka	k6eAd1	zdaleka
nejrozšířenějším	rozšířený	k2eAgInSc7d3	nejrozšířenější
psacím	psací	k2eAgInSc7d1	psací
materiálem	materiál	k1gInSc7	materiál
<g/>
,	,	kIx,	,
nejdůležitější	důležitý	k2eAgInPc1d3	nejdůležitější
nápisy	nápis	k1gInPc1	nápis
byly	být	k5eAaImAgInP	být
však	však	k9	však
tesány	tesat	k5eAaImNgInP	tesat
do	do	k7c2	do
kamene	kámen	k1gInSc2	kámen
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
příchodem	příchod	k1gInSc7	příchod
Aramejců	Aramejec	k1gMnPc2	Aramejec
začaly	začít	k5eAaPmAgInP	začít
ustupovat	ustupovat	k5eAaImF	ustupovat
jimi	on	k3xPp3gMnPc7	on
používanému	používaný	k2eAgInSc3d1	používaný
pergamenu	pergamen	k1gInSc3	pergamen
a	a	k8xC	a
papyru	papyr	k1gInSc3	papyr
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
lépe	dobře	k6eAd2	dobře
hodily	hodit	k5eAaImAgFnP	hodit
k	k	k7c3	k
zachycení	zachycení	k1gNnSc3	zachycení
aramejského	aramejský	k2eAgNnSc2d1	aramejské
hláskového	hláskový	k2eAgNnSc2d1	hláskové
písma	písmo	k1gNnSc2	písmo
<g/>
.	.	kIx.	.
</s>
<s>
Záznam	záznam	k1gInSc1	záznam
na	na	k7c4	na
tabulky	tabulka	k1gFnPc4	tabulka
spočíval	spočívat	k5eAaImAgMnS	spočívat
ve	v	k7c6	v
vytlačení	vytlačení	k1gNnSc6	vytlačení
textu	text	k1gInSc2	text
do	do	k7c2	do
tvárného	tvárný	k2eAgInSc2d1	tvárný
vlhkého	vlhký	k2eAgInSc2d1	vlhký
povrchu	povrch	k1gInSc2	povrch
a	a	k8xC	a
následném	následný	k2eAgNnSc6d1	následné
vypálení	vypálení	k1gNnSc6	vypálení
v	v	k7c6	v
ohni	oheň	k1gInSc6	oheň
nebo	nebo	k8xC	nebo
usušení	usušení	k1gNnSc1	usušení
na	na	k7c6	na
slunci	slunce	k1gNnSc6	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Pisátko	pisátko	k1gNnSc1	pisátko
bylo	být	k5eAaImAgNnS	být
většinou	většinou	k6eAd1	většinou
z	z	k7c2	z
rákosu	rákos	k1gInSc2	rákos
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
někdy	někdy	k6eAd1	někdy
i	i	k9	i
ze	z	k7c2	z
dřeva	dřevo	k1gNnSc2	dřevo
nebo	nebo	k8xC	nebo
jiného	jiný	k2eAgInSc2d1	jiný
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Hliněné	hliněný	k2eAgFnPc1d1	hliněná
tabulky	tabulka	k1gFnPc1	tabulka
dosahovaly	dosahovat	k5eAaImAgFnP	dosahovat
rozměrů	rozměr	k1gInPc2	rozměr
od	od	k7c2	od
1	[number]	k4	1
×	×	k?	×
1	[number]	k4	1
cm	cm	kA	cm
přes	přes	k7c4	přes
běžnější	běžný	k2eAgInPc4d2	běžnější
4	[number]	k4	4
×	×	k?	×
4	[number]	k4	4
cm	cm	kA	cm
až	až	k9	až
po	po	k7c4	po
největší	veliký	k2eAgFnPc4d3	veliký
30	[number]	k4	30
×	×	k?	×
46	[number]	k4	46
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
se	se	k3xPyFc4	se
na	na	k7c4	na
ně	on	k3xPp3gInPc4	on
kreslily	kreslit	k5eAaImAgInP	kreslit
piktogramy	piktogram	k1gInPc4	piktogram
a	a	k8xC	a
poté	poté	k6eAd1	poté
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
klínové	klínový	k2eAgNnSc1d1	klínové
písmo	písmo	k1gNnSc1	písmo
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
vynález	vynález	k1gInSc1	vynález
se	se	k3xPyFc4	se
přisuzuje	přisuzovat	k5eAaImIp3nS	přisuzovat
Sumerům	Sumer	k1gMnPc3	Sumer
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
znaky	znak	k1gInPc7	znak
psaly	psát	k5eAaImAgInP	psát
ve	v	k7c6	v
sloupcích	sloupec	k1gInPc6	sloupec
shora	shora	k6eAd1	shora
dolů	dolů	k6eAd1	dolů
a	a	k8xC	a
zprava	zprava	k6eAd1	zprava
doleva	doleva	k6eAd1	doleva
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
ale	ale	k8xC	ale
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
otočení	otočení	k1gNnSc3	otočení
o	o	k7c4	o
90	[number]	k4	90
<g/>
°	°	k?	°
doleva	doleva	k6eAd1	doleva
a	a	k8xC	a
psalo	psát	k5eAaImAgNnS	psát
se	se	k3xPyFc4	se
zleva	zleva	k6eAd1	zleva
doprava	doprava	k6eAd1	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Klínovým	klínový	k2eAgNnSc7d1	klínové
písmem	písmo	k1gNnSc7	písmo
se	se	k3xPyFc4	se
zapisovalo	zapisovat	k5eAaImAgNnS	zapisovat
několik	několik	k4yIc1	několik
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
např.	např.	kA	např.
perština	perština	k1gFnSc1	perština
<g/>
,	,	kIx,	,
babylonština	babylonština	k1gFnSc1	babylonština
a	a	k8xC	a
asyrština	asyrština	k1gFnSc1	asyrština
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
souhrnně	souhrnně	k6eAd1	souhrnně
akkadština	akkadština	k1gFnSc1	akkadština
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
úspěšných	úspěšný	k2eAgMnPc2d1	úspěšný
badatelů	badatel	k1gMnPc2	badatel
snažících	snažící	k2eAgMnPc2d1	snažící
se	se	k3xPyFc4	se
rozluštit	rozluštit	k5eAaPmF	rozluštit
klínové	klínový	k2eAgNnSc4d1	klínové
písmo	písmo	k1gNnSc4	písmo
se	se	k3xPyFc4	se
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
stal	stát	k5eAaPmAgMnS	stát
Georg	Georg	k1gMnSc1	Georg
Friedrich	Friedrich	k1gMnSc1	Friedrich
Grotefend	Grotefend	k1gMnSc1	Grotefend
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInPc1d3	nejstarší
klínopisné	klínopisný	k2eAgInPc1d1	klínopisný
záznamy	záznam	k1gInPc1	záznam
tvořily	tvořit	k5eAaImAgInP	tvořit
hospodářské	hospodářský	k2eAgInPc4d1	hospodářský
záznamy	záznam	k1gInPc4	záznam
<g/>
,	,	kIx,	,
účetní	účetní	k2eAgInPc4d1	účetní
výkazy	výkaz	k1gInPc4	výkaz
<g/>
,	,	kIx,	,
inventáře	inventář	k1gInPc4	inventář
majetku	majetek	k1gInSc2	majetek
a	a	k8xC	a
seznamy	seznam	k1gInPc1	seznam
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
nebo	nebo	k8xC	nebo
obětí	oběť	k1gFnPc2	oběť
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgNnSc1d1	původní
nepsané	psaný	k2eNgNnSc1d1	nepsané
právo	právo	k1gNnSc1	právo
bylo	být	k5eAaImAgNnS	být
s	s	k7c7	s
rozvíjející	rozvíjející	k2eAgFnSc7d1	rozvíjející
se	se	k3xPyFc4	se
společností	společnost	k1gFnSc7	společnost
potřeba	potřeba	k6eAd1	potřeba
přeměňovat	přeměňovat	k5eAaImF	přeměňovat
na	na	k7c4	na
právo	právo	k1gNnSc4	právo
psané	psaný	k2eAgNnSc4d1	psané
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc4	první
známé	známý	k2eAgNnSc4d1	známé
normativní	normativní	k2eAgNnSc4d1	normativní
dílo	dílo	k1gNnSc4	dílo
nechal	nechat	k5eAaPmAgMnS	nechat
vydat	vydat	k5eAaPmF	vydat
sumerský	sumerský	k2eAgMnSc1d1	sumerský
vládce	vládce	k1gMnSc1	vládce
Urukagina	Urukagina	k1gMnSc1	Urukagina
ve	v	k7c6	v
24	[number]	k4	24
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
např.	např.	kA	např.
zakázal	zakázat	k5eAaPmAgInS	zakázat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
žena	žena	k1gFnSc1	žena
náležela	náležet	k5eAaImAgFnS	náležet
dvěma	dva	k4xCgMnPc3	dva
mužům	muž	k1gMnPc3	muž
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
zakázal	zakázat	k5eAaPmAgMnS	zakázat
vydírání	vydírání	k1gNnSc4	vydírání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
se	se	k3xPyFc4	se
též	též	k9	též
nacházelo	nacházet	k5eAaImAgNnS	nacházet
heslo	heslo	k1gNnSc1	heslo
"	"	kIx"	"
<g/>
Nechť	nechť	k9	nechť
mocný	mocný	k2eAgMnSc1d1	mocný
neubližuje	ubližovat	k5eNaImIp3nS	ubližovat
vdovám	vdova	k1gFnPc3	vdova
a	a	k8xC	a
sirotkům	sirotek	k1gMnPc3	sirotek
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
později	pozdě	k6eAd2	pozdě
převzalo	převzít	k5eAaPmAgNnS	převzít
několik	několik	k4yIc1	několik
panovníků	panovník	k1gMnPc2	panovník
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
dílo	dílo	k1gNnSc1	dílo
většinou	většinou	k6eAd1	většinou
není	být	k5eNaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
zákoník	zákoník	k1gInSc4	zákoník
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
dnešní	dnešní	k2eAgFnSc2d1	dnešní
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
nedochovalo	dochovat	k5eNaPmAgNnS	dochovat
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
zákoník	zákoník	k1gInSc1	zákoník
v	v	k7c6	v
pravém	pravý	k2eAgInSc6d1	pravý
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc2	smysl
vydal	vydat	k5eAaPmAgMnS	vydat
zakladatel	zakladatel	k1gMnSc1	zakladatel
státu	stát	k1gInSc2	stát
3	[number]	k4	3
<g/>
.	.	kIx.	.
dynastie	dynastie	k1gFnSc2	dynastie
urské	urský	k2eAgFnSc2d1	urská
Ur-nammu	Uramma	k1gFnSc4	Ur-namma
ve	v	k7c6	v
21	[number]	k4	21
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Zachoval	Zachoval	k1gMnSc1	Zachoval
se	se	k3xPyFc4	se
na	na	k7c6	na
tabulce	tabulka	k1gFnSc6	tabulka
o	o	k7c6	o
rozměrech	rozměr	k1gInPc6	rozměr
20	[number]	k4	20
×	×	k?	×
10	[number]	k4	10
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Vladař	vladař	k1gMnSc1	vladař
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
jako	jako	k9	jako
posel	posel	k1gMnSc1	posel
bohů	bůh	k1gMnPc2	bůh
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnPc1	jejichž
vůli	vůle	k1gFnSc4	vůle
tlumočí	tlumočit	k5eAaImIp3nP	tlumočit
<g/>
.	.	kIx.	.
</s>
<s>
Zákony	zákon	k1gInPc1	zákon
se	se	k3xPyFc4	se
týkaly	týkat	k5eAaImAgInP	týkat
např.	např.	kA	např.
čarodějnictví	čarodějnictví	k1gNnSc2	čarodějnictví
a	a	k8xC	a
trestů	trest	k1gInPc2	trest
za	za	k7c4	za
ublížení	ublížení	k1gNnSc4	ublížení
na	na	k7c6	na
těle	tělo	k1gNnSc6	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
dva	dva	k4xCgInPc1	dva
(	(	kIx(	(
<g/>
zatím	zatím	k6eAd1	zatím
<g/>
)	)	kIx)	)
nejstarší	starý	k2eAgInPc1d3	nejstarší
zákoníky	zákoník	k1gInPc1	zákoník
jsou	být	k5eAaImIp3nP	být
tzv.	tzv.	kA	tzv.
Bilalamův	Bilalamův	k2eAgInSc1d1	Bilalamův
akkadský	akkadský	k2eAgInSc1d1	akkadský
zákoník	zákoník	k1gInSc1	zákoník
a	a	k8xC	a
Lipit-Ištarův	Lipit-Ištarův	k2eAgInSc1d1	Lipit-Ištarův
sumerský	sumerský	k2eAgInSc1d1	sumerský
zákoník	zákoník	k1gInSc1	zákoník
<g/>
.	.	kIx.	.
</s>
<s>
Zákony	zákon	k1gInPc1	zákon
mají	mít	k5eAaImIp3nP	mít
charakter	charakter	k1gInSc4	charakter
třídního	třídní	k2eAgNnSc2d1	třídní
zákonodárství	zákonodárství	k1gNnSc2	zákonodárství
<g/>
,	,	kIx,	,
zabývají	zabývat	k5eAaImIp3nP	zabývat
se	se	k3xPyFc4	se
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgInPc4d1	jiný
vztahy	vztah	k1gInPc4	vztah
mezi	mezi	k7c7	mezi
otroky	otrok	k1gMnPc7	otrok
a	a	k8xC	a
plnoprávnými	plnoprávný	k2eAgMnPc7d1	plnoprávný
občany	občan	k1gMnPc7	občan
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
oblastmi	oblast	k1gFnPc7	oblast
<g/>
,	,	kIx,	,
na	na	k7c4	na
něž	jenž	k3xRgMnPc4	jenž
se	se	k3xPyFc4	se
soustředily	soustředit	k5eAaPmAgInP	soustředit
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
manželství	manželství	k1gNnSc4	manželství
<g/>
,	,	kIx,	,
dědické	dědický	k2eAgNnSc4d1	dědické
právo	právo	k1gNnSc4	právo
a	a	k8xC	a
hospodářské	hospodářský	k2eAgInPc4d1	hospodářský
poměry	poměr	k1gInPc4	poměr
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějším	známý	k2eAgInSc7d3	nejznámější
zákonodárným	zákonodárný	k2eAgInSc7d1	zákonodárný
dílem	díl	k1gInSc7	díl
pocházejícím	pocházející	k2eAgInSc7d1	pocházející
z	z	k7c2	z
Mezopotámie	Mezopotámie	k1gFnSc2	Mezopotámie
je	být	k5eAaImIp3nS	být
Chammurapiho	Chammurapi	k1gMnSc4	Chammurapi
zákoník	zákoník	k1gInSc4	zákoník
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
zachoval	zachovat	k5eAaPmAgInS	zachovat
na	na	k7c4	na
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvoumetrové	dvoumetrový	k2eAgFnSc3d1	dvoumetrová
čedičové	čedičový	k2eAgFnSc3d1	čedičová
stéle	stéla	k1gFnSc3	stéla
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
psán	psán	k2eAgInSc1d1	psán
akkadsky	akkadsky	k6eAd1	akkadsky
a	a	k8xC	a
zprava	zprava	k6eAd1	zprava
doleva	doleva	k6eAd1	doleva
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
282	[number]	k4	282
paragrafů	paragraf	k1gInPc2	paragraf
<g/>
.	.	kIx.	.
</s>
<s>
Zákony	zákon	k1gInPc1	zákon
řeší	řešit	k5eAaImIp3nP	řešit
konkrétní	konkrétní	k2eAgInPc1d1	konkrétní
případy	případ	k1gInPc1	případ
<g/>
,	,	kIx,	,
nepřinášejí	přinášet	k5eNaImIp3nP	přinášet
obecné	obecný	k2eAgFnPc1d1	obecná
formulace	formulace	k1gFnPc1	formulace
(	(	kIx(	(
<g/>
nedefinují	definovat	k5eNaBmIp3nP	definovat
např.	např.	kA	např.
krádež	krádež	k1gFnSc4	krádež
<g/>
,	,	kIx,	,
závěť	závěť	k1gFnSc1	závěť
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Sumersko-akkadská	Sumerskokkadský	k2eAgFnSc1d1	Sumersko-akkadský
literatura	literatura	k1gFnSc1	literatura
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnPc1	první
literární	literární	k2eAgNnPc1d1	literární
díla	dílo	k1gNnPc1	dílo
<g/>
,	,	kIx,	,
sumerské	sumerský	k2eAgFnPc1d1	sumerská
hymny	hymna	k1gFnPc1	hymna
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
psána	psát	k5eAaImNgFnS	psát
na	na	k7c6	na
konci	konec	k1gInSc6	konec
3	[number]	k4	3
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Ve	v	k7c6	v
větší	veliký	k2eAgFnSc6d2	veliký
míře	míra	k1gFnSc6	míra
se	se	k3xPyFc4	se
sumerská	sumerský	k2eAgFnSc1d1	sumerská
literatura	literatura	k1gFnSc1	literatura
začala	začít	k5eAaPmAgFnS	začít
psát	psát	k5eAaImF	psát
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
v	v	k7c6	v
chrámech	chrám	k1gInPc6	chrám
v	v	k7c6	v
Nippuru	Nippur	k1gInSc6	Nippur
<g/>
,	,	kIx,	,
Uru	Ur	k1gInSc6	Ur
<g/>
,	,	kIx,	,
Uruku	Uruk	k1gInSc6	Uruk
a	a	k8xC	a
Kiši	Kiš	k1gInSc6	Kiš
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
také	také	k9	také
začínala	začínat	k5eAaImAgFnS	začínat
objevovat	objevovat	k5eAaImF	objevovat
akkadsky	akkadsky	k6eAd1	akkadsky
psaná	psaný	k2eAgFnSc1d1	psaná
literatura	literatura	k1gFnSc1	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Charakteristickým	charakteristický	k2eAgInSc7d1	charakteristický
znakem	znak	k1gInSc7	znak
mezopotámské	mezopotámský	k2eAgFnSc2d1	mezopotámská
literatury	literatura	k1gFnSc2	literatura
byla	být	k5eAaImAgFnS	být
anonymita	anonymita	k1gFnSc1	anonymita
autorů	autor	k1gMnPc2	autor
<g/>
.	.	kIx.	.
</s>
<s>
Přední	přední	k2eAgNnSc4d1	přední
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
nejstarší	starý	k2eAgFnSc6d3	nejstarší
literatuře	literatura	k1gFnSc6	literatura
zaujímaly	zaujímat	k5eAaImAgFnP	zaujímat
hymny	hymna	k1gFnPc1	hymna
na	na	k7c4	na
bohy	bůh	k1gMnPc4	bůh
a	a	k8xC	a
světské	světský	k2eAgMnPc4d1	světský
vladaře	vladař	k1gMnPc4	vladař
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
byly	být	k5eAaImAgInP	být
zpravidla	zpravidla	k6eAd1	zpravidla
určeny	určit	k5eAaPmNgInP	určit
k	k	k7c3	k
zhudebnění	zhudebnění	k1gNnSc3	zhudebnění
a	a	k8xC	a
přednesu	přednést	k5eAaPmIp1nS	přednést
při	při	k7c6	při
slavnostech	slavnost	k1gFnPc6	slavnost
<g/>
.	.	kIx.	.
</s>
<s>
Hymny	hymnus	k1gInPc1	hymnus
na	na	k7c4	na
bohy	bůh	k1gMnPc4	bůh
byly	být	k5eAaImAgFnP	být
doprovázeny	doprovázet	k5eAaImNgInP	doprovázet
bubny	buben	k1gInPc1	buben
a	a	k8xC	a
hymny	hymnus	k1gInPc1	hymnus
na	na	k7c4	na
vladaře	vladař	k1gMnSc4	vladař
harfou	harfa	k1gFnSc7	harfa
nebo	nebo	k8xC	nebo
lyrou	lyra	k1gFnSc7	lyra
<g/>
.	.	kIx.	.
</s>
<s>
Příbuzným	příbuzný	k2eAgInSc7d1	příbuzný
druhem	druh	k1gInSc7	druh
literární	literární	k2eAgFnSc2d1	literární
tvorby	tvorba	k1gFnSc2	tvorba
byly	být	k5eAaImAgFnP	být
předmluvy	předmluva	k1gFnPc1	předmluva
k	k	k7c3	k
zákoníkům	zákoník	k1gMnPc3	zákoník
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
sloužily	sloužit	k5eAaImAgFnP	sloužit
jako	jako	k8xS	jako
oslava	oslava	k1gFnSc1	oslava
vladaře	vladař	k1gMnSc2	vladař
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgInPc7d1	další
populárními	populární	k2eAgInPc7d1	populární
žánry	žánr	k1gInPc7	žánr
v	v	k7c6	v
Mezopotámii	Mezopotámie	k1gFnSc6	Mezopotámie
byly	být	k5eAaImAgFnP	být
modlitby	modlitba	k1gFnPc1	modlitba
<g/>
,	,	kIx,	,
nářky	nářek	k1gInPc1	nářek
<g/>
,	,	kIx,	,
báje	báj	k1gFnPc1	báj
a	a	k8xC	a
eposy	epos	k1gInPc1	epos
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
bájemi	báj	k1gFnPc7	báj
vynikala	vynikat	k5eAaImAgFnS	vynikat
báj	báj	k1gFnSc1	báj
o	o	k7c6	o
stvoření	stvoření	k1gNnSc6	stvoření
<g/>
.	.	kIx.	.
</s>
<s>
Vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c4	o
stvoření	stvoření	k1gNnSc4	stvoření
bohů	bůh	k1gMnPc2	bůh
<g/>
,	,	kIx,	,
následném	následný	k2eAgNnSc6d1	následné
stvoření	stvoření	k1gNnSc6	stvoření
Země	zem	k1gFnSc2	zem
a	a	k8xC	a
nebe	nebe	k1gNnSc2	nebe
<g/>
,	,	kIx,	,
vesmírných	vesmírný	k2eAgNnPc2d1	vesmírné
těles	těleso	k1gNnPc2	těleso
a	a	k8xC	a
člověka	člověk	k1gMnSc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgInPc4d1	jiný
se	se	k3xPyFc4	se
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
píše	psát	k5eAaImIp3nS	psát
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Člověk	člověk	k1gMnSc1	člověk
byl	být	k5eAaImAgMnS	být
stvořen	stvořit	k5eAaPmNgMnS	stvořit
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
sloužil	sloužit	k5eAaImAgInS	sloužit
bohům	bůh	k1gMnPc3	bůh
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Největší	veliký	k2eAgFnSc4d3	veliký
roli	role	k1gFnSc4	role
v	v	k7c6	v
sumerské	sumerský	k2eAgFnSc6d1	sumerská
verzi	verze	k1gFnSc6	verze
hrál	hrát	k5eAaImAgMnS	hrát
bůh	bůh	k1gMnSc1	bůh
Enlil	Enlil	k1gMnSc1	Enlil
a	a	k8xC	a
v	v	k7c6	v
akkadské	akkadský	k2eAgFnSc6d1	akkadská
verzi	verze	k1gFnSc6	verze
bůh	bůh	k1gMnSc1	bůh
Marduk	Marduk	k1gMnSc1	Marduk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
bájích	báj	k1gFnPc6	báj
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
motivy	motiv	k1gInPc1	motiv
podobné	podobný	k2eAgInPc1d1	podobný
biblickým	biblický	k2eAgInPc3d1	biblický
příběhům	příběh	k1gInPc3	příběh
<g/>
,	,	kIx,	,
např.	např.	kA	např.
stvoření	stvoření	k1gNnSc2	stvoření
člověka	člověk	k1gMnSc2	člověk
z	z	k7c2	z
hlíny	hlína	k1gFnSc2	hlína
<g/>
,	,	kIx,	,
příběh	příběh	k1gInSc4	příběh
podobný	podobný	k2eAgInSc4d1	podobný
tomu	ten	k3xDgNnSc3	ten
o	o	k7c4	o
Kainu	Kain	k1gMnSc3	Kain
a	a	k8xC	a
Ábelovi	Ábel	k1gMnSc3	Ábel
<g/>
,	,	kIx,	,
mýtus	mýtus	k1gInSc1	mýtus
o	o	k7c6	o
ráji	ráj	k1gInSc6	ráj
nebo	nebo	k8xC	nebo
báje	báj	k1gFnSc2	báj
o	o	k7c6	o
potopě	potopa	k1gFnSc6	potopa
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc7d3	veliký
památkou	památka	k1gFnSc7	památka
mezopotámského	mezopotámský	k2eAgNnSc2d1	mezopotámský
písemnictví	písemnictví	k1gNnSc2	písemnictví
je	být	k5eAaImIp3nS	být
Epos	epos	k1gInSc4	epos
o	o	k7c6	o
Gilgamešovi	Gilgameš	k1gMnSc6	Gilgameš
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ale	ale	k8xC	ale
v	v	k7c6	v
samotné	samotný	k2eAgFnSc6d1	samotná
Mezopotámii	Mezopotámie	k1gFnSc6	Mezopotámie
nebyl	být	k5eNaImAgMnS	být
nejspíše	nejspíše	k9	nejspíše
moc	moc	k6eAd1	moc
znám	znám	k2eAgMnSc1d1	znám
a	a	k8xC	a
neměl	mít	k5eNaImAgMnS	mít
veliký	veliký	k2eAgInSc4d1	veliký
význam	význam	k1gInSc4	význam
v	v	k7c6	v
tamější	tamější	k2eAgFnSc6d1	tamější
literární	literární	k2eAgFnSc6d1	literární
tradici	tradice	k1gFnSc6	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
je	být	k5eAaImIp3nS	být
o	o	k7c6	o
něm	on	k3xPp3gNnSc6	on
známo	znám	k2eAgNnSc1d1	známo
šest	šest	k4xCc4	šest
sumerských	sumerský	k2eAgFnPc2d1	sumerská
básní	báseň	k1gFnPc2	báseň
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
pět	pět	k4xCc4	pět
tvoří	tvořit	k5eAaImIp3nP	tvořit
novoasyrský	novoasyrský	k2eAgInSc4d1	novoasyrský
epos	epos	k1gInSc4	epos
<g/>
.	.	kIx.	.
</s>
<s>
Kanonickou	kanonický	k2eAgFnSc4d1	kanonická
akkadskou	akkadský	k2eAgFnSc4d1	akkadská
verzi	verze	k1gFnSc4	verze
tvoří	tvořit	k5eAaImIp3nS	tvořit
12	[number]	k4	12
tabulek	tabulka	k1gFnPc2	tabulka
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
Gilgamešově	Gilgamešův	k2eAgInSc6d1	Gilgamešův
životě	život	k1gInSc6	život
<g/>
,	,	kIx,	,
o	o	k7c6	o
jeho	jeho	k3xOp3gNnPc6	jeho
dobrodružstvích	dobrodružství	k1gNnPc6	dobrodružství
a	a	k8xC	a
neúspěšném	úspěšný	k2eNgNnSc6d1	neúspěšné
hledání	hledání	k1gNnSc6	hledání
nesmrtelnosti	nesmrtelnost	k1gFnSc2	nesmrtelnost
<g/>
.	.	kIx.	.
</s>
<s>
Epos	epos	k1gInSc1	epos
je	být	k5eAaImIp3nS	být
důležitý	důležitý	k2eAgInSc1d1	důležitý
svým	svůj	k3xOyFgInSc7	svůj
rozsahem	rozsah	k1gInSc7	rozsah
<g/>
,	,	kIx,	,
stavem	stav	k1gInSc7	stav
zachování	zachování	k1gNnSc2	zachování
<g/>
,	,	kIx,	,
příběhem	příběh	k1gInSc7	příběh
a	a	k8xC	a
rozvinutou	rozvinutý	k2eAgFnSc7d1	rozvinutá
básnickou	básnický	k2eAgFnSc7d1	básnická
formou	forma	k1gFnSc7	forma
<g/>
.	.	kIx.	.
</s>
<s>
Mezopotámská	Mezopotámský	k2eAgFnSc1d1	Mezopotámská
společnost	společnost	k1gFnSc1	společnost
byla	být	k5eAaImAgFnS	být
patriarchální	patriarchální	k2eAgFnSc1d1	patriarchální
<g/>
,	,	kIx,	,
muži	muž	k1gMnPc1	muž
se	se	k3xPyFc4	se
zabývali	zabývat	k5eAaImAgMnP	zabývat
vyššími	vysoký	k2eAgInPc7d2	vyšší
hospodářskými	hospodářský	k2eAgInPc7d1	hospodářský
úkoly	úkol	k1gInPc7	úkol
(	(	kIx(	(
<g/>
např.	např.	kA	např.
obdělávání	obdělávání	k1gNnSc4	obdělávání
půdy	půda	k1gFnSc2	půda
pluhem	pluh	k1gInSc7	pluh
<g/>
)	)	kIx)	)
a	a	k8xC	a
ženy	žena	k1gFnPc1	žena
pracovaly	pracovat	k5eAaImAgFnP	pracovat
v	v	k7c6	v
domácnosti	domácnost	k1gFnSc6	domácnost
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k7c2	uvnitř
občin	občina	k1gFnPc2	občina
<g/>
,	,	kIx,	,
k	k	k7c3	k
jejichž	jejichž	k3xOyRp3gFnSc3	jejichž
existenci	existence	k1gFnSc3	existence
byla	být	k5eAaImAgFnS	být
zapotřebí	zapotřebí	k6eAd1	zapotřebí
stavba	stavba	k1gFnSc1	stavba
kanálů	kanál	k1gInPc2	kanál
a	a	k8xC	a
vodních	vodní	k2eAgNnPc2d1	vodní
děl	dělo	k1gNnPc2	dělo
a	a	k8xC	a
jejich	jejich	k3xOp3gNnPc4	jejich
udržování	udržování	k1gNnPc4	udržování
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgInP	začít
vytvářet	vytvářet	k5eAaImF	vytvářet
majetkové	majetkový	k2eAgInPc1d1	majetkový
rozdíly	rozdíl	k1gInPc1	rozdíl
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
vedly	vést	k5eAaImAgInP	vést
k	k	k7c3	k
vrstevnímu	vrstevní	k2eAgNnSc3d1	vrstevní
rozdělení	rozdělení	k1gNnSc3	rozdělení
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
rozdíly	rozdíl	k1gInPc1	rozdíl
mezi	mezi	k7c7	mezi
občinami	občina	k1gFnPc7	občina
vyúsťovaly	vyúsťovat	k5eAaImAgFnP	vyúsťovat
v	v	k7c4	v
loupežnické	loupežnický	k2eAgFnPc4d1	loupežnická
výpravy	výprava	k1gFnPc4	výprava
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
dále	daleko	k6eAd2	daleko
prohlubovaly	prohlubovat	k5eAaImAgFnP	prohlubovat
tyto	tento	k3xDgFnPc1	tento
rozpory	rozpora	k1gFnPc1	rozpora
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
často	často	k6eAd1	často
bývalo	bývat	k5eAaImAgNnS	bývat
podrobení	podrobení	k1gNnSc1	podrobení
poražené	poražený	k2eAgFnSc2d1	poražená
občiny	občina	k1gFnSc2	občina
a	a	k8xC	a
její	její	k3xOp3gNnSc1	její
splynutí	splynutí	k1gNnSc1	splynutí
s	s	k7c7	s
občinou	občina	k1gFnSc7	občina
vítěze	vítěz	k1gMnSc2	vítěz
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
vznikaly	vznikat	k5eAaImAgInP	vznikat
první	první	k4xOgFnPc4	první
vládní	vládní	k2eAgFnPc4d1	vládní
jednotky	jednotka	k1gFnPc4	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnPc1	první
představitelé	představitel	k1gMnPc1	představitel
vládní	vládní	k2eAgFnSc2d1	vládní
moci	moc	k1gFnSc2	moc
byli	být	k5eAaImAgMnP	být
zároveň	zároveň	k6eAd1	zároveň
hlavami	hlava	k1gFnPc7	hlava
chrámového	chrámový	k2eAgNnSc2d1	chrámové
kněžstva	kněžstvo	k1gNnSc2	kněžstvo
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
mocenský	mocenský	k2eAgInSc1d1	mocenský
význam	význam	k1gInSc1	význam
spočíval	spočívat	k5eAaImAgInS	spočívat
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
obrovském	obrovský	k2eAgInSc6d1	obrovský
majetku	majetek	k1gInSc6	majetek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
nahromadili	nahromadit	k5eAaPmAgMnP	nahromadit
<g/>
.	.	kIx.	.
</s>
<s>
Chrámy	chrám	k1gInPc1	chrám
vlastnily	vlastnit	k5eAaImAgInP	vlastnit
velká	velký	k2eAgNnPc1d1	velké
území	území	k1gNnPc1	území
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
nich	on	k3xPp3gMnPc2	on
ale	ale	k9	ale
existovala	existovat	k5eAaImAgFnS	existovat
i	i	k9	i
půda	půda	k1gFnSc1	půda
občinová	občinový	k2eAgFnSc1d1	občinová
a	a	k8xC	a
půda	půda	k1gFnSc1	půda
udělovaná	udělovaný	k2eAgFnSc1d1	udělovaná
některým	některý	k3yIgFnPc3	některý
významným	významný	k2eAgFnPc3d1	významná
rodinám	rodina	k1gFnPc3	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Chrámový	chrámový	k2eAgInSc1d1	chrámový
pozemek	pozemek	k1gInSc1	pozemek
byl	být	k5eAaImAgInS	být
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
na	na	k7c4	na
několik	několik	k4yIc4	několik
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
každá	každý	k3xTgFnSc1	každý
plnila	plnit	k5eAaImAgFnS	plnit
jinou	jiný	k2eAgFnSc4d1	jiná
funkci	funkce	k1gFnSc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
správě	správa	k1gFnSc3	správa
nemovitostí	nemovitost	k1gFnPc2	nemovitost
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
rozvětvený	rozvětvený	k2eAgInSc1d1	rozvětvený
správní	správní	k2eAgInSc1d1	správní
aparát	aparát	k1gInSc1	aparát
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
čele	čelo	k1gNnSc6	čelo
byl	být	k5eAaImAgMnS	být
vrchní	vrchní	k2eAgMnSc1d1	vrchní
správce	správce	k1gMnSc1	správce
<g/>
.	.	kIx.	.
</s>
<s>
Postupem	postupem	k7c2	postupem
času	čas	k1gInSc2	čas
někteří	některý	k3yIgMnPc1	některý
chrámoví	chrámový	k2eAgMnPc1d1	chrámový
zaměstnanci	zaměstnanec	k1gMnPc1	zaměstnanec
klesli	klesnout	k5eAaPmAgMnP	klesnout
do	do	k7c2	do
předtím	předtím	k6eAd1	předtím
nepříliš	příliš	k6eNd1	příliš
rozsáhlé	rozsáhlý	k2eAgFnSc2d1	rozsáhlá
třídy	třída	k1gFnSc2	třída
otroků	otrok	k1gMnPc2	otrok
<g/>
.	.	kIx.	.
</s>
<s>
Majetek	majetek	k1gInSc1	majetek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
chrámy	chrám	k1gInPc1	chrám
nashromáždily	nashromáždit	k5eAaPmAgInP	nashromáždit
<g/>
,	,	kIx,	,
jim	on	k3xPp3gMnPc3	on
zajistil	zajistit	k5eAaPmAgInS	zajistit
hospodářskou	hospodářský	k2eAgFnSc4d1	hospodářská
a	a	k8xC	a
politickou	politický	k2eAgFnSc4d1	politická
moc	moc	k1gFnSc4	moc
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
ochraně	ochrana	k1gFnSc3	ochrana
před	před	k7c7	před
sousedními	sousední	k2eAgInPc7d1	sousední
městskými	městský	k2eAgInPc7d1	městský
státy	stát	k1gInPc7	stát
začaly	začít	k5eAaPmAgFnP	začít
okolo	okolo	k7c2	okolo
sebe	se	k3xPyFc2	se
stavět	stavět	k5eAaImF	stavět
hradby	hradba	k1gFnSc2	hradba
a	a	k8xC	a
opevnění	opevnění	k1gNnSc2	opevnění
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
rostoucímu	rostoucí	k2eAgInSc3d1	rostoucí
významu	význam	k1gInSc3	význam
chrámů	chrám	k1gInPc2	chrám
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gMnPc1	jejich
představitelé	představitel	k1gMnPc1	představitel
začali	začít	k5eAaPmAgMnP	začít
(	(	kIx(	(
<g/>
od	od	k7c2	od
dob	doba	k1gFnPc2	doba
Sargona	Sargon	k1gMnSc2	Sargon
Akkadského	Akkadský	k2eAgMnSc2d1	Akkadský
<g/>
)	)	kIx)	)
nazývat	nazývat	k5eAaImF	nazývat
tituly	titul	k1gInPc4	titul
jako	jako	k9	jako
"	"	kIx"	"
<g/>
král	král	k1gMnSc1	král
Sumeru	Sumer	k1gInSc2	Sumer
i	i	k8xC	i
Akkadu	Akkad	k1gInSc2	Akkad
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
král	král	k1gMnSc1	král
čtyř	čtyři	k4xCgFnPc2	čtyři
světových	světový	k2eAgFnPc2d1	světová
stran	strana	k1gFnPc2	strana
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
vyjadřovali	vyjadřovat	k5eAaImAgMnP	vyjadřovat
snahu	snaha	k1gFnSc4	snaha
rozšířit	rozšířit	k5eAaPmF	rozšířit
svou	svůj	k3xOyFgFnSc4	svůj
vládu	vláda	k1gFnSc4	vláda
na	na	k7c4	na
co	co	k3yRnSc4	co
největší	veliký	k2eAgNnSc4d3	veliký
území	území	k1gNnSc4	území
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
se	se	k3xPyFc4	se
nechávali	nechávat	k5eAaImAgMnP	nechávat
ještě	ještě	k9	ještě
za	za	k7c2	za
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
zbožšťovat	zbožšťovat	k5eAaImF	zbožšťovat
<g/>
.	.	kIx.	.
</s>
<s>
Chammurapi	Chammurapi	k1gNnSc1	Chammurapi
(	(	kIx(	(
<g/>
zhruba	zhruba	k6eAd1	zhruba
1800	[number]	k4	1800
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejvýznamnější	významný	k2eAgMnSc1d3	nejvýznamnější
vládce	vládce	k1gMnSc1	vládce
starobabylonského	starobabylonský	k2eAgNnSc2d1	starobabylonský
období	období	k1gNnSc2	období
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
vládl	vládnout	k5eAaImAgInS	vládnout
plně	plně	k6eAd1	plně
centralizované	centralizovaný	k2eAgFnSc3d1	centralizovaná
říši	říš	k1gFnSc3	říš
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
nejvyšším	vysoký	k2eAgMnSc7d3	nejvyšší
správcem	správce	k1gMnSc7	správce
<g/>
,	,	kIx,	,
zákonodárcem	zákonodárce	k1gMnSc7	zákonodárce
<g/>
,	,	kIx,	,
soudcem	soudce	k1gMnSc7	soudce
a	a	k8xC	a
vojenským	vojenský	k2eAgMnSc7d1	vojenský
velitelem	velitel	k1gMnSc7	velitel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
tradici	tradice	k1gFnSc6	tradice
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
další	další	k2eAgMnPc1d1	další
babylonští	babylonský	k2eAgMnPc1d1	babylonský
a	a	k8xC	a
asyrští	asyrský	k2eAgMnPc1d1	asyrský
panovníci	panovník	k1gMnPc1	panovník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
novobabylonském	novobabylonský	k2eAgNnSc6d1	novobabylonský
období	období	k1gNnSc6	období
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
oddělení	oddělení	k1gNnSc3	oddělení
panovníka	panovník	k1gMnSc2	panovník
od	od	k7c2	od
role	role	k1gFnSc2	role
velekněze	velekněz	k1gMnPc4	velekněz
<g/>
,	,	kIx,	,
což	což	k3yQnSc4	což
posílilo	posílit	k5eAaPmAgNnS	posílit
postavení	postavení	k1gNnSc1	postavení
kněžstva	kněžstvo	k1gNnSc2	kněžstvo
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
z	z	k7c2	z
panovníků	panovník	k1gMnPc2	panovník
Mezopotámie	Mezopotámie	k1gFnSc2	Mezopotámie
byly	být	k5eAaImAgFnP	být
ženy	žena	k1gFnPc1	žena
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vládly	vládnout	k5eAaImAgFnP	vládnout
buď	buď	k8xC	buď
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
iniciativou	iniciativa	k1gFnSc7	iniciativa
překonávaly	překonávat	k5eAaImAgFnP	překonávat
své	svůj	k3xOyFgMnPc4	svůj
manžely	manžel	k1gMnPc4	manžel
panovníky	panovník	k1gMnPc4	panovník
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
i	i	k9	i
samostatně	samostatně	k6eAd1	samostatně
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
ně	on	k3xPp3gInPc4	on
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
Kubaba	Kubaba	k1gFnSc1	Kubaba
a	a	k8xC	a
Semiramis	Semiramis	k1gFnSc1	Semiramis
<g/>
.	.	kIx.	.
</s>
<s>
Otroctví	otroctví	k1gNnSc1	otroctví
bylo	být	k5eAaImAgNnS	být
základním	základní	k2eAgInSc7d1	základní
rysem	rys	k1gInSc7	rys
mezopotámské	mezopotámský	k2eAgFnSc2d1	mezopotámská
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Právně	právně	k6eAd1	právně
se	se	k3xPyFc4	se
rozlišovalo	rozlišovat	k5eAaImAgNnS	rozlišovat
mezi	mezi	k7c7	mezi
otroky	otrok	k1gMnPc7	otrok
v	v	k7c6	v
soukromých	soukromý	k2eAgFnPc6d1	soukromá
a	a	k8xC	a
veřejných	veřejný	k2eAgFnPc6d1	veřejná
službách	služba	k1gFnPc6	služba
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
otroky	otrok	k1gMnPc7	otrok
domácími	domácí	k2eAgMnPc7d1	domácí
(	(	kIx(	(
<g/>
mezopotámskými	mezopotámský	k2eAgMnPc7d1	mezopotámský
<g/>
)	)	kIx)	)
a	a	k8xC	a
cizími	cizí	k2eAgFnPc7d1	cizí
a	a	k8xC	a
mezi	mezi	k7c7	mezi
otroky	otrok	k1gMnPc7	otrok
narozenými	narozený	k2eAgMnPc7d1	narozený
v	v	k7c6	v
domě	dům	k1gInSc6	dům
pána	pán	k1gMnSc2	pán
a	a	k8xC	a
pořízenými	pořízený	k2eAgFnPc7d1	pořízená
odjinud	odjinud	k6eAd1	odjinud
<g/>
.	.	kIx.	.
</s>
<s>
Třída	třída	k1gFnSc1	třída
otroků	otrok	k1gMnPc2	otrok
se	se	k3xPyFc4	se
rozšiřovala	rozšiřovat	k5eAaImAgFnS	rozšiřovat
např.	např.	kA	např.
o	o	k7c4	o
válečné	válečný	k2eAgMnPc4d1	válečný
zajatce	zajatec	k1gMnPc4	zajatec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
byli	být	k5eAaImAgMnP	být
původně	původně	k6eAd1	původně
zabíjeni	zabíjen	k2eAgMnPc1d1	zabíjen
<g/>
,	,	kIx,	,
o	o	k7c4	o
dlužníky	dlužník	k1gMnPc4	dlužník
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
nebyli	být	k5eNaImAgMnP	být
schopni	schopen	k2eAgMnPc1d1	schopen
uspokojit	uspokojit	k5eAaPmF	uspokojit
své	svůj	k3xOyFgMnPc4	svůj
věřitele	věřitel	k1gMnPc4	věřitel
<g/>
,	,	kIx,	,
a	a	k8xC	a
o	o	k7c4	o
pachatele	pachatel	k1gMnPc4	pachatel
trestných	trestný	k2eAgInPc2d1	trestný
činů	čin	k1gInPc2	čin
<g/>
.	.	kIx.	.
</s>
<s>
Otroci	otrok	k1gMnPc1	otrok
bývali	bývat	k5eAaImAgMnP	bývat
označováni	označovat	k5eAaImNgMnP	označovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
buď	buď	k8xC	buď
zavěšením	zavěšení	k1gNnSc7	zavěšení
tabulky	tabulka	k1gFnSc2	tabulka
<g/>
,	,	kIx,	,
tetováním	tetování	k1gNnSc7	tetování
<g/>
,	,	kIx,	,
oholením	oholení	k1gNnSc7	oholení
vlasů	vlas	k1gInPc2	vlas
na	na	k7c6	na
přední	přední	k2eAgFnSc6d1	přední
polovině	polovina	k1gFnSc6	polovina
hlavy	hlava	k1gFnSc2	hlava
nebo	nebo	k8xC	nebo
propíchnutím	propíchnutí	k1gNnPc3	propíchnutí
uší	ucho	k1gNnPc2	ucho
(	(	kIx(	(
<g/>
v	v	k7c6	v
Asýrii	Asýrie	k1gFnSc6	Asýrie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Nábytek	nábytek	k1gInSc1	nábytek
Mezopotámie	Mezopotámie	k1gFnSc2	Mezopotámie
<g/>
.	.	kIx.	.
</s>
<s>
Mezopotámie	Mezopotámie	k1gFnSc1	Mezopotámie
měla	mít	k5eAaImAgFnS	mít
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Egyptem	Egypt	k1gInSc7	Egypt
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
nejlepší	dobrý	k2eAgFnPc4d3	nejlepší
podmínky	podmínka	k1gFnPc4	podmínka
pro	pro	k7c4	pro
zemědělství	zemědělství	k1gNnSc4	zemědělství
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
tam	tam	k6eAd1	tam
(	(	kIx(	(
<g/>
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
tomu	ten	k3xDgMnSc3	ten
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
dnešním	dnešní	k2eAgInSc6d1	dnešní
Iráku	Irák	k1gInSc6	Irák
<g/>
)	)	kIx)	)
kontinentální	kontinentální	k2eAgNnSc1d1	kontinentální
podnebí	podnebí	k1gNnSc1	podnebí
s	s	k7c7	s
velkými	velký	k2eAgInPc7d1	velký
výkyvy	výkyv	k1gInPc7	výkyv
teplot	teplota	k1gFnPc2	teplota
(	(	kIx(	(
<g/>
zhruba	zhruba	k6eAd1	zhruba
od	od	k7c2	od
+50	+50	k4	+50
°	°	k?	°
<g/>
C	C	kA	C
do	do	k7c2	do
−	−	k?	−
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
až	až	k8xS	až
březnu	březen	k1gInSc6	březen
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
začínalo	začínat	k5eAaImAgNnS	začínat
jaro	jaro	k1gNnSc4	jaro
<g/>
,	,	kIx,	,
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
rozvodnění	rozvodnění	k1gNnSc3	rozvodnění
Eufratu	Eufrat	k1gInSc2	Eufrat
a	a	k8xC	a
Tigridu	Tigris	k1gInSc2	Tigris
<g/>
.	.	kIx.	.
</s>
<s>
Období	období	k1gNnSc1	období
sucha	sucho	k1gNnSc2	sucho
začínalo	začínat	k5eAaImAgNnS	začínat
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
června	červen	k1gInSc2	červen
a	a	k8xC	a
končilo	končit	k5eAaImAgNnS	končit
v	v	k7c6	v
září	září	k1gNnSc6	září
<g/>
.	.	kIx.	.
</s>
<s>
Pole	pole	k1gFnSc1	pole
se	se	k3xPyFc4	se
nehnojila	hnojit	k5eNaImAgFnS	hnojit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
to	ten	k3xDgNnSc1	ten
obstarávaly	obstarávat	k5eAaImAgFnP	obstarávat
záplavy	záplava	k1gFnPc1	záplava
<g/>
,	,	kIx,	,
a	a	k8xC	a
k	k	k7c3	k
setbě	setba	k1gFnSc3	setba
se	se	k3xPyFc4	se
využíval	využívat	k5eAaImAgMnS	využívat
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgInPc4d1	jiný
pluh	pluh	k1gInSc4	pluh
<g/>
.	.	kIx.	.
</s>
<s>
Nejúrodnější	úrodný	k2eAgFnSc1d3	nejúrodnější
půda	půda	k1gFnSc1	půda
se	se	k3xPyFc4	se
nacházela	nacházet	k5eAaImAgFnS	nacházet
v	v	k7c6	v
Babylonii	Babylonie	k1gFnSc6	Babylonie
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgFnPc7d1	hlavní
obilninami	obilnina	k1gFnPc7	obilnina
v	v	k7c6	v
Mezopotámii	Mezopotámie	k1gFnSc6	Mezopotámie
byly	být	k5eAaImAgFnP	být
ječmen	ječmen	k1gInSc4	ječmen
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
sloužil	sloužit	k5eAaImAgInS	sloužit
k	k	k7c3	k
přípravě	příprava	k1gFnSc3	příprava
potravin	potravina	k1gFnPc2	potravina
<g/>
,	,	kIx,	,
pšenice	pšenice	k1gFnSc1	pšenice
dvouzrnka	dvouzrnka	k1gFnSc1	dvouzrnka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
sloužila	sloužit	k5eAaImAgFnS	sloužit
k	k	k7c3	k
přípravě	příprava	k1gFnSc3	příprava
chleba	chléb	k1gInSc2	chléb
a	a	k8xC	a
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
piva	pivo	k1gNnSc2	pivo
<g/>
,	,	kIx,	,
a	a	k8xC	a
pšenice	pšenice	k1gFnSc1	pšenice
jednozrnka	jednozrnko	k1gNnSc2	jednozrnko
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
pěstovaly	pěstovat	k5eAaImAgFnP	pěstovat
např.	např.	kA	např.
sezam	sezam	k1gInSc1	sezam
<g/>
,	,	kIx,	,
hrách	hrách	k1gInSc1	hrách
setý	setý	k2eAgInSc1d1	setý
a	a	k8xC	a
boby	bob	k1gInPc7	bob
<g/>
.	.	kIx.	.
</s>
<s>
Zahradnictví	zahradnictví	k1gNnSc1	zahradnictví
patřilo	patřit	k5eAaImAgNnS	patřit
v	v	k7c6	v
Mezopotámii	Mezopotámie	k1gFnSc6	Mezopotámie
k	k	k7c3	k
oblíbeným	oblíbený	k2eAgFnPc3d1	oblíbená
aktivitám	aktivita	k1gFnPc3	aktivita
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
ovocnářství	ovocnářství	k1gNnSc1	ovocnářství
převažovalo	převažovat	k5eAaImAgNnS	převažovat
nad	nad	k7c7	nad
zelinářstvím	zelinářství	k1gNnSc7	zelinářství
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějším	známý	k2eAgInSc7d3	nejznámější
objektem	objekt	k1gInSc7	objekt
z	z	k7c2	z
Mezopotámie	Mezopotámie	k1gFnSc2	Mezopotámie
týkajícím	týkající	k2eAgInSc7d1	týkající
se	se	k3xPyFc4	se
zahradnictví	zahradnictví	k1gNnPc1	zahradnictví
jsou	být	k5eAaImIp3nP	být
Visuté	visutý	k2eAgFnPc4d1	visutá
zahrady	zahrada	k1gFnPc4	zahrada
Semiramidiny	Semiramidin	k2eAgFnPc4d1	Semiramidin
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
sedmi	sedm	k4xCc2	sedm
divů	div	k1gInPc2	div
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastějším	častý	k2eAgInSc7d3	nejčastější
stromem	strom	k1gInSc7	strom
Mezopotámie	Mezopotámie	k1gFnSc2	Mezopotámie
(	(	kIx(	(
<g/>
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
i	i	k8xC	i
Iráku	Irák	k1gInSc2	Irák
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
palma	palma	k1gFnSc1	palma
datlová	datlový	k2eAgFnSc1d1	datlová
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
pěstovaly	pěstovat	k5eAaImAgFnP	pěstovat
např.	např.	kA	např.
fíkovníky	fíkovník	k1gInPc4	fíkovník
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
réva	réva	k1gFnSc1	réva
vinná	vinný	k2eAgFnSc1d1	vinná
<g/>
,	,	kIx,	,
jabloně	jabloň	k1gFnPc1	jabloň
<g/>
,	,	kIx,	,
hrušně	hrušeň	k1gFnPc1	hrušeň
a	a	k8xC	a
mandlovníky	mandlovník	k1gInPc1	mandlovník
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
zeleniny	zelenina	k1gFnSc2	zelenina
to	ten	k3xDgNnSc1	ten
byly	být	k5eAaImAgFnP	být
např.	např.	kA	např.
česnek	česnek	k1gInSc1	česnek
<g/>
,	,	kIx,	,
cibule	cibule	k1gFnSc1	cibule
<g/>
,	,	kIx,	,
salát	salát	k1gInSc1	salát
nebo	nebo	k8xC	nebo
kopr	kopr	k1gInSc1	kopr
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dobytka	dobytek	k1gInSc2	dobytek
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
choval	chovat	k5eAaImAgMnS	chovat
tur	tur	k1gMnSc1	tur
domácí	domácí	k1gMnSc1	domácí
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
využíval	využívat	k5eAaPmAgInS	využívat
hlavně	hlavně	k9	hlavně
k	k	k7c3	k
orbě	orba	k1gFnSc3	orba
<g/>
,	,	kIx,	,
k	k	k7c3	k
obsluze	obsluha	k1gFnSc3	obsluha
zavodňovacích	zavodňovací	k2eAgNnPc2d1	zavodňovací
zařízení	zařízení	k1gNnPc2	zařízení
a	a	k8xC	a
pro	pro	k7c4	pro
mléko	mléko	k1gNnSc4	mléko
<g/>
.	.	kIx.	.
</s>
<s>
Hovězí	hovězí	k2eAgNnSc1d1	hovězí
maso	maso	k1gNnSc1	maso
bylo	být	k5eAaImAgNnS	být
kvůli	kvůli	k7c3	kvůli
nákladnosti	nákladnost	k1gFnSc3	nákladnost
chovu	chov	k1gInSc2	chov
drahé	drahá	k1gFnSc2	drahá
<g/>
.	.	kIx.	.
</s>
<s>
Důležitý	důležitý	k2eAgInSc1d1	důležitý
byl	být	k5eAaImAgInS	být
také	také	k9	také
chov	chov	k1gInSc1	chov
koz	koza	k1gFnPc2	koza
a	a	k8xC	a
ovcí	ovce	k1gFnPc2	ovce
<g/>
,	,	kIx,	,
z	z	k7c2	z
kterých	který	k3yIgFnPc2	který
se	se	k3xPyFc4	se
získávala	získávat	k5eAaImAgFnS	získávat
vlna	vlna	k1gFnSc1	vlna
(	(	kIx(	(
<g/>
ovce	ovce	k1gFnSc1	ovce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
srst	srst	k1gFnSc4	srst
(	(	kIx(	(
<g/>
kozy	koza	k1gFnSc2	koza
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mléko	mléko	k1gNnSc1	mléko
a	a	k8xC	a
maso	maso	k1gNnSc1	maso
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgNnPc4d1	další
hospodářská	hospodářský	k2eAgNnPc4d1	hospodářské
zvířata	zvíře	k1gNnPc4	zvíře
patřily	patřit	k5eAaImAgFnP	patřit
např.	např.	kA	např.
husy	husa	k1gFnPc1	husa
<g/>
,	,	kIx,	,
kachny	kachna	k1gFnPc1	kachna
a	a	k8xC	a
kuřata	kuře	k1gNnPc1	kuře
<g/>
;	;	kIx,	;
v	v	k7c6	v
pozdější	pozdní	k2eAgFnSc6d2	pozdější
asyrské	asyrský	k2eAgFnSc6d1	Asyrská
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
chovat	chovat	k5eAaImF	chovat
i	i	k9	i
včely	včela	k1gFnPc4	včela
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
tažná	tažný	k2eAgNnPc1d1	tažné
zvířata	zvíře	k1gNnPc1	zvíře
se	se	k3xPyFc4	se
nejdříve	dříve	k6eAd3	dříve
využívali	využívat	k5eAaPmAgMnP	využívat
osli	osel	k1gMnPc1	osel
<g/>
,	,	kIx,	,
od	od	k7c2	od
dob	doba	k1gFnPc2	doba
kassitských	kassitský	k2eAgMnPc2d1	kassitský
panovníků	panovník	k1gMnPc2	panovník
pak	pak	k6eAd1	pak
koně	kůň	k1gMnPc1	kůň
a	a	k8xC	a
kříženci	kříženec	k1gMnPc1	kříženec
předchozích	předchozí	k2eAgFnPc2d1	předchozí
dvou	dva	k4xCgMnPc6	dva
muly	mula	k1gFnPc1	mula
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
domácí	domácí	k2eAgMnPc1d1	domácí
mazlíčci	mazlíček	k1gMnPc1	mazlíček
nebo	nebo	k8xC	nebo
pomocníci	pomocník	k1gMnPc1	pomocník
pastýřů	pastýř	k1gMnPc2	pastýř
byli	být	k5eAaImAgMnP	být
chování	chování	k1gNnSc3	chování
psi	pes	k1gMnPc1	pes
<g/>
;	;	kIx,	;
někteří	některý	k3yIgMnPc1	některý
byli	být	k5eAaImAgMnP	být
využíváni	využíván	k2eAgMnPc1d1	využíván
i	i	k9	i
k	k	k7c3	k
lovu	lov	k1gInSc3	lov
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
2	[number]	k4	2
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
hrál	hrát	k5eAaImAgMnS	hrát
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
rybolov	rybolov	k1gInSc1	rybolov
<g/>
.	.	kIx.	.
</s>
<s>
Řemeslníci	řemeslník	k1gMnPc1	řemeslník
se	se	k3xPyFc4	se
zpočátku	zpočátku	k6eAd1	zpočátku
soustředili	soustředit	k5eAaPmAgMnP	soustředit
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
zemědělských	zemědělský	k2eAgInPc2d1	zemědělský
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
,	,	kIx,	,
od	od	k7c2	od
první	první	k4xOgFnSc2	první
poloviny	polovina	k1gFnSc2	polovina
3	[number]	k4	3
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
začalo	začít	k5eAaPmAgNnS	začít
docházet	docházet	k5eAaImF	docházet
k	k	k7c3	k
rozšiřování	rozšiřování	k1gNnSc3	rozšiřování
výroby	výroba	k1gFnSc2	výroba
i	i	k9	i
do	do	k7c2	do
jiných	jiný	k2eAgFnPc2d1	jiná
oblastí	oblast	k1gFnPc2	oblast
(	(	kIx(	(
<g/>
např.	např.	kA	např.
oblečení	oblečení	k1gNnSc1	oblečení
<g/>
,	,	kIx,	,
nádobí	nádobí	k1gNnSc1	nádobí
<g/>
,	,	kIx,	,
šperky	šperk	k1gInPc1	šperk
<g/>
,	,	kIx,	,
zbraně	zbraň	k1gFnPc1	zbraň
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgNnPc2d3	nejstarší
řemesel	řemeslo	k1gNnPc2	řemeslo
bylo	být	k5eAaImAgNnS	být
hrnčířství	hrnčířství	k1gNnSc1	hrnčířství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Mezopotámii	Mezopotámie	k1gFnSc6	Mezopotámie
byl	být	k5eAaImAgInS	být
nedostatek	nedostatek	k1gInSc1	nedostatek
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
,	,	kIx,	,
kamene	kámen	k1gInSc2	kámen
a	a	k8xC	a
kovů	kov	k1gInPc2	kov
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
materiál	materiál	k1gInSc1	materiál
se	se	k3xPyFc4	se
používaly	používat	k5eAaImAgInP	používat
hlavně	hlavně	k9	hlavně
hlína	hlína	k1gFnSc1	hlína
<g/>
,	,	kIx,	,
rákos	rákos	k1gInSc1	rákos
<g/>
,	,	kIx,	,
vlna	vlna	k1gFnSc1	vlna
<g/>
,	,	kIx,	,
kůže	kůže	k1gFnSc1	kůže
a	a	k8xC	a
len	len	k1gInSc1	len
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
náboženských	náboženský	k2eAgFnPc6d1	náboženská
představách	představa	k1gFnPc6	představa
obyvatel	obyvatel	k1gMnSc1	obyvatel
Mezopotámie	Mezopotámie	k1gFnSc1	Mezopotámie
se	se	k3xPyFc4	se
odrážely	odrážet	k5eAaImAgInP	odrážet
přírodní	přírodní	k2eAgInPc1d1	přírodní
jevy	jev	k1gInPc1	jev
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
podstatu	podstata	k1gFnSc4	podstata
si	se	k3xPyFc3	se
nedovedli	dovést	k5eNaPmAgMnP	dovést
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
rozumem	rozum	k1gInSc7	rozum
<g/>
.	.	kIx.	.
</s>
<s>
Představy	představa	k1gFnPc1	představa
se	se	k3xPyFc4	se
projevovaly	projevovat	k5eAaImAgFnP	projevovat
kultem	kult	k1gInSc7	kult
bohyně-matky	bohyněatka	k1gFnSc2	bohyně-matka
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnPc1	jejíž
sošky	soška	k1gFnPc1	soška
jsou	být	k5eAaImIp3nP	být
známé	známý	k2eAgFnPc1d1	známá
již	již	k6eAd1	již
z	z	k7c2	z
5	[number]	k4	5
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Základní	základní	k2eAgFnSc2d1	základní
přírodní	přírodní	k2eAgFnSc2d1	přírodní
síly	síla	k1gFnSc2	síla
byly	být	k5eAaImAgInP	být
zosobňovány	zosobňovat	k5eAaImNgInP	zosobňovat
a	a	k8xC	a
povyšovány	povyšovat	k5eAaImNgInP	povyšovat
na	na	k7c4	na
bohy	bůh	k1gMnPc4	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
měli	mít	k5eAaImAgMnP	mít
Sumerové	Sumer	k1gMnPc1	Sumer
několik	několik	k4yIc4	několik
set	set	k1gInSc4	set
takových	takový	k3xDgMnPc2	takový
bohů	bůh	k1gMnPc2	bůh
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
nejvýznamnější	významný	k2eAgMnPc1d3	nejvýznamnější
byli	být	k5eAaImAgMnP	být
An	An	k1gFnSc4	An
<g/>
,	,	kIx,	,
bůh	bůh	k1gMnSc1	bůh
nebe	nebe	k1gNnSc2	nebe
<g/>
,	,	kIx,	,
Enlil	Enlil	k1gMnSc1	Enlil
<g/>
,	,	kIx,	,
bůh	bůh	k1gMnSc1	bůh
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
a	a	k8xC	a
Enki	Enk	k1gMnPc1	Enk
<g/>
,	,	kIx,	,
bůh	bůh	k1gMnSc1	bůh
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivá	jednotlivý	k2eAgNnPc4d1	jednotlivé
města	město	k1gNnPc4	město
měla	mít	k5eAaImAgFnS	mít
své	svůj	k3xOyFgFnPc4	svůj
božské	božská	k1gFnPc4	božská
ochránce	ochránce	k1gMnSc2	ochránce
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
význam	význam	k1gInSc1	význam
vzrůstal	vzrůstat	k5eAaImAgInS	vzrůstat
a	a	k8xC	a
klesal	klesat	k5eAaImAgInS	klesat
s	s	k7c7	s
významem	význam	k1gInSc7	význam
daného	daný	k2eAgNnSc2d1	dané
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Středem	středem	k7c2	středem
náboženského	náboženský	k2eAgInSc2d1	náboženský
života	život	k1gInSc2	život
byl	být	k5eAaImAgInS	být
chrám	chrám	k1gInSc1	chrám
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc1	jehož
jádro	jádro	k1gNnSc1	jádro
bylo	být	k5eAaImAgNnS	být
tvořeno	tvořit	k5eAaImNgNnS	tvořit
obětním	obětní	k2eAgInSc7d1	obětní
oltářem	oltář	k1gInSc7	oltář
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
byl	být	k5eAaImAgMnS	být
původně	původně	k6eAd1	původně
přístupný	přístupný	k2eAgInSc4d1	přístupný
všem	všecek	k3xTgFnPc3	všecek
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
ale	ale	k8xC	ale
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
vrstva	vrstva	k1gFnSc1	vrstva
kněžích	kněz	k1gMnPc6	kněz
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
považovali	považovat	k5eAaImAgMnP	považovat
za	za	k7c4	za
prostředníky	prostředník	k1gInPc4	prostředník
mezi	mezi	k7c7	mezi
bohy	bůh	k1gMnPc7	bůh
a	a	k8xC	a
lidmi	člověk	k1gMnPc7	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Kněží	kněz	k1gMnPc1	kněz
si	se	k3xPyFc3	se
postupem	postupem	k7c2	postupem
času	čas	k1gInSc2	čas
vybudovali	vybudovat	k5eAaPmAgMnP	vybudovat
silný	silný	k2eAgInSc4d1	silný
společenský	společenský	k2eAgInSc4d1	společenský
a	a	k8xC	a
politický	politický	k2eAgInSc4d1	politický
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
náboženské	náboženský	k2eAgFnPc4d1	náboženská
stavby	stavba	k1gFnPc4	stavba
patřily	patřit	k5eAaImAgInP	patřit
zikkuraty	zikkurat	k1gInPc1	zikkurat
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
měly	mít	k5eAaImAgFnP	mít
zdůraznit	zdůraznit	k5eAaPmF	zdůraznit
menší	malý	k2eAgFnSc1d2	menší
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
kněží	kněz	k1gMnPc2	kněz
k	k	k7c3	k
bohům	bůh	k1gMnPc3	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
hrály	hrát	k5eAaImAgFnP	hrát
chrámové	chrámový	k2eAgFnPc1d1	chrámová
slavnosti	slavnost	k1gFnPc1	slavnost
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
nejvýznamnější	významný	k2eAgMnPc4d3	nejvýznamnější
byly	být	k5eAaImAgFnP	být
novoroční	novoroční	k2eAgInPc4d1	novoroční
svátky	svátek	k1gInPc4	svátek
pořádané	pořádaný	k2eAgInPc4d1	pořádaný
při	při	k7c6	při
jarní	jarní	k2eAgFnSc6d1	jarní
rovnodennosti	rovnodennost	k1gFnSc6	rovnodennost
<g/>
.	.	kIx.	.
</s>
<s>
Chrámy	chrám	k1gInPc1	chrám
měly	mít	k5eAaImAgInP	mít
velký	velký	k2eAgInSc4d1	velký
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
vzdělání	vzdělání	k1gNnSc4	vzdělání
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
u	u	k7c2	u
nich	on	k3xPp3gInPc2	on
zakládaly	zakládat	k5eAaImAgInP	zakládat
archivy	archiv	k1gInPc1	archiv
<g/>
,	,	kIx,	,
knihovny	knihovna	k1gFnPc1	knihovna
a	a	k8xC	a
školy	škola	k1gFnPc1	škola
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
potomci	potomek	k1gMnPc1	potomek
vyšších	vysoký	k2eAgFnPc2d2	vyšší
vrstev	vrstva	k1gFnPc2	vrstva
učili	učit	k5eAaImAgMnP	učit
čtení	čtení	k1gNnSc3	čtení
<g/>
,	,	kIx,	,
psaní	psaní	k1gNnSc3	psaní
a	a	k8xC	a
základním	základní	k2eAgInPc3d1	základní
vědním	vědní	k2eAgInPc3d1	vědní
oborům	obor	k1gInPc3	obor
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
školy	škola	k1gFnPc1	škola
vznikaly	vznikat	k5eAaImAgFnP	vznikat
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
3	[number]	k4	3
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
byly	být	k5eAaImAgFnP	být
zesvětštěny	zesvětštěn	k2eAgFnPc1d1	zesvětštěn
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
se	se	k3xPyFc4	se
chrámy	chrám	k1gInPc1	chrám
snažily	snažit	k5eAaImAgInP	snažit
pomáhat	pomáhat	k5eAaImF	pomáhat
chudým	chudý	k1gMnSc7	chudý
<g/>
,	,	kIx,	,
např.	např.	kA	např.
stanovováním	stanovování	k1gNnSc7	stanovování
spravedlivých	spravedlivý	k2eAgInPc2d1	spravedlivý
standardů	standard	k1gInPc2	standard
váhy	váha	k1gFnSc2	váha
a	a	k8xC	a
míry	míra	k1gFnSc2	míra
nebo	nebo	k8xC	nebo
poskytováním	poskytování	k1gNnSc7	poskytování
bezúročných	bezúročný	k2eAgFnPc2d1	bezúročná
půjček	půjčka	k1gFnPc2	půjčka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Mezopotámii	Mezopotámie	k1gFnSc6	Mezopotámie
byla	být	k5eAaImAgFnS	být
známá	známý	k2eAgFnSc1d1	známá
desítková	desítkový	k2eAgFnSc1d1	desítková
i	i	k8xC	i
šedesátková	šedesátkový	k2eAgFnSc1d1	šedesátková
soustava	soustava	k1gFnSc1	soustava
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
starší	starší	k1gMnPc4	starší
byla	být	k5eAaImAgFnS	být
šedesátková	šedesátkový	k2eAgFnSc1d1	šedesátková
<g/>
.	.	kIx.	.
</s>
<s>
Znak	znak	k1gInSc4	znak
pro	pro	k7c4	pro
nulu	nula	k1gFnSc4	nula
se	se	k3xPyFc4	se
používal	používat	k5eAaImAgInS	používat
až	až	k9	až
v	v	k7c6	v
pozdně	pozdně	k6eAd1	pozdně
babylonských	babylonský	k2eAgInPc6d1	babylonský
astronomických	astronomický	k2eAgInPc6d1	astronomický
textech	text	k1gInPc6	text
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ne	ne	k9	ne
na	na	k7c6	na
konci	konec	k1gInSc6	konec
čísla	číslo	k1gNnSc2	číslo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
vynechávalo	vynechávat	k5eAaImAgNnS	vynechávat
místo	místo	k1gNnSc1	místo
nebo	nebo	k8xC	nebo
tam	tam	k6eAd1	tam
byly	být	k5eAaImAgInP	být
umístěny	umístit	k5eAaPmNgInP	umístit
dva	dva	k4xCgInPc1	dva
šikmé	šikmý	k2eAgInPc1d1	šikmý
klíny	klín	k1gInPc1	klín
<g/>
.	.	kIx.	.
</s>
<s>
Používalo	používat	k5eAaImAgNnS	používat
se	se	k3xPyFc4	se
sčítání	sčítání	k1gNnSc1	sčítání
a	a	k8xC	a
odčítání	odčítání	k1gNnSc1	odčítání
<g/>
,	,	kIx,	,
násobení	násobení	k1gNnSc1	násobení
a	a	k8xC	a
dělení	dělení	k1gNnSc1	dělení
<g/>
,	,	kIx,	,
zlomky	zlomek	k1gInPc1	zlomek
<g/>
,	,	kIx,	,
umocňování	umocňování	k1gNnSc1	umocňování
a	a	k8xC	a
odmocňování	odmocňování	k1gNnSc1	odmocňování
i	i	k9	i
rovnice	rovnice	k1gFnSc1	rovnice
druhého	druhý	k4xOgInSc2	druhý
a	a	k8xC	a
třetího	třetí	k4xOgInSc2	třetí
stupně	stupeň	k1gInSc2	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
Geometrie	geometrie	k1gFnSc1	geometrie
se	se	k3xPyFc4	se
využívala	využívat	k5eAaImAgFnS	využívat
převážně	převážně	k6eAd1	převážně
na	na	k7c4	na
výměru	výměra	k1gFnSc4	výměra
polí	pole	k1gFnPc2	pole
a	a	k8xC	a
jiných	jiný	k2eAgFnPc2d1	jiná
nemovitostí	nemovitost	k1gFnPc2	nemovitost
<g/>
,	,	kIx,	,
měření	měření	k1gNnSc4	měření
objemu	objem	k1gInSc2	objem
košů	koš	k1gInPc2	koš
a	a	k8xC	a
schránek	schránka	k1gFnPc2	schránka
atd.	atd.	kA	atd.
<g/>
,	,	kIx,	,
objevy	objev	k1gInPc4	objev
ale	ale	k8xC	ale
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tehdejší	tehdejší	k2eAgMnPc1d1	tehdejší
odborníci	odborník	k1gMnPc1	odborník
zabývali	zabývat	k5eAaImAgMnP	zabývat
i	i	k8xC	i
problémy	problém	k1gInPc4	problém
Pythagorovy	Pythagorův	k2eAgFnSc2d1	Pythagorova
věty	věta	k1gFnSc2	věta
a	a	k8xC	a
Eukleidovské	eukleidovský	k2eAgFnSc2d1	eukleidovská
geometrie	geometrie	k1gFnSc2	geometrie
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Babylónská	babylónský	k2eAgFnSc1d1	Babylónská
astrologie	astrologie	k1gFnSc1	astrologie
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
astronomická	astronomický	k2eAgNnPc1d1	astronomické
pozorování	pozorování	k1gNnPc1	pozorování
probíhala	probíhat	k5eAaImAgNnP	probíhat
na	na	k7c6	na
zikkuratech	zikkurat	k1gInPc6	zikkurat
<g/>
,	,	kIx,	,
podíleli	podílet	k5eAaImAgMnP	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
nich	on	k3xPp3gFnPc6	on
převážně	převážně	k6eAd1	převážně
kněží	kněz	k1gMnPc2	kněz
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
činnost	činnost	k1gFnSc1	činnost
často	často	k6eAd1	často
nebyla	být	k5eNaImAgFnS	být
vědecká	vědecký	k2eAgFnSc1d1	vědecká
a	a	k8xC	a
namísto	namísto	k7c2	namísto
toho	ten	k3xDgInSc2	ten
se	se	k3xPyFc4	se
zabývali	zabývat	k5eAaImAgMnP	zabývat
astrologií	astrologie	k1gFnSc7	astrologie
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
astronomové	astronom	k1gMnPc1	astronom
zjistili	zjistit	k5eAaPmAgMnP	zjistit
ekliptiku	ekliptika	k1gFnSc4	ekliptika
Slunce	slunce	k1gNnSc2	slunce
a	a	k8xC	a
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
Měsíce	měsíc	k1gInSc2	měsíc
okolo	okolo	k7c2	okolo
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
7	[number]	k4	7
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
dokázali	dokázat	k5eAaPmAgMnP	dokázat
předpovídat	předpovídat	k5eAaImF	předpovídat
zatmění	zatmění	k1gNnPc4	zatmění
Měsíce	měsíc	k1gInSc2	měsíc
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
zatmění	zatmění	k1gNnSc4	zatmění
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Rozdělili	rozdělit	k5eAaPmAgMnP	rozdělit
lunární	lunární	k2eAgInSc4d1	lunární
rok	rok	k1gInSc4	rok
na	na	k7c4	na
12	[number]	k4	12
měsíců	měsíc	k1gInPc2	měsíc
s	s	k7c7	s
354	[number]	k4	354
dny	den	k1gInPc7	den
<g/>
,	,	kIx,	,
k	k	k7c3	k
vyrovnání	vyrovnání	k1gNnSc3	vyrovnání
se	se	k3xPyFc4	se
solárním	solární	k2eAgInSc7d1	solární
rokem	rok	k1gInSc7	rok
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
kalendáře	kalendář	k1gInSc2	kalendář
vkládán	vkládán	k2eAgInSc4d1	vkládán
přestupní	přestupní	k2eAgInSc4d1	přestupní
měsíc	měsíc	k1gInSc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
3	[number]	k4	3
<g/>
.	.	kIx.	.
dynastie	dynastie	k1gFnSc2	dynastie
urské	urský	k2eAgFnSc2d1	urská
začínal	začínat	k5eAaImAgInS	začínat
rok	rok	k1gInSc4	rok
jarní	jarní	k2eAgFnSc7d1	jarní
rovnodenností	rovnodennost	k1gFnSc7	rovnodennost
<g/>
.	.	kIx.	.
–	–	k?	–
<g/>
Josef	Josef	k1gMnSc1	Josef
Klíma	Klíma	k1gMnSc1	Klíma
<g/>
,	,	kIx,	,
Společnost	společnost	k1gFnSc1	společnost
a	a	k8xC	a
kultura	kultura	k1gFnSc1	kultura
starověké	starověký	k2eAgFnSc2d1	starověká
Mezopotámie	Mezopotámie	k1gFnSc2	Mezopotámie
Z	z	k7c2	z
Mezopotámie	Mezopotámie	k1gFnSc2	Mezopotámie
se	se	k3xPyFc4	se
zachovalo	zachovat	k5eAaPmAgNnS	zachovat
množství	množství	k1gNnSc1	množství
lékařských	lékařský	k2eAgInPc2d1	lékařský
textů	text	k1gInPc2	text
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
druhem	druh	k1gInSc7	druh
těchto	tento	k3xDgInPc2	tento
textů	text	k1gInPc2	text
jsou	být	k5eAaImIp3nP	být
předpovědi	předpověď	k1gFnPc1	předpověď
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
zabývaly	zabývat	k5eAaImAgFnP	zabývat
vyhlídkami	vyhlídka	k1gFnPc7	vyhlídka
pacienta	pacient	k1gMnSc2	pacient
v	v	k7c6	v
určitých	určitý	k2eAgFnPc6d1	určitá
situacích	situace	k1gFnPc6	situace
(	(	kIx(	(
<g/>
s	s	k7c7	s
určitými	určitý	k2eAgFnPc7d1	určitá
nemocemi	nemoc	k1gFnPc7	nemoc
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
zřídka	zřídka	k6eAd1	zřídka
se	se	k3xPyFc4	se
zabývaly	zabývat	k5eAaImAgInP	zabývat
léčbou	léčba	k1gFnSc7	léčba
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
texty	text	k1gInPc1	text
zabývající	zabývající	k2eAgFnSc2d1	zabývající
se	se	k3xPyFc4	se
léčením	léčení	k1gNnSc7	léčení
pacientů	pacient	k1gMnPc2	pacient
vypovídají	vypovídat	k5eAaImIp3nP	vypovídat
o	o	k7c6	o
využívaných	využívaný	k2eAgInPc6d1	využívaný
lécích	lék	k1gInPc6	lék
<g/>
,	,	kIx,	,
kterými	který	k3yRgInPc7	který
byly	být	k5eAaImAgFnP	být
živočišné	živočišný	k2eAgInPc4d1	živočišný
produkty	produkt	k1gInPc4	produkt
<g/>
,	,	kIx,	,
např.	např.	kA	např.
tuk	tuk	k1gInSc1	tuk
<g/>
,	,	kIx,	,
krev	krev	k1gFnSc1	krev
<g/>
,	,	kIx,	,
mléko	mléko	k1gNnSc1	mléko
a	a	k8xC	a
kosti	kost	k1gFnPc1	kost
<g/>
,	,	kIx,	,
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
byliny	bylina	k1gFnPc4	bylina
<g/>
,	,	kIx,	,
stonky	stonek	k1gInPc4	stonek
<g/>
,	,	kIx,	,
listy	list	k1gInPc4	list
<g/>
,	,	kIx,	,
kořeny	kořen	k1gInPc4	kořen
a	a	k8xC	a
plody	plod	k1gInPc4	plod
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
mohly	moct	k5eAaImAgInP	moct
být	být	k5eAaImF	být
případně	případně	k6eAd1	případně
smíchány	smíchán	k2eAgInPc4d1	smíchán
např.	např.	kA	např.
s	s	k7c7	s
pivem	pivo	k1gNnSc7	pivo
nebo	nebo	k8xC	nebo
medem	med	k1gInSc7	med
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
se	se	k3xPyFc4	se
polykaly	polykat	k5eAaImAgFnP	polykat
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgInPc4d1	jiný
se	se	k3xPyFc4	se
přikládaly	přikládat	k5eAaImAgInP	přikládat
na	na	k7c4	na
tělo	tělo	k1gNnSc4	tělo
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
do	do	k7c2	do
něho	on	k3xPp3gMnSc2	on
dopravovaly	dopravovat	k5eAaImAgFnP	dopravovat
čípkem	čípek	k1gInSc7	čípek
<g/>
.	.	kIx.	.
</s>
