<s>
Alfeios	Alfeios	k1gMnSc1
</s>
<s>
Alfeios	Alfeios	k1gMnSc1
Hráz	hráz	k1gFnSc4
na	na	k7c6
řece	řeka	k1gFnSc6
nedaleko	daleko	k6eNd1
OlympieZákladní	OlympieZákladný	k2eAgMnPc1d1
informace	informace	k1gFnSc2
Délka	délka	k1gFnSc1
toku	tok	k1gInSc2
</s>
<s>
110	#num#	k4
km	km	kA
Plocha	plocha	k1gFnSc1
povodí	povodit	k5eAaPmIp3nS
</s>
<s>
3600	#num#	k4
km²	km²	k?
Průměrný	průměrný	k2eAgInSc1d1
průtok	průtok	k1gInSc1
</s>
<s>
67	#num#	k4
m³	m³	k?
<g/>
/	/	kIx~
<g/>
s	s	k7c7
Pramen	pramen	k1gInSc1
</s>
<s>
nedaleko	daleko	k6eNd1
Valtetsi	Valtetse	k1gFnSc3
800	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Ústí	ústí	k1gNnSc1
</s>
<s>
Jónské	jónský	k2eAgNnSc1d1
moře	moře	k1gNnSc1
37	#num#	k4
<g/>
°	°	k?
<g/>
36	#num#	k4
<g/>
′	′	k?
<g/>
45	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
21	#num#	k4
<g/>
°	°	k?
<g/>
27	#num#	k4
<g/>
′	′	k?
<g/>
6	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
0	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Protéká	protékat	k5eAaImIp3nS
</s>
<s>
Řecko	Řecko	k1gNnSc1
Řecko	Řecko	k1gNnSc1
</s>
<s>
Mapa	mapa	k1gFnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Alfeios	Alfeios	k1gInSc1
(	(	kIx(
<g/>
řecky	řecky	k6eAd1
Α	Α	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zvaná	zvaný	k2eAgFnSc1d1
také	také	k9
Roufias	Roufias	k1gInSc4
nebo	nebo	k8xC
Charbon	Charbon	k1gInSc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
řeka	řeka	k1gFnSc1
v	v	k7c6
Řecku	Řecko	k1gNnSc6
<g/>
,	,	kIx,
přítok	přítok	k1gInSc4
Jónského	jónský	k2eAgNnSc2d1
moře	moře	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měří	měřit	k5eAaImIp3nS
110	#num#	k4
km	km	kA
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
nejdelší	dlouhý	k2eAgFnSc7d3
řekou	řeka	k1gFnSc7
Peloponésu	Peloponés	k1gInSc2
a	a	k8xC
osmou	osmý	k4xOgFnSc7
nejdelší	dlouhý	k2eAgFnSc7d3
řekou	řeka	k1gFnSc7
celého	celý	k2eAgNnSc2d1
Řecka	Řecko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pramení	pramenit	k5eAaImIp3nS
v	v	k7c6
arkádském	arkádský	k2eAgNnSc6d1
pohoří	pohoří	k1gNnSc6
Parnon	Parnona	k1gFnPc2
<g/>
,	,	kIx,
protéká	protékat	k5eAaImIp3nS
plání	pláň	k1gFnSc7
Megalopolis	Megalopolis	k1gFnSc2
<g/>
,	,	kIx,
její	její	k3xOp3gInSc1
dolní	dolní	k2eAgInSc1d1
tok	tok	k1gInSc1
na	na	k7c6
území	území	k1gNnSc6
Élidy	Élida	k1gFnSc2
vytváří	vytvářit	k5eAaPmIp3nS,k5eAaImIp3nS
úrodnou	úrodný	k2eAgFnSc4d1
údolní	údolní	k2eAgFnSc4d1
nivu	niva	k1gFnSc4
<g/>
,	,	kIx,
vlévá	vlévat	k5eAaImIp3nS
se	se	k3xPyFc4
do	do	k7c2
moře	moře	k1gNnSc2
jižně	jižně	k6eAd1
od	od	k7c2
města	město	k1gNnSc2
Pyrgos	Pyrgosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
deltě	delta	k1gFnSc6
řeky	řeka	k1gFnSc2
se	se	k3xPyFc4
nacházelo	nacházet	k5eAaImAgNnS
jezero	jezero	k1gNnSc1
Agoulinitsa	Agoulinitsa	k1gFnSc1
<g/>
,	,	kIx,
které	který	k3yRgFnSc3,k3yIgFnSc3,k3yQgFnSc3
bylo	být	k5eAaImAgNnS
v	v	k7c6
roce	rok	k1gInSc6
1968	#num#	k4
vysušeno	vysušen	k2eAgNnSc1d1
a	a	k8xC
jeho	jeho	k3xOp3gNnSc1
dno	dno	k1gNnSc1
proměněno	proměnit	k5eAaPmNgNnS
na	na	k7c4
ornou	orný	k2eAgFnSc4d1
půdu	půda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proud	proud	k1gInSc1
unáší	unášet	k5eAaImIp3nS
velké	velký	k2eAgNnSc4d1
množství	množství	k1gNnSc4
sedimentů	sediment	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvýznamnějšími	významný	k2eAgInPc7d3
přítoky	přítok	k1gInPc7
jsou	být	k5eAaImIp3nP
Ladonas	Ladonas	k1gMnSc1
a	a	k8xC
Erymanthos	Erymanthos	k1gMnSc1
<g/>
,	,	kIx,
oblast	oblast	k1gFnSc1
okolo	okolo	k7c2
jejich	jejich	k3xOp3gInSc2
soutoku	soutok	k1gInSc2
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
Tripotamia	Tripotamia	k1gFnSc1
(	(	kIx(
<g/>
„	„	k?
<g/>
tři	tři	k4xCgFnPc1
řeky	řeka	k1gFnPc1
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
řece	řeka	k1gFnSc6
byly	být	k5eAaImAgFnP
zbudovány	zbudován	k2eAgFnPc4d1
přehrady	přehrada	k1gFnPc4
sloužící	sloužící	k2eAgFnPc1d1
k	k	k7c3
zavlažování	zavlažování	k1gNnSc3
i	i	k8xC
výrobě	výroba	k1gFnSc3
elektrické	elektrický	k2eAgFnSc2d1
energie	energie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Povodí	povodí	k1gNnSc1
Alfeia	Alfeius	k1gMnSc2
je	být	k5eAaImIp3nS
významnou	významný	k2eAgFnSc7d1
zemědělskou	zemědělský	k2eAgFnSc7d1
oblastí	oblast	k1gFnSc7
<g/>
,	,	kIx,
v	v	k7c6
obci	obec	k1gFnSc6
Kaliani	Kaliaň	k1gFnSc6
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
velká	velký	k2eAgFnSc1d1
továrna	továrna	k1gFnSc1
na	na	k7c4
ovocné	ovocný	k2eAgFnPc4d1
šťávy	šťáva	k1gFnPc4
<g/>
,	,	kIx,
nazvaná	nazvaný	k2eAgFnSc1d1
podle	podle	k7c2
řeky	řeka	k1gFnSc2
Alfeios	Alfeios	k1gInSc1
Rodi	Rode	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
oblasti	oblast	k1gFnSc6
Megalopolis	Megalopolis	k1gFnPc2
se	se	k3xPyFc4
těží	těžet	k5eAaImIp3nS
lignit	lignit	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řeka	řeka	k1gFnSc1
je	být	k5eAaImIp3nS
také	také	k9
vyhledávaná	vyhledávaný	k2eAgFnSc1d1
raftaři	raftař	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s>
Význam	význam	k1gInSc1
v	v	k7c6
kultuře	kultura	k1gFnSc6
</s>
<s>
V	v	k7c6
antickém	antický	k2eAgNnSc6d1
období	období	k1gNnSc6
byla	být	k5eAaImAgFnS
řeka	řeka	k1gFnSc1
uctívána	uctívat	k5eAaImNgFnS
jako	jako	k8xS,k8xC
sídlo	sídlo	k1gNnSc1
stejnojmenného	stejnojmenný	k2eAgMnSc2d1
boha	bůh	k1gMnSc2
<g/>
,	,	kIx,
syna	syn	k1gMnSc2
Okeana	Okean	k1gMnSc2
a	a	k8xC
Téthys	Téthys	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Publius	Publius	k1gMnSc1
Vergilius	Vergilius	k1gMnSc1
Maro	Maro	k1gMnSc1
uvádí	uvádět	k5eAaImIp3nS
legendu	legenda	k1gFnSc4
<g/>
,	,	kIx,
podle	podle	k7c2
níž	jenž	k3xRgFnSc2
se	se	k3xPyFc4
Alfeios	Alfeios	k1gMnSc1
na	na	k7c6
lovu	lov	k1gInSc6
zamiloval	zamilovat	k5eAaPmAgMnS
do	do	k7c2
nymfy	nymfa	k1gFnSc2
Arethusy	Arethus	k1gInPc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
před	před	k7c7
ním	on	k3xPp3gMnSc7
prchla	prchnout	k5eAaPmAgFnS
podzemní	podzemní	k2eAgFnSc7d1
chodbou	chodba	k1gFnSc7
na	na	k7c4
ostrov	ostrov	k1gInSc4
Ortygia	Ortygium	k1gNnSc2
u	u	k7c2
sicilského	sicilský	k2eAgNnSc2d1
pobřeží	pobřeží	k1gNnSc2
(	(	kIx(
<g/>
řeka	řeka	k1gFnSc1
Alfeios	Alfeiosa	k1gFnPc2
je	být	k5eAaImIp3nS
na	na	k7c6
svém	svůj	k3xOyFgInSc6
horním	horní	k2eAgInSc6d1
toku	tok	k1gInSc6
ponorná	ponorný	k2eAgFnSc1d1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
vedlo	vést	k5eAaImAgNnS
k	k	k7c3
domněnkám	domněnka	k1gFnPc3
<g/>
,	,	kIx,
že	že	k8xS
teče	téct	k5eAaImIp3nS
pod	pod	k7c7
dnem	den	k1gInSc7
Jónského	jónský	k2eAgNnSc2d1
moře	moře	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řeka	řeka	k1gFnSc1
je	být	k5eAaImIp3nS
spojena	spojit	k5eAaPmNgFnS
také	také	k9
s	s	k7c7
pověstí	pověst	k1gFnSc7
o	o	k7c6
Héraklovi	Hérakles	k1gMnSc6
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
vyčistil	vyčistit	k5eAaPmAgMnS
chlévy	chlév	k1gInPc1
elidského	elidský	k2eAgMnSc2d1
krále	král	k1gMnSc2
Augiáše	Augiáš	k1gMnSc2
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
do	do	k7c2
nich	on	k3xPp3gMnPc2
svedl	svést	k5eAaPmAgInS
tok	tok	k1gInSc1
řek	řeka	k1gFnPc2
Alfeios	Alfeiosa	k1gFnPc2
a	a	k8xC
Péneios	Péneiosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
rovině	rovina	k1gFnSc6
u	u	k7c2
města	město	k1gNnSc2
Olympie	Olympia	k1gFnSc2
<g/>
,	,	kIx,
ležícího	ležící	k2eAgMnSc2d1
nedaleko	nedaleko	k7c2
Alfeia	Alfeium	k1gNnSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
pravidelně	pravidelně	k6eAd1
konaly	konat	k5eAaImAgFnP
antické	antický	k2eAgFnPc1d1
olympijské	olympijský	k2eAgFnPc1d1
hry	hra	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Alfeios	Alfeiosa	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Water	Watrat	k5eAaPmRp2nS
Database	Databas	k1gMnSc5
</s>
<s>
Britannica	Britannica	k6eAd1
</s>
<s>
Wonder	Wonder	k1gMnSc1
Greece	Greece	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Řecko	Řecko	k1gNnSc1
</s>
