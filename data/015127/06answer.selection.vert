<s desamb="1">
Nejvýznamnějšími	významný	k2eAgInPc7d3
přítoky	přítok	k1gInPc7
jsou	být	k5eAaImIp3nP
Ladonas	Ladonas	k1gMnSc1
a	a	k8xC
Erymanthos	Erymanthos	k1gMnSc1
<g/>
,	,	kIx,
oblast	oblast	k1gFnSc1
okolo	okolo	k7c2
jejich	jejich	k3xOp3gInSc2
soutoku	soutok	k1gInSc2
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
Tripotamia	Tripotamia	k1gFnSc1
(	(	kIx(
<g/>
„	„	k?
<g/>
tři	tři	k4xCgFnPc1
řeky	řeka	k1gFnPc1
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>