<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Klement	Klement	k1gMnSc1	Klement
I.	I.	kA	I.
či	či	k8xC	či
Kliment	Kliment	k1gMnSc1	Kliment
I.	I.	kA	I.
či	či	k8xC	či
Klement	Klement	k1gMnSc1	Klement
Římský	římský	k2eAgMnSc1d1	římský
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
tradici	tradice	k1gFnSc6	tradice
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c2	za
čtvrtého	čtvrtý	k4xOgMnSc2	čtvrtý
římského	římský	k2eAgMnSc2d1	římský
biskupa	biskup	k1gMnSc2	biskup
<g/>
,	,	kIx,	,
a	a	k8xC	a
tedy	tedy	k9	tedy
čtvrtého	čtvrtý	k4xOgMnSc4	čtvrtý
papeže	papež	k1gMnSc4	papež
<g/>
.	.	kIx.	.
</s>
