<p>
<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Klement	Klement	k1gMnSc1	Klement
I.	I.	kA	I.
či	či	k8xC	či
Kliment	Kliment	k1gMnSc1	Kliment
I.	I.	kA	I.
či	či	k8xC	či
Klement	Klement	k1gMnSc1	Klement
Římský	římský	k2eAgMnSc1d1	římský
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
tradici	tradice	k1gFnSc6	tradice
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c2	za
čtvrtého	čtvrtý	k4xOgMnSc2	čtvrtý
římského	římský	k2eAgMnSc2d1	římský
biskupa	biskup	k1gMnSc2	biskup
<g/>
,	,	kIx,	,
a	a	k8xC	a
tedy	tedy	k9	tedy
čtvrtého	čtvrtý	k4xOgMnSc4	čtvrtý
papeže	papež	k1gMnSc4	papež
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
pontifikát	pontifikát	k1gInSc1	pontifikát
se	se	k3xPyFc4	se
datuje	datovat	k5eAaImIp3nS	datovat
do	do	k7c2	do
let	léto	k1gNnPc2	léto
88	[number]	k4	88
<g/>
/	/	kIx~	/
<g/>
92	[number]	k4	92
–	–	k?	–
97	[number]	k4	97
<g/>
/	/	kIx~	/
<g/>
101	[number]	k4	101
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ostatky	ostatek	k1gInPc1	ostatek
svatého	svatý	k2eAgMnSc2d1	svatý
Klimenta	Kliment	k1gMnSc2	Kliment
údajně	údajně	k6eAd1	údajně
našli	najít	k5eAaPmAgMnP	najít
svatí	svatý	k1gMnPc1	svatý
Cyril	Cyril	k1gMnSc1	Cyril
a	a	k8xC	a
Metoděj	Metoděj	k1gMnSc1	Metoděj
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
spojen	spojit	k5eAaPmNgInS	spojit
s	s	k7c7	s
počátky	počátek	k1gInPc7	počátek
křesťanství	křesťanství	k1gNnSc2	křesťanství
na	na	k7c6	na
Velké	velký	k2eAgFnSc6d1	velká
Moravě	Morava	k1gFnSc6	Morava
i	i	k8xC	i
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Klement	Klement	k1gMnSc1	Klement
I.	I.	kA	I.
<g/>
,	,	kIx,	,
nazývaný	nazývaný	k2eAgMnSc1d1	nazývaný
také	také	k9	také
Klement	Klement	k1gMnSc1	Klement
Římský	římský	k2eAgMnSc1d1	římský
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Klementa	Klement	k1gMnSc2	Klement
Alexandrijského	alexandrijský	k2eAgMnSc2d1	alexandrijský
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
křesťanské	křesťanský	k2eAgFnSc6d1	křesťanská
tradici	tradice	k1gFnSc6	tradice
uváděn	uvádět	k5eAaImNgMnS	uvádět
jako	jako	k9	jako
čtvrtý	čtvrtý	k4xOgMnSc1	čtvrtý
římský	římský	k2eAgMnSc1d1	římský
papež	papež	k1gMnSc1	papež
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
znalci	znalec	k1gMnPc1	znalec
církevních	církevní	k2eAgFnPc2d1	církevní
dějin	dějiny	k1gFnPc2	dějiny
jej	on	k3xPp3gMnSc4	on
však	však	k9	však
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
přímého	přímý	k2eAgMnSc4d1	přímý
nástupce	nástupce	k1gMnSc4	nástupce
svatého	svatý	k2eAgMnSc4d1	svatý
Petra	Petr	k1gMnSc4	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Rozpor	rozpor	k1gInSc1	rozpor
je	být	k5eAaImIp3nS	být
vysvětlován	vysvětlovat	k5eAaImNgInS	vysvětlovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
byl	být	k5eAaImAgMnS	být
svatý	svatý	k2eAgMnSc1d1	svatý
Petr	Petr	k1gMnSc1	Petr
na	na	k7c6	na
cestách	cesta	k1gFnPc6	cesta
<g/>
,	,	kIx,	,
spravovali	spravovat	k5eAaImAgMnP	spravovat
římskou	římský	k2eAgFnSc4d1	římská
církev	církev	k1gFnSc4	církev
Linus	Linus	k1gMnSc1	Linus
a	a	k8xC	a
Anaklét	Anaklét	k1gMnSc1	Anaklét
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
svatého	svatý	k2eAgMnSc2d1	svatý
Petra	Petr	k1gMnSc2	Petr
měl	mít	k5eAaImAgInS	mít
spravovat	spravovat	k5eAaImF	spravovat
církev	církev	k1gFnSc1	církev
Klement	Klement	k1gMnSc1	Klement
<g/>
;	;	kIx,	;
aby	aby	kYmCp3nS	aby
zamezil	zamezit	k5eAaPmAgMnS	zamezit
rozkolu	rozkol	k1gInSc3	rozkol
v	v	k7c6	v
církvi	církev	k1gFnSc6	církev
<g/>
,	,	kIx,	,
ustoupil	ustoupit	k5eAaPmAgMnS	ustoupit
Linovi	Lina	k1gMnSc3	Lina
a	a	k8xC	a
Anaklétovi	Anaklét	k1gMnSc3	Anaklét
a	a	k8xC	a
římským	římský	k2eAgMnSc7d1	římský
biskupem	biskup	k1gMnSc7	biskup
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
až	až	k9	až
po	po	k7c6	po
jejich	jejich	k3xOp3gFnSc6	jejich
smrti	smrt	k1gFnSc6	smrt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Starověký	starověký	k2eAgMnSc1d1	starověký
dějepisec	dějepisec	k1gMnSc1	dějepisec
Eusebios	Eusebios	k1gMnSc1	Eusebios
z	z	k7c2	z
Kaisareie	Kaisareie	k1gFnSc2	Kaisareie
Klementa	Klement	k1gMnSc2	Klement
I.	I.	kA	I.
ztotožnil	ztotožnit	k5eAaPmAgInS	ztotožnit
s	s	k7c7	s
Klementem	Klement	k1gMnSc7	Klement
<g/>
,	,	kIx,	,
o	o	k7c6	o
němž	jenž	k3xRgInSc6	jenž
píše	psát	k5eAaImIp3nS	psát
svatý	svatý	k1gMnSc1	svatý
Pavel	Pavel	k1gMnSc1	Pavel
v	v	k7c6	v
listě	list	k1gInSc6	list
Filipským	Filipský	k2eAgMnSc7d1	Filipský
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
Nového	Nového	k2eAgInSc2d1	Nového
zákona	zákon	k1gInSc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tohoto	tento	k3xDgNnSc2	tento
podání	podání	k1gNnSc2	podání
Klement	Klement	k1gMnSc1	Klement
pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
města	město	k1gNnSc2	město
Filippi	Filipp	k1gFnSc2	Filipp
v	v	k7c6	v
Makedonii	Makedonie	k1gFnSc6	Makedonie
a	a	k8xC	a
na	na	k7c4	na
křesťanskou	křesťanský	k2eAgFnSc4d1	křesťanská
víru	víra	k1gFnSc4	víra
jej	on	k3xPp3gInSc2	on
obrátil	obrátit	k5eAaPmAgMnS	obrátit
svatý	svatý	k1gMnSc1	svatý
Pavel	Pavel	k1gMnSc1	Pavel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gNnSc7	jeho
jménem	jméno	k1gNnSc7	jméno
označujeme	označovat	k5eAaImIp1nP	označovat
třetího	třetí	k4xOgMnSc4	třetí
nástupce	nástupce	k1gMnSc4	nástupce
apoštola	apoštol	k1gMnSc4	apoštol
Petra	Petr	k1gMnSc4	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
tedy	tedy	k9	tedy
k	k	k7c3	k
nejstarším	starý	k2eAgMnPc3d3	nejstarší
církevním	církevní	k2eAgMnPc3d1	církevní
otcům	otec	k1gMnPc3	otec
<g/>
,	,	kIx,	,
apoštolským	apoštolský	k2eAgInSc7d1	apoštolský
<g/>
.	.	kIx.	.
</s>
<s>
Setkáváme	setkávat	k5eAaImIp1nP	setkávat
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
i	i	k9	i
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
jiného	jiný	k2eAgMnSc2d1	jiný
autora	autor	k1gMnSc2	autor
<g/>
,	,	kIx,	,
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
Pastýři	pastýř	k1gMnPc1	pastýř
Hermově	Hermův	k2eAgMnSc6d1	Hermův
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
Klement	Klement	k1gMnSc1	Klement
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
jako	jako	k9	jako
biskup	biskup	k1gInSc4	biskup
zplnomocněný	zplnomocněný	k2eAgInSc4d1	zplnomocněný
k	k	k7c3	k
zasílání	zasílání	k1gNnSc3	zasílání
listů	list	k1gInPc2	list
církevním	církevní	k2eAgFnPc3d1	církevní
obcím	obec	k1gFnPc3	obec
mimo	mimo	k7c4	mimo
Řím	Řím	k1gInSc4	Řím
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Působení	působení	k1gNnSc2	působení
===	===	k?	===
</s>
</p>
<p>
<s>
O	o	k7c6	o
jeho	jeho	k3xOp3gNnSc6	jeho
papežském	papežský	k2eAgNnSc6d1	papežské
působení	působení	k1gNnSc6	působení
není	být	k5eNaImIp3nS	být
mnoho	mnoho	k6eAd1	mnoho
známo	znám	k2eAgNnSc1d1	známo
<g/>
.	.	kIx.	.
</s>
<s>
Údajně	údajně	k6eAd1	údajně
ustanovil	ustanovit	k5eAaPmAgMnS	ustanovit
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
římském	římský	k2eAgInSc6d1	římský
okrese	okres	k1gInSc6	okres
sedm	sedm	k4xCc4	sedm
zapisovatelů	zapisovatel	k1gMnPc2	zapisovatel
(	(	kIx(	(
<g/>
notářů	notář	k1gMnPc2	notář
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
měli	mít	k5eAaImAgMnP	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
zaznamenávat	zaznamenávat	k5eAaImF	zaznamenávat
skutky	skutek	k1gInPc4	skutek
křesťanských	křesťanský	k2eAgMnPc2d1	křesťanský
mučedníků	mučedník	k1gMnPc2	mučedník
<g/>
.	.	kIx.	.
</s>
<s>
Svými	svůj	k3xOyFgInPc7	svůj
dopisy	dopis	k1gInPc7	dopis
církvím	církev	k1gFnPc3	církev
mimo	mimo	k7c4	mimo
Řím	Řím	k1gInSc4	Řím
se	se	k3xPyFc4	se
zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
o	o	k7c4	o
zachování	zachování	k1gNnSc4	zachování
jednoty	jednota	k1gFnSc2	jednota
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Známý	známý	k2eAgMnSc1d1	známý
je	být	k5eAaImIp3nS	být
List	list	k1gInSc1	list
ke	k	k7c3	k
Korintským	korintský	k2eAgFnPc3d1	Korintská
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
jeho	jeho	k3xOp3gNnSc1	jeho
autorství	autorství	k1gNnSc1	autorství
bylo	být	k5eAaImAgNnS	být
zpochybněno	zpochybnit	k5eAaPmNgNnS	zpochybnit
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
se	se	k3xPyFc4	se
zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
o	o	k7c4	o
šíření	šíření	k1gNnSc4	šíření
křesťanství	křesťanství	k1gNnSc2	křesťanství
vysíláním	vysílání	k1gNnSc7	vysílání
hlasatelů	hlasatel	k1gMnPc2	hlasatel
víry	víra	k1gFnSc2	víra
i	i	k8xC	i
do	do	k7c2	do
vzdálených	vzdálený	k2eAgFnPc2d1	vzdálená
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Smrt	smrt	k1gFnSc1	smrt
===	===	k?	===
</s>
</p>
<p>
<s>
Legendy	legenda	k1gFnPc1	legenda
jsou	být	k5eAaImIp3nP	být
značně	značně	k6eAd1	značně
rozporné	rozporný	k2eAgInPc1d1	rozporný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
první	první	k4xOgFnSc2	první
legendy	legenda	k1gFnSc2	legenda
císař	císař	k1gMnSc1	císař
Trajanus	Trajanus	k1gMnSc1	Trajanus
poslal	poslat	k5eAaPmAgMnS	poslat
Klementa	Klement	k1gMnSc4	Klement
do	do	k7c2	do
vyhnanství	vyhnanství	k1gNnSc2	vyhnanství
na	na	k7c4	na
Chersonésos	Chersonésos	k1gInSc4	Chersonésos
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgInSc4d1	dnešní
poloostrov	poloostrov	k1gInSc4	poloostrov
Krym	Krym	k1gInSc4	Krym
v	v	k7c6	v
Černém	černý	k2eAgNnSc6d1	černé
moři	moře	k1gNnSc6	moře
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Klement	Klement	k1gMnSc1	Klement
ani	ani	k8xC	ani
tam	tam	k6eAd1	tam
nepřestal	přestat	k5eNaPmAgInS	přestat
hlásat	hlásat	k5eAaImF	hlásat
křesťanskou	křesťanský	k2eAgFnSc4d1	křesťanská
víru	víra	k1gFnSc4	víra
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
císařův	císařův	k2eAgInSc4d1	císařův
rozkaz	rozkaz	k1gInSc4	rozkaz
v	v	k7c6	v
letech	let	k1gInPc6	let
100	[number]	k4	100
<g/>
–	–	k?	–
<g/>
101	[number]	k4	101
vhozen	vhodit	k5eAaPmNgInS	vhodit
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
s	s	k7c7	s
kotvou	kotva	k1gFnSc7	kotva
uvázanou	uvázaná	k1gFnSc7	uvázaná
okolo	okolo	k7c2	okolo
krku	krk	k1gInSc2	krk
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
je	být	k5eAaImIp3nS	být
časté	častý	k2eAgNnSc1d1	časté
zobrazení	zobrazení	k1gNnSc1	zobrazení
svatého	svatý	k2eAgMnSc2d1	svatý
Klementa	Klement	k1gMnSc2	Klement
s	s	k7c7	s
kotvou	kotva	k1gFnSc7	kotva
<g/>
.	.	kIx.	.
</s>
<s>
Krymští	krymský	k2eAgMnPc1d1	krymský
křesťané	křesťan	k1gMnPc1	křesťan
jeho	jeho	k3xOp3gNnSc4	jeho
tělo	tělo	k1gNnSc4	tělo
nalezli	nalézt	k5eAaBmAgMnP	nalézt
<g/>
,	,	kIx,	,
pohřbili	pohřbít	k5eAaPmAgMnP	pohřbít
a	a	k8xC	a
nad	nad	k7c7	nad
jeho	jeho	k3xOp3gInSc7	jeho
hrobem	hrob	k1gInSc7	hrob
vystavěli	vystavět	k5eAaPmAgMnP	vystavět
kapli	kaple	k1gFnSc4	kaple
<g/>
.	.	kIx.	.
</s>
<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Konstantin	Konstantin	k1gMnSc1	Konstantin
(	(	kIx(	(
<g/>
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xC	jako
slovanský	slovanský	k2eAgMnSc1d1	slovanský
věrozvěst	věrozvěst	k1gMnSc1	věrozvěst
Cyril	Cyril	k1gMnSc1	Cyril
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
861	[number]	k4	861
nalezl	naleznout	k5eAaPmAgInS	naleznout
nejen	nejen	k6eAd1	nejen
onu	onen	k3xDgFnSc4	onen
kotvu	kotva	k1gFnSc4	kotva
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
ostatky	ostatek	k1gInPc4	ostatek
<g/>
.	.	kIx.	.
</s>
<s>
Roger	Roger	k1gMnSc1	Roger
<g/>
,	,	kIx,	,
biskup	biskup	k1gMnSc1	biskup
chalonský	chalonský	k2eAgMnSc1d1	chalonský
<g/>
,	,	kIx,	,
podal	podat	k5eAaPmAgMnS	podat
zprávu	zpráva	k1gFnSc4	zpráva
<g/>
,	,	kIx,	,
že	že	k8xS	že
když	když	k8xS	když
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1048	[number]	k4	1048
navštívil	navštívit	k5eAaPmAgInS	navštívit
Kyjev	Kyjev	k1gInSc1	Kyjev
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
dojednal	dojednat	k5eAaPmAgInS	dojednat
sňatek	sňatek	k1gInSc1	sňatek
Jindřicha	Jindřich	k1gMnSc4	Jindřich
I.	I.	kA	I.
s	s	k7c7	s
Annou	Anna	k1gFnSc7	Anna
Kyjevskou	kyjevský	k2eAgFnSc7d1	Kyjevská
<g/>
,	,	kIx,	,
ukazoval	ukazovat	k5eAaImAgMnS	ukazovat
mu	on	k3xPp3gNnSc3	on
car	car	k1gMnSc1	car
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Moudrý	moudrý	k2eAgMnSc1d1	moudrý
lebku	lebka	k1gFnSc4	lebka
svatého	svatý	k2eAgMnSc2d1	svatý
Klementa	Klement	k1gMnSc2	Klement
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
získal	získat	k5eAaPmAgInS	získat
při	při	k7c6	při
válečném	válečný	k2eAgNnSc6d1	válečné
tažení	tažení	k1gNnSc6	tažení
na	na	k7c4	na
Krym	Krym	k1gInSc4	Krym
<g/>
.	.	kIx.	.
<g/>
Druhá	druhý	k4xOgFnSc1	druhý
verze	verze	k1gFnSc1	verze
legendy	legenda	k1gFnSc2	legenda
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
Klement	Klement	k1gMnSc1	Klement
v	v	k7c6	v
roce	rok	k1gInSc6	rok
100	[number]	k4	100
nebyl	být	k5eNaImAgInS	být
utopen	utopit	k5eAaPmNgInS	utopit
v	v	k7c6	v
moři	moře	k1gNnSc6	moře
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
řece	řeka	k1gFnSc6	řeka
Tibeře	Tibera	k1gFnSc6	Tibera
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Třetí	třetí	k4xOgFnSc1	třetí
verze	verze	k1gFnSc1	verze
píše	psát	k5eAaImIp3nS	psát
<g/>
,	,	kIx,	,
že	že	k8xS	že
Klement	Klement	k1gMnSc1	Klement
zemřel	zemřít	k5eAaPmAgMnS	zemřít
přirozenou	přirozený	k2eAgFnSc7d1	přirozená
smrtí	smrt	k1gFnSc7	smrt
sešlostí	sešlost	k1gFnSc7	sešlost
stářím	stáří	k1gNnSc7	stáří
<g/>
.	.	kIx.	.
<g/>
Křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
církve	církev	k1gFnSc2	církev
Západu	západ	k1gInSc2	západ
i	i	k8xC	i
Východu	východ	k1gInSc2	východ
jej	on	k3xPp3gInSc4	on
všeobecně	všeobecně	k6eAd1	všeobecně
za	za	k7c2	za
mučedníka	mučedník	k1gMnSc2	mučedník
považují	považovat	k5eAaImIp3nP	považovat
i	i	k9	i
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
nebyl	být	k5eNaImAgInS	být
umučen	umučit	k5eAaPmNgInS	umučit
k	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
<g/>
,	,	kIx,	,
stačí	stačit	k5eAaBmIp3nS	stačit
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c4	za
křesťanskou	křesťanský	k2eAgFnSc4d1	křesťanská
víru	víra	k1gFnSc4	víra
trpěl	trpět	k5eAaImAgMnS	trpět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Spisy	spis	k1gInPc4	spis
==	==	k?	==
</s>
</p>
<p>
<s>
Sv.	sv.	kA	sv.
Klementovi	Klement	k1gMnSc3	Klement
bylo	být	k5eAaImAgNnS	být
připisováno	připisován	k2eAgNnSc1d1	připisováno
autorství	autorství	k1gNnSc1	autorství
velkého	velký	k2eAgNnSc2d1	velké
množství	množství	k1gNnSc2	množství
spisů	spis	k1gInPc2	spis
<g/>
;	;	kIx,	;
nejspíše	nejspíše	k9	nejspíše
je	být	k5eAaImIp3nS	být
autorem	autor	k1gMnSc7	autor
pouze	pouze	k6eAd1	pouze
jediného	jediný	k2eAgInSc2d1	jediný
<g/>
,	,	kIx,	,
Prvního	první	k4xOgMnSc2	první
listu	list	k1gInSc2	list
Korinťanům	Korinťan	k1gMnPc3	Korinťan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
První	první	k4xOgInSc1	první
list	list	k1gInSc1	list
Korinťanům	Korinťan	k1gMnPc3	Korinťan
===	===	k?	===
</s>
</p>
<p>
<s>
První	první	k4xOgInSc4	první
list	list	k1gInSc4	list
Korinťanům	Korinťan	k1gMnPc3	Korinťan
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Prima	prima	k2eAgFnSc1d1	prima
Clementis	Clementis	k1gFnSc1	Clementis
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
nezaměňovat	zaměňovat	k5eNaImF	zaměňovat
s	s	k7c7	s
Pavlovým	Pavlův	k2eAgInSc7d1	Pavlův
Prvním	první	k4xOgInSc7	první
listem	list	k1gInSc7	list
Korintským	korintský	k2eAgInSc7d1	korintský
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jediný	jediný	k2eAgInSc1d1	jediný
spis	spis	k1gInSc1	spis
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterého	který	k3yIgInSc2	který
se	se	k3xPyFc4	se
Klementovo	Klementův	k2eAgNnSc1d1	Klementovo
autorství	autorství	k1gNnSc1	autorství
obecně	obecně	k6eAd1	obecně
přijímá	přijímat	k5eAaImIp3nS	přijímat
<g/>
.	.	kIx.	.
</s>
<s>
Řeší	řešit	k5eAaImIp3nS	řešit
korintský	korintský	k2eAgInSc4d1	korintský
spor	spor	k1gInSc4	spor
o	o	k7c4	o
délku	délka	k1gFnSc4	délka
funkčního	funkční	k2eAgNnSc2d1	funkční
období	období	k1gNnSc2	období
presbyteria	presbyterium	k1gNnSc2	presbyterium
<g/>
,	,	kIx,	,
správního	správní	k2eAgInSc2d1	správní
orgánu	orgán	k1gInSc2	orgán
komunity	komunita	k1gFnSc2	komunita
<g/>
.	.	kIx.	.
</s>
<s>
List	list	k1gInSc1	list
objasňuje	objasňovat	k5eAaImIp3nS	objasňovat
fungování	fungování	k1gNnSc4	fungování
duchovního	duchovní	k2eAgInSc2d1	duchovní
úřadu	úřad	k1gInSc2	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Presbyterský	presbyterský	k2eAgInSc1d1	presbyterský
úřad	úřad	k1gInSc1	úřad
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
apoštolské	apoštolský	k2eAgNnSc4d1	apoštolské
pověření	pověření	k1gNnSc4	pověření
od	od	k7c2	od
Krista	Kristus	k1gMnSc2	Kristus
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
apoštolové	apoštol	k1gMnPc1	apoštol
předali	předat	k5eAaPmAgMnP	předat
presbyterům	presbyter	k1gMnPc3	presbyter
<g/>
.	.	kIx.	.
</s>
<s>
Jediné	jediný	k2eAgNnSc1d1	jediné
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
spadá	spadat	k5eAaImIp3nS	spadat
do	do	k7c2	do
pravomoci	pravomoc	k1gFnSc2	pravomoc
církevní	církevní	k2eAgFnSc2d1	církevní
obce	obec	k1gFnSc2	obec
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ustanovení	ustanovení	k1gNnSc1	ustanovení
presbyterů	presbyter	k1gMnPc2	presbyter
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Následují	následovat	k5eAaImIp3nP	následovat
příklady	příklad	k1gInPc4	příklad
z	z	k7c2	z
Bible	bible	k1gFnSc2	bible
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
vypovídají	vypovídat	k5eAaImIp3nP	vypovídat
o	o	k7c6	o
závisti	závist	k1gFnSc6	závist
<g/>
,	,	kIx,	,
zmatku	zmatek	k1gInSc6	zmatek
<g/>
,	,	kIx,	,
nevraživosti	nevraživost	k1gFnSc6	nevraživost
a	a	k8xC	a
o	o	k7c6	o
následcích	následek	k1gInPc6	následek
takového	takový	k3xDgNnSc2	takový
konání	konání	k1gNnSc2	konání
<g/>
.	.	kIx.	.
</s>
<s>
List	list	k1gInSc1	list
končí	končit	k5eAaImIp3nS	končit
překvapivou	překvapivý	k2eAgFnSc7d1	překvapivá
přímluvou	přímluva	k1gFnSc7	přímluva
za	za	k7c4	za
pohanskou	pohanský	k2eAgFnSc4d1	pohanská
vrchnost	vrchnost	k1gFnSc4	vrchnost
<g/>
.	.	kIx.	.
</s>
<s>
Klement	Klement	k1gMnSc1	Klement
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
pohanské	pohanský	k2eAgFnSc2d1	pohanská
vrchnosti	vrchnost	k1gFnSc2	vrchnost
přisuzuje	přisuzovat	k5eAaImIp3nS	přisuzovat
moc	moc	k6eAd1	moc
od	od	k7c2	od
Boha	bůh	k1gMnSc2	bůh
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohla	moct	k5eAaImAgFnS	moct
soudit	soudit	k5eAaImF	soudit
podle	podle	k7c2	podle
moci	moc	k1gFnSc2	moc
Boží	boží	k2eAgFnSc2d1	boží
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Druhý	druhý	k4xOgInSc1	druhý
Klementův	Klementův	k2eAgInSc1d1	Klementův
list	list	k1gInSc1	list
===	===	k?	===
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
list	list	k1gInSc1	list
je	být	k5eAaImIp3nS	být
nejstarším	starý	k2eAgNnSc7d3	nejstarší
dochovaným	dochovaný	k2eAgNnSc7d1	dochované
křesťanským	křesťanský	k2eAgNnSc7d1	křesťanské
kázáním	kázání	k1gNnSc7	kázání
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Kárá	kárat	k5eAaImIp3nS	kárat
špatné	špatný	k2eAgInPc4d1	špatný
mravy	mrav	k1gInPc4	mrav
a	a	k8xC	a
poukazuje	poukazovat	k5eAaImIp3nS	poukazovat
na	na	k7c4	na
blízký	blízký	k2eAgInSc4d1	blízký
konec	konec	k1gInSc4	konec
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
list	list	k1gInSc1	list
byl	být	k5eAaImAgInS	být
zřejmě	zřejmě	k6eAd1	zřejmě
tradován	tradovat	k5eAaImNgInS	tradovat
s	s	k7c7	s
prvním	první	k4xOgInSc7	první
listem	list	k1gInSc7	list
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
povědomí	povědomí	k1gNnSc2	povědomí
o	o	k7c6	o
Klementově	Klementův	k2eAgNnSc6d1	Klementovo
autorství	autorství	k1gNnSc6	autorství
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
místo	místo	k1gNnSc4	místo
vzniku	vznik	k1gInSc2	vznik
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
Korint	Korint	k1gInSc4	Korint
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
spojení	spojení	k1gNnSc2	spojení
s	s	k7c7	s
prvním	první	k4xOgInSc7	první
listem	list	k1gInSc7	list
<g/>
.	.	kIx.	.
</s>
<s>
Doba	doba	k1gFnSc1	doba
vzniku	vznik	k1gInSc2	vznik
se	se	k3xPyFc4	se
udává	udávat	k5eAaImIp3nS	udávat
kolem	kolem	k7c2	kolem
poloviny	polovina	k1gFnSc2	polovina
2	[number]	k4	2
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Pseudoklementinské	Pseudoklementinský	k2eAgInPc1d1	Pseudoklementinský
listy	list	k1gInPc1	list
===	===	k?	===
</s>
</p>
<p>
<s>
Ad	ad	k7c4	ad
virgines	virgines	k1gInSc4	virgines
–	–	k?	–
Pravidla	pravidlo	k1gNnSc2	pravidlo
askeze	askeze	k1gFnSc2	askeze
<g/>
.	.	kIx.	.
</s>
<s>
Pravidla	pravidlo	k1gNnPc1	pravidlo
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
asketi	asketa	k1gMnPc1	asketa
mají	mít	k5eAaImIp3nP	mít
odpoutávat	odpoutávat	k5eAaImF	odpoutávat
od	od	k7c2	od
druhého	druhý	k4xOgNnSc2	druhý
pohlaví	pohlaví	k1gNnSc2	pohlaví
<g/>
.	.	kIx.	.
</s>
<s>
Vznikl	vzniknout	k5eAaPmAgInS	vzniknout
asi	asi	k9	asi
v	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
nebo	nebo	k8xC	nebo
Palestině	Palestina	k1gFnSc3	Palestina
kolem	kolem	k7c2	kolem
poloviny	polovina	k1gFnSc2	polovina
3	[number]	k4	3
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Syrský	syrský	k2eAgInSc1d1	syrský
text	text	k1gInSc1	text
je	být	k5eAaImIp3nS	být
jediný	jediný	k2eAgInSc1d1	jediný
kompletně	kompletně	k6eAd1	kompletně
dochovaný	dochovaný	k2eAgInSc1d1	dochovaný
text	text	k1gInSc1	text
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgInPc1d1	znám
fragmenty	fragment	k1gInPc1	fragment
řeckého	řecký	k2eAgMnSc2d1	řecký
a	a	k8xC	a
koptského	koptský	k2eAgInSc2d1	koptský
překladu	překlad	k1gInSc2	překlad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Pseudoklementina	Pseudoklementin	k2eAgNnPc4d1	Pseudoklementin
===	===	k?	===
</s>
</p>
<p>
<s>
Skupina	skupina	k1gFnSc1	skupina
spisů	spis	k1gInPc2	spis
obsahující	obsahující	k2eAgInPc4d1	obsahující
apokryfní	apokryfní	k2eAgInPc4d1	apokryfní
skutky	skutek	k1gInPc4	skutek
apoštolů	apoštol	k1gMnPc2	apoštol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pisatelem	pisatel	k1gMnSc7	pisatel
textu	text	k1gInSc2	text
o	o	k7c6	o
Petrově	Petrův	k2eAgFnSc6d1	Petrova
cestě	cesta	k1gFnSc6	cesta
a	a	k8xC	a
jeho	on	k3xPp3gNnSc2	on
konání	konání	k1gNnSc2	konání
(	(	kIx(	(
<g/>
řečech	řeč	k1gFnPc6	řeč
<g/>
,	,	kIx,	,
disputacích	disputace	k1gFnPc6	disputace
<g/>
,	,	kIx,	,
zázracích	zázrak	k1gInPc6	zázrak
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
Klement	Klement	k1gMnSc1	Klement
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
Petrem	Petr	k1gMnSc7	Petr
obrácen	obrátit	k5eAaPmNgMnS	obrátit
<g/>
.	.	kIx.	.
</s>
<s>
Spis	spis	k1gInSc1	spis
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
dvacet	dvacet	k4xCc4	dvacet
pseudoklementinských	pseudoklementinský	k2eAgFnPc2d1	pseudoklementinský
Homilií	homilie	k1gFnPc2	homilie
pojmenovaných	pojmenovaný	k2eAgFnPc2d1	pojmenovaná
podle	podle	k7c2	podle
Petrových	Petrových	k2eAgNnSc2d1	Petrových
kázání	kázání	k1gNnSc2	kázání
(	(	kIx(	(
<g/>
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
heterodoxní	heterodoxní	k2eAgFnSc4d1	heterodoxní
teologii	teologie	k1gFnSc4	teologie
původem	původ	k1gInSc7	původ
v	v	k7c6	v
židokřesťanství	židokřesťanství	k1gNnSc6	židokřesťanství
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
tu	tu	k6eAd1	tu
deset	deset	k4xCc1	deset
knih	kniha	k1gFnPc2	kniha
známých	známý	k1gMnPc2	známý
jako	jako	k8xS	jako
Recognitiones	Recognitionesa	k1gFnPc2	Recognitionesa
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc4	ten
vyprávění	vyprávění	k1gNnSc4	vyprávění
o	o	k7c6	o
osudech	osud	k1gInPc6	osud
Klementovy	Klementův	k2eAgFnSc2d1	Klementova
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
letech	léto	k1gNnPc6	léto
rozloučení	rozloučení	k1gNnSc2	rozloučení
opět	opět	k6eAd1	opět
shromážděna	shromáždit	k5eAaPmNgFnS	shromáždit
Petrem	Petr	k1gMnSc7	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Dochovaly	dochovat	k5eAaPmAgFnP	dochovat
se	se	k3xPyFc4	se
dva	dva	k4xCgInPc4	dva
řecké	řecký	k2eAgInPc4d1	řecký
a	a	k8xC	a
dva	dva	k4xCgInPc4	dva
arabské	arabský	k2eAgInPc4d1	arabský
texty	text	k1gInPc4	text
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
spíše	spíše	k9	spíše
výtahy	výtah	k1gInPc1	výtah
z	z	k7c2	z
původního	původní	k2eAgInSc2d1	původní
textu	text	k1gInSc2	text
<g/>
.	.	kIx.	.
</s>
<s>
Doba	doba	k1gFnSc1	doba
sepsání	sepsání	k1gNnSc2	sepsání
spisu	spis	k1gInSc2	spis
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
kolem	kolem	k7c2	kolem
poloviny	polovina	k1gFnSc2	polovina
4	[number]	k4	4
<g/>
.	.	kIx.	.
<g/>
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
text	text	k1gInSc1	text
Homilie	homilie	k1gFnSc2	homilie
je	být	k5eAaImIp3nS	být
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
židokřesťanské	židokřesťanský	k2eAgFnSc3d1	židokřesťanská
povaze	povaha	k1gFnSc3	povaha
starší	starší	k1gMnSc1	starší
než	než	k8xS	než
Recognitiones	Recognitiones	k1gMnSc1	Recognitiones
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Úcta	úcta	k1gFnSc1	úcta
a	a	k8xC	a
ikonografie	ikonografie	k1gFnSc1	ikonografie
==	==	k?	==
</s>
</p>
<p>
<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Klement	Klement	k1gMnSc1	Klement
bývá	bývat	k5eAaImIp3nS	bývat
zobrazován	zobrazován	k2eAgMnSc1d1	zobrazován
jako	jako	k8xC	jako
Říman	Říman	k1gMnSc1	Říman
v	v	k7c6	v
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
tunice	tunika	k1gFnSc6	tunika
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInPc4	jeho
atributy	atribut	k1gInPc4	atribut
jsou	být	k5eAaImIp3nP	být
kotva	kotva	k1gFnSc1	kotva
a	a	k8xC	a
ryba	ryba	k1gFnSc1	ryba
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
mlýnský	mlýnský	k2eAgInSc1d1	mlýnský
kamen	kamna	k1gNnPc2	kamna
na	na	k7c6	na
krku	krk	k1gInSc6	krk
<g/>
,	,	kIx,	,
klíče	klíč	k1gInSc2	klíč
nebo	nebo	k8xC	nebo
kniha	kniha	k1gFnSc1	kniha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Klement	Klement	k1gMnSc1	Klement
je	být	k5eAaImIp3nS	být
také	také	k9	také
postavou	postava	k1gFnSc7	postava
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
romance	romance	k1gFnSc2	romance
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
dochovala	dochovat	k5eAaPmAgFnS	dochovat
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
různých	různý	k2eAgFnPc6d1	různá
verzích	verze	k1gFnPc6	verze
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
této	tento	k3xDgFnSc2	tento
legendy	legenda	k1gFnSc2	legenda
je	být	k5eAaImIp3nS	být
Klement	Klement	k1gMnSc1	Klement
ztotožňován	ztotožňován	k2eAgMnSc1d1	ztotožňován
se	s	k7c7	s
synovcem	synovec	k1gMnSc7	synovec
císaře	císař	k1gMnSc2	císař
Domitiana	Domitian	k1gMnSc2	Domitian
<g/>
,	,	kIx,	,
T.	T.	kA	T.
Flaviem	Flavius	k1gMnSc7	Flavius
Clemensem	Clemens	k1gMnSc7	Clemens
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Svátek	svátek	k1gInSc4	svátek
svatého	svatý	k2eAgMnSc2d1	svatý
Klementa	Klement	k1gMnSc2	Klement
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
církevního	církevní	k2eAgInSc2d1	církevní
kalendáře	kalendář	k1gInSc2	kalendář
23	[number]	k4	23
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
<g/>
.	.	kIx.	.
</s>
<s>
Českou	český	k2eAgFnSc7d1	Česká
obdobou	obdoba	k1gFnSc7	obdoba
jména	jméno	k1gNnSc2	jméno
Klement	Klement	k1gMnSc1	Klement
je	být	k5eAaImIp3nS	být
Klimeš	Klimeš	k1gMnSc1	Klimeš
<g/>
,	,	kIx,	,
ve	v	k7c6	v
staročeské	staročeský	k2eAgFnSc6d1	staročeská
podobě	podoba	k1gFnSc6	podoba
i	i	k9	i
Těšek	Těšek	k6eAd1	Těšek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Relikvie	relikvie	k1gFnSc2	relikvie
===	===	k?	===
</s>
</p>
<p>
<s>
Klementovy	Klementův	k2eAgInPc4d1	Klementův
ostatky	ostatek	k1gInPc4	ostatek
přinesli	přinést	k5eAaPmAgMnP	přinést
do	do	k7c2	do
Českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
údajně	údajně	k6eAd1	údajně
již	již	k6eAd1	již
svatí	svatý	k2eAgMnPc1d1	svatý
Cyril	Cyril	k1gMnSc1	Cyril
a	a	k8xC	a
Metoděj	Metoděj	k1gMnSc1	Metoděj
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c4	na
Hradisko	hradisko	k1gNnSc4	hradisko
svatého	svatý	k2eAgMnSc2d1	svatý
Klimenta	Kliment	k1gMnSc2	Kliment
<g/>
.	.	kIx.	.
</s>
<s>
Sv.	sv.	kA	sv.
Klementovi	Klement	k1gMnSc3	Klement
bylo	být	k5eAaImAgNnS	být
zasvěceno	zasvěcen	k2eAgNnSc1d1	zasvěceno
několik	několik	k4yIc4	několik
z	z	k7c2	z
nejstarších	starý	k2eAgInPc2d3	nejstarší
kostelů	kostel	k1gInPc2	kostel
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
:	:	kIx,	:
kostel	kostel	k1gInSc4	kostel
na	na	k7c6	na
Levém	levý	k2eAgInSc6d1	levý
Hradci	Hradec	k1gInSc6	Hradec
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Staré	Staré	k2eAgFnSc6d1	Staré
Boleslavi	Boleslaev	k1gFnSc6	Boleslaev
a	a	k8xC	a
trojice	trojice	k1gFnSc1	trojice
svatyní	svatyně	k1gFnPc2	svatyně
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
:	:	kIx,	:
na	na	k7c6	na
Starém	starý	k2eAgNnSc6d1	staré
Městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
na	na	k7c6	na
pozdějším	pozdní	k2eAgNnSc6d2	pozdější
Novém	nový	k2eAgNnSc6d1	nové
Městě	město	k1gNnSc6	město
Pražském	pražský	k2eAgNnSc6d1	Pražské
a	a	k8xC	a
v	v	k7c6	v
Holešovicích	Holešovice	k1gFnPc6	Holešovice
–	–	k?	–
Bubnech	Bubny	k1gInPc6	Bubny
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Výtvarné	výtvarný	k2eAgNnSc1d1	výtvarné
umění	umění	k1gNnSc1	umění
===	===	k?	===
</s>
</p>
<p>
<s>
Samotný	samotný	k2eAgMnSc1d1	samotný
světec	světec	k1gMnSc1	světec
i	i	k8xC	i
jeho	jeho	k3xOp3gFnSc1	jeho
legenda	legenda	k1gFnSc1	legenda
bývají	bývat	k5eAaImIp3nP	bývat
v	v	k7c6	v
evropském	evropský	k2eAgNnSc6d1	Evropské
umění	umění	k1gNnSc6	umění
vyobrazeni	vyobrazen	k2eAgMnPc1d1	vyobrazen
od	od	k7c2	od
raně	raně	k6eAd1	raně
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
nejstarším	starý	k2eAgFnPc3d3	nejstarší
nástěnným	nástěnný	k2eAgFnPc3d1	nástěnná
malbám	malba	k1gFnPc3	malba
patří	patřit	k5eAaImIp3nS	patřit
fresky	freska	k1gFnPc4	freska
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
San	San	k1gFnSc2	San
Clemente	Clement	k1gInSc5	Clement
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
České	český	k2eAgFnSc2d1	Česká
země	zem	k1gFnSc2	zem
==	==	k?	==
</s>
</p>
<p>
<s>
Svatému	svatý	k2eAgMnSc3d1	svatý
Klementovi	Klement	k1gMnSc3	Klement
byl	být	k5eAaImAgInS	být
zasvěcen	zasvěcen	k2eAgInSc1d1	zasvěcen
nejstarší	starý	k2eAgInSc1d3	nejstarší
kostel	kostel	k1gInSc1	kostel
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
,	,	kIx,	,
rotunda	rotunda	k1gFnSc1	rotunda
na	na	k7c6	na
Levém	levý	k2eAgInSc6d1	levý
Hradci	Hradec	k1gInSc6	Hradec
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc7d3	nejstarší
dochovanou	dochovaný	k2eAgFnSc7d1	dochovaná
památkou	památka	k1gFnSc7	památka
jsou	být	k5eAaImIp3nP	být
románské	románský	k2eAgFnPc1d1	románská
malby	malba	k1gFnPc1	malba
v	v	k7c6	v
apsidě	apsida	k1gFnSc6	apsida
kostela	kostel	k1gInSc2	kostel
sv.	sv.	kA	sv.
Klementa	Klement	k1gMnSc2	Klement
ve	v	k7c6	v
Staré	Staré	k2eAgFnSc6d1	Staré
Boleslavi	Boleslaev	k1gFnSc6	Boleslaev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
světci	světec	k1gMnSc6	světec
pojmenována	pojmenovat	k5eAaPmNgFnS	pojmenovat
celá	celý	k2eAgFnSc1d1	celá
jezuitská	jezuitský	k2eAgFnSc1d1	jezuitská
kolej	kolej	k1gFnSc1	kolej
Klementinum	Klementinum	k1gNnSc4	Klementinum
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
staršího	starý	k2eAgInSc2d2	starší
kostela	kostel	k1gInSc2	kostel
sv.	sv.	kA	sv.
Klimenta	Kliment	k1gMnSc2	Kliment
při	při	k7c6	při
někdejším	někdejší	k2eAgInSc6d1	někdejší
klášteře	klášter	k1gInSc6	klášter
dominikánů	dominikán	k1gMnPc2	dominikán
<g/>
.	.	kIx.	.
</s>
<s>
Čtyři	čtyři	k4xCgMnPc1	čtyři
vrcholně	vrcholně	k6eAd1	vrcholně
barokní	barokní	k2eAgFnPc4d1	barokní
fresky	freska	k1gFnPc4	freska
tam	tam	k6eAd1	tam
vymaloval	vymalovat	k5eAaPmAgMnS	vymalovat
Jan	Jan	k1gMnSc1	Jan
Hiebel	Hiebel	k1gMnSc1	Hiebel
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
hlavním	hlavní	k2eAgInSc7d1	hlavní
oltářem	oltář	k1gInSc7	oltář
<g/>
:	:	kIx,	:
Sv.	sv.	kA	sv.
Kliment	Kliment	k1gMnSc1	Kliment
byl	být	k5eAaImAgMnS	být
za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
víru	víra	k1gFnSc4	víra
poslán	poslán	k2eAgInSc4d1	poslán
císařem	císař	k1gMnSc7	císař
Trajánem	Traján	k1gMnSc7	Traján
do	do	k7c2	do
vyhnanství	vyhnanství	k1gNnSc2	vyhnanství
na	na	k7c4	na
Chersonésos	Chersonésos	k1gInSc4	Chersonésos
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nalezl	naleznout	k5eAaPmAgMnS	naleznout
2000	[number]	k4	2000
trpících	trpící	k2eAgMnPc2d1	trpící
křesťanů	křesťan	k1gMnPc2	křesťan
<g/>
,	,	kIx,	,
odsouzených	odsouzený	k1gMnPc2	odsouzený
k	k	k7c3	k
pracím	prací	k2eAgFnPc3d1	prací
v	v	k7c6	v
kamenolomech	kamenolom	k1gInPc6	kamenolom
<g/>
.	.	kIx.	.
</s>
<s>
Modlil	modlil	k1gMnSc1	modlil
se	se	k3xPyFc4	se
za	za	k7c4	za
ně	on	k3xPp3gFnPc4	on
a	a	k8xC	a
dodal	dodat	k5eAaPmAgInS	dodat
také	také	k9	také
jim	on	k3xPp3gMnPc3	on
naději	nadát	k5eAaBmIp1nS	nadát
a	a	k8xC	a
víru	víra	k1gFnSc4	víra
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
vyčerpaní	vyčerpaný	k2eAgMnPc1d1	vyčerpaný
a	a	k8xC	a
žízniví	žíznivět	k5eAaImIp3nP	žíznivět
spatřil	spatřit	k5eAaPmAgMnS	spatřit
na	na	k7c6	na
skále	skála	k1gFnSc6	skála
ve	v	k7c6	v
svatozáři	svatozář	k1gFnSc6	svatozář
Beránka	Beránek	k1gMnSc2	Beránek
božího	boží	k2eAgMnSc2d1	boží
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
jehož	jehož	k3xOyRp3gFnPc7	jehož
nohama	noha	k1gFnPc7	noha
vytryskl	vytrysknout	k5eAaPmAgInS	vytrysknout
pramen	pramen	k1gInSc1	pramen
čerstvé	čerstvý	k2eAgFnSc2d1	čerstvá
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Katané	katan	k1gMnPc1	katan
sv.	sv.	kA	sv.
Klimenta	Kliment	k1gMnSc4	Kliment
odsoudili	odsoudit	k5eAaPmAgMnP	odsoudit
k	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
a	a	k8xC	a
přivázali	přivázat	k5eAaPmAgMnP	přivázat
mu	on	k3xPp3gMnSc3	on
na	na	k7c4	na
krk	krk	k1gInSc4	krk
kotvu	kotva	k1gFnSc4	kotva
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
ho	on	k3xPp3gMnSc4	on
vrhají	vrhat	k5eAaImIp3nP	vrhat
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Andělé	anděl	k1gMnPc1	anděl
adorují	adorovat	k5eAaImIp3nP	adorovat
na	na	k7c6	na
oltáři	oltář	k1gInSc6	oltář
ostatky	ostatek	k1gInPc7	ostatek
sv.	sv.	kA	sv.
Klimenta	Kliment	k1gMnSc2	Kliment
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
již	již	k6eAd1	již
byl	být	k5eAaImAgMnS	být
vzat	vzít	k5eAaPmNgMnS	vzít
do	do	k7c2	do
nebe	nebe	k1gNnSc2	nebe
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
chrámu	chrám	k1gInSc3	chrám
se	se	k3xPyFc4	se
sbíhají	sbíhat	k5eAaImIp3nP	sbíhat
věřící	věřící	k2eAgMnPc1d1	věřící
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
kruchou	krucha	k1gFnSc7	krucha
<g/>
:	:	kIx,	:
Nebeská	nebeský	k2eAgFnSc1d1	nebeská
klenba	klenba	k1gFnSc1	klenba
je	být	k5eAaImIp3nS	být
vyobrazena	vyobrazen	k2eAgFnSc1d1	vyobrazena
s	s	k7c7	s
Nejsvětější	nejsvětější	k2eAgFnSc7d1	nejsvětější
Trojicí	trojice	k1gFnSc7	trojice
uprosřed	uprosřed	k1gInSc1	uprosřed
<g/>
,	,	kIx,	,
adorují	adorovat	k5eAaImIp3nP	adorovat
ji	on	k3xPp3gFnSc4	on
sv.	sv.	kA	sv.
Kliment	Kliment	k1gMnSc1	Kliment
a	a	k8xC	a
sv.	sv.	kA	sv.
Alois	Alois	k1gMnSc1	Alois
z	z	k7c2	z
Gonzagy	Gonzaga	k1gFnSc2	Gonzaga
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
nimi	on	k3xPp3gInPc7	on
andělé	anděl	k1gMnPc1	anděl
drží	držet	k5eAaImIp3nP	držet
rozvinutý	rozvinutý	k2eAgInSc4d1	rozvinutý
rys	rys	k1gInSc4	rys
s	s	k7c7	s
vyobrazením	vyobrazení	k1gNnSc7	vyobrazení
koleje	kolej	k1gFnSc2	kolej
Klementinum	Klementinum	k1gNnSc1	Klementinum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Patrocinia	Patrocinium	k1gNnSc2	Patrocinium
==	==	k?	==
</s>
</p>
<p>
<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Klement	Klement	k1gMnSc1	Klement
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgMnSc7d1	hlavní
patronem	patron	k1gMnSc7	patron
královéhradecké	královéhradecký	k2eAgFnSc2d1	Královéhradecká
diecéze	diecéze	k1gFnSc2	diecéze
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gInSc1	jeho
svátek	svátek	k1gInSc1	svátek
slaví	slavit	k5eAaImIp3nS	slavit
jako	jako	k9	jako
liturgická	liturgický	k2eAgFnSc1d1	liturgická
slavnost	slavnost	k1gFnSc1	slavnost
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
mu	on	k3xPp3gMnSc3	on
zasvěcena	zasvěcen	k2eAgFnSc1d1	zasvěcena
kaple	kaple	k1gFnSc1	kaple
pod	pod	k7c7	pod
Bílou	bílý	k2eAgFnSc7d1	bílá
věží	věž	k1gFnSc7	věž
vedle	vedle	k7c2	vedle
katedrály	katedrála	k1gFnSc2	katedrála
svatého	svatý	k2eAgMnSc2d1	svatý
Ducha	duch	k1gMnSc2	duch
v	v	k7c6	v
Hradci	Hradec	k1gInSc6	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
katedrále	katedrála	k1gFnSc6	katedrála
je	být	k5eAaImIp3nS	být
umístěna	umístěn	k2eAgFnSc1d1	umístěna
i	i	k9	i
jeho	jeho	k3xOp3gFnSc1	jeho
busta	busta	k1gFnSc1	busta
od	od	k7c2	od
akademického	akademický	k2eAgMnSc2d1	akademický
sochaře	sochař	k1gMnSc2	sochař
Vladimíra	Vladimír	k1gMnSc4	Vladimír
Preclíka	Preclík	k1gMnSc4	Preclík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Lexikon	lexikon	k1gInSc1	lexikon
der	drát	k5eAaImRp2nS	drát
christlichen	christlichen	k1gInSc1	christlichen
Ikonographie	Ikonographie	k1gFnPc1	Ikonographie
<g/>
,	,	kIx,	,
ed	ed	k?	ed
<g/>
.	.	kIx.	.
</s>
<s>
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Braunfels	Braunfelsa	k1gFnPc2	Braunfelsa
<g/>
,	,	kIx,	,
Ikonographie	Ikonographie	k1gFnSc1	Ikonographie
der	drát	k5eAaImRp2nS	drát
Heiligen	Heiligen	k1gInSc1	Heiligen
<g/>
,	,	kIx,	,
Band	band	k1gInSc1	band
I.	I.	kA	I.
<g/>
,	,	kIx,	,
Basel-Freiburg-Rom-Wien	Basel-Freiburg-Rom-Wien	k2eAgInSc1d1	Basel-Freiburg-Rom-Wien
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SCHAUBER	SCHAUBER	kA	SCHAUBER
<g/>
,	,	kIx,	,
Vera	Vera	k1gMnSc1	Vera
<g/>
;	;	kIx,	;
SCHINDLER	Schindler	k1gMnSc1	Schindler
<g/>
,	,	kIx,	,
Hanns	Hanns	k1gInSc1	Hanns
Michael	Michaela	k1gFnPc2	Michaela
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
se	s	k7c7	s
svatými	svatá	k1gFnPc7	svatá
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Kostelní	kostelní	k2eAgNnSc1d1	kostelní
Vydří	vydří	k2eAgNnSc1d1	vydří
<g/>
:	:	kIx,	:
Karmelitánské	karmelitánský	k2eAgNnSc1d1	Karmelitánské
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
702	[number]	k4	702
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7192	[number]	k4	7192
<g/>
-	-	kIx~	-
<g/>
304	[number]	k4	304
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VONDRUŠKA	Vondruška	k1gMnSc1	Vondruška
<g/>
,	,	kIx,	,
Isidor	Isidor	k1gMnSc1	Isidor
<g/>
.	.	kIx.	.
</s>
<s>
Životopisy	životopis	k1gInPc1	životopis
svatých	svatá	k1gFnPc2	svatá
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
dějin	dějiny	k1gFnPc2	dějiny
církevních	církevní	k2eAgFnPc2d1	církevní
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Ladislav	Ladislav	k1gMnSc1	Ladislav
Kuncíř	Kuncíř	k1gMnSc1	Kuncíř
<g/>
,	,	kIx,	,
1930	[number]	k4	1930
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Klimenta	Kliment	k1gMnSc2	Kliment
</s>
</p>
<p>
<s>
Hradisko	hradisko	k1gNnSc1	hradisko
svatého	svatý	k2eAgMnSc2d1	svatý
Klimenta	Kliment	k1gMnSc2	Kliment
</s>
</p>
<p>
<s>
Kaple	kaple	k1gFnSc1	kaple
svatého	svatý	k2eAgMnSc2d1	svatý
Klimenta	Kliment	k1gMnSc2	Kliment
(	(	kIx(	(
<g/>
Hradec	Hradec	k1gInSc1	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Klement	Klement	k1gMnSc1	Klement
I.	I.	kA	I.
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Klement	Klement	k1gMnSc1	Klement
I.	I.	kA	I.
</s>
</p>
<p>
<s>
Život	život	k1gInSc1	život
a	a	k8xC	a
dílo	dílo	k1gNnSc1	dílo
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
<g/>
)	)	kIx)	)
</s>
</p>
