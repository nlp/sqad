<s>
Tyrolsko	Tyrolsko	k1gNnSc1	Tyrolsko
či	či	k8xC	či
Tyroly	Tyroly	k1gInPc1	Tyroly
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Tirol	Tirol	k1gInSc1	Tirol
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
spolková	spolkový	k2eAgFnSc1d1	spolková
země	země	k1gFnSc1	země
na	na	k7c6	na
západě	západ	k1gInSc6	západ
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
Metropolí	metropol	k1gFnSc7	metropol
a	a	k8xC	a
současně	současně	k6eAd1	současně
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
Tyrolska	Tyrolsko	k1gNnSc2	Tyrolsko
je	být	k5eAaImIp3nS	být
Innsbruck	Innsbruck	k1gInSc1	Innsbruck
<g/>
,	,	kIx,	,
sídlo	sídlo	k1gNnSc1	sídlo
Tyrolského	tyrolský	k2eAgInSc2d1	tyrolský
zemského	zemský	k2eAgInSc2d1	zemský
sněmu	sněm	k1gInSc2	sněm
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
zemských	zemský	k2eAgFnPc2d1	zemská
institucí	instituce	k1gFnPc2	instituce
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
severní	severní	k2eAgFnSc4d1	severní
část	část	k1gFnSc4	část
původně	původně	k6eAd1	původně
většího	veliký	k2eAgNnSc2d2	veliký
Tyrolska	Tyrolsko	k1gNnSc2	Tyrolsko
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
zbytek	zbytek	k1gInSc1	zbytek
(	(	kIx(	(
<g/>
Jižní	jižní	k2eAgNnSc1d1	jižní
Tyrolsko	Tyrolsko	k1gNnSc1	Tyrolsko
<g/>
)	)	kIx)	)
náleží	náležet	k5eAaImIp3nS	náležet
od	od	k7c2	od
10	[number]	k4	10
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1919	[number]	k4	1919
Itálii	Itálie	k1gFnSc4	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Prakticky	prakticky	k6eAd1	prakticky
celé	celý	k2eAgNnSc4d1	celé
rozlehlé	rozlehlý	k2eAgNnSc4d1	rozlehlé
území	území	k1gNnSc4	území
zabírají	zabírat	k5eAaImIp3nP	zabírat
Alpy	Alpy	k1gFnPc1	Alpy
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
<g/>
:	:	kIx,	:
větší	veliký	k2eAgNnSc4d2	veliký
Severní	severní	k2eAgNnSc4d1	severní
Tyrolsko	Tyrolsko	k1gNnSc4	Tyrolsko
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Nordtirol	Nordtirol	k1gInSc1	Nordtirol
<g/>
)	)	kIx)	)
v	v	k7c6	v
západním	západní	k2eAgInSc6d1	západní
výběžku	výběžek	k1gInSc6	výběžek
Rakouska	Rakousko	k1gNnSc2	Rakousko
a	a	k8xC	a
menší	malý	k2eAgNnSc4d2	menší
Východní	východní	k2eAgNnSc4d1	východní
Tyrolsko	Tyrolsko	k1gNnSc4	Tyrolsko
(	(	kIx(	(
<g/>
Osttirol	Osttirol	k1gInSc1	Osttirol
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
části	část	k1gFnPc1	část
jsou	být	k5eAaImIp3nP	být
navzájem	navzájem	k6eAd1	navzájem
odděleny	oddělit	k5eAaPmNgInP	oddělit
Salzburskem	Salzbursko	k1gNnSc7	Salzbursko
a	a	k8xC	a
Jižním	jižní	k2eAgNnSc7d1	jižní
Tyrolskem	Tyrolsko	k1gNnSc7	Tyrolsko
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
mapa	mapa	k1gFnSc1	mapa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
Východního	východní	k2eAgNnSc2d1	východní
Tyrolska	Tyrolsko	k1gNnSc2	Tyrolsko
s	s	k7c7	s
Korutany	Korutany	k1gInPc7	Korutany
ve	v	k7c6	v
Vysokých	vysoký	k2eAgFnPc6d1	vysoká
Taurách	Taura	k1gFnPc6	Taura
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
vrchol	vrchol	k1gInSc1	vrchol
Rakouska	Rakousko	k1gNnSc2	Rakousko
Grossglockner	Grossglockner	k1gInSc4	Grossglockner
3798	[number]	k4	3798
m	m	kA	m
n.	n.	k?	n.
<g/>
m.	m.	k?	m.
Osídlení	osídlení	k1gNnSc1	osídlení
se	se	k3xPyFc4	se
v	v	k7c6	v
Tyrolsku	Tyrolsko	k1gNnSc6	Tyrolsko
soustředí	soustředit	k5eAaPmIp3nS	soustředit
především	především	k9	především
do	do	k7c2	do
řady	řada	k1gFnSc2	řada
údolí	údolí	k1gNnSc2	údolí
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgNnPc6	jenž
je	být	k5eAaImIp3nS	být
provozováno	provozován	k2eAgNnSc1d1	provozováno
i	i	k9	i
zemědělství	zemědělství	k1gNnSc1	zemědělství
a	a	k8xC	a
chov	chov	k1gInSc1	chov
dobytka	dobytek	k1gInSc2	dobytek
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Tyrolsko	Tyrolsko	k1gNnSc4	Tyrolsko
je	být	k5eAaImIp3nS	být
významný	významný	k2eAgInSc1d1	významný
i	i	k8xC	i
turistický	turistický	k2eAgInSc1d1	turistický
ruch	ruch	k1gInSc1	ruch
<g/>
.	.	kIx.	.
</s>
<s>
Široké	Široké	k2eAgNnSc4d1	Široké
a	a	k8xC	a
hluboké	hluboký	k2eAgNnSc4d1	hluboké
údolí	údolí	k1gNnPc4	údolí
Innu	Innus	k1gInSc2	Innus
tvoří	tvořit	k5eAaImIp3nP	tvořit
přirozenou	přirozený	k2eAgFnSc4d1	přirozená
hranici	hranice	k1gFnSc4	hranice
mezi	mezi	k7c7	mezi
vápencovými	vápencový	k2eAgFnPc7d1	vápencová
Alpami	Alpy	k1gFnPc7	Alpy
na	na	k7c6	na
severu	sever	k1gInSc6	sever
a	a	k8xC	a
rulovými	rulový	k2eAgFnPc7d1	rulová
středními	střední	k2eAgFnPc7d1	střední
Alpami	Alpy	k1gFnPc7	Alpy
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
údolí	údolí	k1gNnSc6	údolí
také	také	k9	také
žije	žít	k5eAaImIp3nS	žít
většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
Tyrolska	Tyrolsko	k1gNnSc2	Tyrolsko
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
si	se	k3xPyFc3	se
vyžádalo	vyžádat	k5eAaPmAgNnS	vyžádat
i	i	k9	i
zvýšený	zvýšený	k2eAgInSc4d1	zvýšený
rozvoj	rozvoj	k1gInSc4	rozvoj
dopravy	doprava	k1gFnSc2	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
zavedena	zavést	k5eAaPmNgFnS	zavést
před	před	k7c7	před
více	hodně	k6eAd2	hodně
než	než	k8xS	než
sto	sto	k4xCgNnSc4	sto
lety	léto	k1gNnPc7	léto
železniční	železniční	k2eAgFnSc1d1	železniční
trať	trať	k1gFnSc1	trať
a	a	k8xC	a
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
dálnice	dálnice	k1gFnSc2	dálnice
A12	A12	k1gFnSc2	A12
(	(	kIx(	(
<g/>
E	E	kA	E
<g/>
60	[number]	k4	60
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Řeka	řeka	k1gFnSc1	řeka
Inn	Inn	k1gFnSc2	Inn
protéká	protékat	k5eAaImIp3nS	protékat
i	i	k9	i
Innsbruckem	Innsbruck	k1gInSc7	Innsbruck
<g/>
.	.	kIx.	.
</s>
<s>
Nejstaršími	starý	k2eAgMnPc7d3	nejstarší
známými	známý	k2eAgMnPc7d1	známý
obyvateli	obyvatel	k1gMnPc7	obyvatel
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Tyrolska	Tyrolsko	k1gNnSc2	Tyrolsko
byli	být	k5eAaImAgMnP	být
Ilyrové	Ilyr	k1gMnPc1	Ilyr
a	a	k8xC	a
Keltové	Kelt	k1gMnPc1	Kelt
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
1	[number]	k4	1
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
dobyli	dobýt	k5eAaPmAgMnP	dobýt
území	území	k1gNnPc1	území
Římané	Říman	k1gMnPc1	Říman
a	a	k8xC	a
začlenili	začlenit	k5eAaPmAgMnP	začlenit
jej	on	k3xPp3gMnSc4	on
do	do	k7c2	do
provincií	provincie	k1gFnPc2	provincie
Raetia	Raetium	k1gNnSc2	Raetium
<g/>
,	,	kIx,	,
Noricum	Noricum	k1gNnSc1	Noricum
a	a	k8xC	a
Venetia	Venetia	k1gFnSc1	Venetia
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
6	[number]	k4	6
<g/>
.	.	kIx.	.
–	–	k?	–
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
pronikli	proniknout	k5eAaPmAgMnP	proniknout
až	až	k9	až
do	do	k7c2	do
údolí	údolí	k1gNnSc2	údolí
Adige	Adig	k1gFnSc2	Adig
k	k	k7c3	k
dnešnímu	dnešní	k2eAgInSc3d1	dnešní
Salornu	Salorn	k1gInSc3	Salorn
Bavoři	Bavor	k1gMnPc1	Bavor
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgNnSc1d1	původní
rétorománské	rétorománský	k2eAgNnSc1d1	rétorománský
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
(	(	kIx(	(
<g/>
Ladinové	Ladinový	k2eAgNnSc1d1	Ladinový
<g/>
)	)	kIx)	)
našlo	najít	k5eAaPmAgNnS	najít
tehdy	tehdy	k6eAd1	tehdy
útočiště	útočiště	k1gNnSc1	útočiště
v	v	k7c6	v
Dolomitech	Dolomity	k1gInPc6	Dolomity
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
získali	získat	k5eAaPmAgMnP	získat
do	do	k7c2	do
správy	správa	k1gFnSc2	správa
od	od	k7c2	od
římskoněmeckých	římskoněmecký	k2eAgMnPc2d1	římskoněmecký
císařů	císař	k1gMnPc2	císař
rozsáhlá	rozsáhlý	k2eAgNnPc4d1	rozsáhlé
území	území	k1gNnPc4	území
kolem	kolem	k7c2	kolem
obchodní	obchodní	k2eAgFnSc2d1	obchodní
cesty	cesta	k1gFnSc2	cesta
přes	přes	k7c4	přes
Brennerský	Brennerský	k2eAgInSc4d1	Brennerský
průsmyk	průsmyk	k1gInSc4	průsmyk
tridentský	tridentský	k2eAgInSc4d1	tridentský
a	a	k8xC	a
brixenský	brixenský	k2eAgInSc4d1	brixenský
biskup	biskup	k1gInSc4	biskup
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
začal	začít	k5eAaPmAgMnS	začít
zemi	zem	k1gFnSc4	zem
pod	pod	k7c4	pod
svou	svůj	k3xOyFgFnSc4	svůj
vládu	vláda	k1gFnSc4	vláda
sjednocovat	sjednocovat	k5eAaImF	sjednocovat
šlechtický	šlechtický	k2eAgInSc4d1	šlechtický
rod	rod	k1gInSc4	rod
Vintschgauerů	Vintschgauer	k1gInPc2	Vintschgauer
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
si	se	k3xPyFc3	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1230	[number]	k4	1230
říkal	říkat	k5eAaImAgMnS	říkat
hrabata	hrabě	k1gNnPc4	hrabě
z	z	k7c2	z
Tirol	Tirola	k1gFnPc2	Tirola
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
rodového	rodový	k2eAgInSc2d1	rodový
hradu	hrad	k1gInSc2	hrad
Tirol	Tirol	k1gInSc1	Tirol
u	u	k7c2	u
Merana	Meran	k1gMnSc2	Meran
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
si	se	k3xPyFc3	se
ve	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vymohl	vymoct	k5eAaPmAgMnS	vymoct
na	na	k7c6	na
tridentských	tridentský	k2eAgInPc6d1	tridentský
a	a	k8xC	a
brixenských	brixenský	k2eAgInPc6d1	brixenský
biskupech	biskup	k1gInPc6	biskup
rozsáhlá	rozsáhlý	k2eAgNnPc4d1	rozsáhlé
léna	léno	k1gNnPc4	léno
s	s	k7c7	s
fojtskými	fojtský	k2eAgNnPc7d1	fojtský
a	a	k8xC	a
hraběcími	hraběcí	k2eAgNnPc7d1	hraběcí
právy	právo	k1gNnPc7	právo
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1363	[number]	k4	1363
zdědili	zdědit	k5eAaPmAgMnP	zdědit
Tyrolsko	Tyrolsko	k1gNnSc4	Tyrolsko
Habsburkové	Habsburk	k1gMnPc1	Habsburk
<g/>
.	.	kIx.	.
</s>
<s>
Tyrolsko	Tyrolsko	k1gNnSc1	Tyrolsko
se	se	k3xPyFc4	se
těšilo	těšit	k5eAaImAgNnS	těšit
větším	veliký	k2eAgFnPc3d2	veliký
výsadám	výsada	k1gFnPc3	výsada
než	než	k8xS	než
ostatní	ostatní	k2eAgFnPc1d1	ostatní
habsburské	habsburský	k2eAgFnPc1d1	habsburská
země	zem	k1gFnPc1	zem
(	(	kIx(	(
<g/>
např.	např.	kA	např.
císař	císař	k1gMnSc1	císař
Maxmilián	Maxmilián	k1gMnSc1	Maxmilián
udělil	udělit	k5eAaPmAgMnS	udělit
obyvatelům	obyvatel	k1gMnPc3	obyvatel
Tyrolska	Tyrolsko	k1gNnSc2	Tyrolsko
právo	právo	k1gNnSc4	právo
nosit	nosit	k5eAaImF	nosit
zbraň	zbraň	k1gFnSc4	zbraň
a	a	k8xC	a
použít	použít	k5eAaPmF	použít
ji	on	k3xPp3gFnSc4	on
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
své	svůj	k3xOyFgFnSc2	svůj
země	zem	k1gFnSc2	zem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1782	[number]	k4	1782
připojil	připojit	k5eAaPmAgMnS	připojit
k	k	k7c3	k
Tyrolsku	Tyrolsko	k1gNnSc6	Tyrolsko
císař	císař	k1gMnSc1	císař
Josef	Josef	k1gMnSc1	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
území	území	k1gNnPc2	území
Vorarlberska	Vorarlbersko	k1gNnSc2	Vorarlbersko
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1801	[number]	k4	1801
bylo	být	k5eAaImAgNnS	být
území	území	k1gNnSc1	území
Tyrolska	Tyrolsko	k1gNnSc2	Tyrolsko
tvořeno	tvořen	k2eAgNnSc4d1	tvořeno
většinou	většinou	k6eAd1	většinou
územím	území	k1gNnSc7	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
spolkové	spolkový	k2eAgFnSc2d1	spolková
země	zem	k1gFnSc2	zem
Tyrolsko	Tyrolsko	k1gNnSc1	Tyrolsko
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
Vorarlberskem	Vorarlbersko	k1gNnSc7	Vorarlbersko
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
celým	celý	k2eAgInSc7d1	celý
tzv.	tzv.	kA	tzv.
Jižním	jižní	k2eAgNnSc7d1	jižní
Tyrolskem	Tyrolsko	k1gNnSc7	Tyrolsko
a	a	k8xC	a
některými	některý	k3yIgFnPc7	některý
částmi	část	k1gFnPc7	část
moderní	moderní	k2eAgFnSc2d1	moderní
italské	italský	k2eAgFnSc2d1	italská
autonomní	autonomní	k2eAgFnSc2d1	autonomní
provincie	provincie	k1gFnSc2	provincie
Trento	Trento	k1gNnSc1	Trento
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1801	[number]	k4	1801
bylo	být	k5eAaImAgNnS	být
k	k	k7c3	k
Tyrolsku	Tyrolsko	k1gNnSc3	Tyrolsko
připojeno	připojen	k2eAgNnSc1d1	připojeno
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
nezávislé	závislý	k2eNgNnSc4d1	nezávislé
biskupství	biskupství	k1gNnSc4	biskupství
Brixen	Brixen	k1gInSc1	Brixen
a	a	k8xC	a
4	[number]	k4	4
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1803	[number]	k4	1803
i	i	k8xC	i
území	území	k1gNnSc4	území
Tridentska	Tridentsk	k1gInSc2	Tridentsk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
napoleonských	napoleonský	k2eAgFnPc6d1	napoleonská
dobách	doba	k1gFnPc6	doba
(	(	kIx(	(
<g/>
1805	[number]	k4	1805
<g/>
–	–	k?	–
<g/>
1814	[number]	k4	1814
<g/>
)	)	kIx)	)
patřilo	patřit	k5eAaImAgNnS	patřit
k	k	k7c3	k
Bavorskému	bavorský	k2eAgNnSc3d1	bavorské
království	království	k1gNnSc3	království
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
rámci	rámec	k1gInSc6	rámec
bylo	být	k5eAaImAgNnS	být
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
mezi	mezi	k7c4	mezi
nově	nově	k6eAd1	nově
vzniklé	vzniklý	k2eAgInPc4d1	vzniklý
kraje	kraj	k1gInPc4	kraj
Innský	Innský	k2eAgInSc1d1	Innský
<g/>
,	,	kIx,	,
Eisacký	Eisacký	k2eAgInSc1d1	Eisacký
a	a	k8xC	a
Adižský	Adižský	k2eAgInSc1d1	Adižský
<g/>
;	;	kIx,	;
Tridentsko	Tridentsko	k1gNnSc1	Tridentsko
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
po	po	k7c6	po
místním	místní	k2eAgInSc6d1	místní
povstání	povstání	k1gNnSc2	povstání
připojeno	připojit	k5eAaPmNgNnS	připojit
28	[number]	k4	28
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1810	[number]	k4	1810
k	k	k7c3	k
Italskému	italský	k2eAgNnSc3d1	italské
království	království	k1gNnSc3	království
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1809	[number]	k4	1809
vypuklo	vypuknout	k5eAaPmAgNnS	vypuknout
proti	proti	k7c3	proti
bavorské	bavorský	k2eAgFnSc3d1	bavorská
nadvládě	nadvláda	k1gFnSc3	nadvláda
a	a	k8xC	a
drancování	drancování	k1gNnSc3	drancování
země	zem	k1gFnSc2	zem
lidové	lidový	k2eAgNnSc1d1	lidové
povstání	povstání	k1gNnSc1	povstání
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
čele	čelo	k1gNnSc6	čelo
stál	stát	k5eAaImAgInS	stát
Andreas	Andreas	k1gInSc1	Andreas
Hofer	Hofer	k?	Hofer
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1810	[number]	k4	1810
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
italské	italský	k2eAgFnSc3d1	italská
Mantově	Mantova	k1gFnSc3	Mantova
popraven	popraven	k2eAgInSc1d1	popraven
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
vídeňského	vídeňský	k2eAgInSc2d1	vídeňský
kongresu	kongres	k1gInSc2	kongres
1815	[number]	k4	1815
bylo	být	k5eAaImAgNnS	být
Tyrolsko	Tyrolsko	k1gNnSc1	Tyrolsko
opět	opět	k6eAd1	opět
spojeno	spojit	k5eAaPmNgNnS	spojit
s	s	k7c7	s
Tridentskem	Tridentsek	k1gInSc7	Tridentsek
v	v	k7c4	v
jeden	jeden	k4xCgInSc4	jeden
celek	celek	k1gInSc4	celek
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
začleněno	začleněn	k2eAgNnSc1d1	začleněno
do	do	k7c2	do
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
byly	být	k5eAaImAgFnP	být
připojeny	připojen	k2eAgFnPc1d1	připojena
některé	některý	k3yIgFnSc3	některý
okrajové	okrajový	k2eAgFnSc3d1	okrajová
části	část	k1gFnSc3	část
bývalého	bývalý	k2eAgNnSc2d1	bývalé
Salcburského	salcburský	k2eAgNnSc2d1	salcburské
arcibiskupství	arcibiskupství	k1gNnSc2	arcibiskupství
(	(	kIx(	(
<g/>
Brixenské	brixenský	k2eAgNnSc1d1	brixenský
údolí	údolí	k1gNnSc1	údolí
<g/>
,	,	kIx,	,
obec	obec	k1gFnSc1	obec
Matrei	Matrei	k1gNnSc2	Matrei
<g/>
,	,	kIx,	,
údolí	údolí	k1gNnSc2	údolí
Zillertal	Zillertal	k1gFnSc2	Zillertal
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
26	[number]	k4	26
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1861	[number]	k4	1861
bylo	být	k5eAaImAgNnS	být
od	od	k7c2	od
Tyrolska	Tyrolsko	k1gNnSc2	Tyrolsko
odděleno	oddělen	k2eAgNnSc1d1	odděleno
Vorarlbersko	Vorarlbersko	k1gNnSc1	Vorarlbersko
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
samostatnou	samostatný	k2eAgFnSc7d1	samostatná
rakouskou	rakouský	k2eAgFnSc7d1	rakouská
korunní	korunní	k2eAgFnSc7d1	korunní
zemí	zem	k1gFnSc7	zem
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
bylo	být	k5eAaImAgNnS	být
Tridentsko	Tridentsko	k1gNnSc1	Tridentsko
3	[number]	k4	3
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1918	[number]	k4	1918
anektováno	anektovat	k5eAaBmNgNnS	anektovat
Itálií	Itálie	k1gFnSc7	Itálie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
pak	pak	k6eAd1	pak
10	[number]	k4	10
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1919	[number]	k4	1919
získala	získat	k5eAaPmAgFnS	získat
i	i	k9	i
převážně	převážně	k6eAd1	převážně
německojazyčné	německojazyčný	k2eAgNnSc4d1	německojazyčné
Jižní	jižní	k2eAgNnSc4d1	jižní
Tyrolsko	Tyrolsko	k1gNnSc4	Tyrolsko
<g/>
.	.	kIx.	.
</s>
<s>
Zbytek	zbytek	k1gInSc1	zbytek
Tyrolska	Tyrolsko	k1gNnSc2	Tyrolsko
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
spolkovou	spolkový	k2eAgFnSc7d1	spolková
zemí	zem	k1gFnSc7	zem
nově	nově	k6eAd1	nově
vzniklé	vzniklý	k2eAgFnSc2d1	vzniklá
Republiky	republika	k1gFnSc2	republika
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
.	.	kIx.	.
24	[number]	k4	24
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1921	[number]	k4	1921
pak	pak	k6eAd1	pak
obyvatelé	obyvatel	k1gMnPc1	obyvatel
Tyrolska	Tyrolsko	k1gNnSc2	Tyrolsko
v	v	k7c6	v
referendu	referendum	k1gNnSc6	referendum
hlasovali	hlasovat	k5eAaImAgMnP	hlasovat
pro	pro	k7c4	pro
připojení	připojení	k1gNnSc4	připojení
k	k	k7c3	k
Německu	Německo	k1gNnSc3	Německo
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
však	však	k9	však
nebylo	být	k5eNaImAgNnS	být
realizováno	realizován	k2eAgNnSc1d1	realizováno
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1938	[number]	k4	1938
provedlo	provést	k5eAaPmAgNnS	provést
nacistické	nacistický	k2eAgNnSc1d1	nacistické
Německo	Německo	k1gNnSc1	Německo
anšlus	anšlus	k1gInSc1	anšlus
Rakouska	Rakousko	k1gNnSc2	Rakousko
a	a	k8xC	a
Tyrolsko	Tyrolsko	k1gNnSc4	Tyrolsko
se	se	k3xPyFc4	se
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Země	zem	k1gFnSc2	zem
Rakouska	Rakousko	k1gNnSc2	Rakousko
stalo	stát	k5eAaPmAgNnS	stát
součástí	součást	k1gFnSc7	součást
Velkoněmecké	velkoněmecký	k2eAgFnSc2d1	Velkoněmecká
říše	říš	k1gFnSc2	říš
jako	jako	k8xS	jako
Zemské	zemský	k2eAgNnSc4d1	zemské
hejtmanství	hejtmanství	k1gNnSc4	hejtmanství
Tyrolsko	Tyrolsko	k1gNnSc1	Tyrolsko
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
začali	začít	k5eAaPmAgMnP	začít
nacisté	nacista	k1gMnPc1	nacista
provádět	provádět	k5eAaImF	provádět
územní	územní	k2eAgFnPc4d1	územní
změny	změna	k1gFnPc4	změna
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
27	[number]	k4	27
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1938	[number]	k4	1938
bylo	být	k5eAaImAgNnS	být
Východní	východní	k2eAgNnSc1d1	východní
Tyrolsko	Tyrolsko	k1gNnSc1	Tyrolsko
spravováno	spravován	k2eAgNnSc1d1	spravováno
Zemským	zemský	k2eAgNnSc7d1	zemské
hejtmanstvím	hejtmanství	k1gNnSc7	hejtmanství
Korutany	Korutany	k1gInPc1	Korutany
s	s	k7c7	s
nímž	jenž	k3xRgInSc7	jenž
pak	pak	k6eAd1	pak
bylo	být	k5eAaImAgNnS	být
15	[number]	k4	15
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1938	[number]	k4	1938
také	také	k6eAd1	také
spojeno	spojit	k5eAaPmNgNnS	spojit
a	a	k8xC	a
odděleno	oddělit	k5eAaPmNgNnS	oddělit
od	od	k7c2	od
Tyrolska	Tyrolsko	k1gNnSc2	Tyrolsko
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1938	[number]	k4	1938
byla	být	k5eAaImAgFnS	být
od	od	k7c2	od
Tyrolska	Tyrolsko	k1gNnSc2	Tyrolsko
oddělena	oddělit	k5eAaPmNgFnS	oddělit
také	také	k6eAd1	také
obec	obec	k1gFnSc1	obec
Jungholz	Jungholza	k1gFnPc2	Jungholza
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
nacisté	nacista	k1gMnPc1	nacista
připojili	připojit	k5eAaPmAgMnP	připojit
k	k	k7c3	k
Bavorsku	Bavorsko	k1gNnSc3	Bavorsko
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1939	[number]	k4	1939
pak	pak	k6eAd1	pak
byl	být	k5eAaImAgInS	být
zbytek	zbytek	k1gInSc1	zbytek
Tyrolska	Tyrolsko	k1gNnSc2	Tyrolsko
spojen	spojit	k5eAaPmNgInS	spojit
s	s	k7c7	s
téměř	téměř	k6eAd1	téměř
celým	celý	k2eAgNnSc7d1	celé
Vorarlberskem	Vorarlbersko	k1gNnSc7	Vorarlbersko
v	v	k7c4	v
říšskou	říšský	k2eAgFnSc4d1	říšská
župu	župa	k1gFnSc4	župa
Tyrolsko-Vorarlbersko	Tyrolsko-Vorarlbersko	k1gNnSc1	Tyrolsko-Vorarlbersko
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1945	[number]	k4	1945
vstoupily	vstoupit	k5eAaPmAgFnP	vstoupit
do	do	k7c2	do
Innsbrucku	Innsbruck	k1gInSc2	Innsbruck
americké	americký	k2eAgFnSc2d1	americká
vojenské	vojenský	k2eAgFnSc2d1	vojenská
jednotky	jednotka	k1gFnSc2	jednotka
a	a	k8xC	a
Tyrolsko	Tyrolsko	k1gNnSc1	Tyrolsko
v	v	k7c6	v
hranicích	hranice	k1gFnPc6	hranice
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1919	[number]	k4	1919
<g/>
–	–	k?	–
<g/>
1938	[number]	k4	1938
bylo	být	k5eAaImAgNnS	být
opět	opět	k6eAd1	opět
celé	celá	k1gFnSc3	celá
součástí	součást	k1gFnPc2	součást
obnoveného	obnovený	k2eAgNnSc2d1	obnovené
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
rámci	rámec	k1gInSc6	rámec
se	se	k3xPyFc4	se
Severní	severní	k2eAgNnSc1d1	severní
Tyrolsko	Tyrolsko	k1gNnSc1	Tyrolsko
stalo	stát	k5eAaPmAgNnS	stát
součástí	součást	k1gFnSc7	součást
francouzské	francouzský	k2eAgFnSc2d1	francouzská
okupační	okupační	k2eAgFnSc2d1	okupační
zóny	zóna	k1gFnSc2	zóna
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Východní	východní	k2eAgNnSc4d1	východní
Tyrolsko	Tyrolsko	k1gNnSc4	Tyrolsko
součástí	součást	k1gFnPc2	součást
britské	britský	k2eAgFnSc2d1	britská
okupační	okupační	k2eAgFnSc2d1	okupační
zóny	zóna	k1gFnSc2	zóna
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1947	[number]	k4	1947
pak	pak	k6eAd1	pak
bylo	být	k5eAaImAgNnS	být
Východní	východní	k2eAgNnSc1d1	východní
Tyrolsko	Tyrolsko	k1gNnSc1	Tyrolsko
navráceno	navrácen	k2eAgNnSc1d1	navráceno
Tyrolsku	Tyrolsko	k1gNnSc3	Tyrolsko
<g/>
.	.	kIx.	.
</s>
<s>
Tyrolsko	Tyrolsko	k1gNnSc1	Tyrolsko
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
12	[number]	k4	12
647,71	[number]	k4	647,71
km	km	kA	km
<g/>
2	[number]	k4	2
třetí	třetí	k4xOgFnSc7	třetí
nejrozlehlejší	rozlehlý	k2eAgFnSc7d3	nejrozlehlejší
zemí	zem	k1gFnSc7	zem
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
také	také	k9	také
spolková	spolkový	k2eAgFnSc1d1	spolková
země	země	k1gFnSc1	země
s	s	k7c7	s
nejdelšími	dlouhý	k2eAgFnPc7d3	nejdelší
hranicemi	hranice	k1gFnPc7	hranice
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
719	[number]	k4	719
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Svoje	svůj	k3xOyFgFnSc1	svůj
hranice	hranice	k1gFnSc1	hranice
sdílí	sdílet	k5eAaImIp3nS	sdílet
Tyrolsko	Tyrolsko	k1gNnSc4	Tyrolsko
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
<g/>
,	,	kIx,	,
Itálií	Itálie	k1gFnSc7	Itálie
a	a	k8xC	a
z	z	k7c2	z
malé	malý	k2eAgFnSc2d1	malá
části	část	k1gFnSc2	část
také	také	k9	také
se	s	k7c7	s
Švýcarskem	Švýcarsko	k1gNnSc7	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc4d1	celý
povrch	povrch	k1gInSc4	povrch
Tyrolska	Tyrolsko	k1gNnSc2	Tyrolsko
zabírají	zabírat	k5eAaImIp3nP	zabírat
Východní	východní	k2eAgFnPc1d1	východní
Alpy	Alpy	k1gFnPc1	Alpy
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ty	k3xPp2nSc1	ty
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
člení	členit	k5eAaImIp3nS	členit
do	do	k7c2	do
následujících	následující	k2eAgFnPc2d1	následující
horských	horský	k2eAgFnPc2d1	horská
skupin	skupina	k1gFnPc2	skupina
<g/>
:	:	kIx,	:
Allgäuské	Allgäuský	k2eAgFnPc1d1	Allgäuský
Alpy	Alpy	k1gFnPc1	Alpy
Ammergauské	Ammergauský	k2eAgFnSc2d1	Ammergauský
Alpy	alpa	k1gFnSc2	alpa
Gailtalské	Gailtalský	k2eAgFnSc2d1	Gailtalský
Alpy	alpa	k1gFnSc2	alpa
Chiemgauské	Chiemgauský	k2eAgFnSc2d1	Chiemgauský
Alpy	alpa	k1gFnSc2	alpa
Kaisergebirge	Kaisergebirge	k1gFnPc2	Kaisergebirge
Karnské	Karnský	k2eAgFnSc2d1	Karnský
Alpy	alpa	k1gFnSc2	alpa
Karwendel	Karwendlo	k1gNnPc2	Karwendlo
Kitzbühelské	Kitzbühelský	k2eAgFnSc2d1	Kitzbühelský
Alpy	alpa	k1gFnSc2	alpa
Lechtalské	Lechtalský	k2eAgFnSc2d1	Lechtalský
Alpy	alpa	k1gFnSc2	alpa
Ötztalské	Ötztalský	k2eAgFnSc2d1	Ötztalský
Alpy	alpa	k1gFnSc2	alpa
Rofan	Rofan	k1gMnSc1	Rofan
Samnaun	Samnaun	k1gMnSc1	Samnaun
Silvretta	Silvretta	k1gMnSc1	Silvretta
Steinberge	Steinberge	k1gNnSc7	Steinberge
Stubaiské	Stubaiský	k2eAgFnSc2d1	Stubaiský
Alpy	alpa	k1gFnSc2	alpa
Tuxské	Tuxský	k2eAgFnSc2d1	Tuxský
Alpy	alpa	k1gFnSc2	alpa
Villgratenské	Villgratenský	k2eAgFnSc2d1	Villgratenský
hory	hora	k1gFnSc2	hora
Verwall	Verwalla	k1gFnPc2	Verwalla
Vysoké	vysoký	k2eAgFnSc2d1	vysoká
Taury	Taura	k1gFnSc2	Taura
Wetterstein	Wetterstein	k1gMnSc1	Wetterstein
Mieminger	Mieminger	k1gMnSc1	Mieminger
Kette	Kett	k1gInSc5	Kett
Zillertalské	Zillertalský	k2eAgFnSc2d1	Zillertalská
Alpy	alpa	k1gFnSc2	alpa
Innsbruck	Innsbruck	k1gInSc1	Innsbruck
132	[number]	k4	132
236	[number]	k4	236
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g />
.	.	kIx.	.
</s>
<s>
Kufstein	Kufstein	k1gInSc1	Kufstein
18	[number]	k4	18
973	[number]	k4	973
obyvatel	obyvatel	k1gMnPc2	obyvatel
Telfs	Telfs	k1gInSc4	Telfs
15	[number]	k4	15
582	[number]	k4	582
obyvatel	obyvatel	k1gMnPc2	obyvatel
Hall	Hall	k1gMnSc1	Hall
in	in	k?	in
Tirol	Tirol	k1gInSc1	Tirol
13	[number]	k4	13
801	[number]	k4	801
obyvatel	obyvatel	k1gMnPc2	obyvatel
Schwaz	Schwaz	k1gInSc4	Schwaz
13	[number]	k4	13
606	[number]	k4	606
obyvatel	obyvatel	k1gMnSc1	obyvatel
Wörgl	Wörgl	k1gMnSc1	Wörgl
13	[number]	k4	13
537	[number]	k4	537
obyvatel	obyvatel	k1gMnSc1	obyvatel
Lienz	Lienz	k1gMnSc1	Lienz
11	[number]	k4	11
945	[number]	k4	945
obyvatel	obyvatel	k1gMnSc1	obyvatel
Imst	Imst	k1gMnSc1	Imst
10	[number]	k4	10
371	[number]	k4	371
obyvatel	obyvatel	k1gMnPc2	obyvatel
St.	st.	kA	st.
Johann	Johann	k1gMnSc1	Johann
in	in	k?	in
Tirol	Tirol	k1gInSc1	Tirol
9	[number]	k4	9
425	[number]	k4	425
obyvatel	obyvatel	k1gMnPc2	obyvatel
Rum	rum	k1gInSc4	rum
9	[number]	k4	9
063	[number]	k4	063
obyvatel	obyvatel	k1gMnSc1	obyvatel
Kitzbühel	Kitzbühel	k1gMnSc1	Kitzbühel
8	[number]	k4	8
341	[number]	k4	341
obyvatel	obyvatel	k1gMnSc1	obyvatel
Zirl	Zirl	k1gMnSc1	Zirl
8	[number]	k4	8
134	[number]	k4	134
obyvatel	obyvatel	k1gMnPc2	obyvatel
Wattens	Wattens	k1gInSc4	Wattens
7	[number]	k4	7
870	[number]	k4	870
obyvatel	obyvatel	k1gMnSc1	obyvatel
Landeck	Landeck	k1gMnSc1	Landeck
7	[number]	k4	7
764	[number]	k4	764
obyvatel	obyvatel	k1gMnSc1	obyvatel
Jenbach	Jenbach	k1gMnSc1	Jenbach
7	[number]	k4	7
088	[number]	k4	088
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
k	k	k7c3	k
0	[number]	k4	0
<g/>
1.01	[number]	k4	1.01
<g/>
.2017	.2017	k4	.2017
<g/>
)	)	kIx)	)
V	v	k7c6	v
hospodářství	hospodářství	k1gNnSc6	hospodářství
Tyrolska	Tyrolsko	k1gNnSc2	Tyrolsko
je	být	k5eAaImIp3nS	být
důležitý	důležitý	k2eAgInSc4d1	důležitý
průmysl	průmysl	k1gInSc4	průmysl
textilní	textilní	k2eAgInSc4d1	textilní
<g/>
,	,	kIx,	,
dřevozpracující	dřevozpracující	k2eAgInSc4d1	dřevozpracující
<g/>
,	,	kIx,	,
sklářský	sklářský	k2eAgInSc4d1	sklářský
<g/>
,	,	kIx,	,
chemický	chemický	k2eAgInSc4d1	chemický
<g/>
,	,	kIx,	,
strojírenství	strojírenství	k1gNnSc6	strojírenství
a	a	k8xC	a
potravinářský	potravinářský	k2eAgInSc1d1	potravinářský
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
příjmech	příjem	k1gInPc6	příjem
se	se	k3xPyFc4	se
významně	významně	k6eAd1	významně
podílí	podílet	k5eAaImIp3nS	podílet
také	také	k9	také
cestovní	cestovní	k2eAgInSc4d1	cestovní
ruch	ruch	k1gInSc4	ruch
(	(	kIx(	(
<g/>
ročně	ročně	k6eAd1	ročně
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
40	[number]	k4	40
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
přenocování	přenocování	k1gNnSc1	přenocování
návštěvníků	návštěvník	k1gMnPc2	návštěvník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zemědělství	zemědělství	k1gNnSc1	zemědělství
je	být	k5eAaImIp3nS	být
omezeno	omezit	k5eAaPmNgNnS	omezit
vcelku	vcelku	k6eAd1	vcelku
jen	jen	k9	jen
na	na	k7c6	na
údolí	údolí	k1gNnSc6	údolí
Innu	Innus	k1gInSc2	Innus
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vedlejších	vedlejší	k2eAgNnPc6d1	vedlejší
údolích	údolí	k1gNnPc6	údolí
převládá	převládat	k5eAaImIp3nS	převládat
chov	chov	k1gInSc4	chov
dobytka	dobytek	k1gInSc2	dobytek
<g/>
.	.	kIx.	.
</s>
<s>
Reliéf	reliéf	k1gInSc1	reliéf
umožnil	umožnit	k5eAaPmAgInS	umožnit
výstavbu	výstavba	k1gFnSc4	výstavba
velkých	velký	k2eAgFnPc2d1	velká
hydroelektráren	hydroelektrárna	k1gFnPc2	hydroelektrárna
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Prutz-Imst	Prutz-Imst	k1gMnSc1	Prutz-Imst
<g/>
,	,	kIx,	,
Silz-Sellrain	Silz-Sellrain	k1gMnSc1	Silz-Sellrain
<g/>
,	,	kIx,	,
Zillertal	Zillertal	k1gMnSc1	Zillertal
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
kvetoucí	kvetoucí	k2eAgNnSc1d1	kvetoucí
hornictví	hornictví	k1gNnSc1	hornictví
(	(	kIx(	(
<g/>
rudy	ruda	k1gFnPc1	ruda
stříbra	stříbro	k1gNnSc2	stříbro
<g/>
,	,	kIx,	,
olova	olovo	k1gNnSc2	olovo
<g/>
,	,	kIx,	,
sůl	sůl	k1gFnSc1	sůl
<g/>
,	,	kIx,	,
měď	měď	k1gFnSc1	měď
<g/>
)	)	kIx)	)
zcela	zcela	k6eAd1	zcela
zaniklo	zaniknout	k5eAaPmAgNnS	zaniknout
<g/>
.	.	kIx.	.
</s>
<s>
Turismus	turismus	k1gInSc1	turismus
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
důležitou	důležitý	k2eAgFnSc7d1	důležitá
součástí	součást	k1gFnSc7	součást
ekonomiky	ekonomika	k1gFnSc2	ekonomika
Tyrolska	Tyrolsko	k1gNnSc2	Tyrolsko
<g/>
.	.	kIx.	.
</s>
<s>
Představuje	představovat	k5eAaImIp3nS	představovat
zhruba	zhruba	k6eAd1	zhruba
18	[number]	k4	18
%	%	kIx~	%
regionálního	regionální	k2eAgInSc2d1	regionální
hrubého	hrubý	k2eAgInSc2d1	hrubý
domácího	domácí	k2eAgInSc2d1	domácí
produktu	produkt	k1gInSc2	produkt
Tyrolska	Tyrolsko	k1gNnSc2	Tyrolsko
a	a	k8xC	a
zaměstnává	zaměstnávat	k5eAaImIp3nS	zaměstnávat
přes	přes	k7c4	přes
71	[number]	k4	71
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezoně	sezona	k1gFnSc6	sezona
2012	[number]	k4	2012
<g/>
/	/	kIx~	/
<g/>
13	[number]	k4	13
navštívilo	navštívit	k5eAaPmAgNnS	navštívit
Tyrolsko	Tyrolsko	k1gNnSc1	Tyrolsko
10	[number]	k4	10
203	[number]	k4	203
166	[number]	k4	166
turistů	turist	k1gMnPc2	turist
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
pouze	pouze	k6eAd1	pouze
51	[number]	k4	51
%	%	kIx~	%
byli	být	k5eAaImAgMnP	být
Němci	Němec	k1gMnPc1	Němec
a	a	k8xC	a
49	[number]	k4	49
%	%	kIx~	%
zahraniční	zahraniční	k2eAgMnPc1d1	zahraniční
turisté	turist	k1gMnPc1	turist
přicházející	přicházející	k2eAgMnPc1d1	přicházející
především	především	k6eAd1	především
z	z	k7c2	z
Holandska	Holandsko	k1gNnSc2	Holandsko
(	(	kIx(	(
<g/>
10,4	[number]	k4	10,4
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rakouska	Rakousko	k1gNnSc2	Rakousko
(	(	kIx(	(
<g/>
8,6	[number]	k4	8,6
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
(	(	kIx(	(
<g/>
3,7	[number]	k4	3,7
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
Belgie	Belgie	k1gFnSc1	Belgie
(	(	kIx(	(
<g/>
3,4	[number]	k4	3,4
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Turistů	turist	k1gMnPc2	turist
z	z	k7c2	z
ČR	ČR	kA	ČR
bývá	bývat	k5eAaImIp3nS	bývat
ročně	ročně	k6eAd1	ročně
mírně	mírně	k6eAd1	mírně
pod	pod	k7c7	pod
2	[number]	k4	2
%	%	kIx~	%
<g/>
;	;	kIx,	;
ve	v	k7c6	v
zmíněné	zmíněný	k2eAgFnSc6d1	zmíněná
sezoně	sezona	k1gFnSc6	sezona
2012	[number]	k4	2012
<g/>
/	/	kIx~	/
<g/>
13	[number]	k4	13
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
cca	cca	kA	cca
440	[number]	k4	440
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
(	(	kIx(	(
<g/>
1,7	[number]	k4	1,7
%	%	kIx~	%
návštěvníků	návštěvník	k1gMnPc2	návštěvník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zimní	zimní	k2eAgFnSc1d1	zimní
turistická	turistický	k2eAgFnSc1d1	turistická
sezona	sezona	k1gFnSc1	sezona
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
zemi	zem	k1gFnSc4	zem
důležitější	důležitý	k2eAgFnSc4d2	důležitější
než	než	k8xS	než
letní	letní	k2eAgFnSc4d1	letní
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
58	[number]	k4	58
%	%	kIx~	%
ubytování	ubytování	k1gNnSc4	ubytování
v	v	k7c6	v
hotelech	hotel	k1gInPc6	hotel
a	a	k8xC	a
penzionech	penzion	k1gInPc6	penzion
proběhně	proběheň	k1gFnSc2	proběheň
v	v	k7c6	v
zimní	zimní	k2eAgFnSc6d1	zimní
sezoně	sezona	k1gFnSc6	sezona
<g/>
,	,	kIx,	,
42	[number]	k4	42
%	%	kIx~	%
v	v	k7c6	v
letní	letní	k2eAgFnSc6d1	letní
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Tyrolsko	Tyrolsko	k1gNnSc4	Tyrolsko
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Tyrolsko	Tyrolsko	k1gNnSc4	Tyrolsko
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Cestovní	cestovní	k2eAgMnSc1d1	cestovní
a	a	k8xC	a
informační	informační	k2eAgMnSc1d1	informační
průvodce	průvodce	k1gMnSc1	průvodce
Tyrolskem	Tyrolsko	k1gNnSc7	Tyrolsko
(	(	kIx(	(
<g/>
oficiální	oficiální	k2eAgFnSc1d1	oficiální
stránka	stránka	k1gFnSc1	stránka
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
)	)	kIx)	)
</s>
