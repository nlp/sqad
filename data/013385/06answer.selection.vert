<s>
Kopretina	kopretina	k1gFnSc1	kopretina
bílá	bílý	k2eAgFnSc1d1	bílá
(	(	kIx(	(
<g/>
Leucanthemum	Leucanthemum	k1gInSc1	Leucanthemum
vulgare	vulgar	k1gMnSc5	vulgar
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vytrvalá	vytrvalý	k2eAgFnSc1d1	vytrvalá
<g/>
,	,	kIx,	,
výrazně	výrazně	k6eAd1	výrazně
kvetoucí	kvetoucí	k2eAgInPc1d1	kvetoucí
<g/>
,	,	kIx,	,
20	[number]	k4	20
až	až	k9	až
80	[number]	k4	80
cm	cm	kA	cm
vysoká	vysoký	k2eAgFnSc1d1	vysoká
bylina	bylina	k1gFnSc1	bylina
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
hvězdnicovitých	hvězdnicovitý	k2eAgFnPc2d1	hvězdnicovitý
(	(	kIx(	(
<g/>
Asteraceae	Asteraceae	k1gFnPc2	Asteraceae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
