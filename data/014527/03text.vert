<s>
Akihito	Akihit	k2eAgNnSc1d1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
aktualizaci	aktualizace	k1gFnSc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
obsahuje	obsahovat	k5eAaImIp3nS
zastaralé	zastaralý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
odrážel	odrážet	k5eAaImAgMnS
aktuální	aktuální	k2eAgInSc4d1
stav	stav	k1gInSc4
a	a	k8xC
nedávné	dávný	k2eNgFnPc4d1
události	událost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historické	historický	k2eAgFnPc4d1
informace	informace	k1gFnPc4
nemažte	mazat	k5eNaImRp2nP
<g/>
,	,	kIx,
raději	rád	k6eAd2
je	on	k3xPp3gInPc4
převeďte	převést	k5eAaPmRp2nP
do	do	k7c2
minulého	minulý	k2eAgInSc2d1
času	čas	k1gInSc2
a	a	k8xC
případně	případně	k6eAd1
přesuňte	přesunout	k5eAaPmRp2nP
do	do	k7c2
části	část	k1gFnSc2
článku	článek	k1gInSc2
věnované	věnovaný	k2eAgFnPc4d1
dějinám	dějiny	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s>
Akihito	Akihit	k2eAgNnSc1d1
</s>
<s>
125	#num#	k4
<g/>
.	.	kIx.
japonský	japonský	k2eAgMnSc1d1
císař	císař	k1gMnSc1
</s>
<s>
Císař	Císař	k1gMnSc1
Akihito	Akihit	k2eAgNnSc4d1
</s>
<s>
Doba	doba	k1gFnSc1
vlády	vláda	k1gFnSc2
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1989	#num#	k4
–	–	k?
30	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2019	#num#	k4
(	(	kIx(
<g/>
32	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
Narození	narození	k1gNnSc1
</s>
<s>
23	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1933	#num#	k4
(	(	kIx(
<g/>
87	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
Kókjo	Kókjo	k1gNnSc1
<g/>
,	,	kIx,
Tokio	Tokio	k1gNnSc1
<g/>
,	,	kIx,
Japonsko	Japonsko	k1gNnSc1
Japonsko	Japonsko	k1gNnSc1
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
</s>
<s>
Hirohito	Hirohit	k2eAgNnSc1d1
</s>
<s>
Nástupce	nástupce	k1gMnSc1
</s>
<s>
Naruhito	Naruhit	k2eAgNnSc1d1
</s>
<s>
Královna	královna	k1gFnSc1
</s>
<s>
císařovna	císařovna	k1gFnSc1
Mičiko	Mičika	k1gFnSc5
</s>
<s>
Potomci	potomek	k1gMnPc1
</s>
<s>
císař	císař	k1gMnSc1
Naruhito	Naruhit	k2eAgNnSc1d1
(	(	kIx(
<g/>
1960	#num#	k4
<g/>
–	–	k?
<g/>
)	)	kIx)
</s>
<s>
Princ	princa	k1gFnPc2
Fumihito	Fumihit	k2eAgNnSc1d1
<g/>
,	,	kIx,
kníže	kníže	k1gNnSc1wR
Akišino	Akišin	k2eAgNnSc1d1
(	(	kIx(
<g/>
1965	#num#	k4
<g/>
–	–	k?
<g/>
)	)	kIx)
</s>
<s>
Paní	paní	k1gFnSc1
Sajako	Sajako	k1gNnSc4
(	(	kIx(
<g/>
1969	#num#	k4
<g/>
–	–	k?
<g/>
)	)	kIx)
</s>
<s>
Otec	otec	k1gMnSc1
</s>
<s>
císař	císař	k1gMnSc1
Hirohito	Hirohit	k2eAgNnSc1d1
</s>
<s>
Matka	matka	k1gFnSc1
</s>
<s>
Nagako	Nagako	k6eAd1
Kódžun	Kódžun	k1gInSc1
</s>
<s>
Podpis	podpis	k1gInSc1
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Japonská	japonský	k2eAgFnSc1d1
císařská	císařský	k2eAgFnSc1d1
rodina	rodina	k1gFnSc1
</s>
<s>
JV	JV	kA
císař	císař	k1gMnSc1
NaruhitoJV	NaruhitoJV	k1gMnSc1
císařovna	císařovna	k1gFnSc1
Masako	Masaka	k1gMnSc5
</s>
<s>
JCV	JCV	kA
princezna	princezna	k1gFnSc1
Aiko	Aiko	k1gMnSc1
</s>
<s>
JV	JV	kA
císař	císař	k1gMnSc1
Akihito	Akihita	k1gMnSc5
JV	JV	kA
císařovna	císařovna	k1gFnSc1
Mičiko	Mičika	k1gFnSc5
</s>
<s>
JCV	JCV	kA
korunní	korunní	k2eAgMnSc1d1
princ	princ	k1gMnSc1
AkišinoJCV	AkišinoJCV	k1gFnSc2
korunní	korunní	k2eAgFnSc1d1
princezna	princezna	k1gFnSc1
Kiko	Kiko	k6eAd1
</s>
<s>
JCV	JCV	kA
princezna	princezna	k1gFnSc1
Mako	mako	k1gNnSc5
</s>
<s>
JCV	JCV	kA
princezna	princezna	k1gFnSc1
Kako	Kako	k1gMnSc1
</s>
<s>
JCV	JCV	kA
princ	princ	k1gMnSc1
Hisahito	Hisahita	k1gFnSc5
</s>
<s>
JCV	JCV	kA
princ	princ	k1gMnSc1
HitačiJCV	HitačiJCV	k1gMnSc1
princezna	princezna	k1gFnSc1
Kiko	Kiko	k6eAd1
</s>
<s>
JCV	JCV	kA
princezna	princezna	k1gFnSc1
Juriko	Jurika	k1gFnSc5
</s>
<s>
JCV	JCV	kA
princezna	princezna	k1gFnSc1
Nobuko	Nobuko	k1gNnSc5
</s>
<s>
JCV	JCV	kA
princezna	princezna	k1gFnSc1
Akiko	Akika	k1gFnSc5
</s>
<s>
JCV	JCV	kA
princezna	princezna	k1gFnSc1
Jóko	Jóko	k1gMnSc1
</s>
<s>
JCV	JCV	kA
princezna	princezna	k1gFnSc1
Hisako	Hisako	k1gNnSc1
Takamado	Takamada	k1gFnSc5
</s>
<s>
JCV	JCV	kA
princezna	princezna	k1gFnSc1
Cuguko	Cuguko	k1gNnSc4
</s>
<s>
JCV	JCV	kA
princezna	princezna	k1gFnSc1
Ajako	Ajako	k1gNnSc1
</s>
<s>
z	z	k7c2
•	•	k?
d	d	k?
•	•	k?
e	e	k0
</s>
<s>
Akihito	Akihit	k2eAgNnSc1d1
(	(	kIx(
<g/>
japonsky	japonsky	k6eAd1
明	明	k?
<g/>
;	;	kIx,
*	*	kIx~
<g/>
23	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1933	#num#	k4
Tokio	Tokio	k1gNnSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
od	od	k7c2
roku	rok	k1gInSc2
1989	#num#	k4
do	do	k7c2
roku	rok	k1gInSc2
2019	#num#	k4
125	#num#	k4
<g/>
.	.	kIx.
japonským	japonský	k2eAgMnSc7d1
císařem	císař	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Éra	éra	k1gFnSc1
císaře	císař	k1gMnSc2
Akihita	Akihit	k1gMnSc2
je	být	k5eAaImIp3nS
nazývána	nazývat	k5eAaImNgFnS
Heisei	Heisei	k1gNnSc7
(	(	kIx(
<g/>
平	平	k?
<g/>
,	,	kIx,
doslova	doslova	k6eAd1
všudypřítomný	všudypřítomný	k2eAgInSc1d1
mír	mír	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
podle	podle	k7c2
zvyku	zvyk	k1gInSc2
bude	být	k5eAaImBp3nS
po	po	k7c6
své	svůj	k3xOyFgFnSc6
smrti	smrt	k1gFnSc6
přejmenován	přejmenovat	k5eAaPmNgMnS
na	na	k7c4
Císaře	Císař	k1gMnSc4
Heisei	Heise	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
také	také	k9
bratrancem	bratranec	k1gMnSc7
princezny	princezna	k1gFnSc2
Bangja	Bangj	k1gInSc2
<g/>
,	,	kIx,
poslední	poslední	k2eAgFnSc2d1
korunní	korunní	k2eAgFnSc2d1
princezny	princezna	k1gFnSc2
Koreje	Korea	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnSc7
nástupcem	nástupce	k1gMnSc7
je	být	k5eAaImIp3nS
jeho	jeho	k3xOp3gMnSc1
syn	syn	k1gMnSc1
<g/>
,	,	kIx,
císař	císař	k1gMnSc1
Naruhito	Naruhit	k2eAgNnSc4d1
<g/>
.	.	kIx.
</s>
<s>
Životopis	životopis	k1gInSc1
</s>
<s>
Akihito	Akihit	k2eAgNnSc1d1
je	být	k5eAaImIp3nS
starším	starší	k1gMnPc3
ze	z	k7c2
dvou	dva	k4xCgMnPc2
synů	syn	k1gMnPc2
(	(	kIx(
<g/>
a	a	k8xC
pátým	pátý	k4xOgNnSc7
ze	z	k7c2
sedmi	sedm	k4xCc2
potomků	potomek	k1gMnPc2
<g/>
)	)	kIx)
císaře	císař	k1gMnSc2
Šówy	Šówa	k1gMnSc2
(	(	kIx(
<g/>
Hirohita	Hirohita	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
císařovny	císařovna	k1gFnPc1
Kódžun	Kódžun	k1gInSc1
(	(	kIx(
<g/>
Nagako	Nagako	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dětství	dětství	k1gNnSc6
byl	být	k5eAaImAgInS
nazýván	nazývat	k5eAaImNgInS
princem	princ	k1gMnSc7
Cugu	Cugus	k1gInSc2
a	a	k8xC
vyučován	vyučovat	k5eAaImNgInS
soukromými	soukromý	k2eAgMnPc7d1
učiteli	učitel	k1gMnPc7
<g/>
,	,	kIx,
mezi	mezi	k7c7
lety	léto	k1gNnPc7
1940	#num#	k4
a	a	k8xC
1952	#num#	k4
pak	pak	k6eAd1
navštěvoval	navštěvovat	k5eAaImAgInS
Univerzitu	univerzita	k1gFnSc4
Gakušúin	Gakušúina	k1gFnPc2
(	(	kIx(
<g/>
学	学	k?
<g/>
)	)	kIx)
v	v	k7c6
tokijské	tokijský	k2eAgFnSc6d1
čtvrti	čtvrt	k1gFnSc6
Tošima	Tošima	k1gNnSc4
<g/>
,	,	kIx,
školu	škola	k1gFnSc4
japonské	japonský	k2eAgFnSc2d1
aristokracie	aristokracie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
ostatních	ostatní	k2eAgMnPc2d1
členů	člen	k1gMnPc2
rodiny	rodina	k1gFnSc2
nikdy	nikdy	k6eAd1
nepřijal	přijmout	k5eNaPmAgMnS
žádnou	žádný	k3yNgFnSc4
vojenskou	vojenský	k2eAgFnSc4d1
hodnost	hodnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Během	během	k7c2
amerického	americký	k2eAgNnSc2d1
bombardování	bombardování	k1gNnSc2
Tokia	Tokio	k1gNnSc2
v	v	k7c6
březnu	březen	k1gInSc6
1945	#num#	k4
byl	být	k5eAaImAgInS
společně	společně	k6eAd1
se	s	k7c7
svým	svůj	k3xOyFgMnSc7
bratrem	bratr	k1gMnSc7
princem	princ	k1gMnSc7
Masahito	Masahita	k1gMnSc5
evakuován	evakuovat	k5eAaBmNgMnS
z	z	k7c2
města	město	k1gNnSc2
a	a	k8xC
při	při	k7c6
americké	americký	k2eAgFnSc6d1
okupaci	okupace	k1gFnSc6
<g/>
,	,	kIx,
následující	následující	k2eAgFnSc6d1
bezprostředně	bezprostředně	k6eAd1
po	po	k7c6
druhé	druhý	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
<g/>
,	,	kIx,
jej	on	k3xPp3gInSc4
Elizabeth	Elizabeth	k1gFnSc1
Gray	Graa	k1gFnSc2
Viningová	Viningový	k2eAgFnSc1d1
vyučovala	vyučovat	k5eAaImAgFnS
angličtině	angličtina	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Studoval	studovat	k5eAaImAgMnS
také	také	k9
na	na	k7c6
katedře	katedra	k1gFnSc6
politických	politický	k2eAgFnPc2d1
věd	věda	k1gFnPc2
na	na	k7c6
universitě	universita	k1gFnSc6
Gakušúin	Gakušúina	k1gFnPc2
<g/>
,	,	kIx,
nikdy	nikdy	k6eAd1
však	však	k9
nedosáhl	dosáhnout	k5eNaPmAgMnS
akademické	akademický	k2eAgFnPc4d1
hodnosti	hodnost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Ačkoli	ačkoli	k8xS
byl	být	k5eAaImAgInS
právoplatným	právoplatný	k2eAgMnSc7d1
korunním	korunní	k2eAgMnSc7d1
princem	princ	k1gMnSc7
už	už	k6eAd1
od	od	k7c2
momentu	moment	k1gInSc2
svého	svůj	k3xOyFgNnSc2
narození	narození	k1gNnSc2
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
mu	on	k3xPp3gMnSc3
titul	titul	k1gInSc1
oficiálně	oficiálně	k6eAd1
udělen	udělit	k5eAaPmNgInS
až	až	k9
10	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1952	#num#	k4
v	v	k7c6
Tokijském	tokijský	k2eAgInSc6d1
císařském	císařský	k2eAgInSc6d1
paláci	palác	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
červnu	červen	k1gInSc6
1953	#num#	k4
<g/>
,	,	kIx,
reprezentoval	reprezentovat	k5eAaImAgInS
jako	jako	k9
korunní	korunní	k2eAgMnSc1d1
princ	princ	k1gMnSc1
Akihito	Akihit	k2eAgNnSc1d1
Japonsko	Japonsko	k1gNnSc1
na	na	k7c6
korunovaci	korunovace	k1gFnSc6
královny	královna	k1gFnSc2
Alžběty	Alžběta	k1gFnSc2
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Korunní	korunní	k2eAgMnSc1d1
princ	princ	k1gMnSc1
Akihito	Akihit	k2eAgNnSc4d1
a	a	k8xC
korunní	korunní	k2eAgFnSc1d1
princezna	princezna	k1gFnSc1
Mičiko	Mičika	k1gFnSc5
podnikli	podniknout	k5eAaPmAgMnP
37	#num#	k4
oficiálních	oficiální	k2eAgFnPc2d1
návštěv	návštěva	k1gFnPc2
do	do	k7c2
ostatních	ostatní	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chryzantémový	chryzantémový	k2eAgInSc4d1
trůn	trůn	k1gInSc4
převzal	převzít	k5eAaPmAgMnS
po	po	k7c6
smrti	smrt	k1gFnSc6
svého	svůj	k3xOyFgMnSc2
otce	otec	k1gMnSc2
7	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1989	#num#	k4
<g/>
,	,	kIx,
a	a	k8xC
stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
tak	tak	k9
125	#num#	k4
<g/>
.	.	kIx.
císařem	císař	k1gMnSc7
v	v	k7c6
historii	historie	k1gFnSc6
Japonska	Japonsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oficiálně	oficiálně	k6eAd1
byl	být	k5eAaImAgMnS
císař	císař	k1gMnSc1
Akihito	Akihit	k2eAgNnSc4d1
dosazen	dosazen	k2eAgInSc4d1
na	na	k7c4
trůn	trůn	k1gInSc4
jako	jako	k8xC,k8xS
císař	císař	k1gMnSc1
Japonska	Japonsko	k1gNnSc2
12	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1990	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1998	#num#	k4
obdržel	obdržet	k5eAaPmAgMnS
od	od	k7c2
anglické	anglický	k2eAgFnSc2d1
královny	královna	k1gFnSc2
Podvazkový	podvazkový	k2eAgInSc4d1
řád	řád	k1gInSc4
a	a	k8xC
je	být	k5eAaImIp3nS
tak	tak	k6eAd1
jediným	jediný	k2eAgMnSc7d1
rytířem	rytíř	k1gMnSc7
neevropského	evropský	k2eNgInSc2d1
původu	původ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
lednu	leden	k1gInSc6
2003	#num#	k4
se	se	k3xPyFc4
císař	císař	k1gMnSc1
podrobil	podrobit	k5eAaPmAgMnS
operaci	operace	k1gFnSc4
prostaty	prostata	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
oznámil	oznámit	k5eAaPmAgMnS
císař	císař	k1gMnSc1
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
kvůli	kvůli	k7c3
zdravotním	zdravotní	k2eAgInPc3d1
problémům	problém	k1gInPc3
a	a	k8xC
vysokému	vysoký	k2eAgInSc3d1
věku	věk	k1gInSc3
rozhodl	rozhodnout	k5eAaPmAgInS
abdikovat	abdikovat	k5eAaBmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bude	být	k5eAaImBp3nS
tak	tak	k6eAd1
po	po	k7c6
200	#num#	k4
letech	léto	k1gNnPc6
první	první	k4xOgFnSc2
<g/>
,	,	kIx,
kdo	kdo	k3yRnSc1,k3yInSc1,k3yQnSc1
vládu	vláda	k1gFnSc4
opustí	opustit	k5eAaPmIp3nP
dobrovolně	dobrovolně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svou	svůj	k3xOyFgFnSc4
abdikaci	abdikace	k1gFnSc4
Akihito	Akihit	k2eAgNnSc4d1
oficiálně	oficiálně	k6eAd1
oznámil	oznámit	k5eAaPmAgMnS
30	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2019	#num#	k4
a	a	k8xC
podle	podle	k7c2
tradice	tradice	k1gFnSc2
přestal	přestat	k5eAaPmAgMnS
být	být	k5eAaImF
císařem	císař	k1gMnSc7
o	o	k7c6
půlnoci	půlnoc	k1gFnSc6
ze	z	k7c2
30	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
na	na	k7c4
1	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
japonského	japonský	k2eAgInSc2d1
času	čas	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Společně	společně	k6eAd1
s	s	k7c7
ním	on	k3xPp3gMnSc7
skončila	skončit	k5eAaPmAgFnS
i	i	k9
imperiální	imperiální	k2eAgFnSc1d1
éra	éra	k1gFnSc1
Heisei	Heise	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Akihita	Akihitum	k1gNnPc1
ve	v	k7c6
funkci	funkce	k1gFnSc6
nahradil	nahradit	k5eAaPmAgMnS
korunní	korunní	k2eAgMnSc1d1
princ	princ	k1gMnSc1
Naruhito	Naruhit	k2eAgNnSc1d1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
126	#num#	k4
<g/>
.	.	kIx.
císařem	císař	k1gMnSc7
Japonska	Japonsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Oficiální	oficiální	k2eAgFnSc1d1
funkce	funkce	k1gFnSc1
</s>
<s>
Už	už	k9
od	od	k7c2
svého	svůj	k3xOyFgInSc2
nástupu	nástup	k1gInSc2
na	na	k7c4
trůn	trůn	k1gInSc4
se	se	k3xPyFc4
císař	císař	k1gMnSc1
Akihito	Akihita	k1gMnSc5
snaží	snažit	k5eAaImIp3nP
císařskou	císařský	k2eAgFnSc4d1
rodinu	rodina	k1gFnSc4
více	hodně	k6eAd2
přiblížit	přiblížit	k5eAaPmF
japonskému	japonský	k2eAgInSc3d1
lidu	lid	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
On	on	k3xPp3gMnSc1
i	i	k8xC
se	s	k7c7
svou	svůj	k3xOyFgFnSc7
manželkou	manželka	k1gFnSc7
navštívil	navštívit	k5eAaPmAgMnS
všechny	všechen	k3xTgFnPc4
japonské	japonský	k2eAgFnPc4d1
prefektury	prefektura	k1gFnPc4
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
osmdesát	osmdesát	k4xCc4
dalších	další	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Český	český	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
Václav	Václav	k1gMnSc1
Havel	Havel	k1gMnSc1
se	se	k3xPyFc4
osobně	osobně	k6eAd1
setkal	setkat	k5eAaPmAgMnS
s	s	k7c7
císařem	císař	k1gMnSc7
Akihito	Akihit	k2eAgNnSc4d1
na	na	k7c6
konferenci	konference	k1gFnSc6
"	"	kIx"
<g/>
Budoucnost	budoucnost	k1gFnSc1
naděje	naděje	k1gFnSc1
<g/>
"	"	kIx"
v	v	k7c6
Hirošimě	Hirošima	k1gFnSc6
dne	den	k1gInSc2
5	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1995	#num#	k4
a	a	k8xC
při	při	k7c6
této	tento	k3xDgFnSc6
příležitosti	příležitost	k1gFnSc6
pozval	pozvat	k5eAaPmAgMnS
císaře	císař	k1gMnSc4
na	na	k7c4
návštěvu	návštěva	k1gFnSc4
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
základě	základ	k1gInSc6
tohoto	tento	k3xDgNnSc2
pozvání	pozvání	k1gNnSc2
vykonal	vykonat	k5eAaPmAgMnS
císař	císař	k1gMnSc1
Akihito	Akihit	k2eAgNnSc4d1
s	s	k7c7
císařovnou	císařovna	k1gFnSc7
Mičiko	Mičika	k1gFnSc5
v	v	k7c6
roce	rok	k1gInSc6
2002	#num#	k4
čtyřdenní	čtyřdenní	k2eAgFnSc4d1
státní	státní	k2eAgFnSc4d1
návštěvu	návštěva	k1gFnSc4
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
(	(	kIx(
<g/>
6	#num#	k4
<g/>
.	.	kIx.
červenec	červenec	k1gInSc1
–	–	k?
9	#num#	k4
<g/>
.	.	kIx.
červenec	červenec	k1gInSc1
2002	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
to	ten	k3xDgNnSc1
vůbec	vůbec	k9
poprvé	poprvé	k6eAd1
v	v	k7c6
historii	historie	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
do	do	k7c2
Prahy	Praha	k1gFnSc2
zavítala	zavítat	k5eAaPmAgFnS
hlava	hlava	k1gFnSc1
japonského	japonský	k2eAgNnSc2d1
císařství	císařství	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
červnu	červen	k1gInSc6
roku	rok	k1gInSc2
2005	#num#	k4
navštívil	navštívit	k5eAaPmAgMnS
císař	císař	k1gMnSc1
americké	americký	k2eAgNnSc4d1
teritorium	teritorium	k1gNnSc4
Saipan	Saipana	k1gFnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
odehrála	odehrát	k5eAaPmAgFnS
jedna	jeden	k4xCgFnSc1
z	z	k7c2
nejbrutálnějších	brutální	k2eAgFnPc2d3
bitev	bitva	k1gFnPc2
2	#num#	k4
<g/>
.	.	kIx.
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
doprovodu	doprovod	k1gInSc6
císařovny	císařovna	k1gFnSc2
Mičiko	Mičika	k1gFnSc5
nabídl	nabídnout	k5eAaPmAgMnS
modlitby	modlitba	k1gFnSc2
a	a	k8xC
květiny	květina	k1gFnSc2
na	na	k7c6
mnoha	mnoho	k4c6
místech	místo	k1gNnPc6
vzdávajících	vzdávající	k2eAgFnPc2d1
hold	hold	k1gInSc4
nejen	nejen	k6eAd1
padlým	padlý	k2eAgMnPc3d1
Japoncům	Japonec	k1gMnPc3
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k8xC
americkým	americký	k2eAgMnPc3d1
vojákům	voják	k1gMnPc3
<g/>
,	,	kIx,
korejským	korejský	k2eAgMnPc3d1
dělníkům	dělník	k1gMnPc3
a	a	k8xC
místním	místní	k2eAgMnPc3d1
obyvatelům	obyvatel	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šlo	jít	k5eAaImAgNnS
o	o	k7c4
první	první	k4xOgFnSc4
cestu	cesta	k1gFnSc4
japonského	japonský	k2eAgMnSc2d1
monarchy	monarcha	k1gMnSc2
na	na	k7c4
bojiště	bojiště	k1gNnSc4
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Saipanskou	Saipanský	k2eAgFnSc4d1
cestu	cesta	k1gFnSc4
japonský	japonský	k2eAgInSc4d1
lid	lid	k1gInSc4
velmi	velmi	k6eAd1
ocenil	ocenit	k5eAaPmAgMnS
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
císařovy	císařův	k2eAgFnSc2d1
návštěvy	návštěva	k1gFnSc2
válečných	válečný	k2eAgInPc2d1
pomníků	pomník	k1gInPc2
v	v	k7c6
Tokiu	Tokio	k1gNnSc6
<g/>
,	,	kIx,
Hirošimě	Hirošima	k1gFnSc6
<g/>
,	,	kIx,
Nagasaki	Nagasaki	k1gNnSc6
a	a	k8xC
na	na	k7c6
Okinawě	Okinawa	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
1995	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2006	#num#	k4
oslavil	oslavit	k5eAaPmAgMnS
narození	narození	k1gNnSc4
prvního	první	k4xOgMnSc2
vnuka	vnuk	k1gMnSc2
<g/>
,	,	kIx,
prince	princ	k1gMnSc2
Hisahita	Hisahit	k1gMnSc2
<g/>
,	,	kIx,
třetího	třetí	k4xOgMnSc2
dítěte	dítě	k1gNnSc2
svého	svůj	k3xOyFgMnSc2
mladšího	mladý	k2eAgMnSc2d2
syna	syn	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hisahito	Hisahita	k1gMnSc5
je	být	k5eAaImIp3nS
první	první	k4xOgMnSc1
mužský	mužský	k2eAgMnSc1d1
následník	následník	k1gMnSc1
v	v	k7c6
císařské	císařský	k2eAgFnSc6d1
rodině	rodina	k1gFnSc6
po	po	k7c6
41	#num#	k4
letech	let	k1gInPc6
(	(	kIx(
<g/>
posledním	poslední	k2eAgMnSc6d1
byl	být	k5eAaImAgMnS
jeho	jeho	k3xOp3gMnSc1
bratr	bratr	k1gMnSc1
<g/>
,	,	kIx,
princ	princ	k1gMnSc1
Akišino	Akišin	k2eAgNnSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
mohl	moct	k5eAaImAgInS
by	by	kYmCp3nS
odvrátit	odvrátit	k5eAaPmF
následnickou	následnický	k2eAgFnSc4d1
krizi	krize	k1gFnSc4
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
císařovu	císařův	k2eAgMnSc3d1
staršímu	starší	k1gMnSc3
synovi	syn	k1gMnSc3
<g/>
,	,	kIx,
korunnímu	korunní	k2eAgMnSc3d1
princi	princ	k1gMnSc3
<g/>
,	,	kIx,
se	se	k3xPyFc4
narodilo	narodit	k5eAaPmAgNnS
pouze	pouze	k6eAd1
jedno	jeden	k4xCgNnSc1
děvče	děvče	k1gNnSc1
–	–	k?
princezna	princezna	k1gFnSc1
Aiko	Aiko	k6eAd1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
podle	podle	k7c2
současně	současně	k6eAd1
platného	platný	k2eAgNnSc2d1
nástupnického	nástupnický	k2eAgNnSc2d1
práva	právo	k1gNnSc2
nemůže	moct	k5eNaImIp3nS
usednout	usednout	k5eAaPmF
na	na	k7c4
trůn	trůn	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Manželství	manželství	k1gNnSc1
a	a	k8xC
děti	dítě	k1gFnPc1
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1959	#num#	k4
si	se	k3xPyFc3
vzal	vzít	k5eAaPmAgMnS
slečnu	slečna	k1gFnSc4
Mičiko	Mičika	k1gFnSc5
Šóda	Šód	k2eAgFnSc1d1
(	(	kIx(
<g/>
narozena	narozen	k2eAgFnSc1d1
20	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1934	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nová	nový	k2eAgFnSc1d1
korunní	korunní	k2eAgFnSc1d1
princezna	princezna	k1gFnSc1
byla	být	k5eAaImAgFnS
první	první	k4xOgInSc4
nešlechtic	nešlechtice	k1gFnPc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
se	se	k3xPyFc4
přiženil	přiženit	k5eAaPmAgInS
do	do	k7c2
císařské	císařský	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mají	mít	k5eAaImIp3nP
tři	tři	k4xCgFnPc4
děti	dítě	k1gFnPc4
<g/>
:	:	kIx,
</s>
<s>
korunního	korunní	k2eAgMnSc4d1
prince	princ	k1gMnSc4
Naruhito	Naruhit	k2eAgNnSc1d1
(	(	kIx(
<g/>
narozen	narozen	k2eAgMnSc1d1
23	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1960	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
prince	princ	k1gMnSc2
Akišino	Akišin	k2eAgNnSc1d1
</s>
<s>
princeznu	princezna	k1gFnSc4
Sajako	Sajako	k1gNnSc1
Kuroda	Kurod	k1gMnSc2
</s>
<s>
Tituly	titul	k1gInPc1
a	a	k8xC
vyznamenání	vyznamenání	k1gNnSc1
</s>
<s>
Tituly	titul	k1gInPc1
</s>
<s>
23	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1933	#num#	k4
–	–	k?
10	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1952	#num#	k4
<g/>
:	:	kIx,
Jeho	jeho	k3xOp3gFnSc1
císařská	císařský	k2eAgFnSc1d1
výsost	výsost	k1gFnSc1
Princ	princa	k1gFnPc2
Tsugu	tsuga	k1gFnSc4
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1952	#num#	k4
–	–	k?
7	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1989	#num#	k4
<g/>
:	:	kIx,
皇	皇	k?
Jeho	jeho	k3xOp3gFnSc4
císařská	císařský	k2eAgFnSc1d1
výsost	výsost	k1gFnSc4
korunní	korunní	k2eAgMnSc1d1
princ	princ	k1gMnSc1
Japonska	Japonsko	k1gNnSc2
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1989	#num#	k4
–	–	k?
30	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2019	#num#	k4
<g/>
:	:	kIx,
Jeho	jeho	k3xOp3gFnSc1
výsost	výsost	k1gFnSc1
císař	císař	k1gMnSc1
Japonska	Japonsko	k1gNnSc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2019	#num#	k4
–	–	k?
dosud	dosud	k6eAd1
<g/>
:	:	kIx,
上	上	k?
Jeho	jeho	k3xOp3gFnSc4
výsost	výsost	k1gFnSc4
emeritní	emeritní	k2eAgMnSc1d1
císař	císař	k1gMnSc1
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vyznamenání	vyznamenání	k1gNnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Tituly	titul	k1gInPc1
a	a	k8xC
vyznamenání	vyznamenání	k1gNnSc1
Akihita	Akihitum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Japonsky	japonsky	k6eAd1
cisar	cisar	k1gMnSc1
prijal	prijat	k5eAaPmAgMnS,k5eAaBmAgMnS,k5eAaImAgMnS
k	k	k7c3
mimoradne	mimoradnout	k5eAaPmIp3nS
audienci	audience	k1gFnSc3
ceskeho	ceske	k1gMnSc2
prezidenta	prezident	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1995-12-05	1995-12-05	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Japonský	japonský	k2eAgMnSc1d1
císař	císař	k1gMnSc1
obdivoval	obdivovat	k5eAaImAgMnS
Hrad	hrad	k1gInSc4
<g/>
.	.	kIx.
iDNES	iDNES	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2002-07-08	2002-07-08	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ご	ご	k?
English	English	k1gMnSc1
Titles	Titles	k1gMnSc1
and	and	k?
Basic	Basic	kA
words	words	k1gInSc1
relating	relating	k1gInSc1
to	ten	k3xDgNnSc1
the	the	k?
Imperial	Imperial	k1gInSc1
Succession	Succession	k1gInSc1
Dostupné	dostupný	k2eAgFnSc2d1
online	onlin	k1gMnSc5
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Akihito	Akihit	k2eAgNnSc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Osoba	osoba	k1gFnSc1
Akihito	Akihit	k2eAgNnSc1d1
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
Japonští	japonský	k2eAgMnPc1d1
císaři	císař	k1gMnPc1
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
<g/>
:	:	kIx,
císař	císař	k1gMnSc1
Šówa	Šówa	k1gMnSc1
</s>
<s>
1989	#num#	k4
<g/>
–	–	k?
<g/>
2019	#num#	k4
Akihito	Akihit	k2eAgNnSc1d1
</s>
<s>
Nástupce	nástupce	k1gMnSc1
<g/>
:	:	kIx,
Naruhito	Naruhit	k2eAgNnSc1d1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Japonští	japonský	k2eAgMnPc1d1
císařové	císař	k1gMnPc1
seznam	seznam	k1gInSc4
japonských	japonský	k2eAgMnPc2d1
císařů	císař	k1gMnPc2
Legendární	legendární	k2eAgFnSc2d1
</s>
<s>
Džómon	Džómon	k1gMnSc1
</s>
<s>
660	#num#	k4
ACN	ACN	kA
<g/>
–	–	k?
<g/>
291	#num#	k4
ACN	ACN	kA
</s>
<s>
Džimmu	Džimmat	k5eAaPmIp1nS
</s>
<s>
Suizei	Suizei	k6eAd1
</s>
<s>
Annei	Annei	k6eAd1
</s>
<s>
Itoku	Itoku	k6eAd1
</s>
<s>
Kóšó	Kóšó	k?
</s>
<s>
Kóan	Kóan	k1gMnSc1
</s>
<s>
Jajoi	Jajoi	k6eAd1
</s>
<s>
290	#num#	k4
ACN	ACN	kA
<g/>
–	–	k?
<g/>
269	#num#	k4
AD	ad	k7c4
</s>
<s>
Kórei	Kórei	k6eAd1
</s>
<s>
Kógen	Kógen	k1gInSc1
</s>
<s>
Kaika	Kaika	k6eAd1
</s>
<s>
Sudžin	Sudžin	k1gMnSc1
</s>
<s>
Suinin	Suinin	k1gInSc1
</s>
<s>
Keikó	Keikó	k?
</s>
<s>
Seimu	Seimat	k5eAaPmIp1nS
</s>
<s>
Čúai	Čúai	k6eAd1
</s>
<s>
Džingu	Džing	k1gInSc2
Kógó	Kógó	k1gFnSc7
<g/>
**	**	k?
</s>
<s>
Znak	znak	k1gInSc4
japonského	japonský	k2eAgMnSc2d1
císaře	císař	k1gMnSc2
Jamato	Jamat	k2eAgNnSc4d1
</s>
<s>
Kofun	Kofun	k1gMnSc1
</s>
<s>
269	#num#	k4
<g/>
–	–	k?
<g/>
539	#num#	k4
</s>
<s>
Ódžin	Ódžin	k1gMnSc1
</s>
<s>
Nintoku	Nintok	k1gInSc3
</s>
<s>
Ričú	Ričú	k?
</s>
<s>
Hanzei	Hanzei	k6eAd1
</s>
<s>
Ingjó	Ingjó	k?
</s>
<s>
Ankó	Ankó	k?
</s>
<s>
Júrjaku	Júrjak	k1gMnSc3
</s>
<s>
Seinei	Seinei	k6eAd1
</s>
<s>
Kenzó	Kenzó	k?
</s>
<s>
Ninken	Ninken	k1gInSc1
</s>
<s>
Burecu	Burecu	k6eAd1
</s>
<s>
Keitai	Keitai	k6eAd1
</s>
<s>
Ankan	Ankan	k1gMnSc1
</s>
<s>
Senka	Senka	k6eAd1
</s>
<s>
Asuka	Asuka	k6eAd1
</s>
<s>
539	#num#	k4
<g/>
–	–	k?
<g/>
710	#num#	k4
</s>
<s>
Kinmei	Kinmei	k6eAd1
</s>
<s>
Bidacu	Bidacu	k6eAd1
</s>
<s>
Jómei	Jómei	k6eAd1
</s>
<s>
Sušun	Sušun	k1gMnSc1
</s>
<s>
Suiko	Suiko	k1gNnSc1
<g/>
*	*	kIx~
</s>
<s>
Džomei	Džomei	k6eAd1
</s>
<s>
Kógjoku	Kógjok	k1gInSc3
<g/>
*	*	kIx~
</s>
<s>
Kótoku	Kótok	k1gInSc3
</s>
<s>
Saimei	Saimei	k1gNnSc1
<g/>
*	*	kIx~
</s>
<s>
Tendži	Tendzat	k5eAaPmIp1nS
</s>
<s>
Kóbun	Kóbun	k1gMnSc1
</s>
<s>
Temmu	Temmat	k5eAaPmIp1nS
</s>
<s>
Džitó	Džitó	k?
</s>
<s>
Monmu	Monmat	k5eAaPmIp1nS
<g/>
*	*	kIx~
</s>
<s>
Gemmei	Gemmei	k1gNnSc1
<g/>
*	*	kIx~
</s>
<s>
Nara	Nara	k6eAd1
</s>
<s>
710	#num#	k4
<g/>
–	–	k?
<g/>
794	#num#	k4
</s>
<s>
Gemmei	Gemmei	k1gNnSc1
<g/>
*	*	kIx~
</s>
<s>
Genšó	Genšó	k?
<g/>
*	*	kIx~
</s>
<s>
Šómu	Šómu	k6eAd1
</s>
<s>
Kóken	Kóken	k1gInSc1
<g/>
*	*	kIx~
</s>
<s>
Džunnin	Džunnin	k1gInSc1
</s>
<s>
Šótoku	Šótok	k1gInSc3
<g/>
*	*	kIx~
</s>
<s>
Kónin	Kónin	k1gMnSc1
</s>
<s>
Kammu	Kammat	k5eAaPmIp1nS
</s>
<s>
Heian	Heian	k1gMnSc1
</s>
<s>
794	#num#	k4
<g/>
–	–	k?
<g/>
1185	#num#	k4
</s>
<s>
Kammu	Kammat	k5eAaPmIp1nS
</s>
<s>
Heizei	Heizei	k6eAd1
</s>
<s>
Saga	Saga	k6eAd1
</s>
<s>
Džunna	Džunna	k6eAd1
</s>
<s>
Ninmjó	Ninmjó	k?
</s>
<s>
Montoku	Montok	k1gInSc3
</s>
<s>
Seiwa	Seiwa	k6eAd1
</s>
<s>
Józei	Józei	k6eAd1
</s>
<s>
Kókó	Kókó	k?
</s>
<s>
Uda	Uda	k?
</s>
<s>
Daigo	Daigo	k6eAd1
</s>
<s>
Suzaku	Suzak	k1gMnSc3
</s>
<s>
Murakami	Muraka	k1gFnPc7
</s>
<s>
Reizei	Reizei	k6eAd1
</s>
<s>
En	En	k?
<g/>
'	'	kIx"
<g/>
ju	ju	k0
</s>
<s>
Kazan	Kazan	k1gMnSc1
</s>
<s>
Ichidžó	Ichidžó	k?
</s>
<s>
Sandžó	Sandžó	k?
</s>
<s>
Go-Ichidžó	Go-Ichidžó	k?
</s>
<s>
Go-Suzaku	Go-Suzak	k1gMnSc3
</s>
<s>
Go-Reizei	Go-Reizei	k6eAd1
</s>
<s>
Go-Sandžó	Go-Sandžó	k?
</s>
<s>
Širakawa	Širakawa	k6eAd1
</s>
<s>
Horikawa	Horikawa	k6eAd1
</s>
<s>
Toba	Toba	k6eAd1
</s>
<s>
Sutoku	Sutok	k1gInSc3
</s>
<s>
Konoe	Konoe	k6eAd1
</s>
<s>
Go-Širakawa	Go-Širakawa	k6eAd1
</s>
<s>
Nidžó	Nidžó	k?
</s>
<s>
Rokudžó	Rokudžó	k?
</s>
<s>
Takakura	Takakura	k1gFnSc1
</s>
<s>
Antoku	Antok	k1gInSc3
</s>
<s>
Go-Toba	Go-Toba	k1gFnSc1
</s>
<s>
Kamakura	Kamakura	k1gFnSc1
</s>
<s>
1185	#num#	k4
<g/>
–	–	k?
<g/>
1333	#num#	k4
</s>
<s>
Go-Toba	Go-Toba	k1gFnSc1
</s>
<s>
Cučimikado	Cučimikada	k1gFnSc5
</s>
<s>
Džuntoku	Džuntok	k1gInSc3
</s>
<s>
Čúkjó	Čúkjó	k?
</s>
<s>
Go-Horikawa	Go-Horikawa	k6eAd1
</s>
<s>
Šidžó	Šidžó	k?
</s>
<s>
Go-Saga	Go-Saga	k1gFnSc1
</s>
<s>
Go-Fukakusa	Go-Fukakus	k1gMnSc4
</s>
<s>
Kamejama	Kamejama	k?
</s>
<s>
Go-Uda	Go-Uda	k1gFnSc1
</s>
<s>
Fušimi	Fuši	k1gFnPc7
</s>
<s>
Go-Fušimi	Go-Fuši	k1gFnPc7
</s>
<s>
Go-Nidžó	Go-Nidžó	k?
</s>
<s>
Hanazono	Hanazona	k1gFnSc5
</s>
<s>
Go-Daigo	Go-Daigo	k6eAd1
</s>
<s>
Severní	severní	k2eAgInSc1d1
dvůr	dvůr	k1gInSc1
</s>
<s>
1333	#num#	k4
<g/>
–	–	k?
<g/>
1392	#num#	k4
</s>
<s>
Kógon	Kógon	k1gMnSc1
</s>
<s>
Kómjó	Kómjó	k?
</s>
<s>
Sukó	Sukó	k?
</s>
<s>
Go-Kógon	Go-Kógon	k1gMnSc1
</s>
<s>
Go-En	Go-En	k1gNnSc1
<g/>
'	'	kIx"
<g/>
jú	jú	k0
</s>
<s>
Go-Komacu	Go-Komacu	k6eAd1
</s>
<s>
Muromači	Muromač	k1gMnPc1
</s>
<s>
1333	#num#	k4
<g/>
–	–	k?
<g/>
1573	#num#	k4
</s>
<s>
Go-Murakami	Go-Muraka	k1gFnPc7
</s>
<s>
Čókei	Čókei	k6eAd1
</s>
<s>
Go-Kamejama	Go-Kamejama	k1gFnSc1
</s>
<s>
Go-Komacu	Go-Komacu	k6eAd1
</s>
<s>
Šókó	Šókó	k?
</s>
<s>
Go-Hanazono	Go-Hanazona	k1gFnSc5
</s>
<s>
Go-Cučimikado	Go-Cučimikada	k1gFnSc5
</s>
<s>
Go-Kašiwabara	Go-Kašiwabara	k1gFnSc1
</s>
<s>
Go-Nara	Go-Nara	k1gFnSc1
</s>
<s>
Ógimači	Ógimač	k1gMnPc1
</s>
<s>
Azuči-Momojama	Azuči-Momojama	k1gFnSc1
</s>
<s>
1573	#num#	k4
<g/>
–	–	k?
<g/>
1603	#num#	k4
</s>
<s>
Ógimači	Ógimač	k1gMnPc1
</s>
<s>
Go-Józei	Go-Józei	k6eAd1
</s>
<s>
Edo	Eda	k1gMnSc5
</s>
<s>
1603	#num#	k4
<g/>
–	–	k?
<g/>
1868	#num#	k4
</s>
<s>
Go-Józei	Go-Józei	k6eAd1
</s>
<s>
Go-Mizunó	Go-Mizunó	k?
</s>
<s>
Meišó	Meišó	k?
<g/>
*	*	kIx~
</s>
<s>
Go-Kómjó	Go-Kómjó	k?
</s>
<s>
Go-Sai	Go-Sai	k6eAd1
</s>
<s>
Reigen	Reigen	k1gInSc1
</s>
<s>
Higašijama	Higašijama	k1gFnSc1
</s>
<s>
Nakamikado	Nakamikada	k1gFnSc5
</s>
<s>
Sakuramači	Sakuramač	k1gMnPc1
</s>
<s>
Momozono	Momozona	k1gFnSc5
</s>
<s>
Go-Sakuramači	Go-Sakuramač	k1gMnPc1
<g/>
*	*	kIx~
</s>
<s>
Go-Momozono	Go-Momozona	k1gFnSc5
</s>
<s>
Kókaku	Kókak	k1gMnSc3
</s>
<s>
Ninkó	Ninkó	k?
</s>
<s>
Kómei	Kómei	k6eAd1
</s>
<s>
Meidži	Meidzat	k5eAaPmIp1nS
</s>
<s>
Japonské	japonský	k2eAgNnSc1d1
císařství	císařství	k1gNnSc1
</s>
<s>
1868	#num#	k4
<g/>
–	–	k?
<g/>
1947	#num#	k4
</s>
<s>
Meidži	Meidzat	k5eAaPmIp1nS
</s>
<s>
Taišó	Taišó	k?
</s>
<s>
Šówa	Šówa	k6eAd1
</s>
<s>
Moderní	moderní	k2eAgNnSc1d1
Japonsko	Japonsko	k1gNnSc1
</s>
<s>
1947	#num#	k4
<g/>
–	–	k?
<g/>
dosud	dosud	k6eAd1
</s>
<s>
Šówa	Šówa	k6eAd1
</s>
<s>
Akihito	Akihit	k2eAgNnSc1d1
</s>
<s>
Naruhito	Naruhit	k2eAgNnSc1d1
</s>
<s>
vládnoucí	vládnoucí	k2eAgFnPc1d1
císařovny	císařovna	k1gFnPc1
jsou	být	k5eAaImIp3nP
označeny	označit	k5eAaPmNgInP
hvězdičkou	hvězdička	k1gFnSc7
*	*	kIx~
/	/	kIx~
císařská	císařský	k2eAgFnSc1d1
choť	choť	k1gFnSc1
a	a	k8xC
regentka	regentka	k1gFnSc1
Džingu	Džing	k1gInSc2
Kógó	Kógó	k1gFnSc2
se	se	k3xPyFc4
tradičně	tradičně	k6eAd1
neuvádí	uvádět	k5eNaImIp3nS
**	**	k?
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
xx	xx	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
4208	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
119028379	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
8347	#num#	k4
3348	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
88289770	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
111992561	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
88289770	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Japonsko	Japonsko	k1gNnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
|	|	kIx~
Monarchie	monarchie	k1gFnSc1
</s>
