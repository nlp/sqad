<p>
<s>
Láska	láska	k1gFnSc1	láska
označuje	označovat	k5eAaImIp3nS	označovat
silný	silný	k2eAgInSc4d1	silný
pozitivní	pozitivní	k2eAgInSc4d1	pozitivní
vztah	vztah	k1gInSc4	vztah
náklonnosti	náklonnost	k1gFnSc2	náklonnost
<g/>
,	,	kIx,	,
oddanosti	oddanost	k1gFnSc2	oddanost
nebo	nebo	k8xC	nebo
touhy	touha	k1gFnSc2	touha
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
lišit	lišit	k5eAaImF	lišit
jak	jak	k6eAd1	jak
svojí	svůj	k3xOyFgFnSc7	svůj
povahou	povaha	k1gFnSc7	povaha
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
také	také	k9	také
předmětem	předmět	k1gInSc7	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
převažuje	převažovat	k5eAaImIp3nS	převažovat
volní	volní	k2eAgNnSc1d1	volní
<g/>
,	,	kIx,	,
jindy	jindy	k6eAd1	jindy
citová	citový	k2eAgFnSc1d1	citová
nebo	nebo	k8xC	nebo
sexuální	sexuální	k2eAgFnSc1d1	sexuální
stránka	stránka	k1gFnSc1	stránka
vztahu	vztah	k1gInSc2	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
předmětem	předmět	k1gInSc7	předmět
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
druhý	druhý	k4xOgMnSc1	druhý
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
určité	určitý	k2eAgNnSc1d1	určité
společenství	společenství	k1gNnSc1	společenství
nebo	nebo	k8xC	nebo
božstvo	božstvo	k1gNnSc1	božstvo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
určitá	určitý	k2eAgFnSc1d1	určitá
věc	věc	k1gFnSc1	věc
<g/>
,	,	kIx,	,
místo	místo	k1gNnSc1	místo
a	a	k8xC	a
vlast	vlast	k1gFnSc1	vlast
<g/>
,	,	kIx,	,
nějaké	nějaký	k3yIgNnSc1	nějaký
hnutí	hnutí	k1gNnSc1	hnutí
či	či	k8xC	či
idea	idea	k1gFnSc1	idea
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
i	i	k9	i
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
bohatství	bohatství	k1gNnSc4	bohatství
nebo	nebo	k8xC	nebo
moc	moc	k1gFnSc4	moc
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
z	z	k7c2	z
milovaného	milovaný	k2eAgInSc2d1	milovaný
předmětu	předmět	k1gInSc2	předmět
těšit	těšit	k5eAaImF	těšit
<g/>
,	,	kIx,	,
pomáhat	pomáhat	k5eAaImF	pomáhat
mu	on	k3xPp3gMnSc3	on
a	a	k8xC	a
pečovat	pečovat	k5eAaImF	pečovat
o	o	k7c4	o
něj	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
ho	on	k3xPp3gMnSc4	on
chtít	chtít	k5eAaImF	chtít
mít	mít	k5eAaImF	mít
pro	pro	k7c4	pro
sebe	sebe	k3xPyFc4	sebe
a	a	k8xC	a
ovládat	ovládat	k5eAaImF	ovládat
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
láskou	láska	k1gFnSc7	láska
rozumí	rozumět	k5eAaImIp3nS	rozumět
jen	jen	k9	jen
zamilovanost	zamilovanost	k1gFnSc1	zamilovanost
<g/>
,	,	kIx,	,
počáteční	počáteční	k2eAgFnSc1d1	počáteční
touha	touha	k1gFnSc1	touha
a	a	k8xC	a
emoční	emoční	k2eAgFnSc1d1	emoční
i	i	k8xC	i
sexuální	sexuální	k2eAgFnSc1d1	sexuální
přitažlivost	přitažlivost	k1gFnSc1	přitažlivost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
širším	široký	k2eAgInSc6d2	širší
smyslu	smysl	k1gInSc6	smysl
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
hovoří	hovořit	k5eAaImIp3nS	hovořit
i	i	k9	i
o	o	k7c6	o
lásce	láska	k1gFnSc6	láska
k	k	k7c3	k
sobě	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
egoismus	egoismus	k1gInSc4	egoismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Právě	právě	k9	právě
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
silný	silný	k2eAgInSc4d1	silný
vztah	vztah	k1gInSc4	vztah
ke	k	k7c3	k
druhému	druhý	k4xOgInSc3	druhý
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
z	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
strany	strana	k1gFnSc2	strana
také	také	k9	také
odmítnuta	odmítnout	k5eAaPmNgFnS	odmítnout
či	či	k8xC	či
zklamána	zklamat	k5eAaPmNgFnS	zklamat
<g/>
:	:	kIx,	:
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
"	"	kIx"	"
<g/>
nešťastné	šťastný	k2eNgFnSc6d1	nešťastná
lásce	láska	k1gFnSc6	láska
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgInPc1d1	mnohý
případy	případ	k1gInPc1	případ
nešťastné	šťastný	k2eNgFnSc2d1	nešťastná
lásky	láska	k1gFnSc2	láska
jsou	být	k5eAaImIp3nP	být
zaviněny	zaviněn	k2eAgFnPc1d1	zaviněna
nezralostí	nezralost	k1gFnSc7	nezralost
jedince	jedinec	k1gMnSc2	jedinec
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
osobnosti	osobnost	k1gFnSc2	osobnost
<g/>
.	.	kIx.	.
</s>
<s>
Osobnost	osobnost	k1gFnSc1	osobnost
není	být	k5eNaImIp3nS	být
vyvinuta	vyvinout	k5eAaPmNgFnS	vyvinout
a	a	k8xC	a
projektuje	projektovat	k5eAaBmIp3nS	projektovat
do	do	k7c2	do
partnera	partner	k1gMnSc2	partner
vlastnosti	vlastnost	k1gFnSc2	vlastnost
svých	svůj	k3xOyFgMnPc2	svůj
rodičů	rodič	k1gMnPc2	rodič
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
metaforickém	metaforický	k2eAgInSc6d1	metaforický
smyslu	smysl	k1gInSc6	smysl
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
slovo	slovo	k1gNnSc1	slovo
láska	láska	k1gFnSc1	láska
někdy	někdy	k6eAd1	někdy
používá	používat	k5eAaImIp3nS	používat
i	i	k9	i
pro	pro	k7c4	pro
jednostranný	jednostranný	k2eAgInSc4d1	jednostranný
vztah	vztah	k1gInSc4	vztah
pouhé	pouhý	k2eAgFnSc2d1	pouhá
chtivosti	chtivost	k1gFnSc2	chtivost
<g/>
,	,	kIx,	,
mocenské	mocenský	k2eAgInPc1d1	mocenský
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
láska	láska	k1gFnSc1	láska
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hospodářské	hospodářský	k2eAgFnPc1d1	hospodářská
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
láska	láska	k1gFnSc1	láska
k	k	k7c3	k
penězům	peníze	k1gInPc3	peníze
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
sexuální	sexuální	k2eAgFnPc1d1	sexuální
<g/>
.	.	kIx.	.
</s>
<s>
Spojení	spojení	k1gNnSc1	spojení
"	"	kIx"	"
<g/>
prodejná	prodejný	k2eAgFnSc1d1	prodejná
láska	láska	k1gFnSc1	láska
<g/>
"	"	kIx"	"
znamená	znamenat	k5eAaImIp3nS	znamenat
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
směnu	směna	k1gFnSc4	směna
sexuální	sexuální	k2eAgFnSc2d1	sexuální
chtivosti	chtivost	k1gFnSc2	chtivost
a	a	k8xC	a
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
potřeby	potřeba	k1gFnSc2	potřeba
a	a	k8xC	a
slovo	slovo	k1gNnSc1	slovo
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
použito	použít	k5eAaPmNgNnS	použít
ironicky	ironicky	k6eAd1	ironicky
a	a	k8xC	a
mimo	mimo	k7c4	mimo
svůj	svůj	k3xOyFgInSc4	svůj
vlastní	vlastní	k2eAgInSc4d1	vlastní
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
psychologického	psychologický	k2eAgInSc2d1	psychologický
pohledu	pohled	k1gInSc2	pohled
je	být	k5eAaImIp3nS	být
láska	láska	k1gFnSc1	láska
primární	primární	k2eAgFnSc1d1	primární
lidskou	lidský	k2eAgFnSc7d1	lidská
potřebou	potřeba	k1gFnSc7	potřeba
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
také	také	k9	také
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejrozšířenějších	rozšířený	k2eAgInPc2d3	nejrozšířenější
námětů	námět	k1gInPc2	námět
v	v	k7c6	v
umění	umění	k1gNnSc6	umění
–	–	k?	–
v	v	k7c6	v
knihách	kniha	k1gFnPc6	kniha
<g/>
,	,	kIx,	,
filmech	film	k1gInPc6	film
<g/>
,	,	kIx,	,
divadelních	divadelní	k2eAgFnPc6d1	divadelní
hrách	hra	k1gFnPc6	hra
i	i	k8xC	i
v	v	k7c6	v
písních	píseň	k1gFnPc6	píseň
různých	různý	k2eAgInPc2d1	různý
žánrů	žánr	k1gInPc2	žánr
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštní	zvláštní	k2eAgNnSc4d1	zvláštní
místo	místo	k1gNnSc4	místo
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
láska	láska	k1gFnSc1	láska
v	v	k7c6	v
některých	některý	k3yIgNnPc6	některý
náboženstvích	náboženství	k1gNnPc6	náboženství
a	a	k8xC	a
podle	podle	k7c2	podle
křesťanství	křesťanství	k1gNnSc2	křesťanství
je	být	k5eAaImIp3nS	být
sám	sám	k3xTgMnSc1	sám
Bůh	bůh	k1gMnSc1	bůh
láskou	láska	k1gFnSc7	láska
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozdělení	rozdělení	k1gNnSc1	rozdělení
významů	význam	k1gInPc2	význam
==	==	k?	==
</s>
</p>
<p>
<s>
Objektivní	objektivní	k2eAgNnSc1d1	objektivní
rozdělení	rozdělení	k1gNnSc1	rozdělení
a	a	k8xC	a
popsání	popsání	k1gNnSc1	popsání
všech	všecek	k3xTgInPc2	všecek
významů	význam	k1gInPc2	význam
lásky	láska	k1gFnSc2	láska
prakticky	prakticky	k6eAd1	prakticky
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
vnímání	vnímání	k1gNnSc1	vnímání
lásky	láska	k1gFnSc2	láska
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
závislé	závislý	k2eAgNnSc1d1	závislé
na	na	k7c6	na
osobních	osobní	k2eAgFnPc6d1	osobní
vlastnostech	vlastnost	k1gFnPc6	vlastnost
a	a	k8xC	a
zkušenostech	zkušenost	k1gFnPc6	zkušenost
každého	každý	k3xTgMnSc4	každý
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgInPc1d1	velký
rozdíly	rozdíl	k1gInPc1	rozdíl
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
mezi	mezi	k7c7	mezi
kulturami	kultura	k1gFnPc7	kultura
a	a	k8xC	a
v	v	k7c6	v
čase	čas	k1gInSc6	čas
(	(	kIx(	(
<g/>
např.	např.	kA	např.
ve	v	k7c6	v
starověkém	starověký	k2eAgNnSc6d1	starověké
Řecku	Řecko	k1gNnSc6	Řecko
byla	být	k5eAaImAgFnS	být
láska	láska	k1gFnSc1	láska
definována	definovat	k5eAaBmNgFnS	definovat
zcela	zcela	k6eAd1	zcela
odlišně	odlišně	k6eAd1	odlišně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
velkou	velký	k2eAgFnSc4d1	velká
subjektivitu	subjektivita	k1gFnSc4	subjektivita
výkladů	výklad	k1gInPc2	výklad
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
možné	možný	k2eAgNnSc1d1	možné
rozlišit	rozlišit	k5eAaPmF	rozlišit
tyto	tento	k3xDgFnPc4	tento
obecné	obecný	k2eAgFnPc4d1	obecná
kategorie	kategorie	k1gFnPc4	kategorie
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
===	===	k?	===
Interpersonální	interpersonální	k2eAgFnSc3d1	interpersonální
(	(	kIx(	(
<g/>
mezilidská	mezilidský	k2eAgFnSc1d1	mezilidská
<g/>
)	)	kIx)	)
láska	láska	k1gFnSc1	láska
===	===	k?	===
</s>
</p>
<p>
<s>
Nejčastěji	často	k6eAd3	často
se	s	k7c7	s
slovem	slovo	k1gNnSc7	slovo
láska	láska	k1gFnSc1	láska
popisuje	popisovat	k5eAaImIp3nS	popisovat
interpersonální	interpersonální	k2eAgFnSc1d1	interpersonální
láska	láska	k1gFnSc1	láska
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
láska	láska	k1gFnSc1	láska
mezi	mezi	k7c7	mezi
lidmi	člověk	k1gMnPc7	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
tímto	tento	k3xDgInSc7	tento
pojmem	pojem	k1gInSc7	pojem
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
silné	silný	k2eAgNnSc1d1	silné
citové	citový	k2eAgNnSc1d1	citové
zaujetí	zaujetí	k1gNnSc1	zaujetí
k	k	k7c3	k
jiné	jiný	k2eAgFnSc3d1	jiná
osobě	osoba	k1gFnSc3	osoba
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
osobám	osoba	k1gFnPc3	osoba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spojené	spojený	k2eAgInPc1d1	spojený
se	s	k7c7	s
šťastnými	šťastný	k2eAgInPc7d1	šťastný
pocity	pocit	k1gInPc7	pocit
v	v	k7c6	v
přítomnosti	přítomnost	k1gFnSc6	přítomnost
milované	milovaný	k2eAgFnSc2d1	milovaná
osoby	osoba	k1gFnSc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
jednat	jednat	k5eAaImF	jednat
jak	jak	k6eAd1	jak
o	o	k7c4	o
oboustranný	oboustranný	k2eAgInSc4d1	oboustranný
vztah	vztah	k1gInSc4	vztah
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
o	o	k7c4	o
jednostranný	jednostranný	k2eAgInSc4d1	jednostranný
neopětovaný	opětovaný	k2eNgInSc4d1	neopětovaný
cit	cit	k1gInSc4	cit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
I	i	k9	i
zde	zde	k6eAd1	zde
lze	lze	k6eAd1	lze
rozlišit	rozlišit	k5eAaPmF	rozlišit
více	hodně	k6eAd2	hodně
druhů	druh	k1gInPc2	druh
mezilidské	mezilidský	k2eAgFnSc2d1	mezilidská
lásky	láska	k1gFnSc2	láska
<g/>
,	,	kIx,	,
téma	téma	k1gNnSc1	téma
je	být	k5eAaImIp3nS	být
však	však	k9	však
velmi	velmi	k6eAd1	velmi
široké	široký	k2eAgNnSc1d1	široké
a	a	k8xC	a
vnímání	vnímání	k1gNnSc1	vnímání
lásky	láska	k1gFnSc2	láska
přirozeně	přirozeně	k6eAd1	přirozeně
subjektivní	subjektivní	k2eAgMnSc1d1	subjektivní
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
existuje	existovat	k5eAaImIp3nS	existovat
mnoho	mnoho	k4c1	mnoho
různých	různý	k2eAgFnPc2d1	různá
kategorizací	kategorizace	k1gFnPc2	kategorizace
<g/>
,	,	kIx,	,
kategorie	kategorie	k1gFnPc1	kategorie
nejsou	být	k5eNaImIp3nP	být
ostře	ostro	k6eAd1	ostro
ohraničené	ohraničený	k2eAgFnPc1d1	ohraničená
a	a	k8xC	a
často	často	k6eAd1	často
se	se	k3xPyFc4	se
překrývají	překrývat	k5eAaImIp3nP	překrývat
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
pomůcka	pomůcka	k1gFnSc1	pomůcka
pro	pro	k7c4	pro
rozlišení	rozlišení	k1gNnSc4	rozlišení
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
užívají	užívat	k5eAaImIp3nP	užívat
čtyři	čtyři	k4xCgInPc1	čtyři
výrazy	výraz	k1gInPc1	výraz
staré	starý	k2eAgFnSc2d1	stará
řečtiny	řečtina	k1gFnSc2	řečtina
pro	pro	k7c4	pro
lásku	láska	k1gFnSc4	láska
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Storgé	Storgé	k6eAd1	Storgé
–	–	k?	–
blízká	blízký	k2eAgFnSc1d1	blízká
<g/>
,	,	kIx,	,
příbuzenská	příbuzenský	k2eAgFnSc1d1	příbuzenská
láska	láska	k1gFnSc1	láska
</s>
</p>
<p>
<s>
Filia	Filia	k1gFnSc1	Filia
-	-	kIx~	-
přátelství	přátelství	k1gNnSc1	přátelství
</s>
</p>
<p>
<s>
Erós	erós	k1gInSc1	erós
–	–	k?	–
vášnivá	vášnivý	k2eAgFnSc1d1	vášnivá
láska	láska	k1gFnSc1	láska
</s>
</p>
<p>
<s>
Agapé	Agapý	k2eAgInPc1d1	Agapý
–	–	k?	–
oddaná	oddaný	k2eAgFnSc1d1	oddaná
láska	láska	k1gFnSc1	láska
<g/>
.	.	kIx.	.
<g/>
(	(	kIx(	(
<g/>
Podrobněji	podrobně	k6eAd2	podrobně
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
a	a	k8xC	a
uvádějí	uvádět	k5eAaImIp3nP	uvádět
například	například	k6eAd1	například
tyto	tento	k3xDgInPc4	tento
významy	význam	k1gInPc4	význam
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
klasická	klasický	k2eAgFnSc1d1	klasická
<g/>
"	"	kIx"	"
láska	láska	k1gFnSc1	láska
<g/>
:	:	kIx,	:
Silný	silný	k2eAgInSc1d1	silný
citový	citový	k2eAgInSc1d1	citový
vztah	vztah	k1gInSc1	vztah
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgFnPc7	dva
osobami	osoba	k1gFnPc7	osoba
<g/>
,	,	kIx,	,
založený	založený	k2eAgInSc1d1	založený
na	na	k7c6	na
vzájemné	vzájemný	k2eAgFnSc6d1	vzájemná
znalosti	znalost	k1gFnSc6	znalost
a	a	k8xC	a
důvěře	důvěra	k1gFnSc6	důvěra
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
prakticky	prakticky	k6eAd1	prakticky
o	o	k7c6	o
velmi	velmi	k6eAd1	velmi
pevné	pevný	k2eAgFnSc3d1	pevná
<g/>
,	,	kIx,	,
trvalé	trvalý	k2eAgFnSc3d1	trvalá
a	a	k8xC	a
spíše	spíše	k9	spíše
výlučné	výlučný	k2eAgNnSc4d1	výlučné
přátelství	přátelství	k1gNnSc4	přátelství
<g/>
,	,	kIx,	,
většinou	většina	k1gFnSc7	většina
provázené	provázený	k2eAgNnSc1d1	provázené
sexuálním	sexuální	k2eAgInSc7d1	sexuální
stykem	styk	k1gInSc7	styk
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Milenecká	milenecký	k2eAgFnSc1d1	milenecká
láska	láska	k1gFnSc1	láska
se	se	k3xPyFc4	se
v	v	k7c6	v
ideálním	ideální	k2eAgInSc6d1	ideální
případě	případ	k1gInSc6	případ
transformuje	transformovat	k5eAaBmIp3nS	transformovat
v	v	k7c4	v
lásku	láska	k1gFnSc4	láska
manželskou	manželský	k2eAgFnSc4d1	manželská
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
opírá	opírat	k5eAaImIp3nS	opírat
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
S	s	k7c7	s
<g/>
:	:	kIx,	:
stesk	stesk	k1gInSc1	stesk
<g/>
,	,	kIx,	,
soucit	soucit	k1gInSc1	soucit
<g/>
,	,	kIx,	,
starost	starost	k1gFnSc4	starost
<g/>
.	.	kIx.	.
</s>
<s>
Stesk	stesk	k1gInSc1	stesk
po	po	k7c6	po
partnerovi	partner	k1gMnSc6	partner
v	v	k7c6	v
odloučení	odloučení	k1gNnSc6	odloučení
<g/>
,	,	kIx,	,
soucit	soucit	k1gInSc4	soucit
s	s	k7c7	s
jeho	jeho	k3xOp3gNnSc7	jeho
bolestí	bolestit	k5eAaImIp3nS	bolestit
a	a	k8xC	a
starost	starost	k1gFnSc4	starost
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
něco	něco	k6eAd1	něco
nestalo	stát	k5eNaPmAgNnS	stát
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
to	ten	k3xDgNnSc4	ten
cítíte	cítit	k5eAaImIp2nP	cítit
<g/>
,	,	kIx,	,
máte	mít	k5eAaImIp2nP	mít
toho	ten	k3xDgNnSc2	ten
druhého	druhý	k4xOgMnSc4	druhý
člověka	člověk	k1gMnSc4	člověk
rád	rád	k6eAd1	rád
<g/>
.	.	kIx.	.
</s>
<s>
Říkal	říkat	k5eAaImAgMnS	říkat
bych	by	kYmCp1nS	by
tomu	ten	k3xDgNnSc3	ten
spíš	spát	k5eAaImIp2nS	spát
manželské	manželský	k2eAgNnSc1d1	manželské
citové	citový	k2eAgNnSc1d1	citové
spříznění	spříznění	k1gNnSc1	spříznění
než	než	k8xS	než
manželská	manželský	k2eAgFnSc1d1	manželská
láska	láska	k1gFnSc1	láska
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
--Miroslav	--Miroslav	k1gMnSc1	--Miroslav
Plzák	Plzák	k1gMnSc1	Plzák
</s>
</p>
<p>
<s>
zamilovanost	zamilovanost	k1gFnSc1	zamilovanost
<g/>
:	:	kIx,	:
emoční	emoční	k2eAgInSc1d1	emoční
stav	stav	k1gInSc1	stav
vyznačující	vyznačující	k2eAgFnSc2d1	vyznačující
se	se	k3xPyFc4	se
touhou	touha	k1gFnSc7	touha
a	a	k8xC	a
závislostí	závislost	k1gFnSc7	závislost
na	na	k7c6	na
předmětu	předmět	k1gInSc6	předmět
touhy	touha	k1gFnSc2	touha
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
člověka	člověk	k1gMnSc4	člověk
v	v	k7c6	v
nepřítomnosti	nepřítomnost	k1gFnSc6	nepřítomnost
druhého	druhý	k4xOgNnSc2	druhý
resp.	resp.	kA	resp.
objektu	objekt	k1gInSc2	objekt
tíží	tížit	k5eAaImIp3nS	tížit
stesk	stesk	k1gInSc1	stesk
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
i	i	k9	i
žárlivost	žárlivost	k1gFnSc1	žárlivost
<g/>
.	.	kIx.	.
</s>
<s>
Zamilovaný	zamilovaný	k2eAgMnSc1d1	zamilovaný
člověk	člověk	k1gMnSc1	člověk
přehlíží	přehlížet	k5eAaImIp3nS	přehlížet
chyby	chyba	k1gFnPc4	chyba
a	a	k8xC	a
nedostatky	nedostatek	k1gInPc4	nedostatek
druhého	druhý	k4xOgMnSc4	druhý
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
by	by	kYmCp3nP	by
byly	být	k5eAaImAgFnP	být
sebevíc	sebevíc	k6eAd1	sebevíc
zjevné	zjevný	k2eAgFnPc1d1	zjevná
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
V	v	k7c6	v
některých	některý	k3yIgNnPc6	některý
pojetích	pojetí	k1gNnPc6	pojetí
je	být	k5eAaImIp3nS	být
láska	láska	k1gFnSc1	láska
ztotožňována	ztotožňován	k2eAgFnSc1d1	ztotožňována
se	s	k7c7	s
zamilovaností	zamilovanost	k1gFnSc7	zamilovanost
<g/>
,	,	kIx,	,
v	v	k7c6	v
jiných	jiná	k1gFnPc6	jiná
je	být	k5eAaImIp3nS	být
zamilovanost	zamilovanost	k1gFnSc1	zamilovanost
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
dočasnou	dočasný	k2eAgFnSc4d1	dočasná
fázi	fáze	k1gFnSc4	fáze
lásky	láska	k1gFnSc2	láska
a	a	k8xC	a
v	v	k7c6	v
dalších	další	k1gNnPc6	další
je	být	k5eAaImIp3nS	být
zamilovanost	zamilovanost	k1gFnSc4	zamilovanost
od	od	k7c2	od
lásky	láska	k1gFnSc2	láska
zcela	zcela	k6eAd1	zcela
odlišována	odlišován	k2eAgFnSc1d1	odlišována
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
platonická	platonický	k2eAgFnSc1d1	platonická
láska	láska	k1gFnSc1	láska
<g/>
:	:	kIx,	:
vztah	vztah	k1gInSc1	vztah
velmi	velmi	k6eAd1	velmi
podobný	podobný	k2eAgInSc1d1	podobný
klasické	klasický	k2eAgFnSc3d1	klasická
lásce	láska	k1gFnSc3	láska
(	(	kIx(	(
<g/>
zamilovanosti	zamilovanost	k1gFnSc2	zamilovanost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bez	bez	k7c2	bez
sexuality	sexualita	k1gFnSc2	sexualita
(	(	kIx(	(
<g/>
partneři	partner	k1gMnPc1	partner
spolu	spolu	k6eAd1	spolu
nemají	mít	k5eNaImIp3nP	mít
pohlavní	pohlavní	k2eAgInSc4d1	pohlavní
styk	styk	k1gInSc4	styk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Termínem	termín	k1gInSc7	termín
platonická	platonický	k2eAgFnSc1d1	platonická
láska	láska	k1gFnSc1	láska
se	se	k3xPyFc4	se
v	v	k7c6	v
běžné	běžný	k2eAgFnSc6d1	běžná
řeči	řeč	k1gFnSc6	řeč
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
označuje	označovat	k5eAaImIp3nS	označovat
láska	láska	k1gFnSc1	láska
neopětovaná	opětovaný	k2eNgFnSc1d1	neopětovaná
(	(	kIx(	(
<g/>
obzvláště	obzvláště	k6eAd1	obzvláště
v	v	k7c6	v
pubertálním	pubertální	k2eAgInSc6d1	pubertální
věku	věk	k1gInSc6	věk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
toto	tento	k3xDgNnSc1	tento
označení	označení	k1gNnSc1	označení
je	být	k5eAaImIp3nS	být
však	však	k9	však
mylné	mylný	k2eAgNnSc1d1	mylné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
mateřská	mateřský	k2eAgFnSc1d1	mateřská
(	(	kIx(	(
<g/>
příbuzenská	příbuzenský	k2eAgFnSc1d1	příbuzenská
<g/>
)	)	kIx)	)
láska	láska	k1gFnSc1	láska
<g/>
:	:	kIx,	:
nezištná	zištný	k2eNgFnSc1d1	nezištná
láska	láska	k1gFnSc1	láska
typicky	typicky	k6eAd1	typicky
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
sexuálního	sexuální	k2eAgInSc2d1	sexuální
podtextu	podtext	k1gInSc2	podtext
<g/>
,	,	kIx,	,
charakterizovaná	charakterizovaný	k2eAgFnSc1d1	charakterizovaná
péčí	péče	k1gFnSc7	péče
o	o	k7c4	o
druhého	druhý	k4xOgMnSc4	druhý
<g/>
,	,	kIx,	,
pomocí	pomoc	k1gFnSc7	pomoc
a	a	k8xC	a
plněním	plnění	k1gNnSc7	plnění
jeho	jeho	k3xOp3gFnPc2	jeho
potřeb	potřeba	k1gFnPc2	potřeba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
láska	láska	k1gFnSc1	láska
k	k	k7c3	k
idolu	idol	k1gInSc3	idol
<g/>
:	:	kIx,	:
velmi	velmi	k6eAd1	velmi
častá	častý	k2eAgFnSc1d1	častá
v	v	k7c6	v
pubertálním	pubertální	k2eAgInSc6d1	pubertální
věku	věk	k1gInSc6	věk
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
prakticky	prakticky	k6eAd1	prakticky
o	o	k7c6	o
zbožňování	zbožňování	k1gNnSc6	zbožňování
určité	určitý	k2eAgFnSc2d1	určitá
osoby	osoba	k1gFnSc2	osoba
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
příbuzného	příbuzný	k1gMnSc4	příbuzný
<g/>
,	,	kIx,	,
učitele	učitel	k1gMnSc4	učitel
<g/>
,	,	kIx,	,
známého	známý	k1gMnSc4	známý
umělce	umělec	k1gMnSc4	umělec
<g/>
,	,	kIx,	,
politického	politický	k2eAgMnSc4d1	politický
vůdce	vůdce	k1gMnSc4	vůdce
atd.	atd.	kA	atd.
<g/>
Láska	láska	k1gFnSc1	láska
zpravidla	zpravidla	k6eAd1	zpravidla
souvisí	souviset	k5eAaImIp3nS	souviset
se	s	k7c7	s
sexuální	sexuální	k2eAgFnSc7d1	sexuální
orientací	orientace	k1gFnSc7	orientace
jedince	jedinec	k1gMnSc2	jedinec
a	a	k8xC	a
erotické	erotický	k2eAgFnSc2d1	erotická
lásky	láska	k1gFnSc2	láska
je	být	k5eAaImIp3nS	být
jedinec	jedinec	k1gMnSc1	jedinec
schopen	schopen	k2eAgMnSc1d1	schopen
právě	právě	k6eAd1	právě
k	k	k7c3	k
osobě	osoba	k1gFnSc3	osoba
toho	ten	k3xDgNnSc2	ten
pohlaví	pohlaví	k1gNnSc2	pohlaví
a	a	k8xC	a
toho	ten	k3xDgInSc2	ten
věku	věk	k1gInSc2	věk
<g/>
,	,	kIx,	,
na	na	k7c4	na
který	který	k3yQgInSc4	který
je	být	k5eAaImIp3nS	být
sexuálně	sexuálně	k6eAd1	sexuálně
orientován	orientován	k2eAgInSc1d1	orientován
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Sebeláska	sebeláska	k1gFnSc1	sebeláska
===	===	k?	===
</s>
</p>
<p>
<s>
Zdravé	zdravý	k2eAgNnSc1d1	zdravé
sebevědomí	sebevědomí	k1gNnSc1	sebevědomí
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
označit	označit	k5eAaPmF	označit
za	za	k7c4	za
lásku	láska	k1gFnSc4	láska
k	k	k7c3	k
sobě	se	k3xPyFc3	se
samému	samý	k3xTgMnSc3	samý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
ochotu	ochota	k1gFnSc4	ochota
člověka	člověk	k1gMnSc2	člověk
dbát	dbát	k5eAaImF	dbát
o	o	k7c4	o
své	svůj	k3xOyFgNnSc4	svůj
tělo	tělo	k1gNnSc4	tělo
<g/>
,	,	kIx,	,
zlepšovat	zlepšovat	k5eAaImF	zlepšovat
se	se	k3xPyFc4	se
jak	jak	k8xS	jak
po	po	k7c6	po
fyzické	fyzický	k2eAgFnSc6d1	fyzická
<g/>
,	,	kIx,	,
tak	tak	k9	tak
duševní	duševní	k2eAgFnSc6d1	duševní
stránce	stránka	k1gFnSc6	stránka
a	a	k8xC	a
tolerovat	tolerovat	k5eAaImF	tolerovat
některé	některý	k3yIgInPc4	některý
své	svůj	k3xOyFgInPc4	svůj
nedostatky	nedostatek	k1gInPc4	nedostatek
<g/>
.	.	kIx.	.
</s>
<s>
Přehnaná	přehnaný	k2eAgFnSc1d1	přehnaná
láska	láska	k1gFnSc1	láska
k	k	k7c3	k
sobě	se	k3xPyFc3	se
samému	samý	k3xTgMnSc3	samý
spojená	spojený	k2eAgFnSc1d1	spojená
s	s	k7c7	s
přehlížením	přehlížení	k1gNnSc7	přehlížení
okolního	okolní	k2eAgInSc2d1	okolní
světa	svět	k1gInSc2	svět
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
narcismus	narcismus	k1gInSc1	narcismus
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
řecké	řecký	k2eAgFnSc2d1	řecká
báje	báj	k1gFnSc2	báj
o	o	k7c6	o
krásném	krásný	k2eAgMnSc6d1	krásný
mladíkovi	mladík	k1gMnSc6	mladík
Narcisovi	Narcis	k1gMnSc6	Narcis
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
zamiloval	zamilovat	k5eAaPmAgMnS	zamilovat
do	do	k7c2	do
vlastního	vlastní	k2eAgInSc2d1	vlastní
odrazu	odraz	k1gInSc2	odraz
na	na	k7c6	na
hladině	hladina	k1gFnSc6	hladina
studánky	studánka	k1gFnSc2	studánka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Impersonální	impersonální	k2eAgFnSc3d1	impersonální
(	(	kIx(	(
<g/>
neosobní	osobní	k2eNgFnSc1d1	neosobní
<g/>
)	)	kIx)	)
láska	láska	k1gFnSc1	láska
===	===	k?	===
</s>
</p>
<p>
<s>
Impersonální	impersonální	k2eAgFnSc1d1	impersonální
láska	láska	k1gFnSc1	láska
je	být	k5eAaImIp3nS	být
láska	láska	k1gFnSc1	láska
ke	k	k7c3	k
zvířatům	zvíře	k1gNnPc3	zvíře
<g/>
,	,	kIx,	,
předmětům	předmět	k1gInPc3	předmět
<g/>
,	,	kIx,	,
hodnotám	hodnota	k1gFnPc3	hodnota
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
činnostem	činnost	k1gFnPc3	činnost
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
bez	bez	k7c2	bez
sexuálního	sexuální	k2eAgInSc2d1	sexuální
faktoru	faktor	k1gInSc2	faktor
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
tato	tento	k3xDgFnSc1	tento
kategorie	kategorie	k1gFnSc1	kategorie
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
subjektivní	subjektivní	k2eAgMnSc1d1	subjektivní
a	a	k8xC	a
obtížně	obtížně	k6eAd1	obtížně
definovatelná	definovatelný	k2eAgFnSc1d1	definovatelná
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
tu	tu	k6eAd1	tu
ale	ale	k9	ale
rozlišit	rozlišit	k5eAaPmF	rozlišit
několik	několik	k4yIc4	několik
nejznámějších	známý	k2eAgInPc2d3	nejznámější
druhů	druh	k1gInPc2	druh
impersonální	impersonální	k2eAgFnSc2d1	impersonální
lásky	láska	k1gFnSc2	láska
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Láska	láska	k1gFnSc1	láska
k	k	k7c3	k
vlasti	vlast	k1gFnSc3	vlast
(	(	kIx(	(
<g/>
vlastenectví	vlastenectví	k1gNnSc1	vlastenectví
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vlastenectví	vlastenectví	k1gNnSc1	vlastenectví
(	(	kIx(	(
<g/>
patriotismus	patriotismus	k1gInSc1	patriotismus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
láska	láska	k1gFnSc1	láska
k	k	k7c3	k
vlasti	vlast	k1gFnSc3	vlast
nebo	nebo	k8xC	nebo
k	k	k7c3	k
určitému	určitý	k2eAgNnSc3d1	určité
zeměpisnému	zeměpisný	k2eAgNnSc3d1	zeměpisné
<g/>
,	,	kIx,	,
společenskému	společenský	k2eAgNnSc3d1	společenské
<g/>
,	,	kIx,	,
ekonomickému	ekonomický	k2eAgNnSc3d1	ekonomické
<g/>
,	,	kIx,	,
politickému	politický	k2eAgNnSc3d1	politické
a	a	k8xC	a
kulturnímu	kulturní	k2eAgNnSc3d1	kulturní
prostředí	prostředí	k1gNnSc3	prostředí
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgNnSc6	jenž
národ	národ	k1gInSc1	národ
existuje	existovat	k5eAaImIp3nS	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
úzce	úzko	k6eAd1	úzko
pojatého	pojatý	k2eAgInSc2d1	pojatý
nacionalismu	nacionalismus	k1gInSc2	nacionalismus
nevylučuje	vylučovat	k5eNaImIp3nS	vylučovat
úctu	úcta	k1gFnSc4	úcta
k	k	k7c3	k
jiným	jiný	k2eAgInPc3d1	jiný
národům	národ	k1gInPc3	národ
<g/>
.	.	kIx.	.
<g/>
Láska	láska	k1gFnSc1	láska
k	k	k7c3	k
lidstvuLáska	lidstvuLásek	k1gMnSc4	lidstvuLásek
k	k	k7c3	k
lidstvu	lidstvo	k1gNnSc3	lidstvo
jako	jako	k8xS	jako
celku	celek	k1gInSc3	celek
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
obecně	obecně	k6eAd1	obecně
k	k	k7c3	k
celému	celý	k2eAgInSc3d1	celý
světu	svět	k1gInSc3	svět
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
také	také	k9	také
zařadit	zařadit	k5eAaPmF	zařadit
do	do	k7c2	do
kategorie	kategorie	k1gFnSc2	kategorie
impersonální	impersonální	k2eAgFnSc2d1	impersonální
lásky	láska	k1gFnSc2	láska
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
zde	zde	k6eAd1	zde
nejde	jít	k5eNaImIp3nS	jít
přímo	přímo	k6eAd1	přímo
o	o	k7c4	o
lásku	láska	k1gFnSc4	láska
k	k	k7c3	k
lidem	člověk	k1gMnPc3	člověk
jako	jako	k8xC	jako
jednotlivcům	jednotlivec	k1gMnPc3	jednotlivec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
spíše	spíše	k9	spíše
o	o	k7c4	o
lásku	láska	k1gFnSc4	láska
k	k	k7c3	k
ideji	idea	k1gFnSc3	idea
lidstva	lidstvo	k1gNnSc2	lidstvo
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc3	jeho
hodnotám	hodnota	k1gFnPc3	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Podobným	podobný	k2eAgInSc7d1	podobný
postojem	postoj	k1gInSc7	postoj
(	(	kIx(	(
<g/>
ovšem	ovšem	k9	ovšem
s	s	k7c7	s
přítomností	přítomnost	k1gFnSc7	přítomnost
sexuálního	sexuální	k2eAgInSc2d1	sexuální
faktoru	faktor	k1gInSc2	faktor
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
vyznačovalo	vyznačovat	k5eAaImAgNnS	vyznačovat
hnutí	hnutí	k1gNnSc1	hnutí
Hippies	Hippiesa	k1gFnPc2	Hippiesa
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
působilo	působit	k5eAaImAgNnS	působit
v	v	k7c6	v
USA	USA	kA	USA
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
<g/>
Další	další	k2eAgInPc1d1	další
velmi	velmi	k6eAd1	velmi
rozšířené	rozšířený	k2eAgInPc1d1	rozšířený
druhy	druh	k1gInPc1	druh
impersonální	impersonální	k2eAgFnSc2d1	impersonální
lásky	láska	k1gFnSc2	láska
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
láska	láska	k1gFnSc1	láska
k	k	k7c3	k
jídlu	jídlo	k1gNnSc3	jídlo
<g/>
,	,	kIx,	,
penězům	peníze	k1gInPc3	peníze
<g/>
,	,	kIx,	,
zvířatům	zvíře	k1gNnPc3	zvíře
<g/>
,	,	kIx,	,
hudbě	hudba	k1gFnSc3	hudba
<g/>
,	,	kIx,	,
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
práci	práce	k1gFnSc3	práce
<g/>
,	,	kIx,	,
k	k	k7c3	k
určitému	určitý	k2eAgInSc3d1	určitý
předmětu	předmět	k1gInSc3	předmět
ve	v	k7c6	v
škole	škola	k1gFnSc6	škola
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
v	v	k7c6	v
impersonální	impersonální	k2eAgFnSc6d1	impersonální
lásce	láska	k1gFnSc6	láska
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
i	i	k9	i
sexuální	sexuální	k2eAgInSc1d1	sexuální
faktor	faktor	k1gInSc1	faktor
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
mluví	mluvit	k5eAaImIp3nS	mluvit
o	o	k7c6	o
tzv.	tzv.	kA	tzv.
parafilii	parafilie	k1gFnSc6	parafilie
(	(	kIx(	(
<g/>
sexuální	sexuální	k2eAgFnSc4d1	sexuální
deviaci	deviace	k1gFnSc4	deviace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Další	další	k2eAgInPc4d1	další
významy	význam	k1gInPc4	význam
===	===	k?	===
</s>
</p>
<p>
<s>
Pohlavní	pohlavní	k2eAgInSc1d1	pohlavní
styk	styk	k1gInSc1	styk
se	se	k3xPyFc4	se
často	často	k6eAd1	často
označuje	označovat	k5eAaImIp3nS	označovat
slovním	slovní	k2eAgInSc7d1	slovní
obratem	obrat	k1gInSc7	obrat
"	"	kIx"	"
<g/>
milovat	milovat	k5eAaImF	milovat
se	se	k3xPyFc4	se
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ovšem	ovšem	k9	ovšem
sám	sám	k3xTgMnSc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
nemusí	muset	k5eNaImIp3nS	muset
žádný	žádný	k3yNgInSc4	žádný
citový	citový	k2eAgInSc4d1	citový
vztah	vztah	k1gInSc4	vztah
zahrnovat	zahrnovat	k5eAaImF	zahrnovat
<g/>
.	.	kIx.	.
</s>
<s>
Erotická	erotický	k2eAgFnSc1d1	erotická
přitažlivost	přitažlivost	k1gFnSc1	přitažlivost
a	a	k8xC	a
náklonnost	náklonnost	k1gFnSc1	náklonnost
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
silným	silný	k2eAgMnSc7d1	silný
a	a	k8xC	a
možná	možná	k9	možná
i	i	k9	i
nejčastějším	častý	k2eAgInSc7d3	nejčastější
podnětem	podnět	k1gInSc7	podnět
i	i	k8xC	i
součástí	součást	k1gFnSc7	součást
lásky	láska	k1gFnSc2	láska
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgMnPc7	dva
dospělými	dospělý	k2eAgMnPc7d1	dospělý
lidmi	člověk	k1gMnPc7	člověk
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
také	také	k9	také
druhy	druh	k1gInPc1	druh
lásky	láska	k1gFnSc2	láska
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
sexuální	sexuální	k2eAgNnSc4d1	sexuální
spojení	spojení	k1gNnSc4	spojení
vylučují	vylučovat	k5eAaImIp3nP	vylučovat
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
rodičovská	rodičovský	k2eAgFnSc1d1	rodičovská
láska	láska	k1gFnSc1	láska
nebo	nebo	k8xC	nebo
láska	láska	k1gFnSc1	láska
učitele	učitel	k1gMnPc4	učitel
k	k	k7c3	k
dětem	dítě	k1gFnPc3	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Vědní	vědní	k2eAgFnSc1d1	vědní
disciplína	disciplína	k1gFnSc1	disciplína
zabývající	zabývající	k2eAgFnSc1d1	zabývající
se	s	k7c7	s
sexuální	sexuální	k2eAgFnSc7d1	sexuální
láskou	láska	k1gFnSc7	láska
a	a	k8xC	a
jejími	její	k3xOp3gInPc7	její
projevy	projev	k1gInPc7	projev
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
erotologie	erotologie	k1gFnSc1	erotologie
<g/>
.	.	kIx.	.
<g/>
Láska	láska	k1gFnSc1	láska
jako	jako	k9	jako
ctnostLásku	ctnostLásek	k1gInSc2	ctnostLásek
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
i	i	k9	i
za	za	k7c4	za
lidskou	lidský	k2eAgFnSc4d1	lidská
ctnost	ctnost	k1gFnSc4	ctnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
kontextu	kontext	k1gInSc6	kontext
vidí	vidět	k5eAaImIp3nS	vidět
lásku	láska	k1gFnSc4	láska
například	například	k6eAd1	například
křesťanská	křesťanský	k2eAgFnSc1d1	křesťanská
víra	víra	k1gFnSc1	víra
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
význam	význam	k1gInSc1	význam
je	být	k5eAaImIp3nS	být
zaměnitelný	zaměnitelný	k2eAgInSc1d1	zaměnitelný
za	za	k7c4	za
lásku	láska	k1gFnSc4	láska
k	k	k7c3	k
lidstvu	lidstvo	k1gNnSc3	lidstvo
<g/>
,	,	kIx,	,
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
případech	případ	k1gInPc6	případ
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
lásku	láska	k1gFnSc4	láska
k	k	k7c3	k
lidem	člověk	k1gMnPc3	člověk
jako	jako	k8xS	jako
celku	celek	k1gInSc2	celek
a	a	k8xC	a
ochotu	ochota	k1gFnSc4	ochota
pomáhat	pomáhat	k5eAaImF	pomáhat
ostatním	ostatní	k1gNnSc7	ostatní
<g/>
.	.	kIx.	.
</s>
<s>
Chování	chování	k1gNnSc1	chování
sledující	sledující	k2eAgNnSc1d1	sledující
prospěch	prospěch	k1gInSc4	prospěch
ostatních	ostatní	k2eAgMnPc2d1	ostatní
lidí	člověk	k1gMnPc2	člověk
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
altruismus	altruismus	k1gInSc1	altruismus
<g/>
.	.	kIx.	.
<g/>
Náboženská	náboženský	k2eAgFnSc1d1	náboženská
láska	láska	k1gFnSc1	láska
(	(	kIx(	(
<g/>
láska	láska	k1gFnSc1	láska
k	k	k7c3	k
Bohu	bůh	k1gMnSc3	bůh
<g/>
)	)	kIx)	)
<g/>
V	v	k7c6	v
křesťanství	křesťanství	k1gNnSc6	křesťanství
je	být	k5eAaImIp3nS	být
láska	láska	k1gFnSc1	láska
základní	základní	k2eAgFnSc1d1	základní
charakteristikou	charakteristika	k1gFnSc7	charakteristika
vztahu	vztah	k1gInSc2	vztah
jednak	jednak	k8xC	jednak
mezi	mezi	k7c7	mezi
Bohem	bůh	k1gMnSc7	bůh
a	a	k8xC	a
člověkem	člověk	k1gMnSc7	člověk
a	a	k8xC	a
jednak	jednak	k8xC	jednak
mezi	mezi	k7c7	mezi
lidmi	člověk	k1gMnPc7	člověk
navzájem	navzájem	k6eAd1	navzájem
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
také	také	k9	také
hlavním	hlavní	k2eAgInSc7d1	hlavní
požadavkem	požadavek	k1gInSc7	požadavek
kladeným	kladený	k2eAgInSc7d1	kladený
na	na	k7c4	na
každého	každý	k3xTgMnSc4	každý
křesťana	křesťan	k1gMnSc4	křesťan
<g/>
.	.	kIx.	.
</s>
<s>
Papež	Papež	k1gMnSc1	Papež
Benedikt	Benedikt	k1gMnSc1	Benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
sepsal	sepsat	k5eAaPmAgMnS	sepsat
encykliku	encyklika	k1gFnSc4	encyklika
Deus	Deusa	k1gFnPc2	Deusa
caritas	caritas	k1gInSc4	caritas
est	est	k?	est
(	(	kIx(	(
<g/>
Bůh	bůh	k1gMnSc1	bůh
je	být	k5eAaImIp3nS	být
láska	láska	k1gFnSc1	láska
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgInS	zabývat
tématem	téma	k1gNnSc7	téma
lásky	láska	k1gFnSc2	láska
v	v	k7c6	v
Křesťanství	křesťanství	k1gNnSc6	křesťanství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mnoho	mnoho	k4c1	mnoho
náboženství	náboženství	k1gNnPc2	náboženství
také	také	k9	také
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
sexuální	sexuální	k2eAgFnSc4d1	sexuální
lásku	láska	k1gFnSc4	láska
mezi	mezi	k7c7	mezi
lidmi	člověk	k1gMnPc7	člověk
a	a	k8xC	a
lásku	láska	k1gFnSc4	láska
jako	jako	k8xC	jako
základ	základ	k1gInSc4	základ
veškerého	veškerý	k3xTgNnSc2	veškerý
bytí	bytí	k1gNnSc2	bytí
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
jako	jako	k9	jako
způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
jakým	jaký	k3yRgMnSc7	jaký
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
osvícení	osvícení	k1gNnPc4	osvícení
<g/>
.	.	kIx.	.
<g/>
Některé	některý	k3yIgInPc1	některý
biologické	biologický	k2eAgInPc1d1	biologický
a	a	k8xC	a
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
odvozené	odvozený	k2eAgFnPc1d1	odvozená
ideologické	ideologický	k2eAgFnPc1d1	ideologická
teorie	teorie	k1gFnPc1	teorie
pojímají	pojímat	k5eAaImIp3nP	pojímat
lásku	láska	k1gFnSc4	láska
jako	jako	k8xS	jako
funkci	funkce	k1gFnSc4	funkce
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
sloužit	sloužit	k5eAaImF	sloužit
reprodukci	reprodukce	k1gFnSc4	reprodukce
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
schopnost	schopnost	k1gFnSc4	schopnost
získat	získat	k5eAaPmF	získat
vhodného	vhodný	k2eAgMnSc4d1	vhodný
partnera	partner	k1gMnSc4	partner
<g/>
,	,	kIx,	,
udržet	udržet	k5eAaPmF	udržet
si	se	k3xPyFc3	se
jej	on	k3xPp3gNnSc4	on
<g/>
,	,	kIx,	,
reprodukovat	reprodukovat	k5eAaBmF	reprodukovat
se	se	k3xPyFc4	se
a	a	k8xC	a
investovat	investovat	k5eAaBmF	investovat
se	se	k3xPyFc4	se
do	do	k7c2	do
rodičovské	rodičovský	k2eAgFnSc2d1	rodičovská
péče	péče	k1gFnSc2	péče
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Některé	některý	k3yIgFnPc1	některý
definice	definice	k1gFnPc1	definice
a	a	k8xC	a
typologie	typologie	k1gFnSc1	typologie
lásky	láska	k1gFnSc2	láska
==	==	k?	==
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Kladný	kladný	k2eAgInSc4d1	kladný
<g/>
,	,	kIx,	,
silný	silný	k2eAgInSc4d1	silný
emocionální	emocionální	k2eAgInSc4d1	emocionální
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
osobě	osoba	k1gFnSc3	osoba
<g/>
,	,	kIx,	,
ideji	idea	k1gFnSc3	idea
nebo	nebo	k8xC	nebo
věci	věc	k1gFnSc3	věc
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
podobách	podoba	k1gFnPc6	podoba
přecházející	přecházející	k2eAgInSc1d1	přecházející
až	až	k9	až
k	k	k7c3	k
obecnějšímu	obecní	k2eAgNnSc3d2	obecní
ztotožnění	ztotožnění	k1gNnSc3	ztotožnění
s	s	k7c7	s
altruistickými	altruistický	k2eAgFnPc7d1	altruistická
morálními	morální	k2eAgFnPc7d1	morální
hodnotami	hodnota	k1gFnPc7	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
také	také	k9	také
součásti	součást	k1gFnPc1	součást
diagnostické	diagnostický	k2eAgFnSc2d1	diagnostická
bipolární	bipolární	k2eAgFnSc2d1	bipolární
škály	škála	k1gFnSc2	škála
láska	láska	k1gFnSc1	láska
–	–	k?	–
nenávist	nenávist	k1gFnSc1	nenávist
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Psychologický	psychologický	k2eAgInSc1d1	psychologický
slovník	slovník	k1gInSc1	slovník
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Hartl	Hartl	k1gMnSc1	Hartl
a	a	k8xC	a
Helena	Helena	k1gFnSc1	Helena
Hartlová	Hartlová	k1gFnSc1	Hartlová
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Vztah	vztah	k1gInSc1	vztah
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgMnPc7	dva
lidmi	člověk	k1gMnPc7	člověk
charakterizovaný	charakterizovaný	k2eAgInSc1d1	charakterizovaný
silnou	silný	k2eAgFnSc7d1	silná
citovou	citový	k2eAgFnSc7d1	citová
vazbou	vazba	k1gFnSc7	vazba
<g/>
,	,	kIx,	,
nezištností	nezištnost	k1gFnSc7	nezištnost
<g/>
,	,	kIx,	,
stálostí	stálost	k1gFnSc7	stálost
<g/>
,	,	kIx,	,
vytrvalostí	vytrvalost	k1gFnSc7	vytrvalost
<g/>
,	,	kIx,	,
odpovědností	odpovědnost	k1gFnSc7	odpovědnost
a	a	k8xC	a
vzájemnou	vzájemný	k2eAgFnSc7d1	vzájemná
věrností	věrnost	k1gFnSc7	věrnost
<g/>
.	.	kIx.	.
</s>
<s>
Stadium	stadium	k1gNnSc1	stadium
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
může	moct	k5eAaImIp3nS	moct
navazovat	navazovat	k5eAaImF	navazovat
na	na	k7c4	na
stav	stav	k1gInSc4	stav
zamilovanosti	zamilovanost	k1gFnSc2	zamilovanost
a	a	k8xC	a
přitažlivosti	přitažlivost	k1gFnSc2	přitažlivost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přeneseném	přenesený	k2eAgInSc6d1	přenesený
významu	význam	k1gInSc6	význam
též	též	k9	též
vztah	vztah	k1gInSc4	vztah
ke	k	k7c3	k
skupině	skupina	k1gFnSc3	skupina
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
idealizované	idealizovaný	k2eAgFnSc3d1	idealizovaná
osobnosti	osobnost	k1gFnSc3	osobnost
nebo	nebo	k8xC	nebo
ideji	idea	k1gFnSc3	idea
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
silný	silný	k2eAgInSc1d1	silný
<g/>
,	,	kIx,	,
že	že	k8xS	že
jedinec	jedinec	k1gMnSc1	jedinec
klade	klást	k5eAaImIp3nS	klást
sebe	sebe	k3xPyFc4	sebe
a	a	k8xC	a
své	svůj	k3xOyFgInPc4	svůj
zájmy	zájem	k1gInPc4	zájem
až	až	k9	až
na	na	k7c4	na
druhé	druhý	k4xOgNnSc4	druhý
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
významu	význam	k1gInSc6	význam
je	být	k5eAaImIp3nS	být
láska	láska	k1gFnSc1	láska
totožná	totožný	k2eAgFnSc1d1	totožná
s	s	k7c7	s
romantickou	romantický	k2eAgFnSc7d1	romantická
zamilovaností	zamilovanost	k1gFnSc7	zamilovanost
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Sociologický	sociologický	k2eAgInSc1d1	sociologický
slovník	slovník	k1gInSc1	slovník
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Jandourek	Jandourek	k1gMnSc1	Jandourek
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
...	...	k?	...
schopnost	schopnost	k1gFnSc4	schopnost
překročit	překročit	k5eAaPmF	překročit
sebe	sebe	k3xPyFc4	sebe
sama	sám	k3xTgMnSc4	sám
<g/>
,	,	kIx,	,
svůj	svůj	k3xOyFgInSc4	svůj
sobecký	sobecký	k2eAgInSc4d1	sobecký
životní	životní	k2eAgInSc4d1	životní
rozvrh	rozvrh	k1gInSc4	rozvrh
<g/>
,	,	kIx,	,
opustit	opustit	k5eAaPmF	opustit
pole	pole	k1gFnPc4	pole
jen	jen	k9	jen
svých	svůj	k3xOyFgInPc2	svůj
zájmů	zájem	k1gInPc2	zájem
<g/>
,	,	kIx,	,
cíle	cíl	k1gInSc2	cíl
a	a	k8xC	a
prospěchu	prospěch	k1gInSc2	prospěch
<g/>
,	,	kIx,	,
otevřít	otevřít	k5eAaPmF	otevřít
se	se	k3xPyFc4	se
druhému	druhý	k4xOgInSc3	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Někoho	někdo	k3yInSc4	někdo
považovat	považovat	k5eAaImF	považovat
pro	pro	k7c4	pro
sebe	sebe	k3xPyFc4	sebe
za	za	k7c2	za
cennějšího	cenný	k2eAgNnSc2d2	cennější
<g/>
,	,	kIx,	,
než	než	k8xS	než
jsem	být	k5eAaImIp1nS	být
sám	sám	k3xTgMnSc1	sám
sobě	se	k3xPyFc3	se
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Halík	Halík	k1gMnSc1	Halík
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Americký	americký	k2eAgMnSc1d1	americký
psycholog	psycholog	k1gMnSc1	psycholog
Robert	Robert	k1gMnSc1	Robert
J.	J.	kA	J.
Sternberg	Sternberg	k1gMnSc1	Sternberg
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
typologii	typologie	k1gFnSc4	typologie
mezilidských	mezilidský	k2eAgInPc2d1	mezilidský
vztahů	vztah	k1gInPc2	vztah
založenou	založený	k2eAgFnSc4d1	založená
na	na	k7c6	na
třech	tři	k4xCgInPc6	tři
základních	základní	k2eAgInPc6d1	základní
rozměrech	rozměr	k1gInPc6	rozměr
lásky	láska	k1gFnSc2	láska
<g/>
:	:	kIx,	:
vzájemné	vzájemný	k2eAgFnSc2d1	vzájemná
blízkosti	blízkost	k1gFnSc2	blízkost
(	(	kIx(	(
<g/>
intimacy	intimaca	k1gFnSc2	intimaca
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vášni	vášnit	k5eAaImRp2nS	vášnit
(	(	kIx(	(
<g/>
passion	passion	k1gInSc1	passion
<g/>
)	)	kIx)	)
a	a	k8xC	a
oddanosti	oddanost	k1gFnSc2	oddanost
(	(	kIx(	(
<g/>
commitment	commitment	k1gInSc1	commitment
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
základě	základ	k1gInSc6	základ
rozlišoval	rozlišovat	k5eAaImAgMnS	rozlišovat
8	[number]	k4	8
stavů	stav	k1gInPc2	stav
<g/>
:	:	kIx,	:
<g/>
neláska	neláska	k1gFnSc1	neláska
(	(	kIx(	(
<g/>
nonlove	nonlovat	k5eAaPmIp3nS	nonlovat
<g/>
,	,	kIx,	,
nepřítomen	přítomen	k2eNgInSc1d1	nepřítomen
žádný	žádný	k3yNgInSc1	žádný
základní	základní	k2eAgInSc1d1	základní
rozměr	rozměr	k1gInSc1	rozměr
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
zalíbení	zalíbení	k1gNnSc1	zalíbení
(	(	kIx(	(
<g/>
liking	liking	k1gInSc1	liking
<g/>
,	,	kIx,	,
přítomna	přítomen	k2eAgFnSc1d1	přítomna
intimita	intimita	k1gFnSc1	intimita
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
zamilovanost	zamilovanost	k1gFnSc1	zamilovanost
(	(	kIx(	(
<g/>
infatuated	infatuated	k1gInSc1	infatuated
love	lov	k1gInSc5	lov
<g/>
,	,	kIx,	,
přítomna	přítomen	k2eAgFnSc1d1	přítomna
vášeň	vášeň	k1gFnSc1	vášeň
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
prázdná	prázdný	k2eAgFnSc1d1	prázdná
láska	láska	k1gFnSc1	láska
(	(	kIx(	(
<g/>
empty	empt	k1gInPc1	empt
love	lov	k1gInSc5	lov
<g/>
,	,	kIx,	,
přítomna	přítomen	k2eAgFnSc1d1	přítomna
oddanost	oddanost	k1gFnSc1	oddanost
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
romantická	romantický	k2eAgFnSc1d1	romantická
láska	láska	k1gFnSc1	láska
(	(	kIx(	(
<g/>
romantic	romantice	k1gFnPc2	romantice
love	lov	k1gInSc5	lov
<g/>
,	,	kIx,	,
přítomna	přítomen	k2eAgFnSc1d1	přítomna
intimita	intimita	k1gFnSc1	intimita
a	a	k8xC	a
vášeň	vášeň	k1gFnSc1	vášeň
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
přátelská	přátelský	k2eAgFnSc1d1	přátelská
láska	láska	k1gFnSc1	láska
(	(	kIx(	(
<g/>
companionate	companionat	k1gInSc5	companionat
love	lov	k1gInSc5	lov
<g/>
,	,	kIx,	,
přítomna	přítomen	k2eAgFnSc1d1	přítomna
intimita	intimita	k1gFnSc1	intimita
a	a	k8xC	a
oddanost	oddanost	k1gFnSc1	oddanost
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
osudová	osudový	k2eAgFnSc1d1	osudová
láska	láska	k1gFnSc1	láska
(	(	kIx(	(
<g/>
fatuous	fatuous	k1gInSc1	fatuous
love	lov	k1gInSc5	lov
<g/>
,	,	kIx,	,
přítomna	přítomen	k2eAgFnSc1d1	přítomna
vášeň	vášeň	k1gFnSc1	vášeň
a	a	k8xC	a
oddanost	oddanost	k1gFnSc1	oddanost
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
dokonalá	dokonalý	k2eAgFnSc1d1	dokonalá
láska	láska	k1gFnSc1	láska
(	(	kIx(	(
<g/>
consumate	consumat	k1gInSc5	consumat
love	lov	k1gInSc5	lov
<g/>
,	,	kIx,	,
přítomny	přítomen	k2eAgInPc1d1	přítomen
všechny	všechen	k3xTgInPc4	všechen
tři	tři	k4xCgInPc4	tři
rozměry	rozměr	k1gInPc4	rozměr
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Australský	australský	k2eAgMnSc1d1	australský
psycholog	psycholog	k1gMnSc1	psycholog
J.	J.	kA	J.
T.	T.	kA	T.
A.	A.	kA	A.
Condon	Condon	k1gMnSc1	Condon
lásku	láska	k1gFnSc4	láska
charakterizoval	charakterizovat	k5eAaBmAgMnS	charakterizovat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
své	svůj	k3xOyFgFnSc2	svůj
teorie	teorie	k1gFnSc2	teorie
citové	citový	k2eAgFnSc2d1	citová
vazby	vazba	k1gFnSc2	vazba
(	(	kIx(	(
<g/>
attachment	attachment	k1gInSc1	attachment
<g/>
,	,	kIx,	,
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
se	se	k3xPyFc4	se
překládá	překládat	k5eAaImIp3nS	překládat
též	též	k9	též
jako	jako	k9	jako
přilnutí	přilnutí	k1gNnSc1	přilnutí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kanadská	kanadský	k2eAgFnSc1d1	kanadská
psycholožka	psycholožka	k1gFnSc1	psycholožka
Beverley	Beverlea	k1gFnSc2	Beverlea
Fehrová	Fehrová	k1gFnSc1	Fehrová
rozlišila	rozlišit	k5eAaPmAgFnS	rozlišit
pomocí	pomocí	k7c2	pomocí
faktorové	faktorový	k2eAgFnSc2d1	Faktorová
analýzy	analýza	k1gFnSc2	analýza
9	[number]	k4	9
typů	typ	k1gInPc2	typ
lásky	láska	k1gFnSc2	láska
<g/>
:	:	kIx,	:
<g/>
zalíbení	zalíbení	k1gNnSc4	zalíbení
(	(	kIx(	(
<g/>
affection	affection	k1gInSc1	affection
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
přátelská	přátelský	k2eAgFnSc1d1	přátelská
neintenzivní	intenzivní	k2eNgFnSc1d1	intenzivní
láska	láska	k1gFnSc1	láska
<g/>
,	,	kIx,	,
vzájemná	vzájemný	k2eAgFnSc1d1	vzájemná
blízkost	blízkost	k1gFnSc1	blízkost
<g/>
,	,	kIx,	,
s	s	k7c7	s
objímáním	objímání	k1gNnSc7	objímání
a	a	k8xC	a
polibky	polibek	k1gInPc7	polibek
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
sexuálního	sexuální	k2eAgInSc2d1	sexuální
styku	styk	k1gInSc2	styk
</s>
</p>
<p>
<s>
sexuální	sexuální	k2eAgFnSc1d1	sexuální
láska	láska	k1gFnSc1	láska
(	(	kIx(	(
<g/>
sexual	sexual	k1gInSc1	sexual
love	lov	k1gInSc5	lov
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
zamilovanost	zamilovanost	k1gFnSc1	zamilovanost
<g/>
,	,	kIx,	,
chtíč	chtíč	k1gInSc1	chtíč
<g/>
,	,	kIx,	,
závislost	závislost	k1gFnSc1	závislost
<g/>
,	,	kIx,	,
spíše	spíše	k9	spíše
krátkodobá	krátkodobý	k2eAgFnSc1d1	krátkodobá
láska	láska	k1gFnSc1	láska
</s>
</p>
<p>
<s>
platonická	platonický	k2eAgFnSc1d1	platonická
láska	láska	k1gFnSc1	láska
(	(	kIx(	(
<g/>
platonic	platonice	k1gFnPc2	platonice
love	lov	k1gInSc5	lov
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
přátelská	přátelský	k2eAgFnSc1d1	přátelská
intenzivní	intenzivní	k2eAgFnSc1d1	intenzivní
láska	láska	k1gFnSc1	láska
<g/>
,	,	kIx,	,
velká	velký	k2eAgFnSc1d1	velká
vzájemná	vzájemný	k2eAgFnSc1d1	vzájemná
důvěra	důvěra	k1gFnSc1	důvěra
a	a	k8xC	a
spřízněnost	spřízněnost	k1gFnSc1	spřízněnost
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
sexuálního	sexuální	k2eAgInSc2d1	sexuální
styku	styk	k1gInSc2	styk
</s>
</p>
<p>
<s>
přátelství	přátelství	k1gNnSc1	přátelství
(	(	kIx(	(
<g/>
friendship	friendship	k1gInSc1	friendship
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
empatie	empatie	k1gFnPc4	empatie
<g/>
,	,	kIx,	,
sdílení	sdílení	k1gNnSc4	sdílení
společných	společný	k2eAgInPc2d1	společný
zájmů	zájem	k1gInPc2	zájem
<g/>
,	,	kIx,	,
upřímnost	upřímnost	k1gFnSc1	upřímnost
<g/>
,	,	kIx,	,
podpora	podpora	k1gFnSc1	podpora
druhého	druhý	k4xOgMnSc4	druhý
</s>
</p>
<p>
<s>
romantická	romantický	k2eAgFnSc1d1	romantická
láska	láska	k1gFnSc1	láska
(	(	kIx(	(
<g/>
romantic	romantice	k1gFnPc2	romantice
love	lov	k1gInSc5	lov
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
idealistická	idealistický	k2eAgFnSc1d1	idealistická
<g/>
,	,	kIx,	,
pohádková	pohádkový	k2eAgFnSc1d1	pohádková
láska	láska	k1gFnSc1	láska
<g/>
,	,	kIx,	,
zamilovanost	zamilovanost	k1gFnSc1	zamilovanost
<g/>
,	,	kIx,	,
nervozita	nervozita	k1gFnSc1	nervozita
<g/>
,	,	kIx,	,
trávení	trávení	k1gNnSc1	trávení
času	čas	k1gInSc2	čas
společně	společně	k6eAd1	společně
</s>
</p>
<p>
<s>
dětská	dětský	k2eAgFnSc1d1	dětská
láska	láska	k1gFnSc1	láska
(	(	kIx(	(
<g/>
puppy	pupp	k1gInPc1	pupp
love	lov	k1gInSc5	lov
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
krátkodobá	krátkodobý	k2eAgFnSc1d1	krátkodobá
zamilovanost	zamilovanost	k1gFnSc1	zamilovanost
<g/>
,	,	kIx,	,
nerozumné	rozumný	k2eNgNnSc1d1	nerozumné
chování	chování	k1gNnSc1	chování
<g/>
,	,	kIx,	,
stydlivost	stydlivost	k1gFnSc1	stydlivost
</s>
</p>
<p>
<s>
vášnivá	vášnivý	k2eAgFnSc1d1	vášnivá
láska	láska	k1gFnSc1	láska
(	(	kIx(	(
<g/>
passionate	passionat	k1gInSc5	passionat
love	lov	k1gInSc5	lov
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
intenzivní	intenzivní	k2eAgFnSc1d1	intenzivní
sexuální	sexuální	k2eAgFnSc1d1	sexuální
láska	láska	k1gFnSc1	láska
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
krátkodobá	krátkodobý	k2eAgFnSc1d1	krátkodobá
<g/>
,	,	kIx,	,
velká	velký	k2eAgFnSc1d1	velká
intimita	intimita	k1gFnSc1	intimita
</s>
</p>
<p>
<s>
pobláznění	pobláznění	k1gNnSc1	pobláznění
(	(	kIx(	(
<g/>
infatuation	infatuation	k1gInSc1	infatuation
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
slepá	slepý	k2eAgFnSc1d1	slepá
zamilovanost	zamilovanost	k1gFnSc1	zamilovanost
<g/>
,	,	kIx,	,
krátkodobá	krátkodobý	k2eAgFnSc1d1	krátkodobá
láska	láska	k1gFnSc1	láska
</s>
</p>
<p>
<s>
oddanost	oddanost	k1gFnSc1	oddanost
(	(	kIx(	(
<g/>
commited	commited	k1gInSc1	commited
love	lov	k1gInSc5	lov
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
dlouhodobá	dlouhodobý	k2eAgFnSc1d1	dlouhodobá
oddaná	oddaný	k2eAgFnSc1d1	oddaná
láska	láska	k1gFnSc1	láska
<g/>
,	,	kIx,	,
vzájemná	vzájemný	k2eAgFnSc1d1	vzájemná
důvěra	důvěra	k1gFnSc1	důvěra
a	a	k8xC	a
blízkost	blízkost	k1gFnSc1	blízkost
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
se	s	k7c7	s
sexuálním	sexuální	k2eAgInSc7d1	sexuální
stykem	styk	k1gInSc7	styk
</s>
</p>
<p>
<s>
Asi	asi	k9	asi
nejtriviálnější	triviální	k2eAgFnSc7d3	nejtriviálnější
definicí	definice	k1gFnSc7	definice
lásky	láska	k1gFnSc2	láska
je	být	k5eAaImIp3nS	být
:	:	kIx,	:
"	"	kIx"	"
<g/>
Láska	láska	k1gFnSc1	láska
je	být	k5eAaImIp3nS	být
oblouznění	oblouznění	k1gNnSc4	oblouznění
jehož	jehož	k3xOyRp3gFnSc1	jehož
doba	doba	k1gFnSc1	doba
je	být	k5eAaImIp3nS	být
přímo	přímo	k6eAd1	přímo
úměrná	úměrný	k2eAgNnPc4d1	úměrné
uspokojení	uspokojení	k1gNnPc4	uspokojení
představ	představa	k1gFnPc2	představa
o	o	k7c6	o
dokonalém	dokonalý	k2eAgInSc6d1	dokonalý
partnerovi	partner	k1gMnSc3	partner
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Daniel	Daniel	k1gMnSc1	Daniel
Bulla	bulla	k1gFnSc1	bulla
</s>
</p>
<p>
<s>
==	==	k?	==
Lidové	lidový	k2eAgFnPc1d1	lidová
teorie	teorie	k1gFnPc1	teorie
o	o	k7c6	o
lásce	láska	k1gFnSc6	láska
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
"	"	kIx"	"
<g/>
Protiklady	protiklad	k1gInPc1	protiklad
se	se	k3xPyFc4	se
přitahují	přitahovat	k5eAaImIp3nP	přitahovat
<g/>
"	"	kIx"	"
===	===	k?	===
</s>
</p>
<p>
<s>
Mezi	mezi	k7c7	mezi
širokou	široký	k2eAgFnSc7d1	široká
veřejností	veřejnost	k1gFnSc7	veřejnost
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
rozšířeno	rozšířen	k2eAgNnSc1d1	rozšířeno
rčení	rčení	k1gNnSc1	rčení
prohlašující	prohlašující	k2eAgNnSc1d1	prohlašující
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	s	k7c7	s
"	"	kIx"	"
<g/>
protiklady	protiklad	k1gInPc1	protiklad
přitahují	přitahovat	k5eAaImIp3nP	přitahovat
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
že	že	k8xS	že
si	se	k3xPyFc3	se
lidé	člověk	k1gMnPc1	člověk
vybírají	vybírat	k5eAaImIp3nP	vybírat
partnery	partner	k1gMnPc4	partner
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
od	od	k7c2	od
nich	on	k3xPp3gMnPc2	on
odlišují	odlišovat	k5eAaImIp3nP	odlišovat
<g/>
.	.	kIx.	.
</s>
<s>
Tomuto	tento	k3xDgNnSc3	tento
tématu	téma	k1gNnSc3	téma
se	se	k3xPyFc4	se
věnovaly	věnovat	k5eAaImAgInP	věnovat
různé	různý	k2eAgInPc1d1	různý
výzkumy	výzkum	k1gInPc1	výzkum
lidských	lidský	k2eAgInPc2d1	lidský
vztahů	vztah	k1gInPc2	vztah
s	s	k7c7	s
rozdílnými	rozdílný	k2eAgInPc7d1	rozdílný
závěry	závěr	k1gInPc7	závěr
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
odborníci	odborník	k1gMnPc1	odborník
věří	věřit	k5eAaImIp3nP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
lidé	člověk	k1gMnPc1	člověk
hledají	hledat	k5eAaImIp3nP	hledat
partnery	partner	k1gMnPc4	partner
s	s	k7c7	s
prakticky	prakticky	k6eAd1	prakticky
identickými	identický	k2eAgFnPc7d1	identická
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
<g/>
,	,	kIx,	,
jiní	jiný	k1gMnPc1	jiný
naopak	naopak	k6eAd1	naopak
prohlašují	prohlašovat	k5eAaImIp3nP	prohlašovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
lidé	člověk	k1gMnPc1	člověk
hledají	hledat	k5eAaImIp3nP	hledat
partnery	partner	k1gMnPc4	partner
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
je	být	k5eAaImIp3nS	být
budou	být	k5eAaImBp3nP	být
svými	svůj	k3xOyFgFnPc7	svůj
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
doplňovat	doplňovat	k5eAaImF	doplňovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
dotazníkovém	dotazníkový	k2eAgInSc6d1	dotazníkový
průzkumu	průzkum	k1gInSc6	průzkum
časopisu	časopis	k1gInSc2	časopis
Evolutionary	Evolutionara	k1gFnSc2	Evolutionara
Psychology	psycholog	k1gMnPc7	psycholog
byla	být	k5eAaImAgFnS	být
prokázána	prokázán	k2eAgFnSc1d1	prokázána
většinová	většinový	k2eAgFnSc1d1	většinová
shoda	shoda	k1gFnSc1	shoda
mezi	mezi	k7c7	mezi
uváděnými	uváděný	k2eAgFnPc7d1	uváděná
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
ideálního	ideální	k2eAgMnSc2d1	ideální
partnera	partner	k1gMnSc2	partner
a	a	k8xC	a
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
vyplňovatelů	vyplňovatel	k1gMnPc2	vyplňovatel
(	(	kIx(	(
<g/>
od	od	k7c2	od
0.51	[number]	k4	0.51
do	do	k7c2	do
0.62	[number]	k4	0.62
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
lidé	člověk	k1gMnPc1	člověk
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
do	do	k7c2	do
dotazníku	dotazník	k1gInSc2	dotazník
uváděli	uvádět	k5eAaImAgMnP	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
jejich	jejich	k3xOp3gMnPc1	jejich
partneři	partner	k1gMnPc1	partner
měli	mít	k5eAaImAgMnP	mít
mít	mít	k5eAaImF	mít
podobné	podobný	k2eAgFnPc4d1	podobná
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byli	být	k5eAaImAgMnP	být
ale	ale	k9	ale
respondenti	respondent	k1gMnPc1	respondent
poté	poté	k6eAd1	poté
dotázáni	dotázán	k2eAgMnPc1d1	dotázán
ústně	ústně	k6eAd1	ústně
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
odpovídali	odpovídat	k5eAaImAgMnP	odpovídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
chtěli	chtít	k5eAaImAgMnP	chtít
partnery	partner	k1gMnPc7	partner
s	s	k7c7	s
opačnými	opačný	k2eAgFnPc7d1	opačná
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
(	(	kIx(	(
<g/>
nejspíše	nejspíše	k9	nejspíše
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
tohoto	tento	k3xDgNnSc2	tento
všeobecně	všeobecně	k6eAd1	všeobecně
rozšířeného	rozšířený	k2eAgNnSc2d1	rozšířené
rčení	rčení	k1gNnSc2	rčení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
některých	některý	k3yIgMnPc2	některý
odborníků	odborník	k1gMnPc2	odborník
ale	ale	k8xC	ale
z	z	k7c2	z
krátkodobého	krátkodobý	k2eAgNnSc2d1	krátkodobé
hlediska	hledisko	k1gNnSc2	hledisko
reálný	reálný	k2eAgInSc1d1	reálný
výběr	výběr	k1gInSc1	výběr
partnerů	partner	k1gMnPc2	partner
většinou	většinou	k6eAd1	většinou
neodpovídá	odpovídat	k5eNaImIp3nS	odpovídat
ani	ani	k8xC	ani
tomuto	tento	k3xDgNnSc3	tento
rčení	rčení	k1gNnSc1	rčení
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
uváděným	uváděný	k2eAgFnPc3d1	uváděná
vlastnostem	vlastnost	k1gFnPc3	vlastnost
ideálního	ideální	k2eAgMnSc4d1	ideální
partnera	partner	k1gMnSc4	partner
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Haralda	Harald	k1gMnSc2	Harald
Brauna	Braun	k1gMnSc2	Braun
<g/>
,	,	kIx,	,
německého	německý	k2eAgMnSc2d1	německý
spisovatele	spisovatel	k1gMnSc2	spisovatel
zabývajícího	zabývající	k2eAgMnSc2d1	zabývající
se	se	k3xPyFc4	se
láskou	láska	k1gFnSc7	láska
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
pro	pro	k7c4	pro
lidi	člověk	k1gMnPc4	člověk
zoufale	zoufale	k6eAd1	zoufale
hledající	hledající	k2eAgFnSc4d1	hledající
lásku	láska	k1gFnSc4	láska
jediné	jediný	k2eAgNnSc1d1	jediné
kritérium	kritérium	k1gNnSc1	kritérium
zájem	zájem	k1gInSc1	zájem
druhého	druhý	k4xOgMnSc2	druhý
partnera	partner	k1gMnSc2	partner
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
u	u	k7c2	u
ostatních	ostatní	k2eAgMnPc2d1	ostatní
hraje	hrát	k5eAaImIp3nS	hrát
velkou	velký	k2eAgFnSc4d1	velká
roli	role	k1gFnSc4	role
první	první	k4xOgInSc4	první
dojem	dojem	k1gInSc4	dojem
a	a	k8xC	a
schopnost	schopnost	k1gFnSc1	schopnost
partnera	partner	k1gMnSc2	partner
uspokojovat	uspokojovat	k5eAaImF	uspokojovat
emocionální	emocionální	k2eAgFnSc4d1	emocionální
potřeby	potřeba	k1gFnPc4	potřeba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Většina	většina	k1gFnSc1	většina
autorů	autor	k1gMnPc2	autor
se	se	k3xPyFc4	se
ale	ale	k9	ale
shoduje	shodovat	k5eAaImIp3nS	shodovat
v	v	k7c6	v
názoru	názor	k1gInSc6	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
mezi	mezi	k7c7	mezi
partnery	partner	k1gMnPc7	partner
není	být	k5eNaImIp3nS	být
alespoň	alespoň	k9	alespoň
částečná	částečný	k2eAgFnSc1d1	částečná
shoda	shoda	k1gFnSc1	shoda
v	v	k7c6	v
charakteru	charakter	k1gInSc6	charakter
a	a	k8xC	a
zájmech	zájem	k1gInPc6	zájem
<g/>
,	,	kIx,	,
tyto	tento	k3xDgInPc1	tento
vztahy	vztah	k1gInPc1	vztah
se	se	k3xPyFc4	se
po	po	k7c6	po
určité	určitý	k2eAgFnSc6d1	určitá
době	doba	k1gFnSc6	doba
rozpadají	rozpadat	k5eAaImIp3nP	rozpadat
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
dlouhodobých	dlouhodobý	k2eAgInPc2d1	dlouhodobý
vztahů	vztah	k1gInPc2	vztah
podle	podle	k7c2	podle
nich	on	k3xPp3gMnPc2	on
tedy	tedy	k9	tedy
bývá	bývat	k5eAaImIp3nS	bývat
mezi	mezi	k7c4	mezi
partnery	partner	k1gMnPc4	partner
s	s	k7c7	s
podobnými	podobný	k2eAgInPc7d1	podobný
zájmy	zájem	k1gInPc7	zájem
a	a	k8xC	a
povahovými	povahový	k2eAgInPc7d1	povahový
rysy	rys	k1gInPc7	rys
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
genové	genový	k2eAgInPc4d1	genový
předpoklady	předpoklad	k1gInPc4	předpoklad
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
lidé	člověk	k1gMnPc1	člověk
prokazatelně	prokazatelně	k6eAd1	prokazatelně
raději	rád	k6eAd2	rád
vybírají	vybírat	k5eAaImIp3nP	vybírat
partnery	partner	k1gMnPc7	partner
s	s	k7c7	s
odlišnými	odlišný	k2eAgFnPc7d1	odlišná
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
poskytli	poskytnout	k5eAaPmAgMnP	poskytnout
případným	případný	k2eAgMnPc3d1	případný
potomkům	potomek	k1gMnPc3	potomek
co	co	k3yInSc1	co
největší	veliký	k2eAgFnSc4d3	veliký
genovou	genový	k2eAgFnSc4d1	genová
rozmanitost	rozmanitost	k1gFnSc4	rozmanitost
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
imunitního	imunitní	k2eAgInSc2d1	imunitní
systému	systém	k1gInSc2	systém
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
rozdíly	rozdíl	k1gInPc1	rozdíl
lidé	člověk	k1gMnPc1	člověk
vnímají	vnímat	k5eAaImIp3nP	vnímat
podvědomě	podvědomě	k6eAd1	podvědomě
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
pomocí	pomocí	k7c2	pomocí
feromonů	feromon	k1gInPc2	feromon
vylučovaných	vylučovaný	k2eAgInPc2d1	vylučovaný
partnerem	partner	k1gMnSc7	partner
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
"	"	kIx"	"
<g/>
Láska	láska	k1gFnSc1	láska
prochází	procházet	k5eAaImIp3nS	procházet
žaludkem	žaludek	k1gInSc7	žaludek
<g/>
"	"	kIx"	"
===	===	k?	===
</s>
</p>
<p>
<s>
Výklad	výklad	k1gInSc1	výklad
tohoto	tento	k3xDgNnSc2	tento
rčení	rčení	k1gNnSc2	rčení
není	být	k5eNaImIp3nS	být
zcela	zcela	k6eAd1	zcela
jednoznačný	jednoznačný	k2eAgInSc1d1	jednoznačný
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
popisovat	popisovat	k5eAaImF	popisovat
preferenci	preference	k1gFnSc4	preference
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
umí	umět	k5eAaImIp3nP	umět
dobře	dobře	k6eAd1	dobře
uvařit	uvařit	k5eAaPmF	uvařit
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
spojovat	spojovat	k5eAaImF	spojovat
příjemné	příjemný	k2eAgInPc4d1	příjemný
pocity	pocit	k1gInPc4	pocit
plného	plný	k2eAgInSc2d1	plný
žaludku	žaludek	k1gInSc2	žaludek
s	s	k7c7	s
příjemnými	příjemný	k2eAgInPc7d1	příjemný
pocity	pocit	k1gInPc7	pocit
lásky	láska	k1gFnSc2	láska
<g/>
.	.	kIx.	.
</s>
<s>
Třetím	třetí	k4xOgInSc7	třetí
možným	možný	k2eAgInSc7d1	možný
výkladem	výklad	k1gInSc7	výklad
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
svírání	svírání	k1gNnSc1	svírání
(	(	kIx(	(
<g/>
popř.	popř.	kA	popř.
mravenčení	mravenčení	k1gNnSc4	mravenčení
<g/>
)	)	kIx)	)
žaludku	žaludek	k1gInSc2	žaludek
na	na	k7c6	na
začátcích	začátek	k1gInPc6	začátek
vztahů	vztah	k1gInPc2	vztah
při	při	k7c6	při
spatření	spatření	k1gNnSc6	spatření
milované	milovaný	k2eAgFnSc2d1	milovaná
osoby	osoba	k1gFnSc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
je	být	k5eAaImIp3nS	být
možným	možný	k2eAgMnSc7d1	možný
původcem	původce	k1gMnSc7	původce
tohoto	tento	k3xDgNnSc2	tento
rčení	rčení	k1gNnSc2	rčení
spojitost	spojitost	k1gFnSc4	spojitost
romantických	romantický	k2eAgFnPc2d1	romantická
večeří	večeře	k1gFnPc2	večeře
s	s	k7c7	s
následujícím	následující	k2eAgInSc7d1	následující
pohlavním	pohlavní	k2eAgInSc7d1	pohlavní
stykem	styk	k1gInSc7	styk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejčastějším	častý	k2eAgInSc7d3	nejčastější
výkladem	výklad	k1gInSc7	výklad
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
posílení	posílení	k1gNnSc1	posílení
pocitů	pocit	k1gInPc2	pocit
lásky	láska	k1gFnSc2	láska
k	k	k7c3	k
partnerovi	partner	k1gMnSc3	partner
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
nám	my	k3xPp1nPc3	my
uvařil	uvařit	k5eAaPmAgMnS	uvařit
<g/>
,	,	kIx,	,
po	po	k7c6	po
výborném	výborný	k2eAgNnSc6d1	výborné
jídle	jídlo	k1gNnSc6	jídlo
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
zvýšenou	zvýšený	k2eAgFnSc7d1	zvýšená
hladinou	hladina	k1gFnSc7	hladina
hormonů	hormon	k1gInPc2	hormon
způsobujících	způsobující	k2eAgFnPc2d1	způsobující
dobrou	dobrý	k2eAgFnSc4d1	dobrá
náladu	nálada	k1gFnSc4	nálada
<g/>
,	,	kIx,	,
hlavně	hlavně	k6eAd1	hlavně
serotoninu	serotonin	k1gInSc2	serotonin
a	a	k8xC	a
dopaminu	dopamin	k1gInSc2	dopamin
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
uvolňují	uvolňovat	k5eAaImIp3nP	uvolňovat
jak	jak	k6eAd1	jak
po	po	k7c6	po
vydatném	vydatný	k2eAgNnSc6d1	vydatné
jídle	jídlo	k1gNnSc6	jídlo
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
v	v	k7c6	v
prvních	první	k4xOgInPc6	první
fázích	fág	k1gInPc6	fág
romantických	romantický	k2eAgInPc2d1	romantický
vztahů	vztah	k1gInPc2	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Spojením	spojení	k1gNnSc7	spojení
těchto	tento	k3xDgInPc2	tento
pocitů	pocit	k1gInPc2	pocit
s	s	k7c7	s
osobou	osoba	k1gFnSc7	osoba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
jídlo	jídlo	k1gNnSc4	jídlo
uvařila	uvařit	k5eAaPmAgFnS	uvařit
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
může	moct	k5eAaImIp3nS	moct
docílit	docílit	k5eAaPmF	docílit
stavu	stav	k1gInSc3	stav
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
blíží	blížit	k5eAaImIp3nS	blížit
čerstvé	čerstvý	k2eAgFnSc3d1	čerstvá
zamilovanosti	zamilovanost	k1gFnSc3	zamilovanost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Preference	preference	k1gFnSc1	preference
některých	některý	k3yIgInPc2	některý
ženských	ženský	k2eAgInPc2d1	ženský
znaků	znak	k1gInPc2	znak
===	===	k?	===
</s>
</p>
<p>
<s>
Dalším	další	k2eAgMnSc7d1	další
velmi	velmi	k6eAd1	velmi
diskutovaným	diskutovaný	k2eAgNnSc7d1	diskutované
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
teze	teze	k1gFnSc1	teze
<g/>
,	,	kIx,	,
že	že	k8xS	že
muže	muž	k1gMnPc4	muž
více	hodně	k6eAd2	hodně
přitahují	přitahovat	k5eAaImIp3nP	přitahovat
ženy	žena	k1gFnPc1	žena
s	s	k7c7	s
blonďatou	blonďatý	k2eAgFnSc7d1	blonďatá
barvou	barva	k1gFnSc7	barva
vlasů	vlas	k1gInPc2	vlas
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
odborníci	odborník	k1gMnPc1	odborník
potvrzují	potvrzovat	k5eAaImIp3nP	potvrzovat
pravdivost	pravdivost	k1gFnSc4	pravdivost
tohoto	tento	k3xDgInSc2	tento
názoru	názor	k1gInSc2	názor
pro	pro	k7c4	pro
krátkodobé	krátkodobý	k2eAgInPc4d1	krátkodobý
vztahy	vztah	k1gInPc4	vztah
(	(	kIx(	(
<g/>
obzvláště	obzvláště	k6eAd1	obzvláště
pro	pro	k7c4	pro
tzv.	tzv.	kA	tzv.
vztahy	vztah	k1gInPc1	vztah
na	na	k7c4	na
jednu	jeden	k4xCgFnSc4	jeden
noc	noc	k1gFnSc4	noc
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
preferencích	preference	k1gFnPc6	preference
barvy	barva	k1gFnSc2	barva
vlasů	vlas	k1gInPc2	vlas
pro	pro	k7c4	pro
vážné	vážný	k2eAgInPc4d1	vážný
dlouhodobé	dlouhodobý	k2eAgInPc4d1	dlouhodobý
vztahy	vztah	k1gInPc4	vztah
se	se	k3xPyFc4	se
ale	ale	k9	ale
názory	názor	k1gInPc1	názor
odborníků	odborník	k1gMnPc2	odborník
i	i	k8xC	i
výsledky	výsledek	k1gInPc1	výsledek
různých	různý	k2eAgInPc2d1	různý
výzkumů	výzkum	k1gInPc2	výzkum
rozcházejí	rozcházet	k5eAaImIp3nP	rozcházet
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
amerického	americký	k2eAgInSc2d1	americký
časopisu	časopis	k1gInSc2	časopis
Psychology	psycholog	k1gMnPc4	psycholog
Today	Todaa	k1gFnPc4	Todaa
si	se	k3xPyFc3	se
muži	muž	k1gMnPc1	muž
raději	rád	k6eAd2	rád
vybírají	vybírat	k5eAaImIp3nP	vybírat
blonďaté	blonďatý	k2eAgFnPc4d1	blonďatá
partnerky	partnerka	k1gFnPc4	partnerka
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
z	z	k7c2	z
evolučního	evoluční	k2eAgNnSc2d1	evoluční
hlediska	hledisko	k1gNnSc2	hledisko
jsou	být	k5eAaImIp3nP	být
blonďaté	blonďatý	k2eAgFnPc1d1	blonďatá
ženy	žena	k1gFnPc1	žena
zdravější	zdravý	k2eAgFnPc1d2	zdravější
než	než	k8xS	než
ostatní	ostatní	k2eAgFnPc1d1	ostatní
(	(	kIx(	(
<g/>
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
je	on	k3xPp3gFnPc4	on
větší	veliký	k2eAgFnPc4d2	veliký
šance	šance	k1gFnPc4	šance
přežití	přežití	k1gNnSc2	přežití
potomka	potomek	k1gMnSc2	potomek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
přirozeným	přirozený	k2eAgInSc7d1	přirozený
leskem	lesk	k1gInSc7	lesk
vlasů	vlas	k1gInPc2	vlas
u	u	k7c2	u
zdravých	zdravý	k2eAgMnPc2d1	zdravý
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
při	při	k7c6	při
nemocích	nemoc	k1gFnPc6	nemoc
vytrácí	vytrácet	k5eAaImIp3nS	vytrácet
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
důvodem	důvod	k1gInSc7	důvod
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
hnědnutí	hnědnutí	k1gNnSc1	hnědnutí
vlasů	vlas	k1gInPc2	vlas
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
stárnutí-	stárnutí-	k?	stárnutí-
blonďaté	blonďatý	k2eAgInPc1d1	blonďatý
vlasy	vlas	k1gInPc1	vlas
jsou	být	k5eAaImIp3nP	být
tedy	tedy	k9	tedy
znakem	znak	k1gInSc7	znak
mládí	mládí	k1gNnSc2	mládí
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
předpokladu	předpoklad	k1gInSc2	předpoklad
tedy	tedy	k9	tedy
mohou	moct	k5eAaImIp3nP	moct
vycházet	vycházet	k5eAaImF	vycházet
různé	různý	k2eAgInPc4d1	různý
stereotypy	stereotyp	k1gInPc4	stereotyp
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
že	že	k8xS	že
blonďaté	blonďatý	k2eAgFnPc1d1	blonďatá
ženy	žena	k1gFnPc1	žena
jsou	být	k5eAaImIp3nP	být
hloupější	hloupý	k2eAgFnPc1d2	hloupější
nebo	nebo	k8xC	nebo
hravější	hravý	k2eAgFnPc1d2	hravější
než	než	k8xS	než
ženy	žena	k1gFnPc1	žena
tmavovlasé	tmavovlasý	k2eAgFnPc1d1	tmavovlasá
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
stereotypu	stereotyp	k1gInSc2	stereotyp
<g/>
,	,	kIx,	,
že	že	k8xS	že
děti	dítě	k1gFnPc1	dítě
a	a	k8xC	a
mladiství	mladistvý	k1gMnPc1	mladistvý
bývají	bývat	k5eAaImIp3nP	bývat
hraví	hravý	k2eAgMnPc1d1	hravý
a	a	k8xC	a
nerozumní	rozumný	k2eNgMnPc1d1	nerozumný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Studie	studie	k1gFnSc1	studie
Psychology	psycholog	k1gMnPc4	psycholog
Today	Toda	k2eAgMnPc4d1	Toda
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaImIp3nS	věnovat
i	i	k9	i
dalším	další	k2eAgInPc3d1	další
fyziologickým	fyziologický	k2eAgInPc3d1	fyziologický
znakům	znak	k1gInPc3	znak
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
muži	muž	k1gMnSc3	muž
u	u	k7c2	u
žen	žena	k1gFnPc2	žena
preferují	preferovat	k5eAaImIp3nP	preferovat
<g/>
.	.	kIx.	.
</s>
<s>
Preference	preference	k1gFnSc1	preference
dlouhých	dlouhý	k2eAgInPc2d1	dlouhý
vlasů	vlas	k1gInPc2	vlas
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
dána	dát	k5eAaPmNgFnS	dát
pomalým	pomalý	k2eAgInSc7d1	pomalý
růstem	růst	k1gInSc7	růst
vlasů	vlas	k1gInPc2	vlas
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc7	jejich
vypadáváním	vypadávání	k1gNnSc7	vypadávání
při	při	k7c6	při
vážných	vážný	k2eAgFnPc6d1	vážná
nemocích	nemoc	k1gFnPc6	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhé	Dlouhé	k2eAgInPc1d1	Dlouhé
vlasy	vlas	k1gInPc1	vlas
jsou	být	k5eAaImIp3nP	být
tedy	tedy	k9	tedy
známkou	známka	k1gFnSc7	známka
dlouhodobého	dlouhodobý	k2eAgNnSc2d1	dlouhodobé
zdraví	zdraví	k1gNnSc2	zdraví
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
do	do	k7c2	do
nedávné	dávný	k2eNgFnSc2d1	nedávná
doby	doba	k1gFnSc2	doba
byly	být	k5eAaImAgFnP	být
také	také	k9	také
pro	pro	k7c4	pro
muže	muž	k1gMnPc4	muž
jednoznačně	jednoznačně	k6eAd1	jednoznačně
přitažlivější	přitažlivý	k2eAgFnPc1d2	přitažlivější
baculaté	baculatý	k2eAgFnPc1d1	baculatá
ženy	žena	k1gFnPc1	žena
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
české	český	k2eAgNnSc4d1	české
přirovnání	přirovnání	k1gNnSc4	přirovnání
krev	krev	k1gFnSc1	krev
a	a	k8xC	a
mlíko	mlíko	k?	mlíko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
baculatost	baculatost	k1gFnSc1	baculatost
byla	být	k5eAaImAgFnS	být
také	také	k6eAd1	také
známkou	známka	k1gFnSc7	známka
celkového	celkový	k2eAgNnSc2d1	celkové
zdraví	zdraví	k1gNnSc2	zdraví
a	a	k8xC	a
hojnosti	hojnost	k1gFnSc2	hojnost
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc7d1	velká
záhadou	záhada	k1gFnSc7	záhada
byla	být	k5eAaImAgFnS	být
až	až	k9	až
donedávna	donedávna	k6eAd1	donedávna
preference	preference	k1gFnPc4	preference
velkých	velký	k2eAgNnPc2d1	velké
a	a	k8xC	a
pevných	pevný	k2eAgNnPc2d1	pevné
ňader	ňadro	k1gNnPc2	ňadro
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jejich	jejich	k3xOp3gFnSc1	jejich
velikost	velikost	k1gFnSc1	velikost
nijak	nijak	k6eAd1	nijak
nesouvisí	souviset	k5eNaImIp3nS	souviset
se	s	k7c7	s
schopností	schopnost	k1gFnSc7	schopnost
kojit	kojit	k5eAaImF	kojit
nebo	nebo	k8xC	nebo
vychovávat	vychovávat	k5eAaImF	vychovávat
potomky	potomek	k1gMnPc4	potomek
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
antropologa	antropolog	k1gMnSc2	antropolog
Franka	Frank	k1gMnSc2	Frank
Marlowa	Marlowus	k1gMnSc2	Marlowus
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
tato	tento	k3xDgFnSc1	tento
preference	preference	k1gFnSc1	preference
způsobena	způsobit	k5eAaPmNgFnS	způsobit
snadnějším	snadný	k2eAgNnSc7d2	snazší
zjištěním	zjištění	k1gNnSc7	zjištění
stáří	stáří	k1gNnSc2	stáří
ženy	žena	k1gFnSc2	žena
-	-	kIx~	-
větší	veliký	k2eAgNnPc1d2	veliký
ňadra	ňadro	k1gNnPc1	ňadro
se	se	k3xPyFc4	se
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
stárnutí	stárnutí	k1gNnSc2	stárnutí
znatelně	znatelně	k6eAd1	znatelně
prověšují	prověšovat	k5eAaImIp3nP	prověšovat
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
u	u	k7c2	u
malých	malý	k2eAgNnPc2d1	malé
ňader	ňadro	k1gNnPc2	ňadro
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc4	tento
rozdíl	rozdíl	k1gInSc4	rozdíl
menší	malý	k2eAgNnSc1d2	menší
<g/>
,	,	kIx,	,
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
hůře	zle	k6eAd2	zle
rozpoznatelný	rozpoznatelný	k2eAgInSc1d1	rozpoznatelný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Láska	láska	k1gFnSc1	láska
z	z	k7c2	z
biochemického	biochemický	k2eAgInSc2d1	biochemický
pohledu	pohled	k1gInSc2	pohled
==	==	k?	==
</s>
</p>
<p>
<s>
Biologické	biologický	k2eAgInPc1d1	biologický
modely	model	k1gInPc1	model
nazírají	nazírat	k5eAaImIp3nP	nazírat
na	na	k7c4	na
lásku	láska	k1gFnSc4	láska
jako	jako	k8xS	jako
na	na	k7c4	na
zvířecí	zvířecí	k2eAgInSc4d1	zvířecí
pud	pud	k1gInSc4	pud
<g/>
,	,	kIx,	,
sloužící	sloužící	k2eAgMnSc1d1	sloužící
k	k	k7c3	k
reprodukci	reprodukce	k1gFnSc3	reprodukce
a	a	k8xC	a
vychovávání	vychovávání	k1gNnSc4	vychovávání
potomků	potomek	k1gMnPc2	potomek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Helen	Helena	k1gFnPc2	Helena
Fisherová	Fisherová	k1gFnSc1	Fisherová
<g/>
,	,	kIx,	,
americká	americký	k2eAgFnSc1d1	americká
profesorka	profesorka	k1gFnSc1	profesorka
antropologie	antropologie	k1gFnSc2	antropologie
studující	studující	k1gFnSc2	studující
mezilidské	mezilidský	k2eAgInPc4d1	mezilidský
vztahy	vztah	k1gInPc4	vztah
<g/>
,	,	kIx,	,
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
lásku	láska	k1gFnSc4	láska
do	do	k7c2	do
tří	tři	k4xCgFnPc2	tři
fází	fáze	k1gFnPc2	fáze
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
částečně	částečně	k6eAd1	částečně
překrývají	překrývat	k5eAaImIp3nP	překrývat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Žádostivost	žádostivost	k1gFnSc1	žádostivost
[	[	kIx(	[
<g/>
ang	ang	k?	ang
<g/>
.	.	kIx.	.
lust	lust	k1gMnSc1	lust
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
nadrženost	nadrženost	k1gFnSc1	nadrženost
<g/>
,	,	kIx,	,
chtíč	chtíč	k1gInSc1	chtíč
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
základní	základní	k2eAgFnSc1d1	základní
sexuální	sexuální	k2eAgFnSc1d1	sexuální
potřeba	potřeba	k1gFnSc1	potřeba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
nutí	nutit	k5eAaImIp3nS	nutit
člověka	člověk	k1gMnSc4	člověk
vyhledávat	vyhledávat	k5eAaImF	vyhledávat
partnery	partner	k1gMnPc7	partner
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
fáze	fáze	k1gFnSc1	fáze
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
zvýšenou	zvýšený	k2eAgFnSc7d1	zvýšená
hladinou	hladina	k1gFnSc7	hladina
testosteronu	testosteron	k1gInSc2	testosteron
u	u	k7c2	u
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
estrogenů	estrogen	k1gInPc2	estrogen
u	u	k7c2	u
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Působí	působit	k5eAaImIp3nS	působit
od	od	k7c2	od
několika	několik	k4yIc2	několik
málo	málo	k6eAd1	málo
sekund	sekunda	k1gFnPc2	sekunda
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
navázaném	navázaný	k2eAgInSc6d1	navázaný
vztahu	vztah	k1gInSc6	vztah
málokdy	málokdy	k6eAd1	málokdy
trvá	trvat	k5eAaImIp3nS	trvat
déle	dlouho	k6eAd2	dlouho
než	než	k8xS	než
několik	několik	k4yIc4	několik
týdnů	týden	k1gInPc2	týden
nebo	nebo	k8xC	nebo
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přitažlivost	přitažlivost	k1gFnSc1	přitažlivost
[	[	kIx(	[
<g/>
ang	ang	k?	ang
<g/>
.	.	kIx.	.
attraction	attraction	k1gInSc1	attraction
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
zamilovanost	zamilovanost	k1gFnSc1	zamilovanost
<g/>
,	,	kIx,	,
touha	touha	k1gFnSc1	touha
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
intenzivnější	intenzivní	k2eAgFnSc1d2	intenzivnější
emoce	emoce	k1gFnSc1	emoce
zaměřená	zaměřený	k2eAgFnSc1d1	zaměřená
na	na	k7c4	na
určitého	určitý	k2eAgMnSc4d1	určitý
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
vyvine	vyvinout	k5eAaPmIp3nS	vyvinout
z	z	k7c2	z
žádostivosti	žádostivost	k1gFnSc2	žádostivost
jako	jako	k8xS	jako
prostředek	prostředek	k1gInSc4	prostředek
věrnosti	věrnost	k1gFnSc2	věrnost
ke	k	k7c3	k
konkrétnímu	konkrétní	k2eAgMnSc3d1	konkrétní
partnerovi	partner	k1gMnSc3	partner
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
fáze	fáze	k1gFnSc1	fáze
většinou	většinou	k6eAd1	většinou
trvá	trvat	k5eAaImIp3nS	trvat
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
až	až	k9	až
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Nedávné	dávný	k2eNgFnPc1d1	nedávná
neurologické	urologický	k2eNgFnPc1d1	urologický
studie	studie	k1gFnPc1	studie
prokázaly	prokázat	k5eAaPmAgFnP	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
zamilovanosti	zamilovanost	k1gFnSc2	zamilovanost
mozek	mozek	k1gInSc1	mozek
pravidelně	pravidelně	k6eAd1	pravidelně
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
specifické	specifický	k2eAgFnPc4d1	specifická
skupiny	skupina	k1gFnPc4	skupina
hormonů	hormon	k1gInPc2	hormon
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
největší	veliký	k2eAgFnSc4d3	veliký
roli	role	k1gFnSc4	role
hrají	hrát	k5eAaImIp3nP	hrát
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
feromony	feromon	k1gInPc1	feromon
<g/>
,	,	kIx,	,
chemické	chemický	k2eAgFnPc4d1	chemická
látky	látka	k1gFnPc4	látka
vylučované	vylučovaný	k2eAgFnPc4d1	vylučovaná
z	z	k7c2	z
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
sloužící	sloužící	k2eAgMnSc1d1	sloužící
k	k	k7c3	k
vyvolávání	vyvolávání	k1gNnSc3	vyvolávání
určitých	určitý	k2eAgFnPc2d1	určitá
reakcí	reakce	k1gFnPc2	reakce
u	u	k7c2	u
příjemců	příjemce	k1gMnPc2	příjemce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
dopamin	dopamin	k1gInSc1	dopamin
<g/>
,	,	kIx,	,
hormon	hormon	k1gInSc1	hormon
zodpovědný	zodpovědný	k2eAgInSc1d1	zodpovědný
za	za	k7c4	za
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
motivaci	motivace	k1gFnSc4	motivace
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
šťastné	šťastný	k2eAgInPc4d1	šťastný
pocity	pocit	k1gInPc4	pocit
při	při	k7c6	při
dosažení	dosažení	k1gNnSc6	dosažení
cíle	cíl	k1gInSc2	cíl
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
může	moct	k5eAaImIp3nS	moct
vyvolávat	vyvolávat	k5eAaImF	vyvolávat
určitou	určitý	k2eAgFnSc4d1	určitá
závislost	závislost	k1gFnSc4	závislost
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
hormon	hormon	k1gInSc1	hormon
působí	působit	k5eAaImIp3nS	působit
přímo	přímo	k6eAd1	přímo
na	na	k7c4	na
vegetativní	vegetativní	k2eAgFnSc4d1	vegetativní
nervovou	nervový	k2eAgFnSc4d1	nervová
soustavu	soustava	k1gFnSc4	soustava
<g/>
,	,	kIx,	,
při	při	k7c6	při
jeho	jeho	k3xOp3gFnSc6	jeho
zvýšené	zvýšený	k2eAgFnSc6d1	zvýšená
hladině	hladina	k1gFnSc6	hladina
jsou	být	k5eAaImIp3nP	být
tedy	tedy	k9	tedy
znatelné	znatelný	k2eAgInPc4d1	znatelný
fyziologické	fyziologický	k2eAgInPc4d1	fyziologický
příznaky	příznak	k1gInPc4	příznak
jako	jako	k8xS	jako
např.	např.	kA	např.
zrychlený	zrychlený	k2eAgInSc1d1	zrychlený
srdeční	srdeční	k2eAgInSc1d1	srdeční
tep	tep	k1gInSc1	tep
nebo	nebo	k8xC	nebo
zvýšení	zvýšení	k1gNnSc2	zvýšení
krevní	krevní	k2eAgInSc4d1	krevní
tlak	tlak	k1gInSc4	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
jeho	jeho	k3xOp3gFnSc2	jeho
přirozené	přirozený	k2eAgFnSc2d1	přirozená
výroby	výroba	k1gFnSc2	výroba
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
také	také	k9	také
ve	v	k7c6	v
velkém	velký	k2eAgNnSc6d1	velké
množství	množství	k1gNnSc6	množství
uvolňován	uvolňován	k2eAgInSc1d1	uvolňován
drogami	droga	k1gFnPc7	droga
(	(	kIx(	(
<g/>
hlavně	hlavně	k6eAd1	hlavně
amfetaminy	amfetamin	k1gInPc1	amfetamin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
čemuž	což	k3yRnSc3	což
se	se	k3xPyFc4	se
pocit	pocit	k1gInSc1	pocit
zamilovanosti	zamilovanost	k1gFnSc2	zamilovanost
často	často	k6eAd1	často
přirovnává	přirovnávat	k5eAaImIp3nS	přirovnávat
k	k	k7c3	k
drogovému	drogový	k2eAgNnSc3d1	drogové
opojení	opojení	k1gNnSc3	opojení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
noradrenalin	noradrenalin	k1gInSc1	noradrenalin
<g/>
,	,	kIx,	,
hormon	hormon	k1gInSc1	hormon
umožňující	umožňující	k2eAgInSc1d1	umožňující
tělu	tělo	k1gNnSc3	tělo
překonávat	překonávat	k5eAaImF	překonávat
krátkodobou	krátkodobý	k2eAgFnSc4d1	krátkodobá
zátěž	zátěž	k1gFnSc4	zátěž
a	a	k8xC	a
zvýšenou	zvýšený	k2eAgFnSc4d1	zvýšená
aktivitu	aktivita	k1gFnSc4	aktivita
</s>
</p>
<p>
<s>
serotonin	serotonin	k1gInSc4	serotonin
<g/>
,	,	kIx,	,
hormon	hormon	k1gInSc4	hormon
podílející	podílející	k2eAgInSc4d1	podílející
se	se	k3xPyFc4	se
na	na	k7c6	na
vzniku	vznik	k1gInSc6	vznik
nálad	nálada	k1gFnPc2	nálada
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
zvýšená	zvýšený	k2eAgFnSc1d1	zvýšená
hladina	hladina	k1gFnSc1	hladina
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
šťastné	šťastný	k2eAgInPc4d1	šťastný
pocity	pocit	k1gInPc4	pocit
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
při	při	k7c6	při
jeho	jeho	k3xOp3gInSc6	jeho
nedostatku	nedostatek	k1gInSc6	nedostatek
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
podrážděnost	podrážděnost	k1gFnSc1	podrážděnost
a	a	k8xC	a
deprese	deprese	k1gFnSc1	deprese
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
ale	ale	k9	ale
také	také	k9	také
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
příjem	příjem	k1gInSc1	příjem
potravy	potrava	k1gFnSc2	potrava
a	a	k8xC	a
cyklus	cyklus	k1gInSc1	cyklus
spánku	spánek	k1gInSc2	spánek
a	a	k8xC	a
bdění	bdění	k1gNnSc2	bdění
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
zvýšená	zvýšený	k2eAgFnSc1d1	zvýšená
či	či	k8xC	či
naopak	naopak	k6eAd1	naopak
snížená	snížený	k2eAgFnSc1d1	snížená
hladina	hladina	k1gFnSc1	hladina
proto	proto	k8xC	proto
může	moct	k5eAaImIp3nS	moct
způsobovat	způsobovat	k5eAaImF	způsobovat
poruchy	porucha	k1gFnPc4	porucha
spánku	spánek	k1gInSc2	spánek
a	a	k8xC	a
příjmu	příjem	k1gInSc2	příjem
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oddanost	oddanost	k1gFnSc1	oddanost
[	[	kIx(	[
<g/>
ang	ang	k?	ang
<g/>
.	.	kIx.	.
attachment	attachment	k1gMnSc1	attachment
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
věrnost	věrnost	k1gFnSc1	věrnost
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
předchozích	předchozí	k2eAgFnPc2d1	předchozí
dvou	dva	k4xCgFnPc2	dva
fází	fáze	k1gFnPc2	fáze
dlouhodobá	dlouhodobý	k2eAgFnSc1d1	dlouhodobá
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
evolučního	evoluční	k2eAgNnSc2d1	evoluční
hlediska	hledisko	k1gNnSc2	hledisko
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
fáze	fáze	k1gFnSc1	fáze
důležitá	důležitý	k2eAgFnSc1d1	důležitá
pro	pro	k7c4	pro
výchovu	výchova	k1gFnSc4	výchova
potomků	potomek	k1gMnPc2	potomek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
fázi	fáze	k1gFnSc6	fáze
je	být	k5eAaImIp3nS	být
obvyklá	obvyklý	k2eAgFnSc1d1	obvyklá
zvýšená	zvýšený	k2eAgFnSc1d1	zvýšená
hladina	hladina	k1gFnSc1	hladina
oxytocinu	oxytocin	k1gInSc2	oxytocin
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
hormonu	hormon	k1gInSc2	hormon
lásky	láska	k1gFnSc2	láska
<g/>
)	)	kIx)	)
a	a	k8xC	a
antidiuretického	antidiuretický	k2eAgInSc2d1	antidiuretický
hormonu	hormon	k1gInSc2	hormon
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
předchozích	předchozí	k2eAgFnPc6d1	předchozí
fázích	fáze	k1gFnPc6	fáze
příliš	příliš	k6eAd1	příliš
neprojevují	projevovat	k5eNaImIp3nP	projevovat
<g/>
.	.	kIx.	.
</s>
<s>
Italský	italský	k2eAgMnSc1d1	italský
badatel	badatel	k1gMnSc1	badatel
E.	E.	kA	E.
Emanuele	Emanuela	k1gFnSc3	Emanuela
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
spolupracovníky	spolupracovník	k1gMnPc7	spolupracovník
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgInS	zabývat
zvýšenou	zvýšený	k2eAgFnSc7d1	zvýšená
hladinou	hladina	k1gFnSc7	hladina
tzv.	tzv.	kA	tzv.
nervového	nervový	k2eAgInSc2d1	nervový
růstového	růstový	k2eAgInSc2d1	růstový
faktoru	faktor	k1gInSc2	faktor
(	(	kIx(	(
<g/>
NGF	NGF	kA	NGF
<g/>
)	)	kIx)	)
v	v	k7c6	v
prvních	první	k4xOgInPc6	první
několika	několik	k4yIc6	několik
měsících	měsíc	k1gInPc6	měsíc
vztahu	vztah	k1gInSc2	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Zjistili	zjistit	k5eAaPmAgMnP	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
zhruba	zhruba	k6eAd1	zhruba
po	po	k7c6	po
jednom	jeden	k4xCgInSc6	jeden
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
hladina	hladina	k1gFnSc1	hladina
tohoto	tento	k3xDgInSc2	tento
hormonu	hormon	k1gInSc2	hormon
vrací	vracet	k5eAaImIp3nS	vracet
do	do	k7c2	do
normálu	normál	k1gInSc2	normál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kulturní	kulturní	k2eAgInPc4d1	kulturní
a	a	k8xC	a
náboženské	náboženský	k2eAgInPc4d1	náboženský
rozdíly	rozdíl	k1gInPc4	rozdíl
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Starověké	starověký	k2eAgNnSc1d1	starověké
Řecko	Řecko	k1gNnSc1	Řecko
===	===	k?	===
</s>
</p>
<p>
<s>
Starověká	starověký	k2eAgFnSc1d1	starověká
řecká	řecký	k2eAgFnSc1d1	řecká
kultura	kultura	k1gFnSc1	kultura
v	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
i	i	k8xC	i
dílech	díl	k1gInPc6	díl
filozofů	filozof	k1gMnPc2	filozof
používala	používat	k5eAaImAgFnS	používat
toto	tento	k3xDgNnSc4	tento
obecné	obecný	k2eAgNnSc4d1	obecné
rozdělení	rozdělení	k1gNnSc4	rozdělení
lásky	láska	k1gFnSc2	láska
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Erós	erós	k1gInSc1	erós
(	(	kIx(	(
<g/>
žádostivost	žádostivost	k1gFnSc1	žádostivost
<g/>
)	)	kIx)	)
–	–	k?	–
láska	láska	k1gFnSc1	láska
vášnivá	vášnivý	k2eAgFnSc1d1	vášnivá
<g/>
,	,	kIx,	,
založená	založený	k2eAgFnSc1d1	založená
na	na	k7c6	na
tělesné	tělesný	k2eAgFnSc6d1	tělesná
a	a	k8xC	a
duševní	duševní	k2eAgFnSc6d1	duševní
přitažlivosti	přitažlivost	k1gFnSc6	přitažlivost
<g/>
,	,	kIx,	,
používáno	používán	k2eAgNnSc1d1	používáno
pro	pro	k7c4	pro
erotickou	erotický	k2eAgFnSc4d1	erotická
lásku	láska	k1gFnSc4	láska
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
pohlaví	pohlaví	k1gNnSc4	pohlaví
</s>
</p>
<p>
<s>
Storgé	Storgé	k1gNnSc1	Storgé
(	(	kIx(	(
<g/>
starostlivost	starostlivost	k1gFnSc1	starostlivost
<g/>
)	)	kIx)	)
–	–	k?	–
něžný	něžný	k2eAgInSc4d1	něžný
vztah	vztah	k1gInSc4	vztah
založený	založený	k2eAgInSc4d1	založený
na	na	k7c6	na
dlouhodobé	dlouhodobý	k2eAgFnSc6d1	dlouhodobá
oddanosti	oddanost	k1gFnSc6	oddanost
<g/>
,	,	kIx,	,
používáno	používán	k2eAgNnSc1d1	používáno
hlavně	hlavně	k9	hlavně
pro	pro	k7c4	pro
mateřskou	mateřský	k2eAgFnSc4d1	mateřská
lásku	láska	k1gFnSc4	láska
</s>
</p>
<p>
<s>
Mániá	Mániá	k1gFnSc1	Mániá
(	(	kIx(	(
<g/>
mánie	mánie	k1gFnSc1	mánie
<g/>
,	,	kIx,	,
posedlost	posedlost	k1gFnSc1	posedlost
<g/>
)	)	kIx)	)
–	–	k?	–
láska	láska	k1gFnSc1	láska
šílená	šílený	k2eAgFnSc1d1	šílená
<g/>
,	,	kIx,	,
žárlivá	žárlivý	k2eAgFnSc1d1	žárlivá
<g/>
,	,	kIx,	,
vlastnická	vlastnický	k2eAgFnSc1d1	vlastnická
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
závislostí	závislost	k1gFnSc7	závislost
na	na	k7c6	na
partnerovi	partner	k1gMnSc6	partner
</s>
</p>
<p>
<s>
Agapé	Agapý	k2eAgNnSc1d1	Agapé
(	(	kIx(	(
<g/>
laskavost	laskavost	k1gFnSc1	laskavost
<g/>
,	,	kIx,	,
oddanost	oddanost	k1gFnSc1	oddanost
<g/>
)	)	kIx)	)
–	–	k?	–
pečující	pečující	k2eAgInSc4d1	pečující
vztah	vztah	k1gInSc4	vztah
<g/>
,	,	kIx,	,
sladění	sladění	k1gNnSc4	sladění
vzájemných	vzájemný	k2eAgFnPc2d1	vzájemná
potřeb	potřeba	k1gFnPc2	potřeba
<g/>
,	,	kIx,	,
v	v	k7c6	v
novodobém	novodobý	k2eAgInSc6d1	novodobý
kontextu	kontext	k1gInSc6	kontext
"	"	kIx"	"
<g/>
pravá	pravý	k2eAgFnSc1d1	pravá
láska	láska	k1gFnSc1	láska
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Fíliá	Fíliá	k1gFnSc1	Fíliá
(	(	kIx(	(
<g/>
filie	filie	k1gFnSc1	filie
<g/>
,	,	kIx,	,
náklonnost	náklonnost	k1gFnSc1	náklonnost
<g/>
)	)	kIx)	)
–	–	k?	–
láska	láska	k1gFnSc1	láska
přátelskáPlatón	přátelskáPlatón	k1gInSc1	přátelskáPlatón
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
díle	dílo	k1gNnSc6	dílo
Symposion	symposion	k1gNnSc4	symposion
definoval	definovat	k5eAaBmAgMnS	definovat
lásku	láska	k1gFnSc4	láska
jako	jako	k8xC	jako
sílu	síla	k1gFnSc4	síla
duše	duše	k1gFnSc2	duše
<g/>
,	,	kIx,	,
snažící	snažící	k2eAgFnSc2d1	snažící
se	se	k3xPyFc4	se
o	o	k7c4	o
filozofické	filozofický	k2eAgNnSc4d1	filozofické
poznání	poznání	k1gNnSc4	poznání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dále	daleko	k6eAd2	daleko
také	také	k9	také
rozděloval	rozdělovat	k5eAaImAgInS	rozdělovat
mezilidskou	mezilidský	k2eAgFnSc4d1	mezilidská
lásku	láska	k1gFnSc4	láska
na	na	k7c4	na
lásku	láska	k1gFnSc4	láska
"	"	kIx"	"
<g/>
nebeskou	nebeský	k2eAgFnSc4d1	nebeská
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
kladl	klást	k5eAaImAgInS	klást
důraz	důraz	k1gInSc1	důraz
na	na	k7c4	na
krásu	krása	k1gFnSc4	krása
charakteru	charakter	k1gInSc2	charakter
a	a	k8xC	a
inteligenci	inteligence	k1gFnSc4	inteligence
partnera	partner	k1gMnSc2	partner
(	(	kIx(	(
<g/>
nikoliv	nikoliv	k9	nikoliv
na	na	k7c4	na
fyzickou	fyzický	k2eAgFnSc4d1	fyzická
krásu	krása	k1gFnSc4	krása
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c4	na
lásku	láska	k1gFnSc4	láska
"	"	kIx"	"
<g/>
nízkou	nízký	k2eAgFnSc4d1	nízká
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
šlo	jít	k5eAaImAgNnS	jít
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
tělesné	tělesný	k2eAgNnSc4d1	tělesné
uspokojení	uspokojení	k1gNnSc4	uspokojení
<g/>
.	.	kIx.	.
</s>
<s>
Nebeská	nebeský	k2eAgFnSc1d1	nebeská
láska	láska	k1gFnSc1	láska
přitom	přitom	k6eAd1	přitom
mohla	moct	k5eAaImAgFnS	moct
podle	podle	k7c2	podle
Platóna	Platón	k1gMnSc2	Platón
existovat	existovat	k5eAaImF	existovat
pouze	pouze	k6eAd1	pouze
mezi	mezi	k7c7	mezi
muži	muž	k1gMnPc7	muž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Starověký	starověký	k2eAgInSc1d1	starověký
Řím	Řím	k1gInSc1	Řím
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
mluvící	mluvící	k2eAgInSc1d1	mluvící
svět	svět	k1gInSc1	svět
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Latina	latina	k1gFnSc1	latina
má	mít	k5eAaImIp3nS	mít
pro	pro	k7c4	pro
lásku	láska	k1gFnSc4	láska
dvě	dva	k4xCgNnPc1	dva
slova	slovo	k1gNnPc1	slovo
<g/>
:	:	kIx,	:
amor	amor	k1gMnSc1	amor
(	(	kIx(	(
<g/>
láska	láska	k1gFnSc1	láska
milenecká	milenecký	k2eAgFnSc1d1	milenecká
<g/>
,	,	kIx,	,
toužící	toužící	k2eAgFnSc1d1	toužící
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
sexuální	sexuální	k2eAgFnSc2d1	sexuální
<g/>
)	)	kIx)	)
a	a	k8xC	a
caritas	caritas	k1gInSc1	caritas
(	(	kIx(	(
<g/>
láska	láska	k1gFnSc1	láska
mateřská	mateřský	k2eAgFnSc1d1	mateřská
<g/>
,	,	kIx,	,
pečující	pečující	k2eAgFnSc1d1	pečující
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgNnSc1d1	základní
sloveso	sloveso	k1gNnSc1	sloveso
pro	pro	k7c4	pro
lásku	láska	k1gFnSc4	láska
je	být	k5eAaImIp3nS	být
amā	amā	k?	amā
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
používáno	používat	k5eAaImNgNnS	používat
jak	jak	k6eAd1	jak
pro	pro	k7c4	pro
mezilidskou	mezilidský	k2eAgFnSc4d1	mezilidská
<g/>
,	,	kIx,	,
tak	tak	k9	tak
pro	pro	k7c4	pro
neosobní	osobní	k2eNgFnSc4d1	neosobní
lásku	láska	k1gFnSc4	láska
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
něj	on	k3xPp3gInSc2	on
odvozené	odvozený	k2eAgNnSc1d1	odvozené
podstatné	podstatný	k2eAgNnSc1d1	podstatné
jméno	jméno	k1gNnSc1	jméno
je	být	k5eAaImIp3nS	být
amor	amor	k1gMnSc1	amor
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
množném	množný	k2eAgNnSc6d1	množné
čísle	číslo	k1gNnSc6	číslo
používalo	používat	k5eAaImAgNnS	používat
pro	pro	k7c4	pro
označení	označení	k1gNnSc4	označení
sexuálních	sexuální	k2eAgInPc2d1	sexuální
styků	styk	k1gInPc2	styk
<g/>
.	.	kIx.	.
</s>
<s>
Odvozené	odvozený	k2eAgInPc1d1	odvozený
názvy	název	k1gInPc1	název
pro	pro	k7c4	pro
partnery	partner	k1gMnPc4	partner
jsou	být	k5eAaImIp3nP	být
amans	amans	k1gInSc4	amans
(	(	kIx(	(
<g/>
milenec	milenec	k1gMnSc1	milenec
<g/>
)	)	kIx)	)
a	a	k8xC	a
amica	amic	k2eAgFnSc1d1	amic
(	(	kIx(	(
<g/>
milenka	milenka	k1gFnSc1	milenka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
amica	amic	k1gInSc2	amic
se	se	k3xPyFc4	se
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
používalo	používat	k5eAaImAgNnS	používat
i	i	k9	i
pro	pro	k7c4	pro
označení	označení	k1gNnSc4	označení
prostitutky	prostitutka	k1gFnSc2	prostitutka
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
stejného	stejný	k2eAgInSc2d1	stejný
kořenu	kořen	k1gInSc2	kořen
se	se	k3xPyFc4	se
ale	ale	k9	ale
odvozují	odvozovat	k5eAaImIp3nP	odvozovat
i	i	k9	i
slova	slovo	k1gNnPc1	slovo
amicus	amicus	k1gMnSc1	amicus
(	(	kIx(	(
<g/>
přítel	přítel	k1gMnSc1	přítel
<g/>
)	)	kIx)	)
a	a	k8xC	a
amicitia	amicitia	k1gFnSc1	amicitia
(	(	kIx(	(
<g/>
přátelství	přátelství	k1gNnSc1	přátelství
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dobovém	dobový	k2eAgInSc6d1	dobový
kontextu	kontext	k1gInSc6	kontext
byla	být	k5eAaImAgFnS	být
ovšem	ovšem	k9	ovšem
slovem	slovem	k6eAd1	slovem
amicitia	amicitia	k1gFnSc1	amicitia
myšlena	myšlen	k2eAgFnSc1d1	myšlena
spíše	spíše	k9	spíše
zavázanost	zavázanost	k1gFnSc1	zavázanost
nebo	nebo	k8xC	nebo
ovlivněnost	ovlivněnost	k1gFnSc1	ovlivněnost
<g/>
.	.	kIx.	.
</s>
<s>
Marcus	Marcus	k1gMnSc1	Marcus
Tullius	Tullius	k1gMnSc1	Tullius
Cicero	Cicero	k1gMnSc1	Cicero
se	se	k3xPyFc4	se
tímto	tento	k3xDgNnSc7	tento
tématem	téma	k1gNnSc7	téma
zabývá	zabývat	k5eAaImIp3nS	zabývat
do	do	k7c2	do
hloubky	hloubka	k1gFnSc2	hloubka
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
traktátu	traktát	k1gInSc6	traktát
O	o	k7c6	o
Přátelství	přátelství	k1gNnSc6	přátelství
<g/>
.	.	kIx.	.
<g/>
Další	další	k2eAgMnSc1d1	další
římský	římský	k2eAgMnSc1d1	římský
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
Ovidius	Ovidius	k1gMnSc1	Ovidius
<g/>
,	,	kIx,	,
sepsal	sepsat	k5eAaPmAgMnS	sepsat
obsáhlé	obsáhlý	k2eAgNnSc4d1	obsáhlé
dílo	dílo	k1gNnSc4	dílo
Umění	umění	k1gNnSc2	umění
milovat	milovat	k5eAaImF	milovat
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgNnSc6	který
se	se	k3xPyFc4	se
podrobně	podrobně	k6eAd1	podrobně
zaobírá	zaobírat	k5eAaImIp3nS	zaobírat
všemi	všecek	k3xTgFnPc7	všecek
aspekty	aspekt	k1gInPc7	aspekt
lásky	láska	k1gFnSc2	láska
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
o	o	k7c6	o
lásce	láska	k1gFnSc6	láska
užitečné	užitečný	k2eAgFnSc2d1	užitečná
rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Západní	západní	k2eAgNnPc1d1	západní
náboženství	náboženství	k1gNnPc1	náboženství
===	===	k?	===
</s>
</p>
<p>
<s>
Křesťanství	křesťanství	k1gNnPc1	křesťanství
a	a	k8xC	a
židovství	židovství	k1gNnPc1	židovství
mají	mít	k5eAaImIp3nP	mít
díky	díky	k7c3	díky
podobným	podobný	k2eAgInPc3d1	podobný
základům	základ	k1gInPc3	základ
i	i	k9	i
podobné	podobný	k2eAgInPc4d1	podobný
náhledy	náhled	k1gInPc4	náhled
na	na	k7c4	na
lásku	láska	k1gFnSc4	láska
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgNnPc1	dva
náboženství	náboženství	k1gNnPc1	náboženství
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
mezilidskou	mezilidský	k2eAgFnSc4d1	mezilidská
lásku	láska	k1gFnSc4	láska
a	a	k8xC	a
lásku	láska	k1gFnSc4	láska
mezi	mezi	k7c7	mezi
Bohem	bůh	k1gMnSc7	bůh
a	a	k8xC	a
člověkem	člověk	k1gMnSc7	člověk
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
považují	považovat	k5eAaImIp3nP	považovat
oba	dva	k4xCgInPc1	dva
typy	typ	k1gInPc1	typ
za	za	k7c4	za
nejdůležitější	důležitý	k2eAgFnSc4d3	nejdůležitější
podstatu	podstata	k1gFnSc4	podstata
své	svůj	k3xOyFgFnSc2	svůj
náboženské	náboženský	k2eAgFnSc2d1	náboženská
víry	víra	k1gFnSc2	víra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obou	dva	k4xCgNnPc6	dva
náboženstvích	náboženství	k1gNnPc6	náboženství
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
přikázání	přikázání	k1gNnSc2	přikázání
ohledně	ohledně	k7c2	ohledně
obou	dva	k4xCgInPc2	dva
druhů	druh	k1gInPc2	druh
lásky	láska	k1gFnSc2	láska
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
"	"	kIx"	"
<g/>
miluj	milovat	k5eAaImRp2nS	milovat
bližního	bližní	k1gMnSc2	bližní
svého	svůj	k3xOyFgMnSc2	svůj
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Leviticus	Leviticus	k1gInSc1	Leviticus
19	[number]	k4	19
<g/>
:	:	kIx,	:
<g/>
18	[number]	k4	18
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
Budeš	být	k5eAaImBp2nS	být
milovat	milovat	k5eAaImF	milovat
Hospodina	Hospodin	k1gMnSc4	Hospodin
<g/>
,	,	kIx,	,
svého	svůj	k3xOyFgMnSc4	svůj
Boha	bůh	k1gMnSc4	bůh
<g/>
,	,	kIx,	,
celým	celý	k2eAgMnSc7d1	celý
svým	svůj	k1gMnSc7	svůj
srdcem	srdce	k1gNnSc7	srdce
a	a	k8xC	a
celou	celá	k1gFnSc4	celá
svou	svůj	k3xOyFgFnSc4	svůj
duší	dušit	k5eAaImIp3nP	dušit
a	a	k8xC	a
celou	celý	k2eAgFnSc4d1	celá
svou	svůj	k3xOyFgFnSc7	svůj
silou	síla	k1gFnSc7	síla
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Deuteronomium	Deuteronomium	k1gNnSc1	Deuteronomium
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
starém	starý	k2eAgInSc6d1	starý
zákoně	zákon	k1gInSc6	zákon
(	(	kIx(	(
<g/>
tóře	tóra	k1gFnSc6	tóra
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
více	hodně	k6eAd2	hodně
náhledů	náhled	k1gInPc2	náhled
na	na	k7c4	na
Boží	boží	k2eAgFnSc4d1	boží
lásku	láska	k1gFnSc4	láska
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
neshodují	shodovat	k5eNaImIp3nP	shodovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nejstarší	starý	k2eAgFnSc1d3	nejstarší
a	a	k8xC	a
nejznámější	známý	k2eAgFnSc1d3	nejznámější
"	"	kIx"	"
<g/>
definice	definice	k1gFnSc1	definice
<g/>
"	"	kIx"	"
lásky	láska	k1gFnSc2	láska
v	v	k7c6	v
křesťanství	křesťanství	k1gNnSc6	křesťanství
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
Novém	nový	k2eAgInSc6d1	nový
zákoně	zákon	k1gInSc6	zákon
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
epištol	epištola	k1gFnPc2	epištola
sv.	sv.	kA	sv.
Pavla	Pavel	k1gMnSc2	Pavel
Sv.	sv.	kA	sv.
Tomáš	Tomáš	k1gMnSc1	Tomáš
Akvinský	Akvinský	k2eAgMnSc1d1	Akvinský
<g/>
,	,	kIx,	,
popsal	popsat	k5eAaPmAgMnS	popsat
lásku	láska	k1gFnSc4	láska
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
vůli	vůle	k1gFnSc4	vůle
konat	konat	k5eAaImF	konat
dobro	dobro	k1gNnSc4	dobro
vůči	vůči	k7c3	vůči
ostatním	ostatní	k2eAgFnPc3d1	ostatní
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
touhu	touha	k1gFnSc4	touha
po	po	k7c6	po
úspěchu	úspěch	k1gInSc6	úspěch
druhých	druhý	k4xOgInPc2	druhý
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejčastěji	často	k6eAd3	často
citovaná	citovaný	k2eAgFnSc1d1	citovaná
židovská	židovský	k2eAgFnSc1d1	židovská
definice	definice	k1gFnSc1	definice
lásky	láska	k1gFnSc2	láska
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
novodobého	novodobý	k2eAgMnSc2d1	novodobý
rabína	rabín	k1gMnSc2	rabín
Eliyahu	Eliyah	k1gInSc2	Eliyah
Eliezer	Eliezer	k1gMnSc1	Eliezer
Desslera	Dessler	k1gMnSc2	Dessler
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
ji	on	k3xPp3gFnSc4	on
popisuje	popisovat	k5eAaImIp3nS	popisovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
dávání	dávání	k1gNnSc1	dávání
bez	bez	k7c2	bez
očekávání	očekávání	k1gNnSc2	očekávání
zisku	zisk	k1gInSc2	zisk
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Některé	některý	k3yIgInPc1	některý
křesťanské	křesťanský	k2eAgInPc1d1	křesťanský
zdroje	zdroj	k1gInPc1	zdroj
také	také	k9	také
přímo	přímo	k6eAd1	přímo
prohlašují	prohlašovat	k5eAaImIp3nP	prohlašovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
Bůh	bůh	k1gMnSc1	bůh
je	být	k5eAaImIp3nS	být
láska	láska	k1gFnSc1	láska
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
teze	teze	k1gFnSc1	teze
se	se	k3xPyFc4	se
v	v	k7c6	v
židovské	židovský	k2eAgFnSc6d1	židovská
víře	víra	k1gFnSc6	víra
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
svatých	svatý	k2eAgInPc6d1	svatý
textech	text	k1gInPc6	text
islámské	islámský	k2eAgFnSc2d1	islámská
víry	víra	k1gFnSc2	víra
se	se	k3xPyFc4	se
žádné	žádný	k3yNgInPc1	žádný
ukazatele	ukazatel	k1gInPc1	ukazatel
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
Bůh	bůh	k1gMnSc1	bůh
je	být	k5eAaImIp3nS	být
láska	láska	k1gFnSc1	láska
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nevyskytují	vyskytovat	k5eNaImIp3nP	vyskytovat
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
křesťanství	křesťanství	k1gNnSc4	křesťanství
a	a	k8xC	a
židovství	židovství	k1gNnSc4	židovství
<g/>
,	,	kIx,	,
i	i	k8xC	i
islám	islám	k1gInSc1	islám
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
mezilidskou	mezilidský	k2eAgFnSc4d1	mezilidská
lásku	láska	k1gFnSc4	láska
a	a	k8xC	a
lásku	láska	k1gFnSc4	láska
mezi	mezi	k7c7	mezi
Bohem	bůh	k1gMnSc7	bůh
a	a	k8xC	a
člověkem	člověk	k1gMnSc7	člověk
<g/>
,	,	kIx,	,
tu	tu	k6eAd1	tu
ale	ale	k9	ale
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
ještě	ještě	k9	ještě
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
další	další	k2eAgInPc4d1	další
druhy	druh	k1gInPc4	druh
<g/>
.	.	kIx.	.
</s>
<s>
Tzv.	tzv.	kA	tzv.
rafah	rafah	k1gInSc1	rafah
nebo	nebo	k8xC	nebo
rahmah	rahmah	k1gInSc1	rahmah
je	být	k5eAaImIp3nS	být
Boží	boží	k2eAgFnSc1d1	boží
láska	láska	k1gFnSc1	láska
zahrnující	zahrnující	k2eAgFnPc1d1	zahrnující
všechny	všechen	k3xTgFnPc1	všechen
živé	živý	k2eAgFnPc1d1	živá
bytosti	bytost	k1gFnPc1	bytost
bez	bez	k7c2	bez
rozdílu	rozdíl	k1gInSc2	rozdíl
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
hub	houba	k1gFnPc2	houba
nebo	nebo	k8xC	nebo
wudda	wuddo	k1gNnSc2	wuddo
jsou	být	k5eAaImIp3nP	být
názvy	název	k1gInPc4	název
pro	pro	k7c4	pro
lásku	láska	k1gFnSc4	láska
Boha	bůh	k1gMnSc2	bůh
pouze	pouze	k6eAd1	pouze
k	k	k7c3	k
věřícím	věřící	k1gMnPc3	věřící
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
dodržují	dodržovat	k5eAaImIp3nP	dodržovat
všechna	všechen	k3xTgNnPc4	všechen
přikázání	přikázání	k1gNnPc4	přikázání
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
koránu	korán	k1gInSc2	korán
tedy	tedy	k8xC	tedy
Alláh	Alláh	k1gMnSc1	Alláh
miluje	milovat	k5eAaImIp3nS	milovat
všechny	všechen	k3xTgMnPc4	všechen
lidi	člověk	k1gMnPc4	člověk
bez	bez	k7c2	bez
rozdílu	rozdíl	k1gInSc2	rozdíl
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
si	se	k3xPyFc3	se
ale	ale	k9	ale
lidé	člověk	k1gMnPc1	člověk
musí	muset	k5eAaImIp3nP	muset
vysloužit	vysloužit	k5eAaPmF	vysloužit
jeho	jeho	k3xOp3gFnSc1	jeho
náklonnost	náklonnost	k1gFnSc1	náklonnost
svou	svůj	k3xOyFgFnSc7	svůj
poslušností	poslušnost	k1gFnSc7	poslušnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Východní	východní	k2eAgNnPc1d1	východní
náboženství	náboženství	k1gNnPc1	náboženství
===	===	k?	===
</s>
</p>
<p>
<s>
Buddhismus	buddhismus	k1gInSc1	buddhismus
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
starověké	starověký	k2eAgNnSc4d1	starověké
Řecko	Řecko	k1gNnSc4	Řecko
<g/>
,	,	kIx,	,
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
několik	několik	k4yIc4	několik
druhů	druh	k1gInPc2	druh
lásky	láska	k1gFnSc2	láska
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Káma	Káma	k1gFnSc1	Káma
je	být	k5eAaImIp3nS	být
smyslná	smyslný	k2eAgFnSc1d1	smyslná
<g/>
,	,	kIx,	,
sexuální	sexuální	k2eAgFnSc1d1	sexuální
mezilidská	mezilidský	k2eAgFnSc1d1	mezilidská
láska	láska	k1gFnSc1	láska
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
překážkou	překážka	k1gFnSc7	překážka
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
k	k	k7c3	k
probuzení	probuzení	k1gNnSc3	probuzení
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
egoistická	egoistický	k2eAgFnSc1d1	egoistická
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Karuná	Karuná	k1gFnSc1	Karuná
je	být	k5eAaImIp3nS	být
soucit	soucit	k1gInSc4	soucit
se	s	k7c7	s
všemi	všecek	k3xTgFnPc7	všecek
bytostmi	bytost	k1gFnPc7	bytost
bez	bez	k7c2	bez
rozdílu	rozdíl	k1gInSc2	rozdíl
<g/>
;	;	kIx,	;
snižuje	snižovat	k5eAaImIp3nS	snižovat
utrpení	utrpení	k1gNnSc4	utrpení
druhých	druhý	k4xOgMnPc2	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Doplňuje	doplňovat	k5eAaImIp3nS	doplňovat
se	se	k3xPyFc4	se
s	s	k7c7	s
moudrostí	moudrost	k1gFnSc7	moudrost
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
nezbytná	nezbytný	k2eAgFnSc1d1	nezbytná
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
k	k	k7c3	k
osvícení	osvícení	k1gNnSc3	osvícení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mettá	Mettat	k5eAaPmIp3nS	Mettat
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
dobrotivá	dobrotivý	k2eAgFnSc1d1	dobrotivá
<g/>
"	"	kIx"	"
láska	láska	k1gFnSc1	láska
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
starořecká	starořecký	k2eAgFnSc1d1	starořecká
agapé	agapý	k2eAgFnSc2d1	agapý
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
osvobození	osvobození	k1gNnSc6	osvobození
od	od	k7c2	od
svých	svůj	k3xOyFgFnPc2	svůj
tužeb	tužba	k1gFnPc2	tužba
a	a	k8xC	a
nezištném	zištný	k2eNgNnSc6d1	nezištné
pomáhání	pomáhání	k1gNnSc6	pomáhání
ostatním	ostatní	k2eAgNnSc6d1	ostatní
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
velmi	velmi	k6eAd1	velmi
pokročilý	pokročilý	k2eAgInSc4d1	pokročilý
bod	bod	k1gInSc4	bod
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
za	za	k7c7	za
probuzením	probuzení	k1gNnSc7	probuzení
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
velké	velký	k2eAgNnSc4d1	velké
sebeuvědomění	sebeuvědomění	k1gNnSc4	sebeuvědomění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Cílem	cíl	k1gInSc7	cíl
buddhistů	buddhista	k1gMnPc2	buddhista
je	být	k5eAaImIp3nS	být
osvobodit	osvobodit	k5eAaPmF	osvobodit
se	se	k3xPyFc4	se
od	od	k7c2	od
utrpení	utrpení	k1gNnSc2	utrpení
(	(	kIx(	(
<g/>
tzn.	tzn.	kA	tzn.
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
probuzení	probuzení	k1gNnSc4	probuzení
vymaněním	vymanění	k1gNnPc3	vymanění
se	se	k3xPyFc4	se
za	za	k7c4	za
samsáry	samsár	k1gInPc4	samsár
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hinduismus	hinduismus	k1gInSc1	hinduismus
používá	používat	k5eAaImIp3nS	používat
stejné	stejný	k2eAgNnSc4d1	stejné
rozdělení	rozdělení	k1gNnSc4	rozdělení
jako	jako	k8xC	jako
buddhismus	buddhismus	k1gInSc4	buddhismus
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ještě	ještě	k9	ještě
navíc	navíc	k6eAd1	navíc
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
tzv.	tzv.	kA	tzv.
prema	prema	k1gNnSc1	prema
<g/>
,	,	kIx,	,
povznesenou	povznesený	k2eAgFnSc4d1	povznesená
láska	láska	k1gFnSc1	láska
a	a	k8xC	a
bhakti	bhakt	k1gMnPc1	bhakt
<g/>
,	,	kIx,	,
lásku	láska	k1gFnSc4	láska
k	k	k7c3	k
bohu	bůh	k1gMnSc3	bůh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Hnutí	hnutí	k1gNnSc1	hnutí
Hippies	Hippiesa	k1gFnPc2	Hippiesa
===	===	k?	===
</s>
</p>
<p>
<s>
Hnutí	hnutí	k1gNnSc1	hnutí
Hippies	Hippiesa	k1gFnPc2	Hippiesa
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
působilo	působit	k5eAaImAgNnS	působit
ponejvíce	ponejvíce	k6eAd1	ponejvíce
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
na	na	k7c6	na
území	území	k1gNnSc6	území
USA	USA	kA	USA
<g/>
,	,	kIx,	,
praktikovalo	praktikovat	k5eAaImAgNnS	praktikovat
ideologii	ideologie	k1gFnSc3	ideologie
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
volné	volný	k2eAgFnSc2d1	volná
lásky	láska	k1gFnSc2	láska
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
spočívala	spočívat	k5eAaImAgFnS	spočívat
ve	v	k7c6	v
vyjadřování	vyjadřování	k1gNnSc6	vyjadřování
lásky	láska	k1gFnSc2	láska
ke	k	k7c3	k
všem	všecek	k3xTgMnPc3	všecek
lidem	člověk	k1gMnPc3	člověk
bez	bez	k7c2	bez
rozdílu	rozdíl	k1gInSc2	rozdíl
a	a	k8xC	a
v	v	k7c6	v
názoru	názor	k1gInSc6	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
sex	sex	k1gInSc4	sex
se	s	k7c7	s
žádným	žádný	k3yNgInSc7	žádný
způsobem	způsob	k1gInSc7	způsob
neodlišuje	odlišovat	k5eNaImIp3nS	odlišovat
od	od	k7c2	od
ostatních	ostatní	k2eAgFnPc2d1	ostatní
věcí	věc	k1gFnPc2	věc
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
spolu	spolu	k6eAd1	spolu
lidé	člověk	k1gMnPc1	člověk
mohou	moct	k5eAaImIp3nP	moct
činit	činit	k5eAaImF	činit
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
kulturní	kulturní	k2eAgNnSc1d1	kulturní
hnutí	hnutí	k1gNnSc1	hnutí
tedy	tedy	k9	tedy
nemělo	mít	k5eNaImAgNnS	mít
prakticky	prakticky	k6eAd1	prakticky
žádná	žádný	k3yNgNnPc4	žádný
tabu	tabu	k1gNnPc4	tabu
a	a	k8xC	a
provozování	provozování	k1gNnSc4	provozování
nezávazného	závazný	k2eNgInSc2d1	nezávazný
sexu	sex	k1gInSc2	sex
bylo	být	k5eAaImAgNnS	být
zcela	zcela	k6eAd1	zcela
běžnou	běžný	k2eAgFnSc7d1	běžná
záležitostí	záležitost	k1gFnSc7	záležitost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
PECK	PECK	kA	PECK
<g/>
,	,	kIx,	,
Morgan	morgan	k1gMnSc1	morgan
Scott	Scott	k1gMnSc1	Scott
<g/>
:	:	kIx,	:
Nevyšlapanou	vyšlapaný	k2eNgFnSc7d1	nevyšlapaná
cestou	cesta	k1gFnSc7	cesta
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7203-036-1	[number]	k4	80-7203-036-1
</s>
</p>
<p>
<s>
FINKIELKRAUT	FINKIELKRAUT	kA	FINKIELKRAUT
<g/>
,	,	kIx,	,
A.	A.	kA	A.
<g/>
,	,	kIx,	,
Co	co	k9	co
kdyby	kdyby	kYmCp3nS	kdyby
láska	láska	k1gFnSc1	láska
nikdy	nikdy	k6eAd1	nikdy
neskončila	skončit	k5eNaPmAgFnS	skončit
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
CDK	CDK	kA	CDK
2014	[number]	k4	2014
</s>
</p>
<p>
<s>
OTIS-COUR	OTIS-COUR	k?	OTIS-COUR
<g/>
,	,	kIx,	,
Leah	Leah	k1gInSc1	Leah
<g/>
.	.	kIx.	.
</s>
<s>
Rozkoš	rozkoš	k1gFnSc1	rozkoš
a	a	k8xC	a
láska	láska	k1gFnSc1	láska
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
partnerských	partnerský	k2eAgInPc2d1	partnerský
vztahů	vztah	k1gInPc2	vztah
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
179	[number]	k4	179
s.	s.	k?	s.
(	(	kIx(	(
<g/>
Kulturní	kulturní	k2eAgFnSc1d1	kulturní
historie	historie	k1gFnSc1	historie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7021	[number]	k4	7021
<g/>
-	-	kIx~	-
<g/>
542	[number]	k4	542
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PIEPER	PIEPER	kA	PIEPER
<g/>
,	,	kIx,	,
J.	J.	kA	J.
<g/>
,	,	kIx,	,
O	o	k7c6	o
víře	víra	k1gFnSc6	víra
<g/>
,	,	kIx,	,
O	o	k7c4	o
naději	naděje	k1gFnSc4	naděje
<g/>
,	,	kIx,	,
O	o	k7c6	o
lásce	láska	k1gFnSc6	láska
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Krystal	krystal	k1gInSc1	krystal
OP	op	k1gMnSc1	op
2018	[number]	k4	2018
</s>
</p>
<p>
<s>
LEWIS	LEWIS	kA	LEWIS
<g/>
,	,	kIx,	,
C.	C.	kA	C.
S.	S.	kA	S.
<g/>
,	,	kIx,	,
Čtyři	čtyři	k4xCgFnPc1	čtyři
lásky	láska	k1gFnPc1	láska
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Návrat	návrat	k1gInSc1	návrat
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Emoce	emoce	k1gFnSc1	emoce
</s>
</p>
<p>
<s>
Manželství	manželství	k1gNnSc1	manželství
</s>
</p>
<p>
<s>
Sexuální	sexuální	k2eAgFnSc1d1	sexuální
orientace	orientace	k1gFnSc1	orientace
</s>
</p>
<p>
<s>
Slova	slovo	k1gNnPc1	slovo
obsahující	obsahující	k2eAgFnSc2d1	obsahující
kořen	kořen	k1gInSc4	kořen
-fil-	il-	k?	-fil-
</s>
</p>
<p>
<s>
Volná	volný	k2eAgFnSc1d1	volná
láska	láska	k1gFnSc1	láska
</s>
</p>
<p>
<s>
Nápoj	nápoj	k1gInSc1	nápoj
lásky	láska	k1gFnSc2	láska
</s>
</p>
<p>
<s>
Zámek	zámek	k1gInSc1	zámek
lásky	láska	k1gFnSc2	láska
</s>
</p>
<p>
<s>
Fyzická	fyzický	k2eAgFnSc1d1	fyzická
intimita	intimita	k1gFnSc1	intimita
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
láska	láska	k1gFnSc1	láska
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
láska	láska	k1gFnSc1	láska
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Téma	téma	k1gNnSc1	téma
Láska	láska	k1gFnSc1	láska
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Láska	láska	k1gFnSc1	láska
v	v	k7c6	v
Sociologické	sociologický	k2eAgFnSc6d1	sociologická
encyklopedii	encyklopedie	k1gFnSc6	encyklopedie
Sociologického	sociologický	k2eAgInSc2d1	sociologický
ústavu	ústav	k1gInSc2	ústav
Akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
</s>
</p>
<p>
<s>
FISHER	FISHER	kA	FISHER
<g/>
,	,	kIx,	,
H.	H.	kA	H.
E.	E.	kA	E.
Romantic	Romantice	k1gFnPc2	Romantice
love	lov	k1gInSc5	lov
<g/>
:	:	kIx,	:
a	a	k8xC	a
mammalian	mammalian	k1gInSc4	mammalian
brain	brain	k2eAgInSc4d1	brain
system	syst	k1gInSc7	syst
for	forum	k1gNnPc2	forum
mate	mást	k5eAaImIp3nS	mást
choice	choice	k1gFnSc1	choice
<g/>
.	.	kIx.	.
</s>
<s>
Philosophical	Philosophicat	k5eAaPmAgInS	Philosophicat
Transactions	Transactions	k1gInSc1	Transactions
of	of	k?	of
the	the	k?	the
Royal	Royal	k1gMnSc1	Royal
Society	societa	k1gFnSc2	societa
B	B	kA	B
<g/>
:	:	kIx,	:
Biological	Biological	k1gMnSc1	Biological
Sciences	Sciences	k1gMnSc1	Sciences
<g/>
.	.	kIx.	.
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
361	[number]	k4	361
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
1476	[number]	k4	1476
<g/>
,	,	kIx,	,
s.	s.	k?	s.
2173	[number]	k4	2173
<g/>
-	-	kIx~	-
<g/>
2186	[number]	k4	2186
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
