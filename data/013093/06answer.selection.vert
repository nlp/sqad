<s>
Láska	láska	k1gFnSc1	láska
označuje	označovat	k5eAaImIp3nS	označovat
silný	silný	k2eAgInSc4d1	silný
pozitivní	pozitivní	k2eAgInSc4d1	pozitivní
vztah	vztah	k1gInSc4	vztah
náklonnosti	náklonnost	k1gFnSc2	náklonnost
<g/>
,	,	kIx,	,
oddanosti	oddanost	k1gFnSc2	oddanost
nebo	nebo	k8xC	nebo
touhy	touha	k1gFnSc2	touha
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
lišit	lišit	k5eAaImF	lišit
jak	jak	k6eAd1	jak
svojí	svůj	k3xOyFgFnSc7	svůj
povahou	povaha	k1gFnSc7	povaha
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
také	také	k9	také
předmětem	předmět	k1gInSc7	předmět
<g/>
.	.	kIx.	.
</s>
