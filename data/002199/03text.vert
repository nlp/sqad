<p>
<s>
Facebook	Facebook	k1gInSc1	Facebook
(	(	kIx(	(
<g/>
výslovnost	výslovnost	k1gFnSc1	výslovnost
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
[	[	kIx(	[
<g/>
feɪ	feɪ	k?	feɪ
<g/>
]	]	kIx)	]
IPA	IPA	kA	IPA
<g/>
,	,	kIx,	,
fejsbuk	fejsbuk	k1gInSc1	fejsbuk
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rozsáhlý	rozsáhlý	k2eAgInSc1d1	rozsáhlý
společenský	společenský	k2eAgInSc1d1	společenský
webový	webový	k2eAgInSc1d1	webový
systém	systém	k1gInSc1	systém
sloužící	sloužící	k2eAgInSc1d1	sloužící
hlavně	hlavně	k9	hlavně
k	k	k7c3	k
tvorbě	tvorba	k1gFnSc3	tvorba
sociálních	sociální	k2eAgFnPc2d1	sociální
sítí	síť	k1gFnPc2	síť
<g/>
,	,	kIx,	,
komunikaci	komunikace	k1gFnSc4	komunikace
mezi	mezi	k7c7	mezi
uživateli	uživatel	k1gMnPc7	uživatel
<g/>
,	,	kIx,	,
sdílení	sdílení	k1gNnSc3	sdílení
multimediálních	multimediální	k2eAgFnPc2d1	multimediální
dat	datum	k1gNnPc2	datum
<g/>
,	,	kIx,	,
udržování	udržování	k1gNnSc1	udržování
vztahů	vztah	k1gInPc2	vztah
a	a	k8xC	a
zábavě	zábava	k1gFnSc3	zábava
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
2	[number]	k4	2
miliardami	miliarda	k4xCgFnPc7	miliarda
aktivních	aktivní	k2eAgMnPc2d1	aktivní
uživatelů	uživatel	k1gMnPc2	uživatel
(	(	kIx(	(
<g/>
červen	červen	k1gInSc1	červen
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
společenských	společenský	k2eAgFnPc2d1	společenská
sítí	síť	k1gFnPc2	síť
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
plně	plně	k6eAd1	plně
přeložen	přeložit	k5eAaPmNgInS	přeložit
do	do	k7c2	do
sedmdesáti	sedmdesát	k4xCc2	sedmdesát
tří	tři	k4xCgFnPc2	tři
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
serveru	server	k1gInSc2	server
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
z	z	k7c2	z
papírových	papírový	k2eAgInPc2d1	papírový
letáků	leták	k1gInPc2	leták
zvaných	zvaný	k2eAgFnPc2d1	zvaná
Facebooks	Facebooksa	k1gFnPc2	Facebooksa
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
rozdávají	rozdávat	k5eAaImIp3nP	rozdávat
prvákům	prvák	k1gMnPc3	prvák
na	na	k7c6	na
amerických	americký	k2eAgFnPc6d1	americká
univerzitách	univerzita	k1gFnPc6	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
letáky	leták	k1gInPc1	leták
slouží	sloužit	k5eAaImIp3nP	sloužit
k	k	k7c3	k
bližšímu	blízký	k2eAgNnSc3d2	bližší
seznámení	seznámení	k1gNnSc3	seznámení
studentů	student	k1gMnPc2	student
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
americký	americký	k2eAgInSc1d1	americký
film	film	k1gInSc1	film
The	The	k1gFnSc1	The
Social	Social	k1gMnSc1	Social
Network	network	k1gInSc1	network
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c6	o
počátcích	počátek	k1gInPc6	počátek
Facebooku	Facebook	k1gInSc2	Facebook
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
K	k	k7c3	k
založení	založení	k1gNnSc3	založení
Facebooku	Facebook	k1gInSc2	Facebook
vedla	vést	k5eAaImAgFnS	vést
dohoda	dohoda	k1gFnSc1	dohoda
mezi	mezi	k7c7	mezi
mladým	mladý	k2eAgMnSc7d1	mladý
studentem	student	k1gMnSc7	student
počítačového	počítačový	k2eAgNnSc2d1	počítačové
programování	programování	k1gNnSc2	programování
Markem	Marek	k1gMnSc7	Marek
Zuckerbergem	Zuckerberg	k1gMnSc7	Zuckerberg
<g/>
,	,	kIx,	,
bývalým	bývalý	k2eAgMnSc7d1	bývalý
studentem	student	k1gMnSc7	student
Harvardovy	Harvardův	k2eAgFnSc2d1	Harvardova
univerzity	univerzita	k1gFnSc2	univerzita
a	a	k8xC	a
Eduardem	Eduard	k1gMnSc7	Eduard
Saverinem	Saverin	k1gInSc7	Saverin
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc4	tento
systém	systém	k1gInSc4	systém
omezen	omezit	k5eAaPmNgMnS	omezit
jenom	jenom	k9	jenom
pro	pro	k7c4	pro
studenty	student	k1gMnPc4	student
Harvardovy	Harvardův	k2eAgFnSc2d1	Harvardova
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
doménou	doména	k1gFnSc7	doména
thefacebook.com	thefacebook.com	k1gInSc4	thefacebook.com
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
dvou	dva	k4xCgInPc2	dva
měsíců	měsíc	k1gInPc2	měsíc
byl	být	k5eAaImAgInS	být
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
na	na	k7c4	na
některé	některý	k3yIgMnPc4	některý
další	další	k2eAgNnSc1d1	další
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
patří	patřit	k5eAaImIp3nP	patřit
do	do	k7c2	do
tzv.	tzv.	kA	tzv.
Ivy	Iva	k1gFnSc2	Iva
League	Leagu	k1gFnSc2	Leagu
<g/>
,	,	kIx,	,
a	a	k8xC	a
již	již	k6eAd1	již
do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
byly	být	k5eAaImAgFnP	být
připojeny	připojit	k5eAaPmNgFnP	připojit
další	další	k2eAgFnPc1d1	další
univerzity	univerzita	k1gFnPc1	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
byl	být	k5eAaImAgInS	být
přístup	přístup	k1gInSc1	přístup
otevřen	otevřít	k5eAaPmNgInS	otevřít
pro	pro	k7c4	pro
všechny	všechen	k3xTgMnPc4	všechen
uživatele	uživatel	k1gMnPc4	uživatel
s	s	k7c7	s
univerzitní	univerzitní	k2eAgFnSc7d1	univerzitní
e-mailovou	eailův	k2eAgFnSc7d1	e-mailova
adresou	adresa	k1gFnSc7	adresa
(	(	kIx(	(
<g/>
.	.	kIx.	.
<g/>
edu	edu	k?	edu
<g/>
,	,	kIx,	,
ac	ac	k?	ac
<g/>
.	.	kIx.	.
<g/>
uk	uk	k?	uk
<g/>
,	,	kIx,	,
.	.	kIx.	.
<g/>
..	..	k?	..
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
pro	pro	k7c4	pro
některé	některý	k3yIgFnPc4	některý
zahraniční	zahraniční	k2eAgFnPc4d1	zahraniční
schválené	schválený	k2eAgFnPc4d1	schválená
univerzity	univerzita	k1gFnPc4	univerzita
<g/>
,	,	kIx,	,
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
k	k	k7c3	k
prvním	první	k4xOgFnPc3	první
otevřeným	otevřený	k2eAgFnPc3d1	otevřená
vysokým	vysoký	k2eAgFnPc3d1	vysoká
školám	škola	k1gFnPc3	škola
patřila	patřit	k5eAaImAgFnS	patřit
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
27.	[number]	k4	27.
února	únor	k1gInSc2	únor
2006	[number]	k4	2006
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
do	do	k7c2	do
systému	systém	k1gInSc2	systém
připojovat	připojovat	k5eAaImF	připojovat
některé	některý	k3yIgFnSc3	některý
nadnárodní	nadnárodní	k2eAgFnSc3d1	nadnárodní
obchodní	obchodní	k2eAgFnSc3d1	obchodní
společnosti	společnost	k1gFnSc3	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
26.	[number]	k4	26.
září	září	k1gNnSc2	září
2006	[number]	k4	2006
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
dle	dle	k7c2	dle
licence	licence	k1gFnSc2	licence
používání	používání	k1gNnSc2	používání
připojit	připojit	k5eAaPmF	připojit
kdokoli	kdokoli	k3yInSc1	kdokoli
starší	starý	k2eAgMnPc1d2	starší
13	[number]	k4	13
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
2	[number]	k4	2
<g/>
]	]	kIx)	]
Uživatelé	uživatel	k1gMnPc1	uživatel
se	se	k3xPyFc4	se
v	v	k7c6	v
systému	systém	k1gInSc6	systém
mohou	moct	k5eAaImIp3nP	moct
připojovat	připojovat	k5eAaImF	připojovat
k	k	k7c3	k
různým	různý	k2eAgFnPc3d1	různá
skupinám	skupina	k1gFnPc3	skupina
uživatelů	uživatel	k1gMnPc2	uživatel
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
působí	působit	k5eAaImIp3nP	působit
například	například	k6eAd1	například
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
jedné	jeden	k4xCgFnSc2	jeden
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
firmy	firma	k1gFnSc2	firma
nebo	nebo	k8xC	nebo
geografické	geografický	k2eAgFnPc4d1	geografická
lokace	lokace	k1gFnPc4	lokace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
založení	založení	k1gNnSc6	založení
Facebooku	Facebook	k1gInSc2	Facebook
se	se	k3xPyFc4	se
Mark	Mark	k1gMnSc1	Mark
Zuckerberg	Zuckerberg	k1gMnSc1	Zuckerberg
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
sporu	spor	k1gInSc2	spor
s	s	k7c7	s
dvojčaty	dvojče	k1gNnPc7	dvojče
Cameronem	Cameron	k1gMnSc7	Cameron
a	a	k8xC	a
Tylerem	Tyler	k1gMnSc7	Tyler
Winklevossovými	Winklevossův	k2eAgMnPc7d1	Winklevossův
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
Marka	Marek	k1gMnSc4	Marek
Zuckerberga	Zuckerberg	k1gMnSc4	Zuckerberg
obvinili	obvinit	k5eAaPmAgMnP	obvinit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jim	on	k3xPp3gMnPc3	on
nápad	nápad	k1gInSc1	nápad
na	na	k7c4	na
síť	síť	k1gFnSc4	síť
ukradl	ukradnout	k5eAaPmAgMnS	ukradnout
<g/>
.	.	kIx.	.
</s>
<s>
Spor	spor	k1gInSc1	spor
vyvrcholil	vyvrcholit	k5eAaPmAgInS	vyvrcholit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jim	on	k3xPp3gMnPc3	on
Mark	Mark	k1gMnSc1	Mark
Zuckerberg	Zuckerberg	k1gInSc4	Zuckerberg
vyplatil	vyplatit	k5eAaPmAgMnS	vyplatit
65	[number]	k4	65
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
prosince	prosinec	k1gInSc2	prosinec
2007	[number]	k4	2007
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
Facebook	Facebook	k1gInSc1	Facebook
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
57	[number]	k4	57
milióny	milión	k4xCgInPc7	milión
aktivními	aktivní	k2eAgInPc7d1	aktivní
členy	člen	k1gInPc7	člen
stránkou	stránka	k1gFnSc7	stránka
s	s	k7c7	s
největším	veliký	k2eAgInSc7d3	veliký
počtem	počet	k1gInSc7	počet
uživatelů	uživatel	k1gMnPc2	uživatel
mezi	mezi	k7c7	mezi
studentskými	studentský	k2eAgInPc7d1	studentský
weby	web	k1gInPc7	web
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
září	září	k1gNnSc2	září
2006	[number]	k4	2006
do	do	k7c2	do
září	září	k1gNnSc2	září
2007	[number]	k4	2007
se	se	k3xPyFc4	se
Facebook	Facebook	k1gInSc1	Facebook
dostal	dostat	k5eAaPmAgInS	dostat
z	z	k7c2	z
60.	[number]	k4	60.
na	na	k7c4	na
7.	[number]	k4	7.
pozici	pozice	k1gFnSc4	pozice
mezi	mezi	k7c7	mezi
nejnavštěvovanějšími	navštěvovaný	k2eAgFnPc7d3	nejnavštěvovanější
stránkami	stránka	k1gFnPc7	stránka
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Hodnota	hodnota	k1gFnSc1	hodnota
společnosti	společnost	k1gFnSc2	společnost
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
odhadována	odhadovat	k5eAaImNgFnS	odhadovat
na	na	k7c4	na
100	[number]	k4	100
miliónů	milión	k4xCgInPc2	milión
USD	USD	kA	USD
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
serveru	server	k1gInSc2	server
TechCrunch	TechCruncha	k1gFnPc2	TechCruncha
<g/>
,	,	kIx,	,
mělo	mít	k5eAaImAgNnS	mít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
na	na	k7c6	na
Facebooku	Facebook	k1gInSc6	Facebook
profil	profil	k1gInSc1	profil
85	[number]	k4	85
%	%	kIx~	%
studentů	student	k1gMnPc2	student
amerických	americký	k2eAgFnPc2d1	americká
univerzit	univerzita	k1gFnPc2	univerzita
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
se	s	k7c7	s
60	[number]	k4	60
%	%	kIx~	%
přihlašovalo	přihlašovat	k5eAaImAgNnS	přihlašovat
denně	denně	k6eAd1	denně
<g/>
,	,	kIx,	,
85	[number]	k4	85
%	%	kIx~	%
alespoň	alespoň	k9	alespoň
týdně	týdně	k6eAd1	týdně
a	a	k8xC	a
93	[number]	k4	93
%	%	kIx~	%
alespoň	alespoň	k9	alespoň
jednou	jeden	k4xCgFnSc7	jeden
měsíčně	měsíčně	k6eAd1	měsíčně
<g/>
.	.	kIx.	.
<g/>
Okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
se	se	k3xPyFc4	se
Facebook	Facebook	k1gInSc1	Facebook
dostal	dostat	k5eAaPmAgInS	dostat
nad	nad	k7c4	nad
hranici	hranice	k1gFnSc4	hranice
100	[number]	k4	100
milionů	milion	k4xCgInPc2	milion
uživatelů	uživatel	k1gMnPc2	uživatel
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
celou	celá	k1gFnSc4	celá
svou	svůj	k3xOyFgFnSc4	svůj
dosavadní	dosavadní	k2eAgFnSc4d1	dosavadní
dobu	doba	k1gFnSc4	doba
a	a	k8xC	a
ještě	ještě	k6eAd1	ještě
dalších	další	k2eAgInPc2d1	další
asi	asi	k9	asi
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
po	po	k7c6	po
té	ten	k3xDgFnSc6	ten
měl	mít	k5eAaImAgInS	mít
Facebook	Facebook	k1gInSc1	Facebook
problém	problém	k1gInSc1	problém
s	s	k7c7	s
pěnězi	pěněze	k1gFnSc4	pěněze
<g/>
.	.	kIx.	.
</s>
<s>
Facebook	Facebook	k1gInSc1	Facebook
tou	ten	k3xDgFnSc7	ten
dobou	doba	k1gFnSc7	doba
získával	získávat	k5eAaImAgMnS	získávat
finance	finance	k1gFnPc4	finance
především	především	k9	především
z	z	k7c2	z
fondů	fond	k1gInPc2	fond
rizikového	rizikový	k2eAgInSc2d1	rizikový
kapitálu	kapitál	k1gInSc2	kapitál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
předpovídalo	předpovídat	k5eAaImAgNnS	předpovídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
klasickou	klasický	k2eAgFnSc4d1	klasická
reklamu	reklama	k1gFnSc4	reklama
nahradí	nahradit	k5eAaPmIp3nP	nahradit
lajky	lajka	k1gFnPc1	lajka
a	a	k8xC	a
sdílení	sdílení	k1gNnPc1	sdílení
uživatelů	uživatel	k1gMnPc2	uživatel
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
si	se	k3xPyFc3	se
koupit	koupit	k5eAaPmF	koupit
<g/>
,	,	kIx,	,
a	a	k8xC	a
Facebook	Facebook	k1gInSc4	Facebook
potřeboval	potřebovat	k5eAaImAgMnS	potřebovat
třetí	třetí	k4xOgFnPc4	třetí
firmy	firma	k1gFnPc4	firma
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
by	by	kYmCp3nS	by
tento	tento	k3xDgInSc1	tento
nový	nový	k2eAgInSc1d1	nový
"	"	kIx"	"
<g/>
nereklamní	reklamní	k2eNgInSc1d1	reklamní
reklamní	reklamní	k2eAgInSc1d1	reklamní
<g/>
"	"	kIx"	"
model	model	k1gInSc4	model
prodávaly	prodávat	k5eAaImAgInP	prodávat
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
tyto	tento	k3xDgFnPc1	tento
třetí	třetí	k4xOgFnPc1	třetí
firmy	firma	k1gFnPc1	firma
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
Facebook	Facebook	k1gInSc1	Facebook
potřeboval	potřebovat	k5eAaImAgInS	potřebovat
<g/>
,	,	kIx,	,
vyrostly	vyrůst	k5eAaPmAgFnP	vyrůst
Socialbakers	Socialbakers	k1gInSc4	Socialbakers
a	a	k8xC	a
Cambridge	Cambridge	k1gFnSc1	Cambridge
Analytica	Analytic	k1gInSc2	Analytic
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mohly	moct	k5eAaImAgFnP	moct
jako	jako	k9	jako
základní	základní	k2eAgInSc4d1	základní
nástroj	nástroj	k1gInSc4	nástroj
využívat	využívat	k5eAaImF	využívat
Facebookvé	Facebookvá	k1gFnSc2	Facebookvá
Graph	Grapha	k1gFnPc2	Grapha
API	API	kA	API
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
ve	v	k7c6	v
verzi	verze	k1gFnSc6	verze
1.	[number]	k4	1.
<g/>
0	[number]	k4	0
umožňovalo	umožňovat	k5eAaImAgNnS	umožňovat
z	z	k7c2	z
profilů	profil	k1gInPc2	profil
uživatelů	uživatel	k1gMnPc2	uživatel
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc2	jejich
přátel	přítel	k1gMnPc2	přítel
stáhnout	stáhnout	k5eAaPmF	stáhnout
téměř	téměř	k6eAd1	téměř
jakékoliv	jakýkoliv	k3yIgFnPc4	jakýkoliv
jejich	jejich	k3xOp3gFnPc4	jejich
informace	informace	k1gFnPc4	informace
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
ale	ale	k8xC	ale
Facebook	Facebook	k1gInSc1	Facebook
skončil	skončit	k5eAaPmAgInS	skončit
u	u	k7c2	u
klasického	klasický	k2eAgInSc2d1	klasický
reklamního	reklamní	k2eAgInSc2d1	reklamní
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
byl	být	k5eAaImAgMnS	být
po	po	k7c6	po
nepokojích	nepokoj	k1gInPc6	nepokoj
v	v	k7c6	v
Urumči	Urumč	k1gFnSc6	Urumč
Facebook	Facebook	k1gInSc1	Facebook
zakázán	zakázán	k2eAgInSc1d1	zakázán
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
The	The	k1gFnSc2	The
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
Times	Times	k1gInSc1	Times
Facebook	Facebook	k1gInSc4	Facebook
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
software	software	k1gInSc1	software
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
blokovat	blokovat	k5eAaImF	blokovat
zobrazování	zobrazování	k1gNnSc4	zobrazování
vybraných	vybraný	k2eAgFnPc2d1	vybraná
informací	informace	k1gFnPc2	informace
v	v	k7c6	v
určitých	určitý	k2eAgFnPc6d1	určitá
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
pomoci	pomoct	k5eAaPmF	pomoct
firmě	firma	k1gFnSc3	firma
vrátit	vrátit	k5eAaPmF	vrátit
se	se	k3xPyFc4	se
na	na	k7c4	na
čínský	čínský	k2eAgInSc4d1	čínský
trh	trh	k1gInSc4	trh
<g/>
,	,	kIx,	,
Facebook	Facebook	k1gInSc4	Facebook
ale	ale	k8xC	ale
nezamýšlí	zamýšlet	k5eNaImIp3nS	zamýšlet
používat	používat	k5eAaImF	používat
software	software	k1gInSc1	software
sám	sám	k3xTgInSc1	sám
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
plánuje	plánovat	k5eAaImIp3nS	plánovat
ho	on	k3xPp3gMnSc4	on
nabízet	nabízet	k5eAaImF	nabízet
k	k	k7c3	k
využívání	využívání	k1gNnSc2	využívání
jiným	jiný	k2eAgInPc3d1	jiný
subjektům	subjekt	k1gInPc3	subjekt
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2012	[number]	k4	2012
byly	být	k5eAaImAgFnP	být
akcie	akcie	k1gFnPc1	akcie
společnosti	společnost	k1gFnSc3	společnost
uvedeny	uvést	k5eAaPmNgInP	uvést
na	na	k7c4	na
burzu	burza	k1gFnSc4	burza
NASDAQ	NASDAQ	kA	NASDAQ
<g/>
,	,	kIx,	,
emisní	emisní	k2eAgFnSc1d1	emisní
cena	cena	k1gFnSc1	cena
akcií	akcie	k1gFnPc2	akcie
38	[number]	k4	38
USD	USD	kA	USD
ocenila	ocenit	k5eAaPmAgFnS	ocenit
celou	celý	k2eAgFnSc4d1	celá
společnost	společnost	k1gFnSc4	společnost
na	na	k7c4	na
104	[number]	k4	104
miliard	miliarda	k4xCgFnPc2	miliarda
USD	USD	kA	USD
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
očekávání	očekávání	k1gNnSc3	očekávání
však	však	k9	však
první	první	k4xOgInSc1	první
den	den	k1gInSc1	den
obchodování	obchodování	k1gNnSc2	obchodování
cena	cena	k1gFnSc1	cena
akcií	akcie	k1gFnPc2	akcie
nijak	nijak	k6eAd1	nijak
výrazně	výrazně	k6eAd1	výrazně
nevzrostla	vzrůst	k5eNaPmAgFnS	vzrůst
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
koupil	koupit	k5eAaPmAgInS	koupit
Facebook	Facebook	k1gInSc4	Facebook
svého	svůj	k3xOyFgNnSc2	svůj
zřejmě	zřejmě	k6eAd1	zřejmě
největšího	veliký	k2eAgMnSc4d3	veliký
konkurenta	konkurent	k1gMnSc4	konkurent
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
posílání	posílání	k1gNnSc2	posílání
zpráv	zpráva	k1gFnPc2	zpráva
<g/>
,	,	kIx,	,
službu	služba	k1gFnSc4	služba
WhatsApp	WhatsApp	k1gMnSc1	WhatsApp
<g/>
.	.	kIx.	.
</s>
<s>
Facebook	Facebook	k1gInSc1	Facebook
měl	mít	k5eAaImAgInS	mít
zájem	zájem	k1gInSc4	zájem
i	i	k9	i
o	o	k7c4	o
službu	služba	k1gFnSc4	služba
Snapchat	Snapchat	k1gFnSc2	Snapchat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
koupit	koupit	k5eAaPmF	koupit
nenechala	nechat	k5eNaPmAgFnS	nechat
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
reklamních	reklamní	k2eAgFnPc2d1	reklamní
služeb	služba	k1gFnPc2	služba
Facebook	Facebook	k1gInSc1	Facebook
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
spustil	spustit	k5eAaPmAgInS	spustit
službu	služba	k1gFnSc4	služba
Atlas	Atlas	k1gInSc1	Atlas
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc1	jejíž
základ	základ	k1gInSc1	základ
koupil	koupit	k5eAaPmAgInS	koupit
od	od	k7c2	od
Microsoftu	Microsoft	k1gInSc2	Microsoft
a	a	k8xC	a
která	který	k3yQgFnSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
zobrazovat	zobrazovat	k5eAaImF	zobrazovat
cílenou	cílený	k2eAgFnSc4d1	cílená
reklamu	reklama	k1gFnSc4	reklama
i	i	k9	i
na	na	k7c6	na
mobilních	mobilní	k2eAgInPc6d1	mobilní
zařízeních	zařízení	k1gNnPc6	zařízení
a	a	k8xC	a
dokáže	dokázat	k5eAaPmIp3nS	dokázat
párovat	párovat	k5eAaImF	párovat
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
uživatelích	uživatel	k1gMnPc6	uživatel
i	i	k8xC	i
bez	bez	k7c2	bez
využití	využití	k1gNnSc2	využití
cookies	cookiesa	k1gFnPc2	cookiesa
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
malé	malý	k2eAgMnPc4d1	malý
obchodníky	obchodník	k1gMnPc4	obchodník
spustil	spustit	k5eAaPmAgMnS	spustit
službu	služba	k1gFnSc4	služba
Local	Local	k1gMnSc1	Local
Awareness	Awarenessa	k1gFnPc2	Awarenessa
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
nastavit	nastavit	k5eAaPmF	nastavit
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
od	od	k7c2	od
obchodu	obchod	k1gInSc2	obchod
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
se	se	k3xPyFc4	se
budou	být	k5eAaImBp3nP	být
zobrazovat	zobrazovat	k5eAaImF	zobrazovat
reklamy	reklama	k1gFnPc1	reklama
na	na	k7c4	na
daný	daný	k2eAgInSc4d1	daný
obchod	obchod	k1gInSc4	obchod
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
bude	být	k5eAaImBp3nS	být
uživatel	uživatel	k1gMnSc1	uživatel
mobilního	mobilní	k2eAgNnSc2d1	mobilní
zařízení	zařízení	k1gNnSc2	zařízení
nacházet	nacházet	k5eAaImF	nacházet
a	a	k8xC	a
kromě	kromě	k7c2	kromě
vlastní	vlastní	k2eAgFnSc2d1	vlastní
reklamy	reklama	k1gFnSc2	reklama
bude	být	k5eAaImBp3nS	být
možné	možný	k2eAgNnSc1d1	možné
nechat	nechat	k5eAaPmF	nechat
si	se	k3xPyFc3	se
i	i	k8xC	i
zobrazit	zobrazit	k5eAaPmF	zobrazit
si	se	k3xPyFc3	se
mapu	mapa	k1gFnSc4	mapa
cesty	cesta	k1gFnSc2	cesta
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
obchodu	obchod	k1gInSc3	obchod
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
nástroj	nástroj	k1gInSc1	nástroj
na	na	k7c4	na
rozpoznávání	rozpoznávání	k1gNnSc4	rozpoznávání
a	a	k8xC	a
automatické	automatický	k2eAgNnSc4d1	automatické
mazání	mazání	k1gNnSc4	mazání
nahlášených	nahlášený	k2eAgFnPc2d1	nahlášená
fotografií	fotografia	k1gFnPc2	fotografia
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zabránil	zabránit	k5eAaPmAgMnS	zabránit
dalšímu	další	k2eAgNnSc3d1	další
zveřejňování	zveřejňování	k1gNnSc3	zveřejňování
jednou	jednou	k6eAd1	jednou
smazaných	smazaný	k2eAgFnPc2d1	smazaná
fotografií	fotografia	k1gFnPc2	fotografia
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
nástroje	nástroj	k1gInSc2	nástroj
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
pomoci	pomoct	k5eAaPmF	pomoct
předejít	předejít	k5eAaPmF	předejít
šíření	šíření	k1gNnSc4	šíření
takových	takový	k3xDgFnPc2	takový
fotografií	fotografia	k1gFnPc2	fotografia
nejenon	nejenona	k1gFnPc2	nejenona
na	na	k7c6	na
Facebooku	Facebook	k1gInSc6	Facebook
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
na	na	k7c6	na
dalších	další	k2eAgFnPc6d1	další
platformách	platforma	k1gFnPc6	platforma
Facebooku	Facebook	k1gInSc2	Facebook
<g/>
,	,	kIx,	,
např.	např.	kA	např.
na	na	k7c6	na
Instagramu	Instagram	k1gInSc6	Instagram
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
Facebookem	Facebook	k1gInSc7	Facebook
koupen	koupit	k5eAaPmNgInS	koupit
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
software	software	k1gInSc1	software
byl	být	k5eAaImAgInS	být
vyvinut	vyvinout	k5eAaPmNgInS	vyvinout
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
na	na	k7c4	na
Facebook	Facebook	k1gInSc4	Facebook
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
tlak	tlak	k1gInSc1	tlak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
rychleji	rychle	k6eAd2	rychle
odstraňoval	odstraňovat	k5eAaImAgInS	odstraňovat
nevhodný	vhodný	k2eNgInSc1d1	nevhodný
a	a	k8xC	a
násilný	násilný	k2eAgInSc1d1	násilný
obsah	obsah	k1gInSc1	obsah
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
po	po	k7c6	po
té	ten	k3xDgFnSc6	ten
<g/>
,	,	kIx,	,
co	co	k9	co
byly	být	k5eAaImAgFnP	být
přes	přes	k7c4	přes
Facebook	Facebook	k1gInSc4	Facebook
přenášena	přenášen	k2eAgNnPc4d1	přenášeno
online	onlinout	k5eAaPmIp3nS	onlinout
videa	video	k1gNnPc4	video
znásilnění	znásilnění	k1gNnSc2	znásilnění
ženy	žena	k1gFnSc2	žena
třemi	tři	k4xCgInPc7	tři
muži	muž	k1gMnPc7	muž
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
<g/>
,	,	kIx,	,
znásilnění	znásilnění	k1gNnSc4	znásilnění
15leté	15letý	k2eAgFnSc2d1	15letý
dívky	dívka	k1gFnSc2	dívka
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
a	a	k8xC	a
vraždy	vražda	k1gFnSc2	vražda
<g />
.	.	kIx.	.
</s>
<s>
74letého	74letý	k2eAgMnSc4d1	74letý
muže	muž	k1gMnSc4	muž
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
se	se	k3xPyFc4	se
Facebook	Facebook	k1gInSc1	Facebook
dohodl	dohodnout	k5eAaPmAgInS	dohodnout
s	s	k7c7	s
americkým	americký	k2eAgInSc7d1	americký
kongresem	kongres	k1gInSc7	kongres
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
předá	předat	k5eAaPmIp3nS	předat
3000	[number]	k4	3000
inzerátů	inzerát	k1gInPc2	inzerát
Rusů	Rus	k1gMnPc2	Rus
objednaných	objednaný	k2eAgMnPc2d1	objednaný
kvůli	kvůli	k7c3	kvůli
ovlivnění	ovlivnění	k1gNnSc3	ovlivnění
prezitentských	prezitentský	k2eAgFnPc2d1	prezitentský
voleb	volba	k1gFnPc2	volba
v	v	k7c6	v
USA	USA	kA	USA
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016.	[number]	k4	2016.
<g/>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
srpna	srpen	k1gInSc2	srpen
2017	[number]	k4	2017
spustil	spustit	k5eAaPmAgInS	spustit
Facebook	Facebook	k1gInSc4	Facebook
prodejní	prodejní	k2eAgInSc1d1	prodejní
portál	portál	k1gInSc1	portál
Marketplace	Marketplace	k1gFnSc2	Marketplace
nově	nově	k6eAd1	nově
v	v	k7c6	v
17	[number]	k4	17
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
mj.	mj.	kA	mj.
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
,	,	kIx,	,
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
<g/>
,	,	kIx,	,
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
,	,	kIx,	,
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
aktualne.cz	aktualne.cza	k1gFnPc2	aktualne.cza
je	být	k5eAaImIp3nS	být
záměrem	záměr	k1gInSc7	záměr
firmy	firma	k1gFnSc2	firma
získat	získat	k5eAaPmF	získat
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
nákupním	nákupní	k2eAgNnSc6d1	nákupní
chování	chování	k1gNnSc6	chování
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
ty	ten	k3xDgInPc4	ten
pak	pak	k6eAd1	pak
zužitkovat	zužitkovat	k5eAaPmF	zužitkovat
v	v	k7c6	v
cílení	cílení	k1gNnSc6	cílení
reklamy	reklama	k1gFnSc2	reklama
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
měsíců	měsíc	k1gInPc2	měsíc
před	před	k7c7	před
spuštěním	spuštění	k1gNnSc7	spuštění
Marketplace	Marketplace	k1gFnSc2	Marketplace
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
zavedl	zavést	k5eAaPmAgInS	zavést
Facebook	Facebook	k1gInSc1	Facebook
možnost	možnost	k1gFnSc4	možnost
placení	placení	k1gNnSc2	placení
přes	přes	k7c4	přes
Messenger	Messenger	k1gInSc4	Messenger
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
analytiků	analytik	k1gMnPc2	analytik
bylo	být	k5eAaImAgNnS	být
důvodem	důvod	k1gInSc7	důvod
zavedení	zavedení	k1gNnSc1	zavedení
těchto	tento	k3xDgFnPc2	tento
plateb	platba	k1gFnPc2	platba
získání	získání	k1gNnSc2	získání
většího	veliký	k2eAgInSc2d2	veliký
přehledu	přehled	k1gInSc2	přehled
o	o	k7c4	o
chování	chování	k1gNnSc4	chování
svých	svůj	k3xOyFgMnPc2	svůj
uživatelů	uživatel	k1gMnPc2	uživatel
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2018	[number]	k4	2018
vedení	vedení	k1gNnSc1	vedení
Facebooku	Facebook	k1gInSc2	Facebook
oznámilo	oznámit	k5eAaPmAgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
chystá	chystat	k5eAaImIp3nS	chystat
navýšit	navýšit	k5eAaPmF	navýšit
množství	množství	k1gNnSc4	množství
inzertního	inzertní	k2eAgNnSc2d1	inzertní
sdělení	sdělení	k1gNnSc2	sdělení
<g/>
,	,	kIx,	,
při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
příležitosti	příležitost	k1gFnSc6	příležitost
Mark	Mark	k1gMnSc1	Mark
Zuckerberg	Zuckerberg	k1gMnSc1	Zuckerberg
všechny	všechen	k3xTgInPc4	všechen
ujistil	ujistit	k5eAaPmAgMnS	ujistit
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
že	že	k8xS	že
čas	čas	k1gInSc4	čas
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
strávíme	strávit	k5eAaPmIp1nP	strávit
na	na	k7c6	na
Facebooku	Facebook	k1gInSc6	Facebook
je	být	k5eAaImIp3nS	být
dobře	dobře	k6eAd1	dobře
využitý	využitý	k2eAgInSc1d1	využitý
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
začátkem	začátkem	k7c2	začátkem
roku	rok	k1gInSc2	rok
2018	[number]	k4	2018
Facebook	Facebook	k1gInSc1	Facebook
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
novou	nový	k2eAgFnSc4d1	nová
strategii	strategie	k1gFnSc4	strategie
zakazující	zakazující	k2eAgFnSc2d1	zakazující
reklamy	reklama	k1gFnSc2	reklama
na	na	k7c4	na
finanční	finanční	k2eAgInPc4d1	finanční
produkty	produkt	k1gInPc4	produkt
a	a	k8xC	a
služby	služba	k1gFnPc4	služba
jako	jako	k8xS	jako
binární	binární	k2eAgFnPc4d1	binární
opce	opce	k1gFnPc4	opce
<g/>
,	,	kIx,	,
primární	primární	k2eAgFnPc4d1	primární
nabídky	nabídka	k1gFnPc4	nabídka
mincí	mince	k1gFnPc2	mince
a	a	k8xC	a
kryptoměny	kryptoměn	k2eAgFnPc1d1	kryptoměn
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
Facebook	Facebook	k1gInSc1	Facebook
přiznal	přiznat	k5eAaPmAgInS	přiznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
lidé	člověk	k1gMnPc1	člověk
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
zavaleni	zavalit	k5eAaPmNgMnP	zavalit
obsahem	obsah	k1gInSc7	obsah
<g/>
,	,	kIx,	,
na	na	k7c4	na
který	který	k3yIgInSc4	který
reagují	reagovat	k5eAaBmIp3nP	reagovat
stále	stále	k6eAd1	stále
méně	málo	k6eAd2	málo
<g/>
.	.	kIx.	.
</s>
<s>
Zuckerberg	Zuckerberg	k1gMnSc1	Zuckerberg
proto	proto	k8xC	proto
zadal	zadat	k5eAaPmAgMnS	zadat
svým	svůj	k3xOyFgInPc3	svůj
produktovým	produktový	k2eAgInPc3d1	produktový
týmům	tým	k1gInPc3	tým
úkol	úkol	k1gInSc4	úkol
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
na	na	k7c6	na
Facebooku	Facebook	k1gInSc6	Facebook
lidé	člověk	k1gMnPc1	člověk
zažívali	zažívat	k5eAaImAgMnP	zažívat
smysluplnější	smysluplný	k2eAgFnPc4d2	smysluplnější
sociální	sociální	k2eAgFnPc4d1	sociální
interakce	interakce	k1gFnPc4	interakce
<g/>
.	.	kIx.	.
</s>
<s>
Uživatelům	uživatel	k1gMnPc3	uživatel
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
přednostně	přednostně	k6eAd1	přednostně
zobrazovat	zobrazovat	k5eAaImF	zobrazovat
obsah	obsah	k1gInSc4	obsah
od	od	k7c2	od
jejich	jejich	k3xOp3gMnPc2	jejich
přátel	přítel	k1gMnPc2	přítel
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
dále	daleko	k6eAd2	daleko
omezilo	omezit	k5eAaPmAgNnS	omezit
zobrazování	zobrazování	k1gNnSc4	zobrazování
neplacených	placený	k2eNgInPc2d1	neplacený
příspěvků	příspěvek	k1gInPc2	příspěvek
stránek	stránka	k1gFnPc2	stránka
celebrit	celebrita	k1gFnPc2	celebrita
<g/>
,	,	kIx,	,
firem	firma	k1gFnPc2	firma
<g/>
,	,	kIx,	,
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
té	ten	k3xDgFnSc6	ten
<g/>
,	,	kIx,	,
co	co	k9	co
Facebook	Facebook	k1gInSc1	Facebook
oznámil	oznámit	k5eAaPmAgInS	oznámit
zpřísnění	zpřísnění	k1gNnSc4	zpřísnění
ochrany	ochrana	k1gFnSc2	ochrana
osobních	osobní	k2eAgNnPc2d1	osobní
dat	datum	k1gNnPc2	datum
jako	jako	k8xS	jako
reakci	reakce	k1gFnSc3	reakce
na	na	k7c4	na
neoprávněné	oprávněný	k2eNgNnSc4d1	neoprávněné
získání	získání	k1gNnSc4	získání
dat	datum	k1gNnPc2	datum
od	od	k7c2	od
50	[number]	k4	50
milionech	milion	k4xCgInPc6	milion
uživatelů	uživatel	k1gMnPc2	uživatel
Facebooku	Facebook	k1gInSc2	Facebook
společností	společnost	k1gFnPc2	společnost
Cambridge	Cambridge	k1gFnSc2	Cambridge
Analytica	Analyticum	k1gNnSc2	Analyticum
<g/>
,	,	kIx,	,
objevil	objevit	k5eAaPmAgInS	objevit
se	se	k3xPyFc4	se
interní	interní	k2eAgInSc1d1	interní
dokument	dokument	k1gInSc1	dokument
Andrewa	Andrewus	k1gMnSc2	Andrewus
Boswortha	Bosworth	k1gMnSc2	Bosworth
z	z	k7c2	z
června	červen	k1gInSc2	červen
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
firmu	firma	k1gFnSc4	firma
je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
propojení	propojení	k1gNnSc1	propojení
co	co	k9	co
největšího	veliký	k2eAgInSc2d3	veliký
počtu	počet	k1gInSc2	počet
uživatelů	uživatel	k1gMnPc2	uživatel
<g/>
,	,	kIx,	,
i	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
Facebook	Facebook	k1gInSc1	Facebook
může	moct	k5eAaImIp3nS	moct
sloužit	sloužit	k5eAaImF	sloužit
negativním	negativní	k2eAgInPc3d1	negativní
účelům	účel	k1gInPc3	účel
<g/>
.	.	kIx.	.
</s>
<s>
Bosworth	Bosworth	k1gInSc1	Bosworth
v	v	k7c6	v
dokumentu	dokument	k1gInSc6	dokument
napsal	napsat	k5eAaPmAgMnS	napsat
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
Ošklivou	ošklivý	k2eAgFnSc7d1	ošklivá
pravdou	pravda	k1gFnSc7	pravda
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
věříme	věřit	k5eAaImIp1nP	věřit
ve	v	k7c4	v
spojování	spojování	k1gNnPc4	spojování
lidí	člověk	k1gMnPc2	člověk
tak	tak	k9	tak
hluboce	hluboko	k6eAd1	hluboko
<g/>
,	,	kIx,	,
že	že	k8xS	že
cokoli	cokoli	k3yInSc4	cokoli
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
nám	my	k3xPp1nPc3	my
umožní	umožnit	k5eAaPmIp3nS	umožnit
častěji	často	k6eAd2	často
spojit	spojit	k5eAaPmF	spojit
více	hodně	k6eAd2	hodně
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
́	́	k?	́
<g/>
de	de	k?	de
facto	facto	k1gNnSc4	facto
́	́	k?	́
dobré	dobrý	k2eAgNnSc1d1	dobré
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tak	tak	k6eAd1	tak
spojujeme	spojovat	k5eAaImIp1nP	spojovat
lidi	člověk	k1gMnPc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
špatné	špatný	k2eAgNnSc1d1	špatné
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
to	ten	k3xDgNnSc4	ten
využijí	využít	k5eAaPmIp3nP	využít
negativně	negativně	k6eAd1	negativně
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
to	ten	k3xDgNnSc1	ten
stát	stát	k5eAaPmF	stát
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
když	když	k8xS	když
někoho	někdo	k3yInSc4	někdo
vystaví	vystavit	k5eAaPmIp3nS	vystavit
kyberšikaně	kyberšikaně	k6eAd1	kyberšikaně
<g/>
.	.	kIx.	.
</s>
<s>
Někdo	někdo	k3yInSc1	někdo
možná	možná	k9	možná
zemře	zemřít	k5eAaPmIp3nS	zemřít
při	při	k7c6	při
teroristickém	teroristický	k2eAgInSc6d1	teroristický
útoku	útok	k1gInSc6	útok
domluveném	domluvený	k2eAgInSc6d1	domluvený
na	na	k7c6	na
naší	náš	k3xOp1gFnSc6	náš
síti	síť	k1gFnSc6	síť
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
stejně	stejně	k6eAd1	stejně
spojujeme	spojovat	k5eAaImIp1nP	spojovat
lidi	člověk	k1gMnPc4	člověk
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Podle	podle	k7c2	podle
Zuckerberga	Zuckerberg	k1gMnSc2	Zuckerberg
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
výzvu	výzva	k1gFnSc4	výzva
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yRgFnSc7	který
většina	většina	k1gFnSc1	většina
lidí	člověk	k1gMnPc2	člověk
ve	v	k7c6	v
Facebooku	Facebook	k1gInSc6	Facebook
nesouhlasila	souhlasit	k5eNaImAgFnS	souhlasit
a	a	k8xC	a
sám	sám	k3xTgMnSc1	sám
Bosworth	Bosworth	k1gMnSc1	Bosworth
se	se	k3xPyFc4	se
od	od	k7c2	od
vyznění	vyznění	k1gNnSc2	vyznění
textu	text	k1gInSc2	text
distancoval	distancovat	k5eAaBmAgMnS	distancovat
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
výslechu	výslech	k1gInSc6	výslech
před	před	k7c7	před
americkým	americký	k2eAgInSc7d1	americký
kongresem	kongres	k1gInSc7	kongres
Mark	Mark	k1gMnSc1	Mark
Zuckerberg	Zuckerberg	k1gMnSc1	Zuckerberg
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
od	od	k7c2	od
25.	[number]	k4	25.
května	květen	k1gInSc2	květen
Facebook	Facebook	k1gInSc1	Facebook
použije	použít	k5eAaPmIp3nS	použít
pravidla	pravidlo	k1gNnPc1	pravidlo
GDPR	GDPR	kA	GDPR
nejen	nejen	k6eAd1	nejen
pro	pro	k7c4	pro
Evropany	Evropan	k1gMnPc4	Evropan
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
pro	pro	k7c4	pro
všechny	všechen	k3xTgMnPc4	všechen
ostatní	ostatní	k2eAgMnPc4d1	ostatní
uživatele	uživatel	k1gMnPc4	uživatel
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Američanů	Američan	k1gMnPc2	Američan
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2018	[number]	k4	2018
Facebook	Facebook	k1gInSc1	Facebook
oznámil	oznámit	k5eAaPmAgInS	oznámit
spuštění	spuštění	k1gNnSc4	spuštění
služby	služba	k1gFnSc2	služba
pomáhajícím	pomáhající	k2eAgMnPc3d1	pomáhající
uživatelům	uživatel	k1gMnPc3	uživatel
hledajícím	hledající	k2eAgMnPc3d1	hledající
dlouhodobý	dlouhodobý	k2eAgInSc1d1	dlouhodobý
vztah	vztah	k1gInSc4	vztah
najít	najít	k5eAaPmF	najít
si	se	k3xPyFc3	se
partnera	partner	k1gMnSc4	partner
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
analytika	analytik	k1gMnSc2	analytik
Jamese	Jamese	k1gFnSc1	Jamese
Cordwella	Cordwella	k1gFnSc1	Cordwella
by	by	kYmCp3nS	by
tato	tento	k3xDgFnSc1	tento
služba	služba	k1gFnSc1	služba
mohla	moct	k5eAaImAgFnS	moct
vést	vést	k5eAaImF	vést
ke	k	k7c3	k
zvýšení	zvýšení	k1gNnSc3	zvýšení
množství	množství	k1gNnSc2	množství
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
uživatelé	uživatel	k1gMnPc1	uživatel
na	na	k7c6	na
Facebooku	Facebook	k1gInSc6	Facebook
tráví	trávit	k5eAaImIp3nS	trávit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2018	[number]	k4	2018
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
restrukturalizace	restrukturalizace	k1gFnSc1	restrukturalizace
Facebooku	Facebook	k1gInSc2	Facebook
<g/>
.	.	kIx.	.
</s>
<s>
Facebook	Facebook	k1gInSc1	Facebook
byl	být	k5eAaImAgInS	být
nově	nově	k6eAd1	nově
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
na	na	k7c4	na
tři	tři	k4xCgFnPc4	tři
hlavní	hlavní	k2eAgFnPc4d1	hlavní
divize	divize	k1gFnPc4	divize
<g/>
,	,	kIx,	,
divizi	divize	k1gFnSc4	divize
pro	pro	k7c4	pro
aplikace	aplikace	k1gFnPc4	aplikace
Facebook	Facebook	k1gInSc4	Facebook
<g/>
,	,	kIx,	,
Instragram	Instragram	k1gInSc4	Instragram
<g/>
,	,	kIx,	,
Messenger	Messenger	k1gInSc4	Messenger
a	a	k8xC	a
WhatsApp	WhatsApp	k1gInSc4	WhatsApp
<g/>
,	,	kIx,	,
divizi	divize	k1gFnSc4	divize
pro	pro	k7c4	pro
umělou	umělý	k2eAgFnSc4d1	umělá
inteligenci	inteligence	k1gFnSc4	inteligence
<g/>
,	,	kIx,	,
rozšířenou	rozšířený	k2eAgFnSc4d1	rozšířená
a	a	k8xC	a
virtuální	virtuální	k2eAgFnSc4d1	virtuální
realitu	realita	k1gFnSc4	realita
a	a	k8xC	a
také	také	k9	také
blockchain	blockchain	k1gInSc4	blockchain
a	a	k8xC	a
divizi	divize	k1gFnSc4	divize
pro	pro	k7c4	pro
reklamní	reklamní	k2eAgInPc4d1	reklamní
a	a	k8xC	a
analytické	analytický	k2eAgInPc4d1	analytický
nástroje	nástroj	k1gInPc4	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Vedením	vedení	k1gNnSc7	vedení
divize	divize	k1gFnSc2	divize
zabývající	zabývající	k2eAgFnPc4d1	zabývající
se	s	k7c7	s
blockchainem	blockchain	k1gInSc7	blockchain
byl	být	k5eAaImAgMnS	být
pověřen	pověřen	k2eAgMnSc1d1	pověřen
David	David	k1gMnSc1	David
Marcus	Marcus	k1gMnSc1	Marcus
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
šéf	šéf	k1gMnSc1	šéf
PayPalu	PayPal	k1gInSc2	PayPal
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
měl	mít	k5eAaImAgInS	mít
před	před	k7c7	před
restrukturalizací	restrukturalizace	k1gFnSc7	restrukturalizace
na	na	k7c4	na
starost	starost	k1gFnSc4	starost
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Facebooku	Facebook	k1gInSc2	Facebook
Messenger	Messenger	k1gMnSc1	Messenger
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
serveru	server	k1gInSc2	server
Cheddar.com	Cheddar.com	k1gInSc4	Cheddar.com
Facebook	Facebook	k1gInSc1	Facebook
začal	začít	k5eAaPmAgInS	začít
připravovat	připravovat	k5eAaImF	připravovat
vlastní	vlastní	k2eAgFnSc4d1	vlastní
kryptoměnu	kryptoměn	k2eAgFnSc4d1	kryptoměn
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
kterou	který	k3yRgFnSc4	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mělo	mít	k5eAaImAgNnS	mít
platit	platit	k5eAaImF	platit
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
Facebooku	Facebook	k1gInSc6	Facebook
<g/>
.	.	kIx.	.
<g/>
Koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
2018	[number]	k4	2018
si	se	k3xPyFc3	se
Facebook	Facebook	k1gInSc1	Facebook
nechal	nechat	k5eAaPmAgInS	nechat
patentovat	patentovat	k5eAaBmF	patentovat
technologii	technologie	k1gFnSc4	technologie
Offline	Offlin	k1gInSc5	Offlin
Trajectories	Trajectoriesa	k1gFnPc2	Trajectoriesa
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
umožnit	umožnit	k5eAaPmF	umožnit
odhadnout	odhadnout	k5eAaPmF	odhadnout
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
uživatel	uživatel	k1gMnSc1	uživatel
chystá	chystat	k5eAaImIp3nS	chystat
<g/>
.	.	kIx.	.
</s>
<s>
Facebook	Facebook	k1gInSc1	Facebook
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
tímto	tento	k3xDgInSc7	tento
algoritmem	algoritmus	k1gInSc7	algoritmus
pokoušel	pokoušet	k5eAaImAgInS	pokoušet
hledat	hledat	k5eAaImF	hledat
nějakou	nějaký	k3yIgFnSc4	nějaký
logiku	logika	k1gFnSc4	logika
ve	v	k7c6	v
stávajících	stávající	k2eAgFnPc6d1	stávající
geografických	geografický	k2eAgFnPc6d1	geografická
polohách	poloha	k1gFnPc6	poloha
uživatele	uživatel	k1gMnSc2	uživatel
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
patentu	patent	k1gInSc2	patent
je	být	k5eAaImIp3nS	být
i	i	k9	i
porovnávání	porovnávání	k1gNnSc4	porovnávání
profilů	profil	k1gInPc2	profil
uživatelů	uživatel	k1gMnPc2	uživatel
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
jsou	být	k5eAaImIp3nP	být
si	se	k3xPyFc3	se
nějakým	nějaký	k3yIgInSc7	nějaký
způsobem	způsob	k1gInSc7	způsob
podobní	podobný	k2eAgMnPc1d1	podobný
a	a	k8xC	a
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
uživatelům	uživatel	k1gMnPc3	uživatel
navrhovat	navrhovat	k5eAaImF	navrhovat
restaurace	restaurace	k1gFnPc4	restaurace
<g/>
,	,	kIx,	,
podniky	podnik	k1gInPc4	podnik
<g/>
,	,	kIx,	,
akce	akce	k1gFnPc4	akce
nebo	nebo	k8xC	nebo
události	událost	k1gFnPc4	událost
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2019	[number]	k4	2019
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
Libra	libra	k1gFnSc1	libra
White	Whit	k1gInSc5	Whit
Paper	Paper	k1gInSc4	Paper
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgNnSc6	který
Facebookem	Facebook	k1gInSc7	Facebook
inciovaná	inciovaný	k2eAgFnSc1d1	inciovaný
nezisková	ziskový	k2eNgFnSc1d1	nezisková
organizace	organizace	k1gFnSc1	organizace
Libra	libra	k1gFnSc1	libra
Association	Association	k1gInSc1	Association
popisuje	popisovat	k5eAaImIp3nS	popisovat
novou	nový	k2eAgFnSc4d1	nová
měnu	měna	k1gFnSc4	měna
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
white	white	k5eAaPmIp2nP	white
paper	paper	k1gInSc4	paper
ale	ale	k8xC	ale
neposkytuje	poskytovat	k5eNaImIp3nS	poskytovat
uspokojivé	uspokojivý	k2eAgFnPc4d1	uspokojivá
odpovědi	odpověď	k1gFnPc4	odpověď
na	na	k7c4	na
znepokojivé	znepokojivý	k2eAgFnPc4d1	znepokojivá
otázky	otázka	k1gFnPc4	otázka
a	a	k8xC	a
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
i	i	k9	i
vzájemně	vzájemně	k6eAd1	vzájemně
si	se	k3xPyFc3	se
odporující	odporující	k2eAgFnPc1d1	odporující
fráze	fráze	k1gFnPc1	fráze
<g/>
.	.	kIx.	.
</s>
<s>
Neodpovídá	odpovídat	k5eNaImIp3nS	odpovídat
na	na	k7c4	na
otázku	otázka	k1gFnSc4	otázka
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
Facebook	Facebook	k1gInSc1	Facebook
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
záměr	záměr	k1gInSc4	záměr
zapojit	zapojit	k5eAaPmF	zapojit
do	do	k7c2	do
finančního	finanční	k2eAgInSc2d1	finanční
systému	systém	k1gInSc2	systém
lidi	člověk	k1gMnPc4	člověk
bez	bez	k7c2	bez
bankovního	bankovní	k2eAgInSc2d1	bankovní
účtu	účet	k1gInSc2	účet
<g/>
,	,	kIx,	,
když	když	k8xS	když
jiné	jiný	k2eAgFnPc1d1	jiná
společnosti	společnost	k1gFnPc1	společnost
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
M-Pesa	M-Pesa	k1gFnSc1	M-Pesa
a	a	k8xC	a
WeChat	WeChat	k1gFnSc1	WeChat
ke	k	k7c3	k
stejnému	stejný	k2eAgInSc3d1	stejný
záměru	záměr	k1gInSc3	záměr
svojí	svojit	k5eAaImIp3nP	svojit
kryptoměnu	kryptoměn	k2eAgFnSc4d1	kryptoměn
nepotřebují	potřebovat	k5eNaImIp3nP	potřebovat
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgFnPc4d1	další
otázky	otázka	k1gFnPc4	otázka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
v	v	k7c6	v
dokumentu	dokument	k1gInSc6	dokument
není	být	k5eNaImIp3nS	být
odpověď	odpověď	k1gFnSc4	odpověď
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
<s>
zda	zda	k8xS	zda
Facebook	Facebook	k1gInSc1	Facebook
může	moct	k5eAaImIp3nS	moct
garantovat	garantovat	k5eAaBmF	garantovat
rezistenci	rezistence	k1gFnSc4	rezistence
proti	proti	k7c3	proti
vyloučení	vyloučení	k1gNnSc3	vyloučení
z	z	k7c2	z
transakčního	transakční	k2eAgInSc2d1	transakční
procesu	proces	k1gInSc2	proces
či	či	k8xC	či
odolnost	odolnost	k1gFnSc4	odolnost
proti	proti	k7c3	proti
manipulaci	manipulace	k1gFnSc3	manipulace
transakcí	transakce	k1gFnPc2	transakce
<g/>
,	,	kIx,	,
cenzuře	cenzura	k1gFnSc6	cenzura
a	a	k8xC	a
zabavení	zabavení	k1gNnSc6	zabavení
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
v	v	k7c6	v
případě	případ	k1gInSc6	případ
hacknutí	hacknutí	k1gNnSc2	hacknutí
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
jaká	jaký	k3yIgNnPc4	jaký
práva	právo	k1gNnPc4	právo
drží	držet	k5eAaImIp3nS	držet
uživatel	uživatel	k1gMnSc1	uživatel
k	k	k7c3	k
Libra	libra	k1gFnSc1	libra
tokenu	token	k1gInSc3	token
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
zda	zda	k8xS	zda
vlatnictví	vlatnictví	k1gNnSc1	vlatnictví
privátních	privátní	k2eAgInPc2d1	privátní
klíčů	klíč	k1gInPc2	klíč
zaručuje	zaručovat	k5eAaImIp3nS	zaručovat
skutečné	skutečný	k2eAgNnSc4d1	skutečné
vlastnictví	vlastnictví	k1gNnSc4	vlastnictví
tokenu	token	k1gInSc2	token
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
Libra	libra	k1gFnSc1	libra
Blockchain	Blockchain	k1gInSc1	Blockchain
skutečně	skutečně	k6eAd1	skutečně
blockchain	blockchain	k2eAgInSc1d1	blockchain
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
zda	zda	k8xS	zda
bude	být	k5eAaImBp3nS	být
možné	možný	k2eAgNnSc1d1	možné
v	v	k7c6	v
případě	případ	k1gInSc6	případ
zátěžové	zátěžový	k2eAgFnSc2d1	zátěžová
situace	situace	k1gFnSc2	situace
<g/>
,	,	kIx,	,
např.	např.	kA	např.
krize	krize	k1gFnSc1	krize
<g/>
,	,	kIx,	,
možné	možný	k2eAgFnPc1d1	možná
vyměnit	vyměnit	k5eAaPmF	vyměnit
Libru	libra	k1gFnSc4	libra
za	za	k7c7	za
bezpečnou	bezpečný	k2eAgFnSc7d1	bezpečná
fiat	fiat	k1gInSc1	fiat
měnu	měna	k1gFnSc4	měna
v	v	k7c6	v
kurzu	kurz	k1gInSc6	kurz
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
proč	proč	k6eAd1	proč
musel	muset	k5eAaImAgMnS	muset
Facebook	Facebook	k1gInSc4	Facebook
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
projektem	projekt	k1gInSc7	projekt
vytvořit	vytvořit	k5eAaPmF	vytvořit
dceřinou	dceřiný	k2eAgFnSc4d1	dceřiná
společnost	společnost	k1gFnSc4	společnost
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
proč	proč	k6eAd1	proč
přestože	přestože	k8xS	přestože
Facebook	Facebook	k1gInSc1	Facebook
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
projektu	projekt	k1gInSc6	projekt
dominantní	dominantní	k2eAgFnSc4d1	dominantní
roli	role	k1gFnSc4	role
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
uveden	uvést	k5eAaPmNgMnS	uvést
jen	jen	k9	jen
jako	jako	k9	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
partnerů	partner	k1gMnPc2	partner
<g/>
.	.	kIx.	.
<g/>
Celý	celý	k2eAgInSc1d1	celý
dokument	dokument	k1gInSc1	dokument
tak	tak	k6eAd1	tak
vypadá	vypadat	k5eAaImIp3nS	vypadat
jako	jako	k8xC	jako
snaha	snaha	k1gFnSc1	snaha
něco	něco	k3yInSc4	něco
zamaskovat	zamaskovat	k5eAaPmF	zamaskovat
nejspíše	nejspíše	k9	nejspíše
před	před	k7c7	před
běžnými	běžný	k2eAgMnPc7d1	běžný
lidmi	člověk	k1gMnPc7	člověk
a	a	k8xC	a
regulátory	regulátor	k1gMnPc7	regulátor
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
Facebooku	Facebook	k1gInSc2	Facebook
je	být	k5eAaImIp3nS	být
stát	stát	k5eAaPmF	stát
se	se	k3xPyFc4	se
vydavatelem	vydavatel	k1gMnSc7	vydavatel
elektronických	elektronický	k2eAgInPc2d1	elektronický
peněz	peníze	k1gInPc2	peníze
a	a	k8xC	a
mít	mít	k5eAaImF	mít
velký	velký	k2eAgInSc4d1	velký
podíl	podíl	k1gInSc4	podíl
na	na	k7c6	na
všech	všecek	k3xTgFnPc6	všecek
globálně	globálně	k6eAd1	globálně
provedených	provedený	k2eAgFnPc6d1	provedená
platbách	platba	k1gFnPc6	platba
<g/>
,	,	kIx,	,
k	k	k7c3	k
čemuž	což	k3yRnSc3	což
ale	ale	k9	ale
nejspíše	nejspíše	k9	nejspíše
nemá	mít	k5eNaImIp3nS	mít
patřičné	patřičný	k2eAgFnPc4d1	patřičná
licence	licence	k1gFnPc4	licence
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
obejít	obejít	k5eAaPmF	obejít
kryptoměnovo-blockchainovou	kryptoměnovolockchainový	k2eAgFnSc7d1	kryptoměnovo-blockchainový
rétorikou	rétorika	k1gFnSc7	rétorika
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
disponuje	disponovat	k5eAaBmIp3nS	disponovat
dostatečnou	dostatečný	k2eAgFnSc7d1	dostatečná
masou	masa	k1gFnSc7	masa
uživatelů	uživatel	k1gMnPc2	uživatel
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
na	na	k7c6	na
finančním	finanční	k2eAgInSc6d1	finanční
trhu	trh	k1gInSc6	trh
mohl	moct	k5eAaImAgInS	moct
dominantním	dominantní	k2eAgMnSc7d1	dominantní
hráčem	hráč	k1gMnSc7	hráč
stát	stát	k5eAaImF	stát
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
protože	protože	k8xS	protože
Libra	libra	k1gFnSc1	libra
odráží	odrážet	k5eAaImIp3nS	odrážet
měnový	měnový	k2eAgInSc1d1	měnový
koš	koš	k1gInSc1	koš
a	a	k8xC	a
málo	málo	k6eAd1	málo
rizikové	rizikový	k2eAgInPc1d1	rizikový
dluhopisy	dluhopis	k1gInPc1	dluhopis
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
vlastně	vlastně	k9	vlastně
o	o	k7c4	o
spekulativní	spekulativní	k2eAgNnSc4d1	spekulativní
aktivum	aktivum	k1gNnSc4	aktivum
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
by	by	kYmCp3nS	by
regulátory	regulátor	k1gInPc4	regulátor
mohlo	moct	k5eAaImAgNnS	moct
a	a	k8xC	a
mělo	mít	k5eAaImAgNnS	mít
zajímat	zajímat	k5eAaImF	zajímat
<g/>
.	.	kIx.	.
</s>
<s>
Ohledně	ohledně	k6eAd1	ohledně
Libra	libra	k1gFnSc1	libra
Blockchainu	Blockchain	k1gInSc2	Blockchain
dokument	dokument	k1gInSc1	dokument
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
každý	každý	k3xTgInSc1	každý
blok	blok	k1gInSc1	blok
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
souboru	soubor	k1gInSc2	soubor
transakcí	transakce	k1gFnPc2	transakce
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
účastníci	účastník	k1gMnPc1	účastník
odsouhlasí	odsouhlasit	k5eAaPmIp3nP	odsouhlasit
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
mechanismu	mechanismus	k1gInSc2	mechanismus
LibraBFT	LibraBFT	k1gFnSc2	LibraBFT
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
transakce	transakce	k1gFnSc1	transakce
řadí	řadit	k5eAaImIp3nS	řadit
a	a	k8xC	a
finalizuje	finalizovat	k5eAaBmIp3nS	finalizovat
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
současně	současně	k6eAd1	současně
o	o	k7c6	o
něm	on	k3xPp3gMnSc6	on
protichůdně	protichůdně	k6eAd1	protichůdně
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
předchozích	předchozí	k2eAgInPc2d1	předchozí
blockchainů	blockchain	k1gInPc2	blockchain
je	být	k5eAaImIp3nS	být
Libra	libra	k1gFnSc1	libra
Blockchain	Blockchain	k2eAgInSc1d1	Blockchain
jednolitou	jednolitý	k2eAgFnSc7d1	jednolitá
datovou	datový	k2eAgFnSc7d1	datová
strukturou	struktura	k1gFnSc7	struktura
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
i	i	k9	i
mazání	mazání	k1gNnSc4	mazání
starších	starý	k2eAgInPc2d2	starší
záznamů	záznam	k1gInPc2	záznam
<g/>
.	.	kIx.	.
</s>
<s>
Dceřiná	dceřiný	k2eAgFnSc1d1	dceřiná
společnost	společnost	k1gFnSc1	společnost
Calibra	Calibra	k1gFnSc1	Calibra
byla	být	k5eAaImAgFnS	být
podle	podle	k7c2	podle
white	whit	k1gInSc5	whit
paperu	paper	k1gInSc2	paper
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
kvůli	kvůli	k7c3	kvůli
vytvoření	vytvoření	k1gNnSc3	vytvoření
peněženky	peněženka	k1gFnSc2	peněženka
pro	pro	k7c4	pro
digitální	digitální	k2eAgInSc4d1	digitální
token	token	k1gInSc4	token
Libry	libra	k1gFnSc2	libra
a	a	k8xC	a
podle	podle	k7c2	podle
všeho	všecek	k3xTgNnSc2	všecek
tato	tento	k3xDgFnSc1	tento
dceřiná	dceřiný	k2eAgFnSc1d1	dceřiná
společnost	společnost	k1gFnSc1	společnost
jako	jako	k9	jako
jediná	jediný	k2eAgFnSc1d1	jediná
z	z	k7c2	z
celé	celý	k2eAgFnSc2d1	celá
organizace	organizace	k1gFnSc2	organizace
usiluje	usilovat	k5eAaImIp3nS	usilovat
o	o	k7c4	o
získání	získání	k1gNnSc4	získání
finanční	finanční	k2eAgFnSc2d1	finanční
licence	licence	k1gFnSc2	licence
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
licence	licence	k1gFnSc1	licence
na	na	k7c4	na
převod	převod	k1gInSc4	převod
peněz	peníze	k1gInPc2	peníze
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Facebooku	Facebook	k1gInSc2	Facebook
Calibra	Calibro	k1gNnSc2	Calibro
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
kvůli	kvůli	k7c3	kvůli
oddělení	oddělení	k1gNnSc3	oddělení
finančních	finanční	k2eAgInPc2d1	finanční
a	a	k8xC	a
osobních	osobní	k2eAgNnPc2d1	osobní
dat	datum	k1gNnPc2	datum
uživatelů	uživatel	k1gMnPc2	uživatel
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
finanční	finanční	k2eAgNnPc1d1	finanční
data	datum	k1gNnPc1	datum
uživatelů	uživatel	k1gMnPc2	uživatel
nebudou	být	k5eNaImBp3nP	být
zneužity	zneužít	k5eAaPmNgInP	zneužít
pro	pro	k7c4	pro
cross-selling	crosselling	k1gInSc4	cross-selling
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
to	ten	k3xDgNnSc1	ten
uživatelé	uživatel	k1gMnPc1	uživatel
sami	sám	k3xTgMnPc1	sám
nepovolí	povolit	k5eNaPmIp3nP	povolit
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
protože	protože	k8xS	protože
Facebook	Facebook	k1gInSc1	Facebook
používá	používat	k5eAaImIp3nS	používat
různé	různý	k2eAgInPc4d1	různý
úskoky	úskok	k1gInPc4	úskok
k	k	k7c3	k
získávání	získávání	k1gNnSc3	získávání
souhlasu	souhlas	k1gInSc2	souhlas
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
očekávat	očekávat	k5eAaImF	očekávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nějaký	nějaký	k3yIgInSc1	nějaký
úskok	úskok	k1gInSc1	úskok
bude	být	k5eAaImBp3nS	být
využit	využít	k5eAaPmNgInS	využít
i	i	k9	i
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Calibry	Calibra	k1gFnSc2	Calibra
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
Libra	libra	k1gFnSc1	libra
i	i	k9	i
Calibra	Calibr	k1gInSc2	Calibr
jsou	být	k5eAaImIp3nP	být
produkty	produkt	k1gInPc1	produkt
Facebooku	Facebook	k1gInSc2	Facebook
<g/>
,	,	kIx,	,
Facebook	Facebook	k1gInSc1	Facebook
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
vzbudit	vzbudit	k5eAaPmF	vzbudit
dojem	dojem	k1gInSc4	dojem
nezávislosti	nezávislost	k1gFnSc2	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
Facebook	Facebook	k1gInSc1	Facebook
sice	sice	k8xC	sice
slibuje	slibovat	k5eAaImIp3nS	slibovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
dataCalibry	dataCalibra	k1gFnPc1	dataCalibra
nebudou	být	k5eNaImBp3nP	být
poskytnuta	poskytnut	k2eAgFnSc1d1	poskytnuta
Facebooku	Facebook	k1gInSc3	Facebook
pro	pro	k7c4	pro
reklamní	reklamní	k2eAgInPc4d1	reklamní
účely	účel	k1gInPc4	účel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neříká	říkat	k5eNaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
data	datum	k1gNnPc1	datum
Facebooku	Facebook	k1gInSc2	Facebook
nebudou	být	k5eNaImBp3nP	být
poskytnuta	poskytnout	k5eAaPmNgFnS	poskytnout
Calibře	Calibra	k1gFnSc3	Calibra
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
aby	aby	kYmCp3nS	aby
Facebook	Facebook	k1gInSc1	Facebook
prostřednictví	prostřednictví	k1gNnSc2	prostřednictví
Calibry	Calibra	k1gFnSc2	Calibra
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
uživatel	uživatel	k1gMnSc1	uživatel
povolí	povolit	k5eAaPmIp3nS	povolit
Facebooku	Facebook	k1gInSc2	Facebook
dostávat	dostávat	k5eAaImF	dostávat
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
svých	svůj	k3xOyFgFnPc6	svůj
finančních	finanční	k2eAgFnPc6d1	finanční
transakcích	transakce	k1gFnPc6	transakce
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
téměř	téměř	k6eAd1	téměř
všichni	všechen	k3xTgMnPc1	všechen
uživatelé	uživatel	k1gMnPc1	uživatel
Libry	libra	k1gFnSc2	libra
budou	být	k5eAaImBp3nP	být
využívat	využívat	k5eAaPmF	využívat
některou	některý	k3yIgFnSc4	některý
ze	z	k7c2	z
služeb	služba	k1gFnPc2	služba
Facebooku	Facebook	k1gInSc2	Facebook
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
spárovat	spárovat	k5eAaImF	spárovat
transkace	transkace	k1gFnPc4	transkace
z	z	k7c2	z
Calibry	Calibra	k1gFnSc2	Calibra
s	s	k7c7	s
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
uživatelskými	uživatelský	k2eAgInPc7d1	uživatelský
účty	účet	k1gInPc7	účet
není	být	k5eNaImIp3nS	být
složité	složitý	k2eAgNnSc1d1	složité
<g/>
.	.	kIx.	.
</s>
<s>
Calibra	Calibra	k1gFnSc1	Calibra
je	být	k5eAaImIp3nS	být
zřejmě	zřejmě	k6eAd1	zřejmě
způsob	způsob	k1gInSc4	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
účast	účast	k1gFnSc1	účast
Facebooku	Facebook	k1gInSc2	Facebook
na	na	k7c6	na
projektu	projekt	k1gInSc6	projekt
co	co	k9	co
nejvíce	hodně	k6eAd3	hodně
zamaskovat	zamaskovat	k5eAaPmF	zamaskovat
ale	ale	k8xC	ale
přitom	přitom	k6eAd1	přitom
si	se	k3xPyFc3	se
udržet	udržet	k5eAaPmF	udržet
v	v	k7c6	v
Libra	libra	k1gFnSc1	libra
Association	Association	k1gInSc4	Association
rozhodující	rozhodující	k2eAgInSc1d1	rozhodující
vliv	vliv	k1gInSc1	vliv
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Možnosti	možnost	k1gFnSc3	možnost
systému	systém	k1gInSc2	systém
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Po	po	k7c6	po
registraci	registrace	k1gFnSc6	registrace
v	v	k7c6	v
systému	systém	k1gInSc6	systém
a	a	k8xC	a
odsouhlasení	odsouhlasení	k1gNnSc6	odsouhlasení
licence	licence	k1gFnSc2	licence
používání	používání	k1gNnSc4	používání
má	mít	k5eAaImIp3nS	mít
uživatel	uživatel	k1gMnSc1	uživatel
možnost	možnost	k1gFnSc4	možnost
vyplnit	vyplnit	k5eAaPmF	vyplnit
svůj	svůj	k3xOyFgInSc4	svůj
detailní	detailní	k2eAgInSc4d1	detailní
profil	profil	k1gInSc4	profil
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
připojovat	připojovat	k5eAaImF	připojovat
k	k	k7c3	k
různým	různý	k2eAgFnPc3d1	různá
skupinám	skupina	k1gFnPc3	skupina
uživatelů	uživatel	k1gMnPc2	uživatel
a	a	k8xC	a
získávat	získávat	k5eAaImF	získávat
nové	nový	k2eAgInPc4d1	nový
kontakty	kontakt	k1gInPc4	kontakt
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jiný	jiný	k2eAgMnSc1d1	jiný
uživatel	uživatel	k1gMnSc1	uživatel
souhlasí	souhlasit	k5eAaImIp3nS	souhlasit
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
váš	váš	k3xOp2gMnSc1	váš
přítel	přítel	k1gMnSc1	přítel
<g/>
,	,	kIx,	,
můžete	moct	k5eAaImIp2nP	moct
vidět	vidět	k5eAaImF	vidět
také	také	k9	také
jeho	on	k3xPp3gInSc4	on
profil	profil	k1gInSc4	profil
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
uživatelé	uživatel	k1gMnPc1	uživatel
ovšem	ovšem	k9	ovšem
svoje	svůj	k3xOyFgInPc4	svůj
profily	profil	k1gInPc4	profil
pomocí	pomocí	k7c2	pomocí
nastavení	nastavení	k1gNnSc2	nastavení
práv	právo	k1gNnPc2	právo
uživatelů	uživatel	k1gMnPc2	uživatel
zveřejňují	zveřejňovat	k5eAaImIp3nP	zveřejňovat
i	i	k9	i
lidem	člověk	k1gMnPc3	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
jsou	být	k5eAaImIp3nP	být
zapsáni	zapsat	k5eAaPmNgMnP	zapsat
pod	pod	k7c7	pod
stejnou	stejný	k2eAgFnSc7d1	stejná
skupinou	skupina	k1gFnSc7	skupina
nebo	nebo	k8xC	nebo
i	i	k8xC	i
celému	celý	k2eAgInSc3d1	celý
internetu	internet	k1gInSc3	internet
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
komunikaci	komunikace	k1gFnSc4	komunikace
mezi	mezi	k7c7	mezi
uživateli	uživatel	k1gMnPc7	uživatel
pomocí	pomocí	k7c2	pomocí
zpráv	zpráva	k1gFnPc2	zpráva
<g/>
,	,	kIx,	,
diskusních	diskusní	k2eAgNnPc2d1	diskusní
fór	fórum	k1gNnPc2	fórum
nebo	nebo	k8xC	nebo
diskusemi	diskuse	k1gFnPc7	diskuse
na	na	k7c6	na
uživatelských	uživatelský	k2eAgInPc6d1	uživatelský
profilech	profil	k1gInPc6	profil
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
The	The	k1gMnSc1	The
Wall	Wall	k1gMnSc1	Wall
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
funkce	funkce	k1gFnPc1	funkce
obstarávají	obstarávat	k5eAaImIp3nP	obstarávat
externí	externí	k2eAgInSc4d1	externí
i	i	k8xC	i
interní	interní	k2eAgFnPc4d1	interní
aplikace	aplikace	k1gFnPc4	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
nejpoužívanější	používaný	k2eAgInPc1d3	nejpoužívanější
pocházejí	pocházet	k5eAaImIp3nP	pocházet
přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
dílen	dílna	k1gFnPc2	dílna
Facebooku	Facebook	k1gInSc2	Facebook
<g/>
.	.	kIx.	.
</s>
<s>
Facebook	Facebook	k1gInSc1	Facebook
ale	ale	k8xC	ale
také	také	k9	také
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
otevřené	otevřený	k2eAgNnSc4d1	otevřené
API	API	kA	API
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
může	moct	k5eAaImIp3nS	moct
kdokoli	kdokoli	k3yInSc1	kdokoli
využít	využít	k5eAaPmF	využít
a	a	k8xC	a
napsat	napsat	k5eAaPmF	napsat
si	se	k3xPyFc3	se
vlastní	vlastní	k2eAgFnSc4d1	vlastní
rozšiřující	rozšiřující	k2eAgFnSc4d1	rozšiřující
aplikaci	aplikace	k1gFnSc4	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
síla	síla	k1gFnSc1	síla
Facebooku	Facebook	k1gInSc2	Facebook
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
propojenosti	propojenost	k1gFnSc6	propojenost
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
komponent	komponenta	k1gFnPc2	komponenta
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
textu	text	k1gInSc2	text
může	moct	k5eAaImIp3nS	moct
sloužit	sloužit	k5eAaImF	sloužit
jako	jako	k9	jako
hypertextové	hypertextový	k2eAgInPc4d1	hypertextový
odkazy	odkaz	k1gInPc4	odkaz
k	k	k7c3	k
dalšímu	další	k2eAgInSc3d1	další
obsahu	obsah	k1gInSc3	obsah
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Téměř	téměř	k6eAd1	téměř
jakýkoliv	jakýkoliv	k3yIgInSc4	jakýkoliv
publikovaný	publikovaný	k2eAgInSc4d1	publikovaný
obsah	obsah	k1gInSc4	obsah
lze	lze	k6eAd1	lze
hodnotit	hodnotit	k5eAaImF	hodnotit
označením	označení	k1gNnSc7	označení
Líbí	líbit	k5eAaImIp3nS	líbit
se	se	k3xPyFc4	se
mi	já	k3xPp1nSc3	já
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
nějakou	nějaký	k3yIgFnSc4	nějaký
dobu	doba	k1gFnSc4	doba
se	se	k3xPyFc4	se
diskutovalo	diskutovat	k5eAaImAgNnS	diskutovat
i	i	k9	i
o	o	k7c4	o
zavedení	zavedení	k1gNnSc4	zavedení
opačného	opačný	k2eAgNnSc2d1	opačné
tlačítka	tlačítko	k1gNnSc2	tlačítko
Nelíbí	líbit	k5eNaImIp3nS	líbit
se	se	k3xPyFc4	se
mi	já	k3xPp1nSc3	já
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterého	který	k3yQgNnSc2	který
se	se	k3xPyFc4	se
zdálo	zdát	k5eAaImAgNnS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
opravdu	opravdu	k6eAd1	opravdu
uskutečněno	uskutečněn	k2eAgNnSc1d1	uskutečněno
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
bylo	být	k5eAaImAgNnS	být
místo	místo	k7c2	místo
tohoto	tento	k3xDgNnSc2	tento
tlačítka	tlačítko	k1gNnSc2	tlačítko
uvedeno	uvést	k5eAaPmNgNnS	uvést
šest	šest	k4xCc1	šest
nových	nový	k2eAgNnPc2d1	nové
tlačítek	tlačítko	k1gNnPc2	tlačítko
s	s	k7c7	s
různými	různý	k2eAgFnPc7d1	různá
emocemi	emoce	k1gFnPc7	emoce
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
od	od	k7c2	od
9.	[number]	k4	9.
října	říjen	k1gInSc2	říjen
2015	[number]	k4	2015
v	v	k7c6	v
testovacím	testovací	k2eAgInSc6d1	testovací
provozu	provoz	k1gInSc6	provoz
na	na	k7c6	na
irském	irský	k2eAgInSc6d1	irský
a	a	k8xC	a
španělském	španělský	k2eAgInSc6d1	španělský
Facebooku	Facebook	k1gInSc6	Facebook
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Fotky	fotka	k1gFnPc1	fotka
(	(	kIx(	(
<g/>
Photos	Photos	k1gInSc1	Photos
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Tato	tento	k3xDgFnSc1	tento
aplikace	aplikace	k1gFnSc1	aplikace
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
vyvinuta	vyvinout	k5eAaPmNgFnS	vyvinout
přímo	přímo	k6eAd1	přímo
autory	autor	k1gMnPc4	autor
systému	systém	k1gInSc2	systém
Facebook	Facebook	k1gInSc4	Facebook
<g/>
,	,	kIx,	,
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
sdílet	sdílet	k5eAaImF	sdílet
fotografie	fotografia	k1gFnPc4	fotografia
na	na	k7c6	na
sociální	sociální	k2eAgFnSc6d1	sociální
síti	síť	k1gFnSc6	síť
Facebook	Facebook	k1gInSc4	Facebook
<g/>
.	.	kIx.	.
</s>
<s>
Upload	Upload	k1gInSc1	Upload
fotografií	fotografia	k1gFnPc2	fotografia
je	být	k5eAaImIp3nS	být
řešen	řešit	k5eAaImNgInS	řešit
přes	přes	k7c4	přes
javový	javový	k2eAgInSc4d1	javový
applet	applet	k1gInSc4	applet
<g/>
,	,	kIx,	,
statickou	statický	k2eAgFnSc4d1	statická
stránku	stránka	k1gFnSc4	stránka
s	s	k7c7	s
formulářem	formulář	k1gInSc7	formulář
nebo	nebo	k8xC	nebo
pomocí	pomoc	k1gFnSc7	pomoc
externích	externí	k2eAgFnPc2d1	externí
aplikací	aplikace	k1gFnPc2	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
galerie	galerie	k1gFnSc1	galerie
fotografií	fotografia	k1gFnPc2	fotografia
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
prozatím	prozatím	k6eAd1	prozatím
maximálně	maximálně	k6eAd1	maximálně
200	[number]	k4	200
fotografií	fotografia	k1gFnPc2	fotografia
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
počet	počet	k1gInSc1	počet
galerií	galerie	k1gFnPc2	galerie
na	na	k7c4	na
jednoho	jeden	k4xCgMnSc4	jeden
uživatele	uživatel	k1gMnSc4	uživatel
k	k	k7c3	k
březnu	březen	k1gInSc3	březen
2010	[number]	k4	2010
není	být	k5eNaImIp3nS	být
omezen	omezit	k5eAaPmNgInS	omezit
<g/>
.	.	kIx.	.
</s>
<s>
Fotografie	fotografia	k1gFnPc4	fotografia
lze	lze	k6eAd1	lze
komentovat	komentovat	k5eAaBmF	komentovat
a	a	k8xC	a
lze	lze	k6eAd1	lze
na	na	k7c6	na
nich	on	k3xPp3gFnPc6	on
také	také	k9	také
jednoduše	jednoduše	k6eAd1	jednoduše
označovat	označovat	k5eAaImF	označovat
přátele	přítel	k1gMnPc4	přítel
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
technologie	technologie	k1gFnSc2	technologie
rozpoznávání	rozpoznávání	k1gNnSc2	rozpoznávání
obličejů	obličej	k1gInPc2	obličej
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
pak	pak	k6eAd1	pak
dostanou	dostat	k5eAaPmIp3nP	dostat
oznámení	oznámení	k1gNnSc4	oznámení
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
na	na	k7c6	na
dané	daný	k2eAgFnSc6d1	daná
fotce	fotka	k1gFnSc6	fotka
nacházejí	nacházet	k5eAaImIp3nP	nacházet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
vyjádření	vyjádření	k1gNnSc2	vyjádření
Facebooku	Facebook	k1gInSc2	Facebook
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
<s>
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
50	[number]	k4	50
miliard	miliarda	k4xCgFnPc2	miliarda
uživatelských	uživatelský	k2eAgFnPc2d1	Uživatelská
fotek	fotka	k1gFnPc2	fotka
(	(	kIx(	(
<g/>
červenec	červenec	k1gInSc1	červenec
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
1,5	[number]	k4	1,5
petabajtů	petabajt	k1gInPc2	petabajt
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
<g/>
5	[number]	k4	5
miliónů	milión	k4xCgInPc2	milión
gigabajtů	gigabajt	k1gInPc2	gigabajt
<g/>
)	)	kIx)	)
využitého	využitý	k2eAgNnSc2d1	využité
úložného	úložný	k2eAgNnSc2d1	úložné
místa	místo	k1gNnSc2	místo
pro	pro	k7c4	pro
fotky	fotka	k1gFnPc4	fotka
(	(	kIx(	(
<g/>
květen	květen	k1gInSc4	květen
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Každý	každý	k3xTgInSc1	každý
týden	týden	k1gInSc1	týden
přibývá	přibývat	k5eAaImIp3nS	přibývat
220	[number]	k4	220
miliónů	milión	k4xCgInPc2	milión
fotek	fotka	k1gFnPc2	fotka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
zaberou	zabrat	k5eAaPmIp3nP	zabrat
až	až	k9	až
25	[number]	k4	25
terabajtů	terabajt	k1gInPc2	terabajt
diskového	diskový	k2eAgNnSc2d1	diskové
místa	místo	k1gNnSc2	místo
(	(	kIx(	(
<g/>
květen	květen	k1gInSc1	květen
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
se	se	k3xPyFc4	se
uživatelům	uživatel	k1gMnPc3	uživatel
zobrazí	zobrazit	k5eAaPmIp3nS	zobrazit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
3	[number]	k4	3
miliardy	miliarda	k4xCgFnSc2	miliarda
fotek	fotka	k1gFnPc2	fotka
(	(	kIx(	(
<g/>
květen	květen	k1gInSc1	květen
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
špičce	špička	k1gFnSc6	špička
se	se	k3xPyFc4	se
každou	každý	k3xTgFnSc4	každý
sekundu	sekunda	k1gFnSc4	sekunda
zobrazí	zobrazit	k5eAaPmIp3nS	zobrazit
uživatelům	uživatel	k1gMnPc3	uživatel
více	hodně	k6eAd2	hodně
než	než	k8xS	než
550 000	[number]	k4	550 000
snímků	snímek	k1gInPc2	snímek
(	(	kIx(	(
<g/>
květen	květen	k1gInSc4	květen
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Dne	den	k1gInSc2	den
11.	[number]	k4	11.
dubna	duben	k1gInSc2	duben
2011	[number]	k4	2011
Facebook	Facebook	k1gInSc1	Facebook
spustil	spustit	k5eAaPmAgInS	spustit
novou	nový	k2eAgFnSc4d1	nová
funkci	funkce	k1gFnSc4	funkce
označování	označování	k1gNnSc2	označování
fotek	fotka	k1gFnPc2	fotka
<g/>
:	:	kIx,	:
lidé	člověk	k1gMnPc1	člověk
mohou	moct	k5eAaImIp3nP	moct
na	na	k7c6	na
fotce	fotka	k1gFnSc6	fotka
označit	označit	k5eAaPmF	označit
Facebook	Facebook	k1gInSc4	Facebook
stránku	stránka	k1gFnSc4	stránka
značky	značka	k1gFnSc2	značka
<g/>
,	,	kIx,	,
produktu	produkt	k1gInSc2	produkt
<g/>
,	,	kIx,	,
společnosti	společnost	k1gFnSc2	společnost
nebo	nebo	k8xC	nebo
osoby	osoba	k1gFnSc2	osoba
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
tomu	ten	k3xDgNnSc3	ten
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
označování	označování	k1gNnSc2	označování
přátel	přítel	k1gMnPc2	přítel
na	na	k7c6	na
fotkách	fotka	k1gFnPc6	fotka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2011	[number]	k4	2011
Facebook	Facebook	k1gInSc1	Facebook
oznámil	oznámit	k5eAaPmAgInS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
chystá	chystat	k5eAaImIp3nS	chystat
do	do	k7c2	do
mobilní	mobilní	k2eAgFnSc2d1	mobilní
aplikace	aplikace	k1gFnSc2	aplikace
přidat	přidat	k5eAaPmF	přidat
fotofiltry	fotofiltr	k1gInPc4	fotofiltr
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
plánu	plán	k1gInSc6	plán
zveřejnit	zveřejnit	k5eAaPmF	zveřejnit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
deset	deset	k4xCc4	deset
fotofiltrů	fotofiltr	k1gInPc2	fotofiltr
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
budou	být	k5eAaImBp3nP	být
vytvářet	vytvářet	k5eAaImF	vytvářet
snímky	snímek	k1gInPc4	snímek
podobné	podobný	k2eAgInPc4d1	podobný
zrnitým	zrnitý	k2eAgMnPc3d1	zrnitý
snímkům	snímek	k1gInPc3	snímek
na	na	k7c6	na
Instagramu	Instagram	k1gInSc6	Instagram
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Facebook	Facebook	k1gInSc1	Facebook
připravil	připravit	k5eAaPmAgInS	připravit
software	software	k1gInSc4	software
DeepFace	DeepFace	k1gFnSc2	DeepFace
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
dokáže	dokázat	k5eAaPmIp3nS	dokázat
na	na	k7c6	na
fotografiích	fotografia	k1gFnPc6	fotografia
správně	správně	k6eAd1	správně
rozpoznat	rozpoznat	k5eAaPmF	rozpoznat
tvář	tvář	k1gFnSc4	tvář
konkrétního	konkrétní	k2eAgMnSc2d1	konkrétní
člověka	člověk	k1gMnSc2	člověk
v	v	k7c4	v
97,35	[number]	k4	97,35
%	%	kIx~	%
případů	případ	k1gInPc2	případ
(	(	kIx(	(
<g/>
lidský	lidský	k2eAgInSc4d1	lidský
mozek	mozek	k1gInSc4	mozek
správně	správně	k6eAd1	správně
tvář	tvář	k1gFnSc1	tvář
rozpozná	rozpoznat	k5eAaPmIp3nS	rozpoznat
v	v	k7c6	v
98	[number]	k4	98
%	%	kIx~	%
případů	případ	k1gInPc2	případ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
nasazen	nasadit	k5eAaPmNgMnS	nasadit
kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
uživatel	uživatel	k1gMnSc1	uživatel
Facebooku	Facebook	k1gInSc2	Facebook
dostal	dostat	k5eAaPmAgMnS	dostat
zprávu	zpráva	k1gFnSc4	zpráva
<g/>
,	,	kIx,	,
když	když	k8xS	když
někdo	někdo	k3yInSc1	někdo
na	na	k7c4	na
Facebook	Facebook	k1gInSc4	Facebook
uloží	uložit	k5eAaPmIp3nS	uložit
jeho	jeho	k3xOp3gFnSc4	jeho
fotografii	fotografia	k1gFnSc4	fotografia
<g/>
,	,	kIx,	,
a	a	k8xC	a
mohl	moct	k5eAaImAgMnS	moct
se	se	k3xPyFc4	se
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
ji	on	k3xPp3gFnSc4	on
chce	chtít	k5eAaImIp3nS	chtít
smazat	smazat	k5eAaPmF	smazat
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
ale	ale	k8xC	ale
Facebook	Facebook	k1gInSc1	Facebook
bude	být	k5eAaImBp3nS	být
vědět	vědět	k5eAaImF	vědět
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
dotyčný	dotyčný	k2eAgMnSc1d1	dotyčný
pohyboval	pohybovat	k5eAaImAgMnS	pohybovat
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
ho	on	k3xPp3gMnSc4	on
někdo	někdo	k3yInSc1	někdo
vyfotí	vyfotit	k5eAaPmIp3nS	vyfotit
náhodou	náhoda	k1gFnSc7	náhoda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tuto	tento	k3xDgFnSc4	tento
funkci	funkce	k1gFnSc4	funkce
Facebook	Facebook	k1gInSc1	Facebook
oficiálně	oficiálně	k6eAd1	oficiálně
spustil	spustit	k5eAaPmAgInS	spustit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
<g/>
,	,	kIx,	,
údajně	údajně	k6eAd1	údajně
mimo	mimo	k7c4	mimo
území	území	k1gNnSc4	území
Kanady	Kanada	k1gFnSc2	Kanada
a	a	k8xC	a
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Videa	video	k1gNnSc2	video
(	(	kIx(	(
<g/>
Video	video	k1gNnSc1	video
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Tato	tento	k3xDgFnSc1	tento
aplikace	aplikace	k1gFnSc1	aplikace
je	být	k5eAaImIp3nS	být
určená	určený	k2eAgFnSc1d1	určená
pro	pro	k7c4	pro
sdílení	sdílení	k1gNnSc4	sdílení
videa	video	k1gNnSc2	video
mezi	mezi	k7c7	mezi
přáteli	přítel	k1gMnPc7	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Uživatelé	uživatel	k1gMnPc1	uživatel
mohou	moct	k5eAaImIp3nP	moct
do	do	k7c2	do
služby	služba	k1gFnSc2	služba
přidávat	přidávat	k5eAaImF	přidávat
vlastní	vlastní	k2eAgNnPc4d1	vlastní
videa	video	k1gNnPc4	video
tak	tak	k9	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	on	k3xPp3gMnPc4	on
odešlou	odeslat	k5eAaPmIp3nP	odeslat
<g/>
,	,	kIx,	,
přidají	přidat	k5eAaPmIp3nP	přidat
prostřednictvím	prostřednictví	k1gNnSc7	prostřednictví
Facebook	Facebook	k1gInSc1	Facebook
Mobile	mobile	k1gNnSc1	mobile
nebo	nebo	k8xC	nebo
video	video	k1gNnSc1	video
natočí	natočit	k5eAaBmIp3nS	natočit
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
nahrávací	nahrávací	k2eAgFnSc2d1	nahrávací
funkce	funkce	k1gFnSc2	funkce
webové	webový	k2eAgFnSc2d1	webová
kamery	kamera	k1gFnSc2	kamera
<g/>
.	.	kIx.	.
</s>
<s>
Videa	video	k1gNnPc1	video
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
jednoduše	jednoduše	k6eAd1	jednoduše
prohlížet	prohlížet	k5eAaImF	prohlížet
pomocí	pomocí	k7c2	pomocí
technologie	technologie	k1gFnSc2	technologie
Flash	Flasha	k1gFnPc2	Flasha
a	a	k8xC	a
na	na	k7c6	na
přidávaných	přidávaný	k2eAgNnPc6d1	přidávané
videích	videum	k1gNnPc6	videum
mohou	moct	k5eAaImIp3nP	moct
uživatelé	uživatel	k1gMnPc1	uživatel
navíc	navíc	k6eAd1	navíc
"	"	kIx"	"
<g/>
označit	označit	k5eAaPmF	označit
<g/>
"	"	kIx"	"
svoje	svůj	k3xOyFgMnPc4	svůj
přátele	přítel	k1gMnPc4	přítel
téměř	téměř	k6eAd1	téměř
stejným	stejný	k2eAgInSc7d1	stejný
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
když	když	k8xS	když
označují	označovat	k5eAaImIp3nP	označovat
svoje	svůj	k3xOyFgMnPc4	svůj
přátele	přítel	k1gMnPc4	přítel
na	na	k7c6	na
fotkách	fotka	k1gFnPc6	fotka
<g/>
.	.	kIx.	.
</s>
<s>
Jedinou	jediný	k2eAgFnSc7d1	jediná
výjimkou	výjimka	k1gFnSc7	výjimka
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nezobrazuje	zobrazovat	k5eNaImIp3nS	zobrazovat
umístění	umístění	k1gNnSc1	umístění
přítele	přítel	k1gMnSc2	přítel
ve	v	k7c6	v
videu	video	k1gNnSc6	video
<g/>
.	.	kIx.	.
</s>
<s>
Uživatelé	uživatel	k1gMnPc1	uživatel
si	se	k3xPyFc3	se
také	také	k9	také
mohou	moct	k5eAaImIp3nP	moct
posílat	posílat	k5eAaImF	posílat
videozprávy	videozpráva	k1gFnPc4	videozpráva
<g/>
.	.	kIx.	.
</s>
<s>
Videa	video	k1gNnPc4	video
nelze	lze	k6eNd1	lze
zařazovat	zařazovat	k5eAaImF	zařazovat
do	do	k7c2	do
kategorií	kategorie	k1gFnPc2	kategorie
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
fotek	fotka	k1gFnPc2	fotka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
řazeny	řadit	k5eAaImNgFnP	řadit
do	do	k7c2	do
alb	album	k1gNnPc2	album
<g/>
.	.	kIx.	.
</s>
<s>
Jedno	jeden	k4xCgNnSc1	jeden
video	video	k1gNnSc1	video
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
maximálně	maximálně	k6eAd1	maximálně
1024	[number]	k4	1024
MB	MB	kA	MB
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
maximálně	maximálně	k6eAd1	maximálně
45	[number]	k4	45
minut	minuta	k1gFnPc2	minuta
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
nejvíce	hodně	k6eAd3	hodně
využívá	využívat	k5eAaPmIp3nS	využívat
MP4	MP4	k1gFnSc1	MP4
<g/>
.	.	kIx.	.
</s>
<s>
Facebook	Facebook	k1gInSc1	Facebook
Video	video	k1gNnSc1	video
podporuje	podporovat	k5eAaImIp3nS	podporovat
formát	formát	k1gInSc4	formát
1080p	1080p	k1gMnSc1	1080p
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
rozlišení	rozlišení	k1gNnSc1	rozlišení
4K	[number]	k4	4K
<g/>
.	.	kIx.	.
</s>
<s>
Uživatelé	uživatel	k1gMnPc1	uživatel
si	se	k3xPyFc3	se
teď	teď	k6eAd1	teď
mohou	moct	k5eAaImIp3nP	moct
přidat	přidat	k5eAaPmF	přidat
všechna	všechen	k3xTgNnPc1	všechen
videa	video	k1gNnPc1	video
do	do	k7c2	do
jednoho	jeden	k4xCgInSc2	jeden
seznamu	seznam	k1gInSc2	seznam
stop	stop	k2eAgMnPc1d1	stop
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
na	na	k7c4	na
YouTube	YouTub	k1gInSc5	YouTub
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
možnost	možnost	k1gFnSc1	možnost
byla	být	k5eAaImAgFnS	být
spuštěna	spustit	k5eAaPmNgFnS	spustit
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2015.	[number]	k4	2015.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Události	událost	k1gFnPc1	událost
(	(	kIx(	(
<g/>
Events	Events	k1gInSc1	Events
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Facebook	Facebook	k1gInSc4	Facebook
události	událost	k1gFnSc2	událost
představují	představovat	k5eAaImIp3nP	představovat
způsob	způsob	k1gInSc4	způsob
<g/>
,	,	kIx,	,
jakým	jaký	k3yRgNnSc7	jaký
členové	člen	k1gMnPc1	člen
mohou	moct	k5eAaImIp3nP	moct
dát	dát	k5eAaPmF	dát
svým	svůj	k3xOyFgMnPc3	svůj
přátelům	přítel	k1gMnPc3	přítel
vědět	vědět	k5eAaImF	vědět
o	o	k7c6	o
událostech	událost	k1gFnPc6	událost
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
budou	být	k5eAaImBp3nP	být
konat	konat	k5eAaImF	konat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
jejich	jejich	k3xOp3gFnSc2	jejich
komunity	komunita	k1gFnSc2	komunita
<g/>
,	,	kIx,	,
a	a	k8xC	a
pořádat	pořádat	k5eAaImF	pořádat
společenská	společenský	k2eAgNnPc4d1	společenské
setkání	setkání	k1gNnPc4	setkání
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
aplikace	aplikace	k1gFnSc1	aplikace
je	být	k5eAaImIp3nS	být
určena	určit	k5eAaPmNgFnS	určit
pro	pro	k7c4	pro
plánování	plánování	k1gNnSc4	plánování
událostí	událost	k1gFnPc2	událost
nebo	nebo	k8xC	nebo
jakýchkoliv	jakýkoliv	k3yIgFnPc2	jakýkoliv
akcí	akce	k1gFnPc2	akce
<g/>
.	.	kIx.	.
</s>
<s>
Umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
nastavit	nastavit	k5eAaPmF	nastavit
mnoho	mnoho	k4c4	mnoho
informací	informace	k1gFnPc2	informace
o	o	k7c4	o
akci	akce	k1gFnSc4	akce
<g/>
,	,	kIx,	,
zvát	zvát	k5eAaImF	zvát
přátele	přítel	k1gMnPc4	přítel
na	na	k7c4	na
akce	akce	k1gFnPc4	akce
nebo	nebo	k8xC	nebo
akci	akce	k1gFnSc4	akce
zveřejnit	zveřejnit	k5eAaPmF	zveřejnit
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
akci	akce	k1gFnSc3	akce
lze	lze	k6eAd1	lze
přidávat	přidávat	k5eAaImF	přidávat
další	další	k2eAgInSc4d1	další
multimediální	multimediální	k2eAgInSc4d1	multimediální
obsah	obsah	k1gInSc4	obsah
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Události	událost	k1gFnPc1	událost
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
název	název	k1gInSc4	název
<g/>
,	,	kIx,	,
síť	síť	k1gFnSc4	síť
<g/>
,	,	kIx,	,
jméno	jméno	k1gNnSc4	jméno
pořadatele	pořadatel	k1gMnSc2	pořadatel
<g/>
,	,	kIx,	,
typ	typ	k1gInSc4	typ
události	událost	k1gFnSc2	událost
<g/>
,	,	kIx,	,
čas	čas	k1gInSc4	čas
zahájení	zahájení	k1gNnSc4	zahájení
<g/>
,	,	kIx,	,
místo	místo	k1gNnSc4	místo
konání	konání	k1gNnSc2	konání
a	a	k8xC	a
seznam	seznam	k1gInSc1	seznam
přátel	přítel	k1gMnPc2	přítel
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
byli	být	k5eAaImAgMnP	být
pozváni	pozvat	k5eAaPmNgMnP	pozvat
<g/>
.	.	kIx.	.
</s>
<s>
Události	událost	k1gFnPc1	událost
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
veřejné	veřejný	k2eAgFnPc1d1	veřejná
nebo	nebo	k8xC	nebo
soukromé	soukromý	k2eAgFnPc1d1	soukromá
<g/>
.	.	kIx.	.
</s>
<s>
Soukromé	soukromý	k2eAgFnPc1d1	soukromá
události	událost	k1gFnPc1	událost
nelze	lze	k6eNd1	lze
najít	najít	k5eAaPmF	najít
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
vyhledávání	vyhledávání	k1gNnSc2	vyhledávání
a	a	k8xC	a
přístup	přístup	k1gInSc1	přístup
k	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
je	být	k5eAaImIp3nS	být
jenom	jenom	k9	jenom
na	na	k7c4	na
pozvání	pozvání	k1gNnSc4	pozvání
<g/>
.	.	kIx.	.
</s>
<s>
Osoby	osoba	k1gFnPc1	osoba
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
nebyly	být	k5eNaImAgFnP	být
pozvány	pozvat	k5eAaPmNgFnP	pozvat
<g/>
,	,	kIx,	,
neuvidí	uvidět	k5eNaPmIp3nS	uvidět
popis	popis	k1gInSc4	popis
soukromé	soukromý	k2eAgFnSc2d1	soukromá
události	událost	k1gFnSc2	událost
<g/>
,	,	kIx,	,
zeď	zeď	k1gFnSc4	zeď
ani	ani	k8xC	ani
fotky	fotka	k1gFnPc4	fotka
<g/>
.	.	kIx.	.
</s>
<s>
Neuvidí	uvidět	k5eNaPmIp3nS	uvidět
ani	ani	k8xC	ani
příspěvky	příspěvek	k1gInPc4	příspěvek
týkající	týkající	k2eAgInPc4d1	týkající
se	se	k3xPyFc4	se
dané	daný	k2eAgFnPc1d1	daná
události	událost	k1gFnPc1	událost
v	v	k7c6	v
kanálu	kanál	k1gInSc6	kanál
vybraných	vybraný	k2eAgInPc2d1	vybraný
příspěvků	příspěvek	k1gInPc2	příspěvek
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vytváření	vytváření	k1gNnSc6	vytváření
události	událost	k1gFnSc2	událost
může	moct	k5eAaImIp3nS	moct
uživatel	uživatel	k1gMnSc1	uživatel
povolit	povolit	k5eAaPmF	povolit
přátelům	přítel	k1gMnPc3	přítel
nahrávat	nahrávat	k5eAaImF	nahrávat
fotky	fotka	k1gFnPc4	fotka
a	a	k8xC	a
videa	video	k1gNnSc2	video
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
povšimnutí	povšimnutí	k1gNnSc2	povšimnutí
stojí	stát	k5eAaImIp3nS	stát
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
událostí	událost	k1gFnPc2	událost
v	v	k7c6	v
reálném	reálný	k2eAgInSc6d1	reálný
světě	svět	k1gInSc6	svět
jsou	být	k5eAaImIp3nP	být
všechny	všechen	k3xTgFnPc1	všechen
události	událost	k1gFnPc1	událost
považovány	považován	k2eAgFnPc1d1	považována
za	za	k7c4	za
samostatné	samostatný	k2eAgFnPc4d1	samostatná
entity	entita	k1gFnPc4	entita
(	(	kIx(	(
<g/>
když	když	k8xS	když
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
v	v	k7c6	v
reálném	reálný	k2eAgInSc6d1	reálný
světě	svět	k1gInSc6	svět
události	událost	k1gFnSc2	událost
překrývají	překrývat	k5eAaImIp3nP	překrývat
<g/>
,	,	kIx,	,
nelze	lze	k6eNd1	lze
se	se	k3xPyFc4	se
zúčastnit	zúčastnit	k5eAaPmF	zúčastnit
jedné	jeden	k4xCgFnSc2	jeden
a	a	k8xC	a
současně	současně	k6eAd1	současně
jiné	jiný	k2eAgInPc1d1	jiný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2011	[number]	k4	2011
Facebook	Facebook	k1gInSc1	Facebook
začal	začít	k5eAaPmAgInS	začít
používat	používat	k5eAaImF	používat
mikroformát	mikroformát	k5eAaPmNgInS	mikroformát
hCalendar	hCalendar	k1gInSc1	hCalendar
pro	pro	k7c4	pro
události	událost	k1gFnPc4	událost
a	a	k8xC	a
mikroformát	mikroformát	k5eAaPmF	mikroformát
hCard	hCard	k1gInSc4	hCard
pro	pro	k7c4	pro
místa	místo	k1gNnPc4	místo
konání	konání	k1gNnSc2	konání
událostí	událost	k1gFnPc2	událost
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
mikroformáty	mikroformát	k1gInPc1	mikroformát
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
uživatelům	uživatel	k1gMnPc3	uživatel
extrahovat	extrahovat	k5eAaBmF	extrahovat
podrobnosti	podrobnost	k1gFnPc4	podrobnost
o	o	k7c4	o
události	událost	k1gFnPc4	událost
do	do	k7c2	do
vlastního	vlastní	k2eAgInSc2d1	vlastní
kalendáře	kalendář	k1gInSc2	kalendář
nebo	nebo	k8xC	nebo
aplikací	aplikace	k1gFnPc2	aplikace
pro	pro	k7c4	pro
vytváření	vytváření	k1gNnSc4	vytváření
map	mapa	k1gFnPc2	mapa
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
třetí	třetí	k4xOgFnPc4	třetí
strany	strana	k1gFnPc4	strana
je	být	k5eAaImIp3nS	být
snadnější	snadný	k2eAgMnSc1d2	snazší
exportovat	exportovat	k5eAaBmF	exportovat
události	událost	k1gFnPc4	událost
z	z	k7c2	z
Facebook	Facebook	k1gInSc4	Facebook
stránek	stránka	k1gFnPc2	stránka
do	do	k7c2	do
formátu	formát	k1gInSc2	formát
iCalendar	iCalendara	k1gFnPc2	iCalendara
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Zeď	zeď	k1gFnSc1	zeď
(	(	kIx(	(
<g/>
Wall	Wall	k1gInSc1	Wall
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Každý	každý	k3xTgMnSc1	každý
uživatel	uživatel	k1gMnSc1	uživatel
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
profilu	profil	k1gInSc6	profil
The	The	k1gMnSc1	The
Wall	Wall	k1gMnSc1	Wall
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yIgFnSc4	který
mu	on	k3xPp3gMnSc3	on
ostatní	ostatní	k2eAgMnPc1d1	ostatní
uživatelé	uživatel	k1gMnPc1	uživatel
mohou	moct	k5eAaImIp3nP	moct
psát	psát	k5eAaImF	psát
vzkazy	vzkaz	k1gInPc4	vzkaz
a	a	k8xC	a
zobrazují	zobrazovat	k5eAaImIp3nP	zobrazovat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
všechny	všechen	k3xTgInPc4	všechen
děje	děj	k1gInPc4	děj
v	v	k7c6	v
sociální	sociální	k2eAgFnSc6d1	sociální
síti	síť	k1gFnSc6	síť
Facebook	Facebook	k1gInSc4	Facebook
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Wall	Wall	k1gInSc4	Wall
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
vkládat	vkládat	k5eAaImF	vkládat
i	i	k9	i
další	další	k2eAgInSc4d1	další
multimediální	multimediální	k2eAgInSc4d1	multimediální
obsah	obsah	k1gInSc4	obsah
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
března	březen	k1gInSc2	březen
2012	[number]	k4	2012
je	být	k5eAaImIp3nS	být
forma	forma	k1gFnSc1	forma
ZEĎ	zeď	k1gFnSc4	zeď
postupně	postupně	k6eAd1	postupně
všem	všecek	k3xTgMnPc3	všecek
uživatelům	uživatel	k1gMnPc3	uživatel
změněná	změněný	k2eAgFnSc1d1	změněná
na	na	k7c4	na
styl	styl	k1gInSc4	styl
Timeline	Timelin	k1gInSc5	Timelin
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
styl	styl	k1gInSc1	styl
lépe	dobře	k6eAd2	dobře
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
vaší	váš	k3xOp2gFnSc3	váš
historii	historie	k1gFnSc3	historie
a	a	k8xC	a
důležité	důležitý	k2eAgInPc1d1	důležitý
okamžiky	okamžik	k1gInPc1	okamžik
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Timeline	Timelin	k1gInSc5	Timelin
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
v	v	k7c6	v
záhlaví	záhlaví	k1gNnSc6	záhlaví
velkou	velký	k2eAgFnSc4d1	velká
fotografii	fotografia	k1gFnSc4	fotografia
o	o	k7c6	o
rozměrech	rozměr	k1gInPc6	rozměr
850	[number]	k4	850
<g/>
×	×	k?	×
<g/>
315	[number]	k4	315
bodů	bod	k1gInPc2	bod
a	a	k8xC	a
při	při	k7c6	při
nahrávání	nahrávání	k1gNnSc6	nahrávání
obrázku	obrázek	k1gInSc2	obrázek
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
nahrát	nahrát	k5eAaPmF	nahrát
obrázek	obrázek	k1gInSc4	obrázek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
má	mít	k5eAaImIp3nS	mít
na	na	k7c4	na
výšku	výška	k1gFnSc4	výška
méně	málo	k6eAd2	málo
než	než	k8xS	než
399	[number]	k4	399
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Fotografie	fotografia	k1gFnPc1	fotografia
v	v	k7c6	v
záhlaví	záhlaví	k1gNnSc6	záhlaví
je	být	k5eAaImIp3nS	být
veřejná	veřejný	k2eAgFnSc1d1	veřejná
a	a	k8xC	a
kdokoliv	kdokoliv	k3yInSc1	kdokoliv
si	se	k3xPyFc3	se
jí	on	k3xPp3gFnSc2	on
může	moct	k5eAaImIp3nS	moct
stáhnout	stáhnout	k5eAaPmF	stáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
levé	levý	k2eAgFnSc2d1	levá
spodní	spodní	k2eAgFnSc2d1	spodní
části	část	k1gFnSc2	část
obrázku	obrázek	k1gInSc2	obrázek
v	v	k7c6	v
záhlaví	záhlaví	k1gNnSc6	záhlaví
Facebook	Facebook	k1gInSc1	Facebook
umisťuje	umisťovat	k5eAaImIp3nS	umisťovat
profilovou	profilový	k2eAgFnSc4d1	profilová
fotografii	fotografia	k1gFnSc4	fotografia
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
s	s	k7c7	s
rozměry	rozměr	k1gInPc7	rozměr
125	[number]	k4	125
<g/>
×	×	k?	×
<g/>
125	[number]	k4	125
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Fotografii	fotografia	k1gFnSc4	fotografia
do	do	k7c2	do
záhlaví	záhlaví	k1gNnSc2	záhlaví
není	být	k5eNaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
nahrávat	nahrávat	k5eAaImF	nahrávat
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vhodném	vhodný	k2eAgInSc6d1	vhodný
návrhu	návrh	k1gInSc6	návrh
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
pomocí	pomocí	k7c2	pomocí
kombinace	kombinace	k1gFnSc2	kombinace
fotografie	fotografia	k1gFnSc2	fotografia
v	v	k7c6	v
záhlaví	záhlaví	k1gNnSc6	záhlaví
a	a	k8xC	a
profilové	profilový	k2eAgFnPc4d1	profilová
fotografie	fotografia	k1gFnPc4	fotografia
obě	dva	k4xCgFnPc4	dva
sladit	sladit	k5eAaPmF	sladit
v	v	k7c4	v
jeden	jeden	k4xCgInSc4	jeden
celek	celek	k1gInSc4	celek
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
vidět	vidět	k5eAaImF	vidět
např.	např.	kA	např.
u	u	k7c2	u
uživatelů	uživatel	k1gMnPc2	uživatel
Kay	Kay	k1gFnSc2	Kay
Int	Int	k1gMnSc1	Int
Veen	Veen	k1gMnSc1	Veen
<g/>
,	,	kIx,	,
Eduardo	Eduardo	k1gNnSc1	Eduardo
Calvo	Calvo	k1gNnSc1	Calvo
a	a	k8xC	a
Giuseppe	Giusepp	k1gMnSc5	Giusepp
Draicchio	Draicchia	k1gMnSc5	Draicchia
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c4	na
timeline	timelin	k1gInSc5	timelin
bylo	být	k5eAaImAgNnS	být
zavedené	zavedený	k2eAgNnSc4d1	zavedené
filtrování	filtrování	k1gNnSc4	filtrování
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
uživatel	uživatel	k1gMnSc1	uživatel
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
timeline	timelin	k1gInSc5	timelin
nemá	mít	k5eNaImIp3nS	mít
přístupné	přístupný	k2eAgInPc1d1	přístupný
všechny	všechen	k3xTgInPc1	všechen
příspěvky	příspěvek	k1gInPc1	příspěvek
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
timeline	timelin	k1gInSc5	timelin
se	se	k3xPyFc4	se
po	po	k7c6	po
zavedení	zavedení	k1gNnSc6	zavedení
tohoto	tento	k3xDgNnSc2	tento
filtrování	filtrování	k1gNnSc2	filtrování
nejprve	nejprve	k6eAd1	nejprve
zobrazují	zobrazovat	k5eAaImIp3nP	zobrazovat
vybrané	vybraný	k2eAgInPc4d1	vybraný
příspěvky	příspěvek	k1gInPc4	příspěvek
a	a	k8xC	a
teprve	teprve	k6eAd1	teprve
po	po	k7c6	po
té	ten	k3xDgFnSc6	ten
se	se	k3xPyFc4	se
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
časová	časový	k2eAgFnSc1d1	časová
posloupnost	posloupnost	k1gFnSc1	posloupnost
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
čím	čí	k3xOyRgNnSc7	čí
starší	starý	k2eAgInPc4d2	starší
příspěvky	příspěvek	k1gInPc4	příspěvek
si	se	k3xPyFc3	se
chce	chtít	k5eAaImIp3nS	chtít
uživatel	uživatel	k1gMnSc1	uživatel
zobrazit	zobrazit	k5eAaPmF	zobrazit
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
méně	málo	k6eAd2	málo
příspěvků	příspěvek	k1gInPc2	příspěvek
z	z	k7c2	z
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
vidí	vidět	k5eAaImIp3nS	vidět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Dárky	dárek	k1gInPc4	dárek
(	(	kIx(	(
<g/>
Gifts	Gifts	k1gInSc4	Gifts
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Uživatelé	uživatel	k1gMnPc1	uživatel
si	se	k3xPyFc3	se
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
mohou	moct	k5eAaImIp3nP	moct
posílat	posílat	k5eAaImF	posílat
virtuální	virtuální	k2eAgInPc4d1	virtuální
dárky	dárek	k1gInPc4	dárek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
představují	představovat	k5eAaImIp3nP	představovat
vektorové	vektorový	k2eAgInPc1d1	vektorový
obrázky	obrázek	k1gInPc1	obrázek
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
dárek	dárek	k1gInSc1	dárek
stojí	stát	k5eAaImIp3nS	stát
1	[number]	k4	1
dolar	dolar	k1gInSc1	dolar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Bazar	bazar	k1gInSc1	bazar
(	(	kIx(	(
<g/>
Marketplace	Marketplace	k1gFnSc1	Marketplace
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2007	[number]	k4	2007
Facebook	Facebook	k1gInSc1	Facebook
představil	představit	k5eAaPmAgInS	představit
službu	služba	k1gFnSc4	služba
Facebook	Facebook	k1gInSc1	Facebook
Marketplace	Marketplace	k1gFnPc1	Marketplace
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
uživatelům	uživatel	k1gMnPc3	uživatel
umožňovala	umožňovat	k5eAaImAgFnS	umožňovat
zveřejňovat	zveřejňovat	k5eAaImF	zveřejňovat
bezplatné	bezplatný	k2eAgFnPc4d1	bezplatná
tříděné	tříděný	k2eAgFnPc4d1	tříděná
reklamy	reklama	k1gFnPc4	reklama
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
kategoriích	kategorie	k1gFnPc6	kategorie
<g/>
:	:	kIx,	:
Na	na	k7c4	na
prodej	prodej	k1gInSc4	prodej
<g/>
,	,	kIx,	,
Bydlení	bydlení	k1gNnSc1	bydlení
<g/>
,	,	kIx,	,
Práce	práce	k1gFnPc1	práce
a	a	k8xC	a
Jiné	jiný	k2eAgFnPc1d1	jiná
<g/>
.	.	kIx.	.
</s>
<s>
Reklamy	reklama	k1gFnPc1	reklama
lze	lze	k6eAd1	lze
zveřejňovat	zveřejňovat	k5eAaImF	zveřejňovat
v	v	k7c6	v
dostupném	dostupný	k2eAgInSc6d1	dostupný
nebo	nebo	k8xC	nebo
požadovaném	požadovaný	k2eAgInSc6d1	požadovaný
formátu	formát	k1gInSc6	formát
<g/>
.	.	kIx.	.
</s>
<s>
Tržiště	tržiště	k1gNnPc1	tržiště
mají	mít	k5eAaImIp3nP	mít
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
všichni	všechen	k3xTgMnPc1	všechen
uživatelé	uživatel	k1gMnPc1	uživatel
Facebooku	Facebook	k1gInSc2	Facebook
a	a	k8xC	a
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
zdarma	zdarma	k6eAd1	zdarma
<g/>
.	.	kIx.	.
</s>
<s>
Bazar	bazar	k1gInSc1	bazar
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
vkládání	vkládání	k1gNnSc3	vkládání
inzerátů	inzerát	k1gInPc2	inzerát
<g/>
,	,	kIx,	,
výměně	výměna	k1gFnSc3	výměna
a	a	k8xC	a
prodeji	prodej	k1gInSc3	prodej
různých	různý	k2eAgFnPc2d1	různá
věcí	věc	k1gFnPc2	věc
<g/>
,	,	kIx,	,
tato	tento	k3xDgFnSc1	tento
služba	služba	k1gFnSc1	služba
je	být	k5eAaImIp3nS	být
mezi	mezi	k7c7	mezi
uživateli	uživatel	k1gMnPc7	uživatel
velmi	velmi	k6eAd1	velmi
rozšířená	rozšířený	k2eAgFnSc1d1	rozšířená
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
Facebook	Facebook	k1gInSc1	Facebook
převedl	převést	k5eAaPmAgInS	převést
vlastnictví	vlastnictví	k1gNnSc4	vlastnictví
služby	služba	k1gFnSc2	služba
Marketplace	Marketplace	k1gFnSc2	Marketplace
na	na	k7c4	na
společnost	společnost	k1gFnSc4	společnost
Oodle	Oodle	k1gFnSc2	Oodle
<g/>
.	.	kIx.	.
</s>
<s>
Provoz	provoz	k1gInSc1	provoz
původní	původní	k2eAgFnSc2d1	původní
služby	služba	k1gFnSc2	služba
Marketplace	Marketplace	k1gFnSc2	Marketplace
byl	být	k5eAaImAgInS	být
ukončen	ukončit	k5eAaPmNgInS	ukončit
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2014.	[number]	k4	2014.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Šťouchnutí	šťouchnutý	k2eAgMnPc1d1	šťouchnutý
(	(	kIx(	(
<g/>
Poke	Poke	k1gNnSc7	Poke
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Některý	některý	k3yIgMnSc1	některý
uživatel	uživatel	k1gMnSc1	uživatel
chce	chtít	k5eAaImIp3nS	chtít
čas	čas	k1gInSc4	čas
od	od	k7c2	od
času	čas	k1gInSc2	čas
pošťouchnout	pošťouchnout	k5eAaPmF	pošťouchnout
a	a	k8xC	a
navázat	navázat	k5eAaPmF	navázat
kontakt	kontakt	k1gInSc4	kontakt
nebo	nebo	k8xC	nebo
touto	tento	k3xDgFnSc7	tento
metodou	metoda	k1gFnSc7	metoda
jen	jen	k9	jen
tak	tak	k6eAd1	tak
upozornit	upozornit	k5eAaPmF	upozornit
své	svůj	k3xOyFgMnPc4	svůj
přátele	přítel	k1gMnPc4	přítel
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Šťouchnout	šťouchnout	k5eAaPmF	šťouchnout
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
mezi	mezi	k7c7	mezi
uživateli	uživatel	k1gMnPc7	uživatel
velmi	velmi	k6eAd1	velmi
oblíbená	oblíbený	k2eAgFnSc1d1	oblíbená
funkce	funkce	k1gFnSc1	funkce
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
její	její	k3xOp3gInSc1	její
princip	princip	k1gInSc1	princip
je	být	k5eAaImIp3nS	být
jednoduchý	jednoduchý	k2eAgInSc1d1	jednoduchý
-	-	kIx~	-
jen	jen	k9	jen
oznamuje	oznamovat	k5eAaImIp3nS	oznamovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
vás	vy	k3xPp2nPc2	vy
váš	váš	k3xOp2gInSc4	váš
přítel	přítel	k1gMnSc1	přítel
šťouchl	šťouchnout	k5eAaPmAgMnS	šťouchnout
a	a	k8xC	a
vy	vy	k3xPp2nPc1	vy
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
můžete	moct	k5eAaImIp2nP	moct
reagovat	reagovat	k5eAaBmF	reagovat
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Šťouchnutí	šťouchnutý	k2eAgMnPc1d1	šťouchnutý
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
interpretuje	interpretovat	k5eAaBmIp3nS	interpretovat
různě	různě	k6eAd1	různě
<g/>
,	,	kIx,	,
především	především	k9	především
jako	jako	k9	jako
snaha	snaha	k1gFnSc1	snaha
o	o	k7c4	o
upoutání	upoutání	k1gNnSc4	upoutání
pozornosti	pozornost	k1gFnSc2	pozornost
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
další	další	k2eAgFnSc1d1	další
varianta	varianta	k1gFnSc1	varianta
je	být	k5eAaImIp3nS	být
výzva	výzva	k1gFnSc1	výzva
k	k	k7c3	k
pohlavnímu	pohlavní	k2eAgInSc3d1	pohlavní
styku	styk	k1gInSc3	styk
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
spousta	spousta	k1gFnSc1	spousta
podobných	podobný	k2eAgFnPc2d1	podobná
aplikací	aplikace	k1gFnPc2	aplikace
jako	jako	k8xS	jako
např.	např.	kA	např.
X	X	kA	X
Me	Me	k1gMnSc1	Me
dovolující	dovolující	k2eAgFnSc4d1	dovolující
libovolnou	libovolný	k2eAgFnSc4d1	libovolná
akci	akce	k1gFnSc4	akce
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
Obejmutí	Obejmutí	k1gNnSc1	Obejmutí
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Hug	Hug	k1gFnSc1	Hug
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Polibek	polibek	k1gInSc1	polibek
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Kiss	Kiss	k1gInSc1	Kiss
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
Zlechtání	zlechtání	k1gNnSc1	zlechtání
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Tickle	Tickl	k1gInSc5	Tickl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
podobné	podobný	k2eAgFnPc1d1	podobná
aplikace	aplikace	k1gFnPc1	aplikace
dovolují	dovolovat	k5eAaImIp3nP	dovolovat
uživatelům	uživatel	k1gMnPc3	uživatel
si	se	k3xPyFc3	se
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
posílat	posílat	k5eAaImF	posílat
různé	různý	k2eAgInPc1d1	různý
předměty	předmět	k1gInPc1	předmět
jako	jako	k8xS	jako
různé	různý	k2eAgInPc1d1	různý
druhy	druh	k1gInPc1	druh
alkoholu	alkohol	k1gInSc2	alkohol
(	(	kIx(	(
<g/>
Booze	Booze	k1gFnSc1	Booze
Mail	mail	k1gInSc1	mail
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rybičky	rybička	k1gFnPc4	rybička
v	v	k7c6	v
akváriu	akvárium	k1gNnSc6	akvárium
(	(	kIx(	(
<g/>
My	my	k3xPp1nPc1	my
Aquarium	Aquarium	k1gNnSc4	Aquarium
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
třeba	třeba	k6eAd1	třeba
různé	různý	k2eAgInPc1d1	různý
druhy	druh	k1gInPc1	druh
mimozemšťanů	mimozemšťan	k1gMnPc2	mimozemšťan
(	(	kIx(	(
<g/>
My	my	k3xPp1nPc1	my
Solar	Solar	k1gInSc1	Solar
System	Systo	k1gNnSc7	Systo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Status	status	k1gInSc1	status
bar	bar	k1gInSc1	bar
===	===	k?	===
</s>
</p>
<p>
<s>
Statusový	Statusový	k2eAgInSc1d1	Statusový
řádek	řádek	k1gInSc1	řádek
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
uživateli	uživatel	k1gMnSc3	uživatel
vystavovat	vystavovat	k5eAaImF	vystavovat
tzv.	tzv.	kA	tzv.
statusy	status	k1gInPc1	status
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
jsou	být	k5eAaImIp3nP	být
krátké	krátký	k2eAgInPc1d1	krátký
texty	text	k1gInPc1	text
vyjadřující	vyjadřující	k2eAgInPc1d1	vyjadřující
zpravidla	zpravidla	k6eAd1	zpravidla
nálady	nálada	k1gFnPc4	nálada
<g/>
,	,	kIx,	,
názory	názor	k1gInPc4	názor
nebo	nebo	k8xC	nebo
stav	stav	k1gInSc4	stav
uživatele	uživatel	k1gMnSc2	uživatel
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
jeho	jeho	k3xOp3gInPc4	jeho
oblíbené	oblíbený	k2eAgInPc4d1	oblíbený
vtipy	vtip	k1gInPc4	vtip
<g/>
,	,	kIx,	,
odkazy	odkaz	k1gInPc4	odkaz
či	či	k8xC	či
hlášky	hláška	k1gFnPc4	hláška
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Facebook	Facebook	k1gInSc1	Facebook
e-mail	eail	k1gInSc1	e-mail
===	===	k?	===
</s>
</p>
<p>
<s>
Záměrem	záměr	k1gInSc7	záměr
facebooku	facebook	k1gInSc2	facebook
bylo	být	k5eAaImAgNnS	být
poskytnout	poskytnout	k5eAaPmF	poskytnout
vlastní	vlastní	k2eAgFnSc4d1	vlastní
elektronickou	elektronický	k2eAgFnSc4d1	elektronická
poštu	pošta	k1gFnSc4	pošta
na	na	k7c6	na
doméně	doména	k1gFnSc6	doména
@	@	kIx~	@
<g/>
facebook.com	facebook.com	k1gInSc1	facebook.com
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
tato	tento	k3xDgFnSc1	tento
aktivita	aktivita	k1gFnSc1	aktivita
nevyšla	vyjít	k5eNaPmAgFnS	vyjít
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
doporučeno	doporučit	k5eAaPmNgNnS	doporučit
jako	jako	k9	jako
prevence	prevence	k1gFnSc1	prevence
před	před	k7c7	před
možným	možný	k2eAgNnSc7d1	možné
spamem	spamo	k1gNnSc7	spamo
(	(	kIx(	(
<g/>
e-mailovou	eailový	k2eAgFnSc4d1	e-mailová
adresu	adresa	k1gFnSc4	adresa
lze	lze	k6eAd1	lze
snadno	snadno	k6eAd1	snadno
odhadnout	odhadnout	k5eAaPmF	odhadnout
<g/>
)	)	kIx)	)
službu	služba	k1gFnSc4	služba
Používat	používat	k5eAaImF	používat
Facebook	Facebook	k1gInSc4	Facebook
e-mail	eail	k1gInSc4	e-mail
deaktivovat	deaktivovat	k5eAaImF	deaktivovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Messenger	Messengero	k1gNnPc2	Messengero
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Messenger	Messenger	k1gInSc1	Messenger
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
jako	jako	k8xS	jako
samostatná	samostatný	k2eAgFnSc1d1	samostatná
aplikace	aplikace	k1gFnSc1	aplikace
oddělením	oddělení	k1gNnSc7	oddělení
chatu	chata	k1gFnSc4	chata
z	z	k7c2	z
vlastní	vlastní	k2eAgFnSc2d1	vlastní
facebookové	facebookový	k2eAgFnSc2d1	facebooková
aplikace	aplikace	k1gFnSc2	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2015	[number]	k4	2015
byla	být	k5eAaImAgFnS	být
spuštěna	spustit	k5eAaPmNgFnS	spustit
webová	webový	k2eAgFnSc1d1	webová
verze	verze	k1gFnSc1	verze
Messengeru	Messenger	k1gInSc2	Messenger
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
Služba	služba	k1gFnSc1	služba
Facebook	Facebook	k1gInSc4	Facebook
Poznámky	poznámka	k1gFnSc2	poznámka
byla	být	k5eAaImAgFnS	být
spuštěna	spustit	k5eAaPmNgFnS	spustit
22.	[number]	k4	22.
srpna	srpen	k1gInSc2	srpen
2006	[number]	k4	2006
jako	jako	k8xC	jako
blogovací	blogovací	k2eAgFnSc1d1	blogovací
funkce	funkce	k1gFnSc1	funkce
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
umožňovala	umožňovat	k5eAaImAgFnS	umožňovat
použití	použití	k1gNnSc4	použití
tagů	tag	k1gInPc2	tag
a	a	k8xC	a
integrovaných	integrovaný	k2eAgInPc2d1	integrovaný
obrázků	obrázek	k1gInPc2	obrázek
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
uživatelé	uživatel	k1gMnPc1	uživatel
dostali	dostat	k5eAaPmAgMnP	dostat
možnost	možnost	k1gFnSc4	možnost
importovat	importovat	k5eAaBmF	importovat
blogy	blog	k1gInPc4	blog
ze	z	k7c2	z
služby	služba	k1gFnSc2	služba
Xanga	Xanga	k1gFnSc1	Xanga
<g/>
,	,	kIx,	,
LiveJournal	LiveJournal	k1gFnSc1	LiveJournal
<g/>
,	,	kIx,	,
Blogger	Bloggra	k1gFnPc2	Bloggra
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
blogovacích	blogovací	k2eAgFnPc2d1	blogovací
služeb	služba	k1gFnPc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
Nejnovější	nový	k2eAgNnSc1d3	nejnovější
využití	využití	k1gNnSc1	využití
Poznámek	poznámka	k1gFnPc2	poznámka
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
internetový	internetový	k2eAgInSc1d1	internetový
mém	můj	k1gMnSc6	můj
"	"	kIx"	"
<g/>
25	[number]	k4	25
náhodných	náhodný	k2eAgFnPc2d1	náhodná
věcí	věc	k1gFnPc2	věc
o	o	k7c6	o
mně	já	k3xPp1nSc6	já
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
uživatel	uživatel	k1gMnSc1	uživatel
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
napíše	napsat	k5eAaPmIp3nS	napsat
25	[number]	k4	25
věcí	věc	k1gFnPc2	věc
<g/>
,	,	kIx,	,
o	o	k7c6	o
nichž	jenž	k3xRgFnPc6	jenž
jeho	jeho	k3xOp3gFnPc6	jeho
přátelé	přítel	k1gMnPc1	přítel
neměli	mít	k5eNaImAgMnP	mít
dosud	dosud	k6eAd1	dosud
tušení	tušený	k2eAgMnPc1d1	tušený
<g/>
,	,	kIx,	,
a	a	k8xC	a
použije	použít	k5eAaPmIp3nS	použít
tagovací	tagovací	k2eAgFnSc4d1	tagovací
funkci	funkce	k1gFnSc4	funkce
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
požádá	požádat	k5eAaPmIp3nS	požádat
25	[number]	k4	25
přátel	přítel	k1gMnPc2	přítel
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
udělali	udělat	k5eAaPmAgMnP	udělat
totéž	týž	k3xTgNnSc4	týž
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
prvního	první	k4xOgInSc2	první
únorového	únorový	k2eAgInSc2d1	únorový
týdne	týden	k1gInSc2	týden
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
si	se	k3xPyFc3	se
na	na	k7c4	na
Facebook	Facebook	k1gInSc4	Facebook
profil	profil	k1gInSc4	profil
napsalo	napsat	k5eAaBmAgNnS	napsat
poznámku	poznámka	k1gFnSc4	poznámka
"	"	kIx"	"
<g/>
25	[number]	k4	25
náhodných	náhodný	k2eAgFnPc2d1	náhodná
věcí	věc	k1gFnPc2	věc
<g/>
"	"	kIx"	"
téměř	téměř	k6eAd1	téměř
5	[number]	k4	5
miliónů	milión	k4xCgInPc2	milión
uživatelů	uživatel	k1gMnPc2	uživatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Místa	místo	k1gNnSc2	místo
===	===	k?	===
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
18.	[number]	k4	18.
srpna	srpen	k1gInSc2	srpen
2010	[number]	k4	2010
Facebook	Facebook	k1gInSc1	Facebook
oznámil	oznámit	k5eAaPmAgInS	oznámit
zprovoznění	zprovoznění	k1gNnSc4	zprovoznění
nové	nový	k2eAgFnSc2d1	nová
funkce	funkce	k1gFnSc2	funkce
Místa	místo	k1gNnSc2	místo
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
uživatelům	uživatel	k1gMnPc3	uživatel
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
mobilního	mobilní	k2eAgNnSc2d1	mobilní
zařízení	zařízení	k1gNnSc2	zařízení
"	"	kIx"	"
<g/>
oznámit	oznámit	k5eAaPmF	oznámit
svoji	svůj	k3xOyFgFnSc4	svůj
polohu	poloha	k1gFnSc4	poloha
<g/>
"	"	kIx"	"
na	na	k7c6	na
Facebooku	Facebook	k1gInSc6	Facebook
a	a	k8xC	a
dát	dát	k5eAaPmF	dát
tak	tak	k9	tak
svým	svůj	k3xOyFgMnPc3	svůj
přátelům	přítel	k1gMnPc3	přítel
vědět	vědět	k5eAaImF	vědět
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
právě	právě	k6eAd1	právě
nacházejí	nacházet	k5eAaImIp3nP	nacházet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2010	[number]	k4	2010
Facebook	Facebook	k1gInSc1	Facebook
představil	představit	k5eAaPmAgInS	představit
"	"	kIx"	"
<g/>
Výhodné	výhodný	k2eAgFnPc4d1	výhodná
nabídky	nabídka	k1gFnPc4	nabídka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
součást	součást	k1gFnSc1	součást
služby	služba	k1gFnSc2	služba
Místa	místo	k1gNnSc2	místo
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
uživatelům	uživatel	k1gMnPc3	uživatel
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
z	z	k7c2	z
aplikace	aplikace	k1gFnSc2	aplikace
v	v	k7c6	v
mobilním	mobilní	k2eAgNnSc6d1	mobilní
zařízení	zařízení	k1gNnSc6	zařízení
oznámili	oznámit	k5eAaPmAgMnP	oznámit
svoji	svůj	k3xOyFgFnSc4	svůj
polohu	poloha	k1gFnSc4	poloha
v	v	k7c6	v
restauracích	restaurace	k1gFnPc6	restaurace
<g/>
,	,	kIx,	,
supermarketech	supermarket	k1gInPc6	supermarket
<g/>
,	,	kIx,	,
barech	bar	k1gInPc6	bar
a	a	k8xC	a
kavárnách	kavárna	k1gFnPc6	kavárna
a	a	k8xC	a
jako	jako	k9	jako
odměnu	odměna	k1gFnSc4	odměna
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
získali	získat	k5eAaPmAgMnP	získat
slevy	sleva	k1gFnPc4	sleva
<g/>
,	,	kIx,	,
kupóny	kupón	k1gInPc4	kupón
nebo	nebo	k8xC	nebo
zboží	zboží	k1gNnSc4	zboží
zdarma	zdarma	k6eAd1	zdarma
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
funkce	funkce	k1gFnSc1	funkce
je	být	k5eAaImIp3nS	být
inzerována	inzerovat	k5eAaImNgFnS	inzerovat
jako	jako	k8xC	jako
digitální	digitální	k2eAgFnSc1d1	digitální
verze	verze	k1gFnSc1	verze
věrnostní	věrnostní	k2eAgFnSc2d1	věrnostní
kartičky	kartička	k1gFnSc2	kartička
nebo	nebo	k8xC	nebo
kupónu	kupón	k1gInSc2	kupón
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zákazník	zákazník	k1gMnSc1	zákazník
dostane	dostat	k5eAaPmIp3nS	dostat
odměnu	odměna	k1gFnSc4	odměna
za	za	k7c4	za
svoje	svůj	k3xOyFgNnSc4	svůj
loajální	loajální	k2eAgNnSc4d1	loajální
nákupní	nákupní	k2eAgNnSc4d1	nákupní
chování	chování	k1gNnSc4	chování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Funkce	funkce	k1gFnSc1	funkce
Místa	místo	k1gNnSc2	místo
je	být	k5eAaImIp3nS	být
momentálně	momentálně	k6eAd1	momentálně
dostupná	dostupný	k2eAgFnSc1d1	dostupná
jenom	jenom	k9	jenom
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
<g/>
:	:	kIx,	:
Austrálie	Austrálie	k1gFnSc1	Austrálie
<g/>
,	,	kIx,	,
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
,	,	kIx,	,
Belgie	Belgie	k1gFnSc1	Belgie
<g/>
,	,	kIx,	,
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
<g/>
,	,	kIx,	,
Estonsko	Estonsko	k1gNnSc1	Estonsko
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
,	,	kIx,	,
Kajmanské	Kajmanský	k2eAgInPc1d1	Kajmanský
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc1	Japonsko
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
,	,	kIx,	,
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
,	,	kIx,	,
Norsko	Norsko	k1gNnSc1	Norsko
<g/>
,	,	kIx,	,
Dánsko	Dánsko	k1gNnSc1	Dánsko
<g/>
,	,	kIx,	,
Švédsko	Švédsko	k1gNnSc1	Švédsko
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
,	,	kIx,	,
Jihoafrická	jihoafrický	k2eAgFnSc1d1	Jihoafrická
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
Finsko	Finsko	k1gNnSc1	Finsko
<g/>
,	,	kIx,	,
Irsko	Irsko	k1gNnSc1	Irsko
<g/>
,	,	kIx,	,
Singapur	Singapur	k1gInSc1	Singapur
<g/>
,	,	kIx,	,
Tchaj-wan	Tchajan	k1gInSc1	Tchaj-wan
<g/>
,	,	kIx,	,
Thajsko	Thajsko	k1gNnSc1	Thajsko
<g/>
,	,	kIx,	,
Hongkong	Hongkong	k1gInSc1	Hongkong
<g/>
,	,	kIx,	,
Filipíny	Filipíny	k1gFnPc1	Filipíny
a	a	k8xC	a
Malajsie	Malajsie	k1gFnPc1	Malajsie
<g/>
.	.	kIx.	.
</s>
<s>
Zprovoznění	zprovoznění	k1gNnSc1	zprovoznění
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
dalších	další	k2eAgFnPc6d1	další
zemích	zem	k1gFnPc6	zem
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
10.	[number]	k4	10.
října	říjen	k1gInSc2	říjen
2010	[number]	k4	2010
byla	být	k5eAaImAgFnS	být
funkce	funkce	k1gFnSc1	funkce
Místa	místo	k1gNnSc2	místo
po	po	k7c6	po
iPhonech	iPhon	k1gInPc6	iPhon
zprovozněna	zprovozněn	k2eAgFnSc1d1	zprovozněna
také	také	k9	také
pro	pro	k7c4	pro
BlackBerry	BlackBerr	k1gMnPc4	BlackBerr
<g/>
.	.	kIx.	.
</s>
<s>
Službu	služba	k1gFnSc4	služba
Místa	místo	k1gNnSc2	místo
lze	lze	k6eAd1	lze
používat	používat	k5eAaImF	používat
i	i	k9	i
v	v	k7c6	v
operačním	operační	k2eAgInSc6d1	operační
systému	systém	k1gInSc6	systém
Android	android	k1gInSc1	android
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgMnPc1d1	ostatní
uživatelé	uživatel	k1gMnPc1	uživatel
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
uživatelů	uživatel	k1gMnPc2	uživatel
Windows	Windows	kA	Windows
Mobile	mobile	k1gNnPc4	mobile
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
mít	mít	k5eAaImF	mít
prohlížeč	prohlížeč	k1gMnSc1	prohlížeč
HTML5	HTML5	k1gFnPc4	HTML5
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mohli	moct	k5eAaImAgMnP	moct
používat	používat	k5eAaImF	používat
tuto	tento	k3xDgFnSc4	tento
službu	služba	k1gFnSc4	služba
přes	přes	k7c4	přes
web	web	k1gInSc4	web
Facebook	Facebook	k1gInSc4	Facebook
pro	pro	k7c4	pro
dotyková	dotykový	k2eAgNnPc4d1	dotykové
zařízení	zařízení	k1gNnPc4	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
24.	[number]	k4	24.
srpna	srpen	k1gInSc2	srpen
2011	[number]	k4	2011
bylo	být	k5eAaImAgNnS	být
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
ukončení	ukončení	k1gNnSc1	ukončení
provozu	provoz	k1gInSc2	provoz
služby	služba	k1gFnSc2	služba
Facebook	Facebook	k1gInSc1	Facebook
Místa	místo	k1gNnSc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2014	[number]	k4	2014
zažila	zažít	k5eAaPmAgFnS	zažít
funkce	funkce	k1gFnSc1	funkce
Facebook	Facebook	k1gInSc1	Facebook
Místa	místo	k1gNnSc2	místo
comeback	comebacka	k1gFnPc2	comebacka
a	a	k8xC	a
vrátila	vrátit	k5eAaPmAgFnS	vrátit
se	se	k3xPyFc4	se
v	v	k7c6	v
nové	nový	k2eAgFnSc6d1	nová
podobě	podoba	k1gFnSc6	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc7	její
součástí	součást	k1gFnSc7	součást
jsou	být	k5eAaImIp3nP	být
úvodní	úvodní	k2eAgInPc1d1	úvodní
obrázky	obrázek	k1gInPc1	obrázek
pro	pro	k7c4	pro
vyjádření	vyjádření	k1gNnSc4	vyjádření
emocí	emoce	k1gFnPc2	emoce
<g/>
,	,	kIx,	,
oddíly	oddíl	k1gInPc1	oddíl
pro	pro	k7c4	pro
objevování	objevování	k1gNnSc4	objevování
<g/>
,	,	kIx,	,
úvodní	úvodní	k2eAgInPc1d1	úvodní
stránky	stránka	k1gFnPc4	stránka
měst	město	k1gNnPc2	město
nebo	nebo	k8xC	nebo
kategorií	kategorie	k1gFnPc2	kategorie
<g/>
,	,	kIx,	,
hlubší	hluboký	k2eAgFnSc1d2	hlubší
integrace	integrace	k1gFnSc1	integrace
s	s	k7c7	s
rozhraním	rozhraní	k1gNnSc7	rozhraní
API	API	kA	API
pro	pro	k7c4	pro
umístění	umístění	k1gNnSc4	umístění
<g/>
,	,	kIx,	,
dotazy	dotaz	k1gInPc4	dotaz
protokolu	protokol	k1gInSc2	protokol
Graph	Graph	k1gMnSc1	Graph
Search	Search	k1gMnSc1	Search
a	a	k8xC	a
obsah	obsah	k1gInSc1	obsah
generovaný	generovaný	k2eAgInSc1d1	generovaný
uživateli	uživatel	k1gMnPc7	uživatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Platforma	platforma	k1gFnSc1	platforma
===	===	k?	===
</s>
</p>
<p>
<s>
Facebook	Facebook	k1gInSc1	Facebook
Platforma	platforma	k1gFnSc1	platforma
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
sadu	sada	k1gFnSc4	sada
rozhraní	rozhraní	k1gNnSc2	rozhraní
API	API	kA	API
a	a	k8xC	a
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
externím	externí	k2eAgMnPc3d1	externí
vývojářům	vývojář	k1gMnPc3	vývojář
integrovat	integrovat	k5eAaBmF	integrovat
protokol	protokol	k1gInSc4	protokol
"	"	kIx"	"
<g/>
open	open	k1gMnSc1	open
graph	graph	k1gMnSc1	graph
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
už	už	k9	už
prostřednictvím	prostřednictví	k1gNnSc7	prostřednictví
aplikací	aplikace	k1gFnPc2	aplikace
na	na	k7c6	na
webu	web	k1gInSc6	web
Facebook.com	Facebook.com	k1gInSc1	Facebook.com
nebo	nebo	k8xC	nebo
na	na	k7c6	na
externích	externí	k2eAgInPc6d1	externí
webech	web	k1gInPc6	web
a	a	k8xC	a
zařízeních	zařízení	k1gNnPc6	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Facebook	Facebook	k1gInSc4	Facebook
Platforma	platforma	k1gFnSc1	platforma
byla	být	k5eAaImAgFnS	být
spuštěna	spustit	k5eAaPmNgFnS	spustit
24.	[number]	k4	24.
května	květen	k1gInSc2	květen
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
Vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
se	se	k3xPyFc4	se
z	z	k7c2	z
možnosti	možnost	k1gFnSc2	možnost
vyvíjet	vyvíjet	k5eAaImF	vyvíjet
aplikace	aplikace	k1gFnPc4	aplikace
jenom	jenom	k9	jenom
na	na	k7c6	na
webu	web	k1gInSc6	web
Facebook.com	Facebook.com	k1gInSc4	Facebook.com
v	v	k7c4	v
platformu	platforma	k1gFnSc4	platforma
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
podporuje	podporovat	k5eAaImIp3nS	podporovat
také	také	k9	také
integraci	integrace	k1gFnSc4	integrace
na	na	k7c6	na
webu	web	k1gInSc6	web
a	a	k8xC	a
v	v	k7c6	v
zařízeních	zařízení	k1gNnPc6	zařízení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Statistické	statistický	k2eAgInPc1d1	statistický
údaje	údaj	k1gInPc1	údaj
pro	pro	k7c4	pro
Facebook	Facebook	k1gInSc4	Facebook
Platformu	platforma	k1gFnSc4	platforma
z	z	k7c2	z
května	květen	k1gInSc2	květen
2010	[number]	k4	2010
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
<s>
Facebook	Facebook	k1gInSc1	Facebook
Platformu	platforma	k1gFnSc4	platforma
využívá	využívat	k5eAaPmIp3nS	využívat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
jeden	jeden	k4xCgInSc4	jeden
milión	milión	k4xCgInSc1	milión
vývojářů	vývojář	k1gMnPc2	vývojář
a	a	k8xC	a
podnikatelů	podnikatel	k1gMnPc2	podnikatel
z	z	k7c2	z
více	hodně	k6eAd2	hodně
než	než	k8xS	než
180	[number]	k4	180
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
Facebook	Facebook	k1gInSc4	Facebook
Platformě	platforma	k1gFnSc3	platforma
běží	běžet	k5eAaImIp3nS	běžet
momentálně	momentálně	k6eAd1	momentálně
více	hodně	k6eAd2	hodně
než	než	k8xS	než
550 000	[number]	k4	550 000
aktivních	aktivní	k2eAgFnPc2d1	aktivní
aplikací	aplikace	k1gFnPc2	aplikace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Aplikace	aplikace	k1gFnSc1	aplikace
Platformy	platforma	k1gFnSc2	platforma
použije	použít	k5eAaPmIp3nS	použít
každý	každý	k3xTgInSc4	každý
měsíc	měsíc	k1gInSc4	měsíc
více	hodně	k6eAd2	hodně
než	než	k8xS	než
70	[number]	k4	70
%	%	kIx~	%
uživatelů	uživatel	k1gMnPc2	uživatel
Facebooku	Facebook	k1gInSc2	Facebook
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
S	s	k7c7	s
Facebook	Facebook	k1gInSc1	Facebook
Platformou	platforma	k1gFnSc7	platforma
je	být	k5eAaImIp3nS	být
integrováno	integrovat	k5eAaBmNgNnS	integrovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
250 000	[number]	k4	250 000
webů	web	k1gInPc2	web
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Interakci	interakce	k1gFnSc4	interakce
s	s	k7c7	s
Facebookem	Facebook	k1gInSc7	Facebook
na	na	k7c6	na
externích	externí	k2eAgInPc6d1	externí
webech	web	k1gInPc6	web
provede	provést	k5eAaPmIp3nS	provést
každý	každý	k3xTgInSc4	každý
měsíc	měsíc	k1gInSc4	měsíc
více	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
miliónů	milión	k4xCgInPc2	milión
uživatelů	uživatel	k1gMnPc2	uživatel
Facebooku	Facebook	k1gInSc2	Facebook
<g/>
.	.	kIx.	.
<g/>
Externí	externí	k2eAgFnSc2d1	externí
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
společnosti	společnost	k1gFnPc1	společnost
Adonomics	Adonomicsa	k1gFnPc2	Adonomicsa
<g/>
,	,	kIx,	,
Kontagent	Kontagent	k1gInSc4	Kontagent
a	a	k8xC	a
Mixpanel	Mixpanel	k1gInSc4	Mixpanel
<g/>
,	,	kIx,	,
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
metriky	metrika	k1gFnPc1	metrika
pro	pro	k7c4	pro
aplikace	aplikace	k1gFnPc4	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
reakce	reakce	k1gFnPc1	reakce
na	na	k7c4	na
volání	volání	k1gNnSc4	volání
po	po	k7c4	po
Facebook	Facebook	k1gInSc4	Facebook
aplikacích	aplikace	k1gFnPc6	aplikace
se	se	k3xPyFc4	se
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
blogy	bloga	k1gFnPc1	bloga
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
AppRate	AppRat	k1gMnSc5	AppRat
<g/>
,	,	kIx,	,
Inside	Insid	k1gMnSc5	Insid
Facebook	Facebook	k1gInSc4	Facebook
a	a	k8xC	a
Face	Face	k1gFnSc4	Face
Reviews	Reviewsa	k1gFnPc2	Reviewsa
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
4.	[number]	k4	4.
července	červenec	k1gInSc2	červenec
2007	[number]	k4	2007
společnost	společnost	k1gFnSc1	společnost
Altura	Altura	k1gFnSc1	Altura
Ventures	Ventures	k1gInSc4	Ventures
oznámila	oznámit	k5eAaPmAgFnS	oznámit
vznik	vznik	k1gInSc4	vznik
fondu	fond	k1gInSc2	fond
"	"	kIx"	"
<g/>
Altura	Altura	k1gFnSc1	Altura
1	[number]	k4	1
Facebook	Facebook	k1gInSc1	Facebook
Investment	Investment	k1gMnSc1	Investment
Fund	fund	k1gInSc1	fund
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
světově	světově	k6eAd1	světově
první	první	k4xOgFnSc7	první
firmou	firma	k1gFnSc7	firma
s	s	k7c7	s
investičním	investiční	k2eAgInSc7d1	investiční
kapitálem	kapitál	k1gInSc7	kapitál
vloženým	vložený	k2eAgInSc7d1	vložený
pouze	pouze	k6eAd1	pouze
do	do	k7c2	do
Facebooku	Facebook	k1gInSc2	Facebook
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
29.	[number]	k4	29.
srpna	srpen	k1gInSc2	srpen
2007	[number]	k4	2007
Facebook	Facebook	k1gInSc1	Facebook
změnil	změnit	k5eAaPmAgInS	změnit
způsob	způsob	k1gInSc4	způsob
měření	měření	k1gNnSc2	měření
oblíbenosti	oblíbenost	k1gFnSc2	oblíbenost
aplikací	aplikace	k1gFnPc2	aplikace
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
obrátil	obrátit	k5eAaPmAgMnS	obrátit
pozornost	pozornost	k1gFnSc4	pozornost
k	k	k7c3	k
zajímavějším	zajímavý	k2eAgFnPc3d2	zajímavější
aplikacím	aplikace	k1gFnPc3	aplikace
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
byl	být	k5eAaImAgMnS	být
kritizován	kritizovat	k5eAaImNgMnS	kritizovat
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
hodnocení	hodnocení	k1gNnSc1	hodnocení
aplikací	aplikace	k1gFnPc2	aplikace
jenom	jenom	k9	jenom
podle	podle	k7c2	podle
počtu	počet	k1gInSc2	počet
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
si	se	k3xPyFc3	se
je	on	k3xPp3gFnPc4	on
nainstalovali	nainstalovat	k5eAaPmAgMnP	nainstalovat
<g/>
,	,	kIx,	,
zvýhodňuje	zvýhodňovat	k5eAaImIp3nS	zvýhodňovat
vysoce	vysoce	k6eAd1	vysoce
virální	virální	k2eAgNnSc1d1	virální
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
neužitečné	užitečný	k2eNgFnSc2d1	neužitečná
aplikace	aplikace	k1gFnSc2	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
Facebook	Facebook	k1gInSc1	Facebook
Aplikace	aplikace	k1gFnSc2	aplikace
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
terčem	terč	k1gInSc7	terč
kritiky	kritika	k1gFnSc2	kritika
technického	technický	k2eAgInSc2d1	technický
blogu	blog	k1gInSc2	blog
Valleywag	Valleywag	k1gMnSc1	Valleywag
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	on	k3xPp3gNnSc4	on
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
roh	roh	k1gInSc1	roh
neužitečnosti	neužitečnost	k1gFnSc2	neužitečnost
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgInPc1d1	jiný
hlasy	hlas	k1gInPc1	hlas
volají	volat	k5eAaImIp3nP	volat
po	po	k7c6	po
omezení	omezení	k1gNnSc6	omezení
externích	externí	k2eAgFnPc2d1	externí
aplikací	aplikace	k1gFnPc2	aplikace
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
"	"	kIx"	"
<g/>
uživatelské	uživatelský	k2eAgNnSc1d1	Uživatelské
prostředí	prostředí	k1gNnSc1	prostředí
<g/>
"	"	kIx"	"
na	na	k7c6	na
Facebooku	Facebook	k1gInSc6	Facebook
nebylo	být	k5eNaImAgNnS	být
degradováno	degradovat	k5eAaBmNgNnS	degradovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Primárním	primární	k2eAgInSc7d1	primární
záměrem	záměr	k1gInSc7	záměr
celé	celý	k2eAgFnSc2d1	celá
řady	řada	k1gFnSc2	řada
vývojářů	vývojář	k1gMnPc2	vývojář
Facebook	Facebook	k1gInSc1	Facebook
aplikací	aplikace	k1gFnPc2	aplikace
bylo	být	k5eAaImAgNnS	být
bezpochyby	bezpochyby	k6eAd1	bezpochyby
vytvoření	vytvoření	k1gNnSc1	vytvoření
virálních	virální	k2eAgFnPc2d1	virální
aplikací	aplikace	k1gFnPc2	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
Stanfordova	Stanfordův	k2eAgFnSc1d1	Stanfordova
univerzita	univerzita	k1gFnSc1	univerzita
dokonce	dokonce	k9	dokonce
v	v	k7c6	v
podzimním	podzimní	k2eAgInSc6d1	podzimní
semestru	semestr	k1gInSc6	semestr
2007	[number]	k4	2007
nabídla	nabídnout	k5eAaPmAgFnS	nabídnout
kurz	kurz	k1gInSc4	kurz
s	s	k7c7	s
názvem	název	k1gInSc7	název
Computer	computer	k1gInSc1	computer
Science	Science	k1gFnSc2	Science
(	(	kIx(	(
<g/>
CS	CS	kA	CS
<g/>
)	)	kIx)	)
377W	[number]	k4	377W
<g/>
:	:	kIx,	:
Create	Creat	k1gInSc5	Creat
Engaging	Engaging	k1gInSc4	Engaging
Web	web	k1gInSc1	web
Applications	Applications	k1gInSc1	Applications
Using	Using	k1gInSc1	Using
Metrics	Metrics	k1gInSc1	Metrics
and	and	k?	and
Learning	Learning	k1gInSc1	Learning
on	on	k3xPp3gInSc1	on
Facebook	Facebook	k1gInSc1	Facebook
(	(	kIx(	(
<g/>
Počítačová	počítačový	k2eAgFnSc1d1	počítačová
věda	věda	k1gFnSc1	věda
(	(	kIx(	(
<g/>
PV	PV	kA	PV
<g/>
)	)	kIx)	)
377	[number]	k4	377
W	W	kA	W
<g/>
:	:	kIx,	:
Vytváření	vytváření	k1gNnSc4	vytváření
zajímavých	zajímavý	k2eAgFnPc2d1	zajímavá
webových	webový	k2eAgFnPc2d1	webová
aplikací	aplikace	k1gFnPc2	aplikace
s	s	k7c7	s
využitím	využití	k1gNnSc7	využití
metrik	metrika	k1gFnPc2	metrika
a	a	k8xC	a
informací	informace	k1gFnPc2	informace
na	na	k7c6	na
Facebooku	Facebook	k1gInSc6	Facebook
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c4	mnoho
aplikací	aplikace	k1gFnPc2	aplikace
vytvořených	vytvořený	k2eAgFnPc2d1	vytvořená
studenty	student	k1gMnPc7	student
tohoto	tento	k3xDgInSc2	tento
kurzu	kurz	k1gInSc2	kurz
bylo	být	k5eAaImAgNnS	být
velmi	velmi	k6eAd1	velmi
úspěšných	úspěšný	k2eAgNnPc2d1	úspěšné
a	a	k8xC	a
zařadilo	zařadit	k5eAaPmAgNnS	zařadit
se	se	k3xPyFc4	se
mezi	mezi	k7c4	mezi
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
Facebook	Facebook	k1gInSc4	Facebook
aplikace	aplikace	k1gFnSc2	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
z	z	k7c2	z
aplikací	aplikace	k1gFnPc2	aplikace
získaly	získat	k5eAaPmAgFnP	získat
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
měsíc	měsíc	k1gInSc4	měsíc
více	hodně	k6eAd2	hodně
než	než	k8xS	než
3,5	[number]	k4	3,5
miliónů	milión	k4xCgInPc2	milión
uživatelů	uživatel	k1gMnPc2	uživatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Otázky	otázka	k1gFnPc1	otázka
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2010	[number]	k4	2010
Facebook	Facebook	k1gInSc1	Facebook
začal	začít	k5eAaPmAgInS	začít
testovat	testovat	k5eAaImF	testovat
službu	služba	k1gFnSc4	služba
Otázky	otázka	k1gFnSc2	otázka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
stát	stát	k5eAaPmF	stát
konkurentem	konkurent	k1gMnSc7	konkurent
služeb	služba	k1gFnPc2	služba
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
Yahoo	Yahoo	k6eAd1	Yahoo
<g/>
!	!	kIx.	!
</s>
<s>
Answers	Answers	k6eAd1	Answers
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
24.	[number]	k4	24.
března	březen	k1gInSc2	březen
2011	[number]	k4	2011
Facebook	Facebook	k1gInSc1	Facebook
oznámil	oznámit	k5eAaPmAgInS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gInSc1	jeho
nový	nový	k2eAgInSc1d1	nový
produkt	produkt	k1gInSc1	produkt
<g/>
,	,	kIx,	,
Facebook	Facebook	k1gInSc1	Facebook
Otázky	otázka	k1gFnSc2	otázka
<g/>
,	,	kIx,	,
využívá	využívat	k5eAaImIp3nS	využívat
kromě	kromě	k7c2	kromě
dlouhých	dlouhý	k2eAgFnPc2d1	dlouhá
odpovědí	odpověď	k1gFnPc2	odpověď
i	i	k8xC	i
krátké	krátký	k2eAgFnPc1d1	krátká
anketní	anketní	k2eAgFnPc1d1	anketní
otázky	otázka	k1gFnPc1	otázka
a	a	k8xC	a
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
lze	lze	k6eAd1	lze
propojit	propojit	k5eAaPmF	propojit
přímo	přímo	k6eAd1	přímo
s	s	k7c7	s
relevantními	relevantní	k2eAgFnPc7d1	relevantní
položkami	položka	k1gFnPc7	položka
v	v	k7c6	v
adresáři	adresář	k1gInSc6	adresář
"	"	kIx"	"
<g/>
fanouškovských	fanouškovský	k2eAgFnPc2d1	fanouškovská
stránek	stránka	k1gFnPc2	stránka
<g/>
"	"	kIx"	"
na	na	k7c6	na
Facebooku	Facebook	k1gInSc6	Facebook
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Facebook	Facebook	k1gInSc1	Facebook
Paper	Paper	k1gInSc1	Paper
===	===	k?	===
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
týdnu	týden	k1gInSc6	týden
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
oslavoval	oslavovat	k5eAaImAgMnS	oslavovat
desáté	desátý	k4xOgNnSc4	desátý
výročí	výročí	k1gNnSc4	výročí
vzniku	vznik	k1gInSc2	vznik
<g/>
,	,	kIx,	,
vydal	vydat	k5eAaPmAgInS	vydat
Facebook	Facebook	k1gInSc1	Facebook
aplikaci	aplikace	k1gFnSc4	aplikace
Paper	Paper	k1gInSc4	Paper
pro	pro	k7c4	pro
iPhone	iPhon	k1gMnSc5	iPhon
<g/>
.	.	kIx.	.
</s>
<s>
Aplikace	aplikace	k1gFnSc1	aplikace
má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgFnPc4	dva
hlavní	hlavní	k2eAgFnPc4d1	hlavní
funkce	funkce	k1gFnPc4	funkce
<g/>
:	:	kIx,	:
za	za	k7c4	za
prvé	prvý	k4xOgFnPc4	prvý
<g/>
,	,	kIx,	,
kanál	kanál	k1gInSc1	kanál
vybraných	vybraný	k2eAgInPc2d1	vybraný
příspěvků	příspěvek	k1gInPc2	příspěvek
na	na	k7c6	na
Facebooku	Facebook	k1gInSc6	Facebook
je	být	k5eAaImIp3nS	být
grafičtější	grafický	k2eAgNnSc1d2	grafický
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
aplikace	aplikace	k1gFnSc1	aplikace
využívá	využívat	k5eAaPmIp3nS	využívat
technologii	technologie	k1gFnSc4	technologie
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
zobrazení	zobrazení	k1gNnSc1	zobrazení
fotek	fotka	k1gFnPc2	fotka
na	na	k7c4	na
celou	celý	k2eAgFnSc4d1	celá
obrazovku	obrazovka	k1gFnSc4	obrazovka
a	a	k8xC	a
záznam	záznam	k1gInSc4	záznam
videa	video	k1gNnSc2	video
<g/>
.	.	kIx.	.
</s>
<s>
Obsah	obsah	k1gInSc1	obsah
je	být	k5eAaImIp3nS	být
uspořádán	uspořádat	k5eAaPmNgInS	uspořádat
pod	pod	k7c7	pod
hlavičkami	hlavička	k1gFnPc7	hlavička
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
"	"	kIx"	"
<g/>
Tvůrci	tvůrce	k1gMnPc1	tvůrce
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Planeta	planeta	k1gFnSc1	planeta
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
za	za	k7c4	za
druhé	druhý	k4xOgFnPc4	druhý
<g/>
,	,	kIx,	,
Paper	Paper	k1gInSc1	Paper
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
uživatelům	uživatel	k1gMnPc3	uživatel
zveřejňovat	zveřejňovat	k5eAaImF	zveřejňovat
aktualizace	aktualizace	k1gFnPc4	aktualizace
stavu	stav	k1gInSc2	stav
<g/>
,	,	kIx,	,
fotky	fotka	k1gFnPc1	fotka
a	a	k8xC	a
příspěvky	příspěvek	k1gInPc1	příspěvek
na	na	k7c6	na
Facebooku	Facebook	k1gInSc6	Facebook
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
design	design	k1gInSc1	design
je	být	k5eAaImIp3nS	být
odlišný	odlišný	k2eAgInSc1d1	odlišný
a	a	k8xC	a
zaměřený	zaměřený	k2eAgInSc1d1	zaměřený
více	hodně	k6eAd2	hodně
na	na	k7c6	na
prezentaci	prezentace	k1gFnSc6	prezentace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Možnosti	možnost	k1gFnPc1	možnost
pro	pro	k7c4	pro
firmy	firma	k1gFnPc4	firma
===	===	k?	===
</s>
</p>
<p>
<s>
Firmy	firma	k1gFnPc1	firma
mohou	moct	k5eAaImIp3nP	moct
na	na	k7c6	na
Facebooku	Facebook	k1gInSc6	Facebook
zakládat	zakládat	k5eAaImF	zakládat
oficiální	oficiální	k2eAgFnPc4d1	oficiální
stránky	stránka	k1gFnPc4	stránka
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Pages	Pages	k1gInSc1	Pages
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
kterým	který	k3yQgFnPc3	který
mohou	moct	k5eAaImIp3nP	moct
komunikovat	komunikovat	k5eAaImF	komunikovat
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
příznivci	příznivec	k1gMnPc7	příznivec
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Likers	Likers	k1gInSc1	Likers
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
"	"	kIx"	"
<g/>
Fans	Fans	k1gInSc1	Fans
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
komerčním	komerční	k2eAgInPc3d1	komerční
účelům	účel	k1gInPc3	účel
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
na	na	k7c6	na
Facebooku	Facebook	k1gInSc6	Facebook
využívat	využívat	k5eAaImF	využívat
také	také	k6eAd1	také
PPC	PPC	kA	PPC
reklamu	reklama	k1gFnSc4	reklama
-	-	kIx~	-
textové	textový	k2eAgInPc1d1	textový
inzeráty	inzerát	k1gInPc1	inzerát
doplněné	doplněný	k2eAgInPc1d1	doplněný
o	o	k7c4	o
obrázek	obrázek	k1gInSc4	obrázek
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
dostatečně	dostatečně	k6eAd1	dostatečně
velké	velký	k2eAgFnSc6d1	velká
útratě	útrata	k1gFnSc6	útrata
mohou	moct	k5eAaImIp3nP	moct
firmy	firma	k1gFnPc1	firma
na	na	k7c6	na
Facebooku	Facebook	k1gInSc6	Facebook
nakupovat	nakupovat	k5eAaBmF	nakupovat
i	i	k9	i
bannery	banner	k1gInPc4	banner
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
inzerce	inzerce	k1gFnSc2	inzerce
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
teprve	teprve	k6eAd1	teprve
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
<g/>
,	,	kIx,	,
mediálním	mediální	k2eAgNnSc7d1	mediální
zastoupením	zastoupení	k1gNnSc7	zastoupení
pro	pro	k7c4	pro
Facebook	Facebook	k1gInSc4	Facebook
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
ČR	ČR	kA	ČR
agentura	agentura	k1gFnSc1	agentura
Arbo	arba	k1gFnSc5	arba
Interactive	Interactiv	k1gInSc5	Interactiv
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Aplikace	aplikace	k1gFnPc4	aplikace
třetích	třetí	k4xOgFnPc2	třetí
stran	strana	k1gFnPc2	strana
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Další	další	k2eAgFnPc1d1	další
aplikace	aplikace	k1gFnPc1	aplikace
===	===	k?	===
</s>
</p>
<p>
<s>
Portálových	portálový	k2eAgFnPc2d1	portálová
aplikací	aplikace	k1gFnPc2	aplikace
je	být	k5eAaImIp3nS	být
nepřeberné	přeberný	k2eNgNnSc1d1	nepřeberné
množství	množství	k1gNnSc1	množství
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
<s>
Import	import	k1gInSc1	import
RSS	RSS	kA	RSS
<g/>
:	:	kIx,	:
import	import	k1gInSc1	import
skladeb	skladba	k1gFnPc2	skladba
z	z	k7c2	z
Last	Lasta	k1gFnPc2	Lasta
<g/>
.	.	kIx.	.
<g/>
fm	fm	k?	fm
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
různé	různý	k2eAgInPc1d1	různý
druhy	druh	k1gInPc1	druh
testů	test	k1gInPc2	test
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
mapka	mapka	k1gFnSc1	mapka
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
daný	daný	k2eAgMnSc1d1	daný
uživatel	uživatel	k1gMnSc1	uživatel
navštívil	navštívit	k5eAaPmAgMnS	navštívit
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
přání	přání	k1gNnSc4	přání
k	k	k7c3	k
Vánocům	Vánoce	k1gFnPc3	Vánoce
<g/>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Hry	hra	k1gFnPc1	hra
===	===	k?	===
</s>
</p>
<p>
<s>
Hry	hra	k1gFnPc1	hra
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
založené	založený	k2eAgInPc1d1	založený
na	na	k7c4	na
využití	využití	k1gNnSc4	využití
sociálnosti	sociálnost	k1gFnSc2	sociálnost
uživatelů	uživatel	k1gMnPc2	uživatel
<g/>
.	.	kIx.	.
</s>
<s>
Provozovatelé	provozovatel	k1gMnPc1	provozovatel
her	hra	k1gFnPc2	hra
získávají	získávat	k5eAaImIp3nP	získávat
nové	nový	k2eAgMnPc4d1	nový
uživatele	uživatel	k1gMnPc4	uživatel
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
své	svůj	k3xOyFgMnPc4	svůj
stávající	stávající	k2eAgMnPc4d1	stávající
uživatele	uživatel	k1gMnPc4	uživatel
uplácejí	uplácet	k5eAaImIp3nP	uplácet
novými	nový	k2eAgFnPc7d1	nová
funkcemi	funkce	k1gFnPc7	funkce
nebo	nebo	k8xC	nebo
zpřístupněním	zpřístupnění	k1gNnSc7	zpřístupnění
skrytých	skrytý	k2eAgFnPc2d1	skrytá
funkcí	funkce	k1gFnPc2	funkce
za	za	k7c4	za
rozeslání	rozeslání	k1gNnSc4	rozeslání
pozvánek	pozvánka	k1gFnPc2	pozvánka
co	co	k9	co
možná	možná	k6eAd1	možná
největšímu	veliký	k2eAgInSc3d3	veliký
počtu	počet	k1gInSc3	počet
svých	svůj	k3xOyFgMnPc2	svůj
"	"	kIx"	"
<g/>
přátel	přítel	k1gMnPc2	přítel
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
některé	některý	k3yIgMnPc4	některý
uživatele	uživatel	k1gMnPc4	uživatel
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
zakládání	zakládání	k1gNnSc3	zakládání
vícero	vícero	k1gNnSc1	vícero
účtů	účet	k1gInPc2	účet
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
zase	zase	k9	zase
sám	sám	k3xTgInSc4	sám
FB	FB	kA	FB
snaží	snažit	k5eAaImIp3nS	snažit
popírat	popírat	k5eAaImF	popírat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
Farm	Farm	k1gMnSc1	Farm
Heroes	Heroes	k1gMnSc1	Heroes
Saga	Saga	k1gMnSc1	Saga
====	====	k?	====
</s>
</p>
<p>
<s>
Farma	farma	k1gFnSc1	farma
heroes	heroesa	k1gFnPc2	heroesa
saga	saga	k1gFnSc1	saga
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejpopulárnějších	populární	k2eAgFnPc2d3	nejpopulárnější
her	hra	k1gFnPc2	hra
na	na	k7c6	na
facebooku	facebook	k1gInSc6	facebook
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Hra	hra	k1gFnSc1	hra
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
snadná	snadný	k2eAgFnSc1d1	snadná
<g/>
.	.	kIx.	.
</s>
<s>
Hráč	hráč	k1gMnSc1	hráč
dostane	dostat	k5eAaPmIp3nS	dostat
pět	pět	k4xCc4	pět
životů	život	k1gInPc2	život
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
čase	čas	k1gInSc6	čas
a	a	k8xC	a
musí	muset	k5eAaImIp3nS	muset
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
hrát	hrát	k5eAaImF	hrát
<g/>
.	.	kIx.	.
</s>
<s>
Hráči	hráč	k1gMnPc1	hráč
musí	muset	k5eAaImIp3nP	muset
přepnout	přepnout	k5eAaPmF	přepnout
dlaždice	dlaždice	k1gFnPc4	dlaždice
a	a	k8xC	a
dělat	dělat	k5eAaImF	dělat
zápasy	zápas	k1gInPc1	zápas
dosažení	dosažení	k1gNnSc2	dosažení
zadaného	zadaný	k2eAgInSc2d1	zadaný
cíle	cíl	k1gInSc2	cíl
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Jedno	jeden	k4xCgNnSc1	jeden
je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
<g/>
,	,	kIx,	,
že	že	k8xS	že
zde	zde	k6eAd1	zde
existuje	existovat	k5eAaImIp3nS	existovat
určitý	určitý	k2eAgInSc4d1	určitý
počet	počet	k1gInSc4	počet
tahů	tah	k1gInPc2	tah
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Takže	takže	k9	takže
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
dokončit	dokončit	k5eAaPmF	dokončit
své	svůj	k3xOyFgInPc4	svůj
cíle	cíl	k1gInPc4	cíl
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
počtu	počet	k1gInSc6	počet
tahů	tah	k1gInPc2	tah
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
Friends	Friends	k1gInSc1	Friends
For	forum	k1gNnPc2	forum
Sale	Sal	k1gFnSc2	Sal
====	====	k?	====
</s>
</p>
<p>
<s>
Friends	Friends	k6eAd1	Friends
For	forum	k1gNnPc2	forum
Sale	Sal	k1gInSc2	Sal
má	mít	k5eAaImIp3nS	mít
3,7	[number]	k4	3,7
milionu	milion	k4xCgInSc2	milion
aktivních	aktivní	k2eAgInPc2d1	aktivní
uživatelů	uživatel	k1gMnPc2	uživatel
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
jejího	její	k3xOp3gInSc2	její
zrodu	zrod	k1gInSc2	zrod
stáli	stát	k5eAaImAgMnP	stát
dva	dva	k4xCgMnPc1	dva
studenti	student	k1gMnPc1	student
<g/>
,	,	kIx,	,
Alexander	Alexandra	k1gFnPc2	Alexandra
Le	Le	k1gFnSc2	Le
a	a	k8xC	a
Siqi	Siq	k1gFnSc2	Siq
Chen	Chena	k1gFnPc2	Chena
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
3	[number]	k4	3
měsíců	měsíc	k1gInPc2	měsíc
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
aplikace	aplikace	k1gFnSc1	aplikace
stala	stát	k5eAaPmAgFnS	stát
s	s	k7c7	s
300	[number]	k4	300
<g/>
%	%	kIx~	%
růstem	růst	k1gInSc7	růst
10.	[number]	k4	10.
nejoblíbenější	oblíbený	k2eAgFnSc2d3	nejoblíbenější
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
Facebooku	Facebook	k1gInSc6	Facebook
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
má	mít	k5eAaImIp3nS	mít
na	na	k7c4	na
300	[number]	k4	300
milionů	milion	k4xCgInPc2	milion
zhlédnutých	zhlédnutý	k2eAgFnPc2d1	zhlédnutá
stránek	stránka	k1gFnPc2	stránka
měsíčně	měsíčně	k6eAd1	měsíčně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
Mafia	Mafium	k1gNnPc1	Mafium
Wars	Warsa	k1gFnPc2	Warsa
====	====	k?	====
</s>
</p>
<p>
</p>
<p>
<s>
Aplikace	aplikace	k1gFnSc1	aplikace
Mafia	Mafia	k1gFnSc1	Mafia
Wars	Wars	k1gInSc4	Wars
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
velmi	velmi	k6eAd1	velmi
populárním	populární	k2eAgFnPc3d1	populární
hrám	hra	k1gFnPc3	hra
<g/>
,	,	kIx,	,
zaměřeným	zaměřený	k2eAgNnSc7d1	zaměřené
na	na	k7c4	na
páchání	páchání	k1gNnSc4	páchání
virtuálních	virtuální	k2eAgInPc2d1	virtuální
zločinů	zločin	k1gInPc2	zločin
a	a	k8xC	a
neustálé	neustálý	k2eAgNnSc1d1	neustálé
rozšiřování	rozšiřování	k1gNnSc1	rozšiřování
své	svůj	k3xOyFgFnSc2	svůj
mafie	mafie	k1gFnSc2	mafie
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
zpoplatněna	zpoplatnit	k5eAaPmNgFnS	zpoplatnit
přímo	přímo	k6eAd1	přímo
<g/>
,	,	kIx,	,
její	její	k3xOp3gInPc1	její
výdělky	výdělek	k1gInPc1	výdělek
plynou	plynout	k5eAaImIp3nP	plynout
z	z	k7c2	z
principu	princip	k1gInSc2	princip
zpoplatňování	zpoplatňování	k1gNnSc4	zpoplatňování
prémiového	prémiový	k2eAgInSc2d1	prémiový
obsahu	obsah	k1gInSc2	obsah
pomocí	pomocí	k7c2	pomocí
služeb	služba	k1gFnPc2	služba
kreditních	kreditní	k2eAgFnPc2d1	kreditní
karet	kareta	k1gFnPc2	kareta
nebo	nebo	k8xC	nebo
služby	služba	k1gFnSc2	služba
PayPal	PayPal	k1gInSc1	PayPal
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
FarmVille	FarmVille	k1gNnPc3	FarmVille
====	====	k?	====
</s>
</p>
<p>
</p>
<p>
<s>
Aplikace	aplikace	k1gFnSc1	aplikace
FarmVille	FarmVille	k1gFnSc2	FarmVille
je	být	k5eAaImIp3nS	být
flashová	flashový	k2eAgFnSc1d1	flashová
hra	hra	k1gFnSc1	hra
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
uživatel	uživatel	k1gMnSc1	uživatel
stará	starat	k5eAaImIp3nS	starat
o	o	k7c4	o
virtuální	virtuální	k2eAgFnSc4d1	virtuální
farmu	farma	k1gFnSc4	farma
a	a	k8xC	a
pomocí	pomocí	k7c2	pomocí
virálního	virální	k2eAgInSc2d1	virální
efektu	efekt	k1gInSc2	efekt
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
přáteli	přítel	k1gMnPc7	přítel
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
sousedství	sousedství	k1gNnSc6	sousedství
<g/>
,	,	kIx,	,
aplikace	aplikace	k1gFnSc1	aplikace
také	také	k9	také
nabízí	nabízet	k5eAaImIp3nS	nabízet
placený	placený	k2eAgInSc1d1	placený
prémiový	prémiový	k2eAgInSc1d1	prémiový
obsah	obsah	k1gInSc1	obsah
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Algoritmy	algoritmus	k1gInPc1	algoritmus
a	a	k8xC	a
mechanismy	mechanismus	k1gInPc1	mechanismus
fungování	fungování	k1gNnSc2	fungování
Facebooku	Facebook	k1gInSc6	Facebook
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Udržování	udržování	k1gNnSc1	udržování
pozornosti	pozornost	k1gFnSc2	pozornost
uživatelů	uživatel	k1gMnPc2	uživatel
===	===	k?	===
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
Seana	Sean	k1gMnSc2	Sean
Parkera	Parker	k1gMnSc2	Parker
je	být	k5eAaImIp3nS	být
Facebook	Facebook	k1gInSc4	Facebook
první	první	k4xOgFnSc2	první
z	z	k7c2	z
aplikací	aplikace	k1gFnPc2	aplikace
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yIgMnPc2	který
se	se	k3xPyFc4	se
její	její	k3xOp3gMnSc1	její
tvůrci	tvůrce	k1gMnPc1	tvůrce
zamysleli	zamyslet	k5eAaPmAgMnP	zamyslet
nad	nad	k7c7	nad
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
získat	získat	k5eAaPmF	získat
co	co	k9	co
nejvíce	hodně	k6eAd3	hodně
uživatelova	uživatelův	k2eAgInSc2d1	uživatelův
času	čas	k1gInSc2	čas
a	a	k8xC	a
udržet	udržet	k5eAaPmF	udržet
pozornost	pozornost	k1gFnSc4	pozornost
uživatele	uživatel	k1gMnSc2	uživatel
co	co	k9	co
nejdéle	dlouho	k6eAd3	dlouho
<g/>
.	.	kIx.	.
</s>
<s>
Což	což	k3yQnSc1	což
podle	podle	k7c2	podle
Parkera	Parkero	k1gNnSc2	Parkero
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
uživatel	uživatel	k1gMnSc1	uživatel
musí	muset	k5eAaImIp3nS	muset
každou	každý	k3xTgFnSc4	každý
chvíli	chvíle	k1gFnSc4	chvíle
dostat	dostat	k5eAaPmF	dostat
malou	malý	k2eAgFnSc4d1	malá
dávku	dávka	k1gFnSc4	dávka
dopaminu	dopamin	k1gInSc2	dopamin
<g/>
.	.	kIx.	.
</s>
<s>
Princip	princip	k1gInSc1	princip
je	být	k5eAaImIp3nS	být
podobný	podobný	k2eAgInSc1d1	podobný
jako	jako	k8xC	jako
u	u	k7c2	u
hracích	hrací	k2eAgInPc2d1	hrací
automatů	automat	k1gInPc2	automat
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
automat	automat	k1gInSc1	automat
dává	dávat	k5eAaImIp3nS	dávat
hráči	hráč	k1gMnSc3	hráč
čas	čas	k1gInSc4	čas
od	od	k7c2	od
času	čas	k1gInSc2	čas
malé	malý	k2eAgFnSc2d1	malá
částky	částka	k1gFnSc2	částka
a	a	k8xC	a
hráč	hráč	k1gMnSc1	hráč
nikdy	nikdy	k6eAd1	nikdy
neví	vědět	k5eNaImIp3nS	vědět
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
právě	právě	k9	právě
v	v	k7c6	v
příští	příští	k2eAgFnSc6d1	příští
minutě	minuta	k1gFnSc6	minuta
nevyhraje	vyhrát	k5eNaPmIp3nS	vyhrát
závratnou	závratný	k2eAgFnSc4d1	závratná
výhru	výhra	k1gFnSc4	výhra
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
Facebooku	Facebook	k1gInSc2	Facebook
je	být	k5eAaImIp3nS	být
tou	ten	k3xDgFnSc7	ten
malou	malý	k2eAgFnSc7d1	malá
odměnou	odměna	k1gFnSc7	odměna
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jiný	jiný	k2eAgMnSc1d1	jiný
uživatel	uživatel	k1gMnSc1	uživatel
dá	dát	k5eAaPmIp3nS	dát
uživateli	uživatel	k1gMnSc3	uživatel
lajk	lajk	k6eAd1	lajk
<g/>
.	.	kIx.	.
</s>
<s>
Uživatel	uživatel	k1gMnSc1	uživatel
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
dostal	dostat	k5eAaPmAgMnS	dostat
lajk	lajk	k1gInSc4	lajk
<g/>
,	,	kIx,	,
chce	chtít	k5eAaImIp3nS	chtít
uspokojení	uspokojení	k1gNnSc4	uspokojení
ze	z	k7c2	z
získání	získání	k1gNnSc2	získání
lajku	lajka	k1gFnSc4	lajka
zopakovat	zopakovat	k5eAaPmF	zopakovat
<g/>
.	.	kIx.	.
</s>
<s>
Lajk	Lajk	k6eAd1	Lajk
je	být	k5eAaImIp3nS	být
zpětná	zpětný	k2eAgFnSc1d1	zpětná
vazba	vazba	k1gFnSc1	vazba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
motivuje	motivovat	k5eAaBmIp3nS	motivovat
uživatele	uživatel	k1gMnPc4	uživatel
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
na	na	k7c4	na
Facebook	Facebook	k1gInSc4	Facebook
nahrál	nahrát	k5eAaPmAgMnS	nahrát
více	hodně	k6eAd2	hodně
obsahu	obsah	k1gInSc2	obsah
a	a	k8xC	a
dostal	dostat	k5eAaPmAgMnS	dostat
tak	tak	k6eAd1	tak
více	hodně	k6eAd2	hodně
lajků	lajek	k1gMnPc2	lajek
a	a	k8xC	a
více	hodně	k6eAd2	hodně
komentářů	komentář	k1gInPc2	komentář
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
Parkera	Parkero	k1gNnSc2	Parkero
zakladatelé	zakladatel	k1gMnPc1	zakladatel
Facebooku	Facebook	k1gInSc2	Facebook
(	(	kIx(	(
<g/>
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
<g/>
,	,	kIx,	,
Mark	Mark	k1gMnSc1	Mark
Zuckerberg	Zuckerberg	k1gMnSc1	Zuckerberg
a	a	k8xC	a
Kevin	Kevin	k1gMnSc1	Kevin
Systrom	Systrom	k1gInSc4	Systrom
z	z	k7c2	z
Instagramu	Instagram	k1gInSc2	Instagram
<g/>
)	)	kIx)	)
využili	využít	k5eAaPmAgMnP	využít
zranitelnosti	zranitelnost	k1gFnPc4	zranitelnost
lidské	lidský	k2eAgFnSc2d1	lidská
psychiky	psychika	k1gFnSc2	psychika
a	a	k8xC	a
přesně	přesně	k6eAd1	přesně
věděli	vědět	k5eAaImAgMnP	vědět
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
dělají	dělat	k5eAaImIp3nP	dělat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Doporučování	doporučování	k1gNnSc1	doporučování
přátel	přítel	k1gMnPc2	přítel
===	===	k?	===
</s>
</p>
<p>
<s>
Způsob	způsob	k1gInSc1	způsob
hledání	hledání	k1gNnSc2	hledání
potenciálních	potenciální	k2eAgMnPc2d1	potenciální
přátel	přítel	k1gMnPc2	přítel
Facebookem	Facebook	k1gMnSc7	Facebook
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
neveřejného	veřejný	k2eNgInSc2d1	neveřejný
algoritmu	algoritmus	k1gInSc2	algoritmus
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
řada	řada	k1gFnSc1	řada
mechanismů	mechanismus	k1gInPc2	mechanismus
byla	být	k5eAaImAgFnS	být
již	již	k6eAd1	již
odhalena	odhalit	k5eAaPmNgFnS	odhalit
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
není	být	k5eNaImIp3nS	být
jestli	jestli	k8xS	jestli
ještě	ještě	k6eAd1	ještě
fungují	fungovat	k5eAaImIp3nP	fungovat
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
systém	systém	k1gInSc1	systém
je	být	k5eAaImIp3nS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
pravděpodobnosti	pravděpodobnost	k1gFnSc6	pravděpodobnost
<g/>
.	.	kIx.	.
<g/>
Facebook	Facebook	k1gInSc1	Facebook
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
dohledávání	dohledávání	k1gNnSc3	dohledávání
následující	následující	k2eAgInPc1d1	následující
údaje	údaj	k1gInPc1	údaj
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
<s>
základem	základ	k1gInSc7	základ
pro	pro	k7c4	pro
vyhledávání	vyhledávání	k1gNnSc4	vyhledávání
potenciálních	potenciální	k2eAgMnPc2d1	potenciální
přátel	přítel	k1gMnPc2	přítel
jsou	být	k5eAaImIp3nP	být
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
škole	škola	k1gFnSc6	škola
nebo	nebo	k8xC	nebo
o	o	k7c6	o
zaměstnání	zaměstnání	k1gNnSc6	zaměstnání
<g/>
,	,	kIx,	,
<g/>
uživatel	uživatel	k1gMnSc1	uživatel
nemusí	muset	k5eNaImIp3nS	muset
zadat	zadat	k5eAaPmF	zadat
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
škole	škola	k1gFnSc6	škola
sám	sám	k3xTgMnSc1	sám
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
zadá	zadat	k5eAaPmIp3nS	zadat
školu	škola	k1gFnSc4	škola
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
profilu	profil	k1gInSc2	profil
například	například	k6eAd1	například
sourozenec	sourozenec	k1gMnSc1	sourozenec
uživatele	uživatel	k1gMnSc2	uživatel
<g/>
,	,	kIx,	,
Facebook	Facebook	k1gInSc4	Facebook
uživatele	uživatel	k1gMnSc2	uživatel
se	s	k7c7	s
školou	škola	k1gFnSc7	škola
propojí	propojit	k5eAaPmIp3nS	propojit
také	také	k9	také
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
pokud	pokud	k8xS	pokud
má	mít	k5eAaImIp3nS	mít
Facebook	Facebook	k1gInSc1	Facebook
přístup	přístup	k1gInSc1	přístup
k	k	k7c3	k
emailové	emailový	k2eAgFnSc3d1	emailová
schránce	schránka	k1gFnSc3	schránka
uživatele	uživatel	k1gMnSc2	uživatel
<g/>
,	,	kIx,	,
získá	získat	k5eAaPmIp3nS	získat
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
všechny	všechen	k3xTgInPc4	všechen
emaily	email	k1gInPc4	email
a	a	k8xC	a
porovná	porovnat	k5eAaPmIp3nS	porovnat
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
emaily	email	k1gInPc7	email
ostatních	ostatní	k2eAgMnPc2d1	ostatní
uživatelů	uživatel	k1gMnPc2	uživatel
<g/>
,	,	kIx,	,
<g/>
Facebook	Facebook	k1gInSc1	Facebook
může	moct	k5eAaImIp3nS	moct
získat	získat	k5eAaPmF	získat
email	email	k1gInSc4	email
uživatele	uživatel	k1gMnSc2	uživatel
od	od	k7c2	od
jiného	jiný	k2eAgMnSc2d1	jiný
uživatele	uživatel	k1gMnSc2	uživatel
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
pokud	pokud	k8xS	pokud
uživatel	uživatel	k1gMnSc1	uživatel
povolí	povolit	k5eAaPmIp3nS	povolit
přístup	přístup	k1gInSc4	přístup
Facebooku	Facebook	k1gInSc2	Facebook
ke	k	k7c3	k
kontaktům	kontakt	k1gInPc3	kontakt
v	v	k7c6	v
seznamu	seznam	k1gInSc6	seznam
v	v	k7c6	v
mobilním	mobilní	k2eAgInSc6d1	mobilní
telefonu	telefon	k1gInSc6	telefon
<g/>
,	,	kIx,	,
Facebook	Facebook	k1gInSc1	Facebook
získá	získat	k5eAaPmIp3nS	získat
telefonní	telefonní	k2eAgNnPc4d1	telefonní
čísla	číslo	k1gNnPc4	číslo
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
porovnává	porovnávat	k5eAaImIp3nS	porovnávat
s	s	k7c7	s
telefonními	telefonní	k2eAgNnPc7d1	telefonní
čísly	číslo	k1gNnPc7	číslo
ostatních	ostatní	k2eAgMnPc2d1	ostatní
uživatelů	uživatel	k1gMnPc2	uživatel
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
využitím	využití	k1gNnSc7	využití
štítkování	štítkování	k1gNnSc2	štítkování
a	a	k8xC	a
rozpoznávání	rozpoznávání	k1gNnSc2	rozpoznávání
obličejů	obličej	k1gInPc2	obličej
na	na	k7c6	na
fotografiích	fotografia	k1gFnPc6	fotografia
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
větší	veliký	k2eAgInSc1d2	veliký
počet	počet	k1gInSc1	počet
společných	společný	k2eAgFnPc2d1	společná
známých	známá	k1gFnPc2	známá
s	s	k7c7	s
jiným	jiný	k2eAgMnSc7d1	jiný
uživatelem	uživatel	k1gMnSc7	uživatel
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
lidé	člověk	k1gMnPc1	člověk
navštěvující	navštěvující	k2eAgInSc4d1	navštěvující
profil	profil	k1gInSc4	profil
uživatele	uživatel	k1gMnSc2	uživatel
<g/>
,	,	kIx,	,
vyhledávání	vyhledávání	k1gNnSc4	vyhledávání
jména	jméno	k1gNnSc2	jméno
daného	daný	k2eAgMnSc4d1	daný
uživatele	uživatel	k1gMnSc4	uživatel
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
účast	účast	k1gFnSc1	účast
ve	v	k7c6	v
skupinách	skupina	k1gFnPc6	skupina
nebo	nebo	k8xC	nebo
událostech	událost	k1gFnPc6	událost
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
nabízení	nabízení	k1gNnSc1	nabízení
přátel	přítel	k1gMnPc2	přítel
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
uživatel	uživatel	k1gMnSc1	uživatel
zablokoval	zablokovat	k5eAaPmAgMnS	zablokovat
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
využitím	využití	k1gNnSc7	využití
samotné	samotný	k2eAgFnSc2d1	samotná
služby	služba	k1gFnSc2	služba
navrhování	navrhování	k1gNnSc2	navrhování
přátel	přítel	k1gMnPc2	přítel
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Facebook	Facebook	k1gInSc1	Facebook
sleduje	sledovat	k5eAaImIp3nS	sledovat
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
jak	jak	k6eAd1	jak
uživatel	uživatel	k1gMnSc1	uživatel
na	na	k7c4	na
tyto	tento	k3xDgInPc4	tento
návrhy	návrh	k1gInPc4	návrh
přátel	přítel	k1gMnPc2	přítel
reaguje	reagovat	k5eAaBmIp3nS	reagovat
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
navžení	navžení	k1gNnSc1	navžení
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
stejné	stejný	k2eAgInPc4d1	stejný
zájmy	zájem	k1gInPc4	zájem
(	(	kIx(	(
<g/>
např.	např.	kA	např.
poslouchají	poslouchat	k5eAaImIp3nP	poslouchat
stejnou	stejný	k2eAgFnSc4d1	stejná
hudbu	hudba	k1gFnSc4	hudba
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
fanoušci	fanoušek	k1gMnPc1	fanoušek
konkrétního	konkrétní	k2eAgInSc2d1	konkrétní
fotbalového	fotbalový	k2eAgInSc2d1	fotbalový
klubu	klub	k1gInSc2	klub
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
uživatel	uživatel	k1gMnSc1	uživatel
má	mít	k5eAaImIp3nS	mít
větší	veliký	k2eAgInSc1d2	veliký
počet	počet	k1gInSc1	počet
přátel	přítel	k1gMnPc2	přítel
s	s	k7c7	s
těmito	tento	k3xDgInPc7	tento
zájmy	zájem	k1gInPc7	zájem
<g/>
.	.	kIx.	.
<g/>
Facebook	Facebook	k1gInSc1	Facebook
popírá	popírat	k5eAaImIp3nS	popírat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
k	k	k7c3	k
navrhování	navrhování	k1gNnSc3	navrhování
přátel	přítel	k1gMnPc2	přítel
používal	používat	k5eAaImAgInS	používat
data	datum	k1gNnPc4	datum
od	od	k7c2	od
ostatních	ostatní	k2eAgInPc2d1	ostatní
webů	web	k1gInPc2	web
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
služeb	služba	k1gFnPc2	služba
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterým	který	k3yQgMnPc3	který
uživatelé	uživatel	k1gMnPc1	uživatel
dali	dát	k5eAaPmAgMnP	dát
Facebooku	Facebook	k1gInSc2	Facebook
přístup	přístup	k1gInSc1	přístup
<g/>
.	.	kIx.	.
<g/>
Několik	několik	k4yIc4	několik
hodin	hodina	k1gFnPc2	hodina
po	po	k7c6	po
té	ten	k3xDgFnSc6	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
Facebook	Facebook	k1gInSc4	Facebook
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
používání	používání	k1gNnSc3	používání
geolokačních	geolokační	k2eAgNnPc2d1	geolokační
dat	datum	k1gNnPc2	datum
pro	pro	k7c4	pro
navrhování	navrhování	k1gNnSc4	navrhování
přátel	přítel	k1gMnPc2	přítel
<g/>
,	,	kIx,	,
používání	používání	k1gNnSc1	používání
geolokačních	geolokační	k2eAgNnPc2d1	geolokační
dat	datum	k1gNnPc2	datum
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
účel	účel	k1gInSc4	účel
popřel	popřít	k5eAaPmAgMnS	popřít
<g/>
.	.	kIx.	.
<g/>
Protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
vše	všechen	k3xTgNnSc1	všechen
založené	založený	k2eAgNnSc1d1	založené
na	na	k7c4	na
pravděpodobnosti	pravděpodobnost	k1gFnPc4	pravděpodobnost
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
stát	stát	k5eAaImF	stát
případy	případ	k1gInPc4	případ
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
případ	případ	k1gInSc1	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
muž	muž	k1gMnSc1	muž
odešel	odejít	k5eAaPmAgMnS	odejít
od	od	k7c2	od
manželky	manželka	k1gFnSc2	manželka
kvůli	kvůli	k7c3	kvůli
milence	milenka	k1gFnSc3	milenka
a	a	k8xC	a
Facebook	Facebook	k1gInSc1	Facebook
navrhl	navrhnout	k5eAaPmAgInS	navrhnout
tuto	tento	k3xDgFnSc4	tento
milenku	milenka	k1gFnSc4	milenka
jako	jako	k9	jako
přítelkyni	přítelkyně	k1gFnSc4	přítelkyně
manželce	manželka	k1gFnSc3	manželka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Technické	technický	k2eAgInPc1d1	technický
detaily	detail	k1gInPc1	detail
==	==	k?	==
</s>
</p>
<p>
<s>
Jádro	jádro	k1gNnSc1	jádro
Facebooku	Facebook	k1gInSc2	Facebook
je	být	k5eAaImIp3nS	být
postaveno	postavit	k5eAaPmNgNnS	postavit
na	na	k7c6	na
softwarovém	softwarový	k2eAgInSc6d1	softwarový
balíku	balík	k1gInSc6	balík
LAMP	lampa	k1gFnPc2	lampa
(	(	kIx(	(
<g/>
Linux	linux	k1gInSc1	linux
<g/>
,	,	kIx,	,
Apache	Apache	k1gFnSc1	Apache
<g/>
,	,	kIx,	,
MySQL	MySQL	k1gFnSc1	MySQL
<g/>
,	,	kIx,	,
PHP	PHP	kA	PHP
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
ale	ale	k9	ale
používá	používat	k5eAaImIp3nS	používat
mnoho	mnoho	k4c4	mnoho
dalších	další	k2eAgFnPc2d1	další
technologií	technologie	k1gFnPc2	technologie
jako	jako	k9	jako
AJAX	AJAX	kA	AJAX
<g/>
,	,	kIx,	,
Java	Javum	k1gNnSc2	Javum
nebo	nebo	k8xC	nebo
Flash	Flasha	k1gFnPc2	Flasha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
tvorbu	tvorba	k1gFnSc4	tvorba
aplikací	aplikace	k1gFnPc2	aplikace
pro	pro	k7c4	pro
Facebook	Facebook	k1gInSc4	Facebook
byl	být	k5eAaImAgInS	být
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
jazyk	jazyk	k1gInSc1	jazyk
FBML	FBML	kA	FBML
(	(	kIx(	(
<g/>
Facebook	Facebook	k1gInSc1	Facebook
Markup	Markup	k1gInSc1	Markup
Language	language	k1gFnSc1	language
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
rozšířením	rozšíření	k1gNnSc7	rozšíření
HTML	HTML	kA	HTML
a	a	k8xC	a
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
konzistenci	konzistence	k1gFnSc4	konzistence
aplikací	aplikace	k1gFnPc2	aplikace
a	a	k8xC	a
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
možné	možný	k2eAgNnSc1d1	možné
používat	používat	k5eAaImF	používat
upravený	upravený	k2eAgInSc4d1	upravený
JavaScript	JavaScript	k1gInSc4	JavaScript
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
FBJS	FBJS	kA	FBJS
(	(	kIx(	(
<g/>
Facebook	Facebook	k1gInSc1	Facebook
JavaScript	JavaScript	k1gInSc1	JavaScript
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
dotazy	dotaz	k1gInPc4	dotaz
k	k	k7c3	k
databázi	databáze	k1gFnSc3	databáze
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
FQL	FQL	kA	FQL
(	(	kIx(	(
<g/>
Facebook	Facebook	k1gInSc1	Facebook
Query	Quera	k1gFnSc2	Quera
Language	language	k1gFnSc2	language
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
Facebook	Facebook	k1gInSc4	Facebook
pomocí	pomocí	k7c2	pomocí
svých	svůj	k3xOyFgFnPc2	svůj
developerských	developerský	k2eAgFnPc2d1	developerská
stránek	stránka	k1gFnPc2	stránka
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
některé	některý	k3yIgFnPc4	některý
svoje	svůj	k3xOyFgFnPc4	svůj
technologie	technologie	k1gFnPc4	technologie
pod	pod	k7c7	pod
různými	různý	k2eAgFnPc7d1	různá
licencemi	licence	k1gFnPc7	licence
k	k	k7c3	k
volnému	volný	k2eAgNnSc3d1	volné
užití	užití	k1gNnSc3	užití
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
bezpečnou	bezpečný	k2eAgFnSc4d1	bezpečná
výměnu	výměna	k1gFnSc4	výměna
souborů	soubor	k1gInPc2	soubor
mezi	mezi	k7c4	mezi
zaměnstnanci	zaměnstnance	k1gFnSc4	zaměnstnance
je	být	k5eAaImIp3nS	být
určen	určen	k2eAgInSc1d1	určen
server	server	k1gInSc1	server
files.fb.com	files.fb.com	k1gInSc1	files.fb.com
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yQgInSc6	který
běží	běžet	k5eAaImIp3nP	běžet
program	program	k1gInSc4	program
Accellion	Accellion	k1gInSc1	Accellion
File	File	k1gNnSc1	File
Transfer	transfer	k1gInSc1	transfer
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
ale	ale	k9	ale
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
chyby	chyba	k1gFnPc4	chyba
jako	jako	k8xC	jako
XSS	XSS	kA	XSS
<g/>
,	,	kIx,	,
Pre-Auth	Pre-Auth	k1gInSc1	Pre-Auth
SQL	SQL	kA	SQL
Injection	Injection	k1gInSc1	Injection
<g/>
,	,	kIx,	,
lokální	lokální	k2eAgFnSc1d1	lokální
eskalace	eskalace	k1gFnSc1	eskalace
oprávnění	oprávnění	k1gNnSc2	oprávnění
<g/>
,	,	kIx,	,
či	či	k8xC	či
chyby	chyba	k1gFnSc2	chyba
umožňující	umožňující	k2eAgNnSc4d1	umožňující
vzdálené	vzdálený	k2eAgNnSc4d1	vzdálené
spuštění	spuštění	k1gNnSc4	spuštění
kódu	kód	k1gInSc2	kód
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
server	server	k1gInSc4	server
se	se	k3xPyFc4	se
útočníkům	útočník	k1gMnPc3	útočník
podařilo	podařit	k5eAaPmAgNnS	podařit
nahrát	nahrát	k5eAaPmF	nahrát
dva	dva	k4xCgInPc4	dva
vlastní	vlastní	k2eAgInPc4d1	vlastní
webshelly	webshell	k1gInPc4	webshell
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Statistiky	statistika	k1gFnPc1	statistika
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Aktivních	aktivní	k2eAgMnPc2d1	aktivní
uživatelů	uživatel	k1gMnPc2	uživatel
<g/>
:	:	kIx,	:
2	[number]	k4	2
miliardy	miliarda	k4xCgFnSc2	miliarda
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
/	/	kIx~	/
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
<g/>
+	+	kIx~	+
</s>
</p>
<p>
<s>
Průměr	průměr	k1gInSc1	průměr
nových	nový	k2eAgMnPc2d1	nový
uživatelů	uživatel	k1gMnPc2	uživatel
za	za	k7c4	za
den	den	k1gInSc4	den
<g/>
:	:	kIx,	:
450 000	[number]	k4	450 000
</s>
</p>
<p>
<s>
Zhlédnuté	zhlédnutý	k2eAgFnPc1d1	zhlédnutá
stránky	stránka	k1gFnPc1	stránka
<g/>
:	:	kIx,	:
Přes	přes	k7c4	přes
70	[number]	k4	70
miliard	miliarda	k4xCgFnPc2	miliarda
za	za	k7c4	za
měsíc	měsíc	k1gInSc4	měsíc
</s>
</p>
<p>
<s>
Vyhledávání	vyhledávání	k1gNnSc1	vyhledávání
<g/>
:	:	kIx,	:
Přes	přes	k7c4	přes
500	[number]	k4	500
triliónů	trilión	k4xCgInPc2	trilión
za	za	k7c4	za
měsíc	měsíc	k1gInSc4	měsíc
</s>
</p>
<p>
<s>
Velikost	velikost	k1gFnSc1	velikost
indexu	index	k1gInSc2	index
vyhledávání	vyhledávání	k1gNnSc2	vyhledávání
<g/>
:	:	kIx,	:
200	[number]	k4	200
GB	GB	kA	GB
</s>
</p>
<p>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
země	země	k1gFnSc1	země
<g/>
:	:	kIx,	:
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgNnSc1d1	spojené
královstvíDalší	královstvíDalší	k1gNnSc1	královstvíDalší
země	zem	k1gFnSc2	zem
seřazené	seřazený	k2eAgFnSc2d1	seřazená
podle	podle	k7c2	podle
počtu	počet	k1gInSc2	počet
uživatelů	uživatel	k1gMnPc2	uživatel
<g/>
:	:	kIx,	:
Austrálie	Austrálie	k1gFnSc1	Austrálie
<g/>
,	,	kIx,	,
Turecko	Turecko	k1gNnSc1	Turecko
<g/>
,	,	kIx,	,
Švédsko	Švédsko	k1gNnSc1	Švédsko
<g/>
,	,	kIx,	,
Norsko	Norsko	k1gNnSc1	Norsko
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc1d1	jižní
Afrika	Afrika	k1gFnSc1	Afrika
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
Hong	Hong	k1gInSc1	Hong
Kong	Kongo	k1gNnPc2	Kongo
<g/>
.	.	kIx.	.
<g/>
Největší	veliký	k2eAgFnSc1d3	veliký
sociální	sociální	k2eAgFnSc1d1	sociální
sítě	síť	k1gFnPc1	síť
<g/>
:	:	kIx,	:
Londýn	Londýn	k1gInSc1	Londýn
<g/>
,	,	kIx,	,
UK	UK	kA	UK
<g/>
:	:	kIx,	:
1 760 596	[number]	k4	1 760 596
a	a	k8xC	a
Toronto	Toronto	k1gNnSc1	Toronto
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
:	:	kIx,	:
966 092	[number]	k4	966 092
</s>
</p>
<p>
<s>
Pořadí	pořadí	k1gNnSc1	pořadí
v	v	k7c6	v
návštěvnosti	návštěvnost	k1gFnSc6	návštěvnost
všech	všecek	k3xTgFnPc2	všecek
webových	webový	k2eAgFnPc2d1	webová
aplikací	aplikace	k1gFnPc2	aplikace
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
:	:	kIx,	:
2.	[number]	k4	2.
</s>
</p>
<p>
<s>
Fotografií	fotografia	k1gFnPc2	fotografia
<g/>
:	:	kIx,	:
140	[number]	k4	140
miliard	miliarda	k4xCgFnPc2	miliarda
fotografií	fotografia	k1gFnPc2	fotografia
(	(	kIx(	(
<g/>
denně	denně	k6eAd1	denně
se	se	k3xPyFc4	se
nahraje	nahrát	k5eAaBmIp3nS	nahrát
na	na	k7c4	na
Facebook	Facebook	k1gInSc4	Facebook
200	[number]	k4	200
milionů	milion	k4xCgInPc2	milion
fotografií	fotografia	k1gFnPc2	fotografia
<g/>
)	)	kIx)	)
<g/>
Podle	podle	k7c2	podle
studie	studie	k1gFnSc2	studie
doktorandů	doktorand	k1gMnPc2	doktorand
z	z	k7c2	z
Princetonu	Princeton	k1gInSc2	Princeton
má	mít	k5eAaImIp3nS	mít
Facebook	Facebook	k1gInSc1	Facebook
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
2015	[number]	k4	2015
a	a	k8xC	a
2017	[number]	k4	2017
přijít	přijít	k5eAaPmF	přijít
až	až	k9	až
o	o	k7c4	o
80	[number]	k4	80
%	%	kIx~	%
uživatelů	uživatel	k1gMnPc2	uživatel
<g/>
,	,	kIx,	,
výzkumníci	výzkumník	k1gMnPc1	výzkumník
své	svůj	k3xOyFgFnSc2	svůj
závěry	závěra	k1gFnSc2	závěra
o	o	k7c6	o
vzestupech	vzestup	k1gInPc6	vzestup
a	a	k8xC	a
pádech	pád	k1gInPc6	pád
sociálních	sociální	k2eAgFnPc2d1	sociální
sítí	síť	k1gFnPc2	síť
odvodili	odvodit	k5eAaPmAgMnP	odvodit
od	od	k7c2	od
případu	případ	k1gInSc2	případ
sociální	sociální	k2eAgFnSc2d1	sociální
sítě	síť	k1gFnSc2	síť
MySpace	MySpace	k1gFnSc2	MySpace
a	a	k8xC	a
aplikovali	aplikovat	k5eAaBmAgMnP	aplikovat
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
model	model	k1gInSc4	model
šíření	šíření	k1gNnSc4	šíření
nakažlivých	nakažlivý	k2eAgFnPc2d1	nakažlivá
chorob	choroba	k1gFnPc2	choroba
<g/>
.	.	kIx.	.
</s>
<s>
Data	datum	k1gNnPc1	datum
o	o	k7c6	o
vyhledávání	vyhledávání	k1gNnSc6	vyhledávání
názvu	název	k1gInSc2	název
sociálních	sociální	k2eAgFnPc2d1	sociální
sítí	síť	k1gFnPc2	síť
čerpali	čerpat	k5eAaImAgMnP	čerpat
ze	z	k7c2	z
služby	služba	k1gFnSc2	služba
Google	Google	k1gFnSc2	Google
Trends	Trendsa	k1gFnPc2	Trendsa
<g/>
.	.	kIx.	.
</s>
<s>
Výsledky	výsledek	k1gInPc1	výsledek
výzkumu	výzkum	k1gInSc2	výzkum
byly	být	k5eAaImAgInP	být
po	po	k7c4	po
zveřejnění	zveřejnění	k1gNnSc4	zveřejnění
podrobeny	podroben	k2eAgMnPc4d1	podroben
kritice	kritika	k1gFnSc6	kritika
vzhledem	vzhled	k1gInSc7	vzhled
k	k	k7c3	k
zjednodušení	zjednodušení	k1gNnSc3	zjednodušení
problému	problém	k1gInSc2	problém
–	–	k?	–
významná	významný	k2eAgFnSc1d1	významná
část	část	k1gFnSc1	část
uživatelů	uživatel	k1gMnPc2	uživatel
při	při	k7c6	při
opakovaných	opakovaný	k2eAgFnPc6d1	opakovaná
návštěvách	návštěva	k1gFnPc6	návštěva
sociální	sociální	k2eAgFnSc2d1	sociální
sítě	síť	k1gFnSc2	síť
již	již	k6eAd1	již
adresu	adresa	k1gFnSc4	adresa
nemusí	muset	k5eNaImIp3nS	muset
vyhledávat	vyhledávat	k5eAaImF	vyhledávat
(	(	kIx(	(
<g/>
pamatuje	pamatovat	k5eAaImIp3nS	pamatovat
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
sám	sám	k3xTgMnSc1	sám
uživatel	uživatel	k1gMnSc1	uživatel
či	či	k8xC	či
jeho	jeho	k3xOp3gMnSc1	jeho
webový	webový	k2eAgMnSc1d1	webový
prohlížeč	prohlížeč	k1gMnSc1	prohlížeč
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
uživatel	uživatel	k1gMnSc1	uživatel
se	se	k3xPyFc4	se
k	k	k7c3	k
sociální	sociální	k2eAgFnSc3d1	sociální
síti	síť	k1gFnSc3	síť
připojuje	připojovat	k5eAaImIp3nS	připojovat
pomocí	pomocí	k7c2	pomocí
aplikace	aplikace	k1gFnSc2	aplikace
na	na	k7c6	na
chytrém	chytrý	k2eAgInSc6d1	chytrý
telefonu	telefon	k1gInSc6	telefon
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
název	název	k1gInSc1	název
sítě	síť	k1gFnSc2	síť
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
vyhledávače	vyhledávač	k1gMnSc2	vyhledávač
Google	Googl	k1gMnSc2	Googl
nehledá	hledat	k5eNaImIp3nS	hledat
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
Facebook	Facebook	k1gInSc4	Facebook
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
analýzu	analýza	k1gFnSc4	analýza
zareagovala	zareagovat	k5eAaPmAgFnS	zareagovat
podobnou	podobný	k2eAgFnSc7d1	podobná
zprávou	zpráva	k1gFnSc7	zpráva
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pomocí	pomocí	k7c2	pomocí
počtu	počet	k1gInSc2	počet
"	"	kIx"	"
<g/>
lajků	lajek	k1gMnPc2	lajek
<g/>
"	"	kIx"	"
facebookových	facebookový	k2eAgFnPc6d1	facebooková
stránkách	stránka	k1gFnPc6	stránka
univerzit	univerzita	k1gFnPc2	univerzita
či	či	k8xC	či
dat	datum	k1gNnPc2	datum
z	z	k7c2	z
Google	Google	k1gFnSc2	Google
Scholar	Scholar	k1gInSc1	Scholar
vyvodil	vyvodit	k5eAaPmAgMnS	vyvodit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Princetonu	Princeton	k1gInSc2	Princeton
bude	být	k5eAaImBp3nS	být
ubývat	ubývat	k5eAaImF	ubývat
počet	počet	k1gInSc1	počet
studentů	student	k1gMnPc2	student
<g/>
,	,	kIx,	,
až	až	k9	až
univerzita	univerzita	k1gFnSc1	univerzita
zcela	zcela	k6eAd1	zcela
zanikne	zaniknout	k5eAaPmIp3nS	zaniknout
<g/>
.	.	kIx.	.
</s>
<s>
Facebook	Facebook	k1gInSc1	Facebook
dále	daleko	k6eAd2	daleko
poukázal	poukázat	k5eAaPmAgInS	poukázat
na	na	k7c4	na
snižující	snižující	k2eAgFnSc4d1	snižující
se	se	k3xPyFc4	se
frekvenci	frekvence	k1gFnSc4	frekvence
vyhledávání	vyhledávání	k1gNnSc4	vyhledávání
slova	slovo	k1gNnSc2	slovo
"	"	kIx"	"
<g/>
air	air	k?	air
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
vzduch	vzduch	k1gInSc1	vzduch
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yQnSc2	což
po	po	k7c6	po
vzoru	vzor	k1gInSc6	vzor
princetonské	princetonský	k2eAgFnSc2d1	princetonský
studie	studie	k1gFnSc2	studie
spočítal	spočítat	k5eAaPmAgMnS	spočítat
<g/>
,	,	kIx,	,
že	že	k8xS	že
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
2060	[number]	k4	2060
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
žádný	žádný	k3yNgInSc1	žádný
vzduch	vzduch	k1gInSc1	vzduch
nebude	být	k5eNaImBp3nS	být
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Informační	informační	k2eAgNnSc4d1	informační
soukromí	soukromí	k1gNnSc4	soukromí
uživatelů	uživatel	k1gMnPc2	uživatel
Facebooku	Facebook	k1gInSc2	Facebook
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Problémy	problém	k1gInPc1	problém
Facebooku	Facebook	k1gInSc2	Facebook
vyplývají	vyplývat	k5eAaImIp3nP	vyplývat
z	z	k7c2	z
koncentrace	koncentrace	k1gFnSc2	koncentrace
citlivých	citlivý	k2eAgNnPc2d1	citlivé
osobních	osobní	k2eAgNnPc2d1	osobní
dat	datum	k1gNnPc2	datum
a	a	k8xC	a
spousty	spousta	k1gFnSc2	spousta
aplikací	aplikace	k1gFnPc2	aplikace
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
k	k	k7c3	k
nim	on	k3xPp3gMnPc3	on
mají	mít	k5eAaImIp3nP	mít
přístup	přístup	k1gInSc4	přístup
<g/>
.	.	kIx.	.
</s>
<s>
Aplikace	aplikace	k1gFnSc1	aplikace
může	moct	k5eAaImIp3nS	moct
napsat	napsat	k5eAaPmF	napsat
kdokoli	kdokoli	k3yInSc1	kdokoli
a	a	k8xC	a
poté	poté	k6eAd1	poté
data	datum	k1gNnPc4	datum
použít	použít	k5eAaPmF	použít
ke	k	k7c3	k
svým	svůj	k3xOyFgInPc3	svůj
účelům	účel	k1gInPc3	účel
<g/>
.	.	kIx.	.
</s>
<s>
Facebook	Facebook	k1gInSc1	Facebook
byl	být	k5eAaImAgInS	být
zakázán	zakázat	k5eAaPmNgInS	zakázat
v	v	k7c6	v
některých	některý	k3yIgNnPc6	některý
pracovištích	pracoviště	k1gNnPc6	pracoviště
a	a	k8xC	a
školách	škola	k1gFnPc6	škola
pro	pro	k7c4	pro
sdílení	sdílení	k1gNnSc4	sdílení
nevhodného	vhodný	k2eNgInSc2d1	nevhodný
obsahu	obsah	k1gInSc2	obsah
<g/>
.	.	kIx.	.
</s>
<s>
Facebook	Facebook	k1gInSc1	Facebook
je	být	k5eAaImIp3nS	být
blokován	blokovat	k5eAaImNgInS	blokovat
například	například	k6eAd1	například
v	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
údajně	údajně	k6eAd1	údajně
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
pomlouvání	pomlouvání	k1gNnSc2	pomlouvání
vládních	vládní	k2eAgFnPc2d1	vládní
autorit	autorita	k1gFnPc2	autorita
přes	přes	k7c4	přes
Facebook	Facebook	k1gInSc4	Facebook
<g/>
.	.	kIx.	.
</s>
<s>
Facebook	Facebook	k1gInSc1	Facebook
také	také	k9	také
nechtěně	chtěně	k6eNd1	chtěně
umožnil	umožnit	k5eAaPmAgInS	umožnit
pomocí	pomocí	k7c2	pomocí
aplikace	aplikace	k1gFnSc2	aplikace
Beacon	Beacon	k1gInSc1	Beacon
ostatním	ostatní	k2eAgMnPc3d1	ostatní
přátelům	přítel	k1gMnPc3	přítel
zjistit	zjistit	k5eAaPmF	zjistit
<g/>
,	,	kIx,	,
co	co	k9	co
daný	daný	k2eAgMnSc1d1	daný
uživatel	uživatel	k1gMnSc1	uživatel
nakupuje	nakupovat	k5eAaBmIp3nS	nakupovat
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
<g/>
.	.	kIx.	.
</s>
<s>
Julian	Julian	k1gMnSc1	Julian
Assange	Assang	k1gInSc2	Assang
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Facebook	Facebook	k1gInSc1	Facebook
a	a	k8xC	a
Google	Google	k1gFnSc1	Google
mají	mít	k5eAaImIp3nP	mít
tajné	tajný	k2eAgInPc1d1	tajný
projekty	projekt	k1gInPc1	projekt
na	na	k7c6	na
sledování	sledování	k1gNnSc6	sledování
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
jen	jen	k9	jen
podle	podle	k7c2	podle
lajků	lajk	k1gInPc2	lajk
lze	lze	k6eAd1	lze
statisticky	statisticky	k6eAd1	statisticky
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
určit	určit	k5eAaPmF	určit
pohlaví	pohlaví	k1gNnSc4	pohlaví
<g/>
,	,	kIx,	,
náboženské	náboženský	k2eAgNnSc4d1	náboženské
vyznání	vyznání	k1gNnSc4	vyznání
<g/>
,	,	kIx,	,
rasu	rasa	k1gFnSc4	rasa
<g/>
,	,	kIx,	,
atp	atp	kA	atp
<g/>
.	.	kIx.	.
<g/>
Samotné	samotný	k2eAgFnPc1d1	samotná
podmínky	podmínka	k1gFnPc1	podmínka
užití	užití	k1gNnSc2	užití
Facebooku	Facebook	k1gInSc2	Facebook
velmi	velmi	k6eAd1	velmi
zasahují	zasahovat	k5eAaImIp3nP	zasahovat
do	do	k7c2	do
práv	právo	k1gNnPc2	právo
a	a	k8xC	a
soukromí	soukromí	k1gNnPc2	soukromí
uživatelů	uživatel	k1gMnPc2	uživatel
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
oprávnění	oprávnění	k1gNnSc4	oprávnění
na	na	k7c4	na
použití	použití	k1gNnSc4	použití
veškerého	veškerý	k3xTgInSc2	veškerý
obsahu	obsah	k1gInSc2	obsah
podléhajícího	podléhající	k2eAgInSc2d1	podléhající
duševnímu	duševní	k2eAgNnSc3d1	duševní
vlastnictví	vlastnictví	k1gNnSc3	vlastnictví
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
<g />
.	.	kIx.	.
</s>
<s>
uživatel	uživatel	k1gMnSc1	uživatel
zveřejní	zveřejnit	k5eAaPmIp3nS	zveřejnit
na	na	k7c6	na
Facebooku	Facebook	k1gInSc6	Facebook
nebo	nebo	k8xC	nebo
v	v	k7c6	v
návaznosti	návaznost	k1gFnSc6	návaznost
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
souhlas	souhlas	k1gInSc4	souhlas
s	s	k7c7	s
přenesením	přenesení	k1gNnSc7	přenesení
a	a	k8xC	a
zpracováním	zpracování	k1gNnSc7	zpracování
osobních	osobní	k2eAgInPc2d1	osobní
údajů	údaj	k1gInPc2	údaj
uživatele	uživatel	k1gMnPc4	uživatel
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgFnPc2d1	americká
<g/>
,	,	kIx,	,
povolení	povolení	k1gNnSc4	povolení
použít	použít	k5eAaPmF	použít
jméno	jméno	k1gNnSc4	jméno
uživatele	uživatel	k1gMnSc2	uživatel
a	a	k8xC	a
profilové	profilový	k2eAgFnSc2d1	profilová
fotografie	fotografia	k1gFnSc2	fotografia
ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
s	s	k7c7	s
komerčním	komerční	k2eAgMnSc7d1	komerční
<g/>
,	,	kIx,	,
sponzorovaným	sponzorovaný	k2eAgMnSc7d1	sponzorovaný
nebo	nebo	k8xC	nebo
souvisejícím	související	k2eAgInSc7d1	související
obsahem	obsah	k1gInSc7	obsah
a	a	k8xC	a
souhlas	souhlas	k1gInSc4	souhlas
s	s	k7c7	s
případným	případný	k2eAgNnSc7d1	případné
odškodněním	odškodnění	k1gNnSc7	odškodnění
Facebooku	Facebook	k1gInSc2	Facebook
za	za	k7c4	za
veškeré	veškerý	k3xTgMnPc4	veškerý
<g />
.	.	kIx.	.
</s>
<s>
škody	škoda	k1gFnPc1	škoda
<g/>
,	,	kIx,	,
ztráty	ztráta	k1gFnPc1	ztráta
a	a	k8xC	a
výdaje	výdaj	k1gInPc1	výdaj
jakéhokoli	jakýkoli	k3yIgInSc2	jakýkoli
druhu	druh	k1gInSc2	druh
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
přiměřených	přiměřený	k2eAgInPc2d1	přiměřený
poplatků	poplatek	k1gInPc2	poplatek
a	a	k8xC	a
nákladů	náklad	k1gInPc2	náklad
na	na	k7c4	na
právní	právní	k2eAgNnSc4d1	právní
zastoupení	zastoupení	k1gNnSc4	zastoupení
<g/>
)	)	kIx)	)
pokud	pokud	k8xS	pokud
vůči	vůči	k7c3	vůči
Facebooku	Facebook	k1gInSc3	Facebook
vznese	vznést	k5eAaPmIp3nS	vznést
žalobu	žaloba	k1gFnSc4	žaloba
související	související	k2eAgFnSc4d1	související
s	s	k7c7	s
kroky	krok	k1gInPc1	krok
dotyčného	dotyčný	k2eAgMnSc2d1	dotyčný
uživatele	uživatel	k1gMnSc2	uživatel
<g/>
.	.	kIx.	.
<g/>
I	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
uživatelé	uživatel	k1gMnPc1	uživatel
nastaví	nastavět	k5eAaBmIp3nP	nastavět
omezenou	omezený	k2eAgFnSc4d1	omezená
viditelnost	viditelnost	k1gFnSc4	viditelnost
svého	svůj	k3xOyFgNnSc2	svůj
telefonního	telefonní	k2eAgNnSc2d1	telefonní
čísla	číslo	k1gNnSc2	číslo
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
podle	podle	k7c2	podle
svého	svůj	k3xOyFgNnSc2	svůj
telefonního	telefonní	k2eAgNnSc2d1	telefonní
čísla	číslo	k1gNnSc2	číslo
dohledatelný	dohledatelný	k2eAgInSc1d1	dohledatelný
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
volba	volba	k1gFnSc1	volba
"	"	kIx"	"
<g/>
Kdo	kdo	k3yInSc1	kdo
vás	vy	k3xPp2nPc4	vy
může	moct	k5eAaImIp3nS	moct
vyhledat	vyhledat	k5eAaPmF	vyhledat
pomocí	pomocí	k7c2	pomocí
telefonního	telefonní	k2eAgNnSc2d1	telefonní
čísla	číslo	k1gNnSc2	číslo
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
jste	být	k5eAaImIp2nP	být
zadali	zadat	k5eAaPmAgMnP	zadat
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
o	o	k7c4	o
které	který	k3yQgFnPc4	který
skoro	skoro	k6eAd1	skoro
nikdo	nikdo	k3yNnSc1	nikdo
neví	vědět	k5eNaImIp3nS	vědět
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nadřazená	nadřazený	k2eAgNnPc4d1	nadřazené
nastavení	nastavení	k1gNnPc4	nastavení
viditelnosti	viditelnost	k1gFnSc2	viditelnost
telefonního	telefonní	k2eAgNnSc2d1	telefonní
čísla	číslo	k1gNnSc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
volbu	volba	k1gFnSc4	volba
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
nastavit	nastavit	k5eAaPmF	nastavit
na	na	k7c6	na
"	"	kIx"	"
<g/>
Jenom	jenom	k9	jenom
já	já	k3xPp1nSc1	já
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Standardně	standardně	k6eAd1	standardně
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
volba	volba	k1gFnSc1	volba
nastavena	nastavit	k5eAaPmNgFnS	nastavit
na	na	k7c6	na
"	"	kIx"	"
<g/>
Všichni	všechen	k3xTgMnPc1	všechen
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Obvinění	obviněný	k1gMnPc1	obviněný
z	z	k7c2	z
nezákonného	zákonný	k2eNgNnSc2d1	nezákonné
nakládání	nakládání	k1gNnSc2	nakládání
s	s	k7c7	s
osobními	osobní	k2eAgInPc7d1	osobní
údaji	údaj	k1gInPc7	údaj
uživatelů	uživatel	k1gMnPc2	uživatel
===	===	k?	===
</s>
</p>
<p>
<s>
Kanadská	kanadský	k2eAgFnSc1d1	kanadská
komise	komise	k1gFnSc1	komise
pro	pro	k7c4	pro
ochranu	ochrana	k1gFnSc4	ochrana
soukromí	soukromí	k1gNnSc2	soukromí
zjistila	zjistit	k5eAaPmAgFnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Facebook	Facebook	k1gInSc1	Facebook
porušuje	porušovat	k5eAaImIp3nS	porušovat
kanadské	kanadský	k2eAgInPc4d1	kanadský
zákony	zákon	k1gInPc4	zákon
o	o	k7c6	o
shromažďování	shromažďování	k1gNnSc6	shromažďování
osobních	osobní	k2eAgInPc2d1	osobní
údajů	údaj	k1gInPc2	údaj
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
si	se	k3xPyFc3	se
provozovatel	provozovatel	k1gMnSc1	provozovatel
ponechává	ponechávat	k5eAaImIp3nS	ponechávat
i	i	k9	i
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
se	se	k3xPyFc4	se
uživatelé	uživatel	k1gMnPc1	uživatel
ze	z	k7c2	z
sítě	síť	k1gFnSc2	síť
odhlásí	odhlásit	k5eAaPmIp3nS	odhlásit
<g/>
.	.	kIx.	.
</s>
<s>
Komise	komise	k1gFnSc1	komise
dala	dát	k5eAaPmAgFnS	dát
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2009	[number]	k4	2009
provozovatelům	provozovatel	k1gMnPc3	provozovatel
měsíční	měsíční	k2eAgFnSc4d1	měsíční
lhůtu	lhůta	k1gFnSc4	lhůta
na	na	k7c4	na
přijetí	přijetí	k1gNnSc4	přijetí
opatření	opatření	k1gNnSc2	opatření
ke	k	k7c3	k
změnám	změna	k1gFnPc3	změna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Možnosti	možnost	k1gFnPc1	možnost
ochrany	ochrana	k1gFnSc2	ochrana
uživatelských	uživatelský	k2eAgNnPc2d1	Uživatelské
dat	datum	k1gNnPc2	datum
a	a	k8xC	a
soukromí	soukromý	k2eAgMnPc1d1	soukromý
===	===	k?	===
</s>
</p>
<p>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
rozšíření	rozšíření	k1gNnSc4	rozšíření
webových	webový	k2eAgInPc2d1	webový
prohlížečů	prohlížeč	k1gInPc2	prohlížeč
informující	informující	k2eAgFnSc4d1	informující
uživatele	uživatel	k1gMnSc2	uživatel
Facebooku	Facebook	k1gInSc2	Facebook
o	o	k7c6	o
úniku	únik	k1gInSc6	únik
osobních	osobní	k2eAgFnPc2d1	osobní
informací	informace	k1gFnPc2	informace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Spory	spor	k1gInPc1	spor
o	o	k7c4	o
ochranu	ochrana	k1gFnSc4	ochrana
osobních	osobní	k2eAgNnPc2d1	osobní
dat	datum	k1gNnPc2	datum
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc4	jejich
přenos	přenos	k1gInSc4	přenos
do	do	k7c2	do
USA	USA	kA	USA
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
podle	podle	k7c2	podle
Věry	Věra	k1gFnSc2	Věra
Jourové	Jourová	k1gFnSc2	Jourová
měl	mít	k5eAaImAgInS	mít
řešit	řešit	k5eAaImF	řešit
ÚOOÚ	ÚOOÚ	kA	ÚOOÚ
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Facebook	Facebook	k1gInSc1	Facebook
působí	působit	k5eAaImIp3nS	působit
i	i	k9	i
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Spolupráce	spolupráce	k1gFnSc1	spolupráce
s	s	k7c7	s
NSA	NSA	kA	NSA
===	===	k?	===
</s>
</p>
<p>
<s>
Facebook	Facebook	k1gInSc1	Facebook
spolupracoval	spolupracovat	k5eAaImAgInS	spolupracovat
s	s	k7c7	s
americkou	americký	k2eAgFnSc7d1	americká
Národní	národní	k2eAgFnSc7d1	národní
bezpečnostní	bezpečnostní	k2eAgFnSc7d1	bezpečnostní
agenturou	agentura	k1gFnSc7	agentura
(	(	kIx(	(
<g/>
NSA	NSA	kA	NSA
<g/>
)	)	kIx)	)
na	na	k7c6	na
tajném	tajný	k2eAgInSc6d1	tajný
sledovacím	sledovací	k2eAgInSc6d1	sledovací
programu	program	k1gInSc6	program
PRISM	PRISM	kA	PRISM
<g/>
.	.	kIx.	.
</s>
<s>
Informace	informace	k1gFnPc1	informace
o	o	k7c6	o
programu	program	k1gInSc6	program
unikly	uniknout	k5eAaPmAgFnP	uniknout
na	na	k7c4	na
veřejnost	veřejnost	k1gFnSc4	veřejnost
přičiněním	přičinění	k1gNnSc7	přičinění
Edwarda	Edward	k1gMnSc2	Edward
Snowdena	Snowden	k1gMnSc2	Snowden
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
externím	externí	k2eAgMnSc7d1	externí
spolupracovníkem	spolupracovník	k1gMnSc7	spolupracovník
Národní	národní	k2eAgFnSc2d1	národní
bezpečnostní	bezpečnostní	k2eAgFnSc2d1	bezpečnostní
agentury	agentura	k1gFnSc2	agentura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Ochrana	ochrana	k1gFnSc1	ochrana
prostředí	prostředí	k1gNnSc2	prostředí
Facebooku	Facebook	k1gInSc2	Facebook
==	==	k?	==
</s>
</p>
<p>
<s>
Facebook	Facebook	k1gInSc1	Facebook
blokuje	blokovat	k5eAaImIp3nS	blokovat
účty	účet	k1gInPc4	účet
osobám	osoba	k1gFnPc3	osoba
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
jeho	jeho	k3xOp3gNnSc1	jeho
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
šířily	šířit	k5eAaImAgFnP	šířit
nenávist	nenávist	k1gFnSc4	nenávist
a	a	k8xC	a
extremistické	extremistický	k2eAgInPc4d1	extremistický
názory	názor	k1gInPc4	názor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2019	[number]	k4	2019
tak	tak	k8xS	tak
např.	např.	kA	např.
Facebook	Facebook	k1gInSc1	Facebook
zablokoval	zablokovat	k5eAaPmAgInS	zablokovat
účty	účet	k1gInPc4	účet
několika	několik	k4yIc2	několik
prominentních	prominentní	k2eAgMnPc2d1	prominentní
uživatelů	uživatel	k1gMnPc2	uživatel
<g/>
,	,	kIx,	,
které	který	k3yQgMnPc4	který
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
nebezpečné	bezpečný	k2eNgInPc4d1	nebezpečný
-	-	kIx~	-
kontroverzního	kontroverzní	k2eAgMnSc4d1	kontroverzní
komentátora	komentátor	k1gMnSc4	komentátor
<g/>
,	,	kIx,	,
novináře	novinář	k1gMnPc4	novinář
a	a	k8xC	a
příznivce	příznivec	k1gMnPc4	příznivec
krajní	krajní	k2eAgFnSc2d1	krajní
pravice	pravice	k1gFnSc2	pravice
Mila	Milus	k1gMnSc4	Milus
Yiannopoulose	Yiannopoulosa	k1gFnSc6	Yiannopoulosa
<g/>
,	,	kIx,	,
majitele	majitel	k1gMnSc4	majitel
konspirátorského	konspirátorský	k2eAgInSc2d1	konspirátorský
portálu	portál	k1gInSc2	portál
InfoWars	InfoWarsa	k1gFnPc2	InfoWarsa
Alexe	Alex	k1gMnSc5	Alex
Jonese	Jonese	k1gFnSc2	Jonese
nebo	nebo	k8xC	nebo
Louise	Louis	k1gMnSc2	Louis
Farrakhana	Farrakhan	k1gMnSc2	Farrakhan
<g/>
,	,	kIx,	,
lídra	lídr	k1gMnSc2	lídr
černošského	černošský	k2eAgNnSc2d1	černošské
muslimského	muslimský	k2eAgNnSc2d1	muslimské
hnutí	hnutí	k1gNnSc2	hnutí
Islámský	islámský	k2eAgInSc4d1	islámský
národ	národ	k1gInSc4	národ
<g/>
,	,	kIx,	,
vnímaného	vnímaný	k2eAgNnSc2d1	vnímané
jako	jako	k8xS	jako
černošský	černošský	k2eAgInSc4d1	černošský
protipól	protipól	k1gInSc4	protipól
bělošského	bělošský	k2eAgNnSc2d1	bělošské
rasistického	rasistický	k2eAgNnSc2d1	rasistické
uskupení	uskupení	k1gNnSc2	uskupení
Ku-klux-klan	Kuluxlan	k1gInSc1	Ku-klux-klan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Eliminace	eliminace	k1gFnSc1	eliminace
falešných	falešný	k2eAgMnPc2d1	falešný
lajků	lajek	k1gMnPc2	lajek
a	a	k8xC	a
sledovatelů	sledovatel	k1gMnPc2	sledovatel
===	===	k?	===
</s>
</p>
<p>
<s>
Facebook	Facebook	k1gInSc1	Facebook
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2019	[number]	k4	2019
zažaloval	zažalovat	k5eAaPmAgInS	zažalovat
novozélandskou	novozélandský	k2eAgFnSc4d1	novozélandská
společnost	společnost	k1gFnSc4	společnost
Social	Social	k1gInSc1	Social
Media	medium	k1gNnSc2	medium
Series	Seriesa	k1gFnPc2	Seriesa
Limited	limited	k2eAgMnSc2d1	limited
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
již	již	k6eAd1	již
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
prodává	prodávat	k5eAaImIp3nS	prodávat
falešné	falešný	k2eAgFnSc2d1	falešná
lajky	lajka	k1gFnSc2	lajka
a	a	k8xC	a
sledovatele	sledovatel	k1gMnSc4	sledovatel
pomocí	pomocí	k7c2	pomocí
rozsáhlé	rozsáhlý	k2eAgFnSc2d1	rozsáhlá
sítě	síť	k1gFnSc2	síť
botů	bot	k1gInPc2	bot
a	a	k8xC	a
vlastních	vlastní	k2eAgInPc2d1	vlastní
účtů	účet	k1gInPc2	účet
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
ovládala	ovládat	k5eAaImAgFnS	ovládat
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
praktiky	praktika	k1gFnPc1	praktika
jsou	být	k5eAaImIp3nP	být
zakázané	zakázaný	k2eAgFnPc1d1	zakázaná
a	a	k8xC	a
Facebook	Facebook	k1gInSc1	Facebook
si	se	k3xPyFc3	se
na	na	k7c4	na
firmu	firma	k1gFnSc4	firma
opakovaně	opakovaně	k6eAd1	opakovaně
stěžoval	stěžovat	k5eAaImAgMnS	stěžovat
<g/>
.	.	kIx.	.
</s>
<s>
Social	Social	k1gInSc1	Social
Media	medium	k1gNnSc2	medium
Series	Seriesa	k1gFnPc2	Seriesa
Limited	limited	k2eAgFnPc2d1	limited
přesto	přesto	k8xC	přesto
neustále	neustále	k6eAd1	neustále
vytvářela	vytvářet	k5eAaImAgFnS	vytvářet
nové	nový	k2eAgInPc4d1	nový
a	a	k8xC	a
nové	nový	k2eAgFnPc4d1	nová
stránky	stránka	k1gFnPc4	stránka
typu	typ	k1gInSc2	typ
Likesocial	Likesocial	k1gInSc1	Likesocial
<g/>
.	.	kIx.	.
<g/>
co	co	k3yInSc1	co
a	a	k8xC	a
IGFamous	IGFamous	k1gInSc1	IGFamous
<g/>
.	.	kIx.	.
<g/>
net	net	k?	net
<g/>
,	,	kIx,	,
skrze	skrze	k?	skrze
které	který	k3yQgFnSc3	který
se	se	k3xPyFc4	se
snažila	snažit	k5eAaImAgFnS	snažit
vyhnout	vyhnout	k5eAaPmF	vyhnout
postihům	postih	k1gInPc3	postih
<g/>
.	.	kIx.	.
</s>
<s>
Firma	firma	k1gFnSc1	firma
prodejem	prodej	k1gInSc7	prodej
získala	získat	k5eAaPmAgFnS	získat
finanční	finanční	k2eAgInSc4d1	finanční
prospěch	prospěch	k1gInSc4	prospěch
kolem	kolem	k7c2	kolem
9,4	[number]	k4	9,4
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
předtím	předtím	k6eAd1	předtím
Facebook	Facebook	k1gInSc1	Facebook
žaloval	žalovat	k5eAaImAgInS	žalovat
několik	několik	k4yIc4	několik
čínských	čínský	k2eAgFnPc2d1	čínská
společností	společnost	k1gFnPc2	společnost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
prováděly	provádět	k5eAaImAgFnP	provádět
totožnou	totožný	k2eAgFnSc4d1	totožná
činnost	činnost	k1gFnSc4	činnost
a	a	k8xC	a
falešné	falešný	k2eAgFnPc1d1	falešná
lajky	lajka	k1gFnPc1	lajka
navíc	navíc	k6eAd1	navíc
prodávaly	prodávat	k5eAaImAgFnP	prodávat
i	i	k9	i
na	na	k7c6	na
Twitteru	Twitter	k1gInSc6	Twitter
<g/>
,	,	kIx,	,
Pinterestu	Pinterest	k1gInSc6	Pinterest
a	a	k8xC	a
dalších	další	k2eAgFnPc6d1	další
platformách	platforma	k1gFnPc6	platforma
<g/>
.	.	kIx.	.
</s>
<s>
Nákupem	nákup	k1gInSc7	nákup
falešných	falešný	k2eAgMnPc2d1	falešný
lajků	lajek	k1gMnPc2	lajek
se	se	k3xPyFc4	se
některé	některý	k3yIgFnPc1	některý
firmy	firma	k1gFnPc1	firma
snažily	snažit	k5eAaImAgFnP	snažit
uměle	uměle	k6eAd1	uměle
zvýšit	zvýšit	k5eAaPmF	zvýšit
sledovanost	sledovanost	k1gFnSc4	sledovanost
<g/>
.	.	kIx.	.
</s>
<s>
Instagram	Instagram	k1gInSc1	Instagram
proto	proto	k8xC	proto
využil	využít	k5eAaPmAgInS	využít
k	k	k7c3	k
analýze	analýza	k1gFnSc3	analýza
robotické	robotický	k2eAgInPc4d1	robotický
programy	program	k1gInPc4	program
a	a	k8xC	a
umělou	umělý	k2eAgFnSc4d1	umělá
inteligenci	inteligence	k1gFnSc4	inteligence
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
tyto	tento	k3xDgFnPc4	tento
falešné	falešný	k2eAgFnPc4d1	falešná
lajky	lajka	k1gFnPc4	lajka
mazat	mazat	k5eAaImF	mazat
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
majitele	majitel	k1gMnSc4	majitel
takových	takový	k3xDgInPc2	takový
účtů	účet	k1gInPc2	účet
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
hesla	heslo	k1gNnSc2	heslo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Akcie	akcie	k1gFnSc1	akcie
Facebooku	Facebook	k1gInSc2	Facebook
na	na	k7c6	na
burze	burza	k1gFnSc6	burza
==	==	k?	==
</s>
</p>
<p>
<s>
Akcie	akcie	k1gFnSc1	akcie
společnosti	společnost	k1gFnSc2	společnost
Facebook	Facebook	k1gInSc1	Facebook
jsou	být	k5eAaImIp3nP	být
kótovány	kótovat	k5eAaBmNgInP	kótovat
na	na	k7c4	na
NASDAQ	NASDAQ	kA	NASDAQ
pod	pod	k7c7	pod
symbolem	symbol	k1gInSc7	symbol
FB	FB	kA	FB
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
IPO	IPO	kA	IPO
společnosti	společnost	k1gFnPc1	společnost
Facebook	Facebook	k1gInSc1	Facebook
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
dne	den	k1gInSc2	den
18.	[number]	k4	18.
<g/>
5.2012	[number]	k4	5.2012
a	a	k8xC	a
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
rámci	rámec	k1gInSc6	rámec
umístila	umístit	k5eAaPmAgFnS	umístit
společnost	společnost	k1gFnSc1	společnost
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
421	[number]	k4	421
miliónů	milión	k4xCgInPc2	milión
akcií	akcie	k1gFnPc2	akcie
za	za	k7c4	za
cenu	cena	k1gFnSc4	cena
38	[number]	k4	38
USD	USD	kA	USD
za	za	k7c4	za
akcii	akcie	k1gFnSc4	akcie
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
odpovídalo	odpovídat	k5eAaImAgNnS	odpovídat
horní	horní	k2eAgFnSc4d1	horní
hranici	hranice	k1gFnSc4	hranice
stanoveného	stanovený	k2eAgNnSc2d1	stanovené
cenového	cenový	k2eAgNnSc2d1	cenové
rozpětí	rozpětí	k1gNnSc2	rozpětí
34-38	[number]	k4	34-38
USD	USD	kA	USD
<g/>
.	.	kIx.	.
</s>
<s>
Dosažená	dosažený	k2eAgFnSc1d1	dosažená
cena	cena	k1gFnSc1	cena
IPO	IPO	kA	IPO
ocenila	ocenit	k5eAaPmAgFnS	ocenit
hodnotu	hodnota	k1gFnSc4	hodnota
celého	celý	k2eAgInSc2d1	celý
Facebooku	Facebook	k1gInSc2	Facebook
na	na	k7c4	na
104	[number]	k4	104
mld	mld	k?	mld
<g/>
.	.	kIx.	.
<g/>
USD	USD	kA	USD
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
učinilo	učinit	k5eAaImAgNnS	učinit
tento	tento	k3xDgInSc4	tento
úpis	úpis	k1gInSc1	úpis
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgInPc2d3	veliký
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
technologického	technologický	k2eAgInSc2d1	technologický
sektoru	sektor	k1gInSc2	sektor
a	a	k8xC	a
suverénně	suverénně	k6eAd1	suverénně
největším	veliký	k2eAgInSc7d3	veliký
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
internetu	internet	k1gInSc2	internet
<g/>
.	.	kIx.	.
<g/>
Emise	emise	k1gFnSc1	emise
akcií	akcie	k1gFnPc2	akcie
ovšem	ovšem	k9	ovšem
nebyla	být	k5eNaImAgNnP	být
zpočátku	zpočátku	k6eAd1	zpočátku
příliš	příliš	k6eAd1	příliš
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
a	a	k8xC	a
cena	cena	k1gFnSc1	cena
klesla	klesnout	k5eAaPmAgFnS	klesnout
z	z	k7c2	z
38	[number]	k4	38
dolarů	dolar	k1gInPc2	dolar
za	za	k7c4	za
kus	kus	k1gInSc4	kus
až	až	k9	až
pod	pod	k7c4	pod
20	[number]	k4	20
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Akcií	akcie	k1gFnPc2	akcie
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
zbavila	zbavit	k5eAaPmAgFnS	zbavit
řada	řada	k1gFnSc1	řada
vedoucích	vedoucí	k2eAgMnPc2d1	vedoucí
členů	člen	k1gMnPc2	člen
společnosti	společnost	k1gFnSc2	společnost
Koncem	koncem	k7c2	koncem
srpna	srpen	k1gInSc2	srpen
2013	[number]	k4	2013
přesáhla	přesáhnout	k5eAaPmAgFnS	přesáhnout
cena	cena	k1gFnSc1	cena
akcií	akcie	k1gFnPc2	akcie
kurz	kurz	k1gInSc4	kurz
stanovený	stanovený	k2eAgInSc4d1	stanovený
v	v	k7c4	v
IPO	IPO	kA	IPO
a	a	k8xC	a
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
se	se	k3xPyFc4	se
cena	cena	k1gFnSc1	cena
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
50	[number]	k4	50
USD	USD	kA	USD
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
v	v	k7c6	v
marketingu	marketing	k1gInSc6	marketing
==	==	k?	==
</s>
</p>
<p>
<s>
Facebook	Facebook	k1gInSc4	Facebook
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
jiné	jiný	k2eAgFnSc2d1	jiná
sociální	sociální	k2eAgFnSc2d1	sociální
sítě	síť	k1gFnSc2	síť
<g/>
,	,	kIx,	,
využít	využít	k5eAaPmF	využít
marketingově	marketingově	k6eAd1	marketingově
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
virální	virální	k2eAgInSc4d1	virální
marketing	marketing	k1gInSc4	marketing
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Některé	některý	k3yIgFnPc1	některý
firmy	firma	k1gFnPc1	firma
nakupují	nakupovat	k5eAaBmIp3nP	nakupovat
fanoušky	fanoušek	k1gMnPc4	fanoušek
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vzbudila	vzbudit	k5eAaPmAgFnS	vzbudit
dojem	dojem	k1gInSc4	dojem
většího	veliký	k2eAgInSc2d2	veliký
zájmu	zájem	k1gInSc2	zájem
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
konkurenci	konkurence	k1gFnSc3	konkurence
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
má	mít	k5eAaImIp3nS	mít
ale	ale	k9	ale
za	za	k7c4	za
následek	následek	k1gInSc4	následek
<g/>
,	,	kIx,	,
že	že	k8xS	že
nové	nový	k2eAgInPc4d1	nový
příspěvky	příspěvek	k1gInPc4	příspěvek
se	se	k3xPyFc4	se
zobrazí	zobrazit	k5eAaPmIp3nS	zobrazit
méně	málo	k6eAd2	málo
fanouškům	fanoušek	k1gMnPc3	fanoušek
firmy	firma	k1gFnSc2	firma
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
o	o	k7c4	o
odebírání	odebírání	k1gNnSc4	odebírání
novinek	novinka	k1gFnPc2	novinka
opravdu	opravdu	k6eAd1	opravdu
zájem	zájem	k1gInSc4	zájem
<g/>
.	.	kIx.	.
</s>
<s>
Situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
zhoršila	zhoršit	k5eAaPmAgFnS	zhoršit
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
viditelnost	viditelnost	k1gFnSc1	viditelnost
příspěvků	příspěvek	k1gInPc2	příspěvek
byla	být	k5eAaImAgFnS	být
snížena	snížit	k5eAaPmNgFnS	snížit
na	na	k7c4	na
4	[number]	k4	4
%	%	kIx~	%
fanoušků	fanoušek	k1gMnPc2	fanoušek
a	a	k8xC	a
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2014	[number]	k4	2014
dále	daleko	k6eAd2	daleko
snížena	snížit	k5eAaPmNgFnS	snížit
na	na	k7c4	na
2	[number]	k4	2
%	%	kIx~	%
a	a	k8xC	a
pokud	pokud	k8xS	pokud
firmy	firma	k1gFnPc1	firma
nezaplatí	zaplatit	k5eNaPmIp3nP	zaplatit
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
by	by	kYmCp3nS	by
jim	on	k3xPp3gInPc3	on
být	být	k5eAaImF	být
viditelnost	viditelnost	k1gFnSc1	viditelnost
příspěvků	příspěvek	k1gInPc2	příspěvek
snížena	snížit	k5eAaPmNgFnS	snížit
až	až	k6eAd1	až
na	na	k7c4	na
nulu	nula	k1gFnSc4	nula
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
zachován	zachovat	k5eAaPmNgInS	zachovat
dojem	dojem	k1gInSc1	dojem
fungování	fungování	k1gNnSc2	fungování
<g/>
,	,	kIx,	,
přešel	přejít	k5eAaPmAgInS	přejít
Facebook	Facebook	k1gInSc1	Facebook
z	z	k7c2	z
metriky	metrika	k1gFnSc2	metrika
"	"	kIx"	"
<g/>
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
mluví	mluvit	k5eAaImIp3nS	mluvit
<g/>
"	"	kIx"	"
na	na	k7c4	na
jinou	jiný	k2eAgFnSc4d1	jiná
metriku	metrika	k1gFnSc4	metrika
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Valleywagu	Valleywag	k1gInSc2	Valleywag
se	se	k3xPyFc4	se
tímto	tento	k3xDgInSc7	tento
Facebooku	Facebook	k1gInSc3	Facebook
povedl	povést	k5eAaPmAgInS	povést
největší	veliký	k2eAgInSc1d3	veliký
kanadský	kanadský	k2eAgInSc1d1	kanadský
fór	fór	k1gInSc1	fór
internetové	internetový	k2eAgFnSc2d1	internetová
éry	éra	k1gFnSc2	éra
<g/>
,	,	kIx,	,
když	když	k8xS	když
přesvědčil	přesvědčit	k5eAaPmAgMnS	přesvědčit
nepočítaně	nepočítaně	k6eAd1	nepočítaně
celebrit	celebrita	k1gFnPc2	celebrita
<g/>
,	,	kIx,	,
skupin	skupina	k1gFnPc2	skupina
<g/>
,	,	kIx,	,
firem	firma	k1gFnPc2	firma
i	i	k8xC	i
značek	značka	k1gFnPc2	značka
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnPc1	jeho
služby	služba	k1gFnPc1	služba
jsou	být	k5eAaImIp3nP	být
nejlepší	dobrý	k2eAgFnSc7d3	nejlepší
cestou	cesta	k1gFnSc7	cesta
k	k	k7c3	k
dosažení	dosažení	k1gNnSc3	dosažení
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
v	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
všichni	všechen	k3xTgMnPc1	všechen
doslova	doslova	k6eAd1	doslova
závislí	závislý	k2eAgMnPc1d1	závislý
<g/>
,	,	kIx,	,
udělal	udělat	k5eAaPmAgMnS	udělat
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
svá	svůj	k3xOyFgNnPc4	svůj
rukojmí	rukojmí	k1gNnPc4	rukojmí
<g/>
.	.	kIx.	.
<g/>
Od	od	k7c2	od
1.	[number]	k4	1.
1.	[number]	k4	1.
2016	[number]	k4	2016
Facebook	Facebook	k1gInSc1	Facebook
upravil	upravit	k5eAaPmAgInS	upravit
podmínky	podmínka	k1gFnPc4	podmínka
používání	používání	k1gNnSc1	používání
a	a	k8xC	a
jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
změn	změna	k1gFnPc2	změna
dovoluje	dovolovat	k5eAaImIp3nS	dovolovat
společnosti	společnost	k1gFnPc4	společnost
prodávat	prodávat	k5eAaImF	prodávat
téměř	téměř	k6eAd1	téměř
vše	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
uživatelé	uživatel	k1gMnPc1	uživatel
na	na	k7c4	na
její	její	k3xOp3gInPc4	její
servery	server	k1gInPc4	server
uploadují	uploadovat	k5eAaPmIp3nP	uploadovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
–	–	k?	–
<g/>
výňatek	výňatek	k1gInSc1	výňatek
z	z	k7c2	z
nových	nový	k2eAgFnPc2d1	nová
podmínek	podmínka	k1gFnPc2	podmínka
používání	používání	k1gNnSc2	používání
</s>
</p>
<p>
<s>
To	to	k9	to
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
znamená	znamenat	k5eAaImIp3nS	znamenat
i	i	k9	i
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
profesionální	profesionální	k2eAgFnPc1d1	profesionální
fotografie	fotografia	k1gFnPc1	fotografia
a	a	k8xC	a
záběry	záběr	k1gInPc1	záběr
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
jednostranně	jednostranně	k6eAd1	jednostranně
Facebookem	Facebook	k1gInSc7	Facebook
odprodány	odprodán	k2eAgFnPc4d1	odprodána
třetí	třetí	k4xOgFnSc3	třetí
osobě	osoba	k1gFnSc3	osoba
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
autor	autor	k1gMnSc1	autor
byl	být	k5eAaImAgMnS	být
jakkoli	jakkoli	k6eAd1	jakkoli
finančně	finančně	k6eAd1	finančně
kompenzován	kompenzovat	k5eAaBmNgInS	kompenzovat
<g/>
.	.	kIx.	.
<g/>
Facebook	Facebook	k1gInSc1	Facebook
nabízí	nabízet	k5eAaImIp3nS	nabízet
reklamu	reklama	k1gFnSc4	reklama
nejen	nejen	k6eAd1	nejen
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
Facebooku	Facebook	k1gInSc6	Facebook
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
na	na	k7c6	na
Instagramu	Instagram	k1gInSc6	Instagram
a	a	k8xC	a
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
partnerské	partnerský	k2eAgFnSc6d1	partnerská
síti	síť	k1gFnSc6	síť
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
nazývá	nazývat	k5eAaImIp3nS	nazývat
Audience	audience	k1gFnSc1	audience
Network	network	k1gInSc1	network
<g/>
.	.	kIx.	.
</s>
<s>
Facebook	Facebook	k1gInSc1	Facebook
nabízí	nabízet	k5eAaImIp3nS	nabízet
téměř	téměř	k6eAd1	téměř
neomezené	omezený	k2eNgFnPc4d1	neomezená
možnosti	možnost	k1gFnPc4	možnost
cílení	cílení	k1gNnSc2	cílení
reklamy	reklama	k1gFnSc2	reklama
zaměřené	zaměřený	k2eAgFnSc2d1	zaměřená
na	na	k7c4	na
region	region	k1gInSc4	region
<g/>
,	,	kIx,	,
zájmy	zájem	k1gInPc4	zájem
<g/>
,	,	kIx,	,
pohlaví	pohlaví	k1gNnPc4	pohlaví
<g/>
,	,	kIx,	,
příjem	příjem	k1gInSc4	příjem
<g/>
,	,	kIx,	,
zaměstnání	zaměstnání	k1gNnSc4	zaměstnání
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
cílící	cílící	k2eAgInPc4d1	cílící
ukazetele	ukazetel	k1gInPc4	ukazetel
umožňující	umožňující	k2eAgInPc4d1	umožňující
velmi	velmi	k6eAd1	velmi
přesné	přesný	k2eAgNnSc4d1	přesné
nastavení	nastavení	k1gNnSc4	nastavení
reklamy	reklama	k1gFnSc2	reklama
na	na	k7c6	na
Facebooku	Facebook	k1gInSc6	Facebook
a	a	k8xC	a
Instagramu	Instagram	k1gInSc6	Instagram
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
klíčových	klíčový	k2eAgInPc2d1	klíčový
bodů	bod	k1gInPc2	bod
pro	pro	k7c4	pro
úspěch	úspěch	k1gInSc4	úspěch
na	na	k7c6	na
Facebooku	Facebook	k1gInSc6	Facebook
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
pochopení	pochopení	k1gNnSc1	pochopení
Facebook	Facebook	k1gInSc1	Facebook
Relevance	relevance	k1gFnSc1	relevance
Score	Scor	k1gInSc5	Scor
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
schopnosti	schopnost	k1gFnSc2	schopnost
vytvořit	vytvořit	k5eAaPmF	vytvořit
vhodný	vhodný	k2eAgInSc4d1	vhodný
obsah	obsah	k1gInSc4	obsah
pro	pro	k7c4	pro
potencionální	potencionální	k2eAgFnSc4d1	potencionální
cílovou	cílový	k2eAgFnSc4d1	cílová
skupinu	skupina	k1gFnSc4	skupina
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yRgInPc4	který
chceme	chtít	k5eAaImIp1nP	chtít
obsah	obsah	k1gInSc4	obsah
propagovat	propagovat	k5eAaImF	propagovat
<g/>
.	.	kIx.	.
</s>
<s>
Čím	co	k3yRnSc7	co
lepší	dobrý	k2eAgInSc1d2	lepší
obsah	obsah	k1gInSc1	obsah
a	a	k8xC	a
přesnější	přesný	k2eAgNnPc1d2	přesnější
cílení	cílení	k1gNnPc1	cílení
máme	mít	k5eAaImIp1nP	mít
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
nižší	nízký	k2eAgInPc1d2	nižší
jsou	být	k5eAaImIp3nP	být
náklady	náklad	k1gInPc1	náklad
na	na	k7c4	na
reklamu	reklama	k1gFnSc4	reklama
<g/>
.	.	kIx.	.
</s>
<s>
Cíle	cíl	k1gInPc1	cíl
reklamy	reklama	k1gFnSc2	reklama
na	na	k7c6	na
Facebooku	Facebook	k1gInSc6	Facebook
můžeme	moct	k5eAaImIp1nP	moct
rozdělit	rozdělit	k5eAaPmF	rozdělit
dle	dle	k7c2	dle
Facebook	Facebook	k1gInSc1	Facebook
Ads	Ads	k1gMnSc3	Ads
manažeru	manažer	k1gMnSc3	manažer
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
<s>
Budování	budování	k1gNnSc4	budování
značky	značka	k1gFnSc2	značka
</s>
</p>
<p>
<s>
Maximální	maximální	k2eAgInSc4d1	maximální
dosah	dosah	k1gInSc4	dosah
pro	pro	k7c4	pro
lokální	lokální	k2eAgInPc4d1	lokální
podniky	podnik	k1gInPc4	podnik
</s>
</p>
<p>
<s>
Návštěvnost	návštěvnost	k1gFnSc1	návštěvnost
</s>
</p>
<p>
<s>
Zaujetí	zaujetí	k1gNnSc1	zaujetí
/	/	kIx~	/
Zabavení	zabavení	k1gNnSc1	zabavení
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Engagement	engagement	k1gNnSc4	engagement
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nainstalování	nainstalování	k1gNnSc1	nainstalování
aplikace	aplikace	k1gFnSc2	aplikace
</s>
</p>
<p>
<s>
Psaní	psaní	k1gNnSc1	psaní
zpráv	zpráva	k1gFnPc2	zpráva
se	s	k7c7	s
zákazníky	zákazník	k1gMnPc7	zákazník
</s>
</p>
<p>
<s>
Sběr	sběr	k1gInSc1	sběr
kontaktu	kontakt	k1gInSc2	kontakt
na	na	k7c4	na
zákazníky	zákazník	k1gMnPc4	zákazník
</s>
</p>
<p>
<s>
Konverze	konverze	k1gFnSc1	konverze
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
např.	např.	kA	např.
prodej	prodej	k1gInSc4	prodej
anebo	anebo	k8xC	anebo
vyplnění	vyplnění	k1gNnSc4	vyplnění
formuláře	formulář	k1gInSc2	formulář
na	na	k7c6	na
webu	web	k1gInSc6	web
</s>
</p>
<p>
<s>
Prodej	prodej	k1gInSc1	prodej
z	z	k7c2	z
katalogu	katalog	k1gInSc2	katalog
(	(	kIx(	(
<g/>
známé	známý	k2eAgInPc4d1	známý
jako	jako	k8xC	jako
dynamický	dynamický	k2eAgInSc4d1	dynamický
retargeting	retargeting	k1gInSc4	retargeting
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Návštěva	návštěva	k1gFnSc1	návštěva
obchodu	obchod	k1gInSc2	obchod
-	-	kIx~	-
propojení	propojení	k1gNnSc2	propojení
off-line	offin	k1gInSc5	off-lin
software	software	k1gInSc1	software
s	s	k7c7	s
FacebookemFacebook	FacebookemFacebook	k1gInSc1	FacebookemFacebook
také	také	k9	také
nabízí	nabízet	k5eAaImIp3nS	nabízet
bohaté	bohatý	k2eAgNnSc1d1	bohaté
množství	množství	k1gNnSc1	množství
remarketingových	remarketingový	k2eAgFnPc2d1	remarketingový
a	a	k8xC	a
retargetingových	retargetingový	k2eAgFnPc2d1	retargetingový
cest	cesta	k1gFnPc2	cesta
-	-	kIx~	-
nejen	nejen	k6eAd1	nejen
remarketing	remarketing	k1gInSc1	remarketing
webových	webový	k2eAgFnPc2d1	webová
stánek	stánka	k1gFnPc2	stánka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
znovu	znovu	k6eAd1	znovu
oslovení	oslovení	k1gNnPc2	oslovení
již	již	k6eAd1	již
zaangažovaných	zaangažovaný	k2eAgMnPc2d1	zaangažovaný
lidí	člověk	k1gMnPc2	člověk
na	na	k7c6	na
Facebooku	Facebook	k1gInSc6	Facebook
<g/>
,	,	kIx,	,
např.	např.	kA	např.
zhlédnutí	zhlédnutí	k1gNnSc1	zhlédnutí
videa	video	k1gNnSc2	video
atd.	atd.	kA	atd.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Negativní	negativní	k2eAgInPc4d1	negativní
důsledky	důsledek	k1gInPc4	důsledek
a	a	k8xC	a
kritika	kritika	k1gFnSc1	kritika
==	==	k?	==
</s>
</p>
<p>
<s>
Lidé	člověk	k1gMnPc1	člověk
mohou	moct	k5eAaImIp3nP	moct
získat	získat	k5eAaPmF	získat
psychickou	psychický	k2eAgFnSc4d1	psychická
závislost	závislost	k1gFnSc4	závislost
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
může	moct	k5eAaImIp3nS	moct
užívání	užívání	k1gNnSc4	užívání
prohlubovat	prohlubovat	k5eAaImF	prohlubovat
reálnou	reálný	k2eAgFnSc4d1	reálná
osamělost	osamělost	k1gFnSc4	osamělost
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
někdo	někdo	k3yInSc1	někdo
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
z	z	k7c2	z
Facebooku	Facebook	k1gInSc2	Facebook
odejít	odejít	k5eAaPmF	odejít
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
u	u	k7c2	u
něho	on	k3xPp3gInSc2	on
vzniknout	vzniknout	k5eAaPmF	vzniknout
abstinenční	abstinenční	k2eAgInPc4d1	abstinenční
příznaky	příznak	k1gInPc4	příznak
a	a	k8xC	a
nepochopení	nepochopení	k1gNnSc4	nepochopení
okolí	okolí	k1gNnSc2	okolí
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
dotyčný	dotyčný	k2eAgMnSc1d1	dotyčný
nechce	chtít	k5eNaImIp3nS	chtít
Facebook	Facebook	k1gInSc1	Facebook
používat	používat	k5eAaImF	používat
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
Dánsku	Dánsko	k1gNnSc6	Dánsko
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
experiment	experiment	k1gInSc1	experiment
týkající	týkající	k2eAgInPc4d1	týkající
se	se	k3xPyFc4	se
dopadů	dopad	k1gInPc2	dopad
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
člověk	člověk	k1gMnSc1	člověk
stráví	strávit	k5eAaPmIp3nS	strávit
týden	týden	k1gInSc4	týden
bez	bez	k7c2	bez
Facebooku	Facebook	k1gInSc2	Facebook
<g/>
,	,	kIx,	,
kterého	který	k3yRgNnSc2	který
se	se	k3xPyFc4	se
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
asi	asi	k9	asi
500	[number]	k4	500
lidí	člověk	k1gMnPc2	člověk
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
17	[number]	k4	17
až	až	k9	až
76	[number]	k4	76
let	léto	k1gNnPc2	léto
a	a	k8xC	a
jehož	jehož	k3xOyRp3gInSc7	jehož
výsledkem	výsledek	k1gInSc7	výsledek
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
účastnící	účastnící	k2eAgFnSc1d1	účastnící
experimentu	experiment	k1gInSc6	experiment
byly	být	k5eAaImAgInP	být
po	po	k7c6	po
tomto	tento	k3xDgInSc6	tento
týdnu	týden	k1gInSc6	týden
bez	bez	k7c2	bez
Facebooku	Facebook	k1gInSc2	Facebook
šťastnější	šťastný	k2eAgFnSc1d2	šťastnější
<g/>
,	,	kIx,	,
rozhodnější	rozhodný	k2eAgFnSc1d2	rozhodnější
a	a	k8xC	a
klidnější	klidný	k2eAgFnSc1d2	klidnější
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
potvrdilo	potvrdit	k5eAaPmAgNnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Facebook	Facebook	k1gInSc1	Facebook
je	být	k5eAaImIp3nS	být
příčinou	příčina	k1gFnSc7	příčina
celé	celý	k2eAgFnSc2d1	celá
řady	řada	k1gFnSc2	řada
každodenních	každodenní	k2eAgInPc2d1	každodenní
problémů	problém	k1gInPc2	problém
<g/>
.	.	kIx.	.
</s>
<s>
Účastníci	účastník	k1gMnPc1	účastník
experimentu	experiment	k1gInSc2	experiment
měli	mít	k5eAaImAgMnP	mít
také	také	k9	také
více	hodně	k6eAd2	hodně
času	čas	k1gInSc2	čas
na	na	k7c4	na
skutečné	skutečný	k2eAgFnPc4d1	skutečná
společenské	společenský	k2eAgFnPc4d1	společenská
aktivity	aktivita	k1gFnPc4	aktivita
<g/>
.	.	kIx.	.
<g/>
Facebook	Facebook	k1gInSc1	Facebook
bývá	bývat	k5eAaImIp3nS	bývat
také	také	k9	také
často	často	k6eAd1	často
kritizován	kritizovat	k5eAaImNgMnS	kritizovat
pro	pro	k7c4	pro
nedostatečnou	dostatečný	k2eNgFnSc4d1	nedostatečná
ochranu	ochrana	k1gFnSc4	ochrana
soukromí	soukromí	k1gNnSc2	soukromí
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
dle	dle	k7c2	dle
kritiků	kritik	k1gMnPc2	kritik
otevírá	otevírat	k5eAaImIp3nS	otevírat
dveře	dveře	k1gFnPc4	dveře
kyberšikaně	kyberšikaně	k6eAd1	kyberšikaně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
se	se	k3xPyFc4	se
na	na	k7c4	na
společnost	společnost	k1gFnSc4	společnost
Facebook	Facebook	k1gInSc4	Facebook
snesla	snést	k5eAaPmAgFnS	snést
vlna	vlna	k1gFnSc1	vlna
kritiky	kritika	k1gFnSc2	kritika
kvůli	kvůli	k7c3	kvůli
sociologickému	sociologický	k2eAgInSc3d1	sociologický
průzkumu	průzkum	k1gInSc3	průzkum
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgInSc6	jenž
bylo	být	k5eAaImAgNnS	být
sledováno	sledovat	k5eAaImNgNnS	sledovat
<g/>
,	,	kIx,	,
jaké	jaký	k3yQgInPc1	jaký
dopady	dopad	k1gInPc1	dopad
mají	mít	k5eAaImIp3nP	mít
negativní	negativní	k2eAgInPc1d1	negativní
(	(	kIx(	(
<g/>
či	či	k8xC	či
pozitivní	pozitivní	k2eAgFnPc1d1	pozitivní
<g/>
)	)	kIx)	)
zprávy	zpráva	k1gFnPc1	zpráva
na	na	k7c4	na
náladu	nálada	k1gFnSc4	nálada
jedince	jedinec	k1gMnSc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
zjištění	zjištění	k1gNnSc3	zjištění
bylo	být	k5eAaImAgNnS	být
použito	použít	k5eAaPmNgNnS	použít
např.	např.	kA	např.
zpráv	zpráva	k1gFnPc2	zpráva
od	od	k7c2	od
falešných	falešný	k2eAgMnPc2d1	falešný
přátel	přítel	k1gMnPc2	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Kritika	kritika	k1gFnSc1	kritika
se	se	k3xPyFc4	se
snesla	snést	k5eAaPmAgFnS	snést
hlavně	hlavně	k9	hlavně
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
neuvědomnění	neuvědomnění	k1gNnPc2	neuvědomnění
uživatelů	uživatel	k1gMnPc2	uživatel
na	na	k7c4	na
fakt	fakt	k1gInSc4	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
účastníky	účastník	k1gMnPc7	účastník
experimentu	experiment	k1gInSc2	experiment
<g/>
,	,	kIx,	,
kterých	který	k3yQgInPc2	který
prý	prý	k9	prý
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
až	až	k9	až
700 000	[number]	k4	700 000
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
se	se	k3xPyFc4	se
ale	ale	k9	ale
braní	braní	k1gNnSc2	braní
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
uživatelské	uživatelský	k2eAgFnPc1d1	Uživatelská
podmínky	podmínka	k1gFnPc1	podmínka
podobné	podobný	k2eAgInPc4d1	podobný
skutky	skutek	k1gInPc4	skutek
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Facebook	Facebook	k1gInSc1	Facebook
je	být	k5eAaImIp3nS	být
dále	daleko	k6eAd2	daleko
kritizován	kritizovat	k5eAaImNgMnS	kritizovat
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
sleduje	sledovat	k5eAaImIp3nS	sledovat
chování	chování	k1gNnSc4	chování
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
nejen	nejen	k6eAd1	nejen
registrovaných	registrovaný	k2eAgMnPc2d1	registrovaný
uživatelů	uživatel	k1gMnPc2	uživatel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
neregistrovaných	registrovaný	k2eNgFnPc2d1	neregistrovaná
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgInSc1	sám
Facebook	Facebook	k1gInSc1	Facebook
sledování	sledování	k1gNnSc2	sledování
neregistrovaných	registrovaný	k2eNgMnPc2d1	neregistrovaný
uživatelů	uživatel	k1gMnPc2	uživatel
přiznává	přiznávat	k5eAaImIp3nS	přiznávat
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
sledování	sledování	k1gNnSc3	sledování
uživatelů	uživatel	k1gMnPc2	uživatel
včetně	včetně	k7c2	včetně
těch	ten	k3xDgMnPc2	ten
neregistrovaných	registrovaný	k2eNgMnPc2d1	neregistrovaný
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
Facebook	Facebook	k1gInSc4	Facebook
podána	podat	k5eAaPmNgFnS	podat
žaloba	žaloba	k1gFnSc1	žaloba
<g/>
.	.	kIx.	.
</s>
<s>
Sledování	sledování	k1gNnSc4	sledování
ne-uživatelů	neživatel	k1gMnPc2	ne-uživatel
Facebooku	Facebook	k1gInSc2	Facebook
bylo	být	k5eAaImAgNnS	být
podle	podle	k7c2	podle
prohlášení	prohlášení	k1gNnSc2	prohlášení
Facebooku	Facebook	k1gInSc2	Facebook
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
chybou	chyba	k1gFnSc7	chyba
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
Facebooku	Facebook	k1gInSc2	Facebook
šmírovací	šmírovací	k2eAgFnSc2d1	šmírovací
cookie	cookie	k1gFnSc2	cookie
zásadní	zásadní	k2eAgInSc1d1	zásadní
bezpečnostní	bezpečnostní	k2eAgInSc1d1	bezpečnostní
nástroj	nástroj	k1gInSc1	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
otázka	otázka	k1gFnSc1	otázka
sledování	sledování	k1gNnSc2	sledování
neregistrovaných	registrovaný	k2eNgMnPc2d1	neregistrovaný
uživatelů	uživatel	k1gMnPc2	uživatel
Facebooku	Facebook	k1gInSc2	Facebook
byla	být	k5eAaImAgNnP	být
otevřena	otevřít	k5eAaPmNgNnP	otevřít
už	už	k6eAd1	už
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
a	a	k8xC	a
zřejmě	zřejmě	k6eAd1	zřejmě
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
Facebook	Facebook	k1gInSc4	Facebook
výhodná	výhodný	k2eAgFnSc1d1	výhodná
<g/>
,	,	kIx,	,
očekává	očekávat	k5eAaImIp3nS	očekávat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
podobná	podobný	k2eAgFnSc1d1	podobná
chyba	chyba	k1gFnSc1	chyba
se	se	k3xPyFc4	se
vloudí	vloudit	k5eAaPmIp3nS	vloudit
do	do	k7c2	do
kódu	kód	k1gInSc2	kód
Facebooku	Facebook	k1gInSc2	Facebook
znovu	znovu	k6eAd1	znovu
<g/>
.	.	kIx.	.
</s>
<s>
Uživatel	uživatel	k1gMnSc1	uživatel
přihlášený	přihlášený	k2eAgMnSc1d1	přihlášený
k	k	k7c3	k
Facebooku	Facebook	k1gInSc3	Facebook
je	být	k5eAaImIp3nS	být
sledován	sledovat	k5eAaImNgInS	sledovat
pomocí	pomocí	k7c2	pomocí
cookie	cookie	k1gFnSc2	cookie
na	na	k7c6	na
všech	všecek	k3xTgFnPc6	všecek
stránkách	stránka	k1gFnPc6	stránka
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yRgFnPc6	který
jsou	být	k5eAaImIp3nP	být
umístěny	umístit	k5eAaPmNgInP	umístit
jeho	jeho	k3xOp3gInPc1	jeho
aktivní	aktivní	k2eAgInPc1d1	aktivní
prvky	prvek	k1gInPc1	prvek
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
uživatel	uživatel	k1gMnSc1	uživatel
odhlásí	odhlásit	k5eAaPmIp3nS	odhlásit
a	a	k8xC	a
cookie	cookie	k1gFnSc1	cookie
si	se	k3xPyFc3	se
smaže	smazat	k5eAaPmIp3nS	smazat
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
ji	on	k3xPp3gFnSc4	on
Facebook	Facebook	k1gInSc4	Facebook
snadno	snadno	k6eAd1	snadno
obnovit	obnovit	k5eAaPmF	obnovit
<g/>
.	.	kIx.	.
</s>
<s>
Uživatelé	uživatel	k1gMnPc1	uživatel
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
nejsou	být	k5eNaImIp3nP	být
registrovaní	registrovaný	k2eAgMnPc1d1	registrovaný
na	na	k7c6	na
Facebooku	Facebook	k1gInSc6	Facebook
<g/>
,	,	kIx,	,
dostanou	dostat	k5eAaPmIp3nP	dostat
cookie	cookie	k1gFnPc4	cookie
přidělenou	přidělený	k2eAgFnSc4d1	přidělená
také	také	k6eAd1	také
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
těch	ten	k3xDgMnPc2	ten
uživatelů	uživatel	k1gMnPc2	uživatel
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
vyjádří	vyjádřit	k5eAaPmIp3nP	vyjádřit
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
nesledování	nesledování	k1gNnSc4	nesledování
<g/>
.	.	kIx.	.
</s>
<s>
Facebook	Facebook	k1gInSc1	Facebook
je	být	k5eAaImIp3nS	být
také	také	k9	také
kritizován	kritizován	k2eAgInSc4d1	kritizován
za	za	k7c4	za
tzv.	tzv.	kA	tzv.
stínové	stínový	k2eAgInPc1d1	stínový
profily	profil	k1gInPc4	profil
<g/>
.	.	kIx.	.
</s>
<s>
Facebook	Facebook	k1gInSc1	Facebook
na	na	k7c6	na
základě	základ	k1gInSc6	základ
různých	různý	k2eAgInPc2d1	různý
indikátorů	indikátor	k1gInPc2	indikátor
vyhledává	vyhledávat	k5eAaImIp3nS	vyhledávat
a	a	k8xC	a
shromažďuje	shromažďovat	k5eAaImIp3nS	shromažďovat
data	datum	k1gNnPc4	datum
i	i	k9	i
o	o	k7c6	o
uživatelích	uživatel	k1gMnPc6	uživatel
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
Facebook	Facebook	k1gInSc4	Facebook
nepoužívají	používat	k5eNaImIp3nP	používat
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
těchto	tento	k3xDgInPc6	tento
lidech	lid	k1gInPc6	lid
má	mít	k5eAaImIp3nS	mít
Facebook	Facebook	k1gInSc1	Facebook
obdobné	obdobný	k2eAgFnSc2d1	obdobná
informace	informace	k1gFnSc2	informace
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
o	o	k7c6	o
jeho	jeho	k3xOp3gMnPc6	jeho
uživatelích	uživatel	k1gMnPc6	uživatel
<g/>
.	.	kIx.	.
<g/>
Facebook	Facebook	k1gInSc1	Facebook
používá	používat	k5eAaImIp3nS	používat
i	i	k9	i
stínové	stínový	k2eAgInPc4d1	stínový
údaje	údaj	k1gInPc4	údaj
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
cílení	cílení	k1gNnSc3	cílení
reklam	reklama	k1gFnPc2	reklama
tak	tak	k9	tak
využívá	využívat	k5eAaPmIp3nS	využívat
i	i	k9	i
osobní	osobní	k2eAgInPc4d1	osobní
údaje	údaj	k1gInPc4	údaj
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
nejsou	být	k5eNaImIp3nP	být
součástí	součást	k1gFnSc7	součást
profilu	profil	k1gInSc2	profil
<g/>
.	.	kIx.	.
</s>
<s>
Facebook	Facebook	k1gInSc1	Facebook
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
vazbám	vazba	k1gFnPc3	vazba
na	na	k7c4	na
přátele	přítel	k1gMnPc4	přítel
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
získat	získat	k5eAaPmF	získat
osobnostní	osobnostní	k2eAgInSc4d1	osobnostní
profil	profil	k1gInSc4	profil
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
není	být	k5eNaImIp3nS	být
uživatelem	uživatel	k1gMnSc7	uživatel
<g/>
.	.	kIx.	.
</s>
<s>
Facebook	Facebook	k1gInSc4	Facebook
také	také	k9	také
lobboval	lobbovat	k5eAaImAgMnS	lobbovat
proti	proti	k7c3	proti
GDPR	GDPR	kA	GDPR
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
Čínské	čínský	k2eAgFnSc6d1	čínská
lidové	lidový	k2eAgFnSc6d1	lidová
republice	republika	k1gFnSc6	republika
existuje	existovat	k5eAaImIp3nS	existovat
velice	velice	k6eAd1	velice
přísná	přísný	k2eAgFnSc1d1	přísná
cenzura	cenzura	k1gFnSc1	cenzura
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
<g/>
,	,	kIx,	,
Facebook	Facebook	k1gInSc1	Facebook
proto	proto	k8xC	proto
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
software	software	k1gInSc1	software
na	na	k7c4	na
cenzuru	cenzura	k1gFnSc4	cenzura
příspěvků	příspěvek	k1gInPc2	příspěvek
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
získal	získat	k5eAaPmAgMnS	získat
povolení	povolení	k1gNnSc4	povolení
působit	působit	k5eAaImF	působit
na	na	k7c6	na
lukrativním	lukrativní	k2eAgInSc6d1	lukrativní
čínském	čínský	k2eAgInSc6d1	čínský
trhu	trh	k1gInSc6	trh
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
spoluzakladatele	spoluzakladatel	k1gMnSc2	spoluzakladatel
Facebooku	Facebook	k1gInSc2	Facebook
Seana	Sean	k1gMnSc2	Sean
Parkera	Parker	k1gMnSc2	Parker
Facebook	Facebook	k1gInSc1	Facebook
doslova	doslova	k6eAd1	doslova
mění	měnit	k5eAaImIp3nS	měnit
vztahy	vztah	k1gInPc4	vztah
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
podivným	podivný	k2eAgInSc7d1	podivný
způsobem	způsob	k1gInSc7	způsob
narušuje	narušovat	k5eAaImIp3nS	narušovat
produktivitu	produktivita	k1gFnSc4	produktivita
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
podle	podle	k7c2	podle
Parkera	Parkero	k1gNnSc2	Parkero
jen	jen	k9	jen
Bůh	bůh	k1gMnSc1	bůh
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
Facebook	Facebook	k1gInSc1	Facebook
dělá	dělat	k5eAaImIp3nS	dělat
s	s	k7c7	s
mozky	mozek	k1gInPc7	mozek
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
<g/>
Roku	rok	k1gInSc2	rok
2018	[number]	k4	2018
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
bezpečnostní	bezpečnostní	k2eAgFnSc3d1	bezpečnostní
chybě	chyba	k1gFnSc3	chyba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ohrozila	ohrozit	k5eAaPmAgFnS	ohrozit
50	[number]	k4	50
miliónů	milión	k4xCgInPc2	milión
účtů	účet	k1gInPc2	účet
<g/>
.	.	kIx.	.
</s>
<s>
Útočník	útočník	k1gMnSc1	útočník
mohl	moct	k5eAaImAgMnS	moct
převzít	převzít	k5eAaPmF	převzít
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
účtem	účet	k1gInSc7	účet
a	a	k8xC	a
aplikacemi	aplikace	k1gFnPc7	aplikace
třetích	třetí	k4xOgFnPc2	třetí
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2019	[number]	k4	2019
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
že	že	k8xS	že
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
Facebook	Facebook	k1gInSc1	Facebook
uchovával	uchovávat	k5eAaImAgInS	uchovávat
na	na	k7c6	na
interních	interní	k2eAgInPc6d1	interní
počítačích	počítač	k1gInPc6	počítač
hesla	heslo	k1gNnSc2	heslo
miliónů	milión	k4xCgInPc2	milión
uživatelů	uživatel	k1gMnPc2	uživatel
v	v	k7c6	v
otevřené	otevřený	k2eAgFnSc6d1	otevřená
formě	forma	k1gFnSc6	forma
<g/>
.	.	kIx.	.
</s>
<s>
Facebook	Facebook	k1gInSc1	Facebook
také	také	k9	také
do	do	k7c2	do
IPTC	IPTC	kA	IPTC
obrázků	obrázek	k1gInPc2	obrázek
ukládá	ukládat	k5eAaImIp3nS	ukládat
informace	informace	k1gFnSc1	informace
umožňující	umožňující	k2eAgNnSc1d1	umožňující
sledování	sledování	k1gNnSc4	sledování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Důvody	důvod	k1gInPc1	důvod
pro	pro	k7c4	pro
zrušení	zrušení	k1gNnSc4	zrušení
účtu	účet	k1gInSc2	účet
na	na	k7c6	na
Facebooku	Facebook	k1gInSc6	Facebook
===	===	k?	===
</s>
</p>
<p>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc1	několik
důvodů	důvod	k1gInPc2	důvod
k	k	k7c3	k
zamyšlení	zamyšlení	k1gNnSc3	zamyšlení
nad	nad	k7c7	nad
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
účet	účet	k1gInSc4	účet
u	u	k7c2	u
Facebooku	Facebook	k1gInSc2	Facebook
zrušit	zrušit	k5eAaPmF	zrušit
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
<s>
podle	podle	k7c2	podle
amerických	americký	k2eAgMnPc2d1	americký
teenagerů	teenager	k1gMnPc2	teenager
už	už	k9	už
nikdo	nikdo	k3yNnSc1	nikdo
nikdo	nikdo	k3yNnSc1	nikdo
na	na	k7c4	na
Facebook	Facebook	k1gInSc4	Facebook
nemá	mít	k5eNaImIp3nS	mít
čas	čas	k1gInSc4	čas
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
uživatele	uživatel	k1gMnPc4	uživatel
mohou	moct	k5eAaImIp3nP	moct
sledovat	sledovat	k5eAaImF	sledovat
lidé	člověk	k1gMnPc1	člověk
z	z	k7c2	z
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
Facebook	Facebook	k1gInSc1	Facebook
zrušil	zrušit	k5eAaPmAgInS	zrušit
možnost	možnost	k1gFnSc4	možnost
skrytí	skrytí	k1gNnSc2	skrytí
uživatele	uživatel	k1gMnSc2	uživatel
při	při	k7c6	při
vyhledávání	vyhledávání	k1gNnSc6	vyhledávání
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Facebook	Facebook	k1gInSc1	Facebook
si	se	k3xPyFc3	se
drží	držet	k5eAaImIp3nS	držet
stopy	stopa	k1gFnPc4	stopa
i	i	k9	i
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
nebylo	být	k5eNaImAgNnS	být
sdíleno	sdílet	k5eAaImNgNnS	sdílet
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
uživatel	uživatel	k1gMnSc1	uživatel
nezná	znát	k5eNaImIp3nS	znát
lidi	člověk	k1gMnPc4	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
na	na	k7c6	na
Facebooku	Facebook	k1gInSc6	Facebook
chtějí	chtít	k5eAaImIp3nP	chtít
přátelit	přátelit	k5eAaImF	přátelit
<g/>
,	,	kIx,	,
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
seznamu	seznam	k1gInSc2	seznam
přátel	přítel	k1gMnPc2	přítel
uživatel	uživatel	k1gMnSc1	uživatel
zná	znát	k5eAaImIp3nS	znát
osobně	osobně	k6eAd1	osobně
jenom	jenom	k9	jenom
malou	malý	k2eAgFnSc4d1	malá
část	část	k1gFnSc4	část
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
příliš	příliš	k6eAd1	příliš
mnoho	mnoho	k4c1	mnoho
inzerce	inzerce	k1gFnSc2	inzerce
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
častá	častý	k2eAgNnPc4d1	časté
oznámení	oznámení	k1gNnPc4	oznámení
od	od	k7c2	od
přátel	přítel	k1gMnPc2	přítel
<g/>
,	,	kIx,	,
že	že	k8xS	že
mají	mít	k5eAaImIp3nP	mít
nového	nový	k2eAgMnSc4d1	nový
partnera	partner	k1gMnSc4	partner
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
snížit	snížit	k5eAaPmF	snížit
u	u	k7c2	u
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
nezadaných	zadaný	k2eNgNnPc2d1	nezadané
sebevědomí	sebevědomí	k1gNnPc2	sebevědomí
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
těžší	těžký	k2eAgMnSc1d2	těžší
se	se	k3xPyFc4	se
s	s	k7c7	s
někým	někdo	k3yInSc7	někdo
rozejít	rozejít	k5eAaPmF	rozejít
<g/>
,	,	kIx,	,
když	když	k8xS	když
i	i	k9	i
po	po	k7c6	po
rozchodu	rozchod	k1gInSc6	rozchod
má	mít	k5eAaImIp3nS	mít
uživatel	uživatel	k1gMnSc1	uživatel
dotyčného	dotyčný	k2eAgInSc2d1	dotyčný
pořád	pořád	k6eAd1	pořád
na	na	k7c6	na
očích	oko	k1gNnPc6	oko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Facebook	Facebook	k1gInSc1	Facebook
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Téma	téma	k1gNnSc1	téma
Facebook	Facebook	k1gInSc1	Facebook
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Kategorie	kategorie	k1gFnSc1	kategorie
Facebook	Facebook	k1gInSc1	Facebook
ve	v	k7c6	v
Wikizprávách	Wikizpráva	k1gFnPc6	Wikizpráva
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
<p>
<s>
Facebook	Facebook	k1gInSc1	Facebook
na	na	k7c6	na
Twitteru	Twitter	k1gInSc6	Twitter
</s>
</p>
<p>
<s>
Facebook	Facebook	k1gInSc1	Facebook
reklama	reklama	k1gFnSc1	reklama
(	(	kIx(	(
<g/>
Facebook	Facebook	k1gInSc1	Facebook
Ads	Ads	k1gFnSc2	Ads
<g/>
)	)	kIx)	)
-	-	kIx~	-
kompletní	kompletní	k2eAgInSc1d1	kompletní
zdroj	zdroj	k1gInSc1	zdroj
o	o	k7c4	o
Facebook	Facebook	k1gInSc4	Facebook
reklamě	reklama	k1gFnSc3	reklama
popsané	popsaný	k2eAgInPc1d1	popsaný
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
</s>
</p>
