<s>
Hemofilová	Hemofilový	k2eAgFnSc1d1	Hemofilový
rýma	rýma	k1gFnSc1	rýma
drůbeže	drůbež	k1gFnSc2	drůbež
je	být	k5eAaImIp3nS	být
akutní	akutní	k2eAgNnSc1d1	akutní
nakažlivé	nakažlivý	k2eAgNnSc1d1	nakažlivé
bakteriální	bakteriální	k2eAgNnSc1d1	bakteriální
onemocnění	onemocnění	k1gNnSc1	onemocnění
horních	horní	k2eAgFnPc2d1	horní
cest	cesta	k1gFnPc2	cesta
dýchacích	dýchací	k2eAgFnPc2d1	dýchací
kura	kur	k1gMnSc2	kur
domácího	domácí	k2eAgMnSc2d1	domácí
<g/>
,	,	kIx,	,
způsobované	způsobovaný	k2eAgNnSc1d1	způsobované
bakterií	bakterie	k1gFnSc7	bakterie
Haemophilus	Haemophilus	k1gInSc4	Haemophilus
paragallinarum	paragallinarum	k1gNnSc4	paragallinarum
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
komplikujících	komplikující	k2eAgInPc2d1	komplikující
faktorů	faktor	k1gInPc2	faktor
často	často	k6eAd1	často
přechází	přecházet	k5eAaImIp3nS	přecházet
v	v	k7c6	v
onemocnění	onemocnění	k1gNnSc6	onemocnění
chronické	chronický	k2eAgNnSc1d1	chronické
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
postižených	postižený	k2eAgNnPc6d1	postižené
hejnech	hejno	k1gNnPc6	hejno
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
ztráty	ztráta	k1gFnSc2	ztráta
ojedinělým	ojedinělý	k2eAgInSc7d1	ojedinělý
úhynem	úhyn	k1gInSc7	úhyn
<g/>
,	,	kIx,	,
zhoršenou	zhoršený	k2eAgFnSc7d1	zhoršená
rentabilitou	rentabilita	k1gFnSc7	rentabilita
chovu	chov	k1gInSc2	chov
a	a	k8xC	a
poklesem	pokles	k1gInSc7	pokles
snášky	snáška	k1gFnSc2	snáška
<g/>
.	.	kIx.	.
</s>
<s>
Infekční	infekční	k2eAgFnSc1d1	infekční
rýma	rýma	k1gFnSc1	rýma
drůbeže	drůbež	k1gFnSc2	drůbež
je	být	k5eAaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
již	již	k6eAd1	již
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Klasifikace	klasifikace	k1gFnSc1	klasifikace
příčinného	příčinný	k2eAgNnSc2d1	příčinné
agens	agens	k1gNnSc2	agens
byla	být	k5eAaImAgFnS	být
nejasná	jasný	k2eNgFnSc1d1	nejasná
po	po	k7c4	po
mnoho	mnoho	k4c4	mnoho
roků	rok	k1gInPc2	rok
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nemoc	nemoc	k1gFnSc1	nemoc
byla	být	k5eAaImAgFnS	být
často	často	k6eAd1	často
maskována	maskovat	k5eAaBmNgFnS	maskovat
smíšenými	smíšený	k2eAgFnPc7d1	smíšená
bakteriálními	bakteriální	k2eAgFnPc7d1	bakteriální
infekcemi	infekce	k1gFnPc7	infekce
a	a	k8xC	a
také	také	k6eAd1	také
komplikována	komplikován	k2eAgFnSc1d1	komplikována
neštovicemi	neštovice	k1gFnPc7	neštovice
<g/>
.	.	kIx.	.
</s>
<s>
Původce	původce	k1gMnSc1	původce
izoloval	izolovat	k5eAaBmAgMnS	izolovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
De	De	k?	De
Blieck	Blieck	k1gMnSc1	Blieck
a	a	k8xC	a
nazval	nazvat	k5eAaPmAgMnS	nazvat
jej	on	k3xPp3gMnSc4	on
Bacillus	Bacillus	k1gMnSc1	Bacillus
hemoglobinophilus	hemoglobinophilus	k1gMnSc1	hemoglobinophilus
coryzae	coryzaat	k5eAaPmIp3nS	coryzaat
gallinarum	gallinarum	k1gInSc4	gallinarum
<g/>
.	.	kIx.	.
</s>
<s>
Onemocnění	onemocnění	k1gNnSc1	onemocnění
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
a	a	k8xC	a
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
ekonomické	ekonomický	k2eAgFnPc4d1	ekonomická
ztráty	ztráta	k1gFnPc4	ztráta
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
zvýšené	zvýšený	k2eAgFnSc2d1	zvýšená
brakace	brakace	k1gFnSc2	brakace
zvířat	zvíře	k1gNnPc2	zvíře
a	a	k8xC	a
redukce	redukce	k1gFnSc1	redukce
snášky	snáška	k1gFnSc2	snáška
(	(	kIx(	(
<g/>
o	o	k7c4	o
10-40	[number]	k4	10-40
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
problémem	problém	k1gInSc7	problém
bývá	bývat	k5eAaImIp3nS	bývat
zejména	zejména	k9	zejména
na	na	k7c6	na
farmách	farma	k1gFnPc6	farma
s	s	k7c7	s
chovem	chov	k1gInSc7	chov
drůbeže	drůbež	k1gFnSc2	drůbež
různého	různý	k2eAgInSc2d1	různý
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
studií	studie	k1gFnPc2	studie
uskutečněných	uskutečněný	k2eAgFnPc2d1	uskutečněná
v	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
bylo	být	k5eAaImAgNnS	být
příčinné	příčinný	k2eAgNnSc4d1	příčinné
agens	agens	k1gNnSc4	agens
rýmy	rýma	k1gFnSc2	rýma
klasifikováno	klasifikován	k2eAgNnSc4d1	klasifikováno
jako	jako	k8xS	jako
Haemophilus	Haemophilus	k1gInSc4	Haemophilus
gallinarum	gallinarum	k1gNnSc4	gallinarum
na	na	k7c6	na
základě	základ	k1gInSc6	základ
jeho	jeho	k3xOp3gInPc2	jeho
požadavků	požadavek	k1gInPc2	požadavek
na	na	k7c4	na
přítomnost	přítomnost	k1gFnSc4	přítomnost
heminu	hemin	k1gInSc2	hemin
(	(	kIx(	(
<g/>
X-faktor	Xaktor	k1gInSc1	X-faktor
<g/>
)	)	kIx)	)
a	a	k8xC	a
nikotinamidadenindinukleotidu	nikotinamidadenindinukleotida	k1gFnSc4	nikotinamidadenindinukleotida
-	-	kIx~	-
NAD	nad	k7c7	nad
(	(	kIx(	(
<g/>
V-faktor	Vaktor	k1gMnSc1	V-faktor
<g/>
)	)	kIx)	)
v	v	k7c6	v
růstových	růstový	k2eAgNnPc6d1	růstové
médiích	médium	k1gNnPc6	médium
<g/>
.	.	kIx.	.
</s>
<s>
Počínaje	počínaje	k7c7	počínaje
rokem	rok	k1gInSc7	rok
1962	[number]	k4	1962
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
objevovat	objevovat	k5eAaImF	objevovat
práce	práce	k1gFnPc1	práce
prokazující	prokazující	k2eAgFnPc1d1	prokazující
<g/>
,	,	kIx,	,
že	že	k8xS	že
novější	nový	k2eAgInPc4d2	novější
izoláty	izolát	k1gInPc4	izolát
H.	H.	kA	H.
gallinarum	gallinarum	k1gInSc4	gallinarum
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
ke	k	k7c3	k
svému	svůj	k3xOyFgInSc3	svůj
růstu	růst	k1gInSc3	růst
pouze	pouze	k6eAd1	pouze
V-faktor	Vaktor	k1gInSc4	V-faktor
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
návrhu	návrh	k1gInSc3	návrh
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
k	k	k7c3	k
akceptování	akceptování	k1gNnSc3	akceptování
nového	nový	k2eAgInSc2d1	nový
druhu	druh	k1gInSc2	druh
H.	H.	kA	H.
paragallinarum	paragallinarum	k1gInSc1	paragallinarum
pro	pro	k7c4	pro
hemofily	hemofil	k1gInPc4	hemofil
izolované	izolovaný	k2eAgInPc4d1	izolovaný
z	z	k7c2	z
případů	případ	k1gInPc2	případ
infekční	infekční	k2eAgFnSc2d1	infekční
rýmy	rýma	k1gFnSc2	rýma
u	u	k7c2	u
drůbeže	drůbež	k1gFnSc2	drůbež
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
další	další	k2eAgFnPc4d1	další
růstové	růstový	k2eAgFnPc4d1	růstová
vlastnosti	vlastnost	k1gFnPc4	vlastnost
i	i	k8xC	i
schopnost	schopnost	k1gFnSc4	schopnost
vyvolávat	vyvolávat	k5eAaImF	vyvolávat
nemoc	nemoc	k1gFnSc4	nemoc
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
oba	dva	k4xCgInPc1	dva
druhy	druh	k1gInPc1	druh
identické	identický	k2eAgInPc1d1	identický
<g/>
.	.	kIx.	.
</s>
<s>
H.	H.	kA	H.
paragallinarum	paragallinarum	k1gNnSc1	paragallinarum
je	být	k5eAaImIp3nS	být
G-	G-	k1gFnSc1	G-
<g/>
,	,	kIx,	,
nepohyblivá	pohyblivý	k2eNgFnSc1d1	nepohyblivá
<g/>
,	,	kIx,	,
opouzdřená	opouzdřený	k2eAgFnSc1d1	opouzdřená
kokobakterie	kokobakterie	k1gFnSc1	kokobakterie
až	až	k8xS	až
tyčinka	tyčinka	k1gFnSc1	tyčinka
velikosti	velikost	k1gFnSc2	velikost
1-3	[number]	k4	1-3
x	x	k?	x
0,4	[number]	k4	0,4
<g/>
-	-	kIx~	-
<g/>
0,8	[number]	k4	0,8
μ	μ	k?	μ
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
i	i	k9	i
vláknité	vláknitý	k2eAgFnPc1d1	vláknitá
nebo	nebo	k8xC	nebo
pleomorfní	pleomorfní	k2eAgFnPc1d1	pleomorfní
formy	forma	k1gFnPc1	forma
<g/>
.	.	kIx.	.
</s>
<s>
Spory	spor	k1gInPc1	spor
netvoří	tvořit	k5eNaImIp3nP	tvořit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zorném	zorný	k2eAgNnSc6d1	zorné
poli	pole	k1gNnSc6	pole
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
jednotlivě	jednotlivě	k6eAd1	jednotlivě
<g/>
,	,	kIx,	,
v	v	k7c6	v
párech	pár	k1gInPc6	pár
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
krátké	krátký	k2eAgInPc4d1	krátký
řetízky	řetízek	k1gInPc4	řetízek
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
déle	dlouho	k6eAd2	dlouho
trvající	trvající	k2eAgFnSc6d1	trvající
kultivaci	kultivace	k1gFnSc6	kultivace
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
morfologické	morfologický	k2eAgFnSc3d1	morfologická
degeneraci	degenerace	k1gFnSc3	degenerace
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
obyčejném	obyčejný	k2eAgInSc6d1	obyčejný
krevním	krevní	k2eAgInSc6d1	krevní
agaru	agar	k1gInSc6	agar
roste	růst	k5eAaImIp3nS	růst
H.	H.	kA	H.
paragallinarum	paragallinarum	k1gInSc1	paragallinarum
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
sousedství	sousedství	k1gNnSc6	sousedství
stafylokoků	stafylokok	k1gInPc2	stafylokok
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
satelitní	satelitní	k2eAgInSc1d1	satelitní
růst	růst	k1gInSc1	růst
<g/>
)	)	kIx)	)
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
malých	malý	k2eAgMnPc2d1	malý
(	(	kIx(	(
<g/>
0,3	[number]	k4	0,3
mm	mm	kA	mm
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
<g/>
)	)	kIx)	)
kolonií	kolonie	k1gFnPc2	kolonie
připomínajících	připomínající	k2eAgFnPc2d1	připomínající
kapky	kapka	k1gFnPc4	kapka
rosy	rosa	k1gFnSc2	rosa
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
hemolýzy	hemolýza	k1gFnSc2	hemolýza
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
ptačí	ptačí	k2eAgInPc1d1	ptačí
hemofily	hemofil	k1gInPc1	hemofil
redukují	redukovat	k5eAaBmIp3nP	redukovat
nitráty	nitrát	k1gInPc4	nitrát
a	a	k8xC	a
zkvašují	zkvašovat	k5eAaImIp3nP	zkvašovat
glukózu	glukóza	k1gFnSc4	glukóza
bez	bez	k7c2	bez
tvorby	tvorba	k1gFnSc2	tvorba
plynu	plyn	k1gInSc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Neprodukují	produkovat	k5eNaImIp3nP	produkovat
indol	indol	k1gInSc4	indol
a	a	k8xC	a
nehydrolyzují	hydrolyzovat	k5eNaBmIp3nP	hydrolyzovat
ureu	urea	k1gFnSc4	urea
ani	ani	k8xC	ani
želatinu	želatina	k1gFnSc4	želatina
<g/>
.	.	kIx.	.
</s>
<s>
Aktivita	aktivita	k1gFnSc1	aktivita
oxidázy	oxidáza	k1gFnSc2	oxidáza
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
variabilní	variabilní	k2eAgInSc1d1	variabilní
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
aglutinace	aglutinace	k1gFnSc2	aglutinace
byly	být	k5eAaImAgInP	být
izoláty	izolát	k1gInPc1	izolát
H.	H.	kA	H.
paragallinarum	paragallinarum	k1gNnSc1	paragallinarum
rozděleny	rozdělit	k5eAaPmNgInP	rozdělit
do	do	k7c2	do
tří	tři	k4xCgFnPc2	tři
serovarů	serovar	k1gInPc2	serovar
-	-	kIx~	-
A	A	kA	A
<g/>
,	,	kIx,	,
B	B	kA	B
a	a	k8xC	a
C	C	kA	C
(	(	kIx(	(
<g/>
Page	Pag	k1gMnSc2	Pag
<g/>
,	,	kIx,	,
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kume	kum	k1gMnSc5	kum
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
navrhli	navrhnout	k5eAaPmAgMnP	navrhnout
alternativní	alternativní	k2eAgFnSc4d1	alternativní
sérologickou	sérologický	k2eAgFnSc4d1	sérologická
klasifikaci	klasifikace	k1gFnSc4	klasifikace
založenou	založený	k2eAgFnSc4d1	založená
na	na	k7c6	na
hemaglutinačně	hemaglutinačně	k6eAd1	hemaglutinačně
inhibičním	inhibiční	k2eAgInSc6d1	inhibiční
testu	test	k1gInSc6	test
s	s	k7c7	s
použitím	použití	k1gNnSc7	použití
buněk	buňka	k1gFnPc2	buňka
opracovaných	opracovaný	k2eAgFnPc2d1	opracovaná
thiokyanátem	thiokyanát	k1gInSc7	thiokyanát
draselným	draselný	k2eAgInSc7d1	draselný
<g/>
,	,	kIx,	,
králičího	králičí	k2eAgNnSc2d1	králičí
hyperimunního	hyperimunní	k2eAgNnSc2d1	hyperimunní
séra	sérum	k1gNnSc2	sérum
a	a	k8xC	a
kuřecích	kuřecí	k2eAgInPc2d1	kuřecí
erytrocytů	erytrocyt	k1gInPc2	erytrocyt
fixovaných	fixovaný	k2eAgMnPc2d1	fixovaný
glutaraldehydem	glutaraldehyd	k1gInSc7	glutaraldehyd
<g/>
.	.	kIx.	.
</s>
<s>
Pageho	Pageze	k6eAd1	Pageze
sérovary	sérovar	k1gInPc1	sérovar
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
systému	systém	k1gInSc6	systém
povýšeny	povýšen	k2eAgFnPc1d1	povýšena
na	na	k7c4	na
séroskupiny	séroskupina	k1gFnPc4	séroskupina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
tři	tři	k4xCgFnPc1	tři
séroskupiny	séroskupina	k1gFnPc1	séroskupina
-	-	kIx~	-
A	A	kA	A
<g/>
,	,	kIx,	,
B	B	kA	B
a	a	k8xC	a
C	C	kA	C
<g/>
,	,	kIx,	,
zahrnující	zahrnující	k2eAgInSc4d1	zahrnující
celkově	celkově	k6eAd1	celkově
9	[number]	k4	9
sérovarů	sérovar	k1gInPc2	sérovar
(	(	kIx(	(
<g/>
A-	A-	k1gMnSc1	A-
<g/>
1	[number]	k4	1
až	až	k6eAd1	až
A-	A-	k1gFnSc4	A-
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
B-	B-	k1gFnSc1	B-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
C-1	C-1	k1gFnSc1	C-1
až	až	k8xS	až
C-	C-	k1gFnSc1	C-
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Klasifikace	klasifikace	k1gFnSc1	klasifikace
kmenů	kmen	k1gInPc2	kmen
H.	H.	kA	H.
paragallinarum	paragallinarum	k1gInSc1	paragallinarum
se	se	k3xPyFc4	se
tradičně	tradičně	k6eAd1	tradičně
dělá	dělat	k5eAaImIp3nS	dělat
aglutinací	aglutinace	k1gFnSc7	aglutinace
<g/>
.	.	kIx.	.
</s>
<s>
Typizace	typizace	k1gFnSc1	typizace
podle	podle	k7c2	podle
fermentace	fermentace	k1gFnSc2	fermentace
cukrů	cukr	k1gInPc2	cukr
a	a	k8xC	a
antimikrobiální	antimikrobiální	k2eAgFnSc1d1	antimikrobiální
rezistence	rezistence	k1gFnSc1	rezistence
se	se	k3xPyFc4	se
neosvědčila	osvědčit	k5eNaPmAgFnS	osvědčit
<g/>
.	.	kIx.	.
</s>
<s>
H.	H.	kA	H.
paragallinarum	paragallinarum	k1gNnSc1	paragallinarum
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
citlivý	citlivý	k2eAgInSc1d1	citlivý
organismus	organismus	k1gInSc1	organismus
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
mimo	mimo	k7c4	mimo
tělo	tělo	k1gNnSc4	tělo
hostitele	hostitel	k1gMnSc2	hostitel
rychle	rychle	k6eAd1	rychle
inaktivován	inaktivovat	k5eAaBmNgMnS	inaktivovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
haly	hala	k1gFnSc2	hala
při	při	k7c6	při
18-24	[number]	k4	18-24
°	°	k?	°
<g/>
C	C	kA	C
přežívá	přežívat	k5eAaImIp3nS	přežívat
maximálně	maximálně	k6eAd1	maximálně
48	[number]	k4	48
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
izoláty	izolát	k1gInPc4	izolát
H.	H.	kA	H.
paragallinarum	paragallinarum	k1gInSc4	paragallinarum
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
lišit	lišit	k5eAaImF	lišit
virulencí	virulence	k1gFnSc7	virulence
<g/>
;	;	kIx,	;
byla	být	k5eAaImAgFnS	být
popsána	popsán	k2eAgFnSc1d1	popsána
řada	řada	k1gFnSc1	řada
faktorů	faktor	k1gInPc2	faktor
souvisejících	související	k2eAgInPc2d1	související
s	s	k7c7	s
patogenitou	patogenita	k1gFnSc7	patogenita
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
nejvýznamnější	významný	k2eAgInPc1d3	nejvýznamnější
se	se	k3xPyFc4	se
zdají	zdát	k5eAaPmIp3nP	zdát
být	být	k5eAaImF	být
polysacharidové	polysacharidový	k2eAgInPc4d1	polysacharidový
antigeny	antigen	k1gInPc4	antigen
pouzdra	pouzdro	k1gNnSc2	pouzdro
a	a	k8xC	a
hemaglutinační	hemaglutinační	k2eAgInPc4d1	hemaglutinační
antigeny	antigen	k1gInPc4	antigen
<g/>
.	.	kIx.	.
</s>
<s>
Zmiňovány	zmiňován	k2eAgInPc1d1	zmiňován
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
cytotoxické	cytotoxický	k2eAgInPc1d1	cytotoxický
faktory	faktor	k1gInPc1	faktor
<g/>
,	,	kIx,	,
neuraminidáza	neuraminidáza	k1gFnSc1	neuraminidáza
a	a	k8xC	a
endotoxin	endotoxin	k1gInSc1	endotoxin
<g/>
.	.	kIx.	.
</s>
<s>
Hemofilová	Hemofilový	k2eAgFnSc1d1	Hemofilový
rýma	rýma	k1gFnSc1	rýma
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
nejčastěji	často	k6eAd3	často
na	na	k7c6	na
farmách	farma	k1gFnPc6	farma
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
chová	chovat	k5eAaImIp3nS	chovat
drůbež	drůbež	k1gFnSc1	drůbež
různých	různý	k2eAgFnPc2d1	různá
věkových	věkový	k2eAgFnPc2d1	věková
kategorií	kategorie	k1gFnPc2	kategorie
<g/>
,	,	kIx,	,
s	s	k7c7	s
nedostatečnou	dostatečný	k2eNgFnSc7d1	nedostatečná
zoohygienou	zoohygiena	k1gFnSc7	zoohygiena
a	a	k8xC	a
technologií	technologie	k1gFnSc7	technologie
chovu	chov	k1gInSc2	chov
a	a	k8xC	a
relativně	relativně	k6eAd1	relativně
častěji	často	k6eAd2	často
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
a	a	k8xC	a
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
<g/>
.	.	kIx.	.
</s>
<s>
Přirozeným	přirozený	k2eAgMnSc7d1	přirozený
hostitelem	hostitel	k1gMnSc7	hostitel
H.	H.	kA	H.
paragallinarum	paragallinarum	k1gInSc4	paragallinarum
je	být	k5eAaImIp3nS	být
kur	kur	k1gMnSc1	kur
domácí	domácí	k2eAgMnSc1d1	domácí
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vnímavý	vnímavý	k2eAgInSc1d1	vnímavý
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
věk	věk	k1gInSc4	věk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
u	u	k7c2	u
dospělých	dospělý	k2eAgMnPc2d1	dospělý
ptáků	pták	k1gMnPc2	pták
obvykle	obvykle	k6eAd1	obvykle
probíhá	probíhat	k5eAaImIp3nS	probíhat
nemoc	nemoc	k1gFnSc4	nemoc
vážněji	vážně	k6eAd2	vážně
<g/>
,	,	kIx,	,
s	s	k7c7	s
kratší	krátký	k2eAgFnSc7d2	kratší
inkubační	inkubační	k2eAgFnSc7d1	inkubační
dobou	doba	k1gFnSc7	doba
a	a	k8xC	a
delší	dlouhý	k2eAgFnSc7d2	delší
dobou	doba	k1gFnSc7	doba
trvání	trvání	k1gNnSc2	trvání
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
experimentální	experimentální	k2eAgFnSc3d1	experimentální
infekci	infekce	k1gFnSc3	infekce
H.	H.	kA	H.
paragallinarum	paragallinarum	k1gInSc4	paragallinarum
jsou	být	k5eAaImIp3nP	být
refrakterní	refrakterní	k2eAgFnPc1d1	refrakterní
(	(	kIx(	(
<g/>
nereagující	reagující	k2eNgFnPc1d1	nereagující
<g/>
)	)	kIx)	)
krůty	krůta	k1gFnPc1	krůta
<g/>
,	,	kIx,	,
holubi	holub	k1gMnPc1	holub
<g/>
,	,	kIx,	,
vrabci	vrabec	k1gMnPc1	vrabec
<g/>
,	,	kIx,	,
kachny	kachna	k1gFnPc1	kachna
<g/>
,	,	kIx,	,
vrány	vrána	k1gFnPc1	vrána
<g/>
,	,	kIx,	,
králíci	králík	k1gMnPc1	králík
<g/>
,	,	kIx,	,
morčata	morče	k1gNnPc1	morče
a	a	k8xC	a
myši	myš	k1gFnPc1	myš
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
hlavní	hlavní	k2eAgInSc4d1	hlavní
rezervoár	rezervoár	k1gInSc4	rezervoár
infekce	infekce	k1gFnSc2	infekce
jsou	být	k5eAaImIp3nP	být
považováni	považován	k2eAgMnPc1d1	považován
chroničtí	chronický	k2eAgMnPc1d1	chronický
bacilonosiči	bacilonosič	k1gMnPc1	bacilonosič
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
již	již	k6eAd1	již
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
subklinicky	subklinicky	k6eAd1	subklinicky
infikované	infikovaný	k2eAgMnPc4d1	infikovaný
ptáky	pták	k1gMnPc4	pták
nebo	nebo	k8xC	nebo
rekonvalescenty	rekonvalescent	k1gMnPc4	rekonvalescent
<g/>
.	.	kIx.	.
</s>
<s>
Přenos	přenos	k1gInSc1	přenos
se	se	k3xPyFc4	se
převážně	převážně	k6eAd1	převážně
děje	dít	k5eAaImIp3nS	dít
přímým	přímý	k2eAgInSc7d1	přímý
horizontálním	horizontální	k2eAgInSc7d1	horizontální
kontaktem	kontakt	k1gInSc7	kontakt
vnímavých	vnímavý	k2eAgNnPc2d1	vnímavé
zvířat	zvíře	k1gNnPc2	zvíře
s	s	k7c7	s
infikovanými	infikovaný	k2eAgInPc7d1	infikovaný
(	(	kIx(	(
<g/>
kapénkovou	kapénkový	k2eAgFnSc7d1	kapénková
infekcí	infekce	k1gFnSc7	infekce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nosními	nosní	k2eAgInPc7d1	nosní
sekrety	sekret	k1gInPc7	sekret
kontaminovaným	kontaminovaný	k2eAgNnSc7d1	kontaminované
krmivem	krmivo	k1gNnSc7	krmivo
<g/>
,	,	kIx,	,
vodou	voda	k1gFnSc7	voda
či	či	k8xC	či
jinými	jiný	k2eAgInPc7d1	jiný
předměty	předmět	k1gInPc7	předmět
či	či	k8xC	či
pomůckami	pomůcka	k1gFnPc7	pomůcka
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
vyvolání	vyvolání	k1gNnSc4	vyvolání
infekce	infekce	k1gFnSc2	infekce
stačí	stačit	k5eAaBmIp3nS	stačit
i	i	k9	i
malé	malý	k2eAgNnSc1d1	malé
množství	množství	k1gNnSc1	množství
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
.	.	kIx.	.
</s>
<s>
Infekce	infekce	k1gFnSc1	infekce
není	být	k5eNaImIp3nS	být
transovariálně	transovariálně	k6eAd1	transovariálně
přenosná	přenosný	k2eAgFnSc1d1	přenosná
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
infekci	infekce	k1gFnSc6	infekce
H.	H.	kA	H.
paragallinarum	paragallinarum	k1gInSc1	paragallinarum
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
adherenci	adherence	k1gFnSc3	adherence
bakterií	bakterie	k1gFnPc2	bakterie
na	na	k7c4	na
řasinkový	řasinkový	k2eAgInSc4d1	řasinkový
epitel	epitel	k1gInSc4	epitel
sliznice	sliznice	k1gFnSc2	sliznice
horního	horní	k2eAgInSc2d1	horní
respiračního	respirační	k2eAgInSc2d1	respirační
traktu	trakt	k1gInSc2	trakt
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
kolonizaci	kolonizace	k1gFnSc6	kolonizace
sliznice	sliznice	k1gFnSc2	sliznice
se	se	k3xPyFc4	se
uplatňují	uplatňovat	k5eAaImIp3nP	uplatňovat
pouzdro	pouzdro	k1gNnSc1	pouzdro
a	a	k8xC	a
hemaglutinační	hemaglutinační	k2eAgInSc1d1	hemaglutinační
antigen	antigen	k1gInSc1	antigen
<g/>
.	.	kIx.	.
</s>
<s>
Toxické	toxický	k2eAgFnPc1d1	toxická
látky	látka	k1gFnPc1	látka
uvolňované	uvolňovaný	k2eAgFnPc1d1	uvolňovaná
během	během	k7c2	během
proliferace	proliferace	k1gFnSc2	proliferace
bakterií	bakterie	k1gFnPc2	bakterie
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
změny	změna	k1gFnPc1	změna
na	na	k7c4	na
sliznici	sliznice	k1gFnSc4	sliznice
a	a	k8xC	a
následně	následně	k6eAd1	následně
klinické	klinický	k2eAgInPc4d1	klinický
příznaky	příznak	k1gInPc4	příznak
<g/>
.	.	kIx.	.
</s>
<s>
H.	H.	kA	H.
paragallinarum	paragallinarum	k1gNnSc1	paragallinarum
nepatří	patřit	k5eNaImIp3nS	patřit
mezi	mezi	k7c4	mezi
invazivní	invazivní	k2eAgFnPc4d1	invazivní
bakterie	bakterie	k1gFnPc4	bakterie
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
výrazný	výrazný	k2eAgInSc1d1	výrazný
tropismus	tropismus	k1gInSc1	tropismus
pro	pro	k7c4	pro
řasinkové	řasinkový	k2eAgFnPc4d1	řasinkový
buňky	buňka	k1gFnPc4	buňka
a	a	k8xC	a
do	do	k7c2	do
dolních	dolní	k2eAgFnPc2d1	dolní
respiračních	respirační	k2eAgFnPc2d1	respirační
cest	cesta	k1gFnPc2	cesta
(	(	kIx(	(
<g/>
plíce	plíce	k1gFnPc1	plíce
<g/>
,	,	kIx,	,
vzdušné	vzdušný	k2eAgInPc1d1	vzdušný
vaky	vak	k1gInPc1	vak
<g/>
)	)	kIx)	)
migruje	migrovat	k5eAaImIp3nS	migrovat
pouze	pouze	k6eAd1	pouze
v	v	k7c4	v
interakci	interakce	k1gFnSc4	interakce
s	s	k7c7	s
jinými	jiný	k2eAgNnPc7d1	jiné
infekčními	infekční	k2eAgNnPc7d1	infekční
agens	agens	k1gNnPc7	agens
anebo	anebo	k8xC	anebo
v	v	k7c6	v
případě	případ	k1gInSc6	případ
imunosuprese	imunosuprese	k1gFnSc2	imunosuprese
hostitele	hostitel	k1gMnSc2	hostitel
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
stavu	stav	k1gInSc2	stav
chronického	chronický	k2eAgNnSc2d1	chronické
onemocnění	onemocnění	k1gNnSc2	onemocnění
přechází	přecházet	k5eAaImIp3nS	přecházet
rýma	rýma	k1gFnSc1	rýma
zpravidla	zpravidla	k6eAd1	zpravidla
působením	působení	k1gNnSc7	působení
konkurentních	konkurentní	k2eAgFnPc2d1	konkurentní
infekcí	infekce	k1gFnPc2	infekce
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
hemofilovou	hemofilův	k2eAgFnSc7d1	hemofilův
rýmu	rým	k1gInSc3	rým
je	být	k5eAaImIp3nS	být
charakteristické	charakteristický	k2eAgNnSc1d1	charakteristické
rychlé	rychlý	k2eAgNnSc1d1	rychlé
šíření	šíření	k1gNnSc1	šíření
nemoci	nemoc	k1gFnSc2	nemoc
v	v	k7c6	v
hejnu	hejno	k1gNnSc6	hejno
(	(	kIx(	(
<g/>
inkubační	inkubační	k2eAgFnSc1d1	inkubační
doba	doba	k1gFnSc1	doba
je	být	k5eAaImIp3nS	být
1-3	[number]	k4	1-3
dny	den	k1gInPc4	den
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vysoká	vysoký	k2eAgFnSc1d1	vysoká
morbidita	morbidita	k1gFnSc1	morbidita
a	a	k8xC	a
nízká	nízký	k2eAgFnSc1d1	nízká
mortalita	mortalita	k1gFnSc1	mortalita
<g/>
.	.	kIx.	.
</s>
<s>
Častější	častý	k2eAgNnSc1d2	častější
je	být	k5eAaImIp3nS	být
onemocnění	onemocnění	k1gNnSc1	onemocnění
dospívající	dospívající	k2eAgNnSc1d1	dospívající
a	a	k8xC	a
pohlavně	pohlavně	k6eAd1	pohlavně
dospělé	dospělý	k2eAgFnPc4d1	dospělá
drůbeže	drůbež	k1gFnPc4	drůbež
<g/>
.	.	kIx.	.
</s>
<s>
Průběh	průběh	k1gInSc1	průběh
nemoci	nemoc	k1gFnSc2	nemoc
je	být	k5eAaImIp3nS	být
ovlivňován	ovlivňovat	k5eAaImNgInS	ovlivňovat
virulencí	virulence	k1gFnSc7	virulence
původce	původce	k1gMnSc1	původce
<g/>
,	,	kIx,	,
věkem	věk	k1gInSc7	věk
<g/>
,	,	kIx,	,
plemenem	plemeno	k1gNnSc7	plemeno
<g/>
,	,	kIx,	,
podmínkami	podmínka	k1gFnPc7	podmínka
chovu	chov	k1gInSc2	chov
a	a	k8xC	a
konkurentními	konkurentní	k2eAgFnPc7d1	konkurentní
(	(	kIx(	(
<g/>
současně	současně	k6eAd1	současně
probíhajícími	probíhající	k2eAgFnPc7d1	probíhající
<g/>
)	)	kIx)	)
infekcemi	infekce	k1gFnPc7	infekce
<g/>
.	.	kIx.	.
</s>
<s>
Nedostatky	nedostatek	k1gInPc1	nedostatek
v	v	k7c6	v
ustájení	ustájení	k1gNnSc6	ustájení
<g/>
,	,	kIx,	,
výživě	výživa	k1gFnSc6	výživa
<g/>
,	,	kIx,	,
parazitární	parazitární	k2eAgFnSc1d1	parazitární
<g/>
,	,	kIx,	,
bakteriální	bakteriální	k2eAgFnSc1d1	bakteriální
(	(	kIx(	(
<g/>
mykoplasmata	mykoplasma	k1gNnPc1	mykoplasma
<g/>
,	,	kIx,	,
pasteurely	pasteurel	k1gInPc1	pasteurel
<g/>
)	)	kIx)	)
i	i	k8xC	i
virové	virový	k2eAgFnPc1d1	virová
infekce	infekce	k1gFnPc1	infekce
(	(	kIx(	(
<g/>
infekční	infekční	k2eAgFnSc1d1	infekční
bronchitida	bronchitida	k1gFnSc1	bronchitida
<g/>
,	,	kIx,	,
infekční	infekční	k2eAgFnSc1d1	infekční
laryngotracheitida	laryngotracheitida	k1gFnSc1	laryngotracheitida
<g/>
,	,	kIx,	,
neštovice	neštovice	k1gFnPc1	neštovice
<g/>
)	)	kIx)	)
zhoršují	zhoršovat	k5eAaImIp3nP	zhoršovat
průběh	průběh	k1gInSc4	průběh
nemoci	nemoc	k1gFnSc2	nemoc
a	a	k8xC	a
prodlužují	prodlužovat	k5eAaImIp3nP	prodlužovat
dobu	doba	k1gFnSc4	doba
jejího	její	k3xOp3gNnSc2	její
trvání	trvání	k1gNnSc2	trvání
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zavlečení	zavlečení	k1gNnSc6	zavlečení
H.	H.	kA	H.
paragallinarum	paragallinarum	k1gInSc1	paragallinarum
do	do	k7c2	do
chovu	chov	k1gInSc2	chov
se	se	k3xPyFc4	se
infekce	infekce	k1gFnSc1	infekce
může	moct	k5eAaImIp3nS	moct
rozšířit	rozšířit	k5eAaPmF	rozšířit
na	na	k7c4	na
celé	celý	k2eAgNnSc4d1	celé
hejno	hejno	k1gNnSc4	hejno
během	během	k7c2	během
7-10	[number]	k4	7-10
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
nedojde	dojít	k5eNaPmIp3nS	dojít
ke	k	k7c3	k
komplikacím	komplikace	k1gFnPc3	komplikace
<g/>
,	,	kIx,	,
trvá	trvat	k5eAaImIp3nS	trvat
mírná	mírný	k2eAgFnSc1d1	mírná
forma	forma	k1gFnSc1	forma
nemoci	nemoc	k1gFnSc2	nemoc
asi	asi	k9	asi
10	[number]	k4	10
dní	den	k1gInPc2	den
a	a	k8xC	a
vážnější	vážní	k2eAgNnSc1d2	vážnější
přibližně	přibližně	k6eAd1	přibližně
2-3	[number]	k4	2-3
týdny	týden	k1gInPc4	týden
<g/>
.	.	kIx.	.
</s>
<s>
Trvání	trvání	k1gNnSc1	trvání
nemoci	nemoc	k1gFnSc2	nemoc
kolísá	kolísat	k5eAaImIp3nS	kolísat
podle	podle	k7c2	podle
množství	množství	k1gNnSc2	množství
inokula	inokulum	k1gNnSc2	inokulum
a	a	k8xC	a
virulence	virulence	k1gFnSc2	virulence
bakterie	bakterie	k1gFnSc2	bakterie
<g/>
.	.	kIx.	.
</s>
<s>
Prolongovaný	prolongovaný	k2eAgInSc1d1	prolongovaný
průběh	průběh	k1gInSc1	průběh
nemoci	nemoc	k1gFnSc2	nemoc
bývá	bývat	k5eAaImIp3nS	bývat
zpravidla	zpravidla	k6eAd1	zpravidla
způsobem	způsob	k1gInSc7	způsob
konkurentní	konkurentní	k2eAgFnSc7d1	konkurentní
infekcí	infekce	k1gFnSc7	infekce
mykoplasmaty	mykoplasma	k1gNnPc7	mykoplasma
nebo	nebo	k8xC	nebo
jiným	jiný	k2eAgInSc7d1	jiný
patogenem	patogen	k1gInSc7	patogen
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýraznějším	výrazný	k2eAgInSc7d3	nejvýraznější
příznakem	příznak	k1gInSc7	příznak
je	být	k5eAaImIp3nS	být
postižení	postižení	k1gNnSc1	postižení
horních	horní	k2eAgFnPc2d1	horní
cest	cesta	k1gFnPc2	cesta
dýchacích	dýchací	k2eAgFnPc2d1	dýchací
<g/>
,	,	kIx,	,
projevující	projevující	k2eAgInPc4d1	projevující
se	s	k7c7	s
serózním	serózní	k2eAgInSc7d1	serózní
<g/>
,	,	kIx,	,
hlenovitým	hlenovitý	k2eAgInSc7d1	hlenovitý
až	až	k8xS	až
fibrinózním	fibrinózní	k2eAgInSc7d1	fibrinózní
výtokem	výtok	k1gInSc7	výtok
z	z	k7c2	z
nosu	nos	k1gInSc2	nos
a	a	k8xC	a
otokem	otok	k1gInSc7	otok
tváře	tvář	k1gFnSc2	tvář
<g/>
,	,	kIx,	,
připomínajícím	připomínající	k2eAgInSc7d1	připomínající
syndrom	syndrom	k1gInSc4	syndrom
otoku	otok	k1gInSc2	otok
hlavy	hlava	k1gFnSc2	hlava
a	a	k8xC	a
konjunktivitidu	konjunktivitida	k1gFnSc4	konjunktivitida
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
zduřelé	zduřelý	k2eAgInPc4d1	zduřelý
lalůčky	lalůček	k1gInPc4	lalůček
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
u	u	k7c2	u
samců	samec	k1gMnPc2	samec
<g/>
,	,	kIx,	,
a	a	k8xC	a
průjem	průjem	k1gInSc1	průjem
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
ptáků	pták	k1gMnPc2	pták
s	s	k7c7	s
postižením	postižení	k1gNnSc7	postižení
dolního	dolní	k2eAgInSc2d1	dolní
respiračního	respirační	k2eAgInSc2d1	respirační
traktu	trakt	k1gInSc2	trakt
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
výrazné	výrazný	k2eAgFnPc4d1	výrazná
dýchací	dýchací	k2eAgFnPc4d1	dýchací
potíže	potíž	k1gFnPc4	potíž
a	a	k8xC	a
šelesty	šelest	k1gInPc4	šelest
<g/>
.	.	kIx.	.
</s>
<s>
Příjem	příjem	k1gInSc1	příjem
krmiva	krmivo	k1gNnSc2	krmivo
i	i	k8xC	i
vody	voda	k1gFnSc2	voda
je	být	k5eAaImIp3nS	být
snížen	snížit	k5eAaPmNgInS	snížit
<g/>
,	,	kIx,	,
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
se	se	k3xPyFc4	se
množství	množství	k1gNnSc2	množství
brakovaných	brakovaný	k2eAgNnPc2d1	brakovaný
zvířat	zvíře	k1gNnPc2	zvíře
a	a	k8xC	a
u	u	k7c2	u
nosnic	nosnice	k1gFnPc2	nosnice
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
poklesu	pokles	k1gInSc3	pokles
snášky	snáška	k1gFnSc2	snáška
(	(	kIx(	(
<g/>
o	o	k7c4	o
10-40	[number]	k4	10-40
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
halách	hala	k1gFnPc6	hala
s	s	k7c7	s
chronicky	chronicky	k6eAd1	chronicky
postiženými	postižený	k2eAgNnPc7d1	postižené
zvířaty	zvíře	k1gNnPc7	zvíře
nebo	nebo	k8xC	nebo
při	při	k7c6	při
komplikujících	komplikující	k2eAgFnPc6d1	komplikující
bakteriálních	bakteriální	k2eAgFnPc6d1	bakteriální
infekcích	infekce	k1gFnPc6	infekce
je	být	k5eAaImIp3nS	být
pozorována	pozorovat	k5eAaImNgFnS	pozorovat
vyšší	vysoký	k2eAgFnSc1d2	vyšší
vlhkost	vlhkost	k1gFnSc1	vlhkost
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
cítit	cítit	k5eAaImF	cítit
nepříjemný	příjemný	k2eNgInSc4d1	nepříjemný
zápach	zápach	k1gInSc4	zápach
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
výkrmových	výkrmový	k2eAgNnPc2d1	výkrmový
kuřat	kuře	k1gNnPc2	kuře
byla	být	k5eAaImAgFnS	být
také	také	k9	také
popsána	popsat	k5eAaPmNgFnS	popsat
artritida	artritida	k1gFnSc1	artritida
<g/>
,	,	kIx,	,
u	u	k7c2	u
nosnic	nosnice	k1gFnPc2	nosnice
septikémie	septikémie	k1gFnSc2	septikémie
<g/>
.	.	kIx.	.
</s>
<s>
Pitevní	pitevní	k2eAgInSc1d1	pitevní
nález	nález	k1gInSc1	nález
při	při	k7c6	při
rýmě	rýma	k1gFnSc6	rýma
je	být	k5eAaImIp3nS	být
charakterizován	charakterizovat	k5eAaBmNgInS	charakterizovat
akutním	akutní	k2eAgInSc7d1	akutní
katarálním	katarální	k2eAgInSc7d1	katarální
zánětem	zánět	k1gInSc7	zánět
sliznic	sliznice	k1gFnPc2	sliznice
nosu	nos	k1gInSc2	nos
a	a	k8xC	a
sinusů	sinus	k1gInPc2	sinus
<g/>
,	,	kIx,	,
katarální	katarální	k2eAgFnSc7d1	katarální
konjunktivitidou	konjunktivitida	k1gFnSc7	konjunktivitida
a	a	k8xC	a
podkožním	podkožní	k2eAgInSc7d1	podkožní
edémem	edém	k1gInSc7	edém
obličeje	obličej	k1gInSc2	obličej
a	a	k8xC	a
lalůčků	lalůček	k1gInPc2	lalůček
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
chronickém	chronický	k2eAgInSc6d1	chronický
komplikovaném	komplikovaný	k2eAgInSc6d1	komplikovaný
průběhu	průběh	k1gInSc6	průběh
(	(	kIx(	(
<g/>
bakterie	bakterie	k1gFnSc1	bakterie
<g/>
,	,	kIx,	,
virusy	virus	k1gInPc1	virus
<g/>
,	,	kIx,	,
Candida	Candida	k1gFnSc1	Candida
spp	spp	k?	spp
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
nacházet	nacházet	k5eAaImF	nacházet
až	až	k9	až
fibrinopurulentní	fibrinopurulentní	k2eAgFnSc1d1	fibrinopurulentní
rhinita	rhinita	k1gFnSc1	rhinita
<g/>
,	,	kIx,	,
tracheitida	tracheitida	k1gFnSc1	tracheitida
(	(	kIx(	(
<g/>
zánět	zánět	k1gInSc1	zánět
průdušnice	průdušnice	k1gFnSc2	průdušnice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bronchopneumonie	bronchopneumonie	k1gFnSc1	bronchopneumonie
(	(	kIx(	(
<g/>
zánět	zánět	k1gInSc1	zánět
plic	plíce	k1gFnPc2	plíce
a	a	k8xC	a
průdušek	průduška	k1gFnPc2	průduška
<g/>
)	)	kIx)	)
a	a	k8xC	a
aerosakulitida	aerosakulitida	k1gFnSc1	aerosakulitida
(	(	kIx(	(
<g/>
zánět	zánět	k1gInSc1	zánět
vzdušných	vzdušný	k2eAgInPc2d1	vzdušný
vaků	vak	k1gInPc2	vak
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Předběžnou	předběžný	k2eAgFnSc4d1	předběžná
diagnózu	diagnóza	k1gFnSc4	diagnóza
lze	lze	k6eAd1	lze
stanovit	stanovit	k5eAaPmF	stanovit
na	na	k7c6	na
základě	základ	k1gInSc6	základ
rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
šířícího	šířící	k2eAgNnSc2d1	šířící
respiračního	respirační	k2eAgNnSc2d1	respirační
onemocnění	onemocnění	k1gNnSc2	onemocnění
v	v	k7c6	v
hejnu	hejno	k1gNnSc6	hejno
kura	kura	k1gFnSc1	kura
domácího	domácí	k2eAgMnSc2d1	domácí
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jsou	být	k5eAaImIp3nP	být
postiženy	postihnout	k5eAaPmNgFnP	postihnout
zejména	zejména	k9	zejména
horní	horní	k2eAgFnPc1d1	horní
cesty	cesta	k1gFnPc1	cesta
dýchací	dýchací	k2eAgFnPc1d1	dýchací
<g/>
.	.	kIx.	.
</s>
<s>
Potvrzení	potvrzení	k1gNnSc1	potvrzení
diagnózy	diagnóza	k1gFnSc2	diagnóza
ale	ale	k8xC	ale
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
izolaci	izolace	k1gFnSc3	izolace
a	a	k8xC	a
identifikaci	identifikace	k1gFnSc3	identifikace
příčinného	příčinný	k2eAgNnSc2d1	příčinné
agens	agens	k1gNnSc2	agens
<g/>
.	.	kIx.	.
</s>
<s>
H.	H.	kA	H.
paragallinarum	paragallinarum	k1gNnSc1	paragallinarum
je	být	k5eAaImIp3nS	být
kataláza	kataláza	k1gFnSc1	kataláza
negativní	negativní	k2eAgFnSc2d1	negativní
bakterie	bakterie	k1gFnSc2	bakterie
<g/>
,	,	kIx,	,
vykazující	vykazující	k2eAgInSc1d1	vykazující
satelitní	satelitní	k2eAgInSc1d1	satelitní
růst	růst	k1gInSc1	růst
<g/>
.	.	kIx.	.
</s>
<s>
Diferenciální	diferenciální	k2eAgFnSc1d1	diferenciální
diagnostika	diagnostika	k1gFnSc1	diagnostika
<g/>
.	.	kIx.	.
</s>
<s>
Infekce	infekce	k1gFnSc1	infekce
H.	H.	kA	H.
paragallinarum	paragallinarum	k1gNnSc1	paragallinarum
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
odlišit	odlišit	k5eAaPmF	odlišit
od	od	k7c2	od
jiných	jiný	k2eAgNnPc2d1	jiné
respiračních	respirační	k2eAgNnPc2d1	respirační
onemocnění	onemocnění	k1gNnPc2	onemocnění
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
mykoplasmóza	mykoplasmóza	k1gFnSc1	mykoplasmóza
<g/>
,	,	kIx,	,
chronická	chronický	k2eAgFnSc1d1	chronická
choroba	choroba	k1gFnSc1	choroba
dýchadel	dýchadla	k1gNnPc2	dýchadla
<g/>
,	,	kIx,	,
syndrom	syndrom	k1gInSc1	syndrom
oteklé	oteklý	k2eAgFnSc2d1	oteklá
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
chronická	chronický	k2eAgFnSc1d1	chronická
forma	forma	k1gFnSc1	forma
cholery	cholera	k1gFnSc2	cholera
<g/>
,	,	kIx,	,
neštovice	neštovice	k1gFnSc1	neštovice
a	a	k8xC	a
hypovitaminóza	hypovitaminóza	k1gFnSc1	hypovitaminóza
A.	A.	kA	A.
Onemocnění	onemocnění	k1gNnSc1	onemocnění
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
komplikováno	komplikovat	k5eAaBmNgNnS	komplikovat
sekundární	sekundární	k2eAgFnSc7d1	sekundární
mikrobiální	mikrobiální	k2eAgFnSc7d1	mikrobiální
flórou	flóra	k1gFnSc7	flóra
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
vyšší	vysoký	k2eAgFnSc7d2	vyšší
mortalitou	mortalita	k1gFnSc7	mortalita
a	a	k8xC	a
déle	dlouho	k6eAd2	dlouho
trvajícím	trvající	k2eAgInSc7d1	trvající
průběhem	průběh	k1gInSc7	průběh
<g/>
.	.	kIx.	.
</s>
<s>
Průběh	průběh	k1gInSc4	průběh
hemofilové	hemofil	k1gMnPc1	hemofil
rýmy	rýma	k1gFnSc2	rýma
lze	lze	k6eAd1	lze
příznivě	příznivě	k6eAd1	příznivě
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
sulfonamidy	sulfonamid	k1gInPc4	sulfonamid
<g/>
,	,	kIx,	,
širokospektrálními	širokospektrální	k2eAgNnPc7d1	širokospektrální
antibiotiky	antibiotikum	k1gNnPc7	antibiotikum
a	a	k8xC	a
deriváty	derivát	k1gInPc7	derivát
nitrofuranu	nitrofuran	k1gInSc2	nitrofuran
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
ale	ale	k9	ale
nepůsobí	působit	k5eNaImIp3nS	působit
baktericidně	baktericidně	k6eAd1	baktericidně
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
bacilonosičství	bacilonosičství	k1gNnSc2	bacilonosičství
u	u	k7c2	u
rekonvalescentů	rekonvalescent	k1gMnPc2	rekonvalescent
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
<g/>
.	.	kIx.	.
</s>
<s>
Preventivn	Preventivn	k1gInSc4	Preventivn
opatření	opatření	k1gNnPc2	opatření
vycházejí	vycházet	k5eAaImIp3nP	vycházet
z	z	k7c2	z
poznatku	poznatek	k1gInSc2	poznatek
<g/>
,	,	kIx,	,
že	že	k8xS	že
hlavním	hlavní	k2eAgInSc7d1	hlavní
zdrojem	zdroj	k1gInSc7	zdroj
infekce	infekce	k1gFnSc2	infekce
jsou	být	k5eAaImIp3nP	být
rekonvalescenti	rekonvalescent	k1gMnPc1	rekonvalescent
nebo	nebo	k8xC	nebo
subklinicky	subklinicky	k6eAd1	subklinicky
infikovaní	infikovaný	k2eAgMnPc1d1	infikovaný
jedinci	jedinec	k1gMnPc1	jedinec
a	a	k8xC	a
že	že	k8xS	že
infekce	infekce	k1gFnSc1	infekce
se	se	k3xPyFc4	se
přenáší	přenášet	k5eAaImIp3nS	přenášet
horizontálně	horizontálně	k6eAd1	horizontálně
kontaktem	kontakt	k1gInSc7	kontakt
<g/>
.	.	kIx.	.
</s>
<s>
Ochrana	ochrana	k1gFnSc1	ochrana
chovů	chov	k1gInPc2	chov
proto	proto	k8xC	proto
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
zabránění	zabránění	k1gNnSc6	zabránění
zavlečení	zavlečení	k1gNnSc2	zavlečení
infekce	infekce	k1gFnSc2	infekce
do	do	k7c2	do
hejna	hejno	k1gNnSc2	hejno
<g/>
,	,	kIx,	,
omezení	omezení	k1gNnSc1	omezení
styku	styk	k1gInSc2	styk
různých	různý	k2eAgFnPc2d1	různá
věkových	věkový	k2eAgFnPc2d1	věková
skupin	skupina	k1gFnPc2	skupina
<g/>
,	,	kIx,	,
pečlivě	pečlivě	k6eAd1	pečlivě
prováděné	prováděný	k2eAgFnSc6d1	prováděná
asanaci	asanace	k1gFnSc6	asanace
hal	hala	k1gFnPc2	hala
před	před	k7c7	před
naskladněním	naskladnění	k1gNnSc7	naskladnění
a	a	k8xC	a
kvalitní	kvalitní	k2eAgFnSc6d1	kvalitní
výživě	výživa	k1gFnSc6	výživa
<g/>
.	.	kIx.	.
</s>
<s>
Eradikace	Eradikace	k1gFnSc1	Eradikace
infekce	infekce	k1gFnSc2	infekce
je	být	k5eAaImIp3nS	být
možná	možná	k9	možná
pouze	pouze	k6eAd1	pouze
depopulací	depopulace	k1gFnSc7	depopulace
hejna	hejno	k1gNnSc2	hejno
<g/>
.	.	kIx.	.
</s>
