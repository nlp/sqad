<s>
Hemofilová	Hemofilový	k2eAgFnSc1d1	Hemofilový
rýma	rýma	k1gFnSc1	rýma
drůbeže	drůbež	k1gFnSc2	drůbež
je	být	k5eAaImIp3nS	být
akutní	akutní	k2eAgNnSc1d1	akutní
nakažlivé	nakažlivý	k2eAgNnSc1d1	nakažlivé
bakteriální	bakteriální	k2eAgNnSc1d1	bakteriální
onemocnění	onemocnění	k1gNnSc1	onemocnění
horních	horní	k2eAgFnPc2d1	horní
cest	cesta	k1gFnPc2	cesta
dýchacích	dýchací	k2eAgFnPc2d1	dýchací
kura	kur	k1gMnSc2	kur
domácího	domácí	k2eAgMnSc2d1	domácí
<g/>
,	,	kIx,	,
způsobované	způsobovaný	k2eAgNnSc1d1	způsobované
bakterií	bakterie	k1gFnSc7	bakterie
Haemophilus	Haemophilus	k1gInSc4	Haemophilus
paragallinarum	paragallinarum	k1gNnSc4	paragallinarum
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
komplikujících	komplikující	k2eAgInPc2d1	komplikující
faktorů	faktor	k1gInPc2	faktor
často	často	k6eAd1	často
přechází	přecházet	k5eAaImIp3nS	přecházet
v	v	k7c6	v
onemocnění	onemocnění	k1gNnSc6	onemocnění
chronické	chronický	k2eAgNnSc1d1	chronické
<g/>
.	.	kIx.	.
</s>
