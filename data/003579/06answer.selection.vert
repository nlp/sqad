<s>
Jádro	jádro	k1gNnSc1	jádro
těchto	tento	k3xDgInPc2	tento
systémů	systém	k1gInPc2	systém
obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
částečnou	částečný	k2eAgFnSc4d1	částečná
podporu	podpora	k1gFnSc4	podpora
32	[number]	k4	32
<g/>
bitových	bitový	k2eAgInPc2d1	bitový
ovladačů	ovladač	k1gInPc2	ovladač
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
přistupovaly	přistupovat	k5eAaImAgFnP	přistupovat
k	k	k7c3	k
hardwaru	hardware	k1gInSc3	hardware
přímo	přímo	k6eAd1	přímo
bez	bez	k7c2	bez
využití	využití	k1gNnSc2	využití
služeb	služba	k1gFnPc2	služba
DOSu	DOSus	k1gInSc2	DOSus
a	a	k8xC	a
BIOSu	BIOSus	k1gInSc2	BIOSus
<g/>
.	.	kIx.	.
</s>
