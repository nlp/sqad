<s>
Gaius	Gaius	k1gMnSc1
Galerius	Galerius	k1gMnSc1
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
byl	být	k5eAaImAgMnS
dříve	dříve	k6eAd2
odpůrcem	odpůrce	k1gMnSc7
křesťanů	křesťan	k1gMnPc2
a	a	k8xC
spolupracoval	spolupracovat	k5eAaImAgMnS
na	na	k7c6
jejich	jejich	k3xOp3gNnSc6
pronásledování	pronásledování	k1gNnSc6
<g/>
,	,	kIx,
svým	svůj	k3xOyFgInSc7
ediktem	edikt	k1gInSc7
30	[number]	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
311	[number]	k4
objasnil	objasnit	k5eAaPmAgMnS
motivy	motiv	k1gInPc4
pronásledování	pronásledování	k1gNnSc2
křesťanů	křesťan	k1gMnPc2
<g/>
,	,	kIx,
přiznal	přiznat	k5eAaPmAgInS
neúspěšnost	neúspěšnost	k1gFnSc4
pronásledování	pronásledování	k1gNnSc2
a	a	k8xC
nařídil	nařídit	k5eAaPmAgMnS
veřejnoprávní	veřejnoprávní	k2eAgNnSc4d1
uznání	uznání	k1gNnSc4
křesťanského	křesťanský	k2eAgInSc2d1
kultu	kult	k1gInSc2
-	-	kIx~
přislíbil	přislíbit	k5eAaPmAgMnS
křesťanům	křesťan	k1gMnPc3
v	v	k7c6
rámci	rámec	k1gInSc6
platného	platný	k2eAgNnSc2d1
práva	právo	k1gNnSc2
veškerou	veškerý	k3xTgFnSc4
ochranu	ochrana	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1
jim	on	k3xPp3gMnPc3
jako	jako	k9
členům	člen	k1gMnPc3
povoleného	povolený	k2eAgInSc2d1
kultu	kult	k1gInSc2
(	(	kIx(
<g/>
religio	religio	k1gMnSc1
licita	licita	k1gMnSc1
<g/>
)	)	kIx)
příslušela	příslušet	k5eAaImAgFnS
<g/>
.	.	kIx.
</s>