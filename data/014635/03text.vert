<s>
Heinrich	Heinrich	k1gMnSc1
Schmid	Schmida	k1gFnPc2
</s>
<s>
Dipl	Dipl	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ing.	ing.	kA
Heinrich	Heinrich	k1gMnSc1
Schmid	Schmida	k1gFnPc2
Heinrich	Heinrich	k1gMnSc1
Schmid	Schmid	k1gInSc1
<g/>
,	,	kIx,
foto	foto	k1gNnSc1
z	z	k7c2
Wiener	Wienra	k1gFnPc2
Bilder	Bildra	k1gFnPc2
z	z	k7c2
r.	r.	kA
1907	#num#	k4
</s>
<s>
Poslanec	poslanec	k1gMnSc1
Říšské	říšský	k2eAgFnSc2d1
rady	rada	k1gFnSc2
</s>
<s>
Ve	v	k7c6
funkci	funkce	k1gFnSc6
<g/>
:	:	kIx,
<g/>
1907	#num#	k4
–	–	k?
1911	#num#	k4
</s>
<s>
Stranická	stranický	k2eAgFnSc1d1
příslušnost	příslušnost	k1gFnSc1
Členství	členství	k1gNnSc2
</s>
<s>
Křesťansko	Křesťansko	k1gNnSc1
sociální	sociální	k2eAgFnSc2d1
str	str	kA
<g/>
.	.	kIx.
</s>
<s>
Narození	narození	k1gNnSc1
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1855	#num#	k4
Waidhofen	Waidhofen	k1gInSc1
an	an	k?
der	drát	k5eAaImRp2nS
YbbsRakouské	YbbsRakouský	k2eAgNnSc4d1
císařství	císařství	k1gNnSc4
Rakouské	rakouský	k2eAgNnSc1d1
císařství	císařství	k1gNnSc1
Úmrtí	úmrtí	k1gNnSc1
</s>
<s>
24	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1928	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
72	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
VídeňRakousko	VídeňRakousko	k1gNnSc1
Rakousko	Rakousko	k1gNnSc1
Choť	choť	k1gFnSc1
</s>
<s>
Maira	Maira	k1gMnSc1
Dinold	Dinold	k1gMnSc1
Děti	dítě	k1gFnPc1
</s>
<s>
Heinrich	Heinrich	k1gMnSc1
Schmid	Schmida	k1gFnPc2
ml.	ml.	kA
Příbuzní	příbuzný	k1gMnPc1
</s>
<s>
Alfred	Alfred	k1gMnSc1
Schmid	Schmid	k1gInSc1
(	(	kIx(
<g/>
bratr	bratr	k1gMnSc1
<g/>
)	)	kIx)
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc2
</s>
<s>
VŠ	vš	k0
technická	technický	k2eAgFnSc1d1
Vídeň	Vídeň	k1gFnSc1
Ocenění	ocenění	k1gNnSc1
</s>
<s>
Řád	řád	k1gInSc1
železné	železný	k2eAgFnSc2d1
koruny	koruna	k1gFnSc2
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Heinrich	Heinrich	k1gMnSc1
Schmid	Schmida	k1gFnPc2
(	(	kIx(
<g/>
7	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1855	#num#	k4
Waidhofen	Waidhofen	k1gInSc1
an	an	k?
der	drát	k5eAaImRp2nS
Ybbs	Ybbs	k1gInSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
–	–	k?
24	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1928	#num#	k4
Vídeň	Vídeň	k1gFnSc1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
rakouský	rakouský	k2eAgMnSc1d1
pedagog	pedagog	k1gMnSc1
a	a	k8xC
politik	politik	k1gMnSc1
německé	německý	k2eAgFnSc2d1
národnosti	národnost	k1gFnSc2
z	z	k7c2
Dolních	dolní	k2eAgInPc2d1
Rakous	Rakousy	k1gInPc2
<g/>
,	,	kIx,
na	na	k7c6
počátku	počátek	k1gInSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnPc2
poslanec	poslanec	k1gMnSc1
Říšské	říšský	k2eAgFnSc2d1
rady	rada	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Biografie	biografie	k1gFnSc1
</s>
<s>
Působil	působit	k5eAaImAgMnS
jako	jako	k9
profesor	profesor	k1gMnSc1
na	na	k7c6
Státní	státní	k2eAgFnSc6d1
živnostenské	živnostenský	k2eAgFnSc6d1
škole	škola	k1gFnSc6
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Měl	mít	k5eAaImAgInS
titul	titul	k1gInSc1
dvorního	dvorní	k2eAgMnSc2d1
rady	rada	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Profesí	profes	k1gFnPc2
byl	být	k5eAaImAgMnS
technikem	technikum	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Okolo	okolo	k7c2
roku	rok	k1gInSc2
1875	#num#	k4
dokončil	dokončit	k5eAaPmAgMnS
studia	studio	k1gNnSc2
na	na	k7c6
Vysoké	vysoký	k2eAgFnSc6d1
škole	škola	k1gFnSc6
technické	technický	k2eAgFnSc6d1
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Od	od	k7c2
roku	rok	k1gInSc2
1876	#num#	k4
do	do	k7c2
roku	rok	k1gInSc2
1885	#num#	k4
působil	působit	k5eAaImAgMnS
coby	coby	k?
profesor	profesor	k1gMnSc1
na	na	k7c6
rakouské	rakouský	k2eAgFnSc6d1
stavební	stavební	k2eAgFnSc6d1
škole	škola	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Do	do	k7c2
roku	rok	k1gInSc2
1889	#num#	k4
byl	být	k5eAaImAgInS
suplentem	suplent	k1gMnSc7
na	na	k7c6
Státní	státní	k2eAgFnSc6d1
živnostenské	živnostenský	k2eAgFnSc6d1
škole	škola	k1gFnSc6
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
a	a	k8xC
pak	pak	k6eAd1
v	v	k7c6
letech	let	k1gInPc6
1889	#num#	k4
<g/>
–	–	k?
<g/>
1912	#num#	k4
zde	zde	k6eAd1
působil	působit	k5eAaImAgMnS
jako	jako	k9
profesor	profesor	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Angažoval	angažovat	k5eAaBmAgMnS
se	se	k3xPyFc4
i	i	k9
veřejně	veřejně	k6eAd1
a	a	k8xC
politicky	politicky	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1900	#num#	k4
<g/>
–	–	k?
<g/>
1923	#num#	k4
zasedal	zasedat	k5eAaImAgInS
ve	v	k7c6
vídeňské	vídeňský	k2eAgFnSc6d1
obecní	obecní	k2eAgFnSc6d1
radě	rada	k1gFnSc6
za	za	k7c4
IV	IV	kA
<g/>
.	.	kIx.
okres	okres	k1gInSc1
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
v	v	k7c6
letech	let	k1gInPc6
1912	#num#	k4
<g/>
–	–	k?
<g/>
1920	#num#	k4
byl	být	k5eAaImAgInS
i	i	k9
městským	městský	k2eAgMnPc3d1
radním	radní	k1gMnPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
obecní	obecní	k2eAgFnSc6d1
radě	rada	k1gFnSc6
se	se	k3xPyFc4
angažoval	angažovat	k5eAaBmAgMnS
hlavně	hlavně	k9
v	v	k7c6
referátu	referát	k1gInSc6
pro	pro	k7c4
elektrifikaci	elektrifikace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
rovněž	rovněž	k9
členem	člen	k1gMnSc7
komise	komise	k1gFnSc2
pro	pro	k7c4
regulaci	regulace	k1gFnSc4
Dunaje	Dunaj	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Působil	působit	k5eAaImAgMnS
i	i	k9
jako	jako	k9
poslanec	poslanec	k1gMnSc1
Říšské	říšský	k2eAgFnSc2d1
rady	rada	k1gFnSc2
(	(	kIx(
<g/>
celostátního	celostátní	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
Předlitavska	Předlitavsko	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kam	kam	k6eAd1
usedl	usednout	k5eAaPmAgMnS
ve	v	k7c6
volbách	volba	k1gFnPc6
do	do	k7c2
Říšské	říšský	k2eAgFnSc2d1
rady	rada	k1gFnSc2
roku	rok	k1gInSc2
1907	#num#	k4
<g/>
,	,	kIx,
konaných	konaný	k2eAgFnPc2d1
poprvé	poprvé	k6eAd1
podle	podle	k7c2
všeobecného	všeobecný	k2eAgNnSc2d1
a	a	k8xC
rovného	rovný	k2eAgNnSc2d1
volebního	volební	k2eAgNnSc2d1
práva	právo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
zvolen	zvolit	k5eAaPmNgMnS
za	za	k7c4
obvod	obvod	k1gInSc4
Dolní	dolní	k2eAgInPc1d1
Rakousy	Rakousy	k1gInPc1
0	#num#	k4
<g/>
9	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
volbách	volba	k1gFnPc6
roku	rok	k1gInSc2
1907	#num#	k4
byl	být	k5eAaImAgInS
uváděn	uvádět	k5eAaImNgInS
coby	coby	k?
člen	člen	k1gInSc1
klubu	klub	k1gInSc2
Křesťansko-sociální	křesťansko-sociální	k2eAgNnSc4d1
sjednocení	sjednocení	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
rejstříku	rejstřík	k1gInSc6
poslanců	poslanec	k1gMnPc2
je	být	k5eAaImIp3nS
veden	vést	k5eAaImNgInS
jako	jako	k8xS,k8xC
vládní	vládní	k2eAgFnSc1d1
rada	rada	k1gFnSc1
a	a	k8xC
profesor	profesor	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
letech	let	k1gInPc6
1904	#num#	k4
<g/>
–	–	k?
<g/>
1921	#num#	k4
byl	být	k5eAaImAgMnS
korespondentem	korespondent	k1gMnSc7
Ústřední	ústřední	k2eAgFnSc2d1
komise	komise	k1gFnSc2
pro	pro	k7c4
péči	péče	k1gFnSc4
o	o	k7c4
památky	památka	k1gFnPc4
pro	pro	k7c4
Dolní	dolní	k2eAgInPc4d1
Rakousy	Rakousy	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1912	#num#	k4
získal	získat	k5eAaPmAgInS
Řád	řád	k1gInSc1
železné	železný	k2eAgFnSc2d1
koruny	koruna	k1gFnSc2
III	III	kA
<g/>
.	.	kIx.
třídy	třída	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Patřil	patřit	k5eAaImAgInS
mezi	mezi	k7c7
odborníky	odborník	k1gMnPc7
na	na	k7c4
stavební	stavební	k2eAgInPc4d1
materiály	materiál	k1gInPc4
a	a	k8xC
publikoval	publikovat	k5eAaBmAgMnS
několik	několik	k4yIc4
knih	kniha	k1gFnPc2
a	a	k8xC
řadu	řada	k1gFnSc4
článků	článek	k1gInPc2
na	na	k7c4
toto	tento	k3xDgNnSc4
téma	téma	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc7
manželkou	manželka	k1gFnSc7
byla	být	k5eAaImAgFnS
Maira	Maira	k1gFnSc1
Dinold	Dinolda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měli	mít	k5eAaImAgMnP
dva	dva	k4xCgMnPc4
syny	syn	k1gMnPc4
<g/>
,	,	kIx,
přičem	přič	k1gInSc7
syn	syn	k1gMnSc1
Heinrich	Heinrich	k1gMnSc1
Schmid	Schmida	k1gFnPc2
(	(	kIx(
<g/>
1885	#num#	k4
<g/>
–	–	k?
<g/>
1949	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
architektem	architekt	k1gMnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS
v	v	k7c6
květnu	květen	k1gInSc6
1928	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7
bratrem	bratr	k1gMnSc7
byl	být	k5eAaImAgMnS
podnikatel	podnikatel	k1gMnSc1
a	a	k8xC
politik	politik	k1gMnSc1
Alfred	Alfred	k1gMnSc1
Schmid	Schmida	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
Heinrich	Heinrich	k1gMnSc1
Schmid	Schmida	k1gFnPc2
sen	sen	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
architektenlexikon	architektenlexikon	k1gInSc1
<g/>
.	.	kIx.
<g/>
at	at	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
KNAUER	KNAUER	kA
<g/>
,	,	kIx,
Oswald	Oswald	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Das	Das	k1gMnSc1
österreichische	österreichisch	k1gInSc2
Parlament	parlament	k1gInSc1
von	von	k1gInSc1
1848	#num#	k4
<g/>
–	–	k?
<g/>
1966	#num#	k4
<g/>
,	,	kIx,
Österreich-Reihe	Österreich-Reihe	k1gFnSc1
<g/>
,	,	kIx,
358	#num#	k4
<g/>
–	–	k?
<g/>
361	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Bergland	Bergland	k1gInSc1
Verlag	Verlaga	k1gFnPc2
<g/>
,	,	kIx,
1969	#num#	k4
<g/>
.	.	kIx.
316	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
160	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Hofrat	Hofrat	k1gInSc1
Prof.	prof.	kA
Schmid	Schmid	k1gInSc4
gestorben	gestorbna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reichspost	Reichspost	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Květen	květen	k1gInSc1
1928	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
145	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Databáze	databáze	k1gFnSc1
stenografických	stenografický	k2eAgInPc2d1
protokolů	protokol	k1gInPc2
a	a	k8xC
rejstříků	rejstřík	k1gInPc2
Říšské	říšský	k2eAgFnSc2d1
rady	rada	k1gFnSc2
z	z	k7c2
příslušných	příslušný	k2eAgNnPc2d1
volebních	volební	k2eAgNnPc2d1
období	období	k1gNnPc2
<g/>
,	,	kIx,
http://alex.onb.ac.at/spa.htm.	http://alex.onb.ac.at/spa.htm.	k?
<g/>
↑	↑	k?
http://alex.onb.ac.at/cgi-content/alex?aid=spa&	http://alex.onb.ac.at/cgi-content/alex?aid=spa&	k?
<g/>
↑	↑	k?
http://alex.onb.ac.at/cgi-content/alex?aid=spa&	http://alex.onb.ac.at/cgi-content/alex?aid=spa&	k?
<g/>
↑	↑	k?
Der	drát	k5eAaImRp2nS
ehemalige	ehemalig	k1gInSc2
Reichsratsabgeordnete	Reichsratsabgeordnete	k1gFnPc4
Schmid	Schmida	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deutsches	Deutsches	k1gMnSc1
Volksblatt	Volksblatt	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Srpen	srpen	k1gInSc1
1911	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
8136	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lidé	člověk	k1gMnPc1
|	|	kIx~
Politika	politika	k1gFnSc1
|	|	kIx~
Rakousko	Rakousko	k1gNnSc1
</s>
