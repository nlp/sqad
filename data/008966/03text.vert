<p>
<s>
Graciosa	Graciosa	k1gFnSc1	Graciosa
je	být	k5eAaImIp3nS	být
nejsevernější	severní	k2eAgInSc4d3	nejsevernější
ostrov	ostrov	k1gInSc4	ostrov
v	v	k7c6	v
centrální	centrální	k2eAgFnSc6d1	centrální
skupině	skupina	k1gFnSc6	skupina
Azorských	azorský	k2eAgInPc2d1	azorský
ostrovů	ostrov	k1gInPc2	ostrov
v	v	k7c6	v
Atlantském	atlantský	k2eAgInSc6d1	atlantský
oceánu	oceán	k1gInSc6	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Graciose	Graciosa	k1gFnSc3	Graciosa
se	se	k3xPyFc4	se
přezdívá	přezdívat	k5eAaImIp3nS	přezdívat
Bílý	bílý	k2eAgInSc1d1	bílý
ostrov	ostrov	k1gInSc1	ostrov
(	(	kIx(	(
<g/>
Ilha	Ilha	k1gFnSc1	Ilha
Branca	Branca	k?	Branca
<g/>
)	)	kIx)	)
podle	podle	k7c2	podle
pojmenování	pojmenování	k1gNnSc2	pojmenování
<g/>
,	,	kIx,	,
kterého	který	k3yIgNnSc2	který
se	se	k3xPyFc4	se
tomuto	tento	k3xDgInSc3	tento
ostrovu	ostrov	k1gInSc3	ostrov
dostalo	dostat	k5eAaPmAgNnS	dostat
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
portugalského	portugalský	k2eAgMnSc2d1	portugalský
spisovatele	spisovatel	k1gMnSc2	spisovatel
Raula	Raul	k1gMnSc2	Raul
Brandã	Brandã	k1gMnSc2	Brandã
"	"	kIx"	"
<g/>
As	as	k9	as
Ilhas	Ilhas	k1gMnSc1	Ilhas
Desconhecidas	Desconhecidas	k1gMnSc1	Desconhecidas
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Neznámé	známý	k2eNgInPc1d1	neznámý
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1926	[number]	k4	1926
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
administrativního	administrativní	k2eAgNnSc2d1	administrativní
hlediska	hledisko	k1gNnSc2	hledisko
je	být	k5eAaImIp3nS	být
Graciosa	Graciosa	k1gFnSc1	Graciosa
zámořským	zámořský	k2eAgMnSc7d1	zámořský
autonomním	autonomní	k2eAgMnSc7d1	autonomní
územím	území	k1gNnSc7	území
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
(	(	kIx(	(
<g/>
Regiã	Regiã	k1gFnSc1	Regiã
Autónoma	Autónoma	k1gFnSc1	Autónoma
dos	dos	k?	dos
Açores	Açores	k1gInSc1	Açores
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
ostrova	ostrov	k1gInSc2	ostrov
v	v	k7c6	v
první	první	k4xOgFnSc6	první
dekádě	dekáda	k1gFnSc6	dekáda
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
poklesl	poklesnout	k5eAaPmAgInS	poklesnout
ze	z	k7c2	z
4	[number]	k4	4
777	[number]	k4	777
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
na	na	k7c4	na
4	[number]	k4	4
391	[number]	k4	391
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Správním	správní	k2eAgNnSc7d1	správní
centrem	centrum	k1gNnSc7	centrum
ostrova	ostrov	k1gInSc2	ostrov
je	být	k5eAaImIp3nS	být
Santa	Santa	k1gMnSc1	Santa
Cruz	Cruz	k1gMnSc1	Cruz
da	da	k?	da
Graciosa	Graciosa	k1gFnSc1	Graciosa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
Graciosa	Graciosa	k1gFnSc1	Graciosa
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
zapsán	zapsat	k5eAaPmNgMnS	zapsat
na	na	k7c6	na
seznamu	seznam	k1gInSc6	seznam
biosférických	biosférický	k2eAgFnPc2d1	biosférická
rezervací	rezervace	k1gFnPc2	rezervace
UNESCO	UNESCO	kA	UNESCO
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
častým	častý	k2eAgInSc7d1	častý
cílem	cíl	k1gInSc7	cíl
vědeckých	vědecký	k2eAgFnPc2d1	vědecká
expedicí	expedice	k1gFnPc2	expedice
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
zde	zde	k6eAd1	zde
studují	studovat	k5eAaImIp3nP	studovat
zejména	zejména	k9	zejména
problematiku	problematika	k1gFnSc4	problematika
biodiverzity	biodiverzita	k1gFnSc2	biodiverzita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
Graciosa	Graciosa	k1gFnSc1	Graciosa
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
ostrovy	ostrov	k1gInPc7	ostrov
centrální	centrální	k2eAgFnSc2d1	centrální
části	část	k1gFnSc2	část
Azorského	azorský	k2eAgNnSc2d1	Azorské
souostroví	souostroví	k1gNnSc2	souostroví
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
západním	západní	k2eAgInSc6d1	západní
okraji	okraj	k1gInSc6	okraj
Euroasijské	euroasijský	k2eAgFnSc2d1	euroasijská
tektonické	tektonický	k2eAgFnSc2d1	tektonická
desky	deska	k1gFnSc2	deska
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
součástí	součást	k1gFnSc7	součást
Středoatlantského	Středoatlantský	k2eAgInSc2d1	Středoatlantský
hřbetu	hřbet	k1gInSc2	hřbet
<g/>
.	.	kIx.	.
</s>
<s>
Nejbližší	blízký	k2eAgFnSc7d3	nejbližší
pevninou	pevnina	k1gFnSc7	pevnina
je	být	k5eAaImIp3nS	být
další	další	k2eAgInSc1d1	další
azorský	azorský	k2eAgInSc1d1	azorský
ostrov	ostrov	k1gInSc1	ostrov
Săo	Săo	k1gMnSc2	Săo
Jorge	Jorg	k1gMnSc2	Jorg
<g/>
,	,	kIx,	,
vzdálený	vzdálený	k2eAgInSc1d1	vzdálený
36,5	[number]	k4	36,5
km	km	kA	km
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
jih	jih	k1gInSc4	jih
<g/>
.	.	kIx.	.
</s>
<s>
Vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
Graciosy	Graciosa	k1gFnSc2	Graciosa
od	od	k7c2	od
Ponta	Pont	k1gInSc2	Pont
da	da	k?	da
Serreta	Serret	k1gMnSc2	Serret
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Terceira	Terceir	k1gInSc2	Terceir
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
leží	ležet	k5eAaImIp3nS	ležet
jihovýchodním	jihovýchodní	k2eAgInSc7d1	jihovýchodní
směrem	směr	k1gInSc7	směr
<g/>
,	,	kIx,	,
činí	činit	k5eAaImIp3nS	činit
56,8	[number]	k4	56,8
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Geologie	geologie	k1gFnSc2	geologie
===	===	k?	===
</s>
</p>
<p>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
Graciosa	Graciosa	k1gFnSc1	Graciosa
je	být	k5eAaImIp3nS	být
sopečného	sopečný	k2eAgInSc2d1	sopečný
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
stratovulkán	stratovulkán	k1gInSc4	stratovulkán
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
poslední	poslední	k2eAgFnSc1d1	poslední
aktivita	aktivita	k1gFnSc1	aktivita
je	být	k5eAaImIp3nS	být
datována	datovat	k5eAaImNgFnS	datovat
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Hlavními	hlavní	k2eAgFnPc7d1	hlavní
horninami	hornina	k1gFnPc7	hornina
jsou	být	k5eAaImIp3nP	být
čedič	čedič	k1gInSc4	čedič
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
trachyt	trachyt	k1gInSc1	trachyt
a	a	k8xC	a
tefrit	tefrit	k1gInSc1	tefrit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
části	část	k1gFnSc6	část
ostrova	ostrov	k1gInSc2	ostrov
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
sopečná	sopečný	k2eAgFnSc1d1	sopečná
kaldera	kaldera	k1gFnSc1	kaldera
oválného	oválný	k2eAgInSc2d1	oválný
tvaru	tvar	k1gInSc2	tvar
o	o	k7c6	o
rozměrech	rozměr	k1gInPc6	rozměr
zhruba	zhruba	k6eAd1	zhruba
2	[number]	k4	2
x	x	k?	x
1,5	[number]	k4	1,5
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
několik	několik	k4yIc1	několik
jeskyní	jeskyně	k1gFnPc2	jeskyně
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k6eAd1	jak
při	při	k7c6	při
okraji	okraj	k1gInSc6	okraj
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
uprostřed	uprostřed	k7c2	uprostřed
kaldery	kaldera	k1gFnSc2	kaldera
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Furna	Furna	k1gFnSc1	Furna
do	do	k7c2	do
Maria	Mario	k1gMnSc2	Mario
Encantada	Encantada	k1gFnSc1	Encantada
<g/>
,	,	kIx,	,
Furna	Furen	k2eAgFnSc1d1	Furen
do	do	k7c2	do
Enxofre	Enxofr	k1gInSc5	Enxofr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Klima	klima	k1gNnSc1	klima
===	===	k?	===
</s>
</p>
<p>
<s>
Graciosa	Graciosa	k1gFnSc1	Graciosa
má	mít	k5eAaImIp3nS	mít
nejnižší	nízký	k2eAgFnSc4d3	nejnižší
průměrnou	průměrný	k2eAgFnSc4d1	průměrná
nadmořskou	nadmořský	k2eAgFnSc4d1	nadmořská
výšku	výška	k1gFnSc4	výška
v	v	k7c6	v
celém	celý	k2eAgNnSc6d1	celé
Azorském	azorský	k2eAgNnSc6d1	Azorské
souostroví	souostroví	k1gNnSc6	souostroví
-	-	kIx~	-
většina	většina	k1gFnSc1	většina
jejího	její	k3xOp3gNnSc2	její
území	území	k1gNnSc2	území
nedosahuje	dosahovat	k5eNaImIp3nS	dosahovat
300	[number]	k4	300
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
skutečnost	skutečnost	k1gFnSc1	skutečnost
je	být	k5eAaImIp3nS	být
příčinou	příčina	k1gFnSc7	příčina
nízkého	nízký	k2eAgNnSc2d1	nízké
množství	množství	k1gNnSc2	množství
srážek	srážka	k1gFnPc2	srážka
<g/>
.	.	kIx.	.
</s>
<s>
Graciosa	Graciosa	k1gFnSc1	Graciosa
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
nejsušším	suchý	k2eAgInSc7d3	nejsušší
ostrovem	ostrov	k1gInSc7	ostrov
Azor	Azory	k1gFnPc2	Azory
<g/>
,	,	kIx,	,
což	což	k3yRnSc4	což
v	v	k7c6	v
obdobích	období	k1gNnPc6	období
extrémního	extrémní	k2eAgNnSc2d1	extrémní
sucha	sucho	k1gNnSc2	sucho
vedlo	vést	k5eAaImAgNnS	vést
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
až	až	k9	až
k	k	k7c3	k
ohrožení	ohrožení	k1gNnSc3	ohrožení
života	život	k1gInSc2	život
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Nejstarší	starý	k2eAgInPc1d3	nejstarší
záznamy	záznam	k1gInPc1	záznam
o	o	k7c6	o
průzkumu	průzkum	k1gInSc6	průzkum
ostrova	ostrov	k1gInSc2	ostrov
portugalskými	portugalský	k2eAgMnPc7d1	portugalský
mořeplavci	mořeplavec	k1gMnPc7	mořeplavec
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc3	jeho
následné	následný	k2eAgFnSc3d1	následná
kolonizaci	kolonizace	k1gFnSc3	kolonizace
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
první	první	k4xOgFnSc2	první
poloviny	polovina	k1gFnSc2	polovina
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Stálé	stálý	k2eAgNnSc1d1	stálé
osídlení	osídlení	k1gNnSc1	osídlení
se	se	k3xPyFc4	se
datuje	datovat	k5eAaImIp3nS	datovat
od	od	k7c2	od
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarším	starý	k2eAgNnSc7d3	nejstarší
sídlem	sídlo	k1gNnSc7	sídlo
byla	být	k5eAaImAgFnS	být
obec	obec	k1gFnSc1	obec
Carapacho	Carapacha	k1gFnSc5	Carapacha
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
ostrova	ostrov	k1gInSc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
byly	být	k5eAaImAgFnP	být
vybudovány	vybudován	k2eAgFnPc1d1	vybudována
další	další	k2eAgFnPc1d1	další
obce	obec	k1gFnPc1	obec
-	-	kIx~	-
Luz	luza	k1gFnPc2	luza
<g/>
,	,	kIx,	,
Praia	Praia	k1gFnSc1	Praia
<g/>
,	,	kIx,	,
Guadalupe	Guadalup	k1gInSc5	Guadalup
<g/>
,	,	kIx,	,
Santa	Santa	k1gMnSc1	Santa
Cruz	Cruz	k1gMnSc1	Cruz
da	da	k?	da
Geaciosa	Geaciosa	k1gFnSc1	Geaciosa
a	a	k8xC	a
Vitoria	Vitorium	k1gNnSc2	Vitorium
<g/>
.	.	kIx.	.
</s>
<s>
Základem	základ	k1gInSc7	základ
místní	místní	k2eAgFnSc2d1	místní
ekonomiky	ekonomika	k1gFnSc2	ekonomika
bylo	být	k5eAaImAgNnS	být
zemědělství	zemědělství	k1gNnSc1	zemědělství
a	a	k8xC	a
rybolov	rybolov	k1gInSc1	rybolov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
historii	historie	k1gFnSc6	historie
ostrova	ostrov	k1gInSc2	ostrov
byly	být	k5eAaImAgFnP	být
zaznamenány	zaznamenat	k5eAaPmNgFnP	zaznamenat
některé	některý	k3yIgFnPc1	některý
významné	významný	k2eAgFnPc1d1	významná
krize	krize	k1gFnPc1	krize
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
vpádů	vpád	k1gInPc2	vpád
pirátů	pirát	k1gMnPc2	pirát
byly	být	k5eAaImAgInP	být
způsobeny	způsobit	k5eAaPmNgInP	způsobit
jednak	jednak	k8xC	jednak
suchem	sucho	k1gNnSc7	sucho
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
k	k	k7c3	k
nejhorší	zlý	k2eAgFnSc3d3	nejhorší
situaci	situace	k1gFnSc3	situace
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1844	[number]	k4	1844
<g/>
,	,	kIx,	,
a	a	k8xC	a
jednak	jednak	k8xC	jednak
četnými	četný	k2eAgNnPc7d1	četné
zemětřeseními	zemětřesení	k1gNnPc7	zemětřesení
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
velkém	velký	k2eAgNnSc6d1	velké
zemětřesení	zemětřesení	k1gNnSc6	zemětřesení
byla	být	k5eAaImAgFnS	být
13	[number]	k4	13
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1730	[number]	k4	1730
poničena	poničen	k2eAgFnSc1d1	poničena
obec	obec	k1gFnSc1	obec
Luz	luza	k1gFnPc2	luza
na	na	k7c6	na
západním	západní	k2eAgNnSc6d1	západní
úpatí	úpatí	k1gNnSc6	úpatí
kaldery	kaldera	k1gFnSc2	kaldera
<g/>
,	,	kIx,	,
o	o	k7c4	o
sto	sto	k4xCgNnSc4	sto
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1837	[number]	k4	1837
<g/>
,	,	kIx,	,
zemětřesení	zemětřesení	k1gNnSc1	zemětřesení
pobořilo	pobořit	k5eAaPmAgNnS	pobořit
přístav	přístav	k1gInSc4	přístav
Praia	Praium	k1gNnSc2	Praium
na	na	k7c6	na
východním	východní	k2eAgNnSc6d1	východní
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgNnPc1d1	poslední
zemětřesení	zemětřesení	k1gNnPc1	zemětřesení
ve	v	k7c4	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
zaznamenáno	zaznamenat	k5eAaPmNgNnS	zaznamenat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
nedaleko	nedaleko	k7c2	nedaleko
obce	obec	k1gFnSc2	obec
Carapacha	Carapach	k1gMnSc2	Carapach
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Graciosa	Graciosa	k1gFnSc1	Graciosa
zřízena	zřídit	k5eAaPmNgFnS	zřídit
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
se	s	k7c7	s
smlouvou	smlouva	k1gFnSc7	smlouva
OSN	OSN	kA	OSN
o	o	k7c6	o
zákazu	zákaz	k1gInSc6	zákaz
jaderných	jaderný	k2eAgFnPc2d1	jaderná
zkoušek	zkouška	k1gFnPc2	zkouška
monitorovací	monitorovací	k2eAgFnSc2d1	monitorovací
stanice	stanice	k1gFnSc2	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
stanice	stanice	k1gFnSc1	stanice
<g/>
,	,	kIx,	,
jediná	jediný	k2eAgFnSc1d1	jediná
svého	svůj	k3xOyFgInSc2	svůj
druhu	druh	k1gInSc2	druh
na	na	k7c6	na
území	území	k1gNnSc6	území
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
globální	globální	k2eAgFnSc2d1	globální
sítě	síť	k1gFnSc2	síť
monitorovacích	monitorovací	k2eAgFnPc2d1	monitorovací
stanic	stanice	k1gFnPc2	stanice
jaderných	jaderný	k2eAgInPc2d1	jaderný
testů	test	k1gInPc2	test
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Graciosa	Graciosa	k1gFnSc1	Graciosa
na	na	k7c6	na
portugalské	portugalský	k2eAgFnSc6d1	portugalská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Graciosa	Graciosa	k1gFnSc1	Graciosa
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
