<s>
Graciose	Graciosa	k1gFnSc3	Graciosa
se	se	k3xPyFc4	se
přezdívá	přezdívat	k5eAaImIp3nS	přezdívat
Bílý	bílý	k2eAgInSc1d1	bílý
ostrov	ostrov	k1gInSc1	ostrov
(	(	kIx(	(
<g/>
Ilha	Ilha	k1gFnSc1	Ilha
Branca	Branca	k?	Branca
<g/>
)	)	kIx)	)
podle	podle	k7c2	podle
pojmenování	pojmenování	k1gNnSc2	pojmenování
<g/>
,	,	kIx,	,
kterého	který	k3yRgNnSc2	který
se	se	k3xPyFc4	se
tomuto	tento	k3xDgInSc3	tento
ostrovu	ostrov	k1gInSc3	ostrov
dostalo	dostat	k5eAaPmAgNnS	dostat
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
portugalského	portugalský	k2eAgMnSc2d1	portugalský
spisovatele	spisovatel	k1gMnSc2	spisovatel
Raula	Raul	k1gMnSc2	Raul
Brandã	Brandã	k1gMnSc2	Brandã
"	"	kIx"	"
<g/>
As	as	k9	as
Ilhas	Ilhas	k1gMnSc1	Ilhas
Desconhecidas	Desconhecidas	k1gMnSc1	Desconhecidas
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Neznámé	známý	k2eNgInPc1d1	neznámý
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1926	[number]	k4	1926
<g/>
.	.	kIx.	.
</s>
