<s>
Nejstarším	starý	k2eAgInSc7d3	nejstarší
paleontologickým	paleontologický	k2eAgInSc7d1	paleontologický
nálezem	nález	k1gInSc7	nález
<g/>
,	,	kIx,	,
zařazovaným	zařazovaný	k2eAgInSc7d1	zařazovaný
do	do	k7c2	do
rodu	rod	k1gInSc2	rod
Homo	Homo	k6eAd1	Homo
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
africký	africký	k2eAgMnSc1d1	africký
Homo	Homo	k1gMnSc1	Homo
habilis	habilis	k1gFnSc2	habilis
<g/>
,	,	kIx,	,
starý	starý	k2eAgInSc4d1	starý
až	až	k6eAd1	až
2,5	[number]	k4	2,5
milionu	milion	k4xCgInSc2	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
