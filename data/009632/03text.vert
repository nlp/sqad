<p>
<s>
Těšnovský	Těšnovský	k2eAgInSc1d1	Těšnovský
tunel	tunel	k1gInSc1	tunel
je	být	k5eAaImIp3nS	být
mělký	mělký	k2eAgInSc1d1	mělký
hloubený	hloubený	k2eAgInSc1d1	hloubený
silniční	silniční	k2eAgInSc1d1	silniční
tunel	tunel	k1gInSc1	tunel
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
spojující	spojující	k2eAgNnSc4d1	spojující
nábřeží	nábřeží	k1gNnSc4	nábřeží
Ludvíka	Ludvík	k1gMnSc2	Ludvík
Svobody	Svoboda	k1gMnSc2	Svoboda
na	na	k7c6	na
Novém	nový	k2eAgNnSc6d1	nové
Městě	město	k1gNnSc6	město
(	(	kIx(	(
<g/>
tato	tento	k3xDgFnSc1	tento
část	část	k1gFnSc1	část
čtvrti	čtvrt	k1gFnSc2	čtvrt
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
1	[number]	k4	1
<g/>
)	)	kIx)	)
a	a	k8xC	a
Rohanské	Rohanský	k2eAgNnSc4d1	Rohanské
nábřeží	nábřeží	k1gNnSc4	nábřeží
v	v	k7c6	v
Karlíně	Karlín	k1gInSc6	Karlín
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
8	[number]	k4	8
(	(	kIx(	(
<g/>
kam	kam	k6eAd1	kam
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
východní	východní	k2eAgNnSc1d1	východní
vyústění	vyústění	k1gNnSc1	vyústění
tunelu	tunel	k1gInSc2	tunel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tunel	tunel	k1gInSc1	tunel
tedy	tedy	k9	tedy
převádí	převádět	k5eAaImIp3nS	převádět
dopravu	doprava	k1gFnSc4	doprava
pod	pod	k7c4	pod
zem	zem	k1gFnSc4	zem
v	v	k7c6	v
úseku	úsek	k1gInSc6	úsek
nábřeží	nábřeží	k1gNnSc2	nábřeží
Ludvíka	Ludvík	k1gMnSc2	Ludvík
Svobody	Svoboda	k1gMnSc2	Svoboda
u	u	k7c2	u
budov	budova	k1gFnPc2	budova
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
dopravy	doprava	k1gFnSc2	doprava
a	a	k8xC	a
Českých	český	k2eAgFnPc2d1	Česká
drah	draha	k1gFnPc2	draha
(	(	kIx(	(
<g/>
čp.	čp.	k?	čp.
1222	[number]	k4	1222
<g/>
)	)	kIx)	)
a	a	k8xC	a
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
zemědělství	zemědělství	k1gNnSc2	zemědělství
(	(	kIx(	(
<g/>
čp.	čp.	k?	čp.
65	[number]	k4	65
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
poloze	poloha	k1gFnSc3	poloha
před	před	k7c7	před
bývalým	bývalý	k2eAgNnSc7d1	bývalé
ústředím	ústředí	k1gNnSc7	ústředí
Komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
Československa	Československo	k1gNnSc2	Československo
a	a	k8xC	a
době	doba	k1gFnSc6	doba
stavby	stavba	k1gFnSc2	stavba
bývá	bývat	k5eAaImIp3nS	bývat
přezdíván	přezdívat	k5eAaImNgInS	přezdívat
jako	jako	k8xC	jako
Husákovo	Husákův	k2eAgNnSc1d1	Husákovo
ticho	ticho	k1gNnSc1	ticho
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
dlouhý	dlouhý	k2eAgMnSc1d1	dlouhý
360	[number]	k4	360
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
dva	dva	k4xCgInPc4	dva
dvoupruhové	dvoupruhový	k2eAgInPc4d1	dvoupruhový
tubusy	tubus	k1gInPc4	tubus
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
otevřen	otevřen	k2eAgInSc1d1	otevřen
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tunel	tunel	k1gInSc1	tunel
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
mimoúrovňového	mimoúrovňový	k2eAgNnSc2d1	mimoúrovňové
křížení	křížení	k1gNnSc2	křížení
pobřežního	pobřežní	k2eAgInSc2d1	pobřežní
tahu	tah	k1gInSc2	tah
se	s	k7c7	s
Severojižní	severojižní	k2eAgFnSc7d1	severojižní
magistrálou	magistrála	k1gFnSc7	magistrála
na	na	k7c6	na
jižním	jižní	k2eAgNnSc6d1	jižní
předmostí	předmostí	k1gNnSc6	předmostí
Hlávkova	Hlávkův	k2eAgInSc2d1	Hlávkův
mostu	most	k1gInSc2	most
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
podchází	podcházet	k5eAaImIp3nP	podcházet
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
západního	západní	k2eAgInSc2d1	západní
konce	konec	k1gInSc2	konec
tunelu	tunel	k1gInSc2	tunel
je	být	k5eAaImIp3nS	být
úrovňová	úrovňový	k2eAgFnSc1d1	úrovňová
světelně	světelně	k6eAd1	světelně
řízená	řízený	k2eAgFnSc1d1	řízená
křižovatka	křižovatka	k1gFnSc1	křižovatka
s	s	k7c7	s
ulicí	ulice	k1gFnSc7	ulice
Holbovou	holbový	k2eAgFnSc7d1	Holbová
<g/>
,	,	kIx,	,
u	u	k7c2	u
východního	východní	k2eAgInSc2d1	východní
konce	konec	k1gInSc2	konec
tunelu	tunel	k1gInSc2	tunel
úrovňová	úrovňový	k2eAgFnSc1d1	úrovňová
světelně	světelně	k6eAd1	světelně
řízená	řízený	k2eAgFnSc1d1	řízená
křižovatka	křižovatka	k1gFnSc1	křižovatka
s	s	k7c7	s
ulicí	ulice	k1gFnSc7	ulice
Ke	k	k7c3	k
Štvanici	Štvanice	k1gFnSc3	Štvanice
<g/>
,	,	kIx,	,
každá	každý	k3xTgFnSc1	každý
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
křižovatek	křižovatka	k1gFnPc2	křižovatka
napojuje	napojovat	k5eAaImIp3nS	napojovat
pobřežní	pobřežní	k2eAgFnSc4d1	pobřežní
komunikaci	komunikace	k1gFnSc4	komunikace
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
směr	směr	k1gInSc4	směr
magistrály	magistrála	k1gFnSc2	magistrála
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
povodních	povodeň	k1gFnPc6	povodeň
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
byl	být	k5eAaImAgInS	být
tunel	tunel	k1gInSc1	tunel
kompletně	kompletně	k6eAd1	kompletně
zatopen	zatopen	k2eAgInSc1d1	zatopen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Těšnovský	Těšnovský	k2eAgInSc4d1	Těšnovský
tunel	tunel	k1gInSc4	tunel
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
