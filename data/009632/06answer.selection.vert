<s>
Těšnovský	Těšnovský	k2eAgInSc1d1	Těšnovský
tunel	tunel	k1gInSc1	tunel
je	být	k5eAaImIp3nS	být
mělký	mělký	k2eAgInSc1d1	mělký
hloubený	hloubený	k2eAgInSc1d1	hloubený
silniční	silniční	k2eAgInSc1d1	silniční
tunel	tunel	k1gInSc1	tunel
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
spojující	spojující	k2eAgNnSc4d1	spojující
nábřeží	nábřeží	k1gNnSc4	nábřeží
Ludvíka	Ludvík	k1gMnSc2	Ludvík
Svobody	Svoboda	k1gMnSc2	Svoboda
na	na	k7c6	na
Novém	nový	k2eAgNnSc6d1	nové
Městě	město	k1gNnSc6	město
(	(	kIx(	(
<g/>
tato	tento	k3xDgFnSc1	tento
část	část	k1gFnSc1	část
čtvrti	čtvrt	k1gFnSc2	čtvrt
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
1	[number]	k4	1
<g/>
)	)	kIx)	)
a	a	k8xC	a
Rohanské	Rohanský	k2eAgNnSc4d1	Rohanské
nábřeží	nábřeží	k1gNnSc4	nábřeží
v	v	k7c6	v
Karlíně	Karlín	k1gInSc6	Karlín
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
8	[number]	k4	8
(	(	kIx(	(
<g/>
kam	kam	k6eAd1	kam
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
východní	východní	k2eAgNnSc1d1	východní
vyústění	vyústění	k1gNnSc1	vyústění
tunelu	tunel	k1gInSc2	tunel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
