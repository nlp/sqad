<s>
Emma	Emma	k1gFnSc1
Watsonová	Watsonová	k1gFnSc1*xF
</s>
<s>
Emma	Emma	k1gFnSc1
Watsonová	Watsonová	k1gFnSc1
Ema	Ema	k1gFnSc1
Watsonová	Watsonová	k1gFnSc1
v	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
<g/>
Rodné	rodný	k2eAgInPc4d1
jméno	jméno	k1gNnSc4
</s>
<s>
Emma	Emma	k1gFnSc1
Charlotte	Charlott	k1gInSc5
Duerre	Duerr	k1gInSc5
Watson	Watson	k1gInSc4
Narození	narozený	k2eAgMnPc5d1
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1990	#num#	k4
(	(	kIx(
<g/>
31	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Paříž	Paříž	k1gFnSc1
<g/>
,	,	kIx,
Francie	Francie	k1gFnSc1
Francie	Francie	k1gFnSc1
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
</s>
<s>
Dragon	Dragon	k1gMnSc1
School	Schoola	k1gFnPc2
(	(	kIx(
<g/>
do	do	k7c2
2003	#num#	k4
<g/>
)	)	kIx)
<g/>
Brownova	Brownův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
–	–	k?
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
<g/>
Worcester	Worcester	k1gInSc1
College	College	k1gInSc1
<g/>
,	,	kIx,
Oxford	Oxford	k1gInSc1
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
–	–	k?
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
<g/>
Headington	Headington	k1gInSc1
School	School	k1gInSc1
Aktivní	aktivní	k2eAgInPc1d1
roky	rok	k1gInPc1
</s>
<s>
2000	#num#	k4
<g/>
–	–	k?
<g/>
dosud	dosud	k6eAd1
Rodiče	rodič	k1gMnPc1
</s>
<s>
Chris	Chris	k1gFnSc1
WatsonJacquline	WatsonJacqulin	k1gInSc5
Watsonová	Watsonová	k1gFnSc1
(	(	kIx(
<g/>
rozvedeni	rozveden	k2eAgMnPc1d1
<g/>
)	)	kIx)
Významné	významný	k2eAgFnPc4d1
role	role	k1gFnPc4
</s>
<s>
Hermiona	Hermiona	k1gFnSc1
Grangerová	Grangerová	k1gFnSc1
/	/	kIx~
Harry	Harra	k1gFnSc2
Potter	Potter	k1gInSc1
Český	český	k2eAgInSc1d1
dabing	dabing	k1gInSc4
</s>
<s>
Anežka	Anežka	k1gFnSc1
Pohorská	pohorský	k2eAgFnSc1d1
Cena	cena	k1gFnSc1
BAFTA	BAFTA	kA
</s>
<s>
Britannia	Britannium	k1gNnSc2
Award	Award	k1gInSc4
pro	pro	k7c4
nejlepší	dobrý	k2eAgFnSc4d3
britskou	britský	k2eAgFnSc4d1
herečku	herečka	k1gFnSc4
2014	#num#	k4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Emma	Emma	k1gFnSc1
Watsonová	Watsonová	k1gFnSc1
(	(	kIx(
<g/>
celým	celý	k2eAgNnSc7d1
jménem	jméno	k1gNnSc7
Emma	Emma	k1gFnSc1
Charlotte	Charlotte	k1gInSc1
Duerre	Duerre	k1gInSc1
Watson	Watson	k1gNnSc1*xF
<g/>
,	,	kIx,
*	*	kIx~
15	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1990	#num#	k4
Paříž	Paříž	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
britská	britský	k2eAgFnSc1d1
herečka	herečka	k1gFnSc1
a	a	k8xC
modelka	modelka	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1
ztvárnila	ztvárnit	k5eAaPmAgFnS
roli	role	k1gFnSc4
Hermiony	Hermiona	k1gFnSc4
Grangerové	Grangerová	k1gFnSc4*xF
ve	v	k7c6
filmové	filmový	k2eAgFnSc6d1
sérii	série	k1gFnSc6
Harry	Harry	k1gAnSc1
Potter	Potter	k1gAnSc1*xF
o	o	k7c6
bradavickém	bradavický	k2eAgMnSc6d1
kouzelníkovi	kouzelník	k1gMnSc6
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Narodila	narodit	k5eAaPmAgFnS
se	se	k3xPyFc4
v	v	k7c6
Paříži	Paříž	k1gFnSc6
anglickým	anglický	k2eAgMnPc3d1
právníkům	právník	k1gMnPc3
Chrisovi	Chrisův	k2eAgMnPc1d1
a	a	k8xC
Jacqueline	Jacquelin	k1gInSc5
Watsonovým	Watsonovi	k1gRnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc1
babička	babička	k1gFnSc1
je	být	k5eAaImIp3nS
Francouzka	Francouzka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
rozvodu	rozvod	k1gInSc6
rodičů	rodič	k1gMnPc2
od	od	k7c2
pěti	pět	k4xCc2
let	léto	k1gNnPc2
žije	žít	k5eAaImIp3nS
se	s	k7c7
svou	svůj	k3xOyFgFnSc7
matkou	matka	k1gFnSc7
a	a	k8xC
bratrem	bratr	k1gMnSc7
v	v	k7c6
Oxfordu	Oxford	k1gInSc6
v	v	k7c6
Anglii	Anglie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
útlého	útlý	k2eAgNnSc2d1
dětství	dětství	k1gNnSc2
se	se	k3xPyFc4
projevoval	projevovat	k5eAaImAgInS
její	její	k3xOp3gInSc4
zájem	zájem	k1gInSc4
o	o	k7c4
herectví	herectví	k1gNnSc4
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
vystupovala	vystupovat	k5eAaImAgFnS
v	v	k7c6
řadě	řada	k1gFnSc6
divadelních	divadelní	k2eAgNnPc2d1
představení	představení	k1gNnPc2
<g/>
,	,	kIx,
ve	v	k7c6
školních	školní	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
<g/>
,	,	kIx,
v	v	k7c6
sedmi	sedm	k4xCc6
letech	let	k1gInPc6
vyhrála	vyhrát	k5eAaPmAgFnS
recitační	recitační	k2eAgFnSc4d1
soutěž	soutěž	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc7
první	první	k4xOgFnSc7
filmovou	filmový	k2eAgFnSc7d1
příležitostí	příležitost	k1gFnSc7
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
role	role	k1gFnSc1
Hermiony	Hermion	k1gInPc4
Grangerové	Grangerová	k1gFnSc2
ve	v	k7c6
filmu	film	k1gInSc6
Harry	Harra	k1gFnSc2
Potter	Potter	k1gInSc1
a	a	k8xC
Kámen	kámen	k1gInSc1
mudrců	mudrc	k1gMnPc2
ve	v	k7c6
věku	věk	k1gInSc6
jedenácti	jedenáct	k4xCc2
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Poprvé	poprvé	k6eAd1
se	se	k3xPyFc4
zamilovala	zamilovat	k5eAaPmAgFnS
do	do	k7c2
Toma	Tom	k1gMnSc2
Feltona	Felton	k1gMnSc2
při	při	k7c6
natáčení	natáčení	k1gNnSc6
prvních	první	k4xOgFnPc2
dvou	dva	k4xCgFnPc2
dílů	díl	k1gInPc2
o	o	k7c4
Harrym	Harrym	k1gInSc4
Potterovi	Potterův	k2eAgMnPc1d1
<g/>
,	,	kIx,
představitele	představitel	k1gMnSc4
Draca	Dracus	k1gMnSc4
Malfoye	Malfoy	k1gMnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Ve	v	k7c6
dvaceti	dvacet	k4xCc6
letech	léto	k1gNnPc6
do	do	k7c2
o	o	k7c4
šest	šest	k4xCc4
let	léto	k1gNnPc2
staršího	starý	k2eAgMnSc2d2
britského	britský	k2eAgMnSc2d1
herce	herec	k1gMnSc2
Jay	Jay	k1gMnSc2
Barrymore	Barrymor	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
začátku	začátek	k1gInSc6
ledna	leden	k1gInSc2
2014	#num#	k4
se	se	k3xPyFc4
po	po	k7c6
dvou	dva	k4xCgInPc6
letech	let	k1gInPc6
rozešla	rozejít	k5eAaPmAgFnS
s	s	k7c7
bohatým	bohatý	k2eAgMnSc7d1
dědicem	dědic	k1gMnSc7
Willem	Will	k1gMnSc7
Adamowitzem	Adamowitz	k1gMnSc7
<g/>
,	,	kIx,
kterého	který	k3yRgMnSc4,k3yIgMnSc4,k3yQgMnSc4
poznala	poznat	k5eAaPmAgFnS
při	při	k7c6
svém	svůj	k3xOyFgInSc6
ročním	roční	k2eAgInSc6d1
studijním	studijní	k2eAgInSc6d1
pobytu	pobyt	k1gInSc6
v	v	k7c6
Oxfordu	Oxford	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2014	#num#	k4
se	se	k3xPyFc4
jejím	její	k3xOp3gMnSc7
přítelem	přítel	k1gMnSc7
stal	stát	k5eAaPmAgMnS
o	o	k7c4
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
mladší	mladý	k2eAgFnSc2d2
Matthew	Matthew	k1gFnSc2
Janney	Jannea	k1gFnSc2
<g/>
,	,	kIx,
student	student	k1gMnSc1
z	z	k7c2
Oxfordu	Oxford	k1gInSc2
a	a	k8xC
hráč	hráč	k1gMnSc1
ragby	ragby	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
2009	#num#	k4
studovala	studovat	k5eAaImAgFnS
anglickou	anglický	k2eAgFnSc4d1
literaturu	literatura	k1gFnSc4
na	na	k7c6
Brownově	Brownův	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
v	v	k7c6
Providence	providence	k1gFnPc4
na	na	k7c4
Rhode	Rhodos	k1gInSc5
Islandu	Island	k1gInSc6
v	v	k7c6
Nové	Nové	k2eAgFnSc6d1
Anglii	Anglie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedle	vedle	k7c2
toho	ten	k3xDgInSc2
začala	začít	k5eAaPmAgFnS
studovat	studovat	k5eAaImF
i	i	k9
psychologii	psychologie	k1gFnSc4
a	a	k8xC
filosofii	filosofie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
25	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2014	#num#	k4
odpromovala	odpromovat	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
studií	studie	k1gFnPc2
absolvovala	absolvovat	k5eAaPmAgFnS
roční	roční	k2eAgInSc4d1
studijní	studijní	k2eAgInSc4d1
pobyt	pobyt	k1gInSc4
v	v	k7c6
Oxfordu	Oxford	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
opravdu	opravdu	k6eAd1
chce	chtít	k5eAaImIp3nS
být	být	k5eAaImF
herečkou	herečka	k1gFnSc7
<g/>
,	,	kIx,
dospěla	dochvít	k5eAaPmAgFnS
až	až	k9
na	na	k7c6
univerzitě	univerzita	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
2014	#num#	k4
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
velvyslankyní	velvyslankyně	k1gFnSc7
dobré	dobrý	k2eAgFnSc2d1
vůle	vůle	k1gFnSc2
OSN	OSN	kA
pro	pro	k7c4
ženy	žena	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Filmová	filmový	k2eAgFnSc1d1
kariéra	kariéra	k1gFnSc1
</s>
<s>
Harry	Harra	k1gFnPc1
Potter	Pottra	k1gFnPc2
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1999	#num#	k4
se	se	k3xPyFc4
objevila	objevit	k5eAaPmAgFnS
na	na	k7c6
castingu	casting	k1gInSc6
první	první	k4xOgFnSc2
části	část	k1gFnSc2
série	série	k1gFnSc2
Harryho	Harry	k1gMnSc2
Pottera	Potter	k1gMnSc2
Harry	Harra	k1gMnSc2
Potter	Potter	k1gInSc1
a	a	k8xC
Kámen	kámen	k1gInSc1
mudrců	mudrc	k1gMnPc2
<g/>
,	,	kIx,
filmové	filmový	k2eAgFnSc3d1
adaptaci	adaptace	k1gFnSc3
stejnojmenné	stejnojmenný	k2eAgFnSc2d1
knihy	kniha	k1gFnSc2
britské	britský	k2eAgFnSc2d1
spisovatelky	spisovatelka	k1gFnSc2
J.	J.	kA
K.	K.	kA
Rowlingové	Rowlingový	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
objevena	objevit	k5eAaPmNgFnS
hlavně	hlavně	k9
díky	díky	k7c3
své	svůj	k3xOyFgFnSc3
učitelce	učitelka	k1gFnSc3
divadla	divadlo	k1gNnSc2
a	a	k8xC
brzy	brzy	k6eAd1
všechny	všechen	k3xTgFnPc4
ohromila	ohromit	k5eAaPmAgFnS
svojí	svůj	k3xOyFgFnSc7
sebejistotou	sebejistota	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
J.	J.	kA
K.	K.	kA
Rowlingová	Rowlingový	k2eAgFnSc1d1
byla	být	k5eAaImAgNnP
Emmou	Emma	k1gFnSc7
Watsonovou	Watsonová	k1gFnSc4
v	v	k7c6
roli	role	k1gFnSc6
Hermiony	Hermion	k1gInPc1
od	od	k7c2
začátku	začátek	k1gInSc2
nadšená	nadšený	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Uvedení	uvedení	k1gNnSc1
filmu	film	k1gInSc2
Harry	Harra	k1gFnSc2
Potter	Potter	k1gInSc1
a	a	k8xC
Kámen	kámen	k1gInSc1
mudrců	mudrc	k1gMnPc2
v	v	k7c6
roce	rok	k1gInSc6
2001	#num#	k4
bylo	být	k5eAaImAgNnS
filmovým	filmový	k2eAgInSc7d1
debutem	debut	k1gInSc7
Watsonové	Watsonová	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Film	film	k1gInSc1
se	se	k3xPyFc4
okamžitě	okamžitě	k6eAd1
vyšplhal	vyšplhat	k5eAaPmAgMnS
na	na	k7c4
první	první	k4xOgFnSc4
příčku	příčka	k1gFnSc4
žebříčku	žebříček	k1gInSc2
sledovanosti	sledovanost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kritika	kritika	k1gFnSc1
chválila	chválit	k5eAaImAgFnS
herecké	herecký	k2eAgInPc4d1
výkony	výkon	k1gInPc4
všech	všecek	k3xTgFnPc2
tří	tři	k4xCgFnPc2
hlavních	hlavní	k2eAgFnPc2d1
postav	postava	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yRgFnPc4,k3yIgFnPc4
ztvárnili	ztvárnit	k5eAaPmAgMnP
Emma	Emma	k1gFnSc1
Watsonová	Watsonová	k1gFnSc1
<g/>
,	,	kIx,
Daniel	Daniel	k1gMnSc1
Radcliffe	Radcliff	k1gInSc5
a	a	k8xC
Rupert	Rupert	k1gMnSc1
Grint	Grint	k1gMnSc1
<g/>
,	,	kIx,
zvláště	zvláště	k6eAd1
však	však	k9
byl	být	k5eAaImAgMnS
zdůrazňován	zdůrazňován	k2eAgInSc4d1
její	její	k3xOp3gInSc4
herecký	herecký	k2eAgInSc4d1
projev	projev	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deník	deník	k1gInSc1
The	The	k1gFnSc2
Daily	Daila	k1gFnSc2
Telegraph	Telegraph	k1gMnSc1
označil	označit	k5eAaPmAgMnS
její	její	k3xOp3gInSc4
výkon	výkon	k1gInSc4
za	za	k7c4
velkolepý	velkolepý	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
roli	role	k1gFnSc4
Hermiony	Hermion	k1gInPc1
Grangerové	Grangerový	k2eAgInPc1d1
byla	být	k5eAaImAgFnS
nominována	nominovat	k5eAaBmNgFnS
na	na	k7c4
pět	pět	k4xCc4
cen	cena	k1gFnPc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgInPc2
skutečně	skutečně	k6eAd1
vyhrála	vyhrát	k5eAaPmAgFnS
cenu	cena	k1gFnSc4
za	za	k7c4
hlavní	hlavní	k2eAgFnSc4d1
ženskou	ženský	k2eAgFnSc4d1
roli	role	k1gFnSc4
v	v	k7c6
kategorii	kategorie	k1gFnSc6
mladých	mladý	k2eAgMnPc2d1
umělců	umělec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
O	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
opět	opět	k6eAd1
Hermionou	Hermiona	k1gFnSc7
<g/>
,	,	kIx,
tentokrát	tentokrát	k6eAd1
v	v	k7c6
druhém	druhý	k4xOgInSc6
dílu	díl	k1gInSc6
potterovské	potterovský	k2eAgFnSc2d1
série	série	k1gFnSc2
–	–	k?
Harry	Harra	k1gFnSc2
Potter	Pottra	k1gFnPc2
a	a	k8xC
Tajemná	tajemný	k2eAgFnSc1d1
komnata	komnata	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoliv	ačkoliv	k8xS
kritiky	kritika	k1gFnPc1
filmu	film	k1gInSc2
se	se	k3xPyFc4
lišily	lišit	k5eAaImAgFnP
<g/>
,	,	kIx,
většina	většina	k1gFnSc1
z	z	k7c2
nich	on	k3xPp3gFnPc2
byla	být	k5eAaImAgFnS
pozitivní	pozitivní	k2eAgFnSc1d1
díky	díky	k7c3
výkonům	výkon	k1gInPc3
hlavních	hlavní	k2eAgFnPc2d1
postav	postava	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
List	list	k1gInSc1
Los	los	k1gInSc4
Angeles	Angeles	k1gMnSc1
Times	Times	k1gMnSc1
uvedl	uvést	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
ona	onen	k3xDgFnSc1,k3xPp3gFnSc1
a	a	k8xC
její	její	k3xOp3gMnPc1
kolegové	kolega	k1gMnPc1
během	během	k7c2
druhého	druhý	k4xOgInSc2
dílu	díl	k1gInSc2
dozráli	dozrát	k5eAaPmAgMnP
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
The	The	k1gMnSc1
Times	Times	k1gMnSc1
kritizoval	kritizovat	k5eAaImAgMnS
režiséra	režisér	k1gMnSc4
Chrise	Chrise	k1gFnSc2
Columbuse	Columbuse	k1gFnSc2
za	za	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
role	role	k1gFnSc1
Hermiony	Hermion	k1gInPc1
je	být	k5eAaImIp3nS
v	v	k7c6
tomto	tento	k3xDgInSc6
díle	díl	k1gInSc6
příliš	příliš	k6eAd1
nevýrazná	výrazný	k2eNgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
německého	německý	k2eAgInSc2d1
časopisu	časopis	k1gInSc2
pro	pro	k7c4
mládež	mládež	k1gFnSc4
Bravo	bravo	k6eAd1
za	za	k7c4
svůj	svůj	k3xOyFgInSc4
výkon	výkon	k1gInSc4
obdržela	obdržet	k5eAaPmAgFnS
Cenu	cena	k1gFnSc4
Otto	Otto	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
byl	být	k5eAaImAgInS
uveden	uvést	k5eAaPmNgInS
třetí	třetí	k4xOgInSc1
díl	díl	k1gInSc1
filmové	filmový	k2eAgFnSc2d1
série	série	k1gFnSc2
–	–	k?
Harry	Harra	k1gFnSc2
Potter	Potter	k1gMnSc1
a	a	k8xC
vězeň	vězeň	k1gMnSc1
z	z	k7c2
Azkabanu	Azkaban	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoliv	ačkoliv	k8xS
kritiky	kritika	k1gFnPc1
označily	označit	k5eAaPmAgFnP
Daniela	Daniel	k1gMnSc2
Radcliffa	Radcliff	k1gMnSc4
za	za	k7c2
příliš	příliš	k6eAd1
prkenného	prkenný	k2eAgInSc2d1
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
za	za	k7c4
roli	role	k1gFnSc4
Hermiony	Hermion	k1gInPc1
chválena	chválit	k5eAaImNgNnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
New	New	k1gFnSc2
York	York	k1gInSc1
Times	Times	k1gMnSc1
pěl	pět	k5eAaImAgMnS
chválu	chvála	k1gFnSc4
na	na	k7c4
její	její	k3xOp3gInSc4
výkon	výkon	k1gInSc4
<g/>
,	,	kIx,
uvedl	uvést	k5eAaPmAgMnS
<g/>
:	:	kIx,
„	„	k?
<g/>
Naštěstí	naštěstí	k6eAd1
umírněnost	umírněnost	k1gFnSc1
pana	pan	k1gMnSc2
Radcliffa	Radcliff	k1gMnSc2
je	být	k5eAaImIp3nS
vyvážena	vyvážen	k2eAgFnSc1d1
netrpělivostí	netrpělivost	k1gFnSc7
a	a	k8xC
netečností	netečnost	k1gFnSc7
slečny	slečna	k1gFnSc2
Watsonové	Watsonová	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Harry	Harr	k1gMnPc7
může	moct	k5eAaImIp3nS
dát	dát	k5eAaPmF
na	na	k7c4
obdiv	obdiv	k1gInSc4
své	svůj	k3xOyFgFnSc2
čarodějné	čarodějný	k2eAgFnSc2d1
dovednosti	dovednost	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
Hermiona	Hermiona	k1gFnSc1
<g/>
,	,	kIx,
ta	ten	k3xDgFnSc1
sklízí	sklízet	k5eAaImIp3nS
potlesk	potlesk	k1gInSc4
za	za	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
naprosto	naprosto	k6eAd1
nezkušeným	zkušený	k2eNgInSc7d1
a	a	k8xC
spontánním	spontánní	k2eAgInSc7d1
úderem	úder	k1gInSc7
zlomí	zlomit	k5eAaPmIp3nS
Dracu	Drac	k1gMnSc3
Malfoyovi	Malfoya	k1gMnSc3
nos	nos	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoliv	ačkoliv	k8xS
Harry	Harr	k1gInPc4
Potter	Pottra	k1gFnPc2
a	a	k8xC
vězeň	vězeň	k1gMnSc1
z	z	k7c2
Azkabanu	Azkaban	k1gInSc2
byl	být	k5eAaImAgInS
označen	označit	k5eAaPmNgInS
za	za	k7c4
nejméně	málo	k6eAd3
výdělečný	výdělečný	k2eAgInSc4d1
film	film	k1gInSc4
ze	z	k7c2
série	série	k1gFnSc2
Harryho	Harry	k1gMnSc2
Pottera	Potter	k1gMnSc2
<g/>
,	,	kIx,
její	její	k3xOp3gMnSc1
výkon	výkon	k1gInSc4
pro	pro	k7c4
ni	on	k3xPp3gFnSc4
znamenal	znamenat	k5eAaImAgInS
dvě	dva	k4xCgFnPc4
ocenění	ocenění	k1gNnSc3
Otto	Otto	k1gMnSc1
a	a	k8xC
cenu	cena	k1gFnSc4
za	za	k7c4
dětskou	dětský	k2eAgFnSc4d1
roli	role	k1gFnSc4
roku	rok	k1gInSc2
<g/>
,	,	kIx,
jednu	jeden	k4xCgFnSc4
z	z	k7c2
cen	cena	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yRgFnPc4,k3yQgFnPc4
každoročně	každoročně	k6eAd1
uděluje	udělovat	k5eAaImIp3nS
časopis	časopis	k1gInSc4
Total	totat	k5eAaImAgInS
Film	film	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
S	s	k7c7
dalším	další	k2eAgNnSc7d1
dílem	dílo	k1gNnSc7
s	s	k7c7
názvem	název	k1gInSc7
Harry	Harra	k1gFnSc2
Potter	Potter	k1gInSc1
a	a	k8xC
Ohnivý	ohnivý	k2eAgInSc1d1
pohár	pohár	k1gInSc1
„	„	k?
<g/>
potterovská	potterovský	k2eAgFnSc1d1
série	série	k1gFnSc1
<g/>
“	“	k?
překročila	překročit	k5eAaPmAgFnS
další	další	k2eAgInSc4d1
milník	milník	k1gInSc4
ve	v	k7c6
světě	svět	k1gInSc6
filmu	film	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kritiky	kritika	k1gFnPc1
chválily	chválit	k5eAaImAgFnP
rostoucí	rostoucí	k2eAgFnSc4d1
zralost	zralost	k1gFnSc4
její	její	k3xOp3gFnSc2
i	i	k9
jejích	její	k3xOp3gMnPc2
hereckých	herecký	k2eAgMnPc2d1
kolegů	kolega	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
New	New	k1gFnSc2
York	York	k1gInSc1
Times	Times	k1gMnSc1
označil	označit	k5eAaPmAgMnS
její	její	k3xOp3gInSc4
výkon	výkon	k1gInSc4
za	za	k7c4
„	„	k?
<g/>
dojemně	dojemně	k6eAd1
opravdový	opravdový	k2eAgInSc4d1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
tří	tři	k4xCgFnPc2
nominací	nominace	k1gFnPc2
získala	získat	k5eAaPmAgFnS
bronzovou	bronzový	k2eAgFnSc4d1
cenu	cena	k1gFnSc4
Otto	Otto	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
toho	ten	k3xDgInSc2
roku	rok	k1gInSc2
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
nejmladší	mladý	k2eAgFnSc7d3
osobností	osobnost	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
kdy	kdy	k6eAd1
objevila	objevit	k5eAaPmAgFnS
na	na	k7c6
titulní	titulní	k2eAgFnSc6d1
stránce	stránka	k1gFnSc6
časopisu	časopis	k1gInSc2
Teen	Teen	k1gInSc4
Vogue	Vogue	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
dostala	dostat	k5eAaPmAgFnS
roli	role	k1gFnSc4
Hermiony	Hermion	k1gInPc1
ve	v	k7c6
hře	hra	k1gFnSc6
Queen	Queno	k1gNnPc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Handbag	Handbaga	k1gFnPc2
(	(	kIx(
<g/>
Královnina	královnin	k2eAgFnSc1d1
kabelka	kabelka	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
speciální	speciální	k2eAgFnSc3d1
miniepisodě	miniepisoda	k1gFnSc3
seriálu	seriál	k1gInSc2
o	o	k7c4
Harry	Harra	k1gFnPc4
Potterovi	Potter	k1gMnSc3
<g/>
,	,	kIx,
dávané	dávaný	k2eAgFnSc2d1
na	na	k7c4
počest	počest	k1gFnSc4
80	#num#	k4
<g/>
.	.	kIx.
narozenin	narozeniny	k1gFnPc2
královny	královna	k1gFnSc2
Alžběty	Alžběta	k1gFnSc2
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Pátý	pátý	k4xOgInSc1
díl	díl	k1gInSc1
Harryho	Harry	k1gMnSc2
Pottera	Potter	k1gMnSc2
s	s	k7c7
názvem	název	k1gInSc7
Harry	Harra	k1gFnSc2
Potter	Potter	k1gInSc1
a	a	k8xC
Fénixův	fénixův	k2eAgInSc1d1
řád	řád	k1gInSc1
byl	být	k5eAaImAgInS
uveden	uvést	k5eAaPmNgInS
do	do	k7c2
kin	kino	k1gNnPc2
v	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Film	film	k1gInSc1
sklidil	sklidit	k5eAaPmAgInS
obrovský	obrovský	k2eAgInSc4d1
finanční	finanční	k2eAgInSc4d1
úspěch	úspěch	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celosvětové	celosvětový	k2eAgFnPc1d1
tržby	tržba	k1gFnPc1
během	během	k7c2
jednoho	jeden	k4xCgInSc2
víkendu	víkend	k1gInSc2
dosáhly	dosáhnout	k5eAaPmAgFnP
332,7	332,7	k4
milionů	milion	k4xCgInPc2
amerických	americký	k2eAgInPc2d1
dolarů	dolar	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
ke	k	k7c3
své	svůj	k3xOyFgFnSc3
rostoucí	rostoucí	k2eAgFnSc3d1
proslulosti	proslulost	k1gFnSc3
a	a	k8xC
úspěchu	úspěch	k1gInSc3
celé	celý	k2eAgFnSc2d1
potterovské	potterovský	k2eAgFnSc2d1
série	série	k1gFnSc2
byli	být	k5eAaImAgMnP
ona	onen	k3xDgFnSc1,k3xPp3gFnSc1
sama	sám	k3xTgFnSc1
i	i	k9
její	její	k3xOp3gMnPc1
herečtí	herecký	k2eAgMnPc1d1
kolegové	kolega	k1gMnPc1
Daniel	Daniel	k1gMnSc1
Radcliffe	Radcliff	k1gInSc5
a	a	k8xC
Rupert	Rupert	k1gMnSc1
Grint	Grinta	k1gFnPc2
požádáni	požádat	k5eAaPmNgMnP
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
zanechali	zanechat	k5eAaPmAgMnP
otisky	otisk	k1gInPc4
svých	svůj	k3xOyFgFnPc2
rukou	ruka	k1gFnPc2
<g/>
,	,	kIx,
nohou	noha	k1gFnPc2
a	a	k8xC
kouzelných	kouzelný	k2eAgFnPc2d1
hůlek	hůlka	k1gFnPc2
před	před	k7c7
kinem	kino	k1gNnSc7
Grauman	Grauman	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Chinese	Chinese	k1gFnPc1
Theatre	Theatr	k1gInSc5
v	v	k7c6
Hollywoodu	Hollywood	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Šestý	šestý	k4xOgInSc1
díl	díl	k1gInSc1
Harryho	Harry	k1gMnSc2
Pottera	Potter	k1gMnSc2
s	s	k7c7
názvem	název	k1gInSc7
Harry	Harra	k1gFnSc2
Potter	Potter	k1gInSc1
a	a	k8xC
Princ	princ	k1gMnSc1
dvojí	dvojí	k4xRgFnSc2
krve	krev	k1gFnSc2
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
prvně	prvně	k?
promítán	promítán	k2eAgInSc1d1
v	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
(	(	kIx(
<g/>
knižní	knižní	k2eAgFnSc1d1
předloha	předloha	k1gFnSc1
vyšla	vyjít	k5eAaPmAgFnS
16	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2005	#num#	k4
<g/>
,	,	kIx,
a	a	k8xC
stala	stát	k5eAaPmAgFnS
se	se	k3xPyFc4
nejrychleji	rychle	k6eAd3
prodávanou	prodávaný	k2eAgFnSc7d1
knihou	kniha	k1gFnSc7
své	svůj	k3xOyFgFnSc2
doby	doba	k1gFnSc2
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
–	–	k?
v	v	k7c6
USA	USA	kA
za	za	k7c4
24	#num#	k4
hodin	hodina	k1gFnPc2
bylo	být	k5eAaImAgNnS
prodáno	prodat	k5eAaPmNgNnS
6,9	6,9	k4
milionů	milion	k4xCgInPc2
výtisků	výtisk	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
náklady	náklad	k1gInPc4
250	#num#	k4
mil	míle	k1gFnPc2
<g/>
.	.	kIx.
dolarů	dolar	k1gInPc2
byl	být	k5eAaImAgMnS
jedním	jeden	k4xCgMnSc7
z	z	k7c2
nejdražších	drahý	k2eAgMnPc2d3
filmů	film	k1gInPc2
celé	celý	k2eAgFnSc2d1
série	série	k1gFnSc2
(	(	kIx(
<g/>
z	z	k7c2
premiér	premiéra	k1gFnPc2
utržil	utržit	k5eAaPmAgInS
22	#num#	k4
mil	míle	k1gFnPc2
<g/>
.	.	kIx.
dolarů	dolar	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Režisérem	režisér	k1gMnSc7
byl	být	k5eAaImAgMnS
David	David	k1gMnSc1
Yates	Yates	k1gMnSc1
<g/>
;	;	kIx,
v	v	k7c6
USA	USA	kA
byl	být	k5eAaImAgInS
promítán	promítán	k2eAgInSc1d1
s	s	k7c7
ratingem	rating	k1gInSc7
PG	PG	kA
(	(	kIx(
<g/>
„	„	k?
<g/>
některé	některý	k3yIgFnPc1
scény	scéna	k1gFnPc1
nejsou	být	k5eNaImIp3nP
vhodné	vhodný	k2eAgFnPc1d1
pro	pro	k7c4
děti	dítě	k1gFnPc4
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Film	film	k1gInSc1
byl	být	k5eAaImAgInS
fanoušky	fanoušek	k1gMnPc7
přijat	přijat	k2eAgInSc4d1
vcelku	vcelku	k6eAd1
kladně	kladně	k6eAd1
<g/>
,	,	kIx,
kritici	kritik	k1gMnPc1
už	už	k6eAd1
však	však	k9
takovou	takový	k3xDgFnSc7
chválou	chvála	k1gFnSc7
šetřili	šetřit	k5eAaImAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Radomír	Radomír	k1gMnSc1
Kokeš	Kokeš	k1gMnSc1
(	(	kIx(
<g/>
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
)	)	kIx)
například	například	k6eAd1
napsal	napsat	k5eAaPmAgMnS,k5eAaBmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
film	film	k1gInSc1
je	být	k5eAaImIp3nS
určité	určitý	k2eAgNnSc4d1
zklamání	zklamání	k1gNnSc4
a	a	k8xC
pouze	pouze	k6eAd1
odsouvá	odsouvat	k5eAaImIp3nS
(	(	kIx(
<g/>
zbytečně	zbytečně	k6eAd1
vysvětluje	vysvětlovat	k5eAaImIp3nS
<g/>
)	)	kIx)
finále	finále	k1gNnSc1
série	série	k1gFnSc2
po	po	k7c6
„	„	k?
<g/>
nadupaném	nadupaný	k2eAgInSc6d1
<g/>
“	“	k?
pátém	pátý	k4xOgInSc6
dílu	díl	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Alena	Alena	k1gFnSc1
Prokopcová	Prokopcová	k1gFnSc1
hodnotí	hodnotit	k5eAaImIp3nS
<g />
.	.	kIx.
</s>
<s hack="1">
šestý	šestý	k4xOgInSc1
díl	díl	k1gInSc1
jako	jako	k8xC,k8xS
nejslabší	slabý	k2eAgFnSc7d3
(	(	kIx(
<g/>
naproti	naproti	k7c3
tomu	ten	k3xDgNnSc3
fanoušci	fanoušek	k1gMnPc1
i	i	k8xC
samotná	samotný	k2eAgFnSc1d1
autorka	autorka	k1gFnSc1
J.	J.	kA
K.	K.	kA
Rowlingová	Rowlingový	k2eAgFnSc1d1
jej	on	k3xPp3gMnSc4
hodnotí	hodnotit	k5eAaImIp3nS
jako	jako	k8xS,k8xC
jeden	jeden	k4xCgMnSc1
z	z	k7c2
nejsilnějších	silný	k2eAgMnPc2d3
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
David	David	k1gMnSc1
Hanuš	Hanuš	k1gMnSc1
(	(	kIx(
<g/>
blog	blog	k1gMnSc1
<g/>
.	.	kIx.
<g/>
IDnes	IDnes	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
)	)	kIx)
pak	pak	k6eAd1
poukazuje	poukazovat	k5eAaImIp3nS
na	na	k7c4
pouhé	pouhý	k2eAgNnSc4d1
<g />
.	.	kIx.
</s>
<s hack="1">
„	„	k?
<g/>
vykradení	vykradení	k1gNnSc4
<g/>
“	“	k?
hudebních	hudební	k2eAgInPc2d1
motivů	motiv	k1gInPc2
Nicholasem	Nicholas	k1gMnSc7
Hooperem	Hooper	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
nepřichází	přicházet	k5eNaImIp3nS
s	s	k7c7
ničím	nic	k3yNnSc7
originálním	originální	k2eAgNnSc7d1
<g/>
,	,	kIx,
oproti	oproti	k7c3
Johnu	John	k1gMnSc3
Williamesovi	Williames	k1gMnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
A	a	k9
nakonec	nakonec	k6eAd1
Jakub	Jakub	k1gMnSc1
Leníček	Leníček	k1gMnSc1
(	(	kIx(
<g/>
Rozhlas	rozhlas	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
)	)	kIx)
rovnou	rovnou	k6eAd1
nabádá	nabádat	k5eAaImIp3nS,k5eAaBmIp3nS
diváky	divák	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
nejsou	být	k5eNaImIp3nP
„	„	k?
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
políbeni	políben	k2eAgMnPc1d1
<g/>
“	“	k?
světem	svět	k1gInSc7
Harryho	Harry	k1gMnSc2
Pottera	Potter	k1gMnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
na	na	k7c4
film	film	k1gInSc4
do	do	k7c2
kin	kino	k1gNnPc2
rovnou	rovnou	k6eAd1
nechodili	chodit	k5eNaImAgMnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Výkony	výkon	k1gInPc1
herců	herc	k1gInPc2
jsou	být	k5eAaImIp3nP
hodnoceny	hodnotit	k5eAaImNgInP
stejně	stejně	k6eAd1
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
v	v	k7c6
předchozích	předchozí	k2eAgNnPc6d1
dílech	dílo	k1gNnPc6
–	–	k?
kritici	kritik	k1gMnPc1
si	se	k3xPyFc3
více	hodně	k6eAd2
všímají	všímat	k5eAaImIp3nP
přirozeného	přirozený	k2eAgNnSc2d1
herectví	herectví	k1gNnSc2
Emmy	Emma	k1gFnSc2
Watsonové	Watsonová	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
Ruperta	Ruperta	k1gFnSc1
Grinta	Grinta	k1gFnSc1
<g/>
,	,	kIx,
a	a	k8xC
do	do	k7c2
budoucna	budoucno	k1gNnSc2
od	od	k7c2
nich	on	k3xPp3gMnPc2
očekávají	očekávat	k5eAaImIp3nP
hodnotné	hodnotný	k2eAgNnSc4d1
herecké	herecký	k2eAgNnSc4d1
vystoupení	vystoupení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Sedmý	sedmý	k4xOgInSc1
díl	díl	k1gInSc1
Harryho	Harry	k1gMnSc2
Pottera	Potter	k1gMnSc2
s	s	k7c7
názvem	název	k1gInSc7
Harry	Harra	k1gFnSc2
Potter	Potter	k1gInSc1
a	a	k8xC
relikvie	relikvie	k1gFnSc2
smrti	smrt	k1gFnSc2
byl	být	k5eAaImAgInS
knižně	knižně	k6eAd1
vydán	vydat	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
<g/>
,	,	kIx,
a	a	k8xC
okamžitě	okamžitě	k6eAd1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
nejprodávanější	prodávaný	k2eAgFnSc7d3
knihou	kniha	k1gFnSc7
své	svůj	k3xOyFgFnSc2
doby	doba	k1gFnSc2
(	(	kIx(
<g/>
11	#num#	k4
mil	míle	k1gFnPc2
<g/>
.	.	kIx.
výtisků	výtisk	k1gInPc2
za	za	k7c4
25	#num#	k4
h	h	k?
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
překonal	překonat	k5eAaPmAgMnS
předchozí	předchozí	k2eAgInSc4d1
díl	díl	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
]	]	kIx)
Z	z	k7c2
důvodu	důvod	k1gInSc2
velkého	velký	k2eAgInSc2d1
rozsahu	rozsah	k1gInSc2
příběhu	příběh	k1gInSc2
se	se	k3xPyFc4
producenti	producent	k1gMnPc1
rozhodli	rozhodnout	k5eAaPmAgMnP
knihu	kniha	k1gFnSc4
zfilmovat	zfilmovat	k5eAaPmF
na	na	k7c4
pokračování	pokračování	k1gNnSc4
(	(	kIx(
<g/>
natáčet	natáčet	k5eAaImF
se	se	k3xPyFc4
začalo	začít	k5eAaPmAgNnS
v	v	k7c6
únoru	únor	k1gInSc6
2009	#num#	k4
<g/>
,	,	kIx,
natáčení	natáčení	k1gNnSc1
probíhalo	probíhat	k5eAaImAgNnS
souběžně	souběžně	k6eAd1
<g/>
,	,	kIx,
první	první	k4xOgInSc4
díl	díl	k1gInSc4
měl	mít	k5eAaImAgMnS
premiéru	premiéra	k1gFnSc4
11	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2010	#num#	k4
<g/>
,	,	kIx,
druhý	druhý	k4xOgInSc4
pak	pak	k8xC
7	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2011	#num#	k4
<g/>
;	;	kIx,
režisérem	režisér	k1gMnSc7
byl	být	k5eAaImAgMnS
opět	opět	k6eAd1
David	David	k1gMnSc1
Yates	Yates	k1gMnSc1
<g/>
;	;	kIx,
náklady	náklad	k1gInPc1
byly	být	k5eAaImAgInP
250	#num#	k4
mil	míle	k1gFnPc2
<g/>
.	.	kIx.
dolarů	dolar	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rozdělení	rozdělení	k1gNnSc1
knižní	knižní	k2eAgFnSc2d1
předlohy	předloha	k1gFnSc2
na	na	k7c4
2	#num#	k4
filmy	film	k1gInPc7
bylo	být	k5eAaImAgNnS
často	často	k6eAd1
terčem	terč	k1gInSc7
polemiky	polemika	k1gFnSc2
–	–	k?
vzhledem	vzhledem	k7c3
k	k	k7c3
rozsahu	rozsah	k1gInSc3
knihy	kniha	k1gFnSc2
to	ten	k3xDgNnSc1
však	však	k9
bylo	být	k5eAaImAgNnS
takřka	takřka	k6eAd1
nutností	nutnost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Emma	Emma	k1gFnSc1
Watson	Watsona	k1gFnPc2
(	(	kIx(
<g/>
Hermiona	Hermiona	k1gFnSc1
Grangerová	Grangerová	k1gFnSc1
<g/>
)	)	kIx)
zde	zde	k6eAd1
vynikla	vyniknout	k5eAaPmAgFnS
ve	v	k7c6
scéně	scéna	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
paradoxně	paradoxně	k6eAd1
chybí	chybět	k5eAaImIp3nS,k5eAaPmIp3nS
v	v	k7c6
knize	kniha	k1gFnSc6
–	–	k?
tanec	tanec	k1gInSc1
s	s	k7c7
Danielem	Daniel	k1gMnSc7
Radcliffem	Radcliff	k1gMnSc7
(	(	kIx(
<g/>
Harry	Harra	k1gMnSc2
Potter	Potter	k1gMnSc1
<g/>
)	)	kIx)
ve	v	k7c6
stanu	stan	k1gInSc6
při	při	k7c6
hledání	hledání	k1gNnSc6
viteálů	viteál	k1gInPc2
(	(	kIx(
<g/>
sama	sám	k3xTgFnSc1
autorka	autorka	k1gFnSc1
J.	J.	kA
K.	K.	kA
Rowlingová	Rowlingový	k2eAgFnSc1d1
prohlásila	prohlásit	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
její	její	k3xOp3gMnPc4
<g />
.	.	kIx.
</s>
<s hack="1">
nejoblíbenější	oblíbený	k2eAgFnSc4d3
scénu	scéna	k1gFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Diváci	divák	k1gMnPc1
se	se	k3xPyFc4
také	také	k9
dočkali	dočkat	k5eAaPmAgMnP
(	(	kIx(
<g/>
konečně	konečně	k6eAd1
<g/>
)	)	kIx)
„	„	k?
<g/>
líbací	líbací	k2eAgFnSc2d1
scény	scéna	k1gFnSc2
<g/>
“	“	k?
mezi	mezi	k7c7
Emmou	Emma	k1gFnSc7
Watsonovou	Watsonová	k1gFnSc7
a	a	k8xC
Rupertem	Rupert	k1gMnSc7
Grintem	Grint	k1gMnSc7
(	(	kIx(
<g/>
Ron	Ron	k1gMnSc1
Weasley	Weaslea	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
o	o	k7c4
které	který	k3yQgMnPc4,k3yIgMnPc4,k3yRgMnPc4
Emma	Emma	k1gFnSc1
prohlásila	prohlásit	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
jí	jíst	k5eAaImIp3nS
<g />
.	.	kIx.
</s>
<s hack="1">
bála	bát	k5eAaImAgFnS
kvůli	kvůli	k7c3
jejich	jejich	k3xOp3gNnSc3
přátelství	přátelství	k1gNnSc1
–	–	k?
oba	dva	k4xCgMnPc1
ji	on	k3xPp3gFnSc4
nakonec	nakonec	k6eAd1
filmově	filmově	k6eAd1
zvládli	zvládnout	k5eAaPmAgMnP
velmi	velmi	k6eAd1
dobře	dobře	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
konci	konec	k1gInSc6
natáčení	natáčení	k1gNnSc2
nakonec	nakonec	k6eAd1
pomáhala	pomáhat	k5eAaImAgFnS
Rupertovi	Rupertův	k2eAgMnPc1d1
všem	všecek	k3xTgMnPc3
členům	člen	k1gMnPc3
štábu	štáb	k1gInSc2
rozdávat	rozdávat	k5eAaImF
zmrzlinu	zmrzlina	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
spolu	spolu	k6eAd1
s	s	k7c7
ním	on	k3xPp3gInSc7
točila	točit	k5eAaImAgFnS
v	v	k7c6
jeho	jeho	k3xOp3gNnSc6
zmrzlinářském	zmrzlinářský	k2eAgNnSc6d1
autě	auto	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
také	také	k9
o	o	k7c4
jediný	jediný	k2eAgInSc4d1
film	film	k1gInSc4
(	(	kIx(
<g/>
Relikvie	relikvie	k1gFnSc1
smrti	smrt	k1gFnSc2
2	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kde	kde	k6eAd1
Emma	Emma	k1gFnSc1
Watson	Watsona	k1gFnPc2
létá	létat	k5eAaImIp3nS
na	na	k7c6
koštěti	koště	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
poslední	poslední	k2eAgFnSc6d1
klapce	klapka	k1gFnSc6
si	se	k3xPyFc3
pak	pak	k6eAd1
herečka	herečka	k1gFnSc1
symbolicky	symbolicky	k6eAd1
ostříhala	ostříhat	k5eAaPmAgFnS,k5eAaImAgFnS
své	svůj	k3xOyFgInPc4
dlouhé	dlouhý	k2eAgInPc4d1
vlasy	vlas	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Druhý	druhý	k4xOgInSc1
díl	díl	k1gInSc1
je	být	k5eAaImIp3nS
také	také	k9
jediný	jediný	k2eAgInSc4d1
z	z	k7c2
celé	celý	k2eAgFnSc2d1
ságy	sága	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
překročil	překročit	k5eAaPmAgInS
ve	v	k7c6
svých	svůj	k3xOyFgFnPc6
tržbách	tržba	k1gFnPc6
magickou	magický	k2eAgFnSc4d1
hranici	hranice	k1gFnSc4
1	#num#	k4
mld.	mld.	k?
dolarů	dolar	k1gInPc2
(	(	kIx(
<g/>
za	za	k7c2
první	první	k4xOgFnSc2
3	#num#	k4
týdny	týden	k1gInPc7
v	v	k7c6
kinech	kino	k1gNnPc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Samotné	samotný	k2eAgFnPc1d1
kritiky	kritika	k1gFnPc1
na	na	k7c4
oba	dva	k4xCgInPc4
dva	dva	k4xCgInPc4
díly	díl	k1gInPc4
se	se	k3xPyFc4
různí	různý	k2eAgMnPc1d1
–	–	k?
fanoušci	fanoušek	k1gMnPc1
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
kritici	kritik	k1gMnPc1
vyzdvihují	vyzdvihovat	k5eAaImIp3nP
spíše	spíše	k9
Relikvie	relikvie	k1gFnPc4
smrti	smrt	k1gFnSc2
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInSc1
díl	díl	k1gInSc1
je	být	k5eAaImIp3nS
spíše	spíše	k9
označován	označovat	k5eAaImNgInS
za	za	k7c2
„	„	k?
<g/>
teenagerovský	teenagerovský	k2eAgInSc1d1
<g/>
“	“	k?
<g/>
,	,	kIx,
ale	ale	k8xC
vyzrálý	vyzrálý	k2eAgInSc4d1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
například	například	k6eAd1
Emma	Emma	k1gFnSc1
Watson	Watsona	k1gFnPc2
podává	podávat	k5eAaImIp3nS
své	svůj	k3xOyFgFnPc4
emoce	emoce	k1gFnPc4
velice	velice	k6eAd1
opravdově	opravdově	k6eAd1
(	(	kIx(
<g/>
Milan	Milan	k1gMnSc1
Klíma	Klíma	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Další	další	k2eAgInPc1d1
názory	názor	k1gInPc1
zohledňují	zohledňovat	k5eAaImIp3nP
nemožnost	nemožnost	k1gFnSc4
strhujícího	strhující	k2eAgInSc2d1
děje	děj	k1gInSc2
kvůli	kvůli	k7c3
absenci	absence	k1gFnSc3
finále	finále	k1gNnSc2
<g/>
,	,	kIx,
kterého	který	k3yQgInSc2,k3yRgInSc2,k3yIgInSc2
se	se	k3xPyFc4
divák	divák	k1gMnSc1
dočká	dočkat	k5eAaPmIp3nS
až	až	k9
ve	v	k7c6
druhém	druhý	k4xOgNnSc6
díle	dílo	k1gNnSc6
(	(	kIx(
<g/>
Jakub	Jakub	k1gMnSc1
Miller	Miller	k1gMnSc1
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
Simona	Simona	k1gFnSc1
Schröderová	Schröderová	k1gFnSc1
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
druhý	druhý	k4xOgInSc4
díl	díl	k1gInSc4
se	se	k3xPyFc4
můžeme	moct	k5eAaImIp1nP
také	také	k9
setkat	setkat	k5eAaPmF
s	s	k7c7
negativní	negativní	k2eAgFnSc7d1
kritikou	kritika	k1gFnSc7
(	(	kIx(
<g/>
Radomír	Radomír	k1gMnSc1
Kokeš	Kokeš	k1gMnSc1
označuje	označovat	k5eAaImIp3nS
oba	dva	k4xCgInPc4
díly	díl	k1gInPc4
za	za	k7c2
„	„	k?
<g/>
blbé	blbý	k2eAgNnSc1d1
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Značně	značně	k6eAd1
však	však	k9
převládají	převládat	k5eAaImIp3nP
kladné	kladný	k2eAgFnPc1d1
kritiky	kritika	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
si	se	k3xPyFc3
především	především	k6eAd1
všímají	všímat	k5eAaImIp3nP
dospělosti	dospělost	k1gFnSc2
<g/>
,	,	kIx,
vyzrálosti	vyzrálost	k1gFnSc2
a	a	k8xC
„	„	k?
<g/>
ženskosti	ženskost	k1gFnSc2
<g/>
“	“	k?
Emmy	Emma	k1gFnSc2
Watson	Watsona	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Setkáme	setkat	k5eAaPmIp1nP
se	se	k3xPyFc4
i	i	k9
s	s	k7c7
názorem	názor	k1gInSc7
<g/>
,	,	kIx,
že	že	k8xS
Relikvie	relikvie	k1gFnSc1
smrti	smrt	k1gFnSc2
–	–	k?
část	část	k1gFnSc4
2	#num#	k4
jsou	být	k5eAaImIp3nP
„	„	k?
<g/>
opravdu	opravdu	k6eAd1
skvělá	skvělý	k2eAgFnSc1d1
podívaná	podívaná	k1gFnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
epilogu	epilog	k1gInSc6
Harry	Harra	k1gFnSc2
Potter	Potter	k1gInSc1
a	a	k8xC
relikvie	relikvie	k1gFnSc1
smrti	smrt	k1gFnSc2
–	–	k?
část	část	k1gFnSc1
2	#num#	k4
objeví	objevit	k5eAaPmIp3nS
Hermiona	Hermiona	k1gFnSc1
Grangerová	Grangerový	k2eAgFnSc1d1
(	(	kIx(
<g/>
Emma	Emma	k1gFnSc1
Watson	Watsona	k1gFnPc2
<g/>
)	)	kIx)
o	o	k7c4
19	#num#	k4
let	léto	k1gNnPc2
starší	starý	k2eAgMnSc1d2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
jako	jako	k9
manželka	manželka	k1gFnSc1
Rona	Ron	k1gMnSc2
Weasleyho	Weasley	k1gMnSc2
(	(	kIx(
<g/>
Rupert	Rupert	k1gMnSc1
Grint	Grint	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
doprovázejí	doprovázet	k5eAaImIp3nP
své	svůj	k3xOyFgFnPc4
děti	dítě	k1gFnPc4
na	na	k7c4
„	„	k?
<g/>
nástupiště	nástupiště	k1gNnSc4
9	#num#	k4
a	a	k8xC
3	#num#	k4
<g/>
/	/	kIx~
<g/>
4	#num#	k4
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hraje	hrát	k5eAaImIp3nS
zde	zde	k6eAd1
však	však	k9
menší	malý	k2eAgFnSc4d2
roli	role	k1gFnSc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
epilog	epilog	k1gInSc1
je	být	k5eAaImIp3nS
zaměřený	zaměřený	k2eAgInSc1d1
na	na	k7c4
vztah	vztah	k1gInSc4
mezi	mezi	k7c4
Harrym	Harrym	k1gInSc4
Potterem	Potter	k1gMnSc7
(	(	kIx(
<g/>
Daniel	Daniel	k1gMnSc1
Radcliffe	Radcliff	k1gInSc5
<g/>
)	)	kIx)
a	a	k8xC
jeho	jeho	k3xOp3gFnSc1
druhým	druhý	k4xOgNnSc7
synem	syn	k1gMnSc7
Albusem	Albus	k1gMnSc7
Severusem	Severus	k1gMnSc7
Potterem	Potter	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následuje	následovat	k5eAaImIp3nS
děj	děj	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
rozpracovává	rozpracovávat	k5eAaImIp3nS
„	„	k?
<g/>
co	co	k9
bylo	být	k5eAaImAgNnS
dál	daleko	k6eAd2
<g/>
“	“	k?
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
nebyl	být	k5eNaImAgInS
zfilmován	zfilmovat	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
J.	J.	kA
K.	K.	kA
Rowlingová	Rowlingový	k2eAgFnSc1d1
ho	on	k3xPp3gInSc4
spolu	spolu	k6eAd1
s	s	k7c7
dalšími	další	k1gNnPc7
napsala	napsat	k5eAaBmAgFnS,k5eAaPmAgFnS
jako	jako	k9
divadelní	divadelní	k2eAgInSc4d1
scénář	scénář	k1gInSc4
ke	k	k7c3
hře	hra	k1gFnSc3
Harry	Harra	k1gFnSc2
Potter	Pottra	k1gFnPc2
a	a	k8xC
Prokleté	prokletý	k2eAgNnSc4d1
dítě	dítě	k1gNnSc4
(	(	kIx(
<g/>
ve	v	k7c6
kterém	který	k3yIgNnSc6,k3yRgNnSc6,k3yQgNnSc6
však	však	k9
už	už	k9
Emma	Emma	k1gFnSc1
Watson	Watsona	k1gFnPc2
nehraje	hrát	k5eNaImIp3nS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
je	být	k5eAaImIp3nS
Hermiona	Hermiona	k1gFnSc1
Grangerová	Grangerový	k2eAgFnSc1d1
ministryní	ministryně	k1gFnSc7
kouzel	kouzlo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
2012	#num#	k4
<g/>
–	–	k?
<g/>
současnost	současnost	k1gFnSc4
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
se	se	k3xPyFc4
objevila	objevit	k5eAaPmAgFnS
ve	v	k7c6
filmové	filmový	k2eAgFnSc6d1
adaptaci	adaptace	k1gFnSc6
stejnojmenného	stejnojmenný	k2eAgInSc2d1
románu	román	k1gInSc2
Charlieho	Charlie	k1gMnSc2
malá	malý	k2eAgNnPc4d1
tajemství	tajemství	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
si	se	k3xPyFc3
zahrála	zahrát	k5eAaPmAgFnS
po	po	k7c6
boku	bok	k1gInSc6
Setha	Seth	k1gMnSc2
Rogena	Rogen	k1gMnSc2
a	a	k8xC
Jamese	Jamese	k1gFnSc2
Franca	Franca	k?
ve	v	k7c6
filmu	film	k1gInSc6
Apokalypsa	apokalypsa	k1gFnSc1
v	v	k7c6
Hollywoodu	Hollywood	k1gInSc6
a	a	k8xC
ve	v	k7c6
filmu	film	k1gInSc6
natočeného	natočený	k2eAgInSc2d1
podle	podle	k7c2
skutečné	skutečný	k2eAgFnSc2d1
události	událost	k1gFnSc2
Bling	Bling	k1gMnSc1
Ring	ring	k1gInSc1
<g/>
:	:	kIx,
Jako	jako	k8xS,k8xC
VIPky	VIPka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
natočila	natočit	k5eAaBmAgFnS
film	film	k1gInSc4
Noe	Noe	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
byla	být	k5eAaImAgFnS
zvažována	zvažovat	k5eAaImNgFnS
do	do	k7c2
role	role	k1gFnSc2
Popelky	Popelka	k1gFnSc2
ve	v	k7c6
stejnojmenném	stejnojmenný	k2eAgInSc6d1
filmu	film	k1gInSc6
<g/>
,	,	kIx,
roli	role	k1gFnSc4
nakonec	nakonec	k6eAd1
získala	získat	k5eAaPmAgFnS
Lily	lít	k5eAaImAgFnP
James	James	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
stejném	stejný	k2eAgInSc6d1
roce	rok	k1gInSc6
byla	být	k5eAaImAgFnS
jmenovaná	jmenovaná	k1gFnSc1
magazínem	magazín	k1gInSc7
GQ	GQ	kA
Ženou	žena	k1gFnSc7
roku	rok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2015	#num#	k4
si	se	k3xPyFc3
zahrála	zahrát	k5eAaPmAgFnS
ve	v	k7c6
thrillerech	thriller	k1gInPc6
Kolonie	kolonie	k1gFnSc1
a	a	k8xC
Regresión	Regresión	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oba	dva	k4xCgInPc1
filmy	film	k1gInPc1
získaly	získat	k5eAaPmAgInP
převážně	převážně	k6eAd1
negativní	negativní	k2eAgFnSc2d1
reakce	reakce	k1gFnSc2
od	od	k7c2
kritiků	kritik	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
získala	získat	k5eAaPmAgFnS
roli	role	k1gFnSc4
Krásky	kráska	k1gFnSc2
ve	v	k7c6
filmu	film	k1gInSc6
Kráska	kráska	k1gFnSc1
a	a	k8xC
zvíře	zvíře	k1gNnSc1
<g/>
,	,	kIx,
po	po	k7c6
boku	bok	k1gInSc6
Dana	Dana	k1gFnSc1
Stevense	Stevense	k1gFnSc2
jako	jako	k8xC,k8xS
zvířete	zvíře	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Film	film	k1gInSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
druhým	druhý	k4xOgInSc7
nejvýdělečnějším	výdělečný	k2eAgInSc7d3
filmem	film	k1gInSc7
roku	rok	k1gInSc2
2017	#num#	k4
s	s	k7c7
výdělkem	výdělek	k1gInSc7
přes	přes	k7c4
1,2	1,2	k4
miliardy	miliarda	k4xCgFnSc2
dolarů	dolar	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
čtrnáctým	čtrnáctý	k4xOgInSc7
nejvýdělečnějším	výdělečný	k2eAgInSc7d3
filmem	film	k1gInSc7
všech	všecek	k3xTgFnPc2
dob	doba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
stejném	stejný	k2eAgInSc6d1
roce	rok	k1gInSc6
měl	mít	k5eAaImAgInS
také	také	k9
premiéru	premiéra	k1gFnSc4
film	film	k1gInSc4
The	The	k1gFnSc2
Circle	Circle	k1gFnSc2
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yIgInSc6,k3yQgInSc6,k3yRgInSc6
hrála	hrát	k5eAaImAgFnS
po	po	k7c6
boku	bok	k1gInSc6
Toma	Tom	k1gMnSc2
Hankse	Hanks	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
srpnu	srpen	k1gInSc6
2018	#num#	k4
bylo	být	k5eAaImAgNnS
oznámeno	oznámit	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
získala	získat	k5eAaPmAgFnS
roli	role	k1gFnSc4
Meg	Meg	k1gFnSc1
March	Marcha	k1gFnPc2
ve	v	k7c6
filmové	filmový	k2eAgFnSc6d1
adaptaci	adaptace	k1gFnSc6
Grety	Greta	k1gFnSc2
Gerwigové	Gerwigový	k2eAgFnSc2d1
Malé	Malé	k2eAgFnSc2d1
ženy	žena	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Modeling	Modeling	k1gInSc1
</s>
<s>
S	s	k7c7
rostoucím	rostoucí	k2eAgInSc7d1
věkem	věk	k1gInSc7
se	se	k3xPyFc4
Emma	Emma	k1gFnSc1
Watsonová	Watsonová	k1gFnSc1
začala	začít	k5eAaPmAgFnS
čím	co	k3yQnSc7,k3yRnSc7,k3yInSc7
dál	daleko	k6eAd2
tím	ten	k3xDgNnSc7
více	hodně	k6eAd2
zajímat	zajímat	k5eAaImF
o	o	k7c4
módu	móda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prohlásila	prohlásit	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
módu	móda	k1gFnSc4
považuje	považovat	k5eAaImIp3nS
za	za	k7c4
velmi	velmi	k6eAd1
podobnou	podobný	k2eAgFnSc4d1
umění	umění	k1gNnSc3
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
studovala	studovat	k5eAaImAgFnS
ve	v	k7c6
škole	škola	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
britský	britský	k2eAgInSc1d1
tisk	tisk	k1gInSc1
uvedl	uvést	k5eAaPmAgInS
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
měla	mít	k5eAaImAgFnS
nahradit	nahradit	k5eAaPmF
tvář	tvář	k1gFnSc4
Keiry	Keira	k1gFnSc2
Knightleyové	Knightleyové	k2eAgFnSc2d1
pro	pro	k7c4
módní	módní	k2eAgFnSc4d1
značku	značka	k1gFnSc4
Chanel	Chanela	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
informace	informace	k1gFnSc1
však	však	k9
byla	být	k5eAaImAgFnS
zcela	zcela	k6eAd1
popřena	popřen	k2eAgFnSc1d1
oběma	dva	k4xCgFnPc7
stranami	strana	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
červnu	červen	k1gInSc6
2009	#num#	k4
potvrdila	potvrdit	k5eAaPmAgFnS
Watsonová	Watsonová	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
stane	stanout	k5eAaPmIp3nS
partnerkou	partnerka	k1gFnSc7
a	a	k8xC
tváří	tvář	k1gFnSc7
nové	nový	k2eAgFnSc2d1
kampaně	kampaň	k1gFnSc2
společnosti	společnost	k1gFnSc2
Burberry	Burberra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
se	se	k3xPyFc4
objevila	objevit	k5eAaPmAgFnS
také	také	k9
na	na	k7c6
módní	módní	k2eAgFnSc6d1
přehlídce	přehlídka	k1gFnSc6
Burberry	Burberra	k1gMnSc2
2010	#num#	k4
Jaro	jaro	k1gNnSc1
<g/>
/	/	kIx~
<g/>
Léto	léto	k1gNnSc1
po	po	k7c6
boku	bok	k1gInSc6
svého	svůj	k3xOyFgMnSc2
bratra	bratr	k1gMnSc2
Alexe	Alex	k1gMnSc5
<g/>
,	,	kIx,
muzikantů	muzikant	k1gMnPc2
George	Georg	k1gMnSc2
Craiga	Craig	k1gMnSc2
<g/>
,	,	kIx,
Matta	Matt	k1gMnSc2
Gilmoura	Gilmour	k1gMnSc2
a	a	k8xC
Maxe	Max	k1gMnSc2
Hurda	hurda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Filmografie	filmografie	k1gFnSc1
</s>
<s>
RokNázevRolePoznámky	RokNázevRolePoznámka	k1gFnPc1
</s>
<s>
2001	#num#	k4
<g/>
Harry	Harro	k1gNnPc7
Potter	Pottra	k1gFnPc2
a	a	k8xC
Kámen	kámen	k1gInSc1
mudrcůHermiona	mudrcůHermion	k1gMnSc2
Grangerová	Grangerová	k1gFnSc1
</s>
<s>
2002	#num#	k4
<g/>
Harry	Harro	k1gNnPc7
Potter	Pottra	k1gFnPc2
a	a	k8xC
Tajemná	tajemný	k2eAgFnSc1d1
komnataHermiona	komnataHermiona	k1gFnSc1
Grangerová	Grangerová	k1gFnSc1
</s>
<s>
2004	#num#	k4
<g/>
Harry	Harro	k1gNnPc7
Potter	Pottra	k1gFnPc2
a	a	k8xC
vězeň	vězeň	k1gMnSc1
z	z	k7c2
AzkabanuHermiona	AzkabanuHermion	k1gMnSc2
Grangerová	Grangerový	k2eAgFnSc1d1
</s>
<s>
2005	#num#	k4
<g/>
Harry	Harro	k1gNnPc7
Potter	Pottra	k1gFnPc2
a	a	k8xC
Ohnivý	ohnivý	k2eAgInSc1d1
pohárHermiona	pohárHermiona	k1gFnSc1
Grangerová	Grangerová	k1gFnSc5
</s>
<s>
2007	#num#	k4
<g/>
Harry	Harro	k1gNnPc7
Potter	Pottra	k1gFnPc2
a	a	k8xC
Fénixův	fénixův	k2eAgInSc1d1
řádHermiona	řádHermion	k1gMnSc2
Grangerová	Grangerový	k2eAgNnPc4d1
</s>
<s>
Baletní	baletní	k2eAgFnSc1d1
střevíčkyPauline	střevíčkyPaulin	k1gInSc5
Fossiltelevizní	Fossiltelevizní	k2eAgFnSc2d1
film	film	k1gInSc1
</s>
<s>
2008	#num#	k4
<g/>
Příběh	příběh	k1gInSc1
o	o	k7c4
ZoufálkoviPrincezna	ZoufálkoviPrincezna	k1gFnSc1
Hráška	Hráška	k1gFnSc1
(	(	kIx(
<g/>
hlas	hlas	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
2009	#num#	k4
<g/>
Harry	Harro	k1gNnPc7
Potter	Pottra	k1gFnPc2
a	a	k8xC
Princ	princa	k1gFnPc2
dvojí	dvojit	k5eAaImIp3nS
krveHermiona	krveHermiona	k1gFnSc1
Grangerová	Grangerový	k2eAgFnSc1d1
</s>
<s>
2010	#num#	k4
<g/>
Harry	Harro	k1gNnPc7
Potter	Pottra	k1gFnPc2
a	a	k8xC
Relikvie	relikvie	k1gFnSc2
smrti	smrt	k1gFnSc2
–	–	k?
část	část	k1gFnSc1
1	#num#	k4
<g/>
Hermiona	Hermiona	k1gFnSc1
Grangerová	Grangerová	k1gFnSc1
</s>
<s>
2011	#num#	k4
<g/>
Můj	můj	k1gMnSc1
týden	týden	k1gInSc4
s	s	k7c7
MarilynLucy	MarilynLucy	k1gInPc7
</s>
<s>
Harry	Harr	k1gInPc1
Potter	Pottra	k1gFnPc2
a	a	k8xC
Relikvie	relikvie	k1gFnSc2
smrti	smrt	k1gFnSc2
–	–	k?
část	část	k1gFnSc1
2	#num#	k4
<g/>
Hermiona	Hermiona	k1gFnSc1
Grangerová	Grangerová	k1gFnSc1
</s>
<s>
2012	#num#	k4
<g/>
Charlieho	Charlie	k1gMnSc2
malá	malý	k2eAgNnPc4d1
tajemství	tajemství	k1gNnSc2
</s>
<s>
Sam	Sam	k1gMnSc1
</s>
<s>
2013	#num#	k4
</s>
<s>
Apokalypsa	apokalypsa	k1gFnSc1
v	v	k7c6
Hollywoodu	Hollywood	k1gInSc6
</s>
<s>
Emma	Emma	k1gFnSc1
Watson	Watsona	k1gFnPc2
</s>
<s>
Bling	Bling	k1gInSc1
Ring	ring	k1gInSc1
<g/>
:	:	kIx,
Jako	jako	k8xS,k8xC
VIPky	VIPk	k1gInPc1
</s>
<s>
Nicki	Nick	k1gMnSc5
Moore	Moor	k1gMnSc5
</s>
<s>
2014	#num#	k4
</s>
<s>
Noe	Noe	k1gMnSc1
</s>
<s>
Ila	Ila	k?
</s>
<s>
2015	#num#	k4
</s>
<s>
Regresión	Regresión	k1gMnSc1
</s>
<s>
Angela	Angela	k1gFnSc1
Gray	Graa	k1gFnSc2
</s>
<s>
Kolonie	kolonie	k1gFnSc1
</s>
<s>
Lena	Lena	k1gFnSc1
</s>
<s>
The	The	k?
Vicar	Vicar	k1gInSc1
of	of	k?
Dibley	Diblea	k1gFnSc2
</s>
<s>
Reverend	reverend	k1gMnSc1
Iris	iris	k1gFnSc2
</s>
<s>
televizní	televizní	k2eAgInSc1d1
program	program	k1gInSc1
</s>
<s>
2017	#num#	k4
</s>
<s>
The	The	k?
Circle	Circle	k1gInSc1
</s>
<s>
Mae	Mae	k?
Holland	Holland	k1gInSc1
</s>
<s>
Kráska	kráska	k1gFnSc1
a	a	k8xC
zvíře	zvíře	k1gNnSc1
</s>
<s>
Belle	bell	k1gInSc5
</s>
<s>
2019	#num#	k4
</s>
<s>
Malé	Malé	k2eAgFnPc1d1
ženy	žena	k1gFnPc1
</s>
<s>
Meg	Meg	k?
March	March	k1gInSc1
</s>
<s>
Citáty	citát	k1gInPc1
</s>
<s>
„	„	k?
<g/>
Empatie	empatie	k1gFnSc1
a	a	k8xC
schopnost	schopnost	k1gFnSc1
používat	používat	k5eAaImF
vaši	váš	k3xOp2gFnSc4
představivost	představivost	k1gFnSc4
by	by	kYmCp3nS
neměla	mít	k5eNaImAgFnS
mít	mít	k5eAaImF
žádné	žádný	k3yNgFnPc4
hranice	hranice	k1gFnPc4
<g/>
.	.	kIx.
<g/>
“	“	k?
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
používá	používat	k5eAaImIp3nS
v	v	k7c6
citacích	citace	k1gFnPc6
zdrojů	zdroj	k1gInPc2
prosté	prostý	k2eAgFnSc2d1
internetové	internetový	k2eAgFnSc2d1
adresy	adresa	k1gFnSc2
<g/>
,	,	kIx,
u	u	k7c2
nichž	jenž	k3xRgInPc2
může	moct	k5eAaImIp3nS
hrozit	hrozit	k5eAaImF
znefunkčnění	znefunkčnění	k1gNnSc4
odkazu	odkaz	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Přidejte	přidat	k5eAaPmRp2nP
k	k	k7c3
odkazům	odkaz	k1gInPc3
doplňující	doplňující	k2eAgFnSc2d1
informace	informace	k1gFnSc2
<g/>
,	,	kIx,
nejlépe	dobře	k6eAd3
pomocí	pomocí	k7c2
citačních	citační	k2eAgFnPc2d1
šablon	šablona	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomůžete	pomoct	k5eAaPmIp2nP
tím	ten	k3xDgNnSc7
udržet	udržet	k5eAaPmF
ověřitelnost	ověřitelnost	k1gFnSc4
článku	článek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Blesk	blesk	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
:	:	kIx,
Herečka	hereček	k1gMnSc2
Emma	Emma	k1gFnSc1
Watson	Watson	k1gMnSc1
<g/>
:	:	kIx,
Tom	Tom	k1gMnSc1
Felton	Felton	k1gInSc1
mi	já	k3xPp1nSc3
zlomil	zlomit	k5eAaPmAgInS
srdce	srdce	k1gNnSc4
<g/>
↑	↑	k?
Novinky	novinka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
:	:	kIx,
Emma	Emma	k1gFnSc1
Watsonová	Watsonová	k1gFnSc1
<g/>
:	:	kIx,
Od	od	k7c2
Hermiony	Hermion	k1gInPc4
k	k	k7c3
nejoblíbenější	oblíbený	k2eAgFnSc3d3
britské	britský	k2eAgFnSc3d1
herečce	herečka	k1gFnSc3
<g/>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
9	#num#	k4
Harry	Harr	k1gInPc7
Potter	Potter	k1gMnSc1
a	a	k8xC
Princ	princ	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
dvojí	dvojit	k5eAaImIp3nS
krve	krev	k1gFnSc2
/	/	kIx~
Harry	Harra	k1gMnSc2
Potter	Potter	k1gInSc1
and	and	k?
the	the	k?
Half-Blood	Half-Blood	k1gInSc1
Prince	princ	k1gMnSc2
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
|	|	kIx~
Zajímavosti	zajímavost	k1gFnPc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
Dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
http://aktualne.centrum.cz/kultura/film/recenze/clanek.phtml?id=642581	http://aktualne.centrum.cz/kultura/film/recenze/clanek.phtml?id=642581	k4
<g/>
↑	↑	k?
http://alenaprokopova.blogspot.com/2009/07/harry-potr.html	http://alenaprokopova.blogspot.com/2009/07/harry-potr.html	k1gMnSc1
<g/>
↑	↑	k?
<g />
.	.	kIx.
</s>
<s hack="1">
http://hanus.blog.idnes.cz/c/96335/Recenze-Harry-Potter-a-hormony-utoci.html	http://hanus.blog.idnes.cz/c/96335/Recenze-Harry-Potter-a-hormony-utoci.html	k1gInSc1
<g/>
↑	↑	k?
http://www.rozhlas.cz/kultura/film/_zprava/623706	http://www.rozhlas.cz/kultura/film/_zprava/623706	k4
<g/>
↑	↑	k?
Deník	deník	k1gInSc1
Metro	metro	k1gNnSc1
<g/>
,	,	kIx,
16	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
12	#num#	k4
<g/>
–	–	k?
<g/>
13	#num#	k4
<g/>
↑	↑	k?
http://25fps.cz/2010/prvni-relikvie-a-beznadej-nadeje-a-smrt/	http://25fps.cz/2010/prvni-relikvie-a-beznadej-nadeje-a-smrt/	k4
<g/>
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
cinemo	cinemo	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
http://alenaprokopova.blogspot.com/2010/11/harry-potter-6-12.html	http://alenaprokopova.blogspot.com/2010/11/harry-potter-6-12.html	k1gInSc1
<g/>
↑	↑	k?
http://aktualne.centrum.cz/kultura/film/recenze/clanek.phtml?id=707187	http://aktualne.centrum.cz/kultura/film/recenze/clanek.phtml?id=707187	k4
<g/>
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.bestfilm.cz	www.bestfilm.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Emma	Emma	k1gFnSc1
Watsonová	Watsonový	k2eAgFnSc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Osoba	osoba	k1gFnSc1
Emma	Emma	k1gFnSc1
Watsonová	Watsonová	k1gFnSc1
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Emma	Emma	k1gFnSc1
Watsonová	Watsonový	k2eAgFnSc1d1
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
pna	pnout	k5eAaImSgInS
<g/>
2005261953	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
131379097	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
1445	#num#	k4
3711	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
2002045826	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
59289665	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
2002045826	#num#	k4
</s>
