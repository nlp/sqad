<s>
Oldřich	Oldřich	k1gMnSc1	Oldřich
Homuta	Homut	k1gMnSc2	Homut
byl	být	k5eAaImAgInS	být
český	český	k2eAgMnSc1d1	český
elektrotechnik	elektrotechnik	k1gMnSc1	elektrotechnik
<g/>
,	,	kIx,	,
vynálezce	vynálezce	k1gMnSc1	vynálezce
remosky	remoska	k1gFnSc2	remoska
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
menšími	malý	k2eAgFnPc7d2	menší
modifikacemi	modifikace	k1gFnPc7	modifikace
používána	používat	k5eAaImNgFnS	používat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Homuta	Homut	k2eAgFnSc1d1	Homut
před	před	k7c7	před
druhou	druhý	k4xOgFnSc7	druhý
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
vlastnil	vlastnit	k5eAaImAgInS	vlastnit
malou	malý	k2eAgFnSc4d1	malá
firmu	firma	k1gFnSc4	firma
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
elektromotorů	elektromotor	k1gInPc2	elektromotor
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
znárodnění	znárodnění	k1gNnSc6	znárodnění
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
firmy	firma	k1gFnSc2	firma
Remos	Remos	k1gInSc1	Remos
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
se	se	k3xPyFc4	se
Oldřich	Oldřich	k1gMnSc1	Oldřich
Homuta	Homut	k1gMnSc4	Homut
stal	stát	k5eAaPmAgMnS	stát
pracovníkem	pracovník	k1gMnSc7	pracovník
této	tento	k3xDgFnSc2	tento
firmy	firma	k1gFnSc2	firma
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
návštěvy	návštěva	k1gFnSc2	návštěva
Švédska	Švédsko	k1gNnSc2	Švédsko
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	let	k1gInPc6	let
si	se	k3xPyFc3	se
v	v	k7c6	v
obchodech	obchod	k1gInPc6	obchod
všiml	všimnout	k5eAaPmAgMnS	všimnout
elektrického	elektrický	k2eAgInSc2d1	elektrický
hrnce	hrnec	k1gInSc2	hrnec
<g/>
.	.	kIx.	.
</s>
<s>
Překvapilo	překvapit	k5eAaPmAgNnS	překvapit
ho	on	k3xPp3gNnSc4	on
ovšem	ovšem	k9	ovšem
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
hrnci	hrnec	k1gInSc6	hrnec
lze	lze	k6eAd1	lze
jen	jen	k9	jen
vařit	vařit	k5eAaImF	vařit
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
péct	péct	k5eAaImF	péct
<g/>
.	.	kIx.	.
</s>
<s>
Ihned	ihned	k6eAd1	ihned
ho	on	k3xPp3gMnSc4	on
napadlo	napadnout	k5eAaPmAgNnS	napadnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
takový	takový	k3xDgInSc4	takový
elektrický	elektrický	k2eAgInSc4d1	elektrický
hrnec	hrnec	k1gInSc4	hrnec
šlo	jít	k5eAaImAgNnS	jít
vyrobit	vyrobit	k5eAaPmF	vyrobit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
Československa	Československo	k1gNnSc2	Československo
se	se	k3xPyFc4	se
s	s	k7c7	s
nápadem	nápad	k1gInSc7	nápad
svěřil	svěřit	k5eAaPmAgMnS	svěřit
kolegům	kolega	k1gMnPc3	kolega
<g/>
,	,	kIx,	,
elektrotechnikům	elektrotechnik	k1gMnPc3	elektrotechnik
Jindřichu	Jindřich	k1gMnSc3	Jindřich
Uherovi	Uher	k1gMnSc3	Uher
a	a	k8xC	a
Antonínu	Antonín	k1gMnSc3	Antonín
Tyburcovi	Tyburce	k1gMnSc3	Tyburce
<g/>
,	,	kIx,	,
a	a	k8xC	a
společně	společně	k6eAd1	společně
začali	začít	k5eAaPmAgMnP	začít
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
prototypu	prototyp	k1gInSc6	prototyp
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
prototyp	prototyp	k1gInSc1	prototyp
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
mu	on	k3xPp3gMnSc3	on
říkali	říkat	k5eAaImAgMnP	říkat
HUT	HUT	kA	HUT
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
Homuta	Homut	k2eAgFnSc1d1	Homut
<g/>
,	,	kIx,	,
Uher	Uher	k1gMnSc1	Uher
<g/>
,	,	kIx,	,
Tyburec	Tyburec	k1gMnSc1	Tyburec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vynález	vynález	k1gInSc1	vynález
však	však	k9	však
v	v	k7c6	v
Remosu	Remos	k1gInSc6	Remos
nevzbudil	vzbudit	k5eNaPmAgMnS	vzbudit
valný	valný	k2eAgInSc4d1	valný
zájem	zájem	k1gInSc4	zájem
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
roku	rok	k1gInSc2	rok
1964	[number]	k4	1964
byl	být	k5eAaImAgInS	být
vynález	vynález	k1gInSc1	vynález
patentován	patentovat	k5eAaBmNgInS	patentovat
a	a	k8xC	a
výroba	výroba	k1gFnSc1	výroba
se	se	k3xPyFc4	se
rozjela	rozjet	k5eAaPmAgFnS	rozjet
na	na	k7c4	na
plno	plno	k1gNnSc4	plno
<g/>
.	.	kIx.	.
</s>
<s>
Nazván	nazván	k2eAgInSc1d1	nazván
byl	být	k5eAaImAgInS	být
dle	dle	k7c2	dle
výrobního	výrobní	k2eAgInSc2d1	výrobní
podniku	podnik	k1gInSc2	podnik
-	-	kIx~	-
remoska	remoska	k1gFnSc1	remoska
<g/>
.	.	kIx.	.
</s>
