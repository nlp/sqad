<s>
Jako	jako	k9	jako
výhodnější	výhodný	k2eAgMnSc1d2	výhodnější
se	se	k3xPyFc4	se
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
instrukční	instrukční	k2eAgFnPc1d1	instrukční
sady	sada	k1gFnPc1	sada
typu	typ	k1gInSc2	typ
RISC	RISC	kA	RISC
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
některé	některý	k3yIgFnSc2	některý
architektury	architektura	k1gFnSc2	architektura
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
zachování	zachování	k1gNnSc4	zachování
zpětné	zpětný	k2eAgFnSc2d1	zpětná
kompatibility	kompatibilita	k1gFnSc2	kompatibilita
pracují	pracovat	k5eAaImIp3nP	pracovat
i	i	k9	i
se	s	k7c7	s
strojovým	strojový	k2eAgInSc7d1	strojový
kódem	kód	k1gInSc7	kód
typu	typ	k1gInSc2	typ
CISC	CISC	kA	CISC
(	(	kIx(	(
<g/>
Intel	Intel	kA	Intel
x	x	k?	x
<g/>
86	[number]	k4	86
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
