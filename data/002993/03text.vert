<s>
Gargamel	Gargamel	k1gMnSc1	Gargamel
je	být	k5eAaImIp3nS	být
zlý	zlý	k2eAgMnSc1d1	zlý
čaroděj	čaroděj	k1gMnSc1	čaroděj
v	v	k7c6	v
pohádkovém	pohádkový	k2eAgInSc6d1	pohádkový
světě	svět	k1gInSc6	svět
šmoulů	šmoula	k1gMnPc2	šmoula
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zapřisáhlý	zapřisáhlý	k2eAgMnSc1d1	zapřisáhlý
nepřítel	nepřítel	k1gMnSc1	nepřítel
šmoulů	šmoula	k1gMnPc2	šmoula
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
je	on	k3xPp3gMnPc4	on
snaží	snažit	k5eAaImIp3nP	snažit
ulovit	ulovit	k5eAaPmF	ulovit
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgFnSc7d1	hlavní
zápornou	záporný	k2eAgFnSc7d1	záporná
postavou	postava	k1gFnSc7	postava
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
příběhu	příběh	k1gInSc6	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Gargamel	Gargamel	k1gInSc1	Gargamel
je	být	k5eAaImIp3nS	být
střední	střední	k2eAgFnPc4d1	střední
<g/>
,	,	kIx,	,
shrbené	shrbený	k2eAgFnPc4d1	shrbená
postavy	postava	k1gFnPc4	postava
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
špatný	špatný	k2eAgInSc4d1	špatný
chrup	chrup	k1gInSc4	chrup
<g/>
.	.	kIx.	.
</s>
<s>
Nosí	nosit	k5eAaImIp3nS	nosit
tmavý	tmavý	k2eAgInSc4d1	tmavý
obnošený	obnošený	k2eAgInSc4d1	obnošený
kabát	kabát	k1gInSc4	kabát
se	s	k7c7	s
spoustou	spousta	k1gFnSc7	spousta
záplat	záplata	k1gFnPc2	záplata
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
mizera	mizera	k1gMnSc1	mizera
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
nesnáší	snášet	k5eNaImIp3nS	snášet
všechny	všechen	k3xTgFnPc4	všechen
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
okolí	okolí	k1gNnSc6	okolí
<g/>
,	,	kIx,	,
speciálně	speciálně	k6eAd1	speciálně
šmouly	šmoula	k1gMnPc4	šmoula
<g/>
,	,	kIx,	,
až	až	k9	až
na	na	k7c4	na
jednu	jeden	k4xCgFnSc4	jeden
výjimku	výjimka	k1gFnSc4	výjimka
a	a	k8xC	a
tou	ten	k3xDgFnSc7	ten
je	být	k5eAaImIp3nS	být
Šmoulinka	Šmoulinka	k1gFnSc1	Šmoulinka
-	-	kIx~	-
poslední	poslední	k2eAgFnSc1d1	poslední
ze	z	k7c2	z
šmoulů	šmoula	k1gMnPc2	šmoula
<g/>
,	,	kIx,	,
vytvořená	vytvořený	k2eAgFnSc1d1	vytvořená
Gargamelem	Gargamel	k1gInSc7	Gargamel
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
svoji	svůj	k3xOyFgFnSc4	svůj
nenávist	nenávist	k1gFnSc4	nenávist
vůči	vůči	k7c3	vůči
všem	všecek	k3xTgMnPc3	všecek
občas	občas	k6eAd1	občas
předstírá	předstírat	k5eAaImIp3nS	předstírat
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
vlastním	vlastní	k2eAgInSc6d1	vlastní
zájmu	zájem	k1gInSc6	zájem
přátelství	přátelství	k1gNnSc1	přátelství
ke	k	k7c3	k
Šmoulům	šmoula	k1gMnPc3	šmoula
<g/>
.	.	kIx.	.
</s>
<s>
Gargamel	Gargamel	k1gInSc1	Gargamel
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgInSc1d1	schopen
skoro	skoro	k6eAd1	skoro
všeho	všecek	k3xTgNnSc2	všecek
proto	proto	k6eAd1	proto
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgInS	moct
Šmouly	šmoula	k1gMnPc4	šmoula
pochytat	pochytat	k5eAaPmF	pochytat
<g/>
.	.	kIx.	.
</s>
<s>
Pojmenován	pojmenován	k2eAgMnSc1d1	pojmenován
je	být	k5eAaImIp3nS	být
zjevně	zjevně	k6eAd1	zjevně
po	po	k7c6	po
matce	matka	k1gFnSc6	matka
Gargantua	Gargantua	k1gFnSc1	Gargantua
-	-	kIx~	-
Gargamelle	Gargamelle	k1gInSc1	Gargamelle
<g/>
,	,	kIx,	,
ze	z	k7c2	z
série	série	k1gFnSc2	série
románu	román	k1gInSc2	román
Gargantua	Gargantuum	k1gNnSc2	Gargantuum
a	a	k8xC	a
Pantagruel	Pantagruela	k1gFnPc2	Pantagruela
od	od	k7c2	od
Françoise	Françoise	k1gFnSc2	Françoise
Rabelaise	Rabelaise	k1gFnSc2	Rabelaise
<g/>
.	.	kIx.	.
</s>
<s>
Gargamel	Gargamel	k1gMnSc1	Gargamel
je	být	k5eAaImIp3nS	být
packal	packal	k1gMnSc1	packal
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
z	z	k7c2	z
jeho	jeho	k3xOp3gMnPc2	jeho
nápadů	nápad	k1gInPc2	nápad
jak	jak	k6eAd1	jak
zničit	zničit	k5eAaPmF	zničit
šmouly	šmoula	k1gMnPc4	šmoula
je	být	k5eAaImIp3nS	být
hloupá	hloupý	k2eAgFnSc1d1	hloupá
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
výjimečně	výjimečně	k6eAd1	výjimečně
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
chvíle	chvíle	k1gFnSc1	chvíle
geniální	geniální	k2eAgFnSc1d1	geniální
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jeho	jeho	k3xOp3gInPc4	jeho
plány	plán	k1gInPc4	plán
na	na	k7c4	na
odchycení	odchycení	k1gNnSc4	odchycení
šmoulů	šmoula	k1gMnPc2	šmoula
skoro	skoro	k6eAd1	skoro
nemají	mít	k5eNaImIp3nP	mít
chybu	chyba	k1gFnSc4	chyba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stejnak	stejnak	k?	stejnak
nakonec	nakonec	k6eAd1	nakonec
zmrví	zmrvit	k5eAaPmIp3nS	zmrvit
úplně	úplně	k6eAd1	úplně
všechno	všechen	k3xTgNnSc4	všechen
<g/>
.	.	kIx.	.
</s>
<s>
Gargamel	Gargamel	k1gMnSc1	Gargamel
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
Komiksu	komiks	k1gInSc6	komiks
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
<g/>
.	.	kIx.	.
</s>
<s>
Chytal	chytat	k5eAaImAgMnS	chytat
šmouly	šmoula	k1gMnPc4	šmoula
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
potřeboval	potřebovat	k5eAaImAgMnS	potřebovat
jako	jako	k8xS	jako
ingredienci	ingredience	k1gFnSc4	ingredience
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
lektvaru	lektvar	k1gInSc2	lektvar
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
zlata	zlato	k1gNnSc2	zlato
<g/>
,	,	kIx,	,
tak	tak	k9	tak
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
psalo	psát	k5eAaImAgNnS	psát
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
kouzelné	kouzelný	k2eAgFnSc6d1	kouzelná
legendě	legenda	k1gFnSc6	legenda
o	o	k7c6	o
Kameni	kámen	k1gInSc6	kámen
moudrosti	moudrost	k1gFnSc2	moudrost
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgMnPc1d1	ostatní
šmoulové	šmoula	k1gMnPc1	šmoula
se	se	k3xPyFc4	se
mu	on	k3xPp3gInSc3	on
ale	ale	k9	ale
postavili	postavit	k5eAaPmAgMnP	postavit
a	a	k8xC	a
osvobodili	osvobodit	k5eAaPmAgMnP	osvobodit
uvězněné	uvězněný	k2eAgMnPc4d1	uvězněný
přátele	přítel	k1gMnPc4	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Gargamel	Gargamel	k1gMnSc1	Gargamel
se	se	k3xPyFc4	se
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
se	s	k7c7	s
svojí	svůj	k3xOyFgFnSc7	svůj
povahou	povaha	k1gFnSc7	povaha
vzdal	vzdát	k5eAaPmAgMnS	vzdát
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
chvíle	chvíle	k1gFnSc1	chvíle
kdy	kdy	k6eAd1	kdy
zaujal	zaujmout	k5eAaPmAgMnS	zaujmout
vůči	vůči	k7c3	vůči
šmoulům	šmoula	k1gMnPc3	šmoula
maximálně	maximálně	k6eAd1	maximálně
nepřátelský	přátelský	k2eNgInSc4d1	nepřátelský
postoj	postoj	k1gInSc4	postoj
a	a	k8xC	a
slíbil	slíbit	k5eAaPmAgMnS	slíbit
si	se	k3xPyFc3	se
že	že	k8xS	že
se	se	k3xPyFc4	se
pomstí	pomstít	k5eAaPmIp3nS	pomstít
<g/>
.	.	kIx.	.
</s>
<s>
Použití	použití	k1gNnSc1	použití
šmoulů	šmoula	k1gMnPc2	šmoula
je	být	k5eAaImIp3nS	být
různé	různý	k2eAgNnSc1d1	různé
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
šmouly	šmoula	k1gMnPc4	šmoula
chytá	chytat	k5eAaImIp3nS	chytat
protože	protože	k8xS	protože
je	on	k3xPp3gNnPc4	on
chce	chtít	k5eAaImIp3nS	chtít
sníst	sníst	k5eAaPmF	sníst
<g/>
,	,	kIx,	,
občas	občas	k6eAd1	občas
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
chce	chtít	k5eAaImIp3nS	chtít
vyrobit	vyrobit	k5eAaPmF	vyrobit
zlato	zlato	k1gNnSc4	zlato
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
zlata	zlato	k1gNnSc2	zlato
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
6	[number]	k4	6
šmoulů	šmoula	k1gMnPc2	šmoula
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
epizodě	epizoda	k1gFnSc6	epizoda
seriálů	seriál	k1gInPc2	seriál
Gargamel	Gargamel	k1gInSc1	Gargamel
ve	v	k7c6	v
vzteku	vztek	k1gInSc6	vztek
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
:	:	kIx,	:
<g/>
"	"	kIx"	"
<g/>
Nechci	chtít	k5eNaImIp1nS	chtít
je	on	k3xPp3gInPc4	on
sníst	sníst	k5eAaPmF	sníst
<g/>
.	.	kIx.	.
</s>
<s>
Nechci	chtít	k5eNaImIp1nS	chtít
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
vyrobit	vyrobit	k5eAaPmF	vyrobit
zlato	zlato	k1gNnSc4	zlato
<g/>
.	.	kIx.	.
</s>
<s>
Chci	chtít	k5eAaImIp1nS	chtít
je	on	k3xPp3gMnPc4	on
prostě	prostě	k9	prostě
zničit	zničit	k5eAaPmF	zničit
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
Gargamel	Gargamlo	k1gNnPc2	Gargamlo
ale	ale	k8xC	ale
chytá	chytat	k5eAaImIp3nS	chytat
pouze	pouze	k6eAd1	pouze
šmouly	šmoula	k1gMnPc4	šmoula
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
potulují	potulovat	k5eAaImIp3nP	potulovat
sami	sám	k3xTgMnPc1	sám
v	v	k7c6	v
lese	les	k1gInSc6	les
a	a	k8xC	a
nebo	nebo	k8xC	nebo
v	v	k7c4	v
okolí	okolí	k1gNnSc4	okolí
jeho	on	k3xPp3gInSc2	on
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
neví	vědět	k5eNaImIp3nS	vědět
totiž	totiž	k9	totiž
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
vesnice	vesnice	k1gFnSc1	vesnice
šmoulů	šmoula	k1gMnPc2	šmoula
což	což	k3yRnSc1	což
ho	on	k3xPp3gInSc4	on
velmi	velmi	k6eAd1	velmi
zlobí	zlobit	k5eAaImIp3nP	zlobit
<g/>
.	.	kIx.	.
</s>
<s>
Čas	čas	k1gInSc1	čas
od	od	k7c2	od
času	čas	k1gInSc2	čas
Gargamel	Gargamel	k1gInSc1	Gargamel
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
s	s	k7c7	s
některými	některý	k3yIgMnPc7	některý
z	z	k7c2	z
dalších	další	k2eAgMnPc2d1	další
nepřátel	nepřítel	k1gMnPc2	nepřítel
šmoulů	šmoula	k1gMnPc2	šmoula
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
s	s	k7c7	s
Lordem	lord	k1gMnSc7	lord
Balthazarem	Balthazar	k1gMnSc7	Balthazar
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgMnPc1d1	ostatní
zloduši	zloduch	k1gMnPc1	zloduch
ovšem	ovšem	k9	ovšem
bývají	bývat	k5eAaImIp3nP	bývat
mnohem	mnohem	k6eAd1	mnohem
více	hodně	k6eAd2	hodně
pohotoví	pohotový	k2eAgMnPc1d1	pohotový
-	-	kIx~	-
uzavřou	uzavřít	k5eAaPmIp3nP	uzavřít
s	s	k7c7	s
Gargamelem	Gargamel	k1gInSc7	Gargamel
spojenectví	spojenectví	k1gNnSc2	spojenectví
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
jej	on	k3xPp3gNnSc4	on
v	v	k7c6	v
klidu	klid	k1gInSc6	klid
<g/>
,	,	kIx,	,
nenápadně	nápadně	k6eNd1	nápadně
a	a	k8xC	a
s	s	k7c7	s
přehledem	přehled	k1gInSc7	přehled
zotročí	zotročit	k5eAaPmIp3nS	zotročit
<g/>
.	.	kIx.	.
</s>
<s>
Žen	žena	k1gFnPc2	žena
v	v	k7c6	v
Gargamelově	Gargamelův	k2eAgInSc6d1	Gargamelův
životě	život	k1gInSc6	život
není	být	k5eNaImIp3nS	být
o	o	k7c4	o
mnoho	mnoho	k4c4	mnoho
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
přátel	přítel	k1gMnPc2	přítel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
epizodě	epizoda	k1gFnSc6	epizoda
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
ošklivá	ošklivý	k2eAgFnSc1d1	ošklivá
čarodějnice	čarodějnice	k1gFnSc1	čarodějnice
Hogatha	Hogatha	k1gFnSc1	Hogatha
přeměněná	přeměněný	k2eAgFnSc1d1	přeměněná
na	na	k7c4	na
krásnou	krásný	k2eAgFnSc4d1	krásná
ženu	žena	k1gFnSc4	žena
<g/>
.	.	kIx.	.
</s>
<s>
Hogatha	Hogatha	k1gFnSc1	Hogatha
se	se	k3xPyFc4	se
pokoušela	pokoušet	k5eAaImAgFnS	pokoušet
ukrást	ukrást	k5eAaPmF	ukrást
Gargamelovi	Gargamel	k1gMnSc3	Gargamel
náušnice	náušnice	k1gFnSc2	náušnice
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
koupil	koupit	k5eAaPmAgMnS	koupit
své	svůj	k3xOyFgFnSc3	svůj
matce	matka	k1gFnSc3	matka
k	k	k7c3	k
narozeninám	narozeniny	k1gFnPc3	narozeniny
a	a	k8xC	a
které	který	k3yIgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
kouzelnou	kouzelný	k2eAgFnSc4d1	kouzelná
moc	moc	k1gFnSc4	moc
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
neštěstí	neštěstí	k1gNnSc6	neštěstí
pro	pro	k7c4	pro
Hogathu	Hogatha	k1gFnSc4	Hogatha
ale	ale	k8xC	ale
bylo	být	k5eAaImAgNnS	být
velice	velice	k6eAd1	velice
těžké	těžký	k2eAgNnSc1d1	těžké
udržet	udržet	k5eAaPmF	udržet
si	se	k3xPyFc3	se
krásnou	krásný	k2eAgFnSc4d1	krásná
podobu	podoba	k1gFnSc4	podoba
a	a	k8xC	a
elixír	elixír	k1gInSc4	elixír
přestal	přestat	k5eAaPmAgInS	přestat
fungovat	fungovat	k5eAaImF	fungovat
-	-	kIx~	-
tím	ten	k3xDgNnSc7	ten
Gargamel	Gargamel	k1gInSc1	Gargamel
odhalil	odhalit	k5eAaPmAgInS	odhalit
podvod	podvod	k1gInSc4	podvod
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
si	se	k3xPyFc3	se
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
čarodějnice	čarodějnice	k1gFnSc1	čarodějnice
přichystala	přichystat	k5eAaPmAgFnS	přichystat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jiné	jiný	k2eAgFnSc6d1	jiná
epizodě	epizoda	k1gFnSc6	epizoda
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
Gargamel	Gargamel	k1gInSc1	Gargamel
dokonce	dokonce	k9	dokonce
i	i	k9	i
ženit	ženit	k5eAaImF	ženit
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
se	se	k3xPyFc4	se
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
že	že	k8xS	že
už	už	k6eAd1	už
je	být	k5eAaImIp3nS	být
ideální	ideální	k2eAgInSc4d1	ideální
čas	čas	k1gInSc4	čas
na	na	k7c4	na
manželství	manželství	k1gNnSc4	manželství
a	a	k8xC	a
domluvila	domluvit	k5eAaPmAgFnS	domluvit
Gargamelovi	Gargamelův	k2eAgMnPc1d1	Gargamelův
manželství	manželství	k1gNnSc1	manželství
s	s	k7c7	s
Princeznou	princezna	k1gFnSc7	princezna
<g/>
.	.	kIx.	.
</s>
<s>
Princezna	princezna	k1gFnSc1	princezna
Gargamela	Gargamela	k1gFnSc1	Gargamela
nemilovala	milovat	k5eNaImAgFnS	milovat
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
jako	jako	k8xS	jako
Gargamel	Gargamel	k1gMnSc1	Gargamel
nemiloval	milovat	k5eNaImAgMnS	milovat
princeznu	princezna	k1gFnSc4	princezna
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
po	po	k7c6	po
tom	ten	k3xDgNnSc6	ten
co	co	k9	co
v	v	k7c6	v
jejím	její	k3xOp3gInSc6	její
plášti	plášť	k1gInSc6	plášť
viděl	vidět	k5eAaImAgMnS	vidět
mapu	mapa	k1gFnSc4	mapa
se	s	k7c7	s
zakreslenou	zakreslený	k2eAgFnSc7d1	zakreslená
šmoulí	šmoule	k1gFnSc7	šmoule
vesnicí	vesnice	k1gFnSc7	vesnice
přislíbil	přislíbit	k5eAaPmAgInS	přislíbit
princezně	princezna	k1gFnSc3	princezna
svoji	svůj	k3xOyFgFnSc4	svůj
lásku	láska	k1gFnSc4	láska
i	i	k9	i
brzký	brzký	k2eAgInSc1d1	brzký
sňatek	sňatek	k1gInSc1	sňatek
<g/>
.	.	kIx.	.
</s>
<s>
Obřad	obřad	k1gInSc1	obřad
na	na	k7c4	na
štěstí	štěstí	k1gNnSc4	štěstí
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
Gargamela	Gargamel	k1gMnSc4	Gargamel
<g/>
,	,	kIx,	,
Princeznu	princezna	k1gFnSc4	princezna
i	i	k8xC	i
šmoulí	šmoulit	k5eAaPmIp3nS	šmoulit
vesnici	vesnice	k1gFnSc4	vesnice
šmoulové	šmoula	k1gMnPc1	šmoula
překazili	překazit	k5eAaPmAgMnP	překazit
<g/>
.	.	kIx.	.
</s>
<s>
Dabing	dabing	k1gInSc1	dabing
na	na	k7c6	na
českých	český	k2eAgFnPc6d1	Česká
TV	TV	kA	TV
stanicích	stanice	k1gFnPc6	stanice
Dabing	dabing	k1gInSc1	dabing
šmoulů	šmoula	k1gMnPc2	šmoula
v	v	k7c6	v
cizině	cizina	k1gFnSc6	cizina
</s>
