<s>
Z	z	k7c2	z
českých	český	k2eAgFnPc2d1	Česká
blackmetalových	blackmetalův	k2eAgFnPc2d1	blackmetalův
kapel	kapela	k1gFnPc2	kapela
můžeme	moct	k5eAaImIp1nP	moct
zmínit	zmínit	k5eAaPmF	zmínit
např.	např.	kA	např.
Master	master	k1gMnSc1	master
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Hammer	Hammer	k1gInSc1	Hammer
<g/>
,	,	kIx,	,
Root	Root	k1gInSc1	Root
(	(	kIx(	(
<g/>
druhá	druhý	k4xOgFnSc1	druhý
polovina	polovina	k1gFnSc1	polovina
osmdesátých	osmdesátý	k4xOgNnPc2	osmdesátý
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Törr	Törr	k1gMnSc1	Törr
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
však	však	k9	však
stále	stále	k6eAd1	stále
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
hrají	hrát	k5eAaImIp3nP	hrát
"	"	kIx"	"
<g/>
jen	jen	k9	jen
o	o	k7c4	o
něco	něco	k3yInSc4	něco
tvrdší	tvrdý	k2eAgInSc1d2	tvrdší
rock	rock	k1gInSc1	rock
<g/>
'	'	kIx"	'
<g/>
n	n	k0	n
<g/>
'	'	kIx"	'
<g/>
roll	rollum	k1gNnPc2	rollum
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
či	či	k8xC	či
již	již	k6eAd1	již
neexistující	existující	k2eNgFnSc4d1	neexistující
kapelu	kapela	k1gFnSc4	kapela
RITUAL	RITUAL	kA	RITUAL
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
potom	potom	k6eAd1	potom
Enochian	Enochian	k1gInSc4	Enochian
<g/>
,	,	kIx,	,
Maniac	Maniac	k1gInSc4	Maniac
Butcher	Butchra	k1gFnPc2	Butchra
<g/>
,	,	kIx,	,
Infernal	Infernal	k1gFnPc2	Infernal
Heretic	Heretice	k1gFnPc2	Heretice
nebo	nebo	k8xC	nebo
třeba	třeba	k6eAd1	třeba
Morrior	Morriora	k1gFnPc2	Morriora
<g/>
.	.	kIx.	.
</s>
