<p>
<s>
Glóbus	glóbus	k1gInSc1	glóbus
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
globus	globus	k1gInSc1	globus
<g/>
,	,	kIx,	,
koule	koule	k1gFnSc1	koule
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
kartografii	kartografie	k1gFnSc6	kartografie
zmenšené	zmenšený	k2eAgInPc4d1	zmenšený
prostorové	prostorový	k2eAgInPc4d1	prostorový
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
kulové	kulový	k2eAgFnPc1d1	kulová
<g/>
,	,	kIx,	,
znázornění	znázornění	k1gNnSc1	znázornění
určitého	určitý	k2eAgNnSc2d1	určité
vesmírného	vesmírný	k2eAgNnSc2d1	vesmírné
tělesa	těleso	k1gNnSc2	těleso
(	(	kIx(	(
<g/>
zemský	zemský	k2eAgInSc1d1	zemský
glóbus	glóbus	k1gInSc1	glóbus
<g/>
,	,	kIx,	,
měsíční	měsíční	k2eAgInSc1d1	měsíční
glóbus	glóbus	k1gInSc1	glóbus
<g/>
,	,	kIx,	,
glóbus	glóbus	k1gInSc1	glóbus
Marsu	Mars	k1gInSc2	Mars
<g/>
)	)	kIx)	)
pomocí	pomocí	k7c2	pomocí
kartografických	kartografický	k2eAgInPc2d1	kartografický
prostředků	prostředek	k1gInPc2	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
nejčastější	častý	k2eAgFnSc7d3	nejčastější
patří	patřit	k5eAaImIp3nP	patřit
glóby	glóbus	k1gInPc1	glóbus
zemského	zemský	k2eAgNnSc2d1	zemské
tělesa	těleso	k1gNnSc2	těleso
a	a	k8xC	a
nebeské	nebeský	k2eAgFnSc2d1	nebeská
sféry	sféra	k1gFnSc2	sféra
(	(	kIx(	(
<g/>
hvězdné	hvězdný	k2eAgInPc1d1	hvězdný
glóby	glóbus	k1gInPc1	glóbus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Glóby	glóbus	k1gInPc1	glóbus
řadíme	řadit	k5eAaImIp1nP	řadit
mezi	mezi	k7c4	mezi
"	"	kIx"	"
<g/>
skutečně	skutečně	k6eAd1	skutečně
trojrozměrná	trojrozměrný	k2eAgNnPc1d1	trojrozměrné
znázornění	znázornění	k1gNnPc1	znázornění
<g/>
"	"	kIx"	"
vyjadřující	vyjadřující	k2eAgInSc4d1	vyjadřující
třetí	třetí	k4xOgInSc4	třetí
rozměr	rozměr	k1gInSc4	rozměr
pomocí	pomocí	k7c2	pomocí
hmotných	hmotný	k2eAgInPc2d1	hmotný
prostředků	prostředek	k1gInPc2	prostředek
(	(	kIx(	(
<g/>
patří	patřit	k5eAaImIp3nS	patřit
sem	sem	k6eAd1	sem
i	i	k9	i
reliéfní	reliéfní	k2eAgFnPc1d1	reliéfní
mapy	mapa	k1gFnPc1	mapa
<g/>
,	,	kIx,	,
modely	model	k1gInPc1	model
reliéfu	reliéf	k1gInSc2	reliéf
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
glóby	glóbus	k1gInPc7	glóbus
lze	lze	k6eAd1	lze
zařadit	zařadit	k5eAaPmF	zařadit
i	i	k9	i
mnohostěnné	mnohostěnný	k2eAgInPc4d1	mnohostěnný
modely	model	k1gInPc4	model
(	(	kIx(	(
<g/>
nepravé	pravý	k2eNgInPc1d1	nepravý
glóby	glóbus	k1gInPc1	glóbus
<g/>
)	)	kIx)	)
a	a	k8xC	a
výřezy	výřez	k1gInPc1	výřez
(	(	kIx(	(
<g/>
vrchlíky	vrchlík	k1gInPc1	vrchlík
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
zobrazují	zobrazovat	k5eAaImIp3nP	zobrazovat
pouze	pouze	k6eAd1	pouze
určitou	určitý	k2eAgFnSc4d1	určitá
část	část	k1gFnSc4	část
zemského	zemský	k2eAgInSc2d1	zemský
povrchu	povrch	k1gInSc2	povrch
<g/>
,	,	kIx,	,
např.	např.	kA	např.
polární	polární	k2eAgFnPc4d1	polární
oblasti	oblast	k1gFnPc4	oblast
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zobrazení	zobrazení	k1gNnSc6	zobrazení
==	==	k?	==
</s>
</p>
<p>
<s>
Skutečný	skutečný	k2eAgInSc1d1	skutečný
tvar	tvar	k1gInSc1	tvar
Země	zem	k1gFnSc2	zem
(	(	kIx(	(
<g/>
geoid	geoid	k1gInSc1	geoid
<g/>
)	)	kIx)	)
bývá	bývat	k5eAaImIp3nS	bývat
u	u	k7c2	u
glóbu	glóbus	k1gInSc2	glóbus
nahrazen	nahradit	k5eAaPmNgMnS	nahradit
koulí	koule	k1gFnSc7	koule
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
ale	ale	k9	ale
i	i	k9	i
glóby	glóbus	k1gInPc1	glóbus
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
zachovávají	zachovávat	k5eAaImIp3nP	zachovávat
skutečný	skutečný	k2eAgInSc4d1	skutečný
tvar	tvar	k1gInSc4	tvar
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
dokonce	dokonce	k9	dokonce
glóby	glóbus	k1gInPc1	glóbus
plasticky	plasticky	k6eAd1	plasticky
reprezentující	reprezentující	k2eAgMnSc1d1	reprezentující
i	i	k8xC	i
zemský	zemský	k2eAgInSc1d1	zemský
reliéf	reliéf	k1gInSc1	reliéf
<g/>
.	.	kIx.	.
</s>
<s>
Plastické	plastický	k2eAgInPc1d1	plastický
glóby	glóbus	k1gInPc1	glóbus
mají	mít	k5eAaImIp3nP	mít
ale	ale	k8xC	ale
výrazně	výrazně	k6eAd1	výrazně
zkresleny	zkreslen	k2eAgFnPc4d1	zkreslena
relativní	relativní	k2eAgFnPc4d1	relativní
výšky	výška	k1gFnPc4	výška
terénu	terén	k1gInSc2	terén
v	v	k7c6	v
pohořích	pohoří	k1gNnPc6	pohoří
(	(	kIx(	(
<g/>
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
poměru	poměr	k1gInSc6	poměr
mnohem	mnohem	k6eAd1	mnohem
nižší	nízký	k2eAgFnSc1d2	nižší
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zobrazení	zobrazení	k1gNnSc1	zobrazení
Země	zem	k1gFnSc2	zem
na	na	k7c6	na
glóbu	glóbus	k1gInSc6	glóbus
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
realistické	realistický	k2eAgNnSc1d1	realistické
(	(	kIx(	(
<g/>
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
rovinnou	rovinný	k2eAgFnSc7d1	rovinná
mapou	mapa	k1gFnSc7	mapa
<g/>
)	)	kIx)	)
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
úhlojevné	úhlojevný	k2eAgInPc1d1	úhlojevný
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
zachovává	zachovávat	k5eAaImIp3nS	zachovávat
velikosti	velikost	k1gFnPc4	velikost
úhlů	úhel	k1gInPc2	úhel
<g/>
)	)	kIx)	)
i	i	k9	i
délkojevné	délkojevný	k2eAgInPc1d1	délkojevný
(	(	kIx(	(
<g/>
všechny	všechen	k3xTgFnPc4	všechen
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
skutečným	skutečný	k2eAgFnPc3d1	skutečná
vzdálenostem	vzdálenost	k1gFnPc3	vzdálenost
<g/>
,	,	kIx,	,
zmenšeným	zmenšený	k2eAgMnSc7d1	zmenšený
v	v	k7c6	v
daném	daný	k2eAgNnSc6d1	dané
měřítku	měřítko	k1gNnSc6	měřítko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
glóbu	glóbus	k1gInSc6	glóbus
tak	tak	k6eAd1	tak
lze	lze	k6eAd1	lze
měřit	měřit	k5eAaImF	měřit
délky	délka	k1gFnPc4	délka
v	v	k7c6	v
libovolném	libovolný	k2eAgInSc6d1	libovolný
směru	směr	k1gInSc6	směr
bez	bez	k7c2	bez
nebezpečí	nebezpečí	k1gNnSc2	nebezpečí
zkreslení	zkreslení	k1gNnSc2	zkreslení
<g/>
.	.	kIx.	.
</s>
<s>
Nejvhodnější	vhodný	k2eAgFnSc1d3	nejvhodnější
pro	pro	k7c4	pro
měření	měření	k1gNnSc4	měření
je	být	k5eAaImIp3nS	být
ohebné	ohebný	k2eAgNnSc4d1	ohebné
papírové	papírový	k2eAgNnSc4d1	papírové
pravítko	pravítko	k1gNnSc4	pravítko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Glóby	glóbus	k1gInPc1	glóbus
jsou	být	k5eAaImIp3nP	být
zpravidla	zpravidla	k6eAd1	zpravidla
zhotoveny	zhotovit	k5eAaPmNgInP	zhotovit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
otáčivé	otáčivý	k2eAgInPc1d1	otáčivý
kolem	kolem	k7c2	kolem
osy	osa	k1gFnSc2	osa
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
ose	osa	k1gFnSc3	osa
otáčení	otáčení	k1gNnSc2	otáčení
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
nich	on	k3xPp3gFnPc6	on
tudíž	tudíž	k8xC	tudíž
možné	možný	k2eAgNnSc1d1	možné
demonstrovat	demonstrovat	k5eAaBmF	demonstrovat
otáčení	otáčení	k1gNnSc4	otáčení
Země	zem	k1gFnSc2	zem
kolem	kolem	k7c2	kolem
její	její	k3xOp3gFnSc2	její
osy	osa	k1gFnSc2	osa
<g/>
.	.	kIx.	.
</s>
<s>
Staré	Staré	k2eAgInPc1d1	Staré
globy	globus	k1gInPc1	globus
jsou	být	k5eAaImIp3nP	být
kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
obepjaty	obepnout	k5eAaPmNgFnP	obepnout
rovníkovou	rovníkový	k2eAgFnSc7d1	Rovníková
a	a	k8xC	a
poledníkovou	poledníkový	k2eAgFnSc7d1	Poledníková
kružnicí	kružnice	k1gFnSc7	kružnice
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgInPc6	jenž
lze	lze	k6eAd1	lze
odečítat	odečítat	k5eAaImF	odečítat
zeměpisné	zeměpisný	k2eAgFnPc4d1	zeměpisná
souřadnice	souřadnice	k1gFnPc4	souřadnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
Cicerona	Cicero	k1gMnSc2	Cicero
používal	používat	k5eAaImAgInS	používat
nebeský	nebeský	k2eAgInSc1d1	nebeský
glóbus	glóbus	k1gInSc1	glóbus
už	už	k9	už
Archimédés	Archimédés	k1gInSc1	Archimédés
(	(	kIx(	(
<g/>
+	+	kIx~	+
212	[number]	k4	212
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
jiných	jiný	k2eAgMnPc2d1	jiný
římských	římský	k2eAgMnPc2d1	římský
pramenů	pramen	k1gInPc2	pramen
zhotovil	zhotovit	k5eAaPmAgInS	zhotovit
první	první	k4xOgInSc4	první
terestrický	terestrický	k2eAgInSc4d1	terestrický
glóbus	glóbus	k1gInSc4	glóbus
Kratés	Kratésa	k1gFnPc2	Kratésa
z	z	k7c2	z
Mallu	Mall	k1gInSc2	Mall
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
150	[number]	k4	150
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInSc1d3	nejstarší
zachovaný	zachovaný	k2eAgInSc1d1	zachovaný
nebeský	nebeský	k2eAgInSc1d1	nebeský
glóbus	glóbus	k1gInSc1	glóbus
je	být	k5eAaImIp3nS	být
součást	součást	k1gFnSc4	součást
sochy	socha	k1gFnSc2	socha
Apollóna	Apollón	k1gMnSc2	Apollón
Farnéského	Farnéský	k1gMnSc2	Farnéský
ze	z	k7c2	z
2	[number]	k4	2
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
římská	římský	k2eAgFnSc1d1	římská
kopie	kopie	k1gFnSc1	kopie
starší	starý	k2eAgFnSc2d2	starší
řecké	řecký	k2eAgFnSc2d1	řecká
předlohy	předloha	k1gFnSc2	předloha
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
téže	týž	k3xTgFnSc2	týž
doby	doba	k1gFnSc2	doba
pochází	pocházet	k5eAaImIp3nS	pocházet
i	i	k9	i
římský	římský	k2eAgInSc1d1	římský
bronzový	bronzový	k2eAgInSc1d1	bronzový
nebeský	nebeský	k2eAgInSc1d1	nebeský
glóbus	glóbus	k1gInSc1	glóbus
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
11	[number]	k4	11
cm	cm	kA	cm
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
v	v	k7c6	v
muzeu	muzeum	k1gNnSc6	muzeum
v	v	k7c6	v
Mohuči	Mohuč	k1gFnSc6	Mohuč
<g/>
.	.	kIx.	.
</s>
<s>
Římský	římský	k2eAgInSc1d1	římský
globus	globus	k1gInSc1	globus
jako	jako	k8xC	jako
symbol	symbol	k1gInSc1	symbol
světa	svět	k1gInSc2	svět
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
předlohou	předloha	k1gFnSc7	předloha
pro	pro	k7c4	pro
panovnická	panovnický	k2eAgNnPc4d1	panovnické
jablka	jablko	k1gNnPc4	jablko
panovníků	panovník	k1gMnPc2	panovník
<g/>
.	.	kIx.	.
</s>
<s>
Konstrukci	konstrukce	k1gFnSc4	konstrukce
nebeského	nebeský	k2eAgInSc2d1	nebeský
glóbu	glóbus	k1gInSc2	glóbus
popsal	popsat	k5eAaPmAgMnS	popsat
v	v	k7c6	v
dopise	dopis	k1gInSc6	dopis
pozdější	pozdní	k2eAgMnSc1d2	pozdější
papež	papež	k1gMnSc1	papež
Gerbert	Gerbert	k1gMnSc1	Gerbert
z	z	k7c2	z
Aurillacu	Aurillacus	k1gInSc2	Aurillacus
(	(	kIx(	(
<g/>
+	+	kIx~	+
<g/>
1003	[number]	k4	1003
<g/>
)	)	kIx)	)
a	a	k8xC	a
z	z	k7c2	z
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
zachovaly	zachovat	k5eAaPmAgInP	zachovat
arabské	arabský	k2eAgInPc1d1	arabský
globy	globus	k1gInPc1	globus
Starého	Starého	k2eAgInSc2d1	Starého
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dva	dva	k4xCgInPc4	dva
nebeské	nebeský	k2eAgInPc4d1	nebeský
glóby	glóbus	k1gInPc4	glóbus
koupil	koupit	k5eAaPmAgMnS	koupit
roku	rok	k1gInSc2	rok
1444	[number]	k4	1444
kardinál	kardinál	k1gMnSc1	kardinál
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
Kusánský	Kusánský	k2eAgMnSc1d1	Kusánský
<g/>
,	,	kIx,	,
k	k	k7c3	k
roku	rok	k1gInSc3	rok
1477	[number]	k4	1477
je	být	k5eAaImIp3nS	být
doložen	doložen	k2eAgInSc1d1	doložen
terestrický	terestrický	k2eAgInSc1d1	terestrický
glóbus	glóbus	k1gInSc1	glóbus
<g/>
,	,	kIx,	,
zhotovený	zhotovený	k2eAgInSc1d1	zhotovený
pro	pro	k7c4	pro
papeže	papež	k1gMnSc4	papež
Sixta	Sixtus	k1gMnSc2	Sixtus
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
roku	rok	k1gInSc2	rok
1492	[number]	k4	1492
pochází	pocházet	k5eAaImIp3nS	pocházet
nejstarší	starý	k2eAgInSc1d3	nejstarší
zachovaný	zachovaný	k2eAgInSc1d1	zachovaný
glóbus	glóbus	k1gInSc1	glóbus
Země	zem	k1gFnSc2	zem
od	od	k7c2	od
Martina	Martin	k1gMnSc2	Martin
Behaima	Behaim	k1gMnSc2	Behaim
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
malíře	malíř	k1gMnSc2	malíř
Georga	Georg	k1gMnSc2	Georg
Glockendorna	Glockendorno	k1gNnSc2	Glockendorno
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
si	se	k3xPyFc3	se
objednala	objednat	k5eAaPmAgFnS	objednat
městská	městský	k2eAgFnSc1d1	městská
rada	rada	k1gFnSc1	rada
v	v	k7c6	v
Norimberku	Norimberk	k1gInSc6	Norimberk
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
vystaven	vystavit	k5eAaPmNgMnS	vystavit
v	v	k7c6	v
tamním	tamní	k2eAgNnSc6d1	tamní
muzeu	muzeum	k1gNnSc6	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejstarší	starý	k2eAgFnSc4d3	nejstarší
patří	patřit	k5eAaImIp3nS	patřit
dále	daleko	k6eAd2	daleko
Hunt-Lenoxův	Hunt-Lenoxův	k2eAgInSc1d1	Hunt-Lenoxův
měděný	měděný	k2eAgInSc1d1	měděný
glóbus	glóbus	k1gInSc1	glóbus
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
asi	asi	k9	asi
12,5	[number]	k4	12,5
<g/>
cm	cm	kA	cm
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
vystavený	vystavený	k2eAgMnSc1d1	vystavený
v	v	k7c6	v
New	New	k1gFnSc6	New
York	York	k1gInSc1	York
Public	publicum	k1gNnPc2	publicum
Library	Librara	k1gFnSc2	Librara
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
vyroben	vyrobit	k5eAaPmNgInS	vyrobit
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Evropě	Evropa	k1gFnSc6	Evropa
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1510	[number]	k4	1510
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
něm	on	k3xPp3gNnSc6	on
už	už	k6eAd1	už
zobrazena	zobrazen	k2eAgFnSc1d1	zobrazena
Jižní	jižní	k2eAgFnSc1d1	jižní
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
norimberského	norimberský	k2eAgMnSc2d1	norimberský
astronoma	astronom	k1gMnSc2	astronom
Schönera	Schöner	k1gMnSc2	Schöner
pocházejí	pocházet	k5eAaImIp3nP	pocházet
tři	tři	k4xCgInPc4	tři
globusy	globus	k1gInPc4	globus
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1515	[number]	k4	1515
<g/>
,	,	kIx,	,
1520	[number]	k4	1520
a	a	k8xC	a
1525	[number]	k4	1525
<g/>
.	.	kIx.	.
</s>
<s>
Zemský	zemský	k2eAgInSc4d1	zemský
globus	globus	k1gInSc4	globus
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
také	také	k9	také
Leonardo	Leonardo	k1gMnSc1	Leonardo
da	da	k?	da
Vinci	Vinca	k1gMnPc7	Vinca
(	(	kIx(	(
<g/>
1516	[number]	k4	1516
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
velké	velký	k2eAgInPc1d1	velký
terestrické	terestrický	k2eAgInPc1d1	terestrický
glóby	glóbus	k1gInPc1	glóbus
těšily	těšit	k5eAaImAgInP	těšit
velké	velká	k1gFnSc3	velká
oblibě	obliba	k1gFnSc3	obliba
a	a	k8xC	a
vyráběly	vyrábět	k5eAaImAgInP	vyrábět
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
v	v	k7c6	v
Holandsku	Holandsko	k1gNnSc6	Holandsko
a	a	k8xC	a
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Výrobu	výroba	k1gFnSc4	výroba
podstatně	podstatně	k6eAd1	podstatně
zdokonalil	zdokonalit	k5eAaPmAgMnS	zdokonalit
vlámský	vlámský	k2eAgMnSc1d1	vlámský
kartograf	kartograf	k1gMnSc1	kartograf
<g/>
,	,	kIx,	,
geograf	geograf	k1gMnSc1	geograf
a	a	k8xC	a
vydavatel	vydavatel	k1gMnSc1	vydavatel
map	mapa	k1gFnPc2	mapa
Gerhard	Gerhard	k1gMnSc1	Gerhard
Mercator	Mercator	k1gMnSc1	Mercator
(	(	kIx(	(
<g/>
+	+	kIx~	+
<g/>
1594	[number]	k4	1594
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
Mercatorova	Mercatorův	k2eAgNnSc2d1	Mercatorovo
zobrazení	zobrazení	k1gNnSc2	zobrazení
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
začal	začít	k5eAaPmAgInS	začít
dutou	dutý	k2eAgFnSc4d1	dutá
dřevěnou	dřevěný	k2eAgFnSc4d1	dřevěná
kouli	koule	k1gFnSc4	koule
polepovat	polepovat	k5eAaImF	polepovat
pruhy	pruh	k1gInPc4	pruh
potištěného	potištěný	k2eAgInSc2d1	potištěný
papíru	papír	k1gInSc2	papír
<g/>
.	.	kIx.	.
</s>
<s>
Vyráběl	vyrábět	k5eAaImAgInS	vyrábět
glóby	glóbus	k1gInPc4	glóbus
pro	pro	k7c4	pro
významné	významný	k2eAgMnPc4d1	významný
evropské	evropský	k2eAgMnPc4d1	evropský
panovníky	panovník	k1gMnPc4	panovník
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
byl	být	k5eAaImAgMnS	být
císař	císař	k1gMnSc1	císař
Karel	Karel	k1gMnSc1	Karel
V.	V.	kA	V.
Mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
byl	být	k5eAaImAgMnS	být
i	i	k9	i
nebeský	nebeský	k2eAgInSc4d1	nebeský
globus	globus	k1gInSc4	globus
z	z	k7c2	z
křišťálu	křišťál	k1gInSc2	křišťál
s	s	k7c7	s
vyrytými	vyrytý	k2eAgFnPc7d1	vyrytá
a	a	k8xC	a
pozlacenými	pozlacený	k2eAgFnPc7d1	pozlacená
hvězdami	hvězda	k1gFnPc7	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Slavným	slavný	k2eAgMnSc7d1	slavný
výrobcem	výrobce	k1gMnSc7	výrobce
byl	být	k5eAaImAgMnS	být
také	také	k9	také
františkánský	františkánský	k2eAgMnSc1d1	františkánský
mnich	mnich	k1gMnSc1	mnich
Vincenzo	Vincenza	k1gFnSc5	Vincenza
Coronelli	Coronelle	k1gFnSc6	Coronelle
(	(	kIx(	(
<g/>
+	+	kIx~	+
<g/>
1718	[number]	k4	1718
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
začal	začít	k5eAaPmAgInS	začít
globus	globus	k1gInSc1	globus
ztrácet	ztrácet	k5eAaImF	ztrácet
vědecký	vědecký	k2eAgInSc4d1	vědecký
význam	význam	k1gInSc4	význam
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
spíše	spíše	k9	spíše
školní	školní	k2eAgFnSc7d1	školní
pomůckou	pomůcka	k1gFnSc7	pomůcka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Velikost	velikost	k1gFnSc1	velikost
a	a	k8xC	a
měřítko	měřítko	k1gNnSc1	měřítko
==	==	k?	==
</s>
</p>
<p>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
globusy	globus	k1gInPc1	globus
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
s	s	k7c7	s
průměrem	průměr	k1gInSc7	průměr
15	[number]	k4	15
až	až	k8xS	až
40	[number]	k4	40
cm	cm	kA	cm
<g/>
,	,	kIx,	,
čemuž	což	k3yRnSc3	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
také	také	k9	také
měřítko	měřítko	k1gNnSc1	měřítko
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
lze	lze	k6eAd1	lze
vypočítat	vypočítat	k5eAaPmF	vypočítat
z	z	k7c2	z
poměru	poměr	k1gInSc2	poměr
poloměru	poloměr	k1gInSc2	poloměr
glóbu	glóbus	k1gInSc2	glóbus
a	a	k8xC	a
poloměru	poloměr	k1gInSc2	poloměr
Země	zem	k1gFnSc2	zem
(	(	kIx(	(
<g/>
6378	[number]	k4	6378
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
glóbus	glóbus	k1gInSc1	glóbus
s	s	k7c7	s
průměrem	průměr	k1gInSc7	průměr
20	[number]	k4	20
cm	cm	kA	cm
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
měřítko	měřítko	k1gNnSc4	měřítko
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
64	[number]	k4	64
000	[number]	k4	000
000	[number]	k4	000
a	a	k8xC	a
model	model	k1gInSc1	model
s	s	k7c7	s
průměrem	průměr	k1gInSc7	průměr
40	[number]	k4	40
cm	cm	kA	cm
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
měřítko	měřítko	k1gNnSc4	měřítko
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
32	[number]	k4	32
000	[number]	k4	000
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dělení	dělení	k1gNnSc1	dělení
glóbů	glóbus	k1gInPc2	glóbus
podle	podle	k7c2	podle
==	==	k?	==
</s>
</p>
<p>
<s>
Obsahu	obsah	k1gInSc3	obsah
</s>
</p>
<p>
<s>
Glóby	glóbus	k1gInPc1	glóbus
politické	politický	k2eAgInPc1d1	politický
(	(	kIx(	(
<g/>
zobrazují	zobrazovat	k5eAaImIp3nP	zobrazovat
hranice	hranice	k1gFnPc4	hranice
státních	státní	k2eAgInPc2d1	státní
celků	celek	k1gInPc2	celek
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Glóby	glóbus	k1gInPc1	glóbus
obecně	obecně	k6eAd1	obecně
geografické	geografický	k2eAgInPc1d1	geografický
(	(	kIx(	(
<g/>
zobrazují	zobrazovat	k5eAaImIp3nP	zobrazovat
zemský	zemský	k2eAgInSc4d1	zemský
povrch	povrch	k1gInSc4	povrch
relativně	relativně	k6eAd1	relativně
bez	bez	k7c2	bez
zkreslení	zkreslení	k1gNnSc2	zkreslení
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Glóby	glóbus	k1gInPc1	glóbus
vesmírných	vesmírný	k2eAgNnPc2d1	vesmírné
tělesMěřítka	tělesMěřítko	k1gNnSc2	tělesMěřítko
</s>
</p>
<p>
<s>
Malé	Malé	k2eAgFnPc1d1	Malé
(	(	kIx(	(
<g/>
do	do	k7c2	do
25	[number]	k4	25
cm	cm	kA	cm
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Střední	střední	k2eAgFnSc1d1	střední
(	(	kIx(	(
<g/>
do	do	k7c2	do
40	[number]	k4	40
cm	cm	kA	cm
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Velké	velký	k2eAgNnSc1d1	velké
Provedení	provedení	k1gNnSc1	provedení
</s>
</p>
<p>
<s>
Technické	technický	k2eAgNnSc1d1	technické
(	(	kIx(	(
<g/>
svítící	svítící	k2eAgFnSc1d1	svítící
<g/>
,	,	kIx,	,
nesvítící	svítící	k2eNgFnSc1d1	nesvítící
<g/>
,	,	kIx,	,
odklápěcí	odklápěcí	k2eAgFnSc1d1	odklápěcí
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Reliéfní	reliéfní	k2eAgFnSc1d1	reliéfní
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
plastická	plastický	k2eAgFnSc1d1	plastická
mapa	mapa	k1gFnSc1	mapa
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Indukční	indukční	k2eAgFnSc1d1	indukční
</s>
</p>
<p>
<s>
Levitující	levitující	k2eAgFnSc1d1	levitující
</s>
</p>
<p>
<s>
==	==	k?	==
Převýšení	převýšení	k1gNnPc4	převýšení
u	u	k7c2	u
plastických	plastický	k2eAgInPc2d1	plastický
glóbů	glóbus	k1gInPc2	glóbus
(	(	kIx(	(
<g/>
reliéfní	reliéfní	k2eAgMnSc1d1	reliéfní
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
typ	typ	k1gInSc1	typ
globu	globus	k1gInSc2	globus
působí	působit	k5eAaImIp3nS	působit
efektním	efektní	k2eAgInSc7d1	efektní
dojmem	dojem	k1gInSc7	dojem
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
jeho	jeho	k3xOp3gInSc1	jeho
povrch	povrch	k1gInSc1	povrch
není	být	k5eNaImIp3nS	být
hladký	hladký	k2eAgInSc4d1	hladký
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
povrch	povrch	k1gInSc4	povrch
Země	zem	k1gFnSc2	zem
plasticky	plasticky	k6eAd1	plasticky
<g/>
,	,	kIx,	,
ve	v	k7c6	v
3	[number]	k4	3
<g/>
D.	D.	kA	D.
Měřítko	měřítko	k1gNnSc1	měřítko
převýšení	převýšení	k1gNnSc2	převýšení
nemůže	moct	k5eNaImIp3nS	moct
odpovídat	odpovídat	k5eAaImF	odpovídat
měřítku	měřítko	k1gNnSc3	měřítko
glóbusu	glóbus	k1gInSc2	glóbus
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
například	například	k6eAd1	například
u	u	k7c2	u
modelu	model	k1gInSc2	model
s	s	k7c7	s
průměrem	průměr	k1gInSc7	průměr
40	[number]	k4	40
cm	cm	kA	cm
a	a	k8xC	a
měřítkem	měřítko	k1gNnSc7	měřítko
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
32	[number]	k4	32
000	[number]	k4	000
000	[number]	k4	000
by	by	k9	by
Mt	Mt	k1gFnPc1	Mt
<g/>
.	.	kIx.	.
</s>
<s>
Everest	Everest	k1gInSc1	Everest
představoval	představovat	k5eAaImAgInS	představovat
hrbolek	hrbolek	k1gInSc4	hrbolek
o	o	k7c6	o
výšce	výška	k1gFnSc6	výška
0,28	[number]	k4	0,28
<g/>
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Převýšení	převýšení	k1gNnSc1	převýšení
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
musí	muset	k5eAaImIp3nS	muset
několikanásobně	několikanásobně	k6eAd1	několikanásobně
zvětšit	zvětšit	k5eAaPmF	zvětšit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Technologie	technologie	k1gFnSc2	technologie
výroby	výroba	k1gFnSc2	výroba
==	==	k?	==
</s>
</p>
<p>
<s>
Lepení	lepení	k1gNnSc1	lepení
<g/>
:	:	kIx,	:
Kdysi	kdysi	k6eAd1	kdysi
se	se	k3xPyFc4	se
glóby	glóbus	k1gInPc1	glóbus
vyráběly	vyrábět	k5eAaImAgInP	vyrábět
nalepením	nalepení	k1gNnSc7	nalepení
tištěné	tištěný	k2eAgFnSc2d1	tištěná
mapy	mapa	k1gFnSc2	mapa
na	na	k7c4	na
dutou	dutý	k2eAgFnSc4d1	dutá
kouli	koule	k1gFnSc4	koule
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
ze	z	k7c2	z
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
Papírová	papírový	k2eAgFnSc1d1	papírová
mapa	mapa	k1gFnSc1	mapa
byla	být	k5eAaImAgFnS	být
rozvržena	rozvrhnout	k5eAaPmNgFnS	rozvrhnout
do	do	k7c2	do
určitého	určitý	k2eAgInSc2d1	určitý
počtu	počet	k1gInSc2	počet
čočkovitých	čočkovitý	k2eAgInPc2d1	čočkovitý
úseků	úsek	k1gInPc2	úsek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
navlhčily	navlhčit	k5eAaPmAgFnP	navlhčit
a	a	k8xC	a
nalepovaly	nalepovat	k5eAaImAgFnP	nalepovat
od	od	k7c2	od
pólu	pól	k1gInSc2	pól
k	k	k7c3	k
pólu	pól	k1gInSc3	pól
<g/>
.	.	kIx.	.
</s>
<s>
Čím	co	k3yInSc7	co
víc	hodně	k6eAd2	hodně
úseků	úsek	k1gInPc2	úsek
a	a	k8xC	a
čím	čí	k3xOyQgNnSc7	čí
byly	být	k5eAaImAgInP	být
užší	úzký	k2eAgInPc4d2	užší
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
lépe	dobře	k6eAd2	dobře
mohly	moct	k5eAaImAgInP	moct
ke	k	k7c3	k
kouli	koule	k1gFnSc3	koule
přiléhat	přiléhat	k5eAaImF	přiléhat
<g/>
,	,	kIx,	,
nalepování	nalepování	k1gNnSc1	nalepování
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
pracnější	pracný	k2eAgNnSc1d2	pracnější
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Termovakuové	Termovakuové	k2eAgNnSc1d1	Termovakuové
tvarování	tvarování	k1gNnSc1	tvarování
<g/>
:	:	kIx,	:
K	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
plastová	plastový	k2eAgFnSc1d1	plastová
fólie	fólie	k1gFnSc1	fólie
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yIgFnSc4	který
se	se	k3xPyFc4	se
natisknou	natisknout	k5eAaPmIp3nP	natisknout
mapy	mapa	k1gFnPc1	mapa
například	například	k6eAd1	například
severní	severní	k2eAgFnSc2d1	severní
a	a	k8xC	a
jižní	jižní	k2eAgFnSc2d1	jižní
polokoule	polokoule	k1gFnSc2	polokoule
<g/>
.	.	kIx.	.
</s>
<s>
Výroba	výroba	k1gFnSc1	výroba
probíhá	probíhat	k5eAaImIp3nS	probíhat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
dvě	dva	k4xCgFnPc1	dva
plastové	plastový	k2eAgFnPc1d1	plastová
fólie	fólie	k1gFnPc1	fólie
vloží	vložit	k5eAaPmIp3nP	vložit
do	do	k7c2	do
vakuového	vakuový	k2eAgInSc2d1	vakuový
lisu	lis	k1gInSc2	lis
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
zahřejí	zahřát	k5eAaPmIp3nP	zahřát
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
fólie	fólie	k1gFnSc1	fólie
stává	stávat	k5eAaImIp3nS	stávat
tvárnou	tvárný	k2eAgFnSc4d1	tvárná
<g/>
.	.	kIx.	.
</s>
<s>
Vyčerpáním	vyčerpání	k1gNnSc7	vyčerpání
vzduchu	vzduch	k1gInSc2	vzduch
se	se	k3xPyFc4	se
fólie	fólie	k1gFnPc1	fólie
přisají	přisát	k5eAaPmIp3nP	přisát
k	k	k7c3	k
matrici	matrice	k1gFnSc3	matrice
a	a	k8xC	a
získají	získat	k5eAaPmIp3nP	získat
tak	tak	k6eAd1	tak
tvar	tvar	k1gInSc4	tvar
koule	koule	k1gFnSc2	koule
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
s	s	k7c7	s
reliéfem	reliéf	k1gInSc7	reliéf
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odříznutí	odříznutí	k1gNnSc6	odříznutí
přebytečného	přebytečný	k2eAgInSc2d1	přebytečný
materiálu	materiál	k1gInSc2	materiál
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
úzký	úzký	k2eAgInSc1d1	úzký
proužek	proužek	k1gInSc1	proužek
podél	podél	k7c2	podél
rovníku	rovník	k1gInSc2	rovník
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
ke	k	k7c3	k
spojení	spojení	k1gNnSc4	spojení
obou	dva	k4xCgFnPc2	dva
polokoulí	polokoule	k1gFnPc2	polokoule
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
závěr	závěr	k1gInSc4	závěr
se	se	k3xPyFc4	se
glóby	glóbus	k1gInPc1	glóbus
natřou	natřít	k5eAaPmIp3nP	natřít
bezbarvým	bezbarvý	k2eAgInSc7d1	bezbarvý
lakem	lak	k1gInSc7	lak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Ottův	Ottův	k2eAgInSc1d1	Ottův
slovník	slovník	k1gInSc1	slovník
naučný	naučný	k2eAgInSc1d1	naučný
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
Globus	globus	k1gInSc1	globus
<g/>
.	.	kIx.	.
</s>
<s>
Sv.	sv.	kA	sv.
10	[number]	k4	10
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
199	[number]	k4	199
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Mapa	mapa	k1gFnSc1	mapa
</s>
</p>
<p>
<s>
Gerhard	Gerhard	k1gMnSc1	Gerhard
Mercator	Mercator	k1gMnSc1	Mercator
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
glóbus	glóbus	k1gInSc1	glóbus
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
glóbus	glóbus	k1gInSc1	glóbus
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
starých	starý	k2eAgInPc2d1	starý
amerických	americký	k2eAgInPc2d1	americký
globusů	globus	k1gInPc2	globus
</s>
</p>
<p>
<s>
Sbírka	sbírka	k1gFnSc1	sbírka
globusů	globus	k1gInPc2	globus
Vídeňské	vídeňský	k2eAgFnSc2d1	Vídeňská
národní	národní	k2eAgFnSc2d1	národní
knihovny	knihovna	k1gFnSc2	knihovna
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Encyclopedie	Encyclopedie	k1gFnSc1	Encyclopedie
Britannica	Britannica	k1gFnSc1	Britannica
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
Gerardus	Gerardus	k1gMnSc1	Gerardus
Mercator	Mercator	k1gMnSc1	Mercator
</s>
</p>
