<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
v	v	k7c6	v
kartografii	kartografie	k1gFnSc6	kartografie
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
zmenšené	zmenšený	k2eAgFnPc4d1	zmenšená
prostorové	prostorový	k2eAgFnPc4d1	prostorová
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
kulové	kulový	k2eAgFnPc1d1	kulová
<g/>
,	,	kIx,	,
znázornění	znázornění	k1gNnSc1	znázornění
určitého	určitý	k2eAgNnSc2d1	určité
vesmírného	vesmírný	k2eAgNnSc2d1	vesmírné
tělesa	těleso	k1gNnSc2	těleso
pomocí	pomocí	k7c2	pomocí
kartografických	kartografický	k2eAgInPc2d1	kartografický
prostředků	prostředek	k1gInPc2	prostředek
<g/>
?	?	kIx.	?
</s>
