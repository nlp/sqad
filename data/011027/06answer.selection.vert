<s>
Glóbus	glóbus	k1gInSc1	glóbus
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
globus	globus	k1gInSc1	globus
<g/>
,	,	kIx,	,
koule	koule	k1gFnSc1	koule
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
kartografii	kartografie	k1gFnSc6	kartografie
zmenšené	zmenšený	k2eAgInPc4d1	zmenšený
prostorové	prostorový	k2eAgInPc4d1	prostorový
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
kulové	kulový	k2eAgFnPc1d1	kulová
<g/>
,	,	kIx,	,
znázornění	znázornění	k1gNnSc1	znázornění
určitého	určitý	k2eAgNnSc2d1	určité
vesmírného	vesmírný	k2eAgNnSc2d1	vesmírné
tělesa	těleso	k1gNnSc2	těleso
(	(	kIx(	(
<g/>
zemský	zemský	k2eAgInSc1d1	zemský
glóbus	glóbus	k1gInSc1	glóbus
<g/>
,	,	kIx,	,
měsíční	měsíční	k2eAgInSc1d1	měsíční
glóbus	glóbus	k1gInSc1	glóbus
<g/>
,	,	kIx,	,
glóbus	glóbus	k1gInSc1	glóbus
Marsu	Mars	k1gInSc2	Mars
<g/>
)	)	kIx)	)
pomocí	pomocí	k7c2	pomocí
kartografických	kartografický	k2eAgInPc2d1	kartografický
prostředků	prostředek	k1gInPc2	prostředek
<g/>
.	.	kIx.	.
</s>
