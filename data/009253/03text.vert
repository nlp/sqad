<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Lotyšska	Lotyšsko	k1gNnSc2	Lotyšsko
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
obdélníkovým	obdélníkový	k2eAgInSc7d1	obdélníkový
listem	list	k1gInSc7	list
o	o	k7c6	o
poměru	poměr	k1gInSc6	poměr
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
se	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
vodorovnými	vodorovný	k2eAgInPc7d1	vodorovný
pruhy	pruh	k1gInPc7	pruh
<g/>
:	:	kIx,	:
karmínovým	karmínový	k2eAgInSc7d1	karmínový
<g/>
,	,	kIx,	,
bílým	bílý	k2eAgInSc7d1	bílý
a	a	k8xC	a
karmínovým	karmínový	k2eAgInSc7d1	karmínový
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
šířek	šířka	k1gFnPc2	šířka
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Barvy	barva	k1gFnPc1	barva
a	a	k8xC	a
symbolika	symbolika	k1gFnSc1	symbolika
==	==	k?	==
</s>
</p>
<p>
<s>
Karmínově	karmínově	k6eAd1	karmínově
tmavočervená	tmavočervený	k2eAgFnSc1d1	tmavočervená
barva	barva	k1gFnSc1	barva
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
krev	krev	k1gFnSc4	krev
prolitou	prolitý	k2eAgFnSc4d1	prolitá
v	v	k7c6	v
minulých	minulý	k2eAgFnPc6d1	minulá
dobách	doba	k1gFnPc6	doba
<g/>
,	,	kIx,	,
bílá	bílý	k2eAgFnSc1d1	bílá
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
pravdu	pravda	k1gFnSc4	pravda
<g/>
,	,	kIx,	,
právo	právo	k1gNnSc4	právo
<g/>
,	,	kIx,	,
čest	čest	k1gFnSc4	čest
a	a	k8xC	a
pravdomluvnost	pravdomluvnost	k1gFnSc4	pravdomluvnost
občanů	občan	k1gMnPc2	občan
<g/>
.	.	kIx.	.
</s>
<s>
Tmavočervená	tmavočervený	k2eAgFnSc1d1	tmavočervená
barva	barva	k1gFnSc1	barva
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
nazývána	nazýván	k2eAgFnSc1d1	nazývána
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
lotyšská	lotyšský	k2eAgFnSc1d1	lotyšská
červeň	červeň	k1gFnSc1	červeň
<g/>
"	"	kIx"	"
a	a	k8xC	a
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
lotyšskou	lotyšský	k2eAgFnSc4d1	lotyšská
vlajku	vlajka	k1gFnSc4	vlajka
od	od	k7c2	od
podobné	podobný	k2eAgFnSc2d1	podobná
vlajky	vlajka	k1gFnSc2	vlajka
rakouské	rakouský	k2eAgFnSc2d1	rakouská
<g/>
.	.	kIx.	.
</s>
<s>
Bílá	bílý	k2eAgFnSc1d1	bílá
a	a	k8xC	a
karmínová	karmínový	k2eAgFnSc1d1	karmínová
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
národními	národní	k2eAgFnPc7d1	národní
barvami	barva	k1gFnPc7	barva
už	už	k6eAd1	už
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Lotyšsko	Lotyšsko	k1gNnSc4	Lotyšsko
s	s	k7c7	s
jižním	jižní	k2eAgNnSc7d1	jižní
Estonskem	Estonsko	k1gNnSc7	Estonsko
tvořily	tvořit	k5eAaImAgFnP	tvořit
dohromady	dohromady	k6eAd1	dohromady
jeden	jeden	k4xCgInSc4	jeden
celek	celek	k1gInSc4	celek
zvaný	zvaný	k2eAgInSc4d1	zvaný
Livonsko	Livonsko	k1gNnSc4	Livonsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
10	[number]	k4	10
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Lotyšska	Lotyšsko	k1gNnSc2	Lotyšsko
existovaly	existovat	k5eAaImAgInP	existovat
nezávislé	závislý	k2eNgInPc1d1	nezávislý
státní	státní	k2eAgInPc1d1	státní
útvary	útvar	k1gInPc1	útvar
Ersika	Ersikum	k1gNnSc2	Ersikum
<g/>
,	,	kIx,	,
Koknese	Koknese	k1gFnSc1	Koknese
a	a	k8xC	a
Tálava	Tálava	k1gFnSc1	Tálava
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
území	území	k1gNnSc1	území
bylo	být	k5eAaImAgNnS	být
dobyto	dobýt	k5eAaPmNgNnS	dobýt
řádem	řád	k1gInSc7	řád
livonských	livonský	k2eAgMnPc2d1	livonský
rytířů	rytíř	k1gMnPc2	rytíř
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1237	[number]	k4	1237
spojil	spojit	k5eAaPmAgInS	spojit
s	s	k7c7	s
řádem	řád	k1gInSc7	řád
německých	německý	k2eAgMnPc2d1	německý
rytířů	rytíř	k1gMnPc2	rytíř
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
ovládl	ovládnout	k5eAaPmAgInS	ovládnout
Livonsko	Livonsko	k1gNnSc4	Livonsko
až	až	k6eAd1	až
do	do	k7c2	do
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
livonských	livonský	k2eAgFnPc2d1	livonská
válek	válka	k1gFnPc2	válka
(	(	kIx(	(
<g/>
1558	[number]	k4	1558
<g/>
–	–	k?	–
<g/>
1583	[number]	k4	1583
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
území	území	k1gNnSc1	území
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
(	(	kIx(	(
<g/>
1562	[number]	k4	1562
<g/>
)	)	kIx)	)
mezi	mezi	k7c4	mezi
Polsko	Polsko	k1gNnSc4	Polsko
a	a	k8xC	a
Švédsko	Švédsko	k1gNnSc4	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1629	[number]	k4	1629
získalo	získat	k5eAaPmAgNnS	získat
Švédsko	Švédsko	k1gNnSc1	Švédsko
severozápad	severozápad	k1gInSc1	severozápad
území	území	k1gNnSc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Severní	severní	k2eAgFnSc6d1	severní
válce	válka	k1gFnSc6	válka
(	(	kIx(	(
<g/>
1721	[number]	k4	1721
<g/>
)	)	kIx)	)
připadla	připadnout	k5eAaPmAgFnS	připadnout
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
Lotyšska	Lotyšsko	k1gNnSc2	Lotyšsko
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1795	[number]	k4	1795
postupně	postupně	k6eAd1	postupně
připojilo	připojit	k5eAaPmAgNnS	připojit
i	i	k9	i
zbytek	zbytek	k1gInSc4	zbytek
území	území	k1gNnSc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Rusko	Rusko	k1gNnSc1	Rusko
toto	tento	k3xDgNnSc1	tento
území	území	k1gNnSc1	území
rozčlenilo	rozčlenit	k5eAaPmAgNnS	rozčlenit
na	na	k7c4	na
Kuronskou	Kuronský	k2eAgFnSc4d1	Kuronská
<g/>
,	,	kIx,	,
Livonskou	Livonský	k2eAgFnSc4d1	Livonská
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgFnSc4d1	dnešní
jižní	jižní	k2eAgNnSc4d1	jižní
Estonsko	Estonsko	k1gNnSc4	Estonsko
<g/>
)	)	kIx)	)
a	a	k8xC	a
Vitebskou	Vitebský	k2eAgFnSc4d1	Vitebská
gubernii	gubernie	k1gFnSc4	gubernie
a	a	k8xC	a
na	na	k7c6	na
těchto	tento	k3xDgNnPc6	tento
územích	území	k1gNnPc6	území
začalo	začít	k5eAaPmAgNnS	začít
užívat	užívat	k5eAaImF	užívat
ruské	ruský	k2eAgFnPc4d1	ruská
vlajky	vlajka	k1gFnPc4	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ty	k3xPp2nSc1	ty
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
staly	stát	k5eAaPmAgFnP	stát
prvními	první	k4xOgFnPc7	první
státními	státní	k2eAgFnPc7d1	státní
vlajkami	vlajka	k1gFnPc7	vlajka
v	v	k7c6	v
moderních	moderní	k2eAgFnPc6d1	moderní
dějinách	dějiny	k1gFnPc6	dějiny
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Lotyšska	Lotyšsko	k1gNnSc2	Lotyšsko
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
našel	najít	k5eAaPmAgMnS	najít
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
student	student	k1gMnSc1	student
<g/>
,	,	kIx,	,
folklorista	folklorista	k1gMnSc1	folklorista
a	a	k8xC	a
pozdější	pozdní	k2eAgMnSc1d2	pozdější
profesor	profesor	k1gMnSc1	profesor
Jē	Jē	k1gMnSc1	Jē
Jū	Jū	k1gMnSc1	Jū
(	(	kIx(	(
<g/>
1847	[number]	k4	1847
<g/>
–	–	k?	–
<g/>
1928	[number]	k4	1928
<g/>
)	)	kIx)	)
ve	v	k7c6	v
Starší	starý	k2eAgFnSc6d2	starší
rýmované	rýmovaný	k2eAgFnSc6d1	rýmovaná
kronice	kronika	k1gFnSc6	kronika
Livonského	Livonský	k2eAgInSc2d1	Livonský
řádu	řád	k1gInSc2	řád
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1279	[number]	k4	1279
zmínku	zmínka	k1gFnSc4	zmínka
o	o	k7c6	o
užívání	užívání	k1gNnSc6	užívání
praporu	prapor	k1gInSc2	prapor
s	s	k7c7	s
vodorovnými	vodorovný	k2eAgInPc7d1	vodorovný
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
širokými	široký	k2eAgInPc7d1	široký
červeno-bílo-červenými	červenoílo-červený	k2eAgInPc7d1	červeno-bílo-červený
pruhy	pruh	k1gInPc7	pruh
<g/>
,	,	kIx,	,
starými	starý	k2eAgInPc7d1	starý
lotyšskými	lotyšský	k2eAgInPc7d1	lotyšský
kmeny	kmen	k1gInPc7	kmen
v	v	k7c6	v
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
kmenům	kmen	k1gInPc3	kmen
estonským	estonský	k2eAgInPc3d1	estonský
<g/>
.	.	kIx.	.
</s>
<s>
Lotyšská	lotyšský	k2eAgFnSc1d1	lotyšská
vlajka	vlajka	k1gFnSc1	vlajka
proto	proto	k8xC	proto
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejstarším	starý	k2eAgInPc3d3	nejstarší
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
vzrůstajícím	vzrůstající	k2eAgNnSc7d1	vzrůstající
lotyšským	lotyšský	k2eAgNnSc7d1	lotyšské
národním	národní	k2eAgNnSc7d1	národní
hnutí	hnutí	k1gNnSc1	hnutí
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
vlajka	vlajka	k1gFnSc1	vlajka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1870	[number]	k4	1870
užita	užit	k2eAgFnSc1d1	užita
lotyšskými	lotyšský	k2eAgMnPc7d1	lotyšský
studenty	student	k1gMnPc7	student
Tartuské	tartuský	k2eAgFnSc2d1	Tartuská
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Estonsku	Estonsko	k1gNnSc6	Estonsko
poprvé	poprvé	k6eAd1	poprvé
jako	jako	k8xS	jako
etnická	etnický	k2eAgFnSc1d1	etnická
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Popularita	popularita	k1gFnSc1	popularita
vlajky	vlajka	k1gFnSc2	vlajka
vzrůstala	vzrůstat	k5eAaImAgFnS	vzrůstat
<g/>
,	,	kIx,	,
po	po	k7c6	po
okupaci	okupace	k1gFnSc6	okupace
části	část	k1gFnSc2	část
Lotyšska	Lotyšsko	k1gNnSc2	Lotyšsko
Německem	Německo	k1gNnSc7	Německo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1914	[number]	k4	1914
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1915	[number]	k4	1915
lotyšské	lotyšský	k2eAgFnPc1d1	lotyšská
carské	carský	k2eAgFnPc1d1	carská
jednotky	jednotka	k1gFnPc1	jednotka
formovaly	formovat	k5eAaImAgFnP	formovat
pod	pod	k7c7	pod
touto	tento	k3xDgFnSc7	tento
vlajkou	vlajka	k1gFnSc7	vlajka
(	(	kIx(	(
<g/>
není	být	k5eNaImIp3nS	být
obrázek	obrázek	k1gInSc1	obrázek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Pro	pro	k7c4	pro
možnost	možnost	k1gFnSc4	možnost
záměny	záměna	k1gFnSc2	záměna
s	s	k7c7	s
rakouskou	rakouský	k2eAgFnSc7d1	rakouská
vlajkou	vlajka	k1gFnSc7	vlajka
(	(	kIx(	(
<g/>
ta	ten	k3xDgFnSc1	ten
měla	mít	k5eAaImAgFnS	mít
poměr	poměr	k1gInSc4	poměr
stran	strana	k1gFnPc2	strana
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
<g/>
,	,	kIx,	,
po	po	k7c6	po
návrhu	návrh	k1gInSc6	návrh
lotyšských	lotyšský	k2eAgMnPc2d1	lotyšský
umělců	umělec	k1gMnPc2	umělec
<g/>
,	,	kIx,	,
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1917	[number]	k4	1917
změněn	změnit	k5eAaPmNgInS	změnit
odstín	odstín	k1gInSc1	odstín
červené	červený	k2eAgFnSc2d1	červená
barvy	barva	k1gFnSc2	barva
na	na	k7c6	na
etnické	etnický	k2eAgFnSc6d1	etnická
vlajce	vlajka	k1gFnSc6	vlajka
na	na	k7c4	na
karmínovou	karmínový	k2eAgFnSc4d1	karmínová
a	a	k8xC	a
po	po	k7c4	po
doporučení	doporučení	k1gNnSc4	doporučení
malíře	malíř	k1gMnSc2	malíř
Ansise	Ansise	k1gFnSc2	Ansise
Cirulise	Cirulise	k1gFnSc1	Cirulise
byla	být	k5eAaImAgFnS	být
změněna	změnit	k5eAaPmNgFnS	změnit
šířka	šířka	k1gFnSc1	šířka
bílého	bílý	k2eAgInSc2d1	bílý
pruhu	pruh	k1gInSc2	pruh
na	na	k7c4	na
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
5	[number]	k4	5
šířky	šířka	k1gFnSc2	šířka
listu	list	k1gInSc2	list
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vlajka	vlajka	k1gFnSc1	vlajka
byla	být	k5eAaImAgFnS	být
shodná	shodný	k2eAgFnSc1d1	shodná
se	s	k7c7	s
současnou	současný	k2eAgFnSc7d1	současná
vlajkou	vlajka	k1gFnSc7	vlajka
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
únorové	únorový	k2eAgFnSc6d1	únorová
revoluci	revoluce	k1gFnSc6	revoluce
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Lotyšsku	Lotyšsko	k1gNnSc6	Lotyšsko
ustanovena	ustanoven	k2eAgFnSc1d1	ustanovena
moc	moc	k1gFnSc1	moc
sovětů	sovět	k1gInPc2	sovět
a	a	k8xC	a
země	země	k1gFnSc1	země
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
prakticky	prakticky	k6eAd1	prakticky
autonomní	autonomní	k2eAgFnSc1d1	autonomní
ruskou	ruský	k2eAgFnSc7d1	ruská
republikou	republika	k1gFnSc7	republika
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
rudých	rudý	k2eAgInPc2d1	rudý
praporů	prapor	k1gInPc2	prapor
s	s	k7c7	s
různými	různý	k2eAgInPc7d1	různý
revolučními	revoluční	k2eAgInPc7d1	revoluční
hesly	heslo	k1gNnPc7	heslo
se	se	k3xPyFc4	se
žádné	žádný	k3yNgFnPc1	žádný
oficiální	oficiální	k2eAgFnPc1d1	oficiální
vlajky	vlajka	k1gFnPc1	vlajka
neužívaly	užívat	k5eNaImAgFnP	užívat
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
rudých	rudý	k2eAgFnPc2d1	rudá
vlajek	vlajka	k1gFnPc2	vlajka
(	(	kIx(	(
<g/>
různých	různý	k2eAgInPc2d1	různý
poměrů	poměr	k1gInPc2	poměr
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
odstínů	odstín	k1gInPc2	odstín
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
s	s	k7c7	s
různými	různý	k2eAgNnPc7d1	různé
revolučními	revoluční	k2eAgNnPc7d1	revoluční
hesly	heslo	k1gNnPc7	heslo
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
jiné	jiný	k2eAgFnPc1d1	jiná
vlajky	vlajka	k1gFnPc1	vlajka
neužívaly	užívat	k5eNaImAgFnP	užívat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1918	[number]	k4	1918
bylo	být	k5eAaImAgNnS	být
celé	celý	k2eAgNnSc1d1	celé
území	území	k1gNnSc1	území
Lotyšska	Lotyšsko	k1gNnSc2	Lotyšsko
obsazeno	obsadit	k5eAaPmNgNnS	obsadit
Německem	Německo	k1gNnSc7	Německo
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
podepsalo	podepsat	k5eAaPmAgNnS	podepsat
sovětské	sovětský	k2eAgNnSc1d1	sovětské
Rusko	Rusko	k1gNnSc1	Rusko
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
Brestlitevský	brestlitevský	k2eAgInSc1d1	brestlitevský
mír	mír	k1gInSc1	mír
<g/>
,	,	kIx,	,
tuto	tento	k3xDgFnSc4	tento
smlouvu	smlouva	k1gFnSc4	smlouva
však	však	k9	však
13	[number]	k4	13
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1918	[number]	k4	1918
anulovalo	anulovat	k5eAaBmAgNnS	anulovat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
Němců	Němec	k1gMnPc2	Němec
bylo	být	k5eAaImAgNnS	být
18	[number]	k4	18
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
na	na	k7c6	na
části	část	k1gFnSc6	část
území	území	k1gNnSc4	území
vyhlášeno	vyhlášen	k2eAgNnSc4d1	vyhlášeno
nezávislé	závislý	k2eNgNnSc4d1	nezávislé
Lotyšsko	Lotyšsko	k1gNnSc4	Lotyšsko
<g/>
.	.	kIx.	.
</s>
<s>
Neoficiální	neoficiální	k2eAgFnSc7d1	neoficiální
vlajkou	vlajka	k1gFnSc7	vlajka
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
vlajka	vlajka	k1gFnSc1	vlajka
shodná	shodný	k2eAgFnSc1d1	shodná
s	s	k7c7	s
vlajkou	vlajka	k1gFnSc7	vlajka
současnou	současný	k2eAgFnSc7d1	současná
<g/>
.	.	kIx.	.
<g/>
Lotyšsko	Lotyšsko	k1gNnSc1	Lotyšsko
muselo	muset	k5eAaImAgNnS	muset
čelit	čelit	k5eAaImF	čelit
útokům	útok	k1gInPc3	útok
ze	z	k7c2	z
sovětského	sovětský	k2eAgNnSc2d1	sovětské
Lotyšska	Lotyšsko	k1gNnSc2	Lotyšsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1919	[number]	k4	1919
vyhlášena	vyhlášen	k2eAgFnSc1d1	vyhlášena
Sovětská	sovětský	k2eAgFnSc1d1	sovětská
socialistická	socialistický	k2eAgFnSc1d1	socialistická
republika	republika	k1gFnSc1	republika
Lotyšska	Lotyšsko	k1gNnSc2	Lotyšsko
<g/>
.	.	kIx.	.
</s>
<s>
Vlajkou	vlajka	k1gFnSc7	vlajka
<g/>
,	,	kIx,	,
popsanou	popsaný	k2eAgFnSc4d1	popsaná
v	v	k7c6	v
článku	článek	k1gInSc6	článek
30	[number]	k4	30
ústavy	ústava	k1gFnSc2	ústava
z	z	k7c2	z
15	[number]	k4	15
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1919	[number]	k4	1919
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
rudá	rudý	k2eAgFnSc1d1	rudá
vlajka	vlajka	k1gFnSc1	vlajka
s	s	k7c7	s
písmeny	písmeno	k1gNnPc7	písmeno
S.	S.	kA	S.
<g/>
S.	S.	kA	S.
<g/>
R.	R.	kA	R.
<g/>
L.	L.	kA	L.
nebo	nebo	k8xC	nebo
nápisem	nápis	k1gInSc7	nápis
Socialistická	socialistický	k2eAgFnSc1d1	socialistická
sovětská	sovětský	k2eAgFnSc1d1	sovětská
republika	republika	k1gFnSc1	republika
Lotyšska	Lotyšsko	k1gNnSc2	Lotyšsko
<g/>
.	.	kIx.	.
</s>
<s>
Nebyl	být	k5eNaImAgInS	být
určen	určit	k5eAaPmNgInS	určit
poměr	poměr	k1gInSc1	poměr
stran	strana	k1gFnPc2	strana
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
,	,	kIx,	,
barvy	barva	k1gFnPc4	barva
ani	ani	k8xC	ani
jazyk	jazyk	k1gInSc4	jazyk
písmen	písmeno	k1gNnPc2	písmeno
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
se	se	k3xPyFc4	se
užívala	užívat	k5eAaImAgNnP	užívat
žlutá	žlutý	k2eAgNnPc1d1	žluté
písmena	písmeno	k1gNnPc1	písmeno
v	v	k7c6	v
Lotyštině	lotyština	k1gFnSc6	lotyština
<g/>
:	:	kIx,	:
L.	L.	kA	L.
<g/>
S.	S.	kA	S.
<g/>
P.	P.	kA	P.
<g/>
R.	R.	kA	R.
neboli	neboli	k8xC	neboli
Latvijas	Latvijas	k1gMnSc1	Latvijas
Sociā	Sociā	k1gFnSc2	Sociā
Padomju	Padomju	k1gFnSc1	Padomju
Republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
13	[number]	k4	13
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1920	[number]	k4	1920
bylo	být	k5eAaImAgNnS	být
území	území	k1gNnSc1	území
sovětského	sovětský	k2eAgNnSc2d1	sovětské
Lotyšska	Lotyšsko	k1gNnSc2	Lotyšsko
dobyto	dobyt	k2eAgNnSc1d1	dobyto
příznivci	příznivec	k1gMnPc1	příznivec
lotyšského	lotyšský	k2eAgMnSc2d1	lotyšský
premiéra	premiér	k1gMnSc2	premiér
Kā	Kā	k1gFnSc1	Kā
Ulmanise	Ulmanise	k1gFnSc1	Ulmanise
a	a	k8xC	a
SSRL	SSRL	kA	SSRL
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
<g/>
.	.	kIx.	.
</s>
<s>
Nezávislost	nezávislost	k1gFnSc1	nezávislost
Lotyšska	Lotyšsko	k1gNnSc2	Lotyšsko
byla	být	k5eAaImAgFnS	být
sovětskou	sovětský	k2eAgFnSc7d1	sovětská
vládou	vláda	k1gFnSc7	vláda
uznána	uznán	k2eAgFnSc1d1	uznána
11	[number]	k4	11
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1920.10	[number]	k4	1920.10
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1922	[number]	k4	1922
vydal	vydat	k5eAaPmAgInS	vydat
lotyšský	lotyšský	k2eAgInSc1d1	lotyšský
kabinet	kabinet	k1gInSc1	kabinet
ministrů	ministr	k1gMnPc2	ministr
výnos	výnos	k1gInSc1	výnos
O	o	k7c6	o
státní	státní	k2eAgFnSc6d1	státní
a	a	k8xC	a
obchodní	obchodní	k2eAgFnSc6d1	obchodní
vlajce	vlajka	k1gFnSc6	vlajka
<g/>
,	,	kIx,	,
vlajkách	vlajka	k1gFnPc6	vlajka
osobních	osobní	k2eAgFnPc6d1	osobní
<g/>
,	,	kIx,	,
služebních	služební	k2eAgFnPc6d1	služební
<g/>
,	,	kIx,	,
rezortních	rezortní	k2eAgFnPc6d1	rezortní
a	a	k8xC	a
vlajkách	vlajka	k1gFnPc6	vlajka
válečných	válečný	k2eAgFnPc2d1	válečná
lodí	loď	k1gFnPc2	loď
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
potvrzoval	potvrzovat	k5eAaImAgInS	potvrzovat
vlajku	vlajka	k1gFnSc4	vlajka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
s	s	k7c7	s
poměrem	poměr	k1gInSc7	poměr
stran	stran	k7c2	stran
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1923	[number]	k4	1923
byl	být	k5eAaImAgInS	být
schválen	schválit	k5eAaPmNgInS	schválit
zákon	zákon	k1gInSc1	zákon
s	s	k7c7	s
účinností	účinnost	k1gFnSc7	účinnost
od	od	k7c2	od
20	[number]	k4	20
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
stejného	stejný	k2eAgInSc2d1	stejný
názvu	název	k1gInSc2	název
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
s	s	k7c7	s
uvedeným	uvedený	k2eAgInSc7d1	uvedený
poměrem	poměr	k1gInSc7	poměr
stran	stran	k7c2	stran
vlajky	vlajka	k1gFnSc2	vlajka
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1939	[number]	k4	1939
si	se	k3xPyFc3	se
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
vynutil	vynutit	k5eAaPmAgInS	vynutit
smlouvu	smlouva	k1gFnSc4	smlouva
o	o	k7c6	o
vzájemné	vzájemný	k2eAgFnSc6d1	vzájemná
pomoci	pomoc	k1gFnSc6	pomoc
<g/>
.	.	kIx.	.
17	[number]	k4	17
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1940	[number]	k4	1940
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
na	na	k7c4	na
lotyšské	lotyšský	k2eAgNnSc4d1	lotyšské
území	území	k1gNnSc4	území
a	a	k8xC	a
21	[number]	k4	21
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
Lotyšská	lotyšský	k2eAgFnSc1d1	lotyšská
sovětská	sovětský	k2eAgFnSc1d1	sovětská
socialistická	socialistický	k2eAgFnSc1d1	socialistická
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
5	[number]	k4	5
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
začleněna	začlenit	k5eAaPmNgFnS	začlenit
do	do	k7c2	do
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
úrovni	úroveň	k1gFnSc6	úroveň
byla	být	k5eAaImAgFnS	být
země	země	k1gFnSc1	země
reprezentována	reprezentovat	k5eAaImNgFnS	reprezentovat
sovětskou	sovětský	k2eAgFnSc7d1	sovětská
vlajkou	vlajka	k1gFnSc7	vlajka
<g/>
.25	.25	k4	.25
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1940	[number]	k4	1940
byla	být	k5eAaImAgFnS	být
při	při	k7c6	při
přijetí	přijetí	k1gNnSc6	přijetí
ústavy	ústava	k1gFnSc2	ústava
Lotyšské	lotyšský	k2eAgFnSc2d1	lotyšská
SSR	SSR	kA	SSR
zavedena	zaveden	k2eAgFnSc1d1	zavedena
lotyšská	lotyšský	k2eAgFnSc1d1	lotyšská
státní	státní	k2eAgFnSc1d1	státní
vlajka	vlajka	k1gFnSc1	vlajka
(	(	kIx(	(
<g/>
popsaná	popsaný	k2eAgFnSc1d1	popsaná
v	v	k7c6	v
článku	článek	k1gInSc6	článek
117	[number]	k4	117
<g/>
)	)	kIx)	)
o	o	k7c6	o
poměru	poměr	k1gInSc6	poměr
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
rudým	rudý	k2eAgInSc7d1	rudý
listem	list	k1gInSc7	list
se	s	k7c7	s
zlatým	zlatý	k2eAgInSc7d1	zlatý
srpem	srp	k1gInSc7	srp
a	a	k8xC	a
kladivem	kladivo	k1gNnSc7	kladivo
v	v	k7c6	v
žerďovém	žerďový	k2eAgMnSc6d1	žerďový
<g/>
,	,	kIx,	,
horním	horní	k2eAgInSc6d1	horní
rohu	roh	k1gInSc6	roh
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
nimiž	jenž	k3xRgFnPc7	jenž
jsou	být	k5eAaImIp3nP	být
zlatá	zlatý	k2eAgNnPc4d1	Zlaté
písmena	písmeno	k1gNnPc4	písmeno
<g/>
.	.	kIx.	.
</s>
<s>
Nedokonalý	dokonalý	k2eNgInSc1d1	nedokonalý
popis	popis	k1gInSc1	popis
byl	být	k5eAaImAgInS	být
6	[number]	k4	6
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
usnesením	usnesení	k1gNnSc7	usnesení
prezidia	prezidium	k1gNnSc2	prezidium
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
sovětu	sovět	k1gInSc2	sovět
LoSSR	LoSSR	k1gMnSc3	LoSSR
doplněn	doplněn	k2eAgInSc4d1	doplněn
<g/>
:	:	kIx,	:
zkřížený	zkřížený	k2eAgInSc4d1	zkřížený
srp	srp	k1gInSc4	srp
a	a	k8xC	a
kladivo	kladivo	k1gNnSc4	kladivo
měl	mít	k5eAaImAgMnS	mít
nad	nad	k7c7	nad
sebou	se	k3xPyFc7	se
písmena	písmeno	k1gNnSc2	písmeno
LPSR	LPSR	kA	LPSR
a	a	k8xC	a
zabíral	zabírat	k5eAaImAgInS	zabírat
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
8	[number]	k4	8
plochy	plocha	k1gFnSc2	plocha
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
.	.	kIx.	.
<g/>
Za	za	k7c4	za
II	II	kA	II
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
bylo	být	k5eAaImAgNnS	být
Lotyšsko	Lotyšsko	k1gNnSc1	Lotyšsko
v	v	k7c6	v
letech	let	k1gInPc6	let
1941	[number]	k4	1941
<g/>
–	–	k?	–
<g/>
1944	[number]	k4	1944
okupováno	okupován	k2eAgNnSc4d1	okupováno
Německem	Německo	k1gNnSc7	Německo
a	a	k8xC	a
patřilo	patřit	k5eAaImAgNnS	patřit
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1941	[number]	k4	1941
jako	jako	k8xC	jako
Generální	generální	k2eAgInSc1d1	generální
okres	okres	k1gInSc1	okres
Lotyšsko	Lotyšsko	k1gNnSc1	Lotyšsko
(	(	kIx(	(
<g/>
Generalbezirk	Generalbezirk	k1gInSc1	Generalbezirk
Lettland	Lettland	k1gInSc1	Lettland
<g/>
)	)	kIx)	)
pod	pod	k7c4	pod
Říšský	říšský	k2eAgInSc4d1	říšský
komisariát	komisariát	k1gInSc4	komisariát
Ostland	Ostland	k1gInSc1	Ostland
(	(	kIx(	(
<g/>
Reichskommissariat	Reichskommissariat	k2eAgInSc1d1	Reichskommissariat
Ostland	Ostland	k1gInSc1	Ostland
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Užívaly	užívat	k5eAaImAgFnP	užívat
se	se	k3xPyFc4	se
tehdejší	tehdejší	k2eAgFnPc1d1	tehdejší
německé	německý	k2eAgFnPc1d1	německá
vlajky	vlajka	k1gFnPc1	vlajka
<g/>
,	,	kIx,	,
od	od	k7c2	od
18	[number]	k4	18
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1941	[number]	k4	1941
bylo	být	k5eAaImAgNnS	být
Generálnímu	generální	k2eAgInSc3d1	generální
direktoriátu	direktoriát	k1gInSc3	direktoriát
a	a	k8xC	a
místním	místní	k2eAgMnPc3d1	místní
orgánům	orgán	k1gMnPc3	orgán
samosprávy	samospráva	k1gFnSc2	samospráva
povoleno	povolit	k5eAaPmNgNnS	povolit
vyvěšovat	vyvěšovat	k5eAaImF	vyvěšovat
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
německou	německý	k2eAgFnSc7d1	německá
vlajkou	vlajka	k1gFnSc7	vlajka
<g/>
)	)	kIx)	)
i	i	k8xC	i
lotyšskou	lotyšský	k2eAgFnSc4d1	lotyšská
červeno-bílo-červenou	červenoílo-červený	k2eAgFnSc4d1	červeno-bílo-červený
vlajku	vlajka	k1gFnSc4	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
července	červenec	k1gInSc2	červenec
až	až	k8xS	až
listopadu	listopad	k1gInSc2	listopad
roku	rok	k1gInSc2	rok
1944	[number]	k4	1944
bylo	být	k5eAaImAgNnS	být
Lotyšsko	Lotyšsko	k1gNnSc1	Lotyšsko
dobyto	dobýt	k5eAaPmNgNnS	dobýt
zpět	zpět	k6eAd1	zpět
sovětskými	sovětský	k2eAgNnPc7d1	sovětské
vojsky	vojsko	k1gNnPc7	vojsko
a	a	k8xC	a
opět	opět	k6eAd1	opět
zavedeny	zaveden	k2eAgInPc4d1	zaveden
symboly	symbol	k1gInPc4	symbol
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1940	[number]	k4	1940
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
17	[number]	k4	17
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1953	[number]	k4	1953
byla	být	k5eAaImAgFnS	být
výnosem	výnos	k1gInSc7	výnos
prezidia	prezidium	k1gNnSc2	prezidium
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
sovětu	sovět	k1gInSc2	sovět
LoSSR	LoSSR	k1gMnPc2	LoSSR
schválena	schválit	k5eAaPmNgFnS	schválit
nová	nový	k2eAgFnSc1d1	nová
vlajka	vlajka	k1gFnSc1	vlajka
svazové	svazový	k2eAgFnSc2d1	svazová
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
schválená	schválený	k2eAgFnSc1d1	schválená
následně	následně	k6eAd1	následně
23	[number]	k4	23
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
zákonem	zákon	k1gInSc7	zákon
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
změnil	změnit	k5eAaPmAgInS	změnit
čl	čl	kA	čl
<g/>
.	.	kIx.	.
116	[number]	k4	116
ústavy	ústava	k1gFnSc2	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
byla	být	k5eAaImAgFnS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
listem	list	k1gInSc7	list
o	o	k7c6	o
poměru	poměr	k1gInSc6	poměr
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
na	na	k7c4	na
nějž	jenž	k3xRgMnSc4	jenž
je	být	k5eAaImIp3nS	být
položeno	položit	k5eAaPmNgNnS	položit
pět	pět	k4xCc1	pět
různobarevných	různobarevný	k2eAgFnPc2d1	různobarevná
polí	pole	k1gFnPc2	pole
<g/>
:	:	kIx,	:
rudé	rudý	k2eAgFnSc2d1	rudá
<g/>
,	,	kIx,	,
široké	široký	k2eAgFnSc2d1	široká
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
šířky	šířka	k1gFnSc2	šířka
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
,	,	kIx,	,
dvě	dva	k4xCgFnPc1	dva
a	a	k8xC	a
dvě	dva	k4xCgNnPc4	dva
zvlněná	zvlněný	k2eAgNnPc4d1	zvlněné
pole	pole	k1gNnPc4	pole
-	-	kIx~	-
střídavě	střídavě	k6eAd1	střídavě
bílá	bílý	k2eAgFnSc1d1	bílá
a	a	k8xC	a
modrá	modrý	k2eAgFnSc1d1	modrá
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
horním	horní	k2eAgInSc6d1	horní
rohu	roh	k1gInSc6	roh
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
rudém	rudý	k2eAgNnSc6d1	Rudé
poli	pole	k1gNnSc6	pole
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
5	[number]	k4	5
délky	délka	k1gFnSc2	délka
listu	list	k1gInSc2	list
zobrazen	zobrazit	k5eAaPmNgInS	zobrazit
zlatý	zlatý	k2eAgInSc1d1	zlatý
srp	srp	k1gInSc1	srp
s	s	k7c7	s
kladivem	kladivo	k1gNnSc7	kladivo
a	a	k8xC	a
nad	nad	k7c7	nad
nimi	on	k3xPp3gInPc7	on
rudá	rudý	k2eAgFnSc1d1	rudá
<g/>
,	,	kIx,	,
zlatě	zlatě	k6eAd1	zlatě
lemovaná	lemovaný	k2eAgFnSc1d1	lemovaná
<g/>
,	,	kIx,	,
pěticípá	pěticípý	k2eAgFnSc1d1	pěticípá
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Přesné	přesný	k2eAgInPc1d1	přesný
rozměry	rozměr	k1gInPc1	rozměr
byly	být	k5eAaImAgInP	být
stanoveny	stanovit	k5eAaPmNgInP	stanovit
až	až	k6eAd1	až
Nařízením	nařízení	k1gNnSc7	nařízení
o	o	k7c6	o
státní	státní	k2eAgFnSc6d1	státní
vlajce	vlajka	k1gFnSc6	vlajka
Lotyšské	lotyšský	k2eAgFnSc2d1	lotyšská
SSR	SSR	kA	SSR
-	-	kIx~	-
schváleným	schválený	k2eAgInSc7d1	schválený
28	[number]	k4	28
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1967	[number]	k4	1967
výnosem	výnos	k1gInSc7	výnos
prezidia	prezidium	k1gNnSc2	prezidium
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
sovětu	sovět	k1gInSc2	sovět
LoSSR	LoSSR	k1gFnSc2	LoSSR
<g/>
.	.	kIx.	.
27	[number]	k4	27
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1981	[number]	k4	1981
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
vlajka	vlajka	k1gFnSc1	vlajka
usnesením	usnesení	k1gNnSc7	usnesení
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
sovětu	sovět	k1gInSc2	sovět
LoSSR	LoSSR	k1gFnSc2	LoSSR
změněna	změněn	k2eAgFnSc1d1	změněna
<g/>
:	:	kIx,	:
na	na	k7c6	na
lícové	lícový	k2eAgFnSc6d1	lícová
straně	strana	k1gFnSc6	strana
byl	být	k5eAaImAgInS	být
vypuštěn	vypuštěn	k2eAgInSc1d1	vypuštěn
emblém	emblém	k1gInSc1	emblém
srpu	srp	k1gInSc2	srp
<g/>
,	,	kIx,	,
kladiva	kladivo	k1gNnSc2	kladivo
a	a	k8xC	a
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Změna	změna	k1gFnSc1	změna
se	se	k3xPyFc4	se
však	však	k9	však
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
prakticky	prakticky	k6eAd1	prakticky
neuplatňovala	uplatňovat	k5eNaImAgFnS	uplatňovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
uvolňování	uvolňování	k1gNnSc4	uvolňování
režimu	režim	k1gInSc2	režim
na	na	k7c6	na
konci	konec	k1gInSc6	konec
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
byla	být	k5eAaImAgFnS	být
7	[number]	k4	7
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1988	[number]	k4	1988
uznána	uznat	k5eAaPmNgFnS	uznat
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
sovětem	sovět	k1gInSc7	sovět
LoSSR	LoSSR	k1gFnSc2	LoSSR
lotyšská	lotyšský	k2eAgFnSc1d1	lotyšská
vlajka	vlajka	k1gFnSc1	vlajka
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1918	[number]	k4	1918
<g/>
–	–	k?	–
<g/>
1940	[number]	k4	1940
za	za	k7c4	za
národní	národní	k2eAgInSc4d1	národní
symbol	symbol	k1gInSc4	symbol
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1988	[number]	k4	1988
byla	být	k5eAaImAgFnS	být
vlajka	vlajka	k1gFnSc1	vlajka
na	na	k7c6	na
Rižském	rižský	k2eAgInSc6d1	rižský
zámku	zámek	k1gInSc6	zámek
poprvé	poprvé	k6eAd1	poprvé
vyvěšena	vyvěšen	k2eAgFnSc1d1	vyvěšena
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
<g />
.	.	kIx.	.
</s>
<s>
února	únor	k1gInSc2	únor
1990	[number]	k4	1990
(	(	kIx(	(
<g/>
s	s	k7c7	s
účinností	účinnost	k1gFnSc7	účinnost
od	od	k7c2	od
27	[number]	k4	27
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
schválil	schválit	k5eAaPmAgInS	schválit
tento	tento	k3xDgInSc1	tento
orgán	orgán	k1gInSc1	orgán
změnu	změna	k1gFnSc4	změna
článků	článek	k1gInPc2	článek
168	[number]	k4	168
<g/>
–	–	k?	–
<g/>
170	[number]	k4	170
ústavy	ústava	k1gFnSc2	ústava
a	a	k8xC	a
vlajka	vlajka	k1gFnSc1	vlajka
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
státní	státní	k2eAgFnSc7d1	státní
vlajkou	vlajka	k1gFnSc7	vlajka
Lotyšské	lotyšský	k2eAgFnSc2d1	lotyšská
SSR	SSR	kA	SSR
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1990	[number]	k4	1990
byla	být	k5eAaImAgFnS	být
přijata	přijat	k2eAgFnSc1d1	přijata
deklarace	deklarace	k1gFnSc1	deklarace
o	o	k7c6	o
svrchovanosti	svrchovanost	k1gFnSc6	svrchovanost
Lotyšska	Lotyšsko	k1gNnSc2	Lotyšsko
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
SSSR	SSSR	kA	SSSR
a	a	k8xC	a
název	název	k1gInSc1	název
země	zem	k1gFnSc2	zem
byl	být	k5eAaImAgInS	být
změněn	změnit	k5eAaPmNgInS	změnit
na	na	k7c6	na
Lotyšská	lotyšský	k2eAgFnSc1d1	lotyšská
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
21	[number]	k4	21
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1991	[number]	k4	1991
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
plná	plný	k2eAgFnSc1d1	plná
nezávislost	nezávislost	k1gFnSc1	nezávislost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
Lotyšska	Lotyšsko	k1gNnSc2	Lotyšsko
</s>
</p>
<p>
<s>
Lotyšská	lotyšský	k2eAgFnSc1d1	lotyšská
hymna	hymna	k1gFnSc1	hymna
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInPc1d1	státní
symboly	symbol	k1gInPc1	symbol
Lotyšska	Lotyšsko	k1gNnSc2	Lotyšsko
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Lotyšska	Lotyšsko	k1gNnSc2	Lotyšsko
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Lotyšská	lotyšský	k2eAgFnSc1d1	lotyšská
vlajka	vlajka	k1gFnSc1	vlajka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Valsts	Valsts	k6eAd1	Valsts
simbolika	simbolika	k1gFnSc1	simbolika
(	(	kIx(	(
<g/>
lotyšsky	lotyšsky	k6eAd1	lotyšsky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
NATIONAL	NATIONAL	kA	NATIONAL
SYMBOLS	SYMBOLS	kA	SYMBOLS
OF	OF	kA	OF
LATVIA	LATVIA	kA	LATVIA
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
-	-	kIx~	-
6.2	[number]	k4	6.2
<g/>
.2018	.2018	k4	.2018
nedostupný	dostupný	k2eNgInSc1d1	nedostupný
</s>
</p>
<p>
<s>
Welcome	Welcom	k1gInSc5	Welcom
to	ten	k3xDgNnSc1	ten
online	onlinout	k5eAaPmIp3nS	onlinout
Latvia	Latvia	k1gFnSc1	Latvia
-	-	kIx~	-
National	National	k1gFnSc1	National
Symbols	Symbolsa	k1gFnPc2	Symbolsa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
lotyšsky	lotyšsky	k6eAd1	lotyšsky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
španělsky	španělsky	k6eAd1	španělsky
<g/>
)	)	kIx)	)
</s>
</p>
