<s>
Berkelium	Berkelium	k1gNnSc1	Berkelium
<g/>
,	,	kIx,	,
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Bk	Bk	k1gFnSc2	Bk
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
Berkelium	Berkelium	k1gNnSc4	Berkelium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
devátým	devátý	k4xOgInSc7	devátý
členem	člen	k1gInSc7	člen
z	z	k7c2	z
řady	řada	k1gFnSc2	řada
aktinoidů	aktinoid	k1gInPc2	aktinoid
<g/>
,	,	kIx,	,
pátým	pátý	k4xOgInSc7	pátý
transuranem	transuran	k1gInSc7	transuran
<g/>
,	,	kIx,	,
silně	silně	k6eAd1	silně
radioaktivní	radioaktivní	k2eAgInSc1d1	radioaktivní
kovový	kovový	k2eAgInSc1d1	kovový
prvek	prvek	k1gInSc1	prvek
<g/>
,	,	kIx,	,
připravovaný	připravovaný	k2eAgInSc1d1	připravovaný
uměle	uměle	k6eAd1	uměle
ozařováním	ozařování	k1gNnSc7	ozařování
jader	jádro	k1gNnPc2	jádro
americia	americium	k1gNnSc2	americium
<g/>
.	.	kIx.	.
</s>
