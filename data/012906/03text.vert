<p>
<s>
Berkelium	Berkelium	k1gNnSc1	Berkelium
<g/>
,	,	kIx,	,
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Bk	Bk	k1gFnSc2	Bk
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
Berkelium	Berkelium	k1gNnSc4	Berkelium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
devátým	devátý	k4xOgInSc7	devátý
členem	člen	k1gInSc7	člen
z	z	k7c2	z
řady	řada	k1gFnSc2	řada
aktinoidů	aktinoid	k1gInPc2	aktinoid
<g/>
,	,	kIx,	,
pátým	pátý	k4xOgInSc7	pátý
transuranem	transuran	k1gInSc7	transuran
<g/>
,	,	kIx,	,
silně	silně	k6eAd1	silně
radioaktivní	radioaktivní	k2eAgInSc1d1	radioaktivní
kovový	kovový	k2eAgInSc1d1	kovový
prvek	prvek	k1gInSc1	prvek
<g/>
,	,	kIx,	,
připravovaný	připravovaný	k2eAgInSc1d1	připravovaný
uměle	uměle	k6eAd1	uměle
ozařováním	ozařování	k1gNnSc7	ozařování
jader	jádro	k1gNnPc2	jádro
americia	americium	k1gNnSc2	americium
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Berkelium	Berkelium	k1gNnSc1	Berkelium
se	se	k3xPyFc4	se
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
uměle	uměle	k6eAd1	uměle
připravený	připravený	k2eAgInSc1d1	připravený
kovový	kovový	k2eAgInSc1d1	kovový
prvek	prvek	k1gInSc1	prvek
z	z	k7c2	z
řady	řada	k1gFnSc2	řada
transuranů	transuran	k1gInPc2	transuran
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fyzikálně-chemické	Fyzikálněhemický	k2eAgFnPc4d1	Fyzikálně-chemická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
Berkelium	Berkelium	k1gNnSc1	Berkelium
je	být	k5eAaImIp3nS	být
radioaktivní	radioaktivní	k2eAgInSc4d1	radioaktivní
kovový	kovový	k2eAgInSc4d1	kovový
prvek	prvek	k1gInSc4	prvek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
doposud	doposud	k6eAd1	doposud
nebyl	být	k5eNaImAgInS	být
izolován	izolovat	k5eAaBmNgInS	izolovat
v	v	k7c6	v
dostatečně	dostatečně	k6eAd1	dostatečně
velkém	velký	k2eAgNnSc6d1	velké
množství	množství	k1gNnSc6	množství
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
určit	určit	k5eAaPmF	určit
všechny	všechen	k3xTgFnPc4	všechen
jeho	jeho	k3xOp3gFnPc4	jeho
fyzikální	fyzikální	k2eAgFnPc4d1	fyzikální
konstanty	konstanta	k1gFnPc4	konstanta
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
stříbřitě	stříbřitě	k6eAd1	stříbřitě
bílou	bílý	k2eAgFnSc4d1	bílá
barvu	barva	k1gFnSc4	barva
a	a	k8xC	a
že	že	k8xS	že
se	se	k3xPyFc4	se
působením	působení	k1gNnSc7	působení
vzdušného	vzdušný	k2eAgInSc2d1	vzdušný
kyslíku	kyslík	k1gInSc2	kyslík
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
zvolna	zvolna	k6eAd1	zvolna
oxiduje	oxidovat	k5eAaBmIp3nS	oxidovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vyzařuje	vyzařovat	k5eAaImIp3nS	vyzařovat
α	α	k?	α
a	a	k8xC	a
γ	γ	k?	γ
záření	záření	k1gNnSc2	záření
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
nutno	nutno	k6eAd1	nutno
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
manipulovat	manipulovat	k5eAaImF	manipulovat
za	za	k7c4	za
dodržování	dodržování	k1gNnSc4	dodržování
bezpečnostních	bezpečnostní	k2eAgNnPc2d1	bezpečnostní
opatření	opatření	k1gNnPc2	opatření
pro	pro	k7c4	pro
práci	práce	k1gFnSc4	práce
s	s	k7c7	s
radioaktivními	radioaktivní	k2eAgInPc7d1	radioaktivní
materiály	materiál	k1gInPc7	materiál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
O	o	k7c6	o
jeho	jeho	k3xOp3gFnPc6	jeho
sloučeninách	sloučenina	k1gFnPc6	sloučenina
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc6	jejich
chemickém	chemický	k2eAgNnSc6d1	chemické
chování	chování	k1gNnSc6	chování
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
velmi	velmi	k6eAd1	velmi
málo	málo	k6eAd1	málo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Berkelium	Berkelium	k1gNnSc1	Berkelium
bylo	být	k5eAaImAgNnS	být
poprvé	poprvé	k6eAd1	poprvé
připraveno	připravit	k5eAaPmNgNnS	připravit
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
bombardováním	bombardování	k1gNnSc7	bombardování
241	[number]	k4	241
<g/>
Am	Am	k1gFnPc2	Am
částicemi	částice	k1gFnPc7	částice
α	α	k?	α
v	v	k7c6	v
cyklotronu	cyklotron	k1gInSc6	cyklotron
jaderné	jaderný	k2eAgFnSc2d1	jaderná
laboratoře	laboratoř	k1gFnSc2	laboratoř
kalifornské	kalifornský	k2eAgFnSc2d1	kalifornská
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c4	v
Berkeley	Berkele	k1gMnPc4	Berkele
<g/>
.	.	kIx.	.
</s>
<s>
Vznikl	vzniknout	k5eAaPmAgInS	vzniknout
tak	tak	k9	tak
izotop	izotop	k1gInSc1	izotop
243	[number]	k4	243
<g/>
Bk	Bk	k1gFnPc2	Bk
s	s	k7c7	s
poločasem	poločas	k1gInSc7	poločas
rozpadu	rozpad	k1gInSc2	rozpad
4,5	[number]	k4	4,5
hodiny	hodina	k1gFnSc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
jeho	on	k3xPp3gMnSc2	on
objevitele	objevitel	k1gMnSc2	objevitel
jsou	být	k5eAaImIp3nP	být
označováni	označovat	k5eAaImNgMnP	označovat
Glenn	Glenna	k1gFnPc2	Glenna
T.	T.	kA	T.
Seaborg	Seaborg	k1gMnSc1	Seaborg
<g/>
,	,	kIx,	,
Stanley	Stanlea	k1gMnSc2	Stanlea
G.	G.	kA	G.
Thompson	Thompson	k1gMnSc1	Thompson
a	a	k8xC	a
Albert	Albert	k1gMnSc1	Albert
Ghiorso	Ghiorsa	k1gFnSc5	Ghiorsa
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
jej	on	k3xPp3gMnSc4	on
pojmenovali	pojmenovat	k5eAaPmAgMnP	pojmenovat
po	po	k7c6	po
místě	místo	k1gNnSc6	místo
vzniku	vznik	k1gInSc2	vznik
–	–	k?	–
Berkeley	Berkelea	k1gFnSc2	Berkelea
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
95	[number]	k4	95
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
241	[number]	k4	241
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
A	a	k9	a
</s>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
4	[number]	k4	4
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
</p>
<p>
<s>
⟶	⟶	k?	⟶
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
97	[number]	k4	97
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
243	[number]	k4	243
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
B	B	kA	B
</s>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc4	mathrm
{	{	kIx(	{
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
241	[number]	k4	241
<g/>
}	}	kIx)	}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
95	[number]	k4	95
<g/>
}	}	kIx)	}
<g/>
Am	Am	k1gFnSc1	Am
<g/>
\	\	kIx~	\
+	+	kIx~	+
<g/>
\	\	kIx~	\
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}	}	kIx)	}
<g/>
He	he	k0	he
<g/>
<g />
.	.	kIx.	.
</s>
<s>
\	\	kIx~	\
\	\	kIx~	\
<g/>
longrightarrow	longrightarrow	k?	longrightarrow
\	\	kIx~	\
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
97	[number]	k4	97
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
243	[number]	k4	243
<g/>
}	}	kIx)	}
<g/>
Bk	Bk	k1gFnSc1	Bk
<g/>
\	\	kIx~	\
+	+	kIx~	+
<g/>
\	\	kIx~	\
2	[number]	k4	2
<g/>
\	\	kIx~	\
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
}	}	kIx)	}
</s>
</p>
<p>
<s>
Izotop	izotop	k1gInSc1	izotop
s	s	k7c7	s
poločasem	poločas	k1gInSc7	poločas
rozpadu	rozpad	k1gInSc2	rozpad
320	[number]	k4	320
dnů	den	k1gInPc2	den
–	–	k?	–
249	[number]	k4	249
<g/>
Bk	Bk	k1gFnSc1	Bk
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
připraven	připravit	k5eAaPmNgInS	připravit
bombardováním	bombardování	k1gNnSc7	bombardování
izotopu	izotop	k1gInSc2	izotop
curia	curium	k1gNnSc2	curium
intenzivním	intenzivní	k2eAgInSc7d1	intenzivní
proudem	proud	k1gInSc7	proud
neutronů	neutron	k1gInPc2	neutron
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Izotopy	izotop	k1gInPc4	izotop
==	==	k?	==
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
19	[number]	k4	19
izotopů	izotop	k1gInPc2	izotop
berkelia	berkelius	k1gMnSc2	berkelius
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
je	být	k5eAaImIp3nS	být
nejstabilnější	stabilní	k2eAgFnSc4d3	nejstabilnější
jsou	být	k5eAaImIp3nP	být
247	[number]	k4	247
<g/>
Bk	Bk	k1gFnPc1	Bk
s	s	k7c7	s
poločasem	poločas	k1gInSc7	poločas
přeměny	přeměna	k1gFnSc2	přeměna
1380	[number]	k4	1380
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
248	[number]	k4	248
<g/>
Bk	Bk	k1gFnPc2	Bk
s	s	k7c7	s
poločasem	poločas	k1gInSc7	poločas
9	[number]	k4	9
let	léto	k1gNnPc2	léto
a	a	k8xC	a
249	[number]	k4	249
<g/>
Bk	Bk	k1gFnPc2	Bk
s	s	k7c7	s
poločasem	poločas	k1gInSc7	poločas
330	[number]	k4	330
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Všechny	všechen	k3xTgInPc1	všechen
zbývající	zbývající	k2eAgInPc1d1	zbývající
radioaktivní	radioaktivní	k2eAgInPc1d1	radioaktivní
izotopy	izotop	k1gInPc1	izotop
mají	mít	k5eAaImIp3nP	mít
poločas	poločas	k1gInSc4	poločas
rozpadu	rozpad	k1gInSc2	rozpad
méně	málo	k6eAd2	málo
než	než	k8xS	než
5	[number]	k4	5
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
izotopy	izotop	k1gInPc1	izotop
berkelia	berkelia	k1gFnSc1	berkelia
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Všechny	všechen	k3xTgInPc1	všechen
izotopy	izotop	k1gInPc1	izotop
berkelia	berkelium	k1gNnSc2	berkelium
jsou	být	k5eAaImIp3nP	být
radioaktivní	radioaktivní	k2eAgInPc1d1	radioaktivní
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
u	u	k7c2	u
zbývajících	zbývající	k2eAgInPc2d1	zbývající
aktinoidů	aktinoid	k1gInPc2	aktinoid
představuje	představovat	k5eAaImIp3nS	představovat
největší	veliký	k2eAgNnSc4d3	veliký
biologické	biologický	k2eAgNnSc4d1	biologické
riziko	riziko	k1gNnSc4	riziko
schopnost	schopnost	k1gFnSc1	schopnost
berkelia	berkelium	k1gNnSc2	berkelium
akumulovat	akumulovat	k5eAaBmF	akumulovat
se	se	k3xPyFc4	se
v	v	k7c6	v
kostní	kostní	k2eAgFnSc6d1	kostní
tkáni	tkáň	k1gFnSc6	tkáň
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jeho	jeho	k3xOp3gFnSc1	jeho
radioaktivita	radioaktivita	k1gFnSc1	radioaktivita
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
poruchy	porucha	k1gFnPc4	porucha
krvetvorby	krvetvorba	k1gFnSc2	krvetvorba
–	–	k?	–
brání	bránit	k5eAaImIp3nS	bránit
vytváření	vytváření	k1gNnSc4	vytváření
červených	červený	k2eAgFnPc2d1	červená
krvinek	krvinka	k1gFnPc2	krvinka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Cotton	Cotton	k1gInSc1	Cotton
F.	F.	kA	F.
A.	A.	kA	A.
<g/>
,	,	kIx,	,
Wilkinson	Wilkinson	k1gMnSc1	Wilkinson
J.	J.	kA	J.
<g/>
:	:	kIx,	:
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
souborné	souborný	k2eAgNnSc1d1	souborné
zpracování	zpracování	k1gNnSc1	zpracování
pro	pro	k7c4	pro
pokročilé	pokročilý	k1gMnPc4	pokročilý
<g/>
,	,	kIx,	,
ACADEMIA	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1973	[number]	k4	1973
</s>
</p>
<p>
<s>
N.	N.	kA	N.
N.	N.	kA	N.
Greenwood	Greenwood	k1gInSc1	Greenwood
–	–	k?	–
A.	A.	kA	A.
Earnshaw	Earnshaw	k1gFnSc2	Earnshaw
<g/>
,	,	kIx,	,
Chemie	chemie	k1gFnSc1	chemie
prvků	prvek	k1gInPc2	prvek
II	II	kA	II
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1993	[number]	k4	1993
ISBN	ISBN	kA	ISBN
80-85427-38-9	[number]	k4	80-85427-38-9
</s>
</p>
<p>
<s>
VOHLÍDAL	VOHLÍDAL	kA	VOHLÍDAL
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
;	;	kIx,	;
ŠTULÍK	štulík	k1gInSc1	štulík
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
;	;	kIx,	;
JULÁK	JULÁK	kA	JULÁK
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
<g/>
.	.	kIx.	.
</s>
<s>
Chemické	chemický	k2eAgFnPc1d1	chemická
a	a	k8xC	a
analytické	analytický	k2eAgFnPc1d1	analytická
tabulky	tabulka	k1gFnPc1	tabulka
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Grada	Grada	k1gFnSc1	Grada
Publishing	Publishing	k1gInSc1	Publishing
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7169	[number]	k4	7169
<g/>
-	-	kIx~	-
<g/>
855	[number]	k4	855
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Aktinoidy	Aktinoida	k1gFnPc1	Aktinoida
</s>
</p>
<p>
<s>
Jaderná	jaderný	k2eAgFnSc1d1	jaderná
fyzika	fyzika	k1gFnSc1	fyzika
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
berkelium	berkelium	k1gNnSc4	berkelium
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
berkelium	berkelium	k1gNnSc4	berkelium
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
