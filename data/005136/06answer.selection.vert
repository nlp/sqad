<s>
Autorem	autor	k1gMnSc7	autor
knihy	kniha	k1gFnSc2	kniha
je	být	k5eAaImIp3nS	být
anglický	anglický	k2eAgMnSc1d1	anglický
matematik	matematik	k1gMnSc1	matematik
a	a	k8xC	a
logik	logik	k1gMnSc1	logik
Lewis	Lewis	k1gFnSc2	Lewis
Carroll	Carroll	k1gMnSc1	Carroll
(	(	kIx(	(
<g/>
vl	vl	k?	vl
<g/>
.	.	kIx.	.
jménem	jméno	k1gNnSc7	jméno
Charles	Charles	k1gMnSc1	Charles
Lutwidge	Lutwidge	k1gNnSc1	Lutwidge
Dodgson	Dodgson	k1gMnSc1	Dodgson
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ji	on	k3xPp3gFnSc4	on
údajně	údajně	k6eAd1	údajně
napsal	napsat	k5eAaBmAgInS	napsat
pro	pro	k7c4	pro
malou	malý	k2eAgFnSc4d1	malá
holčičku	holčička	k1gFnSc4	holčička
Alici	Alice	k1gFnSc4	Alice
Liddellovou	Liddellová	k1gFnSc4	Liddellová
<g/>
.	.	kIx.	.
</s>
