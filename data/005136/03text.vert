<s>
Alenka	Alenka	k1gFnSc1	Alenka
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
divů	div	k1gInPc2	div
(	(	kIx(	(
<g/>
též	též	k9	též
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xC	jako
Alenka	Alenka	k1gFnSc1	Alenka
v	v	k7c6	v
kraji	kraj	k1gInSc6	kraj
divů	div	k1gInPc2	div
a	a	k8xC	a
Alenčina	Alenčin	k2eAgNnPc1d1	Alenčino
dobrodružství	dobrodružství	k1gNnPc1	dobrodružství
v	v	k7c6	v
podzemní	podzemní	k2eAgFnSc6d1	podzemní
říši	říš	k1gFnSc6	říš
<g/>
;	;	kIx,	;
anglicky	anglicky	k6eAd1	anglicky
<g/>
:	:	kIx,	:
Alice	Alice	k1gFnSc1	Alice
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Adventures	Adventures	k1gInSc1	Adventures
in	in	k?	in
Wonderland	Wonderland	k1gInSc1	Wonderland
<g/>
,	,	kIx,	,
1865	[number]	k4	1865
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
přeložili	přeložit	k5eAaPmAgMnP	přeložit
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Císař	Císař	k1gMnSc1	Císař
<g/>
,	,	kIx,	,
a	a	k8xC	a
Hana	Hana	k1gFnSc1	Hana
a	a	k8xC	a
Aloys	Aloys	k1gInSc1	Aloys
Skoumalovi	Skoumalovi	k1gRnPc1	Skoumalovi
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
knihy	kniha	k1gFnSc2	kniha
je	být	k5eAaImIp3nS	být
anglický	anglický	k2eAgMnSc1d1	anglický
matematik	matematik	k1gMnSc1	matematik
a	a	k8xC	a
logik	logik	k1gMnSc1	logik
Lewis	Lewis	k1gFnSc2	Lewis
Carroll	Carroll	k1gMnSc1	Carroll
(	(	kIx(	(
<g/>
vl	vl	k?	vl
<g/>
.	.	kIx.	.
jménem	jméno	k1gNnSc7	jméno
Charles	Charles	k1gMnSc1	Charles
Lutwidge	Lutwidge	k1gNnSc1	Lutwidge
Dodgson	Dodgson	k1gMnSc1	Dodgson
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
ji	on	k3xPp3gFnSc4	on
údajně	údajně	k6eAd1	údajně
napsal	napsat	k5eAaBmAgInS	napsat
pro	pro	k7c4	pro
malou	malý	k2eAgFnSc4d1	malá
holčičku	holčička	k1gFnSc4	holčička
Alici	Alice	k1gFnSc4	Alice
Liddellovou	Liddellová	k1gFnSc4	Liddellová
<g/>
.	.	kIx.	.
</s>
<s>
Dílu	díl	k1gInSc3	díl
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
vědecké	vědecký	k2eAgFnPc4d1	vědecká
interpretace	interpretace	k1gFnPc4	interpretace
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
označeno	označit	k5eAaPmNgNnS	označit
za	za	k7c4	za
mimořádné	mimořádný	k2eAgNnSc4d1	mimořádné
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
postavou	postava	k1gFnSc7	postava
je	být	k5eAaImIp3nS	být
neustále	neustále	k6eAd1	neustále
ohromená	ohromený	k2eAgFnSc1d1	ohromená
Alenka	Alenka	k1gFnSc1	Alenka
<g/>
,	,	kIx,	,
v	v	k7c6	v
bezděčných	bezděčný	k2eAgFnPc6d1	bezděčná
reakcích	reakce	k1gFnPc6	reakce
na	na	k7c4	na
imaginativní	imaginativní	k2eAgInSc4d1	imaginativní
svět	svět	k1gInSc4	svět
plný	plný	k2eAgInSc4d1	plný
podivuhodností	podivuhodnost	k1gFnSc7	podivuhodnost
<g/>
.	.	kIx.	.
</s>
<s>
Tvrdohlavá	tvrdohlavý	k2eAgFnSc1d1	tvrdohlavá
dívka	dívka	k1gFnSc1	dívka
s	s	k7c7	s
rázným	rázný	k2eAgNnSc7d1	rázné
řešením	řešení	k1gNnSc7	řešení
fantazijních	fantazijní	k2eAgFnPc2d1	fantazijní
zápletek	zápletka	k1gFnPc2	zápletka
<g/>
,	,	kIx,	,
způsobených	způsobený	k2eAgFnPc2d1	způsobená
obyvateli	obyvatel	k1gMnPc7	obyvatel
říše	říš	k1gFnSc2	říš
divů	div	k1gInPc2	div
<g/>
.	.	kIx.	.
</s>
<s>
Zjevně	zjevně	k6eAd1	zjevně
ochotná	ochotný	k2eAgFnSc1d1	ochotná
a	a	k8xC	a
přívětivá	přívětivý	k2eAgFnSc1d1	přívětivá
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
neústupná	ústupný	k2eNgNnPc4d1	neústupné
až	až	k8xS	až
tvrdošíjná	tvrdošíjný	k2eAgNnPc4d1	tvrdošíjné
<g/>
,	,	kIx,	,
přitom	přitom	k6eAd1	přitom
stále	stále	k6eAd1	stále
dětsky	dětsky	k6eAd1	dětsky
sympatická	sympatický	k2eAgNnPc1d1	sympatické
<g/>
.	.	kIx.	.
</s>
<s>
Bude	být	k5eAaImBp3nS	být
velmi	velmi	k6eAd1	velmi
pečlivá	pečlivý	k2eAgFnSc1d1	pečlivá
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
práci	práce	k1gFnSc6	práce
<g/>
.	.	kIx.	.
</s>
<s>
Carroll	Carroll	k1gInSc1	Carroll
logicky	logicky	k6eAd1	logicky
svazuje	svazovat	k5eAaImIp3nS	svazovat
absurdum	absurdum	k1gNnSc1	absurdum
světa	svět	k1gInSc2	svět
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
šíři	šíř	k1gFnSc6	šíř
<g/>
,	,	kIx,	,
líčená	líčený	k2eAgFnSc1d1	líčená
přirozenost	přirozenost	k1gFnSc1	přirozenost
i	i	k8xC	i
znalost	znalost	k1gFnSc1	znalost
dětského	dětský	k2eAgNnSc2d1	dětské
chování	chování	k1gNnSc2	chování
<g/>
,	,	kIx,	,
zaručily	zaručit	k5eAaPmAgFnP	zaručit
dílu	díl	k1gInSc2	díl
světové	světový	k2eAgFnSc2d1	světová
uznání	uznání	k1gNnSc3	uznání
a	a	k8xC	a
přerod	přerod	k1gInSc1	přerod
v	v	k7c4	v
klasiku	klasika	k1gFnSc4	klasika
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
se	se	k3xPyFc4	se
dočkalo	dočkat	k5eAaPmAgNnS	dočkat
volného	volný	k2eAgNnSc2d1	volné
pokračování	pokračování	k1gNnSc2	pokračování
<g/>
:	:	kIx,	:
Za	za	k7c7	za
zrcadlem	zrcadlo	k1gNnSc7	zrcadlo
a	a	k8xC	a
co	co	k9	co
tam	tam	k6eAd1	tam
Alenka	Alenka	k1gFnSc1	Alenka
našla	najít	k5eAaPmAgFnS	najít
(	(	kIx(	(
<g/>
též	též	k9	též
známé	známý	k2eAgFnPc1d1	známá
jako	jako	k8xC	jako
Za	za	k7c7	za
zrcadlem	zrcadlo	k1gNnSc7	zrcadlo
a	a	k8xC	a
s	s	k7c7	s
čím	co	k3yRnSc7	co
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
Alenka	Alenka	k1gFnSc1	Alenka
setkala	setkat	k5eAaPmAgFnS	setkat
<g/>
;	;	kIx,	;
anglicky	anglicky	k6eAd1	anglicky
<g/>
:	:	kIx,	:
Through	Through	k1gInSc1	Through
the	the	k?	the
Looking-Glass	Looking-Glass	k1gInSc1	Looking-Glass
and	and	k?	and
What	What	k1gInSc1	What
Alice	Alice	k1gFnSc1	Alice
Found	Found	k1gMnSc1	Found
There	Ther	k1gInSc5	Ther
<g/>
,	,	kIx,	,
1872	[number]	k4	1872
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lorina	Lorina	k1gFnSc1	Lorina
Charlotte	Charlott	k1gInSc5	Charlott
Liddell	Liddell	k1gInSc1	Liddell
<g/>
,	,	kIx,	,
Alice	Alice	k1gFnSc1	Alice
Pleasance	Pleasance	k1gFnSc2	Pleasance
Liddell	Liddella	k1gFnPc2	Liddella
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
fotografie	fotografia	k1gFnSc2	fotografia
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Edith	Edith	k1gMnSc1	Edith
Mary	Mary	k1gFnSc2	Mary
Liddell	Liddell	k1gMnSc1	Liddell
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgFnPc1	tři
mladistvé	mladistvý	k2eAgFnPc1d1	mladistvá
dívenky	dívenka	k1gFnPc1	dívenka
<g/>
,	,	kIx,	,
sestry	sestra	k1gFnPc1	sestra
<g/>
.	.	kIx.	.
</s>
<s>
Lewis	Lewis	k1gFnSc3	Lewis
Carroll	Carroll	k1gMnSc1	Carroll
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
cestoval	cestovat	k5eAaImAgMnS	cestovat
loďkou	loďka	k1gFnSc7	loďka
po	po	k7c6	po
řece	řeka	k1gFnSc6	řeka
Temži	Temže	k1gFnSc4	Temže
z	z	k7c2	z
Oxfordu	Oxford	k1gInSc2	Oxford
do	do	k7c2	do
Godstowu	Godstowus	k1gInSc2	Godstowus
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
plavby	plavba	k1gFnSc2	plavba
se	se	k3xPyFc4	se
Alice	Alice	k1gFnSc1	Alice
otázala	otázat	k5eAaPmAgFnS	otázat
Carrolla	Carroll	k1gMnSc4	Carroll
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
by	by	kYmCp3nS	by
jim	on	k3xPp3gMnPc3	on
nevyprávěl	vyprávět	k5eNaImAgInS	vyprávět
příběh	příběh	k1gInSc4	příběh
-	-	kIx~	-
pohádku	pohádka	k1gFnSc4	pohádka
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
Carroll	Carroll	k1gMnSc1	Carroll
tak	tak	k6eAd1	tak
zapustil	zapustit	k5eAaPmAgMnS	zapustit
kořeny	kořen	k1gInPc4	kořen
fenomenálnímu	fenomenální	k2eAgInSc3d1	fenomenální
příběhu	příběh	k1gInSc3	příběh
<g/>
,	,	kIx,	,
souhlasným	souhlasný	k2eAgNnSc7d1	souhlasné
gestem	gesto	k1gNnSc7	gesto
-	-	kIx~	-
započnutím	započnutí	k1gNnSc7	započnutí
vyprávěním	vyprávění	k1gNnPc3	vyprávění
o	o	k7c6	o
Alence	Alenka	k1gFnSc6	Alenka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
svým	svůj	k3xOyFgInSc7	svůj
pádem	pád	k1gInSc7	pád
do	do	k7c2	do
králičí	králičí	k2eAgFnSc2d1	králičí
nory	nora	k1gFnSc2	nora
vnesla	vnést	k5eAaPmAgFnS	vnést
bizarně	bizarně	k6eAd1	bizarně
fantastické	fantastický	k2eAgInPc4d1	fantastický
prvky	prvek	k1gInPc4	prvek
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
pohádkového	pohádkový	k2eAgInSc2d1	pohádkový
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Alice	Alice	k1gFnSc1	Alice
několikrát	několikrát	k6eAd1	několikrát
prosila	prosit	k5eAaImAgFnS	prosit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jí	on	k3xPp3gFnSc3	on
Carroll	Carroll	k1gMnSc1	Carroll
příběh	příběh	k1gInSc4	příběh
napsal	napsat	k5eAaBmAgMnS	napsat
a	a	k8xC	a
mohla	moct	k5eAaImAgFnS	moct
si	se	k3xPyFc3	se
ho	on	k3xPp3gInSc4	on
kdykoliv	kdykoliv	k6eAd1	kdykoliv
číst	číst	k5eAaImF	číst
<g/>
,	,	kIx,	,
až	až	k9	až
nakonec	nakonec	k6eAd1	nakonec
Carroll	Carroll	k1gMnSc1	Carroll
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
přání	přání	k1gNnSc4	přání
dítěte	dítě	k1gNnSc2	dítě
a	a	k8xC	a
opravdu	opravdu	k6eAd1	opravdu
spisek	spisek	k1gInSc4	spisek
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
a	a	k8xC	a
předal	předat	k5eAaPmAgMnS	předat
jej	on	k3xPp3gMnSc4	on
Alici	Alice	k1gFnSc6	Alice
(	(	kIx(	(
<g/>
nazval	nazvat	k5eAaPmAgInS	nazvat
jej	on	k3xPp3gInSc4	on
<g/>
:	:	kIx,	:
Alice	Alice	k1gFnSc2	Alice
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Adventures	Adventures	k1gMnSc1	Adventures
Under	Under	k1gMnSc1	Under
Ground	Ground	k1gMnSc1	Ground
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
Vánocích	Vánoce	k1gFnPc6	Vánoce
1864	[number]	k4	1864
pak	pak	k6eAd1	pak
Alice	Alice	k1gFnSc1	Alice
dostala	dostat	k5eAaPmAgFnS	dostat
od	od	k7c2	od
Carrolla	Carrollo	k1gNnSc2	Carrollo
dárek	dárek	k1gInSc1	dárek
-	-	kIx~	-
přepracované	přepracovaný	k2eAgNnSc1d1	přepracované
a	a	k8xC	a
rozšířené	rozšířený	k2eAgNnSc1d1	rozšířené
vyprávění	vyprávění	k1gNnSc1	vyprávění
i	i	k9	i
s	s	k7c7	s
ilustracemi	ilustrace	k1gFnPc7	ilustrace
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc1d1	následující
rok	rok	k1gInSc1	rok
byl	být	k5eAaImAgInS	být
pak	pak	k6eAd1	pak
spis	spis	k1gInSc1	spis
vydán	vydán	k2eAgInSc1d1	vydán
s	s	k7c7	s
profesionálními	profesionální	k2eAgFnPc7d1	profesionální
ilustracemi	ilustrace	k1gFnPc7	ilustrace
Johna	John	k1gMnSc2	John
Tenniela	Tenniel	k1gMnSc2	Tenniel
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
doprovodné	doprovodný	k2eAgFnPc4d1	doprovodná
ukázky	ukázka	k1gFnPc4	ukázka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Alenka	Alenka	k1gFnSc1	Alenka
zahlédla	zahlédnout	k5eAaPmAgFnS	zahlédnout
mluvícího	mluvící	k2eAgMnSc4d1	mluvící
bílého	bílý	k2eAgMnSc4d1	bílý
králíka	králík	k1gMnSc4	králík
a	a	k8xC	a
vydala	vydat	k5eAaPmAgFnS	vydat
se	se	k3xPyFc4	se
za	za	k7c7	za
ním	on	k3xPp3gMnSc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Upadla	upadnout	k5eAaPmAgFnS	upadnout
do	do	k7c2	do
hluboké	hluboký	k2eAgFnSc2d1	hluboká
králičí	králičí	k2eAgFnSc2d1	králičí
nory	nora	k1gFnSc2	nora
<g/>
,	,	kIx,	,
dopadla	dopadnout	k5eAaPmAgFnS	dopadnout
do	do	k7c2	do
bizarního	bizarní	k2eAgInSc2d1	bizarní
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
našla	najít	k5eAaPmAgFnS	najít
dortík	dortík	k1gInSc4	dortík
s	s	k7c7	s
nápisem	nápis	k1gInSc7	nápis
"	"	kIx"	"
<g/>
sněz	sníst	k5eAaPmRp2nS	sníst
mě	já	k3xPp1nSc2	já
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
tak	tak	k6eAd1	tak
učinila	učinit	k5eAaPmAgFnS	učinit
<g/>
,	,	kIx,	,
zvětšila	zvětšit	k5eAaPmAgFnS	zvětšit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
byl	být	k5eAaImAgInS	být
dům	dům	k1gInSc1	dům
malý	malý	k2eAgInSc1d1	malý
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
zoufalství	zoufalství	k1gNnSc2	zoufalství
plakala	plakat	k5eAaImAgFnS	plakat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vypití	vypití	k1gNnSc6	vypití
lahvičky	lahvička	k1gFnSc2	lahvička
s	s	k7c7	s
nápisem	nápis	k1gInSc7	nápis
"	"	kIx"	"
<g/>
vypij	vypít	k5eAaPmRp2nS	vypít
mě	já	k3xPp1nSc2	já
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
zmenšila	zmenšit	k5eAaPmAgFnS	zmenšit
<g/>
,	,	kIx,	,
až	až	k9	až
byla	být	k5eAaImAgFnS	být
úplně	úplně	k6eAd1	úplně
malinká	malinký	k2eAgFnSc1d1	malinká
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
málem	málem	k6eAd1	málem
utopila	utopit	k5eAaPmAgFnS	utopit
v	v	k7c6	v
kaluži	kaluž	k1gFnSc6	kaluž
svých	svůj	k3xOyFgFnPc2	svůj
slz	slza	k1gFnPc2	slza
<g/>
.	.	kIx.	.
</s>
<s>
Seznámila	seznámit	k5eAaPmAgFnS	seznámit
se	se	k3xPyFc4	se
s	s	k7c7	s
mluvící	mluvící	k2eAgFnSc7d1	mluvící
myškou	myška	k1gFnSc7	myška
<g/>
,	,	kIx,	,
ptáčky	ptáček	k1gInPc7	ptáček
a	a	k8xC	a
jinými	jiný	k2eAgNnPc7d1	jiné
zvířátky	zvířátko	k1gNnPc7	zvířátko
<g/>
.	.	kIx.	.
</s>
<s>
Alenka	Alenka	k1gFnSc1	Alenka
jim	on	k3xPp3gMnPc3	on
začala	začít	k5eAaPmAgFnS	začít
vyprávět	vyprávět	k5eAaImF	vyprávět
o	o	k7c6	o
své	svůj	k3xOyFgFnSc6	svůj
kočce	kočka	k1gFnSc6	kočka
Mindě	Minda	k1gFnSc6	Minda
<g/>
.	.	kIx.	.
</s>
<s>
Zvířátka	zvířátko	k1gNnPc1	zvířátko
se	se	k3xPyFc4	se
koček	kočka	k1gFnPc2	kočka
bála	bát	k5eAaImAgFnS	bát
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
postupně	postupně	k6eAd1	postupně
rozezleně	rozezleně	k6eAd1	rozezleně
odcházela	odcházet	k5eAaImAgFnS	odcházet
<g/>
.	.	kIx.	.
</s>
<s>
Alenka	Alenka	k1gFnSc1	Alenka
chtěla	chtít	k5eAaImAgFnS	chtít
zpět	zpět	k6eAd1	zpět
svou	svůj	k3xOyFgFnSc4	svůj
původní	původní	k2eAgFnSc4d1	původní
velikost	velikost	k1gFnSc4	velikost
<g/>
,	,	kIx,	,
houseňák	houseňák	k1gInSc1	houseňák
ji	on	k3xPp3gFnSc4	on
doporučil	doporučit	k5eAaPmAgInS	doporučit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
snědla	sníst	k5eAaPmAgFnS	sníst
houbu	houba	k1gFnSc4	houba
<g/>
,	,	kIx,	,
ochutnala	ochutnat	k5eAaPmAgFnS	ochutnat
kus	kus	k1gInSc4	kus
houby	houba	k1gFnSc2	houba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
narostl	narůst	k5eAaPmAgMnS	narůst
ji	on	k3xPp3gFnSc4	on
pouze	pouze	k6eAd1	pouze
krk	krk	k1gInSc4	krk
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
holubice	holubice	k1gFnSc1	holubice
spletla	splést	k5eAaPmAgFnS	splést
s	s	k7c7	s
hadem	had	k1gMnSc7	had
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
sousto	sousto	k1gNnSc1	sousto
z	z	k7c2	z
houby	houba	k1gFnSc2	houba
jí	on	k3xPp3gFnSc2	on
vrátilo	vrátit	k5eAaPmAgNnS	vrátit
její	její	k3xOp3gFnSc4	její
původní	původní	k2eAgFnSc4d1	původní
velikost	velikost	k1gFnSc4	velikost
<g/>
.	.	kIx.	.
</s>
<s>
Seznámila	seznámit	k5eAaPmAgFnS	seznámit
se	se	k3xPyFc4	se
s	s	k7c7	s
kočkou	kočka	k1gFnSc7	kočka
Šklíbou	Šklíba	k1gMnSc7	Šklíba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
může	moct	k5eAaImIp3nS	moct
zmizet	zmizet	k5eAaPmF	zmizet
a	a	k8xC	a
zase	zase	k9	zase
se	se	k3xPyFc4	se
objevit	objevit	k5eAaPmF	objevit
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
ji	on	k3xPp3gFnSc4	on
zachce	zachtít	k5eAaPmIp3nS	zachtít
<g/>
.	.	kIx.	.
</s>
<s>
Alenka	Alenka	k1gFnSc1	Alenka
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc3	on
ptala	ptat	k5eAaImAgFnS	ptat
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nevěděla	vědět	k5eNaImAgFnS	vědět
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
má	mít	k5eAaImIp3nS	mít
jít	jít	k5eAaImF	jít
<g/>
,	,	kIx,	,
chtěla	chtít	k5eAaImAgFnS	chtít
jít	jít	k5eAaImF	jít
"	"	kIx"	"
<g/>
prostě	prostě	k9	prostě
někam	někam	k6eAd1	někam
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
Šklíba	Šklíba	k1gMnSc1	Šklíba
jí	on	k3xPp3gFnSc3	on
odvětila	odvětit	k5eAaPmAgFnS	odvětit
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
někam	někam	k6eAd1	někam
<g/>
"	"	kIx"	"
dojde	dojít	k5eAaPmIp3nS	dojít
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
jde	jít	k5eAaImIp3nS	jít
kteroukoliv	kterýkoliv	k3yIgFnSc7	kterýkoliv
cestou	cesta	k1gFnSc7	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Alenka	Alenka	k1gFnSc1	Alenka
po	po	k7c6	po
cestě	cesta	k1gFnSc6	cesta
narazila	narazit	k5eAaPmAgFnS	narazit
na	na	k7c4	na
zajíce	zajíc	k1gMnSc4	zajíc
Březňáka	březňák	k1gMnSc4	březňák
<g/>
,	,	kIx,	,
Kloboučníka	kloboučník	k1gMnSc4	kloboučník
a	a	k8xC	a
plcha	plch	k1gMnSc4	plch
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
si	se	k3xPyFc3	se
právě	právě	k9	právě
dopřávali	dopřávat	k5eAaImAgMnP	dopřávat
"	"	kIx"	"
<g/>
čaj	čaj	k1gInSc4	čaj
o	o	k7c6	o
páté	pátá	k1gFnSc6	pátá
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Plch	Plch	k1gMnSc1	Plch
na	na	k7c6	na
pikniku	piknik	k1gInSc6	piknik
spal	spát	k5eAaImAgMnS	spát
<g/>
,	,	kIx,	,
nečekaně	nečekaně	k6eAd1	nečekaně
se	se	k3xPyFc4	se
probouzel	probouzet	k5eAaImAgMnS	probouzet
a	a	k8xC	a
nečekaně	nečekaně	k6eAd1	nečekaně
opět	opět	k6eAd1	opět
usínal	usínat	k5eAaImAgMnS	usínat
<g/>
,	,	kIx,	,
pohádka	pohádka	k1gFnSc1	pohádka
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
vyprávěl	vyprávět	k5eAaImAgMnS	vyprávět
<g/>
,	,	kIx,	,
když	když	k8xS	když
zrovna	zrovna	k6eAd1	zrovna
bděl	bdít	k5eAaImAgInS	bdít
<g/>
,	,	kIx,	,
neměla	mít	k5eNaImAgFnS	mít
žádný	žádný	k3yNgInSc4	žádný
smysl	smysl	k1gInSc4	smysl
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
byla	být	k5eAaImAgFnS	být
neustále	neustále	k6eAd1	neustále
přerušována	přerušovat	k5eAaImNgFnS	přerušovat
neustálými	neustálý	k2eAgInPc7d1	neustálý
dotazy	dotaz	k1gInPc7	dotaz
<g/>
.	.	kIx.	.
</s>
<s>
Postavy	postava	k1gFnPc1	postava
svačily	svačit	k5eAaImAgFnP	svačit
neustále	neustále	k6eAd1	neustále
<g/>
,	,	kIx,	,
kloboučníkovy	kloboučníkův	k2eAgFnPc1d1	kloboučníkův
hodinky	hodinka	k1gFnPc1	hodinka
neukazovaly	ukazovat	k5eNaImAgFnP	ukazovat
čas	čas	k1gInSc4	čas
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
datum	datum	k1gNnSc1	datum
<g/>
,	,	kIx,	,
neustále	neustále	k6eAd1	neustále
si	se	k3xPyFc3	se
tedy	tedy	k9	tedy
mohli	moct	k5eAaImAgMnP	moct
dopřávat	dopřávat	k5eAaImF	dopřávat
"	"	kIx"	"
<g/>
čaj	čaj	k1gInSc4	čaj
o	o	k7c6	o
páté	pátá	k1gFnSc6	pátá
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
jídlo	jídlo	k1gNnSc4	jídlo
snědli	sníst	k5eAaPmAgMnP	sníst
<g/>
,	,	kIx,	,
posunuli	posunout	k5eAaPmAgMnP	posunout
se	se	k3xPyFc4	se
jen	jen	k9	jen
na	na	k7c4	na
vedlejší	vedlejší	k2eAgFnPc4d1	vedlejší
židle	židle	k1gFnPc4	židle
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
u	u	k7c2	u
velkého	velký	k2eAgInSc2d1	velký
stolu	stol	k1gInSc2	stol
seděli	sedět	k5eAaImAgMnP	sedět
všichni	všechen	k3xTgMnPc1	všechen
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
rohu	roh	k1gInSc6	roh
<g/>
.	.	kIx.	.
</s>
<s>
Alenku	Alenka	k1gFnSc4	Alenka
taková	takový	k3xDgFnSc1	takový
svačina	svačina	k1gFnSc1	svačina
rychle	rychle	k6eAd1	rychle
omrzela	omrzet	k5eAaPmAgFnS	omrzet
a	a	k8xC	a
vydala	vydat	k5eAaPmAgFnS	vydat
se	se	k3xPyFc4	se
dál	daleko	k6eAd2	daleko
<g/>
,	,	kIx,	,
narazila	narazit	k5eAaPmAgFnS	narazit
na	na	k7c4	na
karty	karta	k1gFnPc4	karta
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
vojáky	voják	k1gMnPc4	voják
Srdcové	srdcový	k2eAgFnSc2d1	srdcová
královny	královna	k1gFnSc2	královna
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yRgFnSc7	který
si	se	k3xPyFc3	se
Alenka	Alenka	k1gFnSc1	Alenka
později	pozdě	k6eAd2	pozdě
zahrála	zahrát	k5eAaPmAgFnS	zahrát
kroket	kroket	k1gInSc4	kroket
<g/>
.	.	kIx.	.
</s>
<s>
Pravidla	pravidlo	k1gNnPc1	pravidlo
kroketu	kroket	k1gInSc3	kroket
se	se	k3xPyFc4	se
však	však	k9	však
příliš	příliš	k6eAd1	příliš
nedodržovala	dodržovat	k5eNaImAgFnS	dodržovat
<g/>
,	,	kIx,	,
branky	branka	k1gFnPc4	branka
(	(	kIx(	(
<g/>
které	který	k3yRgFnPc1	který
tvořily	tvořit	k5eAaImAgFnP	tvořit
také	také	k9	také
karty	karta	k1gFnPc1	karta
<g/>
)	)	kIx)	)
nestále	stále	k6eNd1	stále
pobíhaly	pobíhat	k5eAaImAgInP	pobíhat
po	po	k7c6	po
hřišti	hřiště	k1gNnSc6	hřiště
<g/>
,	,	kIx,	,
ježci	ježek	k1gMnPc5	ježek
(	(	kIx(	(
<g/>
místo	místo	k1gNnSc4	místo
kroketové	kroketový	k2eAgFnSc2d1	kroketová
koule	koule	k1gFnSc2	koule
<g/>
)	)	kIx)	)
a	a	k8xC	a
plameňáci	plameňák	k1gMnPc1	plameňák
(	(	kIx(	(
<g/>
místo	místo	k7c2	místo
kroketové	kroketový	k2eAgFnSc2d1	kroketová
hole	hole	k1gFnSc2	hole
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
také	také	k9	také
neustále	neustále	k6eAd1	neustále
pohybovali	pohybovat	k5eAaImAgMnP	pohybovat
a	a	k8xC	a
odcházeli	odcházet	k5eAaImAgMnP	odcházet
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
zamanulo	zamanout	k5eAaPmAgNnS	zamanout
<g/>
.	.	kIx.	.
</s>
<s>
Královna	královna	k1gFnSc1	královna
každému	každý	k3xTgMnSc3	každý
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
se	se	k3xPyFc4	se
jí	jíst	k5eAaImIp3nS	jíst
jen	jen	k9	jen
nepatrně	patrně	k6eNd1	patrně
znelíbil	znelíbit	k5eAaPmAgInS	znelíbit
<g/>
,	,	kIx,	,
chtěla	chtít	k5eAaImAgFnS	chtít
useknout	useknout	k5eAaPmF	useknout
hlavu	hlava	k1gFnSc4	hlava
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kat	kat	k1gInSc1	kat
nikdy	nikdy	k6eAd1	nikdy
nikoho	nikdo	k3yNnSc4	nikdo
nepopravil	popravit	k5eNaPmAgMnS	popravit
<g/>
,	,	kIx,	,
většinu	většina	k1gFnSc4	většina
odsouzenců	odsouzenec	k1gMnPc2	odsouzenec
omilostnil	omilostnit	k5eAaPmAgMnS	omilostnit
král	král	k1gMnSc1	král
<g/>
,	,	kIx,	,
na	na	k7c4	na
ostatní	ostatní	k2eAgInPc4d1	ostatní
se	se	k3xPyFc4	se
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
zmatku	zmatek	k1gInSc6	zmatek
zapomnělo	zapomnět	k5eAaImAgNnS	zapomnět
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
"	"	kIx"	"
<g/>
odsouzence	odsouzenec	k1gMnSc4	odsouzenec
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
třeba	třeba	k6eAd1	třeba
i	i	k9	i
vévodkyně	vévodkyně	k1gFnSc1	vévodkyně
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
kočka	kočka	k1gFnSc1	kočka
Šklíba	Šklíba	k1gMnSc1	Šklíba
-	-	kIx~	-
popraveny	popravit	k5eAaPmNgFnP	popravit
ale	ale	k8xC	ale
nebyly	být	k5eNaImAgInP	být
<g/>
.	.	kIx.	.
</s>
<s>
Alenka	Alenka	k1gFnSc1	Alenka
dále	daleko	k6eAd2	daleko
potkává	potkávat	k5eAaImIp3nS	potkávat
Noha	noh	k1gMnSc4	noh
a	a	k8xC	a
Paželva	Paželv	k1gMnSc4	Paželv
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
jí	jíst	k5eAaImIp3nS	jíst
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
<g/>
,	,	kIx,	,
vyprávěl	vyprávět	k5eAaImAgMnS	vyprávět
o	o	k7c6	o
tanci	tanec	k1gInSc6	tanec
"	"	kIx"	"
<g/>
humří	humří	k2eAgFnSc1d1	humří
čtverylka	čtverylka	k1gFnSc1	čtverylka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
svědkem	svědek	k1gMnSc7	svědek
zcela	zcela	k6eAd1	zcela
absurdního	absurdní	k2eAgNnSc2d1	absurdní
soudního	soudní	k2eAgNnSc2d1	soudní
přelíčení	přelíčení	k1gNnSc2	přelíčení
<g/>
,	,	kIx,	,
obviněn	obvinit	k5eAaPmNgMnS	obvinit
byl	být	k5eAaImAgMnS	být
srdcový	srdcový	k2eAgInSc4d1	srdcový
spodek	spodek	k1gInSc4	spodek
(	(	kIx(	(
<g/>
na	na	k7c6	na
základě	základ	k1gInSc6	základ
dětské	dětský	k2eAgFnSc2d1	dětská
rýmovačky	rýmovačka	k1gFnSc2	rýmovačka
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
které	který	k3yIgFnSc2	který
"	"	kIx"	"
<g/>
ukradl	ukradnout	k5eAaPmAgMnS	ukradnout
vdolky	vdolek	k1gInPc4	vdolek
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
posléze	posléze	k6eAd1	posléze
<g/>
,	,	kIx,	,
za	za	k7c7	za
"	"	kIx"	"
<g/>
vyrušování	vyrušování	k1gNnSc1	vyrušování
<g/>
"	"	kIx"	"
Srdcová	srdcový	k2eAgFnSc1d1	srdcová
královna	královna	k1gFnSc1	královna
rozkázala	rozkázat	k5eAaPmAgFnS	rozkázat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
i	i	k9	i
Alence	Alenka	k1gFnSc3	Alenka
byla	být	k5eAaImAgFnS	být
sražena	sražen	k2eAgFnSc1d1	sražena
hlava	hlava	k1gFnSc1	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
když	když	k8xS	když
se	se	k3xPyFc4	se
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
slétaly	slétat	k5eAaPmAgFnP	slétat
karty	karta	k1gFnPc4	karta
<g/>
,	,	kIx,	,
probudila	probudit	k5eAaPmAgFnS	probudit
Alenku	Alenka	k1gFnSc4	Alenka
její	její	k3xOp3gFnSc1	její
sestra	sestra	k1gFnSc1	sestra
<g/>
,	,	kIx,	,
vše	všechen	k3xTgNnSc1	všechen
byl	být	k5eAaImAgInS	být
pouhý	pouhý	k2eAgInSc1d1	pouhý
sen	sen	k1gInSc1	sen
<g/>
.	.	kIx.	.
1903	[number]	k4	1903
Alice	Alice	k1gFnSc2	Alice
in	in	k?	in
Wonderland	Wonderland	k1gInSc1	Wonderland
(	(	kIx(	(
<g/>
https://www.youtube.com/watch?v=Ke25rh_8veM	[url]	k4	https://www.youtube.com/watch?v=Ke25rh_8veM
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Percy	Perc	k2eAgFnPc1d1	Perca
Stow	Stow	k1gFnPc1	Stow
a	a	k8xC	a
Cecil	Cecil	k1gMnSc1	Cecil
Hepworth	Hepworth	k1gMnSc1	Hepworth
1915	[number]	k4	1915
Alice	Alice	k1gFnSc2	Alice
in	in	k?	in
Wonderland	Wonderland	k1gInSc1	Wonderland
(	(	kIx(	(
<g/>
https://www.youtube.com/watch?v=oqLP7SedmaM	[url]	k4	https://www.youtube.com/watch?v=oqLP7SedmaM
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
adaptace	adaptace	k1gFnSc1	adaptace
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
W.W.	W.W.	k1gMnSc1	W.W.
<g/>
Young	Young	k1gMnSc1	Young
<g/>
,	,	kIx,	,
v	v	k7c6	v
roli	role	k1gFnSc6	role
Alenky	Alenka	k1gFnSc2	Alenka
<g/>
:	:	kIx,	:
Viola	Viola	k1gFnSc1	Viola
Savoy	Savoa	k1gFnSc2	Savoa
1951	[number]	k4	1951
Alenka	Alenka	k1gFnSc1	Alenka
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
divů	div	k1gInPc2	div
<g/>
,	,	kIx,	,
animovaná	animovaný	k2eAgFnSc1d1	animovaná
filmová	filmový	k2eAgFnSc1d1	filmová
adaptace	adaptace	k1gFnSc1	adaptace
studia	studio	k1gNnSc2	studio
Walta	Walt	k1gMnSc2	Walt
Disneye	Disney	k1gMnSc2	Disney
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Hamilton	Hamilton	k1gInSc1	Hamilton
Luske	Lusk	k1gInSc2	Lusk
<g/>
,	,	kIx,	,
Clyde	Clyd	k1gInSc5	Clyd
Geronimi	Geroni	k1gFnPc7	Geroni
<g/>
,	,	kIx,	,
Wilfred	Wilfred	k1gMnSc1	Wilfred
Jackson	Jackson	k1gInSc4	Jackson
1983	[number]	k4	1983
Alenka	Alenka	k1gFnSc1	Alenka
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
divů	div	k1gInPc2	div
<g/>
,	,	kIx,	,
anglický	anglický	k2eAgInSc1d1	anglický
televizní	televizní	k2eAgInSc1d1	televizní
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Kirk	Kirk	k1gInSc1	Kirk
Browning	browning	k1gInSc1	browning
<g/>
,	,	kIx,	,
hrají	hrát	k5eAaImIp3nP	hrát
<g/>
:	:	kIx,	:
Eve	Eve	k1gFnSc7	Eve
Arden	Ardeny	k1gFnPc2	Ardeny
<g/>
,	,	kIx,	,
Richard	Richard	k1gMnSc1	Richard
Burton	Burton	k1gInSc4	Burton
1983	[number]	k4	1983
Alenka	Alenka	k1gFnSc1	Alenka
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
divů	div	k1gInPc2	div
<g/>
,	,	kIx,	,
japonsko-německý	japonskoěmecký	k2eAgInSc1d1	japonsko-německý
padesátidvoudílný	padesátidvoudílný	k2eAgInSc1d1	padesátidvoudílný
animovaný	animovaný	k2eAgInSc1d1	animovaný
seriál	seriál	k1gInSc1	seriál
<g/>
.	.	kIx.	.
</s>
<s>
Režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Šigeo	Šigeo	k6eAd1	Šigeo
Koši	koš	k1gInSc6	koš
1985	[number]	k4	1985
Alenka	Alenka	k1gFnSc1	Alenka
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
divů	div	k1gInPc2	div
<g/>
,	,	kIx,	,
americký	americký	k2eAgInSc1d1	americký
televizní	televizní	k2eAgInSc1d1	televizní
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Harry	Harr	k1gInPc1	Harr
Harris	Harris	k1gFnSc2	Harris
<g/>
,	,	kIx,	,
Hrají	hrát	k5eAaImIp3nP	hrát
<g/>
:	:	kIx,	:
Jack	Jack	k1gInSc1	Jack
Warden	Wardna	k1gFnPc2	Wardna
<g/>
,	,	kIx,	,
Telly	Tella	k1gFnPc1	Tella
Savalas	Savalas	k1gMnSc1	Savalas
<g/>
,	,	kIx,	,
Patrick	Patrick	k1gMnSc1	Patrick
Duffy	Duff	k1gInPc1	Duff
1988	[number]	k4	1988
Alenka	Alenka	k1gFnSc1	Alenka
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
divů	div	k1gInPc2	div
<g/>
,	,	kIx,	,
australský	australský	k2eAgInSc1d1	australský
animovaný	animovaný	k2eAgInSc1d1	animovaný
videofilm	videofilm	k1gInSc1	videofilm
1988	[number]	k4	1988
Něco	něco	k6eAd1	něco
z	z	k7c2	z
<g />
.	.	kIx.	.
</s>
<s>
Alenky	Alenka	k1gFnPc1	Alenka
<g/>
,	,	kIx,	,
československo-švýcarsko-britsko-německo	československo-švýcarskoritskoěmecko	k6eAd1	československo-švýcarsko-britsko-německo
animovaný	animovaný	k2eAgInSc1d1	animovaný
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Švankmajer	Švankmajer	k1gMnSc1	Švankmajer
se	se	k3xPyFc4	se
literární	literární	k2eAgFnSc7d1	literární
předlohou	předloha	k1gFnSc7	předloha
spíše	spíše	k9	spíše
jen	jen	k6eAd1	jen
inspiroval	inspirovat	k5eAaBmAgMnS	inspirovat
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Švankmajer	Švankmajer	k1gInSc4	Švankmajer
1996	[number]	k4	1996
Alenka	Alenka	k1gFnSc1	Alenka
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
za	za	k7c7	za
zrcadlem	zrcadlo	k1gNnSc7	zrcadlo
<g/>
,	,	kIx,	,
anglický	anglický	k2eAgInSc1d1	anglický
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
J.	J.	kA	J.
Henderson	Henderson	k1gNnSc1	Henderson
<g/>
,	,	kIx,	,
hrají	hrát	k5eAaImIp3nP	hrát
<g/>
:	:	kIx,	:
G.	G.	kA	G.
Palmer	Palmer	k1gMnSc1	Palmer
<g/>
,	,	kIx,	,
G.	G.	kA	G.
<g />
.	.	kIx.	.
</s>
<s>
Wise	Wise	k1gFnSc1	Wise
<g/>
,	,	kIx,	,
M.	M.	kA	M.
Warren	Warrna	k1gFnPc2	Warrna
<g/>
,	,	kIx,	,
S.	S.	kA	S.
Coogan	Coogan	k1gInSc1	Coogan
<g/>
,	,	kIx,	,
K.	K.	kA	K.
Beckinsaleová	Beckinsaleová	k1gFnSc1	Beckinsaleová
1999	[number]	k4	1999
Alenka	Alenka	k1gFnSc1	Alenka
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
divů	div	k1gInPc2	div
<g/>
,	,	kIx,	,
britsko-německo-americký	britskoěmeckomerický	k2eAgInSc1d1	britsko-německo-americký
televizní	televizní	k2eAgInSc1d1	televizní
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Nick	Nick	k1gInSc1	Nick
Willing	Willing	k1gInSc1	Willing
<g/>
,	,	kIx,	,
hrají	hrát	k5eAaImIp3nP	hrát
<g/>
:	:	kIx,	:
Robbie	Robbie	k1gFnSc1	Robbie
Coltrane	Coltran	k1gInSc5	Coltran
<g/>
,	,	kIx,	,
Whoopi	Whoop	k1gFnSc2	Whoop
Goldbergová	Goldbergový	k2eAgFnSc1d1	Goldbergová
<g/>
,	,	kIx,	,
Ben	Ben	k1gInSc1	Ben
Kingsley	Kingslea	k1gFnSc2	Kingslea
2005	[number]	k4	2005
Krajina	Krajina	k1gFnSc1	Krajina
přílivu	příliv	k1gInSc2	příliv
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
Tideland	Tideland	k1gInSc1	Tideland
<g/>
)	)	kIx)	)
Kanadsko	Kanadsko	k1gNnSc1	Kanadsko
-	-	kIx~	-
anglický	anglický	k2eAgInSc1d1	anglický
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
psychedelická	psychedelický	k2eAgFnSc1d1	psychedelická
variace	variace	k1gFnSc1	variace
původního	původní	k2eAgInSc2d1	původní
námětu	námět	k1gInSc2	námět
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc2	režie
<g/>
:	:	kIx,	:
Terry	Terra	k1gFnSc2	Terra
Gilliam	Gilliam	k1gInSc1	Gilliam
<g/>
,	,	kIx,	,
hrají	hrát	k5eAaImIp3nP	hrát
<g/>
:	:	kIx,	:
Jodelle	Jodelle	k1gFnSc1	Jodelle
Ferland	Ferland	k1gInSc1	Ferland
<g/>
,	,	kIx,	,
Jeff	Jeff	k1gMnSc1	Jeff
Bridges	Bridges	k1gInSc4	Bridges
2010	[number]	k4	2010
Alenka	Alenka	k1gFnSc1	Alenka
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
divů	div	k1gMnPc2	div
–	–	k?	–
americký	americký	k2eAgInSc4d1	americký
3D	[number]	k4	3D
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
volná	volný	k2eAgFnSc1d1	volná
adaptace	adaptace	k1gFnSc1	adaptace
původního	původní	k2eAgInSc2d1	původní
námětu	námět	k1gInSc2	námět
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Tim	Tim	k?	Tim
Burton	Burton	k1gInSc1	Burton
<g/>
,	,	kIx,	,
hrají	hrát	k5eAaImIp3nP	hrát
<g/>
:	:	kIx,	:
Mia	Mia	k1gFnSc1	Mia
Wasikowska	Wasikowska	k1gFnSc1	Wasikowska
<g/>
,	,	kIx,	,
Johnny	Johnno	k1gNnPc7	Johnno
Depp	Depp	k1gInSc1	Depp
<g/>
,	,	kIx,	,
Helena	Helena	k1gFnSc1	Helena
Bonham	Bonham	k1gInSc1	Bonham
Carterová	Carterová	k1gFnSc1	Carterová
<g/>
,	,	kIx,	,
Crispin	Crispin	k2eAgInSc1d1	Crispin
Glover	Glover	k1gInSc1	Glover
<g/>
,	,	kIx,	,
Anne	Anne	k1gFnSc1	Anne
Hathawayová	Hathawayový	k2eAgFnSc1d1	Hathawayová
Seriálová	seriálový	k2eAgFnSc1d1	seriálová
podoba	podoba	k1gFnSc1	podoba
příběhu	příběh	k1gInSc2	příběh
o	o	k7c6	o
Říši	říš	k1gFnSc6	říš
Divů	div	k1gInPc2	div
byla	být	k5eAaImAgFnS	být
použita	použít	k5eAaPmNgFnS	použít
ve	v	k7c4	v
spin-off	spinff	k1gInSc4	spin-off
amerického	americký	k2eAgMnSc2d1	americký
tv	tv	k?	tv
seriálu	seriál	k1gInSc2	seriál
Once	Once	k1gFnSc4	Once
Upon	Upon	k1gNnSc1	Upon
a	a	k8xC	a
Time	Time	k1gNnSc1	Time
-	-	kIx~	-
Once	Once	k1gNnSc1	Once
Upon	Upona	k1gFnPc2	Upona
a	a	k8xC	a
Time	Time	k1gFnPc2	Time
in	in	k?	in
Wonderland	Wonderlanda	k1gFnPc2	Wonderlanda
(	(	kIx(	(
<g/>
Bylo	být	k5eAaImAgNnS	být
nebylo	být	k5eNaImAgNnS	být
v	v	k7c6	v
Říši	říš	k1gFnSc6	říš
Divů	div	k1gInPc2	div
<g/>
)	)	kIx)	)
Paželv	Paželv	k1gMnSc1	Paželv
Žvahlav	Žvahlav	k1gMnSc1	Žvahlav
</s>
