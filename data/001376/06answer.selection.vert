<s>
Kadmium	kadmium	k1gNnSc1	kadmium
(	(	kIx(	(
<g/>
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Cd	cd	kA	cd
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Cadmium	Cadmium	k1gNnSc1	Cadmium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
měkký	měkký	k2eAgMnSc1d1	měkký
<g/>
,	,	kIx,	,
lehce	lehko	k6eAd1	lehko
tavitelný	tavitelný	k2eAgInSc1d1	tavitelný
<g/>
,	,	kIx,	,
toxický	toxický	k2eAgInSc1d1	toxický
kovový	kovový	k2eAgInSc1d1	kovový
prvek	prvek	k1gInSc1	prvek
<g/>
.	.	kIx.	.
</s>
