<s>
Tvůrce	tvůrce	k1gMnSc1	tvůrce
seriálu	seriál	k1gInSc2	seriál
a	a	k8xC	a
Star	star	k1gFnSc2	star
Treku	Trek	k1gInSc2	Trek
vůbec	vůbec	k9	vůbec
Gene	gen	k1gInSc5	gen
Roddenberry	Roddenberra	k1gFnPc1	Roddenberra
chtěl	chtít	k5eAaImAgInS	chtít
vytvořit	vytvořit	k5eAaPmF	vytvořit
sci-fi	scii	k1gFnSc7	sci-fi
seriál	seriál	k1gInSc1	seriál
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
by	by	kYmCp3nS	by
ukazoval	ukazovat	k5eAaImAgInS	ukazovat
nejen	nejen	k6eAd1	nejen
na	na	k7c4	na
technologický	technologický	k2eAgInSc4d1	technologický
pokrok	pokrok	k1gInSc4	pokrok
lidstva	lidstvo	k1gNnSc2	lidstvo
daleké	daleký	k2eAgFnSc2d1	daleká
budoucnosti	budoucnost	k1gFnSc2	budoucnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
na	na	k7c4	na
odbourání	odbourání	k1gNnSc4	odbourání
projevů	projev	k1gInPc2	projev
rasismu	rasismus	k1gInSc2	rasismus
<g/>
,	,	kIx,	,
diskriminace	diskriminace	k1gFnSc1	diskriminace
žen	žena	k1gFnPc2	žena
a	a	k8xC	a
dalších	další	k2eAgNnPc2d1	další
negativ	negativum	k1gNnPc2	negativum
<g/>
.	.	kIx.	.
</s>
