<p>
<s>
Arménie	Arménie	k1gFnSc2	Arménie
<g/>
,	,	kIx,	,
oficiálním	oficiální	k2eAgInSc7d1	oficiální
názvem	název	k1gInSc7	název
Arménská	arménský	k2eAgFnSc1d1	arménská
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
arménsky	arménsky	k6eAd1	arménsky
Հ	Հ	k?	Հ
Հ	Հ	k?	Հ
<g/>
,	,	kIx,	,
Hajastani	Hajastaň	k1gFnSc6	Hajastaň
Hanrapetuthjun	Hanrapetuthjuno	k1gNnPc2	Hanrapetuthjuno
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vnitrozemský	vnitrozemský	k2eAgInSc4d1	vnitrozemský
stát	stát	k1gInSc4	stát
ležící	ležící	k2eAgInSc4d1	ležící
v	v	k7c6	v
Zakavkazsku	Zakavkazsko	k1gNnSc6	Zakavkazsko
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgFnPc2	některý
definic	definice	k1gFnPc2	definice
hranice	hranice	k1gFnSc2	hranice
mezi	mezi	k7c7	mezi
Evropou	Evropa	k1gFnSc7	Evropa
a	a	k8xC	a
Asií	Asie	k1gFnSc7	Asie
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
jiných	jiný	k2eAgMnPc2d1	jiný
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
hraničí	hraničit	k5eAaImIp3nP	hraničit
s	s	k7c7	s
Gruzií	Gruzie	k1gFnSc7	Gruzie
(	(	kIx(	(
<g/>
164	[number]	k4	164
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
východě	východ	k1gInSc6	východ
s	s	k7c7	s
Ázerbájdžánem	Ázerbájdžán	k1gInSc7	Ázerbájdžán
a	a	k8xC	a
s	s	k7c7	s
de	de	k?	de
jure	jur	k1gFnPc1	jur
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
patřící	patřící	k2eAgFnSc1d1	patřící
<g/>
,	,	kIx,	,
de	de	k?	de
facto	facto	k1gNnSc4	facto
Arménií	Arménie	k1gFnSc7	Arménie
ovládanou	ovládaný	k2eAgFnSc7d1	ovládaná
Republikou	republika	k1gFnSc7	republika
Arcach	Arcach	k1gMnSc1	Arcach
(	(	kIx(	(
<g/>
566	[number]	k4	566
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
s	s	k7c7	s
Íránem	Írán	k1gInSc7	Írán
(	(	kIx(	(
<g/>
35	[number]	k4	35
km	km	kA	km
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
západě	západ	k1gInSc6	západ
s	s	k7c7	s
ázerbájdžánskou	ázerbájdžánský	k2eAgFnSc7d1	Ázerbájdžánská
exklávou	exkláva	k1gFnSc7	exkláva
Nachičevan	Nachičevan	k1gMnSc1	Nachičevan
(	(	kIx(	(
<g/>
221	[number]	k4	221
km	km	kA	km
<g/>
)	)	kIx)	)
a	a	k8xC	a
Tureckem	Turecko	k1gNnSc7	Turecko
(	(	kIx(	(
<g/>
268	[number]	k4	268
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Arménie	Arménie	k1gFnSc1	Arménie
je	být	k5eAaImIp3nS	být
unitární	unitární	k2eAgInSc4d1	unitární
národní	národní	k2eAgInSc4d1	národní
stát	stát	k1gInSc4	stát
s	s	k7c7	s
velmi	velmi	k6eAd1	velmi
starým	starý	k2eAgNnSc7d1	staré
kulturním	kulturní	k2eAgNnSc7d1	kulturní
dědictvím	dědictví	k1gNnSc7	dědictví
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgNnSc1d3	nejstarší
arménské	arménský	k2eAgNnSc1d1	arménské
království	království	k1gNnSc1	království
zvané	zvaný	k2eAgNnSc1d1	zvané
Urartu	Urarta	k1gFnSc4	Urarta
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
již	již	k6eAd1	již
v	v	k7c4	v
9.	[number]	k4	9.
století	století	k1gNnPc2	století
př.	př.	kA	př.
n.	n.	k?	n.
l.	l.	k?	l.
V	v	k7c6	v
letech	let	k1gInPc6	let
321	[number]	k4	321
př.	př.	kA	př.
n.	n.	k?	n.
l.	l.	k?	l.
-	-	kIx~	-
428	[number]	k4	428
existovalo	existovat	k5eAaImAgNnS	existovat
Arménské	arménský	k2eAgNnSc1d1	arménské
království	království	k1gNnSc1	království
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
svého	svůj	k3xOyFgInSc2	svůj
vrcholu	vrchol	k1gInSc2	vrchol
v	v	k7c6	v
1.	[number]	k4	1.
století	století	k1gNnSc6	století
pod	pod	k7c7	pod
vládou	vláda	k1gFnSc7	vláda
Tigrana	Tigran	k1gMnSc2	Tigran
Velikého	veliký	k2eAgMnSc2d1	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
království	království	k1gNnSc1	království
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
první	první	k4xOgMnSc1	první
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
přijalo	přijmout	k5eAaPmAgNnS	přijmout
křesťanství	křesťanství	k1gNnSc1	křesťanství
jakožto	jakožto	k8xS	jakožto
státní	státní	k2eAgNnSc1d1	státní
náboženství	náboženství	k1gNnSc1	náboženství
(	(	kIx(	(
<g/>
asi	asi	k9	asi
roku	rok	k1gInSc2	rok
301	[number]	k4	301
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
5.	[number]	k4	5.
století	století	k1gNnSc6	století
Arménii	Arménie	k1gFnSc4	Arménie
rozvrátily	rozvrátit	k5eAaPmAgFnP	rozvrátit
dvě	dva	k4xCgFnPc1	dva
mocné	mocný	k2eAgFnPc1d1	mocná
říše	říš	k1gFnPc1	říš
<g/>
,	,	kIx,	,
Byzantská	byzantský	k2eAgNnPc1d1	byzantské
a	a	k8xC	a
Sásánovská	sásánovský	k2eAgNnPc1d1	sásánovský
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
9.	[number]	k4	9.
století	století	k1gNnSc6	století
arménský	arménský	k2eAgInSc4d1	arménský
stát	stát	k1gInSc4	stát
obnovili	obnovit	k5eAaPmAgMnP	obnovit
Bagrationové	Bagrationový	k2eAgFnPc4d1	Bagrationový
<g/>
.	.	kIx.	.
</s>
<s>
Rozdrcen	rozdrcen	k2eAgInSc1d1	rozdrcen
byl	být	k5eAaImAgInS	být
znovu	znovu	k6eAd1	znovu
Byzancí	Byzanc	k1gFnPc2	Byzanc
(	(	kIx(	(
<g/>
1045	[number]	k4	1045
<g/>
)	)	kIx)	)
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
území	území	k1gNnPc2	území
obsadili	obsadit	k5eAaPmAgMnP	obsadit
Seldžučtí	Seldžucký	k2eAgMnPc1d1	Seldžucký
Turci	Turek	k1gMnPc1	Turek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
11.	[number]	k4	11.
<g/>
-	-	kIx~	-
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
stál	stát	k5eAaImAgInS	stát
obnovil	obnovit	k5eAaPmAgInS	obnovit
jakožto	jakožto	k8xS	jakožto
Arménské	arménský	k2eAgNnSc1d1	arménské
království	království	k1gNnSc1	království
v	v	k7c6	v
Kilíkii	Kilíkie	k1gFnSc6	Kilíkie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
16.	[number]	k4	16.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
však	však	k9	však
dostal	dostat	k5eAaPmAgMnS	dostat
pod	pod	k7c4	pod
nadvládu	nadvláda	k1gFnSc4	nadvláda
Osmanů	Osman	k1gMnPc2	Osman
a	a	k8xC	a
Peršanů	Peršan	k1gMnPc2	Peršan
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
nadvláda	nadvláda	k1gFnSc1	nadvláda
trvala	trvat	k5eAaImAgFnS	trvat
až	až	k6eAd1	až
do	do	k7c2	do
19.	[number]	k4	19.
století	století	k1gNnPc2	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
část	část	k1gFnSc1	část
území	území	k1gNnSc2	území
(	(	kIx(	(
<g/>
východní	východní	k2eAgFnSc1d1	východní
<g/>
)	)	kIx)	)
získalo	získat	k5eAaPmAgNnS	získat
Ruské	ruský	k2eAgNnSc1d1	ruské
impérium	impérium	k1gNnSc1	impérium
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
Osmani	Osmaň	k1gFnSc6	Osmaň
za	za	k7c2	za
1.	[number]	k4	1.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
prováděli	provádět	k5eAaImAgMnP	provádět
systematickou	systematický	k2eAgFnSc4d1	systematická
genocidu	genocida	k1gFnSc4	genocida
arménského	arménský	k2eAgNnSc2d1	arménské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
1.	[number]	k4	1.
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
krátce	krátce	k6eAd1	krátce
existovala	existovat	k5eAaImAgFnS	existovat
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
<g/>
–	–	k?	–
<g/>
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
První	první	k4xOgFnSc1	první
arménská	arménský	k2eAgFnSc1d1	arménská
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
širší	široký	k2eAgFnSc1d2	širší
Zakavkaská	Zakavkaský	k2eAgFnSc1d1	Zakavkaský
sovětská	sovětský	k2eAgFnSc1d1	sovětská
federativní	federativní	k2eAgFnSc1d1	federativní
socialistická	socialistický	k2eAgFnSc1d1	socialistická
republika	republika	k1gFnSc1	republika
zahrnující	zahrnující	k2eAgFnSc1d1	zahrnující
více	hodně	k6eAd2	hodně
zakavkazských	zakavkazský	k2eAgInPc2d1	zakavkazský
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1922	[number]	k4	1922
stala	stát	k5eAaPmAgFnS	stát
zakládající	zakládající	k2eAgFnSc7d1	zakládající
republikou	republika	k1gFnSc7	republika
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1936	[number]	k4	1936
existovala	existovat	k5eAaImAgFnS	existovat
Arménská	arménský	k2eAgFnSc1d1	arménská
sovětská	sovětský	k2eAgFnSc1d1	sovětská
socialistická	socialistický	k2eAgFnSc1d1	socialistická
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
oddělená	oddělený	k2eAgFnSc1d1	oddělená
od	od	k7c2	od
ostatních	ostatní	k2eAgFnPc2d1	ostatní
zakavkazských	zakavkazský	k2eAgFnPc2d1	zakavkazská
zemí	zem	k1gFnPc2	zem
do	do	k7c2	do
samostatné	samostatný	k2eAgFnSc2d1	samostatná
sovětské	sovětský	k2eAgFnSc2d1	sovětská
socialistické	socialistický	k2eAgFnSc2d1	socialistická
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
současný	současný	k2eAgInSc1d1	současný
stát	stát	k1gInSc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Arménie	Arménie	k1gFnSc2	Arménie
je	být	k5eAaImIp3nS	být
Jerevan	Jerevan	k1gMnSc1	Jerevan
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
populace	populace	k1gFnSc1	populace
1 075 800	[number]	k4	1 075 800
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
k	k	k7c3	k
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
tvoří	tvořit	k5eAaImIp3nS	tvořit
třetinu	třetina	k1gFnSc4	třetina
z	z	k7c2	z
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
Arménie	Arménie	k1gFnSc2	Arménie
(	(	kIx(	(
<g/>
3	[number]	k4	3
018 854	[number]	k4	018 854
během	během	k7c2	během
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jerevan	Jerevan	k1gMnSc1	Jerevan
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgNnPc2d3	nejstarší
měst	město	k1gNnPc2	město
na	na	k7c6	na
světě	svět	k1gInSc6	svět
s	s	k7c7	s
nepřetržitým	přetržitý	k2eNgNnSc7d1	nepřetržité
lidským	lidský	k2eAgNnSc7d1	lidské
osídlením	osídlení	k1gNnSc7	osídlení
<g/>
,	,	kIx,	,
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
782	[number]	k4	782
př.	př.	kA	př.
n.	n.	k?	n.
l.	l.	k?	l.
</s>
</p>
<p>
<s>
Arménská	arménský	k2eAgFnSc1d1	arménská
národní	národní	k2eAgFnSc1d1	národní
identita	identita	k1gFnSc1	identita
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
úzce	úzko	k6eAd1	úzko
spojená	spojený	k2eAgFnSc1d1	spojená
s	s	k7c7	s
Arménskou	arménský	k2eAgFnSc7d1	arménská
apoštolskou	apoštolský	k2eAgFnSc7d1	apoštolská
církví	církev	k1gFnSc7	církev
<g/>
,	,	kIx,	,
nejstarší	starý	k2eAgFnSc1d3	nejstarší
křesťanskou	křesťanský	k2eAgFnSc7d1	křesťanská
národní	národní	k2eAgFnSc7d1	národní
církví	církev	k1gFnSc7	církev
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Arméni	Armén	k1gMnPc1	Armén
též	též	k9	též
používají	používat	k5eAaImIp3nP	používat
jako	jako	k9	jako
jediní	jediný	k2eAgMnPc1d1	jediný
na	na	k7c6	na
světě	svět	k1gInSc6	svět
unikátní	unikátní	k2eAgFnSc4d1	unikátní
abecedu	abeceda	k1gFnSc4	abeceda
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
405	[number]	k4	405
vymyslel	vymyslet	k5eAaPmAgInS	vymyslet
Mesrop	Mesrop	k1gInSc1	Mesrop
Maštoc	Maštoc	k1gFnSc4	Maštoc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Arménie	Arménie	k1gFnSc1	Arménie
je	být	k5eAaImIp3nS	být
členem	člen	k1gInSc7	člen
Euroasijského	euroasijský	k2eAgInSc2d1	euroasijský
ekonomického	ekonomický	k2eAgInSc2d1	ekonomický
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Název	název	k1gInSc1	název
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc4	jeho
původ	původ	k1gInSc4	původ
==	==	k?	==
</s>
</p>
<p>
<s>
Název	název	k1gInSc1	název
Arménie	Arménie	k1gFnSc2	Arménie
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
variantách	varianta	k1gFnPc6	varianta
v	v	k7c4	v
prakticky	prakticky	k6eAd1	prakticky
všech	všecek	k3xTgInPc6	všecek
jazycích	jazyk	k1gInPc6	jazyk
světa	svět	k1gInSc2	svět
kromě	kromě	k7c2	kromě
samotné	samotný	k2eAgFnSc2d1	samotná
arménštiny	arménština	k1gFnSc2	arménština
<g/>
.	.	kIx.	.
</s>
<s>
Pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
staroperského	staroperský	k2eAgInSc2d1	staroperský
názvu	název	k1gInSc2	název
"	"	kIx"	"
<g/>
Armanestán	Armanestán	k1gInSc1	Armanestán
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Arman	Arman	k1gInSc1	Arman
<g/>
"	"	kIx"	"
podle	podle	k7c2	podle
pohraniční	pohraniční	k2eAgFnSc2d1	pohraniční
provincie	provincie	k1gFnSc2	provincie
státu	stát	k1gInSc2	stát
Urartu	Urart	k1gInSc2	Urart
na	na	k7c6	na
historickém	historický	k2eAgNnSc6d1	historické
arménském	arménský	k2eAgNnSc6d1	arménské
území	území	k1gNnSc6	území
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
1.	[number]	k4	1.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př.	př.	kA	př.
n.	n.	k?	n.
l.	l.	k?	l.
osídleno	osídlen	k2eAgNnSc1d1	osídleno
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
Aramejci	Aramejec	k1gMnSc3	Aramejec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sami	sám	k3xTgMnPc1	sám
Arméni	Armén	k1gMnPc1	Armén
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
jménem	jméno	k1gNnSc7	jméno
"	"	kIx"	"
<g/>
Hajer	hajer	k1gMnSc1	hajer
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
arménsky	arménsky	k6eAd1	arménsky
Հ	Հ	k?	Հ
<g/>
)	)	kIx)	)
a	a	k8xC	a
svou	svůj	k3xOyFgFnSc4	svůj
vlast	vlast	k1gFnSc4	vlast
"	"	kIx"	"
<g/>
Hajastan	Hajastan	k1gInSc4	Hajastan
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Հ	Հ	k?	Հ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přípona	přípona	k1gFnSc1	přípona
–	–	k?	–
<g/>
stan	stan	k1gInSc1	stan
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
země	zem	k1gFnPc1	zem
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
Hajastan	Hajastan	k1gInSc1	Hajastan
je	být	k5eAaImIp3nS	být
země	zem	k1gFnPc4	zem
Hajů	Haj	k1gInPc2	Haj
(	(	kIx(	(
<g/>
čili	čili	k8xC	čili
Arménů	Armén	k1gMnPc2	Armén
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nehledě	hledět	k5eNaImSgMnS	hledět
na	na	k7c4	na
staré	starý	k2eAgFnPc4d1	stará
pohádkové	pohádkový	k2eAgFnPc4d1	pohádková
legendy	legenda	k1gFnPc4	legenda
není	být	k5eNaImIp3nS	být
dosud	dosud	k6eAd1	dosud
objasněno	objasněn	k2eAgNnSc1d1	objasněno
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
toto	tento	k3xDgNnSc4	tento
vlastní	vlastní	k2eAgNnSc4d1	vlastní
pojmenování	pojmenování	k1gNnSc4	pojmenování
arménského	arménský	k2eAgInSc2d1	arménský
národa	národ	k1gInSc2	národ
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Dějiny	dějiny	k1gFnPc1	dějiny
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Starověk	starověk	k1gInSc4	starověk
===	===	k?	===
</s>
</p>
<p>
<s>
Arménská	arménský	k2eAgFnSc1d1	arménská
vysočina	vysočina	k1gFnSc1	vysočina
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
dávným	dávný	k2eAgMnPc3d1	dávný
centrům	centr	k1gMnPc3	centr
lidské	lidský	k2eAgFnSc2d1	lidská
civilizace	civilizace	k1gFnSc2	civilizace
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
trvale	trvale	k6eAd1	trvale
osídlená	osídlený	k2eAgFnSc1d1	osídlená
od	od	k7c2	od
paleolitu	paleolit	k1gInSc2	paleolit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nejstarších	starý	k2eAgFnPc6d3	nejstarší
písemných	písemný	k2eAgFnPc6d1	písemná
památkách	památka	k1gFnPc6	památka
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
Chetitů	Chetit	k1gMnPc2	Chetit
je	být	k5eAaImIp3nS	být
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
národu	národ	k1gInSc6	národ
Chajasa	Chajasa	k1gFnSc1	Chajasa
ve	v	k7c6	v
2.	[number]	k4	2.
tisíciletí	tisíciletí	k1gNnSc6	tisíciletí
před	před	k7c7	před
naším	náš	k3xOp1gInSc7	náš
letopočtem	letopočet	k1gInSc7	letopočet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
ovládli	ovládnout	k5eAaPmAgMnP	ovládnout
území	území	k1gNnSc2	území
Arménie	Arménie	k1gFnSc2	Arménie
Asyřané	Asyřan	k1gMnPc1	Asyřan
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
Jerevan	Jerevan	k1gMnSc1	Jerevan
bylo	být	k5eAaImAgNnS	být
nejstarším	starý	k2eAgNnSc6d3	nejstarší
písemně	písemně	k6eAd1	písemně
doloženým	doložený	k2eAgNnSc7d1	doložené
městem	město	k1gNnSc7	město
na	na	k7c6	na
území	území	k1gNnSc6	území
někdejšího	někdejší	k2eAgInSc2d1	někdejší
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
<s>
Spojením	spojení	k1gNnSc7	spojení
arménských	arménský	k2eAgInPc2d1	arménský
kmenů	kmen	k1gInPc2	kmen
v	v	k7c4	v
9.	[number]	k4	9.
století	století	k1gNnPc2	století
př.	př.	kA	př.
n.	n.	k?	n.
l.	l.	k?	l.
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
říše	říše	k1gFnSc1	říše
Urartu	Urart	k1gInSc2	Urart
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Vanského	Vanský	k2eAgNnSc2d1	Vanský
jezera	jezero	k1gNnSc2	jezero
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říši	říš	k1gFnSc6	říš
Urartu	Urart	k1gInSc2	Urart
byla	být	k5eAaImAgNnP	být
založena	založen	k2eAgNnPc1d1	založeno
mnohá	mnohý	k2eAgNnPc1d1	mnohé
opevněná	opevněný	k2eAgNnPc1d1	opevněné
města	město	k1gNnPc1	město
<g/>
,	,	kIx,	,
rozvíjela	rozvíjet	k5eAaImAgNnP	rozvíjet
se	se	k3xPyFc4	se
řemesla	řemeslo	k1gNnPc1	řemeslo
a	a	k8xC	a
pěstování	pěstování	k1gNnSc1	pěstování
vína	víno	k1gNnSc2	víno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
6.	[number]	k4	6.
století	století	k1gNnSc6	století
před	před	k7c7	před
n.	n.	k?	n.
l.	l.	k?	l.
Urartská	Urartský	k2eAgFnSc1d1	Urartský
říše	říše	k1gFnSc1	říše
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
po	po	k7c6	po
vpádu	vpád	k1gInSc6	vpád
médských	médský	k2eAgNnPc2d1	médský
vojsk	vojsko	k1gNnPc2	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Malé	Malé	k2eAgFnSc6d1	Malé
Arménii	Arménie	k1gFnSc6	Arménie
a	a	k8xC	a
na	na	k7c6	na
Araratské	Araratský	k2eAgFnSc6d1	Araratský
rovině	rovina	k1gFnSc6	rovina
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
Arménské	arménský	k2eAgNnSc1d1	arménské
království	království	k1gNnSc1	království
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
se	se	k3xPyFc4	se
formovat	formovat	k5eAaImF	formovat
arménský	arménský	k2eAgInSc4d1	arménský
národ	národ	k1gInSc4	národ
<g/>
.	.	kIx.	.
</s>
<s>
Arménské	arménský	k2eAgNnSc1d1	arménské
království	království	k1gNnSc1	království
bylo	být	k5eAaImAgNnS	být
prvním	první	k4xOgInSc7	první
státem	stát	k1gInSc7	stát
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
přijal	přijmout	k5eAaPmAgInS	přijmout
křesťanství	křesťanství	k1gNnSc4	křesťanství
jako	jako	k8xS	jako
své	svůj	k3xOyFgNnSc4	svůj
oficiální	oficiální	k2eAgNnSc4d1	oficiální
náboženství	náboženství	k1gNnSc4	náboženství
(	(	kIx(	(
<g/>
r.	r.	kA	r.
301	[number]	k4	301
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
4.	[number]	k4	4.
století	století	k1gNnSc6	století
si	se	k3xPyFc3	se
Arménii	Arménie	k1gFnSc6	Arménie
rozdělili	rozdělit	k5eAaPmAgMnP	rozdělit
Římané	Říman	k1gMnPc1	Říman
a	a	k8xC	a
Peršané	Peršan	k1gMnPc1	Peršan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Středověk	středověk	k1gInSc1	středověk
===	===	k?	===
</s>
</p>
<p>
<s>
Od	od	k7c2	od
7.	[number]	k4	7.
do	do	k7c2	do
9.	[number]	k4	9.
století	století	k1gNnSc2	století
tehdejší	tehdejší	k2eAgFnSc4d1	tehdejší
Arménii	Arménie	k1gFnSc4	Arménie
ovládali	ovládat	k5eAaImAgMnP	ovládat
Arabové	Arab	k1gMnPc1	Arab
<g/>
.	.	kIx.	.
</s>
<s>
Arménský	arménský	k2eAgInSc1d1	arménský
stát	stát	k1gInSc1	stát
se	se	k3xPyFc4	se
obnovil	obnovit	k5eAaPmAgInS	obnovit
v	v	k7c6	v
9.	[number]	k4	9.
století	století	k1gNnSc6	století
<g/>
.	.	kIx.	.
</s>
<s>
Arméni	Armén	k1gMnPc1	Armén
vynikali	vynikat	k5eAaImAgMnP	vynikat
jako	jako	k9	jako
obchodníci	obchodník	k1gMnPc1	obchodník
<g/>
,	,	kIx,	,
zakládali	zakládat	k5eAaImAgMnP	zakládat
kolonie	kolonie	k1gFnSc2	kolonie
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
Přední	přední	k2eAgFnSc6d1	přední
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
v	v	k7c6	v
Kyjevě	Kyjev	k1gInSc6	Kyjev
a	a	k8xC	a
Novgorodě	Novgorod	k1gInSc6	Novgorod
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
11.	[number]	k4	11.
století	století	k1gNnSc2	století
ovládali	ovládat	k5eAaImAgMnP	ovládat
zemi	zem	k1gFnSc4	zem
seldžučtí	seldžucký	k2eAgMnPc1d1	seldžucký
Turci	Turek	k1gMnPc1	Turek
<g/>
,	,	kIx,	,
od	od	k7c2	od
13.	[number]	k4	13.
století	stoletý	k2eAgMnPc1d1	stoletý
Mongolové	Mongol	k1gMnPc1	Mongol
<g/>
.	.	kIx.	.
</s>
<s>
Arménští	arménský	k2eAgMnPc1d1	arménský
uprchlíci	uprchlík	k1gMnPc1	uprchlík
před	před	k7c7	před
seldžuckou	seldžucký	k2eAgFnSc7d1	seldžucká
invazí	invaze	k1gFnSc7	invaze
založili	založit	k5eAaPmAgMnP	založit
arménské	arménský	k2eAgNnSc4d1	arménské
království	království	k1gNnSc4	království
v	v	k7c6	v
Kilíkii	Kilíkie	k1gFnSc6	Kilíkie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Novověk	novověk	k1gInSc4	novověk
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
V	v	k7c4	v
17.	[number]	k4	17.
století	století	k1gNnPc2	století
byla	být	k5eAaImAgFnS	být
Arménie	Arménie	k1gFnSc1	Arménie
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
mezi	mezi	k7c4	mezi
Osmanskou	osmanský	k2eAgFnSc4d1	Osmanská
říši	říše	k1gFnSc4	říše
(	(	kIx(	(
<g/>
připadla	připadnout	k5eAaPmAgFnS	připadnout
jí	jíst	k5eAaImIp3nS	jíst
západní	západní	k2eAgFnSc1d1	západní
Arménie	Arménie	k1gFnSc1	Arménie
<g/>
)	)	kIx)	)
a	a	k8xC	a
Persii	Persie	k1gFnSc6	Persie
(	(	kIx(	(
<g/>
východní	východní	k2eAgFnSc2d1	východní
Arménie	Arménie	k1gFnSc2	Arménie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgFnSc4d1	národní
identitu	identita	k1gFnSc4	identita
Arménům	Armén	k1gMnPc3	Armén
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
pomohla	pomoct	k5eAaPmAgFnS	pomoct
udržet	udržet	k5eAaPmF	udržet
jejich	jejich	k3xOp3gFnSc1	jejich
kultura	kultura	k1gFnSc1	kultura
<g/>
,	,	kIx,	,
pěstovaná	pěstovaný	k2eAgFnSc1d1	pěstovaná
převážně	převážně	k6eAd1	převážně
emigranty	emigrant	k1gMnPc7	emigrant
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
začátkem	začátkem	k7c2	začátkem
19.	[number]	k4	19.
století	století	k1gNnPc2	století
připadla	připadnout	k5eAaPmAgFnS	připadnout
část	část	k1gFnSc1	část
východní	východní	k2eAgFnSc2d1	východní
Arménie	Arménie	k1gFnSc2	Arménie
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
východní	východní	k2eAgFnSc7d1	východní
Gruzií	Gruzie	k1gFnSc7	Gruzie
carskému	carský	k2eAgNnSc3d1	carské
Rusku	Rusko	k1gNnSc3	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Ruská	ruský	k2eAgNnPc1d1	ruské
vojska	vojsko	k1gNnPc1	vojsko
a	a	k8xC	a
arménští	arménský	k2eAgMnPc1d1	arménský
dobrovolníci	dobrovolník	k1gMnPc1	dobrovolník
roku	rok	k1gInSc2	rok
1827	[number]	k4	1827
ovládli	ovládnout	k5eAaPmAgMnP	ovládnout
celou	celá	k1gFnSc4	celá
východní	východní	k2eAgFnSc3d1	východní
Arménii	Arménie	k1gFnSc3	Arménie
<g/>
.	.	kIx.	.
</s>
<s>
Armény	Armén	k1gMnPc7	Armén
žijící	žijící	k2eAgNnSc1d1	žijící
v	v	k7c4	v
Osmany	Osman	k1gMnPc4	Osman
spravované	spravovaný	k2eAgInPc1d1	spravovaný
západní	západní	k2eAgFnSc4d1	západní
Arménii	Arménie	k1gFnSc4	Arménie
postihl	postihnout	k5eAaPmAgInS	postihnout
těžký	těžký	k2eAgInSc1d1	těžký
osud	osud	k1gInSc1	osud
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1890	[number]	k4	1890
<g/>
–	–	k?	–
<g/>
1922	[number]	k4	1922
byli	být	k5eAaImAgMnP	být
systematicky	systematicky	k6eAd1	systematicky
vyvražďováni	vyvražďován	k2eAgMnPc1d1	vyvražďován
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
19.	[number]	k4	19.
století	století	k1gNnSc2	století
nastaly	nastat	k5eAaPmAgFnP	nastat
tzv.	tzv.	kA	tzv.
arménské	arménský	k2eAgFnSc2d1	arménská
řeže	řež	k1gFnSc2	řež
<g/>
,	,	kIx,	,
při	při	k7c6	při
nichž	jenž	k3xRgNnPc6	jenž
přišlo	přijít	k5eAaPmAgNnS	přijít
o	o	k7c4	o
život	život	k1gInSc4	život
100	[number]	k4	100
<g/>
–	–	k?	–
<g/>
300	[number]	k4	300
tisíc	tisíc	k4xCgInPc2	tisíc
Arménů	Armén	k1gMnPc2	Armén
<g/>
,	,	kIx,	,
při	při	k7c6	při
genocidě	genocida	k1gFnSc6	genocida
v	v	k7c6	v
době	doba	k1gFnSc6	doba
1.	[number]	k4	1.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
pak	pak	k6eAd1	pak
přišlo	přijít	k5eAaPmAgNnS	přijít
o	o	k7c4	o
život	život	k1gInSc4	život
zhruba	zhruba	k6eAd1	zhruba
1,5	[number]	k4	1,5
miliónu	milión	k4xCgInSc2	milión
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
dalších	další	k2eAgFnPc2d1	další
100	[number]	k4	100
tisíc	tisíc	k4xCgInPc2	tisíc
uteklo	utéct	k5eAaPmAgNnS	utéct
před	před	k7c7	před
pronásledováním	pronásledování	k1gNnSc7	pronásledování
do	do	k7c2	do
Ruska	Rusko	k1gNnSc2	Rusko
a	a	k8xC	a
jiných	jiný	k2eAgFnPc2d1	jiná
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Rozvíjela	rozvíjet	k5eAaImAgFnS	rozvíjet
se	se	k3xPyFc4	se
fakticky	fakticky	k6eAd1	fakticky
už	už	k6eAd1	už
jen	jen	k9	jen
východní	východní	k2eAgFnSc2d1	východní
Arménie	Arménie	k1gFnSc2	Arménie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
Arménie	Arménie	k1gFnSc1	Arménie
dostala	dostat	k5eAaPmAgFnS	dostat
na	na	k7c4	na
nějakou	nějaký	k3yIgFnSc4	nějaký
dobu	doba	k1gFnSc4	doba
pod	pod	k7c4	pod
správu	správa	k1gFnSc4	správa
států	stát	k1gInPc2	stát
Dohody	dohoda	k1gFnSc2	dohoda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Arménie	Arménie	k1gFnSc2	Arménie
v	v	k7c6	v
SSSR	SSSR	kA	SSSR
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
začalo	začít	k5eAaPmAgNnS	začít
komunisty	komunista	k1gMnPc4	komunista
podporované	podporovaný	k2eAgNnSc1d1	podporované
povstání	povstání	k1gNnSc1	povstání
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
vyhnalo	vyhnat	k5eAaPmAgNnS	vyhnat
zahraniční	zahraniční	k2eAgFnSc7d1	zahraniční
vojska	vojsko	k1gNnPc4	vojsko
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
na	na	k7c4	na
svoje	svůj	k3xOyFgNnSc4	svůj
území	území	k1gNnSc1	území
přivolalo	přivolat	k5eAaPmAgNnS	přivolat
Rudou	rudý	k2eAgFnSc4d1	rudá
armádu	armáda	k1gFnSc4	armáda
<g/>
.	.	kIx.	.
29.	[number]	k4	29.
listopadu	listopad	k1gInSc2	listopad
1920	[number]	k4	1920
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
sovětská	sovětský	k2eAgFnSc1d1	sovětská
vláda	vláda	k1gFnSc1	vláda
a	a	k8xC	a
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
Arménie	Arménie	k1gFnSc1	Arménie
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
do	do	k7c2	do
Zakavkazské	zakavkazský	k2eAgFnSc2d1	zakavkazská
federace	federace	k1gFnSc2	federace
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	s	k7c7	s
členem	člen	k1gMnSc7	člen
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
<s>
Arménsko-tureckou	arménskourecký	k2eAgFnSc4d1	arménsko-turecký
hranici	hranice	k1gFnSc4	hranice
<g/>
,	,	kIx,	,
dodnes	dodnes	k6eAd1	dodnes
platnou	platný	k2eAgFnSc4d1	platná
<g/>
,	,	kIx,	,
stanovila	stanovit	k5eAaPmAgFnS	stanovit
tzv.	tzv.	kA	tzv.
Karská	karský	k2eAgFnSc1d1	Karská
smlouva	smlouva	k1gFnSc1	smlouva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Sovětskou	sovětský	k2eAgFnSc4d1	sovětská
Arménii	Arménie	k1gFnSc4	Arménie
postihlo	postihnout	k5eAaPmAgNnS	postihnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
silné	silný	k2eAgNnSc1d1	silné
zemětřesení	zemětřesení	k1gNnPc1	zemětřesení
o	o	k7c6	o
síle	síla	k1gFnSc6	síla
7,2	[number]	k4	7,2
stupně	stupeň	k1gInSc2	stupeň
Richterovy	Richterův	k2eAgFnSc2d1	Richterova
stupnice	stupnice	k1gFnSc2	stupnice
<g/>
,	,	kIx,	,
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
kterého	který	k3yQgInSc2	který
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
nejméně	málo	k6eAd3	málo
50 000	[number]	k4	50 000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Humanitární	humanitární	k2eAgFnSc1d1	humanitární
pomoci	pomoct	k5eAaPmF	pomoct
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgFnPc7d1	další
zeměmi	zem	k1gFnPc7	zem
<g/>
,	,	kIx,	,
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
i	i	k9	i
Československo	Československo	k1gNnSc1	Československo
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
pomocí	pomoc	k1gFnSc7	pomoc
jsou	být	k5eAaImIp3nP	být
spojeny	spojit	k5eAaPmNgInP	spojit
počátky	počátek	k1gInPc1	počátek
české	český	k2eAgFnSc2d1	Česká
humanitární	humanitární	k2eAgFnSc2d1	humanitární
organizace	organizace	k1gFnSc2	organizace
Člověk	člověk	k1gMnSc1	člověk
v	v	k7c6	v
tísni	tíseň	k1gFnSc6	tíseň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Spor	spor	k1gInSc1	spor
o	o	k7c4	o
Náhorní	náhorní	k2eAgInSc4d1	náhorní
Karabach	Karabach	k1gInSc4	Karabach
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
nesouhlasil	souhlasit	k5eNaImAgInS	souhlasit
sovětský	sovětský	k2eAgInSc1d1	sovětský
Ázerbájdžán	Ázerbájdžán	k1gInSc1	Ázerbájdžán
s	s	k7c7	s
připojením	připojení	k1gNnSc7	připojení
Náhorního	náhorní	k2eAgInSc2d1	náhorní
Karabachu	Karabach	k1gInSc2	Karabach
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
většiny	většina	k1gFnSc2	většina
osídlen	osídlit	k5eAaPmNgMnS	osídlit
Armény	Armén	k1gMnPc7	Armén
<g/>
,	,	kIx,	,
k	k	k7c3	k
sovětské	sovětský	k2eAgFnSc3d1	sovětská
Arménii	Arménie	k1gFnSc3	Arménie
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
byl	být	k5eAaImAgInS	být
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Nachičevanem	Nachičevan	k1gMnSc7	Nachičevan
roku	rok	k1gInSc2	rok
1923	[number]	k4	1923
připojen	připojit	k5eAaPmNgInS	připojit
k	k	k7c3	k
Ázerbájdžánu	Ázerbájdžán	k1gInSc3	Ázerbájdžán
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
sovětské	sovětský	k2eAgFnSc2d1	sovětská
éry	éra	k1gFnSc2	éra
Arménie	Arménie	k1gFnSc2	Arménie
bezúspěšně	bezúspěšně	k6eAd1	bezúspěšně
usilovala	usilovat	k5eAaImAgFnS	usilovat
o	o	k7c4	o
připojení	připojení	k1gNnSc4	připojení
Karabachu	Karabach	k1gInSc2	Karabach
ke	k	k7c3	k
svému	svůj	k3xOyFgNnSc3	svůj
území	území	k1gNnSc3	území
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
se	se	k3xPyFc4	se
zformovalo	zformovat	k5eAaPmAgNnS	zformovat
arménské	arménský	k2eAgNnSc1d1	arménské
hnutí	hnutí	k1gNnSc1	hnutí
za	za	k7c4	za
připojení	připojení	k1gNnSc4	připojení
k	k	k7c3	k
Arménii	Arménie	k1gFnSc3	Arménie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
t	t	k?	t
<g/>
.	.	kIx.	.
<g/>
r	r	kA	r
<g/>
.	.	kIx.	.
pak	pak	k6eAd1	pak
Ázerbájdžán	Ázerbájdžán	k1gInSc1	Ázerbájdžán
zakázal	zakázat	k5eAaPmAgInS	zakázat
výuku	výuka	k1gFnSc4	výuka
arménských	arménský	k2eAgFnPc2d1	arménská
dějin	dějiny	k1gFnPc2	dějiny
na	na	k7c6	na
karabašských	karabašský	k2eAgFnPc6d1	karabašská
školách	škola	k1gFnPc6	škola
<g/>
.	.	kIx.	.
</s>
<s>
Snahy	snaha	k1gFnPc1	snaha
Moskvy	Moskva	k1gFnSc2	Moskva
o	o	k7c6	o
urovnání	urovnání	k1gNnSc6	urovnání
konfliktu	konflikt	k1gInSc2	konflikt
byly	být	k5eAaImAgFnP	být
neúspěšné	úspěšný	k2eNgFnPc1d1	neúspěšná
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
byl	být	k5eAaImAgInS	být
Náhorní	náhorní	k2eAgInSc1d1	náhorní
Karabach	Karabach	k1gInSc1	Karabach
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1988	[number]	k4	1988
podřízen	podřídit	k5eAaPmNgInS	podřídit
přímo	přímo	k6eAd1	přímo
ústřední	ústřední	k2eAgFnSc3d1	ústřední
vládě	vláda	k1gFnSc3	vláda
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
postupem	postup	k1gInSc7	postup
nesouhlasila	souhlasit	k5eNaImAgFnS	souhlasit
ani	ani	k9	ani
jedna	jeden	k4xCgFnSc1	jeden
strana	strana	k1gFnSc1	strana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
začaly	začít	k5eAaPmAgInP	začít
boje	boj	k1gInPc1	boj
mezi	mezi	k7c7	mezi
Arménií	Arménie	k1gFnSc7	Arménie
a	a	k8xC	a
Ázerbájdžánem	Ázerbájdžán	k1gInSc7	Ázerbájdžán
o	o	k7c4	o
tuto	tento	k3xDgFnSc4	tento
oblast	oblast	k1gFnSc4	oblast
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
vyostřily	vyostřit	k5eAaPmAgInP	vyostřit
po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
<s>
Turecko	Turecko	k1gNnSc1	Turecko
na	na	k7c4	na
protest	protest	k1gInSc4	protest
proti	proti	k7c3	proti
obsazení	obsazení	k1gNnSc3	obsazení
Náhorního	náhorní	k2eAgInSc2d1	náhorní
Karabachu	Karabach	k1gInSc2	Karabach
uzavřelo	uzavřít	k5eAaPmAgNnS	uzavřít
arménské	arménský	k2eAgFnPc4d1	arménská
hranice	hranice	k1gFnPc4	hranice
<g/>
.	.	kIx.	.
28.	[number]	k4	28.
listopadu	listopad	k1gInSc2	listopad
1989	[number]	k4	1989
byl	být	k5eAaImAgInS	být
vrácen	vrátit	k5eAaPmNgInS	vrátit
pod	pod	k7c4	pod
správu	správa	k1gFnSc4	správa
Ázerbájdžánu	Ázerbájdžán	k1gInSc2	Ázerbájdžán
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Arménie	Arménie	k1gFnPc1	Arménie
ho	on	k3xPp3gNnSc2	on
hned	hned	k6eAd1	hned
1.	[number]	k4	1.
prosince	prosinec	k1gInSc2	prosinec
anektovala	anektovat	k5eAaBmAgNnP	anektovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1991	[number]	k4	1991
až	až	k9	až
1994	[number]	k4	1994
probíhala	probíhat	k5eAaImAgFnS	probíhat
o	o	k7c4	o
karabašské	karabašský	k2eAgNnSc4d1	karabašský
území	území	k1gNnSc4	území
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgFnPc7	dva
zeměmi	zem	k1gFnPc7	zem
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
Dočasným	dočasný	k2eAgNnSc7d1	dočasné
východiskem	východisko	k1gNnSc7	východisko
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
příměří	příměří	k1gNnSc1	příměří
z	z	k7c2	z
16.	[number]	k4	16.
května	květen	k1gInSc2	květen
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
v	v	k7c6	v
platnosti	platnost	k1gFnSc6	platnost
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
je	být	k5eAaImIp3nS	být
Karabach	Karabach	k1gInSc1	Karabach
z	z	k7c2	z
92,5	[number]	k4	92,5
%	%	kIx~	%
ovládaný	ovládaný	k2eAgMnSc1d1	ovládaný
arménsko	arménsko	k6eAd1	arménsko
<g/>
–	–	k?	–
<g/>
karabašskými	karabašský	k2eAgFnPc7d1	karabašská
silami	síla	k1gFnPc7	síla
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
pod	pod	k7c7	pod
kontrolou	kontrola	k1gFnSc7	kontrola
také	také	k6eAd1	také
dalších	další	k2eAgInPc2d1	další
5	[number]	k4	5
celých	celý	k2eAgInPc2d1	celý
okresů	okres	k1gInPc2	okres
(	(	kIx(	(
<g/>
a	a	k8xC	a
2	[number]	k4	2
částečně	částečně	k6eAd1	částečně
<g/>
)	)	kIx)	)
mimo	mimo	k7c4	mimo
vlastní	vlastní	k2eAgInSc4d1	vlastní
Karabach	Karabach	k1gInSc4	Karabach
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
tak	tak	k9	tak
karabašští	karabašský	k2eAgMnPc1d1	karabašský
Arméni	Armén	k1gMnPc1	Armén
kontrolují	kontrolovat	k5eAaImIp3nP	kontrolovat
13,4	[number]	k4	13,4
%	%	kIx~	%
rozlohy	rozloha	k1gFnSc2	rozloha
Ázerbájdžánu	Ázerbájdžán	k1gInSc2	Ázerbájdžán
<g/>
.	.	kIx.	.
</s>
<s>
Vojenské	vojenský	k2eAgInPc1d1	vojenský
zásahy	zásah	k1gInPc1	zásah
byly	být	k5eAaImAgInP	být
doprovázeny	doprovázet	k5eAaImNgInP	doprovázet
exodem	exodus	k1gInSc7	exodus
azerských	azerský	k2eAgFnPc2d1	azerský
a	a	k8xC	a
arménských	arménský	k2eAgFnPc2d1	arménská
menšin	menšina	k1gFnPc2	menšina
z	z	k7c2	z
území	území	k1gNnSc2	území
sousedního	sousední	k2eAgInSc2d1	sousední
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Ázerbájdžánu	Ázerbájdžán	k1gInSc2	Ázerbájdžán
odešlo	odejít	k5eAaPmAgNnS	odejít
413 000	[number]	k4	413 000
Arménů	Armén	k1gMnPc2	Armén
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Arménii	Arménie	k1gFnSc4	Arménie
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
konfliktu	konflikt	k1gInSc2	konflikt
opustilo	opustit	k5eAaPmAgNnS	opustit
724 000	[number]	k4	724 000
Azerů	Azer	k1gMnPc2	Azer
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Nezávislá	závislý	k2eNgFnSc1d1	nezávislá
Arménie	Arménie	k1gFnSc1	Arménie
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
získala	získat	k5eAaPmAgFnS	získat
Arménie	Arménie	k1gFnSc1	Arménie
nezávislost	nezávislost	k1gFnSc4	nezávislost
na	na	k7c6	na
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	s	k7c7	s
součástí	součást	k1gFnSc7	součást
SNS	SNS	kA	SNS
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
byl	být	k5eAaImAgMnS	být
při	při	k7c6	při
zasedání	zasedání	k1gNnSc6	zasedání
parlamentu	parlament	k1gInSc2	parlament
zavražděn	zavražděn	k2eAgMnSc1d1	zavražděn
premiér	premiér	k1gMnSc1	premiér
Vazgen	Vazgen	k1gInSc4	Vazgen
Sarkisjan	Sarkisjany	k1gInPc2	Sarkisjany
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
následně	následně	k6eAd1	následně
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
částečné	částečný	k2eAgFnSc3d1	částečná
ekonomické	ekonomický	k2eAgFnSc3d1	ekonomická
i	i	k8xC	i
společenské	společenský	k2eAgFnSc3d1	společenská
krizi	krize	k1gFnSc3	krize
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Území	území	k1gNnSc1	území
Arménie	Arménie	k1gFnSc2	Arménie
má	mít	k5eAaImIp3nS	mít
plochu	plocha	k1gFnSc4	plocha
29 743	[number]	k4	29 743
kilometrů	kilometr	k1gInPc2	kilometr
čtverečních	čtvereční	k2eAgInPc2d1	čtvereční
<g/>
.	.	kIx.	.
</s>
<s>
Krajina	Krajina	k1gFnSc1	Krajina
je	být	k5eAaImIp3nS	být
hornatá	hornatý	k2eAgFnSc1d1	hornatá
<g/>
,	,	kIx,	,
nadmořská	nadmořský	k2eAgFnSc1d1	nadmořská
výška	výška	k1gFnSc1	výška
na	na	k7c6	na
celém	celý	k2eAgNnSc6d1	celé
území	území	k1gNnSc6	území
neklesne	klesnout	k5eNaPmIp3nS	klesnout
pod	pod	k7c4	pod
390	[number]	k4	390
metrů	metr	k1gInPc2	metr
nad	nad	k7c7	nad
mořem	moře	k1gNnSc7	moře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
Arménie	Arménie	k1gFnSc2	Arménie
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
horské	horský	k2eAgNnSc4d1	horské
pásmo	pásmo	k1gNnSc4	pásmo
Malého	Malého	k2eAgInSc2d1	Malého
Kavkazu	Kavkaz	k1gInSc2	Kavkaz
a	a	k8xC	a
vysokohorské	vysokohorský	k2eAgNnSc4d1	vysokohorské
pohoří	pohoří	k1gNnSc4	pohoří
Ararat	Ararat	k1gMnSc1	Ararat
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
také	také	k9	také
nachází	nacházet	k5eAaImIp3nS	nacházet
oficiální	oficiální	k2eAgFnSc1d1	oficiální
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
Arménie	Arménie	k1gFnSc2	Arménie
(	(	kIx(	(
<g/>
Aragac	Aragac	k1gInSc1	Aragac
<g/>
,	,	kIx,	,
4090	[number]	k4	4090
m	m	kA	m
n	n	k0	n
<g/>
.	.	kIx.	.
m	m	kA	m
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Arméni	Armén	k1gMnPc1	Armén
však	však	k9	však
za	za	k7c4	za
svoji	svůj	k3xOyFgFnSc4	svůj
považují	považovat	k5eAaImIp3nP	považovat
o	o	k7c4	o
biblickou	biblický	k2eAgFnSc4d1	biblická
horu	hora	k1gFnSc4	hora
Ararat	Ararat	k1gInSc1	Ararat
(	(	kIx(	(
<g/>
5134	[number]	k4	5134
m	m	kA	m
n	n	k0	n
<g/>
.	.	kIx.	.
m	m	kA	m
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc7	který
mají	mít	k5eAaImIp3nP	mít
dokonce	dokonce	k9	dokonce
ve	v	k7c6	v
státním	státní	k2eAgInSc6d1	státní
znaku	znak	k1gInSc6	znak
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
však	však	k9	však
leží	ležet	k5eAaImIp3nS	ležet
asi	asi	k9	asi
kilometr	kilometr	k1gInSc4	kilometr
od	od	k7c2	od
hranic	hranice	k1gFnPc2	hranice
v	v	k7c6	v
muslimském	muslimský	k2eAgNnSc6d1	muslimské
Turecku	Turecko	k1gNnSc6	Turecko
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tisíciletí	tisíciletí	k1gNnSc6	tisíciletí
obývali	obývat	k5eAaImAgMnP	obývat
území	území	k1gNnSc4	území
kolem	kolem	k7c2	kolem
Araratu	Ararat	k1gInSc2	Ararat
právě	právě	k9	právě
Arméni	Armén	k1gMnPc1	Armén
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
po	po	k7c6	po
genocidě	genocida	k1gFnSc3	genocida
osmanskými	osmanský	k2eAgInPc7d1	osmanský
Turky	turek	k1gInPc7	turek
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1913	[number]	k4	1913
<g/>
–	–	k?	–
<g/>
1920	[number]	k4	1920
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
kdysi	kdysi	k6eAd1	kdysi
velké	velký	k2eAgFnSc3d1	velká
Arménii	Arménie	k1gFnSc3	Arménie
pouze	pouze	k6eAd1	pouze
malé	malý	k2eAgNnSc4d1	malé
území	území	k1gNnSc4	území
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
největší	veliký	k2eAgFnSc2d3	veliký
územní	územní	k2eAgFnSc2d1	územní
expanze	expanze	k1gFnSc2	expanze
v	v	k7c4	v
1.	[number]	k4	1.
století	století	k1gNnPc2	století
př.	př.	kA	př.
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
arménské	arménský	k2eAgNnSc1d1	arménské
království	království	k1gNnSc1	království
rozkládalo	rozkládat	k5eAaImAgNnS	rozkládat
od	od	k7c2	od
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
až	až	k9	až
ke	k	k7c3	k
Kaspickému	kaspický	k2eAgNnSc3d1	Kaspické
moři	moře	k1gNnSc3	moře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Klima	klima	k1gNnSc1	klima
je	být	k5eAaImIp3nS	být
převážně	převážně	k6eAd1	převážně
kontinentální	kontinentální	k2eAgFnSc4d1	kontinentální
<g/>
,	,	kIx,	,
vyznačující	vyznačující	k2eAgFnSc4d1	vyznačující
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
horkými	horký	k2eAgNnPc7d1	horké
léty	léto	k1gNnPc7	léto
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
chladnými	chladný	k2eAgFnPc7d1	chladná
zimami	zima	k1gFnPc7	zima
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
suchému	suchý	k2eAgNnSc3d1	suché
létu	léto	k1gNnSc3	léto
zde	zde	k6eAd1	zde
převládají	převládat	k5eAaImIp3nP	převládat
polopouštní	polopouštní	k2eAgFnPc1d1	polopouštní
a	a	k8xC	a
stepní	stepní	k2eAgFnPc1d1	stepní
vegetace	vegetace	k1gFnPc1	vegetace
<g/>
;	;	kIx,	;
řídké	řídký	k2eAgInPc1d1	řídký
lesy	les	k1gInPc1	les
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
pouze	pouze	k6eAd1	pouze
sedminu	sedmina	k1gFnSc4	sedmina
území	území	k1gNnSc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
hornaté	hornatý	k2eAgFnSc3d1	hornatá
krajině	krajina	k1gFnSc3	krajina
je	být	k5eAaImIp3nS	být
rozšiřování	rozšiřování	k1gNnSc1	rozšiřování
zemědělských	zemědělský	k2eAgFnPc2d1	zemědělská
oblastí	oblast	k1gFnPc2	oblast
obtížné	obtížný	k2eAgFnPc1d1	obtížná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
zemi	zem	k1gFnSc4	zem
postihlo	postihnout	k5eAaPmAgNnS	postihnout
zemětřesení	zemětřesení	k1gNnSc1	zemětřesení
o	o	k7c6	o
síle	síla	k1gFnSc6	síla
7,2	[number]	k4	7,2
stupně	stupeň	k1gInSc2	stupeň
magnitudy	magnituda	k1gFnSc2	magnituda
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
připravilo	připravit	k5eAaPmAgNnS	připravit
o	o	k7c4	o
život	život	k1gInSc4	život
více	hodně	k6eAd2	hodně
než	než	k8xS	než
50 000	[number]	k4	50 000
lidí	člověk	k1gMnPc2	člověk
především	především	k6eAd1	především
ve	v	k7c6	v
městech	město	k1gNnPc6	město
Spitak	Spitak	k1gInSc1	Spitak
<g/>
,	,	kIx,	,
Leninakan	Leninakan	k1gInSc1	Leninakan
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Gjumri	Gjumri	k1gNnSc1	Gjumri
<g/>
)	)	kIx)	)
a	a	k8xC	a
Kirovakan	Kirovakan	k1gInSc4	Kirovakan
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Vanadzor	Vanadzor	k1gInSc1	Vanadzor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Politický	politický	k2eAgInSc1d1	politický
systém	systém	k1gInSc1	systém
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Arménie	Arménie	k1gFnSc1	Arménie
je	být	k5eAaImIp3nS	být
poloprezidentská	poloprezidentský	k2eAgFnSc1d1	poloprezidentská
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Jednokomorový	jednokomorový	k2eAgInSc1d1	jednokomorový
parlament	parlament	k1gInSc1	parlament
(	(	kIx(	(
<g/>
Národní	národní	k2eAgNnSc1d1	národní
shromáždění	shromáždění	k1gNnSc1	shromáždění
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
131	[number]	k4	131
křesel	křeslo	k1gNnPc2	křeslo
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
volen	volit	k5eAaImNgInS	volit
na	na	k7c4	na
5	[number]	k4	5
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
špatným	špatný	k2eAgFnPc3d1	špatná
historickým	historický	k2eAgFnPc3d1	historická
zkušenostem	zkušenost	k1gFnPc3	zkušenost
má	mít	k5eAaImIp3nS	mít
Arménie	Arménie	k1gFnSc2	Arménie
komplikované	komplikovaný	k2eAgInPc4d1	komplikovaný
vztahy	vztah	k1gInPc4	vztah
se	s	k7c7	s
dvěma	dva	k4xCgInPc7	dva
sousedy	soused	k1gMnPc7	soused
<g/>
,	,	kIx,	,
Tureckem	Turecko	k1gNnSc7	Turecko
a	a	k8xC	a
Ázerbájdžánem	Ázerbájdžán	k1gInSc7	Ázerbájdžán
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vede	vést	k5eAaImIp3nS	vést
arménskou	arménský	k2eAgFnSc4d1	arménská
zahraniční	zahraniční	k2eAgFnSc4d1	zahraniční
politiku	politika	k1gFnSc4	politika
k	k	k7c3	k
udržování	udržování	k1gNnSc3	udržování
úzkých	úzký	k2eAgInPc2d1	úzký
vztahů	vztah	k1gInPc2	vztah
s	s	k7c7	s
Ruskem	Rusko	k1gNnSc7	Rusko
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
garantuje	garantovat	k5eAaBmIp3nS	garantovat
arménskou	arménský	k2eAgFnSc4d1	arménská
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
<g/>
.	.	kIx.	.
</s>
<s>
Arménie	Arménie	k1gFnSc1	Arménie
se	se	k3xPyFc4	se
připojila	připojit	k5eAaPmAgFnS	připojit
k	k	k7c3	k
celní	celní	k2eAgFnSc3d1	celní
unii	unie	k1gFnSc3	unie
Běloruska	Bělorusko	k1gNnSc2	Bělorusko
<g/>
,	,	kIx,	,
Kazachstánu	Kazachstán	k1gInSc2	Kazachstán
a	a	k8xC	a
Ruska	Rusko	k1gNnSc2	Rusko
a	a	k8xC	a
k	k	k7c3	k
Eurasijské	Eurasijský	k2eAgFnSc3d1	Eurasijská
hospodářské	hospodářský	k2eAgFnSc3d1	hospodářská
unii	unie	k1gFnSc3	unie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
sympatizuje	sympatizovat	k5eAaImIp3nS	sympatizovat
Arménie	Arménie	k1gFnSc1	Arménie
také	také	k9	také
se	s	k7c7	s
západním	západní	k2eAgInSc7d1	západní
světem	svět	k1gInSc7	svět
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nP	snažit
se	se	k3xPyFc4	se
budovat	budovat	k5eAaImF	budovat
dobré	dobrý	k2eAgInPc4d1	dobrý
vztahy	vztah	k1gInPc4	vztah
s	s	k7c7	s
EU	EU	kA	EU
a	a	k8xC	a
NATO	NATO	kA	NATO
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
začala	začít	k5eAaPmAgFnS	začít
jednání	jednání	k1gNnSc4	jednání
o	o	k7c4	o
partnerství	partnerství	k1gNnSc4	partnerství
s	s	k7c7	s
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
<s>
Arménie	Arménie	k1gFnSc1	Arménie
se	se	k3xPyFc4	se
podílela	podílet	k5eAaImAgFnS	podílet
na	na	k7c4	na
operaci	operace	k1gFnSc4	operace
NATO	NATO	kA	NATO
v	v	k7c4	v
Kosovu	Kosův	k2eAgFnSc4d1	Kosova
(	(	kIx(	(
<g/>
KFOR	KFOR	kA	KFOR
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tradičním	tradiční	k2eAgMnSc7d1	tradiční
spojencem	spojenec	k1gMnSc7	spojenec
Arménie	Arménie	k1gFnSc2	Arménie
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
jsou	být	k5eAaImIp3nP	být
Francie	Francie	k1gFnSc1	Francie
a	a	k8xC	a
Řecko	Řecko	k1gNnSc1	Řecko
<g/>
;	;	kIx,	;
v	v	k7c6	v
obou	dva	k4xCgFnPc6	dva
zemích	zem	k1gFnPc6	zem
žije	žít	k5eAaImIp3nS	žít
velká	velký	k2eAgFnSc1d1	velká
arménská	arménský	k2eAgFnSc1d1	arménská
komunita	komunita	k1gFnSc1	komunita
<g/>
.	.	kIx.	.
</s>
<s>
Spolupráce	spolupráce	k1gFnSc1	spolupráce
Arménie	Arménie	k1gFnSc2	Arménie
s	s	k7c7	s
Ruskem	Rusko	k1gNnSc7	Rusko
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
v	v	k7c6	v
západním	západní	k2eAgInSc6d1	západní
světě	svět	k1gInSc6	svět
přijímána	přijímat	k5eAaImNgFnS	přijímat
s	s	k7c7	s
nevůlí	nevůle	k1gFnSc7	nevůle
<g/>
.	.	kIx.	.
</s>
<s>
Organizace	organizace	k1gFnPc4	organizace
Freedom	Freedom	k1gInSc4	Freedom
House	house	k1gNnSc1	house
<g/>
,	,	kIx,	,
financovaná	financovaný	k2eAgNnPc4d1	financované
přímo	přímo	k6eAd1	přímo
americkou	americký	k2eAgFnSc7d1	americká
vládou	vláda	k1gFnSc7	vláda
<g/>
,	,	kIx,	,
označila	označit	k5eAaPmAgFnS	označit
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
Arménii	Arménie	k1gFnSc4	Arménie
jen	jen	k6eAd1	jen
za	za	k7c4	za
"	"	kIx"	"
<g/>
částečně	částečně	k6eAd1	částečně
svobodnou	svobodný	k2eAgFnSc4d1	svobodná
zemi	zem	k1gFnSc4	zem
<g/>
"	"	kIx"	"
a	a	k8xC	a
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
provokačně	provokačně	k6eAd1	provokačně
dokonce	dokonce	k9	dokonce
za	za	k7c4	za
poloautoritářský	poloautoritářský	k2eAgInSc4d1	poloautoritářský
režim	režim	k1gInSc4	režim
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
volby	volba	k1gFnPc1	volba
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
Republikánská	republikánský	k2eAgFnSc1d1	republikánská
strana	strana	k1gFnSc1	strana
Arménie	Arménie	k1gFnSc2	Arménie
(	(	kIx(	(
<g/>
Hayastani	Hayastan	k1gMnPc1	Hayastan
Hanrapetakan	Hanrapetakan	k1gMnSc1	Hanrapetakan
Kusaktsutyun	Kusaktsutyun	k1gMnSc1	Kusaktsutyun
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
nejvlivnější	vlivný	k2eAgFnSc1d3	nejvlivnější
síla	síla	k1gFnSc1	síla
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
také	také	k9	také
vždy	vždy	k6eAd1	vždy
sestavovala	sestavovat	k5eAaImAgFnS	sestavovat
vládu	vláda	k1gFnSc4	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
označována	označovat	k5eAaImNgFnS	označovat
za	za	k7c4	za
nacionálně-konzervativní	nacionálněonzervativní	k2eAgNnSc4d1	nacionálně-konzervativní
<g/>
.	.	kIx.	.
</s>
<s>
Hlásí	hlásit	k5eAaImIp3nS	hlásit
se	se	k3xPyFc4	se
k	k	k7c3	k
ideologii	ideologie	k1gFnSc3	ideologie
zvané	zvaný	k2eAgInPc4d1	zvaný
tseghakronismus	tseghakronismus	k1gInSc4	tseghakronismus
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zdůrazňuje	zdůrazňovat	k5eAaImIp3nS	zdůrazňovat
spjatost	spjatost	k1gFnSc4	spjatost
arménské	arménský	k2eAgFnSc2d1	arménská
identity	identita	k1gFnSc2	identita
s	s	k7c7	s
náboženstvím	náboženství	k1gNnSc7	náboženství
a	a	k8xC	a
církví	církev	k1gFnPc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgMnSc7d1	hlavní
tvůrcem	tvůrce	k1gMnSc7	tvůrce
této	tento	k3xDgFnSc2	tento
ideologie	ideologie	k1gFnSc2	ideologie
byl	být	k5eAaImAgMnS	být
politik	politik	k1gMnSc1	politik
Garegin	Garegin	k1gMnSc1	Garegin
Nzhdeh	Nzhdeh	k1gMnSc1	Nzhdeh
<g/>
,	,	kIx,	,
klíčová	klíčový	k2eAgFnSc1d1	klíčová
postava	postava	k1gFnSc1	postava
první	první	k4xOgFnSc2	první
arménské	arménský	k2eAgFnSc2d1	arménská
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
<g/>
–	–	k?	–
<g/>
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
antikomunismus	antikomunismus	k1gInSc1	antikomunismus
jej	on	k3xPp3gMnSc4	on
ovšem	ovšem	k9	ovšem
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
přivedl	přivést	k5eAaPmAgInS	přivést
ke	k	k7c3	k
spolupráci	spolupráce	k1gFnSc3	spolupráce
s	s	k7c7	s
německými	německý	k2eAgMnPc7d1	německý
nacisty	nacista	k1gMnPc7	nacista
<g/>
.	.	kIx.	.
</s>
<s>
Tseghakronismus	Tseghakronismus	k1gInSc1	Tseghakronismus
je	být	k5eAaImIp3nS	být
i	i	k9	i
kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
někdy	někdy	k6eAd1	někdy
označován	označovat	k5eAaImNgInS	označovat
za	za	k7c4	za
rasistickou	rasistický	k2eAgFnSc4d1	rasistická
ideologii	ideologie	k1gFnSc4	ideologie
<g/>
.	.	kIx.	.
</s>
<s>
Výhrady	výhrada	k1gFnPc4	výhrada
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
mají	mít	k5eAaImIp3nP	mít
i	i	k9	i
někteří	některý	k3yIgMnPc1	některý
představitelé	představitel	k1gMnPc1	představitel
Republikánské	republikánský	k2eAgFnSc2d1	republikánská
strany	strana	k1gFnSc2	strana
Arménie	Arménie	k1gFnSc2	Arménie
<g/>
,	,	kIx,	,
formuloval	formulovat	k5eAaImAgMnS	formulovat
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
arménský	arménský	k2eAgMnSc1d1	arménský
premiér	premiér	k1gMnSc1	premiér
Andranik	Andranik	k1gMnSc1	Andranik
Markarjan	Markarjan	k1gMnSc1	Markarjan
<g/>
.	.	kIx.	.
</s>
<s>
Jiní	jiný	k2eAgMnPc1d1	jiný
republikáni	republikán	k1gMnPc1	republikán
vnímají	vnímat	k5eAaImIp3nP	vnímat
tseghakronismus	tseghakronismus	k1gInSc4	tseghakronismus
spíše	spíše	k9	spíše
jako	jako	k8xS	jako
historický	historický	k2eAgInSc4d1	historický
relikt	relikt	k1gInSc4	relikt
a	a	k8xC	a
posouvají	posouvat	k5eAaImIp3nP	posouvat
stranu	strana	k1gFnSc4	strana
k	k	k7c3	k
neideologickému	ideologický	k2eNgInSc3d1	neideologický
středu	střed	k1gInSc3	střed
<g/>
.	.	kIx.	.
</s>
<s>
Představitel	představitel	k1gMnSc1	představitel
Republikánské	republikánský	k2eAgFnSc2d1	republikánská
strany	strana	k1gFnSc2	strana
Arménie	Arménie	k1gFnSc2	Arménie
Serž	serž	k1gInSc1	serž
Sarkisjan	Sarkisjan	k1gInSc1	Sarkisjan
byl	být	k5eAaImAgInS	být
prezidentem	prezident	k1gMnSc7	prezident
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2018.	[number]	k4	2018.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Administrativní	administrativní	k2eAgNnSc1d1	administrativní
dělení	dělení	k1gNnSc1	dělení
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Arménie	Arménie	k1gFnSc1	Arménie
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
10	[number]	k4	10
provincií	provincie	k1gFnPc2	provincie
(	(	kIx(	(
<g/>
marzer	marzer	k1gInSc1	marzer
<g/>
,	,	kIx,	,
sg	sg	k?	sg
<g/>
.	.	kIx.	.
marz	marz	k1gMnSc1	marz
<g/>
)	)	kIx)	)
a	a	k8xC	a
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
<s>
Aragacotn	Aragacotn	k1gNnSc1	Aragacotn
(	(	kIx(	(
<g/>
Ա	Ա	k?	Ա
մ	մ	k?	մ
<g/>
,	,	kIx,	,
Aragac	Aragac	k1gFnSc1	Aragac
<g/>
'	'	kIx"	'
<g/>
otni	otni	k1gNnSc1	otni
marz	marza	k1gFnPc2	marza
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ararat	Ararat	k1gInSc1	Ararat
(	(	kIx(	(
<g/>
Ա	Ա	k?	Ա
մ	մ	k?	մ
<g/>
,	,	kIx,	,
Ararati	Ararat	k1gMnPc1	Ararat
marz	marza	k1gFnPc2	marza
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Armavir	Armavir	k1gInSc1	Armavir
(	(	kIx(	(
<g/>
Ա	Ա	k?	Ա
մ	մ	k?	մ
<g/>
,	,	kIx,	,
Armaviri	Armaviri	k1gNnSc1	Armaviri
marz	marza	k1gFnPc2	marza
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Gegharkunik	Gegharkunik	k1gInSc1	Gegharkunik
(	(	kIx(	(
<g/>
Գ	Գ	k?	Գ
մ	մ	k?	մ
<g/>
,	,	kIx,	,
Gegharkhunikhi	Gegharkhunikhi	k1gNnSc1	Gegharkhunikhi
marz	marza	k1gFnPc2	marza
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kotajk	Kotajk	k1gInSc1	Kotajk
(	(	kIx(	(
<g/>
Կ	Կ	k?	Կ
մ	մ	k?	մ
<g/>
,	,	kIx,	,
Kotajkhi	Kotajkhi	k1gNnSc1	Kotajkhi
marz	marza	k1gFnPc2	marza
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Lorri	Lorri	k1gNnSc1	Lorri
(	(	kIx(	(
<g/>
Լ	Լ	k?	Լ
մ	մ	k?	մ
<g/>
,	,	kIx,	,
Lorru	Lorr	k1gInSc2	Lorr
marz	marza	k1gFnPc2	marza
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Širak	Širak	k1gInSc1	Širak
(	(	kIx(	(
<g/>
Շ	Շ	k?	Շ
մ	մ	k?	մ
<g/>
,	,	kIx,	,
Širaki	Širaki	k1gNnSc1	Širaki
marz	marza	k1gFnPc2	marza
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sjunik	Sjunik	k1gInSc1	Sjunik
(	(	kIx(	(
<g/>
Ս	Ս	k?	Ս
մ	մ	k?	մ
<g/>
,	,	kIx,	,
Sjunikhi	Sjunikhi	k1gNnSc1	Sjunikhi
marz	marza	k1gFnPc2	marza
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Tavuš	Tavuš	k1gInSc1	Tavuš
(	(	kIx(	(
<g/>
Տ	Տ	k?	Տ
մ	մ	k?	մ
<g/>
,	,	kIx,	,
Tavuši	Tavuše	k1gFnSc4	Tavuše
marz	marza	k1gFnPc2	marza
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vajoc	Vajoc	k1gFnSc1	Vajoc
Dzor	Dzora	k1gFnPc2	Dzora
(	(	kIx(	(
<g/>
Վ	Վ	k?	Վ
Ձ	Ձ	k?	Ձ
մ	մ	k?	մ
<g/>
,	,	kIx,	,
Vajoch	Vajoch	k1gMnSc1	Vajoch
Dzori	Dzor	k1gFnSc2	Dzor
marz	marz	k1gMnSc1	marz
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jerevan	Jerevan	k1gMnSc1	Jerevan
(	(	kIx(	(
<g/>
Ե	Ե	k?	Ե
<g/>
,	,	kIx,	,
Jervan	Jervan	k1gMnSc1	Jervan
<g/>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Ekonomika	ekonomika	k1gFnSc1	ekonomika
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Připojení	připojení	k1gNnSc1	připojení
Arménie	Arménie	k1gFnSc2	Arménie
k	k	k7c3	k
SSSR	SSSR	kA	SSSR
mělo	mít	k5eAaImAgNnS	mít
výrazný	výrazný	k2eAgInSc4d1	výrazný
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
rozvoj	rozvoj	k1gInSc4	rozvoj
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
země	zem	k1gFnSc2	zem
se	se	k3xPyFc4	se
pomalu	pomalu	k6eAd1	pomalu
stal	stát	k5eAaPmAgInS	stát
rozvinutý	rozvinutý	k2eAgInSc1d1	rozvinutý
průmyslovo-zemědělský	průmyslovoemědělský	k2eAgInSc1d1	průmyslovo-zemědělský
stát	stát	k1gInSc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
Arménie	Arménie	k1gFnSc1	Arménie
vyvážela	vyvážet	k5eAaImAgFnS	vyvážet
stroje	stroj	k1gInPc4	stroj
<g/>
,	,	kIx,	,
textil	textil	k1gInSc4	textil
a	a	k8xC	a
spotřební	spotřební	k2eAgInPc4d1	spotřební
výrobky	výrobek	k1gInPc4	výrobek
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
SSSR	SSSR	kA	SSSR
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
země	země	k1gFnSc1	země
zrušila	zrušit	k5eAaPmAgFnS	zrušit
zemědělská	zemědělský	k2eAgNnPc4d1	zemědělské
družstva	družstvo	k1gNnPc4	družstvo
a	a	k8xC	a
o	o	k7c4	o
zemědělskou	zemědělský	k2eAgFnSc4d1	zemědělská
výrobu	výroba	k1gFnSc4	výroba
se	se	k3xPyFc4	se
starají	starat	k5eAaImIp3nP	starat
drobní	drobný	k2eAgMnPc1d1	drobný
rolníci	rolník	k1gMnPc1	rolník
<g/>
.	.	kIx.	.
</s>
<s>
Průmysl	průmysl	k1gInSc1	průmysl
prošel	projít	k5eAaPmAgInS	projít
začátkem	začátkem	k7c2	začátkem
90.	[number]	k4	90.
let	léto	k1gNnPc2	léto
obdobím	období	k1gNnSc7	období
stagnace	stagnace	k1gFnSc1	stagnace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nová	nový	k2eAgFnSc1d1	nová
vláda	vláda	k1gFnSc1	vláda
začala	začít	k5eAaPmAgFnS	začít
s	s	k7c7	s
liberalizací	liberalizace	k1gFnSc7	liberalizace
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
zaznamenává	zaznamenávat	k5eAaImIp3nS	zaznamenávat
růst	růst	k1gInSc4	růst
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Významnou	významný	k2eAgFnSc7d1	významná
je	být	k5eAaImIp3nS	být
v	v	k7c4	v
zemi	zem	k1gFnSc4	zem
těžba	těžba	k1gFnSc1	těžba
barevných	barevný	k2eAgInPc2d1	barevný
kovů	kov	k1gInPc2	kov
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
mědi	měď	k1gFnSc2	měď
<g/>
,	,	kIx,	,
olova	olovo	k1gNnSc2	olovo
<g/>
,	,	kIx,	,
zinku	zinek	k1gInSc2	zinek
<g/>
,	,	kIx,	,
železné	železný	k2eAgFnSc2d1	železná
rudy	ruda	k1gFnSc2	ruda
<g/>
,	,	kIx,	,
zlata	zlato	k1gNnSc2	zlato
a	a	k8xC	a
stříbra	stříbro	k1gNnSc2	stříbro
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
se	se	k3xPyFc4	se
také	také	k9	také
zpracovávají	zpracovávat	k5eAaImIp3nP	zpracovávat
diamanty	diamant	k1gInPc4	diamant
a	a	k8xC	a
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
šperky	šperk	k1gInPc4	šperk
<g/>
.	.	kIx.	.
</s>
<s>
Pilířem	pilíř	k1gInSc7	pilíř
ekonomiky	ekonomika	k1gFnSc2	ekonomika
je	být	k5eAaImIp3nS	být
také	také	k9	také
potravinářský	potravinářský	k2eAgInSc1d1	potravinářský
průmysl	průmysl	k1gInSc1	průmysl
<g/>
,	,	kIx,	,
výroba	výroba	k1gFnSc1	výroba
automobilů	automobil	k1gInPc2	automobil
<g/>
,	,	kIx,	,
pneumatik	pneumatika	k1gFnPc2	pneumatika
a	a	k8xC	a
elektromotorů	elektromotor	k1gInPc2	elektromotor
a	a	k8xC	a
přesné	přesný	k2eAgNnSc4d1	přesné
strojírenství	strojírenství	k1gNnSc4	strojírenství
<g/>
.	.	kIx.	.
</s>
<s>
Tradiční	tradiční	k2eAgFnSc1d1	tradiční
je	být	k5eAaImIp3nS	být
textilní	textilní	k2eAgFnSc1d1	textilní
výroba	výroba	k1gFnSc1	výroba
(	(	kIx(	(
<g/>
zejm	zejm	k?	zejm
<g/>
.	.	kIx.	.
koberce	koberec	k1gInPc4	koberec
<g/>
,	,	kIx,	,
pletené	pletený	k2eAgNnSc4d1	pletené
zboží	zboží	k1gNnSc4	zboží
a	a	k8xC	a
obuv	obuv	k1gFnSc4	obuv
<g/>
)	)	kIx)	)
a	a	k8xC	a
výroba	výroba	k1gFnSc1	výroba
hudebních	hudební	k2eAgInPc2d1	hudební
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
hornaté	hornatý	k2eAgFnSc3d1	hornatá
krajině	krajina	k1gFnSc3	krajina
lze	lze	k6eAd1	lze
k	k	k7c3	k
zemědělství	zemědělství	k1gNnSc3	zemědělství
využít	využít	k5eAaPmF	využít
jen	jen	k6eAd1	jen
46	[number]	k4	46
%	%	kIx~	%
plochy	plocha	k1gFnSc2	plocha
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
obdělávané	obdělávaný	k2eAgFnSc2d1	obdělávaná
půdy	půda	k1gFnSc2	půda
leží	ležet	k5eAaImIp3nS	ležet
nad	nad	k7c7	nad
hranicí	hranice	k1gFnSc7	hranice
1000	[number]	k4	1000
m	m	kA	m
<g/>
.	.	kIx.	.
n	n	k0	n
<g/>
.	.	kIx.	.
m	m	kA	m
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
specializace	specializace	k1gFnSc1	specializace
na	na	k7c4	na
pěstování	pěstování	k1gNnSc4	pěstování
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
ovoce	ovoce	k1gNnSc1	ovoce
(	(	kIx(	(
<g/>
zejm	zejm	k?	zejm
<g/>
.	.	kIx.	.
broskve	broskev	k1gFnPc1	broskev
<g/>
,	,	kIx,	,
meruňky	meruňka	k1gFnPc1	meruňka
a	a	k8xC	a
švestky	švestka	k1gFnPc1	švestka
<g/>
)	)	kIx)	)
a	a	k8xC	a
vína	víno	k1gNnSc2	víno
<g/>
.	.	kIx.	.
</s>
<s>
Arménská	arménský	k2eAgNnPc4d1	arménské
vína	víno	k1gNnPc4	víno
jsou	být	k5eAaImIp3nP	být
známá	známá	k1gFnSc1	známá
svou	svůj	k3xOyFgFnSc7	svůj
aromatičností	aromatičnost	k1gFnSc7	aromatičnost
<g/>
.	.	kIx.	.
</s>
<s>
Proslulým	proslulý	k2eAgMnSc7d1	proslulý
je	být	k5eAaImIp3nS	být
zvláště	zvláště	k9	zvláště
arménský	arménský	k2eAgInSc1d1	arménský
koňak	koňak	k1gInSc1	koňak
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejslavnějším	slavný	k2eAgFnPc3d3	nejslavnější
značkám	značka	k1gFnPc3	značka
patří	patřit	k5eAaImIp3nS	patřit
brandy	brandy	k1gFnSc1	brandy
Ararat	Ararat	k1gInSc1	Ararat
<g/>
,	,	kIx,	,
vyráběná	vyráběný	k2eAgFnSc1d1	vyráběná
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1887	[number]	k4	1887
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
část	část	k1gFnSc1	část
vývozu	vývoz	k1gInSc2	vývoz
<g/>
,	,	kIx,	,
23	[number]	k4	23
procent	procento	k1gNnPc2	procento
<g/>
,	,	kIx,	,
míří	mířit	k5eAaImIp3nS	mířit
do	do	k7c2	do
Belgie	Belgie	k1gFnSc2	Belgie
<g/>
.	.	kIx.	.
</s>
<s>
Klíčovým	klíčový	k2eAgMnSc7d1	klíčový
dovozním	dovozní	k2eAgMnSc7d1	dovozní
partnerem	partner	k1gMnSc7	partner
je	být	k5eAaImIp3nS	být
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dlouhodobým	dlouhodobý	k2eAgInSc7d1	dlouhodobý
problémem	problém	k1gInSc7	problém
arménského	arménský	k2eAgNnSc2d1	arménské
hospodářství	hospodářství	k1gNnSc2	hospodářství
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
vysoká	vysoký	k2eAgFnSc1d1	vysoká
nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
(	(	kIx(	(
<g/>
18	[number]	k4	18
<g/>
,	,	kIx,	,
<g/>
5	[number]	k4	5
procenta	procento	k1gNnSc2	procento
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
tomu	ten	k3xDgNnSc3	ten
růst	růst	k1gInSc1	růst
HDP	HDP	kA	HDP
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
se	s	k7c7	s
západní	západní	k2eAgFnSc7d1	západní
Evropou	Evropa	k1gFnSc7	Evropa
rychlý	rychlý	k2eAgInSc4d1	rychlý
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
<g/>
2	[number]	k4	2
procenta	procento	k1gNnSc2	procento
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
3,7	[number]	k4	3,7
procenta	procento	k1gNnSc2	procento
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
k	k	k7c3	k
1.	[number]	k4	1.
lednu	leden	k1gInSc3	leden
2016	[number]	k4	2016
měla	mít	k5eAaImAgFnS	mít
Arménie	Arménie	k1gFnSc1	Arménie
2 977 092	[number]	k4	2 977 092
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
roku	rok	k1gInSc3	rok
2006	[number]	k4	2006
to	ten	k3xDgNnSc1	ten
značilo	značit	k5eAaImAgNnS	značit
pokles	pokles	k1gInSc4	pokles
o	o	k7c4	o
zhruba	zhruba	k6eAd1	zhruba
250 000	[number]	k4	250 000
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
je	být	k5eAaImIp3nS	být
Arménie	Arménie	k1gFnSc1	Arménie
stále	stále	k6eAd1	stále
druhou	druhý	k4xOgFnSc7	druhý
nejhustěji	husto	k6eAd3	husto
osídlenou	osídlený	k2eAgFnSc7d1	osídlená
zemí	zem	k1gFnSc7	zem
z	z	k7c2	z
bývalých	bývalý	k2eAgFnPc2d1	bývalá
sovětských	sovětský	k2eAgFnPc2d1	sovětská
republik	republika	k1gFnPc2	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Národnostně	národnostně	k6eAd1	národnostně
je	být	k5eAaImIp3nS	být
Arménie	Arménie	k1gFnSc1	Arménie
mimořádně	mimořádně	k6eAd1	mimořádně
homogenní	homogenní	k2eAgFnSc1d1	homogenní
<g/>
:	:	kIx,	:
97,9	[number]	k4	97,9
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
tvoří	tvořit	k5eAaImIp3nS	tvořit
Arméni	Armén	k1gMnPc1	Armén
<g/>
;	;	kIx,	;
následují	následovat	k5eAaImIp3nP	následovat
jezidští	jezidský	k2eAgMnPc1d1	jezidský
Kurdové	Kurd	k1gMnPc1	Kurd
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
<g/>
3	[number]	k4	3
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
Rusové	Rus	k1gMnPc1	Rus
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
<g/>
5	[number]	k4	5
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
tvořili	tvořit	k5eAaImAgMnP	tvořit
významnou	významný	k2eAgFnSc4d1	významná
menšinu	menšina	k1gFnSc4	menšina
v	v	k7c6	v
Arménii	Arménie	k1gFnSc6	Arménie
Azerové	Azerový	k2eAgFnSc6d1	Azerový
<g/>
,	,	kIx,	,
na	na	k7c6	na
konci	konec	k1gInSc6	konec
80.	[number]	k4	80.
let	let	k1gInSc4	let
asi	asi	k9	asi
2,5	[number]	k4	2,5
<g/>
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
odešli	odejít	k5eAaPmAgMnP	odejít
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
po	po	k7c6	po
začátku	začátek	k1gInSc6	začátek
konfliktu	konflikt	k1gInSc2	konflikt
o	o	k7c4	o
Náhorní	náhorní	k2eAgInSc4d1	náhorní
Karabach	Karabach	k1gInSc4	Karabach
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
protisměru	protisměr	k1gInSc6	protisměr
z	z	k7c2	z
Ázerbájdžánu	Ázerbájdžán	k1gInSc2	Ázerbájdžán
zamířilo	zamířit	k5eAaPmAgNnS	zamířit
do	do	k7c2	do
Arménie	Arménie	k1gFnSc2	Arménie
mnoho	mnoho	k4c1	mnoho
etnických	etnický	k2eAgMnPc2d1	etnický
Arménů	Armén	k1gMnPc2	Armén
<g/>
.	.	kIx.	.
139 000	[number]	k4	139 000
jich	on	k3xPp3gInPc2	on
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Náhorním	náhorní	k2eAgInSc6d1	náhorní
Karabachu	Karabach	k1gInSc6	Karabach
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
tvoří	tvořit	k5eAaImIp3nP	tvořit
většinu	většina	k1gFnSc4	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
6	[number]	k4	6
milionů	milion	k4xCgInPc2	milion
Arménů	Armén	k1gMnPc2	Armén
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnPc1d3	veliký
komunity	komunita	k1gFnPc1	komunita
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
Íránu	Írán	k1gInSc6	Írán
<g/>
,	,	kIx,	,
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
Gruzii	Gruzie	k1gFnSc6	Gruzie
<g/>
,	,	kIx,	,
Sýrii	Sýrie	k1gFnSc6	Sýrie
<g/>
,	,	kIx,	,
Libanonu	Libanon	k1gInSc6	Libanon
<g/>
,	,	kIx,	,
Argentině	Argentina	k1gFnSc6	Argentina
<g/>
,	,	kIx,	,
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
,	,	kIx,	,
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
,	,	kIx,	,
Řecku	Řecko	k1gNnSc6	Řecko
či	či	k8xC	či
Izraeli	Izrael	k1gInSc6	Izrael
(	(	kIx(	(
<g/>
prastará	prastarý	k2eAgFnSc1d1	prastará
tisícová	tisícový	k2eAgFnSc1d1	tisícová
komunita	komunita	k1gFnSc1	komunita
ve	v	k7c6	v
starém	starý	k2eAgInSc6d1	starý
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
<g/>
)	)	kIx)	)
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jediným	jediný	k2eAgInSc7d1	jediný
oficiálním	oficiální	k2eAgInSc7d1	oficiální
jazykem	jazyk	k1gInSc7	jazyk
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
je	být	k5eAaImIp3nS	být
arménština	arménština	k1gFnSc1	arménština
<g/>
.	.	kIx.	.
</s>
<s>
Arménštinou	arménština	k1gFnSc7	arménština
mluví	mluvit	k5eAaImIp3nS	mluvit
drtivá	drtivý	k2eAgFnSc1d1	drtivá
většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
okolo	okolo	k7c2	okolo
96	[number]	k4	96
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
lidí	člověk	k1gMnPc2	člověk
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
(	(	kIx(	(
<g/>
95	[number]	k4	95
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
ovládá	ovládat	k5eAaImIp3nS	ovládat
i	i	k9	i
ruštinu	ruština	k1gFnSc4	ruština
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
dáno	dán	k2eAgNnSc1d1	dáno
historickou	historický	k2eAgFnSc7d1	historická
i	i	k8xC	i
současnou	současný	k2eAgFnSc7d1	současná
spoluprací	spolupráce	k1gFnSc7	spolupráce
s	s	k7c7	s
Ruskem	Rusko	k1gNnSc7	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Arménština	arménština	k1gFnSc1	arménština
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
cizince	cizinec	k1gMnPc4	cizinec
obtížná	obtížný	k2eAgFnSc1d1	obtížná
především	především	k6eAd1	především
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
užívá	užívat	k5eAaImIp3nS	užívat
specifické	specifický	k2eAgNnSc4d1	specifické
<g/>
,	,	kIx,	,
prastaré	prastarý	k2eAgNnSc4d1	prastaré
originální	originální	k2eAgNnSc4d1	originální
písmo	písmo	k1gNnSc4	písmo
<g/>
.	.	kIx.	.
</s>
<s>
Tvůrcem	tvůrce	k1gMnSc7	tvůrce
této	tento	k3xDgFnSc2	tento
unikátní	unikátní	k2eAgFnSc2d1	unikátní
arménské	arménský	k2eAgFnSc2d1	arménská
abecedy	abeceda	k1gFnSc2	abeceda
byl	být	k5eAaImAgInS	být
Mesrop	Mesrop	k1gInSc1	Mesrop
Maštoc	Maštoc	k1gInSc4	Maštoc
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
405.	[number]	k4	405.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Náboženství	náboženství	k1gNnSc2	náboženství
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Arménské	arménský	k2eAgNnSc1d1	arménské
království	království	k1gNnSc1	království
jako	jako	k8xS	jako
první	první	k4xOgInSc4	první
státní	státní	k2eAgInSc4d1	státní
útvar	útvar	k1gInSc4	útvar
na	na	k7c6	na
světě	svět	k1gInSc6	svět
přijalo	přijmout	k5eAaPmAgNnS	přijmout
roku	rok	k1gInSc2	rok
301	[number]	k4	301
křesťanství	křesťanství	k1gNnSc1	křesťanství
jakožto	jakožto	k8xS	jakožto
státní	státní	k2eAgNnSc1d1	státní
náboženství	náboženství	k1gNnSc1	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
arménské	arménský	k2eAgFnSc3d1	arménská
apoštolské	apoštolský	k2eAgFnSc3d1	apoštolská
církvi	církev	k1gFnSc3	církev
<g/>
,	,	kIx,	,
nejstarší	starý	k2eAgFnSc3d3	nejstarší
národní	národní	k2eAgFnSc3d1	národní
církvi	církev	k1gFnSc3	církev
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nS	hlásit
takřka	takřka	k6eAd1	takřka
95	[number]	k4	95
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
zakladatele	zakladatel	k1gMnSc2	zakladatel
arménské	arménský	k2eAgFnSc2d1	arménská
církve	církev	k1gFnSc2	církev
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
Řehoř	Řehoř	k1gMnSc1	Řehoř
Osvětitel	Osvětitel	k1gMnSc1	Osvětitel
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgInSc7d1	významný
duchovním	duchovní	k2eAgInSc7d1	duchovní
centrem	centr	k1gInSc7	centr
země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
malé	malý	k2eAgNnSc1d1	malé
město	město	k1gNnSc1	město
Ečmiadzin	Ečmiadzin	k1gInSc4	Ečmiadzin
ležící	ležící	k2eAgFnSc1d1	ležící
asi	asi	k9	asi
25	[number]	k4	25
kilometrů	kilometr	k1gInPc2	kilometr
západně	západně	k6eAd1	západně
od	od	k7c2	od
Jerevanu	Jerevan	k1gMnSc3	Jerevan
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
katedrála	katedrála	k1gFnSc1	katedrála
Mayr	Mayr	k1gInSc4	Mayr
Tačar	Tačar	k1gInSc1	Tačar
<g/>
;	;	kIx,	;
zde	zde	k6eAd1	zde
sídlí	sídlet	k5eAaImIp3nS	sídlet
i	i	k9	i
tzv.	tzv.	kA	tzv.
katholikos	katholikos	k1gInSc1	katholikos
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
hlava	hlava	k1gFnSc1	hlava
arménské	arménský	k2eAgFnSc2d1	arménská
apoštolské	apoštolský	k2eAgFnSc2d1	apoštolská
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
roku	rok	k1gInSc2	rok
1855	[number]	k4	1855
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
v	v	k7c6	v
Zákupech	zákup	k1gInPc6	zákup
na	na	k7c6	na
návštěvě	návštěva	k1gFnSc6	návštěva
u	u	k7c2	u
excísaře	excísař	k1gMnSc2	excísař
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
I.	I.	kA	I.
Dobrotivého	dobrotivý	k2eAgInSc2d1	dobrotivý
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
Arménů	Armén	k1gMnPc2	Armén
Arihtazes	Arihtazes	k1gMnSc1	Arihtazes
Azaria	Azarium	k1gNnSc2	Azarium
<g/>
.	.	kIx.	.
<g/>
Kurdové	Kurd	k1gMnPc1	Kurd
žijící	žijící	k2eAgMnPc1d1	žijící
na	na	k7c6	na
západě	západ	k1gInSc6	západ
země	zem	k1gFnSc2	zem
praktikují	praktikovat	k5eAaImIp3nP	praktikovat
zoroastrismus	zoroastrismus	k1gInSc4	zoroastrismus
a	a	k8xC	a
šamanismus	šamanismus	k1gInSc4	šamanismus
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
Azerů	Azer	k1gInPc2	Azer
vyznávajících	vyznávající	k2eAgInPc2d1	vyznávající
islám	islám	k1gInSc4	islám
utekla	utéct	k5eAaPmAgFnS	utéct
během	během	k7c2	během
konfliktu	konflikt	k1gInSc2	konflikt
o	o	k7c4	o
Náhorní	náhorní	k2eAgInSc4d1	náhorní
Karabach	Karabach	k1gInSc4	Karabach
do	do	k7c2	do
Ázerbájdžánu	Ázerbájdžán	k1gInSc2	Ázerbájdžán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Kultura	kultura	k1gFnSc1	kultura
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
oblíbené	oblíbený	k2eAgInPc4d1	oblíbený
hudební	hudební	k2eAgInPc4d1	hudební
nástroje	nástroj	k1gInPc4	nástroj
patří	patřit	k5eAaImIp3nS	patřit
duduk	duduk	k6eAd1	duduk
<g/>
.	.	kIx.	.
<g/>
Turecká	turecký	k2eAgFnSc1d1	turecká
genocida	genocida	k1gFnSc1	genocida
a	a	k8xC	a
útlak	útlak	k1gInSc1	útlak
zplodily	zplodit	k5eAaPmAgInP	zplodit
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
arménskou	arménský	k2eAgFnSc4d1	arménská
diasporu	diaspora	k1gFnSc4	diaspora
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
arménskou	arménský	k2eAgFnSc4d1	arménská
kulturu	kultura	k1gFnSc4	kultura
do	do	k7c2	do
více	hodně	k6eAd2	hodně
než	než	k8xS	než
120	[number]	k4	120
zemí	zem	k1gFnPc2	zem
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Doma	doma	k6eAd1	doma
i	i	k9	i
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
se	se	k3xPyFc4	se
prosadila	prosadit	k5eAaPmAgFnS	prosadit
řada	řada	k1gFnSc1	řada
osobností	osobnost	k1gFnPc2	osobnost
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgMnSc7d1	významný
hudebním	hudební	k2eAgMnSc7d1	hudební
skladatelem	skladatel	k1gMnSc7	skladatel
byl	být	k5eAaImAgMnS	být
Aram	Aram	k1gMnSc1	Aram
Chačaturjan	Chačaturjan	k1gMnSc1	Chačaturjan
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
slavného	slavný	k2eAgInSc2d1	slavný
Šavlového	šavlový	k2eAgInSc2d1	šavlový
tance	tanec	k1gInSc2	tanec
<g/>
.	.	kIx.	.
</s>
<s>
Zakladatelem	zakladatel	k1gMnSc7	zakladatel
moderní	moderní	k2eAgFnSc2d1	moderní
arménské	arménský	k2eAgFnSc2d1	arménská
hudby	hudba	k1gFnSc2	hudba
byl	být	k5eAaImAgInS	být
Komitas	Komitas	k1gInSc1	Komitas
<g/>
.	.	kIx.	.
</s>
<s>
Představiteli	představitel	k1gMnSc3	představitel
klasické	klasický	k2eAgFnSc2d1	klasická
hudby	hudba	k1gFnSc2	hudba
byli	být	k5eAaImAgMnP	být
též	též	k9	též
Alexander	Alexandra	k1gFnPc2	Alexandra
Arutjunjan	Arutjunjan	k1gMnSc1	Arutjunjan
<g/>
,	,	kIx,	,
Avet	Avet	k1gMnSc1	Avet
Terterjan	Terterjany	k1gInPc2	Terterjany
či	či	k8xC	či
Alexander	Alexandra	k1gFnPc2	Alexandra
Spendiarjan	Spendiarjana	k1gFnPc2	Spendiarjana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
populární	populární	k2eAgFnSc6d1	populární
hudbě	hudba	k1gFnSc6	hudba
se	se	k3xPyFc4	se
prosadili	prosadit	k5eAaPmAgMnP	prosadit
někteří	některý	k3yIgMnPc1	některý
představitelé	představitel	k1gMnPc1	představitel
arménské	arménský	k2eAgFnSc2d1	arménská
diaspory	diaspora	k1gFnSc2	diaspora
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
francouzský	francouzský	k2eAgMnSc1d1	francouzský
šansoniér	šansoniér	k1gMnSc1	šansoniér
Charles	Charles	k1gMnSc1	Charles
Aznavour	Aznavour	k1gMnSc1	Aznavour
<g/>
,	,	kIx,	,
americká	americký	k2eAgFnSc1d1	americká
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Cher	Chera	k1gFnPc2	Chera
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
rocker	rocker	k1gMnSc1	rocker
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
System	Syst	k1gMnSc7	Syst
of	of	k?	of
a	a	k8xC	a
Down	Down	k1gMnSc1	Down
Serj	Serj	k1gMnSc1	Serj
Tankian	Tankian	k1gMnSc1	Tankian
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Arménie	Arménie	k1gFnSc2	Arménie
naopak	naopak	k6eAd1	naopak
vyrážel	vyrážet	k5eAaImAgMnS	vyrážet
do	do	k7c2	do
světa	svět	k1gInSc2	svět
hráč	hráč	k1gMnSc1	hráč
na	na	k7c4	na
duduk	duduk	k1gInSc4	duduk
Djivan	Djivan	k1gMnSc1	Djivan
Gasparyan	Gasparyan	k1gMnSc1	Gasparyan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Arménský	arménský	k2eAgInSc4d1	arménský
původ	původ	k1gInSc4	původ
měl	mít	k5eAaImAgMnS	mít
malíř	malíř	k1gMnSc1	malíř
Ivan	Ivan	k1gMnSc1	Ivan
Ajvazovskij	Ajvazovskij	k1gMnSc1	Ajvazovskij
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
Američan	Američan	k1gMnSc1	Američan
Arshile	Arshila	k1gFnSc3	Arshila
Gorky	Gorka	k1gFnSc2	Gorka
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
známým	známý	k2eAgMnPc3d1	známý
malířům	malíř	k1gMnPc3	malíř
působícím	působící	k2eAgInSc6d1	působící
v	v	k7c6	v
Arménii	Arménie	k1gFnSc6	Arménie
patřil	patřit	k5eAaImAgInS	patřit
Martiros	Martirosa	k1gFnPc2	Martirosa
Sarjan	Sarjana	k1gFnPc2	Sarjana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
sovětském	sovětský	k2eAgInSc6d1	sovětský
filmu	film	k1gInSc6	film
se	se	k3xPyFc4	se
prosadil	prosadit	k5eAaPmAgMnS	prosadit
režisér	režisér	k1gMnSc1	režisér
Sergej	Sergej	k1gMnSc1	Sergej
Paradžanov	Paradžanovo	k1gNnPc2	Paradžanovo
či	či	k8xC	či
herec	herec	k1gMnSc1	herec
Armen	Armna	k1gFnPc2	Armna
Džigarchanjan	Džigarchanjan	k1gMnSc1	Džigarchanjan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
režisér	režisér	k1gMnSc1	režisér
Atom	atom	k1gInSc1	atom
Egoyan	Egoyan	k1gInSc1	Egoyan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejznámějším	známý	k2eAgMnSc7d3	nejznámější
spisovatelem	spisovatel	k1gMnSc7	spisovatel
arménského	arménský	k2eAgInSc2d1	arménský
původu	původ	k1gInSc2	původ
je	být	k5eAaImIp3nS	být
patrně	patrně	k6eAd1	patrně
William	William	k1gInSc1	William
Saroyan	Saroyana	k1gFnPc2	Saroyana
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
ten	ten	k3xDgMnSc1	ten
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
diaspoře	diaspora	k1gFnSc6	diaspora
<g/>
.	.	kIx.	.
</s>
<s>
Zakladatelem	zakladatel	k1gMnSc7	zakladatel
moderní	moderní	k2eAgFnSc2d1	moderní
arménské	arménský	k2eAgFnSc2d1	arménská
literatury	literatura	k1gFnSc2	literatura
byl	být	k5eAaImAgInS	být
Chačatur	Chačatura	k1gFnPc2	Chačatura
Abovjan	Abovjana	k1gFnPc2	Abovjana
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
důležitým	důležitý	k2eAgNnPc3d1	důležité
jménům	jméno	k1gNnPc3	jméno
patří	patřit	k5eAaImIp3nS	patřit
též	též	k6eAd1	též
Jeghiše	Jeghiše	k1gFnSc1	Jeghiše
Čarenc	Čarenc	k1gFnSc1	Čarenc
<g/>
,	,	kIx,	,
Hovhannes	Hovhannes	k1gMnSc1	Hovhannes
Tumanjan	Tumanjan	k1gMnSc1	Tumanjan
<g/>
,	,	kIx,	,
Avetik	Avetik	k1gMnSc1	Avetik
Isahakjan	Isahakjan	k1gMnSc1	Isahakjan
<g/>
,	,	kIx,	,
Sajať-Nova	Sajať-Nův	k2eAgFnSc1d1	Sajať-Nův
či	či	k8xC	či
Diana	Diana	k1gFnSc1	Diana
Abgar	Abgara	k1gFnPc2	Abgara
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
seznam	seznam	k1gInSc4	seznam
světového	světový	k2eAgNnSc2d1	světové
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
byly	být	k5eAaImAgInP	být
zapsány	zapsat	k5eAaPmNgInP	zapsat
tři	tři	k4xCgInPc1	tři
arménské	arménský	k2eAgInPc1d1	arménský
klášterní	klášterní	k2eAgInPc1d1	klášterní
komplexy	komplex	k1gInPc1	komplex
<g/>
:	:	kIx,	:
Haghpat	Haghpat	k1gMnSc1	Haghpat
<g/>
,	,	kIx,	,
Sanahin	Sanahin	k1gMnSc1	Sanahin
a	a	k8xC	a
Geghard	Geghard	k1gMnSc1	Geghard
<g/>
.	.	kIx.	.
</s>
<s>
Krom	krom	k7c2	krom
toho	ten	k3xDgMnSc2	ten
katedrála	katedrál	k1gMnSc2	katedrál
(	(	kIx(	(
<g/>
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
kostely	kostel	k1gInPc1	kostel
<g/>
)	)	kIx)	)
v	v	k7c6	v
Ečmiadzinu	Ečmiadzin	k1gInSc6	Ečmiadzin
<g/>
.	.	kIx.	.
</s>
<s>
Chrám	chrám	k1gInSc1	chrám
sv.	sv.	kA	sv.
Ripsime	Ripsim	k1gInSc5	Ripsim
v	v	k7c6	v
Ečmiadzinu	Ečmiadzin	k1gInSc6	Ečmiadzin
byl	být	k5eAaImAgInS	být
vystavěn	vystavět	k5eAaPmNgInS	vystavět
Řehořem	Řehoř	k1gMnSc7	Řehoř
Osvětitelem	Osvětitel	k1gMnSc7	Osvětitel
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
303	[number]	k4	303
<g/>
.	.	kIx.	.
</s>
<s>
Sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
hlava	hlava	k1gFnSc1	hlava
Arménské	arménský	k2eAgFnSc2d1	arménská
apoštolské	apoštolský	k2eAgFnSc2d1	apoštolská
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dalších	další	k2eAgFnPc2d1	další
staveb	stavba	k1gFnPc2	stavba
je	být	k5eAaImIp3nS	být
významný	významný	k2eAgInSc1d1	významný
klášter	klášter	k1gInSc1	klášter
Tatev	Tatev	k1gFnSc1	Tatev
<g/>
,	,	kIx,	,
helénský	helénský	k2eAgInSc1d1	helénský
chrám	chrám	k1gInSc1	chrám
v	v	k7c6	v
Garni	Gareň	k1gFnSc6	Gareň
zasvěcený	zasvěcený	k2eAgMnSc1d1	zasvěcený
kdysi	kdysi	k6eAd1	kdysi
bohu	bůh	k1gMnSc3	bůh
Mithrovi	Mithr	k1gMnSc3	Mithr
<g/>
,	,	kIx,	,
klášter	klášter	k1gInSc1	klášter
Chor	Chora	k1gFnPc2	Chora
Virab	Virab	k1gInSc1	Virab
a	a	k8xC	a
klášter	klášter	k1gInSc1	klášter
Noravank	Noravanka	k1gFnPc2	Noravanka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Galerie	galerie	k1gFnSc1	galerie
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Kuchyně	kuchyně	k1gFnSc2	kuchyně
===	===	k?	===
</s>
</p>
<p>
<s>
Národním	národní	k2eAgNnSc7d1	národní
jídlem	jídlo	k1gNnSc7	jídlo
je	být	k5eAaImIp3nS	být
chléb	chléb	k1gInSc1	chléb
zvaný	zvaný	k2eAgInSc1d1	zvaný
lavaš	lavaš	k1gInSc1	lavaš
<g/>
.	.	kIx.	.
</s>
<s>
Peče	péct	k5eAaImIp3nS	péct
se	se	k3xPyFc4	se
v	v	k7c6	v
pecích	pec	k1gFnPc6	pec
kónického	kónický	k2eAgInSc2d1	kónický
tvaru	tvar	k1gInSc2	tvar
umístěných	umístěný	k2eAgFnPc2d1	umístěná
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
tenkou	tenký	k2eAgFnSc4d1	tenká
placku	placka	k1gFnSc4	placka
<g/>
,	,	kIx,	,
do	do	k7c2	do
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
často	často	k6eAd1	často
balí	balit	k5eAaImIp3nS	balit
jiné	jiný	k2eAgNnSc4d1	jiné
jídlo	jídlo	k1gNnSc4	jídlo
-	-	kIx~	-
třeba	třeba	k6eAd1	třeba
směs	směs	k1gFnSc1	směs
sýru	sýr	k1gInSc2	sýr
<g/>
,	,	kIx,	,
masa	maso	k1gNnSc2	maso
a	a	k8xC	a
zeleniny	zelenina	k1gFnSc2	zelenina
<g/>
.	.	kIx.	.
</s>
<s>
Tradičním	tradiční	k2eAgMnSc7d1	tradiční
je	být	k5eAaImIp3nS	být
zejména	zejména	k9	zejména
tzv.	tzv.	kA	tzv.
chorovac	chorovac	k1gInSc1	chorovac
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
grilované	grilovaný	k2eAgNnSc1d1	grilované
maso	maso	k1gNnSc1	maso
zabalené	zabalený	k2eAgNnSc1d1	zabalené
do	do	k7c2	do
lavaše	lavaše	k1gFnSc2	lavaše
<g/>
,	,	kIx,	,
kořeněné	kořeněný	k2eAgFnSc2d1	kořeněná
zejména	zejména	k9	zejména
bazalkou	bazalka	k1gFnSc7	bazalka
<g/>
,	,	kIx,	,
koriandrem	koriandr	k1gInSc7	koriandr
<g/>
,	,	kIx,	,
rozmarýnem	rozmarýn	k1gInSc7	rozmarýn
a	a	k8xC	a
koprem	kopr	k1gInSc7	kopr
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
polévek	polévka	k1gFnPc2	polévka
arménské	arménský	k2eAgFnSc2d1	arménská
kuchyně	kuchyně	k1gFnSc2	kuchyně
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
bozbaš	bozbaš	k1gInSc1	bozbaš
<g/>
,	,	kIx,	,
též	též	k9	též
zvaný	zvaný	k2eAgInSc1d1	zvaný
haš	haš	k?	haš
<g/>
,	,	kIx,	,
připomínající	připomínající	k2eAgFnSc4d1	připomínající
českou	český	k2eAgFnSc4d1	Česká
dršťkovou	dršťkový	k2eAgFnSc4d1	dršťková
polévku	polévka	k1gFnSc4	polévka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Věda	věda	k1gFnSc1	věda
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Otcem	otec	k1gMnSc7	otec
arménské	arménský	k2eAgFnSc2d1	arménská
vědy	věda	k1gFnSc2	věda
je	být	k5eAaImIp3nS	být
starověký	starověký	k2eAgMnSc1d1	starověký
učenec	učenec	k1gMnSc1	učenec
Anania	Ananium	k1gNnSc2	Ananium
Širakaci	Širakace	k1gFnSc3	Širakace
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgMnSc7d1	významný
botanikem	botanik	k1gMnSc7	botanik
byl	být	k5eAaImAgMnS	být
Armen	Armen	k2eAgMnSc1d1	Armen
Tachtadžjan	Tachtadžjan	k1gMnSc1	Tachtadžjan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
časech	čas	k1gInPc6	čas
SSSR	SSSR	kA	SSSR
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgMnS	proslavit
astronom	astronom	k1gMnSc1	astronom
Viktor	Viktor	k1gMnSc1	Viktor
Amazaspovič	Amazaspovič	k1gMnSc1	Amazaspovič
Ambarcumjan	Ambarcumjan	k1gMnSc1	Ambarcumjan
a	a	k8xC	a
letecký	letecký	k2eAgMnSc1d1	letecký
konstruktér	konstruktér	k1gMnSc1	konstruktér
Arťom	Arťom	k1gInSc4	Arťom
Mikojan	Mikojany	k1gInPc2	Mikojany
<g/>
.	.	kIx.	.
</s>
<s>
Arménský	arménský	k2eAgInSc1d1	arménský
původ	původ	k1gInSc1	původ
měl	mít	k5eAaImAgInS	mít
i	i	k9	i
rakouský	rakouský	k2eAgMnSc1d1	rakouský
matematik	matematik	k1gMnSc1	matematik
Emil	Emil	k1gMnSc1	Emil
Artin	Artin	k1gMnSc1	Artin
či	či	k8xC	či
známý	známý	k2eAgMnSc1d1	známý
průkopník	průkopník	k1gMnSc1	průkopník
eutanázie	eutanázie	k1gFnSc2	eutanázie
Jack	Jack	k1gMnSc1	Jack
Kevorkian	Kevorkian	k1gMnSc1	Kevorkian
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
intelektuálním	intelektuální	k2eAgInPc3d1	intelektuální
pilířům	pilíř	k1gInPc3	pilíř
starověké	starověký	k2eAgFnSc2d1	starověká
Arménie	Arménie	k1gFnSc2	Arménie
patřili	patřit	k5eAaImAgMnP	patřit
zakladatel	zakladatel	k1gMnSc1	zakladatel
arménské	arménský	k2eAgFnSc2d1	arménská
církve	církev	k1gFnSc2	církev
Řehoř	Řehoř	k1gMnSc1	Řehoř
Osvětitel	Osvětitel	k1gMnSc1	Osvětitel
<g/>
,	,	kIx,	,
historik	historik	k1gMnSc1	historik
Movses	Movsesa	k1gFnPc2	Movsesa
Chorenaci	Chorenace	k1gFnSc4	Chorenace
či	či	k8xC	či
teolog	teolog	k1gMnSc1	teolog
Řehoř	Řehoř	k1gMnSc1	Řehoř
z	z	k7c2	z
Nareku	Narek	k1gInSc2	Narek
<g/>
.	.	kIx.	.
</s>
<s>
Georgij	Georgít	k5eAaPmRp2nS	Georgít
Ivanovič	Ivanovič	k1gMnSc1	Ivanovič
Gurdžijev	Gurdžijev	k1gMnSc1	Gurdžijev
byl	být	k5eAaImAgMnS	být
známým	známý	k2eAgMnSc7d1	známý
mystikem	mystik	k1gMnSc7	mystik
20.	[number]	k4	20.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kořeny	kořen	k1gInPc1	kořen
arménské	arménský	k2eAgFnSc2d1	arménská
vzdělanosti	vzdělanost	k1gFnSc2	vzdělanost
jsou	být	k5eAaImIp3nP	být
hluboké	hluboký	k2eAgFnPc1d1	hluboká
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
v	v	k7c6	v
Gladzoru	Gladzor	k1gInSc6	Gladzor
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1280	[number]	k4	1280
a	a	k8xC	a
fungovala	fungovat	k5eAaImAgFnS	fungovat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1340	[number]	k4	1340
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tom	ten	k3xDgInSc6	ten
roce	rok	k1gInSc6	rok
její	její	k3xOp3gFnSc4	její
pozici	pozice	k1gFnSc4	pozice
převzala	převzít	k5eAaPmAgFnS	převzít
nově	nově	k6eAd1	nově
založená	založený	k2eAgFnSc1d1	založená
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Tatevu	Tatevo	k1gNnSc6	Tatevo
(	(	kIx(	(
<g/>
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
tatevského	tatevský	k2eAgInSc2d1	tatevský
kláštera	klášter	k1gInSc2	klášter
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1435.	[number]	k4	1435.
kdy	kdy	k6eAd1	kdy
ji	on	k3xPp3gFnSc4	on
i	i	k8xC	i
klášter	klášter	k1gInSc4	klášter
vypálili	vypálit	k5eAaPmAgMnP	vypálit
Mongolové	Mongol	k1gMnPc1	Mongol
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc7d3	veliký
univerzitou	univerzita	k1gFnSc7	univerzita
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
Jerevanská	Jerevanský	k2eAgFnSc1d1	Jerevanská
státní	státní	k2eAgFnSc1d1	státní
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Studuje	studovat	k5eAaImIp3nS	studovat
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
okolo	okolo	k7c2	okolo
20 000	[number]	k4	20 000
studentů	student	k1gMnPc2	student
na	na	k7c6	na
19	[number]	k4	19
fakultách	fakulta	k1gFnPc6	fakulta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Sport	sport	k1gInSc1	sport
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Největší	veliký	k2eAgInPc4d3	veliký
úspěchy	úspěch	k1gInPc4	úspěch
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
Arméni	Armén	k1gMnPc1	Armén
v	v	k7c6	v
zápase	zápas	k1gInSc6	zápas
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
jiné	jiný	k2eAgInPc1d1	jiný
kavkazské	kavkazský	k2eAgInPc1d1	kavkazský
národy	národ	k1gInPc1	národ
<g/>
.	.	kIx.	.
</s>
<s>
Zápasník	zápasník	k1gMnSc1	zápasník
Armen	Armna	k1gFnPc2	Armna
Nazarjan	Nazarjan	k1gMnSc1	Nazarjan
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
pro	pro	k7c4	pro
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
Arménii	Arménie	k1gFnSc6	Arménie
první	první	k4xOgFnSc4	první
zlatou	zlatý	k2eAgFnSc4d1	zlatá
olympijskou	olympijský	k2eAgFnSc4d1	olympijská
medaili	medaile	k1gFnSc4	medaile
<g/>
,	,	kIx,	,
druhou	druhý	k4xOgFnSc4	druhý
přidal	přidat	k5eAaPmAgMnS	přidat
na	na	k7c6	na
olympiádě	olympiáda	k1gFnSc6	olympiáda
v	v	k7c6	v
Riu	Riu	k1gFnSc6	Riu
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
Artur	Artur	k1gMnSc1	Artur
Aleksanjan	Aleksanjan	k1gMnSc1	Aleksanjan
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
kolegové	kolega	k1gMnPc1	kolega
Arsen	arsen	k1gInSc4	arsen
Julfalakjan	Julfalakjan	k1gInSc1	Julfalakjan
<g/>
,	,	kIx,	,
Armen	Armen	k2eAgInSc1d1	Armen
Mkrtčjan	Mkrtčjan	k1gInSc1	Mkrtčjan
a	a	k8xC	a
Miran	Miran	k1gInSc1	Miran
Arutjunjan	Arutjunjany	k1gInPc2	Arutjunjany
mají	mít	k5eAaImIp3nP	mít
olympijské	olympijský	k2eAgNnSc4d1	Olympijské
stříbro	stříbro	k1gNnSc4	stříbro
<g/>
.	.	kIx.	.
</s>
<s>
Suren	Suren	k2eAgMnSc1d1	Suren
Nalbandjan	Nalbandjan	k1gMnSc1	Nalbandjan
<g/>
,	,	kIx,	,
Sanasar	Sanasar	k1gMnSc1	Sanasar
Oganisjan	Oganisjan	k1gMnSc1	Oganisjan
<g/>
,	,	kIx,	,
Levon	Levon	k1gMnSc1	Levon
Julfalakjan	Julfalakjan	k1gMnSc1	Julfalakjan
a	a	k8xC	a
Mnacakan	Mnacakan	k1gMnSc1	Mnacakan
Iskandarjan	Iskandarjan	k1gMnSc1	Iskandarjan
vyhráli	vyhrát	k5eAaPmAgMnP	vyhrát
olympiádu	olympiáda	k1gFnSc4	olympiáda
ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
dresu	dres	k1gInSc6	dres
SSSR	SSSR	kA	SSSR
(	(	kIx(	(
<g/>
Iskandarjan	Iskandarjan	k1gMnSc1	Iskandarjan
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
Sjednoceném	sjednocený	k2eAgInSc6d1	sjednocený
týmu	tým	k1gInSc6	tým
v	v	k7c6	v
Barceloně	Barcelona	k1gFnSc6	Barcelona
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zápas	zápas	k1gInSc1	zápas
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
Arménii	Arménie	k1gFnSc6	Arménie
velké	velký	k2eAgNnSc4d1	velké
zázemí	zázemí	k1gNnSc4	zázemí
<g/>
,	,	kIx,	,
přetrvává	přetrvávat	k5eAaImIp3nS	přetrvávat
i	i	k9	i
tradice	tradice	k1gFnSc1	tradice
specifického	specifický	k2eAgInSc2d1	specifický
arménského	arménský	k2eAgInSc2d1	arménský
druhu	druh	k1gInSc2	druh
zápasu	zápas	k1gInSc2	zápas
zvaného	zvaný	k2eAgInSc2d1	zvaný
kokh	kokh	k1gInSc1	kokh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dalším	další	k2eAgInSc7d1	další
sportem	sport	k1gInSc7	sport
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
Arménii	Arménie	k1gFnSc4	Arménie
značných	značný	k2eAgInPc2d1	značný
úspěchů	úspěch	k1gInPc2	úspěch
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vzpírání	vzpírání	k1gNnSc4	vzpírání
<g/>
.	.	kIx.	.
</s>
<s>
Vybojovali	vybojovat	k5eAaPmAgMnP	vybojovat
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
již	již	k6eAd1	již
pět	pět	k4xCc1	pět
olympijských	olympijský	k2eAgInPc2d1	olympijský
cenných	cenný	k2eAgInPc2d1	cenný
kovů	kov	k1gInPc2	kov
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgMnSc2	ten
třikrát	třikrát	k6eAd1	třikrát
stříbro	stříbro	k1gNnSc1	stříbro
(	(	kIx(	(
<g/>
Simon	Simon	k1gMnSc1	Simon
Martirosjan	Martirosjan	k1gMnSc1	Martirosjan
<g/>
,	,	kIx,	,
Gor	Gor	k1gMnSc1	Gor
Minasjan	Minasjan	k1gMnSc1	Minasjan
<g/>
,	,	kIx,	,
Tigran	Tigran	k1gInSc1	Tigran
Vardan	Vardan	k1gMnSc1	Vardan
Martirosjan	Martirosjan	k1gMnSc1	Martirosjan
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dresu	dres	k1gInSc6	dres
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
vybojovali	vybojovat	k5eAaPmAgMnP	vybojovat
zlato	zlato	k1gNnSc4	zlato
arménští	arménský	k2eAgMnPc1d1	arménský
vzpěrači	vzpěrač	k1gMnPc1	vzpěrač
Rafael	Rafael	k1gMnSc1	Rafael
Chimiškjan	Chimiškjan	k1gMnSc1	Chimiškjan
(	(	kIx(	(
<g/>
Helsinki	Helsinki	k1gNnSc1	Helsinki
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jurik	Jurik	k1gMnSc1	Jurik
Vardanjan	Vardanjan	k1gMnSc1	Vardanjan
(	(	kIx(	(
<g/>
Moskva	Moskva	k1gFnSc1	Moskva
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Oksen	Oksen	k2eAgInSc1d1	Oksen
Mirzojan	Mirzojan	k1gInSc1	Mirzojan
(	(	kIx(	(
<g/>
Soul	Soul	k1gInSc1	Soul
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
a	a	k8xC	a
Israel	Israel	k1gMnSc1	Israel
Militosjan	Militosjan	k1gMnSc1	Militosjan
(	(	kIx(	(
<g/>
Barcelona	Barcelona	k1gFnSc1	Barcelona
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Norair	Norair	k1gMnSc1	Norair
Nurikjan	Nurikjan	k1gMnSc1	Nurikjan
to	ten	k3xDgNnSc4	ten
dokázal	dokázat	k5eAaPmAgMnS	dokázat
v	v	k7c6	v
dresu	dres	k1gInSc6	dres
Bulharska	Bulharsko	k1gNnSc2	Bulharsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Čtyři	čtyři	k4xCgInPc1	čtyři
olympijské	olympijský	k2eAgInPc1d1	olympijský
kovy	kov	k1gInPc1	kov
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
dvě	dva	k4xCgFnPc1	dva
zlaté	zlatý	k2eAgFnPc1d1	zlatá
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
z	z	k7c2	z
Helsinek	Helsinky	k1gFnPc2	Helsinky
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
přivezl	přivézt	k5eAaPmAgMnS	přivézt
gymnasta	gymnasta	k1gMnSc1	gymnasta
Hrant	Hrant	k?	Hrant
Šahinjan	Šahinjan	k1gMnSc1	Šahinjan
<g/>
,	,	kIx,	,
tři	tři	k4xCgInPc4	tři
zlaté	zlatý	k2eAgInPc4d1	zlatý
a	a	k8xC	a
jedno	jeden	k4xCgNnSc1	jeden
stříbro	stříbro	k1gNnSc1	stříbro
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
kontě	konto	k1gNnSc6	konto
jeho	jeho	k3xOp3gMnSc1	jeho
nástupce	nástupce	k1gMnSc1	nástupce
Albert	Albert	k1gMnSc1	Albert
Azarjan	Azarjan	k1gMnSc1	Azarjan
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgMnSc4	jenž
syn	syn	k1gMnSc1	syn
Eduard	Eduard	k1gMnSc1	Eduard
Azarjan	Azarjan	k1gMnSc1	Azarjan
později	pozdě	k6eAd2	pozdě
získal	získat	k5eAaPmAgMnS	získat
rovněž	rovněž	k9	rovněž
zlato	zlato	k1gNnSc4	zlato
<g/>
,	,	kIx,	,
ze	z	k7c2	z
závodu	závod	k1gInSc2	závod
družstev	družstvo	k1gNnPc2	družstvo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
boxu	box	k1gInSc6	box
vybojoval	vybojovat	k5eAaPmAgMnS	vybojovat
nejvyšší	vysoký	k2eAgNnSc4d3	nejvyšší
olympijské	olympijský	k2eAgNnSc4d1	Olympijské
ocenění	ocenění	k1gNnSc4	ocenění
Vladimir	Vladimir	k1gMnSc1	Vladimir
Jengibarjan	Jengibarjan	k1gMnSc1	Jengibarjan
<g/>
,	,	kIx,	,
v	v	k7c6	v
Melbourne	Melbourne	k1gNnSc6	Melbourne
roku	rok	k1gInSc2	rok
1956	[number]	k4	1956
<g/>
,	,	kIx,	,
ve	v	k7c6	v
střelbě	střelba	k1gFnSc6	střelba
Hrachja	Hrachja	k1gMnSc1	Hrachja
Petikjan	Petikjan	k1gMnSc1	Petikjan
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kolektivních	kolektivní	k2eAgInPc6d1	kolektivní
sportech	sport	k1gInPc6	sport
si	se	k3xPyFc3	se
v	v	k7c6	v
barvách	barva	k1gFnPc6	barva
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
zlato	zlato	k1gNnSc4	zlato
z	z	k7c2	z
olympiády	olympiáda	k1gFnSc2	olympiáda
přivezli	přivézt	k5eAaPmAgMnP	přivézt
fotbalista	fotbalista	k1gMnSc1	fotbalista
Nikita	Nikita	k1gMnSc1	Nikita
Simonjan	Simonjan	k1gMnSc1	Simonjan
(	(	kIx(	(
<g/>
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
basketbalistka	basketbalistka	k1gFnSc1	basketbalistka
Elen	Elena	k1gFnPc2	Elena
Šakirovová	Šakirovová	k1gFnSc1	Šakirovová
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
a	a	k8xC	a
šermíř	šermíř	k1gMnSc1	šermíř
Heorhij	Heorhij	k1gFnSc2	Heorhij
Pohosov	Pohosov	k1gInSc1	Pohosov
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejúspěšnějším	úspěšný	k2eAgMnSc7d3	nejúspěšnější
arménským	arménský	k2eAgMnSc7d1	arménský
atletem	atlet	k1gMnSc7	atlet
je	být	k5eAaImIp3nS	být
skokan	skokan	k1gMnSc1	skokan
do	do	k7c2	do
dálky	dálka	k1gFnSc2	dálka
Igor	Igor	k1gMnSc1	Igor
Ter-Ovanesjan	Ter-Ovanesjan	k1gMnSc1	Ter-Ovanesjan
<g/>
,	,	kIx,	,
dvojnásobný	dvojnásobný	k2eAgMnSc1d1	dvojnásobný
mistr	mistr	k1gMnSc1	mistr
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
šachistů	šachista	k1gMnPc2	šachista
k	k	k7c3	k
nejvýznamnějším	významný	k2eAgFnPc3d3	nejvýznamnější
patří	patřit	k5eAaImIp3nS	patřit
Tigran	Tigrana	k1gFnPc2	Tigrana
Petrosjan	Petrosjan	k1gMnSc1	Petrosjan
či	či	k8xC	či
Levon	Levon	k1gMnSc1	Levon
Aronjan	Aronjan	k1gMnSc1	Aronjan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
vyniká	vynikat	k5eAaImIp3nS	vynikat
především	především	k9	především
klub	klub	k1gInSc1	klub
Pjunik	Pjunik	k1gMnSc1	Pjunik
Jerevan	Jerevan	k1gMnSc1	Jerevan
<g/>
,	,	kIx,	,
z	z	k7c2	z
hráčů	hráč	k1gMnPc2	hráč
můžeme	moct	k5eAaImIp1nP	moct
zmínit	zmínit	k5eAaPmF	zmínit
hlavně	hlavně	k9	hlavně
záložníka	záložník	k1gMnSc4	záložník
Henricha	Henrich	k1gMnSc4	Henrich
Mchitarjana	Mchitarjan	k1gMnSc4	Mchitarjan
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
s	s	k7c7	s
Manchesterem	Manchester	k1gInSc7	Manchester
United	United	k1gMnSc1	United
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
Evropskou	evropský	k2eAgFnSc4d1	Evropská
ligu	liga	k1gFnSc4	liga
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Arménský	arménský	k2eAgInSc4d1	arménský
původ	původ	k1gInSc4	původ
měla	mít	k5eAaImAgFnS	mít
i	i	k9	i
řada	řada	k1gFnSc1	řada
úspěšných	úspěšný	k2eAgMnPc2d1	úspěšný
francouzských	francouzský	k2eAgMnPc2d1	francouzský
fotbalistů	fotbalista	k1gMnPc2	fotbalista
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Youri	Your	k1gMnPc1	Your
Djorkaeff	Djorkaeff	k1gMnSc1	Djorkaeff
či	či	k8xC	či
Alain	Alain	k1gMnSc1	Alain
Boghossian	Boghossian	k1gMnSc1	Boghossian
<g/>
,	,	kIx,	,
či	či	k8xC	či
americký	americký	k2eAgMnSc1d1	americký
tenista	tenista	k1gMnSc1	tenista
Andre	Andr	k1gMnSc5	Andr
Agassi	Agass	k1gMnSc5	Agass
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Ararat	Ararat	k1gMnSc1	Ararat
</s>
</p>
<p>
<s>
Arménská	arménský	k2eAgFnSc1d1	arménská
genocida	genocida	k1gFnSc1	genocida
</s>
</p>
<p>
<s>
Náhorní	náhorní	k2eAgInSc1d1	náhorní
Karabach	Karabach	k1gInSc1	Karabach
</s>
</p>
<p>
<s>
Republika	republika	k1gFnSc1	republika
Arcach	Arcacha	k1gFnPc2	Arcacha
</s>
</p>
<p>
<s>
Arménská	arménský	k2eAgFnSc1d1	arménská
apoštolská	apoštolský	k2eAgFnSc1d1	apoštolská
církev	církev	k1gFnSc1	církev
</s>
</p>
<p>
<s>
Armeniapedia	Armeniapedium	k1gNnPc1	Armeniapedium
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Arménie	Arménie	k1gFnSc2	Arménie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Arménie	Arménie	k1gFnSc2	Arménie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Arménie	Arménie	k1gFnSc2	Arménie
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
arménsky	arménsky	k6eAd1	arménsky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Armenia	Armenium	k1gNnPc1	Armenium
Information	Information	k1gInSc4	Information
–	–	k?	–
oficiální	oficiální	k2eAgFnSc2d1	oficiální
turistické	turistický	k2eAgFnSc2d1	turistická
stránky	stránka	k1gFnSc2	stránka
republiky	republika	k1gFnSc2	republika
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
arménsky	arménsky	k6eAd1	arménsky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
<g/>
)	)	kIx)	)
Hayastan	Hayastan	k1gInSc1	Hayastan
–	–	k?	–
stránky	stránka	k1gFnSc2	stránka
S.	S.	kA	S.
Šahumjana	Šahumjan	k1gMnSc2	Šahumjan
<g/>
,	,	kIx,	,
polského	polský	k2eAgMnSc2d1	polský
Arména	Armén	k1gMnSc2	Armén
<g/>
,	,	kIx,	,
s	s	k7c7	s
informacemi	informace	k1gFnPc7	informace
o	o	k7c6	o
Arménii	Arménie	k1gFnSc6	Arménie
a	a	k8xC	a
množstvím	množství	k1gNnSc7	množství
fotoalb	fotoalbum	k1gNnPc2	fotoalbum
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
HayLife	HayLif	k1gInSc5	HayLif
<g/>
.	.	kIx.	.
<g/>
ru	ru	k?	ru
–	–	k?	–
internetový	internetový	k2eAgInSc4d1	internetový
portál	portál	k1gInSc4	portál
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
švédsky	švédsky	k6eAd1	švédsky
<g/>
)	)	kIx)	)
Armenica.org	Armenica.org	k1gMnSc1	Armenica.org
–	–	k?	–
History	Histor	k1gInPc1	Histor
of	of	k?	of
Armenia	Armenium	k1gNnSc2	Armenium
–	–	k?	–
vše	všechen	k3xTgNnSc1	všechen
o	o	k7c6	o
Arménii	Arménie	k1gFnSc6	Arménie
a	a	k8xC	a
Náhorním	náhorní	k2eAgInSc6d1	náhorní
Karabachu	Karabach	k1gInSc6	Karabach
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Topografická	topografický	k2eAgFnSc1d1	topografická
mapa	mapa	k1gFnSc1	mapa
Arménie	Arménie	k1gFnSc2	Arménie
(	(	kIx(	(
<g/>
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
SSSR	SSSR	kA	SSSR
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Arménský	arménský	k2eAgInSc1d1	arménský
dům	dům	k1gInSc1	dům
–	–	k?	–
Portál	portál	k1gInSc4	portál
arménské	arménský	k2eAgFnSc2d1	arménská
komunity	komunita	k1gFnSc2	komunita
v	v	k7c6	v
ČR	ČR	kA	ČR
</s>
</p>
<p>
<s>
Armenia	Armenium	k1gNnPc1	Armenium
-	-	kIx~	-
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc1	International
Report	report	k1gInSc1	report
2011	[number]	k4	2011
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc2	International
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011-08-16	[number]	k4	2011-08-16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Armenia	Armenium	k1gNnPc1	Armenium
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Freedom	Freedom	k1gInSc1	Freedom
House	house	k1gNnSc1	house
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011-08-16	[number]	k4	2011-08-16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bertelsmann	Bertelsmann	k1gMnSc1	Bertelsmann
Stiftung	Stiftung	k1gMnSc1	Stiftung
<g/>
.	.	kIx.	.
</s>
<s>
BTI	BTI	kA	BTI
2010	[number]	k4	2010
–	–	k?	–
Armenia	Armenium	k1gNnSc2	Armenium
Country	country	k2eAgInSc4d1	country
Report	report	k1gInSc4	report
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Gütersloh	Gütersloh	k1gMnSc1	Gütersloh
<g/>
:	:	kIx,	:
Bertelsmann	Bertelsmann	k1gMnSc1	Bertelsmann
Stiftung	Stiftung	k1gMnSc1	Stiftung
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011-08-16	[number]	k4	2011-08-16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2011-05-21	[number]	k4	2011-05-21
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bureau	Bureau	k6eAd1	Bureau
of	of	k?	of
European	European	k1gInSc1	European
and	and	k?	and
Eurasian	Eurasian	k1gInSc1	Eurasian
Affairs	Affairs	k1gInSc1	Affairs
<g/>
.	.	kIx.	.
</s>
<s>
Background	Background	k1gInSc1	Background
Note	Not	k1gInSc2	Not
<g/>
:	:	kIx,	:
Armenia	Armenium	k1gNnSc2	Armenium
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
U.S.	U.S.	k?	U.S.
Department	department	k1gInSc1	department
of	of	k?	of
State	status	k1gInSc5	status
<g/>
,	,	kIx,	,
2010-11-17	[number]	k4	2010-11-17
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011-08-16	[number]	k4	2011-08-16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2013-01-08	[number]	k4	2013-01-08
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
CIA	CIA	kA	CIA
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
World	World	k1gInSc1	World
Factbook	Factbook	k1gInSc1	Factbook
-	-	kIx~	-
Armenia	Armenium	k1gNnSc2	Armenium
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Rev	Rev	k?	Rev
<g/>
.	.	kIx.	.
2011-07-05	[number]	k4	2011-07-05
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011-08-16	[number]	k4	2011-08-16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zastupitelský	zastupitelský	k2eAgInSc1d1	zastupitelský
úřad	úřad	k1gInSc1	úřad
ČR	ČR	kA	ČR
v	v	k7c6	v
Tbilisi	Tbilisi	k1gNnSc6	Tbilisi
<g/>
.	.	kIx.	.
</s>
<s>
Souhrnná	souhrnný	k2eAgFnSc1d1	souhrnná
teritoriální	teritoriální	k2eAgFnSc1d1	teritoriální
informace	informace	k1gFnSc1	informace
<g/>
:	:	kIx,	:
Arménie	Arménie	k1gFnSc1	Arménie
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Businessinfo.cz	Businessinfo.cz	k1gMnSc1	Businessinfo.cz
<g/>
,	,	kIx,	,
2010-03-16	[number]	k4	2010-03-16
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011-08-16	[number]	k4	2011-08-16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2012-01-11	[number]	k4	2012-01-11
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
DOWSETT	DOWSETT	kA	DOWSETT
<g/>
,	,	kIx,	,
Charles	Charles	k1gMnSc1	Charles
James	James	k1gMnSc1	James
Frank	Frank	k1gMnSc1	Frank
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Armenia	Armenium	k1gNnPc1	Armenium
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Encyclopaedia	Encyclopaedium	k1gNnPc1	Encyclopaedium
Britannica	Britannic	k2eAgNnPc1d1	Britannic
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011-08-16	[number]	k4	2011-08-16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
