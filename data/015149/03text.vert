<s>
Oto	Oto	k1gMnSc1
Bachorík	Bachorík	k1gMnSc1
</s>
<s>
Oto	Oto	k1gMnSc1
Bachorík	Bachorík	k1gMnSc1
Narození	narození	k1gNnSc4
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1959	#num#	k4
(	(	kIx(
<g/>
62	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Povolání	povolání	k1gNnPc2
</s>
<s>
sochař	sochař	k1gMnSc1
<g/>
,	,	kIx,
restaurátor	restaurátor	k1gMnSc1
a	a	k8xC
učitel	učitel	k1gMnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybět	k5eAaImIp3nS,k5eAaPmIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Oto	Oto	k1gMnSc1
Bachorík	Bachorík	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
1959	#num#	k4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
slovenský	slovenský	k2eAgMnSc1d1
výtvarný	výtvarný	k2eAgMnSc1d1
umělec	umělec	k1gMnSc1
–	–	k?
sochař	sochař	k1gMnSc1
<g/>
,	,	kIx,
restaurátor	restaurátor	k1gMnSc1
a	a	k8xC
pedagog	pedagog	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
umělecké	umělecký	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gFnSc4
otec	otec	k1gMnSc1
byl	být	k5eAaImAgMnS
grafik	grafik	k1gMnSc1
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gMnSc4,k3xPp3gMnSc4
matka	matka	k1gFnSc1
byla	být	k5eAaImAgFnS
výtvarnice-keramička	výtvarnice-keramička	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Vystudoval	vystudovat	k5eAaPmAgMnS
řezbářství	řezbářství	k1gNnSc1
na	na	k7c6
Střední	střední	k2eAgFnSc6d1
škole	škola	k1gFnSc6
uměleckého	umělecký	k2eAgInSc2d1
průmyslu	průmysl	k1gInSc2
a	a	k8xC
sochařství	sochařství	k1gNnSc2
a	a	k8xC
restaurátorství	restaurátorství	k1gNnSc2
na	na	k7c6
Vysoké	vysoký	k2eAgFnSc6d1
škole	škola	k1gFnSc6
výtvarných	výtvarný	k2eAgNnPc2d1
umění	umění	k1gNnPc2
v	v	k7c6
Bratislavě	Bratislava	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7
manželkou	manželka	k1gFnSc7
je	být	k5eAaImIp3nS
malířka	malířka	k1gFnSc1
Zuzana	Zuzana	k1gFnSc1
Bachoríková	Bachoríková	k1gFnSc1
<g/>
,	,	kIx,
se	s	k7c7
kterou	který	k3yIgFnSc7,k3yRgFnSc7,k3yQgFnSc7
má	mít	k5eAaImIp3nS
dvě	dva	k4xCgFnPc4
děti	dítě	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
http://www.gmab.sk/vystavy-a-podujatia/oto-bachorik-socha-zuzana-rabina-bachorikova-malba.html?page_id=327	http://www.gmab.sk/vystavy-a-podujatia/oto-bachorik-socha-zuzana-rabina-bachorikova-malba.html?page_id=327	k4
</s>
<s>
http://www.artgallery.sk/pouzivatel.php?getPouzivatel=124	http://www.artgallery.sk/pouzivatel.php?getPouzivatel=124	k4
</s>
<s>
http://www.zilinskyvecernik.sk/articles/2013/04/15/galeria-m-oslavila-4-vyrocie-vystavou-bachorikovcov	http://www.zilinskyvecernik.sk/articles/2013/04/15/galeria-m-oslavila-4-vyrocie-vystavou-bachorikovcov	k1gInSc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Nositelé	nositel	k1gMnPc1
Krištáľového	Krištáľový	k2eAgInSc2d1
krídla	krídnout	k5eAaPmAgFnS
za	za	k7c4
výtvarné	výtvarný	k2eAgNnSc4d1
umění	umění	k1gNnSc4
</s>
<s>
Karol	Karol	k1gInSc1
Kállay	Kállaa	k1gFnSc2
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Hedviga	Hedviga	k1gFnSc1
Hamžíková	Hamžíková	k1gFnSc1
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Dušan	Dušan	k1gMnSc1
Kállay	Kállaa	k1gFnSc2
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Zora	Zora	k1gFnSc1
Palová	Palová	k1gFnSc1
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Milota	milota	k1gFnSc1
Havránková	Havránková	k1gFnSc1
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Peter	Peter	k1gMnSc1
Pollág	Pollág	k1gMnSc1
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Vladimír	Vladimír	k1gMnSc1
Gažovič	Gažovič	k1gMnSc1
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Marek	Marek	k1gMnSc1
Ormandík	Ormandík	k1gMnSc1
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1
Chrenka	Chrenka	k1gFnSc1
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Jozef	Jozef	k1gMnSc1
Jankovič	Jankovič	k1gMnSc1
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Michal	Michal	k1gMnSc1
Staško	Staška	k1gFnSc5
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Štefan	Štefan	k1gMnSc1
Klein	Klein	k1gMnSc1
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Ivan	Ivan	k1gMnSc1
Pavle	Pavla	k1gFnSc3
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Palo	Pala	k1gMnSc5
Macho	macha	k1gFnSc5
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Juraj	Juraj	k1gMnSc1
Čutek	Čutek	k1gMnSc1
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Ján	Ján	k1gMnSc1
Ťapák	Ťapák	k1gMnSc1
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Oto	Oto	k1gMnSc1
Bachorík	Bachorík	k1gMnSc1
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jo	jo	k9
<g/>
2003190792	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
122830008	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
7864	#num#	k4
6830	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
18115623	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lidé	člověk	k1gMnPc1
|	|	kIx~
Slovensko	Slovensko	k1gNnSc1
|	|	kIx~
Umění	umění	k1gNnSc1
</s>
