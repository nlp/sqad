<s>
Škrob	škrob	k1gInSc1	škrob
(	(	kIx(	(
<g/>
amylum	amylum	k1gInSc1	amylum
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
makromolekulární	makromolekulární	k2eAgFnSc1d1	makromolekulární
látka	látka	k1gFnSc1	látka
(	(	kIx(	(
<g/>
konkrétně	konkrétně	k6eAd1	konkrétně
směs	směs	k1gFnSc1	směs
polysacharidů	polysacharid	k1gInPc2	polysacharid
glukanů	glukan	k1gMnPc2	glukan
<g/>
)	)	kIx)	)
syntetizovaná	syntetizovaný	k2eAgFnSc1d1	syntetizovaná
rostlinami	rostlina	k1gFnPc7	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
bílý	bílý	k2eAgInSc1d1	bílý
prášek	prášek	k1gInSc1	prášek
bez	bez	k7c2	bez
chuti	chuť	k1gFnSc2	chuť
a	a	k8xC	a
vůně	vůně	k1gFnSc2	vůně
<g/>
,	,	kIx,	,
nerozpustný	rozpustný	k2eNgInSc1d1	nerozpustný
ve	v	k7c6	v
studené	studený	k2eAgFnSc6d1	studená
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
konečný	konečný	k2eAgInSc4d1	konečný
produkt	produkt	k1gInSc4	produkt
fotosyntézy	fotosyntéza	k1gFnSc2	fotosyntéza
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Škrob	škrob	k1gInSc1	škrob
je	být	k5eAaImIp3nS	být
polysacharid	polysacharid	k1gInSc4	polysacharid
se	s	k7c7	s
vzorcem	vzorec	k1gInSc7	vzorec
(	(	kIx(	(
<g/>
C	C	kA	C
<g/>
6	[number]	k4	6
<g/>
H	H	kA	H
<g/>
10	[number]	k4	10
<g/>
O	o	k7c4	o
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
<g/>
n	n	k0	n
složený	složený	k2eAgInSc1d1	složený
z	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
různých	různý	k2eAgInPc2d1	různý
polysacharidů	polysacharid	k1gInPc2	polysacharid
<g/>
:	:	kIx,	:
amylózy	amylóza	k1gFnSc2	amylóza
a	a	k8xC	a
amylopektinu	amylopektin	k1gInSc2	amylopektin
<g/>
,	,	kIx,	,
tvořených	tvořený	k2eAgFnPc2d1	tvořená
několika	několik	k4yIc7	několik
tisíci	tisíc	k4xCgInPc7	tisíc
až	až	k8xS	až
desetitisíci	desetitisíce	k1gInPc7	desetitisíce
molekul	molekula	k1gFnPc2	molekula
glukózy	glukóza	k1gFnSc2	glukóza
<g/>
.	.	kIx.	.
</s>
<s>
Škrob	škrob	k1gInSc1	škrob
kromě	kromě	k7c2	kromě
glukózy	glukóza	k1gFnSc2	glukóza
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
v	v	k7c6	v
malém	malý	k2eAgInSc6d1	malý
množství	množství	k1gNnSc2	množství
lipidy	lipid	k1gInPc4	lipid
<g/>
,	,	kIx,	,
proteiny	protein	k1gInPc4	protein
a	a	k8xC	a
zhruba	zhruba	k6eAd1	zhruba
25	[number]	k4	25
<g/>
–	–	k?	–
<g/>
35	[number]	k4	35
%	%	kIx~	%
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Škrob	škrob	k1gInSc1	škrob
není	být	k5eNaImIp3nS	být
alkoholicky	alkoholicky	k6eAd1	alkoholicky
zkvasitelný	zkvasitelný	k2eAgInSc1d1	zkvasitelný
<g/>
,	,	kIx,	,
teprve	teprve	k6eAd1	teprve
enzymaticky	enzymaticky	k6eAd1	enzymaticky
(	(	kIx(	(
<g/>
v	v	k7c6	v
trávicí	trávicí	k2eAgFnSc6d1	trávicí
soustavě	soustava	k1gFnSc6	soustava
živočichů	živočich	k1gMnPc2	živočich
včetně	včetně	k7c2	včetně
člověka	člověk	k1gMnSc2	člověk
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
odbourává	odbourávat	k5eAaImIp3nS	odbourávat
na	na	k7c4	na
zkvasitelné	zkvasitelný	k2eAgInPc4d1	zkvasitelný
sacharidy	sacharid	k1gInPc4	sacharid
<g/>
.	.	kIx.	.
</s>
<s>
Zahříváním	zahřívání	k1gNnSc7	zahřívání
škrobu	škrob	k1gInSc2	škrob
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
škrobový	škrobový	k2eAgInSc1d1	škrobový
maz	maz	k1gInSc1	maz
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc7	jeho
hydrolýzou	hydrolýza	k1gFnSc7	hydrolýza
vzniká	vznikat	k5eAaImIp3nS	vznikat
škrobový	škrobový	k2eAgInSc4d1	škrobový
sirup	sirup	k1gInSc4	sirup
<g/>
,	,	kIx,	,
škrobový	škrobový	k2eAgInSc4d1	škrobový
cukr	cukr	k1gInSc4	cukr
a	a	k8xC	a
glukóza	glukóza	k1gFnSc1	glukóza
<g/>
.	.	kIx.	.
</s>
<s>
Pražením	pražení	k1gNnSc7	pražení
škrobu	škrob	k1gInSc2	škrob
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
dextrin	dextrin	k1gInSc1	dextrin
<g/>
.	.	kIx.	.
</s>
<s>
Důkaz	důkaz	k1gInSc1	důkaz
škrobu	škrob	k1gInSc2	škrob
v	v	k7c6	v
neznámé	známý	k2eNgFnSc6d1	neznámá
látce	látka	k1gFnSc6	látka
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
roztokem	roztok	k1gInSc7	roztok
jódu	jód	k1gInSc2	jód
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
přítomnost	přítomnost	k1gFnSc1	přítomnost
prozrazuje	prozrazovat	k5eAaImIp3nS	prozrazovat
modrofialové	modrofialový	k2eAgNnSc4d1	modrofialové
zbarvení	zbarvení	k1gNnSc4	zbarvení
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
polysacharid	polysacharid	k1gInSc4	polysacharid
s	s	k7c7	s
funkcí	funkce	k1gFnSc7	funkce
zásobní	zásobní	k2eAgFnSc2d1	zásobní
látky	látka	k1gFnSc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Ukládá	ukládat	k5eAaImIp3nS	ukládat
se	se	k3xPyFc4	se
procesem	proces	k1gInSc7	proces
asimilací	asimilace	k1gFnPc2	asimilace
v	v	k7c6	v
zásobních	zásobní	k2eAgInPc6d1	zásobní
orgánech	orgán	k1gInPc6	orgán
rostlin	rostlina	k1gFnPc2	rostlina
(	(	kIx(	(
<g/>
semenech	semeno	k1gNnPc6	semeno
kukuřice	kukuřice	k1gFnSc2	kukuřice
<g/>
,	,	kIx,	,
pšenice	pšenice	k1gFnSc2	pšenice
<g/>
,	,	kIx,	,
rýže	rýže	k1gFnSc2	rýže
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
nebo	nebo	k8xC	nebo
v	v	k7c6	v
hlízách	hlíza	k1gFnPc6	hlíza
brambor	brambora	k1gFnPc2	brambora
<g/>
)	)	kIx)	)
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
škrobových	škrobový	k2eAgNnPc2d1	škrobové
zrn	zrno	k1gNnPc2	zrno
<g/>
.	.	kIx.	.
</s>
<s>
Zvláště	zvláště	k6eAd1	zvláště
bohaté	bohatý	k2eAgInPc1d1	bohatý
na	na	k7c4	na
škrob	škrob	k1gInSc4	škrob
jsou	být	k5eAaImIp3nP	být
brambory	brambora	k1gFnPc1	brambora
<g/>
,	,	kIx,	,
banány	banán	k1gInPc1	banán
<g/>
,	,	kIx,	,
obilniny	obilnina	k1gFnPc1	obilnina
a	a	k8xC	a
tapioka	tapioka	k1gFnSc1	tapioka
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
surovin	surovina	k1gFnPc2	surovina
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yQgInPc2	který
je	být	k5eAaImIp3nS	být
vyroben	vyroben	k2eAgInSc1d1	vyroben
<g/>
,	,	kIx,	,
rozeznáváme	rozeznávat	k5eAaImIp1nP	rozeznávat
škrob	škrob	k1gInSc4	škrob
bramborový	bramborový	k2eAgInSc4d1	bramborový
<g/>
,	,	kIx,	,
kukuřičný	kukuřičný	k2eAgInSc4d1	kukuřičný
<g/>
,	,	kIx,	,
pšeničný	pšeničný	k2eAgInSc4d1	pšeničný
<g/>
,	,	kIx,	,
rýžový	rýžový	k2eAgInSc4d1	rýžový
a	a	k8xC	a
jiné	jiný	k2eAgNnSc4d1	jiné
<g/>
.	.	kIx.	.
</s>
<s>
Získávání	získávání	k1gNnSc1	získávání
škrobu	škrob	k1gInSc2	škrob
je	být	k5eAaImIp3nS	být
mechanické	mechanický	k2eAgFnPc4d1	mechanická
–	–	k?	–
surovina	surovina	k1gFnSc1	surovina
je	být	k5eAaImIp3nS	být
rozdrcena	rozdrtit	k5eAaPmNgFnS	rozdrtit
a	a	k8xC	a
škrob	škrob	k1gInSc1	škrob
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
získán	získat	k5eAaPmNgInS	získat
vypíráním	vypírání	k1gNnSc7	vypírání
<g/>
.	.	kIx.	.
</s>
<s>
Škrob	škrob	k1gInSc1	škrob
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
například	například	k6eAd1	například
v	v	k7c6	v
potravinářství	potravinářství	k1gNnSc6	potravinářství
<g/>
,	,	kIx,	,
v	v	k7c6	v
kvasném	kvasný	k2eAgInSc6d1	kvasný
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
,	,	kIx,	,
ve	v	k7c6	v
farmacii	farmacie	k1gFnSc6	farmacie
<g/>
,	,	kIx,	,
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
lepidel	lepidlo	k1gNnPc2	lepidlo
<g/>
,	,	kIx,	,
nátěrů	nátěr	k1gInPc2	nátěr
a	a	k8xC	a
apretur	apretura	k1gFnPc2	apretura
a	a	k8xC	a
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
škrobových	škrobový	k2eAgInPc2d1	škrobový
derivátů	derivát	k1gInPc2	derivát
<g/>
.	.	kIx.	.
</s>
