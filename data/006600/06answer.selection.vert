<s>
Škrob	škrob	k1gInSc1	škrob
je	být	k5eAaImIp3nS	být
polysacharid	polysacharid	k1gInSc4	polysacharid
se	s	k7c7	s
vzorcem	vzorec	k1gInSc7	vzorec
(	(	kIx(	(
<g/>
C	C	kA	C
<g/>
6	[number]	k4	6
<g/>
H	H	kA	H
<g/>
10	[number]	k4	10
<g/>
O	o	k7c4	o
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
<g/>
n	n	k0	n
složený	složený	k2eAgInSc1d1	složený
z	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
různých	různý	k2eAgInPc2d1	různý
polysacharidů	polysacharid	k1gInPc2	polysacharid
<g/>
:	:	kIx,	:
amylózy	amylóza	k1gFnSc2	amylóza
a	a	k8xC	a
amylopektinu	amylopektin	k1gInSc2	amylopektin
<g/>
,	,	kIx,	,
tvořených	tvořený	k2eAgFnPc2d1	tvořená
několika	několik	k4yIc7	několik
tisíci	tisíc	k4xCgInPc7	tisíc
až	až	k8xS	až
desetitisíci	desetitisíce	k1gInPc7	desetitisíce
molekul	molekula	k1gFnPc2	molekula
glukózy	glukóza	k1gFnSc2	glukóza
<g/>
.	.	kIx.	.
</s>
