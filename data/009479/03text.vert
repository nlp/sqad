<p>
<s>
Ambra	ambra	k1gFnSc1	ambra
je	být	k5eAaImIp3nS	být
pevná	pevný	k2eAgFnSc1d1	pevná
<g/>
,	,	kIx,	,
voskovitá	voskovitý	k2eAgFnSc1d1	voskovitá
substance	substance	k1gFnSc1	substance
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
v	v	k7c6	v
trávicím	trávicí	k2eAgInSc6d1	trávicí
traktu	trakt	k1gInSc6	trakt
vorvaně	vorvaň	k1gMnSc2	vorvaň
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
domněnek	domněnka	k1gFnPc2	domněnka
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
napomáhá	napomáhat	k5eAaImIp3nS	napomáhat
k	k	k7c3	k
trávení	trávení	k1gNnSc3	trávení
krakatic	krakatice	k1gFnPc2	krakatice
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
naopak	naopak	k6eAd1	naopak
hypotéza	hypotéza	k1gFnSc1	hypotéza
<g/>
,	,	kIx,	,
že	že	k8xS	že
ostrý	ostrý	k2eAgInSc1d1	ostrý
zobák	zobák	k1gInSc1	zobák
zkonzumované	zkonzumovaný	k2eAgFnSc2d1	zkonzumovaná
krakatice	krakatice	k1gFnSc2	krakatice
usazený	usazený	k2eAgInSc4d1	usazený
ve	v	k7c6	v
střevě	střevo	k1gNnSc6	střevo
velryby	velryba	k1gFnSc2	velryba
vede	vést	k5eAaImIp3nS	vést
právě	právě	k6eAd1	právě
k	k	k7c3	k
produkci	produkce	k1gFnSc3	produkce
ambry	ambra	k1gFnSc2	ambra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
vyloučena	vyloučit	k5eAaPmNgFnS	vyloučit
z	z	k7c2	z
těla	tělo	k1gNnSc2	tělo
vorvaně	vorvaň	k1gMnSc2	vorvaň
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
trusem	trus	k1gInSc7	trus
a	a	k8xC	a
plave	plavat	k5eAaImIp3nS	plavat
na	na	k7c6	na
hladině	hladina	k1gFnSc6	hladina
jako	jako	k9	jako
velký	velký	k2eAgInSc1d1	velký
<g/>
,	,	kIx,	,
šedočerný	šedočerný	k2eAgInSc1d1	šedočerný
chomáč	chomáč	k1gInSc1	chomáč
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
ji	on	k3xPp3gFnSc4	on
lze	lze	k6eAd1	lze
také	také	k9	také
nalézt	nalézt	k5eAaBmF	nalézt
jako	jako	k8xS	jako
usazeninu	usazenina	k1gFnSc4	usazenina
na	na	k7c6	na
skaliscích	skalisko	k1gNnPc6	skalisko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Čerstvá	čerstvý	k2eAgFnSc1d1	čerstvá
ambra	ambra	k1gFnSc1	ambra
nepříjemně	příjemně	k6eNd1	příjemně
zapáchá	zapáchat	k5eAaImIp3nS	zapáchat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jak	jak	k6eAd1	jak
stárne	stárnout	k5eAaImIp3nS	stárnout
<g/>
,	,	kIx,	,
změní	změnit	k5eAaPmIp3nS	změnit
se	se	k3xPyFc4	se
aroma	aroma	k1gNnSc1	aroma
na	na	k7c4	na
příjemné	příjemný	k2eAgNnSc4d1	příjemné
a	a	k8xC	a
zemité	zemitý	k2eAgNnSc4d1	zemité
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
barva	barva	k1gFnSc1	barva
se	se	k3xPyFc4	se
změní	změnit	k5eAaPmIp3nS	změnit
z	z	k7c2	z
šedočerné	šedočerný	k2eAgFnSc2d1	šedočerná
na	na	k7c4	na
oranžovo	oranžovo	k1gNnSc4	oranžovo
žlutou	žlutý	k2eAgFnSc7d1	žlutá
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
jantarovou	jantarový	k2eAgFnSc4d1	jantarová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přestože	přestože	k8xS	přestože
již	již	k6eAd1	již
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
parfumerii	parfumerie	k1gFnSc6	parfumerie
většinou	většinou	k6eAd1	většinou
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
syntetickými	syntetický	k2eAgInPc7d1	syntetický
materiály	materiál	k1gInPc7	materiál
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
je	být	k5eAaImIp3nS	být
vysoce	vysoce	k6eAd1	vysoce
ceněna	cenit	k5eAaImNgFnS	cenit
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
placena	platit	k5eAaImNgFnS	platit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
parfumérie	parfumérie	k1gFnSc1	parfumérie
–	–	k?	–
užívá	užívat	k5eAaImIp3nS	užívat
se	se	k3xPyFc4	se
jako	jako	k9	jako
ustalovač	ustalovač	k1gInSc1	ustalovač
</s>
</p>
<p>
<s>
potravinářství	potravinářství	k1gNnSc1	potravinářství
–	–	k?	–
užívá	užívat	k5eAaImIp3nS	užívat
se	se	k3xPyFc4	se
jako	jako	k9	jako
aroma	aroma	k1gNnSc1	aroma
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
ambra	ambra	k1gFnSc1	ambra
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
ambra	ambra	k1gFnSc1	ambra
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
