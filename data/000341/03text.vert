<s>
Půlnoc	půlnoc	k1gFnSc1	půlnoc
neboli	neboli	k8xC	neboli
polovina	polovina	k1gFnSc1	polovina
noci	noc	k1gFnSc2	noc
je	být	k5eAaImIp3nS	být
okamžik	okamžik	k1gInSc4	okamžik
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
Slunce	slunce	k1gNnSc1	slunce
leží	ležet	k5eAaImIp3nS	ležet
nejdále	daleko	k6eAd3	daleko
od	od	k7c2	od
zenitu	zenit	k1gInSc2	zenit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pásu	pás	k1gInSc6	pás
mezi	mezi	k7c7	mezi
polárními	polární	k2eAgInPc7d1	polární
kruhy	kruh	k1gInPc7	kruh
se	se	k3xPyFc4	se
Slunce	slunce	k1gNnSc1	slunce
v	v	k7c4	v
tento	tento	k3xDgInSc4	tento
okamžik	okamžik	k1gInSc4	okamžik
nachází	nacházet	k5eAaImIp3nS	nacházet
pod	pod	k7c7	pod
horizontem	horizont	k1gInSc7	horizont
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
není	být	k5eNaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
<g/>
.	.	kIx.	.
</s>
<s>
Protikladem	protiklad	k1gInSc7	protiklad
půlnoci	půlnoc	k1gFnSc2	půlnoc
je	být	k5eAaImIp3nS	být
poledne	poledne	k1gNnSc4	poledne
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
pohybu	pohyb	k1gInSc3	pohyb
Země	zem	k1gFnSc2	zem
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
není	být	k5eNaImIp3nS	být
doba	doba	k1gFnSc1	doba
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgFnPc7d1	jednotlivá
půlnocemi	půlnoc	k1gFnPc7	půlnoc
konstantní	konstantní	k2eAgFnSc7d1	konstantní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
ještě	ještě	k6eAd1	ještě
jeden	jeden	k4xCgInSc4	jeden
okamžik	okamžik	k1gInSc4	okamžik
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
půlnocí	půlnoc	k1gFnSc7	půlnoc
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
místního	místní	k2eAgInSc2d1	místní
slunečního	sluneční	k2eAgInSc2d1	sluneční
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
místní	místní	k2eAgInSc1d1	místní
sluneční	sluneční	k2eAgInSc1d1	sluneční
čas	čas	k1gInSc1	čas
může	moct	k5eAaImIp3nS	moct
od	od	k7c2	od
místního	místní	k2eAgInSc2d1	místní
času	čas	k1gInSc2	čas
lišit	lišit	k5eAaImF	lišit
a	a	k8xC	a
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
i	i	k9	i
délka	délka	k1gFnSc1	délka
slunečního	sluneční	k2eAgInSc2d1	sluneční
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
liší	lišit	k5eAaImIp3nP	lišit
se	se	k3xPyFc4	se
více	hodně	k6eAd2	hodně
nebo	nebo	k8xC	nebo
méně	málo	k6eAd2	málo
i	i	k9	i
takto	takto	k6eAd1	takto
definovaná	definovaný	k2eAgFnSc1d1	definovaná
půlnoc	půlnoc	k1gFnSc1	půlnoc
od	od	k7c2	od
půlnoci	půlnoc	k1gFnSc2	půlnoc
skutečné	skutečný	k2eAgFnSc2d1	skutečná
<g/>
.	.	kIx.	.
</s>
<s>
Půlnoc	půlnoc	k1gFnSc1	půlnoc
tvoří	tvořit	k5eAaImIp3nS	tvořit
hranici	hranice	k1gFnSc4	hranice
mezi	mezi	k7c7	mezi
kalendářními	kalendářní	k2eAgInPc7d1	kalendářní
dny	den	k1gInPc7	den
<g/>
.	.	kIx.	.
</s>
<s>
Přesně	přesně	k6eAd1	přesně
o	o	k7c6	o
půlnoci	půlnoc	k1gFnSc6	půlnoc
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
datum	datum	k1gNnSc1	datum
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
půlnoc	půlnoc	k1gFnSc4	půlnoc
hranicí	hranice	k1gFnSc7	hranice
mezi	mezi	k7c4	mezi
dny	den	k1gInPc4	den
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
čas	čas	k1gInSc4	čas
půlnoci	půlnoc	k1gFnSc2	půlnoc
zapsat	zapsat	k5eAaPmF	zapsat
dvěma	dva	k4xCgInPc7	dva
různými	různý	k2eAgInPc7d1	různý
způsoby	způsob	k1gInPc7	způsob
<g/>
.	.	kIx.	.
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
"	"	kIx"	"
<g/>
starého	starý	k2eAgInSc2d1	starý
dne	den	k1gInSc2	den
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
čas	čas	k1gInSc1	čas
24	[number]	k4	24
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
"	"	kIx"	"
<g/>
nového	nový	k2eAgInSc2d1	nový
dne	den	k1gInSc2	den
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
čas	čas	k1gInSc1	čas
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
dopravě	doprava	k1gFnSc6	doprava
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
v	v	k7c6	v
jízdním	jízdní	k2eAgInSc6d1	jízdní
řádu	řád	k1gInSc6	řád
je	být	k5eAaImIp3nS	být
dojezd	dojezd	k1gInSc4	dojezd
o	o	k7c6	o
půlnoci	půlnoc	k1gFnSc6	půlnoc
vyznačen	vyznačit	k5eAaPmNgInS	vyznačit
časem	čas	k1gInSc7	čas
24	[number]	k4	24
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
a	a	k8xC	a
odjezd	odjezd	k1gInSc4	odjezd
o	o	k7c6	o
půlnoci	půlnoc	k1gFnSc6	půlnoc
je	být	k5eAaImIp3nS	být
označen	označit	k5eAaPmNgInS	označit
časem	čas	k1gInSc7	čas
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
půlnoc	půlnoc	k1gFnSc1	půlnoc
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
půlnoc	půlnoc	k1gFnSc1	půlnoc
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
