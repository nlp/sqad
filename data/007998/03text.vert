<s>
Alexander	Alexandra	k1gFnPc2	Alexandra
Graham	Graham	k1gMnSc1	Graham
Bell	bell	k1gInSc1	bell
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1847	[number]	k4	1847
<g/>
,	,	kIx,	,
Edinburgh	Edinburgh	k1gInSc1	Edinburgh
<g/>
,	,	kIx,	,
Skotsko	Skotsko	k1gNnSc1	Skotsko
–	–	k?	–
2	[number]	k4	2
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1922	[number]	k4	1922
<g/>
,	,	kIx,	,
Baddeck	Baddeck	k1gInSc1	Baddeck
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
skotsko-americký	skotskomerický	k2eAgMnSc1d1	skotsko-americký
vědec	vědec	k1gMnSc1	vědec
a	a	k8xC	a
vynálezce	vynálezce	k1gMnSc1	vynálezce
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgInS	zabývat
tvorbou	tvorba	k1gFnSc7	tvorba
lidské	lidský	k2eAgFnSc2d1	lidská
řeči	řeč	k1gFnSc2	řeč
<g/>
,	,	kIx,	,
výchovou	výchova	k1gFnSc7	výchova
hluchoněmých	hluchoněmý	k2eAgInPc2d1	hluchoněmý
a	a	k8xC	a
elektromagnetickým	elektromagnetický	k2eAgInSc7d1	elektromagnetický
přenosem	přenos	k1gInSc7	přenos
řeči	řeč	k1gFnSc2	řeč
<g/>
.	.	kIx.	.
</s>
<s>
Vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
mikrofon	mikrofon	k1gInSc4	mikrofon
<g/>
,	,	kIx,	,
zkonstruoval	zkonstruovat	k5eAaPmAgInS	zkonstruovat
první	první	k4xOgInSc1	první
použitelný	použitelný	k2eAgInSc1d1	použitelný
telefon	telefon	k1gInSc1	telefon
(	(	kIx(	(
<g/>
1876	[number]	k4	1876
<g/>
)	)	kIx)	)
a	a	k8xC	a
gramofon	gramofon	k1gInSc1	gramofon
(	(	kIx(	(
<g/>
s	s	k7c7	s
A.	A.	kA	A.
C.	C.	kA	C.
Bellem	bell	k1gInSc7	bell
a	a	k8xC	a
S.	S.	kA	S.
Tairotem	Tairot	k1gMnSc7	Tairot
<g/>
,	,	kIx,	,
1883	[number]	k4	1883
<g/>
)	)	kIx)	)
a	a	k8xC	a
řadu	řada	k1gFnSc4	řada
dalších	další	k2eAgInPc2d1	další
přístrojů	přístroj	k1gInPc2	přístroj
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
také	také	k9	také
zakladatelem	zakladatel	k1gMnSc7	zakladatel
koncernu	koncern	k1gInSc2	koncern
Bell	bell	k1gInSc1	bell
a	a	k8xC	a
pozdějšho	pozdějšze	k6eAd1	pozdějšze
AT	AT	kA	AT
<g/>
&	&	k?	&
<g/>
T.	T.	kA	T.
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
profesora	profesor	k1gMnSc2	profesor
fonetiky	fonetika	k1gFnSc2	fonetika
A.	A.	kA	A.
M.	M.	kA	M.
Bella	Bella	k1gMnSc1	Bella
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
jako	jako	k8xS	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgMnPc2	první
fonetickou	fonetický	k2eAgFnSc4d1	fonetická
abecedu	abeceda	k1gFnSc4	abeceda
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
A.	A.	kA	A.
Bell	bell	k1gInSc1	bell
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgMnS	zabývat
fyziologií	fyziologie	k1gFnSc7	fyziologie
řeči	řeč	k1gFnSc2	řeč
<g/>
.	.	kIx.	.
</s>
<s>
Bellova	Bellův	k2eAgFnSc1d1	Bellova
matka	matka	k1gFnSc1	matka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
své	svůj	k3xOyFgMnPc4	svůj
tři	tři	k4xCgMnPc4	tři
syny	syn	k1gMnPc4	syn
vychovávala	vychovávat	k5eAaImAgFnS	vychovávat
<g/>
,	,	kIx,	,
brzy	brzy	k6eAd1	brzy
ohluchla	ohluchnout	k5eAaPmAgFnS	ohluchnout
a	a	k8xC	a
mladý	mladý	k1gMnSc1	mladý
Alexander	Alexandra	k1gFnPc2	Alexandra
se	se	k3xPyFc4	se
naučil	naučit	k5eAaPmAgInS	naučit
znakový	znakový	k2eAgInSc1d1	znakový
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
mohl	moct	k5eAaImAgMnS	moct
komunikovat	komunikovat	k5eAaImF	komunikovat
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
se	se	k3xPyFc4	se
naučil	naučit	k5eAaPmAgMnS	naučit
hrát	hrát	k5eAaImF	hrát
na	na	k7c4	na
klavír	klavír	k1gInSc4	klavír
a	a	k8xC	a
brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
zajímat	zajímat	k5eAaImF	zajímat
o	o	k7c4	o
akustiku	akustika	k1gFnSc4	akustika
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
10	[number]	k4	10
let	léto	k1gNnPc2	léto
studoval	studovat	k5eAaImAgMnS	studovat
klasické	klasický	k2eAgInPc4d1	klasický
jazyky	jazyk	k1gInPc4	jazyk
v	v	k7c6	v
Edinburku	Edinburk	k1gInSc6	Edinburk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zajímal	zajímat	k5eAaImAgMnS	zajímat
se	se	k3xPyFc4	se
hlavně	hlavně	k9	hlavně
o	o	k7c4	o
vědu	věda	k1gFnSc4	věda
a	a	k8xC	a
vynálezy	vynález	k1gInPc4	vynález
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
učitel	učitel	k1gMnSc1	učitel
sluchově	sluchově	k6eAd1	sluchově
postižených	postižený	k1gMnPc2	postižený
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1865	[number]	k4	1865
následoval	následovat	k5eAaImAgInS	následovat
svého	svůj	k3xOyFgMnSc4	svůj
otce	otec	k1gMnSc4	otec
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
působil	působit	k5eAaImAgInS	působit
jako	jako	k9	jako
demostrátor	demostrátor	k1gInSc1	demostrátor
na	na	k7c4	na
University	universita	k1gFnPc4	universita
College	College	k1gFnPc2	College
London	London	k1gMnSc1	London
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
také	také	k9	také
studoval	studovat	k5eAaImAgMnS	studovat
anatomii	anatomie	k1gFnSc4	anatomie
a	a	k8xC	a
fyziologii	fyziologie	k1gFnSc4	fyziologie
<g/>
,	,	kIx,	,
seznámil	seznámit	k5eAaPmAgMnS	seznámit
se	se	k3xPyFc4	se
s	s	k7c7	s
různými	různý	k2eAgInPc7d1	různý
pokusy	pokus	k1gInPc7	pokus
o	o	k7c4	o
umělou	umělý	k2eAgFnSc4d1	umělá
řeč	řeč	k1gFnSc4	řeč
i	i	k9	i
s	s	k7c7	s
dílem	dílo	k1gNnSc7	dílo
německého	německý	k2eAgMnSc2d1	německý
fyzika	fyzik	k1gMnSc2	fyzik
Hermanna	Hermann	k1gMnSc2	Hermann
Helmholtze	Helmholtze	k1gFnSc2	Helmholtze
o	o	k7c6	o
fyziologii	fyziologie	k1gFnSc6	fyziologie
vnímání	vnímání	k1gNnSc2	vnímání
zvuku	zvuk	k1gInSc2	zvuk
a	a	k8xC	a
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1868	[number]	k4	1868
a	a	k8xC	a
1870	[number]	k4	1870
zemřeli	zemřít	k5eAaPmAgMnP	zemřít
oba	dva	k4xCgMnPc4	dva
Alexandrovi	Alexandrův	k2eAgMnPc1d1	Alexandrův
bratři	bratr	k1gMnPc1	bratr
na	na	k7c6	na
tuberkulozu	tuberkuloz	k1gInSc6	tuberkuloz
a	a	k8xC	a
otec	otec	k1gMnSc1	otec
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
rodina	rodina	k1gFnSc1	rodina
odstěhuje	odstěhovat	k5eAaPmIp3nS	odstěhovat
za	za	k7c7	za
lepším	dobrý	k2eAgNnSc7d2	lepší
klimatem	klima	k1gNnSc7	klima
do	do	k7c2	do
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1871	[number]	k4	1871
dostal	dostat	k5eAaPmAgInS	dostat
Bell	bell	k1gInSc1	bell
pozvání	pozvání	k1gNnSc2	pozvání
do	do	k7c2	do
Bostonu	Boston	k1gInSc2	Boston
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
začal	začít	k5eAaPmAgInS	začít
s	s	k7c7	s
úspěchem	úspěch	k1gInSc7	úspěch
učit	učit	k5eAaImF	učit
neslyšící	slyšící	k2eNgFnPc4d1	neslyšící
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
byla	být	k5eAaImAgFnS	být
i	i	k9	i
pozěji	pozě	k6eAd2	pozě
slavná	slavný	k2eAgFnSc1d1	slavná
Helen	Helena	k1gFnPc2	Helena
Kellerová	Kellerová	k1gFnSc1	Kellerová
<g/>
,	,	kIx,	,
neslyšící	slyšící	k2eNgFnSc1d1	neslyšící
i	i	k8xC	i
nevidomá	vidomý	k2eNgFnSc1d1	nevidomá
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
jeho	jeho	k3xOp3gFnSc1	jeho
pozdější	pozdní	k2eAgFnSc1d2	pozdější
žena	žena	k1gFnSc1	žena
<g/>
,	,	kIx,	,
neslyšící	slyšící	k2eNgMnSc1d1	neslyšící
Mabel	Mabel	k1gMnSc1	Mabel
Hubbard	Hubbard	k1gMnSc1	Hubbard
<g/>
.	.	kIx.	.
</s>
<s>
Bell	bell	k1gInSc1	bell
byl	být	k5eAaImAgInS	být
přesvědčen	přesvědčit	k5eAaPmNgInS	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
neslyšící	slyšící	k2eNgMnSc1d1	neslyšící
naučí	naučit	k5eAaPmIp3nS	naučit
mluvit	mluvit	k5eAaImF	mluvit
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
tedy	tedy	k9	tedy
proti	proti	k7c3	proti
výuce	výuka	k1gFnSc3	výuka
znakového	znakový	k2eAgInSc2d1	znakový
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mu	on	k3xPp3gMnSc3	on
později	pozdě	k6eAd2	pozdě
mnozí	mnohý	k2eAgMnPc1d1	mnohý
vytýkali	vytýkat	k5eAaImAgMnP	vytýkat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1873-1876	[number]	k4	1873-1876
byl	být	k5eAaImAgInS	být
Bell	bell	k1gInSc1	bell
profesorem	profesor	k1gMnSc7	profesor
výslovnosti	výslovnost	k1gFnSc2	výslovnost
a	a	k8xC	a
fyziologie	fyziologie	k1gFnSc2	fyziologie
řeči	řeč	k1gFnSc2	řeč
na	na	k7c6	na
Bostonské	bostonský	k2eAgFnSc6d1	Bostonská
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
zrušit	zrušit	k5eAaPmF	zrušit
svou	svůj	k3xOyFgFnSc4	svůj
prosperující	prosperující	k2eAgFnSc4d1	prosperující
soukromou	soukromý	k2eAgFnSc4d1	soukromá
praxi	praxe	k1gFnSc4	praxe
a	a	k8xC	a
přijal	přijmout	k5eAaPmAgMnS	přijmout
nabídku	nabídka	k1gFnSc4	nabídka
bohatých	bohatý	k2eAgMnPc2d1	bohatý
rodičů	rodič	k1gMnPc2	rodič
dvou	dva	k4xCgFnPc2	dva
svých	svůj	k3xOyFgMnPc2	svůj
žáků	žák	k1gMnPc2	žák
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
si	se	k3xPyFc3	se
u	u	k7c2	u
nich	on	k3xPp3gMnPc2	on
zřídil	zřídit	k5eAaPmAgMnS	zřídit
laboratoř	laboratoř	k1gFnSc4	laboratoř
a	a	k8xC	a
věnoval	věnovat	k5eAaImAgMnS	věnovat
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInPc3	svůj
pokusům	pokus	k1gInPc3	pokus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1874	[number]	k4	1874
se	se	k3xPyFc4	se
telegrafní	telegrafní	k2eAgInSc1d1	telegrafní
provoz	provoz	k1gInSc1	provoz
neobyčejně	obyčejně	k6eNd1	obyčejně
rychle	rychle	k6eAd1	rychle
rozšířil	rozšířit	k5eAaPmAgMnS	rozšířit
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
slovy	slovo	k1gNnPc7	slovo
presidenta	president	k1gMnSc2	president
Western	Western	kA	Western
Union	union	k1gInSc1	union
"	"	kIx"	"
<g/>
nervovou	nervový	k2eAgFnSc7d1	nervová
soustavou	soustava	k1gFnSc7	soustava
obchodu	obchod	k1gInSc2	obchod
<g/>
"	"	kIx"	"
a	a	k8xC	a
Bellovi	Bell	k1gMnSc3	Bell
mecenášové	mecenášové	k?	mecenášové
byli	být	k5eAaImAgMnP	být
ochotni	ochoten	k2eAgMnPc1d1	ochoten
jeho	jeho	k3xOp3gInPc1	jeho
výzkumy	výzkum	k1gInPc1	výzkum
financovat	financovat	k5eAaBmF	financovat
<g/>
.	.	kIx.	.
</s>
<s>
Bell	bell	k1gInSc1	bell
tak	tak	k9	tak
mohl	moct	k5eAaImAgInS	moct
najmout	najmout	k5eAaPmF	najmout
inženýra	inženýr	k1gMnSc4	inženýr
T.	T.	kA	T.
A.	A.	kA	A.
Watsona	Watson	k1gMnSc2	Watson
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
jeho	jeho	k3xOp3gInPc4	jeho
nápady	nápad	k1gInPc4	nápad
realizoval	realizovat	k5eAaBmAgMnS	realizovat
<g/>
.	.	kIx.	.
</s>
<s>
Experimentoval	experimentovat	k5eAaImAgMnS	experimentovat
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
se	s	k7c7	s
záznamem	záznam	k1gInSc7	záznam
zvuku	zvuk	k1gInSc2	zvuk
na	na	k7c4	na
začazené	začazený	k2eAgNnSc4d1	začazené
sklo	sklo	k1gNnSc4	sklo
a	a	k8xC	a
měl	mít	k5eAaImAgMnS	mít
představy	představa	k1gFnPc4	představa
o	o	k7c6	o
akustickém	akustický	k2eAgInSc6d1	akustický
telegrafu	telegraf	k1gInSc6	telegraf
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
si	se	k3xPyFc3	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1876	[number]	k4	1876
chtěl	chtít	k5eAaImAgMnS	chtít
nechat	nechat	k5eAaPmF	nechat
patentovat	patentovat	k5eAaBmF	patentovat
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
však	však	k9	však
patentovou	patentový	k2eAgFnSc4d1	patentová
přihlášku	přihláška	k1gFnSc4	přihláška
přinesl	přinést	k5eAaPmAgMnS	přinést
i	i	k9	i
Elisha	Elisha	k1gMnSc1	Elisha
Gray	Graa	k1gMnSc2	Graa
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgInS	být
s	s	k7c7	s
realizací	realizace	k1gFnSc7	realizace
myšlenky	myšlenka	k1gFnSc2	myšlenka
patrně	patrně	k6eAd1	patrně
dál	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
roku	rok	k1gInSc2	rok
1871	[number]	k4	1871
podal	podat	k5eAaPmAgInS	podat
podobnou	podobný	k2eAgFnSc4d1	podobná
předběžnou	předběžný	k2eAgFnSc4d1	předběžná
přihlášku	přihláška	k1gFnSc4	přihláška
(	(	kIx(	(
<g/>
caveat	caveat	k2eAgMnSc1d1	caveat
<g/>
)	)	kIx)	)
italský	italský	k2eAgMnSc1d1	italský
vědec	vědec	k1gMnSc1	vědec
a	a	k8xC	a
vynálezce	vynálezce	k1gMnSc1	vynálezce
Antonio	Antonio	k1gMnSc1	Antonio
Meucci	Meucce	k1gMnSc3	Meucce
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
o	o	k7c6	o
ní	on	k3xPp3gFnSc6	on
psal	psát	k5eAaImAgInS	psát
v	v	k7c6	v
italském	italský	k2eAgInSc6d1	italský
časopise	časopis	k1gInSc6	časopis
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
o	o	k7c4	o
americký	americký	k2eAgInSc4d1	americký
patent	patent	k1gInSc4	patent
nakonec	nakonec	k6eAd1	nakonec
nepožádal	požádat	k5eNaPmAgMnS	požádat
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
neuměl	umět	k5eNaImAgMnS	umět
anglicky	anglicky	k6eAd1	anglicky
<g/>
.	.	kIx.	.
</s>
<s>
Popisy	popis	k1gInPc4	popis
vynálezu	vynález	k1gInSc2	vynález
v	v	k7c6	v
přihláškách	přihláška	k1gFnPc6	přihláška
nebyly	být	k5eNaImAgFnP	být
příliš	příliš	k6eAd1	příliš
zřetelné	zřetelný	k2eAgFnPc1d1	zřetelná
a	a	k8xC	a
jasné	jasný	k2eAgFnPc1d1	jasná
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
vážné	vážný	k2eAgNnSc4d1	vážné
podezření	podezření	k1gNnSc4	podezření
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
posuzovatelů	posuzovatel	k1gMnPc2	posuzovatel
v	v	k7c6	v
patentovém	patentový	k2eAgInSc6d1	patentový
úřadě	úřad	k1gInSc6	úřad
ukázal	ukázat	k5eAaPmAgInS	ukázat
Bellovým	Bellová	k1gFnPc3	Bellová
lidem	člověk	k1gMnPc3	člověk
obě	dva	k4xCgFnPc4	dva
konkurenční	konkurenční	k2eAgFnPc4d1	konkurenční
přihlášky	přihláška	k1gFnPc4	přihláška
<g/>
.	.	kIx.	.
</s>
<s>
Spory	spor	k1gInPc1	spor
o	o	k7c4	o
prioritu	priorita	k1gFnSc4	priorita
se	se	k3xPyFc4	se
táhly	táhnout	k5eAaImAgInP	táhnout
více	hodně	k6eAd2	hodně
než	než	k8xS	než
sto	sto	k4xCgNnSc4	sto
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
americký	americký	k2eAgInSc1d1	americký
kongres	kongres	k1gInSc1	kongres
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
priorota	priorota	k1gFnSc1	priorota
patří	patřit	k5eAaImIp3nS	patřit
Meuccimu	Meuccima	k1gFnSc4	Meuccima
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
1876	[number]	k4	1876
začal	začít	k5eAaPmAgInS	začít
Bell	bell	k1gInSc1	bell
svůj	svůj	k3xOyFgInSc4	svůj
vynález	vynález	k1gInSc4	vynález
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
mezitím	mezitím	k6eAd1	mezitím
podstatně	podstatně	k6eAd1	podstatně
zdokonalil	zdokonalit	k5eAaPmAgMnS	zdokonalit
<g/>
,	,	kIx,	,
propagovat	propagovat	k5eAaImF	propagovat
a	a	k8xC	a
předvádět	předvádět	k5eAaImF	předvádět
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
společníky	společník	k1gMnPc7	společník
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
telefon	telefon	k1gInSc4	telefon
telegrafní	telegrafní	k2eAgInSc4d1	telegrafní
firmě	firma	k1gFnSc6	firma
Western	Western	kA	Western
Union	union	k1gInSc1	union
za	za	k7c4	za
sto	sto	k4xCgNnSc4	sto
tisíc	tisíc	k4xCgInPc2	tisíc
USD	USD	kA	USD
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
to	ten	k3xDgNnSc4	ten
však	však	k9	však
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
její	její	k3xOp3gMnSc1	její
president	president	k1gMnSc1	president
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
za	za	k7c2	za
něj	on	k3xPp3gMnSc2	on
rád	rád	k6eAd1	rád
zaplatil	zaplatit	k5eAaPmAgInS	zaplatit
25	[number]	k4	25
milionů	milion	k4xCgInPc2	milion
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Bell	bell	k1gInSc1	bell
jej	on	k3xPp3gInSc4	on
už	už	k6eAd1	už
prodat	prodat	k5eAaPmF	prodat
nechtěl	chtít	k5eNaImAgMnS	chtít
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
na	na	k7c6	na
stoleté	stoletý	k2eAgFnSc6d1	stoletá
jubilejní	jubilejní	k2eAgFnSc6d1	jubilejní
výstavě	výstava	k1gFnSc6	výstava
ve	v	k7c6	v
Filadelfii	Filadelfie	k1gFnSc6	Filadelfie
se	se	k3xPyFc4	se
o	o	k7c4	o
telefon	telefon	k1gInSc4	telefon
začaly	začít	k5eAaPmAgFnP	začít
zajímat	zajímat	k5eAaImF	zajímat
slavné	slavný	k2eAgFnPc1d1	slavná
osoby	osoba	k1gFnPc1	osoba
<g/>
,	,	kIx,	,
brazilský	brazilský	k2eAgMnSc1d1	brazilský
císař	císař	k1gMnSc1	císař
Pedro	Pedro	k1gNnSc4	Pedro
II	II	kA	II
<g/>
.	.	kIx.	.
nebo	nebo	k8xC	nebo
Sir	sir	k1gMnSc1	sir
William	William	k1gInSc4	William
Thomson	Thomson	k1gMnSc1	Thomson
<g/>
,	,	kIx,	,
pozdější	pozdní	k2eAgMnSc1d2	pozdější
Lord	lord	k1gMnSc1	lord
Kelvin	kelvin	k1gInSc4	kelvin
<g/>
,	,	kIx,	,
a	a	k8xC	a
Bell	bell	k1gInSc1	bell
mohl	moct	k5eAaImAgInS	moct
svůj	svůj	k3xOyFgInSc4	svůj
telefon	telefon	k1gInSc4	telefon
předvést	předvést	k5eAaPmF	předvést
i	i	k8xC	i
královně	královna	k1gFnSc3	královna
Viktorii	Viktoria	k1gFnSc3	Viktoria
v	v	k7c6	v
jejím	její	k3xOp3gNnSc6	její
sídle	sídlo	k1gNnSc6	sídlo
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Wight	Wight	k1gInSc1	Wight
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1877	[number]	k4	1877
se	se	k3xPyFc4	se
Bell	bell	k1gInSc1	bell
oženil	oženit	k5eAaPmAgInS	oženit
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
bývalou	bývalý	k2eAgFnSc7d1	bývalá
neslyšící	slyšící	k2eNgFnSc7d1	neslyšící
žačkou	žačka	k1gFnSc7	žačka
Mabel	Mabel	k1gMnSc1	Mabel
Hubbard	Hubbard	k1gMnSc1	Hubbard
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
měl	mít	k5eAaImAgMnS	mít
později	pozdě	k6eAd2	pozdě
dvě	dva	k4xCgFnPc4	dva
dcery	dcera	k1gFnPc4	dcera
<g/>
,	,	kIx,	,
a	a	k8xC	a
po	po	k7c6	po
svatbě	svatba	k1gFnSc6	svatba
odjeli	odjet	k5eAaPmAgMnP	odjet
na	na	k7c4	na
roční	roční	k2eAgFnSc4d1	roční
cestu	cesta	k1gFnSc4	cesta
po	po	k7c6	po
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
také	také	k9	také
propagoval	propagovat	k5eAaImAgMnS	propagovat
svůj	svůj	k3xOyFgInSc4	svůj
telefon	telefon	k1gInSc4	telefon
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
Bell	bell	k1gInSc1	bell
Telephone	Telephon	k1gInSc5	Telephon
Company	Compana	k1gFnSc2	Compana
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
roku	rok	k1gInSc2	rok
1879	[number]	k4	1879
koupila	koupit	k5eAaPmAgFnS	koupit
Edisonův	Edisonův	k2eAgInSc4d1	Edisonův
patent	patent	k1gInSc4	patent
na	na	k7c4	na
uhlíkový	uhlíkový	k2eAgInSc4d1	uhlíkový
mikrofon	mikrofon	k1gInSc4	mikrofon
a	a	k8xC	a
roku	rok	k1gInSc3	rok
1886	[number]	k4	1886
už	už	k9	už
měla	mít	k5eAaImAgFnS	mít
v	v	k7c6	v
USA	USA	kA	USA
150	[number]	k4	150
tisíc	tisíc	k4xCgInSc1	tisíc
přípojek	přípojka	k1gFnPc2	přípojka
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1880	[number]	k4	1880
udělila	udělit	k5eAaPmAgFnS	udělit
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
akademie	akademie	k1gFnSc1	akademie
Bellovi	Bell	k1gMnSc3	Bell
Voltovu	Voltův	k2eAgFnSc4d1	Voltova
cenu	cena	k1gFnSc4	cena
(	(	kIx(	(
<g/>
asi	asi	k9	asi
6	[number]	k4	6
milionů	milion	k4xCgInPc2	milion
Kč	Kč	kA	Kč
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
mohl	moct	k5eAaImAgMnS	moct
financovat	financovat	k5eAaBmF	financovat
The	The	k1gFnSc4	The
Volta	Volta	k1gFnSc1	Volta
Laboratory	Laborator	k1gMnPc4	Laborator
a	a	k8xC	a
své	svůj	k3xOyFgMnPc4	svůj
další	další	k2eAgMnPc4d1	další
výzkumy	výzkum	k1gInPc4	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
14	[number]	k4	14
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1876	[number]	k4	1876
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
sobě	sebe	k3xPyFc6	sebe
přihlásili	přihlásit	k5eAaPmAgMnP	přihlásit
Američan	Američan	k1gMnSc1	Američan
Elisha	Elisha	k1gMnSc1	Elisha
Gray	Graa	k1gFnSc2	Graa
a	a	k8xC	a
ze	z	k7c2	z
Skotska	Skotsko	k1gNnSc2	Skotsko
pocházející	pocházející	k2eAgMnSc1d1	pocházející
fyziolog	fyziolog	k1gMnSc1	fyziolog
Alexander	Alexandra	k1gFnPc2	Alexandra
Bell	bell	k1gInSc1	bell
své	svůj	k3xOyFgInPc4	svůj
patenty	patent	k1gInPc4	patent
na	na	k7c4	na
nový	nový	k2eAgInSc4d1	nový
telefon	telefon	k1gInSc4	telefon
<g/>
.	.	kIx.	.
</s>
<s>
Gray	Gra	k1gMnPc4	Gra
vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
svůj	svůj	k3xOyFgInSc4	svůj
přístroj	přístroj	k1gInSc4	přístroj
už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1874	[number]	k4	1874
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
mikrofon	mikrofon	k1gInSc1	mikrofon
byl	být	k5eAaImAgInS	být
podobný	podobný	k2eAgInSc1d1	podobný
mikrofonu	mikrofon	k1gInSc2	mikrofon
Johanna	Johann	k1gMnSc4	Johann
Philippa	Philipp	k1gMnSc4	Philipp
Reise	Reis	k1gMnSc4	Reis
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byl	být	k5eAaImAgInS	být
zlepšen	zlepšit	k5eAaPmNgInS	zlepšit
podle	podle	k7c2	podle
principu	princip	k1gInSc2	princip
kapalinového	kapalinový	k2eAgInSc2d1	kapalinový
mikrofonu	mikrofon	k1gInSc2	mikrofon
Angličana	Angličan	k1gMnSc2	Angličan
Jeatese	Jeatese	k1gFnSc2	Jeatese
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
sluchátku	sluchátko	k1gNnSc6	sluchátko
použil	použít	k5eAaPmAgMnS	použít
Gray	Graa	k1gFnPc4	Graa
elektromagneticky	elektromagneticky	k6eAd1	elektromagneticky
prohýbanou	prohýbaný	k2eAgFnSc4d1	prohýbaná
membránu	membrána	k1gFnSc4	membrána
<g/>
.	.	kIx.	.
</s>
<s>
Bell	bell	k1gInSc1	bell
použil	použít	k5eAaPmAgInS	použít
membránu	membrána	k1gFnSc4	membrána
jak	jak	k8xC	jak
v	v	k7c6	v
mikrofonu	mikrofon	k1gInSc6	mikrofon
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
ve	v	k7c6	v
sluchátku	sluchátko	k1gNnSc6	sluchátko
<g/>
.	.	kIx.	.
</s>
<s>
Membrána	membrána	k1gFnSc1	membrána
kmitala	kmitat	k5eAaImAgFnS	kmitat
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
cívky	cívka	k1gFnSc2	cívka
navinuté	navinutý	k2eAgFnSc2d1	navinutá
na	na	k7c6	na
ocelovém	ocelový	k2eAgInSc6d1	ocelový
magnetu	magnet	k1gInSc6	magnet
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
uspořádání	uspořádání	k1gNnSc1	uspořádání
bylo	být	k5eAaImAgNnS	být
nejen	nejen	k6eAd1	nejen
jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
nepotřebovalo	potřebovat	k5eNaImAgNnS	potřebovat
baterii	baterie	k1gFnSc4	baterie
ani	ani	k8xC	ani
na	na	k7c4	na
přijímací	přijímací	k2eAgFnSc4d1	přijímací
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
na	na	k7c6	na
vysílací	vysílací	k2eAgFnSc6d1	vysílací
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
indukční	indukční	k2eAgInSc1d1	indukční
mikrofon	mikrofon	k1gInSc1	mikrofon
byl	být	k5eAaImAgInS	být
málo	málo	k6eAd1	málo
citlivý	citlivý	k2eAgInSc1d1	citlivý
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
proto	proto	k8xC	proto
později	pozdě	k6eAd2	pozdě
nahrazen	nahradit	k5eAaPmNgInS	nahradit
uhlíkovým	uhlíkový	k2eAgInSc7d1	uhlíkový
odporovým	odporový	k2eAgInSc7d1	odporový
mikrofonem	mikrofon	k1gInSc7	mikrofon
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
bez	bez	k7c2	bez
podstatných	podstatný	k2eAgFnPc2d1	podstatná
změn	změna	k1gFnPc2	změna
užíval	užívat	k5eAaImAgInS	užívat
do	do	k7c2	do
konce	konec	k1gInSc2	konec
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Bell	bell	k1gInSc1	bell
sám	sám	k3xTgInSc1	sám
zmínil	zmínit	k5eAaPmAgMnS	zmínit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gInPc1	jeho
první	první	k4xOgInPc1	první
pokusy	pokus	k1gInPc1	pokus
byly	být	k5eAaImAgInP	být
neúspěšné	úspěšný	k2eNgInPc1d1	neúspěšný
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
po	po	k7c6	po
dlouhých	dlouhý	k2eAgInPc6d1	dlouhý
experimentech	experiment	k1gInPc6	experiment
našel	najít	k5eAaPmAgMnS	najít
membránu	membrána	k1gFnSc4	membrána
ve	v	k7c6	v
správných	správný	k2eAgInPc6d1	správný
parametrech	parametr	k1gInPc6	parametr
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
očekávaný	očekávaný	k2eAgInSc1d1	očekávaný
efekt	efekt	k1gInSc1	efekt
dostavil	dostavit	k5eAaPmAgInS	dostavit
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
2	[number]	k4	2
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1875	[number]	k4	1875
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
poprvé	poprvé	k6eAd1	poprvé
podařilo	podařit	k5eAaPmAgNnS	podařit
elektricky	elektricky	k6eAd1	elektricky
přenést	přenést	k5eAaPmF	přenést
tón	tón	k1gInSc4	tón
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
devět	devět	k4xCc4	devět
měsíců	měsíc	k1gInPc2	měsíc
později	pozdě	k6eAd2	pozdě
udělal	udělat	k5eAaPmAgInS	udělat
technická	technický	k2eAgNnPc4d1	technické
zlepšení	zlepšení	k1gNnPc4	zlepšení
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
nebyla	být	k5eNaImAgNnP	být
zahrnuta	zahrnout	k5eAaPmNgNnP	zahrnout
do	do	k7c2	do
patentové	patentový	k2eAgFnSc2d1	patentová
přihlášky	přihláška	k1gFnSc2	přihláška
a	a	k8xC	a
která	který	k3yRgFnSc1	který
umožňovala	umožňovat	k5eAaImAgFnS	umožňovat
přenos	přenos	k1gInSc4	přenos
lidské	lidský	k2eAgFnSc2d1	lidská
řeči	řeč	k1gFnSc2	řeč
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
10	[number]	k4	10
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1876	[number]	k4	1876
uslyšel	uslyšet	k5eAaPmAgMnS	uslyšet
jeho	jeho	k3xOp3gMnSc1	jeho
pomocník	pomocník	k1gMnSc1	pomocník
v	v	k7c6	v
laboratoři	laboratoř	k1gFnSc6	laboratoř
první	první	k4xOgFnSc4	první
větu	věta	k1gFnSc4	věta
přenesenou	přenesený	k2eAgFnSc4d1	přenesená
jeho	jeho	k3xOp3gInSc7	jeho
telefonem	telefon	k1gInSc7	telefon
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Pane	Pan	k1gMnSc5	Pan
Watsone	Watson	k1gMnSc5	Watson
<g/>
,	,	kIx,	,
přijďte	přijít	k5eAaPmRp2nP	přijít
sem	sem	k6eAd1	sem
<g/>
,	,	kIx,	,
potřebuji	potřebovat	k5eAaImIp1nS	potřebovat
vás	vy	k3xPp2nPc4	vy
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Přes	přes	k7c4	přes
technické	technický	k2eAgInPc4d1	technický
úspěchy	úspěch	k1gInPc4	úspěch
zůstal	zůstat	k5eAaPmAgInS	zůstat
jeho	jeho	k3xOp3gInSc1	jeho
telefon	telefon	k1gInSc1	telefon
zpočátku	zpočátku	k6eAd1	zpočátku
nepovšimnut	povšimnut	k2eNgInSc1d1	nepovšimnut
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
komunikační	komunikační	k2eAgInSc4d1	komunikační
prostředek	prostředek	k1gInSc4	prostředek
si	se	k3xPyFc3	se
ho	on	k3xPp3gNnSc4	on
nikdo	nikdo	k3yNnSc1	nikdo
neuměl	umět	k5eNaImAgMnS	umět
představit	představit	k5eAaPmF	představit
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
na	na	k7c6	na
světové	světový	k2eAgFnSc6d1	světová
výstavě	výstava	k1gFnSc6	výstava
roku	rok	k1gInSc2	rok
1876	[number]	k4	1876
ve	v	k7c6	v
Filadelfii	Filadelfie	k1gFnSc6	Filadelfie
nevzbuzovalo	vzbuzovat	k5eNaImAgNnS	vzbuzovat
instalované	instalovaný	k2eAgNnSc1d1	instalované
zařízení	zařízení	k1gNnSc1	zařízení
zprvu	zprvu	k6eAd1	zprvu
žádnou	žádný	k3yNgFnSc4	žádný
zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
pozornost	pozornost	k1gFnSc4	pozornost
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
když	když	k8xS	když
ho	on	k3xPp3gNnSc4	on
objevil	objevit	k5eAaPmAgMnS	objevit
brazilský	brazilský	k2eAgMnSc1d1	brazilský
císař	císař	k1gMnSc1	císař
a	a	k8xC	a
věnoval	věnovat	k5eAaPmAgMnS	věnovat
mu	on	k3xPp3gInSc3	on
svoji	svůj	k3xOyFgFnSc4	svůj
vladařskou	vladařský	k2eAgFnSc4d1	vladařská
pozornost	pozornost	k1gFnSc4	pozornost
<g/>
,	,	kIx,	,
rozpomenulo	rozpomenout	k5eAaPmAgNnS	rozpomenout
se	se	k3xPyFc4	se
vedení	vedení	k1gNnSc1	vedení
výstavy	výstava	k1gFnSc2	výstava
na	na	k7c4	na
jeho	jeho	k3xOp3gInSc4	jeho
exponát	exponát	k1gInSc4	exponát
a	a	k8xC	a
udělilo	udělit	k5eAaPmAgNnS	udělit
mu	on	k3xPp3gMnSc3	on
zlatou	zlatý	k2eAgFnSc4d1	zlatá
medaili	medaile	k1gFnSc4	medaile
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
tak	tak	k6eAd1	tak
motivován	motivovat	k5eAaBmNgInS	motivovat
k	k	k7c3	k
dalšímu	další	k2eAgInSc3d1	další
rozvoji	rozvoj	k1gInSc3	rozvoj
telefonu	telefon	k1gInSc2	telefon
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgMnS	začít
experimentovat	experimentovat	k5eAaImF	experimentovat
s	s	k7c7	s
dálkovým	dálkový	k2eAgNnSc7d1	dálkové
spojením	spojení	k1gNnSc7	spojení
a	a	k8xC	a
8	[number]	k4	8
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
uskutečnil	uskutečnit	k5eAaPmAgMnS	uskutečnit
rozhovor	rozhovor	k1gInSc4	rozhovor
na	na	k7c4	na
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
3200	[number]	k4	3200
metrů	metr	k1gInPc2	metr
mezi	mezi	k7c7	mezi
Bostonem	Boston	k1gInSc7	Boston
a	a	k8xC	a
Cambridge	Cambridge	k1gFnSc1	Cambridge
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc1	svůj
telefon	telefon	k1gInSc1	telefon
nabídl	nabídnout	k5eAaPmAgInS	nabídnout
americké	americký	k2eAgFnSc3d1	americká
a	a	k8xC	a
britské	britský	k2eAgFnSc3d1	britská
vládě	vláda	k1gFnSc3	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Britský	britský	k2eAgInSc1d1	britský
poštovní	poštovní	k2eAgInSc1d1	poštovní
úřad	úřad	k1gInSc1	úřad
odpověděl	odpovědět	k5eAaPmAgInS	odpovědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
Američané	Američan	k1gMnPc1	Američan
snad	snad	k9	snad
takovou	takový	k3xDgFnSc4	takový
věc	věc	k1gFnSc4	věc
potřebují	potřebovat	k5eAaImIp3nP	potřebovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Angličané	Angličan	k1gMnPc1	Angličan
předávají	předávat	k5eAaImIp3nP	předávat
zprávy	zpráva	k1gFnPc4	zpráva
pomocí	pomocí	k7c2	pomocí
malých	malý	k2eAgMnPc2d1	malý
chlapců	chlapec	k1gMnPc2	chlapec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
přenášejí	přenášet	k5eAaImIp3nP	přenášet
listy	list	k1gInPc4	list
mezi	mezi	k7c4	mezi
adresáty	adresát	k1gMnPc4	adresát
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
9	[number]	k4	9
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
založil	založit	k5eAaPmAgMnS	založit
společně	společně	k6eAd1	společně
s	s	k7c7	s
Watsonem	Watson	k1gMnSc7	Watson
<g/>
,	,	kIx,	,
G.	G.	kA	G.
Hubbardem	Hubbard	k1gMnSc7	Hubbard
a	a	k8xC	a
Sandersem	Sanders	k1gMnSc7	Sanders
firmu	firma	k1gFnSc4	firma
Bell	bell	k1gInSc1	bell
Telephone	Telephon	k1gInSc5	Telephon
Company	Compan	k1gMnPc7	Compan
<g/>
,	,	kIx,	,
Gardina	gardina	k1gFnSc1	gardina
D.	D.	kA	D.
Hubbard	Hubbard	k1gInSc4	Hubbard
Trustee	Truste	k1gInSc2	Truste
v	v	k7c6	v
Bostonu	Boston	k1gInSc6	Boston
a	a	k8xC	a
zahájil	zahájit	k5eAaPmAgMnS	zahájit
výrobu	výroba	k1gFnSc4	výroba
telefonních	telefonní	k2eAgInPc2d1	telefonní
přístrojů	přístroj	k1gInPc2	přístroj
ve	v	k7c6	v
velkém	velký	k2eAgNnSc6d1	velké
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
společnost	společnost	k1gFnSc1	společnost
telefonní	telefonní	k2eAgInSc4d1	telefonní
přístroje	přístroj	k1gInPc4	přístroj
neprodávala	prodávat	k5eNaImAgFnS	prodávat
<g/>
,	,	kIx,	,
jen	jen	k9	jen
je	být	k5eAaImIp3nS	být
pronajímala	pronajímat	k5eAaImAgFnS	pronajímat
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1878	[number]	k4	1878
se	se	k3xPyFc4	se
společnost	společnost	k1gFnSc1	společnost
jmenovala	jmenovat	k5eAaBmAgFnS	jmenovat
Bell	bell	k1gInSc4	bell
Telephone	Telephon	k1gInSc5	Telephon
Company	Compan	k1gMnPc4	Compan
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zahrnutí	zahrnutí	k1gNnSc6	zahrnutí
Grayových	Grayová	k1gFnPc2	Grayová
patentových	patentový	k2eAgNnPc2d1	patentové
práv	právo	k1gNnPc2	právo
přešla	přejít	k5eAaPmAgFnS	přejít
28	[number]	k4	28
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1885	[number]	k4	1885
v	v	k7c6	v
American	Americana	k1gFnPc2	Americana
Telephone	Telephon	k1gInSc5	Telephon
and	and	k?	and
Telegraph	Telegraph	k1gInSc4	Telegraph
Company	Compana	k1gFnSc2	Compana
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gInSc7	její
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
provozovat	provozovat	k5eAaImF	provozovat
telefonní	telefonní	k2eAgNnSc4d1	telefonní
spojení	spojení	k1gNnSc4	spojení
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
měl	mít	k5eAaImAgInS	mít
každý	každý	k3xTgInSc4	každý
telefonní	telefonní	k2eAgInSc4d1	telefonní
přístroj	přístroj	k1gInSc4	přístroj
svou	svůj	k3xOyFgFnSc4	svůj
baterii	baterie	k1gFnSc4	baterie
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
bylo	být	k5eAaImAgNnS	být
řešeno	řešit	k5eAaImNgNnS	řešit
centrální	centrální	k2eAgNnSc1d1	centrální
napájení	napájení	k1gNnSc1	napájení
telefonních	telefonní	k2eAgInPc2d1	telefonní
přístrojů	přístroj	k1gInPc2	přístroj
z	z	k7c2	z
telefonní	telefonní	k2eAgFnSc2d1	telefonní
ústředny	ústředna	k1gFnSc2	ústředna
<g/>
.	.	kIx.	.
</s>
<s>
Bell	bell	k1gInSc1	bell
byl	být	k5eAaImAgInS	být
mimořáně	mimořána	k1gFnSc3	mimořána
tvořivý	tvořivý	k2eAgMnSc1d1	tvořivý
člověk	člověk	k1gMnSc1	člověk
s	s	k7c7	s
velmi	velmi	k6eAd1	velmi
širokými	široký	k2eAgInPc7d1	široký
zájmy	zájem	k1gInPc7	zájem
a	a	k8xC	a
mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gInPc4	jeho
další	další	k2eAgInPc4d1	další
vynálezy	vynález	k1gInPc4	vynález
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
Fotofon	Fotofon	k1gInSc1	Fotofon
<g/>
,	,	kIx,	,
bezdrátový	bezdrátový	k2eAgInSc1d1	bezdrátový
optický	optický	k2eAgInSc4d1	optický
telefon	telefon	k1gInSc4	telefon
<g/>
.	.	kIx.	.
</s>
<s>
Detektor	detektor	k1gInSc1	detektor
kovů	kov	k1gInPc2	kov
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
měl	mít	k5eAaImAgInS	mít
pomoci	pomoct	k5eAaPmF	pomoct
najít	najít	k5eAaPmF	najít
kulku	kulka	k1gFnSc4	kulka
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
amerického	americký	k2eAgMnSc2d1	americký
presidenta	president	k1gMnSc2	president
Garfielda	Garfield	k1gMnSc2	Garfield
<g/>
.	.	kIx.	.
</s>
<s>
Fonograf	fonograf	k1gInSc4	fonograf
i	i	k8xC	i
první	první	k4xOgInPc4	první
pokusy	pokus	k1gInPc4	pokus
s	s	k7c7	s
magnetickým	magnetický	k2eAgInSc7d1	magnetický
záznamem	záznam	k1gInSc7	záznam
zvuku	zvuk	k1gInSc2	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Letadla	letadlo	k1gNnPc1	letadlo
těžší	těžký	k2eAgFnSc2d2	těžší
než	než	k8xS	než
vzduch	vzduch	k1gInSc1	vzduch
<g/>
,	,	kIx,	,
jimž	jenž	k3xRgFnPc3	jenž
se	se	k3xPyFc4	se
intenzivně	intenzivně	k6eAd1	intenzivně
věnoval	věnovat	k5eAaPmAgMnS	věnovat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1903	[number]	k4	1903
<g/>
-	-	kIx~	-
<g/>
1915	[number]	k4	1915
<g/>
.	.	kIx.	.
</s>
<s>
Křídlový	křídlový	k2eAgInSc1d1	křídlový
člun	člun	k1gInSc1	člun
a	a	k8xC	a
hydroplán	hydroplán	k1gInSc1	hydroplán
<g/>
.	.	kIx.	.
</s>
<s>
Audiometr	audiometr	k1gInSc1	audiometr
na	na	k7c4	na
měření	měření	k1gNnSc4	měření
sluchu	sluch	k1gInSc2	sluch
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
́	́	k?	́
<g/>
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
Bell	bell	k1gInSc1	bell
angažoval	angažovat	k5eAaBmAgInS	angažovat
v	v	k7c6	v
National	National	k1gFnSc6	National
Geographic	Geographice	k1gFnPc2	Geographice
Society	societa	k1gFnSc2	societa
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
jejím	její	k3xOp3gMnSc7	její
druhým	druhý	k4xOgMnSc7	druhý
presidentem	president	k1gMnSc7	president
<g/>
.	.	kIx.	.
</s>
<s>
Uvažoval	uvažovat	k5eAaImAgInS	uvažovat
o	o	k7c6	o
alternativních	alternativní	k2eAgInPc6d1	alternativní
zdrojích	zdroj	k1gInPc6	zdroj
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
o	o	k7c6	o
obnovitelných	obnovitelný	k2eAgFnPc6d1	obnovitelná
energiích	energie	k1gFnPc6	energie
a	a	k8xC	a
mnoha	mnoho	k4c2	mnoho
dalších	další	k2eAgInPc6d1	další
tématech	téma	k1gNnPc6	téma
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
se	se	k3xPyFc4	se
dočkala	dočkat	k5eAaPmAgNnP	dočkat
uznání	uznání	k1gNnPc1	uznání
až	až	k6eAd1	až
dlouho	dlouho	k6eAd1	dlouho
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Bellovu	Bellův	k2eAgNnSc3d1	Bellovo
dílu	dílo	k1gNnSc3	dílo
je	být	k5eAaImIp3nS	být
věnováno	věnován	k2eAgNnSc1d1	věnováno
muzeum	muzeum	k1gNnSc1	muzeum
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Cape	capat	k5eAaImIp3nS	capat
Breton	breton	k1gInSc1	breton
(	(	kIx(	(
<g/>
Nova	nova	k1gFnSc1	nova
Scotia	Scotia	k1gFnSc1	Scotia
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Bellovu	Bellův	k2eAgFnSc4d1	Bellova
počest	počest	k1gFnSc4	počest
je	být	k5eAaImIp3nS	být
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
technická	technický	k2eAgFnSc1d1	technická
jednotka	jednotka	k1gFnSc1	jednotka
decibel	decibel	k1gInSc4	decibel
<g/>
,	,	kIx,	,
ostrov	ostrov	k1gInSc4	ostrov
Grahama	Graham	k1gMnSc4	Graham
Bella	Bell	k1gMnSc4	Bell
a	a	k8xC	a
Bellův	Bellův	k2eAgInSc4d1	Bellův
ostrov	ostrov	k1gInSc4	ostrov
v	v	k7c6	v
arktickém	arktický	k2eAgNnSc6d1	arktické
souostroví	souostroví	k1gNnSc6	souostroví
Země	zem	k1gFnSc2	zem
Františka	František	k1gMnSc2	František
Josefa	Josef	k1gMnSc2	Josef
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Nedokážu	dokázat	k5eNaPmIp1nS	dokázat
říct	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
za	za	k7c4	za
sílu	síla	k1gFnSc4	síla
<g/>
,	,	kIx,	,
víme	vědět	k5eAaImIp1nP	vědět
jen	jen	k9	jen
<g/>
,	,	kIx,	,
že	že	k8xS	že
existuje	existovat	k5eAaImIp3nS	existovat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
