<s>
Alexander	Alexandra	k1gFnPc2	Alexandra
Graham	Graham	k1gMnSc1	Graham
Bell	bell	k1gInSc1	bell
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1847	[number]	k4	1847
<g/>
,	,	kIx,	,
Edinburgh	Edinburgh	k1gInSc1	Edinburgh
<g/>
,	,	kIx,	,
Skotsko	Skotsko	k1gNnSc1	Skotsko
–	–	k?	–
2	[number]	k4	2
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1922	[number]	k4	1922
<g/>
,	,	kIx,	,
Baddeck	Baddeck	k1gInSc1	Baddeck
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
skotsko-americký	skotskomerický	k2eAgMnSc1d1	skotsko-americký
vědec	vědec	k1gMnSc1	vědec
a	a	k8xC	a
vynálezce	vynálezce	k1gMnSc1	vynálezce
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgInS	zabývat
tvorbou	tvorba	k1gFnSc7	tvorba
lidské	lidský	k2eAgFnSc2d1	lidská
řeči	řeč	k1gFnSc2	řeč
<g/>
,	,	kIx,	,
výchovou	výchova	k1gFnSc7	výchova
hluchoněmých	hluchoněmý	k2eAgInPc2d1	hluchoněmý
a	a	k8xC	a
elektromagnetickým	elektromagnetický	k2eAgInSc7d1	elektromagnetický
přenosem	přenos	k1gInSc7	přenos
řeči	řeč	k1gFnSc2	řeč
<g/>
.	.	kIx.	.
</s>
