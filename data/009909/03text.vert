<p>
<s>
Ekliptika	ekliptika	k1gFnSc1	ekliptika
je	být	k5eAaImIp3nS	být
průsečnice	průsečnice	k1gFnSc1	průsečnice
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
rovina	rovina	k1gFnSc1	rovina
dráhy	dráha	k1gFnSc2	dráha
Země	zem	k1gFnSc2	zem
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
protíná	protínat	k5eAaImIp3nS	protínat
nebeskou	nebeský	k2eAgFnSc4d1	nebeská
sféru	sféra	k1gFnSc4	sféra
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
je	být	k5eAaImIp3nS	být
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
od	od	k7c2	od
řec.	řec.	k?	řec.
ekleipsis	ekleipsis	k1gFnSc2	ekleipsis
a	a	k8xC	a
lat.	lat.	k?	lat.
eclipsis	eclipsis	k1gFnSc1	eclipsis
-	-	kIx~	-
zatmění	zatmění	k1gNnSc1	zatmění
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
právě	právě	k9	právě
v	v	k7c6	v
nejtěsnější	těsný	k2eAgFnSc6d3	nejtěsnější
blízkosti	blízkost	k1gFnSc6	blízkost
ekliptiky	ekliptika	k1gFnSc2	ekliptika
nastávají	nastávat	k5eAaImIp3nP	nastávat
zatmění	zatmění	k1gNnSc4	zatmění
Slunce	slunce	k1gNnSc2	slunce
a	a	k8xC	a
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ekliptika	ekliptika	k1gFnSc1	ekliptika
má	mít	k5eAaImIp3nS	mít
tvar	tvar	k1gInSc4	tvar
kružnice	kružnice	k1gFnSc2	kružnice
a	a	k8xC	a
protíná	protínat	k5eAaImIp3nS	protínat
nebeský	nebeský	k2eAgInSc4d1	nebeský
rovník	rovník	k1gInSc4	rovník
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
bodech	bod	k1gInPc6	bod
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
jarní	jarní	k2eAgInSc1d1	jarní
a	a	k8xC	a
druhý	druhý	k4xOgInSc1	druhý
podzimní	podzimní	k2eAgInSc1d1	podzimní
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
putuje	putovat	k5eAaImIp3nS	putovat
Slunce	slunce	k1gNnSc1	slunce
po	po	k7c6	po
ekliptice	ekliptika	k1gFnSc6	ekliptika
a	a	k8xC	a
prochází	procházet	k5eAaImIp3nS	procházet
přitom	přitom	k6eAd1	přitom
během	během	k7c2	během
roku	rok	k1gInSc2	rok
různými	různý	k2eAgNnPc7d1	různé
souhvězdími	souhvězdí	k1gNnPc7	souhvězdí
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
nazýváme	nazývat	k5eAaImIp1nP	nazývat
zvířetníková	zvířetníkový	k2eAgNnPc1d1	zvířetníkové
souhvězdí	souhvězdí	k1gNnPc1	souhvězdí
<g/>
.	.	kIx.	.
</s>
<s>
Blízko	blízko	k7c2	blízko
ekliptiky	ekliptika	k1gFnSc2	ekliptika
najdeme	najít	k5eAaPmIp1nP	najít
i	i	k9	i
všechny	všechen	k3xTgFnPc4	všechen
planety	planeta	k1gFnPc4	planeta
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
i	i	k9	i
Měsíc	měsíc	k1gInSc4	měsíc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rovina	rovina	k1gFnSc1	rovina
ekliptiky	ekliptika	k1gFnSc2	ekliptika
==	==	k?	==
</s>
</p>
<p>
<s>
Rovina	rovina	k1gFnSc1	rovina
ekliptiky	ekliptika	k1gFnSc2	ekliptika
je	být	k5eAaImIp3nS	být
rovina	rovina	k1gFnSc1	rovina
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
obíhá	obíhat	k5eAaImIp3nS	obíhat
Země	země	k1gFnSc1	země
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
rovina	rovina	k1gFnSc1	rovina
se	se	k3xPyFc4	se
s	s	k7c7	s
časem	čas	k1gInSc7	čas
téměř	téměř	k6eAd1	téměř
nemění	měnit	k5eNaImIp3nS	měnit
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
vhodná	vhodný	k2eAgFnSc1d1	vhodná
pro	pro	k7c4	pro
definování	definování	k1gNnSc4	definování
soustavy	soustava	k1gFnSc2	soustava
souřadnic	souřadnice	k1gFnPc2	souřadnice
(	(	kIx(	(
<g/>
ekliptikální	ekliptikální	k2eAgFnSc2d1	ekliptikální
souřadnice	souřadnice	k1gFnSc2	souřadnice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
planety	planeta	k1gFnPc1	planeta
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
obíhají	obíhat	k5eAaImIp3nP	obíhat
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
v	v	k7c6	v
rovinách	rovina	k1gFnPc6	rovina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
od	od	k7c2	od
roviny	rovina	k1gFnSc2	rovina
ekliptiky	ekliptika	k1gFnSc2	ekliptika
liší	lišit	k5eAaImIp3nS	lišit
málo	málo	k6eAd1	málo
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
skutečnost	skutečnost	k1gFnSc1	skutečnost
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
důsledkem	důsledek	k1gInSc7	důsledek
vývoje	vývoj	k1gInSc2	vývoj
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
z	z	k7c2	z
plynného	plynný	k2eAgInSc2d1	plynný
protoplanetárního	protoplanetární	k2eAgInSc2d1	protoplanetární
disku	disk	k1gInSc2	disk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sklon	sklon	k1gInSc4	sklon
dráhy	dráha	k1gFnSc2	dráha
k	k	k7c3	k
ekliptice	ekliptika	k1gFnSc3	ekliptika
==	==	k?	==
</s>
</p>
<p>
<s>
Sklon	sklon	k1gInSc1	sklon
dráhy	dráha	k1gFnSc2	dráha
k	k	k7c3	k
ekliptice	ekliptika	k1gFnSc3	ekliptika
je	být	k5eAaImIp3nS	být
úhel	úhel	k1gInSc1	úhel
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
svírá	svírat	k5eAaImIp3nS	svírat
hlavní	hlavní	k2eAgFnSc1d1	hlavní
rovina	rovina	k1gFnSc1	rovina
dráhy	dráha	k1gFnSc2	dráha
tělesa	těleso	k1gNnSc2	těleso
s	s	k7c7	s
rovinou	rovina	k1gFnSc7	rovina
ekliptiky	ekliptika	k1gFnSc2	ekliptika
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgInPc1d1	velký
sklony	sklon	k1gInPc1	sklon
jsou	být	k5eAaImIp3nP	být
typické	typický	k2eAgInPc1d1	typický
pro	pro	k7c4	pro
tělesa	těleso	k1gNnPc4	těleso
rozptýleného	rozptýlený	k2eAgInSc2d1	rozptýlený
a	a	k8xC	a
odděleného	oddělený	k2eAgInSc2d1	oddělený
disku	disk	k1gInSc2	disk
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
planet	planeta	k1gFnPc2	planeta
má	mít	k5eAaImIp3nS	mít
největší	veliký	k2eAgInSc4d3	veliký
sklon	sklon	k1gInSc4	sklon
dráhy	dráha	k1gFnSc2	dráha
Merkur	Merkur	k1gInSc1	Merkur
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
°	°	k?	°
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Extrémně	extrémně	k6eAd1	extrémně
nakloněnou	nakloněný	k2eAgFnSc4d1	nakloněná
dráhu	dráha	k1gFnSc4	dráha
má	mít	k5eAaImIp3nS	mít
kentaur	kentaur	k1gMnSc1	kentaur
(	(	kIx(	(
<g/>
127546	[number]	k4	127546
<g/>
)	)	kIx)	)
2002	[number]	k4	2002
XU93	XU93	k1gFnPc2	XU93
(	(	kIx(	(
<g/>
77,9	[number]	k4	77,9
<g/>
°	°	k?	°
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Oběžná	oběžný	k2eAgFnSc1d1	oběžná
rovina	rovina	k1gFnSc1	rovina
</s>
</p>
<p>
<s>
Zvířetníková	zvířetníkový	k2eAgNnPc1d1	zvířetníkové
souhvězdí	souhvězdí	k1gNnPc1	souhvězdí
</s>
</p>
<p>
<s>
Nebeský	nebeský	k2eAgInSc1d1	nebeský
rovník	rovník	k1gInSc1	rovník
</s>
</p>
<p>
<s>
Jarní	jarní	k2eAgInSc1d1	jarní
bod	bod	k1gInSc1	bod
</s>
</p>
<p>
<s>
Podzimní	podzimní	k2eAgInSc1d1	podzimní
bod	bod	k1gInSc1	bod
</s>
</p>
<p>
<s>
Ekliptikální	ekliptikální	k2eAgFnSc1d1	ekliptikální
souřadnice	souřadnice	k1gFnSc1	souřadnice
</s>
</p>
<p>
<s>
Protoplanetární	Protoplanetární	k2eAgInSc1d1	Protoplanetární
disk	disk	k1gInSc1	disk
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Ekliptika	ekliptika	k1gFnSc1	ekliptika
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Ekliptika	ekliptika	k1gFnSc1	ekliptika
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
