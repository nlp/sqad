<s>
Ekliptika	ekliptika	k1gFnSc1	ekliptika
je	být	k5eAaImIp3nS	být
průsečnice	průsečnice	k1gFnSc1	průsečnice
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
rovina	rovina	k1gFnSc1	rovina
dráhy	dráha	k1gFnSc2	dráha
Země	zem	k1gFnSc2	zem
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
protíná	protínat	k5eAaImIp3nS	protínat
nebeskou	nebeský	k2eAgFnSc4d1	nebeská
sféru	sféra	k1gFnSc4	sféra
<g/>
.	.	kIx.	.
</s>
