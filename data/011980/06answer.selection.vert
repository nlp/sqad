<s>
Skleníkový	skleníkový	k2eAgInSc1d1	skleníkový
efekt	efekt	k1gInSc1	efekt
<g/>
,	,	kIx,	,
také	také	k9	také
zvaný	zvaný	k2eAgInSc1d1	zvaný
skleníkový	skleníkový	k2eAgInSc1d1	skleníkový
jev	jev	k1gInSc1	jev
je	být	k5eAaImIp3nS	být
proces	proces	k1gInSc1	proces
<g/>
,	,	kIx,	,
kterým	který	k3yRgNnSc7	který
záření	záření	k1gNnSc4	záření
atmosféry	atmosféra	k1gFnSc2	atmosféra
planety	planeta	k1gFnSc2	planeta
ohřívá	ohřívat	k5eAaImIp3nS	ohřívat
povrch	povrch	k1gInSc4	povrch
planety	planeta	k1gFnSc2	planeta
na	na	k7c4	na
teplotu	teplota	k1gFnSc4	teplota
vyšší	vysoký	k2eAgFnSc4d2	vyšší
<g/>
,	,	kIx,	,
než	než	k8xS	než
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
bez	bez	k7c2	bez
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
