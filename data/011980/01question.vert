<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
efekt	efekt	k1gInSc1	efekt
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
povrch	povrch	k1gInSc1	povrch
planety	planeta	k1gFnSc2	planeta
ohřívá	ohřívat	k5eAaImIp3nS	ohřívat
na	na	k7c4	na
teplotu	teplota	k1gFnSc4	teplota
vyšší	vysoký	k2eAgFnSc4d2	vyšší
<g/>
,	,	kIx,	,
než	než	k8xS	než
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
bez	bez	k7c2	bez
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
?	?	kIx.	?
</s>
