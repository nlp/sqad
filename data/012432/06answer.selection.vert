<s>
Královna	královna	k1gFnSc1	královna
Šarlota	Šarlota	k1gFnSc1	Šarlota
se	se	k3xPyFc4	se
zajímala	zajímat	k5eAaImAgFnS	zajímat
o	o	k7c6	o
umění	umění	k1gNnSc6	umění
<g/>
,	,	kIx,	,
podporovala	podporovat	k5eAaImAgFnS	podporovat
Johanna	Johann	k1gMnSc4	Johann
Christiana	Christian	k1gMnSc4	Christian
Bacha	Bacha	k?	Bacha
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
jejím	její	k3xOp3gMnSc7	její
učitelem	učitel	k1gMnSc7	učitel
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
i	i	k8xC	i
Wolfganag	Wolfganaga	k1gFnPc2	Wolfganaga
Amadea	Amadeus	k1gMnSc4	Amadeus
Mozarta	Mozart	k1gMnSc4	Mozart
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
jí	jíst	k5eAaImIp3nS	jíst
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
8	[number]	k4	8
let	léto	k1gNnPc2	léto
věnoval	věnovat	k5eAaPmAgMnS	věnovat
jedno	jeden	k4xCgNnSc4	jeden
ze	z	k7c2	z
svých	svůj	k3xOyFgNnPc2	svůj
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
