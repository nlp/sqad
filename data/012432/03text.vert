<p>
<s>
Šarlota	Šarlota	k1gFnSc1	Šarlota
Meklenbursko-Střelická	Meklenbursko-Střelický	k2eAgFnSc1d1	Meklenbursko-Střelický
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Sophie	Sophie	k1gFnSc1	Sophie
Charlotte	Charlott	k1gInSc5	Charlott
von	von	k1gInSc4	von
Mecklenburg-Strelitz	Mecklenburg-Strelitz	k1gInSc4	Mecklenburg-Strelitz
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1744	[number]	k4	1744
<g/>
,	,	kIx,	,
Mirow	Mirow	k1gFnSc1	Mirow
–	–	k?	–
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1818	[number]	k4	1818
<g/>
,	,	kIx,	,
Kew	Kew	k1gFnSc1	Kew
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
rozená	rozený	k2eAgFnSc1d1	rozená
princezna	princezna	k1gFnSc1	princezna
meklenbursko-střelická	meklenburskotřelický	k2eAgFnSc1d1	meklenbursko-střelický
a	a	k8xC	a
jako	jako	k9	jako
manželka	manželka	k1gFnSc1	manželka
britského	britský	k2eAgMnSc2d1	britský
krále	král	k1gMnSc2	král
Jiřího	Jiří	k1gMnSc2	Jiří
III	III	kA	III
<g/>
.	.	kIx.	.
britská	britský	k2eAgFnSc1d1	britská
královna	královna	k1gFnSc1	královna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Životopis	životopis	k1gInSc4	životopis
==	==	k?	==
</s>
</p>
<p>
<s>
Jejími	její	k3xOp3gMnPc7	její
rodiči	rodič	k1gMnPc7	rodič
byli	být	k5eAaImAgMnP	být
vévoda	vévoda	k1gMnSc1	vévoda
meklenbursko-střelický	meklenburskotřelický	k2eAgMnSc1d1	meklenbursko-střelický
Karel	Karel	k1gMnSc1	Karel
Ludvík	Ludvík	k1gMnSc1	Ludvík
Fridrich	Fridrich	k1gMnSc1	Fridrich
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
manželka	manželka	k1gFnSc1	manželka
Alžběta	Alžběta	k1gFnSc1	Alžběta
<g/>
,	,	kIx,	,
saská	saský	k2eAgFnSc1d1	saská
princezna	princezna	k1gFnSc1	princezna
<g/>
.	.	kIx.	.
</s>
<s>
Šarlota	Šarlota	k1gFnSc1	Šarlota
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
jako	jako	k9	jako
osmá	osmý	k4xOgFnSc1	osmý
z	z	k7c2	z
jejich	jejich	k3xOp3gFnPc2	jejich
deseti	deset	k4xCc2	deset
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
roku	rok	k1gInSc2	rok
1752	[number]	k4	1752
zemřel	zemřít	k5eAaPmAgMnS	zemřít
starší	starší	k1gMnSc1	starší
bratr	bratr	k1gMnSc1	bratr
jejího	její	k3xOp3gMnSc4	její
otce	otec	k1gMnSc2	otec
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
zcela	zcela	k6eAd1	zcela
změnilo	změnit	k5eAaPmAgNnS	změnit
postavení	postavení	k1gNnSc1	postavení
její	její	k3xOp3gFnSc2	její
rodiny	rodina	k1gFnSc2	rodina
–	–	k?	–
otec	otec	k1gMnSc1	otec
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
panovníkem	panovník	k1gMnSc7	panovník
Meklenburska	Meklenbursk	k1gInSc2	Meklenbursk
a	a	k8xC	a
změnila	změnit	k5eAaPmAgFnS	změnit
se	se	k3xPyFc4	se
i	i	k9	i
Šarlotina	Šarlotin	k2eAgFnSc1d1	Šarlotina
pozice	pozice	k1gFnSc1	pozice
jako	jako	k8xC	jako
nevěsty	nevěsta	k1gFnPc1	nevěsta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Její	její	k3xOp3gMnPc1	její
bratři	bratr	k1gMnPc1	bratr
a	a	k8xC	a
matka	matka	k1gFnSc1	matka
vdova	vdova	k1gFnSc1	vdova
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
hledali	hledat	k5eAaImAgMnP	hledat
výhodný	výhodný	k2eAgInSc4d1	výhodný
svazek	svazek	k1gInSc4	svazek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
17	[number]	k4	17
letech	léto	k1gNnPc6	léto
byla	být	k5eAaImAgFnS	být
vybrána	vybrat	k5eAaPmNgFnS	vybrat
jako	jako	k9	jako
nevěsta	nevěsta	k1gFnSc1	nevěsta
pro	pro	k7c4	pro
mladého	mladý	k2eAgMnSc4d1	mladý
britského	britský	k2eAgMnSc4d1	britský
krále	král	k1gMnSc4	král
Jiřího	Jiří	k1gMnSc2	Jiří
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
třebaže	třebaže	k8xS	třebaže
její	její	k3xOp3gFnSc1	její
ruka	ruka	k1gFnSc1	ruka
nebyla	být	k5eNaImAgFnS	být
první	první	k4xOgFnSc1	první
<g/>
,	,	kIx,	,
o	o	k7c4	o
niž	jenž	k3xRgFnSc4	jenž
se	se	k3xPyFc4	se
ucházel	ucházet	k5eAaImAgMnS	ucházet
–	–	k?	–
uvažoval	uvažovat	k5eAaImAgMnS	uvažovat
o	o	k7c4	o
několikero	několikero	k4xRyIgNnSc4	několikero
mladých	mladý	k2eAgMnPc2d1	mladý
princeznách	princezna	k1gFnPc6	princezna
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
však	však	k9	však
jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
Augusta	Augusta	k1gMnSc1	Augusta
Sasko-Gothajská	saskoothajský	k2eAgFnSc1d1	sasko-gothajský
i	i	k8xC	i
její	její	k3xOp3gFnSc4	její
rádci	rádce	k1gMnPc1	rádce
nepovažovali	považovat	k5eNaImAgMnP	považovat
za	za	k7c4	za
vhodné	vhodný	k2eAgFnPc4d1	vhodná
partie	partie	k1gFnPc4	partie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Manželství	manželství	k1gNnSc1	manželství
===	===	k?	===
</s>
</p>
<p>
<s>
Šarlota	Šarlota	k1gFnSc1	Šarlota
přijela	přijet	k5eAaPmAgFnS	přijet
do	do	k7c2	do
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1761	[number]	k4	1761
a	a	k8xC	a
8	[number]	k4	8
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
provdala	provdat	k5eAaPmAgFnS	provdat
za	za	k7c4	za
krále	král	k1gMnSc4	král
Jiřího	Jiří	k1gMnSc2	Jiří
III	III	kA	III
<g/>
.	.	kIx.	.
v	v	k7c6	v
Královské	královský	k2eAgFnSc6d1	královská
kapli	kaple	k1gFnSc6	kaple
St	St	kA	St
James	James	k1gInSc1	James
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Palace	Palace	k1gFnSc1	Palace
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
týdny	týden	k1gInPc4	týden
později	pozdě	k6eAd2	pozdě
byli	být	k5eAaImAgMnP	být
oba	dva	k4xCgMnPc1	dva
korunováni	korunován	k2eAgMnPc1d1	korunován
ve	v	k7c6	v
Westminsterském	Westminsterský	k2eAgNnSc6d1	Westminsterské
opatství	opatství	k1gNnSc6	opatství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tchyně	tchyně	k1gFnSc1	tchyně
<g/>
,	,	kIx,	,
waleská	waleský	k2eAgFnSc1d1	Waleská
princezna	princezna	k1gFnSc1	princezna
Augusta	August	k1gMnSc2	August
Sasko-Gothajská	saskoothajský	k2eAgFnSc1d1	sasko-gothajský
<g/>
,	,	kIx,	,
Šarlotu	Šarlota	k1gFnSc4	Šarlota
nepřivítala	přivítat	k5eNaPmAgFnS	přivítat
s	s	k7c7	s
otevřenou	otevřený	k2eAgFnSc7d1	otevřená
náručí	náruč	k1gFnSc7	náruč
a	a	k8xC	a
jistý	jistý	k2eAgInSc4d1	jistý
čas	čas	k1gInSc4	čas
panovalo	panovat	k5eAaImAgNnS	panovat
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgFnPc7	dva
ženami	žena	k1gFnPc7	žena
velké	velká	k1gFnSc2	velká
napětí	napětí	k1gNnSc2	napětí
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
tento	tento	k3xDgInSc4	tento
nedostatek	nedostatek	k1gInSc4	nedostatek
sympatií	sympatie	k1gFnPc2	sympatie
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
tchyně	tchyně	k1gFnSc2	tchyně
i	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
Jiří	Jiří	k1gMnSc1	Jiří
měl	mít	k5eAaImAgMnS	mít
před	před	k7c7	před
manželstvím	manželství	k1gNnSc7	manželství
mnoho	mnoho	k6eAd1	mnoho
avantýr	avantýra	k1gFnPc2	avantýra
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
jejich	jejich	k3xOp3gNnSc1	jejich
manželství	manželství	k1gNnSc1	manželství
šťastné	šťastný	k2eAgNnSc1d1	šťastné
<g/>
;	;	kIx,	;
po	po	k7c6	po
svatbě	svatba	k1gFnSc6	svatba
již	již	k6eAd1	již
byl	být	k5eAaImAgMnS	být
Jiří	Jiří	k1gMnSc1	Jiří
Šarlotě	Šarlota	k1gFnSc3	Šarlota
vždy	vždy	k6eAd1	vždy
věrný	věrný	k2eAgInSc1d1	věrný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
času	čas	k1gInSc2	čas
získala	získat	k5eAaPmAgFnS	získat
Šarlota	Šarlota	k1gFnSc1	Šarlota
velký	velký	k2eAgInSc4d1	velký
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
Jiřího	Jiří	k1gMnSc4	Jiří
a	a	k8xC	a
přístup	přístup	k1gInSc4	přístup
do	do	k7c2	do
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
nezneužívala	zneužívat	k5eNaImAgFnS	zneužívat
jej	on	k3xPp3gNnSc4	on
však	však	k9	však
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Královna	královna	k1gFnSc1	královna
Šarlota	Šarlota	k1gFnSc1	Šarlota
se	se	k3xPyFc4	se
zajímala	zajímat	k5eAaImAgFnS	zajímat
o	o	k7c6	o
umění	umění	k1gNnSc6	umění
<g/>
,	,	kIx,	,
podporovala	podporovat	k5eAaImAgFnS	podporovat
Johanna	Johann	k1gMnSc4	Johann
Christiana	Christian	k1gMnSc4	Christian
Bacha	Bacha	k?	Bacha
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
jejím	její	k3xOp3gMnSc7	její
učitelem	učitel	k1gMnSc7	učitel
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
i	i	k8xC	i
Wolfganag	Wolfganaga	k1gFnPc2	Wolfganaga
Amadea	Amadeus	k1gMnSc4	Amadeus
Mozarta	Mozart	k1gMnSc4	Mozart
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
jí	jíst	k5eAaImIp3nS	jíst
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
8	[number]	k4	8
let	léto	k1gNnPc2	léto
věnoval	věnovat	k5eAaPmAgMnS	věnovat
jedno	jeden	k4xCgNnSc4	jeden
ze	z	k7c2	z
svých	svůj	k3xOyFgNnPc2	svůj
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
<s>
Královna	královna	k1gFnSc1	královna
byla	být	k5eAaImAgFnS	být
i	i	k9	i
velmi	velmi	k6eAd1	velmi
zdatným	zdatný	k2eAgMnSc7d1	zdatný
amatérským	amatérský	k2eAgMnSc7d1	amatérský
botanikem	botanik	k1gMnSc7	botanik
a	a	k8xC	a
významně	významně	k6eAd1	významně
přispěla	přispět	k5eAaPmAgFnS	přispět
k	k	k7c3	k
vybudování	vybudování	k1gNnSc3	vybudování
Královských	královský	k2eAgFnPc2d1	královská
botanických	botanický	k2eAgFnPc2d1	botanická
zahrad	zahrada	k1gFnPc2	zahrada
(	(	kIx(	(
<g/>
Kew	Kew	k1gFnSc1	Kew
Gardens	Gardensa	k1gFnPc2	Gardensa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zakládala	zakládat	k5eAaImAgFnS	zakládat
domy	dům	k1gInPc4	dům
a	a	k8xC	a
nemocnice	nemocnice	k1gFnPc4	nemocnice
pro	pro	k7c4	pro
nastávající	nastávající	k2eAgFnPc4d1	nastávající
matky	matka	k1gFnPc4	matka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velkou	velký	k2eAgFnSc4d1	velká
váhu	váha	k1gFnSc4	váha
přikládala	přikládat	k5eAaImAgFnS	přikládat
vzdělání	vzdělání	k1gNnSc3	vzdělání
jako	jako	k8xC	jako
takovému	takový	k3xDgNnSc3	takový
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
však	však	k9	však
vzdělání	vzdělání	k1gNnSc2	vzdělání
a	a	k8xC	a
výchově	výchova	k1gFnSc3	výchova
dívek	dívka	k1gFnPc2	dívka
a	a	k8xC	a
žen	žena	k1gFnPc2	žena
<g/>
;	;	kIx,	;
ona	onen	k3xDgFnSc1	onen
sama	sám	k3xTgMnSc4	sám
dohlížela	dohlížet	k5eAaImAgFnS	dohlížet
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
její	její	k3xOp3gFnSc2	její
dcery	dcera	k1gFnSc2	dcera
získaly	získat	k5eAaPmAgFnP	získat
lepší	dobrý	k2eAgNnSc4d2	lepší
vzdělání	vzdělání	k1gNnSc4	vzdělání
než	než	k8xS	než
bylo	být	k5eAaImAgNnS	být
běžné	běžný	k2eAgNnSc1d1	běžné
u	u	k7c2	u
mladých	mladý	k2eAgFnPc2d1	mladá
žen	žena	k1gFnPc2	žena
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Manželova	manželův	k2eAgFnSc1d1	manželova
nemoc	nemoc	k1gFnSc1	nemoc
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
vypuknutí	vypuknutí	k1gNnSc6	vypuknutí
Jiřího	Jiří	k1gMnSc2	Jiří
choroby	choroba	k1gFnSc2	choroba
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
označované	označovaný	k2eAgFnPc1d1	označovaná
jako	jako	k8xS	jako
šílenství	šílenství	k1gNnSc1	šílenství
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Šarlota	Šarlota	k1gFnSc1	Šarlota
ujala	ujmout	k5eAaPmAgFnS	ujmout
péče	péče	k1gFnSc1	péče
o	o	k7c4	o
něj	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
nemohla	moct	k5eNaImAgFnS	moct
však	však	k9	však
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
být	být	k5eAaImF	být
tak	tak	k6eAd1	tak
často	často	k6eAd1	často
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
by	by	kYmCp3nS	by
chtěla	chtít	k5eAaImAgFnS	chtít
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gNnSc4	jeho
nepříčetné	příčetný	k2eNgNnSc4d1	nepříčetné
chování	chování	k1gNnSc4	chování
a	a	k8xC	a
časté	častý	k2eAgFnPc4d1	častá
nepředvídatelné	předvídatelný	k2eNgFnPc4d1	nepředvídatelná
nebezpečné	bezpečný	k2eNgFnPc4d1	nebezpečná
reakce	reakce	k1gFnPc4	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
však	však	k9	však
Šarlota	Šarlota	k1gFnSc1	Šarlota
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gInPc1	jeho
oporou	opora	k1gFnSc7	opora
v	v	k7c6	v
nemoci	nemoc	k1gFnSc6	nemoc
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
projevy	projev	k1gInPc4	projev
porfyrie	porfyrie	k1gFnSc2	porfyrie
<g/>
,	,	kIx,	,
těžké	těžký	k2eAgFnSc2d1	těžká
metabolické	metabolický	k2eAgFnSc2d1	metabolická
poruchy	porucha	k1gFnSc2	porucha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
časem	časem	k6eAd1	časem
zhoršovala	zhoršovat	k5eAaImAgFnS	zhoršovat
<g/>
.	.	kIx.	.
</s>
<s>
Třebaže	třebaže	k8xS	třebaže
jejich	jejich	k3xOp3gMnSc1	jejich
syn	syn	k1gMnSc1	syn
Jiří	Jiří	k1gMnSc1	Jiří
převzal	převzít	k5eAaPmAgMnS	převzít
panovnické	panovnický	k2eAgFnPc4d1	panovnická
povinnosti	povinnost	k1gFnPc4	povinnost
<g/>
,	,	kIx,	,
Šarlota	Šarlota	k1gFnSc1	Šarlota
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
právní	právní	k2eAgFnSc6d1	právní
stránce	stránka	k1gFnSc6	stránka
královou	králův	k2eAgFnSc7d1	králova
zástupkyní	zástupkyně	k1gFnSc7	zástupkyně
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1811	[number]	k4	1811
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zemřela	zemřít	k5eAaPmAgFnS	zemřít
v	v	k7c4	v
Dutch	Dutch	k1gInSc4	Dutch
House	house	k1gNnSc4	house
v	v	k7c4	v
Surrey	Surrea	k1gFnPc4	Surrea
u	u	k7c2	u
paláce	palác	k1gInSc2	palác
Kew	Kew	k1gFnSc2	Kew
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
roku	rok	k1gInSc2	rok
1818	[number]	k4	1818
v	v	k7c6	v
74	[number]	k4	74
letech	léto	k1gNnPc6	léto
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
<s>
Zhasla	zhasnout	k5eAaPmAgFnS	zhasnout
sedíc	sedit	k5eAaImSgFnS	sedit
v	v	k7c6	v
křesle	křeslo	k1gNnSc6	křeslo
při	při	k7c6	při
malování	malování	k1gNnSc6	malování
portrétu	portrét	k1gInSc2	portrét
rodiny	rodina	k1gFnSc2	rodina
<g/>
;	;	kIx,	;
v	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
její	její	k3xOp3gFnSc2	její
smrti	smrt	k1gFnSc2	smrt
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
jejím	její	k3xOp3gInSc6	její
boku	bok	k1gInSc6	bok
její	její	k3xOp3gMnSc1	její
nejstarší	starý	k2eAgMnSc1d3	nejstarší
syn	syn	k1gMnSc1	syn
<g/>
,	,	kIx,	,
waleský	waleský	k2eAgMnSc1d1	waleský
princ	princ	k1gMnSc1	princ
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
,	,	kIx,	,
budoucí	budoucí	k2eAgMnSc1d1	budoucí
král	král	k1gMnSc1	král
Jiří	Jiří	k1gMnSc1	Jiří
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
podpíral	podpírat	k5eAaImAgMnS	podpírat
její	její	k3xOp3gFnSc4	její
ruku	ruka	k1gFnSc4	ruka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pochována	pochován	k2eAgFnSc1d1	pochována
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
kapli	kaple	k1gFnSc6	kaple
sv.	sv.	kA	sv.
Jiří	Jiří	k1gMnSc2	Jiří
na	na	k7c6	na
hradě	hrad	k1gInSc6	hrad
Windsor	Windsor	k1gInSc1	Windsor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Potomci	potomek	k1gMnPc5	potomek
==	==	k?	==
</s>
</p>
<p>
<s>
Šarlota	Šarlota	k1gFnSc1	Šarlota
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
měli	mít	k5eAaImAgMnP	mít
spolu	spolu	k6eAd1	spolu
patnáct	patnáct	k4xCc4	patnáct
potomků	potomek	k1gMnPc2	potomek
<g/>
,	,	kIx,	,
devět	devět	k4xCc1	devět
synů	syn	k1gMnPc2	syn
a	a	k8xC	a
šest	šest	k4xCc1	šest
dcer	dcera	k1gFnPc2	dcera
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
dva	dva	k4xCgInPc4	dva
budoucí	budoucí	k2eAgMnSc1d1	budoucí
krále	král	k1gMnSc4	král
Velké	velká	k1gFnSc2	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgMnPc1	dva
poslední	poslední	k2eAgMnPc1d1	poslední
synové	syn	k1gMnPc1	syn
zemřeli	zemřít	k5eAaPmAgMnP	zemřít
v	v	k7c6	v
útlém	útlý	k2eAgInSc6d1	útlý
věku	věk	k1gInSc6	věk
na	na	k7c4	na
neštovice	neštovice	k1gFnPc4	neštovice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
IV	IV	kA	IV
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1762	[number]	k4	1762
–	–	k?	–
26	[number]	k4	26
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1830	[number]	k4	1830
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
britský	britský	k2eAgMnSc1d1	britský
král	král	k1gMnSc1	král
∞	∞	k?	∞
1795	[number]	k4	1795
princezna	princezna	k1gFnSc1	princezna
Karolina	Karolinum	k1gNnSc2	Karolinum
Brunšvická	brunšvický	k2eAgFnSc1d1	Brunšvická
(	(	kIx(	(
<g/>
1768	[number]	k4	1768
<g/>
–	–	k?	–
<g/>
1821	[number]	k4	1821
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Fridrich	Fridrich	k1gMnSc1	Fridrich
August	August	k1gMnSc1	August
(	(	kIx(	(
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1763	[number]	k4	1763
–	–	k?	–
5	[number]	k4	5
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1827	[number]	k4	1827
<g/>
)	)	kIx)	)
∞	∞	k?	∞
1791	[number]	k4	1791
princezna	princezna	k1gFnSc1	princezna
Jindřiška	Jindřiška	k1gFnSc1	Jindřiška
Šarlota	Šarlota	k1gFnSc1	Šarlota
Pruská	pruský	k2eAgFnSc1d1	pruská
(	(	kIx(	(
<g/>
1767	[number]	k4	1767
<g/>
–	–	k?	–
<g/>
1820	[number]	k4	1820
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vilém	Vilém	k1gMnSc1	Vilém
IV	IV	kA	IV
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1765	[number]	k4	1765
–	–	k?	–
20	[number]	k4	20
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1837	[number]	k4	1837
<g/>
)	)	kIx)	)
∞	∞	k?	∞
1818	[number]	k4	1818
princezna	princezna	k1gFnSc1	princezna
Adelheid	Adelheida	k1gFnPc2	Adelheida
Sasko-Meiningenská	Sasko-Meiningenský	k2eAgFnSc1d1	Sasko-Meiningenský
(	(	kIx(	(
<g/>
1792	[number]	k4	1792
<g/>
–	–	k?	–
<g/>
1849	[number]	k4	1849
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Šarlota	Šarlota	k1gFnSc1	Šarlota
(	(	kIx(	(
<g/>
29	[number]	k4	29
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1766	[number]	k4	1766
–	–	k?	–
5	[number]	k4	5
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1828	[number]	k4	1828
<g/>
)	)	kIx)	)
∞	∞	k?	∞
1797	[number]	k4	1797
württemberský	württemberský	k2eAgMnSc1d1	württemberský
král	král	k1gMnSc1	král
Fridrich	Fridrich	k1gMnSc1	Fridrich
I.	I.	kA	I.
(	(	kIx(	(
<g/>
1754	[number]	k4	1754
<g/>
–	–	k?	–
<g/>
1816	[number]	k4	1816
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Eduard	Eduard	k1gMnSc1	Eduard
August	August	k1gMnSc1	August
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1767	[number]	k4	1767
–	–	k?	–
23	[number]	k4	23
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1820	[number]	k4	1820
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Kentu	Kent	k1gInSc2	Kent
a	a	k8xC	a
Strathearnu	Strathearn	k1gInSc2	Strathearn
∞	∞	k?	∞
1818	[number]	k4	1818
princezna	princezna	k1gFnSc1	princezna
Viktorie	Viktorie	k1gFnSc1	Viktorie
Sasko-Kobursko-Saalfeldská	Sasko-Kobursko-Saalfeldský	k2eAgFnSc1d1	Sasko-Kobursko-Saalfeldský
(	(	kIx(	(
<g/>
1786	[number]	k4	1786
–	–	k?	–
1861	[number]	k4	1861
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
otec	otec	k1gMnSc1	otec
královny	královna	k1gFnSc2	královna
Viktorie	Viktoria	k1gFnSc2	Viktoria
</s>
</p>
<p>
<s>
Augusta	Augusta	k1gMnSc1	Augusta
Žofie	Žofie	k1gFnSc2	Žofie
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1768	[number]	k4	1768
–	–	k?	–
22	[number]	k4	22
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1840	[number]	k4	1840
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Alžběta	Alžběta	k1gFnSc1	Alžběta
(	(	kIx(	(
<g/>
22	[number]	k4	22
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1770	[number]	k4	1770
–	–	k?	–
10	[number]	k4	10
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1840	[number]	k4	1840
<g/>
)	)	kIx)	)
∞	∞	k?	∞
1818	[number]	k4	1818
lankrabě	lankrabě	k1gMnSc1	lankrabě
Fridrich	Fridrich	k1gMnSc1	Fridrich
VI	VI	kA	VI
<g/>
.	.	kIx.	.
</s>
<s>
Hessensko-Homburský	Hessensko-Homburský	k2eAgMnSc1d1	Hessensko-Homburský
(	(	kIx(	(
<g/>
1769	[number]	k4	1769
<g/>
–	–	k?	–
<g/>
1829	[number]	k4	1829
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ernst	Ernst	k1gMnSc1	Ernst
August	August	k1gMnSc1	August
I.	I.	kA	I.
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1771	[number]	k4	1771
–	–	k?	–
18	[number]	k4	18
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1851	[number]	k4	1851
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hannoverský	hannoverský	k2eAgMnSc1d1	hannoverský
král	král	k1gMnSc1	král
∞	∞	k?	∞
1815	[number]	k4	1815
princezna	princezna	k1gFnSc1	princezna
Frederika	Frederika	k1gFnSc1	Frederika
Meklenbursko-Střelická	Meklenbursko-Střelický	k2eAgFnSc1d1	Meklenbursko-Střelický
(	(	kIx(	(
<g/>
1778	[number]	k4	1778
<g/>
–	–	k?	–
<g/>
1841	[number]	k4	1841
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
August	August	k1gMnSc1	August
Frederik	Frederik	k1gMnSc1	Frederik
(	(	kIx(	(
<g/>
27	[number]	k4	27
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1773	[number]	k4	1773
–	–	k?	–
21	[number]	k4	21
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1843	[number]	k4	1843
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vévoda	vévoda	k1gMnSc1	vévoda
ze	z	k7c2	z
Sussexu	Sussex	k1gInSc2	Sussex
∞	∞	k?	∞
</s>
</p>
<p>
<s>
1793	[number]	k4	1793
lady	lady	k1gFnSc1	lady
Augusta	August	k1gMnSc2	August
Murray	Murraa	k1gMnSc2	Murraa
(	(	kIx(	(
<g/>
1768	[number]	k4	1768
–	–	k?	–
1830	[number]	k4	1830
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
anulováno	anulován	k2eAgNnSc1d1	anulováno
1794	[number]	k4	1794
</s>
</p>
<p>
<s>
1831	[number]	k4	1831
lady	lady	k1gFnSc1	lady
Cecilia	Cecilia	k1gFnSc1	Cecilia
Underwood	underwood	k1gInSc1	underwood
(	(	kIx(	(
<g/>
1785	[number]	k4	1785
–	–	k?	–
1873	[number]	k4	1873
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Adolf	Adolf	k1gMnSc1	Adolf
Frederik	Frederik	k1gMnSc1	Frederik
(	(	kIx(	(
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1774	[number]	k4	1774
–	–	k?	–
8	[number]	k4	8
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1850	[number]	k4	1850
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vévoda	vévoda	k1gMnSc1	vévoda
Cambridge	Cambridge	k1gFnSc2	Cambridge
∞	∞	k?	∞
1818	[number]	k4	1818
princezna	princezna	k1gFnSc1	princezna
Augusta	August	k1gMnSc2	August
Hesensko-Kaselská	Hesensko-Kaselský	k2eAgFnSc1d1	Hesensko-Kaselský
(	(	kIx(	(
<g/>
1797	[number]	k4	1797
<g/>
–	–	k?	–
<g/>
1889	[number]	k4	1889
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Marie	Marie	k1gFnSc1	Marie
(	(	kIx(	(
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1776	[number]	k4	1776
–	–	k?	–
39	[number]	k4	39
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1857	[number]	k4	1857
<g/>
)	)	kIx)	)
∞	∞	k?	∞
1816	[number]	k4	1816
Vilém	Vilém	k1gMnSc1	Vilém
Fridrich	Fridrich	k1gMnSc1	Fridrich
<g/>
,	,	kIx,	,
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Gloucesteru	Gloucester	k1gInSc2	Gloucester
a	a	k8xC	a
Edinburghu	Edinburgh	k1gInSc2	Edinburgh
(	(	kIx(	(
<g/>
1776	[number]	k4	1776
–	–	k?	–
1834	[number]	k4	1834
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sophia	Sophia	k1gFnSc1	Sophia
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1777	[number]	k4	1777
–	–	k?	–
27	[number]	k4	27
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1848	[number]	k4	1848
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Octavius	Octavius	k1gInSc1	Octavius
(	(	kIx(	(
<g/>
23	[number]	k4	23
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1779	[number]	k4	1779
–	–	k?	–
3	[number]	k4	3
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1783	[number]	k4	1783
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Alfred	Alfred	k1gMnSc1	Alfred
(	(	kIx(	(
<g/>
22	[number]	k4	22
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1780	[number]	k4	1780
–	–	k?	–
20	[number]	k4	20
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1782	[number]	k4	1782
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Amelia	Amelia	k1gFnSc1	Amelia
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1783	[number]	k4	1783
–	–	k?	–
2	[number]	k4	2
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1810	[number]	k4	1810
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Vztahy	vztah	k1gInPc1	vztah
s	s	k7c7	s
Marií	Maria	k1gFnSc7	Maria
Antoinettou	Antoinetta	k1gFnSc7	Antoinetta
==	==	k?	==
</s>
</p>
<p>
<s>
Šarlota	Šarlota	k1gFnSc1	Šarlota
udržovala	udržovat	k5eAaImAgFnS	udržovat
úzké	úzký	k2eAgInPc4d1	úzký
vztahy	vztah	k1gInPc4	vztah
s	s	k7c7	s
francouzskou	francouzský	k2eAgFnSc7d1	francouzská
královnou	královna	k1gFnSc7	královna
Marií	Maria	k1gFnSc7	Maria
Antoinettou	Antoinetta	k1gFnSc7	Antoinetta
<g/>
.	.	kIx.	.
</s>
<s>
Spojovala	spojovat	k5eAaImAgFnS	spojovat
je	být	k5eAaImIp3nS	být
láska	láska	k1gFnSc1	láska
k	k	k7c3	k
hudbě	hudba	k1gFnSc3	hudba
a	a	k8xC	a
umění	umění	k1gNnSc3	umění
<g/>
,	,	kIx,	,
jimž	jenž	k3xRgMnPc3	jenž
se	se	k3xPyFc4	se
i	i	k8xC	i
samy	sám	k3xTgInPc1	sám
věnovaly	věnovat	k5eAaImAgInP	věnovat
<g/>
;	;	kIx,	;
nikdy	nikdy	k6eAd1	nikdy
se	se	k3xPyFc4	se
však	však	k9	však
osobně	osobně	k6eAd1	osobně
nesetkaly	setkat	k5eNaPmAgFnP	setkat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vypuknutí	vypuknutí	k1gNnSc6	vypuknutí
francouzské	francouzský	k2eAgFnSc2d1	francouzská
revoluce	revoluce	k1gFnSc2	revoluce
britská	britský	k2eAgFnSc1d1	britská
královna	královna	k1gFnSc1	královna
již	již	k6eAd1	již
chystala	chystat	k5eAaImAgFnS	chystat
apartmány	apartmán	k1gInPc4	apartmán
pro	pro	k7c4	pro
Marii	Maria	k1gFnSc4	Maria
Antoinettu	Antoinetta	k1gFnSc4	Antoinetta
a	a	k8xC	a
její	její	k3xOp3gFnSc4	její
rodinu	rodina	k1gFnSc4	rodina
<g/>
,	,	kIx,	,
předpokládajíc	předpokládat	k5eAaImSgFnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gInSc7	jejich
údělem	úděl	k1gInSc7	úděl
bude	být	k5eAaImBp3nS	být
exil	exil	k1gInSc1	exil
<g/>
,	,	kIx,	,
vývoj	vývoj	k1gInSc1	vývoj
událostí	událost	k1gFnPc2	událost
však	však	k9	však
spěl	spět	k5eAaImAgMnS	spět
jinam	jinam	k6eAd1	jinam
–	–	k?	–
francouzskému	francouzský	k2eAgInSc3d1	francouzský
královskému	královský	k2eAgInSc3d1	královský
páru	pár	k1gInSc3	pár
se	se	k3xPyFc4	se
nepodařilo	podařit	k5eNaPmAgNnS	podařit
prchnout	prchnout	k5eAaPmF	prchnout
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
popraven	popravit	k5eAaPmNgInS	popravit
<g/>
.	.	kIx.	.
</s>
<s>
Šarlotta	Šarlotta	k1gFnSc1	Šarlotta
byla	být	k5eAaImAgFnS	být
jejich	jejich	k3xOp3gInSc7	jejich
tragickým	tragický	k2eAgInSc7d1	tragický
osudem	osud	k1gInSc7	osud
zdrcena	zdrtit	k5eAaPmNgFnS	zdrtit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavost	zajímavost	k1gFnSc4	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Královně	královna	k1gFnSc3	královna
Šarlottě	Šarlotta	k1gFnSc3	Šarlotta
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
jedné	jeden	k4xCgFnSc2	jeden
verze	verze	k1gFnSc2	verze
připisuje	připisovat	k5eAaImIp3nS	připisovat
autorství	autorství	k1gNnSc1	autorství
receptu	recept	k1gInSc2	recept
na	na	k7c4	na
šarlotku	šarlotka	k1gFnSc4	šarlotka
<g/>
,	,	kIx,	,
sladký	sladký	k2eAgInSc4d1	sladký
dezert	dezert	k1gInSc4	dezert
z	z	k7c2	z
jablek	jablko	k1gNnPc2	jablko
zapečených	zapečený	k2eAgMnPc2d1	zapečený
v	v	k7c6	v
těstě	těsto	k1gNnSc6	těsto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vývod	vývod	k1gInSc1	vývod
z	z	k7c2	z
předků	předek	k1gInPc2	předek
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Šarlota	Šarlota	k1gFnSc1	Šarlota
Meklenbursko-Střelická	Meklenbursko-Střelický	k2eAgFnSc1d1	Meklenbursko-Střelický
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Šarlota	Šarlota	k1gFnSc1	Šarlota
Meklenbursko-Střelická	Meklenbursko-Střelický	k2eAgFnSc1d1	Meklenbursko-Střelický
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
http://www.lib.virginia.edu/small/exhibits/charlotte/sophie.html	[url]	k1gMnSc1	http://www.lib.virginia.edu/small/exhibits/charlotte/sophie.html
</s>
</p>
<p>
<s>
http://www.thepeerage.com/p10078.htm#i100778	[url]	k4	http://www.thepeerage.com/p10078.htm#i100778
</s>
</p>
