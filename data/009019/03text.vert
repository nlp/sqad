<p>
<s>
Imunologie	imunologie	k1gFnSc1	imunologie
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
zjednodušeně	zjednodušeně	k6eAd1	zjednodušeně
řečeno	říct	k5eAaPmNgNnS	říct
<g/>
,	,	kIx,	,
věda	věda	k1gFnSc1	věda
zabývající	zabývající	k2eAgFnSc1d1	zabývající
se	s	k7c7	s
zkoumáním	zkoumání	k1gNnSc7	zkoumání
imunitního	imunitní	k2eAgInSc2d1	imunitní
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
bývá	bývat	k5eAaImIp3nS	bývat
ne	ne	k9	ne
zcela	zcela	k6eAd1	zcela
správně	správně	k6eAd1	správně
chápána	chápat	k5eAaImNgFnS	chápat
jako	jako	k8xC	jako
odvětví	odvětví	k1gNnSc1	odvětví
medicíny	medicína	k1gFnSc2	medicína
<g/>
,	,	kIx,	,
její	její	k3xOp3gInSc1	její
rozsah	rozsah	k1gInSc1	rozsah
je	být	k5eAaImIp3nS	být
však	však	k9	však
daleko	daleko	k6eAd1	daleko
širší	široký	k2eAgFnSc1d2	širší
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejstarší	starý	k2eAgNnSc1d3	nejstarší
doložené	doložený	k2eAgNnSc1d1	doložené
využití	využití	k1gNnSc1	využití
imunologických	imunologický	k2eAgFnPc2d1	imunologická
metod	metoda	k1gFnPc2	metoda
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
z	z	k7c2	z
Číny	Čína	k1gFnSc2	Čína
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
používáno	používán	k2eAgNnSc1d1	používáno
vdechování	vdechování	k1gNnSc1	vdechování
usušených	usušený	k2eAgInPc2d1	usušený
neštovičních	neštoviční	k2eAgInPc2d1	neštoviční
strupů	strup	k1gInPc2	strup
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgInPc1d1	obsahující
především	především	k6eAd1	především
neaktivní	aktivní	k2eNgInPc1d1	neaktivní
viriony	virion	k1gInPc1	virion
<g/>
,	,	kIx,	,
k	k	k7c3	k
ochraně	ochrana	k1gFnSc3	ochrana
před	před	k7c7	před
pravými	pravý	k2eAgFnPc7d1	pravá
neštovicemi	neštovice	k1gFnPc7	neštovice
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
variolace	variolace	k1gFnSc5	variolace
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
zdokonalil	zdokonalit	k5eAaPmAgMnS	zdokonalit
britský	britský	k2eAgMnSc1d1	britský
lékař	lékař	k1gMnSc1	lékař
Edward	Edward	k1gMnSc1	Edward
Jenner	Jenner	k1gMnSc1	Jenner
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
použil	použít	k5eAaPmAgMnS	použít
k	k	k7c3	k
očkování	očkování	k1gNnSc3	očkování
proti	proti	k7c3	proti
pravým	pravý	k2eAgFnPc3d1	pravá
neštovicím	neštovice	k1gFnPc3	neštovice
virus	virus	k1gInSc1	virus
neštovic	neštovice	k1gFnPc2	neštovice
kravských	kravský	k2eAgFnPc2d1	kravská
a	a	k8xC	a
položil	položit	k5eAaPmAgMnS	položit
tak	tak	k6eAd1	tak
základy	základ	k1gInPc4	základ
vakcinace	vakcinace	k1gFnSc2	vakcinace
(	(	kIx(	(
<g/>
vacca	vacca	k6eAd1	vacca
-	-	kIx~	-
latinsky	latinsky	k6eAd1	latinsky
kráva	kráva	k1gFnSc1	kráva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
významnou	významný	k2eAgFnSc7d1	významná
osobností	osobnost	k1gFnSc7	osobnost
období	období	k1gNnSc2	období
zrodu	zrod	k1gInSc2	zrod
tohoto	tento	k3xDgInSc2	tento
oboru	obor	k1gInSc2	obor
byl	být	k5eAaImAgMnS	být
Louis	Louis	k1gMnSc1	Louis
Pasteur	Pasteur	k1gMnSc1	Pasteur
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
kromě	kromě	k7c2	kromě
jiného	jiný	k2eAgNnSc2d1	jiné
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
očkování	očkování	k1gNnSc4	očkování
proti	proti	k7c3	proti
anthraxu	anthrax	k1gInSc3	anthrax
a	a	k8xC	a
vzteklině	vzteklina	k1gFnSc3	vzteklina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Imunologie	imunologie	k1gFnSc1	imunologie
poskytla	poskytnout	k5eAaPmAgFnS	poskytnout
lidstvu	lidstvo	k1gNnSc3	lidstvo
vedle	vedle	k7c2	vedle
vakcinačních	vakcinační	k2eAgInPc2d1	vakcinační
programů	program	k1gInPc2	program
i	i	k8xC	i
moderní	moderní	k2eAgFnSc2d1	moderní
způsoby	způsoba	k1gFnSc2	způsoba
imunoterapie	imunoterapie	k1gFnSc1	imunoterapie
lidských	lidský	k2eAgNnPc2d1	lidské
onemocnění	onemocnění	k1gNnPc2	onemocnění
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
založené	založený	k2eAgInPc1d1	založený
na	na	k7c4	na
použití	použití	k1gNnSc4	použití
uměle	uměle	k6eAd1	uměle
vytvořených	vytvořený	k2eAgFnPc2d1	vytvořená
monoklonálních	monoklonální	k2eAgFnPc2d1	monoklonální
protilátek	protilátka	k1gFnPc2	protilátka
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
hlavní	hlavní	k2eAgInPc4d1	hlavní
nástroje	nástroj	k1gInPc4	nástroj
biologické	biologický	k2eAgFnSc2d1	biologická
léčby	léčba	k1gFnSc2	léčba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
dnes	dnes	k6eAd1	dnes
tvoří	tvořit	k5eAaImIp3nS	tvořit
zásadní	zásadní	k2eAgFnSc4d1	zásadní
část	část	k1gFnSc4	část
vývoje	vývoj	k1gInSc2	vývoj
nových	nový	k2eAgNnPc2d1	nové
léčiv	léčivo	k1gNnPc2	léčivo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Klasicky	klasicky	k6eAd1	klasicky
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
imunologie	imunologie	k1gFnPc1	imunologie
podle	podle	k7c2	podle
funkce	funkce	k1gFnSc2	funkce
na	na	k7c4	na
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
evolučně	evolučně	k6eAd1	evolučně
starší	starý	k2eAgMnSc1d2	starší
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
funkčně	funkčně	k6eAd1	funkčně
důležitější	důležitý	k2eAgFnSc4d2	důležitější
vrozenou	vrozený	k2eAgFnSc4d1	vrozená
imunitu	imunita	k1gFnSc4	imunita
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Innate	Innat	k1gInSc5	Innat
immunity	immunit	k2eAgInPc4d1	immunit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
spoléhá	spoléhat	k5eAaImIp3nS	spoléhat
na	na	k7c4	na
antigenně-nespecifickou	antigenněespecifický	k2eAgFnSc4d1	antigenně-nespecifický
odpověď	odpověď	k1gFnSc4	odpověď
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
zánět	zánět	k1gInSc4	zánět
<g/>
,	,	kIx,	,
srážení	srážení	k1gNnSc4	srážení
krve	krev	k1gFnSc2	krev
<g/>
,	,	kIx,	,
fagocytóza	fagocytóza	k1gFnSc1	fagocytóza
<g/>
,	,	kIx,	,
antimikrobiální	antimikrobiální	k2eAgInPc1d1	antimikrobiální
peptidy	peptid	k1gInPc1	peptid
<g/>
,	,	kIx,	,
komplementový	komplementový	k2eAgInSc1d1	komplementový
systém	systém	k1gInSc1	systém
nebo	nebo	k8xC	nebo
rozeznávání	rozeznávání	k1gNnSc1	rozeznávání
molekulárních	molekulární	k2eAgInPc2d1	molekulární
vzorů	vzor	k1gInPc2	vzor
spojených	spojený	k2eAgInPc2d1	spojený
s	s	k7c7	s
patogeny	patogen	k1gInPc7	patogen
a	a	k8xC	a
nebo	nebo	k8xC	nebo
typických	typický	k2eAgInPc2d1	typický
pro	pro	k7c4	pro
poškození	poškození	k1gNnSc4	poškození
vlastních	vlastní	k2eAgFnPc2d1	vlastní
buněk	buňka	k1gFnPc2	buňka
a	a	k8xC	a
tkání	tkáň	k1gFnPc2	tkáň
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
PRR	PRR	kA	PRR
receptorů	receptor	k1gInPc2	receptor
vedoucí	vedoucí	k1gFnSc2	vedoucí
k	k	k7c3	k
aktivaci	aktivace	k1gFnSc3	aktivace
APC	APC	kA	APC
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
následně	následně	k6eAd1	následně
prezentují	prezentovat	k5eAaBmIp3nP	prezentovat
antigenní	antigenní	k2eAgInPc1d1	antigenní
fragmenty	fragment	k1gInPc1	fragment
specifickým	specifický	k2eAgNnSc7d1	specifické
T-lymfocytům	Tymfocyt	k1gInPc3	T-lymfocyt
</s>
</p>
<p>
<s>
získanou	získaný	k2eAgFnSc4d1	získaná
adaptivní	adaptivní	k2eAgFnSc4d1	adaptivní
imunitu	imunita	k1gFnSc4	imunita
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Adaptive	Adaptiv	k1gInSc5	Adaptiv
immunity	immunit	k2eAgFnPc4d1	immunit
<g/>
)	)	kIx)	)
využívající	využívající	k2eAgFnPc4d1	využívající
schopnosti	schopnost	k1gFnPc4	schopnost
vysoce	vysoce	k6eAd1	vysoce
rozmanitých	rozmanitý	k2eAgInPc2d1	rozmanitý
imunoreceptorů	imunoreceptor	k1gInPc2	imunoreceptor
lymfocytů	lymfocyt	k1gInPc2	lymfocyt
(	(	kIx(	(
<g/>
protilátek	protilátka	k1gFnPc2	protilátka
a	a	k8xC	a
T-buněčných	Tuněčný	k2eAgInPc2d1	T-buněčný
receptorů	receptor	k1gInPc2	receptor
u	u	k7c2	u
obratlovců	obratlovec	k1gMnPc2	obratlovec
<g/>
)	)	kIx)	)
rozpoznat	rozpoznat	k5eAaPmF	rozpoznat
specifické	specifický	k2eAgFnPc4d1	specifická
cizorodé	cizorodý	k2eAgFnPc4d1	cizorodá
struktury	struktura	k1gFnPc4	struktura
(	(	kIx(	(
<g/>
antigeny	antigen	k1gInPc4	antigen
<g/>
)	)	kIx)	)
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc1	jejich
fragmenty	fragment	k1gInPc1	fragment
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Laureáti	laureát	k1gMnPc1	laureát
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
==	==	k?	==
</s>
</p>
<p>
<s>
1901	[number]	k4	1901
Emil	Emil	k1gMnSc1	Emil
von	von	k1gInSc4	von
Behring	Behring	k1gInSc1	Behring
(	(	kIx(	(
<g/>
1854	[number]	k4	1854
<g/>
-	-	kIx~	-
<g/>
1917	[number]	k4	1917
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
za	za	k7c4	za
objev	objev	k1gInSc4	objev
séra	sérum	k1gNnSc2	sérum
proti	proti	k7c3	proti
záškrtu	záškrt	k1gInSc3	záškrt
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
první	první	k4xOgFnSc1	první
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
fyziologii	fyziologie	k1gFnSc4	fyziologie
a	a	k8xC	a
medicínu	medicína	k1gFnSc4	medicína
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1908	[number]	k4	1908
Ilja	Ilja	k1gMnSc1	Ilja
Iljič	Iljič	k1gMnSc1	Iljič
Mečnikov	Mečnikov	k1gInSc1	Mečnikov
a	a	k8xC	a
Paul	Paul	k1gMnSc1	Paul
Ehrlich	Ehrlich	k1gMnSc1	Ehrlich
(	(	kIx(	(
<g/>
1854	[number]	k4	1854
<g/>
-	-	kIx~	-
<g/>
1915	[number]	k4	1915
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
za	za	k7c4	za
výzkumy	výzkum	k1gInPc4	výzkum
imunitního	imunitní	k2eAgInSc2d1	imunitní
systému	systém	k1gInSc2	systém
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
1919	[number]	k4	1919
Jules	Jules	k1gMnSc1	Jules
Bordet	Bordet	k1gMnSc1	Bordet
(	(	kIx(	(
<g/>
1870	[number]	k4	1870
<g/>
-	-	kIx~	-
<g/>
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
za	za	k7c4	za
objevy	objev	k1gInPc4	objev
v	v	k7c4	v
související	související	k2eAgNnSc4d1	související
s	s	k7c7	s
imunitním	imunitní	k2eAgInSc7d1	imunitní
systémem	systém	k1gInSc7	systém
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
1930	[number]	k4	1930
Karl	Karl	k1gMnSc1	Karl
Landsteiner	Landsteiner	k1gMnSc1	Landsteiner
(	(	kIx(	(
<g/>
1868	[number]	k4	1868
<g/>
-	-	kIx~	-
<g/>
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
za	za	k7c4	za
výzkumy	výzkum	k1gInPc4	výzkum
typů	typ	k1gInPc2	typ
lidské	lidský	k2eAgFnSc2d1	lidská
krve	krev	k1gFnSc2	krev
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
1960	[number]	k4	1960
Peter	Peter	k1gMnSc1	Peter
B.	B.	kA	B.
Medawar	Medawar	k1gMnSc1	Medawar
(	(	kIx(	(
<g/>
1915	[number]	k4	1915
<g/>
-	-	kIx~	-
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
a	a	k8xC	a
Frank	Frank	k1gMnSc1	Frank
Macfarlane	Macfarlan	k1gMnSc5	Macfarlan
Burnet	Burnet	k1gMnSc1	Burnet
(	(	kIx(	(
<g/>
1899	[number]	k4	1899
<g/>
-	-	kIx~	-
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
objev	objev	k1gInSc1	objev
získané	získaný	k2eAgFnSc2d1	získaná
imunologické	imunologický	k2eAgFnSc2d1	imunologická
tolerance	tolerance	k1gFnSc2	tolerance
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
1972	[number]	k4	1972
Gerald	Gerald	k1gMnSc1	Gerald
Maurice	Maurika	k1gFnSc3	Maurika
Edelman	Edelman	k1gMnSc1	Edelman
(	(	kIx(	(
<g/>
*	*	kIx~	*
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
a	a	k8xC	a
Rodney	Rodnea	k1gFnSc2	Rodnea
Robert	Robert	k1gMnSc1	Robert
Porter	porter	k1gInSc1	porter
(	(	kIx(	(
<g/>
1917	[number]	k4	1917
<g/>
-	-	kIx~	-
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
za	za	k7c4	za
práci	práce	k1gFnSc4	práce
na	na	k7c6	na
struktuře	struktura	k1gFnSc6	struktura
protilátek	protilátka	k1gFnPc2	protilátka
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
1980	[number]	k4	1980
Baruj	Baruj	k1gMnSc1	Baruj
Benacerraf	Benacerraf	k1gMnSc1	Benacerraf
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
–	–	k?	–
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jean	Jean	k1gMnSc1	Jean
Dausset	Dausset	k1gMnSc1	Dausset
(	(	kIx(	(
<g/>
1916	[number]	k4	1916
<g/>
–	–	k?	–
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
a	a	k8xC	a
George	Georg	k1gMnSc2	Georg
Davis	Davis	k1gFnSc1	Davis
Snell	Snell	k1gMnSc1	Snell
(	(	kIx(	(
<g/>
1903	[number]	k4	1903
<g/>
-	-	kIx~	-
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
za	za	k7c4	za
rozvoj	rozvoj	k1gInSc4	rozvoj
studia	studio	k1gNnSc2	studio
histokompatibilních	histokompatibilní	k2eAgInPc2d1	histokompatibilní
antigenů	antigen	k1gInPc2	antigen
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
1984	[number]	k4	1984
Niels	Niels	k1gInSc1	Niels
Jerne	Jern	k1gInSc5	Jern
(	(	kIx(	(
<g/>
1911	[number]	k4	1911
<g/>
-	-	kIx~	-
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Georges	Georges	k1gMnSc1	Georges
J.	J.	kA	J.
F.	F.	kA	F.
Köhler	Köhler	k1gMnSc1	Köhler
(	(	kIx(	(
<g/>
1946	[number]	k4	1946
<g/>
-	-	kIx~	-
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
a	a	k8xC	a
César	César	k1gMnSc1	César
Milstein	Milstein	k1gMnSc1	Milstein
(	(	kIx(	(
<g/>
1927	[number]	k4	1927
<g/>
-	-	kIx~	-
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
za	za	k7c4	za
zásluhy	zásluha	k1gFnPc4	zásluha
o	o	k7c4	o
pokrok	pokrok	k1gInSc4	pokrok
v	v	k7c6	v
teorii	teorie	k1gFnSc6	teorie
imunitního	imunitní	k2eAgInSc2d1	imunitní
systému	systém	k1gInSc2	systém
a	a	k8xC	a
objev	objev	k1gInSc1	objev
principu	princip	k1gInSc2	princip
tvorby	tvorba	k1gFnSc2	tvorba
monoklonálních	monoklonální	k2eAgFnPc2d1	monoklonální
protilátek	protilátka	k1gFnPc2	protilátka
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
1987	[number]	k4	1987
Susumu	Susum	k1gInSc2	Susum
Tonegawa	Tonegaw	k1gInSc2	Tonegaw
(	(	kIx(	(
<g/>
*	*	kIx~	*
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
za	za	k7c4	za
objev	objev	k1gInSc4	objev
genetického	genetický	k2eAgInSc2d1	genetický
základu	základ	k1gInSc2	základ
vytváření	vytváření	k1gNnSc2	vytváření
variability	variabilita	k1gFnSc2	variabilita
protilátek	protilátka	k1gFnPc2	protilátka
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
1989	[number]	k4	1989
J.	J.	kA	J.
Michael	Michael	k1gMnSc1	Michael
Bishop	Bishop	k1gInSc1	Bishop
(	(	kIx(	(
<g/>
*	*	kIx~	*
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
a	a	k8xC	a
Harold	Harold	k1gMnSc1	Harold
E.	E.	kA	E.
Varmus	Varmus	k1gMnSc1	Varmus
(	(	kIx(	(
<g/>
*	*	kIx~	*
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
za	za	k7c4	za
objevy	objev	k1gInPc4	objev
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
retrovirů	retrovir	k1gInPc2	retrovir
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
1996	[number]	k4	1996
Peter	Peter	k1gMnSc1	Peter
C.	C.	kA	C.
Doherty	Dohert	k1gInPc1	Dohert
(	(	kIx(	(
<g/>
*	*	kIx~	*
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
a	a	k8xC	a
Rolf	Rolf	k1gMnSc1	Rolf
M.	M.	kA	M.
Zinkernagel	Zinkernagel	k1gMnSc1	Zinkernagel
(	(	kIx(	(
<g/>
*	*	kIx~	*
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
za	za	k7c4	za
objevy	objev	k1gInPc4	objev
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
ochrany	ochrana	k1gFnSc2	ochrana
lidské	lidský	k2eAgFnSc2d1	lidská
imunity	imunita	k1gFnSc2	imunita
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
2011	[number]	k4	2011
Bruce	Bruec	k1gInSc2	Bruec
A.	A.	kA	A.
Beutler	Beutler	k1gMnSc1	Beutler
a	a	k8xC	a
Jules	Jules	k1gMnSc1	Jules
A.	A.	kA	A.
Hoffmann	Hoffmann	k1gMnSc1	Hoffmann
"	"	kIx"	"
<g/>
za	za	k7c4	za
jejich	jejich	k3xOp3gInPc4	jejich
objevy	objev	k1gInPc4	objev
týkající	týkající	k2eAgInPc4d1	týkající
se	se	k3xPyFc4	se
aktivace	aktivace	k1gFnSc1	aktivace
vrozené	vrozený	k2eAgFnSc2d1	vrozená
imunity	imunita	k1gFnSc2	imunita
<g/>
"	"	kIx"	"
a	a	k8xC	a
Ralph	Ralph	k1gMnSc1	Ralph
M.	M.	kA	M.
Steinman	Steinman	k1gMnSc1	Steinman
"	"	kIx"	"
<g/>
za	za	k7c4	za
jeho	jeho	k3xOp3gInSc4	jeho
objev	objev	k1gInSc4	objev
dendritické	dendritický	k2eAgFnSc2d1	dendritická
buňky	buňka	k1gFnSc2	buňka
a	a	k8xC	a
její	její	k3xOp3gFnSc2	její
role	role	k1gFnSc2	role
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
získané	získaný	k2eAgFnSc2d1	získaná
imunity	imunita	k1gFnSc2	imunita
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
imunologie	imunologie	k1gFnSc2	imunologie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
imunologie	imunologie	k1gFnSc2	imunologie
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Imunologie	imunologie	k1gFnSc1	imunologie
</s>
</p>
