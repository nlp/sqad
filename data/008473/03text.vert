<p>
<s>
Brian	Brian	k1gInSc1	Brian
Houghton	Houghton	k1gInSc1	Houghton
Hodgson	Hodgson	k1gInSc1	Hodgson
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1800	[number]	k4	1800
–	–	k?	–
23	[number]	k4	23
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1894	[number]	k4	1894
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
anglický	anglický	k2eAgMnSc1d1	anglický
zoolog	zoolog	k1gMnSc1	zoolog
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
působící	působící	k2eAgMnSc1d1	působící
v	v	k7c6	v
Britské	britský	k2eAgFnSc6d1	britská
Indii	Indie	k1gFnSc6	Indie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biografie	biografie	k1gFnSc2	biografie
==	==	k?	==
</s>
</p>
<p>
<s>
Hodgson	Hodgson	k1gMnSc1	Hodgson
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1800	[number]	k4	1800
v	v	k7c4	v
Prestbury	Prestbur	k1gInPc4	Prestbur
v	v	k7c6	v
Cheshireu	Cheshireus	k1gInSc6	Cheshireus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
osmnácti	osmnáct	k4xCc6	osmnáct
letech	léto	k1gNnPc6	léto
odcestoval	odcestovat	k5eAaPmAgMnS	odcestovat
do	do	k7c2	do
Indie	Indie	k1gFnSc2	Indie
jako	jako	k8xC	jako
spisovatel	spisovatel	k1gMnSc1	spisovatel
Britské	britský	k2eAgFnSc2d1	britská
východoindické	východoindický	k2eAgFnSc2d1	Východoindická
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1819	[number]	k4	1819
byl	být	k5eAaImAgMnS	být
poslán	poslat	k5eAaPmNgMnS	poslat
do	do	k7c2	do
Káthmandú	Káthmandú	k1gFnSc2	Káthmandú
v	v	k7c6	v
Nepálu	Nepál	k1gInSc6	Nepál
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
studoval	studovat	k5eAaImAgMnS	studovat
místní	místní	k2eAgMnPc4d1	místní
obyvatele	obyvatel	k1gMnPc4	obyvatel
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInPc1	jejich
zvyky	zvyk	k1gInPc1	zvyk
<g/>
,	,	kIx,	,
literaturu	literatura	k1gFnSc4	literatura
i	i	k8xC	i
náboženství	náboženství	k1gNnSc4	náboženství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
pobytu	pobyt	k1gInSc2	pobyt
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
silným	silný	k2eAgMnSc7d1	silný
oponentem	oponent	k1gMnSc7	oponent
Thomase	Thomas	k1gMnSc2	Thomas
Macaulaye	Macaulay	k1gFnSc2	Macaulay
a	a	k8xC	a
podpůrcem	podpůrce	k1gMnSc7	podpůrce
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
v	v	k7c6	v
místním	místní	k2eAgInSc6d1	místní
jazyce	jazyk	k1gInSc6	jazyk
a	a	k8xC	a
odpůrcem	odpůrce	k1gMnSc7	odpůrce
výuky	výuka	k1gFnSc2	výuka
podávané	podávaný	k2eAgInPc4d1	podávaný
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Začal	začít	k5eAaPmAgMnS	začít
zde	zde	k6eAd1	zde
zkoumat	zkoumat	k5eAaImF	zkoumat
i	i	k9	i
místní	místní	k2eAgFnSc4d1	místní
faunu	fauna	k1gFnSc4	fauna
<g/>
.	.	kIx.	.
</s>
<s>
Pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
něj	on	k3xPp3gInSc2	on
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
nejrůznějších	různý	k2eAgFnPc2d3	nejrůznější
nových	nový	k2eAgFnPc2d1	nová
informací	informace	k1gFnPc2	informace
o	o	k7c6	o
místním	místní	k2eAgNnSc6d1	místní
ptactvu	ptactvo	k1gNnSc6	ptactvo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
savcích	savec	k1gMnPc6	savec
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
později	pozdě	k6eAd2	pozdě
daroval	darovat	k5eAaPmAgMnS	darovat
Britskému	britský	k2eAgNnSc3d1	Britské
muzeu	muzeum	k1gNnSc3	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Objevil	objevit	k5eAaPmAgInS	objevit
např.	např.	kA	např.
nový	nový	k2eAgInSc4d1	nový
druh	druh	k1gInSc4	druh
antilopy	antilopa	k1gFnSc2	antilopa
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
latinským	latinský	k2eAgInSc7d1	latinský
názvem	název	k1gInSc7	název
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
po	po	k7c6	po
něm	on	k3xPp3gMnSc6	on
<g/>
,	,	kIx,	,
gou	gou	k?	gou
(	(	kIx(	(
<g/>
Pantholops	Pantholops	k1gInSc4	Pantholops
hongsonii	hongsonie	k1gFnSc4	hongsonie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Objevil	objevit	k5eAaPmAgInS	objevit
dále	daleko	k6eAd2	daleko
více	hodně	k6eAd2	hodně
jak	jak	k8xS	jak
39	[number]	k4	39
druhů	druh	k1gInPc2	druh
savců	savec	k1gMnPc2	savec
a	a	k8xC	a
124	[number]	k4	124
druhů	druh	k1gInPc2	druh
ptáků	pták	k1gMnPc2	pták
<g/>
;	;	kIx,	;
u	u	k7c2	u
dalších	další	k2eAgInPc2d1	další
79	[number]	k4	79
ptačích	ptačí	k2eAgInPc2d1	ptačí
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
byly	být	k5eAaImAgFnP	být
známy	znám	k2eAgInPc1d1	znám
již	již	k9	již
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
nashromáždil	nashromáždit	k5eAaPmAgMnS	nashromáždit
několik	několik	k4yIc4	několik
nových	nový	k2eAgInPc2d1	nový
poznatků	poznatek	k1gInPc2	poznatek
a	a	k8xC	a
informací	informace	k1gFnPc2	informace
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Britského	britský	k2eAgNnSc2d1	Britské
muzea	muzeum	k1gNnSc2	muzeum
přivezl	přivézt	k5eAaPmAgMnS	přivézt
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1843	[number]	k4	1843
až	až	k8xS	až
1858	[number]	k4	1858
více	hodně	k6eAd2	hodně
jak	jak	k8xC	jak
10	[number]	k4	10
499	[number]	k4	499
nových	nový	k2eAgInPc2d1	nový
podkladů	podklad	k1gInPc2	podklad
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
se	se	k3xPyFc4	se
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
muzeu	muzeum	k1gNnSc6	muzeum
dříve	dříve	k6eAd2	dříve
nalézalo	nalézat	k5eAaImAgNnS	nalézat
i	i	k9	i
několik	několik	k4yIc4	několik
ilustrací	ilustrace	k1gFnPc2	ilustrace
k	k	k7c3	k
daným	daný	k2eAgMnPc3d1	daný
živočichům	živočich	k1gMnPc3	živočich
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
tvořili	tvořit	k5eAaImAgMnP	tvořit
nadaní	nadaný	k2eAgMnPc1d1	nadaný
umělci	umělec	k1gMnPc1	umělec
pod	pod	k7c7	pod
Hodgonovým	Hodgonův	k2eAgInSc7d1	Hodgonův
dozorem	dozor	k1gInSc7	dozor
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
však	však	k9	však
byla	být	k5eAaImAgFnS	být
později	pozdě	k6eAd2	pozdě
převezena	převézt	k5eAaPmNgFnS	převézt
do	do	k7c2	do
Londýnské	londýnský	k2eAgFnSc2d1	londýnská
zoologické	zoologický	k2eAgFnSc2d1	zoologická
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1844	[number]	k4	1844
od	od	k7c2	od
společnosti	společnost	k1gFnSc2	společnost
odstoupil	odstoupit	k5eAaPmAgInS	odstoupit
a	a	k8xC	a
na	na	k7c4	na
krátko	krátko	k6eAd1	krátko
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1845	[number]	k4	1845
se	se	k3xPyFc4	se
usadil	usadit	k5eAaPmAgInS	usadit
v	v	k7c6	v
Bengálsku	Bengálsko	k1gNnSc6	Bengálsko
v	v	k7c6	v
Darjeelingu	Darjeeling	k1gInSc6	Darjeeling
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
stále	stále	k6eAd1	stále
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
výzkumech	výzkum	k1gInPc6	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1858	[number]	k4	1858
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
a	a	k8xC	a
usadil	usadit	k5eAaPmAgInS	usadit
se	se	k3xPyFc4	se
v	v	k7c6	v
Cotswoldsu	Cotswolds	k1gInSc6	Cotswolds
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
23	[number]	k4	23
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1894	[number]	k4	1894
v	v	k7c6	v
Alderleyi	Alderley	k1gFnSc6	Alderley
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Brian	Briana	k1gFnPc2	Briana
Houghton	Houghton	k1gInSc1	Houghton
Hodgson	Hodgson	k1gMnSc1	Hodgson
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Brian	Brian	k1gInSc4	Brian
Houghton	Houghton	k1gInSc4	Houghton
Hodgson	Hodgsona	k1gFnPc2	Hodgsona
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Hodgson	Hodgsona	k1gFnPc2	Hodgsona
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
