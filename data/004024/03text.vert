<s>
Molekula	molekula	k1gFnSc1	molekula
je	být	k5eAaImIp3nS	být
částice	částice	k1gFnSc1	částice
složená	složený	k2eAgFnSc1d1	složená
z	z	k7c2	z
atomů	atom	k1gInPc2	atom
nebo	nebo	k8xC	nebo
iontů	ion	k1gInPc2	ion
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
důležitým	důležitý	k2eAgInSc7d1	důležitý
pojmem	pojem	k1gInSc7	pojem
chemie	chemie	k1gFnSc2	chemie
a	a	k8xC	a
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
už	už	k9	už
název	název	k1gInSc1	název
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
ústředním	ústřední	k2eAgInSc7d1	ústřední
pojmem	pojem	k1gInSc7	pojem
molekulové	molekulový	k2eAgFnSc2d1	molekulová
fyziky	fyzika	k1gFnSc2	fyzika
a	a	k8xC	a
kinetické	kinetický	k2eAgFnSc2d1	kinetická
teorie	teorie	k1gFnSc2	teorie
plynů	plyn	k1gInPc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
částici	částice	k1gFnSc4	částice
<g/>
,	,	kIx,	,
představující	představující	k2eAgFnSc4d1	představující
minimální	minimální	k2eAgNnSc4d1	minimální
množství	množství	k1gNnSc4	množství
dané	daný	k2eAgFnSc2d1	daná
látky	látka	k1gFnSc2	látka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
do	do	k7c2	do
chemické	chemický	k2eAgFnSc2d1	chemická
reakce	reakce	k1gFnSc2	reakce
a	a	k8xC	a
určuje	určovat	k5eAaImIp3nS	určovat
zároveň	zároveň	k6eAd1	zároveň
fyzikální	fyzikální	k2eAgFnPc4d1	fyzikální
vlastnosti	vlastnost	k1gFnPc4	vlastnost
látky	látka	k1gFnSc2	látka
obsažené	obsažený	k2eAgFnSc2d1	obsažená
v	v	k7c6	v
částici	částice	k1gFnSc6	částice
<g/>
.	.	kIx.	.
</s>
<s>
Molekuly	molekula	k1gFnPc4	molekula
jsou	být	k5eAaImIp3nP	být
zpravidla	zpravidla	k6eAd1	zpravidla
tvořeny	tvořit	k5eAaImNgInP	tvořit
více	hodně	k6eAd2	hodně
vázanými	vázaný	k2eAgInPc7d1	vázaný
atomy	atom	k1gInPc7	atom
(	(	kIx(	(
<g/>
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
více	hodně	k6eAd2	hodně
atomových	atomový	k2eAgNnPc2d1	atomové
jader	jádro	k1gNnPc2	jádro
a	a	k8xC	a
sdílený	sdílený	k2eAgInSc4d1	sdílený
atomový	atomový	k2eAgInSc4d1	atomový
obal	obal	k1gInSc4	obal
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
je	být	k5eAaImIp3nS	být
zprostředkována	zprostředkován	k2eAgFnSc1d1	zprostředkována
jejich	jejich	k3xOp3gFnSc1	jejich
vazba	vazba	k1gFnSc1	vazba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
však	však	k9	však
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
i	i	k9	i
jednoatomové	jednoatomový	k2eAgFnSc2d1	jednoatomový
<g/>
,	,	kIx,	,
např.	např.	kA	např.
u	u	k7c2	u
vzácných	vzácný	k2eAgInPc2d1	vzácný
plynů	plyn	k1gInPc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
buď	buď	k8xC	buď
elektricky	elektricky	k6eAd1	elektricky
neutrální	neutrální	k2eAgFnSc1d1	neutrální
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
mohou	moct	k5eAaImIp3nP	moct
nést	nést	k5eAaImF	nést
kladný	kladný	k2eAgInSc4d1	kladný
nebo	nebo	k8xC	nebo
záporný	záporný	k2eAgInSc4d1	záporný
náboj	náboj	k1gInSc4	náboj
a	a	k8xC	a
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
o	o	k7c4	o
ionty	ion	k1gInPc4	ion
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
polarity	polarita	k1gFnSc2	polarita
náboje	náboj	k1gInSc2	náboj
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
molekulových	molekulový	k2eAgInPc6d1	molekulový
kationtech	kation	k1gInPc6	kation
nebo	nebo	k8xC	nebo
molekulových	molekulový	k2eAgInPc6d1	molekulový
aniontech	anion	k1gInPc6	anion
<g/>
.	.	kIx.	.
</s>
<s>
Molekula	molekula	k1gFnSc1	molekula
představuje	představovat	k5eAaImIp3nS	představovat
kvantum	kvantum	k1gNnSc1	kvantum
prvků	prvek	k1gInPc2	prvek
i	i	k8xC	i
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
může	moct	k5eAaImIp3nS	moct
samostatně	samostatně	k6eAd1	samostatně
existovat	existovat	k5eAaImF	existovat
v	v	k7c6	v
plynném	plynný	k2eAgInSc6d1	plynný
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kondenzovaném	kondenzovaný	k2eAgInSc6d1	kondenzovaný
stavu	stav	k1gInSc6	stav
látek	látka	k1gFnPc2	látka
molekula	molekula	k1gFnSc1	molekula
jako	jako	k8xS	jako
částice	částice	k1gFnSc1	částice
často	často	k6eAd1	často
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
svůj	svůj	k3xOyFgInSc4	svůj
smysl	smysl	k1gInSc4	smysl
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
rozsáhlejší	rozsáhlý	k2eAgFnSc2d2	rozsáhlejší
struktury	struktura	k1gFnSc2	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
mnoho	mnoho	k4c1	mnoho
kapalin	kapalina	k1gFnPc2	kapalina
ještě	ještě	k6eAd1	ještě
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
slabě	slabě	k6eAd1	slabě
vázané	vázaný	k2eAgFnPc4d1	vázaná
molekuly	molekula	k1gFnPc4	molekula
<g/>
,	,	kIx,	,
u	u	k7c2	u
pevných	pevný	k2eAgFnPc2d1	pevná
látek	látka	k1gFnPc2	látka
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
možné	možný	k2eAgNnSc1d1	možné
pouze	pouze	k6eAd1	pouze
u	u	k7c2	u
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
struktura	struktura	k1gFnSc1	struktura
(	(	kIx(	(
<g/>
krystalická	krystalický	k2eAgFnSc1d1	krystalická
<g/>
,	,	kIx,	,
kvazikrystalická	kvazikrystalický	k2eAgFnSc1d1	kvazikrystalický
či	či	k8xC	či
amorfní	amorfní	k2eAgFnSc1d1	amorfní
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
držena	držet	k5eAaImNgFnS	držet
namísto	namísto	k7c2	namísto
kovalentních	kovalentní	k2eAgInPc2d1	kovalentní
<g/>
,	,	kIx,	,
koordinačně	koordinačně	k6eAd1	koordinačně
kovalentních	kovalentní	k2eAgFnPc2d1	kovalentní
<g/>
,	,	kIx,	,
iontových	iontový	k2eAgFnPc2d1	iontová
či	či	k8xC	či
kovových	kovový	k2eAgFnPc2d1	kovová
vazeb	vazba	k1gFnPc2	vazba
pouze	pouze	k6eAd1	pouze
relativně	relativně	k6eAd1	relativně
slabými	slabý	k2eAgFnPc7d1	slabá
mezimolekulovými	mezimolekulův	k2eAgFnPc7d1	mezimolekulův
interakcemi	interakce	k1gFnPc7	interakce
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
objevitele	objevitel	k1gMnSc2	objevitel
molekuly	molekula	k1gFnSc2	molekula
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
Amedeo	Amedeo	k6eAd1	Amedeo
Avogadro	Avogadra	k1gFnSc5	Avogadra
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
r.	r.	kA	r.
1811	[number]	k4	1811
publikoval	publikovat	k5eAaBmAgMnS	publikovat
výsledky	výsledek	k1gInPc1	výsledek
své	svůj	k3xOyFgFnPc4	svůj
práce	práce	k1gFnPc4	práce
<g/>
,	,	kIx,	,
kterými	který	k3yIgFnPc7	který
vysvětlil	vysvětlit	k5eAaPmAgMnS	vysvětlit
rozpor	rozpor	k1gInSc4	rozpor
Daltonova	Daltonův	k2eAgNnSc2d1	Daltonovo
atomového	atomový	k2eAgNnSc2d1	atomové
vysvětlení	vysvětlení	k1gNnSc2	vysvětlení
hmotnostních	hmotnostní	k2eAgInPc2d1	hmotnostní
slučovacích	slučovací	k2eAgInPc2d1	slučovací
poměrů	poměr	k1gInPc2	poměr
a	a	k8xC	a
Gay-Lussacem	Gay-Lussace	k1gNnSc7	Gay-Lussace
zjištěných	zjištěný	k2eAgInPc2d1	zjištěný
objemových	objemový	k2eAgInPc2d1	objemový
slučovacích	slučovací	k2eAgInPc2d1	slučovací
poměrů	poměr	k1gInPc2	poměr
a	a	k8xC	a
formuloval	formulovat	k5eAaImAgMnS	formulovat
i	i	k9	i
tvrzení	tvrzení	k1gNnSc4	tvrzení
zvané	zvaný	k2eAgNnSc4d1	zvané
dnes	dnes	k6eAd1	dnes
Avogadrův	Avogadrův	k2eAgInSc4d1	Avogadrův
zákon	zákon	k1gInSc4	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
plný	plný	k2eAgInSc4d1	plný
průkaz	průkaz	k1gInSc4	průkaz
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
až	až	k9	až
o	o	k7c4	o
plných	plný	k2eAgNnPc2d1	plné
100	[number]	k4	100
let	léto	k1gNnPc2	léto
mladší	mladý	k2eAgFnSc1d2	mladší
syntéza	syntéza	k1gFnSc1	syntéza
výsledků	výsledek	k1gInPc2	výsledek
experimentálního	experimentální	k2eAgNnSc2d1	experimentální
zkoumání	zkoumání	k1gNnSc2	zkoumání
hustoty	hustota	k1gFnSc2	hustota
a	a	k8xC	a
viskozity	viskozita	k1gFnSc2	viskozita
kapalin	kapalina	k1gFnPc2	kapalina
<g/>
,	,	kIx,	,
kinetické	kinetický	k2eAgFnPc1d1	kinetická
teorie	teorie	k1gFnPc1	teorie
a	a	k8xC	a
teorie	teorie	k1gFnSc1	teorie
Brownova	Brownův	k2eAgInSc2d1	Brownův
pohybu	pohyb	k1gInSc2	pohyb
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
prezentoval	prezentovat	k5eAaBmAgMnS	prezentovat
Jean	Jean	k1gMnSc1	Jean
Perrin	Perrin	k1gInSc4	Perrin
na	na	k7c6	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Solvayově	Solvayově	k6eAd1	Solvayově
kongresu	kongres	k1gInSc6	kongres
konaném	konaný	k2eAgInSc6d1	konaný
v	v	k7c6	v
r.	r.	kA	r.
2011	[number]	k4	2011
v	v	k7c6	v
Bruselu	Brusel	k1gInSc6	Brusel
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
molekulové	molekulový	k2eAgFnSc2d1	molekulová
fyziky	fyzika	k1gFnSc2	fyzika
vymezuje	vymezovat	k5eAaImIp3nS	vymezovat
moderní	moderní	k2eAgFnSc2d1	moderní
fyzikální	fyzikální	k2eAgFnSc2d1	fyzikální
chemie	chemie	k1gFnSc2	chemie
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
publikacích	publikace	k1gFnPc6	publikace
pojem	pojem	k1gInSc4	pojem
molekuly	molekula	k1gFnSc2	molekula
přísnější	přísný	k2eAgFnSc7d2	přísnější
definicí	definice	k1gFnSc7	definice
<g/>
,	,	kIx,	,
vylučující	vylučující	k2eAgFnSc2d1	vylučující
jednoatomové	jednoatomový	k2eAgFnSc2d1	jednoatomový
molekuly	molekula	k1gFnSc2	molekula
nebo	nebo	k8xC	nebo
molekulové	molekulový	k2eAgInPc4d1	molekulový
ionty	ion	k1gInPc4	ion
a	a	k8xC	a
zahrnující	zahrnující	k2eAgInSc4d1	zahrnující
i	i	k9	i
požadavek	požadavek	k1gInSc4	požadavek
na	na	k7c4	na
sílu	síla	k1gFnSc4	síla
vazby	vazba	k1gFnSc2	vazba
atomů	atom	k1gInPc2	atom
v	v	k7c6	v
molekule	molekula	k1gFnSc6	molekula
<g/>
:	:	kIx,	:
Molekula	molekula	k1gFnSc1	molekula
je	být	k5eAaImIp3nS	být
elektricky	elektricky	k6eAd1	elektricky
neutrální	neutrální	k2eAgFnSc1d1	neutrální
entita	entita	k1gFnSc1	entita
sestávající	sestávající	k2eAgFnSc1d1	sestávající
z	z	k7c2	z
více	hodně	k6eAd2	hodně
než	než	k8xS	než
jednoho	jeden	k4xCgInSc2	jeden
atomu	atom	k1gInSc2	atom
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
hladina	hladina	k1gFnSc1	hladina
jejího	její	k3xOp3gInSc2	její
vazbového	vazbový	k2eAgInSc2d1	vazbový
potenciálu	potenciál	k1gInSc2	potenciál
musí	muset	k5eAaImIp3nS	muset
vykazovat	vykazovat	k5eAaImF	vykazovat
snížení	snížení	k1gNnSc1	snížení
umožňující	umožňující	k2eAgNnSc1d1	umožňující
obsáhnout	obsáhnout	k5eAaPmF	obsáhnout
alespoň	alespoň	k9	alespoň
jeden	jeden	k4xCgInSc4	jeden
vibrační	vibrační	k2eAgInSc4d1	vibrační
stav	stav	k1gInSc4	stav
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
atomů	atom	k1gInPc2	atom
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
je	být	k5eAaImIp3nS	být
molekula	molekula	k1gFnSc1	molekula
složena	složen	k2eAgFnSc1d1	složena
se	se	k3xPyFc4	se
molekuly	molekula	k1gFnSc2	molekula
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
homonukleární	homonukleární	k2eAgFnPc4d1	homonukleární
–	–	k?	–
Molekuly	molekula	k1gFnPc4	molekula
obsahující	obsahující	k2eAgFnPc1d1	obsahující
pouze	pouze	k6eAd1	pouze
atomy	atom	k1gInPc4	atom
stejného	stejný	k2eAgInSc2d1	stejný
prvku	prvek	k1gInSc2	prvek
(	(	kIx(	(
<g/>
např.	např.	kA	např.
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
O	o	k7c6	o
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
heteronukleární	heteronukleárnit	k5eAaPmIp3nS	heteronukleárnit
–	–	k?	–
Molekuly	molekula	k1gFnSc2	molekula
skládající	skládající	k2eAgFnSc2d1	skládající
se	se	k3xPyFc4	se
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
druhů	druh	k1gInPc2	druh
atomů	atom	k1gInPc2	atom
(	(	kIx(	(
<g/>
např.	např.	kA	např.
LiH	LiH	k1gFnSc2	LiH
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Molekula	molekula	k1gFnSc1	molekula
prvku	prvek	k1gInSc2	prvek
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
atomy	atom	k1gInPc7	atom
jednoho	jeden	k4xCgInSc2	jeden
druhu	druh	k1gInSc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Molekula	molekula	k1gFnSc1	molekula
sloučeniny	sloučenina	k1gFnSc2	sloučenina
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
atomy	atom	k1gInPc4	atom
různých	různý	k2eAgInPc2d1	různý
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
energie	energie	k1gFnSc1	energie
systému	systém	k1gInSc2	systém
částic	částice	k1gFnPc2	částice
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vyšší	vysoký	k2eAgFnSc1d2	vyšší
nebo	nebo	k8xC	nebo
nižší	nízký	k2eAgFnSc1d2	nižší
než	než	k8xS	než
energie	energie	k1gFnSc1	energie
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
neinteragujících	interagující	k2eNgFnPc2d1	interagující
částic	částice	k1gFnPc2	částice
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
ovlivňováno	ovlivňovat	k5eAaImNgNnS	ovlivňovat
vzájemným	vzájemný	k2eAgNnSc7d1	vzájemné
působením	působení	k1gNnSc7	působení
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgFnPc7d1	jednotlivá
částicemi	částice	k1gFnPc7	částice
(	(	kIx(	(
<g/>
popř.	popř.	kA	popř.
skupinami	skupina	k1gFnPc7	skupina
částic	částice	k1gFnPc2	částice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
interakce	interakce	k1gFnPc1	interakce
mezi	mezi	k7c7	mezi
částicemi	částice	k1gFnPc7	částice
snižuje	snižovat	k5eAaImIp3nS	snižovat
celkovou	celkový	k2eAgFnSc4d1	celková
energii	energie	k1gFnSc4	energie
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
vzájemnému	vzájemný	k2eAgNnSc3d1	vzájemné
přitahování	přitahování	k1gNnSc3	přitahování
částic	částice	k1gFnPc2	částice
<g/>
,	,	kIx,	,
a	a	k8xC	a
ty	ten	k3xDgMnPc4	ten
mohou	moct	k5eAaImIp3nP	moct
vytvořit	vytvořit	k5eAaPmF	vytvořit
stabilní	stabilní	k2eAgInSc4d1	stabilní
systém	systém	k1gInSc4	systém
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
interakce	interakce	k1gFnPc1	interakce
mezi	mezi	k7c7	mezi
částicemi	částice	k1gFnPc7	částice
celkovou	celkový	k2eAgFnSc4d1	celková
energii	energie	k1gFnSc4	energie
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
částice	částice	k1gFnPc1	částice
odpuzovány	odpuzován	k2eAgFnPc1d1	odpuzován
<g/>
,	,	kIx,	,
a	a	k8xC	a
nemohou	moct	k5eNaImIp3nP	moct
vytvořit	vytvořit	k5eAaPmF	vytvořit
stabilní	stabilní	k2eAgInSc4d1	stabilní
systém	systém	k1gInSc4	systém
<g/>
.	.	kIx.	.
</s>
<s>
Molekula	molekula	k1gFnSc1	molekula
tedy	tedy	k9	tedy
může	moct	k5eAaImIp3nS	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
pouze	pouze	k6eAd1	pouze
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
atomy	atom	k1gInPc1	atom
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
skládat	skládat	k5eAaImF	skládat
<g/>
,	,	kIx,	,
vzájemně	vzájemně	k6eAd1	vzájemně
přitahovány	přitahován	k2eAgInPc1d1	přitahován
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
celková	celkový	k2eAgFnSc1d1	celková
energie	energie	k1gFnSc1	energie
molekuly	molekula	k1gFnSc2	molekula
je	být	k5eAaImIp3nS	být
nižší	nízký	k2eAgFnSc1d2	nižší
než	než	k8xS	než
energie	energie	k1gFnSc1	energie
samostatných	samostatný	k2eAgInPc2d1	samostatný
atomů	atom	k1gInPc2	atom
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
části	část	k1gFnPc1	část
molekuly	molekula	k1gFnSc2	molekula
(	(	kIx(	(
<g/>
tedy	tedy	k8xC	tedy
atomy	atom	k1gInPc1	atom
<g/>
)	)	kIx)	)
drží	držet	k5eAaImIp3nP	držet
pohromadě	pohromadě	k6eAd1	pohromadě
síly	síla	k1gFnPc1	síla
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
nazýváme	nazývat	k5eAaImIp1nP	nazývat
chemické	chemický	k2eAgFnPc1d1	chemická
vazby	vazba	k1gFnPc1	vazba
<g/>
.	.	kIx.	.
</s>
<s>
Chemická	chemický	k2eAgFnSc1d1	chemická
vazba	vazba	k1gFnSc1	vazba
je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
elektrické	elektrický	k2eAgFnSc6d1	elektrická
interakci	interakce	k1gFnSc6	interakce
nabitých	nabitý	k2eAgFnPc2d1	nabitá
částic	částice	k1gFnPc2	částice
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
atomy	atom	k1gInPc1	atom
<g/>
.	.	kIx.	.
</s>
<s>
Síly	síl	k1gInPc4	síl
působící	působící	k2eAgInPc4d1	působící
mezi	mezi	k7c7	mezi
atomy	atom	k1gInPc7	atom
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k9	jako
výměnné	výměnný	k2eAgFnPc4d1	výměnná
síly	síla	k1gFnPc4	síla
<g/>
.	.	kIx.	.
</s>
<s>
Výměnné	výměnný	k2eAgFnPc1d1	výměnná
síly	síla	k1gFnPc1	síla
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
popsat	popsat	k5eAaPmF	popsat
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
vlastností	vlastnost	k1gFnPc2	vlastnost
molekul	molekula	k1gFnPc2	molekula
<g/>
.	.	kIx.	.
</s>
<s>
Chemickou	chemický	k2eAgFnSc4d1	chemická
vazbu	vazba	k1gFnSc4	vazba
můžeme	moct	k5eAaImIp1nP	moct
definovat	definovat	k5eAaBmF	definovat
podle	podle	k7c2	podle
rozdílu	rozdíl	k1gInSc2	rozdíl
elektronegativit	elektronegativita	k1gFnPc2	elektronegativita
iontů	ion	k1gInPc2	ion
jako	jako	k8xC	jako
kovovou	kovový	k2eAgFnSc4d1	kovová
<g/>
,	,	kIx,	,
kovalentní	kovalentní	k2eAgFnSc4d1	kovalentní
<g/>
,	,	kIx,	,
polární	polární	k2eAgFnSc4d1	polární
nebo	nebo	k8xC	nebo
iontovou	iontový	k2eAgFnSc4d1	iontová
<g/>
.	.	kIx.	.
</s>
<s>
Energie	energie	k1gFnSc1	energie
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
je	být	k5eAaImIp3nS	být
molekule	molekula	k1gFnSc3	molekula
nutno	nutno	k6eAd1	nutno
dodat	dodat	k5eAaPmF	dodat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
jejímu	její	k3xOp3gInSc3	její
rozpadu	rozpad	k1gInSc3	rozpad
na	na	k7c4	na
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
atomy	atom	k1gInPc4	atom
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
disociační	disociační	k2eAgFnSc1d1	disociační
energie	energie	k1gFnSc1	energie
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
jev	jev	k1gInSc1	jev
rozpadu	rozpad	k1gInSc2	rozpad
molekuly	molekula	k1gFnSc2	molekula
při	při	k7c6	při
dodání	dodání	k1gNnSc6	dodání
disociační	disociační	k2eAgFnSc2d1	disociační
energie	energie	k1gFnSc2	energie
pak	pak	k6eAd1	pak
nazýváme	nazývat	k5eAaImIp1nP	nazývat
disociací	disociace	k1gFnSc7	disociace
<g/>
.	.	kIx.	.
</s>
<s>
Molekuly	molekula	k1gFnPc1	molekula
jsou	být	k5eAaImIp3nP	být
základní	základní	k2eAgInPc4d1	základní
stavební	stavební	k2eAgInPc4d1	stavební
kameny	kámen	k1gInPc4	kámen
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgNnPc2	jenž
jsou	být	k5eAaImIp3nP	být
vybudována	vybudován	k2eAgNnPc1d1	vybudováno
hmotná	hmotný	k2eAgNnPc1d1	hmotné
tělesa	těleso	k1gNnPc1	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Různý	různý	k2eAgInSc1d1	různý
způsob	způsob	k1gInSc1	způsob
uspořádání	uspořádání	k1gNnSc2	uspořádání
a	a	k8xC	a
vzájemného	vzájemný	k2eAgNnSc2d1	vzájemné
silového	silový	k2eAgNnSc2d1	silové
působení	působení	k1gNnSc2	působení
základních	základní	k2eAgFnPc2d1	základní
částic	částice	k1gFnPc2	částice
v	v	k7c6	v
tělesech	těleso	k1gNnPc6	těleso
pak	pak	k6eAd1	pak
určuje	určovat	k5eAaImIp3nS	určovat
jejich	jejich	k3xOp3gFnPc4	jejich
různé	různý	k2eAgFnPc4d1	různá
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Editor	editor	k1gInSc1	editor
molekul	molekula	k1gFnPc2	molekula
Atom	atom	k1gInSc4	atom
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
molekula	molekula	k1gFnSc1	molekula
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
molekula	molekula	k1gFnSc1	molekula
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
