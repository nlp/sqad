<s>
Belgie	Belgie	k1gFnSc1	Belgie
má	mít	k5eAaImIp3nS	mít
vysokou	vysoký	k2eAgFnSc4d1	vysoká
hustotu	hustota	k1gFnSc4	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
(	(	kIx(	(
<g/>
365	[number]	k4	365
obyv	obyva	k1gFnPc2	obyva
<g/>
.	.	kIx.	.
na	na	k7c4	na
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
má	mít	k5eAaImIp3nS	mít
vyšší	vysoký	k2eAgFnSc1d2	vyšší
pouze	pouze	k6eAd1	pouze
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
a	a	k8xC	a
několik	několik	k4yIc1	několik
malých	malý	k2eAgInPc2d1	malý
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
Monako	Monako	k1gNnSc1	Monako
<g/>
.	.	kIx.	.
</s>
