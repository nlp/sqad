<p>
<s>
Kacap	Kacap	k1gMnSc1	Kacap
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
kacap	kacap	k1gInSc1	kacap
<g/>
,	,	kIx,	,
ukrajinsky	ukrajinsky	k6eAd1	ukrajinsky
к	к	k?	к
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hanlivé	hanlivý	k2eAgNnSc1d1	hanlivé
označení	označení	k1gNnSc1	označení
Rusů	Rus	k1gMnPc2	Rus
<g/>
,	,	kIx,	,
používané	používaný	k2eAgInPc1d1	používaný
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
a	a	k8xC	a
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
<g/>
.	.	kIx.	.
</s>
<s>
Etymologie	etymologie	k1gFnSc1	etymologie
označení	označení	k1gNnSc2	označení
je	být	k5eAaImIp3nS	být
nejednoznačná	jednoznačný	k2eNgFnSc1d1	nejednoznačná
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
pocházet	pocházet	k5eAaImF	pocházet
ze	z	k7c2	z
spojení	spojení	k1gNnSc2	spojení
jak	jak	k8xS	jak
cap	capa	k1gFnPc2	capa
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
[	[	kIx(	[
<g/>
vypadající	vypadající	k2eAgFnSc1d1	vypadající
<g/>
]	]	kIx)	]
jako	jako	k8xS	jako
kozel	kozel	k1gMnSc1	kozel
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vycházejícího	vycházející	k2eAgMnSc2d1	vycházející
z	z	k7c2	z
tradice	tradice	k1gFnSc2	tradice
nošení	nošení	k1gNnSc2	nošení
plnovousu	plnovous	k1gInSc2	plnovous
na	na	k7c6	na
středověké	středověký	k2eAgFnSc6d1	středověká
Rusi	Rus	k1gFnSc6	Rus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ze	z	k7c2	z
slova	slovo	k1gNnSc2	slovo
kacap	kacap	k1gMnSc1	kacap
pocházející	pocházející	k2eAgInSc1d1	pocházející
výraz	výraz	k1gInSc1	výraz
Kacapščina	Kacapščin	k2eAgMnSc2d1	Kacapščin
či	či	k8xC	či
Kacapstan	Kacapstan	k1gInSc1	Kacapstan
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
hanlivé	hanlivý	k2eAgNnSc1d1	hanlivé
označení	označení	k1gNnSc1	označení
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Moskal	Moskal	k?	Moskal
</s>
</p>
<p>
<s>
Chochol	Chochol	k1gMnSc1	Chochol
</s>
</p>
