<p>
<s>
Francouzština	francouzština	k1gFnSc1	francouzština
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
též	též	k9	též
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
franština	franština	k1gFnSc1	franština
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
románský	románský	k2eAgInSc1d1	románský
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
se	se	k3xPyFc4	se
hovoří	hovořit	k5eAaImIp3nS	hovořit
především	především	k9	především
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
Belgii	Belgie	k1gFnSc6	Belgie
<g/>
,	,	kIx,	,
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
,	,	kIx,	,
Kanadě	Kanada	k1gFnSc6	Kanada
a	a	k8xC	a
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
zemích	zem	k1gFnPc6	zem
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vznik	vznik	k1gInSc1	vznik
a	a	k8xC	a
vývoj	vývoj	k1gInSc1	vývoj
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Původ	původ	k1gInSc1	původ
===	===	k?	===
</s>
</p>
<p>
<s>
Prvopočátky	prvopočátek	k1gInPc1	prvopočátek
vývoje	vývoj	k1gInSc2	vývoj
francouzštiny	francouzština	k1gFnSc2	francouzština
jako	jako	k8xC	jako
románského	románský	k2eAgInSc2d1	románský
jazyka	jazyk	k1gInSc2	jazyk
jsou	být	k5eAaImIp3nP	být
spojovány	spojovat	k5eAaImNgFnP	spojovat
s	s	k7c7	s
postupným	postupný	k2eAgNnSc7d1	postupné
pronikáním	pronikání	k1gNnSc7	pronikání
římského	římský	k2eAgMnSc2d1	římský
vojevůdce	vojevůdce	k1gMnSc2	vojevůdce
Julia	Julius	k1gMnSc2	Julius
Caesara	Caesar	k1gMnSc2	Caesar
do	do	k7c2	do
Galie	Galie	k1gFnSc2	Galie
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
58	[number]	k4	58
až	až	k9	až
52	[number]	k4	52
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Římané	Říman	k1gMnPc1	Říman
s	s	k7c7	s
sebou	se	k3xPyFc7	se
přinesli	přinést	k5eAaPmAgMnP	přinést
svůj	svůj	k3xOyFgInSc4	svůj
talent	talent	k1gInSc4	talent
pro	pro	k7c4	pro
správu	správa	k1gFnSc4	správa
území	území	k1gNnSc2	území
a	a	k8xC	a
jako	jako	k8xS	jako
vítězové	vítěz	k1gMnPc1	vítěz
se	se	k3xPyFc4	se
ujali	ujmout	k5eAaPmAgMnP	ujmout
administrativy	administrativa	k1gFnSc2	administrativa
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
latina	latina	k1gFnSc1	latina
(	(	kIx(	(
<g/>
především	především	k9	především
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
mluvené	mluvený	k2eAgFnSc2d1	mluvená
vulgární	vulgární	k2eAgFnSc2d1	vulgární
latiny	latina	k1gFnSc2	latina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Původní	původní	k2eAgInSc1d1	původní
keltský	keltský	k2eAgInSc1d1	keltský
jazyk	jazyk	k1gInSc1	jazyk
Galů	Gal	k1gMnPc2	Gal
přežíval	přežívat	k5eAaImAgInS	přežívat
i	i	k9	i
nadále	nadále	k6eAd1	nadále
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
mluvené	mluvený	k2eAgFnSc6d1	mluvená
podobě	podoba	k1gFnSc6	podoba
na	na	k7c6	na
venkově	venkov	k1gInSc6	venkov
<g/>
,	,	kIx,	,
zčásti	zčásti	k6eAd1	zčásti
i	i	k9	i
v	v	k7c6	v
městských	městský	k2eAgFnPc6d1	městská
domácnostech	domácnost	k1gFnPc6	domácnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
proces	proces	k1gInSc1	proces
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
romanizace	romanizace	k1gFnSc1	romanizace
<g/>
,	,	kIx,	,
probíhal	probíhat	k5eAaImAgInS	probíhat
od	od	k7c2	od
jihu	jih	k1gInSc2	jih
země	zem	k1gFnSc2	zem
a	a	k8xC	a
rozdíly	rozdíl	k1gInPc4	rozdíl
v	v	k7c6	v
nářečích	nářečí	k1gNnPc6	nářečí
lze	lze	k6eAd1	lze
pozorovat	pozorovat	k5eAaImF	pozorovat
ještě	ještě	k9	ještě
dnes	dnes	k6eAd1	dnes
<g/>
.	.	kIx.	.
</s>
<s>
Tzv.	tzv.	kA	tzv.
provençal	provençal	k1gInSc1	provençal
<g/>
,	,	kIx,	,
nářečí	nářečí	k1gNnSc1	nářečí
kraje	kraj	k1gInSc2	kraj
Provence	Provence	k1gFnSc2	Provence
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
svou	svůj	k3xOyFgFnSc7	svůj
výslovností	výslovnost	k1gFnSc7	výslovnost
velmi	velmi	k6eAd1	velmi
podobné	podobný	k2eAgFnSc6d1	podobná
italštině	italština	k1gFnSc6	italština
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Období	období	k1gNnSc1	období
Germánů	Germán	k1gMnPc2	Germán
===	===	k?	===
</s>
</p>
<p>
<s>
Germáni	Germán	k1gMnPc1	Germán
pronikají	pronikat	k5eAaImIp3nP	pronikat
na	na	k7c4	na
galská	galský	k2eAgNnPc4d1	galské
území	území	k1gNnPc4	území
v	v	k7c6	v
pozdním	pozdní	k2eAgInSc6d1	pozdní
starověku	starověk	k1gInSc6	starověk
<g/>
.	.	kIx.	.
</s>
<s>
Vulgární	vulgární	k2eAgFnSc1d1	vulgární
latina	latina	k1gFnSc1	latina
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
vytrácí	vytrácet	k5eAaImIp3nS	vytrácet
a	a	k8xC	a
přežívá	přežívat	k5eAaImIp3nS	přežívat
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
latiny	latina	k1gFnSc2	latina
klasické	klasický	k2eAgFnSc2d1	klasická
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
od	od	k7c2	od
roku	rok	k1gInSc2	rok
313	[number]	k4	313
vyhlášením	vyhlášení	k1gNnSc7	vyhlášení
Ediktu	edikt	k1gInSc2	edikt
milánského	milánský	k2eAgInSc2d1	milánský
jediným	jediný	k2eAgInSc7d1	jediný
jazykem	jazyk	k1gInSc7	jazyk
bohoslužeb	bohoslužba	k1gFnPc2	bohoslužba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Země	země	k1gFnSc1	země
je	být	k5eAaImIp3nS	být
relativně	relativně	k6eAd1	relativně
roztříštěná	roztříštěný	k2eAgFnSc1d1	roztříštěná
a	a	k8xC	a
vyvíjejí	vyvíjet	k5eAaImIp3nP	vyvíjet
se	se	k3xPyFc4	se
místní	místní	k2eAgNnPc1d1	místní
nářečí	nářečí	k1gNnPc1	nářečí
(	(	kIx(	(
<g/>
patois	patois	k1gInSc1	patois
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
nejvýznamnější	významný	k2eAgFnSc1d3	nejvýznamnější
je	on	k3xPp3gNnSc4	on
nářečí	nářečí	k1gNnSc4	nářečí
'	'	kIx"	'
<g/>
Île-de-France	Îlee-France	k1gFnSc2	Île-de-France
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
utvářejí	utvářet	k5eAaImIp3nP	utvářet
tři	tři	k4xCgFnPc1	tři
hlavní	hlavní	k2eAgFnPc1d1	hlavní
jazykové	jazykový	k2eAgFnPc1d1	jazyková
oblasti	oblast	k1gFnPc1	oblast
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Nářečí	nářečí	k1gNnSc1	nářečí
severu	sever	k1gInSc2	sever
země	zem	k1gFnSc2	zem
–	–	k?	–
la	la	k1gNnSc1	la
langue	langue	k1gFnSc2	langue
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
oï	oï	k?	oï
<g/>
,	,	kIx,	,
označované	označovaný	k2eAgInPc1d1	označovaný
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
ano	ano	k9	ano
(	(	kIx(	(
<g/>
oui	oui	k?	oui
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
nářečí	nářečí	k1gNnSc6	nářečí
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
jako	jako	k9	jako
oï	oï	k?	oï
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nářečí	nářečí	k1gNnSc1	nářečí
jihu	jih	k1gInSc2	jih
země	zem	k1gFnSc2	zem
–	–	k?	–
la	la	k1gNnSc1	la
langue	langue	k1gFnSc2	langue
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
oc	oc	k?	oc
<g/>
,	,	kIx,	,
označované	označovaný	k2eAgInPc1d1	označovaný
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
ano	ano	k9	ano
(	(	kIx(	(
<g/>
oui	oui	k?	oui
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
nářečí	nářečí	k1gNnSc6	nářečí
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
jako	jako	k9	jako
oc	oc	k?	oc
<g/>
.	.	kIx.	.
</s>
<s>
Nepatří	patřit	k5eNaImIp3nS	patřit
vlastně	vlastně	k9	vlastně
do	do	k7c2	do
francouzštiny	francouzština	k1gFnSc2	francouzština
<g/>
,	,	kIx,	,
jazykověda	jazykověda	k1gFnSc1	jazykověda
je	být	k5eAaImIp3nS	být
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
samostatný	samostatný	k2eAgInSc4d1	samostatný
románský	románský	k2eAgInSc4d1	románský
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
okcitánštinu	okcitánština	k1gFnSc4	okcitánština
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nářečí	nářečí	k1gNnPc1	nářečí
frankoprovensálská	frankoprovensálský	k2eAgNnPc1d1	frankoprovensálský
-	-	kIx~	-
le	le	k?	le
francoprovençal	francoprovençal	k1gInSc1	francoprovençal
<g/>
,	,	kIx,	,
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
oblasti	oblast	k1gFnSc6	oblast
<g/>
,	,	kIx,	,
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
měst	město	k1gNnPc2	město
Lyon	Lyon	k1gInSc1	Lyon
<g/>
,	,	kIx,	,
Ženeva	Ženeva	k1gFnSc1	Ženeva
a	a	k8xC	a
Grenoble	Grenoble	k1gInSc1	Grenoble
<g/>
.	.	kIx.	.
</s>
<s>
Nepatří	patřit	k5eNaImIp3nS	patřit
vlastně	vlastně	k9	vlastně
do	do	k7c2	do
francouzštiny	francouzština	k1gFnSc2	francouzština
<g/>
,	,	kIx,	,
jazykověda	jazykověda	k1gFnSc1	jazykověda
je	být	k5eAaImIp3nS	být
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
samostatný	samostatný	k2eAgInSc4d1	samostatný
románský	románský	k2eAgInSc4d1	románský
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
frankoprovensálštinu	frankoprovensálština	k1gFnSc4	frankoprovensálština
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Vliv	vliv	k1gInSc1	vliv
germánských	germánský	k2eAgInPc2d1	germánský
jazyků	jazyk	k1gInPc2	jazyk
na	na	k7c4	na
francouzštinu	francouzština	k1gFnSc4	francouzština
====	====	k?	====
</s>
</p>
<p>
<s>
Vznik	vznik	k1gInSc1	vznik
tzv.	tzv.	kA	tzv.
h	h	k?	h
aspiré	aspirý	k2eAgFnSc2d1	aspirý
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
má	mít	k5eAaImIp3nS	mít
vliv	vliv	k1gInSc4	vliv
na	na	k7c6	na
vázání	vázání	k1gNnSc6	vázání
slov	slovo	k1gNnPc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
germánských	germánský	k2eAgInPc6d1	germánský
jazycích	jazyk	k1gInPc6	jazyk
se	se	k3xPyFc4	se
hláska	hláska	k1gFnSc1	hláska
'	'	kIx"	'
<g/>
h	h	k?	h
<g/>
'	'	kIx"	'
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
<g/>
,	,	kIx,	,
ve	v	k7c6	v
francouzštině	francouzština	k1gFnSc6	francouzština
tomu	ten	k3xDgNnSc3	ten
tak	tak	k6eAd1	tak
není	být	k5eNaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
slovech	slovo	k1gNnPc6	slovo
s	s	k7c7	s
'	'	kIx"	'
<g/>
h	h	k?	h
<g/>
'	'	kIx"	'
germánského	germánský	k2eAgInSc2d1	germánský
původu	původ	k1gInSc2	původ
nelze	lze	k6eNd1	lze
provádět	provádět	k5eAaImF	provádět
elizi	elize	k1gFnSc4	elize
(	(	kIx(	(
<g/>
např.	např.	kA	např.
le	le	k?	le
Havre	Havr	k1gInSc5	Havr
/	/	kIx~	/
<g/>
lə	lə	k?	lə
<g/>
/	/	kIx~	/
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
hôtel	hôtel	k1gMnSc1	hôtel
/	/	kIx~	/
<g/>
lɔ	lɔ	k?	lɔ
<g/>
/	/	kIx~	/
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
charakteristický	charakteristický	k2eAgInSc1d1	charakteristický
přízvuk	přízvuk	k1gInSc1	přízvuk
francouzštiny	francouzština	k1gFnSc2	francouzština
(	(	kIx(	(
<g/>
accent	accent	k1gInSc1	accent
pointu	pointa	k1gFnSc4	pointa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
na	na	k7c6	na
koncové	koncový	k2eAgFnSc6d1	koncová
slabice	slabika	k1gFnSc6	slabika
slova	slovo	k1gNnSc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
v	v	k7c6	v
germánských	germánský	k2eAgInPc6d1	germánský
jazycích	jazyk	k1gInPc6	jazyk
je	být	k5eAaImIp3nS	být
přízvuk	přízvuk	k1gInSc1	přízvuk
na	na	k7c6	na
první	první	k4xOgFnSc6	první
slabice	slabika	k1gFnSc6	slabika
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
redukci	redukce	k1gFnSc3	redukce
výslovnosti	výslovnost	k1gFnSc2	výslovnost
koncových	koncový	k2eAgFnPc2d1	koncová
slabik	slabika	k1gFnPc2	slabika
u	u	k7c2	u
původních	původní	k2eAgNnPc2d1	původní
latinských	latinský	k2eAgNnPc2d1	latinské
slov	slovo	k1gNnPc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
např.	např.	kA	např.
z	z	k7c2	z
latinského	latinský	k2eAgNnSc2d1	latinské
slova	slovo	k1gNnSc2	slovo
TĒ	TĒ	k1gMnPc2	TĒ
se	se	k3xPyFc4	se
vyvinulo	vyvinout	k5eAaPmAgNnS	vyvinout
francouzské	francouzský	k2eAgNnSc1d1	francouzské
'	'	kIx"	'
<g/>
toile	toile	k1gFnSc1	toile
<g/>
'	'	kIx"	'
/	/	kIx~	/
<g/>
twal	twal	k1gInSc1	twal
<g/>
/	/	kIx~	/
s	s	k7c7	s
tzv.	tzv.	kA	tzv.
němým	němý	k2eAgFnPc3d1	němá
e	e	k0	e
na	na	k7c6	na
konci	konec	k1gInSc6	konec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
románských	románský	k2eAgInPc6d1	románský
jazycích	jazyk	k1gInPc6	jazyk
je	být	k5eAaImIp3nS	být
přídavné	přídavný	k2eAgNnSc4d1	přídavné
jméno	jméno	k1gNnSc4	jméno
umístěno	umístit	k5eAaPmNgNnS	umístit
většinou	většinou	k6eAd1	většinou
za	za	k7c7	za
podstatným	podstatný	k2eAgNnSc7d1	podstatné
jménem	jméno	k1gNnSc7	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
vlivu	vliv	k1gInSc3	vliv
germánských	germánský	k2eAgInPc2d1	germánský
jazyků	jazyk	k1gInPc2	jazyk
lze	lze	k6eAd1	lze
některá	některý	k3yIgNnPc4	některý
krátká	krátké	k1gNnPc4	krátké
přídavná	přídavný	k2eAgNnPc4d1	přídavné
jména	jméno	k1gNnPc4	jméno
ve	v	k7c6	v
francouzštině	francouzština	k1gFnSc6	francouzština
nalézt	nalézt	k5eAaBmF	nalézt
před	před	k7c7	před
podstatným	podstatný	k2eAgNnSc7d1	podstatné
jménem	jméno	k1gNnSc7	jméno
(	(	kIx(	(
<g/>
např.	např.	kA	např.
un	un	k?	un
petit	petit	k1gInSc1	petit
garçon	garçon	k1gInSc1	garçon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Období	období	k1gNnSc1	období
šíření	šíření	k1gNnSc2	šíření
křesťanství	křesťanství	k1gNnSc2	křesťanství
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
496	[number]	k4	496
si	se	k3xPyFc3	se
franský	franský	k2eAgMnSc1d1	franský
král	král	k1gMnSc1	král
Chlodvík	Chlodvík	k1gMnSc1	Chlodvík
I.	I.	kA	I.
bere	brát	k5eAaImIp3nS	brát
za	za	k7c4	za
ženu	žena	k1gFnSc4	žena
burgundskou	burgundský	k2eAgFnSc4d1	burgundská
kněžnu	kněžna	k1gFnSc4	kněžna
Klotildu	Klotild	k1gInSc2	Klotild
a	a	k8xC	a
přebírá	přebírat	k5eAaImIp3nS	přebírat
i	i	k9	i
její	její	k3xOp3gFnSc4	její
katolickou	katolický	k2eAgFnSc4d1	katolická
víru	víra	k1gFnSc4	víra
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
katolické	katolický	k2eAgNnSc1d1	katolické
křesťanství	křesťanství	k1gNnSc1	křesťanství
stává	stávat	k5eAaImIp3nS	stávat
oficiálním	oficiální	k2eAgNnSc7d1	oficiální
náboženstvím	náboženství	k1gNnSc7	náboženství
Franské	franský	k2eAgFnSc2d1	Franská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Znovu	znovu	k6eAd1	znovu
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
vliv	vliv	k1gInSc1	vliv
latiny	latina	k1gFnSc2	latina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
do	do	k7c2	do
různých	různý	k2eAgNnPc2d1	různé
nářečí	nářečí	k1gNnPc2	nářečí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Veliký	veliký	k2eAgMnSc1d1	veliký
a	a	k8xC	a
počátky	počátek	k1gInPc1	počátek
francouzštiny	francouzština	k1gFnSc2	francouzština
</s>
</p>
<p>
<s>
Ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
období	období	k1gNnSc6	období
vlády	vláda	k1gFnSc2	vláda
Karla	Karel	k1gMnSc2	Karel
Velikého	veliký	k2eAgNnSc2d1	veliké
se	se	k3xPyFc4	se
nedá	dát	k5eNaPmIp3nS	dát
mluvit	mluvit	k5eAaImF	mluvit
o	o	k7c6	o
francouzštině	francouzština	k1gFnSc6	francouzština
jako	jako	k8xS	jako
takové	takový	k3xDgFnSc2	takový
<g/>
.	.	kIx.	.
</s>
<s>
Jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
se	se	k3xPyFc4	se
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
mluvilo	mluvit	k5eAaImAgNnS	mluvit
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
složen	složit	k5eAaPmNgInS	složit
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
nářečí	nářečí	k1gNnPc2	nářečí
a	a	k8xC	a
souhrnně	souhrnně	k6eAd1	souhrnně
jej	on	k3xPp3gInSc4	on
francouzština	francouzština	k1gFnSc1	francouzština
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
la	la	k1gNnSc1	la
langue	langue	k1gFnSc2	langue
romaine	romainout	k5eAaPmIp3nS	romainout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Veliký	veliký	k2eAgMnSc1d1	veliký
dbal	dbát	k5eAaImAgInS	dbát
na	na	k7c4	na
šíření	šíření	k1gNnSc4	šíření
kultury	kultura	k1gFnSc2	kultura
a	a	k8xC	a
miloval	milovat	k5eAaImAgInS	milovat
klasickou	klasický	k2eAgFnSc4d1	klasická
latinu	latina	k1gFnSc4	latina
<g/>
.	.	kIx.	.
</s>
<s>
Zakládal	zakládat	k5eAaImAgMnS	zakládat
školy	škola	k1gFnPc4	škola
jak	jak	k8xS	jak
pro	pro	k7c4	pro
obyčejný	obyčejný	k2eAgInSc4d1	obyčejný
lid	lid	k1gInSc4	lid
při	při	k7c6	při
každé	každý	k3xTgFnSc6	každý
farnosti	farnost	k1gFnSc6	farnost
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
školy	škola	k1gFnPc1	škola
pro	pro	k7c4	pro
vzdělance	vzdělanec	k1gMnSc4	vzdělanec
při	při	k7c6	při
každém	každý	k3xTgInSc6	každý
klášteře	klášter	k1gInSc6	klášter
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
vyučovalo	vyučovat	k5eAaImAgNnS	vyučovat
klasické	klasický	k2eAgFnSc3d1	klasická
latině	latina	k1gFnSc3	latina
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Tato	tento	k3xDgFnSc1	tento
tradice	tradice	k1gFnSc1	tradice
však	však	k9	však
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
a	a	k8xC	a
mnoho	mnoho	k4c4	mnoho
škol	škola	k1gFnPc2	škola
bylo	být	k5eAaImAgNnS	být
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
a	a	k8xC	a
opuštěno	opustit	k5eAaPmNgNnS	opustit
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Z	z	k7c2	z
Anglie	Anglie	k1gFnSc2	Anglie
si	se	k3xPyFc3	se
pozval	pozvat	k5eAaPmAgInS	pozvat
mnicha	mnich	k1gMnSc4	mnich
Alquina	Alquin	k1gMnSc4	Alquin
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
úkolem	úkol	k1gInSc7	úkol
bylo	být	k5eAaImAgNnS	být
postarat	postarat	k5eAaPmF	postarat
se	se	k3xPyFc4	se
o	o	k7c4	o
znovunalezení	znovunalezený	k2eAgMnPc1d1	znovunalezený
pravidel	pravidlo	k1gNnPc2	pravidlo
klasické	klasický	k2eAgFnSc2d1	klasická
latiny	latina	k1gFnSc2	latina
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
jediným	jediný	k2eAgInSc7d1	jediný
jazykem	jazyk	k1gInSc7	jazyk
vzdělanců	vzdělanec	k1gMnPc2	vzdělanec
a	a	k8xC	a
jediným	jediný	k2eAgInSc7d1	jediný
psaným	psaný	k2eAgInSc7d1	psaný
jazykem	jazyk	k1gInSc7	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tyto	tento	k3xDgInPc4	tento
účely	účel	k1gInPc4	účel
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
nové	nový	k2eAgNnSc1d1	nové
písmo	písmo	k1gNnSc1	písmo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
karolinská	karolinský	k2eAgFnSc1d1	Karolinská
minuskula	minuskula	k1gFnSc1	minuskula
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
karolinské	karolinský	k2eAgFnSc2d1	Karolinská
minuskuly	minuskula	k1gFnSc2	minuskula
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
později	pozdě	k6eAd2	pozdě
malá	malý	k2eAgNnPc4d1	malé
písmena	písmeno	k1gNnPc4	písmeno
abecedy	abeceda	k1gFnSc2	abeceda
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
používáme	používat	k5eAaImIp1nP	používat
dnes	dnes	k6eAd1	dnes
i	i	k8xC	i
my	my	k3xPp1nPc1	my
–	–	k?	–
nazývá	nazývat	k5eAaImIp3nS	nazývat
se	se	k3xPyFc4	se
latinka	latinka	k1gFnSc1	latinka
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgNnPc1d1	velké
písmena	písmeno	k1gNnPc1	písmeno
(	(	kIx(	(
<g/>
majuskule	majuskule	k1gFnSc1	majuskule
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
převzata	převzít	k5eAaPmNgFnS	převzít
z	z	k7c2	z
antických	antický	k2eAgInPc2d1	antický
nápisů	nápis	k1gInPc2	nápis
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Ludvíka	Ludvík	k1gMnSc2	Ludvík
Pobožného	pobožný	k2eAgMnSc2d1	pobožný
<g/>
,	,	kIx,	,
syna	syn	k1gMnSc2	syn
Karla	Karel	k1gMnSc2	Karel
Velikého	veliký	k2eAgMnSc2d1	veliký
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
země	země	k1gFnSc1	země
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
Ludvíkovými	Ludvíkův	k2eAgMnPc7d1	Ludvíkův
syny	syn	k1gMnPc7	syn
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
bojům	boj	k1gInPc3	boj
o	o	k7c4	o
moc	moc	k1gFnSc4	moc
a	a	k8xC	a
Ludvík	Ludvík	k1gMnSc1	Ludvík
Němec	Němec	k1gMnSc1	Němec
a	a	k8xC	a
Karel	Karel	k1gMnSc1	Karel
Holý	Holý	k1gMnSc1	Holý
uzavírají	uzavírat	k5eAaPmIp3nP	uzavírat
alianci	aliance	k1gFnSc4	aliance
proti	proti	k7c3	proti
staršímu	starý	k2eAgMnSc3d2	starší
bratru	bratr	k1gMnSc3	bratr
Lotharovi	Lothar	k1gMnSc3	Lothar
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
tímto	tento	k3xDgInSc7	tento
účelem	účel	k1gInSc7	účel
vzniká	vznikat	k5eAaImIp3nS	vznikat
Štrasburská	štrasburský	k2eAgFnSc1d1	Štrasburská
přísaha	přísaha	k1gFnSc1	přísaha
<g/>
,	,	kIx,	,
dokument	dokument	k1gInSc1	dokument
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
jako	jako	k8xS	jako
první	první	k4xOgMnSc1	první
v	v	k7c6	v
písemné	písemný	k2eAgFnSc6d1	písemná
podobě	podoba	k1gFnSc6	podoba
dokládá	dokládat	k5eAaImIp3nS	dokládat
podobu	podoba	k1gFnSc4	podoba
staré	starý	k2eAgFnSc2d1	stará
francouzštiny	francouzština	k1gFnSc2	francouzština
(	(	kIx(	(
<g/>
la	la	k1gNnSc1	la
langue	langue	k1gFnSc2	langue
romaine	romainout	k5eAaPmIp3nS	romainout
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
díky	díky	k7c3	díky
šíření	šíření	k1gNnSc3	šíření
kultury	kultura	k1gFnSc2	kultura
vzniká	vznikat	k5eAaImIp3nS	vznikat
v	v	k7c6	v
období	období	k1gNnSc6	období
karolinské	karolinský	k2eAgFnSc2d1	Karolinská
renesance	renesance	k1gFnSc2	renesance
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ve	v	k7c6	v
francouzštině	francouzština	k1gFnSc6	francouzština
nazývá	nazývat	k5eAaImIp3nS	nazývat
la	la	k1gNnSc1	la
langue	langue	k1gFnSc2	langue
franque	franque	k1gNnSc2	franque
nebo	nebo	k8xC	nebo
la	la	k1gNnSc2	la
langue	langue	k1gFnSc2	langue
francienne	franciennout	k5eAaPmIp3nS	franciennout
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
základ	základ	k1gInSc1	základ
moderní	moderní	k2eAgFnSc2d1	moderní
francouzštiny	francouzština	k1gFnSc2	francouzština
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Změny	změna	k1gFnPc1	změna
jazyka	jazyk	k1gInSc2	jazyk
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
obdobíObnova	obdobíObnův	k2eAgFnSc1d1	obdobíObnův
klasické	klasický	k2eAgFnPc4d1	klasická
latiny	latina	k1gFnPc4	latina
měla	mít	k5eAaImAgNnP	mít
vliv	vliv	k1gInSc4	vliv
především	především	k9	především
na	na	k7c4	na
slovní	slovní	k2eAgFnSc4d1	slovní
zásobu	zásoba	k1gFnSc4	zásoba
<g/>
.	.	kIx.	.
</s>
<s>
Vznikají	vznikat	k5eAaImIp3nP	vznikat
tzv.	tzv.	kA	tzv.
dubleta	dubleta	k1gFnSc1	dubleta
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
dvě	dva	k4xCgNnPc1	dva
slova	slovo	k1gNnPc1	slovo
různé	různý	k2eAgFnSc2d1	různá
pravopisné	pravopisný	k2eAgFnSc2d1	pravopisná
i	i	k8xC	i
fonetické	fonetický	k2eAgFnSc2d1	fonetická
formy	forma	k1gFnSc2	forma
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
pocházejí	pocházet	k5eAaImIp3nP	pocházet
ze	z	k7c2	z
stejného	stejný	k2eAgInSc2d1	stejný
latinského	latinský	k2eAgInSc2d1	latinský
základu	základ	k1gInSc2	základ
<g/>
.	.	kIx.	.
</s>
<s>
Příčinou	příčina	k1gFnSc7	příčina
je	být	k5eAaImIp3nS	být
dvojí	dvojí	k4xRgFnSc1	dvojí
podoba	podoba	k1gFnSc1	podoba
latiny	latina	k1gFnSc2	latina
<g/>
.	.	kIx.	.
</s>
<s>
Vulgární	vulgární	k2eAgFnSc1d1	vulgární
latina	latina	k1gFnSc1	latina
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
používal	používat	k5eAaImAgInS	používat
prostý	prostý	k2eAgInSc1d1	prostý
lid	lid	k1gInSc1	lid
<g/>
,	,	kIx,	,
dala	dát	k5eAaPmAgFnS	dát
za	za	k7c4	za
vznik	vznik	k1gInSc4	vznik
jednomu	jeden	k4xCgNnSc3	jeden
tvaru	tvar	k1gInSc3	tvar
<g/>
,	,	kIx,	,
vznikajícímu	vznikající	k2eAgNnSc3d1	vznikající
přirozenou	přirozený	k2eAgFnSc7d1	přirozená
cestou	cesta	k1gFnSc7	cesta
<g/>
,	,	kIx,	,
a	a	k8xC	a
latina	latina	k1gFnSc1	latina
klasická	klasický	k2eAgFnSc1d1	klasická
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
používali	používat	k5eAaImAgMnP	používat
učenci	učenec	k1gMnPc1	učenec
<g/>
,	,	kIx,	,
dala	dát	k5eAaPmAgFnS	dát
za	za	k7c4	za
vznik	vznik	k1gInSc4	vznik
druhému	druhý	k4xOgInSc3	druhý
tvaru	tvar	k1gInSc3	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
slova	slovo	k1gNnPc1	slovo
vznikala	vznikat	k5eAaImAgNnP	vznikat
uměle	uměle	k6eAd1	uměle
a	a	k8xC	a
vyplňovala	vyplňovat	k5eAaImAgFnS	vyplňovat
mezery	mezera	k1gFnPc4	mezera
ve	v	k7c6	v
slovní	slovní	k2eAgFnSc6d1	slovní
zásobě	zásoba	k1gFnSc6	zásoba
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
<g/>
-li	i	k?	-li
třeba	třeba	k6eAd1	třeba
popsat	popsat	k5eAaPmF	popsat
nový	nový	k2eAgInSc4d1	nový
jev	jev	k1gInSc4	jev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Např.	např.	kA	např.
z	z	k7c2	z
latinského	latinský	k2eAgInSc2d1	latinský
hospitalem	hospital	k1gMnSc7	hospital
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
přirozeným	přirozený	k2eAgInSc7d1	přirozený
vývojem	vývoj	k1gInSc7	vývoj
hôte	hôt	k1gInSc2	hôt
(	(	kIx(	(
<g/>
hostitel	hostitel	k1gMnSc1	hostitel
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
hôtel	hôtel	k1gInSc1	hôtel
(	(	kIx(	(
<g/>
hotel	hotel	k1gInSc1	hotel
<g/>
)	)	kIx)	)
a	a	k8xC	a
uměle	uměle	k6eAd1	uměle
bylo	být	k5eAaImAgNnS	být
vytvořeno	vytvořit	k5eAaPmNgNnS	vytvořit
hôpital	hôpital	k1gMnSc1	hôpital
(	(	kIx(	(
<g/>
nemocnice	nemocnice	k1gFnSc1	nemocnice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Změny	změna	k1gFnPc1	změna
v	v	k7c6	v
pravopise	pravopis	k1gInSc6	pravopis
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Změna	změna	k1gFnSc1	změna
souhlásky	souhláska	k1gFnSc2	souhláska
c	c	k0	c
na	na	k7c4	na
spřežku	spřežka	k1gFnSc4	spřežka
ch	ch	k0	ch
/	/	kIx~	/
<g/>
ʃ	ʃ	k?	ʃ
<g/>
/	/	kIx~	/
(	(	kIx(	(
<g/>
např.	např.	kA	např.
z	z	k7c2	z
latinského	latinský	k2eAgMnSc2d1	latinský
castelum	castelum	k1gInSc4	castelum
vzniká	vznikat	k5eAaImIp3nS	vznikat
château	château	k1gNnSc1	château
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Souhláska	souhláska	k1gFnSc1	souhláska
'	'	kIx"	'
<g/>
s	s	k7c7	s
<g/>
'	'	kIx"	'
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
mizí	mizet	k5eAaImIp3nP	mizet
před	před	k7c7	před
samohláskami	samohláska	k1gFnPc7	samohláska
<g/>
,	,	kIx,	,
místo	místo	k7c2	místo
ní	on	k3xPp3gFnSc2	on
se	se	k3xPyFc4	se
nad	nad	k7c7	nad
samohláskou	samohláska	k1gFnSc7	samohláska
píše	psát	k5eAaImIp3nS	psát
cirkumflex	cirkumflex	k1gInSc1	cirkumflex
^	^	kIx~	^
(	(	kIx(	(
<g/>
např.	např.	kA	např.
z	z	k7c2	z
latinského	latinský	k2eAgMnSc2d1	latinský
insula	insul	k1gMnSc2	insul
vzniká	vznikat	k5eAaImIp3nS	vznikat
île	île	k?	île
–	–	k?	–
ostrov	ostrov	k1gInSc1	ostrov
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
V	v	k7c6	v
pravopise	pravopis	k1gInSc6	pravopis
mizí	mizet	k5eAaImIp3nS	mizet
souhláska	souhláska	k1gFnSc1	souhláska
'	'	kIx"	'
<g/>
l	l	kA	l
<g/>
'	'	kIx"	'
a	a	k8xC	a
mění	měnit	k5eAaImIp3nS	měnit
se	se	k3xPyFc4	se
na	na	k7c4	na
spřežku	spřežka	k1gFnSc4	spřežka
'	'	kIx"	'
<g/>
au	au	k0	au
<g/>
'	'	kIx"	'
/	/	kIx~	/
<g/>
o	o	k7c4	o
<g/>
/	/	kIx~	/
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
případech	případ	k1gInPc6	případ
už	už	k6eAd1	už
ani	ani	k8xC	ani
v	v	k7c6	v
latině	latina	k1gFnSc6	latina
nevyslovovala	vyslovovat	k5eNaImAgFnS	vyslovovat
(	(	kIx(	(
<g/>
např.	např.	kA	např.
z	z	k7c2	z
latinského	latinský	k2eAgInSc2d1	latinský
auba	aub	k1gInSc2	aub
vzniká	vznikat	k5eAaImIp3nS	vznikat
aube	aube	k1gInSc1	aube
-	-	kIx~	-
úsvit	úsvit	k1gInSc1	úsvit
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Francouzština	francouzština	k1gFnSc1	francouzština
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
==	==	k?	==
</s>
</p>
<p>
<s>
Z	z	k7c2	z
francouzštiny	francouzština	k1gFnSc2	francouzština
pochází	pocházet	k5eAaImIp3nS	pocházet
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
mezinárodních	mezinárodní	k2eAgNnPc2d1	mezinárodní
slov	slovo	k1gNnPc2	slovo
–	–	k?	–
až	až	k6eAd1	až
70	[number]	k4	70
%	%	kIx~	%
slov	slovo	k1gNnPc2	slovo
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
angličtině	angličtina	k1gFnSc6	angličtina
má	mít	k5eAaImIp3nS	mít
francouzský	francouzský	k2eAgMnSc1d1	francouzský
(	(	kIx(	(
<g/>
a	a	k8xC	a
zprostředkovaně	zprostředkovaně	k6eAd1	zprostředkovaně
přes	přes	k7c4	přes
francouzštinu	francouzština	k1gFnSc4	francouzština
vlastně	vlastně	k9	vlastně
latinský	latinský	k2eAgInSc4d1	latinský
<g/>
)	)	kIx)	)
původ	původ	k1gInSc4	původ
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
.	.	kIx.	.
</s>
<s>
Francouzština	francouzština	k1gFnSc1	francouzština
je	být	k5eAaImIp3nS	být
mezinárodním	mezinárodní	k2eAgInSc7d1	mezinárodní
diplomatickým	diplomatický	k2eAgInSc7d1	diplomatický
jazykem	jazyk	k1gInSc7	jazyk
<g/>
,	,	kIx,	,
komunikačním	komunikační	k2eAgInSc7d1	komunikační
jazykem	jazyk	k1gInSc7	jazyk
EU	EU	kA	EU
<g/>
,	,	kIx,	,
NATO	NATO	kA	NATO
<g/>
,	,	kIx,	,
OSN	OSN	kA	OSN
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
organizací	organizace	k1gFnPc2	organizace
<g/>
;	;	kIx,	;
po	po	k7c6	po
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
je	být	k5eAaImIp3nS	být
však	však	k9	však
vytlačována	vytlačovat	k5eAaImNgFnS	vytlačovat
angličtinou	angličtina	k1gFnSc7	angličtina
a	a	k8xC	a
její	její	k3xOp3gInSc1	její
celosvětový	celosvětový	k2eAgInSc1d1	celosvětový
význam	význam	k1gInSc1	význam
je	být	k5eAaImIp3nS	být
menší	malý	k2eAgInSc1d2	menší
(	(	kIx(	(
<g/>
přes	přes	k7c4	přes
snahu	snaha	k1gFnSc4	snaha
francouzské	francouzský	k2eAgFnSc2d1	francouzská
vlády	vláda	k1gFnSc2	vláda
podporovat	podporovat	k5eAaImF	podporovat
šíření	šíření	k1gNnSc4	šíření
jazyka	jazyk	k1gInSc2	jazyk
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Abeceda	abeceda	k1gFnSc1	abeceda
a	a	k8xC	a
výslovnost	výslovnost	k1gFnSc1	výslovnost
==	==	k?	==
</s>
</p>
<p>
<s>
Francouzština	francouzština	k1gFnSc1	francouzština
se	se	k3xPyFc4	se
píše	psát	k5eAaImIp3nS	psát
latinkou	latinka	k1gFnSc7	latinka
s	s	k7c7	s
diakritickými	diakritický	k2eAgNnPc7d1	diakritické
znaménky	znaménko	k1gNnPc7	znaménko
<g/>
.	.	kIx.	.
</s>
<s>
Písmena	písmeno	k1gNnPc1	písmeno
s	s	k7c7	s
diakritikou	diakritika	k1gFnSc7	diakritika
se	se	k3xPyFc4	se
v	v	k7c6	v
abecedním	abecední	k2eAgNnSc6d1	abecední
pořadí	pořadí	k1gNnSc6	pořadí
řadí	řadit	k5eAaImIp3nS	řadit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
by	by	kYmCp3nS	by
diakritiku	diakritika	k1gFnSc4	diakritika
neobsahovala	obsahovat	k5eNaImAgFnS	obsahovat
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
např.	např.	kA	např.
slovo	slovo	k1gNnSc1	slovo
mè	mè	k?	mè
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
abecedě	abeceda	k1gFnSc6	abeceda
mezi	mezi	k7c7	mezi
slovy	slovo	k1gNnPc7	slovo
merci	merc	k1gMnSc5	merc
a	a	k8xC	a
mettre	mettr	k1gMnSc5	mettr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Francouzština	francouzština	k1gFnSc1	francouzština
používá	používat	k5eAaImIp3nS	používat
částečně	částečně	k6eAd1	částečně
spřežkový	spřežkový	k2eAgInSc1d1	spřežkový
pravopis	pravopis	k1gInSc1	pravopis
–	–	k?	–
ch	ch	k0	ch
[	[	kIx(	[
<g/>
ʃ	ʃ	k?	ʃ
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
gn	gn	k?	gn
[	[	kIx(	[
<g/>
ɲ	ɲ	k?	ɲ
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
ou	ou	k0	ou
[	[	kIx(	[
<g/>
u	u	k7c2	u
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
tch	tch	k0	tch
[	[	kIx(	[
<g/>
tʃ	tʃ	k?	tʃ
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
que	que	k?	que
[	[	kIx(	[
<g/>
kɛ	kɛ	k?	kɛ
<g/>
,	,	kIx,	,
k	k	k7c3	k
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
gue	gue	k?	gue
[	[	kIx(	[
<g/>
gɛ	gɛ	k?	gɛ
<g/>
,	,	kIx,	,
g	g	kA	g
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
aux	aux	k?	aux
[	[	kIx(	[
<g/>
o	o	k7c4	o
<g/>
]	]	kIx)	]
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
Dále	daleko	k6eAd2	daleko
používá	používat	k5eAaImIp3nS	používat
systém	systém	k1gInSc1	systém
přízvuků	přízvuk	k1gInPc2	přízvuk
(	(	kIx(	(
<g/>
fr	fr	k0	fr
<g/>
:	:	kIx,	:
accent	accent	k1gMnSc1	accent
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čtyři	čtyři	k4xCgInPc1	čtyři
pro	pro	k7c4	pro
samohlásky	samohláska	k1gFnPc4	samohláska
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
pro	pro	k7c4	pro
souhlásky	souhláska	k1gFnPc4	souhláska
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Accent	Accent	k1gMnSc1	Accent
circonflexe	circonflex	k1gInSc5	circonflex
(	(	kIx(	(
<g/>
cirkumflex	cirkumflex	k1gInSc4	cirkumflex
<g/>
)	)	kIx)	)
–	–	k?	–
â	â	k?	â
<g/>
,	,	kIx,	,
ê	ê	k?	ê
<g/>
,	,	kIx,	,
î	î	k?	î
<g/>
,	,	kIx,	,
ô	ô	k?	ô
<g/>
,	,	kIx,	,
û	û	k?	û
–	–	k?	–
česky	česky	k6eAd1	česky
vokáň	vokáň	k1gInSc4	vokáň
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
obrácený	obrácený	k2eAgInSc4d1	obrácený
háček	háček	k1gInSc4	háček
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
stříška	stříška	k1gFnSc1	stříška
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Použití	použití	k1gNnSc1	použití
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
historického	historický	k2eAgInSc2d1	historický
vývoje	vývoj	k1gInSc2	vývoj
francouzštiny	francouzština	k1gFnSc2	francouzština
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
po	po	k7c6	po
takto	takto	k6eAd1	takto
označené	označený	k2eAgFnSc6d1	označená
samohlásce	samohláska	k1gFnSc6	samohláska
následovala	následovat	k5eAaImAgFnS	následovat
souhláska	souhláska	k1gFnSc1	souhláska
"	"	kIx"	"
<g/>
s	s	k7c7	s
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
např.	např.	kA	např.
hostel	hostel	k1gMnSc1	hostel
–	–	k?	–
hôtel	hôtel	k1gMnSc1	hôtel
[	[	kIx(	[
<g/>
ɔ	ɔ	k?	ɔ
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Accent	Accent	k1gInSc1	Accent
aigu	aigus	k1gInSc2	aigus
(	(	kIx(	(
<g/>
ostrý	ostrý	k2eAgInSc1d1	ostrý
přízvuk	přízvuk	k1gInSc1	přízvuk
<g/>
)	)	kIx)	)
–	–	k?	–
é	é	k0	é
–	–	k?	–
pozor	pozor	k1gInSc4	pozor
<g/>
,	,	kIx,	,
neznačí	značit	k5eNaImIp3nS	značit
jako	jako	k9	jako
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
prodlouženou	prodloužený	k2eAgFnSc4d1	prodloužená
výslovnost	výslovnost	k1gFnSc4	výslovnost
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
předchází	předcházet	k5eAaImIp3nS	předcházet
ve	v	k7c6	v
slově	slovo	k1gNnSc6	slovo
vyslovenou	vyslovený	k2eAgFnSc4d1	vyslovená
samohlásku	samohláska	k1gFnSc4	samohláska
(	(	kIx(	(
<g/>
např.	např.	kA	např.
étudiant	étudiant	k1gInSc1	étudiant
[	[	kIx(	[
<g/>
etydjɑ	etydjɑ	k?	etydjɑ
<g/>
̃	̃	k?	̃
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Accent	Accent	k1gInSc1	Accent
grave	grave	k1gNnSc2	grave
(	(	kIx(	(
<g/>
tupý	tupý	k2eAgInSc1d1	tupý
přízvuk	přízvuk	k1gInSc1	přízvuk
<g/>
)	)	kIx)	)
–	–	k?	–
à	à	k?	à
<g/>
,	,	kIx,	,
è	è	k?	è
<g/>
,	,	kIx,	,
ù	ù	k?	ù
–	–	k?	–
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
se	se	k3xPyFc4	se
prodlouženě	prodlouženě	k6eAd1	prodlouženě
pouze	pouze	k6eAd1	pouze
před	před	k7c7	před
souhláskami	souhláska	k1gFnPc7	souhláska
či	či	k8xC	či
skupinami	skupina	k1gFnPc7	skupina
souhlásek	souhláska	k1gFnPc2	souhláska
vyslovovaných	vyslovovaný	k2eAgFnPc2d1	vyslovovaná
[	[	kIx(	[
<g/>
v	v	k7c6	v
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
ʀ	ʀ	k?	ʀ
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
z	z	k7c2	z
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
ʒ	ʒ	k?	ʒ
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
vʀ	vʀ	k?	vʀ
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
např.	např.	kA	např.
pè	pè	k?	pè
[	[	kIx(	[
<g/>
pɛ	pɛ	k?	pɛ
<g/>
:	:	kIx,	:
<g/>
ʀ	ʀ	k?	ʀ
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
ve	v	k7c6	v
slově	slovo	k1gNnSc6	slovo
předchází	předcházet	k5eAaImIp3nP	předcházet
nevyslovenou	vyslovený	k2eNgFnSc4d1	nevyslovená
samohlásku	samohláska	k1gFnSc4	samohláska
<g/>
,	,	kIx,	,
většinou	většina	k1gFnSc7	většina
tzv.	tzv.	kA	tzv.
němé	němý	k2eAgFnSc2d1	němá
e	e	k0	e
na	na	k7c6	na
konci	konec	k1gInSc6	konec
slova	slovo	k1gNnSc2	slovo
(	(	kIx(	(
<g/>
např.	např.	kA	např.
mè	mè	k?	mè
[	[	kIx(	[
<g/>
mɛ	mɛ	k?	mɛ
<g/>
:	:	kIx,	:
<g/>
ʀ	ʀ	k?	ʀ
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tréma	tréma	k1gFnSc1	tréma
(	(	kIx(	(
<g/>
dvojtečka	dvojtečka	k1gFnSc1	dvojtečka
nad	nad	k7c7	nad
samohláskou	samohláska	k1gFnSc7	samohláska
<g/>
)	)	kIx)	)
–	–	k?	–
ë	ë	k?	ë
<g/>
,	,	kIx,	,
ï	ï	k?	ï
<g/>
,	,	kIx,	,
ü	ü	k?	ü
<g/>
,	,	kIx,	,
ÿ	ÿ	k?	ÿ
–	–	k?	–
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
dvou	dva	k4xCgFnPc2	dva
hlásek	hláska	k1gFnPc2	hláska
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
chceme	chtít	k5eAaImIp1nP	chtít
zachovat	zachovat	k5eAaPmF	zachovat
jejich	jejich	k3xOp3gFnSc4	jejich
výslovnost	výslovnost	k1gFnSc4	výslovnost
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
hlásky	hlásek	k1gInPc1	hlásek
by	by	kYmCp3nP	by
jinak	jinak	k6eAd1	jinak
fungovaly	fungovat	k5eAaImAgFnP	fungovat
jako	jako	k9	jako
spřežky	spřežka	k1gFnPc1	spřežka
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
výslovnost	výslovnost	k1gFnSc1	výslovnost
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
redukovaná	redukovaný	k2eAgFnSc1d1	redukovaná
toliko	toliko	k6eAd1	toliko
na	na	k7c4	na
jediný	jediný	k2eAgInSc4d1	jediný
foném	foném	k1gInSc4	foném
(	(	kIx(	(
<g/>
např.	např.	kA	např.
maï	maï	k?	maï
[	[	kIx(	[
<g/>
mais	mais	k1gInSc1	mais
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mais	mais	k1gInSc1	mais
[	[	kIx(	[
<g/>
mɛ	mɛ	k?	mɛ
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
C	C	kA	C
cédille	cédille	k1gInPc1	cédille
(	(	kIx(	(
<g/>
cedilla	cedilla	k1gFnSc1	cedilla
<g/>
)	)	kIx)	)
–	–	k?	–
ç	ç	k?	ç
–	–	k?	–
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
se	se	k3xPyFc4	se
jako	jako	k9	jako
[	[	kIx(	[
<g/>
s	s	k7c7	s
<g/>
]	]	kIx)	]
a	a	k8xC	a
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
před	před	k7c4	před
tzv.	tzv.	kA	tzv.
tvrdými	tvrdý	k2eAgFnPc7d1	tvrdá
samohláskami	samohláska	k1gFnPc7	samohláska
"	"	kIx"	"
<g/>
a	a	k8xC	a
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
o	o	k7c4	o
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
u	u	k7c2	u
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
např.	např.	kA	např.
François	François	k1gFnSc6	François
[	[	kIx(	[
<g/>
fʀ	fʀ	k?	fʀ
<g/>
̃	̃	k?	̃
<g/>
sła	sła	k?	sła
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Jazyk	jazyk	k1gInSc1	jazyk
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
silnou	silný	k2eAgFnSc7d1	silná
fonetickou	fonetický	k2eAgFnSc7d1	fonetická
redukcí	redukce	k1gFnSc7	redukce
slova	slovo	k1gNnSc2	slovo
–	–	k?	–
koncové	koncový	k2eAgFnSc2d1	koncová
souhlásky	souhláska	k1gFnSc2	souhláska
se	se	k3xPyFc4	se
většinou	většina	k1gFnSc7	většina
vůbec	vůbec	k9	vůbec
nevyslovují	vyslovovat	k5eNaImIp3nP	vyslovovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Gramatika	gramatika	k1gFnSc1	gramatika
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Zájmena	zájmeno	k1gNnSc2	zájmeno
===	===	k?	===
</s>
</p>
<p>
<s>
Francouzština	francouzština	k1gFnSc1	francouzština
používá	používat	k5eAaImIp3nS	používat
následující	následující	k2eAgNnPc4d1	následující
osobní	osobní	k2eAgNnPc4d1	osobní
zájmena	zájmeno	k1gNnPc4	zájmeno
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Poznámky	poznámka	k1gFnPc1	poznámka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Všimněte	všimnout	k5eAaPmRp2nP	všimnout
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
výslovnost	výslovnost	k1gFnSc1	výslovnost
tvarů	tvar	k1gInPc2	tvar
il	il	k?	il
a	a	k8xC	a
ils	ils	k?	ils
je	být	k5eAaImIp3nS	být
stejná	stejný	k2eAgFnSc1d1	stejná
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
gramatické	gramatický	k2eAgNnSc4d1	gramatické
číslo	číslo	k1gNnSc4	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Totéž	týž	k3xTgNnSc1	týž
platí	platit	k5eAaImIp3nS	platit
pro	pro	k7c4	pro
osoby	osoba	k1gFnPc4	osoba
elle	elle	k1gFnPc2	elle
a	a	k8xC	a
elles	ellesa	k1gFnPc2	ellesa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
přehledu	přehled	k1gInSc2	přehled
můžeme	moct	k5eAaImIp1nP	moct
zařadit	zařadit	k5eAaPmF	zařadit
rovněž	rovněž	k9	rovněž
tzv.	tzv.	kA	tzv.
neurčitou	určitý	k2eNgFnSc4d1	neurčitá
osobu	osoba	k1gFnSc4	osoba
on	on	k3xPp3gMnSc1	on
[	[	kIx(	[
<g/>
ɔ	ɔ	k?	ɔ
<g/>
̃	̃	k?	̃
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
on	on	k3xPp3gInSc1	on
parle	parle	k1gInSc1	parle
français	français	k1gFnSc2	français
ici	ici	k?	ici
-	-	kIx~	-
tady	tady	k6eAd1	tady
se	se	k3xPyFc4	se
mluví	mluvit	k5eAaImIp3nS	mluvit
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
,	,	kIx,	,
mluvíme	mluvit	k5eAaImIp1nP	mluvit
zde	zde	k6eAd1	zde
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
(	(	kIx(	(
<g/>
lze	lze	k6eAd1	lze
použít	použít	k5eAaPmF	použít
také	také	k9	také
osobu	osoba	k1gFnSc4	osoba
nous	nousa	k1gFnPc2	nousa
<g/>
:	:	kIx,	:
nous	nous	k6eAd1	nous
parlons	parlons	k6eAd1	parlons
français	français	k1gInSc1	français
ici	ici	k?	ici
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
on	on	k3xPp3gMnSc1	on
mange	mangat	k5eAaPmIp3nS	mangat
quelque	quelque	k6eAd1	quelque
chose	chose	k6eAd1	chose
de	de	k?	de
bon	bon	k1gInSc1	bon
-	-	kIx~	-
jíme	jíst	k5eAaImIp1nP	jíst
něco	něco	k3yInSc4	něco
dobrého	dobrý	k2eAgNnSc2d1	dobré
<g/>
(	(	kIx(	(
<g/>
lze	lze	k6eAd1	lze
použít	použít	k5eAaPmF	použít
také	také	k9	také
osobu	osoba	k1gFnSc4	osoba
nous	nousa	k1gFnPc2	nousa
<g/>
:	:	kIx,	:
nous	nous	k6eAd1	nous
mangeons	mangeons	k6eAd1	mangeons
quelque	quelquat	k5eAaPmIp3nS	quelquat
chose	chose	k6eAd1	chose
de	de	k?	de
bon	bon	k1gInSc1	bon
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Číslovky	číslovka	k1gFnSc2	číslovka
===	===	k?	===
</s>
</p>
<p>
<s>
Francouzský	francouzský	k2eAgInSc1d1	francouzský
systém	systém	k1gInSc1	systém
číslovek	číslovka	k1gFnPc2	číslovka
je	být	k5eAaImIp3nS	být
komplikovaný	komplikovaný	k2eAgMnSc1d1	komplikovaný
a	a	k8xC	a
nevychází	vycházet	k5eNaImIp3nS	vycházet
zcela	zcela	k6eAd1	zcela
z	z	k7c2	z
desítkové	desítkový	k2eAgFnSc2d1	desítková
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
číslovka	číslovka	k1gFnSc1	číslovka
"	"	kIx"	"
<g/>
sedmdesát	sedmdesát	k4xCc1	sedmdesát
<g/>
"	"	kIx"	"
není	být	k5eNaImIp3nS	být
odvozena	odvodit	k5eAaPmNgFnS	odvodit
z	z	k7c2	z
číslovky	číslovka	k1gFnSc2	číslovka
"	"	kIx"	"
<g/>
sedm	sedm	k4xCc4	sedm
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
řekne	říct	k5eAaPmIp3nS	říct
"	"	kIx"	"
<g/>
soixante-dix	soixanteix	k1gInSc1	soixante-dix
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
šedesát	šedesát	k4xCc1	šedesát
deset	deset	k4xCc4	deset
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Doslovný	doslovný	k2eAgInSc1d1	doslovný
překlad	překlad	k1gInSc1	překlad
osmdesátky	osmdesátka	k1gFnSc2	osmdesátka
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
čtyři	čtyři	k4xCgFnPc1	čtyři
dvacítky	dvacítka	k1gFnPc1	dvacítka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
92	[number]	k4	92
se	se	k3xPyFc4	se
řekne	říct	k5eAaPmIp3nS	říct
"	"	kIx"	"
<g/>
čtyři	čtyři	k4xCgMnPc4	čtyři
dvacet	dvacet	k4xCc4	dvacet
dvanáct	dvanáct	k4xCc4	dvanáct
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
99	[number]	k4	99
potom	potom	k8xC	potom
dokonce	dokonce	k9	dokonce
"	"	kIx"	"
<g/>
čtyři	čtyři	k4xCgMnPc4	čtyři
dvacet	dvacet	k4xCc4	dvacet
deset	deset	k4xCc4	deset
devět	devět	k4xCc4	devět
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
tradiční	tradiční	k2eAgInSc1d1	tradiční
systém	systém	k1gInSc1	systém
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
zpravidelňuje	zpravidelňovat	k5eAaImIp3nS	zpravidelňovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výrazy	výraz	k1gInPc1	výraz
septante	septant	k1gMnSc5	septant
(	(	kIx(	(
<g/>
70	[number]	k4	70
<g/>
)	)	kIx)	)
a	a	k8xC	a
nonante	nonant	k1gMnSc5	nonant
(	(	kIx(	(
<g/>
90	[number]	k4	90
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
oficiálně	oficiálně	k6eAd1	oficiálně
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
a	a	k8xC	a
Belgii	Belgie	k1gFnSc3	Belgie
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
běžné	běžný	k2eAgInPc1d1	běžný
také	také	k9	také
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
(	(	kIx(	(
<g/>
Val	val	k1gInSc1	val
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Aoste	Aost	k1gMnSc5	Aost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kongu	Kongo	k1gNnSc6	Kongo
a	a	k8xC	a
Rwandě	Rwanda	k1gFnSc6	Rwanda
<g/>
.	.	kIx.	.
</s>
<s>
Huitante	Huitant	k1gMnSc5	Huitant
(	(	kIx(	(
<g/>
80	[number]	k4	80
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
kantonech	kanton	k1gInPc6	kanton
i	i	k9	i
oficiálně	oficiálně	k6eAd1	oficiálně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Původ	původ	k1gInSc1	původ
tohoto	tento	k3xDgInSc2	tento
systému	systém	k1gInSc2	systém
není	být	k5eNaImIp3nS	být
jednoznačný	jednoznačný	k2eAgInSc1d1	jednoznačný
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
jednat	jednat	k5eAaImF	jednat
o	o	k7c4	o
pozůstatek	pozůstatek	k1gInSc4	pozůstatek
historického	historický	k2eAgNnSc2d1	historické
počítání	počítání	k1gNnSc2	počítání
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
české	český	k2eAgFnSc6d1	Česká
pár	pár	k4xCyI	pár
<g/>
,	,	kIx,	,
půltucet	půltucet	k1gInSc1	půltucet
<g/>
,	,	kIx,	,
tucet	tucet	k1gInSc1	tucet
<g/>
,	,	kIx,	,
veletucet	veletucet	k1gInSc1	veletucet
<g/>
,	,	kIx,	,
kopa	kopa	k1gFnSc1	kopa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
ale	ale	k9	ale
také	také	k9	také
jednat	jednat	k5eAaImF	jednat
o	o	k7c4	o
důvod	důvod	k1gInSc4	důvod
ten	ten	k3xDgInSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
výrazy	výraz	k1gInPc1	výraz
septante	septant	k1gMnSc5	septant
(	(	kIx(	(
<g/>
70	[number]	k4	70
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
huitante	huitant	k1gMnSc5	huitant
(	(	kIx(	(
<g/>
80	[number]	k4	80
<g/>
)	)	kIx)	)
a	a	k8xC	a
nonante	nonant	k1gMnSc5	nonant
(	(	kIx(	(
<g/>
90	[number]	k4	90
<g/>
)	)	kIx)	)
zní	znět	k5eAaImIp3nS	znět
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
jiné	jiný	k2eAgFnPc1d1	jiná
číslovky	číslovka	k1gFnPc1	číslovka
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
záměně	záměna	k1gFnSc3	záměna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Slovesa	sloveso	k1gNnSc2	sloveso
===	===	k?	===
</s>
</p>
<p>
<s>
Systém	systém	k1gInSc1	systém
slovesných	slovesný	k2eAgInPc2d1	slovesný
časů	čas	k1gInPc2	čas
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
bohatší	bohatý	k2eAgFnSc1d2	bohatší
než	než	k8xS	než
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
.	.	kIx.	.
</s>
<s>
Francouzština	francouzština	k1gFnSc1	francouzština
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
šest	šest	k4xCc4	šest
minulých	minulý	k2eAgInPc2d1	minulý
časů	čas	k1gInPc2	čas
(	(	kIx(	(
<g/>
passé	passý	k2eAgFnPc1d1	passý
composé	composý	k2eAgFnPc1d1	composý
<g/>
,	,	kIx,	,
imparfait	imparfait	k1gMnSc1	imparfait
<g/>
,	,	kIx,	,
plus-que-parfait	plusuearfait	k1gMnSc1	plus-que-parfait
<g/>
,	,	kIx,	,
passé	passý	k2eAgFnPc1d1	passý
simple	simple	k6eAd1	simple
<g/>
,	,	kIx,	,
passé	passý	k2eAgFnPc1d1	passý
antérieur	antérieura	k1gFnPc2	antérieura
a	a	k8xC	a
passé	passý	k2eAgInPc4d1	passý
récent	récent	k1gInSc4	récent
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
přítomný	přítomný	k1gMnSc1	přítomný
a	a	k8xC	a
tři	tři	k4xCgFnPc4	tři
budoucí	budoucí	k2eAgFnPc4d1	budoucí
(	(	kIx(	(
<g/>
futur	futurum	k1gNnPc2	futurum
simple	simple	k6eAd1	simple
<g/>
,	,	kIx,	,
futur	futurum	k1gNnPc2	futurum
antérieur	antérieura	k1gFnPc2	antérieura
a	a	k8xC	a
futur	futurum	k1gNnPc2	futurum
proche	proche	k1gFnPc2	proche
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Časování	časování	k1gNnSc1	časování
pravidelných	pravidelný	k2eAgNnPc2d1	pravidelné
sloves	sloveso	k1gNnPc2	sloveso
====	====	k?	====
</s>
</p>
<p>
<s>
=====	=====	k?	=====
Časování	časování	k1gNnSc6	časování
sloves	sloveso	k1gNnPc2	sloveso
1	[number]	k4	1
<g/>
.	.	kIx.	.
třídy	třída	k1gFnSc2	třída
=====	=====	k?	=====
</s>
</p>
<p>
<s>
Pozn	pozn	kA	pozn
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Koncovky	koncovka	k1gFnPc1	koncovka
této	tento	k3xDgFnSc2	tento
slovesné	slovesný	k2eAgFnSc2d1	slovesná
třídy	třída	k1gFnSc2	třída
jsou	být	k5eAaImIp3nP	být
tedy	tedy	k9	tedy
-e	-e	k?	-e
<g/>
,	,	kIx,	,
-es	-es	k?	-es
<g/>
,	,	kIx,	,
-e	-e	k?	-e
<g/>
,	,	kIx,	,
-ons	-ons	k1gInSc1	-ons
<g/>
,	,	kIx,	,
-ez	-ez	k?	-ez
<g/>
,	,	kIx,	,
-ent	-ent	k1gInSc1	-ent
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc4	tento
koncovky	koncovka	k1gFnPc4	koncovka
přiřazujeme	přiřazovat	k5eAaImIp1nP	přiřazovat
ke	k	k7c3	k
slovesnému	slovesný	k2eAgInSc3d1	slovesný
základu	základ	k1gInSc3	základ
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
obdržíme	obdržet	k5eAaPmIp1nP	obdržet
odtržením	odtržení	k1gNnSc7	odtržení
koncovky	koncovka	k1gFnSc2	koncovka
-er	r	k?	-er
z	z	k7c2	z
infinitivu	infinitiv	k1gInSc2	infinitiv
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
slovesa	sloveso	k1gNnSc2	sloveso
parler	parler	k1gInSc1	parler
bude	být	k5eAaImBp3nS	být
tedy	tedy	k9	tedy
základem	základ	k1gInSc7	základ
časování	časování	k1gNnSc2	časování
kořen	kořen	k1gInSc1	kořen
parl-	parl-	k?	parl-
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pouze	pouze	k6eAd1	pouze
koncovky	koncovka	k1gFnSc2	koncovka
-ons	ns	k6eAd1	-ons
a	a	k8xC	a
-ez	z	k?	-ez
se	se	k3xPyFc4	se
čtou	číst	k5eAaImIp3nP	číst
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
přízvučné	přízvučný	k2eAgInPc1d1	přízvučný
(	(	kIx(	(
<g/>
v	v	k7c6	v
tabulce	tabulka	k1gFnSc6	tabulka
je	být	k5eAaImIp3nS	být
přízvuk	přízvuk	k1gInSc1	přízvuk
označen	označen	k2eAgInSc1d1	označen
tučným	tučný	k2eAgNnSc7d1	tučné
písmenem	písmeno	k1gNnSc7	písmeno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgFnPc1d1	ostatní
koncovky	koncovka	k1gFnPc1	koncovka
jsou	být	k5eAaImIp3nP	být
nepřízvučné	přízvučný	k2eNgFnPc1d1	nepřízvučná
a	a	k8xC	a
slovní	slovní	k2eAgInSc1d1	slovní
přízvuk	přízvuk	k1gInSc1	přízvuk
se	se	k3xPyFc4	se
stěhuje	stěhovat	k5eAaImIp3nS	stěhovat
na	na	k7c4	na
předchozí	předchozí	k2eAgFnSc4d1	předchozí
samohlásku	samohláska	k1gFnSc4	samohláska
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
nejblíže	blízce	k6eAd3	blízce
konci	konec	k1gInSc3	konec
slova	slovo	k1gNnSc2	slovo
(	(	kIx(	(
<g/>
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
[	[	kIx(	[
<g/>
a	a	k8xC	a
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
rovněž	rovněž	k9	rovněž
označeno	označit	k5eAaPmNgNnS	označit
tučně	tučně	k6eAd1	tučně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Pozor	pozor	k1gInSc1	pozor
na	na	k7c4	na
časování	časování	k1gNnSc4	časování
těch	ten	k3xDgMnPc2	ten
sloves	sloveso	k1gNnPc2	sloveso
1	[number]	k4	1
<g/>
.	.	kIx.	.
třídy	třída	k1gFnSc2	třída
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
infinitiv	infinitiv	k1gInSc1	infinitiv
začíná	začínat	k5eAaImIp3nS	začínat
na	na	k7c4	na
samohlásku	samohláska	k1gFnSc4	samohláska
nebo	nebo	k8xC	nebo
tzv.	tzv.	kA	tzv.
němé	němý	k2eAgInPc1d1	němý
h	h	k?	h
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Pozn	pozn	kA	pozn
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
osobě	osoba	k1gFnSc3	osoba
jednotného	jednotný	k2eAgNnSc2d1	jednotné
čísla	číslo	k1gNnSc2	číslo
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
tzv.	tzv.	kA	tzv.
elizi	elize	k1gFnSc4	elize
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
k	k	k7c3	k
odstranění	odstranění	k1gNnSc3	odstranění
písmene	písmeno	k1gNnSc2	písmeno
e	e	k0	e
ze	z	k7c2	z
zájmene	zájmenout	k5eAaPmIp3nS	zájmenout
je	on	k3xPp3gMnPc4	on
a	a	k8xC	a
k	k	k7c3	k
nahrazení	nahrazení	k1gNnSc3	nahrazení
tohoto	tento	k3xDgNnSc2	tento
písmene	písmeno	k1gNnSc2	písmeno
apostrofem	apostrof	k1gInSc7	apostrof
<g/>
.	.	kIx.	.
</s>
<s>
Znamená	znamenat	k5eAaImIp3nS	znamenat
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
zájmeno	zájmeno	k1gNnSc4	zájmeno
píšeme	psát	k5eAaImIp1nP	psát
dohromady	dohromady	k6eAd1	dohromady
se	s	k7c7	s
slovesným	slovesný	k2eAgInSc7d1	slovesný
tvarem	tvar	k1gInSc7	tvar
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
bez	bez	k7c2	bez
mezery	mezera	k1gFnSc2	mezera
mezi	mezi	k7c7	mezi
zájmenem	zájmeno	k1gNnSc7	zájmeno
a	a	k8xC	a
slovesným	slovesný	k2eAgInSc7d1	slovesný
tvarem	tvar	k1gInSc7	tvar
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výslovnost	výslovnost	k1gFnSc1	výslovnost
takového	takový	k3xDgMnSc4	takový
zájmene	zájmenout	k5eAaPmIp3nS	zájmenout
rovněž	rovněž	k9	rovněž
"	"	kIx"	"
<g/>
srůstá	srůstat	k5eAaImIp3nS	srůstat
<g/>
"	"	kIx"	"
se	s	k7c7	s
slovesným	slovesný	k2eAgInSc7d1	slovesný
tvarem	tvar	k1gInSc7	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Nevyslovujeme	vyslovovat	k5eNaImIp1nP	vyslovovat
tedy	tedy	k9	tedy
zvlášť	zvlášť	k6eAd1	zvlášť
[	[	kIx(	[
<g/>
ʒ	ʒ	k?	ʒ
<g/>
]	]	kIx)	]
a	a	k8xC	a
[	[	kIx(	[
<g/>
ekut	ekut	k2eAgMnSc1d1	ekut
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
odstraníme	odstranit	k5eAaPmIp1nP	odstranit
z	z	k7c2	z
výslovnosti	výslovnost	k1gFnSc2	výslovnost
zájmene	zájmenout	k5eAaPmIp3nS	zájmenout
tzv.	tzv.	kA	tzv.
němé	němý	k2eAgInPc1d1	němý
[	[	kIx(	[
<g/>
ə	ə	k?	ə
<g/>
]	]	kIx)	]
a	a	k8xC	a
vyslovíme	vyslovit	k5eAaPmIp1nP	vyslovit
rovnou	rovnou	k6eAd1	rovnou
[	[	kIx(	[
<g/>
ʒ	ʒ	k?	ʒ
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
osobách	osoba	k1gFnPc6	osoba
on	on	k3xPp3gMnSc1	on
<g/>
,	,	kIx,	,
nous	nous	k1gInSc1	nous
<g/>
,	,	kIx,	,
vous	vous	k1gInSc1	vous
<g/>
,	,	kIx,	,
ils	ils	k?	ils
a	a	k8xC	a
elles	elles	k1gMnSc1	elles
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
tzv.	tzv.	kA	tzv.
mezislovnímu	mezislovní	k2eAgInSc3d1	mezislovní
vázání	vázání	k1gNnSc6	vázání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
písmu	písmo	k1gNnSc6	písmo
se	se	k3xPyFc4	se
nemění	měnit	k5eNaImIp3nS	měnit
nic	nic	k3yNnSc1	nic
<g/>
.	.	kIx.	.
</s>
<s>
Zato	zato	k6eAd1	zato
ve	v	k7c6	v
výslovnosti	výslovnost	k1gFnSc6	výslovnost
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
oživení	oživení	k1gNnSc3	oživení
poslední	poslední	k2eAgFnSc2d1	poslední
souhlásky	souhláska	k1gFnSc2	souhláska
zájmene	zájmenout	k5eAaPmIp3nS	zájmenout
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
n	n	k0	n
nebo	nebo	k8xC	nebo
s.	s.	k?	s.
V	v	k7c6	v
případě	případ	k1gInSc6	případ
samohlásky	samohláska	k1gFnSc2	samohláska
s	s	k7c7	s
je	být	k5eAaImIp3nS	být
vyslovena	vysloven	k2eAgFnSc1d1	vyslovena
její	její	k3xOp3gFnSc1	její
znělá	znělý	k2eAgFnSc1d1	znělá
podoba	podoba	k1gFnSc1	podoba
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
celý	celý	k2eAgInSc1d1	celý
slovesný	slovesný	k2eAgInSc1d1	slovesný
tvar	tvar	k1gInSc1	tvar
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
zájmeno	zájmeno	k1gNnSc4	zájmeno
a	a	k8xC	a
sloveso	sloveso	k1gNnSc4	sloveso
<g/>
)	)	kIx)	)
zněl	znět	k5eAaImAgInS	znět
dohromady	dohromady	k6eAd1	dohromady
jako	jako	k8xC	jako
jedno	jeden	k4xCgNnSc1	jeden
slovo	slovo	k1gNnSc1	slovo
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
výše	výše	k1gFnSc2	výše
v	v	k7c6	v
tabulce	tabulka	k1gFnSc6	tabulka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zájmeno	zájmeno	k1gNnSc1	zájmeno
on	on	k3xPp3gMnSc1	on
ve	v	k7c6	v
výslovnostní	výslovnostní	k2eAgFnSc6d1	výslovnostní
praxi	praxe	k1gFnSc6	praxe
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
svou	svůj	k3xOyFgFnSc4	svůj
znělost	znělost	k1gFnSc4	znělost
a	a	k8xC	a
místo	místo	k1gNnSc4	místo
nosovosti	nosovost	k1gFnSc2	nosovost
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
výslovnost	výslovnost	k1gFnSc1	výslovnost
[	[	kIx(	[
<g/>
on	on	k3xPp3gMnSc1	on
<g/>
...	...	k?	...
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
=====	=====	k?	=====
Časování	časování	k1gNnSc6	časování
sloves	sloveso	k1gNnPc2	sloveso
2	[number]	k4	2
<g/>
.	.	kIx.	.
třídy	třída	k1gFnSc2	třída
=====	=====	k?	=====
</s>
</p>
<p>
<s>
====	====	k?	====
Časování	časování	k1gNnSc1	časování
nepravidelných	pravidelný	k2eNgNnPc2d1	nepravidelné
sloves	sloveso	k1gNnPc2	sloveso
====	====	k?	====
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
francouzštině	francouzština	k1gFnSc6	francouzština
je	být	k5eAaImIp3nS	být
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
např.	např.	kA	např.
od	od	k7c2	od
italštiny	italština	k1gFnSc2	italština
<g/>
)	)	kIx)	)
poměrně	poměrně	k6eAd1	poměrně
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
nepravidelných	pravidelný	k2eNgNnPc2d1	nepravidelné
sloves	sloveso	k1gNnPc2	sloveso
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Počet	počet	k1gInSc1	počet
slov	slovo	k1gNnPc2	slovo
==	==	k?	==
</s>
</p>
<p>
<s>
Francouzština	francouzština	k1gFnSc1	francouzština
má	mít	k5eAaImIp3nS	mít
asi	asi	k9	asi
100	[number]	k4	100
000	[number]	k4	000
slov	slovo	k1gNnPc2	slovo
</s>
</p>
<p>
<s>
==	==	k?	==
Vzorový	vzorový	k2eAgInSc1d1	vzorový
text	text	k1gInSc1	text
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Všeobecná	všeobecný	k2eAgFnSc1d1	všeobecná
deklarace	deklarace	k1gFnSc1	deklarace
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
francouzština	francouzština	k1gFnSc1	francouzština
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
francouzština	francouzština	k1gFnSc1	francouzština
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Francouzsko-český	francouzsko-český	k2eAgInSc1d1	francouzsko-český
slovník	slovník	k1gInSc1	slovník
online	onlinout	k5eAaPmIp3nS	onlinout
</s>
</p>
<p>
<s>
Online	Onlinout	k5eAaPmIp3nS	Onlinout
test	test	k1gInSc1	test
z	z	k7c2	z
francouzštiny	francouzština	k1gFnSc2	francouzština
zdarma	zdarma	k6eAd1	zdarma
</s>
</p>
