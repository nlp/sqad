<s>
Francouzština	francouzština	k1gFnSc1	francouzština
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
též	též	k9	též
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
franština	franština	k1gFnSc1	franština
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
románský	románský	k2eAgInSc1d1	románský
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
se	se	k3xPyFc4	se
hovoří	hovořit	k5eAaImIp3nS	hovořit
především	především	k9	především
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
Belgii	Belgie	k1gFnSc6	Belgie
<g/>
,	,	kIx,	,
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
,	,	kIx,	,
Kanadě	Kanada	k1gFnSc6	Kanada
a	a	k8xC	a
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
zemích	zem	k1gFnPc6	zem
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
