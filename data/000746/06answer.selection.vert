<s>
Již	již	k6eAd1	již
jako	jako	k8xS	jako
dítě	dítě	k1gNnSc4	dítě
byla	být	k5eAaImAgFnS	být
Simone	Simon	k1gMnSc5	Simon
de	de	k?	de
Beauvoir	Beauvoir	k1gMnSc1	Beauvoir
intelektuálně	intelektuálně	k6eAd1	intelektuálně
velmi	velmi	k6eAd1	velmi
zdatná	zdatný	k2eAgFnSc1d1	zdatná
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
také	také	k9	také
vystudovala	vystudovat	k5eAaPmAgFnS	vystudovat
matematiku	matematika	k1gFnSc4	matematika
a	a	k8xC	a
poté	poté	k6eAd1	poté
filosofii	filosofie	k1gFnSc3	filosofie
na	na	k7c6	na
prestižní	prestižní	k2eAgFnSc6d1	prestižní
pařížské	pařížský	k2eAgFnSc6d1	Pařížská
univerzitě	univerzita	k1gFnSc6	univerzita
-	-	kIx~	-
Sorbonně	Sorbonna	k1gFnSc6	Sorbonna
<g/>
.	.	kIx.	.
</s>
