<p>
<s>
Jodid	jodid	k1gInSc1	jodid
měďný	měďný	k1gMnSc1	měďný
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
,	,	kIx,	,
anorganická	anorganický	k2eAgFnSc1d1	anorganická
sloučenina	sloučenina	k1gFnSc1	sloučenina
se	s	k7c7	s
vzorcem	vzorec	k1gInSc7	vzorec
CuI	CuI	k1gFnSc2	CuI
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
látka	látka	k1gFnSc1	látka
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
málo	málo	k6eAd1	málo
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
ve	v	k7c6	v
třech	tři	k4xCgFnPc6	tři
modifikacích	modifikace	k1gFnPc6	modifikace
<g/>
,	,	kIx,	,
nejčastější	častý	k2eAgNnSc1d3	nejčastější
je	být	k5eAaImIp3nS	být
však	však	k9	však
ta	ten	k3xDgFnSc1	ten
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
krychlové	krychlový	k2eAgFnSc6d1	krychlová
soustavě	soustava	k1gFnSc6	soustava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výroba	výroba	k1gFnSc1	výroba
==	==	k?	==
</s>
</p>
<p>
<s>
Průmyslově	průmyslově	k6eAd1	průmyslově
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
reakcí	reakce	k1gFnSc7	reakce
elementárního	elementární	k2eAgInSc2d1	elementární
jódu	jód	k1gInSc2	jód
s	s	k7c7	s
mědí	měď	k1gFnSc7	měď
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
rovnice	rovnice	k1gFnSc2	rovnice
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
tuto	tento	k3xDgFnSc4	tento
látku	látka	k1gFnSc4	látka
vyrábět	vyrábět	k5eAaImF	vyrábět
reakcí	reakce	k1gFnPc2	reakce
měďnatých	měďnatý	k2eAgFnPc2d1	měďnatá
solí	sůl	k1gFnPc2	sůl
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
oxidu	oxid	k1gInSc2	oxid
měďného	měďný	k1gMnSc2	měďný
s	s	k7c7	s
kyselinou	kyselina	k1gFnSc7	kyselina
jodovodíkovou	jodovodíkový	k2eAgFnSc7d1	jodovodíková
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Tuto	tento	k3xDgFnSc4	tento
látku	látka	k1gFnSc4	látka
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
v	v	k7c6	v
laboratoři	laboratoř	k1gFnSc6	laboratoř
vytvořit	vytvořit	k5eAaPmF	vytvořit
reakcí	reakce	k1gFnSc7	reakce
síranu	síran	k1gInSc2	síran
měďnatého	měďnatý	k2eAgInSc2d1	měďnatý
a	a	k8xC	a
jodidu	jodid	k1gInSc2	jodid
draselného	draselný	k2eAgInSc2d1	draselný
<g/>
,	,	kIx,	,
vedlejším	vedlejší	k2eAgInSc7d1	vedlejší
produktem	produkt	k1gInSc7	produkt
je	být	k5eAaImIp3nS	být
jod	jod	k1gInSc1	jod
<g/>
.	.	kIx.	.
</s>
<s>
Reakce	reakce	k1gFnSc1	reakce
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
smícháním	smíchání	k1gNnSc7	smíchání
horkých	horký	k2eAgInPc2d1	horký
vodných	vodný	k2eAgInPc2d1	vodný
roztoků	roztok	k1gInPc2	roztok
<g/>
,	,	kIx,	,
meziproduktem	meziprodukt	k1gInSc7	meziprodukt
je	být	k5eAaImIp3nS	být
jodid	jodid	k1gInSc1	jodid
měďnatý	měďnatý	k2eAgInSc1d1	měďnatý
<g/>
.	.	kIx.	.
</s>
<s>
Reakce	reakce	k1gFnSc1	reakce
probíhá	probíhat	k5eAaImIp3nS	probíhat
dle	dle	k7c2	dle
rovnice	rovnice	k1gFnSc2	rovnice
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
==	==	k?	==
Reakce	reakce	k1gFnPc1	reakce
==	==	k?	==
</s>
</p>
<p>
<s>
Při	při	k7c6	při
reakci	reakce	k1gFnSc6	reakce
s	s	k7c7	s
rtutí	rtuť	k1gFnSc7	rtuť
vzniká	vznikat	k5eAaImIp3nS	vznikat
hnědo-černý	hnědo-černý	k2eAgInSc4d1	hnědo-černý
komplex	komplex	k1gInSc4	komplex
<g/>
,	,	kIx,	,
tetrajodoměďnatan	tetrajodoměďnatan	k1gInSc4	tetrajodoměďnatan
rtuťný	rtuťný	k2eAgInSc4d1	rtuťný
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Se	s	k7c7	s
silnými	silný	k2eAgFnPc7d1	silná
kyselinami	kyselina	k1gFnPc7	kyselina
tvoří	tvořit	k5eAaImIp3nP	tvořit
jodovodík	jodovodík	k1gInSc4	jodovodík
a	a	k8xC	a
měďnaté	měďnatý	k2eAgFnPc4d1	měďnatá
soli	sůl	k1gFnPc4	sůl
dané	daný	k2eAgFnSc2d1	daná
kyseliny	kyselina	k1gFnSc2	kyselina
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
reaguje	reagovat	k5eAaBmIp3nS	reagovat
např.	např.	kA	např.
s	s	k7c7	s
kyselinou	kyselina	k1gFnSc7	kyselina
chlorovodíkovou	chlorovodíkový	k2eAgFnSc7d1	chlorovodíková
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
Tato	tento	k3xDgFnSc1	tento
látka	látka	k1gFnSc1	látka
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
na	na	k7c4	na
detekci	detekce	k1gFnSc4	detekce
elementární	elementární	k2eAgFnSc2d1	elementární
rtuti	rtuť	k1gFnSc2	rtuť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
látka	látka	k1gFnSc1	látka
používá	používat	k5eAaImIp3nS	používat
při	při	k7c6	při
organických	organický	k2eAgFnPc6d1	organická
syntézách	syntéza	k1gFnPc6	syntéza
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obdobně	obdobně	k6eAd1	obdobně
jako	jako	k9	jako
jodid	jodid	k1gInSc1	jodid
stříbrný	stříbrný	k1gInSc1	stříbrný
je	být	k5eAaImIp3nS	být
tuto	tento	k3xDgFnSc4	tento
látku	látka	k1gFnSc4	látka
možno	možno	k6eAd1	možno
používat	používat	k5eAaImF	používat
na	na	k7c4	na
vyvolávání	vyvolávání	k1gNnSc4	vyvolávání
deště	dešť	k1gInSc2	dešť
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
látka	látka	k1gFnSc1	látka
totiž	totiž	k9	totiž
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
schopnost	schopnost	k1gFnSc4	schopnost
vzdušné	vzdušný	k2eAgFnSc2d1	vzdušná
vlhkosti	vlhkost	k1gFnSc2	vlhkost
tvořit	tvořit	k5eAaImF	tvořit
kapky	kapka	k1gFnPc4	kapka
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
krystalky	krystalka	k1gFnSc2	krystalka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
jako	jako	k9	jako
katalyzátor	katalyzátor	k1gInSc1	katalyzátor
při	při	k7c6	při
průmyslové	průmyslový	k2eAgFnSc6d1	průmyslová
výrobě	výroba	k1gFnSc6	výroba
nylonu	nylon	k1gInSc2	nylon
<g/>
.	.	kIx.	.
</s>
</p>
