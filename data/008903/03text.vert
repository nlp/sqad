<p>
<s>
Římské	římský	k2eAgFnPc1d1	římská
číslice	číslice	k1gFnPc1	číslice
představují	představovat	k5eAaImIp3nP	představovat
způsob	způsob	k1gInSc4	způsob
zápisu	zápis	k1gInSc2	zápis
čísel	číslo	k1gNnPc2	číslo
pomocí	pomoc	k1gFnPc2	pomoc
několika	několik	k4yIc2	několik
vybraných	vybraný	k2eAgNnPc2d1	vybrané
písmen	písmeno	k1gNnPc2	písmeno
latinské	latinský	k2eAgFnSc2d1	Latinská
abecedy	abeceda	k1gFnSc2	abeceda
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
takzvanou	takzvaný	k2eAgFnSc4d1	takzvaná
nepoziční	poziční	k2eNgFnSc4d1	poziční
číselnou	číselný	k2eAgFnSc4d1	číselná
soustavu	soustava	k1gFnSc4	soustava
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yRgFnPc4	který
je	být	k5eAaImIp3nS	být
charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
hodnota	hodnota	k1gFnSc1	hodnota
číslice	číslice	k1gFnSc1	číslice
je	být	k5eAaImIp3nS	být
daná	daný	k2eAgFnSc1d1	daná
jejím	její	k3xOp3gInSc7	její
symbolem	symbol	k1gInSc7	symbol
a	a	k8xC	a
nezávisí	záviset	k5eNaImIp3nS	záviset
na	na	k7c4	na
její	její	k3xOp3gFnSc4	její
pozici	pozice	k1gFnSc4	pozice
v	v	k7c6	v
zapsaném	zapsaný	k2eAgNnSc6d1	zapsané
čísle	číslo	k1gNnSc6	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
používají	používat	k5eAaImIp3nP	používat
arabské	arabský	k2eAgFnPc4d1	arabská
číslice	číslice	k1gFnPc4	číslice
<g/>
,	,	kIx,	,
římské	římský	k2eAgFnPc4d1	římská
číslice	číslice	k1gFnPc4	číslice
většinou	většina	k1gFnSc7	většina
jen	jen	k6eAd1	jen
ve	v	k7c6	v
specifických	specifický	k2eAgInPc6d1	specifický
případech	případ	k1gInPc6	případ
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
k	k	k7c3	k
označení	označení	k1gNnSc3	označení
pořadí	pořadí	k1gNnSc2	pořadí
kapitoly	kapitola	k1gFnSc2	kapitola
<g/>
,	,	kIx,	,
odstavců	odstavec	k1gInPc2	odstavec
či	či	k8xC	či
panovníků	panovník	k1gMnPc2	panovník
stejného	stejný	k2eAgNnSc2d1	stejné
jména	jméno	k1gNnSc2	jméno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Základní	základní	k2eAgInPc1d1	základní
symboly	symbol	k1gInPc1	symbol
==	==	k?	==
</s>
</p>
<p>
<s>
Základní	základní	k2eAgFnPc4d1	základní
římské	římský	k2eAgFnPc4d1	římská
číslice	číslice	k1gFnPc4	číslice
používané	používaný	k2eAgFnPc4d1	používaná
dnes	dnes	k6eAd1	dnes
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
snazší	snadný	k2eAgNnSc4d2	snazší
zapamatování	zapamatování	k1gNnSc4	zapamatování
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
používat	používat	k5eAaImF	používat
mnemotechnické	mnemotechnický	k2eAgFnPc4d1	mnemotechnická
pomůcky	pomůcka	k1gFnPc4	pomůcka
jako	jako	k8xS	jako
např.	např.	kA	např.
Ivan	Ivan	k1gMnSc1	Ivan
Vedl	vést	k5eAaImAgInS	vést
Xénii	Xénie	k1gFnSc4	Xénie
Lesní	lesní	k2eAgFnSc7d1	lesní
Cestou	cesta	k1gFnSc7	cesta
Do	do	k7c2	do
Města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
Ivan	Ivan	k1gMnSc1	Ivan
<g/>
,	,	kIx,	,
Vašek	Vašek	k1gMnSc1	Vašek
<g/>
,	,	kIx,	,
Xénie	Xénie	k1gFnPc1	Xénie
Lijí	lít	k5eAaImIp3nP	lít
Cín	cín	k1gInSc4	cín
Do	do	k7c2	do
Mumie	mumie	k1gFnSc2	mumie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
první	první	k4xOgNnPc1	první
písmena	písmeno	k1gNnPc1	písmeno
určují	určovat	k5eAaImIp3nP	určovat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
jdou	jít	k5eAaImIp3nP	jít
římské	římský	k2eAgFnPc4d1	římská
číslice	číslice	k1gFnPc4	číslice
po	po	k7c6	po
sobě	sebe	k3xPyFc6	sebe
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Původ	původ	k1gInSc1	původ
symbolů	symbol	k1gInPc2	symbol
===	===	k?	===
</s>
</p>
<p>
<s>
I	i	k9	i
</s>
</p>
<p>
<s>
Římské	římský	k2eAgFnPc1d1	římská
číslice	číslice	k1gFnPc1	číslice
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
přirozenou	přirozený	k2eAgFnSc7d1	přirozená
cestou	cesta	k1gFnSc7	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Římané	Říman	k1gMnPc1	Říman
počítali	počítat	k5eAaImAgMnP	počítat
na	na	k7c6	na
prstech	prst	k1gInPc6	prst
<g/>
.	.	kIx.	.
</s>
<s>
Zápis	zápis	k1gInSc1	zápis
čísel	číslo	k1gNnPc2	číslo
1	[number]	k4	1
<g/>
,	,	kIx,	,
2	[number]	k4	2
a	a	k8xC	a
3	[number]	k4	3
jako	jako	k8xC	jako
I	I	kA	I
<g/>
,	,	kIx,	,
II	II	kA	II
a	a	k8xC	a
III	III	kA	III
graficky	graficky	k6eAd1	graficky
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
prsty	prst	k1gInPc4	prst
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	V	kA	V
a	a	k8xC	a
X	X	kA	X
</s>
</p>
<p>
<s>
Také	také	k9	také
tyto	tento	k3xDgFnPc1	tento
dvě	dva	k4xCgFnPc1	dva
římské	římský	k2eAgFnPc1d1	římská
číslice	číslice	k1gFnPc1	číslice
mají	mít	k5eAaImIp3nP	mít
svůj	svůj	k3xOyFgInSc4	svůj
původ	původ	k1gInSc4	původ
v	v	k7c6	v
lidské	lidský	k2eAgFnSc6d1	lidská
ruce	ruka	k1gFnSc6	ruka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Římská	římský	k2eAgFnSc1d1	římská
číslice	číslice	k1gFnSc1	číslice
V	v	k7c6	v
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vyjádřením	vyjádření	k1gNnSc7	vyjádření
dlaně	dlaň	k1gFnSc2	dlaň
s	s	k7c7	s
pěti	pět	k4xCc7	pět
prsty	prst	k1gInPc7	prst
-	-	kIx~	-
V	V	kA	V
tvoří	tvořit	k5eAaImIp3nS	tvořit
tvar	tvar	k1gInSc4	tvar
mezi	mezi	k7c7	mezi
palcem	palec	k1gInSc7	palec
a	a	k8xC	a
malíčkem	malíček	k1gInSc7	malíček
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Římská	římský	k2eAgFnSc1d1	římská
číslice	číslice	k1gFnSc1	číslice
X	X	kA	X
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
dvě	dva	k4xCgFnPc4	dva
dlaně	dlaň	k1gFnPc4	dlaň
u	u	k7c2	u
sebe	se	k3xPyFc2	se
(	(	kIx(	(
<g/>
10	[number]	k4	10
prstů	prst	k1gInPc2	prst
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
L	L	kA	L
a	a	k8xC	a
C	C	kA	C
</s>
</p>
<p>
<s>
Latinsky	latinsky	k6eAd1	latinsky
sto	sto	k4xCgNnSc1	sto
je	být	k5eAaImIp3nS	být
centum	centum	k1gNnSc1	centum
-	-	kIx~	-
odtud	odtud	k6eAd1	odtud
C.	C.	kA	C.
Padesát	padesát	k4xCc4	padesát
je	být	k5eAaImIp3nS	být
polovina	polovina	k1gFnSc1	polovina
ze	z	k7c2	z
stovky	stovka	k1gFnSc2	stovka
<g/>
.	.	kIx.	.
</s>
<s>
L	L	kA	L
tedy	tedy	k9	tedy
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
"	"	kIx"	"
<g/>
rozpůlením	rozpůlení	k1gNnPc3	rozpůlení
<g/>
"	"	kIx"	"
znaku	znak	k1gInSc2	znak
pro	pro	k7c4	pro
100	[number]	k4	100
(	(	kIx(	(
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
D	D	kA	D
a	a	k8xC	a
M	M	kA	M
</s>
</p>
<p>
<s>
Tisíc	tisíc	k4xCgInSc4	tisíc
je	být	k5eAaImIp3nS	být
latinsky	latinsky	k6eAd1	latinsky
mille	mille	k6eAd1	mille
(	(	kIx(	(
<g/>
odtud	odtud	k6eAd1	odtud
M	M	kA	M
pro	pro	k7c4	pro
1000	[number]	k4	1000
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Znak	znak	k1gInSc1	znak
D	D	kA	D
pro	pro	k7c4	pro
500	[number]	k4	500
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
opět	opět	k6eAd1	opět
grafickým	grafický	k2eAgMnSc7d1	grafický
"	"	kIx"	"
<g/>
půlením	půlení	k1gNnSc7	půlení
<g/>
"	"	kIx"	"
znaku	znak	k1gInSc6	znak
M	M	kA	M
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
svisle	svisle	k6eAd1	svisle
<g/>
.	.	kIx.	.
</s>
<s>
Vznikl	vzniknout	k5eAaPmAgInS	vzniknout
tak	tak	k9	tak
znak	znak	k1gInSc1	znak
podobný	podobný	k2eAgInSc1d1	podobný
písmenu	písmeno	k1gNnSc3	písmeno
D.	D.	kA	D.
</s>
<s>
XXXIII	XXXIII	kA	XXXIII
je	být	k5eAaImIp3nS	být
33	[number]	k4	33
v	v	k7c6	v
římských	římský	k2eAgFnPc6d1	římská
číslicích	číslice	k1gFnPc6	číslice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pravidla	pravidlo	k1gNnPc1	pravidlo
zápisu	zápis	k1gInSc2	zápis
větších	veliký	k2eAgNnPc2d2	veliký
čísel	číslo	k1gNnPc2	číslo
==	==	k?	==
</s>
</p>
<p>
<s>
Spojováním	spojování	k1gNnSc7	spojování
a	a	k8xC	a
opakováním	opakování	k1gNnSc7	opakování
základních	základní	k2eAgInPc2d1	základní
symbolů	symbol	k1gInPc2	symbol
lze	lze	k6eAd1	lze
zapisovat	zapisovat	k5eAaImF	zapisovat
i	i	k9	i
větší	veliký	k2eAgNnPc4d2	veliký
čísla	číslo	k1gNnPc4	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgFnPc1d2	veliký
číslice	číslice	k1gFnPc1	číslice
vždy	vždy	k6eAd1	vždy
předcházejí	předcházet	k5eAaImIp3nP	předcházet
menší	malý	k2eAgFnPc1d2	menší
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
např.	např.	kA	např.
VI	VI	kA	VI
je	být	k5eAaImIp3nS	být
6	[number]	k4	6
<g/>
,	,	kIx,	,
CLXXIII	CLXXIII	kA	CLXXIII
je	být	k5eAaImIp3nS	být
173	[number]	k4	173
a	a	k8xC	a
MDCCCXXII	MDCCCXXII	kA	MDCCCXXII
je	být	k5eAaImIp3nS	být
1822	[number]	k4	1822
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
IIII	IIII	kA	IIII
nebo	nebo	k8xC	nebo
IV	IV	kA	IV
===	===	k?	===
</s>
</p>
<p>
<s>
Římané	Říman	k1gMnPc1	Říman
obvykle	obvykle	k6eAd1	obvykle
psali	psát	k5eAaImAgMnP	psát
číslo	číslo	k1gNnSc4	číslo
4	[number]	k4	4
jako	jako	k8xC	jako
IIII	IIII	kA	IIII
<g/>
,	,	kIx,	,
číslo	číslo	k1gNnSc1	číslo
40	[number]	k4	40
jako	jako	k8xS	jako
XXXX	XXXX	kA	XXXX
<g/>
,	,	kIx,	,
číslo	číslo	k1gNnSc1	číslo
999	[number]	k4	999
jako	jako	k8xS	jako
DCCCCLXXXXVIIII	DCCCCLXXXXVIIII	kA	DCCCCLXXXXVIIII
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ke	k	k7c3	k
zkrácení	zkrácení	k1gNnSc3	zkrácení
zápisu	zápis	k1gInSc2	zápis
takových	takový	k3xDgNnPc2	takový
dlouhých	dlouhý	k2eAgNnPc2d1	dlouhé
čísel	číslo	k1gNnPc2	číslo
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
používalo	používat	k5eAaImAgNnS	používat
zvláštního	zvláštní	k2eAgNnSc2d1	zvláštní
pravidla	pravidlo	k1gNnSc2	pravidlo
pro	pro	k7c4	pro
odečítání	odečítání	k1gNnSc4	odečítání
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
teprve	teprve	k6eAd1	teprve
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
se	se	k3xPyFc4	se
toto	tento	k3xDgNnSc1	tento
pravidlo	pravidlo	k1gNnSc1	pravidlo
stalo	stát	k5eAaPmAgNnS	stát
obecně	obecně	k6eAd1	obecně
používaným	používaný	k2eAgNnSc7d1	používané
<g/>
.	.	kIx.	.
</s>
<s>
Pravidlo	pravidlo	k1gNnSc1	pravidlo
pro	pro	k7c4	pro
odečítání	odečítání	k1gNnSc4	odečítání
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
použití	použití	k1gNnSc4	použití
šesti	šest	k4xCc2	šest
složených	složený	k2eAgInPc2d1	složený
symbolů	symbol	k1gInPc2	symbol
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgMnPc6	který
menší	malý	k2eAgFnSc1d2	menší
číslice	číslice	k1gFnSc1	číslice
předchází	předcházet	k5eAaImIp3nS	předcházet
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
IV	IV	kA	IV
=	=	kIx~	=
4	[number]	k4	4
</s>
</p>
<p>
<s>
IX	IX	kA	IX
=	=	kIx~	=
9	[number]	k4	9
</s>
</p>
<p>
<s>
XL	XL	kA	XL
=	=	kIx~	=
40	[number]	k4	40
</s>
</p>
<p>
<s>
XC	XC	kA	XC
=	=	kIx~	=
90	[number]	k4	90
</s>
</p>
<p>
<s>
CD	CD	kA	CD
=	=	kIx~	=
400	[number]	k4	400
</s>
</p>
<p>
<s>
CM	cm	kA	cm
=	=	kIx~	=
900	[number]	k4	900
<g/>
Při	při	k7c6	při
použití	použití	k1gNnSc6	použití
tohoto	tento	k3xDgNnSc2	tento
pravidla	pravidlo	k1gNnSc2	pravidlo
lze	lze	k6eAd1	lze
číslo	číslo	k1gNnSc4	číslo
999	[number]	k4	999
napsat	napsat	k5eAaBmF	napsat
úspornějším	úsporný	k2eAgInSc7d2	úspornější
způsobem	způsob	k1gInSc7	způsob
CMXCIX	CMXCIX	kA	CMXCIX
<g/>
.	.	kIx.	.
</s>
<s>
Používání	používání	k1gNnSc1	používání
jiných	jiný	k2eAgInPc2d1	jiný
symbolů	symbol	k1gInPc2	symbol
není	být	k5eNaImIp3nS	být
dovoleno	dovolit	k5eAaPmNgNnS	dovolit
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
nelze	lze	k6eNd1	lze
napsat	napsat	k5eAaBmF	napsat
999	[number]	k4	999
jako	jako	k8xS	jako
IM	IM	kA	IM
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
ale	ale	k8xC	ale
používání	používání	k1gNnSc1	používání
tohoto	tento	k3xDgNnSc2	tento
pravidla	pravidlo	k1gNnSc2	pravidlo
není	být	k5eNaImIp3nS	být
povinné	povinný	k2eAgNnSc1d1	povinné
<g/>
.	.	kIx.	.
</s>
<s>
Číslici	číslice	k1gFnSc4	číslice
4	[number]	k4	4
lze	lze	k6eAd1	lze
napsat	napsat	k5eAaBmF	napsat
správně	správně	k6eAd1	správně
jako	jako	k9	jako
IV	IV	kA	IV
i	i	k8xC	i
jako	jako	k8xC	jako
IIII	IIII	kA	IIII
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zavedení	zavedení	k1gNnSc1	zavedení
pravidla	pravidlo	k1gNnSc2	pravidlo
pro	pro	k7c4	pro
odečítání	odečítání	k1gNnSc4	odečítání
pomohlo	pomoct	k5eAaPmAgNnS	pomoct
zkrátit	zkrátit	k5eAaPmF	zkrátit
zápis	zápis	k1gInSc4	zápis
větších	veliký	k2eAgNnPc2d2	veliký
čísel	číslo	k1gNnPc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
ještě	ještě	k9	ještě
více	hodně	k6eAd2	hodně
ztěžovalo	ztěžovat	k5eAaImAgNnS	ztěžovat
provádění	provádění	k1gNnSc4	provádění
výpočtů	výpočet	k1gInPc2	výpočet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Historický	historický	k2eAgInSc1d1	historický
zápis	zápis	k1gInSc1	zápis
velkých	velký	k2eAgNnPc2d1	velké
čísel	číslo	k1gNnPc2	číslo
===	===	k?	===
</s>
</p>
<p>
<s>
Pozor	pozor	k1gInSc1	pozor
<g/>
.	.	kIx.	.
</s>
<s>
Symboly	symbol	k1gInPc1	symbol
D	D	kA	D
(	(	kIx(	(
<g/>
500	[number]	k4	500
<g/>
)	)	kIx)	)
a	a	k8xC	a
M	M	kA	M
(	(	kIx(	(
<g/>
1000	[number]	k4	1000
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgInP	být
původně	původně	k6eAd1	původně
psány	psán	k2eAgInPc1d1	psán
jako	jako	k8xS	jako
kolmá	kolmý	k2eAgFnSc1d1	kolmá
čára	čára	k1gFnSc1	čára
s	s	k7c7	s
obloučkem	oblouček	k1gInSc7	oblouček
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
nebo	nebo	k8xC	nebo
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
Symbol	symbol	k1gInSc1	symbol
D	D	kA	D
proto	proto	k8xC	proto
vypadal	vypadat	k5eAaImAgInS	vypadat
asi	asi	k9	asi
jako	jako	k9	jako
|	|	kIx~	|
<g/>
)	)	kIx)	)
a	a	k8xC	a
symbol	symbol	k1gInSc1	symbol
M	M	kA	M
byl	být	k5eAaImAgInS	být
ↀ	ↀ	k?	ↀ
Tento	tento	k3xDgInSc4	tento
způsob	způsob	k1gInSc4	způsob
zápisu	zápis	k1gInSc2	zápis
umožňoval	umožňovat	k5eAaImAgInS	umožňovat
násobit	násobit	k5eAaImF	násobit
deseti	deset	k4xCc7	deset
čísla	číslo	k1gNnSc2	číslo
větší	veliký	k2eAgFnSc4d2	veliký
než	než	k8xS	než
tisíc	tisíc	k4xCgInSc4	tisíc
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
přidal	přidat	k5eAaPmAgInS	přidat
další	další	k2eAgInSc1d1	další
oblouček	oblouček	k1gInSc1	oblouček
<g/>
.	.	kIx.	.
</s>
<s>
Číslo	číslo	k1gNnSc1	číslo
10000	[number]	k4	10000
se	se	k3xPyFc4	se
dalo	dát	k5eAaPmAgNnS	dát
napsat	napsat	k5eAaPmF	napsat
jako	jako	k9	jako
((	((	k?	((
<g/>
|	|	kIx~	|
<g/>
))	))	k?	))
<g/>
,	,	kIx,	,
100000	[number]	k4	100000
(((	(((	k?	(((
<g/>
|	|	kIx~	|
<g/>
)))	)))	k?	)))
<g/>
.	.	kIx.	.
</s>
<s>
Římané	Říman	k1gMnPc1	Říman
neměli	mít	k5eNaImAgMnP	mít
žádné	žádný	k3yNgNnSc4	žádný
slovo	slovo	k1gNnSc4	slovo
pro	pro	k7c4	pro
milión	milión	k4xCgInSc4	milión
a	a	k8xC	a
takto	takto	k6eAd1	takto
velká	velký	k2eAgNnPc4d1	velké
čísla	číslo	k1gNnPc4	číslo
používali	používat	k5eAaImAgMnP	používat
velmi	velmi	k6eAd1	velmi
zřídka	zřídka	k6eAd1	zřídka
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
později	pozdě	k6eAd2	pozdě
a	a	k8xC	a
zvlášť	zvlášť	k6eAd1	zvlášť
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
bylo	být	k5eAaImAgNnS	být
nutné	nutný	k2eAgNnSc1d1	nutné
zapisovat	zapisovat	k5eAaImF	zapisovat
i	i	k9	i
větší	veliký	k2eAgNnPc4d2	veliký
čísla	číslo	k1gNnPc4	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
znak	znak	k1gInSc1	znak
X	X	kA	X
číslem	číslo	k1gNnSc7	číslo
10	[number]	k4	10
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
C	C	kA	C
číslem	číslo	k1gNnSc7	číslo
100	[number]	k4	100
000	[number]	k4	000
a	a	k8xC	a
M	M	kA	M
číslem	číslo	k1gNnSc7	číslo
1	[number]	k4	1
000	[number]	k4	000
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
čárkované	čárkovaný	k2eAgInPc1d1	čárkovaný
symboly	symbol	k1gInPc1	symbol
se	se	k3xPyFc4	se
ale	ale	k9	ale
dnes	dnes	k6eAd1	dnes
prakticky	prakticky	k6eAd1	prakticky
nevyskytují	vyskytovat	k5eNaImIp3nP	vyskytovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Tabulka	tabulka	k1gFnSc1	tabulka
římských	římský	k2eAgNnPc2d1	římské
čísel	číslo	k1gNnPc2	číslo
===	===	k?	===
</s>
</p>
<p>
<s>
Při	při	k7c6	při
správném	správný	k2eAgInSc6d1	správný
způsobu	způsob	k1gInSc6	způsob
psaní	psaní	k1gNnSc2	psaní
dlouhých	dlouhý	k2eAgNnPc2d1	dlouhé
čísel	číslo	k1gNnPc2	číslo
se	se	k3xPyFc4	se
nejprve	nejprve	k6eAd1	nejprve
napíší	napsat	k5eAaPmIp3nP	napsat
tisíce	tisíc	k4xCgInPc1	tisíc
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
stovky	stovka	k1gFnPc1	stovka
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
desítky	desítka	k1gFnPc4	desítka
a	a	k8xC	a
pak	pak	k6eAd1	pak
jednotky	jednotka	k1gFnPc1	jednotka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příklad	příklad	k1gInSc1	příklad
<g/>
:	:	kIx,	:
číslo	číslo	k1gNnSc1	číslo
1988	[number]	k4	1988
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeden	jeden	k4xCgInSc1	jeden
tisíc	tisíc	k4xCgInSc1	tisíc
je	být	k5eAaImIp3nS	být
M	M	kA	M
<g/>
,	,	kIx,	,
devět	devět	k4xCc4	devět
set	set	k1gInSc4	set
je	být	k5eAaImIp3nS	být
CM	cm	kA	cm
<g/>
,	,	kIx,	,
osmdesát	osmdesát	k4xCc4	osmdesát
je	být	k5eAaImIp3nS	být
LXXX	LXXX	kA	LXXX
<g/>
,	,	kIx,	,
a	a	k8xC	a
osm	osm	k4xCc4	osm
je	být	k5eAaImIp3nS	být
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dohromady	dohromady	k6eAd1	dohromady
pak	pak	k6eAd1	pak
<g/>
:	:	kIx,	:
MCMLXXXVIII	MCMLXXXVIII	kA	MCMLXXXVIII
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Použití	použití	k1gNnSc1	použití
malých	malý	k2eAgNnPc2d1	malé
písmen	písmeno	k1gNnPc2	písmeno
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
římských	římský	k2eAgFnPc6d1	římská
dobách	doba	k1gFnPc6	doba
se	se	k3xPyFc4	se
používala	používat	k5eAaImAgFnS	používat
k	k	k7c3	k
zápisu	zápis	k1gInSc3	zápis
pouze	pouze	k6eAd1	pouze
velká	velký	k2eAgNnPc4d1	velké
písmena	písmeno	k1gNnPc4	písmeno
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
</s>
</p>
<p>
<s>
v	v	k7c6	v
běžném	běžný	k2eAgInSc6d1	běžný
životě	život	k1gInSc6	život
prosadila	prosadit	k5eAaPmAgFnS	prosadit
i	i	k9	i
malá	malý	k2eAgNnPc4d1	malé
písmena	písmeno	k1gNnPc4	písmeno
a	a	k8xC	a
začala	začít	k5eAaPmAgNnP	začít
používat	používat	k5eAaImF	používat
i	i	k9	i
pro	pro	k7c4	pro
zápis	zápis	k1gInSc4	zápis
</s>
</p>
<p>
<s>
římských	římský	k2eAgFnPc2d1	římská
číslic	číslice	k1gFnPc2	číslice
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
např.	např.	kA	např.
vi	vi	k?	vi
znamená	znamenat	k5eAaImIp3nS	znamenat
6	[number]	k4	6
a	a	k8xC	a
cxiv	cxiv	k1gInSc1	cxiv
114	[number]	k4	114
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
můžeme	moct	k5eAaImIp1nP	moct
</s>
</p>
<p>
<s>
setkat	setkat	k5eAaPmF	setkat
i	i	k9	i
s	s	k7c7	s
čísly	číslo	k1gNnPc7	číslo
namíchanými	namíchaný	k2eAgFnPc7d1	namíchaná
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Cxl	Cxl	k1gFnSc2	Cxl
pro	pro	k7c4	pro
140	[number]	k4	140
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
používá	používat	k5eAaImIp3nS	používat
</s>
</p>
<p>
<s>
namísto	namísto	k7c2	namísto
malého	malý	k2eAgInSc2d1	malý
i	i	k9	i
písmeno	písmeno	k1gNnSc1	písmeno
j.	j.	k?	j.
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
písmeno	písmeno	k1gNnSc1	písmeno
j	j	k?	j
pouze	pouze	k6eAd1	pouze
jako	jako	k8xS	jako
poslední	poslední	k2eAgInSc1d1	poslední
znak	znak	k1gInSc1	znak
ve	v	k7c6	v
sledu	sled	k1gInSc6	sled
jedniček	jednička	k1gFnPc2	jednička
<g/>
,	,	kIx,	,
např.	např.	kA	např.
xxiij	xxiít	k5eAaPmRp2nS	xxiít
pro	pro	k7c4	pro
23	[number]	k4	23
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nula	nula	k1gFnSc1	nula
==	==	k?	==
</s>
</p>
<p>
<s>
Římské	římský	k2eAgFnPc1d1	římská
číslice	číslice	k1gFnPc1	číslice
neobsahují	obsahovat	k5eNaImIp3nP	obsahovat
symbol	symbol	k1gInSc4	symbol
pro	pro	k7c4	pro
nulu	nula	k1gFnSc4	nula
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k6eAd1	právě
absence	absence	k1gFnSc1	absence
tohoto	tento	k3xDgInSc2	tento
symbolu	symbol	k1gInSc2	symbol
zabránila	zabránit	k5eAaPmAgFnS	zabránit
postupné	postupný	k2eAgFnSc3d1	postupná
přeměně	přeměna	k1gFnSc3	přeměna
římského	římský	k2eAgInSc2d1	římský
zápisu	zápis	k1gInSc2	zápis
na	na	k7c4	na
poziční	poziční	k2eAgInSc4d1	poziční
systém	systém	k1gInSc4	systém
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
byl	být	k5eAaImAgInS	být
během	během	k7c2	během
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
v	v	k7c6	v
praktickém	praktický	k2eAgInSc6d1	praktický
životě	život	k1gInSc6	život
nahrazen	nahradit	k5eAaPmNgInS	nahradit
arabskými	arabský	k2eAgFnPc7d1	arabská
číslicemi	číslice	k1gFnPc7	číslice
<g/>
.	.	kIx.	.
</s>
<s>
Používání	používání	k1gNnSc1	používání
římských	římský	k2eAgFnPc2d1	římská
číslic	číslice	k1gFnPc2	číslice
v	v	k7c6	v
matematice	matematika	k1gFnSc6	matematika
není	být	k5eNaImIp3nS	být
tak	tak	k6eAd1	tak
výhodné	výhodný	k2eAgNnSc1d1	výhodné
jako	jako	k8xS	jako
používání	používání	k1gNnSc1	používání
pozičních	poziční	k2eAgFnPc2d1	poziční
číselných	číselný	k2eAgFnPc2d1	číselná
soustav	soustava	k1gFnPc2	soustava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vývoj	vývoj	k1gInSc1	vývoj
používání	používání	k1gNnSc2	používání
římských	římský	k2eAgNnPc2d1	římské
čísel	číslo	k1gNnPc2	číslo
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Historie	historie	k1gFnSc1	historie
===	===	k?	===
</s>
</p>
<p>
<s>
Dnešní	dnešní	k2eAgInSc1d1	dnešní
způsob	způsob	k1gInSc1	způsob
zapisování	zapisování	k1gNnSc2	zapisování
římských	římský	k2eAgFnPc2d1	římská
číslic	číslice	k1gFnPc2	číslice
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
ze	z	k7c2	z
způsobu	způsob	k1gInSc2	způsob
<g/>
,	,	kIx,	,
jakým	jaký	k3yRgNnSc7	jaký
zapisovali	zapisovat	k5eAaImAgMnP	zapisovat
číslice	číslice	k1gFnPc4	číslice
staří	starý	k2eAgMnPc1d1	starý
Římané	Říman	k1gMnPc1	Říman
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
některá	některý	k3yIgNnPc4	některý
vylepšení	vylepšení	k1gNnPc4	vylepšení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
zvykem	zvyk	k1gInSc7	zvyk
označovat	označovat	k5eAaImF	označovat
vydání	vydání	k1gNnSc4	vydání
knih	kniha	k1gFnPc2	kniha
římskými	římský	k2eAgFnPc7d1	římská
číslicemi	číslice	k1gFnPc7	číslice
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
římské	římský	k2eAgFnPc1d1	římská
číslice	číslice	k1gFnPc1	číslice
používaly	používat	k5eAaImAgFnP	používat
k	k	k7c3	k
označení	označení	k1gNnSc3	označení
data	datum	k1gNnSc2	datum
staveb	stavba	k1gFnPc2	stavba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Římské	římský	k2eAgFnPc4d1	římská
číslice	číslice	k1gFnPc4	číslice
dnes	dnes	k6eAd1	dnes
===	===	k?	===
</s>
</p>
<p>
<s>
Římské	římský	k2eAgFnPc1d1	římská
číslice	číslice	k1gFnPc1	číslice
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
dodnes	dodnes	k6eAd1	dodnes
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
ústupu	ústup	k1gInSc6	ústup
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
prostředí	prostředí	k1gNnSc6	prostředí
se	se	k3xPyFc4	se
jimi	on	k3xPp3gMnPc7	on
číslují	číslovat	k5eAaImIp3nP	číslovat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
díly	díl	k1gInPc1	díl
čí	čí	k3xOyQgFnSc2	čí
série	série	k1gFnSc2	série
knih	kniha	k1gFnPc2	kniha
či	či	k8xC	či
filmů	film	k1gInPc2	film
(	(	kIx(	(
<g/>
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
významu	význam	k1gInSc6	význam
jsou	být	k5eAaImIp3nP	být
vytlačovány	vytlačovat	k5eAaImNgInP	vytlačovat
arabskými	arabský	k2eAgFnPc7d1	arabská
číslicemi	číslice	k1gFnPc7	číslice
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ročníky	ročník	k1gInPc1	ročník
časopisů	časopis	k1gInPc2	časopis
či	či	k8xC	či
novin	novina	k1gFnPc2	novina
(	(	kIx(	(
<g/>
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
významu	význam	k1gInSc6	význam
jsou	být	k5eAaImIp3nP	být
vytlačovány	vytlačovat	k5eAaImNgInP	vytlačovat
arabskými	arabský	k2eAgFnPc7d1	arabská
číslicemi	číslice	k1gFnPc7	číslice
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
kapitoly	kapitola	k1gFnPc1	kapitola
v	v	k7c6	v
knihách	kniha	k1gFnPc6	kniha
či	či	k8xC	či
článcích	článek	k1gInPc6	článek
(	(	kIx(	(
<g/>
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
významu	význam	k1gInSc6	význam
jsou	být	k5eAaImIp3nP	být
vytlačovány	vytlačovat	k5eAaImNgInP	vytlačovat
arabskými	arabský	k2eAgFnPc7d1	arabská
číslicemi	číslice	k1gFnPc7	číslice
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
opakované	opakovaný	k2eAgFnPc1d1	opakovaná
společenské	společenský	k2eAgFnPc1d1	společenská
<g/>
,	,	kIx,	,
sportovní	sportovní	k2eAgFnPc1d1	sportovní
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
události	událost	k1gFnPc1	událost
(	(	kIx(	(
<g/>
soutěže	soutěž	k1gFnPc1	soutěž
<g/>
,	,	kIx,	,
závody	závod	k1gInPc1	závod
<g/>
,	,	kIx,	,
přehlídky	přehlídka	k1gFnPc1	přehlídka
<g/>
,	,	kIx,	,
sjezdy	sjezd	k1gInPc1	sjezd
a	a	k8xC	a
slety	slet	k1gInPc1	slet
<g/>
,	,	kIx,	,
války	válka	k1gFnPc1	válka
<g/>
,	,	kIx,	,
festivaly	festival	k1gInPc1	festival
<g/>
,	,	kIx,	,
veletrhy	veletrh	k1gInPc1	veletrh
<g/>
,	,	kIx,	,
výstavy	výstava	k1gFnPc1	výstava
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
však	však	k9	však
již	již	k6eAd1	již
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
začínají	začínat	k5eAaImIp3nP	začínat
převládat	převládat	k5eAaImF	převládat
arabské	arabský	k2eAgFnPc1d1	arabská
číslice	číslice	k1gFnPc1	číslice
</s>
</p>
<p>
<s>
typové	typový	k2eAgFnPc1d1	typová
řady	řada	k1gFnPc1	řada
či	či	k8xC	či
modely	model	k1gInPc1	model
výrobků	výrobek	k1gInPc2	výrobek
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
generace	generace	k1gFnSc1	generace
rallye	rallye	k1gFnSc2	rallye
vozů	vůz	k1gInPc2	vůz
Mitsubishi	mitsubishi	k1gNnSc7	mitsubishi
Lancer	Lancer	k1gInSc1	Lancer
Evolution	Evolution	k1gInSc1	Evolution
<g/>
,	,	kIx,	,
též	též	k9	též
například	například	k6eAd1	například
tramvaj	tramvaj	k1gFnSc1	tramvaj
Tatra	Tatra	k1gFnSc1	Tatra
T3	T3	k1gFnSc1	T3
byla	být	k5eAaImAgFnS	být
původně	původně	k6eAd1	původně
označovaná	označovaný	k2eAgFnSc1d1	označovaná
jako	jako	k8xS	jako
Tatra	Tatra	k1gFnSc1	Tatra
T	T	kA	T
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
rok	rok	k1gInSc1	rok
(	(	kIx(	(
<g/>
prakticky	prakticky	k6eAd1	prakticky
jen	jen	k9	jen
na	na	k7c6	na
historických	historický	k2eAgInPc6d1	historický
nápisech	nápis	k1gInPc6	nápis
a	a	k8xC	a
v	v	k7c6	v
titulcích	titulek	k1gInPc6	titulek
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
se	se	k3xPyFc4	se
v	v	k7c6	v
moderních	moderní	k2eAgInPc6d1	moderní
textech	text	k1gInPc6	text
používají	používat	k5eAaImIp3nP	používat
výhradně	výhradně	k6eAd1	výhradně
arabské	arabský	k2eAgFnPc1d1	arabská
číslice	číslice	k1gFnPc1	číslice
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
století	století	k1gNnSc1	století
(	(	kIx(	(
<g/>
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
významu	význam	k1gInSc6	význam
jsou	být	k5eAaImIp3nP	být
vytlačovány	vytlačovat	k5eAaImNgInP	vytlačovat
arabskými	arabský	k2eAgFnPc7d1	arabská
číslicemi	číslice	k1gFnPc7	číslice
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
měsíce	měsíc	k1gInPc4	měsíc
v	v	k7c6	v
roce	rok	k1gInSc6	rok
(	(	kIx(	(
<g/>
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
významu	význam	k1gInSc6	význam
jsou	být	k5eAaImIp3nP	být
vytlačovány	vytlačovat	k5eAaImNgInP	vytlačovat
arabskými	arabský	k2eAgFnPc7d1	arabská
číslicemi	číslice	k1gFnPc7	číslice
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
pořadové	pořadový	k2eAgNnSc4d1	pořadové
číslo	číslo	k1gNnSc4	číslo
čtvrtletí	čtvrtletí	k1gNnSc2	čtvrtletí
(	(	kIx(	(
<g/>
používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
ale	ale	k9	ale
i	i	k9	i
arabské	arabský	k2eAgFnPc1d1	arabská
číslice	číslice	k1gFnPc1	číslice
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
pro	pro	k7c4	pro
číslice	číslice	k1gFnPc4	číslice
na	na	k7c6	na
hodinovém	hodinový	k2eAgInSc6d1	hodinový
ciferníku	ciferník	k1gInSc6	ciferník
</s>
</p>
<p>
<s>
pořadová	pořadový	k2eAgNnPc4d1	pořadové
čísla	číslo	k1gNnPc4	číslo
panovníků	panovník	k1gMnPc2	panovník
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
účel	účel	k1gInSc4	účel
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
používají	používat	k5eAaImIp3nP	používat
takřka	takřka	k6eAd1	takřka
výhradně	výhradně	k6eAd1	výhradně
římské	římský	k2eAgFnPc4d1	římská
číslice	číslice	k1gFnPc4	číslice
</s>
</p>
<p>
<s>
čísla	číslo	k1gNnPc1	číslo
místních	místní	k2eAgFnPc2d1	místní
částí	část	k1gFnPc2	část
obce	obec	k1gFnSc2	obec
–	–	k?	–
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
tvořilo	tvořit	k5eAaImAgNnS	tvořit
římské	římský	k2eAgNnSc1d1	římské
číslo	číslo	k1gNnSc1	číslo
místní	místní	k2eAgFnSc2d1	místní
části	část	k1gFnSc2	část
dvojici	dvojice	k1gFnSc4	dvojice
s	s	k7c7	s
číslem	číslo	k1gNnSc7	číslo
popisným	popisný	k2eAgNnSc7d1	popisné
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
se	se	k3xPyFc4	se
místo	místo	k7c2	místo
něj	on	k3xPp3gMnSc2	on
běžně	běžně	k6eAd1	běžně
používá	používat	k5eAaImIp3nS	používat
název	název	k1gInSc1	název
místní	místní	k2eAgFnSc2d1	místní
části	část	k1gFnSc2	část
<g/>
;	;	kIx,	;
označení	označení	k1gNnSc1	označení
čtvrtí	čtvrt	k1gFnPc2	čtvrt
římskými	římský	k2eAgNnPc7d1	římské
čísly	číslo	k1gNnPc7	číslo
přežívá	přežívat	k5eAaImIp3nS	přežívat
jen	jen	k9	jen
v	v	k7c6	v
některých	některý	k3yIgNnPc6	některý
městech	město	k1gNnPc6	město
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Děčíně	Děčín	k1gInSc6	Děčín
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
v	v	k7c6	v
Pardubicích	Pardubice	k1gInPc6	Pardubice
či	či	k8xC	či
Mladé	mladý	k2eAgFnSc6d1	mladá
Boleslavi	Boleslaev	k1gFnSc6	Boleslaev
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
třída	třída	k1gFnSc1	třída
silnic	silnice	k1gFnPc2	silnice
(	(	kIx(	(
<g/>
I	I	kA	I
<g/>
,	,	kIx,	,
II	II	kA	II
<g/>
,	,	kIx,	,
III	III	kA	III
<g/>
)	)	kIx)	)
–	–	k?	–
např.	např.	kA	např.
"	"	kIx"	"
<g/>
silnice	silnice	k1gFnSc1	silnice
I.	I.	kA	I.
třídy	třída	k1gFnSc2	třída
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
silnice	silnice	k1gFnSc1	silnice
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
201	[number]	k4	201
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
třída	třída	k1gFnSc1	třída
místních	místní	k2eAgFnPc2d1	místní
komunikací	komunikace	k1gFnPc2	komunikace
(	(	kIx(	(
<g/>
I	I	kA	I
<g/>
,	,	kIx,	,
II	II	kA	II
<g/>
,	,	kIx,	,
III	III	kA	III
<g/>
,	,	kIx,	,
IV	IV	kA	IV
<g/>
,	,	kIx,	,
v	v	k7c6	v
evidenčním	evidenční	k2eAgNnSc6d1	evidenční
značení	značení	k1gNnSc6	značení
se	se	k3xPyFc4	se
však	však	k9	však
používají	používat	k5eAaImIp3nP	používat
písmena	písmeno	k1gNnPc4	písmeno
a	a	k8xC	a
<g/>
,	,	kIx,	,
b	b	k?	b
<g/>
,	,	kIx,	,
c	c	k0	c
<g/>
,	,	kIx,	,
d	d	k?	d
<g/>
,	,	kIx,	,
např.	např.	kA	např.
29	[number]	k4	29
<g/>
a	a	k8xC	a
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Kategorie	kategorie	k1gFnSc2	kategorie
pozemních	pozemní	k2eAgFnPc2d1	pozemní
komunikací	komunikace	k1gFnPc2	komunikace
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
souběžné	souběžný	k2eAgFnPc1d1	souběžná
ulice	ulice	k1gFnPc1	ulice
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jinak	jinak	k6eAd1	jinak
mají	mít	k5eAaImIp3nP	mít
stejný	stejný	k2eAgInSc4d1	stejný
název	název	k1gInSc4	název
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
některých	některý	k3yIgNnPc6	některý
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
názvosloví	názvosloví	k1gNnSc1	názvosloví
inspirováno	inspirovat	k5eAaBmNgNnS	inspirovat
americkými	americký	k2eAgInPc7d1	americký
systémy	systém	k1gInPc7	systém
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
na	na	k7c6	na
pražském	pražský	k2eAgNnSc6d1	Pražské
Spořilově	spořilův	k2eAgNnSc6d1	spořilův
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
díly	díl	k1gInPc1	díl
či	či	k8xC	či
části	část	k1gFnPc1	část
obcí	obec	k1gFnPc2	obec
či	či	k8xC	či
katastrálních	katastrální	k2eAgNnPc2d1	katastrální
území	území	k1gNnPc2	území
či	či	k8xC	či
obce	obec	k1gFnSc2	obec
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
by	by	kYmCp3nP	by
jinak	jinak	k6eAd1	jinak
měly	mít	k5eAaImAgFnP	mít
stejný	stejný	k2eAgInSc4d1	stejný
název	název	k1gInSc4	název
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
nejsou	být	k5eNaImIp3nP	být
rozlišeny	rozlišit	k5eAaPmNgFnP	rozlišit
slovním	slovní	k2eAgInSc7d1	slovní
přívlastkem	přívlastek	k1gInSc7	přívlastek
</s>
</p>
<p>
<s>
v	v	k7c6	v
českých	český	k2eAgInPc6d1	český
právních	právní	k2eAgInPc6d1	právní
předpisech	předpis	k1gInPc6	předpis
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
aktuálních	aktuální	k2eAgNnPc2d1	aktuální
legislativních	legislativní	k2eAgNnPc2d1	legislativní
pravidel	pravidlo	k1gNnPc2	pravidlo
vlády	vláda	k1gFnSc2	vláda
římskými	římský	k2eAgNnPc7d1	římské
čísly	číslo	k1gNnPc7	číslo
bez	bez	k7c2	bez
tečky	tečka	k1gFnSc2	tečka
označují	označovat	k5eAaImIp3nP	označovat
varianty	varianta	k1gFnPc1	varianta
návrhů	návrh	k1gInPc2	návrh
<g/>
,	,	kIx,	,
čísla	číslo	k1gNnSc2	číslo
článků	článek	k1gInPc2	článek
v	v	k7c6	v
novelách	novela	k1gFnPc6	novela
předpisů	předpis	k1gInPc2	předpis
a	a	k8xC	a
hlavy	hlava	k1gFnSc2	hlava
(	(	kIx(	(
<g/>
celek	celek	k1gInSc1	celek
nižší	nízký	k2eAgInSc1d2	nižší
než	než	k8xS	než
část	část	k1gFnSc1	část
a	a	k8xC	a
vyšší	vysoký	k2eAgFnSc1d2	vyšší
než	než	k8xS	než
díl	díl	k1gInSc1	díl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
oxidační	oxidační	k2eAgNnPc1d1	oxidační
čísla	číslo	k1gNnPc1	číslo
prvkůV	prvkůV	k?	prvkůV
polštině	polština	k1gFnSc6	polština
se	se	k3xPyFc4	se
římské	římský	k2eAgFnSc2d1	římská
číslice	číslice	k1gFnSc2	číslice
používají	používat	k5eAaImIp3nP	používat
takřka	takřka	k6eAd1	takřka
výhradně	výhradně	k6eAd1	výhradně
při	při	k7c6	při
uvádění	uvádění	k1gNnSc6	uvádění
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
např.	např.	kA	např.
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
je	být	k5eAaImIp3nS	být
XIX	XIX	kA	XIX
wiek	wiek	k1gInSc1	wiek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
mezinárodních	mezinárodní	k2eAgFnPc6d1	mezinárodní
normách	norma	k1gFnPc6	norma
a	a	k8xC	a
předpisech	předpis	k1gInPc6	předpis
(	(	kIx(	(
<g/>
zpravidla	zpravidla	k6eAd1	zpravidla
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
<g/>
,	,	kIx,	,
francouzštině	francouzština	k1gFnSc6	francouzština
či	či	k8xC	či
španělštině	španělština	k1gFnSc6	španělština
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
římské	římský	k2eAgFnSc2d1	římská
číslice	číslice	k1gFnSc2	číslice
zapisované	zapisovaný	k2eAgFnSc2d1	zapisovaná
malými	malý	k2eAgInPc7d1	malý
písmeny	písmeno	k1gNnPc7	písmeno
(	(	kIx(	(
<g/>
i	i	k9	i
<g/>
,	,	kIx,	,
ii	ii	k?	ii
<g/>
,	,	kIx,	,
iii	iii	k?	iii
<g/>
,	,	kIx,	,
iv	iv	k?	iv
<g/>
,	,	kIx,	,
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
používají	používat	k5eAaImIp3nP	používat
</s>
</p>
<p>
<s>
k	k	k7c3	k
číslování	číslování	k1gNnSc3	číslování
úvodních	úvodní	k2eAgFnPc2d1	úvodní
stran	strana	k1gFnPc2	strana
(	(	kIx(	(
<g/>
předmluva	předmluva	k1gFnSc1	předmluva
<g/>
,	,	kIx,	,
obsah	obsah	k1gInSc1	obsah
<g/>
,	,	kIx,	,
poděkování	poděkování	k1gNnSc1	poděkování
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
předcházejících	předcházející	k2eAgFnPc2d1	předcházející
vlastnímu	vlastní	k2eAgInSc3d1	vlastní
textu	text	k1gInSc3	text
předpisu	předpis	k1gInSc2	předpis
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
k	k	k7c3	k
číslování	číslování	k1gNnSc3	číslování
odstavců	odstavec	k1gInPc2	odstavec
či	či	k8xC	či
položek	položka	k1gFnPc2	položka
výčtových	výčtový	k2eAgInPc2d1	výčtový
seznamů	seznam	k1gInPc2	seznam
(	(	kIx(	(
<g/>
zpravidla	zpravidla	k6eAd1	zpravidla
doplněné	doplněný	k2eAgInPc1d1	doplněný
pravou	pravý	k2eAgFnSc7d1	pravá
kulatou	kulatý	k2eAgFnSc7d1	kulatá
závorkou	závorka	k1gFnSc7	závorka
či	či	k8xC	či
uzavřené	uzavřený	k2eAgFnPc1d1	uzavřená
závorkou	závorka	k1gFnSc7	závorka
oboustranně	oboustranně	k6eAd1	oboustranně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kalendář	kalendář	k1gInSc1	kalendář
a	a	k8xC	a
ciferník	ciferník	k1gInSc1	ciferník
===	===	k?	===
</s>
</p>
<p>
<s>
Hodinové	hodinový	k2eAgInPc1d1	hodinový
ciferníky	ciferník	k1gInPc1	ciferník
se	se	k3xPyFc4	se
popisují	popisovat	k5eAaImIp3nP	popisovat
římskými	římský	k2eAgFnPc7d1	římská
číslicemi	číslice	k1gFnPc7	číslice
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
používají	používat	k5eAaImIp3nP	používat
symbol	symbol	k1gInSc4	symbol
IIII	IIII	kA	IIII
pro	pro	k7c4	pro
4	[number]	k4	4
a	a	k8xC	a
IX	IX	kA	IX
pro	pro	k7c4	pro
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jednou	k6eAd1	jednou
se	se	k3xPyFc4	se
tak	tak	k9	tak
odečítací	odečítací	k2eAgNnSc1d1	odečítací
pravidlo	pravidlo	k1gNnSc1	pravidlo
neaplikuje	aplikovat	k5eNaBmIp3nS	aplikovat
a	a	k8xC	a
podruhé	podruhé	k6eAd1	podruhé
ano	ano	k9	ano
<g/>
.	.	kIx.	.
</s>
<s>
Proč	proč	k6eAd1	proč
<g/>
?	?	kIx.	?
</s>
</p>
<p>
<s>
Nabízí	nabízet	k5eAaImIp3nS	nabízet
se	se	k3xPyFc4	se
několik	několik	k4yIc4	několik
vysvětlení	vysvětlení	k1gNnPc2	vysvětlení
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Čtyřznakové	čtyřznakový	k2eAgFnSc2d1	čtyřznakový
IIII	IIII	kA	IIII
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
vizuální	vizuální	k2eAgFnSc4d1	vizuální
symetrii	symetrie	k1gFnSc4	symetrie
s	s	k7c7	s
VIII	VIII	kA	VIII
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
ciferníku	ciferník	k1gInSc2	ciferník
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
IV	IV	kA	IV
ne	ne	k9	ne
</s>
</p>
<p>
<s>
IV	IV	kA	IV
jsou	být	k5eAaImIp3nP	být
první	první	k4xOgFnPc1	první
dvě	dva	k4xCgFnPc1	dva
písmena	písmeno	k1gNnPc4	písmeno
jména	jméno	k1gNnSc2	jméno
boha	bůh	k1gMnSc2	bůh
Jupitera	Jupiter	k1gMnSc2	Jupiter
(	(	kIx(	(
<g/>
IVPITER	IVPITER	kA	IVPITER
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Tabulkové	tabulkový	k2eAgInPc1d1	tabulkový
procesory	procesor	k1gInPc1	procesor
a	a	k8xC	a
římská	římský	k2eAgNnPc1d1	římské
čísla	číslo	k1gNnPc1	číslo
===	===	k?	===
</s>
</p>
<p>
<s>
Tabulkové	tabulkový	k2eAgInPc1d1	tabulkový
procesory	procesor	k1gInPc1	procesor
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
OpenOffice	OpenOffice	k1gFnPc1	OpenOffice
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
Calc	Calc	k1gInSc1	Calc
nebo	nebo	k8xC	nebo
Microsoft	Microsoft	kA	Microsoft
Excel	Excel	kA	Excel
<g/>
,	,	kIx,	,
mívají	mívat	k5eAaImIp3nP	mívat
v	v	k7c6	v
sobě	se	k3xPyFc3	se
zabudovány	zabudován	k2eAgFnPc4d1	zabudována
funkce	funkce	k1gFnPc4	funkce
pro	pro	k7c4	pro
převod	převod	k1gInSc4	převod
mezi	mezi	k7c7	mezi
arabskými	arabský	k2eAgFnPc7d1	arabská
a	a	k8xC	a
římskými	římský	k2eAgFnPc7d1	římská
číslicemi	číslice	k1gFnPc7	číslice
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
ve	v	k7c6	v
zmíněném	zmíněný	k2eAgInSc6d1	zmíněný
OpenOffice	OpenOffika	k1gFnSc6	OpenOffika
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
Calc	Calc	k1gInSc1	Calc
můžete	moct	k5eAaImIp2nP	moct
využít	využít	k5eAaPmF	využít
funkce	funkce	k1gFnPc4	funkce
</s>
</p>
<p>
<s>
=	=	kIx~	=
<g/>
ROMAN	Romana	k1gFnPc2	Romana
(	(	kIx(	(
<g/>
arabské	arabský	k2eAgNnSc1d1	arabské
číslo	číslo	k1gNnSc1	číslo
<g/>
;	;	kIx,	;
forma	forma	k1gFnSc1	forma
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
=	=	kIx~	=
<g/>
ARABIC	ARABIC	kA	ARABIC
(	(	kIx(	(
<g/>
římské	římský	k2eAgNnSc1d1	římské
číslo	číslo	k1gNnSc1	číslo
<g/>
)	)	kIx)	)
<g/>
Nepovinný	povinný	k2eNgInSc1d1	nepovinný
argument	argument	k1gInSc1	argument
forma	forma	k1gFnSc1	forma
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
nastavení	nastavení	k1gNnSc4	nastavení
míry	míra	k1gFnSc2	míra
zjednodušení	zjednodušení	k1gNnSc1	zjednodušení
zápisu	zápis	k1gInSc2	zápis
římského	římský	k2eAgNnSc2d1	římské
čísla	číslo	k1gNnSc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
informace	informace	k1gFnPc1	informace
najdete	najít	k5eAaPmIp2nP	najít
v	v	k7c6	v
nápovědě	nápověda	k1gFnSc6	nápověda
příslušného	příslušný	k2eAgInSc2d1	příslušný
programu	program	k1gInSc2	program
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Chronogramy	chronogram	k1gInPc4	chronogram
==	==	k?	==
</s>
</p>
<p>
<s>
Římské	římský	k2eAgFnPc1d1	římská
číslice	číslice	k1gFnPc1	číslice
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
chronogramech	chronogram	k1gInPc6	chronogram
<g/>
.	.	kIx.	.
</s>
<s>
Chronogram	chronogram	k1gInSc1	chronogram
je	být	k5eAaImIp3nS	být
nápis	nápis	k1gInSc1	nápis
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
hexametrový	hexametrový	k2eAgInSc1d1	hexametrový
verš	verš	k1gInSc1	verš
anebo	anebo	k8xC	anebo
elegické	elegický	k2eAgNnSc1d1	elegické
distichon	distichon	k1gNnSc1	distichon
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
skrytý	skrytý	k2eAgInSc1d1	skrytý
letopočet	letopočet	k1gInSc1	letopočet
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
latinské	latinský	k2eAgInPc4d1	latinský
nápisy	nápis	k1gInPc4	nápis
s	s	k7c7	s
několika	několik	k4yIc7	několik
zvýrazněnými	zvýrazněný	k2eAgMnPc7d1	zvýrazněný
velkými	velký	k2eAgNnPc7d1	velké
písmeny	písmeno	k1gNnPc7	písmeno
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
mechanicky	mechanicky	k6eAd1	mechanicky
sečíst	sečíst	k5eAaPmF	sečíst
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
stanovená	stanovený	k2eAgNnPc4d1	stanovené
pravidla	pravidlo	k1gNnPc4	pravidlo
<g/>
.	.	kIx.	.
</s>
<s>
Součet	součet	k1gInSc1	součet
dává	dávat	k5eAaImIp3nS	dávat
letopočet	letopočet	k1gInSc4	letopočet
zveřejnění	zveřejnění	k1gNnSc2	zveřejnění
nápisu	nápis	k1gInSc2	nápis
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Např.	např.	kA	např.
kartuš	kartuš	k1gFnSc1	kartuš
nad	nad	k7c7	nad
vchodem	vchod	k1gInSc7	vchod
do	do	k7c2	do
Lužického	lužický	k2eAgInSc2d1	lužický
semináře	seminář	k1gInSc2	seminář
</s>
</p>
<p>
<s>
DEO	DEO	kA	DEO
ET	ET	kA	ET
APOSTOLORVM	APOSTOLORVM	kA	APOSTOLORVM
PRINCIPI	PRINCIPI	kA	PRINCIPI
LVSATIAE	LVSATIAE	kA	LVSATIAE
PIETAS	PIETAS	kA	PIETAS
EREXIT	EREXIT	kA	EREXIT
</s>
</p>
<p>
<s>
Bohu	bůh	k1gMnSc3	bůh
a	a	k8xC	a
knížeti	kníže	k1gMnSc3	kníže
apoštolů	apoštol	k1gMnPc2	apoštol
postavila	postavit	k5eAaPmAgFnS	postavit
zbožnost	zbožnost	k1gFnSc1	zbožnost
LužiceNápis	LužiceNápis	k1gFnSc1	LužiceNápis
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
chronogram	chronogram	k1gInSc4	chronogram
vyznačený	vyznačený	k2eAgInSc4d1	vyznačený
velikostí	velikost	k1gFnSc7	velikost
písmen	písmeno	k1gNnPc2	písmeno
<g/>
:	:	kIx,	:
MDCLLXVVIIIIII	MDCLLXVVIIIIII	kA	MDCLLXVVIIIIII
=	=	kIx~	=
1726	[number]	k4	1726
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Nepoziční	poziční	k2eNgFnSc1d1	poziční
číselná	číselný	k2eAgFnSc1d1	číselná
soustava	soustava	k1gFnSc1	soustava
</s>
</p>
<p>
<s>
Číselná	číselný	k2eAgFnSc1d1	číselná
soustava	soustava	k1gFnSc1	soustava
</s>
</p>
<p>
<s>
Chronogram	chronogram	k1gInSc1	chronogram
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
římské	římský	k2eAgFnSc2d1	římská
číslice	číslice	k1gFnSc2	číslice
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
římské	římský	k2eAgFnSc2d1	římská
číslo	číslo	k1gNnSc4	číslo
ve	v	k7c4	v
WikislovníkuŘímské	WikislovníkuŘímský	k2eAgFnPc4d1	WikislovníkuŘímský
číslice	číslice	k1gFnPc4	číslice
-	-	kIx~	-
Konvertor	konvertor	k1gInSc1	konvertor
římských	římský	k2eAgNnPc2d1	římské
čísel	číslo	k1gNnPc2	číslo
a	a	k8xC	a
teoretické	teoretický	k2eAgFnPc4d1	teoretická
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
jejich	jejich	k3xOp3gFnSc6	jejich
tvorbě	tvorba	k1gFnSc6	tvorba
</s>
</p>
<p>
<s>
do-skoly	dokola	k1gFnPc1	do-skola
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
-	-	kIx~	-
Pravidla	pravidlo	k1gNnPc1	pravidlo
zápisu	zápis	k1gInSc2	zápis
římských	římský	k2eAgFnPc2d1	římská
číslic	číslice	k1gFnPc2	číslice
<g/>
,	,	kIx,	,
kalkulátor	kalkulátor	k1gInSc4	kalkulátor
s	s	k7c7	s
kontrolou	kontrola	k1gFnSc7	kontrola
validity	validita	k1gFnSc2	validita
výrazů	výraz	k1gInPc2	výraz
a	a	k8xC	a
online	onlinout	k5eAaPmIp3nS	onlinout
testy	test	k1gInPc4	test
<g/>
.	.	kIx.	.
</s>
</p>
