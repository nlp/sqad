<s>
Aviární	Aviární	k2eAgFnSc1d1	Aviární
encefalomyelitida	encefalomyelitida	k1gFnSc1	encefalomyelitida
(	(	kIx(	(
<g/>
AE	AE	kA	AE
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vysoce	vysoce	k6eAd1	vysoce
nakažlivé	nakažlivý	k2eAgNnSc1d1	nakažlivé
virové	virový	k2eAgNnSc1d1	virové
onemocnění	onemocnění	k1gNnSc1	onemocnění
nervového	nervový	k2eAgInSc2d1	nervový
a	a	k8xC	a
trávicího	trávicí	k2eAgInSc2d1	trávicí
systému	systém	k1gInSc2	systém
převážně	převážně	k6eAd1	převážně
kura	kura	k1gFnSc1	kura
domácího	domácí	k2eAgMnSc2d1	domácí
<g/>
,	,	kIx,	,
charakterizované	charakterizovaný	k2eAgNnSc1d1	charakterizované
u	u	k7c2	u
mladých	mladý	k2eAgNnPc2d1	mladé
kuřat	kuře	k1gNnPc2	kuře
poruchou	porucha	k1gFnSc7	porucha
hybnosti	hybnost	k1gFnSc2	hybnost
(	(	kIx(	(
<g/>
ataxie	ataxie	k1gFnSc1	ataxie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
třesem	třes	k1gInSc7	třes
(	(	kIx(	(
<g/>
tremor	tremor	k1gInSc1	tremor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
úplným	úplný	k2eAgNnSc7d1	úplné
ochrnutím	ochrnutí	k1gNnSc7	ochrnutí
(	(	kIx(	(
<g/>
paralýza	paralýza	k1gFnSc1	paralýza
<g/>
)	)	kIx)	)
a	a	k8xC	a
vysokou	vysoký	k2eAgFnSc7d1	vysoká
mortalitou	mortalita	k1gFnSc7	mortalita
<g/>
;	;	kIx,	;
u	u	k7c2	u
dospívající	dospívající	k2eAgFnSc2d1	dospívající
a	a	k8xC	a
pohlavně	pohlavně	k6eAd1	pohlavně
dospělé	dospělý	k2eAgFnPc4d1	dospělá
drůbeže	drůbež	k1gFnPc4	drůbež
probíhá	probíhat	k5eAaImIp3nS	probíhat
infekce	infekce	k1gFnSc1	infekce
bez	bez	k7c2	bez
příznaků	příznak	k1gInPc2	příznak
nebo	nebo	k8xC	nebo
s	s	k7c7	s
dočasným	dočasný	k2eAgInSc7d1	dočasný
poklesem	pokles	k1gInSc7	pokles
snášky	snáška	k1gFnSc2	snáška
u	u	k7c2	u
nosnic	nosnice	k1gFnPc2	nosnice
a	a	k8xC	a
sníženou	snížený	k2eAgFnSc7d1	snížená
líhnivostí	líhnivost	k1gFnSc7	líhnivost
násadových	násadový	k2eAgNnPc2d1	násadový
vajec	vejce	k1gNnPc2	vejce
<g/>
.	.	kIx.	.
</s>
<s>
Původcem	původce	k1gMnSc7	původce
onemocnění	onemocnění	k1gNnSc2	onemocnění
je	být	k5eAaImIp3nS	být
Tremovirus	Tremovirus	k1gInSc1	Tremovirus
z	z	k7c2	z
čeledě	čeleď	k1gFnSc2	čeleď
Picornaviridae	Picornavirida	k1gFnSc2	Picornavirida
<g/>
.	.	kIx.	.
</s>
<s>
Onemocnění	onemocnění	k1gNnSc1	onemocnění
bylo	být	k5eAaImAgNnS	být
poprvé	poprvé	k6eAd1	poprvé
pozorováno	pozorovat	k5eAaImNgNnS	pozorovat
v	v	k7c6	v
USA	USA	kA	USA
v	v	k7c6	v
komerčním	komerční	k2eAgInSc6d1	komerční
chovu	chov	k1gInSc6	chov
2	[number]	k4	2
<g/>
týdenních	týdenní	k2eAgNnPc2d1	týdenní
kuřat	kuře	k1gNnPc2	kuře
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
a	a	k8xC	a
u	u	k7c2	u
krůt	krůta	k1gFnPc2	krůta
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
<s>
AE	AE	kA	AE
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
exportovanou	exportovaný	k2eAgFnSc7d1	exportovaná
drůbeží	drůbež	k1gFnSc7	drůbež
a	a	k8xC	a
násadovými	násadový	k2eAgNnPc7d1	násadový
vejci	vejce	k1gNnPc7	vejce
z	z	k7c2	z
USA	USA	kA	USA
prakticky	prakticky	k6eAd1	prakticky
do	do	k7c2	do
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
drůbež	drůbež	k1gFnSc1	drůbež
chová	chovat	k5eAaImIp3nS	chovat
na	na	k7c6	na
komerční	komerční	k2eAgFnSc6d1	komerční
bázi	báze	k1gFnSc6	báze
<g/>
.	.	kIx.	.
</s>
<s>
Výskyt	výskyt	k1gInSc1	výskyt
(	(	kIx(	(
<g/>
incidence	incidence	k1gFnSc2	incidence
<g/>
)	)	kIx)	)
nemoci	nemoc	k1gFnPc4	nemoc
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
nízký	nízký	k2eAgInSc1d1	nízký
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
všechny	všechen	k3xTgInPc1	všechen
rozmnožovací	rozmnožovací	k2eAgInPc1d1	rozmnožovací
chovy	chov	k1gInPc1	chov
nejsou	být	k5eNaImIp3nP	být
vakcinovány	vakcinován	k2eAgInPc1d1	vakcinován
a	a	k8xC	a
infikují	infikovat	k5eAaBmIp3nP	infikovat
se	s	k7c7	s
přirozeným	přirozený	k2eAgInSc7d1	přirozený
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Vertikální	vertikální	k2eAgInSc1d1	vertikální
přenos	přenos	k1gInSc1	přenos
(	(	kIx(	(
<g/>
přenos	přenos	k1gInSc1	přenos
vejcem	vejce	k1gNnSc7	vejce
<g/>
)	)	kIx)	)
viru	vira	k1gFnSc4	vira
od	od	k7c2	od
infikovaných	infikovaný	k2eAgFnPc2d1	infikovaná
nosnic	nosnice	k1gFnPc2	nosnice
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
infekci	infekce	k1gFnSc3	infekce
a	a	k8xC	a
onemocnění	onemocnění	k1gNnSc1	onemocnění
jejich	jejich	k3xOp3gNnSc2	jejich
potomstva	potomstvo	k1gNnSc2	potomstvo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
byla	být	k5eAaImAgFnS	být
AE	AE	kA	AE
poprvé	poprvé	k6eAd1	poprvé
zaznamenána	zaznamenat	k5eAaPmNgNnP	zaznamenat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
závažnost	závažnost	k1gFnSc1	závažnost
AE	AE	kA	AE
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
přímých	přímý	k2eAgFnPc6d1	přímá
ztrátách	ztráta	k1gFnPc6	ztráta
úhynem	úhyn	k1gInSc7	úhyn
mladých	mladý	k2eAgNnPc2d1	mladé
kuřat	kuře	k1gNnPc2	kuře
<g/>
,	,	kIx,	,
v	v	k7c6	v
nerentabilnosti	nerentabilnost	k1gFnSc6	nerentabilnost
dalšího	další	k2eAgInSc2d1	další
chovu	chov	k1gInSc2	chov
rekonvalescentní	rekonvalescentní	k2eAgFnSc2d1	rekonvalescentní
drůbeže	drůbež	k1gFnSc2	drůbež
<g/>
,	,	kIx,	,
v	v	k7c6	v
krátkodobě	krátkodobě	k6eAd1	krátkodobě
snížené	snížený	k2eAgFnSc3d1	snížená
produkci	produkce	k1gFnSc3	produkce
a	a	k8xC	a
líhnivosti	líhnivost	k1gFnSc3	líhnivost
násadových	násadový	k2eAgNnPc2d1	násadový
vajec	vejce	k1gNnPc2	vejce
a	a	k8xC	a
v	v	k7c6	v
nákladech	náklad	k1gInPc6	náklad
na	na	k7c4	na
vakcinaci	vakcinace	k1gFnSc4	vakcinace
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
zákona	zákon	k1gInSc2	zákon
o	o	k7c4	o
veterinární	veterinární	k2eAgFnSc4d1	veterinární
péči	péče	k1gFnSc4	péče
č.	č.	k?	č.
166	[number]	k4	166
<g/>
/	/	kIx~	/
<g/>
1999	[number]	k4	1999
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
je	být	k5eAaImIp3nS	být
AE	AE	kA	AE
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
nebezpečnou	bezpečný	k2eNgFnSc4d1	nebezpečná
nákazu	nákaza	k1gFnSc4	nákaza
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
viru	vir	k1gInSc2	vir
AE	AE	kA	AE
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
16-30	[number]	k4	16-30
nm	nm	k?	nm
<g/>
.	.	kIx.	.
</s>
<s>
Virus	virus	k1gInSc1	virus
je	být	k5eAaImIp3nS	být
rezistentní	rezistentní	k2eAgInSc1d1	rezistentní
k	k	k7c3	k
éteru	éter	k1gInSc3	éter
<g/>
,	,	kIx,	,
chloroformu	chloroform	k1gInSc3	chloroform
<g/>
,	,	kIx,	,
kyselinám	kyselina	k1gFnPc3	kyselina
<g/>
,	,	kIx,	,
trypsinu	trypsin	k1gInSc3	trypsin
<g/>
,	,	kIx,	,
pepsinu	pepsin	k1gInSc3	pepsin
a	a	k8xC	a
DNase	DNasa	k1gFnSc3	DNasa
a	a	k8xC	a
proti	proti	k7c3	proti
účinkům	účinek	k1gInPc3	účinek
tepla	teplo	k1gNnSc2	teplo
je	být	k5eAaImIp3nS	být
chráněn	chránit	k5eAaImNgInS	chránit
divalentními	divalentní	k2eAgInPc7d1	divalentní
ionty	ion	k1gInPc7	ion
hořčíku	hořčík	k1gInSc2	hořčík
<g/>
.	.	kIx.	.
</s>
<s>
Hustotní	hustotní	k2eAgInSc1d1	hustotní
gradient	gradient	k1gInSc1	gradient
v	v	k7c4	v
CsCl	CsCl	k1gInSc4	CsCl
je	být	k5eAaImIp3nS	být
1,31	[number]	k4	1,31
<g/>
-	-	kIx~	-
<g/>
1,33	[number]	k4	1,33
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
ml	ml	kA	ml
a	a	k8xC	a
sedimentační	sedimentační	k2eAgFnSc1d1	sedimentační
konstanta	konstanta	k1gFnSc1	konstanta
je	být	k5eAaImIp3nS	být
148	[number]	k4	148
S.	S.	kA	S.
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
těchto	tento	k3xDgFnPc2	tento
vlastností	vlastnost	k1gFnPc2	vlastnost
byl	být	k5eAaImAgMnS	být
původce	původce	k1gMnSc1	původce
AE	AE	kA	AE
klasifikován	klasifikovat	k5eAaImNgMnS	klasifikovat
jako	jako	k9	jako
enterovirus	enterovirus	k1gMnSc1	enterovirus
z	z	k7c2	z
čeledě	čeleď	k1gFnSc2	čeleď
Picornaviridae	Picornavirida	k1gFnSc2	Picornavirida
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
původce	původce	k1gMnSc4	původce
AE	AE	kA	AE
zařazován	zařazován	k2eAgInSc4d1	zařazován
do	do	k7c2	do
rodu	rod	k1gInSc2	rod
Tremovirus	Tremovirus	k1gInSc1	Tremovirus
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
čeledi	čeleď	k1gFnSc6	čeleď
V	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
je	být	k5eAaImIp3nS	být
virus	virus	k1gInSc1	virus
relativně	relativně	k6eAd1	relativně
odolný	odolný	k2eAgMnSc1d1	odolný
k	k	k7c3	k
působení	působení	k1gNnSc3	působení
fyzikálních	fyzikální	k2eAgInPc2d1	fyzikální
a	a	k8xC	a
chemických	chemický	k2eAgInPc2d1	chemický
činitelů	činitel	k1gInPc2	činitel
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
20	[number]	k4	20
°	°	k?	°
<g/>
C	C	kA	C
se	se	k3xPyFc4	se
inaktivuje	inaktivovat	k5eAaBmIp3nS	inaktivovat
za	za	k7c4	za
21	[number]	k4	21
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
37	[number]	k4	37
°	°	k?	°
<g/>
C	C	kA	C
za	za	k7c4	za
12	[number]	k4	12
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
56	[number]	k4	56
°	°	k?	°
<g/>
C	C	kA	C
je	být	k5eAaImIp3nS	být
inaktivován	inaktivovat	k5eAaBmNgInS	inaktivovat
za	za	k7c4	za
6	[number]	k4	6
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tkáni	tkáň	k1gFnSc6	tkáň
mozku	mozek	k1gInSc2	mozek
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
infekční	infekční	k2eAgNnSc1d1	infekční
při	při	k7c6	při
4	[number]	k4	4
°	°	k?	°
<g/>
C	C	kA	C
minimálně	minimálně	k6eAd1	minimálně
88	[number]	k4	88
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
ve	v	k7c6	v
zmrzlém	zmrzlý	k2eAgInSc6d1	zmrzlý
stavu	stav	k1gInSc6	stav
si	se	k3xPyFc3	se
uchovává	uchovávat	k5eAaImIp3nS	uchovávat
infekčnost	infekčnost	k1gFnSc4	infekčnost
až	až	k9	až
20	[number]	k4	20
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
terénními	terénní	k2eAgInPc7d1	terénní
izoláty	izolát	k1gInPc7	izolát
viru	vir	k1gInSc2	vir
AE	AE	kA	AE
(	(	kIx(	(
<g/>
AEV	AEV	kA	AEV
<g/>
)	)	kIx)	)
nebyly	být	k5eNaImAgInP	být
zatím	zatím	k6eAd1	zatím
zjištěny	zjistit	k5eAaPmNgInP	zjistit
žádné	žádný	k3yNgInPc1	žádný
antigenní	antigenní	k2eAgInPc1d1	antigenní
rozdíly	rozdíl	k1gInPc1	rozdíl
<g/>
;	;	kIx,	;
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
existuje	existovat	k5eAaImIp3nS	existovat
pouze	pouze	k6eAd1	pouze
jeden	jeden	k4xCgInSc4	jeden
sérotyp	sérotyp	k1gInSc4	sérotyp
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
ale	ale	k9	ale
rozdíly	rozdíl	k1gInPc1	rozdíl
mezi	mezi	k7c7	mezi
izoláty	izolát	k1gInPc7	izolát
a	a	k8xC	a
kmeny	kmen	k1gInPc7	kmen
viru	vir	k1gInSc2	vir
adaptovanými	adaptovaný	k2eAgFnPc7d1	adaptovaná
na	na	k7c4	na
kuřecí	kuřecí	k2eAgNnPc4d1	kuřecí
embrya	embryo	k1gNnPc4	embryo
<g/>
.	.	kIx.	.
</s>
<s>
Rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
se	s	k7c7	s
proto	proto	k8xC	proto
2	[number]	k4	2
patotypy	patotyp	k1gInPc7	patotyp
viru	vir	k1gInSc2	vir
–	–	k?	–
enterotropní	enterotropní	k2eAgInPc4d1	enterotropní
terénní	terénní	k2eAgInPc4d1	terénní
izoláty	izolát	k1gInPc4	izolát
a	a	k8xC	a
neurotropní	urotropní	k2eNgInPc4d1	neurotropní
laboratorní	laboratorní	k2eAgInPc4d1	laboratorní
kmeny	kmen	k1gInPc4	kmen
viru	vir	k1gInSc2	vir
<g/>
.	.	kIx.	.
</s>
<s>
Terénní	terénní	k2eAgInPc1d1	terénní
izoláty	izolát	k1gInPc1	izolát
<g/>
.	.	kIx.	.
</s>
<s>
Patogenita	patogenita	k1gFnSc1	patogenita
terénních	terénní	k2eAgInPc2d1	terénní
kmenů	kmen	k1gInPc2	kmen
viru	vir	k1gInSc2	vir
značně	značně	k6eAd1	značně
kolísá	kolísat	k5eAaImIp3nS	kolísat
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
jsou	být	k5eAaImIp3nP	být
enterotropní	enterotropní	k2eAgFnSc4d1	enterotropní
(	(	kIx(	(
<g/>
mající	mající	k2eAgFnSc4d1	mající
afinitu	afinita	k1gFnSc4	afinita
ke	k	k7c3	k
střevu	střevo	k1gNnSc3	střevo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
snadno	snadno	k6eAd1	snadno
infikují	infikovat	k5eAaBmIp3nP	infikovat
mladá	mladý	k2eAgNnPc1d1	mladé
kuřata	kuře	k1gNnPc1	kuře
orální	orální	k2eAgFnSc1d1	orální
cestou	cesta	k1gFnSc7	cesta
a	a	k8xC	a
vylučují	vylučovat	k5eAaImIp3nP	vylučovat
se	s	k7c7	s
trusem	trus	k1gInSc7	trus
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
ale	ale	k8xC	ale
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
i	i	k9	i
neurotropní	urotropní	k2eNgFnSc4d1	urotropní
(	(	kIx(	(
<g/>
mající	mající	k2eAgFnSc4d1	mající
afinitu	afinita	k1gFnSc4	afinita
k	k	k7c3	k
nervové	nervový	k2eAgFnSc3d1	nervová
tkáni	tkáň	k1gFnSc3	tkáň
<g/>
)	)	kIx)	)
<g/>
a	a	k8xC	a
poškozovat	poškozovat	k5eAaImF	poškozovat
CNS	CNS	kA	CNS
a	a	k8xC	a
vyvolávat	vyvolávat	k5eAaImF	vyvolávat
u	u	k7c2	u
kuřat	kuře	k1gNnPc2	kuře
klinické	klinický	k2eAgInPc4d1	klinický
příznaky	příznak	k1gInPc4	příznak
<g/>
.	.	kIx.	.
</s>
<s>
Terénní	terénní	k2eAgInPc1d1	terénní
izoláty	izolát	k1gInPc1	izolát
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
nepatogenní	patogenní	k2eNgMnPc1d1	nepatogenní
pro	pro	k7c4	pro
kuřecí	kuřecí	k2eAgNnPc4d1	kuřecí
embrya	embryo	k1gNnPc4	embryo
<g/>
,	,	kIx,	,
teprve	teprve	k6eAd1	teprve
opakovaným	opakovaný	k2eAgNnSc7d1	opakované
pasážováním	pasážování	k1gNnSc7	pasážování
se	se	k3xPyFc4	se
adaptují	adaptovat	k5eAaBmIp3nP	adaptovat
<g/>
.	.	kIx.	.
</s>
<s>
Kmeny	kmen	k1gInPc1	kmen
viru	vir	k1gInSc2	vir
adaptované	adaptovaný	k2eAgInPc1d1	adaptovaný
na	na	k7c4	na
embryo	embryo	k1gNnSc4	embryo
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgNnSc1d3	nejznámější
je	být	k5eAaImIp3nS	být
laboratorní	laboratorní	k2eAgInSc4d1	laboratorní
van	van	k1gInSc4	van
Roekelův	Roekelův	k2eAgInSc1d1	Roekelův
kmen	kmen	k1gInSc1	kmen
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
vysoce	vysoce	k6eAd1	vysoce
neurotropní	urotropní	k2eNgMnSc1d1	urotropní
a	a	k8xC	a
po	po	k7c4	po
parenterální	parenterální	k2eAgFnSc4d1	parenterální
(	(	kIx(	(
<g/>
mimo	mimo	k7c4	mimo
střevní	střevní	k2eAgFnSc4d1	střevní
<g/>
)	)	kIx)	)
aplikaci	aplikace	k1gFnSc4	aplikace
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
u	u	k7c2	u
kura	kur	k1gMnSc2	kur
klinické	klinický	k2eAgNnSc4d1	klinické
onemocnění	onemocnění	k1gNnSc4	onemocnění
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
věk	věk	k1gInSc4	věk
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
ale	ale	k9	ale
schopen	schopen	k2eAgMnSc1d1	schopen
infikovat	infikovat	k5eAaBmF	infikovat
kuřata	kuře	k1gNnPc4	kuře
orální	orální	k2eAgFnSc2d1	orální
(	(	kIx(	(
<g/>
ústní	ústní	k2eAgFnSc2d1	ústní
<g/>
)	)	kIx)	)
cestou	cesta	k1gFnSc7	cesta
a	a	k8xC	a
mezi	mezi	k7c7	mezi
drůbeží	drůbež	k1gFnSc7	drůbež
se	se	k3xPyFc4	se
kontaktem	kontakt	k1gInSc7	kontakt
nešíří	šířit	k5eNaImIp3nS	šířit
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vysoce	vysoce	k6eAd1	vysoce
patogenní	patogenní	k2eAgInSc1d1	patogenní
(	(	kIx(	(
<g/>
choroboplodný	choroboplodný	k2eAgInSc1d1	choroboplodný
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
vnímavá	vnímavý	k2eAgNnPc4d1	vnímavé
kuřecí	kuřecí	k2eAgNnPc4d1	kuřecí
embrya	embryo	k1gNnPc4	embryo
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yIgInPc2	který
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
svalovou	svalový	k2eAgFnSc4d1	svalová
atrofii	atrofie	k1gFnSc4	atrofie
a	a	k8xC	a
nepohyblivost	nepohyblivost	k1gFnSc4	nepohyblivost
<g/>
.	.	kIx.	.
</s>
<s>
Embrya	embryo	k1gNnPc1	embryo
jsou	být	k5eAaImIp3nP	být
živá	živý	k2eAgNnPc1d1	živé
<g/>
,	,	kIx,	,
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
srdeční	srdeční	k2eAgInSc4d1	srdeční
tep	tep	k1gInSc4	tep
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
činnost	činnost	k1gFnSc1	činnost
svalstva	svalstvo	k1gNnSc2	svalstvo
je	být	k5eAaImIp3nS	být
kompletně	kompletně	k6eAd1	kompletně
inhibována	inhibován	k2eAgFnSc1d1	inhibována
<g/>
.	.	kIx.	.
</s>
<s>
Virus	virus	k1gInSc1	virus
je	být	k5eAaImIp3nS	být
detekovatelný	detekovatelný	k2eAgInSc1d1	detekovatelný
v	v	k7c6	v
mozku	mozek	k1gInSc6	mozek
inokulovaných	inokulovaný	k2eAgNnPc2d1	inokulovaný
embryí	embryo	k1gNnPc2	embryo
za	za	k7c4	za
3-4	[number]	k4	3-4
dny	den	k1gInPc4	den
PI	pi	k0	pi
a	a	k8xC	a
vrcholových	vrcholový	k2eAgInPc2d1	vrcholový
titrů	titr	k1gInPc2	titr
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
za	za	k7c4	za
6-9	[number]	k4	6-9
dní	den	k1gInPc2	den
PI	pi	k0	pi
<g/>
.	.	kIx.	.
</s>
<s>
Histopatologické	Histopatologický	k2eAgFnPc1d1	Histopatologická
změny	změna	k1gFnPc1	změna
embryí	embryo	k1gNnPc2	embryo
jsou	být	k5eAaImIp3nP	být
uniformní	uniformní	k2eAgInSc1d1	uniformní
svým	svůj	k3xOyFgInSc7	svůj
charakterem	charakter	k1gInSc7	charakter
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
variabilní	variabilní	k2eAgFnSc7d1	variabilní
intenzitou	intenzita	k1gFnSc7	intenzita
a	a	k8xC	a
lokalizaci	lokalizace	k1gFnSc6	lokalizace
<g/>
.	.	kIx.	.
</s>
<s>
Nervové	nervový	k2eAgFnPc1d1	nervová
léze	léze	k1gFnPc1	léze
jsou	být	k5eAaImIp3nP	být
mikroskopicky	mikroskopicky	k6eAd1	mikroskopicky
charakterizovány	charakterizován	k2eAgInPc1d1	charakterizován
těžkými	těžký	k2eAgInPc7d1	těžký
lokálními	lokální	k2eAgInPc7d1	lokální
edémy	edém	k1gInPc7	edém
<g/>
,	,	kIx,	,
gliózou	glióza	k1gFnSc7	glióza
<g/>
,	,	kIx,	,
vaskulární	vaskulární	k2eAgFnSc7d1	vaskulární
proliferací	proliferace	k1gFnSc7	proliferace
a	a	k8xC	a
pyknózou	pyknóza	k1gFnSc7	pyknóza
jader	jádro	k1gNnPc2	jádro
nervových	nervový	k2eAgFnPc2d1	nervová
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Svalové	svalový	k2eAgFnPc1d1	svalová
změny	změna	k1gFnPc1	změna
představují	představovat	k5eAaImIp3nP	představovat
eozinofilní	eozinofilní	k2eAgNnSc4d1	eozinofilní
zduření	zduření	k1gNnSc4	zduření
<g/>
,	,	kIx,	,
nekrózu	nekróza	k1gFnSc4	nekróza
<g/>
,	,	kIx,	,
fragmentaci	fragmentace	k1gFnSc4	fragmentace
a	a	k8xC	a
ztrátu	ztráta	k1gFnSc4	ztráta
žíhání	žíhání	k1gNnSc2	žíhání
postižených	postižený	k2eAgFnPc2d1	postižená
svalových	svalový	k2eAgFnPc2d1	svalová
vláken	vlákna	k1gFnPc2	vlákna
<g/>
,	,	kIx,	,
s	s	k7c7	s
ojediněle	ojediněle	k6eAd1	ojediněle
se	se	k3xPyFc4	se
vyskytující	vyskytující	k2eAgFnSc7d1	vyskytující
proliferací	proliferace	k1gFnSc7	proliferace
sarkolemy	sarkolema	k1gFnSc2	sarkolema
a	a	k8xC	a
heterofilní	heterofilní	k2eAgFnSc7d1	heterofilní
infiltrací	infiltrace	k1gFnSc7	infiltrace
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
pomnožení	pomnožení	k1gNnSc3	pomnožení
viru	vir	k1gInSc2	vir
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
různé	různý	k2eAgInPc1d1	různý
systémy	systém	k1gInPc1	systém
–	–	k?	–
kuřata	kuře	k1gNnPc4	kuře
a	a	k8xC	a
kuřecí	kuřecí	k2eAgNnPc4d1	kuřecí
embrya	embryo	k1gNnPc4	embryo
z	z	k7c2	z
neimunních	imunní	k2eNgInPc2d1	imunní
chovů	chov	k1gInPc2	chov
<g/>
,	,	kIx,	,
buněčné	buněčný	k2eAgFnPc1d1	buněčná
kultury	kultura	k1gFnPc1	kultura
připravené	připravený	k2eAgFnPc1d1	připravená
z	z	k7c2	z
buněk	buňka	k1gFnPc2	buňka
neuroglie	neuroglie	k1gFnSc2	neuroglie
<g/>
,	,	kIx,	,
kuřecích	kuřecí	k2eAgFnPc2d1	kuřecí
embryonálních	embryonální	k2eAgFnPc2d1	embryonální
ledvin	ledvina	k1gFnPc2	ledvina
<g/>
,	,	kIx,	,
kuřecích	kuřecí	k2eAgInPc2d1	kuřecí
embryonálních	embryonální	k2eAgInPc2d1	embryonální
fibroblastů	fibroblast	k1gInPc2	fibroblast
<g/>
,	,	kIx,	,
buněk	buňka	k1gFnPc2	buňka
mozku	mozek	k1gInSc2	mozek
nebo	nebo	k8xC	nebo
pankreatu	pankreas	k1gInSc2	pankreas
kuřecích	kuřecí	k2eAgNnPc2d1	kuřecí
embryí	embryo	k1gNnPc2	embryo
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
kuřecí	kuřecí	k2eAgNnPc1d1	kuřecí
embrya	embryo	k1gNnPc1	embryo
inokulovaná	inokulovaný	k2eAgNnPc1d1	inokulovaný
do	do	k7c2	do
žloutkového	žloutkový	k2eAgInSc2d1	žloutkový
vaku	vak	k1gInSc2	vak
nebo	nebo	k8xC	nebo
do	do	k7c2	do
alantoidní	alantoidní	k2eAgFnSc2d1	alantoidní
dutiny	dutina	k1gFnSc2	dutina
<g/>
.	.	kIx.	.
</s>
<s>
Hostitelský	hostitelský	k2eAgInSc1d1	hostitelský
rozsah	rozsah	k1gInSc1	rozsah
viru	vir	k1gInSc2	vir
AE	AE	kA	AE
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
malý	malý	k2eAgInSc1d1	malý
–	–	k?	–
přirozeně	přirozeně	k6eAd1	přirozeně
se	se	k3xPyFc4	se
vyskytující	vyskytující	k2eAgNnSc1d1	vyskytující
onemocnění	onemocnění	k1gNnSc1	onemocnění
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
pouze	pouze	k6eAd1	pouze
u	u	k7c2	u
mladých	mladý	k2eAgNnPc2d1	mladé
kuřat	kuře	k1gNnPc2	kuře
<g/>
,	,	kIx,	,
krůťat	krůtě	k1gNnPc2	krůtě
<g/>
,	,	kIx,	,
bažantů	bažant	k1gMnPc2	bažant
<g/>
,	,	kIx,	,
koroptví	koroptev	k1gFnPc2	koroptev
<g/>
,	,	kIx,	,
japonských	japonský	k2eAgFnPc2d1	japonská
křepelek	křepelka	k1gFnPc2	křepelka
a	a	k8xC	a
tetřevovitých	tetřevovitý	k2eAgMnPc2d1	tetřevovitý
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
experimentální	experimentální	k2eAgFnSc3d1	experimentální
infekci	infekce	k1gFnSc3	infekce
jsou	být	k5eAaImIp3nP	být
vnímavá	vnímavý	k2eAgNnPc4d1	vnímavé
kachňata	kachně	k1gNnPc4	kachně
<g/>
,	,	kIx,	,
holoubata	holoubě	k1gNnPc4	holoubě
<g/>
,	,	kIx,	,
koroptve	koroptev	k1gFnPc4	koroptev
a	a	k8xC	a
perličky	perlička	k1gFnPc4	perlička
<g/>
.	.	kIx.	.
</s>
<s>
Myši	myš	k1gFnPc1	myš
<g/>
,	,	kIx,	,
morčata	morče	k1gNnPc1	morče
<g/>
,	,	kIx,	,
králíci	králík	k1gMnPc1	králík
i	i	k8xC	i
opice	opice	k1gFnPc1	opice
jsou	být	k5eAaImIp3nP	být
refrakterní	refrakterní	k2eAgMnPc4d1	refrakterní
(	(	kIx(	(
<g/>
nereagující	reagující	k2eNgMnPc4d1	nereagující
<g/>
)	)	kIx)	)
k	k	k7c3	k
intracerebrální	intracerebrální	k2eAgFnPc4d1	intracerebrální
(	(	kIx(	(
<g/>
do	do	k7c2	do
mozku	mozek	k1gInSc2	mozek
<g/>
)	)	kIx)	)
infekci	infekce	k1gFnSc4	infekce
<g/>
.	.	kIx.	.
</s>
<s>
Přirozená	přirozený	k2eAgFnSc1d1	přirozená
infekce	infekce	k1gFnSc1	infekce
nebyla	být	k5eNaImAgFnS	být
sérologicky	sérologicky	k6eAd1	sérologicky
prokázána	prokázat	k5eAaPmNgFnS	prokázat
u	u	k7c2	u
pěnkav	pěnkava	k1gFnPc2	pěnkava
<g/>
,	,	kIx,	,
vrabců	vrabec	k1gMnPc2	vrabec
<g/>
,	,	kIx,	,
špačků	špaček	k1gMnPc2	špaček
<g/>
,	,	kIx,	,
holubů	holub	k1gMnPc2	holub
<g/>
,	,	kIx,	,
kavek	kavka	k1gFnPc2	kavka
<g/>
,	,	kIx,	,
havranů	havran	k1gMnPc2	havran
<g/>
,	,	kIx,	,
hrdliček	hrdlička	k1gFnPc2	hrdlička
a	a	k8xC	a
divokých	divoký	k2eAgFnPc2d1	divoká
kachen	kachna	k1gFnPc2	kachna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přirozených	přirozený	k2eAgFnPc6d1	přirozená
podmínkách	podmínka	k1gFnPc6	podmínka
představuje	představovat	k5eAaImIp3nS	představovat
AE	AE	kA	AE
enterální	enterální	k2eAgFnSc4d1	enterální
infekci	infekce	k1gFnSc4	infekce
šířenou	šířený	k2eAgFnSc7d1	šířená
horizontální	horizontální	k2eAgFnSc7d1	horizontální
(	(	kIx(	(
<g/>
kontaktem	kontakt	k1gInSc7	kontakt
<g/>
)	)	kIx)	)
cestou	cesta	k1gFnSc7	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Virus	virus	k1gInSc1	virus
je	být	k5eAaImIp3nS	být
infikovanými	infikovaný	k2eAgFnPc7d1	infikovaná
ptáky	pták	k1gMnPc4	pták
vylučován	vylučovat	k5eAaImNgInS	vylučovat
trusem	trus	k1gInSc7	trus
po	po	k7c6	po
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
a	a	k8xC	a
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
odolný	odolný	k2eAgInSc1d1	odolný
ve	v	k7c6	v
vnějším	vnější	k2eAgNnSc6d1	vnější
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
,	,	kIx,	,
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
trus	trus	k1gInSc1	trus
infekční	infekční	k2eAgInSc1d1	infekční
po	po	k7c4	po
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
<g/>
.	.	kIx.	.
</s>
<s>
Doba	doba	k1gFnSc1	doba
vylučování	vylučování	k1gNnSc2	vylučování
viru	vir	k1gInSc2	vir
trusem	trus	k1gInSc7	trus
závisí	záviset	k5eAaImIp3nS	záviset
částečně	částečně	k6eAd1	částečně
na	na	k7c6	na
věku	věk	k1gInSc6	věk
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
byla	být	k5eAaImAgFnS	být
drůbež	drůbež	k1gFnSc1	drůbež
infikována	infikovat	k5eAaBmNgFnS	infikovat
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
mladá	mladý	k2eAgNnPc1d1	mladé
kuřata	kuře	k1gNnPc1	kuře
mohou	moct	k5eAaImIp3nP	moct
vylučovat	vylučovat	k5eAaImF	vylučovat
virus	virus	k1gInSc4	virus
více	hodně	k6eAd2	hodně
jak	jak	k8xS	jak
dva	dva	k4xCgInPc4	dva
týdny	týden	k1gInPc4	týden
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
kuřata	kuře	k1gNnPc1	kuře
infikovaná	infikovaný	k2eAgNnPc1d1	infikované
po	po	k7c6	po
3	[number]	k4	3
<g/>
.	.	kIx.	.
týdnu	týden	k1gInSc6	týden
věku	věk	k1gInSc2	věk
vylučují	vylučovat	k5eAaImIp3nP	vylučovat
virus	virus	k1gInSc1	virus
asi	asi	k9	asi
jen	jen	k9	jen
5	[number]	k4	5
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Infikovaná	infikovaný	k2eAgFnSc1d1	infikovaná
podestýlka	podestýlka	k1gFnSc1	podestýlka
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
významným	významný	k2eAgInSc7d1	významný
zdrojem	zdroj	k1gInSc7	zdroj
infekce	infekce	k1gFnSc2	infekce
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
tak	tak	k6eAd1	tak
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
rozšiřována	rozšiřovat	k5eAaImNgFnS	rozšiřovat
ošetřovateli	ošetřovatel	k1gMnPc7	ošetřovatel
na	na	k7c6	na
botách	bota	k1gFnPc6	bota
<g/>
,	,	kIx,	,
oděvem	oděv	k1gInSc7	oděv
nebo	nebo	k8xC	nebo
pracovními	pracovní	k2eAgFnPc7d1	pracovní
pomůckami	pomůcka	k1gFnPc7	pomůcka
do	do	k7c2	do
dalších	další	k2eAgFnPc2d1	další
hal	hala	k1gFnPc2	hala
na	na	k7c6	na
farmě	farma	k1gFnSc6	farma
<g/>
.	.	kIx.	.
</s>
<s>
Infekce	infekce	k1gFnSc1	infekce
zavlečená	zavlečený	k2eAgFnSc1d1	zavlečená
do	do	k7c2	do
chovu	chov	k1gInSc2	chov
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
šíří	šířit	k5eAaImIp3nS	šířit
<g/>
.	.	kIx.	.
</s>
<s>
Farmy	farma	k1gFnPc1	farma
s	s	k7c7	s
drůbeží	drůbež	k1gFnSc7	drůbež
stejného	stejný	k2eAgInSc2d1	stejný
věku	věk	k1gInSc2	věk
jsou	být	k5eAaImIp3nP	být
méně	málo	k6eAd2	málo
ohroženy	ohrozit	k5eAaPmNgFnP	ohrozit
než	než	k8xS	než
farmy	farma	k1gFnPc1	farma
s	s	k7c7	s
drůbeží	drůbež	k1gFnSc7	drůbež
různých	různý	k2eAgFnPc2d1	různá
věkových	věkový	k2eAgFnPc2d1	věková
kategorií	kategorie	k1gFnPc2	kategorie
<g/>
.	.	kIx.	.
</s>
<s>
Virus	virus	k1gInSc1	virus
se	se	k3xPyFc4	se
šíří	šířit	k5eAaImIp3nS	šířit
pomaleji	pomale	k6eAd2	pomale
v	v	k7c6	v
chovech	chov	k1gInPc6	chov
s	s	k7c7	s
klecovou	klecový	k2eAgFnSc7d1	klecová
technologií	technologie	k1gFnSc7	technologie
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
chovy	chov	k1gInPc7	chov
na	na	k7c6	na
podestýlce	podestýlka	k1gFnSc6	podestýlka
<g/>
.	.	kIx.	.
</s>
<s>
Neméně	málo	k6eNd2	málo
důležitý	důležitý	k2eAgInSc1d1	důležitý
je	být	k5eAaImIp3nS	být
vertikální	vertikální	k2eAgInSc1d1	vertikální
přenos	přenos	k1gInSc1	přenos
infekce	infekce	k1gFnSc2	infekce
(	(	kIx(	(
<g/>
vejcem	vejce	k1gNnSc7	vejce
<g/>
)	)	kIx)	)
v	v	k7c6	v
populaci	populace	k1gFnSc6	populace
rozmnožovacích	rozmnožovací	k2eAgInPc2d1	rozmnožovací
chovů	chov	k1gInPc2	chov
drůbeže	drůbež	k1gFnSc2	drůbež
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
je	být	k5eAaImIp3nS	být
vnímavé	vnímavý	k2eAgNnSc4d1	vnímavé
hejno	hejno	k1gNnSc4	hejno
infikováno	infikován	k2eAgNnSc4d1	infikováno
po	po	k7c6	po
dosažení	dosažení	k1gNnSc6	dosažení
pohlavní	pohlavní	k2eAgFnSc2d1	pohlavní
dospělosti	dospělost	k1gFnSc2	dospělost
<g/>
,	,	kIx,	,
vylučují	vylučovat	k5eAaImIp3nP	vylučovat
nosnice	nosnice	k1gFnSc1	nosnice
virus	virus	k1gInSc4	virus
do	do	k7c2	do
vajec	vejce	k1gNnPc2	vejce
za	za	k7c4	za
5-13	[number]	k4	5-13
dní	den	k1gInPc2	den
po	po	k7c6	po
infekci	infekce	k1gFnSc6	infekce
(	(	kIx(	(
<g/>
PI	pi	k0	pi
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
při	při	k7c6	při
současném	současný	k2eAgNnSc6d1	současné
snížení	snížení	k1gNnSc6	snížení
snášky	snáška	k1gFnSc2	snáška
i	i	k8xC	i
líhnivosti	líhnivost	k1gFnSc2	líhnivost
<g/>
.	.	kIx.	.
</s>
<s>
Transovariálně	Transovariálně	k6eAd1	Transovariálně
infikovaná	infikovaný	k2eAgNnPc1d1	infikované
a	a	k8xC	a
vylíhnutá	vylíhnutý	k2eAgNnPc1d1	vylíhnuté
kuřata	kuře	k1gNnPc1	kuře
mohou	moct	k5eAaImIp3nP	moct
onemocnět	onemocnět	k5eAaPmF	onemocnět
již	již	k6eAd1	již
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
týdnu	týden	k1gInSc6	týden
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
kuřata	kuře	k1gNnPc1	kuře
líhnutá	líhnutý	k2eAgNnPc1d1	líhnutý
z	z	k7c2	z
NV	NV	kA	NV
snesených	snesený	k2eAgFnPc2d1	snesená
před	před	k7c7	před
a	a	k8xC	a
po	po	k7c6	po
poklesu	pokles	k1gInSc6	pokles
snášky	snáška	k1gFnSc2	snáška
jsou	být	k5eAaImIp3nP	být
normální	normální	k2eAgInPc1d1	normální
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
mohou	moct	k5eAaImIp3nP	moct
nakazit	nakazit	k5eAaPmF	nakazit
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
líhni	líheň	k1gFnSc6	líheň
od	od	k7c2	od
líhnutých	líhnutý	k2eAgNnPc2d1	líhnutý
infikovaných	infikovaný	k2eAgNnPc2d1	infikované
kuřat	kuře	k1gNnPc2	kuře
a	a	k8xC	a
onemocnět	onemocnět	k5eAaPmF	onemocnět
po	po	k7c6	po
2	[number]	k4	2
<g/>
.	.	kIx.	.
týdnu	týden	k1gInSc6	týden
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Přímý	přímý	k2eAgInSc4d1	přímý
průkaz	průkaz	k1gInSc4	průkaz
existence	existence	k1gFnSc2	existence
virusonosičství	virusonosičství	k1gNnSc2	virusonosičství
zatím	zatím	k6eAd1	zatím
nebyl	být	k5eNaImAgInS	být
prokázán	prokázat	k5eAaPmNgInS	prokázat
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
mladých	mladý	k2eAgNnPc2d1	mladé
kuřat	kuře	k1gNnPc2	kuře
orálně	orálně	k6eAd1	orálně
infikovaných	infikovaný	k2eAgMnPc2d1	infikovaný
terénními	terénní	k2eAgFnPc7d1	terénní
kmeny	kmen	k1gInPc4	kmen
AEV	AEV	kA	AEV
je	být	k5eAaImIp3nS	být
primárně	primárně	k6eAd1	primárně
infikován	infikován	k2eAgInSc4d1	infikován
střevní	střevní	k2eAgInSc4d1	střevní
trakt	trakt	k1gInSc4	trakt
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
duodenum	duodenum	k1gNnSc1	duodenum
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
následné	následný	k2eAgFnSc6d1	následná
virémií	virémie	k1gFnSc7	virémie
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
infekci	infekce	k1gFnSc3	infekce
viscerálních	viscerální	k2eAgInPc2d1	viscerální
orgánů	orgán	k1gInPc2	orgán
(	(	kIx(	(
<g/>
pankreas	pankreas	k1gInSc4	pankreas
<g/>
,	,	kIx,	,
játra	játra	k1gNnPc4	játra
<g/>
,	,	kIx,	,
srdce	srdce	k1gNnPc4	srdce
<g/>
,	,	kIx,	,
ledviny	ledvina	k1gFnPc4	ledvina
<g/>
,	,	kIx,	,
slezina	slezina	k1gFnSc1	slezina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
svaloviny	svalovina	k1gFnSc2	svalovina
a	a	k8xC	a
CNS	CNS	kA	CNS
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
CNS	CNS	kA	CNS
se	se	k3xPyFc4	se
virový	virový	k2eAgInSc1d1	virový
antigen	antigen	k1gInSc1	antigen
nachází	nacházet	k5eAaImIp3nS	nacházet
zejména	zejména	k9	zejména
v	v	k7c6	v
mozečku	mozeček	k1gInSc6	mozeček
(	(	kIx(	(
<g/>
molekulární	molekulární	k2eAgFnSc1d1	molekulární
vrstva	vrstva	k1gFnSc1	vrstva
a	a	k8xC	a
Purkyňovy	Purkyňův	k2eAgFnPc1d1	Purkyňova
buňky	buňka	k1gFnPc1	buňka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
důležitým	důležitý	k2eAgInSc7d1	důležitý
faktorem	faktor	k1gInSc7	faktor
pro	pro	k7c4	pro
rozvoj	rozvoj	k1gInSc4	rozvoj
nemoci	nemoc	k1gFnSc2	nemoc
je	být	k5eAaImIp3nS	být
věk	věk	k1gInSc1	věk
<g/>
.	.	kIx.	.
</s>
<s>
Kuřata	kuře	k1gNnPc1	kuře
infikovaná	infikovaný	k2eAgNnPc1d1	infikované
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
dnu	dno	k1gNnSc3	dno
věku	věk	k1gInSc2	věk
obvykle	obvykle	k6eAd1	obvykle
uhynou	uhynout	k5eAaPmIp3nP	uhynout
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
kuřata	kuře	k1gNnPc1	kuře
infikovaná	infikovaný	k2eAgNnPc1d1	infikované
po	po	k7c6	po
8	[number]	k4	8
<g/>
.	.	kIx.	.
dnu	dno	k1gNnSc3	dno
věku	věk	k1gInSc2	věk
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
mírnější	mírný	k2eAgInPc1d2	mírnější
klinické	klinický	k2eAgInPc1d1	klinický
příznaky	příznak	k1gInPc1	příznak
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
uzdravit	uzdravit	k5eAaPmF	uzdravit
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
od	od	k7c2	od
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
týdne	týden	k1gInSc2	týden
věku	věk	k1gInSc2	věk
nastupuje	nastupovat	k5eAaImIp3nS	nastupovat
tzv.	tzv.	kA	tzv.
věková	věkový	k2eAgFnSc1d1	věková
rezistence	rezistence	k1gFnSc1	rezistence
k	k	k7c3	k
onemocnění	onemocnění	k1gNnSc3	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Podstatou	podstata	k1gFnSc7	podstata
věkové	věkový	k2eAgFnSc2d1	věková
rezistence	rezistence	k1gFnSc2	rezistence
k	k	k7c3	k
onemocnění	onemocnění	k1gNnSc3	onemocnění
je	být	k5eAaImIp3nS	být
imunita	imunita	k1gFnSc1	imunita
<g/>
.	.	kIx.	.
</s>
<s>
Imunologická	imunologický	k2eAgFnSc1d1	imunologická
vyzrálost	vyzrálost	k1gFnSc1	vyzrálost
dospělé	dospělý	k2eAgFnSc2d1	dospělá
drůbeže	drůbež	k1gFnSc2	drůbež
zabraňuje	zabraňovat	k5eAaImIp3nS	zabraňovat
rozšíření	rozšíření	k1gNnSc1	rozšíření
infekce	infekce	k1gFnSc2	infekce
až	až	k9	až
do	do	k7c2	do
CNS	CNS	kA	CNS
<g/>
.	.	kIx.	.
</s>
<s>
Inkubační	inkubační	k2eAgFnSc1d1	inkubační
doba	doba	k1gFnSc1	doba
po	po	k7c6	po
vertikální	vertikální	k2eAgFnSc6d1	vertikální
infekci	infekce	k1gFnSc6	infekce
je	být	k5eAaImIp3nS	být
1-7	[number]	k4	1-7
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
po	po	k7c6	po
kontaktní	kontaktní	k2eAgFnSc6d1	kontaktní
nebo	nebo	k8xC	nebo
experimentální	experimentální	k2eAgFnSc3d1	experimentální
orální	orální	k2eAgFnSc3d1	orální
infekci	infekce	k1gFnSc3	infekce
minimálně	minimálně	k6eAd1	minimálně
9-11	[number]	k4	9-11
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Nervové	nervový	k2eAgInPc1d1	nervový
příznaky	příznak	k1gInPc1	příznak
onemocnění	onemocnění	k1gNnSc2	onemocnění
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
objevit	objevit	k5eAaPmF	objevit
již	již	k6eAd1	již
po	po	k7c6	po
vylíhnutí	vylíhnutí	k1gNnSc6	vylíhnutí
(	(	kIx(	(
<g/>
transovariální	transovariální	k2eAgFnSc2d1	transovariální
infekce	infekce	k1gFnSc2	infekce
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
častější	častý	k2eAgNnSc1d2	častější
je	být	k5eAaImIp3nS	být
však	však	k9	však
výskyt	výskyt	k1gInSc1	výskyt
u	u	k7c2	u
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
týdenních	týdenní	k2eAgNnPc2d1	týdenní
kuřat	kuře	k1gNnPc2	kuře
(	(	kIx(	(
<g/>
kontaktní	kontaktní	k2eAgFnSc1d1	kontaktní
infekce	infekce	k1gFnSc1	infekce
v	v	k7c6	v
líhních	líheň	k1gFnPc6	líheň
nebo	nebo	k8xC	nebo
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
vylíhnutí	vylíhnutí	k1gNnSc6	vylíhnutí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Morbidita	morbidita	k1gFnSc1	morbidita
(	(	kIx(	(
<g/>
nemocnost	nemocnost	k1gFnSc1	nemocnost
<g/>
)	)	kIx)	)
a	a	k8xC	a
mortalita	mortalita	k1gFnSc1	mortalita
v	v	k7c6	v
hejnu	hejno	k1gNnSc6	hejno
závisejí	záviset	k5eAaImIp3nP	záviset
na	na	k7c6	na
původu	původ	k1gInSc2	původ
kuřat	kuře	k1gNnPc2	kuře
(	(	kIx(	(
<g/>
zda	zda	k8xS	zda
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
imunního	imunní	k2eAgNnSc2d1	imunní
nebo	nebo	k8xC	nebo
vnímavého	vnímavý	k2eAgNnSc2d1	vnímavé
hejna	hejno	k1gNnSc2	hejno
nosnic	nosnice	k1gFnPc2	nosnice
<g/>
)	)	kIx)	)
a	a	k8xC	a
věku	věk	k1gInSc3	věk
kuřat	kuře	k1gNnPc2	kuře
při	při	k7c6	při
infekci	infekce	k1gFnSc6	infekce
(	(	kIx(	(
<g/>
u	u	k7c2	u
mladších	mladý	k2eAgNnPc2d2	mladší
kuřat	kuře	k1gNnPc2	kuře
je	být	k5eAaImIp3nS	být
průběh	průběh	k1gInSc4	průběh
nemoci	nemoc	k1gFnSc2	nemoc
těžší	těžký	k2eAgFnSc2d2	těžší
a	a	k8xC	a
ztráty	ztráta	k1gFnSc2	ztráta
vyšší	vysoký	k2eAgInPc1d2	vyšší
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mortalita	mortalita	k1gFnSc1	mortalita
se	se	k3xPyFc4	se
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
kolem	kolem	k7c2	kolem
25	[number]	k4	25
%	%	kIx~	%
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
může	moct	k5eAaImIp3nS	moct
přesahovat	přesahovat	k5eAaImF	přesahovat
i	i	k9	i
50	[number]	k4	50
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
nosnic	nosnice	k1gFnPc2	nosnice
se	se	k3xPyFc4	se
AE	AE	kA	AE
projevuje	projevovat	k5eAaImIp3nS	projevovat
pouze	pouze	k6eAd1	pouze
krátkodobým	krátkodobý	k2eAgInSc7d1	krátkodobý
poklesem	pokles	k1gInSc7	pokles
snášky	snáška	k1gFnSc2	snáška
(	(	kIx(	(
<g/>
asi	asi	k9	asi
o	o	k7c4	o
5-30	[number]	k4	5-30
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
zhoršenou	zhoršený	k2eAgFnSc7d1	zhoršená
líhnivostí	líhnivost	k1gFnSc7	líhnivost
násadových	násadový	k2eAgNnPc2d1	násadový
vajec	vejce	k1gNnPc2	vejce
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
krůt	krůta	k1gFnPc2	krůta
probíhá	probíhat	k5eAaImIp3nS	probíhat
AE	AE	kA	AE
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
u	u	k7c2	u
kura	kur	k1gMnSc2	kur
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
průběh	průběh	k1gInSc1	průběh
lehčí	lehký	k2eAgMnSc1d2	lehčí
a	a	k8xC	a
ztráty	ztráta	k1gFnPc1	ztráta
menší	malý	k2eAgFnPc1d2	menší
<g/>
.	.	kIx.	.
</s>
<s>
Postižená	postižený	k2eAgNnPc1d1	postižené
kuřata	kuře	k1gNnPc1	kuře
jsou	být	k5eAaImIp3nP	být
otupělá	otupělý	k2eAgNnPc1d1	otupělé
<g/>
,	,	kIx,	,
trpí	trpět	k5eAaImIp3nP	trpět
progresivní	progresivní	k2eAgFnSc7d1	progresivní
ataxií	ataxie	k1gFnSc7	ataxie
(	(	kIx(	(
<g/>
porucha	porucha	k1gFnSc1	porucha
hybnosti	hybnost	k1gFnSc2	hybnost
<g/>
)	)	kIx)	)
a	a	k8xC	a
nekoordinovanou	koordinovaný	k2eNgFnSc7d1	nekoordinovaná
činností	činnost	k1gFnSc7	činnost
kosterního	kosterní	k2eAgNnSc2d1	kosterní
svalstva	svalstvo	k1gNnSc2	svalstvo
<g/>
.	.	kIx.	.
</s>
<s>
Posedávají	posedávat	k5eAaImIp3nP	posedávat
<g/>
,	,	kIx,	,
tiše	tiš	k1gFnPc1	tiš
naříkají	naříkat	k5eAaBmIp3nP	naříkat
a	a	k8xC	a
nerada	nerad	k2eAgFnSc1d1	nerada
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
<g/>
.	.	kIx.	.
</s>
<s>
Chuť	chuť	k1gFnSc1	chuť
k	k	k7c3	k
přijímání	přijímání	k1gNnSc3	přijímání
krmiva	krmivo	k1gNnSc2	krmivo
mají	mít	k5eAaImIp3nP	mít
zachovanou	zachovaný	k2eAgFnSc4d1	zachovaná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ke	k	k7c3	k
krmítkům	krmítko	k1gNnPc3	krmítko
ani	ani	k8xC	ani
napáječkám	napáječka	k1gFnPc3	napáječka
se	se	k3xPyFc4	se
nedostanou	dostat	k5eNaPmIp3nP	dostat
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
průjem	průjem	k1gInSc4	průjem
a	a	k8xC	a
zalepenou	zalepený	k2eAgFnSc4d1	zalepená
kloaku	kloaka	k1gFnSc4	kloaka
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vynuceném	vynucený	k2eAgInSc6d1	vynucený
pohybu	pohyb	k1gInSc6	pohyb
se	se	k3xPyFc4	se
potácejí	potácet	k5eAaImIp3nP	potácet
<g/>
,	,	kIx,	,
padají	padat	k5eAaImIp3nP	padat
na	na	k7c4	na
bok	bok	k1gInSc4	bok
nebo	nebo	k8xC	nebo
na	na	k7c4	na
záda	záda	k1gNnPc4	záda
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
po	po	k7c6	po
hleznových	hleznův	k2eAgInPc6d1	hleznův
kloubech	kloub	k1gInPc6	kloub
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
uchopení	uchopení	k1gNnSc6	uchopení
kuřat	kuře	k1gNnPc2	kuře
do	do	k7c2	do
dlaně	dlaň	k1gFnSc2	dlaň
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
zjistí	zjistit	k5eAaPmIp3nS	zjistit
jemný	jemný	k2eAgInSc1d1	jemný
tremor	tremor	k1gInSc1	tremor
(	(	kIx(	(
<g/>
třes	třes	k1gInSc1	třes
<g/>
)	)	kIx)	)
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
krku	krk	k1gInSc2	krk
a	a	k8xC	a
celého	celý	k2eAgNnSc2d1	celé
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
pro	pro	k7c4	pro
nízkou	nízký	k2eAgFnSc4d1	nízká
amplitudu	amplituda	k1gFnSc4	amplituda
a	a	k8xC	a
vysokou	vysoký	k2eAgFnSc4d1	vysoká
frekvenci	frekvence	k1gFnSc4	frekvence
je	být	k5eAaImIp3nS	být
okem	oke	k1gNnSc7	oke
nepostřehnutelný	postřehnutelný	k2eNgInSc1d1	nepostřehnutelný
<g/>
.	.	kIx.	.
</s>
<s>
Ataxie	ataxie	k1gFnSc1	ataxie
postupně	postupně	k6eAd1	postupně
přechází	přecházet	k5eAaImIp3nS	přecházet
v	v	k7c4	v
parézu	paréza	k1gFnSc4	paréza
až	až	k9	až
paralýzu	paralýza	k1gFnSc4	paralýza
a	a	k8xC	a
kuřata	kuře	k1gNnPc1	kuře
hynou	hynout	k5eAaImIp3nP	hynout
vysílením	vysílení	k1gNnSc7	vysílení
a	a	k8xC	a
dehydratací	dehydratace	k1gFnSc7	dehydratace
nebo	nebo	k8xC	nebo
jsou	být	k5eAaImIp3nP	být
ušlapána	ušlapán	k2eAgNnPc4d1	ušlapáno
ostatními	ostatní	k2eAgNnPc7d1	ostatní
kuřaty	kuře	k1gNnPc7	kuře
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
klinicky	klinicky	k6eAd1	klinicky
nemocných	nemocný	k2eAgNnPc2d1	nemocný
kuřat	kuře	k1gNnPc2	kuře
se	se	k3xPyFc4	se
uzdraví	uzdravit	k5eAaPmIp3nP	uzdravit
bez	bez	k7c2	bez
následků	následek	k1gInPc2	následek
<g/>
,	,	kIx,	,
u	u	k7c2	u
jiných	jiný	k2eAgMnPc2d1	jiný
mohou	moct	k5eAaImIp3nP	moct
přetrvávat	přetrvávat	k5eAaImF	přetrvávat
dále	daleko	k6eAd2	daleko
mírné	mírný	k2eAgInPc1d1	mírný
pohybové	pohybový	k2eAgInPc1d1	pohybový
poruchy	poruch	k1gInPc1	poruch
(	(	kIx(	(
<g/>
atrofie	atrofie	k1gFnSc1	atrofie
svalstva	svalstvo	k1gNnSc2	svalstvo
<g/>
)	)	kIx)	)
včetně	včetně	k7c2	včetně
zaostávání	zaostávání	k1gNnSc2	zaostávání
v	v	k7c6	v
růstu	růst	k1gInSc6	růst
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgNnPc2	některý
kuřat	kuře	k1gNnPc2	kuře
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
šedomodrému	šedomodrý	k2eAgInSc3d1	šedomodrý
zákalu	zákal	k1gInSc3	zákal
oční	oční	k2eAgFnSc2d1	oční
čočky	čočka	k1gFnSc2	čočka
(	(	kIx(	(
<g/>
katarakta	katarakta	k1gFnSc1	katarakta
<g/>
)	)	kIx)	)
různého	různý	k2eAgInSc2d1	různý
stupně	stupeň	k1gInSc2	stupeň
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
postihuje	postihovat	k5eAaImIp3nS	postihovat
jedno	jeden	k4xCgNnSc1	jeden
nebo	nebo	k8xC	nebo
obě	dva	k4xCgNnPc4	dva
oči	oko	k1gNnPc4	oko
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
až	až	k9	až
k	k	k7c3	k
oslepnutí	oslepnutí	k1gNnSc3	oslepnutí
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
dospívající	dospívající	k2eAgFnSc2d1	dospívající
a	a	k8xC	a
pohlavně	pohlavně	k6eAd1	pohlavně
dospělé	dospělý	k2eAgFnPc4d1	dospělá
drůbeže	drůbež	k1gFnPc4	drůbež
se	se	k3xPyFc4	se
nervové	nervový	k2eAgInPc1d1	nervový
příznaky	příznak	k1gInPc1	příznak
neobjevují	objevovat	k5eNaImIp3nP	objevovat
(	(	kIx(	(
<g/>
věková	věkový	k2eAgFnSc1d1	věková
rezistence	rezistence	k1gFnSc1	rezistence
k	k	k7c3	k
onemocnění	onemocnění	k1gNnSc3	onemocnění
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k6eAd1	mimo
krátkodobého	krátkodobý	k2eAgInSc2d1	krátkodobý
poklesu	pokles	k1gInSc2	pokles
snášky	snáška	k1gFnSc2	snáška
a	a	k8xC	a
zhoršené	zhoršený	k2eAgFnSc2d1	zhoršená
líhnivosti	líhnivost	k1gFnSc2	líhnivost
násadových	násadový	k2eAgNnPc2d1	násadový
vajec	vejce	k1gNnPc2	vejce
se	se	k3xPyFc4	se
ojediněle	ojediněle	k6eAd1	ojediněle
pozorují	pozorovat	k5eAaImIp3nP	pozorovat
stejné	stejný	k2eAgFnPc4d1	stejná
změny	změna	k1gFnPc4	změna
na	na	k7c6	na
očích	oko	k1gNnPc6	oko
jako	jako	k9	jako
u	u	k7c2	u
rekonvalescentních	rekonvalescentní	k2eAgNnPc2d1	rekonvalescentní
kuřat	kuře	k1gNnPc2	kuře
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
průběh	průběh	k1gInSc4	průběh
AE	AE	kA	AE
u	u	k7c2	u
nosnic	nosnice	k1gFnPc2	nosnice
i	i	k8xC	i
přehlédnout	přehlédnout	k5eAaPmF	přehlédnout
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
jediným	jediný	k2eAgInSc7d1	jediný
příznakem	příznak	k1gInSc7	příznak
nemoci	nemoc	k1gFnSc2	nemoc
je	být	k5eAaImIp3nS	být
výskyt	výskyt	k1gInSc1	výskyt
AE	AE	kA	AE
u	u	k7c2	u
jejich	jejich	k3xOp3gNnSc2	jejich
potomstva	potomstvo	k1gNnSc2	potomstvo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hejnu	hejno	k1gNnSc6	hejno
nosnic	nosnice	k1gFnPc2	nosnice
se	se	k3xPyFc4	se
infekce	infekce	k1gFnSc2	infekce
prokazuje	prokazovat	k5eAaImIp3nS	prokazovat
sérologicky	sérologicky	k6eAd1	sérologicky
<g/>
.	.	kIx.	.
</s>
<s>
Pokles	pokles	k1gInSc1	pokles
snášky	snáška	k1gFnSc2	snáška
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
rychlý	rychlý	k2eAgInSc1d1	rychlý
–	–	k?	–
během	během	k7c2	během
3-6	[number]	k4	3-6
dnů	den	k1gInPc2	den
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
maxima	maxima	k1gFnSc1	maxima
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yRgInSc6	který
setrvává	setrvávat	k5eAaImIp3nS	setrvávat
asi	asi	k9	asi
jeden	jeden	k4xCgInSc4	jeden
den	den	k1gInSc4	den
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
za	za	k7c4	za
11-13	[number]	k4	11-13
dnů	den	k1gInPc2	den
vrací	vracet	k5eAaImIp3nS	vracet
k	k	k7c3	k
normální	normální	k2eAgFnSc3d1	normální
snáškové	snáškový	k2eAgFnSc3d1	snáškový
křivce	křivka	k1gFnSc3	křivka
<g/>
.	.	kIx.	.
</s>
<s>
Období	období	k1gNnSc1	období
vylučování	vylučování	k1gNnSc2	vylučování
viru	vir	k1gInSc2	vir
nosnicemi	nosnice	k1gFnPc7	nosnice
do	do	k7c2	do
vajec	vejce	k1gNnPc2	vejce
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
úzké	úzký	k2eAgFnSc6d1	úzká
korelaci	korelace	k1gFnSc6	korelace
s	s	k7c7	s
poklesem	pokles	k1gInSc7	pokles
snášky	snáška	k1gFnSc2	snáška
a	a	k8xC	a
líhnivosti	líhnivost	k1gFnSc2	líhnivost
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
postižených	postižený	k1gMnPc2	postižený
kuřat	kuře	k1gNnPc2	kuře
se	se	k3xPyFc4	se
makroskopicky	makroskopicky	k6eAd1	makroskopicky
ojediněle	ojediněle	k6eAd1	ojediněle
zjišťují	zjišťovat	k5eAaImIp3nP	zjišťovat
bělavé	bělavý	k2eAgInPc4d1	bělavý
okrsky	okrsek	k1gInPc4	okrsek
<g/>
,	,	kIx,	,
představované	představovaný	k2eAgInPc4d1	představovaný
masou	masa	k1gFnSc7	masa
infiltrujících	infiltrující	k2eAgInPc2d1	infiltrující
lymfocytů	lymfocyt	k1gInPc2	lymfocyt
<g/>
,	,	kIx,	,
ve	v	k7c6	v
svalovině	svalovina	k1gFnSc6	svalovina
proventrikulu	proventrikul	k1gInSc2	proventrikul
a	a	k8xC	a
drobné	drobný	k2eAgFnSc2d1	drobná
krváceniny	krvácenina	k1gFnSc2	krvácenina
na	na	k7c6	na
mozečku	mozeček	k1gInSc6	mozeček
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgInPc1d1	jiný
nálezy	nález	k1gInPc1	nález
obvykle	obvykle	k6eAd1	obvykle
nejsou	být	k5eNaImIp3nP	být
<g/>
.	.	kIx.	.
</s>
<s>
Mikroskopické	mikroskopický	k2eAgFnPc1d1	mikroskopická
změny	změna	k1gFnPc1	změna
se	se	k3xPyFc4	se
nalézají	nalézat	k5eAaImIp3nP	nalézat
v	v	k7c6	v
mozku	mozek	k1gInSc6	mozek
<g/>
,	,	kIx,	,
míše	mícha	k1gFnSc6	mícha
a	a	k8xC	a
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
viscerálních	viscerální	k2eAgInPc6d1	viscerální
orgánech	orgán	k1gInPc6	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Periferní	periferní	k2eAgInPc1d1	periferní
nervy	nerv	k1gInPc1	nerv
nejsou	být	k5eNaImIp3nP	být
postiženy	postižen	k2eAgInPc1d1	postižen
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
má	mít	k5eAaImIp3nS	mít
diagnostický	diagnostický	k2eAgInSc4d1	diagnostický
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
úzká	úzký	k2eAgFnSc1d1	úzká
korelace	korelace	k1gFnSc1	korelace
mezi	mezi	k7c7	mezi
klinickými	klinický	k2eAgInPc7d1	klinický
příznaky	příznak	k1gInPc7	příznak
a	a	k8xC	a
histopatologickými	histopatologický	k2eAgFnPc7d1	histopatologická
změnami	změna	k1gFnPc7	změna
<g/>
.	.	kIx.	.
</s>
<s>
Změny	změna	k1gFnPc1	změna
v	v	k7c6	v
CNS	CNS	kA	CNS
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
neuronech	neuron	k1gInPc6	neuron
regresivní	regresivní	k2eAgFnSc1d1	regresivní
a	a	k8xC	a
v	v	k7c6	v
cévách	céva	k1gFnPc6	céva
a	a	k8xC	a
podpůrných	podpůrný	k2eAgFnPc6d1	podpůrná
tkáních	tkáň	k1gFnPc6	tkáň
proliferativní	proliferativní	k2eAgInSc4d1	proliferativní
charakter	charakter	k1gInSc4	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
představovány	představován	k2eAgInPc1d1	představován
diseminovanou	diseminovaný	k2eAgFnSc7d1	diseminovaná
nehnisavou	hnisavý	k2eNgFnSc7d1	nehnisavá
encefalomyelitidou	encefalomyelitida	k1gFnSc7	encefalomyelitida
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
kterou	který	k3yRgFnSc4	který
je	být	k5eAaImIp3nS	být
typický	typický	k2eAgInSc1d1	typický
zánět	zánět	k1gInSc1	zánět
ganglií	ganglie	k1gFnPc2	ganglie
dorzálních	dorzální	k2eAgInPc2d1	dorzální
rohů	roh	k1gInPc2	roh
míšních	míšní	k2eAgInPc2d1	míšní
<g/>
,	,	kIx,	,
degenerace	degenerace	k1gFnPc4	degenerace
neuronů	neuron	k1gInPc2	neuron
<g/>
,	,	kIx,	,
difuzní	difuzní	k2eAgFnPc4d1	difuzní
nebo	nebo	k8xC	nebo
lokální	lokální	k2eAgFnPc4d1	lokální
proliferace	proliferace	k1gFnPc4	proliferace
glie	gli	k1gInSc2	gli
(	(	kIx(	(
<g/>
glióza	glióza	k1gFnSc1	glióza
<g/>
)	)	kIx)	)
a	a	k8xC	a
perivaskulární	perivaskulární	k2eAgFnSc1d1	perivaskulární
lymfocytární	lymfocytární	k2eAgFnSc1d1	lymfocytární
infiltrace	infiltrace	k1gFnSc1	infiltrace
<g/>
.	.	kIx.	.
</s>
<s>
Charakteristickou	charakteristický	k2eAgFnSc7d1	charakteristická
lézí	léze	k1gFnSc7	léze
je	být	k5eAaImIp3nS	být
centrální	centrální	k2eAgFnSc1d1	centrální
chromatolýza	chromatolýza	k1gFnSc1	chromatolýza
neuronů	neuron	k1gInPc2	neuron
(	(	kIx(	(
<g/>
axonální	axonální	k2eAgFnPc4d1	axonální
reakce	reakce	k1gFnPc4	reakce
<g/>
)	)	kIx)	)
v	v	k7c6	v
jádrech	jádro	k1gNnPc6	jádro
mozkového	mozkový	k2eAgInSc2d1	mozkový
kmene	kmen	k1gInSc2	kmen
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
prodloužené	prodloužený	k2eAgFnSc6d1	prodloužená
míše	mícha	k1gFnSc6	mícha
<g/>
.	.	kIx.	.
</s>
<s>
Centrální	centrální	k2eAgFnSc1d1	centrální
chromatolýza	chromatolýza	k1gFnSc1	chromatolýza
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
bez	bez	k7c2	bez
buněčné	buněčný	k2eAgFnSc2d1	buněčná
reakce	reakce	k1gFnSc2	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Změny	změna	k1gFnPc1	změna
v	v	k7c6	v
dorzálních	dorzální	k2eAgFnPc6d1	dorzální
míšních	míšní	k2eAgFnPc6d1	míšní
gangliích	ganglie	k1gFnPc6	ganglie
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořit	k5eAaImNgInP	tvořit
hustými	hustý	k2eAgInPc7d1	hustý
agregáty	agregát	k1gInPc7	agregát
malých	malý	k2eAgInPc2d1	malý
lymfocytů	lymfocyt	k1gInPc2	lymfocyt
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
omezeny	omezit	k5eAaPmNgInP	omezit
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
ganglia	ganglion	k1gNnPc4	ganglion
<g/>
,	,	kIx,	,
nikdy	nikdy	k6eAd1	nikdy
nepřesahují	přesahovat	k5eNaImIp3nP	přesahovat
do	do	k7c2	do
nervů	nerv	k1gInPc2	nerv
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svalové	svalový	k2eAgFnSc6d1	svalová
vrstvě	vrstva	k1gFnSc6	vrstva
proventrikulu	proventrikul	k1gInSc2	proventrikul
<g/>
,	,	kIx,	,
žaludku	žaludek	k1gInSc2	žaludek
a	a	k8xC	a
v	v	k7c6	v
pankreatu	pankreas	k1gInSc6	pankreas
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
hyperplazii	hyperplazie	k1gFnSc3	hyperplazie
a	a	k8xC	a
zvýšení	zvýšení	k1gNnSc3	zvýšení
počtu	počet	k1gInSc2	počet
germinativních	germinativní	k2eAgNnPc2d1	germinativní
lymfocytárních	lymfocytární	k2eAgNnPc2d1	lymfocytární
ložisek	ložisko	k1gNnPc2	ložisko
<g/>
.	.	kIx.	.
</s>
<s>
Infikovaná	infikovaný	k2eAgFnSc1d1	infikovaná
nebo	nebo	k8xC	nebo
aktivně	aktivně	k6eAd1	aktivně
imunizovaná	imunizovaný	k2eAgFnSc1d1	imunizovaná
(	(	kIx(	(
<g/>
vakcinovaná	vakcinovaný	k2eAgFnSc1d1	vakcinovaná
<g/>
)	)	kIx)	)
drůbež	drůbež	k1gFnSc1	drůbež
produkuje	produkovat	k5eAaImIp3nS	produkovat
imunodifuzní	imunodifuzní	k2eAgFnPc4d1	imunodifuzní
protilátky	protilátka	k1gFnPc4	protilátka
prokazatelné	prokazatelný	k2eAgFnPc4d1	prokazatelná
za	za	k7c4	za
4-10	[number]	k4	4-10
dní	den	k1gInPc2	den
PI	pi	k0	pi
a	a	k8xC	a
neutralizační	neutralizační	k2eAgFnPc4d1	neutralizační
protilátky	protilátka	k1gFnPc4	protilátka
za	za	k7c4	za
11-14	[number]	k4	11-14
dní	den	k1gInPc2	den
PI	pi	k0	pi
<g/>
.	.	kIx.	.
</s>
<s>
Protilátky	protilátka	k1gFnPc1	protilátka
přetrvávají	přetrvávat	k5eAaImIp3nP	přetrvávat
na	na	k7c6	na
detekovatelné	detekovatelný	k2eAgFnSc6d1	detekovatelná
úrovni	úroveň	k1gFnSc6	úroveň
nejméně	málo	k6eAd3	málo
několik	několik	k4yIc1	několik
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
celoživotně	celoživotně	k6eAd1	celoživotně
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
hladina	hladina	k1gFnSc1	hladina
v	v	k7c6	v
séru	sérum	k1gNnSc6	sérum
nosnic	nosnice	k1gFnPc2	nosnice
však	však	k9	však
není	být	k5eNaImIp3nS	být
konstantní	konstantní	k2eAgFnSc1d1	konstantní
<g/>
,	,	kIx,	,
kolísá	kolísat	k5eAaImIp3nS	kolísat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
hejna	hejno	k1gNnSc2	hejno
i	i	k9	i
u	u	k7c2	u
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
nosnic	nosnice	k1gFnPc2	nosnice
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
imunních	imunní	k2eAgNnPc6d1	imunní
hejnech	hejno	k1gNnPc6	hejno
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
nosnice	nosnice	k1gFnPc4	nosnice
bez	bez	k7c2	bez
protilátek	protilátka	k1gFnPc2	protilátka
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
mladé	mladý	k2eAgFnPc1d1	mladá
slepice	slepice	k1gFnPc1	slepice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
obsahem	obsah	k1gInSc7	obsah
protilátek	protilátka	k1gFnPc2	protilátka
v	v	k7c6	v
séru	sérum	k1gNnSc6	sérum
nosnic	nosnice	k1gFnPc2	nosnice
<g/>
,	,	kIx,	,
ve	v	k7c6	v
žloutku	žloutek	k1gInSc6	žloutek
NV	NV	kA	NV
a	a	k8xC	a
v	v	k7c6	v
séru	sérum	k1gNnSc6	sérum
kuřat	kuře	k1gNnPc2	kuře
líhnutých	líhnutý	k2eAgNnPc2d1	líhnutý
z	z	k7c2	z
těchto	tento	k3xDgNnPc2	tento
vajec	vejce	k1gNnPc2	vejce
existuje	existovat	k5eAaImIp3nS	existovat
úzká	úzký	k2eAgFnSc1d1	úzká
korelace	korelace	k1gFnSc1	korelace
<g/>
.	.	kIx.	.
</s>
<s>
Kuřecí	kuřecí	k2eAgNnPc1d1	kuřecí
embrya	embryo	k1gNnPc1	embryo
pocházející	pocházející	k2eAgNnPc1d1	pocházející
od	od	k7c2	od
imunních	imunní	k2eAgFnPc2d1	imunní
nosnic	nosnice	k1gFnPc2	nosnice
jsou	být	k5eAaImIp3nP	být
rezistentní	rezistentní	k2eAgFnPc1d1	rezistentní
k	k	k7c3	k
infekci	infekce	k1gFnSc3	infekce
virem	vir	k1gInSc7	vir
AE	AE	kA	AE
do	do	k7c2	do
žloutkového	žloutkový	k2eAgInSc2d1	žloutkový
vaku	vak	k1gInSc2	vak
<g/>
,	,	kIx,	,
čehož	což	k3yRnSc2	což
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
v	v	k7c6	v
testu	test	k1gInSc6	test
vnímavosti	vnímavost	k1gFnSc2	vnímavost
kuřecích	kuřecí	k2eAgNnPc2d1	kuřecí
embryí	embryo	k1gNnPc2	embryo
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
potomstvo	potomstvo	k1gNnSc4	potomstvo
těchto	tento	k3xDgFnPc2	tento
nosnic	nosnice	k1gFnPc2	nosnice
je	být	k5eAaImIp3nS	být
méně	málo	k6eAd2	málo
vnímavé	vnímavý	k2eAgFnSc2d1	vnímavá
k	k	k7c3	k
orální	orální	k2eAgFnSc3d1	orální
infekci	infekce	k1gFnSc3	infekce
do	do	k7c2	do
8	[number]	k4	8
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
dne	den	k1gInSc2	den
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
<s>
Mateřské	mateřský	k2eAgFnPc1d1	mateřská
protilátky	protilátka	k1gFnPc1	protilátka
jsou	být	k5eAaImIp3nP	být
detekovatelné	detekovatelný	k2eAgFnPc1d1	detekovatelná
v	v	k7c6	v
séru	sérum	k1gNnSc6	sérum
kuřat	kuře	k1gNnPc2	kuře
až	až	k8xS	až
do	do	k7c2	do
stáří	stáří	k1gNnSc2	stáří
4-6	[number]	k4	4-6
týdnů	týden	k1gInPc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
Obsah	obsah	k1gInSc1	obsah
protilátek	protilátka	k1gFnPc2	protilátka
ve	v	k7c6	v
vejcích	vejce	k1gNnPc6	vejce
není	být	k5eNaImIp3nS	být
konstantní	konstantní	k2eAgNnSc1d1	konstantní
<g/>
,	,	kIx,	,
kolísá	kolísat	k5eAaImIp3nS	kolísat
i	i	k9	i
ve	v	k7c6	v
vejcích	vejce	k1gNnPc6	vejce
od	od	k7c2	od
stejné	stejný	k2eAgFnSc2d1	stejná
slepice	slepice	k1gFnSc2	slepice
<g/>
.	.	kIx.	.
</s>
<s>
Pasivní	pasivní	k2eAgFnPc1d1	pasivní
protilátky	protilátka	k1gFnPc1	protilátka
zabraňují	zabraňovat	k5eAaImIp3nP	zabraňovat
vývoji	vývoj	k1gInSc3	vývoj
klinické	klinický	k2eAgFnSc2d1	klinická
nemoci	nemoc	k1gFnSc2	nemoc
u	u	k7c2	u
kuřat	kuře	k1gNnPc2	kuře
a	a	k8xC	a
redukují	redukovat	k5eAaBmIp3nP	redukovat
množství	množství	k1gNnSc4	množství
i	i	k8xC	i
dobu	doba	k1gFnSc4	doba
vylučování	vylučování	k1gNnSc2	vylučování
AEV	AEV	kA	AEV
trusem	trus	k1gInSc7	trus
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vymizení	vymizení	k1gNnSc6	vymizení
pasivních	pasivní	k2eAgFnPc2d1	pasivní
protilátek	protilátka	k1gFnPc2	protilátka
se	se	k3xPyFc4	se
kuřata	kuře	k1gNnPc1	kuře
stávají	stávat	k5eAaImIp3nP	stávat
plně	plně	k6eAd1	plně
vnímavými	vnímavý	k2eAgInPc7d1	vnímavý
k	k	k7c3	k
infekci	infekce	k1gFnSc3	infekce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hejnech	hejno	k1gNnPc6	hejno
s	s	k7c7	s
poměrně	poměrně	k6eAd1	poměrně
nízkou	nízký	k2eAgFnSc7d1	nízká
imunitou	imunita	k1gFnSc7	imunita
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
vejce	vejce	k1gNnPc4	vejce
jak	jak	k6eAd1	jak
imunní	imunní	k2eAgMnSc1d1	imunní
<g/>
,	,	kIx,	,
tak	tak	k9	tak
vejce	vejce	k1gNnSc4	vejce
vnímavá	vnímavý	k2eAgNnPc4d1	vnímavé
s	s	k7c7	s
nízkým	nízký	k2eAgInSc7d1	nízký
nebo	nebo	k8xC	nebo
žádným	žádný	k3yNgInSc7	žádný
obsahem	obsah	k1gInSc7	obsah
protilátek	protilátka	k1gFnPc2	protilátka
<g/>
.	.	kIx.	.
</s>
<s>
Nervové	nervový	k2eAgInPc4d1	nervový
příznaky	příznak	k1gInPc4	příznak
u	u	k7c2	u
mladých	mladý	k2eAgNnPc2d1	mladé
kuřat	kuře	k1gNnPc2	kuře
nebo	nebo	k8xC	nebo
krůťat	krůtě	k1gNnPc2	krůtě
<g/>
,	,	kIx,	,
nepřítomnost	nepřítomnost	k1gFnSc1	nepřítomnost
patologicko-anatomických	patologickonatomický	k2eAgFnPc2d1	patologicko-anatomická
změn	změna	k1gFnPc2	změna
a	a	k8xC	a
lokalizace	lokalizace	k1gFnSc2	lokalizace
i	i	k9	i
charakter	charakter	k1gInSc4	charakter
histopatologických	histopatologický	k2eAgInPc2d1	histopatologický
nálezů	nález	k1gInPc2	nález
v	v	k7c6	v
CNS	CNS	kA	CNS
a	a	k8xC	a
viscerálních	viscerální	k2eAgInPc6d1	viscerální
orgánech	orgán	k1gInPc6	orgán
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
stanovit	stanovit	k5eAaPmF	stanovit
diagnózu	diagnóza	k1gFnSc4	diagnóza
AE	AE	kA	AE
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
ji	on	k3xPp3gFnSc4	on
potvrdit	potvrdit	k5eAaPmF	potvrdit
izolací	izolace	k1gFnSc7	izolace
viru	vir	k1gInSc2	vir
(	(	kIx(	(
<g/>
nejlépe	dobře	k6eAd3	dobře
z	z	k7c2	z
mozku	mozek	k1gInSc2	mozek
klinicky	klinicky	k6eAd1	klinicky
postižených	postižený	k2eAgNnPc2d1	postižené
kuřat	kuře	k1gNnPc2	kuře
<g/>
)	)	kIx)	)
ve	v	k7c6	v
vnímavých	vnímavý	k2eAgNnPc6d1	vnímavé
kuřecích	kuřecí	k2eAgNnPc6d1	kuřecí
embryích	embryo	k1gNnPc6	embryo
inokulací	inokulace	k1gFnPc2	inokulace
do	do	k7c2	do
žloutkového	žloutkový	k2eAgInSc2d1	žloutkový
vaku	vak	k1gInSc2	vak
(	(	kIx(	(
<g/>
nepohyblivost	nepohyblivost	k1gFnSc1	nepohyblivost
embryí	embryo	k1gNnPc2	embryo
<g/>
,	,	kIx,	,
zakrslost	zakrslost	k1gFnSc1	zakrslost
<g/>
,	,	kIx,	,
svalová	svalový	k2eAgFnSc1d1	svalová
atrofie	atrofie	k1gFnSc1	atrofie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
intracerebrální	intracerebrální	k2eAgFnSc7d1	intracerebrální
inokulací	inokulace	k1gFnSc7	inokulace
jednodenních	jednodenní	k2eAgNnPc2d1	jednodenní
kuřat	kuře	k1gNnPc2	kuře
nebo	nebo	k8xC	nebo
průkazem	průkaz	k1gInSc7	průkaz
virového	virový	k2eAgInSc2d1	virový
antigenu	antigen	k1gInSc2	antigen
ve	v	k7c6	v
tkáních	tkáň	k1gFnPc6	tkáň
nemocných	nemocná	k1gFnPc6	nemocná
kuřat	kuře	k1gNnPc2	kuře
pomocí	pomocí	k7c2	pomocí
imunofluorescence	imunofluorescence	k1gFnSc2	imunofluorescence
či	či	k8xC	či
imunodifuze	imunodifuze	k1gFnSc2	imunodifuze
<g/>
.	.	kIx.	.
</s>
<s>
Primoizolace	Primoizolace	k1gFnSc1	Primoizolace
viru	vir	k1gInSc2	vir
na	na	k7c6	na
buněčných	buněčný	k2eAgFnPc6d1	buněčná
kulturách	kultura	k1gFnPc6	kultura
nemá	mít	k5eNaImIp3nS	mít
praktický	praktický	k2eAgInSc1d1	praktický
význam	význam	k1gInSc1	význam
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
CPE	cpát	k5eAaImIp3nS	cpát
není	být	k5eNaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
pozorován	pozorovat	k5eAaImNgInS	pozorovat
<g/>
.	.	kIx.	.
</s>
<s>
Identifikace	identifikace	k1gFnSc1	identifikace
izolovaného	izolovaný	k2eAgInSc2d1	izolovaný
viru	vir	k1gInSc2	vir
nebo	nebo	k8xC	nebo
retrospektivní	retrospektivní	k2eAgFnSc1d1	retrospektivní
diagnóza	diagnóza	k1gFnSc1	diagnóza
infekce	infekce	k1gFnSc2	infekce
u	u	k7c2	u
nosnic	nosnice	k1gFnPc2	nosnice
či	či	k8xC	či
kontrola	kontrola	k1gFnSc1	kontrola
stavu	stav	k1gInSc2	stav
postvakcinační	postvakcinační	k2eAgFnSc2d1	postvakcinační
imunity	imunita	k1gFnSc2	imunita
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
sérologickými	sérologický	k2eAgInPc7d1	sérologický
testy	test	k1gInPc7	test
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
detekci	detekce	k1gFnSc4	detekce
humorální	humorální	k2eAgFnSc2d1	humorální
imunity	imunita	k1gFnSc2	imunita
u	u	k7c2	u
AE	AE	kA	AE
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
zejména	zejména	k9	zejména
neutralizační	neutralizační	k2eAgInSc4d1	neutralizační
test	test	k1gInSc4	test
<g/>
,	,	kIx,	,
imunofluorescenční	imunofluorescenční	k2eAgInSc4d1	imunofluorescenční
test	test	k1gInSc4	test
<g/>
,	,	kIx,	,
imunodifuze	imunodifuze	k1gFnSc1	imunodifuze
<g/>
,	,	kIx,	,
test	test	k1gInSc1	test
vnímavosti	vnímavost	k1gFnSc2	vnímavost
kuřecích	kuřecí	k2eAgNnPc2d1	kuřecí
embryí	embryo	k1gNnPc2	embryo
a	a	k8xC	a
ELISA	ELISA	kA	ELISA
<g/>
.	.	kIx.	.
</s>
<s>
Diferenciální	diferenciální	k2eAgFnSc1d1	diferenciální
diagnostika	diagnostika	k1gFnSc1	diagnostika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
terénních	terénní	k2eAgInPc6d1	terénní
případech	případ	k1gInPc6	případ
výskytu	výskyt	k1gInSc2	výskyt
AE	AE	kA	AE
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
odlišit	odlišit	k5eAaPmF	odlišit
Newcastleskou	Newcastleský	k2eAgFnSc4d1	Newcastleská
nemoc	nemoc	k1gFnSc4	nemoc
<g/>
,	,	kIx,	,
nutriční	nutriční	k2eAgFnPc1d1	nutriční
poruchy	porucha	k1gFnPc1	porucha
(	(	kIx(	(
<g/>
rachitida	rachitida	k1gFnSc1	rachitida
<g/>
,	,	kIx,	,
encefalomalacie	encefalomalacie	k1gFnSc1	encefalomalacie
<g/>
,	,	kIx,	,
nedostatek	nedostatek	k1gInSc1	nedostatek
riboflavinu	riboflavin	k1gInSc2	riboflavin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Markovu	Markův	k2eAgFnSc4d1	Markova
nemoc	nemoc	k1gFnSc4	nemoc
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
intoxikace	intoxikace	k1gFnPc1	intoxikace
doprovázené	doprovázený	k2eAgFnPc1d1	doprovázená
poruchami	porucha	k1gFnPc7	porucha
nervového	nervový	k2eAgInSc2d1	nervový
a	a	k8xC	a
pohybového	pohybový	k2eAgInSc2d1	pohybový
aparátu	aparát	k1gInSc2	aparát
<g/>
.	.	kIx.	.
</s>
<s>
Newcastleská	Newcastleský	k2eAgFnSc1d1	Newcastleská
nemoc	nemoc	k1gFnSc1	nemoc
(	(	kIx(	(
<g/>
ND	ND	kA	ND
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
u	u	k7c2	u
kuřat	kuře	k1gNnPc2	kuře
stejného	stejný	k2eAgInSc2d1	stejný
věku	věk	k1gInSc2	věk
jako	jako	k8xC	jako
při	při	k7c6	při
AE	AE	kA	AE
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
AE	AE	kA	AE
je	být	k5eAaImIp3nS	být
typická	typický	k2eAgFnSc1d1	typická
centrální	centrální	k2eAgFnSc1d1	centrální
chromatolýza	chromatolýza	k1gFnSc1	chromatolýza
neuronů	neuron	k1gInPc2	neuron
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
periferní	periferní	k2eAgFnSc2d1	periferní
chromatolýzy	chromatolýza	k1gFnSc2	chromatolýza
u	u	k7c2	u
ND	ND	kA	ND
<g/>
,	,	kIx,	,
glióza	glióza	k1gFnSc1	glióza
v	v	k7c6	v
n.	n.	k?	n.
rotundus	rotundus	k1gMnSc1	rotundus
a	a	k8xC	a
n.	n.	k?	n.
ovoidales	ovoidales	k1gInSc1	ovoidales
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
není	být	k5eNaImIp3nS	být
pozorována	pozorovat	k5eAaImNgFnS	pozorovat
u	u	k7c2	u
ND	ND	kA	ND
<g/>
,	,	kIx,	,
lymfatické	lymfatický	k2eAgInPc4d1	lymfatický
folikuly	folikul	k1gInPc4	folikul
ve	v	k7c6	v
svalové	svalový	k2eAgFnSc6d1	svalová
stěně	stěna	k1gFnSc6	stěna
proventrikulu	proventrikul	k1gInSc2	proventrikul
a	a	k8xC	a
v	v	k7c6	v
pankreatu	pankreas	k1gInSc6	pankreas
<g/>
.	.	kIx.	.
</s>
<s>
ND	ND	kA	ND
vzácně	vzácně	k6eAd1	vzácně
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
intersticiální	intersticiální	k2eAgFnSc4d1	intersticiální
pankreatitidu	pankreatitida	k1gFnSc4	pankreatitida
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
AE	AE	kA	AE
převládá	převládat	k5eAaImIp3nS	převládat
proliferace	proliferace	k1gFnPc4	proliferace
glie	gli	k1gFnSc2	gli
oproti	oproti	k7c3	oproti
změnám	změna	k1gFnPc3	změna
na	na	k7c6	na
cévách	céva	k1gFnPc6	céva
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
u	u	k7c2	u
ND	ND	kA	ND
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
opačně	opačně	k6eAd1	opačně
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
u	u	k7c2	u
ND	ND	kA	ND
i	i	k8xC	i
příznaky	příznak	k1gInPc1	příznak
respirační	respirační	k2eAgInPc1d1	respirační
a	a	k8xC	a
gastrointestinální	gastrointestinální	k2eAgInPc1d1	gastrointestinální
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
izolace	izolace	k1gFnSc2	izolace
paramyxoviru	paramyxovir	k1gInSc2	paramyxovir
či	či	k8xC	či
sérologického	sérologický	k2eAgNnSc2d1	sérologické
odlišení	odlišení	k1gNnSc2	odlišení
pomocí	pomocí	k7c2	pomocí
hemaglutinačně	hemaglutinačně	k6eAd1	hemaglutinačně
inhibičního	inhibiční	k2eAgInSc2d1	inhibiční
testu	test	k1gInSc2	test
<g/>
.	.	kIx.	.
</s>
<s>
Encefalomalacie	Encefalomalacie	k1gFnSc1	Encefalomalacie
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
u	u	k7c2	u
kuřat	kuře	k1gNnPc2	kuře
o	o	k7c4	o
2-3	[number]	k4	2-3
týdny	týden	k1gInPc4	týden
starších	starší	k1gMnPc2	starší
než	než	k8xS	než
při	při	k7c6	při
AE	AE	kA	AE
<g/>
.	.	kIx.	.
</s>
<s>
Histopatologicky	Histopatologicky	k6eAd1	Histopatologicky
se	se	k3xPyFc4	se
zjišťují	zjišťovat	k5eAaImIp3nP	zjišťovat
výrazné	výrazný	k2eAgFnPc1d1	výrazná
degenerativní	degenerativní	k2eAgFnPc1d1	degenerativní
a	a	k8xC	a
nekrotické	nekrotický	k2eAgFnPc1d1	nekrotická
změny	změna	k1gFnPc1	změna
<g/>
,	,	kIx,	,
doprovázené	doprovázený	k2eAgFnPc1d1	doprovázená
krváceninami	krvácenina	k1gFnPc7	krvácenina
a	a	k8xC	a
edémem	edém	k1gInSc7	edém
<g/>
.	.	kIx.	.
</s>
<s>
Markova	Markův	k2eAgFnSc1d1	Markova
nemoc	nemoc	k1gFnSc1	nemoc
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
obvykle	obvykle	k6eAd1	obvykle
u	u	k7c2	u
kuřat	kuře	k1gNnPc2	kuře
starších	starý	k2eAgNnPc2d2	starší
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
mohou	moct	k5eAaImIp3nP	moct
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
lymfoidní	lymfoidní	k2eAgInPc4d1	lymfoidní
nádory	nádor	k1gInPc4	nádor
a	a	k8xC	a
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
postižení	postižení	k1gNnSc3	postižení
periferních	periferní	k2eAgInPc2d1	periferní
nervů	nerv	k1gInPc2	nerv
<g/>
.	.	kIx.	.
</s>
<s>
Hypovitaminózu	hypovitaminóza	k1gFnSc4	hypovitaminóza
B	B	kA	B
lze	lze	k6eAd1	lze
upravit	upravit	k5eAaPmF	upravit
aplikací	aplikace	k1gFnPc2	aplikace
vysokých	vysoký	k2eAgFnPc2d1	vysoká
dávek	dávka	k1gFnPc2	dávka
vitamínů	vitamín	k1gInPc2	vitamín
komplexu	komplex	k1gInSc2	komplex
B.	B.	kA	B.
Při	při	k7c6	při
rachitidě	rachitida	k1gFnSc6	rachitida
jsou	být	k5eAaImIp3nP	být
deformace	deformace	k1gFnPc1	deformace
kostí	kost	k1gFnPc2	kost
a	a	k8xC	a
onemocnění	onemocnění	k1gNnSc1	onemocnění
nemá	mít	k5eNaImIp3nS	mít
infekční	infekční	k2eAgInSc4d1	infekční
charakter	charakter	k1gInSc4	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Účinná	účinný	k2eAgFnSc1d1	účinná
terapie	terapie	k1gFnSc1	terapie
kuřat	kuře	k1gNnPc2	kuře
postižených	postižený	k1gMnPc2	postižený
AE	AE	kA	AE
neexistuje	existovat	k5eNaImIp3nS	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Prevence	prevence	k1gFnSc1	prevence
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
izolaci	izolace	k1gFnSc6	izolace
chovů	chov	k1gInPc2	chov
a	a	k8xC	a
dodržování	dodržování	k1gNnSc4	dodržování
veterinárně	veterinárně	k6eAd1	veterinárně
hygienických	hygienický	k2eAgInPc2d1	hygienický
požadavků	požadavek	k1gInPc2	požadavek
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
hejn	hejno	k1gNnPc2	hejno
před	před	k7c7	před
zavlečením	zavlečení	k1gNnSc7	zavlečení
nákaz	nákaza	k1gFnPc2	nákaza
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc2	jejich
šíření	šíření	k1gNnSc2	šíření
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
epizootologii	epizootologie	k1gFnSc3	epizootologie
nemoci	nemoc	k1gFnSc3	nemoc
a	a	k8xC	a
všeobecnému	všeobecný	k2eAgNnSc3d1	všeobecné
rozšíření	rozšíření	k1gNnSc3	rozšíření
viru	vir	k1gInSc2	vir
AE	AE	kA	AE
v	v	k7c6	v
populaci	populace	k1gFnSc6	populace
drůbeže	drůbež	k1gFnSc2	drůbež
je	být	k5eAaImIp3nS	být
žádoucí	žádoucí	k2eAgMnSc1d1	žádoucí
líhnout	líhnout	k5eAaImF	líhnout
kuřata	kuře	k1gNnPc4	kuře
(	(	kIx(	(
<g/>
krůťata	krůtě	k1gNnPc1	krůtě
<g/>
)	)	kIx)	)
jen	jen	k9	jen
z	z	k7c2	z
imunních	imunní	k2eAgNnPc2d1	imunní
hejn	hejno	k1gNnPc2	hejno
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
jsou	být	k5eAaImIp3nP	být
tak	tak	k6eAd1	tak
chráněna	chránit	k5eAaImNgFnS	chránit
před	před	k7c7	před
kontaktní	kontaktní	k2eAgFnSc7d1	kontaktní
infekcí	infekce	k1gFnSc7	infekce
během	během	k7c2	během
kritických	kritický	k2eAgInPc2d1	kritický
prvních	první	k4xOgInPc2	první
2-3	[number]	k4	2-3
týdnů	týden	k1gInPc2	týden
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
vakcinací	vakcinace	k1gFnSc7	vakcinace
rozmnožovacích	rozmnožovací	k2eAgInPc2d1	rozmnožovací
chovů	chov	k1gInPc2	chov
před	před	k7c7	před
dosažením	dosažení	k1gNnSc7	dosažení
pohlavní	pohlavní	k2eAgFnSc2d1	pohlavní
dospělosti	dospělost	k1gFnSc2	dospělost
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
zabraňuje	zabraňovat	k5eAaImIp3nS	zabraňovat
šíření	šíření	k1gNnSc1	šíření
infekce	infekce	k1gFnSc2	infekce
vertikální	vertikální	k2eAgFnSc7d1	vertikální
cestou	cesta	k1gFnSc7	cesta
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
vakcinaci	vakcinace	k1gFnSc3	vakcinace
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
živé	živý	k2eAgInPc1d1	živý
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
i	i	k9	i
inaktivované	inaktivovaný	k2eAgFnPc4d1	inaktivovaná
vakcíny	vakcína	k1gFnPc4	vakcína
<g/>
.	.	kIx.	.
</s>
<s>
Živé	živý	k2eAgFnPc1d1	živá
vakcíny	vakcína	k1gFnPc1	vakcína
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
plně	plně	k6eAd1	plně
virulentní	virulentní	k2eAgInSc4d1	virulentní
virus	virus	k1gInSc4	virus
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
kmen	kmen	k1gInSc1	kmen
1143	[number]	k4	1143
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
mírně	mírně	k6eAd1	mírně
virulentní	virulentní	k2eAgInSc1d1	virulentní
terénní	terénní	k2eAgInSc1d1	terénní
kmen	kmen	k1gInSc1	kmen
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
udržuje	udržovat	k5eAaImIp3nS	udržovat
nízkými	nízký	k2eAgFnPc7d1	nízká
pasážemi	pasáž	k1gFnPc7	pasáž
na	na	k7c6	na
embryích	embryo	k1gNnPc6	embryo
(	(	kIx(	(
<g/>
maximálně	maximálně	k6eAd1	maximálně
2	[number]	k4	2
pasáže	pasáž	k1gFnPc1	pasáž
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
je	být	k5eAaImIp3nS	být
adaptován	adaptován	k2eAgMnSc1d1	adaptován
na	na	k7c4	na
embrya	embryo	k1gNnPc4	embryo
a	a	k8xC	a
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
schopnost	schopnost	k1gFnSc4	schopnost
vyvolat	vyvolat	k5eAaPmF	vyvolat
imunitu	imunita	k1gFnSc4	imunita
po	po	k7c6	po
orální	orální	k2eAgFnSc6d1	orální
aplikaci	aplikace	k1gFnSc6	aplikace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
se	se	k3xPyFc4	se
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
vakcinace	vakcinace	k1gFnSc1	vakcinace
chovných	chovný	k2eAgNnPc2d1	chovné
hejn	hejno	k1gNnPc2	hejno
v	v	k7c6	v
období	období	k1gNnSc6	období
od	od	k7c2	od
10	[number]	k4	10
<g/>
.	.	kIx.	.
týdne	týden	k1gInSc2	týden
věku	věk	k1gInSc2	věk
a	a	k8xC	a
nejpozději	pozdě	k6eAd3	pozdě
do	do	k7c2	do
4	[number]	k4	4
týdnů	týden	k1gInPc2	týden
před	před	k7c7	před
zahájením	zahájení	k1gNnSc7	zahájení
snášky	snáška	k1gFnSc2	snáška
<g/>
.	.	kIx.	.
</s>
<s>
Vakcinační	vakcinační	k2eAgInSc1d1	vakcinační
virus	virus	k1gInSc1	virus
se	se	k3xPyFc4	se
snadno	snadno	k6eAd1	snadno
šíří	šířit	k5eAaImIp3nS	šířit
v	v	k7c6	v
hejnu	hejno	k1gNnSc6	hejno
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
terénní	terénní	k2eAgInSc1d1	terénní
virus	virus	k1gInSc1	virus
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
vakcinovat	vakcinovat	k5eAaBmF	vakcinovat
pouze	pouze	k6eAd1	pouze
část	část	k1gFnSc4	část
zvířat	zvíře	k1gNnPc2	zvíře
v	v	k7c6	v
hejnu	hejno	k1gNnSc6	hejno
<g/>
,	,	kIx,	,
zbytek	zbytek	k1gInSc1	zbytek
se	se	k3xPyFc4	se
infikuje	infikovat	k5eAaBmIp3nS	infikovat
samovolně	samovolně	k6eAd1	samovolně
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
klecovém	klecový	k2eAgInSc6d1	klecový
odchovu	odchov	k1gInSc6	odchov
však	však	k9	však
nutno	nutno	k6eAd1	nutno
vakcinovat	vakcinovat	k5eAaBmF	vakcinovat
všechna	všechen	k3xTgNnPc4	všechen
zvířata	zvíře	k1gNnPc4	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sousedství	sousedství	k1gNnSc6	sousedství
vakcinovaného	vakcinovaný	k2eAgNnSc2d1	vakcinované
hejna	hejno	k1gNnSc2	hejno
nesmí	smět	k5eNaImIp3nS	smět
být	být	k5eAaImF	být
umístěna	umístit	k5eAaPmNgFnS	umístit
drůbež	drůbež	k1gFnSc1	drůbež
mladší	mladý	k2eAgFnSc1d2	mladší
6	[number]	k4	6
týdnů	týden	k1gInPc2	týden
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
vnímavé	vnímavý	k2eAgFnPc1d1	vnímavá
nosnice	nosnice	k1gFnPc1	nosnice
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
vakcinovaná	vakcinovaný	k2eAgFnSc1d1	vakcinovaná
drůbež	drůbež	k1gFnSc1	drůbež
vylučuje	vylučovat	k5eAaImIp3nS	vylučovat
do	do	k7c2	do
prostředí	prostředí	k1gNnSc2	prostředí
trusem	trus	k1gInSc7	trus
asi	asi	k9	asi
14-16	[number]	k4	14-16
dnů	den	k1gInPc2	den
po	po	k7c6	po
vakcinaci	vakcinace	k1gFnSc6	vakcinace
virulentní	virulentní	k2eAgInSc1d1	virulentní
vakcinační	vakcinační	k2eAgInSc1d1	vakcinační
virus	virus	k1gInSc1	virus
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
krůt	krůta	k1gFnPc2	krůta
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
stejných	stejný	k2eAgFnPc2d1	stejná
vakcín	vakcína	k1gFnPc2	vakcína
mezi	mezi	k7c7	mezi
23	[number]	k4	23
<g/>
.	.	kIx.	.
až	až	k9	až
28	[number]	k4	28
<g/>
.	.	kIx.	.
týdnem	týden	k1gInSc7	týden
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
nutnosti	nutnost	k1gFnSc2	nutnost
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
nosnice	nosnice	k1gFnSc1	nosnice
revakcinovat	revakcinovat	k5eAaBmF	revakcinovat
inaktivovanou	inaktivovaný	k2eAgFnSc7d1	inaktivovaná
vakcínou	vakcína	k1gFnSc7	vakcína
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
živých	živý	k2eAgFnPc2d1	živá
vakcín	vakcína	k1gFnPc2	vakcína
nejsou	být	k5eNaImIp3nP	být
vhodné	vhodný	k2eAgInPc1d1	vhodný
kmeny	kmen	k1gInPc1	kmen
viru	vir	k1gInSc2	vir
adaptované	adaptovaný	k2eAgNnSc1d1	adaptované
na	na	k7c4	na
kuřecí	kuřecí	k2eAgNnPc4d1	kuřecí
embrya	embryo	k1gNnPc4	embryo
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nemají	mít	k5eNaImIp3nP	mít
schopnost	schopnost	k1gFnSc4	schopnost
infikovat	infikovat	k5eAaBmF	infikovat
hostitele	hostitel	k1gMnPc4	hostitel
přes	přes	k7c4	přes
střevní	střevní	k2eAgInSc4d1	střevní
trakt	trakt	k1gInSc4	trakt
a	a	k8xC	a
při	při	k7c6	při
parenterální	parenterální	k2eAgFnSc6d1	parenterální
aplikaci	aplikace	k1gFnSc6	aplikace
(	(	kIx(	(
<g/>
metodou	metoda	k1gFnSc7	metoda
dvojího	dvojí	k4xRgInSc2	dvojí
vpichu	vpich	k1gInSc2	vpich
<g/>
,	,	kIx,	,
wing-web	wingba	k1gFnPc2	wing-wba
<g/>
)	)	kIx)	)
mohou	moct	k5eAaImIp3nP	moct
způsobovat	způsobovat	k5eAaImF	způsobovat
klinické	klinický	k2eAgNnSc4d1	klinické
onemocnění	onemocnění	k1gNnSc4	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Mrtvé	mrtvý	k2eAgFnPc1d1	mrtvá
vakcíny	vakcína	k1gFnPc1	vakcína
jsou	být	k5eAaImIp3nP	být
většinou	většina	k1gFnSc7	většina
inaktivovány	inaktivovat	k5eAaBmNgInP	inaktivovat
propiolaktonem	propiolakton	k1gInSc7	propiolakton
a	a	k8xC	a
používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
v	v	k7c6	v
chovech	chov	k1gInPc6	chov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
vakcinace	vakcinace	k1gFnSc2	vakcinace
živou	živý	k2eAgFnSc7d1	živá
vakcínou	vakcína	k1gFnSc7	vakcína
kontraindikována	kontraindikovat	k5eAaImNgFnS	kontraindikovat
<g/>
.	.	kIx.	.
</s>
<s>
Imunita	imunita	k1gFnSc1	imunita
po	po	k7c6	po
nich	on	k3xPp3gMnPc6	on
trvá	trvat	k5eAaImIp3nS	trvat
6-9	[number]	k4	6-9
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
.	.	kIx.	.
</s>
<s>
JURAJDA	JURAJDA	kA	JURAJDA
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
<g/>
.	.	kIx.	.
</s>
<s>
Nemoci	nemoc	k1gFnPc4	nemoc
drůbeže	drůbež	k1gFnSc2	drůbež
a	a	k8xC	a
ptactva	ptactvo	k1gNnSc2	ptactvo
–	–	k?	–
virové	virový	k2eAgFnSc2d1	virová
infekce	infekce	k1gFnSc2	infekce
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
:	:	kIx,	:
ES	ES	kA	ES
VFU	VFU	kA	VFU
Brno	Brno	k1gNnSc4	Brno
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
184	[number]	k4	184
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7305	[number]	k4	7305
<g/>
-	-	kIx~	-
<g/>
436	[number]	k4	436
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
