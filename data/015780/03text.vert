<s>
Bitva	bitva	k1gFnSc1
o	o	k7c4
Berlín	Berlín	k1gInSc4
</s>
<s>
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
Bitva	bitva	k1gFnSc1
o	o	k7c4
Berlín	Berlín	k1gInSc4
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Bitva	bitva	k1gFnSc1
o	o	k7c4
Berlín	Berlín	k1gInSc4
</s>
<s>
konflikt	konflikt	k1gInSc1
<g/>
:	:	kIx,
Velká	velký	k2eAgFnSc1d1
vlastenecká	vlastenecký	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
<s>
Braniborská	braniborský	k2eAgFnSc1d1
brána	brána	k1gFnSc1
po	po	k7c6
bojích	boj	k1gInPc6
</s>
<s>
trvání	trvání	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
–	–	k?
2	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1945	#num#	k4
</s>
<s>
místo	místo	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
Berlín	Berlín	k1gInSc1
<g/>
,	,	kIx,
Německo	Německo	k1gNnSc1
</s>
<s>
výsledek	výsledek	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
Sovětské	sovětský	k2eAgNnSc1d1
vítězství	vítězství	k1gNnSc1
</s>
<s>
strany	strana	k1gFnPc1
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
Polsko	Polsko	k1gNnSc1
</s>
<s>
Velkoněmecká	velkoněmecký	k2eAgFnSc1d1
říše	říše	k1gFnSc1
</s>
<s>
velitelé	velitel	k1gMnPc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
běloruský	běloruský	k2eAgInSc1d1
front	front	k1gInSc1
<g/>
:	:	kIx,
Georgij	Georgij	k1gFnSc1
Žukov	Žukov	k1gInSc1
<g/>
2	#num#	k4
<g/>
.	.	kIx.
běloruský	běloruský	k2eAgInSc1d1
front	front	k1gInSc1
<g/>
:	:	kIx,
Konstantin	Konstantin	k1gMnSc1
Rokossovskij	Rokossovskij	k1gMnSc1
<g/>
1	#num#	k4
<g/>
.	.	kIx.
ukrajinský	ukrajinský	k2eAgInSc1d1
front	front	k1gInSc1
<g/>
:	:	kIx,
Ivan	Ivan	k1gMnSc1
Koněv	Koněv	k1gMnSc1
</s>
<s>
Adolf	Adolf	k1gMnSc1
Hitler	Hitler	k1gMnSc1
†	†	k?
<g/>
Skupina	skupina	k1gFnSc1
armád	armáda	k1gFnPc2
Visla	visnout	k5eAaImAgFnS
<g/>
:	:	kIx,
Gotthard	Gotthard	k1gMnSc1
Heinrici	Heinric	k1gMnSc3
Kurt	kurt	k1gInSc4
von	von	k1gInSc1
Tippelskirch	Tippelskirch	k1gInSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
Skupina	skupina	k1gFnSc1
armád	armáda	k1gFnPc2
Střed	středa	k1gFnPc2
<g/>
:	:	kIx,
Ferdinand	Ferdinand	k1gMnSc1
SchörnerObrana	SchörnerObran	k1gMnSc2
Berlína	Berlín	k1gInSc2
<g/>
:	:	kIx,
Hellmuth	Hellmuth	k1gMnSc1
Reymannpoté	Reymannpota	k1gMnPc1
Helmuth	Helmuth	k1gMnSc1
Weidling	Weidling	k1gInSc1
</s>
<s>
Rudolf	Rudolf	k1gMnSc1
Sieckenius	Sieckenius	k1gMnSc1
†	†	k?
</s>
<s>
síla	síla	k1gFnSc1
</s>
<s>
2	#num#	k4
500	#num#	k4
000	#num#	k4
vojáků	voják	k1gMnPc2
<g/>
,	,	kIx,
<g/>
(	(	kIx(
<g/>
v	v	k7c6
tom	ten	k3xDgInSc6
185	#num#	k4
000	#num#	k4
Poláků	Polák	k1gMnPc2
<g/>
)	)	kIx)
<g/>
,6	,6	k4
250	#num#	k4
tanků	tank	k1gInPc2
<g/>
,7	,7	k4
500	#num#	k4
letadel	letadlo	k1gNnPc2
<g/>
,41	,41	k4
600	#num#	k4
děl	dělo	k1gNnPc2
a	a	k8xC
minometů	minomet	k1gInPc2
</s>
<s>
766	#num#	k4
750	#num#	k4
vojáků	voják	k1gMnPc2
<g/>
,1	,1	k4
519	#num#	k4
tanků	tank	k1gInPc2
a	a	k8xC
obrněných	obrněný	k2eAgNnPc2d1
vozidel	vozidlo	k1gNnPc2
<g/>
2	#num#	k4
224	#num#	k4
letadel	letadlo	k1gNnPc2
<g/>
9	#num#	k4
303	#num#	k4
děl	dělo	k1gNnPc2
a	a	k8xC
minometů	minomet	k1gInPc2
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
Berlínská	berlínský	k2eAgFnSc1d1
posádka	posádka	k1gFnSc1
<g/>
:	:	kIx,
<g/>
kolem	kolem	k7c2
50	#num#	k4
000	#num#	k4
vojáků	voják	k1gMnPc2
Wehrmachtu	wehrmacht	k1gInSc2
<g/>
,	,	kIx,
Waffen-SSa	Waffen-SSa	k1gMnSc1
Hitlerjugend	Hitlerjugend	k1gMnSc1
<g/>
.	.	kIx.
<g/>
V	v	k7c6
záloze	záloha	k1gFnSc6
bylo	být	k5eAaImAgNnS
40	#num#	k4
000	#num#	k4
příslušníků	příslušník	k1gMnPc2
Volkssturmudoplněných	Volkssturmudoplněný	k2eAgMnPc2d1
policejním	policejní	k2eAgInSc7d1
a	a	k8xC
hasičským	hasičský	k2eAgInSc7d1
sborem	sbor	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
ztráty	ztráta	k1gFnPc1
</s>
<s>
Archivní	archivní	k2eAgInSc1d1
výzkum	výzkum	k1gInSc1
<g/>
81	#num#	k4
116	#num#	k4
mrtvých	mrtvý	k1gMnPc2
či	či	k8xC
pohřešovaných	pohřešovaný	k1gMnPc2
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
včetně	včetně	k7c2
2	#num#	k4
825	#num#	k4
Poláků	Polák	k1gMnPc2
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
280	#num#	k4
251	#num#	k4
nemocných	nemocný	k1gMnPc2
či	či	k8xC
zraněných	zraněný	k1gMnPc2
Celkové	celkový	k2eAgFnSc2d1
ztráty	ztráta	k1gFnSc2
361,367	361,367	k4
mužů	muž	k1gMnPc2
<g/>
1	#num#	k4
997	#num#	k4
tanků	tank	k1gInPc2
<g/>
,2	,2	k4
108	#num#	k4
ks	ks	kA
dělostřelectva	dělostřelectvo	k1gNnSc2
<g/>
,917	,917	k4
letadel	letadlo	k1gNnPc2
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
88	#num#	k4
080	#num#	k4
mrtvých	mrtvý	k2eAgInPc2d1
<g/>
,479	,479	k4
298	#num#	k4
zajatců	zajatec	k1gMnPc2
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Celkové	celkový	k2eAgFnSc2d1
ztráty	ztráta	k1gFnSc2
937	#num#	k4
378	#num#	k4
mužůUvnitř	mužůUvnitř	k1gFnSc1
obrany	obrana	k1gFnSc2
Berlina	berlina	k1gFnSc1
<g/>
:	:	kIx,
<g/>
22	#num#	k4
000	#num#	k4
civilistů	civilista	k1gMnPc2
<g/>
,	,	kIx,
<g/>
okolo	okolo	k7c2
22	#num#	k4
000	#num#	k4
vojáků	voják	k1gMnPc2
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Bitva	bitva	k1gFnSc1
o	o	k7c4
Berlín	Berlín	k1gInSc4
(	(	kIx(
<g/>
útočícím	útočící	k2eAgInSc7d1
Sovětským	sovětský	k2eAgInSc7d1
svazem	svaz	k1gInSc7
označena	označit	k5eAaPmNgFnS
jako	jako	k8xC,k8xS
Berlínská	berlínský	k2eAgFnSc1d1
operace	operace	k1gFnSc1
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
jednou	jeden	k4xCgFnSc7
ze	z	k7c2
závěrečných	závěrečný	k2eAgFnPc2d1
ofenziv	ofenziva	k1gFnPc2
na	na	k7c6
evropských	evropský	k2eAgNnPc6d1
bojištích	bojiště	k1gNnPc6
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sovětské	sovětský	k2eAgFnPc4d1
jednotky	jednotka	k1gFnPc4
v	v	k7c6
průběhu	průběh	k1gInSc6
dubna	duben	k1gInSc2
1945	#num#	k4
postupně	postupně	k6eAd1
obklíčily	obklíčit	k5eAaPmAgFnP
a	a	k8xC
dobyly	dobýt	k5eAaPmAgFnP
Berlín	Berlín	k1gInSc4
<g/>
,	,	kIx,
hlavní	hlavní	k2eAgNnSc4d1
město	město	k1gNnSc4
nacistické	nacistický	k2eAgFnSc2d1
Třetí	třetí	k4xOgFnSc2
říše	říše	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Pozadí	pozadí	k1gNnSc1
</s>
<s>
V	v	k7c6
listopadu	listopad	k1gInSc6
1944	#num#	k4
vypracovalo	vypracovat	k5eAaPmAgNnS
sovětské	sovětský	k2eAgFnSc3d1
vrchní	vrchní	k1gFnSc3
velení	velení	k1gNnPc2
plán	plán	k1gInSc1
válečných	válečný	k2eAgFnPc2d1
akcí	akce	k1gFnPc2
v	v	k7c6
Evropě	Evropa	k1gFnSc6
na	na	k7c4
rok	rok	k1gInSc4
1945	#num#	k4
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yQgMnSc6,k3yIgMnSc6,k3yRgMnSc6
byl	být	k5eAaImAgMnS
mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
určen	určit	k5eAaPmNgInS
i	i	k9
termín	termín	k1gInSc1
zahájení	zahájení	k1gNnSc2
operací	operace	k1gFnPc2
na	na	k7c4
20	#num#	k4
<g/>
.	.	kIx.
leden	leden	k1gInSc4
1945	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgInSc4d1
úsilí	úsilí	k1gNnSc1
se	se	k3xPyFc4
přitom	přitom	k6eAd1
soustředilo	soustředit	k5eAaPmAgNnS
na	na	k7c4
útok	útok	k1gInSc4
směrem	směr	k1gInSc7
do	do	k7c2
nitra	nitro	k1gNnSc2
Třetí	třetí	k4xOgFnSc2
říše	říš	k1gFnSc2
–	–	k?
na	na	k7c4
Berlín	Berlín	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
k	k	k7c3
obtížné	obtížný	k2eAgFnSc3d1
situaci	situace	k1gFnSc3
spojenců	spojenec	k1gMnPc2
v	v	k7c6
Ardenách	Ardeny	k1gFnPc6
a	a	k8xC
po	po	k7c6
žádosti	žádost	k1gFnSc6
jejich	jejich	k3xOp3gMnPc2
představitelů	představitel	k1gMnPc2
o	o	k7c4
odlehčovací	odlehčovací	k2eAgFnSc4d1
ofenzívu	ofenzíva	k1gFnSc4
na	na	k7c6
východní	východní	k2eAgFnSc6d1
frontě	fronta	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
by	by	kYmCp3nS
vázala	vázat	k5eAaImAgFnS
další	další	k2eAgFnPc4d1
jednotky	jednotka	k1gFnPc4
<g/>
,	,	kIx,
Stalin	Stalin	k1gMnSc1
rozhodl	rozhodnout	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
útok	útok	k1gInSc1
začne	začít	k5eAaPmIp3nS
dříve	dříve	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
on	on	k3xPp3gMnSc1
si	se	k3xPyFc3
byl	být	k5eAaImAgMnS
však	však	k9
vědom	vědom	k2eAgMnSc1d1
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
že	že	k8xS
pokud	pokud	k8xS
spojenci	spojenec	k1gMnPc1
vyhrají	vyhrát	k5eAaPmIp3nP
bitvu	bitva	k1gFnSc4
v	v	k7c6
Ardenách	Ardeny	k1gFnPc6
<g/>
,	,	kIx,
mohou	moct	k5eAaImIp3nP
vstoupit	vstoupit	k5eAaPmF
do	do	k7c2
Berlína	Berlín	k1gInSc2
jako	jako	k9
první	první	k4xOgFnSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
bylo	být	k5eAaImAgNnS
pro	pro	k7c4
Stalina	Stalin	k1gMnSc4
nepřijatelné	přijatelný	k2eNgNnSc1d1
nejen	nejen	k6eAd1
z	z	k7c2
prestižních	prestižní	k2eAgInPc2d1
důvodů	důvod	k1gInPc2
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
z	z	k7c2
hlediska	hledisko	k1gNnSc2
jeho	jeho	k3xOp3gInPc2
plánů	plán	k1gInPc2
na	na	k7c4
budoucí	budoucí	k2eAgInSc4d1
postup	postup	k1gInSc4
proti	proti	k7c3
Západu	západ	k1gInSc3
<g/>
.	.	kIx.
</s>
<s>
Viselsko	Viselsko	k6eAd1
–	–	k?
oderská	oderský	k2eAgFnSc1d1
operace	operace	k1gFnSc1
</s>
<s>
Dne	den	k1gInSc2
12	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1945	#num#	k4
zahájila	zahájit	k5eAaPmAgFnS
Rudá	rudý	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
útok	útok	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
bývá	bývat	k5eAaImIp3nS
nazýván	nazývat	k5eAaImNgInS
jako	jako	k8xC,k8xS
Viselsko-oderská	viselsko-oderský	k2eAgFnSc1d1
operace	operace	k1gFnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
běloruský	běloruský	k2eAgInSc1d1
front	front	k1gInSc1
pod	pod	k7c7
vedením	vedení	k1gNnSc7
maršála	maršál	k1gMnSc2
Žukova	Žukovo	k1gNnSc2
dostal	dostat	k5eAaPmAgMnS
za	za	k7c4
úkol	úkol	k1gInSc4
udeřit	udeřit	k5eAaPmF
z	z	k7c2
magnuszewského	magnuszewský	k1gMnSc2
a	a	k8xC
pulawského	pulawského	k2eAgNnSc6d1
předmostí	předmostí	k1gNnSc6
směrem	směr	k1gInSc7
na	na	k7c4
Poznaň	Poznaň	k1gFnSc4
<g/>
,	,	kIx,
část	část	k1gFnSc1
sil	síla	k1gFnPc2
měla	mít	k5eAaImAgFnS
zaútočit	zaútočit	k5eAaPmF
proti	proti	k7c3
varšavskému	varšavský	k2eAgNnSc3d1
uskupení	uskupení	k1gNnSc3
německých	německý	k2eAgNnPc2d1
vojsk	vojsko	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Vojska	vojsko	k1gNnPc4
1	#num#	k4
<g/>
.	.	kIx.
ukrajinského	ukrajinský	k2eAgInSc2d1
frontu	front	k1gInSc2
pod	pod	k7c7
vedením	vedení	k1gNnSc7
maršála	maršál	k1gMnSc2
Koněva	Koněv	k1gMnSc2
měla	mít	k5eAaImAgFnS
podniknout	podniknout	k5eAaPmF
útok	útok	k1gInSc4
ze	z	k7c2
Sandoměřského	Sandoměřský	k2eAgNnSc2d1
předmostí	předmostí	k1gNnSc2
na	na	k7c4
Vratislav	Vratislav	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
10	#num#	k4
hodin	hodina	k1gFnPc2
zahájily	zahájit	k5eAaPmAgFnP
tisíce	tisíc	k4xCgInPc4
děl	dělo	k1gNnPc2
a	a	k8xC
minometů	minomet	k1gInPc2
na	na	k7c6
úseku	úsek	k1gInSc6
1	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
4	#num#	k4
<g/>
.	.	kIx.
ukrajinského	ukrajinský	k2eAgInSc2d1
frontu	front	k1gInSc2
drtivou	drtivý	k2eAgFnSc4d1
a	a	k8xC
nepřetržitou	přetržitý	k2eNgFnSc4d1
dvouhodinovou	dvouhodinový	k2eAgFnSc4d1
palbu	palba	k1gFnSc4
<g/>
,	,	kIx,
po	po	k7c6
které	který	k3yQgFnSc6,k3yRgFnSc6,k3yIgFnSc6
vyrazily	vyrazit	k5eAaPmAgFnP
úderné	úderný	k2eAgFnPc1d1
skupiny	skupina	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
provedly	provést	k5eAaPmAgFnP
průlom	průlom	k1gInSc4
fronty	fronta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
14	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
obdobně	obdobně	k6eAd1
zaútočila	zaútočit	k5eAaPmAgFnS
sovětská	sovětský	k2eAgFnSc1d1
vojska	vojsko	k1gNnPc4
1	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
2	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
3	#num#	k4
<g/>
.	.	kIx.
běloruského	běloruský	k2eAgInSc2d1
frontu	front	k1gInSc2
a	a	k8xC
i	i	k9
zde	zde	k6eAd1
byly	být	k5eAaImAgFnP
uskutečněny	uskutečněn	k2eAgInPc4d1
mohutné	mohutný	k2eAgInPc4d1
průlomy	průlom	k1gInPc4
fronty	fronta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Pohyb	pohyb	k1gInSc1
vojsk	vojsko	k1gNnPc2
Rudé	rudý	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
se	se	k3xPyFc4
rozběhl	rozběhnout	k5eAaPmAgInS
tak	tak	k6eAd1
rychle	rychle	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
i	i	k9
americké	americký	k2eAgFnSc2d1
noviny	novina	k1gFnSc2
New	New	k1gMnSc1
York	York	k1gInSc1
Times	Times	k1gInSc4
oznamovaly	oznamovat	k5eAaImAgFnP
<g/>
,	,	kIx,
že	že	k8xS
„	„	k?
<g/>
ruská	ruský	k2eAgFnSc1d1
ofenzíva	ofenzíva	k1gFnSc1
se	se	k3xPyFc4
rozvíjí	rozvíjet	k5eAaImIp3nS
s	s	k7c7
bleskovou	bleskový	k2eAgFnSc7d1
rychlostí	rychlost	k1gFnSc7
<g/>
,	,	kIx,
před	před	k7c7
níž	jenž	k3xRgFnSc7
blednou	blednout	k5eAaImIp3nP
německá	německý	k2eAgFnSc1d1
tažení	tažení	k1gNnSc6
1939	#num#	k4
v	v	k7c6
Polsku	Polsko	k1gNnSc6
a	a	k8xC
1940	#num#	k4
ve	v	k7c6
Francii	Francie	k1gFnSc6
<g/>
.	.	kIx.
<g/>
“	“	k?
Sovětské	sovětský	k2eAgFnSc2d1
tankové	tankový	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
a	a	k8xC
mechanizované	mechanizovaný	k2eAgInPc1d1
sbory	sbor	k1gInPc1
postupovaly	postupovat	k5eAaImAgInP
denně	denně	k6eAd1
45	#num#	k4
<g/>
–	–	k?
<g/>
70	#num#	k4
km	km	kA
<g/>
,	,	kIx,
obcházely	obcházet	k5eAaImAgInP
uzly	uzel	k1gInPc1
odporu	odpor	k1gInSc2
a	a	k8xC
rozvíjely	rozvíjet	k5eAaImAgInP
útok	útok	k1gInSc4
do	do	k7c2
hloubky	hloubka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Koncem	koncem	k7c2
ledna	leden	k1gInSc2
stála	stát	k5eAaImAgNnP
sovětská	sovětský	k2eAgNnPc1d1
vojska	vojsko	k1gNnPc1
na	na	k7c6
Odře	Odra	k1gFnSc6
<g/>
,	,	kIx,
zhruba	zhruba	k6eAd1
60	#num#	k4
<g/>
–	–	k?
<g/>
70	#num#	k4
km	km	kA
od	od	k7c2
Berlína	Berlín	k1gInSc2
a	a	k8xC
u	u	k7c2
Kostřína	Kostřín	k1gInSc2
si	se	k3xPyFc3
vybudovala	vybudovat	k5eAaPmAgFnS
několik	několik	k4yIc4
předmostí	předmostí	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
Hitlera	Hitler	k1gMnSc4
byla	být	k5eAaImAgFnS
informace	informace	k1gFnSc1
o	o	k7c6
této	tento	k3xDgFnSc6
situaci	situace	k1gFnSc6
šokující	šokující	k2eAgMnSc1d1
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
na	na	k7c4
radu	rada	k1gFnSc4
generálního	generální	k2eAgInSc2d1
štábu	štáb	k1gInSc2
rozkázal	rozkázat	k5eAaPmAgMnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
jeho	jeho	k3xOp3gFnSc1
Skupina	skupina	k1gFnSc1
armád	armáda	k1gFnPc2
Visla	Visla	k1gFnSc1
<g/>
,	,	kIx,
rozmístěná	rozmístěný	k2eAgFnSc1d1
v	v	k7c6
Pomořansku	Pomořansko	k1gNnSc6
<g/>
,	,	kIx,
zahájila	zahájit	k5eAaPmAgFnS
útok	útok	k1gInSc4
na	na	k7c4
severní	severní	k2eAgNnSc4d1
křídlo	křídlo	k1gNnSc4
sovětských	sovětský	k2eAgFnPc2d1
armád	armáda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Maršál	maršál	k1gMnSc1
Žukov	Žukov	k1gInSc4
si	se	k3xPyFc3
však	však	k9
byl	být	k5eAaImAgMnS
vědom	vědom	k2eAgMnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
útok	útok	k1gInSc1
na	na	k7c4
pravé	pravý	k2eAgNnSc4d1
křídlo	křídlo	k1gNnSc4
může	moct	k5eAaImIp3nS
znamenat	znamenat	k5eAaImF
oddělení	oddělení	k1gNnSc2
jeho	jeho	k3xOp3gNnPc2
útočných	útočný	k2eAgNnPc2d1
vojsk	vojsko	k1gNnPc2
se	s	k7c7
zázemím	zázemí	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
ignoroval	ignorovat	k5eAaImAgMnS
Stalina	Stalin	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
si	se	k3xPyFc3
přál	přát	k5eAaImAgMnS
pokračování	pokračování	k1gNnSc4
v	v	k7c6
útoku	útok	k1gInSc6
na	na	k7c4
Berlín	Berlín	k1gInSc4
<g/>
,	,	kIx,
a	a	k8xC
vyčlenil	vyčlenit	k5eAaPmAgMnS
dvě	dva	k4xCgFnPc4
tankové	tankový	k2eAgFnPc4d1
a	a	k8xC
dvě	dva	k4xCgFnPc4
vševojskové	vševojskový	k2eAgFnPc4d1
armády	armáda	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
provedly	provést	k5eAaPmAgFnP
útok	útok	k1gInSc4
na	na	k7c4
uskupení	uskupení	k1gNnSc4
německých	německý	k2eAgNnPc2d1
vojsk	vojsko	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
3	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
byla	být	k5eAaImAgFnS
Rudá	rudý	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
u	u	k7c2
Baltského	baltský	k2eAgNnSc2d1
moře	moře	k1gNnSc2
a	a	k8xC
odřízla	odříznout	k5eAaPmAgFnS
po	po	k7c6
souši	souš	k1gFnSc6
Východní	východní	k2eAgNnSc4d1
Prusko	Prusko	k1gNnSc4
od	od	k7c2
Německa	Německo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobný	podobný	k2eAgInSc1d1
problém	problém	k1gInSc1
jako	jako	k9
pro	pro	k7c4
Žukova	Žukovo	k1gNnPc4
v	v	k7c6
Pomořansku	Pomořansko	k1gNnSc6
nastal	nastat	k5eAaPmAgInS
pro	pro	k7c4
Koněva	Koněv	k1gMnSc4
i	i	k9
v	v	k7c6
německém	německý	k2eAgNnSc6d1
Slezsku	Slezsko	k1gNnSc6
(	(	kIx(
<g/>
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
po	po	k7c6
válce	válka	k1gFnSc6
připadlo	připadnout	k5eAaPmAgNnS
Polsku	Polsko	k1gNnSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kde	kde	k6eAd1
musel	muset	k5eAaImAgInS
1	#num#	k4
<g/>
.	.	kIx.
ukrajinský	ukrajinský	k2eAgInSc1d1
front	front	k1gInSc1
porazit	porazit	k5eAaPmF
mohutné	mohutný	k2eAgNnSc4d1
uskupení	uskupení	k1gNnSc4
o	o	k7c6
síle	síla	k1gFnSc6
33	#num#	k4
divizí	divize	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
průběhu	průběh	k1gInSc6
února	únor	k1gInSc2
i	i	k8xC
počátkem	počátkem	k7c2
března	březen	k1gInSc2
vedla	vést	k5eAaImAgFnS
sovětská	sovětský	k2eAgFnSc1d1
vojska	vojsko	k1gNnSc2
boje	boj	k1gInSc2
proti	proti	k7c3
německým	německý	k2eAgFnPc3d1
skupinám	skupina	k1gFnPc3
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
zůstaly	zůstat	k5eAaPmAgFnP
v	v	k7c6
obklíčení	obklíčení	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyčistěním	vyčistění	k1gNnSc7
oblastí	oblast	k1gFnPc2
východně	východně	k6eAd1
od	od	k7c2
Odry	Odra	k1gFnSc2
a	a	k8xC
Nisy	Nisa	k1gFnSc2
si	se	k3xPyFc3
Rudá	rudý	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
zabezpečila	zabezpečit	k5eAaPmAgFnS
prostor	prostor	k1gInSc4
pro	pro	k7c4
závěrečný	závěrečný	k2eAgInSc4d1
útok	útok	k1gInSc4
proti	proti	k7c3
Berlínu	Berlín	k1gInSc3
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
frontě	fronta	k1gFnSc6
jižně	jižně	k6eAd1
od	od	k7c2
Karpat	Karpaty	k1gInPc2
probíhaly	probíhat	k5eAaImAgInP
boje	boj	k1gInPc1
o	o	k7c4
Budapešť	Budapešť	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Němci	Němec	k1gMnPc1
podnikli	podniknout	k5eAaPmAgMnP
tři	tři	k4xCgFnPc4
protiofenzivy	protiofenziva	k1gFnPc4
<g/>
,	,	kIx,
aby	aby	k9
obklíčené	obklíčený	k2eAgNnSc4d1
hlavní	hlavní	k2eAgNnSc4d1
město	město	k1gNnSc4
jejich	jejich	k3xOp3gMnSc2
posledního	poslední	k2eAgMnSc2d1
zbývajícího	zbývající	k2eAgMnSc2d1
spojence	spojenec	k1gMnSc2
získali	získat	k5eAaPmAgMnP
zpět	zpět	k6eAd1
<g/>
.	.	kIx.
13	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
však	však	k9
Budapešť	Budapešť	k1gFnSc1
padla	padnout	k5eAaPmAgFnS,k5eAaImAgFnS
do	do	k7c2
sovětských	sovětský	k2eAgFnPc2d1
rukou	ruka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Adolf	Adolf	k1gMnSc1
Hitler	Hitler	k1gMnSc1
následně	následně	k6eAd1
nařídil	nařídit	k5eAaPmAgMnS
další	další	k2eAgInSc4d1
protiútok	protiútok	k1gInSc4
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc7
dalším	další	k2eAgInSc7d1
cílem	cíl	k1gInSc7
mělo	mít	k5eAaImAgNnS
být	být	k5eAaImF
vytvoření	vytvoření	k1gNnSc1
obranné	obranný	k2eAgFnSc2d1
linie	linie	k1gFnSc2
na	na	k7c6
řece	řeka	k1gFnSc6
Dunaj	Dunaj	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
16	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
však	však	k9
byla	být	k5eAaImAgFnS
tato	tento	k3xDgFnSc1
tzv.	tzv.	kA
operace	operace	k1gFnSc2
Jarní	jarní	k2eAgNnPc1d1
probuzení	probuzení	k1gNnPc1
zastavena	zastaven	k2eAgNnPc1d1
<g/>
,	,	kIx,
jednak	jednak	k8xC
sovětským	sovětský	k2eAgInSc7d1
odporem	odpor	k1gInSc7
<g/>
,	,	kIx,
jednak	jednak	k8xC
špatným	špatný	k2eAgNnSc7d1
počasím	počasí	k1gNnSc7
a	a	k8xC
blátem	bláto	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
několik	několik	k4yIc4
hodin	hodina	k1gFnPc2
<g/>
,	,	kIx,
celé	celý	k2eAgNnSc1d1
těžce	těžce	k6eAd1
nabyté	nabytý	k2eAgNnSc1d1
území	území	k1gNnSc1
<g/>
,	,	kIx,
o	o	k7c4
které	který	k3yIgFnPc4,k3yQgFnPc4,k3yRgFnPc4
probíhaly	probíhat	k5eAaImAgFnP
10	#num#	k4
dní	den	k1gInPc2
silné	silný	k2eAgInPc1d1
boje	boj	k1gInPc1
<g/>
,	,	kIx,
získaly	získat	k5eAaPmAgFnP
ofenzívou	ofenzíva	k1gFnSc7
sovětské	sovětský	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
zpět	zpět	k6eAd1
<g/>
.	.	kIx.
30	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
vstoupily	vstoupit	k5eAaPmAgFnP
sovětské	sovětský	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
na	na	k7c4
rakouské	rakouský	k2eAgNnSc4d1
území	území	k1gNnSc4
a	a	k8xC
do	do	k7c2
13	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
dobyly	dobýt	k5eAaPmAgFnP
Vídeň	Vídeň	k1gFnSc1
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
se	se	k3xPyFc4
po	po	k7c6
více	hodně	k6eAd2
než	než	k8xS
dvou	dva	k4xCgNnPc2
měsících	měsíc	k1gInPc6
bojů	boj	k1gInPc2
v	v	k7c6
těžkých	těžký	k2eAgFnPc6d1
horských	horský	k2eAgFnPc6d1
podmínkách	podmínka	k1gFnPc6
podařilo	podařit	k5eAaPmAgNnS
sovětským	sovětský	k2eAgFnPc3d1
a	a	k8xC
československým	československý	k2eAgFnPc3d1
jednotkám	jednotka	k1gFnPc3
Němce	Němec	k1gMnSc2
definitivně	definitivně	k6eAd1
vytlačit	vytlačit	k5eAaPmF
z	z	k7c2
Liptovského	liptovský	k2eAgMnSc2d1
Mikuláše	Mikuláš	k1gMnSc2
a	a	k8xC
zahájit	zahájit	k5eAaPmF
rychlý	rychlý	k2eAgInSc1d1
postup	postup	k1gInSc1
údolím	údolí	k1gNnSc7
Váhu	Váh	k1gInSc2
směrem	směr	k1gInSc7
na	na	k7c4
západ	západ	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
téže	týž	k3xTgFnSc6,k3xDgFnSc6
době	doba	k1gFnSc6
pravé	pravý	k2eAgNnSc4d1
křídlo	křídlo	k1gNnSc4
Žukovova	Žukovův	k2eAgNnSc2d1
1	#num#	k4
<g/>
.	.	kIx.
běloruského	běloruský	k2eAgInSc2d1
frontu	front	k1gInSc2
spolu	spolu	k6eAd1
s	s	k7c7
Rokossovského	Rokossovský	k2eAgMnSc2d1
2	#num#	k4
<g/>
.	.	kIx.
běloruským	běloruský	k2eAgInSc7d1
frontem	front	k1gInSc7
zajistilo	zajistit	k5eAaPmAgNnS
pobřeží	pobřeží	k1gNnSc1
baltského	baltský	k2eAgNnSc2d1
moře	moře	k1gNnSc2
v	v	k7c6
severovýchodním	severovýchodní	k2eAgNnSc6d1
Německu	Německo	k1gNnSc6
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
se	se	k3xPyFc4
zajistilo	zajistit	k5eAaPmAgNnS
před	před	k7c7
útokem	útok	k1gInSc7
z	z	k7c2
křídla	křídlo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Jaltská	jaltský	k2eAgFnSc1d1
konference	konference	k1gFnSc1
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
byla	být	k5eAaImAgFnS
na	na	k7c6
Jaltě	Jalta	k1gFnSc6
zahájena	zahájen	k2eAgFnSc1d1
konference	konference	k1gFnSc1
hlav	hlava	k1gFnPc2
Sovětského	sovětský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
<g/>
,	,	kIx,
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
a	a	k8xC
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
konferenci	konference	k1gFnSc6
se	se	k3xPyFc4
velmoci	velmoc	k1gFnPc1
dohodly	dohodnout	k5eAaPmAgFnP
na	na	k7c4
demilitarizaci	demilitarizace	k1gFnSc4
Německa	Německo	k1gNnSc2
a	a	k8xC
jeho	jeho	k3xOp3gNnSc4
rozdělení	rozdělení	k1gNnSc4
na	na	k7c4
4	#num#	k4
okupační	okupační	k2eAgFnSc2d1
zóny	zóna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
průběhu	průběh	k1gInSc6
března	březen	k1gInSc2
byli	být	k5eAaImAgMnP
Britové	Brit	k1gMnPc1
a	a	k8xC
Američani	Američan	k1gMnPc1
120	#num#	k4
km	km	kA
západně	západně	k6eAd1
od	od	k7c2
Berlína	Berlín	k1gInSc2
na	na	k7c6
řece	řeka	k1gFnSc6
Labe	Labe	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
18	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1945	#num#	k4
v	v	k7c6
době	doba	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
začala	začít	k5eAaPmAgFnS
rozjíždět	rozjíždět	k5eAaImF
sovětská	sovětský	k2eAgFnSc1d1
ofenzíva	ofenzíva	k1gFnSc1
však	však	k9
dosáhla	dosáhnout	k5eAaPmAgFnS
Montgomeryho	Montgomery	k1gMnSc2
21	#num#	k4
<g/>
.	.	kIx.
armádní	armádní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
přibližně	přibližně	k6eAd1
stejnou	stejný	k2eAgFnSc4d1
vzdálenost	vzdálenost	k1gFnSc4
od	od	k7c2
města	město	k1gNnSc2
jako	jako	k8xS,k8xC
Sověti	Sovět	k1gMnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mezi	mezi	k7c7
západními	západní	k2eAgMnPc7d1
Spojenci	spojenec	k1gMnPc7
však	však	k9
nepanoval	panovat	k5eNaImAgInS
jednotný	jednotný	k2eAgInSc1d1
názor	názor	k1gInSc1
na	na	k7c4
další	další	k2eAgNnPc4d1
pokračování	pokračování	k1gNnPc4
operací	operace	k1gFnPc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
Churchill	Churchill	k1gInSc1
<g/>
,	,	kIx,
Patton	Patton	k1gInSc1
<g/>
,	,	kIx,
Simpson	Simpson	k1gInSc1
a	a	k8xC
Brooke	Brooke	k1gFnSc1
hodlali	hodlat	k5eAaImAgMnP
v	v	k7c6
případě	případ	k1gInSc6
vhodných	vhodný	k2eAgFnPc2d1
podmínek	podmínka	k1gFnPc2
napadnout	napadnout	k5eAaPmF
Berlín	Berlín	k1gInSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
ho	on	k3xPp3gInSc4
získali	získat	k5eAaPmAgMnP
dříve	dříve	k6eAd2
než	než	k8xS
Sověti	Sovět	k1gMnPc1
<g/>
,	,	kIx,
tak	tak	k8xS,k8xC
Eisenhower	Eisenhower	k1gMnSc1
naproti	naproti	k7c3
tomu	ten	k3xDgNnSc3
předpokládal	předpokládat	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
Němci	Němec	k1gMnPc1
se	se	k3xPyFc4
budou	být	k5eAaImBp3nP
moci	moct	k5eAaImF
udržet	udržet	k5eAaPmF
hlavně	hlavně	k9
na	na	k7c6
jihu	jih	k1gInSc6
a	a	k8xC
v	v	k7c6
oblasti	oblast	k1gFnSc6
Alp	Alpy	k1gFnPc2
mohli	moct	k5eAaImAgMnP
začít	začít	k5eAaPmF
i	i	k9
partyzánskou	partyzánský	k2eAgFnSc4d1
válku	válka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navrhoval	navrhovat	k5eAaImAgInS
proto	proto	k8xC
útok	útok	k1gInSc1
směrem	směr	k1gInSc7
na	na	k7c4
Drážďany	Drážďany	k1gInPc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
dojde	dojít	k5eAaPmIp3nS
na	na	k7c6
Labi	Labe	k1gNnSc6
k	k	k7c3
setkání	setkání	k1gNnSc3
se	s	k7c7
sovětskými	sovětský	k2eAgFnPc7d1
silami	síla	k1gFnPc7
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
bude	být	k5eAaImBp3nS
Němci	Němec	k1gMnSc3
držené	držený	k2eAgNnSc1d1
území	území	k1gNnSc1
rozděleno	rozdělit	k5eAaPmNgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Eisenhower	Eisenhowra	k1gFnPc2
stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
například	například	k6eAd1
Bradley	Bradlea	k1gFnSc2
nechtěl	chtít	k5eNaImAgMnS
překračovat	překračovat	k5eAaImF
vymezenou	vymezený	k2eAgFnSc4d1
demarkační	demarkační	k2eAgFnSc4d1
linii	linie	k1gFnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
později	pozdě	k6eAd2
nemusel	muset	k5eNaImAgInS
Sovětům	Sovět	k1gMnPc3
získané	získaný	k2eAgNnSc4d1
území	území	k1gNnSc4
předávat	předávat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
rozhodnutí	rozhodnutí	k1gNnSc1
<g/>
,	,	kIx,
kterým	který	k3yRgNnSc7,k3yIgNnSc7,k3yQgNnSc7
odmítl	odmítnout	k5eAaPmAgMnS
se	s	k7c7
Stalinem	Stalin	k1gMnSc7
závodit	závodit	k5eAaImF
o	o	k7c4
Berlín	Berlín	k1gInSc4
<g/>
,	,	kIx,
patří	patřit	k5eAaImIp3nS
mezi	mezi	k7c7
nejkontroverznějším	kontroverzní	k2eAgInSc7d3
v	v	k7c6
celé	celý	k2eAgFnSc6d1
válce	válka	k1gFnSc6
a	a	k8xC
je	být	k5eAaImIp3nS
mnohými	mnohý	k2eAgFnPc7d1
dodnes	dodnes	k6eAd1
kritizováno	kritizovat	k5eAaImNgNnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
28	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1945	#num#	k4
proto	proto	k8xC
Eisenhower	Eisenhower	k1gMnSc1
poslal	poslat	k5eAaPmAgMnS
Stalinovi	Stalin	k1gMnSc3
depeši	depeše	k1gFnSc4
v	v	k7c6
níž	jenž	k3xRgFnSc6
ho	on	k3xPp3gMnSc4
informoval	informovat	k5eAaBmAgMnS
o	o	k7c6
svých	svůj	k3xOyFgInPc6
záměrech	záměr	k1gInPc6
a	a	k8xC
zároveň	zároveň	k6eAd1
žádal	žádat	k5eAaImAgMnS
lepší	dobrý	k2eAgFnSc4d2
koordinaci	koordinace	k1gFnSc4
i	i	k8xC
plány	plán	k1gInPc4
Rudé	rudý	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stalin	Stalin	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
pravděpodobně	pravděpodobně	k6eAd1
západním	západní	k2eAgMnPc3d1
Spojencům	spojenec	k1gMnPc3
v	v	k7c6
otázce	otázka	k1gFnSc6
útoku	útok	k1gInSc2
na	na	k7c4
Berlín	Berlín	k1gInSc4
nedůvěřoval	důvěřovat	k5eNaImAgMnS
<g/>
,	,	kIx,
souhlasil	souhlasit	k5eAaImAgMnS
s	s	k7c7
významem	význam	k1gInSc7
setkání	setkání	k1gNnSc2
na	na	k7c6
Labi	Labe	k1gNnSc6
v	v	k7c6
oblasti	oblast	k1gFnSc6
spojnice	spojnice	k1gFnSc1
Lipsko	Lipsko	k1gNnSc4
-	-	kIx~
Drážďany	Drážďany	k1gInPc1
<g/>
,	,	kIx,
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
však	však	k9
rozhodl	rozhodnout	k5eAaPmAgInS
o	o	k7c6
podstatně	podstatně	k6eAd1
silnější	silný	k2eAgFnSc6d2
ofenzívě	ofenzíva	k1gFnSc6
směrem	směr	k1gInSc7
na	na	k7c4
Berlín	Berlín	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
poslední	poslední	k2eAgFnSc4d1
ofenzívu	ofenzíva	k1gFnSc4
Stalin	Stalin	k1gMnSc1
nařídil	nařídit	k5eAaPmAgMnS
maršálům	maršál	k1gMnPc3
Žukovovi	Žukova	k1gMnSc3
a	a	k8xC
Koněvovi	Koněva	k1gMnSc3
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
po	po	k7c6
předešlých	předešlý	k2eAgInPc6d1
těžkých	těžký	k2eAgInPc6d1
bojích	boj	k1gInPc6
očekávali	očekávat	k5eAaImAgMnP
<g/>
,	,	kIx,
že	že	k8xS
jejich	jejich	k3xOp3gFnPc1
armády	armáda	k1gFnPc1
budou	být	k5eAaImBp3nP
moci	moct	k5eAaImF
do	do	k7c2
poloviny	polovina	k1gFnSc2
května	květen	k1gInSc2
odpočívat	odpočívat	k5eAaImF
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
připravili	připravit	k5eAaPmAgMnP
plány	plán	k1gInPc4
pro	pro	k7c4
útok	útok	k1gInSc4
na	na	k7c4
Berlín	Berlín	k1gInSc4
do	do	k7c2
48	#num#	k4
hodin	hodina	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Sovětské	sovětský	k2eAgFnSc2d1
síly	síla	k1gFnSc2
poznamenány	poznamenat	k5eAaPmNgInP
těžkými	těžký	k2eAgFnPc7d1
bitvami	bitva	k1gFnPc7
viselsko-oderské	viselsko-oderský	k2eAgFnSc2d1
operace	operace	k1gFnSc2
dostaly	dostat	k5eAaPmAgFnP
na	na	k7c4
přípravu	příprava	k1gFnSc4
do	do	k7c2
dalších	další	k2eAgInPc2d1
bojů	boj	k1gInPc2
necelých	celý	k2eNgInPc2d1
12	#num#	k4
-	-	kIx~
14	#num#	k4
dní	den	k1gInPc2
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stalin	Stalin	k1gMnSc1
se	se	k3xPyFc4
přitom	přitom	k6eAd1
snažil	snažit	k5eAaImAgMnS
využít	využít	k5eAaPmF
rivalitu	rivalita	k1gFnSc4
mezi	mezi	k7c7
Žukovem	Žukov	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
držel	držet	k5eAaImAgInS
pozice	pozice	k1gFnPc4
na	na	k7c6
nejkratší	krátký	k2eAgFnSc6d3
ose	osa	k1gFnSc6
postupu	postup	k1gInSc2
a	a	k8xC
Koněvem	Koněv	k1gInSc7
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnPc1
jednotky	jednotka	k1gFnPc1
se	se	k3xPyFc4
musely	muset	k5eAaImAgFnP
probojovat	probojovat	k5eAaPmF
o	o	k7c4
třetinu	třetina	k1gFnSc4
delší	dlouhý	k2eAgFnSc4d2
cestou	cesta	k1gFnSc7
<g/>
,	,	kIx,
na	na	k7c6
níž	jenž	k3xRgFnSc6
je	on	k3xPp3gFnPc4
čekalo	čekat	k5eAaImAgNnS
překročení	překročení	k1gNnSc1
několika	několik	k4yIc2
vodních	vodní	k2eAgInPc2d1
toků	tok	k1gInPc2
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
slabší	slabý	k2eAgFnSc2d2
nepřátelské	přátelský	k2eNgFnSc2d1
síly	síla	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Přípravy	příprava	k1gFnPc1
</s>
<s>
Východní	východní	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
od	od	k7c2
ledna	leden	k1gInSc2
do	do	k7c2
května	květen	k1gInSc2
1945	#num#	k4
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1945	#num#	k4
vydal	vydat	k5eAaPmAgMnS
Hitler	Hitler	k1gMnSc1
rozkaz	rozkaz	k1gInSc4
známý	známý	k2eAgInSc4d1
jako	jako	k8xS,k8xC
ARLZ-Maßnahmen	ARLZ-Maßnahmen	k1gInSc4
na	na	k7c6
jehož	jehož	k3xOyRp3gInSc6
základě	základ	k1gInSc6
měla	mít	k5eAaImAgFnS
armáda	armáda	k1gFnSc1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc1
ničit	ničit	k5eAaImF
všechny	všechen	k3xTgInPc4
objekty	objekt	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yRgInPc4,k3yQgInPc4,k3yIgInPc4
by	by	kYmCp3nS
mohl	moct	k5eAaImAgMnS
nepřítel	nepřítel	k1gMnSc1
využít	využít	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozkaz	rozkaz	k1gInSc1
se	se	k3xPyFc4
týkal	týkat	k5eAaImAgInS
i	i	k9
domů	dům	k1gInPc2
a	a	k8xC
průmyslu	průmysl	k1gInSc2
a	a	k8xC
nebyl	být	k5eNaImAgInS
plně	plně	k6eAd1
aplikován	aplikován	k2eAgInSc1d1
<g/>
,	,	kIx,
protože	protože	k8xS
mezi	mezi	k7c7
samotnými	samotný	k2eAgMnPc7d1
nacisty	nacista	k1gMnPc7
vyvolával	vyvolávat	k5eAaImAgMnS
rozpaky	rozpak	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedním	jeden	k4xCgInSc7
z	z	k7c2
prvních	první	k4xOgInPc2
<g/>
,	,	kIx,
kdo	kdo	k3yQnSc1,k3yInSc1,k3yRnSc1
se	se	k3xPyFc4
proti	proti	k7c3
rozkazu	rozkaz	k1gInSc3
postavili	postavit	k5eAaPmAgMnP
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
německý	německý	k2eAgMnSc1d1
ministr	ministr	k1gMnSc1
zbrojení	zbrojení	k1gNnSc2
Albert	Albert	k1gMnSc1
Speer	Speer	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
dubnu	duben	k1gInSc6
1945	#num#	k4
zaujala	zaujmout	k5eAaPmAgFnS
německá	německý	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
armád	armáda	k1gFnPc2
Visla	visnout	k5eAaImAgFnS
pod	pod	k7c7
vedením	vedení	k1gNnSc7
generála	generál	k1gMnSc2
Heinriciho	Heinrici	k1gMnSc2
spolu	spolu	k6eAd1
s	s	k7c7
některými	některý	k3yIgFnPc7
jednotkami	jednotka	k1gFnPc7
skupiny	skupina	k1gFnSc2
armád	armáda	k1gFnPc2
Střed	střed	k1gInSc4
obranné	obranný	k2eAgFnSc2d1
pozice	pozice	k1gFnSc2
podél	podél	k7c2
řeky	řeka	k1gFnSc2
Odry	Odra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nacistické	nacistický	k2eAgNnSc1d1
Německo	Německo	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
postrádalo	postrádat	k5eAaImAgNnS
bojeschopné	bojeschopný	k2eAgNnSc1d1
muže	muž	k1gMnSc4
do	do	k7c2
obrany	obrana	k1gFnSc2
oblasti	oblast	k1gFnSc2
intenzivně	intenzivně	k6eAd1
mobilizovalo	mobilizovat	k5eAaBmAgNnS
i	i	k9
jednotky	jednotka	k1gFnSc2
Volkssturmu	Volkssturm	k1gInSc2
(	(	kIx(
<g/>
domobrany	domobrana	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
Hitlerjugend	Hitlerjugend	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
jednotkách	jednotka	k1gFnPc6
domobrany	domobrana	k1gFnSc2
sloužily	sloužit	k5eAaImAgInP
zejména	zejména	k9
starší	starý	k2eAgMnSc1d2
muži	muž	k1gMnPc1
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
v	v	k7c4
Hitlerjugend	Hitlerjugend	k1gInSc4
převážně	převážně	k6eAd1
mladí	mladý	k2eAgMnPc1d1
chlapci	chlapec	k1gMnPc1
<g/>
,	,	kIx,
mnozí	mnohý	k2eAgMnPc1d1
ve	v	k7c6
věku	věk	k1gInSc6
12	#num#	k4
-	-	kIx~
13	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hitler	Hitler	k1gMnSc1
při	při	k7c6
obraně	obrana	k1gFnSc6
oblasti	oblast	k1gFnSc6
disponoval	disponovat	k5eAaBmAgInS
85	#num#	k4
divizemi	divize	k1gFnPc7
a	a	k8xC
řadou	řada	k1gFnSc7
menších	malý	k2eAgFnPc2d2
jednotek	jednotka	k1gFnPc2
o	o	k7c6
síle	síla	k1gFnSc6
asi	asi	k9
750	#num#	k4
000	#num#	k4
až	až	k6eAd1
milion	milion	k4xCgInSc1
mužů	muž	k1gMnPc2
a	a	k8xC
chlapců	chlapec	k1gMnPc2
<g/>
,	,	kIx,
s	s	k7c7
asi	asi	k9
500	#num#	k4
tanky	tank	k1gInPc4
a	a	k8xC
1	#num#	k4
000	#num#	k4
samohybnými	samohybný	k2eAgNnPc7d1
děly	dělo	k1gNnPc7
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
10	#num#	k4
400	#num#	k4
děly	dělo	k1gNnPc7
a	a	k8xC
minomety	minomet	k1gInPc7
a	a	k8xC
přibližně	přibližně	k6eAd1
3300	#num#	k4
letadly	letadlo	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Německou	německý	k2eAgFnSc4d1
obranu	obrana	k1gFnSc4
na	na	k7c6
nejkratších	krátký	k2eAgFnPc6d3
přístupových	přístupový	k2eAgFnPc6d1
cestách	cesta	k1gFnPc6
k	k	k7c3
Berlínu	Berlín	k1gInSc3
zajišťovala	zajišťovat	k5eAaImAgFnS
9	#num#	k4
<g/>
.	.	kIx.
armáda	armáda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc4
představy	představa	k1gFnPc4
o	o	k7c6
možnosti	možnost	k1gFnSc6
obrany	obrana	k1gFnSc2
však	však	k9
byly	být	k5eAaImAgFnP
čistě	čistě	k6eAd1
iluzorní	iluzorní	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stále	stále	k6eAd1
věřil	věřit	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
při	při	k7c6
těchto	tento	k3xDgInPc6
bojích	boj	k1gInPc6
dojde	dojít	k5eAaPmIp3nS
k	k	k7c3
zastavení	zastavení	k1gNnSc3
sovětského	sovětský	k2eAgInSc2d1
postupu	postup	k1gInSc2
kvůli	kvůli	k7c3
roztržce	roztržka	k1gFnSc3
mezi	mezi	k7c7
Spojenci	spojenec	k1gMnPc7
<g/>
,	,	kIx,
a	a	k8xC
že	že	k8xS
Berlín	Berlín	k1gInSc4
obsadí	obsadit	k5eAaPmIp3nP
anglo-americká	anglo-americký	k2eAgNnPc1d1
vojska	vojsko	k1gNnPc1
<g/>
,	,	kIx,
která	který	k3yRgNnPc1,k3yQgNnPc1,k3yIgNnPc1
se	se	k3xPyFc4
po	po	k7c6
kapitulaci	kapitulace	k1gFnSc6
Modelovy	Modelovy	k?
armády	armáda	k1gFnSc2
v	v	k7c6
Porúří	Porúří	k1gNnSc6
zastavila	zastavit	k5eAaPmAgFnS
zhruba	zhruba	k6eAd1
80	#num#	k4
km	km	kA
od	od	k7c2
Berlína	Berlín	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
neustálé	neustálý	k2eAgFnPc4d1
neshody	neshoda	k1gFnPc4
s	s	k7c7
náčelníkem	náčelník	k1gInSc7
svého	svůj	k3xOyFgInSc2
generálního	generální	k2eAgInSc2d1
štábu	štáb	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
navrhoval	navrhovat	k5eAaImAgInS
začít	začít	k5eAaPmF
jednat	jednat	k5eAaImF
o	o	k7c4
míru	míra	k1gFnSc4
odvolal	odvolat	k5eAaPmAgMnS
Heinze	Heinza	k1gFnSc3
Guderiana	Guderian	k1gMnSc2
a	a	k8xC
pokračoval	pokračovat	k5eAaImAgInS
v	v	k7c6
řízení	řízení	k1gNnSc6
operací	operace	k1gFnPc2
ze	z	k7c2
svého	svůj	k3xOyFgInSc2
bunkru	bunkr	k1gInSc2
<g/>
,	,	kIx,
pod	pod	k7c7
říšským	říšský	k2eAgNnSc7d1
kancléřstvím	kancléřství	k1gNnSc7
v	v	k7c6
Berlíně	Berlín	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hitlerovy	Hitlerův	k2eAgFnPc4d1
naděje	naděje	k1gFnPc4
na	na	k7c4
zlepšení	zlepšení	k1gNnSc4
vojenské	vojenský	k2eAgFnSc2d1
situace	situace	k1gFnSc2
Třetí	třetí	k4xOgFnSc2
říše	říš	k1gFnSc2
se	se	k3xPyFc4
však	však	k9
nesplnily	splnit	k5eNaPmAgFnP
<g/>
.	.	kIx.
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1945	#num#	k4
<g/>
,	,	kIx,
Joseph	Joseph	k1gInSc1
Goebbels	Goebbels	k1gInSc1
předává	předávat	k5eAaImIp3nS
16	#num#	k4
<g/>
letému	letý	k2eAgMnSc3d1
Willi	Will	k1gMnSc3
Hübnerovi	Hübner	k1gMnSc3
železný	železný	k2eAgInSc4d1
kříž	kříž	k1gInSc4
po	po	k7c6
znovudobytí	znovudobytí	k1gNnSc6
města	město	k1gNnSc2
Lauban	Laubana	k1gFnPc2
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
se	se	k3xPyFc4
rozhodl	rozhodnout	k5eAaPmAgInS
do	do	k7c2
předpokládané	předpokládaný	k2eAgFnSc2d1
poslední	poslední	k2eAgFnSc2d1
bitvy	bitva	k1gFnSc2
nasadit	nasadit	k5eAaPmF
zbytek	zbytek	k1gInSc4
svých	svůj	k3xOyFgFnPc2
sil	síla	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sovětské	sovětský	k2eAgFnPc1d1
ozbrojené	ozbrojený	k2eAgFnPc1d1
síly	síla	k1gFnPc1
totiž	totiž	k9
koncem	koncem	k7c2
války	válka	k1gFnSc2
začaly	začít	k5eAaPmAgInP
pociťovat	pociťovat	k5eAaImF
nedostatek	nedostatek	k1gInSc4
lidských	lidský	k2eAgFnPc2d1
rezerv	rezerva	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vojenské	vojenský	k2eAgFnPc4d1
jednotky	jednotka	k1gFnPc4
před	před	k7c7
bitvou	bitva	k1gFnSc7
byly	být	k5eAaImAgInP
doplněny	doplnit	k5eAaPmNgInP
materiálem	materiál	k1gInSc7
<g/>
,	,	kIx,
zbraněmi	zbraň	k1gFnPc7
a	a	k8xC
především	především	k6eAd1
obrněnou	obrněný	k2eAgFnSc7d1
technikou	technika	k1gFnSc7
<g/>
,	,	kIx,
oproti	oproti	k7c3
předešlým	předešlý	k2eAgFnPc3d1
operacím	operace	k1gFnPc3
však	však	k9
bylo	být	k5eAaImAgNnS
doplnění	doplnění	k1gNnSc1
živé	živý	k2eAgFnSc2d1
síly	síla	k1gFnSc2
malé	malý	k2eAgFnSc2d1
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
se	se	k3xPyFc4
po	po	k7c6
těžkých	těžký	k2eAgInPc6d1
bojích	boj	k1gInPc6
vzdal	vzdát	k5eAaPmAgInS
Königsberg	Königsberg	k1gInSc1
ve	v	k7c6
Východním	východní	k2eAgNnSc6d1
Prusku	Prusko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rokossovského	Rokossovský	k2eAgMnSc2d1
2	#num#	k4
<g/>
.	.	kIx.
běloruský	běloruský	k2eAgInSc1d1
front	front	k1gInSc1
tak	tak	k9
mohl	moct	k5eAaImAgInS
uvolnit	uvolnit	k5eAaPmF
většinu	většina	k1gFnSc4
svých	svůj	k3xOyFgFnPc2
sil	síla	k1gFnPc2
a	a	k8xC
začal	začít	k5eAaPmAgMnS
se	se	k3xPyFc4
urychleně	urychleně	k6eAd1
přesouvat	přesouvat	k5eAaImF
na	na	k7c4
západ	západ	k1gInSc4
k	k	k7c3
řece	řeka	k1gFnSc3
Odře	odřít	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
prvních	první	k4xOgInPc2
dvou	dva	k4xCgInPc2
dubnových	dubnový	k2eAgMnPc2d1
týdnů	týden	k1gInPc2
jednotky	jednotka	k1gFnSc2
1	#num#	k4
<g/>
.	.	kIx.
běloruského	běloruský	k2eAgInSc2d1
frontu	front	k1gInSc2
maršála	maršál	k1gMnSc2
Žukova	Žukovo	k1gNnSc2
uskutečnily	uskutečnit	k5eAaPmAgInP
přesun	přesun	k1gInSc4
velké	velký	k2eAgFnSc2d1
části	část	k1gFnSc2
svých	svůj	k3xOyFgFnPc2
sil	síla	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgInS
to	ten	k3xDgNnSc1
jeden	jeden	k4xCgInSc1
z	z	k7c2
největších	veliký	k2eAgInPc2d3
a	a	k8xC
nejrychlejších	rychlý	k2eAgInPc2d3
přesunů	přesun	k1gInPc2
obrovského	obrovský	k2eAgNnSc2d1
množství	množství	k1gNnSc2
lidí	člověk	k1gMnPc2
a	a	k8xC
techniky	technika	k1gFnSc2
ve	v	k7c6
druhé	druhý	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
oblasti	oblast	k1gFnSc2
Frankfurtu	Frankfurt	k1gInSc2
a	a	k8xC
Baltského	baltský	k2eAgNnSc2d1
moře	moře	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
uvolnily	uvolnit	k5eAaPmAgFnP
pozice	pozice	k1gFnPc1
pro	pro	k7c4
2	#num#	k4
<g/>
.	.	kIx.
běloruský	běloruský	k2eAgInSc1d1
front	front	k1gInSc1
se	se	k3xPyFc4
přesunuly	přesunout	k5eAaPmAgInP
na	na	k7c4
jih	jih	k1gInSc4
před	před	k7c7
Seelowské	Seelowské	k2eAgInPc7d1
výšiny	výšin	k1gInPc7
<g/>
,	,	kIx,
odkud	odkud	k6eAd1
měly	mít	k5eAaImAgFnP
útočit	útočit	k5eAaImF
přes	přes	k7c4
Odru	Odra	k1gFnSc4
přímo	přímo	k6eAd1
na	na	k7c4
západ	západ	k1gInSc4
směrem	směr	k1gInSc7
na	na	k7c4
Berlín	Berlín	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některým	některý	k3yIgFnPc3
německým	německý	k2eAgFnPc3d1
jednotkám	jednotka	k1gFnPc3
se	se	k3xPyFc4
při	při	k7c6
tomto	tento	k3xDgInSc6
sovětském	sovětský	k2eAgInSc6d1
přesunu	přesun	k1gInSc6
podařilo	podařit	k5eAaPmAgNnS
uniknout	uniknout	k5eAaPmF
dírami	díra	k1gFnPc7
ve	v	k7c6
frontové	frontový	k2eAgFnSc6d1
linii	linie	k1gFnSc6
<g/>
,	,	kIx,
šlo	jít	k5eAaImAgNnS
zejména	zejména	k9
o	o	k7c4
zbytky	zbytek	k1gInPc4
obklíčené	obklíčený	k2eAgInPc4d1
2	#num#	k4
<g/>
.	.	kIx.
armády	armáda	k1gFnSc2
generála	generál	k1gMnSc2
Dietricha	Dietrich	k1gMnSc2
von	von	k1gInSc4
Sauckena	Sauckeno	k1gNnSc2
z	z	k7c2
Gdaňsku	Gdaňsk	k1gInSc2
do	do	k7c2
delty	delta	k1gFnSc2
řeky	řeka	k1gFnSc2
Visly	Visla	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jižněji	jižně	k6eAd2
se	s	k7c7
1	#num#	k4
<g/>
.	.	kIx.
ukrajinský	ukrajinský	k2eAgInSc1d1
front	front	k1gInSc1
maršála	maršál	k1gMnSc2
Koněva	Koněv	k1gMnSc2
přesunul	přesunout	k5eAaPmAgMnS
z	z	k7c2
horního	horní	k2eAgNnSc2d1
Slezska	Slezsko	k1gNnSc2
do	do	k7c2
oblasti	oblast	k1gFnSc2
Nisy	Nisa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInSc7
úkolem	úkol	k1gInSc7
bylo	být	k5eAaImAgNnS
proniknout	proniknout	k5eAaPmF
z	z	k7c2
jihovýchodu	jihovýchod	k1gInSc2
přes	přes	k7c4
Odru	Odra	k1gFnSc4
a	a	k8xC
Nisu	Nisa	k1gFnSc4
a	a	k8xC
v	v	k7c6
případě	případ	k1gInSc6
dostatečně	dostatečně	k6eAd1
rychlého	rychlý	k2eAgInSc2d1
postupu	postup	k1gInSc2
obchvatů	obchvat	k1gInPc2
proniknout	proniknout	k5eAaPmF
k	k	k7c3
Berlínu	Berlín	k1gInSc3
z	z	k7c2
jihozápadu	jihozápad	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobnou	podobný	k2eAgFnSc4d1
roli	role	k1gFnSc4
svěřilo	svěřit	k5eAaPmAgNnS
nejvyšší	vysoký	k2eAgNnSc1d3
velení	velení	k1gNnSc1
i	i	k8xC
2	#num#	k4
<g/>
.	.	kIx.
běloruskému	běloruský	k2eAgInSc3d1
frontu	fronta	k1gFnSc4
maršála	maršál	k1gMnSc2
Rokossovského	Rokossovský	k2eAgMnSc2d1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
měl	mít	k5eAaImAgMnS
útočit	útočit	k5eAaImF
na	na	k7c6
Žukovově	Žukovův	k2eAgNnSc6d1
pravém	pravý	k2eAgNnSc6d1
křídle	křídlo	k1gNnSc6
a	a	k8xC
obklíčit	obklíčit	k5eAaPmF
Berlín	Berlín	k1gInSc4
ze	z	k7c2
severozápadu	severozápad	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkově	celkově	k6eAd1
tato	tento	k3xDgFnSc1
sovětská	sovětský	k2eAgFnSc1d1
vojska	vojsko	k1gNnSc2
čítala	čítat	k5eAaImAgFnS
2	#num#	k4
062	#num#	k4
200	#num#	k4
vojáků	voják	k1gMnPc2
<g/>
,	,	kIx,
41	#num#	k4
000	#num#	k4
děl	dělo	k1gNnPc2
<g/>
,	,	kIx,
raketometů	raketomet	k1gInPc2
a	a	k8xC
minometů	minomet	k1gInPc2
<g/>
,	,	kIx,
6	#num#	k4
250	#num#	k4
tanků	tank	k1gInPc2
<g/>
,	,	kIx,
samohybných	samohybný	k2eAgNnPc2d1
děl	dělo	k1gNnPc2
a	a	k8xC
také	také	k9
okolo	okolo	k7c2
7	#num#	k4
500	#num#	k4
letadel	letadlo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Bitva	bitva	k1gFnSc1
</s>
<s>
Obklíčení	obklíčení	k1gNnSc1
Berlína	Berlín	k1gInSc2
</s>
<s>
Příslušník	příslušník	k1gMnSc1
volkssturmu	volkssturma	k1gFnSc4
s	s	k7c7
panzerschreckem	panzerschreck	k1gInSc7
</s>
<s>
Sovětské	sovětský	k2eAgNnSc1d1
dělostřelectvo	dělostřelectvo	k1gNnSc1
před	před	k7c7
Berlínem	Berlín	k1gInSc7
</s>
<s>
Fáze	fáze	k1gFnSc1
od	od	k7c2
16	#num#	k4
<g/>
.	.	kIx.
do	do	k7c2
25	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1945	#num#	k4
</s>
<s>
Německé	německý	k2eAgInPc1d1
protiútoky	protiútok	k1gInPc1
</s>
<s>
Dne	den	k1gInSc2
14	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
začaly	začít	k5eAaPmAgInP
první	první	k4xOgInPc1
útoky	útok	k1gInPc1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
jednotky	jednotka	k1gFnPc1
Rudé	rudý	k2eAgFnPc1d1
armády	armáda	k1gFnPc1
prováděly	provádět	k5eAaImAgFnP
průzkum	průzkum	k1gInSc4
bojem	boj	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgInSc1d1
útok	útok	k1gInSc1
byl	být	k5eAaImAgInS
zahájen	zahájit	k5eAaPmNgInS
v	v	k7c6
noci	noc	k1gFnSc6
16	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
ve	v	k7c4
4	#num#	k4
hodiny	hodina	k1gFnSc2
ráno	ráno	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
úseku	úsek	k1gInSc6
Žukovova	Žukovův	k2eAgMnSc2d1
1	#num#	k4
<g/>
.	.	kIx.
běloruského	běloruský	k2eAgInSc2d1
frontu	front	k1gInSc2
v	v	k7c6
okolí	okolí	k1gNnSc6
předmostí	předmostí	k1gNnSc2
u	u	k7c2
Kostřína	Kostřín	k1gInSc2
nejprve	nejprve	k6eAd1
začala	začít	k5eAaPmAgFnS
35	#num#	k4
minut	minuta	k1gFnPc2
trvající	trvající	k2eAgFnSc1d1
mohutná	mohutný	k2eAgFnSc1d1
dělostřelecká	dělostřelecký	k2eAgFnSc1d1
příprava	příprava	k1gFnSc1
asi	asi	k9
9	#num#	k4
000	#num#	k4
tisíc	tisíc	k4xCgInSc4
děl	dělo	k1gNnPc2
a	a	k8xC
1500	#num#	k4
raketometů	raketomet	k1gInPc2
kaťuša	kaťuša	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šlo	jít	k5eAaImAgNnS
o	o	k7c6
jednu	jeden	k4xCgFnSc4
z	z	k7c2
nejsilnějších	silný	k2eAgFnPc2d3
dělostřeleckých	dělostřelecký	k2eAgFnPc2d1
příprav	příprava	k1gFnPc2
srovnatelnou	srovnatelný	k2eAgFnSc4d1
s	s	k7c7
palbou	palba	k1gFnSc7
vedenou	vedený	k2eAgFnSc7d1
během	během	k7c2
nejtěžších	těžký	k2eAgFnPc2d3
bitev	bitva	k1gFnPc2
první	první	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žukov	Žukov	k1gInSc1
soustředil	soustředit	k5eAaPmAgInS
295	#num#	k4
kanónů	kanón	k1gInPc2
na	na	k7c4
1	#num#	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc4
dělostřelci	dělostřelec	k1gMnPc1
vypálili	vypálit	k5eAaPmAgMnP
přes	přes	k7c4
7	#num#	k4
milionů	milion	k4xCgInPc2
granátů	granát	k1gInPc2
<g/>
,	,	kIx,
min	mina	k1gFnPc2
a	a	k8xC
raket	raketa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Palba	palba	k1gFnSc1
měla	mít	k5eAaImAgFnS
podle	podle	k7c2
očekávání	očekávání	k1gNnSc2
zcela	zcela	k6eAd1
zničit	zničit	k5eAaPmF
první	první	k4xOgFnSc4
linii	linie	k1gFnSc4
německé	německý	k2eAgFnSc2d1
obrany	obrana	k1gFnSc2
<g/>
,	,	kIx,
Heinrici	Heinrice	k1gFnSc4
však	však	k9
takový	takový	k3xDgInSc1
krok	krok	k1gInSc1
nepřítele	nepřítel	k1gMnSc4
předpokládal	předpokládat	k5eAaImAgInS
a	a	k8xC
noc	noc	k1gFnSc4
před	před	k7c7
útokem	útok	k1gInSc7
rozhodl	rozhodnout	k5eAaPmAgInS
tyto	tento	k3xDgFnPc4
své	svůj	k3xOyFgFnPc4
pozice	pozice	k1gFnPc4
držené	držený	k2eAgFnPc1d1
jednotkami	jednotka	k1gFnPc7
9	#num#	k4
<g/>
.	.	kIx.
armády	armáda	k1gFnSc2
vyprázdnit	vyprázdnit	k5eAaPmF
<g/>
,	,	kIx,
takže	takže	k8xS
německé	německý	k2eAgFnPc1d1
ztráty	ztráta	k1gFnPc1
nebyly	být	k5eNaImAgFnP
těžké	těžký	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhou	druhý	k4xOgFnSc7
fází	fáze	k1gFnSc7
útoku	útok	k1gInSc2
byl	být	k5eAaImAgInS
netradiční	tradiční	k2eNgInSc1d1
postup	postup	k1gInSc1
sovětských	sovětský	k2eAgFnPc2d1
úderných	úderný	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
v	v	k7c6
pozadí	pozadí	k1gNnSc6
se	s	k7c7
140	#num#	k4
svítícími	svítící	k2eAgInPc7d1
protiletadlovými	protiletadlový	k2eAgInPc7d1
světlomety	světlomet	k1gInPc7
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
měly	mít	k5eAaImAgInP
Němce	Němec	k1gMnSc4
oslepovat	oslepovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Světlomety	světlomet	k1gInPc1
však	však	k9
nebyly	být	k5eNaImAgInP
efektivní	efektivní	k2eAgInPc1d1
a	a	k8xC
pouze	pouze	k6eAd1
usnadňovaly	usnadňovat	k5eAaImAgFnP
zaměřování	zaměřování	k1gNnSc4
a	a	k8xC
střelbu	střelba	k1gFnSc4
německým	německý	k2eAgMnPc3d1
obráncům	obránce	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sovětský	sovětský	k2eAgInSc1d1
útok	útok	k1gInSc1
se	se	k3xPyFc4
v	v	k7c6
oblasti	oblast	k1gFnSc6
Seelowských	Seelowských	k2eAgInSc1d1
výšin	výšin	k1gInSc1
několik	několik	k4yIc1
kilometrů	kilometr	k1gInPc2
za	za	k7c7
Odrou	Odra	k1gFnSc7
významně	významně	k6eAd1
zpomalil	zpomalit	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obě	dva	k4xCgFnPc1
strany	strana	k1gFnPc1
se	se	k3xPyFc4
při	při	k7c6
tom	ten	k3xDgNnSc6
snažily	snažit	k5eAaImAgFnP
vést	vést	k5eAaImF
energické	energický	k2eAgFnPc1d1
operace	operace	k1gFnPc1
<g/>
,	,	kIx,
sovětské	sovětský	k2eAgInPc1d1
útoky	útok	k1gInPc1
často	často	k6eAd1
střídaly	střídat	k5eAaImAgInP
lokální	lokální	k2eAgInPc1d1
německé	německý	k2eAgInPc1d1
protiútoky	protiútok	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Němci	Němec	k1gMnPc7
zde	zde	k6eAd1
měli	mít	k5eAaImAgMnP
ve	v	k7c6
vhodném	vhodný	k2eAgInSc6d1
terénu	terén	k1gInSc6
vybudovanou	vybudovaný	k2eAgFnSc4d1
hluboce	hluboko	k6eAd1
členěnou	členěný	k2eAgFnSc4d1
obranu	obrana	k1gFnSc4
opírající	opírající	k2eAgFnSc4d1
se	se	k3xPyFc4
o	o	k7c4
okolní	okolní	k2eAgInPc4d1
pahorky	pahorek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navíc	navíc	k6eAd1
se	se	k3xPyFc4
jednotky	jednotka	k1gFnSc2
Wehrmachtu	wehrmacht	k1gInSc2
<g/>
,	,	kIx,
Waffen	Waffen	k1gInSc1
SS	SS	kA
<g/>
,	,	kIx,
Volkssturmu	Volkssturma	k1gFnSc4
i	i	k8xC
Hitlerjugend	Hitlerjugend	k1gInSc4
zarputile	zarputile	k6eAd1
bránily	bránit	k5eAaImAgInP
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
ze	z	k7c2
strachu	strach	k1gInSc2
jak	jak	k8xC,k8xS
z	z	k7c2
Rudé	rudý	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
<g/>
,	,	kIx,
tak	tak	k6eAd1
i	i	k9
z	z	k7c2
Hitlera	Hitler	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
nařídil	nařídit	k5eAaPmAgMnS
všechny	všechen	k3xTgInPc4
„	„	k?
<g/>
zbabělce	zbabělec	k1gMnSc2
<g/>
“	“	k?
na	na	k7c6
místě	místo	k1gNnSc6
popravit	popravit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svými	svůj	k3xOyFgInPc7
lidmi	člověk	k1gMnPc7
byli	být	k5eAaImAgMnP
věšeni	věsit	k5eAaImNgMnP
a	a	k8xC
stříleni	střílet	k5eAaImNgMnP
i	i	k8xC
němečtí	německý	k2eAgMnPc1d1
civilisté	civilista	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
se	se	k3xPyFc4
chtěli	chtít	k5eAaImAgMnP
vyhnout	vyhnout	k5eAaPmF
„	„	k?
<g/>
totální	totální	k2eAgFnSc3d1
mobilizaci	mobilizace	k1gFnSc3
<g/>
,	,	kIx,
<g/>
“	“	k?
či	či	k8xC
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
vyvěsili	vyvěsit	k5eAaPmAgMnP
bílé	bílý	k2eAgFnPc4d1
vlajky	vlajka	k1gFnPc4
<g/>
.	.	kIx.
17	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
se	se	k3xPyFc4
sice	sice	k8xC
Žukovovi	Žukova	k1gMnSc3
podařilo	podařit	k5eAaPmAgNnS
prolomit	prolomit	k5eAaPmF
německou	německý	k2eAgFnSc4d1
obranu	obrana	k1gFnSc4
na	na	k7c6
Seelowských	Seelowský	k2eAgFnPc6d1
výšinách	výšina	k1gFnPc6
<g/>
,	,	kIx,
ale	ale	k8xC
při	při	k7c6
dalším	další	k2eAgInSc6d1
postupu	postup	k1gInSc6
ho	on	k3xPp3gInSc2
zastavily	zastavit	k5eAaPmAgFnP
opevněné	opevněný	k2eAgFnPc1d1
pozice	pozice	k1gFnPc1
Weidlingova	Weidlingův	k2eAgNnSc2d1
LIV	LIV	kA
<g/>
.	.	kIx.
tankového	tankový	k2eAgInSc2d1
sboru	sbor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zničující	zničující	k2eAgInPc1d1
boje	boj	k1gInPc1
trvaly	trvat	k5eAaImAgInP
do	do	k7c2
19	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
začala	začít	k5eAaPmAgFnS
německá	německý	k2eAgFnSc1d1
obrana	obrana	k1gFnSc1
zahlcena	zahltit	k5eAaPmNgFnS
silnějším	silný	k2eAgInSc7d2
nepřítelem	nepřítel	k1gMnSc7
kolabovat	kolabovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
Druhý	druhý	k4xOgInSc1
směr	směr	k1gInSc1
sovětského	sovětský	k2eAgInSc2d1
útoku	útok	k1gInSc2
maršála	maršál	k1gMnSc4
Koněva	Koněv	k1gMnSc4
začal	začít	k5eAaPmAgMnS
dále	daleko	k6eAd2
na	na	k7c6
jihovýchodě	jihovýchod	k1gInSc6
dvouhodinovou	dvouhodinový	k2eAgFnSc7d1
dělostřeleckou	dělostřelecký	k2eAgFnSc7d1
přípravou	příprava	k1gFnSc7
po	po	k7c6
níž	jenž	k3xRgFnSc6
následoval	následovat	k5eAaImAgInS
útok	útok	k1gInSc4
5	#num#	k4
vševojskových	vševojskový	k2eAgMnPc2d1
a	a	k8xC
2	#num#	k4
tankových	tankový	k2eAgFnPc2d1
armád	armáda	k1gFnPc2
pod	pod	k7c7
krytím	krytí	k1gNnSc7
kouřové	kouřový	k2eAgFnSc2d1
clony	clona	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Už	už	k6eAd1
v	v	k7c6
odpoledních	odpolední	k2eAgFnPc6d1
hodinách	hodina	k1gFnPc6
postavili	postavit	k5eAaPmAgMnP
sovětští	sovětský	k2eAgMnPc1d1
ženisté	ženista	k1gMnPc1
několik	několik	k4yIc4
60	#num#	k4
tunových	tunový	k2eAgInPc2d1
mostů	most	k1gInPc2
přes	přes	k7c4
Nisu	Nisa	k1gFnSc4
<g/>
,	,	kIx,
po	po	k7c6
kterých	který	k3yIgMnPc6,k3yRgMnPc6,k3yQgMnPc6
mohla	moct	k5eAaImAgFnS
na	na	k7c4
předmostí	předmostí	k1gNnSc4
na	na	k7c6
druhém	druhý	k4xOgInSc6
břehu	břeh	k1gInSc6
přejíždět	přejíždět	k5eAaImF
i	i	k9
těžká	těžký	k2eAgFnSc1d1
technika	technika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sovětským	sovětský	k2eAgFnPc3d1
jednotkám	jednotka	k1gFnPc3
se	se	k3xPyFc4
podařilo	podařit	k5eAaPmAgNnS
prolomit	prolomit	k5eAaPmF
pozice	pozice	k1gFnPc4
4	#num#	k4
<g/>
.	.	kIx.
tankové	tankový	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
a	a	k8xC
18	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
se	se	k3xPyFc4
dostat	dostat	k5eAaPmF
ke	k	k7c3
Sprévě	Spréva	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stalin	Stalin	k1gMnSc1
spokojený	spokojený	k2eAgMnSc1d1
s	s	k7c7
takovým	takový	k3xDgInSc7
rychlým	rychlý	k2eAgInSc7d1
postupem	postup	k1gInSc7
dovolil	dovolit	k5eAaPmAgInS
Koněvovi	Koněva	k1gMnSc3
otočit	otočit	k5eAaPmF
svá	svůj	k3xOyFgNnPc4
vojska	vojsko	k1gNnPc4
na	na	k7c4
Berlín	Berlín	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
Sprévy	Spréva	k1gFnSc2
vyrazily	vyrazit	k5eAaPmAgFnP
Koněvovy	Koněvův	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
k	k	k7c3
Berlínu	Berlín	k1gInSc3
<g/>
,	,	kIx,
kde	kde	k6eAd1
začaly	začít	k5eAaPmAgFnP
obkličovací	obkličovací	k2eAgInSc4d1
manévr	manévr	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Rokossovského	Rokossovský	k2eAgNnSc2d1
2	#num#	k4
<g/>
.	.	kIx.
běloruský	běloruský	k2eAgInSc1d1
front	front	k1gInSc1
zaútočil	zaútočit	k5eAaPmAgInS
přes	přes	k7c4
Odru	Odra	k1gFnSc4
20	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čelila	čelit	k5eAaImAgFnS
mu	on	k3xPp3gMnSc3
německá	německý	k2eAgFnSc1d1
3	#num#	k4
<g/>
.	.	kIx.
tanková	tankový	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
Hasso	Hassa	k1gFnSc5
von	von	k1gInSc4
Mantufela	Mantufel	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rokossovskij	Rokossovskij	k1gMnSc1
pronikl	proniknout	k5eAaPmAgMnS
podél	podél	k7c2
německého	německý	k2eAgNnSc2d1
pobřeží	pobřeží	k1gNnSc2
severně	severně	k6eAd1
od	od	k7c2
Berlína	Berlín	k1gInSc2
a	a	k8xC
na	na	k7c6
řece	řeka	k1gFnSc6
Labe	Labe	k1gNnSc2
se	se	k3xPyFc4
spojil	spojit	k5eAaPmAgMnS
s	s	k7c7
anglo-americkými	anglo-americký	k2eAgNnPc7d1
vojsky	vojsko	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následně	následně	k6eAd1
změnil	změnit	k5eAaPmAgInS
směr	směr	k1gInSc4
postupu	postup	k1gInSc2
a	a	k8xC
udeřil	udeřit	k5eAaPmAgMnS
na	na	k7c4
město	město	k1gNnSc4
ze	z	k7c2
severu	sever	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
a	a	k8xC
se	se	k3xPyFc4
jednotkám	jednotka	k1gFnPc3
3	#num#	k4
<g/>
.	.	kIx.
gardové	gardový	k2eAgFnPc1d1
tankové	tankový	k2eAgFnPc1d1
armády	armáda	k1gFnPc1
generála	generál	k1gMnSc2
Rybalka	Rybalka	k1gFnSc1
podařilo	podařit	k5eAaPmAgNnS
proniknout	proniknout	k5eAaPmF
do	do	k7c2
oblasti	oblast	k1gFnSc2
Zossenu	Zossen	k2eAgFnSc4d1
<g/>
,	,	kIx,
sídla	sídlo	k1gNnSc2
hlavního	hlavní	k2eAgNnSc2d1
velitelství	velitelství	k1gNnSc2
Wehrmachtu	wehrmacht	k1gInSc2
s	s	k7c7
největší	veliký	k2eAgFnSc7d3
telefonní	telefonní	k2eAgFnSc7d1
centrálou	centrála	k1gFnSc7
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Němci	Němec	k1gMnPc1
z	z	k7c2
oblasti	oblast	k1gFnSc2
před	před	k7c7
postupujícím	postupující	k2eAgMnSc7d1
nepřítelem	nepřítel	k1gMnSc7
unikli	uniknout	k5eAaPmAgMnP
tak	tak	k6eAd1
rychle	rychle	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
sovětští	sovětský	k2eAgMnPc1d1
vojáci	voják	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
se	se	k3xPyFc4
dostali	dostat	k5eAaPmAgMnP
do	do	k7c2
centrály	centrála	k1gFnSc2
<g/>
,	,	kIx,
ještě	ještě	k9
našli	najít	k5eAaPmAgMnP
zvonící	zvonící	k2eAgInPc4d1
telefony	telefon	k1gInPc4
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Dne	den	k1gInSc2
22	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
uspořádal	uspořádat	k5eAaPmAgMnS
Adolf	Adolf	k1gMnSc1
Hitler	Hitler	k1gMnSc1
poslední	poslední	k2eAgFnSc4d1
operační	operační	k2eAgFnSc4d1
poradu	porada	k1gFnSc4
<g/>
,	,	kIx,
při	při	k7c6
níž	jenž	k3xRgFnSc6
bylo	být	k5eAaImAgNnS
rozhodnuto	rozhodnout	k5eAaPmNgNnS
stáhnout	stáhnout	k5eAaPmF
všechna	všechen	k3xTgNnPc1
vojska	vojsko	k1gNnPc1
ze	z	k7c2
západní	západní	k2eAgFnSc2d1
fronty	fronta	k1gFnSc2
a	a	k8xC
vrhnout	vrhnout	k5eAaImF,k5eAaPmF
je	být	k5eAaImIp3nS
do	do	k7c2
bojů	boj	k1gInPc2
o	o	k7c4
Berlín	Berlín	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Týž	týž	k3xTgInSc4
den	den	k1gInSc4
se	se	k3xPyFc4
německá	německý	k2eAgFnSc1d1
12	#num#	k4
<g/>
.	.	kIx.
armáda	armáda	k1gFnSc1
generála	generál	k1gMnSc2
Walthera	Walthera	k1gFnSc1
Wencka	Wencka	k1gFnSc1
obrátila	obrátit	k5eAaPmAgFnS
na	na	k7c4
východ	východ	k1gInSc4
a	a	k8xC
začala	začít	k5eAaPmAgFnS
se	se	k3xPyFc4
narychlo	narychlo	k6eAd1
přemisťovat	přemisťovat	k5eAaImF
k	k	k7c3
Berlínu	Berlín	k1gInSc3
<g/>
,	,	kIx,
kam	kam	k6eAd1
se	se	k3xPyFc4
však	však	k9
nedostala	dostat	k5eNaPmAgFnS
<g/>
.	.	kIx.
20	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
v	v	k7c4
den	den	k1gInSc4
Hitlerových	Hitlerových	k2eAgFnSc2d1
56	#num#	k4
<g/>
.	.	kIx.
narozenin	narozeniny	k1gFnPc2
se	se	k3xPyFc4
sovětská	sovětský	k2eAgNnPc1d1
vojska	vojsko	k1gNnPc1
přiblížila	přiblížit	k5eAaPmAgNnP
k	k	k7c3
Berlínu	Berlín	k1gInSc3
natolik	natolik	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
na	na	k7c4
něj	on	k3xPp3gMnSc4
mohla	moct	k5eAaImAgFnS
vést	vést	k5eAaImF
dělostřeleckou	dělostřelecký	k2eAgFnSc4d1
palbu	palba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vojáci	voják	k1gMnPc1
1	#num#	k4
<g/>
.	.	kIx.
běloruského	běloruský	k2eAgInSc2d1
frontu	front	k1gInSc2
město	město	k1gNnSc4
ostřelovali	ostřelovat	k5eAaImAgMnP
dokud	dokud	k6eAd1
se	se	k3xPyFc4
nevzdalo	vzdát	k5eNaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hmotnost	hmotnost	k1gFnSc1
munice	munice	k1gFnSc2
vypálené	vypálený	k2eAgFnSc2d1
sovětským	sovětský	k2eAgNnSc7d1
dělostřelectvem	dělostřelectvo	k1gNnSc7
během	během	k7c2
bitvy	bitva	k1gFnSc2
byla	být	k5eAaImAgFnS
větší	veliký	k2eAgFnSc1d2
než	než	k8xS
celková	celkový	k2eAgFnSc1d1
tonáž	tonáž	k1gFnSc1
leteckých	letecký	k2eAgFnPc2d1
pum	puma	k1gFnPc2
<g/>
,	,	kIx,
kterými	který	k3yQgFnPc7,k3yRgFnPc7,k3yIgFnPc7
západních	západní	k2eAgFnPc6d1
spojeneci	spojeneec	k1gInSc6
město	město	k1gNnSc1
za	za	k7c2
války	válka	k1gFnSc2
bombardovali	bombardovat	k5eAaImAgMnP
<g/>
.	.	kIx.
</s>
<s>
Dne	den	k1gInSc2
24	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
se	se	k3xPyFc4
první	první	k4xOgFnPc1
Žukovovy	Žukovův	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
dostaly	dostat	k5eAaPmAgFnP
do	do	k7c2
předměstí	předměstí	k1gNnSc2
Berlína	Berlín	k1gInSc2
<g/>
,	,	kIx,
na	na	k7c6
jihu	jih	k1gInSc2
se	se	k3xPyFc4
Koněvovy	Koněvův	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
přiblížily	přiblížit	k5eAaPmAgFnP
k	k	k7c3
letišti	letiště	k1gNnSc3
Tempelhof	Tempelhof	k1gInSc4
a	a	k8xC
překročily	překročit	k5eAaPmAgFnP
Teltowský	Teltowský	k2eAgInSc4d1
kanál	kanál	k1gInSc4
a	a	k8xC
obklíčení	obklíčení	k1gNnSc4
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
se	se	k3xPyFc4
uzavřelo	uzavřít	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednotky	jednotka	k1gFnSc2
jižně	jižně	k6eAd1
od	od	k7c2
Berlína	Berlín	k1gInSc2
byly	být	k5eAaImAgFnP
rozbity	rozbit	k2eAgFnPc1d1
na	na	k7c4
několik	několik	k4yIc4
menších	malý	k2eAgFnPc2d2
částí	část	k1gFnPc2
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
armáda	armáda	k1gFnSc1
byla	být	k5eAaImAgFnS
obklíčena	obklíčit	k5eAaPmNgFnS
v	v	k7c6
okolí	okolí	k1gNnSc6
Halbe	Halb	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Wenckova	Wenckov	k1gInSc2
12	#num#	k4
<g/>
.	.	kIx.
armáda	armáda	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
neustále	neustále	k6eAd1
pokoušela	pokoušet	k5eAaImAgFnS
probít	probít	k5eAaPmF
na	na	k7c4
pomoc	pomoc	k1gFnSc4
Berlínu	Berlín	k1gInSc3
byla	být	k5eAaImAgFnS
zastavena	zastaven	k2eAgFnSc1d1
silnou	silný	k2eAgFnSc7d1
obranou	obrana	k1gFnSc7
1	#num#	k4
<g/>
.	.	kIx.
ukrajinského	ukrajinský	k2eAgInSc2d1
frontu	front	k1gInSc2
v	v	k7c6
oblasti	oblast	k1gFnSc6
Postupimi	Postupim	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Schörnerova	Schörnerův	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
armád	armáda	k1gFnPc2
Střed	středa	k1gFnPc2
byla	být	k5eAaImAgFnS
z	z	k7c2
bojů	boj	k1gInPc2
o	o	k7c4
Berlín	Berlín	k1gInSc4
celkově	celkově	k6eAd1
vyřazena	vyřadit	k5eAaPmNgFnS
a	a	k8xC
pokoušela	pokoušet	k5eAaImAgFnS
se	se	k3xPyFc4
stáhnout	stáhnout	k5eAaPmF
přes	přes	k7c4
Československo	Československo	k1gNnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
vzdala	vzdát	k5eAaPmAgFnS
anglo-americkým	anglo-americký	k2eAgNnPc3d1
vojskům	vojsko	k1gNnPc3
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
obraně	obrana	k1gFnSc3
města	město	k1gNnSc2
měl	mít	k5eAaImAgMnS
generál	generál	k1gMnSc1
Weidling	Weidling	k1gInSc4
k	k	k7c3
dispozici	dispozice	k1gFnSc3
zbytky	zbytek	k1gInPc1
9	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
3	#num#	k4
<g/>
.	.	kIx.
pancéřové	pancéřový	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
a	a	k8xC
jednotky	jednotka	k1gFnSc2
sesbírané	sesbíraný	k2eAgFnSc2d1
z	z	k7c2
řad	řada	k1gFnPc2
policie	policie	k1gFnSc2
<g/>
,	,	kIx,
protiletadlového	protiletadlový	k2eAgNnSc2d1
dělostřelectva	dělostřelectvo	k1gNnSc2
<g/>
,	,	kIx,
Hitlerjugend	Hitlerjugenda	k1gFnPc2
a	a	k8xC
Volkssturmu	Volkssturm	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgNnPc1
vojska	vojsko	k1gNnPc1
o	o	k7c6
síle	síla	k1gFnSc6
asi	asi	k9
85	#num#	k4
000	#num#	k4
mužů	muž	k1gMnPc2
a	a	k8xC
ozbrojených	ozbrojený	k2eAgMnPc2d1
členů	člen	k1gMnPc2
Hitlerjugend	Hitlerjugenda	k1gFnPc2
byla	být	k5eAaImAgFnS
obklíčena	obklíčit	k5eAaPmNgFnS
v	v	k7c6
prostoru	prostor	k1gInSc6
o	o	k7c6
obvodu	obvod	k1gInSc6
kolem	kolem	k7c2
100	#num#	k4
km	km	kA
Přípravy	příprava	k1gFnSc2
obrany	obrana	k1gFnSc2
Berlína	Berlín	k1gInSc2
začaly	začít	k5eAaPmAgFnP
v	v	k7c4
už	už	k6eAd1
lednu	leden	k1gInSc6
1945	#num#	k4
po	po	k7c6
prolomení	prolomení	k1gNnSc6
německé	německý	k2eAgFnSc2d1
obrany	obrana	k1gFnSc2
na	na	k7c6
Visle	Visla	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Berlínský	berlínský	k2eAgInSc1d1
úsek	úsek	k1gInSc1
obrany	obrana	k1gFnSc2
se	se	k3xPyFc4
skládal	skládat	k5eAaImAgMnS
z	z	k7c2
množství	množství	k1gNnSc2
uzavřených	uzavřený	k2eAgInPc2d1
obranných	obranný	k2eAgInPc2d1
obvodů	obvod	k1gInPc2
a	a	k8xC
města	město	k1gNnSc2
samotného	samotný	k2eAgNnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
řízení	řízení	k1gNnSc4
byl	být	k5eAaImAgInS
vytvořen	vytvořen	k2eAgInSc1d1
zvláštní	zvláštní	k2eAgInSc1d1
štáb	štáb	k1gInSc1
a	a	k8xC
zmobilizováno	zmobilizován	k2eAgNnSc1d1
bylo	být	k5eAaImAgNnS
všechno	všechen	k3xTgNnSc4
práceschopné	práceschopný	k2eAgNnSc1d1
obyvatelstvo	obyvatelstvo	k1gNnSc1
<g/>
,	,	kIx,
jednotky	jednotka	k1gFnPc1
Volkssturmu	Volkssturm	k1gInSc2
a	a	k8xC
množství	množství	k1gNnSc1
válečných	válečný	k2eAgMnPc2d1
zajatců	zajatec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Denně	denně	k6eAd1
zde	zde	k6eAd1
bylo	být	k5eAaImAgNnS
při	při	k7c6
opevňovacích	opevňovací	k2eAgFnPc6d1
pracích	práce	k1gFnPc6
zaměstnáno	zaměstnat	k5eAaPmNgNnS
asi	asi	k9
100	#num#	k4
000	#num#	k4
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Obrana	obrana	k1gFnSc1
města	město	k1gNnSc2
se	se	k3xPyFc4
opírala	opírat	k5eAaImAgFnS
o	o	k7c4
barikády	barikáda	k1gFnPc4
<g/>
,	,	kIx,
opevněné	opevněný	k2eAgInPc1d1
suterény	suterén	k1gInPc1
pospojováné	pospojováný	k2eAgInPc1d1
průchody	průchod	k1gInPc1
mezi	mezi	k7c4
domy	dům	k1gInPc4
a	a	k8xC
minová	minový	k2eAgNnPc4d1
pole	pole	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Město	město	k1gNnSc1
bylo	být	k5eAaImAgNnS
rozděleno	rozdělit	k5eAaPmNgNnS
na	na	k7c4
úseky	úsek	k1gInPc4
<g/>
,	,	kIx,
obrana	obrana	k1gFnSc1
každého	každý	k3xTgInSc2
z	z	k7c2
nich	on	k3xPp3gInPc2
byla	být	k5eAaImAgFnS
organizována	organizován	k2eAgFnSc1d1
na	na	k7c6
principu	princip	k1gInSc6
opěrných	opěrný	k2eAgInPc2d1
bodů	bod	k1gInPc2
a	a	k8xC
ohnisek	ohnisko	k1gNnPc2
odporu	odpor	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
se	se	k3xPyFc4
mohly	moct	k5eAaImAgFnP
navzájem	navzájem	k6eAd1
podporovat	podporovat	k5eAaImF
palbou	palba	k1gFnSc7
i	i	k8xC
přesuny	přesun	k1gInPc1
živé	živý	k2eAgFnSc2d1
síly	síla	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
centru	centrum	k1gNnSc6
města	město	k1gNnSc2
byla	být	k5eAaImAgFnS
obrana	obrana	k1gFnSc1
nejkoncentrovanější	koncentrovaný	k2eAgFnSc1d3
<g/>
,	,	kIx,
všechny	všechen	k3xTgFnPc1
křižovatky	křižovatka	k1gFnPc1
byly	být	k5eAaImAgFnP
přeměněny	přeměnit	k5eAaPmNgFnP
na	na	k7c4
barikády	barikáda	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
obranu	obrana	k1gFnSc4
byla	být	k5eAaImAgFnS
využita	využít	k5eAaPmNgFnS
i	i	k9
síť	síť	k1gFnSc1
městských	městský	k2eAgInPc2d1
průplavů	průplav	k1gInPc2
a	a	k8xC
říčních	říční	k2eAgNnPc2d1
ramen	rameno	k1gNnPc2
Sprévy	Spréva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobně	podobně	k6eAd1
jako	jako	k8xC,k8xS
v	v	k7c6
jiných	jiný	k2eAgFnPc6d1
těžkých	těžký	k2eAgFnPc6d1
městských	městský	k2eAgFnPc6d1
bitvách	bitva	k1gFnPc6
se	se	k3xPyFc4
bojovalo	bojovat	k5eAaImAgNnS
i	i	k9
v	v	k7c6
suterénech	suterén	k1gInPc6
<g/>
,	,	kIx,
kanálech	kanál	k1gInPc6
a	a	k8xC
metru	metro	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yQnSc4,k3yInSc4,k3yRnSc4
Němci	Němec	k1gMnPc1
přišli	přijít	k5eAaPmAgMnP
o	o	k7c4
všechna	všechen	k3xTgNnPc4
letiště	letiště	k1gNnPc4
v	v	k7c6
Berlíně	Berlín	k1gInSc6
a	a	k8xC
jeho	jeho	k3xOp3gNnSc4
okolí	okolí	k1gNnSc4
<g/>
,	,	kIx,
použili	použít	k5eAaPmAgMnP
jako	jako	k9
provizorní	provizorní	k2eAgNnSc4d1
letiště	letiště	k1gNnSc4
ulici	ulice	k1gFnSc4
Unter	Unter	k1gInSc4
den	den	k1gInSc4
Linden	Lindna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1
nejvyšší	vysoký	k2eAgMnPc1d3
nacističtí	nacistický	k2eAgMnPc1d1
pohlaváři	pohlavár	k1gMnPc1
v	v	k7c6
obavách	obava	k1gFnPc6
před	před	k7c4
přibližující	přibližující	k2eAgInSc4d1
se	se	k3xPyFc4
Rudou	rudý	k2eAgFnSc7d1
armádou	armáda	k1gFnSc7
Berlín	Berlín	k1gInSc4
opustili	opustit	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
Göringa	Göring	k1gMnSc2
či	či	k8xC
Himmlera	Himmler	k1gMnSc2
byl	být	k5eAaImAgMnS
Hitler	Hitler	k1gMnSc1
rozhodnut	rozhodnout	k5eAaPmNgMnS
ve	v	k7c6
městě	město	k1gNnSc6
setrvat	setrvat	k5eAaPmF
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
na	na	k7c4
mnohé	mnohé	k1gNnSc4
působilo	působit	k5eAaImAgNnS
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
lze	lze	k6eAd1
situaci	situace	k1gFnSc4
ještě	ještě	k6eAd1
zvrátit	zvrátit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1945	#num#	k4
se	se	k3xPyFc4
na	na	k7c6
Labi	Labe	k1gNnSc6
u	u	k7c2
města	město	k1gNnSc2
Torgau	Torgaus	k1gInSc2
poprvé	poprvé	k6eAd1
setkali	setkat	k5eAaPmAgMnP
Sověti	Sovět	k1gMnPc1
s	s	k7c7
americkými	americký	k2eAgFnPc7d1
jednotkami	jednotka	k1gFnPc7
na	na	k7c6
dříve	dříve	k6eAd2
určené	určený	k2eAgFnSc6d1
demarkační	demarkační	k2eAgFnSc6d1
linii	linie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byli	být	k5eAaImAgMnP
to	ten	k3xDgNnSc1
vojáci	voják	k1gMnPc1
sovětské	sovětský	k2eAgFnPc4d1
58	#num#	k4
<g/>
.	.	kIx.
gardové	gardový	k2eAgFnSc2d1
divize	divize	k1gFnSc2
5	#num#	k4
<g/>
.	.	kIx.
gardové	gardový	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
1	#num#	k4
<g/>
.	.	kIx.
ukrajinského	ukrajinský	k2eAgInSc2d1
frontu	front	k1gInSc2
a	a	k8xC
vojáci	voják	k1gMnPc1
americké	americký	k2eAgFnSc2d1
69	#num#	k4
<g/>
.	.	kIx.
pěchotní	pěchotní	k2eAgFnSc2d1
divize	divize	k1gFnSc2
1	#num#	k4
<g/>
.	.	kIx.
armády	armáda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInSc4
kontakt	kontakt	k1gInSc4
na	na	k7c6
americké	americký	k2eAgFnSc6d1
straně	strana	k1gFnSc6
navázal	navázat	k5eAaPmAgMnS
poručík	poručík	k1gMnSc1
Albert	Albert	k1gMnSc1
Kotzube	Kotzub	k1gInSc5
<g/>
,	,	kIx,
velící	velící	k2eAgNnPc4d1
26	#num#	k4
<g/>
členné	členný	k2eAgFnSc6d1
hlídce	hlídka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgMnSc1
sovětský	sovětský	k2eAgMnSc1d1
voják	voják	k1gMnSc1
<g/>
,	,	kIx,
jehož	jenž	k3xRgMnSc4,k3xOyRp3gMnSc4
spatřili	spatřit	k5eAaPmAgMnP
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
průzkumník	průzkumník	k1gMnSc1
na	na	k7c6
koni	kůň	k1gMnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
tomto	tento	k3xDgNnSc6
prvním	první	k4xOgNnSc6
setkání	setkání	k1gNnSc6
se	se	k3xPyFc4
sovětští	sovětský	k2eAgMnPc1d1
a	a	k8xC
američtí	americký	k2eAgMnPc1d1
vojáci	voják	k1gMnPc1
na	na	k7c4
sebe	sebe	k3xPyFc4
jen	jen	k9
beze	beze	k7c2
slova	slovo	k1gNnSc2
podívali	podívat	k5eAaPmAgMnP,k5eAaImAgMnP
a	a	k8xC
odešli	odejít	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oficiální	oficiální	k2eAgFnSc1d1
slavnost	slavnost	k1gFnSc1
se	se	k3xPyFc4
udála	udát	k5eAaPmAgFnS
o	o	k7c4
16	#num#	k4
<g/>
.	.	kIx.
hodině	hodina	k1gFnSc6
odpoledne	odpoledne	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Dobytí	dobytí	k1gNnSc1
Berlína	Berlín	k1gInSc2
</s>
<s>
Zničená	zničený	k2eAgFnSc1d1
ulice	ulice	k1gFnSc1
v	v	k7c6
centru	centrum	k1gNnSc6
Berlína	Berlín	k1gInSc2
poblíž	poblíž	k6eAd1
hlavní	hlavní	k2eAgInSc1d1
Unter	Unter	k1gInSc1
den	dna	k1gFnPc2
Linden	Lindno	k1gNnPc2
3	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1945	#num#	k4
</s>
<s>
Bitva	bitva	k1gFnSc1
o	o	k7c4
Reichstag	Reichstag	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Frontové	frontový	k2eAgFnPc1d1
linie	linie	k1gFnPc1
1	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
(	(	kIx(
<g/>
růžově	růžově	k6eAd1
=	=	kIx~
spojenci	spojenec	k1gMnPc1
okupovaná	okupovaný	k2eAgNnPc4d1
území	území	k1gNnPc4
<g/>
;	;	kIx,
červeně	červeně	k6eAd1
=	=	kIx~
místa	místo	k1gNnSc2
bojů	boj	k1gInPc2
<g/>
)	)	kIx)
</s>
<s>
První	první	k4xOgInPc1
boje	boj	k1gInPc1
v	v	k7c6
předměstích	předměstí	k1gNnPc6
začaly	začít	k5eAaPmAgFnP
již	již	k6eAd1
21	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vojska	vojsko	k1gNnSc2
1	#num#	k4
<g/>
.	.	kIx.
běloruského	běloruský	k2eAgInSc2d1
a	a	k8xC
1	#num#	k4
<g/>
.	.	kIx.
ukrajinského	ukrajinský	k2eAgInSc2d1
frontu	front	k1gInSc2
začala	začít	k5eAaPmAgFnS
přímý	přímý	k2eAgInSc4d1
útok	útok	k1gInSc4
na	na	k7c4
město	město	k1gNnSc4
Berlín	Berlín	k1gInSc1
25	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
29	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
dobyla	dobýt	k5eAaPmAgFnS
podstatnou	podstatný	k2eAgFnSc4d1
část	část	k1gFnSc4
Berlína	Berlín	k1gInSc2
a	a	k8xC
pronikla	proniknout	k5eAaPmAgFnS
do	do	k7c2
středu	střed	k1gInSc2
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Brzy	brzy	k6eAd1
ráno	ráno	k6eAd1
29	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
Hitler	Hitler	k1gMnSc1
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
bunkru	bunkr	k1gInSc6
dopsal	dopsat	k5eAaPmAgMnS
svou	svůj	k3xOyFgFnSc4
závěť	závěť	k1gFnSc4
a	a	k8xC
uzavřel	uzavřít	k5eAaPmAgMnS
manželství	manželství	k1gNnSc4
se	s	k7c7
svou	svůj	k3xOyFgFnSc7
dlouholetou	dlouholetý	k2eAgFnSc7d1
milenkou	milenka	k1gFnSc7
Evou	Eva	k1gFnSc7
Braunovou	Braunová	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Téhož	týž	k3xTgInSc2
dne	den	k1gInSc2
se	se	k3xPyFc4
rozhořely	rozhořet	k5eAaPmAgInP
těžké	těžký	k2eAgInPc1d1
boje	boj	k1gInPc1
o	o	k7c4
budovu	budova	k1gFnSc4
velitelství	velitelství	k1gNnSc2
Gestapa	gestapo	k1gNnSc2
na	na	k7c6
Prinz-Albrechtstrasse	Prinz-Albrechtstrassa	k1gFnSc6
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
nejprve	nejprve	k6eAd1
obsadily	obsadit	k5eAaPmAgFnP
sovětské	sovětský	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
<g/>
,	,	kIx,
ale	ale	k8xC
protiútok	protiútok	k1gInSc1
SS	SS	kA
je	být	k5eAaImIp3nS
později	pozdě	k6eAd2
přinutil	přinutit	k5eAaPmAgMnS
ustoupit	ustoupit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jižněji	jižně	k6eAd2
se	se	k3xPyFc4
jednotkám	jednotka	k1gFnPc3
8	#num#	k4
<g/>
.	.	kIx.
gardové	gardový	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
podařilo	podařit	k5eAaPmAgNnS
překročit	překročit	k5eAaPmF
kanál	kanál	k1gInSc4
Landwehr	Landwehra	k1gFnPc2
a	a	k8xC
proniknout	proniknout	k5eAaPmF
do	do	k7c2
parku	park	k1gInSc2
Tiergarten	Tiergartno	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Berlínská	berlínský	k2eAgFnSc1d1
posádka	posádka	k1gFnSc1
byla	být	k5eAaImAgFnS
během	během	k7c2
celodenních	celodenní	k2eAgInPc2d1
bojů	boj	k1gInPc2
rozdělena	rozdělen	k2eAgFnSc1d1
na	na	k7c4
tři	tři	k4xCgFnPc4
izolované	izolovaný	k2eAgFnPc4d1
oblasti	oblast	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c4
stejný	stejný	k2eAgInSc4d1
den	den	k1gInSc4
bylo	být	k5eAaImAgNnS
Rudou	rudý	k2eAgFnSc7d1
armádou	armáda	k1gFnSc7
obsazeno	obsazen	k2eAgNnSc4d1
ministerstvo	ministerstvo	k1gNnSc4
vnitra	vnitro	k1gNnSc2
a	a	k8xC
Sověti	Sovět	k1gMnPc1
byli	být	k5eAaImAgMnP
od	od	k7c2
Říšského	říšský	k2eAgInSc2d1
sněmu	sněm	k1gInSc2
vzdáleni	vzdálen	k2eAgMnPc1d1
pouze	pouze	k6eAd1
500	#num#	k4
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obsazení	obsazení	k1gNnSc1
ministerstva	ministerstvo	k1gNnSc2
vnitra	vnitro	k1gNnSc2
byla	být	k5eAaImAgNnP
po	po	k7c6
získání	získání	k1gNnSc6
Moltkeho	Moltke	k1gMnSc2
mostu	most	k1gInSc2
druhou	druhý	k4xOgFnSc7
fází	fáze	k1gFnSc7
útoku	útok	k1gInSc2
na	na	k7c4
Říšský	říšský	k2eAgInSc4d1
sněm	sněm	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnSc7
obsazením	obsazení	k1gNnSc7
byl	být	k5eAaImAgInS
pověřen	pověřit	k5eAaPmNgInS
79	#num#	k4
<g/>
.	.	kIx.
střelecký	střelecký	k2eAgInSc1d1
sbor	sbor	k1gInSc1
generála	generál	k1gMnSc2
Perevertkina	Perevertkin	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Říšský	říšský	k2eAgInSc1d1
sněm	sněm	k1gInSc1
bránilo	bránit	k5eAaImAgNnS
asi	asi	k9
5	#num#	k4
000	#num#	k4
vojáků	voják	k1gMnPc2
Waffen	Waffen	k2eAgMnSc1d1
SS	SS	kA
<g/>
,	,	kIx,
armády	armáda	k1gFnSc2
a	a	k8xC
Volkssturmu	Volkssturm	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Těžké	těžký	k2eAgInPc1d1
boje	boj	k1gInPc1
se	se	k3xPyFc4
rozpoutaly	rozpoutat	k5eAaPmAgInP
zejména	zejména	k9
před	před	k7c7
budovou	budova	k1gFnSc7
<g/>
,	,	kIx,
kde	kde	k6eAd1
stálo	stát	k5eAaImAgNnS
několik	několik	k4yIc1
88	#num#	k4
mm	mm	kA
protiletadlových	protiletadlový	k2eAgInPc2d1
kanónů	kanón	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
bránily	bránit	k5eAaImAgFnP
přístup	přístup	k1gInSc4
z	z	k7c2
Moltkeho	Moltke	k1gMnSc2
mostu	most	k1gInSc2
před	před	k7c7
tanky	tank	k1gInPc7
<g/>
.	.	kIx.
30	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
vrhli	vrhnout	k5eAaImAgMnP,k5eAaPmAgMnP
Sověti	Sovět	k1gMnPc1
do	do	k7c2
útoku	útok	k1gInSc2
proti	proti	k7c3
říšskému	říšský	k2eAgNnSc3d1
Kancléřství	kancléřství	k1gNnSc3
150	#num#	k4
<g/>
.	.	kIx.
divizi	divize	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInPc1
dva	dva	k4xCgInPc1
útoky	útok	k1gInPc1
na	na	k7c4
Reichstag	Reichstag	k1gInSc4
v	v	k7c6
průběhu	průběh	k1gInSc6
dne	den	k1gInSc2
uvázly	uváznout	k5eAaPmAgInP
<g/>
,	,	kIx,
o	o	k7c4
třetí	třetí	k4xOgInSc4
úspěšný	úspěšný	k2eAgInSc4d1
se	se	k3xPyFc4
pokusili	pokusit	k5eAaPmAgMnP
až	až	k9
po	po	k7c6
silném	silný	k2eAgNnSc6d1
dělostřeleckém	dělostřelecký	k2eAgNnSc6d1
ostřelování	ostřelování	k1gNnSc6
budovy	budova	k1gFnSc2
po	po	k7c6
18	#num#	k4
<g/>
.	.	kIx.
hodině	hodina	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedním	jeden	k4xCgInSc7
z	z	k7c2
důvodů	důvod	k1gInPc2
neúspěchů	neúspěch	k1gInPc2
předešlých	předešlý	k2eAgInPc2d1
útoků	útok	k1gInPc2
byla	být	k5eAaImAgFnS
silná	silný	k2eAgFnSc1d1
palba	palba	k1gFnSc1
z	z	k7c2
2	#num#	k4
km	km	kA
vzdálené	vzdálený	k2eAgFnSc2d1
flakové	flakový	k2eAgFnSc2d1
věže	věž	k1gFnSc2
s	s	k7c7
88	#num#	k4
<g/>
mm	mm	kA
kanóny	kanón	k1gInPc1
poblíž	poblíž	k7c2
Berlínské	berlínský	k2eAgFnSc2d1
zoo	zoo	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
tom	ten	k3xDgNnSc6
co	co	k3yQnSc1,k3yInSc1,k3yRnSc1
se	se	k3xPyFc4
sovětské	sovětský	k2eAgFnSc3d1
pěchotě	pěchota	k1gFnSc3
podařilo	podařit	k5eAaPmAgNnS
dostat	dostat	k5eAaPmF
dovnitř	dovnitř	k7c2
Říšského	říšský	k2eAgInSc2d1
sněmu	sněm	k1gInSc2
ho	on	k3xPp3gMnSc4
Němci	Němec	k1gMnPc1
zapálili	zapálit	k5eAaPmAgMnP
<g/>
,	,	kIx,
doufajíc	doufat	k5eAaImSgFnS
<g/>
,	,	kIx,
že	že	k8xS
z	z	k7c2
ní	on	k3xPp3gFnSc2
nepřítel	nepřítel	k1gMnSc1
ustoupí	ustoupit	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
průběhu	průběh	k1gInSc6
bojů	boj	k1gInPc2
se	s	k7c7
4	#num#	k4
vojáci	voják	k1gMnPc1
Michail	Michail	k1gMnSc1
Minin	Minin	k1gMnSc1
<g/>
,	,	kIx,
Gazi	Gaz	k1gMnPc1
Zagitov	Zagitovo	k1gNnPc2
<g/>
,	,	kIx,
Alexandr	Alexandr	k1gMnSc1
Lisimenko	Lisimenka	k1gFnSc5
a	a	k8xC
Alexej	Alexej	k1gMnSc1
Bobrov	Bobrov	k1gInSc4
kolem	kolem	k7c2
22	#num#	k4
<g/>
:	:	kIx,
<g/>
40	#num#	k4
dostali	dostat	k5eAaPmAgMnP
jako	jako	k9
první	první	k4xOgMnPc1
sovětští	sovětský	k2eAgMnPc1d1
vojáci	voják	k1gMnPc1
s	s	k7c7
vlajkou	vlajka	k1gFnSc7
na	na	k7c4
střechu	střecha	k1gFnSc4
budovy	budova	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
ji	on	k3xPp3gFnSc4
vyvěsili	vyvěsit	k5eAaPmAgMnP
na	na	k7c6
místě	místo	k1gNnSc6
jedné	jeden	k4xCgFnSc2
z	z	k7c2
bronzových	bronzový	k2eAgFnPc2d1
soch	socha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
vlajku	vlajka	k1gFnSc4
vynesli	vynést	k5eAaPmAgMnP
ve	v	k7c6
tmě	tma	k1gFnSc6
bez	bez	k7c2
přítomnosti	přítomnost	k1gFnSc2
fotografů	fotograf	k1gMnPc2
<g/>
,	,	kIx,
tak	tak	k6eAd1
jejich	jejich	k3xOp3gInSc4
čin	čin	k1gInSc4
upadl	upadnout	k5eAaPmAgMnS
do	do	k7c2
zapomnění	zapomnění	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
Oficiální	oficiální	k2eAgFnSc4d1
vlajku	vlajka	k1gFnSc4
na	na	k7c4
střechu	střecha	k1gFnSc4
Říšského	říšský	k2eAgInSc2d1
sněmu	sněm	k1gInSc2
umístili	umístit	k5eAaPmAgMnP
kolem	kolem	k7c2
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
ráno	ráno	k6eAd1
následujícího	následující	k2eAgInSc2d1
dne	den	k1gInSc2
seržanti	seržant	k1gMnPc1
Michail	Michail	k1gInSc4
Jegorov	Jegorov	k1gInSc1
a	a	k8xC
Meliton	Meliton	k1gInSc1
Kantaria	Kantarium	k1gNnSc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
akt	akt	k1gInSc4
zopakovali	zopakovat	k5eAaPmAgMnP
za	za	k7c2
dobrého	dobrý	k2eAgNnSc2d1
světla	světlo	k1gNnSc2
před	před	k7c7
fotografem	fotograf	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgFnSc1d1
izolované	izolovaný	k2eAgFnPc4d1
skupinky	skupinka	k1gFnPc4
Němců	Němec	k1gMnPc2
v	v	k7c6
budově	budova	k1gFnSc6
se	se	k3xPyFc4
vzdaly	vzdát	k5eAaPmAgFnP
až	až	k9
2	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Když	když	k8xS
Keitel	Keitel	k1gMnSc1
Hitlerovi	Hitler	k1gMnSc3
sdělil	sdělit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
Wenckova	Wenckův	k2eAgFnSc1d1
12	#num#	k4
<g/>
.	.	kIx.
armáda	armáda	k1gFnSc1
svádí	svádět	k5eAaImIp3nS
těžké	těžký	k2eAgInPc4d1
boje	boj	k1gInPc4
a	a	k8xC
do	do	k7c2
Berlína	Berlín	k1gInSc2
nedorazí	dorazit	k5eNaPmIp3nS
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
rozhodnuto	rozhodnout	k5eAaPmNgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Weidling	Weidling	k1gInSc1
ho	on	k3xPp3gMnSc4
také	také	k9
informoval	informovat	k5eAaBmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
obráncům	obránce	k1gMnPc3
dochází	docházet	k5eAaImIp3nS
munice	munice	k1gFnSc1
a	a	k8xC
během	během	k7c2
několika	několik	k4yIc2
hodin	hodina	k1gFnPc2
jejich	jejich	k3xOp3gFnSc4
obranu	obrana	k1gFnSc4
Sověti	Sovět	k1gMnPc1
přemohou	přemoct	k5eAaPmIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Adolf	Adolf	k1gMnSc1
Hitler	Hitler	k1gMnSc1
následně	následně	k6eAd1
povolil	povolit	k5eAaPmAgMnS
přeživším	přeživší	k2eAgMnSc7d1
obráncům	obránce	k1gMnPc3
pokoušet	pokoušet	k5eAaImF
se	se	k3xPyFc4
probít	probít	k5eAaPmF
na	na	k7c4
západ	západ	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
se	se	k3xPyFc4
již	již	k6eAd1
v	v	k7c6
blízkosti	blízkost	k1gFnSc6
bunkru	bunkr	k1gInSc2
bojovalo	bojovat	k5eAaImAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Adolf	Adolf	k1gMnSc1
Hitler	Hitler	k1gMnSc1
a	a	k8xC
jeho	jeho	k3xOp3gFnSc1
novomanželka	novomanželka	k1gFnSc1
Eva	Eva	k1gFnSc1
<g/>
,	,	kIx,
rozená	rozený	k2eAgFnSc1d1
Braunová	Braunová	k1gFnSc1
<g/>
,	,	kIx,
po	po	k7c6
poledni	poledne	k1gNnSc6
spáchali	spáchat	k5eAaPmAgMnP
sebevraždu	sebevražda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gInPc4
těla	tělo	k1gNnSc2
byla	být	k5eAaImAgFnS
následně	následně	k6eAd1
spálena	spálit	k5eAaPmNgFnS
a	a	k8xC
zakopána	zakopat	k5eAaPmNgFnS
blízko	blízko	k7c2
bunkru	bunkr	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
Hitlerova	Hitlerův	k2eAgMnSc2d1
následníka	následník	k1gMnSc2
byl	být	k5eAaImAgMnS
jmenován	jmenovat	k5eAaImNgMnS,k5eAaBmNgMnS
podle	podle	k7c2
závěti	závěť	k1gFnSc2
admirál	admirál	k1gMnSc1
Karl	Karl	k1gMnSc1
Dönitz	Dönitz	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
začal	začít	k5eAaPmAgMnS
formovat	formovat	k5eAaImF
novou	nový	k2eAgFnSc4d1
vládu	vláda	k1gFnSc4
ve	v	k7c6
Flensburgu	Flensburg	k1gInSc6
poblíž	poblíž	k6eAd1
Dánských	dánský	k2eAgFnPc2d1
hranic	hranice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ještě	ještě	k6eAd1
týž	týž	k3xTgInSc4
den	den	k1gInSc4
ve	v	k7c4
22	#num#	k4
hodin	hodina	k1gFnPc2
vztyčili	vztyčit	k5eAaPmAgMnP
dva	dva	k4xCgMnPc1
rudoarmějci	rudoarmějec	k1gMnPc1
sovětskou	sovětský	k2eAgFnSc4d1
vlajku	vlajka	k1gFnSc4
na	na	k7c4
kupoli	kupole	k1gFnSc4
Reichstagu	Reichstag	k1gInSc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1945	#num#	k4
se	se	k3xPyFc4
Joseph	Joseph	k1gMnSc1
Goebbels	Goebbelsa	k1gFnPc2
pokusil	pokusit	k5eAaPmAgMnS
dosáhnout	dosáhnout	k5eAaPmF
zastavení	zastavení	k1gNnSc4
palby	palba	k1gFnSc2
<g/>
,	,	kIx,
avšak	avšak	k8xC
Stalin	Stalin	k1gMnSc1
trval	trvat	k5eAaImAgMnS
na	na	k7c6
bezpodmínečné	bezpodmínečný	k2eAgFnSc6d1
kapitulaci	kapitulace	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzápětí	vzápětí	k6eAd1
na	na	k7c4
to	ten	k3xDgNnSc4
Goebbels	Goebbels	k1gInSc1
a	a	k8xC
jeho	jeho	k3xOp3gFnSc1
žena	žena	k1gFnSc1
otrávili	otrávit	k5eAaPmAgMnP
své	svůj	k3xOyFgFnPc4
děti	dítě	k1gFnPc4
a	a	k8xC
spáchali	spáchat	k5eAaPmAgMnP
sebevraždu	sebevražda	k1gFnSc4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
se	se	k3xPyFc4
generál	generál	k1gMnSc1
Weidling	Weidling	k1gInSc4
rozhodl	rozhodnout	k5eAaPmAgMnS
beznadějný	beznadějný	k2eAgInSc4d1
boj	boj	k1gInSc4
ukončit	ukončit	k5eAaPmF
a	a	k8xC
berlínská	berlínský	k2eAgFnSc1d1
posádka	posádka	k1gFnSc1
se	se	k3xPyFc4
vzdala	vzdát	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
noci	noc	k1gFnSc6
z	z	k7c2
2	#num#	k4
<g/>
.	.	kIx.
na	na	k7c4
3	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1945	#num#	k4
se	se	k3xPyFc4
na	na	k7c6
jihu	jih	k1gInSc6
Berlína	Berlín	k1gInSc2
německý	německý	k2eAgMnSc1d1
generál	generál	k1gMnSc1
von	von	k1gInSc4
Manteuffel	Manteuffel	k1gMnSc1
<g/>
,	,	kIx,
velitel	velitel	k1gMnSc1
3	#num#	k4
<g/>
.	.	kIx.
tankové	tankový	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
spolu	spolu	k6eAd1
s	s	k7c7
generálem	generál	k1gMnSc7
von	von	k1gInSc4
Tippelskirch	Tippelskircha	k1gFnPc2
<g/>
,	,	kIx,
velitelem	velitel	k1gMnSc7
21	#num#	k4
<g/>
.	.	kIx.
armády	armáda	k1gFnSc2
vzdali	vzdát	k5eAaPmAgMnP
armádě	armáda	k1gFnSc3
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
sovětské	sovětský	k2eAgNnSc4d1
velení	velení	k1gNnSc4
přebral	přebrat	k5eAaPmAgInS
kapitulaci	kapitulace	k1gFnSc4
Berlína	Berlín	k1gInSc2
generál	generál	k1gMnSc1
Vasilij	Vasilij	k1gMnSc1
Čujkov	Čujkov	k1gInSc1
<g/>
,	,	kIx,
velitel	velitel	k1gMnSc1
obrany	obrana	k1gFnSc2
Stalingradu	Stalingrad	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Boje	boj	k1gInSc2
v	v	k7c6
Evropě	Evropa	k1gFnSc6
tím	ten	k3xDgNnSc7
však	však	k9
ještě	ještě	k6eAd1
neskončily	skončit	k5eNaPmAgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zbytky	zbytek	k1gInPc1
německých	německý	k2eAgNnPc2d1
vojsk	vojsko	k1gNnPc2
se	se	k3xPyFc4
stále	stále	k6eAd1
pokoušely	pokoušet	k5eAaImAgFnP
probojovat	probojovat	k5eAaPmF
ze	z	k7c2
sevření	sevření	k1gNnSc2
sovětských	sovětský	k2eAgNnPc2d1
vojsk	vojsko	k1gNnPc2
na	na	k7c4
západ	západ	k1gInSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
chtěli	chtít	k5eAaImAgMnP
vzdát	vzdát	k5eAaPmF
do	do	k7c2
rukou	ruka	k1gFnPc2
anglo-amerických	anglo-americký	k2eAgNnPc2d1
vojsk	vojsko	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Sovětské	sovětský	k2eAgInPc1d1
zločiny	zločin	k1gInPc1
v	v	k7c6
Německu	Německo	k1gNnSc6
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Zločiny	zločin	k1gInPc1
Sovětského	sovětský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
za	za	k7c2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Nápisy	nápis	k1gInPc4
zanechané	zanechaný	k2eAgInPc4d1
sovětskými	sovětský	k2eAgMnPc7d1
vojáky	voják	k1gMnPc7
na	na	k7c6
stěnách	stěna	k1gFnPc6
říšského	říšský	k2eAgInSc2d1
sněmu	sněm	k1gInSc2
</s>
<s>
Příchod	příchod	k1gInSc1
sovětských	sovětský	k2eAgNnPc2d1
vojsk	vojsko	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
byla	být	k5eAaImAgFnS
nacistickou	nacistický	k2eAgFnSc7d1
propagandou	propaganda	k1gFnSc7
zobrazena	zobrazit	k5eAaPmNgFnS
jako	jako	k9
„	„	k?
<g/>
židobolševické	židobolševický	k2eAgFnSc2d1
krvežíznivé	krvežíznivý	k2eAgFnSc2d1
hordy	horda	k1gFnSc2
<g/>
“	“	k?
vyvolal	vyvolat	k5eAaPmAgInS
masový	masový	k2eAgInSc4d1
útěk	útěk	k1gInSc4
německého	německý	k2eAgNnSc2d1
obyvatelstva	obyvatelstvo	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
se	se	k3xPyFc4
obávalo	obávat	k5eAaImAgNnS
násilností	násilnost	k1gFnSc7
<g/>
,	,	kIx,
směrem	směr	k1gInSc7
na	na	k7c4
západ	západ	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
mnoha	mnoho	k4c6
místech	místo	k1gNnPc6
se	se	k3xPyFc4
opakovalo	opakovat	k5eAaImAgNnS
brutální	brutální	k2eAgMnSc1d1
a	a	k8xC
necivilizované	civilizovaný	k2eNgNnSc1d1
zacházení	zacházení	k1gNnSc1
vojsk	vojsko	k1gNnPc2
s	s	k7c7
civilním	civilní	k2eAgNnSc7d1
obyvatelstvem	obyvatelstvo	k1gNnSc7
<g/>
,	,	kIx,
podobné	podobný	k2eAgInPc1d1
tomu	ten	k3xDgInSc3
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yRgFnPc4,k3yIgFnPc4
dříve	dříve	k6eAd2
nacisté	nacista	k1gMnPc1
aplikovali	aplikovat	k5eAaBmAgMnP
na	na	k7c6
okupovaných	okupovaný	k2eAgNnPc6d1
územích	území	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počet	počet	k1gInSc1
obětí	oběť	k1gFnSc7
rostl	růst	k5eAaImAgInS
i	i	k9
proto	proto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
velení	velení	k1gNnSc1
nepořádalo	pořádat	k5eNaImAgNnS
evakuaci	evakuace	k1gFnSc4
civilního	civilní	k2eAgNnSc2d1
obyvatelstva	obyvatelstvo	k1gNnSc2
<g/>
,	,	kIx,
proto	proto	k8xC
aby	aby	kYmCp3nS
tak	tak	k6eAd1
podpořilo	podpořit	k5eAaPmAgNnS
morálku	morálka	k1gFnSc4
bojujících	bojující	k2eAgNnPc2d1
vojsk	vojsko	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
nesměla	smět	k5eNaImAgFnS
ztratit	ztratit	k5eAaPmF
pocit	pocit	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
brání	bránit	k5eAaImIp3nS
svou	svůj	k3xOyFgFnSc4
vlast	vlast	k1gFnSc4
a	a	k8xC
obyvatele	obyvatel	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s>
Válka	válka	k1gFnSc1
na	na	k7c6
východní	východní	k2eAgFnSc6d1
frontě	fronta	k1gFnSc6
od	od	k7c2
svého	svůj	k3xOyFgInSc2
počátku	počátek	k1gInSc2
překračovala	překračovat	k5eAaImAgFnS
morální	morální	k2eAgFnPc4d1
bariéry	bariéra	k1gFnPc4
dodržováné	dodržováný	k2eAgFnPc4d1
v	v	k7c6
dřívějších	dřívější	k2eAgInPc6d1
válečných	válečný	k2eAgInPc6d1
konfliktech	konflikt	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
rámci	rámec	k1gInSc6
nacistického	nacistický	k2eAgInSc2d1
plánu	plán	k1gInSc2
genocidy	genocida	k1gFnSc2
slovanských	slovanský	k2eAgInPc2d1
národů	národ	k1gInPc2
tzv.	tzv.	kA
Generalplan	Generalplan	k1gInSc1
Ost	Ost	k1gMnSc2
vedlo	vést	k5eAaImAgNnS
Německo	Německo	k1gNnSc1
proti	proti	k7c3
obyvatelstvu	obyvatelstvo	k1gNnSc3
Sovětského	sovětský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
vyhlazovací	vyhlazovací	k2eAgFnSc4d1
válku	válka	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
Během	během	k7c2
války	válka	k1gFnSc2
zahynulo	zahynout	k5eAaPmAgNnS
více	hodně	k6eAd2
než	než	k8xS
26	#num#	k4
milionů	milion	k4xCgInPc2
sovětských	sovětský	k2eAgMnPc2d1
občanů	občan	k1gMnPc2
<g/>
,	,	kIx,
z	z	k7c2
větší	veliký	k2eAgFnSc2d2
části	část	k1gFnSc2
civilistů	civilista	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
Mnozí	mnohý	k2eAgMnPc1d1
sovětští	sovětský	k2eAgMnPc1d1
vojáci	voják	k1gMnPc1
se	se	k3xPyFc4
chtěli	chtít	k5eAaImAgMnP
pomstít	pomstít	k5eAaPmF
za	za	k7c4
smrt	smrt	k1gFnSc4
členů	člen	k1gInPc2
svých	svůj	k3xOyFgFnPc2
rodin	rodina	k1gFnPc2
a	a	k8xC
za	za	k7c4
německé	německý	k2eAgInPc4d1
válečné	válečný	k2eAgInPc4d1
zločiny	zločin	k1gInPc4
na	na	k7c4
území	území	k1gNnSc4
Sovětského	sovětský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
Obě	dva	k4xCgFnPc1
strany	strana	k1gFnPc1
bojovaly	bojovat	k5eAaImAgFnP
brutálními	brutální	k2eAgInPc7d1
způsoby	způsob	k1gInPc7
bez	bez	k7c2
ohledu	ohled	k1gInSc2
na	na	k7c4
ztráty	ztráta	k1gFnPc4
civilního	civilní	k2eAgNnSc2d1
obyvatelstva	obyvatelstvo	k1gNnSc2
nepřítele	nepřítel	k1gMnSc2
v	v	k7c6
čemž	což	k3yRnSc6,k3yQnSc6
je	být	k5eAaImIp3nS
výrazně	výrazně	k6eAd1
podporovala	podporovat	k5eAaImAgFnS
propaganda	propaganda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
sovětské	sovětský	k2eAgFnSc6d1
straně	strana	k1gFnSc6
byl	být	k5eAaImAgInS
až	až	k6eAd1
do	do	k7c2
konce	konec	k1gInSc2
války	válka	k1gFnSc2
velkým	velký	k2eAgMnSc7d1
stoupencem	stoupenec	k1gMnSc7
odplaty	odplata	k1gFnSc2
populární	populární	k2eAgMnSc1d1
sovětský	sovětský	k2eAgMnSc1d1
židovský	židovský	k2eAgMnSc1d1
novinář	novinář	k1gMnSc1
Ilja	Ilja	k1gMnSc1
Erenburg	Erenburg	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sovětští	sovětský	k2eAgMnPc1d1
vojáci	voják	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
už	už	k6eAd1
čtyři	čtyři	k4xCgInPc4
roky	rok	k1gInPc4
dříve	dříve	k6eAd2
bojovali	bojovat	k5eAaImAgMnP
na	na	k7c6
vlastní	vlastní	k2eAgFnSc6d1
půdě	půda	k1gFnSc6
a	a	k8xC
viděli	vidět	k5eAaImAgMnP
řadu	řada	k1gFnSc4
měst	město	k1gNnPc2
a	a	k8xC
vesnic	vesnice	k1gFnPc2
v	v	k7c6
troskách	troska	k1gFnPc6
a	a	k8xC
byli	být	k5eAaImAgMnP
svědky	svědek	k1gMnPc7
brutálních	brutální	k2eAgInPc2d1
nacistických	nacistický	k2eAgInPc2d1
zločinů	zločin	k1gInPc2
<g/>
,	,	kIx,
nedokázali	dokázat	k5eNaPmAgMnP
po	po	k7c6
svém	svůj	k3xOyFgInSc6
vstupu	vstup	k1gInSc6
na	na	k7c4
území	území	k1gNnSc4
nepřítele	nepřítel	k1gMnSc2
potlačit	potlačit	k5eAaPmF
touhu	touha	k1gFnSc4
po	po	k7c6
odvetě	odveta	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
To	ten	k3xDgNnSc1
neplatilo	platit	k5eNaImAgNnS
jen	jen	k9
pro	pro	k7c4
obyčejné	obyčejný	k2eAgMnPc4d1
vojáky	voják	k1gMnPc4
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
pro	pro	k7c4
některé	některý	k3yIgMnPc4
generály	generál	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
tvrdé	tvrdý	k2eAgNnSc4d1
zacházení	zacházení	k1gNnSc4
s	s	k7c7
nepřítelem	nepřítel	k1gMnSc7
se	se	k3xPyFc4
zasazovali	zasazovat	k5eAaImAgMnP
zejména	zejména	k9
ti	ten	k3xDgMnPc1
<g/>
,	,	kIx,
kterých	který	k3yQgNnPc2,k3yRgNnPc2,k3yIgNnPc2
se	se	k3xPyFc4
válka	válka	k1gFnSc1
osobně	osobně	k6eAd1
dotkla	dotknout	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
ty	ten	k3xDgMnPc4
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
na	na	k7c6
německém	německý	k2eAgNnSc6d1
území	území	k1gNnSc6
neprojevovali	projevovat	k5eNaImAgMnP
žádné	žádný	k3yNgFnPc4
známky	známka	k1gFnPc4
slitování	slitování	k1gNnSc2
patřil	patřit	k5eAaImAgMnS
například	například	k6eAd1
generál	generál	k1gMnSc1
Pavel	Pavel	k1gMnSc1
Rybalko	Rybalka	k1gFnSc5
<g/>
,	,	kIx,
velitel	velitel	k1gMnSc1
3	#num#	k4
<g/>
.	.	kIx.
gardové	gardový	k2eAgFnSc2d1
tankové	tankový	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc4
dceru	dcera	k1gFnSc4
nacisté	nacista	k1gMnPc1
násilně	násilně	k6eAd1
odvlekli	odvléct	k5eAaPmAgMnP
do	do	k7c2
Německa	Německo	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
Jiní	jiný	k2eAgMnPc1d1
jako	jako	k8xS,k8xC
například	například	k6eAd1
generál	generál	k1gMnSc1
Ivan	Ivan	k1gMnSc1
Čerňachovskij	Čerňachovskij	k1gMnSc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
později	pozdě	k6eAd2
zahynul	zahynout	k5eAaPmAgMnS
při	při	k7c6
bojích	boj	k1gInPc6
ve	v	k7c6
Východním	východní	k2eAgNnSc6d1
Prusku	Prusko	k1gNnSc6
<g/>
,	,	kIx,
před	před	k7c7
svými	svůj	k3xOyFgMnPc7
vojáky	voják	k1gMnPc7
otevřeně	otevřeně	k6eAd1
řekl	říct	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
<g/>
:	:	kIx,
„	„	k?
<g/>
Nesmíme	smět	k5eNaImIp1nP
mít	mít	k5eAaImF
žádné	žádný	k3yNgNnSc4
slitování	slitování	k1gNnSc4
před	před	k7c7
nikým	nikdo	k3yNnSc7
<g/>
,	,	kIx,
tak	tak	k8xS,k8xC
jak	jak	k8xC,k8xS
nikdo	nikdo	k3yNnSc1
neměl	mít	k5eNaImAgMnS
slitování	slitování	k1gNnSc4
s	s	k7c7
námi	my	k3xPp1nPc7
<g/>
.	.	kIx.
<g/>
“	“	k?
Na	na	k7c6
mnoha	mnoho	k4c6
místech	místo	k1gNnPc6
si	se	k3xPyFc3
sovětská	sovětský	k2eAgNnPc1d1
vojska	vojsko	k1gNnPc1
vybila	vybít	k5eAaPmAgNnP
vztek	vztek	k1gInSc4
tak	tak	k9
<g/>
,	,	kIx,
že	že	k8xS
způsobila	způsobit	k5eAaPmAgFnS
velké	velký	k2eAgFnPc4d1
škody	škoda	k1gFnPc4
na	na	k7c6
německém	německý	k2eAgInSc6d1
majetku	majetek	k1gInSc6
neodůvodněnou	odůvodněný	k2eNgFnSc7d1
palbou	palba	k1gFnSc7
zbraní	zbraň	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Běžné	běžný	k2eAgInPc1d1
byly	být	k5eAaImAgFnP
krádeže	krádež	k1gFnPc4
<g/>
,	,	kIx,
rabování	rabování	k1gNnSc3
<g/>
,	,	kIx,
ale	ale	k8xC
nejhorší	zlý	k2eAgInSc4d3
dopad	dopad	k1gInSc4
mělo	mít	k5eAaImAgNnS
znásilňování	znásilňování	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
na	na	k7c6
mnoha	mnoho	k4c6
místech	místo	k1gNnPc6
nabylo	nabýt	k5eAaPmAgNnS
masový	masový	k2eAgInSc4d1
rozměr	rozměr	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
vstupu	vstup	k1gInSc2
sovětských	sovětský	k2eAgNnPc2d1
vojsk	vojsko	k1gNnPc2
na	na	k7c4
německé	německý	k2eAgNnSc4d1
území	území	k1gNnSc4
bylo	být	k5eAaImAgNnS
znásilněno	znásilnit	k5eAaPmNgNnS
asi	asi	k9
1	#num#	k4
až	až	k9
2	#num#	k4
miliony	milion	k4xCgInPc1
žen	žena	k1gFnPc2
<g/>
,	,	kIx,
často	často	k6eAd1
opakovaně	opakovaně	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
některých	některý	k3yIgNnPc6
místech	místo	k1gNnPc6
také	také	k6eAd1
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
masovým	masový	k2eAgFnPc3d1
vraždám	vražda	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s>
Sovětské	sovětský	k2eAgNnSc1d1
vedení	vedení	k1gNnSc1
proti	proti	k7c3
těmto	tento	k3xDgMnPc3
zločinům	zločin	k1gMnPc3
zpočátku	zpočátku	k6eAd1
nezasahovalo	zasahovat	k5eNaImAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zacházení	zacházení	k1gNnSc1
s	s	k7c7
provinilci	provinilec	k1gMnPc7
bylo	být	k5eAaImAgNnS
ponecháno	ponechat	k5eAaPmNgNnS
na	na	k7c6
nižších	nízký	k2eAgMnPc6d2
velitelích	velitel	k1gMnPc6
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
proti	proti	k7c3
vlastním	vlastní	k2eAgMnPc3d1
vojákům	voják	k1gMnPc3
zasahovali	zasahovat	k5eAaImAgMnP
pouze	pouze	k6eAd1
sporadicky	sporadicky	k6eAd1
<g/>
,	,	kIx,
zejména	zejména	k9
když	když	k8xS
se	se	k3xPyFc4
jejich	jejich	k3xOp3gNnSc1
řádění	řádění	k1gNnSc1
rozrostlo	rozrůst	k5eAaPmAgNnS
do	do	k7c2
větších	veliký	k2eAgInPc2d2
rozměrů	rozměr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostré	ostrý	k2eAgInPc1d1
zásahy	zásah	k1gInPc1
byly	být	k5eAaImAgFnP
obvykle	obvykle	k6eAd1
ke	k	k7c3
znásilnění	znásilnění	k1gNnSc3
malých	malý	k2eAgFnPc2d1
dívek	dívka	k1gFnPc2
a	a	k8xC
provinilci	provinilec	k1gMnPc1
byli	být	k5eAaImAgMnP
většinou	většinou	k6eAd1
zastřeleni	zastřelit	k5eAaPmNgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teprve	teprve	k6eAd1
po	po	k7c6
německé	německý	k2eAgFnSc6d1
kapitulaci	kapitulace	k1gFnSc6
byla	být	k5eAaImAgNnP
zavedena	zaveden	k2eAgNnPc1d1
různá	různý	k2eAgNnPc1d1
opatření	opatření	k1gNnPc1
a	a	k8xC
vysoké	vysoký	k2eAgInPc1d1
tresty	trest	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
<g/>
,	,	kIx,
spolu	spolu	k6eAd1
s	s	k7c7
přesunem	přesun	k1gInSc7
okupačních	okupační	k2eAgNnPc2d1
vojsk	vojsko	k1gNnPc2
do	do	k7c2
lépe	dobře	k6eAd2
kontrolovaných	kontrolovaný	k2eAgInPc2d1
táborů	tábor	k1gInPc2
<g/>
,	,	kIx,
míru	míra	k1gFnSc4
znásilnění	znásilnění	k1gNnSc4
snížily	snížit	k5eAaPmAgFnP
<g/>
.	.	kIx.
</s>
<s>
Dohra	dohra	k1gFnSc1
</s>
<s>
Wilhelm	Wilhelm	k1gMnSc1
Keitel	Keitel	k1gMnSc1
při	při	k7c6
podpisu	podpis	k1gInSc6
kapitulace	kapitulace	k1gFnSc2
8	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1945	#num#	k4
</s>
<s>
Rudá	rudý	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
měla	mít	k5eAaImAgFnS
při	při	k7c6
dobývání	dobývání	k1gNnSc6
města	město	k1gNnSc2
asi	asi	k9
80	#num#	k4
291	#num#	k4
padlých	padlý	k1gMnPc2
a	a	k8xC
pohřešovaných	pohřešovaný	k2eAgMnPc2d1
<g/>
,	,	kIx,
274	#num#	k4
184	#num#	k4
raněných	raněný	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přišla	přijít	k5eAaPmAgFnS
o	o	k7c4
téměř	téměř	k6eAd1
2	#num#	k4
000	#num#	k4
tanků	tank	k1gInPc2
a	a	k8xC
samohybných	samohybný	k2eAgNnPc2d1
děl	dělo	k1gNnPc2
a	a	k8xC
o	o	k7c4
500	#num#	k4
letadel	letadlo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Polská	polský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
ztratila	ztratit	k5eAaPmAgFnS
2825	#num#	k4
mrtvých	mrtvý	k1gMnPc2
a	a	k8xC
6	#num#	k4
067	#num#	k4
raněných	raněný	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Němci	Němec	k1gMnPc1
měli	mít	k5eAaImAgMnP
v	v	k7c6
důsledku	důsledek	k1gInSc6
bojů	boj	k1gInPc2
okolo	okolo	k7c2
100	#num#	k4
000	#num#	k4
padlých	padlý	k1gMnPc2
a	a	k8xC
nezvěstných	zvěstný	k2eNgInPc2d1
a	a	k8xC
dalších	další	k2eAgInPc2d1
480	#num#	k4
000	#num#	k4
zajatých	zajatá	k1gFnPc2
<g/>
,	,	kIx,
navíc	navíc	k6eAd1
v	v	k7c6
dobývaném	dobývaný	k2eAgInSc6d1
Berlíně	Berlín	k1gInSc6
zahynulo	zahynout	k5eAaPmAgNnS
velké	velký	k2eAgNnSc1d1
množství	množství	k1gNnSc1
civilního	civilní	k2eAgNnSc2d1
obyvatelstva	obyvatelstvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkový	celkový	k2eAgInSc1d1
počet	počet	k1gInSc1
nelze	lze	k6eNd1
přesně	přesně	k6eAd1
stanovit	stanovit	k5eAaPmF
<g/>
,	,	kIx,
protože	protože	k8xS
v	v	k7c6
Berlíně	Berlín	k1gInSc6
našly	najít	k5eAaPmAgInP
útočiště	útočiště	k1gNnSc4
desetitisíce	desetitisíce	k1gInPc4
uprchlíků	uprchlík	k1gMnPc2
před	před	k7c7
frontou	fronta	k1gFnSc7
<g/>
,	,	kIx,
především	především	k9
z	z	k7c2
východních	východní	k2eAgFnPc2d1
částí	část	k1gFnPc2
Německa	Německo	k1gNnSc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Válka	válka	k1gFnSc1
ještě	ještě	k9
zcela	zcela	k6eAd1
neskončila	skončit	k5eNaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Boje	boj	k1gInSc2
menší	malý	k2eAgFnSc2d2
intenzity	intenzita	k1gFnSc2
na	na	k7c6
území	území	k1gNnSc6
Německa	Německo	k1gNnSc2
a	a	k8xC
Protektorátu	protektorát	k1gInSc2
pokračovaly	pokračovat	k5eAaImAgInP
až	až	k9
do	do	k7c2
kapitulace	kapitulace	k1gFnSc2
ještě	ještě	k9
několik	několik	k4yIc4
následujících	následující	k2eAgInPc2d1
dní	den	k1gInPc2
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
přes	přes	k7c4
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
ustupovalo	ustupovat	k5eAaImAgNnS
velké	velký	k2eAgNnSc1d1
množství	množství	k1gNnSc1
německých	německý	k2eAgNnPc2d1
vojsk	vojsko	k1gNnPc2
<g/>
,	,	kIx,
vypuklo	vypuknout	k5eAaPmAgNnS
povstání	povstání	k1gNnSc1
<g/>
,	,	kIx,
ke	k	k7c3
kterému	který	k3yRgNnSc3,k3yQgNnSc3,k3yIgNnSc3
Stalin	Stalin	k1gMnSc1
urychleně	urychleně	k6eAd1
vyslal	vyslat	k5eAaPmAgMnS
části	část	k1gFnPc4
1	#num#	k4
<g/>
.	.	kIx.
ukrajinského	ukrajinský	k2eAgInSc2d1
frontu	front	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
Němci	Němec	k1gMnPc1
v	v	k7c6
Remeši	Remeš	k1gFnSc6
podepsali	podepsat	k5eAaPmAgMnP
kapitulační	kapitulační	k2eAgInSc4d1
akt	akt	k1gInSc4
se	s	k7c7
západními	západní	k2eAgMnPc7d1
Spojenci	spojenec	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jelikož	jelikož	k8xS
nebyl	být	k5eNaImAgMnS
přítomen	přítomen	k2eAgInSc4d1
žádný	žádný	k3yNgInSc4
vyšší	vysoký	k2eAgMnSc1d2
sovětský	sovětský	k2eAgMnSc1d1
důstojník	důstojník	k1gMnSc1
<g/>
,	,	kIx,
opakoval	opakovat	k5eAaImAgInS
se	se	k3xPyFc4
akt	akt	k1gInSc1
8	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1945	#num#	k4
za	za	k7c2
přítomnosti	přítomnost	k1gFnSc2
sovětských	sovětský	k2eAgMnPc2d1
představitelů	představitel	k1gMnPc2
v	v	k7c6
Berlínském	berlínský	k2eAgNnSc6d1
předměstí	předměstí	k1gNnSc6
Karlshorst	Karlshorst	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgInSc7
dnem	den	k1gInSc7
oficiálně	oficiálně	k6eAd1
skončila	skončit	k5eAaPmAgFnS
2	#num#	k4
<g/>
.	.	kIx.
světová	světový	k2eAgFnSc1d1
válka	válka	k1gFnSc1
na	na	k7c6
evropském	evropský	k2eAgInSc6d1
kontinentě	kontinent	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgInPc1d1
výstřely	výstřel	k1gInPc1
války	válka	k1gFnSc2
padly	padnout	k5eAaImAgFnP,k5eAaPmAgFnP
11	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1945	#num#	k4
v	v	k7c6
Československu	Československo	k1gNnSc6
tři	tři	k4xCgFnPc1
dny	dna	k1gFnPc1
po	po	k7c6
kapitulaci	kapitulace	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Bitva	bitva	k1gFnSc1
o	o	k7c4
Berlín	Berlín	k1gInSc4
byla	být	k5eAaImAgFnS
vyvrcholením	vyvrcholení	k1gNnSc7
bojů	boj	k1gInPc2
2	#num#	k4
<g/>
.	.	kIx.
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
,	,	kIx,
při	při	k7c6
níž	jenž	k3xRgFnSc6
vlastní	vlastní	k2eAgFnSc6d1
rukou	ruka	k1gFnSc7
zemřel	zemřít	k5eAaPmAgMnS
vůdce	vůdce	k1gMnSc1
nacismu	nacismus	k1gInSc2
–	–	k?
Adolf	Adolf	k1gMnSc1
Hitler	Hitler	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
Stalina	Stalin	k1gMnSc4
bylo	být	k5eAaImAgNnS
dobytí	dobytí	k1gNnSc1
Berlína	Berlín	k1gInSc2
jeho	jeho	k3xOp3gMnSc7
největším	veliký	k2eAgInSc7d3
vojenským	vojenský	k2eAgInSc7d1
triumfem	triumf	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
prosté	prostý	k2eAgNnSc4d1
obyvatelstvo	obyvatelstvo	k1gNnSc4
osvobozených	osvobozený	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
to	ten	k3xDgNnSc4
bylo	být	k5eAaImAgNnS
největší	veliký	k2eAgNnSc1d3
zadostiučinění	zadostiučinění	k1gNnSc1
za	za	k7c4
útrapy	útrapa	k1gFnPc4
a	a	k8xC
strádání	strádání	k1gNnPc1
z	z	k7c2
let	léto	k1gNnPc2
1939	#num#	k4
<g/>
-	-	kIx~
<g/>
45	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Vztyčení	vztyčení	k1gNnSc1
vlajky	vlajka	k1gFnSc2
nad	nad	k7c7
Reichstagem	Reichstago	k1gNnSc7
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Heinriciho	Heinrici	k1gMnSc2
28	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
nahradil	nahradit	k5eAaPmAgMnS
generál	generál	k1gMnSc1
Kurt	Kurt	k1gMnSc1
Student	student	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
General	General	k1gMnSc1
Kurt	Kurt	k1gMnSc1
von	von	k1gInSc4
Tippelskirch	Tippelskirch	k1gInSc1
byl	být	k5eAaImAgInS
označen	označit	k5eAaPmNgInS
jako	jako	k8xS,k8xC
Heinriciho	Heinrici	k1gMnSc2
prozatímní	prozatímní	k2eAgFnSc1d1
náhrada	náhrada	k1gFnSc1
<g/>
,	,	kIx,
než	než	k8xS
dorazí	dorazit	k5eAaPmIp3nS
gen.	gen.	kA
Student	student	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Studenta	student	k1gMnSc2
však	však	k9
zajali	zajmout	k5eAaPmAgMnP
Britové	Brit	k1gMnPc1
a	a	k8xC
tak	tak	k6eAd1
nedorazil	dorazit	k5eNaPmAgInS
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Glantz	Glantz	k1gInSc1
<g/>
,	,	kIx,
p.	p.	k?
373	#num#	k4
<g/>
↑	↑	k?
První	první	k4xOgInPc1
sovětské	sovětský	k2eAgInPc1d1
odhady	odhad	k1gInPc1
byly	být	k5eAaImAgInP
okolo	okolo	k7c2
1	#num#	k4
million	million	k1gInSc4
vojáků	voják	k1gMnPc2
<g/>
,	,	kIx,
to	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
však	však	k9
přehnané	přehnaný	k2eAgFnPc4d1
(	(	kIx(
<g/>
Glantz	Glantz	k1gInSc4
<g/>
,	,	kIx,
p.	p.	k?
258	#num#	k4
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Beevor	Beevor	k1gInSc1
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
p.	p.	k?
2871	#num#	k4
2	#num#	k4
3	#num#	k4
Khrivosheev	Khrivosheva	k1gFnPc2
<g/>
,	,	kIx,
pp	pp	k?
<g/>
.	.	kIx.
219,220	219,220	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Glantz	Glantz	k1gInSc1
<g/>
,	,	kIx,
p.	p.	k?
271	#num#	k4
<g/>
↑	↑	k?
Antill	Antill	k1gMnSc1
<g/>
,	,	kIx,
p.	p.	k?
85	#num#	k4
<g/>
↑	↑	k?
Charles	Charles	k1gMnSc1
Winchester	Winchester	k1gMnSc1
<g/>
:	:	kIx,
Ostfornt	Ostfornt	k1gMnSc1
/	/	kIx~
Hitler	Hitler	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
War	War	k1gFnSc7
on	on	k3xPp3gMnSc1
Russia	Russium	k1gNnPc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Osprey	Ospre	k2eAgFnPc4d1
Pbl	Pbl	k1gFnPc4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Oxford	Oxford	k1gInSc1
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
142	#num#	k4
<g/>
↑	↑	k?
David	David	k1gMnSc1
E.	E.	kA
Glanz	Glanz	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Soviet-German	Soviet-German	k1gMnSc1
War	War	k1gMnSc1
1941	#num#	k4
<g/>
–	–	k?
<g/>
1945	#num#	k4
<g/>
:	:	kIx,
Myths	Myths	k1gInSc1
and	and	k?
Realities	Realities	k1gInSc1
<g/>
:	:	kIx,
A	a	k9
Survey	Survea	k1gFnSc2
Essay	Essaa	k1gFnSc2
Archivováno	archivovat	k5eAaBmNgNnS
17	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2011	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
2001	#num#	k4
<g/>
,	,	kIx,
Clemson	Clemson	k1gMnSc1
<g/>
,	,	kIx,
South	South	k1gMnSc1
Carolina	Carolina	k1gFnSc1
<g/>
,	,	kIx,
s.	s.	k?
94	#num#	k4
–	–	k?
96	#num#	k4
<g/>
↑	↑	k?
Stephen	Stephen	k1gInSc1
Ambrose	Ambrosa	k1gFnSc3
<g/>
:	:	kIx,
Vítězové	vítěz	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80-7217-426-6	80-7217-426-6	k4
Jota	jota	k1gNnPc2
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc1
2006	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
364	#num#	k4
–	–	k?
365	#num#	k4
<g/>
↑	↑	k?
John	John	k1gMnSc1
Macdonald	Macdonald	k1gMnSc1
<g/>
:	:	kIx,
Veľké	Veľký	k2eAgFnSc2d1
bitky	bitka	k1gFnSc2
druhej	druhat	k5eAaPmRp2nS,k5eAaBmRp2nS,k5eAaImRp2nS
svetovej	svetovat	k5eAaImRp2nS,k5eAaPmRp2nS
vojny	vojna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7145	#num#	k4
<g/>
-	-	kIx~
<g/>
185	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bratislava	Bratislava	k1gFnSc1
<g/>
,	,	kIx,
Slovart	Slovart	k1gInSc1
1995	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
166	#num#	k4
–	–	k?
179	#num#	k4
<g/>
↑	↑	k?
John	John	k1gMnSc1
and	and	k?
Ljubica	Ljubica	k1gMnSc1
Erickson	Erickson	k1gMnSc1
<g/>
:	:	kIx,
Hitler	Hitler	k1gMnSc1
versus	versus	k7c1
Stalin	Stalin	k1gMnSc1
The	The	k1gFnSc2
Eastern	Eastern	k1gMnSc1
Front	front	k1gInSc1
in	in	k?
Photographs	Photographs	k1gInSc1
ISBN	ISBN	kA
1	#num#	k4
<g/>
-	-	kIx~
<g/>
84222	#num#	k4
<g/>
-	-	kIx~
<g/>
242	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
London	London	k1gMnSc1
<g/>
,	,	kIx,
Carlton	Carlton	k1gInSc1
Books	Books	k1gInSc1
2001	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
211	#num#	k4
<g/>
↑	↑	k?
Charles	Charles	k1gMnSc1
Winchester	Winchester	k1gMnSc1
<g/>
:	:	kIx,
Ostfornt	Ostfornt	k1gMnSc1
/	/	kIx~
Hitler	Hitler	k1gMnSc1
'	'	kIx"
<g/>
s	s	k7c7
War	War	k1gFnSc7
on	on	k3xPp3gMnSc1
Russia	Russium	k1gNnPc4
<g/>
.	.	kIx.
<g/>
'	'	kIx"
'	'	kIx"
<g/>
Osprey	Osprea	k1gFnSc2
PBL	PBL	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Oxford	Oxford	k1gInSc1
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
144	#num#	k4
-	-	kIx~
145	#num#	k4
↑	↑	k?
Charles	Charles	k1gMnSc1
Winchester	Winchester	k1gMnSc1
<g/>
:	:	kIx,
Ostfornt	Ostfornt	k1gMnSc1
/	/	kIx~
Hitler	Hitler	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
War	War	k1gFnSc7
on	on	k3xPp3gMnSc1
Russia	Russium	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Osprey	Ospre	k2eAgFnPc4d1
Pbl	Pbl	k1gFnPc4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Oxford	Oxford	k1gInSc1
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
147	#num#	k4
<g/>
↑	↑	k?
Kolektiv	kolektiv	k1gInSc1
autorov	autorov	k1gInSc1
<g/>
:	:	kIx,
Na	na	k7c4
Berlín	Berlín	k1gInSc4
<g/>
!	!	kIx.
</s>
<s desamb="1">
Naše	náš	k3xOp1gNnSc1
Vojsko	vojsko	k1gNnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
1953	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
12	#num#	k4
<g/>
↑	↑	k?
OSIPOV	OSIPOV	kA
<g/>
,	,	kIx,
Sergej	Sergej	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jegorov	Jegorov	k1gInSc4
i	i	k8xC
Kantaria	Kantarium	k1gNnPc4
nebili	bít	k5eNaImAgMnP
pervymi	pervy	k1gFnPc7
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
gazeta	gazeta	k1gFnSc1
<g/>
.	.	kIx.
<g/>
aif	aif	k?
<g/>
.	.	kIx.
<g/>
ru	ru	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
po	po	k7c6
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
„	„	k?
<g/>
Geografie	geografie	k1gFnSc2
krvavých	krvavý	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Respekt	respekt	k1gInSc1
<g/>
.	.	kIx.
24	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
↑	↑	k?
Podíl	podíl	k1gInSc1
Ruska	Rusko	k1gNnSc2
na	na	k7c6
válečném	válečný	k2eAgNnSc6d1
úsilí	úsilí	k1gNnSc6
a	a	k8xC
obětech	oběť	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reflex	reflex	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
DE	DE	k?
ZAYAS	ZAYAS	kA
<g/>
,	,	kIx,
Alfred	Alfred	k1gMnSc1
M.	M.	kA
Review	Review	k1gMnSc1
<g/>
:	:	kIx,
Prussian	Prussian	k1gInSc1
Nights	Nightsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Review	Review	k1gMnSc1
of	of	k?
Politics	Politics	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
January	Januara	k1gFnSc2
2017	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
154	#num#	k4
<g/>
–	–	k?
<g/>
156	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
journal	journat	k5eAaImAgMnS,k5eAaPmAgMnS
<g/>
}}	}}	k?
označená	označený	k2eAgNnPc4d1
jako	jako	k8xS,k8xC
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Alexander	Alexandra	k1gFnPc2
Werth	Werth	k1gMnSc1
<g/>
:	:	kIx,
Od	od	k7c2
Stalingradu	Stalingrad	k1gInSc2
po	po	k7c4
Berlín	Berlín	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Epocha	epocha	k1gFnSc1
<g/>
,	,	kIx,
Bratislava	Bratislava	k1gFnSc1
<g/>
,	,	kIx,
1969	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
384	#num#	k4
–	–	k?
385	#num#	k4
<g/>
↑	↑	k?
Charles	Charles	k1gMnSc1
Winchester	Winchester	k1gMnSc1
<g/>
:	:	kIx,
Ostfornt	Ostfornt	k1gMnSc1
/	/	kIx~
Hitler	Hitler	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
War	War	k1gFnSc7
on	on	k3xPp3gMnSc1
Russia	Russium	k1gNnPc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Osprey	Ospre	k2eAgFnPc4d1
Pbl	Pbl	k1gFnPc4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Oxford	Oxford	k1gInSc1
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
147	#num#	k4
<g/>
↑	↑	k?
Autobiografie	autobiografie	k1gFnSc1
prolamuje	prolamovat	k5eAaImIp3nS
tabu	tabu	k1gNnSc7
o	o	k7c6
znásilněních	znásilnění	k1gNnPc6
Rudé	rudý	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
aktualne	aktualnout	k5eAaPmIp3nS,k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
centrum	centrum	k1gNnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
bitka	bitka	k1gFnSc1
o	o	k7c4
Berlín	Berlín	k1gInSc4
na	na	k7c6
slovenské	slovenský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
BAHM	BAHM	kA
<g/>
,	,	kIx,
Karl	Karl	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Berlín	Berlín	k1gInSc1
1945	#num#	k4
<g/>
:	:	kIx,
Konečné	Konečné	k2eAgNnSc1d1
zúčtování	zúčtování	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
Těšín	Těšín	k1gInSc1
<g/>
:	:	kIx,
Ottovo	Ottův	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7360	#num#	k4
<g/>
-	-	kIx~
<g/>
217	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
RYAN	RYAN	kA
<g/>
,	,	kIx,
Cornelius	Cornelius	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgFnSc1d1
bitva	bitva	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
NLN	NLN	kA
-	-	kIx~
Nakladatelství	nakladatelství	k1gNnSc1
Lidové	lidový	k2eAgFnSc2d1
noviny	novina	k1gFnSc2
<g/>
,	,	kIx,
1993	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7106	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Filmotéka	filmotéka	k1gFnSc1
</s>
<s>
Osvobození	osvobození	k1gNnSc1
IV	Iva	k1gFnPc2
<g/>
.	.	kIx.
-	-	kIx~
Bitva	bitva	k1gFnSc1
o	o	k7c4
Berlín	Berlín	k1gInSc4
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Jurij	Jurij	k1gMnSc1
Ozerov	Ozerov	k1gInSc1
</s>
<s>
Osvobození	osvobození	k1gNnSc1
V.	V.	kA
-	-	kIx~
Poslední	poslední	k2eAgInSc1d1
úder	úder	k1gInSc1
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Jurij	Jurij	k1gMnSc1
Ozerov	Ozerov	k1gInSc1
</s>
<s>
Pád	Pád	k1gInSc1
Třetí	třetí	k4xOgFnSc2
říše	říš	k1gFnSc2
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
<g/>
:	:	kIx,
Oliver	Oliver	k1gMnSc1
Hirschbiegel	Hirschbiegel	k1gMnSc1
</s>
<s>
Video	video	k1gNnSc1
</s>
<s>
Velká	velký	k2eAgFnSc1d1
vlastenecká	vlastenecký	k2eAgFnSc1d1
válka	válka	k1gFnSc1
-8	-8	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc4
Bitva	bitva	k1gFnSc1
o	o	k7c4
Berlín	Berlín	k1gInSc4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
bitva	bitva	k1gFnSc1
o	o	k7c4
Berlín	Berlín	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Glynwed	Glynwed	k1gInSc1
<g/>
:	:	kIx,
Poslední	poslední	k2eAgInPc1d1
boje	boj	k1gInPc1
9	#num#	k4
<g/>
.	.	kIx.
armády	armáda	k1gFnSc2
(	(	kIx(
<g/>
duben-květen	duben-květen	k2eAgInSc1d1
1945	#num#	k4
<g/>
)	)	kIx)
1	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
2	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
3	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
</s>
<s>
Glynved	Glynved	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Generál	generál	k1gMnSc1
Weidling	Weidling	k1gInSc4
o	o	k7c6
pádu	pád	k1gInSc6
Berlína	Berlín	k1gInSc2
</s>
<s>
Fotogalerie	Fotogalerie	k1gFnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Operace	operace	k1gFnSc1
a	a	k8xC
bitvy	bitva	k1gFnPc1
na	na	k7c6
východní	východní	k2eAgFnSc6d1
frontě	fronta	k1gFnSc6
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
</s>
<s>
Operace	operace	k1gFnSc1
Barbarossa	Barbarossa	k1gMnSc1
•	•	k?
Obrana	obrana	k1gFnSc1
Brestské	brestský	k2eAgFnPc1d1
pevnosti	pevnost	k1gFnPc1
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Bělostoku	Bělostok	k1gInSc2
a	a	k8xC
Minsku	Minsk	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Brodů	Brod	k1gInPc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Smolenska	Smolensko	k1gNnSc2
•	•	k?
Obležení	obležení	k1gNnSc2
Oděsy	Oděsa	k1gFnSc2
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c4
Kyjevský	kyjevský	k2eAgInSc4d1
kotel	kotel	k1gInSc4
•	•	k?
Bitva	bitva	k1gFnSc1
před	před	k7c7
Moskvou	Moskva	k1gFnSc7
•	•	k?
Pokračovací	pokračovací	k2eAgFnSc1d1
válka	válka	k1gFnSc1
•	•	k?
Obležení	obležení	k1gNnSc1
Leningradu	Leningrad	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c4
Sevastopol	Sevastopol	k1gInSc4
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c4
Rostov	Rostov	k1gInSc4
•	•	k?
Kerčsko-feodosijská	Kerčsko-feodosijský	k2eAgFnSc1d1
operace	operace	k1gFnSc1
•	•	k?
Děmjanský	Děmjanský	k2eAgInSc1d1
<g />
.	.	kIx.
</s>
<s hack="1">
kotel	kotel	k1gInSc1
•	•	k?
Druhá	druhý	k4xOgFnSc1
bitva	bitva	k1gFnSc1
o	o	k7c4
Charkov	Charkov	k1gInSc4
•	•	k?
Operace	operace	k1gFnSc1
Blau	Blaus	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Voroněže	Voroněž	k1gFnSc2
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c4
Ržev	Ržev	k1gMnSc1
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c4
Kavkaz	Kavkaz	k1gInSc4
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Stalingradu	Stalingrad	k1gInSc2
•	•	k?
Operace	operace	k1gFnSc1
Uran	Uran	k1gInSc1
•	•	k?
Operace	operace	k1gFnSc1
Saturn	Saturn	k1gInSc1
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c6
Velikije	Velikije	k1gFnSc6
Luki	Luk	k1gFnSc2
•	•	k?
Operace	operace	k1gFnSc1
Mars	Mars	k1gInSc1
•	•	k?
Mansteinova	Mansteinův	k2eAgFnSc1d1
jarní	jarní	k2eAgFnSc1d1
ofenzíva	ofenzíva	k1gFnSc1
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Sokolova	Sokolov	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
<g />
.	.	kIx.
</s>
<s hack="1">
Kurska	Kursk	k1gInSc2
•	•	k?
Operace	operace	k1gFnSc1
Kutuzov	Kutuzovo	k1gNnPc2
•	•	k?
Operace	operace	k1gFnSc2
Vojevůdce	vojevůdce	k1gMnSc1
Rumjancev	Rumjancev	k1gFnSc1
•	•	k?
Operace	operace	k1gFnSc1
Suvorov	Suvorov	k1gInSc1
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c4
Dněpr	Dněpr	k1gInSc4
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c4
Kyjev	Kyjev	k1gInSc4
•	•	k?
Žytomyrsko-berdyčevská	Žytomyrsko-berdyčevský	k2eAgFnSc1d1
operace	operace	k1gFnSc1
•	•	k?
Leningradsko-novgorodská	leningradsko-novgorodský	k2eAgFnSc1d1
operace	operace	k1gFnSc1
•	•	k?
Korsuň-ševčenkovská	Korsuň-ševčenkovský	k2eAgFnSc1d1
operace	operace	k1gFnSc1
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Kamence	Kamenec	k1gInSc2
Podolského	podolský	k2eAgInSc2d1
•	•	k?
Krymská	krymský	k2eAgFnSc1d1
operace	operace	k1gFnSc1
•	•	k?
Operace	operace	k1gFnSc1
Bagration	Bagration	k1gInSc1
•	•	k?
Bitva	bitva	k1gFnSc1
v	v	k7c6
oblasti	oblast	k1gFnSc6
Tali-Ihantala	Tali-Ihantal	k1gMnSc2
•	•	k?
Lvovsko-sandoměřská	Lvovsko-sandoměřský	k2eAgFnSc1d1
operace	operace	k1gFnSc1
•	•	k?
Jasko-kišiněvská	Jasko-kišiněvský	k2eAgFnSc1d1
operace	operace	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Karpatsko-dukelská	karpatsko-dukelský	k2eAgFnSc1d1
operace	operace	k1gFnSc1
•	•	k?
Baltická	baltický	k2eAgFnSc1d1
operace	operace	k1gFnSc1
•	•	k?
Bělehradská	bělehradský	k2eAgFnSc1d1
operace	operace	k1gFnSc1
•	•	k?
Budapešťská	budapešťský	k2eAgFnSc1d1
operace	operace	k1gFnSc1
•	•	k?
Viselsko-oderská	viselsko-oderský	k2eAgFnSc1d1
operace	operace	k1gFnSc1
•	•	k?
Západokarpatská	Západokarpatský	k2eAgFnSc1d1
operace	operace	k1gFnSc1
•	•	k?
Východopruská	východopruský	k2eAgFnSc1d1
operace	operace	k1gFnSc1
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Jasla	Jaslo	k1gNnSc2
•	•	k?
Boje	boj	k1gInPc1
o	o	k7c4
Liptovský	liptovský	k2eAgInSc4d1
Mikuláš	mikuláš	k1gInSc4
•	•	k?
Východopomořanská	Východopomořanský	k2eAgFnSc1d1
operace	operace	k1gFnSc1
•	•	k?
Operace	operace	k1gFnSc2
Jarní	jarní	k2eAgNnSc1d1
probuzení	probuzení	k1gNnSc1
•	•	k?
Ostravská	ostravský	k2eAgFnSc1d1
operace	operace	k1gFnSc1
•	•	k?
Hornoslezská	hornoslezský	k2eAgFnSc1d1
operace	operace	k1gFnSc1
•	•	k?
Bratislavsko-brněnská	bratislavsko-brněnský	k2eAgFnSc1d1
operace	operace	k1gFnSc1
•	•	k?
Vídeňská	vídeňský	k2eAgFnSc1d1
operace	operace	k1gFnSc1
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c4
Berlín	Berlín	k1gInSc4
•	•	k?
Pražská	pražský	k2eAgFnSc1d1
operace	operace	k1gFnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc4
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc4
<g/>
:	:	kIx,
Druhá	druhý	k4xOgFnSc1
světová	světový	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
