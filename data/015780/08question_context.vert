<s>
Bitva	bitva	k1gFnSc1
o	o	k7c4
Berlín	Berlín	k1gInSc4
(	(	kIx(
<g/>
útočícím	útočící	k2eAgInSc7d1
Sovětským	sovětský	k2eAgInSc7d1
svazem	svaz	k1gInSc7
označena	označit	k5eAaPmNgFnS
jako	jako	k8xC,k8xS
Berlínská	berlínský	k2eAgFnSc1d1
operace	operace	k1gFnSc1
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
jednou	jeden	k4xCgFnSc7
ze	z	k7c2
závěrečných	závěrečný	k2eAgFnPc2d1
ofenziv	ofenziva	k1gFnPc2
na	na	k7c6
evropských	evropský	k2eAgNnPc6d1
bojištích	bojiště	k1gNnPc6
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>