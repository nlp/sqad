<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
norský	norský	k2eAgMnSc1d1	norský
polárník	polárník	k1gMnSc1	polárník
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
jako	jako	k8xC	jako
první	první	k4xOgMnSc1	první
stanul	stanout	k5eAaPmAgInS	stanout
na	na	k7c6	na
jižním	jižní	k2eAgNnSc6d1	jižní
pólu	pólo	k1gNnSc6	pólo
<g/>
?	?	kIx.	?
</s>
