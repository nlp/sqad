<s>
Jižní	jižní	k2eAgInSc1d1	jižní
pól	pól	k1gInSc1	pól
<g/>
,	,	kIx,	,
též	též	k9	též
jižní	jižní	k2eAgFnSc1d1	jižní
točna	točna	k1gFnSc1	točna
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
je	být	k5eAaImIp3nS	být
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zemská	zemský	k2eAgFnSc1d1	zemská
osa	osa	k1gFnSc1	osa
protíná	protínat	k5eAaImIp3nS	protínat
zemský	zemský	k2eAgInSc4d1	zemský
povrch	povrch	k1gInSc4	povrch
<g/>
,	,	kIx,	,
a	a	k8xC	a
ze	z	k7c2	z
kterého	který	k3yRgInSc2	který
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
Země	zem	k1gFnSc2	zem
při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
rovníku	rovník	k1gInSc3	rovník
jeví	jevit	k5eAaImIp3nS	jevit
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
otáčela	otáčet	k5eAaImAgFnS	otáčet
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
hodinových	hodinový	k2eAgFnPc2d1	hodinová
ručiček	ručička	k1gFnPc2	ručička
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
výprava	výprava	k1gFnSc1	výprava
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
jižního	jižní	k2eAgInSc2d1	jižní
pólu	pól	k1gInSc2	pól
14	[number]	k4	14
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1911	[number]	k4	1911
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
Amundsenova	Amundsenův	k2eAgFnSc1d1	Amundsenova
expedice	expedice	k1gFnSc1	expedice
vedená	vedený	k2eAgFnSc1d1	vedená
norským	norský	k2eAgMnSc7d1	norský
polárníkem	polárník	k1gMnSc7	polárník
Roaldem	Roald	k1gMnSc7	Roald
Amundsenem	Amundsen	k1gMnSc7	Amundsen
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
stejnou	stejný	k2eAgFnSc4d1	stejná
dobu	doba	k1gFnSc4	doba
se	se	k3xPyFc4	se
pokoušela	pokoušet	k5eAaImAgFnS	pokoušet
jižní	jižní	k2eAgInSc4d1	jižní
pól	pól	k1gInSc4	pól
dobýt	dobýt	k5eAaPmF	dobýt
také	také	k9	také
expedice	expedice	k1gFnSc1	expedice
Terra	Terra	k1gFnSc1	Terra
Nova	nova	k1gFnSc1	nova
vedená	vedený	k2eAgFnSc1d1	vedená
Angličanem	Angličan	k1gMnSc7	Angličan
Robertem	Robert	k1gMnSc7	Robert
Scottem	Scott	k1gMnSc7	Scott
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
pětičlenná	pětičlenný	k2eAgFnSc1d1	pětičlenná
skupina	skupina	k1gFnSc1	skupina
dorazila	dorazit	k5eAaPmAgFnS	dorazit
na	na	k7c4	na
točnu	točna	k1gFnSc4	točna
o	o	k7c4	o
měsíc	měsíc	k1gInSc4	měsíc
později	pozdě	k6eAd2	pozdě
a	a	k8xC	a
na	na	k7c6	na
strastiplné	strastiplný	k2eAgFnSc6d1	strastiplná
zpáteční	zpáteční	k2eAgFnSc6d1	zpáteční
cestě	cesta	k1gFnSc6	cesta
všichni	všechen	k3xTgMnPc1	všechen
její	její	k3xOp3gMnPc1	její
členové	člen	k1gMnPc1	člen
zahynuli	zahynout	k5eAaPmAgMnP	zahynout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
na	na	k7c6	na
jižním	jižní	k2eAgNnSc6d1	jižní
pólu	pólo	k1gNnSc6	pólo
nachází	nacházet	k5eAaImIp3nS	nacházet
trvale	trvale	k6eAd1	trvale
obydlená	obydlený	k2eAgFnSc1d1	obydlená
polární	polární	k2eAgFnSc1d1	polární
stanice	stanice	k1gFnSc1	stanice
Amundsen-Scott	Amundsen-Scotta	k1gFnPc2	Amundsen-Scotta
<g/>
.	.	kIx.	.
</s>
<s>
Jižní	jižní	k2eAgInSc1d1	jižní
pól	pól	k1gInSc1	pól
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
2835	[number]	k4	2835
m	m	kA	m
nad	nad	k7c7	nad
mořem	moře	k1gNnSc7	moře
uprostřed	uprostřed	k6eAd1	uprostřed
v	v	k7c6	v
těchto	tento	k3xDgNnPc6	tento
místech	místo	k1gNnPc6	místo
rovného	rovný	k2eAgInSc2d1	rovný
2850	[number]	k4	2850
m	m	kA	m
silného	silný	k2eAgInSc2d1	silný
ledového	ledový	k2eAgInSc2d1	ledový
masivu	masiv	k1gInSc2	masiv
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
posouvá	posouvat	k5eAaImIp3nS	posouvat
rychlostí	rychlost	k1gFnSc7	rychlost
10	[number]	k4	10
m	m	kA	m
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
postavení	postavení	k1gNnSc2	postavení
polární	polární	k2eAgFnSc2d1	polární
stanice	stanice	k1gFnSc2	stanice
jsou	být	k5eAaImIp3nP	být
o	o	k7c6	o
jižním	jižní	k2eAgNnSc6d1	jižní
pólu	pólo	k1gNnSc6	pólo
známa	známo	k1gNnSc2	známo
některá	některý	k3yIgNnPc4	některý
klimatická	klimatický	k2eAgNnPc4d1	klimatické
data	datum	k1gNnPc4	datum
<g/>
.	.	kIx.	.
</s>
<s>
Zaznamenané	zaznamenaný	k2eAgFnPc1d1	zaznamenaná
teploty	teplota	k1gFnPc1	teplota
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
mezi	mezi	k7c7	mezi
-82,2	-82,2	k4	-82,2
a	a	k8xC	a
-13,6	-13,6	k4	-13,6
<g/>
°	°	k?	°
C	C	kA	C
s	s	k7c7	s
průměrnou	průměrný	k2eAgFnSc7d1	průměrná
hodnotou	hodnota	k1gFnSc7	hodnota
-49	-49	k4	-49
<g/>
°	°	k?	°
C.	C.	kA	C.
Vítr	vítr	k1gInSc1	vítr
zde	zde	k6eAd1	zde
fouká	foukat	k5eAaImIp3nS	foukat
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
5,5	[number]	k4	5,5
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Jižní	jižní	k2eAgInSc1d1	jižní
magnetický	magnetický	k2eAgInSc1d1	magnetický
pól	pól	k1gInSc1	pól
Severní	severní	k2eAgInSc1d1	severní
pól	pól	k1gInSc4	pól
Severní	severní	k2eAgInSc4d1	severní
magnetický	magnetický	k2eAgInSc4d1	magnetický
pól	pól	k1gInSc4	pól
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Jižní	jižní	k2eAgInSc4d1	jižní
pól	pól	k1gInSc4	pól
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
