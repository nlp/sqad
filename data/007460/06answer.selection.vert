<s>
Ernest	Ernest	k1gMnSc1	Ernest
Thompson	Thompson	k1gMnSc1	Thompson
Seton	Seton	k1gMnSc1	Seton
(	(	kIx(	(
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1860	[number]	k4	1860
South	Southa	k1gFnPc2	Southa
Shields	Shieldsa	k1gFnPc2	Shieldsa
<g/>
,	,	kIx,	,
Durhamské	Durhamský	k2eAgNnSc1d1	Durhamský
hrabství	hrabství	k1gNnSc1	hrabství
<g/>
,	,	kIx,	,
Anglie	Anglie	k1gFnSc1	Anglie
–	–	k?	–
23	[number]	k4	23
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1946	[number]	k4	1946
Seton	Seton	k1gNnSc4	Seton
Village	Village	k1gNnSc2	Village
<g/>
,	,	kIx,	,
Nové	Nové	k2eAgNnSc1d1	Nové
Mexiko	Mexiko	k1gNnSc1	Mexiko
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
ilustrátor	ilustrátor	k1gMnSc1	ilustrátor
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
zakladatel	zakladatel	k1gMnSc1	zakladatel
woodcrafterského	woodcrafterský	k2eAgNnSc2d1	woodcrafterské
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
.	.	kIx.	.
</s>
