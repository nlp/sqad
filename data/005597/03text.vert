<s>
Billboard	billboard	k1gInSc1	billboard
200	[number]	k4	200
je	být	k5eAaImIp3nS	být
hitparáda	hitparáda	k1gFnSc1	hitparáda
dvou	dva	k4xCgFnPc2	dva
set	set	k1gInSc4	set
nejprodávanějších	prodávaný	k2eAgFnPc2d3	nejprodávanější
hudebních	hudební	k2eAgFnPc2d1	hudební
desek	deska	k1gFnPc2	deska
a	a	k8xC	a
EP	EP	kA	EP
na	na	k7c6	na
území	území	k1gNnSc6	území
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Výsledky	výsledek	k1gInPc4	výsledek
publikuje	publikovat	k5eAaBmIp3nS	publikovat
každý	každý	k3xTgInSc4	každý
týden	týden	k1gInSc4	týden
magazín	magazín	k1gInSc1	magazín
Billboard	billboard	k1gInSc1	billboard
<g/>
,	,	kIx,	,
první	první	k4xOgInSc1	první
žebříček	žebříček	k1gInSc1	žebříček
byl	být	k5eAaImAgInS	být
zveřejněn	zveřejnit	k5eAaPmNgInS	zveřejnit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
poprvé	poprvé	k6eAd1	poprvé
začal	začít	k5eAaPmAgInS	začít
být	být	k5eAaImF	být
vydáván	vydáván	k2eAgInSc4d1	vydáván
žebříček	žebříček	k1gInSc4	žebříček
hitparády	hitparáda	k1gFnSc2	hitparáda
Billboard	billboard	k1gInSc4	billboard
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
však	však	k9	však
jen	jen	k9	jen
pětimístný	pětimístný	k2eAgMnSc1d1	pětimístný
<g/>
.	.	kIx.	.
</s>
<s>
Nebyl	být	k5eNaImAgInS	být
vydáván	vydávat	k5eAaPmNgInS	vydávat
jednou	jednou	k6eAd1	jednou
týdně	týdně	k6eAd1	týdně
<g/>
,	,	kIx,	,
tak	tak	k9	tak
jako	jako	k9	jako
dnes	dnes	k6eAd1	dnes
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
tři	tři	k4xCgInPc4	tři
až	až	k9	až
sedm	sedm	k4xCc4	sedm
týdnů	týden	k1gInPc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
Změna	změna	k1gFnSc1	změna
přišla	přijít	k5eAaPmAgFnS	přijít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1955	[number]	k4	1955
s	s	k7c7	s
rozmachem	rozmach	k1gInSc7	rozmach
rockenrolu	rockenrol	k1gInSc2	rockenrol
<g/>
.	.	kIx.	.
</s>
<s>
Žebříček	žebříček	k1gInSc1	žebříček
byl	být	k5eAaImAgMnS	být
rozšířen	rozšířit	k5eAaPmNgMnS	rozšířit
na	na	k7c4	na
patnáct	patnáct	k4xCc4	patnáct
míst	místo	k1gNnPc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
24	[number]	k4	24
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1956	[number]	k4	1956
začal	začít	k5eAaPmAgMnS	začít
vycházet	vycházet	k5eAaImF	vycházet
týdenní	týdenní	k2eAgInSc4d1	týdenní
žebříček	žebříček	k1gInSc4	žebříček
Best-Selling	Best-Selling	k1gInSc1	Best-Selling
Popular	Populara	k1gFnPc2	Populara
Albums	Albumsa	k1gFnPc2	Albumsa
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
rozmezí	rozmezí	k1gNnSc1	rozmezí
žebříčku	žebříček	k1gInSc2	žebříček
začalo	začít	k5eAaPmAgNnS	začít
pohybovat	pohybovat	k5eAaImF	pohybovat
od	od	k7c2	od
10	[number]	k4	10
do	do	k7c2	do
30	[number]	k4	30
alb	album	k1gNnPc2	album
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
nejprodávanějším	prodávaný	k2eAgInSc7d3	nejprodávanější
albem	album	k1gNnSc7	album
na	na	k7c6	na
pravidelném	pravidelný	k2eAgInSc6d1	pravidelný
týdenním	týdenní	k2eAgInSc6d1	týdenní
žebříčku	žebříček	k1gInSc6	žebříček
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
album	album	k1gNnSc1	album
Belafonte	Belafont	k1gMnSc5	Belafont
Harryho	Harryha	k1gMnSc5	Harryha
Belafonteho	Belafonteha	k1gMnSc5	Belafonteha
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1959	[number]	k4	1959
se	se	k3xPyFc4	se
hitparáda	hitparáda	k1gFnSc1	hitparáda
Billboard	billboard	k1gInSc1	billboard
rozštěpila	rozštěpit	k5eAaPmAgFnS	rozštěpit
na	na	k7c4	na
Best-Selling	Best-Selling	k1gInSc4	Best-Selling
Stereophonic	Stereophonice	k1gFnPc2	Stereophonice
LPs	LPs	k1gFnPc2	LPs
(	(	kIx(	(
<g/>
30	[number]	k4	30
míst	místo	k1gNnPc2	místo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
alba	album	k1gNnPc4	album
se	s	k7c7	s
stereo	stereo	k2eAgInSc7d1	stereo
zvukem	zvuk	k1gInSc7	zvuk
a	a	k8xC	a
Best-Selling	Best-Selling	k1gInSc1	Best-Selling
Monophonic	Monophonice	k1gFnPc2	Monophonice
LPs	LPs	k1gFnPc2	LPs
(	(	kIx(	(
<g/>
50	[number]	k4	50
míst	místo	k1gNnPc2	místo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
alba	album	k1gNnPc4	album
s	s	k7c7	s
mono	mono	k2eAgInSc7d1	mono
zvukem	zvuk	k1gInSc7	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
byly	být	k5eAaImAgInP	být
přejmenovány	přejmenovat	k5eAaPmNgInP	přejmenovat
na	na	k7c4	na
Stereo	stereo	k2eAgInSc4d1	stereo
Action	Action	k1gInSc4	Action
Chart	charta	k1gFnPc2	charta
(	(	kIx(	(
<g/>
30	[number]	k4	30
míst	místo	k1gNnPc2	místo
<g/>
)	)	kIx)	)
a	a	k8xC	a
Mono	mono	k2eAgInSc1d1	mono
Action	Action	k1gInSc1	Action
Chart	charta	k1gFnPc2	charta
(	(	kIx(	(
<g/>
40	[number]	k4	40
míst	místo	k1gNnPc2	místo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1961	[number]	k4	1961
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
přejmenovaly	přejmenovat	k5eAaPmAgFnP	přejmenovat
na	na	k7c4	na
Action	Action	k1gInSc4	Action
Albums	Albums	k1gInSc4	Albums
-	-	kIx~	-
Stereophonic	Stereophonice	k1gFnPc2	Stereophonice
(	(	kIx(	(
<g/>
15	[number]	k4	15
míst	místo	k1gNnPc2	místo
<g/>
)	)	kIx)	)
a	a	k8xC	a
Action	Action	k1gInSc1	Action
Albums	Albumsa	k1gFnPc2	Albumsa
-	-	kIx~	-
Monophonic	Monophonice	k1gFnPc2	Monophonice
(	(	kIx(	(
<g/>
25	[number]	k4	25
míst	místo	k1gNnPc2	místo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
tři	tři	k4xCgInPc4	tři
měsíce	měsíc	k1gInPc4	měsíc
později	pozdě	k6eAd2	pozdě
zaznamenaly	zaznamenat	k5eAaPmAgInP	zaznamenat
další	další	k2eAgFnSc4d1	další
změnu	změna	k1gFnSc4	změna
názvů	název	k1gInPc2	název
na	na	k7c4	na
Top	topit	k5eAaImRp2nS	topit
LPs	LPs	k1gMnSc5	LPs
-	-	kIx~	-
Stereo	stereo	k2eAgFnSc7d1	stereo
(	(	kIx(	(
<g/>
50	[number]	k4	50
míst	místo	k1gNnPc2	místo
<g/>
)	)	kIx)	)
a	a	k8xC	a
Top	topit	k5eAaImRp2nS	topit
LPs	LPs	k1gMnSc5	LPs
-	-	kIx~	-
Monaural	Monaural	k1gMnSc6	Monaural
(	(	kIx(	(
<g/>
150	[number]	k4	150
míst	místo	k1gNnPc2	místo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
17	[number]	k4	17
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1963	[number]	k4	1963
byly	být	k5eAaImAgInP	být
žebříčky	žebříček	k1gInPc1	žebříček
sloučeny	sloučen	k2eAgInPc1d1	sloučen
do	do	k7c2	do
Top	topit	k5eAaImRp2nS	topit
LPs	LPs	k1gMnSc1	LPs
(	(	kIx(	(
<g/>
150	[number]	k4	150
míst	místo	k1gNnPc2	místo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gInSc1	jeho
rozsah	rozsah	k1gInSc1	rozsah
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
nejdříve	dříve	k6eAd3	dříve
na	na	k7c4	na
175	[number]	k4	175
a	a	k8xC	a
13	[number]	k4	13
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
na	na	k7c4	na
konečných	konečný	k2eAgNnPc2d1	konečné
200	[number]	k4	200
míst	místo	k1gNnPc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
hitparáda	hitparáda	k1gFnSc1	hitparáda
přejmenována	přejmenovat	k5eAaPmNgFnS	přejmenovat
na	na	k7c4	na
Top	topit	k5eAaImRp2nS	topit
LPs	LPs	k1gFnSc2	LPs
&	&	k?	&
Tapes	Tapes	k1gMnSc1	Tapes
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
na	na	k7c6	na
Top	topit	k5eAaImRp2nS	topit
Pop	pop	k1gInSc1	pop
Albums	Albumsa	k1gFnPc2	Albumsa
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
na	na	k7c4	na
The	The	k1gFnSc4	The
Billboard	billboard	k1gInSc1	billboard
200	[number]	k4	200
top	topit	k5eAaImRp2nS	topit
Albums	Albums	k1gInSc4	Albums
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
na	na	k7c4	na
nynější	nynější	k2eAgFnSc4d1	nynější
The	The	k1gFnSc4	The
Billboard	billboard	k1gInSc1	billboard
200	[number]	k4	200
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
26	[number]	k4	26
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1991	[number]	k4	1991
hitparáda	hitparáda	k1gFnSc1	hitparáda
Billboard	billboard	k1gInSc1	billboard
čerpá	čerpat	k5eAaImIp3nS	čerpat
data	datum	k1gNnPc4	datum
z	z	k7c2	z
databáze	databáze	k1gFnSc2	databáze
prodejů	prodej	k1gInPc2	prodej
vedenou	vedený	k2eAgFnSc4d1	vedená
společností	společnost	k1gFnSc7	společnost
Nielsen	Nielsen	k2eAgInSc4d1	Nielsen
SoundScan	SoundScan	k1gInSc4	SoundScan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
tato	tento	k3xDgFnSc1	tento
společnost	společnost	k1gFnSc1	společnost
spravovala	spravovat	k5eAaImAgFnS	spravovat
data	datum	k1gNnPc4	datum
o	o	k7c4	o
více	hodně	k6eAd2	hodně
jak	jak	k8xC	jak
14	[number]	k4	14
000	[number]	k4	000
hudebních	hudební	k2eAgMnPc2d1	hudební
umělcích	umělec	k1gMnPc6	umělec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
sestavil	sestavit	k5eAaPmAgInS	sestavit
magazín	magazín	k1gInSc1	magazín
Billboard	billboard	k1gInSc1	billboard
žebříček	žebříček	k1gInSc4	žebříček
největších	veliký	k2eAgInPc2d3	veliký
úspěchů	úspěch	k1gInPc2	úspěch
za	za	k7c2	za
posledních	poslední	k2eAgNnPc2d1	poslední
52	[number]	k4	52
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Rolling	Rolling	k1gInSc1	Rolling
Stones	Stones	k1gInSc1	Stones
(	(	kIx(	(
<g/>
37	[number]	k4	37
<g/>
)	)	kIx)	)
Frank	Frank	k1gMnSc1	Frank
Sinatra	Sinatr	k1gMnSc2	Sinatr
(	(	kIx(	(
<g/>
34	[number]	k4	34
<g/>
)	)	kIx)	)
Barbra	Barbr	k1gInSc2	Barbr
Streisand	Streisand	k1gInSc1	Streisand
(	(	kIx(	(
<g/>
33	[number]	k4	33
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Beatles	Beatles	k1gFnPc2	Beatles
(	(	kIx(	(
<g/>
32	[number]	k4	32
<g/>
)	)	kIx)	)
Elvis	Elvis	k1gMnSc1	Elvis
Presley	Preslea	k1gMnSc2	Preslea
(	(	kIx(	(
<g/>
27	[number]	k4	27
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Beatles	Beatles	k1gFnPc2	Beatles
(	(	kIx(	(
<g/>
19	[number]	k4	19
<g/>
)	)	kIx)	)
Jay-Z	Jay-Z	k1gFnSc2	Jay-Z
(	(	kIx(	(
<g/>
14	[number]	k4	14
<g/>
)	)	kIx)	)
Bruce	Bruec	k1gInSc2	Bruec
<g />
.	.	kIx.	.
</s>
<s>
Springsteen	Springsteen	k1gInSc1	Springsteen
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
)	)	kIx)	)
Barbra	Barbr	k1gInSc2	Barbr
Streisand	Streisand	k1gInSc1	Streisand
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
)	)	kIx)	)
Elvis	Elvis	k1gMnSc1	Elvis
Presley	Preslea	k1gMnSc2	Preslea
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
)	)	kIx)	)
Garth	Garth	k1gInSc1	Garth
Brooks	Brooks	k1gInSc1	Brooks
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
)	)	kIx)	)
The	The	k1gFnPc2	The
Rolling	Rolling	k1gInSc1	Rolling
Stones	Stones	k1gMnSc1	Stones
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Beatles	Beatles	k1gFnPc2	Beatles
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
)	)	kIx)	)
The	The	k1gFnPc2	The
Rolling	Rolling	k1gInSc1	Rolling
Stones	Stones	k1gMnSc1	Stones
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Eminem	Emin	k1gInSc7	Emin
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
)	)	kIx)	)
Elton	Elton	k1gMnSc1	Elton
John	John	k1gMnSc1	John
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
)	)	kIx)	)
Kanye	Kany	k1gInSc2	Kany
West	West	k1gInSc1	West
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
)	)	kIx)	)
Dave	Dav	k1gInSc5	Dav
Matthews	Matthewsa	k1gFnPc2	Matthewsa
Band	banda	k1gFnPc2	banda
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
Beyoncé	Beyoncé	k1gNnSc6	Beyoncé
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
Metallica	Metallic	k1gInSc2	Metallic
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
DMX	DMX	kA	DMX
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
Madonna	Madonn	k1gInSc2	Madonn
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
U2	U2	k1gMnSc6	U2
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
Wings	Wingsa	k1gFnPc2	Wingsa
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
Linkin	Linkin	k2eAgInSc1d1	Linkin
Park	park	k1gInSc1	park
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
Kanye	Kany	k1gInSc2	Kany
West	West	k1gInSc1	West
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
)	)	kIx)	)
Metallica	Metallic	k1gInSc2	Metallic
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
Beyoncé	Beyoncé	k1gNnSc6	Beyoncé
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
Dave	Dav	k1gInSc5	Dav
Matthews	Matthewsa	k1gFnPc2	Matthewsa
Band	banda	k1gFnPc2	banda
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
Eminem	Emin	k1gInSc7	Emin
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
Madonna	Madonn	k1gInSc2	Madonn
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
U2	U2	k1gFnSc2	U2
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
Disturbed	Disturbed	k1gInSc1	Disturbed
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Beatles	Beatles	k1gFnPc2	Beatles
(	(	kIx(	(
<g/>
132	[number]	k4	132
<g/>
)	)	kIx)	)
Elvis	Elvis	k1gMnSc1	Elvis
Presley	Preslea	k1gMnSc2	Preslea
(	(	kIx(	(
<g/>
67	[number]	k4	67
<g/>
)	)	kIx)	)
Michael	Michael	k1gMnSc1	Michael
Jackson	Jackson	k1gMnSc1	Jackson
(	(	kIx(	(
<g/>
51	[number]	k4	51
<g/>
)	)	kIx)	)
Garth	Garth	k1gInSc1	Garth
Brooks	Brooks	k1gInSc1	Brooks
(	(	kIx(	(
<g/>
51	[number]	k4	51
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Whitney	Whitnea	k1gFnPc1	Whitnea
Houston	Houston	k1gInSc1	Houston
(	(	kIx(	(
<g/>
46	[number]	k4	46
<g/>
)	)	kIx)	)
The	The	k1gFnPc4	The
Kingston	Kingston	k1gInSc4	Kingston
Trio	trio	k1gNnSc1	trio
(	(	kIx(	(
<g/>
46	[number]	k4	46
<g/>
)	)	kIx)	)
Prince	princa	k1gFnSc6	princa
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
2016	[number]	k4	2016
The	The	k1gFnPc4	The
Kingston	Kingston	k1gInSc4	Kingston
Trio	trio	k1gNnSc1	trio
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
1959	[number]	k4	1959
Herb	Herb	k1gMnSc1	Herb
Alpert	Alpert	k1gMnSc1	Alpert
&	&	k?	&
the	the	k?	the
Tijuana	Tijuana	k1gFnSc1	Tijuana
Brass	Brassa	k1gFnPc2	Brassa
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
1966	[number]	k4	1966
Peter	Peter	k1gMnSc1	Peter
<g/>
,	,	kIx,	,
Paul	Paul	k1gMnSc1	Paul
&	&	k?	&
Mary	Mary	k1gFnSc1	Mary
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
Whitney	Whitnea	k1gFnSc2	Whitnea
Houston	Houston	k1gInSc1	Houston
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
2012	[number]	k4	2012
Led	led	k1gInSc1	led
Zeppelin	Zeppelina	k1gFnPc2	Zeppelina
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
2014	[number]	k4	2014
Prince	princa	k1gFnSc6	princa
(	(	kIx(	(
<g/>
19	[number]	k4	19
<g/>
)	)	kIx)	)
2016	[number]	k4	2016
The	The	k1gFnPc2	The
Beatles	Beatles	k1gFnPc2	Beatles
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
)	)	kIx)	)
2014	[number]	k4	2014
Whitney	Whitnea	k1gFnSc2	Whitnea
Houston	Houston	k1gInSc1	Houston
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
)	)	kIx)	)
2012	[number]	k4	2012
David	David	k1gMnSc1	David
Bowie	Bowie	k1gFnSc1	Bowie
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
2016	[number]	k4	2016
Led	led	k1gInSc1	led
Zeppelin	Zeppelina	k1gFnPc2	Zeppelina
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
)	)	kIx)	)
1979	[number]	k4	1979
Crosby	Crosba	k1gFnSc2	Crosba
<g/>
,	,	kIx,	,
Stills	Stills	k1gInSc1	Stills
<g/>
,	,	kIx,	,
Nash	Nash	k1gMnSc1	Nash
&	&	k?	&
Young	Young	k1gMnSc1	Young
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
)	)	kIx)	)
1971	[number]	k4	1971
The	The	k1gMnSc1	The
Monkees	Monkees	k1gMnSc1	Monkees
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
)	)	kIx)	)
1986	[number]	k4	1986
U2	U2	k1gFnPc2	U2
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
)	)	kIx)	)
Pearl	Pearl	k1gInSc1	Pearl
Jam	jam	k1gInSc1	jam
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
)	)	kIx)	)
2001	[number]	k4	2001
West	Westum	k1gNnPc2	Westum
Side	Side	k1gFnSc1	Side
Story	story	k1gFnSc1	story
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
soundtrack	soundtrack	k1gInSc1	soundtrack
(	(	kIx(	(
<g/>
54	[number]	k4	54
<g/>
)	)	kIx)	)
Thriller	thriller	k1gInSc1	thriller
-	-	kIx~	-
Michael	Michael	k1gMnSc1	Michael
Jackson	Jackson	k1gMnSc1	Jackson
(	(	kIx(	(
<g/>
37	[number]	k4	37
<g/>
)	)	kIx)	)
Rumours	Rumours	k1gInSc1	Rumours
-	-	kIx~	-
Fleetwood	Fleetwood	k1gInSc1	Fleetwood
Mac	Mac	kA	Mac
(	(	kIx(	(
<g/>
31	[number]	k4	31
<g/>
)	)	kIx)	)
Jižní	jižní	k2eAgInSc1d1	jižní
Pacifik	Pacifik	k1gInSc1	Pacifik
-	-	kIx~	-
soundtrack	soundtrack	k1gInSc1	soundtrack
(	(	kIx(	(
<g/>
31	[number]	k4	31
<g/>
)	)	kIx)	)
Calypso	Calypsa	k1gFnSc5	Calypsa
-	-	kIx~	-
Harry	Harra	k1gFnPc1	Harra
Belafonte	Belafont	k1gInSc5	Belafont
(	(	kIx(	(
<g/>
31	[number]	k4	31
<g/>
)	)	kIx)	)
21	[number]	k4	21
-	-	kIx~	-
Adele	Adelo	k1gNnSc6	Adelo
(	(	kIx(	(
<g/>
24	[number]	k4	24
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Purple	Purple	k6eAd1	Purple
Rain	Rain	k1gInSc4	Rain
-	-	kIx~	-
Prince	princ	k1gMnSc2	princ
a	a	k8xC	a
the	the	k?	the
Revolution	Revolution	k1gInSc1	Revolution
<g/>
/	/	kIx~	/
<g/>
soundtrack	soundtrack	k1gInSc1	soundtrack
(	(	kIx(	(
<g/>
24	[number]	k4	24
<g/>
)	)	kIx)	)
Saturday	Saturdaa	k1gFnSc2	Saturdaa
Night	Night	k1gMnSc1	Night
Fever	Fever	k1gMnSc1	Fever
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Original	Original	k1gMnSc1	Original
Movie	Movie	k1gFnSc2	Movie
Sound	Sound	k1gMnSc1	Sound
Track	Track	k1gMnSc1	Track
-	-	kIx~	-
Bee	Bee	k1gFnSc1	Bee
Gees	Gees	k1gInSc1	Gees
<g/>
/	/	kIx~	/
<g/>
soundtrack	soundtrack	k1gInSc1	soundtrack
(	(	kIx(	(
<g/>
24	[number]	k4	24
<g/>
)	)	kIx)	)
Please	Pleasa	k1gFnSc6	Pleasa
Hammer	Hammra	k1gFnPc2	Hammra
<g/>
,	,	kIx,	,
Don	dona	k1gFnPc2	dona
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Hurt	Hurt	k1gMnSc1	Hurt
'	'	kIx"	'
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Em	Ema	k1gFnPc2	Ema
-	-	kIx~	-
MC	MC	kA	MC
Hammer	Hammer	k1gMnSc1	Hammer
(	(	kIx(	(
<g/>
21	[number]	k4	21
<g/>
)	)	kIx)	)
Osobní	osobní	k2eAgMnSc1d1	osobní
strážce	strážce	k1gMnSc1	strážce
-	-	kIx~	-
Whitney	Whitney	k1gInPc4	Whitney
Houston	Houston	k1gInSc1	Houston
<g/>
/	/	kIx~	/
<g/>
soundtrack	soundtrack	k1gInSc1	soundtrack
(	(	kIx(	(
<g/>
20	[number]	k4	20
<g/>
)	)	kIx)	)
Blue	Blue	k1gInSc4	Blue
Hawaii	Hawaie	k1gFnSc4	Hawaie
-	-	kIx~	-
Elvis	Elvis	k1gMnSc1	Elvis
Presley	Preslea	k1gFnSc2	Preslea
<g/>
/	/	kIx~	/
<g/>
soundtrack	soundtrack	k1gInSc1	soundtrack
(	(	kIx(	(
<g/>
20	[number]	k4	20
<g/>
)	)	kIx)	)
The	The	k1gFnSc6	The
Dark	Darka	k1gFnPc2	Darka
Side	Sid	k1gFnSc2	Sid
of	of	k?	of
the	the	k?	the
Moon	Moon	k1gInSc1	Moon
-	-	kIx~	-
Pink	pink	k2eAgInSc1d1	pink
Floyd	Floyd	k1gInSc1	Floyd
(	(	kIx(	(
<g/>
931	[number]	k4	931
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Johnny	Johnn	k1gInPc4	Johnn
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Greatest	Greatest	k1gInSc1	Greatest
Hits	Hits	k1gInSc4	Hits
-	-	kIx~	-
Johnny	Johnen	k2eAgFnPc1d1	Johnna
Mathis	Mathis	k1gFnPc1	Mathis
(	(	kIx(	(
<g/>
490	[number]	k4	490
<g/>
)	)	kIx)	)
My	my	k3xPp1nPc1	my
Fair	fair	k2eAgFnPc1d1	fair
Lady	lady	k1gFnSc3	lady
-	-	kIx~	-
obsazení	obsazení	k1gNnSc3	obsazení
(	(	kIx(	(
<g/>
480	[number]	k4	480
<g/>
)	)	kIx)	)
Legend	legenda	k1gFnPc2	legenda
-	-	kIx~	-
Bob	Bob	k1gMnSc1	Bob
Marley	Marlea	k1gFnSc2	Marlea
a	a	k8xC	a
the	the	k?	the
Wailers	Wailers	k1gInSc1	Wailers
(	(	kIx(	(
<g/>
479	[number]	k4	479
<g/>
)	)	kIx)	)
Journey	Journea	k1gFnSc2	Journea
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Greatest	Greatest	k1gInSc1	Greatest
Hits	Hits	k1gInSc1	Hits
-	-	kIx~	-
Journey	Journea	k1gFnPc1	Journea
(	(	kIx(	(
<g/>
470	[number]	k4	470
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Metallica	Metallic	k2eAgNnPc4d1	Metallic
-	-	kIx~	-
Metallica	Metallicum	k1gNnPc4	Metallicum
(	(	kIx(	(
<g/>
440	[number]	k4	440
<g/>
)	)	kIx)	)
Greatest	Greatest	k1gInSc1	Greatest
Hits	Hits	k1gInSc1	Hits
-	-	kIx~	-
Guns	Guns	k1gInSc1	Guns
N	N	kA	N
<g/>
'	'	kIx"	'
Roses	Roses	k1gMnSc1	Roses
(	(	kIx(	(
<g/>
408	[number]	k4	408
<g/>
)	)	kIx)	)
Nevermind	Nevermind	k1gInSc4	Nevermind
-	-	kIx~	-
Nirvana	Nirvan	k1gMnSc2	Nirvan
(	(	kIx(	(
<g/>
370	[number]	k4	370
<g/>
)	)	kIx)	)
Curtain	Curtain	k2eAgInSc1d1	Curtain
Call	Call	k1gInSc1	Call
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Hits	Hitsa	k1gFnPc2	Hitsa
-	-	kIx~	-
Eminem	Emin	k1gMnSc7	Emin
(	(	kIx(	(
<g/>
350	[number]	k4	350
<g/>
)	)	kIx)	)
21	[number]	k4	21
-	-	kIx~	-
Adele	Adelo	k1gNnSc6	Adelo
(	(	kIx(	(
<g/>
335	[number]	k4	335
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Doo-Wops	Doo-Wopsa	k1gFnPc2	Doo-Wopsa
&	&	k?	&
Hooligans	Hooligansa	k1gFnPc2	Hooligansa
–	–	k?	–
Bruno	Bruno	k1gMnSc1	Bruno
Mars	Mars	k1gMnSc1	Mars
(	(	kIx(	(
<g/>
333	[number]	k4	333
<g/>
)	)	kIx)	)
Fantom	fantom	k1gInSc1	fantom
opery	opera	k1gFnSc2	opera
-	-	kIx~	-
obsazení	obsazení	k1gNnSc1	obsazení
(	(	kIx(	(
<g/>
331	[number]	k4	331
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Eminem	Emino	k1gNnSc7	Emino
Show	show	k1gNnSc1	show
-	-	kIx~	-
Eminem	Emin	k1gInSc7	Emin
(	(	kIx(	(
<g/>
329	[number]	k4	329
<g/>
)	)	kIx)	)
Chronicle	Chronicle	k1gNnSc2	Chronicle
<g/>
:	:	kIx,	:
The	The	k1gFnPc2	The
20	[number]	k4	20
Greatest	Greatest	k1gFnSc4	Greatest
Hits	Hitsa	k1gFnPc2	Hitsa
-	-	kIx~	-
Creedence	Creedence	k1gFnSc1	Creedence
Clearwater	Clearwater	k1gMnSc1	Clearwater
Revival	revival	k1gInSc1	revival
(	(	kIx(	(
<g/>
324	[number]	k4	324
<g/>
)	)	kIx)	)
Tapestry	Tapestra	k1gFnSc2	Tapestra
-	-	kIx~	-
Carole	Carole	k1gFnSc1	Carole
King	King	k1gMnSc1	King
(	(	kIx(	(
<g/>
318	[number]	k4	318
<g/>
)	)	kIx)	)
Oklahoma	Oklahomum	k1gNnSc2	Oklahomum
<g/>
!	!	kIx.	!
</s>
<s>
–	–	k?	–
obsazení	obsazení	k1gNnSc1	obsazení
(	(	kIx(	(
<g/>
305	[number]	k4	305
<g/>
)	)	kIx)	)
Thriller	thriller	k1gInSc1	thriller
-	-	kIx~	-
Michael	Michael	k1gMnSc1	Michael
Jackson	Jackson	k1gMnSc1	Jackson
(	(	kIx(	(
<g/>
298	[number]	k4	298
<g/>
)	)	kIx)	)
Back	Back	k1gMnSc1	Back
in	in	k?	in
Black	Black	k1gMnSc1	Black
-	-	kIx~	-
AC	AC	kA	AC
<g/>
/	/	kIx~	/
<g/>
DC	DC	kA	DC
(	(	kIx(	(
<g/>
297	[number]	k4	297
<g/>
)	)	kIx)	)
Heavenly	Heavenla	k1gFnPc1	Heavenla
-	-	kIx~	-
Johnny	Johnen	k2eAgFnPc1d1	Johnna
Mathis	Mathis	k1gFnPc1	Mathis
(	(	kIx(	(
<g/>
295	[number]	k4	295
<g/>
)	)	kIx)	)
Led	led	k1gInSc1	led
Zeppelin	Zeppelina	k1gFnPc2	Zeppelina
IV	Iva	k1gFnPc2	Iva
-	-	kIx~	-
Led	led	k1gInSc1	led
Zeppelin	Zeppelina	k1gFnPc2	Zeppelina
(	(	kIx(	(
<g/>
289	[number]	k4	289
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Foundation	Foundation	k1gInSc1	Foundation
-	-	kIx~	-
Zac	Zac	k1gFnSc1	Zac
Brown	Brown	k1gMnSc1	Brown
Band	band	k1gInSc1	band
(	(	kIx(	(
<g/>
288	[number]	k4	288
<g/>
)	)	kIx)	)
Recovery	Recovera	k1gFnSc2	Recovera
-	-	kIx~	-
Eminem	Emin	k1gMnSc7	Emin
(	(	kIx(	(
<g/>
288	[number]	k4	288
<g/>
)	)	kIx)	)
Life	Lif	k1gInSc2	Lif
After	After	k1gMnSc1	After
Death	Death	k1gMnSc1	Death
-	-	kIx~	-
The	The	k1gMnSc1	The
Notorious	Notorious	k1gMnSc1	Notorious
B.I.G.	B.I.G.	k1gMnSc1	B.I.G.
(	(	kIx(	(
<g/>
176	[number]	k4	176
-	-	kIx~	-
1	[number]	k4	1
<g/>
)	)	kIx)	)
Vitalogy	Vitaloga	k1gFnSc2	Vitaloga
-	-	kIx~	-
Pearl	Pearl	k1gInSc1	Pearl
Jam	jam	k1gInSc1	jam
(	(	kIx(	(
<g/>
173	[number]	k4	173
-	-	kIx~	-
1	[number]	k4	1
<g/>
)	)	kIx)	)
In	In	k1gFnSc1	In
Rainbows	Rainbows	k1gInSc1	Rainbows
-	-	kIx~	-
Radiohead	Radiohead	k1gInSc1	Radiohead
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
156	[number]	k4	156
-	-	kIx~	-
1	[number]	k4	1
<g/>
)	)	kIx)	)
Ghetto	ghetto	k1gNnSc4	ghetto
D	D	kA	D
-	-	kIx~	-
Master	master	k1gMnSc1	master
P	P	kA	P
(	(	kIx(	(
<g/>
137	[number]	k4	137
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
More	mor	k1gInSc5	mor
of	of	k?	of
The	The	k1gMnSc1	The
Monkees	Monkees	k1gMnSc1	Monkees
-	-	kIx~	-
The	The	k1gMnSc1	The
Monkees	Monkees	k1gMnSc1	Monkees
(	(	kIx(	(
<g/>
122	[number]	k4	122
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
MP	MP	kA	MP
da	da	k?	da
Last	Last	k1gMnSc1	Last
Don	Don	k1gMnSc1	Don
-	-	kIx~	-
Master	master	k1gMnSc1	master
P	P	kA	P
(	(	kIx(	(
<g/>
112	[number]	k4	112
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
Beatles	Beatles	k1gFnPc2	Beatles
'	'	kIx"	'
<g/>
65	[number]	k4	65
-	-	kIx~	-
The	The	k1gFnSc1	The
Beatles	Beatles	k1gFnPc2	Beatles
(	(	kIx(	(
<g/>
98	[number]	k4	98
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
Help	help	k1gInSc1	help
<g/>
!	!	kIx.	!
</s>
<s>
-	-	kIx~	-
The	The	k1gMnSc3	The
Beatles	beatles	k1gMnSc3	beatles
(	(	kIx(	(
<g/>
61	[number]	k4	61
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
Rubber	Rubber	k1gInSc1	Rubber
Soul	Soul	k1gInSc1	Soul
-	-	kIx~	-
The	The	k1gFnSc1	The
Beatles	Beatles	k1gFnPc2	Beatles
(	(	kIx(	(
<g/>
60	[number]	k4	60
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
Ballad	Ballad	k1gInSc1	Ballad
of	of	k?	of
the	the	k?	the
Green	Green	k2eAgInSc4d1	Green
Berets	Berets	k1gInSc4	Berets
-	-	kIx~	-
Barry	Barra	k1gMnSc2	Barra
Sadler	Sadler	k1gMnSc1	Sadler
(	(	kIx(	(
<g/>
53	[number]	k4	53
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
This	This	k1gInSc1	This
House	house	k1gNnSc1	house
Is	Is	k1gFnPc2	Is
Not	nota	k1gFnPc2	nota
for	forum	k1gNnPc2	forum
Sale	Sale	k1gFnSc1	Sale
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
Bon	bon	k1gInSc1	bon
Jovi	Jov	k1gFnSc2	Jov
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
43	[number]	k4	43
<g/>
)	)	kIx)	)
A	a	k9	a
Pentatonix	Pentatonix	k1gInSc1	Pentatonix
Christmas	Christmas	k1gInSc1	Christmas
-	-	kIx~	-
Pentatonix	Pentatonix	k1gInSc1	Pentatonix
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
41	[number]	k4	41
<g/>
)	)	kIx)	)
Light	Light	k2eAgInSc1d1	Light
Grenades	Grenades	k1gInSc1	Grenades
–	–	k?	–
Incubus	Incubus	k1gInSc1	Incubus
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
37	[number]	k4	37
<g/>
)	)	kIx)	)
One	One	k1gFnSc1	One
More	mor	k1gInSc5	mor
Light	Light	k2eAgInSc1d1	Light
-	-	kIx~	-
Linkin	Linkin	k2eAgInSc1d1	Linkin
Park	park	k1gInSc1	park
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
<g />
.	.	kIx.	.
</s>
<s>
30	[number]	k4	30
<g/>
)	)	kIx)	)
Mission	Mission	k1gInSc1	Mission
Bell	bell	k1gInSc1	bell
–	–	k?	–
Amos	Amos	k1gMnSc1	Amos
Lee	Lea	k1gFnSc3	Lea
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
26	[number]	k4	26
<g/>
)	)	kIx)	)
I	i	k9	i
Like	Like	k1gNnSc2	Like
It	It	k1gFnSc2	It
When	When	k1gMnSc1	When
You	You	k1gMnSc1	You
Sleep	Sleep	k1gMnSc1	Sleep
<g/>
,	,	kIx,	,
for	forum	k1gNnPc2	forum
You	You	k1gMnSc1	You
Are	ar	k1gInSc5	ar
So	So	kA	So
Beautiful	Beautifula	k1gFnPc2	Beautifula
Yet	Yet	k1gFnPc2	Yet
So	So	kA	So
Unaware	Unawar	k1gMnSc5	Unawar
of	of	k?	of
It	It	k1gFnSc1	It
-	-	kIx~	-
The	The	k1gFnSc1	The
1975	[number]	k4	1975
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
26	[number]	k4	26
<g/>
)	)	kIx)	)
Showroom	Showroom	k1gInSc1	Showroom
of	of	k?	of
Compassion	Compassion	k1gInSc1	Compassion
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
Cake	Cake	k1gNnPc6	Cake
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
25	[number]	k4	25
<g/>
)	)	kIx)	)
Christmas	Christmas	k1gMnSc1	Christmas
-	-	kIx~	-
Michael	Michael	k1gMnSc1	Michael
Bublé	Bublý	k2eAgFnSc2d1	Bublý
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
24	[number]	k4	24
<g/>
)	)	kIx)	)
Blue	Blu	k1gInSc2	Blu
Slide	slide	k1gInSc1	slide
Park	park	k1gInSc1	park
-	-	kIx~	-
Mac	Mac	kA	Mac
Miller	Miller	k1gMnSc1	Miller
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
24	[number]	k4	24
<g/>
)	)	kIx)	)
Burning	Burning	k1gInSc1	Burning
Lights	Lights	k1gInSc1	Lights
-	-	kIx~	-
Chris	Chris	k1gInSc1	Chris
Tomlin	Tomlin	k1gInSc1	Tomlin
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
<g />
.	.	kIx.	.
</s>
<s>
22	[number]	k4	22
<g/>
)	)	kIx)	)
The	The	k1gFnSc6	The
Golden	Goldna	k1gFnPc2	Goldna
Age	Age	k1gFnSc2	Age
of	of	k?	of
Grotesque	Grotesque	k1gInSc1	Grotesque
-	-	kIx~	-
Marilyn	Marilyn	k1gFnSc1	Marilyn
Manson	Manson	k1gMnSc1	Manson
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Circle	Circle	k1gFnSc1	Circle
-	-	kIx~	-
Bon	bon	k1gInSc1	bon
Jovi	Jov	k1gFnSc2	Jov
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
19	[number]	k4	19
<g/>
)	)	kIx)	)
1000	[number]	k4	1000
Forms	Forms	k1gInSc1	Forms
of	of	k?	of
Fear	Fear	k1gInSc1	Fear
-	-	kIx~	-
Sia	Sia	k1gFnSc1	Sia
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
19	[number]	k4	19
<g/>
)	)	kIx)	)
Drones	Drones	k1gMnSc1	Drones
-	-	kIx~	-
Muse	Musa	k1gFnSc6	Musa
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
19	[number]	k4	19
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Inspiration	Inspiration	k1gInSc1	Inspiration
-	-	kIx~	-
Young	Young	k1gInSc1	Young
Jeezy	Jeeza	k1gFnSc2	Jeeza
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
18	[number]	k4	18
<g/>
)	)	kIx)	)
Handwritten	Handwritten	k2eAgMnSc1d1	Handwritten
-	-	kIx~	-
Shawn	Shawn	k1gMnSc1	Shawn
Mendes	Mendes	k1gMnSc1	Mendes
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
17	[number]	k4	17
<g/>
)	)	kIx)	)
Billboard	billboard	k1gInSc1	billboard
Hot	hot	k0	hot
100	[number]	k4	100
Billboard	billboard	k1gInSc1	billboard
200	[number]	k4	200
<g/>
-aktuální	ktuální	k2eAgInSc4d1	-aktuální
stav	stav	k1gInSc4	stav
</s>
