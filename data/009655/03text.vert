<p>
<s>
Tovaryšstvo	tovaryšstvo	k1gNnSc1	tovaryšstvo
Ježíšovo	Ježíšův	k2eAgNnSc1d1	Ježíšovo
(	(	kIx(	(
<g/>
Societas	Societas	k1gInSc1	Societas
Iesu	Iesus	k1gInSc2	Iesus
<g/>
,	,	kIx,	,
ve	v	k7c6	v
zkratce	zkratka	k1gFnSc6	zkratka
SI	si	k1gNnSc2	si
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
její	její	k3xOp3gFnSc4	její
mutaci	mutace	k1gFnSc4	mutace
pro	pro	k7c4	pro
užívaný	užívaný	k2eAgInSc4d1	užívaný
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
SJ	SJ	kA	SJ
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
TJ	tj	kA	tj
<g/>
,	,	kIx,	,
hovorově	hovorově	k6eAd1	hovorově
jezuitský	jezuitský	k2eAgInSc1d1	jezuitský
řád	řád	k1gInSc1	řád
nebo	nebo	k8xC	nebo
jezuité	jezuita	k1gMnPc1	jezuita
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgInPc2d3	veliký
a	a	k8xC	a
nejvýznamnějších	významný	k2eAgInPc2d3	nejvýznamnější
řeholních	řeholní	k2eAgInPc2d1	řeholní
řádů	řád	k1gInPc2	řád
římskokatolické	římskokatolický	k2eAgFnSc2d1	Římskokatolická
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Založen	založit	k5eAaPmNgInS	založit
byl	být	k5eAaImAgInS	být
sv.	sv.	kA	sv.
Ignácem	Ignác	k1gMnSc7	Ignác
z	z	k7c2	z
Loyoly	Loyola	k1gFnSc2	Loyola
roku	rok	k1gInSc2	rok
1534	[number]	k4	1534
(	(	kIx(	(
<g/>
papežem	papež	k1gMnSc7	papež
schválen	schválen	k2eAgInSc1d1	schválen
roku	rok	k1gInSc2	rok
1540	[number]	k4	1540
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1773	[number]	k4	1773
jej	on	k3xPp3gMnSc4	on
sice	sice	k8xC	sice
papež	papež	k1gMnSc1	papež
Klement	Klement	k1gMnSc1	Klement
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
na	na	k7c4	na
nátlak	nátlak	k1gInSc4	nátlak
evropských	evropský	k2eAgMnPc2d1	evropský
panovníků	panovník	k1gMnPc2	panovník
zrušil	zrušit	k5eAaPmAgMnS	zrušit
<g/>
,	,	kIx,	,
řád	řád	k1gInSc1	řád
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
fakticky	fakticky	k6eAd1	fakticky
nikdy	nikdy	k6eAd1	nikdy
zcela	zcela	k6eAd1	zcela
nezanikl	zaniknout	k5eNaPmAgMnS	zaniknout
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1814	[number]	k4	1814
jej	on	k3xPp3gMnSc4	on
papež	papež	k1gMnSc1	papež
Pius	Pius	k1gMnSc1	Pius
VII	VII	kA	VII
<g/>
.	.	kIx.	.
celosvětově	celosvětově	k6eAd1	celosvětově
obnovil	obnovit	k5eAaPmAgInS	obnovit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
má	mít	k5eAaImIp3nS	mít
řád	řád	k1gInSc1	řád
přibližně	přibližně	k6eAd1	přibližně
20	[number]	k4	20
200	[number]	k4	200
členů	člen	k1gMnPc2	člen
<g/>
.	.	kIx.	.
</s>
<s>
Současným	současný	k2eAgMnSc7d1	současný
generálem	generál	k1gMnSc7	generál
řádu	řád	k1gInSc2	řád
je	být	k5eAaImIp3nS	být
Arturo	Artura	k1gFnSc5	Artura
Sosa	Sosum	k1gNnSc2	Sosum
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
činnost	činnost	k1gFnSc1	činnost
se	se	k3xPyFc4	se
soustřeďuje	soustřeďovat	k5eAaImIp3nS	soustřeďovat
zejména	zejména	k9	zejména
na	na	k7c4	na
pastorační	pastorační	k2eAgFnSc4d1	pastorační
a	a	k8xC	a
misijní	misijní	k2eAgFnSc4d1	misijní
činnost	činnost	k1gFnSc4	činnost
a	a	k8xC	a
na	na	k7c4	na
oblast	oblast	k1gFnSc4	oblast
vědy	věda	k1gFnSc2	věda
a	a	k8xC	a
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
řádu	řád	k1gInSc2	řád
připojují	připojovat	k5eAaImIp3nP	připojovat
za	za	k7c4	za
své	svůj	k3xOyFgNnSc4	svůj
jméno	jméno	k1gNnSc4	jméno
zkratku	zkratka	k1gFnSc4	zkratka
řádu	řád	k1gInSc2	řád
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
působí	působit	k5eAaImIp3nS	působit
Česká	český	k2eAgFnSc1d1	Česká
provincie	provincie	k1gFnSc1	provincie
Tovaryšstva	tovaryšstvo	k1gNnSc2	tovaryšstvo
Ježíšova	Ježíšův	k2eAgFnSc1d1	Ježíšova
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zde	zde	k6eAd1	zde
provozuje	provozovat	k5eAaImIp3nS	provozovat
8	[number]	k4	8
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gMnSc7	její
provinciálem	provinciál	k1gMnSc7	provinciál
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
února	únor	k1gInSc2	únor
2013	[number]	k4	2013
Josef	Josef	k1gMnSc1	Josef
Stuchlý	Stuchlý	k2eAgMnSc1d1	Stuchlý
<g/>
.	.	kIx.	.
</s>
<s>
Tovaryšstvo	tovaryšstvo	k1gNnSc1	tovaryšstvo
Ježíšovo	Ježíšův	k2eAgNnSc1d1	Ježíšovo
je	být	k5eAaImIp3nS	být
mužský	mužský	k2eAgInSc4d1	mužský
řád	řád	k1gInSc4	řád
bez	bez	k7c2	bez
paralelní	paralelní	k2eAgFnSc2d1	paralelní
ženské	ženský	k2eAgFnSc2d1	ženská
větve	větev	k1gFnSc2	větev
<g/>
,	,	kIx,	,
určitou	určitý	k2eAgFnSc4d1	určitá
podobnost	podobnost	k1gFnSc4	podobnost
ale	ale	k8xC	ale
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaPmF	nalézt
u	u	k7c2	u
samostatných	samostatný	k2eAgFnPc2d1	samostatná
ženských	ženská	k1gFnPc2	ženská
řeholních	řeholní	k2eAgNnPc2d1	řeholní
společenství	společenství	k1gNnPc2	společenství
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
zejména	zejména	k9	zejména
o	o	k7c4	o
kongregace	kongregace	k1gFnPc4	kongregace
Anglických	anglický	k2eAgFnPc2d1	anglická
panen	panna	k1gFnPc2	panna
(	(	kIx(	(
<g/>
Congregacio	Congregacio	k1gNnSc1	Congregacio
Jesu	Jesus	k1gInSc2	Jesus
<g/>
,	,	kIx,	,
asi	asi	k9	asi
2	[number]	k4	2
700	[number]	k4	700
sester	sestra	k1gFnPc2	sestra
<g/>
)	)	kIx)	)
či	či	k8xC	či
sester	sestra	k1gFnPc2	sestra
Sacré-Coeur	Sacré-Coeura	k1gFnPc2	Sacré-Coeura
(	(	kIx(	(
<g/>
asi	asi	k9	asi
4	[number]	k4	4
200	[number]	k4	200
sester	sestra	k1gFnPc2	sestra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
působí	působit	k5eAaImIp3nP	působit
Anglické	anglický	k2eAgFnPc1d1	anglická
panny	panna	k1gFnPc1	panna
a	a	k8xC	a
také	také	k9	také
poměrně	poměrně	k6eAd1	poměrně
mladá	mladý	k2eAgFnSc1d1	mladá
Společnost	společnost	k1gFnSc1	společnost
sester	sestra	k1gFnPc2	sestra
Ježíšových	Ježíšův	k2eAgFnPc2d1	Ježíšova
s	s	k7c7	s
několika	několik	k4yIc7	několik
desítkami	desítka	k1gFnPc7	desítka
členek	členka	k1gFnPc2	členka
<g/>
.	.	kIx.	.
<g/>
Jedinou	jediný	k2eAgFnSc7d1	jediná
ženou	žena	k1gFnSc7	žena
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
do	do	k7c2	do
řádu	řád	k1gInSc2	řád
přijata	přijat	k2eAgFnSc1d1	přijata
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
jen	jen	k9	jen
do	do	k7c2	do
nižšího	nízký	k2eAgInSc2d2	nižší
stupně	stupeň	k1gInSc2	stupeň
bez	bez	k7c2	bez
složení	složení	k1gNnSc2	složení
řeholních	řeholní	k2eAgInPc2d1	řeholní
slibů	slib	k1gInPc2	slib
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
španělská	španělský	k2eAgFnSc1d1	španělská
infantka	infantka	k1gFnSc1	infantka
Jana	Jana	k1gFnSc1	Jana
Španělská	španělský	k2eAgFnSc1d1	španělská
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
císaře	císař	k1gMnSc2	císař
Karla	Karel	k1gMnSc2	Karel
V.	V.	kA	V.
a	a	k8xC	a
osobní	osobní	k2eAgFnSc1d1	osobní
přítelkyně	přítelkyně	k1gFnSc1	přítelkyně
Ignáce	Ignác	k1gMnSc2	Ignác
z	z	k7c2	z
Loyoly	Loyola	k1gFnSc2	Loyola
<g/>
;	;	kIx,	;
její	její	k3xOp3gFnSc4	její
příslušnost	příslušnost	k1gFnSc4	příslušnost
k	k	k7c3	k
řádu	řád	k1gInSc3	řád
však	však	k8xC	však
byla	být	k5eAaImAgFnS	být
chována	chovat	k5eAaImNgFnS	chovat
v	v	k7c6	v
přísném	přísný	k2eAgNnSc6d1	přísné
utajení	utajení	k1gNnSc6	utajení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vznik	vznik	k1gInSc1	vznik
řádu	řád	k1gInSc2	řád
==	==	k?	==
</s>
</p>
<p>
<s>
Zřízení	zřízení	k1gNnSc1	zřízení
řádu	řád	k1gInSc2	řád
bylo	být	k5eAaImAgNnS	být
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
reakcí	reakce	k1gFnPc2	reakce
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
na	na	k7c4	na
reformaci	reformace	k1gFnSc4	reformace
<g/>
.	.	kIx.	.
</s>
<s>
Tovaryšstvo	tovaryšstvo	k1gNnSc1	tovaryšstvo
Ježíšovo	Ježíšův	k2eAgNnSc1d1	Ježíšovo
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
15	[number]	k4	15
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1534	[number]	k4	1534
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
sedm	sedm	k4xCc1	sedm
studentů	student	k1gMnPc2	student
Université	Universitý	k2eAgFnSc2d1	Universitá
de	de	k?	de
Paris	Paris	k1gMnSc1	Paris
vedených	vedený	k2eAgFnPc2d1	vedená
Ignácem	Ignác	k1gMnSc7	Ignác
z	z	k7c2	z
Loyoly	Loyola	k1gFnSc2	Loyola
složilo	složit	k5eAaPmAgNnS	složit
vstupní	vstupní	k2eAgInPc4d1	vstupní
sliby	slib	k1gInPc4	slib
v	v	k7c6	v
kapli	kaple	k1gFnSc6	kaple
sv.	sv.	kA	sv.
Denise	Denisa	k1gFnSc6	Denisa
na	na	k7c6	na
Montmartru	Montmartr	k1gInSc6	Montmartr
<g/>
.	.	kIx.	.
</s>
<s>
Zakládajícími	zakládající	k2eAgMnPc7d1	zakládající
členy	člen	k1gMnPc7	člen
nové	nový	k2eAgFnSc2d1	nová
řeholní	řeholní	k2eAgFnSc2d1	řeholní
společnosti	společnost	k1gFnSc2	společnost
byli	být	k5eAaImAgMnP	být
vedle	vedle	k7c2	vedle
Ignáce	Ignác	k1gMnSc2	Ignác
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
prvním	první	k4xOgMnSc7	první
generálem	generál	k1gMnSc7	generál
řádu	řád	k1gInSc2	řád
<g/>
,	,	kIx,	,
Španělé	Španěl	k1gMnPc1	Španěl
František	František	k1gMnSc1	František
Xaverský	xaverský	k2eAgMnSc1d1	xaverský
<g/>
,	,	kIx,	,
Alfonso	Alfonso	k1gMnSc1	Alfonso
Salmeron	Salmeron	k1gMnSc1	Salmeron
<g/>
,	,	kIx,	,
Diego	Diego	k1gMnSc1	Diego
Laínez	Laínez	k1gMnSc1	Laínez
a	a	k8xC	a
Nicolás	Nicolás	k1gInSc1	Nicolás
de	de	k?	de
Bobadilla	Bobadilla	k1gMnSc1	Bobadilla
<g/>
,	,	kIx,	,
Savojčan	Savojčan	k1gMnSc1	Savojčan
Petr	Petr	k1gMnSc1	Petr
Faber	Faber	k1gMnSc1	Faber
a	a	k8xC	a
Portugalec	Portugalec	k1gMnSc1	Portugalec
Simã	Simã	k1gMnSc1	Simã
Rodrigues	Rodrigues	k1gMnSc1	Rodrigues
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
řádu	řád	k1gInSc2	řád
byl	být	k5eAaImAgInS	být
27	[number]	k4	27
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
roku	rok	k1gInSc2	rok
1540	[number]	k4	1540
stvrzen	stvrzen	k2eAgInSc1d1	stvrzen
bulou	bula	k1gFnSc7	bula
Regimini	Regimin	k2eAgMnPc1d1	Regimin
militantis	militantis	k1gFnPc4	militantis
ecclesiae	ecclesia	k1gMnSc2	ecclesia
papeže	papež	k1gMnSc2	papež
Pavla	Pavel	k1gMnSc2	Pavel
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
činností	činnost	k1gFnSc7	činnost
řádu	řád	k1gInSc2	řád
byla	být	k5eAaImAgFnS	být
misijní	misijní	k2eAgFnSc1d1	misijní
<g/>
,	,	kIx,	,
vzdělávací	vzdělávací	k2eAgFnSc1d1	vzdělávací
a	a	k8xC	a
vědecká	vědecký	k2eAgFnSc1d1	vědecká
činnost	činnost	k1gFnSc1	činnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zrušení	zrušení	k1gNnSc1	zrušení
řádu	řád	k1gInSc2	řád
roku	rok	k1gInSc2	rok
1773	[number]	k4	1773
==	==	k?	==
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
vliv	vliv	k1gInSc1	vliv
jezuitů	jezuita	k1gMnPc2	jezuita
začal	začít	k5eAaPmAgInS	začít
omezovat	omezovat	k5eAaImF	omezovat
a	a	k8xC	a
roku	rok	k1gInSc3	rok
1773	[number]	k4	1773
byl	být	k5eAaImAgInS	být
řád	řád	k1gInSc1	řád
na	na	k7c4	na
nátlak	nátlak	k1gInSc4	nátlak
evropských	evropský	k2eAgMnPc2d1	evropský
panovníků	panovník	k1gMnPc2	panovník
papežem	papež	k1gMnSc7	papež
Klementem	Klement	k1gMnSc7	Klement
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
zrušen	zrušen	k2eAgInSc4d1	zrušen
(	(	kIx(	(
<g/>
část	část	k1gFnSc1	část
ho	on	k3xPp3gMnSc4	on
ale	ale	k9	ale
přetrvala	přetrvat	k5eAaPmAgFnS	přetrvat
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Úvod	úvod	k1gInSc1	úvod
===	===	k?	===
</s>
</p>
<p>
<s>
Zrušení	zrušení	k1gNnSc1	zrušení
jezuitů	jezuita	k1gMnPc2	jezuita
v	v	k7c6	v
Portugalsku	Portugalsko	k1gNnSc6	Portugalsko
<g/>
,	,	kIx,	,
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
Obojí	obojí	k4xRgFnSc4	obojí
Sicílii	Sicílie	k1gFnSc4	Sicílie
<g/>
,	,	kIx,	,
Parmě	Parma	k1gFnSc6	Parma
a	a	k8xC	a
ve	v	k7c6	v
Španělském	španělský	k2eAgNnSc6d1	španělské
impériu	impérium	k1gNnSc6	impérium
roku	rok	k1gInSc2	rok
1767	[number]	k4	1767
bylo	být	k5eAaImAgNnS	být
výsledkem	výsledek	k1gInSc7	výsledek
mnoha	mnoho	k4c2	mnoho
politických	politický	k2eAgInPc2d1	politický
tahů	tah	k1gInPc2	tah
spíše	spíše	k9	spíše
nežli	nežli	k8xS	nežli
teologických	teologický	k2eAgInPc2d1	teologický
sporů	spor	k1gInPc2	spor
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
breve	breve	k1gNnSc2	breve
Dominus	Dominus	k1gMnSc1	Dominus
ac	ac	k?	ac
Redemptor	Redemptor	k1gMnSc1	Redemptor
z	z	k7c2	z
21	[number]	k4	21
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1773	[number]	k4	1773
papeže	papež	k1gMnSc2	papež
Klementa	Klement	k1gMnSc2	Klement
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
bylo	být	k5eAaImAgNnS	být
Tovaryšstvo	tovaryšstvo	k1gNnSc1	tovaryšstvo
Ježíšovo	Ježíšův	k2eAgNnSc1d1	Ježíšovo
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
v	v	k7c6	v
nekatolických	katolický	k2eNgFnPc6d1	nekatolická
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
Rusku	Rusko	k1gNnSc6	Rusko
a	a	k8xC	a
Prusku	Prusko	k1gNnSc6	Prusko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
autorita	autorita	k1gFnSc1	autorita
papeže	papež	k1gMnSc2	papež
nebyla	být	k5eNaImAgFnS	být
uznávána	uznávat	k5eAaImNgFnS	uznávat
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc1	tento
příkaz	příkaz	k1gInSc1	příkaz
ignorován	ignorován	k2eAgInSc1d1	ignorován
<g/>
.	.	kIx.	.
</s>
<s>
Centrum	centrum	k1gNnSc1	centrum
jezuitského	jezuitský	k2eAgNnSc2d1	jezuitské
školství	školství	k1gNnSc2	školství
se	se	k3xPyFc4	se
přesunulo	přesunout	k5eAaPmAgNnS	přesunout
z	z	k7c2	z
Antverp	Antverpy	k1gFnPc2	Antverpy
do	do	k7c2	do
Bruselu	Brusel	k1gInSc2	Brusel
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
činnosti	činnost	k1gFnSc6	činnost
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1788	[number]	k4	1788
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
jejich	jejich	k3xOp3gFnSc1	jejich
činnost	činnost	k1gFnSc1	činnost
také	také	k9	také
zakázána	zakázat	k5eAaPmNgFnS	zakázat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Přehled	přehled	k1gInSc4	přehled
===	===	k?	===
</s>
</p>
<p>
<s>
Řada	řada	k1gFnSc1	řada
politických	politický	k2eAgNnPc2d1	politické
střetnutí	střetnutí	k1gNnPc2	střetnutí
mezi	mezi	k7c7	mezi
různými	různý	k2eAgFnPc7d1	různá
monarchiemi	monarchie	k1gFnPc7	monarchie
<g/>
,	,	kIx,	,
obzvláště	obzvláště	k6eAd1	obzvláště
Francií	Francie	k1gFnSc7	Francie
a	a	k8xC	a
Portugalskem	Portugalsko	k1gNnSc7	Portugalsko
<g/>
,	,	kIx,	,
začala	začít	k5eAaPmAgFnS	začít
územními	územní	k2eAgFnPc7d1	územní
různicemi	různice	k1gFnPc7	různice
roku	rok	k1gInSc2	rok
1750	[number]	k4	1750
a	a	k8xC	a
vyvrcholila	vyvrcholit	k5eAaPmAgFnS	vyvrcholit
přerušením	přerušení	k1gNnSc7	přerušení
diplomatických	diplomatický	k2eAgInPc2d1	diplomatický
styků	styk	k1gInPc2	styk
<g/>
,	,	kIx,	,
rozpuštěním	rozpuštění	k1gNnSc7	rozpuštění
Tovaryšstva	tovaryšstvo	k1gNnSc2	tovaryšstvo
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
i	i	k9	i
několika	několik	k4yIc7	několik
popravami	poprava	k1gFnPc7	poprava
<g/>
.	.	kIx.	.
</s>
<s>
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
Obojí	obojí	k4xRgFnSc1	obojí
Sicílie	Sicílie	k1gFnSc1	Sicílie
<g/>
,	,	kIx,	,
Parma	Parma	k1gFnSc1	Parma
i	i	k9	i
Španělsko	Španělsko	k1gNnSc4	Španělsko
byly	být	k5eAaImAgInP	být
zapleteny	zapleten	k2eAgInPc1d1	zapleten
do	do	k7c2	do
alespoň	alespoň	k9	alespoň
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
záležitostí	záležitost	k1gFnPc2	záležitost
<g/>
.	.	kIx.	.
</s>
<s>
Konflikt	konflikt	k1gInSc1	konflikt
vypukl	vypuknout	k5eAaPmAgInS	vypuknout
díky	díky	k7c3	díky
obchodním	obchodní	k2eAgInPc3d1	obchodní
sporům	spor	k1gInPc3	spor
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1750	[number]	k4	1750
v	v	k7c6	v
Portugalsku	Portugalsko	k1gNnSc6	Portugalsko
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1755	[number]	k4	1755
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c6	na
sklonku	sklonek	k1gInSc6	sklonek
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
na	na	k7c6	na
Obou	dva	k4xCgFnPc6	dva
Sicíliích	Sicílie	k1gFnPc6	Sicílie
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1758	[number]	k4	1758
využila	využít	k5eAaPmAgFnS	využít
vláda	vláda	k1gFnSc1	vláda
Josefa	Josef	k1gMnSc4	Josef
I.	I.	kA	I.
Portugalského	portugalský	k2eAgMnSc4d1	portugalský
ubývajících	ubývající	k2eAgFnPc2d1	ubývající
sil	síla	k1gFnPc2	síla
papeže	papež	k1gMnSc4	papež
Benedikta	Benedikt	k1gMnSc2	Benedikt
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
a	a	k8xC	a
deportovala	deportovat	k5eAaBmAgFnS	deportovat
jezuity	jezuita	k1gMnPc4	jezuita
z	z	k7c2	z
Ameriky	Amerika	k1gFnSc2	Amerika
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
vykázala	vykázat	k5eAaPmAgFnS	vykázat
jezuity	jezuita	k1gMnPc4	jezuita
i	i	k9	i
jejich	jejich	k3xOp3gMnPc4	jejich
domorodé	domorodý	k2eAgMnPc4d1	domorodý
spolupracovníky	spolupracovník	k1gMnPc4	spolupracovník
z	z	k7c2	z
misií	misie	k1gFnPc2	misie
<g/>
.	.	kIx.	.
</s>
<s>
Formálně	formálně	k6eAd1	formálně
řád	řád	k1gInSc4	řád
zakázala	zakázat	k5eAaPmAgFnS	zakázat
roku	rok	k1gInSc2	rok
1759	[number]	k4	1759
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1762	[number]	k4	1762
schválil	schválit	k5eAaPmAgInS	schválit
pod	pod	k7c7	pod
tlakem	tlak	k1gInSc7	tlak
mnoha	mnoho	k4c2	mnoho
skupin	skupina	k1gFnPc2	skupina
–	–	k?	–
ať	ať	k8xS	ať
již	již	k6eAd1	již
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
církve	církev	k1gFnSc2	církev
či	či	k8xC	či
od	od	k7c2	od
světských	světský	k2eAgMnPc2d1	světský
myslitelů	myslitel	k1gMnPc2	myslitel
po	po	k7c4	po
královu	králův	k2eAgFnSc4d1	králova
milenku	milenka	k1gFnSc4	milenka
–	–	k?	–
francouzský	francouzský	k2eAgInSc1d1	francouzský
parlament	parlament	k1gInSc1	parlament
zákon	zákon	k1gInSc1	zákon
proti	proti	k7c3	proti
Tovaryšstvu	tovaryšstvo	k1gNnSc3	tovaryšstvo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Neapoli	Neapol	k1gFnSc6	Neapol
a	a	k8xC	a
na	na	k7c6	na
Sicílii	Sicílie	k1gFnSc6	Sicílie
byl	být	k5eAaImAgInS	být
řád	řád	k1gInSc1	řád
zakázán	zakázán	k2eAgInSc1d1	zakázán
výnosem	výnos	k1gInSc7	výnos
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1767	[number]	k4	1767
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
roku	rok	k1gInSc2	rok
1773	[number]	k4	1773
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
antiklerikální	antiklerikální	k2eAgInPc4d1	antiklerikální
výstřelky	výstřelek	k1gInPc4	výstřelek
Francouzské	francouzský	k2eAgFnSc2d1	francouzská
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1815	[number]	k4	1815
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
církvi	církev	k1gFnSc3	církev
opět	opět	k5eAaPmF	opět
přisouzena	přisouzen	k2eAgFnSc1d1	přisouzena
vítanější	vítaný	k2eAgFnSc1d2	vítanější
role	role	k1gFnSc1	role
v	v	k7c6	v
oficiálním	oficiální	k2eAgInSc6d1	oficiální
evropském	evropský	k2eAgInSc6d1	evropský
životě	život	k1gInSc6	život
<g/>
,	,	kIx,	,
a	a	k8xC	a
národ	národ	k1gInSc1	národ
za	za	k7c7	za
národem	národ	k1gInSc7	národ
začal	začít	k5eAaPmAgInS	začít
obnovovat	obnovovat	k5eAaImF	obnovovat
jezuity	jezuita	k1gMnPc4	jezuita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Moderní	moderní	k2eAgInSc1d1	moderní
pohled	pohled	k1gInSc1	pohled
na	na	k7c6	na
zrušení	zrušení	k1gNnSc6	zrušení
řádu	řád	k1gInSc2	řád
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
šlo	jít	k5eAaImAgNnS	jít
spíše	spíše	k9	spíše
o	o	k7c4	o
výsledek	výsledek	k1gInSc4	výsledek
řady	řada	k1gFnSc2	řada
politických	politický	k2eAgInPc2d1	politický
a	a	k8xC	a
ekonomických	ekonomický	k2eAgInPc2d1	ekonomický
konfliktů	konflikt	k1gInPc2	konflikt
a	a	k8xC	a
prosazení	prosazení	k1gNnSc4	prosazení
nezávislosti	nezávislost	k1gFnSc2	nezávislost
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
zemí	zem	k1gFnPc2	zem
na	na	k7c6	na
katolické	katolický	k2eAgFnSc6d1	katolická
církvi	církev	k1gFnSc6	církev
než	než	k8xS	než
o	o	k7c4	o
teologické	teologický	k2eAgInPc4d1	teologický
rozpory	rozpor	k1gInPc4	rozpor
<g/>
.	.	kIx.	.
</s>
<s>
Vyhnání	vyhnání	k1gNnSc1	vyhnání
jezuitů	jezuita	k1gMnPc2	jezuita
z	z	k7c2	z
území	území	k1gNnSc2	území
římskokatolických	římskokatolický	k2eAgInPc2d1	římskokatolický
národů	národ	k1gInPc2	národ
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc2	jejich
koloniálních	koloniální	k2eAgFnPc2d1	koloniální
říší	říš	k1gFnPc2	říš
je	být	k5eAaImIp3nS	být
také	také	k9	také
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
první	první	k4xOgInSc4	první
"	"	kIx"	"
<g/>
triumf	triumf	k1gInSc4	triumf
<g/>
"	"	kIx"	"
světských	světský	k2eAgFnPc2d1	světská
představ	představa	k1gFnPc2	představa
osvícenců	osvícenec	k1gMnPc2	osvícenec
<g/>
,	,	kIx,	,
poněkud	poněkud	k6eAd1	poněkud
poplatné	poplatný	k2eAgFnPc1d1	poplatná
antiklericismu	antiklericismus	k1gInSc6	antiklericismus
Francouzské	francouzský	k2eAgFnSc2d1	francouzská
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Potlačení	potlačení	k1gNnSc1	potlačení
je	být	k5eAaImIp3nS	být
také	také	k9	také
často	často	k6eAd1	často
vnímáno	vnímat	k5eAaImNgNnS	vnímat
jako	jako	k8xC	jako
pokus	pokus	k1gInSc1	pokus
monarchů	monarcha	k1gMnPc2	monarcha
o	o	k7c4	o
získání	získání	k1gNnSc4	získání
kontroly	kontrola	k1gFnSc2	kontrola
nad	nad	k7c7	nad
příjmy	příjem	k1gInPc7	příjem
a	a	k8xC	a
obchodem	obchod	k1gInSc7	obchod
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
převážně	převážně	k6eAd1	převážně
ovládané	ovládaný	k2eAgNnSc1d1	ovládané
Tovaryšstvem	tovaryšstvo	k1gNnSc7	tovaryšstvo
<g/>
.	.	kIx.	.
</s>
<s>
Katoličtí	katolický	k2eAgMnPc1d1	katolický
historici	historik	k1gMnPc1	historik
často	často	k6eAd1	často
poukazují	poukazovat	k5eAaImIp3nP	poukazovat
na	na	k7c4	na
osobní	osobní	k2eAgInSc4d1	osobní
konflikt	konflikt	k1gInSc4	konflikt
mezi	mezi	k7c7	mezi
papežem	papež	k1gMnSc7	papež
Klementem	Klement	k1gMnSc7	Klement
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1758	[number]	k4	1758
<g/>
–	–	k?	–
<g/>
1769	[number]	k4	1769
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
příznivci	příznivec	k1gMnPc1	příznivec
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
jedné	jeden	k4xCgFnSc6	jeden
<g/>
,	,	kIx,	,
s	s	k7c7	s
"	"	kIx"	"
<g/>
korunním	korunní	k2eAgMnSc7d1	korunní
<g/>
"	"	kIx"	"
kardinálem	kardinál	k1gMnSc7	kardinál
podporovaným	podporovaný	k2eAgInSc7d1	podporovaný
Francií	Francie	k1gFnSc7	Francie
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
druhé	druhý	k4xOgFnSc6	druhý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
===	===	k?	===
</s>
</p>
<p>
<s>
Vyhnání	vyhnání	k1gNnSc1	vyhnání
jezuitů	jezuita	k1gMnPc2	jezuita
z	z	k7c2	z
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
bylo	být	k5eAaImAgNnS	být
omezeno	omezit	k5eAaPmNgNnS	omezit
na	na	k7c4	na
osobní	osobní	k2eAgInPc4d1	osobní
roztržky	roztržek	k1gInPc4	roztržek
s	s	k7c7	s
prvním	první	k4xOgMnSc7	první
ministrem	ministr	k1gMnSc7	ministr
Josefa	Josef	k1gMnSc4	Josef
I.	I.	kA	I.
Portugalského	portugalský	k2eAgMnSc4d1	portugalský
<g/>
,	,	kIx,	,
markýzem	markýz	k1gMnSc7	markýz
de	de	k?	de
Pombal	Pombal	k1gInSc1	Pombal
<g/>
.	.	kIx.	.
</s>
<s>
Ať	ať	k9	ať
již	již	k6eAd1	již
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
Pombala	Pombal	k1gMnSc4	Pombal
či	či	k8xC	či
Portugalsko	Portugalsko	k1gNnSc4	Portugalsko
všeobecně	všeobecně	k6eAd1	všeobecně
<g/>
,	,	kIx,	,
konflikt	konflikt	k1gInSc1	konflikt
s	s	k7c7	s
jezuity	jezuita	k1gMnPc7	jezuita
započal	započnout	k5eAaPmAgMnS	započnout
díky	díky	k7c3	díky
výměně	výměna	k1gFnSc3	výměna
portugalského	portugalský	k2eAgNnSc2d1	portugalské
koloniálního	koloniální	k2eAgNnSc2d1	koloniální
teritoria	teritorium	k1gNnSc2	teritorium
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
se	s	k7c7	s
Španělskem	Španělsko	k1gNnSc7	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tajného	tajný	k2eAgNnSc2d1	tajné
ujednání	ujednání	k1gNnSc2	ujednání
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1750	[number]	k4	1750
přenechalo	přenechat	k5eAaPmAgNnS	přenechat
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
Španělsku	Španělsko	k1gNnSc6	Španělsko
sporné	sporný	k2eAgNnSc1d1	sporné
území	území	k1gNnSc1	území
kolonie	kolonie	k1gFnSc2	kolonie
San	San	k1gFnSc2	San
Sacramento	Sacramento	k1gNnSc1	Sacramento
v	v	k7c6	v
ústí	ústí	k1gNnSc6	ústí
řeky	řeka	k1gFnSc2	řeka
Uruguay	Uruguay	k1gFnSc1	Uruguay
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c4	za
"	"	kIx"	"
<g/>
Seven	Seven	k2eAgInSc1d1	Seven
Reductions	Reductions	k1gInSc1	Reductions
of	of	k?	of
Paraguay	Paraguay	k1gFnSc1	Paraguay
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
samosprávné	samosprávný	k2eAgFnSc2d1	samosprávná
jezuitské	jezuitský	k2eAgFnSc2d1	jezuitská
misie	misie	k1gFnSc2	misie
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
byly	být	k5eAaImAgFnP	být
formálně	formálně	k6eAd1	formálně
španělskou	španělský	k2eAgFnSc7d1	španělská
kolonií	kolonie	k1gFnSc7	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
Domorodým	domorodý	k2eAgInSc7d1	domorodý
Guarani	Guaran	k1gMnPc1	Guaran
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
žili	žít	k5eAaImAgMnP	žít
na	na	k7c6	na
území	území	k1gNnSc6	území
misií	misie	k1gFnPc2	misie
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
přikázáno	přikázán	k2eAgNnSc1d1	přikázáno
opustit	opustit	k5eAaPmF	opustit
svou	svůj	k3xOyFgFnSc4	svůj
zem	zem	k1gFnSc4	zem
a	a	k8xC	a
usídlit	usídlit	k5eAaPmF	usídlit
se	se	k3xPyFc4	se
za	za	k7c7	za
řekou	řeka	k1gFnSc7	řeka
Uruguay	Uruguay	k1gFnSc1	Uruguay
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
krutým	krutý	k2eAgFnPc3d1	krutá
podmínkám	podmínka	k1gFnPc3	podmínka
indiáni	indián	k1gMnPc1	indián
pozvedli	pozvednout	k5eAaPmAgMnP	pozvednout
své	svůj	k3xOyFgFnPc4	svůj
zbraně	zbraň	k1gFnPc4	zbraň
proti	proti	k7c3	proti
kolonialistům	kolonialista	k1gMnPc3	kolonialista
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
propukla	propuknout	k5eAaPmAgFnS	propuknout
takzvaná	takzvaný	k2eAgFnSc1d1	takzvaná
Válka	válka	k1gFnSc1	válka
Guarani	Guaraň	k1gFnSc3	Guaraň
<g/>
;	;	kIx,	;
pro	pro	k7c4	pro
Indiány	Indián	k1gMnPc7	Indián
skončila	skončit	k5eAaPmAgFnS	skončit
naprostou	naprostý	k2eAgFnSc7d1	naprostá
katastrofou	katastrofa	k1gFnSc7	katastrofa
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
tím	ten	k3xDgNnSc7	ten
byli	být	k5eAaImAgMnP	být
vinni	vinen	k2eAgMnPc1d1	vinen
jezuité	jezuita	k1gMnPc1	jezuita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Portugalsku	Portugalsko	k1gNnSc6	Portugalsko
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
doslova	doslova	k6eAd1	doslova
válka	válka	k1gFnSc1	válka
buřičských	buřičský	k2eAgInPc2d1	buřičský
letáků	leták	k1gInPc2	leták
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
byli	být	k5eAaImAgMnP	být
jezuité	jezuita	k1gMnPc1	jezuita
bráněni	bráněn	k2eAgMnPc1d1	bráněn
i	i	k8xC	i
haněni	haněn	k2eAgMnPc1d1	haněn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Představeným	představená	k1gFnPc3	představená
jezuitů	jezuita	k1gMnPc2	jezuita
<g/>
,	,	kIx,	,
nařčeným	nařčený	k2eAgInSc7d1	nařčený
z	z	k7c2	z
pokusu	pokus	k1gInSc2	pokus
o	o	k7c4	o
vybudování	vybudování	k1gNnSc4	vybudování
nezávislé	závislý	k2eNgFnSc2d1	nezávislá
říše	říš	k1gFnSc2	říš
v	v	k7c6	v
Novém	nový	k2eAgInSc6d1	nový
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
zapovězeno	zapovědět	k5eAaPmNgNnS	zapovědět
pokračovat	pokračovat	k5eAaImF	pokračovat
v	v	k7c6	v
územní	územní	k2eAgFnSc6d1	územní
správě	správa	k1gFnSc6	správa
svých	svůj	k3xOyFgFnPc2	svůj
bývalých	bývalý	k2eAgFnPc2d1	bývalá
misií	misie	k1gFnPc2	misie
<g/>
,	,	kIx,	,
a	a	k8xC	a
portugalští	portugalský	k2eAgMnPc1d1	portugalský
jezuité	jezuita	k1gMnPc1	jezuita
byli	být	k5eAaImAgMnP	být
posláni	poslat	k5eAaPmNgMnP	poslat
pryč	pryč	k6eAd1	pryč
od	od	k7c2	od
královského	královský	k2eAgInSc2d1	královský
dvora	dvůr	k1gInSc2	dvůr
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1758	[number]	k4	1758
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
získat	získat	k5eAaPmF	získat
od	od	k7c2	od
letitého	letitý	k2eAgMnSc2d1	letitý
papeže	papež	k1gMnSc2	papež
Benedikta	Benedikt	k1gMnSc2	Benedikt
XIV	XIV	kA	XIV
dokument	dokument	k1gInSc1	dokument
<g/>
,	,	kIx,	,
pověřující	pověřující	k2eAgInSc1d1	pověřující
portugalského	portugalský	k2eAgMnSc2d1	portugalský
kardinála	kardinál	k1gMnSc2	kardinál
Saldanha	Saldanh	k1gMnSc2	Saldanh
<g/>
,	,	kIx,	,
doporučeného	doporučený	k2eAgInSc2d1	doporučený
markýzem	markýz	k1gMnSc7	markýz
de	de	k?	de
Pombal	Pombal	k1gInSc1	Pombal
<g/>
,	,	kIx,	,
vyšetřením	vyšetření	k1gNnSc7	vyšetření
obvinění	obvinění	k1gNnSc2	obvinění
vzneseným	vznesený	k2eAgInPc3d1	vznesený
proti	proti	k7c3	proti
jezuitům	jezuita	k1gMnPc3	jezuita
ve	v	k7c6	v
jménu	jméno	k1gNnSc6	jméno
Portugalského	portugalský	k2eAgMnSc2d1	portugalský
krále	král	k1gMnSc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Benedikt	benedikt	k1gInSc1	benedikt
byl	být	k5eAaImAgInS	být
skeptický	skeptický	k2eAgMnSc1d1	skeptický
k	k	k7c3	k
závažnosti	závažnost	k1gFnSc3	závažnost
údajných	údajný	k2eAgInPc2d1	údajný
zločinů	zločin	k1gInPc2	zločin
<g/>
.	.	kIx.	.
</s>
<s>
Nařídil	nařídit	k5eAaPmAgMnS	nařídit
rychlé	rychlý	k2eAgNnSc4d1	rychlé
vyšetření	vyšetření	k1gNnSc4	vyšetření
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
ochráněno	ochráněn	k2eAgNnSc1d1	ochráněno
dobré	dobrý	k2eAgNnSc1d1	dobré
jméno	jméno	k1gNnSc1	jméno
Tovaryšstva	tovaryšstvo	k1gNnSc2	tovaryšstvo
<g/>
,	,	kIx,	,
a	a	k8xC	a
o	o	k7c6	o
všech	všecek	k3xTgFnPc6	všecek
záležitostech	záležitost	k1gFnPc6	záležitost
si	se	k3xPyFc3	se
přál	přát	k5eAaImAgMnS	přát
být	být	k5eAaImF	být
osobně	osobně	k6eAd1	osobně
informován	informovat	k5eAaBmNgMnS	informovat
<g/>
.	.	kIx.	.
</s>
<s>
Uplynul	uplynout	k5eAaPmAgMnS	uplynout
však	však	k9	však
pouhý	pouhý	k2eAgInSc4d1	pouhý
měsíc	měsíc	k1gInSc4	měsíc
<g/>
,	,	kIx,	,
když	když	k8xS	když
3	[number]	k4	3
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
stařičký	stařičký	k2eAgMnSc1d1	stařičký
papež	papež	k1gMnSc1	papež
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
15	[number]	k4	15
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
kardinál	kardinál	k1gMnSc1	kardinál
Saldanha	Saldanha	k1gMnSc1	Saldanha
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
obdržel	obdržet	k5eAaPmAgMnS	obdržet
papežův	papežův	k2eAgInSc4d1	papežův
dokument	dokument	k1gInSc4	dokument
jen	jen	k9	jen
před	před	k7c7	před
čtrnácti	čtrnáct	k4xCc7	čtrnáct
dny	den	k1gInPc7	den
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
opomněl	opomnět	k5eAaPmAgMnS	opomnět
<g/>
"	"	kIx"	"
řádně	řádně	k6eAd1	řádně
navštívit	navštívit	k5eAaPmF	navštívit
jezuitské	jezuitský	k2eAgInPc4d1	jezuitský
domy	dům	k1gInPc4	dům
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
přikázáno	přikázat	k5eAaPmNgNnS	přikázat
<g/>
,	,	kIx,	,
a	a	k8xC	a
prohlašoval	prohlašovat	k5eAaImAgMnS	prohlašovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jezuité	jezuita	k1gMnPc1	jezuita
jsou	být	k5eAaImIp3nP	být
špatní	špatný	k2eAgMnPc1d1	špatný
a	a	k8xC	a
provozují	provozovat	k5eAaImIp3nP	provozovat
nezákonné	zákonný	k2eNgInPc4d1	nezákonný
<g/>
,	,	kIx,	,
veřejné	veřejný	k2eAgInPc4d1	veřejný
a	a	k8xC	a
skandální	skandální	k2eAgInPc4d1	skandální
obchody	obchod	k1gInPc4	obchod
jak	jak	k8xS	jak
v	v	k7c6	v
Portugalsku	Portugalsko	k1gNnSc6	Portugalsko
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
v	v	k7c6	v
jeho	jeho	k3xOp3gFnPc6	jeho
koloniích	kolonie	k1gFnPc6	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
sede	sed	k1gInSc5	sed
vacante	vacant	k1gMnSc5	vacant
(	(	kIx(	(
<g/>
období	období	k1gNnSc4	období
mezi	mezi	k7c7	mezi
úmrtím	úmrtí	k1gNnSc7	úmrtí
papeže	papež	k1gMnSc2	papež
a	a	k8xC	a
zvolením	zvolení	k1gNnSc7	zvolení
nového	nový	k2eAgMnSc2d1	nový
<g/>
)	)	kIx)	)
Pombal	Pombal	k1gInSc1	Pombal
rychle	rychle	k6eAd1	rychle
jednal	jednat	k5eAaImAgInS	jednat
<g/>
;	;	kIx,	;
během	během	k7c2	během
tří	tři	k4xCgInPc2	tři
týdnů	týden	k1gInPc2	týden
byl	být	k5eAaImAgInS	být
jezuitům	jezuita	k1gMnPc3	jezuita
zabaven	zabavit	k5eAaPmNgInS	zabavit
veškerý	veškerý	k3xTgInSc4	veškerý
majetek	majetek	k1gInSc4	majetek
v	v	k7c6	v
Portugalsku	Portugalsko	k1gNnSc6	Portugalsko
<g/>
,	,	kIx,	,
a	a	k8xC	a
než	než	k8xS	než
se	se	k3xPyFc4	se
kardinál	kardinál	k1gMnSc1	kardinál
Rezzonico	Rezzonico	k6eAd1	Rezzonico
stal	stát	k5eAaPmAgMnS	stát
papežem	papež	k1gMnSc7	papež
Klementem	Klement	k1gMnSc7	Klement
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1758	[number]	k4	1758
<g/>
,	,	kIx,	,
vyvlastnění	vyvlastnění	k1gNnSc1	vyvlastnění
v	v	k7c6	v
Portugalsku	Portugalsko	k1gNnSc6	Portugalsko
bylo	být	k5eAaImAgNnS	být
dokončeno	dokončit	k5eAaPmNgNnS	dokončit
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc7d1	poslední
kapkou	kapka	k1gFnSc7	kapka
pro	pro	k7c4	pro
Portugalský	portugalský	k2eAgInSc4d1	portugalský
královský	královský	k2eAgInSc4d1	královský
dvůr	dvůr	k1gInSc4	dvůr
byl	být	k5eAaImAgInS	být
pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
vraždu	vražda	k1gFnSc4	vražda
krále	král	k1gMnSc2	král
Josefa	Josef	k1gMnSc2	Josef
I.	I.	kA	I.
3	[number]	k4	3
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
<g/>
,	,	kIx,	,
o	o	k7c6	o
níž	jenž	k3xRgFnSc6	jenž
prý	prý	k9	prý
jezuité	jezuita	k1gMnPc1	jezuita
předem	předem	k6eAd1	předem
věděli	vědět	k5eAaImAgMnP	vědět
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
zatčení	zatčení	k1gNnSc3	zatčení
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
upálení	upálení	k1gNnSc4	upálení
Gabriela	Gabriela	k1gFnSc1	Gabriela
Malagridy	Malagrid	k1gInPc4	Malagrid
<g/>
,	,	kIx,	,
jezuitského	jezuitský	k2eAgMnSc4d1	jezuitský
zpovědníka	zpovědník	k1gMnSc4	zpovědník
Leonora	Leonora	k1gFnSc1	Leonora
Távory	Távor	k1gInPc4	Távor
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgMnSc1	tento
šlechtic	šlechtic	k1gMnSc1	šlechtic
byl	být	k5eAaImAgMnS	být
obviněn	obvinit	k5eAaPmNgMnS	obvinit
z	z	k7c2	z
vraždy	vražda	k1gFnSc2	vražda
a	a	k8xC	a
uvězněn	uvězněn	k2eAgMnSc1d1	uvězněn
i	i	k9	i
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
rodinou	rodina	k1gFnSc7	rodina
<g/>
;	;	kIx,	;
jejich	jejich	k3xOp3gNnSc1	jejich
jmění	jmění	k1gNnSc1	jmění
bylo	být	k5eAaImAgNnS	být
zabaveno	zabavit	k5eAaPmNgNnS	zabavit
<g/>
,	,	kIx,	,
rodinný	rodinný	k2eAgInSc1d1	rodinný
palác	palác	k1gInSc1	palác
zničen	zničit	k5eAaPmNgInS	zničit
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc1	jeho
základy	základ	k1gInPc1	základ
zasoleny	zasolen	k2eAgInPc1d1	zasolen
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
mužští	mužský	k2eAgMnPc1d1	mužský
členové	člen	k1gMnPc1	člen
rodu	rod	k1gInSc2	rod
byli	být	k5eAaImAgMnP	být
popraveni	popravit	k5eAaPmNgMnP	popravit
a	a	k8xC	a
jen	jen	k6eAd1	jen
díky	díky	k7c3	díky
přímluvě	přímluva	k1gFnSc3	přímluva
královny	královna	k1gFnSc2	královna
Marianny	Marianna	k1gFnSc2	Marianna
a	a	k8xC	a
Marie	Maria	k1gFnSc2	Maria
Františky	Františka	k1gFnSc2	Františka
<g/>
,	,	kIx,	,
dědičky	dědička	k1gFnSc2	dědička
trůnu	trůn	k1gInSc2	trůn
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
stejného	stejný	k2eAgInSc2d1	stejný
osudu	osud	k1gInSc2	osud
ušetřeny	ušetřen	k2eAgFnPc1d1	ušetřena
alespoň	alespoň	k9	alespoň
ženy	žena	k1gFnPc1	žena
i	i	k8xC	i
děti	dítě	k1gFnPc1	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
této	tento	k3xDgFnSc2	tento
rodiny	rodina	k1gFnSc2	rodina
bylo	být	k5eAaImAgNnS	být
vymazáno	vymazat	k5eAaPmNgNnS	vymazat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jezuité	jezuita	k1gMnPc1	jezuita
byli	být	k5eAaImAgMnP	být
vyhnáni	vyhnat	k5eAaPmNgMnP	vyhnat
z	z	k7c2	z
království	království	k1gNnSc2	království
a	a	k8xC	a
někteří	některý	k3yIgMnPc1	některý
významní	významný	k2eAgMnPc1d1	významný
představitelé	představitel	k1gMnPc1	představitel
řádu	řád	k1gInSc2	řád
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
nebyli	být	k5eNaImAgMnP	být
portugalské	portugalský	k2eAgFnPc4d1	portugalská
národnosti	národnost	k1gFnPc4	národnost
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
uvězněni	uvěznit	k5eAaPmNgMnP	uvěznit
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1759	[number]	k4	1759
byl	být	k5eAaImAgInS	být
řád	řád	k1gInSc1	řád
postaven	postavit	k5eAaPmNgInS	postavit
mimo	mimo	k7c4	mimo
zákon	zákon	k1gInSc4	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Portugalský	portugalský	k2eAgMnSc1d1	portugalský
vyslanec	vyslanec	k1gMnSc1	vyslanec
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
byl	být	k5eAaImAgMnS	být
povolán	povolat	k5eAaPmNgMnS	povolat
zpět	zpět	k6eAd1	zpět
a	a	k8xC	a
papežský	papežský	k2eAgMnSc1d1	papežský
nuncius	nuncius	k1gMnSc1	nuncius
poslán	poslán	k2eAgMnSc1d1	poslán
domů	domů	k6eAd1	domů
s	s	k7c7	s
ostudou	ostuda	k1gFnSc7	ostuda
<g/>
.	.	kIx.	.
</s>
<s>
Vztahy	vztah	k1gInPc1	vztah
mezi	mezi	k7c7	mezi
Římem	Řím	k1gInSc7	Řím
a	a	k8xC	a
Portugalskem	Portugalsko	k1gNnSc7	Portugalsko
se	se	k3xPyFc4	se
ocitly	ocitnout	k5eAaPmAgInP	ocitnout
na	na	k7c6	na
bodu	bod	k1gInSc6	bod
mrazu	mráz	k1gInSc2	mráz
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1770	[number]	k4	1770
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Francie	Francie	k1gFnSc2	Francie
===	===	k?	===
</s>
</p>
<p>
<s>
Potlačení	potlačení	k1gNnSc1	potlačení
jezuitského	jezuitský	k2eAgInSc2d1	jezuitský
řádu	řád	k1gInSc2	řád
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
začalo	začít	k5eAaPmAgNnS	začít
na	na	k7c6	na
francouzské	francouzský	k2eAgFnSc6d1	francouzská
ostrovní	ostrovní	k2eAgFnSc6d1	ostrovní
kolonii	kolonie	k1gFnSc6	kolonie
Martinique	Martiniqu	k1gFnSc2	Martiniqu
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mělo	mít	k5eAaImAgNnS	mít
Tovaryšstvo	tovaryšstvo	k1gNnSc1	tovaryšstvo
velké	velký	k2eAgFnSc2d1	velká
obchodní	obchodní	k2eAgFnSc2d1	obchodní
investice	investice	k1gFnSc2	investice
<g/>
.	.	kIx.	.
</s>
<s>
Nezaměřovali	zaměřovat	k5eNaImAgMnP	zaměřovat
se	s	k7c7	s
–	–	k?	–
a	a	k8xC	a
ani	ani	k8xC	ani
to	ten	k3xDgNnSc4	ten
neměli	mít	k5eNaImAgMnP	mít
povoleno	povolit	k5eAaPmNgNnS	povolit
–	–	k?	–
na	na	k7c4	na
obchod	obchod	k1gInSc4	obchod
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
zisku	zisk	k1gInSc2	zisk
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
než	než	k8xS	než
ostatní	ostatní	k2eAgInPc4d1	ostatní
řády	řád	k1gInPc4	řád
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gFnPc1	jejich
velké	velký	k2eAgFnPc1d1	velká
plantáže	plantáž	k1gFnPc1	plantáž
v	v	k7c6	v
misiích	misie	k1gFnPc6	misie
zahrnovaly	zahrnovat	k5eAaImAgFnP	zahrnovat
velké	velká	k1gFnPc1	velká
množství	množství	k1gNnSc2	množství
místních	místní	k2eAgMnPc2d1	místní
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
pracovali	pracovat	k5eAaImAgMnP	pracovat
za	za	k7c2	za
podmínek	podmínka	k1gFnPc2	podmínka
obvyklých	obvyklý	k2eAgFnPc2d1	obvyklá
v	v	k7c6	v
tropickém	tropický	k2eAgNnSc6d1	tropické
koloniálním	koloniální	k2eAgNnSc6d1	koloniální
zemědělství	zemědělství	k1gNnSc6	zemědělství
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
dovoleno	dovolit	k5eAaPmNgNnS	dovolit
<g/>
,	,	kIx,	,
zčásti	zčásti	k6eAd1	zčásti
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
pokryly	pokrýt	k5eAaPmAgInP	pokrýt
náklady	náklad	k1gInPc1	náklad
na	na	k7c4	na
misie	misie	k1gFnPc4	misie
<g/>
,	,	kIx,	,
zčásti	zčásti	k6eAd1	zčásti
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
domorodci	domorodec	k1gMnPc1	domorodec
nakonec	nakonec	k6eAd1	nakonec
nebyli	být	k5eNaImAgMnP	být
nuceni	nutit	k5eAaImNgMnP	nutit
pracovat	pracovat	k5eAaImF	pracovat
v	v	k7c6	v
haciendách	hacienda	k1gFnPc6	hacienda
za	za	k7c4	za
ještě	ještě	k6eAd1	ještě
horších	zlý	k2eAgFnPc2d2	horší
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Otec	otec	k1gMnSc1	otec
Antoine	Antoin	k1gInSc5	Antoin
La	la	k0	la
Valette	Valett	k1gMnSc5	Valett
<g/>
,	,	kIx,	,
představený	představený	k1gMnSc1	představený
misie	misie	k1gFnSc2	misie
na	na	k7c6	na
Matinique	Matinique	k1gFnSc6	Matinique
<g/>
,	,	kIx,	,
řídil	řídit	k5eAaImAgMnS	řídit
tyto	tento	k3xDgFnPc4	tento
plantáže	plantáž	k1gFnPc4	plantáž
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
úspěchem	úspěch	k1gInSc7	úspěch
<g/>
,	,	kIx,	,
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
světští	světský	k2eAgMnPc1d1	světský
vlastníci	vlastník	k1gMnPc1	vlastník
plantáží	plantáž	k1gFnPc2	plantáž
potřeboval	potřebovat	k5eAaImAgInS	potřebovat
půjčky	půjčka	k1gFnPc4	půjčka
peněz	peníze	k1gInPc2	peníze
k	k	k7c3	k
expanzi	expanze	k1gFnSc3	expanze
nevyužitých	využitý	k2eNgInPc2d1	nevyužitý
zdrojů	zdroj	k1gInPc2	zdroj
kolonie	kolonie	k1gFnSc2	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
po	po	k7c6	po
vypuknutí	vypuknutí	k1gNnSc6	vypuknutí
sedmileté	sedmiletý	k2eAgFnSc2d1	sedmiletá
války	válka	k1gFnSc2	válka
s	s	k7c7	s
Británií	Británie	k1gFnSc7	Británie
byly	být	k5eAaImAgFnP	být
ukořistěny	ukořistit	k5eAaPmNgFnP	ukořistit
jeho	jeho	k3xOp3gFnPc1	jeho
lodě	loď	k1gFnPc1	loď
<g/>
,	,	kIx,	,
nesoucí	nesoucí	k2eAgNnSc1d1	nesoucí
zboží	zboží	k1gNnSc1	zboží
v	v	k7c6	v
ceně	cena	k1gFnSc6	cena
2	[number]	k4	2
000	[number]	k4	000
000	[number]	k4	000
liver	livra	k1gFnPc2	livra
a	a	k8xC	a
La	la	k1gNnSc6	la
Vallete	Valle	k1gNnSc2	Valle
nebyl	být	k5eNaImAgInS	být
schopen	schopen	k2eAgInSc1d1	schopen
splácet	splácet	k5eAaImF	splácet
obrovské	obrovský	k2eAgFnPc4d1	obrovská
sumy	suma	k1gFnPc4	suma
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
věřitelé	věřitel	k1gMnPc1	věřitel
se	se	k3xPyFc4	se
obrátili	obrátit	k5eAaPmAgMnP	obrátit
na	na	k7c4	na
prokurátora	prokurátor	k1gMnSc4	prokurátor
řádu	řád	k1gInSc2	řád
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
a	a	k8xC	a
žádali	žádat	k5eAaImAgMnP	žádat
zaplacení	zaplacení	k1gNnSc4	zaplacení
<g/>
.	.	kIx.	.
</s>
<s>
Prokurátor	prokurátor	k1gMnSc1	prokurátor
však	však	k9	však
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
zodpovědnost	zodpovědnost	k1gFnSc4	zodpovědnost
za	za	k7c4	za
dluhy	dluh	k1gInPc4	dluh
nezávislé	závislý	k2eNgFnSc2d1	nezávislá
misie	misie	k1gFnSc2	misie
<g/>
,	,	kIx,	,
nabídl	nabídnout	k5eAaPmAgInS	nabídnout
však	však	k9	však
vyjednávání	vyjednávání	k1gNnSc4	vyjednávání
o	o	k7c6	o
smíru	smír	k1gInSc6	smír
<g/>
.	.	kIx.	.
</s>
<s>
Věřitelé	věřitel	k1gMnPc1	věřitel
šli	jít	k5eAaImAgMnP	jít
k	k	k7c3	k
soudu	soud	k1gInSc3	soud
a	a	k8xC	a
roku	rok	k1gInSc3	rok
1760	[number]	k4	1760
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
příkaz	příkaz	k1gInSc1	příkaz
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Tovaryšstvo	tovaryšstvo	k1gNnSc1	tovaryšstvo
zaplatilo	zaplatit	k5eAaPmAgNnS	zaplatit
<g/>
,	,	kIx,	,
a	a	k8xC	a
povolení	povolení	k1gNnSc4	povolení
k	k	k7c3	k
exekuci	exekuce	k1gFnSc3	exekuce
<g/>
.	.	kIx.	.
</s>
<s>
Otcové	otec	k1gMnPc1	otec
se	se	k3xPyFc4	se
na	na	k7c4	na
radu	rada	k1gFnSc4	rada
svých	svůj	k3xOyFgMnPc2	svůj
právníků	právník	k1gMnPc2	právník
odvolali	odvolat	k5eAaPmAgMnP	odvolat
k	k	k7c3	k
parlamentu	parlament	k1gInSc3	parlament
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
však	však	k9	však
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
být	být	k5eAaImF	být
zcela	zcela	k6eAd1	zcela
chybným	chybný	k2eAgInSc7d1	chybný
krokem	krok	k1gInSc7	krok
<g/>
.	.	kIx.	.
</s>
<s>
Nejen	nejen	k6eAd1	nejen
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
parlament	parlament	k1gInSc1	parlament
potvrdil	potvrdit	k5eAaPmAgInS	potvrdit
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
nižšího	nízký	k2eAgInSc2d2	nižší
soudu	soud	k1gInSc2	soud
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
když	když	k8xS	když
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
věc	věc	k1gFnSc1	věc
jednou	jednou	k6eAd1	jednou
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
jejich	jejich	k3xOp3gFnPc2	jejich
rukou	ruka	k1gFnPc2	ruka
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
nepřátelé	nepřítel	k1gMnPc1	nepřítel
jezuitů	jezuita	k1gMnPc2	jezuita
odhodláni	odhodlán	k2eAgMnPc1d1	odhodlán
zasadit	zasadit	k5eAaPmF	zasadit
jim	on	k3xPp3gMnPc3	on
tvrdou	tvrdý	k2eAgFnSc4d1	tvrdá
ránu	rána	k1gFnSc4	rána
<g/>
.	.	kIx.	.
</s>
<s>
Nepřátelé	nepřítel	k1gMnPc1	nepřítel
jakéhokoliv	jakýkoliv	k3yIgInSc2	jakýkoliv
druhu	druh	k1gInSc2	druh
se	se	k3xPyFc4	se
spojili	spojit	k5eAaPmAgMnP	spojit
<g/>
.	.	kIx.	.
</s>
<s>
Jansenité	Jansenitý	k2eAgNnSc1d1	Jansenitý
byli	být	k5eAaImAgMnP	být
početní	početní	k2eAgMnPc1d1	početní
mezi	mezi	k7c7	mezi
řadami	řada	k1gFnPc7	řada
ortodoxních	ortodoxní	k2eAgMnPc2d1	ortodoxní
<g/>
,	,	kIx,	,
Sorbonna	Sorbonna	k1gFnSc1	Sorbonna
se	se	k3xPyFc4	se
připojila	připojit	k5eAaPmAgFnS	připojit
ke	k	k7c3	k
gallikanům	gallikan	k1gMnPc3	gallikan
<g/>
,	,	kIx,	,
filosofům	filosof	k1gMnPc3	filosof
a	a	k8xC	a
encyklopedistům	encyklopedista	k1gMnPc3	encyklopedista
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ludvík	Ludvík	k1gMnSc1	Ludvík
XV	XV	kA	XV
<g/>
.	.	kIx.	.
byl	být	k5eAaImAgInS	být
slabý	slabý	k2eAgInSc1d1	slabý
<g/>
;	;	kIx,	;
jeho	jeho	k3xOp3gFnSc1	jeho
choť	choť	k1gFnSc1	choť
i	i	k8xC	i
děti	dítě	k1gFnPc1	dítě
stály	stát	k5eAaImAgFnP	stát
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
jezuitů	jezuita	k1gMnPc2	jezuita
<g/>
;	;	kIx,	;
jeho	jeho	k3xOp3gMnSc1	jeho
přičinlivý	přičinlivý	k2eAgMnSc1d1	přičinlivý
první	první	k4xOgMnSc1	první
ministr	ministr	k1gMnSc1	ministr
<g/>
,	,	kIx,	,
hrabě	hrabě	k1gMnSc1	hrabě
de	de	k?	de
Choiseul	Choiseul	k1gInSc1	Choiseul
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
hlavní	hlavní	k2eAgFnSc1d1	hlavní
milenka	milenka	k1gFnSc1	milenka
<g/>
,	,	kIx,	,
markýza	markýza	k1gFnSc1	markýza
de	de	k?	de
Pompadour	Pompadoura	k1gFnPc2	Pompadoura
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
jezuitský	jezuitský	k2eAgMnSc1d1	jezuitský
zpovědník	zpovědník	k1gMnSc1	zpovědník
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
udělit	udělit	k5eAaPmF	udělit
rozhřešení	rozhřešení	k1gNnSc4	rozhřešení
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
žila	žít	k5eAaImAgFnS	žít
s	s	k7c7	s
králem	král	k1gMnSc7	král
Francie	Francie	k1gFnSc2	Francie
ve	v	k7c6	v
smilstvu	smilstvo	k1gNnSc6	smilstvo
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
pevně	pevně	k6eAd1	pevně
odhodlaní	odhodlaný	k2eAgMnPc1d1	odhodlaný
protivníci	protivník	k1gMnPc1	protivník
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodnost	rozhodnost	k1gFnSc1	rozhodnost
parlamentu	parlament	k1gInSc2	parlament
smetla	smetnout	k5eAaPmAgFnS	smetnout
jakoukoliv	jakýkoliv	k3yIgFnSc4	jakýkoliv
opozici	opozice	k1gFnSc4	opozice
<g/>
.	.	kIx.	.
</s>
<s>
Útok	útok	k1gInSc1	útok
na	na	k7c4	na
jezuity	jezuita	k1gMnPc4	jezuita
byl	být	k5eAaImAgMnS	být
započat	započnout	k5eAaPmNgMnS	započnout
Jansenistickým	jansenistický	k2eAgMnSc7d1	jansenistický
sympatizérem	sympatizér	k1gMnSc7	sympatizér
<g/>
,	,	kIx,	,
Abbé	abbé	k1gMnSc7	abbé
Chauvelinem	Chauvelin	k1gInSc7	Chauvelin
<g/>
,	,	kIx,	,
17	[number]	k4	17
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1762	[number]	k4	1762
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
obžaloval	obžalovat	k5eAaPmAgMnS	obžalovat
představenstvo	představenstvo	k1gNnSc4	představenstvo
jezuitů	jezuita	k1gMnPc2	jezuita
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
veřejně	veřejně	k6eAd1	veřejně
šetřeno	šetřit	k5eAaImNgNnS	šetřit
a	a	k8xC	a
rozebíráno	rozebírat	k5eAaImNgNnS	rozebírat
v	v	k7c6	v
nepřátelském	přátelský	k2eNgInSc6d1	nepřátelský
tisku	tisk	k1gInSc6	tisk
<g/>
.	.	kIx.	.
</s>
<s>
Parlament	parlament	k1gInSc1	parlament
zveřejnil	zveřejnit	k5eAaPmAgInS	zveřejnit
jeho	on	k3xPp3gInSc4	on
Extraits	Extraits	k1gInSc4	Extraits
des	des	k1gNnSc2	des
assertions	assertionsa	k1gFnPc2	assertionsa
<g/>
,	,	kIx,	,
složené	složený	k2eAgInPc1d1	složený
z	z	k7c2	z
pasáží	pasáž	k1gFnPc2	pasáž
z	z	k7c2	z
teologických	teologický	k2eAgInPc2d1	teologický
a	a	k8xC	a
kanonických	kanonický	k2eAgInPc2d1	kanonický
spisů	spis	k1gInPc2	spis
jezuitského	jezuitský	k2eAgInSc2d1	jezuitský
řádu	řád	k1gInSc2	řád
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
bylo	být	k5eAaImAgNnS	být
uváděno	uvádět	k5eAaImNgNnS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
jezuité	jezuita	k1gMnPc1	jezuita
učí	učit	k5eAaImIp3nP	učit
bludy	blud	k1gInPc4	blud
a	a	k8xC	a
zvrácenosti	zvrácenost	k1gFnPc4	zvrácenost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
6	[number]	k4	6
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
roku	rok	k1gInSc2	rok
1762	[number]	k4	1762
bylo	být	k5eAaImAgNnS	být
konečně	konečně	k6eAd1	konečně
zveřejněno	zveřejnit	k5eAaPmNgNnS	zveřejnit
odsouzení	odsouzení	k1gNnSc4	odsouzení
Tovaryšstva	tovaryšstvo	k1gNnSc2	tovaryšstvo
k	k	k7c3	k
zániku	zánik	k1gInSc3	zánik
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
králova	králův	k2eAgFnSc1d1	králova
intervence	intervence	k1gFnSc1	intervence
přinesla	přinést	k5eAaPmAgFnS	přinést
osm	osm	k4xCc4	osm
měsíců	měsíc	k1gInPc2	měsíc
prodlení	prodlení	k1gNnSc2	prodlení
<g/>
;	;	kIx,	;
mezitím	mezitím	k6eAd1	mezitím
navrhl	navrhnout	k5eAaPmAgInS	navrhnout
dvůr	dvůr	k1gInSc1	dvůr
kompromis	kompromis	k1gInSc1	kompromis
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
francouzští	francouzský	k2eAgMnPc1d1	francouzský
jezuité	jezuita	k1gMnPc1	jezuita
zřeknou	zřeknout	k5eAaPmIp3nP	zřeknout
příslušnosti	příslušnost	k1gFnPc4	příslušnost
k	k	k7c3	k
řádu	řád	k1gInSc3	řád
a	a	k8xC	a
podřídí	podřídit	k5eAaPmIp3nS	podřídit
se	se	k3xPyFc4	se
francouzským	francouzský	k2eAgMnSc7d1	francouzský
kněžím	kněz	k1gMnPc3	kněz
tak	tak	k9	tak
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
kněží	kněz	k1gMnPc1	kněz
Gallické	gallický	k2eAgFnSc2d1	gallický
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
Koruna	koruna	k1gFnSc1	koruna
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
bude	být	k5eAaImBp3nS	být
chránit	chránit	k5eAaImF	chránit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Navzdory	navzdory	k7c3	navzdory
nebezpečí	nebezpečí	k1gNnSc3	nebezpečí
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
nutně	nutně	k6eAd1	nutně
vyplývalo	vyplývat	k5eAaImAgNnS	vyplývat
z	z	k7c2	z
odmítnutí	odmítnutí	k1gNnSc2	odmítnutí
<g/>
,	,	kIx,	,
jezuité	jezuita	k1gMnPc1	jezuita
nesouhlasili	souhlasit	k5eNaImAgMnP	souhlasit
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1763	[number]	k4	1763
byly	být	k5eAaImAgFnP	být
veškeré	veškerý	k3xTgFnPc1	veškerý
jejich	jejich	k3xOp3gFnPc1	jejich
školy	škola	k1gFnPc1	škola
uzavřeny	uzavřen	k2eAgFnPc1d1	uzavřena
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
9	[number]	k4	9
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
roku	rok	k1gInSc2	rok
1764	[number]	k4	1764
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
nuceni	nutit	k5eAaImNgMnP	nutit
zříci	zříct	k5eAaPmF	zříct
se	se	k3xPyFc4	se
svých	svůj	k3xOyFgInPc2	svůj
slibů	slib	k1gInPc2	slib
pod	pod	k7c7	pod
pohrůžkou	pohrůžka	k1gFnSc7	pohrůžka
vyhnanství	vyhnanství	k1gNnSc2	vyhnanství
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
listopadu	listopad	k1gInSc2	listopad
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
král	král	k1gMnSc1	král
podepsal	podepsat	k5eAaPmAgMnS	podepsat
edikt	edikt	k1gInSc4	edikt
<g/>
,	,	kIx,	,
rozpouštějící	rozpouštějící	k2eAgNnSc4d1	rozpouštějící
Tovaryšštsvo	Tovaryšštsvo	k1gNnSc4	Tovaryšštsvo
po	po	k7c6	po
celém	celý	k2eAgNnSc6d1	celé
svém	své	k1gNnSc6	své
území	území	k1gNnSc2	území
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
byli	být	k5eAaImAgMnP	být
stále	stále	k6eAd1	stále
chráněni	chránit	k5eAaImNgMnP	chránit
některými	některý	k3yIgInPc7	některý
provinčními	provinční	k2eAgInPc7d1	provinční
parlamenty	parlament	k1gInPc7	parlament
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
ve	v	k7c6	v
Franche-Comté	Franche-Comtý	k2eAgFnSc6d1	Franche-Comtý
<g/>
,	,	kIx,	,
Alsace	Alsace	k1gFnSc1	Alsace
a	a	k8xC	a
Artois	Artois	k1gFnSc1	Artois
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
v	v	k7c6	v
hrubém	hrubý	k2eAgInSc6d1	hrubý
nárysu	nárys	k1gInSc6	nárys
ediktu	edikt	k1gInSc2	edikt
chyběla	chybět	k5eAaImAgFnS	chybět
klauzule	klauzule	k1gFnSc1	klauzule
prohlašující	prohlašující	k2eAgFnSc1d1	prohlašující
<g/>
,	,	kIx,	,
že	že	k8xS	že
jezuité	jezuita	k1gMnPc1	jezuita
jsou	být	k5eAaImIp3nP	být
špatní	špatný	k2eAgMnPc1d1	špatný
<g/>
;	;	kIx,	;
a	a	k8xC	a
dopis	dopis	k1gInSc4	dopis
Choiseilovi	Choiseilův	k2eAgMnPc1d1	Choiseilův
zakončil	zakončit	k5eAaPmAgMnS	zakončit
slovy	slovo	k1gNnPc7	slovo
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Pokud	pokud	k8xS	pokud
přijmu	přijmout	k5eAaPmIp1nS	přijmout
rady	rada	k1gFnSc2	rada
ostatních	ostatní	k2eAgInPc2d1	ostatní
pro	pro	k7c4	pro
klid	klid	k1gInSc4	klid
své	svůj	k3xOyFgFnSc2	svůj
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
ty	ten	k3xDgMnPc4	ten
musíš	muset	k5eAaImIp2nS	muset
udělat	udělat	k5eAaPmF	udělat
změny	změna	k1gFnPc4	změna
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
jsem	být	k5eAaImIp1nS	být
zamýšlel	zamýšlet	k5eAaImAgMnS	zamýšlet
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
neučiním	učinit	k5eNaPmIp1nS	učinit
nic	nic	k3yNnSc1	nic
<g/>
.	.	kIx.	.
</s>
<s>
Neříkám	říkat	k5eNaImIp1nS	říkat
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
abych	aby	kYmCp1nS	aby
snad	snad	k9	snad
neřekl	říct	k5eNaPmAgMnS	říct
příliš	příliš	k6eAd1	příliš
mnoho	mnoho	k6eAd1	mnoho
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
===	===	k?	===
Španělsko	Španělsko	k1gNnSc1	Španělsko
a	a	k8xC	a
Neapol	Neapol	k1gFnSc1	Neapol
===	===	k?	===
</s>
</p>
<p>
<s>
Potlačení	potlačení	k1gNnSc1	potlačení
jezuitů	jezuita	k1gMnPc2	jezuita
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
a	a	k8xC	a
ve	v	k7c6	v
španělských	španělský	k2eAgFnPc6d1	španělská
koloniích	kolonie	k1gFnPc6	kolonie
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc3	jeho
državě	država	k1gFnSc3	država
království	království	k1gNnSc6	království
Neapolském	neapolský	k2eAgNnSc6d1	Neapolské
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
neslo	nést	k5eAaImAgNnS	nést
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
mlčenlivosti	mlčenlivost	k1gFnSc2	mlčenlivost
<g/>
;	;	kIx,	;
a	a	k8xC	a
ministři	ministr	k1gMnPc1	ministr
Karla	Karel	k1gMnSc2	Karel
III	III	kA	III
<g/>
.	.	kIx.	.
si	se	k3xPyFc3	se
nechali	nechat	k5eAaPmAgMnP	nechat
své	svůj	k3xOyFgNnSc4	svůj
mínění	mínění	k1gNnSc4	mínění
pro	pro	k7c4	pro
sebe	sebe	k3xPyFc4	sebe
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
král	král	k1gMnSc1	král
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
jednal	jednat	k5eAaImAgMnS	jednat
pod	pod	k7c7	pod
tlakem	tlak	k1gInSc7	tlak
"	"	kIx"	"
<g/>
bezodkladných	bezodkladný	k2eAgFnPc2d1	bezodkladná
<g/>
,	,	kIx,	,
spravedlivých	spravedlivý	k2eAgFnPc2d1	spravedlivá
a	a	k8xC	a
nezbytných	zbytný	k2eNgFnPc2d1	zbytný
příčin	příčina	k1gFnPc2	příčina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
si	se	k3xPyFc3	se
vyhrazuji	vyhrazovat	k5eAaImIp1nS	vyhrazovat
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
korunovanou	korunovaný	k2eAgFnSc4d1	korunovaná
hlavu	hlava	k1gFnSc4	hlava
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Korespondence	korespondence	k1gFnSc1	korespondence
Bernarda	Bernard	k1gMnSc2	Bernard
Tanucciho	Tanucci	k1gMnSc2	Tanucci
<g/>
,	,	kIx,	,
antiklerikálního	antiklerikální	k2eAgMnSc2d1	antiklerikální
ministra	ministr	k1gMnSc2	ministr
Karla	Karel	k1gMnSc2	Karel
III	III	kA	III
<g/>
.	.	kIx.	.
v	v	k7c6	v
Neapoli	Neapol	k1gFnSc6	Neapol
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
veškeré	veškerý	k3xTgFnPc4	veškerý
ideje	idea	k1gFnPc4	idea
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
byly	být	k5eAaImAgFnP	být
čas	čas	k1gInSc4	čas
od	od	k7c2	od
času	čas	k1gInSc2	čas
vodítkem	vodítko	k1gNnSc7	vodítko
španělské	španělský	k2eAgFnSc2d1	španělská
politiky	politika	k1gFnSc2	politika
<g/>
.	.	kIx.	.
</s>
<s>
Karlovu	Karlův	k2eAgFnSc4d1	Karlova
vládu	vláda	k1gFnSc4	vláda
vedl	vést	k5eAaImAgMnS	vést
hrabě	hrabě	k1gMnSc1	hrabě
Aranda	Aranda	k1gFnSc1	Aranda
<g/>
,	,	kIx,	,
čtenář	čtenář	k1gMnSc1	čtenář
Voltaira	Voltaira	k1gMnSc1	Voltaira
<g/>
,	,	kIx,	,
a	a	k8xC	a
jiní	jiný	k2eAgMnPc1d1	jiný
liberálové	liberál	k1gMnPc1	liberál
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zasedání	zasedání	k1gNnSc6	zasedání
rady	rada	k1gFnSc2	rada
29	[number]	k4	29
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1767	[number]	k4	1767
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
ujednáno	ujednat	k5eAaPmNgNnS	ujednat
vyhoštění	vyhoštění	k1gNnSc1	vyhoštění
Tovaryšstva	tovaryšstvo	k1gNnSc2	tovaryšstvo
Ježíšova	Ježíšův	k2eAgNnSc2d1	Ježíšovo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tajné	tajný	k2eAgInPc1d1	tajný
příkazy	příkaz	k1gInPc1	příkaz
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
měly	mít	k5eAaImAgInP	mít
být	být	k5eAaImF	být
otevřeny	otevřít	k5eAaPmNgInP	otevřít
o	o	k7c6	o
půlnoci	půlnoc	k1gFnSc6	půlnoc
mezi	mezi	k7c7	mezi
1	[number]	k4	1
<g/>
.	.	kIx.	.
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
dubnem	duben	k1gInSc7	duben
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
odeslány	odeslat	k5eAaPmNgFnP	odeslat
na	na	k7c4	na
radnice	radnice	k1gFnSc1	radnice
každého	každý	k3xTgNnSc2	každý
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
sídlili	sídlit	k5eAaImAgMnP	sídlit
jezuité	jezuita	k1gMnPc1	jezuita
<g/>
.	.	kIx.	.
</s>
<s>
Plán	plán	k1gInSc1	plán
postupoval	postupovat	k5eAaImAgInS	postupovat
hladce	hladko	k6eAd1	hladko
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgNnSc2	ten
rána	ráno	k1gNnSc2	ráno
putovalo	putovat	k5eAaImAgNnS	putovat
asi	asi	k9	asi
6000	[number]	k4	6000
jezuitů	jezuita	k1gMnPc2	jezuita
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
jakožto	jakožto	k8xS	jakožto
trestanci	trestanec	k1gMnSc3	trestanec
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byli	být	k5eAaImAgMnP	být
deportováni	deportován	k2eAgMnPc1d1	deportován
<g/>
;	;	kIx,	;
nejprve	nejprve	k6eAd1	nejprve
do	do	k7c2	do
Papežského	papežský	k2eAgInSc2d1	papežský
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
na	na	k7c4	na
Korsiku	Korsika	k1gFnSc4	Korsika
<g/>
,	,	kIx,	,
závislé	závislý	k2eAgNnSc1d1	závislé
území	území	k1gNnSc1	území
města	město	k1gNnSc2	město
Janov	Janov	k1gInSc1	Janov
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
izolaci	izolace	k1gFnSc3	izolace
španělských	španělský	k2eAgFnPc2d1	španělská
misií	misie	k1gFnPc2	misie
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
do	do	k7c2	do
ostatních	ostatní	k2eAgFnPc2d1	ostatní
částí	část	k1gFnPc2	část
Nového	Nového	k2eAgNnSc2d1	Nového
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
,	,	kIx,	,
výnos	výnos	k1gInSc1	výnos
dorazil	dorazit	k5eAaPmAgInS	dorazit
až	až	k9	až
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
však	však	k9	však
odložen	odložit	k5eAaPmNgInS	odložit
do	do	k7c2	do
30	[number]	k4	30
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
přijel	přijet	k5eAaPmAgMnS	přijet
nový	nový	k2eAgMnSc1d1	nový
guvernér	guvernér	k1gMnSc1	guvernér
se	s	k7c7	s
zprávami	zpráva	k1gFnPc7	zpráva
<g/>
.	.	kIx.	.
</s>
<s>
Jezuité	jezuita	k1gMnPc1	jezuita
ze	z	k7c2	z
čtrnácti	čtrnáct	k4xCc2	čtrnáct
provozních	provozní	k2eAgFnPc2d1	provozní
misií	misie	k1gFnPc2	misie
se	se	k3xPyFc4	se
shledali	shledat	k5eAaPmAgMnP	shledat
v	v	k7c6	v
Loretu	Loreto	k1gNnSc6	Loreto
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
odešli	odejít	k5eAaPmAgMnP	odejít
3	[number]	k4	3
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1768	[number]	k4	1768
do	do	k7c2	do
exilu	exil	k1gInSc2	exil
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
sklonku	sklonek	k1gInSc6	sklonek
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgInS	dostat
královský	královský	k2eAgInSc1d1	královský
rozkaz	rozkaz	k1gInSc1	rozkaz
i	i	k9	i
na	na	k7c4	na
jezuitské	jezuitský	k2eAgFnPc4d1	jezuitská
misie	misie	k1gFnPc4	misie
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Filipín	Filipíny	k1gFnPc2	Filipíny
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
jezuité	jezuita	k1gMnPc1	jezuita
byli	být	k5eAaImAgMnP	být
vyhnáni	vyhnat	k5eAaPmNgMnP	vyhnat
z	z	k7c2	z
celého	celý	k2eAgNnSc2d1	celé
Španělského	španělský	k2eAgNnSc2d1	španělské
dominia	dominion	k1gNnSc2	dominion
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tanucci	Tanucce	k1gFnSc4	Tanucce
prosadil	prosadit	k5eAaPmAgMnS	prosadit
podobnou	podobný	k2eAgFnSc4d1	podobná
politiku	politika	k1gFnSc4	politika
v	v	k7c6	v
bourbonské	bourbonský	k2eAgFnSc6d1	Bourbonská
Neapoli	Neapol	k1gFnSc6	Neapol
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
byli	být	k5eAaImAgMnP	být
jezuité	jezuita	k1gMnPc1	jezuita
bez	bez	k7c2	bez
soudu	soud	k1gInSc2	soud
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
i	i	k9	i
bez	bez	k7c2	bez
obžaloby	obžaloba	k1gFnSc2	obžaloba
vyvedeni	vyvést	k5eAaPmNgMnP	vyvést
za	za	k7c2	za
hranice	hranice	k1gFnSc2	hranice
do	do	k7c2	do
Papežského	papežský	k2eAgInSc2d1	papežský
státu	stát	k1gInSc2	stát
pod	pod	k7c7	pod
pohrůžkou	pohrůžka	k1gFnSc7	pohrůžka
smrtí	smrt	k1gFnSc7	smrt
při	při	k7c6	při
pokusu	pokus	k1gInSc6	pokus
o	o	k7c4	o
návrat	návrat	k1gInSc4	návrat
<g/>
.	.	kIx.	.
</s>
<s>
Změna	změna	k1gFnSc1	změna
ve	v	k7c6	v
španělských	španělský	k2eAgFnPc6d1	španělská
koloniích	kolonie	k1gFnPc6	kolonie
v	v	k7c6	v
Novém	nový	k2eAgInSc6d1	nový
světě	svět	k1gInSc6	svět
byla	být	k5eAaImAgFnS	být
zvláště	zvláště	k6eAd1	zvláště
patrná	patrný	k2eAgFnSc1d1	patrná
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
ve	v	k7c6	v
vzdálených	vzdálený	k2eAgFnPc6d1	vzdálená
osadách	osada	k1gFnPc6	osada
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
byly	být	k5eAaImAgFnP	být
často	často	k6eAd1	často
spravovány	spravován	k2eAgFnPc1d1	spravována
misiemi	misie	k1gFnPc7	misie
<g/>
.	.	kIx.	.
</s>
<s>
Takřka	takřka	k6eAd1	takřka
každou	každý	k3xTgFnSc4	každý
noc	noc	k1gFnSc4	noc
zmizely	zmizet	k5eAaPmAgFnP	zmizet
v	v	k7c6	v
misijních	misijní	k2eAgNnPc6d1	misijní
městech	město	k1gNnPc6	město
"	"	kIx"	"
<g/>
černé	černý	k2eAgFnSc2d1	černá
róby	róba	k1gFnSc2	róba
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
jak	jak	k6eAd1	jak
byli	být	k5eAaImAgMnP	být
nazýváni	nazýván	k2eAgMnPc1d1	nazýván
jezuité	jezuita	k1gMnPc1	jezuita
<g/>
)	)	kIx)	)
a	a	k8xC	a
nahradily	nahradit	k5eAaPmAgInP	nahradit
je	on	k3xPp3gFnPc4	on
"	"	kIx"	"
<g/>
šedé	šedý	k2eAgFnPc4d1	šedá
róby	róba	k1gFnPc4	róba
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
františkáni	františkán	k1gMnPc1	františkán
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Parma	Parma	k1gFnSc1	Parma
===	===	k?	===
</s>
</p>
<p>
<s>
Nezávislé	závislý	k2eNgNnSc1d1	nezávislé
vévodství	vévodství	k1gNnSc1	vévodství
Parmské	parmský	k2eAgFnSc2d1	parmská
byl	být	k5eAaImAgInS	být
nejmenší	malý	k2eAgInSc4d3	nejmenší
Bourbonský	bourbonský	k2eAgInSc4d1	bourbonský
dvůr	dvůr	k1gInSc4	dvůr
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
byla	být	k5eAaImAgFnS	být
vévodkyní	vévodkyně	k1gFnSc7	vévodkyně
nejoblíbenější	oblíbený	k2eAgFnSc1d3	nejoblíbenější
dcera	dcera	k1gFnSc1	dcera
Ludvíka	Ludvík	k1gMnSc2	Ludvík
XV	XV	kA	XV
<g/>
.	.	kIx.	.
</s>
<s>
Parma	Parma	k1gFnSc1	Parma
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
antiklericismu	antiklericismus	k1gInSc6	antiklericismus
tak	tak	k6eAd1	tak
agresivní	agresivní	k2eAgNnSc1d1	agresivní
<g/>
,	,	kIx,	,
když	když	k8xS	když
došla	dojít	k5eAaPmAgFnS	dojít
zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
vypuzení	vypuzení	k1gNnSc6	vypuzení
jezuitů	jezuita	k1gMnPc2	jezuita
z	z	k7c2	z
Neapole	Neapol	k1gFnSc2	Neapol
<g/>
,	,	kIx,	,
že	že	k8xS	že
papež	papež	k1gMnSc1	papež
Klement	Klement	k1gMnSc1	Klement
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
jí	on	k3xPp3gFnSc3	on
adresoval	adresovat	k5eAaBmAgMnS	adresovat
30	[number]	k4	30
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1768	[number]	k4	1768
veřejné	veřejný	k2eAgNnSc4d1	veřejné
varování	varování	k1gNnPc4	varování
<g/>
,	,	kIx,	,
hroze	hrozit	k5eAaImSgInS	hrozit
vévodství	vévodství	k1gNnSc1	vévodství
církevním	církevní	k2eAgNnSc7d1	církevní
zavržením	zavržení	k1gNnSc7	zavržení
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
nebyl	být	k5eNaImAgInS	být
příliš	příliš	k6eAd1	příliš
diplomatický	diplomatický	k2eAgInSc1d1	diplomatický
tah	tah	k1gInSc1	tah
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
Bourbonské	bourbonský	k2eAgInPc1d1	bourbonský
dvory	dvůr	k1gInPc1	dvůr
obrátily	obrátit	k5eAaPmAgInP	obrátit
v	v	k7c6	v
hněvu	hněv	k1gInSc6	hněv
proti	proti	k7c3	proti
Svatému	svatý	k2eAgInSc3d1	svatý
Stolci	stolec	k1gInSc3	stolec
<g/>
,	,	kIx,	,
a	a	k8xC	a
dožadovaly	dožadovat	k5eAaImAgFnP	dožadovat
se	se	k3xPyFc4	se
konečného	konečný	k2eAgNnSc2d1	konečné
rozpuštění	rozpuštění	k1gNnSc2	rozpuštění
jezuitů	jezuita	k1gMnPc2	jezuita
<g/>
.	.	kIx.	.
</s>
<s>
Parma	Parma	k1gFnSc1	Parma
jakožto	jakožto	k8xS	jakožto
předběžné	předběžný	k2eAgNnSc1d1	předběžné
opatření	opatření	k1gNnSc1	opatření
vypudila	vypudit	k5eAaPmAgFnS	vypudit
jezuity	jezuita	k1gMnPc4	jezuita
ze	z	k7c2	z
svého	svůj	k3xOyFgNnSc2	svůj
území	území	k1gNnSc2	území
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
zkonfiskovala	zkonfiskovat	k5eAaPmAgFnS	zkonfiskovat
veškeré	veškerý	k3xTgNnSc4	veškerý
jejich	jejich	k3xOp3gNnSc4	jejich
vlastnictví	vlastnictví	k1gNnSc4	vlastnictví
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Papež	Papež	k1gMnSc1	Papež
Klement	Klement	k1gMnSc1	Klement
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
===	===	k?	===
</s>
</p>
<p>
<s>
Papež	Papež	k1gMnSc1	Papež
Klement	Klement	k1gMnSc1	Klement
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgInS	zabývat
zrušením	zrušení	k1gNnSc7	zrušení
jezuitů	jezuita	k1gMnPc2	jezuita
<g/>
,	,	kIx,	,
dokument	dokument	k1gInSc1	dokument
největšího	veliký	k2eAgInSc2d3	veliký
významu	význam	k1gInSc2	význam
byl	být	k5eAaImAgInS	být
napsán	napsat	k5eAaPmNgInS	napsat
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
roku	rok	k1gInSc2	rok
1772	[number]	k4	1772
a	a	k8xC	a
podepsán	podepsán	k2eAgInSc1d1	podepsán
21	[number]	k4	21
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
roku	rok	k1gInSc2	rok
1773	[number]	k4	1773
–	–	k?	–
bula	bula	k1gFnSc1	bula
<g/>
,	,	kIx,	,
rušící	rušící	k2eAgInSc1d1	rušící
jezuitský	jezuitský	k2eAgInSc1d1	jezuitský
řád	řád	k1gInSc1	řád
navždy	navždy	k6eAd1	navždy
a	a	k8xC	a
neodvolatelně	odvolatelně	k6eNd1	odvolatelně
<g/>
,	,	kIx,	,
Dominus	Dominus	k1gMnSc1	Dominus
ac	ac	k?	ac
Redemptor	Redemptor	k1gMnSc1	Redemptor
Noster	Noster	k1gMnSc1	Noster
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
–	–	k?	–
<g/>
bula	bula	k1gFnSc1	bula
Dominus	Dominus	k1gMnSc1	Dominus
ac	ac	k?	ac
Redemptor	Redemptor	k1gMnSc1	Redemptor
Noster	Noster	k1gMnSc1	Noster
</s>
</p>
<p>
<s>
Jezuité	jezuita	k1gMnPc1	jezuita
mohli	moct	k5eAaImAgMnP	moct
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1814	[number]	k4	1814
dále	daleko	k6eAd2	daleko
působit	působit	k5eAaImF	působit
jen	jen	k9	jen
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
a	a	k8xC	a
Prusku	Prusko	k1gNnSc6	Prusko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obnovení	obnovení	k1gNnSc1	obnovení
jezuitského	jezuitský	k2eAgInSc2d1	jezuitský
řádu	řád	k1gInSc2	řád
1814	[number]	k4	1814
==	==	k?	==
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1814	[number]	k4	1814
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
napoleonské	napoleonský	k2eAgFnPc1d1	napoleonská
války	válka	k1gFnPc1	válka
chýlily	chýlit	k5eAaImAgFnP	chýlit
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
Vídeňském	vídeňský	k2eAgInSc6d1	vídeňský
kongresu	kongres	k1gInSc6	kongres
obnoven	obnovit	k5eAaPmNgInS	obnovit
politický	politický	k2eAgInSc1d1	politický
řád	řád	k1gInSc1	řád
Evropy	Evropa	k1gFnSc2	Evropa
ve	v	k7c6	v
značném	značný	k2eAgInSc6d1	značný
rozsahu	rozsah	k1gInSc6	rozsah
<g/>
;	;	kIx,	;
po	po	k7c6	po
letech	let	k1gInPc6	let
bojů	boj	k1gInPc2	boj
a	a	k8xC	a
revolucí	revoluce	k1gFnPc2	revoluce
<g/>
,	,	kIx,	,
během	během	k7c2	během
nichž	jenž	k3xRgInPc2	jenž
byla	být	k5eAaImAgFnS	být
církev	církev	k1gFnSc1	církev
perzekvována	perzekvovat	k5eAaImNgFnS	perzekvovat
jakožto	jakožto	k8xS	jakožto
agent	agent	k1gMnSc1	agent
starých	starý	k2eAgInPc2d1	starý
pořádků	pořádek	k1gInPc2	pořádek
a	a	k8xC	a
utlačována	utlačován	k2eAgFnSc1d1	utlačována
pod	pod	k7c7	pod
vládou	vláda	k1gFnSc7	vláda
Napoleona	Napoleon	k1gMnSc2	Napoleon
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
politické	politický	k2eAgNnSc1d1	politické
klima	klima	k1gNnSc1	klima
Evropy	Evropa	k1gFnSc2	Evropa
poněkud	poněkud	k6eAd1	poněkud
stabilizovalo	stabilizovat	k5eAaBmAgNnS	stabilizovat
a	a	k8xC	a
mocní	mocný	k2eAgMnPc1d1	mocný
monarchové	monarcha	k1gMnPc1	monarcha
<g/>
,	,	kIx,	,
ježto	jenžto	k3yRgMnPc4	jenžto
volali	volat	k5eAaImAgMnP	volat
po	po	k7c4	po
zrušení	zrušení	k1gNnSc4	zrušení
jezuitů	jezuita	k1gMnPc2	jezuita
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
pozbyli	pozbýt	k5eAaPmAgMnP	pozbýt
své	svůj	k3xOyFgFnPc4	svůj
síly	síla	k1gFnPc4	síla
<g/>
,	,	kIx,	,
papež	papež	k1gMnSc1	papež
Pius	Pius	k1gMnSc1	Pius
VII	VII	kA	VII
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
zasadil	zasadit	k5eAaPmAgInS	zasadit
o	o	k7c4	o
obnovení	obnovení	k1gNnSc4	obnovení
řádu	řád	k1gInSc2	řád
Tovaryšstva	tovaryšstvo	k1gNnSc2	tovaryšstvo
Ježíšova	Ježíšův	k2eAgNnSc2d1	Ježíšovo
v	v	k7c6	v
katolických	katolický	k2eAgFnPc6d1	katolická
zemích	zem	k1gFnPc6	zem
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
samotného	samotný	k2eAgNnSc2d1	samotné
Tovaryšstva	tovaryšstvo	k1gNnSc2	tovaryšstvo
týče	týkat	k5eAaImIp3nS	týkat
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
na	na	k7c6	na
prvním	první	k4xOgNnSc6	první
generálním	generální	k2eAgNnSc6d1	generální
shromáždění	shromáždění	k1gNnSc6	shromáždění
po	po	k7c6	po
obnově	obnova	k1gFnSc6	obnova
usneslo	usnést	k5eAaPmAgNnS	usnést
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
svou	svůj	k3xOyFgFnSc4	svůj
činnost	činnost	k1gFnSc4	činnost
i	i	k9	i
nadále	nadále	k6eAd1	nadále
stejným	stejný	k2eAgInSc7d1	stejný
směrem	směr	k1gInSc7	směr
jako	jako	k8xC	jako
před	před	k7c7	před
zrušením	zrušení	k1gNnSc7	zrušení
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
bylo	být	k5eAaImAgNnS	být
přikázáno	přikázat	k5eAaPmNgNnS	přikázat
roku	rok	k1gInSc2	rok
1773	[number]	k4	1773
<g/>
.	.	kIx.	.
</s>
<s>
Řád	řád	k1gInSc1	řád
působí	působit	k5eAaImIp3nS	působit
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Činnost	činnost	k1gFnSc4	činnost
řádu	řád	k1gInSc2	řád
==	==	k?	==
</s>
</p>
<p>
<s>
Činnost	činnost	k1gFnSc1	činnost
řádu	řád	k1gInSc2	řád
se	se	k3xPyFc4	se
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
soustřeďuje	soustřeďovat	k5eAaImIp3nS	soustřeďovat
zejména	zejména	k9	zejména
na	na	k7c4	na
pastorační	pastorační	k2eAgFnSc4d1	pastorační
a	a	k8xC	a
misijní	misijní	k2eAgFnSc4d1	misijní
činnost	činnost	k1gFnSc4	činnost
a	a	k8xC	a
na	na	k7c4	na
oblast	oblast	k1gFnSc4	oblast
vědy	věda	k1gFnSc2	věda
a	a	k8xC	a
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jezuitské	jezuitský	k2eAgNnSc1d1	jezuitské
školství	školství	k1gNnSc1	školství
</s>
</p>
<p>
<s>
Jezuitské	jezuitský	k2eAgFnPc1d1	jezuitská
školy	škola	k1gFnPc1	škola
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
elitě	elita	k1gFnSc3	elita
katolického	katolický	k2eAgNnSc2d1	katolické
školství	školství	k1gNnSc2	školství
a	a	k8xC	a
i	i	k9	i
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
mají	mít	k5eAaImIp3nP	mít
dobrou	dobrý	k2eAgFnSc4d1	dobrá
pověst	pověst	k1gFnSc4	pověst
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
jich	on	k3xPp3gMnPc2	on
již	již	k6eAd1	již
není	být	k5eNaImIp3nS	být
tolik	tolik	k6eAd1	tolik
jako	jako	k8xS	jako
např.	např.	kA	např.
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgFnPc4d3	nejvýznamnější
univerzity	univerzita	k1gFnPc4	univerzita
provozované	provozovaný	k2eAgFnPc4d1	provozovaná
dnes	dnes	k6eAd1	dnes
řádem	řád	k1gInSc7	řád
patří	patřit	k5eAaImIp3nP	patřit
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
i	i	k9	i
Boston	Boston	k1gInSc4	Boston
College	Colleg	k1gFnSc2	Colleg
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
sídlem	sídlo	k1gNnSc7	sídlo
největší	veliký	k2eAgFnSc2d3	veliký
jezuitské	jezuitský	k2eAgFnSc2d1	jezuitská
komunity	komunita	k1gFnSc2	komunita
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
a	a	k8xC	a
především	především	k6eAd1	především
historicky	historicky	k6eAd1	historicky
první	první	k4xOgFnSc1	první
jezuitská	jezuitský	k2eAgFnSc1d1	jezuitská
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
Papežská	papežský	k2eAgFnSc1d1	Papežská
univerzita	univerzita	k1gFnSc1	univerzita
Gregoriana	Gregoriana	k1gFnSc1	Gregoriana
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
založil	založit	k5eAaPmAgMnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1551	[number]	k4	1551
sv.	sv.	kA	sv.
Ignác	Ignác	k1gMnSc1	Ignác
z	z	k7c2	z
Loyoly	Loyola	k1gFnSc2	Loyola
a	a	k8xC	a
která	který	k3yIgNnPc1	který
náleží	náležet	k5eAaImIp3nP	náležet
k	k	k7c3	k
nejprestižnějším	prestižní	k2eAgFnPc3d3	nejprestižnější
univerzitám	univerzita	k1gFnPc3	univerzita
světa	svět	k1gInSc2	svět
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
teologie	teologie	k1gFnSc2	teologie
<g/>
.	.	kIx.	.
</s>
<s>
Nutno	nutno	k6eAd1	nutno
však	však	k9	však
dodat	dodat	k5eAaPmF	dodat
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
těchto	tento	k3xDgFnPc6	tento
školách	škola	k1gFnPc6	škola
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
nebyla	být	k5eNaImAgFnS	být
svoboda	svoboda	k1gFnSc1	svoboda
vyznání	vyznání	k1gNnSc2	vyznání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
výuka	výuka	k1gFnSc1	výuka
jiné	jiný	k2eAgFnSc2d1	jiná
než	než	k8xS	než
katolické	katolický	k2eAgFnSc2d1	katolická
teologie	teologie	k1gFnSc2	teologie
na	na	k7c6	na
vzestupu	vzestup	k1gInSc6	vzestup
a	a	k8xC	a
katolická	katolický	k2eAgFnSc1d1	katolická
teologie	teologie	k1gFnSc1	teologie
plně	plně	k6eAd1	plně
na	na	k7c6	na
platformě	platforma	k1gFnSc6	platforma
neomodernismu	neomodernismus	k1gInSc2	neomodernismus
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Působení	působení	k1gNnPc4	působení
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
==	==	k?	==
</s>
</p>
<p>
<s>
Do	do	k7c2	do
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
přišli	přijít	k5eAaPmAgMnP	přijít
jezuité	jezuita	k1gMnPc1	jezuita
z	z	k7c2	z
Vídně	Vídeň	k1gFnSc2	Vídeň
roku	rok	k1gInSc2	rok
1556	[number]	k4	1556
<g/>
.	.	kIx.	.
</s>
<s>
Podnětem	podnět	k1gInSc7	podnět
k	k	k7c3	k
jejich	jejich	k3xOp3gInSc3	jejich
příchodu	příchod	k1gInSc3	příchod
byl	být	k5eAaImAgMnS	být
dopis	dopis	k1gInSc4	dopis
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
roku	rok	k1gInSc2	rok
1554	[number]	k4	1554
poslala	poslat	k5eAaPmAgFnS	poslat
Ignáci	Ignác	k1gMnSc3	Ignác
z	z	k7c2	z
Loyoly	Loyola	k1gFnSc2	Loyola
česká	český	k2eAgFnSc1d1	Česká
katolická	katolický	k2eAgFnSc1d1	katolická
strana	strana	k1gFnSc1	strana
<g/>
.	.	kIx.	.
</s>
<s>
Určitá	určitý	k2eAgNnPc1d1	určité
jednání	jednání	k1gNnPc1	jednání
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
vedl	vést	k5eAaImAgMnS	vést
i	i	k9	i
král	král	k1gMnSc1	král
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
I.	I.	kA	I.
Jezuité	jezuita	k1gMnPc1	jezuita
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
působili	působit	k5eAaImAgMnP	působit
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1618	[number]	k4	1618
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
byli	být	k5eAaImAgMnP	být
vykázáni	vykázán	k2eAgMnPc1d1	vykázán
nekatolíky	nekatolík	k1gMnPc4	nekatolík
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nazpět	nazpět	k6eAd1	nazpět
se	se	k3xPyFc4	se
vrátili	vrátit	k5eAaPmAgMnP	vrátit
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1620	[number]	k4	1620
po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
na	na	k7c6	na
Bílé	bílý	k2eAgFnSc6d1	bílá
hoře	hora	k1gFnSc6	hora
<g/>
.	.	kIx.	.
</s>
<s>
Jezuité	jezuita	k1gMnPc1	jezuita
prováděli	provádět	k5eAaImAgMnP	provádět
takzvané	takzvaný	k2eAgFnPc4d1	takzvaná
vnitřní	vnitřní	k2eAgFnPc4d1	vnitřní
misie	misie	k1gFnPc4	misie
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
cílem	cíl	k1gInSc7	cíl
byla	být	k5eAaImAgFnS	být
rekatolizace	rekatolizace	k1gFnSc1	rekatolizace
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
působení	působení	k1gNnSc1	působení
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
vnímáno	vnímat	k5eAaImNgNnS	vnímat
rozporně	rozporně	k6eAd1	rozporně
<g/>
.	.	kIx.	.
</s>
<s>
Podporovali	podporovat	k5eAaImAgMnP	podporovat
vzdělanost	vzdělanost	k1gFnSc4	vzdělanost
a	a	k8xC	a
zakládali	zakládat	k5eAaImAgMnP	zakládat
školy	škola	k1gFnPc4	škola
(	(	kIx(	(
<g/>
základní	základní	k2eAgFnSc2d1	základní
<g/>
,	,	kIx,	,
střední	střední	k2eAgFnSc2d1	střední
i	i	k8xC	i
vysoké	vysoký	k2eAgFnSc2d1	vysoká
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
vynikající	vynikající	k2eAgFnSc4d1	vynikající
úroveň	úroveň	k1gFnSc4	úroveň
vzdělání	vzdělání	k1gNnSc2	vzdělání
<g/>
,	,	kIx,	,
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
školách	škola	k1gFnPc6	škola
neexistovala	existovat	k5eNaImAgFnS	existovat
svoboda	svoboda	k1gFnSc1	svoboda
vyznání	vyznání	k1gNnSc2	vyznání
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
praktikována	praktikovat	k5eAaImNgFnS	praktikovat
i	i	k9	i
cenzura	cenzura	k1gFnSc1	cenzura
části	část	k1gFnSc2	část
česky	česky	k6eAd1	česky
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
a	a	k8xC	a
francouzsky	francouzsky	k6eAd1	francouzsky
psané	psaný	k2eAgFnSc2d1	psaná
literatury	literatura	k1gFnSc2	literatura
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
fyzické	fyzický	k2eAgFnSc2d1	fyzická
likvidace	likvidace	k1gFnSc2	likvidace
knih	kniha	k1gFnPc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
dílo	dílo	k1gNnSc1	dílo
Jana	Jan	k1gMnSc2	Jan
Amose	Amos	k1gMnSc2	Amos
Komenského	Komenský	k1gMnSc2	Komenský
<g/>
,	,	kIx,	,
z	z	k7c2	z
nějž	jenž	k3xRgInSc2	jenž
mohly	moct	k5eAaImAgInP	moct
být	být	k5eAaImF	být
(	(	kIx(	(
<g/>
po	po	k7c6	po
cenzurních	cenzurní	k2eAgFnPc6d1	cenzurní
úpravách	úprava	k1gFnPc6	úprava
<g/>
)	)	kIx)	)
vydávány	vydávat	k5eAaImNgFnP	vydávat
pouze	pouze	k6eAd1	pouze
některé	některý	k3yIgFnPc4	některý
učebnice	učebnice	k1gFnPc4	učebnice
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
faktů	fakt	k1gInPc2	fakt
o	o	k7c4	o
fungování	fungování	k1gNnSc4	fungování
jezuitské	jezuitský	k2eAgFnSc2d1	jezuitská
cenzury	cenzura	k1gFnSc2	cenzura
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
bylo	být	k5eAaImAgNnS	být
ovšem	ovšem	k9	ovšem
díky	díky	k7c3	díky
různým	různý	k2eAgInPc3d1	různý
politickým	politický	k2eAgInPc3d1	politický
tlakům	tlak	k1gInPc3	tlak
a	a	k8xC	a
aktuálním	aktuální	k2eAgFnPc3d1	aktuální
náladám	nálada	k1gFnPc3	nálada
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
také	také	k9	také
často	často	k6eAd1	často
zkreslováno	zkreslován	k2eAgNnSc1d1	zkreslováno
a	a	k8xC	a
zjednodušováno	zjednodušován	k2eAgNnSc1d1	zjednodušováno
(	(	kIx(	(
<g/>
vizte	vidět	k5eAaImRp2nP	vidět
případ	případ	k1gInSc1	případ
pátera	páter	k1gMnSc2	páter
Koniáše	Koniáš	k1gMnSc2	Koniáš
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
společnost	společnost	k1gFnSc1	společnost
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
řád	řád	k1gInSc4	řád
pohlížela	pohlížet	k5eAaImAgFnS	pohlížet
spíše	spíše	k9	spíše
negativně	negativně	k6eAd1	negativně
a	a	k8xC	a
tento	tento	k3xDgInSc4	tento
náhled	náhled	k1gInSc4	náhled
přetrvává	přetrvávat	k5eAaImIp3nS	přetrvávat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
přišli	přijít	k5eAaPmAgMnP	přijít
první	první	k4xOgMnPc1	první
jezuité	jezuita	k1gMnPc1	jezuita
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
roku	rok	k1gInSc2	rok
1556	[number]	k4	1556
z	z	k7c2	z
Vídně	Vídeň	k1gFnSc2	Vídeň
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
teologa	teolog	k1gMnSc2	teolog
Petra	Petr	k1gMnSc2	Petr
Canisia	Canisius	k1gMnSc2	Canisius
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
12	[number]	k4	12
bratří	bratr	k1gMnPc2	bratr
a	a	k8xC	a
založili	založit	k5eAaPmAgMnP	založit
první	první	k4xOgFnSc4	první
kolej	kolej	k1gFnSc4	kolej
na	na	k7c6	na
Starém	starý	k2eAgNnSc6d1	staré
Městě	město	k1gNnSc6	město
pražském	pražský	k2eAgNnSc6d1	Pražské
<g/>
,	,	kIx,	,
západní	západní	k2eAgNnSc1d1	západní
křídlo	křídlo	k1gNnSc1	křídlo
později	pozdě	k6eAd2	pozdě
dostavěného	dostavěný	k2eAgInSc2d1	dostavěný
areálu	areál	k1gInSc2	areál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
koleji	kolej	k1gFnSc6	kolej
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
dostavbě	dostavba	k1gFnSc6	dostavba
pojmenována	pojmenován	k2eAgNnPc4d1	pojmenováno
Klementinum	Klementinum	k1gNnSc4	Klementinum
<g/>
,	,	kIx,	,
řád	řád	k1gInSc4	řád
zřídil	zřídit	k5eAaPmAgMnS	zřídit
ještě	ještě	k6eAd1	ještě
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
gymnázium	gymnázium	k1gNnSc1	gymnázium
a	a	k8xC	a
akademii	akademie	k1gFnSc3	akademie
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
byly	být	k5eAaImAgFnP	být
konány	konán	k2eAgFnPc1d1	konána
přednášky	přednáška	k1gFnPc1	přednáška
a	a	k8xC	a
zkoušky	zkouška	k1gFnPc1	zkouška
z	z	k7c2	z
filosofie	filosofie	k1gFnSc2	filosofie
a	a	k8xC	a
teologie	teologie	k1gFnSc2	teologie
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1562	[number]	k4	1562
dal	dát	k5eAaPmAgMnS	dát
král	král	k1gMnSc1	král
koleji	kolej	k1gFnSc3	kolej
právo	právo	k1gNnSc4	právo
udílet	udílet	k5eAaImF	udílet
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
věd	věda	k1gFnPc2	věda
doktorské	doktorský	k2eAgInPc1d1	doktorský
grady	grad	k1gInPc1	grad
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
ji	on	k3xPp3gFnSc4	on
postavil	postavit	k5eAaPmAgMnS	postavit
na	na	k7c4	na
úroveň	úroveň	k1gFnSc4	úroveň
universitě	universita	k1gFnSc3	universita
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
vyučovala	vyučovat	k5eAaImAgFnS	vyučovat
zprvu	zprvu	k6eAd1	zprvu
latinsky	latinsky	k6eAd1	latinsky
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
postupem	postup	k1gInSc7	postup
po	po	k7c6	po
příchodu	příchod	k1gInSc6	příchod
českých	český	k2eAgMnPc2d1	český
příslušníků	příslušník	k1gMnPc2	příslušník
řádu	řád	k1gInSc2	řád
se	se	k3xPyFc4	se
výuka	výuka	k1gFnSc1	výuka
počeštila	počeštit	k5eAaPmAgFnS	počeštit
<g/>
.	.	kIx.	.
</s>
<s>
Pražské	pražský	k2eAgNnSc1d1	Pražské
působení	působení	k1gNnSc1	působení
jezuitů	jezuita	k1gMnPc2	jezuita
se	se	k3xPyFc4	se
po	po	k7c6	po
polovině	polovina	k1gFnSc6	polovina
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
vybudováním	vybudování	k1gNnSc7	vybudování
druhé	druhý	k4xOgFnSc6	druhý
–	–	k?	–
novoměstské	novoměstský	k2eAgFnPc1d1	Novoměstská
koleje	kolej	k1gFnPc1	kolej
(	(	kIx(	(
<g/>
Profesní	profesní	k2eAgInSc1d1	profesní
dům	dům	k1gInSc1	dům
<g/>
)	)	kIx)	)
s	s	k7c7	s
kostelem	kostel	k1gInSc7	kostel
sv.	sv.	kA	sv.
Ignáce	Ignác	k1gMnSc2	Ignác
<g/>
,	,	kIx,	,
a	a	k8xC	a
třetí	třetí	k4xOgFnSc1	třetí
–	–	k?	–
malostranské	malostranský	k2eAgFnPc1d1	Malostranská
koleje	kolej	k1gFnPc1	kolej
při	při	k7c6	při
kostele	kostel	k1gInSc6	kostel
sv.	sv.	kA	sv.
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
<g/>
;	;	kIx,	;
pro	pro	k7c4	pro
vysokoškolské	vysokoškolský	k2eAgMnPc4d1	vysokoškolský
studenty	student	k1gMnPc4	student
sloužil	sloužit	k5eAaImAgMnS	sloužit
také	také	k6eAd1	také
staroměstský	staroměstský	k2eAgInSc4d1	staroměstský
konvikt	konvikt	k1gInSc4	konvikt
s	s	k7c7	s
kostelem	kostel	k1gInSc7	kostel
sv.	sv.	kA	sv.
Bartoloměje	Bartoloměj	k1gMnSc2	Bartoloměj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Postupně	postupně	k6eAd1	postupně
byly	být	k5eAaImAgFnP	být
zakládány	zakládat	k5eAaImNgFnP	zakládat
jezuitské	jezuitský	k2eAgFnPc1d1	jezuitská
koleje	kolej	k1gFnPc1	kolej
v	v	k7c6	v
Březnici	Březnice	k1gFnSc6	Březnice
<g/>
,	,	kIx,	,
Jičíně	Jičín	k1gInSc6	Jičín
<g/>
,	,	kIx,	,
Klatovech	Klatovy	k1gInPc6	Klatovy
(	(	kIx(	(
<g/>
1636	[number]	k4	1636
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Litoměřicích	Litoměřice	k1gInPc6	Litoměřice
<g/>
,	,	kIx,	,
Plzni	Plzeň	k1gFnSc6	Plzeň
<g/>
,	,	kIx,	,
Českém	český	k2eAgInSc6d1	český
Krumlově	Krumlov	k1gInSc6	Krumlov
<g/>
,	,	kIx,	,
Jindřichově	Jindřichův	k2eAgInSc6d1	Jindřichův
Hradci	Hradec	k1gInSc6	Hradec
<g/>
,	,	kIx,	,
Chomutově	Chomutov	k1gInSc6	Chomutov
<g/>
,	,	kIx,	,
Kladsku	Kladsko	k1gNnSc6	Kladsko
<g/>
,	,	kIx,	,
Telči	Telč	k1gFnSc6	Telč
<g/>
,	,	kIx,	,
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
,	,	kIx,	,
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
Jihlavě	Jihlava	k1gFnSc6	Jihlava
<g/>
,	,	kIx,	,
Znojmě	Znojmo	k1gNnSc6	Znojmo
<g/>
,	,	kIx,	,
Uherském	uherský	k2eAgNnSc6d1	Uherské
Hradišti	Hradiště	k1gNnSc6	Hradiště
a	a	k8xC	a
Opavě	Opava	k1gFnSc6	Opava
<g/>
.	.	kIx.	.
</s>
<s>
Jezuité	jezuita	k1gMnPc1	jezuita
se	se	k3xPyFc4	se
angažovali	angažovat	k5eAaBmAgMnP	angažovat
v	v	k7c6	v
kázání	kázání	k1gNnSc6	kázání
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
výuka	výuka	k1gFnSc1	výuka
nejprve	nejprve	k6eAd1	nejprve
latinské	latinský	k2eAgFnSc2d1	Latinská
a	a	k8xC	a
německé	německý	k2eAgFnSc2d1	německá
<g/>
,	,	kIx,	,
posléze	posléze	k6eAd1	posléze
také	také	k9	také
české	český	k2eAgFnSc3d1	Česká
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
na	na	k7c6	na
Bílé	bílý	k2eAgFnSc6d1	bílá
hoře	hora	k1gFnSc6	hora
byly	být	k5eAaImAgFnP	být
hlavní	hlavní	k2eAgFnSc7d1	hlavní
náplní	náplň	k1gFnSc7	náplň
činnosti	činnost	k1gFnSc2	činnost
řádu	řád	k1gInSc2	řád
v	v	k7c6	v
Českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
liturgie	liturgie	k1gFnSc2	liturgie
a	a	k8xC	a
školství	školství	k1gNnSc2	školství
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
hlavní	hlavní	k2eAgInPc1d1	hlavní
nástroje	nástroj	k1gInPc1	nástroj
potridentské	potridentský	k2eAgFnSc2d1	potridentská
rekatolizace	rekatolizace	k1gFnSc2	rekatolizace
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1622	[number]	k4	1622
byla	být	k5eAaImAgFnS	být
Tovaryšstvu	tovaryšstvo	k1gNnSc3	tovaryšstvo
Ježíšovu	Ježíšův	k2eAgInSc3d1	Ježíšův
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
svěřena	svěřit	k5eAaPmNgFnS	svěřit
výuka	výuka	k1gFnSc1	výuka
na	na	k7c6	na
Karlově	Karlův	k2eAgFnSc6d1	Karlova
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
činnost	činnost	k1gFnSc4	činnost
ve	v	k7c6	v
školství	školství	k1gNnSc6	školství
<g/>
,	,	kIx,	,
vědách	věda	k1gFnPc6	věda
<g/>
,	,	kIx,	,
umění	umění	k1gNnSc6	umění
a	a	k8xC	a
kultuře	kultura	k1gFnSc6	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
kolejích	kolej	k1gFnPc6	kolej
byla	být	k5eAaImAgFnS	být
zakládána	zakládán	k2eAgFnSc1d1	zakládána
studentská	studentská	k1gFnSc1	studentská
divadla	divadlo	k1gNnSc2	divadlo
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
při	při	k7c6	při
nich	on	k3xPp3gInPc6	on
organizována	organizován	k2eAgNnPc1d1	organizováno
náboženská	náboženský	k2eAgNnPc1d1	náboženské
bratrstva	bratrstvo	k1gNnPc1	bratrstvo
laiků	laik	k1gMnPc2	laik
<g/>
,	,	kIx,	,
zvaná	zvaný	k2eAgFnSc1d1	zvaná
mariánské	mariánský	k2eAgFnPc4d1	Mariánská
družiny	družina	k1gFnPc4	družina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
V	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
současných	současný	k2eAgMnPc2d1	současný
==	==	k?	==
</s>
</p>
<p>
<s>
Řádové	řádový	k2eAgInPc1d1	řádový
domy	dům	k1gInPc1	dům
České	český	k2eAgFnSc2d1	Česká
provincie	provincie	k1gFnSc2	provincie
Tovaryšstva	tovaryšstvo	k1gNnSc2	tovaryšstvo
Ježíšova	Ježíšův	k2eAgNnSc2d1	Ježíšovo
dnes	dnes	k6eAd1	dnes
existují	existovat	k5eAaImIp3nP	existovat
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
Děčíně	Děčín	k1gInSc6	Děčín
<g/>
,	,	kIx,	,
Olomouci	Olomouc	k1gFnSc6	Olomouc
a	a	k8xC	a
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
na	na	k7c6	na
moravských	moravský	k2eAgNnPc6d1	Moravské
poutních	poutní	k2eAgNnPc6d1	poutní
místech	místo	k1gNnPc6	místo
Hostýn	Hostýn	k1gInSc4	Hostýn
a	a	k8xC	a
Velehrad	Velehrad	k1gInSc4	Velehrad
<g/>
,	,	kIx,	,
společný	společný	k2eAgInSc4d1	společný
česko-slovenský	českolovenský	k2eAgInSc4d1	česko-slovenský
noviciát	noviciát	k1gInSc4	noviciát
pak	pak	k6eAd1	pak
existuje	existovat	k5eAaImIp3nS	existovat
v	v	k7c6	v
Ružomberku	Ružomberk	k1gInSc6	Ružomberk
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgFnPc4d3	nejvýznamnější
činnosti	činnost	k1gFnPc4	činnost
řádu	řád	k1gInSc2	řád
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
patří	patřit	k5eAaImIp3nS	patřit
kromě	kromě	k7c2	kromě
správy	správa	k1gFnSc2	správa
výše	výše	k1gFnSc1	výše
zmíněných	zmíněný	k2eAgNnPc2d1	zmíněné
poutních	poutní	k2eAgNnPc2d1	poutní
míst	místo	k1gNnPc2	místo
především	především	k9	především
pastorace	pastorace	k1gFnSc1	pastorace
studentů	student	k1gMnPc2	student
a	a	k8xC	a
mladých	mladý	k2eAgMnPc2d1	mladý
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
výchova	výchova	k1gFnSc1	výchova
a	a	k8xC	a
vzdělávání	vzdělávání	k1gNnSc1	vzdělávání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Děčíně	Děčín	k1gInSc6	Děčín
zahájila	zahájit	k5eAaPmAgFnS	zahájit
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2008	[number]	k4	2008
výuku	výuka	k1gFnSc4	výuka
řádová	řádový	k2eAgFnSc1d1	řádová
druhostupňová	druhostupňový	k2eAgFnSc1d1	druhostupňový
Křesťanská	křesťanský	k2eAgFnSc1d1	křesťanská
základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
Nativity	nativita	k1gFnSc2	nativita
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
jezuitská	jezuitský	k2eAgFnSc1d1	jezuitská
škola	škola	k1gFnSc1	škola
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
od	od	k7c2	od
zrušení	zrušení	k1gNnSc2	zrušení
církevních	církevní	k2eAgFnPc2d1	církevní
škol	škola	k1gFnPc2	škola
komunistickým	komunistický	k2eAgInSc7d1	komunistický
režimem	režim	k1gInSc7	režim
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
jednotliví	jednotlivý	k2eAgMnPc1d1	jednotlivý
členové	člen	k1gMnPc1	člen
řádu	řád	k1gInSc2	řád
působí	působit	k5eAaImIp3nP	působit
jako	jako	k9	jako
učitelé	učitel	k1gMnPc1	učitel
na	na	k7c6	na
různých	různý	k2eAgFnPc6d1	různá
školách	škola	k1gFnPc6	škola
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
teologických	teologický	k2eAgFnPc6d1	teologická
fakultách	fakulta	k1gFnPc6	fakulta
univerzit	univerzita	k1gFnPc2	univerzita
a	a	k8xC	a
Stojanově	stojanově	k6eAd1	stojanově
gymnáziu	gymnázium	k1gNnSc6	gymnázium
na	na	k7c6	na
Velehradě	Velehrad	k1gInSc6	Velehrad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Známí	známý	k2eAgMnPc1d1	známý
členové	člen	k1gMnPc1	člen
řádu	řád	k1gInSc2	řád
==	==	k?	==
</s>
</p>
<p>
<s>
Ignác	Ignác	k1gMnSc1	Ignác
z	z	k7c2	z
Loyoly	Loyola	k1gFnSc2	Loyola
(	(	kIx(	(
<g/>
1491	[number]	k4	1491
<g/>
–	–	k?	–
<g/>
1556	[number]	k4	1556
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Xaverský	xaverský	k2eAgMnSc1d1	xaverský
(	(	kIx(	(
<g/>
1506	[number]	k4	1506
<g/>
–	–	k?	–
<g/>
1552	[number]	k4	1552
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Petr	Petr	k1gMnSc1	Petr
Canisius	Canisius	k1gMnSc1	Canisius
(	(	kIx(	(
<g/>
1521	[number]	k4	1521
<g/>
–	–	k?	–
<g/>
1597	[number]	k4	1597
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Suárez	Suárez	k1gMnSc1	Suárez
(	(	kIx(	(
<g/>
1548	[number]	k4	1548
<g/>
-	-	kIx~	-
<g/>
1617	[number]	k4	1617
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
právník	právník	k1gMnSc1	právník
<g/>
,	,	kIx,	,
filosof	filosof	k1gMnSc1	filosof
<g/>
,	,	kIx,	,
teolog	teolog	k1gMnSc1	teolog
</s>
</p>
<p>
<s>
Matteo	Matteo	k6eAd1	Matteo
Ricci	Ricce	k1gMnPc1	Ricce
(	(	kIx(	(
<g/>
1552	[number]	k4	1552
<g/>
–	–	k?	–
<g/>
1610	[number]	k4	1610
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
misionář	misionář	k1gMnSc1	misionář
<g/>
,	,	kIx,	,
teolog	teolog	k1gMnSc1	teolog
</s>
</p>
<p>
<s>
Pedro	Pedra	k1gFnSc5	Pedra
Hurtado	Hurtada	k1gFnSc5	Hurtada
de	de	k?	de
Mendoza	Mendoz	k1gMnSc2	Mendoz
(	(	kIx(	(
<g/>
1578	[number]	k4	1578
<g/>
-	-	kIx~	-
<g/>
1641	[number]	k4	1641
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
filosof	filosof	k1gMnSc1	filosof
<g/>
,	,	kIx,	,
teolog	teolog	k1gMnSc1	teolog
</s>
</p>
<p>
<s>
Martin	Martin	k1gMnSc1	Martin
Středa	středa	k1gFnSc1	středa
(	(	kIx(	(
<g/>
1587	[number]	k4	1587
<g/>
–	–	k?	–
<g/>
1649	[number]	k4	1649
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Friedrich	Friedrich	k1gMnSc1	Friedrich
Spee	Spe	k1gFnSc2	Spe
(	(	kIx(	(
<g/>
1591	[number]	k4	1591
<g/>
–	–	k?	–
<g/>
1635	[number]	k4	1635
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bedřich	Bedřich	k1gMnSc1	Bedřich
Bridel	Bridlo	k1gNnPc2	Bridlo
(	(	kIx(	(
<g/>
1619	[number]	k4	1619
<g/>
–	–	k?	–
<g/>
1680	[number]	k4	1680
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Balbín	Balbín	k1gMnSc1	Balbín
(	(	kIx(	(
<g/>
1621	[number]	k4	1621
<g/>
–	–	k?	–
<g/>
1688	[number]	k4	1688
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Tanner	Tanner	k1gMnSc1	Tanner
(	(	kIx(	(
<g/>
1623	[number]	k4	1623
<g/>
-	-	kIx~	-
<g/>
1694	[number]	k4	1694
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jakub	Jakub	k1gMnSc1	Jakub
Kresa	Kres	k1gMnSc2	Kres
(	(	kIx(	(
<g/>
1648	[number]	k4	1648
<g/>
–	–	k?	–
<g/>
1715	[number]	k4	1715
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Samuel	Samuel	k1gMnSc1	Samuel
Fritz	Fritz	k1gMnSc1	Fritz
(	(	kIx(	(
<g/>
1654	[number]	k4	1654
<g/>
–	–	k?	–
<g/>
1725	[number]	k4	1725
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Georg	Georg	k1gMnSc1	Georg
Joseph	Joseph	k1gMnSc1	Joseph
Kamel	Kamel	k1gMnSc1	Kamel
(	(	kIx(	(
<g/>
1661	[number]	k4	1661
<g/>
–	–	k?	–
<g/>
1706	[number]	k4	1706
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Antonín	Antonín	k1gMnSc1	Antonín
Koniáš	Koniáš	k1gMnSc1	Koniáš
(	(	kIx(	(
<g/>
1691	[number]	k4	1691
<g/>
–	–	k?	–
<g/>
1760	[number]	k4	1760
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Dobrovský	Dobrovský	k1gMnSc1	Dobrovský
(	(	kIx(	(
<g/>
1753	[number]	k4	1753
<g/>
–	–	k?	–
<g/>
1829	[number]	k4	1829
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pierre	Pierr	k1gMnSc5	Pierr
Teilhard	Teilhard	k1gMnSc1	Teilhard
de	de	k?	de
Chardin	Chardina	k1gFnPc2	Chardina
(	(	kIx(	(
<g/>
1881	[number]	k4	1881
<g/>
–	–	k?	–
<g/>
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
paleontolog	paleontolog	k1gMnSc1	paleontolog
<g/>
,	,	kIx,	,
teolog	teolog	k1gMnSc1	teolog
</s>
</p>
<p>
<s>
Michal	Michal	k1gMnSc1	Michal
Pro	pro	k7c4	pro
(	(	kIx(	(
<g/>
1891	[number]	k4	1891
<g/>
–	–	k?	–
<g/>
1927	[number]	k4	1927
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Adolf	Adolf	k1gMnSc1	Adolf
Kajpr	Kajpr	k1gMnSc1	Kajpr
(	(	kIx(	(
<g/>
1902	[number]	k4	1902
<g/>
–	–	k?	–
<g/>
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Karl	Karl	k1gMnSc1	Karl
Rahner	Rahner	k1gMnSc1	Rahner
(	(	kIx(	(
<g/>
1904	[number]	k4	1904
<g/>
–	–	k?	–
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
filosof	filosof	k1gMnSc1	filosof
<g/>
,	,	kIx,	,
teolog	teolog	k1gMnSc1	teolog
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Šilhan	Šilhan	k1gMnSc1	Šilhan
(	(	kIx(	(
<g/>
1905	[number]	k4	1905
<g/>
–	–	k?	–
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Hans	Hans	k1gMnSc1	Hans
Urs	Urs	k1gFnSc2	Urs
von	von	k1gInSc1	von
Balthasar	Balthasar	k1gMnSc1	Balthasar
(	(	kIx(	(
<g/>
1905	[number]	k4	1905
<g/>
–	–	k?	–
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
teolog	teolog	k1gMnSc1	teolog
</s>
</p>
<p>
<s>
Avery	Aver	k1gInPc1	Aver
Dulles	Dullesa	k1gFnPc2	Dullesa
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
<g/>
–	–	k?	–
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
teolog	teolog	k1gMnSc1	teolog
</s>
</p>
<p>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Špidlík	Špidlík	k1gMnSc1	Špidlík
(	(	kIx(	(
<g/>
1919	[number]	k4	1919
<g/>
–	–	k?	–
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
teolog	teolog	k1gMnSc1	teolog
</s>
</p>
<p>
<s>
Anthony	Anthona	k1gFnPc1	Anthona
de	de	k?	de
Mello	Mello	k1gNnSc1	Mello
(	(	kIx(	(
<g/>
1931	[number]	k4	1931
<g/>
–	–	k?	–
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
duchovních	duchovní	k2eAgMnPc2d1	duchovní
děl	dělo	k1gNnPc2	dělo
</s>
</p>
<p>
<s>
papež	papež	k1gMnSc1	papež
František	František	k1gMnSc1	František
(	(	kIx(	(
<g/>
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Přenesený	přenesený	k2eAgInSc4d1	přenesený
význam	význam	k1gInSc4	význam
==	==	k?	==
</s>
</p>
<p>
<s>
Výraz	výraz	k1gInSc1	výraz
jezuita	jezuita	k1gMnSc1	jezuita
nebo	nebo	k8xC	nebo
jezovita	jezovita	k1gMnSc1	jezovita
<g/>
,	,	kIx,	,
jezuitský	jezuitský	k2eAgMnSc1d1	jezuitský
se	se	k3xPyFc4	se
též	též	k6eAd1	též
používá	používat	k5eAaImIp3nS	používat
jako	jako	k8xS	jako
hanlivé	hanlivý	k2eAgNnSc1d1	hanlivé
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
úskočné	úskočný	k2eAgNnSc4d1	úskočné
<g/>
,	,	kIx,	,
pokrytecké	pokrytecký	k2eAgNnSc4d1	pokrytecké
chování	chování	k1gNnSc4	chování
a	a	k8xC	a
zásadu	zásada	k1gFnSc4	zásada
<g/>
,	,	kIx,	,
že	že	k8xS	že
účel	účel	k1gInSc1	účel
světí	světit	k5eAaImIp3nS	světit
prostředky	prostředek	k1gInPc4	prostředek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Jezuité	jezuita	k1gMnPc1	jezuita
v	v	k7c6	v
beletrii	beletrie	k1gFnSc6	beletrie
==	==	k?	==
</s>
</p>
<p>
<s>
Známý	známý	k2eAgMnSc1d1	známý
je	být	k5eAaImIp3nS	být
epigram	epigram	k1gInSc1	epigram
ideového	ideový	k2eAgMnSc2d1	ideový
odpůrce	odpůrce	k1gMnSc2	odpůrce
jezuitů	jezuita	k1gMnPc2	jezuita
Karla	Karel	k1gMnSc4	Karel
Havlíčka	Havlíček	k1gMnSc4	Havlíček
<g/>
:	:	kIx,	:
Českých	český	k2eAgFnPc2d1	Česká
knížek	knížka	k1gFnPc2	knížka
hubitelé	hubitel	k1gMnPc1	hubitel
lítí	lítit	k5eAaImIp3nP	lítit
<g/>
:	:	kIx,	:
plesnivina	plesnivina	k1gFnSc1	plesnivina
<g/>
,	,	kIx,	,
moli	mol	k1gMnPc1	mol
<g/>
,	,	kIx,	,
jesoviti	jesovit	k5eAaImF	jesovit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zápornou	záporný	k2eAgFnSc4d1	záporná
roli	role	k1gFnSc4	role
hrají	hrát	k5eAaImIp3nP	hrát
jezuité	jezuita	k1gMnPc1	jezuita
také	také	k9	také
v	v	k7c6	v
románech	román	k1gInPc6	román
Aloise	Alois	k1gMnSc4	Alois
Jiráska	Jirásek	k1gMnSc2	Jirásek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jiný	jiný	k2eAgInSc1d1	jiný
pohled	pohled	k1gInSc1	pohled
na	na	k7c6	na
historii	historie	k1gFnSc6	historie
nabízí	nabízet	k5eAaImIp3nS	nabízet
Jiří	Jiří	k1gMnSc1	Jiří
Šotola	Šotola	k1gMnSc1	Šotola
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
románu	román	k1gInSc6	román
Tovaryšstvo	tovaryšstvo	k1gNnSc4	tovaryšstvo
Ježíšovo	Ježíšův	k2eAgNnSc1d1	Ježíšovo
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
zaobírá	zaobírat	k5eAaImIp3nS	zaobírat
bezvýznamnou	bezvýznamný	k2eAgFnSc7d1	bezvýznamná
rolí	role	k1gFnSc7	role
jednotlivce	jednotlivec	k1gMnSc2	jednotlivec
v	v	k7c6	v
soukolí	soukolí	k1gNnSc6	soukolí
moci	moc	k1gFnSc2	moc
<g/>
.	.	kIx.	.
</s>
<s>
Popisuje	popisovat	k5eAaImIp3nS	popisovat
působení	působení	k1gNnSc4	působení
jezuitského	jezuitský	k2eAgInSc2d1	jezuitský
řádu	řád	k1gInSc2	řád
v	v	k7c6	v
Českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Čtivé	čtivý	k2eAgNnSc1d1	čtivé
zpracování	zpracování	k1gNnSc1	zpracování
oblíbených	oblíbený	k2eAgNnPc2d1	oblíbené
klišé	klišé	k1gNnPc2	klišé
z	z	k7c2	z
lidového	lidový	k2eAgNnSc2d1	lidové
čtiva	čtivo	k1gNnSc2	čtivo
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
je	být	k5eAaImIp3nS	být
podáno	podat	k5eAaPmNgNnS	podat
v	v	k7c6	v
Krvavém	krvavý	k2eAgInSc6d1	krvavý
románu	román	k1gInSc6	román
Josefa	Josef	k1gMnSc2	Josef
Váchala	Váchal	k1gMnSc2	Váchal
(	(	kIx(	(
<g/>
zfilmováno	zfilmovat	k5eAaPmNgNnS	zfilmovat
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Jezuitské	jezuitský	k2eAgNnSc1d1	jezuitské
divadlo	divadlo	k1gNnSc1	divadlo
</s>
</p>
<p>
<s>
Jezuitské	jezuitský	k2eAgFnPc1d1	jezuitská
koleje	kolej	k1gFnPc1	kolej
</s>
</p>
<p>
<s>
Jezuitská	jezuitský	k2eAgFnSc1d1	jezuitská
redukce	redukce	k1gFnSc1	redukce
</s>
</p>
<p>
<s>
Jezuitské	jezuitský	k2eAgFnPc1d1	jezuitská
univerzity	univerzita	k1gFnPc1	univerzita
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
BÍLEK	Bílek	k1gMnSc1	Bílek
<g/>
,	,	kIx,	,
Tomáš	Tomáš	k1gMnSc1	Tomáš
Václav	Václav	k1gMnSc1	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Tovaryšstvo	tovaryšstvo	k1gNnSc1	tovaryšstvo
Ježíšovo	Ježíšův	k2eAgNnSc1d1	Ježíšovo
a	a	k8xC	a
působení	působení	k1gNnSc1	působení
jeho	jeho	k3xOp3gMnPc2	jeho
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
království	království	k1gNnSc2	království
Českého	český	k2eAgNnSc2d1	české
vůbec	vůbec	k9	vůbec
a	a	k8xC	a
v	v	k7c6	v
kollegiu	kollegium	k1gNnSc6	kollegium
Pražském	pražský	k2eAgInSc6d1	pražský
u	u	k7c2	u
sv.	sv.	kA	sv.
Klimenta	Kliment	k1gMnSc4	Kliment
zvláště	zvláště	k6eAd1	zvláště
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
vl	vl	k?	vl
<g/>
.	.	kIx.	.
n.	n.	k?	n.
<g/>
,	,	kIx,	,
1873	[number]	k4	1873
<g/>
.	.	kIx.	.
44	[number]	k4	44
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
BÍLEK	Bílek	k1gMnSc1	Bílek
<g/>
,	,	kIx,	,
Tomáš	Tomáš	k1gMnSc1	Tomáš
Václav	Václav	k1gMnSc1	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Statky	statek	k1gInPc1	statek
a	a	k8xC	a
jmění	jmění	k1gNnPc1	jmění
kollejí	kollet	k5eAaImIp3nP	kollet
jesuitských	jesuitský	k2eAgMnPc2d1	jesuitský
<g/>
,	,	kIx,	,
klášterů	klášter	k1gInPc2	klášter
<g/>
,	,	kIx,	,
kostelů	kostel	k1gInPc2	kostel
<g/>
,	,	kIx,	,
bratrstev	bratrstvo	k1gNnPc2	bratrstvo
a	a	k8xC	a
jiných	jiný	k2eAgInPc2d1	jiný
ústavů	ústav	k1gInPc2	ústav
v	v	k7c6	v
království	království	k1gNnSc6	království
Českém	český	k2eAgMnSc6d1	český
od	od	k7c2	od
císaře	císař	k1gMnSc4	císař
Josefa	Josef	k1gMnSc2	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
zrušených	zrušený	k2eAgFnPc2d1	zrušená
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Bačkovský	Bačkovský	k2eAgMnSc1d1	Bačkovský
<g/>
,	,	kIx,	,
1893	[number]	k4	1893
<g/>
.	.	kIx.	.
472	[number]	k4	472
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
nedostupný	dostupný	k2eNgInSc1d1	nedostupný
zdroj	zdroj	k1gInSc1	zdroj
<g/>
]	]	kIx)	]
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
(	(	kIx(	(
<g/>
archive	archiv	k1gInSc5	archiv
org	org	k?	org
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
BÍLEK	Bílek	k1gMnSc1	Bílek
<g/>
,	,	kIx,	,
Tomáš	Tomáš	k1gMnSc1	Tomáš
Václav	Václav	k1gMnSc1	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
řádu	řád	k1gInSc2	řád
Tovaryšstva	tovaryšstvo	k1gNnSc2	tovaryšstvo
Ježíšova	Ježíšův	k2eAgNnSc2d1	Ježíšovo
a	a	k8xC	a
působení	působení	k1gNnSc1	působení
jeho	jeho	k3xOp3gFnSc2	jeho
vůbec	vůbec	k9	vůbec
a	a	k9	a
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
království	království	k1gNnSc2	království
Českého	český	k2eAgNnSc2d1	české
zvláště	zvláště	k6eAd1	zvláště
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Frant	Frant	k1gMnSc1	Frant
<g/>
.	.	kIx.	.
</s>
<s>
Bačkovský	Bačkovský	k2eAgMnSc1d1	Bačkovský
<g/>
,	,	kIx,	,
1896	[number]	k4	1896
<g/>
.	.	kIx.	.
550	[number]	k4	550
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
BOBKOVÁ-VALENTOVÁ	BOBKOVÁ-VALENTOVÁ	k?	BOBKOVÁ-VALENTOVÁ
<g/>
,	,	kIx,	,
Kateřina	Kateřina	k1gFnSc1	Kateřina
<g/>
.	.	kIx.	.
</s>
<s>
Každodenní	každodenní	k2eAgInSc1d1	každodenní
život	život	k1gInSc1	život
učitele	učitel	k1gMnSc2	učitel
a	a	k8xC	a
žáka	žák	k1gMnSc2	žák
jezuitského	jezuitský	k2eAgNnSc2d1	jezuitské
gymnázia	gymnázium	k1gNnSc2	gymnázium
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Karolinum	Karolinum	k1gNnSc1	Karolinum
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
280	[number]	k4	280
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
246	[number]	k4	246
<g/>
-	-	kIx~	-
<g/>
1082	[number]	k4	1082
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
CATALANO	CATALANO	kA	CATALANO
<g/>
,	,	kIx,	,
Alessandro	Alessandra	k1gFnSc5	Alessandra
<g/>
.	.	kIx.	.
</s>
<s>
Zápas	zápas	k1gInSc1	zápas
o	o	k7c6	o
svědomí	svědomí	k1gNnSc6	svědomí
<g/>
:	:	kIx,	:
kardinál	kardinál	k1gMnSc1	kardinál
Arnošt	Arnošt	k1gMnSc1	Arnošt
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
z	z	k7c2	z
Harrachu	Harrach	k1gInSc2	Harrach
(	(	kIx(	(
<g/>
1598	[number]	k4	1598
<g/>
-	-	kIx~	-
<g/>
1667	[number]	k4	1667
<g/>
)	)	kIx)	)
a	a	k8xC	a
protireformace	protireformace	k1gFnSc1	protireformace
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
637	[number]	k4	637
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
942	[number]	k4	942
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
CEMUS	CEMUS	kA	CEMUS
<g/>
,	,	kIx,	,
Petronilla	Petronillo	k1gNnSc2	Petronillo
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Bohemia	bohemia	k1gFnSc1	bohemia
Jesuitica	Jesuitica	k1gFnSc1	Jesuitica
1556	[number]	k4	1556
<g/>
-	-	kIx~	-
<g/>
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Karolinum	Karolinum	k1gNnSc1	Karolinum
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
1512	[number]	k4	1512
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
246	[number]	k4	246
<g/>
-	-	kIx~	-
<g/>
1755	[number]	k4	1755
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ČERNÝ	Černý	k1gMnSc1	Černý
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
;	;	kIx,	;
HAVLÍK	Havlík	k1gMnSc1	Havlík
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
M.	M.	kA	M.
Jezuité	jezuita	k1gMnPc1	jezuita
a	a	k8xC	a
mor	mor	k1gInSc1	mor
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
214	[number]	k4	214
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
967	[number]	k4	967
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ČORNEJOVÁ	ČORNEJOVÁ	kA	ČORNEJOVÁ
<g/>
,	,	kIx,	,
Ivana	Ivana	k1gFnSc1	Ivana
<g/>
.	.	kIx.	.
</s>
<s>
Tovaryšstvo	tovaryšstvo	k1gNnSc1	tovaryšstvo
Ježíšovo	Ježíšův	k2eAgNnSc1d1	Ježíšovo
:	:	kIx,	:
jezuité	jezuita	k1gMnPc1	jezuita
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Hart	Hart	k1gMnSc1	Hart
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
264	[number]	k4	264
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86529	[number]	k4	86529
<g/>
-	-	kIx~	-
<g/>
30	[number]	k4	30
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HERZIG	HERZIG	kA	HERZIG
<g/>
,	,	kIx,	,
Arno	Arno	k6eAd1	Arno
<g/>
.	.	kIx.	.
</s>
<s>
Die	Die	k?	Die
Jesuiten	Jesuiten	k2eAgInSc1d1	Jesuiten
im	im	k?	im
feudalen	feudalen	k2eAgInSc4d1	feudalen
Nexus	nexus	k1gInSc4	nexus
<g/>
:	:	kIx,	:
Der	drát	k5eAaImRp2nS	drát
Aufstand	Aufstand	k1gInSc1	Aufstand
der	drát	k5eAaImRp2nS	drát
Ordensuntertanen	Ordensuntertanen	k1gInSc1	Ordensuntertanen
in	in	k?	in
der	drát	k5eAaImRp2nS	drát
Grafschaft	Grafschaft	k1gMnSc1	Grafschaft
Glatz	Glatz	k1gMnSc1	Glatz
im	im	k?	im
ausgehenden	ausgehendno	k1gNnPc2	ausgehendno
17	[number]	k4	17
<g/>
.	.	kIx.	.
</s>
<s>
Jahrhundert	Jahrhundert	k1gMnSc1	Jahrhundert
<g/>
.	.	kIx.	.
</s>
<s>
Prague	Prague	k1gFnSc1	Prague
Papers	Papersa	k1gFnPc2	Papersa
on	on	k3xPp3gMnSc1	on
the	the	k?	the
History	Histor	k1gInPc4	Histor
of	of	k?	of
International	International	k1gFnSc2	International
Relations	Relationsa	k1gFnPc2	Relationsa
<g/>
.	.	kIx.	.
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
,	,	kIx,	,
s.	s.	k?	s.
41	[number]	k4	41
<g/>
-	-	kIx~	-
<g/>
62	[number]	k4	62
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85899	[number]	k4	85899
<g/>
-	-	kIx~	-
<g/>
73	[number]	k4	73
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
JACKOVÁ	JACKOVÁ	kA	JACKOVÁ
<g/>
,	,	kIx,	,
Magdaléna	Magdaléna	k1gFnSc1	Magdaléna
<g/>
:	:	kIx,	:
Divadlo	divadlo	k1gNnSc1	divadlo
jako	jako	k8xC	jako
škola	škola	k1gFnSc1	škola
ctnosti	ctnost	k1gFnSc2	ctnost
a	a	k8xC	a
zbožnosti	zbožnost	k1gFnSc2	zbožnost
<g/>
.	.	kIx.	.
</s>
<s>
Jezuitské	jezuitský	k2eAgNnSc1d1	jezuitské
školské	školský	k2eAgNnSc1d1	školské
drama	drama	k1gNnSc1	drama
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Univerzita	univerzita	k1gFnSc1	univerzita
Karlova	Karlův	k2eAgFnSc1d1	Karlova
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
Filozofická	filozofický	k2eAgFnSc1d1	filozofická
fakulta	fakulta	k1gFnSc1	fakulta
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978-80-7308-360-1	[number]	k4	978-80-7308-360-1
</s>
</p>
<p>
<s>
ASCHENBRENNER	ASCHENBRENNER	kA	ASCHENBRENNER
<g/>
,	,	kIx,	,
Vít	Vít	k1gMnSc1	Vít
<g/>
:	:	kIx,	:
Hudebně-liturgický	Hudebněiturgický	k2eAgInSc1d1	Hudebně-liturgický
provoz	provoz	k1gInSc1	provoz
jezuitské	jezuitský	k2eAgFnSc2d1	jezuitská
koleje	kolej	k1gFnSc2	kolej
v	v	k7c6	v
Klatovech	Klatovy	k1gInPc6	Klatovy
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Západočeská	západočeský	k2eAgFnSc1d1	Západočeská
univerzita	univerzita	k1gFnSc1	univerzita
Plzeň	Plzeň	k1gFnSc1	Plzeň
–	–	k?	–
Scriptorium	Scriptorium	k1gNnSc1	Scriptorium
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Plzeň	Plzeň	k1gFnSc1	Plzeň
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
412	[number]	k4	412
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978-80-7043-952-4	[number]	k4	978-80-7043-952-4
(	(	kIx(	(
<g/>
recenze	recenze	k1gFnPc1	recenze
na	na	k7c4	na
knihu	kniha	k1gFnSc4	kniha
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
LIŠČÁK	LIŠČÁK	kA	LIŠČÁK
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
<g/>
,	,	kIx,	,
Mezi	mezi	k7c7	mezi
tolerancí	tolerance	k1gFnSc7	tolerance
a	a	k8xC	a
intolerancí	intolerance	k1gFnSc7	intolerance
<g/>
:	:	kIx,	:
První	první	k4xOgFnPc1	první
dvě	dva	k4xCgFnPc1	dva
století	století	k1gNnSc2	století
novověkých	novověký	k2eAgFnPc2d1	novověká
katolických	katolický	k2eAgFnPc2d1	katolická
misií	misie	k1gFnPc2	misie
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
2017	[number]	k4	2017
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-200-2680-4	[number]	k4	978-80-200-2680-4
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Tovaryšstvo	tovaryšstvo	k1gNnSc1	tovaryšstvo
Ježíšovo	Ježíšův	k2eAgNnSc1d1	Ježíšovo
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Tovaryšstvo	tovaryšstvo	k1gNnSc1	tovaryšstvo
Ježíšovo	Ježíšův	k2eAgNnSc1d1	Ježíšovo
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
jezuita	jezuita	k1gMnSc1	jezuita
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Česká	český	k2eAgFnSc1d1	Česká
provincie	provincie	k1gFnSc1	provincie
jezuitů	jezuita	k1gMnPc2	jezuita
</s>
</p>
<p>
<s>
Jezuité	jezuita	k1gMnPc1	jezuita
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
</s>
</p>
<p>
<s>
Jezuité	jezuita	k1gMnPc1	jezuita
(	(	kIx(	(
<g/>
Z	z	k7c2	z
cyklu	cyklus	k1gInSc2	cyklus
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
o	o	k7c6	o
církevních	církevní	k2eAgInPc6d1	církevní
řádech	řád	k1gInPc6	řád
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
"	"	kIx"	"
<g/>
Zasvěcení	zasvěcení	k1gNnSc1	zasvěcení
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
–	–	k?	–
video	video	k1gNnSc1	video
on-line	onin	k1gInSc5	on-lin
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
ČT	ČT	kA	ČT
</s>
</p>
<p>
<s>
Jezuitský	jezuitský	k2eAgInSc1d1	jezuitský
řád	řád	k1gInSc1	řád
–	–	k?	–
video	video	k1gNnSc1	video
z	z	k7c2	z
cyklu	cyklus	k1gInSc2	cyklus
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
Historický	historický	k2eAgInSc1d1	historický
magazín	magazín	k1gInSc1	magazín
</s>
</p>
