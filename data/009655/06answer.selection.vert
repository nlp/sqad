<s>
Tovaryšstvo	tovaryšstvo	k1gNnSc1	tovaryšstvo
Ježíšovo	Ježíšův	k2eAgNnSc1d1	Ježíšovo
je	být	k5eAaImIp3nS	být
mužský	mužský	k2eAgInSc4d1	mužský
řád	řád	k1gInSc4	řád
bez	bez	k7c2	bez
paralelní	paralelní	k2eAgFnSc2d1	paralelní
ženské	ženský	k2eAgFnSc2d1	ženská
větve	větev	k1gFnSc2	větev
<g/>
,	,	kIx,	,
určitou	určitý	k2eAgFnSc4d1	určitá
podobnost	podobnost	k1gFnSc4	podobnost
ale	ale	k8xC	ale
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaBmF	nalézt
u	u	k7c2	u
samostatných	samostatný	k2eAgFnPc2d1	samostatná
ženských	ženská	k1gFnPc2	ženská
řeholních	řeholní	k2eAgNnPc2d1	řeholní
společenství	společenství	k1gNnPc2	společenství
<g/>
.	.	kIx.	.
</s>
