<s>
Dámské	dámský	k2eAgFnPc4d1	dámská
společenské	společenský	k2eAgFnPc4d1	společenská
boty	bota	k1gFnPc4	bota
mívají	mívat	k5eAaImIp3nP	mívat
podpatky	podpatek	k1gInPc1	podpatek
vysoké	vysoká	k1gFnSc2	vysoká
<g/>
,	,	kIx,	,
tato	tento	k3xDgFnSc1	tento
obuv	obuv	k1gFnSc1	obuv
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
podle	podle	k7c2	podle
svého	svůj	k3xOyFgInSc2	svůj
tvaru	tvar	k1gInSc2	tvar
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
podobá	podobat	k5eAaImIp3nS	podobat
loďce	loďka	k1gFnSc3	loďka
<g/>
,	,	kIx,	,
nazývá	nazývat	k5eAaImIp3nS	nazývat
"	"	kIx"	"
<g/>
lodičky	lodička	k1gFnPc4	lodička
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
