<s>
Kubismus	kubismus	k1gInSc1	kubismus
je	být	k5eAaImIp3nS	být
avantgardní	avantgardní	k2eAgNnSc4d1	avantgardní
umělecké	umělecký	k2eAgNnSc4d1	umělecké
hnutí	hnutí	k1gNnSc4	hnutí
přelomu	přelom	k1gInSc2	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
<g/>
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
pojímá	pojímat	k5eAaImIp3nS	pojímat
výtvarné	výtvarný	k2eAgNnSc1d1	výtvarné
umění	umění	k1gNnSc1	umění
revolučním	revoluční	k2eAgInSc7d1	revoluční
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
