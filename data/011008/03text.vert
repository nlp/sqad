<p>
<s>
Kubismus	kubismus	k1gInSc1	kubismus
je	být	k5eAaImIp3nS	být
avantgardní	avantgardní	k2eAgNnSc4d1	avantgardní
umělecké	umělecký	k2eAgNnSc4d1	umělecké
hnutí	hnutí	k1gNnSc4	hnutí
přelomu	přelom	k1gInSc2	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
<g/>
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
pojímá	pojímat	k5eAaImIp3nS	pojímat
výtvarné	výtvarný	k2eAgNnSc1d1	výtvarné
umění	umění	k1gNnSc1	umění
revolučním	revoluční	k2eAgInSc7d1	revoluční
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Princip	princip	k1gInSc1	princip
kubismu	kubismus	k1gInSc2	kubismus
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
prostorové	prostorový	k2eAgFnSc6d1	prostorová
koncepci	koncepce	k1gFnSc6	koncepce
díla	dílo	k1gNnSc2	dílo
<g/>
,	,	kIx,	,
předmět	předmět	k1gInSc1	předmět
nezobrazuje	zobrazovat	k5eNaImIp3nS	zobrazovat
jen	jen	k9	jen
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
úhlu	úhel	k1gInSc2	úhel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
úhlů	úhel	k1gInPc2	úhel
současně	současně	k6eAd1	současně
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
znamenal	znamenat	k5eAaImAgInS	znamenat
i	i	k9	i
obrovskou	obrovský	k2eAgFnSc4d1	obrovská
změnu	změna	k1gFnSc4	změna
v	v	k7c6	v
přístupu	přístup	k1gInSc6	přístup
ke	k	k7c3	k
skutečnosti	skutečnost	k1gFnSc3	skutečnost
<g/>
,	,	kIx,	,
nejen	nejen	k6eAd1	nejen
ve	v	k7c6	v
vidění	vidění	k1gNnSc6	vidění
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
především	především	k9	především
v	v	k7c6	v
práci	práce	k1gFnSc6	práce
s	s	k7c7	s
realitou	realita	k1gFnSc7	realita
<g/>
.	.	kIx.	.
</s>
<s>
Zobrazovaný	zobrazovaný	k2eAgInSc1d1	zobrazovaný
předmět	předmět	k1gInSc1	předmět
byl	být	k5eAaImAgInS	být
rozkládán	rozkládat	k5eAaImNgInS	rozkládat
až	až	k9	až
na	na	k7c4	na
nejjednodušší	jednoduchý	k2eAgInPc4d3	nejjednodušší
geometrické	geometrický	k2eAgInPc4d1	geometrický
tvary	tvar	k1gInPc4	tvar
(	(	kIx(	(
<g/>
především	především	k9	především
krychle	krychle	k1gFnSc1	krychle
-	-	kIx~	-
latinsky	latinsky	k6eAd1	latinsky
cubus	cubus	k1gInSc1	cubus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
pak	pak	k6eAd1	pak
pomocí	pomocí	k7c2	pomocí
fantazie	fantazie	k1gFnSc2	fantazie
skládány	skládat	k5eAaImNgInP	skládat
do	do	k7c2	do
obrazu	obraz	k1gInSc2	obraz
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
zobrazené	zobrazený	k2eAgInPc1d1	zobrazený
předměty	předmět	k1gInPc1	předmět
působí	působit	k5eAaImIp3nP	působit
dojmem	dojem	k1gInSc7	dojem
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
deformované	deformovaný	k2eAgFnPc1d1	deformovaná
a	a	k8xC	a
objevují	objevovat	k5eAaImIp3nP	objevovat
se	se	k3xPyFc4	se
zároveň	zároveň	k6eAd1	zároveň
z	z	k7c2	z
několika	několik	k4yIc2	několik
pohledů	pohled	k1gInPc2	pohled
<g/>
.	.	kIx.	.
</s>
<s>
Problém	problém	k1gInSc1	problém
nastal	nastat	k5eAaPmAgInS	nastat
u	u	k7c2	u
předmětů	předmět	k1gInPc2	předmět
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
vzájemném	vzájemný	k2eAgInSc6d1	vzájemný
vztahu	vztah	k1gInSc6	vztah
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
pak	pak	k6eAd1	pak
vznikalo	vznikat	k5eAaImAgNnS	vznikat
mnoho	mnoho	k4c1	mnoho
průhledů	průhled	k1gInPc2	průhled
s	s	k7c7	s
neobvyklými	obvyklý	k2eNgInPc7d1	neobvyklý
úhly	úhel	k1gInPc7	úhel
pohledu	pohled	k1gInSc2	pohled
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kubismus	kubismus	k1gInSc1	kubismus
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgInS	snažit
počítat	počítat	k5eAaImF	počítat
i	i	k9	i
se	s	k7c7	s
čtvrtou	čtvrtý	k4xOgFnSc7	čtvrtý
dimenzí	dimenze	k1gFnSc7	dimenze
(	(	kIx(	(
<g/>
ovlivněno	ovlivněn	k2eAgNnSc1d1	ovlivněno
A.	A.	kA	A.
Einsteinem	Einstein	k1gMnSc7	Einstein
<g/>
)	)	kIx)	)
a	a	k8xC	a
s	s	k7c7	s
nekonečnem	nekonečno	k1gNnSc7	nekonečno
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
kubismus	kubismus	k1gInSc1	kubismus
musel	muset	k5eAaImAgInS	muset
řešit	řešit	k5eAaImF	řešit
nové	nový	k2eAgInPc4d1	nový
problémy	problém	k1gInPc4	problém
perspektivy	perspektiva	k1gFnSc2	perspektiva
a	a	k8xC	a
vytvářel	vytvářet	k5eAaImAgInS	vytvářet
nové	nový	k2eAgInPc4d1	nový
prostorové	prostorový	k2eAgInPc4d1	prostorový
vztahy	vztah	k1gInPc4	vztah
mezi	mezi	k7c7	mezi
předměty	předmět	k1gInPc7	předmět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kubismus	kubismus	k1gInSc1	kubismus
měl	mít	k5eAaImAgInS	mít
velký	velký	k2eAgInSc4d1	velký
dopad	dopad	k1gInSc4	dopad
na	na	k7c4	na
umělce	umělec	k1gMnPc4	umělec
prvních	první	k4xOgNnPc6	první
desetiletí	desetiletí	k1gNnPc4	desetiletí
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
ovlivnil	ovlivnit	k5eAaPmAgMnS	ovlivnit
(	(	kIx(	(
<g/>
ať	ať	k8xC	ať
přímo	přímo	k6eAd1	přímo
či	či	k8xC	či
nepřímo	přímo	k6eNd1	přímo
<g/>
)	)	kIx)	)
vývoji	vývoj	k1gInSc6	vývoj
nových	nový	k2eAgInPc2d1	nový
uměleckých	umělecký	k2eAgInPc2d1	umělecký
stylů	styl	k1gInPc2	styl
(	(	kIx(	(
<g/>
futurismus	futurismus	k1gInSc1	futurismus
<g/>
,	,	kIx,	,
konstruktivismus	konstruktivismus	k1gInSc1	konstruktivismus
a	a	k8xC	a
expresionismus	expresionismus	k1gInSc1	expresionismus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
lokalitách	lokalita	k1gFnPc6	lokalita
získal	získat	k5eAaPmAgInS	získat
charakter	charakter	k1gInSc1	charakter
uměleckého	umělecký	k2eAgInSc2d1	umělecký
slohu	sloh	k1gInSc2	sloh
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
a	a	k8xC	a
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Protože	protože	k8xS	protože
kubismus	kubismus	k1gInSc1	kubismus
byl	být	k5eAaImAgInS	být
směr	směr	k1gInSc4	směr
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
literatura	literatura	k1gFnSc1	literatura
neměla	mít	k5eNaImAgFnS	mít
velký	velký	k2eAgInSc4d1	velký
vliv	vliv	k1gInSc4	vliv
<g/>
,	,	kIx,	,
nemá	mít	k5eNaImIp3nS	mít
žádný	žádný	k3yNgInSc4	žádný
přesně	přesně	k6eAd1	přesně
daný	daný	k2eAgInSc4d1	daný
počátek	počátek	k1gInSc4	počátek
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
nevyšel	vyjít	k5eNaPmAgMnS	vyjít
mu	on	k3xPp3gMnSc3	on
žádný	žádný	k3yNgInSc4	žádný
manifest	manifest	k1gInSc4	manifest
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgInPc4	první
impulsy	impuls	k1gInPc4	impuls
<g/>
,	,	kIx,	,
kterými	který	k3yQgInPc7	který
byl	být	k5eAaImAgInS	být
kubismus	kubismus	k1gInSc1	kubismus
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
<g/>
,	,	kIx,	,
můžeme	moct	k5eAaImIp1nP	moct
vysledovat	vysledovat	k5eAaPmF	vysledovat
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1906	[number]	k4	1906
u	u	k7c2	u
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
během	během	k7c2	během
svých	svůj	k3xOyFgFnPc2	svůj
studií	studie	k1gFnPc2	studie
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
vytvářeli	vytvářet	k5eAaImAgMnP	vytvářet
Georges	Georges	k1gInSc4	Georges
Braque	Braqu	k1gFnSc2	Braqu
(	(	kIx(	(
<g/>
Francouz	Francouz	k1gMnSc1	Francouz
<g/>
)	)	kIx)	)
a	a	k8xC	a
Pablo	Pablo	k1gNnSc1	Pablo
Picasso	Picassa	k1gFnSc5	Picassa
(	(	kIx(	(
<g/>
Španěl	Španěl	k1gMnSc1	Španěl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
byla	být	k5eAaImAgFnS	být
jejich	jejich	k3xOp3gFnSc1	jejich
tvorba	tvorba	k1gFnSc1	tvorba
na	na	k7c6	na
sobě	se	k3xPyFc3	se
nezávislá	závislý	k2eNgFnSc1d1	nezávislá
<g/>
,	,	kIx,	,
oba	dva	k4xCgMnPc1	dva
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
Paul	Paul	k1gMnSc1	Paul
Cézanne	Cézann	k1gInSc5	Cézann
a	a	k8xC	a
africké	africký	k2eAgNnSc4d1	africké
domorodé	domorodý	k2eAgNnSc4d1	domorodé
umění	umění	k1gNnSc4	umění
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1907	[number]	k4	1907
začali	začít	k5eAaPmAgMnP	začít
pracovat	pracovat	k5eAaImF	pracovat
společně	společně	k6eAd1	společně
a	a	k8xC	a
zůstali	zůstat	k5eAaPmAgMnP	zůstat
spolu	spolu	k6eAd1	spolu
až	až	k6eAd1	až
do	do	k7c2	do
vypuknutí	vypuknutí	k1gNnSc2	vypuknutí
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
(	(	kIx(	(
<g/>
1914	[number]	k4	1914
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Termínu	termín	k1gInSc2	termín
"	"	kIx"	"
<g/>
kubismus	kubismus	k1gInSc1	kubismus
<g/>
"	"	kIx"	"
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
použil	použít	k5eAaPmAgMnS	použít
francouzský	francouzský	k2eAgMnSc1d1	francouzský
kritik	kritik	k1gMnSc1	kritik
umění	umění	k1gNnSc2	umění
Louis	Louis	k1gMnSc1	Louis
Vauxcelles	Vauxcelles	k1gMnSc1	Vauxcelles
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1908	[number]	k4	1908
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
divný	divný	k2eAgInSc1d1	divný
cubiques	cubiques	k1gInSc1	cubiques
<g/>
"	"	kIx"	"
=	=	kIx~	=
krychle	krychle	k1gFnPc1	krychle
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
použití	použití	k1gNnSc1	použití
bylo	být	k5eAaImAgNnS	být
myšleno	myslet	k5eAaImNgNnS	myslet
hanlivě	hanlivě	k6eAd1	hanlivě
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
termín	termín	k1gInSc1	termín
přijat	přijmout	k5eAaPmNgInS	přijmout
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gMnPc3	jeho
zakladatelům	zakladatel	k1gMnPc3	zakladatel
nelíbil	líbit	k5eNaImAgInS	líbit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kubistické	kubistický	k2eAgNnSc1d1	kubistické
hnutí	hnutí	k1gNnSc1	hnutí
se	se	k3xPyFc4	se
i	i	k9	i
díky	díky	k7c3	díky
podpoře	podpora	k1gFnSc3	podpora
galeristy	galerista	k1gMnSc2	galerista
D.	D.	kA	D.
<g/>
-	-	kIx~	-
<g/>
H.	H.	kA	H.
Kahnweilera	Kahnweiler	k1gMnSc2	Kahnweiler
<g/>
,	,	kIx,	,
rychle	rychle	k6eAd1	rychle
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
a	a	k8xC	a
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1910	[number]	k4	1910
lze	lze	k6eAd1	lze
mluvit	mluvit	k5eAaImF	mluvit
o	o	k7c6	o
kubistické	kubistický	k2eAgFnSc6d1	kubistická
škole	škola	k1gFnSc6	škola
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
tvorby	tvorba	k1gFnSc2	tvorba
dělíme	dělit	k5eAaImIp1nP	dělit
kubismus	kubismus	k1gInSc4	kubismus
na	na	k7c4	na
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Pre-cubismus	Preubismus	k1gInSc1	Pre-cubismus
(	(	kIx(	(
<g/>
1906	[number]	k4	1906
<g/>
–	–	k?	–
<g/>
1909	[number]	k4	1909
<g/>
)	)	kIx)	)
–	–	k?	–
toto	tento	k3xDgNnSc4	tento
období	období	k1gNnSc4	období
zahájili	zahájit	k5eAaPmAgMnP	zahájit
kubisté	kubista	k1gMnPc1	kubista
Georges	Georgesa	k1gFnPc2	Georgesa
Braque	Braqu	k1gInSc2	Braqu
a	a	k8xC	a
Pablo	Pablo	k1gNnSc1	Pablo
Picasso	Picassa	k1gFnSc5	Picassa
<g/>
.	.	kIx.	.
</s>
<s>
Období	období	k1gNnSc1	období
kdy	kdy	k6eAd1	kdy
kubismus	kubismus	k1gInSc1	kubismus
vznikal	vznikat	k5eAaImAgInS	vznikat
<g/>
,	,	kIx,	,
díla	dílo	k1gNnPc1	dílo
jsou	být	k5eAaImIp3nP	být
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
předchozími	předchozí	k2eAgInPc7d1	předchozí
malířskými	malířský	k2eAgInPc7d1	malířský
trendy	trend	k1gInPc7	trend
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
objevování	objevování	k1gNnSc3	objevování
kubistické	kubistický	k2eAgFnSc2d1	kubistická
perspektivy	perspektiva	k1gFnSc2	perspektiva
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Pablo	Pablo	k1gNnSc1	Pablo
Picasso	Picassa	k1gFnSc5	Picassa
-	-	kIx~	-
Ženský	ženský	k2eAgInSc1d1	ženský
akt	akt	k1gInSc1	akt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
jsou	být	k5eAaImIp3nP	být
zobrazované	zobrazovaný	k2eAgInPc1d1	zobrazovaný
předměty	předmět	k1gInPc1	předmět
omezovány	omezován	k2eAgInPc1d1	omezován
na	na	k7c4	na
základní	základní	k2eAgInPc4d1	základní
geometrické	geometrický	k2eAgInPc4d1	geometrický
tvary	tvar	k1gInPc4	tvar
<g/>
,	,	kIx,	,
z	z	k7c2	z
barev	barva	k1gFnPc2	barva
se	se	k3xPyFc4	se
používaly	používat	k5eAaImAgInP	používat
pouze	pouze	k6eAd1	pouze
odstíny	odstín	k1gInPc1	odstín
šedé	šedý	k2eAgFnSc2d1	šedá
a	a	k8xC	a
hnědé	hnědý	k2eAgFnSc2d1	hnědá
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Analytický	analytický	k2eAgInSc1d1	analytický
kubismus	kubismus	k1gInSc1	kubismus
(	(	kIx(	(
<g/>
1909	[number]	k4	1909
<g/>
–	–	k?	–
<g/>
1912	[number]	k4	1912
<g/>
)	)	kIx)	)
–	–	k?	–
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
kubismus	kubismus	k1gInSc1	kubismus
nebyl	být	k5eNaImAgInS	být
abstraktním	abstraktní	k2eAgNnSc7d1	abstraktní
uměním	umění	k1gNnSc7	umění
<g/>
,	,	kIx,	,
barva	barva	k1gFnSc1	barva
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
téměř	téměř	k6eAd1	téměř
nepoužívala	používat	k5eNaImAgFnS	používat
<g/>
.	.	kIx.	.
</s>
<s>
Dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
částečné	částečný	k2eAgFnSc3d1	částečná
abstrakci	abstrakce	k1gFnSc3	abstrakce
tzn.	tzn.	kA	tzn.
zobrazované	zobrazovaný	k2eAgInPc1d1	zobrazovaný
předměty	předmět	k1gInPc1	předmět
jsou	být	k5eAaImIp3nP	být
nahrazovány	nahrazovat	k5eAaImNgInP	nahrazovat
jinými	jiný	k2eAgInPc7d1	jiný
podobnými	podobný	k2eAgInPc7d1	podobný
předměty	předmět	k1gInPc7	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Zobrazovaný	zobrazovaný	k2eAgInSc1d1	zobrazovaný
předmět	předmět	k1gInSc1	předmět
je	být	k5eAaImIp3nS	být
rozložen	rozložit	k5eAaPmNgInS	rozložit
na	na	k7c4	na
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
geometrické	geometrický	k2eAgInPc4d1	geometrický
tvary	tvar	k1gInPc4	tvar
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
zobrazeny	zobrazit	k5eAaPmNgFnP	zobrazit
neperspektivně	perspektivně	k6eNd1	perspektivně
vedle	vedle	k7c2	vedle
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Syntetický	syntetický	k2eAgInSc1d1	syntetický
kubismus	kubismus	k1gInSc1	kubismus
(	(	kIx(	(
<g/>
též	též	k9	též
vrcholný	vrcholný	k2eAgMnSc1d1	vrcholný
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1912	[number]	k4	1912
<g/>
–	–	k?	–
<g/>
1914	[number]	k4	1914
<g/>
)	)	kIx)	)
–	–	k?	–
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
fázi	fáze	k1gFnSc6	fáze
se	se	k3xPyFc4	se
kubismus	kubismus	k1gInSc1	kubismus
přiblížil	přiblížit	k5eAaPmAgInS	přiblížit
abstrakci	abstrakce	k1gFnSc4	abstrakce
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
stále	stále	k6eAd1	stále
důležitý	důležitý	k2eAgInSc1d1	důležitý
zobrazovaný	zobrazovaný	k2eAgInSc1d1	zobrazovaný
předmět	předmět	k1gInSc1	předmět
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
skládán	skládat	k5eAaImNgInS	skládat
z	z	k7c2	z
linií	linie	k1gFnPc2	linie
a	a	k8xC	a
ploch	plocha	k1gFnPc2	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
koláž	koláž	k1gFnSc1	koláž
a	a	k8xC	a
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
i	i	k9	i
barva	barva	k1gFnSc1	barva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Orfismus	orfismus	k1gInSc1	orfismus
–	–	k?	–
toto	tento	k3xDgNnSc1	tento
období	období	k1gNnSc1	období
dospělo	dochvít	k5eAaPmAgNnS	dochvít
k	k	k7c3	k
úplné	úplný	k2eAgFnSc3d1	úplná
abstrakci	abstrakce	k1gFnSc3	abstrakce
<g/>
,	,	kIx,	,
využívá	využívat	k5eAaImIp3nS	využívat
barvy	barva	k1gFnPc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
již	již	k6eAd1	již
umělci	umělec	k1gMnPc1	umělec
nenapodobují	napodobovat	k5eNaImIp3nP	napodobovat
ani	ani	k8xC	ani
se	se	k3xPyFc4	se
nijak	nijak	k6eAd1	nijak
nesnaží	snažit	k5eNaImIp3nS	snažit
rozkládat	rozkládat	k5eAaImF	rozkládat
či	či	k8xC	či
znovu	znovu	k6eAd1	znovu
skládat	skládat	k5eAaImF	skládat
reálný	reálný	k2eAgInSc4d1	reálný
předmět	předmět	k1gInSc4	předmět
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
vlastně	vlastně	k9	vlastně
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
malířské	malířský	k2eAgInPc1d1	malířský
prvky	prvek	k1gInPc1	prvek
staly	stát	k5eAaPmAgInP	stát
zcela	zcela	k6eAd1	zcela
nezávislé	závislý	k2eNgInPc1d1	nezávislý
na	na	k7c6	na
realitě	realita	k1gFnSc6	realita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
dalšímu	další	k2eAgInSc3d1	další
rozvoji	rozvoj	k1gInSc3	rozvoj
kubismu	kubismus	k1gInSc2	kubismus
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
již	již	k6eAd1	již
nešlo	jít	k5eNaImAgNnS	jít
o	o	k7c4	o
čistý	čistý	k2eAgInSc4d1	čistý
kubismus	kubismus	k1gInSc4	kubismus
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kubismus	kubismus	k1gInSc1	kubismus
něčím	něco	k3yInSc7	něco
ovlivněný	ovlivněný	k2eAgInSc1d1	ovlivněný
<g/>
,	,	kIx,	,
např.	např.	kA	např.
imaginativní	imaginativní	k2eAgInSc1d1	imaginativní
kubismus	kubismus	k1gInSc1	kubismus
je	být	k5eAaImIp3nS	být
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
surrealismem	surrealismus	k1gInSc7	surrealismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výtvarné	výtvarný	k2eAgNnSc1d1	výtvarné
umění	umění	k1gNnSc1	umění
==	==	k?	==
</s>
</p>
<p>
<s>
Světové	světový	k2eAgNnSc1d1	světové
malířství	malířství	k1gNnSc1	malířství
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Georges	Georges	k1gMnSc1	Georges
Braque	Braqu	k1gFnSc2	Braqu
(	(	kIx(	(
<g/>
Francouz	Francouz	k1gMnSc1	Francouz
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Robert	Robert	k1gMnSc1	Robert
Delaunay	Delaunaa	k1gFnSc2	Delaunaa
(	(	kIx(	(
<g/>
Francouz	Francouz	k1gMnSc1	Francouz
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Juan	Juan	k1gMnSc1	Juan
Gris	Gris	k1gInSc1	Gris
(	(	kIx(	(
<g/>
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
José	Josá	k1gFnSc2	Josá
Victoriano	Victoriana	k1gFnSc5	Victoriana
Gonzales	Gonzales	k1gMnSc1	Gonzales
<g/>
,	,	kIx,	,
Španěl	Španěl	k1gMnSc1	Španěl
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Fernand	Fernand	k1gInSc1	Fernand
Léger	Léger	k1gInSc1	Léger
(	(	kIx(	(
<g/>
Francouz	Francouz	k1gMnSc1	Francouz
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jacques	Jacques	k1gMnSc1	Jacques
Lipchitz	Lipchitz	k1gMnSc1	Lipchitz
(	(	kIx(	(
<g/>
americký	americký	k2eAgMnSc1d1	americký
sochař	sochař	k1gMnSc1	sochař
<g/>
,	,	kIx,	,
litevského	litevský	k2eAgInSc2d1	litevský
původu	původ	k1gInSc2	původ
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pablo	Pabla	k1gFnSc5	Pabla
Picasso	Picassa	k1gFnSc5	Picassa
(	(	kIx(	(
<g/>
Španěl	Španěl	k1gMnSc1	Španěl
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Fritz	Fritz	k1gMnSc1	Fritz
Wotruba	Wotruba	k1gMnSc1	Wotruba
(	(	kIx(	(
<g/>
rakouský	rakouský	k2eAgMnSc1d1	rakouský
sochař	sochař	k1gMnSc1	sochař
a	a	k8xC	a
grafik	grafik	k1gMnSc1	grafik
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
České	český	k2eAgNnSc1d1	české
malířství	malířství	k1gNnSc1	malířství
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Emil	Emil	k1gMnSc1	Emil
Filla	Filla	k1gMnSc1	Filla
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Čapek	Čapek	k1gMnSc1	Čapek
</s>
</p>
<p>
<s>
Otakar	Otakar	k1gMnSc1	Otakar
Kubín	Kubín	k1gMnSc1	Kubín
</s>
</p>
<p>
<s>
Bohumil	Bohumil	k1gMnSc1	Bohumil
Kubišta	Kubišta	k1gMnSc1	Kubišta
</s>
</p>
<p>
<s>
Antonín	Antonín	k1gMnSc1	Antonín
Procházka	Procházka	k1gMnSc1	Procházka
</s>
</p>
<p>
<s>
Václav	Václav	k1gMnSc1	Václav
ŠpálaSlovenské	ŠpálaSlovenský	k2eAgNnSc4d1	ŠpálaSlovenský
malířství	malířství	k1gNnSc4	malířství
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Ludo	Ludo	k6eAd1	Ludo
Lehen	Lehen	k2eAgInSc1d1	Lehen
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Kubismus	kubismus	k1gInSc4	kubismus
literaturu	literatura	k1gFnSc4	literatura
příliš	příliš	k6eAd1	příliš
neoslovil	oslovit	k5eNaPmAgMnS	oslovit
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
velmi	velmi	k6eAd1	velmi
málo	málo	k4c1	málo
děl	dělo	k1gNnPc2	dělo
spojených	spojený	k2eAgInPc2d1	spojený
s	s	k7c7	s
tímto	tento	k3xDgNnSc7	tento
hnutím	hnutí	k1gNnSc7	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
kubistické	kubistický	k2eAgFnPc4d1	kubistická
básně	báseň	k1gFnPc4	báseň
není	být	k5eNaImIp3nS	být
rozhodující	rozhodující	k2eAgInSc1d1	rozhodující
význam	význam	k1gInSc1	význam
ani	ani	k8xC	ani
melodičnost	melodičnost	k1gFnSc1	melodičnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
obraz	obraz	k1gInSc1	obraz
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
jsou	být	k5eAaImIp3nP	být
básně	báseň	k1gFnPc4	báseň
kubistů	kubista	k1gMnPc2	kubista
netradiční	tradiční	k2eNgNnSc4d1	netradiční
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
svého	svůj	k3xOyFgInSc2	svůj
tvaru	tvar	k1gInSc2	tvar
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
tvořily	tvořit	k5eAaImAgFnP	tvořit
z	z	k7c2	z
písmen	písmeno	k1gNnPc2	písmeno
obrazce	obrazec	k1gInSc2	obrazec
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
byly	být	k5eAaImAgInP	být
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
dané	daný	k2eAgFnSc3d1	daná
básni	báseň	k1gFnSc3	báseň
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Důraz	důraz	k1gInSc1	důraz
byl	být	k5eAaImAgInS	být
kladen	klást	k5eAaImNgInS	klást
na	na	k7c4	na
dynamiku	dynamika	k1gFnSc4	dynamika
a	a	k8xC	a
fantasii	fantasie	k1gFnSc4	fantasie
<g/>
.	.	kIx.	.
</s>
<s>
Oblíbeným	oblíbený	k2eAgInSc7d1	oblíbený
básnickým	básnický	k2eAgInSc7d1	básnický
prostředkem	prostředek	k1gInSc7	prostředek
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
tzv.	tzv.	kA	tzv.
volná	volný	k2eAgFnSc1d1	volná
asociace	asociace	k1gFnSc1	asociace
představ	představa	k1gFnPc2	představa
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
níž	jenž	k3xRgFnSc2	jenž
chtěli	chtít	k5eAaImAgMnP	chtít
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
jakési	jakýsi	k3yIgNnSc4	jakýsi
"	"	kIx"	"
<g/>
nadskutečno	nadskutečno	k1gNnSc4	nadskutečno
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Guillaume	Guillaum	k1gInSc5	Guillaum
Apollinaire	Apollinair	k1gInSc5	Apollinair
</s>
</p>
<p>
<s>
Jean	Jean	k1gMnSc1	Jean
Cocteau	Cocteaus	k1gInSc2	Cocteaus
</s>
</p>
<p>
<s>
Max	Max	k1gMnSc1	Max
Jacob	Jacoba	k1gFnPc2	Jacoba
</s>
</p>
<p>
<s>
Pierre	Pierr	k1gMnSc5	Pierr
Reverdy	Reverda	k1gFnPc1	Reverda
</s>
</p>
<p>
<s>
Andre	Andr	k1gMnSc5	Andr
Salamon	Salamon	k1gMnSc1	Salamon
</s>
</p>
<p>
<s>
Gertrude	Gertrude	k6eAd1	Gertrude
Steinová	Steinová	k1gFnSc1	Steinová
</s>
</p>
<p>
<s>
==	==	k?	==
Architektura	architektura	k1gFnSc1	architektura
==	==	k?	==
</s>
</p>
<p>
<s>
U	u	k7c2	u
některých	některý	k3yIgMnPc2	některý
architektů	architekt	k1gMnPc2	architekt
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
velmi	velmi	k6eAd1	velmi
výraznému	výrazný	k2eAgNnSc3d1	výrazné
ovlivnění	ovlivnění	k1gNnSc3	ovlivnění
kubismem	kubismus	k1gInSc7	kubismus
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnPc1	jejich
stavby	stavba	k1gFnPc1	stavba
samozřejmě	samozřejmě	k6eAd1	samozřejmě
musely	muset	k5eAaImAgFnP	muset
být	být	k5eAaImF	být
především	především	k6eAd1	především
funkční	funkční	k2eAgNnSc1d1	funkční
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
nelze	lze	k6eNd1	lze
hovořit	hovořit	k5eAaImF	hovořit
o	o	k7c6	o
čistém	čistý	k2eAgInSc6d1	čistý
kubismu	kubismus	k1gInSc6	kubismus
<g/>
.	.	kIx.	.
</s>
<s>
Architekti	architekt	k1gMnPc1	architekt
tvořící	tvořící	k2eAgMnPc1d1	tvořící
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
kubismu	kubismus	k1gInSc2	kubismus
vytvářeli	vytvářet	k5eAaImAgMnP	vytvářet
velmi	velmi	k6eAd1	velmi
zvláštní	zvláštní	k2eAgNnPc4d1	zvláštní
díla	dílo	k1gNnPc4	dílo
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
působí	působit	k5eAaImIp3nP	působit
velice	velice	k6eAd1	velice
silným	silný	k2eAgInSc7d1	silný
dojmem	dojem	k1gInSc7	dojem
<g/>
.	.	kIx.	.
</s>
<s>
Kubistická	kubistický	k2eAgFnSc1d1	kubistická
architektura	architektura	k1gFnSc1	architektura
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
na	na	k7c6	na
území	území	k1gNnSc6	území
současného	současný	k2eAgNnSc2d1	současné
Česka	Česko	k1gNnSc2	Česko
díky	díky	k7c3	díky
skupině	skupina	k1gFnSc3	skupina
umělců	umělec	k1gMnPc2	umělec
jako	jako	k8xS	jako
Emil	Emil	k1gMnSc1	Emil
Filla	Filla	k1gMnSc1	Filla
<g/>
,	,	kIx,	,
Otto	Otto	k1gMnSc1	Otto
Gutfreund	Gutfreund	k1gMnSc1	Gutfreund
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Čapek	Čapek	k1gMnSc1	Čapek
a	a	k8xC	a
architektů	architekt	k1gMnPc2	architekt
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
Josef	Josef	k1gMnSc1	Josef
Gočár	Gočár	k1gMnSc1	Gočár
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Chochol	Chochol	k1gMnSc1	Chochol
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Kubistický	kubistický	k2eAgInSc1d1	kubistický
směr	směr	k1gInSc1	směr
je	být	k5eAaImIp3nS	být
světově	světově	k6eAd1	světově
unikátní	unikátní	k2eAgMnSc1d1	unikátní
a	a	k8xC	a
nikde	nikde	k6eAd1	nikde
jinde	jinde	k6eAd1	jinde
nedosáhla	dosáhnout	k5eNaPmAgFnS	dosáhnout
kubistická	kubistický	k2eAgFnSc1d1	kubistická
architektura	architektura	k1gFnSc1	architektura
takového	takový	k3xDgInSc2	takový
rozmachu	rozmach	k1gInSc2	rozmach
jako	jako	k8xC	jako
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
kubistickou	kubistický	k2eAgFnSc4d1	kubistická
architekturu	architektura	k1gFnSc4	architektura
později	pozdě	k6eAd2	pozdě
navazuje	navazovat	k5eAaImIp3nS	navazovat
dekorativnější	dekorativní	k2eAgInSc1d2	dekorativnější
rondokubismus	rondokubismus	k1gInSc1	rondokubismus
<g/>
,	,	kIx,	,
zvaný	zvaný	k2eAgInSc1d1	zvaný
také	také	k9	také
české	český	k2eAgInPc4d1	český
art	art	k?	art
deco	deco	k1gNnSc1	deco
<g/>
.	.	kIx.	.
<g/>
Architekti	architekt	k1gMnPc1	architekt
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Gočár	Gočár	k1gMnSc1	Gočár
–	–	k?	–
Dům	dům	k1gInSc1	dům
U	u	k7c2	u
Černé	Černé	k2eAgFnSc2d1	Černé
Matky	matka	k1gFnSc2	matka
Boží	boží	k2eAgFnSc2d1	boží
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
Lázeňský	lázeňský	k2eAgInSc1d1	lázeňský
dům	dům	k1gInSc1	dům
v	v	k7c6	v
Bohdanči	Bohdanči	k1gFnSc6	Bohdanči
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Chochol	Chochol	k1gMnSc1	Chochol
-	-	kIx~	-
Kovařovicova	Kovařovicův	k2eAgFnSc1d1	Kovařovicova
vila	vila	k1gFnSc1	vila
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
pod	pod	k7c7	pod
Vyšehradem	Vyšehrad	k1gInSc7	Vyšehrad
<g/>
,	,	kIx,	,
činžovní	činžovní	k2eAgFnSc7d1	činžovní
dům	dům	k1gInSc4	dům
v	v	k7c6	v
Neklanově	klanově	k6eNd1	klanově
ulici	ulice	k1gFnSc6	ulice
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
trojdům	trojdům	k1gInSc1	trojdům
na	na	k7c6	na
Rašínově	Rašínův	k2eAgNnSc6d1	Rašínovo
nábřeží	nábřeží	k1gNnSc6	nábřeží
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
pod	pod	k7c7	pod
Vyšehradem	Vyšehrad	k1gInSc7	Vyšehrad
</s>
</p>
<p>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
Janák	Janák	k1gMnSc1	Janák
–	–	k?	–
Libeňský	libeňský	k2eAgInSc4d1	libeňský
most	most	k1gInSc4	most
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
Škodův	Škodův	k2eAgInSc1d1	Škodův
palác	palác	k1gInSc1	palác
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
</s>
</p>
<p>
<s>
Otakar	Otakar	k1gMnSc1	Otakar
Novotný	Novotný	k1gMnSc1	Novotný
–	–	k?	–
Učitelské	učitelský	k2eAgInPc4d1	učitelský
domy	dům	k1gInPc4	dům
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
</s>
</p>
<p>
<s>
Vlastislav	Vlastislav	k1gMnSc1	Vlastislav
Hofman	Hofman	k1gMnSc1	Hofman
–	–	k?	–
Ďáblický	ďáblický	k2eAgInSc4d1	ďáblický
hřbitov	hřbitov	k1gInSc4	hřbitov
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
</s>
</p>
<p>
<s>
Emil	Emil	k1gMnSc1	Emil
Králíček	Králíček	k1gMnSc1	Králíček
–	–	k?	–
kubistická	kubistický	k2eAgFnSc1d1	kubistická
lucerna	lucerna	k1gFnSc1	lucerna
na	na	k7c6	na
Jungmannově	Jungmannův	k2eAgNnSc6d1	Jungmannovo
náměstí	náměstí	k1gNnSc6	náměstí
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
Dům	dům	k1gInSc1	dům
Diamant	diamant	k1gInSc1	diamant
v	v	k7c6	v
Lazarské	Lazarský	k2eAgFnSc6d1	Lazarská
ulici	ulice	k1gFnSc6	ulice
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
</s>
</p>
<p>
<s>
===	===	k?	===
Galerie	galerie	k1gFnSc1	galerie
kubistické	kubistický	k2eAgFnSc2d1	kubistická
architektury	architektura	k1gFnSc2	architektura
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Český	český	k2eAgInSc1d1	český
kubismus	kubismus	k1gInSc1	kubismus
</s>
</p>
<p>
<s>
Art	Art	k?	Art
deco	deco	k6eAd1	deco
</s>
</p>
<p>
<s>
rondokubismus	rondokubismus	k1gInSc1	rondokubismus
(	(	kIx(	(
<g/>
české	český	k2eAgNnSc1d1	české
art	art	k?	art
deco	deco	k1gNnSc1	deco
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Christopher	Christophra	k1gFnPc2	Christophra
R.	R.	kA	R.
W.	W.	kA	W.
Nevinson	Nevinson	k1gMnSc1	Nevinson
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
kubismus	kubismus	k1gInSc1	kubismus
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Téma	téma	k1gNnSc1	téma
Kubismus	kubismus	k1gInSc1	kubismus
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Fostinum	Fostinum	k1gNnSc1	Fostinum
<g/>
:	:	kIx,	:
Czech	Czech	k1gMnSc1	Czech
Cubist	Cubist	k1gMnSc1	Cubist
Architecture	Architectur	k1gMnSc5	Architectur
(	(	kIx(	(
<g/>
Česká	český	k2eAgFnSc1d1	Česká
kubistická	kubistický	k2eAgFnSc1d1	kubistická
architektura	architektura	k1gFnSc1	architektura
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
On-Line	On-Linout	k5eAaPmIp3nS	On-Linout
Picasso	Picassa	k1gFnSc5	Picassa
Project	Project	k1gMnSc1	Project
</s>
</p>
<p>
<s>
The	The	k?	The
Cubist	Cubist	k1gInSc1	Cubist
Rupture	Ruptur	k1gMnSc5	Ruptur
Artistic	Artistice	k1gFnPc2	Artistice
comments	comments	k1gInSc1	comments
in	in	k?	in
MundoArta	MundoArta	k1gFnSc1	MundoArta
</s>
</p>
<p>
<s>
Video	video	k1gNnSc1	video
decoding	decoding	k1gInSc1	decoding
a	a	k8xC	a
Picasso	Picassa	k1gFnSc5	Picassa
Cubist	Cubist	k1gMnSc1	Cubist
still-life	stillif	k1gInSc5	still-lif
</s>
</p>
<p>
<s>
ibiblio	ibiblio	k1gNnSc1	ibiblio
(	(	kIx(	(
<g/>
internet	internet	k1gInSc1	internet
library	librara	k1gFnSc2	librara
<g/>
)	)	kIx)	)
Cubism	Cubism	k1gInSc1	Cubism
</s>
</p>
<p>
<s>
The	The	k?	The
Czech	Czech	k1gInSc1	Czech
Cubism	Cubism	k1gMnSc1	Cubism
Foundation	Foundation	k1gInSc1	Foundation
</s>
</p>
<p>
<s>
Cubism	Cubism	k6eAd1	Cubism
<g/>
,	,	kIx,	,
The	The	k1gMnSc5	The
Big	Big	k1gMnSc5	Big
Picture	Pictur	k1gMnSc5	Pictur
</s>
</p>
