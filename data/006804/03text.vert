<s>
Mission	Mission	k1gInSc1	Mission
<g/>
:	:	kIx,	:
Impossible	Impossible	k1gFnSc1	Impossible
–	–	k?	–
Ghost	Ghost	k1gInSc1	Ghost
Protocol	Protocol	k1gInSc1	Protocol
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgInSc1d1	americký
film	film	k1gInSc1	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
režírovaný	režírovaný	k2eAgMnSc1d1	režírovaný
Bradem	Bradem	k?	Bradem
Birdem	Bird	k1gInSc7	Bird
<g/>
.	.	kIx.	.
</s>
<s>
Snímek	snímek	k1gInSc1	snímek
představuje	představovat	k5eAaImIp3nS	představovat
pokračovaní	pokračovaný	k2eAgMnPc1d1	pokračovaný
série	série	k1gFnPc4	série
Mission	Mission	k1gInSc1	Mission
<g/>
:	:	kIx,	:
Impossible	Impossible	k1gFnSc1	Impossible
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc4d1	hlavní
postavu	postava	k1gFnSc4	postava
agenta	agent	k1gMnSc2	agent
IMF	IMF	kA	IMF
Ethana	Ethan	k1gMnSc2	Ethan
Hunta	Hunt	k1gMnSc2	Hunt
ztvárnil	ztvárnit	k5eAaPmAgMnS	ztvárnit
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
v	v	k7c6	v
předchozích	předchozí	k2eAgInPc6d1	předchozí
filmech	film	k1gInPc6	film
Tom	Tom	k1gMnSc1	Tom
Cruise	Cruise	k1gFnSc1	Cruise
<g/>
.	.	kIx.	.
</s>
<s>
Natáčelo	natáčet	k5eAaImAgNnS	natáčet
se	se	k3xPyFc4	se
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
<g/>
,	,	kIx,	,
Dubaji	Dubaj	k1gFnSc6	Dubaj
<g/>
,	,	kIx,	,
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
Mladé	mladý	k2eAgFnSc6d1	mladá
Boleslavi	Boleslaev	k1gFnSc6	Boleslaev
<g/>
,	,	kIx,	,
Bombaji	Bombaj	k1gFnSc6	Bombaj
a	a	k8xC	a
ve	v	k7c6	v
Vancouveru	Vancouver	k1gInSc6	Vancouver
<g/>
.	.	kIx.	.
</s>
<s>
Tentokrát	tentokrát	k6eAd1	tentokrát
Ethan	ethan	k1gInSc1	ethan
Hunt	hunt	k1gInSc1	hunt
operuje	operovat	k5eAaImIp3nS	operovat
v	v	k7c6	v
Kremlu	Kreml	k1gInSc6	Kreml
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
ho	on	k3xPp3gInSc4	on
jeho	jeho	k3xOp3gInSc4	jeho
tým	tým	k1gInSc4	tým
dostane	dostat	k5eAaPmIp3nS	dostat
z	z	k7c2	z
ruského	ruský	k2eAgNnSc2d1	ruské
vězení	vězení	k1gNnSc2	vězení
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nasazen	nasadit	k5eAaPmNgMnS	nasadit
do	do	k7c2	do
mise	mise	k1gFnSc2	mise
<g/>
,	,	kIx,	,
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
které	který	k3yIgNnSc1	který
má	mít	k5eAaImIp3nS	mít
zajistit	zajistit	k5eAaPmF	zajistit
odpalovací	odpalovací	k2eAgInPc4d1	odpalovací
kódy	kód	k1gInPc4	kód
k	k	k7c3	k
jaderným	jaderný	k2eAgFnPc3d1	jaderná
hlavicím	hlavice	k1gFnPc3	hlavice
a	a	k8xC	a
identitu	identita	k1gFnSc4	identita
jednoho	jeden	k4xCgMnSc2	jeden
ruského	ruský	k2eAgMnSc2d1	ruský
špióna	špión	k1gMnSc2	špión
<g/>
.	.	kIx.	.
</s>
<s>
Americký	americký	k2eAgMnSc1d1	americký
prezident	prezident	k1gMnSc1	prezident
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
operace	operace	k1gFnSc2	operace
distancuje	distancovat	k5eAaBmIp3nS	distancovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
Ethan	ethan	k1gInSc1	ethan
operuje	operovat	k5eAaImIp3nS	operovat
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
týmem	tým	k1gInSc7	tým
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
ho	on	k3xPp3gInSc4	on
dostal	dostat	k5eAaPmAgInS	dostat
z	z	k7c2	z
vězení	vězení	k1gNnSc2	vězení
a	a	k8xC	a
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
s	s	k7c7	s
šéfanalytikem	šéfanalytik	k1gMnSc7	šéfanalytik
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
ztvárnil	ztvárnit	k5eAaPmAgInS	ztvárnit
Jeremy	Jerema	k1gFnSc2	Jerema
Renner	Renner	k1gInSc1	Renner
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
přežil	přežít	k5eAaPmAgMnS	přežít
útok	útok	k1gInSc4	útok
na	na	k7c4	na
kolonu	kolona	k1gFnSc4	kolona
ministra	ministr	k1gMnSc2	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Ukáže	ukázat	k5eAaPmIp3nS	ukázat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
šéfanalytika	šéfanalytik	k1gMnSc4	šéfanalytik
je	být	k5eAaImIp3nS	být
příliš	příliš	k6eAd1	příliš
dobrý	dobrý	k2eAgInSc1d1	dobrý
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
také	také	k9	také
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
aktivní	aktivní	k2eAgFnSc6d1	aktivní
službě	služba	k1gFnSc6	služba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bylo	být	k5eAaImAgNnS	být
nutno	nutno	k6eAd1	nutno
provést	provést	k5eAaPmF	provést
šedou	šedý	k2eAgFnSc4d1	šedá
operaci	operace	k1gFnSc4	operace
a	a	k8xC	a
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
přeřazení	přeřazení	k1gNnSc3	přeřazení
<g/>
.	.	kIx.	.
</s>
<s>
Zajištění	zajištění	k1gNnSc1	zajištění
odpalovacích	odpalovací	k2eAgFnPc2d1	odpalovací
kódu	kód	k1gInSc3	kód
se	se	k3xPyFc4	se
však	však	k9	však
zvrtne	zvrtnout	k5eAaPmIp3nS	zvrtnout
a	a	k8xC	a
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
rakety	raketa	k1gFnPc4	raketa
znefunkčnit	znefunkčnit	k5eAaPmF	znefunkčnit
jinak	jinak	k6eAd1	jinak
<g/>
...	...	k?	...
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Mission	Mission	k1gInSc1	Mission
<g/>
:	:	kIx,	:
Impossible	Impossible	k1gFnSc1	Impossible
-	-	kIx~	-
Ghost	Ghost	k1gInSc1	Ghost
Protocol	Protocola	k1gFnPc2	Protocola
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Mission	Mission	k1gInSc1	Mission
<g/>
:	:	kIx,	:
Impossible	Impossible	k1gFnSc1	Impossible
–	–	k?	–
Ghost	Ghost	k1gInSc1	Ghost
Protocol	Protocol	k1gInSc1	Protocol
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc3d1	filmová
databázi	databáze	k1gFnSc3	databáze
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Mission	Mission	k1gInSc1	Mission
<g/>
:	:	kIx,	:
Impossible	Impossible	k1gFnSc1	Impossible
–	–	k?	–
Ghost	Ghost	k1gInSc1	Ghost
Protocol	Protocola	k1gFnPc2	Protocola
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databas	k1gInSc6	Databas
</s>
