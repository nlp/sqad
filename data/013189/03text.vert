<p>
<s>
Trinitrotoluen	trinitrotoluen	k1gInSc1	trinitrotoluen
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
označovaný	označovaný	k2eAgInSc1d1	označovaný
jako	jako	k8xC	jako
TNT	TNT	kA	TNT
nebo	nebo	k8xC	nebo
tritol	tritol	k1gInSc1	tritol
<g/>
,	,	kIx,	,
chemicky	chemicky	k6eAd1	chemicky
názvem	název	k1gInSc7	název
2,4	[number]	k4	2,4
<g/>
,6	,6	k4	,6
<g/>
-trinitrotoluen	rinitrotoluna	k1gFnPc2	-trinitrotoluna
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
silná	silný	k2eAgFnSc1d1	silná
<g/>
,	,	kIx,	,
relativně	relativně	k6eAd1	relativně
bezpečná	bezpečný	k2eAgFnSc1d1	bezpečná
a	a	k8xC	a
hojně	hojně	k6eAd1	hojně
používaná	používaný	k2eAgFnSc1d1	používaná
trhavina	trhavina	k1gFnSc1	trhavina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Poprvé	poprvé	k6eAd1	poprvé
jej	on	k3xPp3gMnSc4	on
připravil	připravit	k5eAaPmAgInS	připravit
roku	rok	k1gInSc2	rok
1863	[number]	k4	1863
německý	německý	k2eAgMnSc1d1	německý
chemik	chemik	k1gMnSc1	chemik
Julius	Julius	k1gMnSc1	Julius
Wilbrand	Wilbrando	k1gNnPc2	Wilbrando
a	a	k8xC	a
původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
používal	používat	k5eAaImAgInS	používat
jako	jako	k9	jako
žluté	žlutý	k2eAgNnSc4d1	žluté
barvivo	barvivo	k1gNnSc4	barvivo
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
výbušný	výbušný	k2eAgInSc1d1	výbušný
potenciál	potenciál	k1gInSc1	potenciál
nebyl	být	k5eNaImAgInS	být
řadu	řada	k1gFnSc4	řada
let	léto	k1gNnPc2	léto
doceněn	doceněn	k2eAgMnSc1d1	doceněn
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
bylo	být	k5eAaImAgNnS	být
tak	tak	k6eAd1	tak
obtížné	obtížný	k2eAgNnSc1d1	obtížné
jej	on	k3xPp3gMnSc4	on
odpálit	odpálit	k5eAaPmF	odpálit
a	a	k8xC	a
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
o	o	k7c4	o
zlomek	zlomek	k1gInSc4	zlomek
méně	málo	k6eAd2	málo
účinný	účinný	k2eAgInSc4d1	účinný
než	než	k8xS	než
jiné	jiný	k2eAgFnPc4d1	jiná
výbušniny	výbušnina	k1gFnPc4	výbušnina
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
v	v	k7c6	v
tekutém	tekutý	k2eAgInSc6d1	tekutý
stavu	stav	k1gInSc6	stav
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
TNT	TNT	kA	TNT
plnit	plnit	k5eAaImF	plnit
do	do	k7c2	do
dělostřeleckých	dělostřelecký	k2eAgInPc2d1	dělostřelecký
granátů	granát	k1gInPc2	granát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tekutém	tekutý	k2eAgInSc6d1	tekutý
stavu	stav	k1gInSc6	stav
je	být	k5eAaImIp3nS	být
citlivost	citlivost	k1gFnSc1	citlivost
TNT	TNT	kA	TNT
oproti	oproti	k7c3	oproti
pevnému	pevný	k2eAgNnSc3d1	pevné
TNT	TNT	kA	TNT
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
však	však	k9	však
výrazně	výrazně	k6eAd1	výrazně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pevném	pevný	k2eAgInSc6d1	pevný
stavu	stav	k1gInSc6	stav
je	být	k5eAaImIp3nS	být
citlivost	citlivost	k1gFnSc1	citlivost
TNT	TNT	kA	TNT
nízká	nízký	k2eAgFnSc1d1	nízká
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
obtížné	obtížný	k2eAgNnSc1d1	obtížné
jej	on	k3xPp3gMnSc4	on
iniciovat	iniciovat	k5eAaBmF	iniciovat
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1910	[number]	k4	1910
vyřazen	vyřadit	k5eAaPmNgInS	vyřadit
z	z	k7c2	z
britského	britský	k2eAgInSc2d1	britský
oficiálního	oficiální	k2eAgInSc2d1	oficiální
seznamu	seznam	k1gInSc2	seznam
výbušnin	výbušnina	k1gFnPc2	výbušnina
<g/>
.	.	kIx.	.
<g/>
Německé	německý	k2eAgFnPc1d1	německá
ozbrojené	ozbrojený	k2eAgFnPc1d1	ozbrojená
síly	síla	k1gFnPc1	síla
začaly	začít	k5eAaPmAgFnP	začít
TNT	TNT	kA	TNT
používat	používat	k5eAaImF	používat
jako	jako	k9	jako
náplně	náplň	k1gFnPc1	náplň
dělostřeleckých	dělostřelecký	k2eAgInPc2d1	dělostřelecký
granátů	granát	k1gInPc2	granát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1902	[number]	k4	1902
<g/>
.	.	kIx.	.
</s>
<s>
Protipanceřové	Protipanceřový	k2eAgInPc1d1	Protipanceřový
granáty	granát	k1gInPc1	granát
plněné	plněný	k2eAgInPc1d1	plněný
TNT	TNT	kA	TNT
vybuchovaly	vybuchovat	k5eAaImAgInP	vybuchovat
až	až	k9	až
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
prorazily	prorazit	k5eAaPmAgInP	prorazit
panceřování	panceřování	k1gNnSc4	panceřování
britských	britský	k2eAgNnPc2d1	Britské
bojových	bojový	k2eAgNnPc2d1	bojové
plavidel	plavidlo	k1gNnPc2	plavidlo
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
britské	britský	k2eAgInPc1d1	britský
granáty	granát	k1gInPc1	granát
plněné	plněný	k2eAgInPc1d1	plněný
lydditem	lyddit	k1gInSc7	lyddit
vybuchovaly	vybuchovat	k5eAaImAgFnP	vybuchovat
často	často	k6eAd1	často
již	již	k6eAd1	již
při	při	k7c6	při
nárazu	náraz	k1gInSc6	náraz
na	na	k7c4	na
pancíř	pancíř	k1gInSc4	pancíř
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
vydaly	vydat	k5eAaPmAgFnP	vydat
většinu	většina	k1gFnSc4	většina
energie	energie	k1gFnSc1	energie
mimo	mimo	k7c4	mimo
loď	loď	k1gFnSc4	loď
<g/>
.	.	kIx.	.
</s>
<s>
Britové	Brit	k1gMnPc1	Brit
začali	začít	k5eAaPmAgMnP	začít
nahrazovat	nahrazovat	k5eAaImF	nahrazovat
lyddit	lyddit	k5eAaImF	lyddit
trinitrotoluenem	trinitrotoluen	k1gInSc7	trinitrotoluen
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1907	[number]	k4	1907
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Americké	americký	k2eAgNnSc1d1	americké
námořnictvo	námořnictvo	k1gNnSc1	námořnictvo
dál	daleko	k6eAd2	daleko
používalo	používat	k5eAaImAgNnS	používat
granáty	granát	k1gInPc4	granát
plněné	plněný	k2eAgFnSc2d1	plněná
dunnitem	dunnit	k1gInSc7	dunnit
(	(	kIx(	(
<g/>
pikrát	pikrát	k1gInSc1	pikrát
amonný	amonný	k2eAgInSc1d1	amonný
<g/>
)	)	kIx)	)
i	i	k9	i
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
jiné	jiný	k2eAgInPc1d1	jiný
národy	národ	k1gInPc1	národ
přešly	přejít	k5eAaPmAgInP	přejít
na	na	k7c4	na
TNT	TNT	kA	TNT
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
začaly	začít	k5eAaPmAgFnP	začít
trinitrotoluenem	trinitrotoluen	k1gInSc7	trinitrotoluen
plnit	plnit	k5eAaImF	plnit
námořní	námořní	k2eAgFnPc4d1	námořní
miny	mina	k1gFnPc4	mina
<g/>
,	,	kIx,	,
hlubinné	hlubinný	k2eAgFnPc4d1	hlubinná
pumy	puma	k1gFnPc4	puma
a	a	k8xC	a
hlavice	hlavice	k1gFnPc4	hlavice
torpéd	torpédo	k1gNnPc2	torpédo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Základní	základní	k2eAgFnPc1d1	základní
fyzikální	fyzikální	k2eAgFnPc1d1	fyzikální
a	a	k8xC	a
chemické	chemický	k2eAgFnPc1d1	chemická
vlastnosti	vlastnost	k1gFnPc1	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
Chemicky	chemicky	k6eAd1	chemicky
je	být	k5eAaImIp3nS	být
TNT	TNT	kA	TNT
aromatický	aromatický	k2eAgInSc1d1	aromatický
uhlovodík	uhlovodík	k1gInSc1	uhlovodík
toluen	toluen	k1gInSc1	toluen
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
3	[number]	k4	3
uhlíkové	uhlíkový	k2eAgInPc1d1	uhlíkový
atomy	atom	k1gInPc1	atom
aromatického	aromatický	k2eAgNnSc2d1	aromatické
jádra	jádro	k1gNnSc2	jádro
nesou	nést	k5eAaImIp3nP	nést
namísto	namísto	k7c2	namísto
vodíkového	vodíkový	k2eAgInSc2d1	vodíkový
atomu	atom	k1gInSc2	atom
nitroskupinu	nitroskupina	k1gFnSc4	nitroskupina
(	(	kIx(	(
<g/>
-NO	-NO	k?	-NO
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
funkční	funkční	k2eAgInSc1d1	funkční
chemický	chemický	k2eAgInSc1d1	chemický
vzorec	vzorec	k1gInSc1	vzorec
této	tento	k3xDgFnSc2	tento
sloučeniny	sloučenina	k1gFnSc2	sloučenina
je	být	k5eAaImIp3nS	být
C	C	kA	C
<g/>
6	[number]	k4	6
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
(	(	kIx(	(
<g/>
NO	no	k9	no
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
3	[number]	k4	3
<g/>
CH	Ch	kA	Ch
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nažloutlá	nažloutlý	k2eAgFnSc1d1	nažloutlá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
o	o	k7c6	o
hustotě	hustota	k1gFnSc6	hustota
1,663	[number]	k4	1,663
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
cm	cm	kA	cm
<g/>
3	[number]	k4	3
a	a	k8xC	a
teplotě	teplota	k1gFnSc6	teplota
tání	tání	k1gNnSc2	tání
80,7	[number]	k4	80,7
°	°	k?	°
<g/>
C.	C.	kA	C.
Lze	lze	k6eAd1	lze
jej	on	k3xPp3gNnSc4	on
snadno	snadno	k6eAd1	snadno
bezpečně	bezpečně	k6eAd1	bezpečně
roztavit	roztavit	k5eAaPmF	roztavit
a	a	k8xC	a
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
kapalina	kapalina	k1gFnSc1	kapalina
má	mít	k5eAaImIp3nS	mít
teplotu	teplota	k1gFnSc4	teplota
varu	var	k1gInSc2	var
210	[number]	k4	210
<g/>
–	–	k?	–
<g/>
212	[number]	k4	212
°	°	k?	°
<g/>
C.	C.	kA	C.
Jako	jako	k8xC	jako
pevná	pevný	k2eAgFnSc1d1	pevná
látka	látka	k1gFnSc1	látka
se	se	k3xPyFc4	se
chová	chovat	k5eAaImIp3nS	chovat
pouze	pouze	k6eAd1	pouze
do	do	k7c2	do
teploty	teplota	k1gFnSc2	teplota
35	[number]	k4	35
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
při	při	k7c6	při
zvýšení	zvýšení	k1gNnSc6	zvýšení
teploty	teplota	k1gFnSc2	teplota
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
stává	stávat	k5eAaImIp3nS	stávat
plastickou	plastický	k2eAgFnSc7d1	plastická
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
proto	proto	k8xC	proto
docházet	docházet	k5eAaImF	docházet
i	i	k9	i
k	k	k7c3	k
vytékání	vytékání	k1gNnSc3	vytékání
ze	z	k7c2	z
střeliva	střelivo	k1gNnSc2	střelivo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
je	být	k5eAaImIp3nS	být
prakticky	prakticky	k6eAd1	prakticky
nerozpustný	rozpustný	k2eNgInSc1d1	nerozpustný
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
se	se	k3xPyFc4	se
však	však	k9	však
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
běžných	běžný	k2eAgNnPc2d1	běžné
organických	organický	k2eAgNnPc2d1	organické
rozpouštědel	rozpouštědlo	k1gNnPc2	rozpouštědlo
kromě	kromě	k7c2	kromě
ethanolu	ethanol	k1gInSc2	ethanol
a	a	k8xC	a
sirouhlíku	sirouhlík	k1gInSc2	sirouhlík
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
rozpustný	rozpustný	k2eAgMnSc1d1	rozpustný
v	v	k7c6	v
kyselině	kyselina	k1gFnSc6	kyselina
sírové	sírový	k2eAgFnSc2d1	sírová
<g/>
,	,	kIx,	,
vůči	vůči	k7c3	vůči
níž	jenž	k3xRgFnSc3	jenž
je	být	k5eAaImIp3nS	být
stálý	stálý	k2eAgMnSc1d1	stálý
až	až	k9	až
do	do	k7c2	do
teplot	teplota	k1gFnPc2	teplota
nad	nad	k7c7	nad
150	[number]	k4	150
°	°	k?	°
<g/>
C.	C.	kA	C.
Působením	působení	k1gNnSc7	působení
bazických	bazický	k2eAgFnPc2d1	bazická
sloučenin	sloučenina	k1gFnPc2	sloučenina
však	však	k9	však
přechází	přecházet	k5eAaImIp3nS	přecházet
do	do	k7c2	do
nestabilních	stabilní	k2eNgFnPc2d1	nestabilní
forem	forma	k1gFnPc2	forma
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
trytololáty	trytololát	k1gInPc1	trytololát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
velmi	velmi	k6eAd1	velmi
snadnou	snadný	k2eAgFnSc7d1	snadná
výbušností	výbušnost	k1gFnSc7	výbušnost
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
manipulaci	manipulace	k1gFnSc6	manipulace
s	s	k7c7	s
TNT	TNT	kA	TNT
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
nutno	nutno	k6eAd1	nutno
zabezpečit	zabezpečit	k5eAaPmF	zabezpečit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
nedostal	dostat	k5eNaPmAgMnS	dostat
do	do	k7c2	do
styku	styk	k1gInSc2	styk
s	s	k7c7	s
alkalickými	alkalický	k2eAgFnPc7d1	alkalická
látkami	látka	k1gFnPc7	látka
za	za	k7c2	za
zvýšené	zvýšený	k2eAgFnSc2d1	zvýšená
teploty	teplota	k1gFnSc2	teplota
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pyrotechnické	pyrotechnický	k2eAgFnPc4d1	pyrotechnická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
a	a	k8xC	a
využití	využití	k1gNnSc4	využití
==	==	k?	==
</s>
</p>
<p>
<s>
Jako	jako	k8xC	jako
výbušnina	výbušnina	k1gFnSc1	výbušnina
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
mimořádně	mimořádně	k6eAd1	mimořádně
dobré	dobrý	k2eAgFnPc4d1	dobrá
vlastnosti	vlastnost	k1gFnPc4	vlastnost
–	–	k?	–
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
stabilní	stabilní	k2eAgFnSc1d1	stabilní
a	a	k8xC	a
málo	málo	k6eAd1	málo
citlivá	citlivý	k2eAgFnSc1d1	citlivá
vůči	vůči	k7c3	vůči
vnějším	vnější	k2eAgInPc3d1	vnější
vlivům	vliv	k1gInPc3	vliv
a	a	k8xC	a
přitom	přitom	k6eAd1	přitom
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
velmi	velmi	k6eAd1	velmi
vysokou	vysoký	k2eAgFnSc7d1	vysoká
brizancí	brizance	k1gFnSc7	brizance
a	a	k8xC	a
razancí	razance	k1gFnSc7	razance
výbuchu	výbuch	k1gInSc2	výbuch
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
ideální	ideální	k2eAgFnSc7d1	ideální
látkou	látka	k1gFnSc7	látka
pro	pro	k7c4	pro
přípravu	příprava	k1gFnSc4	příprava
jak	jak	k6eAd1	jak
průmyslových	průmyslový	k2eAgFnPc2d1	průmyslová
tak	tak	k8xC	tak
vojenských	vojenský	k2eAgFnPc2d1	vojenská
trhavin	trhavina	k1gFnPc2	trhavina
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
přitom	přitom	k6eAd1	přitom
v	v	k7c6	v
různém	různý	k2eAgInSc6d1	různý
poměru	poměr	k1gInSc6	poměr
mísí	mísit	k5eAaImIp3nP	mísit
s	s	k7c7	s
jinými	jiný	k2eAgFnPc7d1	jiná
explozivními	explozivní	k2eAgFnPc7d1	explozivní
látkami	látka	k1gFnPc7	látka
a	a	k8xC	a
setkáme	setkat	k5eAaPmIp1nP	setkat
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgInPc4d1	jiný
pod	pod	k7c7	pod
obchodním	obchodní	k2eAgInSc7d1	obchodní
názvem	název	k1gInSc7	název
Permonit	Permonit	k1gInSc1	Permonit
<g/>
,	,	kIx,	,
Permonex	Permonex	k1gInSc1	Permonex
<g/>
,	,	kIx,	,
Karpatit	Karpatit	k1gFnPc1	Karpatit
<g/>
,	,	kIx,	,
známé	známý	k2eAgFnPc1d1	známá
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
vojensky	vojensky	k6eAd1	vojensky
využívané	využívaný	k2eAgFnPc1d1	využívaná
Amatoly	Amatola	k1gFnPc1	Amatola
a	a	k8xC	a
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgNnPc2d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnPc1	jeho
explozivní	explozivní	k2eAgFnPc1d1	explozivní
vlastnosti	vlastnost	k1gFnPc1	vlastnost
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
prozkoumány	prozkoumat	k5eAaPmNgInP	prozkoumat
<g/>
,	,	kIx,	,
uvádí	uvádět	k5eAaImIp3nS	uvádět
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
vyjádření	vyjádření	k1gNnSc4	vyjádření
síly	síla	k1gFnSc2	síla
jaderného	jaderný	k2eAgInSc2d1	jaderný
nebo	nebo	k8xC	nebo
termojaderného	termojaderný	k2eAgInSc2d1	termojaderný
výbuchu	výbuch	k1gInSc2	výbuch
ekvivalentní	ekvivalentní	k2eAgNnSc1d1	ekvivalentní
množství	množství	k1gNnSc1	množství
TNT	TNT	kA	TNT
v	v	k7c6	v
kilotunách	kilotuna	k1gFnPc6	kilotuna
(	(	kIx(	(
<g/>
kt	kt	k?	kt
<g/>
)	)	kIx)	)
či	či	k8xC	či
megatunách	megatuna	k1gFnPc6	megatuna
(	(	kIx(	(
<g/>
Mt	Mt	k1gFnSc1	Mt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
zavádějící	zavádějící	k2eAgMnSc1d1	zavádějící
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
množství	množství	k1gNnSc1	množství
TNT	TNT	kA	TNT
nad	nad	k7c4	nad
100	[number]	k4	100
tun	tuna	k1gFnPc2	tuna
jsou	být	k5eAaImIp3nP	být
schopna	schopen	k2eAgNnPc1d1	schopno
výrazného	výrazný	k2eAgNnSc2d1	výrazné
sekundárního	sekundární	k2eAgNnSc2d1	sekundární
zapálení	zapálení	k1gNnSc2	zapálení
sazí	saze	k1gFnPc2	saze
vzniklých	vzniklý	k2eAgFnPc2d1	vzniklá
po	po	k7c6	po
výbuchu	výbuch	k1gInSc6	výbuch
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
že	že	k8xS	že
100	[number]	k4	100
tun	tuna	k1gFnPc2	tuna
TNT	TNT	kA	TNT
má	mít	k5eAaImIp3nS	mít
reálně	reálně	k6eAd1	reálně
ekvivalent	ekvivalent	k1gInSc4	ekvivalent
uvolněné	uvolněný	k2eAgFnSc2d1	uvolněná
energie	energie	k1gFnSc2	energie
jako	jako	k8xS	jako
130	[number]	k4	130
tunových	tunový	k2eAgFnPc2d1	tunová
náloží	nálož	k1gFnPc2	nálož
(	(	kIx(	(
<g/>
např	např	kA	např
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
již	již	k6eAd1	již
bylo	být	k5eAaImAgNnS	být
zmíněno	zmínit	k5eAaPmNgNnS	zmínit
<g/>
,	,	kIx,	,
typickou	typický	k2eAgFnSc7d1	typická
vlastností	vlastnost	k1gFnSc7	vlastnost
výbuchu	výbuch	k1gInSc2	výbuch
TNT	TNT	kA	TNT
je	být	k5eAaImIp3nS	být
tvorba	tvorba	k1gFnSc1	tvorba
mraku	mrak	k1gInSc2	mrak
sazí	saze	k1gFnPc2	saze
po	po	k7c6	po
explozi	exploze	k1gFnSc6	exploze
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
relativně	relativně	k6eAd1	relativně
nízké	nízký	k2eAgFnSc2d1	nízká
a	a	k8xC	a
záporné	záporný	k2eAgFnSc2d1	záporná
kyslíkové	kyslíkový	k2eAgFnSc2d1	kyslíková
bilance	bilance	k1gFnSc2	bilance
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgInSc3	ten
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
i	i	k9	i
malá	malý	k2eAgNnPc4d1	malé
množství	množství	k1gNnPc4	množství
chovají	chovat	k5eAaImIp3nP	chovat
jako	jako	k9	jako
slabá	slabý	k2eAgFnSc1d1	slabá
termobarická	termobarický	k2eAgFnSc1d1	termobarická
výbušnina	výbušnina	k1gFnSc1	výbušnina
<g/>
,	,	kIx,	,
např.	např.	kA	např.
200	[number]	k4	200
g	g	kA	g
TNT	TNT	kA	TNT
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
při	při	k7c6	při
detonaci	detonace	k1gFnSc6	detonace
reálný	reálný	k2eAgInSc4d1	reálný
vývin	vývin	k1gInSc4	vývin
energie	energie	k1gFnSc2	energie
jako	jako	k9	jako
225	[number]	k4	225
g	g	kA	g
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
oproti	oproti	k7c3	oproti
teoretické	teoretický	k2eAgFnSc3d1	teoretická
hodnotě	hodnota	k1gFnSc3	hodnota
8,2	[number]	k4	8,2
MJ	mj	kA	mj
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
rapidně	rapidně	k6eAd1	rapidně
kolem	kolem	k7c2	kolem
9	[number]	k4	9
MJ	mj	kA	mj
energie	energie	k1gFnSc2	energie
TNT	TNT	kA	TNT
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
TNT	TNT	kA	TNT
překvapivě	překvapivě	k6eAd1	překvapivě
generuje	generovat	k5eAaImIp3nS	generovat
i	i	k9	i
v	v	k7c6	v
malých	malý	k2eAgNnPc6d1	malé
množstvích	množství	k1gNnPc6	množství
tlakové	tlakový	k2eAgFnSc2d1	tlaková
vlny	vlna	k1gFnSc2	vlna
srovnatelné	srovnatelný	k2eAgInPc4d1	srovnatelný
se	s	k7c7	s
Semtexem	semtex	k1gInSc7	semtex
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
naivně	naivně	k6eAd1	naivně
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
dala	dát	k5eAaPmAgFnS	dát
očekávat	očekávat	k5eAaImF	očekávat
20	[number]	k4	20
%	%	kIx~	%
výhoda	výhoda	k1gFnSc1	výhoda
Semtexu	semtex	k1gInSc2	semtex
(	(	kIx(	(
<g/>
toto	tento	k3xDgNnSc4	tento
však	však	k9	však
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
dané	daný	k2eAgFnSc6d1	daná
studi	stud	k1gMnPc1	stud
a	a	k8xC	a
některé	některý	k3yIgNnSc1	některý
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
impulzy	impulz	k1gInPc1	impulz
tlakové	tlakový	k2eAgFnSc2d1	tlaková
vlny	vlna	k1gFnSc2	vlna
konzistentní	konzistentní	k2eAgFnSc1d1	konzistentní
s	s	k7c7	s
nulovou	nulový	k2eAgFnSc7d1	nulová
rychlou	rychlý	k2eAgFnSc7d1	rychlá
oxidací	oxidace	k1gFnSc7	oxidace
sazí	saze	k1gFnPc2	saze
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1	[number]	k4	1
kg	kg	kA	kg
TNT	TNT	kA	TNT
má	mít	k5eAaImIp3nS	mít
50	[number]	k4	50
<g/>
%	%	kIx~	%
úmrtnost	úmrtnost	k1gFnSc1	úmrtnost
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
tlakové	tlakový	k2eAgFnSc2d1	tlaková
vlny	vlna	k1gFnSc2	vlna
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
kolem	kolem	k7c2	kolem
1,4	[number]	k4	1,4
m	m	kA	m
(	(	kIx(	(
<g/>
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
mezi	mezi	k7c7	mezi
náloží	nálož	k1gFnSc7	nálož
a	a	k8xC	a
horní	horní	k2eAgFnSc7d1	horní
polovinou	polovina	k1gFnSc7	polovina
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
orientaci	orientace	k1gFnSc4	orientace
osoby	osoba	k1gFnSc2	osoba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
4,5	[number]	k4	4,5
kg	kg	kA	kg
má	mít	k5eAaImIp3nS	mít
na	na	k7c4	na
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
2,5	[number]	k4	2,5
m	m	kA	m
úmrtnost	úmrtnost	k1gFnSc4	úmrtnost
kolem	kolem	k7c2	kolem
99	[number]	k4	99
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
ani	ani	k8xC	ani
při	při	k7c6	při
použití	použití	k1gNnSc6	použití
10	[number]	k4	10
kg	kg	kA	kg
není	být	k5eNaImIp3nS	být
smrtelná	smrtelný	k2eAgFnSc1d1	smrtelná
zóna	zóna	k1gFnSc1	zóna
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
přibližně	přibližně	k6eAd1	přibližně
4-5	[number]	k4	4-5
m.	m.	k?	m.
Moderní	moderní	k2eAgInSc1d1	moderní
155	[number]	k4	155
<g/>
mm	mm	kA	mm
granáty	granát	k1gInPc4	granát
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
necelých	celý	k2eNgInPc2d1	necelý
11	[number]	k4	11
kg	kg	kA	kg
TNT	TNT	kA	TNT
<g/>
,	,	kIx,	,
60	[number]	k4	60
<g/>
mm	mm	kA	mm
minometný	minometný	k2eAgInSc4d1	minometný
granát	granát	k1gInSc4	granát
má	mít	k5eAaImIp3nS	mít
kolem	kolem	k7c2	kolem
200	[number]	k4	200
g	g	kA	g
a	a	k8xC	a
rozšířené	rozšířený	k2eAgInPc4d1	rozšířený
ruční	ruční	k2eAgInPc4d1	ruční
granáty	granát	k1gInPc4	granát
kolem	kolem	k7c2	kolem
100	[number]	k4	100
g.	g.	k?	g.
V	v	k7c6	v
běžných	běžný	k2eAgFnPc6d1	běžná
bombách	bomba	k1gFnPc6	bomba
se	s	k7c7	s
30-70	[number]	k4	30-70
%	%	kIx~	%
energie	energie	k1gFnSc1	energie
spotřebuje	spotřebovat	k5eAaPmIp3nS	spotřebovat
na	na	k7c4	na
fragmentaci	fragmentace	k1gFnSc4	fragmentace
a	a	k8xC	a
nikoliv	nikoliv	k9	nikoliv
na	na	k7c4	na
generování	generování	k1gNnSc4	generování
tlakové	tlakový	k2eAgFnSc2d1	tlaková
vlny	vlna	k1gFnSc2	vlna
<g/>
.	.	kIx.	.
</s>
<s>
Tlaková	tlakový	k2eAgFnSc1d1	tlaková
vlna	vlna	k1gFnSc1	vlna
tedy	tedy	k9	tedy
obecně	obecně	k6eAd1	obecně
není	být	k5eNaImIp3nS	být
vhodným	vhodný	k2eAgInSc7d1	vhodný
účinným	účinný	k2eAgInSc7d1	účinný
mechanismem	mechanismus	k1gInSc7	mechanismus
likvidace	likvidace	k1gFnSc2	likvidace
živé	živý	k2eAgFnSc2d1	živá
síly	síla	k1gFnSc2	síla
a	a	k8xC	a
tak	tak	k6eAd1	tak
hlavním	hlavní	k2eAgInSc7d1	hlavní
cílem	cíl	k1gInSc7	cíl
TNT	TNT	kA	TNT
je	být	k5eAaImIp3nS	být
urychlování	urychlování	k1gNnSc1	urychlování
střepin	střepina	k1gFnPc2	střepina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Základní	základní	k2eAgFnPc4d1	základní
vlastnosti	vlastnost	k1gFnPc4	vlastnost
výbušniny	výbušnina	k1gFnSc2	výbušnina
===	===	k?	===
</s>
</p>
<p>
<s>
Energie	energie	k1gFnSc1	energie
výbuchu	výbuch	k1gInSc2	výbuch
<g/>
:	:	kIx,	:
4	[number]	k4	4
100	[number]	k4	100
–	–	k?	–
4	[number]	k4	4
220	[number]	k4	220
kJ	kJ	k?	kJ
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
980	[number]	k4	980
<g/>
–	–	k?	–
<g/>
1010	[number]	k4	1010
kcal	kcal	k1gInSc1	kcal
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
sekundární	sekundární	k2eAgFnSc2d1	sekundární
oxidace	oxidace	k1gFnSc2	oxidace
sazí	saze	k1gFnPc2	saze
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Detonační	detonační	k2eAgFnSc1d1	detonační
rychlost	rychlost	k1gFnSc1	rychlost
<g/>
:	:	kIx,	:
6	[number]	k4	6
900	[number]	k4	900
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
</s>
</p>
<p>
<s>
Objem	objem	k1gInSc1	objem
spalných	spalný	k2eAgInPc2d1	spalný
plynů	plyn	k1gInPc2	plyn
<g/>
:	:	kIx,	:
730	[number]	k4	730
l	l	kA	l
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
</s>
</p>
<p>
<s>
Teplota	teplota	k1gFnSc1	teplota
exploze	exploze	k1gFnSc1	exploze
<g/>
:	:	kIx,	:
3	[number]	k4	3
100	[number]	k4	100
°	°	k?	°
<g/>
C	C	kA	C
</s>
</p>
<p>
<s>
Specifické	specifický	k2eAgNnSc4d1	specifické
spalné	spalný	k2eAgNnSc4d1	spalné
teplo	teplo	k1gNnSc4	teplo
<g/>
:	:	kIx,	:
4,184	[number]	k4	4,184
MJ	mj	kA	mj
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
</s>
</p>
<p>
<s>
Detonační	detonační	k2eAgInSc1d1	detonační
tlak	tlak	k1gInSc1	tlak
Pcj	Pcj	k1gFnSc2	Pcj
:	:	kIx,	:
190	[number]	k4	190
-	-	kIx~	-
220	[number]	k4	220
kbar	kbara	k1gFnPc2	kbara
dle	dle	k7c2	dle
výpočetní	výpočetní	k2eAgFnSc2d1	výpočetní
nebo	nebo	k8xC	nebo
experimentální	experimentální	k2eAgFnSc2d1	experimentální
metody	metoda	k1gFnSc2	metoda
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
u	u	k7c2	u
spodní	spodní	k2eAgFnSc2d1	spodní
hranice	hranice	k1gFnSc2	hranice
rozpětí	rozpětí	k1gNnSc2	rozpětí
</s>
</p>
<p>
<s>
==	==	k?	==
Výroba	výroba	k1gFnSc1	výroba
==	==	k?	==
</s>
</p>
<p>
<s>
Základem	základ	k1gInSc7	základ
výroby	výroba	k1gFnSc2	výroba
je	být	k5eAaImIp3nS	být
postupná	postupný	k2eAgFnSc1d1	postupná
nitrace	nitrace	k1gFnSc1	nitrace
aromatického	aromatický	k2eAgInSc2d1	aromatický
uhlovodíku	uhlovodík	k1gInSc2	uhlovodík
toluenu	toluen	k1gInSc2	toluen
směsí	směs	k1gFnPc2	směs
kyseliny	kyselina	k1gFnSc2	kyselina
dusičné	dusičný	k2eAgFnSc2d1	dusičná
a	a	k8xC	a
kyseliny	kyselina	k1gFnSc2	kyselina
sírové	sírový	k2eAgFnSc2d1	sírová
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
nitrace	nitrace	k1gFnSc1	nitrace
do	do	k7c2	do
2	[number]	k4	2
<g/>
.	.	kIx.	.
stupně	stupeň	k1gInSc2	stupeň
(	(	kIx(	(
<g/>
směs	směs	k1gFnSc1	směs
různých	různý	k2eAgInPc2d1	různý
izomerů	izomer	k1gInPc2	izomer
dinitrotoluenů	dinitrotoluen	k1gInPc2	dinitrotoluen
<g/>
)	)	kIx)	)
probíhá	probíhat	k5eAaImIp3nS	probíhat
za	za	k7c2	za
relativně	relativně	k6eAd1	relativně
mírných	mírný	k2eAgFnPc2d1	mírná
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
poslední	poslední	k2eAgInSc4d1	poslední
nitrační	nitrační	k2eAgInSc4d1	nitrační
stupeň	stupeň	k1gInSc4	stupeň
nutno	nutno	k6eAd1	nutno
použít	použít	k5eAaPmF	použít
značně	značně	k6eAd1	značně
drastických	drastický	k2eAgFnPc2d1	drastická
reakčních	reakční	k2eAgFnPc2d1	reakční
podmínek	podmínka	k1gFnPc2	podmínka
–	–	k?	–
nitrace	nitrace	k1gFnSc2	nitrace
probíhá	probíhat	k5eAaImIp3nS	probíhat
obvykle	obvykle	k6eAd1	obvykle
ve	v	k7c6	v
20	[number]	k4	20
<g/>
%	%	kIx~	%
oleu	oleum	k1gNnSc6	oleum
(	(	kIx(	(
<g/>
roztok	roztok	k1gInSc1	roztok
oxidu	oxid	k1gInSc2	oxid
sírového	sírový	k2eAgInSc2d1	sírový
v	v	k7c6	v
koncentrované	koncentrovaný	k2eAgFnSc6d1	koncentrovaná
kyselině	kyselina	k1gFnSc6	kyselina
sírové	sírový	k2eAgFnPc1d1	sírová
<g/>
)	)	kIx)	)
za	za	k7c2	za
teplot	teplota	k1gFnPc2	teplota
kolem	kolem	k7c2	kolem
80	[number]	k4	80
°	°	k?	°
<g/>
C.	C.	kA	C.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
přípravy	příprava	k1gFnSc2	příprava
TNT	TNT	kA	TNT
přitom	přitom	k6eAd1	přitom
vzniká	vznikat	k5eAaImIp3nS	vznikat
pestrá	pestrý	k2eAgFnSc1d1	pestrá
směs	směs	k1gFnSc1	směs
různých	různý	k2eAgInPc2d1	různý
vedlejších	vedlejší	k2eAgInPc2d1	vedlejší
produktů	produkt	k1gInPc2	produkt
a	a	k8xC	a
pro	pro	k7c4	pro
docílení	docílení	k1gNnSc4	docílení
kvalitního	kvalitní	k2eAgInSc2d1	kvalitní
produktu	produkt	k1gInSc2	produkt
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
co	co	k3yRnSc4	co
nejlépe	dobře	k6eAd3	dobře
izolovat	izolovat	k5eAaBmF	izolovat
právě	právě	k9	právě
požadovaný	požadovaný	k2eAgInSc4d1	požadovaný
2,4	[number]	k4	2,4
<g/>
,6	,6	k4	,6
<g/>
-trinitromethylbenzen	rinitromethylbenzna	k1gFnPc2	-trinitromethylbenzna
<g/>
.	.	kIx.	.
</s>
<s>
Způsob	způsob	k1gInSc1	způsob
jak	jak	k9	jak
toho	ten	k3xDgInSc2	ten
docílit	docílit	k5eAaPmF	docílit
s	s	k7c7	s
minimálními	minimální	k2eAgInPc7d1	minimální
výrobními	výrobní	k2eAgInPc7d1	výrobní
náklady	náklad	k1gInPc7	náklad
<g/>
,	,	kIx,	,
maximálním	maximální	k2eAgInSc7d1	maximální
reakčním	reakční	k2eAgInSc7d1	reakční
výtěžkem	výtěžek	k1gInSc7	výtěžek
a	a	k8xC	a
co	co	k9	co
největším	veliký	k2eAgNnSc7d3	veliký
bezpečím	bezpečí	k1gNnSc7	bezpečí
pro	pro	k7c4	pro
obsluhující	obsluhující	k2eAgInSc4d1	obsluhující
personál	personál	k1gInSc4	personál
je	být	k5eAaImIp3nS	být
předmětem	předmět	k1gInSc7	předmět
pečlivě	pečlivě	k6eAd1	pečlivě
chráněných	chráněný	k2eAgNnPc2d1	chráněné
výrobních	výrobní	k2eAgNnPc2d1	výrobní
tajemství	tajemství	k1gNnPc2	tajemství
každé	každý	k3xTgFnSc2	každý
firmy	firma	k1gFnSc2	firma
<g/>
,	,	kIx,	,
zabývající	zabývající	k2eAgFnSc4d1	zabývající
se	se	k3xPyFc4	se
průmyslovou	průmyslový	k2eAgFnSc7d1	průmyslová
výrobou	výroba	k1gFnSc7	výroba
této	tento	k3xDgFnSc2	tento
užitečné	užitečný	k2eAgFnSc2d1	užitečná
výbušniny	výbušnina	k1gFnSc2	výbušnina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
==	==	k?	==
</s>
</p>
<p>
<s>
TNT	TNT	kA	TNT
je	být	k5eAaImIp3nS	být
jedovatý	jedovatý	k2eAgInSc4d1	jedovatý
<g/>
,	,	kIx,	,
styk	styk	k1gInSc4	styk
s	s	k7c7	s
kůží	kůže	k1gFnSc7	kůže
může	moct	k5eAaImIp3nS	moct
vyvolat	vyvolat	k5eAaPmF	vyvolat
její	její	k3xOp3gNnSc4	její
podráždění	podráždění	k1gNnSc4	podráždění
a	a	k8xC	a
zbarvení	zbarvení	k1gNnSc4	zbarvení
do	do	k7c2	do
žlutooranžova	žlutooranžův	k2eAgNnSc2d1	žlutooranžův
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
pracovnicím	pracovnice	k1gFnPc3	pracovnice
nakládajícím	nakládající	k2eAgMnSc7d1	nakládající
s	s	k7c7	s
TNT	TNT	kA	TNT
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
munice	munice	k1gFnSc2	munice
zbarvovala	zbarvovat	k5eAaImAgFnS	zbarvovat
kůže	kůže	k1gFnSc1	kůže
do	do	k7c2	do
jasné	jasný	k2eAgFnSc2d1	jasná
žluté	žlutý	k2eAgFnSc2d1	žlutá
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
získaly	získat	k5eAaPmAgFnP	získat
přezdívku	přezdívka	k1gFnSc4	přezdívka
"	"	kIx"	"
<g/>
kanárkové	kanárkový	k2eAgFnPc4d1	kanárková
dívky	dívka	k1gFnPc4	dívka
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
jednoduše	jednoduše	k6eAd1	jednoduše
"	"	kIx"	"
<g/>
kanárci	kanárek	k1gMnPc1	kanárek
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lidé	člověk	k1gMnPc1	člověk
vystaveni	vystavit	k5eAaPmNgMnP	vystavit
po	po	k7c4	po
delší	dlouhý	k2eAgInSc4d2	delší
čas	čas	k1gInSc4	čas
expozici	expozice	k1gFnSc4	expozice
TNT	TNT	kA	TNT
mohou	moct	k5eAaImIp3nP	moct
trpět	trpět	k5eAaImF	trpět
anémií	anémie	k1gFnSc7	anémie
nebo	nebo	k8xC	nebo
poruchami	porucha	k1gFnPc7	porucha
funkce	funkce	k1gFnSc2	funkce
jater	játra	k1gNnPc2	játra
<g/>
.	.	kIx.	.
</s>
<s>
Účinky	účinek	k1gInPc1	účinek
na	na	k7c4	na
krev	krev	k1gFnSc4	krev
a	a	k8xC	a
játra	játra	k1gNnPc4	játra
<g/>
,	,	kIx,	,
zvětšování	zvětšování	k1gNnSc4	zvětšování
sleziny	slezina	k1gFnSc2	slezina
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
škodlivé	škodlivý	k2eAgInPc4d1	škodlivý
účinky	účinek	k1gInPc4	účinek
na	na	k7c4	na
imunitní	imunitní	k2eAgInSc4d1	imunitní
systém	systém	k1gInSc4	systém
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
také	také	k9	také
u	u	k7c2	u
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
požila	požít	k5eAaPmAgFnS	požít
nebo	nebo	k8xC	nebo
vdechovala	vdechovat	k5eAaImAgFnS	vdechovat
TNT	TNT	kA	TNT
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
prokázáno	prokázat	k5eAaPmNgNnS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
TNT	TNT	kA	TNT
nepříznivě	příznivě	k6eNd1	příznivě
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
reprodukční	reprodukční	k2eAgFnPc4d1	reprodukční
schopnosti	schopnost	k1gFnPc4	schopnost
u	u	k7c2	u
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
TNT	TNT	kA	TNT
je	být	k5eAaImIp3nS	být
také	také	k9	také
uveden	uvést	k5eAaPmNgInS	uvést
na	na	k7c6	na
seznamu	seznam	k1gInSc6	seznam
možných	možný	k2eAgInPc2d1	možný
karcinogenů	karcinogen	k1gInPc2	karcinogen
<g/>
.	.	kIx.	.
</s>
<s>
Konzumace	konzumace	k1gFnSc1	konzumace
TNT	TNT	kA	TNT
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
červené	červený	k2eAgNnSc4d1	červené
zbarvení	zbarvení	k1gNnSc4	zbarvení
moči	moč	k1gFnSc2	moč
<g/>
.	.	kIx.	.
</s>
<s>
Nikoli	nikoli	k9	nikoli
ovšem	ovšem	k9	ovšem
kvůli	kvůli	k7c3	kvůli
přítomnosti	přítomnost	k1gFnSc3	přítomnost
krve	krev	k1gFnSc2	krev
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
občas	občas	k6eAd1	občas
chybně	chybně	k6eAd1	chybně
uváděno	uvádět	k5eAaImNgNnS	uvádět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kvůli	kvůli	k7c3	kvůli
zbarvení	zbarvení	k1gNnSc3	zbarvení
produktů	produkt	k1gInPc2	produkt
rozkladu	rozklad	k1gInSc2	rozklad
TNT	TNT	kA	TNT
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhodobá	dlouhodobý	k2eAgFnSc1d1	dlouhodobá
expozice	expozice	k1gFnSc1	expozice
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
zákaly	zákal	k1gInPc4	zákal
čočky	čočka	k1gFnSc2	čočka
v	v	k7c6	v
ekvatoriální	ekvatoriální	k2eAgFnSc6d1	ekvatoriální
oblasti	oblast	k1gFnSc6	oblast
(	(	kIx(	(
<g/>
konsekutivní	konsekutivní	k2eAgFnSc1d1	konsekutivní
katarakta	katarakta	k1gFnSc1	katarakta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
uznávána	uznáván	k2eAgFnSc1d1	uznávána
jako	jako	k8xC	jako
nemoc	nemoc	k1gFnSc1	nemoc
z	z	k7c2	z
povolání	povolání	k1gNnSc2	povolání
<g/>
.	.	kIx.	.
<g/>
Některé	některý	k3yIgFnPc4	některý
vojenské	vojenský	k2eAgFnPc4d1	vojenská
zkušební	zkušební	k2eAgFnPc4d1	zkušební
oblasti	oblast	k1gFnPc4	oblast
jsou	být	k5eAaImIp3nP	být
kontaminovány	kontaminovat	k5eAaBmNgFnP	kontaminovat
TNT	TNT	kA	TNT
<g/>
.	.	kIx.	.
</s>
<s>
Odpadní	odpadní	k2eAgFnPc1d1	odpadní
vody	voda	k1gFnPc1	voda
z	z	k7c2	z
výroby	výroba	k1gFnSc2	výroba
munice	munice	k1gFnSc2	munice
<g/>
,	,	kIx,	,
kontaminované	kontaminovaný	k2eAgInPc4d1	kontaminovaný
povrchy	povrch	k1gInPc4	povrch
a	a	k8xC	a
podpovrchové	podpovrchový	k2eAgFnSc2d1	podpovrchová
vody	voda	k1gFnSc2	voda
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
vzhledem	vzhled	k1gInSc7	vzhled
k	k	k7c3	k
přítomnosti	přítomnost	k1gFnSc3	přítomnost
TNT	TNT	kA	TNT
zbarveny	zbarven	k2eAgInPc1d1	zbarven
do	do	k7c2	do
růžova	růžovo	k1gNnSc2	růžovo
<g/>
.	.	kIx.	.
</s>
<s>
Odstranění	odstranění	k1gNnSc1	odstranění
takové	takový	k3xDgFnSc2	takový
kontaminace	kontaminace	k1gFnSc2	kontaminace
<g/>
,	,	kIx,	,
zvané	zvaný	k2eAgFnPc1d1	zvaná
též	též	k6eAd1	též
"	"	kIx"	"
<g/>
růžová	růžový	k2eAgFnSc1d1	růžová
voda	voda	k1gFnSc1	voda
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
obtížné	obtížný	k2eAgNnSc1d1	obtížné
a	a	k8xC	a
drahé	drahý	k2eAgNnSc1d1	drahé
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
TNT	TNT	kA	TNT
je	být	k5eAaImIp3nS	být
relativně	relativně	k6eAd1	relativně
necitlivý	citlivý	k2eNgMnSc1d1	necitlivý
na	na	k7c4	na
náraz	náraz	k1gInSc4	náraz
<g/>
,	,	kIx,	,
statickou	statický	k2eAgFnSc4d1	statická
elektřinu	elektřina	k1gFnSc4	elektřina
<g/>
,	,	kIx,	,
tření	tření	k1gNnSc4	tření
a	a	k8xC	a
zvýšenou	zvýšený	k2eAgFnSc4d1	zvýšená
teplotu	teplota	k1gFnSc4	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Běžné	běžný	k2eAgInPc1d1	běžný
údery	úder	k1gInPc1	úder
kladivem	kladivo	k1gNnSc7	kladivo
<g/>
,	,	kIx,	,
běžné	běžný	k2eAgInPc1d1	běžný
elektrostatické	elektrostatický	k2eAgInPc1d1	elektrostatický
výboje	výboj	k1gInPc1	výboj
a	a	k8xC	a
obvyklé	obvyklý	k2eAgNnSc1d1	obvyklé
tření	tření	k1gNnSc1	tření
ho	on	k3xPp3gMnSc4	on
nedetonují	detonovat	k5eNaBmIp3nP	detonovat
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
malá	malý	k2eAgNnPc1d1	malé
množství	množství	k1gNnPc1	množství
TNT	TNT	kA	TNT
po	po	k7c6	po
zapálení	zapálení	k1gNnSc6	zapálení
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
pevného	pevný	k2eAgInSc2d1	pevný
obalu	obal	k1gInSc2	obal
<g/>
)	)	kIx)	)
nedetonují	detonovat	k5eNaBmIp3nP	detonovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
TNT	TNT	kA	TNT
je	být	k5eAaImIp3nS	být
náchylný	náchylný	k2eAgInSc1d1	náchylný
k	k	k7c3	k
vylučování	vylučování	k1gNnSc3	vylučování
dinitrotoluenů	dinitrotoluen	k1gInPc2	dinitrotoluen
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
efekt	efekt	k1gInSc4	efekt
mohou	moct	k5eAaImIp3nP	moct
vyvolat	vyvolat	k5eAaPmF	vyvolat
i	i	k9	i
malá	malý	k2eAgNnPc4d1	malé
množství	množství	k1gNnPc4	množství
takových	takový	k3xDgFnPc2	takový
nečistot	nečistota	k1gFnPc2	nečistota
<g/>
.	.	kIx.	.
</s>
<s>
Projevuje	projevovat	k5eAaImIp3nS	projevovat
se	se	k3xPyFc4	se
zvláště	zvláště	k6eAd1	zvláště
u	u	k7c2	u
střel	střela	k1gFnPc2	střela
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
TNT	TNT	kA	TNT
uložených	uložený	k2eAgInPc2d1	uložený
při	při	k7c6	při
vyšších	vysoký	k2eAgFnPc6d2	vyšší
teplotách	teplota	k1gFnPc6	teplota
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Vylučování	vylučování	k1gNnSc1	vylučování
nečistot	nečistota	k1gFnPc2	nečistota
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
tvorbě	tvorba	k1gFnSc3	tvorba
pórů	pór	k1gInPc2	pór
a	a	k8xC	a
prasklin	prasklina	k1gFnPc2	prasklina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mohou	moct	k5eAaImIp3nP	moct
vést	vést	k5eAaImF	vést
ke	k	k7c3	k
zvýšení	zvýšení	k1gNnSc3	zvýšení
citlivosti	citlivost	k1gFnSc2	citlivost
na	na	k7c4	na
náraz	náraz	k1gInSc4	náraz
<g/>
.	.	kIx.	.
</s>
<s>
Migrací	migrace	k1gFnSc7	migrace
takto	takto	k6eAd1	takto
vyloučené	vyloučený	k2eAgFnSc2d1	vyloučená
kapaliny	kapalina	k1gFnSc2	kapalina
k	k	k7c3	k
zapalovači	zapalovač	k1gInSc3	zapalovač
vznikají	vznikat	k5eAaImIp3nP	vznikat
ohňové	ohňový	k2eAgInPc1d1	ohňový
kanály	kanál	k1gInPc1	kanál
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
riziko	riziko	k1gNnSc4	riziko
náhodné	náhodný	k2eAgFnSc2d1	náhodná
detonace	detonace	k1gFnSc2	detonace
<g/>
;	;	kIx,	;
kvůli	kvůli	k7c3	kvůli
těmto	tento	k3xDgFnPc3	tento
kapalinám	kapalina	k1gFnPc3	kapalina
může	moct	k5eAaImIp3nS	moct
také	také	k9	také
selhat	selhat	k5eAaPmF	selhat
zapalovač	zapalovač	k1gInSc4	zapalovač
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Trinitrotoluene	trinitrotoluen	k1gInSc5	trinitrotoluen
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Trinitrotoluen	trinitrotoluen	k1gInSc1	trinitrotoluen
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
