<s>
Trinitrotoluen	trinitrotoluen	k1gInSc1	trinitrotoluen
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
označovaný	označovaný	k2eAgInSc1d1	označovaný
jako	jako	k8xC	jako
TNT	TNT	kA	TNT
nebo	nebo	k8xC	nebo
tritol	tritol	k1gInSc1	tritol
<g/>
,	,	kIx,	,
chemicky	chemicky	k6eAd1	chemicky
názvem	název	k1gInSc7	název
2,4	[number]	k4	2,4
<g/>
,6	,6	k4	,6
<g/>
-trinitrotoluen	rinitrotoluna	k1gFnPc2	-trinitrotoluna
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
silná	silný	k2eAgFnSc1d1	silná
<g/>
,	,	kIx,	,
relativně	relativně	k6eAd1	relativně
bezpečná	bezpečný	k2eAgFnSc1d1	bezpečná
a	a	k8xC	a
hojně	hojně	k6eAd1	hojně
používaná	používaný	k2eAgFnSc1d1	používaná
trhavina	trhavina	k1gFnSc1	trhavina
<g/>
.	.	kIx.	.
</s>
