<p>
<s>
Neverwinter	Neverwinter	k1gInSc1	Neverwinter
Nights	Nights	k1gInSc1	Nights
je	být	k5eAaImIp3nS	být
RPG	RPG	kA	RPG
hra	hra	k1gFnSc1	hra
postavená	postavený	k2eAgFnSc1d1	postavená
na	na	k7c6	na
herním	herní	k2eAgInSc6d1	herní
systému	systém	k1gInSc6	systém
Dungeons	Dungeons	k1gInSc1	Dungeons
&	&	k?	&
Dragons	Dragons	k1gInSc1	Dragons
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Neverwinter	Neverwintrum	k1gNnPc2	Neverwintrum
Nights	Nightsa	k1gFnPc2	Nightsa
==	==	k?	==
</s>
</p>
<p>
<s>
Hru	hra	k1gFnSc4	hra
Neverwinter	Neverwintra	k1gFnPc2	Neverwintra
Nights	Nightsa	k1gFnPc2	Nightsa
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
firma	firma	k1gFnSc1	firma
BioWare	BioWar	k1gMnSc5	BioWar
a	a	k8xC	a
byla	být	k5eAaImAgNnP	být
vydaná	vydaný	k2eAgNnPc1d1	vydané
firmou	firma	k1gFnSc7	firma
Infogrames	Infogrames	k1gMnSc1	Infogrames
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
Atari	Atari	k1gNnSc1	Atari
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
hru	hra	k1gFnSc4	hra
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
třetí	třetí	k4xOgFnSc2	třetí
osoby	osoba	k1gFnSc2	osoba
založenou	založený	k2eAgFnSc4d1	založená
na	na	k7c6	na
RPG	RPG	kA	RPG
a	a	k8xC	a
využívající	využívající	k2eAgNnSc4d1	využívající
třetí	třetí	k4xOgNnSc4	třetí
vydání	vydání	k1gNnSc4	vydání
pravidel	pravidlo	k1gNnPc2	pravidlo
Dungeons	Dungeonsa	k1gFnPc2	Dungeonsa
&	&	k?	&
Dragons	Dragonsa	k1gFnPc2	Dragonsa
a	a	k8xC	a
herním	herní	k2eAgInSc6d1	herní
světě	svět	k1gInSc6	svět
Forgotten	Forgotten	k2eAgInSc4d1	Forgotten
Realms	Realms	k1gInSc4	Realms
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Infogrames	Infogrames	k1gInSc4	Infogrames
vydali	vydat	k5eAaPmAgMnP	vydat
Neverwinter	Neverwinter	k1gInSc4	Neverwinter
Nights	Nightsa	k1gFnPc2	Nightsa
pro	pro	k7c4	pro
Windows	Windows	kA	Windows
18	[number]	k4	18
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
BioWare	BioWar	k1gMnSc5	BioWar
vydali	vydat	k5eAaPmAgMnP	vydat
také	také	k9	také
zdarma	zdarma	k6eAd1	zdarma
stáhnutelného	stáhnutelný	k2eAgMnSc4d1	stáhnutelný
klienta	klient	k1gMnSc4	klient
pro	pro	k7c4	pro
Linux	linux	k1gInSc4	linux
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2003	[number]	k4	2003
(	(	kIx(	(
<g/>
nákup	nákup	k1gInSc1	nákup
hry	hra	k1gFnSc2	hra
byl	být	k5eAaImAgInS	být
ale	ale	k8xC	ale
pořád	pořád	k6eAd1	pořád
nutný	nutný	k2eAgInSc1d1	nutný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
MacSoft	MacSoft	k1gInSc1	MacSoft
vydal	vydat	k5eAaPmAgInS	vydat
pro	pro	k7c4	pro
Mac	Mac	kA	Mac
OS	OS	kA	OS
X	X	kA	X
hru	hra	k1gFnSc4	hra
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozšiřující	rozšiřující	k2eAgInPc1d1	rozšiřující
datadisky	datadisek	k1gInPc1	datadisek
Shadows	Shadowsa	k1gFnPc2	Shadowsa
of	of	k?	of
Undrentide	Undrentid	k1gInSc5	Undrentid
a	a	k8xC	a
Hordes	Hordes	k1gInSc1	Hordes
of	of	k?	of
Underdark	Underdark	k1gInSc1	Underdark
byly	být	k5eAaImAgFnP	být
vydány	vydat	k5eAaPmNgInP	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příběh	příběh	k1gInSc1	příběh
==	==	k?	==
</s>
</p>
<p>
<s>
Hra	hra	k1gFnSc1	hra
začíná	začínat	k5eAaImIp3nS	začínat
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
Neverwinter	Neverwintrum	k1gNnPc2	Neverwintrum
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
řádí	řádit	k5eAaImIp3nS	řádit
mor	mor	k1gInSc4	mor
<g/>
.	.	kIx.	.
</s>
<s>
Vy	vy	k3xPp2nPc1	vy
jako	jako	k9	jako
nově	nově	k6eAd1	nově
vycvičený	vycvičený	k2eAgMnSc1d1	vycvičený
hrdina	hrdina	k1gMnSc1	hrdina
máte	mít	k5eAaImIp2nP	mít
pomoci	pomoct	k5eAaPmF	pomoct
bojovat	bojovat	k5eAaImF	bojovat
proti	proti	k7c3	proti
moru	mor	k1gInSc3	mor
a	a	k8xC	a
s	s	k7c7	s
jinými	jiný	k2eAgFnPc7d1	jiná
důležitými	důležitý	k2eAgFnPc7d1	důležitá
věcmi	věc	k1gFnPc7	věc
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c4	v
poslední	poslední	k2eAgInSc4d1	poslední
den	den	k1gInSc4	den
vašeho	váš	k3xOp2gInSc2	váš
výcviku	výcvik	k1gInSc2	výcvik
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
akademii	akademie	k1gFnSc6	akademie
na	na	k7c4	na
které	který	k3yIgFnPc4	který
studujete	studovat	k5eAaImIp2nP	studovat
podniknut	podniknut	k2eAgInSc4d1	podniknut
útok	útok	k1gInSc4	útok
a	a	k8xC	a
dlouho	dlouho	k6eAd1	dlouho
očekávaný	očekávaný	k2eAgInSc4d1	očekávaný
lék	lék	k1gInSc4	lék
na	na	k7c4	na
mor	mor	k1gInSc4	mor
je	být	k5eAaImIp3nS	být
ukraden	ukrást	k5eAaPmNgInS	ukrást
<g/>
.	.	kIx.	.
</s>
<s>
Vy	vy	k3xPp2nPc1	vy
jako	jako	k8xS	jako
správný	správný	k2eAgMnSc1d1	správný
dobrodruh	dobrodruh	k1gMnSc1	dobrodruh
se	se	k3xPyFc4	se
jej	on	k3xPp3gInSc2	on
vydáváte	vydávat	k5eAaImIp2nP	vydávat
hledat	hledat	k5eAaImF	hledat
<g/>
.	.	kIx.	.
</s>
<s>
Hledání	hledání	k1gNnSc2	hledání
vás	vy	k3xPp2nPc4	vy
provede	provést	k5eAaPmIp3nS	provést
celým	celý	k2eAgNnSc7d1	celé
městem	město	k1gNnSc7	město
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nakonec	nakonec	k6eAd1	nakonec
stvůry	stvůra	k1gFnPc1	stvůra
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
lék	lék	k1gInSc4	lék
na	na	k7c4	na
mor	mor	k1gInSc4	mor
<g/>
,	,	kIx,	,
najdete	najít	k5eAaPmIp2nP	najít
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
však	však	k8xC	však
hra	hra	k1gFnSc1	hra
nekončí	končit	k5eNaImIp3nS	končit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
teprve	teprve	k6eAd1	teprve
začíná	začínat	k5eAaImIp3nS	začínat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
O	o	k7c6	o
hře	hra	k1gFnSc6	hra
samotné	samotný	k2eAgNnSc1d1	samotné
==	==	k?	==
</s>
</p>
<p>
<s>
Hra	hra	k1gFnSc1	hra
vyniká	vynikat	k5eAaImIp3nS	vynikat
multiplayerovým	multiplayerův	k2eAgInSc7d1	multiplayerův
systémem	systém	k1gInSc7	systém
a	a	k8xC	a
nástrojem	nástroj	k1gInSc7	nástroj
na	na	k7c4	na
tvorbu	tvorba	k1gFnSc4	tvorba
a	a	k8xC	a
vedení	vedení	k1gNnSc4	vedení
dobrodružství	dobrodružství	k1gNnSc2	dobrodružství
<g/>
.	.	kIx.	.
</s>
<s>
Pán	pán	k1gMnSc1	pán
jeskyně	jeskyně	k1gFnSc2	jeskyně
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
dobrodružství	dobrodružství	k1gNnSc4	dobrodružství
pomocí	pomocí	k7c2	pomocí
nástroje	nástroj	k1gInSc2	nástroj
"	"	kIx"	"
<g/>
nwn	nwn	k?	nwn
Aurora	Aurora	k1gFnSc1	Aurora
Toolset	Toolset	k1gMnSc1	Toolset
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
pak	pak	k6eAd1	pak
může	moct	k5eAaImIp3nS	moct
uložit	uložit	k5eAaPmF	uložit
na	na	k7c4	na
server	server	k1gInSc4	server
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
připojují	připojovat	k5eAaImIp3nP	připojovat
hráči	hráč	k1gMnPc1	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
server	server	k1gInSc4	server
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
připojeno	připojit	k5eAaPmNgNnS	připojit
až	až	k9	až
64	[number]	k4	64
hráčů	hráč	k1gMnPc2	hráč
a	a	k8xC	a
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
servery	server	k1gInPc4	server
spolu	spolu	k6eAd1	spolu
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
navzájem	navzájem	k6eAd1	navzájem
propojeny	propojit	k5eAaPmNgFnP	propojit
<g/>
.	.	kIx.	.
</s>
<s>
Herní	herní	k2eAgInSc1d1	herní
svět	svět	k1gInSc1	svět
může	moct	k5eAaImIp3nS	moct
pán	pán	k1gMnSc1	pán
jeskyně	jeskyně	k1gFnSc2	jeskyně
(	(	kIx(	(
<g/>
DM	dm	kA	dm
<g/>
)	)	kIx)	)
moderovat	moderovat	k5eAaBmF	moderovat
přímo	přímo	k6eAd1	přímo
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
postava	postava	k1gFnSc1	postava
<g/>
.	.	kIx.	.
</s>
<s>
Hra	hra	k1gFnSc1	hra
běží	běžet	k5eAaImIp3nS	běžet
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
pravidel	pravidlo	k1gNnPc2	pravidlo
Dungeons	Dungeons	k1gInSc1	Dungeons
and	and	k?	and
Dragons	Dragons	k1gInSc1	Dragons
–	–	k?	–
třetí	třetí	k4xOgNnSc4	třetí
vydání	vydání	k1gNnSc4	vydání
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
7	[number]	k4	7
ras	rasa	k1gFnPc2	rasa
od	od	k7c2	od
půl-orka	půlrka	k1gFnSc1	půl-orka
až	až	k9	až
po	po	k7c4	po
elfa	elf	k1gMnSc4	elf
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
také	také	k9	také
11	[number]	k4	11
povolání	povolání	k1gNnPc2	povolání
–	–	k?	–
od	od	k7c2	od
barbara	barbar	k1gMnSc2	barbar
přes	přes	k7c4	přes
čaroděje	čaroděj	k1gMnSc4	čaroděj
až	až	k9	až
ke	k	k7c3	k
klerikovi	klerik	k1gMnSc3	klerik
<g/>
.	.	kIx.	.
</s>
<s>
Potěší	potěšit	k5eAaPmIp3nS	potěšit
také	také	k9	také
tvorba	tvorba	k1gFnSc1	tvorba
vlastní	vlastní	k2eAgFnSc2d1	vlastní
postavy	postava	k1gFnSc2	postava
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
libovolné	libovolný	k2eAgFnSc2d1	libovolná
kombinace	kombinace	k1gFnSc2	kombinace
rasy	rasa	k1gFnSc2	rasa
a	a	k8xC	a
povolání	povolání	k1gNnSc2	povolání
<g/>
.	.	kIx.	.
</s>
<s>
Sice	sice	k8xC	sice
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
výsledná	výsledný	k2eAgFnSc1d1	výsledná
postava	postava	k1gFnSc1	postava
slabá	slabý	k2eAgFnSc1d1	slabá
v	v	k7c6	v
daném	daný	k2eAgNnSc6d1	dané
povolání	povolání	k1gNnSc6	povolání
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
vyloženě	vyloženě	k6eAd1	vyloženě
její	její	k3xOp3gFnSc1	její
rasa	rasa	k1gFnSc1	rasa
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
úkol	úkol	k1gInSc4	úkol
nehodí	hodit	k5eNaPmIp3nS	hodit
(	(	kIx(	(
<g/>
gnómský	gnómský	k2eAgMnSc1d1	gnómský
bojovník	bojovník	k1gMnSc1	bojovník
nebo	nebo	k8xC	nebo
trpasličí	trpasličí	k2eAgMnSc1d1	trpasličí
čaroděj	čaroděj	k1gMnSc1	čaroděj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
potěší	potěšit	k5eAaPmIp3nS	potěšit
ta	ten	k3xDgFnSc1	ten
možnost	možnost	k1gFnSc1	možnost
že	že	k8xS	že
nejste	být	k5eNaImIp2nP	být
nijak	nijak	k6eAd1	nijak
omezeni	omezit	k5eAaPmNgMnP	omezit
<g/>
.	.	kIx.	.
</s>
<s>
Opět	opět	k6eAd1	opět
si	se	k3xPyFc3	se
můžete	moct	k5eAaImIp2nP	moct
vybrat	vybrat	k5eAaPmF	vybrat
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
v	v	k7c6	v
Baldur	Baldura	k1gFnPc2	Baldura
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Gate	Gate	k1gFnPc7	Gate
II	II	kA	II
jestli	jestli	k8xS	jestli
bude	být	k5eAaImBp3nS	být
vaše	váš	k3xOp2gFnSc1	váš
postava	postava	k1gFnSc1	postava
neutrálně	neutrálně	k6eAd1	neutrálně
dobrá	dobrý	k2eAgFnSc1d1	dobrá
<g/>
/	/	kIx~	/
<g/>
zlá	zlý	k2eAgFnSc1d1	zlá
<g/>
,	,	kIx,	,
zákonně	zákonně	k6eAd1	zákonně
dobrá	dobrý	k2eAgFnSc1d1	dobrá
<g/>
/	/	kIx~	/
<g/>
zlá	zlý	k2eAgFnSc1d1	zlá
<g/>
,	,	kIx,	,
zmatené	zmatený	k2eAgNnSc1d1	zmatené
dobro	dobro	k1gNnSc1	dobro
<g/>
/	/	kIx~	/
<g/>
zlo	zlo	k1gNnSc1	zlo
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Neverwinter	Neverwinter	k1gInSc1	Neverwinter
Nights	Nights	k1gInSc1	Nights
může	moct	k5eAaImIp3nS	moct
nahradit	nahradit	k5eAaPmF	nahradit
klasické	klasický	k2eAgFnPc4d1	klasická
večerní	večerní	k2eAgFnPc4d1	večerní
seance	seance	k1gFnPc4	seance
hráčů	hráč	k1gMnPc2	hráč
AD	ad	k7c4	ad
<g/>
&	&	k?	&
<g/>
D	D	kA	D
s	s	k7c7	s
tužkou	tužka	k1gFnSc7	tužka
<g/>
,	,	kIx,	,
papírem	papír	k1gInSc7	papír
a	a	k8xC	a
kostkami	kostka	k1gFnPc7	kostka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Equilibrie	Equilibrie	k1gFnSc1	Equilibrie
–	–	k?	–
český	český	k2eAgMnSc1d1	český
HC	HC	kA	HC
RP	RP	kA	RP
online	onlinout	k5eAaPmIp3nS	onlinout
perzistentní	perzistentní	k2eAgInPc4d1	perzistentní
fantasy	fantas	k1gInPc4	fantas
svět	svět	k1gInSc1	svět
postavený	postavený	k2eAgInSc1d1	postavený
na	na	k7c4	na
Neverwinter	Neverwinter	k1gInSc4	Neverwinter
Nights	Nights	k1gInSc4	Nights
(	(	kIx(	(
<g/>
Dle	dle	k7c2	dle
výsledku	výsledek	k1gInSc2	výsledek
ankety	anketa	k1gFnSc2	anketa
dostal	dostat	k5eAaPmAgInS	dostat
tento	tento	k3xDgInSc1	tento
server	server	k1gInSc1	server
hodnocení	hodnocení	k1gNnSc2	hodnocení
<g/>
:	:	kIx,	:
5	[number]	k4	5
<g/>
/	/	kIx~	/
<g/>
10	[number]	k4	10
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Thalie	Thalie	k1gFnSc1	Thalie
–	–	k?	–
český	český	k2eAgMnSc1d1	český
HC	HC	kA	HC
RP	RP	kA	RP
online	onlinout	k5eAaPmIp3nS	onlinout
perzistentní	perzistentní	k2eAgInPc4d1	perzistentní
fantasy	fantas	k1gInPc4	fantas
svět	svět	k1gInSc1	svět
</s>
</p>
<p>
<s>
Demona	Demona	k1gFnSc1	Demona
–	–	k?	–
český	český	k2eAgInSc1d1	český
RP	RP	kA	RP
online	onlinout	k5eAaPmIp3nS	onlinout
perzistentní	perzistentní	k2eAgInPc4d1	perzistentní
fantasy	fantas	k1gInPc4	fantas
svět	svět	k1gInSc1	svět
</s>
</p>
<p>
<s>
ABCgames	ABCgames	k1gInSc1	ABCgames
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
–	–	k?	–
vše	všechen	k3xTgNnSc4	všechen
o	o	k7c4	o
Neverwinter	Neverwinter	k1gInSc4	Neverwinter
Nights	Nightsa	k1gFnPc2	Nightsa
</s>
</p>
