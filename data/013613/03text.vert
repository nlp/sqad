<s>
Jan	Jan	k1gMnSc1
Hrubý	Hrubý	k1gMnSc1
(	(	kIx(
<g/>
voják	voják	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Jan	Jan	k1gMnSc1
Hrubý	Hrubý	k1gMnSc1
četař	četař	k1gMnSc1
Jan	Jan	k1gMnSc1
Hrubý	Hrubý	k1gMnSc1
Narození	narození	k1gNnSc4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1915	#num#	k4
<g/>
KunoviceRakousko-Uhersko	KunoviceRakousko-Uhersko	k1gNnSc4
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc4
Úmrtí	úmrtí	k1gNnPc1
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1942	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
27	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
PrahaProtektorát	PrahaProtektorát	k1gInSc1
Čechy	Čechy	k1gFnPc1
a	a	k8xC
Morava	Morava	k1gFnSc1
Protektorát	protektorát	k1gInSc1
Čechy	Čechy	k1gFnPc1
a	a	k8xC
Morava	Morava	k1gFnSc1
Místo	místo	k7c2
pohřbení	pohřbení	k1gNnSc2
</s>
<s>
Ďáblický	ďáblický	k2eAgInSc1d1
hřbitov	hřbitov	k1gInSc1
Národnost	národnost	k1gFnSc1
</s>
<s>
Češi	Čech	k1gMnPc1
Povolání	povolání	k1gNnSc2
</s>
<s>
voják	voják	k1gMnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Četař	četař	k1gMnSc1
Jan	Jan	k1gMnSc1
Hrubý	Hrubý	k1gMnSc1
(	(	kIx(
<g/>
4	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1915	#num#	k4
<g/>
,	,	kIx,
Kunovice	Kunovice	k1gFnPc4
–	–	k?
18	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1942	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
příslušník	příslušník	k1gMnSc1
československé	československý	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
ve	v	k7c6
Spojeném	spojený	k2eAgNnSc6d1
království	království	k1gNnSc6
<g/>
,	,	kIx,
příslušník	příslušník	k1gMnSc1
výsadku	výsadek	k1gInSc2
Bioscop	Bioscop	k1gInSc1
a	a	k8xC
jeden	jeden	k4xCgMnSc1
ze	z	k7c2
sedmi	sedm	k4xCc2
výsadkářů	výsadkář	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
zahynuli	zahynout	k5eAaPmAgMnP
v	v	k7c6
kryptě	krypta	k1gFnSc6
pravoslavného	pravoslavný	k2eAgInSc2d1
kostela	kostel	k1gInSc2
svatých	svatý	k1gMnPc2
Cyrila	Cyril	k1gMnSc2
a	a	k8xC
Metoděje	Metoděj	k1gMnSc2
po	po	k7c6
atentátu	atentát	k1gInSc6
na	na	k7c4
Heydricha	Heydrich	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2002	#num#	k4
povýšen	povýšen	k2eAgInSc1d1
in	in	k?
memoriam	memoriam	k1gInSc1
do	do	k7c2
hodnosti	hodnost	k1gFnSc2
plukovníka	plukovník	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Mládí	mládí	k1gNnSc1
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS
se	s	k7c7
4	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1915	#num#	k4
v	v	k7c6
Kunovicích	Kunovice	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Otec	otec	k1gMnSc1
Jan	Jan	k1gMnSc1
(	(	kIx(
<g/>
1879	#num#	k4
<g/>
–	–	k?
<g/>
1931	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
pomocný	pomocný	k2eAgMnSc1d1
dělník	dělník	k1gMnSc1
<g/>
,	,	kIx,
matka	matka	k1gFnSc1
Kateřina	Kateřina	k1gFnSc1
(	(	kIx(
<g/>
1876	#num#	k4
<g/>
–	–	k?
<g/>
1944	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
rozená	rozený	k2eAgFnSc1d1
Komínková	Komínková	k1gFnSc1
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
v	v	k7c6
domácnosti	domácnost	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měl	mít	k5eAaImAgInS
tři	tři	k4xCgMnPc4
starší	starý	k2eAgMnPc4d2
sourozence	sourozenec	k1gMnPc4
<g/>
;	;	kIx,
dva	dva	k4xCgMnPc4
bratry	bratr	k1gMnPc4
a	a	k8xC
sestru	sestra	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Absolvoval	absolvovat	k5eAaPmAgMnS
obecnou	obecná	k1gFnSc4
a	a	k8xC
měšťanskou	měšťanský	k2eAgFnSc4d1
školu	škola	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyučil	vyučit	k5eAaPmAgMnS
se	se	k3xPyFc4
číšníkem	číšník	k1gMnSc7
ve	v	k7c6
Zlíně	Zlín	k1gInSc6
a	a	k8xC
později	pozdě	k6eAd2
také	také	k9
pracoval	pracovat	k5eAaImAgMnS
jako	jako	k9
číšník	číšník	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Vojenská	vojenský	k2eAgFnSc1d1
služba	služba	k1gFnSc1
</s>
<s>
Dne	den	k1gInSc2
1	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1937	#num#	k4
nastoupil	nastoupit	k5eAaPmAgInS
základní	základní	k2eAgFnSc4d1
vojenskou	vojenský	k2eAgFnSc4d1
službu	služba	k1gFnSc4
u	u	k7c2
hraničářského	hraničářský	k2eAgInSc2d1
praporu	prapor	k1gInSc2
v	v	k7c6
Trebišově	Trebišov	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
byl	být	k5eAaImAgMnS
povýšen	povýšit	k5eAaPmNgMnS
do	do	k7c2
hodnosti	hodnost	k1gFnSc2
svobodníka	svobodník	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
okupaci	okupace	k1gFnSc6
Čech	Čechy	k1gFnPc2
a	a	k8xC
Moravy	Morava	k1gFnSc2
nacistickým	nacistický	k2eAgNnSc7d1
Německem	Německo	k1gNnSc7
byl	být	k5eAaImAgInS
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
dubnu	duben	k1gInSc6
1939	#num#	k4
propuštěn	propustit	k5eAaPmNgMnS
na	na	k7c4
trvalou	trvalý	k2eAgFnSc4d1
dovolenou	dovolená	k1gFnSc4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1939	#num#	k4
se	se	k3xPyFc4
přihlásil	přihlásit	k5eAaPmAgMnS
na	na	k7c4
práci	práce	k1gFnSc4
do	do	k7c2
Německa	Německo	k1gNnSc2
a	a	k8xC
dostal	dostat	k5eAaPmAgMnS
se	se	k3xPyFc4
do	do	k7c2
Kielu	Kiel	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1940	#num#	k4
při	při	k7c6
dovolené	dovolená	k1gFnSc6
v	v	k7c6
protektorátu	protektorát	k1gInSc6
se	se	k3xPyFc4
se	s	k7c7
dvěma	dva	k4xCgMnPc7
kamarády	kamarád	k1gMnPc7
pokusil	pokusit	k5eAaPmAgMnS
o	o	k7c4
útěk	útěk	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
exilu	exil	k1gInSc6
</s>
<s>
Britský	britský	k2eAgInSc4d1
plakát	plakát	k1gInSc4
přibližující	přibližující	k2eAgNnSc4d1
československé	československý	k2eAgNnSc4d1
exilové	exilový	k2eAgNnSc4d1
vojsko	vojsko	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
plakátu	plakát	k1gInSc6
četař	četař	k1gMnSc1
Jan	Jan	k1gMnSc1
Hrubý	Hrubý	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Přes	přes	k7c4
Slovensko	Slovensko	k1gNnSc4
se	se	k3xPyFc4
dostali	dostat	k5eAaPmAgMnP
do	do	k7c2
Maďarska	Maďarsko	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
byli	být	k5eAaImAgMnP
ale	ale	k8xC
zadrženi	zadržet	k5eAaPmNgMnP
a	a	k8xC
vráceni	vrátit	k5eAaPmNgMnP
zpět	zpět	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhý	druhý	k4xOgInSc1
pokus	pokus	k1gInSc1
byl	být	k5eAaImAgInS
již	již	k6eAd1
úspěšný	úspěšný	k2eAgInSc1d1
a	a	k8xC
přes	přes	k7c4
Jugoslávii	Jugoslávie	k1gFnSc4
<g/>
,	,	kIx,
Řecko	Řecko	k1gNnSc1
<g/>
,	,	kIx,
Turecko	Turecko	k1gNnSc1
a	a	k8xC
Bejrút	Bejrút	k1gInSc1
se	se	k3xPyFc4
dostali	dostat	k5eAaPmAgMnP
do	do	k7c2
Francie	Francie	k1gFnSc2
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1940	#num#	k4
byl	být	k5eAaImAgMnS
zařazen	zařadit	k5eAaPmNgMnS
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
pěšímu	pěší	k1gMnSc3
pluku	pluk	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c6
jehož	jehož	k3xOyRp3gFnPc6
řadách	řada	k1gFnPc6
se	se	k3xPyFc4
účastnil	účastnit	k5eAaImAgInS
bojů	boj	k1gInPc2
o	o	k7c6
Francii	Francie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
pádu	pád	k1gInSc6
Francie	Francie	k1gFnSc2
byl	být	k5eAaImAgInS
13	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1940	#num#	k4
spolu	spolu	k6eAd1
s	s	k7c7
dalšími	další	k2eAgMnPc7d1
evakuován	evakuovat	k5eAaBmNgMnS
do	do	k7c2
Anglie	Anglie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tam	tam	k6eAd1
byl	být	k5eAaImAgMnS
zařazen	zařadit	k5eAaPmNgMnS
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
pěšímu	pěší	k1gMnSc3
praporu	prapor	k1gInSc2
<g/>
.	.	kIx.
28	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1940	#num#	k4
byl	být	k5eAaImAgMnS
povýšen	povýšit	k5eAaPmNgMnS
na	na	k7c4
četaře	četař	k1gMnPc4
a	a	k8xC
krátce	krátce	k6eAd1
nato	nato	k6eAd1
vybrán	vybrat	k5eAaPmNgMnS
pro	pro	k7c4
plnění	plnění	k1gNnSc4
zvláštních	zvláštní	k2eAgInPc2d1
úkolů	úkol	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
15	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
do	do	k7c2
7	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
absolvoval	absolvovat	k5eAaPmAgInS
základní	základní	k2eAgInSc1d1
sabotážní	sabotážní	k2eAgInSc1d1
kurz	kurz	k1gInSc1
<g/>
,	,	kIx,
paravýcvik	paravýcvik	k1gInSc1
a	a	k8xC
kurz	kurz	k1gInSc1
průmyslové	průmyslový	k2eAgFnSc2d1
sabotáže	sabotáž	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Nasazení	nasazení	k1gNnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Operace	operace	k1gFnSc1
Bioscop	Bioscop	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Společně	společně	k6eAd1
s	s	k7c7
Bohuslavem	Bohuslav	k1gMnSc7
Koubou	Kouba	k1gMnSc7
a	a	k8xC
Josefem	Josef	k1gMnSc7
Bublíkem	Bublík	k1gMnSc7
byli	být	k5eAaImAgMnP
vysazeni	vysadit	k5eAaPmNgMnP
28	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1942	#num#	k4
u	u	k7c2
hospodářského	hospodářský	k2eAgInSc2d1
dvora	dvůr	k1gInSc2
Požáry	požár	k1gInPc1
na	na	k7c6
Křivoklátsku	Křivoklátsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejprve	nejprve	k6eAd1
se	se	k3xPyFc4
společně	společně	k6eAd1
s	s	k7c7
Bublíkem	Bublík	k1gInSc7
pohybovali	pohybovat	k5eAaImAgMnP
na	na	k7c6
Uherskohradišťsku	Uherskohradišťsko	k1gNnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
protože	protože	k8xS
se	se	k3xPyFc4
jim	on	k3xPp3gMnPc3
nepodařilo	podařit	k5eNaPmAgNnS
navázat	navázat	k5eAaPmF
kontakt	kontakt	k1gInSc4
s	s	k7c7
odbojem	odboj	k1gInSc7
<g/>
,	,	kIx,
přesunuli	přesunout	k5eAaPmAgMnP
se	se	k3xPyFc4
do	do	k7c2
Prahy	Praha	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
podřídili	podřídit	k5eAaPmAgMnP
velení	velení	k1gNnSc2
Adolfa	Adolf	k1gMnSc2
Opálky	opálka	k1gFnSc2
a	a	k8xC
za	za	k7c2
pomoci	pomoc	k1gFnSc2
odbojářů	odbojář	k1gMnPc2
se	se	k3xPyFc4
ukrývali	ukrývat	k5eAaImAgMnP
v	v	k7c6
konspiračních	konspirační	k2eAgInPc6d1
bytech	byt	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
atentátu	atentát	k1gInSc6
na	na	k7c4
zastupujícího	zastupující	k2eAgMnSc4d1
říšského	říšský	k2eAgMnSc4d1
protektora	protektor	k1gMnSc4
Heydricha	Heydrich	k1gMnSc4
se	se	k3xPyFc4
společně	společně	k6eAd1
s	s	k7c7
dalšími	další	k2eAgMnPc7d1
výsadkáři	výsadkář	k1gMnPc7
ukrýval	ukrývat	k5eAaImAgMnS
v	v	k7c6
kryptě	krypta	k1gFnSc6
pravoslavného	pravoslavný	k2eAgInSc2d1
kostela	kostel	k1gInSc2
Cyrila	Cyril	k1gMnSc2
a	a	k8xC
Metoděje	Metoděj	k1gMnSc2
v	v	k7c6
Resslově	Resslově	k1gFnSc6
ulici	ulice	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
po	po	k7c6
boji	boj	k1gInSc6
v	v	k7c6
obklíčení	obklíčení	k1gNnSc6
jednotkami	jednotka	k1gFnPc7
okupační	okupační	k2eAgFnSc2d1
moci	moc	k1gFnSc2
18	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1942	#num#	k4
spáchal	spáchat	k5eAaPmAgMnS
sebevraždu	sebevražda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
válce	válka	k1gFnSc6
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1945	#num#	k4
byl	být	k5eAaImAgInS
in	in	k?
memoriam	memoriam	k6eAd1
jmenován	jmenovat	k5eAaBmNgMnS,k5eAaImNgMnS
podporučíkem	podporučík	k1gMnSc7
pěchoty	pěchota	k1gFnSc2
a	a	k8xC
zároveň	zároveň	k6eAd1
povýšen	povýšit	k5eAaPmNgMnS
do	do	k7c2
hodnosti	hodnost	k1gFnSc2
poručíka	poručík	k1gMnSc2
pěchoty	pěchota	k1gFnSc2
<g/>
.	.	kIx.
17	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1948	#num#	k4
byl	být	k5eAaImAgInS
povýšen	povýšit	k5eAaPmNgMnS
do	do	k7c2
hodnosti	hodnost	k1gFnSc2
nadporučíka	nadporučík	k1gMnSc2
pěchoty	pěchota	k1gFnSc2
a	a	k8xC
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2002	#num#	k4
do	do	k7c2
hodnosti	hodnost	k1gFnSc2
podplukovníka	podplukovník	k1gMnSc2
pěchoty	pěchota	k1gFnSc2
in	in	k?
memoriam	memoriam	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Kunovicích	Kunovice	k1gFnPc6
a	a	k8xC
Praze-Kobylisích	Praze-Kobylise	k1gFnPc6
jsou	být	k5eAaImIp3nP
po	po	k7c6
něm	on	k3xPp3gInSc6
pojmenovány	pojmenovat	k5eAaPmNgFnP
ulice	ulice	k1gFnPc1
<g/>
,	,	kIx,
na	na	k7c6
rodném	rodný	k2eAgInSc6d1
domě	dům	k1gInSc6
je	být	k5eAaImIp3nS
umístěna	umístěn	k2eAgFnSc1d1
pamětní	pamětní	k2eAgFnSc1d1
deska	deska	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1
tělo	tělo	k1gNnSc1
bylo	být	k5eAaImAgNnS
po	po	k7c6
identifikaci	identifikace	k1gFnSc6
a	a	k8xC
pitvě	pitva	k1gFnSc6
pohřbeno	pohřbít	k5eAaPmNgNnS
v	v	k7c6
hromadném	hromadný	k2eAgInSc6d1
hrobě	hrob	k1gInSc6
na	na	k7c6
hřbitově	hřbitov	k1gInSc6
v	v	k7c6
Ďáblicích	ďáblice	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rodina	rodina	k1gFnSc1
Jana	Jana	k1gFnSc1
Hrubého	Hrubého	k2eAgFnSc1d1
nebyla	být	k5eNaImAgFnS
perzekvována	perzekvován	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jan	Jan	k1gMnSc1
Hrubý	Hrubý	k1gMnSc1
byl	být	k5eAaImAgMnS
hodnocen	hodnotit	k5eAaImNgMnS
v	v	k7c6
armádě	armáda	k1gFnSc6
jako	jako	k8xS,k8xC
naprosto	naprosto	k6eAd1
spolehlivý	spolehlivý	k2eAgInSc1d1
<g/>
,	,	kIx,
samostatný	samostatný	k2eAgInSc1d1
a	a	k8xC
vzor	vzor	k1gInSc1
pro	pro	k7c4
mužstvo	mužstvo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Část	část	k1gFnSc1
pozůstalosti	pozůstalost	k1gFnSc2
po	po	k7c6
Janu	Jan	k1gMnSc6
Hrubém	Hrubý	k1gMnSc6
a	a	k8xC
dalších	další	k2eAgMnPc6d1
parašutistech	parašutista	k1gMnPc6
je	být	k5eAaImIp3nS
vystavena	vystavit	k5eAaPmNgFnS
v	v	k7c6
Armádním	armádní	k2eAgNnSc6d1
muzeu	muzeum	k1gNnSc6
na	na	k7c6
Žižkově	Žižkov	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2015	#num#	k4
daroval	darovat	k5eAaPmAgMnS
městu	město	k1gNnSc3
Kunovice	Kunovice	k1gFnPc4
akademický	akademický	k2eAgMnSc1d1
malíř	malíř	k1gMnSc1
Pavel	Pavel	k1gMnSc1
Vavrys	Vavrys	k1gInSc4
pietní	pietní	k2eAgNnSc4d1
dílo	dílo	k1gNnSc4
–	–	k?
památník	památník	k1gInSc1
připomínající	připomínající	k2eAgInSc1d1
parašutistu	parašutista	k1gMnSc4
Jana	Jan	k1gMnSc2
Hrubého	Hrubý	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Památník	památník	k1gInSc1
je	být	k5eAaImIp3nS
tvořen	tvořit	k5eAaImNgInS
kruhovým	kruhový	k2eAgInSc7d1
obrazem	obraz	k1gInSc7
(	(	kIx(
<g/>
terčem	terč	k1gInSc7
o	o	k7c6
průměru	průměr	k1gInSc6
84	#num#	k4
cm	cm	kA
<g/>
)	)	kIx)
a	a	k8xC
podkladovou	podkladový	k2eAgFnSc7d1
deskou	deska	k1gFnSc7
<g/>
,	,	kIx,
pevně	pevně	k6eAd1
osazenou	osazený	k2eAgFnSc4d1
do	do	k7c2
stěny	stěna	k1gFnSc2
v	v	k7c6
tamní	tamní	k2eAgFnSc6d1
základní	základní	k2eAgFnSc6d1
škole	škola	k1gFnSc6
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
Jan	Jan	k1gMnSc1
Hrubý	Hrubý	k1gMnSc1
v	v	k7c6
mládí	mládí	k1gNnSc6
navštěvoval	navštěvovat	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dílo	dílo	k1gNnSc1
je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7
Vavrysova	Vavrysův	k2eAgInSc2d1
obrazového	obrazový	k2eAgInSc2d1
cyklu	cyklus	k1gInSc2
„	„	k?
<g/>
Česká	český	k2eAgFnSc1d1
paměť	paměť	k1gFnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vyznamenání	vyznamenání	k1gNnSc1
</s>
<s>
1940	#num#	k4
–	–	k?
Československý	československý	k2eAgInSc1d1
válečný	válečný	k2eAgInSc1d1
kříž	kříž	k1gInSc1
1939	#num#	k4
</s>
<s>
1944	#num#	k4
–	–	k?
Pamětní	pamětní	k2eAgFnSc1d1
medaile	medaile	k1gFnSc1
československé	československý	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
v	v	k7c6
zahraničí	zahraničí	k1gNnSc6
se	s	k7c7
štítky	štítek	k1gInPc7
Francie	Francie	k1gFnSc2
a	a	k8xC
Velká	velký	k2eAgFnSc1d1
Británie	Británie	k1gFnSc1
</s>
<s>
1945	#num#	k4
–	–	k?
druhý	druhý	k4xOgMnSc1
Československý	československý	k2eAgInSc1d1
válečný	válečný	k2eAgInSc1d1
kříž	kříž	k1gInSc1
1939	#num#	k4
</s>
<s>
1946	#num#	k4
–	–	k?
třetí	třetí	k4xOgMnSc1
Československý	československý	k2eAgInSc1d1
válečný	válečný	k2eAgInSc1d1
kříž	kříž	k1gInSc1
1939	#num#	k4
</s>
<s>
1949	#num#	k4
–	–	k?
Zlatá	zlatý	k2eAgFnSc1d1
hvězda	hvězda	k1gFnSc1
Československého	československý	k2eAgInSc2d1
vojenského	vojenský	k2eAgInSc2d1
řádu	řád	k1gInSc2
Za	za	k7c4
svobodu	svoboda	k1gFnSc4
</s>
<s>
1968	#num#	k4
–	–	k?
Řád	řád	k1gInSc1
rudé	rudý	k2eAgInPc1d1
zástavy	zástav	k1gInPc1
</s>
<s>
2010	#num#	k4
–	–	k?
Kříž	Kříž	k1gMnSc1
obrany	obrana	k1gFnSc2
státu	stát	k1gInSc2
ministra	ministr	k1gMnSc2
obrany	obrana	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Operace	operace	k1gFnSc1
Anthropoid	Anthropoid	k1gInSc1
</s>
<s>
Karel	Karel	k1gMnSc1
Čurda	Čurda	k1gMnSc1
</s>
<s>
Jozef	Jozef	k1gMnSc1
Gabčík	Gabčík	k1gMnSc1
</s>
<s>
Jan	Jan	k1gMnSc1
Kubiš	Kubiš	k1gMnSc1
</s>
<s>
Adolf	Adolf	k1gMnSc1
Opálka	opálka	k1gFnSc1
</s>
<s>
Josef	Josef	k1gMnSc1
Valčík	valčík	k1gInSc4
</s>
<s>
Atentát	atentát	k1gInSc1
(	(	kIx(
<g/>
film	film	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Pavel	Pavel	k1gMnSc1
Vavrys	Vavrys	k1gInSc1
-	-	kIx~
Cyklus	cyklus	k1gInSc1
Česká	český	k2eAgFnSc1d1
paměť	paměť	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
www.galerievavrys.cz	www.galerievavrys.cz	k1gMnSc1
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
8	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
http://www.mocr.army.cz/informacni-servis/vyroci/zpravodajstvi/udeleni-resortnich-vyznamenani-40146/	http://www.mocr.army.cz/informacni-servis/vyroci/zpravodajstvi/udeleni-resortnich-vyznamenani-40146/	k4
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
ČVANČARA	čvančara	k1gMnSc1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někomu	někdo	k3yInSc3
život	život	k1gInSc4
<g/>
,	,	kIx,
někomu	někdo	k3yInSc3
smrt	smrt	k1gFnSc4
(	(	kIx(
<g/>
1941	#num#	k4
-	-	kIx~
1943	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Laguna	laguna	k1gFnSc1
<g/>
,	,	kIx,
nakladatelství	nakladatelství	k1gNnSc1
a	a	k8xC
vydavatelství	vydavatelství	k1gNnSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86274	#num#	k4
<g/>
-	-	kIx~
<g/>
40	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
REICHL	REICHL	kA
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cesty	cesta	k1gFnPc4
osudu	osud	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cheb	Cheb	k1gInSc1
<g/>
:	:	kIx,
Svět	svět	k1gInSc1
křídel	křídlo	k1gNnPc2
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86808	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Kmenový	kmenový	k2eAgInSc1d1
list	list	k1gInSc1
</s>
<s>
Četař	četař	k1gMnSc1
Jan	Jan	k1gMnSc1
Hrubý	Hrubý	k1gMnSc1
na	na	k7c4
vets	vets	k1gInSc4
<g/>
.	.	kIx.
<g/>
estranky	estranka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
pna	pnout	k5eAaImSgInS
<g/>
2006338161	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
85102609	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc4
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc4
<g/>
:	:	kIx,
Druhá	druhý	k4xOgFnSc1
světová	světový	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
