<s>
Četař	četař	k1gMnSc1
Jan	Jan	k1gMnSc1
Hrubý	Hrubý	k1gMnSc1
(	(	kIx(
<g/>
4	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1915	#num#	k4
<g/>
,	,	kIx,
Kunovice	Kunovice	k1gFnPc4
–	–	k?
18	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1942	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
příslušník	příslušník	k1gMnSc1
československé	československý	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
ve	v	k7c6
Spojeném	spojený	k2eAgNnSc6d1
království	království	k1gNnSc6
<g/>
,	,	kIx,
příslušník	příslušník	k1gMnSc1
výsadku	výsadek	k1gInSc2
Bioscop	Bioscop	k1gInSc1
a	a	k8xC
jeden	jeden	k4xCgMnSc1
ze	z	k7c2
sedmi	sedm	k4xCc2
výsadkářů	výsadkář	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
zahynuli	zahynout	k5eAaPmAgMnP
v	v	k7c6
kryptě	krypta	k1gFnSc6
pravoslavného	pravoslavný	k2eAgInSc2d1
kostela	kostel	k1gInSc2
svatých	svatý	k1gMnPc2
Cyrila	Cyril	k1gMnSc2
a	a	k8xC
Metoděje	Metoděj	k1gMnSc2
po	po	k7c6
atentátu	atentát	k1gInSc6
na	na	k7c4
Heydricha	Heydrich	k1gMnSc4
<g/>
.	.	kIx.
</s>