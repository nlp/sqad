<s>
Tyto	tento	k3xDgInPc1	tento
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
parazitické	parazitický	k2eAgFnPc1d1	parazitická
(	(	kIx(	(
<g/>
cizopasné	cizopasný	k2eAgFnPc1d1	cizopasná
<g/>
)	)	kIx)	)
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
vyvolávat	vyvolávat	k5eAaImF	vyvolávat
onemocnění	onemocnění	k1gNnSc4	onemocnění
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
těla	tělo	k1gNnSc2	tělo
i	i	k9	i
ve	v	k7c6	v
vnitřních	vnitřní	k2eAgInPc6d1	vnitřní
orgánech	orgán	k1gInPc6	orgán
<g/>
.	.	kIx.	.
</s>
