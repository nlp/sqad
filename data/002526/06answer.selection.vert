<s>
Mekka	Mekka	k1gFnSc1	Mekka
(	(	kIx(	(
<g/>
arabsky	arabsky	k6eAd1	arabsky
م	م	k?	م
Makka	Makek	k1gInSc2	Makek
<g/>
,	,	kIx,	,
v	v	k7c6	v
novější	nový	k2eAgFnSc6d2	novější
době	doba	k1gFnSc6	doba
většinou	většinou	k6eAd1	většinou
م	م	k?	م
ا	ا	k?	ا
<g/>
ّ	ّ	k?	ّ
<g/>
م	م	k?	م
Makka	Makka	k1gFnSc1	Makka
al-Mukarrama	al-Mukarrama	k1gFnSc1	al-Mukarrama
<g/>
'	'	kIx"	'
Mekka	Mekka	k1gFnSc1	Mekka
<g/>
,	,	kIx,	,
ctihodná	ctihodný	k2eAgFnSc1d1	ctihodná
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
s	s	k7c7	s
2	[number]	k4	2
000	[number]	k4	000
000	[number]	k4	000
obyvateli	obyvatel	k1gMnPc7	obyvatel
(	(	kIx(	(
<g/>
stav	stav	k1gInSc4	stav
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
Saúdské	saúdský	k2eAgFnSc2d1	Saúdská
Arábie	Arábie	k1gFnSc2	Arábie
<g/>
.	.	kIx.	.
</s>
