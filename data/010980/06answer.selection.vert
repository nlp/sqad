<s>
Plynný	plynný	k2eAgInSc1d1	plynný
dusík	dusík	k1gInSc1	dusík
nalézá	nalézat	k5eAaImIp3nS	nalézat
využití	využití	k1gNnSc1	využití
jako	jako	k8xC	jako
inertní	inertní	k2eAgFnSc1d1	inertní
atmosféra	atmosféra	k1gFnSc1	atmosféra
např.	např.	kA	např.
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hrozí	hrozit	k5eAaImIp3nS	hrozit
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
výbuchu	výbuch	k1gInSc2	výbuch
<g/>
,	,	kIx,	,
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
integrovaných	integrovaný	k2eAgInPc2d1	integrovaný
obvodů	obvod	k1gInPc2	obvod
<g/>
,	,	kIx,	,
nerezové	rezový	k2eNgFnSc2d1	nerezová
oceli	ocel	k1gFnSc2	ocel
a	a	k8xC	a
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
k	k	k7c3	k
plnění	plnění	k1gNnSc3	plnění
obalů	obal	k1gInPc2	obal
výrobků	výrobek	k1gInPc2	výrobek
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nedošlo	dojít	k5eNaPmAgNnS	dojít
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
zmačkání	zmačkání	k1gNnSc3	zmačkání
a	a	k8xC	a
zvlhčení	zvlhčení	k1gNnSc3	zvlhčení
-	-	kIx~	-
například	například	k6eAd1	například
sáčky	sáček	k1gInPc4	sáček
s	s	k7c7	s
brambůrky	brambůrek	k1gInPc7	brambůrek
<g/>
.	.	kIx.	.
</s>
