<s>
Pampeliška	pampeliška	k1gFnSc1	pampeliška
(	(	kIx(	(
<g/>
Taraxacum	Taraxacum	k1gInSc1	Taraxacum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
či	či	k8xC	či
také	také	k9	také
smetánka	smetánka	k1gFnSc1	smetánka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
známý	známý	k2eAgInSc1d1	známý
rod	rod	k1gInSc1	rod
dvouděložných	dvouděložný	k2eAgFnPc2d1	dvouděložná
rostlin	rostlina	k1gFnPc2	rostlina
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
hvězdnicovité	hvězdnicovitý	k2eAgFnSc2d1	hvězdnicovitý
<g/>
.	.	kIx.	.
</s>
<s>
Typickým	typický	k2eAgMnSc7d1	typický
zástupcem	zástupce	k1gMnSc7	zástupce
je	být	k5eAaImIp3nS	být
pampeliška	pampeliška	k1gFnSc1	pampeliška
lékařská	lékařský	k2eAgFnSc1d1	lékařská
(	(	kIx(	(
<g/>
též	též	k9	též
smetánka	smetánka	k1gFnSc1	smetánka
lékařská	lékařský	k2eAgFnSc1d1	lékařská
<g/>
,	,	kIx,	,
Taraxacum	Taraxacum	k1gInSc1	Taraxacum
officinale	officinale	k6eAd1	officinale
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
plevelná	plevelný	k2eAgFnSc1d1	plevelná
rostlina	rostlina	k1gFnSc1	rostlina
běžná	běžný	k2eAgFnSc1d1	běžná
po	po	k7c6	po
celé	celá	k1gFnSc6	celá
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s desamb="1">
Pampeliška	pampeliška	k1gFnSc1	pampeliška
je	být	k5eAaImIp3nS	být
nenáročná	náročný	k2eNgFnSc1d1	nenáročná
na	na	k7c4	na
půdu	půda	k1gFnSc4	půda
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
velmi	velmi	k6eAd1	velmi
dužinaté	dužinatý	k2eAgInPc4d1	dužinatý
listy	list	k1gInPc4	list
<g/>
,	,	kIx,	,
květenstvím	květenství	k1gNnSc7	květenství
je	být	k5eAaImIp3nS	být
žlutý	žlutý	k2eAgInSc1d1	žlutý
úbor	úbor	k1gInSc1	úbor
<g/>
.	.	kIx.	.
</s>
<s>
Označení	označení	k1gNnSc1	označení
pampeliška	pampeliška	k1gFnSc1	pampeliška
pochází	pocházet	k5eAaImIp3nS	pocházet
buď	buď	k8xC	buď
přes	přes	k7c4	přes
označení	označení	k1gNnSc4	označení
pléška	pléšek	k1gInSc2	pléšek
(	(	kIx(	(
<g/>
mnišskou	mnišský	k2eAgFnSc4d1	mnišská
pleš	pleš	k1gFnSc4	pleš
připomínající	připomínající	k2eAgMnSc1d1	připomínající
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
ze	z	k7c2	z
starohornoněmeckého	starohornoněmecký	k2eAgMnSc2d1	starohornoněmecký
pappala	pappal	k1gMnSc2	pappal
(	(	kIx(	(
<g/>
kaše	kaše	k1gFnSc1	kaše
<g/>
)	)	kIx)	)
či	či	k8xC	či
z	z	k7c2	z
latinského	latinský	k2eAgInSc2d1	latinský
pappus	pappus	k1gInSc1	pappus
(	(	kIx(	(
<g/>
chmýří	chmýří	k1gNnSc1	chmýří
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
postupnou	postupný	k2eAgFnSc7d1	postupná
kombinací	kombinace	k1gFnSc7	kombinace
uvedených	uvedený	k2eAgInPc2d1	uvedený
původů	původ	k1gInPc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Polský	polský	k2eAgInSc1d1	polský
název	název	k1gInSc1	název
mniszek	mniszka	k1gFnPc2	mniszka
totiž	totiž	k9	totiž
může	moct	k5eAaImIp3nS	moct
odpovídat	odpovídat	k5eAaImF	odpovídat
překladu	překlad	k1gInSc6	překlad
pop	pop	k1gMnSc1	pop
<g/>
/	/	kIx~	/
<g/>
kněz	kněz	k1gMnSc1	kněz
(	(	kIx(	(
<g/>
lidově	lidově	k6eAd1	lidově
např.	např.	kA	např.
kněžská	kněžský	k2eAgFnSc1d1	kněžská
pleš	pleš	k1gFnSc1	pleš
<g/>
)	)	kIx)	)
a	a	k8xC	a
ten	ten	k3xDgMnSc1	ten
německému	německý	k2eAgNnSc3d1	německé
pǫ	pǫ	k?	pǫ
<g/>
/	/	kIx~	/
<g/>
Pappus	pappus	k1gInSc1	pappus
<g/>
/	/	kIx~	/
<g/>
chmýr	chmýr	k1gInSc1	chmýr
<g/>
.	.	kIx.	.
</s>
<s>
Staročeské	staročeský	k2eAgNnSc1d1	staročeské
pojmenování	pojmenování	k1gNnSc1	pojmenování
smetanka	smetanka	k1gFnSc1	smetanka
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
od	od	k7c2	od
smetana	smetana	k1gFnSc1	smetana
(	(	kIx(	(
<g/>
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
např.	např.	kA	např.
označení	označení	k1gNnSc1	označení
mlíčí	mlíčí	k1gNnSc1	mlíčí
od	od	k7c2	od
mléko	mléko	k1gNnSc4	mléko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rod	rod	k1gInSc1	rod
Taraxacum	Taraxacum	k1gInSc1	Taraxacum
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
složitě	složitě	k6eAd1	složitě
klasifikovatelný	klasifikovatelný	k2eAgInSc1d1	klasifikovatelný
<g/>
,	,	kIx,	,
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
obrovské	obrovský	k2eAgNnSc1d1	obrovské
množství	množství	k1gNnSc1	množství
drobných	drobný	k2eAgInPc2d1	drobný
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
rozmnožuje	rozmnožovat	k5eAaImIp3nS	rozmnožovat
nepohlavně	pohlavně	k6eNd1	pohlavně
díky	díky	k7c3	díky
tzv.	tzv.	kA	tzv.
apomixii	apomixie	k1gFnSc4	apomixie
<g/>
.	.	kIx.	.
</s>
<s>
Pampeliška	pampeliška	k1gFnSc1	pampeliška
je	být	k5eAaImIp3nS	být
léčivá	léčivý	k2eAgFnSc1d1	léčivá
rostlina	rostlina	k1gFnSc1	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Anglické	anglický	k2eAgNnSc1d1	anglické
a	a	k8xC	a
francouzské	francouzský	k2eAgNnSc1d1	francouzské
lidové	lidový	k2eAgNnSc1d1	lidové
pojmenování	pojmenování	k1gNnSc1	pojmenování
pampelišky	pampeliška	k1gFnSc2	pampeliška
(	(	kIx(	(
<g/>
dandelion	dandelion	k1gInSc1	dandelion
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
dent	dent	k1gInSc1	dent
de	de	k?	de
lion	lion	k?	lion
<g/>
)	)	kIx)	)
znamená	znamenat	k5eAaImIp3nS	znamenat
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
zub	zub	k1gInSc1	zub
lva	lev	k1gMnSc2	lev
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
složitého	složitý	k2eAgInSc2d1	složitý
profilu	profil	k1gInSc2	profil
listů	list	k1gInPc2	list
(	(	kIx(	(
<g/>
z	z	k7c2	z
latinského	latinský	k2eAgInSc2d1	latinský
leontodon	leontodona	k1gFnPc2	leontodona
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pampeliška	pampeliška	k1gFnSc1	pampeliška
lékařská	lékařský	k2eAgFnSc1d1	lékařská
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
pampeliška	pampeliška	k1gFnSc1	pampeliška
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
pampeliška	pampeliška	k1gFnSc1	pampeliška
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Taxon	taxon	k1gInSc1	taxon
Taraxacum	Taraxacum	k1gInSc1	Taraxacum
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
