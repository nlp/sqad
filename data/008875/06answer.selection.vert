<s>
Má	mít	k5eAaImIp3nS	mít
červené	červený	k2eAgNnSc1d1	červené
pole	pole	k1gNnSc1	pole
<g/>
,	,	kIx,	,
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
horním	horní	k2eAgInSc6d1	horní
rohu	roh	k1gInSc6	roh
je	být	k5eAaImIp3nS	být
velká	velký	k2eAgFnSc1d1	velká
žlutá	žlutý	k2eAgFnSc1d1	žlutá
pěticípá	pěticípý	k2eAgFnSc1d1	pěticípá
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
obklopují	obklopovat	k5eAaImIp3nP	obklopovat
čtyři	čtyři	k4xCgFnPc1	čtyři
menší	malý	k2eAgFnPc1d2	menší
žluté	žlutý	k2eAgFnPc1d1	žlutá
hvězdy	hvězda	k1gFnPc1	hvězda
<g/>
,	,	kIx,	,
pootočené	pootočený	k2eAgFnPc1d1	pootočená
vždy	vždy	k6eAd1	vždy
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jeden	jeden	k4xCgMnSc1	jeden
jejich	jejich	k3xOp3gNnSc2	jejich
cíp	cíp	k1gInSc1	cíp
směřoval	směřovat	k5eAaImAgInS	směřovat
do	do	k7c2	do
středu	střed	k1gInSc2	střed
velké	velký	k2eAgFnSc2d1	velká
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
