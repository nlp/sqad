<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Čínské	čínský	k2eAgFnSc2d1	čínská
lidové	lidový	k2eAgFnSc2d1	lidová
republiky	republika	k1gFnSc2	republika
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
tradiční	tradiční	k2eAgFnSc2d1	tradiční
čínské	čínský	k2eAgFnSc2d1	čínská
barvy	barva	k1gFnSc2	barva
a	a	k8xC	a
inspirovala	inspirovat	k5eAaBmAgFnS	inspirovat
se	s	k7c7	s
vlajkou	vlajka	k1gFnSc7	vlajka
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
červené	červený	k2eAgNnSc1d1	červené
pole	pole	k1gNnSc1	pole
<g/>
,	,	kIx,	,
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
horním	horní	k2eAgInSc6d1	horní
rohu	roh	k1gInSc6	roh
je	být	k5eAaImIp3nS	být
velká	velký	k2eAgFnSc1d1	velká
žlutá	žlutý	k2eAgFnSc1d1	žlutá
pěticípá	pěticípý	k2eAgFnSc1d1	pěticípá
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
obklopují	obklopovat	k5eAaImIp3nP	obklopovat
čtyři	čtyři	k4xCgFnPc1	čtyři
menší	malý	k2eAgFnPc1d2	menší
žluté	žlutý	k2eAgFnPc1d1	žlutá
hvězdy	hvězda	k1gFnPc1	hvězda
<g/>
,	,	kIx,	,
pootočené	pootočený	k2eAgFnPc1d1	pootočená
vždy	vždy	k6eAd1	vždy
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jeden	jeden	k4xCgMnSc1	jeden
jejich	jejich	k3xOp3gNnSc2	jejich
cíp	cíp	k1gInSc1	cíp
směřoval	směřovat	k5eAaImAgInS	směřovat
do	do	k7c2	do
středu	střed	k1gInSc2	střed
velké	velký	k2eAgFnSc2d1	velká
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
oficiální	oficiální	k2eAgFnSc2d1	oficiální
interpretace	interpretace	k1gFnSc2	interpretace
představuje	představovat	k5eAaImIp3nS	představovat
velká	velký	k2eAgFnSc1d1	velká
hvězda	hvězda	k1gFnSc1	hvězda
společný	společný	k2eAgInSc4d1	společný
program	program	k1gInSc4	program
a	a	k8xC	a
jednotu	jednota	k1gFnSc4	jednota
lidu	lid	k1gInSc2	lid
vedeného	vedený	k2eAgInSc2d1	vedený
komunistickou	komunistický	k2eAgFnSc4d1	komunistická
stranou	strana	k1gFnSc7	strana
<g/>
,	,	kIx,	,
malé	malý	k2eAgFnPc1d1	malá
hvězdy	hvězda	k1gFnPc1	hvězda
reprezentují	reprezentovat	k5eAaImIp3nP	reprezentovat
dělníky	dělník	k1gMnPc4	dělník
<g/>
,	,	kIx,	,
rolníky	rolník	k1gMnPc4	rolník
<g/>
,	,	kIx,	,
malou	malý	k2eAgFnSc4d1	malá
buržoazii	buržoazie	k1gFnSc4	buržoazie
a	a	k8xC	a
vlastenecký	vlastenecký	k2eAgInSc4d1	vlastenecký
kapitalismus	kapitalismus	k1gInSc4	kapitalismus
<g/>
,	,	kIx,	,
čtyři	čtyři	k4xCgFnPc1	čtyři
sociální	sociální	k2eAgFnPc1d1	sociální
skupiny	skupina	k1gFnPc1	skupina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
účastní	účastnit	k5eAaImIp3nS	účastnit
společného	společný	k2eAgInSc2d1	společný
programu	program	k1gInSc2	program
socialistické	socialistický	k2eAgFnSc2d1	socialistická
výstavby	výstavba	k1gFnSc2	výstavba
<g/>
.	.	kIx.	.
</s>
<s>
Červená	červený	k2eAgFnSc1d1	červená
je	být	k5eAaImIp3nS	být
barvou	barva	k1gFnSc7	barva
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
symbolizovala	symbolizovat	k5eAaImAgFnS	symbolizovat
Číňany	Číňan	k1gMnPc4	Číňan
<g/>
.	.	kIx.	.
</s>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
byla	být	k5eAaImAgFnS	být
zavedena	zavést	k5eAaPmNgFnS	zavést
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
a	a	k8xC	a
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
ji	on	k3xPp3gFnSc4	on
Ceng	Ceng	k1gMnSc1	Ceng
Lien-sung	Lienung	k1gMnSc1	Lien-sung
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zvláštní	zvláštní	k2eAgFnPc1d1	zvláštní
správní	správní	k2eAgFnPc1d1	správní
oblasti	oblast	k1gFnPc1	oblast
Čínské	čínský	k2eAgFnSc2d1	čínská
lidové	lidový	k2eAgFnSc2d1	lidová
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
Hongkong	Hongkong	k1gInSc4	Hongkong
a	a	k8xC	a
Macao	Macao	k1gNnSc4	Macao
<g/>
)	)	kIx)	)
mají	mít	k5eAaImIp3nP	mít
historicky	historicky	k6eAd1	historicky
své	svůj	k3xOyFgFnPc4	svůj
vlastní	vlastní	k2eAgFnPc4d1	vlastní
vlajky	vlajka	k1gFnPc4	vlajka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Návrhy	návrh	k1gInPc1	návrh
čínské	čínský	k2eAgFnSc2d1	čínská
vlajky	vlajka	k1gFnSc2	vlajka
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
používala	používat	k5eAaImAgFnS	používat
Čína	Čína	k1gFnSc1	Čína
vlajku	vlajka	k1gFnSc4	vlajka
Kuomintangu	Kuomintang	k1gInSc2	Kuomintang
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
dosud	dosud	k6eAd1	dosud
státním	státní	k2eAgInSc7d1	státní
symbolem	symbol	k1gInSc7	symbol
Čínské	čínský	k2eAgFnSc2d1	čínská
republiky	republika	k1gFnSc2	republika
na	na	k7c6	na
Tchaj-wanu	Tchajan	k1gInSc6	Tchaj-wan
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
vítězství	vítězství	k1gNnSc4	vítězství
komunistů	komunista	k1gMnPc2	komunista
v	v	k7c6	v
občanské	občanský	k2eAgFnSc6d1	občanská
válce	válka	k1gFnSc6	válka
vybíralo	vybírat	k5eAaImAgNnS	vybírat
stranické	stranický	k2eAgNnSc1d1	stranické
vedení	vedení	k1gNnSc1	vedení
nové	nový	k2eAgInPc1d1	nový
státní	státní	k2eAgInPc1d1	státní
symboly	symbol	k1gInPc1	symbol
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc4d3	veliký
podporu	podpora	k1gFnSc4	podpora
měl	mít	k5eAaImAgInS	mít
zpočátku	zpočátku	k6eAd1	zpočátku
návrh	návrh	k1gInSc1	návrh
vlajky	vlajka	k1gFnSc2	vlajka
s	s	k7c7	s
jednou	jeden	k4xCgFnSc7	jeden
hvězdou	hvězda	k1gFnSc7	hvězda
a	a	k8xC	a
žlutým	žlutý	k2eAgInSc7d1	žlutý
pruhem	pruh	k1gInSc7	pruh
ve	v	k7c6	v
spodní	spodní	k2eAgFnSc6d1	spodní
části	část	k1gFnSc6	část
<g/>
,	,	kIx,	,
symbolizujícím	symbolizující	k2eAgInSc6d1	symbolizující
Žlutou	žlutý	k2eAgFnSc4d1	žlutá
řeku	řeka	k1gFnSc4	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
němu	on	k3xPp3gMnSc3	on
ale	ale	k8xC	ale
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
generál	generál	k1gMnSc1	generál
Čang	Čang	k1gMnSc1	Čang
Č	Č	kA	Č
<g/>
'	'	kIx"	'
<g/>
-čung	-čung	k1gMnSc1	-čung
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
vypadá	vypadat	k5eAaPmIp3nS	vypadat
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
rudá	rudý	k2eAgFnSc1d1	rudá
barva	barva	k1gFnSc1	barva
revoluce	revoluce	k1gFnSc2	revoluce
přeškrtnuta	přeškrtnut	k2eAgFnSc1d1	přeškrtnuta
<g/>
.	.	kIx.	.
</s>
<s>
Mao	Mao	k?	Mao
Ce-tung	Ceung	k1gInSc1	Ce-tung
tento	tento	k3xDgInSc4	tento
argument	argument	k1gInSc4	argument
uznal	uznat	k5eAaPmAgInS	uznat
a	a	k8xC	a
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
nakonec	nakonec	k6eAd1	nakonec
o	o	k7c6	o
přijetí	přijetí	k1gNnSc6	přijetí
vlajky	vlajka	k1gFnSc2	vlajka
s	s	k7c7	s
pěti	pět	k4xCc7	pět
hvězdami	hvězda	k1gFnPc7	hvězda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
Čínské	čínský	k2eAgFnSc2d1	čínská
lidové	lidový	k2eAgFnSc2d1	lidová
republiky	republika	k1gFnSc2	republika
</s>
</p>
<p>
<s>
Hymna	hymna	k1gFnSc1	hymna
Čínské	čínský	k2eAgFnSc2d1	čínská
lidové	lidový	k2eAgFnSc2d1	lidová
republiky	republika	k1gFnSc2	republika
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Číny	Čína	k1gFnSc2	Čína
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Čínské	čínský	k2eAgFnSc2d1	čínská
lidové	lidový	k2eAgFnSc2d1	lidová
republiky	republika	k1gFnSc2	republika
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Vlajka	vlajka	k1gFnSc1	vlajka
Čínské	čínský	k2eAgFnSc2d1	čínská
lidové	lidový	k2eAgFnSc2d1	lidová
republiky	republika	k1gFnSc2	republika
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
