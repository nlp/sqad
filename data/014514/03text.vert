<s>
Poker	poker	k1gInSc1
</s>
<s>
Královská	královský	k2eAgFnSc1d1
postupka	postupka	k1gFnSc1
Royal	Royal	k1gMnSc1
Flush	flush	k1gInSc1
</s>
<s>
Výherní	výherní	k2eAgFnSc1d1
kombinace	kombinace	k1gFnSc1
</s>
<s>
Poker	poker	k1gInSc4
(	(	kIx(
<g/>
čti	číst	k5eAaImRp2nS
pokr	pokr	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
karetní	karetní	k2eAgFnSc1d1
hra	hra	k1gFnSc1
pro	pro	k7c4
2	#num#	k4
až	až	k9
10	#num#	k4
hráčů	hráč	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poker	poker	k1gInSc1
se	se	k3xPyFc4
vyvinul	vyvinout	k5eAaPmAgInS
v	v	k7c6
USA	USA	kA
<g/>
,	,	kIx,
kde	kde	k6eAd1
ve	v	k7c6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
získal	získat	k5eAaPmAgMnS
svoji	svůj	k3xOyFgFnSc4
konečnou	konečný	k2eAgFnSc4d1
podobu	podoba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základní	základní	k2eAgInSc1d1
typ	typ	k1gInSc1
pokeru	poker	k1gInSc2
je	být	k5eAaImIp3nS
draw	draw	k?
poker	poker	k1gInSc1
<g/>
,	,	kIx,
u	u	k7c2
kterého	který	k3yRgInSc2,k3yQgInSc2,k3yIgInSc2
se	se	k3xPyFc4
při	při	k7c6
rozdávání	rozdávání	k1gNnSc6
neukazuje	ukazovat	k5eNaImIp3nS
nic	nic	k3yNnSc1
a	a	k8xC
od	od	k7c2
kterého	který	k3yRgNnSc2,k3yIgNnSc2,k3yQgNnSc2
je	být	k5eAaImIp3nS
odvozena	odvodit	k5eAaPmNgFnS
řada	řada	k1gFnSc1
nejrůznějších	různý	k2eAgFnPc2d3
variant	varianta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnSc7d1
variantou	varianta	k1gFnSc7
je	být	k5eAaImIp3nS
stud	stud	k1gInSc1
poker	poker	k1gInSc1
<g/>
,	,	kIx,
při	při	k7c6
kterém	který	k3yQgInSc6,k3yIgInSc6,k3yRgInSc6
se	se	k3xPyFc4
během	během	k7c2
rozdávání	rozdávání	k1gNnSc2
ukazuje	ukazovat	k5eAaImIp3nS
část	část	k1gFnSc1
karet	kareta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ale	ale	k8xC
dnes	dnes	k6eAd1
se	se	k3xPyFc4
hrají	hrát	k5eAaImIp3nP
hlavně	hlavně	k9
community	communit	k1gInPc1
card	card	k6eAd1
pokers	pokers	k6eAd1
<g/>
,	,	kIx,
především	především	k6eAd1
Texas	Texas	k1gInSc1
Hold	hold	k1gInSc1
<g/>
'	'	kIx"
<g/>
em	em	k?
No	no	k9
Limit	limit	k1gInSc1
<g/>
,	,	kIx,
protože	protože	k8xS
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
draw	draw	k?
pokeru	poker	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
sází	sázet	k5eAaImIp3nS
jen	jen	k9
2	#num#	k4
<g/>
×	×	k?
<g/>
,	,	kIx,
se	se	k3xPyFc4
zde	zde	k6eAd1
sází	sázet	k5eAaImIp3nS
4	#num#	k4
<g/>
×	×	k?
<g/>
,	,	kIx,
takže	takže	k8xS
je	být	k5eAaImIp3nS
více	hodně	k6eAd2
strategický	strategický	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s>
Hraje	hrát	k5eAaImIp3nS
se	se	k3xPyFc4
zpravidla	zpravidla	k6eAd1
s	s	k7c7
francouzskými	francouzský	k2eAgFnPc7d1
kartami	karta	k1gFnPc7
(	(	kIx(
<g/>
52	#num#	k4
listů	list	k1gInPc2
–	–	k?
kříže	kříž	k1gInPc1
<g/>
,	,	kIx,
káry	káry	k1gInPc1
<g/>
,	,	kIx,
srdce	srdce	k1gNnSc1
a	a	k8xC
piky	pik	k1gInPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cílem	cíl	k1gInSc7
hry	hra	k1gFnSc2
je	být	k5eAaImIp3nS
vybudovat	vybudovat	k5eAaPmF
jednu	jeden	k4xCgFnSc4
z	z	k7c2
mnoha	mnoho	k4c2
herních	herní	k2eAgFnPc2d1
kombinací	kombinace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ty	ty	k3xPp2nSc1
rozhodují	rozhodovat	k5eAaImIp3nP
o	o	k7c6
ztrátě	ztráta	k1gFnSc6
či	či	k8xC
zisku	zisk	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
hře	hra	k1gFnSc6
však	však	k9
nerozhoduje	rozhodovat	k5eNaImIp3nS
pouze	pouze	k6eAd1
kvalita	kvalita	k1gFnSc1
kombinace	kombinace	k1gFnSc2
listů	list	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc4,k3yQgInPc4,k3yIgInPc4
má	mít	k5eAaImIp3nS
hráč	hráč	k1gMnSc1
v	v	k7c6
ruce	ruka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozhoduje	rozhodovat	k5eAaImIp3nS
také	také	k9
taktika	taktika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Úplně	úplně	k6eAd1
prvním	první	k4xOgMnSc7
předchůdcem	předchůdce	k1gMnSc7
pokeru	poker	k1gInSc2
byla	být	k5eAaImAgFnS
zřejmě	zřejmě	k6eAd1
perská	perský	k2eAgFnSc1d1
karetní	karetní	k2eAgFnSc1d1
hra	hra	k1gFnSc1
As	as	k1gNnSc2
Nas	Nas	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hrála	hrát	k5eAaImAgFnS
se	se	k3xPyFc4
s	s	k7c7
25	#num#	k4
kartami	karta	k1gFnPc7
pěti	pět	k4xCc2
různých	různý	k2eAgFnPc2d1
barev	barva	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
As	as	k1gNnSc1
Nas	Nas	k1gFnPc2
se	se	k3xPyFc4
od	od	k7c2
Peršanů	Peršan	k1gMnPc2
naučili	naučit	k5eAaPmAgMnP
francouzští	francouzský	k2eAgMnPc1d1
námořníci	námořník	k1gMnPc1
a	a	k8xC
dovezli	dovézt	k5eAaPmAgMnP
ho	on	k3xPp3gInSc4
do	do	k7c2
Evropy	Evropa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
As	as	k1gNnSc2
Nasu	Nasus	k1gInSc2
se	se	k3xPyFc4
pak	pak	k6eAd1
vyvinuly	vyvinout	k5eAaPmAgFnP
hry	hra	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
daly	dát	k5eAaPmAgFnP
pokeru	poker	k1gInSc3
jméno	jméno	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Německá	německý	k2eAgFnSc1d1
Pochspiel	Pochspiel	k1gInSc1
a	a	k8xC
francouzská	francouzský	k2eAgFnSc1d1
Poque	Poque	k1gFnSc1
se	se	k3xPyFc4
hrály	hrát	k5eAaImAgInP
shodně	shodně	k6eAd1
s	s	k7c7
25	#num#	k4
kartami	karta	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poque	Poqu	k1gInSc2
pak	pak	k6eAd1
pronikla	proniknout	k5eAaPmAgFnS
do	do	k7c2
tehdejších	tehdejší	k2eAgFnPc2d1
francouzských	francouzský	k2eAgFnPc2d1
kolonií	kolonie	k1gFnPc2
v	v	k7c6
Americe	Amerika	k1gFnSc6
<g/>
,	,	kIx,
hlavně	hlavně	k9
do	do	k7c2
Louisiany	Louisiana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přejali	přejmout	k5eAaPmAgMnP
ji	on	k3xPp3gFnSc4
i	i	k9
místní	místní	k2eAgMnPc1d1
anglicky	anglicky	k6eAd1
mluvící	mluvící	k2eAgMnPc1d1
osadníci	osadník	k1gMnPc1
a	a	k8xC
okolo	okolo	k7c2
roku	rok	k1gInSc2
1800	#num#	k4
vzniká	vznikat	k5eAaImIp3nS
slovo	slovo	k1gNnSc4
„	„	k?
<g/>
poker	poker	k1gInSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgFnSc1
písemná	písemný	k2eAgFnSc1d1
zmínka	zmínka	k1gFnSc1
o	o	k7c6
pokeru	poker	k1gInSc6
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
knize	kniha	k1gFnSc6
Joe	Joe	k1gFnSc2
Cowella	Cowello	k1gNnSc2
Strasti	strast	k1gFnSc2
a	a	k8xC
slasti	slast	k1gFnSc2
hazardu	hazard	k1gInSc2
z	z	k7c2
roku	rok	k1gInSc2
1829	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cowell	Cowell	k1gInSc1
ho	on	k3xPp3gMnSc4
popisuje	popisovat	k5eAaImIp3nS
jako	jako	k8xC,k8xS
hru	hra	k1gFnSc4
čtyř	čtyři	k4xCgMnPc2
hráčů	hráč	k1gMnPc2
s	s	k7c7
dvacetikaretním	dvacetikaretní	k2eAgInSc7d1
balíčkem	balíček	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
polovině	polovina	k1gFnSc6
devatenáctého	devatenáctý	k4xOgNnSc2
století	století	k1gNnSc2
se	se	k3xPyFc4
už	už	k6eAd1
poker	poker	k1gInSc1
hraje	hrát	k5eAaImIp3nS
s	s	k7c7
52	#num#	k4
kartami	karta	k1gFnPc7
a	a	k8xC
objevují	objevovat	k5eAaImIp3nP
se	se	k3xPyFc4
dodnes	dodnes	k6eAd1
hrané	hraný	k2eAgFnSc2d1
varianty	varianta	k1gFnSc2
5	#num#	k4
<g/>
-card	-cardo	k1gNnPc2
stud	stud	k1gInSc4
a	a	k8xC
5	#num#	k4
<g/>
-card	-card	k1gMnSc1
draw	draw	k?
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
dvacátých	dvacátý	k4xOgNnPc6
letech	léto	k1gNnPc6
dvacátého	dvacátý	k4xOgNnSc2
století	století	k1gNnSc2
se	se	k3xPyFc4
poprvé	poprvé	k6eAd1
objevuje	objevovat	k5eAaImIp3nS
varianta	varianta	k1gFnSc1
Texas	Texas	k1gInSc1
Hold	hold	k1gInSc1
<g/>
'	'	kIx"
<g/>
em	em	k?
<g/>
,	,	kIx,
ve	v	k7c4
které	který	k3yQgNnSc4,k3yRgNnSc4,k3yIgNnSc4
je	být	k5eAaImIp3nS
hráčům	hráč	k1gMnPc3
rozdáno	rozdat	k5eAaPmNgNnS
po	po	k7c6
dvou	dva	k4xCgFnPc6
kartách	karta	k1gFnPc6
a	a	k8xC
pět	pět	k4xCc4
karet	kareta	k1gFnPc2
mají	mít	k5eAaImIp3nP
společných	společný	k2eAgMnPc6d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
svou	svůj	k3xOyFgFnSc4
zábavnost	zábavnost	k1gFnSc4
a	a	k8xC
komplexnost	komplexnost	k1gFnSc4
získal	získat	k5eAaPmAgInS
Texas	Texas	k1gInSc1
Hold	hold	k1gInSc1
<g/>
'	'	kIx"
<g/>
em	em	k?
rychle	rychle	k6eAd1
na	na	k7c6
oblibě	obliba	k1gFnSc6
a	a	k8xC
dnes	dnes	k6eAd1
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
nejhranější	hraný	k2eAgFnSc4d3
pokerovou	pokerový	k2eAgFnSc4d1
variantu	varianta	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1970	#num#	k4
se	se	k3xPyFc4
v	v	k7c6
Las	laso	k1gNnPc2
Vegas	Vegas	k1gInSc1
konal	konat	k5eAaImAgInS
první	první	k4xOgInSc4
ročník	ročník	k1gInSc4
World	World	k1gMnSc1
Series	Series	k1gMnSc1
of	of	k?
Poker	poker	k1gInSc1
(	(	kIx(
<g/>
WSOP	WSOP	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Světová	světový	k2eAgFnSc1d1
pokerová	pokerový	k2eAgFnSc1d1
série	série	k1gFnSc1
se	se	k3xPyFc4
postupně	postupně	k6eAd1
rozrostla	rozrůst	k5eAaPmAgFnS
a	a	k8xC
stala	stát	k5eAaPmAgFnS
se	se	k3xPyFc4
nejslavnější	slavný	k2eAgFnSc1d3
a	a	k8xC
nejprestižnější	prestižní	k2eAgFnSc1d3
pokerovou	pokerový	k2eAgFnSc7d1
turnajovou	turnajový	k2eAgFnSc7d1
sérií	série	k1gFnSc7
a	a	k8xC
vítěz	vítěz	k1gMnSc1
hlavního	hlavní	k2eAgInSc2d1
turnaje	turnaj	k1gInSc2
je	být	k5eAaImIp3nS
považován	považován	k2eAgMnSc1d1
za	za	k7c4
světového	světový	k2eAgMnSc4d1
šampiona	šampion	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Okolo	okolo	k7c2
roku	rok	k1gInSc2
2000	#num#	k4
vznikají	vznikat	k5eAaImIp3nP
i	i	k9
další	další	k2eAgFnPc4d1
velké	velký	k2eAgFnPc4d1
pokerové	pokerový	k2eAgFnPc4d1
série	série	k1gFnPc4
jako	jako	k8xS,k8xC
World	World	k1gInSc4
Poker	poker	k1gInSc1
Tour	Tour	k1gInSc1
(	(	kIx(
<g/>
WPT	WPT	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
European	European	k1gInSc1
Poker	poker	k1gInSc1
Tour	Tour	k1gInSc1
(	(	kIx(
<g/>
EPT	EPT	kA
<g/>
)	)	kIx)
nebo	nebo	k8xC
Aussie	Aussie	k1gFnSc1
Millions	Millionsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
stejné	stejný	k2eAgFnSc6d1
době	doba	k1gFnSc6
dochází	docházet	k5eAaImIp3nS
také	také	k9
k	k	k7c3
velkému	velký	k2eAgInSc3d1
rozmachu	rozmach	k1gInSc3
online	onlinout	k5eAaPmIp3nS
pokeru	poker	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Výherní	výherní	k2eAgFnSc1d1
kombinace	kombinace	k1gFnSc1
</s>
<s>
High	High	k1gMnSc1
card	card	k6eAd1
–	–	k?
vysoká	vysoký	k2eAgFnSc1d1
karta	karta	k1gFnSc1
(	(	kIx(
<g/>
karta	karta	k1gFnSc1
nejvyšší	vysoký	k2eAgFnSc2d3
hodnoty	hodnota	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
One	One	k?
pair	pair	k1gMnSc1
–	–	k?
jeden	jeden	k4xCgMnSc1
pár	pár	k4xCyI
</s>
<s>
Two	Two	k?
pair	pair	k1gMnSc1
–	–	k?
dva	dva	k4xCgInPc4
páry	pár	k1gInPc4
</s>
<s>
Three	Three	k1gFnSc1
of	of	k?
a	a	k8xC
kind	kind	k1gMnSc1
–	–	k?
trojice	trojice	k1gFnSc2
stejné	stejný	k2eAgFnSc2d1
hodnoty	hodnota	k1gFnSc2
</s>
<s>
Straight	Straight	k1gInSc1
–	–	k?
postupka	postupka	k1gFnSc1
(	(	kIx(
<g/>
pět	pět	k4xCc1
karet	kareta	k1gFnPc2
v	v	k7c6
řadě	řada	k1gFnSc6
v	v	k7c6
různých	různý	k2eAgFnPc6d1
barvách	barva	k1gFnPc6
<g/>
)	)	kIx)
</s>
<s>
Flush	flush	k1gInSc1
–	–	k?
barva	barva	k1gFnSc1
(	(	kIx(
<g/>
pět	pět	k4xCc1
karet	kareta	k1gFnPc2
stejné	stejný	k2eAgFnSc2d1
barvy	barva	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Full	Full	k1gMnSc1
house	house	k1gNnSc1
–	–	k?
trojice	trojice	k1gFnSc1
a	a	k8xC
dvojice	dvojice	k1gFnSc1
stejných	stejný	k2eAgFnPc2d1
hodnot	hodnota	k1gFnPc2
</s>
<s>
Four	Four	k1gInSc1
of	of	k?
a	a	k8xC
kind	kind	k1gMnSc1
–	–	k?
čtveřice	čtveřice	k1gFnSc1
stejné	stejný	k2eAgFnPc1d1
hodnoty	hodnota	k1gFnPc1
(	(	kIx(
<g/>
někdy	někdy	k6eAd1
také	také	k9
„	„	k?
<g/>
Poker	poker	k1gInSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
</s>
<s>
Straight	Straight	k2eAgInSc1d1
flush	flush	k1gInSc1
–	–	k?
čistá	čistý	k2eAgFnSc1d1
postupka	postupka	k1gFnSc1
(	(	kIx(
<g/>
pět	pět	k4xCc1
karet	kareta	k1gFnPc2
v	v	k7c6
řadě	řada	k1gFnSc6
a	a	k8xC
ve	v	k7c6
stejné	stejný	k2eAgFnSc6d1
barvě	barva	k1gFnSc6
<g/>
)	)	kIx)
</s>
<s>
Royal	Royal	k1gInSc1
Flush	flush	k1gInSc1
–	–	k?
královská	královský	k2eAgFnSc1d1
postupka	postupka	k1gFnSc1
(	(	kIx(
<g/>
10	#num#	k4
<g/>
,	,	kIx,
J	J	kA
<g/>
,	,	kIx,
Q	Q	kA
<g/>
,	,	kIx,
K	K	kA
<g/>
,	,	kIx,
A	a	k9
v	v	k7c6
jedné	jeden	k4xCgFnSc6
barvě	barva	k1gFnSc6
<g/>
)	)	kIx)
</s>
<s>
Verze	verze	k1gFnSc1
pokeru	poker	k1gInSc2
</s>
<s>
Hráči	hráč	k1gMnPc1
pokeru	poker	k1gInSc2
si	se	k3xPyFc3
poker	poker	k1gInSc4
průběžně	průběžně	k6eAd1
upravují	upravovat	k5eAaImIp3nP
–	–	k?
existuje	existovat	k5eAaImIp3nS
více	hodně	k6eAd2
než	než	k8xS
1	#num#	k4
000	#num#	k4
druhů	druh	k1gInPc2
pokeru	poker	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejhranější	hraný	k2eAgInSc4d3
druh	druh	k1gInSc4
pokeru	poker	k1gInSc2
<g/>
,	,	kIx,
Texas	Texas	k1gInSc1
Hold	hold	k1gInSc1
<g/>
'	'	kIx"
<g/>
em	em	k?
<g/>
,	,	kIx,
vznikl	vzniknout	k5eAaPmAgInS
ze	z	k7c2
7	#num#	k4
Card	Cardo	k1gNnPc2
Stud	stud	k1gInSc1
(	(	kIx(
<g/>
zkráceně	zkráceně	k6eAd1
7	#num#	k4
<g/>
CS	CS	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ten	ten	k3xDgMnSc1
vznikl	vzniknout	k5eAaPmAgInS
z	z	k7c2
5	#num#	k4
Card	Cardo	k1gNnPc2
Stud	stud	k1gInSc4
a	a	k8xC
ten	ten	k3xDgInSc1
z	z	k7c2
5	#num#	k4
Card	Cardo	k1gNnPc2
Draw	Draw	k1gFnPc2
pokeru	poker	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	V	kA
poslední	poslední	k2eAgFnSc7d1
dobou	doba	k1gFnSc7
je	být	k5eAaImIp3nS
především	především	k9
na	na	k7c6
nejvyšších	vysoký	k2eAgInPc6d3
limitech	limit	k1gInPc6
velmi	velmi	k6eAd1
oblíbená	oblíbený	k2eAgFnSc1d1
varianta	varianta	k1gFnSc1
Omaha	Omaha	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
Česku	Česko	k1gNnSc6
se	se	k3xPyFc4
ještě	ještě	k9
ojediněle	ojediněle	k6eAd1
hraje	hrát	k5eAaImIp3nS
také	také	k9
varianta	varianta	k1gFnSc1
pokeru	poker	k1gInSc2
s	s	k7c7
32	#num#	k4
německými	německý	k2eAgFnPc7d1
kartami	karta	k1gFnPc7
podle	podle	k7c2
pravidel	pravidlo	k1gNnPc2
v	v	k7c6
Draw	Draw	k1gFnSc6
Pokeru	poker	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
four	four	k1gInSc1
of	of	k?
a	a	k8xC
kind	kind	k1gInSc1
je	být	k5eAaImIp3nS
nejhodnotnější	hodnotný	k2eAgFnSc7d3
kombinací	kombinace	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
Schéma	schéma	k1gNnSc4
vývoje	vývoj	k1gInSc2
pokeru	poker	k1gInSc2
</s>
<s>
Pravidla	pravidlo	k1gNnPc1
</s>
<s>
Texas	Texas	k1gInSc1
Hold	hold	k1gInSc1
<g/>
'	'	kIx"
<g/>
em	em	k?
</s>
<s>
Dnes	dnes	k6eAd1
nejoblíbenější	oblíbený	k2eAgFnSc7d3
variantou	varianta	k1gFnSc7
této	tento	k3xDgFnSc2
hry	hra	k1gFnSc2
je	být	k5eAaImIp3nS
Texas	Texas	k1gInSc1
hold	hold	k1gInSc4
'	'	kIx"
<g/>
em	em	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
variantě	varianta	k1gFnSc6
dostává	dostávat	k5eAaImIp3nS
hráč	hráč	k1gMnSc1
do	do	k7c2
ruky	ruka	k1gFnSc2
dvě	dva	k4xCgFnPc4
karty	karta	k1gFnPc4
a	a	k8xC
na	na	k7c4
stůl	stůl	k1gInSc4
se	se	k3xPyFc4
postupně	postupně	k6eAd1
vykládá	vykládat	k5eAaImIp3nS
pět	pět	k4xCc4
společných	společný	k2eAgFnPc2d1
karet	kareta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
začátku	začátek	k1gInSc6
partie	partie	k1gFnSc2
je	být	k5eAaImIp3nS
vylosován	vylosován	k2eAgMnSc1d1
dealer	dealer	k1gMnSc1
(	(	kIx(
<g/>
rozdávající	rozdávající	k2eAgMnSc1d1
<g/>
)	)	kIx)
a	a	k8xC
tato	tento	k3xDgFnSc1
pozice	pozice	k1gFnSc1
se	se	k3xPyFc4
po	po	k7c6
každé	každý	k3xTgFnSc6
hře	hra	k1gFnSc6
posouvá	posouvat	k5eAaImIp3nS
o	o	k7c4
jedno	jeden	k4xCgNnSc4
místo	místo	k1gNnSc4
ve	v	k7c6
směru	směr	k1gInSc6
hodinových	hodinový	k2eAgFnPc2d1
ručiček	ručička	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hráč	hráč	k1gMnSc1
nalevo	nalevo	k6eAd1
od	od	k7c2
dealera	dealer	k1gMnSc2
vkládá	vkládat	k5eAaImIp3nS
do	do	k7c2
hry	hra	k1gFnSc2
small	small	k1gMnSc1
blind	blind	k1gMnSc1
(	(	kIx(
<g/>
malý	malý	k2eAgInSc1d1
blind	blind	k1gInSc1
<g/>
,	,	kIx,
malou	malý	k2eAgFnSc4d1
sázku	sázka	k1gFnSc4
naslepo	naslepo	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
druhý	druhý	k4xOgMnSc1
hráč	hráč	k1gMnSc1
po	po	k7c6
levici	levice	k1gFnSc6
dealera	dealer	k1gMnSc2
vkládá	vkládat	k5eAaImIp3nS
big	big	k?
blind	blind	k1gInSc1
(	(	kIx(
<g/>
velký	velký	k2eAgInSc1d1
blind	blind	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yQnSc1,k3yRnSc1
hráči	hráč	k1gMnSc3
na	na	k7c6
blindech	blind	k1gInPc6
vloží	vložit	k5eAaPmIp3nS
své	svůj	k3xOyFgFnPc4
povinné	povinný	k2eAgFnPc4d1
sázky	sázka	k1gFnPc4
<g/>
,	,	kIx,
rozdá	rozdat	k5eAaPmIp3nS
dealer	dealer	k1gMnSc1
všem	všecek	k3xTgMnPc3
hráčům	hráč	k1gMnPc3
po	po	k7c6
dvou	dva	k4xCgFnPc6
kartách	karta	k1gFnPc6
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
vidí	vidět	k5eAaImIp3nP
jenom	jenom	k9
jejich	jejich	k3xOp3gMnPc1
majitelé	majitel	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Probíhá	probíhat	k5eAaImIp3nS
první	první	k4xOgNnSc4
sázkové	sázkový	k2eAgNnSc4d1
kolo	kolo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgMnSc1
se	se	k3xPyFc4
rozhoduje	rozhodovat	k5eAaImIp3nS
hráč	hráč	k1gMnSc1
nalevo	nalevo	k6eAd1
od	od	k7c2
velkého	velký	k2eAgInSc2d1
blindu	blind	k1gInSc2
a	a	k8xC
po	po	k7c6
něm	on	k3xPp3gNnSc6
postupně	postupně	k6eAd1
ve	v	k7c6
směru	směr	k1gInSc6
hodinových	hodinový	k2eAgFnPc2d1
ručiček	ručička	k1gFnPc2
i	i	k8xC
ostatní	ostatní	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hráči	hráč	k1gMnPc1
můžou	můžou	k?
dorovnat	dorovnat	k5eAaPmF
sázku	sázka	k1gFnSc4
(	(	kIx(
<g/>
call	call	k1gInSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
navýšit	navýšit	k5eAaPmF
(	(	kIx(
<g/>
raise	raise	k6eAd1
<g/>
)	)	kIx)
nebo	nebo	k8xC
své	svůj	k3xOyFgFnPc4
karty	karta	k1gFnPc4
zahodit	zahodit	k5eAaPmF
a	a	k8xC
do	do	k7c2
hry	hra	k1gFnSc2
se	se	k3xPyFc4
nezapojit	zapojit	k5eNaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hráč	hráč	k1gMnSc1
na	na	k7c6
big	big	k?
blindu	blind	k1gInSc6
může	moct	k5eAaImIp3nS
pouze	pouze	k6eAd1
checknout	checknout	k5eAaImF,k5eAaPmF
<g/>
,	,	kIx,
pokud	pokud	k8xS
nikdo	nikdo	k3yNnSc1
před	před	k7c7
ním	on	k3xPp3gNnSc7
nenavýšil	navýšit	k5eNaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
Když	když	k8xS
skončí	skončit	k5eAaPmIp3nS
první	první	k4xOgNnSc1
sázkové	sázkový	k2eAgNnSc1d1
kolo	kolo	k1gNnSc1
<g/>
,	,	kIx,
dealer	dealer	k1gMnSc1
vyloží	vyložit	k5eAaPmIp3nS
na	na	k7c4
stůl	stůl	k1gInSc4
první	první	k4xOgFnSc2
tři	tři	k4xCgFnPc4
společné	společný	k2eAgFnPc4d1
karty	karta	k1gFnPc4
(	(	kIx(
<g/>
flop	flop	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
proběhne	proběhnout	k5eAaPmIp3nS
druhé	druhý	k4xOgNnSc4
kolo	kolo	k1gNnSc4
sázek	sázka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
probíhá	probíhat	k5eAaImIp3nS
podle	podle	k7c2
stejných	stejný	k2eAgNnPc2d1
pravidel	pravidlo	k1gNnPc2
jako	jako	k8xC,k8xS
to	ten	k3xDgNnSc1
předchozí	předchozí	k2eAgMnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
začíná	začínat	k5eAaImIp3nS
se	se	k3xPyFc4
od	od	k7c2
hráče	hráč	k1gMnSc2
na	na	k7c6
malém	malý	k2eAgInSc6d1
blindu	blind	k1gInSc6
a	a	k8xC
ve	v	k7c6
hře	hra	k1gFnSc6
není	být	k5eNaImIp3nS
žádná	žádný	k3yNgFnSc1
povinná	povinný	k2eAgFnSc1d1
sázka	sázka	k1gFnSc1
<g/>
,	,	kIx,
takže	takže	k8xS
hráčům	hráč	k1gMnPc3
přibývá	přibývat	k5eAaImIp3nS
možnost	možnost	k1gFnSc4
checkovat	checkovat	k5eAaPmF,k5eAaBmF,k5eAaImF
(	(	kIx(
<g/>
zůstat	zůstat	k5eAaPmF
ve	v	k7c6
hře	hra	k1gFnSc6
bez	bez	k7c2
vsazení	vsazení	k1gNnSc2
<g/>
,	,	kIx,
pokud	pokud	k8xS
nikdo	nikdo	k3yNnSc1
před	před	k7c7
nimi	on	k3xPp3gMnPc7
nevsadil	vsadit	k5eNaPmAgMnS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yRnSc4,k3yQnSc4,k3yInSc4
je	být	k5eAaImIp3nS
uzavřeno	uzavřen	k2eAgNnSc4d1
druhé	druhý	k4xOgNnSc4
kolo	kolo	k1gNnSc4
sázek	sázka	k1gFnPc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
na	na	k7c4
board	board	k1gInSc4
vyložena	vyložen	k2eAgFnSc1d1
čtvrtá	čtvrtá	k1gFnSc1
společná	společný	k2eAgFnSc1d1
karta	karta	k1gFnSc1
(	(	kIx(
<g/>
turn	turn	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
proběhne	proběhnout	k5eAaPmIp3nS
další	další	k2eAgNnSc1d1
sázkové	sázkový	k2eAgNnSc1d1
kolo	kolo	k1gNnSc1
a	a	k8xC
na	na	k7c4
stůl	stůl	k1gInSc4
je	být	k5eAaImIp3nS
vyložena	vyložen	k2eAgFnSc1d1
poslední	poslední	k2eAgFnSc1d1
pátá	pátá	k1gFnSc1
společná	společný	k2eAgFnSc1d1
karta	karta	k1gFnSc1
(	(	kIx(
<g/>
river	river	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proběhne	proběhnout	k5eAaPmIp3nS
poslední	poslední	k2eAgNnSc1d1
sázkové	sázkový	k2eAgNnSc1d1
kolo	kolo	k1gNnSc1
podle	podle	k7c2
stejných	stejný	k2eAgNnPc2d1
pravidel	pravidlo	k1gNnPc2
a	a	k8xC
zbývající	zbývající	k2eAgMnPc1d1
hráči	hráč	k1gMnPc1
ve	v	k7c6
hře	hra	k1gFnSc6
ukážou	ukázat	k5eAaPmIp3nP
své	svůj	k3xOyFgFnPc4
karty	karta	k1gFnPc4
(	(	kIx(
<g/>
showdown	showdown	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Majitel	majitel	k1gMnSc1
nejsilnější	silný	k2eAgFnSc2d3
pokerové	pokerový	k2eAgFnSc2d1
kombinace	kombinace	k1gFnSc2
dostane	dostat	k5eAaPmIp3nS
všechny	všechen	k3xTgInPc4
vsazené	vsazený	k2eAgInPc4d1
žetony	žeton	k1gInPc4
(	(	kIx(
<g/>
pot	pot	k1gInSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
případě	případ	k1gInSc6
dvou	dva	k4xCgNnPc2
nebo	nebo	k8xC
více	hodně	k6eAd2
stejně	stejně	k6eAd1
silných	silný	k2eAgFnPc2d1
kombinací	kombinace	k1gFnPc2
se	se	k3xPyFc4
pot	pot	k1gInSc1
dělí	dělit	k5eAaImIp3nS
mezi	mezi	k7c4
jejich	jejich	k3xOp3gMnSc4
majitele	majitel	k1gMnSc4
(	(	kIx(
<g/>
split	split	k2eAgInSc4d1
pot	pot	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hra	hra	k1gFnSc1
může	moct	k5eAaImIp3nS
skončit	skončit	k5eAaPmF
i	i	k9
před	před	k7c7
showdownem	showdowno	k1gNnSc7
<g/>
,	,	kIx,
když	když	k8xS
jeden	jeden	k4xCgMnSc1
z	z	k7c2
hráčů	hráč	k1gMnPc2
dokáže	dokázat	k5eAaPmIp3nS
přimět	přimět	k5eAaPmF
svými	svůj	k3xOyFgFnPc7
sázkami	sázka	k1gFnPc7
ostatní	ostatní	k2eAgFnPc1d1
účastníky	účastník	k1gMnPc4
partie	partie	k1gFnSc1
zahodit	zahodit	k5eAaPmF
své	svůj	k3xOyFgFnPc4
karty	karta	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Omaha	Omaha	k1gFnSc1
</s>
<s>
Omaha	Omaha	k1gFnSc1
má	mít	k5eAaImIp3nS
v	v	k7c6
podstatě	podstata	k1gFnSc6
totožná	totožný	k2eAgNnPc1d1
pravidla	pravidlo	k1gNnPc1
s	s	k7c7
variantou	varianta	k1gFnSc7
Texas	Texas	k1gInSc1
Hold	hold	k1gInSc4
<g/>
'	'	kIx"
<g/>
em	em	k?
<g/>
,	,	kIx,
ale	ale	k8xC
hráčům	hráč	k1gMnPc3
se	se	k3xPyFc4
rozdává	rozdávat	k5eAaImIp3nS
po	po	k7c6
čtyřech	čtyři	k4xCgFnPc6
kartách	karta	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pětikaretní	pětikaretní	k2eAgFnSc1d1
výherní	výherní	k2eAgFnSc1d1
kombinace	kombinace	k1gFnSc1
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
ze	z	k7c2
dvou	dva	k4xCgFnPc2
hráčových	hráčův	k2eAgFnPc2d1
karet	kareta	k1gFnPc2
a	a	k8xC
třech	tři	k4xCgFnPc6
společných	společný	k2eAgFnPc2d1
karet	kareta	k1gFnPc2
na	na	k7c6
stole	stol	k1gInSc6
(	(	kIx(
<g/>
boardu	boardo	k1gNnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
5	#num#	k4
<g/>
-Card	-Card	k1gMnSc1
Draw	Draw	k1gMnSc1
</s>
<s>
Rozdává	rozdávat	k5eAaImIp3nS
se	se	k3xPyFc4
po	po	k7c6
pěti	pět	k4xCc6
kartách	karta	k1gFnPc6
<g/>
,	,	kIx,
zbytek	zbytek	k1gInSc1
karet	kareta	k1gFnPc2
se	se	k3xPyFc4
odloží	odložit	k5eAaPmIp3nS
jako	jako	k9
talon	talon	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každý	každý	k3xTgMnSc1
hráč	hráč	k1gMnSc1
se	se	k3xPyFc4
po	po	k7c6
prostudování	prostudování	k1gNnSc6
karet	kareta	k1gFnPc2
rozhodne	rozhodnout	k5eAaPmIp3nS
<g/>
,	,	kIx,
zda	zda	k8xS
bude	být	k5eAaImBp3nS
hrát	hrát	k5eAaImF
či	či	k8xC
nikoliv	nikoliv	k9
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neúčast	neúčast	k1gFnSc1
ve	v	k7c6
hře	hra	k1gFnSc6
se	se	k3xPyFc4
hlásí	hlásit	k5eAaImIp3nS
slovem	slovo	k1gNnSc7
pass	passa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takový	takový	k3xDgMnSc1
hráč	hráč	k1gMnSc1
odloží	odložit	k5eAaPmIp3nS
karty	karta	k1gFnPc4
viditelně	viditelně	k6eAd1
na	na	k7c4
stůl	stůl	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zbývající	zbývající	k2eAgMnPc1d1
hráči	hráč	k1gMnPc1
vloží	vložit	k5eAaPmIp3nP
do	do	k7c2
banku	bank	k1gInSc2
základní	základní	k2eAgInSc4d1
vklad	vklad	k1gInSc4
ve	v	k7c6
smluvené	smluvený	k2eAgFnSc6d1
výši	výše	k1gFnSc6,k1gFnSc6wB
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
bankéř	bankéř	k1gMnSc1
mění	měnit	k5eAaImIp3nS
hráčům	hráč	k1gMnPc3
karty	karta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
vyměnit	vyměnit	k5eAaPmF
až	až	k6eAd1
tři	tři	k4xCgFnPc4
karty	karta	k1gFnPc4
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
za	za	k7c4
každou	každý	k3xTgFnSc4
kartu	karta	k1gFnSc4
se	se	k3xPyFc4
do	do	k7c2
banku	bank	k1gInSc2
platí	platit	k5eAaImIp3nS
smluvený	smluvený	k2eAgInSc4d1
vklad	vklad	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Hráči	hráč	k1gMnPc1
během	během	k7c2
hry	hra	k1gFnSc2
buď	buď	k8xC
vložený	vložený	k2eAgInSc4d1
vklad	vklad	k1gInSc4
drží	držet	k5eAaImIp3nS
<g/>
,	,	kIx,
nebo	nebo	k8xC
pasují	pasovat	k5eAaBmIp3nP,k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mohou	moct	k5eAaImIp3nP
také	také	k9
nabídku	nabídka	k1gFnSc4
zvýšit	zvýšit	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
oznámí	oznámit	k5eAaPmIp3nS
ostatním	ostatní	k2eAgMnPc3d1
hráčům	hráč	k1gMnPc3
o	o	k7c4
kolik	kolik	k4yIc4,k4yQc4,k4yRc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bank	bank	k1gInSc1
vyhrává	vyhrávat	k5eAaImIp3nS
hráč	hráč	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
zůstane	zůstat	k5eAaPmIp3nS
jako	jako	k9
poslední	poslední	k2eAgMnSc1d1
ve	v	k7c6
hře	hra	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
více	hodně	k6eAd2
hráčů	hráč	k1gMnPc2
drží	držet	k5eAaImIp3nS
své	svůj	k3xOyFgFnSc2
nabídky	nabídka	k1gFnSc2
a	a	k8xC
již	již	k6eAd1
nezvyšují	zvyšovat	k5eNaImIp3nP
bank	bank	k1gInSc4
<g/>
,	,	kIx,
ukazují	ukazovat	k5eAaImIp3nP
své	svůj	k3xOyFgInPc4
karty	karta	k1gFnPc1
na	na	k7c4
stůl	stůl	k1gInSc4
a	a	k8xC
porovnávají	porovnávat	k5eAaImIp3nP
hodnoty	hodnota	k1gFnPc1
svých	svůj	k3xOyFgInPc2
listů	list	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
rovnosti	rovnost	k1gFnSc6
hodnot	hodnota	k1gFnPc2
jednoho	jeden	k4xCgInSc2
nebo	nebo	k8xC
dvou	dva	k4xCgInPc2
párů	pár	k1gInPc2
rozhodují	rozhodovat	k5eAaImIp3nP
o	o	k7c6
vítězi	vítěz	k1gMnSc6
karty	karta	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
jsou	být	k5eAaImIp3nP
mimo	mimo	k7c4
spárované	spárovaný	k2eAgFnPc4d1
kombinace	kombinace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Limity	limit	k1gInPc1
v	v	k7c6
pokeru	poker	k1gInSc6
</s>
<s>
Limit	limit	k1gInSc1
(	(	kIx(
<g/>
fixed	fixed	k1gInSc1
limit	limit	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Pokud	pokud	k8xS
se	se	k3xPyFc4
hraje	hrát	k5eAaImIp3nS
poker	poker	k1gInSc1
s	s	k7c7
limitem	limit	k1gInSc7
<g/>
,	,	kIx,
mohou	moct	k5eAaImIp3nP
hráči	hráč	k1gMnPc1
navýšit	navýšit	k5eAaPmF
(	(	kIx(
<g/>
raise	raise	k1gFnSc1
<g/>
)	)	kIx)
sázku	sázka	k1gFnSc4
maximálně	maximálně	k6eAd1
třikrát	třikrát	k4xCNnPc1
v	v	k7c6
průběhu	průběh	k1gInSc6
jednoho	jeden	k4xCgNnSc2
sázkového	sázkový	k2eAgNnSc2d1
kola	kolo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
třetím	třetí	k4xOgInSc6
navýšení	navýšení	k1gNnSc6
mohou	moct	k5eAaImIp3nP
zbývající	zbývající	k2eAgMnPc1d1
hráči	hráč	k1gMnPc1
buď	buď	k8xC
karty	karta	k1gFnPc1
složit	složit	k5eAaPmF
a	a	k8xC
ve	v	k7c6
hře	hra	k1gFnSc6
skončit	skončit	k5eAaPmF
<g/>
,	,	kIx,
nebo	nebo	k8xC
vyrovnat	vyrovnat	k5eAaPmF,k5eAaBmF
poslední	poslední	k2eAgFnSc4d1
sázku	sázka	k1gFnSc4
(	(	kIx(
<g/>
call	call	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sázky	sázka	k1gFnPc1
a	a	k8xC
zvyšování	zvyšování	k1gNnSc1
sázek	sázka	k1gFnPc2
jsou	být	k5eAaImIp3nP
vždy	vždy	k6eAd1
v	v	k7c6
částkách	částka	k1gFnPc6
ekvivalentních	ekvivalentní	k2eAgFnPc6d1
výši	výše	k1gFnSc3,k1gFnSc3wB
povinných	povinný	k2eAgFnPc2d1
sázek	sázka	k1gFnPc2
(	(	kIx(
<g/>
blinds	blinds	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Např.	např.	kA
pokud	pokud	k8xS
jsou	být	k5eAaImIp3nP
povinné	povinný	k2eAgFnPc1d1
sázky	sázka	k1gFnPc1
1	#num#	k4
a	a	k8xC
2	#num#	k4
<g/>
,	,	kIx,
pak	pak	k6eAd1
maximální	maximální	k2eAgNnSc4d1
zvýšení	zvýšení	k1gNnSc4
je	být	k5eAaImIp3nS
2	#num#	k4
<g/>
.	.	kIx.
<g/>
Pokud	pokud	k8xS
již	již	k6eAd1
bylo	být	k5eAaImAgNnS
dosaženo	dosáhnout	k5eAaPmNgNnS
maximálního	maximální	k2eAgInSc2d1
počtu	počet	k1gInSc2
sázek	sázka	k1gFnPc2
(	(	kIx(
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
pak	pak	k6eAd1
je	být	k5eAaImIp3nS
bank	banka	k1gFnPc2
(	(	kIx(
<g/>
pot	pot	k1gInSc4
<g/>
)	)	kIx)
„	„	k?
<g/>
zmrazen	zmrazit	k5eAaPmNgInS
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Až	až	k6eAd1
s	s	k7c7
novým	nový	k2eAgNnSc7d1
sázecím	sázecí	k2eAgNnSc7d1
kolem	kolo	k1gNnSc7
je	být	k5eAaImIp3nS
opět	opět	k6eAd1
povoleno	povolen	k2eAgNnSc1d1
třikrát	třikrát	k6eAd1
zvýšit	zvýšit	k5eAaPmF
sázky	sázka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Bez	bez	k7c2
limitu	limit	k1gInSc2
(	(	kIx(
<g/>
no	no	k9
limit	limit	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
typu	typ	k1gInSc6
her	hra	k1gFnPc2
není	být	k5eNaImIp3nS
žádný	žádný	k3yNgInSc1
limit	limit	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každý	každý	k3xTgMnSc1
hráč	hráč	k1gMnSc1
může	moct	k5eAaImIp3nS
kdykoliv	kdykoliv	k6eAd1
vsadit	vsadit	k5eAaPmF
všechny	všechen	k3xTgInPc4
svoje	svůj	k3xOyFgInPc4
zbývající	zbývající	k2eAgInPc4d1
žetony	žeton	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
minimální	minimální	k2eAgFnSc1d1
sázka	sázka	k1gFnSc1
se	se	k3xPyFc4
rovná	rovnat	k5eAaImIp3nS
výši	výše	k1gFnSc4
spodního	spodní	k2eAgInSc2d1
limitu	limit	k1gInSc2
hry	hra	k1gFnSc2
(	(	kIx(
<g/>
big	big	k?
blind	blind	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
Pokud	pokud	k8xS
má	mít	k5eAaImIp3nS
hráč	hráč	k1gMnSc1
A	a	k9
12	#num#	k4
000	#num#	k4
žetonů	žeton	k1gInPc2
a	a	k8xC
vsadí	vsadit	k5eAaPmIp3nS
všechno	všechen	k3xTgNnSc1
<g/>
,	,	kIx,
hráč	hráč	k1gMnSc1
B	B	kA
si	se	k3xPyFc3
může	moct	k5eAaImIp3nS
stále	stále	k6eAd1
vsadit	vsadit	k5eAaPmF
<g/>
,	,	kIx,
ačkoliv	ačkoliv	k8xS
mu	on	k3xPp3gInSc3
zbývá	zbývat	k5eAaImIp3nS
pouze	pouze	k6eAd1
5	#num#	k4
000	#num#	k4
žetonů	žeton	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hráč	hráč	k1gMnSc1
A	a	k9
v	v	k7c6
tomto	tento	k3xDgInSc6
případě	případ	k1gInSc6
dostane	dostat	k5eAaPmIp3nS
okamžitě	okamžitě	k6eAd1
zpět	zpět	k6eAd1
svých	svůj	k3xOyFgInPc6
7	#num#	k4
000	#num#	k4
žetonů	žeton	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgInPc4,k3yRgInPc4,k3yQgInPc4
vsadil	vsadit	k5eAaPmAgInS
<g/>
,	,	kIx,
neboť	neboť	k8xC
tyto	tento	k3xDgInPc1
žetony	žeton	k1gInPc1
zbudou	zbýt	k5eAaPmIp3nP
po	po	k7c6
pokrytí	pokrytí	k1gNnSc6
částky	částka	k1gFnSc2
5	#num#	k4
000	#num#	k4
žetonů	žeton	k1gInPc2
vsazených	vsazený	k2eAgInPc2d1
hráčem	hráč	k1gMnSc7
B.	B.	kA
</s>
<s>
Limit	limit	k1gInSc1
banku	bank	k1gInSc2
(	(	kIx(
<g/>
pot	pot	k1gInSc1
limit	limit	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Maximální	maximální	k2eAgFnSc1d1
částka	částka	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
může	moct	k5eAaImIp3nS
hráč	hráč	k1gMnSc1
vsadit	vsadit	k5eAaPmF
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
rovna	roven	k2eAgFnSc1d1
součtu	součet	k1gInSc6
částky	částka	k1gFnSc2
peněz	peníze	k1gInPc2
v	v	k7c4
banku	banka	k1gFnSc4
v	v	k7c6
daném	daný	k2eAgInSc6d1
okamžiku	okamžik	k1gInSc6
a	a	k8xC
dvojnásobku	dvojnásobek	k1gInSc6
spodního	spodní	k2eAgInSc2d1
limitu	limit	k1gInSc2
hry	hra	k1gFnSc2
(	(	kIx(
<g/>
big	big	k?
blind	blind	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příklad	příklad	k1gInSc1
<g/>
:	:	kIx,
první	první	k4xOgMnSc1
hráč	hráč	k1gMnSc1
je	být	k5eAaImIp3nS
na	na	k7c6
tahu	tah	k1gInSc6
a	a	k8xC
povinné	povinný	k2eAgFnPc4d1
sázky	sázka	k1gFnPc4
(	(	kIx(
<g/>
blinds	blinds	k6eAd1
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
1	#num#	k4
a	a	k8xC
2	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
banku	bank	k1gInSc6
jsou	být	k5eAaImIp3nP
3	#num#	k4
další	další	k2eAgInPc1d1
žetony	žeton	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
chce	chtít	k5eAaImIp3nS
učinit	učinit	k5eAaImF,k5eAaPmF
maximální	maximální	k2eAgFnSc4d1
sázku	sázka	k1gFnSc4
<g/>
,	,	kIx,
pak	pak	k6eAd1
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
součet	součet	k1gInSc1
částky	částka	k1gFnSc2
v	v	k7c6
banku	bank	k1gInSc6
(	(	kIx(
<g/>
3	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
dvojnásobku	dvojnásobek	k1gInSc2
poslední	poslední	k2eAgFnSc2d1
sázky	sázka	k1gFnSc2
(	(	kIx(
<g/>
v	v	k7c6
tomto	tento	k3xDgInSc6
případě	případ	k1gInSc6
spodní	spodní	k2eAgInSc4d1
limit	limit	k1gInSc4
hry	hra	k1gFnSc2
–	–	k?
big	big	k?
blind	blind	k1gInSc1
2	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tedy	tedy	k8xC
celková	celkový	k2eAgFnSc1d1
sázka	sázka	k1gFnSc1
se	se	k3xPyFc4
rovná	rovnat	k5eAaImIp3nS
bank	banka	k1gFnPc2
(	(	kIx(
<g/>
3	#num#	k4
<g/>
)	)	kIx)
+	+	kIx~
dvojnásobný	dvojnásobný	k2eAgMnSc1d1
big	big	k?
blind	blind	k1gMnSc1
(	(	kIx(
<g/>
4	#num#	k4
<g/>
)	)	kIx)
=	=	kIx~
celkem	celkem	k6eAd1
(	(	kIx(
<g/>
7	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Způsoby	způsob	k1gInPc1
hry	hra	k1gFnSc2
</s>
<s>
Cash	cash	k1gFnSc1
game	game	k1gInSc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
způsobu	způsob	k1gInSc6
si	se	k3xPyFc3
hráč	hráč	k1gMnSc1
může	moct	k5eAaImIp3nS
koupit	koupit	k5eAaPmF
chipy	chip	k1gInPc4
(	(	kIx(
<g/>
herní	herní	k2eAgInPc4d1
žetony	žeton	k1gInPc4
<g/>
)	)	kIx)
podle	podle	k7c2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
kolik	kolik	k4yIc4,k4yQc4,k4yRc4
uzná	uznat	k5eAaPmIp3nS
za	za	k7c4
vhodné	vhodný	k2eAgNnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většinou	většinou	k6eAd1
tyto	tento	k3xDgFnPc1
hry	hra	k1gFnPc1
jsou	být	k5eAaImIp3nP
omezeny	omezit	k5eAaPmNgFnP
maximem	maximum	k1gNnSc7
a	a	k8xC
minimem	minimum	k1gNnSc7
možné	možný	k2eAgFnSc2d1
koupě	koupě	k1gFnSc2
chipů	chip	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
těchto	tento	k3xDgFnPc6
hrách	hra	k1gFnPc6
každý	každý	k3xTgMnSc1
chip	chip	k1gMnSc1
znázorňuje	znázorňovat	k5eAaImIp3nS
reálné	reálný	k2eAgInPc4d1
peníze	peníz	k1gInPc4
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
není	být	k5eNaImIp3nS
třeba	třeba	k6eAd1
čekat	čekat	k5eAaImF
na	na	k7c4
vítěze	vítěz	k1gMnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
hráči	hráč	k1gMnPc1
můžou	můžou	k?
do	do	k7c2
hry	hra	k1gFnSc2
libovolně	libovolně	k6eAd1
vstupovat	vstupovat	k5eAaImF
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
hru	hra	k1gFnSc4
opouštět	opouštět	k5eAaImF
s	s	k7c7
tím	ten	k3xDgNnSc7
že	že	k8xS
jsou	být	k5eAaImIp3nP
vyplaceni	vyplatit	k5eAaPmNgMnP
podle	podle	k7c2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
kolik	kolik	k4yQc4,k4yIc4,k4yRc4
chipů	chip	k1gInPc2
právě	právě	k6eAd1
vlastní	vlastnit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Sit	Sit	k?
<g/>
&	&	k?
<g/>
go	go	k?
(	(	kIx(
<g/>
SnG	SnG	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
způsobu	způsob	k1gInSc6
po	po	k7c4
zaplacení	zaplacení	k1gNnSc4
jednotného	jednotný	k2eAgNnSc2d1
startovného	startovné	k1gNnSc2
každý	každý	k3xTgMnSc1
hráč	hráč	k1gMnSc1
získává	získávat	k5eAaImIp3nS
stanovený	stanovený	k2eAgInSc4d1
počet	počet	k1gInSc4
chipů	chip	k1gMnPc2
(	(	kIx(
<g/>
herní	herní	k2eAgInPc1d1
žetony	žeton	k1gInPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s	s	k7c7
kterými	který	k3yIgInPc7,k3yQgInPc7,k3yRgInPc7
může	moct	k5eAaImIp3nS
ve	v	k7c6
hře	hra	k1gFnSc6
dále	daleko	k6eAd2
operovat	operovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hráči	hráč	k1gMnPc1
si	se	k3xPyFc3
sesedají	sesedat	k5eAaPmIp3nP,k5eAaImIp3nP
většinou	většinou	k6eAd1
k	k	k7c3
jednomu	jeden	k4xCgInSc3
stolu	stol	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
SnG	SnG	k1gMnSc1
končí	končit	k5eAaImIp3nS
v	v	k7c6
momentu	moment	k1gInSc6
kdy	kdy	k6eAd1
jeden	jeden	k4xCgMnSc1
hráč	hráč	k1gMnSc1
vlastní	vlastnit	k5eAaImIp3nS
všechny	všechen	k3xTgFnPc4
chipy	chipa	k1gFnPc4
ve	v	k7c6
hře	hra	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existují	existovat	k5eAaImIp3nP
ovšem	ovšem	k9
i	i	k9
varianty	varianta	k1gFnPc1
(	(	kIx(
<g/>
například	například	k6eAd1
Double	double	k2eAgInSc1d1
or	or	k?
Nothing	Nothing	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
vítězí	vítězit	k5eAaImIp3nS
hned	hned	k6eAd1
několik	několik	k4yIc4
lidí	člověk	k1gMnPc2
najednou	najednou	k6eAd1
a	a	k8xC
vyhrávají	vyhrávat	k5eAaImIp3nP
stejnou	stejný	k2eAgFnSc4d1
předem	předem	k6eAd1
danou	daný	k2eAgFnSc4d1
částku	částka	k1gFnSc4
bez	bez	k7c2
ohledu	ohled	k1gInSc2
na	na	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
kolik	kolik	k9
kdo	kdo	k3yRnSc1,k3yQnSc1,k3yInSc1
aktuálně	aktuálně	k6eAd1
vlastní	vlastnit	k5eAaImIp3nP
žetonů	žeton	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Umístění	umístění	k1gNnSc1
hráčů	hráč	k1gMnPc2
je	být	k5eAaImIp3nS
podle	podle	k7c2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
postupně	postupně	k6eAd1
vypadávali	vypadávat	k5eAaImAgMnP
ze	z	k7c2
hry	hra	k1gFnSc2
<g/>
,	,	kIx,
tedy	tedy	k8xC
neměli	mít	k5eNaImAgMnP
již	již	k9
žádné	žádný	k3yNgInPc4
chipy	chip	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
SnG	SnG	k1gFnSc6
je	být	k5eAaImIp3nS
specifické	specifický	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
vždy	vždy	k6eAd1
předem	předem	k6eAd1
určeno	určit	k5eAaPmNgNnS
kolik	kolik	k4yIc1,k4yRc1,k4yQc1
bude	být	k5eAaImBp3nS
placených	placený	k2eAgFnPc2d1
míst	místo	k1gNnPc2
<g/>
,	,	kIx,
ať	ať	k8xC,k8xS
už	už	k6eAd1
první	první	k4xOgInSc4
jeden	jeden	k4xCgInSc4
<g/>
,	,	kIx,
první	první	k4xOgInPc4
tři	tři	k4xCgInPc4
anebo	anebo	k8xC
více	hodně	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výhry	výhra	k1gFnPc1
prvního	první	k4xOgNnSc2
jsou	být	k5eAaImIp3nP
většinou	většina	k1gFnSc7
3	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
<g/>
×	×	k?
větší	veliký	k2eAgMnSc1d2
než	než	k8xS
bylo	být	k5eAaImAgNnS
zaplacení	zaplacení	k1gNnSc1
vstupu	vstup	k1gInSc2
do	do	k7c2
SnG	SnG	k1gFnSc2
<g/>
,	,	kIx,
pokud	pokud	k8xS
byl	být	k5eAaImAgInS
hrán	hrát	k5eAaImNgInS
na	na	k7c6
jednom	jeden	k4xCgInSc6
stole	stol	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Turnaje	turnaj	k1gInPc1
</s>
<s>
Tento	tento	k3xDgInSc1
způsob	způsob	k1gInSc1
hry	hra	k1gFnSc2
se	se	k3xPyFc4
neliší	lišit	k5eNaImIp3nS
moc	moc	k6eAd1
od	od	k7c2
Sit	Sit	k1gFnSc2
<g/>
&	&	k?
<g/>
Go	Go	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
zaplacení	zaplacení	k1gNnSc6
vstupního	vstupní	k2eAgInSc2d1
poplatku	poplatek	k1gInSc2
do	do	k7c2
hry	hra	k1gFnSc2
hráči	hráč	k1gMnPc1
dostávají	dostávat	k5eAaImIp3nP
stanovený	stanovený	k2eAgInSc4d1
počet	počet	k1gInSc4
chipů	chip	k1gInPc2
a	a	k8xC
sesedají	sesedat	k5eAaPmIp3nP,k5eAaImIp3nP
ke	k	k7c3
stolům	stol	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavním	hlavní	k2eAgInSc7d1
rozdílem	rozdíl	k1gInSc7
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
turnaje	turnaj	k1gInPc1
jsou	být	k5eAaImIp3nP
hrávány	hráván	k2eAgInPc1d1
na	na	k7c6
více	hodně	k6eAd2
stolech	stol	k1gInPc6
<g/>
,	,	kIx,
tudíž	tudíž	k8xC
jsou	být	k5eAaImIp3nP
hrány	hrát	k5eAaImNgInP
více	hodně	k6eAd2
hráči	hráč	k1gMnPc1
a	a	k8xC
výhry	výhra	k1gFnPc1
na	na	k7c6
prvních	první	k4xOgFnPc6
pozicích	pozice	k1gFnPc6
jsou	být	k5eAaImIp3nP
mnohonásobně	mnohonásobně	k6eAd1
vyšší	vysoký	k2eAgNnSc4d2
než	než	k8xS
zaplacení	zaplacení	k1gNnSc4
vstupu	vstup	k1gInSc2
do	do	k7c2
turnaje	turnaj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Poker	poker	k1gInSc1
jako	jako	k8xS,k8xC
hra	hra	k1gFnSc1
pravděpodobností	pravděpodobnost	k1gFnSc7
</s>
<s>
Tato	tento	k3xDgFnSc1
část	část	k1gFnSc1
článku	článek	k1gInSc2
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaPmF,k5eAaImF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Konkrétní	konkrétní	k2eAgInPc4d1
problémy	problém	k1gInPc4
<g/>
:	:	kIx,
neencyklopedický	encyklopedický	k2eNgInSc4d1
styl	styl	k1gInSc4
</s>
<s>
Poker	poker	k1gInSc1
je	být	k5eAaImIp3nS
hra	hra	k1gFnSc1
neúplných	úplný	k2eNgFnPc2d1
informací	informace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hráč	hráč	k1gMnSc1
nezná	znát	k5eNaImIp3nS,k5eAaImIp3nS
soupeřovy	soupeřův	k2eAgFnPc4d1
karty	karta	k1gFnPc4
<g/>
,	,	kIx,
ale	ale	k8xC
na	na	k7c6
základě	základ	k1gInSc6
jeho	on	k3xPp3gNnSc2,k3xOp3gNnSc2
chování	chování	k1gNnSc2
během	během	k7c2
jednotlivých	jednotlivý	k2eAgFnPc2d1
sázkových	sázkový	k2eAgFnPc2d1
kol	kola	k1gFnPc2
zařadí	zařadit	k5eAaPmIp3nS
jeho	jeho	k3xOp3gFnPc4
karty	karta	k1gFnPc4
do	do	k7c2
nějaké	nějaký	k3yIgFnSc2
skupiny	skupina	k1gFnSc2
kombinací	kombinace	k1gFnPc2
(	(	kIx(
<g/>
tzv.	tzv.	kA
range	rang	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
během	během	k7c2
partie	partie	k1gFnSc2
postupně	postupně	k6eAd1
zužuje	zužovat	k5eAaImIp3nS
a	a	k8xC
volí	volit	k5eAaImIp3nS
takový	takový	k3xDgInSc4
způsob	způsob	k1gInSc4
zahrání	zahrání	k1gNnSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
proti	proti	k7c3
soupeřově	soupeřův	k2eAgFnSc3d1
rangi	rang	k1gFnSc3
z	z	k7c2
hlediska	hledisko	k1gNnSc2
matematické	matematický	k2eAgFnSc2d1
pravděpodobnosti	pravděpodobnost	k1gFnSc2
nejvýhodnější	výhodný	k2eAgFnSc2d3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Správné	správný	k2eAgNnSc1d1
rozhodnutí	rozhodnutí	k1gNnSc1
nemusí	muset	k5eNaImIp3nS
být	být	k5eAaImF
vždy	vždy	k6eAd1
výnosné	výnosný	k2eAgFnPc1d1
v	v	k7c6
dané	daný	k2eAgFnSc6d1
situaci	situace	k1gFnSc6
<g/>
,	,	kIx,
to	ten	k3xDgNnSc1
ale	ale	k9
neznamená	znamenat	k5eNaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
není	být	k5eNaImIp3nS
výnosné	výnosný	k2eAgNnSc1d1
z	z	k7c2
dlouhodobého	dlouhodobý	k2eAgNnSc2d1
hlediska	hledisko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Poker	poker	k1gInSc1
bývá	bývat	k5eAaImIp3nS
často	často	k6eAd1
označován	označovat	k5eAaImNgInS
jako	jako	k8xC,k8xS
hazardní	hazardní	k2eAgFnSc1d1
hra	hra	k1gFnSc1
<g/>
,	,	kIx,
protože	protože	k8xS
v	v	k7c6
něm	on	k3xPp3gNnSc6
z	z	k7c2
velké	velký	k2eAgFnSc2d1
části	část	k1gFnSc2
rozhoduje	rozhodovat	k5eAaImIp3nS
prvek	prvek	k1gInSc1
náhody	náhoda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zastánci	zastánce	k1gMnPc1
pokeru	poker	k1gInSc2
jako	jako	k8xC,k8xS
dovednostní	dovednostní	k2eAgFnSc2d1
hry	hra	k1gFnSc2
ale	ale	k8xC
tvrdí	tvrdit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
prvek	prvek	k1gInSc1
náhody	náhoda	k1gFnSc2
je	být	k5eAaImIp3nS
v	v	k7c6
dlouhodobém	dlouhodobý	k2eAgNnSc6d1
měřítku	měřítko	k1gNnSc6
potlačen	potlačen	k2eAgMnSc1d1
a	a	k8xC
na	na	k7c6
vzorku	vzorek	k1gInSc6
několika	několik	k4yIc2
tisíc	tisíc	k4xCgInPc2
her	hra	k1gFnPc2
už	už	k6eAd1
rozhodují	rozhodovat	k5eAaImIp3nP
dovednosti	dovednost	k1gFnPc4
hráče	hráč	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
fenomén	fenomén	k1gInSc1
se	se	k3xPyFc4
v	v	k7c6
pokeru	poker	k1gInSc6
(	(	kIx(
<g/>
a	a	k8xC
v	v	k7c6
matematice	matematika	k1gFnSc6
<g/>
)	)	kIx)
označuje	označovat	k5eAaImIp3nS
jako	jako	k9
variance	variance	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Pravděpodobnost	pravděpodobnost	k1gFnSc1
sestavení	sestavení	k1gNnSc2
jednotlivých	jednotlivý	k2eAgFnPc2d1
kombinací	kombinace	k1gFnPc2
při	při	k7c6
pěti	pět	k4xCc6
vyložených	vyložený	k2eAgFnPc6d1
kartách	karta	k1gFnPc6
</s>
<s>
High	High	k1gInSc1
card	card	k1gInSc1
–	–	k?
1	#num#	k4
:	:	kIx,
1	#num#	k4
</s>
<s>
One	One	k?
pair	pair	k1gMnSc1
–	–	k?
1	#num#	k4
:	:	kIx,
1,37	1,37	k4
</s>
<s>
Two	Two	k?
pairs	pairs	k1gInSc1
–	–	k?
1	#num#	k4
:	:	kIx,
20	#num#	k4
</s>
<s>
Three	Three	k1gFnSc1
of	of	k?
a	a	k8xC
kind	kind	k1gInSc1
–	–	k?
1	#num#	k4
:	:	kIx,
46	#num#	k4
</s>
<s>
Straight	Straight	k1gInSc1
–	–	k?
1	#num#	k4
:	:	kIx,
253,8	253,8	k4
</s>
<s>
Flush	flush	k1gInSc1
–	–	k?
1	#num#	k4
:	:	kIx,
507,8	507,8	k4
</s>
<s>
Full	Full	k1gMnSc1
house	house	k1gNnSc1
–	–	k?
1	#num#	k4
:	:	kIx,
693,2	693,2	k4
</s>
<s>
Four	Four	k1gInSc1
of	of	k?
a	a	k8xC
kind	kind	k1gInSc1
–	–	k?
1	#num#	k4
:	:	kIx,
4	#num#	k4
164	#num#	k4
</s>
<s>
Straight	Straight	k2eAgInSc1d1
flush	flush	k1gInSc1
–	–	k?
1	#num#	k4
:	:	kIx,
72	#num#	k4
192,33	192,33	k4
</s>
<s>
Royal	Royal	k1gInSc1
flush	flush	k1gInSc1
–	–	k?
1	#num#	k4
:	:	kIx,
649	#num#	k4
740	#num#	k4
</s>
<s>
Pokerové	pokerový	k2eAgFnPc4d1
série	série	k1gFnPc4
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
</s>
<s>
European	European	k1gInSc1
Poker	poker	k1gInSc1
Tour	Tour	k1gInSc1
</s>
<s>
Série	série	k1gFnSc1
European	European	k1gInSc1
Poker	poker	k1gInSc1
Tour	Tour	k1gInSc1
(	(	kIx(
<g/>
EPT	EPT	kA
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
založena	založit	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
prosinci	prosinec	k1gInSc6
2007	#num#	k4
byl	být	k5eAaImAgMnS
v	v	k7c6
rámci	rámec	k1gInSc6
čtvrté	čtvrtá	k1gFnSc2
sezóny	sezóna	k1gFnSc2
EPT	EPT	kA
odehrán	odehrát	k5eAaPmNgInS
i	i	k9
první	první	k4xOgInSc1
turnaj	turnaj	k1gInSc1
v	v	k7c6
pražském	pražský	k2eAgInSc6d1
hotelu	hotel	k1gInSc6
Hilton	Hilton	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
České	český	k2eAgNnSc4d1
hlavní	hlavní	k2eAgNnSc4d1
město	město	k1gNnSc4
pak	pak	k6eAd1
prosincovou	prosincový	k2eAgFnSc4d1
zastávku	zastávka	k1gFnSc4
EPT	EPT	kA
hostilo	hostit	k5eAaImAgNnS
každý	každý	k3xTgInSc4
rok	rok	k1gInSc4
až	až	k9
do	do	k7c2
zániku	zánik	k1gInSc2
série	série	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
obnovení	obnovení	k1gNnSc6
značky	značka	k1gFnSc2
byla	být	k5eAaImAgFnS
Praha	Praha	k1gFnSc1
zařazena	zařadit	k5eAaPmNgFnS
mezi	mezi	k7c4
pořadatelská	pořadatelský	k2eAgNnPc4d1
města	město	k1gNnPc4
i	i	k9
pro	pro	k7c4
rok	rok	k1gInSc4
2018	#num#	k4
<g/>
,	,	kIx,
EPT	EPT	kA
se	se	k3xPyFc4
do	do	k7c2
pražského	pražský	k2eAgInSc2d1
Hiltonu	Hilton	k1gInSc2
vrátila	vrátit	k5eAaPmAgFnS
v	v	k7c6
termínu	termín	k1gInSc6
od	od	k7c2
6	#num#	k4
<g/>
.	.	kIx.
do	do	k7c2
18	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2018	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Jediným	jediný	k2eAgMnSc7d1
českým	český	k2eAgMnSc7d1
hráčem	hráč	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
dokázal	dokázat	k5eAaPmAgMnS
zvítězit	zvítězit	k5eAaPmF
v	v	k7c6
pražských	pražský	k2eAgInPc6d1
Main	Maina	k1gFnPc2
Eventech	Event	k1gInPc6
(	(	kIx(
<g/>
hlavních	hlavní	k2eAgInPc6d1
turnajích	turnaj	k1gInPc6
<g/>
)	)	kIx)
EPT	EPT	kA
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
Jan	Jan	k1gMnSc1
Škampa	škampa	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
o	o	k7c4
měsíc	měsíc	k1gInSc4
dříve	dříve	k6eAd2
obsadil	obsadit	k5eAaPmAgMnS
v	v	k7c4
Main	Main	k1gInSc4
Eventu	Event	k1gInSc2
EPT	EPT	kA
v	v	k7c6
portugalské	portugalský	k2eAgFnSc6d1
Vilamouře	Vilamoura	k1gFnSc6
4	#num#	k4
<g/>
.	.	kIx.
příčku	příčka	k1gFnSc4
<g/>
,	,	kIx,
v	v	k7c6
prosinci	prosinec	k1gInSc6
2009	#num#	k4
pak	pak	k6eAd1
nenašel	najít	k5eNaPmAgMnS
přemožitele	přemožitel	k1gMnSc4
v	v	k7c6
pražském	pražský	k2eAgInSc6d1
hlavním	hlavní	k2eAgInSc6d1
turnaji	turnaj	k1gInSc6
s	s	k7c7
buy-inem	buy-in	k1gInSc7
5	#num#	k4
250	#num#	k4
€	€	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
závěrečném	závěrečný	k2eAgInSc6d1
duelu	duel	k1gInSc6
Škampa	škampa	k1gFnSc1
porazil	porazit	k5eAaPmAgMnS
Izraelce	Izraelec	k1gMnPc4
Eyala	Eyalo	k1gNnSc2
Avitana	Avitan	k1gMnSc2
a	a	k8xC
získal	získat	k5eAaPmAgInS
vítěznou	vítězný	k2eAgFnSc4d1
odměnu	odměna	k1gFnSc4
v	v	k7c6
hodnotě	hodnota	k1gFnSc6
682	#num#	k4
000	#num#	k4
€	€	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
danou	daný	k2eAgFnSc4d1
chvíli	chvíle	k1gFnSc4
se	se	k3xPyFc4
jednalo	jednat	k5eAaImAgNnS
o	o	k7c4
největší	veliký	k2eAgFnSc4d3
odměnu	odměna	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
jakýkoli	jakýkoli	k3yIgMnSc1
český	český	k2eAgMnSc1d1
hráč	hráč	k1gMnSc1
v	v	k7c6
živém	živý	k2eAgInSc6d1
pokerovém	pokerový	k2eAgInSc6d1
turnaji	turnaj	k1gInSc6
získal	získat	k5eAaPmAgMnS
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
World	World	k1gMnSc1
Series	Series	k1gMnSc1
of	of	k?
Poker	poker	k1gInSc1
Europe	Europ	k1gInSc5
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
podepsali	podepsat	k5eAaPmAgMnP
zástupci	zástupce	k1gMnPc1
rozvadovského	rozvadovský	k2eAgNnSc2d1
kasina	kasino	k1gNnSc2
King	King	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
smlouvu	smlouva	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
jim	on	k3xPp3gMnPc3
zajistila	zajistit	k5eAaPmAgFnS
pořadatelství	pořadatelství	k1gNnSc4
nadcházejících	nadcházející	k2eAgFnPc2d1
dvou	dva	k4xCgFnPc2
edicí	edice	k1gFnPc2
festivalu	festival	k1gInSc2
World	World	k1gMnSc1
Series	Series	k1gMnSc1
of	of	k?
Poker	poker	k1gInSc1
Europe	Europ	k1gInSc5
(	(	kIx(
<g/>
WSOPE	WSOPE	kA
<g/>
)	)	kIx)
v	v	k7c6
letech	léto	k1gNnPc6
2017	#num#	k4
a	a	k8xC
2019	#num#	k4
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Premiéra	premiéra	k1gFnSc1
World	Worlda	k1gFnPc2
Series	Series	k1gMnSc1
of	of	k?
Poker	poker	k1gInSc1
Europe	Europ	k1gInSc5
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
se	se	k3xPyFc4
odehrála	odehrát	k5eAaPmAgFnS
v	v	k7c6
termínu	termín	k1gInSc6
od	od	k7c2
17	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
do	do	k7c2
10	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2017	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
jejím	její	k3xOp3gInSc6
rámci	rámec	k1gInSc6
bylo	být	k5eAaImAgNnS
odehráno	odehrát	k5eAaPmNgNnS
na	na	k7c4
poměry	poměra	k1gFnPc4
WSOPE	WSOPE	kA
rekordních	rekordní	k2eAgInPc2d1
11	#num#	k4
turnajů	turnaj	k1gInPc2
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnPc4
vítězové	vítěz	k1gMnPc1
získali	získat	k5eAaPmAgMnP
zlaté	zlatý	k2eAgInPc4d1
náramky	náramek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
třech	tři	k4xCgNnPc6
z	z	k7c2
těchto	tento	k3xDgInPc2
turnajů	turnaj	k1gInPc2
se	se	k3xPyFc4
podařilo	podařit	k5eAaPmAgNnS
zvítězit	zvítězit	k5eAaPmF
domácím	domácí	k2eAgMnPc3d1
hráčům	hráč	k1gMnPc3
<g/>
,	,	kIx,
od	od	k7c2
počátku	počátek	k1gInSc2
WSOP	WSOP	kA
v	v	k7c6
roce	rok	k1gInSc6
1970	#num#	k4
přitom	přitom	k6eAd1
zlatý	zlatý	k2eAgInSc4d1
náramek	náramek	k1gInSc4
získali	získat	k5eAaPmAgMnP
pouze	pouze	k6eAd1
Tomáš	Tomáš	k1gMnSc1
Junek	Junek	k1gMnSc1
v	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
Artur	Artur	k1gMnSc1
Rudziankov	Rudziankov	k1gInSc1
v	v	k7c6
létě	léto	k1gNnSc6
2017	#num#	k4
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Třetím	třetí	k4xOgMnSc7
českým	český	k2eAgMnSc7d1
majitelem	majitel	k1gMnSc7
náramku	náramek	k1gInSc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
Martin	Martin	k1gMnSc1
Kabrhel	Kabrhel	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
zvítězil	zvítězit	k5eAaPmAgMnS
v	v	k7c6
No	no	k9
Limit	limit	k1gInSc1
Hold	hold	k1gInSc1
<g/>
'	'	kIx"
<g/>
em	em	k?
Super	super	k1gInSc1
Turbo	turba	k1gFnSc5
Bounty	Bount	k1gInPc4
eventu	event	k1gInSc2
se	s	k7c7
vstupním	vstupní	k2eAgInSc7d1
poplatkem	poplatek	k1gInSc7
1	#num#	k4
100	#num#	k4
€	€	k?
a	a	k8xC
společně	společně	k6eAd1
s	s	k7c7
náramkem	náramek	k1gInSc7
získal	získat	k5eAaPmAgInS
odměnu	odměna	k1gFnSc4
53	#num#	k4
557	#num#	k4
€	€	k?
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
další	další	k2eAgInSc4d1
český	český	k2eAgInSc4d1
triumf	triumf	k1gInSc4
se	se	k3xPyFc4
postaral	postarat	k5eAaPmAgMnS
Lukáš	Lukáš	k1gMnSc1
Záškodný	záškodný	k2eAgMnSc1d1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
zvítězil	zvítězit	k5eAaPmAgMnS
v	v	k7c4
Pot	pot	k1gInSc4
Limit	limit	k1gInSc1
Omaha	Omaha	k1gFnSc1
8	#num#	k4
<g/>
-Max	-Max	k1gInSc4
eventu	event	k1gInSc2
vstupním	vstupní	k2eAgInSc7d1
poplatkem	poplatek	k1gInSc7
2	#num#	k4
200	#num#	k4
€	€	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Záškodný	záškodný	k2eAgMnSc1d1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
prvním	první	k4xOgMnSc7
českým	český	k2eAgMnSc7d1
hráčem	hráč	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yRgInSc4,k3yQgInSc4,k3yIgInSc4
svůj	svůj	k3xOyFgInSc4
náramek	náramek	k1gInSc4
získal	získat	k5eAaPmAgMnS
v	v	k7c6
turnaji	turnaj	k1gInSc6
hraném	hraný	k2eAgInSc6d1
v	v	k7c6
jiné	jiný	k2eAgFnSc6d1
herní	herní	k2eAgFnSc6d1
variantě	varianta	k1gFnSc6
<g/>
,	,	kIx,
než	než	k8xS
je	být	k5eAaImIp3nS
No	no	k9
Limit	limit	k1gInSc1
Hold	hold	k1gInSc1
<g/>
'	'	kIx"
<g/>
em	em	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pátým	pátá	k1gFnPc3
českým	český	k2eAgMnSc7d1
šampionem	šampion	k1gMnSc7
WSOP	WSOP	kA
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
Matouš	Matouš	k1gMnSc1
Skořepa	Skořepa	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
nenašel	najít	k5eNaPmAgMnS
přemožitele	přemožitel	k1gMnPc4
v	v	k7c6
No	no	k9
Limit	limit	k1gInSc1
Hold	hold	k1gInSc1
<g/>
'	'	kIx"
<g/>
em	em	k?
Colossus	Colossus	k1gMnSc1
eventu	event	k1gInSc2
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yIgInSc6,k3yRgInSc6,k3yQgInSc6
byl	být	k5eAaImAgInS
vstupní	vstupní	k2eAgInSc1d1
poplatek	poplatek	k1gInSc1
ve	v	k7c6
výši	výše	k1gFnSc6,k1gFnSc6wB
550	#num#	k4
€	€	k?
zaplacen	zaplacen	k2eAgInSc4d1
v	v	k7c4
4	#num#	k4
115	#num#	k4
případech	případ	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
vítězství	vítězství	k1gNnSc4
v	v	k7c6
nejpočetnějším	početní	k2eAgNnSc6d3
hráčském	hráčský	k2eAgNnSc6d1
poli	pole	k1gNnSc6
v	v	k7c6
historii	historie	k1gFnSc6
WSOPE	WSOPE	kA
Skořepa	Skořepa	k1gMnSc1
získal	získat	k5eAaPmAgMnS
odměnu	odměna	k1gFnSc4
270	#num#	k4
015	#num#	k4
€	€	k?
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
hlavním	hlavní	k2eAgInSc6d1
turnaji	turnaj	k1gInSc6
festivalu	festival	k1gInSc2
se	s	k7c7
vstupním	vstupní	k2eAgInSc7d1
poplatkem	poplatek	k1gInSc7
10	#num#	k4
350	#num#	k4
€	€	k?
bylo	být	k5eAaImAgNnS
zaplaceno	zaplatit	k5eAaPmNgNnS
529	#num#	k4
vstupů	vstup	k1gInPc2
<g/>
,	,	kIx,
šampionem	šampion	k1gMnSc7
a	a	k8xC
majitelem	majitel	k1gMnSc7
hlavní	hlavní	k2eAgFnSc2d1
odměny	odměna	k1gFnSc2
ve	v	k7c6
výši	výše	k1gFnSc6,k1gFnSc6wB
1	#num#	k4
115	#num#	k4
207	#num#	k4
€	€	k?
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
Španěl	Španěl	k1gMnSc1
Marti	Marti	k?
Roca	Roca	k1gMnSc1
de	de	k?
Torres	Torres	k1gMnSc1
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čech	Čech	k1gMnSc1
Michal	Michal	k1gMnSc1
Mrakeš	Mrakeš	k1gMnSc1
byl	být	k5eAaImAgMnS
v	v	k7c4
Main	Main	k1gNnSc4
Eventu	Event	k1gInSc2
vyřazen	vyřadit	k5eAaPmNgMnS
na	na	k7c4
14	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
s	s	k7c7
odměnou	odměna	k1gFnSc7
46	#num#	k4
594	#num#	k4
€	€	k?
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
nejlepší	dobrý	k2eAgInSc4d3
výsledek	výsledek	k1gInSc4
<g/>
,	,	kIx,
kterého	který	k3yRgMnSc4,k3yQgMnSc4,k3yIgMnSc4
český	český	k2eAgMnSc1d1
hráč	hráč	k1gMnSc1
v	v	k7c6
historii	historie	k1gFnSc6
Main	Maino	k1gNnPc2
eventů	event	k1gMnPc2
WSOPE	WSOPE	kA
dosáhl	dosáhnout	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
Během	během	k7c2
jedenácti	jedenáct	k4xCc2
náramkových	náramkový	k2eAgInPc2d1
turnajů	turnaj	k1gInPc2
bylo	být	k5eAaImAgNnS
v	v	k7c6
Rozvadově	Rozvadův	k2eAgFnSc6d1
napočítáno	napočítat	k5eAaPmNgNnS
7	#num#	k4
689	#num#	k4
registrací	registrace	k1gFnPc2
a	a	k8xC
v	v	k7c6
prizepoolech	prizepool	k1gInPc6
se	se	k3xPyFc4
sešlo	sejít	k5eAaPmAgNnS
25	#num#	k4
442	#num#	k4
796	#num#	k4
€	€	k?
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Česká	český	k2eAgFnSc1d1
pokerová	pokerový	k2eAgFnSc1d1
tour	tour	k1gMnSc1
</s>
<s>
Česká	český	k2eAgFnSc1d1
pokerová	pokerový	k2eAgFnSc1d1
tour	tour	k1gInSc1
(	(	kIx(
<g/>
zkráceně	zkráceně	k6eAd1
ČPT	ČPT	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
pokerová	pokerový	k2eAgFnSc1d1
turnajová	turnajový	k2eAgFnSc1d1
série	série	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
od	od	k7c2
roku	rok	k1gInSc2
2008	#num#	k4
koná	konat	k5eAaImIp3nS
v	v	k7c6
kasinech	kasino	k1gNnPc6
a	a	k8xC
poker	poker	k1gInSc1
roomech	roomo	k1gNnPc6
na	na	k7c6
území	území	k1gNnSc6
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
Pokerová	pokerový	k2eAgFnSc1d1
Tour	Tour	k1gMnSc1
je	být	k5eAaImIp3nS
nejstarší	starý	k2eAgMnSc1d3
nepřetržitě	přetržitě	k6eNd1
fungující	fungující	k2eAgFnSc7d1
českou	český	k2eAgFnSc7d1
turnajovou	turnajový	k2eAgFnSc7d1
sérií	série	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavním	hlavní	k2eAgMnSc7d1
mediálním	mediální	k2eAgMnSc7d1
partnerem	partner	k1gMnSc7
ČPT	ČPT	kA
je	být	k5eAaImIp3nS
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
na	na	k7c6
svém	svůj	k3xOyFgInSc6
programu	program	k1gInSc6
ČT	ČT	kA
Sport	sport	k1gInSc1
pravidelně	pravidelně	k6eAd1
vysílá	vysílat	k5eAaImIp3nS
záznamy	záznam	k1gInPc1
finálových	finálový	k2eAgNnPc2d1
klání	klání	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgInPc1d1
turnaje	turnaj	k1gInPc1
ČPT	ČPT	kA
<g/>
,	,	kIx,
Main	Main	k1gNnSc1
Eventy	Eventa	k1gFnSc2
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
odehrány	odehrát	k5eAaPmNgInP
v	v	k7c6
pokerové	pokerový	k2eAgFnSc6d1
variantě	varianta	k1gFnSc6
No	no	k9
limit	limit	k1gInSc1
hold	hold	k1gInSc1
<g/>
’	’	k?
<g/>
em	em	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jubilejní	jubilejní	k2eAgInSc4d1
stý	stý	k4xOgInSc4
Main	Maina	k1gFnPc2
Event	Eventa	k1gFnPc2
se	se	k3xPyFc4
uskutečnil	uskutečnit	k5eAaPmAgInS
v	v	k7c6
říjnu	říjen	k1gInSc6
roku	rok	k1gInSc2
2016	#num#	k4
na	na	k7c6
pražských	pražský	k2eAgNnPc6d1
Lukách	luka	k1gNnPc6
a	a	k8xC
bylo	být	k5eAaImAgNnS
v	v	k7c6
něm	on	k3xPp3gNnSc6
zaplaceno	zaplatit	k5eAaPmNgNnS
850	#num#	k4
buy-inů	buy-in	k1gMnPc2
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gFnSc4
šampion	šampion	k1gMnSc1
Adam	Adam	k1gMnSc1
Husák	Husák	k1gMnSc1
získal	získat	k5eAaPmAgMnS
odměnu	odměna	k1gFnSc4
433	#num#	k4
620	#num#	k4
Kč	Kč	kA
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgInSc1
Main	Main	k1gInSc1
Event	Event	k1gInSc1
ČPT	ČPT	kA
se	se	k3xPyFc4
odehrál	odehrát	k5eAaPmAgInS
v	v	k7c6
Casinu	Casin	k1gInSc6
Černigov	Černigov	k1gInSc1
v	v	k7c6
Hradci	Hradec	k1gInSc6
Králové	Králová	k1gFnSc2
v	v	k7c6
termínu	termín	k1gInSc6
od	od	k7c2
14	#num#	k4
<g/>
.	.	kIx.
do	do	k7c2
19	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2018	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
SVOBODA	Svoboda	k1gMnSc1
<g/>
,	,	kIx,
Tomáš	Tomáš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oficiální	oficiální	k2eAgFnSc1d1
pravidla	pravidlo	k1gNnPc4
karetních	karetní	k2eAgFnPc2d1
her	hra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
1	#num#	k4
<g/>
:	:	kIx,
Eminent	Eminent	k1gMnSc1
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7281	#num#	k4
<g/>
-	-	kIx~
<g/>
116	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Poker	poker	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
264	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
POKERMAN	POKERMAN	kA
<g/>
.	.	kIx.
<g/>
CZ	CZ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
EPT	EPT	kA
Prague	Pragu	k1gFnSc2
-	-	kIx~
Jan	Jan	k1gMnSc1
Škampa	škampa	k1gFnSc1
zvítězil	zvítězit	k5eAaPmAgInS
na	na	k7c6
domácí	domácí	k2eAgFnSc6d1
půdě	půda	k1gFnSc6
<g/>
!	!	kIx.
</s>
<s desamb="1">
|	|	kIx~
Pokerman	Pokerman	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
www.pokerman.cz	www.pokerman.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
POKERMAN	POKERMAN	kA
<g/>
.	.	kIx.
<g/>
CZ	CZ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mrazí	mrazit	k5eAaImIp3nS
nás	my	k3xPp1nPc4
v	v	k7c6
zádech	záda	k1gNnPc6
<g/>
:	:	kIx,
King	King	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
bude	být	k5eAaImBp3nS
od	od	k7c2
příštího	příští	k2eAgInSc2d1
roku	rok	k1gInSc2
domovem	domov	k1gInSc7
WSOPE	WSOPE	kA
|	|	kIx~
Pokerman	Pokerman	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
www.pokerman.cz	www.pokerman.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Tournament	Tournament	k1gInSc1
Results	Results	k1gInSc1
|	|	kIx~
Official	Official	k1gMnSc1
World	World	k1gMnSc1
Series	Series	k1gMnSc1
of	of	k?
Poker	poker	k1gInSc1
Online	Onlin	k1gInSc5
<g/>
.	.	kIx.
www.wsop.com	www.wsop.com	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
POKERMAN	POKERMAN	kA
<g/>
.	.	kIx.
<g/>
CZ	CZ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fantazie	fantazie	k1gFnSc1
<g/>
,	,	kIx,
extáze	extáze	k1gFnSc1
<g/>
,	,	kIx,
Nagano	Nagano	k1gNnSc1
<g/>
...	...	k?
<g/>
a	a	k8xC
tak	tak	k9
dál	daleko	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Artur	Artur	k1gMnSc1
Rudziankov	Rudziankov	k1gInSc4
získal	získat	k5eAaPmAgMnS
zlatý	zlatý	k2eAgInSc4d1
náramek	náramek	k1gInSc4
<g/>
!	!	kIx.
</s>
<s desamb="1">
|	|	kIx~
Pokerman	Pokerman	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
www.pokerman.cz	www.pokerman.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
POKERMAN	POKERMAN	kA
<g/>
.	.	kIx.
<g/>
CZ	CZ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rok	rok	k1gInSc4
zlatých	zlatý	k2eAgInPc2d1
náramků	náramek	k1gInPc2
<g/>
!	!	kIx.
</s>
<s desamb="1">
Historicky	historicky	k6eAd1
třetí	třetí	k4xOgMnSc1
pro	pro	k7c4
Českou	český	k2eAgFnSc4d1
republiku	republika	k1gFnSc4
získal	získat	k5eAaPmAgMnS
Martin	Martin	k1gMnSc1
Kabrhel	Kabrhel	k1gMnSc1
|	|	kIx~
Pokerman	Pokerman	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
www.pokerman.cz	www.pokerman.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
POKERMAN	POKERMAN	kA
<g/>
.	.	kIx.
<g/>
CZ	CZ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
nás	my	k3xPp1nPc2
pět	pět	k4xCc1
<g/>
:	:	kIx,
Kolosální	kolosální	k2eAgNnSc1d1
vítězství	vítězství	k1gNnSc1
Matouše	Matouš	k1gMnSc2
Skořepy	skořepa	k1gFnSc2
jej	on	k3xPp3gMnSc4
řadí	řadit	k5eAaImIp3nP
k	k	k7c3
legendám	legenda	k1gFnPc3
|	|	kIx~
Pokerman	Pokerman	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
www.pokerman.cz	www.pokerman.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
POKERMAN	POKERMAN	kA
<g/>
.	.	kIx.
<g/>
CZ	CZ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Den	den	k1gInSc1
plný	plný	k2eAgInSc1d1
zvratů	zvrat	k1gInPc2
rozhodnul	rozhodnout	k5eAaPmAgMnS
flip	flip	k1gMnSc1
<g/>
,	,	kIx,
Roca	Roca	k1gMnSc1
De	De	k?
Torres	Torres	k1gMnSc1
je	být	k5eAaImIp3nS
šamionem	šamion	k1gInSc7
ME	ME	kA
WSOPE	WSOPE	kA
|	|	kIx~
Pokerman	Pokerman	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
www.pokerman.cz	www.pokerman.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
POKERMAN	POKERMAN	kA
<g/>
.	.	kIx.
<g/>
CZ	CZ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dvanáctku	dvanáctka	k1gFnSc4
semifinalistů	semifinalista	k1gMnPc2
Main	Main	k1gMnSc1
Eventu	Event	k1gInSc2
vede	vést	k5eAaImIp3nS
Maria	Maria	k1gFnSc1
Ho	on	k3xPp3gNnSc4
<g/>
,	,	kIx,
Michal	Michal	k1gMnSc1
Mrakeš	Mrakeš	k1gMnSc1
čtrnáctý	čtrnáctý	k4xOgMnSc1
|	|	kIx~
Pokerman	Pokerman	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
www.pokerman.cz	www.pokerman.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
POKERMAN	POKERMAN	kA
<g/>
.	.	kIx.
<g/>
CZ	CZ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rekordní	rekordní	k2eAgInSc1d1
WSOPE	WSOPE	kA
je	být	k5eAaImIp3nS
pouhým	pouhý	k2eAgInSc7d1
začátkem	začátek	k1gInSc7
<g/>
,	,	kIx,
festival	festival	k1gInSc4
by	by	kYmCp3nS
se	se	k3xPyFc4
mohl	moct	k5eAaImAgInS
vrátit	vrátit	k5eAaPmF
již	již	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
|	|	kIx~
Pokerman	Pokerman	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
www.pokerman.cz	www.pokerman.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
POKERMAN	POKERMAN	kA
<g/>
.	.	kIx.
<g/>
CZ	CZ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Začínají	začínat	k5eAaImIp3nP
další	další	k2eAgNnPc4d1
Husákova	Husákův	k2eAgNnPc4d1
léta	léto	k1gNnPc4
<g/>
?	?	kIx.
</s>
<s desamb="1">
Adam	Adam	k1gMnSc1
Husák	Husák	k1gMnSc1
ovládl	ovládnout	k5eAaPmAgInS
stou	stoa	k1gFnSc4
ČPT	ČPT	kA
|	|	kIx~
Pokerman	Pokerman	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
www.pokerman.cz	www.pokerman.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1
federace	federace	k1gFnSc1
pokeru	poker	k1gInSc2
/	/	kIx~
International	International	k1gFnSc1
Federation	Federation	k1gInSc1
of	of	k?
Poker	poker	k1gInSc1
(	(	kIx(
<g/>
IFP	IFP	kA
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
poker	poker	k1gInSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
poker	poker	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Poker	poker	k1gInSc1
–	–	k?
fenomén	fenomén	k1gInSc1
nového	nový	k2eAgNnSc2d1
tisíciletí	tisíciletí	k1gNnSc2
<g/>
,	,	kIx,
sport	sport	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
7	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
2010	#num#	k4
</s>
<s>
Asociace	asociace	k1gFnSc1
českého	český	k2eAgInSc2d1
pokeru	poker	k1gInSc2
</s>
<s>
Asociace	asociace	k1gFnSc1
pokerových	pokerový	k2eAgInPc2d1
klubů	klub	k1gInPc2
o.s.	o.s.	k?
(	(	kIx(
<g/>
APoK	APoK	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Pokerfed	Pokerfed	k1gInSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
<g/>
:	:	kIx,
Mezinárodní	mezinárodní	k2eAgFnSc1d1
federace	federace	k1gFnSc1
pokeru	poker	k1gInSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Sport	sport	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4046472-6	4046472-6	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85103968	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85103968	#num#	k4
</s>
