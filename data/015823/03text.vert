<s>
Milan	Milan	k1gMnSc1
Špalek	špalek	k1gInSc4
</s>
<s>
Milan	Milan	k1gMnSc1
Špalek	špalek	k1gInSc4
Základní	základní	k2eAgFnSc1d1
informace	informace	k1gFnPc1
Jinak	jinak	k6eAd1
zvaný	zvaný	k2eAgInSc4d1
</s>
<s>
Mr	Mr	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Špalek	špalek	k1gInSc1
Přezdívka	přezdívka	k1gFnSc1
</s>
<s>
Špalek	špalek	k1gInSc1
Narození	narození	k1gNnSc2
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1966	#num#	k4
Teplice	teplice	k1gFnSc1
<g/>
,	,	kIx,
Československo	Československo	k1gNnSc1
Původ	původ	k1gInSc1
</s>
<s>
Teplice	Teplice	k1gFnPc1
Žánry	žánr	k1gInPc1
</s>
<s>
rock	rock	k1gInSc1
<g/>
,	,	kIx,
hardrock	hardrock	k1gInSc1
<g/>
,	,	kIx,
trashmetal	trashmetat	k5eAaPmAgInS,k5eAaImAgInS
Povolání	povolání	k1gNnSc4
</s>
<s>
baskytarista	baskytarista	k1gMnSc1
<g/>
,	,	kIx,
doprovodný	doprovodný	k2eAgMnSc1d1
zpěvák	zpěvák	k1gMnSc1
<g/>
,	,	kIx,
textař	textař	k1gMnSc1
<g/>
,	,	kIx,
příležitostný	příležitostný	k2eAgMnSc1d1
hlavní	hlavní	k2eAgMnSc1d1
zpěvák	zpěvák	k1gMnSc1
Nástroje	nástroj	k1gInSc2
</s>
<s>
baskytara	baskytara	k1gFnSc1
<g/>
,	,	kIx,
hlas	hlas	k1gInSc1
Hlasový	hlasový	k2eAgInSc4d1
obor	obor	k1gInSc4
</s>
<s>
Bas	bas	k1gInSc1
Aktivní	aktivní	k2eAgInSc1d1
roky	rok	k1gInPc4
</s>
<s>
1983	#num#	k4
-	-	kIx~
dodnes	dodnes	k6eAd1
Vydavatel	vydavatel	k1gMnSc1
</s>
<s>
Pink	pink	k6eAd1
Panther	Panthra	k1gFnPc2
Media	medium	k1gNnPc4
Příbuzná	příbuzný	k2eAgNnPc4d1
témata	téma	k1gNnPc4
</s>
<s>
Kabát	Kabát	k1gMnSc1
<g/>
,	,	kIx,
Olympic	Olympic	k1gMnSc1
Významná	významný	k2eAgFnSc1d1
díla	dílo	k1gNnPc4
</s>
<s>
"	"	kIx"
<g/>
Žízeň	žízeň	k1gFnSc4
<g/>
"	"	kIx"
<g/>
,	,	kIx,
<g/>
"	"	kIx"
<g/>
Moderní	moderní	k2eAgNnSc1d1
děvče	děvče	k1gNnSc1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
<g/>
"	"	kIx"
<g/>
Šaman	šaman	k1gMnSc1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
"	"	kIx"
<g/>
Pohoda	pohoda	k1gFnSc1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
<g/>
"	"	kIx"
<g/>
Burlaci	Burlace	k1gFnSc4
<g/>
"	"	kIx"
<g/>
,	,	kIx,
<g/>
"	"	kIx"
<g/>
Kdoví	kdoví	k0
jestli	jestli	k9
<g/>
"	"	kIx"
Ocenění	ocenění	k1gNnSc1
</s>
<s>
Výroční	výroční	k2eAgFnPc4d1
ceny	cena	k1gFnPc4
OSA	osa	k1gFnSc1
-	-	kIx~
Nejúspěšnější	úspěšný	k2eAgMnSc1d3
textař	textař	k1gMnSc1
roku	rok	k1gInSc2
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
,	,	kIx,
2019	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Osobnost	osobnost	k1gFnSc1
roku	rok	k1gInSc2
Ústeckého	ústecký	k2eAgInSc2d1
kraje	kraj	k1gInSc2
-	-	kIx~
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k7c2
Manžel	manžel	k1gMnSc1
<g/>
(	(	kIx(
<g/>
ka	ka	k?
<g/>
)	)	kIx)
</s>
<s>
Pavla	Pavla	k1gFnSc1
Špalková	špalkový	k2eAgFnSc1d1
Děti	dítě	k1gFnPc1
</s>
<s>
syn	syn	k1gMnSc1
Sídlo	sídlo	k1gNnSc4
</s>
<s>
Vrahožily	Vrahožit	k5eAaImAgFnP,k5eAaPmAgFnP
Web	web	k1gInSc4
</s>
<s>
https://kabat.cz/milan-spalek/	https://kabat.cz/milan-spalek/	k?
Významný	významný	k2eAgInSc1d1
nástroj	nástroj	k1gInSc1
</s>
<s>
Fender	Fender	k1gMnSc1
Precision	Precision	k1gInSc4
Bass	Bass	k1gMnSc1
<g/>
,	,	kIx,
Vivian	Viviana	k1gFnPc2
-	-	kIx~
Jan	Jan	k1gMnSc1
Vlasák	Vlasák	k1gMnSc1
<g/>
,	,	kIx,
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Milan	Milan	k1gMnSc1
Špalek	špalek	k1gInSc1
(	(	kIx(
<g/>
*	*	kIx~
10	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1966	#num#	k4
Teplice	teplice	k1gFnSc1
<g/>
,	,	kIx,
Československo	Československo	k1gNnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
český	český	k2eAgMnSc1d1
rockový	rockový	k2eAgMnSc1d1
baskytarista	baskytarista	k1gMnSc1
<g/>
,	,	kIx,
příležitostný	příležitostný	k2eAgMnSc1d1
hlavní	hlavní	k2eAgMnSc1d1
zpěvák	zpěvák	k1gMnSc1
a	a	k8xC
textař	textař	k1gMnSc1
skupiny	skupina	k1gFnSc2
Kabát	Kabát	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
písním	píseň	k1gFnPc3
„	„	k?
<g/>
Colorado	Colorado	k1gNnSc4
<g/>
“	“	k?
ze	z	k7c2
stejnojmenného	stejnojmenný	k2eAgNnSc2d1
alba	album	k1gNnSc2
a	a	k8xC
„	„	k?
<g/>
Dávám	dávat	k5eAaImIp1nS
ti	ten	k3xDgMnPc1
jeden	jeden	k4xCgInSc4
den	den	k1gInSc4
<g/>
“	“	k?
složil	složit	k5eAaPmAgMnS
i	i	k9
hudbu	hudba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
založení	založení	k1gNnSc6
skupiny	skupina	k1gFnSc2
Kabát	kabát	k1gInSc1
byl	být	k5eAaImAgMnS
hlavním	hlavní	k2eAgMnSc7d1
zpěvákem	zpěvák	k1gMnSc7
<g/>
,	,	kIx,
pak	pak	k6eAd1
ho	on	k3xPp3gMnSc4
nahradil	nahradit	k5eAaPmAgMnS
Josef	Josef	k1gMnSc1
Vojtek	Vojtek	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnSc7
koníčkem	koníček	k1gMnSc7
je	být	k5eAaImIp3nS
rybaření	rybařený	k2eAgMnPc1d1
<g/>
,	,	kIx,
bowling	bowling	k1gInSc4
<g/>
,	,	kIx,
a	a	k8xC
poker	poker	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2015	#num#	k4
mu	on	k3xPp3gMnSc3
byla	být	k5eAaImAgNnP
v	v	k7c6
Zámecké	zámecký	k2eAgFnSc6d1
zahradě	zahrada	k1gFnSc6
Teplice	teplice	k1gFnSc2
odhalena	odhalen	k2eAgFnSc1d1
socha	socha	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Původní	původní	k2eAgFnSc1d1
profese	profese	k1gFnSc1
</s>
<s>
Instalatér	instalatér	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Diskografie	diskografie	k1gFnSc1
</s>
<s>
Více	hodně	k6eAd2
informací	informace	k1gFnPc2
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Kabát	kabát	k1gInSc1
(	(	kIx(
<g/>
hudební	hudební	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Název	název	k1gInSc1
</s>
<s>
Rok	rok	k1gInSc1
vydání	vydání	k1gNnSc2
</s>
<s>
Má	mít	k5eAaImIp3nS
ji	on	k3xPp3gFnSc4
motorovou	motorový	k2eAgFnSc4d1
</s>
<s>
1991	#num#	k4
</s>
<s>
Děvky	děvka	k1gFnPc1
tyto	tento	k3xDgFnPc4
znaj	znaj	k?
</s>
<s>
1993	#num#	k4
</s>
<s>
Colorado	Colorado	k1gNnSc1
</s>
<s>
1994	#num#	k4
</s>
<s>
Země	země	k1gFnSc1
plná	plný	k2eAgFnSc1d1
trpaslíků	trpaslík	k1gMnPc2
</s>
<s>
1995	#num#	k4
</s>
<s>
Čert	čert	k1gMnSc1
na	na	k7c6
koze	koza	k1gFnSc6
jel	jet	k5eAaImAgMnS
</s>
<s>
1997	#num#	k4
</s>
<s>
MegaHu	MegaHu	k6eAd1
</s>
<s>
1999	#num#	k4
</s>
<s>
Go	Go	k?
satane	satan	k1gMnSc5
go	go	k?
</s>
<s>
2000	#num#	k4
</s>
<s>
Dole	dole	k6eAd1
v	v	k7c6
dole	dol	k1gInSc6
</s>
<s>
2003	#num#	k4
</s>
<s>
Corrida	Corrida	k1gFnSc1
</s>
<s>
2006	#num#	k4
</s>
<s>
Banditi	Banditi	k?
di	di	k?
Praga	Praga	k1gFnSc1
</s>
<s>
2010	#num#	k4
</s>
<s>
Do	do	k7c2
pekla	peklo	k1gNnSc2
<g/>
/	/	kIx~
<g/>
do	do	k7c2
nebe	nebe	k1gNnSc2
</s>
<s>
2015	#num#	k4
</s>
<s>
bude	být	k5eAaImBp3nS
oznámeno	oznámen	k2eAgNnSc1d1
</s>
<s>
2021	#num#	k4
</s>
<s>
Nazpívané	nazpívaný	k2eAgFnPc1d1
písně	píseň	k1gFnPc1
</s>
<s>
Hlavním	hlavní	k2eAgMnSc7d1
zpěvákem	zpěvák	k1gMnSc7
skupiny	skupina	k1gFnSc2
Kabát	Kabát	k1gMnSc1
je	být	k5eAaImIp3nS
Josef	Josef	k1gMnSc1
Vojtek	Vojtek	k1gMnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
některé	některý	k3yIgFnPc1
pomalejší	pomalý	k2eAgFnPc1d2
(	(	kIx(
<g/>
někdy	někdy	k6eAd1
také	také	k9
baladičtější	baladický	k2eAgFnSc1d2
<g/>
)	)	kIx)
zpívá	zpívat	k5eAaImIp3nS
právě	právě	k9
Milan	Milan	k1gMnSc1
Špalek	špalek	k1gInSc1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
tabulka	tabulka	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Název	název	k1gInSc1
písně	píseň	k1gFnSc2
</s>
<s>
Album	album	k1gNnSc1
</s>
<s>
Autor	autor	k1gMnSc1
</s>
<s>
"	"	kIx"
<g/>
Sex	sex	k1gInSc4
<g/>
,	,	kIx,
drogy	droga	k1gFnPc4
<g/>
,	,	kIx,
rock	rock	k1gInSc4
"	"	kIx"
<g/>
n	n	k0
<g/>
"	"	kIx"
roll	rollum	k1gNnPc2
<g/>
"	"	kIx"
</s>
<s>
Má	mít	k5eAaImIp3nS
ji	on	k3xPp3gFnSc4
motorovou	motorový	k2eAgFnSc4d1
</s>
<s>
(	(	kIx(
<g/>
Špalek	špalek	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
"	"	kIx"
<g/>
Balada	balada	k1gFnSc1
o	o	k7c6
špinavejch	špinavejch	k?
fuseklích	fusekle	k1gFnPc6
<g/>
"	"	kIx"
</s>
<s>
Čert	čert	k1gMnSc1
na	na	k7c6
koze	koza	k1gFnSc6
jel	jet	k5eAaImAgMnS
</s>
<s>
(	(	kIx(
<g/>
Váňa	Váňa	k1gMnSc1
<g/>
/	/	kIx~
<g/>
Špalek	špalek	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
"	"	kIx"
<g/>
Ďábel	ďábel	k1gMnSc1
a	a	k8xC
syn	syn	k1gMnSc1
<g/>
"	"	kIx"
</s>
<s>
MegaHu	MegaHu	k6eAd1
</s>
<s>
"	"	kIx"
<g/>
Ohroženej	Ohroženej	k?
druh	druh	k1gInSc1
<g/>
"	"	kIx"
</s>
<s>
Go	Go	k?
satane	satan	k1gMnSc5
go	go	k?
</s>
<s>
"	"	kIx"
<g/>
Joy	Joy	k1gMnPc5
<g/>
"	"	kIx"
</s>
<s>
Corrida	Corrida	k1gFnSc1
</s>
<s>
"	"	kIx"
<g/>
Potkali	potkat	k5eAaPmAgMnP
se	se	k3xPyFc4
v	v	k7c6
Paříži	Paříž	k1gFnSc6
<g/>
"	"	kIx"
</s>
<s>
Banditi	Banditi	k?
di	di	k?
Praga	Praga	k1gFnSc1
</s>
<s>
"	"	kIx"
<g/>
Valkýra	valkýra	k1gFnSc1
<g/>
"	"	kIx"
</s>
<s>
Do	do	k7c2
pekla	peklo	k1gNnSc2
<g/>
/	/	kIx~
<g/>
do	do	k7c2
nebe	nebe	k1gNnSc2
</s>
<s>
"	"	kIx"
<g/>
Houby	houby	k6eAd1
magický	magický	k2eAgInSc1d1
<g/>
"	"	kIx"
</s>
<s>
(	(	kIx(
<g/>
Špalek	špalek	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
Dílo	dílo	k1gNnSc1
</s>
<s>
Je	být	k5eAaImIp3nS
dvorním	dvorní	k2eAgMnSc7d1
textařem	textař	k1gMnSc7
skupiny	skupina	k1gFnSc2
Kabát	Kabát	k1gMnSc1
a	a	k8xC
občasně	občasně	k6eAd1
také	také	k9
skládá	skládat	k5eAaImIp3nS
hudbu	hudba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hudbu	hudba	k1gFnSc4
mu	on	k3xPp3gMnSc3
skládá	skládat	k5eAaImIp3nS
kytarové	kytarový	k2eAgNnSc1d1
duo	duo	k1gNnSc1
Váňa	Váňa	k1gMnSc1
<g/>
/	/	kIx~
<g/>
Krulich	Krulich	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Mezi	mezi	k7c4
jeho	jeho	k3xOp3gNnPc4
významná	významný	k2eAgNnPc4d1
díla	dílo	k1gNnPc4
patříː	patříː	k?
</s>
<s>
Kabát	Kabát	k1gMnSc1
</s>
<s>
Název	název	k1gInSc1
písně	píseň	k1gFnSc2
</s>
<s>
Autor	autor	k1gMnSc1
</s>
<s>
Album	album	k1gNnSc1
</s>
<s>
"	"	kIx"
<g/>
Máš	mít	k5eAaImIp2nS
to	ten	k3xDgNnSc1
už	už	k6eAd1
za	za	k7c7
sebou	se	k3xPyFc7
<g/>
"	"	kIx"
</s>
<s>
(	(	kIx(
<g/>
Hurčík	Hurčík	k1gInSc1
<g/>
/	/	kIx~
<g/>
Špalek	špalek	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Má	mít	k5eAaImIp3nS
ji	on	k3xPp3gFnSc4
motorovou	motorový	k2eAgFnSc4d1
</s>
<s>
"	"	kIx"
<g/>
Má	mít	k5eAaImIp3nS
ji	on	k3xPp3gFnSc4
motorovou	motorový	k2eAgFnSc4d1
<g/>
"	"	kIx"
</s>
<s>
(	(	kIx(
<g/>
Špalek	špalek	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
"	"	kIx"
<g/>
Porcelánový	porcelánový	k2eAgInSc1d1
prasata	prase	k1gNnPc1
<g/>
"	"	kIx"
</s>
<s>
(	(	kIx(
<g/>
Špalek	špalek	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
Děvky	děvka	k1gFnPc1
tyto	tento	k3xDgFnPc4
znaj	znaj	k?
</s>
<s>
"	"	kIx"
<g/>
Opilci	opilec	k1gMnPc1
v	v	k7c6
dějinách	dějiny	k1gFnPc6
<g/>
"	"	kIx"
</s>
<s>
(	(	kIx(
<g/>
Krulich	Krulich	k1gInSc1
<g/>
/	/	kIx~
<g/>
Špalek	špalek	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
"	"	kIx"
<g/>
Óda	óda	k1gFnSc1
na	na	k7c6
konopí	konopí	k1gNnSc6
<g/>
"	"	kIx"
</s>
<s>
"	"	kIx"
<g/>
Děvky	děvka	k1gFnPc1
tyto	tento	k3xDgFnPc1
znaj	znaj	k?
<g/>
"	"	kIx"
</s>
<s>
"	"	kIx"
<g/>
Žízeň	žízeň	k1gFnSc4
<g/>
"	"	kIx"
</s>
<s>
"	"	kIx"
<g/>
Moderní	moderní	k2eAgNnSc1d1
děvče	děvče	k1gNnSc1
<g/>
"	"	kIx"
</s>
<s>
(	(	kIx(
<g/>
Váňa	Váňa	k1gMnSc1
<g/>
/	/	kIx~
<g/>
Špalek	špalek	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
"	"	kIx"
<g/>
Centryfuga	Centryfuga	k1gFnSc1
<g/>
"	"	kIx"
</s>
<s>
Colorado	Colorado	k1gNnSc1
</s>
<s>
"	"	kIx"
<g/>
Starej	starat	k5eAaImRp2nS
bar	bar	k1gInSc1
<g/>
"	"	kIx"
</s>
<s>
(	(	kIx(
<g/>
Krulich	Krulich	k1gInSc1
<g/>
/	/	kIx~
<g/>
Špalek	špalek	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
"	"	kIx"
<g/>
Colorado	Colorado	k1gNnSc1
<g/>
"	"	kIx"
</s>
<s>
(	(	kIx(
<g/>
Špalek	špalek	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
"	"	kIx"
<g/>
Kdyby	kdyby	kYmCp3nS
ženský	ženský	k2eAgInSc4d1
nebyly	být	k5eNaImAgInP
<g/>
"	"	kIx"
</s>
<s>
(	(	kIx(
<g/>
Krulich	Krulich	k1gInSc1
<g/>
/	/	kIx~
<g/>
Špalek	špalek	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
"	"	kIx"
<g/>
Piju	pít	k5eAaImIp1nS
já	já	k3xPp1nSc1
<g/>
,	,	kIx,
piju	pít	k5eAaImIp1nS
rád	rád	k6eAd1
<g/>
"	"	kIx"
</s>
<s>
Země	země	k1gFnSc1
plná	plný	k2eAgFnSc1d1
trpaslíků	trpaslík	k1gMnPc2
</s>
<s>
"	"	kIx"
<g/>
Dávám	dávat	k5eAaImIp1nS
ti	ty	k3xPp2nSc3
jeden	jeden	k4xCgInSc4
den	den	k1gInSc4
<g/>
"	"	kIx"
</s>
<s>
(	(	kIx(
<g/>
Špalek	špalek	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
"	"	kIx"
<g/>
Tak	tak	k9
teda	teda	k?
pojď	jít	k5eAaImRp2nS
<g/>
"	"	kIx"
</s>
<s>
"	"	kIx"
<g/>
Láďa	Láďa	k1gMnSc1
<g/>
"	"	kIx"
</s>
<s>
(	(	kIx(
<g/>
Vojtek	Vojtek	k1gMnSc1
<g/>
,	,	kIx,
Špalek	špalek	k1gInSc1
<g/>
/	/	kIx~
<g/>
Špalek	špalek	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
"	"	kIx"
<g/>
Čert	čert	k1gMnSc1
na	na	k7c6
koze	koza	k1gFnSc6
jel	jet	k5eAaImAgMnS
<g/>
"	"	kIx"
</s>
<s>
(	(	kIx(
<g/>
Špalek	špalek	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
Čert	čert	k1gMnSc1
na	na	k7c6
koze	koza	k1gFnSc6
jel	jet	k5eAaImAgMnS
</s>
<s>
"	"	kIx"
<g/>
Wonder	Wondero	k1gNnPc2
<g/>
"	"	kIx"
</s>
<s>
(	(	kIx(
<g/>
Váňa	Váňa	k1gMnSc1
<g/>
/	/	kIx~
<g/>
Špalek	špalek	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
"	"	kIx"
<g/>
Lady	lady	k1gFnPc6
Lane	Lan	k1gInSc2
<g/>
"	"	kIx"
</s>
<s>
"	"	kIx"
<g/>
Boogie	Boogie	k1gFnSc1
českýho	českýho	k?
šífaře	šífař	k1gMnSc2
<g/>
"	"	kIx"
</s>
<s>
(	(	kIx(
<g/>
Krulich	Krulich	k1gInSc1
<g/>
/	/	kIx~
<g/>
Hájíček	hájíček	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
"	"	kIx"
<g/>
Balada	balada	k1gFnSc1
o	o	k7c6
špinavejch	špinavejch	k?
fuseklích	fusekle	k1gFnPc6
<g/>
"	"	kIx"
</s>
<s>
(	(	kIx(
<g/>
Váňa	Váňa	k1gMnSc1
<g/>
/	/	kIx~
<g/>
Špalek	špalek	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
"	"	kIx"
<g/>
Blues	blues	k1gNnSc1
folsomské	folsomský	k2eAgFnSc2d1
věznice	věznice	k1gFnSc2
<g/>
"	"	kIx"
(	(	kIx(
<g/>
Greenhorns	Greenhorns	k1gInSc1
cover	cover	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
(	(	kIx(
<g/>
Cash	cash	k1gFnSc6
<g/>
/	/	kIx~
<g/>
Vyčítal	vyčítat	k5eAaImAgInS
<g/>
)	)	kIx)
</s>
<s>
"	"	kIx"
<g/>
Všechno	všechen	k3xTgNnSc1
bude	být	k5eAaImBp3nS
jako	jako	k9
dřív	dříve	k6eAd2
<g/>
"	"	kIx"
</s>
<s>
(	(	kIx(
<g/>
tradicionál	tradicionál	k1gInSc1
<g/>
/	/	kIx~
<g/>
Špalek	špalek	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
"	"	kIx"
<g/>
Králíci	Králík	k1gMnPc5
<g/>
"	"	kIx"
</s>
<s>
(	(	kIx(
<g/>
Špalek	špalek	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
"	"	kIx"
<g/>
Teta	Teta	k1gFnSc1
<g/>
"	"	kIx"
</s>
<s>
MegaHu	MegaHu	k6eAd1
</s>
<s>
"	"	kIx"
<g/>
Víte	vědět	k5eAaImIp2nP
jak	jak	k6eAd1
to	ten	k3xDgNnSc4
bolí	bolet	k5eAaImIp3nP
<g/>
"	"	kIx"
</s>
<s>
"	"	kIx"
<g/>
Mám	mít	k5eAaImIp1nS
obě	dva	k4xCgFnPc4
ruce	ruka	k1gFnPc4
levý	levý	k2eAgInSc4d1
<g/>
"	"	kIx"
</s>
<s>
(	(	kIx(
<g/>
Špalek	špalek	k1gInSc1
<g/>
/	/	kIx~
<g/>
Hájíček	hájíček	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
"	"	kIx"
<g/>
Bruce	Bruec	k1gInPc4
Willis	Willis	k1gInSc1
<g/>
"	"	kIx"
</s>
<s>
(	(	kIx(
<g/>
Váňa	Váňa	k1gMnSc1
<g/>
/	/	kIx~
<g/>
Špalek	špalek	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
"	"	kIx"
<g/>
Ďábel	ďábel	k1gMnSc1
a	a	k8xC
syn	syn	k1gMnSc1
<g/>
"	"	kIx"
</s>
<s>
"	"	kIx"
<g/>
Prdel	prdel	k1gFnSc1
vody	voda	k1gFnSc2
<g/>
"	"	kIx"
</s>
<s>
(	(	kIx(
<g/>
Krulich	Krulich	k1gInSc1
<g/>
/	/	kIx~
<g/>
Špalek	špalek	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
"	"	kIx"
<g/>
Šaman	šaman	k1gMnSc1
<g/>
"	"	kIx"
</s>
<s>
"	"	kIx"
<g/>
Go	Go	k1gMnSc5
satane	satan	k1gMnSc5
go	go	k?
<g/>
"	"	kIx"
</s>
<s>
(	(	kIx(
<g/>
Špalek	špalek	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
Go	Go	k?
satane	satan	k1gMnSc5
go	go	k?
</s>
<s>
"	"	kIx"
<g/>
V	v	k7c6
pekle	peklo	k1gNnSc6
sudy	suda	k1gFnSc2
válej	válet	k5eAaImRp2nS
<g/>
"	"	kIx"
</s>
<s>
(	(	kIx(
<g/>
Krulich	Krulich	k1gInSc1
<g/>
/	/	kIx~
<g/>
Špalek	špalek	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
"	"	kIx"
<g/>
Na	na	k7c4
sever	sever	k1gInSc4
<g/>
"	"	kIx"
</s>
<s>
(	(	kIx(
<g/>
Váňa	Váňa	k1gMnSc1
<g/>
/	/	kIx~
<g/>
Špalek	špalek	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
"	"	kIx"
<g/>
Mravenci	mravenec	k1gMnPc5
<g/>
"	"	kIx"
</s>
<s>
(	(	kIx(
<g/>
Krulich	Krulich	k1gInSc1
<g/>
/	/	kIx~
<g/>
Špalek	špalek	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
"	"	kIx"
<g/>
Bára	Bára	k1gFnSc1
<g/>
"	"	kIx"
</s>
<s>
"	"	kIx"
<g/>
Bum	bum	k0
bum	bum	k0
tequila	tequila	k1gFnSc1
<g/>
"	"	kIx"
</s>
<s>
"	"	kIx"
<g/>
Pohoda	pohoda	k1gFnSc1
<g/>
"	"	kIx"
</s>
<s>
Suma	suma	k1gFnSc1
Sumárum	sumárum	k9
</s>
<s>
"	"	kIx"
<g/>
Dole	dole	k6eAd1
v	v	k7c6
dole	dol	k1gInSc6
<g/>
"	"	kIx"
</s>
<s>
Dole	dole	k6eAd1
v	v	k7c6
dole	dol	k1gInSc6
</s>
<s>
"	"	kIx"
<g/>
Stará	starý	k2eAgFnSc1d1
Lou	Lou	k1gFnSc1
<g/>
"	"	kIx"
</s>
<s>
(	(	kIx(
<g/>
Váňa	Váňa	k1gMnSc1
<g/>
/	/	kIx~
<g/>
Špalek	špalek	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
"	"	kIx"
<g/>
Kalamity	kalamita	k1gFnPc1
Jane	Jan	k1gMnSc5
<g/>
"	"	kIx"
</s>
<s>
(	(	kIx(
<g/>
Krulich	Krulich	k1gInSc1
<g/>
/	/	kIx~
<g/>
Špalek	špalek	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
"	"	kIx"
<g/>
Bahno	bahno	k1gNnSc1
<g/>
"	"	kIx"
</s>
<s>
(	(	kIx(
<g/>
Váňa	Váňa	k1gMnSc1
<g/>
/	/	kIx~
<g/>
Špalek	špalek	k1gInSc1
<g/>
,	,	kIx,
Hájíček	hájíček	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
"	"	kIx"
<g/>
Frau	Fraus	k1gInSc2
Vogelrauch	Vogelrauch	k1gInSc1
<g/>
"	"	kIx"
</s>
<s>
(	(	kIx(
<g/>
Krulich	Krulich	k1gInSc1
<g/>
/	/	kIx~
<g/>
Špalek	špalek	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
"	"	kIx"
<g/>
Shořel	shořet	k5eAaPmAgInS
náš	náš	k3xOp1gInSc1
dům	dům	k1gInSc1
<g/>
"	"	kIx"
</s>
<s>
(	(	kIx(
<g/>
Špalek	špalek	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
"	"	kIx"
<g/>
Corrida	Corrida	k1gFnSc1
<g/>
"	"	kIx"
</s>
<s>
(	(	kIx(
<g/>
Váňa	Váňa	k1gMnSc1
<g/>
/	/	kIx~
<g/>
Špalek	špalek	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Corrida	Corrida	k1gFnSc1
</s>
<s>
"	"	kIx"
<g/>
Burlaci	Burlace	k1gFnSc6
<g/>
"	"	kIx"
</s>
<s>
"	"	kIx"
<g/>
Úsměv	úsměv	k1gInSc1
pana	pan	k1gMnSc2
Lloyda	Lloyd	k1gMnSc2
<g/>
"	"	kIx"
</s>
<s>
"	"	kIx"
<g/>
Malá	malý	k2eAgFnSc1d1
dáma	dáma	k1gFnSc1
<g/>
"	"	kIx"
</s>
<s>
"	"	kIx"
<g/>
Raci	rak	k1gMnPc1
v	v	k7c6
práci	práce	k1gFnSc6
s.	s.	k?
r.	r.	kA
o.	o.	k?
<g/>
"	"	kIx"
</s>
<s>
"	"	kIx"
<g/>
Virtuoz	Virtuoz	k1gInSc4
<g/>
"	"	kIx"
</s>
<s>
(	(	kIx(
<g/>
Krulich	Krulich	k1gInSc1
<g/>
/	/	kIx~
<g/>
Špalek	špalek	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
"	"	kIx"
<g/>
Buldozerem	buldozer	k1gInSc7
<g/>
"	"	kIx"
</s>
<s>
"	"	kIx"
<g/>
Joy	Joy	k1gMnPc5
<g/>
"	"	kIx"
</s>
<s>
(	(	kIx(
<g/>
Váňa	Váňa	k1gMnSc1
<g/>
/	/	kIx~
<g/>
Špalek	špalek	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
"	"	kIx"
<g/>
Kdo	kdo	k3yRnSc1,k3yInSc1,k3yQnSc1
ví	vědět	k5eAaImIp3nS
jestli	jestli	k8xS
<g/>
"	"	kIx"
</s>
<s>
"	"	kIx"
<g/>
Banditi	Banditi	k?
di	di	k?
Praga	Praga	k1gFnSc1
<g/>
"	"	kIx"
</s>
<s>
Banditi	Banditi	k?
di	di	k?
Praga	Praga	k1gFnSc1
</s>
<s>
"	"	kIx"
<g/>
Lady	lady	k1gFnPc2
Gag	gag	k1gInSc1
a	a	k8xC
Rin	Rin	k1gFnSc1
<g/>
"	"	kIx"
</s>
<s>
"	"	kIx"
<g/>
Kávu	káva	k1gFnSc4
si	se	k3xPyFc3
osladil	osladit	k5eAaPmAgMnS
(	(	kIx(
<g/>
pro	pro	k7c4
Frantu	Franta	k1gMnSc4
<g/>
)	)	kIx)
<g/>
"	"	kIx"
</s>
<s>
"	"	kIx"
<g/>
Ebenový	ebenový	k2eAgMnSc1d1
hole	hole	k1gFnPc4
<g/>
"	"	kIx"
</s>
<s>
"	"	kIx"
<g/>
Don	Don	k1gInSc1
Pedro	Pedro	k1gNnSc1
von	von	k1gInSc1
Poltergeist	Poltergeist	k1gMnSc1
<g/>
"	"	kIx"
</s>
<s>
(	(	kIx(
<g/>
Krulich	Krulich	k1gInSc1
<g/>
/	/	kIx~
<g/>
Špalek	špalek	k1gInSc1
<g/>
,	,	kIx,
Kodym	Kodym	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
"	"	kIx"
<g/>
Schody	schod	k1gInPc4
<g/>
"	"	kIx"
</s>
<s>
(	(	kIx(
<g/>
Váňa	Váňa	k1gMnSc1
<g/>
/	/	kIx~
<g/>
Špalek	špalek	k1gInSc1
<g/>
,	,	kIx,
Cimfe	Cimf	k1gMnSc5
<g/>
)	)	kIx)
</s>
<s>
"	"	kIx"
<g/>
Do	do	k7c2
pekla	peklo	k1gNnSc2
<g/>
/	/	kIx~
<g/>
do	do	k7c2
nebe	nebe	k1gNnSc2
<g/>
"	"	kIx"
</s>
<s>
(	(	kIx(
<g/>
Špalek	špalek	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
Do	do	k7c2
pekla	peklo	k1gNnSc2
<g/>
/	/	kIx~
<g/>
do	do	k7c2
nebe	nebe	k1gNnSc2
</s>
<s>
"	"	kIx"
<g/>
Western	Western	kA
Boogie	Boogie	k1gFnSc1
<g/>
"	"	kIx"
</s>
<s>
(	(	kIx(
<g/>
Krulich	Krulich	k1gInSc1
<g/>
/	/	kIx~
<g/>
Špalek	špalek	k1gInSc1
<g/>
,	,	kIx,
Homola	Homola	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
"	"	kIx"
<g/>
Valkýra	valkýra	k1gFnSc1
<g/>
"	"	kIx"
</s>
<s>
(	(	kIx(
<g/>
Váňa	Váňa	k1gMnSc1
<g/>
/	/	kIx~
<g/>
Špalek	špalek	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
"	"	kIx"
<g/>
Myslivecký	myslivecký	k2eAgInSc1d1
ples	ples	k1gInSc1
<g/>
"	"	kIx"
</s>
<s>
(	(	kIx(
<g/>
Špalek	špalek	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
"	"	kIx"
<g/>
Do	do	k7c2
Bolívie	Bolívie	k1gFnSc2
na	na	k7c4
banány	banán	k1gInPc4
<g/>
"	"	kIx"
</s>
<s>
(	(	kIx(
<g/>
Váňa	Váňa	k1gMnSc1
<g/>
/	/	kIx~
<g/>
Špalek	špalek	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
"	"	kIx"
<g/>
Houby	houby	k6eAd1
magický	magický	k2eAgInSc1d1
<g/>
"	"	kIx"
</s>
<s>
"	"	kIx"
<g/>
Brousíme	brousit	k5eAaImIp1nP
nože	nůž	k1gInPc4
<g/>
"	"	kIx"
</s>
<s>
"	"	kIx"
<g/>
Pakliže	pakliže	k8xS
<g/>
"	"	kIx"
</s>
<s>
Uruguay	Uruguay	k1gFnSc1
Cavallery	Cavaller	k1gInPc1
</s>
<s>
Název	název	k1gInSc1
</s>
<s>
Autor	autor	k1gMnSc1
</s>
<s>
Album	album	k1gNnSc1
</s>
<s>
"	"	kIx"
<g/>
Uruguay	Uruguay	k1gFnSc1
Cavallery	Cavaller	k1gInPc1
<g/>
"	"	kIx"
</s>
<s>
(	(	kIx(
<g/>
Krulich	Krulich	k1gInSc1
<g/>
/	/	kIx~
<g/>
Špalek	špalek	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Uruguay	Uruguay	k1gFnSc1
</s>
<s>
Cavallery	Cavaller	k1gInPc1
</s>
<s>
"	"	kIx"
<g/>
Krutej	Krutej	k?
nezájem	nezájem	k1gInSc1
<g/>
"	"	kIx"
</s>
<s>
"	"	kIx"
<g/>
Studená	studený	k2eAgFnSc1d1
<g/>
"	"	kIx"
</s>
<s>
Olympic	Olympic	k1gMnSc1
</s>
<s>
Název	název	k1gInSc1
</s>
<s>
Autor	autor	k1gMnSc1
</s>
<s>
Album	album	k1gNnSc1
</s>
<s>
"	"	kIx"
<g/>
Tak	tak	k9
mi	já	k3xPp1nSc3
to	ten	k3xDgNnSc4
pěkně	pěkně	k6eAd1
začíná	začínat	k5eAaImIp3nS
<g/>
"	"	kIx"
</s>
<s>
(	(	kIx(
<g/>
Janda	Janda	k1gMnSc1
<g/>
/	/	kIx~
<g/>
Špalek	špalek	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Dávno	dávno	k6eAd1
</s>
<s>
"	"	kIx"
<g/>
Sedm	sedm	k4xCc4
statečných	statečný	k2eAgFnPc2d1
<g/>
"	"	kIx"
</s>
<s>
Brejle	brejle	k1gFnPc1
</s>
<s>
"	"	kIx"
<g/>
To	to	k9
já	já	k3xPp1nSc1
si	se	k3xPyFc3
jen	jen	k9
tak	tak	k6eAd1
<g/>
"	"	kIx"
</s>
<s>
Filmografie	filmografie	k1gFnSc1
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
Název	název	k1gInSc1
filmu	film	k1gInSc2
</s>
<s>
Poznámka	poznámka	k1gFnSc1
</s>
<s>
1998	#num#	k4
</s>
<s>
Čert	čert	k1gMnSc1
na	na	k7c6
koze	koza	k1gFnSc6
jel	jet	k5eAaImAgMnS
</s>
<s>
Záznam	záznam	k1gInSc1
koncertu	koncert	k1gInSc2
skupiny	skupina	k1gFnSc2
Kabát	kabát	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
2001	#num#	k4
</s>
<s>
Suma	suma	k1gFnSc1
Sumárum	sumárum	k9
-	-	kIx~
Best	Best	k1gMnSc1
Of	Of	k1gMnSc1
</s>
<s>
Sestřih	sestřih	k1gInSc1
z	z	k7c2
koncertu	koncert	k1gInSc2
a	a	k8xC
videoklipy	videoklip	k1gInPc4
skupiny	skupina	k1gFnSc2
Kabát	kabát	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
2002	#num#	k4
</s>
<s>
Suma	suma	k1gFnSc1
Sumárum	sumárum	k9
</s>
<s>
Záznam	záznam	k1gInSc1
koncertu	koncert	k1gInSc2
skupiny	skupina	k1gFnSc2
Kabát	kabát	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
2004	#num#	k4
</s>
<s>
Kabát	kabát	k1gInSc1
2003-2004	2003-2004	k4
</s>
<s>
Sestřih	sestřih	k1gInSc1
z	z	k7c2
koncertu	koncert	k1gInSc2
a	a	k8xC
záznam	záznam	k1gInSc4
koncertu	koncert	k1gInSc2
skupiny	skupina	k1gFnSc2
Kabát	kabát	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
2007	#num#	k4
</s>
<s>
Eurosong	Eurosong	k1gMnSc1
aneb	aneb	k?
Kabát	Kabát	k1gMnSc1
jde	jít	k5eAaImIp3nS
do	do	k7c2
Evropy	Evropa	k1gFnSc2
</s>
<s>
Dokumentární	dokumentární	k2eAgInSc1d1
film	film	k1gInSc1
České	český	k2eAgFnSc2d1
televize	televize	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
2008	#num#	k4
</s>
<s>
Kabátː	Kabátː	k?
Corrida	Corrida	k1gFnSc1
2007	#num#	k4
</s>
<s>
Záznam	záznam	k1gInSc1
koncertu	koncert	k1gInSc2
skupiny	skupina	k1gFnSc2
Kabát	kabát	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
2009	#num#	k4
</s>
<s>
Po	po	k7c6
čertech	čert	k1gMnPc6
velkej	velkej	k?
koncert	koncert	k1gInSc1
</s>
<s>
Záznam	záznam	k1gInSc1
koncertu	koncert	k1gInSc2
skupiny	skupina	k1gFnSc2
Kabát	kabát	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
2011	#num#	k4
</s>
<s>
Kabátː	Kabátː	k?
Banditi	Banditi	k?
di	di	k?
Praga	Praga	k1gFnSc1
-	-	kIx~
Turné	turné	k1gNnSc1
2011	#num#	k4
</s>
<s>
Záznam	záznam	k1gInSc1
koncertu	koncert	k1gInSc2
skupiny	skupina	k1gFnSc2
Kabát	kabát	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
2016	#num#	k4
</s>
<s>
Kabát	kabát	k1gInSc1
2013-2015	2013-2015	k4
</s>
<s>
Záznam	záznam	k1gInSc1
koncertů	koncert	k1gInPc2
a	a	k8xC
poslední	poslední	k2eAgNnSc4d1
album	album	k1gNnSc4
skupiny	skupina	k1gFnSc2
Kabát	Kabát	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Země	země	k1gFnSc1
revivalů	revival	k1gInPc2
</s>
<s>
Dokumentární	dokumentární	k2eAgInSc1d1
film	film	k1gInSc1
České	český	k2eAgFnSc2d1
televize	televize	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
2019	#num#	k4
</s>
<s>
Kabátː	Kabátː	k?
Noc	noc	k1gFnSc1
v	v	k7c6
Edenu	Eden	k1gInSc6
</s>
<s>
Sestřih	sestřih	k1gInSc1
koncertu	koncert	k1gInSc2
skupiny	skupina	k1gFnSc2
Kabát	kabát	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Baskytary	baskytara	k1gFnPc1
</s>
<s>
Hraje	hrát	k5eAaImIp3nS
na	na	k7c4
Fender	Fender	k1gInSc4
Precision	Precision	k1gInSc4
Bass	Bass	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
počátcích	počátek	k1gInPc6
hrál	hrát	k5eAaImAgInS
(	(	kIx(
<g/>
v	v	k7c6
době	doba	k1gFnSc6
neexistence	neexistence	k1gFnSc2
skupiny	skupina	k1gFnSc2
Kabát	kabát	k1gInSc1
<g/>
)	)	kIx)
na	na	k7c4
českou	český	k2eAgFnSc4d1
značku	značka	k1gFnSc4
Jolana	Jolana	k1gFnSc1
Iris	iris	k1gFnPc2
Bass	Bass	k1gMnSc1
Když	když	k8xS
skupina	skupina	k1gFnSc1
Kabát	kabát	k1gInSc4
měla	mít	k5eAaImAgFnS
vystupovat	vystupovat	k5eAaImF
v	v	k7c6
Londýně	Londýn	k1gInSc6
a	a	k8xC
Dublinu	Dublin	k1gInSc6
v	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
<g/>
,	,	kIx,
tak	tak	k6eAd1
si	se	k3xPyFc3
svoji	svůj	k3xOyFgFnSc4
baskytaru	baskytara	k1gFnSc4
Milan	Milan	k1gMnSc1
Špalek	špalek	k1gInSc4
zapomněl	zapomenout	k5eAaPmAgMnS,k5eAaImAgMnS
doma	doma	k6eAd1
a	a	k8xC
nic	nic	k3yNnSc1
jiného	jiný	k2eAgNnSc2d1
mu	on	k3xPp3gNnSc3
nezbývalo	zbývat	k5eNaImAgNnS
než	než	k8xS
ji	on	k3xPp3gFnSc4
zakoupit	zakoupit	k5eAaPmF
přímo	přímo	k6eAd1
v	v	k7c6
jednom	jeden	k4xCgInSc6
londýnském	londýnský	k2eAgInSc6d1
krámku	krámek	k1gInSc6
s	s	k7c7
hudebninami	hudebnina	k1gFnPc7
<g/>
,	,	kIx,
kde	kde	k6eAd1
si	se	k3xPyFc3
zakoupil	zakoupit	k5eAaPmAgInS
svůj	svůj	k3xOyFgInSc4
model	model	k1gInSc4
(	(	kIx(
<g/>
konkrétně	konkrétně	k6eAd1
Series	Series	k1gInSc1
P-Bass	P-Bassa	k1gFnPc2
PF	PF	kA
3	#num#	k4
<g/>
TS	ts	k0
<g/>
)	)	kIx)
za	za	k7c4
drahé	drahý	k2eAgInPc4d1
peníze	peníz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Fender	Fender	k1gInSc1
Precision	Precision	k1gInSc1
Bass	Bass	k1gMnSc1
-	-	kIx~
P-Bass	P-Bass	k1gInSc1
PF	PF	kA
3TS	3TS	k4
</s>
<s>
Rickenbacker	Rickenbacker	k1gInSc1
4003	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Fender	Fender	k1gInSc1
Precision	Precision	k1gInSc1
Bass	Bass	k1gMnSc1
</s>
<s>
Aparatura	aparatura	k1gFnSc1
</s>
<s>
Používá	používat	k5eAaImIp3nS
zesilovač	zesilovač	k1gInSc4
firmy	firma	k1gFnSc2
Mesa	Mesa	k1gFnSc1
<g/>
/	/	kIx~
<g/>
Boogie	Boogie	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
spojuje	spojovat	k5eAaImIp3nS
s	s	k7c7
lampovým	lampový	k2eAgInSc7d1
ampem	amp	k1gInSc7
firmy	firma	k1gFnSc2
Ampeg	Ampeg	k1gInSc1
<g/>
,	,	kIx,
protože	protože	k8xS
mu	on	k3xPp3gMnSc3
Mesa	Mesa	k1gFnSc1
<g/>
/	/	kIx~
<g/>
Boogie	Boogie	k1gFnSc1
zní	znět	k5eAaImIp3nS
lépe	dobře	k6eAd2
<g/>
.	.	kIx.
</s>
<s>
Logo	logo	k1gNnSc1
firmy	firma	k1gFnSc2
Mesa	Mesum	k1gNnSc2
<g/>
/	/	kIx~
<g/>
Boogie	Boogie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Tiskové	tiskový	k2eAgInPc1d1
rozhovory	rozhovor	k1gInPc1
</s>
<s>
https://www.idnes.cz/kultura/hudba/milan-spalek-kabat-cena-osa.A190626_172901_hudba_vha	https://www.idnes.cz/kultura/hudba/milan-spalek-kabat-cena-osa.A190626_172901_hudba_vha	k1gFnSc1
</s>
<s>
https://www.denik.cz/hudba/milan-spalek-hrat-stary-pecky-uz-je-dnes-drina-20171026.html	https://www.denik.cz/hudba/milan-spalek-hrat-stary-pecky-uz-je-dnes-drina-20171026.html	k1gMnSc1
</s>
<s>
https://frontman.cz/milan-spalek-v-kabatu-nejsou-individualisti-jsme-parta	https://frontman.cz/milan-spalek-v-kabatu-nejsou-individualisti-jsme-parta	k1gFnSc1
</s>
<s>
Videa	video	k1gNnPc1
</s>
<s>
https://www.youtube.com/watch?v=H2WObviwz5E	https://www.youtube.com/watch?v=H2WObviwz5E	k4
</s>
<s>
https://www.youtube.com/watch?v=vvIYQf-T0F4	https://www.youtube.com/watch?v=vvIYQf-T0F4	k4
</s>
<s>
https://www.youtube.com/watch?v=cALydzpok2c	https://www.youtube.com/watch?v=cALydzpok2c	k6eAd1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Stránky	stránka	k1gFnSc2
skupiny	skupina	k1gFnSc2
Kabát	Kabát	k1gMnSc1
<g/>
↑	↑	k?
Milan	Milan	k1gMnSc1
Špalek	špalek	k1gInSc4
má	mít	k5eAaImIp3nS
v	v	k7c6
Zámecké	zámecký	k2eAgFnSc6d1
zahradě	zahrada	k1gFnSc6
sochu	socha	k1gFnSc4
<g/>
,	,	kIx,
teplicky	teplicky	k6eAd1
<g/>
.	.	kIx.
<g/>
denik	denik	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Milan	Milan	k1gMnSc1
Špalek	špalek	k1gInSc1
</s>
<s>
Milan	Milan	k1gMnSc1
Špalek	špalek	k1gInSc4
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Kabát	Kabát	k1gMnSc1
Pepa	Pepa	k1gMnSc1
Vojtek	Vojtek	k1gMnSc1
•	•	k?
Tomáš	Tomáš	k1gMnSc1
Krulich	Krulich	k1gMnSc1
•	•	k?
Milan	Milan	k1gMnSc1
Špalek	špalek	k1gInSc1
•	•	k?
Radek	Radek	k1gMnSc1
„	„	k?
<g/>
Hurvajs	Hurvajs	k1gInSc1
<g/>
“	“	k?
Hurčík	Hurčík	k1gMnSc1
•	•	k?
Ota	Ota	k1gMnSc1
Váňa	Váňa	k1gMnSc1
Studiová	studiový	k2eAgNnPc1d1
alba	album	k1gNnPc4
</s>
<s>
Má	mít	k5eAaImIp3nS
ji	on	k3xPp3gFnSc4
motorovou	motorový	k2eAgFnSc4d1
•	•	k?
Děvky	děvka	k1gFnSc2
ty	ty	k3xPp2nSc5
to	ten	k3xDgNnSc4
znaj	znaj	k?
•	•	k?
Colorado	Colorado	k1gNnSc4
•	•	k?
Země	země	k1gFnSc1
plná	plný	k2eAgFnSc1d1
trpaslíků	trpaslík	k1gMnPc2
•	•	k?
Čert	čert	k1gMnSc1
na	na	k7c6
koze	koza	k1gFnSc6
jel	jet	k5eAaImAgInS
•	•	k?
MegaHu	MegaHus	k1gInSc2
•	•	k?
Go	Go	k1gMnSc5
satane	satan	k1gMnSc5
go	go	k?
•	•	k?
Dole	dole	k6eAd1
v	v	k7c6
dole	dol	k1gInSc6
•	•	k?
Corrida	Corrid	k1gMnSc2
•	•	k?
Banditi	Banditi	k?
di	di	k?
Praga	Prag	k1gMnSc2
•	•	k?
Do	do	k7c2
pekla	peklo	k1gNnSc2
<g/>
/	/	kIx~
<g/>
do	do	k7c2
nebe	nebe	k1gNnSc2
Koncertní	koncertní	k2eAgFnSc1d1
alba	alba	k1gFnSc1
</s>
<s>
Živě	živě	k6eAd1
<g/>
!	!	kIx.
</s>
<s desamb="1">
•	•	k?
Suma	suma	k1gFnSc1
sumárum	sumárum	k9
•	•	k?
Po	po	k7c6
čertech	čert	k1gMnPc6
velkej	velkej	k?
koncert	koncert	k1gInSc1
•	•	k?
Banditi	Banditi	k?
di	di	k?
Praga	Praga	k1gFnSc1
Turné	turné	k1gNnSc2
2011	#num#	k4
Kompilace	kompilace	k1gFnSc2
a	a	k8xC
box	box	k1gInSc4
sety	set	k1gInPc4
</s>
<s>
Suma	suma	k1gFnSc1
sumárum	sumárum	k9
•	•	k?
Box	box	k1gInSc1
2007	#num#	k4
•	•	k?
Kabát	kabát	k1gInSc1
2013	#num#	k4
<g/>
–	–	k?
<g/>
2015	#num#	k4
Videozáznamy	videozáznam	k1gInPc7
</s>
<s>
Suma	suma	k1gFnSc1
sumárum	sumárum	k9
–	–	k?
Best	Besta	k1gFnPc2
of	of	k?
video	video	k1gNnSc4
•	•	k?
Kabát	kabát	k1gInSc1
2003	#num#	k4
<g/>
–	–	k?
<g/>
2004	#num#	k4
•	•	k?
Kabát	kabát	k1gInSc1
Corrida	Corrida	k1gFnSc1
2007	#num#	k4
•	•	k?
Po	po	k7c6
čertech	čert	k1gMnPc6
velkej	velkej	k?
koncert	koncert	k1gInSc1
•	•	k?
Banditi	Banditi	k?
di	di	k?
Praga	Praga	k1gFnSc1
Turné	turné	k1gNnSc2
2011	#num#	k4
Příbuzné	příbuzná	k1gFnSc2
</s>
<s>
Seznam	seznam	k1gInSc1
písní	píseň	k1gFnPc2
skupiny	skupina	k1gFnSc2
Kabát	Kabát	k1gMnSc1
•	•	k?
Seznam	seznam	k1gInSc1
koncertních	koncertní	k2eAgInPc2d1
turné	turné	k1gNnSc1
skupiny	skupina	k1gFnPc1
Kabát	Kabát	k1gMnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
xx	xx	k?
<g/>
0	#num#	k4
<g/>
133681	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
1835	#num#	k4
3216	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
169423703	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Hudba	hudba	k1gFnSc1
</s>
