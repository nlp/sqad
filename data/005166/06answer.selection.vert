<s>
První	první	k4xOgFnSc7	první
sondou	sonda	k1gFnSc7	sonda
u	u	k7c2	u
Merkuru	Merkur	k1gInSc2	Merkur
byla	být	k5eAaImAgFnS	být
americká	americký	k2eAgFnSc1d1	americká
sonda	sonda	k1gFnSc1	sonda
Mariner	Mariner	k1gInSc1	Mariner
10	[number]	k4	10
v	v	k7c4	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
nasnímala	nasnímat	k5eAaPmAgNnP	nasnímat
přibližně	přibližně	k6eAd1	přibližně
45	[number]	k4	45
%	%	kIx~	%
povrchu	povrch	k1gInSc3	povrch
<g/>
.	.	kIx.	.
</s>
