<s>
Čeština	čeština	k1gFnSc1	čeština
je	být	k5eAaImIp3nS	být
též	též	k9	též
(	(	kIx(	(
<g/>
od	od	k7c2	od
května	květen	k1gInSc2	květen
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
24	[number]	k4	24
(	(	kIx(	(
<g/>
stav	stav	k1gInSc4	stav
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
oficiálních	oficiální	k2eAgInPc2d1	oficiální
(	(	kIx(	(
<g/>
úředních	úřední	k2eAgInPc2d1	úřední
<g/>
)	)	kIx)	)
jazyků	jazyk	k1gInPc2	jazyk
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
