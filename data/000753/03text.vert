<s>
Petr	Petr	k1gMnSc1	Petr
z	z	k7c2	z
Ailly	Ailla	k1gFnSc2	Ailla
či	či	k8xC	či
Pierre	Pierr	k1gInSc5	Pierr
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Ailly	Ailla	k1gMnSc2	Ailla
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
Petrus	Petrus	k1gInSc1	Petrus
Aliacensis	Aliacensis	k1gFnSc2	Aliacensis
nebo	nebo	k8xC	nebo
Petrus	Petrus	k1gMnSc1	Petrus
de	de	k?	de
Alliaco	Alliaco	k1gMnSc1	Alliaco
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1351	[number]	k4	1351
-	-	kIx~	-
9	[number]	k4	9
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1420	[number]	k4	1420
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
teolog	teolog	k1gMnSc1	teolog
<g/>
,	,	kIx,	,
filosof	filosof	k1gMnSc1	filosof
<g/>
,	,	kIx,	,
logik	logik	k1gMnSc1	logik
<g/>
,	,	kIx,	,
astrolog	astrolog	k1gMnSc1	astrolog
a	a	k8xC	a
kardinál	kardinál	k1gMnSc1	kardinál
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Compiè	Compiè	k1gFnSc6	Compiè
<g/>
,	,	kIx,	,
studoval	studovat	k5eAaImAgMnS	studovat
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1385	[number]	k4	1385
až	až	k9	až
1395	[number]	k4	1395
byl	být	k5eAaImAgMnS	být
kancléřem	kancléř	k1gMnSc7	kancléř
zdejší	zdejší	k2eAgFnSc2d1	zdejší
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
biskupem	biskup	k1gInSc7	biskup
v	v	k7c6	v
Cambrai	Cambra	k1gInSc6	Cambra
a	a	k8xC	a
kardinálem	kardinál	k1gMnSc7	kardinál
<g/>
.	.	kIx.	.
</s>
<s>
Účastnil	účastnit	k5eAaImAgInS	účastnit
se	se	k3xPyFc4	se
koncilů	koncil	k1gInPc2	koncil
v	v	k7c6	v
Pise	Pisa	k1gFnSc6	Pisa
a	a	k8xC	a
Kostnici	Kostnice	k1gFnSc6	Kostnice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c6	na
procesu	proces	k1gInSc6	proces
s	s	k7c7	s
Janem	Jan	k1gMnSc7	Jan
Husem	Hus	k1gMnSc7	Hus
<g/>
.	.	kIx.	.
</s>
<s>
Psal	psát	k5eAaImAgMnS	psát
texty	text	k1gInPc7	text
jak	jak	k8xS	jak
na	na	k7c4	na
církevní	církevní	k2eAgNnPc4d1	církevní
témata	téma	k1gNnPc4	téma
(	(	kIx(	(
<g/>
papežské	papežský	k2eAgNnSc4d1	papežské
schizma	schizma	k1gNnSc4	schizma
<g/>
,	,	kIx,	,
reforma	reforma	k1gFnSc1	reforma
církve	církev	k1gFnSc2	církev
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
práce	práce	k1gFnSc2	práce
filosofické	filosofický	k2eAgFnSc2d1	filosofická
(	(	kIx(	(
<g/>
věnované	věnovaný	k2eAgFnSc2d1	věnovaná
např.	např.	kA	např.
logice	logika	k1gFnSc6	logika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
či	či	k8xC	či
z	z	k7c2	z
dalších	další	k2eAgFnPc2d1	další
disciplín	disciplína	k1gFnPc2	disciplína
(	(	kIx(	(
<g/>
kosmologie	kosmologie	k1gFnSc1	kosmologie
<g/>
,	,	kIx,	,
komputistika	komputistika	k1gFnSc1	komputistika
<g/>
,	,	kIx,	,
astrologie	astrologie	k1gFnSc1	astrologie
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
kosmografická	kosmografický	k2eAgFnSc1d1	kosmografický
práce	práce	k1gFnSc1	práce
Imago	Imago	k6eAd1	Imago
Mundi	Mund	k1gMnPc1	Mund
sloužila	sloužit	k5eAaImAgFnS	sloužit
Kolumbovi	Kolumbus	k1gMnSc6	Kolumbus
jako	jako	k8xS	jako
podklad	podklad	k1gInSc4	podklad
při	při	k7c6	při
odhadech	odhad	k1gInPc6	odhad
velikosti	velikost	k1gFnSc2	velikost
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Petrovi	Petr	k1gMnSc6	Petr
z	z	k7c2	z
Ailly	Ailla	k1gFnSc2	Ailla
je	být	k5eAaImIp3nS	být
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
měsíční	měsíční	k2eAgInSc1d1	měsíční
kráter	kráter	k1gInSc1	kráter
Aliacensis	Aliacensis	k1gFnSc2	Aliacensis
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
logice	logika	k1gFnSc6	logika
rozpracoval	rozpracovat	k5eAaPmAgMnS	rozpracovat
Ockhamovu	Ockhamův	k2eAgFnSc4d1	Ockhamova
teorii	teorie	k1gFnSc4	teorie
mentálního	mentální	k2eAgInSc2d1	mentální
jazyka	jazyk	k1gInSc2	jazyk
(	(	kIx(	(
<g/>
spis	spis	k1gInSc1	spis
Conceptus	Conceptus	k1gInSc1	Conceptus
<g/>
)	)	kIx)	)
a	a	k8xC	a
využil	využít	k5eAaPmAgMnS	využít
ji	on	k3xPp3gFnSc4	on
k	k	k7c3	k
řešení	řešení	k1gNnSc3	řešení
insolubilií	insolubilie	k1gFnPc2	insolubilie
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
autoreferenčních	autoreferenční	k2eAgInPc2d1	autoreferenční
sémantických	sémantický	k2eAgInPc2d1	sémantický
paradoxů	paradox	k1gInPc2	paradox
(	(	kIx(	(
<g/>
spis	spis	k1gInSc1	spis
Insolubilia	Insolubilium	k1gNnSc2	Insolubilium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgInS	proslavit
útokem	útok	k1gInSc7	útok
na	na	k7c4	na
modistickou	modistický	k2eAgFnSc4d1	modistický
sémantiku	sémantika	k1gFnSc4	sémantika
vedeným	vedený	k2eAgInSc7d1	vedený
z	z	k7c2	z
pozic	pozice	k1gFnPc2	pozice
terministické	terministický	k2eAgFnSc2d1	terministický
logiky	logika	k1gFnSc2	logika
(	(	kIx(	(
<g/>
spis	spis	k1gInSc1	spis
Destructiones	Destructiones	k1gInSc1	Destructiones
modorum	modorum	k1gInSc4	modorum
significandi	significand	k1gMnPc1	significand
-	-	kIx~	-
možná	možná	k9	možná
je	být	k5eAaImIp3nS	být
mu	on	k3xPp3gMnSc3	on
připisován	připisovat	k5eAaImNgInS	připisovat
mylně	mylně	k6eAd1	mylně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Středověká	středověký	k2eAgFnSc1d1	středověká
logika	logika	k1gFnSc1	logika
Vilém	Vilém	k1gMnSc1	Vilém
Ockham	Ockham	k1gInSc1	Ockham
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
Louis	Louis	k1gMnSc1	Louis
B.	B.	kA	B.
Pascoe	Pascoe	k1gInSc1	Pascoe
<g/>
,	,	kIx,	,
Church	Church	k1gInSc1	Church
and	and	k?	and
Reform	Reform	k1gInSc1	Reform
<g/>
:	:	kIx,	:
bishops	bishops	k1gInSc1	bishops
<g/>
,	,	kIx,	,
theologians	theologians	k1gInSc1	theologians
<g/>
,	,	kIx,	,
and	and	k?	and
canon	canon	k1gInSc1	canon
lawyers	lawyers	k1gInSc1	lawyers
in	in	k?	in
the	the	k?	the
thought	thought	k1gInSc1	thought
of	of	k?	of
Pierre	Pierr	k1gInSc5	Pierr
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Ailly	Ailla	k1gMnSc2	Ailla
(	(	kIx(	(
<g/>
1351	[number]	k4	1351
<g/>
-	-	kIx~	-
<g/>
1420	[number]	k4	1420
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Leiden	Leidna	k1gFnPc2	Leidna
<g/>
:	:	kIx,	:
Brill	Brill	k1gMnSc1	Brill
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Laura	Laura	k1gFnSc1	Laura
A.	A.	kA	A.
Smoller	Smoller	k1gInSc1	Smoller
<g/>
,	,	kIx,	,
History	Histor	k1gInPc1	Histor
<g/>
,	,	kIx,	,
Prophecy	Prophecy	k1gInPc1	Prophecy
<g/>
,	,	kIx,	,
and	and	k?	and
the	the	k?	the
Stars	Stars	k1gInSc1	Stars
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Christian	Christian	k1gMnSc1	Christian
Astrology	astrolog	k1gMnPc4	astrolog
of	of	k?	of
Pierre	Pierr	k1gInSc5	Pierr
D	D	kA	D
<g/>
'	'	kIx"	'
<g/>
Ailly	Ailla	k1gMnSc2	Ailla
<g/>
,	,	kIx,	,
1350	[number]	k4	1350
<g/>
-	-	kIx~	-
<g/>
1420	[number]	k4	1420
<g/>
.	.	kIx.	.
</s>
<s>
Princeton	Princeton	k1gInSc1	Princeton
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
(	(	kIx(	(
<g/>
Princeton	Princeton	k1gInSc1	Princeton
<g/>
,	,	kIx,	,
NJ	NJ	kA	NJ
<g/>
;	;	kIx,	;
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
691	[number]	k4	691
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8788	[number]	k4	8788
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Alan	Alan	k1gMnSc1	Alan
E.	E.	kA	E.
Bernstein	Bernstein	k1gMnSc1	Bernstein
<g/>
,	,	kIx,	,
Pierre	Pierr	k1gInSc5	Pierr
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Ailly	Ailla	k1gMnSc2	Ailla
and	and	k?	and
the	the	k?	the
Blanchard	Blanchard	k1gInSc1	Blanchard
affair	affair	k1gInSc1	affair
:	:	kIx,	:
University	universita	k1gFnPc1	universita
and	and	k?	and
Chancellor	Chancellor	k1gMnSc1	Chancellor
of	of	k?	of
Paris	Paris	k1gMnSc1	Paris
at	at	k?	at
the	the	k?	the
beginning	beginning	k1gInSc1	beginning
of	of	k?	of
the	the	k?	the
Great	Great	k2eAgInSc4d1	Great
Schism	Schism	k1gInSc4	Schism
<g/>
.	.	kIx.	.
</s>
<s>
Leiden	Leidna	k1gFnPc2	Leidna
<g/>
:	:	kIx,	:
Brill	Brill	k1gMnSc1	Brill
<g/>
,	,	kIx,	,
1978	[number]	k4	1978
<g/>
.	.	kIx.	.
</s>
<s>
KREN	KREN	kA	KREN
<g/>
,	,	kIx,	,
Claudia	Claudia	k1gFnSc1	Claudia
<g/>
.	.	kIx.	.
</s>
<s>
Ailly	Ailla	k1gFnPc1	Ailla
<g/>
,	,	kIx,	,
Pierre	Pierr	k1gInSc5	Pierr
D	D	kA	D
<g/>
'	'	kIx"	'
in	in	k?	in
Dictionary	Dictionara	k1gFnSc2	Dictionara
of	of	k?	of
Scientific	Scientific	k1gMnSc1	Scientific
Biography	Biographa	k1gFnSc2	Biographa
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
:	:	kIx,	:
Charles	Charles	k1gMnSc1	Charles
Scribner	Scribner	k1gMnSc1	Scribner
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Sons	Sons	k1gInSc1	Sons
<g/>
,	,	kIx,	,
1970	[number]	k4	1970
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
684101149	[number]	k4	684101149
<g/>
.	.	kIx.	.
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Ailly	Ailla	k1gMnSc2	Ailla
ve	v	k7c6	v
Vlastenském	vlastenský	k2eAgInSc6d1	vlastenský
slovníku	slovník	k1gInSc6	slovník
historickém	historický	k2eAgInSc6d1	historický
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
