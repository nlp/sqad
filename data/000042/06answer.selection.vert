<s>
Romové	Rom	k1gMnPc1	Rom
slaví	slavit	k5eAaImIp3nP	slavit
Mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
den	den	k1gInSc4	den
Romů	Rom	k1gMnPc2	Rom
každoročně	každoročně	k6eAd1	každoročně
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
ustanoven	ustanovit	k5eAaPmNgInS	ustanovit
na	na	k7c6	na
4	[number]	k4	4
<g/>
.	.	kIx.	.
kongresu	kongres	k1gInSc2	kongres
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
romské	romský	k2eAgFnSc2d1	romská
unie	unie	k1gFnSc2	unie
ve	v	k7c6	v
Varšavě	Varšava	k1gFnSc6	Varšava
<g/>
.	.	kIx.	.
</s>
