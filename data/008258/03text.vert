<p>
<s>
Robert	Robert	k1gMnSc1	Robert
King	King	k1gMnSc1	King
Merton	Merton	k1gInSc1	Merton
(	(	kIx(	(
<g/>
původním	původní	k2eAgNnSc7d1	původní
jménem	jméno	k1gNnSc7	jméno
Meyer	Meyer	k1gMnSc1	Meyer
Robert	Robert	k1gMnSc1	Robert
Schkolnick	Schkolnick	k1gMnSc1	Schkolnick
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
židovských	židovský	k2eAgMnPc2d1	židovský
uprchlíků	uprchlík	k1gMnPc2	uprchlík
z	z	k7c2	z
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
;	;	kIx,	;
4	[number]	k4	4
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1910	[number]	k4	1910
<g/>
,	,	kIx,	,
Filadelfie	Filadelfie	k1gFnSc1	Filadelfie
<g/>
,	,	kIx,	,
Pensylvánie	Pensylvánie	k1gFnSc1	Pensylvánie
–	–	k?	–
23	[number]	k4	23
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
sociolog	sociolog	k1gMnSc1	sociolog
<g/>
,	,	kIx,	,
představitel	představitel	k1gMnSc1	představitel
sociologického	sociologický	k2eAgInSc2d1	sociologický
funkcionalismu	funkcionalismus	k1gInSc2	funkcionalismus
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
Columbia	Columbia	k1gFnSc1	Columbia
University	universita	k1gFnSc2	universita
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
považovaný	považovaný	k2eAgInSc1d1	považovaný
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
největších	veliký	k2eAgMnPc2d3	veliký
sociologů	sociolog	k1gMnPc2	sociolog
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
a	a	k8xC	a
otce	otec	k1gMnSc2	otec
sociologie	sociologie	k1gFnSc2	sociologie
vědy	věda	k1gFnSc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
Zavedl	zavést	k5eAaPmAgInS	zavést
do	do	k7c2	do
sociologie	sociologie	k1gFnSc2	sociologie
pojem	pojem	k1gInSc4	pojem
sociologické	sociologický	k2eAgNnSc1d1	sociologické
paradigma	paradigma	k1gNnSc1	paradigma
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
vydal	vydat	k5eAaPmAgMnS	vydat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
20	[number]	k4	20
knih	kniha	k1gFnPc2	kniha
a	a	k8xC	a
napsal	napsat	k5eAaBmAgInS	napsat
přes	přes	k7c4	přes
300	[number]	k4	300
článků	článek	k1gInPc2	článek
a	a	k8xC	a
recenzí	recenze	k1gFnPc2	recenze
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
obdržel	obdržet	k5eAaPmAgMnS	obdržet
od	od	k7c2	od
prezidenta	prezident	k1gMnSc2	prezident
USA	USA	kA	USA
Billa	Bill	k1gMnSc2	Bill
Clintona	Clinton	k1gMnSc2	Clinton
Národní	národní	k2eAgNnSc1d1	národní
vyznamenání	vyznamenání	k1gNnSc1	vyznamenání
za	za	k7c4	za
vědu	věda	k1gFnSc4	věda
za	za	k7c4	za
"	"	kIx"	"
<g/>
položení	položení	k1gNnSc4	položení
základů	základ	k1gInPc2	základ
sociologie	sociologie	k1gFnSc2	sociologie
vědy	věda	k1gFnSc2	věda
a	a	k8xC	a
průkopnický	průkopnický	k2eAgInSc4d1	průkopnický
přínos	přínos	k1gInSc4	přínos
ke	k	k7c3	k
studiu	studio	k1gNnSc3	studio
společenského	společenský	k2eAgInSc2d1	společenský
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pak	pak	k6eAd1	pak
sebenaplňující	sebenaplňující	k2eAgFnPc1d1	sebenaplňující
předpovědi	předpověď	k1gFnPc1	předpověď
a	a	k8xC	a
nezamýšlených	zamýšlený	k2eNgInPc2d1	nezamýšlený
důsledků	důsledek	k1gInPc2	důsledek
společenského	společenský	k2eAgNnSc2d1	společenské
jednání	jednání	k1gNnSc2	jednání
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Merton	Merton	k1gInSc1	Merton
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgInS	narodit
jako	jako	k9	jako
mladší	mladý	k2eAgMnSc1d2	mladší
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
dětství	dětství	k1gNnSc2	dětství
se	se	k3xPyFc4	se
zajímal	zajímat	k5eAaImAgInS	zajímat
o	o	k7c4	o
vědu	věda	k1gFnSc4	věda
<g/>
,	,	kIx,	,
historii	historie	k1gFnSc4	historie
a	a	k8xC	a
literaturu	literatura	k1gFnSc4	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
14	[number]	k4	14
letech	léto	k1gNnPc6	léto
si	se	k3xPyFc3	se
změnil	změnit	k5eAaPmAgMnS	změnit
jméno	jméno	k1gNnSc4	jméno
na	na	k7c4	na
Roberta	Robert	k1gMnSc4	Robert
Kinga	King	k1gMnSc4	King
Mertona	Merton	k1gMnSc4	Merton
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
domovské	domovský	k2eAgFnSc6d1	domovská
Filadelfii	Filadelfie	k1gFnSc6	Filadelfie
studoval	studovat	k5eAaImAgMnS	studovat
střední	střední	k2eAgFnSc4d1	střední
školu	škola	k1gFnSc4	škola
a	a	k8xC	a
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
také	také	k9	také
další	další	k2eAgFnPc4d1	další
instituce	instituce	k1gFnPc4	instituce
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
mu	on	k3xPp3gMnSc3	on
pomohly	pomoct	k5eAaPmAgFnP	pomoct
formovat	formovat	k5eAaImF	formovat
jeho	jeho	k3xOp3gNnSc3	jeho
myšlení	myšlení	k1gNnSc3	myšlení
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
Carnegieho	Carnegie	k1gMnSc4	Carnegie
knihovnu	knihovna	k1gFnSc4	knihovna
<g/>
,	,	kIx,	,
Hudební	hudební	k2eAgFnSc4d1	hudební
akademii	akademie	k1gFnSc4	akademie
nebo	nebo	k8xC	nebo
Muzeum	muzeum	k1gNnSc4	muzeum
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
<g/>
Svá	svůj	k3xOyFgNnPc4	svůj
vysokoškolská	vysokoškolský	k2eAgNnPc4d1	vysokoškolské
studia	studio	k1gNnPc4	studio
započal	započnout	k5eAaPmAgInS	započnout
na	na	k7c4	na
Temple	templ	k1gInSc5	templ
University	universita	k1gFnSc2	universita
ve	v	k7c6	v
Filadelfii	Filadelfie	k1gFnSc6	Filadelfie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
asistent	asistent	k1gMnSc1	asistent
sociologa	sociolog	k1gMnSc2	sociolog
George	Georg	k1gMnSc2	Georg
E.	E.	kA	E.
Simpsona	Simpson	k1gMnSc2	Simpson
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
získal	získat	k5eAaPmAgInS	získat
bakalářský	bakalářský	k2eAgInSc1d1	bakalářský
titul	titul	k1gInSc1	titul
z	z	k7c2	z
filosofie	filosofie	k1gFnSc2	filosofie
a	a	k8xC	a
sociologie	sociologie	k1gFnSc2	sociologie
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
pokračování	pokračování	k1gNnSc4	pokračování
svého	svůj	k3xOyFgNnSc2	svůj
studia	studio	k1gNnSc2	studio
si	se	k3xPyFc3	se
vybral	vybrat	k5eAaPmAgMnS	vybrat
Harvardovu	Harvardův	k2eAgFnSc4d1	Harvardova
univerzitu	univerzita	k1gFnSc4	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
kroku	krok	k1gInSc3	krok
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
po	po	k7c6	po
setkání	setkání	k1gNnSc6	setkání
se	s	k7c7	s
sociologem	sociolog	k1gMnSc7	sociolog
<g/>
,	,	kIx,	,
Pitirimem	Pitirim	k1gMnSc7	Pitirim
Alexandrovičem	Alexandrovič	k1gMnSc7	Alexandrovič
Sorokinem	Sorokin	k1gMnSc7	Sorokin
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
vedoucí	vedoucí	k1gMnSc1	vedoucí
katedry	katedra	k1gFnSc2	katedra
sociologie	sociologie	k1gFnSc1	sociologie
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1931	[number]	k4	1931
až	až	k9	až
1937	[number]	k4	1937
byl	být	k5eAaImAgInS	být
žákem	žák	k1gMnSc7	žák
T.	T.	kA	T.
Parsonse	Parsons	k1gInSc6	Parsons
<g/>
.	.	kIx.	.
</s>
<s>
Svá	svůj	k3xOyFgNnPc4	svůj
studia	studio	k1gNnPc4	studio
úspěšně	úspěšně	k6eAd1	úspěšně
zakončil	zakončit	k5eAaPmAgMnS	zakončit
získáním	získání	k1gNnSc7	získání
doktorátu	doktorát	k1gInSc2	doktorát
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
vystudování	vystudování	k1gNnSc6	vystudování
se	se	k3xPyFc4	se
Merton	Merton	k1gInSc1	Merton
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
pro	pro	k7c4	pro
akademickou	akademický	k2eAgFnSc4d1	akademická
dráhu	dráha	k1gFnSc4	dráha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
začal	začít	k5eAaPmAgMnS	začít
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c4	na
Tulane	Tulan	k1gMnSc5	Tulan
University	universita	k1gFnSc2	universita
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
odešel	odejít	k5eAaPmAgMnS	odejít
na	na	k7c4	na
Kolumbijskou	kolumbijský	k2eAgFnSc4d1	kolumbijská
univerzitu	univerzita	k1gFnSc4	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
působil	působit	k5eAaImAgMnS	působit
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1979	[number]	k4	1979
<g/>
.	.	kIx.	.
</s>
<s>
Učit	učit	k5eAaImF	učit
přestal	přestat	k5eAaPmAgInS	přestat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
23	[number]	k4	23
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2003	[number]	k4	2003
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Robert	Robert	k1gMnSc1	Robert
King	King	k1gMnSc1	King
Merton	Merton	k1gInSc4	Merton
se	se	k3xPyFc4	se
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
dvakrát	dvakrát	k6eAd1	dvakrát
oženil	oženit	k5eAaPmAgInS	oženit
a	a	k8xC	a
se	s	k7c7	s
svojí	svůj	k3xOyFgFnSc7	svůj
první	první	k4xOgFnSc7	první
manželkou	manželka	k1gFnSc7	manželka
měl	mít	k5eAaImAgMnS	mít
tři	tři	k4xCgFnPc4	tři
děti	dítě	k1gFnPc4	dítě
–	–	k?	–
syna	syn	k1gMnSc4	syn
a	a	k8xC	a
2	[number]	k4	2
dcery	dcera	k1gFnPc4	dcera
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
druhou	druhý	k4xOgFnSc7	druhý
manželkou	manželka	k1gFnSc7	manželka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ho	on	k3xPp3gMnSc4	on
přežila	přežít	k5eAaPmAgFnS	přežít
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
socioložka	socioložka	k1gFnSc1	socioložka
a	a	k8xC	a
Mertonova	Mertonův	k2eAgFnSc1d1	Mertonova
spolupracovnice	spolupracovnice	k1gFnSc1	spolupracovnice
Harriet	Harriet	k1gInSc1	Harriet
Zuckermanová	Zuckermanová	k1gFnSc1	Zuckermanová
<g/>
.	.	kIx.	.
<g/>
Jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
–	–	k?	–
Robert	Robert	k1gMnSc1	Robert
C.	C.	kA	C.
Merton	Merton	k1gInSc1	Merton
je	být	k5eAaImIp3nS	být
známým	známý	k2eAgMnSc7d1	známý
ekonomem	ekonom	k1gMnSc7	ekonom
–	–	k?	–
laureátem	laureát	k1gMnSc7	laureát
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
ekonomii	ekonomie	k1gFnSc4	ekonomie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Mertonův	Mertonův	k2eAgInSc1d1	Mertonův
funkcionalismus	funkcionalismus	k1gInSc1	funkcionalismus
==	==	k?	==
</s>
</p>
<p>
<s>
Mertonova	Mertonův	k2eAgFnSc1d1	Mertonova
varianta	varianta	k1gFnSc1	varianta
funkcionalismu	funkcionalismus	k1gInSc2	funkcionalismus
se	se	k3xPyFc4	se
lišila	lišit	k5eAaImAgFnS	lišit
od	od	k7c2	od
verzí	verze	k1gFnPc2	verze
jeho	jeho	k3xOp3gMnPc2	jeho
předchůdců	předchůdce	k1gMnPc2	předchůdce
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
byli	být	k5eAaImAgMnP	být
Bronislaw	Bronislaw	k1gMnSc4	Bronislaw
Malinowski	Malinowsk	k1gFnSc2	Malinowsk
nebo	nebo	k8xC	nebo
Alfred	Alfred	k1gMnSc1	Alfred
R.	R.	kA	R.
Radcliffe-Brown	Radcliffe-Brown	k1gMnSc1	Radcliffe-Brown
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
více	hodně	k6eAd2	hodně
propracovaná	propracovaný	k2eAgFnSc1d1	propracovaná
a	a	k8xC	a
dala	dát	k5eAaPmAgFnS	dát
se	se	k3xPyFc4	se
lépe	dobře	k6eAd2	dobře
aplikovat	aplikovat	k5eAaBmF	aplikovat
na	na	k7c4	na
moderní	moderní	k2eAgFnSc4d1	moderní
společnost	společnost	k1gFnSc4	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
původní	původní	k2eAgFnSc1d1	původní
teorie	teorie	k1gFnSc1	teorie
sociologického	sociologický	k2eAgInSc2d1	sociologický
funkcionalismu	funkcionalismus	k1gInSc2	funkcionalismus
počítala	počítat	k5eAaImAgFnS	počítat
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
pozitivními	pozitivní	k2eAgFnPc7d1	pozitivní
funkcemi	funkce	k1gFnPc7	funkce
<g/>
,	,	kIx,	,
Merton	Merton	k1gInSc4	Merton
říkal	říkat	k5eAaImAgMnS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
funkce	funkce	k1gFnPc1	funkce
jsou	být	k5eAaImIp3nP	být
nejen	nejen	k6eAd1	nejen
pozitivní	pozitivní	k2eAgInPc1d1	pozitivní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
takové	takový	k3xDgFnPc1	takový
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
negativní	negativní	k2eAgInSc4d1	negativní
dopad	dopad	k1gInSc4	dopad
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
variantě	varianta	k1gFnSc6	varianta
funkcionalismu	funkcionalismus	k1gInSc2	funkcionalismus
proto	proto	k8xC	proto
dělí	dělit	k5eAaImIp3nP	dělit
funkce	funkce	k1gFnPc4	funkce
do	do	k7c2	do
dvou	dva	k4xCgFnPc2	dva
skupin	skupina	k1gFnPc2	skupina
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
jejich	jejich	k3xOp3gInSc2	jejich
dopadu	dopad	k1gInSc2	dopad
na	na	k7c4	na
společnost	společnost	k1gFnSc4	společnost
<g/>
:	:	kIx,	:
na	na	k7c4	na
pozitivní	pozitivní	k2eAgFnPc4d1	pozitivní
funkce	funkce	k1gFnPc4	funkce
a	a	k8xC	a
dysfunkce	dysfunkce	k1gFnPc4	dysfunkce
<g/>
.	.	kIx.	.
<g/>
Ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
díle	dílo	k1gNnSc6	dílo
navázal	navázat	k5eAaPmAgInS	navázat
na	na	k7c4	na
Parsonse	Parsons	k1gMnSc4	Parsons
a	a	k8xC	a
dál	daleko	k6eAd2	daleko
rozpracoval	rozpracovat	k5eAaPmAgMnS	rozpracovat
jeho	jeho	k3xOp3gFnPc4	jeho
teorie	teorie	k1gFnPc4	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Reagoval	reagovat	k5eAaBmAgMnS	reagovat
na	na	k7c4	na
kritiku	kritika	k1gFnSc4	kritika
strukturního	strukturní	k2eAgInSc2d1	strukturní
funkcionalismu	funkcionalismus	k1gInSc2	funkcionalismus
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
konfliktualistů	konfliktualista	k1gMnPc2	konfliktualista
<g/>
.	.	kIx.	.
</s>
<s>
Zavedl	zavést	k5eAaPmAgMnS	zavést
pojmy	pojem	k1gInPc4	pojem
manifestní	manifestní	k2eAgInPc4d1	manifestní
a	a	k8xC	a
latentní	latentní	k2eAgFnPc4d1	latentní
funkce	funkce	k1gFnPc4	funkce
<g/>
:	:	kIx,	:
Manifestní	manifestní	k2eAgFnPc1d1	manifestní
funkce	funkce	k1gFnPc1	funkce
jsou	být	k5eAaImIp3nP	být
zjevné	zjevný	k2eAgFnPc1d1	zjevná
<g/>
,	,	kIx,	,
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
vědomých	vědomý	k2eAgFnPc2d1	vědomá
intencí	intence	k1gFnPc2	intence
aktérů	aktér	k1gMnPc2	aktér
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
sledují	sledovat	k5eAaImIp3nP	sledovat
určitý	určitý	k2eAgInSc4d1	určitý
cíl	cíl	k1gInSc4	cíl
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
latentní	latentní	k2eAgFnPc1d1	latentní
funkce	funkce	k1gFnPc1	funkce
jsou	být	k5eAaImIp3nP	být
skryté	skrytý	k2eAgFnPc1d1	skrytá
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc1	jejich
význam	význam	k1gInSc1	význam
není	být	k5eNaImIp3nS	být
aktérům	aktér	k1gMnPc3	aktér
známý	známý	k2eAgInSc1d1	známý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
Mertona	Merton	k1gMnSc2	Merton
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
sociologii	sociologie	k1gFnSc4	sociologie
klíčové	klíčový	k2eAgFnSc2d1	klíčová
zejména	zejména	k9	zejména
zkoumání	zkoumání	k1gNnSc3	zkoumání
latentních	latentní	k2eAgFnPc2d1	latentní
funkcí	funkce	k1gFnPc2	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
odkrývání	odkrývání	k1gNnSc1	odkrývání
má	mít	k5eAaImIp3nS	mít
podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
velký	velký	k2eAgInSc1d1	velký
přínos	přínos	k1gInSc4	přínos
pro	pro	k7c4	pro
sociologii	sociologie	k1gFnSc4	sociologie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Teorie	teorie	k1gFnSc1	teorie
středního	střední	k2eAgInSc2d1	střední
dosahu	dosah	k1gInSc2	dosah
==	==	k?	==
</s>
</p>
<p>
<s>
Základem	základ	k1gInSc7	základ
sociologického	sociologický	k2eAgNnSc2d1	sociologické
zkoumání	zkoumání	k1gNnSc2	zkoumání
by	by	kYmCp3nS	by
měly	mít	k5eAaImAgInP	mít
podle	podle	k7c2	podle
Mertona	Merton	k1gMnSc2	Merton
být	být	k5eAaImF	být
teorie	teorie	k1gFnPc1	teorie
středního	střední	k2eAgInSc2d1	střední
dosahu	dosah	k1gInSc2	dosah
<g/>
.	.	kIx.	.
</s>
<s>
Reagoval	reagovat	k5eAaBmAgMnS	reagovat
tím	ten	k3xDgNnSc7	ten
na	na	k7c4	na
tehdejší	tehdejší	k2eAgInSc4d1	tehdejší
rozdvojený	rozdvojený	k2eAgInSc4d1	rozdvojený
stav	stav	k1gInSc4	stav
sociologie	sociologie	k1gFnSc2	sociologie
<g/>
.	.	kIx.	.
</s>
<s>
Tvorba	tvorba	k1gFnSc1	tvorba
teorií	teorie	k1gFnSc7	teorie
středního	střední	k2eAgInSc2d1	střední
dosahu	dosah	k1gInSc2	dosah
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
kompromisem	kompromis	k1gInSc7	kompromis
mezi	mezi	k7c7	mezi
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
velkými	velký	k2eAgFnPc7d1	velká
a	a	k8xC	a
všeobecnými	všeobecný	k2eAgFnPc7d1	všeobecná
teoriemi	teorie	k1gFnPc7	teorie
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
byla	být	k5eAaImAgFnS	být
ta	ten	k3xDgFnSc1	ten
Parsonsova	Parsonsův	k2eAgFnSc1d1	Parsonsova
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgInP	být
příliš	příliš	k6eAd1	příliš
vzdálené	vzdálený	k2eAgInPc1d1	vzdálený
od	od	k7c2	od
empirické	empirický	k2eAgFnSc2d1	empirická
zkušenosti	zkušenost	k1gFnSc2	zkušenost
<g/>
,	,	kIx,	,
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc3	strana
suchými	suchý	k2eAgInPc7d1	suchý
empirickými	empirický	k2eAgInPc7d1	empirický
výzkumy	výzkum	k1gInPc7	výzkum
<g/>
,	,	kIx,	,
popisujícími	popisující	k2eAgInPc7d1	popisující
pouze	pouze	k6eAd1	pouze
konkrétní	konkrétní	k2eAgFnPc4d1	konkrétní
skutečnosti	skutečnost	k1gFnPc4	skutečnost
bez	bez	k7c2	bez
nějaké	nějaký	k3yIgFnSc2	nějaký
teoretické	teoretický	k2eAgFnSc2d1	teoretická
zobecnitelnosti	zobecnitelnost	k1gFnSc2	zobecnitelnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Teorie	teorie	k1gFnSc1	teorie
středního	střední	k2eAgInSc2d1	střední
dosahu	dosah	k1gInSc2	dosah
jsou	být	k5eAaImIp3nP	být
podle	podle	k7c2	podle
Mertona	Merton	k1gMnSc2	Merton
výhodné	výhodný	k2eAgNnSc1d1	výhodné
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
empirické	empirický	k2eAgFnSc3d1	empirická
ověřitelnosti	ověřitelnost	k1gFnSc3	ověřitelnost
a	a	k8xC	a
dají	dát	k5eAaPmIp3nP	dát
se	se	k3xPyFc4	se
použít	použít	k5eAaPmF	použít
na	na	k7c4	na
širokou	široký	k2eAgFnSc4d1	široká
škálu	škála	k1gFnSc4	škála
jevů	jev	k1gInPc2	jev
<g/>
.	.	kIx.	.
</s>
<s>
Upřednostňoval	upřednostňovat	k5eAaImAgMnS	upřednostňovat
je	být	k5eAaImIp3nS	být
před	před	k7c7	před
rozsáhlými	rozsáhlý	k2eAgNnPc7d1	rozsáhlé
teoretickými	teoretický	k2eAgNnPc7d1	teoretické
schématy	schéma	k1gNnPc7	schéma
<g/>
,	,	kIx,	,
typickými	typický	k2eAgFnPc7d1	typická
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
přírodní	přírodní	k2eAgFnPc4d1	přírodní
vědy	věda	k1gFnPc4	věda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Mertonův	Mertonův	k2eAgInSc1d1	Mertonův
pojem	pojem	k1gInSc1	pojem
anomie	anomie	k1gFnSc2	anomie
==	==	k?	==
</s>
</p>
<p>
<s>
Pojem	pojem	k1gInSc1	pojem
anomie	anomie	k1gFnSc2	anomie
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
době	doba	k1gFnSc6	doba
Velké	velký	k2eAgFnSc2d1	velká
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
krize	krize	k1gFnSc2	krize
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
zhroucení	zhroucení	k1gNnSc3	zhroucení
hodnot	hodnota	k1gFnPc2	hodnota
a	a	k8xC	a
krizi	krize	k1gFnSc4	krize
americké	americký	k2eAgFnSc2d1	americká
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
zakladatele	zakladatel	k1gMnSc2	zakladatel
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
Émile	Émile	k1gInSc1	Émile
Durkheim	Durkheima	k1gFnPc2	Durkheima
<g/>
.	.	kIx.	.
</s>
<s>
Merton	Merton	k1gInSc1	Merton
se	se	k3xPyFc4	se
však	však	k9	však
na	na	k7c6	na
anomii	anomie	k1gFnSc6	anomie
díval	dívat	k5eAaImAgMnS	dívat
jinak	jinak	k6eAd1	jinak
než	než	k8xS	než
on	on	k3xPp3gMnSc1	on
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
něj	on	k3xPp3gNnSc2	on
<g/>
,	,	kIx,	,
existují	existovat	k5eAaImIp3nP	existovat
různé	různý	k2eAgFnPc4d1	různá
formy	forma	k1gFnPc4	forma
přizpůsobení	přizpůsobení	k1gNnSc2	přizpůsobení
se	se	k3xPyFc4	se
na	na	k7c4	na
anomickou	anomický	k2eAgFnSc4d1	anomická
situaci	situace	k1gFnSc4	situace
<g/>
.	.	kIx.	.
<g/>
Americká	americký	k2eAgFnSc1d1	americká
společnost	společnost	k1gFnSc1	společnost
staví	stavit	k5eAaPmIp3nS	stavit
na	na	k7c4	na
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
svého	svůj	k3xOyFgInSc2	svůj
žebříčku	žebříček	k1gInSc2	žebříček
hodnot	hodnota	k1gFnPc2	hodnota
úspěch	úspěch	k1gInSc1	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
cíl	cíl	k1gInSc4	cíl
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yQgInSc3	který
se	se	k3xPyFc4	se
všichni	všechen	k3xTgMnPc1	všechen
snaží	snažit	k5eAaImIp3nP	snažit
směřovat	směřovat	k5eAaImF	směřovat
a	a	k8xC	a
pomocí	pomocí	k7c2	pomocí
tvrdé	tvrdý	k2eAgFnSc2d1	tvrdá
práce	práce	k1gFnSc2	práce
ho	on	k3xPp3gMnSc4	on
mohou	moct	k5eAaImIp3nP	moct
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Krize	krize	k1gFnSc1	krize
ale	ale	k9	ale
ukázala	ukázat	k5eAaPmAgFnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ani	ani	k8xC	ani
ta	ten	k3xDgFnSc1	ten
nemusí	muset	k5eNaImIp3nS	muset
vždy	vždy	k6eAd1	vždy
stačit	stačit	k5eAaBmF	stačit
k	k	k7c3	k
úspěchu	úspěch	k1gInSc3	úspěch
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c4	na
anomickou	anomický	k2eAgFnSc4d1	anomická
situaci	situace	k1gFnSc4	situace
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
Mertona	Merton	k1gMnSc2	Merton
<g/>
,	,	kIx,	,
reagovat	reagovat	k5eAaBmF	reagovat
pěti	pět	k4xCc7	pět
různými	různý	k2eAgInPc7d1	různý
způsoby	způsob	k1gInPc7	způsob
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
nichž	jenž	k3xRgMnPc2	jenž
jsou	být	k5eAaImIp3nP	být
lidé	člověk	k1gMnPc1	člověk
schopni	schopen	k2eAgMnPc1d1	schopen
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
svých	svůj	k3xOyFgInPc2	svůj
cílů	cíl	k1gInPc2	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
konformita	konformita	k1gFnSc1	konformita
<g/>
,	,	kIx,	,
inovace	inovace	k1gFnSc1	inovace
<g/>
,	,	kIx,	,
ritualismus	ritualismus	k1gInSc1	ritualismus
<g/>
,	,	kIx,	,
únik	únik	k1gInSc1	únik
a	a	k8xC	a
rebelie	rebelie	k1gFnSc1	rebelie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
konformita	konformita	k1gFnSc1	konformita
–	–	k?	–
Konformisté	konformista	k1gMnPc1	konformista
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
všeobecně	všeobecně	k6eAd1	všeobecně
uznávaných	uznávaný	k2eAgInPc2d1	uznávaný
cílů	cíl	k1gInPc2	cíl
prostředky	prostředek	k1gInPc4	prostředek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
všeobecně	všeobecně	k6eAd1	všeobecně
přijímané	přijímaný	k2eAgFnPc1d1	přijímaná
a	a	k8xC	a
přijatelné	přijatelný	k2eAgFnPc1d1	přijatelná
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
společnost	společnost	k1gFnSc1	společnost
stabilní	stabilní	k2eAgFnSc1d1	stabilní
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nejrozšířenější	rozšířený	k2eAgInSc4d3	nejrozšířenější
typ	typ	k1gInSc4	typ
adaptace	adaptace	k1gFnSc2	adaptace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
inovace	inovace	k1gFnSc1	inovace
–	–	k?	–
Inovátoři	inovátor	k1gMnPc1	inovátor
mají	mít	k5eAaImIp3nP	mít
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
konformisté	konformista	k1gMnPc1	konformista
cíle	cíl	k1gInSc2	cíl
uznávané	uznávaný	k2eAgNnSc4d1	uznávané
společností	společnost	k1gFnSc7	společnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
dosažení	dosažení	k1gNnSc3	dosažení
používají	používat	k5eAaImIp3nP	používat
své	svůj	k3xOyFgFnPc4	svůj
vlastní	vlastní	k2eAgFnPc4d1	vlastní
metody	metoda	k1gFnPc4	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Inovátoři	inovátor	k1gMnPc1	inovátor
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
jak	jak	k8xC	jak
třeba	třeba	k6eAd1	třeba
zloději	zloděj	k1gMnPc1	zloděj
nebo	nebo	k8xC	nebo
podvodníci	podvodník	k1gMnPc1	podvodník
tak	tak	k8xC	tak
vynálezci	vynálezce	k1gMnPc1	vynálezce
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Mertona	Merton	k1gMnSc2	Merton
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
inovátoři	inovátor	k1gMnPc1	inovátor
uchylují	uchylovat	k5eAaImIp3nP	uchylovat
k	k	k7c3	k
nelegitmním	legitmní	k2eNgInPc3d1	legitmní
prostředkům	prostředek	k1gInPc3	prostředek
dosažení	dosažení	k1gNnSc4	dosažení
cílů	cíl	k1gInPc2	cíl
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
neexistují	existovat	k5eNaImIp3nP	existovat
prostředky	prostředek	k1gInPc1	prostředek
legitimní	legitimní	k2eAgInPc1d1	legitimní
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
reagují	reagovat	k5eAaBmIp3nP	reagovat
"	"	kIx"	"
<g/>
normálně	normálně	k6eAd1	normálně
<g/>
"	"	kIx"	"
na	na	k7c4	na
"	"	kIx"	"
<g/>
nenormální	normální	k2eNgFnSc4d1	nenormální
<g/>
"	"	kIx"	"
situaci	situace	k1gFnSc4	situace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ritualismus	ritualismus	k1gInSc1	ritualismus
–	–	k?	–
Ritualisté	Ritualista	k1gMnPc1	Ritualista
sice	sice	k8xC	sice
už	už	k6eAd1	už
neuznávají	uznávat	k5eNaImIp3nP	uznávat
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
zapomněli	zapomenout	k5eAaPmAgMnP	zapomenout
na	na	k7c4	na
společenské	společenský	k2eAgFnPc4d1	společenská
hodnoty	hodnota	k1gFnPc4	hodnota
a	a	k8xC	a
cíle	cíl	k1gInPc4	cíl
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přesto	přesto	k8xC	přesto
dodržují	dodržovat	k5eAaImIp3nP	dodržovat
již	již	k6eAd1	již
existující	existující	k2eAgNnPc1d1	existující
pravidla	pravidlo	k1gNnPc1	pravidlo
a	a	k8xC	a
normy	norma	k1gFnPc1	norma
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
stereotypní	stereotypní	k2eAgFnPc4d1	stereotypní
práce	práce	k1gFnPc4	práce
s	s	k7c7	s
minimální	minimální	k2eAgFnSc7d1	minimální
příležitostí	příležitost	k1gFnSc7	příležitost
ke	k	k7c3	k
kariérnímu	kariérní	k2eAgInSc3d1	kariérní
růstu	růst	k1gInSc3	růst
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
únik	únik	k1gInSc1	únik
–	–	k?	–
Takzvaní	takzvaný	k2eAgMnPc1d1	takzvaný
"	"	kIx"	"
<g/>
odpadlíci	odpadlík	k1gMnPc1	odpadlík
<g/>
"	"	kIx"	"
jsou	být	k5eAaImIp3nP	být
před	před	k7c7	před
většinovou	většinový	k2eAgFnSc7d1	většinová
společností	společnost	k1gFnSc7	společnost
uzavřeni	uzavřen	k2eAgMnPc1d1	uzavřen
sami	sám	k3xTgMnPc1	sám
do	do	k7c2	do
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Odmítají	odmítat	k5eAaImIp3nP	odmítat
cíle	cíl	k1gInPc1	cíl
společnosti	společnost	k1gFnSc2	společnost
i	i	k8xC	i
způsoby	způsob	k1gInPc1	způsob
jejich	jejich	k3xOp3gNnSc2	jejich
dosažení	dosažení	k1gNnSc2	dosažení
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
jednat	jednat	k5eAaImF	jednat
například	například	k6eAd1	například
o	o	k7c4	o
tuláky	tulák	k1gMnPc4	tulák
nebo	nebo	k8xC	nebo
drogově	drogově	k6eAd1	drogově
závislé	závislý	k2eAgNnSc1d1	závislé
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
jedince	jedinko	k6eAd1	jedinko
s	s	k7c7	s
nadčasovými	nadčasový	k2eAgInPc7d1	nadčasový
názory	názor	k1gInPc7	názor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
rebelie	rebelie	k1gFnSc1	rebelie
–	–	k?	–
Rebelové	rebel	k1gMnPc1	rebel
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
nahradit	nahradit	k5eAaPmF	nahradit
existující	existující	k2eAgFnPc4d1	existující
hodnoty	hodnota	k1gFnPc4	hodnota
jinými	jiný	k2eAgNnPc7d1	jiné
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
úplně	úplně	k6eAd1	úplně
změnit	změnit	k5eAaPmF	změnit
strukturu	struktura	k1gFnSc4	struktura
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc4	ten
je	být	k5eAaImIp3nS	být
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
od	od	k7c2	od
odpadlíků	odpadlík	k1gMnPc2	odpadlík
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
hodnoty	hodnota	k1gFnPc4	hodnota
a	a	k8xC	a
cesty	cesta	k1gFnPc4	cesta
také	také	k9	také
odmítají	odmítat	k5eAaImIp3nP	odmítat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nesnaží	snažit	k5eNaImIp3nS	snažit
se	se	k3xPyFc4	se
o	o	k7c6	o
dosazení	dosazení	k1gNnSc6	dosazení
nových	nový	k2eAgInPc2d1	nový
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
jednat	jednat	k5eAaImF	jednat
například	například	k6eAd1	například
o	o	k7c4	o
členy	člen	k1gInPc4	člen
radikálních	radikální	k2eAgFnPc2d1	radikální
politických	politický	k2eAgFnPc2d1	politická
nebo	nebo	k8xC	nebo
náboženských	náboženský	k2eAgFnPc2d1	náboženská
hnutí	hnutí	k1gNnPc2	hnutí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
lidi	člověk	k1gMnPc4	člověk
stavící	stavící	k2eAgFnSc1d1	stavící
se	se	k3xPyFc4	se
například	například	k6eAd1	například
proti	proti	k7c3	proti
diktatuře	diktatura	k1gFnSc3	diktatura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dysfunkce	dysfunkce	k1gFnSc1	dysfunkce
byrokracie	byrokracie	k1gFnSc1	byrokracie
==	==	k?	==
</s>
</p>
<p>
<s>
Merton	Merton	k1gInSc1	Merton
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaPmAgInS	věnovat
struktuře	struktura	k1gFnSc3	struktura
byrokracie	byrokracie	k1gFnSc1	byrokracie
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vycházel	vycházet	k5eAaImAgMnS	vycházet
z	z	k7c2	z
analýzy	analýza	k1gFnSc2	analýza
byrokracie	byrokracie	k1gFnSc2	byrokracie
Maxe	Max	k1gMnSc2	Max
Webera	Weber	k1gMnSc2	Weber
<g/>
.	.	kIx.	.
</s>
<s>
Definoval	definovat	k5eAaBmAgInS	definovat
pojem	pojem	k1gInSc1	pojem
dysfunkce	dysfunkce	k1gFnSc2	dysfunkce
byrokracie	byrokracie	k1gFnSc2	byrokracie
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
problém	problém	k1gInSc1	problém
spatřoval	spatřovat	k5eAaImAgInS	spatřovat
v	v	k7c6	v
situaci	situace	k1gFnSc6	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
úředník	úředník	k1gMnSc1	úředník
příliš	příliš	k6eAd1	příliš
soustředí	soustředit	k5eAaPmIp3nS	soustředit
na	na	k7c4	na
dodržování	dodržování	k1gNnSc4	dodržování
všech	všecek	k3xTgInPc2	všecek
předpisů	předpis	k1gInPc2	předpis
a	a	k8xC	a
vysokou	vysoký	k2eAgFnSc4d1	vysoká
disciplinovanost	disciplinovanost	k1gFnSc4	disciplinovanost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
až	až	k9	až
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
dodržování	dodržování	k1gNnSc1	dodržování
pravidel	pravidlo	k1gNnPc2	pravidlo
stává	stávat	k5eAaImIp3nS	stávat
konečnou	konečný	k2eAgFnSc7d1	konečná
hodnotou	hodnota	k1gFnSc7	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
oddanosti	oddanost	k1gFnSc3	oddanost
k	k	k7c3	k
pravidlům	pravidlo	k1gNnPc3	pravidlo
se	se	k3xPyFc4	se
byrokrat	byrokrat	k1gMnSc1	byrokrat
hůře	zle	k6eAd2	zle
adaptuje	adaptovat	k5eAaBmIp3nS	adaptovat
na	na	k7c4	na
nové	nový	k2eAgFnPc4d1	nová
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Stává	stávat	k5eAaImIp3nS	stávat
se	se	k3xPyFc4	se
neefektivním	efektivní	k2eNgMnPc3d1	neefektivní
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
míry	míra	k1gFnSc2	míra
<g/>
,	,	kIx,	,
že	že	k8xS	že
kvůli	kvůli	k7c3	kvůli
svázanosti	svázanost	k1gFnSc3	svázanost
pravidly	pravidlo	k1gNnPc7	pravidlo
není	být	k5eNaImIp3nS	být
schopný	schopný	k2eAgInSc1d1	schopný
pomoct	pomoct	k5eAaPmF	pomoct
svým	svůj	k3xOyFgMnPc3	svůj
klientům	klient	k1gMnPc3	klient
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dalším	další	k2eAgInSc7d1	další
problém	problém	k1gInSc1	problém
byrokracie	byrokracie	k1gFnSc1	byrokracie
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
Mertona	Merton	k1gMnSc2	Merton
odosobnění	odosobnění	k1gNnSc2	odosobnění
vztahů	vztah	k1gInPc2	vztah
na	na	k7c6	na
pracovišti	pracoviště	k1gNnSc6	pracoviště
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
neschopnosti	neschopnost	k1gFnSc3	neschopnost
byrokratů	byrokrat	k1gMnPc2	byrokrat
přistupovat	přistupovat	k5eAaImF	přistupovat
individuálně	individuálně	k6eAd1	individuálně
k	k	k7c3	k
jednotlivým	jednotlivý	k2eAgFnPc3d1	jednotlivá
klientů	klient	k1gMnPc2	klient
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sociologie	sociologie	k1gFnSc2	sociologie
vědy	věda	k1gFnSc2	věda
==	==	k?	==
</s>
</p>
<p>
<s>
Merton	Merton	k1gInSc1	Merton
položil	položit	k5eAaPmAgInS	položit
základy	základ	k1gInPc4	základ
k	k	k7c3	k
sociologii	sociologie	k1gFnSc3	sociologie
vědy	věda	k1gFnSc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
disciplína	disciplína	k1gFnSc1	disciplína
by	by	kYmCp3nS	by
podle	podle	k7c2	podle
Mertona	Merton	k1gMnSc2	Merton
měla	mít	k5eAaImAgFnS	mít
zkoumat	zkoumat	k5eAaImF	zkoumat
<g/>
,	,	kIx,	,
do	do	k7c2	do
jaké	jaký	k3yIgFnSc2	jaký
míry	míra	k1gFnSc2	míra
jsou	být	k5eAaImIp3nP	být
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
obory	obor	k1gInPc4	obor
vědecké	vědecký	k2eAgInPc4d1	vědecký
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
obecná	obecná	k1gFnSc1	obecná
kritéria	kritérion	k1gNnSc2	kritérion
vědecké	vědecký	k2eAgFnSc2d1	vědecká
zralosti	zralost	k1gFnSc2	zralost
–	–	k?	–
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
vědy	věda	k1gFnPc4	věda
na	na	k7c6	na
zralé	zralý	k2eAgFnSc6d1	zralá
a	a	k8xC	a
nezralé	zralý	k2eNgFnSc6d1	nezralá
<g/>
.	.	kIx.	.
</s>
<s>
Snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
zformulovat	zformulovat	k5eAaPmF	zformulovat
čtyři	čtyři	k4xCgInPc1	čtyři
základní	základní	k2eAgInPc1d1	základní
imperativy	imperativ	k1gInPc1	imperativ
sociologa	sociolog	k1gMnSc2	sociolog
jako	jako	k8xS	jako
vědce	vědec	k1gMnSc2	vědec
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
univerzalismus	univerzalismus	k1gInSc1	univerzalismus
–	–	k?	–
Věda	věda	k1gFnSc1	věda
je	být	k5eAaImIp3nS	být
univerzální	univerzální	k2eAgFnSc1d1	univerzální
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
na	na	k7c6	na
rase	rasa	k1gFnSc6	rasa
<g/>
,	,	kIx,	,
národnosti	národnost	k1gFnSc6	národnost
<g/>
,	,	kIx,	,
na	na	k7c6	na
filosofické	filosofický	k2eAgFnSc6d1	filosofická
nebo	nebo	k8xC	nebo
světonázorové	světonázorový	k2eAgFnSc6d1	světonázorová
orientaci	orientace	k1gFnSc6	orientace
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
majetkem	majetek	k1gInSc7	majetek
všeho	všecek	k3xTgNnSc2	všecek
lidstva	lidstvo	k1gNnSc2	lidstvo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
komunismus	komunismus	k1gInSc1	komunismus
(	(	kIx(	(
<g/>
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
původní	původní	k2eAgInSc4d1	původní
význam	význam	k1gInSc4	význam
tohoto	tento	k3xDgNnSc2	tento
slova	slovo	k1gNnSc2	slovo
<g/>
)	)	kIx)	)
–	–	k?	–
Základní	základní	k2eAgInPc4d1	základní
vědecké	vědecký	k2eAgInPc4d1	vědecký
objevy	objev	k1gInPc4	objev
jsou	být	k5eAaImIp3nP	být
výsledkem	výsledek	k1gInSc7	výsledek
kooperace	kooperace	k1gFnSc2	kooperace
a	a	k8xC	a
výsledkem	výsledek	k1gInSc7	výsledek
společné	společný	k2eAgFnSc2d1	společná
práce	práce	k1gFnSc2	práce
ve	v	k7c6	v
věde	věde	k1gFnSc6	věde
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
nichž	jenž	k3xRgInPc2	jenž
byly	být	k5eAaImAgFnP	být
nazvány	nazvat	k5eAaBmNgInP	nazvat
vědecké	vědecký	k2eAgInPc1d1	vědecký
zákony	zákon	k1gInPc1	zákon
<g/>
,	,	kIx,	,
nejsou	být	k5eNaImIp3nP	být
jejich	jejich	k3xOp3gMnPc4	jejich
vlastníky	vlastník	k1gMnPc4	vlastník
<g/>
.	.	kIx.	.
</s>
<s>
Vědecké	vědecký	k2eAgNnSc1d1	vědecké
bádání	bádání	k1gNnSc1	bádání
má	mít	k5eAaImIp3nS	mít
mimotržní	mimotržní	k2eAgInSc4d1	mimotržní
charakter	charakter	k1gInSc4	charakter
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
dezinteresovanost	dezinteresovanost	k1gFnSc4	dezinteresovanost
–	–	k?	–
osobní	osobní	k2eAgInPc4d1	osobní
zájmy	zájem	k1gInPc4	zájem
nemají	mít	k5eNaImIp3nP	mít
vstupovat	vstupovat	k5eAaImF	vstupovat
do	do	k7c2	do
tvorby	tvorba	k1gFnSc2	tvorba
vědeckého	vědecký	k2eAgNnSc2d1	vědecké
poznání	poznání	k1gNnSc2	poznání
<g/>
.	.	kIx.	.
</s>
<s>
Merton	Merton	k1gInSc4	Merton
si	se	k3xPyFc3	se
byl	být	k5eAaImAgMnS	být
vědom	vědom	k2eAgMnSc1d1	vědom
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
příkaz	příkaz	k1gInSc1	příkaz
je	být	k5eAaImIp3nS	být
nejspornější	sporný	k2eAgInSc1d3	nejspornější
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
organizovaný	organizovaný	k2eAgInSc1d1	organizovaný
skepticismus	skepticismus	k1gInSc1	skepticismus
–	–	k?	–
přikazuje	přikazovat	k5eAaImIp3nS	přikazovat
respektovat	respektovat	k5eAaImF	respektovat
autoritu	autorita	k1gFnSc4	autorita
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nepodřizovat	podřizovat	k5eNaImF	podřizovat
jí	on	k3xPp3gFnSc7	on
výsledky	výsledek	k1gInPc4	výsledek
bádání	bádání	k1gNnSc2	bádání
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
vědec	vědec	k1gMnSc1	vědec
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
skeptický	skeptický	k2eAgMnSc1d1	skeptický
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
autoritami	autorita	k1gFnPc7	autorita
jsou	být	k5eAaImIp3nP	být
většinou	většinou	k6eAd1	většinou
omylní	omylný	k2eAgMnPc1d1	omylný
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vede	vést	k5eAaImIp3nS	vést
ke	k	k7c3	k
konfliktu	konflikt	k1gInSc3	konflikt
uvnitř	uvnitř	k7c2	uvnitř
komunity	komunita	k1gFnSc2	komunita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Mertonovy	Mertonův	k2eAgInPc1d1	Mertonův
koncepty	koncept	k1gInPc1	koncept
==	==	k?	==
</s>
</p>
<p>
<s>
Merton	Merton	k1gInSc1	Merton
během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
celoživotní	celoživotní	k2eAgFnSc2d1	celoživotní
práce	práce	k1gFnSc2	práce
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
několik	několik	k4yIc4	několik
konceptů	koncept	k1gInPc2	koncept
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgInP	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
jeho	jeho	k3xOp3gNnSc4	jeho
ocenění	ocenění	k1gNnSc4	ocenění
Národní	národní	k2eAgNnSc1d1	národní
vyznamenání	vyznamenání	k1gNnSc1	vyznamenání
za	za	k7c4	za
vědu	věda	k1gFnSc4	věda
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nezamýšlené	zamýšlený	k2eNgInPc1d1	nezamýšlený
důsledky	důsledek	k1gInPc1	důsledek
–	–	k?	–
výsledky	výsledek	k1gInPc4	výsledek
jednání	jednání	k1gNnSc2	jednání
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
nejsou	být	k5eNaImIp3nP	být
očekávány	očekávat	k5eAaImNgInP	očekávat
<g/>
.	.	kIx.	.
</s>
<s>
Merton	Merton	k1gInSc1	Merton
tomuto	tento	k3xDgMnSc3	tento
pojmu	pojmout	k5eAaPmIp1nS	pojmout
zjednal	zjednat	k5eAaPmAgInS	zjednat
status	status	k1gInSc4	status
vědeckého	vědecký	k2eAgInSc2d1	vědecký
pojmu	pojem	k1gInSc2	pojem
<g/>
.	.	kIx.	.
</s>
<s>
Rozdělil	rozdělit	k5eAaPmAgMnS	rozdělit
nezamýšlené	zamýšlený	k2eNgInPc4d1	nezamýšlený
důsledky	důsledek	k1gInPc4	důsledek
do	do	k7c2	do
dvou	dva	k4xCgFnPc2	dva
kategorií	kategorie	k1gFnPc2	kategorie
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Eufunkční	Eufunkční	k2eAgFnSc1d1	Eufunkční
–	–	k?	–
neočekávaná	očekávaný	k2eNgFnSc1d1	neočekávaná
pozitivní	pozitivní	k2eAgFnSc1d1	pozitivní
výhoda	výhoda	k1gFnSc1	výhoda
<g/>
,	,	kIx,	,
např.	např.	kA	např.
škola	škola	k1gFnSc1	škola
přináší	přinášet	k5eAaImIp3nS	přinášet
dětem	dítě	k1gFnPc3	dítě
znalosti	znalost	k1gFnSc2	znalost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
je	on	k3xPp3gInPc4	on
drží	držet	k5eAaImIp3nS	držet
od	od	k7c2	od
trhu	trh	k1gInSc2	trh
pracovních	pracovní	k2eAgFnPc2d1	pracovní
příležitostí	příležitost	k1gFnPc2	příležitost
</s>
</p>
<p>
<s>
Dysfunkční	dysfunkční	k2eAgFnPc4d1	dysfunkční
–	–	k?	–
neočekávané	očekávaný	k2eNgFnPc4d1	neočekávaná
nevýhody	nevýhoda	k1gFnPc4	nevýhoda
<g/>
,	,	kIx,	,
např.	např.	kA	např.
firma	firma	k1gFnSc1	firma
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
produkt	produkt	k1gInSc4	produkt
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vyhovoval	vyhovovat	k5eAaImAgInS	vyhovovat
zákazníkovi	zákazník	k1gMnSc3	zákazník
co	co	k9	co
nejvíce	nejvíce	k6eAd1	nejvíce
(	(	kIx(	(
<g/>
dobrý	dobrý	k2eAgInSc1d1	dobrý
úmysl	úmysl	k1gInSc1	úmysl
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
touto	tento	k3xDgFnSc7	tento
výrobou	výroba	k1gFnSc7	výroba
ničí	ničit	k5eAaImIp3nS	ničit
životní	životní	k2eAgNnSc4d1	životní
prostředí	prostředí	k1gNnSc4	prostředí
</s>
</p>
<p>
<s>
Referenční	referenční	k2eAgFnSc1d1	referenční
skupina	skupina	k1gFnSc1	skupina
–	–	k?	–
skupina	skupina	k1gFnSc1	skupina
<g/>
,	,	kIx,	,
od	od	k7c2	od
níž	jenž	k3xRgFnSc2	jenž
odvozujete	odvozovat	k5eAaImIp2nP	odvozovat
své	svůj	k3xOyFgFnPc4	svůj
hodnoty	hodnota	k1gFnPc4	hodnota
a	a	k8xC	a
normy	norma	k1gFnPc4	norma
chování	chování	k1gNnSc2	chování
</s>
</p>
<p>
<s>
Vzor	vzor	k1gInSc1	vzor
–	–	k?	–
osoba	osoba	k1gFnSc1	osoba
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
člověk	člověk	k1gMnSc1	člověk
obdivuje	obdivovat	k5eAaImIp3nS	obdivovat
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc2	on
napodobit	napodobit	k5eAaPmF	napodobit
<g/>
.	.	kIx.	.
</s>
<s>
Koncept	koncept	k1gInSc1	koncept
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
z	z	k7c2	z
jeho	on	k3xPp3gInSc2	on
dalšího	další	k2eAgInSc2d1	další
konceptu	koncept	k1gInSc2	koncept
o	o	k7c6	o
referenční	referenční	k2eAgFnSc6d1	referenční
skupině	skupina	k1gFnSc6	skupina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
Mertonově	Mertonův	k2eAgFnSc6d1	Mertonova
studii	studie	k1gFnSc6	studie
na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
téma	téma	k1gNnSc4	téma
poukazuje	poukazovat	k5eAaImIp3nS	poukazovat
hlavně	hlavně	k9	hlavně
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
člověk	člověk	k1gMnSc1	člověk
nemá	mít	k5eNaImIp3nS	mít
pouze	pouze	k6eAd1	pouze
jednu	jeden	k4xCgFnSc4	jeden
roli	role	k1gFnSc4	role
a	a	k8xC	a
status	status	k1gInSc4	status
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
celou	celý	k2eAgFnSc4d1	celá
sadu	sada	k1gFnSc4	sada
statusů	status	k1gInPc2	status
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
každý	každý	k3xTgInSc4	každý
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
určité	určitý	k2eAgNnSc1d1	určité
chování	chování	k1gNnSc1	chování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sebenaplňující	Sebenaplňující	k2eAgNnSc4d1	Sebenaplňující
proroctví	proroctví	k1gNnSc4	proroctví
–	–	k?	–
očekávaní	očekávaný	k2eAgMnPc1d1	očekávaný
nějaké	nějaký	k3yIgFnSc3	nějaký
události	událost	k1gFnSc3	událost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
nastat	nastat	k5eAaPmF	nastat
<g/>
,	,	kIx,	,
způsobí	způsobit	k5eAaPmIp3nS	způsobit
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
událost	událost	k1gFnSc1	událost
opravdu	opravdu	k6eAd1	opravdu
stane	stanout	k5eAaPmIp3nS	stanout
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
definice	definice	k1gFnSc2	definice
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
optimistům	optimist	k1gMnPc3	optimist
se	se	k3xPyFc4	se
daří	dařit	k5eAaImIp3nS	dařit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
pesimistům	pesimist	k1gMnPc3	pesimist
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
prosadit	prosadit	k5eAaPmF	prosadit
a	a	k8xC	a
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
ostatní	ostatní	k2eAgInPc4d1	ostatní
<g/>
:	:	kIx,	:
prosaďte	prosadit	k5eAaPmRp2nP	prosadit
své	svůj	k3xOyFgInPc4	svůj
návrhy	návrh	k1gInPc4	návrh
<g/>
,	,	kIx,	,
nápady	nápad	k1gInPc4	nápad
a	a	k8xC	a
projekty	projekt	k1gInPc4	projekt
<g/>
,	,	kIx,	,
35	[number]	k4	35
<g/>
)	)	kIx)	)
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
předpověď	předpověď	k1gFnSc1	předpověď
negativní	negativní	k2eAgFnSc1d1	negativní
říká	říkat	k5eAaImIp3nS	říkat
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
Golemův	Golemův	k2eAgInSc1d1	Golemův
efekt	efekt	k1gInSc1	efekt
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
pozitivní	pozitivní	k2eAgInSc1d1	pozitivní
Pygmalion	Pygmalion	k1gInSc1	Pygmalion
efekt	efekt	k1gInSc1	efekt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Významná	významný	k2eAgNnPc1d1	významné
díla	dílo	k1gNnPc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Social	Social	k1gMnSc1	Social
Theory	Theora	k1gFnSc2	Theora
and	and	k?	and
Social	Social	k1gMnSc1	Social
Structure	Structur	k1gMnSc5	Structur
(	(	kIx(	(
<g/>
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
On	on	k3xPp3gInSc1	on
the	the	k?	the
Shoulders	Shoulders	k1gInSc1	Shoulders
of	of	k?	of
Giants	Giants	k1gInSc1	Giants
<g/>
:	:	kIx,	:
A	a	k9	a
Shandean	Shandean	k1gMnSc1	Shandean
Postsript	Postsript	k1gMnSc1	Postsript
(	(	kIx(	(
<g/>
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
The	The	k?	The
Sociology	sociolog	k1gMnPc7	sociolog
of	of	k?	of
Science	Scienec	k1gInSc2	Scienec
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sociological	Sociologicat	k5eAaPmAgMnS	Sociologicat
Ambivalence	ambivalence	k1gFnSc2	ambivalence
and	and	k?	and
Other	Other	k1gInSc1	Other
Essays	Essays	k1gInSc1	Essays
(	(	kIx(	(
<g/>
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
On	on	k3xPp3gMnSc1	on
Social	Social	k1gMnSc1	Social
Structure	Structur	k1gMnSc5	Structur
and	and	k?	and
Science	Science	k1gFnSc1	Science
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
HOLTON	HOLTON	kA	HOLTON
<g/>
,	,	kIx,	,
Gerald	Gerald	k1gMnSc1	Gerald
<g/>
.	.	kIx.	.
</s>
<s>
Robert	Robert	k1gMnSc1	Robert
K.	K.	kA	K.
Merton	Merton	k1gInSc1	Merton
<g/>
.	.	kIx.	.
</s>
<s>
Proceedings	Proceedings	k1gInSc1	Proceedings
of	of	k?	of
the	the	k?	the
American	American	k1gInSc4	American
Philosophical	Philosophical	k1gMnSc4	Philosophical
Society	societa	k1gFnSc2	societa
<g/>
.	.	kIx.	.
</s>
<s>
Roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
3049	[number]	k4	3049
<g/>
X.	X.	kA	X.
</s>
</p>
<p>
<s>
ŠUBRT	Šubrt	k1gMnSc1	Šubrt
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
;	;	kIx,	;
BALON	balon	k1gInSc1	balon
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Soudobá	soudobý	k2eAgFnSc1d1	soudobá
sociologická	sociologický	k2eAgFnSc1d1	sociologická
teorie	teorie	k1gFnSc1	teorie
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Grada	Grada	k1gFnSc1	Grada
Publishing	Publishing	k1gInSc1	Publishing
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
232	[number]	k4	232
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
247	[number]	k4	247
<g/>
-	-	kIx~	-
<g/>
2457	[number]	k4	2457
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
CLAMPITT	CLAMPITT	kA	CLAMPITT
<g/>
,	,	kIx,	,
Cynthia	Cynthium	k1gNnSc2	Cynthium
<g/>
.	.	kIx.	.
</s>
<s>
Robert	Robert	k1gMnSc1	Robert
K.	K.	kA	K.
Merton	Merton	k1gInSc1	Merton
<g/>
.	.	kIx.	.
</s>
<s>
Biographical	Biographicat	k5eAaPmAgMnS	Biographicat
Encyclopedia	Encyclopedium	k1gNnSc2	Encyclopedium
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Salem	Salem	k1gInSc1	Salem
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
2016	[number]	k4	2016
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2016	[number]	k4	2016
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
-	-	kIx~	-
<g/>
29	[number]	k4	29
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
GIDDENS	GIDDENS	kA	GIDDENS
<g/>
,	,	kIx,	,
Anthony	Anthon	k1gInPc7	Anthon
<g/>
.	.	kIx.	.
</s>
<s>
Sociologie	sociologie	k1gFnSc1	sociologie
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Argo	Argo	k1gNnSc1	Argo
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
1049	[number]	k4	1049
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
257	[number]	k4	257
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
807	[number]	k4	807
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
BEDNÁŘ	Bednář	k1gMnSc1	Bednář
<g/>
,	,	kIx,	,
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
<g/>
.	.	kIx.	.
</s>
<s>
Sociální	sociální	k2eAgInPc1d1	sociální
vztahy	vztah	k1gInPc1	vztah
v	v	k7c6	v
organizaci	organizace	k1gFnSc6	organizace
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc1	jejich
management	management	k1gInSc1	management
<g/>
.	.	kIx.	.
</s>
<s>
Vyd	Vyd	k?	Vyd
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Grada	Grada	k1gFnSc1	Grada
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
224	[number]	k4	224
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
247	[number]	k4	247
<g/>
-	-	kIx~	-
<g/>
4211	[number]	k4	4211
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
CALHOUN	CALHOUN	kA	CALHOUN
<g/>
,	,	kIx,	,
Craig	Craig	k1gMnSc1	Craig
J.	J.	kA	J.
Robert	Robert	k1gMnSc1	Robert
K.	K.	kA	K.
Merton	Merton	k1gInSc1	Merton
<g/>
:	:	kIx,	:
sociology	sociolog	k1gMnPc4	sociolog
of	of	k?	of
science	scienec	k1gInSc2	scienec
and	and	k?	and
sociology	sociolog	k1gMnPc4	sociolog
as	as	k1gNnSc7	as
science	science	k1gFnSc2	science
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
:	:	kIx,	:
Columbia	Columbia	k1gFnSc1	Columbia
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
c	c	k0	c
<g/>
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
311	[number]	k4	311
<g/>
-	-	kIx~	-
<g/>
5112	[number]	k4	5112
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
DEVITO	DEVITO	kA	DEVITO
<g/>
,	,	kIx,	,
Joseph	Joseph	k1gInSc1	Joseph
A.	A.	kA	A.
Základy	základ	k1gInPc1	základ
mezilidské	mezilidský	k2eAgFnSc2d1	mezilidská
komunikace	komunikace	k1gFnSc2	komunikace
<g/>
:	:	kIx,	:
6	[number]	k4	6
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc6	vydání
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Grada	Grada	k1gFnSc1	Grada
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
247	[number]	k4	247
<g/>
-	-	kIx~	-
<g/>
2018	[number]	k4	2018
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
FISCHER	Fischer	k1gMnSc1	Fischer
<g/>
,	,	kIx,	,
Slavomil	Slavomil	k1gMnSc1	Slavomil
<g/>
;	;	kIx,	;
ŠKODA	Škoda	k1gMnSc1	Škoda
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Sociální	sociální	k2eAgFnSc1d1	sociální
patologie	patologie	k1gFnSc1	patologie
<g/>
:	:	kIx,	:
závažné	závažný	k2eAgInPc1d1	závažný
sociálně	sociálně	k6eAd1	sociálně
patologické	patologický	k2eAgInPc4d1	patologický
jevy	jev	k1gInPc4	jev
<g/>
,	,	kIx,	,
příčiny	příčina	k1gFnPc4	příčina
<g/>
,	,	kIx,	,
prevence	prevence	k1gFnPc4	prevence
<g/>
,	,	kIx,	,
možnosti	možnost	k1gFnPc4	možnost
řešení	řešení	k1gNnPc2	řešení
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
rozš	rozš	k5eAaPmIp2nS	rozš
<g/>
.	.	kIx.	.
a	a	k8xC	a
aktualiz	aktualiz	k1gInSc1	aktualiz
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Grada	Grada	k1gFnSc1	Grada
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
232	[number]	k4	232
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
247	[number]	k4	247
<g/>
-	-	kIx~	-
<g/>
5046	[number]	k4	5046
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MERTON	MERTON	kA	MERTON
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
King	King	k1gMnSc1	King
<g/>
.	.	kIx.	.
</s>
<s>
Studie	studie	k1gFnSc1	studie
ze	z	k7c2	z
sociologické	sociologický	k2eAgFnSc2d1	sociologická
teorie	teorie	k1gFnSc2	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Sociologické	sociologický	k2eAgNnSc1d1	sociologické
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85850	[number]	k4	85850
<g/>
-	-	kIx~	-
<g/>
92	[number]	k4	92
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MERTON	MERTON	kA	MERTON
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
King	King	k1gMnSc1	King
a	a	k8xC	a
SZTOMPKA	SZTOMPKA	kA	SZTOMPKA
<g/>
,	,	kIx,	,
Piotr	Piotr	k1gInSc1	Piotr
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
social	social	k1gMnSc1	social
structure	structur	k1gMnSc5	structur
and	and	k?	and
science	scienka	k1gFnSc6	scienka
<g/>
.	.	kIx.	.
</s>
<s>
Chicago	Chicago	k1gNnSc1	Chicago
:	:	kIx,	:
University	universita	k1gFnPc1	universita
of	of	k?	of
Chicago	Chicago	k1gNnSc1	Chicago
Press	Press	k1gInSc1	Press
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
265	[number]	k4	265
<g/>
-	-	kIx~	-
<g/>
2071	[number]	k4	2071
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
POSSEHL	POSSEHL	kA	POSSEHL
<g/>
,	,	kIx,	,
Gianna	Gianna	k1gFnSc1	Gianna
a	a	k8xC	a
Frank	Frank	k1gMnSc1	Frank
KITTEL	KITTEL	kA	KITTEL
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
prosadit	prosadit	k5eAaPmF	prosadit
a	a	k8xC	a
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
ostatní	ostatní	k2eAgInPc4d1	ostatní
<g/>
:	:	kIx,	:
prosaďte	prosadit	k5eAaPmRp2nP	prosadit
své	svůj	k3xOyFgInPc4	svůj
návrhy	návrh	k1gInPc4	návrh
<g/>
,	,	kIx,	,
nápady	nápad	k1gInPc4	nápad
a	a	k8xC	a
projekty	projekt	k1gInPc4	projekt
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Grada	Grada	k1gFnSc1	Grada
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
247	[number]	k4	247
<g/>
-	-	kIx~	-
<g/>
2677	[number]	k4	2677
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
WALLACE	WALLACE	kA	WALLACE
<g/>
,	,	kIx,	,
Ruth	Ruth	k1gFnPc1	Ruth
A.	A.	kA	A.
a	a	k8xC	a
Alison	Alison	k1gInSc1	Alison
<g/>
.	.	kIx.	.
</s>
<s>
WOLF	Wolf	k1gMnSc1	Wolf
<g/>
.	.	kIx.	.
</s>
<s>
Contemporary	Contemporar	k1gInPc4	Contemporar
sociological	sociologicat	k5eAaPmAgMnS	sociologicat
theory	theora	k1gFnPc4	theora
<g/>
:	:	kIx,	:
Continuing	Continuing	k1gInSc1	Continuing
the	the	k?	the
classical	classicat	k5eAaPmAgInS	classicat
tradition	tradition	k1gInSc1	tradition
<g/>
.	.	kIx.	.
</s>
<s>
Londýn	Londýn	k1gInSc1	Londýn
:	:	kIx,	:
Pearson	Pearson	k1gInSc1	Pearson
College	Colleg	k1gInSc2	Colleg
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
130362452	[number]	k4	130362452
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KELLER	Keller	k1gMnSc1	Keller
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Nedomyšlená	domyšlený	k2eNgFnSc1d1	nedomyšlená
společnost	společnost	k1gFnSc1	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Vyd	Vyd	k?	Vyd
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
:	:	kIx,	:
Doplněk	doplněk	k1gInSc1	doplněk
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85765	[number]	k4	85765
<g/>
-	-	kIx~	-
<g/>
41	[number]	k4	41
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Analyzátor	analyzátor	k1gInSc1	analyzátor
programu	program	k1gInSc2	program
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Robert	Roberta	k1gFnPc2	Roberta
K.	K.	kA	K.
Merton	Merton	k1gInSc1	Merton
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Robert	Robert	k1gMnSc1	Robert
King	King	k1gMnSc1	King
Merton	Merton	k1gInSc4	Merton
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Robert	Robert	k1gMnSc1	Robert
K.	K.	kA	K.
Merton	Merton	k1gInSc1	Merton
</s>
</p>
