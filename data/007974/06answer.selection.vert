<s>
Maria	Maria	k1gFnSc1	Maria
nebo	nebo	k8xC	nebo
Marie	Marie	k1gFnSc1	Marie
<g/>
,	,	kIx,	,
také	také	k9	také
Panna	Panna	k1gFnSc1	Panna
Maria	Maria	k1gFnSc1	Maria
(	(	kIx(	(
<g/>
hebrejsky	hebrejsky	k6eAd1	hebrejsky
Miriam	Miriam	k1gFnSc1	Miriam
<g/>
,	,	kIx,	,
aramejsky	aramejsky	k6eAd1	aramejsky
מ	מ	k?	מ
<g/>
ַ	ַ	k?	ַ
<g/>
ר	ר	k?	ר
<g/>
ְ	ְ	k?	ְ
<g/>
י	י	k?	י
<g/>
ָ	ָ	k?	ָ
<g/>
ם	ם	k?	ם
Marjā	Marjā	k1gFnSc2	Marjā
<g/>
;	;	kIx,	;
řecky	řecky	k6eAd1	řecky
Μ	Μ	k?	Μ
či	či	k8xC	či
Μ	Μ	k?	Μ
<g/>
;	;	kIx,	;
arabsky	arabsky	k6eAd1	arabsky
م	م	k?	م
Marjam	Marjam	k1gInSc1	Marjam
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
křesťanství	křesťanství	k1gNnSc6	křesťanství
a	a	k8xC	a
islámu	islám	k1gInSc6	islám
matka	matka	k1gFnSc1	matka
Ježíše	Ježíš	k1gMnSc2	Ježíš
z	z	k7c2	z
Nazareta	Nazaret	k1gInSc2	Nazaret
<g/>
,	,	kIx,	,
snoubenka	snoubenka	k1gFnSc1	snoubenka
tesaře	tesař	k1gMnSc2	tesař
Josefa	Josef	k1gMnSc2	Josef
<g/>
.	.	kIx.	.
</s>
