<s>
Maria	Maria	k1gFnSc1	Maria
nebo	nebo	k8xC	nebo
Marie	Marie	k1gFnSc1	Marie
<g/>
,	,	kIx,	,
také	také	k9	také
Panna	Panna	k1gFnSc1	Panna
Maria	Maria	k1gFnSc1	Maria
(	(	kIx(	(
<g/>
hebrejsky	hebrejsky	k6eAd1	hebrejsky
Miriam	Miriam	k1gFnSc1	Miriam
<g/>
,	,	kIx,	,
aramejsky	aramejsky	k6eAd1	aramejsky
מ	מ	k?	מ
<g/>
ַ	ַ	k?	ַ
<g/>
ר	ר	k?	ר
<g/>
ְ	ְ	k?	ְ
<g/>
י	י	k?	י
<g/>
ָ	ָ	k?	ָ
<g/>
ם	ם	k?	ם
Marjā	Marjā	k1gFnSc2	Marjā
<g/>
;	;	kIx,	;
řecky	řecky	k6eAd1	řecky
Μ	Μ	k?	Μ
či	či	k8xC	či
Μ	Μ	k?	Μ
<g/>
;	;	kIx,	;
arabsky	arabsky	k6eAd1	arabsky
م	م	k?	م
Marjam	Marjam	k1gInSc1	Marjam
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
křesťanství	křesťanství	k1gNnSc6	křesťanství
a	a	k8xC	a
islámu	islám	k1gInSc6	islám
matka	matka	k1gFnSc1	matka
Ježíše	Ježíš	k1gMnSc2	Ježíš
z	z	k7c2	z
Nazareta	Nazaret	k1gInSc2	Nazaret
<g/>
,	,	kIx,	,
snoubenka	snoubenka	k1gFnSc1	snoubenka
tesaře	tesař	k1gMnSc2	tesař
Josefa	Josef	k1gMnSc2	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Otázka	otázka	k1gFnSc1	otázka
historicity	historicita	k1gFnSc2	historicita
Marie	Maria	k1gFnSc2	Maria
je	být	k5eAaImIp3nS	být
úzce	úzko	k6eAd1	úzko
svázána	svázat	k5eAaPmNgFnS	svázat
se	s	k7c7	s
sporem	spor	k1gInSc7	spor
o	o	k7c4	o
historicitu	historicita	k1gFnSc4	historicita
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
a	a	k8xC	a
důvěryhodností	důvěryhodnost	k1gFnSc7	důvěryhodnost
základních	základní	k2eAgInPc2d1	základní
textů	text	k1gInPc2	text
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
víry	víra	k1gFnSc2	víra
jako	jako	k8xC	jako
historického	historický	k2eAgInSc2d1	historický
pramene	pramen	k1gInSc2	pramen
<g/>
.	.	kIx.	.
</s>
<s>
Historikové	historik	k1gMnPc1	historik
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
přijímají	přijímat	k5eAaImIp3nP	přijímat
svědectví	svědectví	k1gNnSc4	svědectví
evangelií	evangelium	k1gNnPc2	evangelium
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
novozákonních	novozákonní	k2eAgInPc2d1	novozákonní
spisů	spis	k1gInPc2	spis
<g/>
,	,	kIx,	,
přirozeně	přirozeně	k6eAd1	přirozeně
postavu	postava	k1gFnSc4	postava
Marie	Maria	k1gFnSc2	Maria
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
Ježíšovu	Ježíšův	k2eAgFnSc4d1	Ježíšova
matku	matka	k1gFnSc4	matka
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
o	o	k7c6	o
ní	on	k3xPp3gFnSc6	on
tak	tak	k9	tak
zmiňují	zmiňovat	k5eAaImIp3nP	zmiňovat
všechna	všechen	k3xTgNnPc4	všechen
kanonická	kanonický	k2eAgNnPc4d1	kanonické
evangelia	evangelium	k1gNnPc4	evangelium
a	a	k8xC	a
Skutky	skutek	k1gInPc4	skutek
apoštolů	apoštol	k1gMnPc2	apoštol
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
dobových	dobový	k2eAgInPc6d1	dobový
pramenech	pramen	k1gInPc6	pramen
(	(	kIx(	(
<g/>
mimo	mimo	k6eAd1	mimo
Nový	nový	k2eAgInSc1d1	nový
zákon	zákon	k1gInSc1	zákon
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
Maria	Maria	k1gFnSc1	Maria
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
odmítnutím	odmítnutí	k1gNnSc7	odmítnutí
důvěryhodnosti	důvěryhodnost	k1gFnSc2	důvěryhodnost
evangelií	evangelium	k1gNnPc2	evangelium
tak	tak	k6eAd1	tak
lze	lze	k6eAd1	lze
odmítnout	odmítnout	k5eAaPmF	odmítnout
i	i	k9	i
existenci	existence	k1gFnSc4	existence
Marie	Maria	k1gFnSc2	Maria
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jde	jít	k5eAaImIp3nS	jít
spíše	spíše	k9	spíše
o	o	k7c4	o
podružnou	podružný	k2eAgFnSc4d1	podružná
otázku	otázka	k1gFnSc4	otázka
v	v	k7c6	v
širším	široký	k2eAgInSc6d2	širší
sporu	spor	k1gInSc6	spor
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
dostupných	dostupný	k2eAgInPc2d1	dostupný
pramenů	pramen	k1gInPc2	pramen
je	být	k5eAaImIp3nS	být
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
matkou	matka	k1gFnSc7	matka
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
mladém	mladý	k2eAgInSc6d1	mladý
věku	věk	k1gInSc6	věk
(	(	kIx(	(
<g/>
zřejmě	zřejmě	k6eAd1	zřejmě
již	již	k6eAd1	již
ve	v	k7c6	v
13	[number]	k4	13
letech	léto	k1gNnPc6	léto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
Nový	nový	k2eAgInSc1d1	nový
zákon	zákon	k1gInSc1	zákon
neposkytuje	poskytovat	k5eNaImIp3nS	poskytovat
příliš	příliš	k6eAd1	příliš
dalších	další	k2eAgFnPc2d1	další
informací	informace	k1gFnPc2	informace
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
jejího	její	k3xOp3gInSc2	její
života	život	k1gInSc2	život
týkaly	týkat	k5eAaImAgFnP	týkat
<g/>
.	.	kIx.	.
</s>
<s>
Katolická	katolický	k2eAgFnSc1d1	katolická
a	a	k8xC	a
pravoslavná	pravoslavný	k2eAgFnSc1d1	pravoslavná
tradice	tradice	k1gFnSc1	tradice
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
ústním	ústní	k2eAgNnSc6d1	ústní
podání	podání	k1gNnSc6	podání
<g/>
,	,	kIx,	,
svědectví	svědectví	k1gNnSc4	svědectví
církevních	církevní	k2eAgMnPc2d1	církevní
Otců	otec	k1gMnPc2	otec
a	a	k8xC	a
výkladu	výklad	k1gInSc2	výklad
Bible	bible	k1gFnSc2	bible
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Novém	nový	k2eAgInSc6d1	nový
zákoně	zákon	k1gInSc6	zákon
není	být	k5eNaImIp3nS	být
o	o	k7c6	o
Mariině	Mariin	k2eAgNnSc6d1	Mariino
narození	narození	k1gNnSc6	narození
a	a	k8xC	a
rodičích	rodič	k1gMnPc6	rodič
žádná	žádný	k3yNgFnSc1	žádný
zmínka	zmínka	k1gFnSc1	zmínka
<g/>
.	.	kIx.	.
</s>
<s>
Jakubovo	Jakubův	k2eAgNnSc1d1	Jakubovo
protoevangelium	protoevangelium	k1gNnSc1	protoevangelium
ze	z	k7c2	z
2	[number]	k4	2
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc1	jehož
autorství	autorství	k1gNnSc1	autorství
je	být	k5eAaImIp3nS	být
připisováno	připisovat	k5eAaImNgNnS	připisovat
Jakubu	Jakub	k1gMnSc3	Jakub
Alfeovu	Alfeův	k2eAgMnSc3d1	Alfeův
<g/>
,	,	kIx,	,
považované	považovaný	k2eAgNnSc1d1	považované
církevní	církevní	k2eAgFnSc7d1	církevní
tradicí	tradice	k1gFnSc7	tradice
za	za	k7c4	za
apokryf	apokryf	k1gInSc4	apokryf
<g/>
,	,	kIx,	,
popisuje	popisovat	k5eAaImIp3nS	popisovat
Mariino	Mariin	k2eAgNnSc1d1	Mariino
dětství	dětství	k1gNnSc1	dětství
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
se	se	k3xPyFc4	se
také	také	k9	také
dovídáme	dovídat	k5eAaImIp1nP	dovídat
o	o	k7c6	o
jejích	její	k3xOp3gMnPc6	její
rodičích	rodič	k1gMnPc6	rodič
<g/>
,	,	kIx,	,
Jáchymovi	Jáchym	k1gMnSc3	Jáchym
a	a	k8xC	a
Anně	Anna	k1gFnSc3	Anna
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
Joachim	Joachima	k1gFnPc2	Joachima
je	být	k5eAaImIp3nS	být
vykládáno	vykládat	k5eAaImNgNnS	vykládat
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
příprava	příprava	k1gFnSc1	příprava
na	na	k7c4	na
Pána	pán	k1gMnSc4	pán
<g/>
"	"	kIx"	"
a	a	k8xC	a
jméno	jméno	k1gNnSc4	jméno
Anny	Anna	k1gFnSc2	Anna
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
milostná	milostný	k2eAgFnSc1d1	milostná
<g/>
,	,	kIx,	,
laskavá	laskavý	k2eAgFnSc1d1	laskavá
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Výtěžek	výtěžek	k1gInSc1	výtěžek
svého	svůj	k3xOyFgNnSc2	svůj
hospodářství	hospodářství	k1gNnSc2	hospodářství
spravedlivě	spravedlivě	k6eAd1	spravedlivě
dělili	dělit	k5eAaImAgMnP	dělit
mezi	mezi	k7c4	mezi
jeruzalémský	jeruzalémský	k2eAgInSc4d1	jeruzalémský
chrám	chrám	k1gInSc4	chrám
<g/>
,	,	kIx,	,
pomoc	pomoc	k1gFnSc4	pomoc
chudým	chudý	k2eAgNnSc7d1	chudé
a	a	k8xC	a
vlastní	vlastní	k2eAgFnPc4d1	vlastní
potřeby	potřeba	k1gFnPc4	potřeba
<g/>
.	.	kIx.	.
</s>
<s>
Byli	být	k5eAaImAgMnP	být
však	však	k9	však
považováni	považován	k2eAgMnPc1d1	považován
za	za	k7c4	za
neplodné	plodný	k2eNgFnPc4d1	neplodná
a	a	k8xC	a
chrámoví	chrámový	k2eAgMnPc1d1	chrámový
kněží	kněz	k1gMnPc1	kněz
proto	proto	k8xC	proto
dokonce	dokonce	k9	dokonce
jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
přijmout	přijmout	k5eAaPmF	přijmout
oběti	oběť	k1gFnPc4	oběť
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
chrámu	chrám	k1gInSc6	chrám
přinesli	přinést	k5eAaPmAgMnP	přinést
<g/>
.	.	kIx.	.
</s>
<s>
Hluboce	hluboko	k6eAd1	hluboko
ponížený	ponížený	k2eAgMnSc1d1	ponížený
Jáchym	Jáchym	k1gMnSc1	Jáchym
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
pouště	poušť	k1gFnSc2	poušť
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
čtyřicet	čtyřicet	k4xCc1	čtyřicet
dní	den	k1gInPc2	den
postil	postila	k1gFnPc2	postila
a	a	k8xC	a
modlil	modlit	k5eAaImAgMnS	modlit
k	k	k7c3	k
Bohu	bůh	k1gMnSc3	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
čtyřiceti	čtyřicet	k4xCc6	čtyřicet
dnech	den	k1gInPc6	den
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
zjevil	zjevit	k5eAaPmAgMnS	zjevit
anděl	anděl	k1gMnSc1	anděl
a	a	k8xC	a
oznámil	oznámit	k5eAaPmAgMnS	oznámit
mu	on	k3xPp3gMnSc3	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnPc1	jeho
modlitby	modlitba	k1gFnPc1	modlitba
byly	být	k5eAaImAgFnP	být
vyslyšeny	vyslyšet	k5eAaPmNgFnP	vyslyšet
<g/>
,	,	kIx,	,
a	a	k8xC	a
slíbil	slíbit	k5eAaPmAgMnS	slíbit
jim	on	k3xPp3gMnPc3	on
narození	narození	k1gNnSc2	narození
dítěte	dítě	k1gNnSc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vysokém	vysoký	k2eAgInSc6d1	vysoký
věku	věk	k1gInSc6	věk
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
pak	pak	k6eAd1	pak
narodila	narodit	k5eAaPmAgFnS	narodit
dcera	dcera	k1gFnSc1	dcera
<g/>
,	,	kIx,	,
které	který	k3yIgFnSc3	který
dali	dát	k5eAaPmAgMnP	dát
jméno	jméno	k1gNnSc4	jméno
Marie	Maria	k1gFnSc2	Maria
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
zemřel	zemřít	k5eAaPmAgMnS	zemřít
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
jejím	její	k3xOp3gNnSc6	její
narození	narození	k1gNnSc6	narození
a	a	k8xC	a
Marie	Marie	k1gFnSc1	Marie
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
třech	tři	k4xCgNnPc6	tři
letech	léto	k1gNnPc6	léto
předána	předat	k5eAaPmNgFnS	předat
do	do	k7c2	do
výchovy	výchova	k1gFnSc2	výchova
správě	správa	k1gFnSc3	správa
jeruzalémského	jeruzalémský	k2eAgInSc2d1	jeruzalémský
chrámu	chrám	k1gInSc2	chrám
<g/>
.	.	kIx.	.
</s>
<s>
Scény	scéna	k1gFnPc4	scéna
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
příběhu	příběh	k1gInSc2	příběh
byly	být	k5eAaImAgInP	být
velmi	velmi	k6eAd1	velmi
oblíbeným	oblíbený	k2eAgNnSc7d1	oblíbené
tématem	téma	k1gNnSc7	téma
malířů	malíř	k1gMnPc2	malíř
až	až	k9	až
do	do	k7c2	do
Tridentského	tridentský	k2eAgInSc2d1	tridentský
koncilu	koncil	k1gInSc2	koncil
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1545	[number]	k4	1545
až	až	k9	až
1563	[number]	k4	1563
<g/>
,	,	kIx,	,
definoval	definovat	k5eAaBmAgInS	definovat
katolický	katolický	k2eAgInSc1d1	katolický
biblický	biblický	k2eAgInSc1d1	biblický
kánon	kánon	k1gInSc1	kánon
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
zakázal	zakázat	k5eAaPmAgInS	zakázat
zobrazování	zobrazování	k1gNnSc4	zobrazování
apokryfních	apokryfní	k2eAgFnPc2d1	apokryfní
scén	scéna	k1gFnPc2	scéna
<g/>
.	.	kIx.	.
</s>
<s>
Svatí	svatý	k1gMnPc1	svatý
Jáchym	Jáchym	k1gMnSc1	Jáchym
a	a	k8xC	a
Anna	Anna	k1gFnSc1	Anna
však	však	k9	však
zůstali	zůstat	k5eAaPmAgMnP	zůstat
v	v	k7c6	v
katolickém	katolický	k2eAgInSc6d1	katolický
kalendáři	kalendář	k1gInSc6	kalendář
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
příbuznou	příbuzná	k1gFnSc7	příbuzná
Alžbětinou	Alžbětin	k2eAgFnSc7d1	Alžbětina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
pocházela	pocházet	k5eAaImAgFnS	pocházet
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Árónova	Árónův	k2eAgInSc2d1	Árónův
<g/>
.	.	kIx.	.
</s>
<s>
Bydlela	bydlet	k5eAaImAgFnS	bydlet
v	v	k7c6	v
Nazaretě	Nazaret	k1gInSc6	Nazaret
s	s	k7c7	s
rodiči	rodič	k1gMnPc7	rodič
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
zasnoubena	zasnoubit	k5eAaPmNgFnS	zasnoubit
Josefovi	Josef	k1gMnSc3	Josef
z	z	k7c2	z
Nazaretu	Nazaret	k1gInSc2	Nazaret
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
Maria	Mario	k1gMnSc4	Mario
pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
židovského	židovský	k2eAgMnSc2d1	židovský
krále	král	k1gMnSc2	král
Davida	David	k1gMnSc2	David
<g/>
.	.	kIx.	.
6	[number]	k4	6
měsíců	měsíc	k1gInPc2	měsíc
po	po	k7c6	po
Alžbětině	Alžbětin	k2eAgNnSc6d1	Alžbětino
početí	početí	k1gNnSc6	početí
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Marii	Maria	k1gFnSc4	Maria
zjevil	zjevit	k5eAaPmAgMnS	zjevit
archanděl	archanděl	k1gMnSc1	archanděl
Gabriel	Gabriel	k1gMnSc1	Gabriel
a	a	k8xC	a
pozdravil	pozdravit	k5eAaPmAgMnS	pozdravit
ji	on	k3xPp3gFnSc4	on
slovy	slovo	k1gNnPc7	slovo
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
základem	základ	k1gInSc7	základ
modlitby	modlitba	k1gFnPc4	modlitba
používané	používaný	k2eAgFnPc1d1	používaná
dodnes	dodnes	k6eAd1	dodnes
<g />
.	.	kIx.	.
</s>
<s>
mezi	mezi	k7c7	mezi
vyznavači	vyznavač	k1gMnPc7	vyznavač
marianského	marianský	k2eAgInSc2d1	marianský
kultu	kult	k1gInSc2	kult
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Buď	buď	k8xC	buď
zdráva	zdrávo	k1gNnSc2	zdrávo
<g/>
,	,	kIx,	,
milostí	milost	k1gFnPc2	milost
zahrnutá	zahrnutý	k2eAgFnSc1d1	zahrnutá
<g/>
,	,	kIx,	,
Pán	pán	k1gMnSc1	pán
s	s	k7c7	s
tebou	ty	k3xPp2nSc7	ty
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Lk	Lk	k1gFnSc1	Lk
1	[number]	k4	1
<g/>
,	,	kIx,	,
28	[number]	k4	28
(	(	kIx(	(
<g/>
Kral	Kral	k1gMnSc1	Kral
<g/>
,	,	kIx,	,
ČEP	Čep	k1gMnSc1	Čep
<g/>
))	))	k?	))
a	a	k8xC	a
oznámil	oznámit	k5eAaPmAgMnS	oznámit
jí	on	k3xPp3gFnSc3	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
matkou	matka	k1gFnSc7	matka
zaslíbeného	zaslíbený	k1gMnSc2	zaslíbený
Mesiáše	Mesiáš	k1gMnSc2	Mesiáš
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
zůstane	zůstat	k5eAaPmIp3nS	zůstat
pannou	panna	k1gFnSc7	panna
<g/>
.	.	kIx.	.
</s>
<s>
Maria	Maria	k1gFnSc1	Maria
se	se	k3xPyFc4	se
podrobila	podrobit	k5eAaPmAgFnS	podrobit
Boží	boží	k2eAgFnSc4d1	boží
vůli	vůle	k1gFnSc4	vůle
se	s	k7c7	s
slovy	slovo	k1gNnPc7	slovo
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Hle	hle	k0	hle
<g/>
,	,	kIx,	,
jsem	být	k5eAaImIp1nS	být
služebnice	služebnice	k1gFnSc1	služebnice
Páně	páně	k2eAgFnSc1d1	páně
<g/>
;	;	kIx,	;
staň	stanout	k5eAaPmRp2nS	stanout
se	se	k3xPyFc4	se
mi	já	k3xPp1nSc3	já
podle	podle	k7c2	podle
tvého	tvůj	k3xOp2gNnSc2	tvůj
slova	slovo	k1gNnSc2	slovo
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Lk	Lk	k1gFnSc1	Lk
1	[number]	k4	1
<g/>
,	,	kIx,	,
38	[number]	k4	38
(	(	kIx(	(
<g/>
Kral	Kral	k1gMnSc1	Kral
<g/>
,	,	kIx,	,
ČEP	Čep	k1gMnSc1	Čep
<g/>
))	))	k?	))
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
vydala	vydat	k5eAaPmAgFnS	vydat
na	na	k7c4	na
dalekou	daleký	k2eAgFnSc4d1	daleká
cestu	cesta	k1gFnSc4	cesta
navštívit	navštívit	k5eAaPmF	navštívit
svoji	svůj	k3xOyFgFnSc4	svůj
těhotnou	těhotný	k2eAgFnSc4d1	těhotná
příbuznou	příbuzný	k2eAgFnSc4d1	příbuzná
Alžbetu	Alžbeta	k1gFnSc4	Alžbeta
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
žila	žít	k5eAaImAgFnS	žít
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
manželem	manžel	k1gMnSc7	manžel
Zachariášem	Zachariáš	k1gMnSc7	Zachariáš
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
jak	jak	k6eAd1	jak
vešla	vejít	k5eAaPmAgFnS	vejít
do	do	k7c2	do
jejich	jejich	k3xOp3gInSc2	jejich
domu	dům	k1gInSc2	dům
a	a	k8xC	a
pozdravila	pozdravit	k5eAaPmAgFnS	pozdravit
Alžbetu	Alžbeta	k1gFnSc4	Alžbeta
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
jí	on	k3xPp3gFnSc3	on
vzdala	vzdát	k5eAaPmAgFnS	vzdát
úctu	úcta	k1gFnSc4	úcta
jako	jako	k8xC	jako
matce	matka	k1gFnSc3	matka
svého	svůj	k3xOyFgMnSc2	svůj
Pána	pán	k1gMnSc2	pán
a	a	k8xC	a
Maria	Maria	k1gFnSc1	Maria
jí	on	k3xPp3gFnSc3	on
odpověděla	odpovědět	k5eAaPmAgFnS	odpovědět
chvalozpěvem	chvalozpěv	k1gInSc7	chvalozpěv
na	na	k7c4	na
Pána	pán	k1gMnSc4	pán
známým	známý	k2eAgNnSc7d1	známé
jako	jako	k8xS	jako
Magnificat	Magnificat	k1gNnSc7	Magnificat
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Lk	Lk	k1gFnSc1	Lk
1	[number]	k4	1
<g/>
,	,	kIx,	,
46	[number]	k4	46
<g/>
–	–	k?	–
<g/>
55	[number]	k4	55
(	(	kIx(	(
<g/>
Kral	Kral	k1gMnSc1	Kral
<g/>
,	,	kIx,	,
ČEP	Čep	k1gMnSc1	Čep
<g/>
))	))	k?	))
Po	po	k7c6	po
třech	tři	k4xCgInPc6	tři
měsících	měsíc	k1gInPc6	měsíc
se	se	k3xPyFc4	se
Maria	Maria	k1gFnSc1	Maria
vrátila	vrátit	k5eAaPmAgFnS	vrátit
domů	domů	k6eAd1	domů
do	do	k7c2	do
Nazaretu	Nazaret	k1gInSc2	Nazaret
<g/>
.	.	kIx.	.
</s>
<s>
Jejímu	její	k3xOp3gMnSc3	její
snoubenci	snoubenec	k1gMnSc3	snoubenec
Josefovi	Josef	k1gMnSc3	Josef
se	se	k3xPyFc4	se
zjevil	zjevit	k5eAaPmAgMnS	zjevit
ve	v	k7c6	v
snu	sen	k1gInSc6	sen
anděl	anděl	k1gMnSc1	anděl
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
mu	on	k3xPp3gMnSc3	on
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Josefe	Josef	k1gMnSc5	Josef
<g/>
,	,	kIx,	,
synu	syn	k1gMnSc3	syn
Davidův	Davidův	k2eAgMnSc1d1	Davidův
<g/>
,	,	kIx,	,
neboj	bát	k5eNaImRp2nS	bát
se	se	k3xPyFc4	se
přijmout	přijmout	k5eAaPmF	přijmout
Marii	Maria	k1gFnSc3	Maria
<g/>
,	,	kIx,	,
svou	svůj	k3xOyFgFnSc4	svůj
manželku	manželka	k1gFnSc4	manželka
<g/>
;	;	kIx,	;
neboť	neboť	k8xC	neboť
co	co	k3yRnSc1	co
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
bylo	být	k5eAaImAgNnS	být
počato	počnout	k5eAaPmNgNnS	počnout
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
Ducha	duch	k1gMnSc2	duch
svatého	svatý	k2eAgMnSc2d1	svatý
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Mt	Mt	k1gFnSc1	Mt
1	[number]	k4	1
<g/>
,	,	kIx,	,
20	[number]	k4	20
(	(	kIx(	(
<g/>
Kral	Kral	k1gMnSc1	Kral
<g/>
,	,	kIx,	,
ČEP	Čep	k1gMnSc1	Čep
<g/>
))	))	k?	))
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
pak	pak	k6eAd1	pak
vzal	vzít	k5eAaPmAgMnS	vzít
k	k	k7c3	k
sobě	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
navzdory	navzdory	k7c3	navzdory
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
byla	být	k5eAaImAgFnS	být
těhotná	těhotný	k2eAgFnSc1d1	těhotná
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
Augustův	Augustův	k2eAgInSc1d1	Augustův
výnos	výnos	k1gInSc1	výnos
o	o	k7c4	o
sčítání	sčítání	k1gNnSc4	sčítání
lidu	lid	k1gInSc2	lid
stanovil	stanovit	k5eAaPmAgInS	stanovit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
každý	každý	k3xTgMnSc1	každý
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
svého	svůj	k3xOyFgNnSc2	svůj
rodného	rodný	k2eAgNnSc2d1	rodné
města	město	k1gNnSc2	město
a	a	k8xC	a
dal	dát	k5eAaPmAgMnS	dát
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
zapsat	zapsat	k5eAaPmF	zapsat
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
Josef	Josef	k1gMnSc1	Josef
s	s	k7c7	s
těhotnou	těhotná	k1gFnSc7	těhotná
Marii	Maria	k1gFnSc4	Maria
vydal	vydat	k5eAaPmAgInS	vydat
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
Betléma	Betlém	k1gInSc2	Betlém
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hostinci	hostinec	k1gInSc6	hostinec
nenašli	najít	k5eNaPmAgMnP	najít
ubytování	ubytování	k1gNnSc4	ubytování
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
plné	plný	k2eAgNnSc1d1	plné
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
byli	být	k5eAaImAgMnP	být
nuceni	nutit	k5eAaImNgMnP	nutit
přespat	přespat	k5eAaPmF	přespat
ve	v	k7c6	v
chlévě	chlév	k1gInSc6	chlév
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Ježíš	Ježíš	k1gMnSc1	Ježíš
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
Betlémě	Betlém	k1gInSc6	Betlém
(	(	kIx(	(
<g/>
Lk	Lk	k1gFnSc1	Lk
2	[number]	k4	2
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
(	(	kIx(	(
<g/>
Kral	Kral	k1gMnSc1	Kral
<g/>
,	,	kIx,	,
ČEP	Čep	k1gMnSc1	Čep
<g/>
))	))	k?	))
<g/>
.	.	kIx.	.
</s>
<s>
Následují	následovat	k5eAaImIp3nP	následovat
přinesení	přinesení	k1gNnSc4	přinesení
Ježíše	Ježíš	k1gMnSc2	Ježíš
do	do	k7c2	do
chrámu	chrám	k1gInSc2	chrám
<g/>
,	,	kIx,	,
útěk	útěk	k1gInSc4	útěk
do	do	k7c2	do
Egypta	Egypt	k1gInSc2	Egypt
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
král	král	k1gMnSc1	král
Herodes	Herodes	k1gMnSc1	Herodes
usiloval	usilovat	k5eAaImAgMnS	usilovat
Ježíšovi	Ježíš	k1gMnSc3	Ježíš
o	o	k7c4	o
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
návrat	návrat	k1gInSc4	návrat
zpět	zpět	k6eAd1	zpět
a	a	k8xC	a
usídlení	usídlení	k1gNnSc1	usídlení
se	se	k3xPyFc4	se
v	v	k7c6	v
Nazaretu	Nazaret	k1gInSc6	Nazaret
(	(	kIx(	(
<g/>
Mt	Mt	k1gFnSc1	Mt
2	[number]	k4	2
<g/>
,	,	kIx,	,
13	[number]	k4	13
<g/>
–	–	k?	–
<g/>
23	[number]	k4	23
(	(	kIx(	(
<g/>
Kral	Kral	k1gMnSc1	Kral
<g/>
,	,	kIx,	,
ČEP	Čep	k1gMnSc1	Čep
<g/>
))	))	k?	))
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dalších	další	k2eAgNnPc2d1	další
30	[number]	k4	30
let	léto	k1gNnPc2	léto
života	život	k1gInSc2	život
zaznamenávají	zaznamenávat	k5eAaImIp3nP	zaznamenávat
evangelia	evangelium	k1gNnPc4	evangelium
pouze	pouze	k6eAd1	pouze
pouť	pouť	k1gFnSc4	pouť
do	do	k7c2	do
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
<g/>
,	,	kIx,	,
když	když	k8xS	když
bylo	být	k5eAaImAgNnS	být
Ježíšovi	Ježíšův	k2eAgMnPc1d1	Ježíšův
12	[number]	k4	12
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Marii	Maria	k1gFnSc3	Maria
pak	pak	k6eAd1	pak
nalézáme	nalézat	k5eAaImIp1nP	nalézat
<g/>
,	,	kIx,	,
když	když	k8xS	když
Ježíš	Ježíš	k1gMnSc1	Ježíš
začíná	začínat	k5eAaImIp3nS	začínat
veřejně	veřejně	k6eAd1	veřejně
působit	působit	k5eAaImF	působit
–	–	k?	–
při	při	k7c6	při
svatbě	svatba	k1gFnSc6	svatba
v	v	k7c6	v
galilejské	galilejský	k2eAgFnSc6d1	Galilejská
vesnici	vesnice	k1gFnSc6	vesnice
zvané	zvaný	k2eAgFnSc2d1	zvaná
Kána	kát	k5eAaImNgNnP	kát
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
po	po	k7c6	po
Mariině	Mariin	k2eAgNnSc6d1	Mariino
upozornění	upozornění	k1gNnSc6	upozornění
<g/>
,	,	kIx,	,
že	že	k8xS	že
došlo	dojít	k5eAaPmAgNnS	dojít
víno	víno	k1gNnSc4	víno
<g/>
,	,	kIx,	,
činí	činit	k5eAaImIp3nS	činit
Ježíš	ježit	k5eAaImIp2nS	ježit
první	první	k4xOgInSc1	první
zázrak	zázrak	k1gInSc1	zázrak
–	–	k?	–
proměňuje	proměňovat	k5eAaImIp3nS	proměňovat
vodu	voda	k1gFnSc4	voda
ve	v	k7c4	v
víno	víno	k1gNnSc4	víno
(	(	kIx(	(
<g/>
Jan	Jan	k1gMnSc1	Jan
2	[number]	k4	2
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
12	[number]	k4	12
(	(	kIx(	(
<g/>
Kral	Kral	k1gMnSc1	Kral
<g/>
,	,	kIx,	,
ČEP	Čep	k1gMnSc1	Čep
<g/>
))	))	k?	))
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ostatními	ostatní	k2eAgMnPc7d1	ostatní
učedníky	učedník	k1gMnPc7	učedník
plakala	plakat	k5eAaImAgFnS	plakat
pod	pod	k7c7	pod
křížem	kříž	k1gInSc7	kříž
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yQgNnSc6	který
byl	být	k5eAaImAgMnS	být
její	její	k3xOp3gMnSc1	její
syn	syn	k1gMnSc1	syn
Ježíš	ježit	k5eAaImIp2nS	ježit
ukřižován	ukřižován	k2eAgMnSc1d1	ukřižován
<g/>
.	.	kIx.	.
<g/>
(	(	kIx(	(
<g/>
J	J	kA	J
19	[number]	k4	19
<g/>
,	,	kIx,	,
24	[number]	k4	24
<g/>
–	–	k?	–
<g/>
26	[number]	k4	26
(	(	kIx(	(
<g/>
Kral	Kral	k1gMnSc1	Kral
<g/>
,	,	kIx,	,
ČEP	Čep	k1gMnSc1	Čep
<g/>
))	))	k?	))
<g/>
.	.	kIx.	.
</s>
<s>
Motiv	motiv	k1gInSc1	motiv
Marie	Maria	k1gFnSc2	Maria
s	s	k7c7	s
mrtvým	mrtvý	k2eAgNnSc7d1	mrtvé
Ježíšovým	Ježíšův	k2eAgNnSc7d1	Ježíšovo
tělem	tělo	k1gNnSc7	tělo
na	na	k7c6	na
klíně	klín	k1gInSc6	klín
je	být	k5eAaImIp3nS	být
častý	častý	k2eAgInSc1d1	častý
v	v	k7c6	v
umění	umění	k1gNnSc6	umění
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Pieta	pieta	k1gFnSc1	pieta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
Marii	Maria	k1gFnSc6	Maria
v	v	k7c6	v
bibli	bible	k1gFnSc6	bible
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
popisu	popis	k1gInSc2	popis
očekávání	očekávání	k1gNnSc2	očekávání
slíbeného	slíbený	k2eAgMnSc2d1	slíbený
Ducha	duch	k1gMnSc2	duch
svatého	svatý	k1gMnSc2	svatý
(	(	kIx(	(
<g/>
Sk	Sk	kA	Sk
1	[number]	k4	1
<g/>
,	,	kIx,	,
14	[number]	k4	14
(	(	kIx(	(
<g/>
Kral	Kral	k1gMnSc1	Kral
<g/>
,	,	kIx,	,
ČEP	Čep	k1gMnSc1	Čep
<g/>
))	))	k?	))
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
s	s	k7c7	s
apoštoly	apoštol	k1gMnPc7	apoštol
modlila	modlit	k5eAaImAgFnS	modlit
po	po	k7c6	po
Ježíšově	Ježíšův	k2eAgNnSc6d1	Ježíšovo
nanebevstoupení	nanebevstoupení	k1gNnSc6	nanebevstoupení
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc4	její
smrt	smrt	k1gFnSc4	smrt
a	a	k8xC	a
nanebevzetí	nanebevzetí	k1gNnSc4	nanebevzetí
Nový	nový	k2eAgInSc1d1	nový
zákon	zákon	k1gInSc1	zákon
nezaznamenává	zaznamenávat	k5eNaImIp3nS	zaznamenávat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
křesťanských	křesťanský	k2eAgFnPc6d1	křesťanská
církvích	církev	k1gFnPc6	církev
se	se	k3xPyFc4	se
pohled	pohled	k1gInSc1	pohled
na	na	k7c4	na
Marii	Maria	k1gFnSc4	Maria
různí	různit	k5eAaImIp3nP	různit
<g/>
.	.	kIx.	.
</s>
<s>
Jednotné	jednotný	k2eAgFnPc1d1	jednotná
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
názoru	názor	k1gInSc6	názor
na	na	k7c4	na
Mariino	Mariin	k2eAgNnSc4d1	Mariino
panenství	panenství	k1gNnSc4	panenství
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
také	také	k9	také
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
označení	označení	k1gNnSc4	označení
Ježíšovy	Ježíšův	k2eAgFnSc2d1	Ježíšova
matky	matka	k1gFnSc2	matka
výraz	výraz	k1gInSc1	výraz
Panna	Panna	k1gFnSc1	Panna
Maria	Maria	k1gFnSc1	Maria
<g/>
.	.	kIx.	.
</s>
<s>
Kritickým	kritický	k2eAgNnSc7d1	kritické
zkoumáním	zkoumání	k1gNnSc7	zkoumání
Mariiny	Mariin	k2eAgFnSc2d1	Mariina
postavy	postava	k1gFnSc2	postava
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaPmIp3nS	věnovat
zvláštní	zvláštní	k2eAgFnSc1d1	zvláštní
oblast	oblast	k1gFnSc1	oblast
teologie	teologie	k1gFnSc1	teologie
<g/>
,	,	kIx,	,
zvaná	zvaný	k2eAgFnSc1d1	zvaná
mariologie	mariologie	k1gFnSc1	mariologie
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Neposkvrněné	poskvrněný	k2eNgNnSc1d1	neposkvrněné
početí	početí	k1gNnSc1	početí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
.	.	kIx.	.
</s>
<s>
Nauka	nauka	k1gFnSc1	nauka
o	o	k7c6	o
neposkvrněném	poskvrněný	k2eNgNnSc6d1	neposkvrněné
početí	početí	k1gNnSc6	početí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Maria	Maria	k1gFnSc1	Maria
nebyla	být	k5eNaImAgFnS	být
při	při	k7c6	při
početí	početí	k1gNnSc6	početí
(	(	kIx(	(
<g/>
svatou	svatý	k2eAgFnSc4d1	svatá
Annou	Anna	k1gFnSc7	Anna
a	a	k8xC	a
svatým	svatý	k1gMnSc7	svatý
Jáchymem	Jáchym	k1gMnSc7	Jáchym
<g/>
)	)	kIx)	)
poskvrněna	poskvrněn	k2eAgNnPc4d1	poskvrněno
dědičným	dědičný	k2eAgInSc7d1	dědičný
prvotním	prvotní	k2eAgInSc7d1	prvotní
hříchem	hřích	k1gInSc7	hřích
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
katolické	katolický	k2eAgFnSc6d1	katolická
církvi	církev	k1gFnSc6	církev
je	být	k5eAaImIp3nS	být
uctíváno	uctíván	k2eAgNnSc1d1	uctíváno
a	a	k8xC	a
slaveno	slaven	k2eAgNnSc1d1	slaveno
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
<g/>
)	)	kIx)	)
od	od	k7c2	od
středověku	středověk	k1gInSc2	středověk
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1854	[number]	k4	1854
vyhlášeno	vyhlásit	k5eAaPmNgNnS	vyhlásit
za	za	k7c4	za
církevní	církevní	k2eAgNnSc4d1	církevní
dogma	dogma	k1gNnSc4	dogma
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
ostatních	ostatní	k2eAgFnPc2d1	ostatní
církví	církev	k1gFnPc2	církev
neposkvrněné	poskvrněný	k2eNgNnSc1d1	neposkvrněné
početí	početí	k1gNnSc1	početí
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
nemá	mít	k5eNaImIp3nS	mít
přímou	přímý	k2eAgFnSc4d1	přímá
oporu	opora	k1gFnSc4	opora
v	v	k7c6	v
bibli	bible	k1gFnSc6	bible
<g/>
,	,	kIx,	,
neuznává	uznávat	k5eNaImIp3nS	uznávat
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Panenské	panenský	k2eAgNnSc1d1	panenské
početí	početí	k1gNnSc1	početí
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
<g/>
.	.	kIx.	.
</s>
<s>
Křesťané	křesťan	k1gMnPc1	křesťan
věří	věřit	k5eAaImIp3nP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Maria	Maria	k1gFnSc1	Maria
počala	počnout	k5eAaPmAgFnS	počnout
Ježíše	Ježíš	k1gMnPc4	Ježíš
jako	jako	k9	jako
panna	panna	k1gFnSc1	panna
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnSc2	některý
církve	církev	k1gFnSc2	církev
(	(	kIx(	(
<g/>
např.	např.	kA	např.
římskokatolická	římskokatolický	k2eAgNnPc1d1	římskokatolické
či	či	k8xC	či
pravoslavná	pravoslavný	k2eAgNnPc1d1	pravoslavné
<g/>
)	)	kIx)	)
vyznávají	vyznávat	k5eAaImIp3nP	vyznávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Maria	Maria	k1gFnSc1	Maria
pannou	panna	k1gFnSc7	panna
zůstala	zůstat	k5eAaPmAgNnP	zůstat
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Mariino	Mariin	k2eAgNnSc1d1	Mariino
panenské	panenský	k2eAgNnSc1d1	panenské
početí	početí	k1gNnSc1	početí
Ježíše	Ježíš	k1gMnSc2	Ježíš
je	být	k5eAaImIp3nS	být
historicky	historicky	k6eAd1	historicky
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejlépe	dobře	k6eAd3	dobře
doložených	doložený	k2eAgInPc2d1	doložený
mariologických	mariologický	k2eAgInPc2d1	mariologický
článků	článek	k1gInPc2	článek
víry	víra	k1gFnSc2	víra
<g/>
,	,	kIx,	,
nacházíme	nacházet	k5eAaImIp1nP	nacházet
je	on	k3xPp3gFnPc4	on
třeba	třeba	k6eAd1	třeba
již	již	k6eAd1	již
v	v	k7c6	v
Apoštolském	apoštolský	k2eAgNnSc6d1	apoštolské
vyznání	vyznání	k1gNnSc6	vyznání
víry	víra	k1gFnSc2	víra
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
raně	raně	k6eAd1	raně
křesťanských	křesťanský	k2eAgInPc6d1	křesťanský
symbolech	symbol	k1gInPc6	symbol
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
dogma	dogma	k1gNnSc1	dogma
bylo	být	k5eAaImAgNnS	být
definováno	definovat	k5eAaBmNgNnS	definovat
na	na	k7c6	na
Prvním	první	k4xOgMnSc7	první
konstantinopolském	konstantinopolský	k2eAgInSc6d1	konstantinopolský
(	(	kIx(	(
<g/>
381	[number]	k4	381
<g/>
)	)	kIx)	)
a	a	k8xC	a
chalkedonském	chalkedonský	k2eAgInSc6d1	chalkedonský
koncilu	koncil	k1gInSc6	koncil
(	(	kIx(	(
<g/>
451	[number]	k4	451
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Skrze	Skrze	k?	Skrze
Ducha	duch	k1gMnSc2	duch
svatého	svatý	k1gMnSc2	svatý
přijal	přijmout	k5eAaPmAgMnS	přijmout
tělo	tělo	k1gNnSc4	tělo
z	z	k7c2	z
Marie	Maria	k1gFnSc2	Maria
Panny	Panna	k1gFnSc2	Panna
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
člověkem	člověk	k1gMnSc7	člověk
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Liturgicky	liturgicky	k6eAd1	liturgicky
však	však	k9	však
panenství	panenství	k1gNnSc1	panenství
Mariino	Mariin	k2eAgNnSc1d1	Mariino
nemá	mít	k5eNaImIp3nS	mít
vlastní	vlastní	k2eAgInSc4d1	vlastní
svátek	svátek	k1gInSc4	svátek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
církvích	církev	k1gFnPc6	církev
uznávajících	uznávající	k2eAgFnPc6d1	uznávající
Mariino	Mariin	k2eAgNnSc4d1	Mariino
"	"	kIx"	"
<g/>
věčné	věčný	k2eAgNnSc4d1	věčné
panenství	panenství	k1gNnSc4	panenství
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
Maria	Maria	k1gFnSc1	Maria
označována	označovat	k5eAaImNgFnS	označovat
též	též	k9	též
titulem	titul	k1gInSc7	titul
vždy	vždy	k6eAd1	vždy
panna	panna	k1gFnSc1	panna
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
konstantinopolský	konstantinopolský	k2eAgInSc1d1	konstantinopolský
koncil	koncil	k1gInSc1	koncil
(	(	kIx(	(
<g/>
553	[number]	k4	553
<g/>
)	)	kIx)	)
již	již	k6eAd1	již
užívá	užívat	k5eAaImIp3nS	užívat
o	o	k7c4	o
Marii	Maria	k1gFnSc4	Maria
pojmenování	pojmenování	k1gNnSc1	pojmenování
ἀ	ἀ	k?	ἀ
π	π	k?	π
(	(	kIx(	(
<g/>
aei	aei	k?	aei
parthenos	parthenos	k1gInSc1	parthenos
<g/>
,	,	kIx,	,
lat.	lat.	k?	lat.
semper	semper	k1gInSc1	semper
virgo	virgo	k6eAd1	virgo
vždy	vždy	k6eAd1	vždy
panna	panna	k1gFnSc1	panna
<g/>
)	)	kIx)	)
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
potvrzení	potvrzení	k1gNnSc2	potvrzení
jejího	její	k3xOp3gNnSc2	její
Božího	boží	k2eAgNnSc2d1	boží
mateřství	mateřství	k1gNnSc2	mateřství
<g/>
.	.	kIx.	.
</s>
<s>
Nebylo	být	k5eNaImAgNnS	být
však	však	k9	však
definováno	definovat	k5eAaBmNgNnS	definovat
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
tímto	tento	k3xDgNnSc7	tento
"	"	kIx"	"
<g/>
vždy	vždy	k6eAd1	vždy
<g/>
"	"	kIx"	"
míní	mínit	k5eAaImIp3nS	mínit
<g/>
.	.	kIx.	.
</s>
<s>
Lateránská	lateránský	k2eAgFnSc1d1	Lateránská
synoda	synoda	k1gFnSc1	synoda
roku	rok	k1gInSc2	rok
649	[number]	k4	649
sice	sice	k8xC	sice
vyznala	vyznat	k5eAaPmAgFnS	vyznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
svatá	svatý	k2eAgFnSc1d1	svatá
<g/>
,	,	kIx,	,
ustavičná	ustavičný	k2eAgFnSc1d1	ustavičná
a	a	k8xC	a
neposkvrněná	poskvrněný	k2eNgFnSc1d1	neposkvrněná
Panna	Panna	k1gFnSc1	Panna
Maria	Maria	k1gFnSc1	Maria
...	...	k?	...
božské	božský	k2eAgNnSc1d1	božské
Slovo	slovo	k1gNnSc1	slovo
skutečně	skutečně	k6eAd1	skutečně
a	a	k8xC	a
vpravdě	vpravdě	k9	vpravdě
bez	bez	k7c2	bez
semene	semeno	k1gNnSc2	semeno
počala	počnout	k5eAaPmAgFnS	počnout
z	z	k7c2	z
Ducha	duch	k1gMnSc2	duch
svatého	svatý	k2eAgMnSc2d1	svatý
a	a	k8xC	a
neporušeně	porušeně	k6eNd1	porušeně
ho	on	k3xPp3gMnSc4	on
porodila	porodit	k5eAaPmAgFnS	porodit
a	a	k8xC	a
její	její	k3xOp3gNnSc4	její
panenství	panenství	k1gNnSc4	panenství
po	po	k7c6	po
porodu	porod	k1gInSc6	porod
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
netknuté	tknutý	k2eNgNnSc1d1	netknuté
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
protože	protože	k8xS	protože
nešlo	jít	k5eNaImAgNnS	jít
o	o	k7c4	o
všeobecný	všeobecný	k2eAgInSc4d1	všeobecný
sněm	sněm	k1gInSc4	sněm
<g/>
,	,	kIx,	,
nepatří	patřit	k5eNaImIp3nS	patřit
toto	tento	k3xDgNnSc1	tento
dogma	dogma	k1gNnSc1	dogma
mezi	mezi	k7c7	mezi
ta	ten	k3xDgFnSc1	ten
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
slavnostně	slavnostně	k6eAd1	slavnostně
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Matoušově	Matoušův	k2eAgNnSc6d1	Matoušovo
evangeliu	evangelium	k1gNnSc6	evangelium
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
výslovně	výslovně	k6eAd1	výslovně
uvedeni	uveden	k2eAgMnPc1d1	uveden
Ježíšovi	Ježíšův	k2eAgMnPc1d1	Ježíšův
bratři	bratr	k1gMnPc1	bratr
Jakub	Jakub	k1gMnSc1	Jakub
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
,	,	kIx,	,
Juda	judo	k1gNnSc2	judo
a	a	k8xC	a
Šimon	Šimon	k1gMnSc1	Šimon
a	a	k8xC	a
nejmenované	jmenovaný	k2eNgFnPc1d1	nejmenovaná
sestry	sestra	k1gFnPc1	sestra
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Theotokos	Theotokosa	k1gFnPc2	Theotokosa
a	a	k8xC	a
Narození	narození	k1gNnSc2	narození
Páně	páně	k2eAgNnSc2d1	páně
<g/>
.	.	kIx.	.
</s>
<s>
Katolíci	katolík	k1gMnPc1	katolík
a	a	k8xC	a
pravoslavní	pravoslavný	k2eAgMnPc1d1	pravoslavný
Marii	Maria	k1gFnSc3	Maria
též	též	k9	též
označují	označovat	k5eAaImIp3nP	označovat
podle	podle	k7c2	podle
formulace	formulace	k1gFnSc2	formulace
Efezského	Efezský	k2eAgInSc2d1	Efezský
koncilu	koncil	k1gInSc2	koncil
(	(	kIx(	(
<g/>
431	[number]	k4	431
<g/>
)	)	kIx)	)
Bohorodičkou	Bohorodička	k1gFnSc7	Bohorodička
(	(	kIx(	(
<g/>
θ	θ	k?	θ
Theotokos	Theotokos	k1gInSc1	Theotokos
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
Matkou	matka	k1gFnSc7	matka
Boží	božit	k5eAaImIp3nS	božit
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
dei	dei	k?	dei
genetrix	genetrix	k1gInSc1	genetrix
nebo	nebo	k8xC	nebo
deipara	deipara	k1gFnSc1	deipara
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Maria	Maria	k1gFnSc1	Maria
je	být	k5eAaImIp3nS	být
bohorodičkou	bohorodička	k1gFnSc7	bohorodička
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
4	[number]	k4	4
mariánských	mariánský	k2eAgNnPc2d1	Mariánské
dogmat	dogma	k1gNnPc2	dogma
v	v	k7c6	v
katolické	katolický	k2eAgFnSc6d1	katolická
církvi	církev	k1gFnSc6	církev
(	(	kIx(	(
<g/>
zbylými	zbylý	k2eAgInPc7d1	zbylý
třemi	tři	k4xCgInPc7	tři
pak	pak	k9	pak
jsou	být	k5eAaImIp3nP	být
dogma	dogma	k1gNnSc4	dogma
o	o	k7c6	o
panenství	panenství	k1gNnSc6	panenství
Mariině	Mariin	k2eAgNnSc6d1	Mariino
<g/>
,	,	kIx,	,
neposkvrněném	poskvrněný	k2eNgNnSc6d1	neposkvrněné
početí	početí	k1gNnSc6	početí
a	a	k8xC	a
nanebevzetí	nanebevzetí	k1gNnSc6	nanebevzetí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
428	[number]	k4	428
proti	proti	k7c3	proti
pojmu	pojem	k1gInSc3	pojem
Bohorodička	Bohorodička	k1gFnSc1	Bohorodička
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
Nestorios	Nestorios	k1gMnSc1	Nestorios
–	–	k?	–
patriarcha	patriarcha	k1gMnSc1	patriarcha
Konstantinopole	Konstantinopol	k1gInSc2	Konstantinopol
<g/>
.	.	kIx.	.
</s>
<s>
Nešlo	jít	k5eNaImAgNnS	jít
přitom	přitom	k6eAd1	přitom
vlastně	vlastně	k9	vlastně
o	o	k7c4	o
Marii	Maria	k1gFnSc4	Maria
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
o	o	k7c4	o
Krista	Kristus	k1gMnSc4	Kristus
<g/>
.	.	kIx.	.
</s>
<s>
Nestórius	Nestórius	k1gMnSc1	Nestórius
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
stoupenci	stoupenec	k1gMnPc1	stoupenec
tvrdili	tvrdit	k5eAaImAgMnP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Marii	Maria	k1gFnSc3	Maria
nelze	lze	k6eNd1	lze
nazývat	nazývat	k5eAaImF	nazývat
Boží	boží	k2eAgFnSc7d1	boží
Matkou	matka	k1gFnSc7	matka
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
matkou	matka	k1gFnSc7	matka
lidské	lidský	k2eAgFnSc2d1	lidská
přirozenosti	přirozenost	k1gFnSc2	přirozenost
Ježíše	Ježíš	k1gMnSc2	Ježíš
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
není	být	k5eNaImIp3nS	být
Theotokos	Theotokos	k1gInSc4	Theotokos
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Christotokos	Christotokos	k1gInSc1	Christotokos
<g/>
.	.	kIx.	.
</s>
<s>
Bůh	bůh	k1gMnSc1	bůh
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
zrozený	zrozený	k2eAgMnSc1d1	zrozený
<g/>
.	.	kIx.	.
</s>
<s>
Nestóriovo	Nestóriův	k2eAgNnSc1d1	Nestóriův
učení	učení	k1gNnSc1	učení
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
tvrzení	tvrzení	k1gNnSc3	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ježíš	Ježíš	k1gMnSc1	Ježíš
měl	mít	k5eAaImAgMnS	mít
dvě	dva	k4xCgFnPc4	dva
úplně	úplně	k6eAd1	úplně
odlišné	odlišný	k2eAgFnPc4d1	odlišná
přirozenosti	přirozenost	k1gFnPc4	přirozenost
<g/>
:	:	kIx,	:
božskou	božský	k2eAgFnSc4d1	božská
a	a	k8xC	a
lidskou	lidský	k2eAgFnSc4d1	lidská
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
zůstávaly	zůstávat	k5eAaImAgInP	zůstávat
vedle	vedle	k7c2	vedle
sebe	se	k3xPyFc2	se
a	a	k8xC	a
zachovávaly	zachovávat	k5eAaImAgFnP	zachovávat
si	se	k3xPyFc3	se
své	svůj	k3xOyFgFnPc4	svůj
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Sv.	sv.	kA	sv.
Cyril	Cyril	k1gMnSc1	Cyril
Alexandrijský	alexandrijský	k2eAgMnSc1d1	alexandrijský
<g/>
,	,	kIx,	,
patriarcha	patriarcha	k1gMnSc1	patriarcha
Alexandrie	Alexandrie	k1gFnSc2	Alexandrie
<g/>
,	,	kIx,	,
protestoval	protestovat	k5eAaBmAgMnS	protestovat
proti	proti	k7c3	proti
takovému	takový	k3xDgNnSc3	takový
chápání	chápání	k1gNnSc3	chápání
Ježíše	Ježíš	k1gMnSc2	Ježíš
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
popíralo	popírat	k5eAaImAgNnS	popírat
jednotu	jednota	k1gFnSc4	jednota
Boha	bůh	k1gMnSc2	bůh
a	a	k8xC	a
člověka	člověk	k1gMnSc2	člověk
v	v	k7c6	v
osobě	osoba	k1gFnSc6	osoba
Krista	Kristus	k1gMnSc2	Kristus
<g/>
.	.	kIx.	.
</s>
<s>
Učil	učít	k5eAaPmAgMnS	učít
<g/>
,	,	kIx,	,
že	že	k8xS	že
Maria	Maria	k1gFnSc1	Maria
neporodila	porodit	k5eNaPmAgFnS	porodit
stejného	stejný	k2eAgMnSc4d1	stejný
člověka	člověk	k1gMnSc4	člověk
jako	jako	k8xS	jako
jiní	jiný	k2eAgMnPc1d1	jiný
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Syna	syn	k1gMnSc2	syn
Božího	boží	k2eAgMnSc2d1	boží
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
člověkem	člověk	k1gMnSc7	člověk
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Máme	mít	k5eAaImIp1nP	mít
ji	on	k3xPp3gFnSc4	on
nazývat	nazývat	k5eAaImF	nazývat
Bohorodičkou	Bohorodička	k1gFnSc7	Bohorodička
<g/>
?	?	kIx.	?
</s>
<s>
Nepochybně	pochybně	k6eNd1	pochybně
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
počala	počnout	k5eAaPmAgFnS	počnout
a	a	k8xC	a
porodila	porodit	k5eAaPmAgFnS	porodit
Boha-Slovo	Boha-Slův	k2eAgNnSc4d1	Boha-Slův
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
člověkem	člověk	k1gMnSc7	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
titul	titul	k1gInSc4	titul
uznávali	uznávat	k5eAaImAgMnP	uznávat
všichni	všechen	k3xTgMnPc1	všechen
pravověrní	pravověrný	k2eAgMnPc1d1	pravověrný
Otcové	otec	k1gMnPc1	otec
Východu	východ	k1gInSc2	východ
i	i	k8xC	i
Západu	západ	k1gInSc2	západ
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Na	na	k7c6	na
třetím	třetí	k4xOgInSc6	třetí
všeobecném	všeobecný	k2eAgNnSc6d1	všeobecné
koncile	koncil	k1gInSc5	koncil
v	v	k7c6	v
Efezu	Efeza	k1gFnSc4	Efeza
v	v	k7c6	v
roce	rok	k1gInSc6	rok
431	[number]	k4	431
se	se	k3xPyFc4	se
měli	mít	k5eAaImAgMnP	mít
zaobírat	zaobírat	k5eAaImF	zaobírat
tímto	tento	k3xDgInSc7	tento
sporem	spor	k1gInSc7	spor
<g/>
.	.	kIx.	.
</s>
<s>
Koncil	koncil	k1gInSc1	koncil
začal	začít	k5eAaPmAgInS	začít
předčasně	předčasně	k6eAd1	předčasně
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
nepřítomnosti	nepřítomnost	k1gFnSc2	nepřítomnost
konstantinopolského	konstantinopolský	k2eAgMnSc2d1	konstantinopolský
patriarchy	patriarcha	k1gMnSc2	patriarcha
Nestória	Nestórium	k1gNnSc2	Nestórium
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnPc2	jeho
stoupenců	stoupenec	k1gMnPc2	stoupenec
a	a	k8xC	a
dalších	další	k2eAgNnPc2d1	další
43	[number]	k4	43
antiochijských	antiochijský	k2eAgMnPc2d1	antiochijský
biskupů	biskup	k1gMnPc2	biskup
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
patriarchem	patriarchem	k?	patriarchem
Janem	Jan	k1gMnSc7	Jan
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Nestórius	Nestórius	k1gMnSc1	Nestórius
odsouzený	odsouzený	k2eAgMnSc1d1	odsouzený
a	a	k8xC	a
koncil	koncil	k1gInSc1	koncil
dal	dát	k5eAaPmAgInS	dát
zapravdu	zapravdu	k6eAd1	zapravdu
sv.	sv.	kA	sv.
Cyrilovi	Cyril	k1gMnSc3	Cyril
a	a	k8xC	a
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
Kristovi	Kristus	k1gMnSc6	Kristus
se	se	k3xPyFc4	se
"	"	kIx"	"
<g/>
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
sjednocení	sjednocení	k1gNnSc1	sjednocení
dvou	dva	k4xCgFnPc2	dva
podstat	podstata	k1gFnPc2	podstata
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stoupenci	stoupenec	k1gMnPc1	stoupenec
Nestoria	Nestorium	k1gNnSc2	Nestorium
brzy	brzy	k6eAd1	brzy
založili	založit	k5eAaPmAgMnP	založit
Nestoriánsku	Nestoriánsko	k1gNnSc3	Nestoriánsko
církev	církev	k1gFnSc1	církev
<g/>
.	.	kIx.	.
</s>
<s>
Že	že	k8xS	že
Maria	Maria	k1gFnSc1	Maria
je	být	k5eAaImIp3nS	být
matkou	matka	k1gFnSc7	matka
Boha	bůh	k1gMnSc2	bůh
<g/>
,	,	kIx,	,
neuznávají	uznávat	k5eNaImIp3nP	uznávat
někteří	některý	k3yIgMnPc1	některý
protestanté	protestant	k1gMnPc1	protestant
a	a	k8xC	a
některé	některý	k3yIgFnSc2	některý
východní	východní	k2eAgFnSc2d1	východní
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Maria	Maria	k1gFnSc1	Maria
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
označovaná	označovaný	k2eAgFnSc1d1	označovaná
jako	jako	k8xS	jako
Bohorodička	Bohorodička	k1gFnSc1	Bohorodička
(	(	kIx(	(
<g/>
Theotokos	Theotokos	k1gInSc1	Theotokos
<g/>
)	)	kIx)	)
i	i	k9	i
v	v	k7c6	v
chvalozpěvech	chvalozpěv	k1gInPc6	chvalozpěv
východní	východní	k2eAgFnSc2d1	východní
ortodoxní	ortodoxní	k2eAgFnSc2d1	ortodoxní
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jejich	jejich	k3xOp3gFnSc2	jejich
víry	víra	k1gFnSc2	víra
Maria	Maria	k1gFnSc1	Maria
není	být	k5eNaImIp3nS	být
matkou	matka	k1gFnSc7	matka
božstva	božstvo	k1gNnSc2	božstvo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
skrz	skrz	k7c4	skrz
lidské	lidský	k2eAgNnSc4d1	lidské
zplození	zplození	k1gNnSc4	zplození
je	být	k5eAaImIp3nS	být
skutečnou	skutečný	k2eAgFnSc7d1	skutečná
matkou	matka	k1gFnSc7	matka
Syna	syn	k1gMnSc2	syn
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
Bohem	bůh	k1gMnSc7	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
matkou	matka	k1gFnSc7	matka
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
spojil	spojit	k5eAaPmAgMnS	spojit
s	s	k7c7	s
Bohem	bůh	k1gMnSc7	bůh
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
první	první	k4xOgFnSc2	první
chvíle	chvíle	k1gFnSc2	chvíle
svého	svůj	k3xOyFgNnSc2	svůj
početí	početí	k1gNnSc2	početí
opravdovým	opravdový	k2eAgMnSc7d1	opravdový
Bohem	bůh	k1gMnSc7	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Nanebevzetí	nanebevzetí	k1gNnSc2	nanebevzetí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tradici	tradice	k1gFnSc6	tradice
církve	církev	k1gFnSc2	církev
se	se	k3xPyFc4	se
již	již	k6eAd1	již
od	od	k7c2	od
prvních	první	k4xOgNnPc2	první
staletí	staletí	k1gNnPc2	staletí
ustálila	ustálit	k5eAaPmAgFnS	ustálit
víra	víra	k1gFnSc1	víra
o	o	k7c6	o
Mariině	Mariin	k2eAgNnSc6d1	Mariino
nanebevzetí	nanebevzetí	k1gNnSc6	nanebevzetí
<g/>
.	.	kIx.	.
</s>
<s>
Tedy	tedy	k9	tedy
že	že	k8xS	že
Maria	Maria	k1gFnSc1	Maria
nezemřela	zemřít	k5eNaPmAgFnS	zemřít
jako	jako	k9	jako
ostatní	ostatní	k2eAgMnPc1d1	ostatní
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
byla	být	k5eAaImAgNnP	být
vzata	vzít	k5eAaPmNgNnP	vzít
do	do	k7c2	do
nebeské	nebeský	k2eAgFnSc2d1	nebeská
slávy	sláva	k1gFnSc2	sláva
i	i	k8xC	i
se	s	k7c7	s
svým	svůj	k3xOyFgNnSc7	svůj
tělem	tělo	k1gNnSc7	tělo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
křesťanské	křesťanský	k2eAgFnSc6d1	křesťanská
víře	víra	k1gFnSc6	víra
je	být	k5eAaImIp3nS	být
smrt	smrt	k1gFnSc4	smrt
následkem	následkem	k7c2	následkem
hříchu	hřích	k1gInSc2	hřích
a	a	k8xC	a
Maria	Maria	k1gFnSc1	Maria
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
dle	dle	k7c2	dle
katolického	katolický	k2eAgNnSc2d1	katolické
vyznání	vyznání	k1gNnSc2	vyznání
uchráněna	uchráněn	k2eAgFnSc1d1	uchráněna
jak	jak	k6eAd1	jak
hříchu	hřích	k1gInSc3	hřích
osobního	osobní	k2eAgInSc2d1	osobní
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
prvotního	prvotní	k2eAgInSc2d1	prvotní
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Neposkvrněné	poskvrněný	k2eNgNnSc4d1	neposkvrněné
početí	početí	k1gNnSc4	početí
<g/>
)	)	kIx)	)
tedy	tedy	k9	tedy
nezemřela	zemřít	k5eNaPmAgFnS	zemřít
jako	jako	k9	jako
ostatní	ostatní	k2eAgMnPc1d1	ostatní
lidé	člověk	k1gMnPc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
oslavení	oslavení	k1gNnSc4	oslavení
jejího	její	k3xOp3gNnSc2	její
těla	tělo	k1gNnSc2	tělo
při	při	k7c6	při
nanebevzetí	nanebevzetí	k1gNnSc6	nanebevzetí
předzvěstí	předzvěst	k1gFnPc2	předzvěst
a	a	k8xC	a
zdrojem	zdroj	k1gInSc7	zdroj
naděje	naděje	k1gFnSc2	naděje
pro	pro	k7c4	pro
katolíky	katolík	k1gMnPc4	katolík
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
ti	ten	k3xDgMnPc1	ten
věří	věřit	k5eAaImIp3nP	věřit
ve	v	k7c4	v
vzkříšení	vzkříšení	k1gNnSc4	vzkříšení
těl	tělo	k1gNnPc2	tělo
všech	všecek	k3xTgMnPc2	všecek
lidí	člověk	k1gMnPc2	člověk
na	na	k7c6	na
konci	konec	k1gInSc6	konec
věků	věk	k1gInPc2	věk
k	k	k7c3	k
poslednímu	poslední	k2eAgInSc3d1	poslední
soudu	soud	k1gInSc3	soud
<g/>
.	.	kIx.	.
</s>
<s>
Tradiční	tradiční	k2eAgFnSc1d1	tradiční
víra	víra	k1gFnSc1	víra
v	v	k7c4	v
Mariino	Mariin	k2eAgNnSc4d1	Mariino
nanebevzetí	nanebevzetí	k1gNnSc4	nanebevzetí
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
katolické	katolický	k2eAgFnSc6d1	katolická
církvi	církev	k1gFnSc6	církev
papežem	papež	k1gMnSc7	papež
Piem	Pius	k1gMnSc7	Pius
XII	XII	kA	XII
<g/>
.	.	kIx.	.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
stvrzena	stvrzen	k2eAgFnSc1d1	stvrzena
dogmatem	dogma	k1gNnSc7	dogma
o	o	k7c4	o
nanebevzetí	nanebevzetí	k1gNnSc4	nanebevzetí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
článek	článek	k1gInSc1	článek
víry	víra	k1gFnSc2	víra
stal	stát	k5eAaPmAgInS	stát
závazným	závazný	k2eAgInSc7d1	závazný
pro	pro	k7c4	pro
všechny	všechen	k3xTgMnPc4	všechen
katolíky	katolík	k1gMnPc4	katolík
a	a	k8xC	a
v	v	k7c6	v
katolické	katolický	k2eAgFnSc6d1	katolická
církvi	církev	k1gFnSc6	církev
byla	být	k5eAaImAgFnS	být
otázka	otázka	k1gFnSc1	otázka
Mariina	Mariin	k2eAgNnSc2d1	Mariino
nanebevzetí	nanebevzetí	k1gNnSc2	nanebevzetí
jednou	jednou	k6eAd1	jednou
provždy	provždy	k6eAd1	provždy
vyřešena	vyřešit	k5eAaPmNgFnS	vyřešit
<g/>
.	.	kIx.	.
</s>
<s>
Víra	víra	k1gFnSc1	víra
v	v	k7c6	v
nanebevzetí	nanebevzetí	k1gNnSc6	nanebevzetí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
je	být	k5eAaImIp3nS	být
také	také	k9	také
prvkem	prvek	k1gInSc7	prvek
pravoslaví	pravoslaví	k1gNnSc2	pravoslaví
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
reformované	reformovaný	k2eAgFnPc1d1	reformovaná
církve	církev	k1gFnPc1	církev
se	se	k3xPyFc4	se
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
k	k	k7c3	k
dalším	další	k2eAgNnPc3d1	další
mariánským	mariánský	k2eAgNnPc3d1	Mariánské
dogmatům	dogma	k1gNnPc3	dogma
<g/>
,	,	kIx,	,
stavějí	stavět	k5eAaImIp3nP	stavět
odmítavě	odmítavě	k6eAd1	odmítavě
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
nanebevzetí	nanebevzetí	k1gNnSc6	nanebevzetí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
Bibli	bible	k1gFnSc6	bible
žádná	žádný	k3yNgFnSc1	žádný
zmínka	zmínka	k1gFnSc1	zmínka
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Zesnutí	zesnutí	k1gNnSc2	zesnutí
přesvaté	přesvatý	k2eAgFnSc2d1	přesvatá
Bohorodice	bohorodice	k1gFnSc2	bohorodice
<g/>
.	.	kIx.	.
</s>
<s>
Svátek	svátek	k1gInSc4	svátek
Zesnutí	zesnutí	k1gNnSc2	zesnutí
přesvaté	přesvatý	k2eAgFnSc2d1	přesvatá
Bohorodice	bohorodice	k1gFnSc2	bohorodice
se	se	k3xPyFc4	se
slaví	slavit	k5eAaImIp3nS	slavit
zejména	zejména	k9	zejména
v	v	k7c6	v
pravoslaví	pravoslaví	k1gNnSc6	pravoslaví
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c7	mezi
tzv.	tzv.	kA	tzv.
Dvanáct	dvanáct	k4xCc1	dvanáct
velkých	velký	k2eAgInPc2d1	velký
svátků	svátek	k1gInPc2	svátek
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
prakticky	prakticky	k6eAd1	prakticky
o	o	k7c4	o
svátek	svátek	k1gInSc4	svátek
shodný	shodný	k2eAgInSc4d1	shodný
s	s	k7c7	s
katolickým	katolický	k2eAgInSc7d1	katolický
svátkem	svátek	k1gInSc7	svátek
Nanebevzetí	nanebevzetí	k1gNnSc2	nanebevzetí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Mariánská	mariánský	k2eAgNnPc4d1	Mariánské
zjevení	zjevení	k1gNnSc4	zjevení
<g/>
.	.	kIx.	.
</s>
<s>
Maria	Maria	k1gFnSc1	Maria
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
katolíků	katolík	k1gMnPc2	katolík
a	a	k8xC	a
pravoslavných	pravoslavný	k2eAgMnPc2d1	pravoslavný
předmětem	předmět	k1gInSc7	předmět
obzvláštní	obzvláštní	k2eAgFnSc2d1	obzvláštní
úcty	úcta	k1gFnSc2	úcta
<g/>
,	,	kIx,	,
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
je	být	k5eAaImIp3nS	být
chápána	chápán	k2eAgFnSc1d1	chápána
jako	jako	k8xC	jako
první	první	k4xOgFnSc4	první
mezi	mezi	k7c7	mezi
světci	světec	k1gMnPc7	světec
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
poutních	poutní	k2eAgNnPc2d1	poutní
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
jsou	být	k5eAaImIp3nP	být
spojena	spojen	k2eAgNnPc1d1	spojeno
se	s	k7c7	s
zjeveními	zjevení	k1gNnPc7	zjevení
či	či	k8xC	či
zázraky	zázrak	k1gInPc1	zázrak
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
hojně	hojně	k6eAd1	hojně
navštěvována	navštěvován	k2eAgNnPc1d1	navštěvováno
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
náleží	náležet	k5eAaImIp3nS	náležet
mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgNnPc4d3	nejvýznamnější
poutní	poutní	k2eAgNnPc4d1	poutní
místa	místo	k1gNnPc4	místo
svých	svůj	k3xOyFgFnPc2	svůj
zemí	zem	k1gFnPc2	zem
nebo	nebo	k8xC	nebo
i	i	k9	i
kontinentů	kontinent	k1gInPc2	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgNnPc4d3	nejvýznamnější
mariánská	mariánský	k2eAgNnPc4d1	Mariánské
poutní	poutní	k2eAgNnPc4d1	poutní
místa	místo	k1gNnPc4	místo
oficiálně	oficiálně	k6eAd1	oficiálně
uznávaná	uznávaný	k2eAgFnSc1d1	uznávaná
katolickou	katolický	k2eAgFnSc7d1	katolická
církví	církev	k1gFnSc7	církev
náleží	náležet	k5eAaImIp3nS	náležet
<g/>
:	:	kIx,	:
Jasná	jasný	k2eAgFnSc1d1	jasná
Hora	hora	k1gFnSc1	hora
<g/>
,	,	kIx,	,
Mariazell	Mariazell	k1gInSc1	Mariazell
<g/>
,	,	kIx,	,
Lurdy	Lurd	k1gInPc1	Lurd
<g/>
,	,	kIx,	,
Fatima	Fatimum	k1gNnPc1	Fatimum
a	a	k8xC	a
Guadalupe	Guadalup	k1gInSc5	Guadalup
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
je	být	k5eAaImIp3nS	být
nejvýznamnějšími	významný	k2eAgInPc7d3	nejvýznamnější
mariánskými	mariánský	k2eAgInPc7d1	mariánský
poutními	poutní	k2eAgInPc7d1	poutní
místy	místo	k1gNnPc7	místo
Svatý	svatý	k2eAgInSc4d1	svatý
Hostýn	Hostýn	k1gInSc4	Hostýn
<g/>
,	,	kIx,	,
Svatý	svatý	k2eAgInSc4d1	svatý
kopeček	kopeček	k1gInSc4	kopeček
nad	nad	k7c7	nad
Olomoucí	Olomouc	k1gFnSc7	Olomouc
a	a	k8xC	a
Svatá	svatý	k2eAgFnSc1d1	svatá
Hora	hora	k1gFnSc1	hora
u	u	k7c2	u
Příbrami	Příbram	k1gFnSc2	Příbram
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
významným	významný	k2eAgNnSc7d1	významné
neoficiálním	neoficiální	k2eAgNnSc7d1	neoficiální
poutním	poutní	k2eAgNnSc7d1	poutní
místem	místo	k1gNnSc7	místo
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgNnSc4	jenž
pověst	pověst	k1gFnSc1	pověst
je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
údajných	údajný	k2eAgNnPc6d1	údajné
zjeveních	zjevení	k1gNnPc6	zjevení
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Medžugorie	Medžugorie	k1gFnSc1	Medžugorie
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
těmto	tento	k3xDgNnPc3	tento
zjevením	zjevení	k1gNnPc3	zjevení
však	však	k9	však
zatím	zatím	k6eAd1	zatím
oficiální	oficiální	k2eAgNnSc4d1	oficiální
prohlášení	prohlášení	k1gNnSc4	prohlášení
církve	církev	k1gFnSc2	církev
nebylo	být	k5eNaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
<g/>
.	.	kIx.	.
</s>
<s>
Katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
se	se	k3xPyFc4	se
k	k	k7c3	k
mariánským	mariánský	k2eAgNnPc3d1	Mariánské
zjevením	zjevení	k1gNnPc3	zjevení
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
smyslu	smysl	k1gInSc6	smysl
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
jsou	být	k5eAaImIp3nP	být
věroučně	věroučně	k6eAd1	věroučně
v	v	k7c6	v
pořádku	pořádek	k1gInSc6	pořádek
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
ano	ano	k9	ano
<g/>
,	,	kIx,	,
nebrání	bránit	k5eNaImIp3nS	bránit
jejich	jejich	k3xOp3gNnSc1	jejich
akceptování	akceptování	k1gNnSc1	akceptování
mezi	mezi	k7c7	mezi
lidem	lid	k1gInSc7	lid
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
nikdo	nikdo	k3yNnSc1	nikdo
jim	on	k3xPp3gMnPc3	on
není	být	k5eNaImIp3nS	být
povinen	povinen	k2eAgMnSc1d1	povinen
věřit	věřit	k5eAaImF	věřit
<g/>
.	.	kIx.	.
</s>
<s>
Nelze	lze	k6eNd1	lze
tedy	tedy	k9	tedy
mluvit	mluvit	k5eAaImF	mluvit
o	o	k7c6	o
nějakém	nějaký	k3yIgNnSc6	nějaký
potvrzení	potvrzení	k1gNnSc1	potvrzení
pravosti	pravost	k1gFnSc2	pravost
zjevení	zjevení	k1gNnSc1	zjevení
<g/>
.	.	kIx.	.
</s>
<s>
Konečným	konečný	k2eAgNnSc7d1	konečné
zjevením	zjevení	k1gNnSc7	zjevení
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
Ježíš	Ježíš	k1gMnSc1	Ježíš
Kristus	Kristus	k1gMnSc1	Kristus
a	a	k8xC	a
všechna	všechen	k3xTgNnPc4	všechen
další	další	k2eAgNnPc4d1	další
zjevení	zjevení	k1gNnPc4	zjevení
tudíž	tudíž	k8xC	tudíž
nemohou	moct	k5eNaImIp3nP	moct
přinést	přinést	k5eAaPmF	přinést
nic	nic	k3yNnSc1	nic
nového	nový	k2eAgMnSc4d1	nový
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
pouze	pouze	k6eAd1	pouze
upozorňovat	upozorňovat	k5eAaImF	upozorňovat
na	na	k7c4	na
pravdy	pravda	k1gFnPc4	pravda
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
již	již	k6eAd1	již
byly	být	k5eAaImAgFnP	být
vyřčeny	vyřčen	k2eAgFnPc1d1	vyřčena
<g/>
.	.	kIx.	.
</s>
<s>
Vztah	vztah	k1gInSc1	vztah
protestantských	protestantský	k2eAgFnPc2d1	protestantská
církví	církev	k1gFnPc2	církev
k	k	k7c3	k
Marii	Maria	k1gFnSc3	Maria
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
reformačními	reformační	k2eAgInPc7d1	reformační
důrazy	důraz	k1gInPc7	důraz
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
nichž	jenž	k3xRgFnPc2	jenž
byla	být	k5eAaImAgFnS	být
zcela	zcela	k6eAd1	zcela
odmítnuta	odmítnut	k2eAgFnSc1d1	odmítnuta
tradice	tradice	k1gFnSc1	tradice
jako	jako	k8xS	jako
věroučný	věroučný	k2eAgInSc1d1	věroučný
základ	základ	k1gInSc1	základ
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
zůstala	zůstat	k5eAaPmAgFnS	zůstat
pouze	pouze	k6eAd1	pouze
sama	sám	k3xTgFnSc1	sám
Bible	bible	k1gFnSc1	bible
(	(	kIx(	(
<g/>
princip	princip	k1gInSc1	princip
sola	sol	k2eAgFnSc1d1	sola
scriptura	scriptura	k1gFnSc1	scriptura
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
Maria	Maria	k1gFnSc1	Maria
v	v	k7c6	v
protestantských	protestantský	k2eAgFnPc6d1	protestantská
církvích	církev	k1gFnPc6	církev
chápána	chápat	k5eAaImNgFnS	chápat
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
pouze	pouze	k6eAd1	pouze
jako	jako	k8xC	jako
nástroj	nástroj	k1gInSc1	nástroj
inkarnace	inkarnace	k1gFnSc2	inkarnace
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
svět	svět	k1gInSc4	svět
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
v	v	k7c6	v
případě	případ	k1gInSc6	případ
dalších	další	k2eAgFnPc2d1	další
katolických	katolický	k2eAgFnPc2d1	katolická
svatých	svatá	k1gFnPc2	svatá
je	být	k5eAaImIp3nS	být
odmítnuto	odmítnout	k5eAaPmNgNnS	odmítnout
také	také	k9	také
uctívání	uctívání	k1gNnSc3	uctívání
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
reformačním	reformační	k2eAgInSc7d1	reformační
principem	princip	k1gInSc7	princip
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
něhož	jenž	k3xRgNnSc2	jenž
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
uctívat	uctívat	k5eAaImF	uctívat
pouze	pouze	k6eAd1	pouze
samotného	samotný	k2eAgMnSc4d1	samotný
Boha	bůh	k1gMnSc4	bůh
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
princip	princip	k1gInSc1	princip
soli	sůl	k1gFnSc2	sůl
Deo	Deo	k1gFnSc1	Deo
gloria	gloria	k1gFnSc1	gloria
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mariánský	mariánský	k2eAgInSc1d1	mariánský
kult	kult	k1gInSc1	kult
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
kult	kult	k1gInSc1	kult
svatých	svatá	k1gFnPc2	svatá
<g/>
,	,	kIx,	,
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
modloslužbu	modloslužba	k1gFnSc4	modloslužba
<g/>
.	.	kIx.	.
</s>
<s>
Islám	islám	k1gInSc1	islám
vidí	vidět	k5eAaImIp3nS	vidět
v	v	k7c4	v
Marii	Maria	k1gFnSc4	Maria
matku	matka	k1gFnSc4	matka
proroka	prorok	k1gMnSc2	prorok
Isy	Isy	k1gMnSc2	Isy
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
Ježíše	Ježíš	k1gMnSc2	Ježíš
<g/>
.	.	kIx.	.
</s>
<s>
Shoduje	shodovat	k5eAaImIp3nS	shodovat
se	se	k3xPyFc4	se
s	s	k7c7	s
křesťanskou	křesťanský	k2eAgFnSc7d1	křesťanská
pozicí	pozice	k1gFnSc7	pozice
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Maria	Maria	k1gFnSc1	Maria
porodila	porodit	k5eAaPmAgFnS	porodit
Ježíše	Ježíš	k1gMnPc4	Ježíš
jako	jako	k9	jako
panna	panna	k1gFnSc1	panna
<g/>
.	.	kIx.	.
</s>
<s>
Korán	korán	k1gInSc1	korán
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c4	o
Marii	Maria	k1gFnSc4	Maria
na	na	k7c6	na
dvou	dva	k4xCgNnPc6	dva
místech	místo	k1gNnPc6	místo
(	(	kIx(	(
<g/>
3,35	[number]	k4	3,35
<g/>
–	–	k?	–
<g/>
47	[number]	k4	47
a	a	k8xC	a
19,16	[number]	k4	19,16
<g/>
–	–	k?	–
<g/>
34	[number]	k4	34
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
stručnější	stručný	k2eAgNnSc4d2	stručnější
než	než	k8xS	než
biblické	biblický	k2eAgNnSc4d1	biblické
podání	podání	k1gNnSc4	podání
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Koránu	korán	k1gInSc2	korán
byla	být	k5eAaImAgFnS	být
Maria	Maria	k1gFnSc1	Maria
zasvěcena	zasvěcen	k2eAgFnSc1d1	zasvěcena
Bohu	bůh	k1gMnSc3	bůh
ještě	ještě	k9	ještě
před	před	k7c7	před
narozením	narození	k1gNnSc7	narození
<g/>
.	.	kIx.	.
</s>
