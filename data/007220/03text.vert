<s>
Olomouc	Olomouc	k1gFnSc1	Olomouc
(	(	kIx(	(
<g/>
rod	rod	k1gInSc1	rod
ženský	ženský	k2eAgInSc1d1	ženský
<g/>
,	,	kIx,	,
v	v	k7c6	v
místním	místní	k2eAgInSc6d1	místní
úzu	úzus	k1gInSc6	úzus
i	i	k9	i
mužský	mužský	k2eAgInSc1d1	mužský
<g/>
;	;	kIx,	;
hanácky	hanácky	k6eAd1	hanácky
Olomóc	Olomóc	k1gFnSc1	Olomóc
nebo	nebo	k8xC	nebo
Holomóc	Holomóc	k1gFnSc1	Holomóc
<g/>
;	;	kIx,	;
německy	německy	k6eAd1	německy
Olmütz	Olmütz	k1gInSc1	Olmütz
<g/>
;	;	kIx,	;
polsky	polsky	k6eAd1	polsky
Ołomuniec	Ołomuniec	k1gInSc1	Ołomuniec
<g/>
;	;	kIx,	;
latinsky	latinsky	k6eAd1	latinsky
Olomucium	Olomucium	k1gNnSc1	Olomucium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
statutární	statutární	k2eAgMnSc1d1	statutární
a	a	k8xC	a
univerzitní	univerzitní	k2eAgNnSc1d1	univerzitní
město	město	k1gNnSc1	město
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
centrum	centrum	k1gNnSc1	centrum
Olomouckého	olomoucký	k2eAgInSc2d1	olomoucký
kraje	kraj	k1gInSc2	kraj
<g/>
,	,	kIx,	,
metropole	metropol	k1gFnSc2	metropol
Hané	Haná	k1gFnSc2	Haná
a	a	k8xC	a
historická	historický	k2eAgFnSc1d1	historická
metropole	metropole	k1gFnSc1	metropole
celé	celý	k2eAgFnSc2d1	celá
Moravy	Morava	k1gFnSc2	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
10	[number]	k4	10
336	[number]	k4	336
ha	ha	kA	ha
žije	žít	k5eAaImIp3nS	žít
100	[number]	k4	100
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
šestým	šestý	k4xOgMnSc7	šestý
nejlidnatějším	lidnatý	k2eAgInSc7d3	nejlidnatější
městem	město	k1gNnSc7	město
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Olomouc	Olomouc	k1gFnSc1	Olomouc
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
centrem	centrum	k1gNnSc7	centrum
Moravy	Morava	k1gFnSc2	Morava
<g/>
,	,	kIx,	,
do	do	k7c2	do
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
třetím	třetí	k4xOgMnSc6	třetí
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
zemí	zem	k1gFnSc7	zem
České	český	k2eAgFnSc2d1	Česká
koruny	koruna	k1gFnSc2	koruna
(	(	kIx(	(
<g/>
po	po	k7c6	po
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
Vratislavi	Vratislav	k1gFnSc6	Vratislav
<g/>
)	)	kIx)	)
a	a	k8xC	a
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
sídlem	sídlo	k1gNnSc7	sídlo
arcibiskupství	arcibiskupství	k1gNnSc2	arcibiskupství
a	a	k8xC	a
metropolity	metropolita	k1gMnSc2	metropolita
moravské	moravský	k2eAgFnSc2d1	Moravská
církevní	církevní	k2eAgFnSc2d1	církevní
provincie	provincie	k1gFnSc2	provincie
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
centrem	centrum	k1gNnSc7	centrum
pravoslavné	pravoslavný	k2eAgFnSc2d1	pravoslavná
církve	církev	k1gFnSc2	církev
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
soudní	soudní	k2eAgFnSc2d1	soudní
moci	moc	k1gFnSc2	moc
je	být	k5eAaImIp3nS	být
významnou	významný	k2eAgFnSc7d1	významná
institucí	instituce	k1gFnSc7	instituce
Vrchní	vrchní	k2eAgInSc1d1	vrchní
soud	soud	k1gInSc1	soud
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
.	.	kIx.	.
</s>
<s>
Význam	význam	k1gInSc1	význam
Olomouce	Olomouc	k1gFnSc2	Olomouc
jako	jako	k8xS	jako
vzdělanostního	vzdělanostní	k2eAgNnSc2d1	vzdělanostní
centra	centrum	k1gNnSc2	centrum
podtrhují	podtrhovat	k5eAaImIp3nP	podtrhovat
Univerzita	univerzita	k1gFnSc1	univerzita
Palackého	Palackého	k2eAgFnSc1d1	Palackého
<g/>
,	,	kIx,	,
nejstarší	starý	k2eAgFnSc1d3	nejstarší
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
a	a	k8xC	a
druhá	druhý	k4xOgFnSc1	druhý
nejstarší	starý	k2eAgFnSc1d3	nejstarší
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
,	,	kIx,	,
Vědecká	vědecký	k2eAgFnSc1d1	vědecká
knihovna	knihovna	k1gFnSc1	knihovna
a	a	k8xC	a
Slovanské	slovanský	k2eAgNnSc1d1	slovanské
gymnázium	gymnázium	k1gNnSc1	gymnázium
<g/>
,	,	kIx,	,
nejstarší	starý	k2eAgFnSc1d3	nejstarší
nepřetržitě	přetržitě	k6eNd1	přetržitě
fungující	fungující	k2eAgFnSc1d1	fungující
střední	střední	k2eAgFnSc1d1	střední
škola	škola	k1gFnSc1	škola
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
s	s	k7c7	s
českým	český	k2eAgInSc7d1	český
vyučovacím	vyučovací	k2eAgInSc7d1	vyučovací
jazykem	jazyk	k1gInSc7	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Olomouc	Olomouc	k1gFnSc1	Olomouc
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
významným	významný	k2eAgNnSc7d1	významné
obranným	obranný	k2eAgNnSc7d1	obranné
centrem	centrum	k1gNnSc7	centrum
<g/>
,	,	kIx,	,
sídlem	sídlo	k1gNnSc7	sídlo
silné	silný	k2eAgFnSc2d1	silná
vojenské	vojenský	k2eAgFnSc2d1	vojenská
posádky	posádka	k1gFnSc2	posádka
<g/>
,	,	kIx,	,
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
přebudováno	přebudovat	k5eAaPmNgNnS	přebudovat
na	na	k7c4	na
mocnou	mocný	k2eAgFnSc4d1	mocná
pevnost	pevnost	k1gFnSc4	pevnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
bylo	být	k5eAaImAgNnS	být
vystavěno	vystavět	k5eAaPmNgNnS	vystavět
mnoho	mnoho	k4c1	mnoho
kasáren	kasárny	k1gFnPc2	kasárny
<g/>
,	,	kIx,	,
cvičišť	cvičiště	k1gNnPc2	cvičiště
<g/>
,	,	kIx,	,
skladišť	skladiště	k1gNnPc2	skladiště
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
objektů	objekt	k1gInPc2	objekt
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
využívala	využívat	k5eAaImAgFnS	využívat
armáda	armáda	k1gFnSc1	armáda
i	i	k9	i
po	po	k7c6	po
zrušení	zrušení	k1gNnSc6	zrušení
pevnosti	pevnost	k1gFnSc2	pevnost
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1884	[number]	k4	1884
<g/>
,	,	kIx,	,
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
Československé	československý	k2eAgFnSc2d1	Československá
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
kdy	kdy	k6eAd1	kdy
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
též	též	k9	též
významné	významný	k2eAgNnSc1d1	významné
letiště	letiště	k1gNnSc1	letiště
v	v	k7c6	v
Neředíně	Neředín	k1gInSc6	Neředín
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
i	i	k9	i
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
i	i	k9	i
velice	velice	k6eAd1	velice
početná	početný	k2eAgFnSc1d1	početná
(	(	kIx(	(
<g/>
okupační	okupační	k2eAgFnSc1d1	okupační
<g/>
)	)	kIx)	)
posádka	posádka	k1gFnSc1	posádka
Sovětské	sovětský	k2eAgFnSc2d1	sovětská
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
sídlilo	sídlit	k5eAaImAgNnS	sídlit
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
Velitelství	velitelství	k1gNnSc4	velitelství
společných	společný	k2eAgFnPc2d1	společná
sil	síla	k1gFnPc2	síla
AČR	AČR	kA	AČR
<g/>
,	,	kIx,	,
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
tedy	tedy	k8xC	tedy
město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
hlavním	hlavní	k2eAgMnSc7d1	hlavní
centrem	centr	k1gMnSc7	centr
obrany	obrana	k1gFnSc2	obrana
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
atmosféru	atmosféra	k1gFnSc4	atmosféra
a	a	k8xC	a
historické	historický	k2eAgFnPc4d1	historická
pamětihodnosti	pamětihodnost	k1gFnPc4	pamětihodnost
je	být	k5eAaImIp3nS	být
Olomouc	Olomouc	k1gFnSc4	Olomouc
vyhledávaným	vyhledávaný	k2eAgNnSc7d1	vyhledávané
střediskem	středisko	k1gNnSc7	středisko
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
konferencí	konference	k1gFnPc2	konference
a	a	k8xC	a
festivalů	festival	k1gInPc2	festival
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
čtyřicetiletou	čtyřicetiletý	k2eAgFnSc4d1	čtyřicetiletá
tradici	tradice	k1gFnSc4	tradice
má	mít	k5eAaImIp3nS	mít
Academia	academia	k1gFnSc1	academia
film	film	k1gInSc1	film
Olomouc	Olomouc	k1gFnSc1	Olomouc
(	(	kIx(	(
<g/>
AFO	AFO	kA	AFO
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
festival	festival	k1gInSc1	festival
dokumentárních	dokumentární	k2eAgInPc2d1	dokumentární
filmů	film	k1gInPc2	film
a	a	k8xC	a
videoprogramů	videoprogram	k1gInPc2	videoprogram
<g/>
.	.	kIx.	.
</s>
<s>
Koná	konat	k5eAaImIp3nS	konat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
také	také	k9	také
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgInPc2d3	nejvýznamnější
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
českých	český	k2eAgInPc2d1	český
divadelních	divadelní	k2eAgInPc2d1	divadelní
festivalů	festival	k1gInPc2	festival
Divadelní	divadelní	k2eAgFnSc1d1	divadelní
Flora	Flora	k1gFnSc1	Flora
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
Flora	Flora	k1gFnSc1	Flora
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
,	,	kIx,	,
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
výstava	výstava	k1gFnSc1	výstava
zaměřená	zaměřený	k2eAgFnSc1d1	zaměřená
na	na	k7c4	na
zahradnictví	zahradnictví	k1gNnSc4	zahradnictví
a	a	k8xC	a
pěstování	pěstování	k1gNnSc4	pěstování
rostlin	rostlina	k1gFnPc2	rostlina
s	s	k7c7	s
každoroční	každoroční	k2eAgFnSc7d1	každoroční
návštěvností	návštěvnost	k1gFnSc7	návštěvnost
okolo	okolo	k7c2	okolo
80	[number]	k4	80
000	[number]	k4	000
návštěvníků	návštěvník	k1gMnPc2	návštěvník
<g/>
.	.	kIx.	.
</s>
<s>
Oblíbený	oblíbený	k2eAgInSc1d1	oblíbený
je	být	k5eAaImIp3nS	být
také	také	k9	také
Podzimní	podzimní	k2eAgInSc1d1	podzimní
festival	festival	k1gInSc1	festival
duchovní	duchovní	k2eAgFnSc2d1	duchovní
hudby	hudba	k1gFnSc2	hudba
či	či	k8xC	či
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
varhanní	varhanní	k2eAgInSc1d1	varhanní
festival	festival	k1gInSc1	festival
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgMnPc4	svůj
fanoušky	fanoušek	k1gMnPc4	fanoušek
si	se	k3xPyFc3	se
již	již	k6eAd1	již
našly	najít	k5eAaPmAgFnP	najít
také	také	k9	také
mladší	mladý	k2eAgFnPc4d2	mladší
akce	akce	k1gFnPc4	akce
–	–	k?	–
květnový	květnový	k2eAgInSc4d1	květnový
Beerfest	Beerfest	k1gInSc4	Beerfest
i	i	k8xC	i
srpnový	srpnový	k2eAgInSc4d1	srpnový
Flamenco	flamenco	k1gNnSc1	flamenco
festival	festival	k1gInSc1	festival
<g/>
.	.	kIx.	.
</s>
<s>
Olomouc	Olomouc	k1gFnSc1	Olomouc
je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
svými	svůj	k3xOyFgFnPc7	svůj
historickými	historický	k2eAgFnPc7d1	historická
památkami	památka	k1gFnPc7	památka
<g/>
,	,	kIx,	,
její	její	k3xOp3gNnSc1	její
historické	historický	k2eAgNnSc1d1	historické
jádro	jádro	k1gNnSc1	jádro
je	být	k5eAaImIp3nS	být
městskou	městský	k2eAgFnSc7d1	městská
památkovou	památkový	k2eAgFnSc7d1	památková
rezervací	rezervace	k1gFnSc7	rezervace
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
pražské	pražský	k2eAgFnSc6d1	Pražská
druhá	druhý	k4xOgFnSc1	druhý
nejvýznamnější	významný	k2eAgFnSc1d3	nejvýznamnější
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
jména	jméno	k1gNnSc2	jméno
Olomouc	Olomouc	k1gFnSc1	Olomouc
je	být	k5eAaImIp3nS	být
etymologicky	etymologicky	k6eAd1	etymologicky
sporný	sporný	k2eAgInSc1d1	sporný
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
slovo	slovo	k1gNnSc1	slovo
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
na	na	k7c4	na
Olo-mouc	Oloouc	k1gFnSc4	Olo-mouc
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
druhá	druhý	k4xOgFnSc1	druhý
část	část	k1gFnSc1	část
-mouc	ouc	k1gFnSc1	-mouc
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
souvislost	souvislost	k1gFnSc1	souvislost
se	s	k7c7	s
staročeským	staročeský	k2eAgNnSc7d1	staročeské
slovesem	sloveso	k1gNnSc7	sloveso
mútit	mútit	k5eAaImF	mútit
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
hlučet	hlučet	k5eAaImF	hlučet
(	(	kIx(	(
<g/>
doloženo	doložit	k5eAaPmNgNnS	doložit
v	v	k7c6	v
osobních	osobní	k2eAgInPc6d1	osobní
i	i	k8xC	i
místních	místní	k2eAgInPc6d1	místní
jménech	jméno	k1gNnPc6	jméno
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Mutimír	Mutimír	k1gMnSc1	Mutimír
<g/>
,	,	kIx,	,
Moutnice	Moutnice	k1gFnSc1	Moutnice
<g/>
,	,	kIx,	,
Mutná	Mutná	k1gFnSc1	Mutná
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc4	první
část	část	k1gFnSc4	část
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
vycházet	vycházet	k5eAaImF	vycházet
z	z	k7c2	z
hypotetického	hypotetický	k2eAgNnSc2d1	hypotetické
praslovanského	praslovanský	k2eAgNnSc2d1	praslovanské
slova	slovo	k1gNnSc2	slovo
*	*	kIx~	*
<g/>
ol	ol	k?	ol
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
pivo	pivo	k1gNnSc4	pivo
(	(	kIx(	(
<g/>
srovnej	srovnat	k5eAaPmRp2nS	srovnat
anglické	anglický	k2eAgFnPc4d1	anglická
ale	ale	k8xC	ale
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jiný	jiný	k2eAgInSc1d1	jiný
výklad	výklad	k1gInSc1	výklad
operuje	operovat	k5eAaImIp3nS	operovat
s	s	k7c7	s
hypotetickým	hypotetický	k2eAgNnSc7d1	hypotetické
osobním	osobní	k2eAgNnSc7d1	osobní
jménem	jméno	k1gNnSc7	jméno
*	*	kIx~	*
<g/>
Olomút	Olomút	k1gInSc1	Olomút
<g/>
,	,	kIx,	,
jméno	jméno	k1gNnSc1	jméno
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
utvořeno	utvořit	k5eAaPmNgNnS	utvořit
přivlastňovací	přivlastňovací	k2eAgFnSc7d1	přivlastňovací
příponou	přípona	k1gFnSc7	přípona
-jь	ь	k?	-jь
(	(	kIx(	(
<g/>
j	j	k?	j
<g/>
'	'	kIx"	'
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
asimilovanou	asimilovaný	k2eAgFnSc4d1	asimilovaná
v	v	k7c6	v
-c	-c	k?	-c
<g/>
,	,	kIx,	,
a	a	k8xC	a
znamenalo	znamenat	k5eAaImAgNnS	znamenat
by	by	kYmCp3nS	by
"	"	kIx"	"
<g/>
ves	ves	k1gFnSc1	ves
v	v	k7c6	v
Olomútově	Olomútův	k2eAgNnSc6d1	Olomútův
vlastnictví	vlastnictví	k1gNnSc6	vlastnictví
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
Bolelouc	Bolelouc	k1gInSc1	Bolelouc
–	–	k?	–
Bolelútův	Bolelútův	k2eAgInSc1d1	Bolelútův
majetek	majetek	k1gInSc1	majetek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
výklad	výklad	k1gInSc4	výklad
zmiňovali	zmiňovat	k5eAaImAgMnP	zmiňovat
jako	jako	k8xC	jako
jediný	jediný	k2eAgMnSc1d1	jediný
například	například	k6eAd1	například
Václav	Václav	k1gMnSc1	Václav
Ertl	Ertl	k1gMnSc1	Ertl
či	či	k8xC	či
Alena	Alena	k1gFnSc1	Alena
Polívková	Polívková	k1gFnSc1	Polívková
<g/>
.	.	kIx.	.
</s>
<s>
Osobní	osobní	k2eAgNnSc1d1	osobní
jméno	jméno	k1gNnSc1	jméno
Olomút	Olomúta	k1gFnPc2	Olomúta
ale	ale	k8xC	ale
není	být	k5eNaImIp3nS	být
doloženo	doložen	k2eAgNnSc1d1	doloženo
a	a	k8xC	a
nedává	dávat	k5eNaImIp3nS	dávat
ve	v	k7c6	v
slovanských	slovanský	k2eAgInPc6d1	slovanský
jazycích	jazyk	k1gInPc6	jazyk
smysl	smysl	k1gInSc4	smysl
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytly	vyskytnout	k5eAaPmAgFnP	vyskytnout
se	se	k3xPyFc4	se
také	také	k6eAd1	také
pokusy	pokus	k1gInPc4	pokus
vyložit	vyložit	k5eAaPmF	vyložit
jméno	jméno	k1gNnSc4	jméno
pomocí	pomocí	k7c2	pomocí
západoevropských	západoevropský	k2eAgInPc2d1	západoevropský
jazyků	jazyk	k1gInPc2	jazyk
(	(	kIx(	(
<g/>
Alamud	Alamud	k1gMnSc1	Alamud
<g/>
,	,	kIx,	,
Aulomont	Aulomont	k1gMnSc1	Aulomont
<g/>
)	)	kIx)	)
či	či	k8xC	či
z	z	k7c2	z
latiny	latina	k1gFnSc2	latina
(	(	kIx(	(
<g/>
Juliomontium	Juliomontium	k1gNnSc1	Juliomontium
<g/>
,	,	kIx,	,
údajně	údajně	k6eAd1	údajně
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
Julia	Julius	k1gMnSc2	Julius
Caesara	Caesar	k1gMnSc2	Caesar
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
Caesarova	Caesarův	k2eAgFnSc1d1	Caesarova
kašna	kašna	k1gFnSc1	kašna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
ale	ale	k9	ale
není	být	k5eNaImIp3nS	být
všeobecně	všeobecně	k6eAd1	všeobecně
uznáván	uznáván	k2eAgInSc1d1	uznáván
<g/>
.	.	kIx.	.
</s>
<s>
Místní	místní	k2eAgNnSc1d1	místní
jméno	jméno	k1gNnSc1	jméno
je	být	k5eAaImIp3nS	být
poprvé	poprvé	k6eAd1	poprvé
doloženo	doložit	k5eAaPmNgNnS	doložit
v	v	k7c6	v
Kosmově	Kosmův	k2eAgFnSc6d1	Kosmova
kronice	kronika	k1gFnSc6	kronika
Čechů	Čech	k1gMnPc2	Čech
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1055	[number]	k4	1055
<g/>
.	.	kIx.	.
</s>
<s>
Nářeční	nářeční	k2eAgInSc4d1	nářeční
tvar	tvar	k1gInSc4	tvar
s	s	k7c7	s
počátečním	počáteční	k2eAgInSc7d1	počáteční
h-	h-	k?	h-
(	(	kIx(	(
<g/>
v	v	k7c6	v
latinské	latinský	k2eAgFnSc6d1	Latinská
formě	forma	k1gFnSc6	forma
Holomucensis	Holomucensis	k1gFnSc2	Holomucensis
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
poprvé	poprvé	k6eAd1	poprvé
doložen	doložit	k5eAaPmNgInS	doložit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1208	[number]	k4	1208
<g/>
.	.	kIx.	.
</s>
<s>
Tradičním	tradiční	k2eAgInSc7d1	tradiční
problémem	problém	k1gInSc7	problém
je	být	k5eAaImIp3nS	být
rod	rod	k1gInSc1	rod
názvu	název	k1gInSc2	název
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
byla	být	k5eAaImAgNnP	být
všechna	všechen	k3xTgNnPc1	všechen
jména	jméno	k1gNnSc2	jméno
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
(	(	kIx(	(
<g/>
Boleslav	Boleslav	k1gMnSc1	Boleslav
<g/>
,	,	kIx,	,
Dobříš	Dobříš	k1gFnSc1	Dobříš
<g/>
,	,	kIx,	,
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
)	)	kIx)	)
mužského	mužský	k2eAgInSc2d1	mužský
rodu	rod	k1gInSc2	rod
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
si	se	k3xPyFc3	se
podržela	podržet	k5eAaPmAgFnS	podržet
nejméně	málo	k6eAd3	málo
do	do	k7c2	do
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
i	i	k9	i
déle	dlouho	k6eAd2	dlouho
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
většiny	většina	k1gFnSc2	většina
z	z	k7c2	z
těchto	tento	k3xDgNnPc2	tento
jmen	jméno	k1gNnPc2	jméno
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
postupně	postupně	k6eAd1	postupně
od	od	k7c2	od
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
sporadicky	sporadicky	k6eAd1	sporadicky
již	již	k6eAd1	již
od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
především	především	k9	především
v	v	k7c6	v
nářečí	nářečí	k1gNnSc6	nářečí
centrální	centrální	k2eAgFnSc2d1	centrální
části	část	k1gFnSc2	část
Čech	Čech	k1gMnSc1	Čech
změna	změna	k1gFnSc1	změna
z	z	k7c2	z
mužského	mužský	k2eAgInSc2d1	mužský
rodu	rod	k1gInSc2	rod
na	na	k7c4	na
ženský	ženský	k2eAgInSc4d1	ženský
<g/>
,	,	kIx,	,
vymizením	vymizení	k1gNnSc7	vymizení
měkkého	měkký	k2eAgInSc2d1	měkký
jeru	jer	k1gInSc2	jer
a	a	k8xC	a
příklonem	příklon	k1gInSc7	příklon
ke	k	k7c3	k
gramatickému	gramatický	k2eAgInSc3d1	gramatický
typu	typ	k1gInSc3	typ
obuv	obuv	k1gFnSc4	obuv
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
některých	některý	k3yIgNnPc6	některý
okrajových	okrajový	k2eAgNnPc6d1	okrajové
nářečích	nářečí	k1gNnPc6	nářečí
Čech	Čechy	k1gFnPc2	Čechy
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
i	i	k9	i
například	například	k6eAd1	například
v	v	k7c6	v
Dobříši	Dobříš	k1gFnSc6	Dobříš
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
a	a	k8xC	a
ve	v	k7c6	v
Slezsku	Slezsko	k1gNnSc6	Slezsko
si	se	k3xPyFc3	se
podržela	podržet	k5eAaPmAgFnS	podržet
mužský	mužský	k2eAgInSc4d1	mužský
rod	rod	k1gInSc4	rod
<g/>
.	.	kIx.	.
</s>
<s>
Hranici	hranice	k1gFnSc4	hranice
mezi	mezi	k7c7	mezi
mužským	mužský	k2eAgInSc7d1	mužský
a	a	k8xC	a
ženským	ženský	k2eAgInSc7d1	ženský
rodem	rod	k1gInSc7	rod
u	u	k7c2	u
jmen	jméno	k1gNnPc2	jméno
typu	typ	k1gInSc3	typ
Olomouc	Olomouc	k1gFnSc4	Olomouc
popsal	popsat	k5eAaPmAgMnS	popsat
R.	R.	kA	R.
Šrámek	Šrámek	k1gMnSc1	Šrámek
v	v	k7c6	v
publikaci	publikace	k1gFnSc6	publikace
L.	L.	kA	L.
Hosák	Hosák	k1gInSc1	Hosák
<g/>
,	,	kIx,	,
R.	R.	kA	R.
Šrámek	šrámek	k1gInSc1	šrámek
<g/>
:	:	kIx,	:
Místní	místní	k2eAgNnPc1d1	místní
jména	jméno	k1gNnPc1	jméno
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
a	a	k8xC	a
ve	v	k7c6	v
Slezsku	Slezsko	k1gNnSc6	Slezsko
II	II	kA	II
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
</s>
<s>
Ertlova	Ertlův	k2eAgFnSc1d1	Ertlova
kodifikace	kodifikace	k1gFnSc1	kodifikace
jmen	jméno	k1gNnPc2	jméno
typu	typ	k1gInSc2	typ
Olomouc	Olomouc	k1gFnSc1	Olomouc
stanovila	stanovit	k5eAaPmAgFnS	stanovit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
jako	jako	k8xC	jako
spisovný	spisovný	k2eAgInSc4d1	spisovný
pouze	pouze	k6eAd1	pouze
ženský	ženský	k2eAgInSc4d1	ženský
tvar	tvar	k1gInSc4	tvar
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
své	svůj	k3xOyFgFnPc4	svůj
teze	teze	k1gFnPc4	teze
<g/>
,	,	kIx,	,
že	že	k8xS	že
kdo	kdo	k3yQnSc1	kdo
porušuje	porušovat	k5eAaImIp3nS	porušovat
jednotu	jednota	k1gFnSc4	jednota
spisovné	spisovný	k2eAgFnSc2d1	spisovná
češtiny	čeština	k1gFnSc2	čeština
<g/>
,	,	kIx,	,
páše	páchat	k5eAaImIp3nS	páchat
největší	veliký	k2eAgInSc4d3	veliký
kulturní	kulturní	k2eAgInSc4d1	kulturní
hřích	hřích	k1gInSc4	hřích
na	na	k7c6	na
potomstvu	potomstvo	k1gNnSc6	potomstvo
<g/>
,	,	kIx,	,
a	a	k8xC	a
kulturní	kulturní	k2eAgFnSc1d1	kulturní
úroveň	úroveň	k1gFnSc1	úroveň
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
krajů	kraj	k1gInPc2	kraj
se	se	k3xPyFc4	se
nejlépe	dobře	k6eAd3	dobře
projevuje	projevovat	k5eAaImIp3nS	projevovat
dobrovolným	dobrovolný	k2eAgNnSc7d1	dobrovolné
podřizováním	podřizování	k1gNnSc7	podřizování
individuálních	individuální	k2eAgFnPc2d1	individuální
zvláštností	zvláštnost	k1gFnPc2	zvláštnost
místních	místní	k2eAgFnPc2d1	místní
jednotné	jednotný	k2eAgFnSc3d1	jednotná
spisovné	spisovný	k2eAgFnSc3d1	spisovná
češtině	čeština	k1gFnSc3	čeština
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
spisovné	spisovný	k2eAgFnSc6d1	spisovná
češtině	čeština	k1gFnSc6	čeština
je	být	k5eAaImIp3nS	být
Olomouc	Olomouc	k1gFnSc1	Olomouc
zásadně	zásadně	k6eAd1	zásadně
rodu	rod	k1gInSc2	rod
ženského	ženský	k2eAgInSc2d1	ženský
a	a	k8xC	a
skloňuje	skloňovat	k5eAaImIp3nS	skloňovat
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
vzoru	vzor	k1gInSc2	vzor
píseň	píseň	k1gFnSc1	píseň
(	(	kIx(	(
<g/>
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
jiná	jiný	k2eAgNnPc1d1	jiné
místní	místní	k2eAgNnPc1d1	místní
jména	jméno	k1gNnPc1	jméno
zakončená	zakončený	k2eAgNnPc1d1	zakončené
na	na	k7c4	na
měkkou	měkký	k2eAgFnSc4d1	měkká
souhlásku	souhláska	k1gFnSc4	souhláska
–	–	k?	–
Bystrc	Bystrc	k1gFnSc1	Bystrc
<g/>
,	,	kIx,	,
Třeboň	Třeboň	k1gFnSc1	Třeboň
<g/>
,	,	kIx,	,
Třebíč	Třebíč	k1gFnSc1	Třebíč
<g/>
,	,	kIx,	,
Dobříš	Dobříš	k1gFnSc1	Dobříš
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
místním	místní	k2eAgInSc6d1	místní
úzu	úzus	k1gInSc6	úzus
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
často	často	k6eAd1	často
používán	používat	k5eAaImNgInS	používat
rod	rod	k1gInSc1	rod
mužský	mužský	k2eAgInSc1d1	mužský
a	a	k8xC	a
skloňování	skloňování	k1gNnPc2	skloňování
podle	podle	k7c2	podle
vzoru	vzor	k1gInSc2	vzor
stroj	stroj	k1gInSc4	stroj
<g/>
,	,	kIx,	,
původní	původní	k2eAgInSc4d1	původní
a	a	k8xC	a
v	v	k7c6	v
nářečích	nářečí	k1gNnPc6	nářečí
zachovaný	zachovaný	k2eAgInSc4d1	zachovaný
mužský	mužský	k2eAgInSc4d1	mužský
rod	rod	k1gInSc4	rod
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
místními	místní	k2eAgMnPc7d1	místní
obyvateli	obyvatel	k1gMnPc7	obyvatel
proti	proti	k7c3	proti
spisovné	spisovný	k2eAgFnSc3d1	spisovná
gramatice	gramatika	k1gFnSc3	gramatika
vyžadován	vyžadován	k2eAgMnSc1d1	vyžadován
jako	jako	k8xC	jako
správný	správný	k2eAgMnSc1d1	správný
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
případě	případ	k1gInSc6	případ
nejistoty	nejistota	k1gFnSc2	nejistota
se	se	k3xPyFc4	se
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
užívání	užívání	k1gNnSc4	užívání
opisů	opis	k1gInPc2	opis
typu	typ	k1gInSc2	typ
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgFnPc6	který
se	se	k3xPyFc4	se
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
připouští	připouštět	k5eAaImIp3nS	připouštět
užití	užití	k1gNnSc3	užití
prvního	první	k4xOgInSc2	první
pádu	pád	k1gInSc2	pád
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
se	se	k3xPyFc4	se
olomoučtí	olomoucký	k2eAgMnPc1d1	olomoucký
radní	radní	k1gMnPc1	radní
obrátili	obrátit	k5eAaPmAgMnP	obrátit
na	na	k7c4	na
Zemský	zemský	k2eAgInSc4d1	zemský
úřad	úřad	k1gInSc4	úřad
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
v	v	k7c6	v
otázce	otázka	k1gFnSc6	otázka
jmenného	jmenný	k2eAgInSc2d1	jmenný
rodu	rod	k1gInSc2	rod
názvu	název	k1gInSc2	název
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
si	se	k3xPyFc3	se
nechal	nechat	k5eAaPmAgInS	nechat
vypracovat	vypracovat	k5eAaPmF	vypracovat
lingvistický	lingvistický	k2eAgInSc1d1	lingvistický
rozbor	rozbor	k1gInSc1	rozbor
u	u	k7c2	u
profesora	profesor	k1gMnSc2	profesor
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
univerzity	univerzita	k1gFnSc2	univerzita
Josefa	Josef	k1gMnSc2	Josef
Zubatého	zubatý	k2eAgMnSc2d1	zubatý
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
prokázal	prokázat	k5eAaPmAgInS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
správný	správný	k2eAgInSc1d1	správný
je	být	k5eAaImIp3nS	být
rod	rod	k1gInSc1	rod
ženský	ženský	k2eAgInSc1d1	ženský
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
bylo	být	k5eAaImAgNnS	být
i	i	k9	i
úředně	úředně	k6eAd1	úředně
potvrzeno	potvrdit	k5eAaPmNgNnS	potvrdit
ministerstvem	ministerstvo	k1gNnSc7	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
v	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nespisovné	spisovný	k2eNgFnSc6d1	nespisovná
řeči	řeč	k1gFnSc6	řeč
se	se	k3xPyFc4	se
také	také	k9	také
někdy	někdy	k6eAd1	někdy
užívá	užívat	k5eAaImIp3nS	užívat
slangových	slangový	k2eAgFnPc2d1	slangová
variant	varianta	k1gFnPc2	varianta
Olmík	Olmík	k1gInSc1	Olmík
(	(	kIx(	(
<g/>
mužského	mužský	k2eAgInSc2d1	mužský
rodu	rod	k1gInSc2	rod
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Olm	Olm	k1gFnSc1	Olm
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
vzoru	vzor	k1gInSc2	vzor
hrad	hrad	k1gInSc1	hrad
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ty	ten	k3xDgInPc1	ten
ale	ale	k9	ale
pochází	pocházet	k5eAaImIp3nS	pocházet
až	až	k9	až
ze	z	k7c2	z
sklonku	sklonek	k1gInSc2	sklonek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
používalo	používat	k5eAaImAgNnS	používat
už	už	k6eAd1	už
od	od	k7c2	od
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
znaku	znak	k1gInSc6	znak
i	i	k9	i
na	na	k7c6	na
pečetích	pečeť	k1gFnPc6	pečeť
moravské	moravský	k2eAgFnSc2d1	Moravská
šachované	šachovaný	k2eAgFnSc2d1	šachovaná
orlice	orlice	k1gFnSc2	orlice
<g/>
.	.	kIx.	.
</s>
<s>
Rozdílem	rozdíl	k1gInSc7	rozdíl
byl	být	k5eAaImAgInS	být
jen	jen	k9	jen
červený	červený	k2eAgInSc1d1	červený
jazyk	jazyk	k1gInSc1	jazyk
orlice	orlice	k1gFnSc1	orlice
<g/>
,	,	kIx,	,
moravská	moravský	k2eAgFnSc1d1	Moravská
má	mít	k5eAaImIp3nS	mít
zlatý	zlatý	k1gInSc4	zlatý
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1558	[number]	k4	1558
se	se	k3xPyFc4	se
začínají	začínat	k5eAaImIp3nP	začínat
objevovat	objevovat	k5eAaImF	objevovat
v	v	k7c6	v
rozích	roh	k1gInPc6	roh
majuskulní	majuskulní	k2eAgNnPc4d1	majuskulní
písmena	písmeno	k1gNnPc4	písmeno
SPQO	SPQO	kA	SPQO
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
Senatus	Senatus	k1gInSc1	Senatus
PopulusQue	PopulusQu	k1gFnSc2	PopulusQu
Olomuncesis	Olomuncesis	k1gFnSc2	Olomuncesis
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
senát	senát	k1gInSc1	senát
a	a	k8xC	a
lid	lid	k1gInSc1	lid
olomoucký	olomoucký	k2eAgInSc1d1	olomoucký
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
odkaz	odkaz	k1gInSc1	odkaz
na	na	k7c4	na
zkratku	zkratka	k1gFnSc4	zkratka
římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
SPQR	SPQR	kA	SPQR
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1758	[number]	k4	1758
znak	znak	k1gInSc4	znak
polepšila	polepšit	k5eAaPmAgFnS	polepšit
císařovna	císařovna	k1gFnSc1	císařovna
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc2	Terezie
přidáním	přidání	k1gNnSc7	přidání
babenberského	babenberský	k2eAgInSc2d1	babenberský
štítku	štítek	k1gInSc2	štítek
na	na	k7c4	na
prsa	prsa	k1gNnPc4	prsa
orlice	orlice	k1gFnSc2	orlice
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
památku	památka	k1gFnSc4	památka
neúspěšného	úspěšný	k2eNgNnSc2d1	neúspěšné
obléhání	obléhání	k1gNnSc2	obléhání
Prusy	Prus	k1gMnPc4	Prus
ovinut	ovinout	k5eAaPmNgInS	ovinout
zlatým	zlatý	k2eAgInSc7d1	zlatý
řetězem	řetěz	k1gInSc7	řetěz
a	a	k8xC	a
na	na	k7c6	na
kterém	který	k3yQgInSc6	který
bylo	být	k5eAaImAgNnS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
FMT	FMT	kA	FMT
(	(	kIx(	(
<g/>
iniciály	iniciála	k1gFnPc1	iniciála
císařů	císař	k1gMnPc2	císař
Františka	František	k1gMnSc2	František
I.	I.	kA	I.
a	a	k8xC	a
právě	právě	k6eAd1	právě
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc2	Terezie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
byly	být	k5eAaImAgFnP	být
tyto	tento	k3xDgFnPc1	tento
iniciály	iniciála	k1gFnPc1	iniciála
odstraněny	odstranit	k5eAaPmNgFnP	odstranit
a	a	k8xC	a
k	k	k7c3	k
žádosti	žádost	k1gFnSc3	žádost
města	město	k1gNnSc2	město
došlo	dojít	k5eAaPmAgNnS	dojít
roku	rok	k1gInSc2	rok
1934	[number]	k4	1934
k	k	k7c3	k
úplnému	úplný	k2eAgInSc3d1	úplný
návratu	návrat	k1gInSc3	návrat
k	k	k7c3	k
prosté	prostý	k2eAgFnSc3d1	prostá
šachované	šachovaný	k2eAgFnSc3d1	šachovaná
orlici	orlice	k1gFnSc3	orlice
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
se	se	k3xPyFc4	se
písmena	písmeno	k1gNnSc2	písmeno
SPQO	SPQO	kA	SPQO
vrátila	vrátit	k5eAaPmAgFnS	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
poslední	poslední	k2eAgFnSc2d1	poslední
úpravy	úprava	k1gFnSc2	úprava
znaku	znak	k1gInSc2	znak
je	být	k5eAaImIp3nS	být
známý	známý	k2eAgMnSc1d1	známý
olomoucký	olomoucký	k2eAgMnSc1d1	olomoucký
heraldik	heraldik	k1gMnSc1	heraldik
a	a	k8xC	a
přední	přední	k2eAgMnSc1d1	přední
znalec	znalec	k1gMnSc1	znalec
této	tento	k3xDgFnSc2	tento
problematiky	problematika	k1gFnSc2	problematika
<g/>
,	,	kIx,	,
plk.	plk.	kA	plk.
Jiří	Jiří	k1gMnSc1	Jiří
Louda	Louda	k1gMnSc1	Louda
<g/>
,	,	kIx,	,
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
<g/>
h.c.	h.c.	k?	h.c.
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
získalo	získat	k5eAaPmAgNnS	získat
město	město	k1gNnSc1	město
nové	nový	k2eAgNnSc1d1	nové
logo	logo	k1gNnSc1	logo
<g/>
.	.	kIx.	.
</s>
<s>
Základním	základní	k2eAgInSc7d1	základní
prvkem	prvek	k1gInSc7	prvek
logotypu	logotyp	k1gInSc2	logotyp
je	být	k5eAaImIp3nS	být
motiv	motiv	k1gInSc1	motiv
šachovnice	šachovnice	k1gFnSc2	šachovnice
vycházející	vycházející	k2eAgFnSc2d1	vycházející
z	z	k7c2	z
Moravské	moravský	k2eAgFnSc2d1	Moravská
orlice	orlice	k1gFnSc2	orlice
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
heraldického	heraldický	k2eAgInSc2d1	heraldický
znaku	znak	k1gInSc2	znak
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
čerpá	čerpat	k5eAaImIp3nS	čerpat
i	i	k9	i
červeno-bílá	červenoílý	k2eAgFnSc1d1	červeno-bílá
barevnost	barevnost	k1gFnSc1	barevnost
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
loga	logo	k1gNnSc2	logo
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
grafik	grafik	k1gMnSc1	grafik
Mgr.	Mgr.	kA	Mgr.
<g/>
Art	Art	k1gMnSc1	Art
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Kolář	Kolář	k1gMnSc1	Kolář
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc4	dějiny
Olomouce	Olomouc	k1gFnSc2	Olomouc
<g/>
.	.	kIx.	.
</s>
<s>
Významnými	významný	k2eAgFnPc7d1	významná
historickými	historický	k2eAgFnPc7d1	historická
událostmi	událost	k1gFnPc7	událost
Olomouce	Olomouc	k1gFnPc4	Olomouc
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Mladý	mladý	k2eAgInSc4d1	mladý
paleolit	paleolit	k1gInSc4	paleolit
(	(	kIx(	(
<g/>
40	[number]	k4	40
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
000	[number]	k4	000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
–	–	k?	–
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
nalezeny	nalezen	k2eAgInPc1d1	nalezen
kamenné	kamenný	k2eAgInPc1d1	kamenný
nástroje	nástroj	k1gInPc1	nástroj
na	na	k7c6	na
území	území	k1gNnSc6	území
města	město	k1gNnSc2	město
Neolit	neolit	k1gInSc1	neolit
–	–	k?	–
6	[number]	k4	6
000	[number]	k4	000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
–	–	k?	–
souvislé	souvislý	k2eAgFnSc2d1	souvislá
<g />
.	.	kIx.	.
</s>
<s>
osídlení	osídlení	k1gNnSc1	osídlení
<g/>
,	,	kIx,	,
nálezy	nález	k1gInPc1	nález
kultury	kultura	k1gFnSc2	kultura
s	s	k7c7	s
lineární	lineární	k2eAgFnSc7d1	lineární
keramikou	keramika	k1gFnSc7	keramika
1	[number]	k4	1
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
období	období	k1gNnSc2	období
markomanských	markomanský	k2eAgFnPc2d1	markomanská
válek	válka	k1gFnPc2	válka
(	(	kIx(	(
<g/>
166	[number]	k4	166
<g/>
-	-	kIx~	-
<g/>
180	[number]	k4	180
<g/>
)	)	kIx)	)
–	–	k?	–
objeveny	objeven	k2eAgInPc4d1	objeven
zbytky	zbytek	k1gInPc4	zbytek
římského	římský	k2eAgInSc2d1	římský
tábora	tábor	k1gInSc2	tábor
v	v	k7c6	v
lokalitě	lokalita	k1gFnSc6	lokalita
Olomouc-Neředín	Olomouc-Neředína	k1gFnPc2	Olomouc-Neředína
<g/>
,	,	kIx,	,
nejsevernější	severní	k2eAgInSc1d3	nejsevernější
doložený	doložený	k2eAgInSc1d1	doložený
pobyt	pobyt	k1gInSc1	pobyt
Římanů	Říman	k1gMnPc2	Říman
na	na	k7c6	na
území	území	k1gNnSc6	území
střední	střední	k2eAgFnSc2d1	střední
Evropy	Evropa	k1gFnSc2	Evropa
severně	severně	k6eAd1	severně
od	od	k7c2	od
Dunaje	Dunaj	k1gInSc2	Dunaj
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
příchod	příchod	k1gInSc1	příchod
prvních	první	k4xOgMnPc2	první
Slovanů	Slovan	k1gMnPc2	Slovan
<g/>
,	,	kIx,	,
vznik	vznik	k1gInSc4	vznik
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
osady	osada	k1gFnSc2	osada
(	(	kIx(	(
<g/>
nejstarší	starý	k2eAgNnSc1d3	nejstarší
trvale	trvale	k6eAd1	trvale
obydlené	obydlený	k2eAgNnSc1d1	obydlené
sídlo	sídlo	k1gNnSc1	sídlo
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
)	)	kIx)	)
konec	konec	k1gInSc1	konec
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
–	–	k?	–
vznik	vznik	k1gInSc4	vznik
opevněného	opevněný	k2eAgNnSc2d1	opevněné
slovanského	slovanský	k2eAgNnSc2d1	slovanské
hradiště	hradiště	k1gNnSc2	hradiště
(	(	kIx(	(
<g/>
nejstarší	starý	k2eAgFnSc1d3	nejstarší
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
)	)	kIx)	)
na	na	k7c6	na
předměstích	předměstí	k1gNnPc6	předměstí
Povel	povel	k1gInSc1	povel
a	a	k8xC	a
Nové	Nové	k2eAgInPc1d1	Nové
Sady	sad	k1gInPc1	sad
(	(	kIx(	(
<g/>
1,5	[number]	k4	1,5
km	km	kA	km
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
současného	současný	k2eAgNnSc2d1	současné
<g />
.	.	kIx.	.
</s>
<s>
historického	historický	k2eAgNnSc2d1	historické
centra	centrum	k1gNnSc2	centrum
<g/>
)	)	kIx)	)
počátek	počátek	k1gInSc1	počátek
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
–	–	k?	–
dobytí	dobytí	k1gNnSc2	dobytí
starého	starý	k2eAgNnSc2d1	staré
a	a	k8xC	a
postavení	postavení	k1gNnSc2	postavení
nového	nový	k2eAgNnSc2d1	nové
<g/>
,	,	kIx,	,
velkomoravského	velkomoravský	k2eAgMnSc2d1	velkomoravský
<g/>
,	,	kIx,	,
hradiště	hradiště	k1gNnPc1	hradiště
na	na	k7c6	na
Petrském	petrský	k2eAgNnSc6d1	Petrské
návrší	návrší	k1gNnSc6	návrší
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
–	–	k?	–
krátkodobý	krátkodobý	k2eAgInSc4d1	krátkodobý
zánik	zánik	k1gInSc4	zánik
a	a	k8xC	a
znovuobnovení	znovuobnovení	k1gNnSc4	znovuobnovení
olomouckého	olomoucký	k2eAgNnSc2d1	olomoucké
hradiště	hradiště	k1gNnSc2	hradiště
<g/>
,	,	kIx,	,
příliv	příliv	k1gInSc1	příliv
nových	nový	k2eAgMnPc2d1	nový
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
pravděpodobná	pravděpodobný	k2eAgFnSc1d1	pravděpodobná
souvislost	souvislost	k1gFnSc1	souvislost
se	s	k7c7	s
zánikem	zánik	k1gInSc7	zánik
Velkomoravské	velkomoravský	k2eAgFnSc2d1	Velkomoravská
říše	říš	k1gFnSc2	říš
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
polovina	polovina	k1gFnSc1	polovina
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
–	–	k?	–
vláda	vláda	k1gFnSc1	vláda
Přemyslovce	Přemyslovec	k1gMnSc4	Přemyslovec
Boleslava	Boleslav	k1gMnSc4	Boleslav
I.	I.	kA	I.
<g/>
,	,	kIx,	,
Olomouc	Olomouc	k1gFnSc1	Olomouc
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
vojensko-správních	vojenskoprávní	k2eAgNnPc2d1	vojensko-správní
středisek	středisko	k1gNnPc2	středisko
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
na	na	k7c6	na
trase	trasa	k1gFnSc6	trasa
transevropské	transevropský	k2eAgFnSc2d1	transevropská
obchodní	obchodní	k2eAgFnSc2d1	obchodní
magistrály	magistrála	k1gFnSc2	magistrála
Řezno	Řezno	k1gNnSc1	Řezno
<g/>
–	–	k?	–
<g/>
Praha	Praha	k1gFnSc1	Praha
<g/>
–	–	k?	–
<g/>
Libice	Libice	k1gFnPc4	Libice
<g/>
–	–	k?	–
<g/>
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
–	–	k?	–
<g/>
Krakov	Krakov	k1gInSc1	Krakov
<g/>
–	–	k?	–
<g/>
Kyjev	Kyjev	k1gInSc1	Kyjev
1019	[number]	k4	1019
–	–	k?	–
Morava	Morava	k1gFnSc1	Morava
připojena	připojen	k2eAgFnSc1d1	připojena
k	k	k7c3	k
přemyslovským	přemyslovský	k2eAgFnPc3d1	Přemyslovská
Čechám	Čechy	k1gFnPc3	Čechy
<g/>
,	,	kIx,	,
hlavním	hlavní	k2eAgNnSc7d1	hlavní
mocenským	mocenský	k2eAgNnSc7d1	mocenské
centrem	centrum	k1gNnSc7	centrum
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
sídlí	sídlet	k5eAaImIp3nS	sídlet
knížetem	kníže	k1gMnSc7	kníže
Oldřichem	Oldřich	k1gMnSc7	Oldřich
dosazený	dosazený	k2eAgMnSc1d1	dosazený
Břetislav	Břetislav	k1gMnSc1	Břetislav
<g/>
,	,	kIx,	,
budoucí	budoucí	k2eAgMnSc1d1	budoucí
český	český	k2eAgMnSc1d1	český
kníže	kníže	k1gMnSc1	kníže
1021	[number]	k4	1021
–	–	k?	–
Břetislav	Břetislav	k1gMnSc1	Břetislav
unesl	unést	k5eAaPmAgMnS	unést
z	z	k7c2	z
kláštera	klášter	k1gInSc2	klášter
ve	v	k7c6	v
Svinibrodu	Svinibrod	k1gInSc6	Svinibrod
dceru	dcera	k1gFnSc4	dcera
bavorského	bavorský	k2eAgMnSc2d1	bavorský
velmože	velmož	k1gMnSc2	velmož
Jitku	Jitka	k1gFnSc4	Jitka
a	a	k8xC	a
žil	žít	k5eAaImAgMnS	žít
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
na	na	k7c6	na
olomouckém	olomoucký	k2eAgInSc6d1	olomoucký
hradě	hrad	k1gInSc6	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Historici	historik	k1gMnPc1	historik
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
vedou	vést	k5eAaImIp3nP	vést
diskuze	diskuze	k1gFnPc1	diskuze
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
Břetislav	Břetislav	k1gMnSc1	Břetislav
sídlil	sídlit	k5eAaImAgMnS	sídlit
ještě	ještě	k6eAd1	ještě
na	na	k7c6	na
starším	starý	k2eAgNnSc6d2	starší
hradišti	hradiště	k1gNnSc6	hradiště
na	na	k7c6	na
Petrském	petrský	k2eAgNnSc6d1	Petrské
návrší	návrší	k1gNnSc6	návrší
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
Arcibiskupský	arcibiskupský	k2eAgInSc1d1	arcibiskupský
palác	palác	k1gInSc1	palác
a	a	k8xC	a
Tereziánská	tereziánský	k2eAgFnSc1d1	Tereziánská
zbrojnice	zbrojnice	k1gFnSc1	zbrojnice
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
už	už	k9	už
na	na	k7c6	na
Václavském	václavský	k2eAgNnSc6d1	Václavské
návrší	návrší	k1gNnSc6	návrší
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
stojí	stát	k5eAaImIp3nS	stát
katedrála	katedrála	k1gFnSc1	katedrála
a	a	k8xC	a
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
olomoucký	olomoucký	k2eAgInSc1d1	olomoucký
hrad	hrad	k1gInSc1	hrad
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
staletích	staletí	k1gNnPc6	staletí
1063	[number]	k4	1063
–	–	k?	–
v	v	k7c6	v
<g />
.	.	kIx.	.
</s>
<s>
Olomouci	Olomouc	k1gFnSc3	Olomouc
bylo	být	k5eAaImAgNnS	být
knížetem	kníže	k1gNnSc7wR	kníže
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
králem	král	k1gMnSc7	král
Vratislavem	Vratislav	k1gMnSc7	Vratislav
II	II	kA	II
<g/>
.	.	kIx.	.
obnoveno	obnoven	k2eAgNnSc1d1	obnoveno
biskupství	biskupství	k1gNnSc1	biskupství
kolem	kolem	k7c2	kolem
1070	[number]	k4	1070
–	–	k?	–
založení	založení	k1gNnSc2	založení
nového	nový	k2eAgInSc2d1	nový
kamenného	kamenný	k2eAgInSc2d1	kamenný
olomouckého	olomoucký	k2eAgInSc2d1	olomoucký
hradu	hrad	k1gInSc2	hrad
1077	[number]	k4	1077
-	-	kIx~	-
dorazila	dorazit	k5eAaPmAgFnS	dorazit
na	na	k7c4	na
Hradisko	hradisko	k1gNnSc4	hradisko
na	na	k7c4	na
pozvání	pozvání	k1gNnSc4	pozvání
olomouckého	olomoucký	k2eAgMnSc2d1	olomoucký
údělníka	údělník	k1gMnSc2	údělník
Oty	Ota	k1gMnSc2	Ota
I.	I.	kA	I.
Olomouckého	olomoucký	k2eAgInSc2d1	olomoucký
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
manželky	manželka	k1gFnSc2	manželka
Eufémie	eufémie	k1gFnSc2	eufémie
benediktinská	benediktinský	k2eAgFnSc1d1	benediktinská
kolonie	kolonie	k1gFnSc1	kolonie
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zde	zde	k6eAd1	zde
vybudovala	vybudovat	k5eAaPmAgFnS	vybudovat
klášter	klášter	k1gInSc4	klášter
"	"	kIx"	"
<g/>
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
Spasitele	spasitel	k1gMnSc2	spasitel
a	a	k8xC	a
svatého	svatý	k2eAgMnSc2d1	svatý
prvomučedníka	prvomučedník	k1gMnSc2	prvomučedník
Štěpána	Štěpán	k1gMnSc2	Štěpán
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
po	po	k7c6	po
několika	několik	k4yIc6	několik
měsících	měsíc	k1gInPc6	měsíc
vysvětil	vysvětit	k5eAaPmAgMnS	vysvětit
3	[number]	k4	3
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1078	[number]	k4	1078
olomoucký	olomoucký	k2eAgInSc1d1	olomoucký
biskup	biskup	k1gInSc1	biskup
Jan	Jan	k1gMnSc1	Jan
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
klášterní	klášterní	k2eAgFnSc4d1	klášterní
oratoř	oratoř	k1gFnSc4	oratoř
<g/>
.	.	kIx.	.
</s>
<s>
Stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
za	za	k7c2	za
přítomnosti	přítomnost	k1gFnSc2	přítomnost
obou	dva	k4xCgMnPc2	dva
fundátorů	fundátor	k1gMnPc2	fundátor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
českého	český	k2eAgNnSc2d1	české
knížete	kníže	k1gNnSc2wR	kníže
Vratislava	Vratislav	k1gMnSc2	Vratislav
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
břevnovského	břevnovský	k2eAgMnSc2d1	břevnovský
opata	opat	k1gMnSc2	opat
Meinharda	Meinhard	k1gMnSc2	Meinhard
a	a	k8xC	a
sázavského	sázavský	k2eAgMnSc2d1	sázavský
opata	opat	k1gMnSc2	opat
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
1201	[number]	k4	1201
–	–	k?	–
vymírá	vymírat	k5eAaImIp3nS	vymírat
olomoucká	olomoucký	k2eAgFnSc1d1	olomoucká
větev	větev	k1gFnSc1	větev
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
<g/>
,	,	kIx,	,
Moravu	Morava	k1gFnSc4	Morava
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
spravuje	spravovat	k5eAaImIp3nS	spravovat
markrabí	markrabí	k1gMnSc1	markrabí
1239	[number]	k4	1239
<g/>
–	–	k?	–
<g/>
48	[number]	k4	48
–	–	k?	–
lokace	lokace	k1gFnSc1	lokace
královského	královský	k2eAgNnSc2d1	královské
města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
Dolní	dolní	k2eAgFnSc1d1	dolní
a	a	k8xC	a
Horní	horní	k2eAgFnSc1d1	horní
nám.	nám.	k?	nám.
<g/>
)	)	kIx)	)
1306	[number]	k4	1306
–	–	k?	–
zavraždění	zavraždění	k1gNnSc2	zavraždění
krále	král	k1gMnSc2	král
Václava	Václav	k1gMnSc2	Václav
III	III	kA	III
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
vymření	vymření	k1gNnSc1	vymření
královské	královský	k2eAgFnSc2d1	královská
větve	větev	k1gFnSc2	větev
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
<g/>
)	)	kIx)	)
1314	[number]	k4	1314
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
král	král	k1gMnSc1	král
Jan	Jan	k1gMnSc1	Jan
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
privilegiem	privilegium	k1gNnSc7	privilegium
Olomouc	Olomouc	k1gFnSc1	Olomouc
jako	jako	k9	jako
první	první	k4xOgNnSc1	první
město	město	k1gNnSc1	město
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
1422	[number]	k4	1422
–	–	k?	–
král	král	k1gMnSc1	král
Zikmund	Zikmund	k1gMnSc1	Zikmund
Lucemburský	lucemburský	k2eAgMnSc1d1	lucemburský
povolil	povolit	k5eAaPmAgMnS	povolit
městu	město	k1gNnSc3	město
razit	razit	k5eAaImF	razit
vlastní	vlastní	k2eAgFnPc4d1	vlastní
mince	mince	k1gFnPc4	mince
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nejstarší	starý	k2eAgNnSc4d3	nejstarší
dochované	dochovaný	k2eAgNnSc4d1	dochované
mincovní	mincovní	k2eAgNnSc4d1	mincovní
privilegium	privilegium	k1gNnSc4	privilegium
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
1566	[number]	k4	1566
–	–	k?	–
příchod	příchod	k1gInSc4	příchod
jezuitů	jezuita	k1gMnPc2	jezuita
kvůli	kvůli	k7c3	kvůli
rekatolizaci	rekatolizace	k1gFnSc3	rekatolizace
<g/>
,	,	kIx,	,
zakládání	zakládání	k1gNnSc3	zakládání
škol	škola	k1gFnPc2	škola
a	a	k8xC	a
gymnázia	gymnázium	k1gNnSc2	gymnázium
<g/>
,	,	kIx,	,
jezuitské	jezuitský	k2eAgFnSc2d1	jezuitská
akademie	akademie	k1gFnSc2	akademie
1573	[number]	k4	1573
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
založena	založen	k2eAgFnSc1d1	založena
první	první	k4xOgFnSc1	první
univerzita	univerzita	k1gFnSc1	univerzita
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
,	,	kIx,	,
po	po	k7c6	po
revoluci	revoluce	k1gFnSc6	revoluce
roku	rok	k1gInSc2	rok
1848	[number]	k4	1848
postupně	postupně	k6eAd1	postupně
likvidována	likvidován	k2eAgFnSc1d1	likvidována
<g/>
,	,	kIx,	,
1860	[number]	k4	1860
<g/>
–	–	k?	–
<g/>
1946	[number]	k4	1946
zůstala	zůstat	k5eAaPmAgFnS	zůstat
pouze	pouze	k6eAd1	pouze
samostatná	samostatný	k2eAgFnSc1d1	samostatná
Teologická	teologický	k2eAgFnSc1d1	teologická
fakulta	fakulta	k1gFnSc1	fakulta
<g/>
)	)	kIx)	)
1642	[number]	k4	1642
<g/>
–	–	k?	–
<g/>
1650	[number]	k4	1650
okupace	okupace	k1gFnSc1	okupace
města	město	k1gNnSc2	město
švédskou	švédský	k2eAgFnSc7d1	švédská
armádou	armáda	k1gFnSc7	armáda
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
L.	L.	kA	L.
Torstensona	Torstensona	k1gFnSc1	Torstensona
(	(	kIx(	(
<g/>
třicetiletá	třicetiletý	k2eAgFnSc1d1	třicetiletá
válka	válka	k1gFnSc1	válka
<g/>
)	)	kIx)	)
1716	[number]	k4	1716
<g/>
–	–	k?	–
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1754	[number]	k4	1754
–	–	k?	–
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
sloup	sloup	k1gInSc1	sloup
Nejsvětější	nejsvětější	k2eAgFnSc2d1	nejsvětější
Trojice	trojice	k1gFnSc2	trojice
(	(	kIx(	(
<g/>
r.	r.	kA	r.
2000	[number]	k4	2000
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
památkou	památka	k1gFnSc7	památka
světového	světový	k2eAgNnSc2d1	světové
kulturního	kulturní	k2eAgNnSc2d1	kulturní
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
největší	veliký	k2eAgNnSc4d3	veliký
seskupení	seskupení	k1gNnSc4	seskupení
soch	socha	k1gFnPc2	socha
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
jedné	jeden	k4xCgFnSc2	jeden
skulptury	skulptura	k1gFnSc2	skulptura
<g/>
,	,	kIx,	,
stavba	stavba	k1gFnSc1	stavba
trvala	trvat	k5eAaImAgFnS	trvat
38	[number]	k4	38
let	léto	k1gNnPc2	léto
a	a	k8xC	a
během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
několik	několik	k4yIc1	několik
umělců	umělec	k1gMnPc2	umělec
1741	[number]	k4	1741
<g/>
–	–	k?	–
<g/>
42	[number]	k4	42
–	–	k?	–
okupace	okupace	k1gFnSc1	okupace
města	město	k1gNnSc2	město
pruskou	pruský	k2eAgFnSc7d1	pruská
armádou	armáda	k1gFnSc7	armáda
(	(	kIx(	(
<g/>
války	válka	k1gFnSc2	válka
o	o	k7c4	o
rakouské	rakouský	k2eAgNnSc4d1	rakouské
dědictví	dědictví	k1gNnSc4	dědictví
<g/>
)	)	kIx)	)
1746	[number]	k4	1746
–	–	k?	–
založena	založen	k2eAgFnSc1d1	založena
Societas	Societas	k1gInSc1	Societas
eruditorum	eruditorum	k1gNnSc1	eruditorum
incognitorum	incognitorum	k1gInSc1	incognitorum
in	in	k?	in
terris	terris	k1gFnSc2	terris
Austriacis	Austriacis	k1gFnSc2	Austriacis
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
učená	učený	k2eAgFnSc1d1	učená
společnost	společnost	k1gFnSc1	společnost
na	na	k7c6	na
území	území	k1gNnSc6	území
ovládaném	ovládaný	k2eAgNnSc6d1	ovládané
rakouskými	rakouský	k2eAgMnPc7d1	rakouský
Habsburky	Habsburk	k1gMnPc7	Habsburk
<g/>
.	.	kIx.	.
</s>
<s>
Časopis	časopis	k1gInSc1	časopis
společnosti	společnost	k1gFnSc2	společnost
Monatliche	Monatlich	k1gMnSc2	Monatlich
Auszüge	Auszüg	k1gMnSc2	Auszüg
byl	být	k5eAaImAgInS	být
prvním	první	k4xOgInSc7	první
vědeckým	vědecký	k2eAgInSc7d1	vědecký
časopisem	časopis	k1gInSc7	časopis
na	na	k7c6	na
území	území	k1gNnSc6	území
Habsburské	habsburský	k2eAgFnSc2d1	habsburská
monarchie	monarchie	k1gFnSc2	monarchie
<g/>
.	.	kIx.	.
1758	[number]	k4	1758
–	–	k?	–
Olomouc	Olomouc	k1gFnSc1	Olomouc
se	se	k3xPyFc4	se
ubránila	ubránit	k5eAaPmAgFnS	ubránit
druhému	druhý	k4xOgInSc3	druhý
pruskému	pruský	k2eAgNnSc3d1	pruské
obléhání	obléhání	k1gNnSc3	obléhání
<g/>
,	,	kIx,	,
královna	královna	k1gFnSc1	královna
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc1	Terezie
povýšila	povýšit	k5eAaPmAgFnS	povýšit
16	[number]	k4	16
měšťanů	měšťan	k1gMnPc2	měšťan
do	do	k7c2	do
šlechtického	šlechtický	k2eAgInSc2d1	šlechtický
stavu	stav	k1gInSc2	stav
<g/>
,	,	kIx,	,
vyplatila	vyplatit	k5eAaPmAgFnS	vyplatit
městu	město	k1gNnSc3	město
odškodnění	odškodnění	k1gNnSc1	odškodnění
<g/>
,	,	kIx,	,
polepšila	polepšit	k5eAaPmAgFnS	polepšit
městský	městský	k2eAgInSc4d1	městský
znak	znak	k1gInSc4	znak
a	a	k8xC	a
udělila	udělit	k5eAaPmAgFnS	udělit
Olomouci	Olomouc	k1gFnSc3	Olomouc
právo	právo	k1gNnSc4	právo
používat	používat	k5eAaImF	používat
titul	titul	k1gInSc4	titul
Královské	královský	k2eAgFnSc2d1	královská
<g />
.	.	kIx.	.
</s>
<s>
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
.	.	kIx.	.
1767	[number]	k4	1767
–	–	k?	–
za	za	k7c2	za
svého	svůj	k3xOyFgInSc2	svůj
pobytu	pobyt	k1gInSc2	pobyt
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
zde	zde	k6eAd1	zde
W.	W.	kA	W.
A.	A.	kA	A.
Mozart	Mozart	k1gMnSc1	Mozart
zkomponoval	zkomponovat	k5eAaPmAgMnS	zkomponovat
svoji	svůj	k3xOyFgFnSc4	svůj
6	[number]	k4	6
<g/>
.	.	kIx.	.
symfonii	symfonie	k1gFnSc4	symfonie
F-dur	Fura	k1gFnPc2	F-dura
1777	[number]	k4	1777
–	–	k?	–
biskupství	biskupství	k1gNnPc2	biskupství
olomoucké	olomoucký	k2eAgNnSc1d1	olomoucké
povýšeno	povýšen	k2eAgNnSc1d1	povýšeno
na	na	k7c4	na
arcibiskupství	arcibiskupství	k1gNnSc4	arcibiskupství
1829	[number]	k4	1829
–	–	k?	–
velitelem	velitel	k1gMnSc7	velitel
pevnosti	pevnost	k1gFnSc2	pevnost
Olomouc	Olomouc	k1gFnSc1	Olomouc
je	být	k5eAaImIp3nS	být
jmenován	jmenován	k2eAgMnSc1d1	jmenován
slavný	slavný	k2eAgMnSc1d1	slavný
maršál	maršál	k1gMnSc1	maršál
Radecký	Radecký	k1gMnSc1	Radecký
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
přepychovém	přepychový	k2eAgInSc6d1	přepychový
Edelmannově	Edelmannův	k2eAgInSc6d1	Edelmannův
paláci	palác	k1gInSc6	palác
na	na	k7c6	na
Horním	horní	k2eAgNnSc6d1	horní
<g />
.	.	kIx.	.
</s>
<s>
náměstí	náměstí	k1gNnSc1	náměstí
1841	[number]	k4	1841
–	–	k?	–
příjezd	příjezd	k1gInSc1	příjezd
prvního	první	k4xOgInSc2	první
vlaku	vlak	k1gInSc2	vlak
do	do	k7c2	do
města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
trasa	trasa	k1gFnSc1	trasa
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
–	–	k?	–
<g/>
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
)	)	kIx)	)
1845	[number]	k4	1845
–	–	k?	–
odjezd	odjezd	k1gInSc1	odjezd
prvního	první	k4xOgInSc2	první
vlaku	vlak	k1gInSc2	vlak
z	z	k7c2	z
Olomouce	Olomouc	k1gFnSc2	Olomouc
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
1905	[number]	k4	1905
–	–	k?	–
Eduard	Eduard	k1gMnSc1	Eduard
Konrád	Konrád	k1gMnSc1	Konrád
Zirm	Zirm	k1gMnSc1	Zirm
<g/>
,	,	kIx,	,
primář	primář	k1gMnSc1	primář
očního	oční	k2eAgNnSc2d1	oční
oddělení	oddělení	k1gNnSc2	oddělení
olomoucké	olomoucký	k2eAgFnSc2d1	olomoucká
nemocnice	nemocnice	k1gFnSc2	nemocnice
<g/>
,	,	kIx,	,
provedl	provést	k5eAaPmAgInS	provést
první	první	k4xOgFnSc4	první
úspěšnou	úspěšný	k2eAgFnSc4d1	úspěšná
transplantaci	transplantace	k1gFnSc4	transplantace
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
jednalo	jednat	k5eAaImAgNnS	jednat
<g />
.	.	kIx.	.
</s>
<s>
se	se	k3xPyFc4	se
o	o	k7c4	o
transplantaci	transplantace	k1gFnSc4	transplantace
oční	oční	k2eAgFnSc2d1	oční
rohovky	rohovka	k1gFnSc2	rohovka
1919	[number]	k4	1919
–	–	k?	–
připojením	připojení	k1gNnSc7	připojení
okolních	okolní	k2eAgFnPc2d1	okolní
obcí	obec	k1gFnPc2	obec
vzniká	vznikat	k5eAaImIp3nS	vznikat
Velká	velký	k2eAgFnSc1d1	velká
Olomouc	Olomouc	k1gFnSc1	Olomouc
1939	[number]	k4	1939
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
–	–	k?	–
okupace	okupace	k1gFnSc1	okupace
města	město	k1gNnSc2	město
německou	německý	k2eAgFnSc7d1	německá
armádou	armáda	k1gFnSc7	armáda
<g/>
,	,	kIx,	,
připojení	připojení	k1gNnSc4	připojení
k	k	k7c3	k
Protektorátu	protektorát	k1gInSc3	protektorát
Čechy	Čechy	k1gFnPc1	Čechy
a	a	k8xC	a
Morava	Morava	k1gFnSc1	Morava
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1939	[number]	k4	1939
zatčeno	zatknout	k5eAaPmNgNnS	zatknout
800	[number]	k4	800
židovských	židovský	k2eAgMnPc2d1	židovský
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
někteří	některý	k3yIgMnPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
byli	být	k5eAaImAgMnP	být
převezeni	převézt	k5eAaPmNgMnP	převézt
do	do	k7c2	do
Dachau	Dachaus	k1gInSc2	Dachaus
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1942	[number]	k4	1942
až	až	k8xS	až
březnu	březen	k1gInSc6	březen
1945	[number]	k4	1945
odvezeno	odvézt	k5eAaPmNgNnS	odvézt
3498	[number]	k4	3498
Židů	Žid	k1gMnPc2	Žid
do	do	k7c2	do
Terezína	Terezín	k1gInSc2	Terezín
a	a	k8xC	a
do	do	k7c2	do
vyhlazovacích	vyhlazovací	k2eAgInPc2d1	vyhlazovací
táborů	tábor	k1gInPc2	tábor
na	na	k7c6	na
východě	východ	k1gInSc6	východ
<g/>
)	)	kIx)	)
1946	[number]	k4	1946
–	–	k?	–
obnovení	obnovení	k1gNnSc4	obnovení
univerzity	univerzita	k1gFnSc2	univerzita
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Univerzita	univerzita	k1gFnSc1	univerzita
Palackého	Palacký	k1gMnSc2	Palacký
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
čítající	čítající	k2eAgFnSc2d1	čítající
osm	osm	k4xCc1	osm
fakult	fakulta	k1gFnPc2	fakulta
1974	[number]	k4	1974
<g/>
–	–	k?	–
<g/>
1980	[number]	k4	1980
připojením	připojení	k1gNnSc7	připojení
15	[number]	k4	15
okolních	okolní	k2eAgFnPc2d1	okolní
obcí	obec	k1gFnPc2	obec
vzrostla	vzrůst	k5eAaPmAgFnS	vzrůst
plošná	plošný	k2eAgFnSc1d1	plošná
rozloha	rozloha	k1gFnSc1	rozloha
města	město	k1gNnSc2	město
<g />
.	.	kIx.	.
</s>
<s>
na	na	k7c4	na
trojnásobek	trojnásobek	k1gInSc4	trojnásobek
1990	[number]	k4	1990
–	–	k?	–
obnovení	obnovení	k1gNnSc1	obnovení
pomníku	pomník	k1gInSc2	pomník
T.	T.	kA	T.
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
jako	jako	k8xS	jako
symbolu	symbol	k1gInSc2	symbol
návratu	návrat	k1gInSc2	návrat
k	k	k7c3	k
demokracii	demokracie	k1gFnSc3	demokracie
1995	[number]	k4	1995
–	–	k?	–
navštěva	navštěvo	k1gNnSc2	navštěvo
papeže	papež	k1gMnSc2	papež
Jana	Jan	k1gMnSc2	Jan
Pavla	Pavel	k1gMnSc2	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
svatořečil	svatořečit	k5eAaBmAgMnS	svatořečit
zde	zde	k6eAd1	zde
blahoslavenou	blahoslavený	k2eAgFnSc4d1	blahoslavená
Zdislavu	Zdislava	k1gFnSc4	Zdislava
a	a	k8xC	a
Jana	Jan	k1gMnSc4	Jan
Sarkandera	Sarkander	k1gMnSc4	Sarkander
(	(	kIx(	(
<g/>
na	na	k7c6	na
stotisícovém	stotisícový	k2eAgNnSc6d1	stotisícové
shromáždění	shromáždění	k1gNnSc6	shromáždění
na	na	k7c6	na
letišti	letiště	k1gNnSc6	letiště
v	v	k7c6	v
Neředíně	Neředína	k1gFnSc6	Neředína
<g/>
)	)	kIx)	)
1997	[number]	k4	1997
–	–	k?	–
město	město	k1gNnSc4	město
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
katastrofální	katastrofální	k2eAgFnSc1d1	katastrofální
povodeň	povodeň	k1gFnSc1	povodeň
<g/>
,	,	kIx,	,
nejničivější	ničivý	k2eAgFnSc1d3	nejničivější
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
historii	historie	k1gFnSc6	historie
Geografická	geografický	k2eAgFnSc1d1	geografická
poloha	poloha	k1gFnSc1	poloha
<g/>
:	:	kIx,	:
Olomouc	Olomouc	k1gFnSc1	Olomouc
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgNnSc4d3	veliký
město	město	k1gNnSc4	město
ležící	ležící	k2eAgNnSc4d1	ležící
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
se	se	k3xPyFc4	se
v	v	k7c6	v
Hornomoravském	hornomoravský	k2eAgInSc6d1	hornomoravský
úvalu	úval	k1gInSc6	úval
v	v	k7c6	v
nivě	niva	k1gFnSc6	niva
řeky	řeka	k1gFnSc2	řeka
Moravy	Morava	k1gFnSc2	Morava
při	při	k7c6	při
soutoku	soutok	k1gInSc6	soutok
s	s	k7c7	s
Bystřicí	Bystřice	k1gFnSc7	Bystřice
zleva	zleva	k6eAd1	zleva
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
města	město	k1gNnSc2	město
a	a	k8xC	a
Mlýnským	mlýnský	k2eAgInSc7d1	mlýnský
potokem	potok	k1gInSc7	potok
zprava	zprava	k6eAd1	zprava
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Obklopena	obklopen	k2eAgFnSc1d1	obklopena
je	být	k5eAaImIp3nS	být
úrodnou	úrodný	k2eAgFnSc7d1	úrodná
krajinou	krajina	k1gFnSc7	krajina
Hané	Haná	k1gFnSc2	Haná
<g/>
.	.	kIx.	.
</s>
<s>
Charakter	charakter	k1gInSc1	charakter
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
rovinatý	rovinatý	k2eAgInSc1d1	rovinatý
<g/>
,	,	kIx,	,
na	na	k7c6	na
západě	západ	k1gInSc6	západ
a	a	k8xC	a
hlavně	hlavně	k6eAd1	hlavně
na	na	k7c6	na
východě	východ	k1gInSc6	východ
výrazně	výrazně	k6eAd1	výrazně
ohraničen	ohraničit	k5eAaPmNgInS	ohraničit
vyšším	vysoký	k2eAgInSc7d2	vyšší
georeliéfem	georeliéf	k1gInSc7	georeliéf
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
tak	tak	k6eAd1	tak
uzavírá	uzavírat	k5eAaPmIp3nS	uzavírat
město	město	k1gNnSc1	město
do	do	k7c2	do
protáhlé	protáhlý	k2eAgFnSc2d1	protáhlá
sníženiny	sníženina	k1gFnSc2	sníženina
otevřené	otevřený	k2eAgFnSc2d1	otevřená
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
severozápad	severozápad	k1gInSc1	severozápad
<g/>
–	–	k?	–
<g/>
jihovýchod	jihovýchod	k1gInSc1	jihovýchod
<g/>
.	.	kIx.	.
</s>
<s>
Střed	střed	k1gInSc1	střed
města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
49	[number]	k4	49
<g/>
°	°	k?	°
<g/>
35	[number]	k4	35
<g/>
́	́	k?	́
severní	severní	k2eAgFnSc2d1	severní
šířky	šířka	k1gFnSc2	šířka
a	a	k8xC	a
17	[number]	k4	17
<g/>
°	°	k?	°
<g/>
15	[number]	k4	15
<g/>
́	́	k?	́
východní	východní	k2eAgFnSc2d1	východní
délky	délka	k1gFnSc2	délka
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
219	[number]	k4	219
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
jižní	jižní	k2eAgFnSc1d1	jižní
část	část	k1gFnSc1	část
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
mírně	mírně	k6eAd1	mírně
snižuje	snižovat	k5eAaImIp3nS	snižovat
do	do	k7c2	do
nadmořské	nadmořský	k2eAgFnSc2d1	nadmořská
výšky	výška	k1gFnSc2	výška
208	[number]	k4	208
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
severovýchodní	severovýchodní	k2eAgFnSc1d1	severovýchodní
část	část	k1gFnSc1	část
se	se	k3xPyFc4	se
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
až	až	k9	až
na	na	k7c4	na
nadmořskou	nadmořský	k2eAgFnSc4d1	nadmořská
výšku	výška	k1gFnSc4	výška
420	[number]	k4	420
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Průměrný	průměrný	k2eAgInSc1d1	průměrný
roční	roční	k2eAgInSc1d1	roční
úhrn	úhrn	k1gInSc1	úhrn
srážek	srážka	k1gFnPc2	srážka
na	na	k7c6	na
území	území	k1gNnSc6	území
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
600	[number]	k4	600
<g/>
–	–	k?	–
<g/>
1100	[number]	k4	1100
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Průměrné	průměrný	k2eAgFnPc1d1	průměrná
teploty	teplota	k1gFnPc1	teplota
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
hodnot	hodnota	k1gFnPc2	hodnota
od	od	k7c2	od
-1	-1	k4	-1
do	do	k7c2	do
-4	-4	k4	-4
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
občas	občas	k6eAd1	občas
spadne	spadnout	k5eAaPmIp3nS	spadnout
teplota	teplota	k1gFnSc1	teplota
až	až	k9	až
k	k	k7c3	k
-15	-15	k4	-15
°	°	k?	°
<g/>
C.	C.	kA	C.
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
je	být	k5eAaImIp3nS	být
průměrná	průměrný	k2eAgFnSc1d1	průměrná
teplota	teplota	k1gFnSc1	teplota
19	[number]	k4	19
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
výjimkou	výjimka	k1gFnSc7	výjimka
však	však	k9	však
nejsou	být	k5eNaImIp3nP	být
ani	ani	k8xC	ani
dny	den	k1gInPc1	den
s	s	k7c7	s
teplotami	teplota	k1gFnPc7	teplota
okolo	okolo	k7c2	okolo
35	[number]	k4	35
°	°	k?	°
<g/>
C.	C.	kA	C.
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1237	[number]	k4	1237
město	město	k1gNnSc4	město
Olomouc	Olomouc	k1gFnSc1	Olomouc
i	i	k9	i
s	s	k7c7	s
jeho	jeho	k3xOp3gNnPc7	jeho
předměstími	předměstí	k1gNnPc7	předměstí
obývalo	obývat	k5eAaImAgNnS	obývat
16	[number]	k4	16
300	[number]	k4	300
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1622	[number]	k4	1622
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
na	na	k7c4	na
mor	mor	k1gInSc4	mor
14	[number]	k4	14
tisíc	tisíc	k4xCgInSc4	tisíc
lidí	člověk	k1gMnPc2	člověk
(	(	kIx(	(
<g/>
neuvedeno	uveden	k2eNgNnSc1d1	neuvedeno
zda	zda	k8xS	zda
jen	jen	k9	jen
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ještě	ještě	k9	ještě
před	před	k7c7	před
švédskou	švédský	k2eAgFnSc7d1	švédská
okupací	okupace	k1gFnSc7	okupace
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
asi	asi	k9	asi
30	[number]	k4	30
tisíc	tisíc	k4xCgInSc4	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
Švédů	Švéd	k1gMnPc2	Švéd
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1650	[number]	k4	1650
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
jen	jen	k9	jen
1	[number]	k4	1
675	[number]	k4	675
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Mezi	mezi	k7c7	mezi
roky	rok	k1gInPc7	rok
1713	[number]	k4	1713
<g/>
–	–	k?	–
<g/>
1715	[number]	k4	1715
propukla	propuknout	k5eAaPmAgFnS	propuknout
další	další	k2eAgFnSc1d1	další
morová	morový	k2eAgFnSc1d1	morová
epidemie	epidemie	k1gFnSc1	epidemie
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
přežilo	přežít	k5eAaPmAgNnS	přežít
1	[number]	k4	1
500	[number]	k4	500
Olomoučanů	Olomoučan	k1gMnPc2	Olomoučan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1900	[number]	k4	1900
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
21	[number]	k4	21
933	[number]	k4	933
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
asi	asi	k9	asi
15	[number]	k4	15
tisíc	tisíc	k4xCgInPc2	tisíc
Němců	Němec	k1gMnPc2	Němec
a	a	k8xC	a
6	[number]	k4	6
tisíc	tisíc	k4xCgInPc2	tisíc
Čechů	Čech	k1gMnPc2	Čech
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1910	[number]	k4	1910
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
22	[number]	k4	22
245	[number]	k4	245
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
kromě	kromě	k7c2	kromě
vojenské	vojenský	k2eAgFnSc2d1	vojenská
posádky	posádka	k1gFnSc2	posádka
12	[number]	k4	12
tisíc	tisíc	k4xCgInPc2	tisíc
Němců	Němec	k1gMnPc2	Němec
a	a	k8xC	a
necelých	celý	k2eNgInPc2d1	necelý
7	[number]	k4	7
tisíc	tisíc	k4xCgInPc2	tisíc
Čechů	Čech	k1gMnPc2	Čech
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vytvoření	vytvoření	k1gNnSc6	vytvoření
Velké	velký	k2eAgFnSc2d1	velká
Olomouce	Olomouc	k1gFnSc2	Olomouc
zde	zde	k6eAd1	zde
k	k	k7c3	k
roku	rok	k1gInSc3	rok
1920	[number]	k4	1920
žilo	žít	k5eAaImAgNnS	žít
už	už	k6eAd1	už
57	[number]	k4	57
206	[number]	k4	206
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
téměř	téměř	k6eAd1	téměř
40	[number]	k4	40
tisíc	tisíc	k4xCgInPc2	tisíc
Čechů	Čech	k1gMnPc2	Čech
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
ve	v	k7c4	v
4	[number]	k4	4
484	[number]	k4	484
domech	dům	k1gInPc6	dům
66	[number]	k4	66
440	[number]	k4	440
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
47	[number]	k4	47
861	[number]	k4	861
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
hlásilo	hlásit	k5eAaImAgNnS	hlásit
k	k	k7c3	k
československé	československý	k2eAgFnSc3d1	Československá
národnosti	národnost	k1gFnSc3	národnost
a	a	k8xC	a
15	[number]	k4	15
017	[number]	k4	017
k	k	k7c3	k
německé	německý	k2eAgFnSc2d1	německá
<g/>
.	.	kIx.	.
</s>
<s>
Žilo	žít	k5eAaImAgNnS	žít
zde	zde	k6eAd1	zde
47	[number]	k4	47
771	[number]	k4	771
římských	římský	k2eAgMnPc2d1	římský
katolíků	katolík	k1gMnPc2	katolík
<g/>
,	,	kIx,	,
2	[number]	k4	2
603	[number]	k4	603
evangelíků	evangelík	k1gMnPc2	evangelík
<g/>
,	,	kIx,	,
9	[number]	k4	9
192	[number]	k4	192
příslušníků	příslušník	k1gMnPc2	příslušník
Církve	církev	k1gFnSc2	církev
československé	československý	k2eAgFnSc2d1	Československá
husitské	husitský	k2eAgFnSc2d1	husitská
a	a	k8xC	a
2	[number]	k4	2
198	[number]	k4	198
židů	žid	k1gMnPc2	žid
<g/>
.	.	kIx.	.
</s>
<s>
Velkoměstem	velkoměsto	k1gNnSc7	velkoměsto
se	se	k3xPyFc4	se
Olomouc	Olomouc	k1gFnSc1	Olomouc
stala	stát	k5eAaPmAgFnS	stát
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
roku	rok	k1gInSc2	rok
1978	[number]	k4	1978
<g/>
,	,	kIx,	,
největšího	veliký	k2eAgInSc2d3	veliký
počtu	počet	k1gInSc2	počet
<g/>
,	,	kIx,	,
107	[number]	k4	107
399	[number]	k4	399
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
ale	ale	k9	ale
začal	začít	k5eAaPmAgInS	začít
počet	počet	k1gInSc1	počet
olomouckých	olomoucký	k2eAgMnPc2d1	olomoucký
občanů	občan	k1gMnPc2	občan
postupně	postupně	k6eAd1	postupně
klesat	klesat	k5eAaImF	klesat
a	a	k8xC	a
poprvé	poprvé	k6eAd1	poprvé
byl	být	k5eAaImAgInS	být
pokles	pokles	k1gInSc1	pokles
pod	pod	k7c4	pod
hranici	hranice	k1gFnSc4	hranice
statisíce	statisíce	k1gInPc4	statisíce
obyvatel	obyvatel	k1gMnSc1	obyvatel
statistickým	statistický	k2eAgInSc7d1	statistický
úřadem	úřad	k1gInSc7	úřad
zjištěn	zjistit	k5eAaPmNgInS	zjistit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
se	se	k3xPyFc4	se
Olomouc	Olomouc	k1gFnSc1	Olomouc
vrátila	vrátit	k5eAaPmAgFnS	vrátit
mezi	mezi	k7c4	mezi
velkoměsta	velkoměsto	k1gNnPc4	velkoměsto
opětovným	opětovný	k2eAgNnSc7d1	opětovné
překonáním	překonání	k1gNnSc7	překonání
hranice	hranice	k1gFnSc1	hranice
100	[number]	k4	100
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Aglomerace	aglomerace	k1gFnSc1	aglomerace
Olomouce	Olomouc	k1gFnSc2	Olomouc
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
vymezena	vymezit	k5eAaPmNgFnS	vymezit
poměrně	poměrně	k6eAd1	poměrně
široce	široko	k6eAd1	široko
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
okolních	okolní	k2eAgFnPc2d1	okolní
obcí	obec	k1gFnPc2	obec
a	a	k8xC	a
nejbližších	blízký	k2eAgNnPc2d3	nejbližší
měst	město	k1gNnPc2	město
Prostějov	Prostějov	k1gInSc1	Prostějov
<g/>
,	,	kIx,	,
Přerov	Přerov	k1gInSc1	Přerov
<g/>
,	,	kIx,	,
Lipník	Lipník	k1gInSc1	Lipník
nad	nad	k7c7	nad
Bečvou	Bečva	k1gFnSc7	Bečva
<g/>
,	,	kIx,	,
Šternberk	Šternberk	k1gInSc1	Šternberk
a	a	k8xC	a
Litovel	Litovel	k1gFnSc1	Litovel
(	(	kIx(	(
<g/>
jádro	jádro	k1gNnSc1	jádro
polycentrické	polycentrický	k2eAgFnSc2d1	polycentrická
aglomerace	aglomerace	k1gFnSc2	aglomerace
se	se	k3xPyFc4	se
zhruba	zhruba	k6eAd1	zhruba
360	[number]	k4	360
tisíci	tisíc	k4xCgInPc7	tisíc
obyvateli	obyvatel	k1gMnPc7	obyvatel
<g/>
)	)	kIx)	)
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
byly	být	k5eAaImAgFnP	být
zahrnuty	zahrnout	k5eAaPmNgFnP	zahrnout
i	i	k9	i
vzdálenější	vzdálený	k2eAgNnPc1d2	vzdálenější
města	město	k1gNnPc1	město
jako	jako	k9	jako
jsou	být	k5eAaImIp3nP	být
Hranice	hranice	k1gFnSc1	hranice
<g/>
,	,	kIx,	,
Uničov	Uničov	k1gInSc1	Uničov
či	či	k8xC	či
Mohelnice	Mohelnice	k1gFnSc1	Mohelnice
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
vymezenou	vymezený	k2eAgFnSc4d1	vymezená
aglomeraci	aglomerace	k1gFnSc4	aglomerace
obývá	obývat	k5eAaImIp3nS	obývat
zhruba	zhruba	k6eAd1	zhruba
450	[number]	k4	450
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
činí	činit	k5eAaImIp3nS	činit
71	[number]	k4	71
%	%	kIx~	%
obyvatel	obyvatel	k1gMnSc1	obyvatel
celého	celý	k2eAgInSc2d1	celý
Olomouckého	olomoucký	k2eAgInSc2d1	olomoucký
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
představitelů	představitel	k1gMnPc2	představitel
Olomouce	Olomouc	k1gFnSc2	Olomouc
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
uzákonění	uzákonění	k1gNnSc2	uzákonění
obecního	obecní	k2eAgInSc2d1	obecní
řádu	řád	k1gInSc2	řád
města	město	k1gNnSc2	město
Olomouce	Olomouc	k1gFnSc2	Olomouc
6	[number]	k4	6
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1850	[number]	k4	1850
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
bylo	být	k5eAaImAgNnS	být
vedení	vedení	k1gNnSc1	vedení
města	město	k1gNnSc2	město
čistě	čistě	k6eAd1	čistě
německé	německý	k2eAgNnSc1d1	německé
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
let	léto	k1gNnPc2	léto
1878	[number]	k4	1878
<g/>
–	–	k?	–
<g/>
1886	[number]	k4	1886
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgMnS	být
členem	člen	k1gMnSc7	člen
městské	městský	k2eAgFnSc2d1	městská
rady	rada	k1gFnSc2	rada
Čech	Čech	k1gMnSc1	Čech
F.	F.	kA	F.
Stejskal	Stejskal	k1gMnSc1	Stejskal
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Češi	Čech	k1gMnPc1	Čech
tvořili	tvořit	k5eAaImAgMnP	tvořit
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
menšinu	menšina	k1gFnSc4	menšina
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
v	v	k7c6	v
obecních	obecní	k2eAgFnPc6d1	obecní
volbách	volba	k1gFnPc6	volba
(	(	kIx(	(
<g/>
i	i	k8xC	i
kvůli	kvůli	k7c3	kvůli
volebnímu	volební	k2eAgInSc3d1	volební
cenzu	cenzus	k1gInSc3	cenzus
<g/>
)	)	kIx)	)
neměli	mít	k5eNaImAgMnP	mít
šanci	šance	k1gFnSc4	šance
být	být	k5eAaImF	být
zvoleni	zvolen	k2eAgMnPc1d1	zvolen
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
až	až	k9	až
k	k	k7c3	k
úplnému	úplný	k2eAgInSc3d1	úplný
bojkotu	bojkot	k1gInSc3	bojkot
obecních	obecní	k2eAgFnPc2d1	obecní
voleb	volba	k1gFnPc2	volba
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
českého	český	k2eAgNnSc2d1	české
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Nejdéle	dlouho	k6eAd3	dlouho
úřadujícím	úřadující	k2eAgMnSc7d1	úřadující
představitelem	představitel	k1gMnSc7	představitel
Olomouce	Olomouc	k1gFnSc2	Olomouc
v	v	k7c6	v
moderních	moderní	k2eAgFnPc6d1	moderní
dějinách	dějiny	k1gFnPc6	dějiny
byl	být	k5eAaImAgMnS	být
Josef	Josef	k1gMnSc1	Josef
Engel	Engel	k1gMnSc1	Engel
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
funkci	funkce	k1gFnSc4	funkce
starosty	starosta	k1gMnSc2	starosta
zastával	zastávat	k5eAaImAgMnS	zastávat
od	od	k7c2	od
listopadu	listopad	k1gInSc2	listopad
1872	[number]	k4	1872
až	až	k9	až
do	do	k7c2	do
října	říjen	k1gInSc2	říjen
1896	[number]	k4	1896
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
téměř	téměř	k6eAd1	téměř
24	[number]	k4	24
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Éra	éra	k1gFnSc1	éra
českých	český	k2eAgMnPc2d1	český
starostů	starosta	k1gMnPc2	starosta
začala	začít	k5eAaPmAgFnS	začít
až	až	k9	až
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
rozšířeno	rozšířit	k5eAaPmNgNnS	rozšířit
připojením	připojení	k1gNnSc7	připojení
mnoha	mnoho	k4c2	mnoho
okolních	okolní	k2eAgFnPc2d1	okolní
obcí	obec	k1gFnPc2	obec
a	a	k8xC	a
kdy	kdy	k6eAd1	kdy
v	v	k7c6	v
sloučené	sloučený	k2eAgFnSc6d1	sloučená
Velké	velký	k2eAgFnSc6d1	velká
Olomouci	Olomouc	k1gFnSc6	Olomouc
převádlo	převádnout	k5eAaPmAgNnS	převádnout
etnicky	etnicky	k6eAd1	etnicky
české	český	k2eAgNnSc1d1	české
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
(	(	kIx(	(
<g/>
došlo	dojít	k5eAaPmAgNnS	dojít
zároveň	zároveň	k6eAd1	zároveň
k	k	k7c3	k
zavedení	zavedení	k1gNnSc3	zavedení
všeobecného	všeobecný	k2eAgNnSc2d1	všeobecné
a	a	k8xC	a
rovného	rovný	k2eAgNnSc2d1	rovné
volebního	volební	k2eAgNnSc2d1	volební
práva	právo	k1gNnSc2	právo
pro	pro	k7c4	pro
komunální	komunální	k2eAgFnPc4d1	komunální
volby	volba	k1gFnPc4	volba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
nacistické	nacistický	k2eAgFnSc2d1	nacistická
okupace	okupace	k1gFnSc2	okupace
stáli	stát	k5eAaImAgMnP	stát
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
města	město	k1gNnSc2	město
němečtí	německý	k2eAgMnPc1d1	německý
vládní	vládní	k2eAgMnPc1d1	vládní
komisaři	komisar	k1gMnPc1	komisar
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
se	se	k3xPyFc4	se
struktura	struktura	k1gFnSc1	struktura
městské	městský	k2eAgFnSc2d1	městská
samosprávy	samospráva	k1gFnSc2	samospráva
změnila	změnit	k5eAaPmAgFnS	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšším	vysoký	k2eAgMnSc7d3	nejvyšší
představitelem	představitel	k1gMnSc7	představitel
města	město	k1gNnSc2	město
byl	být	k5eAaImAgMnS	být
předseda	předseda	k1gMnSc1	předseda
národního	národní	k2eAgInSc2d1	národní
výboru	výbor	k1gInSc2	výbor
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
výlučně	výlučně	k6eAd1	výlučně
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
členů	člen	k1gMnPc2	člen
KSČ	KSČ	kA	KSČ
<g/>
.	.	kIx.	.
</s>
<s>
Struktura	struktura	k1gFnSc1	struktura
národních	národní	k2eAgInPc2d1	národní
výborů	výbor	k1gInPc2	výbor
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
Olomouce	Olomouc	k1gFnSc2	Olomouc
primátor	primátor	k1gMnSc1	primátor
volený	volený	k2eAgMnSc1d1	volený
zastupitelstvem	zastupitelstvo	k1gNnSc7	zastupitelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
komunálních	komunální	k2eAgFnPc2d1	komunální
voleb	volba	k1gFnPc2	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
je	být	k5eAaImIp3nS	být
primátorem	primátor	k1gMnSc7	primátor
sociální	sociální	k2eAgMnSc1d1	sociální
demokrat	demokrat	k1gMnSc1	demokrat
Antonín	Antonín	k1gMnSc1	Antonín
Staněk	Staněk	k1gMnSc1	Staněk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnových	říjnový	k2eAgFnPc6d1	říjnová
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
ČSSD	ČSSD	kA	ČSSD
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
tak	tak	k6eAd1	tak
získala	získat	k5eAaPmAgFnS	získat
17	[number]	k4	17
mandátů	mandát	k1gInPc2	mandát
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
uspěli	uspět	k5eAaPmAgMnP	uspět
ODS	ODS	kA	ODS
(	(	kIx(	(
<g/>
14	[number]	k4	14
mandátů	mandát	k1gInPc2	mandát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
TOP	topit	k5eAaImRp2nS	topit
09	[number]	k4	09
(	(	kIx(	(
<g/>
6	[number]	k4	6
mandátů	mandát	k1gInPc2	mandát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
KSČM	KSČM	kA	KSČM
(	(	kIx(	(
<g/>
5	[number]	k4	5
mandátů	mandát	k1gInPc2	mandát
<g/>
)	)	kIx)	)
a	a	k8xC	a
KDU-ČSL	KDU-ČSL	k1gMnSc1	KDU-ČSL
(	(	kIx(	(
<g/>
3	[number]	k4	3
mandáty	mandát	k1gInPc4	mandát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Senátní	senátní	k2eAgFnPc4d1	senátní
volby	volba	k1gFnPc4	volba
v	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
kole	kolo	k1gNnSc6	kolo
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
Martin	Martin	k1gMnSc1	Martin
Tesařík	Tesařík	k1gMnSc1	Tesařík
za	za	k7c2	za
ČSSD	ČSSD	kA	ČSSD
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
byla	být	k5eAaImAgFnS	být
19	[number]	k4	19
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
na	na	k7c6	na
radnici	radnice	k1gFnSc6	radnice
podepsána	podepsán	k2eAgFnSc1d1	podepsána
koaliční	koaliční	k2eAgFnSc1d1	koaliční
smlouva	smlouva	k1gFnSc1	smlouva
bez	bez	k7c2	bez
vítězné	vítězný	k2eAgFnSc2d1	vítězná
strany	strana	k1gFnSc2	strana
mezi	mezi	k7c7	mezi
ODS	ODS	kA	ODS
<g/>
,	,	kIx,	,
TOP	topit	k5eAaImRp2nS	topit
09	[number]	k4	09
a	a	k8xC	a
KDU-ČSL	KDU-ČSL	k1gMnSc1	KDU-ČSL
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
změn	změna	k1gFnPc2	změna
je	být	k5eAaImIp3nS	být
snížení	snížení	k1gNnSc1	snížení
počtu	počet	k1gInSc2	počet
náměstků	náměstek	k1gMnPc2	náměstek
z	z	k7c2	z
dosavadních	dosavadní	k2eAgInPc2d1	dosavadní
šesti	šest	k4xCc2	šest
na	na	k7c4	na
pět	pět	k4xCc4	pět
<g/>
.	.	kIx.	.
</s>
<s>
Primátorem	primátor	k1gMnSc7	primátor
byl	být	k5eAaImAgMnS	být
Martin	Martin	k1gMnSc1	Martin
Novotný	Novotný	k1gMnSc1	Novotný
<g/>
;	;	kIx,	;
ODS	ODS	kA	ODS
s	s	k7c7	s
TOP	topit	k5eAaImRp2nS	topit
09	[number]	k4	09
mají	mít	k5eAaImIp3nP	mít
dva	dva	k4xCgMnPc4	dva
náměstky	náměstek	k1gMnPc4	náměstek
<g/>
,	,	kIx,	,
KDU-ČSL	KDU-ČSL	k1gMnPc4	KDU-ČSL
jednoho	jeden	k4xCgMnSc4	jeden
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
sobotu	sobota	k1gFnSc4	sobota
1	[number]	k4	1
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2014	[number]	k4	2014
Martin	Martin	k1gMnSc1	Martin
Novotný	Novotný	k1gMnSc1	Novotný
odstoupil	odstoupit	k5eAaPmAgMnS	odstoupit
z	z	k7c2	z
funkce	funkce	k1gFnSc2	funkce
primátora	primátor	k1gMnSc2	primátor
a	a	k8xC	a
zastupitelé	zastupitel	k1gMnPc1	zastupitel
zvolili	zvolit	k5eAaPmAgMnP	zvolit
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
místo	místo	k1gNnSc4	místo
Martina	Martin	k1gMnSc2	Martin
Majora	major	k1gMnSc2	major
<g/>
.	.	kIx.	.
</s>
<s>
Martin	Martin	k1gMnSc1	Martin
Novotný	Novotný	k1gMnSc1	Novotný
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
stal	stát	k5eAaPmAgInS	stát
neuvolněným	uvolněný	k2eNgMnSc7d1	neuvolněný
členem	člen	k1gMnSc7	člen
Rady	rada	k1gFnSc2	rada
města	město	k1gNnSc2	město
Olomouce	Olomouc	k1gFnSc2	Olomouc
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
náměstků	náměstek	k1gMnPc2	náměstek
primátora	primátor	k1gMnSc2	primátor
tak	tak	k8xS	tak
klesl	klesnout	k5eAaPmAgInS	klesnout
o	o	k7c4	o
jednoho	jeden	k4xCgMnSc4	jeden
<g/>
.	.	kIx.	.
</s>
<s>
Vo	Vo	k?	Vo
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
od	od	k7c2	od
sametové	sametový	k2eAgFnSc2d1	sametová
revoluce	revoluce	k1gFnSc2	revoluce
volilo	volit	k5eAaImAgNnS	volit
v	v	k7c6	v
jediném	jediný	k2eAgInSc6d1	jediný
obvodu	obvod	k1gInSc6	obvod
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
komplikace	komplikace	k1gFnPc4	komplikace
při	při	k7c6	při
organizaci	organizace	k1gFnSc6	organizace
voleb	volba	k1gFnPc2	volba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
vedlo	vést	k5eAaImAgNnS	vést
ke	k	k7c3	k
snížení	snížení	k1gNnSc3	snížení
přirozeného	přirozený	k2eAgInSc2d1	přirozený
volebního	volební	k2eAgInSc2d1	volební
prahu	práh	k1gInSc2	práh
a	a	k8xC	a
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
tak	tak	k6eAd1	tak
lépe	dobře	k6eAd2	dobře
uspět	uspět	k5eAaPmF	uspět
menším	malý	k2eAgInPc3d2	menší
volebním	volební	k2eAgInPc3d1	volební
subjektům	subjekt	k1gInPc3	subjekt
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
zvítězilo	zvítězit	k5eAaPmAgNnS	zvítězit
hnutí	hnutí	k1gNnSc1	hnutí
ANO	ano	k9	ano
2011	[number]	k4	2011
se	s	k7c7	s
ziskem	zisk	k1gInSc7	zisk
12	[number]	k4	12
mandátů	mandát	k1gInPc2	mandát
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
uspěli	uspět	k5eAaPmAgMnP	uspět
ČSSD	ČSSD	kA	ČSSD
(	(	kIx(	(
<g/>
10	[number]	k4	10
mandátů	mandát	k1gInPc2	mandát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ODS	ODS	kA	ODS
(	(	kIx(	(
<g/>
5	[number]	k4	5
mandátů	mandát	k1gInPc2	mandát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
KDU-ČSL	KDU-ČSL	k1gMnSc1	KDU-ČSL
(	(	kIx(	(
<g/>
5	[number]	k4	5
mandátů	mandát	k1gInPc2	mandát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
TOP	topit	k5eAaImRp2nS	topit
09	[number]	k4	09
(	(	kIx(	(
<g/>
4	[number]	k4	4
mandáty	mandát	k1gInPc4	mandát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
KSČM	KSČM	kA	KSČM
(	(	kIx(	(
<g/>
4	[number]	k4	4
mandáty	mandát	k1gInPc4	mandát
<g/>
)	)	kIx)	)
a	a	k8xC	a
volen	volit	k5eAaImNgInS	volit
sdružení	sdružený	k2eAgMnPc1d1	sdružený
Občané	občan	k1gMnPc1	občan
pro	pro	k7c4	pro
Olomouc	Olomouc	k1gFnSc4	Olomouc
(	(	kIx(	(
<g/>
nezávislý	závislý	k2eNgInSc1d1	nezávislý
<g/>
,	,	kIx,	,
SZ	SZ	kA	SZ
a	a	k8xC	a
Piráti	pirát	k1gMnPc1	pirát
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
3	[number]	k4	3
mandáty	mandát	k1gInPc4	mandát
<g/>
)	)	kIx)	)
a	a	k8xC	a
nová	nový	k2eAgFnSc1d1	nová
lokální	lokální	k2eAgFnSc1d1	lokální
strana	strana	k1gFnSc1	strana
Pro	pro	k7c4	pro
Olomouc	Olomouc	k1gFnSc4	Olomouc
(	(	kIx(	(
<g/>
2	[number]	k4	2
mandáty	mandát	k1gInPc4	mandát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Necelých	celý	k2eNgFnPc2d1	necelá
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
po	po	k7c6	po
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
výsledku	výsledek	k1gInSc3	výsledek
voleb	volba	k1gFnPc2	volba
<g/>
,	,	kIx,	,
uzavřeli	uzavřít	k5eAaPmAgMnP	uzavřít
koaliční	koaliční	k2eAgFnSc4d1	koaliční
dohodu	dohoda	k1gFnSc4	dohoda
ČSSD	ČSSD	kA	ČSSD
<g/>
,	,	kIx,	,
KDU-ČSL	KDU-ČSL	k1gFnSc1	KDU-ČSL
<g/>
,	,	kIx,	,
ODS	ODS	kA	ODS
<g/>
,	,	kIx,	,
TOP	topit	k5eAaImRp2nS	topit
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Olomouc	Olomouc	k1gFnSc1	Olomouc
se	se	k3xPyFc4	se
člení	členit	k5eAaImIp3nS	členit
na	na	k7c4	na
26	[number]	k4	26
částí	část	k1gFnPc2	část
města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
zároveň	zároveň	k6eAd1	zároveň
katastrální	katastrální	k2eAgNnPc1d1	katastrální
území	území	k1gNnPc1	území
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
původním	původní	k2eAgFnPc3d1	původní
historickým	historický	k2eAgFnPc3d1	historická
obcím	obec	k1gFnPc3	obec
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
katastrální	katastrální	k2eAgFnSc1d1	katastrální
hranice	hranice	k1gFnSc1	hranice
se	se	k3xPyFc4	se
již	již	k6eAd1	již
poněkud	poněkud	k6eAd1	poněkud
liší	lišit	k5eAaImIp3nP	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
byly	být	k5eAaImAgFnP	být
součástí	součást	k1gFnSc7	součást
města	město	k1gNnSc2	město
i	i	k8xC	i
dnes	dnes	k6eAd1	dnes
samostatné	samostatný	k2eAgFnPc1d1	samostatná
obce	obec	k1gFnPc1	obec
Křelov-Břuchotín	Křelov-Břuchotína	k1gFnPc2	Křelov-Břuchotína
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
–	–	k?	–
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bystrovany	Bystrovan	k1gMnPc7	Bystrovan
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
–	–	k?	–
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
a	a	k8xC	a
Samotišky	Samotišek	k1gInPc1	Samotišek
(	(	kIx(	(
<g/>
1974	[number]	k4	1974
<g/>
–	–	k?	–
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
23	[number]	k4	23
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2007	[number]	k4	2007
je	být	k5eAaImIp3nS	být
stanoveno	stanovit	k5eAaPmNgNnS	stanovit
27	[number]	k4	27
komisí	komise	k1gFnPc2	komise
městských	městský	k2eAgFnPc2d1	městská
částí	část	k1gFnPc2	část
<g/>
.	.	kIx.	.
</s>
<s>
Komise	komise	k1gFnSc1	komise
městských	městský	k2eAgFnPc2d1	městská
částí	část	k1gFnPc2	část
nejsou	být	k5eNaImIp3nP	být
komisemi	komise	k1gFnPc7	komise
správních	správní	k2eAgInPc2d1	správní
celků	celek	k1gInPc2	celek
v	v	k7c6	v
pravém	pravý	k2eAgInSc6d1	pravý
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
poradní	poradní	k2eAgInPc1d1	poradní
orgány	orgán	k1gInPc1	orgán
rady	rada	k1gFnSc2	rada
města	město	k1gNnSc2	město
Olomouce	Olomouc	k1gFnSc2	Olomouc
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
jejich	jejich	k3xOp3gFnSc1	jejich
územní	územní	k2eAgFnSc1d1	územní
působnost	působnost	k1gFnSc1	působnost
zcela	zcela	k6eAd1	zcela
neodpovídá	odpovídat	k5eNaImIp3nS	odpovídat
územím	území	k1gNnSc7	území
místních	místní	k2eAgFnPc2d1	místní
částí	část	k1gFnPc2	část
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnosti	podrobnost	k1gFnPc1	podrobnost
jsou	být	k5eAaImIp3nP	být
uvedeny	uvést	k5eAaPmNgFnP	uvést
ve	v	k7c6	v
statutu	statut	k1gInSc6	statut
města	město	k1gNnSc2	město
Olomouce	Olomouc	k1gFnSc2	Olomouc
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Okres	okres	k1gInSc1	okres
Olomouc	Olomouc	k1gFnSc1	Olomouc
a	a	k8xC	a
Obvod	obvod	k1gInSc1	obvod
obce	obec	k1gFnSc2	obec
s	s	k7c7	s
rozšířenou	rozšířený	k2eAgFnSc7d1	rozšířená
působností	působnost	k1gFnSc7	působnost
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
.	.	kIx.	.
</s>
<s>
Olomouc	Olomouc	k1gFnSc1	Olomouc
je	být	k5eAaImIp3nS	být
krajským	krajský	k2eAgNnSc7d1	krajské
městem	město	k1gNnSc7	město
a	a	k8xC	a
také	také	k9	také
obcí	obec	k1gFnPc2	obec
s	s	k7c7	s
rozšířenou	rozšířený	k2eAgFnSc7d1	rozšířená
působností	působnost	k1gFnSc7	působnost
a	a	k8xC	a
pověřeným	pověřený	k2eAgInSc7d1	pověřený
obecním	obecní	k2eAgInSc7d1	obecní
úřadem	úřad	k1gInSc7	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Okres	okres	k1gInSc1	okres
Olomouc	Olomouc	k1gFnSc1	Olomouc
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
95	[number]	k4	95
obcí	obec	k1gFnPc2	obec
<g/>
,	,	kIx,	,
ORP	ORP	kA	ORP
z	z	k7c2	z
45	[number]	k4	45
obcí	obec	k1gFnPc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
Olomoucký	olomoucký	k2eAgInSc1d1	olomoucký
kraj	kraj	k1gInSc1	kraj
spravuje	spravovat	k5eAaImIp3nS	spravovat
krajský	krajský	k2eAgInSc1d1	krajský
úřad	úřad	k1gInSc1	úřad
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc1	jeho
odbory	odbor	k1gInPc1	odbor
(	(	kIx(	(
<g/>
dopravy	doprava	k1gFnSc2	doprava
<g/>
,	,	kIx,	,
zdravotnictví	zdravotnictví	k1gNnSc2	zdravotnictví
<g/>
,	,	kIx,	,
školství	školství	k1gNnSc2	školství
<g/>
,	,	kIx,	,
sociální	sociální	k2eAgFnSc1d1	sociální
<g/>
,	,	kIx,	,
dotační	dotační	k2eAgFnSc1d1	dotační
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
a	a	k8xC	a
správu	správa	k1gFnSc4	správa
vlastního	vlastní	k2eAgNnSc2d1	vlastní
města	město	k1gNnSc2	město
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
Magistrát	magistrát	k1gInSc1	magistrát
statutárního	statutární	k2eAgNnSc2d1	statutární
města	město	k1gNnSc2	město
Olomouce	Olomouc	k1gFnSc2	Olomouc
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
celostátních	celostátní	k2eAgFnPc2d1	celostátní
institucí	instituce	k1gFnPc2	instituce
je	být	k5eAaImIp3nS	být
Olomouc	Olomouc	k1gFnSc1	Olomouc
oficiálně	oficiálně	k6eAd1	oficiálně
sídlem	sídlo	k1gNnSc7	sídlo
České	český	k2eAgFnSc2d1	Česká
lékařské	lékařský	k2eAgFnSc2d1	lékařská
komory	komora	k1gFnSc2	komora
nebo	nebo	k8xC	nebo
Agrární	agrární	k2eAgFnSc2d1	agrární
komory	komora	k1gFnSc2	komora
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
zde	zde	k6eAd1	zde
sídlí	sídlet	k5eAaImIp3nS	sídlet
např.	např.	kA	např.
územní	územní	k2eAgFnSc1d1	územní
odborné	odborný	k2eAgNnSc4d1	odborné
pracoviště	pracoviště	k1gNnSc4	pracoviště
Národního	národní	k2eAgInSc2d1	národní
památkového	památkový	k2eAgInSc2d1	památkový
ústavu	ústav	k1gInSc2	ústav
<g/>
,	,	kIx,	,
pracoviště	pracoviště	k1gNnSc4	pracoviště
Agentury	agentura	k1gFnSc2	agentura
ochrany	ochrana	k1gFnSc2	ochrana
přírody	příroda	k1gFnSc2	příroda
a	a	k8xC	a
krajiny	krajina	k1gFnSc2	krajina
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
Hasičský	hasičský	k2eAgInSc1d1	hasičský
záchranný	záchranný	k2eAgInSc1d1	záchranný
sbor	sbor	k1gInSc1	sbor
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
Olomouckého	olomoucký	k2eAgInSc2d1	olomoucký
kraje	kraj	k1gInSc2	kraj
či	či	k8xC	či
Krajská	krajský	k2eAgFnSc1d1	krajská
hygienická	hygienický	k2eAgFnSc1d1	hygienická
stanice	stanice	k1gFnSc1	stanice
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
významným	významný	k2eAgNnSc7d1	významné
církevním	církevní	k2eAgNnSc7d1	církevní
centrem	centrum	k1gNnSc7	centrum
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
potvrzuje	potvrzovat	k5eAaImIp3nS	potvrzovat
Arcibiskupství	arcibiskupství	k1gNnSc4	arcibiskupství
olomoucké	olomoucký	k2eAgFnSc2d1	olomoucká
<g/>
,	,	kIx,	,
povýšené	povýšený	k2eAgFnSc2d1	povýšená
roku	rok	k1gInSc2	rok
1777	[number]	k4	1777
z	z	k7c2	z
původního	původní	k2eAgNnSc2d1	původní
biskupství	biskupství	k1gNnSc2	biskupství
založeného	založený	k2eAgNnSc2d1	založené
už	už	k6eAd1	už
roku	rok	k1gInSc2	rok
1063	[number]	k4	1063
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
Československa	Československo	k1gNnSc2	Československo
byla	být	k5eAaImAgFnS	být
zřízena	zřídit	k5eAaPmNgFnS	zřídit
také	také	k9	také
Olomoucká	olomoucký	k2eAgFnSc1d1	olomoucká
diecéze	diecéze	k1gFnSc1	diecéze
Církve	církev	k1gFnSc2	církev
československé	československý	k2eAgFnSc2d1	Československá
husitské	husitský	k2eAgFnSc2d1	husitská
<g/>
.	.	kIx.	.
</s>
<s>
Olomouc	Olomouc	k1gFnSc1	Olomouc
je	být	k5eAaImIp3nS	být
kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
tradičním	tradiční	k2eAgNnSc7d1	tradiční
vojenským	vojenský	k2eAgNnSc7d1	vojenské
městem	město	k1gNnSc7	město
<g/>
,	,	kIx,	,
sídlilo	sídlit	k5eAaImAgNnS	sídlit
zde	zde	k6eAd1	zde
Velitelství	velitelství	k1gNnSc1	velitelství
pozemního	pozemní	k2eAgNnSc2d1	pozemní
vojska	vojsko	k1gNnSc2	vojsko
a	a	k8xC	a
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2003	[number]	k4	2003
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
sídlo	sídlo	k1gNnSc1	sídlo
Velitelství	velitelství	k1gNnPc2	velitelství
společných	společný	k2eAgFnPc2d1	společná
sil	síla	k1gFnPc2	síla
Armády	armáda	k1gFnSc2	armáda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
umístěno	umístit	k5eAaPmNgNnS	umístit
i	i	k9	i
velitelství	velitelství	k1gNnSc1	velitelství
Vojenské	vojenský	k2eAgFnSc2d1	vojenská
policie	policie	k1gFnSc2	policie
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
justičních	justiční	k2eAgFnPc2d1	justiční
institucí	instituce	k1gFnPc2	instituce
je	být	k5eAaImIp3nS	být
významným	významný	k2eAgInSc7d1	významný
Vrchní	vrchní	k2eAgInSc1d1	vrchní
soud	soud	k1gInSc1	soud
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Vrchním	vrchní	k2eAgNnSc7d1	vrchní
státním	státní	k2eAgNnSc7d1	státní
zastupitelstvím	zastupitelství	k1gNnSc7	zastupitelství
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
mají	mít	k5eAaImIp3nP	mít
působnost	působnost	k1gFnSc4	působnost
na	na	k7c6	na
prakticky	prakticky	k6eAd1	prakticky
celém	celý	k2eAgNnSc6d1	celé
území	území	k1gNnSc6	území
Moravy	Morava	k1gFnSc2	Morava
a	a	k8xC	a
Slezska	Slezsko	k1gNnSc2	Slezsko
<g/>
.	.	kIx.	.
</s>
<s>
Sídlí	sídlet	k5eAaImIp3nS	sídlet
zde	zde	k6eAd1	zde
také	také	k9	také
Okresní	okresní	k2eAgInSc1d1	okresní
soud	soud	k1gInSc1	soud
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
příslušným	příslušný	k2eAgNnSc7d1	příslušné
okresním	okresní	k2eAgNnSc7d1	okresní
státním	státní	k2eAgNnSc7d1	státní
zastupitelstvím	zastupitelství	k1gNnSc7	zastupitelství
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
přes	přes	k7c4	přes
existenci	existence	k1gFnSc4	existence
samosprávného	samosprávný	k2eAgInSc2d1	samosprávný
Olomouckého	olomoucký	k2eAgInSc2d1	olomoucký
kraje	kraj	k1gInSc2	kraj
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
samostatný	samostatný	k2eAgInSc1d1	samostatný
krajský	krajský	k2eAgInSc1d1	krajský
soud	soud	k1gInSc1	soud
<g/>
,	,	kIx,	,
působí	působit	k5eAaImIp3nS	působit
zde	zde	k6eAd1	zde
jen	jen	k9	jen
pobočka	pobočka	k1gFnSc1	pobočka
Krajského	krajský	k2eAgInSc2d1	krajský
soudu	soud	k1gInSc2	soud
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
a	a	k8xC	a
ostravského	ostravský	k2eAgNnSc2d1	ostravské
krajského	krajský	k2eAgNnSc2d1	krajské
zastupitelství	zastupitelství	k1gNnSc2	zastupitelství
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
okresního	okresní	k2eAgInSc2d1	okresní
soudu	soud	k1gInSc2	soud
je	být	k5eAaImIp3nS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
vazební	vazební	k2eAgFnSc1d1	vazební
věznice	věznice	k1gFnSc1	věznice
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
rozhlas	rozhlas	k1gInSc1	rozhlas
Olomouc	Olomouc	k1gFnSc1	Olomouc
Rádio	rádio	k1gNnSc1	rádio
Haná	Haná	k1gFnSc1	Haná
Hitrádio	Hitrádio	k1gNnSc1	Hitrádio
apollo	apolnout	k5eAaPmAgNnS	apolnout
Rádio	rádio	k1gNnSc4	rádio
Hity	hit	k1gInPc1	hit
–	–	k?	–
Olomouc	Olomouc	k1gFnSc1	Olomouc
Rádio	rádio	k1gNnSc1	rádio
Rubi	Rub	k1gFnSc2	Rub
Radio	radio	k1gNnSc1	radio
Proglas	Proglasa	k1gFnPc2	Proglasa
–	–	k?	–
studio	studio	k1gNnSc1	studio
Radim	Radim	k1gMnSc1	Radim
Rádio	rádio	k1gNnSc1	rádio
Čas	čas	k1gInSc4	čas
Moravské	moravský	k2eAgNnSc1d1	Moravské
divadlo	divadlo	k1gNnSc1	divadlo
Olomouc	Olomouc	k1gFnSc1	Olomouc
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
divadlo	divadlo	k1gNnSc1	divadlo
Oldřicha	Oldřich	k1gMnSc2	Oldřich
Stibora	Stibor	k1gMnSc2	Stibor
<g/>
)	)	kIx)	)
Divadlo	divadlo	k1gNnSc1	divadlo
na	na	k7c4	na
cucky	cucek	k1gInPc4	cucek
–	–	k?	–
nezávislý	závislý	k2eNgInSc1d1	nezávislý
divadelní	divadelní	k2eAgInSc1d1	divadelní
soubor	soubor	k1gInSc1	soubor
s	s	k7c7	s
vlastní	vlastní	k2eAgFnSc7d1	vlastní
scénou	scéna	k1gFnSc7	scéna
<g/>
,	,	kIx,	,
progresivní	progresivní	k2eAgFnSc7d1	progresivní
dramaturgií	dramaturgie	k1gFnSc7	dramaturgie
a	a	k8xC	a
s	s	k7c7	s
širokým	široký	k2eAgNnSc7d1	široké
spektrem	spektrum	k1gNnSc7	spektrum
aktivit	aktivita	k1gFnPc2	aktivita
<g />
.	.	kIx.	.
</s>
<s>
<g/>
;	;	kIx,	;
produkci	produkce	k1gFnSc3	produkce
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
DW7	DW7	k1gFnSc1	DW7
o.p.s.	o.p.s.	k?	o.p.s.
Divadlo	divadlo	k1gNnSc1	divadlo
hudby	hudba	k1gFnSc2	hudba
–	–	k?	–
olomoucké	olomoucký	k2eAgNnSc1d1	olomoucké
stagionové	stagionový	k2eAgNnSc1d1	stagionový
divadlo	divadlo	k1gNnSc1	divadlo
Moravská	moravský	k2eAgFnSc1d1	Moravská
filharmonie	filharmonie	k1gFnSc1	filharmonie
–	–	k?	–
pořádá	pořádat	k5eAaImIp3nS	pořádat
mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
hudební	hudební	k2eAgInSc4d1	hudební
festival	festival	k1gInSc4	festival
Olomoucké	olomoucký	k2eAgNnSc4d1	olomoucké
hudební	hudební	k2eAgNnSc4d1	hudební
jaro	jaro	k1gNnSc4	jaro
a	a	k8xC	a
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
varhanní	varhanní	k2eAgInSc1d1	varhanní
festival	festival	k1gInSc1	festival
Divadlo	divadlo	k1gNnSc1	divadlo
Tramtarie	Tramtarie	k1gFnSc2	Tramtarie
–	–	k?	–
nezávislé	závislý	k2eNgNnSc4d1	nezávislé
divadlo	divadlo	k1gNnSc4	divadlo
se	s	k7c7	s
stálým	stálý	k2eAgInSc7d1	stálý
divadelním	divadelní	k2eAgInSc7d1	divadelní
souborem	soubor	k1gInSc7	soubor
malé	malý	k2eAgFnSc2d1	malá
formy	forma	k1gFnSc2	forma
s	s	k7c7	s
autorskou	autorský	k2eAgFnSc7d1	autorská
dramaturgií	dramaturgie	k1gFnSc7	dramaturgie
Slovanský	slovanský	k2eAgInSc1d1	slovanský
tyátr	tyátr	k1gInSc1	tyátr
–	–	k?	–
divadelní	divadelní	k2eAgInSc1d1	divadelní
soubor	soubor	k1gInSc1	soubor
studentů	student	k1gMnPc2	student
Slovanského	slovanský	k2eAgNnSc2d1	slovanské
gymnázia	gymnázium	k1gNnSc2	gymnázium
Olomouc	Olomouc	k1gFnSc4	Olomouc
Vlastivědné	vlastivědný	k2eAgFnSc2d1	vlastivědná
<g />
.	.	kIx.	.
</s>
<s>
muzeum	muzeum	k1gNnSc1	muzeum
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
–	–	k?	–
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1951	[number]	k4	1951
Muzeum	muzeum	k1gNnSc1	muzeum
moderního	moderní	k2eAgNnSc2d1	moderní
umění	umění	k1gNnSc2	umění
–	–	k?	–
má	mít	k5eAaImIp3nS	mít
asi	asi	k9	asi
60	[number]	k4	60
000	[number]	k4	000
sbírkových	sbírkový	k2eAgInPc2d1	sbírkový
předmětů	předmět	k1gInPc2	předmět
Arcidiecézní	arcidiecézní	k2eAgNnSc4d1	arcidiecézní
muzeum	muzeum	k1gNnSc4	muzeum
Olomouc	Olomouc	k1gFnSc1	Olomouc
–	–	k?	–
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
Středoevropské	středoevropský	k2eAgFnSc2d1	středoevropská
fórum	fórum	k1gNnSc1	fórum
moderního	moderní	k2eAgNnSc2d1	moderní
umění	umění	k1gNnSc2	umění
–	–	k?	–
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nerealizováno	realizován	k2eNgNnSc4d1	nerealizováno
<g/>
,	,	kIx,	,
ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
kultury	kultura	k1gFnSc2	kultura
projekt	projekt	k1gInSc4	projekt
zamítlo	zamítnout	k5eAaPmAgNnS	zamítnout
Muzeum	muzeum	k1gNnSc1	muzeum
Olomoucké	olomoucký	k2eAgFnSc2d1	olomoucká
pevnosti	pevnost	k1gFnSc2	pevnost
Letecké	letecký	k2eAgNnSc1d1	letecké
muzeum	muzeum	k1gNnSc1	muzeum
Olomouc	Olomouc	k1gFnSc1	Olomouc
-	-	kIx~	-
Neředín	Neředín	k1gInSc1	Neředín
Veteran	Veterana	k1gFnPc2	Veterana
Arena	Aren	k1gInSc2	Aren
Olomouc	Olomouc	k1gFnSc1	Olomouc
–	–	k?	–
Muzeum	muzeum	k1gNnSc1	muzeum
<g />
.	.	kIx.	.
</s>
<s>
historických	historický	k2eAgInPc2d1	historický
automobilů	automobil	k1gInPc2	automobil
Pevnost	pevnost	k1gFnSc4	pevnost
poznání	poznání	k1gNnSc1	poznání
-	-	kIx~	-
muzeum	muzeum	k1gNnSc1	muzeum
popularizace	popularizace	k1gFnSc2	popularizace
vědy	věda	k1gFnSc2	věda
pod	pod	k7c7	pod
záštitou	záštita	k1gFnSc7	záštita
Přírodovědecké	přírodovědecký	k2eAgFnSc2d1	Přírodovědecká
fakulty	fakulta	k1gFnSc2	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Palackého	Palackého	k2eAgFnSc1d1	Palackého
Vědecká	vědecký	k2eAgFnSc1d1	vědecká
knihovna	knihovna	k1gFnSc1	knihovna
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
knihovna	knihovna	k1gFnSc1	knihovna
Univerzity	univerzita	k1gFnSc2	univerzita
Palackého	Palackého	k2eAgFnSc1d1	Palackého
krajská	krajský	k2eAgFnSc1d1	krajská
knihovna	knihovna	k1gFnSc1	knihovna
Olomouc	Olomouc	k1gFnSc1	Olomouc
městská	městský	k2eAgFnSc1d1	městská
knihovna	knihovna	k1gFnSc1	knihovna
Olomouc	Olomouc	k1gFnSc1	Olomouc
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Matice	matice	k1gFnSc2	matice
cyrilometodějská	cyrilometodějský	k2eAgNnPc1d1	Cyrilometodějské
nakladatelství	nakladatelství	k1gNnPc1	nakladatelství
Votobia	Votobius	k1gMnSc2	Votobius
nakladatelství	nakladatelství	k1gNnSc2	nakladatelství
Solen	solen	k2eAgInSc1d1	solen
nakladatelství	nakladatelství	k1gNnPc2	nakladatelství
Anag	Anag	k1gMnSc1	Anag
nakladatelství	nakladatelství	k1gNnSc2	nakladatelství
DANAL	DANAL	kA	DANAL
nakladatelství	nakladatelství	k1gNnSc2	nakladatelství
ALDA	ALDA	kA	ALDA
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Refugium	refugium	k1gNnSc1	refugium
Velehrad-Roma	Velehrad-Romum	k1gNnSc2	Velehrad-Romum
nakladatelství	nakladatelství	k1gNnSc2	nakladatelství
PRODOS	PRODOS	kA	PRODOS
–	–	k?	–
pedagogické	pedagogický	k2eAgNnSc1d1	pedagogické
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
nakladatelství	nakladatelství	k1gNnSc2	nakladatelství
Moraviaj	Moraviaj	k1gMnSc1	Moraviaj
<g />
.	.	kIx.	.
</s>
<s>
Esperanto-Pioniroj	Esperanto-Pioniroj	k1gInSc1	Esperanto-Pioniroj
Afas	Afasa	k1gFnPc2	Afasa
galerie	galerie	k1gFnSc2	galerie
Galerie	galerie	k1gFnSc2	galerie
Patro	patro	k1gNnSc1	patro
AbaKus-Art	AbaKus-Art	k1gInSc1	AbaKus-Art
<g/>
,	,	kIx,	,
internetová	internetový	k2eAgFnSc1d1	internetová
galerie	galerie	k1gFnSc1	galerie
Galerie	galerie	k1gFnSc2	galerie
1499	[number]	k4	1499
Galerie	galerie	k1gFnSc1	galerie
Bohéma	bohéma	k1gFnSc1	bohéma
Galerie	galerie	k1gFnSc2	galerie
Caesar	Caesar	k1gMnSc1	Caesar
Galerie	galerie	k1gFnSc2	galerie
W7	W7	k1gMnSc1	W7
Galerie	galerie	k1gFnSc1	galerie
G	G	kA	G
Galerie	galerie	k1gFnSc1	galerie
Hesperia	Hesperium	k1gNnSc2	Hesperium
Galerie	galerie	k1gFnSc2	galerie
informačního	informační	k2eAgNnSc2d1	informační
centra	centrum	k1gNnSc2	centrum
Univerzity	univerzita	k1gFnSc2	univerzita
Palackého	Palacký	k1gMnSc2	Palacký
Galerie	galerie	k1gFnSc2	galerie
Kopeček	Kopeček	k1gMnSc1	Kopeček
<g/>
,	,	kIx,	,
Olomouc-Svatý	Olomouc-Svatý	k2eAgMnSc1d1	Olomouc-Svatý
Kopeček	Kopeček	k1gMnSc1	Kopeček
Galerie	galerie	k1gFnSc2	galerie
Labyrint	labyrint	k1gInSc1	labyrint
Galerie	galerie	k1gFnSc2	galerie
Mona	Mona	k1gFnSc1	Mona
Lisa	Lisa	k1gFnSc1	Lisa
Galerie	galerie	k1gFnSc1	galerie
Podkova	podkova	k1gFnSc1	podkova
Galerie	galerie	k1gFnSc1	galerie
Rubikon	Rubikon	k1gInSc1	Rubikon
Galerie	galerie	k1gFnSc2	galerie
Skácelík	Skácelík	k1gMnSc1	Skácelík
Galerie	galerie	k1gFnSc1	galerie
U	u	k7c2	u
Mloka	mlok	k1gMnSc2	mlok
Umělecké	umělecký	k2eAgNnSc1d1	umělecké
centrum	centrum	k1gNnSc1	centrum
Univerzity	univerzita	k1gFnSc2	univerzita
Palackého	Palacký	k1gMnSc2	Palacký
Galerie	galerie	k1gFnSc2	galerie
Jola	jola	k1gFnSc1	jola
(	(	kIx(	(
<g/>
Dolní	dolní	k2eAgNnSc1d1	dolní
náměstí	náměstí	k1gNnSc1	náměstí
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
)	)	kIx)	)
1966	[number]	k4	1966
–	–	k?	–
Olomoucké	olomoucký	k2eAgFnSc2d1	olomoucká
výstavní	výstavní	k2eAgFnSc2d1	výstavní
sady	sada	k1gFnSc2	sada
→	→	k?	→
1970	[number]	k4	1970
–	–	k?	–
Flora	Flora	k1gFnSc1	Flora
Olomouc	Olomouc	k1gFnSc1	Olomouc
→	→	k?	→
2000	[number]	k4	2000
–	–	k?	–
Výstaviště	výstaviště	k1gNnSc2	výstaviště
Flora	Flora	k1gFnSc1	Flora
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
výstaviště	výstaviště	k1gNnSc1	výstaviště
navštívilo	navštívit	k5eAaPmAgNnS	navštívit
222	[number]	k4	222
550	[number]	k4	550
návštěvníků	návštěvník	k1gMnPc2	návštěvník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
bylo	být	k5eAaImAgNnS	být
Výstaviště	výstaviště	k1gNnSc1	výstaviště
Flora	Flora	k1gFnSc1	Flora
Olomouc	Olomouc	k1gFnSc1	Olomouc
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
počtu	počet	k1gInSc2	počet
návštěvníků	návštěvník	k1gMnPc2	návštěvník
(	(	kIx(	(
<g/>
nikoliv	nikoliv	k9	nikoliv
turisticky	turisticky	k6eAd1	turisticky
<g/>
)	)	kIx)	)
s	s	k7c7	s
200	[number]	k4	200
506	[number]	k4	506
návštěvníky	návštěvník	k1gMnPc7	návštěvník
třetím	třetí	k4xOgMnSc6	třetí
nejnavštěvovanějším	navštěvovaný	k2eAgNnSc7d3	nejnavštěvovanější
místem	místo	k1gNnSc7	místo
v	v	k7c6	v
Olomouckém	olomoucký	k2eAgInSc6d1	olomoucký
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Academia	academia	k1gFnSc1	academia
film	film	k1gInSc1	film
Olomouc	Olomouc	k1gFnSc1	Olomouc
(	(	kIx(	(
<g/>
AFO	AFO	kA	AFO
<g/>
)	)	kIx)	)
–	–	k?	–
od	od	k7c2	od
r.	r.	kA	r.
1966	[number]	k4	1966
festival	festival	k1gInSc4	festival
dokumentárních	dokumentární	k2eAgInPc2d1	dokumentární
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
od	od	k7c2	od
r.	r.	kA	r.
1992	[number]	k4	1992
s	s	k7c7	s
mezinárodní	mezinárodní	k2eAgFnSc7d1	mezinárodní
účastí	účast	k1gFnSc7	účast
Beerfest	Beerfest	k1gFnSc1	Beerfest
Olomouc	Olomouc	k1gFnSc1	Olomouc
–	–	k?	–
největší	veliký	k2eAgInSc1d3	veliký
český	český	k2eAgInSc1d1	český
pivní	pivní	k2eAgInSc1d1	pivní
festival	festival	k1gInSc1	festival
s	s	k7c7	s
hudebním	hudební	k2eAgInSc7d1	hudební
programem	program	k1gInSc7	program
od	od	k7c2	od
r.	r.	kA	r.
2002	[number]	k4	2002
Divadelní	divadelní	k2eAgFnSc1d1	divadelní
Flora	Flora	k1gFnSc1	Flora
–	–	k?	–
od	od	k7c2	od
r.	r.	kA	r.
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgMnPc2d3	veliký
českých	český	k2eAgMnPc2d1	český
divadelních	divadelní	k2eAgMnPc2d1	divadelní
festivalů	festival	k1gInPc2	festival
<g />
.	.	kIx.	.
</s>
<s>
<g/>
;	;	kIx,	;
pořádá	pořádat	k5eAaImIp3nS	pořádat
DW7	DW7	k1gMnSc1	DW7
o.p.s.	o.p.s.	k?	o.p.s.
Festival	festival	k1gInSc1	festival
Baroko	baroko	k1gNnSc1	baroko
–	–	k?	–
festival	festival	k1gInSc4	festival
barokní	barokní	k2eAgFnSc2d1	barokní
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
od	od	k7c2	od
r.	r.	kA	r.
1998	[number]	k4	1998
Festival	festival	k1gInSc1	festival
Jeden	jeden	k4xCgInSc1	jeden
svět	svět	k1gInSc1	svět
-	-	kIx~	-
festival	festival	k1gInSc1	festival
dokumentárních	dokumentární	k2eAgInPc2d1	dokumentární
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
organizuje	organizovat	k5eAaBmIp3nS	organizovat
Člověk	člověk	k1gMnSc1	člověk
v	v	k7c6	v
tísni	tíseň	k1gFnSc6	tíseň
Festival	festival	k1gInSc1	festival
Religiosa	Religiosa	k1gFnSc1	Religiosa
–	–	k?	–
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
festival	festival	k1gInSc1	festival
duchovní	duchovní	k2eAgFnSc2d1	duchovní
a	a	k8xC	a
církevní	církevní	k2eAgFnSc2d1	církevní
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
od	od	k7c2	od
r.	r.	kA	r.
2006	[number]	k4	2006
Svátky	svátek	k1gInPc7	svátek
Písní	píseň	k1gFnPc2	píseň
Olomouc	Olomouc	k1gFnSc1	Olomouc
–	–	k?	–
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
festival	festival	k1gInSc1	festival
<g />
.	.	kIx.	.
</s>
<s>
pěveckých	pěvecký	k2eAgInPc2d1	pěvecký
sborů	sbor	k1gInPc2	sbor
<g/>
,	,	kIx,	,
od	od	k7c2	od
r.	r.	kA	r.
1972	[number]	k4	1972
(	(	kIx(	(
<g/>
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
tradici	tradice	k1gFnSc4	tradice
festivalu	festival	k1gInSc2	festival
Dětských	dětský	k2eAgInPc2d1	dětský
pěveckých	pěvecký	k2eAgInPc2d1	pěvecký
sborů	sbor	k1gInPc2	sbor
<g/>
)	)	kIx)	)
Film	film	k1gInSc1	film
ve	v	k7c6	v
znamení	znamení	k1gNnSc6	znamení
ryby	ryba	k1gFnSc2	ryba
–	–	k?	–
mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
festival	festival	k1gInSc4	festival
duchovních	duchovní	k2eAgMnPc2d1	duchovní
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
10	[number]	k4	10
<g/>
.	.	kIx.	.
ročník	ročník	k1gInSc1	ročník
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
se	se	k3xPyFc4	se
neuskutečnil	uskutečnit	k5eNaPmAgInS	uskutečnit
<g/>
)	)	kIx)	)
Ostrovy	ostrov	k1gInPc7	ostrov
bez	bez	k7c2	bez
hranic	hranice	k1gFnPc2	hranice
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
festival	festival	k1gInSc1	festival
poezie	poezie	k1gFnSc2	poezie
od	od	k7c2	od
r.	r.	kA	r.
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
v	v	k7c6	v
návaznosti	návaznost	k1gFnSc6	návaznost
na	na	k7c4	na
festival	festival	k1gInSc4	festival
Poezie	poezie	k1gFnSc2	poezie
bez	bez	k7c2	bez
hranic	hranice	k1gFnPc2	hranice
2001	[number]	k4	2001
<g/>
–	–	k?	–
<g/>
2004	[number]	k4	2004
Přehlídka	přehlídka	k1gFnSc1	přehlídka
animovaného	animovaný	k2eAgInSc2d1	animovaný
filmu	film	k1gInSc2	film
(	(	kIx(	(
<g/>
PAF	paf	k2eAgFnSc4d1	paf
<g/>
)	)	kIx)	)
–	–	k?	–
festival	festival	k1gInSc4	festival
animovaného	animovaný	k2eAgInSc2d1	animovaný
filmu	film	k1gInSc2	film
a	a	k8xC	a
pohyblivých	pohyblivý	k2eAgInPc2d1	pohyblivý
obrazů	obraz	k1gInPc2	obraz
založený	založený	k2eAgInSc4d1	založený
r.	r.	kA	r.
2000	[number]	k4	2000
TANEC	tanec	k1gInSc4	tanec
PRAHA	Praha	k1gFnSc1	Praha
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
–	–	k?	–
mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
festival	festival	k1gInSc4	festival
současného	současný	k2eAgInSc2d1	současný
tance	tanec	k1gInSc2	tanec
a	a	k8xC	a
pohybového	pohybový	k2eAgNnSc2d1	pohybové
<g />
.	.	kIx.	.
</s>
<s>
divadla	divadlo	k1gNnPc4	divadlo
-	-	kIx~	-
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
Divadle	divadlo	k1gNnSc6	divadlo
na	na	k7c4	na
cucky	cucek	k1gInPc4	cucek
<g/>
;	;	kIx,	;
produkce	produkce	k1gFnSc1	produkce
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
DW7	DW7	k1gFnSc4	DW7
o.p.s.	o.p.s.	k?	o.p.s.
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
pražskými	pražský	k2eAgMnPc7d1	pražský
organizátory	organizátor	k1gMnPc7	organizátor
Týden	týden	k1gInSc4	týden
improvizace	improvizace	k1gFnSc2	improvizace
–	–	k?	–
projekt	projekt	k1gInSc1	projekt
DW7	DW7	k1gMnSc2	DW7
o.p.s.	o.p.s.	k?	o.p.s.
směřující	směřující	k2eAgInSc4d1	směřující
k	k	k7c3	k
oživování	oživování	k1gNnSc3	oživování
městských	městský	k2eAgFnPc2d1	městská
periferií	periferie	k1gFnPc2	periferie
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
uměleckých	umělecký	k2eAgFnPc2d1	umělecká
intervencí	intervence	k1gFnPc2	intervence
VZÁŘÍ	VZÁŘÍ	kA	VZÁŘÍ
–	–	k?	–
mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
festival	festival	k1gInSc4	festival
světla	světlo	k1gNnSc2	světlo
a	a	k8xC	a
videomappingu	videomapping	k1gInSc2	videomapping
<g/>
,	,	kIx,	,
od	od	k7c2	od
r.	r.	kA	r.
2011	[number]	k4	2011
(	(	kIx(	(
<g/>
též	též	k9	též
anglicky	anglicky	k6eAd1	anglicky
Septembeam	Septembeam	k1gInSc4	Septembeam
<g/>
)	)	kIx)	)
Za	za	k7c4	za
<g/>
(	(	kIx(	(
<g/>
o	o	k7c4	o
<g/>
)	)	kIx)	)
<g/>
hrada	hrada	k1gFnSc1	hrada
–	–	k?	–
projekt	projekt	k1gInSc4	projekt
komunitního	komunitní	k2eAgInSc2d1	komunitní
zahradničení	zahradničený	k2eAgMnPc1d1	zahradničený
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
Olomouce	Olomouc	k1gFnSc2	Olomouc
<g/>
;	;	kIx,	;
zaštiťuje	zaštiťovat	k5eAaImIp3nS	zaštiťovat
DW7	DW7	k1gFnSc1	DW7
o.p.s.	o.p.s.	k?	o.p.s.
Zoologická	zoologický	k2eAgFnSc1d1	zoologická
zahrada	zahrada	k1gFnSc1	zahrada
Olomouc	Olomouc	k1gFnSc1	Olomouc
Cena	cena	k1gFnSc1	cena
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
udělována	udělovat	k5eAaImNgFnS	udělovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
tuto	tento	k3xDgFnSc4	tento
dobu	doba	k1gFnSc4	doba
ji	on	k3xPp3gFnSc4	on
získalo	získat	k5eAaPmAgNnS	získat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
120	[number]	k4	120
osobností	osobnost	k1gFnPc2	osobnost
a	a	k8xC	a
subjektů	subjekt	k1gInPc2	subjekt
<g/>
.	.	kIx.	.
</s>
<s>
Ocenění	ocenění	k1gNnSc1	ocenění
je	být	k5eAaImIp3nS	být
udělováno	udělovat	k5eAaImNgNnS	udělovat
kolektivům	kolektiv	k1gInPc3	kolektiv
a	a	k8xC	a
jednotlivcům	jednotlivec	k1gMnPc3	jednotlivec
za	za	k7c4	za
činnost	činnost	k1gFnSc4	činnost
v	v	k7c6	v
umělecké	umělecký	k2eAgFnSc6d1	umělecká
<g/>
,	,	kIx,	,
hospodářské	hospodářský	k2eAgFnSc3d1	hospodářská
<g/>
,	,	kIx,	,
publicistické	publicistický	k2eAgFnSc3d1	publicistická
či	či	k8xC	či
jiné	jiný	k2eAgFnSc3d1	jiná
sféře	sféra	k1gFnSc3	sféra
<g/>
,	,	kIx,	,
práci	práce	k1gFnSc4	práce
nebo	nebo	k8xC	nebo
díla	dílo	k1gNnPc4	dílo
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
úzkém	úzký	k2eAgInSc6d1	úzký
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
městu	město	k1gNnSc3	město
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
byla	být	k5eAaImAgFnS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
území	území	k1gNnSc6	území
<g/>
,	,	kIx,	,
či	či	k8xC	či
jinak	jinak	k6eAd1	jinak
přispívají	přispívat	k5eAaImIp3nP	přispívat
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Cena	cena	k1gFnSc1	cena
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
uděluje	udělovat	k5eAaImIp3nS	udělovat
jednou	jeden	k4xCgFnSc7	jeden
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Zastupitelstvo	zastupitelstvo	k1gNnSc1	zastupitelstvo
města	město	k1gNnSc2	město
na	na	k7c4	na
návrh	návrh	k1gInSc4	návrh
rady	rada	k1gFnSc2	rada
města	město	k1gNnSc2	město
uděluje	udělovat	k5eAaImIp3nS	udělovat
Cenu	cena	k1gFnSc4	cena
města	město	k1gNnSc2	město
Olomouce	Olomouc	k1gFnSc2	Olomouc
–	–	k?	–
ocenění	ocenění	k1gNnSc4	ocenění
významné	významný	k2eAgFnSc2d1	významná
činnosti	činnost	k1gFnSc2	činnost
nebo	nebo	k8xC	nebo
díla	dílo	k1gNnSc2	dílo
se	se	k3xPyFc4	se
zřetelným	zřetelný	k2eAgInSc7d1	zřetelný
časovým	časový	k2eAgInSc7d1	časový
přesahem	přesah	k1gInSc7	přesah
<g/>
,	,	kIx,	,
v	v	k7c6	v
některé	některý	k3yIgFnSc6	některý
z	z	k7c2	z
oblastí	oblast	k1gFnPc2	oblast
<g/>
:	:	kIx,	:
hudba	hudba	k1gFnSc1	hudba
<g/>
,	,	kIx,	,
přírodní	přírodní	k2eAgFnPc1d1	přírodní
vědy	věda	k1gFnPc1	věda
<g/>
,	,	kIx,	,
užité	užitý	k2eAgNnSc1d1	užité
umění	umění	k1gNnSc1	umění
<g/>
,	,	kIx,	,
architektura	architektura	k1gFnSc1	architektura
a	a	k8xC	a
urbanismus	urbanismus	k1gInSc1	urbanismus
<g/>
,	,	kIx,	,
výtvarné	výtvarný	k2eAgNnSc4d1	výtvarné
umění	umění	k1gNnSc4	umění
<g/>
,	,	kIx,	,
technický	technický	k2eAgInSc4d1	technický
pokrok	pokrok	k1gInSc4	pokrok
<g/>
,	,	kIx,	,
literární	literární	k2eAgFnSc4d1	literární
činnost	činnost	k1gFnSc4	činnost
<g/>
,	,	kIx,	,
dramatické	dramatický	k2eAgNnSc4d1	dramatické
umění	umění	k1gNnSc4	umění
<g/>
,	,	kIx,	,
společenské	společenský	k2eAgFnPc4d1	společenská
vědy	věda	k1gFnPc4	věda
<g/>
,	,	kIx,	,
hospodářský	hospodářský	k2eAgInSc4d1	hospodářský
rozvoj	rozvoj	k1gInSc4	rozvoj
<g/>
,	,	kIx,	,
sport	sport	k1gInSc1	sport
<g/>
,	,	kIx,	,
žurnalistika	žurnalistika	k1gFnSc1	žurnalistika
a	a	k8xC	a
publicistika	publicistika	k1gFnSc1	publicistika
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgInPc1d1	jiný
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
uděluje	udělovat	k5eAaImIp3nS	udělovat
Cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
počin	počin	k1gInSc4	počin
roku	rok	k1gInSc2	rok
–	–	k?	–
ocenění	ocenění	k1gNnSc2	ocenění
významného	významný	k2eAgNnSc2d1	významné
jednání	jednání	k1gNnSc2	jednání
<g/>
,	,	kIx,	,
konkrétního	konkrétní	k2eAgNnSc2d1	konkrétní
díla	dílo	k1gNnSc2	dílo
nebo	nebo	k8xC	nebo
mimořádného	mimořádný	k2eAgInSc2d1	mimořádný
úspěchu	úspěch	k1gInSc2	úspěch
v	v	k7c6	v
příslušném	příslušný	k2eAgInSc6d1	příslušný
roce	rok	k1gInSc6	rok
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
sportu	sport	k1gInSc2	sport
<g/>
,	,	kIx,	,
vědy	věda	k1gFnSc2	věda
a	a	k8xC	a
výzkumu	výzkum	k1gInSc2	výzkum
<g/>
,	,	kIx,	,
hrdinského	hrdinský	k2eAgInSc2d1	hrdinský
činu	čin	k1gInSc2	čin
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgInPc1d1	jiný
<g/>
.	.	kIx.	.
</s>
<s>
Univerzita	univerzita	k1gFnSc1	univerzita
Palackého	Palackého	k2eAgFnSc1d1	Palackého
Moravská	moravský	k2eAgFnSc1d1	Moravská
vysoká	vysoký	k2eAgFnSc1d1	vysoká
škola	škola	k1gFnSc1	škola
Olomouc	Olomouc	k1gFnSc1	Olomouc
–	–	k?	–
soukromá	soukromý	k2eAgFnSc1d1	soukromá
vysoká	vysoký	k2eAgFnSc1d1	vysoká
škola	škola	k1gFnSc1	škola
neuniverzitního	univerzitní	k2eNgInSc2d1	neuniverzitní
typu	typ	k1gInSc2	typ
<g/>
,	,	kIx,	,
bakalářské	bakalářský	k2eAgNnSc1d1	bakalářské
vzdělání	vzdělání	k1gNnSc1	vzdělání
<g/>
,	,	kIx,	,
obory	obor	k1gInPc1	obor
<g/>
:	:	kIx,	:
Ekonomika	ekonomika	k1gFnSc1	ekonomika
a	a	k8xC	a
management	management	k1gInSc1	management
<g/>
,	,	kIx,	,
Podnikové	podnikový	k2eAgInPc1d1	podnikový
a	a	k8xC	a
informační	informační	k2eAgInPc1d1	informační
systémy	systém	k1gInPc1	systém
<g/>
.	.	kIx.	.
</s>
<s>
Vyšší	vysoký	k2eAgFnSc1d2	vyšší
odborná	odborný	k2eAgFnSc1d1	odborná
škola	škola	k1gFnSc1	škola
zdravotnická	zdravotnický	k2eAgFnSc1d1	zdravotnická
Emanuela	Emanuela	k1gFnSc1	Emanuela
Pöttinga	Pöttinga	k1gFnSc1	Pöttinga
Vyšší	vysoký	k2eAgFnSc1d2	vyšší
odborná	odborný	k2eAgFnSc1d1	odborná
škola	škola	k1gFnSc1	škola
a	a	k8xC	a
Střední	střední	k2eAgFnSc1d1	střední
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
škola	škola	k1gFnSc1	škola
elektrotechnická	elektrotechnický	k2eAgFnSc1d1	elektrotechnická
Vyšší	vysoký	k2eAgFnSc1d2	vyšší
odborná	odborný	k2eAgFnSc1d1	odborná
škola	škola	k1gFnSc1	škola
sociální	sociální	k2eAgFnSc2d1	sociální
–	–	k?	–
CARITAS	CARITAS	kA	CARITAS
Vyšší	vysoký	k2eAgFnSc1d2	vyšší
odborná	odborný	k2eAgFnSc1d1	odborná
škola	škola	k1gFnSc1	škola
sociální	sociální	k2eAgFnSc1d1	sociální
a	a	k8xC	a
teologická	teologický	k2eAgFnSc1d1	teologická
–	–	k?	–
DORKAS	DORKAS	kA	DORKAS
Konzervatoř	konzervatoř	k1gFnSc4	konzervatoř
evangelické	evangelický	k2eAgFnSc2d1	evangelická
akademie	akademie	k1gFnSc2	akademie
Olomouc	Olomouc	k1gFnSc1	Olomouc
–	–	k?	–
střední	střední	k2eAgNnSc4d1	střední
a	a	k8xC	a
vyšší	vysoký	k2eAgNnSc4d2	vyšší
odborné	odborný	k2eAgNnSc4d1	odborné
vzdělání	vzdělání	k1gNnSc4	vzdělání
Církevní	církevní	k2eAgNnSc1d1	církevní
gymnázium	gymnázium	k1gNnSc1	gymnázium
Německého	německý	k2eAgInSc2d1	německý
řádu	řád	k1gInSc2	řád
Gymnázium	gymnázium	k1gNnSc1	gymnázium
Čajkovského	Čajkovský	k2eAgNnSc2d1	Čajkovský
Gymnázium	gymnázium	k1gNnSc1	gymnázium
Olomouc-Hejčín	Olomouc-Hejčín	k1gMnSc1	Olomouc-Hejčín
Slovanské	slovanský	k2eAgNnSc1d1	slovanské
gymnázium	gymnázium	k1gNnSc1	gymnázium
Soukromé	soukromý	k2eAgNnSc1d1	soukromé
gymnázium	gymnázium	k1gNnSc1	gymnázium
<g />
.	.	kIx.	.
</s>
<s>
Olomouc	Olomouc	k1gFnSc1	Olomouc
-	-	kIx~	-
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
Střední	střední	k2eAgFnSc1d1	střední
škola	škola	k1gFnSc1	škola
logistiky	logistika	k1gFnSc2	logistika
a	a	k8xC	a
chemie	chemie	k1gFnSc2	chemie
Obchodní	obchodní	k2eAgFnSc2d1	obchodní
akademie	akademie	k1gFnSc2	akademie
Střední	střední	k2eAgFnSc1d1	střední
odborná	odborný	k2eAgFnSc1d1	odborná
škola	škola	k1gFnSc1	škola
Olomouc	Olomouc	k1gFnSc1	Olomouc
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
Střední	střední	k2eAgFnSc1d1	střední
odborná	odborný	k2eAgFnSc1d1	odborná
škola	škola	k1gFnSc1	škola
služeb	služba	k1gFnPc2	služba
Střední	střední	k2eAgFnSc1d1	střední
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
škola	škola	k1gFnSc1	škola
elektrotechnická	elektrotechnický	k2eAgFnSc1d1	elektrotechnická
Střední	střední	k2eAgFnSc1d1	střední
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
škola	škola	k1gFnSc1	škola
strojnická	strojnický	k2eAgFnSc1d1	strojnická
Střední	střední	k2eAgFnSc1d1	střední
škola	škola	k1gFnSc1	škola
Olomouc	Olomouc	k1gFnSc1	Olomouc
-	-	kIx~	-
Svatý	svatý	k2eAgInSc1d1	svatý
Kopeček	kopeček	k1gInSc1	kopeček
Střední	střední	k2eAgFnSc1d1	střední
škola	škola	k1gFnSc1	škola
polygrafická	polygrafický	k2eAgFnSc1d1	polygrafická
Střední	střední	k2eAgFnSc1d1	střední
škola	škola	k1gFnSc1	škola
polytechnická	polytechnický	k2eAgFnSc1d1	polytechnická
Střední	střední	k2eAgFnSc1d1	střední
škola	škola	k1gFnSc1	škola
poštovní	poštovní	k2eAgFnSc1d1	poštovní
Střední	střední	k2eAgFnSc1d1	střední
zdravotnická	zdravotnický	k2eAgFnSc1d1	zdravotnická
a	a	k8xC	a
Vyšší	vysoký	k2eAgFnSc1d2	vyšší
<g />
.	.	kIx.	.
</s>
<s>
zdravotnická	zdravotnický	k2eAgFnSc1d1	zdravotnická
škola	škola	k1gFnSc1	škola
Emanuela	Emanuel	k1gMnSc2	Emanuel
Pöttinga	Pötting	k1gMnSc2	Pötting
Střední	střední	k2eAgFnSc1d1	střední
škola	škola	k1gFnSc1	škola
zemědělská	zemědělský	k2eAgFnSc1d1	zemědělská
a	a	k8xC	a
zahradnická	zahradnický	k2eAgFnSc1d1	zahradnická
Olomouc	Olomouc	k1gFnSc1	Olomouc
Soukromá	soukromý	k2eAgFnSc1d1	soukromá
střední	střední	k2eAgFnSc1d1	střední
škola	škola	k1gFnSc1	škola
podnikání	podnikání	k1gNnSc2	podnikání
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
,	,	kIx,	,
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
Střední	střední	k2eAgFnSc1d1	střední
škola	škola	k1gFnSc1	škola
obchodu	obchod	k1gInSc2	obchod
<g/>
,	,	kIx,	,
gastronomie	gastronomie	k1gFnSc2	gastronomie
a	a	k8xC	a
designu	design	k1gInSc2	design
PRAKTIK	praktik	k1gMnSc1	praktik
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
Střední	střední	k2eAgNnSc1d1	střední
odborné	odborný	k2eAgNnSc1d1	odborné
učiliště	učiliště	k1gNnSc1	učiliště
farmaceutické	farmaceutický	k2eAgNnSc1d1	farmaceutické
Střední	střední	k2eAgNnSc1d1	střední
odborné	odborný	k2eAgNnSc1d1	odborné
učiliště	učiliště	k1gNnSc1	učiliště
obchodu	obchod	k1gInSc2	obchod
a	a	k8xC	a
služeb	služba	k1gFnPc2	služba
Střední	střední	k2eAgNnSc4d1	střední
odborné	odborný	k2eAgNnSc4d1	odborné
učiliště	učiliště	k1gNnSc4	učiliště
technické	technický	k2eAgFnSc2d1	technická
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
obchodní	obchodní	k2eAgNnSc1d1	obchodní
Soukromé	soukromý	k2eAgNnSc1d1	soukromé
střední	střední	k2eAgNnSc1d1	střední
odborné	odborný	k2eAgNnSc1d1	odborné
učiliště	učiliště	k1gNnSc1	učiliště
stavební	stavební	k2eAgNnSc1d1	stavební
<g/>
,	,	kIx,	,
Odborné	odborný	k2eAgNnSc1d1	odborné
učiliště	učiliště	k1gNnSc1	učiliště
a	a	k8xC	a
Učiliště	učiliště	k1gNnSc1	učiliště
-	-	kIx~	-
PhDr.	PhDr.	kA	PhDr.
L.	L.	kA	L.
Bortl	Bortl	k1gMnSc1	Bortl
<g/>
,	,	kIx,	,
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
Speciální	speciální	k2eAgFnSc1d1	speciální
škola	škola	k1gFnSc1	škola
DC	DC	kA	DC
90	[number]	k4	90
<g/>
,	,	kIx,	,
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Architektura	architektura	k1gFnSc1	architektura
Olomouce	Olomouc	k1gFnSc2	Olomouc
<g/>
,	,	kIx,	,
Pevnost	pevnost	k1gFnSc1	pevnost
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
,	,	kIx,	,
Seznam	seznam	k1gInSc1	seznam
církevních	církevní	k2eAgFnPc2d1	církevní
staveb	stavba	k1gFnPc2	stavba
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
,	,	kIx,	,
Seznam	seznam	k1gInSc1	seznam
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
staveb	stavba	k1gFnPc2	stavba
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
a	a	k8xC	a
Seznam	seznam	k1gInSc4	seznam
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
historickém	historický	k2eAgNnSc6d1	historické
jádru	jádro	k1gNnSc6	jádro
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
obklopeném	obklopený	k2eAgNnSc6d1	obklopené
rameny	rameno	k1gNnPc7	rameno
řeky	řeka	k1gFnSc2	řeka
Moravy	Morava	k1gFnSc2	Morava
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc4	jehož
formování	formování	k1gNnSc1	formování
bylo	být	k5eAaImAgNnS	být
započato	započnout	k5eAaPmNgNnS	započnout
počátkem	počátkem	k7c2	počátkem
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
dokončeno	dokončit	k5eAaPmNgNnS	dokončit
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
situováno	situovat	k5eAaBmNgNnS	situovat
několik	několik	k4yIc1	několik
velkých	velká	k1gFnPc2	velká
náměstí	náměstí	k1gNnSc2	náměstí
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
na	na	k7c6	na
Horním	horní	k2eAgNnSc6d1	horní
náměstí	náměstí	k1gNnSc6	náměstí
je	být	k5eAaImIp3nS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
radnice	radnice	k1gFnSc1	radnice
s	s	k7c7	s
orlojem	orloj	k1gInSc7	orloj
z	z	k7c2	z
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
Herkulova	Herkulův	k2eAgFnSc1d1	Herkulova
<g/>
,	,	kIx,	,
Ariónova	Ariónův	k2eAgFnSc1d1	Ariónova
a	a	k8xC	a
Caesarova	Caesarův	k2eAgFnSc1d1	Caesarova
kašna	kašna	k1gFnSc1	kašna
<g/>
,	,	kIx,	,
a	a	k8xC	a
především	především	k9	především
památka	památka	k1gFnSc1	památka
UNESCO	Unesco	k1gNnSc1	Unesco
sloup	sloup	k1gInSc4	sloup
Nejsvětější	nejsvětější	k2eAgFnSc2d1	nejsvětější
Trojice	trojice	k1gFnSc2	trojice
<g/>
,	,	kIx,	,
postavený	postavený	k2eAgMnSc1d1	postavený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1740	[number]	k4	1740
<g/>
.	.	kIx.	.
</s>
<s>
Pozoruhodný	pozoruhodný	k2eAgInSc1d1	pozoruhodný
je	být	k5eAaImIp3nS	být
také	také	k9	také
arcibiskupský	arcibiskupský	k2eAgInSc1d1	arcibiskupský
dóm	dóm	k1gInSc1	dóm
svatého	svatý	k2eAgMnSc2d1	svatý
Václava	Václav	k1gMnSc2	Václav
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
roku	rok	k1gInSc2	rok
1469	[number]	k4	1469
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
část	část	k1gFnSc1	část
českých	český	k2eAgMnPc2d1	český
a	a	k8xC	a
moravských	moravský	k2eAgMnPc2d1	moravský
pánů	pan	k1gMnPc2	pan
uherského	uherský	k2eAgMnSc2d1	uherský
krále	král	k1gMnSc2	král
Matyáše	Matyáš	k1gMnSc2	Matyáš
Korvína	Korvín	k1gMnSc2	Korvín
králem	král	k1gMnSc7	král
českým	český	k2eAgMnPc3d1	český
a	a	k8xC	a
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
taktéž	taktéž	k?	taktéž
nachází	nacházet	k5eAaImIp3nS	nacházet
hrobka	hrobka	k1gFnSc1	hrobka
českého	český	k2eAgMnSc2d1	český
krále	král	k1gMnSc2	král
Václava	Václav	k1gMnSc2	Václav
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
zavražděného	zavražděný	k2eAgInSc2d1	zavražděný
roku	rok	k1gInSc2	rok
1306	[number]	k4	1306
právě	právě	k6eAd1	právě
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
historického	historický	k2eAgNnSc2d1	historické
jádra	jádro	k1gNnSc2	jádro
je	být	k5eAaImIp3nS	být
budova	budova	k1gFnSc1	budova
Univerzity	univerzita	k1gFnSc2	univerzita
Palackého	Palacký	k1gMnSc2	Palacký
(	(	kIx(	(
<g/>
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
existovala	existovat	k5eAaImAgFnS	existovat
její	její	k3xOp3gFnPc4	její
předchůdkyně	předchůdkyně	k1gFnPc4	předchůdkyně
navazující	navazující	k2eAgFnPc4d1	navazující
na	na	k7c4	na
jezuitské	jezuitský	k2eAgFnPc4d1	jezuitská
vysoké	vysoká	k1gFnPc4	vysoká
učení	učení	k1gNnSc2	učení
zal	zal	k?	zal
<g/>
.	.	kIx.	.
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1573	[number]	k4	1573
<g/>
/	/	kIx~	/
<g/>
76	[number]	k4	76
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
okolo	okolo	k7c2	okolo
25	[number]	k4	25
000	[number]	k4	000
studentů	student	k1gMnPc2	student
<g/>
.	.	kIx.	.
</s>
<s>
Historická	historický	k2eAgFnSc1d1	historická
část	část	k1gFnSc1	část
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
chráněna	chráněn	k2eAgFnSc1d1	chráněna
jako	jako	k8xC	jako
městská	městský	k2eAgFnSc1d1	městská
památková	památkový	k2eAgFnSc1d1	památková
rezervace	rezervace	k1gFnSc1	rezervace
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
národní	národní	k2eAgFnPc4d1	národní
kulturní	kulturní	k2eAgFnPc4d1	kulturní
památky	památka	k1gFnPc4	památka
patří	patřit	k5eAaImIp3nS	patřit
Olomoucký	olomoucký	k2eAgInSc1d1	olomoucký
hrad	hrad	k1gInSc1	hrad
<g/>
,	,	kIx,	,
klášter	klášter	k1gInSc1	klášter
Hradisko	hradisko	k1gNnSc4	hradisko
<g/>
,	,	kIx,	,
kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Mořice	Mořic	k1gMnSc2	Mořic
<g/>
,	,	kIx,	,
sloup	sloup	k1gInSc1	sloup
Nejsvětější	nejsvětější	k2eAgFnSc2d1	nejsvětější
Trojice	trojice	k1gFnSc2	trojice
a	a	k8xC	a
mariánský	mariánský	k2eAgInSc1d1	mariánský
sloup	sloup	k1gInSc1	sloup
se	se	k3xPyFc4	se
souborem	soubor	k1gInSc7	soubor
barokních	barokní	k2eAgFnPc2d1	barokní
kašen	kašna	k1gFnPc2	kašna
a	a	k8xC	a
vila	vít	k5eAaImAgFnS	vít
Primavesi	Primavese	k1gFnSc4	Primavese
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Kašny	kašna	k1gFnSc2	kašna
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
.	.	kIx.	.
konec	konec	k1gInSc1	konec
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
:	:	kIx,	:
1683	[number]	k4	1683
Neptunova	Neptunův	k2eAgFnSc1d1	Neptunova
kašna	kašna	k1gFnSc1	kašna
(	(	kIx(	(
<g/>
na	na	k7c6	na
Dolním	dolní	k2eAgNnSc6d1	dolní
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
)	)	kIx)	)
–	–	k?	–
autoři	autor	k1gMnPc1	autor
<g/>
:	:	kIx,	:
gdaňský	gdaňský	k2eAgMnSc1d1	gdaňský
sochař	sochař	k1gMnSc1	sochař
Michael	Michael	k1gMnSc1	Michael
Mandík	Mandík	k1gMnSc1	Mandík
a	a	k8xC	a
olomoucký	olomoucký	k2eAgMnSc1d1	olomoucký
kameník	kameník	k1gMnSc1	kameník
Václav	Václav	k1gMnSc1	Václav
Schüler	Schüler	k1gInSc4	Schüler
1687	[number]	k4	1687
Herkulova	Herkulův	k2eAgFnSc1d1	Herkulova
kašna	kašna	k1gFnSc1	kašna
(	(	kIx(	(
<g/>
na	na	k7c6	na
Horním	horní	k2eAgNnSc6d1	horní
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
stejní	stejný	k2eAgMnPc1d1	stejný
autoři	autor	k1gMnPc1	autor
začátek	začátek	k1gInSc4	začátek
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
:	:	kIx,	:
1707	[number]	k4	1707
Jupiterova	Jupiterův	k2eAgFnSc1d1	Jupiterova
(	(	kIx(	(
<g/>
na	na	k7c6	na
Dolním	dolní	k2eAgNnSc6d1	dolní
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
)	)	kIx)	)
–	–	k?	–
autor	autor	k1gMnSc1	autor
původní	původní	k2eAgFnSc2d1	původní
sochy	socha	k1gFnSc2	socha
sv.	sv.	kA	sv.
Floriána	Florián	k1gMnSc2	Florián
<g/>
:	:	kIx,	:
Václav	Václav	k1gMnSc1	Václav
Render	Render	k1gMnSc1	Render
<g/>
;	;	kIx,	;
1735	[number]	k4	1735
přestavěna	přestavět	k5eAaPmNgFnS	přestavět
autor	autor	k1gMnSc1	autor
<g/>
:	:	kIx,	:
tyrolský	tyrolský	k2eAgMnSc1d1	tyrolský
sochař	sochař	k1gMnSc1	sochař
Filip	Filip	k1gMnSc1	Filip
Sattler	Sattler	k1gMnSc1	Sattler
1709	[number]	k4	1709
Tritónů	tritón	k1gInPc2	tritón
(	(	kIx(	(
<g/>
Náměstí	náměstí	k1gNnSc1	náměstí
republiky	republika	k1gFnSc2	republika
<g/>
)	)	kIx)	)
–	–	k?	–
autor	autor	k1gMnSc1	autor
<g />
.	.	kIx.	.
</s>
<s>
<g/>
:	:	kIx,	:
olomoucký	olomoucký	k2eAgMnSc1d1	olomoucký
měšťan	měšťan	k1gMnSc1	měšťan
a	a	k8xC	a
kameník	kameník	k1gMnSc1	kameník
Václav	Václav	k1gMnSc1	Václav
Render	Render	k1gInSc4	Render
1725	[number]	k4	1725
Caesarova	Caesarův	k2eAgFnSc1d1	Caesarova
(	(	kIx(	(
<g/>
na	na	k7c6	na
Horním	horní	k2eAgNnSc6d1	horní
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
)	)	kIx)	)
–	–	k?	–
autor	autor	k1gMnSc1	autor
<g/>
:	:	kIx,	:
Václav	Václav	k1gMnSc1	Václav
Render	Render	k1gInSc4	Render
Delfínova	delfínův	k2eAgFnSc1d1	Delfínova
kašna	kašna	k1gFnSc1	kašna
z	z	k7c2	z
r.	r.	kA	r.
1725	[number]	k4	1725
od	od	k7c2	od
sochaře	sochař	k1gMnSc2	sochař
Filipa	Filip	k1gMnSc2	Filip
Sattlera	Sattler	k1gMnSc2	Sattler
znovuobjevená	znovuobjevený	k2eAgFnSc1d1	znovuobjevená
r.	r.	kA	r.
2005	[number]	k4	2005
na	na	k7c6	na
dvoře	dvůr	k1gInSc6	dvůr
ZŠ	ZŠ	kA	ZŠ
Na	na	k7c6	na
hradě	hrad	k1gInSc6	hrad
umístěna	umístit	k5eAaPmNgFnS	umístit
před	před	k7c4	před
dominikánský	dominikánský	k2eAgInSc4d1	dominikánský
klášter	klášter	k1gInSc4	klášter
v	v	k7c6	v
Sokolské	sokolský	k2eAgFnSc6d1	Sokolská
ulici	ulice	k1gFnSc6	ulice
1727	[number]	k4	1727
Merkurova	Merkurův	k2eAgFnSc1d1	Merkurova
kašna	kašna	k1gFnSc1	kašna
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
mezi	mezi	k7c7	mezi
obchodním	obchodní	k2eAgInSc7d1	obchodní
domem	dům	k1gInSc7	dům
Prior	priora	k1gNnPc2	priora
a	a	k8xC	a
hotelem	hotel	k1gInSc7	hotel
Národní	národní	k2eAgInSc1d1	národní
dům	dům	k1gInSc1	dům
<g/>
)	)	kIx)	)
–	–	k?	–
autoři	autor	k1gMnPc1	autor
<g/>
:	:	kIx,	:
Václav	Václav	k1gMnSc1	Václav
Render	Render	k1gMnSc1	Render
a	a	k8xC	a
Filip	Filip	k1gMnSc1	Filip
Sattler	Sattler	k1gInSc4	Sattler
2002	[number]	k4	2002
Ariónova	Ariónův	k2eAgFnSc1d1	Ariónova
kašna	kašna	k1gFnSc1	kašna
na	na	k7c6	na
Horním	horní	k2eAgNnSc6d1	horní
náměstí	náměstí	k1gNnSc6	náměstí
od	od	k7c2	od
sochaře	sochař	k1gMnSc2	sochař
Ivana	Ivan	k1gMnSc2	Ivan
Theimera	Theimer	k1gMnSc2	Theimer
Zdíkův	Zdíkův	k2eAgInSc4d1	Zdíkův
palác	palác	k1gInSc4	palác
<g/>
,	,	kIx,	,
Václavské	václavský	k2eAgNnSc4d1	Václavské
náměstí	náměstí	k1gNnSc4	náměstí
Edelmannův	Edelmannův	k2eAgInSc1d1	Edelmannův
palác	palác	k1gInSc1	palác
<g/>
,	,	kIx,	,
Horní	horní	k2eAgNnSc4d1	horní
náměstí	náměstí	k1gNnSc4	náměstí
5	[number]	k4	5
Hauenschildův	Hauenschildův	k2eAgInSc1d1	Hauenschildův
palác	palác	k1gInSc1	palác
<g/>
,	,	kIx,	,
Dolní	dolní	k2eAgNnSc1d1	dolní
náměstí	náměstí	k1gNnSc1	náměstí
<g />
.	.	kIx.	.
</s>
<s>
38	[number]	k4	38
Petrášův	Petrášův	k2eAgInSc4d1	Petrášův
palác	palác	k1gInSc4	palác
<g/>
,	,	kIx,	,
Horní	horní	k2eAgNnSc4d1	horní
náměstí	náměstí	k1gNnSc4	náměstí
25	[number]	k4	25
Žerotínský	žerotínský	k2eAgInSc1d1	žerotínský
palác	palác	k1gInSc1	palác
<g/>
,	,	kIx,	,
Purkrabská	purkrabský	k2eAgFnSc1d1	Purkrabská
2	[number]	k4	2
Palác	palác	k1gInSc4	palác
Podstatských	Podstatský	k2eAgInPc2d1	Podstatský
z	z	k7c2	z
Prusínovic	Prusínovice	k1gFnPc2	Prusínovice
<g/>
,	,	kIx,	,
Ztracená	ztracený	k2eAgFnSc1d1	ztracená
2	[number]	k4	2
Ditrichštejnský	Ditrichštejnský	k2eAgInSc4d1	Ditrichštejnský
palác	palác	k1gInSc4	palác
<g/>
,	,	kIx,	,
Horní	horní	k2eAgNnSc4d1	horní
náměstí	náměstí	k1gNnSc4	náměstí
9	[number]	k4	9
Salmův	Salmův	k2eAgInSc1d1	Salmův
palác	palác	k1gInSc1	palác
<g/>
,	,	kIx,	,
Horní	horní	k2eAgNnSc4d1	horní
náměstí	náměstí	k1gNnSc4	náměstí
1	[number]	k4	1
Arcibiskupský	arcibiskupský	k2eAgInSc1d1	arcibiskupský
palác	palác	k1gInSc1	palác
<g/>
,	,	kIx,	,
Wurmova	Wurmův	k2eAgFnSc1d1	Wurmova
9	[number]	k4	9
Vila	vila	k1gFnSc1	vila
Eduarda	Eduard	k1gMnSc2	Eduard
Hamburgera	Hamburger	k1gMnSc2	Hamburger
<g/>
,	,	kIx,	,
Vídeňská	vídeňský	k2eAgFnSc1d1	Vídeňská
2	[number]	k4	2
Vila	vila	k1gFnSc1	vila
<g />
.	.	kIx.	.
</s>
<s>
Eduarda	Eduard	k1gMnSc2	Eduard
Šrota	Šrot	k1gMnSc2	Šrot
<g/>
,	,	kIx,	,
třída	třída	k1gFnSc1	třída
Spojenců	spojenec	k1gMnPc2	spojenec
20	[number]	k4	20
Vila	vila	k1gFnSc1	vila
Otto	Otto	k1gMnSc1	Otto
Zweiga	Zweiga	k1gFnSc1	Zweiga
<g/>
,	,	kIx,	,
třída	třída	k1gFnSc1	třída
Spojenců	spojenec	k1gMnPc2	spojenec
16	[number]	k4	16
Vila	vila	k1gFnSc1	vila
Elly	Ella	k1gFnSc2	Ella
Krickové	Kricková	k1gFnSc2	Kricková
<g/>
,	,	kIx,	,
třída	třída	k1gFnSc1	třída
Spojenců	spojenec	k1gMnPc2	spojenec
10	[number]	k4	10
Vila	vila	k1gFnSc1	vila
Pauly	Paula	k1gFnSc2	Paula
a	a	k8xC	a
Hanse	Hans	k1gMnSc2	Hans
Briessových	Briessová	k1gFnPc2	Briessová
<g/>
,	,	kIx,	,
Na	na	k7c6	na
Vozovce	vozovka	k1gFnSc6	vozovka
12	[number]	k4	12
Vila	vila	k1gFnSc1	vila
Julia	Julius	k1gMnSc2	Julius
Pelikána	Pelikán	k1gMnSc2	Pelikán
<g/>
,	,	kIx,	,
Na	na	k7c6	na
Vozovce	vozovka	k1gFnSc6	vozovka
21	[number]	k4	21
Vila	vila	k1gFnSc1	vila
Františka	Františka	k1gFnSc1	Františka
a	a	k8xC	a
Ludmily	Ludmila	k1gFnPc1	Ludmila
Kousalíkových	Kousalíkových	k2eAgFnPc1d1	Kousalíkových
<g/>
,	,	kIx,	,
Na	na	k7c6	na
Vozovce	vozovka	k1gFnSc6	vozovka
<g />
.	.	kIx.	.
</s>
<s>
33	[number]	k4	33
Vila	vila	k1gFnSc1	vila
Stanislava	Stanislava	k1gFnSc1	Stanislava
Nakládala	nakládat	k5eAaImAgFnS	nakládat
<g/>
,	,	kIx,	,
Polívkova	Polívkův	k2eAgNnPc4d1	Polívkovo
35	[number]	k4	35
Vila	vila	k1gFnSc1	vila
Antonína	Antonín	k1gMnSc2	Antonín
a	a	k8xC	a
Ludmily	Ludmila	k1gFnPc1	Ludmila
Hofmanových	Hofmanových	k2eAgFnSc1d1	Hofmanových
<g/>
,	,	kIx,	,
Resslova	Resslův	k2eAgFnSc1d1	Resslova
20	[number]	k4	20
Rodinný	rodinný	k2eAgInSc1d1	rodinný
dům	dům	k1gInSc1	dům
Willibalda	Willibald	k1gMnSc2	Willibald
Müllera	Müller	k1gMnSc2	Müller
<g/>
,	,	kIx,	,
Dvořákova	Dvořákův	k2eAgFnSc1d1	Dvořákova
3	[number]	k4	3
Vila	vít	k5eAaImAgFnS	vít
Wilhelma	Wilhelma	k1gFnSc1	Wilhelma
Giebela	Giebela	k1gFnSc1	Giebela
<g/>
,	,	kIx,	,
Na	na	k7c6	na
Šibeníku	šibeník	k1gMnSc6	šibeník
26	[number]	k4	26
Vila	vít	k5eAaImAgFnS	vít
Rudolfa	Rudolfa	k1gFnSc1	Rudolfa
Seidlera	Seidler	k1gMnSc2	Seidler
<g/>
,	,	kIx,	,
Václavkova	Václavkův	k2eAgFnSc1d1	Václavkova
2	[number]	k4	2
Vila	vila	k1gFnSc1	vila
Camilly	Camilla	k1gFnSc2	Camilla
a	a	k8xC	a
Josefa	Josef	k1gMnSc2	Josef
Krausových	Krausová	k1gFnPc2	Krausová
a	a	k8xC	a
Franze	Franze	k1gFnSc1	Franze
Bruckmanna	Bruckmanna	k1gFnSc1	Bruckmanna
<g/>
,	,	kIx,	,
Vídeňská	vídeňský	k2eAgFnSc1d1	Vídeňská
14	[number]	k4	14
Vila	vít	k5eAaImAgFnS	vít
na	na	k7c6	na
sile	silo	k1gNnSc6	silo
Barbory	Barbora	k1gFnSc2	Barbora
a	a	k8xC	a
Radima	Radim	k1gMnSc2	Radim
Králíkových	Králíkových	k2eAgNnSc1d1	Králíkových
<g/>
,	,	kIx,	,
Polská	polský	k2eAgFnSc1d1	polská
7	[number]	k4	7
Vila	vít	k5eAaImAgFnS	vít
Primavesi	Primavese	k1gFnSc4	Primavese
<g/>
,	,	kIx,	,
Univerzitní	univerzitní	k2eAgFnSc4d1	univerzitní
7	[number]	k4	7
Vila	vít	k5eAaImAgFnS	vít
Vladimíra	Vladimíra	k1gFnSc1	Vladimíra
Müllera	Müller	k1gMnSc2	Müller
<g/>
,	,	kIx,	,
Černochova	Černochův	k2eAgFnSc1d1	Černochova
6	[number]	k4	6
Vila	vila	k1gFnSc1	vila
Roberta	Robert	k1gMnSc2	Robert
Schneidera	Schneider	k1gMnSc2	Schneider
<g/>
,	,	kIx,	,
Černochova	Černochův	k2eAgFnSc1d1	Černochova
10	[number]	k4	10
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Olomoucké	olomoucký	k2eAgInPc1d1	olomoucký
parky	park	k1gInPc1	park
<g/>
.	.	kIx.	.
</s>
<s>
Čechovy	Čechův	k2eAgFnPc1d1	Čechova
sady	sada	k1gFnPc1	sada
Smetanovy	Smetanův	k2eAgFnSc2d1	Smetanova
sady	sada	k1gFnSc2	sada
–	–	k?	–
r.	r.	kA	r.
1866	[number]	k4	1866
byla	být	k5eAaImAgFnS	být
vysázena	vysázet	k5eAaPmNgFnS	vysázet
jírovcová	jírovcový	k2eAgFnSc1d1	jírovcová
a	a	k8xC	a
lipová	lipový	k2eAgFnSc1d1	Lipová
alej	alej	k1gFnSc1	alej
ve	v	k7c6	v
Smetanových	Smetanových	k2eAgInPc6d1	Smetanových
sadech	sad	k1gInPc6	sad
a	a	k8xC	a
slavnostně	slavnostně	k6eAd1	slavnostně
ji	on	k3xPp3gFnSc4	on
otevřel	otevřít	k5eAaPmAgMnS	otevřít
František	František	k1gMnSc1	František
Josef	Josef	k1gMnSc1	Josef
I.	I.	kA	I.
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
jsou	být	k5eAaImIp3nP	být
stromy	strom	k1gInPc1	strom
přiřezané	přiřezaný	k2eAgInPc1d1	přiřezaný
do	do	k7c2	do
vysokých	vysoký	k2eAgFnPc2d1	vysoká
rovných	rovný	k2eAgFnPc2d1	rovná
stěn	stěna	k1gFnPc2	stěna
díky	díky	k7c3	díky
prvnímu	první	k4xOgNnSc3	první
českému	český	k2eAgMnSc3d1	český
správci	správce	k1gMnSc3	správce
parku	park	k1gInSc2	park
Emanuelu	Emanuel	k1gMnSc3	Emanuel
Černému	Černý	k1gMnSc3	Černý
<g/>
.	.	kIx.	.
</s>
<s>
Bezručovy	Bezručův	k2eAgFnPc1d1	Bezručova
sady	sada	k1gFnPc1	sada
a	a	k8xC	a
rozárium	rozárium	k1gNnSc1	rozárium
se	s	k7c7	s
sbírkou	sbírka	k1gFnSc7	sbírka
40	[number]	k4	40
tisíc	tisíc	k4xCgInPc2	tisíc
keřů	keř	k1gInPc2	keř
růží	růž	k1gFnPc2	růž
<g/>
.	.	kIx.	.
</s>
<s>
Existence	existence	k1gFnSc1	existence
části	část	k1gFnSc2	část
rozária	rozárium	k1gNnSc2	rozárium
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
ohrožena	ohrozit	k5eAaPmNgFnS	ohrozit
výstavbou	výstavba	k1gFnSc7	výstavba
tenisových	tenisový	k2eAgInPc2d1	tenisový
kurtů	kurt	k1gInPc2	kurt
<g/>
.	.	kIx.	.
botanické	botanický	k2eAgFnPc4d1	botanická
zahrady	zahrada	k1gFnPc4	zahrada
Zoologická	zoologický	k2eAgFnSc1d1	zoologická
zahrada	zahrada	k1gFnSc1	zahrada
Olomouc	Olomouc	k1gFnSc1	Olomouc
na	na	k7c6	na
Svatém	svatý	k2eAgMnSc6d1	svatý
Kopečku	Kopeček	k1gMnSc6	Kopeček
Rozloha	rozloha	k1gFnSc1	rozloha
všech	všecek	k3xTgInPc2	všecek
parků	park	k1gInPc2	park
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
než	než	k8xS	než
47	[number]	k4	47
ha	ha	kA	ha
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
hlavních	hlavní	k2eAgFnPc2d1	hlavní
alejí	alej	k1gFnPc2	alej
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
2,5	[number]	k4	2,5
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Katedrála	katedrála	k1gFnSc1	katedrála
sv.	sv.	kA	sv.
Václava	Václava	k1gFnSc1	Václava
Bazilika	bazilika	k1gFnSc1	bazilika
Navštívení	navštívení	k1gNnPc2	navštívení
Panny	Panna	k1gFnSc2	Panna
Marie	Marie	k1gFnSc1	Marie
(	(	kIx(	(
<g/>
Svatý	svatý	k2eAgInSc1d1	svatý
Kopeček	kopeček	k1gInSc1	kopeček
<g/>
)	)	kIx)	)
Červený	červený	k2eAgInSc1d1	červený
kostel	kostel	k1gInSc1	kostel
Kostel	kostel	k1gInSc1	kostel
Neposkvrněného	poskvrněný	k2eNgNnSc2d1	neposkvrněné
početí	početí	k1gNnSc2	početí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
Kostel	kostel	k1gInSc1	kostel
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
Sněžné	sněžný	k2eAgFnSc2d1	sněžná
Kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Cyrila	Cyril	k1gMnSc2	Cyril
a	a	k8xC	a
Metoděje	Metoděj	k1gMnSc2	Metoděj
Kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Gorazda	Gorazd	k1gMnSc2	Gorazd
Kostel	kostel	k1gInSc4	kostel
svaté	svatý	k2eAgFnSc2d1	svatá
Kateřiny	Kateřina	k1gFnSc2	Kateřina
Kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Michala	Michal	k1gMnSc2	Michal
Kostel	kostel	k1gInSc4	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Mořice	Mořic	k1gMnSc2	Mořic
Kostel	kostel	k1gInSc4	kostel
Zvěstování	zvěstování	k1gNnSc2	zvěstování
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
Evangelický	evangelický	k2eAgInSc4d1	evangelický
kostel	kostel	k1gInSc4	kostel
Kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Filipa	Filip	k1gMnSc2	Filip
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
Jakuba	Jakub	k1gMnSc2	Jakub
Kostel	kostel	k1gInSc1	kostel
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
Pomocné	pomocný	k2eAgInPc1d1	pomocný
Kostel	kostel	k1gInSc1	kostel
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
Pomocnice	pomocnice	k1gFnSc2	pomocnice
křesťanů	křesťan	k1gMnPc2	křesťan
Kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Urbana	Urban	k1gMnSc2	Urban
Kostel	kostel	k1gInSc4	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Štěpána	Štěpán	k1gMnSc2	Štěpán
Kostel	kostel	k1gInSc4	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Ondřeje	Ondřej	k1gMnSc2	Ondřej
Kostel	kostel	k1gInSc4	kostel
svaté	svatý	k2eAgFnSc2d1	svatá
Barbory	Barbora	k1gFnSc2	Barbora
Husův	Husův	k2eAgInSc1d1	Husův
sbor	sbor	k1gInSc1	sbor
Sbor	sbor	k1gInSc1	sbor
Prokopa	Prokop	k1gMnSc2	Prokop
Holého	Holého	k2eAgFnSc2d1	Holého
Kaple	kaple	k1gFnSc2	kaple
svatého	svatý	k2eAgMnSc2d1	svatý
Jana	Jan	k1gMnSc2	Jan
Sarkandra	Sarkandr	k1gMnSc2	Sarkandr
Kaple	kaple	k1gFnSc2	kaple
svaté	svatý	k2eAgFnSc2d1	svatá
Anny	Anna	k1gFnSc2	Anna
Kaple	kaple	k1gFnSc2	kaple
svaté	svatý	k2eAgFnSc2d1	svatá
Barbory	Barbora	k1gFnSc2	Barbora
Kaple	kaple	k1gFnSc2	kaple
svatého	svatý	k2eAgMnSc2d1	svatý
Františka	František	k1gMnSc2	František
z	z	k7c2	z
Assisi	Assise	k1gFnSc6	Assise
Kaple	kaple	k1gFnPc1	kaple
Božského	božský	k2eAgNnSc2d1	božské
srdce	srdce	k1gNnSc2	srdce
Páně	páně	k2eAgFnSc2d1	páně
Kaple	kaple	k1gFnSc2	kaple
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g />
.	.	kIx.	.
</s>
<s>
Opatrovnice	opatrovnice	k1gFnSc1	opatrovnice
Kaple	kaple	k1gFnSc1	kaple
Božího	boží	k2eAgNnSc2d1	boží
Těla	tělo	k1gNnSc2	tělo
Kaple	kaple	k1gFnSc2	kaple
Královny	královna	k1gFnSc2	královna
Kaple	kaple	k1gFnSc2	kaple
svatého	svatý	k2eAgMnSc2d1	svatý
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomuckého	k2eAgFnSc2d1	Nepomuckého
Kaple	kaple	k1gFnSc2	kaple
Neposkvrněného	poskvrněný	k2eNgNnSc2d1	neposkvrněné
početí	početí	k1gNnSc2	početí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
Kaple	kaple	k1gFnSc2	kaple
Božího	boží	k2eAgNnSc2d1	boží
milosrdenství	milosrdenství	k1gNnSc2	milosrdenství
Kaple	kaple	k1gFnSc2	kaple
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
Bolestné	bolestný	k2eAgFnSc2d1	bolestná
V	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
a	a	k8xC	a
přidružených	přidružený	k2eAgFnPc6d1	přidružená
obcích	obec	k1gFnPc6	obec
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
celkem	celkem	k6eAd1	celkem
jedenáct	jedenáct	k4xCc4	jedenáct
veřejných	veřejný	k2eAgNnPc2d1	veřejné
pohřebišť	pohřebiště	k1gNnPc2	pohřebiště
<g/>
:	:	kIx,	:
Neředín	Neředín	k1gInSc1	Neředín
–	–	k?	–
ústřední	ústřední	k2eAgInSc1d1	ústřední
hřbitov	hřbitov	k1gInSc1	hřbitov
<g/>
,	,	kIx,	,
Nová	nový	k2eAgFnSc1d1	nová
Ulice	ulice	k1gFnSc1	ulice
<g/>
,	,	kIx,	,
Nedvězí	Nedvězí	k1gNnSc1	Nedvězí
<g/>
,	,	kIx,	,
Slavonín	Slavonín	k1gInSc1	Slavonín
<g/>
,	,	kIx,	,
Nové	Nové	k2eAgInPc1d1	Nové
Sady	sad	k1gInPc1	sad
<g/>
,	,	kIx,	,
Holice	Holice	k1gFnPc1	Holice
<g/>
,	,	kIx,	,
Hodolany	Hodolan	k1gInPc1	Hodolan
<g/>
,	,	kIx,	,
Chválkovice	Chválkovice	k1gFnPc1	Chválkovice
<g/>
,	,	kIx,	,
Svatý	svatý	k2eAgMnSc1d1	svatý
Kopeček	Kopeček	k1gMnSc1	Kopeček
<g/>
,	,	kIx,	,
Černovír	Černovír	k1gMnSc1	Černovír
a	a	k8xC	a
Chomoutov	Chomoutov	k1gInSc1	Chomoutov
<g/>
.	.	kIx.	.
</s>
<s>
Památkově	památkově	k6eAd1	památkově
chráněné	chráněný	k2eAgNnSc1d1	chráněné
je	být	k5eAaImIp3nS	být
funkcionalistické	funkcionalistický	k2eAgNnSc1d1	funkcionalistické
krematorium	krematorium	k1gNnSc1	krematorium
na	na	k7c6	na
hřbitově	hřbitov	k1gInSc6	hřbitov
v	v	k7c6	v
Neředíně	Neředína	k1gFnSc6	Neředína
<g/>
.	.	kIx.	.
</s>
<s>
Krematorium	krematorium	k1gNnSc1	krematorium
a	a	k8xC	a
pohřebiště	pohřebiště	k1gNnSc1	pohřebiště
spravuje	spravovat	k5eAaImIp3nS	spravovat
městská	městský	k2eAgFnSc1d1	městská
příspěvková	příspěvkový	k2eAgFnSc1d1	příspěvková
organizace	organizace	k1gFnSc1	organizace
Hřbitovy	hřbitov	k1gInPc4	hřbitov
města	město	k1gNnSc2	město
Olomouce	Olomouc	k1gFnSc2	Olomouc
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zavraždění	zavraždění	k1gNnSc6	zavraždění
Václava	Václav	k1gMnSc2	Václav
III	III	kA	III
<g/>
.	.	kIx.	.
na	na	k7c6	na
olomouckém	olomoucký	k2eAgInSc6d1	olomoucký
hradě	hrad	k1gInSc6	hrad
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1306	[number]	k4	1306
nastoupili	nastoupit	k5eAaPmAgMnP	nastoupit
Lucemburkové	Lucemburk	k1gMnPc1	Lucemburk
a	a	k8xC	a
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
přišel	přijít	k5eAaPmAgMnS	přijít
velmi	velmi	k6eAd1	velmi
významný	významný	k2eAgInSc4d1	významný
rozvoj	rozvoj	k1gInSc4	rozvoj
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
křižovatka	křižovatka	k1gFnSc1	křižovatka
cest	cesta	k1gFnPc2	cesta
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
největším	veliký	k2eAgMnSc7d3	veliký
centrem	centr	k1gMnSc7	centr
obchodu	obchod	k1gInSc2	obchod
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
kterému	který	k3yIgMnSc3	který
obchod	obchod	k1gInSc1	obchod
přinášel	přinášet	k5eAaImAgInS	přinášet
nemalé	malý	k2eNgInPc4d1	nemalý
zisky	zisk	k1gInPc4	zisk
<g/>
,	,	kIx,	,
čilý	čilý	k2eAgInSc1d1	čilý
ruch	ruch	k1gInSc1	ruch
na	na	k7c6	na
tržišti	tržiště	k1gNnSc6	tržiště
samozřejmě	samozřejmě	k6eAd1	samozřejmě
podporovalo	podporovat	k5eAaImAgNnS	podporovat
a	a	k8xC	a
co	co	k9	co
víc	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
získalo	získat	k5eAaPmAgNnS	získat
různá	různý	k2eAgNnPc4d1	různé
královská	královský	k2eAgNnPc4d1	královské
privilegia	privilegium	k1gNnPc4	privilegium
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimiž	jenž	k3xRgMnPc7	jenž
bylo	být	k5eAaImAgNnS	být
i	i	k9	i
právo	právo	k1gNnSc4	právo
vybudovat	vybudovat	k5eAaPmF	vybudovat
další	další	k2eAgFnSc4d1	další
tržnici	tržnice	k1gFnSc4	tržnice
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
turistů	turist	k1gMnPc2	turist
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
činil	činit	k5eAaImAgInS	činit
111	[number]	k4	111
700	[number]	k4	700
<g/>
,	,	kIx,	,
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2006	[number]	k4	2006
jich	on	k3xPp3gInPc2	on
do	do	k7c2	do
Olomouce	Olomouc	k1gFnSc2	Olomouc
zavítalo	zavítat	k5eAaPmAgNnS	zavítat
113	[number]	k4	113
400	[number]	k4	400
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
šlo	jít	k5eAaImAgNnS	jít
už	už	k6eAd1	už
o	o	k7c4	o
121	[number]	k4	121
000	[number]	k4	000
turistů	turist	k1gMnPc2	turist
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
jediné	jediný	k2eAgNnSc1d1	jediné
město	město	k1gNnSc1	město
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
má	mít	k5eAaImIp3nS	mít
zachovalou	zachovalý	k2eAgFnSc4d1	zachovalá
parní	parní	k2eAgFnSc4d1	parní
vodárnu	vodárna	k1gFnSc4	vodárna
a	a	k8xC	a
jako	jako	k8xC	jako
jediné	jediný	k2eAgNnSc1d1	jediné
město	město	k1gNnSc1	město
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
000	[number]	k4	000
obyvateli	obyvatel	k1gMnPc7	obyvatel
je	být	k5eAaImIp3nS	být
100	[number]	k4	100
<g/>
%	%	kIx~	%
zásobováno	zásobovat	k5eAaImNgNnS	zásobovat
vodou	voda	k1gFnSc7	voda
z	z	k7c2	z
podzemních	podzemní	k2eAgInPc2d1	podzemní
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Vydatnost	vydatnost	k1gFnSc1	vydatnost
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
:	:	kIx,	:
Olomouc-Černovír	Olomouc-Černovír	k1gInSc1	Olomouc-Černovír
300	[number]	k4	300
l	l	kA	l
/	/	kIx~	/
s	s	k7c7	s
(	(	kIx(	(
<g/>
část	část	k1gFnSc1	část
lze	lze	k6eAd1	lze
použít	použít	k5eAaPmF	použít
bez	bez	k7c2	bez
úpravy	úprava	k1gFnSc2	úprava
<g/>
)	)	kIx)	)
Příkazy	příkaz	k1gInPc7	příkaz
250	[number]	k4	250
l	l	kA	l
/	/	kIx~	/
s	s	k7c7	s
Litovel	Litovel	k1gFnSc1	Litovel
160	[number]	k4	160
l	l	kA	l
/	/	kIx~	/
s	s	k7c7	s
(	(	kIx(	(
<g/>
lze	lze	k6eAd1	lze
použít	použít	k5eAaPmF	použít
bez	bez	k7c2	bez
úpravy	úprava	k1gFnSc2	úprava
<g/>
)	)	kIx)	)
Senice	Senice	k1gFnSc2	Senice
na	na	k7c6	na
Hané	Haná	k1gFnSc6	Haná
50	[number]	k4	50
l	l	kA	l
/	/	kIx~	/
s	s	k7c7	s
(	(	kIx(	(
<g/>
lze	lze	k6eAd1	lze
použít	použít	k5eAaPmF	použít
bez	bez	k7c2	bez
úpravy	úprava	k1gFnSc2	úprava
<g/>
)	)	kIx)	)
Město	město	k1gNnSc1	město
spotřebuje	spotřebovat	k5eAaPmIp3nS	spotřebovat
denně	denně	k6eAd1	denně
asi	asi	k9	asi
27	[number]	k4	27
000	[number]	k4	000
m	m	kA	m
<g/>
3	[number]	k4	3
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Čistírna	čistírna	k1gFnSc1	čistírna
odpadních	odpadní	k2eAgFnPc2d1	odpadní
vod	voda	k1gFnPc2	voda
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
části	část	k1gFnSc6	část
Olomouc-Nové	Olomouc-Nové	k2eAgFnSc2d1	Olomouc-Nové
Sady	sada	k1gFnSc2	sada
<g/>
.	.	kIx.	.
</s>
<s>
Dodavatelem	dodavatel	k1gMnSc7	dodavatel
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
provozovatelem	provozovatel	k1gMnSc7	provozovatel
vodovodů	vodovod	k1gInPc2	vodovod
<g/>
,	,	kIx,	,
kanalizace	kanalizace	k1gFnSc2	kanalizace
a	a	k8xC	a
čistírny	čistírna	k1gFnSc2	čistírna
je	být	k5eAaImIp3nS	být
Moravská	moravský	k2eAgFnSc1d1	Moravská
vodárenská	vodárenský	k2eAgFnSc1d1	vodárenská
a.	a.	k?	a.
s.	s.	k?	s.
(	(	kIx(	(
<g/>
do	do	k7c2	do
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2008	[number]	k4	2008
s	s	k7c7	s
názvem	název	k1gInSc7	název
Středomoravská	středomoravský	k2eAgFnSc1d1	Středomoravská
vodárenská	vodárenský	k2eAgFnSc1d1	vodárenská
a.	a.	k?	a.
s.	s.	k?	s.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Elektřinu	elektřina	k1gFnSc4	elektřina
rozvádí	rozvádět	k5eAaImIp3nP	rozvádět
v	v	k7c6	v
síti	síť	k1gFnSc6	síť
vysokého	vysoký	k2eAgNnSc2d1	vysoké
a	a	k8xC	a
nízkého	nízký	k2eAgNnSc2d1	nízké
napětí	napětí	k1gNnSc2	napětí
ČEZ	ČEZ	kA	ČEZ
a.s.	a.s.	k?	a.s.
Teplo	teplo	k1gNnSc1	teplo
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
teplárna	teplárna	k1gFnSc1	teplárna
na	na	k7c4	na
uhlí	uhlí	k1gNnSc4	uhlí
a	a	k8xC	a
výtopna	výtopna	k1gFnSc1	výtopna
na	na	k7c4	na
mazut	mazut	k1gInSc4	mazut
a	a	k8xC	a
zemní	zemní	k2eAgInSc4d1	zemní
plyn	plyn	k1gInSc4	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Teplo	teplo	k1gNnSc1	teplo
rozvádí	rozvádět	k5eAaImIp3nS	rozvádět
síť	síť	k1gFnSc4	síť
horkovodů	horkovod	k1gInPc2	horkovod
a	a	k8xC	a
parovodů	parovod	k1gInPc2	parovod
<g/>
.	.	kIx.	.
</s>
<s>
Dodavatel	dodavatel	k1gMnSc1	dodavatel
<g/>
:	:	kIx,	:
Moravskoslezské	moravskoslezský	k2eAgFnPc1d1	Moravskoslezská
teplárny	teplárna	k1gFnPc1	teplárna
<g/>
,	,	kIx,	,
Teplárna	teplárna	k1gFnSc1	teplárna
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
.	.	kIx.	.
</s>
<s>
Zemní	zemní	k2eAgInSc1d1	zemní
plyn	plyn	k1gInSc1	plyn
rozvádí	rozvádět	k5eAaImIp3nS	rozvádět
plynovody	plynovod	k1gInPc4	plynovod
po	po	k7c6	po
celém	celý	k2eAgNnSc6d1	celé
území	území	k1gNnSc6	území
města	město	k1gNnSc2	město
Severomoravská	severomoravský	k2eAgFnSc1d1	Severomoravská
plynárenská	plynárenský	k2eAgFnSc1d1	plynárenská
a.s.	a.s.	k?	a.s.
Svoz	svoz	k1gInSc1	svoz
odpadů	odpad	k1gInPc2	odpad
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
Technické	technický	k2eAgFnPc4d1	technická
služby	služba	k1gFnPc4	služba
města	město	k1gNnSc2	město
Olomouce	Olomouc	k1gFnSc2	Olomouc
<g/>
,	,	kIx,	,
a.	a.	k?	a.
s.	s.	k?	s.
Separovaný	separovaný	k2eAgInSc4d1	separovaný
odpad	odpad	k1gInSc4	odpad
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
je	být	k5eAaImIp3nS	být
svážen	svážit	k5eAaPmNgInS	svážit
ze	z	k7c2	z
širšího	široký	k2eAgNnSc2d2	širší
okolí	okolí	k1gNnSc2	okolí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přibližně	přibližně	k6eAd1	přibližně
kopíruje	kopírovat	k5eAaImIp3nS	kopírovat
trendy	trend	k1gInPc4	trend
v	v	k7c6	v
odpadovém	odpadový	k2eAgNnSc6d1	odpadové
hospodářství	hospodářství	k1gNnSc6	hospodářství
města	město	k1gNnSc2	město
Olomouce	Olomouc	k1gFnSc2	Olomouc
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
rok	rok	k1gInSc4	rok
2003	[number]	k4	2003
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
704	[number]	k4	704
t	t	k?	t
papíru	papír	k1gInSc2	papír
<g/>
,	,	kIx,	,
440	[number]	k4	440
t	t	k?	t
skla	sklo	k1gNnSc2	sklo
<g/>
,	,	kIx,	,
369	[number]	k4	369
t	t	k?	t
plastů	plast	k1gInPc2	plast
a	a	k8xC	a
0,5	[number]	k4	0,5
t	t	k?	t
nápojových	nápojový	k2eAgInPc2d1	nápojový
kartonů	karton	k1gInPc2	karton
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
pak	pak	k6eAd1	pak
bylo	být	k5eAaImAgNnS	být
z	z	k7c2	z
Olomouce	Olomouc	k1gFnSc2	Olomouc
svezeno	svezen	k2eAgNnSc1d1	svezeno
<g/>
:	:	kIx,	:
17	[number]	k4	17
394	[number]	k4	394
t	t	k?	t
komunálního	komunální	k2eAgInSc2d1	komunální
odpadu	odpad	k1gInSc2	odpad
<g/>
,	,	kIx,	,
1	[number]	k4	1
486	[number]	k4	486
t	t	k?	t
papíru	papír	k1gInSc2	papír
<g/>
,	,	kIx,	,
851	[number]	k4	851
t	t	k?	t
skla	sklo	k1gNnSc2	sklo
<g/>
,	,	kIx,	,
562	[number]	k4	562
t	t	k?	t
plastů	plast	k1gInPc2	plast
a	a	k8xC	a
55	[number]	k4	55
t	t	k?	t
nápojových	nápojový	k2eAgInPc2d1	nápojový
kartonů	karton	k1gInPc2	karton
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
množství	množství	k1gNnSc2	množství
separovaného	separovaný	k2eAgInSc2d1	separovaný
odpadu	odpad	k1gInSc2	odpad
(	(	kIx(	(
<g/>
svezeného	svezený	k2eAgInSc2d1	svezený
ze	z	k7c2	z
širšího	široký	k2eAgNnSc2d2	širší
okolí	okolí	k1gNnSc2	okolí
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
roku	rok	k1gInSc3	rok
udáváno	udávat	k5eAaImNgNnS	udávat
1488	[number]	k4	1488
t	t	k?	t
papíru	papír	k1gInSc2	papír
<g/>
,	,	kIx,	,
859	[number]	k4	859
t	t	k?	t
skla	sklo	k1gNnSc2	sklo
<g/>
,	,	kIx,	,
562	[number]	k4	562
t	t	k?	t
plastů	plast	k1gInPc2	plast
<g/>
,	,	kIx,	,
54,76	[number]	k4	54,76
t	t	k?	t
nápojových	nápojový	k2eAgInPc2d1	nápojový
kartonů	karton	k1gInPc2	karton
<g/>
,	,	kIx,	,
a	a	k8xC	a
pro	pro	k7c4	pro
1	[number]	k4	1
<g/>
.	.	kIx.	.
pololetí	pololetí	k1gNnSc6	pololetí
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
823	[number]	k4	823
<g />
.	.	kIx.	.
</s>
<s>
t	t	k?	t
papíru	papír	k1gInSc2	papír
<g/>
,	,	kIx,	,
425	[number]	k4	425
t	t	k?	t
skla	sklo	k1gNnSc2	sklo
<g/>
,	,	kIx,	,
308	[number]	k4	308
t	t	k?	t
plastů	plast	k1gInPc2	plast
<g/>
,	,	kIx,	,
31	[number]	k4	31
t	t	k?	t
nápojových	nápojový	k2eAgInPc2d1	nápojový
kartonů	karton	k1gInPc2	karton
Lesní	lesní	k2eAgInSc4d1	lesní
majetek	majetek	k1gInSc4	majetek
města	město	k1gNnSc2	město
Olomouce	Olomouc	k1gFnSc2	Olomouc
na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
4002,96	[number]	k4	4002,96
ha	ha	kA	ha
(	(	kIx(	(
<g/>
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
3871,56	[number]	k4	3871,56
ha	ha	kA	ha
je	být	k5eAaImIp3nS	být
porostní	porostní	k2eAgFnSc1d1	porostní
půda	půda	k1gFnSc1	půda
<g/>
,	,	kIx,	,
stav	stav	k1gInSc1	stav
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
spravuje	spravovat	k5eAaImIp3nS	spravovat
akciová	akciový	k2eAgFnSc1d1	akciová
společnost	společnost	k1gFnSc1	společnost
Lesy	les	k1gInPc1	les
města	město	k1gNnSc2	město
Olomouce	Olomouc	k1gFnSc2	Olomouc
a.s.	a.s.	k?	a.s.
Hospodaření	hospodaření	k1gNnSc1	hospodaření
na	na	k7c6	na
velké	velký	k2eAgFnSc6d1	velká
části	část	k1gFnSc6	část
(	(	kIx(	(
<g/>
1204	[number]	k4	1204
ha	ha	kA	ha
<g/>
,	,	kIx,	,
31	[number]	k4	31
%	%	kIx~	%
celkové	celkový	k2eAgFnSc2d1	celková
plochy	plocha	k1gFnSc2	plocha
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
ovlivněno	ovlivnit	k5eAaPmNgNnS	ovlivnit
kvůli	kvůli	k7c3	kvůli
ochraně	ochrana	k1gFnSc3	ochrana
přírody	příroda	k1gFnSc2	příroda
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
lesní	lesní	k2eAgInSc4d1	lesní
majetek	majetek	k1gInSc4	majetek
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
800	[number]	k4	800
ha	ha	kA	ha
získalo	získat	k5eAaPmAgNnS	získat
město	město	k1gNnSc1	město
Olomouc	Olomouc	k1gFnSc1	Olomouc
v	v	k7c6	v
letech	let	k1gInPc6	let
1352	[number]	k4	1352
<g/>
–	–	k?	–
<g/>
1393	[number]	k4	1393
v	v	k7c6	v
k.ú.	k.ú.	k?	k.ú.
Grygov	Grygov	k1gInSc1	Grygov
jako	jako	k8xS	jako
dar	dar	k1gInSc1	dar
od	od	k7c2	od
krále	král	k1gMnSc2	král
Karla	Karel	k1gMnSc2	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
lesy	les	k1gInPc1	les
získalo	získat	k5eAaPmAgNnS	získat
město	město	k1gNnSc1	město
např.	např.	kA	např.
nákupem	nákup	k1gInSc7	nákup
od	od	k7c2	od
moravského	moravský	k2eAgMnSc2d1	moravský
zemského	zemský	k2eAgMnSc2d1	zemský
hejtmana	hejtman	k1gMnSc2	hejtman
Jana	Jan	k1gMnSc2	Jan
z	z	k7c2	z
Lipé	Lipá	k1gFnSc2	Lipá
(	(	kIx(	(
<g/>
1534	[number]	k4	1534
<g/>
,	,	kIx,	,
250	[number]	k4	250
ha	ha	kA	ha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Václava	Václav	k1gMnSc4	Václav
Haugvice	Haugvice	k1gFnSc2	Haugvice
z	z	k7c2	z
Biskupic	Biskupice	k1gFnPc2	Biskupice
(	(	kIx(	(
<g/>
1546	[number]	k4	1546
<g/>
,	,	kIx,	,
500	[number]	k4	500
ha	ha	kA	ha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kardinála	kardinál	k1gMnSc4	kardinál
Ditrichštejna	Ditrichštejn	k1gMnSc4	Ditrichštejn
(	(	kIx(	(
<g/>
1606	[number]	k4	1606
<g/>
,	,	kIx,	,
1	[number]	k4	1
000	[number]	k4	000
ha	ha	kA	ha
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Liechtensteina	Liechtenstein	k1gMnSc4	Liechtenstein
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1936	[number]	k4	1936
<g/>
,	,	kIx,	,
988	[number]	k4	988
ha	ha	kA	ha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1952	[number]	k4	1952
byl	být	k5eAaImAgInS	být
lesní	lesní	k2eAgInSc1d1	lesní
majetek	majetek	k1gInSc1	majetek
(	(	kIx(	(
<g/>
3984	[number]	k4	3984
ha	ha	kA	ha
<g/>
)	)	kIx)	)
převeden	převést	k5eAaPmNgInS	převést
na	na	k7c4	na
Československé	československý	k2eAgInPc4d1	československý
státní	státní	k2eAgInPc4d1	státní
lesy	les	k1gInPc4	les
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1993	[number]	k4	1993
převzala	převzít	k5eAaPmAgFnS	převzít
lesní	lesní	k2eAgInSc4d1	lesní
majetek	majetek	k1gInSc4	majetek
Správa	správa	k1gFnSc1	správa
lesů	les	k1gInPc2	les
města	město	k1gNnSc2	město
Olomouce	Olomouc	k1gFnSc2	Olomouc
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2010	[number]	k4	2010
se	se	k3xPyFc4	se
přeměnila	přeměnit	k5eAaPmAgFnS	přeměnit
na	na	k7c4	na
akciovou	akciový	k2eAgFnSc4d1	akciová
společnost	společnost	k1gFnSc4	společnost
Lesy	les	k1gInPc4	les
města	město	k1gNnSc2	město
Olomouce	Olomouc	k1gFnSc2	Olomouc
a.s.	a.s.	k?	a.s.
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
a	a	k8xC	a
Městská	městský	k2eAgFnSc1d1	městská
autobusová	autobusový	k2eAgFnSc1d1	autobusová
doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1845	[number]	k4	1845
začaly	začít	k5eAaPmAgInP	začít
jezdit	jezdit	k5eAaImF	jezdit
mezi	mezi	k7c7	mezi
centrem	centrum	k1gNnSc7	centrum
města	město	k1gNnSc2	město
a	a	k8xC	a
vzdáleným	vzdálený	k2eAgNnSc7d1	vzdálené
nádražím	nádraží	k1gNnSc7	nádraží
omnibusy	omnibus	k1gInPc1	omnibus
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
ale	ale	k9	ale
kapacitně	kapacitně	k6eAd1	kapacitně
nedostačovaly	dostačovat	k5eNaImAgFnP	dostačovat
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
úseku	úsek	k1gInSc6	úsek
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1899	[number]	k4	1899
postavena	postaven	k2eAgFnSc1d1	postavena
tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
trať	trať	k1gFnSc1	trať
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
byly	být	k5eAaImAgFnP	být
tratě	trať	k1gFnPc1	trať
prodlužovány	prodlužovat	k5eAaImNgFnP	prodlužovat
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1940	[number]	k4	1940
probíhalo	probíhat	k5eAaImAgNnS	probíhat
jejich	jejich	k3xOp3gNnSc1	jejich
postupné	postupný	k2eAgNnSc1d1	postupné
zdvoukolejňování	zdvoukolejňování	k1gNnSc1	zdvoukolejňování
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
rozsáhlé	rozsáhlý	k2eAgFnSc3d1	rozsáhlá
reorganizaci	reorganizace	k1gFnSc3	reorganizace
tramvajové	tramvajový	k2eAgFnSc2d1	tramvajová
dopravy	doprava	k1gFnSc2	doprava
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
prodloužení	prodloužení	k1gNnSc3	prodloužení
–	–	k?	–
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
trať	trať	k1gFnSc1	trať
na	na	k7c6	na
třídě	třída	k1gFnSc6	třída
Kosmonautů	kosmonaut	k1gMnPc2	kosmonaut
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
pomohla	pomoct	k5eAaPmAgFnS	pomoct
přetíženému	přetížený	k2eAgInSc3d1	přetížený
úseku	úsek	k1gInSc3	úsek
na	na	k7c6	na
Masarykově	Masarykův	k2eAgFnSc6d1	Masarykova
třídě	třída	k1gFnSc6	třída
přes	přes	k7c4	přes
centrum	centrum	k1gNnSc4	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
nová	nový	k2eAgFnSc1d1	nová
trať	trať	k1gFnSc1	trať
z	z	k7c2	z
areálu	areál	k1gInSc2	areál
Šantovka	Šantovka	k1gFnSc1	Šantovka
na	na	k7c6	na
Nové	Nová	k1gFnSc6	Nová
Sady	sada	k1gFnSc2	sada
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1927	[number]	k4	1927
se	se	k3xPyFc4	se
Olomouc	Olomouc	k1gFnSc1	Olomouc
stala	stát	k5eAaPmAgFnS	stát
prvním	první	k4xOgNnSc7	první
moravským	moravský	k2eAgNnSc7d1	Moravské
městem	město	k1gNnSc7	město
s	s	k7c7	s
vlastní	vlastní	k2eAgFnSc7d1	vlastní
autobusovou	autobusový	k2eAgFnSc7d1	autobusová
dopravou	doprava	k1gFnSc7	doprava
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Městskou	městský	k2eAgFnSc4d1	městská
hromadnou	hromadný	k2eAgFnSc4d1	hromadná
dopravu	doprava	k1gFnSc4	doprava
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
Dopravní	dopravní	k2eAgInSc1d1	dopravní
podnik	podnik	k1gInSc1	podnik
města	město	k1gNnSc2	město
Olomouce	Olomouc	k1gFnSc2	Olomouc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
tyto	tento	k3xDgFnPc4	tento
vlaková	vlakový	k2eAgFnSc1d1	vlaková
nádraží	nádraží	k1gNnSc2	nádraží
a	a	k8xC	a
železniční	železniční	k2eAgFnSc2d1	železniční
zastávky	zastávka	k1gFnSc2	zastávka
<g/>
:	:	kIx,	:
Olomouc	Olomouc	k1gFnSc1	Olomouc
hlavní	hlavní	k2eAgNnSc1d1	hlavní
nádraží	nádraží	k1gNnSc1	nádraží
(	(	kIx(	(
<g/>
tratě	trať	k1gFnPc1	trať
<g/>
:	:	kIx,	:
270	[number]	k4	270
<g/>
,	,	kIx,	,
275	[number]	k4	275
<g/>
,	,	kIx,	,
290	[number]	k4	290
<g/>
,	,	kIx,	,
301	[number]	k4	301
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Olomouc-Smetanovy	Olomouc-Smetanův	k2eAgFnPc1d1	Olomouc-Smetanův
sady	sada	k1gFnPc1	sada
(	(	kIx(	(
<g/>
trať	trať	k1gFnSc1	trať
<g/>
:	:	kIx,	:
275	[number]	k4	275
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Olomouc-Nová	Olomouc-Nový	k2eAgFnSc1d1	Olomouc-Nový
Ulice	ulice	k1gFnSc1	ulice
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
trať	trať	k1gFnSc1	trať
<g/>
:	:	kIx,	:
275	[number]	k4	275
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Olomouc	Olomouc	k1gFnSc1	Olomouc
město	město	k1gNnSc4	město
(	(	kIx(	(
<g/>
trať	trať	k1gFnSc1	trať
<g/>
:	:	kIx,	:
275	[number]	k4	275
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Olomouc-Hejčín	Olomouc-Hejčín	k1gMnSc1	Olomouc-Hejčín
(	(	kIx(	(
<g/>
trať	trať	k1gFnSc1	trať
<g/>
:	:	kIx,	:
275	[number]	k4	275
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Olomouc-Řepčín	Olomouc-Řepčín	k1gMnSc1	Olomouc-Řepčín
(	(	kIx(	(
<g/>
trať	trať	k1gFnSc1	trať
<g/>
:	:	kIx,	:
275	[number]	k4	275
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Olomouc-Nové	Olomouc-Nové	k2eAgInPc1d1	Olomouc-Nové
Sady	sad	k1gInPc1	sad
(	(	kIx(	(
<g/>
trať	trať	k1gFnSc1	trať
<g/>
:	:	kIx,	:
301	[number]	k4	301
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Nemilany	Nemilan	k1gInPc1	Nemilan
(	(	kIx(	(
<g/>
trať	trať	k1gFnSc1	trať
<g/>
:	:	kIx,	:
301	[number]	k4	301
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
zde	zde	k6eAd1	zde
jsou	být	k5eAaImIp3nP	být
dvě	dva	k4xCgFnPc1	dva
autobusové	autobusový	k2eAgFnPc1d1	autobusová
nádraží	nádraží	k1gNnSc4	nádraží
a	a	k8xC	a
letiště	letiště	k1gNnSc4	letiště
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
Schengenský	schengenský	k2eAgInSc4d1	schengenský
prostor	prostor	k1gInSc4	prostor
v	v	k7c6	v
Neředíně	Neředína	k1gFnSc6	Neředína
(	(	kIx(	(
<g/>
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
centrální	centrální	k2eAgNnSc1d1	centrální
letiště	letiště	k1gNnSc1	letiště
helikoptér	helikoptéra	k1gFnPc2	helikoptéra
policie	policie	k1gFnSc2	policie
<g/>
,	,	kIx,	,
hasičů	hasič	k1gMnPc2	hasič
a	a	k8xC	a
pro	pro	k7c4	pro
nemocnici	nemocnice	k1gFnSc4	nemocnice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
SK	Sk	kA	Sk
Sigma	sigma	k1gNnSc1	sigma
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
,	,	kIx,	,
fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
klub	klub	k1gInSc1	klub
HC	HC	kA	HC
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
,	,	kIx,	,
hokejový	hokejový	k2eAgInSc1d1	hokejový
klub	klub	k1gInSc1	klub
Slackline	Slacklin	k1gInSc5	Slacklin
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
slackline	slacklin	k1gInSc5	slacklin
klub	klub	k1gInSc1	klub
v	v	k7c6	v
ČR	ČR	kA	ČR
(	(	kIx(	(
<g/>
web	web	k1gInSc1	web
<g/>
)	)	kIx)	)
Skokani	skokan	k1gMnPc1	skokan
Olomouc	Olomouc	k1gFnSc4	Olomouc
<g/>
,	,	kIx,	,
baseballový	baseballový	k2eAgInSc4d1	baseballový
klub	klub	k1gInSc4	klub
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
HFK	HFK	kA	HFK
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
,	,	kIx,	,
fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
klub	klub	k1gInSc1	klub
DHK	DHK	kA	DHK
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
,	,	kIx,	,
dámský	dámský	k2eAgInSc1d1	dámský
házenkářský	házenkářský	k2eAgInSc1d1	házenkářský
klub	klub	k1gInSc1	klub
SK	Sk	kA	Sk
UP	UP	kA	UP
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
,	,	kIx,	,
ženský	ženský	k2eAgInSc1d1	ženský
volejbalový	volejbalový	k2eAgInSc1d1	volejbalový
klub	klub	k1gInSc1	klub
Basketbal	basketbal	k1gInSc1	basketbal
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
,	,	kIx,	,
basketbal	basketbal	k1gInSc1	basketbal
klub	klub	k1gInSc1	klub
T.	T.	kA	T.
J.	J.	kA	J.
Sokol	Sokol	k1gMnSc1	Sokol
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
,	,	kIx,	,
Sokol	Sokol	k1gMnSc1	Sokol
<g/>
,	,	kIx,	,
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
je	být	k5eAaImIp3nS	být
celkem	celkem	k6eAd1	celkem
10	[number]	k4	10
místních	místní	k2eAgFnPc2d1	místní
jednot	jednota	k1gFnPc2	jednota
<g/>
.	.	kIx.	.
</s>
<s>
AK	AK	kA	AK
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
,	,	kIx,	,
aerobik	aerobik	k1gInSc1	aerobik
klub	klub	k1gInSc1	klub
Olomouc	Olomouc	k1gFnSc1	Olomouc
AK	AK	kA	AK
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
,	,	kIx,	,
atletický	atletický	k2eAgInSc4d1	atletický
klub	klub	k1gInSc4	klub
Haná	Haná	k1gFnSc1	Haná
Dragons	Dragons	k1gInSc1	Dragons
<g/>
,	,	kIx,	,
klub	klub	k1gInSc1	klub
dračích	dračí	k2eAgFnPc2d1	dračí
lodí	loď	k1gFnPc2	loď
(	(	kIx(	(
<g/>
web	web	k1gInSc1	web
<g/>
)	)	kIx)	)
TJ	tj	kA	tj
Lodní	lodní	k2eAgInPc1d1	lodní
Sporty	sport	k1gInPc1	sport
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
,	,	kIx,	,
sdružuje	sdružovat	k5eAaImIp3nS	sdružovat
Kanoistický	kanoistický	k2eAgInSc1d1	kanoistický
a	a	k8xC	a
Veslařský	veslařský	k2eAgInSc1d1	veslařský
Klub	klub	k1gInSc1	klub
<g/>
:	:	kIx,	:
VK	VK	kA	VK
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
,	,	kIx,	,
veslařský	veslařský	k2eAgInSc1d1	veslařský
klub	klub	k1gInSc1	klub
(	(	kIx(	(
<g/>
web	web	k1gInSc1	web
<g/>
)	)	kIx)	)
KK	KK	kA	KK
<g />
.	.	kIx.	.
</s>
<s>
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
,	,	kIx,	,
kanoistický	kanoistický	k2eAgInSc1d1	kanoistický
klub	klub	k1gInSc1	klub
–	–	k?	–
rychlostní	rychlostní	k2eAgFnSc1d1	rychlostní
kanoistika	kanoistika	k1gFnSc1	kanoistika
(	(	kIx(	(
<g/>
web	web	k1gInSc1	web
<g/>
)	)	kIx)	)
RC	RC	kA	RC
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
,	,	kIx,	,
ragbyový	ragbyový	k2eAgInSc1d1	ragbyový
klub	klub	k1gInSc1	klub
(	(	kIx(	(
<g/>
web	web	k1gInSc1	web
<g/>
)	)	kIx)	)
AŠK	AŠK	kA	AŠK
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
,	,	kIx,	,
armádní	armádní	k2eAgInSc1d1	armádní
šachový	šachový	k2eAgInSc1d1	šachový
klub	klub	k1gInSc1	klub
HKK	HKK	kA	HKK
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
,	,	kIx,	,
kuželkářský	kuželkářský	k2eAgInSc1d1	kuželkářský
klub	klub	k1gInSc1	klub
(	(	kIx(	(
<g/>
web	web	k1gInSc1	web
<g/>
)	)	kIx)	)
FBS	FBS	kA	FBS
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
,	,	kIx,	,
florbalový	florbalový	k2eAgInSc1d1	florbalový
klub	klub	k1gInSc1	klub
BK	BK	kA	BK
Omega	omega	k1gFnSc1	omega
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
,	,	kIx,	,
badmintonový	badmintonový	k2eAgInSc1d1	badmintonový
klub	klub	k1gInSc1	klub
JK	JK	kA	JK
FILIPPI	FILIPPI	kA	FILIPPI
Olomouc	Olomouc	k1gFnSc1	Olomouc
-	-	kIx~	-
Řepčín	Řepčín	k1gInSc1	Řepčín
<g/>
,	,	kIx,	,
sportovní	sportovní	k2eAgFnSc1d1	sportovní
stáj	stáj	k1gFnSc1	stáj
KSP	KSP	kA	KSP
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
,	,	kIx,	,
klub	klub	k1gInSc1	klub
ploutvového	ploutvový	k2eAgNnSc2d1	ploutvové
plavání	plavání	k1gNnSc2	plavání
(	(	kIx(	(
<g/>
web	web	k1gInSc1	web
<g/>
)	)	kIx)	)
UN	UN	kA	UN
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
,	,	kIx,	,
plavecký	plavecký	k2eAgInSc1d1	plavecký
klub	klub	k1gInSc1	klub
fotbalový	fotbalový	k2eAgInSc4d1	fotbalový
stadion	stadion	k1gInSc4	stadion
Sigmy	Sigma	k1gFnSc2	Sigma
(	(	kIx(	(
<g/>
Andrův	Andrův	k2eAgInSc4d1	Andrův
stadion	stadion	k1gInSc4	stadion
<g/>
)	)	kIx)	)
fotbalový	fotbalový	k2eAgInSc4d1	fotbalový
stadion	stadion	k1gInSc4	stadion
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
HFK	HFK	kA	HFK
Olomouc	Olomouc	k1gFnSc1	Olomouc
zimní	zimní	k2eAgFnSc1d1	zimní
stadion	stadion	k1gInSc4	stadion
plavecký	plavecký	k2eAgInSc4d1	plavecký
stadion	stadion	k1gInSc4	stadion
lanové	lanový	k2eAgNnSc4d1	lanové
centrum	centrum	k1gNnSc4	centrum
sportovní	sportovní	k2eAgFnSc1d1	sportovní
hala	hala	k1gFnSc1	hala
Univerzity	univerzita	k1gFnSc2	univerzita
Palackého	Palackého	k2eAgFnSc1d1	Palackého
omega	omega	k1gFnSc1	omega
centrum	centrum	k1gNnSc4	centrum
sportu	sport	k1gInSc2	sport
a	a	k8xC	a
zdraví	zdraví	k1gNnSc4	zdraví
aqua	aqu	k1gInSc2	aqu
centrum	centrum	k1gNnSc4	centrum
fitness	fitnessa	k1gFnPc2	fitnessa
tribuna	tribun	k1gMnSc2	tribun
Čajkaréna	Čajkarén	k1gMnSc2	Čajkarén
atletický	atletický	k2eAgInSc4d1	atletický
stadion	stadion	k1gInSc4	stadion
(	(	kIx(	(
<g/>
sokolský	sokolský	k2eAgInSc4d1	sokolský
stadion	stadion	k1gInSc4	stadion
<g/>
)	)	kIx)	)
baseballový	baseballový	k2eAgInSc4d1	baseballový
stadión	stadión	k1gInSc4	stadión
golfové	golfový	k2eAgNnSc1d1	golfové
hřiště	hřiště	k1gNnSc1	hřiště
Dům	dům	k1gInSc4	dům
Armády	armáda	k1gFnSc2	armáda
Olomouc	Olomouc	k1gFnSc1	Olomouc
Equine	Equin	k1gInSc5	Equin
sport	sport	k1gInSc4	sport
center	centrum	k1gNnPc2	centrum
Olomouc	Olomouc	k1gFnSc1	Olomouc
(	(	kIx(	(
<g/>
parkurové	parkurový	k2eAgNnSc1d1	parkurové
závodiště	závodiště	k1gNnSc1	závodiště
<g/>
)	)	kIx)	)
Kuželna	kuželna	k1gFnSc1	kuželna
HKK	HKK	kA	HKK
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
Největší	veliký	k2eAgFnPc4d3	veliký
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
)	)	kIx)	)
Bowland	Bowland	k1gInSc1	Bowland
bowling	bowling	k1gInSc1	bowling
center	centrum	k1gNnPc2	centrum
Šantovka	Šantovka	k1gFnSc1	Šantovka
(	(	kIx(	(
<g/>
Největší	veliký	k2eAgFnSc1d3	veliký
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
)	)	kIx)	)
Olomoucký	olomoucký	k2eAgInSc1d1	olomoucký
půlmaraton	půlmaraton	k1gInSc1	půlmaraton
ITS	ITS	kA	ITS
Cup	cup	k1gInSc1	cup
Mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
florbale	florbal	k1gInSc5	florbal
juniorek	juniorka	k1gFnPc2	juniorka
2010	[number]	k4	2010
Mistrovství	mistrovství	k1gNnPc2	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
orientačním	orientační	k2eAgInSc6d1	orientační
běhu	běh	k1gInSc6	běh
2008	[number]	k4	2008
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
kickboxu	kickbox	k1gInSc6	kickbox
2008	[number]	k4	2008
Mistrovství	mistrovství	k1gNnSc4	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
v	v	k7c6	v
baseballu	baseball	k1gInSc6	baseball
2005	[number]	k4	2005
Hry	hra	k1gFnSc2	hra
V.	V.	kA	V.
letní	letní	k2eAgFnSc2d1	letní
olympiády	olympiáda	k1gFnSc2	olympiáda
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
mládeže	mládež	k1gFnSc2	mládež
2011	[number]	k4	2011
Mistrovství	mistrovství	k1gNnSc2	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
hráčů	hráč	k1gMnPc2	hráč
do	do	k7c2	do
21	[number]	k4	21
let	léto	k1gNnPc2	léto
2015	[number]	k4	2015
V	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2002	[number]	k4	2002
bylo	být	k5eAaImAgNnS	být
celkem	celek	k1gInSc7	celek
1	[number]	k4	1
084	[number]	k4	084
lékařů	lékař	k1gMnPc2	lékař
<g/>
,	,	kIx,	,
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
tak	tak	k9	tak
na	na	k7c4	na
1	[number]	k4	1
lékaře	lékař	k1gMnSc4	lékař
činil	činit	k5eAaImAgMnS	činit
96	[number]	k4	96
a	a	k8xC	a
počet	počet	k1gInSc1	počet
lůžek	lůžko	k1gNnPc2	lůžko
ve	v	k7c6	v
zdravotnictví	zdravotnictví	k1gNnSc6	zdravotnictví
na	na	k7c4	na
1	[number]	k4	1
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
bylo	být	k5eAaImAgNnS	být
19	[number]	k4	19
<g/>
.	.	kIx.	.
</s>
<s>
Působí	působit	k5eAaImIp3nP	působit
zde	zde	k6eAd1	zde
tři	tři	k4xCgFnPc4	tři
nemocnice	nemocnice	k1gFnPc4	nemocnice
<g/>
,	,	kIx,	,
147	[number]	k4	147
ordinací	ordinace	k1gFnPc2	ordinace
praktického	praktický	k2eAgMnSc2d1	praktický
lékaře	lékař	k1gMnSc2	lékař
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
197	[number]	k4	197
odborných	odborný	k2eAgFnPc2d1	odborná
ordinací	ordinace	k1gFnPc2	ordinace
a	a	k8xC	a
pracovišť	pracoviště	k1gNnPc2	pracoviště
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
21	[number]	k4	21
lékáren	lékárna	k1gFnPc2	lékárna
a	a	k8xC	a
11	[number]	k4	11
ostatních	ostatní	k2eAgNnPc2d1	ostatní
zdravotnických	zdravotnický	k2eAgNnPc2d1	zdravotnické
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
tak	tak	k9	tak
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2002	[number]	k4	2002
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
379	[number]	k4	379
zdravotnických	zdravotnický	k2eAgNnPc2d1	zdravotnické
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgFnSc7d3	nejvýznamnější
institucí	instituce	k1gFnSc7	instituce
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
zdravotnictví	zdravotnictví	k1gNnSc2	zdravotnictví
je	být	k5eAaImIp3nS	být
Fakultní	fakultní	k2eAgFnSc1d1	fakultní
nemocnice	nemocnice	k1gFnSc1	nemocnice
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
osoby	osoba	k1gFnPc4	osoba
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
ve	v	k7c6	v
městě	město	k1gNnSc6	město
narodily	narodit	k5eAaPmAgFnP	narodit
<g/>
,	,	kIx,	,
zemřely	zemřít	k5eAaPmAgFnP	zemřít
anebo	anebo	k8xC	anebo
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
nějakou	nějaký	k3yIgFnSc4	nějaký
dobu	doba	k1gFnSc4	doba
pobývaly	pobývat	k5eAaImAgInP	pobývat
<g/>
.	.	kIx.	.
</s>
<s>
Osobnosti	osobnost	k1gFnPc1	osobnost
jsou	být	k5eAaImIp3nP	být
řazeny	řadit	k5eAaImNgFnP	řadit
podle	podle	k7c2	podle
roku	rok	k1gInSc2	rok
narození	narození	k1gNnSc2	narození
<g/>
.	.	kIx.	.
svatá	svatý	k2eAgFnSc1d1	svatá
Pavlína	Pavlína	k1gFnSc1	Pavlína
Římská	římský	k2eAgFnSc1d1	římská
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
patronkou	patronka	k1gFnSc7	patronka
města	město	k1gNnSc2	město
Břetislav	Břetislav	k1gMnSc1	Břetislav
I.	I.	kA	I.
(	(	kIx(	(
<g/>
1002	[number]	k4	1002
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
1055	[number]	k4	1055
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
kníže	kníže	k1gMnSc1	kníže
Jitka	Jitka	k1gFnSc1	Jitka
ze	z	k7c2	z
Schweinfurtu	Schweinfurt	k1gInSc2	Schweinfurt
(	(	kIx(	(
<g/>
1003	[number]	k4	1003
<g/>
–	–	k?	–
<g/>
1058	[number]	k4	1058
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
kněžna	kněžna	k1gFnSc1	kněžna
Spytihněv	Spytihněv	k1gFnSc2	Spytihněv
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1031	[number]	k4	1031
<g/>
–	–	k?	–
<g/>
1061	[number]	k4	1061
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
kníže	kníže	k1gMnSc1	kníže
Jan	Jan	k1gMnSc1	Jan
I.	I.	kA	I.
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
–	–	k?	–
<g/>
1085	[number]	k4	1085
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
historicky	historicky	k6eAd1	historicky
první	první	k4xOgInSc1	první
doložený	doložený	k2eAgInSc1d1	doložený
biskup	biskup	k1gInSc1	biskup
olomoucký	olomoucký	k2eAgInSc1d1	olomoucký
Ludmila	Ludmila	k1gFnSc1	Ludmila
Přemyslovna	Přemyslovna	k1gFnSc1	Přemyslovna
(	(	kIx(	(
<g/>
1110	[number]	k4	1110
<g/>
–	–	k?	–
<g/>
1240	[number]	k4	1240
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bavorská	bavorský	k2eAgFnSc1d1	bavorská
vévodkyně	vévodkyně	k1gFnSc1	vévodkyně
Václav	Václav	k1gMnSc1	Václav
III	III	kA	III
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1289	[number]	k4	1289
<g/>
–	–	k?	–
<g/>
1306	[number]	k4	1306
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
<g/>
,	,	kIx,	,
polský	polský	k2eAgMnSc1d1	polský
a	a	k8xC	a
uherský	uherský	k2eAgMnSc1d1	uherský
král	král	k1gMnSc1	král
Jan	Jan	k1gMnSc1	Jan
XII	XII	kA	XII
<g/>
.	.	kIx.	.
</s>
<s>
Železný	Železný	k1gMnSc1	Železný
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
–	–	k?	–
<g/>
1430	[number]	k4	1430
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kardinál	kardinál	k1gMnSc1	kardinál
<g/>
,	,	kIx,	,
biskup	biskup	k1gMnSc1	biskup
litomyšlský	litomyšlský	k2eAgMnSc1d1	litomyšlský
<g/>
,	,	kIx,	,
biskup	biskup	k1gMnSc1	biskup
olomoucký	olomoucký	k2eAgMnSc1d1	olomoucký
Heinrich	Heinrich	k1gMnSc1	Heinrich
Kramer	Kramer	k1gMnSc1	Kramer
(	(	kIx(	(
<g/>
1430	[number]	k4	1430
<g/>
–	–	k?	–
<g/>
1505	[number]	k4	1505
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dominikán	dominikán	k1gMnSc1	dominikán
a	a	k8xC	a
inkvizitor	inkvizitor	k1gMnSc1	inkvizitor
Matyáš	Matyáš	k1gMnSc1	Matyáš
Korvín	Korvín	k1gMnSc1	Korvín
(	(	kIx(	(
<g/>
1443	[number]	k4	1443
<g/>
–	–	k?	–
<g/>
1490	[number]	k4	1490
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
uherský	uherský	k2eAgMnSc1d1	uherský
král	král	k1gMnSc1	král
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
český	český	k2eAgMnSc1d1	český
protikrál	protikrál	k1gMnSc1	protikrál
Hurtado	Hurtada	k1gFnSc5	Hurtada
Pérez	Pérez	k1gMnSc1	Pérez
(	(	kIx(	(
<g/>
1526	[number]	k4	1526
<g/>
–	–	k?	–
<g/>
1594	[number]	k4	1594
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
rektor	rektor	k1gMnSc1	rektor
olomoucké	olomoucký	k2eAgFnSc2d1	olomoucká
univerzity	univerzita	k1gFnSc2	univerzita
Zbyněk	Zbyněk	k1gMnSc1	Zbyněk
Berka	Berka	k1gMnSc1	Berka
z	z	k7c2	z
Dubé	Dubá	k1gFnSc2	Dubá
a	a	k8xC	a
Lipé	Lipá	k1gFnSc2	Lipá
(	(	kIx(	(
<g/>
1551	[number]	k4	1551
<g/>
–	–	k?	–
<g/>
1606	[number]	k4	1606
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kardinál	kardinál	k1gMnSc1	kardinál
<g/>
,	,	kIx,	,
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
pražský	pražský	k2eAgMnSc1d1	pražský
<g/>
,	,	kIx,	,
probošt	probošt	k1gMnSc1	probošt
olomoucké	olomoucký	k2eAgFnSc2d1	olomoucká
kapituly	kapitula	k1gFnSc2	kapitula
Georg	Georg	k1gMnSc1	Georg
Flegel	Flegel	k1gMnSc1	Flegel
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1566	[number]	k4	1566
<g/>
–	–	k?	–
<g/>
1638	[number]	k4	1638
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
barokní	barokní	k2eAgMnSc1d1	barokní
malíř	malíř	k1gMnSc1	malíř
specializovaný	specializovaný	k2eAgInSc4d1	specializovaný
na	na	k7c4	na
zátiší	zátiší	k1gNnSc4	zátiší
František	Františka	k1gFnPc2	Františka
z	z	k7c2	z
Ditrichštejna	Ditrichštejn	k1gInSc2	Ditrichštejn
(	(	kIx(	(
<g/>
1570	[number]	k4	1570
<g/>
–	–	k?	–
<g/>
1636	[number]	k4	1636
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kardinál	kardinál	k1gMnSc1	kardinál
<g/>
,	,	kIx,	,
biskup	biskup	k1gMnSc1	biskup
olomoucký	olomoucký	k2eAgMnSc1d1	olomoucký
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
moravských	moravský	k2eAgMnPc2d1	moravský
politiků	politik	k1gMnPc2	politik
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Sv.	sv.	kA	sv.
Jan	Jan	k1gMnSc1	Jan
Sarkander	Sarkander	k1gMnSc1	Sarkander
(	(	kIx(	(
<g/>
1576	[number]	k4	1576
<g />
.	.	kIx.	.
</s>
<s>
<g/>
–	–	k?	–
<g/>
1620	[number]	k4	1620
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kněz	kněz	k1gMnSc1	kněz
<g/>
,	,	kIx,	,
mučedník	mučedník	k1gMnSc1	mučedník
Svatý	svatý	k2eAgMnSc1d1	svatý
John	John	k1gMnSc1	John
Ogilvie	Ogilvie	k1gFnSc2	Ogilvie
(	(	kIx(	(
<g/>
1579	[number]	k4	1579
<g/>
–	–	k?	–
<g/>
1615	[number]	k4	1615
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jezuita	jezuita	k1gMnSc1	jezuita
<g/>
,	,	kIx,	,
mučedník	mučedník	k1gMnSc1	mučedník
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Balbín	Balbín	k1gMnSc1	Balbín
(	(	kIx(	(
<g/>
1621	[number]	k4	1621
<g/>
–	–	k?	–
<g/>
1688	[number]	k4	1688
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
literát	literát	k1gMnSc1	literát
<g/>
,	,	kIx,	,
historik	historik	k1gMnSc1	historik
<g/>
,	,	kIx,	,
kněz	kněz	k1gMnSc1	kněz
<g/>
,	,	kIx,	,
zeměpisec	zeměpisec	k1gMnSc1	zeměpisec
a	a	k8xC	a
pedagog	pedagog	k1gMnSc1	pedagog
Karel	Karel	k1gMnSc1	Karel
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Irmler	Irmler	k1gMnSc1	Irmler
(	(	kIx(	(
<g/>
1650	[number]	k4	1650
<g/>
–	–	k?	–
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
práva	právo	k1gNnSc2	právo
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Hannibal	Hannibal	k1gInSc4	Hannibal
von	von	k1gInSc1	von
Schrattenbach	Schrattenbach	k1gInSc1	Schrattenbach
(	(	kIx(	(
<g/>
1660	[number]	k4	1660
<g/>
–	–	k?	–
<g/>
1738	[number]	k4	1738
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kardinál	kardinál	k1gMnSc1	kardinál
<g/>
,	,	kIx,	,
biskup	biskup	k1gMnSc1	biskup
olomoucký	olomoucký	k2eAgMnSc1d1	olomoucký
<g/>
,	,	kIx,	,
místokrál	místokrál	k1gMnSc1	místokrál
neapolský	neapolský	k2eAgMnSc1d1	neapolský
Václav	Václav	k1gMnSc1	Václav
Render	Render	k1gMnSc1	Render
(	(	kIx(	(
<g/>
1669	[number]	k4	1669
<g/>
–	–	k?	–
<g/>
1733	[number]	k4	1733
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
významný	významný	k2eAgMnSc1d1	významný
kameník	kameník	k1gMnSc1	kameník
a	a	k8xC	a
delineator	delineator	k1gMnSc1	delineator
Jan	Jan	k1gMnSc1	Jan
Kryštof	Kryštof	k1gMnSc1	Kryštof
Handke	Handk	k1gFnSc2	Handk
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
1694	[number]	k4	1694
<g/>
–	–	k?	–
<g/>
1774	[number]	k4	1774
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
významný	významný	k2eAgMnSc1d1	významný
moravský	moravský	k2eAgMnSc1d1	moravský
malíř	malíř	k1gMnSc1	malíř
Pierre	Pierr	k1gInSc5	Pierr
Philippe	Philipp	k1gInSc5	Philipp
Bechade	Bechad	k1gInSc5	Bechad
de	de	k?	de
Rochepin	Rochepin	k1gInSc1	Rochepin
(	(	kIx(	(
<g/>
1694	[number]	k4	1694
<g/>
–	–	k?	–
<g/>
1776	[number]	k4	1776
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
projektant	projektant	k1gMnSc1	projektant
a	a	k8xC	a
stavitel	stavitel	k1gMnSc1	stavitel
olomoucké	olomoucký	k2eAgFnSc2d1	olomoucká
bastionové	bastionový	k2eAgFnSc2d1	bastionová
pevnosti	pevnost	k1gFnSc2	pevnost
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Julius	Julius	k1gMnSc1	Julius
Troyer	Troyer	k1gMnSc1	Troyer
z	z	k7c2	z
Troyersteinu	Troyerstein	k1gInSc2	Troyerstein
(	(	kIx(	(
<g/>
1698	[number]	k4	1698
<g/>
–	–	k?	–
<g/>
1758	[number]	k4	1758
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
kardinál	kardinál	k1gMnSc1	kardinál
<g/>
,	,	kIx,	,
biskup	biskup	k1gMnSc1	biskup
olomoucký	olomoucký	k2eAgMnSc1d1	olomoucký
Václav	Václav	k1gMnSc1	Václav
Matyáš	Matyáš	k1gMnSc1	Matyáš
Gurecký	Gurecký	k2eAgMnSc1d1	Gurecký
(	(	kIx(	(
<g/>
1705	[number]	k4	1705
<g/>
–	–	k?	–
<g/>
1743	[number]	k4	1743
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
,	,	kIx,	,
kapelník	kapelník	k1gMnSc1	kapelník
v	v	k7c6	v
katedrále	katedrála	k1gFnSc6	katedrála
sv.	sv.	kA	sv.
Václava	Václav	k1gMnSc2	Václav
Antonín	Antonín	k1gMnSc1	Antonín
Theodor	Theodor	k1gMnSc1	Theodor
Colloredo-Waldsee	Colloredo-Waldsee	k1gFnSc1	Colloredo-Waldsee
(	(	kIx(	(
<g/>
1729	[number]	k4	1729
<g/>
–	–	k?	–
<g/>
1811	[number]	k4	1811
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kardinál	kardinál	k1gMnSc1	kardinál
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
olomoucký	olomoucký	k2eAgMnSc1d1	olomoucký
<g />
.	.	kIx.	.
</s>
<s>
Gilbert	Gilbert	k1gMnSc1	Gilbert
du	du	k?	du
Motier	Motier	k1gMnSc1	Motier
<g/>
,	,	kIx,	,
markýz	markýz	k1gMnSc1	markýz
de	de	k?	de
La	la	k1gNnPc2	la
Fayette	Fayett	k1gInSc5	Fayett
(	(	kIx(	(
<g/>
1757	[number]	k4	1757
<g/>
–	–	k?	–
<g/>
1834	[number]	k4	1834
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
generál	generál	k1gMnSc1	generál
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
Jan	Jan	k1gMnSc1	Jan
Radecký	Radecký	k1gMnSc1	Radecký
z	z	k7c2	z
Radče	Radč	k1gFnSc2	Radč
(	(	kIx(	(
<g/>
1766	[number]	k4	1766
<g/>
–	–	k?	–
<g/>
1858	[number]	k4	1858
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
šlechtic	šlechtic	k1gMnSc1	šlechtic
a	a	k8xC	a
rakouský	rakouský	k2eAgMnSc1d1	rakouský
maršál	maršál	k1gMnSc1	maršál
Maria	Mario	k1gMnSc2	Mario
Tadeáš	Tadeáš	k1gMnSc1	Tadeáš
Trauttmansdorff	Trauttmansdorff	k1gMnSc1	Trauttmansdorff
(	(	kIx(	(
<g/>
1761	[number]	k4	1761
<g />
.	.	kIx.	.
</s>
<s>
<g/>
–	–	k?	–
<g/>
1819	[number]	k4	1819
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kardinál	kardinál	k1gMnSc1	kardinál
<g/>
,	,	kIx,	,
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
olomoucký	olomoucký	k2eAgMnSc1d1	olomoucký
<g/>
,	,	kIx,	,
biskup	biskup	k1gMnSc1	biskup
královéhradecký	královéhradecký	k2eAgMnSc1d1	královéhradecký
Maxmilián	Maxmilián	k1gMnSc1	Maxmilián
Josef	Josef	k1gMnSc1	Josef
Sommerau-Beckh	Sommerau-Beckh	k1gMnSc1	Sommerau-Beckh
(	(	kIx(	(
<g/>
1769	[number]	k4	1769
<g/>
–	–	k?	–
<g/>
1853	[number]	k4	1853
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kardinál	kardinál	k1gMnSc1	kardinál
<g/>
,	,	kIx,	,
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
olomoucký	olomoucký	k2eAgMnSc1d1	olomoucký
Rudolf	Rudolf	k1gMnSc1	Rudolf
Jan	Jan	k1gMnSc1	Jan
kardinál	kardinál	k1gMnSc1	kardinál
Habsbursko-Lotrinský	habsburskootrinský	k2eAgMnSc1d1	habsbursko-lotrinský
(	(	kIx(	(
<g/>
1788	[number]	k4	1788
<g/>
–	–	k?	–
<g/>
1831	[number]	k4	1831
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
kardinál	kardinál	k1gMnSc1	kardinál
<g/>
,	,	kIx,	,
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
olomoucký	olomoucký	k2eAgMnSc1d1	olomoucký
<g/>
,	,	kIx,	,
rakouský	rakouský	k2eAgMnSc1d1	rakouský
arcivévoda	arcivévoda	k1gMnSc1	arcivévoda
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
I.	I.	kA	I.
Dobrotivý	dobrotivý	k2eAgMnSc1d1	dobrotivý
(	(	kIx(	(
<g/>
1793	[number]	k4	1793
<g/>
–	–	k?	–
<g/>
1875	[number]	k4	1875
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rakouský	rakouský	k2eAgMnSc1d1	rakouský
císař	císař	k1gMnSc1	císař
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
Alois	Alois	k1gMnSc1	Alois
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Šembera	Šembera	k1gMnSc1	Šembera
(	(	kIx(	(
<g/>
1807	[number]	k4	1807
<g/>
–	–	k?	–
<g/>
1882	[number]	k4	1882
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jazykovědec	jazykovědec	k1gMnSc1	jazykovědec
<g/>
,	,	kIx,	,
literární	literární	k2eAgMnSc1d1	literární
historik	historik	k1gMnSc1	historik
Bedřich	Bedřich	k1gMnSc1	Bedřich
<g />
.	.	kIx.	.
</s>
<s>
z	z	k7c2	z
Fürstenberka	Fürstenberka	k1gFnSc1	Fürstenberka
(	(	kIx(	(
<g/>
1813	[number]	k4	1813
<g/>
–	–	k?	–
<g/>
1892	[number]	k4	1892
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kardinál	kardinál	k1gMnSc1	kardinál
<g/>
,	,	kIx,	,
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
olomoucký	olomoucký	k2eAgMnSc1d1	olomoucký
Rudolf	Rudolf	k1gMnSc1	Rudolf
Eitelberger	Eitelbergra	k1gFnPc2	Eitelbergra
von	von	k1gInSc1	von
Edelberg	Edelberg	k1gMnSc1	Edelberg
(	(	kIx(	(
<g/>
1817	[number]	k4	1817
<g/>
–	–	k?	–
<g/>
1885	[number]	k4	1885
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
historik	historik	k1gMnSc1	historik
umění	umění	k1gNnSc2	umění
Pavel	Pavel	k1gMnSc1	Pavel
Křížkovský	Křížkovský	k2eAgMnSc1d1	Křížkovský
(	(	kIx(	(
<g/>
1820	[number]	k4	1820
<g/>
–	–	k?	–
<g/>
1885	[number]	k4	1885
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
<g />
.	.	kIx.	.
</s>
<s>
Jindřich	Jindřich	k1gMnSc1	Jindřich
Wankel	Wankel	k1gMnSc1	Wankel
(	(	kIx(	(
<g/>
1821	[number]	k4	1821
<g/>
–	–	k?	–
<g/>
1897	[number]	k4	1897
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lékař	lékař	k1gMnSc1	lékař
<g/>
,	,	kIx,	,
archeolog	archeolog	k1gMnSc1	archeolog
<g/>
,	,	kIx,	,
speleolog	speleolog	k1gMnSc1	speleolog
<g/>
,	,	kIx,	,
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
bydlel	bydlet	k5eAaImAgMnS	bydlet
1883	[number]	k4	1883
<g/>
–	–	k?	–
<g/>
1897	[number]	k4	1897
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
pochován	pochován	k2eAgMnSc1d1	pochován
Gregor	Gregor	k1gMnSc1	Gregor
Mendel	Mendel	k1gMnSc1	Mendel
(	(	kIx(	(
<g/>
1822	[number]	k4	1822
<g/>
–	–	k?	–
<g/>
1884	[number]	k4	1884
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přírodovědec	přírodovědec	k1gMnSc1	přírodovědec
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
zakladatel	zakladatel	k1gMnSc1	zakladatel
genetiky	genetika	k1gFnSc2	genetika
Ignát	Ignát	k1gMnSc1	Ignát
Wurm	Wurm	k1gMnSc1	Wurm
(	(	kIx(	(
<g/>
1825	[number]	k4	1825
<g/>
–	–	k?	–
<g/>
1911	[number]	k4	1911
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kněz	kněz	k1gMnSc1	kněz
<g/>
,	,	kIx,	,
redaktor	redaktor	k1gMnSc1	redaktor
<g/>
,	,	kIx,	,
etnograf	etnograf	k1gMnSc1	etnograf
Konrad	Konrada	k1gFnPc2	Konrada
Bayer	Bayer	k1gMnSc1	Bayer
(	(	kIx(	(
<g/>
1828	[number]	k4	1828
<g/>
–	–	k?	–
<g/>
1897	[number]	k4	1897
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
šachový	šachový	k2eAgMnSc1d1	šachový
skladatel	skladatel	k1gMnSc1	skladatel
František	František	k1gMnSc1	František
Josef	Josef	k1gMnSc1	Josef
I.	I.	kA	I.
(	(	kIx(	(
<g/>
1830	[number]	k4	1830
<g/>
–	–	k?	–
<g/>
1916	[number]	k4	1916
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
rakouský	rakouský	k2eAgMnSc1d1	rakouský
císař	císař	k1gMnSc1	císař
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
František	František	k1gMnSc1	František
Saleský	Saleský	k2eAgMnSc1d1	Saleský
Bauer	Bauer	k1gMnSc1	Bauer
(	(	kIx(	(
<g/>
1841	[number]	k4	1841
<g/>
–	–	k?	–
<g/>
1915	[number]	k4	1915
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kardinál	kardinál	k1gMnSc1	kardinál
<g/>
,	,	kIx,	,
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
olomoucký	olomoucký	k2eAgMnSc1d1	olomoucký
<g/>
,	,	kIx,	,
biskup	biskup	k1gMnSc1	biskup
brněnský	brněnský	k2eAgMnSc1d1	brněnský
Josef	Josef	k1gMnSc1	Josef
Nešvera	Nešvero	k1gNnSc2	Nešvero
(	(	kIx(	(
<g/>
1842	[number]	k4	1842
<g/>
–	–	k?	–
<g/>
1914	[number]	k4	1914
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
František	František	k1gMnSc1	František
Polívka	Polívka	k1gMnSc1	Polívka
(	(	kIx(	(
<g/>
1860	[number]	k4	1860
<g />
.	.	kIx.	.
</s>
<s>
<g/>
–	–	k?	–
<g/>
1923	[number]	k4	1923
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
botanik	botanik	k1gMnSc1	botanik
Gustav	Gustav	k1gMnSc1	Gustav
Mahler	Mahler	k1gMnSc1	Mahler
(	(	kIx(	(
<g/>
1860	[number]	k4	1860
<g/>
–	–	k?	–
<g/>
1911	[number]	k4	1911
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
Mořic	Mořic	k1gMnSc1	Mořic
Hruban	Hruban	k1gMnSc1	Hruban
(	(	kIx(	(
<g/>
1862	[number]	k4	1862
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
právník	právník	k1gMnSc1	právník
Lev	Lev	k1gMnSc1	Lev
Skrbenský	Skrbenský	k2eAgMnSc1d1	Skrbenský
z	z	k7c2	z
Hříště	hříště	k1gNnSc2	hříště
(	(	kIx(	(
<g/>
1863	[number]	k4	1863
<g/>
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kardinál	kardinál	k1gMnSc1	kardinál
<g/>
,	,	kIx,	,
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
pražský	pražský	k2eAgMnSc1d1	pražský
(	(	kIx(	(
<g/>
1899	[number]	k4	1899
<g/>
–	–	k?	–
<g/>
1916	[number]	k4	1916
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
arbibiskup	arbibiskup	k1gMnSc1	arbibiskup
olomoucký	olomoucký	k2eAgMnSc1d1	olomoucký
(	(	kIx(	(
<g/>
1916	[number]	k4	1916
<g/>
–	–	k?	–
<g/>
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
Eduard	Eduard	k1gMnSc1	Eduard
Konrád	Konrád	k1gMnSc1	Konrád
Zirm	Zirm	k1gMnSc1	Zirm
(	(	kIx(	(
<g/>
1863	[number]	k4	1863
<g/>
–	–	k?	–
<g/>
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lékař	lékař	k1gMnSc1	lékař
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
oftalmolog	oftalmolog	k1gMnSc1	oftalmolog
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
provedl	provést	k5eAaPmAgMnS	provést
první	první	k4xOgFnSc4	první
úspěšnou	úspěšný	k2eAgFnSc4d1	úspěšná
transplantaci	transplantace	k1gFnSc4	transplantace
na	na	k7c6	na
světě	svět	k1gInSc6	svět
Vavro	Vavro	k1gNnSc1	Vavro
Šrobár	Šrobár	k1gInSc1	Šrobár
(	(	kIx(	(
<g/>
1867	[number]	k4	1867
<g/>
–	–	k?	–
<g/>
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
slovenský	slovenský	k2eAgMnSc1d1	slovenský
lékař	lékař	k1gMnSc1	lékař
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
Petr	Petr	k1gMnSc1	Petr
Bezruč	Bezruč	k1gMnSc1	Bezruč
(	(	kIx(	(
<g/>
1867	[number]	k4	1867
<g/>
–	–	k?	–
<g/>
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
Leo	Leo	k1gMnSc1	Leo
Fall	Fall	k1gMnSc1	Fall
(	(	kIx(	(
<g/>
1873	[number]	k4	1873
<g/>
–	–	k?	–
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1925	[number]	k4	1925
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
operetní	operetní	k2eAgMnSc1d1	operetní
skladatel	skladatel	k1gMnSc1	skladatel
Bohumír	Bohumír	k1gMnSc1	Bohumír
Cigánek	cigánka	k1gFnPc2	cigánka
(	(	kIx(	(
<g/>
1874	[number]	k4	1874
<g/>
–	–	k?	–
<g/>
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
duchovní	duchovní	k1gMnSc1	duchovní
a	a	k8xC	a
biskup	biskup	k1gMnSc1	biskup
Církve	církev	k1gFnSc2	církev
československé	československý	k2eAgFnSc2d1	Československá
husitské	husitský	k2eAgFnSc2d1	husitská
Karel	Karel	k1gMnSc1	Karel
Wellner	Wellner	k1gInSc1	Wellner
(	(	kIx(	(
<g/>
1875	[number]	k4	1875
<g/>
–	–	k?	–
<g/>
1926	[number]	k4	1926
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
grafik	grafik	k1gMnSc1	grafik
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
kreslíř	kreslíř	k1gMnSc1	kreslíř
<g/>
,	,	kIx,	,
ilustrátor	ilustrátor	k1gMnSc1	ilustrátor
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
výtvarný	výtvarný	k2eAgMnSc1d1	výtvarný
historik	historik	k1gMnSc1	historik
a	a	k8xC	a
kritik	kritik	k1gMnSc1	kritik
Adolf	Adolf	k1gMnSc1	Adolf
Kašpar	Kašpar	k1gMnSc1	Kašpar
(	(	kIx(	(
<g/>
1877	[number]	k4	1877
<g/>
–	–	k?	–
<g/>
1934	[number]	k4	1934
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
a	a	k8xC	a
ilustrátor	ilustrátor	k1gMnSc1	ilustrátor
Svatý	svatý	k2eAgMnSc1d1	svatý
Gorazd	Gorazd	k1gMnSc1	Gorazd
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1879	[number]	k4	1879
<g/>
–	–	k?	–
<g/>
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kněz	kněz	k1gMnSc1	kněz
<g/>
,	,	kIx,	,
mučedník	mučedník	k1gMnSc1	mučedník
Jaromír	Jaromír	k1gMnSc1	Jaromír
John	John	k1gMnSc1	John
(	(	kIx(	(
<g/>
1882	[number]	k4	1882
<g/>
–	–	k?	–
<g/>
1952	[number]	k4	1952
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
novinář	novinář	k1gMnSc1	novinář
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Durych	Durych	k1gMnSc1	Durych
(	(	kIx(	(
<g/>
1886	[number]	k4	1886
<g/>
–	–	k?	–
<g/>
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vojenský	vojenský	k2eAgMnSc1d1	vojenský
lékař	lékař	k1gMnSc1	lékař
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
Josef	Josef	k1gMnSc1	Josef
Ander	Ander	k1gMnSc1	Ander
(	(	kIx(	(
<g/>
1888	[number]	k4	1888
<g/>
–	–	k?	–
<g/>
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podnikatel	podnikatel	k1gMnSc1	podnikatel
Otokar	Otokar	k1gMnSc1	Otokar
Fierlinger	Fierlinger	k1gMnSc1	Fierlinger
(	(	kIx(	(
<g/>
1888	[number]	k4	1888
<g/>
–	–	k?	–
<g/>
1941	[number]	k4	1941
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
architekt	architekt	k1gMnSc1	architekt
Jarmila	Jarmila	k1gFnSc1	Jarmila
Kurandová	Kurandový	k2eAgFnSc1d1	Kurandová
(	(	kIx(	(
<g/>
1890	[number]	k4	1890
<g/>
–	–	k?	–
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
Paul	Paul	k1gMnSc1	Paul
Engelmann	Engelmann	k1gMnSc1	Engelmann
(	(	kIx(	(
<g/>
1891	[number]	k4	1891
<g/>
–	–	k?	–
<g/>
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
architekt	architekt	k1gMnSc1	architekt
<g/>
,	,	kIx,	,
filozof	filozof	k1gMnSc1	filozof
Hubert	Hubert	k1gMnSc1	Hubert
Aust	Aust	k1gMnSc1	Aust
(	(	kIx(	(
<g/>
1891	[number]	k4	1891
<g/>
–	–	k?	–
<g/>
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
akademický	akademický	k2eAgMnSc1d1	akademický
architekt	architekt	k1gMnSc1	architekt
<g />
.	.	kIx.	.
</s>
<s>
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Fierlinger	Fierlinger	k1gMnSc1	Fierlinger
(	(	kIx(	(
<g/>
1891	[number]	k4	1891
<g/>
–	–	k?	–
<g/>
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
Jacques	Jacques	k1gMnSc1	Jacques
Groag	Groag	k1gMnSc1	Groag
(	(	kIx(	(
<g/>
1892	[number]	k4	1892
<g/>
–	–	k?	–
<g/>
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
architekt	architekt	k1gMnSc1	architekt
a	a	k8xC	a
interiérový	interiérový	k2eAgMnSc1d1	interiérový
návrhář	návrhář	k1gMnSc1	návrhář
Stanislav	Stanislav	k1gMnSc1	Stanislav
Zela	zet	k5eAaImAgFnS	zet
(	(	kIx(	(
<g/>
1893	[number]	k4	1893
<g/>
–	–	k?	–
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pomocný	pomocný	k2eAgMnSc1d1	pomocný
biskup	biskup	k1gMnSc1	biskup
olomoucký	olomoucký	k2eAgMnSc1d1	olomoucký
<g/>
,	,	kIx,	,
kapitulní	kapitulní	k2eAgMnSc1d1	kapitulní
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
1947	[number]	k4	1947
<g/>
–	–	k?	–
<g/>
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
a	a	k8xC	a
generální	generální	k2eAgMnSc1d1	generální
vikář	vikář	k1gMnSc1	vikář
olomoucké	olomoucký	k2eAgFnSc2d1	olomoucká
arcidiecéze	arcidiecéze	k1gFnSc2	arcidiecéze
(	(	kIx(	(
<g/>
1948	[number]	k4	1948
<g/>
–	–	k?	–
<g/>
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
Josef	Josef	k1gMnSc1	Josef
Ludvík	Ludvík	k1gMnSc1	Ludvík
Fischer	Fischer	k1gMnSc1	Fischer
(	(	kIx(	(
<g/>
1894	[number]	k4	1894
<g/>
–	–	k?	–
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
významný	významný	k2eAgMnSc1d1	významný
český	český	k2eAgMnSc1d1	český
filozof	filozof	k1gMnSc1	filozof
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
rektor	rektor	k1gMnSc1	rektor
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
obnovené	obnovený	k2eAgFnSc2d1	obnovená
olomoucké	olomoucký	k2eAgFnSc2d1	olomoucká
univerzity	univerzita	k1gFnSc2	univerzita
Josef	Josef	k1gMnSc1	Josef
<g />
.	.	kIx.	.
</s>
<s>
Rostislav	Rostislav	k1gMnSc1	Rostislav
Stejskal	Stejskal	k1gMnSc1	Stejskal
(	(	kIx(	(
<g/>
1894	[number]	k4	1894
<g/>
–	–	k?	–
<g/>
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
teolog	teolog	k1gMnSc1	teolog
<g/>
,	,	kIx,	,
duchovní	duchovní	k1gMnSc1	duchovní
a	a	k8xC	a
biskup	biskup	k1gMnSc1	biskup
Církve	církev	k1gFnSc2	církev
československé	československý	k2eAgFnSc2d1	Československá
husitské	husitský	k2eAgFnSc2d1	husitská
Karel	Karel	k1gMnSc1	Karel
Svolinský	Svolinský	k2eAgMnSc1d1	Svolinský
(	(	kIx(	(
<g/>
1896	[number]	k4	1896
<g/>
–	–	k?	–
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
grafik	grafik	k1gMnSc1	grafik
a	a	k8xC	a
ilustrátor	ilustrátor	k1gMnSc1	ilustrátor
Bedřich	Bedřich	k1gMnSc1	Bedřich
Václavek	Václavek	k1gMnSc1	Václavek
(	(	kIx(	(
<g/>
1897	[number]	k4	1897
<g/>
–	–	k?	–
<g/>
1943	[number]	k4	1943
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
estetik	estetik	k1gMnSc1	estetik
<g/>
,	,	kIx,	,
literární	literární	k2eAgMnSc1d1	literární
teoretik	teoretik	k1gMnSc1	teoretik
a	a	k8xC	a
kritik	kritik	k1gMnSc1	kritik
Eliška	Eliška	k1gFnSc1	Eliška
Junková	Junková	k1gFnSc1	Junková
(	(	kIx(	(
<g/>
1900	[number]	k4	1900
<g/>
–	–	k?	–
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
automobilová	automobilový	k2eAgFnSc1d1	automobilová
závodnice	závodnice	k1gFnSc1	závodnice
Franz	Franz	k1gMnSc1	Franz
Karmasin	Karmasin	k1gMnSc1	Karmasin
(	(	kIx(	(
<g/>
1901	[number]	k4	1901
<g/>
–	–	k?	–
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nacistický	nacistický	k2eAgMnSc1d1	nacistický
politik	politik	k1gMnSc1	politik
Vít	Vít	k1gMnSc1	Vít
Obrtel	Obrtel	k1gMnSc1	Obrtel
(	(	kIx(	(
<g/>
1901	[number]	k4	1901
<g/>
-	-	kIx~	-
<g/>
1988	[number]	k4	1988
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
architekt	architekt	k1gMnSc1	architekt
<g/>
,	,	kIx,	,
návrhář	návrhář	k1gMnSc1	návrhář
<g/>
,	,	kIx,	,
scénograf	scénograf	k1gMnSc1	scénograf
Miroslav	Miroslav	k1gMnSc1	Miroslav
Jiroušek	Jiroušek	k1gMnSc1	Jiroušek
(	(	kIx(	(
<g/>
1903	[number]	k4	1903
<g/>
–	–	k?	–
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
matematik	matematik	k1gMnSc1	matematik
a	a	k8xC	a
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
Kurt	Kurt	k1gMnSc1	Kurt
Spielmann	Spielmann	k1gMnSc1	Spielmann
(	(	kIx(	(
<g/>
1903	[number]	k4	1903
<g/>
-	-	kIx~	-
<g/>
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
architekt	architekt	k1gMnSc1	architekt
Edgar	Edgar	k1gMnSc1	Edgar
G.	G.	kA	G.
Ulmer	Ulmer	k1gMnSc1	Ulmer
(	(	kIx(	(
<g/>
1904	[number]	k4	1904
<g/>
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
<g/>
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
režisér	režisér	k1gMnSc1	režisér
<g/>
,	,	kIx,	,
scenárista	scenárista	k1gMnSc1	scenárista
<g/>
,	,	kIx,	,
filmový	filmový	k2eAgMnSc1d1	filmový
producent	producent	k1gMnSc1	producent
František	František	k1gMnSc1	František
M.	M.	kA	M.
Hník	Hník	k1gMnSc1	Hník
(	(	kIx(	(
<g/>
1905	[number]	k4	1905
<g/>
–	–	k?	–
<g/>
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
teolog	teolog	k1gMnSc1	teolog
<g/>
,	,	kIx,	,
duchovní	duchovní	k1gMnSc1	duchovní
a	a	k8xC	a
biskup	biskup	k1gMnSc1	biskup
Církve	církev	k1gFnSc2	církev
československé	československý	k2eAgFnSc2d1	Československá
husitské	husitský	k2eAgFnSc2d1	husitská
<g/>
,	,	kIx,	,
vysokoškolský	vysokoškolský	k2eAgMnSc1d1	vysokoškolský
pedagog	pedagog	k1gMnSc1	pedagog
Magda	Magda	k1gFnSc1	Magda
Jansová	Jansová	k1gFnSc1	Jansová
(	(	kIx(	(
<g/>
1906	[number]	k4	1906
<g/>
–	–	k?	–
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
architektka	architektka	k1gFnSc1	architektka
Aljo	Aljo	k1gMnSc1	Aljo
Beran	Beran	k1gMnSc1	Beran
(	(	kIx(	(
<g/>
1907	[number]	k4	1907
<g/>
–	–	k?	–
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
Lubomír	Lubomír	k1gMnSc1	Lubomír
Šlapeta	Šlapeta	k1gFnSc1	Šlapeta
(	(	kIx(	(
<g/>
1908	[number]	k4	1908
<g/>
–	–	k?	–
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
architekt	architekt	k1gMnSc1	architekt
Helena	Helena	k1gFnSc1	Helena
Šmahelová	Šmahelová	k1gFnSc1	Šmahelová
(	(	kIx(	(
<g/>
1910	[number]	k4	1910
<g/>
–	–	k?	–
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
Evžen	Evžen	k1gMnSc1	Evžen
Rošický	Rošický	k2eAgMnSc1d1	Rošický
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
1914	[number]	k4	1914
<g/>
–	–	k?	–
<g/>
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
atlet	atlet	k1gMnSc1	atlet
a	a	k8xC	a
sportovní	sportovní	k2eAgMnSc1d1	sportovní
novinář	novinář	k1gMnSc1	novinář
Josef	Josef	k1gMnSc1	Josef
Bek	bek	k1gMnSc1	bek
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
<g/>
–	–	k?	–
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
Oldřich	Oldřich	k1gMnSc1	Oldřich
Velen	velen	k2eAgMnSc1d1	velen
(	(	kIx(	(
<g/>
1921	[number]	k4	1921
<g/>
–	–	k?	–
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
Hynek	Hynek	k1gMnSc1	Hynek
Maxa	Maxa	k1gMnSc1	Maxa
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
operní	operní	k2eAgMnSc1d1	operní
pěvec	pěvec	k1gMnSc1	pěvec
<g/>
,	,	kIx,	,
pěvecký	pěvecký	k2eAgMnSc1d1	pěvecký
pedagog	pedagog	k1gMnSc1	pedagog
Jiří	Jiří	k1gMnSc1	Jiří
Pelikán	Pelikán	k1gMnSc1	Pelikán
(	(	kIx(	(
<g/>
1923	[number]	k4	1923
<g/>
–	–	k?	–
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
Slavoj	Slavoj	k1gMnSc1	Slavoj
Kovařík	Kovařík	k1gMnSc1	Kovařík
(	(	kIx(	(
<g/>
1923	[number]	k4	1923
<g/>
–	–	k?	–
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
akademický	akademický	k2eAgMnSc1d1	akademický
malíř	malíř	k1gMnSc1	malíř
František	František	k1gMnSc1	František
Řehák	Řehák	k1gMnSc1	Řehák
(	(	kIx(	(
<g/>
*	*	kIx~	*
1923	[number]	k4	1923
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1959	[number]	k4	1959
žije	žít	k5eAaImIp3nS	žít
trvale	trvale	k6eAd1	trvale
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
Slávka	Slávka	k1gFnSc1	Slávka
Budínová	Budínová	k1gFnSc1	Budínová
(	(	kIx(	(
<g/>
1924	[number]	k4	1924
<g/>
–	–	k?	–
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
Antonín	Antonín	k1gMnSc1	Antonín
Schindler	Schindler	k1gMnSc1	Schindler
(	(	kIx(	(
<g/>
1925	[number]	k4	1925
<g/>
-	-	kIx~	-
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hudebník	hudebník	k1gMnSc1	hudebník
Juraj	Juraj	k1gMnSc1	Juraj
Pospíšil	Pospíšil	k1gMnSc1	Pospíšil
(	(	kIx(	(
<g/>
1931	[number]	k4	1931
<g/>
–	–	k?	–
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
slovenský	slovenský	k2eAgMnSc1d1	slovenský
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
českého	český	k2eAgInSc2d1	český
původu	původ	k1gInSc2	původ
Vlasta	Vlasta	k1gFnSc1	Vlasta
Janečková	Janečková	k1gFnSc1	Janečková
(	(	kIx(	(
<g/>
1934	[number]	k4	1934
<g/>
–	–	k?	–
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
filmová	filmový	k2eAgFnSc1d1	filmová
a	a	k8xC	a
televizní	televizní	k2eAgFnSc1d1	televizní
scenáristka	scenáristka	k1gFnSc1	scenáristka
a	a	k8xC	a
režisérka	režisérka	k1gFnSc1	režisérka
Jiří	Jiří	k1gMnSc1	Jiří
Fiedler	Fiedler	k1gMnSc1	Fiedler
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1935	[number]	k4	1935
Olomouc	Olomouc	k1gFnSc1	Olomouc
–	–	k?	–
31	[number]	k4	31
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2014	[number]	k4	2014
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
historik	historik	k1gMnSc1	historik
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
redaktor	redaktor	k1gMnSc1	redaktor
<g/>
,	,	kIx,	,
překladatel	překladatel	k1gMnSc1	překladatel
a	a	k8xC	a
muzejník	muzejník	k1gMnSc1	muzejník
Hana	Hana	k1gFnSc1	Hana
Talpová	Talpová	k1gFnSc1	Talpová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
a	a	k8xC	a
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Milan	Milan	k1gMnSc1	Milan
Togner	Togner	k1gMnSc1	Togner
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
–	–	k?	–
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
historik	historik	k1gMnSc1	historik
umění	umění	k1gNnSc2	umění
Karel	Karel	k1gMnSc1	Karel
Brückner	Brückner	k1gMnSc1	Brückner
(	(	kIx(	(
<g/>
*	*	kIx~	*
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fotbalový	fotbalový	k2eAgMnSc1d1	fotbalový
trenér	trenér	k1gMnSc1	trenér
Jiří	Jiří	k1gMnSc1	Jiří
Rusinský	rusinský	k2eAgMnSc1d1	rusinský
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
*	*	kIx~	*
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
publikací	publikace	k1gFnPc2	publikace
o	o	k7c6	o
městské	městský	k2eAgFnSc6d1	městská
části	část	k1gFnSc6	část
Olomouce	Olomouc	k1gFnSc2	Olomouc
–	–	k?	–
Neředíně	Neředína	k1gFnSc3	Neředína
Václav	Václav	k1gMnSc1	Václav
Čada	Čada	k1gMnSc1	Čada
(	(	kIx(	(
<g/>
1942	[number]	k4	1942
<g/>
–	–	k?	–
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
historik	historik	k1gMnSc1	historik
Josef	Josef	k1gMnSc1	Josef
Hrdlička	Hrdlička	k1gMnSc1	Hrdlička
(	(	kIx(	(
<g/>
*	*	kIx~	*
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pomocný	pomocný	k2eAgMnSc1d1	pomocný
biskup	biskup	k1gMnSc1	biskup
olomoucký	olomoucký	k2eAgMnSc1d1	olomoucký
<g/>
,	,	kIx,	,
děkan	děkan	k1gMnSc1	děkan
olomoucké	olomoucký	k2eAgFnSc2d1	olomoucká
metropolitní	metropolitní	k2eAgFnSc2d1	metropolitní
kapituly	kapitula	k1gFnSc2	kapitula
Pavel	Pavel	k1gMnSc1	Pavel
Kantorek	kantorka	k1gFnPc2	kantorka
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
*	*	kIx~	*
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přírodovědec	přírodovědec	k1gMnSc1	přírodovědec
<g/>
,	,	kIx,	,
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
,	,	kIx,	,
karikaturista	karikaturista	k1gMnSc1	karikaturista
<g/>
,	,	kIx,	,
vysokoškolský	vysokoškolský	k2eAgMnSc1d1	vysokoškolský
pedagog	pedagog	k1gMnSc1	pedagog
Pavel	Pavel	k1gMnSc1	Pavel
Dostál	Dostál	k1gMnSc1	Dostál
(	(	kIx(	(
<g/>
1943	[number]	k4	1943
<g/>
–	–	k?	–
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
režisér	režisér	k1gMnSc1	režisér
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
ministr	ministr	k1gMnSc1	ministr
kultury	kultura	k1gFnSc2	kultura
Pavel	Pavel	k1gMnSc1	Pavel
Roman	Roman	k1gMnSc1	Roman
(	(	kIx(	(
<g/>
1943	[number]	k4	1943
<g/>
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
<g/>
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
krasobruslař	krasobruslař	k1gMnSc1	krasobruslař
Karel	Karel	k1gMnSc1	Karel
Kryl	Kryl	k1gMnSc1	Kryl
(	(	kIx(	(
<g/>
1944	[number]	k4	1944
<g/>
–	–	k?	–
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
písničkář	písničkář	k1gMnSc1	písničkář
a	a	k8xC	a
básník	básník	k1gMnSc1	básník
Ivan	Ivan	k1gMnSc1	Ivan
Theimer	Theimer	k1gMnSc1	Theimer
(	(	kIx(	(
<g/>
*	*	kIx~	*
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sochař	sochař	k1gMnSc1	sochař
František	František	k1gMnSc1	František
Kunetka	Kunetka	k1gFnSc1	Kunetka
(	(	kIx(	(
<g/>
*	*	kIx~	*
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
teolog	teolog	k1gMnSc1	teolog
<g/>
,	,	kIx,	,
duchovní-salesián	duchovníalesián	k1gMnSc1	duchovní-salesián
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
vysokoškolský	vysokoškolský	k2eAgMnSc1d1	vysokoškolský
pedagog	pedagog	k1gMnSc1	pedagog
Hana	Hana	k1gFnSc1	Hana
Maciuchová	Maciuchová	k1gFnSc1	Maciuchová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
Eva	Eva	k1gFnSc1	Eva
Romanová	Romanová	k1gFnSc1	Romanová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
krasobruslařka	krasobruslařka	k1gFnSc1	krasobruslařka
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Hutka	Hutka	k1gMnSc1	Hutka
(	(	kIx(	(
<g/>
*	*	kIx~	*
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
písničkář	písničkář	k1gMnSc1	písničkář
Petr	Petr	k1gMnSc1	Petr
Novotný	Novotný	k1gMnSc1	Novotný
(	(	kIx(	(
<g/>
*	*	kIx~	*
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
bavič	bavič	k1gMnSc1	bavič
Jan	Jan	k1gMnSc1	Jan
<g />
.	.	kIx.	.
</s>
<s>
Kanyza	Kanyza	k1gFnSc1	Kanyza
(	(	kIx(	(
<g/>
*	*	kIx~	*
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
Věra	Věra	k1gFnSc1	Věra
Beranová	Beranová	k1gFnSc1	Beranová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kunsthistorička	kunsthistorička	k1gFnSc1	kunsthistorička
Vladimír	Vladimír	k1gMnSc1	Vladimír
Šlapeta	Šlapeta	k1gFnSc1	Šlapeta
(	(	kIx(	(
<g/>
*	*	kIx~	*
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
architekt	architekt	k1gMnSc1	architekt
<g/>
,	,	kIx,	,
pedagog	pedagog	k1gMnSc1	pedagog
Jan	Jan	k1gMnSc1	Jan
Graubner	Graubner	k1gMnSc1	Graubner
(	(	kIx(	(
<g/>
*	*	kIx~	*
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
olomoucký	olomoucký	k2eAgMnSc1d1	olomoucký
a	a	k8xC	a
metropolita	metropolita	k1gMnSc1	metropolita
moravský	moravský	k2eAgMnSc1d1	moravský
<g />
.	.	kIx.	.
</s>
<s>
Emil	Emil	k1gMnSc1	Emil
Viklický	Viklický	k2eAgMnSc1d1	Viklický
(	(	kIx(	(
<g/>
*	*	kIx~	*
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jazzový	jazzový	k2eAgMnSc1d1	jazzový
pianista	pianista	k1gMnSc1	pianista
a	a	k8xC	a
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
Petr	Petr	k1gMnSc1	Petr
Mikeš	Mikeš	k1gMnSc1	Mikeš
<g/>
(	(	kIx(	(
<g/>
*	*	kIx~	*
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
překladatel	překladatel	k1gMnSc1	překladatel
a	a	k8xC	a
redaktor	redaktor	k1gMnSc1	redaktor
Pavel	Pavel	k1gMnSc1	Pavel
Taussig	Taussig	k1gMnSc1	Taussig
(	(	kIx(	(
<g/>
*	*	kIx~	*
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
filmový	filmový	k2eAgMnSc1d1	filmový
historik	historik	k1gMnSc1	historik
<g/>
,	,	kIx,	,
scenárista	scenárista	k1gMnSc1	scenárista
<g/>
,	,	kIx,	,
novinář	novinář	k1gMnSc1	novinář
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
publicista	publicista	k1gMnSc1	publicista
Ladislav	Ladislav	k1gMnSc1	Ladislav
Daniel	Daniel	k1gMnSc1	Daniel
(	(	kIx(	(
<g/>
*	*	kIx~	*
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
historik	historik	k1gMnSc1	historik
umění	umění	k1gNnSc2	umění
Jana	Jana	k1gFnSc1	Jana
Šilerová	Šilerová	k1gFnSc1	Šilerová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
duchovní	duchovní	k1gMnSc1	duchovní
a	a	k8xC	a
biskupka	biskupka	k1gFnSc1	biskupka
Církve	církev	k1gFnSc2	církev
Československé	československý	k2eAgFnSc2d1	Československá
husitské	husitský	k2eAgFnSc2d1	husitská
Jiří	Jiří	k1gMnSc1	Jiří
Paroubek	Paroubek	k1gInSc1	Paroubek
(	(	kIx(	(
<g/>
*	*	kIx~	*
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
Rudolf	Rudolf	k1gMnSc1	Rudolf
Göbel	Göbel	k1gMnSc1	Göbel
(	(	kIx(	(
<g/>
*	*	kIx~	*
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
duchovní	duchovní	k1gMnSc1	duchovní
a	a	k8xC	a
biskup	biskup	k1gMnSc1	biskup
Církve	církev	k1gFnSc2	církev
Československé	československý	k2eAgFnSc2d1	Československá
husitské	husitský	k2eAgFnSc2d1	husitská
Jarmila	Jarmila	k1gFnSc1	Jarmila
Švehlová	Švehlová	k1gFnSc1	Švehlová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
Alice	Alice	k1gFnSc1	Alice
Šnirychová	Šnirychová	k1gFnSc1	Šnirychová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
Jaroslava	Jaroslava	k1gFnSc1	Jaroslava
Maxová	Maxová	k1gFnSc1	Maxová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
operní	operní	k2eAgFnPc1d1	operní
pěvkyně	pěvkyně	k1gFnPc1	pěvkyně
Karel	Karel	k1gMnSc1	Karel
Plíhal	Plíhal	k1gMnSc1	Plíhal
(	(	kIx(	(
<g/>
*	*	kIx~	*
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
písničkář	písničkář	k1gMnSc1	písničkář
Rostislav	Rostislav	k1gMnSc1	Rostislav
Čtvrtlík	Čtvrtlík	k1gInSc1	Čtvrtlík
(	(	kIx(	(
<g/>
1963	[number]	k4	1963
<g/>
–	–	k?	–
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
dabér	dabér	k1gMnSc1	dabér
Igor	Igor	k1gMnSc1	Igor
Bareš	Bareš	k1gMnSc1	Bareš
(	(	kIx(	(
<g/>
*	*	kIx~	*
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
Richard	Richard	k1gMnSc1	Richard
Pachman	Pachman	k1gMnSc1	Pachman
(	(	kIx(	(
<g/>
*	*	kIx~	*
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
Petr	Petr	k1gMnSc1	Petr
Pravec	Pravec	k1gMnSc1	Pravec
(	(	kIx(	(
<g/>
*	*	kIx~	*
1967	[number]	k4	1967
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
astronom	astronom	k1gMnSc1	astronom
Ivan	Ivan	k1gMnSc1	Ivan
Langer	Langer	k1gMnSc1	Langer
(	(	kIx(	(
<g/>
*	*	kIx~	*
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
a	a	k8xC	a
bývalý	bývalý	k2eAgMnSc1d1	bývalý
ministr	ministr	k1gMnSc1	ministr
Jiří	Jiří	k1gMnSc1	Jiří
Dopita	dopit	k2eAgMnSc4d1	dopit
(	(	kIx(	(
<g/>
*	*	kIx~	*
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hokejista	hokejista	k1gMnSc1	hokejista
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Šnirych	Šnirych	k1gMnSc1	Šnirych
(	(	kIx(	(
<g/>
*	*	kIx~	*
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
knihař	knihař	k1gMnSc1	knihař
Václav	Václav	k1gMnSc1	Václav
Blahunek	Blahunka	k1gFnPc2	Blahunka
(	(	kIx(	(
<g/>
*	*	kIx~	*
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
šéfdirigent	šéfdirigent	k1gMnSc1	šéfdirigent
Hudby	hudba	k1gFnSc2	hudba
Hradní	hradní	k2eAgFnSc2d1	hradní
stráže	stráž	k1gFnSc2	stráž
a	a	k8xC	a
Policie	policie	k1gFnSc2	policie
ČR	ČR	kA	ČR
Marek	Marek	k1gMnSc1	Marek
Spáčil	Spáčil	k1gMnSc1	Spáčil
(	(	kIx(	(
<g/>
*	*	kIx~	*
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
automobilový	automobilový	k2eAgMnSc1d1	automobilový
závodník	závodník	k1gMnSc1	závodník
Sandra	Sandra	k1gFnSc1	Sandra
Pogodová	Pogodový	k2eAgFnSc1d1	Pogodová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
Lubomír	Lubomír	k1gMnSc1	Lubomír
Kopeček	Kopeček	k1gMnSc1	Kopeček
(	(	kIx(	(
<g/>
*	*	kIx~	*
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
politolog	politolog	k1gMnSc1	politolog
Veronika	Veronika	k1gFnSc1	Veronika
Vařeková	Vařeková	k1gFnSc1	Vařeková
(	(	kIx(	(
<g/>
*	*	kIx~	*
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
topmodelka	topmodelka	k1gFnSc1	topmodelka
Tomáš	Tomáš	k1gMnSc1	Tomáš
Hudeček	Hudeček	k1gMnSc1	Hudeček
(	(	kIx(	(
<g/>
*	*	kIx~	*
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
geograf	geograf	k1gMnSc1	geograf
<g/>
,	,	kIx,	,
politika	politika	k1gFnSc1	politika
<g/>
,	,	kIx,	,
primátor	primátor	k1gMnSc1	primátor
hl.	hl.	k?	hl.
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
Aleš	Aleš	k1gMnSc1	Aleš
Loprais	Loprais	k1gInSc1	Loprais
(	(	kIx(	(
<g/>
*	*	kIx~	*
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
automobilový	automobilový	k2eAgMnSc1d1	automobilový
závodník	závodník	k1gMnSc1	závodník
Roman	Roman	k1gMnSc1	Roman
Smetana	Smetana	k1gMnSc1	Smetana
(	(	kIx(	(
<g/>
*	*	kIx~	*
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aktivista	aktivista	k1gMnSc1	aktivista
Jiří	Jiří	k1gMnSc1	Jiří
Hudler	Hudler	k1gMnSc1	Hudler
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
*	*	kIx~	*
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hokejista	hokejista	k1gMnSc1	hokejista
Karlos	Karlos	k1gMnSc1	Karlos
Vémola	Vémola	k1gFnSc1	Vémola
(	(	kIx(	(
<g/>
*	*	kIx~	*
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bojovník	bojovník	k1gMnSc1	bojovník
Mma	Mma	k1gFnSc2	Mma
Olomouc	Olomouc	k1gFnSc1	Olomouc
má	mít	k5eAaImIp3nS	mít
následující	následující	k2eAgNnPc4d1	následující
partnerská	partnerský	k2eAgNnPc4d1	partnerské
města	město	k1gNnPc4	město
<g/>
:	:	kIx,	:
Antony	anton	k1gInPc1	anton
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
Lucern	Lucern	k1gInSc1	Lucern
<g/>
,	,	kIx,	,
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
Nördlingen	Nördlingen	k1gInSc1	Nördlingen
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
>	>	kIx)	>
www	www	k?	www
<g/>
.	.	kIx.	.
<g/>
nördlingen	nördlingen	k1gInSc1	nördlingen
<g/>
.	.	kIx.	.
<g/>
de	de	k?	de
Pětikostelí	pětikostelí	k1gNnSc2	pětikostelí
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
Subotica	Subotic	k1gInSc2	Subotic
<g/>
,	,	kIx,	,
Srbsko	Srbsko	k1gNnSc4	Srbsko
Tampere	Tamper	k1gInSc5	Tamper
<g/>
,	,	kIx,	,
Finsko	Finsko	k1gNnSc1	Finsko
Veenendaal	Veenendaal	k1gInSc1	Veenendaal
<g/>
,	,	kIx,	,
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
Bratislava	Bratislava	k1gFnSc1	Bratislava
-	-	kIx~	-
Staré	Staré	k2eAgNnSc1d1	Staré
Mesto	Mesto	k1gNnSc1	Mesto
<g/>
,	,	kIx,	,
Slovensko	Slovensko	k1gNnSc1	Slovensko
Krakov	Krakov	k1gInSc1	Krakov
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
Kunming	Kunming	k1gInSc1	Kunming
<g/>
,	,	kIx,	,
Čínská	čínský	k2eAgFnSc1d1	čínská
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
Makarska	Makarská	k1gFnSc1	Makarská
<g/>
,	,	kIx,	,
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
Owensboro	Owensbora	k1gFnSc5	Owensbora
<g/>
,	,	kIx,	,
USA	USA	kA	USA
Díky	díky	k7c3	díky
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Petru	Petr	k1gMnSc3	Petr
Pravcovi	Pravec	k1gMnSc3	Pravec
se	se	k3xPyFc4	se
planetka	planetka	k1gFnSc1	planetka
s	s	k7c7	s
číslem	číslo	k1gNnSc7	číslo
30	[number]	k4	30
564	[number]	k4	564
v	v	k7c6	v
hlavním	hlavní	k2eAgInSc6d1	hlavní
pásu	pás	k1gInSc6	pás
asteroidů	asteroid	k1gInPc2	asteroid
mezi	mezi	k7c7	mezi
Marsem	Mars	k1gInSc7	Mars
a	a	k8xC	a
Jupiterem	Jupiter	k1gMnSc7	Jupiter
nazývá	nazývat	k5eAaImIp3nS	nazývat
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
.	.	kIx.	.
</s>
<s>
Astronom	astronom	k1gMnSc1	astronom
ji	on	k3xPp3gFnSc4	on
objevil	objevit	k5eAaPmAgMnS	objevit
při	při	k7c6	při
pozorování	pozorování	k1gNnSc6	pozorování
z	z	k7c2	z
hvězdárny	hvězdárna	k1gFnSc2	hvězdárna
v	v	k7c6	v
Ondřejově	Ondřejův	k2eAgNnSc6d1	Ondřejovo
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
šéf	šéf	k1gMnSc1	šéf
hemato-onkologické	hematonkologický	k2eAgFnSc2d1	hemato-onkologická
kliniky	klinika	k1gFnSc2	klinika
olomoucké	olomoucký	k2eAgFnSc2d1	olomoucká
fakultní	fakultní	k2eAgFnSc2d1	fakultní
nemocnice	nemocnice	k1gFnSc2	nemocnice
Karel	Karel	k1gMnSc1	Karel
Indrák	Indrák	k1gMnSc1	Indrák
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
zjistil	zjistit	k5eAaPmAgInS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jednu	jeden	k4xCgFnSc4	jeden
dědičnou	dědičný	k2eAgFnSc4d1	dědičná
chorobu	choroba	k1gFnSc4	choroba
krve	krev	k1gFnSc2	krev
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
neznámý	známý	k2eNgInSc4d1	neznámý
druh	druh	k1gInSc4	druh
hemoglobinu	hemoglobin	k1gInSc2	hemoglobin
a	a	k8xC	a
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
ho	on	k3xPp3gNnSc4	on
po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
rodném	rodný	k2eAgNnSc6d1	rodné
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Miroslav	Miroslav	k1gMnSc1	Miroslav
Strnad	Strnad	k1gMnSc1	Strnad
z	z	k7c2	z
Univerzity	univerzita	k1gFnSc2	univerzita
Palackého	Palacký	k1gMnSc2	Palacký
společně	společně	k6eAd1	společně
s	s	k7c7	s
kolegou	kolega	k1gMnSc7	kolega
Jaroslavem	Jaroslav	k1gMnSc7	Jaroslav
Veselým	Veselý	k1gMnSc7	Veselý
hledali	hledat	k5eAaImAgMnP	hledat
v	v	k7c6	v
rostlinách	rostlina	k1gFnPc6	rostlina
látku	látka	k1gFnSc4	látka
podporující	podporující	k2eAgMnPc1d1	podporující
jejich	jejich	k3xOp3gInSc4	jejich
růst	růst	k1gInSc4	růst
nalezli	nalézt	k5eAaBmAgMnP	nalézt
i	i	k8xC	i
derivát	derivát	k1gInSc4	derivát
rostlinných	rostlinný	k2eAgInPc2d1	rostlinný
hormonů	hormon	k1gInPc2	hormon
cytokininů	cytokinin	k1gInPc2	cytokinin
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
buněčné	buněčný	k2eAgInPc4d1	buněčný
dělení	dělení	k1gNnSc1	dělení
naopak	naopak	k6eAd1	naopak
brzdí	brzdit	k5eAaImIp3nS	brzdit
<g/>
.	.	kIx.	.
</s>
<s>
Novou	nový	k2eAgFnSc4d1	nová
látku	látka	k1gFnSc4	látka
nazvali	nazvat	k5eAaPmAgMnP	nazvat
olomoucin	olomoucin	k1gMnSc1	olomoucin
<g/>
,	,	kIx,	,
při	při	k7c6	při
testování	testování	k1gNnSc6	testování
pak	pak	k6eAd1	pak
zjistili	zjistit	k5eAaPmAgMnP	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
protinádorové	protinádorový	k2eAgInPc4d1	protinádorový
účinky	účinek	k1gInPc4	účinek
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
toxická	toxický	k2eAgFnSc1d1	toxická
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
francouzský	francouzský	k2eAgMnSc1d1	francouzský
kolega	kolega	k1gMnSc1	kolega
Laurent	Laurent	k1gMnSc1	Laurent
Meijer	Meijer	k1gMnSc1	Meijer
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
německém	německý	k2eAgNnSc6d1	německé
městě	město	k1gNnSc6	město
Nördlingen	Nördlingen	k1gInSc4	Nördlingen
<g/>
,	,	kIx,	,
jednom	jeden	k4xCgMnSc6	jeden
z	z	k7c2	z
osmi	osm	k4xCc2	osm
partnerských	partnerský	k2eAgNnPc2d1	partnerské
měst	město	k1gNnPc2	město
Olomouce	Olomouc	k1gFnSc2	Olomouc
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
najít	najít	k5eAaPmF	najít
Olomouckou	olomoucký	k2eAgFnSc4d1	olomoucká
ulici	ulice	k1gFnSc4	ulice
(	(	kIx(	(
<g/>
Olmützer	Olmützer	k1gInSc4	Olmützer
Strasse	Strasse	k1gFnSc2	Strasse
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
obdivovat	obdivovat	k5eAaImF	obdivovat
Olomouckou	olomoucký	k2eAgFnSc4d1	olomoucká
kašnu	kašna	k1gFnSc4	kašna
(	(	kIx(	(
<g/>
Olmützer	Olmützer	k1gInSc1	Olmützer
Brunnen	Brunnen	k1gInSc1	Brunnen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
v	v	k7c6	v
příštích	příští	k2eAgNnPc6d1	příští
letech	léto	k1gNnPc6	léto
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
Olomouc	Olomouc	k1gFnSc1	Olomouc
zanechat	zanechat	k5eAaPmF	zanechat
stopu	stopa	k1gFnSc4	stopa
i	i	k9	i
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Antony	anton	k1gInPc4	anton
<g/>
,	,	kIx,	,
nedaleko	nedaleko	k7c2	nedaleko
Paříže	Paříž	k1gFnSc2	Paříž
(	(	kIx(	(
<g/>
v	v	k7c6	v
departementu	departement	k1gInSc6	departement
Hauts-de-Seine	Hautse-Sein	k1gInSc5	Hauts-de-Sein
<g/>
,	,	kIx,	,
Île-de-France	Îlee-France	k1gFnSc1	Île-de-France
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
Olomoucké	olomoucký	k2eAgNnSc1d1	olomoucké
náměstí	náměstí	k1gNnSc1	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Chystá	chystat	k5eAaImIp3nS	chystat
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
výstavba	výstavba	k1gFnSc1	výstavba
nové	nový	k2eAgFnSc2d1	nová
čtvrti	čtvrt	k1gFnSc2	čtvrt
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gNnSc4	jejíž
centrální	centrální	k2eAgNnSc4d1	centrální
náměstí	náměstí	k1gNnSc4	náměstí
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
jmenovat	jmenovat	k5eAaBmF	jmenovat
Place	plac	k1gInSc6	plac
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgNnPc6	některý
partnerských	partnerský	k2eAgNnPc6d1	partnerské
městech	město	k1gNnPc6	město
také	také	k9	také
upozorňují	upozorňovat	k5eAaImIp3nP	upozorňovat
na	na	k7c4	na
Olomouc	Olomouc	k1gFnSc4	Olomouc
ukazatele	ukazatel	k1gInSc2	ukazatel
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterých	který	k3yQgMnPc2	který
lidé	člověk	k1gMnPc1	člověk
poznají	poznat	k5eAaPmIp3nP	poznat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
daleko	daleko	k6eAd1	daleko
se	se	k3xPyFc4	se
od	od	k7c2	od
města	město	k1gNnSc2	město
nacházejí	nacházet	k5eAaImIp3nP	nacházet
<g/>
.	.	kIx.	.
</s>
<s>
Nechybí	chybět	k5eNaImIp3nS	chybět
například	například	k6eAd1	například
ve	v	k7c6	v
finském	finský	k2eAgNnSc6d1	finské
Tampere	Tamper	k1gInSc5	Tamper
<g/>
.	.	kIx.	.
</s>
