<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1997	[number]	k4	1997
byl	být	k5eAaImAgMnS	být
k	k	k7c3	k
Čínské	čínský	k2eAgFnSc3d1	čínská
lidové	lidový	k2eAgFnSc3d1	lidová
republice	republika	k1gFnSc3	republika
připojen	připojen	k2eAgInSc1d1	připojen
Hong	Hong	k1gInSc1	Hong
Kong	Kongo	k1gNnPc2	Kongo
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
pod	pod	k7c7	pod
správou	správa	k1gFnSc7	správa
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
podmínkou	podmínka	k1gFnSc7	podmínka
"	"	kIx"	"
<g/>
jeden	jeden	k4xCgInSc1	jeden
stát	stát	k1gInSc1	stát
<g/>
,	,	kIx,	,
dva	dva	k4xCgInPc1	dva
systémy	systém	k1gInPc1	systém
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
20	[number]	k4	20
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1999	[number]	k4	1999
bylo	být	k5eAaImAgNnS	být
pod	pod	k7c7	pod
stejnými	stejný	k2eAgFnPc7d1	stejná
podmínkami	podmínka	k1gFnPc7	podmínka
připojeno	připojit	k5eAaPmNgNnS	připojit
Macao	Macao	k1gNnSc1	Macao
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
pod	pod	k7c7	pod
správou	správa	k1gFnSc7	správa
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
<g/>
.	.	kIx.	.
</s>
