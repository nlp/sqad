<p>
<s>
Ovce	ovce	k1gFnSc1	ovce
Dolly	Dolla	k1gFnSc2	Dolla
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1996	[number]	k4	1996
–	–	k?	–
14	[number]	k4	14
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
prvním	první	k4xOgMnSc7	první
savcem	savec	k1gMnSc7	savec
úspěšně	úspěšně	k6eAd1	úspěšně
naklonovaným	naklonovaný	k2eAgMnPc3d1	naklonovaný
ze	z	k7c2	z
somatické	somatický	k2eAgFnSc2d1	somatická
buňky	buňka	k1gFnSc2	buňka
dospělého	dospělý	k2eAgMnSc2d1	dospělý
jedince	jedinec	k1gMnSc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
historie	historie	k1gFnSc2	historie
Dolly	Dolla	k1gFnSc2	Dolla
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
23	[number]	k4	23
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgMnS	být
veřejnosti	veřejnost	k1gFnSc6	veřejnost
Skotským	skotský	k2eAgInSc7d1	skotský
výzkumným	výzkumný	k2eAgInSc7d1	výzkumný
ústavem	ústav	k1gInSc7	ústav
potvrzen	potvrzen	k2eAgInSc1d1	potvrzen
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
pokus	pokus	k1gInSc1	pokus
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
vedl	vést	k5eAaImAgMnS	vést
její	její	k3xOp3gMnSc1	její
"	"	kIx"	"
<g/>
duchovní	duchovní	k1gMnSc1	duchovní
otec	otec	k1gMnSc1	otec
<g/>
"	"	kIx"	"
profesor	profesor	k1gMnSc1	profesor
Ian	Ian	k1gMnSc1	Ian
Wilmut	Wilmut	k1gMnSc1	Wilmut
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
buňky	buňka	k1gFnPc1	buňka
v	v	k7c6	v
jejím	její	k3xOp3gNnSc6	její
těle	tělo	k1gNnSc6	tělo
neodpovídají	odpovídat	k5eNaImIp3nP	odpovídat
jejímu	její	k3xOp3gNnSc3	její
fyzickému	fyzický	k2eAgNnSc3d1	fyzické
stáří	stáří	k1gNnSc3	stáří
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
spíše	spíše	k9	spíše
věku	věk	k1gInSc2	věk
její	její	k3xOp3gFnSc2	její
genetické	genetický	k2eAgFnSc2d1	genetická
předchůdkyně	předchůdkyně	k1gFnSc2	předchůdkyně
–	–	k?	–
šestileté	šestiletý	k2eAgFnSc2d1	šestiletá
ovce	ovce	k1gFnSc2	ovce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dolly	Doll	k1gInPc4	Doll
přivedla	přivést	k5eAaPmAgFnS	přivést
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
na	na	k7c4	na
svět	svět	k1gInSc4	svět
šest	šest	k4xCc1	šest
zdravých	zdravý	k2eAgNnPc2d1	zdravé
jehňat	jehně	k1gNnPc2	jehně
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
života	život	k1gInSc2	život
ji	on	k3xPp3gFnSc4	on
sužovala	sužovat	k5eAaImAgFnS	sužovat
artritida	artritida	k1gFnSc1	artritida
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
onemocnění	onemocnění	k1gNnSc1	onemocnění
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
takto	takto	k6eAd1	takto
starých	starý	k2eAgFnPc2d1	stará
ovcí	ovce	k1gFnPc2	ovce
neobvyklé	obvyklý	k2eNgNnSc1d1	neobvyklé
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ne	ne	k9	ne
výjimečné	výjimečný	k2eAgFnPc1d1	výjimečná
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
Dolly	Dolla	k1gFnSc2	Dolla
získala	získat	k5eAaPmAgFnS	získat
podle	podle	k7c2	podle
slavné	slavný	k2eAgFnSc2d1	slavná
americké	americký	k2eAgFnSc2d1	americká
country	country	k2eAgFnSc2d1	country
zpěvačky	zpěvačka	k1gFnSc2	zpěvačka
Dolly	Dolla	k1gFnSc2	Dolla
Partonové	Partonový	k2eAgFnSc2d1	Partonová
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
vědců	vědec	k1gMnPc2	vědec
ve	v	k7c6	v
skotském	skotský	k1gInSc6	skotský
Roslinově	Roslinově	k1gFnSc1	Roslinově
institutu	institut	k1gInSc2	institut
ji	on	k3xPp3gFnSc4	on
totiž	totiž	k9	totiž
stvořila	stvořit	k5eAaPmAgFnS	stvořit
z	z	k7c2	z
buňky	buňka	k1gFnSc2	buňka
mléčné	mléčný	k2eAgFnSc2d1	mléčná
žlázy	žláza	k1gFnSc2	žláza
a	a	k8xC	a
nikdo	nikdo	k3yNnSc1	nikdo
si	se	k3xPyFc3	se
prý	prý	k9	prý
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
vybavit	vybavit	k5eAaPmF	vybavit
působivější	působivý	k2eAgNnPc4d2	působivější
prsa	prsa	k1gNnPc4	prsa
<g/>
,	,	kIx,	,
než	než	k8xS	než
jaká	jaký	k3yRgFnSc1	jaký
měla	mít	k5eAaImAgFnS	mít
Dolly	Doll	k1gMnPc4	Doll
Partonová	Partonový	k2eAgFnSc1d1	Partonová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
14	[number]	k4	14
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2003	[number]	k4	2003
byla	být	k5eAaImAgFnS	být
Dolly	Dolla	k1gFnSc2	Dolla
utracena	utratit	k5eAaPmNgFnS	utratit
pro	pro	k7c4	pro
plicní	plicní	k2eAgFnSc4d1	plicní
infekci	infekce	k1gFnSc4	infekce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Dolly	Dolla	k1gFnSc2	Dolla
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Dolly	Dolla	k1gFnPc1	Dolla
the	the	k?	the
Sheep	Sheep	k1gMnSc1	Sheep
<g/>
,	,	kIx,	,
1996-2000	[number]	k4	1996-2000
from	from	k1gInSc1	from
the	the	k?	the
Science	Science	k1gFnSc1	Science
Museum	museum	k1gNnSc4	museum
<g/>
,	,	kIx,	,
London	London	k1gMnSc1	London
</s>
</p>
<p>
<s>
Cloning	Cloning	k1gInSc1	Cloning
-	-	kIx~	-
A	a	k9	a
life	lifat	k5eAaPmIp3nS	lifat
of	of	k?	of
Dolly	Doll	k1gInPc1	Doll
from	from	k1gInSc1	from
the	the	k?	the
Roslin	Roslin	k1gInSc1	Roslin
Institute	institut	k1gInSc5	institut
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Animal	animal	k1gMnSc1	animal
cloning	cloning	k1gInSc1	cloning
&	&	k?	&
Dolly	Dolla	k1gFnSc2	Dolla
</s>
</p>
<p>
<s>
Image	image	k1gFnSc1	image
library	librara	k1gFnSc2	librara
Photos	Photos	k1gInSc1	Photos
of	of	k?	of
Dolly	Dolla	k1gFnSc2	Dolla
and	and	k?	and
other	other	k1gMnSc1	other
cloned	cloned	k1gMnSc1	cloned
animals	animals	k6eAd1	animals
at	at	k?	at
the	the	k?	the
Roslin	Roslin	k1gInSc1	Roslin
Institute	institut	k1gInSc5	institut
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Analysis	Analysis	k1gFnSc1	Analysis
of	of	k?	of
Telomere	Telomer	k1gInSc5	Telomer
Length	Length	k1gInSc4	Length
in	in	k?	in
Dolly	Dolla	k1gFnSc2	Dolla
<g/>
,	,	kIx,	,
a	a	k8xC	a
Sheep	Sheep	k1gMnSc1	Sheep
Derived	Derived	k1gMnSc1	Derived
by	by	kYmCp3nS	by
Nuclear	Nuclear	k1gInSc1	Nuclear
Transfer	transfer	k1gInSc4	transfer
:	:	kIx,	:
Cloning	Cloning	k1gInSc1	Cloning
</s>
</p>
