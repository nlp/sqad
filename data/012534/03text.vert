<p>
<s>
Paso	Paso	k6eAd1	Paso
doble	doble	k6eAd1	doble
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
psáno	psát	k5eAaImNgNnS	psát
pasodoble	pasodoble	k6eAd1	pasodoble
<g/>
,	,	kIx,	,
španělsky	španělsky	k6eAd1	španělsky
dvojkrok	dvojkrok	k1gInSc1	dvojkrok
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
latinskoamerického	latinskoamerický	k2eAgInSc2d1	latinskoamerický
tance	tanec	k1gInSc2	tanec
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
symbolicky	symbolicky	k6eAd1	symbolicky
znázorňuje	znázorňovat	k5eAaImIp3nS	znázorňovat
koridu	korida	k1gFnSc4	korida
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
tomto	tento	k3xDgInSc6	tento
tanci	tanec	k1gInSc6	tanec
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
jako	jako	k8xC	jako
o	o	k7c6	o
španělském	španělský	k2eAgInSc6d1	španělský
onestepu	onestep	k1gInSc6	onestep
<g/>
.	.	kIx.	.
</s>
<s>
Známějším	známý	k2eAgMnPc3d2	známější
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
víc	hodně	k6eAd2	hodně
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
<g/>
.	.	kIx.	.
</s>
<s>
Standardizovali	standardizovat	k5eAaBmAgMnP	standardizovat
ho	on	k3xPp3gNnSc4	on
francouzští	francouzský	k2eAgMnPc1d1	francouzský
taneční	taneční	k2eAgMnPc1d1	taneční
učitelé	učitel	k1gMnPc1	učitel
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
původ	původ	k1gInSc1	původ
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
v	v	k7c6	v
lidových	lidový	k2eAgInPc6d1	lidový
tancích	tanec	k1gInPc6	tanec
oslavujících	oslavující	k2eAgInPc6d1	oslavující
vítězného	vítězný	k2eAgMnSc4d1	vítězný
toreadora	toreador	k1gMnSc4	toreador
při	při	k7c6	při
býčích	býčí	k2eAgInPc6d1	býčí
zápasech	zápas	k1gInPc6	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Partner	partner	k1gMnSc1	partner
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
toreadora	toreador	k1gMnSc4	toreador
v	v	k7c6	v
aréně	aréna	k1gFnSc6	aréna
<g/>
,	,	kIx,	,
partnerka	partnerka	k1gFnSc1	partnerka
představuje	představovat	k5eAaImIp3nS	představovat
toreadorův	toreadorův	k2eAgInSc4d1	toreadorův
šátek	šátek	k1gInSc4	šátek
(	(	kIx(	(
<g/>
muletu	muleta	k1gFnSc4	muleta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Soutěžním	soutěžní	k2eAgInSc7d1	soutěžní
tancem	tanec	k1gInSc7	tanec
se	se	k3xPyFc4	se
paso	paso	k1gNnSc1	paso
doble	doble	k6eAd1	doble
stalo	stát	k5eAaPmAgNnS	stát
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Paso	Paso	k6eAd1	Paso
doble	doble	k6eAd1	doble
se	se	k3xPyFc4	se
tančí	tančit	k5eAaImIp3nS	tančit
ve	v	k7c6	v
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
taktu	takt	k1gInSc2	takt
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc1	jeho
tempo	tempo	k1gNnSc1	tempo
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c7	mezi
60	[number]	k4	60
a	a	k8xC	a
62	[number]	k4	62
takty	takt	k1gInPc7	takt
za	za	k7c4	za
minutu	minuta	k1gFnSc4	minuta
(	(	kIx(	(
<g/>
metronom	metronom	k1gInSc1	metronom
120	[number]	k4	120
<g/>
-	-	kIx~	-
<g/>
124	[number]	k4	124
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Paso	Paso	k6eAd1	Paso
doble	doble	k6eAd1	doble
se	se	k3xPyFc4	se
na	na	k7c6	na
tanečních	taneční	k2eAgFnPc6d1	taneční
soutěžích	soutěž	k1gFnPc6	soutěž
tančí	tančit	k5eAaImIp3nS	tančit
až	až	k9	až
od	od	k7c2	od
taneční	taneční	k2eAgFnSc2d1	taneční
třídy	třída	k1gFnSc2	třída
"	"	kIx"	"
<g/>
C	C	kA	C
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Figury	figura	k1gFnSc2	figura
==	==	k?	==
</s>
</p>
<p>
<s>
Figury	figura	k1gFnPc1	figura
podle	podle	k7c2	podle
ČSTS	ČSTS	kA	ČSTS
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Na	na	k7c6	na
místě	místo	k1gNnSc6	místo
–	–	k?	–
Sur	Sur	k1gFnSc4	Sur
Place	plac	k1gInSc6	plac
</s>
</p>
<p>
<s>
Základní	základní	k2eAgInSc1d1	základní
pohyb	pohyb	k1gInSc1	pohyb
–	–	k?	–
Basic	Basic	kA	Basic
Movement	Movement	k1gMnSc1	Movement
</s>
</p>
<p>
<s>
Výzva	výzva	k1gFnSc1	výzva
–	–	k?	–
Apel	apel	k1gInSc1	apel
</s>
</p>
<p>
<s>
Přeměny	přeměna	k1gFnPc1	přeměna
vpravo	vpravo	k6eAd1	vpravo
a	a	k8xC	a
vlevo	vlevo	k6eAd1	vlevo
–	–	k?	–
Chasses	Chasses	k1gInSc1	Chasses
to	ten	k3xDgNnSc1	ten
Right	Right	k2eAgMnSc1d1	Right
and	and	k?	and
Left	Left	k1gMnSc1	Left
</s>
</p>
<p>
<s>
Zdvihy	zdvih	k1gInPc4	zdvih
vpravo	vpravo	k6eAd1	vpravo
a	a	k8xC	a
vlevo	vlevo	k6eAd1	vlevo
–	–	k?	–
Elevations	Elevations	k1gInSc1	Elevations
to	ten	k3xDgNnSc1	ten
Right	Right	k2eAgMnSc1d1	Right
and	and	k?	and
Left	Left	k1gMnSc1	Left
</s>
</p>
<p>
<s>
Z	z	k7c2	z
místa	místo	k1gNnSc2	místo
–	–	k?	–
Déplacement	Déplacement	k1gMnSc1	Déplacement
</s>
</p>
<p>
<s>
Útok	útok	k1gInSc1	útok
–	–	k?	–
Attack	Attack	k1gInSc1	Attack
</s>
</p>
<p>
<s>
Oddálení	oddálení	k1gNnSc4	oddálení
–	–	k?	–
Separation	Separation	k1gInSc1	Separation
</s>
</p>
<p>
<s>
základní	základní	k2eAgFnSc1d1	základní
–	–	k?	–
Basic	Basic	kA	Basic
</s>
</p>
<p>
<s>
se	s	k7c7	s
spádným	spádný	k2eAgNnSc7d1	spádný
zakončením	zakončení	k1gNnSc7	zakončení
–	–	k?	–
with	with	k1gMnSc1	with
Fallaway	Fallawaa	k1gFnSc2	Fallawaa
ending	ending	k1gInSc1	ending
</s>
</p>
<p>
<s>
do	do	k7c2	do
spádného	spádný	k2eAgInSc2d1	spádný
zášvihu	zášvih	k1gInSc2	zášvih
–	–	k?	–
to	ten	k3xDgNnSc1	ten
Fallaway	Fallawa	k2eAgInPc4d1	Fallawa
Whisk	Whisk	k1gInSc1	Whisk
</s>
</p>
<p>
<s>
se	s	k7c7	s
šátkovou	šátková	k1gFnSc7	šátková
chůzi	chůze	k1gFnSc4	chůze
dámy	dáma	k1gFnSc2	dáma
do	do	k7c2	do
spádného	spádný	k2eAgInSc2d1	spádný
zášvihu	zášvih	k1gInSc2	zášvih
–	–	k?	–
with	with	k1gInSc1	with
Lady	lady	k1gFnSc1	lady
<g/>
́	́	k?	́
<g/>
s	s	k7c7	s
Caping	Caping	k1gInSc1	Caping
Walks	Walks	k1gInSc1	Walks
to	ten	k3xDgNnSc1	ten
Fallaway	Fallawaa	k1gFnPc4	Fallawaa
Whisk	Whisk	k1gInSc4	Whisk
</s>
</p>
<p>
<s>
Synkopované	synkopovaný	k2eAgNnSc4d1	synkopovaný
oddálení	oddálení	k1gNnSc4	oddálení
–	–	k?	–
Syncopated	Syncopated	k1gInSc1	Syncopated
Separations	Separations	k1gInSc1	Separations
</s>
</p>
<p>
<s>
Osmička	osmička	k1gFnSc1	osmička
–	–	k?	–
Huit	Huit	k1gInSc1	Huit
</s>
</p>
<p>
<s>
Šestnáctka	šestnáctka	k1gFnSc1	šestnáctka
–	–	k?	–
Sixteen	Sixteen	k1gInSc1	Sixteen
</s>
</p>
<p>
<s>
Promenády	promenáda	k1gFnPc1	promenáda
–	–	k?	–
Promenades	Promenades	k1gMnSc1	Promenades
</s>
</p>
<p>
<s>
promenáda	promenáda	k1gFnSc1	promenáda
–	–	k?	–
Promenade	Promenad	k1gInSc5	Promenad
</s>
</p>
<p>
<s>
promenádou	promenáda	k1gFnSc7	promenáda
do	do	k7c2	do
obrácené	obrácený	k2eAgFnSc2d1	obrácená
promenády	promenáda	k1gFnSc2	promenáda
–	–	k?	–
Promenade	Promenad	k1gInSc5	Promenad
to	ten	k3xDgNnSc1	ten
Counter	Countrum	k1gNnPc2	Countrum
Promenade	Promenad	k1gInSc5	Promenad
</s>
</p>
<p>
<s>
Promenádní	promenádní	k2eAgNnSc1d1	promenádní
uzavření	uzavření	k1gNnSc1	uzavření
–	–	k?	–
Promenade	Promenad	k1gInSc5	Promenad
Close	Close	k1gFnPc4	Close
</s>
</p>
<p>
<s>
Velký	velký	k2eAgInSc1d1	velký
kruh	kruh	k1gInSc1	kruh
–	–	k?	–
Grand	grand	k1gMnSc1	grand
Circle	Circle	k1gFnSc2	Circle
</s>
</p>
<p>
<s>
Twisty	twist	k1gInPc1	twist
–	–	k?	–
Twists	Twists	k1gInSc1	Twists
</s>
</p>
<p>
<s>
Výdrže	výdrž	k1gFnPc1	výdrž
–	–	k?	–
La	la	k1gNnSc1	la
Passe	Passe	k1gFnSc2	Passe
</s>
</p>
<p>
<s>
Šipky	šipka	k1gFnPc1	šipka
–	–	k?	–
Banderillas	Banderillas	k1gMnSc1	Banderillas
</s>
</p>
<p>
<s>
Spádný	spádný	k2eAgInSc1d1	spádný
zášvih	zášvih	k1gInSc1	zášvih
–	–	k?	–
Ecart	Ecart	k1gInSc1	Ecart
(	(	kIx(	(
<g/>
Fallaway	Fallawaa	k1gFnSc2	Fallawaa
Whisk	Whisk	k1gInSc1	Whisk
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Spádná	spádný	k2eAgFnSc1d1	spádná
otáčka	otáčka	k1gFnSc1	otáčka
vlevo	vlevo	k6eAd1	vlevo
–	–	k?	–
Fallaway	Fallawa	k2eAgFnPc4d1	Fallawa
Reverse	reverse	k1gFnPc4	reverse
</s>
</p>
<p>
<s>
Otevřený	otevřený	k2eAgInSc1d1	otevřený
telemark	telemark	k1gInSc1	telemark
–	–	k?	–
Open	Open	k1gInSc1	Open
Telemark	telemark	k1gInSc1	telemark
</s>
</p>
<p>
<s>
Šátková	šátkový	k2eAgFnSc1d1	Šátková
přeměna	přeměna	k1gFnSc1	přeměna
–	–	k?	–
Chasse	Chasse	k1gFnSc1	Chasse
Cape	capat	k5eAaImIp3nS	capat
</s>
</p>
<p>
<s>
Postupové	postupový	k2eAgFnPc1d1	postupová
spiny	spina	k1gFnPc1	spina
z	z	k7c2	z
promenády	promenáda	k1gFnSc2	promenáda
a	a	k8xC	a
obrácené	obrácený	k2eAgFnPc1d1	obrácená
promenáda	promenáda	k1gFnSc1	promenáda
–	–	k?	–
Travelling	Travelling	k1gInSc1	Travelling
Spins	Spins	k1gInSc1	Spins
from	from	k1gInSc4	from
PP	PP	kA	PP
and	and	k?	and
OPP	OPP	kA	OPP
</s>
</p>
<p>
<s>
Metody	metoda	k1gFnPc1	metoda
změny	změna	k1gFnSc2	změna
chodidla	chodidlo	k1gNnSc2	chodidlo
–	–	k?	–
Metods	Metods	k1gInSc1	Metods
of	of	k?	of
Changing	Changing	k1gInSc1	Changing
Feet	Feet	k1gInSc1	Feet
</s>
</p>
<p>
<s>
výdrž	výdrž	k1gFnSc4	výdrž
na	na	k7c4	na
jednu	jeden	k4xCgFnSc4	jeden
dobu	doba	k1gFnSc4	doba
–	–	k?	–
One	One	k1gFnSc1	One
Beat	beat	k1gInSc1	beat
Hesitation	Hesitation	k1gInSc1	Hesitation
</s>
</p>
<p>
<s>
synkopovaným	synkopovaný	k2eAgMnSc7d1	synkopovaný
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
–	–	k?	–
Syncopated	Syncopated	k1gMnSc1	Syncopated
Sur	Sur	k1gMnSc1	Sur
Place	plac	k1gInSc5	plac
</s>
</p>
<p>
<s>
synkopovaná	synkopovaný	k2eAgFnSc1d1	synkopovaná
přeměna	přeměna	k1gFnSc1	přeměna
–	–	k?	–
Syncopated	Syncopated	k1gInSc4	Syncopated
Chasse	Chasse	k1gFnSc2	Chasse
</s>
</p>
<p>
<s>
variace	variace	k1gFnPc1	variace
na	na	k7c4	na
levou	levý	k2eAgFnSc4d1	levá
nohu	noha	k1gFnSc4	noha
–	–	k?	–
Left	Left	k1gMnSc1	Left
Foot	Foot	k1gMnSc1	Foot
Variation	Variation	k1gInSc4	Variation
</s>
</p>
<p>
<s>
bodnutí	bodnutí	k1gNnSc1	bodnutí
pikou	pikou	k?	pikou
se	s	k7c7	s
změnou	změna	k1gFnSc7	změna
z	z	k7c2	z
Pn	Pn	k1gFnSc2	Pn
na	na	k7c6	na
Ln	Ln	k1gFnSc6	Ln
a	a	k8xC	a
z	z	k7c2	z
Ln	Ln	k1gFnSc2	Ln
na	na	k7c4	na
Pn	Pn	k1gFnPc4	Pn
–	–	k?	–
Coup	coup	k1gInSc1	coup
de	de	k?	de
Pique	Pique	k1gInSc1	Pique
Changing	Changing	k1gInSc1	Changing
from	from	k1gInSc4	from
LF	LF	kA	LF
to	ten	k3xDgNnSc1	ten
RF	RF	kA	RF
and	and	k?	and
RF	RF	kA	RF
to	ten	k3xDgNnSc1	ten
LF	LF	kA	LF
</s>
</p>
<p>
<s>
párové	párový	k2eAgNnSc1d1	párové
bodnutí	bodnutí	k1gNnSc1	bodnutí
pikou	pikou	k?	pikou
–	–	k?	–
Coup	coup	k1gInSc1	coup
de	de	k?	de
Pique	Pique	k1gInSc1	Pique
Couplet	Couplet	k1gInSc1	Couplet
</s>
</p>
<p>
<s>
synkopované	synkopovaný	k2eAgNnSc1d1	synkopovaný
bodnutí	bodnutí	k1gNnSc1	bodnutí
pikou	pikou	k?	pikou
–	–	k?	–
Syncopated	Syncopated	k1gInSc1	Syncopated
Coup	coup	k1gInSc1	coup
de	de	k?	de
Pique	Pique	k1gInSc1	Pique
<g/>
#	#	kIx~	#
</s>
</p>
<p>
<s>
Promenádní	promenádní	k2eAgFnSc1d1	promenádní
spojka	spojka	k1gFnSc1	spojka
–	–	k?	–
Promenade	Promenad	k1gInSc5	Promenad
Link	Linko	k1gNnPc2	Linko
</s>
</p>
<p>
<s>
Bodnutí	bodnutí	k1gNnSc1	bodnutí
pikou	pikou	k?	pikou
–	–	k?	–
Coup	coup	k1gInSc1	coup
de	de	k?	de
PiqueČeský	PiqueČeský	k2eAgInSc1d1	PiqueČeský
svaz	svaz	k1gInSc1	svaz
tanečního	taneční	k2eAgInSc2d1	taneční
sportu	sport	k1gInSc2	sport
Soutěžní	soutěžní	k2eAgInSc1d1	soutěžní
řád	řád	k1gInSc1	řád
ČSTS	ČSTS	kA	ČSTS
</s>
</p>
<p>
<s>
Twistová	Twistový	k2eAgFnSc1d1	Twistový
otáčka	otáčka	k1gFnSc1	otáčka
–	–	k?	–
Twist	twist	k1gInSc1	twist
Turn	Turn	k1gInSc1	Turn
</s>
</p>
<p>
<s>
Španělská	španělský	k2eAgFnSc1d1	španělská
linie	linie	k1gFnSc1	linie
–	–	k?	–
Spanish	Spanish	k1gInSc1	Spanish
Line	linout	k5eAaImIp3nS	linout
</s>
</p>
<p>
<s>
Flamengové	Flamengový	k2eAgInPc1d1	Flamengový
poklepy	poklep	k1gInPc1	poklep
(	(	kIx(	(
<g/>
v	v	k7c6	v
promenádním	promenádní	k2eAgNnSc6d1	promenádní
postavení	postavení	k1gNnSc6	postavení
a	a	k8xC	a
z	z	k7c2	z
postavení	postavení	k1gNnSc2	postavení
obrácené	obrácený	k2eAgFnSc2d1	obrácená
promenády	promenáda	k1gFnSc2	promenáda
<g/>
)	)	kIx)	)
–	–	k?	–
Flamenco	flamenco	k1gNnSc1	flamenco
Taps	Taps	k1gInSc1	Taps
(	(	kIx(	(
<g/>
in	in	k?	in
PP	PP	kA	PP
or	or	k?	or
CPP	CPP	kA	CPP
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sloup	sloup	k1gInSc1	sloup
–	–	k?	–
Farol	Farol	k1gInSc1	Farol
</s>
</p>
<p>
<s>
Fregolina	Fregolin	k2eAgFnSc1d1	Fregolin
–	–	k?	–
Fregolina	Fregolina	k1gFnSc1	Fregolina
</s>
</p>
<p>
<s>
Alternativní	alternativní	k2eAgInPc1d1	alternativní
vchody	vchod	k1gInPc1	vchod
do	do	k7c2	do
Promenádního	promenádní	k2eAgNnSc2d1	promenádní
postavení	postavení	k1gNnSc2	postavení
(	(	kIx(	(
<g/>
Metody	metoda	k1gFnSc2	metoda
1,2	[number]	k4	1,2
<g/>
,3	,3	k4	,3
<g/>
)	)	kIx)	)
–	–	k?	–
Alternative	Alternativ	k1gInSc5	Alternativ
Entries	Entries	k1gMnSc1	Entries
to	ten	k3xDgNnSc4	ten
PP	PP	kA	PP
(	(	kIx(	(
<g/>
Metods	Metods	k1gInSc1	Metods
1,2	[number]	k4	1,2
<g/>
,3	,3	k4	,3
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Landsfeld	Landsfeld	k1gMnSc1	Landsfeld
<g/>
,	,	kIx,	,
Z.	Z.	kA	Z.
<g/>
,	,	kIx,	,
Plamínek	plamínek	k1gInSc1	plamínek
<g/>
,	,	kIx,	,
J.	J.	kA	J.
<g/>
;	;	kIx,	;
Technika	technika	k1gFnSc1	technika
latinskoamerických	latinskoamerický	k2eAgInPc2d1	latinskoamerický
tanců	tanec	k1gInPc2	tanec
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2000	[number]	k4	2000
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Paso	Paso	k6eAd1	Paso
doble	doble	k6eAd1	doble
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Český	český	k2eAgInSc1d1	český
svaz	svaz	k1gInSc1	svaz
tanečního	taneční	k2eAgInSc2d1	taneční
sportu	sport	k1gInSc2	sport
</s>
</p>
<p>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
federace	federace	k1gFnSc1	federace
tanečního	taneční	k2eAgInSc2d1	taneční
sportu	sport	k1gInSc2	sport
(	(	kIx(	(
<g/>
IDSF	IDSF	kA	IDSF
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Světová	světový	k2eAgFnSc1d1	světová
rada	rada	k1gFnSc1	rada
tanečních	taneční	k2eAgMnPc2d1	taneční
profesionálů	profesionál	k1gMnPc2	profesionál
(	(	kIx(	(
<g/>
WDC	WDC	kA	WDC
<g/>
)	)	kIx)	)
</s>
</p>
