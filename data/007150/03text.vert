<s>
Červená	červený	k2eAgFnSc1d1	červená
je	být	k5eAaImIp3nS	být
barva	barva	k1gFnSc1	barva
odpovídající	odpovídající	k2eAgMnSc1d1	odpovídající
nejnižším	nízký	k2eAgFnPc3d3	nejnižší
frekvencím	frekvence	k1gFnPc3	frekvence
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
oko	oko	k1gNnSc1	oko
dokáže	dokázat	k5eAaPmIp3nS	dokázat
vnímat	vnímat	k5eAaImF	vnímat
<g/>
.	.	kIx.	.
</s>
<s>
Červená	červený	k2eAgFnSc1d1	červená
barva	barva	k1gFnSc1	barva
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
základní	základní	k2eAgFnPc4d1	základní
barvy	barva	k1gFnPc4	barva
barevného	barevný	k2eAgNnSc2d1	barevné
spektra	spektrum	k1gNnSc2	spektrum
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
barva	barva	k1gFnSc1	barva
monochromatického	monochromatický	k2eAgNnSc2d1	monochromatické
světla	světlo	k1gNnSc2	světlo
o	o	k7c6	o
vlnové	vlnový	k2eAgFnSc6d1	vlnová
délce	délka	k1gFnSc6	délka
zhruba	zhruba	k6eAd1	zhruba
700	[number]	k4	700
nm	nm	k?	nm
<g/>
.	.	kIx.	.
</s>
<s>
Světlo	světlo	k1gNnSc1	světlo
s	s	k7c7	s
ještě	ještě	k6eAd1	ještě
nižší	nízký	k2eAgFnSc7d2	nižší
frekvencí	frekvence	k1gFnSc7	frekvence
již	již	k6eAd1	již
lidský	lidský	k2eAgInSc1d1	lidský
zrak	zrak	k1gInSc1	zrak
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
vnímat	vnímat	k5eAaImF	vnímat
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
tzv.	tzv.	kA	tzv.
infračervené	infračervený	k2eAgNnSc1d1	infračervené
záření	záření	k1gNnSc1	záření
<g/>
.	.	kIx.	.
</s>
<s>
Červená	červený	k2eAgFnSc1d1	červená
barva	barva	k1gFnSc1	barva
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
základní	základní	k2eAgFnPc4d1	základní
barvy	barva	k1gFnPc4	barva
při	při	k7c6	při
aditivním	aditivní	k2eAgNnSc6d1	aditivní
míchání	míchání	k1gNnSc6	míchání
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
RGB	RGB	kA	RGB
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejím	její	k3xOp3gInSc7	její
doplňkem	doplněk	k1gInSc7	doplněk
je	být	k5eAaImIp3nS	být
azurová	azurový	k2eAgFnSc1d1	azurová
<g/>
.	.	kIx.	.
</s>
<s>
Lidská	lidský	k2eAgFnSc1d1	lidská
krev	krev	k1gFnSc1	krev
(	(	kIx(	(
<g/>
a	a	k8xC	a
mnohých	mnohý	k2eAgNnPc2d1	mnohé
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
červenou	červený	k2eAgFnSc4d1	červená
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
přítomností	přítomnost	k1gFnSc7	přítomnost
hemoglobinu	hemoglobin	k1gInSc2	hemoglobin
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
faktem	fakt	k1gInSc7	fakt
zřejmě	zřejmě	k6eAd1	zřejmě
souvisí	souviset	k5eAaImIp3nS	souviset
další	další	k2eAgFnSc1d1	další
symbolika	symbolika	k1gFnSc1	symbolika
<g/>
:	:	kIx,	:
Červená	červený	k2eAgFnSc1d1	červená
barva	barva	k1gFnSc1	barva
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
hněv	hněv	k1gInSc4	hněv
<g/>
,	,	kIx,	,
např.	např.	kA	např.
ve	v	k7c6	v
rčení	rčení	k1gNnSc6	rčení
vidět	vidět	k5eAaImF	vidět
rudě	rudě	k6eAd1	rudě
<g/>
.	.	kIx.	.
</s>
<s>
Planeta	planeta	k1gFnSc1	planeta
Mars	Mars	k1gInSc1	Mars
má	mít	k5eAaImIp3nS	mít
načervenalou	načervenalý	k2eAgFnSc4d1	načervenalá
barvu	barva	k1gFnSc4	barva
(	(	kIx(	(
<g/>
kvůli	kvůli	k7c3	kvůli
přítomnosti	přítomnost	k1gFnSc3	přítomnost
oxidů	oxid	k1gInPc2	oxid
železa	železo	k1gNnSc2	železo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
kterou	který	k3yIgFnSc4	který
byl	být	k5eAaImAgMnS	být
asociován	asociován	k2eAgMnSc1d1	asociován
s	s	k7c7	s
bohem	bůh	k1gMnSc7	bůh
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
Martem	Mars	k1gMnSc7	Mars
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
křesťanství	křesťanství	k1gNnSc6	křesťanství
je	být	k5eAaImIp3nS	být
červená	červený	k2eAgFnSc1d1	červená
barva	barva	k1gFnSc1	barva
symbolem	symbol	k1gInSc7	symbol
prolité	prolitý	k2eAgFnSc2d1	prolitá
krve	krev	k1gFnSc2	krev
mučedníků	mučedník	k1gMnPc2	mučedník
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
liturgická	liturgický	k2eAgFnSc1d1	liturgická
barva	barva	k1gFnSc1	barva
při	při	k7c6	při
jejich	jejich	k3xOp3gFnPc6	jejich
památkách	památka	k1gFnPc6	památka
<g/>
.	.	kIx.	.
</s>
<s>
Historicky	historicky	k6eAd1	historicky
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
tradici	tradice	k1gFnSc6	tradice
antického	antický	k2eAgInSc2d1	antický
Říma	Řím	k1gInSc2	Řím
je	být	k5eAaImIp3nS	být
červená	červený	k2eAgFnSc1d1	červená
či	či	k8xC	či
purpurová	purpurový	k2eAgFnSc1d1	purpurová
barva	barva	k1gFnSc1	barva
chápána	chápat	k5eAaImNgFnS	chápat
jako	jako	k9	jako
barva	barva	k1gFnSc1	barva
královská	královský	k2eAgFnSc1d1	královská
či	či	k8xC	či
slavnostní	slavnostní	k2eAgFnSc1d1	slavnostní
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
též	též	k9	též
liturgické	liturgický	k2eAgNnSc1d1	liturgické
užití	užití	k1gNnSc1	užití
červené	červený	k2eAgFnSc2d1	červená
barvy	barva	k1gFnSc2	barva
při	při	k7c6	při
velkých	velký	k2eAgInPc6d1	velký
svátcích	svátek	k1gInPc6	svátek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
nejsou	být	k5eNaImIp3nP	být
oslavami	oslava	k1gFnPc7	oslava
mučedníků	mučedník	k1gMnPc2	mučedník
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Květná	květný	k2eAgFnSc1d1	Květná
neděle	neděle	k1gFnSc1	neděle
<g/>
,	,	kIx,	,
Velký	velký	k2eAgInSc1d1	velký
pátek	pátek	k1gInSc4	pátek
<g/>
,	,	kIx,	,
Seslání	seslání	k1gNnSc4	seslání
Ducha	duch	k1gMnSc2	duch
svatého	svatý	k2eAgMnSc2d1	svatý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Červená	červený	k2eAgFnSc1d1	červená
barva	barva	k1gFnSc1	barva
je	být	k5eAaImIp3nS	být
výrazná	výrazný	k2eAgFnSc1d1	výrazná
<g/>
,	,	kIx,	,
chápe	chápat	k5eAaImIp3nS	chápat
se	se	k3xPyFc4	se
jako	jako	k9	jako
varování	varování	k1gNnSc1	varování
či	či	k8xC	či
jiná	jiný	k2eAgFnSc1d1	jiná
důležitá	důležitý	k2eAgFnSc1d1	důležitá
zpráva	zpráva	k1gFnSc1	zpráva
<g/>
.	.	kIx.	.
</s>
<s>
Červená	červený	k2eAgFnSc1d1	červená
barva	barva	k1gFnSc1	barva
na	na	k7c6	na
semaforu	semafor	k1gInSc6	semafor
(	(	kIx(	(
<g/>
i	i	k9	i
na	na	k7c6	na
jiných	jiný	k2eAgNnPc6d1	jiné
místech	místo	k1gNnPc6	místo
<g/>
)	)	kIx)	)
znamená	znamenat	k5eAaImIp3nS	znamenat
stůj	stát	k5eAaImRp2nS	stát
<g/>
.	.	kIx.	.
</s>
<s>
Červená	červený	k2eAgFnSc1d1	červená
barva	barva	k1gFnSc1	barva
pozičního	poziční	k2eAgNnSc2d1	poziční
světla	světlo	k1gNnSc2	světlo
na	na	k7c6	na
letadlech	letadlo	k1gNnPc6	letadlo
a	a	k8xC	a
lodích	loď	k1gFnPc6	loď
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
označení	označení	k1gNnSc3	označení
levé	levý	k2eAgFnSc2d1	levá
strany	strana	k1gFnSc2	strana
(	(	kIx(	(
<g/>
port	port	k1gInSc1	port
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Červená	červený	k2eAgFnSc1d1	červená
barva	barva	k1gFnSc1	barva
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
lásku	láska	k1gFnSc4	láska
či	či	k8xC	či
erotiku	erotika	k1gFnSc4	erotika
(	(	kIx(	(
<g/>
červená	červený	k2eAgFnSc1d1	červená
knihovna	knihovna	k1gFnSc1	knihovna
<g/>
,	,	kIx,	,
červené	červený	k2eAgFnPc1d1	červená
lucerny	lucerna	k1gFnPc1	lucerna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Červená	červený	k2eAgFnSc1d1	červená
barva	barva	k1gFnSc1	barva
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
socialisty	socialist	k1gMnPc7	socialist
či	či	k8xC	či
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
<g/>
)	)	kIx)	)
komunisty	komunista	k1gMnPc7	komunista
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čínské	čínský	k2eAgFnSc6d1	čínská
tradici	tradice	k1gFnSc6	tradice
je	být	k5eAaImIp3nS	být
červená	červený	k2eAgFnSc1d1	červená
barvou	barva	k1gFnSc7	barva
štěstí	štěstit	k5eAaImIp3nS	štěstit
<g/>
.	.	kIx.	.
</s>
<s>
Červenou	červený	k2eAgFnSc7d1	červená
barvou	barva	k1gFnSc7	barva
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
účetnictví	účetnictví	k1gNnSc6	účetnictví
vyznačen	vyznačen	k2eAgInSc1d1	vyznačen
dluh	dluh	k1gInSc1	dluh
<g/>
,	,	kIx,	,
ztráta	ztráta	k1gFnSc1	ztráta
(	(	kIx(	(
<g/>
výraz	výraz	k1gInSc1	výraz
v	v	k7c6	v
červených	červený	k2eAgNnPc6d1	červené
číslech	číslo	k1gNnPc6	číslo
znamená	znamenat	k5eAaImIp3nS	znamenat
ve	v	k7c6	v
ztrátě	ztráta	k1gFnSc6	ztráta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
barevném	barevný	k2eAgNnSc6d1	barevné
značení	značení	k1gNnSc6	značení
odporů	odpor	k1gInPc2	odpor
znamená	znamenat	k5eAaImIp3nS	znamenat
červená	červený	k2eAgFnSc1d1	červená
barva	barva	k1gFnSc1	barva
číslici	číslice	k1gFnSc4	číslice
2	[number]	k4	2
nebo	nebo	k8xC	nebo
toleranci	tolerance	k1gFnSc4	tolerance
±	±	k?	±
<g/>
2	[number]	k4	2
<g/>
%	%	kIx~	%
</s>
<s>
Červená	červený	k2eAgFnSc1d1	červená
barva	barva	k1gFnSc1	barva
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
také	také	k9	také
ocenění	ocenění	k1gNnSc1	ocenění
nebo	nebo	k8xC	nebo
uznání	uznání	k1gNnSc1	uznání
<g/>
:	:	kIx,	:
vysokoškolský	vysokoškolský	k2eAgInSc4d1	vysokoškolský
diplom	diplom	k1gInSc4	diplom
studentů	student	k1gMnPc2	student
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
prospěli	prospět	k5eAaPmAgMnP	prospět
s	s	k7c7	s
vyznamenáním	vyznamenání	k1gNnSc7	vyznamenání
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
červené	červený	k2eAgFnPc4d1	červená
desky	deska	k1gFnPc4	deska
(	(	kIx(	(
<g/>
červený	červený	k2eAgInSc1d1	červený
diplom	diplom	k1gInSc1	diplom
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Červená	červený	k2eAgFnSc1d1	červená
barva	barva	k1gFnSc1	barva
je	být	k5eAaImIp3nS	být
takzvanou	takzvaný	k2eAgFnSc7d1	takzvaná
barvou	barva	k1gFnSc7	barva
muže	muž	k1gMnSc2	muž
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
znamená	znamenat	k5eAaImIp3nS	znamenat
krev	krev	k1gFnSc4	krev
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
válku	válka	k1gFnSc4	válka
<g/>
.	.	kIx.	.
</s>
<s>
Červená	červený	k2eAgFnSc1d1	červená
barva	barva	k1gFnSc1	barva
je	být	k5eAaImIp3nS	být
dále	daleko	k6eAd2	daleko
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
barvu	barva	k1gFnSc4	barva
revoluce	revoluce	k1gFnSc2	revoluce
a	a	k8xC	a
změn	změna	k1gFnPc2	změna
systému	systém	k1gInSc2	systém
obecně	obecně	k6eAd1	obecně
<g/>
.	.	kIx.	.
</s>
<s>
Červenou	červený	k2eAgFnSc7d1	červená
barvou	barva	k1gFnSc7	barva
jsou	být	k5eAaImIp3nP	být
označeny	označit	k5eAaPmNgInP	označit
vodovodní	vodovodní	k2eAgInPc1d1	vodovodní
kohoutky	kohoutek	k1gInPc1	kohoutek
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
teče	téct	k5eAaImIp3nS	téct
horká	horký	k2eAgFnSc1d1	horká
voda	voda	k1gFnSc1	voda
<g/>
.	.	kIx.	.
</s>
<s>
Červená	červený	k2eAgFnSc1d1	červená
je	být	k5eAaImIp3nS	být
také	také	k9	také
jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
čtyř	čtyři	k4xCgFnPc2	čtyři
základních	základní	k2eAgFnPc2d1	základní
barev	barva	k1gFnPc2	barva
na	na	k7c6	na
mariášových	mariášový	k2eAgFnPc6d1	mariášová
kartách	karta	k1gFnPc6	karta
a	a	k8xC	a
tarokových	tarokový	k2eAgFnPc6d1	taroková
kartách	karta	k1gFnPc6	karta
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
znázorněna	znázornit	k5eAaPmNgFnS	znázornit
červenými	červený	k2eAgInPc7d1	červený
srdíčky	srdíčko	k1gNnPc7	srdíčko
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
ale	ale	k9	ale
i	i	k9	i
u	u	k7c2	u
hracích	hrací	k2eAgFnPc2d1	hrací
karet	kareta	k1gFnPc2	kareta
francouzského	francouzský	k2eAgInSc2d1	francouzský
a	a	k8xC	a
španělského	španělský	k2eAgInSc2d1	španělský
typu	typ	k1gInSc2	typ
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
červená	červené	k1gNnPc1	červené
srdce	srdce	k1gNnSc1	srdce
a	a	k8xC	a
červená	červený	k2eAgFnSc1d1	červená
kára	kára	k1gFnSc1	kára
<g/>
.	.	kIx.	.
</s>
<s>
Červená	červený	k2eAgFnSc1d1	červená
řeka	řeka	k1gFnSc1	řeka
Červené	Červená	k1gFnSc2	Červená
blato	blat	k2eAgNnSc4d1	blato
Červené	Červené	k2eAgNnSc4d1	Červené
jezero	jezero	k1gNnSc4	jezero
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
červená	červenat	k5eAaImIp3nS	červenat
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
červený	červený	k2eAgMnSc1d1	červený
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
