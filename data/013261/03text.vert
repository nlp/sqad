<p>
<s>
Město	město	k1gNnSc1	město
malomocného	malomocný	k1gMnSc2	malomocný
krále	král	k1gMnSc2	král
(	(	kIx(	(
<g/>
1904	[number]	k4	1904
<g/>
,	,	kIx,	,
La	la	k1gNnSc1	la
citta	citt	k1gInSc2	citt
del	del	k?	del
re	re	k9	re
lebbroso	lebbrosa	k1gFnSc5	lebbrosa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
dobrodružný	dobrodružný	k2eAgInSc1d1	dobrodružný
román	román	k1gInSc1	román
italského	italský	k2eAgMnSc2d1	italský
spisovatele	spisovatel	k1gMnSc2	spisovatel
Emilia	Emilius	k1gMnSc2	Emilius
Salgariho	Salgari	k1gMnSc2	Salgari
z	z	k7c2	z
prostředí	prostředí	k1gNnSc2	prostředí
siamského	siamský	k2eAgInSc2d1	siamský
dvora	dvůr	k1gInSc2	dvůr
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obsah	obsah	k1gInSc1	obsah
románu	román	k1gInSc2	román
==	==	k?	==
</s>
</p>
<p>
<s>
Královský	královský	k2eAgMnSc1d1	královský
ministr	ministr	k1gMnSc1	ministr
a	a	k8xC	a
generál	generál	k1gMnSc1	generál
Lakon-tay	Lakonaa	k1gFnSc2	Lakon-taa
<g/>
,	,	kIx,	,
pověřený	pověřený	k2eAgMnSc1d1	pověřený
péčí	péče	k1gFnSc7	péče
o	o	k7c4	o
posvátné	posvátný	k2eAgMnPc4d1	posvátný
bílé	bílý	k2eAgMnPc4d1	bílý
slony	slon	k1gMnPc4	slon
<g/>
,	,	kIx,	,
upadne	upadnout	k5eAaPmIp3nS	upadnout
v	v	k7c4	v
královu	králův	k2eAgFnSc4d1	králova
nemilost	nemilost	k1gFnSc4	nemilost
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
jediném	jediný	k2eAgInSc6d1	jediný
měsíci	měsíc	k1gInSc6	měsíc
zahyne	zahynout	k5eAaPmIp3nS	zahynout
sedm	sedm	k4xCc1	sedm
slonů	slon	k1gMnPc2	slon
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
uvěří	uvěřit	k5eAaPmIp3nS	uvěřit
podlému	podlý	k2eAgMnSc3d1	podlý
našeptávačovi	našeptávač	k1gMnSc3	našeptávač
<g/>
,	,	kIx,	,
královskému	královský	k2eAgMnSc3d1	královský
katovi	kat	k1gMnSc3	kat
Mien-Mingovi	Mien-Ming	k1gMnSc3	Mien-Ming
a	a	k8xC	a
odmítnutému	odmítnutý	k2eAgMnSc3d1	odmítnutý
nápadníkovi	nápadník	k1gMnSc3	nápadník
Lakon-tayovy	Lakonayův	k2eAgFnSc2d1	Lakon-tayův
dcery	dcera	k1gFnSc2	dcera
Len-Pry	Len-Pra	k1gFnSc2	Len-Pra
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
chce	chtít	k5eAaImIp3nS	chtít
pomstít	pomstít	k5eAaPmF	pomstít
<g/>
,	,	kIx,	,
a	a	k8xC	a
vyšle	vyslat	k5eAaPmIp3nS	vyslat
ministra	ministr	k1gMnSc4	ministr
pro	pro	k7c4	pro
zlatý	zlatý	k2eAgInSc4d1	zlatý
bodec	bodec	k1gInSc4	bodec
na	na	k7c4	na
slony	slon	k1gMnPc4	slon
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
jedné	jeden	k4xCgFnSc2	jeden
dávné	dávný	k2eAgFnSc2d1	dávná
pověsti	pověst	k1gFnSc2	pověst
ukryt	ukrýt	k5eAaPmNgInS	ukrýt
v	v	k7c6	v
dalekých	daleký	k2eAgFnPc6d1	daleká
zříceninách	zřícenina	k1gFnPc6	zřícenina
opuštěného	opuštěný	k2eAgNnSc2d1	opuštěné
města	město	k1gNnSc2	město
malomocného	malomocný	k1gMnSc2	malomocný
krále	král	k1gMnSc2	král
(	(	kIx(	(
<g/>
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
tohoto	tento	k3xDgInSc2	tento
bodce	bodec	k1gInSc2	bodec
chce	chtít	k5eAaImIp3nS	chtít
pak	pak	k9	pak
král	král	k1gMnSc1	král
získávat	získávat	k5eAaImF	získávat
nové	nový	k2eAgMnPc4d1	nový
bílé	bílý	k2eAgMnPc4d1	bílý
slony	slon	k1gMnPc4	slon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Len-Pra	Len-Pra	k6eAd1	Len-Pra
se	se	k3xPyFc4	se
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
pojede	jet	k5eAaImIp3nS	jet
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
otcem	otec	k1gMnSc7	otec
<g/>
.	.	kIx.	.
</s>
<s>
Doprovází	doprovázet	k5eAaImIp3nS	doprovázet
je	on	k3xPp3gNnSc4	on
italský	italský	k2eAgMnSc1d1	italský
lékař	lékař	k1gMnSc1	lékař
Roberto	Roberta	k1gFnSc5	Roberta
Galeno	Galen	k2eAgNnSc1d1	Galeno
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
miluje	milovat	k5eAaImIp3nS	milovat
Len-Pru	Len-Pra	k1gFnSc4	Len-Pra
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
jeho	jeho	k3xOp3gFnSc4	jeho
lásku	láska	k1gFnSc4	láska
opětuje	opětovat	k5eAaImIp3nS	opětovat
<g/>
.	.	kIx.	.
</s>
<s>
Mien-Ming	Mien-Ming	k1gInSc1	Mien-Ming
sežene	sehnat	k5eAaPmIp3nS	sehnat
bandu	banda	k1gFnSc4	banda
domorodých	domorodý	k2eAgMnPc2d1	domorodý
zloduchů	zloduch	k1gMnPc2	zloduch
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
neštítí	štítit	k5eNaImIp3nS	štítit
ničeho	nic	k3yNnSc2	nic
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
ministr	ministr	k1gMnSc1	ministr
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
věnoval	věnovat	k5eAaPmAgMnS	věnovat
slonům	slon	k1gMnPc3	slon
všechnu	všechen	k3xTgFnSc4	všechen
péči	péče	k1gFnSc4	péče
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
nevinen	vinen	k2eNgMnSc1d1	nevinen
<g/>
,	,	kIx,	,
pachatele	pachatel	k1gMnSc4	pachatel
násilné	násilný	k2eAgFnSc2d1	násilná
smrti	smrt	k1gFnSc2	smrt
posvátných	posvátný	k2eAgNnPc2d1	posvátné
zvířat	zvíře	k1gNnPc2	zvíře
odhalí	odhalit	k5eAaPmIp3nS	odhalit
<g/>
.	.	kIx.	.
</s>
<s>
Mien-Ming	Mien-Ming	k1gInSc1	Mien-Ming
je	být	k5eAaImIp3nS	být
nakonec	nakonec	k6eAd1	nakonec
zajat	zajat	k2eAgMnSc1d1	zajat
a	a	k8xC	a
při	při	k7c6	při
pokusu	pokus	k1gInSc6	pokus
probodnout	probodnout	k5eAaPmF	probodnout
krásnou	krásný	k2eAgFnSc4d1	krásná
Len-Pru	Len-Pra	k1gFnSc4	Len-Pra
i	i	k8xC	i
zastřelen	zastřelit	k5eAaPmNgMnS	zastřelit
<g/>
.	.	kIx.	.
</s>
<s>
Siamský	siamský	k2eAgMnSc1d1	siamský
král	král	k1gMnSc1	král
generála	generál	k1gMnSc2	generál
omilostní	omilostnit	k5eAaPmIp3nS	omilostnit
a	a	k8xC	a
doktor	doktor	k1gMnSc1	doktor
Galeno	Galena	k1gFnSc5	Galena
si	se	k3xPyFc3	se
vezme	vzít	k5eAaPmIp3nS	vzít
generálovu	generálův	k2eAgFnSc4d1	generálova
dceru	dcera	k1gFnSc4	dcera
za	za	k7c4	za
ženu	žena	k1gFnSc4	žena
<g/>
...	...	k?	...
</s>
</p>
<p>
<s>
==	==	k?	==
Česká	český	k2eAgNnPc1d1	české
vydání	vydání	k1gNnPc1	vydání
==	==	k?	==
</s>
</p>
<p>
<s>
Město	město	k1gNnSc1	město
malomocného	malomocný	k1gMnSc2	malomocný
krále	král	k1gMnSc2	král
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
Hynek	Hynek	k1gMnSc1	Hynek
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1925	[number]	k4	1925
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Jindřich	Jindřich	k1gMnSc1	Jindřich
Entlicher	Entlichra	k1gFnPc2	Entlichra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Město	město	k1gNnSc1	město
malomocného	malomocný	k1gMnSc2	malomocný
krále	král	k1gMnSc2	král
<g/>
,	,	kIx,	,
Albatros	albatros	k1gMnSc1	albatros
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1974	[number]	k4	1974
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Vladimír	Vladimír	k1gMnSc1	Vladimír
Henzl	Henzl	k1gMnSc1	Henzl
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
Hanácké	hanácký	k2eAgNnSc1d1	Hanácké
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
Vyškov	Vyškov	k1gInSc1	Vyškov
1992	[number]	k4	1992
a	a	k8xC	a
Albatros	albatros	k1gMnSc1	albatros
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
http://www.kodovky.cz/kniha/132	[url]	k4	http://www.kodovky.cz/kniha/132
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
http://www.kodovky.cz/kniha/197	[url]	k4	http://www.kodovky.cz/kniha/197
</s>
</p>
