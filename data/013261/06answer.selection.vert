<s>
Město	město	k1gNnSc1	město
malomocného	malomocný	k1gMnSc2	malomocný
krále	král	k1gMnSc2	král
(	(	kIx(	(
<g/>
1904	[number]	k4	1904
<g/>
,	,	kIx,	,
La	la	k1gNnSc1	la
citta	citt	k1gInSc2	citt
del	del	k?	del
re	re	k9	re
lebbroso	lebbrosa	k1gFnSc5	lebbrosa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
dobrodružný	dobrodružný	k2eAgInSc1d1	dobrodružný
román	román	k1gInSc1	román
italského	italský	k2eAgMnSc2d1	italský
spisovatele	spisovatel	k1gMnSc2	spisovatel
Emilia	Emilius	k1gMnSc2	Emilius
Salgariho	Salgari	k1gMnSc2	Salgari
z	z	k7c2	z
prostředí	prostředí	k1gNnSc2	prostředí
siamského	siamský	k2eAgInSc2d1	siamský
dvora	dvůr	k1gInSc2	dvůr
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
