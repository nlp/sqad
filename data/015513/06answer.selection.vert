<s>
Avtomat	Avtomat	k1gInSc1
Kalašnikova	kalašnikov	k1gInSc2
obrazca	obrazca	k6eAd1
47	#num#	k4
(	(	kIx(
<g/>
А	А	k?
К	К	k?
о	о	k?
1947	#num#	k4
г	г	k?
<g/>
,	,	kIx,
do	do	k7c2
češtiny	čeština	k1gFnSc2
přeložitelné	přeložitelný	k2eAgFnPc1d1
jako	jako	k8xC,k8xS
Kalašnikovův	Kalašnikovův	k2eAgInSc1d1
automat	automat	k1gInSc1
vzor	vzor	k1gInSc1
1947	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
známý	známý	k2eAgMnSc1d1
pod	pod	k7c7
zkratkou	zkratka	k1gFnSc7
AK-47	AK-47	k1gFnSc2
nebo	nebo	k8xC
zjednodušeně	zjednodušeně	k6eAd1
jako	jako	k9
kalašnikov	kalašnikov	k1gInSc1
,	,	kIx,
je	být	k5eAaImIp3nS
nejpoužívanější	používaný	k2eAgFnSc1d3
a	a	k8xC
jedna	jeden	k4xCgFnSc1
z	z	k7c2
nejznámějších	známý	k2eAgFnPc2d3
útočných	útočný	k2eAgFnPc2d1
pušek	puška	k1gFnPc2
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>