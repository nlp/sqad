<s>
AK-47	AK-47	k4
</s>
<s>
Na	na	k7c4
tento	tento	k3xDgInSc4
článek	článek	k1gInSc4
je	být	k5eAaImIp3nS
přesměrováno	přesměrován	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Kalašnikov	kalašnikov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
útočné	útočný	k2eAgFnSc6d1
pušce	puška	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
jejím	její	k3xOp3gMnSc6
konstruktérovi	konstruktér	k1gMnSc6
pojednává	pojednávat	k5eAaImIp3nS
článek	článek	k1gInSc1
Michail	Michail	k1gMnSc1
Kalašnikov	kalašnikov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
není	být	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
ozdrojován	ozdrojován	k2eAgMnSc1d1
a	a	k8xC
může	moct	k5eAaImIp3nS
tedy	tedy	k9
obsahovat	obsahovat	k5eAaImF
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yRgFnPc4,k3yIgFnPc4,k3yQgFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
ověřit	ověřit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
Jste	být	k5eAaImIp2nP
<g/>
-li	-li	k?
s	s	k7c7
popisovaným	popisovaný	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
seznámeni	seznámen	k2eAgMnPc1d1
<g/>
,	,	kIx,
pomozte	pomoct	k5eAaPmRp2nPwC
doložit	doložit	k5eAaPmF
uvedená	uvedený	k2eAgNnPc4d1
tvrzení	tvrzení	k1gNnPc4
doplněním	doplnění	k1gNnSc7
referencí	reference	k1gFnPc2
na	na	k7c4
věrohodné	věrohodný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
AK-47	AK-47	k4
AK-47	AK-47	k1gFnPc2
druhé	druhý	k4xOgFnSc2
výrobní	výrobní	k2eAgFnSc2d1
varianty	varianta	k1gFnSc2
(	(	kIx(
<g/>
Typ	typ	k1gInSc1
2	#num#	k4
<g/>
)	)	kIx)
<g/>
Typ	typ	k1gInSc1
</s>
<s>
útočná	útočný	k2eAgFnSc1d1
puška	puška	k1gFnSc1
Místo	místo	k7c2
původu	původ	k1gInSc2
</s>
<s>
SSSR	SSSR	kA
Historie	historie	k1gFnSc1
služby	služba	k1gFnSc2
Ve	v	k7c6
službě	služba	k1gFnSc6
</s>
<s>
1949	#num#	k4
–	–	k?
dosud	dosud	k6eAd1
1949	#num#	k4
–	–	k?
80	#num#	k4
<g/>
.	.	kIx.
léta	léto	k1gNnSc2
(	(	kIx(
<g/>
SSSR	SSSR	kA
<g/>
)	)	kIx)
Používána	používán	k2eAgFnSc1d1
</s>
<s>
celosvětově	celosvětově	k6eAd1
Války	válek	k1gInPc1
</s>
<s>
válka	válka	k1gFnSc1
ve	v	k7c6
Vietnamu	Vietnam	k1gInSc6
<g/>
,	,	kIx,
válka	válka	k1gFnSc1
v	v	k7c6
Zálivu	záliv	k1gInSc6
<g/>
,	,	kIx,
Sovětská	sovětský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
v	v	k7c6
Afghánistánu	Afghánistán	k1gInSc6
<g/>
,	,	kIx,
operace	operace	k1gFnPc1
Lité	litý	k2eAgNnSc1d1
olovo	olovo	k1gNnSc1
(	(	kIx(
<g/>
Hamás	Hamás	k1gInSc1
<g/>
)	)	kIx)
atd.	atd.	kA
Historie	historie	k1gFnSc1
výroby	výroba	k1gFnSc2
Konstruktér	konstruktér	k1gMnSc1
</s>
<s>
Michail	Michait	k5eAaPmAgInS,k5eAaImAgInS,k5eAaBmAgInS
Kalašnikov	kalašnikov	k1gInSc1
Navrženo	navržen	k2eAgNnSc1d1
</s>
<s>
1946	#num#	k4
<g/>
–	–	k?
<g/>
1948	#num#	k4
Výrobce	výrobce	k1gMnSc2
</s>
<s>
Ižmaš	Ižmaš	k1gMnSc1
a	a	k8xC
další	další	k2eAgMnPc1d1
včetně	včetně	k7c2
Norinco	Norinco	k6eAd1
Výroba	výroba	k1gFnSc1
</s>
<s>
1949	#num#	k4
<g/>
–	–	k?
<g/>
1959	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Vyrobeno	vyrobit	k5eAaPmNgNnS
kusů	kus	k1gInPc2
</s>
<s>
cca	cca	kA
100	#num#	k4
mil	míle	k1gFnPc2
<g/>
.	.	kIx.
všech	všecek	k3xTgFnPc2
verzí	verze	k1gFnPc2
Základní	základní	k2eAgInPc1d1
údaje	údaj	k1gInPc1
Hmotnost	hmotnost	k1gFnSc1
</s>
<s>
4.3	4.3	k4
kg	kg	kA
Délka	délka	k1gFnSc1
</s>
<s>
870	#num#	k4
mm	mm	kA
Délka	délka	k1gFnSc1
hlavně	hlavně	k9
</s>
<s>
415	#num#	k4
mm	mm	kA
Typ	typ	k1gInSc4
náboje	náboj	k1gInSc2
</s>
<s>
7,62	7,62	k4
×	×	k?
39	#num#	k4
mm	mm	kA
Kadence	kadence	k1gFnSc2
</s>
<s>
600	#num#	k4
ran	rána	k1gFnPc2
<g/>
/	/	kIx~
<g/>
min	min	kA
Úsťová	úsťový	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
</s>
<s>
715	#num#	k4
m	m	kA
<g/>
/	/	kIx~
<g/>
s	s	k7c7
Účinný	účinný	k2eAgMnSc5d1
dostřel	dostřelit	k5eAaPmRp2nS
</s>
<s>
100	#num#	k4
<g/>
–	–	k?
<g/>
800	#num#	k4
m	m	kA
Maximální	maximální	k2eAgInSc4d1
dostřel	dostřel	k1gInSc4
</s>
<s>
2800	#num#	k4
m	m	kA
Zásobování	zásobování	k1gNnSc1
municí	munice	k1gFnSc7
</s>
<s>
schránkový	schránkový	k2eAgInSc1d1
zásobník	zásobník	k1gInSc1
na	na	k7c4
30	#num#	k4
nábojů	náboj	k1gInPc2
</s>
<s>
Avtomat	Avtomat	k1gInSc1
Kalašnikova	kalašnikov	k1gInSc2
obrazca	obrazca	k6eAd1
47	#num#	k4
(	(	kIx(
<g/>
А	А	k?
К	К	k?
о	о	k?
1947	#num#	k4
г	г	k?
<g/>
,	,	kIx,
do	do	k7c2
češtiny	čeština	k1gFnSc2
přeložitelné	přeložitelný	k2eAgFnPc1d1
jako	jako	k8xC,k8xS
Kalašnikovův	Kalašnikovův	k2eAgInSc1d1
automat	automat	k1gInSc1
vzor	vzor	k1gInSc1
1947	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
známý	známý	k2eAgMnSc1d1
pod	pod	k7c7
zkratkou	zkratka	k1gFnSc7
AK-47	AK-47	k1gFnSc2
nebo	nebo	k8xC
zjednodušeně	zjednodušeně	k6eAd1
jako	jako	k9
kalašnikov	kalašnikov	k1gInSc1
,	,	kIx,
je	být	k5eAaImIp3nS
nejpoužívanější	používaný	k2eAgFnSc1d3
a	a	k8xC
jedna	jeden	k4xCgFnSc1
z	z	k7c2
nejznámějších	známý	k2eAgFnPc2d3
útočných	útočný	k2eAgFnPc2d1
pušek	puška	k1gFnPc2
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sestrojena	sestrojen	k2eAgFnSc1d1
byla	být	k5eAaImAgFnS
v	v	k7c6
SSSR	SSSR	kA
mladým	mladý	k2eAgMnSc7d1
sovětským	sovětský	k2eAgMnSc7d1
konstruktérem	konstruktér	k1gMnSc7
Michailem	Michail	k1gMnSc7
Timofejevičem	Timofejevič	k1gMnSc7
Kalašnikovem	kalašnikov	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
při	při	k7c6
návrhu	návrh	k1gInSc6
zbraně	zbraň	k1gFnSc2
přebíral	přebírat	k5eAaImAgMnS
prvky	prvek	k1gInPc4
z	z	k7c2
celé	celý	k2eAgFnSc2d1
řady	řada	k1gFnSc2
starších	starý	k2eAgFnPc2d2
i	i	k8xC
novějších	nový	k2eAgFnPc2d2
automatických	automatický	k2eAgFnPc2d1
a	a	k8xC
útočných	útočný	k2eAgFnPc2d1
pušek	puška	k1gFnPc2
<g/>
,	,	kIx,
zejména	zejména	k9
konkurenčního	konkurenční	k2eAgInSc2d1
návrhu	návrh	k1gInSc2
konstruktéra	konstruktér	k1gMnSc4
Bulkina	Bulkin	k1gMnSc4
TKB-	TKB-	k1gMnSc4
<g/>
415	#num#	k4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
dále	daleko	k6eAd2
samonabíjecích	samonabíjecí	k2eAgFnPc2d1
pušek	puška	k1gFnPc2
SKS	SKS	kA
a	a	k8xC
SVT	SVT	kA
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
německé	německý	k2eAgFnSc2d1
útočné	útočný	k2eAgFnSc2d1
<g />
.	.	kIx.
</s>
<s hack="1">
pušky	puška	k1gFnSc2
StG	StG	k1gFnSc1
44	#num#	k4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
ale	ale	k8xC
i	i	k9
české	český	k2eAgFnSc2d1
pušky	puška	k1gFnSc2
ZH	ZH	kA
29	#num#	k4
<g/>
,	,	kIx,
americké	americký	k2eAgFnSc6d1
Remington	remington	k1gInSc4
Model	model	k1gInSc1
8	#num#	k4
<g/>
,	,	kIx,
sovětské	sovětský	k2eAgFnSc6d1
AS-	AS-	k1gFnSc6
<g/>
44	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Jeho	jeho	k3xOp3gFnSc1
konstrukce	konstrukce	k1gFnSc1
je	být	k5eAaImIp3nS
však	však	k9
původní	původní	k2eAgMnSc1d1
a	a	k8xC
v	v	k7c6
mnohém	mnohé	k1gNnSc6
je	být	k5eAaImIp3nS
překonává	překonávat	k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Projektové	projektový	k2eAgFnPc1d1
práce	práce	k1gFnPc1
na	na	k7c6
AK-47	AK-47	k1gFnSc6
začaly	začít	k5eAaPmAgFnP
v	v	k7c6
posledním	poslední	k2eAgInSc6d1
roce	rok	k1gInSc6
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
(	(	kIx(
<g/>
1945	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1946	#num#	k4
byl	být	k5eAaImAgMnS
AK-47	AK-47	k1gFnSc4
podroben	podroben	k2eAgMnSc1d1
oficiálním	oficiální	k2eAgFnPc3d1
vojenským	vojenský	k2eAgFnPc3d1
zkouškám	zkouška	k1gFnPc3
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1948	#num#	k4
byla	být	k5eAaImAgFnS
verze	verze	k1gFnSc1
s	s	k7c7
pevnou	pevný	k2eAgFnSc7d1
pažbou	pažba	k1gFnSc7
uvedena	uvést	k5eAaPmNgFnS
do	do	k7c2
aktivní	aktivní	k2eAgFnSc2d1
služby	služba	k1gFnSc2
ve	v	k7c6
vybraných	vybraný	k2eAgFnPc6d1
jednotkách	jednotka	k1gFnPc6
Sovětské	sovětský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeden	jeden	k4xCgInSc1
z	z	k7c2
prvních	první	k4xOgInPc2
návrhů	návrh	k1gInPc2
konstrukce	konstrukce	k1gFnSc2
byl	být	k5eAaImAgInS
AKS	AKS	kA
(	(	kIx(
<g/>
S-Skladnoj	S-Skladnoj	k1gInSc1
nebo	nebo	k8xC
"	"	kIx"
<g/>
skládací	skládací	k2eAgMnSc1d1
<g/>
"	"	kIx"
<g/>
)	)	kIx)
vybavený	vybavený	k2eAgInSc1d1
sklopnou	sklopný	k2eAgFnSc7d1
kovovou	kovový	k2eAgFnSc7d1
ramenní	ramenní	k2eAgFnSc7d1
opěrkou	opěrka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1949	#num#	k4
byl	být	k5eAaImAgMnS
AK-47	AK-47	k1gFnSc4
oficiálně	oficiálně	k6eAd1
přijat	přijmout	k5eAaPmNgInS
sovětskými	sovětský	k2eAgFnPc7d1
ozbrojenými	ozbrojený	k2eAgFnPc7d1
silami	síla	k1gFnPc7
a	a	k8xC
využívala	využívat	k5eAaImAgFnS,k5eAaPmAgFnS
ho	on	k3xPp3gNnSc4
i	i	k9
většina	většina	k1gFnSc1
členských	členský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
Varšavské	varšavský	k2eAgFnSc2d1
smlouvy	smlouva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
I	i	k9
po	po	k7c6
sedmdesáti	sedmdesát	k4xCc6
letech	léto	k1gNnPc6
jsou	být	k5eAaImIp3nP
tento	tento	k3xDgInSc4
model	model	k1gInSc4
a	a	k8xC
jeho	jeho	k3xOp3gFnPc4
varianty	varianta	k1gFnPc4
stále	stále	k6eAd1
široce	široko	k6eAd1
používanými	používaný	k2eAgFnPc7d1
a	a	k8xC
nejpopulárnějšími	populární	k2eAgFnPc7d3
útočnými	útočný	k2eAgFnPc7d1
puškami	puška	k1gFnPc7
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zbraň	zbraň	k1gFnSc1
si	se	k3xPyFc3
drží	držet	k5eAaImIp3nS
spolehlivost	spolehlivost	k1gFnSc4
za	za	k7c2
náročných	náročný	k2eAgFnPc2d1
podmínek	podmínka	k1gFnPc2
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
nízké	nízký	k2eAgInPc4d1
výrobní	výrobní	k2eAgInPc4d1
náklady	náklad	k1gInPc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
dostupná	dostupný	k2eAgFnSc1d1
prakticky	prakticky	k6eAd1
v	v	k7c6
každé	každý	k3xTgFnSc6
geografické	geografický	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
a	a	k8xC
její	její	k3xOp3gFnSc1
obsluha	obsluha	k1gFnSc1
je	být	k5eAaImIp3nS
snadná	snadný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
AK-47	AK-47	k1gFnSc1
byl	být	k5eAaImAgInS
vyráběn	vyráběn	k2eAgInSc1d1
v	v	k7c6
mnoha	mnoho	k4c6
zemích	zem	k1gFnPc6
<g/>
,	,	kIx,
dočkal	dočkat	k5eAaPmAgMnS
se	se	k3xPyFc4
služby	služba	k1gFnSc2
v	v	k7c6
ozbrojených	ozbrojený	k2eAgFnPc6d1
složkách	složka	k1gFnPc6
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
nepravidelných	pravidelný	k2eNgFnPc6d1
silách	síla	k1gFnPc6
po	po	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
a	a	k8xC
byl	být	k5eAaImAgInS
základem	základ	k1gInSc7
pro	pro	k7c4
vývoj	vývoj	k1gInSc4
mnoha	mnoho	k4c2
dalších	další	k2eAgInPc2d1
typů	typ	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Počátky	počátek	k1gInPc4
</s>
<s>
Na	na	k7c6
počátku	počátek	k1gInSc6
východního	východní	k2eAgNnSc2d1
tažení	tažení	k1gNnSc2
byli	být	k5eAaImAgMnP
němečtí	německý	k2eAgMnPc1d1
vojáci	voják	k1gMnPc1
často	často	k6eAd1
konfrontováni	konfrontován	k2eAgMnPc1d1
s	s	k7c7
účinností	účinnost	k1gFnSc7
sovětských	sovětský	k2eAgFnPc2d1
automatických	automatický	k2eAgFnPc2d1
pušek	puška	k1gFnPc2
AVS-36	AVS-36	k1gFnSc2
a	a	k8xC
AVT-40	AVT-40	k1gFnSc2
ve	v	k7c6
výzbroji	výzbroj	k1gFnSc6
jednotek	jednotka	k1gFnPc2
Rudé	rudý	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
přimělo	přimět	k5eAaPmAgNnS
německé	německý	k2eAgMnPc4d1
konstruktéry	konstruktér	k1gMnPc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
obnovili	obnovit	k5eAaPmAgMnP
vývoj	vývoj	k1gInSc4
vlastní	vlastní	k2eAgFnSc2d1
zbraně	zbraň	k1gFnSc2
podobného	podobný	k2eAgInSc2d1
typu	typ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledkem	výsledek	k1gInSc7
byla	být	k5eAaImAgFnS
FG	FG	kA
42	#num#	k4
<g/>
,	,	kIx,
automatická	automatický	k2eAgFnSc1d1
puška	puška	k1gFnSc1
pro	pro	k7c4
speciální	speciální	k2eAgFnPc4d1
jednotky	jednotka	k1gFnPc4
<g/>
,	,	kIx,
a	a	k8xC
"	"	kIx"
<g/>
strojní	strojní	k2eAgFnSc1d1
karabina	karabina	k1gFnSc1
<g/>
"	"	kIx"
Mkb	Mkb	k1gFnSc1
42	#num#	k4
<g/>
(	(	kIx(
<g/>
H	H	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
však	však	k9
přišla	přijít	k5eAaPmAgFnS
do	do	k7c2
výzbroje	výzbroj	k1gFnSc2
německých	německý	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
teprve	teprve	k6eAd1
v	v	k7c6
březnu	březen	k1gInSc6
1943	#num#	k4
<g/>
,	,	kIx,
a	a	k8xC
z	z	k7c2
ní	on	k3xPp3gFnSc2
vyvinutá	vyvinutý	k2eAgFnSc1d1
útočná	útočný	k2eAgFnSc1d1
puška	puška	k1gFnSc1
MP	MP	kA
43	#num#	k4
<g/>
,	,	kIx,
po	po	k7c6
drobných	drobný	k2eAgFnPc6d1
úpravách	úprava	k1gFnPc6
přeznačená	přeznačený	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
StG	StG	k1gFnSc1
44	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Inovací	inovace	k1gFnPc2
u	u	k7c2
zbraní	zbraň	k1gFnPc2
vyvinutých	vyvinutý	k2eAgFnPc2d1
z	z	k7c2
Mkb	Mkb	k1gFnSc2
42	#num#	k4
<g/>
(	(	kIx(
<g/>
H	H	kA
<g/>
)	)	kIx)
bylo	být	k5eAaImAgNnS
použití	použití	k1gNnSc1
zkráceného	zkrácený	k2eAgInSc2d1
puškového	puškový	k2eAgInSc2d1
náboje	náboj	k1gInSc2
7,92	7,92	k4
×	×	k?
33	#num#	k4
mm	mm	kA
Kurz	kurz	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zkrácený	zkrácený	k2eAgInSc1d1
náboj	náboj	k1gInSc1
umožnil	umožnit	k5eAaPmAgInS
kompromis	kompromis	k1gInSc4
mezi	mezi	k7c7
palebnou	palebný	k2eAgFnSc7d1
silou	síla	k1gFnSc7
samopalu	samopal	k1gInSc2
a	a	k8xC
dostřelem	dostřel	k1gInSc7
a	a	k8xC
přesností	přesnost	k1gFnSc7
pušky	puška	k1gFnSc2
<g/>
:	:	kIx,
dovolil	dovolit	k5eAaPmAgMnS
ke	k	k7c3
zbrani	zbraň	k1gFnSc3
připojit	připojit	k5eAaPmF
zásobník	zásobník	k1gInSc4
s	s	k7c7
větší	veliký	k2eAgFnSc7d2
kapacitou	kapacita	k1gFnSc7
nábojů	náboj	k1gInPc2
<g/>
,	,	kIx,
než	než	k8xS
měly	mít	k5eAaImAgFnP
pušky	puška	k1gFnPc1
<g/>
,	,	kIx,
a	a	k8xC
současně	současně	k6eAd1
umožnil	umožnit	k5eAaPmAgInS
přesnou	přesný	k2eAgFnSc4d1
střelbu	střelba	k1gFnSc4
na	na	k7c4
větší	veliký	k2eAgFnSc4d2
vzdálenost	vzdálenost	k1gFnSc4
<g/>
,	,	kIx,
než	než	k8xS
bylo	být	k5eAaImAgNnS
možné	možný	k2eAgNnSc1d1
u	u	k7c2
pistolí	pistol	k1gFnPc2
a	a	k8xC
samopalů	samopal	k1gInPc2
<g/>
.	.	kIx.
15	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1943	#num#	k4
byl	být	k5eAaImAgInS
Sturmgewehr	Sturmgewehr	k1gInSc1
předveden	předveden	k2eAgInSc1d1
lidovému	lidový	k2eAgInSc3d1
komisariátu	komisariát	k1gInSc3
výzbroje	výzbroj	k1gFnSc2
SSSR	SSSR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
Sověty	Sověty	k1gInPc4
Sturmgewehr	Sturmgewehra	k1gFnPc2
udělal	udělat	k5eAaPmAgInS
takový	takový	k3xDgInSc1
dojem	dojem	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
okamžitě	okamžitě	k6eAd1
pustili	pustit	k5eAaPmAgMnP
do	do	k7c2
vývoje	vývoj	k1gInSc2
vlastní	vlastní	k2eAgFnPc4d1
automatické	automatický	k2eAgFnPc4d1
zbraně	zbraň	k1gFnPc4
na	na	k7c4
zkrácený	zkrácený	k2eAgInSc4d1
puškový	puškový	k2eAgInSc4d1
náboj	náboj	k1gInSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
by	by	kYmCp3nS
nahradila	nahradit	k5eAaPmAgFnS
jak	jak	k8xC,k8xS
pušky	puška	k1gFnSc2
Mosin-Nagant	Mosin-Naganta	k1gFnPc2
<g/>
,	,	kIx,
tak	tak	k9
i	i	k9
samopaly	samopal	k1gInPc1
PPŠ-	PPŠ-	k1gFnSc1
<g/>
41	#num#	k4
<g/>
,	,	kIx,
jimiž	jenž	k3xRgFnPc7
byla	být	k5eAaImAgFnS
vyzbrojena	vyzbrojit	k5eAaPmNgFnS
většina	většina	k1gFnSc1
sovětské	sovětský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Koncepce	koncepce	k1gFnSc1
</s>
<s>
Avtomat	Avtomat	k1gInSc1
Fjodorova	Fjodorův	k2eAgInSc2d1
</s>
<s>
Simonov	Simonov	k1gInSc1
AVS-36	AVS-36	k1gFnSc2
</s>
<s>
Mkb	Mkb	k?
<g/>
.42	.42	k4
Haenel	Haenel	k1gInSc1
<g/>
/	/	kIx~
<g/>
MP	MP	kA
<g/>
43	#num#	k4
<g/>
A	a	k8xC
</s>
<s>
Michail	Michail	k1gInSc1
Kalašnikov	kalašnikov	k1gInSc1
zahájil	zahájit	k5eAaPmAgInS
svou	svůj	k3xOyFgFnSc4
kariéru	kariéra	k1gFnSc4
jako	jako	k8xC,k8xS
konstruktér	konstruktér	k1gMnSc1
zbraní	zbraň	k1gFnPc2
v	v	k7c6
roce	rok	k1gInSc6
1941	#num#	k4
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
se	se	k3xPyFc4
zotavoval	zotavovat	k5eAaImAgMnS
ze	z	k7c2
poranění	poranění	k1gNnSc2
ramene	rameno	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yQgNnSc4,k3yIgNnSc4,k3yRgNnSc4
utrpěl	utrpět	k5eAaPmAgMnS
během	během	k7c2
bitvy	bitva	k1gFnSc2
u	u	k7c2
Brjansku	Brjansko	k1gNnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Sám	sám	k3xTgInSc1
Kalašnikov	kalašnikov	k1gInSc1
uvedl	uvést	k5eAaPmAgInS
<g/>
...	...	k?
<g/>
„	„	k?
<g/>
Byl	být	k5eAaImAgInS
jsem	být	k5eAaImIp1nS
v	v	k7c6
nemocnici	nemocnice	k1gFnSc6
a	a	k8xC
voják	voják	k1gMnSc1
v	v	k7c6
posteli	postel	k1gFnSc6
vedle	vedle	k7c2
mě	já	k3xPp1nSc2
se	se	k3xPyFc4
zeptal	zeptat	k5eAaPmAgMnS
<g/>
:	:	kIx,
<g/>
„	„	k?
Proč	proč	k6eAd1
mají	mít	k5eAaImIp3nP
naši	náš	k3xOp1gMnPc1
vojáci	voják	k1gMnPc1
jen	jen	k9
jednu	jeden	k4xCgFnSc4
pušku	puška	k1gFnSc4
pro	pro	k7c4
dva	dva	k4xCgInPc4
nebo	nebo	k8xC
tři	tři	k4xCgInPc4
naše	náš	k3xOp1gMnPc4
muže	muž	k1gMnPc4
<g/>
,	,	kIx,
když	když	k8xS
Němci	Němec	k1gMnPc1
mají	mít	k5eAaImIp3nP
automaty	automat	k1gInPc4
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
“	“	k?
Tak	tak	k9
jsem	být	k5eAaImIp1nS
jeden	jeden	k4xCgInSc1
navrhl	navrhnout	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
jsem	být	k5eAaImIp1nS
vojákem	voják	k1gMnSc7
a	a	k8xC
pro	pro	k7c4
vojáka	voják	k1gMnSc4
jsem	být	k5eAaImIp1nS
sestrojil	sestrojit	k5eAaPmAgMnS
kulomet	kulomet	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nazýval	nazývat	k5eAaImAgInS
se	se	k3xPyFc4
Avtomat	Avtomat	k1gInSc1
Kalašnikova	kalašnikov	k1gInSc2
<g/>
,	,	kIx,
automatická	automatický	k2eAgFnSc1d1
zbraň	zbraň	k1gFnSc1
Kalašnikova	kalašnikov	k1gInSc2
-	-	kIx~
AK	AK	kA
-	-	kIx~
a	a	k8xC
nesl	nést	k5eAaImAgMnS
rok	rok	k1gInSc4
první	první	k4xOgFnSc2
výroby	výroba	k1gFnSc2
<g/>
,	,	kIx,
1947	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
AK-47	AK-47	k4
je	být	k5eAaImIp3nS
nejlépe	dobře	k6eAd3
popisován	popisován	k2eAgMnSc1d1
jako	jako	k8xS,k8xC
hybrid	hybrid	k1gInSc1
předchozích	předchozí	k2eAgFnPc2d1
technologických	technologický	k2eAgFnPc2d1
inovací	inovace	k1gFnPc2
pušek	puška	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kalašnikov	kalašnikov	k1gInSc1
se	se	k3xPyFc4
rozhodl	rozhodnout	k5eAaPmAgInS
navrhnout	navrhnout	k5eAaPmF
automatickou	automatický	k2eAgFnSc4d1
pušku	puška	k1gFnSc4
kombinující	kombinující	k2eAgFnSc2d1
nejlepší	dobrý	k2eAgFnPc4d3
vlastnosti	vlastnost	k1gFnPc4
americké	americký	k2eAgFnSc2d1
M1	M1	k1gFnSc2
Garand	Garanda	k1gFnPc2
a	a	k8xC
německé	německý	k2eAgFnSc2d1
StG	StG	k1gFnSc2
44	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Kalašnikovův	Kalašnikovův	k2eAgInSc4d1
tým	tým	k1gInSc4
měl	mít	k5eAaImAgMnS
k	k	k7c3
těmto	tento	k3xDgFnPc3
zbraním	zbraň	k1gFnPc3
přístup	přístup	k1gInSc4
a	a	k8xC
nemusel	muset	k5eNaImAgInS
„	„	k?
<g/>
znovu	znovu	k6eAd1
vynalézat	vynalézat	k5eAaImF
kolo	kolo	k1gNnSc4
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kalašnikov	kalašnikov	k1gInSc4
sám	sám	k3xTgInSc4
poznamenal	poznamenat	k5eAaPmAgInS
<g/>
:	:	kIx,
„	„	k?
<g/>
Mnoho	mnoho	k4c1
ruských	ruský	k2eAgMnPc2d1
vojáků	voják	k1gMnPc2
se	se	k3xPyFc4
mě	já	k3xPp1nSc4
ptá	ptat	k5eAaImIp3nS
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
se	se	k3xPyFc4
člověk	člověk	k1gMnSc1
může	moct	k5eAaImIp3nS
stát	stát	k5eAaPmF,k5eAaImF
konstruktérem	konstruktér	k1gMnSc7
a	a	k8xC
jak	jak	k6eAd1
se	se	k3xPyFc4
nové	nový	k2eAgFnPc1d1
zbraně	zbraň	k1gFnPc1
vyvíjejí	vyvíjet	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
o	o	k7c4
velmi	velmi	k6eAd1
obtížné	obtížný	k2eAgFnPc4d1
otázky	otázka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zdá	zdát	k5eAaImIp3nS,k5eAaPmIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
každý	každý	k3xTgMnSc1
konstruktér	konstruktér	k1gMnSc1
má	mít	k5eAaImIp3nS
svou	svůj	k3xOyFgFnSc4
vlastní	vlastní	k2eAgFnSc4d1
cestu	cesta	k1gFnSc4
<g/>
,	,	kIx,
vlastní	vlastní	k2eAgInPc4d1
úspěchy	úspěch	k1gInPc4
a	a	k8xC
neúspěchy	neúspěch	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ale	ale	k9
jedna	jeden	k4xCgFnSc1
věc	věc	k1gFnSc1
je	být	k5eAaImIp3nS
jasná	jasný	k2eAgFnSc1d1
<g/>
:	:	kIx,
před	před	k7c7
pokusem	pokus	k1gInSc7
vytvořit	vytvořit	k5eAaPmF
něco	něco	k3yInSc4
nového	nový	k2eAgNnSc2d1
je	být	k5eAaImIp3nS
důležité	důležitý	k2eAgNnSc1d1
mít	mít	k5eAaImF
dobré	dobrý	k2eAgFnSc2d1
znalosti	znalost	k1gFnSc2
všeho	všecek	k3xTgNnSc2
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yInSc1,k3yRnSc1
již	již	k6eAd1
v	v	k7c6
této	tento	k3xDgFnSc6
oblasti	oblast	k1gFnSc6
existuje	existovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sám	sám	k3xTgMnSc1
jsem	být	k5eAaImIp1nS
měl	mít	k5eAaImAgMnS
mnoho	mnoho	k4c4
zkušeností	zkušenost	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
to	ten	k3xDgNnSc1
potvrzují	potvrzovat	k5eAaImIp3nP
<g/>
.	.	kIx.
<g/>
“	“	k?
Kalašnikov	kalašnikov	k1gInSc1
údajně	údajně	k6eAd1
okopíroval	okopírovat	k5eAaPmAgInS
i	i	k9
další	další	k2eAgInPc4d1
návrhy	návrh	k1gInPc4
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
byl	být	k5eAaImAgInS
Bulkinův	Bulkinův	k2eAgInSc1d1
prototyp	prototyp	k1gInSc1
TKB-	TKB-	k1gFnSc1
<g/>
415	#num#	k4
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
nebo	nebo	k8xC
Simonovova	Simonovův	k2eAgFnSc1d1
AVS-	AVS-	k1gFnSc1
<g/>
31	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
První	první	k4xOgFnSc1
konstrukce	konstrukce	k1gFnSc1
</s>
<s>
Michail	Michail	k1gInSc1
Kalašnikov	kalašnikov	k1gInSc1
se	se	k3xPyFc4
začal	začít	k5eAaPmAgInS
konstrukcí	konstrukce	k1gFnSc7
samopalu	samopal	k1gInSc2
pro	pro	k7c4
náboj	náboj	k1gInSc4
7,62	7,62	k4
×	×	k?
39	#num#	k4
mm	mm	kA
M43	M43	k1gFnPc2
zabývat	zabývat	k5eAaImF
v	v	k7c6
roce	rok	k1gInSc6
1942	#num#	k4
a	a	k8xC
lehkého	lehký	k2eAgInSc2d1
kulometu	kulomet	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
1943	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1944	#num#	k4
vstoupil	vstoupit	k5eAaPmAgMnS
do	do	k7c2
soutěže	soutěž	k1gFnSc2
s	s	k7c7
novou	nový	k2eAgFnSc7d1
poloautomatickou	poloautomatický	k2eAgFnSc7d1
karabinou	karabina	k1gFnSc7
fungující	fungující	k2eAgFnSc1d1
na	na	k7c6
principu	princip	k1gInSc6
odběru	odběr	k1gInSc2
prachových	prachový	k2eAgInPc2d1
plynů	plyn	k1gInPc2
se	s	k7c7
systémem	systém	k1gInSc7
s	s	k7c7
dlouhým	dlouhý	k2eAgInSc7d1
zdvihem	zdvih	k1gInSc7
pístu	píst	k1gInSc2
a	a	k8xC
ráží	rážet	k5eAaImIp3nS
7,62	7,62	k4
×	×	k?
39	#num#	k4
mm	mm	kA
<g/>
,	,	kIx,
silně	silně	k6eAd1
ovlivněnou	ovlivněný	k2eAgFnSc7d1
americkou	americký	k2eAgFnSc7d1
M1	M1	k1gFnSc7
Garand	Garanda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnSc1
návrh	návrh	k1gInSc4
však	však	k9
s	s	k7c7
konkurenční	konkurenční	k2eAgFnSc7d1
zbraní	zbraň	k1gFnSc7
SKS-45	SKS-45	k1gFnSc2
Simonov	Simonovo	k1gNnPc2
prohrál	prohrát	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1946	#num#	k4
byla	být	k5eAaImAgFnS
zahájena	zahájit	k5eAaPmNgFnS
soutěž	soutěž	k1gFnSc1
na	na	k7c4
novou	nový	k2eAgFnSc4d1
útočnou	útočný	k2eAgFnSc4d1
pušku	puška	k1gFnSc4
a	a	k8xC
Kalašnikov	kalašnikov	k1gInSc4
potvrdil	potvrdit	k5eAaPmAgMnS
vstup	vstup	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šlo	jít	k5eAaImAgNnS
o	o	k7c4
pušku	puška	k1gFnSc4
fungující	fungující	k2eAgFnSc4d1
na	na	k7c6
principu	princip	k1gInSc6
odběru	odběr	k1gInSc2
prachových	prachový	k2eAgInPc2d1
plynů	plyn	k1gInPc2
se	se	k3xPyFc4
systém	systém	k1gInSc1
s	s	k7c7
krátkým	krátký	k2eAgInSc7d1
zdvihem	zdvih	k1gInSc7
pístu	píst	k1gInSc2
nad	nad	k7c4
hlavní	hlavní	k2eAgFnPc4d1
<g/>
,	,	kIx,
závěrem	závěr	k1gInSc7
podobným	podobný	k2eAgInSc7d1
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
měla	mít	k5eAaImAgFnS
jeho	jeho	k3xOp3gFnSc1
karabina	karabina	k1gFnSc1
z	z	k7c2
roku	rok	k1gInSc2
1944	#num#	k4
a	a	k8xC
zakřiveným	zakřivený	k2eAgInSc7d1
zásobníkem	zásobník	k1gInSc7
na	na	k7c4
30	#num#	k4
nábojů	náboj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kalašnikovovy	Kalašnikovův	k2eAgFnPc4d1
pušky	puška	k1gFnPc4
AK-1	AK-1	k1gFnSc2
(	(	kIx(
<g/>
s	s	k7c7
obráběným	obráběný	k2eAgNnSc7d1
pouzdrem	pouzdro	k1gNnSc7
závěru	závěr	k1gInSc2
<g/>
)	)	kIx)
a	a	k8xC
AK-2	AK-2	k1gMnSc1
(	(	kIx(
<g/>
s	s	k7c7
pouzdrem	pouzdro	k1gNnSc7
závěru	závěr	k1gInSc2
vyrobeným	vyrobený	k2eAgFnPc3d1
z	z	k7c2
lisovaného	lisovaný	k2eAgInSc2d1
plechu	plech	k1gInSc2
<g/>
)	)	kIx)
se	se	k3xPyFc4
ukázaly	ukázat	k5eAaPmAgFnP
jako	jako	k9
spolehlivé	spolehlivý	k2eAgFnPc1d1
zbraně	zbraň	k1gFnPc1
a	a	k8xC
byly	být	k5eAaImAgFnP
přijaty	přijmout	k5eAaPmNgFnP
do	do	k7c2
druhého	druhý	k4xOgNnSc2
kola	kolo	k1gNnSc2
soutěže	soutěž	k1gFnSc2
spolu	spolu	k6eAd1
s	s	k7c7
dalšími	další	k2eAgInPc7d1
návrhy	návrh	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
prototypy	prototyp	k1gInPc4
(	(	kIx(
<g/>
také	také	k9
známé	známý	k2eAgMnPc4d1
jako	jako	k8xS,k8xC
AK-	AK-	k1gFnSc1
<g/>
46	#num#	k4
<g/>
)	)	kIx)
měly	mít	k5eAaImAgInP
rotační	rotační	k2eAgInSc4d1
závorník	závorník	k1gInSc4
<g/>
,	,	kIx,
dvoudílné	dvoudílný	k2eAgNnSc4d1
pouzdro	pouzdro	k1gNnSc4
závěru	závěr	k1gInSc2
s	s	k7c7
odděleným	oddělený	k2eAgNnSc7d1
chránítkem	chránítko	k1gNnSc7
spouště	spoušť	k1gFnSc2
<g/>
,	,	kIx,
dvojí	dvojí	k4xRgFnSc4
kontrolu	kontrola	k1gFnSc4
(	(	kIx(
<g/>
oddělená	oddělený	k2eAgFnSc1d1
pojistka	pojistka	k1gFnSc1
a	a	k8xC
přepínač	přepínač	k1gInSc1
režimu	režim	k1gInSc2
střelby	střelba	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
nevratnou	vratný	k2eNgFnSc4d1
natahovací	natahovací	k2eAgFnSc4d1
páku	páka	k1gFnSc4
umístěnou	umístěný	k2eAgFnSc7d1
na	na	k7c6
levé	levý	k2eAgFnSc6d1
straně	strana	k1gFnSc6
zbraně	zbraň	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
dopracování	dopracování	k1gNnSc3
konstrukce	konstrukce	k1gFnSc2
a	a	k8xC
výrobě	výroba	k1gFnSc6
prototypů	prototyp	k1gInPc2
byl	být	k5eAaImAgMnS
koncem	koncem	k7c2
roku	rok	k1gInSc2
1946	#num#	k4
Kalašnikov	kalašnikov	k1gInSc1
přeložen	přeložen	k2eAgInSc1d1
z	z	k7c2
Vědecko-výzkumného	vědecko-výzkumný	k2eAgInSc2d1
polygonu	polygon	k1gInSc2
střelecké	střelecký	k2eAgFnSc2d1
a	a	k8xC
minometné	minometný	k2eAgFnSc2d1
výzbroje	výzbroj	k1gFnSc2
v	v	k7c6
Šurovu	Šurov	k1gInSc6
u	u	k7c2
Moskvy	Moskva	k1gFnSc2
(	(	kIx(
<g/>
kde	kde	k6eAd1
působil	působit	k5eAaImAgMnS
od	od	k7c2
roku	rok	k1gInSc2
1942	#num#	k4
<g/>
)	)	kIx)
do	do	k7c2
zbrojovky	zbrojovka	k1gFnSc2
v	v	k7c4
Kovrovu	Kovrův	k2eAgFnSc4d1
ve	v	k7c6
Vladimirské	Vladimirský	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
konci	konec	k1gInSc6
roku	rok	k1gInSc2
1946	#num#	k4
byly	být	k5eAaImAgFnP
pušky	puška	k1gFnPc1
testovány	testován	k2eAgFnPc1d1
a	a	k8xC
jeden	jeden	k4xCgMnSc1
z	z	k7c2
kovrovských	kovrovský	k2eAgMnPc2d1
konstruktérů	konstruktér	k1gMnPc2
<g/>
,	,	kIx,
Alexandr	Alexandr	k1gMnSc1
Zajcev	Zajcev	k1gFnSc1
<g/>
,	,	kIx,
navrhl	navrhnout	k5eAaPmAgMnS
zbraň	zbraň	k1gFnSc4
ještě	ještě	k6eAd1
překonstruovat	překonstruovat	k5eAaPmF
ke	k	k7c3
zlepšení	zlepšení	k1gNnSc3
spolehlivosti	spolehlivost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zpočátku	zpočátku	k6eAd1
Kalašnikov	kalašnikov	k1gInSc1
nebyl	být	k5eNaImAgInS
ochotný	ochotný	k2eAgMnSc1d1
<g/>
,	,	kIx,
protože	protože	k8xS
pušky	puška	k1gFnPc1
už	už	k6eAd1
tak	tak	k6eAd1
dopadly	dopadnout	k5eAaPmAgFnP
lépe	dobře	k6eAd2
než	než	k8xS
u	u	k7c2
jeho	jeho	k3xOp3gMnPc2
konkurentů	konkurent	k1gMnPc2
<g/>
,	,	kIx,
nakonec	nakonec	k6eAd1
ho	on	k3xPp3gNnSc4
ale	ale	k8xC
Zajcev	Zajcev	k1gFnSc4
přesvědčil	přesvědčit	k5eAaPmAgMnS
a	a	k8xC
zbraň	zbraň	k1gFnSc4
spolu	spolu	k6eAd1
radikálně	radikálně	k6eAd1
přepracovali	přepracovat	k5eAaPmAgMnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
listopadu	listopad	k1gInSc6
1947	#num#	k4
byly	být	k5eAaImAgFnP
dokončeny	dokončit	k5eAaPmNgInP
nové	nový	k2eAgInPc1d1
prototypy	prototyp	k1gInPc1
AK-	AK-	k1gFnSc2
<g/>
47	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
modely	model	k1gInPc7
využívaly	využívat	k5eAaPmAgFnP,k5eAaImAgFnP
systém	systém	k1gInSc4
s	s	k7c7
dlouhým	dlouhý	k2eAgInSc7d1
zdvihem	zdvih	k1gInSc7
pístu	píst	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Horní	horní	k2eAgFnSc1d1
a	a	k8xC
spodní	spodní	k2eAgNnPc1d1
pouzdra	pouzdro	k1gNnPc1
závěru	závěr	k1gInSc2
byla	být	k5eAaImAgFnS
spojena	spojit	k5eAaPmNgFnS
do	do	k7c2
jednoho	jeden	k4xCgNnSc2
pouzdra	pouzdro	k1gNnSc2
závěru	závěr	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Volič	volič	k1gMnSc1
režimu	režim	k1gInSc2
střelby	střelba	k1gFnSc2
a	a	k8xC
pojistka	pojistka	k1gFnSc1
byly	být	k5eAaImAgFnP
sloučeny	sloučit	k5eAaPmNgFnP
do	do	k7c2
jediné	jediný	k2eAgFnSc2d1
páky	páka	k1gFnSc2
na	na	k7c6
pravé	pravý	k2eAgFnSc6d1
straně	strana	k1gFnSc6
pušky	puška	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Natahovací	natahovací	k2eAgFnSc1d1
páka	páka	k1gFnSc1
byla	být	k5eAaImAgFnS
jednoduše	jednoduše	k6eAd1
připojena	připojit	k5eAaPmNgFnS
k	k	k7c3
nosiči	nosič	k1gInSc3
závěru	závěr	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
zjednodušilo	zjednodušit	k5eAaPmAgNnS
konstrukci	konstrukce	k1gFnSc4
a	a	k8xC
výrobu	výroba	k1gFnSc4
pušky	puška	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Verze	verze	k1gFnSc1
Kalašnikova-Zajceva	Kalašnikova-Zajceva	k1gFnSc1
měla	mít	k5eAaImAgFnS
u	u	k7c2
hodnotící	hodnotící	k2eAgFnSc2d1
komise	komise	k1gFnSc2
úspěch	úspěch	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
výrobě	výroba	k1gFnSc3
nové	nový	k2eAgFnSc2d1
zbraně	zbraň	k1gFnSc2
byl	být	k5eAaImAgInS
určen	určit	k5eAaPmNgInS
závod	závod	k1gInSc1
v	v	k7c6
Iževsku	Iževsko	k1gNnSc6
v	v	k7c6
Udmurtské	Udmurtský	k2eAgFnSc6d1
republice	republika	k1gFnSc6
<g/>
,	,	kIx,
kam	kam	k6eAd1
byl	být	k5eAaImAgInS
Kalašnikov	kalašnikov	k1gInSc1
přeložen	přeložit	k5eAaPmNgInS
koncem	koncem	k7c2
roku	rok	k1gInSc2
1947	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
První	první	k4xOgFnPc1
armádní	armádní	k2eAgFnPc1d1
zkoušky	zkouška	k1gFnPc1
začaly	začít	k5eAaPmAgFnP
na	na	k7c6
počátku	počátek	k1gInSc6
roku	rok	k1gInSc2
1948	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nová	nový	k2eAgFnSc1d1
zbraň	zbraň	k1gFnSc1
se	se	k3xPyFc4
ukázala	ukázat	k5eAaPmAgFnS
být	být	k5eAaImF
spolehlivá	spolehlivý	k2eAgFnSc1d1
v	v	k7c6
širokém	široký	k2eAgNnSc6d1
rozmezí	rozmezí	k1gNnSc6
podmínek	podmínka	k1gFnPc2
a	a	k8xC
pohodlná	pohodlný	k2eAgFnSc1d1
na	na	k7c4
obsluhu	obsluha	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1949	#num#	k4
byla	být	k5eAaImAgFnS
přijata	přijmout	k5eAaPmNgFnS
sovětskou	sovětský	k2eAgFnSc7d1
armádou	armáda	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Hugo	Hugo	k1gMnSc1
Schmeisser	Schmeisser	k1gMnSc1
<g/>
,	,	kIx,
německý	německý	k2eAgMnSc1d1
konstruktér	konstruktér	k1gMnSc1
útočné	útočný	k2eAgFnSc2d1
pušky	puška	k1gFnSc2
StG	StG	k1gFnSc2
44	#num#	k4
<g/>
,	,	kIx,
po	po	k7c6
válce	válka	k1gFnSc6
žil	žít	k5eAaImAgMnS
a	a	k8xC
pracoval	pracovat	k5eAaImAgMnS
v	v	k7c6
Iževsku	Iževsko	k1gNnSc6
<g/>
,	,	kIx,
kvůli	kvůli	k7c3
čemuž	což	k3yRnSc3,k3yQnSc3
(	(	kIx(
<g/>
a	a	k8xC
kvůli	kvůli	k7c3
podobnému	podobný	k2eAgInSc3d1
vzhledu	vzhled	k1gInSc3
StG	StG	k1gFnSc2
44	#num#	k4
<g/>
)	)	kIx)
–	–	k?
u	u	k7c2
autorů	autor	k1gMnPc2
neznalých	znalý	k2eNgMnPc2d1
Kalašnikovovy	Kalašnikovův	k2eAgFnPc1d1
práce	práce	k1gFnSc2
v	v	k7c4
Šnurovu	Šnurův	k2eAgFnSc4d1
a	a	k8xC
Kovrovu	Kovrův	k2eAgFnSc4d1
–	–	k?
vznikl	vzniknout	k5eAaPmAgInS
názor	názor	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
Schmeisser	Schmeisser	k1gInSc1
je	být	k5eAaImIp3nS
<g />
.	.	kIx.
</s>
<s hack="1">
jedním	jeden	k4xCgMnSc7
z	z	k7c2
tvůrců	tvůrce	k1gMnPc2
konstrukce	konstrukce	k1gFnSc1
AK-	AK-	k1gFnSc1
<g/>
47	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
českých	český	k2eAgNnPc6d1
popularizačních	popularizační	k2eAgNnPc6d1
médiích	médium	k1gNnPc6
se	se	k3xPyFc4
dokonce	dokonce	k9
objevuje	objevovat	k5eAaImIp3nS
tvrzení	tvrzení	k1gNnSc1
<g/>
,	,	kIx,
že	že	k8xS
„	„	k?
<g/>
Schmeisser	Schmeisser	k1gMnSc1
pracoval	pracovat	k5eAaImAgMnS
pod	pod	k7c7
Kalašnikovem	kalašnikov	k1gInSc7
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Další	další	k2eAgInSc1d1
vývoj	vývoj	k1gInSc1
</s>
<s>
AKM	AKM	kA
a	a	k8xC
SKS-45	SKS-45	k1gFnSc1
</s>
<s>
AKMS	AKMS	kA
s	s	k7c7
lisovaným	lisovaný	k2eAgNnSc7d1
pouzdrem	pouzdro	k1gNnSc7
závěru	závěr	k1gInSc2
typ	typ	k1gInSc4
4B	4B	k4
(	(	kIx(
<g/>
nahoře	nahoře	k6eAd1
<g/>
)	)	kIx)
a	a	k8xC
AK-47	AK-47	k1gFnSc1
s	s	k7c7
obráběným	obráběný	k2eAgNnSc7d1
pouzdrem	pouzdro	k1gNnSc7
závěru	závěr	k1gInSc2
typ	typ	k1gInSc4
2A	2A	k4
</s>
<s>
AK-47	AK-47	k4
z	z	k7c2
roku	rok	k1gInSc2
1955	#num#	k4
s	s	k7c7
obráběným	obráběný	k2eAgNnSc7d1
pouzdrem	pouzdro	k1gNnSc7
závěru	závěr	k1gInSc2
typ	typ	k1gInSc4
3A	3A	k4
</s>
<s>
Během	během	k7c2
počáteční	počáteční	k2eAgFnSc2d1
fáze	fáze	k1gFnSc2
výroby	výroba	k1gFnSc2
se	se	k3xPyFc4
objevilo	objevit	k5eAaPmAgNnS
mnoho	mnoho	k4c1
potíží	potíž	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInPc1
modely	model	k1gInPc1
měly	mít	k5eAaImAgInP
pouzdro	pouzdro	k1gNnSc4
závěru	závěr	k1gInSc2
vyrobené	vyrobený	k2eAgNnSc4d1
z	z	k7c2
lisovaného	lisovaný	k2eAgInSc2d1
plechu	plech	k1gInSc2
<g/>
,	,	kIx,
obráběnou	obráběný	k2eAgFnSc4d1
hlavu	hlava	k1gFnSc4
pouzdra	pouzdro	k1gNnSc2
závěru	závěr	k1gInSc2
a	a	k8xC
lisovaný	lisovaný	k2eAgInSc4d1
rám	rám	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Problémy	problém	k1gInPc7
nastaly	nastat	k5eAaPmAgInP
při	při	k7c6
svařování	svařování	k1gNnSc6
částí	část	k1gFnPc2
vyhazovače	vyhazovač	k1gMnSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
způsobilo	způsobit	k5eAaPmAgNnS
vysokou	vysoký	k2eAgFnSc4d1
zmetkovost	zmetkovost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Místo	místo	k1gNnSc1
zastavení	zastavení	k1gNnSc1
výroby	výroba	k1gFnSc2
bylo	být	k5eAaImAgNnS
lisované	lisovaný	k2eAgNnSc1d1
pouzdro	pouzdro	k1gNnSc1
závěru	závěr	k1gInSc2
nahrazeno	nahradit	k5eAaPmNgNnS
asi	asi	k9
o	o	k7c4
2,2	2,2	k4
libry	libra	k1gFnSc2
těžším	těžký	k2eAgNnSc7d2
obráběným	obráběný	k2eAgNnSc7d1
pouzdrem	pouzdro	k1gNnSc7
závěru	závěr	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šlo	jít	k5eAaImAgNnS
o	o	k7c4
nákladnější	nákladný	k2eAgInSc4d2
proces	proces	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
použití	použití	k1gNnSc1
obrobených	obrobený	k2eAgFnPc2d1
částí	část	k1gFnPc2
zrychlilo	zrychlit	k5eAaPmAgNnS
výrobu	výroba	k1gFnSc4
a	a	k8xC
také	také	k9
byly	být	k5eAaImAgFnP
využity	využít	k5eAaPmNgInP
nástroje	nástroj	k1gInPc1
a	a	k8xC
pracovní	pracovní	k2eAgFnSc1d1
síla	síla	k1gFnSc1
zvyklá	zvyklý	k2eAgFnSc1d1
obrábět	obrábět	k5eAaImF
části	část	k1gFnPc4
starých	starý	k2eAgFnPc2d1
pušek	puška	k1gFnPc2
Mosin-Nagant	Mosin-Nagant	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Částečně	částečně	k6eAd1
kvůli	kvůli	k7c3
těmto	tento	k3xDgInPc3
problémů	problém	k1gInPc2
nebyl	být	k5eNaImAgInS
SSSR	SSSR	kA
schopen	schopen	k2eAgInSc1d1
distribuovat	distribuovat	k5eAaBmF
velké	velký	k2eAgNnSc4d1
množství	množství	k1gNnSc4
nové	nový	k2eAgFnSc2d1
pušky	puška	k1gFnSc2
armádě	armáda	k1gFnSc3
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
1956	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
této	tento	k3xDgFnSc2
doby	doba	k1gFnSc2
také	také	k9
pokračovala	pokračovat	k5eAaImAgFnS
výroba	výroba	k1gFnSc1
pušek	puška	k1gFnPc2
SKS	SKS	kA
<g/>
.	.	kIx.
</s>
<s>
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k9
byly	být	k5eAaImAgFnP
překonány	překonán	k2eAgFnPc1d1
výrobní	výrobní	k2eAgFnPc1d1
potíže	potíž	k1gFnPc1
neobráběných	obráběný	k2eNgNnPc2d1
pouzder	pouzdro	k1gNnPc2
závěru	závěr	k1gInSc2
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
přepracovaná	přepracovaný	k2eAgFnSc1d1
verze	verze	k1gFnSc1
označena	označen	k2eAgFnSc1d1
AKM	AKM	kA
(	(	kIx(
<g/>
M	M	kA
jako	jako	k9
"	"	kIx"
<g/>
modernizovaný	modernizovaný	k2eAgMnSc1d1
<g/>
"	"	kIx"
<g/>
)	)	kIx)
a	a	k8xC
představena	představit	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1959	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
nový	nový	k2eAgInSc1d1
model	model	k1gInSc1
měl	mít	k5eAaImAgInS
pouzdro	pouzdro	k1gNnSc4
závěru	závěr	k1gInSc2
lisované	lisovaný	k2eAgFnSc2d1
z	z	k7c2
plechu	plech	k1gInSc2
a	a	k8xC
šikmou	šikmý	k2eAgFnSc4d1
úsťovou	úsťový	k2eAgFnSc4d1
brzdu	brzda	k1gFnSc4
na	na	k7c6
konci	konec	k1gInSc6
hlavně	hlavně	k6eAd1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
kompenzuje	kompenzovat	k5eAaBmIp3nS
zdvih	zdvih	k1gInSc4
a	a	k8xC
zpětný	zpětný	k2eAgInSc4d1
ráz	ráz	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
toho	ten	k3xDgNnSc2
byl	být	k5eAaImAgInS
vylepšen	vylepšen	k2eAgInSc1d1
úderný	úderný	k2eAgInSc1d1
mechanismus	mechanismus	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
brání	bránit	k5eAaImIp3nS
dopadu	dopad	k1gInSc2
kladívka	kladívko	k1gNnSc2
na	na	k7c4
úderník	úderník	k1gInSc4
před	před	k7c7
úplným	úplný	k2eAgNnSc7d1
uzavřením	uzavření	k1gNnSc7
závěru	závěr	k1gInSc2
při	při	k7c6
rychlé	rychlý	k2eAgFnSc6d1
palbě	palba	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
je	být	k5eAaImIp3nS
někdy	někdy	k6eAd1
označováno	označovat	k5eAaImNgNnS
jako	jako	k8xS,k8xC
"	"	kIx"
<g/>
redukce	redukce	k1gFnSc1
kadence	kadence	k1gFnSc2
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
má	mít	k5eAaImIp3nS
za	za	k7c4
následek	následek	k1gInSc4
snížení	snížení	k1gNnSc2
počtu	počet	k1gInSc2
výstřelů	výstřel	k1gInPc2
za	za	k7c4
minutu	minuta	k1gFnSc4
během	během	k7c2
automatické	automatický	k2eAgFnSc2d1
palby	palba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
AKM	AKM	kA
je	být	k5eAaImIp3nS
také	také	k9
zhruba	zhruba	k6eAd1
o	o	k7c4
třetinu	třetina	k1gFnSc4
lehčí	lehký	k2eAgInPc4d2
než	než	k8xS
předchozí	předchozí	k2eAgInSc4d1
model	model	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
60	#num#	k4
<g/>
.	.	kIx.
letech	let	k1gInPc6
byl	být	k5eAaImAgInS
také	také	k6eAd1
představen	představit	k5eAaPmNgInS
lehký	lehký	k2eAgInSc1d1
kulomet	kulomet	k1gInSc1
RPK	RPK	kA
<g/>
,	,	kIx,
zbraň	zbraň	k1gFnSc1
typu	typ	k1gInSc2
AK-47	AK-47	k1gMnPc2
se	s	k7c7
silnějším	silný	k2eAgNnSc7d2
pouzdrem	pouzdro	k1gNnSc7
závěru	závěr	k1gInSc2
<g/>
,	,	kIx,
delší	dlouhý	k2eAgFnSc7d2
hlavní	hlavní	k2eAgFnSc7d1
a	a	k8xC
dvojnožkou	dvojnožka	k1gFnSc7
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
nahradil	nahradit	k5eAaPmAgInS
lehký	lehký	k2eAgInSc1d1
kulomet	kulomet	k1gInSc1
RPD	RPD	kA
<g/>
.	.	kIx.
</s>
<s>
Typ	typ	k1gInSc1
pouzdra	pouzdro	k1gNnSc2
závěru	závěr	k1gInSc2
</s>
<s>
Popis	popis	k1gInSc1
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Typ	typ	k1gInSc1
1	#num#	k4
<g/>
A	A	kA
<g/>
/	/	kIx~
<g/>
B	B	kA
</s>
<s>
Původní	původní	k2eAgNnSc1d1
pouzdro	pouzdro	k1gNnSc1
z	z	k7c2
lisovaného	lisovaný	k2eAgInSc2d1
plechu	plech	k1gInSc2
pro	pro	k7c4
AK-	AK-	k1gFnSc4
<g/>
47	#num#	k4
<g/>
;	;	kIx,
1B	1B	k4
upravené	upravený	k2eAgFnSc2d1
pro	pro	k7c4
sklopnou	sklopný	k2eAgFnSc4d1
opěrku	opěrka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
každé	každý	k3xTgFnSc6
straně	strana	k1gFnSc6
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
velká	velký	k2eAgFnSc1d1
díra	díra	k1gFnSc1
pro	pro	k7c4
příslušenství	příslušenství	k1gNnSc4
ke	k	k7c3
sklopné	sklopný	k2eAgFnSc3d1
opěrce	opěrka	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
(	(	kIx(
<g/>
Systém	systém	k1gInSc1
pojmenování	pojmenování	k1gNnSc1
pokračuje	pokračovat	k5eAaImIp3nS
u	u	k7c2
všech	všecek	k3xTgInPc2
typů	typ	k1gInPc2
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Typ	typ	k1gInSc1
2	#num#	k4
<g/>
A	A	kA
<g/>
/	/	kIx~
<g/>
B	B	kA
</s>
<s>
První	první	k4xOgNnSc4
obráběné	obráběný	k2eAgNnSc4d1
pouzdro	pouzdro	k1gNnSc4
závěru	závěr	k1gInSc2
vyrobené	vyrobený	k2eAgNnSc4d1
z	z	k7c2
ocelového	ocelový	k2eAgInSc2d1
výkovku	výkovek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
výroby	výroba	k1gFnSc2
vstoupilo	vstoupit	k5eAaPmAgNnS
roku	rok	k1gInSc2
1949	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Typ	typ	k1gInSc1
2A	2A	k4
má	mít	k5eAaImIp3nS
charakteristickou	charakteristický	k2eAgFnSc4d1
vložku	vložka	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
spojuje	spojovat	k5eAaImIp3nS
pažbu	pažba	k1gFnSc4
s	s	k7c7
pouzdrem	pouzdro	k1gNnSc7
závěru	závěr	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
je	být	k5eAaImIp3nS
po	po	k7c6
stranách	strana	k1gFnPc6
směrem	směr	k1gInSc7
k	k	k7c3
hlavni	hlaveň	k1gFnSc3
vyfrézované	vyfrézovaný	k2eAgFnSc2d1
kvůli	kvůli	k7c3
odlehčení	odlehčení	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s>
Typ	typ	k1gInSc1
3	#num#	k4
<g/>
A	A	kA
<g/>
/	/	kIx~
<g/>
B	B	kA
</s>
<s>
"	"	kIx"
<g/>
Finální	finální	k2eAgFnSc2d1
<g/>
"	"	kIx"
verze	verze	k1gFnSc2
obráběného	obráběný	k2eAgNnSc2d1
pouzdra	pouzdro	k1gNnSc2
závěru	závěr	k1gInSc2
AK-47	AK-47	k1gFnSc2
vyrobená	vyrobený	k2eAgFnSc1d1
z	z	k7c2
kusu	kus	k1gInSc2
oceli	ocel	k1gFnSc2
(	(	kIx(
<g/>
tyče	tyč	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
výroby	výroba	k1gFnSc2
vstoupila	vstoupit	k5eAaPmAgFnS
v	v	k7c6
letech	léto	k1gNnPc6
1953	#num#	k4
a	a	k8xC
1954	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
o	o	k7c4
nejrozšířenější	rozšířený	k2eAgInSc4d3
typ	typ	k1gInSc4
obráběného	obráběný	k2eAgNnSc2d1
pouzdra	pouzdro	k1gNnSc2
závěru	závěr	k1gInSc2
AK-	AK-	k1gFnSc2
<g/>
47	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobně	podobně	k6eAd1
jako	jako	k8xS,k8xC
u	u	k7c2
předchozího	předchozí	k2eAgInSc2d1
typu	typ	k1gInSc2
je	být	k5eAaImIp3nS
po	po	k7c6
stranách	strana	k1gFnPc6
část	část	k1gFnSc4
vyfrézovaná	vyfrézovaný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Typ	typ	k1gInSc1
4	#num#	k4
<g/>
A	A	kA
<g/>
/	/	kIx~
<g/>
B	B	kA
</s>
<s>
Pouzdro	pouzdro	k1gNnSc1
závěru	závěr	k1gInSc2
AKM	AKM	kA
je	být	k5eAaImIp3nS
vyrobeno	vyrobit	k5eAaPmNgNnS
z	z	k7c2
hladkého	hladký	k2eAgInSc2d1
plechu	plech	k1gInSc2
1	#num#	k4
mm	mm	kA
silného	silný	k2eAgMnSc4d1
<g/>
,	,	kIx,
spoje	spoj	k1gInPc1
jsou	být	k5eAaImIp3nP
z	z	k7c2
nýtů	nýt	k1gInPc2
a	a	k8xC
kolíků	kolík	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
výroby	výroba	k1gFnSc2
vstoupilo	vstoupit	k5eAaPmAgNnS
roku	rok	k1gInSc2
1959	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
o	o	k7c6
nejrozšířenější	rozšířený	k2eAgFnSc6d3
konstrukci	konstrukce	k1gFnSc6
série	série	k1gFnSc2
pušek	puška	k1gFnPc2
AK.	AK.	k1gMnSc1
</s>
<s>
Jak	jak	k6eAd1
licencované	licencovaný	k2eAgNnSc1d1
<g/>
,	,	kIx,
tak	tak	k8xS,k8xC
i	i	k9
nelicencované	licencovaný	k2eNgFnPc1d1
zbraně	zbraň	k1gFnPc1
typu	typ	k1gInSc2
Kalašnikov	kalašnikov	k1gInSc1
vyrobené	vyrobený	k2eAgNnSc4d1
v	v	k7c6
zahraničí	zahraničí	k1gNnSc6
jsou	být	k5eAaImIp3nP
téměř	téměř	k6eAd1
výhradně	výhradně	k6eAd1
varianty	varianta	k1gFnPc4
AKM	AKM	kA
<g/>
,	,	kIx,
částečně	částečně	k6eAd1
kvůli	kvůli	k7c3
mnohem	mnohem	k6eAd1
snadnější	snadný	k2eAgFnSc3d2
výrobě	výroba	k1gFnSc3
lisovaného	lisovaný	k2eAgNnSc2d1
pouzdra	pouzdro	k1gNnSc2
závěru	závěr	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
tímto	tento	k3xDgInSc7
modelem	model	k1gInSc7
se	se	k3xPyFc4
lze	lze	k6eAd1
nejčastěji	často	k6eAd3
setkat	setkat	k5eAaPmF
<g/>
,	,	kIx,
protože	protože	k8xS
byl	být	k5eAaImAgInS
vyráběn	vyrábět	k5eAaImNgInS
v	v	k7c6
mnohem	mnohem	k6eAd1
větších	veliký	k2eAgInPc6d2
počtech	počet	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgInPc4
pušky	puška	k1gFnSc2
založené	založený	k2eAgInPc4d1
na	na	k7c6
principu	princip	k1gInSc6
Kalašnikov	kalašnikov	k1gInSc4
jsou	být	k5eAaImIp3nP
na	na	k7c6
Západě	západ	k1gInSc6
často	často	k6eAd1
označovány	označovat	k5eAaImNgFnP
jako	jako	k8xS,k8xC
AK-	AK-	k1gFnSc1
<g/>
47	#num#	k4
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
správné	správný	k2eAgNnSc1d1
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
pouze	pouze	k6eAd1
v	v	k7c6
případech	případ	k1gInPc6
původních	původní	k2eAgInPc2d1
tří	tři	k4xCgInPc2
typů	typ	k1gInPc2
pouzdra	pouzdro	k1gNnSc2
závěru	závěr	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
většině	většina	k1gFnSc6
zemí	zem	k1gFnPc2
bývalého	bývalý	k2eAgInSc2d1
východního	východní	k2eAgInSc2d1
bloku	blok	k1gInSc2
je	být	k5eAaImIp3nS
zbraň	zbraň	k1gFnSc1
známá	známý	k2eAgFnSc1d1
jednoduše	jednoduše	k6eAd1
jako	jako	k8xS,k8xC
"	"	kIx"
<g/>
kalašnikov	kalašnikov	k1gInSc1
<g/>
"	"	kIx"
nebo	nebo	k8xC
"	"	kIx"
<g/>
AK	AK	kA
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozdíly	rozdíl	k1gInPc4
mezi	mezi	k7c7
obráběným	obráběný	k2eAgNnSc7d1
a	a	k8xC
lisovaným	lisovaný	k2eAgNnSc7d1
pouzdrem	pouzdro	k1gNnSc7
závěru	závěr	k1gInSc2
jsou	být	k5eAaImIp3nP
v	v	k7c6
použití	použití	k1gNnSc6
spíše	spíše	k9
nýtů	nýt	k1gInPc2
než	než	k8xS
svarů	svar	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
má	mít	k5eAaImIp3nS
AKM	AKM	kA
nad	nad	k7c7
zásobníkem	zásobník	k1gInSc7
umístěnou	umístěný	k2eAgFnSc4d1
malou	malý	k2eAgFnSc4d1
západku	západka	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
ho	on	k3xPp3gNnSc4
v	v	k7c6
rámu	rám	k1gInSc6
lépe	dobře	k6eAd2
stabilizuje	stabilizovat	k5eAaBmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Náhrada	náhrada	k1gFnSc1
</s>
<s>
Varianty	variant	k1gInPc1
AK-47	AK-47	k1gFnSc2
a	a	k8xC
AKM	AKM	kA
byly	být	k5eAaImAgFnP
v	v	k7c6
SSSR	SSSR	kA
od	od	k7c2
roku	rok	k1gInSc2
1974	#num#	k4
nahrazovány	nahrazovat	k5eAaImNgFnP
novější	nový	k2eAgFnSc7d2
konstrukcí	konstrukce	k1gFnSc7
AK-	AK-	k1gFnSc2
<g/>
74	#num#	k4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
používá	používat	k5eAaImIp3nS
střelivo	střelivo	k1gNnSc4
5,45	5,45	k4
×	×	k?
39	#num#	k4
mm	mm	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k8xC
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
stále	stále	k6eAd1
nejrozšířenější	rozšířený	k2eAgFnSc1d3
zbraň	zbraň	k1gFnSc1
světa	svět	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Konstrukce	konstrukce	k1gFnSc1
</s>
<s>
Tato	tento	k3xDgFnSc1
část	část	k1gFnSc1
článku	článek	k1gInSc2
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaImF,k5eAaPmF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Systém	systém	k1gInSc1
AK-47	AK-47	k1gFnSc2
</s>
<s>
Puška	puška	k1gFnSc1
používá	používat	k5eAaImIp3nS
náboj	náboj	k1gInSc4
7,62	7,62	k4
×	×	k?
39	#num#	k4
mm	mm	kA
vzor	vzor	k1gInSc4
43	#num#	k4
<g/>
,	,	kIx,
poprvé	poprvé	k6eAd1
využitý	využitý	k2eAgMnSc1d1
u	u	k7c2
samonabíjecí	samonabíjecí	k2eAgFnSc2d1
karabiny	karabina	k1gFnSc2
Simonov	Simonov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zbraň	zbraň	k1gFnSc1
při	při	k7c6
střelbě	střelba	k1gFnSc6
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
tlaku	tlak	k1gInSc3
prachových	prachový	k2eAgInPc2d1
plynů	plyn	k1gInPc2
odebíraných	odebíraný	k2eAgInPc2d1
z	z	k7c2
hlavně	hlavně	k9
prostřednictvím	prostřednictví	k1gNnSc7
plynového	plynový	k2eAgInSc2d1
kanálku	kanálek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	dík	k1gInPc1
ne	ne	k9
naprosto	naprosto	k6eAd1
těsnící	těsnící	k2eAgFnSc4d1
konstrukci	konstrukce	k1gFnSc4
závěru	závěr	k1gInSc2
je	být	k5eAaImIp3nS
ovšem	ovšem	k9
velice	velice	k6eAd1
náchylná	náchylný	k2eAgFnSc1d1
na	na	k7c4
přímý	přímý	k2eAgInSc4d1
kontakt	kontakt	k1gInSc4
s	s	k7c7
nečistotou	nečistota	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
po	po	k7c6
ponoření	ponoření	k1gNnSc6
v	v	k7c6
bahně	bahno	k1gNnSc6
se	se	k3xPyFc4
závěr	závěr	k1gInSc1
s	s	k7c7
největší	veliký	k2eAgFnSc7d3
pravděpodobností	pravděpodobnost	k1gFnSc7
znečistí	znečistit	k5eAaPmIp3nS
a	a	k8xC
zadrhne	zadrhnout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nebude	být	k5eNaImBp3nS
tedy	tedy	k9
sám	sám	k3xTgMnSc1
plně	plně	k6eAd1
cyklovat	cyklovat	k5eAaImF
a	a	k8xC
dovolí	dovolit	k5eAaPmIp3nS
pouze	pouze	k6eAd1
manuálně	manuálně	k6eAd1
operovanou	operovaný	k2eAgFnSc4d1
střelbu	střelba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Značnou	značný	k2eAgFnSc7d1
nevýhodou	nevýhoda	k1gFnSc7
je	být	k5eAaImIp3nS
její	její	k3xOp3gFnSc4
hmotnost	hmotnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
hmotnosti	hmotnost	k1gFnSc6
4,3	4,3	k4
kg	kg	kA
nejde	jít	k5eNaImIp3nS
o	o	k7c4
právě	právě	k6eAd1
nejlehčí	lehký	k2eAgFnSc4d3
zbraň	zbraň	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Určitou	určitý	k2eAgFnSc7d1
nevýhodou	nevýhoda	k1gFnSc7
konstrukce	konstrukce	k1gFnSc2
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
závěr	závěr	k1gInSc1
je	být	k5eAaImIp3nS
těžký	těžký	k2eAgInSc1d1
<g/>
,	,	kIx,
takže	takže	k8xS
zpětný	zpětný	k2eAgInSc1d1
ráz	ráz	k1gInSc1
je	být	k5eAaImIp3nS
velký	velký	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mířidla	mířidlo	k1gNnPc1
jsou	být	k5eAaImIp3nP
sice	sice	k8xC
odolná	odolný	k2eAgFnSc1d1
a	a	k8xC
jednoduchá	jednoduchý	k2eAgFnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
neumožňují	umožňovat	k5eNaImIp3nP
přesnou	přesný	k2eAgFnSc4d1
střelbu	střelba	k1gFnSc4
na	na	k7c4
větší	veliký	k2eAgFnPc4d2
vzdálenosti	vzdálenost	k1gFnPc4
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
znemožňují	znemožňovat	k5eAaImIp3nP
plné	plný	k2eAgNnSc4d1
využití	využití	k1gNnSc4
potenciálu	potenciál	k1gInSc2
zbraně	zbraň	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zbraň	zbraň	k1gFnSc1
také	také	k9
nemá	mít	k5eNaImIp3nS
tzv.	tzv.	kA
střeleckou	střelecký	k2eAgFnSc4d1
pohotovost	pohotovost	k1gFnSc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
závěr	závěr	k1gInSc1
zůstane	zůstat	k5eAaPmIp3nS
po	po	k7c6
vyprázdnění	vyprázdnění	k1gNnSc6
zásobníku	zásobník	k1gInSc2
otevřený	otevřený	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
funkce	funkce	k1gFnSc1
je	být	k5eAaImIp3nS
užitečná	užitečný	k2eAgFnSc1d1
k	k	k7c3
okamžitému	okamžitý	k2eAgNnSc3d1
rozlišení	rozlišení	k1gNnSc3
prázdného	prázdný	k2eAgInSc2d1
zásobníku	zásobník	k1gInSc2
(	(	kIx(
<g/>
např.	např.	kA
od	od	k7c2
případné	případný	k2eAgFnSc2d1
poruchy	porucha	k1gFnSc2
<g/>
/	/	kIx~
<g/>
selhání	selhání	k1gNnSc1
<g/>
)	)	kIx)
a	a	k8xC
pomáhá	pomáhat	k5eAaImIp3nS
k	k	k7c3
rychlejšímu	rychlý	k2eAgNnSc3d2
přebití	přebití	k1gNnSc3
po	po	k7c6
výměně	výměna	k1gFnSc6
zásobníku	zásobník	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
důvodu	důvod	k1gInSc2
absence	absence	k1gFnSc2
střelecké	střelecký	k2eAgFnSc2d1
pohotovosti	pohotovost	k1gFnSc2
může	moct	k5eAaImIp3nS
nastat	nastat	k5eAaPmF
krajně	krajně	k6eAd1
nepříjemná	příjemný	k2eNgFnSc1d1
situace	situace	k1gFnSc1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
střelec	střelec	k1gMnSc1
zkusí	zkusit	k5eAaPmIp3nS
pokračovat	pokračovat	k5eAaImF
v	v	k7c6
boji	boj	k1gInSc6
s	s	k7c7
již	již	k6eAd1
prázdnou	prázdný	k2eAgFnSc7d1
zbraní	zbraň	k1gFnSc7
(	(	kIx(
<g/>
tzv.	tzv.	kA
"	"	kIx"
<g/>
cvaknutí	cvaknutí	k1gNnSc4
mrtvého	mrtvý	k2eAgMnSc2d1
muže	muž	k1gMnSc2
<g/>
"	"	kIx"
-	-	kIx~
"	"	kIx"
<g/>
dead	dead	k1gMnSc1
man	man	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
click	click	k6eAd1
<g/>
"	"	kIx"
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původní	původní	k2eAgFnSc1d1
verze	verze	k1gFnSc1
neobsahuje	obsahovat	k5eNaImIp3nS
žádné	žádný	k3yNgFnPc4
lišty	lišta	k1gFnPc4
pro	pro	k7c4
příslušenství	příslušenství	k1gNnSc4
<g/>
,	,	kIx,
takže	takže	k8xS
montáž	montáž	k1gFnSc1
kolimátorového	kolimátorový	k2eAgInSc2d1
zaměřovače	zaměřovač	k1gInSc2
a	a	k8xC
podobných	podobný	k2eAgInPc2d1
moderních	moderní	k2eAgInPc2d1
doplňků	doplněk	k1gInPc2
je	být	k5eAaImIp3nS
obtížná	obtížný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
2006	#num#	k4
se	se	k3xPyFc4
vyrábějí	vyrábět	k5eAaImIp3nP
kalašnikovy	kalašnikov	k1gInPc1
nelegálně	legálně	k6eNd1
v	v	k7c6
třiceti	třicet	k4xCc6
zemích	zem	k1gFnPc6
světa	svět	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
hrozí	hrozit	k5eAaImIp3nP
výrobci	výrobce	k1gMnPc1
bankrot	bankrot	k1gInSc4
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
Rusko	Rusko	k1gNnSc1
začíná	začínat	k5eAaImIp3nS
podnikat	podnikat	k5eAaImF
kroky	krok	k1gInPc4
k	k	k7c3
zajištění	zajištění	k1gNnSc3
licence	licence	k1gFnSc2
AK-	AK-	k1gFnSc7
<g/>
47	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Roku	rok	k1gInSc2
2014	#num#	k4
se	se	k3xPyFc4
dostává	dostávat	k5eAaImIp3nS
výrobce	výrobce	k1gMnSc1
opět	opět	k6eAd1
do	do	k7c2
zisku	zisk	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
Výrobcům	výrobce	k1gMnPc3
AK-47	AK-47	k1gMnPc1
v	v	k7c6
USA	USA	kA
končí	končit	k5eAaImIp3nS
licence	licence	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
2015	#num#	k4
bude	být	k5eAaImBp3nS
vyrábět	vyrábět	k5eAaImF
v	v	k7c6
USA	USA	kA
AK-47	AK-47	k1gFnSc1
společnost	společnost	k1gFnSc1
RWC	RWC	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Československý	československý	k2eAgInSc1d1
samopal	samopal	k1gInSc1
vzor	vzor	k1gInSc1
58	#num#	k4
sice	sice	k8xC
zvnějšku	zvnějšku	k6eAd1
připomíná	připomínat	k5eAaImIp3nS
AK-	AK-	k1gFnSc1
<g/>
47	#num#	k4
<g/>
,	,	kIx,
ale	ale	k8xC
jeho	jeho	k3xOp3gInSc1
vnitřní	vnitřní	k2eAgInSc1d1
mechanismus	mechanismus	k1gInSc1
je	být	k5eAaImIp3nS
zcela	zcela	k6eAd1
odlišný	odlišný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Izraelská	izraelský	k2eAgFnSc1d1
puška	puška	k1gFnSc1
IMI	IMI	kA
Galil	Galil	k1gInSc4
se	se	k3xPyFc4
inspirovala	inspirovat	k5eAaBmAgFnS
v	v	k7c6
AK-	AK-	k1gFnSc6
<g/>
47	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Technické	technický	k2eAgInPc1d1
údaje	údaj	k1gInPc1
</s>
<s>
AK-47	AK-47	k4
v	v	k7c6
provedení	provedení	k1gNnSc6
se	s	k7c7
sklopnou	sklopný	k2eAgFnSc7d1
ramenní	ramenní	k2eAgFnSc7d1
opěrkou	opěrka	k1gFnSc7
(	(	kIx(
<g/>
AKMS	AKMS	kA
<g/>
)	)	kIx)
</s>
<s>
AK-47	AK-47	k4
Typ	typ	k1gInSc1
2A	2A	k4
s	s	k7c7
bajonetem	bajonet	k1gInSc7
6H2	6H2	k4
</s>
<s>
RPK	RPK	kA
–	–	k?
kulomet	kulomet	k1gInSc1
pracující	pracující	k2eAgMnSc1d1
na	na	k7c6
principu	princip	k1gInSc6
AK-47	AK-47	k1gFnSc2
</s>
<s>
Délka	délka	k1gFnSc1
hlavně	hlavně	k9
</s>
<s>
415	#num#	k4
mm	mm	kA
</s>
<s>
Délka	délka	k1gFnSc1
zbraně	zbraň	k1gFnSc2
<g/>
:	:	kIx,
</s>
<s>
AK-47	AK-47	k4
</s>
<s>
870	#num#	k4
mm	mm	kA
</s>
<s>
AK-47	AK-47	k4
se	s	k7c7
sklopenou	sklopený	k2eAgFnSc7d1
opěrkou	opěrka	k1gFnSc7
</s>
<s>
651	#num#	k4
mm	mm	kA
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1
zbraně	zbraň	k1gFnSc2
<g/>
:	:	kIx,
</s>
<s>
s	s	k7c7
prázdným	prázdný	k2eAgInSc7d1
zásobníkem	zásobník	k1gInSc7
</s>
<s>
4,30	4,30	k4
kg	kg	kA
</s>
<s>
s	s	k7c7
plným	plný	k2eAgInSc7d1
zásobníkem	zásobník	k1gInSc7
</s>
<s>
4,88	4,88	k4
kg	kg	kA
</s>
<s>
Ran	Rana	k1gFnPc2
za	za	k7c4
sekundu	sekunda	k1gFnSc4
</s>
<s>
10	#num#	k4
</s>
<s>
Verze	verze	k1gFnSc1
</s>
<s>
AK-47	AK-47	k4
–	–	k?
verze	verze	k1gFnSc1
s	s	k7c7
pevnou	pevný	k2eAgFnSc7d1
pažbou	pažba	k1gFnSc7
</s>
<s>
AKN-47	AKN-47	k4
–	–	k?
verze	verze	k1gFnSc1
AK-47	AK-47	k1gFnSc1
s	s	k7c7
lištou	lišta	k1gFnSc7
na	na	k7c6
levé	levý	k2eAgFnSc6d1
straně	strana	k1gFnSc6
zbraně	zbraň	k1gFnSc2
pro	pro	k7c4
noční	noční	k2eAgInPc4d1
zaměřovače	zaměřovač	k1gInPc4
</s>
<s>
AKS-47	AKS-47	k4
–	–	k?
verze	verze	k1gFnSc1
se	s	k7c7
sklopnou	sklopný	k2eAgFnSc7d1
ramenní	ramenní	k2eAgFnSc7d1
opěrkou	opěrka	k1gFnSc7
</s>
<s>
AKSN-47	AKSN-47	k4
–	–	k?
verze	verze	k1gFnSc1
AKS-47	AKS-47	k1gFnSc1
s	s	k7c7
lištou	lišta	k1gFnSc7
na	na	k7c6
levé	levý	k2eAgFnSc6d1
straně	strana	k1gFnSc6
zbraně	zbraň	k1gFnSc2
pro	pro	k7c4
noční	noční	k2eAgInPc4d1
zaměřovače	zaměřovač	k1gInPc4
</s>
<s>
Modernizace	modernizace	k1gFnSc1
</s>
<s>
AKM	AKM	kA
<g/>
,	,	kIx,
7,62	7,62	k4
×	×	k?
39	#num#	k4
mm	mm	kA
–	–	k?
odlehčená	odlehčený	k2eAgFnSc1d1
<g/>
,	,	kIx,
modernější	moderní	k2eAgFnSc1d2
a	a	k8xC
levnější	levný	k2eAgFnSc1d2
varianta	varianta	k1gFnSc1
AK-47	AK-47	k1gFnSc2
</s>
<s>
RPK	RPK	kA
<g/>
,	,	kIx,
7,62	7,62	k4
×	×	k?
39	#num#	k4
mm	mm	kA
–	–	k?
kulometná	kulometný	k2eAgFnSc1d1
verze	verze	k1gFnSc1
odvozená	odvozený	k2eAgFnSc1d1
od	od	k7c2
AKM	AKM	kA
</s>
<s>
AK-74	AK-74	k4
5,45	5,45	k4
×	×	k?
39	#num#	k4
mm	mm	kA
–	–	k?
předělaná	předělaný	k2eAgFnSc1d1
verze	verze	k1gFnSc1
AKM	AKM	kA
na	na	k7c4
moderní	moderní	k2eAgNnSc4d1
střelivo	střelivo	k1gNnSc4
</s>
<s>
RPK-74	RPK-74	k4
–	–	k?
modernizace	modernizace	k1gFnSc2
RPK	RPK	kA
na	na	k7c4
moderní	moderní	k2eAgNnSc4d1
střelivo	střelivo	k1gNnSc4
</s>
<s>
AK-101	AK-101	k4
–	–	k?
modernizace	modernizace	k1gFnSc2
AK-74	AK-74	k1gFnSc2
pro	pro	k7c4
náboje	náboj	k1gInPc4
5,56	5,56	k4
×	×	k?
45	#num#	k4
mm	mm	kA
NATO	NATO	kA
</s>
<s>
AK-	AK-	k?
<g/>
103	#num#	k4
<g/>
/	/	kIx~
<g/>
AK-	AK-	k1gFnSc1
<g/>
104	#num#	k4
–	–	k?
modernizace	modernizace	k1gFnSc2
AK-74	AK-74	k1gFnSc2
</s>
<s>
AK-	AK-	k?
<g/>
107	#num#	k4
<g/>
/	/	kIx~
<g/>
AK-	AK-	k1gFnSc1
<g/>
108	#num#	k4
–	–	k?
modernizace	modernizace	k1gFnSc2
AK-74	AK-74	k1gFnSc2
</s>
<s>
AK-12	AK-12	k4
</s>
<s>
V	v	k7c6
populární	populární	k2eAgFnSc6d1
kultuře	kultura	k1gFnSc6
</s>
<s>
AK-47	AK-47	k4
se	se	k3xPyFc4
díky	díky	k7c3
své	svůj	k3xOyFgFnSc3
popularitě	popularita	k1gFnSc3
a	a	k8xC
exaktnímu	exaktní	k2eAgInSc3d1
vzhledu	vzhled	k1gInSc3
stal	stát	k5eAaPmAgMnS
i	i	k9
často	často	k6eAd1
používaným	používaný	k2eAgInSc7d1
kulturním	kulturní	k2eAgInSc7d1
prvkem	prvek	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
filmech	film	k1gInPc6
a	a	k8xC
videohrách	videohra	k1gFnPc6
(	(	kIx(
<g/>
jako	jako	k8xC,k8xS
například	například	k6eAd1
GTA	GTA	kA
<g/>
:	:	kIx,
San	San	k1gMnSc1
Andreas	Andreas	k1gMnSc1
<g/>
)	)	kIx)
bývá	bývat	k5eAaImIp3nS
často	často	k6eAd1
vyobrazen	vyobrazen	k2eAgInSc1d1
jako	jako	k8xC,k8xS
zbraň	zbraň	k1gFnSc1
používaná	používaný	k2eAgFnSc1d1
gangstery	gangster	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostal	dostat	k5eAaPmAgMnS
se	se	k3xPyFc4
i	i	k9
na	na	k7c4
vlajku	vlajka	k1gFnSc4
jihoafrického	jihoafrický	k2eAgInSc2d1
Mosambiku	Mosambik	k1gInSc2
jako	jako	k8xS,k8xC
odraz	odraz	k1gInSc4
skutečnosti	skutečnost	k1gFnSc2
<g/>
,	,	kIx,
že	že	k8xS
tamější	tamější	k2eAgFnSc2d1
revoluce	revoluce	k1gFnSc2
proti	proti	k7c3
portugalským	portugalský	k2eAgMnPc3d1
kolonizátorům	kolonizátor	k1gMnPc3
byla	být	k5eAaImAgNnP
z	z	k7c2
velké	velký	k2eAgFnSc2d1
části	část	k1gFnSc2
podporována	podporovat	k5eAaImNgFnS
východním	východní	k2eAgInSc7d1
blokem	blok	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
AK-47	AK-47	k1gMnSc1
se	se	k3xPyFc4
také	také	k9
používá	používat	k5eAaImIp3nS
jako	jako	k9
jméno	jméno	k1gNnSc1
kroku	krok	k1gInSc2
v	v	k7c6
dancehallovém	dancehallový	k2eAgInSc6d1
tanci	tanec	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Galerie	galerie	k1gFnSc1
</s>
<s>
Americký	americký	k2eAgInSc1d1
mariňák	mariňák	k?
v	v	k7c6
Iráku	Irák	k1gInSc6
<g/>
,	,	kIx,
zkoušející	zkoušející	k1gMnPc1
si	se	k3xPyFc3
palbu	palba	k1gFnSc4
z	z	k7c2
AKMS	AKMS	kA
</s>
<s>
Typ	typ	k1gInSc1
56	#num#	k4
a	a	k8xC
AKS-47	AKS-47	k1gMnSc1
</s>
<s>
AK-47	AK-47	k4
při	při	k7c6
čistění	čistění	k1gNnSc6
</s>
<s>
Mířidla	mířidlo	k1gNnPc1
čínské	čínský	k2eAgFnSc2d1
Type	typ	k1gInSc5
56	#num#	k4
</s>
<s>
Bajonet	bajonet	k1gInSc1
určený	určený	k2eAgInSc1d1
pro	pro	k7c4
použití	použití	k1gNnSc4
na	na	k7c6
puškách	puška	k1gFnPc6
typu	typ	k1gInSc2
Kalašnikov	kalašnikov	k1gInSc4
</s>
<s>
Plastové	plastový	k2eAgInPc1d1
zásobníky	zásobník	k1gInPc1
pro	pro	k7c4
AK-47	AK-47	k1gFnSc4
</s>
<s>
Náboje	náboj	k1gInSc2
7,62	7,62	k4
×	×	k?
39	#num#	k4
mm	mm	kA
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
POPENKER	POPENKER	kA
<g/>
,	,	kIx,
Maksim	Maksim	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kalashnikov	Kalashnikov	k1gInSc1
AK	AK	kA
(	(	kIx(
<g/>
AK-	AK-	k1gFnSc1
<g/>
47	#num#	k4
<g/>
)	)	kIx)
AKS	AKS	kA
<g/>
,	,	kIx,
AKM	AKM	kA
and	and	k?
AKMS	AKMS	kA
assault	assault	k1gMnSc1
rifles	rifles	k1gMnSc1
(	(	kIx(
<g/>
USSR	USSR	kA
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
5	#num#	k4
February	Februara	k1gFnSc2
2009	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
П	П	k?
<g/>
,	,	kIx,
М	М	k?
Р	Р	k?
Ш	Ш	k?
в	в	k?
м	м	k?
<g/>
.	.	kIx.
М	М	k?
<g/>
:	:	kIx,
А	А	k?
<g/>
,	,	kIx,
П	П	k?
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
304	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
5	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
23816	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
А	А	k?
К	К	k?
А	А	k?
А	А	k?
А	А	k?
А	А	k?
(	(	kIx(
<g/>
С	С	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
BOLOTIN	BOLOTIN	kA
<g/>
,	,	kIx,
David	David	k1gMnSc1
Naumovich	Naumovich	k1gMnSc1
<g/>
;	;	kIx,
NAFTUL	NAFTUL	kA
<g/>
'	'	kIx"
<g/>
EFF	EFF	kA
<g/>
,	,	kIx,
Igor	Igor	k1gMnSc1
F	F	kA
<g/>
;	;	kIx,
WALTER	Walter	k1gMnSc1
<g/>
,	,	kIx,
John	John	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soviet	Soviet	k1gInSc1
small-arms	small-arms	k1gInSc4
and	and	k?
ammunition	ammunition	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hyvinkää	Hyvinkää	k1gFnSc1
<g/>
:	:	kIx,
Suomen	Suomen	k1gInSc1
asemuseosäätiö	asemuseosäätiö	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9519718419	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
9789519718415	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
English	English	k1gMnSc1
<g/>
)	)	kIx)
OCLC	OCLC	kA
<g/>
:	:	kIx,
58159533	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
AK-47	AK-47	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Legendární	legendární	k2eAgMnSc1d1
„	„	k?
<g/>
kalašnikov	kalašnikov	k1gInSc1
<g/>
“	“	k?
se	se	k3xPyFc4
prý	prý	k9
chystá	chystat	k5eAaImIp3nS
do	do	k7c2
důchodu	důchod	k1gInSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Legendární	legendární	k2eAgInPc1d1
"	"	kIx"
<g/>
kalašnikov	kalašnikov	k1gInSc1
<g/>
"	"	kIx"
se	se	k3xPyFc4
prý	prý	k9
chystá	chystat	k5eAaImIp3nS
do	do	k7c2
důchodu	důchod	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Týden	týden	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
2016	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Kalašnikov	kalašnikov	k1gInSc1
slaví	slavit	k5eAaImIp3nS
60	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozebrali	rozebrat	k5eAaPmAgMnP
jsme	být	k5eAaImIp1nP
nejslavnější	slavný	k2eAgInSc4d3
samopal	samopal	k1gInSc4
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
2016	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Bolotin	Bolotina	k1gFnPc2
1995	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
123	#num#	k4
<g/>
–	–	k?
<g/>
124	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
An	An	k1gMnSc1
interview	interview	k1gNnSc2
with	with	k1gMnSc1
Mikhail	Mikhail	k1gMnSc1
Kalashnikov	Kalashnikov	k1gInSc1
<g/>
,	,	kIx,
Robert	Robert	k1gMnSc1
Fisk	Fisk	k1gMnSc1
<g/>
,	,	kIx,
The	The	k1gMnSc1
Independent	independent	k1gMnSc1
(	(	kIx(
<g/>
centrist	centrist	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
London	London	k1gMnSc1
<g/>
,	,	kIx,
England	England	k1gInSc1
<g/>
.	.	kIx.
22	#num#	k4
April	April	k1gInSc1
2001	#num#	k4
<g/>
.	.	kIx.
http://www.worldpress.org/cover5.htm	http://www.worldpress.org/cover5.htma	k1gFnPc2
October	Octobero	k1gNnPc2
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
https://web.archive.org/web/20161001104001/http://www.worldpress.org/cover5.htm	https://web.archive.org/web/20161001104001/http://www.worldpress.org/cover5.htm	k6eAd1
Archivováno	archivovat	k5eAaBmNgNnS
1	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
2016	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
↑	↑	k?
AK-47	AK-47	k1gMnSc1
Inventor	Inventor	k1gMnSc1
Doesn	Doesn	k1gMnSc1
<g/>
'	'	kIx"
<g/>
t	t	k?
Lose	los	k1gInSc6
Sleep	Sleep	k1gInSc1
Over	Over	k1gInSc1
Havoc	Havoc	k1gInSc1
Wrought	Wrought	k2eAgInSc1d1
With	With	k1gInSc1
His	his	k1gNnSc1
Invention	Invention	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
An	An	k1gMnPc3
interview	interview	k1gNnSc2
with	with	k1gMnSc1
Mikhail	Mikhail	k1gMnSc1
Kalashnikov	Kalashnikov	k1gInSc4
Archivováno	archivovat	k5eAaBmNgNnS
14	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2014	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Associated	Associated	k1gInSc1
Press	Press	k1gInSc4
via	via	k7c4
Fox	fox	k1gInSc4
News	Newsa	k1gFnPc2
Channel	Channela	k1gFnPc2
(	(	kIx(
<g/>
6	#num#	k4
July	Jula	k1gFnSc2
2007	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Retrieved	Retrieved	k1gInSc1
25	#num#	k4
November	Novembero	k1gNnPc2
2015	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Popenker	Popenker	k1gInSc1
<g/>
,	,	kIx,
Maksim	Maksim	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kalashnikov	Kalashnikov	k1gInSc1
AK	AK	kA
(	(	kIx(
<g/>
AK-	AK-	k1gFnSc1
<g/>
47	#num#	k4
<g/>
)	)	kIx)
AKS	AKS	kA
<g/>
,	,	kIx,
AKM	AKM	kA
and	and	k?
AKMS	AKMS	kA
assault	assault	k1gMnSc1
rifles	rifles	k1gMnSc1
(	(	kIx(
<g/>
USSR	USSR	kA
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
5	#num#	k4
February	Februara	k1gFnSc2
2009	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kuptsov	Kuptsovo	k1gNnPc2
<g/>
,	,	kIx,
Andrei	Andrea	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moscow	Moscow	k1gFnSc1
<g/>
:	:	kIx,
Kraft	Kraft	k1gInSc1
<g/>
+	+	kIx~
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
93675	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
25	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
262	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Russian	Russian	k1gInSc1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
KALASHNIKOV	KALASHNIKOV	kA
<g/>
,	,	kIx,
Mikhail	Mikhail	k1gInSc1
<g/>
;	;	kIx,
JOLY	jola	k1gFnPc1
<g/>
,	,	kIx,
Elena	Elena	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Gun	Gun	k1gMnSc1
that	that	k1gMnSc1
Changed	Changed	k1gMnSc1
the	the	k?
World	World	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Polity	polit	k2eAgInPc1d1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
745636918	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
9780745636917	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
60	#num#	k4
<g/>
–	–	k?
<g/>
62	#num#	k4
<g/>
,	,	kIx,
68	#num#	k4
a	a	k8xC
násl	násl	k1gMnSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
163	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
HODGES	HODGES	kA
<g/>
,	,	kIx,
Michael	Michael	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
AK	AK	kA
<g/>
47	#num#	k4
<g/>
:	:	kIx,
The	The	k1gFnSc1
Story	story	k1gFnSc2
of	of	k?
a	a	k8xC
Gun	Gun	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
MacAdam	MacAdam	k1gInSc4
<g/>
/	/	kIx~
<g/>
Cage	Cagus	k1gMnSc5
Pub	Pub	k1gMnSc5
<g/>
.	.	kIx.
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
1596922869	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
9781596922860	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
184	#num#	k4
<g/>
–	–	k?
<g/>
188	#num#	k4
a	a	k8xC
násl	násl	k1gMnSc1
<g/>
..	..	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
KOLLER	KOLLER	kA
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
autorství	autorství	k1gNnSc2
slavné	slavný	k2eAgFnSc2d1
pušky	puška	k1gFnSc2
AK-47	AK-47	k1gMnSc2
pouze	pouze	k6eAd1
oficiální	oficiální	k2eAgFnSc7d1
legendou	legenda	k1gFnSc7
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
Securitymagazin	Securitymagazin	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017-09-06	2017-09-06	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
VEVERA	vever	k1gMnSc4
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spor	spor	k1gInSc1
o	o	k7c4
otcovství	otcovství	k1gNnSc4
AK-	AK-	k1gFnSc2
<g/>
47	#num#	k4
<g/>
:	:	kIx,
Kalašnikov	kalašnikov	k1gInSc1
<g/>
,	,	kIx,
nebo	nebo	k8xC
Schmeisser	Schmeisser	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014-01-21	2014-01-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ROTTMAN	ROTTMAN	kA
<g/>
,	,	kIx,
Gordon	Gordon	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
AK-	AK-	k1gFnSc1
<g/>
47	#num#	k4
<g/>
:	:	kIx,
Kalashnikov-series	Kalashnikov-series	k1gMnSc1
assault	assault	k1gMnSc1
rifles	rifles	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Bloomsbury	Bloomsbura	k1gFnSc2
Publishing	Publishing	k1gInSc1
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
.	.	kIx.
80	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9781849088350	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
browningmgs	browningmgs	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
http://www.glocktalk.com/threads/the-different-types-of-aks.1183387/	http://www.glocktalk.com/threads/the-different-types-of-aks.1183387/	k4
<g/>
↑	↑	k?
Kalašnikovy	kalašnikov	k1gInPc7
se	se	k3xPyFc4
nelegálně	legálně	k6eNd1
vyrábějí	vyrábět	k5eAaImIp3nP
v	v	k7c6
třiceti	třicet	k4xCc6
zemích	zem	k1gFnPc6
<g/>
↑	↑	k?
Ruskému	ruský	k2eAgInSc3d1
výrobci	výrobce	k1gMnPc1
kalašnikovů	kalašnikov	k1gInPc2
hrozí	hrozit	k5eAaImIp3nP
bankrot	bankrot	k1gInSc4
<g/>
↑	↑	k?
Rusko	Rusko	k1gNnSc1
požaduje	požadovat	k5eAaImIp3nS
zavedení	zavedení	k1gNnSc4
licencí	licence	k1gFnPc2
na	na	k7c4
výrobu	výroba	k1gFnSc4
Kalašnikovů	kalašnikov	k1gInPc2
<g/>
↑	↑	k?
Výrobce	výrobce	k1gMnSc1
zbraní	zbraň	k1gFnPc2
Kalašnikov	kalašnikov	k1gInSc4
se	se	k3xPyFc4
dostal	dostat	k5eAaPmAgMnS
po	po	k7c6
7	#num#	k4
letech	léto	k1gNnPc6
do	do	k7c2
zisku	zisk	k1gInSc2
<g/>
↑	↑	k?
Koncern	koncern	k1gInSc1
Kalašnikov	kalašnikov	k1gInSc1
<g/>
:	:	kIx,
USA	USA	kA
nemá	mít	k5eNaImIp3nS
povolení	povolení	k1gNnPc4
k	k	k7c3
výrobě	výroba	k1gFnSc3
AK-	AK-	k1gFnSc1
<g/>
47	#num#	k4
<g/>
↑	↑	k?
Kvůli	kvůli	k7c3
sankcím	sankce	k1gFnPc3
budou	být	k5eAaImBp3nP
mít	mít	k5eAaImF
kalašnikovy	kalašnikov	k1gInPc1
v	v	k7c6
Americe	Amerika	k1gFnSc6
štítek	štítek	k1gInSc1
„	„	k?
<g/>
vyrobeno	vyroben	k2eAgNnSc1d1
v	v	k7c6
USA	USA	kA
<g/>
“	“	k?
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
ROTTMAN	ROTTMAN	kA
<g/>
,	,	kIx,
Gordon	Gordon	k1gInSc4
L.	L.	kA
Útočná	útočný	k2eAgFnSc1d1
puška	puška	k1gFnSc1
AK	AK	kA
<g/>
–	–	k?
<g/>
47	#num#	k4
a	a	k8xC
její	její	k3xOp3gFnPc4
modifikace	modifikace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Grada	Grada	k1gFnSc1
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
.	.	kIx.
80	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
247	#num#	k4
<g/>
-	-	kIx~
<g/>
4363	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Galerie	galerie	k1gFnSc1
AK-47	AK-47	k1gMnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
AK-47	AK-47	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
The	The	k1gFnSc1
AK-47	AK-47	k1gMnSc1
Kalashnikov	Kalashnikov	k1gInSc1
Museum	museum	k1gNnSc1
Virtual	Virtual	k1gInSc1
Tour	Tour	k1gMnSc1
Archivováno	archivován	k2eAgNnSc4d1
16	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
2008	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gMnSc5
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
AK-47	AK-47	k1gFnSc1
na	na	k7c4
Modern	Modern	k1gNnSc4
Firearms	Firearmsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Video	video	k1gNnSc1
na	na	k7c6
serveru	server	k1gInSc6
YouTube	YouTub	k1gInSc5
stručně	stručně	k6eAd1
seznamující	seznamující	k2eAgFnSc6d1
s	s	k7c7
původem	původ	k1gInSc7
a	a	k8xC
atributy	atribut	k1gInPc1
zbraně	zbraň	k1gFnSc2
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Rodokmen	rodokmen	k1gInSc1
legendy	legenda	k1gFnSc2
AK	AK	kA
47	#num#	k4
<g/>
,	,	kIx,
historie	historie	k1gFnSc1
a	a	k8xC
popis	popis	k1gInSc1
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
AK-47	AK-47	k1gFnSc1
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Kalašnikov	kalašnikov	k1gInSc1
slaví	slavit	k5eAaImIp3nS
60	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozebrali	rozebrat	k5eAaPmAgMnP
jsme	být	k5eAaImIp1nP
nejslavnější	slavný	k2eAgInSc4d3
samopal	samopal	k1gInSc4
na	na	k7c6
světě	svět	k1gInSc6
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Příběhy	příběh	k1gInPc1
zbraní	zbraň	k1gFnPc2
–	–	k?
AK-47	AK-47	k1gMnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4240099-5	4240099-5	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85003036	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85003036	#num#	k4
</s>
