<s>
Petr	Petr	k1gMnSc1	Petr
Hájek	Hájek	k1gMnSc1	Hájek
(	(	kIx(	(
<g/>
*	*	kIx~	*
23	[number]	k4	23
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1951	[number]	k4	1951
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
novinář	novinář	k1gMnSc1	novinář
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
tiskový	tiskový	k2eAgMnSc1d1	tiskový
mluvčí	mluvčí	k1gMnSc1	mluvčí
Václava	Václav	k1gMnSc2	Václav
Klause	Klaus	k1gMnSc2	Klaus
a	a	k8xC	a
bývalý	bývalý	k2eAgMnSc1d1	bývalý
zástupce	zástupce	k1gMnSc1	zástupce
vedoucího	vedoucí	k1gMnSc2	vedoucí
Kanceláře	kancelář	k1gFnSc2	kancelář
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
pro	pro	k7c4	pro
oblast	oblast	k1gFnSc4	oblast
komunikace	komunikace	k1gFnSc2	komunikace
a	a	k8xC	a
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
synem	syn	k1gMnSc7	syn
komunistického	komunistický	k2eAgMnSc4d1	komunistický
literárního	literární	k2eAgMnSc4d1	literární
kritika	kritik	k1gMnSc4	kritik
Jiřího	Jiří	k1gMnSc4	Jiří
Hájka	Hájek	k1gMnSc4	Hájek
(	(	kIx(	(
<g/>
1919	[number]	k4	1919
<g/>
–	–	k?	–
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
někdejšího	někdejší	k2eAgMnSc4d1	někdejší
šéfredaktora	šéfredaktor	k1gMnSc4	šéfredaktor
časopisu	časopis	k1gInSc2	časopis
Plamen	plamen	k1gInSc1	plamen
a	a	k8xC	a
normalizačního	normalizační	k2eAgNnSc2d1	normalizační
kulturního	kulturní	k2eAgNnSc2d1	kulturní
periodika	periodikum	k1gNnSc2	periodikum
Tvorba	tvorba	k1gFnSc1	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Vyučil	vyučit	k5eAaPmAgInS	vyučit
se	se	k3xPyFc4	se
elektromontérem	elektromontér	k1gMnSc7	elektromontér
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
absolvoval	absolvovat	k5eAaPmAgInS	absolvovat
gymnázium	gymnázium	k1gNnSc4	gymnázium
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
základní	základní	k2eAgFnSc2d1	základní
vojenské	vojenský	k2eAgFnSc2d1	vojenská
služby	služba	k1gFnSc2	služba
působil	působit	k5eAaImAgMnS	působit
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Čs	čs	kA	čs
<g/>
.	.	kIx.	.
voják	voják	k1gMnSc1	voják
a	a	k8xC	a
poté	poté	k6eAd1	poté
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1974	[number]	k4	1974
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Květy	Květa	k1gFnSc2	Květa
<g/>
,	,	kIx,	,
nejdříve	dříve	k6eAd3	dříve
v	v	k7c6	v
kulturní	kulturní	k2eAgFnSc6d1	kulturní
rubrice	rubrika	k1gFnSc6	rubrika
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
ve	v	k7c6	v
vnitropolitickém	vnitropolitický	k2eAgNnSc6d1	vnitropolitické
oddělení	oddělení	k1gNnSc6	oddělení
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
vedoucí	vedoucí	k1gMnSc1	vedoucí
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1973	[number]	k4	1973
dálkově	dálkově	k6eAd1	dálkově
studoval	studovat	k5eAaImAgMnS	studovat
scenáristiku	scenáristika	k1gFnSc4	scenáristika
a	a	k8xC	a
dramaturgii	dramaturgie	k1gFnSc4	dramaturgie
na	na	k7c4	na
FAMU	FAMU	kA	FAMU
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
(	(	kIx(	(
<g/>
absolvoval	absolvovat	k5eAaPmAgInS	absolvovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
spoluzakládal	spoluzakládat	k5eAaImAgInS	spoluzakládat
společenský	společenský	k2eAgInSc1d1	společenský
týdeník	týdeník	k1gInSc1	týdeník
Reflex	reflex	k1gInSc1	reflex
a	a	k8xC	a
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
byl	být	k5eAaImAgMnS	být
jeho	jeho	k3xOp3gMnSc7	jeho
šéfredaktorem	šéfredaktor	k1gMnSc7	šéfredaktor
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
krátkém	krátký	k2eAgInSc6d1	krátký
čase	čas	k1gInSc6	čas
časopis	časopis	k1gInSc1	časopis
prodal	prodat	k5eAaPmAgMnS	prodat
a	a	k8xC	a
věnoval	věnovat	k5eAaPmAgMnS	věnovat
se	se	k3xPyFc4	se
podnikání	podnikání	k1gNnSc4	podnikání
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
reklamy	reklama	k1gFnSc2	reklama
a	a	k8xC	a
marketingu	marketing	k1gInSc2	marketing
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
s	s	k7c7	s
Václavem	Václav	k1gMnSc7	Václav
Klausem	Klaus	k1gMnSc7	Klaus
<g/>
,	,	kIx,	,
podílel	podílet	k5eAaImAgMnS	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
kampani	kampaň	k1gFnSc6	kampaň
ODS	ODS	kA	ODS
a	a	k8xC	a
po	po	k7c6	po
Klausově	Klausův	k2eAgInSc6d1	Klausův
zvolení	zvolení	k1gNnSc2	zvolení
prezidentem	prezident	k1gMnSc7	prezident
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2003	[number]	k4	2003
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
ředitelem	ředitel	k1gMnSc7	ředitel
tiskového	tiskový	k2eAgInSc2d1	tiskový
odboru	odbor	k1gInSc2	odbor
Kanceláře	kancelář	k1gFnSc2	kancelář
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
prvního	první	k4xOgMnSc2	první
tiskového	tiskový	k2eAgMnSc2d1	tiskový
mluvčího	mluvčí	k1gMnSc2	mluvčí
Tomáše	Tomáš	k1gMnSc2	Tomáš
Klvani	Klvaň	k1gFnSc3	Klvaň
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
také	také	k6eAd1	také
tiskovým	tiskový	k2eAgMnSc7d1	tiskový
mluvčím	mluvčí	k1gMnSc7	mluvčí
prezidenta	prezident	k1gMnSc2	prezident
Klause	Klaus	k1gMnSc2	Klaus
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jej	on	k3xPp3gMnSc4	on
vystřídal	vystřídat	k5eAaPmAgMnS	vystřídat
Radim	Radim	k1gMnSc1	Radim
Ochvat	ochvat	k1gInSc4	ochvat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
získal	získat	k5eAaPmAgMnS	získat
anticenu	anticen	k2eAgFnSc4d1	anticena
"	"	kIx"	"
<g/>
Němá	němý	k2eAgFnSc1d1	němá
barikáda	barikáda	k1gFnSc1	barikáda
<g/>
"	"	kIx"	"
pro	pro	k7c4	pro
nejméně	málo	k6eAd3	málo
oblíbeného	oblíbený	k2eAgMnSc4d1	oblíbený
mluvčího	mluvčí	k1gMnSc4	mluvčí
roku	rok	k1gInSc2	rok
mezi	mezi	k7c7	mezi
vybranými	vybraný	k2eAgMnPc7d1	vybraný
novináři	novinář	k1gMnPc7	novinář
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
zástupce	zástupce	k1gMnSc1	zástupce
vedoucího	vedoucí	k1gMnSc2	vedoucí
Kanceláře	kancelář	k1gFnSc2	kancelář
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
pro	pro	k7c4	pro
oblast	oblast	k1gFnSc4	oblast
komunikace	komunikace	k1gFnSc2	komunikace
a	a	k8xC	a
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1999	[number]	k4	1999
<g/>
–	–	k?	–
<g/>
2004	[number]	k4	2004
byl	být	k5eAaImAgMnS	být
členem	člen	k1gMnSc7	člen
správní	správní	k2eAgFnSc2d1	správní
rady	rada	k1gFnSc2	rada
Nadačního	nadační	k2eAgInSc2d1	nadační
fondu	fond	k1gInSc2	fond
Václava	Václav	k1gMnSc2	Václav
Klause	Klaus	k1gMnSc2	Klaus
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
je	být	k5eAaImIp3nS	být
členem	člen	k1gInSc7	člen
správní	správní	k2eAgFnSc2d1	správní
rady	rada	k1gFnSc2	rada
Nadačního	nadační	k2eAgInSc2d1	nadační
fondu	fond	k1gInSc2	fond
manželů	manžel	k1gMnPc2	manžel
Livie	Livie	k1gFnSc2	Livie
a	a	k8xC	a
Václava	Václav	k1gMnSc2	Václav
Klausových	Klausových	k2eAgMnSc2d1	Klausových
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
dubna	duben	k1gInSc2	duben
2013	[number]	k4	2013
je	být	k5eAaImIp3nS	být
šéfredaktorem	šéfredaktor	k1gMnSc7	šéfredaktor
internetového	internetový	k2eAgNnSc2d1	internetové
periodika	periodikum	k1gNnSc2	periodikum
Protiproud	protiproud	k1gInSc1	protiproud
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
založil	založit	k5eAaPmAgInS	založit
jako	jako	k9	jako
platformu	platforma	k1gFnSc4	platforma
pro	pro	k7c4	pro
informace	informace	k1gFnPc4	informace
a	a	k8xC	a
názory	názor	k1gInPc4	názor
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
podle	podle	k7c2	podle
Hájka	Hájek	k1gMnSc2	Hájek
nedostávají	dostávat	k5eNaImIp3nP	dostávat
prostor	prostor	k1gInSc4	prostor
v	v	k7c6	v
hlavním	hlavní	k2eAgInSc6d1	hlavní
proudu	proud	k1gInSc6	proud
sdělovacích	sdělovací	k2eAgInPc2d1	sdělovací
prostředků	prostředek	k1gInPc2	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Proslul	proslout	k5eAaPmAgMnS	proslout
mj.	mj.	kA	mj.
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejenže	nejenže	k6eAd1	nejenže
je	být	k5eAaImIp3nS	být
těžkým	těžký	k2eAgInSc7d1	těžký
kuřákem	kuřák	k1gInSc7	kuřák
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
že	že	k8xS	že
také	také	k9	také
popírá	popírat	k5eAaImIp3nS	popírat
důkazy	důkaz	k1gInPc4	důkaz
škodlivosti	škodlivost	k1gFnSc2	škodlivost
kouření	kouření	k1gNnSc2	kouření
pro	pro	k7c4	pro
lidské	lidský	k2eAgNnSc4d1	lidské
zdraví	zdraví	k1gNnSc4	zdraví
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jeho	jeho	k3xOp3gNnPc2	jeho
slov	slovo	k1gNnPc2	slovo
"	"	kIx"	"
<g/>
nikdo	nikdo	k3yNnSc1	nikdo
nikdy	nikdy	k6eAd1	nikdy
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
a	a	k8xC	a
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
kouření	kouření	k1gNnSc1	kouření
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
škodlivé	škodlivý	k2eAgFnPc4d1	škodlivá
<g/>
.	.	kIx.	.
</s>
<s>
Já	já	k3xPp1nSc1	já
významně	významně	k6eAd1	významně
pochybuji	pochybovat	k5eAaImIp1nS	pochybovat
o	o	k7c6	o
zdravotním	zdravotní	k2eAgNnSc6d1	zdravotní
riziku	riziko	k1gNnSc6	riziko
kouření	kouření	k1gNnSc2	kouření
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Mediální	mediální	k2eAgInSc4d1	mediální
rozruch	rozruch	k1gInSc4	rozruch
vzbudily	vzbudit	k5eAaPmAgInP	vzbudit
Hájkovy	Hájkův	k2eAgInPc1d1	Hájkův
výroky	výrok	k1gInPc1	výrok
týkající	týkající	k2eAgFnSc2d1	týkající
se	se	k3xPyFc4	se
evoluční	evoluční	k2eAgFnSc2d1	evoluční
teorie	teorie	k1gFnSc2	teorie
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
přednesl	přednést	k5eAaPmAgMnS	přednést
za	za	k7c2	za
účasti	účast	k1gFnSc2	účast
předních	přední	k2eAgMnPc2d1	přední
českých	český	k2eAgMnPc2d1	český
evolučních	evoluční	k2eAgMnPc2d1	evoluční
biologů	biolog	k1gMnPc2	biolog
na	na	k7c6	na
semináři	seminář	k1gInSc6	seminář
"	"	kIx"	"
<g/>
Darwin	Darwin	k1gMnSc1	Darwin
a	a	k8xC	a
darwinismus	darwinismus	k1gInSc1	darwinismus
–	–	k?	–
věda	věda	k1gFnSc1	věda
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
ideologie	ideologie	k1gFnSc1	ideologie
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
pořádalo	pořádat	k5eAaImAgNnS	pořádat
20	[number]	k4	20
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2009	[number]	k4	2009
Centrum	centrum	k1gNnSc1	centrum
pro	pro	k7c4	pro
ekonomiku	ekonomika	k1gFnSc4	ekonomika
a	a	k8xC	a
politiku	politika	k1gFnSc4	politika
a	a	k8xC	a
který	který	k3yQgInSc4	který
moderoval	moderovat	k5eAaBmAgMnS	moderovat
prezident	prezident	k1gMnSc1	prezident
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
<g/>
.	.	kIx.	.
</s>
<s>
Hájek	Hájek	k1gMnSc1	Hájek
zdůrazňoval	zdůrazňovat	k5eAaImAgMnS	zdůrazňovat
příbuznost	příbuznost	k1gFnSc4	příbuznost
darwinismu	darwinismus	k1gInSc2	darwinismus
s	s	k7c7	s
marxismem	marxismus	k1gInSc7	marxismus
a	a	k8xC	a
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
nesouhlas	nesouhlas	k1gInSc4	nesouhlas
s	s	k7c7	s
tzv.	tzv.	kA	tzv.
vědeckým	vědecký	k2eAgInSc7d1	vědecký
darwinismem	darwinismus	k1gInSc7	darwinismus
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
ovšem	ovšem	k9	ovšem
nemá	mít	k5eNaImIp3nS	mít
podle	podle	k7c2	podle
Hájka	Hájek	k1gMnSc2	Hájek
s	s	k7c7	s
názory	názor	k1gInPc1	názor
samotného	samotný	k2eAgMnSc2d1	samotný
Charlese	Charles	k1gMnSc2	Charles
Darwina	Darwin	k1gMnSc2	Darwin
téměř	téměř	k6eAd1	téměř
nic	nic	k3yNnSc1	nic
společného	společný	k2eAgNnSc2d1	společné
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
Hájek	Hájek	k1gMnSc1	Hájek
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
i	i	k9	i
moderní	moderní	k2eAgInSc1d1	moderní
antropocentrický	antropocentrický	k2eAgInSc1d1	antropocentrický
pohled	pohled	k1gInSc1	pohled
na	na	k7c4	na
svět	svět	k1gInSc4	svět
a	a	k8xC	a
Descartův	Descartův	k2eAgInSc4d1	Descartův
skepticismus	skepticismus	k1gInSc4	skepticismus
<g/>
.	.	kIx.	.
</s>
<s>
Hájek	Hájek	k1gMnSc1	Hájek
v	v	k7c6	v
příspěvku	příspěvek	k1gInSc6	příspěvek
nazvaném	nazvaný	k2eAgInSc6d1	nazvaný
"	"	kIx"	"
<g/>
Nevím	vědět	k5eNaImIp1nS	vědět
jak	jak	k8xS	jak
vy	vy	k3xPp2nPc1	vy
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
já	já	k3xPp1nSc1	já
z	z	k7c2	z
opice	opice	k1gFnSc2	opice
nepocházím	pocházet	k5eNaImIp1nS	pocházet
<g/>
"	"	kIx"	"
darwinismu	darwinismus	k1gInSc6	darwinismus
vytkl	vytknout	k5eAaPmAgMnS	vytknout
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
slepě	slepě	k6eAd1	slepě
a	a	k8xC	a
hluše	hluš	k1gFnSc2wB	hluš
opakuje	opakovat	k5eAaImIp3nS	opakovat
četné	četný	k2eAgInPc4d1	četný
nesmysly	nesmysl	k1gInPc4	nesmysl
a	a	k8xC	a
exaktními	exaktní	k2eAgInPc7d1	exaktní
důkazy	důkaz	k1gInPc7	důkaz
překonané	překonaný	k2eAgFnSc2d1	překonaná
evolucionistické	evolucionistický	k2eAgFnSc2d1	evolucionistická
mantry	mantra	k1gFnSc2	mantra
jako	jako	k8xC	jako
nedotknutelná	nedotknutelná	k?	nedotknutelná
tabu	tabu	k1gNnSc1	tabu
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
darwinismus	darwinismus	k1gInSc1	darwinismus
Hájek	hájka	k1gFnPc2	hájka
kladl	klást	k5eAaImAgInS	klást
do	do	k7c2	do
protikladu	protiklad	k1gInSc2	protiklad
s	s	k7c7	s
náboženstvím	náboženství	k1gNnSc7	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Zúčastnění	zúčastněný	k2eAgMnPc1d1	zúčastněný
vědci	vědec	k1gMnPc1	vědec
byli	být	k5eAaImAgMnP	být
původně	původně	k6eAd1	původně
pozváni	pozvat	k5eAaPmNgMnP	pozvat
na	na	k7c4	na
seminář	seminář	k1gInSc4	seminář
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Charles	Charles	k1gMnSc1	Charles
Darwin	Darwin	k1gMnSc1	Darwin
dvě	dva	k4xCgFnPc4	dva
stě	sto	k4xCgFnPc1	sto
let	léto	k1gNnPc2	léto
od	od	k7c2	od
narození	narození	k1gNnSc2	narození
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
moderovat	moderovat	k5eAaBmF	moderovat
Cyril	Cyril	k1gMnSc1	Cyril
Höschl	Höschl	k1gFnSc2	Höschl
a	a	k8xC	a
kterého	který	k3yRgInSc2	který
se	se	k3xPyFc4	se
Hájek	Hájek	k1gMnSc1	Hájek
ani	ani	k8xC	ani
Klaus	Klaus	k1gMnSc1	Klaus
neměli	mít	k5eNaImAgMnP	mít
účastnit	účastnit	k5eAaImF	účastnit
<g/>
;	;	kIx,	;
změna	změna	k1gFnSc1	změna
byla	být	k5eAaImAgFnS	být
provedena	provést	k5eAaPmNgFnS	provést
několik	několik	k4yIc1	několik
dní	den	k1gInPc2	den
před	před	k7c7	před
konáním	konání	k1gNnSc7	konání
semináře	seminář	k1gInSc2	seminář
a	a	k8xC	a
kvůli	kvůli	k7c3	kvůli
ní	on	k3xPp3gFnSc3	on
byl	být	k5eAaImAgInS	být
odmítnut	odmítnut	k2eAgInSc1d1	odmítnut
objednaný	objednaný	k2eAgInSc1d1	objednaný
příspěvek	příspěvek	k1gInSc1	příspěvek
docenta	docent	k1gMnSc2	docent
Frynty	Frynta	k1gFnSc2	Frynta
<g/>
.	.	kIx.	.
</s>
<s>
Hájkovu	Hájkův	k2eAgInSc3d1	Hájkův
příspěvku	příspěvek	k1gInSc3	příspěvek
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
dostalo	dostat	k5eAaPmAgNnS	dostat
široké	široký	k2eAgFnPc4d1	široká
publicity	publicita	k1gFnPc4	publicita
ve	v	k7c6	v
významných	významný	k2eAgNnPc6d1	významné
médiích	médium	k1gNnPc6	médium
a	a	k8xC	a
proti	proti	k7c3	proti
příspěvku	příspěvek	k1gInSc3	příspěvek
se	se	k3xPyFc4	se
postavili	postavit	k5eAaPmAgMnP	postavit
významní	významný	k2eAgMnPc1d1	významný
čeští	český	k2eAgMnPc1d1	český
vědci	vědec	k1gMnPc1	vědec
jak	jak	k6eAd1	jak
formou	forma	k1gFnSc7	forma
komentáře	komentář	k1gInSc2	komentář
v	v	k7c6	v
těchto	tento	k3xDgNnPc6	tento
médiích	médium	k1gNnPc6	médium
(	(	kIx(	(
<g/>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Flegr	Flegr	k1gMnSc1	Flegr
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Zrzavý	zrzavý	k2eAgMnSc1d1	zrzavý
<g/>
,	,	kIx,	,
Anton	Anton	k1gMnSc1	Anton
Markoš	Markoš	k1gMnSc1	Markoš
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
např.	např.	kA	např.
otevřeným	otevřený	k2eAgInSc7d1	otevřený
dopisem	dopis	k1gInSc7	dopis
prezidentu	prezident	k1gMnSc3	prezident
Klausovi	Klaus	k1gMnSc3	Klaus
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
blogu	blog	k1gInSc6	blog
zveřejnila	zveřejnit	k5eAaPmAgFnS	zveřejnit
bioložka	bioložka	k1gFnSc1	bioložka
Fatima	Fatima	k1gFnSc1	Fatima
Cvrčková	Cvrčková	k1gFnSc1	Cvrčková
a	a	k8xC	a
o	o	k7c6	o
němž	jenž	k3xRgInSc6	jenž
také	také	k9	také
referovala	referovat	k5eAaBmAgFnS	referovat
některá	některý	k3yIgNnPc4	některý
média	médium	k1gNnPc4	médium
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2010	[number]	k4	2010
pak	pak	k6eAd1	pak
také	také	k9	také
za	za	k7c4	za
svá	svůj	k3xOyFgNnPc4	svůj
prohlášení	prohlášení	k1gNnPc4	prohlášení
získal	získat	k5eAaPmAgMnS	získat
anticenu	anticen	k2eAgFnSc4d1	anticena
Bludný	bludný	k2eAgInSc1d1	bludný
balvan	balvan	k1gInSc1	balvan
(	(	kIx(	(
<g/>
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Konkrétně	konkrétně	k6eAd1	konkrétně
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
zlatý	zlatý	k2eAgInSc4d1	zlatý
Bludný	bludný	k2eAgInSc4d1	bludný
balvan	balvan	k1gInSc4	balvan
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
jednotlivců	jednotlivec	k1gMnPc2	jednotlivec
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Seznam	seznam	k1gInSc1	seznam
nositelů	nositel	k1gMnPc2	nositel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hájek	Hájek	k1gMnSc1	Hájek
se	se	k3xPyFc4	se
v	v	k7c6	v
několika	několik	k4yIc6	několik
pozdějších	pozdní	k2eAgInPc6d2	pozdější
rozhovorech	rozhovor	k1gInPc6	rozhovor
postavil	postavit	k5eAaPmAgMnS	postavit
i	i	k9	i
proti	proti	k7c3	proti
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
na	na	k7c6	na
školách	škola	k1gFnPc6	škola
vyučuje	vyučovat	k5eAaImIp3nS	vyučovat
pouze	pouze	k6eAd1	pouze
"	"	kIx"	"
<g/>
jediná	jediný	k2eAgFnSc1d1	jediná
<g/>
,	,	kIx,	,
neprověřená	prověřený	k2eNgFnSc1d1	neprověřená
teorie	teorie	k1gFnSc1	teorie
<g/>
"	"	kIx"	"
o	o	k7c6	o
vzniku	vznik	k1gInSc6	vznik
života	život	k1gInSc2	život
a	a	k8xC	a
původu	původ	k1gInSc2	původ
člověka	člověk	k1gMnSc2	člověk
a	a	k8xC	a
že	že	k8xS	že
mladí	mladý	k2eAgMnPc1d1	mladý
lidé	člověk	k1gMnPc1	člověk
nejsou	být	k5eNaImIp3nP	být
informováni	informovat	k5eAaBmNgMnP	informovat
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
vysvětlení	vysvětlení	k1gNnPc1	vysvětlení
vzniku	vznik	k1gInSc2	vznik
života	život	k1gInSc2	život
a	a	k8xC	a
původu	původ	k1gInSc2	původ
člověka	člověk	k1gMnSc2	člověk
je	být	k5eAaImIp3nS	být
daleko	daleko	k6eAd1	daleko
více	hodně	k6eAd2	hodně
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
vzbudil	vzbudit	k5eAaPmAgInS	vzbudit
pozornost	pozornost	k1gFnSc4	pozornost
médií	médium	k1gNnPc2	médium
<g/>
,	,	kIx,	,
když	když	k8xS	když
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
Bin	Bin	k1gMnSc1	Bin
Ládin	Ládin	k2eAgMnSc1d1	Ládin
je	být	k5eAaImIp3nS	být
mediální	mediální	k2eAgFnPc4d1	mediální
fikce	fikce	k1gFnPc4	fikce
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
zrodil	zrodit	k5eAaPmAgInS	zrodit
<g/>
,	,	kIx,	,
tak	tak	k9	tak
také	také	k9	také
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
,	,	kIx,	,
za	za	k7c2	za
podivných	podivný	k2eAgInPc2d1	podivný
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
mystických	mystický	k2eAgFnPc2d1	mystická
okolností	okolnost	k1gFnPc2	okolnost
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
moderní	moderní	k2eAgFnSc1d1	moderní
pohádka	pohádka	k1gFnSc1	pohádka
pro	pro	k7c4	pro
dospělé	dospělý	k2eAgInPc4d1	dospělý
–	–	k?	–
dobro	dobro	k1gNnSc1	dobro
a	a	k8xC	a
zlo	zlo	k1gNnSc1	zlo
<g/>
.	.	kIx.	.
</s>
<s>
Chceme	chtít	k5eAaImIp1nP	chtít
<g/>
-li	i	k?	-li
<g/>
,	,	kIx,	,
věřme	věřit	k5eAaImRp1nP	věřit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Také	také	k9	také
zastává	zastávat	k5eAaImIp3nS	zastávat
názor	názor	k1gInSc1	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
teroristické	teroristický	k2eAgInPc4d1	teroristický
útoky	útok	k1gInPc4	útok
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2001	[number]	k4	2001
mohly	moct	k5eAaImAgInP	moct
zorganizovat	zorganizovat	k5eAaPmF	zorganizovat
americké	americký	k2eAgFnSc2d1	americká
tajné	tajný	k2eAgFnSc2d1	tajná
služby	služba	k1gFnSc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
těchto	tento	k3xDgInPc2	tento
názorů	názor	k1gInPc2	názor
se	se	k3xPyFc4	se
ale	ale	k9	ale
prezident	prezident	k1gMnSc1	prezident
republiky	republika	k1gFnSc2	republika
distancoval	distancovat	k5eAaBmAgMnS	distancovat
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
o	o	k7c6	o
Hájkovi	Hájek	k1gMnSc3	Hájek
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
zbláznil	zbláznit	k5eAaPmAgMnS	zbláznit
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Prague	Pragu	k1gFnSc2	Pragu
Pride	Prid	k1gInSc5	Prid
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
se	se	k3xPyFc4	se
Hájek	Hájek	k1gMnSc1	Hájek
pustil	pustit	k5eAaPmAgMnS	pustit
do	do	k7c2	do
ostré	ostrý	k2eAgFnSc2d1	ostrá
kritiky	kritika	k1gFnSc2	kritika
vedení	vedení	k1gNnSc1	vedení
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
převzalo	převzít	k5eAaPmAgNnS	převzít
záštitu	záštita	k1gFnSc4	záštita
nad	nad	k7c7	nad
Festivalem	festival	k1gInSc7	festival
tolerance	tolerance	k1gFnSc2	tolerance
Prague	Pragu	k1gFnSc2	Pragu
Pride	Prid	k1gInSc5	Prid
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Hájek	Hájek	k1gMnSc1	Hájek
akci	akce	k1gFnSc4	akce
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
"	"	kIx"	"
<g/>
nátlakovou	nátlakový	k2eAgFnSc4d1	nátlaková
akci	akce	k1gFnSc4	akce
homosexuálů	homosexuál	k1gMnPc2	homosexuál
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
homosexuály	homosexuál	k1gMnPc4	homosexuál
pak	pak	k6eAd1	pak
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
"	"	kIx"	"
<g/>
deviantní	deviantní	k2eAgMnPc4d1	deviantní
občany	občan	k1gMnPc4	občan
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Pozastavil	pozastavit	k5eAaPmAgMnS	pozastavit
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
nad	nad	k7c7	nad
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
mohou	moct	k5eAaImIp3nP	moct
záštitu	záštita	k1gFnSc4	záštita
nad	nad	k7c7	nad
takovou	takový	k3xDgFnSc7	takový
akcí	akce	k1gFnSc7	akce
převzít	převzít	k5eAaPmF	převzít
pravicoví	pravicový	k2eAgMnPc1d1	pravicový
politici	politik	k1gMnPc1	politik
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
primátorem	primátor	k1gMnSc7	primátor
Bohuslavem	Bohuslav	k1gMnSc7	Bohuslav
Svobodou	Svoboda	k1gMnSc7	Svoboda
a	a	k8xC	a
předsedou	předseda	k1gMnSc7	předseda
pražské	pražský	k2eAgFnSc2d1	Pražská
ODS	ODS	kA	ODS
Borisem	Boris	k1gMnSc7	Boris
Šťastným	Šťastný	k1gMnSc7	Šťastný
a	a	k8xC	a
oba	dva	k4xCgMnPc1	dva
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
k	k	k7c3	k
odchodu	odchod	k1gInSc3	odchod
z	z	k7c2	z
ODS	ODS	kA	ODS
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
Hájkovy	Hájkův	k2eAgInPc4d1	Hájkův
výroky	výrok	k1gInPc4	výrok
se	se	k3xPyFc4	se
postavil	postavit	k5eAaPmAgMnS	postavit
i	i	k9	i
prezident	prezident	k1gMnSc1	prezident
republiky	republika	k1gFnSc2	republika
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
vzápětí	vzápětí	k6eAd1	vzápětí
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vedení	vedení	k1gNnSc1	vedení
Prahy	Praha	k1gFnSc2	Praha
by	by	kYmCp3nS	by
této	tento	k3xDgFnSc3	tento
akci	akce	k1gFnSc3	akce
záštitu	záštita	k1gFnSc4	záštita
dávat	dávat	k5eAaImF	dávat
nemělo	mít	k5eNaImAgNnS	mít
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
není	být	k5eNaImIp3nS	být
projevem	projev	k1gInSc7	projev
homosexuality	homosexualita	k1gFnSc2	homosexualita
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
"	"	kIx"	"
<g/>
homosexualismu	homosexualismus	k1gInSc6	homosexualismus
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kterého	který	k3yRgNnSc2	který
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
obává	obávat	k5eAaImIp3nS	obávat
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
"	"	kIx"	"
<g/>
deviace	deviace	k1gFnSc1	deviace
<g/>
"	"	kIx"	"
pak	pak	k6eAd1	pak
Klaus	Klaus	k1gMnSc1	Klaus
pokládá	pokládat	k5eAaImIp3nS	pokládat
za	za	k7c4	za
hodnotově	hodnotově	k6eAd1	hodnotově
neutrální	neutrální	k2eAgNnSc4d1	neutrální
slovo	slovo	k1gNnSc4	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
Hájkem	hájek	k1gInSc7	hájek
kritizovaní	kritizovaný	k2eAgMnPc1d1	kritizovaný
politici	politik	k1gMnPc1	politik
si	se	k3xPyFc3	se
ale	ale	k9	ale
za	za	k7c7	za
podporou	podpora	k1gFnSc7	podpora
akce	akce	k1gFnSc2	akce
stáli	stát	k5eAaImAgMnP	stát
<g/>
.	.	kIx.	.
</s>
<s>
Primátoru	primátor	k1gMnSc3	primátor
Svobodovi	Svoboda	k1gMnSc3	Svoboda
se	se	k3xPyFc4	se
příčilo	příčit	k5eAaImAgNnS	příčit
dělení	dělení	k1gNnSc1	dělení
lidí	člověk	k1gMnPc2	člověk
dle	dle	k7c2	dle
jejich	jejich	k3xOp3gFnSc2	jejich
sexuální	sexuální	k2eAgFnSc2d1	sexuální
orientace	orientace	k1gFnSc2	orientace
a	a	k8xC	a
pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
opuštění	opuštění	k1gNnSc4	opuštění
ODS	ODS	kA	ODS
<g/>
,	,	kIx,	,
přišlo	přijít	k5eAaPmAgNnS	přijít
mu	on	k3xPp3gMnSc3	on
úsměvné	úsměvný	k2eAgNnSc1d1	úsměvné
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jej	on	k3xPp3gMnSc4	on
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
podle	podle	k7c2	podle
jeho	jeho	k3xOp3gNnPc2	jeho
informací	informace	k1gFnPc2	informace
členem	člen	k1gInSc7	člen
ODS	ODS	kA	ODS
není	být	k5eNaImIp3nS	být
a	a	k8xC	a
ani	ani	k8xC	ani
tuto	tento	k3xDgFnSc4	tento
stranu	strana	k1gFnSc4	strana
nevolil	volit	k5eNaImAgMnS	volit
<g/>
.	.	kIx.	.
</s>
<s>
Boris	Boris	k1gMnSc1	Boris
Šťastný	Šťastný	k1gMnSc1	Šťastný
zase	zase	k9	zase
nechtěl	chtít	k5eNaImAgMnS	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Praha	Praha	k1gFnSc1	Praha
byla	být	k5eAaImAgFnS	být
městem	město	k1gNnSc7	město
maloměšťáků	maloměšťák	k1gMnPc2	maloměšťák
<g/>
,	,	kIx,	,
šovinistů	šovinista	k1gMnPc2	šovinista
a	a	k8xC	a
xenofobů	xenofob	k1gMnPc2	xenofob
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
otevřenou	otevřený	k2eAgFnSc7d1	otevřená
metropolí	metropol	k1gFnSc7	metropol
jako	jako	k8xS	jako
Londýn	Londýn	k1gInSc1	Londýn
či	či	k8xC	či
Berlín	Berlín	k1gInSc1	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
Vyjádřilo	vyjádřit	k5eAaPmAgNnS	vyjádřit
se	se	k3xPyFc4	se
také	také	k9	také
americké	americký	k2eAgNnSc1d1	americké
velvyslanectví	velvyslanectví	k1gNnSc1	velvyslanectví
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterého	který	k3yRgNnSc2	který
bylo	být	k5eAaImAgNnS	být
"	"	kIx"	"
<g/>
politováníhodné	politováníhodný	k2eAgNnSc4d1	politováníhodné
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
tu	tu	k6eAd1	tu
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
i	i	k9	i
na	na	k7c6	na
oficiálních	oficiální	k2eAgNnPc6d1	oficiální
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
ztotožňují	ztotožňovat	k5eAaImIp3nP	ztotožňovat
s	s	k7c7	s
netolerantním	tolerantní	k2eNgInSc7d1	netolerantní
pohledem	pohled	k1gInSc7	pohled
na	na	k7c4	na
věc	věc	k1gFnSc4	věc
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Hájek	Hájek	k1gMnSc1	Hájek
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
pro	pro	k7c4	pro
server	server	k1gInSc4	server
Parlamentní	parlamentní	k2eAgInPc4d1	parlamentní
listy	list	k1gInPc4	list
zpochybnil	zpochybnit	k5eAaPmAgMnS	zpochybnit
kněžské	kněžský	k2eAgNnSc4d1	kněžské
vysvěcení	vysvěcení	k1gNnSc4	vysvěcení
katolického	katolický	k2eAgMnSc2d1	katolický
kněze	kněz	k1gMnSc2	kněz
Tomáše	Tomáš	k1gMnSc2	Tomáš
Halíka	Halík	k1gMnSc2	Halík
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
dlouhodobé	dlouhodobý	k2eAgFnPc4d1	dlouhodobá
kritiky	kritika	k1gFnPc4	kritika
Václava	Václav	k1gMnSc2	Václav
Klause	Klaus	k1gMnSc2	Klaus
<g/>
.	.	kIx.	.
</s>
<s>
Hájek	Hájek	k1gMnSc1	Hájek
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
zpochybnil	zpochybnit	k5eAaPmAgInS	zpochybnit
také	také	k9	také
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
patří	patřit	k5eAaImIp3nS	patřit
profesorský	profesorský	k2eAgInSc1d1	profesorský
titul	titul	k1gInSc1	titul
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Hájkových	Hájkových	k2eAgFnPc2d1	Hájkových
informací	informace	k1gFnPc2	informace
Halíka	Halík	k1gMnSc2	Halík
nevysvětil	vysvětit	k5eNaPmAgMnS	vysvětit
kardinál	kardinál	k1gMnSc1	kardinál
Meisner	Meisner	k1gMnSc1	Meisner
<g/>
,	,	kIx,	,
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
z	z	k7c2	z
Kolína	Kolín	k1gInSc2	Kolín
nad	nad	k7c7	nad
Rýnem	Rýn	k1gInSc7	Rýn
<g/>
,	,	kIx,	,
což	což	k3yQnSc4	což
Halík	Halík	k1gMnSc1	Halík
údajně	údajně	k6eAd1	údajně
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
tedy	tedy	k9	tedy
vznikat	vznikat	k5eAaImF	vznikat
otázka	otázka	k1gFnSc1	otázka
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
jde	jít	k5eAaImIp3nS	jít
skutečně	skutečně	k6eAd1	skutečně
o	o	k7c4	o
kněze	kněz	k1gMnPc4	kněz
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jen	jen	k9	jen
o	o	k7c4	o
"	"	kIx"	"
<g/>
běžného	běžný	k2eAgMnSc4d1	běžný
havlistického	havlistický	k2eAgMnSc4d1	havlistický
aktivistu	aktivista	k1gMnSc4	aktivista
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Halík	Halík	k1gMnSc1	Halík
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
vysvěcen	vysvěcen	k2eAgMnSc1d1	vysvěcen
byl	být	k5eAaImAgMnS	být
21	[number]	k4	21
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1978	[number]	k4	1978
biskupem	biskup	k1gMnSc7	biskup
Hugem	Hugo	k1gMnSc7	Hugo
Aufderbeckem	Aufderbeck	k1gInSc7	Aufderbeck
v	v	k7c6	v
kapli	kaple	k1gFnSc6	kaple
v	v	k7c6	v
Erfurtu	Erfurt	k1gInSc6	Erfurt
a	a	k8xC	a
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
pochybovat	pochybovat	k5eAaImF	pochybovat
o	o	k7c6	o
Hájkově	Hájkův	k2eAgNnSc6d1	Hájkovo
duševním	duševní	k2eAgNnSc6d1	duševní
zdraví	zdraví	k1gNnSc6	zdraví
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
svého	svůj	k3xOyFgMnSc2	svůj
světitele	světitel	k1gMnSc2	světitel
přitom	přitom	k6eAd1	přitom
Halík	Halík	k1gMnSc1	Halík
uvedl	uvést	k5eAaPmAgMnS	uvést
biskupa	biskup	k1gMnSc4	biskup
Aufderbecka	Aufderbecko	k1gNnSc2	Aufderbecko
již	již	k6eAd1	již
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
rozhovorů	rozhovor	k1gInPc2	rozhovor
s	s	k7c7	s
Janem	Jan	k1gMnSc7	Jan
Jandourkem	Jandourek	k1gMnSc7	Jandourek
Ptal	ptat	k5eAaImAgMnS	ptat
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
cest	cesta	k1gFnPc2	cesta
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
biskupská	biskupský	k2eAgFnSc1d1	biskupská
konference	konference	k1gFnSc1	konference
na	na	k7c6	na
základě	základ	k1gInSc6	základ
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
svého	svůj	k3xOyFgMnSc2	svůj
předsedy	předseda	k1gMnSc2	předseda
Dominika	Dominik	k1gMnSc4	Dominik
Duky	Duka	k1gMnSc2	Duka
Halíkovo	Halíkův	k2eAgNnSc4d1	Halíkovo
svěcení	svěcení	k1gNnSc4	svěcení
doložila	doložit	k5eAaPmAgFnS	doložit
zveřejněním	zveřejnění	k1gNnSc7	zveřejnění
skenu	sken	k1gInSc2	sken
nedatovaného	datovaný	k2eNgNnSc2d1	nedatované
potvrzení	potvrzení	k1gNnSc2	potvrzení
arcibiskupa	arcibiskup	k1gMnSc2	arcibiskup
kard	kard	k6eAd1	kard
<g/>
.	.	kIx.	.
</s>
<s>
Meisnera	Meisner	k1gMnSc4	Meisner
na	na	k7c6	na
svých	svůj	k3xOyFgFnPc6	svůj
webových	webový	k2eAgFnPc6d1	webová
stránkách	stránka	k1gFnPc6	stránka
a	a	k8xC	a
Hájkovo	Hájkův	k2eAgNnSc4d1	Hájkovo
tvrzení	tvrzení	k1gNnSc4	tvrzení
tím	ten	k3xDgNnSc7	ten
vyvrátila	vyvrátit	k5eAaPmAgFnS	vyvrátit
<g/>
.	.	kIx.	.
</s>
<s>
Halíkovo	Halíkův	k2eAgNnSc1d1	Halíkovo
vysvěcení	vysvěcení	k1gNnSc1	vysvěcení
v	v	k7c6	v
Erfurtu	Erfurt	k1gInSc6	Erfurt
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
už	už	k6eAd1	už
dříve	dříve	k6eAd2	dříve
v	v	k7c6	v
televizním	televizní	k2eAgInSc6d1	televizní
pořadu	pořad	k1gInSc6	pořad
13	[number]	k4	13
<g/>
.	.	kIx.	.
komnata	komnata	k1gFnSc1	komnata
Tomáše	Tomáš	k1gMnSc2	Tomáš
Halíka	Halík	k1gMnSc2	Halík
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
erfurtský	erfurtský	k2eAgMnSc1d1	erfurtský
generální	generální	k2eAgMnSc1d1	generální
vikář	vikář	k1gMnSc1	vikář
(	(	kIx(	(
<g/>
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
světící	světící	k2eAgMnSc1d1	světící
biskup	biskup	k1gMnSc1	biskup
<g/>
)	)	kIx)	)
Mons	Mons	k1gInSc1	Mons
<g/>
.	.	kIx.	.
</s>
<s>
Hans-Reinhard	Hans-Reinhard	k1gMnSc1	Hans-Reinhard
Koch	Koch	k1gMnSc1	Koch
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
přípravu	příprava	k1gFnSc4	příprava
tajného	tajný	k2eAgNnSc2d1	tajné
svěcení	svěcení	k1gNnSc2	svěcení
na	na	k7c4	na
starosti	starost	k1gFnPc4	starost
<g/>
.	.	kIx.	.
</s>
<s>
Kriticky	kriticky	k6eAd1	kriticky
se	se	k3xPyFc4	se
k	k	k7c3	k
Hájkovu	Hájkův	k2eAgNnSc3d1	Hájkovo
tvrzení	tvrzení	k1gNnSc3	tvrzení
vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
i	i	k8xC	i
emeritní	emeritní	k2eAgMnSc1d1	emeritní
pražský	pražský	k2eAgMnSc1d1	pražský
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
kardinál	kardinál	k1gMnSc1	kardinál
Miloslav	Miloslav	k1gMnSc1	Miloslav
Vlk	Vlk	k1gMnSc1	Vlk
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
delším	dlouhý	k2eAgInSc6d2	delší
textu	text	k1gInSc6	text
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
podiv	podiv	k1gInSc4	podiv
nad	nad	k7c7	nad
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Hájek	Hájek	k1gMnSc1	Hájek
"	"	kIx"	"
<g/>
stále	stále	k6eAd1	stále
ještě	ještě	k9	ještě
zastává	zastávat	k5eAaImIp3nS	zastávat
tak	tak	k6eAd1	tak
vysokou	vysoký	k2eAgFnSc4d1	vysoká
funkci	funkce	k1gFnSc4	funkce
ve	v	k7c6	v
veřejném	veřejný	k2eAgInSc6d1	veřejný
životě	život	k1gInSc6	život
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Zesnulý	zesnulý	k2eAgMnSc1d1	zesnulý
prezident	prezident	k1gMnSc1	prezident
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
Satanových	Satanův	k2eAgFnPc6d1	Satanův
službách	služba	k1gFnPc6	služba
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
nástrojem	nástroj	k1gInSc7	nástroj
lži	lež	k1gFnSc2	lež
a	a	k8xC	a
nenávisti	nenávist	k1gFnSc2	nenávist
<g/>
.	.	kIx.	.
</s>
<s>
Nástrojem	nástroj	k1gInSc7	nástroj
pravdy	pravda	k1gFnSc2	pravda
a	a	k8xC	a
lásky	láska	k1gFnSc2	láska
je	být	k5eAaImIp3nS	být
naopak	naopak	k6eAd1	naopak
prezident	prezident	k1gMnSc1	prezident
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
<g/>
,	,	kIx,	,
uvádí	uvádět	k5eAaImIp3nS	uvádět
Hájek	Hájek	k1gMnSc1	Hájek
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
nové	nový	k2eAgFnSc6d1	nová
knize	kniha	k1gFnSc6	kniha
Smrt	smrt	k1gFnSc1	smrt
v	v	k7c6	v
sametu	samet	k1gInSc6	samet
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Havel	Havel	k1gMnSc1	Havel
byl	být	k5eAaImAgInS	být
podle	podle	k7c2	podle
Hájka	Hájek	k1gMnSc4	Hájek
teoretický	teoretický	k2eAgMnSc1d1	teoretický
moralista	moralista	k1gMnSc1	moralista
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
si	se	k3xPyFc3	se
nepotrpěl	potrpět	k5eNaPmAgMnS	potrpět
na	na	k7c4	na
morální	morální	k2eAgFnPc4d1	morální
skrupule	skrupule	k1gFnPc4	skrupule
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jeho	jeho	k3xOp3gNnPc2	jeho
slov	slovo	k1gNnPc2	slovo
byl	být	k5eAaImAgMnS	být
exprezident	exprezident	k1gMnSc1	exprezident
současně	současně	k6eAd1	současně
i	i	k9	i
apoštol	apoštol	k1gMnSc1	apoštol
svobody	svoboda	k1gFnSc2	svoboda
a	a	k8xC	a
demokracie	demokracie	k1gFnSc2	demokracie
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
koncepce	koncepce	k1gFnSc1	koncepce
otevřeně	otevřeně	k6eAd1	otevřeně
směřovala	směřovat	k5eAaImAgFnS	směřovat
k	k	k7c3	k
fašistickému	fašistický	k2eAgInSc3d1	fašistický
korporativistickému	korporativistický	k2eAgInSc3d1	korporativistický
státu	stát	k1gInSc3	stát
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
Hájek	Hájek	k1gMnSc1	Hájek
označuje	označovat	k5eAaImIp3nS	označovat
občanskou	občanský	k2eAgFnSc4d1	občanská
společnost	společnost	k1gFnSc4	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
uvedl	uvést	k5eAaPmAgMnS	uvést
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
pro	pro	k7c4	pro
Parlamentní	parlamentní	k2eAgInPc4d1	parlamentní
listy	list	k1gInPc4	list
<g/>
,	,	kIx,	,
že	že	k8xS	že
Hájkovu	Hájkův	k2eAgFnSc4d1	Hájkova
kritiku	kritika	k1gFnSc4	kritika
historické	historický	k2eAgFnSc2d1	historická
role	role	k1gFnSc2	role
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
sdílí	sdílet	k5eAaImIp3nS	sdílet
<g/>
,	,	kIx,	,
ne	ne	k9	ne
však	však	k9	však
všechny	všechen	k3xTgInPc4	všechen
jeho	jeho	k3xOp3gInPc4	jeho
použité	použitý	k2eAgInPc4d1	použitý
expresivní	expresivní	k2eAgInPc4d1	expresivní
výrazy	výraz	k1gInPc4	výraz
<g/>
.	.	kIx.	.
</s>
<s>
Knihu	kniha	k1gFnSc4	kniha
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
pozoruhodnou	pozoruhodný	k2eAgFnSc4d1	pozoruhodná
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
s	s	k7c7	s
autorem	autor	k1gMnSc7	autor
podle	podle	k7c2	podle
vlastních	vlastní	k2eAgNnPc2d1	vlastní
slov	slovo	k1gNnPc2	slovo
v	v	k7c6	v
mnohém	mnohé	k1gNnSc6	mnohé
nesouhlasí	souhlasit	k5eNaImIp3nP	souhlasit
a	a	k8xC	a
liší	lišit	k5eAaImIp3nP	lišit
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
základním	základní	k2eAgInSc7d1	základní
světonázorem	světonázor	k1gInSc7	světonázor
<g/>
;	;	kIx,	;
za	za	k7c4	za
neakceptovatelnou	akceptovatelný	k2eNgFnSc4d1	neakceptovatelná
označil	označit	k5eAaPmAgMnS	označit
Hájkovu	Hájkův	k2eAgFnSc4d1	Hájkova
víru	víra	k1gFnSc4	víra
v	v	k7c4	v
katolickou	katolický	k2eAgFnSc4d1	katolická
doktrínu	doktrína	k1gFnSc4	doktrína
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
socioložky	socioložka	k1gFnSc2	socioložka
Jiřiny	Jiřina	k1gFnSc2	Jiřina
Šiklové	Šiklová	k1gFnSc2	Šiklová
by	by	kYmCp3nS	by
si	se	k3xPyFc3	se
měl	mít	k5eAaImAgMnS	mít
vicekancléř	vicekancléř	k1gMnSc1	vicekancléř
prezidenta	prezident	k1gMnSc4	prezident
uvědomit	uvědomit	k5eAaPmF	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
placen	placen	k2eAgMnSc1d1	placen
z	z	k7c2	z
peněz	peníze	k1gInPc2	peníze
daňových	daňový	k2eAgMnPc2d1	daňový
poplatníků	poplatník	k1gMnPc2	poplatník
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
údajů	údaj	k1gInPc2	údaj
z	z	k7c2	z
Pražského	pražský	k2eAgInSc2d1	pražský
hradu	hrad	k1gInSc2	hrad
činil	činit	k5eAaImAgInS	činit
v	v	k7c6	v
prvním	první	k4xOgNnSc6	první
pololetí	pololetí	k1gNnSc6	pololetí
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
Hájkův	Hájkův	k2eAgInSc1d1	Hájkův
průměrný	průměrný	k2eAgInSc1d1	průměrný
měsíční	měsíční	k2eAgInSc1d1	měsíční
příjem	příjem	k1gInSc1	příjem
111	[number]	k4	111
666	[number]	k4	666
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Když	když	k8xS	když
ho	on	k3xPp3gMnSc4	on
bude	být	k5eAaImBp3nS	být
platit	platit	k5eAaImF	platit
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
ze	z	k7c2	z
svého	svůj	k3xOyFgNnSc2	svůj
kapesného	kapesné	k1gNnSc2	kapesné
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
si	se	k3xPyFc3	se
pan	pan	k1gMnSc1	pan
Hájek	Hájek	k1gMnSc1	Hájek
vydává	vydávat	k5eAaPmIp3nS	vydávat
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
chce	chtít	k5eAaImIp3nS	chtít
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
uvedla	uvést	k5eAaPmAgFnS	uvést
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
nové	nový	k2eAgFnSc3d1	nová
knize	kniha	k1gFnSc3	kniha
Šiklová	Šiklová	k1gFnSc1	Šiklová
<g/>
.	.	kIx.	.
</s>
<s>
Volby	volba	k1gFnPc1	volba
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2012	[number]	k4	2012
podle	podle	k7c2	podle
Hájka	Hájek	k1gMnSc2	Hájek
ukázaly	ukázat	k5eAaPmAgInP	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pravice	pravice	k1gFnSc1	pravice
umírá	umírat	k5eAaImIp3nS	umírat
<g/>
,	,	kIx,	,
možná	možná	k9	možná
již	již	k6eAd1	již
umřela	umřít	k5eAaPmAgFnS	umřít
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
něj	on	k3xPp3gInSc2	on
už	už	k6eAd1	už
ohrožen	ohrožen	k2eAgInSc1d1	ohrožen
celý	celý	k2eAgInSc1d1	celý
demokratický	demokratický	k2eAgInSc1d1	demokratický
systém	systém	k1gInSc1	systém
nastavený	nastavený	k2eAgInSc1d1	nastavený
po	po	k7c6	po
listopadu	listopad	k1gInSc6	listopad
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Jsem	být	k5eAaImIp1nS	být
přesvědčen	přesvědčit	k5eAaPmNgMnS	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
dopadne	dopadnout	k5eAaPmIp3nS	dopadnout
tragicky	tragicky	k6eAd1	tragicky
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
uvedl	uvést	k5eAaPmAgMnS	uvést
Hájek	Hájek	k1gMnSc1	Hájek
<g/>
.	.	kIx.	.
</s>
<s>
Čas	čas	k1gInSc1	čas
vyměřený	vyměřený	k2eAgInSc1d1	vyměřený
světu	svět	k1gInSc3	svět
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
nepochybně	pochybně	k6eNd1	pochybně
krátí	krátit	k5eAaImIp3nS	krátit
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Vím	vědět	k5eAaImIp1nS	vědět
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
to	ten	k3xDgNnSc1	ten
celé	celý	k2eAgNnSc1d1	celé
proběhne	proběhnout	k5eAaPmIp3nS	proběhnout
a	a	k8xC	a
proč	proč	k6eAd1	proč
–	–	k?	–
ale	ale	k8xC	ale
především	především	k6eAd1	především
znám	znát	k5eAaImIp1nS	znát
symptomy	symptom	k1gInPc1	symptom
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
na	na	k7c4	na
blízkost	blízkost	k1gFnSc4	blízkost
konce	konec	k1gInSc2	konec
budou	být	k5eAaImBp3nP	být
neomylně	omylně	k6eNd1	omylně
ukazovat	ukazovat	k5eAaImF	ukazovat
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
řekl	říct	k5eAaPmAgMnS	říct
Hájek	Hájek	k1gMnSc1	Hájek
<g/>
.	.	kIx.	.
</s>
<s>
Apokalypsa	apokalypsa	k1gFnSc1	apokalypsa
podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
nade	nad	k7c4	nad
vši	veš	k1gFnPc4	veš
pochybnost	pochybnost	k1gFnSc1	pochybnost
již	již	k6eAd1	již
začala	začít	k5eAaPmAgFnS	začít
a	a	k8xC	a
Antikrist	Antikrist	k1gMnSc1	Antikrist
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Vypadá	vypadat	k5eAaImIp3nS	vypadat
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
zatím	zatím	k6eAd1	zatím
poslední	poslední	k2eAgInSc4d1	poslední
neúspěšný	úspěšný	k2eNgInSc4d1	neúspěšný
pokus	pokus	k1gInSc4	pokus
o	o	k7c4	o
nástup	nástup	k1gInSc4	nástup
Antikrista	Antikrist	k1gMnSc2	Antikrist
pramenil	pramenit	k5eAaImAgMnS	pramenit
dost	dost	k6eAd1	dost
možná	možná	k9	možná
právě	právě	k9	právě
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
líhni	líheň	k1gFnSc6	líheň
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
soudí	soudit	k5eAaImIp3nS	soudit
Hájek	Hájek	k1gMnSc1	Hájek
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
On	on	k3xPp3gMnSc1	on
to	ten	k3xDgNnSc4	ten
myslí	myslet	k5eAaImIp3nS	myslet
skutečně	skutečně	k6eAd1	skutečně
vážně	vážně	k6eAd1	vážně
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
mi	já	k3xPp1nSc3	já
nezbývá	zbývat	k5eNaImIp3nS	zbývat
nic	nic	k3yNnSc1	nic
jiného	jiný	k2eAgNnSc2d1	jiné
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
za	za	k7c4	za
něj	on	k3xPp3gMnSc4	on
pomodlit	pomodlit	k5eAaPmF	pomodlit
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
komentoval	komentovat	k5eAaBmAgInS	komentovat
Hájkova	Hájkův	k2eAgNnSc2d1	Hájkovo
slova	slovo	k1gNnSc2	slovo
ministr	ministr	k1gMnSc1	ministr
financí	finance	k1gFnPc2	finance
Miroslav	Miroslav	k1gMnSc1	Miroslav
Kalousek	Kalousek	k1gMnSc1	Kalousek
<g/>
.	.	kIx.	.
</s>
<s>
Balada	balada	k1gFnSc1	balada
číslo	číslo	k1gNnSc1	číslo
jedna	jeden	k4xCgNnPc1	jeden
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
–	–	k?	–
novela	novela	k1gFnSc1	novela
Halelujá	Halelujá	k1gFnSc1	Halelujá
(	(	kIx(	(
<g/>
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
–	–	k?	–
sbírka	sbírka	k1gFnSc1	sbírka
povídek	povídka	k1gFnPc2	povídka
Areál	areál	k1gInSc4	areál
snů	sen	k1gInPc2	sen
(	(	kIx(	(
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
–	–	k?	–
román	román	k1gInSc1	román
Vlídná	vlídný	k2eAgFnSc1d1	vlídná
past	past	k1gFnSc1	past
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
–	–	k?	–
triptych	triptych	k1gInSc1	triptych
(	(	kIx(	(
<g/>
román	román	k1gInSc1	román
v	v	k7c6	v
povídkách	povídka	k1gFnPc6	povídka
<g/>
)	)	kIx)	)
Předběžný	předběžný	k2eAgInSc4d1	předběžný
portrét	portrét	k1gInSc4	portrét
<g/>
,	,	kIx,	,
Československý	československý	k2eAgMnSc1d1	československý
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
–	–	k?	–
koláž	koláž	k1gFnSc4	koláž
z	z	k7c2	z
interview	interview	k1gNnSc2	interview
a	a	k8xC	a
úryvků	úryvek	k1gInPc2	úryvek
divadelních	divadelní	k2eAgInPc2d1	divadelní
textů	text	k1gInPc2	text
Bolka	Bolek	k1gMnSc2	Bolek
Polívky	Polívka	k1gMnSc2	Polívka
Svět	svět	k1gInSc1	svět
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
rozvod	rozvod	k1gInSc4	rozvod
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
–	–	k?	–
triptych	triptych	k1gInSc1	triptych
(	(	kIx(	(
<g/>
román	román	k1gInSc1	román
v	v	k7c6	v
povídkách	povídka	k1gFnPc6	povídka
<g/>
)	)	kIx)	)
Na	na	k7c4	na
rovinu	rovina	k1gFnSc4	rovina
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
–	–	k?	–
knižní	knižní	k2eAgInSc4d1	knižní
interview	interview	k1gInSc4	interview
s	s	k7c7	s
Václavem	Václav	k1gMnSc7	Václav
Klausem	Klaus	k1gMnSc7	Klaus
Smrt	smrt	k1gFnSc1	smrt
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
Smrt	smrt	k1gFnSc1	smrt
v	v	k7c6	v
sametu	samet	k1gInSc6	samet
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
názory	názor	k1gInPc4	názor
hradního	hradní	k2eAgMnSc4d1	hradní
kancléře	kancléř	k1gMnSc4	kancléř
Petra	Petr	k1gMnSc4	Petr
Hájka	Hájek	k1gMnSc4	Hájek
v	v	k7c6	v
krátkometrážním	krátkometrážní	k2eAgInSc6d1	krátkometrážní
dokumentárním	dokumentární	k2eAgInSc6d1	dokumentární
filmu	film	k1gInSc6	film
Hájek	Hájek	k1gMnSc1	Hájek
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
v	v	k7c6	v
podzámčí	podzámčí	k1gNnSc6	podzámčí
režisérka	režisérka	k1gFnSc1	režisérka
Apolena	Apolena	k1gFnSc1	Apolena
Rychlíková	Rychlíková	k1gFnSc1	Rychlíková
<g/>
.	.	kIx.	.
</s>
