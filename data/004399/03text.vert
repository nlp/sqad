<s>
Ústava	ústava	k1gFnSc1	ústava
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
je	být	k5eAaImIp3nS	být
základní	základní	k2eAgInSc1d1	základní
zákon	zákon	k1gInSc1	zákon
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
jako	jako	k8xC	jako
ústavní	ústavní	k2eAgInSc1d1	ústavní
zákon	zákon	k1gInSc1	zákon
Českou	český	k2eAgFnSc7d1	Česká
národní	národní	k2eAgFnSc7d1	národní
radou	rada	k1gFnSc7	rada
16	[number]	k4	16
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1992	[number]	k4	1992
a	a	k8xC	a
publikován	publikovat	k5eAaBmNgInS	publikovat
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
Sbírce	sbírka	k1gFnSc6	sbírka
zákonů	zákon	k1gInPc2	zákon
pod	pod	k7c7	pod
č.	č.	k?	č.
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
1993	[number]	k4	1993
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
ústavního	ústavní	k2eAgInSc2d1	ústavní
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
československé	československý	k2eAgFnSc6d1	Československá
federaci	federace	k1gFnSc6	federace
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
nejenže	nejenže	k6eAd1	nejenže
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1969	[number]	k4	1969
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
Česká	český	k2eAgFnSc1d1	Česká
socialistická	socialistický	k2eAgFnSc1d1	socialistická
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bylo	být	k5eAaImAgNnS	být
také	také	k9	také
počítáno	počítat	k5eAaImNgNnS	počítat
s	s	k7c7	s
přijetím	přijetí	k1gNnSc7	přijetí
republikových	republikový	k2eAgFnPc2d1	republiková
ústav	ústava	k1gFnPc2	ústava
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
však	však	k9	však
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
přijaty	přijat	k2eAgFnPc1d1	přijata
nebyly	být	k5eNaImAgFnP	být
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
byly	být	k5eAaImAgFnP	být
zahájeny	zahájit	k5eAaPmNgFnP	zahájit
přípravné	přípravný	k2eAgFnPc1d1	přípravná
práce	práce	k1gFnPc1	práce
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
nejasnosti	nejasnost	k1gFnPc1	nejasnost
kolem	kolem	k7c2	kolem
budoucnosti	budoucnost	k1gFnSc2	budoucnost
federace	federace	k1gFnSc2	federace
způsobily	způsobit	k5eAaPmAgInP	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
práce	práce	k1gFnSc1	práce
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
stagnovaly	stagnovat	k5eAaImAgInP	stagnovat
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
začala	začít	k5eAaPmAgFnS	začít
být	být	k5eAaImF	být
první	první	k4xOgFnSc1	první
česká	český	k2eAgFnSc1d1	Česká
ústava	ústava	k1gFnSc1	ústava
připravována	připravovat	k5eAaImNgFnS	připravovat
až	až	k9	až
v	v	k7c6	v
časové	časový	k2eAgFnSc6d1	časová
tísni	tíseň	k1gFnSc6	tíseň
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
souvisela	souviset	k5eAaImAgFnS	souviset
s	s	k7c7	s
blížícím	blížící	k2eAgMnSc7d1	blížící
se	se	k3xPyFc4	se
zánikem	zánik	k1gInSc7	zánik
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
ústava	ústava	k1gFnSc1	ústava
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
preambulí	preambule	k1gFnSc7	preambule
a	a	k8xC	a
8	[number]	k4	8
hlavami	hlava	k1gFnPc7	hlava
<g/>
,	,	kIx,	,
zahrnujícími	zahrnující	k2eAgInPc7d1	zahrnující
základní	základní	k2eAgFnSc4d1	základní
ustanovení	ustanovení	k1gNnSc3	ustanovení
<g/>
,	,	kIx,	,
moc	moc	k6eAd1	moc
zákonodárnou	zákonodárný	k2eAgFnSc4d1	zákonodárná
<g/>
,	,	kIx,	,
moc	moc	k6eAd1	moc
výkonnou	výkonný	k2eAgFnSc4d1	výkonná
<g/>
,	,	kIx,	,
moc	moc	k6eAd1	moc
soudní	soudní	k2eAgInSc1d1	soudní
<g/>
,	,	kIx,	,
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
kontrolní	kontrolní	k2eAgInSc4d1	kontrolní
úřad	úřad	k1gInSc4	úřad
<g/>
,	,	kIx,	,
Českou	český	k2eAgFnSc4d1	Česká
národní	národní	k2eAgFnSc4d1	národní
banku	banka	k1gFnSc4	banka
<g/>
,	,	kIx,	,
územní	územní	k2eAgFnSc4d1	územní
samosprávu	samospráva	k1gFnSc4	samospráva
a	a	k8xC	a
přechodná	přechodný	k2eAgFnSc1d1	přechodná
a	a	k8xC	a
závěrečná	závěrečný	k2eAgNnPc1d1	závěrečné
ustanovení	ustanovení	k1gNnPc1	ustanovení
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
června	červen	k1gInSc2	červen
2013	[number]	k4	2013
byla	být	k5eAaImAgFnS	být
Ústava	ústava	k1gFnSc1	ústava
osmkrát	osmkrát	k6eAd1	osmkrát
novelizována	novelizován	k2eAgFnSc1d1	novelizována
<g/>
.	.	kIx.	.
22	[number]	k4	22
článků	článek	k1gInPc2	článek
bylo	být	k5eAaImAgNnS	být
dotčeno	dotknout	k5eAaPmNgNnS	dotknout
změnami	změna	k1gFnPc7	změna
<g/>
,	,	kIx,	,
devět	devět	k4xCc4	devět
článků	článek	k1gInPc2	článek
bylo	být	k5eAaImAgNnS	být
doplněno	doplnit	k5eAaPmNgNnS	doplnit
<g/>
,	,	kIx,	,
dva	dva	k4xCgInPc1	dva
články	článek	k1gInPc1	článek
byly	být	k5eAaImAgInP	být
vloženy	vložit	k5eAaPmNgInP	vložit
a	a	k8xC	a
jeden	jeden	k4xCgInSc1	jeden
článek	článek	k1gInSc1	článek
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Ústavě	ústav	k1gInSc6	ústav
zrušen	zrušit	k5eAaPmNgInS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Ústava	ústava	k1gFnSc1	ústava
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
Českou	český	k2eAgFnSc4d1	Česká
republiku	republika	k1gFnSc4	republika
existující	existující	k2eAgFnSc4d1	existující
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
zanikající	zanikající	k2eAgFnSc2d1	zanikající
federace	federace	k1gFnSc2	federace
a	a	k8xC	a
stanoví	stanovit	k5eAaPmIp3nS	stanovit
přechodná	přechodný	k2eAgNnPc4d1	přechodné
opatření	opatření	k1gNnPc4	opatření
pro	pro	k7c4	pro
působnost	působnost	k1gFnSc4	působnost
ústavních	ústavní	k2eAgInPc2d1	ústavní
orgánů	orgán	k1gInPc2	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Výslovně	výslovně	k6eAd1	výslovně
zrušila	zrušit	k5eAaPmAgFnS	zrušit
Ústavu	ústav	k1gInSc3	ústav
České	český	k2eAgFnSc2d1	Česká
a	a	k8xC	a
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
Federativní	federativní	k2eAgFnSc2d1	federativní
Republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
ústavní	ústavní	k2eAgInSc1d1	ústavní
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
československé	československý	k2eAgFnSc6d1	Československá
federaci	federace	k1gFnSc6	federace
<g/>
,	,	kIx,	,
ústavní	ústavní	k2eAgInPc4d1	ústavní
zákony	zákon	k1gInPc4	zákon
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
je	být	k5eAaImIp3nS	být
měnily	měnit	k5eAaImAgFnP	měnit
a	a	k8xC	a
doplňovaly	doplňovat	k5eAaImAgFnP	doplňovat
a	a	k8xC	a
ústavní	ústavní	k2eAgInSc1d1	ústavní
zákon	zákon	k1gInSc1	zákon
České	český	k2eAgFnSc2d1	Česká
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
č.	č.	k?	č.
67	[number]	k4	67
<g/>
/	/	kIx~	/
<g/>
1990	[number]	k4	1990
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
státních	státní	k2eAgInPc6d1	státní
symbolech	symbol	k1gInPc6	symbol
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
vyjmenovává	vyjmenovávat	k5eAaImIp3nS	vyjmenovávat
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
dosavadní	dosavadní	k2eAgInPc1d1	dosavadní
československé	československý	k2eAgInPc1d1	československý
i	i	k8xC	i
české	český	k2eAgInPc1d1	český
ústavní	ústavní	k2eAgInPc1d1	ústavní
zákony	zákon	k1gInPc1	zákon
platí	platit	k5eAaImIp3nP	platit
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
jako	jako	k8xC	jako
ústavní	ústavní	k2eAgInPc1d1	ústavní
zákony	zákon	k1gInPc1	zákon
i	i	k9	i
nadále	nadále	k6eAd1	nadále
(	(	kIx(	(
<g/>
ty	ten	k3xDgFnPc4	ten
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
upravují	upravovat	k5eAaImIp3nP	upravovat
státní	státní	k2eAgFnPc4d1	státní
hranice	hranice	k1gFnPc4	hranice
<g/>
,	,	kIx,	,
a	a	k8xC	a
ty	ten	k3xDgMnPc4	ten
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
Česká	český	k2eAgFnSc1d1	Česká
národní	národní	k2eAgFnSc1d1	národní
rada	rada	k1gFnSc1	rada
přijala	přijmout	k5eAaPmAgFnS	přijmout
po	po	k7c6	po
6	[number]	k4	6
<g/>
.	.	kIx.	.
červnu	červen	k1gInSc3	červen
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ty	ten	k3xDgFnPc1	ten
jsou	být	k5eAaImIp3nP	být
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Listinou	listina	k1gFnSc7	listina
základních	základní	k2eAgNnPc2d1	základní
práv	právo	k1gNnPc2	právo
a	a	k8xC	a
svobod	svoboda	k1gFnPc2	svoboda
součástí	součást	k1gFnPc2	součást
tzv.	tzv.	kA	tzv.
ústavního	ústavní	k2eAgInSc2d1	ústavní
pořádku	pořádek	k1gInSc2	pořádek
<g/>
.	.	kIx.	.
</s>
<s>
Ostatním	ostatní	k2eAgInPc3d1	ostatní
ústavním	ústavní	k2eAgInPc3d1	ústavní
zákonům	zákon	k1gInPc3	zákon
dosud	dosud	k6eAd1	dosud
platným	platný	k2eAgInPc3d1	platný
na	na	k7c6	na
území	území	k1gNnSc6	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
pak	pak	k6eAd1	pak
přiznala	přiznat	k5eAaPmAgFnS	přiznat
sílu	síla	k1gFnSc4	síla
(	(	kIx(	(
<g/>
obyčejného	obyčejný	k2eAgInSc2d1	obyčejný
<g/>
)	)	kIx)	)
zákona	zákon	k1gInSc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
28	[number]	k4	28
<g/>
.	.	kIx.	.
schůzi	schůze	k1gFnSc6	schůze
Národního	národní	k2eAgNnSc2d1	národní
shromáždění	shromáždění	k1gNnSc2	shromáždění
Československé	československý	k2eAgFnSc2d1	Československá
socialistické	socialistický	k2eAgFnSc2d1	socialistická
republiky	republika	k1gFnSc2	republika
27	[number]	k4	27
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1968	[number]	k4	1968
byl	být	k5eAaImAgInS	být
předložen	předložit	k5eAaPmNgInS	předložit
společný	společný	k2eAgInSc1d1	společný
návrh	návrh	k1gInSc1	návrh
České	český	k2eAgFnSc2d1	Česká
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
(	(	kIx(	(
<g/>
ČNR	ČNR	kA	ČNR
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
(	(	kIx(	(
<g/>
SNR	SNR	kA	SNR
<g/>
)	)	kIx)	)
a	a	k8xC	a
československé	československý	k2eAgFnSc2d1	Československá
vlády	vláda	k1gFnSc2	vláda
na	na	k7c4	na
vydání	vydání	k1gNnSc4	vydání
ústavního	ústavní	k2eAgInSc2d1	ústavní
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
československé	československý	k2eAgFnSc6d1	Československá
federaci	federace	k1gFnSc6	federace
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
byl	být	k5eAaImAgInS	být
ústavní	ústavní	k2eAgInSc1d1	ústavní
zákon	zákon	k1gInSc1	zákon
(	(	kIx(	(
<g/>
č.	č.	k?	č.
143	[number]	k4	143
<g/>
/	/	kIx~	/
<g/>
1968	[number]	k4	1968
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
přijat	přijmout	k5eAaPmNgInS	přijmout
a	a	k8xC	a
s	s	k7c7	s
nabytím	nabytí	k1gNnSc7	nabytí
účinnosti	účinnost	k1gFnSc2	účinnost
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1969	[number]	k4	1969
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
Česká	český	k2eAgFnSc1d1	Česká
socialistická	socialistický	k2eAgFnSc1d1	socialistická
republika	republika	k1gFnSc1	republika
a	a	k8xC	a
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
socialistická	socialistický	k2eAgFnSc1d1	socialistická
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
obě	dva	k4xCgFnPc1	dva
republiky	republika	k1gFnPc1	republika
měly	mít	k5eAaImAgFnP	mít
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
federace	federace	k1gFnSc2	federace
rovnoprávné	rovnoprávný	k2eAgNnSc1d1	rovnoprávné
postavení	postavení	k1gNnSc1	postavení
<g/>
.	.	kIx.	.
</s>
<s>
Ústavní	ústavní	k2eAgInSc1d1	ústavní
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
československé	československý	k2eAgFnSc6d1	Československá
federaci	federace	k1gFnSc6	federace
v	v	k7c6	v
článku	článek	k1gInSc6	článek
142	[number]	k4	142
odst	odstum	k1gNnPc2	odstum
<g/>
.	.	kIx.	.
2	[number]	k4	2
stanovil	stanovit	k5eAaPmAgInS	stanovit
<g/>
,	,	kIx,	,
že	že	k8xS	že
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
přijetím	přijetí	k1gNnSc7	přijetí
ústavy	ústava	k1gFnSc2	ústava
Československé	československý	k2eAgFnSc2d1	Československá
socialistické	socialistický	k2eAgFnSc2d1	socialistická
republiky	republika	k1gFnSc2	republika
přijmou	přijmout	k5eAaPmIp3nP	přijmout
obě	dva	k4xCgFnPc1	dva
republiky	republika	k1gFnPc1	republika
vlastní	vlastní	k2eAgFnSc2d1	vlastní
ústavy	ústava	k1gFnSc2	ústava
a	a	k8xC	a
předpokládal	předpokládat	k5eAaImAgMnS	předpokládat
i	i	k9	i
vznik	vznik	k1gInSc4	vznik
tří	tři	k4xCgInPc2	tři
ústavních	ústavní	k2eAgInPc2d1	ústavní
soudů	soud	k1gInPc2	soud
<g/>
.	.	kIx.	.
</s>
<s>
Zvrat	zvrat	k1gInSc1	zvrat
však	však	k9	však
přinesl	přinést	k5eAaPmAgInS	přinést
nástup	nástup	k1gInSc1	nástup
normalizace	normalizace	k1gFnSc2	normalizace
<g/>
.	.	kIx.	.
</s>
<s>
Reálnou	reálný	k2eAgFnSc7d1	reálná
federací	federace	k1gFnSc7	federace
bylo	být	k5eAaImAgNnS	být
Československo	Československo	k1gNnSc1	Československo
jen	jen	k9	jen
do	do	k7c2	do
prosince	prosinec	k1gInSc2	prosinec
1970	[number]	k4	1970
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
ústavní	ústavní	k2eAgInSc1d1	ústavní
zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
125	[number]	k4	125
<g/>
/	/	kIx~	/
<g/>
1970	[number]	k4	1970
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
důvodová	důvodový	k2eAgFnSc1d1	Důvodová
zpráva	zpráva	k1gFnSc1	zpráva
jasně	jasně	k6eAd1	jasně
zdůrazňovala	zdůrazňovat	k5eAaImAgFnS	zdůrazňovat
"	"	kIx"	"
<g/>
posílení	posílení	k1gNnSc1	posílení
koncepční	koncepční	k2eAgFnSc2d1	koncepční
úlohy	úloha	k1gFnSc2	úloha
federálního	federální	k2eAgNnSc2d1	federální
centra	centrum	k1gNnSc2	centrum
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ústavní	ústavní	k2eAgInSc1d1	ústavní
zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
125	[number]	k4	125
<g/>
/	/	kIx~	/
<g/>
1970	[number]	k4	1970
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
přinášel	přinášet	k5eAaImAgInS	přinášet
37	[number]	k4	37
přímých	přímý	k2eAgFnPc2d1	přímá
změn	změna	k1gFnPc2	změna
a	a	k8xC	a
doplňků	doplněk	k1gInPc2	doplněk
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
znamenaly	znamenat	k5eAaImAgFnP	znamenat
likvidaci	likvidace	k1gFnSc4	likvidace
původní	původní	k2eAgFnSc2d1	původní
koncepce	koncepce	k1gFnSc2	koncepce
federace	federace	k1gFnSc2	federace
a	a	k8xC	a
pro	pro	k7c4	pro
obě	dva	k4xCgFnPc4	dva
republiky	republika	k1gFnPc4	republika
ztrátu	ztráta	k1gFnSc4	ztráta
podstatné	podstatný	k2eAgFnPc4d1	podstatná
části	část	k1gFnPc4	část
svých	svůj	k3xOyFgFnPc2	svůj
kompetencí	kompetence	k1gFnPc2	kompetence
<g/>
.	.	kIx.	.
</s>
<s>
Československo	Československo	k1gNnSc1	Československo
tak	tak	k6eAd1	tak
bylo	být	k5eAaImAgNnS	být
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
centralisticky	centralisticky	k6eAd1	centralisticky
spravovaným	spravovaný	k2eAgInSc7d1	spravovaný
státem	stát	k1gInSc7	stát
s	s	k7c7	s
vnějšími	vnější	k2eAgInPc7d1	vnější
formálními	formální	k2eAgInPc7d1	formální
znaky	znak	k1gInPc7	znak
federace	federace	k1gFnSc2	federace
<g/>
.	.	kIx.	.
</s>
<s>
Nutnost	nutnost	k1gFnSc1	nutnost
přípravy	příprava	k1gFnSc2	příprava
nové	nový	k2eAgFnSc2d1	nová
federální	federální	k2eAgFnSc2d1	federální
ústavy	ústava	k1gFnSc2	ústava
zazněla	zaznět	k5eAaImAgFnS	zaznět
poprvé	poprvé	k6eAd1	poprvé
oficiálně	oficiálně	k6eAd1	oficiálně
na	na	k7c6	na
XVII	XVII	kA	XVII
<g/>
.	.	kIx.	.
sjezdu	sjezd	k1gInSc2	sjezd
Komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
Československa	Československo	k1gNnSc2	Československo
(	(	kIx(	(
<g/>
KSČ	KSČ	kA	KSČ
<g/>
)	)	kIx)	)
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1986	[number]	k4	1986
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
fungovala	fungovat	k5eAaImAgFnS	fungovat
pracovní	pracovní	k2eAgFnSc1d1	pracovní
skupina	skupina	k1gFnSc1	skupina
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Mariánem	Marián	k1gMnSc7	Marián
Čalfou	Čalfa	k1gMnSc7	Čalfa
a	a	k8xC	a
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1988	[number]	k4	1988
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
153	[number]	k4	153
<g/>
členná	členný	k2eAgFnSc1d1	členná
komise	komise	k1gFnSc1	komise
KSČ	KSČ	kA	KSČ
a	a	k8xC	a
Národní	národní	k2eAgFnSc2d1	národní
fronty	fronta	k1gFnSc2	fronta
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Milošem	Miloš	k1gMnSc7	Miloš
Jakešem	Jakeš	k1gMnSc7	Jakeš
pro	pro	k7c4	pro
přípravu	příprava	k1gFnSc4	příprava
nové	nový	k2eAgFnSc2d1	nová
ústavy	ústava	k1gFnSc2	ústava
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
přijata	přijmout	k5eAaPmNgFnS	přijmout
po	po	k7c6	po
XVIII	XVIII	kA	XVIII
<g/>
.	.	kIx.	.
sjezdu	sjezd	k1gInSc6	sjezd
KSČ	KSČ	kA	KSČ
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledním	poslední	k2eAgInSc6d1	poslední
pracovním	pracovní	k2eAgInSc6d1	pracovní
textu	text	k1gInSc6	text
byla	být	k5eAaImAgFnS	být
ústava	ústava	k1gFnSc1	ústava
koncipována	koncipovat	k5eAaBmNgFnS	koncipovat
jako	jako	k8xC	jako
společná	společný	k2eAgFnSc1d1	společná
pro	pro	k7c4	pro
federaci	federace	k1gFnSc4	federace
i	i	k9	i
obě	dva	k4xCgFnPc4	dva
republiky	republika	k1gFnPc4	republika
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
vypuštěno	vypuštěn	k2eAgNnSc1d1	vypuštěno
ustanovení	ustanovení	k1gNnSc1	ustanovení
o	o	k7c6	o
vedoucí	vedoucí	k2eAgFnSc6d1	vedoucí
úloze	úloha	k1gFnSc6	úloha
KSČ	KSČ	kA	KSČ
a	a	k8xC	a
do	do	k7c2	do
určité	určitý	k2eAgFnSc2d1	určitá
míry	míra	k1gFnSc2	míra
rozšířen	rozšířen	k2eAgInSc1d1	rozšířen
katalog	katalog	k1gInSc1	katalog
základních	základní	k2eAgNnPc2d1	základní
práv	právo	k1gNnPc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
1989	[number]	k4	1989
navrhla	navrhnout	k5eAaPmAgFnS	navrhnout
skupina	skupina	k1gFnSc1	skupina
poslanců	poslanec	k1gMnPc2	poslanec
Federálního	federální	k2eAgNnSc2d1	federální
shromáždění	shromáždění	k1gNnSc2	shromáždění
vydat	vydat	k5eAaPmF	vydat
ústavní	ústavní	k2eAgInSc4d1	ústavní
zákon	zákon	k1gInSc4	zákon
o	o	k7c6	o
způsobu	způsob	k1gInSc6	způsob
přijetí	přijetí	k1gNnSc4	přijetí
nové	nový	k2eAgFnSc2d1	nová
Ústavy	ústava	k1gFnSc2	ústava
Československé	československý	k2eAgFnSc2d1	Československá
socialistické	socialistický	k2eAgFnSc2d1	socialistická
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
České	český	k2eAgFnSc2d1	Česká
socialistické	socialistický	k2eAgFnSc2d1	socialistická
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
socialistické	socialistický	k2eAgFnSc2d1	socialistická
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
národní	národní	k2eAgFnPc1d1	národní
rady	rada	k1gFnPc1	rada
obou	dva	k4xCgFnPc2	dva
republik	republika	k1gFnPc2	republika
neměly	mít	k5eNaImAgFnP	mít
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
návrhy	návrh	k1gInPc1	návrh
nových	nový	k2eAgFnPc2d1	nová
ústav	ústava	k1gFnPc2	ústava
nebo	nebo	k8xC	nebo
společné	společný	k2eAgFnSc2d1	společná
ústavy	ústava	k1gFnSc2	ústava
<g/>
,	,	kIx,	,
vyslovily	vyslovit	k5eAaPmAgFnP	vyslovit
31	[number]	k4	31
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1989	[number]	k4	1989
souhlas	souhlas	k1gInSc1	souhlas
s	s	k7c7	s
návrhem	návrh	k1gInSc7	návrh
ústavního	ústavní	k2eAgInSc2d1	ústavní
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
způsobu	způsob	k1gInSc6	způsob
jejich	jejich	k3xOp3gNnSc2	jejich
přijetí	přijetí	k1gNnSc2	přijetí
<g/>
:	:	kIx,	:
usnesení	usnesení	k1gNnSc1	usnesení
České	český	k2eAgFnSc2d1	Česká
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
bylo	být	k5eAaImAgNnS	být
publikováno	publikovat	k5eAaBmNgNnS	publikovat
jako	jako	k9	jako
č.	č.	k?	č.
123	[number]	k4	123
<g/>
/	/	kIx~	/
<g/>
1989	[number]	k4	1989
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
usnesení	usnesení	k1gNnSc1	usnesení
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
jako	jako	k8xC	jako
č.	č.	k?	č.
124	[number]	k4	124
<g/>
/	/	kIx~	/
<g/>
1989	[number]	k4	1989
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
Přijetím	přijetí	k1gNnSc7	přijetí
návrhu	návrh	k1gInSc2	návrh
ústavního	ústavní	k2eAgInSc2d1	ústavní
zákona	zákon	k1gInSc2	zákon
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
vytvořeny	vytvořen	k2eAgFnPc4d1	vytvořena
podmínky	podmínka	k1gFnPc4	podmínka
pro	pro	k7c4	pro
vydání	vydání	k1gNnSc4	vydání
trojjediné	trojjediný	k2eAgFnSc2d1	trojjediná
ústavy	ústava	k1gFnSc2	ústava
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
společné	společný	k2eAgFnSc2d1	společná
ústavy	ústava	k1gFnSc2	ústava
federace	federace	k1gFnSc2	federace
i	i	k8xC	i
obou	dva	k4xCgFnPc2	dva
republik	republika	k1gFnPc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Souhlas	souhlas	k1gInSc1	souhlas
národních	národní	k2eAgFnPc2d1	národní
rad	rada	k1gFnPc2	rada
s	s	k7c7	s
přijetím	přijetí	k1gNnSc7	přijetí
ústavního	ústavní	k2eAgInSc2d1	ústavní
zákona	zákon	k1gInSc2	zákon
znamenal	znamenat	k5eAaImAgInS	znamenat
současně	současně	k6eAd1	současně
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
republiky	republika	k1gFnPc1	republika
vzdaly	vzdát	k5eAaPmAgFnP	vzdát
svého	svůj	k3xOyFgNnSc2	svůj
práva	právo	k1gNnSc2	právo
na	na	k7c4	na
vlastní	vlastní	k2eAgFnSc4d1	vlastní
ústavu	ústava	k1gFnSc4	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Přelom	přelom	k1gInSc1	přelom
však	však	k9	však
nastal	nastat	k5eAaPmAgInS	nastat
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
událostí	událost	k1gFnPc2	událost
sametové	sametový	k2eAgFnSc2d1	sametová
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
30	[number]	k4	30
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1989	[number]	k4	1989
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
na	na	k7c6	na
plénu	plénum	k1gNnSc6	plénum
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
dotazoval	dotazovat	k5eAaImAgMnS	dotazovat
poslanec	poslanec	k1gMnSc1	poslanec
Majer	Majer	k1gMnSc1	Majer
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
přijetím	přijetí	k1gNnSc7	přijetí
nové	nový	k2eAgFnSc2d1	nová
ústavy	ústava	k1gFnSc2	ústava
znovu	znovu	k6eAd1	znovu
schvalovat	schvalovat	k5eAaImF	schvalovat
i	i	k9	i
ustanovení	ustanovení	k1gNnSc4	ustanovení
o	o	k7c6	o
způsobu	způsob	k1gInSc6	způsob
přijetí	přijetí	k1gNnSc2	přijetí
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
zda	zda	k8xS	zda
usnesení	usnesení	k1gNnSc1	usnesení
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
z	z	k7c2	z
konce	konec	k1gInSc2	konec
října	říjen	k1gInSc2	říjen
zůstane	zůstat	k5eAaPmIp3nS	zůstat
netknuté	tknutý	k2eNgNnSc1d1	netknuté
<g/>
.	.	kIx.	.
</s>
<s>
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
národní	národní	k2eAgFnSc1d1	národní
rada	rada	k1gFnSc1	rada
pak	pak	k6eAd1	pak
na	na	k7c6	na
dalším	další	k2eAgNnSc6d1	další
zasedání	zasedání	k1gNnSc6	zasedání
6	[number]	k4	6
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1989	[number]	k4	1989
svůj	svůj	k3xOyFgInSc4	svůj
souhlas	souhlas	k1gInSc4	souhlas
z	z	k7c2	z
31	[number]	k4	31
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1989	[number]	k4	1989
usnesením	usnesení	k1gNnSc7	usnesení
č.	č.	k?	č.
167	[number]	k4	167
<g/>
/	/	kIx~	/
<g/>
1989	[number]	k4	1989
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
zrušila	zrušit	k5eAaPmAgFnS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
národní	národní	k2eAgFnSc1d1	národní
rada	rada	k1gFnSc1	rada
přijala	přijmout	k5eAaPmAgFnS	přijmout
obdobné	obdobný	k2eAgNnSc4d1	obdobné
usnesení	usnesení	k1gNnSc4	usnesení
(	(	kIx(	(
<g/>
č.	č.	k?	č.
166	[number]	k4	166
<g/>
/	/	kIx~	/
<g/>
1989	[number]	k4	1989
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
19	[number]	k4	19
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujícím	následující	k2eAgNnSc6d1	následující
období	období	k1gNnSc6	období
byla	být	k5eAaImAgFnS	být
přijata	přijat	k2eAgFnSc1d1	přijata
řada	řada	k1gFnSc1	řada
ústavních	ústavní	k2eAgInPc2d1	ústavní
zákonů	zákon	k1gInPc2	zákon
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
měly	mít	k5eAaImAgInP	mít
směřovat	směřovat	k5eAaImF	směřovat
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
obnovení	obnovení	k1gNnSc2	obnovení
<g/>
,	,	kIx,	,
reálné	reálný	k2eAgFnSc2d1	reálná
federace	federace	k1gFnSc2	federace
<g/>
.	.	kIx.	.
</s>
<s>
Republiky	republika	k1gFnPc1	republika
přijaly	přijmout	k5eAaPmAgFnP	přijmout
ústavní	ústavní	k2eAgInSc4d1	ústavní
zákony	zákon	k1gInPc4	zákon
o	o	k7c6	o
svých	svůj	k3xOyFgInPc6	svůj
symbolech	symbol	k1gInPc6	symbol
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
ně	on	k3xPp3gFnPc4	on
přenesena	přenesen	k2eAgFnSc1d1	přenesena
řada	řada	k1gFnSc1	řada
dosud	dosud	k6eAd1	dosud
federálních	federální	k2eAgFnPc2d1	federální
kompetencí	kompetence	k1gFnPc2	kompetence
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
voleb	volba	k1gFnPc2	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
však	však	k9	však
nebyly	být	k5eNaImAgInP	být
přijaty	přijmout	k5eAaPmNgInP	přijmout
nové	nový	k2eAgInPc1d1	nový
ústavy	ústav	k1gInPc1	ústav
ani	ani	k8xC	ani
federace	federace	k1gFnSc1	federace
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
obou	dva	k4xCgFnPc2	dva
republik	republika	k1gFnPc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dnech	den	k1gInPc6	den
5	[number]	k4	5
<g/>
.	.	kIx.	.
až	až	k9	až
6	[number]	k4	6
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1992	[number]	k4	1992
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgFnP	konat
volby	volba	k1gFnPc1	volba
do	do	k7c2	do
Federálního	federální	k2eAgNnSc2d1	federální
shromáždění	shromáždění	k1gNnSc2	shromáždění
ČSFR	ČSFR	kA	ČSFR
<g/>
,	,	kIx,	,
České	český	k2eAgFnSc2d1	Česká
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
a	a	k8xC	a
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
Občanská	občanský	k2eAgFnSc1d1	občanská
demokratická	demokratický	k2eAgFnSc1d1	demokratická
strana	strana	k1gFnSc1	strana
(	(	kIx(	(
<g/>
ODS	ODS	kA	ODS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
volebním	volební	k2eAgInSc6d1	volební
programu	program	k1gInSc6	program
hovořila	hovořit	k5eAaImAgFnS	hovořit
buď	buď	k8xC	buď
o	o	k7c4	o
fungující	fungující	k2eAgFnSc4d1	fungující
federaci	federace	k1gFnSc4	federace
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
rozdělení	rozdělení	k1gNnSc1	rozdělení
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
spíše	spíše	k9	spíše
strana	strana	k1gFnSc1	strana
dávala	dávat	k5eAaImAgFnS	dávat
přednost	přednost	k1gFnSc4	přednost
zachování	zachování	k1gNnSc2	zachování
federace	federace	k1gFnSc2	federace
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
ostatně	ostatně	k6eAd1	ostatně
právě	právě	k9	právě
ODS	ODS	kA	ODS
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
jako	jako	k9	jako
jediná	jediný	k2eAgFnSc1d1	jediná
z	z	k7c2	z
tehdy	tehdy	k6eAd1	tehdy
významných	významný	k2eAgFnPc2d1	významná
politických	politický	k2eAgFnPc2d1	politická
stran	strana	k1gFnPc2	strana
kandidovala	kandidovat	k5eAaImAgFnS	kandidovat
v	v	k7c6	v
obou	dva	k4xCgFnPc6	dva
částech	část	k1gFnPc6	část
federace	federace	k1gFnSc2	federace
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
zvítězilo	zvítězit	k5eAaPmAgNnS	zvítězit
Hnutí	hnutí	k1gNnSc1	hnutí
za	za	k7c4	za
demokratické	demokratický	k2eAgNnSc4d1	demokratické
Slovensko	Slovensko	k1gNnSc4	Slovensko
(	(	kIx(	(
<g/>
HZDS	HZDS	kA	HZDS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
prosazovalo	prosazovat	k5eAaImAgNnS	prosazovat
především	především	k9	především
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
subjektivitu	subjektivita	k1gFnSc4	subjektivita
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
s	s	k7c7	s
existencí	existence	k1gFnSc7	existence
společného	společný	k2eAgInSc2d1	společný
státu	stát	k1gInSc2	stát
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
vylučovalo	vylučovat	k5eAaImAgNnS	vylučovat
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
HZDS	HZDS	kA	HZDS
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
voliče	volič	k1gMnPc4	volič
o	o	k7c6	o
slučitelnosti	slučitelnost	k1gFnSc6	slučitelnost
takových	takový	k3xDgInPc2	takový
požadavků	požadavek	k1gInPc2	požadavek
s	s	k7c7	s
existencí	existence	k1gFnSc7	existence
federace	federace	k1gFnSc2	federace
<g/>
.	.	kIx.	.
</s>
<s>
Ústava	ústava	k1gFnSc1	ústava
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
republiky	republika	k1gFnSc2	republika
byla	být	k5eAaImAgFnS	být
přijata	přijat	k2eAgFnSc1d1	přijata
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1992	[number]	k4	1992
a	a	k8xC	a
účinnosti	účinnost	k1gFnSc3	účinnost
nabyla	nabýt	k5eAaPmAgFnS	nabýt
již	již	k6eAd1	již
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
tři	tři	k4xCgInPc4	tři
měsíce	měsíc	k1gInPc4	měsíc
před	před	k7c7	před
zánikem	zánik	k1gInSc7	zánik
federace	federace	k1gFnSc2	federace
<g/>
.	.	kIx.	.
</s>
<s>
Nedlouho	dlouho	k6eNd1	dlouho
po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
byla	být	k5eAaImAgFnS	být
zahájena	zahájen	k2eAgFnSc1d1	zahájena
i	i	k8xC	i
příprava	příprava	k1gFnSc1	příprava
Ústavy	ústava	k1gFnSc2	ústava
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k6eAd1	vedle
sebe	sebe	k3xPyFc4	sebe
fungovaly	fungovat	k5eAaImAgFnP	fungovat
dvě	dva	k4xCgFnPc1	dva
komise	komise	k1gFnPc1	komise
<g/>
:	:	kIx,	:
komise	komise	k1gFnSc1	komise
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
komise	komise	k1gFnSc2	komise
předsednictva	předsednictvo	k1gNnSc2	předsednictvo
České	český	k2eAgFnSc2d1	Česká
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
</s>
<s>
Vládní	vládní	k2eAgFnSc1d1	vládní
komise	komise	k1gFnSc1	komise
byla	být	k5eAaImAgFnS	být
zřízena	zřídit	k5eAaPmNgFnS	zřídit
usnesením	usnesení	k1gNnSc7	usnesení
vlády	vláda	k1gFnSc2	vláda
č.	č.	k?	č.
484	[number]	k4	484
z	z	k7c2	z
10	[number]	k4	10
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Předsedou	předseda	k1gMnSc7	předseda
komise	komise	k1gFnSc2	komise
byl	být	k5eAaImAgMnS	být
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
<g/>
,	,	kIx,	,
místopředsedou	místopředseda	k1gMnSc7	místopředseda
Jan	Jan	k1gMnSc1	Jan
Kalvoda	Kalvoda	k1gMnSc1	Kalvoda
<g/>
,	,	kIx,	,
sekretářem	sekretář	k1gMnSc7	sekretář
Cyril	Cyril	k1gMnSc1	Cyril
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
členové	člen	k1gMnPc1	člen
komise	komise	k1gFnSc2	komise
byli	být	k5eAaImAgMnP	být
Filip	Filip	k1gMnSc1	Filip
Šedivý	Šedivý	k1gMnSc1	Šedivý
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Vlach	Vlach	k1gMnSc1	Vlach
<g/>
,	,	kIx,	,
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Cepl	Cepl	k1gMnSc1	Cepl
<g/>
,	,	kIx,	,
Daniel	Daniel	k1gMnSc1	Daniel
Kroupa	Kroupa	k1gMnSc1	Kroupa
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Benda	Benda	k1gMnSc1	Benda
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Pečich	Pečich	k1gMnSc1	Pečich
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Litomiský	Litomiský	k1gMnSc1	Litomiský
<g/>
,	,	kIx,	,
Miloslav	Miloslav	k1gMnSc1	Miloslav
Výborný	Výborný	k1gMnSc1	Výborný
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Novotný	Novotný	k1gMnSc1	Novotný
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
Sylla	Sylla	k1gMnSc1	Sylla
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Zářecký	zářecký	k2eAgMnSc1d1	zářecký
a	a	k8xC	a
Dušan	Dušan	k1gMnSc1	Dušan
Hendrych	Hendrych	k1gMnSc1	Hendrych
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
si	se	k3xPyFc3	se
přál	přát	k5eAaImAgMnS	přát
<g/>
,	,	kIx,	,
aby	aby	k9	aby
členem	člen	k1gMnSc7	člen
komise	komise	k1gFnSc2	komise
byl	být	k5eAaImAgMnS	být
i	i	k9	i
jeho	jeho	k3xOp3gMnSc1	jeho
poradce	poradce	k1gMnSc1	poradce
Vladimír	Vladimír	k1gMnSc1	Vladimír
Klokočka	Klokočka	k1gFnSc1	Klokočka
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc1	ten
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
na	na	k7c6	na
činnosti	činnost	k1gFnSc6	činnost
komise	komise	k1gFnSc2	komise
přímo	přímo	k6eAd1	přímo
nepodílel	podílet	k5eNaImAgMnS	podílet
<g/>
,	,	kIx,	,
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1992	[number]	k4	1992
byl	být	k5eAaImAgInS	být
pak	pak	k6eAd1	pak
ale	ale	k8xC	ale
jedním	jeden	k4xCgMnSc7	jeden
ze	z	k7c2	z
tří	tři	k4xCgMnPc2	tři
profesorů	profesor	k1gMnPc2	profesor
ústavního	ústavní	k2eAgNnSc2d1	ústavní
práva	právo	k1gNnSc2	právo
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
podíleli	podílet	k5eAaImAgMnP	podílet
na	na	k7c6	na
závěrečné	závěrečný	k2eAgFnSc6d1	závěrečná
oponentuře	oponentura	k1gFnSc6	oponentura
Ústavy	ústava	k1gFnSc2	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Členy	člen	k1gMnPc4	člen
komise	komise	k1gFnSc2	komise
předsednictva	předsednictvo	k1gNnSc2	předsednictvo
ČNR	ČNR	kA	ČNR
byli	být	k5eAaImAgMnP	být
Marek	Marek	k1gMnSc1	Marek
Benda	Benda	k1gMnSc1	Benda
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Bílý	bílý	k1gMnSc1	bílý
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Hirsch	Hirsch	k1gMnSc1	Hirsch
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
Hrazdíra	Hrazdíra	k1gMnSc1	Hrazdíra
<g/>
,	,	kIx,	,
Ivana	Ivana	k1gFnSc1	Ivana
Janů	Janů	k1gFnSc1	Janů
<g/>
,	,	kIx,	,
Hana	Hana	k1gFnSc1	Hana
Marvanová	Marvanová	k1gFnSc1	Marvanová
<g/>
,	,	kIx,	,
Ivan	Ivan	k1gMnSc1	Ivan
Mašek	Mašek	k1gMnSc1	Mašek
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Ortman	Ortman	k1gMnSc1	Ortman
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc5	Jiří
Payne	Payn	k1gMnSc5	Payn
<g/>
,	,	kIx,	,
Anna	Anna	k1gFnSc1	Anna
Röschová	Röschová	k1gFnSc1	Röschová
<g/>
,	,	kIx,	,
Vítězslav	Vítězslav	k1gMnSc1	Vítězslav
Sochor	Sochor	k1gMnSc1	Sochor
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
Uhde	Uhd	k1gFnSc2	Uhd
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
Vik	Vik	k1gMnSc1	Vik
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
srpna	srpen	k1gInSc2	srpen
1992	[number]	k4	1992
bylo	být	k5eAaImAgNnS	být
dohodnuto	dohodnout	k5eAaPmNgNnS	dohodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
základem	základ	k1gInSc7	základ
pro	pro	k7c4	pro
další	další	k2eAgNnSc4d1	další
jednání	jednání	k1gNnSc4	jednání
bude	být	k5eAaImBp3nS	být
návrh	návrh	k1gInSc4	návrh
Ústavy	ústava	k1gFnSc2	ústava
od	od	k7c2	od
vládní	vládní	k2eAgFnSc2d1	vládní
komise	komise	k1gFnSc2	komise
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
činnosti	činnost	k1gFnSc2	činnost
vládní	vládní	k2eAgFnSc2d1	vládní
komise	komise	k1gFnSc2	komise
se	se	k3xPyFc4	se
zachovaly	zachovat	k5eAaPmAgInP	zachovat
jen	jen	k9	jen
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
dokumenty	dokument	k1gInPc1	dokument
–	–	k?	–
směrnice	směrnice	k1gFnSc2	směrnice
<g/>
,	,	kIx,	,
zásady	zásada	k1gFnPc1	zásada
<g/>
,	,	kIx,	,
ideový	ideový	k2eAgInSc1d1	ideový
nástin	nástin	k1gInSc1	nástin
a	a	k8xC	a
ideový	ideový	k2eAgInSc1d1	ideový
návrh	návrh	k1gInSc1	návrh
(	(	kIx(	(
<g/>
červenec	červenec	k1gInSc1	červenec
<g/>
/	/	kIx~	/
<g/>
srpen	srpen	k1gInSc1	srpen
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zásady	zásada	k1gFnPc4	zásada
ústavy	ústava	k1gFnSc2	ústava
(	(	kIx(	(
<g/>
září	září	k1gNnSc2	září
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
a	a	k8xC	a
vládní	vládní	k2eAgInSc4d1	vládní
návrh	návrh	k1gInSc4	návrh
ústavy	ústava	k1gFnSc2	ústava
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
byl	být	k5eAaImAgInS	být
základem	základ	k1gInSc7	základ
pro	pro	k7c4	pro
budoucí	budoucí	k2eAgFnSc4d1	budoucí
českou	český	k2eAgFnSc4d1	Česká
ústavu	ústava	k1gFnSc4	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Komise	komise	k1gFnSc1	komise
předsednictva	předsednictvo	k1gNnSc2	předsednictvo
ČNR	ČNR	kA	ČNR
se	se	k3xPyFc4	se
sešla	sejít	k5eAaPmAgFnS	sejít
třináctkrát	třináctkrát	k6eAd1	třináctkrát
<g/>
.	.	kIx.	.
</s>
<s>
Možnosti	možnost	k1gFnSc3	možnost
tvorby	tvorba	k1gFnSc2	tvorba
nové	nový	k2eAgFnSc2d1	nová
ústavy	ústava	k1gFnSc2	ústava
shrnul	shrnout	k5eAaPmAgMnS	shrnout
již	již	k6eAd1	již
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
činnosti	činnost	k1gFnSc2	činnost
vládní	vládní	k2eAgFnSc2d1	vládní
komise	komise	k1gFnSc2	komise
její	její	k3xOp3gMnSc1	její
tajemník	tajemník	k1gMnSc1	tajemník
Cyril	Cyril	k1gMnSc1	Cyril
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
:	:	kIx,	:
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
vycházet	vycházet	k5eAaImF	vycházet
z	z	k7c2	z
československé	československý	k2eAgFnSc2d1	Československá
ústavy	ústava	k1gFnSc2	ústava
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
<g/>
,	,	kIx,	,
přepracovat	přepracovat	k5eAaPmF	přepracovat
dosavadní	dosavadní	k2eAgFnSc4d1	dosavadní
Ústavu	ústava	k1gFnSc4	ústava
ČSFR	ČSFR	kA	ČSFR
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
vypracovat	vypracovat	k5eAaPmF	vypracovat
zcela	zcela	k6eAd1	zcela
nový	nový	k2eAgInSc4d1	nový
dokument	dokument	k1gInSc4	dokument
<g/>
.	.	kIx.	.
</s>
<s>
Nejen	nejen	k6eAd1	nejen
Svoboda	svoboda	k1gFnSc1	svoboda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
ostatní	ostatní	k2eAgMnPc1d1	ostatní
členové	člen	k1gMnPc1	člen
komise	komise	k1gFnSc2	komise
směřovali	směřovat	k5eAaImAgMnP	směřovat
k	k	k7c3	k
možnosti	možnost	k1gFnSc3	možnost
využití	využití	k1gNnSc2	využití
prvorepublikové	prvorepublikový	k2eAgFnSc2d1	prvorepubliková
ústavy	ústava	k1gFnSc2	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Problematické	problematický	k2eAgNnSc1d1	problematické
se	se	k3xPyFc4	se
jevilo	jevit	k5eAaImAgNnS	jevit
především	především	k9	především
postavení	postavení	k1gNnSc4	postavení
Listiny	listina	k1gFnSc2	listina
základních	základní	k2eAgNnPc2d1	základní
práv	právo	k1gNnPc2	právo
a	a	k8xC	a
svobod	svoboda	k1gFnPc2	svoboda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
nesena	nést	k5eAaImNgFnS	nést
samostatným	samostatný	k2eAgInSc7d1	samostatný
ústavním	ústavní	k2eAgInSc7d1	ústavní
zákonem	zákon	k1gInSc7	zákon
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
jí	on	k3xPp3gFnSc3	on
zajišťoval	zajišťovat	k5eAaImAgMnS	zajišťovat
nadústavní	nadústavní	k2eAgInSc4d1	nadústavní
charakter	charakter	k1gInSc4	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Zařazení	zařazení	k1gNnSc1	zařazení
Listiny	listina	k1gFnSc2	listina
do	do	k7c2	do
Ústavy	ústava	k1gFnSc2	ústava
se	se	k3xPyFc4	se
jevilo	jevit	k5eAaImAgNnS	jevit
jako	jako	k9	jako
politicky	politicky	k6eAd1	politicky
neprůchodné	průchodný	k2eNgFnPc1d1	neprůchodná
především	především	k9	především
kvůli	kvůli	k7c3	kvůli
Václavu	Václav	k1gMnSc3	Václav
Klausovi	Klaus	k1gMnSc3	Klaus
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
Listinu	listina	k1gFnSc4	listina
nechtěl	chtít	k5eNaImAgMnS	chtít
vůbec	vůbec	k9	vůbec
a	a	k8xC	a
její	její	k3xOp3gNnSc1	její
případné	případný	k2eAgNnSc1d1	případné
včlenění	včlenění	k1gNnSc1	včlenění
přímo	přímo	k6eAd1	přímo
do	do	k7c2	do
textu	text	k1gInSc2	text
Ústavy	ústava	k1gFnSc2	ústava
nazval	nazvat	k5eAaBmAgInS	nazvat
"	"	kIx"	"
<g/>
plevelem	plevel	k1gInSc7	plevel
Ústavy	ústava	k1gFnSc2	ústava
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Klausovi	Klaus	k1gMnSc3	Klaus
především	především	k9	především
vadila	vadit	k5eAaImAgFnS	vadit
formulace	formulace	k1gFnSc1	formulace
čl	čl	kA	čl
<g/>
.	.	kIx.	.
17	[number]	k4	17
Listiny	listina	k1gFnSc2	listina
o	o	k7c6	o
právu	právo	k1gNnSc6	právo
na	na	k7c4	na
informace	informace	k1gFnPc4	informace
<g/>
,	,	kIx,	,
když	když	k8xS	když
byl	být	k5eAaImAgInS	být
spíše	spíše	k9	spíše
pro	pro	k7c4	pro
formulaci	formulace	k1gFnSc4	formulace
práva	právo	k1gNnPc4	právo
vyhledávat	vyhledávat	k5eAaImF	vyhledávat
informace	informace	k1gFnPc4	informace
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
mu	on	k3xPp3gNnSc3	on
vadilo	vadit	k5eAaImAgNnS	vadit
právo	právo	k1gNnSc1	právo
sdružovat	sdružovat	k5eAaImF	sdružovat
se	se	k3xPyFc4	se
v	v	k7c6	v
odborech	odbor	k1gInPc6	odbor
a	a	k8xC	a
právo	právo	k1gNnSc1	právo
na	na	k7c4	na
odměnu	odměna	k1gFnSc4	odměna
za	za	k7c4	za
práci	práce	k1gFnSc4	práce
(	(	kIx(	(
<g/>
čl	čl	kA	čl
<g/>
.	.	kIx.	.
28	[number]	k4	28
Listiny	listina	k1gFnSc2	listina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Problém	problém	k1gInSc1	problém
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgInS	pokusit
vyřešit	vyřešit	k5eAaPmF	vyřešit
Miroslav	Miroslav	k1gMnSc1	Miroslav
Výborný	Výborný	k1gMnSc1	Výborný
vytvořením	vytvoření	k1gNnSc7	vytvoření
koncepce	koncepce	k1gFnSc2	koncepce
tzv.	tzv.	kA	tzv.
ústavního	ústavní	k2eAgInSc2d1	ústavní
pořádku	pořádek	k1gInSc2	pořádek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
teoretiky	teoretik	k1gMnPc4	teoretik
kritizován	kritizován	k2eAgMnSc1d1	kritizován
(	(	kIx(	(
<g/>
Filip	Filip	k1gMnSc1	Filip
<g/>
,	,	kIx,	,
Knapp	Knapp	k1gMnSc1	Knapp
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
týdnu	týden	k1gInSc6	týden
od	od	k7c2	od
19	[number]	k4	19
<g/>
.	.	kIx.	.
do	do	k7c2	do
24	[number]	k4	24
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1992	[number]	k4	1992
se	se	k3xPyFc4	se
v	v	k7c6	v
Karlových	Karlův	k2eAgInPc6d1	Karlův
Varech	Vary	k1gInPc6	Vary
začala	začít	k5eAaPmAgFnS	začít
připravovat	připravovat	k5eAaImF	připravovat
čistá	čistý	k2eAgFnSc1d1	čistá
verze	verze	k1gFnSc1	verze
Ústavy	ústava	k1gFnSc2	ústava
<g/>
:	:	kIx,	:
první	první	k4xOgInPc4	první
články	článek	k1gInPc1	článek
byly	být	k5eAaImAgInP	být
převzaty	převzít	k5eAaPmNgInP	převzít
ze	z	k7c2	z
starších	starý	k2eAgInPc2d2	starší
návrhů	návrh	k1gInPc2	návrh
<g/>
,	,	kIx,	,
články	článek	k1gInPc1	článek
o	o	k7c6	o
moci	moc	k1gFnSc6	moc
zákonodárné	zákonodárný	k2eAgFnSc2d1	zákonodárná
psali	psát	k5eAaImAgMnP	psát
Miroslav	Miroslav	k1gMnSc1	Miroslav
a	a	k8xC	a
Jindřiška	Jindřiška	k1gFnSc1	Jindřiška
Syllovi	Syllovi	k1gRnPc1	Syllovi
<g/>
,	,	kIx,	,
články	článek	k1gInPc7	článek
k	k	k7c3	k
prezidentovi	prezident	k1gMnSc3	prezident
psal	psát	k5eAaImAgMnS	psát
Cyril	Cyril	k1gMnSc1	Cyril
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
články	článek	k1gInPc1	článek
o	o	k7c6	o
vládě	vláda	k1gFnSc6	vláda
Dušan	Dušan	k1gMnSc1	Dušan
Hendrych	Hendrych	k1gMnSc1	Hendrych
<g/>
,	,	kIx,	,
moc	moc	k6eAd1	moc
soudní	soudní	k2eAgMnSc1d1	soudní
pak	pak	k6eAd1	pak
František	František	k1gMnSc1	František
Zoulík	Zoulík	k1gMnSc1	Zoulík
<g/>
.	.	kIx.	.
</s>
<s>
Hendrych	Hendrych	k1gMnSc1	Hendrych
také	také	k9	také
napsal	napsat	k5eAaBmAgMnS	napsat
pasáže	pasáž	k1gFnPc4	pasáž
týkající	týkající	k2eAgFnPc4d1	týkající
se	se	k3xPyFc4	se
České	český	k2eAgFnSc2d1	Česká
národní	národní	k2eAgFnSc2d1	národní
banky	banka	k1gFnSc2	banka
a	a	k8xC	a
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
kontrolního	kontrolní	k2eAgInSc2d1	kontrolní
úřadu	úřad	k1gInSc2	úřad
<g/>
,	,	kIx,	,
územní	územní	k2eAgFnSc4d1	územní
samosprávu	samospráva	k1gFnSc4	samospráva
psal	psát	k5eAaImAgMnS	psát
Pavel	Pavel	k1gMnSc1	Pavel
Zářecký	zářecký	k2eAgMnSc1d1	zářecký
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
23	[number]	k4	23
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1992	[number]	k4	1992
přijeli	přijet	k5eAaPmAgMnP	přijet
provést	provést	k5eAaPmF	provést
závěrečnou	závěrečný	k2eAgFnSc4d1	závěrečná
oponenturu	oponentura	k1gFnSc4	oponentura
tři	tři	k4xCgMnPc1	tři
ústavní	ústavní	k2eAgMnPc1d1	ústavní
právníci	právník	k1gMnPc1	právník
<g/>
:	:	kIx,	:
Pavel	Pavel	k1gMnSc1	Pavel
Peška	Peška	k1gMnSc1	Peška
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Klokočka	Klokočka	k1gFnSc1	Klokočka
a	a	k8xC	a
Pavel	Pavel	k1gMnSc1	Pavel
Holländer	Holländer	k1gMnSc1	Holländer
<g/>
.	.	kIx.	.
</s>
<s>
Důvodovou	důvodový	k2eAgFnSc4d1	Důvodová
zprávu	zpráva	k1gFnSc4	zpráva
ke	k	k7c3	k
karlovarskému	karlovarský	k2eAgInSc3d1	karlovarský
návrhu	návrh	k1gInSc3	návrh
Ústavy	ústava	k1gFnSc2	ústava
sepsal	sepsat	k5eAaPmAgMnS	sepsat
Cyril	Cyril	k1gMnSc1	Cyril
Svoboda	Svoboda	k1gMnSc1	Svoboda
s	s	k7c7	s
Milenou	Milena	k1gFnSc7	Milena
Polákovou	Poláková	k1gFnSc4	Poláková
víkend	víkend	k1gInSc4	víkend
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
než	než	k8xS	než
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
předložena	předložen	k2eAgFnSc1d1	předložena
premiéru	premiér	k1gMnSc3	premiér
Klausovi	Klaus	k1gMnSc3	Klaus
<g/>
.	.	kIx.	.
</s>
<s>
Karlovarskému	karlovarský	k2eAgInSc3d1	karlovarský
návrhu	návrh	k1gInSc3	návrh
se	se	k3xPyFc4	se
posléze	posléze	k6eAd1	posléze
věnovaly	věnovat	k5eAaPmAgFnP	věnovat
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
v	v	k7c6	v
Lánech	lán	k1gInPc6	lán
poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
výše	výše	k1gFnSc2	výše
zmíněné	zmíněný	k2eAgFnSc2d1	zmíněná
komise	komise	k1gFnSc2	komise
i	i	k8xC	i
ústavněprávní	ústavněprávní	k2eAgInSc1d1	ústavněprávní
výbor	výbor	k1gInSc1	výbor
České	český	k2eAgFnSc2d1	Česká
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
také	také	k9	také
zformulován	zformulován	k2eAgInSc1d1	zformulován
článek	článek	k1gInSc1	článek
9	[number]	k4	9
o	o	k7c6	o
podstatných	podstatný	k2eAgFnPc6d1	podstatná
náležitostech	náležitost	k1gFnPc6	náležitost
demokratického	demokratický	k2eAgInSc2d1	demokratický
právního	právní	k2eAgInSc2d1	právní
státu	stát	k1gInSc2	stát
inspirovaný	inspirovaný	k2eAgInSc1d1	inspirovaný
německým	německý	k2eAgInSc7d1	německý
Základním	základní	k2eAgInSc7d1	základní
zákonem	zákon	k1gInSc7	zákon
a	a	k8xC	a
navržený	navržený	k2eAgInSc1d1	navržený
Ivanou	Ivana	k1gFnSc7	Ivana
Janů	Janů	k1gFnPc2	Janů
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byl	být	k5eAaImAgInS	být
zde	zde	k6eAd1	zde
také	také	k9	také
zakotven	zakotvit	k5eAaPmNgInS	zakotvit
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
správní	správní	k2eAgInSc1d1	správní
soud	soud	k1gInSc1	soud
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nočních	noční	k2eAgFnPc6d1	noční
hodinách	hodina	k1gFnPc6	hodina
patnáctého	patnáctý	k4xOgInSc2	patnáctý
prosince	prosinec	k1gInSc2	prosinec
1992	[number]	k4	1992
se	se	k3xPyFc4	se
sešli	sejít	k5eAaPmAgMnP	sejít
zástupci	zástupce	k1gMnPc1	zástupce
vládních	vládní	k2eAgFnPc2d1	vládní
stran	strana	k1gFnPc2	strana
a	a	k8xC	a
uzavřeli	uzavřít	k5eAaPmAgMnP	uzavřít
písemnou	písemný	k2eAgFnSc4d1	písemná
dohodu	dohoda	k1gFnSc4	dohoda
vlastnoručně	vlastnoručně	k6eAd1	vlastnoručně
uvedenou	uvedený	k2eAgFnSc4d1	uvedená
na	na	k7c6	na
kusu	kus	k1gInSc6	kus
papíru	papír	k1gInSc2	papír
z	z	k7c2	z
poznámkového	poznámkový	k2eAgInSc2d1	poznámkový
bloku	blok	k1gInSc2	blok
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
sjednali	sjednat	k5eAaPmAgMnP	sjednat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nebude	být	k5eNaImBp3nS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
žádný	žádný	k3yNgInSc4	žádný
pozměňovací	pozměňovací	k2eAgInSc4d1	pozměňovací
návrh	návrh	k1gInSc4	návrh
předložený	předložený	k2eAgInSc4d1	předložený
v	v	k7c6	v
diskusi	diskuse	k1gFnSc6	diskuse
<g/>
,	,	kIx,	,
ledaže	ledaže	k8xS	ledaže
se	se	k3xPyFc4	se
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
dohodnou	dohodnout	k5eAaPmIp3nP	dohodnout
všechny	všechen	k3xTgFnPc4	všechen
vládní	vládní	k2eAgFnPc4d1	vládní
strany	strana	k1gFnPc4	strana
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
reakce	reakce	k1gFnSc1	reakce
na	na	k7c6	na
situaci	situace	k1gFnSc6	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
k	k	k7c3	k
přijetí	přijetí	k1gNnSc3	přijetí
pozměňovacího	pozměňovací	k2eAgInSc2d1	pozměňovací
návrhu	návrh	k1gInSc2	návrh
stačila	stačit	k5eAaBmAgFnS	stačit
(	(	kIx(	(
<g/>
a	a	k8xC	a
stačí	stačit	k5eAaBmIp3nS	stačit
<g/>
)	)	kIx)	)
většina	většina	k1gFnSc1	většina
přítomných	přítomný	k1gMnPc2	přítomný
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
k	k	k7c3	k
přijetí	přijetí	k1gNnSc3	přijetí
ústavního	ústavní	k2eAgInSc2d1	ústavní
zákona	zákon	k1gInSc2	zákon
je	být	k5eAaImIp3nS	být
potřebná	potřebný	k2eAgFnSc1d1	potřebná
třípětinová	třípětinový	k2eAgFnSc1d1	třípětinová
většina	většina	k1gFnSc1	většina
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
politickou	politický	k2eAgFnSc4d1	politická
dohodu	dohoda	k1gFnSc4	dohoda
nebyly	být	k5eNaImAgInP	být
přijaty	přijat	k2eAgInPc1d1	přijat
i	i	k8xC	i
takové	takový	k3xDgInPc1	takový
pozměňovací	pozměňovací	k2eAgInPc1d1	pozměňovací
návrhy	návrh	k1gInPc1	návrh
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc4	jenž
lze	lze	k6eAd1	lze
označit	označit	k5eAaPmF	označit
za	za	k7c4	za
kvalitní	kvalitní	k2eAgInPc4d1	kvalitní
a	a	k8xC	a
dobré	dobrý	k2eAgInPc4d1	dobrý
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
v	v	k7c6	v
období	období	k1gNnSc6	období
přípravy	příprava	k1gFnSc2	příprava
Ústavy	ústava	k1gFnSc2	ústava
samostatné	samostatný	k2eAgFnSc2d1	samostatná
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
čtyři	čtyři	k4xCgInPc1	čtyři
kompletní	kompletní	k2eAgInPc1d1	kompletní
návrhy	návrh	k1gInPc1	návrh
<g/>
:	:	kIx,	:
vládní	vládní	k2eAgInSc1d1	vládní
návrh	návrh	k1gInSc1	návrh
<g/>
,	,	kIx,	,
návrh	návrh	k1gInSc1	návrh
Československé	československý	k2eAgFnSc2d1	Československá
sociálně	sociálně	k6eAd1	sociálně
demokratické	demokratický	k2eAgFnSc2d1	demokratická
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
návrh	návrh	k1gInSc4	návrh
Komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
Moravy	Morava	k1gFnSc2	Morava
a	a	k8xC	a
návrh	návrh	k1gInSc1	návrh
Liberálně	liberálně	k6eAd1	liberálně
sociální	sociální	k2eAgFnSc2d1	sociální
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInPc1d1	poslední
tři	tři	k4xCgInPc1	tři
zmiňované	zmiňovaný	k2eAgInPc1d1	zmiňovaný
návrhy	návrh	k1gInPc1	návrh
nebyly	být	k5eNaImAgInP	být
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
národní	národní	k2eAgFnSc6d1	národní
radě	rada	k1gFnSc6	rada
projednány	projednán	k2eAgFnPc1d1	projednána
<g/>
.	.	kIx.	.
</s>
<s>
Vládní	vládní	k2eAgInSc1d1	vládní
návrh	návrh	k1gInSc1	návrh
ústavního	ústavní	k2eAgInSc2d1	ústavní
zákona	zákon	k1gInSc2	zákon
ČNR	ČNR	kA	ČNR
<g/>
,	,	kIx,	,
Ústava	ústava	k1gFnSc1	ústava
ČR	ČR	kA	ČR
(	(	kIx(	(
<g/>
sněmovní	sněmovní	k2eAgInPc1d1	sněmovní
tisky	tisk	k1gInPc1	tisk
152	[number]	k4	152
a	a	k8xC	a
154	[number]	k4	154
<g/>
)	)	kIx)	)
s	s	k7c7	s
upřesňujícím	upřesňující	k2eAgInSc7d1	upřesňující
dodatkem	dodatek	k1gInSc7	dodatek
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
projednán	projednat	k5eAaPmNgInS	projednat
Českou	český	k2eAgFnSc7d1	Česká
národní	národní	k2eAgFnSc7d1	národní
radou	rada	k1gFnSc7	rada
na	na	k7c6	na
její	její	k3xOp3gFnSc6	její
desáté	desátý	k4xOgFnSc6	desátý
schůzi	schůze	k1gFnSc6	schůze
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
16	[number]	k4	16
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Návrh	návrh	k1gInSc4	návrh
odůvodnil	odůvodnit	k5eAaPmAgMnS	odůvodnit
nejprve	nejprve	k6eAd1	nejprve
předseda	předseda	k1gMnSc1	předseda
republikové	republikový	k2eAgFnSc2d1	republiková
vlády	vláda	k1gFnSc2	vláda
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
úvodem	úvodem	k7c2	úvodem
svého	svůj	k3xOyFgInSc2	svůj
projevu	projev	k1gInSc2	projev
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
parlament	parlament	k1gInSc4	parlament
i	i	k8xC	i
vládu	vláda	k1gFnSc4	vláda
je	být	k5eAaImIp3nS	být
toto	tento	k3xDgNnSc4	tento
jednání	jednání	k1gNnSc4	jednání
zkouškou	zkouška	k1gFnSc7	zkouška
dospělosti	dospělost	k1gFnSc2	dospělost
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
návrhu	návrh	k1gInSc3	návrh
ústavního	ústavní	k2eAgInSc2d1	ústavní
zákona	zákon	k1gInSc2	zákon
byli	být	k5eAaImAgMnP	být
dva	dva	k4xCgMnPc4	dva
společní	společní	k2eAgMnPc1d1	společní
zpravodajové	zpravodaj	k1gMnPc1	zpravodaj
–	–	k?	–
Hana	Hana	k1gFnSc1	Hana
Marvanová	Marvanová	k1gFnSc1	Marvanová
a	a	k8xC	a
Miroslav	Miroslav	k1gMnSc1	Miroslav
Výborný	Výborný	k1gMnSc1	Výborný
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
množství	množství	k1gNnSc2	množství
pozměňovacích	pozměňovací	k2eAgInPc2d1	pozměňovací
návrhů	návrh	k1gInPc2	návrh
byl	být	k5eAaImAgMnS	být
úspěšný	úspěšný	k2eAgMnSc1d1	úspěšný
jen	jen	k6eAd1	jen
návrh	návrh	k1gInSc1	návrh
poslance	poslanec	k1gMnSc2	poslanec
Pavla	Pavel	k1gMnSc2	Pavel
Hirše	Hirš	k1gMnSc2	Hirš
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Ústavy	ústava	k1gFnSc2	ústava
poměrný	poměrný	k2eAgInSc1d1	poměrný
volební	volební	k2eAgInSc1d1	volební
systém	systém	k1gInSc1	systém
pro	pro	k7c4	pro
Sněmovnu	sněmovna	k1gFnSc4	sněmovna
a	a	k8xC	a
většinový	většinový	k2eAgInSc4d1	většinový
pro	pro	k7c4	pro
Senát	senát	k1gInSc4	senát
<g/>
.	.	kIx.	.
</s>
<s>
Výsledky	výsledek	k1gInPc4	výsledek
hlasování	hlasování	k1gNnSc2	hlasování
oznámil	oznámit	k5eAaPmAgMnS	oznámit
předseda	předseda	k1gMnSc1	předseda
České	český	k2eAgFnSc2d1	Česká
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
Milan	Milan	k1gMnSc1	Milan
Uhde	Uhde	k1gInSc1	Uhde
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
...	...	k?	...
ze	z	k7c2	z
198	[number]	k4	198
přítomných	přítomný	k2eAgFnPc2d1	přítomná
poslankyň	poslankyně	k1gFnPc2	poslankyně
a	a	k8xC	a
poslanců	poslanec	k1gMnPc2	poslanec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
účastnili	účastnit	k5eAaImAgMnP	účastnit
hlasování	hlasování	k1gNnSc2	hlasování
<g/>
,	,	kIx,	,
16	[number]	k4	16
hlasovalo	hlasovat	k5eAaImAgNnS	hlasovat
proti	proti	k7c3	proti
<g/>
,	,	kIx,	,
10	[number]	k4	10
poslankyň	poslankyně	k1gFnPc2	poslankyně
a	a	k8xC	a
poslanců	poslanec	k1gMnPc2	poslanec
se	se	k3xPyFc4	se
zdrželo	zdržet	k5eAaPmAgNnS	zdržet
hlasování	hlasování	k1gNnSc1	hlasování
a	a	k8xC	a
172	[number]	k4	172
kladnými	kladný	k2eAgInPc7d1	kladný
hlasy	hlas	k1gInPc7	hlas
byla	být	k5eAaImAgFnS	být
Ústava	ústava	k1gFnSc1	ústava
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
přijata	přijat	k2eAgFnSc1d1	přijata
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
jednání	jednání	k1gNnSc2	jednání
se	se	k3xPyFc4	se
sešlo	sejít	k5eAaPmAgNnS	sejít
ke	k	k7c3	k
krátké	krátký	k2eAgFnSc3d1	krátká
schůzi	schůze	k1gFnSc3	schůze
předsednictvo	předsednictvo	k1gNnSc1	předsednictvo
České	český	k2eAgFnSc2d1	Česká
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
vyhlásilo	vyhlásit	k5eAaPmAgNnS	vyhlásit
Ústavu	ústav	k1gInSc3	ústav
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
ustanovení	ustanovení	k1gNnSc4	ustanovení
čl	čl	kA	čl
<g/>
.	.	kIx.	.
39	[number]	k4	39
odst	odsta	k1gFnPc2	odsta
<g/>
.	.	kIx.	.
4	[number]	k4	4
Ústavy	ústava	k1gFnSc2	ústava
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
něhož	jenž	k3xRgMnSc2	jenž
"	"	kIx"	"
<g/>
[	[	kIx(	[
<g/>
k	k	k7c3	k
<g/>
]	]	kIx)	]
přijetí	přijetí	k1gNnSc4	přijetí
ústavního	ústavní	k2eAgInSc2d1	ústavní
zákona	zákon	k1gInSc2	zákon
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
souhlasu	souhlas	k1gInSc2	souhlas
třípětinové	třípětinový	k2eAgFnSc2d1	třípětinová
většiny	většina	k1gFnSc2	většina
všech	všecek	k3xTgMnPc2	všecek
poslanců	poslanec	k1gMnPc2	poslanec
a	a	k8xC	a
třípětinové	třípětinový	k2eAgFnSc2d1	třípětinová
většiny	většina	k1gFnSc2	většina
přítomných	přítomný	k2eAgMnPc2d1	přítomný
senátorů	senátor	k1gMnPc2	senátor
<g/>
[	[	kIx(	[
<g/>
,	,	kIx,	,
<g/>
]	]	kIx)	]
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
zkonstatovat	zkonstatovat	k5eAaPmF	zkonstatovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
změnu	změna	k1gFnSc4	změna
Ústavy	ústava	k1gFnSc2	ústava
je	být	k5eAaImIp3nS	být
nutný	nutný	k2eAgInSc1d1	nutný
složitější	složitý	k2eAgInSc1d2	složitější
postup	postup	k1gInSc1	postup
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
typologie	typologie	k1gFnSc2	typologie
ústav	ústav	k1gInSc1	ústav
značí	značit	k5eAaImIp3nS	značit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c6	o
ústavu	ústav	k1gInSc6	ústav
rigidní	rigidní	k2eAgNnSc1d1	rigidní
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
historická	historický	k2eAgFnSc1d1	historická
zvyklost	zvyklost	k1gFnSc1	zvyklost
mít	mít	k5eAaImF	mít
ústavu	ústava	k1gFnSc4	ústava
rigidní	rigidní	k2eAgFnSc1d1	rigidní
<g/>
,	,	kIx,	,
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
přípravou	příprava	k1gFnSc7	příprava
Ústavy	ústava	k1gFnSc2	ústava
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
se	se	k3xPyFc4	se
objevovaly	objevovat	k5eAaImAgInP	objevovat
hlasy	hlas	k1gInPc1	hlas
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Ústava	ústava	k1gFnSc1	ústava
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
měněna	měnit	k5eAaImNgFnS	měnit
prostou	prostý	k2eAgFnSc7d1	prostá
–	–	k?	–
nikoliv	nikoliv	k9	nikoliv
kvalifikovanou	kvalifikovaný	k2eAgFnSc7d1	kvalifikovaná
–	–	k?	–
většinou	většina	k1gFnSc7	většina
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c6	o
ústavu	ústav	k1gInSc6	ústav
flexibilní	flexibilní	k2eAgNnSc1d1	flexibilní
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
Loewensteinovy	Loewensteinův	k2eAgFnPc4d1	Loewensteinův
ústavní	ústavní	k2eAgFnPc4d1	ústavní
ontologie	ontologie	k1gFnPc4	ontologie
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgFnPc4d1	možná
českou	český	k2eAgFnSc4d1	Česká
Ústavu	ústava	k1gFnSc4	ústava
charakterizovat	charakterizovat	k5eAaBmF	charakterizovat
jako	jako	k9	jako
ústavu	ústava	k1gFnSc4	ústava
normativní	normativní	k2eAgFnSc4d1	normativní
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
že	že	k8xS	že
politický	politický	k2eAgInSc1d1	politický
proces	proces	k1gInSc1	proces
probíhá	probíhat	k5eAaImIp3nS	probíhat
podle	podle	k7c2	podle
jejich	jejich	k3xOp3gNnPc2	jejich
pravidel	pravidlo	k1gNnPc2	pravidlo
<g/>
.	.	kIx.	.
</s>
<s>
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
míra	míra	k1gFnSc1	míra
souladu	soulad	k1gInSc2	soulad
mezi	mezi	k7c7	mezi
právní	právní	k2eAgFnSc7d1	právní
a	a	k8xC	a
faktickou	faktický	k2eAgFnSc7d1	faktická
ústavou	ústava	k1gFnSc7	ústava
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
česká	český	k2eAgFnSc1d1	Česká
Ústava	ústava	k1gFnSc1	ústava
je	být	k5eAaImIp3nS	být
také	také	k9	také
ústavou	ústava	k1gFnSc7	ústava
reálnou	reálný	k2eAgFnSc7d1	reálná
<g/>
,	,	kIx,	,
ústava	ústava	k1gFnSc1	ústava
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
kryje	krýt	k5eAaImIp3nS	krýt
se	s	k7c7	s
skutečností	skutečnost	k1gFnSc7	skutečnost
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
ústava	ústava	k1gFnSc1	ústava
je	být	k5eAaImIp3nS	být
psanou	psaný	k2eAgFnSc7d1	psaná
ústavou	ústava	k1gFnSc7	ústava
<g/>
,	,	kIx,	,
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
písemně	písemně	k6eAd1	písemně
zachycena	zachytit	k5eAaPmNgFnS	zachytit
v	v	k7c6	v
psaném	psaný	k2eAgInSc6d1	psaný
(	(	kIx(	(
<g/>
ústavním	ústavní	k2eAgInSc6d1	ústavní
<g/>
)	)	kIx)	)
zákoně	zákon	k1gInSc6	zákon
v	v	k7c6	v
promulgačním	promulgační	k2eAgInSc6d1	promulgační
listu	list	k1gInSc6	list
–	–	k?	–
Sbírce	sbírka	k1gFnSc6	sbírka
zákonů	zákon	k1gInPc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Ústava	ústava	k1gFnSc1	ústava
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
je	být	k5eAaImIp3nS	být
také	také	k9	také
ústavou	ústava	k1gFnSc7	ústava
původní	původní	k2eAgFnSc7d1	původní
a	a	k8xC	a
dohodnutou	dohodnutý	k2eAgFnSc7d1	dohodnutá
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
existovala	existovat	k5eAaImAgFnS	existovat
snaha	snaha	k1gFnSc1	snaha
přijmout	přijmout	k5eAaPmF	přijmout
ústavní	ústavní	k2eAgFnSc4d1	ústavní
listinu	listina	k1gFnSc4	listina
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
bychom	by	kYmCp1nP	by
hovořili	hovořit	k5eAaImAgMnP	hovořit
o	o	k7c6	o
přenesené	přenesený	k2eAgFnSc6d1	přenesená
ústavě	ústava	k1gFnSc6	ústava
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
tento	tento	k3xDgInSc1	tento
pokus	pokus	k1gInSc1	pokus
nebyl	být	k5eNaImAgInS	být
úspěšný	úspěšný	k2eAgMnSc1d1	úspěšný
a	a	k8xC	a
česká	český	k2eAgFnSc1d1	Česká
ústava	ústava	k1gFnSc1	ústava
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
je	být	k5eAaImIp3nS	být
tou	ten	k3xDgFnSc7	ten
prvorepublikovou	prvorepublikový	k2eAgFnSc7d1	prvorepubliková
silně	silně	k6eAd1	silně
inspirována	inspirovat	k5eAaBmNgNnP	inspirovat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ústavou	ústava	k1gFnSc7	ústava
původní	původní	k2eAgFnSc7d1	původní
<g/>
.	.	kIx.	.
</s>
<s>
Neboť	neboť	k8xC	neboť
byla	být	k5eAaImAgFnS	být
česká	český	k2eAgFnSc1d1	Česká
ústava	ústava	k1gFnSc1	ústava
výsledkem	výsledek	k1gInSc7	výsledek
politického	politický	k2eAgInSc2d1	politický
kompromisu	kompromis	k1gInSc2	kompromis
završeného	završený	k2eAgInSc2d1	završený
na	na	k7c6	na
půdě	půda	k1gFnSc6	půda
České	český	k2eAgFnSc2d1	Česká
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
16	[number]	k4	16
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
ústavu	ústava	k1gFnSc4	ústava
dohodnutou	dohodnutý	k2eAgFnSc4d1	dohodnutá
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Českou	český	k2eAgFnSc4d1	Česká
republiku	republika	k1gFnSc4	republika
a	a	k8xC	a
její	její	k3xOp3gNnSc4	její
ústavní	ústavní	k2eAgNnSc4d1	ústavní
právo	právo	k1gNnSc4	právo
je	být	k5eAaImIp3nS	být
typická	typický	k2eAgFnSc1d1	typická
výrazná	výrazný	k2eAgFnSc1d1	výrazná
stabilita	stabilita	k1gFnSc1	stabilita
a	a	k8xC	a
reálná	reálný	k2eAgFnSc1d1	reálná
neměnnost	neměnnost	k1gFnSc1	neměnnost
(	(	kIx(	(
<g/>
polylegální	polylegální	k2eAgFnSc2d1	polylegální
<g/>
)	)	kIx)	)
ústavy	ústava	k1gFnSc2	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
účinnosti	účinnost	k1gFnSc2	účinnost
Ústavy	ústava	k1gFnSc2	ústava
došlo	dojít	k5eAaPmAgNnS	dojít
jen	jen	k9	jen
k	k	k7c3	k
několika	několik	k4yIc3	několik
mála	málo	k4c2	málo
změnám	změna	k1gFnPc3	změna
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
však	však	k9	však
nikterak	nikterak	k6eAd1	nikterak
výrazně	výrazně	k6eAd1	výrazně
neovlivnily	ovlivnit	k5eNaPmAgFnP	ovlivnit
podobu	podoba	k1gFnSc4	podoba
českého	český	k2eAgInSc2d1	český
ústavního	ústavní	k2eAgInSc2d1	ústavní
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
zásah	zásah	k1gInSc1	zásah
do	do	k7c2	do
textu	text	k1gInSc2	text
Ústavy	ústava	k1gFnSc2	ústava
byl	být	k5eAaImAgInS	být
uskutečněn	uskutečnit	k5eAaPmNgInS	uskutečnit
ústavním	ústavní	k2eAgInSc7d1	ústavní
zákonem	zákon	k1gInSc7	zákon
č.	č.	k?	č.
347	[number]	k4	347
<g/>
/	/	kIx~	/
<g/>
1997	[number]	k4	1997
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
vytvoření	vytvoření	k1gNnSc6	vytvoření
vyšších	vysoký	k2eAgInPc2d2	vyšší
územních	územní	k2eAgInPc2d1	územní
samosprávných	samosprávný	k2eAgInPc2d1	samosprávný
celků	celek	k1gInPc2	celek
a	a	k8xC	a
o	o	k7c6	o
změně	změna	k1gFnSc6	změna
ústavního	ústavní	k2eAgInSc2d1	ústavní
zákona	zákon	k1gInSc2	zákon
České	český	k2eAgFnSc2d1	Česká
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
č.	č.	k?	č.
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
1993	[number]	k4	1993
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Ústava	ústava	k1gFnSc1	ústava
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
naplňoval	naplňovat	k5eAaImAgMnS	naplňovat
čl	čl	kA	čl
<g/>
.	.	kIx.	.
100	[number]	k4	100
odst	odsta	k1gFnPc2	odsta
<g/>
.	.	kIx.	.
3	[number]	k4	3
Ústavy	ústava	k1gFnSc2	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
ústavním	ústavní	k2eAgInSc7d1	ústavní
zákonem	zákon	k1gInSc7	zákon
byly	být	k5eAaImAgFnP	být
vytvořeny	vytvořit	k5eAaPmNgInP	vytvořit
vyšší	vysoký	k2eAgInPc1d2	vyšší
územní	územní	k2eAgInPc1d1	územní
samosprávné	samosprávný	k2eAgInPc1d1	samosprávný
celky	celek	k1gInPc1	celek
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc4d1	další
změnu	změna	k1gFnSc4	změna
do	do	k7c2	do
textu	text	k1gInSc2	text
Ústavy	ústava	k1gFnSc2	ústava
přinesl	přinést	k5eAaPmAgInS	přinést
ústavní	ústavní	k2eAgInSc1d1	ústavní
zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
300	[number]	k4	300
<g/>
/	/	kIx~	/
<g/>
2000	[number]	k4	2000
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
se	s	k7c7	s
vstupem	vstup	k1gInSc7	vstup
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
do	do	k7c2	do
Severoatlantické	severoatlantický	k2eAgFnSc2d1	Severoatlantická
aliance	aliance	k1gFnSc2	aliance
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
který	který	k3yQgInSc1	který
upravoval	upravovat	k5eAaImAgInS	upravovat
otázky	otázka	k1gFnPc4	otázka
související	související	k2eAgFnPc4d1	související
s	s	k7c7	s
vysíláním	vysílání	k1gNnSc7	vysílání
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
pobytem	pobyt	k1gInSc7	pobyt
spojeneckých	spojenecký	k2eAgFnPc2d1	spojenecká
sil	síla	k1gFnPc2	síla
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
participací	participace	k1gFnSc7	participace
republiky	republika	k1gFnSc2	republika
na	na	k7c6	na
obranných	obranný	k2eAgInPc6d1	obranný
systémech	systém	k1gInPc6	systém
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
organizací	organizace	k1gFnPc2	organizace
a	a	k8xC	a
také	také	k9	také
rozdělení	rozdělení	k1gNnSc1	rozdělení
kompetencí	kompetence	k1gFnPc2	kompetence
mezi	mezi	k7c4	mezi
vládu	vláda	k1gFnSc4	vláda
a	a	k8xC	a
parlament	parlament	k1gInSc4	parlament
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
otázkách	otázka	k1gFnPc6	otázka
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
změna	změna	k1gFnSc1	změna
byla	být	k5eAaImAgFnS	být
provedena	provést	k5eAaPmNgFnS	provést
ústavním	ústavní	k2eAgInSc7d1	ústavní
zákonem	zákon	k1gInSc7	zákon
č.	č.	k?	č.
448	[number]	k4	448
<g/>
/	/	kIx~	/
<g/>
2001	[number]	k4	2001
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
Vláda	vláda	k1gFnSc1	vláda
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2000	[number]	k4	2000
připravila	připravit	k5eAaPmAgFnS	připravit
novelu	novela	k1gFnSc4	novela
Ústavy	ústava	k1gFnSc2	ústava
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
však	však	k9	však
Poslanecká	poslanecký	k2eAgFnSc1d1	Poslanecká
sněmovna	sněmovna	k1gFnSc1	sněmovna
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
čtení	čtení	k1gNnSc6	čtení
zamítla	zamítnout	k5eAaPmAgFnS	zamítnout
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
novela	novela	k1gFnSc1	novela
zahrnovala	zahrnovat	k5eAaImAgFnS	zahrnovat
také	také	k9	také
změnu	změna	k1gFnSc4	změna
v	v	k7c6	v
čl	čl	kA	čl
<g/>
.	.	kIx.	.
98	[number]	k4	98
Ústavy	ústava	k1gFnSc2	ústava
<g/>
,	,	kIx,	,
když	když	k8xS	když
tento	tento	k3xDgInSc1	tento
článek	článek	k1gInSc1	článek
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
uveden	uvést	k5eAaPmNgInS	uvést
do	do	k7c2	do
souladu	soulad	k1gInSc2	soulad
s	s	k7c7	s
novým	nový	k2eAgNnSc7d1	nové
zněním	znění	k1gNnSc7	znění
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
6	[number]	k4	6
<g/>
/	/	kIx~	/
<g/>
1993	[number]	k4	1993
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
České	český	k2eAgFnSc6d1	Česká
národní	národní	k2eAgFnSc6d1	národní
bance	banka	k1gFnSc6	banka
(	(	kIx(	(
<g/>
ČNB	ČNB	kA	ČNB
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Poslanecká	poslanecký	k2eAgFnSc1d1	Poslanecká
sněmovna	sněmovna	k1gFnSc1	sněmovna
novelu	novela	k1gFnSc4	novela
zamítla	zamítnout	k5eAaPmAgFnS	zamítnout
<g/>
,	,	kIx,	,
dostal	dostat	k5eAaPmAgInS	dostat
se	se	k3xPyFc4	se
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
ČNB	ČNB	kA	ČNB
do	do	k7c2	do
nesouladu	nesoulad	k1gInSc2	nesoulad
s	s	k7c7	s
textem	text	k1gInSc7	text
Ústavy	ústava	k1gFnSc2	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
dikce	dikce	k1gFnSc1	dikce
zákona	zákon	k1gInSc2	zákon
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
nutnost	nutnost	k1gFnSc4	nutnost
změny	změna	k1gFnSc2	změna
Ústavy	ústava	k1gFnSc2	ústava
<g/>
,	,	kIx,	,
souvisela	souviset	k5eAaImAgFnS	souviset
s	s	k7c7	s
požadavkem	požadavek	k1gInSc7	požadavek
Smlouvy	smlouva	k1gFnSc2	smlouva
o	o	k7c4	o
založení	založení	k1gNnSc4	založení
Evropského	evropský	k2eAgNnSc2d1	Evropské
společenství	společenství	k1gNnSc2	společenství
a	a	k8xC	a
Protokolu	protokol	k1gInSc2	protokol
o	o	k7c6	o
Statutu	statut	k1gInSc6	statut
Evropského	evropský	k2eAgInSc2d1	evropský
systému	systém	k1gInSc2	systém
centrálních	centrální	k2eAgFnPc2d1	centrální
bank	banka	k1gFnPc2	banka
a	a	k8xC	a
Evropské	evropský	k2eAgFnSc2d1	Evropská
centrální	centrální	k2eAgFnSc2d1	centrální
banky	banka	k1gFnSc2	banka
<g/>
,	,	kIx,	,
připojeného	připojený	k2eAgInSc2d1	připojený
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
smlouvě	smlouva	k1gFnSc3	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
tak	tak	k6eAd1	tak
připraven	připravit	k5eAaPmNgInS	připravit
nový	nový	k2eAgInSc1d1	nový
návrh	návrh	k1gInSc1	návrh
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
měl	mít	k5eAaImAgInS	mít
toliko	toliko	k6eAd1	toliko
legislativně	legislativně	k6eAd1	legislativně
technický	technický	k2eAgInSc4d1	technický
význam	význam	k1gInSc4	význam
a	a	k8xC	a
který	který	k3yIgInSc4	který
v	v	k7c6	v
článku	článek	k1gInSc6	článek
98	[number]	k4	98
nahradil	nahradit	k5eAaPmAgMnS	nahradit
"	"	kIx"	"
<g/>
stabilitu	stabilita	k1gFnSc4	stabilita
měny	měna	k1gFnSc2	měna
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
cenovou	cenový	k2eAgFnSc7d1	cenová
stabilitou	stabilita	k1gFnSc7	stabilita
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Významnou	významný	k2eAgFnSc7d1	významná
změnou	změna	k1gFnSc7	změna
byla	být	k5eAaImAgFnS	být
tzv.	tzv.	kA	tzv.
euronovela	euronovela	k1gFnSc1	euronovela
Ústavy	ústava	k1gFnSc2	ústava
z	z	k7c2	z
18	[number]	k4	18
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2001	[number]	k4	2001
(	(	kIx(	(
<g/>
č.	č.	k?	č.
395	[number]	k4	395
<g/>
/	/	kIx~	/
<g/>
2001	[number]	k4	2001
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Euronovela	Euronovela	k1gFnSc1	Euronovela
s	s	k7c7	s
účinností	účinnost	k1gFnSc7	účinnost
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2002	[number]	k4	2002
doplnila	doplnit	k5eAaPmAgFnS	doplnit
druhý	druhý	k4xOgInSc4	druhý
odstavec	odstavec	k1gInSc4	odstavec
do	do	k7c2	do
článku	článek	k1gInSc2	článek
1	[number]	k4	1
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
republika	republika	k1gFnSc1	republika
dodržuje	dodržovat	k5eAaImIp3nS	dodržovat
závazky	závazek	k1gInPc4	závazek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
vyplývají	vyplývat	k5eAaImIp3nP	vyplývat
z	z	k7c2	z
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
práva	právo	k1gNnSc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Článek	článek	k1gInSc1	článek
10	[number]	k4	10
<g/>
,	,	kIx,	,
upravující	upravující	k2eAgNnSc1d1	upravující
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
aplikační	aplikační	k2eAgFnSc4d1	aplikační
přednost	přednost	k1gFnSc4	přednost
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
smluv	smlouva	k1gFnPc2	smlouva
o	o	k7c6	o
základních	základní	k2eAgNnPc6d1	základní
právech	právo	k1gNnPc6	právo
a	a	k8xC	a
lidských	lidský	k2eAgFnPc6d1	lidská
svobodách	svoboda	k1gFnPc6	svoboda
<g/>
,	,	kIx,	,
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
okruh	okruh	k1gInSc4	okruh
aplikační	aplikační	k2eAgFnSc2d1	aplikační
přednosti	přednost	k1gFnSc2	přednost
na	na	k7c4	na
vyhlášené	vyhlášený	k2eAgFnPc4d1	vyhlášená
mezinárodní	mezinárodní	k2eAgFnPc4d1	mezinárodní
smlouvy	smlouva	k1gFnPc4	smlouva
<g/>
,	,	kIx,	,
k	k	k7c3	k
jejichž	jejichž	k3xOyRp3gFnSc3	jejichž
ratifikaci	ratifikace	k1gFnSc3	ratifikace
dal	dát	k5eAaPmAgInS	dát
Parlament	parlament	k1gInSc1	parlament
souhlas	souhlas	k1gInSc4	souhlas
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
článek	článek	k1gInSc4	článek
10	[number]	k4	10
byly	být	k5eAaImAgFnP	být
euronovelou	euronovela	k1gFnSc7	euronovela
doplněny	doplněn	k2eAgInPc1d1	doplněn
články	článek	k1gInPc1	článek
10	[number]	k4	10
<g/>
a	a	k8xC	a
a	a	k8xC	a
10	[number]	k4	10
<g/>
b	b	k?	b
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
upravují	upravovat	k5eAaImIp3nP	upravovat
podmínky	podmínka	k1gFnPc4	podmínka
přenesení	přenesení	k1gNnSc2	přenesení
pravomoci	pravomoc	k1gFnSc2	pravomoc
(	(	kIx(	(
<g/>
čl	čl	kA	čl
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
a	a	k8xC	a
<g/>
)	)	kIx)	)
na	na	k7c4	na
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
organizaci	organizace	k1gFnSc4	organizace
nebo	nebo	k8xC	nebo
instituci	instituce	k1gFnSc4	instituce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
otázkami	otázka	k1gFnPc7	otázka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
souvisejí	souviset	k5eAaImIp3nP	souviset
se	s	k7c7	s
závazky	závazek	k1gInPc7	závazek
vyplývajícími	vyplývající	k2eAgInPc7d1	vyplývající
z	z	k7c2	z
členství	členství	k1gNnSc2	členství
v	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
subjektu	subjekt	k1gInSc6	subjekt
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
založena	založen	k2eAgFnSc1d1	založena
povinnost	povinnost	k1gFnSc1	povinnost
vlády	vláda	k1gFnSc2	vláda
informovat	informovat	k5eAaBmF	informovat
Parlament	parlament	k1gInSc1	parlament
a	a	k8xC	a
právo	právo	k1gNnSc1	právo
komor	komora	k1gFnPc2	komora
Parlamentu	parlament	k1gInSc2	parlament
se	se	k3xPyFc4	se
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
(	(	kIx(	(
<g/>
čl	čl	kA	čl
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
b	b	k?	b
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Upravena	upraven	k2eAgFnSc1d1	upravena
byla	být	k5eAaImAgFnS	být
současně	současně	k6eAd1	současně
dikce	dikce	k1gFnSc1	dikce
čl	čl	kA	čl
<g/>
.	.	kIx.	.
39	[number]	k4	39
odst	odsta	k1gFnPc2	odsta
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
nově	nově	k6eAd1	nově
nehovořil	hovořit	k5eNaImAgMnS	hovořit
o	o	k7c6	o
schválení	schválení	k1gNnSc6	schválení
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
souhlasu	souhlas	k1gInSc2	souhlas
k	k	k7c3	k
ratifikaci	ratifikace	k1gFnSc3	ratifikace
<g/>
.	.	kIx.	.
</s>
<s>
Zcela	zcela	k6eAd1	zcela
změněn	změněn	k2eAgMnSc1d1	změněn
byl	být	k5eAaImAgInS	být
článek	článek	k1gInSc1	článek
49	[number]	k4	49
upravující	upravující	k2eAgFnSc4d1	upravující
ratifikaci	ratifikace	k1gFnSc4	ratifikace
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
smluv	smlouva	k1gFnPc2	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Změněn	změnit	k5eAaPmNgInS	změnit
byl	být	k5eAaImAgInS	být
článek	článek	k1gInSc1	článek
52	[number]	k4	52
<g/>
,	,	kIx,	,
když	když	k8xS	když
bylo	být	k5eAaImAgNnS	být
upřesněno	upřesnit	k5eAaPmNgNnS	upřesnit
<g/>
,	,	kIx,	,
že	že	k8xS	že
u	u	k7c2	u
zákonů	zákon	k1gInPc2	zákon
je	být	k5eAaImIp3nS	být
publikace	publikace	k1gFnSc1	publikace
v	v	k7c6	v
promulgačním	promulgační	k2eAgInSc6d1	promulgační
listu	list	k1gInSc6	list
(	(	kIx(	(
<g/>
Sbírce	sbírka	k1gFnSc6	sbírka
zákonů	zákon	k1gInPc2	zákon
<g/>
)	)	kIx)	)
podmínkou	podmínka	k1gFnSc7	podmínka
platnosti	platnost	k1gFnSc2	platnost
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
u	u	k7c2	u
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
smluv	smlouva	k1gFnPc2	smlouva
podmínkou	podmínka	k1gFnSc7	podmínka
použitelnosti	použitelnost	k1gFnPc1	použitelnost
<g/>
.	.	kIx.	.
</s>
<s>
Upravena	upraven	k2eAgFnSc1d1	upravena
byla	být	k5eAaImAgFnS	být
formulace	formulace	k1gFnSc1	formulace
čl	čl	kA	čl
<g/>
.	.	kIx.	.
87	[number]	k4	87
vymezující	vymezující	k2eAgFnPc1d1	vymezující
kompetence	kompetence	k1gFnPc1	kompetence
Ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
změněn	změnit	k5eAaPmNgInS	změnit
okruh	okruh	k1gInSc1	okruh
aktů	akt	k1gInPc2	akt
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgInPc7	jenž
je	být	k5eAaImIp3nS	být
ústavní	ústavní	k2eAgMnPc1d1	ústavní
soudce	soudce	k1gMnSc1	soudce
vázán	vázat	k5eAaImNgMnS	vázat
(	(	kIx(	(
<g/>
čl	čl	kA	čl
<g/>
.	.	kIx.	.
88	[number]	k4	88
<g/>
)	)	kIx)	)
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
připojen	připojit	k5eAaPmNgInS	připojit
odstavec	odstavec	k1gInSc1	odstavec
3	[number]	k4	3
do	do	k7c2	do
článku	článek	k1gInSc2	článek
89	[number]	k4	89
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
upravuje	upravovat	k5eAaImIp3nS	upravovat
závaznost	závaznost	k1gFnSc4	závaznost
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
Ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
<g/>
.	.	kIx.	.
</s>
<s>
Změnu	změna	k1gFnSc4	změna
doznal	doznat	k5eAaPmAgInS	doznat
také	také	k9	také
článek	článek	k1gInSc1	článek
95	[number]	k4	95
upravující	upravující	k2eAgFnSc4d1	upravující
vázanost	vázanost	k1gFnSc4	vázanost
soudců	soudce	k1gMnPc2	soudce
(	(	kIx(	(
<g/>
obecných	obecný	k2eAgInPc2d1	obecný
soudů	soud	k1gInPc2	soud
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Odpor	odpor	k1gInSc1	odpor
části	část	k1gFnSc2	část
politické	politický	k2eAgFnSc2d1	politická
reprezentace	reprezentace	k1gFnSc2	reprezentace
k	k	k7c3	k
zavádění	zavádění	k1gNnSc3	zavádění
institutů	institut	k1gInPc2	institut
přímé	přímý	k2eAgFnSc2d1	přímá
demokracie	demokracie	k1gFnSc2	demokracie
<g/>
,	,	kIx,	,
a	a	k8xC	a
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
související	související	k2eAgFnSc1d1	související
obava	obava	k1gFnSc1	obava
z	z	k7c2	z
obecné	obecný	k2eAgFnSc2d1	obecná
formulace	formulace	k1gFnSc2	formulace
referenda	referendum	k1gNnSc2	referendum
<g/>
,	,	kIx,	,
způsobila	způsobit	k5eAaPmAgFnS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
referendum	referendum	k1gNnSc4	referendum
o	o	k7c6	o
přistoupení	přistoupení	k1gNnSc6	přistoupení
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
k	k	k7c3	k
Evropské	evropský	k2eAgFnSc3d1	Evropská
unii	unie	k1gFnSc3	unie
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
ústavní	ústavní	k2eAgInSc1d1	ústavní
zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
515	[number]	k4	515
<g/>
/	/	kIx~	/
<g/>
2002	[number]	k4	2002
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
do	do	k7c2	do
textu	text	k1gInSc2	text
Ústavy	ústava	k1gFnSc2	ústava
zakomponoval	zakomponovat	k5eAaPmAgInS	zakomponovat
jednorázovou	jednorázový	k2eAgFnSc4d1	jednorázová
úpravu	úprava	k1gFnSc4	úprava
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
tomuto	tento	k3xDgNnSc3	tento
referendu	referendum	k1gNnSc3	referendum
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
referendu	referendum	k1gNnSc6	referendum
se	se	k3xPyFc4	se
tato	tento	k3xDgNnPc1	tento
ustanovení	ustanovení	k1gNnPc1	ustanovení
Ústavy	ústava	k1gFnSc2	ústava
stala	stát	k5eAaPmAgFnS	stát
obsahově	obsahově	k6eAd1	obsahově
obsoletními	obsoletní	k2eAgInPc7d1	obsoletní
a	a	k8xC	a
novelou	novela	k1gFnSc7	novela
č.	č.	k?	č.
71	[number]	k4	71
<g/>
/	/	kIx~	/
<g/>
2012	[number]	k4	2012
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
se	se	k3xPyFc4	se
zavedla	zavést	k5eAaPmAgFnS	zavést
přímá	přímý	k2eAgFnSc1d1	přímá
volba	volba	k1gFnSc1	volba
prezidenta	prezident	k1gMnSc2	prezident
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
z	z	k7c2	z
Ústavy	ústava	k1gFnSc2	ústava
vypuštěna	vypustit	k5eAaPmNgFnS	vypustit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
byla	být	k5eAaImAgFnS	být
přijata	přijmout	k5eAaPmNgFnS	přijmout
další	další	k2eAgFnSc1d1	další
novela	novela	k1gFnSc1	novela
Ústavy	ústava	k1gFnSc2	ústava
(	(	kIx(	(
<g/>
ústavní	ústavní	k2eAgInSc1d1	ústavní
zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
319	[number]	k4	319
<g/>
/	/	kIx~	/
<g/>
2009	[number]	k4	2009
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
připravená	připravený	k2eAgFnSc1d1	připravená
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
Stálou	stálý	k2eAgFnSc7d1	stálá
komisí	komise	k1gFnSc7	komise
Senátu	senát	k1gInSc2	senát
pro	pro	k7c4	pro
Ústavu	ústava	k1gFnSc4	ústava
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
parlamentní	parlamentní	k2eAgFnSc2d1	parlamentní
procedury	procedura	k1gFnSc2	procedura
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
byla	být	k5eAaImAgFnS	být
zavedena	zaveden	k2eAgFnSc1d1	zavedena
možnost	možnost	k1gFnSc1	možnost
samorozpuštění	samorozpuštění	k1gNnSc2	samorozpuštění
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byla	být	k5eAaImAgFnS	být
reakce	reakce	k1gFnSc1	reakce
na	na	k7c4	na
předchozí	předchozí	k2eAgNnSc4d1	předchozí
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
Ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnPc3	který
byl	být	k5eAaImAgInS	být
zrušen	zrušit	k5eAaPmNgInS	zrušit
ústavní	ústavní	k2eAgInSc1d1	ústavní
zákon	zákon	k1gInSc1	zákon
o	o	k7c4	o
zkrácení	zkrácení	k1gNnSc4	zkrácení
pátého	pátý	k4xOgNnSc2	pátý
volebního	volební	k2eAgNnSc2d1	volební
období	období	k1gNnSc2	období
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
byla	být	k5eAaImAgFnS	být
odstraněna	odstranit	k5eAaPmNgFnS	odstranit
zásadní	zásadní	k2eAgFnSc1d1	zásadní
vada	vada	k1gFnSc1	vada
v	v	k7c6	v
Ústavě	ústava	k1gFnSc6	ústava
pocházející	pocházející	k2eAgFnSc6d1	pocházející
z	z	k7c2	z
redakčního	redakční	k2eAgNnSc2d1	redakční
pochybení	pochybení	k1gNnSc2	pochybení
při	při	k7c6	při
její	její	k3xOp3gFnSc6	její
přípravě	příprava	k1gFnSc6	příprava
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Ústava	ústava	k1gFnSc1	ústava
totiž	totiž	k9	totiž
pominula	pominout	k5eAaPmAgFnS	pominout
přechod	přechod	k1gInSc4	přechod
pravomoci	pravomoc	k1gFnSc2	pravomoc
vyhlašovat	vyhlašovat	k5eAaImF	vyhlašovat
volby	volba	k1gFnPc4	volba
do	do	k7c2	do
Sněmovny	sněmovna	k1gFnSc2	sněmovna
a	a	k8xC	a
Senátu	senát	k1gInSc2	senát
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
prezident	prezident	k1gMnSc1	prezident
nemůže	moct	k5eNaImIp3nS	moct
funkci	funkce	k1gFnSc4	funkce
vykonávat	vykonávat	k5eAaImF	vykonávat
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
mezera	mezera	k1gFnSc1	mezera
tak	tak	k6eAd1	tak
mohla	moct	k5eAaImAgFnS	moct
způsobit	způsobit	k5eAaPmF	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
by	by	kYmCp3nP	by
mezi	mezi	k7c7	mezi
rozpuštěním	rozpuštění	k1gNnSc7	rozpuštění
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
a	a	k8xC	a
vyhlášením	vyhlášení	k1gNnSc7	vyhlášení
voleb	volba	k1gFnPc2	volba
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
nastala	nastat	k5eAaPmAgFnS	nastat
situace	situace	k1gFnSc1	situace
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
prezident	prezident	k1gMnSc1	prezident
nemohl	moct	k5eNaImAgMnS	moct
úřad	úřad	k1gInSc4	úřad
vykonávat	vykonávat	k5eAaImF	vykonávat
<g/>
,	,	kIx,	,
nemohly	moct	k5eNaImAgFnP	moct
by	by	kYmCp3nP	by
být	být	k5eAaImF	být
volby	volba	k1gFnSc2	volba
vůbec	vůbec	k9	vůbec
vyhlášeny	vyhlášen	k2eAgInPc1d1	vyhlášen
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
přechod	přechod	k1gInSc1	přechod
této	tento	k3xDgFnSc2	tento
pravomoci	pravomoc	k1gFnSc2	pravomoc
Ústava	ústava	k1gFnSc1	ústava
neřešila	řešit	k5eNaImAgFnS	řešit
a	a	k8xC	a
případná	případný	k2eAgFnSc1d1	případná
změna	změna	k1gFnSc1	změna
Ústavy	ústava	k1gFnSc2	ústava
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
k	k	k7c3	k
níž	jenž	k3xRgFnSc3	jenž
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
třeba	třeba	k6eAd1	třeba
ústavního	ústavní	k2eAgInSc2d1	ústavní
zákona	zákon	k1gInSc2	zákon
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
nebyla	být	k5eNaImAgFnS	být
možná	možný	k2eAgFnSc1d1	možná
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
sám	sám	k3xTgInSc1	sám
Senát	senát	k1gInSc1	senát
takový	takový	k3xDgInSc4	takový
předpis	předpis	k1gInSc4	předpis
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
Sněmovna	sněmovna	k1gFnSc1	sněmovna
rozpuštěna	rozpuštěn	k2eAgFnSc1d1	rozpuštěna
<g/>
,	,	kIx,	,
přijmout	přijmout	k5eAaPmF	přijmout
nemohl	moct	k5eNaImAgInS	moct
a	a	k8xC	a
nemůže	moct	k5eNaImIp3nS	moct
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
dikci	dikce	k1gFnSc4	dikce
čl	čl	kA	čl
<g/>
.	.	kIx.	.
33	[number]	k4	33
odst	odsta	k1gFnPc2	odsta
<g/>
.	.	kIx.	.
2	[number]	k4	2
Ústavy	ústava	k1gFnSc2	ústava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2011	[number]	k4	2011
předložila	předložit	k5eAaPmAgFnS	předložit
vláda	vláda	k1gFnSc1	vláda
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovně	sněmovna	k1gFnSc3	sněmovna
návrh	návrh	k1gInSc4	návrh
ústavního	ústavní	k2eAgInSc2d1	ústavní
zákona	zákon	k1gInSc2	zákon
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
počítal	počítat	k5eAaImAgInS	počítat
se	s	k7c7	s
zavedením	zavedení	k1gNnSc7	zavedení
přímé	přímý	k2eAgFnSc2d1	přímá
volby	volba	k1gFnSc2	volba
prezidenta	prezident	k1gMnSc2	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Návrh	návrh	k1gInSc1	návrh
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Poslanecké	poslanecký	k2eAgFnSc6d1	Poslanecká
sněmovně	sněmovna	k1gFnSc6	sněmovna
přijat	přijmout	k5eAaPmNgInS	přijmout
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
v	v	k7c6	v
Senátu	senát	k1gInSc6	senát
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
ústavního	ústavní	k2eAgInSc2d1	ústavní
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
71	[number]	k4	71
<g/>
/	/	kIx~	/
<g/>
2012	[number]	k4	2012
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
má	mít	k5eAaImIp3nS	mít
stanovenou	stanovený	k2eAgFnSc4d1	stanovená
účinnost	účinnost	k1gFnSc4	účinnost
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
zbývající	zbývající	k2eAgNnSc4d1	zbývající
ustanovení	ustanovení	k1gNnSc4	ustanovení
od	od	k7c2	od
8	[number]	k4	8
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Obecné	obecný	k2eAgNnSc1d1	obecné
datum	datum	k1gNnSc1	datum
nabytí	nabytí	k1gNnSc1	nabytí
účinnosti	účinnost	k1gFnSc2	účinnost
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
říjen	říjen	k1gInSc1	říjen
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
stanoveno	stanovit	k5eAaPmNgNnS	stanovit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
dodržení	dodržení	k1gNnSc4	dodržení
termínů	termín	k1gInPc2	termín
pro	pro	k7c4	pro
vyhlášení	vyhlášení	k1gNnSc4	vyhlášení
a	a	k8xC	a
konání	konání	k1gNnSc4	konání
volby	volba	k1gFnSc2	volba
prezidenta	prezident	k1gMnSc4	prezident
přímou	přímý	k2eAgFnSc7d1	přímá
volbou	volba	k1gFnSc7	volba
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomuto	tento	k3xDgNnSc3	tento
datu	datum	k1gNnSc3	datum
by	by	kYmCp3nS	by
pak	pak	k6eAd1	pak
měl	mít	k5eAaImAgMnS	mít
nabýt	nabýt	k5eAaPmF	nabýt
účinnosti	účinnost	k1gFnSc2	účinnost
i	i	k9	i
volební	volební	k2eAgInSc1d1	volební
zákon	zákon	k1gInSc1	zákon
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
má	mít	k5eAaImIp3nS	mít
podrobně	podrobně	k6eAd1	podrobně
upravovat	upravovat	k5eAaImF	upravovat
přímou	přímý	k2eAgFnSc4d1	přímá
volbu	volba	k1gFnSc4	volba
prezidenta	prezident	k1gMnSc2	prezident
<g/>
,	,	kIx,	,
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
novely	novela	k1gFnPc4	novela
zákonů	zákon	k1gInPc2	zákon
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
se	s	k7c7	s
zavedením	zavedení	k1gNnSc7	zavedení
přímé	přímý	k2eAgFnSc2d1	přímá
volby	volba	k1gFnSc2	volba
<g/>
.	.	kIx.	.
</s>
<s>
Ustanovení	ustanovení	k1gNnSc1	ustanovení
s	s	k7c7	s
odloženou	odložený	k2eAgFnSc7d1	odložená
účinností	účinnost	k1gFnSc7	účinnost
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
březen	březen	k1gInSc1	březen
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
týkají	týkat	k5eAaImIp3nP	týkat
odpovědnosti	odpovědnost	k1gFnSc2	odpovědnost
prezidenta	prezident	k1gMnSc2	prezident
za	za	k7c4	za
velezradu	velezrada	k1gFnSc4	velezrada
a	a	k8xC	a
podmínek	podmínka	k1gFnPc2	podmínka
pro	pro	k7c4	pro
podání	podání	k1gNnSc4	podání
ústavní	ústavní	k2eAgFnSc2d1	ústavní
žaloby	žaloba	k1gFnSc2	žaloba
Senátu	senát	k1gInSc2	senát
proti	proti	k7c3	proti
prezidentovi	prezident	k1gMnSc3	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
datum	datum	k1gNnSc1	datum
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
konec	konec	k1gInSc4	konec
volebního	volební	k2eAgNnSc2d1	volební
období	období	k1gNnSc2	období
dosavadního	dosavadní	k2eAgMnSc2d1	dosavadní
prezidenta	prezident	k1gMnSc2	prezident
Václava	Václav	k1gMnSc2	Václav
Klause	Klaus	k1gMnSc2	Klaus
dne	den	k1gInSc2	den
7	[number]	k4	7
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
účinnosti	účinnost	k1gFnSc2	účinnost
ústavního	ústavní	k2eAgInSc2d1	ústavní
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
71	[number]	k4	71
<g/>
/	/	kIx~	/
<g/>
2012	[number]	k4	2012
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
byl	být	k5eAaImAgMnS	být
prezident	prezident	k1gMnSc1	prezident
republiky	republika	k1gFnSc2	republika
volen	volno	k1gNnPc2	volno
Parlamentem	parlament	k1gInSc7	parlament
na	na	k7c6	na
společné	společný	k2eAgFnSc6d1	společná
schůzi	schůze	k1gFnSc6	schůze
obou	dva	k4xCgFnPc2	dva
komor	komora	k1gFnPc2	komora
(	(	kIx(	(
<g/>
čl	čl	kA	čl
<g/>
.	.	kIx.	.
54	[number]	k4	54
odst	odsta	k1gFnPc2	odsta
<g/>
.	.	kIx.	.
2	[number]	k4	2
v	v	k7c6	v
původním	původní	k2eAgNnSc6d1	původní
znění	znění	k1gNnSc6	znění
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Volba	volba	k1gFnSc1	volba
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
v	v	k7c6	v
posledních	poslední	k2eAgInPc6d1	poslední
třiceti	třicet	k4xCc6	třicet
dnech	den	k1gInPc6	den
volebního	volební	k2eAgNnSc2d1	volební
období	období	k1gNnSc2	období
úřadujícího	úřadující	k2eAgMnSc2d1	úřadující
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
úřad	úřad	k1gInSc1	úřad
uvolnil	uvolnit	k5eAaPmAgInS	uvolnit
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
se	se	k3xPyFc4	se
volba	volba	k1gFnSc1	volba
konat	konat	k5eAaImF	konat
do	do	k7c2	do
třiceti	třicet	k4xCc2	třicet
dnů	den	k1gInPc2	den
(	(	kIx(	(
<g/>
čl	čl	kA	čl
<g/>
.	.	kIx.	.
56	[number]	k4	56
v	v	k7c6	v
původním	původní	k2eAgNnSc6d1	původní
znění	znění	k1gNnSc6	znění
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Způsob	způsob	k1gInSc1	způsob
a	a	k8xC	a
průběh	průběh	k1gInSc1	průběh
volby	volba	k1gFnSc2	volba
prezidenta	prezident	k1gMnSc2	prezident
byl	být	k5eAaImAgMnS	být
upraven	upravit	k5eAaPmNgMnS	upravit
v	v	k7c6	v
článku	článek	k1gInSc6	článek
58	[number]	k4	58
v	v	k7c6	v
původním	původní	k2eAgNnSc6d1	původní
znění	znění	k1gNnSc6	znění
Ústavy	ústava	k1gFnSc2	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
skládal	skládat	k5eAaImAgMnS	skládat
prezident	prezident	k1gMnSc1	prezident
slib	slib	k1gInSc4	slib
a	a	k8xC	a
vzdával	vzdávat	k5eAaImAgMnS	vzdávat
se	se	k3xPyFc4	se
svého	svůj	k3xOyFgInSc2	svůj
úřadu	úřad	k1gInSc2	úřad
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
předsedy	předseda	k1gMnSc2	předseda
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
(	(	kIx(	(
<g/>
čl	čl	kA	čl
<g/>
.	.	kIx.	.
59	[number]	k4	59
odst	odsta	k1gFnPc2	odsta
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
;	;	kIx,	;
čl	čl	kA	čl
<g/>
.	.	kIx.	.
61	[number]	k4	61
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nově	nově	k6eAd1	nově
půjde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
předsedu	předseda	k1gMnSc4	předseda
Senátu	senát	k1gInSc2	senát
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
došlo	dojít	k5eAaPmAgNnS	dojít
i	i	k9	i
v	v	k7c6	v
otázce	otázka	k1gFnSc6	otázka
pravomocí	pravomoc	k1gFnPc2	pravomoc
<g/>
:	:	kIx,	:
k	k	k7c3	k
nařízení	nařízení	k1gNnSc3	nařízení
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
trestní	trestní	k2eAgNnSc1d1	trestní
řízení	řízení	k1gNnSc1	řízení
nezahajovalo	zahajovat	k5eNaImAgNnS	zahajovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
<g/>
-li	i	k?	-li
zahájeno	zahájit	k5eAaPmNgNnS	zahájit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
nepokračovalo	pokračovat	k5eNaImAgNnS	pokračovat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nově	nově	k6eAd1	nově
vyžadována	vyžadován	k2eAgFnSc1d1	vyžadována
součinnost	součinnost	k1gFnSc1	součinnost
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
vypadla	vypadnout	k5eAaPmAgFnS	vypadnout
jednorázová	jednorázový	k2eAgFnSc1d1	jednorázová
privilegovaná	privilegovaný	k2eAgFnSc1d1	privilegovaná
pravomoc	pravomoc	k1gFnSc1	pravomoc
vyhlásit	vyhlásit	k5eAaPmF	vyhlásit
referendum	referendum	k1gNnSc4	referendum
o	o	k7c4	o
přistoupení	přistoupení	k1gNnSc4	přistoupení
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
k	k	k7c3	k
Evropské	evropský	k2eAgFnSc3d1	Evropská
unii	unie	k1gFnSc3	unie
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc4	jeho
výsledek	výsledek	k1gInSc4	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
poslední	poslední	k2eAgFnSc4d1	poslední
změnu	změna	k1gFnSc4	změna
Ústavy	ústava	k1gFnSc2	ústava
znamenal	znamenat	k5eAaImAgInS	znamenat
ústavní	ústavní	k2eAgInSc1d1	ústavní
zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
98	[number]	k4	98
<g/>
/	/	kIx~	/
<g/>
2013	[number]	k4	2013
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
nabyl	nabýt	k5eAaPmAgInS	nabýt
účinnosti	účinnost	k1gFnSc2	účinnost
1	[number]	k4	1
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Jím	jíst	k5eAaImIp1nS	jíst
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
několika	několik	k4yIc6	několik
letech	léto	k1gNnPc6	léto
diskuzí	diskuze	k1gFnPc2	diskuze
omezena	omezen	k2eAgFnSc1d1	omezena
imunita	imunita	k1gFnSc1	imunita
poslanců	poslanec	k1gMnPc2	poslanec
<g/>
,	,	kIx,	,
senátorů	senátor	k1gMnPc2	senátor
a	a	k8xC	a
soudců	soudce	k1gMnPc2	soudce
Ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
jen	jen	k9	jen
na	na	k7c4	na
dobu	doba	k1gFnSc4	doba
jejich	jejich	k3xOp3gInSc2	jejich
mandátu	mandát	k1gInSc2	mandát
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
vládní	vládní	k2eAgInSc1d1	vládní
návrh	návrh	k1gInSc1	návrh
počítal	počítat	k5eAaImAgInS	počítat
se	s	k7c7	s
zcela	zcela	k6eAd1	zcela
odlišnou	odlišný	k2eAgFnSc7d1	odlišná
formulací	formulace	k1gFnSc7	formulace
preambule	preambule	k1gFnSc2	preambule
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
však	však	k9	však
byla	být	k5eAaImAgFnS	být
přijata	přijat	k2eAgFnSc1d1	přijata
formulace	formulace	k1gFnSc1	formulace
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
hlavním	hlavní	k2eAgMnSc7d1	hlavní
autorem	autor	k1gMnSc7	autor
byl	být	k5eAaImAgMnS	být
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
a	a	k8xC	a
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
závěrečným	závěrečný	k2eAgInSc7d1	závěrečný
editorem	editor	k1gInSc7	editor
předseda	předseda	k1gMnSc1	předseda
České	český	k2eAgFnSc2d1	Česká
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
Milan	Milan	k1gMnSc1	Milan
Uhde	Uhde	k1gFnSc1	Uhde
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
preambule	preambule	k1gFnSc2	preambule
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
vysledovat	vysledovat	k5eAaPmF	vysledovat
dvě	dva	k4xCgFnPc1	dva
části	část	k1gFnPc1	část
<g/>
:	:	kIx,	:
rekapitulační	rekapitulační	k2eAgFnPc1d1	rekapitulační
a	a	k8xC	a
programově-manifestační	programověanifestační	k2eAgFnPc1d1	programově-manifestační
<g/>
.	.	kIx.	.
</s>
<s>
Rekapitulační	rekapitulační	k2eAgFnSc1d1	rekapitulační
část	část	k1gFnSc1	část
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
historické	historický	k2eAgFnPc4d1	historická
tradice	tradice	k1gFnPc4	tradice
a	a	k8xC	a
státoprávní	státoprávní	k2eAgInSc4d1	státoprávní
vývoj	vývoj	k1gInSc4	vývoj
na	na	k7c6	na
českém	český	k2eAgNnSc6d1	české
území	území	k1gNnSc6	území
<g/>
,	,	kIx,	,
programově-manifestační	programověanifestační	k2eAgFnSc1d1	programově-manifestační
část	část	k1gFnSc1	část
pak	pak	k6eAd1	pak
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
deklaraci	deklarace	k1gFnSc4	deklarace
základních	základní	k2eAgInPc2d1	základní
státních	státní	k2eAgInPc2d1	státní
cílů	cíl	k1gInPc2	cíl
a	a	k8xC	a
hodnot	hodnota	k1gFnPc2	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
formulace	formulace	k1gFnSc2	formulace
preambule	preambule	k1gFnSc2	preambule
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
odvodit	odvodit	k5eAaPmF	odvodit
republikánský	republikánský	k2eAgInSc4d1	republikánský
charakter	charakter	k1gInSc4	charakter
státu	stát	k1gInSc2	stát
a	a	k8xC	a
z	z	k7c2	z
absence	absence	k1gFnSc2	absence
odkazu	odkaz	k1gInSc2	odkaz
na	na	k7c4	na
náboženství	náboženství	k1gNnSc4	náboženství
či	či	k8xC	či
víru	víra	k1gFnSc4	víra
je	být	k5eAaImIp3nS	být
patrná	patrný	k2eAgFnSc1d1	patrná
také	také	k9	také
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
je	být	k5eAaImIp3nS	být
laickým	laický	k2eAgInSc7d1	laický
státem	stát	k1gInSc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Provolání	provolání	k1gNnSc1	provolání
označuje	označovat	k5eAaImIp3nS	označovat
za	za	k7c4	za
příjemce	příjemce	k1gMnPc4	příjemce
Ústavy	ústava	k1gFnSc2	ústava
občany	občan	k1gMnPc4	občan
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
,	,	kIx,	,
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
a	a	k8xC	a
ve	v	k7c6	v
Slezsku	Slezsko	k1gNnSc6	Slezsko
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
nikoli	nikoli	k9	nikoli
národ	národ	k1gInSc1	národ
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
tomu	ten	k3xDgNnSc3	ten
činila	činit	k5eAaImAgFnS	činit
Ústavní	ústavní	k2eAgFnSc1d1	ústavní
listina	listina	k1gFnSc1	listina
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
Václavu	Václav	k1gMnSc3	Václav
Havlovi	Havel	k1gMnSc3	Havel
se	se	k3xPyFc4	se
v	v	k7c6	v
preambuli	preambule	k1gFnSc6	preambule
objevuje	objevovat	k5eAaImIp3nS	objevovat
pojem	pojem	k1gInSc1	pojem
"	"	kIx"	"
<g/>
občanská	občanský	k2eAgFnSc1d1	občanská
společnost	společnost	k1gFnSc1	společnost
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
jinde	jinde	k6eAd1	jinde
v	v	k7c6	v
Ústavě	ústava	k1gFnSc6	ústava
či	či	k8xC	či
v	v	k7c6	v
Listině	listina	k1gFnSc6	listina
přítomen	přítomno	k1gNnPc2	přítomno
není	být	k5eNaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Ustanovení	ustanovení	k1gNnSc1	ustanovení
článku	článek	k1gInSc2	článek
1	[number]	k4	1
Ústavy	ústav	k1gInPc4	ústav
zakotvuje	zakotvovat	k5eAaImIp3nS	zakotvovat
základní	základní	k2eAgInPc4d1	základní
principy	princip	k1gInPc4	princip
celého	celý	k2eAgInSc2d1	celý
ústavního	ústavní	k2eAgInSc2d1	ústavní
systému	systém	k1gInSc2	systém
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
odstavec	odstavec	k1gInSc1	odstavec
byl	být	k5eAaImAgInS	být
ke	k	k7c3	k
článku	článek	k1gInSc3	článek
připojen	připojit	k5eAaPmNgInS	připojit
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
tzv.	tzv.	kA	tzv.
euronovely	euronovet	k5eAaPmAgInP	euronovet
Ústavy	ústav	k1gInPc1	ústav
<g/>
.	.	kIx.	.
</s>
<s>
Článek	článek	k1gInSc1	článek
jedna	jeden	k4xCgFnSc1	jeden
také	také	k9	také
jako	jako	k8xC	jako
jediný	jediný	k2eAgInSc1d1	jediný
článek	článek	k1gInSc1	článek
Ústavy	ústava	k1gFnSc2	ústava
vymezuje	vymezovat	k5eAaImIp3nS	vymezovat
název	název	k1gInSc4	název
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
ustanovení	ustanovení	k1gNnSc2	ustanovení
také	také	k9	také
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
stát	stát	k1gInSc1	stát
je	být	k5eAaImIp3nS	být
republikou	republika	k1gFnSc7	republika
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
toto	tento	k3xDgNnSc1	tento
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
vyvodit	vyvodit	k5eAaPmF	vyvodit
již	již	k6eAd1	již
z	z	k7c2	z
preambule	preambule	k1gFnSc2	preambule
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
i	i	k9	i
dalších	další	k2eAgNnPc2d1	další
ustanovení	ustanovení	k1gNnPc2	ustanovení
Ústavy	ústava	k1gFnSc2	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Republika	republika	k1gFnSc1	republika
je	být	k5eAaImIp3nS	být
poté	poté	k6eAd1	poté
vymezena	vymezit	k5eAaPmNgFnS	vymezit
jako	jako	k8xC	jako
svrchovaný	svrchovaný	k2eAgMnSc1d1	svrchovaný
<g/>
,	,	kIx,	,
jednotný	jednotný	k2eAgInSc1d1	jednotný
a	a	k8xC	a
demokratický	demokratický	k2eAgInSc1d1	demokratický
právní	právní	k2eAgInSc1d1	právní
stát	stát	k1gInSc1	stát
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
je	být	k5eAaImIp3nS	být
založený	založený	k2eAgInSc1d1	založený
na	na	k7c6	na
úctě	úcta	k1gFnSc6	úcta
k	k	k7c3	k
právům	právo	k1gNnPc3	právo
a	a	k8xC	a
svobodám	svoboda	k1gFnPc3	svoboda
člověka	člověk	k1gMnSc4	člověk
a	a	k8xC	a
občana	občan	k1gMnSc4	občan
<g/>
.	.	kIx.	.
</s>
<s>
Svrchovanost	svrchovanost	k1gFnSc1	svrchovanost
značí	značit	k5eAaImIp3nS	značit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
je	být	k5eAaImIp3nS	být
plně	plně	k6eAd1	plně
způsobilá	způsobilý	k2eAgFnSc1d1	způsobilá
k	k	k7c3	k
právům	právo	k1gNnPc3	právo
a	a	k8xC	a
právním	právní	k2eAgInPc3d1	právní
úkonům	úkon	k1gInPc3	úkon
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
plnoprávným	plnoprávný	k2eAgInSc7d1	plnoprávný
subjektem	subjekt	k1gInSc7	subjekt
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
práva	právo	k1gNnSc2	právo
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
nezávislá	závislý	k2eNgFnSc1d1	nezávislá
na	na	k7c6	na
jiné	jiný	k2eAgFnSc6d1	jiná
moci	moc	k1gFnSc6	moc
<g/>
.	.	kIx.	.
</s>
<s>
Svrchovanost	svrchovanost	k1gFnSc1	svrchovanost
však	však	k9	však
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
dobrovolně	dobrovolně	k6eAd1	dobrovolně
omezena	omezit	k5eAaPmNgFnS	omezit
účastí	účast	k1gFnSc7	účast
na	na	k7c4	na
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
organizaci	organizace	k1gFnSc4	organizace
–	–	k?	–
v	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
koncept	koncept	k1gInSc4	koncept
tzv.	tzv.	kA	tzv.
sdílené	sdílený	k2eAgFnSc2d1	sdílená
či	či	k8xC	či
slité	slitý	k2eAgFnSc2d1	slitá
suverenity	suverenita	k1gFnSc2	suverenita
–	–	k?	–
a	a	k8xC	a
konkrétně	konkrétně	k6eAd1	konkrétně
v	v	k7c6	v
případě	případ	k1gInSc6	případ
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
její	její	k3xOp3gNnSc4	její
členství	členství	k1gNnSc4	členství
v	v	k7c6	v
Evropské	evropský	k2eAgFnSc6d1	Evropská
unii	unie	k1gFnSc6	unie
<g/>
.	.	kIx.	.
</s>
<s>
Prohlášení	prohlášení	k1gNnSc1	prohlášení
<g/>
,	,	kIx,	,
že	že	k8xS	že
republika	republika	k1gFnSc1	republika
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
jednotný	jednotný	k2eAgInSc1d1	jednotný
<g/>
"	"	kIx"	"
stát	stát	k1gInSc1	stát
má	mít	k5eAaImIp3nS	mít
znamenat	znamenat	k5eAaImF	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nejedná	jednat	k5eNaImIp3nS	jednat
o	o	k7c4	o
jakoukoliv	jakýkoliv	k3yIgFnSc4	jakýkoliv
formu	forma	k1gFnSc4	forma
konfederace	konfederace	k1gFnSc2	konfederace
či	či	k8xC	či
federace	federace	k1gFnSc2	federace
<g/>
.	.	kIx.	.
</s>
<s>
Definice	definice	k1gFnSc1	definice
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
jako	jako	k8xS	jako
demokratického	demokratický	k2eAgInSc2d1	demokratický
právního	právní	k2eAgInSc2d1	právní
státu	stát	k1gInSc2	stát
zdůrazňuje	zdůrazňovat	k5eAaImIp3nS	zdůrazňovat
spojení	spojení	k1gNnSc4	spojení
těchto	tento	k3xDgFnPc2	tento
dvou	dva	k4xCgFnPc2	dva
zásad	zásada	k1gFnPc2	zásada
–	–	k?	–
demokratického	demokratický	k2eAgNnSc2d1	demokratické
a	a	k8xC	a
právně-státního	právnětátní	k2eAgNnSc2d1	právně-státní
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
změna	změna	k1gFnSc1	změna
jejich	jejich	k3xOp3gFnPc2	jejich
podstatných	podstatný	k2eAgFnPc2d1	podstatná
náležitostí	náležitost	k1gFnPc2	náležitost
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
dikce	dikce	k1gFnSc2	dikce
čl	čl	kA	čl
<g/>
.	.	kIx.	.
9	[number]	k4	9
Ústavy	ústava	k1gFnSc2	ústava
nepřípustná	přípustný	k2eNgFnSc1d1	nepřípustná
<g/>
.	.	kIx.	.
</s>
<s>
Pojem	pojem	k1gInSc1	pojem
právního	právní	k2eAgInSc2d1	právní
státu	stát	k1gInSc2	stát
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
–	–	k?	–
také	také	k9	také
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
judikaturu	judikatura	k1gFnSc4	judikatura
Ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
–	–	k?	–
vykládán	vykládán	k2eAgInSc1d1	vykládán
v	v	k7c6	v
materiálním	materiální	k2eAgInSc6d1	materiální
a	a	k8xC	a
nikoliv	nikoliv	k9	nikoliv
jen	jen	k9	jen
ve	v	k7c6	v
formálním	formální	k2eAgInSc6d1	formální
smyslu	smysl	k1gInSc6	smysl
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1993	[number]	k4	1993
Ústavní	ústavní	k2eAgInSc1d1	ústavní
soud	soud	k1gInSc1	soud
judikoval	judikovat	k5eAaBmAgInS	judikovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
[	[	kIx(	[
<g/>
ú	ú	k0	ú
<g/>
]	]	kIx)	]
<g/>
stava	stava	k6eAd1	stava
akceptuje	akceptovat	k5eAaBmIp3nS	akceptovat
a	a	k8xC	a
respektuje	respektovat	k5eAaImIp3nS	respektovat
princip	princip	k1gInSc1	princip
legality	legalita	k1gFnSc2	legalita
jako	jako	k8xC	jako
součást	součást	k1gFnSc4	součást
celkové	celkový	k2eAgFnSc2d1	celková
koncepce	koncepce	k1gFnSc2	koncepce
právního	právní	k2eAgInSc2d1	právní
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
neváže	vázat	k5eNaImIp3nS	vázat
však	však	k9	však
pozitivní	pozitivní	k2eAgNnSc4d1	pozitivní
právo	právo	k1gNnSc4	právo
jen	jen	k9	jen
na	na	k7c4	na
formální	formální	k2eAgFnSc4d1	formální
legalitu	legalita	k1gFnSc4	legalita
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
výklad	výklad	k1gInSc1	výklad
a	a	k8xC	a
použití	použití	k1gNnSc1	použití
právních	právní	k2eAgFnPc2d1	právní
norem	norma	k1gFnPc2	norma
<g />
.	.	kIx.	.
</s>
<s>
podřizuje	podřizovat	k5eAaImIp3nS	podřizovat
jejich	jejich	k3xOp3gNnSc1	jejich
obsahově	obsahově	k6eAd1	obsahově
materiálnímu	materiální	k2eAgInSc3d1	materiální
smyslu	smysl	k1gInSc3	smysl
<g/>
,	,	kIx,	,
podmiňuje	podmiňovat	k5eAaImIp3nS	podmiňovat
právo	právo	k1gNnSc4	právo
respektováním	respektování	k1gNnSc7	respektování
základních	základní	k2eAgFnPc2d1	základní
konstitutivních	konstitutivní	k2eAgFnPc2d1	konstitutivní
hodnot	hodnota	k1gFnPc2	hodnota
demokratické	demokratický	k2eAgFnSc2d1	demokratická
společnosti	společnost	k1gFnSc2	společnost
a	a	k8xC	a
těmito	tento	k3xDgFnPc7	tento
hodnotami	hodnota	k1gFnPc7	hodnota
také	také	k6eAd1	také
užití	užití	k1gNnSc3	užití
právních	právní	k2eAgFnPc2d1	právní
norem	norma	k1gFnPc2	norma
měří	měřit	k5eAaImIp3nS	měřit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Konstatování	konstatování	k1gNnSc1	konstatování
<g/>
,	,	kIx,	,
že	že	k8xS	že
republika	republika	k1gFnSc1	republika
je	být	k5eAaImIp3nS	být
stát	stát	k5eAaImF	stát
"	"	kIx"	"
<g/>
založený	založený	k2eAgInSc4d1	založený
na	na	k7c6	na
úctě	úcta	k1gFnSc6	úcta
k	k	k7c3	k
právům	právo	k1gNnPc3	právo
a	a	k8xC	a
svobodám	svoboda	k1gFnPc3	svoboda
člověka	člověk	k1gMnSc2	člověk
a	a	k8xC	a
občana	občan	k1gMnSc2	občan
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
zakotvením	zakotvení	k1gNnSc7	zakotvení
státního	státní	k2eAgInSc2d1	státní
cíle	cíl	k1gInSc2	cíl
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zavazuje	zavazovat	k5eAaImIp3nS	zavazovat
státní	státní	k2eAgFnSc4d1	státní
moc	moc	k1gFnSc4	moc
<g/>
.	.	kIx.	.
</s>
<s>
Formulace	formulace	k1gFnSc1	formulace
pak	pak	k6eAd1	pak
úzce	úzko	k6eAd1	úzko
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
článkem	článek	k1gInSc7	článek
3	[number]	k4	3
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
prohlašuje	prohlašovat	k5eAaImIp3nS	prohlašovat
Listinu	listina	k1gFnSc4	listina
součástí	součást	k1gFnPc2	součást
ústavního	ústavní	k2eAgInSc2d1	ústavní
pořádku	pořádek	k1gInSc2	pořádek
a	a	k8xC	a
článkem	článek	k1gInSc7	článek
9	[number]	k4	9
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
něhož	jenž	k3xRgInSc2	jenž
je	být	k5eAaImIp3nS	být
zakázána	zakázán	k2eAgFnSc1d1	zakázána
–	–	k?	–
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
ústavodárci	ústavodárce	k1gMnPc1	ústavodárce
–	–	k?	–
změna	změna	k1gFnSc1	změna
podstatných	podstatný	k2eAgFnPc2d1	podstatná
náležitostí	náležitost	k1gFnPc2	náležitost
demokratického	demokratický	k2eAgInSc2d1	demokratický
právního	právní	k2eAgInSc2d1	právní
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
jako	jako	k8xS	jako
právní	právní	k2eAgInSc1d1	právní
a	a	k8xC	a
demokratický	demokratický	k2eAgInSc1d1	demokratický
stát	stát	k1gInSc1	stát
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
podstatě	podstata	k1gFnSc6	podstata
také	také	k9	také
nepochybně	pochybně	k6eNd1	pochybně
sociálním	sociální	k2eAgInSc7d1	sociální
státem	stát	k1gInSc7	stát
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
taková	takový	k3xDgFnSc1	takový
formulace	formulace	k1gFnSc1	formulace
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
Ústavě	ústava	k1gFnSc6	ústava
výslovně	výslovně	k6eAd1	výslovně
uvedena	uvést	k5eAaPmNgFnS	uvést
<g/>
.	.	kIx.	.
</s>
<s>
Odstavec	odstavec	k1gInSc1	odstavec
druhý	druhý	k4xOgMnSc1	druhý
pak	pak	k9	pak
přebírá	přebírat	k5eAaImIp3nS	přebírat
základní	základní	k2eAgFnSc4d1	základní
zásadu	zásada	k1gFnSc4	zásada
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
práva	právo	k1gNnSc2	právo
o	o	k7c6	o
poctivém	poctivý	k2eAgNnSc6d1	poctivé
plnění	plnění	k1gNnSc6	plnění
mezinárodních	mezinárodní	k2eAgInPc2d1	mezinárodní
závazků	závazek	k1gInPc2	závazek
a	a	k8xC	a
do	do	k7c2	do
Ústavy	ústava	k1gFnSc2	ústava
byl	být	k5eAaImAgInS	být
připojen	připojit	k5eAaPmNgInS	připojit
až	až	k9	až
euronovelou	euronovela	k1gFnSc7	euronovela
Ústavy	ústava	k1gFnSc2	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
odstavce	odstavec	k1gInSc2	odstavec
pak	pak	k6eAd1	pak
vyplývají	vyplývat	k5eAaImIp3nP	vyplývat
povinnosti	povinnost	k1gFnPc4	povinnost
subjektům	subjekt	k1gInPc3	subjekt
státní	státní	k2eAgFnSc2d1	státní
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
především	především	k9	především
zákonodárci	zákonodárce	k1gMnSc3	zákonodárce
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nepřijímal	přijímat	k5eNaImAgMnS	přijímat
zákony	zákon	k1gInPc4	zákon
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
by	by	kYmCp3nP	by
znemožňovaly	znemožňovat	k5eAaImAgInP	znemožňovat
dodržení	dodržení	k1gNnSc4	dodržení
mezinárodněprávních	mezinárodněprávní	k2eAgInPc2d1	mezinárodněprávní
závazků	závazek	k1gInPc2	závazek
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
aplikace	aplikace	k1gFnSc2	aplikace
mezinárodněprávní	mezinárodněprávní	k2eAgFnSc2d1	mezinárodněprávní
normy	norma	k1gFnSc2	norma
je	být	k5eAaImIp3nS	být
také	také	k9	také
zohlednění	zohlednění	k1gNnSc1	zohlednění
judikatury	judikatura	k1gFnSc2	judikatura
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
institucí	instituce	k1gFnPc2	instituce
soudního	soudní	k2eAgInSc2d1	soudní
typu	typ	k1gInSc2	typ
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
mají	mít	k5eAaImIp3nP	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
tyto	tento	k3xDgFnPc4	tento
normy	norma	k1gFnPc4	norma
aplikovat	aplikovat	k5eAaBmF	aplikovat
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
ratifikací	ratifikace	k1gFnSc7	ratifikace
Lisabonské	lisabonský	k2eAgFnSc2d1	Lisabonská
smlouvy	smlouva	k1gFnSc2	smlouva
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
vyjádřili	vyjádřit	k5eAaPmAgMnP	vyjádřit
někteří	některý	k3yIgMnPc1	některý
politici	politik	k1gMnPc1	politik
obavy	obava	k1gFnSc2	obava
<g/>
,	,	kIx,	,
že	že	k8xS	že
její	její	k3xOp3gNnSc4	její
vtělení	vtělení	k1gNnSc4	vtělení
do	do	k7c2	do
právního	právní	k2eAgInSc2d1	právní
řádu	řád	k1gInSc2	řád
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
může	moct	k5eAaImIp3nS	moct
do	do	k7c2	do
těchto	tento	k3xDgInPc2	tento
základních	základní	k2eAgInPc2d1	základní
principů	princip	k1gInPc2	princip
zasáhnout	zasáhnout	k5eAaPmF	zasáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Ústavní	ústavní	k2eAgInSc1d1	ústavní
soud	soud	k1gInSc1	soud
nicméně	nicméně	k8xC	nicméně
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
nálezu	nález	k1gInSc6	nález
Pl.	Pl.	k1gFnSc2	Pl.
ÚS	ÚS	kA	ÚS
19	[number]	k4	19
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
judikoval	judikovat	k5eAaBmAgMnS	judikovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
důležité	důležitý	k2eAgNnSc1d1	důležité
poukázat	poukázat	k5eAaPmF	poukázat
na	na	k7c4	na
možnost	možnost	k1gFnSc4	možnost
členského	členský	k2eAgInSc2d1	členský
státu	stát	k1gInSc2	stát
z	z	k7c2	z
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
vystoupit	vystoupit	k5eAaPmF	vystoupit
postupem	postup	k1gInSc7	postup
stanoveným	stanovený	k2eAgInSc7d1	stanovený
v	v	k7c6	v
čl	čl	kA	čl
<g/>
.	.	kIx.	.
50	[number]	k4	50
Smlouvy	smlouva	k1gFnSc2	smlouva
o	o	k7c4	o
EU	EU	kA	EU
<g/>
;	;	kIx,	;
explicitní	explicitní	k2eAgFnSc2d1	explicitní
artikulace	artikulace	k1gFnSc2	artikulace
této	tento	k3xDgFnSc2	tento
možnosti	možnost	k1gFnSc2	možnost
v	v	k7c6	v
Lisabonské	lisabonský	k2eAgFnSc6d1	Lisabonská
smlouvě	smlouva	k1gFnSc6	smlouva
je	být	k5eAaImIp3nS	být
nesporným	sporný	k2eNgNnSc7d1	nesporné
potvrzením	potvrzení	k1gNnSc7	potvrzení
principu	princip	k1gInSc2	princip
States	Statesa	k1gFnPc2	Statesa
are	ar	k1gInSc5	ar
the	the	k?	the
Masters	Masters	k1gInSc1	Masters
of	of	k?	of
the	the	k?	the
Treaty	Treata	k1gFnSc2	Treata
a	a	k8xC	a
trvající	trvající	k2eAgFnSc2d1	trvající
svrchovanosti	svrchovanost	k1gFnSc2	svrchovanost
členských	členský	k2eAgInPc2d1	členský
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1863	[number]	k4	1863
vymezil	vymezit	k5eAaPmAgMnS	vymezit
Abraham	Abraham	k1gMnSc1	Abraham
Lincoln	Lincoln	k1gMnSc1	Lincoln
při	při	k7c6	při
projevu	projev	k1gInSc6	projev
v	v	k7c6	v
Gettysburgu	Gettysburg	k1gInSc6	Gettysburg
tři	tři	k4xCgInPc4	tři
znaky	znak	k1gInPc4	znak
vymezující	vymezující	k2eAgFnSc4d1	vymezující
demokracii	demokracie	k1gFnSc4	demokracie
<g/>
:	:	kIx,	:
vládu	vláda	k1gFnSc4	vláda
lidu	lid	k1gInSc2	lid
<g/>
,	,	kIx,	,
lidem	člověk	k1gMnPc3	člověk
a	a	k8xC	a
pro	pro	k7c4	pro
lid	lid	k1gInSc4	lid
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
lidu	lid	k1gInSc2	lid
je	být	k5eAaImIp3nS	být
upravena	upravit	k5eAaPmNgFnS	upravit
v	v	k7c6	v
čl	čl	kA	čl
<g/>
.	.	kIx.	.
2	[number]	k4	2
odst	odsta	k1gFnPc2	odsta
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
,	,	kIx,	,
vláda	vláda	k1gFnSc1	vláda
lidem	lid	k1gInSc7	lid
v	v	k7c6	v
odst	odst	k1gMnSc1	odst
<g/>
.	.	kIx.	.
1	[number]	k4	1
věty	věta	k1gFnPc4	věta
za	za	k7c7	za
středníkem	středník	k1gInSc7	středník
ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
s	s	k7c7	s
odst	odsta	k1gFnPc2	odsta
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
,	,	kIx,	,
a	a	k8xC	a
vláda	vláda	k1gFnSc1	vláda
pro	pro	k7c4	pro
lid	lid	k1gInSc4	lid
v	v	k7c6	v
odst	odst	k1gMnSc1	odst
<g/>
.	.	kIx.	.
3	[number]	k4	3
článku	článek	k1gInSc2	článek
2	[number]	k4	2
Ústavy	ústava	k1gFnSc2	ústava
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
odstavec	odstavec	k1gInSc1	odstavec
článku	článek	k1gInSc2	článek
2	[number]	k4	2
zakotvuje	zakotvovat	k5eAaImIp3nS	zakotvovat
princip	princip	k1gInSc1	princip
svrchovanosti	svrchovanost	k1gFnSc2	svrchovanost
lidu	lid	k1gInSc2	lid
a	a	k8xC	a
dělbu	dělba	k1gFnSc4	dělba
státní	státní	k2eAgFnSc2d1	státní
moci	moc	k1gFnSc2	moc
na	na	k7c4	na
zákonodárnou	zákonodárný	k2eAgFnSc4d1	zákonodárná
<g/>
,	,	kIx,	,
výkonnou	výkonný	k2eAgFnSc4d1	výkonná
a	a	k8xC	a
soudní	soudní	k2eAgFnSc4d1	soudní
<g/>
.	.	kIx.	.
</s>
<s>
Princip	princip	k1gInSc1	princip
svrchovanosti	svrchovanost	k1gFnSc2	svrchovanost
lidu	lid	k1gInSc2	lid
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
politický	politický	k2eAgInSc1d1	politický
nikoliv	nikoliv	k9	nikoliv
právní	právní	k2eAgInSc1d1	právní
–	–	k?	–
pojem	pojem	k1gInSc1	pojem
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
značí	značit	k5eAaImIp3nS	značit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
právě	právě	k9	právě
lid	lid	k1gInSc1	lid
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
disponuje	disponovat	k5eAaBmIp3nS	disponovat
právem	právem	k6eAd1	právem
vytvořit	vytvořit	k5eAaPmF	vytvořit
systém	systém	k1gInSc4	systém
hodnot	hodnota	k1gFnPc2	hodnota
<g/>
,	,	kIx,	,
institucí	instituce	k1gFnPc2	instituce
a	a	k8xC	a
procedur	procedura	k1gFnPc2	procedura
<g/>
,	,	kIx,	,
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
nichž	jenž	k3xRgInPc2	jenž
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
stát	stát	k5eAaPmF	stát
řídit	řídit	k5eAaImF	řídit
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
vyloučeno	vyloučen	k2eAgNnSc1d1	vyloučeno
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
existoval	existovat	k5eAaImAgInS	existovat
státní	státní	k2eAgInSc1d1	státní
orgán	orgán	k1gInSc1	orgán
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
by	by	kYmCp3nS	by
přímo	přímo	k6eAd1	přímo
či	či	k8xC	či
nepřímo	přímo	k6eNd1	přímo
svou	svůj	k3xOyFgFnSc4	svůj
legitimitu	legitimita	k1gFnSc4	legitimita
neodvozoval	odvozovat	k5eNaImAgMnS	odvozovat
od	od	k7c2	od
lidu	lid	k1gInSc2	lid
<g/>
.	.	kIx.	.
</s>
<s>
Odstavec	odstavec	k1gInSc1	odstavec
druhý	druhý	k4xOgInSc1	druhý
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
možnost	možnost	k1gFnSc4	možnost
přijmout	přijmout	k5eAaPmF	přijmout
ústavní	ústavní	k2eAgInSc4d1	ústavní
zákon	zákon	k1gInSc4	zákon
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
by	by	kYmCp3nS	by
zakotvil	zakotvit	k5eAaPmAgInS	zakotvit
možnost	možnost	k1gFnSc4	možnost
zavést	zavést	k5eAaPmF	zavést
některý	některý	k3yIgInSc4	některý
z	z	k7c2	z
institutů	institut	k1gInPc2	institut
přímé	přímý	k2eAgFnSc2d1	přímá
demokracie	demokracie	k1gFnSc2	demokracie
–	–	k?	–
především	především	k6eAd1	především
referenda	referendum	k1gNnSc2	referendum
<g/>
.	.	kIx.	.
</s>
<s>
Vládní	vládní	k2eAgInSc1d1	vládní
návrh	návrh	k1gInSc1	návrh
Ústavy	ústava	k1gFnSc2	ústava
s	s	k7c7	s
tímto	tento	k3xDgNnSc7	tento
ustanovením	ustanovení	k1gNnSc7	ustanovení
nepočítal	počítat	k5eNaImAgMnS	počítat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
ODS	ODS	kA	ODS
a	a	k8xC	a
Občanská	občanský	k2eAgFnSc1d1	občanská
demokratická	demokratický	k2eAgFnSc1d1	demokratická
aliance	aliance	k1gFnSc1	aliance
(	(	kIx(	(
<g/>
ODA	ODA	kA	ODA
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgFnP	být
proti	proti	k7c3	proti
referendu	referendum	k1gNnSc3	referendum
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
se	se	k3xPyFc4	se
však	však	k9	však
objevila	objevit	k5eAaPmAgFnS	objevit
hrozba	hrozba	k1gFnSc1	hrozba
nepřijetí	nepřijetí	k1gNnSc2	nepřijetí
Ústavy	ústava	k1gFnSc2	ústava
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
národní	národní	k2eAgFnSc6d1	národní
radě	rada	k1gFnSc6	rada
<g/>
,	,	kIx,	,
přišli	přijít	k5eAaPmAgMnP	přijít
někteří	některý	k3yIgMnPc1	některý
poslanci	poslanec	k1gMnPc1	poslanec
ODS	ODS	kA	ODS
s	s	k7c7	s
návrhem	návrh	k1gInSc7	návrh
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
k	k	k7c3	k
Ústavě	ústava	k1gFnSc3	ústava
uspořádáno	uspořádán	k2eAgNnSc1d1	uspořádáno
ratifikační	ratifikační	k2eAgNnSc1d1	ratifikační
referendum	referendum	k1gNnSc1	referendum
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
bylo	být	k5eAaImAgNnS	být
ustanovení	ustanovení	k1gNnSc1	ustanovení
do	do	k7c2	do
Ústavy	ústava	k1gFnSc2	ústava
zakomponováno	zakomponován	k2eAgNnSc1d1	zakomponováno
především	především	k9	především
z	z	k7c2	z
popudu	popud	k1gInSc2	popud
některých	některý	k3yIgMnPc2	některý
sociálnědemokratických	sociálnědemokratický	k2eAgMnPc2d1	sociálnědemokratický
poslanců	poslanec	k1gMnPc2	poslanec
a	a	k8xC	a
na	na	k7c4	na
naléhání	naléhání	k1gNnSc4	naléhání
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
bylo	být	k5eAaImAgNnS	být
od	od	k7c2	od
přijetí	přijetí	k1gNnSc2	přijetí
Ústavy	ústava	k1gFnSc2	ústava
připraveno	připravit	k5eAaPmNgNnS	připravit
několik	několik	k4yIc1	několik
návrhů	návrh	k1gInPc2	návrh
ústavních	ústavní	k2eAgInPc2d1	ústavní
zákonů	zákon	k1gInPc2	zákon
o	o	k7c6	o
referendu	referendum	k1gNnSc6	referendum
<g/>
,	,	kIx,	,
zatím	zatím	k6eAd1	zatím
jediné	jediný	k2eAgNnSc4d1	jediné
celostátní	celostátní	k2eAgNnSc4d1	celostátní
referendum	referendum	k1gNnSc4	referendum
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
se	se	k3xPyFc4	se
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
o	o	k7c6	o
přistoupení	přistoupení	k1gNnSc6	přistoupení
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
k	k	k7c3	k
Evropské	evropský	k2eAgFnSc3d1	Evropská
unii	unie	k1gFnSc3	unie
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
němuž	jenž	k3xRgNnSc3	jenž
byla	být	k5eAaImAgFnS	být
přijata	přijat	k2eAgFnSc1d1	přijata
i	i	k8xC	i
novela	novela	k1gFnSc1	novela
Ústavy	ústava	k1gFnSc2	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Odstavce	odstavec	k1gInPc1	odstavec
3	[number]	k4	3
a	a	k8xC	a
4	[number]	k4	4
upravují	upravovat	k5eAaImIp3nP	upravovat
zásadu	zásada	k1gFnSc4	zásada
enumerativnosti	enumerativnost	k1gFnSc2	enumerativnost
veřejnoprávních	veřejnoprávní	k2eAgFnPc2d1	veřejnoprávní
pretenzí	pretenze	k1gFnPc2	pretenze
a	a	k8xC	a
zásadu	zásada	k1gFnSc4	zásada
legální	legální	k2eAgFnSc2d1	legální
licence	licence	k1gFnSc2	licence
<g/>
.	.	kIx.	.
</s>
<s>
Zásada	zásada	k1gFnSc1	zásada
enumerativnosti	enumerativnost	k1gFnSc2	enumerativnost
veřejnoprávních	veřejnoprávní	k2eAgFnPc2d1	veřejnoprávní
pretenzí	pretenze	k1gFnPc2	pretenze
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
moc	moc	k1gFnSc4	moc
lze	lze	k6eAd1	lze
uplatňovat	uplatňovat	k5eAaImF	uplatňovat
jen	jen	k9	jen
v	v	k7c6	v
případech	případ	k1gInPc6	případ
<g/>
,	,	kIx,	,
mezích	mez	k1gFnPc6	mez
a	a	k8xC	a
způsoby	způsob	k1gInPc7	způsob
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
stanoví	stanovit	k5eAaPmIp3nS	stanovit
zákon	zákon	k1gInSc1	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Zásada	zásada	k1gFnSc1	zásada
legální	legální	k2eAgFnSc2d1	legální
licence	licence	k1gFnSc2	licence
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
opakem	opak	k1gInSc7	opak
zásady	zásada	k1gFnSc2	zásada
enumerativnosti	enumerativnost	k1gFnSc2	enumerativnost
veřejnoprávních	veřejnoprávní	k2eAgFnPc2d1	veřejnoprávní
pretenzí	pretenze	k1gFnPc2	pretenze
<g/>
,	,	kIx,	,
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
každý	každý	k3xTgMnSc1	každý
může	moct	k5eAaImIp3nS	moct
činit	činit	k5eAaImF	činit
vše	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
zákon	zákon	k1gInSc1	zákon
nezakazuje	zakazovat	k5eNaImIp3nS	zakazovat
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
není	být	k5eNaImIp3nS	být
povinen	povinen	k2eAgMnSc1d1	povinen
činit	činit	k5eAaImF	činit
nic	nic	k3yNnSc1	nic
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
mu	on	k3xPp3gMnSc3	on
zákon	zákon	k1gInSc1	zákon
neukládá	ukládat	k5eNaImIp3nS	ukládat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
patrná	patrný	k2eAgFnSc1d1	patrná
podobnost	podobnost	k1gFnSc1	podobnost
s	s	k7c7	s
ustanovením	ustanovení	k1gNnSc7	ustanovení
čl	čl	kA	čl
<g/>
.	.	kIx.	.
2	[number]	k4	2
Listiny	listina	k1gFnSc2	listina
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
zatímco	zatímco	k8xS	zatímco
Ústava	ústava	k1gFnSc1	ústava
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
"	"	kIx"	"
<g/>
každém	každý	k3xTgInSc6	každý
občanu	občan	k1gMnSc6	občan
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
Listina	listina	k1gFnSc1	listina
tento	tento	k3xDgInSc4	tento
okruh	okruh	k1gInSc4	okruh
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
na	na	k7c4	na
"	"	kIx"	"
<g/>
každého	každý	k3xTgMnSc2	každý
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Článek	článek	k1gInSc4	článek
3	[number]	k4	3
vymezující	vymezující	k2eAgInPc1d1	vymezující
<g/>
,	,	kIx,	,
že	že	k8xS	že
Listina	listina	k1gFnSc1	listina
základních	základní	k2eAgNnPc2d1	základní
práv	právo	k1gNnPc2	právo
a	a	k8xC	a
svobod	svoboda	k1gFnPc2	svoboda
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
ústavního	ústavní	k2eAgInSc2d1	ústavní
pořádku	pořádek	k1gInSc2	pořádek
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
obvyklým	obvyklý	k2eAgNnSc7d1	obvyklé
ustanovením	ustanovení	k1gNnSc7	ustanovení
ústav	ústav	k1gInSc1	ústav
<g/>
.	.	kIx.	.
</s>
<s>
Článek	článek	k1gInSc1	článek
3	[number]	k4	3
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
Ústavy	ústava	k1gFnSc2	ústava
zakomponován	zakomponovat	k5eAaPmNgInS	zakomponovat
až	až	k9	až
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1992	[number]	k4	1992
jako	jako	k8xS	jako
výsledek	výsledek	k1gInSc1	výsledek
politické	politický	k2eAgFnSc2d1	politická
dohody	dohoda	k1gFnSc2	dohoda
<g/>
:	:	kIx,	:
vládní	vládní	k2eAgInSc4d1	vládní
návrh	návrh	k1gInSc4	návrh
na	na	k7c4	na
Listinu	listina	k1gFnSc4	listina
vůbec	vůbec	k9	vůbec
neodkazoval	odkazovat	k5eNaImAgMnS	odkazovat
<g/>
,	,	kIx,	,
pozdější	pozdní	k2eAgInPc1d2	pozdější
návrhy	návrh	k1gInPc1	návrh
již	jenž	k3xRgFnSc4	jenž
Listinu	listina	k1gFnSc4	listina
zmiňovaly	zmiňovat	k5eAaImAgFnP	zmiňovat
alespoň	alespoň	k9	alespoň
v	v	k7c6	v
přechodných	přechodný	k2eAgInPc6d1	přechodný
a	a	k8xC	a
závěrečných	závěrečný	k2eAgInPc6d1	závěrečný
ustanoveních	ustanovení	k1gNnPc6	ustanovení
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
způsob	způsob	k1gInSc4	způsob
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
neodpovídal	odpovídat	k5eNaImAgInS	odpovídat
významu	význam	k1gInSc3	význam
Listiny	listina	k1gFnSc2	listina
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
všechny	všechen	k3xTgInPc4	všechen
výbory	výbor	k1gInPc4	výbor
ČNR	ČNR	kA	ČNR
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1992	[number]	k4	1992
navrhly	navrhnout	k5eAaPmAgFnP	navrhnout
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
odkaz	odkaz	k1gInSc4	odkaz
na	na	k7c4	na
Listinu	listina	k1gFnSc4	listina
uveden	uveden	k2eAgInSc1d1	uveden
již	již	k6eAd1	již
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
první	první	k4xOgFnSc6	první
hlavě	hlava	k1gFnSc6	hlava
Ústavy	ústava	k1gFnSc2	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštností	zvláštnost	k1gFnSc7	zvláštnost
ustanovení	ustanovení	k1gNnSc2	ustanovení
je	být	k5eAaImIp3nS	být
také	také	k9	také
podivný	podivný	k2eAgInSc4d1	podivný
slovosled	slovosled	k1gInSc4	slovosled
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgInS	zvolit
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
slovo	slovo	k1gNnSc1	slovo
Listina	listina	k1gFnSc1	listina
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
psáno	psán	k2eAgNnSc1d1	psáno
s	s	k7c7	s
velkým	velký	k2eAgNnSc7d1	velké
počátečním	počáteční	k2eAgNnSc7d1	počáteční
písmenem	písmeno	k1gNnSc7	písmeno
<g/>
.	.	kIx.	.
</s>
<s>
Listina	listina	k1gFnSc1	listina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
doposud	doposud	k6eAd1	doposud
připojena	připojen	k2eAgFnSc1d1	připojena
k	k	k7c3	k
ústavnímu	ústavní	k2eAgInSc3d1	ústavní
zákonu	zákon	k1gInSc3	zákon
č.	č.	k?	č.
23	[number]	k4	23
<g/>
/	/	kIx~	/
<g/>
1991	[number]	k4	1991
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
od	od	k7c2	od
něj	on	k3xPp3gMnSc2	on
oddělena	oddělen	k2eAgFnSc1d1	oddělena
a	a	k8xC	a
nově	nově	k6eAd1	nově
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
formou	forma	k1gFnSc7	forma
zvláštního	zvláštní	k2eAgNnSc2d1	zvláštní
usnesení	usnesení	k1gNnSc2	usnesení
Předsednictva	předsednictvo	k1gNnSc2	předsednictvo
ČNR	ČNR	kA	ČNR
č.	č.	k?	č.
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
1993	[number]	k4	1993
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
Tento	tento	k3xDgInSc1	tento
postup	postup	k1gInSc1	postup
Předsednictva	předsednictvo	k1gNnSc2	předsednictvo
ČNR	ČNR	kA	ČNR
v	v	k7c6	v
následujícím	následující	k2eAgNnSc6d1	následující
období	období	k1gNnSc6	období
posloužil	posloužit	k5eAaPmAgMnS	posloužit
ke	k	k7c3	k
zpochybnění	zpochybnění	k1gNnSc3	zpochybnění
normativní	normativní	k2eAgFnSc2d1	normativní
povahy	povaha	k1gFnSc2	povaha
Listiny	listina	k1gFnSc2	listina
jako	jako	k8xS	jako
takové	takový	k3xDgFnSc2	takový
<g/>
.	.	kIx.	.
</s>
<s>
Podobný	podobný	k2eAgInSc1d1	podobný
odkaz	odkaz	k1gInSc1	odkaz
na	na	k7c4	na
úpravu	úprava	k1gFnSc4	úprava
základních	základní	k2eAgNnPc2d1	základní
práv	právo	k1gNnPc2	právo
a	a	k8xC	a
svobod	svoboda	k1gFnPc2	svoboda
se	se	k3xPyFc4	se
nacházel	nacházet	k5eAaImAgMnS	nacházet
také	také	k9	také
v	v	k7c6	v
prosincové	prosincový	k2eAgFnSc6d1	prosincová
ústavě	ústava	k1gFnSc6	ústava
(	(	kIx(	(
<g/>
1867	[number]	k4	1867
<g/>
)	)	kIx)	)
a	a	k8xC	a
ve	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
ústavním	ústavní	k2eAgNnSc6d1	ústavní
provizoriu	provizorium	k1gNnSc6	provizorium
(	(	kIx(	(
<g/>
č.	č.	k?	č.
37	[number]	k4	37
<g/>
/	/	kIx~	/
<g/>
1918	[number]	k4	1918
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
z.	z.	k?	z.
a	a	k8xC	a
n.	n.	k?	n.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Článek	článek	k1gInSc1	článek
4	[number]	k4	4
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
něhož	jenž	k3xRgInSc2	jenž
jsou	být	k5eAaImIp3nP	být
základní	základní	k2eAgNnPc4d1	základní
práva	právo	k1gNnPc4	právo
a	a	k8xC	a
svobody	svoboda	k1gFnPc4	svoboda
pod	pod	k7c7	pod
ochranou	ochrana	k1gFnSc7	ochrana
soudní	soudní	k2eAgFnSc2d1	soudní
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
nezakládá	zakládat	k5eNaImIp3nS	zakládat
ochranu	ochrana	k1gFnSc4	ochrana
toliko	toliko	k6eAd1	toliko
na	na	k7c4	na
ta	ten	k3xDgNnPc4	ten
práva	právo	k1gNnPc4	právo
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnPc1	jenž
jsou	být	k5eAaImIp3nP	být
upravena	upraven	k2eAgNnPc1d1	upraveno
v	v	k7c6	v
Listině	listina	k1gFnSc6	listina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
ústavních	ústavní	k2eAgInPc6d1	ústavní
předpisech	předpis	k1gInPc6	předpis
a	a	k8xC	a
mezinárodních	mezinárodní	k2eAgFnPc6d1	mezinárodní
úmluvách	úmluva	k1gFnPc6	úmluva
<g/>
.	.	kIx.	.
</s>
<s>
Podstata	podstata	k1gFnSc1	podstata
politického	politický	k2eAgInSc2d1	politický
systému	systém	k1gInSc2	systém
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
je	být	k5eAaImIp3nS	být
vyjádřena	vyjádřit	k5eAaPmNgFnS	vyjádřit
v	v	k7c6	v
článku	článek	k1gInSc6	článek
5	[number]	k4	5
Ústavy	ústava	k1gFnSc2	ústava
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
zakotveny	zakotven	k2eAgFnPc1d1	zakotvena
politické	politický	k2eAgFnPc1d1	politická
strany	strana	k1gFnPc1	strana
<g/>
,	,	kIx,	,
jimž	jenž	k3xRgFnPc3	jenž
je	být	k5eAaImIp3nS	být
přiznána	přiznán	k2eAgFnSc1d1	přiznána
nezastupitelná	zastupitelný	k2eNgFnSc1d1	nezastupitelná
úloha	úloha	k1gFnSc1	úloha
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc7	jejich
prostřednictvím	prostřednictví	k1gNnSc7	prostřednictví
se	se	k3xPyFc4	se
občané	občan	k1gMnPc1	občan
účastní	účastnit	k5eAaImIp3nP	účastnit
na	na	k7c6	na
politickém	politický	k2eAgInSc6d1	politický
životě	život	k1gInSc6	život
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
politické	politický	k2eAgFnSc2d1	politická
strany	strana	k1gFnSc2	strana
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
svobodný	svobodný	k2eAgMnSc1d1	svobodný
a	a	k8xC	a
nepodléhá	podléhat	k5eNaImIp3nS	podléhat
tudíž	tudíž	k8xC	tudíž
povolení	povolení	k1gNnSc4	povolení
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
státního	státní	k2eAgInSc2d1	státní
orgánu	orgán	k1gInSc2	orgán
(	(	kIx(	(
<g/>
k	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
fungování	fungování	k1gNnSc3	fungování
je	být	k5eAaImIp3nS	být
nicméně	nicméně	k8xC	nicméně
vyžadována	vyžadován	k2eAgFnSc1d1	vyžadována
registrace	registrace	k1gFnSc1	registrace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Důraz	důraz	k1gInSc1	důraz
na	na	k7c4	na
volnou	volný	k2eAgFnSc4d1	volná
soutěž	soutěž	k1gFnSc4	soutěž
odmítá	odmítat	k5eAaImIp3nS	odmítat
omezování	omezování	k1gNnSc1	omezování
plurality	pluralita	k1gFnSc2	pluralita
stran	strana	k1gFnPc2	strana
a	a	k8xC	a
vedoucí	vedoucí	k2eAgFnSc4d1	vedoucí
úlohu	úloha	k1gFnSc4	úloha
jakékoli	jakýkoli	k3yIgFnSc3	jakýkoli
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Strany	strana	k1gFnPc1	strana
a	a	k8xC	a
hnutí	hnutí	k1gNnPc1	hnutí
nesmějí	smát	k5eNaImIp3nP	smát
své	svůj	k3xOyFgInPc4	svůj
zájmy	zájem	k1gInPc4	zájem
prosazovat	prosazovat	k5eAaImF	prosazovat
násilím	násilí	k1gNnSc7	násilí
<g/>
.	.	kIx.	.
</s>
<s>
Fungování	fungování	k1gNnSc1	fungování
stran	strana	k1gFnPc2	strana
a	a	k8xC	a
hnutí	hnutí	k1gNnSc2	hnutí
potom	potom	k6eAd1	potom
blíže	blízce	k6eAd2	blízce
upravuje	upravovat	k5eAaImIp3nS	upravovat
zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
424	[number]	k4	424
<g/>
/	/	kIx~	/
<g/>
1991	[number]	k4	1991
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
o	o	k7c6	o
sdružování	sdružování	k1gNnSc6	sdružování
v	v	k7c6	v
politických	politický	k2eAgFnPc6d1	politická
stranách	strana	k1gFnPc6	strana
a	a	k8xC	a
politických	politický	k2eAgNnPc6d1	politické
hnutích	hnutí	k1gNnPc6	hnutí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
politickému	politický	k2eAgNnSc3d1	politické
rozhodování	rozhodování	k1gNnSc3	rozhodování
se	se	k3xPyFc4	se
váže	vázat	k5eAaImIp3nS	vázat
článek	článek	k1gInSc1	článek
6	[number]	k4	6
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zakotvuje	zakotvovat	k5eAaImIp3nS	zakotvovat
princip	princip	k1gInSc1	princip
vlády	vláda	k1gFnSc2	vláda
většiny	většina	k1gFnSc2	většina
a	a	k8xC	a
ochranu	ochrana	k1gFnSc4	ochrana
menšin	menšina	k1gFnPc2	menšina
<g/>
.	.	kIx.	.
</s>
<s>
Článek	článek	k1gInSc1	článek
7	[number]	k4	7
zakotvující	zakotvující	k2eAgFnSc4d1	zakotvující
ochranu	ochrana	k1gFnSc4	ochrana
přírody	příroda	k1gFnSc2	příroda
původní	původní	k2eAgFnSc2d1	původní
vládní	vládní	k2eAgInSc4d1	vládní
návrh	návrh	k1gInSc4	návrh
Ústavy	ústava	k1gFnSc2	ústava
nezahrnoval	zahrnovat	k5eNaImAgMnS	zahrnovat
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
autorem	autor	k1gMnSc7	autor
je	být	k5eAaImIp3nS	být
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgInS	být
přesvědčen	přesvědčit	k5eAaPmNgMnS	přesvědčit
o	o	k7c6	o
vhodnosti	vhodnost	k1gFnSc6	vhodnost
mít	mít	k5eAaImF	mít
v	v	k7c6	v
Ústavě	ústava	k1gFnSc6	ústava
"	"	kIx"	"
<g/>
ekologický	ekologický	k2eAgInSc1d1	ekologický
paragraf	paragraf	k1gInSc1	paragraf
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
byla	být	k5eAaImAgFnS	být
do	do	k7c2	do
Ústavy	ústava	k1gFnSc2	ústava
přijata	přijat	k2eAgFnSc1d1	přijata
redukovaná	redukovaný	k2eAgFnSc1d1	redukovaná
podoba	podoba	k1gFnSc1	podoba
Havlova	Havlův	k2eAgInSc2d1	Havlův
návrhu	návrh	k1gInSc2	návrh
<g/>
.	.	kIx.	.
</s>
<s>
Článek	článek	k1gInSc1	článek
8	[number]	k4	8
je	být	k5eAaImIp3nS	být
základním	základní	k2eAgNnSc7d1	základní
ustanovením	ustanovení	k1gNnSc7	ustanovení
zaručující	zaručující	k2eAgInSc4d1	zaručující
princip	princip	k1gInSc4	princip
územní	územní	k2eAgFnSc2d1	územní
samosprávy	samospráva	k1gFnSc2	samospráva
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc1	jehož
odstranění	odstranění	k1gNnSc1	odstranění
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
porušení	porušení	k1gNnSc4	porušení
nejen	nejen	k6eAd1	nejen
Evropské	evropský	k2eAgFnSc2d1	Evropská
charty	charta	k1gFnSc2	charta
místní	místní	k2eAgFnSc2d1	místní
samosprávy	samospráva	k1gFnSc2	samospráva
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
ústavně	ústavně	k6eAd1	ústavně
nepřípustné	přípustný	k2eNgNnSc1d1	nepřípustné
kvůli	kvůli	k7c3	kvůli
zásahu	zásah	k1gInSc3	zásah
do	do	k7c2	do
podstatných	podstatný	k2eAgFnPc2d1	podstatná
náležitostí	náležitost	k1gFnPc2	náležitost
demokratického	demokratický	k2eAgInSc2d1	demokratický
právního	právní	k2eAgInSc2d1	právní
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Územní	územní	k2eAgFnSc1d1	územní
samospráva	samospráva	k1gFnSc1	samospráva
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
dále	daleko	k6eAd2	daleko
rozvedena	rozveden	k2eAgFnSc1d1	rozvedena
v	v	k7c6	v
čl	čl	kA	čl
<g/>
.	.	kIx.	.
99	[number]	k4	99
až	až	k9	až
105	[number]	k4	105
<g/>
.	.	kIx.	.
</s>
<s>
Článek	článek	k1gInSc1	článek
9	[number]	k4	9
vymezuje	vymezovat	k5eAaImIp3nS	vymezovat
tři	tři	k4xCgNnPc4	tři
základní	základní	k2eAgNnPc4d1	základní
pravidla	pravidlo	k1gNnPc4	pravidlo
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
že	že	k8xS	že
změna	změna	k1gFnSc1	změna
Ústavy	ústava	k1gFnSc2	ústava
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
provedena	provést	k5eAaPmNgFnS	provést
toliko	toliko	k6eAd1	toliko
formou	forma	k1gFnSc7	forma
ústavního	ústavní	k2eAgInSc2d1	ústavní
zákona	zákon	k1gInSc2	zákon
(	(	kIx(	(
<g/>
odst	odst	k1gInSc1	odst
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
že	že	k8xS	že
ani	ani	k8xC	ani
takovou	takový	k3xDgFnSc7	takový
formou	forma	k1gFnSc7	forma
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
změnit	změnit	k5eAaPmF	změnit
podstatné	podstatný	k2eAgFnPc4d1	podstatná
náležitosti	náležitost	k1gFnPc4	náležitost
demokratického	demokratický	k2eAgInSc2d1	demokratický
právního	právní	k2eAgInSc2d1	právní
státu	stát	k1gInSc2	stát
(	(	kIx(	(
<g/>
odst	odst	k1gInSc1	odst
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
)	)	kIx)	)
a	a	k8xC	a
že	že	k8xS	že
toto	tento	k3xDgNnSc4	tento
"	"	kIx"	"
<g/>
materiální	materiální	k2eAgNnSc4d1	materiální
jádro	jádro	k1gNnSc4	jádro
Ústavy	ústava	k1gFnSc2	ústava
<g/>
"	"	kIx"	"
nesmí	smět	k5eNaImIp3nS	smět
být	být	k5eAaImF	být
odstraněno	odstranit	k5eAaPmNgNnS	odstranit
nebo	nebo	k8xC	nebo
ohroženo	ohrožen	k2eAgNnSc1d1	ohroženo
ani	ani	k8xC	ani
výkladem	výklad	k1gInSc7	výklad
právních	právní	k2eAgInPc2d1	právní
předpisů	předpis	k1gInPc2	předpis
(	(	kIx(	(
<g/>
odst	odst	k1gMnSc1	odst
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Odstavec	odstavec	k1gInSc1	odstavec
druhý	druhý	k4xOgInSc1	druhý
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
vymezení	vymezení	k1gNnSc2	vymezení
<g/>
,	,	kIx,	,
že	že	k8xS	že
změna	změna	k1gFnSc1	změna
podstatných	podstatný	k2eAgFnPc2d1	podstatná
náležitostí	náležitost	k1gFnPc2	náležitost
demokratického	demokratický	k2eAgInSc2d1	demokratický
právního	právní	k2eAgInSc2d1	právní
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
nepřípustná	přípustný	k2eNgFnSc1d1	nepřípustná
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
imperativem	imperativ	k1gInSc7	imperativ
nezměnitelnosti	nezměnitelnost	k1gFnSc3	nezměnitelnost
materiálního	materiální	k2eAgNnSc2d1	materiální
jádra	jádro	k1gNnSc2	jádro
Ústavy	ústava	k1gFnSc2	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Historicky	historicky	k6eAd1	historicky
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
takový	takový	k3xDgInSc1	takový
imperativ	imperativ	k1gInSc1	imperativ
objevil	objevit	k5eAaPmAgInS	objevit
v	v	k7c6	v
ústavě	ústava	k1gFnSc6	ústava
Francouzské	francouzský	k2eAgFnSc2d1	francouzská
republiky	republika	k1gFnSc2	republika
ze	z	k7c2	z
srpna	srpen	k1gInSc2	srpen
1884	[number]	k4	1884
<g/>
,	,	kIx,	,
kterým	který	k3yIgFnPc3	který
bylo	být	k5eAaImAgNnS	být
stanoveno	stanovit	k5eAaPmNgNnS	stanovit
<g/>
,	,	kIx,	,
že	že	k8xS	že
republikánská	republikánský	k2eAgFnSc1d1	republikánská
forma	forma	k1gFnSc1	forma
vlády	vláda	k1gFnSc2	vláda
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
předmětem	předmět	k1gInSc7	předmět
revize	revize	k1gFnSc2	revize
<g/>
.	.	kIx.	.
</s>
<s>
Identická	identický	k2eAgFnSc1d1	identická
konstrukce	konstrukce	k1gFnSc1	konstrukce
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
i	i	k9	i
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
Ústavě	ústava	k1gFnSc6	ústava
Francouzské	francouzský	k2eAgFnSc2d1	francouzská
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
přijatý	přijatý	k2eAgInSc1d1	přijatý
Základní	základní	k2eAgInSc1d1	základní
zákon	zákon	k1gInSc1	zákon
Spolkové	spolkový	k2eAgFnSc2d1	spolková
republiky	republika	k1gFnSc2	republika
Německo	Německo	k1gNnSc4	Německo
reagoval	reagovat	k5eAaBmAgInS	reagovat
nejen	nejen	k6eAd1	nejen
procedurální	procedurální	k2eAgFnSc7d1	procedurální
úpravou	úprava	k1gFnSc7	úprava
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
i	i	k9	i
dalšími	další	k2eAgInPc7d1	další
dvěma	dva	k4xCgInPc7	dva
omezeními	omezení	k1gNnPc7	omezení
<g/>
,	,	kIx,	,
na	na	k7c4	na
vývoj	vývoj	k1gInSc4	vývoj
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1919	[number]	k4	1919
až	až	k6eAd1	až
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
omezení	omezení	k1gNnSc1	omezení
stanovuje	stanovovat	k5eAaImIp3nS	stanovovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Základní	základní	k2eAgInSc1d1	základní
zákon	zákon	k1gInSc1	zákon
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
měněn	měnit	k5eAaImNgInS	měnit
pouze	pouze	k6eAd1	pouze
zákonem	zákon	k1gInSc7	zákon
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
výslovně	výslovně	k6eAd1	výslovně
mění	měnit	k5eAaImIp3nS	měnit
či	či	k8xC	či
doplňuje	doplňovat	k5eAaImIp3nS	doplňovat
text	text	k1gInSc4	text
Základního	základní	k2eAgInSc2d1	základní
zákona	zákon	k1gInSc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgNnSc7	druhý
opatřením	opatření	k1gNnSc7	opatření
bylo	být	k5eAaImAgNnS	být
vyjmutí	vyjmutí	k1gNnSc1	vyjmutí
materiálního	materiální	k2eAgNnSc2d1	materiální
ohniska	ohnisko	k1gNnSc2	ohnisko
z	z	k7c2	z
dispozice	dispozice	k1gFnSc2	dispozice
ústavodárce	ústavodárce	k1gMnSc2	ústavodárce
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
imperativu	imperativ	k1gInSc6	imperativ
nezměnitelnosti	nezměnitelnost	k1gFnSc2	nezměnitelnost
či	či	k8xC	či
o	o	k7c6	o
klausuli	klausule	k1gFnSc6	klausule
věčnosti	věčnost	k1gFnSc2	věčnost
<g/>
.	.	kIx.	.
</s>
<s>
Německá	německý	k2eAgFnSc1d1	německá
klausule	klausule	k1gFnSc1	klausule
věčnosti	věčnost	k1gFnSc2	věčnost
(	(	kIx(	(
<g/>
Ewigkeitsklausul	Ewigkeitsklausul	k1gInSc1	Ewigkeitsklausul
<g/>
)	)	kIx)	)
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
české	český	k2eAgFnSc2d1	Česká
blíže	blíž	k1gFnSc2	blíž
specifikuje	specifikovat	k5eAaBmIp3nS	specifikovat
materiální	materiální	k2eAgNnSc4d1	materiální
ohnisko	ohnisko	k1gNnSc4	ohnisko
(	(	kIx(	(
<g/>
členění	členění	k1gNnSc4	členění
spolku	spolek	k1gInSc2	spolek
do	do	k7c2	do
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
spolupůsobení	spolupůsobení	k1gNnSc1	spolupůsobení
zemí	zem	k1gFnPc2	zem
při	při	k7c6	při
zákonodárství	zákonodárství	k1gNnSc6	zákonodárství
<g/>
,	,	kIx,	,
důstojnost	důstojnost	k1gFnSc1	důstojnost
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
principy	princip	k1gInPc1	princip
demokratického	demokratický	k2eAgNnSc2d1	demokratické
a	a	k8xC	a
sociálního	sociální	k2eAgInSc2d1	sociální
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
suverenity	suverenita	k1gFnSc2	suverenita
lidu	lid	k1gInSc2	lid
<g/>
,	,	kIx,	,
dělbu	dělba	k1gFnSc4	dělba
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
vázanost	vázanost	k1gFnSc1	vázanost
státní	státní	k2eAgFnSc2d1	státní
moci	moc	k1gFnSc2	moc
zákonem	zákon	k1gInSc7	zákon
a	a	k8xC	a
právo	právo	k1gNnSc4	právo
na	na	k7c4	na
odpor	odpor	k1gInSc4	odpor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Klíčová	klíčový	k2eAgFnSc1d1	klíčová
norma	norma	k1gFnSc1	norma
pro	pro	k7c4	pro
inkorporaci	inkorporace	k1gFnSc4	inkorporace
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
práva	právo	k1gNnSc2	právo
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
článku	článek	k1gInSc6	článek
10	[number]	k4	10
Ústavy	ústava	k1gFnSc2	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
euronovely	euronovela	k1gFnSc2	euronovela
Ústavy	ústava	k1gFnSc2	ústava
poskytoval	poskytovat	k5eAaImAgInS	poskytovat
článek	článek	k1gInSc4	článek
mezinárodním	mezinárodnět	k5eAaPmIp1nS	mezinárodnět
smlouvám	smlouva	k1gFnPc3	smlouva
o	o	k7c6	o
lidských	lidský	k2eAgNnPc6d1	lidské
právech	právo	k1gNnPc6	právo
a	a	k8xC	a
základních	základní	k2eAgFnPc6d1	základní
svobodách	svoboda	k1gFnPc6	svoboda
sílu	síl	k1gInSc2	síl
obdobnou	obdobný	k2eAgFnSc7d1	obdobná
ústavnímu	ústavní	k2eAgMnSc3d1	ústavní
pořádku	pořádek	k1gInSc6	pořádek
<g/>
.	.	kIx.	.
</s>
<s>
Euronovelou	Euronovela	k1gFnSc7	Euronovela
byl	být	k5eAaImAgInS	být
okruh	okruh	k1gInSc1	okruh
smluv	smlouva	k1gFnPc2	smlouva
rozšířen	rozšířen	k2eAgMnSc1d1	rozšířen
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
zavedena	zavést	k5eAaPmNgFnS	zavést
aplikační	aplikační	k2eAgFnSc1d1	aplikační
přednost	přednost	k1gFnSc1	přednost
<g/>
.	.	kIx.	.
</s>
<s>
Články	článek	k1gInPc1	článek
10	[number]	k4	10
<g/>
a	a	k8xC	a
a	a	k8xC	a
10	[number]	k4	10
<g/>
b	b	k?	b
byly	být	k5eAaImAgFnP	být
do	do	k7c2	do
Ústavy	ústava	k1gFnSc2	ústava
zakomponovány	zakomponován	k2eAgFnPc4d1	zakomponována
tzv.	tzv.	kA	tzv.
euronovelou	euronovela	k1gFnSc7	euronovela
Ústavy	ústava	k1gFnSc2	ústava
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
reakcí	reakce	k1gFnSc7	reakce
na	na	k7c4	na
přistoupení	přistoupení	k1gNnSc4	přistoupení
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
k	k	k7c3	k
Evropské	evropský	k2eAgFnSc3d1	Evropská
unii	unie	k1gFnSc3	unie
<g/>
.	.	kIx.	.
</s>
<s>
Článek	článek	k1gInSc1	článek
10	[number]	k4	10
<g/>
a	a	k8xC	a
upravuje	upravovat	k5eAaImIp3nS	upravovat
podmínky	podmínka	k1gFnPc4	podmínka
přenesení	přenesení	k1gNnSc2	přenesení
pravomoci	pravomoc	k1gFnSc2	pravomoc
na	na	k7c4	na
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
organizaci	organizace	k1gFnSc4	organizace
nebo	nebo	k8xC	nebo
instituci	instituce	k1gFnSc4	instituce
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
otázkami	otázka	k1gFnPc7	otázka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
souvisejí	souviset	k5eAaImIp3nP	souviset
se	s	k7c7	s
závazky	závazek	k1gInPc7	závazek
vyplývajícími	vyplývající	k2eAgInPc7d1	vyplývající
z	z	k7c2	z
členství	členství	k1gNnSc2	členství
v	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
subjektu	subjekt	k1gInSc6	subjekt
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
založena	založen	k2eAgFnSc1d1	založena
povinnost	povinnost	k1gFnSc1	povinnost
vlády	vláda	k1gFnSc2	vláda
informovat	informovat	k5eAaBmF	informovat
Parlament	parlament	k1gInSc1	parlament
a	a	k8xC	a
právo	právo	k1gNnSc1	právo
komor	komora	k1gFnPc2	komora
parlamentu	parlament	k1gInSc2	parlament
se	se	k3xPyFc4	se
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
(	(	kIx(	(
<g/>
čl	čl	kA	čl
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
b	b	k?	b
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgNnSc1d1	základní
ústavní	ústavní	k2eAgNnSc1d1	ústavní
východisko	východisko	k1gNnSc1	východisko
pro	pro	k7c4	pro
vymezení	vymezení	k1gNnSc4	vymezení
a	a	k8xC	a
určení	určení	k1gNnSc4	určení
státního	státní	k2eAgNnSc2d1	státní
území	území	k1gNnSc2	území
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
článku	článek	k1gInSc6	článek
11	[number]	k4	11
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
zároveň	zároveň	k6eAd1	zároveň
určuje	určovat	k5eAaImIp3nS	určovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ke	k	k7c3	k
změnám	změna	k1gFnPc3	změna
státní	státní	k2eAgFnSc2d1	státní
hranice	hranice	k1gFnSc2	hranice
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
pouze	pouze	k6eAd1	pouze
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
ústavního	ústavní	k2eAgInSc2d1	ústavní
zákona	zákon	k1gInSc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Úprava	úprava	k1gFnSc1	úprava
nabývání	nabývání	k1gNnSc2	nabývání
a	a	k8xC	a
pozbývání	pozbývání	k1gNnSc2	pozbývání
státního	státní	k2eAgNnSc2d1	státní
občanství	občanství	k1gNnSc2	občanství
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
ustanovení	ustanovení	k1gNnSc6	ustanovení
článku	článek	k1gInSc2	článek
12	[number]	k4	12
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterého	který	k3yRgInSc2	který
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc4d1	možné
státní	státní	k2eAgNnSc4d1	státní
občanství	občanství	k1gNnSc4	občanství
pozbýt	pozbýt	k5eAaPmF	pozbýt
proti	proti	k7c3	proti
své	svůj	k3xOyFgFnSc3	svůj
vůli	vůle	k1gFnSc3	vůle
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
byl	být	k5eAaImAgInS	být
připraven	připravit	k5eAaPmNgInS	připravit
návrh	návrh	k1gInSc1	návrh
ústavního	ústavní	k2eAgInSc2d1	ústavní
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
státním	státní	k2eAgNnSc6d1	státní
občanství	občanství	k1gNnSc6	občanství
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
měl	mít	k5eAaImAgInS	mít
především	především	k6eAd1	především
zakotvit	zakotvit	k5eAaPmF	zakotvit
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
udělení	udělení	k1gNnSc4	udělení
státního	státní	k2eAgNnSc2d1	státní
občanství	občanství	k1gNnSc2	občanství
není	být	k5eNaImIp3nS	být
právní	právní	k2eAgInSc4d1	právní
nárok	nárok	k1gInSc4	nárok
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byla	být	k5eAaImAgFnS	být
nejspíše	nejspíše	k9	nejspíše
snaha	snaha	k1gFnSc1	snaha
o	o	k7c6	o
překonání	překonání	k1gNnSc6	překonání
judikatury	judikatura	k1gFnSc2	judikatura
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
správního	správní	k2eAgInSc2d1	správní
soudu	soud	k1gInSc2	soud
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
níž	jenž	k3xRgFnSc2	jenž
na	na	k7c4	na
udělení	udělení	k1gNnSc4	udělení
státního	státní	k2eAgNnSc2d1	státní
občanství	občanství	k1gNnSc2	občanství
je	být	k5eAaImIp3nS	být
subjektivní	subjektivní	k2eAgNnSc1d1	subjektivní
právo	právo	k1gNnSc1	právo
<g/>
.	.	kIx.	.
</s>
<s>
Zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
státním	státní	k2eAgNnSc6d1	státní
občanství	občanství	k1gNnSc6	občanství
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
č.	č.	k?	č.
186	[number]	k4	186
<g/>
/	/	kIx~	/
<g/>
2013	[number]	k4	2013
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
nabyl	nabýt	k5eAaPmAgInS	nabýt
účinnosti	účinnost	k1gFnSc2	účinnost
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2014	[number]	k4	2014
a	a	k8xC	a
v	v	k7c6	v
§	§	k?	§
12	[number]	k4	12
stanovil	stanovit	k5eAaPmAgInS	stanovit
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
získání	získání	k1gNnSc4	získání
občanství	občanství	k1gNnSc2	občanství
není	být	k5eNaImIp3nS	být
právní	právní	k2eAgInSc4d1	právní
nárok	nárok	k1gInSc4	nárok
<g/>
.	.	kIx.	.
</s>
<s>
Ústavní	ústavní	k2eAgNnSc1d1	ústavní
zakotvení	zakotvení	k1gNnSc1	zakotvení
Prahy	Praha	k1gFnSc2	Praha
jako	jako	k8xC	jako
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
článku	článek	k1gInSc6	článek
13	[number]	k4	13
Ústavy	ústava	k1gFnSc2	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
ustanovení	ustanovení	k1gNnSc2	ustanovení
byl	být	k5eAaImAgMnS	být
Pavel	Pavel	k1gMnSc1	Pavel
Zářecký	zářecký	k2eAgMnSc1d1	zářecký
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
redundantní	redundantní	k2eAgMnPc1d1	redundantní
ho	on	k3xPp3gNnSc4	on
původně	původně	k6eAd1	původně
označil	označit	k5eAaPmAgMnS	označit
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
premiér	premiér	k1gMnSc1	premiér
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
odstavec	odstavec	k1gInSc1	odstavec
článku	článek	k1gInSc2	článek
14	[number]	k4	14
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
taxativní	taxativní	k2eAgInSc4d1	taxativní
výčet	výčet	k1gInSc4	výčet
státních	státní	k2eAgInPc2d1	státní
symbolů	symbol	k1gInPc2	symbol
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
kterými	který	k3yRgFnPc7	který
jsou	být	k5eAaImIp3nP	být
malý	malý	k2eAgInSc1d1	malý
a	a	k8xC	a
velký	velký	k2eAgInSc1d1	velký
státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
<g/>
,	,	kIx,	,
státní	státní	k2eAgFnPc1d1	státní
barvy	barva	k1gFnPc1	barva
<g/>
,	,	kIx,	,
státní	státní	k2eAgFnSc1d1	státní
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
,	,	kIx,	,
vlajka	vlajka	k1gFnSc1	vlajka
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
státní	státní	k2eAgFnSc1d1	státní
pečeť	pečeť	k1gFnSc1	pečeť
a	a	k8xC	a
státní	státní	k2eAgFnSc1d1	státní
hymna	hymna	k1gFnSc1	hymna
<g/>
.	.	kIx.	.
</s>
<s>
Pouhý	pouhý	k2eAgInSc4d1	pouhý
výčet	výčet	k1gInSc4	výčet
státních	státní	k2eAgInPc2d1	státní
symbolů	symbol	k1gInPc2	symbol
bez	bez	k7c2	bez
jejich	jejich	k3xOp3gFnSc2	jejich
další	další	k2eAgFnSc2d1	další
ústavní	ústavní	k2eAgFnSc2d1	ústavní
konkretizace	konkretizace	k1gFnSc2	konkretizace
je	být	k5eAaImIp3nS	být
jevem	jev	k1gInSc7	jev
poměrně	poměrně	k6eAd1	poměrně
výjimečným	výjimečný	k2eAgInSc7d1	výjimečný
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgInPc1d1	státní
symboly	symbol	k1gInPc1	symbol
blíže	blíž	k1gFnSc2	blíž
upravuje	upravovat	k5eAaImIp3nS	upravovat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
odkazu	odkaz	k1gInSc2	odkaz
zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
1993	[number]	k4	1993
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
o	o	k7c6	o
státních	státní	k2eAgInPc6d1	státní
symbolech	symbol	k1gInPc6	symbol
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnSc1	jejich
užívání	užívání	k1gNnSc1	užívání
potom	potom	k6eAd1	potom
zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
352	[number]	k4	352
<g/>
/	/	kIx~	/
<g/>
2001	[number]	k4	2001
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
o	o	k7c6	o
užívání	užívání	k1gNnSc6	užívání
státních	státní	k2eAgInPc2d1	státní
symbolů	symbol	k1gInPc2	symbol
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlava	hlava	k1gFnSc1	hlava
druhá	druhý	k4xOgFnSc1	druhý
Ústavy	ústava	k1gFnSc2	ústava
(	(	kIx(	(
<g/>
čl	čl	kA	čl
<g/>
.	.	kIx.	.
15	[number]	k4	15
až	až	k9	až
53	[number]	k4	53
<g/>
)	)	kIx)	)
zakotvuje	zakotvovat	k5eAaImIp3nS	zakotvovat
postavení	postavení	k1gNnSc4	postavení
Parlamentu	parlament	k1gInSc2	parlament
jako	jako	k8xC	jako
nositele	nositel	k1gMnSc2	nositel
moci	moc	k1gFnSc2	moc
zákonodárné	zákonodárný	k2eAgFnSc2d1	zákonodárná
a	a	k8xC	a
ústavodárné	ústavodárný	k2eAgFnSc2d1	ústavodárná
<g/>
.	.	kIx.	.
</s>
<s>
Článek	článek	k1gInSc1	článek
15	[number]	k4	15
vymezuje	vymezovat	k5eAaImIp3nS	vymezovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
zákonodárná	zákonodárný	k2eAgFnSc1d1	zákonodárná
moc	moc	k1gFnSc1	moc
náleží	náležet	k5eAaImIp3nS	náležet
Parlamentu	parlament	k1gInSc2	parlament
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
dvěma	dva	k4xCgFnPc7	dva
komorami	komora	k1gFnPc7	komora
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
Poslaneckou	poslanecký	k2eAgFnSc4d1	Poslanecká
sněmovnou	sněmovna	k1gFnSc7	sněmovna
a	a	k8xC	a
Senátem	senát	k1gInSc7	senát
<g/>
.	.	kIx.	.
</s>
<s>
Poslanecká	poslanecký	k2eAgFnSc1d1	Poslanecká
sněmovna	sněmovna	k1gFnSc1	sněmovna
má	mít	k5eAaImIp3nS	mít
z	z	k7c2	z
dikce	dikce	k1gFnSc2	dikce
článku	článek	k1gInSc2	článek
16	[number]	k4	16
dvě	dva	k4xCgNnPc4	dva
stě	sto	k4xCgFnPc1	sto
poslanců	poslanec	k1gMnPc2	poslanec
volených	volený	k2eAgMnPc2d1	volený
na	na	k7c4	na
čtyřleté	čtyřletý	k2eAgNnSc4d1	čtyřleté
období	období	k1gNnSc4	období
<g/>
,	,	kIx,	,
Senát	senát	k1gInSc1	senát
81	[number]	k4	81
senátorů	senátor	k1gMnPc2	senátor
volených	volený	k2eAgMnPc2d1	volený
na	na	k7c4	na
šestileté	šestiletý	k2eAgNnSc4d1	šestileté
období	období	k1gNnSc4	období
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
každé	každý	k3xTgInPc4	každý
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
se	se	k3xPyFc4	se
volí	volit	k5eAaImIp3nS	volit
třetina	třetina	k1gFnSc1	třetina
senátorů	senátor	k1gMnPc2	senátor
<g/>
.	.	kIx.	.
</s>
<s>
Článek	článek	k1gInSc1	článek
17	[number]	k4	17
upravuje	upravovat	k5eAaImIp3nS	upravovat
lhůty	lhůta	k1gFnPc4	lhůta
voleb	volba	k1gFnPc2	volba
<g/>
,	,	kIx,	,
článek	článek	k1gInSc1	článek
18	[number]	k4	18
pak	pak	k6eAd1	pak
aktivní	aktivní	k2eAgNnSc4d1	aktivní
volební	volební	k2eAgNnSc4d1	volební
právo	právo	k1gNnSc4	právo
a	a	k8xC	a
způsob	způsob	k1gInSc4	způsob
volby	volba	k1gFnSc2	volba
<g/>
.	.	kIx.	.
</s>
<s>
Volby	volba	k1gFnPc1	volba
do	do	k7c2	do
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
se	se	k3xPyFc4	se
konají	konat	k5eAaImIp3nP	konat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
všeobecného	všeobecný	k2eAgNnSc2d1	všeobecné
<g/>
,	,	kIx,	,
rovného	rovný	k2eAgNnSc2d1	rovné
a	a	k8xC	a
přímého	přímý	k2eAgNnSc2d1	přímé
volebního	volební	k2eAgNnSc2d1	volební
práva	právo	k1gNnSc2	právo
dle	dle	k7c2	dle
zásad	zásada	k1gFnPc2	zásada
poměrného	poměrný	k2eAgNnSc2d1	poměrné
zastoupení	zastoupení	k1gNnSc2	zastoupení
<g/>
,	,	kIx,	,
do	do	k7c2	do
Senátu	senát	k1gInSc2	senát
se	se	k3xPyFc4	se
volí	volit	k5eAaImIp3nS	volit
tajným	tajný	k2eAgNnSc7d1	tajné
hlasováním	hlasování	k1gNnSc7	hlasování
taktéž	taktéž	k?	taktéž
na	na	k7c6	na
základě	základ	k1gInSc6	základ
všeobecného	všeobecný	k2eAgNnSc2d1	všeobecné
<g/>
,	,	kIx,	,
rovného	rovný	k2eAgNnSc2d1	rovné
a	a	k8xC	a
přímého	přímý	k2eAgNnSc2d1	přímé
volebního	volební	k2eAgNnSc2d1	volební
práva	právo	k1gNnSc2	právo
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
podle	podle	k7c2	podle
zásad	zásada	k1gFnPc2	zásada
většinového	většinový	k2eAgInSc2d1	většinový
volebního	volební	k2eAgInSc2d1	volební
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Aktivní	aktivní	k2eAgNnSc1d1	aktivní
volební	volební	k2eAgNnSc1d1	volební
právo	právo	k1gNnSc1	právo
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
právo	práv	k2eAgNnSc1d1	právo
volit	volit	k5eAaImF	volit
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
každý	každý	k3xTgMnSc1	každý
občan	občan	k1gMnSc1	občan
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
věku	věk	k1gInSc6	věk
osmnácti	osmnáct	k4xCc2	osmnáct
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Pasivní	pasivní	k2eAgNnSc4d1	pasivní
volební	volební	k2eAgNnSc4d1	volební
právo	právo	k1gNnSc4	právo
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
právo	právo	k1gNnSc4	právo
být	být	k5eAaImF	být
volen	volit	k5eAaImNgMnS	volit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
upraveno	upravit	k5eAaPmNgNnS	upravit
v	v	k7c6	v
článku	článek	k1gInSc6	článek
19	[number]	k4	19
<g/>
:	:	kIx,	:
do	do	k7c2	do
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
zvoleni	zvolen	k2eAgMnPc1d1	zvolen
občané	občan	k1gMnPc1	občan
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
aktivní	aktivní	k2eAgNnSc4d1	aktivní
volební	volební	k2eAgNnSc4d1	volební
právo	právo	k1gNnSc4	právo
a	a	k8xC	a
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
věku	věk	k1gInSc2	věk
21	[number]	k4	21
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Senátu	senát	k1gInSc2	senát
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
každý	každý	k3xTgMnSc1	každý
občan	občan	k1gMnSc1	občan
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
má	mít	k5eAaImIp3nS	mít
aktivní	aktivní	k2eAgMnSc1d1	aktivní
volební	volební	k2eAgNnSc4d1	volební
právo	právo	k1gNnSc4	právo
a	a	k8xC	a
který	který	k3yQgInSc1	který
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
věku	věk	k1gInSc2	věk
40	[number]	k4	40
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Mandát	mandát	k1gInSc1	mandát
z	z	k7c2	z
dikce	dikce	k1gFnSc2	dikce
Ústavy	ústava	k1gFnSc2	ústava
vzniká	vznikat	k5eAaImIp3nS	vznikat
zvolením	zvolení	k1gNnSc7	zvolení
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
den	den	k1gInSc4	den
zvolení	zvolení	k1gNnSc2	zvolení
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
poslední	poslední	k2eAgInSc4d1	poslední
den	den	k1gInSc4	den
voleb	volba	k1gFnPc2	volba
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
podmínky	podmínka	k1gFnPc1	podmínka
výkonu	výkon	k1gInSc2	výkon
volebního	volební	k2eAgNnSc2d1	volební
práva	právo	k1gNnSc2	právo
<g/>
,	,	kIx,	,
organizaci	organizace	k1gFnSc3	organizace
voleb	volba	k1gFnPc2	volba
a	a	k8xC	a
rozsah	rozsah	k1gInSc1	rozsah
soudního	soudní	k2eAgInSc2d1	soudní
přezkumu	přezkum	k1gInSc2	přezkum
stanovuje	stanovovat	k5eAaImIp3nS	stanovovat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
odkazu	odkaz	k1gInSc2	odkaz
v	v	k7c6	v
článku	článek	k1gInSc6	článek
20	[number]	k4	20
Ústavy	ústava	k1gFnSc2	ústava
zákon	zákon	k1gInSc1	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Vzájemná	vzájemný	k2eAgFnSc1d1	vzájemná
neslučitelnost	neslučitelnost	k1gFnSc1	neslučitelnost
členství	členství	k1gNnSc2	členství
v	v	k7c6	v
obou	dva	k4xCgFnPc6	dva
komorách	komora	k1gFnPc6	komora
Parlamentu	parlament	k1gInSc2	parlament
je	být	k5eAaImIp3nS	být
zakotvena	zakotvit	k5eAaPmNgFnS	zakotvit
v	v	k7c6	v
článku	článek	k1gInSc6	článek
21	[number]	k4	21
<g/>
.	.	kIx.	.
</s>
<s>
Pravá	pravý	k2eAgFnSc1d1	pravá
inkompatibilita	inkompatibilita	k1gFnSc1	inkompatibilita
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
neslučitelnost	neslučitelnost	k1gFnSc4	neslučitelnost
funkcí	funkce	k1gFnPc2	funkce
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
možný	možný	k2eAgInSc4d1	možný
střet	střet	k1gInSc4	střet
veřejného	veřejný	k2eAgInSc2d1	veřejný
zájmu	zájem	k1gInSc2	zájem
při	při	k7c6	při
výkonu	výkon	k1gInSc6	výkon
různých	různý	k2eAgFnPc2d1	různá
funkcí	funkce	k1gFnPc2	funkce
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
trojdělby	trojdělba	k1gFnSc2	trojdělba
státní	státní	k2eAgFnSc2d1	státní
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
upravena	upravit	k5eAaPmNgFnS	upravit
v	v	k7c6	v
článku	článek	k1gInSc6	článek
22	[number]	k4	22
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
funkcí	funkce	k1gFnSc7	funkce
poslance	poslanec	k1gMnSc2	poslanec
či	či	k8xC	či
senátora	senátor	k1gMnSc2	senátor
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
neslučitelný	slučitelný	k2eNgInSc1d1	neslučitelný
výkon	výkon	k1gInSc1	výkon
funkce	funkce	k1gFnSc2	funkce
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
soudce	soudce	k1gMnSc2	soudce
<g/>
,	,	kIx,	,
na	na	k7c6	na
základě	základ	k1gInSc6	základ
odkazu	odkaz	k1gInSc2	odkaz
na	na	k7c4	na
zákon	zákon	k1gInSc4	zákon
–	–	k?	–
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
159	[number]	k4	159
<g/>
/	/	kIx~	/
<g/>
2006	[number]	k4	2006
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
–	–	k?	–
také	také	k9	také
funkce	funkce	k1gFnSc2	funkce
vykonávané	vykonávaný	k2eAgFnSc2d1	vykonávaná
v	v	k7c6	v
pracovněprávní	pracovněprávní	k2eAgFnSc6d1	pracovněprávní
vztahu	vztah	k1gInSc6	vztah
nebo	nebo	k8xC	nebo
služebním	služební	k2eAgInSc6d1	služební
poměru	poměr	k1gInSc6	poměr
k	k	k7c3	k
České	český	k2eAgFnSc3d1	Česká
republice	republika	k1gFnSc3	republika
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
<g />
.	.	kIx.	.
</s>
<s>
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
funkce	funkce	k1gFnPc4	funkce
jmenované	jmenovaný	k2eAgFnPc4d1	jmenovaná
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
také	také	k9	také
funkce	funkce	k1gFnSc1	funkce
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgFnPc6	jenž
se	se	k3xPyFc4	se
při	při	k7c6	při
výkonu	výkon	k1gInSc6	výkon
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
na	na	k7c6	na
ministerstvech	ministerstvo	k1gNnPc6	ministerstvo
<g/>
,	,	kIx,	,
jiných	jiný	k2eAgNnPc6d1	jiné
správní	správní	k2eAgInPc1d1	správní
úřadech	úřad	k1gInPc6	úřad
<g/>
,	,	kIx,	,
státním	státní	k2eAgNnSc6d1	státní
zastupitelství	zastupitelství	k1gNnSc6	zastupitelství
či	či	k8xC	či
soudech	soud	k1gInPc6	soud
<g/>
,	,	kIx,	,
bezpečnostních	bezpečnostní	k2eAgInPc6d1	bezpečnostní
sborech	sbor	k1gInPc6	sbor
a	a	k8xC	a
ozbrojených	ozbrojený	k2eAgFnPc6d1	ozbrojená
silách	síla	k1gFnPc6	síla
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
Nejvyšším	vysoký	k2eAgInSc6d3	Nejvyšší
kontrolním	kontrolní	k2eAgInSc6d1	kontrolní
úřadu	úřad	k1gInSc6	úřad
<g/>
,	,	kIx,	,
Kanceláři	kancelář	k1gFnSc6	kancelář
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
Kanceláři	kancelář	k1gFnSc6	kancelář
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
a	a	k8xC	a
Kanceláři	kancelář	k1gFnSc6	kancelář
Senátu	senát	k1gInSc6	senát
<g/>
,	,	kIx,	,
Pozemkovém	pozemkový	k2eAgInSc6d1	pozemkový
fondu	fond	k1gInSc6	fond
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
či	či	k8xC	či
jiných	jiný	k2eAgInPc6d1	jiný
státních	státní	k2eAgInPc6d1	státní
fondech	fond	k1gInPc6	fond
a	a	k8xC	a
v	v	k7c6	v
Kanceláři	kancelář	k1gFnSc6	kancelář
Veřejného	veřejný	k2eAgMnSc2d1	veřejný
ochránce	ochránce	k1gMnSc2	ochránce
práv	práv	k2eAgInSc1d1	práv
<g/>
.	.	kIx.	.
</s>
<s>
Článek	článek	k1gInSc1	článek
23	[number]	k4	23
Ústavy	ústava	k1gFnSc2	ústava
upravuje	upravovat	k5eAaImIp3nS	upravovat
slib	slib	k1gInSc4	slib
poslance	poslanec	k1gMnSc2	poslanec
a	a	k8xC	a
senátora	senátor	k1gMnSc2	senátor
<g/>
.	.	kIx.	.
</s>
<s>
Článek	článek	k1gInSc1	článek
24	[number]	k4	24
řeší	řešit	k5eAaImIp3nS	řešit
vzdání	vzdání	k1gNnSc4	vzdání
se	se	k3xPyFc4	se
mandátu	mandát	k1gInSc3	mandát
<g/>
,	,	kIx,	,
článek	článek	k1gInSc4	článek
25	[number]	k4	25
pak	pak	k6eAd1	pak
zánik	zánik	k1gInSc1	zánik
mandátu	mandát	k1gInSc2	mandát
<g/>
.	.	kIx.	.
</s>
<s>
Mandát	mandát	k1gInSc1	mandát
zaniká	zanikat	k5eAaImIp3nS	zanikat
odepřením	odepření	k1gNnSc7	odepření
slibu	slib	k1gInSc2	slib
nebo	nebo	k8xC	nebo
složením	složení	k1gNnSc7	složení
slibu	slib	k1gInSc2	slib
s	s	k7c7	s
výhradou	výhrada	k1gFnSc7	výhrada
<g/>
,	,	kIx,	,
uplynutím	uplynutí	k1gNnSc7	uplynutí
volebního	volební	k2eAgNnSc2d1	volební
období	období	k1gNnSc2	období
<g/>
,	,	kIx,	,
vzdáním	vzdání	k1gNnPc3	vzdání
se	se	k3xPyFc4	se
mandátu	mandát	k1gInSc3	mandát
<g/>
,	,	kIx,	,
ztrátou	ztráta	k1gFnSc7	ztráta
volitelnosti	volitelnost	k1gFnSc2	volitelnost
<g/>
,	,	kIx,	,
rozpuštěním	rozpuštění	k1gNnSc7	rozpuštění
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
–	–	k?	–
v	v	k7c6	v
případě	případ	k1gInSc6	případ
poslanců	poslanec	k1gMnPc2	poslanec
–	–	k?	–
a	a	k8xC	a
vznikem	vznik	k1gInSc7	vznik
neslučitelnosti	neslučitelnost	k1gFnSc2	neslučitelnost
dle	dle	k7c2	dle
článku	článek	k1gInSc2	článek
22	[number]	k4	22
Ústavy	ústava	k1gFnSc2	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
mandátu	mandát	k1gInSc3	mandát
Ústava	ústava	k1gFnSc1	ústava
dále	daleko	k6eAd2	daleko
upravuje	upravovat	k5eAaImIp3nS	upravovat
jeho	on	k3xPp3gInSc4	on
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
článku	článek	k1gInSc6	článek
26	[number]	k4	26
<g/>
.	.	kIx.	.
</s>
<s>
Imunita	imunita	k1gFnSc1	imunita
je	být	k5eAaImIp3nS	být
upravena	upravit	k5eAaPmNgFnS	upravit
v	v	k7c6	v
ustanovení	ustanovení	k1gNnSc6	ustanovení
článku	článek	k1gInSc2	článek
27	[number]	k4	27
Ústavy	ústava	k1gFnSc2	ústava
<g/>
,	,	kIx,	,
odst	odst	k1gMnSc1	odst
<g/>
.	.	kIx.	.
1	[number]	k4	1
vylučuje	vylučovat	k5eAaImIp3nS	vylučovat
jakýkoliv	jakýkoliv	k3yIgInSc4	jakýkoliv
postih	postih	k1gInSc4	postih
poslance	poslanec	k1gMnSc2	poslanec
či	či	k8xC	či
senátora	senátor	k1gMnSc2	senátor
pro	pro	k7c4	pro
hlasování	hlasování	k1gNnSc4	hlasování
v	v	k7c6	v
plénu	plénum	k1gNnSc6	plénum
či	či	k8xC	či
orgánech	orgán	k1gInPc6	orgán
obou	dva	k4xCgFnPc2	dva
komor	komora	k1gFnPc2	komora
<g/>
;	;	kIx,	;
odstavec	odstavec	k1gInSc1	odstavec
druhý	druhý	k4xOgMnSc1	druhý
pak	pak	k9	pak
upravuje	upravovat	k5eAaImIp3nS	upravovat
tzv.	tzv.	kA	tzv.
hmotněprávní	hmotněprávní	k2eAgFnSc1d1	hmotněprávní
neodpovědnost	neodpovědnost	k1gFnSc1	neodpovědnost
(	(	kIx(	(
<g/>
indemnitu	indemnita	k1gFnSc4	indemnita
<g/>
)	)	kIx)	)
poslanců	poslanec	k1gMnPc2	poslanec
a	a	k8xC	a
senátorů	senátor	k1gMnPc2	senátor
vztahující	vztahující	k2eAgInPc4d1	vztahující
se	se	k3xPyFc4	se
na	na	k7c4	na
projevy	projev	k1gInPc4	projev
učiněné	učiněný	k2eAgInPc4d1	učiněný
v	v	k7c6	v
Poslanecké	poslanecký	k2eAgFnSc6d1	Poslanecká
sněmovně	sněmovna	k1gFnSc6	sněmovna
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
v	v	k7c6	v
Senátu	senát	k1gInSc6	senát
či	či	k8xC	či
orgánech	orgán	k1gInPc6	orgán
komor	komora	k1gFnPc2	komora
Parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dikce	dikce	k1gFnSc2	dikce
třetího	třetí	k4xOgInSc2	třetí
odstavce	odstavec	k1gInSc2	odstavec
podléhá	podléhat	k5eAaImIp3nS	podléhat
poslanec	poslanec	k1gMnSc1	poslanec
či	či	k8xC	či
senátor	senátor	k1gMnSc1	senátor
za	za	k7c4	za
spáchané	spáchaný	k2eAgInPc4d1	spáchaný
přestupky	přestupek	k1gInPc4	přestupek
jen	jen	k6eAd1	jen
disciplinární	disciplinární	k2eAgFnPc4d1	disciplinární
pravomoci	pravomoc	k1gFnPc4	pravomoc
komory	komora	k1gFnSc2	komora
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
<g/>
,	,	kIx,	,
nestanoví	stanovit	k5eNaPmIp3nS	stanovit
<g/>
-li	i	k?	-li
zákon	zákon	k1gInSc1	zákon
jinak	jinak	k6eAd1	jinak
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
čtvrtý	čtvrtý	k4xOgInSc4	čtvrtý
odstavec	odstavec	k1gInSc4	odstavec
jsou	být	k5eAaImIp3nP	být
poslanci	poslanec	k1gMnPc1	poslanec
a	a	k8xC	a
senátoři	senátor	k1gMnPc1	senátor
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
svého	svůj	k3xOyFgInSc2	svůj
mandátu	mandát	k1gInSc2	mandát
nestíhatelní	stíhatelný	k2eNgMnPc1d1	nestíhatelný
<g/>
.	.	kIx.	.
</s>
<s>
Člena	člen	k1gMnSc4	člen
Parlamentu	parlament	k1gInSc2	parlament
lze	lze	k6eAd1	lze
zadržet	zadržet	k5eAaPmF	zadržet
–	–	k?	–
dle	dle	k7c2	dle
odstavce	odstavec	k1gInSc2	odstavec
5	[number]	k4	5
–	–	k?	–
toliko	toliko	k6eAd1	toliko
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
byl	být	k5eAaImAgMnS	být
dopaden	dopadnout	k5eAaPmNgMnS	dopadnout
při	při	k7c6	při
spáchání	spáchání	k1gNnSc6	spáchání
trestného	trestný	k2eAgInSc2d1	trestný
činu	čin	k1gInSc2	čin
či	či	k8xC	či
bezprostředně	bezprostředně	k6eAd1	bezprostředně
poté	poté	k6eAd1	poté
<g/>
.	.	kIx.	.
</s>
<s>
Právo	právo	k1gNnSc1	právo
odepřít	odepřít	k5eAaPmF	odepřít
svědectví	svědectví	k1gNnSc4	svědectví
o	o	k7c6	o
skutečnostech	skutečnost	k1gFnPc6	skutečnost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
poslanec	poslanec	k1gMnSc1	poslanec
či	či	k8xC	či
senátor	senátor	k1gMnSc1	senátor
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
výkonem	výkon	k1gInSc7	výkon
mandátu	mandát	k1gInSc2	mandát
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
mandátu	mandát	k1gInSc2	mandát
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zakotveno	zakotven	k2eAgNnSc1d1	zakotveno
ve	v	k7c6	v
článku	článek	k1gInSc6	článek
28	[number]	k4	28
Ústavy	ústava	k1gFnSc2	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Funkcionáři	funkcionář	k1gMnPc1	funkcionář
komor	komora	k1gFnPc2	komora
–	–	k?	–
předsedové	předseda	k1gMnPc1	předseda
a	a	k8xC	a
místopředsedové	místopředseda	k1gMnPc1	místopředseda
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
a	a	k8xC	a
Senátu	senát	k1gInSc2	senát
–	–	k?	–
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
dikce	dikce	k1gFnSc2	dikce
článku	článek	k1gInSc2	článek
29	[number]	k4	29
voleni	volit	k5eAaImNgMnP	volit
a	a	k8xC	a
odvoláváni	odvolávat	k5eAaImNgMnP	odvolávat
svou	svůj	k3xOyFgFnSc7	svůj
komorou	komora	k1gFnSc7	komora
Parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Poslanecká	poslanecký	k2eAgFnSc1d1	Poslanecká
sněmovna	sněmovna	k1gFnSc1	sněmovna
dle	dle	k7c2	dle
článku	článek	k1gInSc2	článek
30	[number]	k4	30
Ústavy	ústava	k1gFnSc2	ústava
může	moct	k5eAaImIp3nS	moct
zřídit	zřídit	k5eAaPmF	zřídit
vyšetřovací	vyšetřovací	k2eAgFnSc4d1	vyšetřovací
komisi	komise	k1gFnSc4	komise
pro	pro	k7c4	pro
prošetření	prošetření	k1gNnSc4	prošetření
věci	věc	k1gFnSc2	věc
veřejného	veřejný	k2eAgInSc2d1	veřejný
zájmu	zájem	k1gInSc2	zájem
<g/>
.	.	kIx.	.
</s>
<s>
Vyhrazení	vyhrazení	k1gNnSc1	vyhrazení
práva	právo	k1gNnSc2	právo
zřídit	zřídit	k5eAaPmF	zřídit
vyšetřovací	vyšetřovací	k2eAgFnSc4d1	vyšetřovací
komise	komise	k1gFnPc4	komise
Poslanecké	poslanecký	k2eAgFnSc3d1	Poslanecká
sněmovně	sněmovna	k1gFnSc3	sněmovna
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
odpovědností	odpovědnost	k1gFnSc7	odpovědnost
vlády	vláda	k1gFnSc2	vláda
Sněmovně	sněmovna	k1gFnSc3	sněmovna
<g/>
.	.	kIx.	.
</s>
<s>
Vyšetřovací	vyšetřovací	k2eAgFnSc1d1	vyšetřovací
komise	komise	k1gFnSc1	komise
je	být	k5eAaImIp3nS	být
zřizována	zřizovat	k5eAaImNgFnS	zřizovat
usnesením	usnesení	k1gNnSc7	usnesení
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
článku	článek	k1gInSc2	článek
31	[number]	k4	31
zřizují	zřizovat	k5eAaImIp3nP	zřizovat
komory	komora	k1gFnSc2	komora
výbory	výbor	k1gInPc1	výbor
a	a	k8xC	a
komise	komise	k1gFnSc2	komise
jako	jako	k8xC	jako
své	svůj	k3xOyFgInPc4	svůj
orgány	orgán	k1gInPc4	orgán
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
činnost	činnost	k1gFnSc1	činnost
těchto	tento	k3xDgInPc2	tento
orgánů	orgán	k1gInPc2	orgán
komor	komora	k1gFnPc2	komora
je	být	k5eAaImIp3nS	být
upravena	upravit	k5eAaPmNgFnS	upravit
zákonem	zákon	k1gInSc7	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
orgány	orgán	k1gMnPc7	orgán
Parlamentu	parlament	k1gInSc2	parlament
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
neformálně	formálně	k6eNd1	formálně
počítat	počítat	k5eAaImF	počítat
i	i	k9	i
delegace	delegace	k1gFnPc1	delegace
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
reprezentují	reprezentovat	k5eAaImIp3nP	reprezentovat
stát	stát	k5eAaPmF	stát
kupř	kupř	kA	kupř
<g/>
.	.	kIx.	.
v	v	k7c6	v
Parlamentním	parlamentní	k2eAgNnSc6d1	parlamentní
shromáždění	shromáždění	k1gNnSc6	shromáždění
NATO	NATO	kA	NATO
či	či	k8xC	či
Rady	rada	k1gFnSc2	rada
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Článek	článek	k1gInSc1	článek
32	[number]	k4	32
zakotvuje	zakotvovat	k5eAaImIp3nS	zakotvovat
omezení	omezení	k1gNnSc4	omezení
současného	současný	k2eAgInSc2d1	současný
výkonu	výkon	k1gInSc2	výkon
členství	členství	k1gNnSc2	členství
ve	v	k7c6	v
vládě	vláda	k1gFnSc6	vláda
a	a	k8xC	a
v	v	k7c6	v
Parlamentu	parlament	k1gInSc6	parlament
<g/>
,	,	kIx,	,
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
stanovena	stanovit	k5eAaPmNgFnS	stanovit
neslučitelnost	neslučitelnost	k1gFnSc1	neslučitelnost
vládní	vládní	k2eAgFnSc2d1	vládní
funkce	funkce	k1gFnSc2	funkce
s	s	k7c7	s
funkcí	funkce	k1gFnSc7	funkce
funkcionáře	funkcionář	k1gMnSc2	funkcionář
komory	komora	k1gFnSc2	komora
Parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Článek	článek	k1gInSc1	článek
33	[number]	k4	33
upravuje	upravovat	k5eAaImIp3nS	upravovat
tzv.	tzv.	kA	tzv.
náhradní	náhradní	k2eAgNnSc1d1	náhradní
či	či	k8xC	či
nouzové	nouzový	k2eAgNnSc1d1	nouzové
zákonodárství	zákonodárství	k1gNnSc1	zákonodárství
<g/>
:	:	kIx,	:
podle	podle	k7c2	podle
tohoto	tento	k3xDgNnSc2	tento
ustanovení	ustanovení	k1gNnSc2	ustanovení
může	moct	k5eAaImIp3nS	moct
Senát	senát	k1gInSc1	senát
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
rozpuštěna	rozpuštěn	k2eAgFnSc1d1	rozpuštěna
Poslanecká	poslanecký	k2eAgFnSc1d1	Poslanecká
sněmovna	sněmovna	k1gFnSc1	sněmovna
ve	v	k7c6	v
věcech	věc	k1gFnPc6	věc
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
nesnesou	snést	k5eNaPmIp3nP	snést
odkladu	odklad	k1gInSc3	odklad
a	a	k8xC	a
vyžadovaly	vyžadovat	k5eAaImAgInP	vyžadovat
by	by	kYmCp3nS	by
jinak	jinak	k6eAd1	jinak
přijetí	přijetí	k1gNnSc4	přijetí
zákona	zákon	k1gInSc2	zákon
<g/>
,	,	kIx,	,
vydat	vydat	k5eAaPmF	vydat
zákonné	zákonný	k2eAgNnSc4d1	zákonné
opatření	opatření	k1gNnSc4	opatření
<g/>
.	.	kIx.	.
</s>
<s>
Zákonná	zákonný	k2eAgNnPc1d1	zákonné
opatření	opatření	k1gNnPc1	opatření
jsou	být	k5eAaImIp3nP	být
ústavně	ústavně	k6eAd1	ústavně
vyloučena	vyloučit	k5eAaPmNgFnS	vyloučit
ve	v	k7c6	v
věcech	věc	k1gFnPc6	věc
Ústavy	ústava	k1gFnSc2	ústava
<g/>
,	,	kIx,	,
státního	státní	k2eAgInSc2d1	státní
rozpočtu	rozpočet	k1gInSc2	rozpočet
<g/>
,	,	kIx,	,
státního	státní	k2eAgInSc2d1	státní
závěrečného	závěrečný	k2eAgInSc2d1	závěrečný
účtu	účet	k1gInSc2	účet
<g/>
,	,	kIx,	,
volebního	volební	k2eAgInSc2d1	volební
zákona	zákon	k1gInSc2	zákon
a	a	k8xC	a
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
smluv	smlouva	k1gFnPc2	smlouva
dle	dle	k7c2	dle
článku	článek	k1gInSc2	článek
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
<s>
Nepanuje	panovat	k5eNaImIp3nS	panovat
teoretická	teoretický	k2eAgFnSc1d1	teoretická
shoda	shoda	k1gFnSc1	shoda
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
nemožnost	nemožnost	k1gFnSc1	nemožnost
přijímání	přijímání	k1gNnSc2	přijímání
zákonných	zákonný	k2eAgNnPc2d1	zákonné
opatření	opatření	k1gNnPc2	opatření
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
jen	jen	k9	jen
k	k	k7c3	k
Ústavě	ústava	k1gFnSc3	ústava
či	či	k8xC	či
k	k	k7c3	k
ústavnímu	ústavní	k2eAgInSc3d1	ústavní
pořádku	pořádek	k1gInSc3	pořádek
<g/>
.	.	kIx.	.
</s>
<s>
Zákonné	zákonný	k2eAgNnSc1d1	zákonné
opatření	opatření	k1gNnSc1	opatření
může	moct	k5eAaImIp3nS	moct
Senátu	senát	k1gInSc2	senát
navrhnout	navrhnout	k5eAaPmF	navrhnout
jedině	jedině	k6eAd1	jedině
vláda	vláda	k1gFnSc1	vláda
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
upraveno	upravit	k5eAaPmNgNnS	upravit
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
zákonná	zákonný	k2eAgFnSc1d1	zákonná
opatření	opatření	k1gNnSc1	opatření
podepisuje	podepisovat	k5eAaImIp3nS	podepisovat
<g/>
,	,	kIx,	,
prezident	prezident	k1gMnSc1	prezident
nemá	mít	k5eNaImIp3nS	mít
právo	právo	k1gNnSc4	právo
suspenzivního	suspenzivní	k2eAgNnSc2d1	suspenzivní
veta	veto	k1gNnSc2	veto
<g/>
.	.	kIx.	.
</s>
<s>
Zákonná	zákonný	k2eAgNnPc1d1	zákonné
opatření	opatření	k1gNnPc1	opatření
musejí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
ratihabována	ratihabovat	k5eAaBmNgFnS	ratihabovat
Poslaneckou	poslanecký	k2eAgFnSc7d1	Poslanecká
sněmovnou	sněmovna	k1gFnSc7	sněmovna
na	na	k7c6	na
její	její	k3xOp3gFnSc6	její
první	první	k4xOgFnSc6	první
schůzi	schůze	k1gFnSc6	schůze
a	a	k8xC	a
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
neschválí	schválit	k5eNaPmIp3nS	schválit
<g/>
,	,	kIx,	,
pozbývají	pozbývat	k5eAaImIp3nP	pozbývat
platnosti	platnost	k1gFnSc3	platnost
<g/>
.	.	kIx.	.
</s>
<s>
Tohoto	tento	k3xDgInSc2	tento
nástroje	nástroj	k1gInSc2	nástroj
využil	využít	k5eAaPmAgInS	využít
Senát	senát	k1gInSc1	senát
poprvé	poprvé	k6eAd1	poprvé
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
byla	být	k5eAaImAgFnS	být
prezidentem	prezident	k1gMnSc7	prezident
k	k	k7c3	k
28	[number]	k4	28
<g/>
.	.	kIx.	.
srpnu	srpen	k1gInSc6	srpen
2013	[number]	k4	2013
rozpuštěna	rozpuštěn	k2eAgFnSc1d1	rozpuštěna
Poslanecká	poslanecký	k2eAgFnSc1d1	Poslanecká
sněmovna	sněmovna	k1gFnSc1	sněmovna
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
návrh	návrh	k1gInSc4	návrh
vlády	vláda	k1gFnSc2	vláda
přijel	přijet	k5eAaPmAgInS	přijet
zákonná	zákonný	k2eAgNnPc4d1	zákonné
opatření	opatření	k1gNnPc4	opatření
o	o	k7c6	o
dani	daň	k1gFnSc6	daň
z	z	k7c2	z
nabytí	nabytí	k1gNnSc2	nabytí
nemovitých	movitý	k2eNgFnPc2d1	nemovitá
věcí	věc	k1gFnPc2	věc
<g/>
,	,	kIx,	,
veřejných	veřejný	k2eAgFnPc6d1	veřejná
zakázkách	zakázka	k1gFnPc6	zakázka
a	a	k8xC	a
o	o	k7c6	o
zdravotním	zdravotní	k2eAgNnSc6d1	zdravotní
pojištění	pojištění	k1gNnSc6	pojištění
<g/>
.	.	kIx.	.
</s>
<s>
Zasedání	zasedání	k1gNnSc1	zasedání
komor	komora	k1gFnPc2	komora
Parlamentu	parlament	k1gInSc2	parlament
upravuje	upravovat	k5eAaImIp3nS	upravovat
článek	článek	k1gInSc1	článek
34	[number]	k4	34
Ústavy	ústava	k1gFnSc2	ústava
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
něhož	jenž	k3xRgInSc2	jenž
jsou	být	k5eAaImIp3nP	být
zasedání	zasedání	k1gNnSc1	zasedání
komor	komora	k1gFnPc2	komora
stálá	stálý	k2eAgFnSc1d1	stálá
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
zřetelný	zřetelný	k2eAgInSc1d1	zřetelný
rozdíl	rozdíl	k1gInSc1	rozdíl
oproti	oproti	k7c3	oproti
Ústavní	ústavní	k2eAgFnSc3d1	ústavní
listině	listina	k1gFnSc3	listina
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
níž	jenž	k3xRgFnSc2	jenž
byla	být	k5eAaImAgFnS	být
zasedání	zasedání	k1gNnSc6	zasedání
konána	konat	k5eAaImNgFnS	konat
v	v	k7c6	v
určitých	určitý	k2eAgNnPc6d1	určité
obdobích	období	k1gNnPc6	období
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
zahajována	zahajován	k2eAgFnSc1d1	zahajována
a	a	k8xC	a
ukončována	ukončován	k2eAgFnSc1d1	ukončována
aktem	akt	k1gInSc7	akt
presidenta	president	k1gMnSc2	president
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Institut	institut	k1gInSc1	institut
rozpuštění	rozpuštění	k1gNnSc2	rozpuštění
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
je	být	k5eAaImIp3nS	být
upraven	upravit	k5eAaPmNgInS	upravit
v	v	k7c6	v
článku	článek	k1gInSc6	článek
35	[number]	k4	35
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
byl	být	k5eAaImAgInS	být
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
o	o	k7c4	o
institut	institut	k1gInSc4	institut
samorozpuštění	samorozpuštění	k1gNnSc2	samorozpuštění
<g/>
,	,	kIx,	,
když	když	k8xS	když
Ústavní	ústavní	k2eAgInSc1d1	ústavní
soud	soud	k1gInSc1	soud
zrušil	zrušit	k5eAaPmAgMnS	zrušit
svým	svůj	k3xOyFgInSc7	svůj
nálezem	nález	k1gInSc7	nález
ad	ad	k7c4	ad
hoc	hoc	k?	hoc
přijatý	přijatý	k2eAgInSc1d1	přijatý
ústavní	ústavní	k2eAgInSc1d1	ústavní
zákon	zákon	k1gInSc1	zákon
ke	k	k7c3	k
zkrácení	zkrácení	k1gNnSc3	zkrácení
volebního	volební	k2eAgNnSc2d1	volební
období	období	k1gNnSc2	období
<g/>
.	.	kIx.	.
</s>
<s>
Schůze	schůze	k1gFnSc1	schůze
komor	komora	k1gFnPc2	komora
jsou	být	k5eAaImIp3nP	být
veřejné	veřejný	k2eAgInPc1d1	veřejný
(	(	kIx(	(
<g/>
čl	čl	kA	čl
<g/>
.	.	kIx.	.
36	[number]	k4	36
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
zákonem	zákon	k1gInSc7	zákon
toto	tento	k3xDgNnSc4	tento
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vyloučeno	vyloučit	k5eAaPmNgNnS	vyloučit
<g/>
.	.	kIx.	.
</s>
<s>
Společná	společný	k2eAgFnSc1d1	společná
schůze	schůze	k1gFnSc1	schůze
obou	dva	k4xCgFnPc2	dva
komor	komora	k1gFnPc2	komora
Parlamentu	parlament	k1gInSc2	parlament
je	být	k5eAaImIp3nS	být
upravena	upravit	k5eAaPmNgFnS	upravit
v	v	k7c6	v
článku	článek	k1gInSc6	článek
37	[number]	k4	37
<g/>
.	.	kIx.	.
</s>
<s>
Koná	konat	k5eAaImIp3nS	konat
se	se	k3xPyFc4	se
při	při	k7c6	při
složení	složení	k1gNnSc6	složení
slibu	slib	k1gInSc2	slib
nastupujícího	nastupující	k2eAgMnSc2d1	nastupující
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
čl	čl	kA	čl
<g/>
.	.	kIx.	.
59	[number]	k4	59
odst	odsta	k1gFnPc2	odsta
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
je	být	k5eAaImIp3nS	být
však	však	k9	však
vhodné	vhodný	k2eAgNnSc1d1	vhodné
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
poslanci	poslanec	k1gMnPc1	poslanec
a	a	k8xC	a
senátoři	senátor	k1gMnPc1	senátor
sešli	sejít	k5eAaPmAgMnP	sejít
společně	společně	k6eAd1	společně
i	i	k9	i
při	při	k7c6	při
jiné	jiný	k2eAgFnSc6d1	jiná
příležitosti	příležitost	k1gFnSc6	příležitost
–	–	k?	–
kupř	kupř	kA	kupř
<g/>
.	.	kIx.	.
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
vystoupení	vystoupení	k1gNnSc2	vystoupení
hlavy	hlava	k1gFnSc2	hlava
státu	stát	k1gInSc2	stát
či	či	k8xC	či
zahraničního	zahraniční	k2eAgMnSc2d1	zahraniční
státníka	státník	k1gMnSc2	státník
–	–	k?	–
a	a	k8xC	a
proto	proto	k8xC	proto
připadají	připadat	k5eAaPmIp3nP	připadat
v	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
společná	společný	k2eAgNnPc1d1	společné
jednání	jednání	k1gNnPc1	jednání
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnPc1	jenž
však	však	k9	však
nemohou	moct	k5eNaImIp3nP	moct
v	v	k7c6	v
žádném	žádný	k3yNgInSc6	žádný
případě	případ	k1gInSc6	případ
vyústit	vyústit	k5eAaPmF	vyústit
v	v	k7c4	v
autoritativní	autoritativní	k2eAgInSc4d1	autoritativní
projev	projev	k1gInSc4	projev
vůle	vůle	k1gFnSc2	vůle
<g/>
.	.	kIx.	.
</s>
<s>
Společnou	společný	k2eAgFnSc4d1	společná
schůzi	schůze	k1gFnSc4	schůze
svolává	svolávat	k5eAaImIp3nS	svolávat
předseda	předseda	k1gMnSc1	předseda
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
a	a	k8xC	a
pro	pro	k7c4	pro
její	její	k3xOp3gNnSc4	její
jednání	jednání	k1gNnSc4	jednání
platí	platit	k5eAaImIp3nS	platit
jednací	jednací	k2eAgInSc1d1	jednací
řád	řád	k1gInSc1	řád
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
.	.	kIx.	.
</s>
<s>
Právo	právo	k1gNnSc1	právo
i	i	k8xC	i
povinnost	povinnost	k1gFnSc1	povinnost
člena	člen	k1gMnSc2	člen
vlády	vláda	k1gFnSc2	vláda
účastnit	účastnit	k5eAaImF	účastnit
se	s	k7c7	s
schůzí	schůze	k1gFnSc7	schůze
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
,	,	kIx,	,
vycházející	vycházející	k2eAgFnSc2d1	vycházející
ze	z	k7c2	z
vztahu	vztah	k1gInSc2	vztah
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
Sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
upraveno	upravit	k5eAaPmNgNnS	upravit
v	v	k7c6	v
článku	článek	k1gInSc6	článek
38	[number]	k4	38
Ústavy	ústava	k1gFnSc2	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Způsobilost	způsobilost	k1gFnSc1	způsobilost
komor	komora	k1gFnPc2	komora
parlamentu	parlament	k1gInSc2	parlament
k	k	k7c3	k
přijímání	přijímání	k1gNnSc3	přijímání
jejich	jejich	k3xOp3gNnSc2	jejich
usnesení	usnesení	k1gNnSc2	usnesení
je	být	k5eAaImIp3nS	být
upravena	upravit	k5eAaPmNgFnS	upravit
v	v	k7c6	v
ustanovení	ustanovení	k1gNnSc6	ustanovení
článku	článek	k1gInSc2	článek
39	[number]	k4	39
Ústavy	ústava	k1gFnSc2	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
vymezeno	vymezit	k5eAaPmNgNnS	vymezit
kvórum	kvórum	k1gNnSc1	kvórum
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
nejmenší	malý	k2eAgInSc1d3	nejmenší
počet	počet	k1gInSc1	počet
členů	člen	k1gInPc2	člen
komory	komora	k1gFnSc2	komora
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
musejí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
přítomni	přítomen	k2eAgMnPc1d1	přítomen
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
komora	komora	k1gFnSc1	komora
usnášeníschopná	usnášeníschopný	k2eAgFnSc1d1	usnášeníschopná
<g/>
.	.	kIx.	.
</s>
<s>
Komory	komora	k1gFnPc1	komora
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
usnášet	usnášet	k5eAaImF	usnášet
za	za	k7c2	za
přítomnosti	přítomnost	k1gFnSc2	přítomnost
alespoň	alespoň	k9	alespoň
jedné	jeden	k4xCgFnSc2	jeden
třetiny	třetina	k1gFnSc2	třetina
členů	člen	k1gInPc2	člen
komory	komora	k1gFnSc2	komora
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
toliko	toliko	k6eAd1	toliko
jednat	jednat	k5eAaImF	jednat
může	moct	k5eAaImIp3nS	moct
komora	komora	k1gFnSc1	komora
i	i	k9	i
za	za	k7c4	za
situace	situace	k1gFnPc4	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
počet	počet	k1gInSc1	počet
přítomných	přítomný	k2eAgMnPc2d1	přítomný
nedosahuje	dosahovat	k5eNaImIp3nS	dosahovat
jedné	jeden	k4xCgFnSc2	jeden
třetiny	třetina	k1gFnSc2	třetina
<g/>
,	,	kIx,	,
přítomen	přítomen	k2eAgMnSc1d1	přítomen
však	však	k9	však
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
alespoň	alespoň	k9	alespoň
jeden	jeden	k4xCgMnSc1	jeden
člen	člen	k1gMnSc1	člen
komory	komora	k1gFnSc2	komora
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
řídí	řídit	k5eAaImIp3nS	řídit
schůzi	schůze	k1gFnSc4	schůze
<g/>
.	.	kIx.	.
</s>
<s>
Odstavec	odstavec	k1gInSc1	odstavec
2	[number]	k4	2
článku	článek	k1gInSc2	článek
39	[number]	k4	39
upravuje	upravovat	k5eAaImIp3nS	upravovat
prostou	prostý	k2eAgFnSc4d1	prostá
většinu	většina	k1gFnSc4	většina
nutnou	nutný	k2eAgFnSc4d1	nutná
pro	pro	k7c4	pro
přijetí	přijetí	k1gNnSc4	přijetí
usnesení	usnesení	k1gNnSc2	usnesení
komory	komora	k1gFnSc2	komora
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgInSc1	třetí
odstavec	odstavec	k1gInSc1	odstavec
pak	pak	k6eAd1	pak
vymezuje	vymezovat	k5eAaImIp3nS	vymezovat
většinu	většina	k1gFnSc4	většina
v	v	k7c6	v
případech	případ	k1gInPc6	případ
usnesení	usnesení	k1gNnSc2	usnesení
o	o	k7c4	o
vyhlášení	vyhlášení	k1gNnSc4	vyhlášení
válečného	válečný	k2eAgInSc2d1	válečný
stavu	stav	k1gInSc2	stav
<g/>
,	,	kIx,	,
k	k	k7c3	k
přijetí	přijetí	k1gNnSc3	přijetí
usnesení	usnesení	k1gNnSc2	usnesení
o	o	k7c6	o
souhlasu	souhlas	k1gInSc6	souhlas
s	s	k7c7	s
vysláním	vyslání	k1gNnSc7	vyslání
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
ČR	ČR	kA	ČR
mimo	mimo	k7c4	mimo
území	území	k1gNnSc4	území
republiky	republika	k1gFnSc2	republika
nebo	nebo	k8xC	nebo
s	s	k7c7	s
pobytem	pobyt	k1gInSc7	pobyt
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
jiných	jiný	k2eAgInPc2d1	jiný
států	stát	k1gInPc2	stát
na	na	k7c6	na
území	území	k1gNnSc6	území
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
k	k	k7c3	k
přijetí	přijetí	k1gNnSc3	přijetí
usnesení	usnesení	k1gNnSc2	usnesení
o	o	k7c6	o
účasti	účast	k1gFnSc6	účast
ČR	ČR	kA	ČR
v	v	k7c6	v
obranných	obranný	k2eAgInPc6d1	obranný
systémech	systém	k1gInPc6	systém
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
organizace	organizace	k1gFnSc2	organizace
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
je	být	k5eAaImIp3nS	být
republika	republika	k1gFnSc1	republika
členem	člen	k1gInSc7	člen
–	–	k?	–
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
případech	případ	k1gInPc6	případ
je	být	k5eAaImIp3nS	být
potřebná	potřebný	k2eAgFnSc1d1	potřebná
absolutní	absolutní	k2eAgFnSc1d1	absolutní
většina	většina	k1gFnSc1	většina
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
nejméně	málo	k6eAd3	málo
101	[number]	k4	101
hlasů	hlas	k1gInPc2	hlas
poslanců	poslanec	k1gMnPc2	poslanec
a	a	k8xC	a
41	[number]	k4	41
senátorů	senátor	k1gMnPc2	senátor
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrtý	čtvrtý	k4xOgInSc1	čtvrtý
odstavec	odstavec	k1gInSc1	odstavec
pak	pak	k6eAd1	pak
upravuje	upravovat	k5eAaImIp3nS	upravovat
kvalifikovanou	kvalifikovaný	k2eAgFnSc4d1	kvalifikovaná
většinu	většina	k1gFnSc4	většina
nutnou	nutný	k2eAgFnSc4d1	nutná
k	k	k7c3	k
přijetí	přijetí	k1gNnSc3	přijetí
ústavního	ústavní	k2eAgInSc2d1	ústavní
zákona	zákon	k1gInSc2	zákon
a	a	k8xC	a
souhlasu	souhlas	k1gInSc2	souhlas
k	k	k7c3	k
ratifikaci	ratifikace	k1gFnSc3	ratifikace
mezinárodní	mezinárodní	k2eAgFnPc4d1	mezinárodní
smlouvy	smlouva	k1gFnPc4	smlouva
uvedené	uvedený	k2eAgFnPc4d1	uvedená
v	v	k7c6	v
čl	čl	kA	čl
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
a	a	k8xC	a
odst	odst	k1gMnSc1	odst
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
potřebná	potřebný	k2eAgFnSc1d1	potřebná
třípětinová	třípětinový	k2eAgFnSc1d1	třípětinová
většina	většina	k1gFnSc1	většina
všech	všecek	k3xTgMnPc2	všecek
poslanců	poslanec	k1gMnPc2	poslanec
(	(	kIx(	(
<g/>
120	[number]	k4	120
<g/>
)	)	kIx)	)
a	a	k8xC	a
třípětinová	třípětinový	k2eAgFnSc1d1	třípětinová
většina	většina	k1gFnSc1	většina
přítomných	přítomný	k2eAgMnPc2d1	přítomný
senátorů	senátor	k1gMnPc2	senátor
(	(	kIx(	(
<g/>
49	[number]	k4	49
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
při	při	k7c6	při
minimální	minimální	k2eAgFnSc6d1	minimální
prezenci	prezence	k1gFnSc6	prezence
ovšem	ovšem	k9	ovšem
pouhých	pouhý	k2eAgInPc2d1	pouhý
17	[number]	k4	17
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Články	článek	k1gInPc1	článek
40	[number]	k4	40
až	až	k8xS	až
52	[number]	k4	52
upravují	upravovat	k5eAaImIp3nP	upravovat
zákonodárný	zákonodárný	k2eAgInSc4d1	zákonodárný
proces	proces	k1gInSc4	proces
<g/>
:	:	kIx,	:
čl	čl	kA	čl
<g/>
.	.	kIx.	.
40	[number]	k4	40
podmínky	podmínka	k1gFnSc2	podmínka
přijímání	přijímání	k1gNnSc2	přijímání
určitých	určitý	k2eAgInPc2d1	určitý
zákonů	zákon	k1gInPc2	zákon
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
vyžadováno	vyžadován	k2eAgNnSc1d1	vyžadováno
schválení	schválení	k1gNnSc1	schválení
oběma	dva	k4xCgFnPc7	dva
komorami	komora	k1gFnPc7	komora
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
např.	např.	kA	např.
zmíněn	zmíněn	k2eAgInSc1d1	zmíněn
doposud	doposud	k6eAd1	doposud
nepřijatý	přijatý	k2eNgInSc1d1	nepřijatý
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
zásadách	zásada	k1gFnPc6	zásada
jednání	jednání	k1gNnSc2	jednání
a	a	k8xC	a
styku	styk	k1gInSc2	styk
obou	dva	k4xCgFnPc2	dva
komor	komora	k1gFnPc2	komora
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
navenek	navenek	k6eAd1	navenek
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
stykový	stykový	k2eAgInSc1d1	stykový
zákon	zákon	k1gInSc1	zákon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čl	čl	kA	čl
<g/>
.	.	kIx.	.
41	[number]	k4	41
zákonodárnou	zákonodárný	k2eAgFnSc4d1	zákonodárná
iniciativu	iniciativa	k1gFnSc4	iniciativa
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
přísluší	příslušet	k5eAaImIp3nS	příslušet
jednotlivému	jednotlivý	k2eAgMnSc3d1	jednotlivý
poslanci	poslanec	k1gMnSc3	poslanec
<g/>
,	,	kIx,	,
skupině	skupina	k1gFnSc3	skupina
poslanců	poslanec	k1gMnPc2	poslanec
<g/>
,	,	kIx,	,
Senátu	senát	k1gInSc2	senát
jako	jako	k8xS	jako
celku	celek	k1gInSc2	celek
<g/>
,	,	kIx,	,
vládě	vláda	k1gFnSc6	vláda
a	a	k8xC	a
zastupitelstvu	zastupitelstvo	k1gNnSc6	zastupitelstvo
vyššího	vysoký	k2eAgInSc2d2	vyšší
územního	územní	k2eAgInSc2d1	územní
samosprávného	samosprávný	k2eAgInSc2d1	samosprávný
celku	celek	k1gInSc2	celek
<g/>
.	.	kIx.	.
</s>
<s>
Článek	článek	k1gInSc1	článek
42	[number]	k4	42
určuje	určovat	k5eAaImIp3nS	určovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
návrh	návrh	k1gInSc1	návrh
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
státním	státní	k2eAgInSc6d1	státní
rozpočtu	rozpočet	k1gInSc6	rozpočet
a	a	k8xC	a
návrh	návrh	k1gInSc4	návrh
státního	státní	k2eAgInSc2d1	státní
závěrečného	závěrečný	k2eAgInSc2d1	závěrečný
účtu	účet	k1gInSc2	účet
podává	podávat	k5eAaImIp3nS	podávat
vláda	vláda	k1gFnSc1	vláda
a	a	k8xC	a
tyto	tento	k3xDgInPc4	tento
návrhy	návrh	k1gInPc4	návrh
projednává	projednávat	k5eAaImIp3nS	projednávat
a	a	k8xC	a
usnáší	usnášet	k5eAaImIp3nS	usnášet
se	se	k3xPyFc4	se
o	o	k7c6	o
nich	on	k3xPp3gMnPc6	on
jen	jen	k9	jen
Sněmovna	sněmovna	k1gFnSc1	sněmovna
<g/>
,	,	kIx,	,
čl	čl	kA	čl
<g/>
.	.	kIx.	.
43	[number]	k4	43
upravuje	upravovat	k5eAaImIp3nS	upravovat
otázky	otázka	k1gFnPc4	otázka
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
čl	čl	kA	čl
<g/>
.	.	kIx.	.
44	[number]	k4	44
zakotvuje	zakotvovat	k5eAaImIp3nS	zakotvovat
právo	právo	k1gNnSc4	právo
vlády	vláda	k1gFnSc2	vláda
v	v	k7c6	v
zákonodárném	zákonodárný	k2eAgInSc6d1	zákonodárný
procesu	proces	k1gInSc6	proces
<g/>
,	,	kIx,	,
úprava	úprava	k1gFnSc1	úprava
postupu	postup	k1gInSc2	postup
schváleného	schválený	k2eAgInSc2d1	schválený
návrhu	návrh	k1gInSc2	návrh
zákona	zákon	k1gInSc2	zákon
mezi	mezi	k7c7	mezi
komorami	komora	k1gFnPc7	komora
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
článku	článek	k1gInSc6	článek
45	[number]	k4	45
<g/>
.	.	kIx.	.
</s>
<s>
Projednání	projednání	k1gNnSc1	projednání
Senátem	senát	k1gInSc7	senát
je	být	k5eAaImIp3nS	být
řešeno	řešit	k5eAaImNgNnS	řešit
v	v	k7c6	v
článku	článek	k1gInSc6	článek
46	[number]	k4	46
<g/>
,	,	kIx,	,
článek	článek	k1gInSc1	článek
47	[number]	k4	47
pak	pak	k6eAd1	pak
řeší	řešit	k5eAaImIp3nS	řešit
opakované	opakovaný	k2eAgNnSc1d1	opakované
projednání	projednání	k1gNnSc1	projednání
návrhu	návrh	k1gInSc2	návrh
Poslaneckou	poslanecký	k2eAgFnSc7d1	Poslanecká
sněmovnou	sněmovna	k1gFnSc7	sněmovna
<g/>
,	,	kIx,	,
když	když	k8xS	když
byl	být	k5eAaImAgInS	být
návrh	návrh	k1gInSc1	návrh
zákona	zákon	k1gInSc2	zákon
zamítnut	zamítnout	k5eAaPmNgInS	zamítnout
nebo	nebo	k8xC	nebo
vrácen	vrátit	k5eAaPmNgInS	vrátit
Senátem	senát	k1gInSc7	senát
<g/>
.	.	kIx.	.
</s>
<s>
Čl	čl	kA	čl
<g/>
.	.	kIx.	.
48	[number]	k4	48
zakotvuje	zakotvovat	k5eAaImIp3nS	zakotvovat
institut	institut	k1gInSc4	institut
vyjádření	vyjádření	k1gNnSc2	vyjádření
vůle	vůle	k1gFnSc2	vůle
Senátu	senát	k1gInSc2	senát
nezabývat	zabývat	k5eNaImF	zabývat
se	se	k3xPyFc4	se
návrhem	návrh	k1gInSc7	návrh
zákona	zákon	k1gInSc2	zákon
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
byl	být	k5eAaImAgInS	být
schválen	schválit	k5eAaPmNgInS	schválit
Sněmovnou	sněmovna	k1gFnSc7	sněmovna
<g/>
.	.	kIx.	.
</s>
<s>
Ratifikace	ratifikace	k1gFnSc1	ratifikace
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
smluv	smlouva	k1gFnPc2	smlouva
je	být	k5eAaImIp3nS	být
upravena	upravit	k5eAaPmNgFnS	upravit
v	v	k7c6	v
článku	článek	k1gInSc6	článek
49	[number]	k4	49
<g/>
,	,	kIx,	,
právo	právo	k1gNnSc4	právo
suspenzivního	suspenzivní	k2eAgNnSc2d1	suspenzivní
veta	veto	k1gNnSc2	veto
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
článku	článek	k1gInSc6	článek
50	[number]	k4	50
<g/>
.	.	kIx.	.
</s>
<s>
Podepisování	podepisování	k1gNnSc1	podepisování
zákonů	zákon	k1gInPc2	zákon
upravuje	upravovat	k5eAaImIp3nS	upravovat
článek	článek	k1gInSc4	článek
51	[number]	k4	51
a	a	k8xC	a
vyhlášení	vyhlášení	k1gNnSc4	vyhlášení
zákonů	zákon	k1gInPc2	zákon
a	a	k8xC	a
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
smluv	smlouva	k1gFnPc2	smlouva
článek	článek	k1gInSc1	článek
52	[number]	k4	52
<g/>
.	.	kIx.	.
</s>
<s>
Právo	právo	k1gNnSc1	právo
interpelace	interpelace	k1gFnSc2	interpelace
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
právo	právo	k1gNnSc4	právo
poslanců	poslanec	k1gMnPc2	poslanec
klást	klást	k5eAaImF	klást
členům	člen	k1gMnPc3	člen
vlády	vláda	k1gFnSc2	vláda
dotazy	dotaz	k1gInPc1	dotaz
se	s	k7c7	s
žádostí	žádost	k1gFnSc7	žádost
o	o	k7c4	o
vysvětlení	vysvětlení	k1gNnSc4	vysvětlení
<g/>
,	,	kIx,	,
spojené	spojený	k2eAgInPc4d1	spojený
s	s	k7c7	s
povinností	povinnost	k1gFnSc7	povinnost
členů	člen	k1gMnPc2	člen
vlády	vláda	k1gFnSc2	vláda
odpovědět	odpovědět	k5eAaPmF	odpovědět
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zakotveno	zakotven	k2eAgNnSc1d1	zakotveno
v	v	k7c6	v
článku	článek	k1gInSc6	článek
53	[number]	k4	53
Ústavy	ústava	k1gFnSc2	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Ústní	ústní	k2eAgFnPc1d1	ústní
i	i	k8xC	i
písemné	písemný	k2eAgFnPc1d1	písemná
interpelace	interpelace	k1gFnPc1	interpelace
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
jednacího	jednací	k2eAgInSc2d1	jednací
řádu	řád	k1gInSc2	řád
konají	konat	k5eAaImIp3nP	konat
ve	v	k7c4	v
čtvrtek	čtvrtek	k1gInSc4	čtvrtek
<g/>
.	.	kIx.	.
</s>
<s>
Ústava	ústava	k1gFnSc1	ústava
nedefinuje	definovat	k5eNaBmIp3nS	definovat
pojem	pojem	k1gInSc4	pojem
výkonné	výkonný	k2eAgFnSc2d1	výkonná
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gInSc4	jeho
obsah	obsah	k1gInSc4	obsah
nutné	nutný	k2eAgNnSc1d1	nutné
odvodit	odvodit	k5eAaPmF	odvodit
z	z	k7c2	z
jednotlivých	jednotlivý	k2eAgNnPc2d1	jednotlivé
ustanovení	ustanovení	k1gNnPc2	ustanovení
hlavy	hlava	k1gFnSc2	hlava
třetí	třetí	k4xOgFnSc2	třetí
Ústavy	ústava	k1gFnSc2	ústava
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
moci	moc	k1gFnSc2	moc
výkonné	výkonný	k2eAgInPc1d1	výkonný
zařazen	zařazen	k2eAgMnSc1d1	zařazen
prezident	prezident	k1gMnSc1	prezident
(	(	kIx(	(
<g/>
čl	čl	kA	čl
<g/>
.	.	kIx.	.
54	[number]	k4	54
až	až	k9	až
66	[number]	k4	66
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vláda	vláda	k1gFnSc1	vláda
(	(	kIx(	(
<g/>
čl	čl	kA	čl
<g/>
.	.	kIx.	.
67	[number]	k4	67
až	až	k9	až
78	[number]	k4	78
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ministerstva	ministerstvo	k1gNnPc1	ministerstvo
a	a	k8xC	a
jiné	jiný	k2eAgInPc1d1	jiný
správní	správní	k2eAgInPc1d1	správní
úřady	úřad	k1gInPc1	úřad
(	(	kIx(	(
<g/>
čl	čl	kA	čl
<g/>
.	.	kIx.	.
79	[number]	k4	79
<g/>
)	)	kIx)	)
a	a	k8xC	a
státní	státní	k2eAgNnSc1d1	státní
zastupitelství	zastupitelství	k1gNnSc1	zastupitelství
(	(	kIx(	(
<g/>
čl	čl	kA	čl
<g/>
.	.	kIx.	.
80	[number]	k4	80
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
republiky	republika	k1gFnSc2	republika
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
dikce	dikce	k1gFnSc2	dikce
čl	čl	kA	čl
<g/>
.	.	kIx.	.
54	[number]	k4	54
odst	odsta	k1gFnPc2	odsta
<g/>
.	.	kIx.	.
1	[number]	k4	1
Ústavy	ústav	k1gInPc7	ústav
hlavou	hlava	k1gFnSc7	hlava
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
volena	volit	k5eAaImNgFnS	volit
v	v	k7c6	v
přímých	přímý	k2eAgFnPc6d1	přímá
volbách	volba	k1gFnPc6	volba
(	(	kIx(	(
<g/>
odst	odsta	k1gFnPc2	odsta
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
republiky	republika	k1gFnSc2	republika
není	být	k5eNaImIp3nS	být
z	z	k7c2	z
dikce	dikce	k1gFnSc2	dikce
Ústavy	ústava	k1gFnSc2	ústava
z	z	k7c2	z
výkonu	výkon	k1gInSc2	výkon
své	svůj	k3xOyFgFnSc2	svůj
funkce	funkce	k1gFnSc2	funkce
odpovědný	odpovědný	k2eAgMnSc1d1	odpovědný
<g/>
,	,	kIx,	,
výjimku	výjimka	k1gFnSc4	výjimka
tvoří	tvořit	k5eAaImIp3nS	tvořit
možnost	možnost	k1gFnSc1	možnost
stíhat	stíhat	k5eAaImF	stíhat
prezidenta	prezident	k1gMnSc4	prezident
pro	pro	k7c4	pro
velezradu	velezrada	k1gFnSc4	velezrada
(	(	kIx(	(
<g/>
čl	čl	kA	čl
<g/>
.	.	kIx.	.
65	[number]	k4	65
odst	odsta	k1gFnPc2	odsta
<g/>
.	.	kIx.	.
2	[number]	k4	2
Ústavy	ústava	k1gFnSc2	ústava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Neodpovědnost	neodpovědnost	k1gFnSc1	neodpovědnost
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
pouze	pouze	k6eAd1	pouze
výkonu	výkon	k1gInSc3	výkon
funkce	funkce	k1gFnSc2	funkce
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
není	být	k5eNaImIp3nS	být
vyloučeno	vyloučen	k2eAgNnSc1d1	vyloučeno
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgMnS	být
prezident	prezident	k1gMnSc1	prezident
hnán	hnán	k2eAgMnSc1d1	hnán
k	k	k7c3	k
odpovědnosti	odpovědnost	k1gFnSc3	odpovědnost
v	v	k7c6	v
majetkových	majetkový	k2eAgFnPc6d1	majetková
<g/>
,	,	kIx,	,
rodinněprávních	rodinněprávní	k2eAgFnPc6d1	rodinněprávní
či	či	k8xC	či
osobnostních	osobnostní	k2eAgFnPc6d1	osobnostní
záležitostech	záležitost	k1gFnPc6	záležitost
<g/>
.	.	kIx.	.
</s>
<s>
Problematické	problematický	k2eAgNnSc1d1	problematické
však	však	k9	však
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
případech	případ	k1gInPc6	případ
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
odlišení	odlišení	k1gNnSc1	odlišení
výkonu	výkon	k1gInSc2	výkon
a	a	k8xC	a
"	"	kIx"	"
<g/>
nevýkonu	nevýkon	k1gInSc6	nevýkon
<g/>
"	"	kIx"	"
funkce	funkce	k1gFnSc1	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
republiky	republika	k1gFnSc2	republika
se	se	k3xPyFc4	se
z	z	k7c2	z
dikce	dikce	k1gFnSc2	dikce
čl	čl	kA	čl
<g/>
.	.	kIx.	.
55	[number]	k4	55
Ústavy	ústav	k1gInPc4	ústav
ujímá	ujímat	k5eAaImIp3nS	ujímat
úřadu	úřad	k1gInSc2	úřad
složením	složení	k1gNnSc7	složení
slibu	slib	k1gInSc2	slib
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc1	jehož
znění	znění	k1gNnSc1	znění
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
článku	článek	k1gInSc6	článek
59	[number]	k4	59
odst	odstum	k1gNnPc2	odstum
<g/>
.	.	kIx.	.
2	[number]	k4	2
a	a	k8xC	a
který	který	k3yQgMnSc1	který
skládá	skládat	k5eAaImIp3nS	skládat
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
předsedy	předseda	k1gMnSc2	předseda
Senátu	senát	k1gInSc2	senát
na	na	k7c6	na
společné	společný	k2eAgFnSc6d1	společná
schůzi	schůze	k1gFnSc6	schůze
obou	dva	k4xCgFnPc2	dva
komor	komora	k1gFnPc2	komora
(	(	kIx(	(
<g/>
čl	čl	kA	čl
<g/>
.	.	kIx.	.
59	[number]	k4	59
odst	odsta	k1gFnPc2	odsta
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Volební	volební	k2eAgNnSc1d1	volební
období	období	k1gNnSc1	období
prezidenta	prezident	k1gMnSc2	prezident
trvá	trvat	k5eAaImIp3nS	trvat
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
a	a	k8xC	a
začíná	začínat	k5eAaImIp3nS	začínat
dnem	den	k1gInSc7	den
složení	složení	k1gNnSc2	složení
slibu	slib	k1gInSc2	slib
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
prezident	prezident	k1gMnSc1	prezident
odmítne	odmítnout	k5eAaPmIp3nS	odmítnout
složit	složit	k5eAaPmF	složit
slib	slib	k1gInSc4	slib
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
pokud	pokud	k8xS	pokud
složí	složit	k5eAaPmIp3nS	složit
slib	slib	k1gInSc4	slib
s	s	k7c7	s
výhradou	výhrada	k1gFnSc7	výhrada
<g/>
,	,	kIx,	,
pohlíží	pohlížet	k5eAaImIp3nS	pohlížet
se	se	k3xPyFc4	se
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
dle	dle	k7c2	dle
článku	článek	k1gInSc2	článek
60	[number]	k4	60
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
by	by	kYmCp3nS	by
nebyl	být	k5eNaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
<g/>
.	.	kIx.	.
</s>
<s>
Volba	volba	k1gFnSc1	volba
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
se	se	k3xPyFc4	se
po	po	k7c6	po
zavedení	zavedení	k1gNnSc6	zavedení
přímé	přímý	k2eAgFnSc2d1	přímá
volby	volba	k1gFnSc2	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
koná	konat	k5eAaImIp3nS	konat
tajným	tajný	k2eAgNnSc7d1	tajné
hlasováním	hlasování	k1gNnSc7	hlasování
na	na	k7c6	na
základě	základ	k1gInSc6	základ
všeobecného	všeobecný	k2eAgNnSc2d1	všeobecné
<g/>
,	,	kIx,	,
rovného	rovný	k2eAgNnSc2d1	rovné
a	a	k8xC	a
přímého	přímý	k2eAgNnSc2d1	přímé
volebního	volební	k2eAgNnSc2d1	volební
práva	právo	k1gNnSc2	právo
(	(	kIx(	(
<g/>
čl	čl	kA	čl
<g/>
.	.	kIx.	.
54	[number]	k4	54
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
otázce	otázka	k1gFnSc6	otázka
podmínek	podmínka	k1gFnPc2	podmínka
výkonu	výkon	k1gInSc2	výkon
volebního	volební	k2eAgNnSc2d1	volební
práva	právo	k1gNnSc2	právo
<g/>
,	,	kIx,	,
navrhování	navrhování	k1gNnSc1	navrhování
kandidátů	kandidát	k1gMnPc2	kandidát
<g/>
,	,	kIx,	,
vyhlašování	vyhlašování	k1gNnSc4	vyhlašování
a	a	k8xC	a
provádění	provádění	k1gNnSc4	provádění
volby	volba	k1gFnSc2	volba
a	a	k8xC	a
v	v	k7c6	v
otázce	otázka	k1gFnSc6	otázka
soudního	soudní	k2eAgInSc2d1	soudní
přezkumu	přezkum	k1gInSc2	přezkum
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
ústava	ústava	k1gFnSc1	ústava
na	na	k7c4	na
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
zákon	zákon	k1gInSc4	zákon
(	(	kIx(	(
<g/>
čl	čl	kA	čl
<g/>
.	.	kIx.	.
58	[number]	k4	58
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ústava	ústava	k1gFnSc1	ústava
však	však	k9	však
v	v	k7c6	v
čl	čl	kA	čl
<g/>
.	.	kIx.	.
56	[number]	k4	56
určuje	určovat	k5eAaImIp3nS	určovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
prezidentem	prezident	k1gMnSc7	prezident
republiky	republika	k1gFnSc2	republika
je	být	k5eAaImIp3nS	být
zvolen	zvolen	k2eAgMnSc1d1	zvolen
kandidát	kandidát	k1gMnSc1	kandidát
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
obdržel	obdržet	k5eAaPmAgMnS	obdržet
nadpoloviční	nadpoloviční	k2eAgFnSc4d1	nadpoloviční
většinu	většina	k1gFnSc4	většina
platných	platný	k2eAgInPc2d1	platný
hlasů	hlas	k1gInPc2	hlas
oprávněných	oprávněný	k2eAgMnPc2d1	oprávněný
voličů	volič	k1gMnPc2	volič
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
<g/>
-li	i	k?	-li
takový	takový	k3xDgMnSc1	takový
kandidát	kandidát	k1gMnSc1	kandidát
<g/>
,	,	kIx,	,
koná	konat	k5eAaImIp3nS	konat
se	se	k3xPyFc4	se
za	za	k7c4	za
čtrnáct	čtrnáct	k4xCc4	čtrnáct
dnů	den	k1gInPc2	den
po	po	k7c6	po
začátku	začátek	k1gInSc6	začátek
prvního	první	k4xOgNnSc2	první
kola	kolo	k1gNnSc2	kolo
volby	volba	k1gFnPc1	volba
druhé	druhý	k4xOgNnSc4	druhý
kolo	kolo	k1gNnSc4	kolo
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yQgInSc2	který
postupují	postupovat	k5eAaImIp3nP	postupovat
dva	dva	k4xCgMnPc1	dva
nejúspěšnější	úspěšný	k2eAgMnPc1d3	nejúspěšnější
kandidáti	kandidát	k1gMnPc1	kandidát
z	z	k7c2	z
prvního	první	k4xOgNnSc2	první
kola	kolo	k1gNnSc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
rovnosti	rovnost	k1gFnSc6	rovnost
hlasů	hlas	k1gInPc2	hlas
postupují	postupovat	k5eAaImIp3nP	postupovat
do	do	k7c2	do
druhého	druhý	k4xOgNnSc2	druhý
kola	kolo	k1gNnSc2	kolo
volby	volba	k1gFnSc2	volba
všichni	všechen	k3xTgMnPc1	všechen
kandidáti	kandidát	k1gMnPc1	kandidát
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
v	v	k7c6	v
prvním	první	k4xOgNnSc6	první
kole	kolo	k1gNnSc6	kolo
získali	získat	k5eAaPmAgMnP	získat
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
počet	počet	k1gInSc4	počet
platných	platný	k2eAgInPc2d1	platný
hlasů	hlas	k1gInPc2	hlas
oprávněných	oprávněný	k2eAgMnPc2d1	oprávněný
voličů	volič	k1gMnPc2	volič
<g/>
,	,	kIx,	,
a	a	k8xC	a
nejsou	být	k5eNaImIp3nP	být
<g/>
-li	i	k?	-li
takoví	takový	k3xDgMnPc1	takový
kandidáti	kandidát	k1gMnPc1	kandidát
alespoň	alespoň	k9	alespoň
dva	dva	k4xCgInPc4	dva
<g/>
,	,	kIx,	,
postupují	postupovat	k5eAaImIp3nP	postupovat
i	i	k9	i
kandidáti	kandidát	k1gMnPc1	kandidát
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
obdrželi	obdržet	k5eAaPmAgMnP	obdržet
druhý	druhý	k4xOgInSc4	druhý
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
počet	počet	k1gInSc4	počet
platných	platný	k2eAgInPc2d1	platný
hlasů	hlas	k1gInPc2	hlas
oprávněných	oprávněný	k2eAgMnPc2d1	oprávněný
voličů	volič	k1gMnPc2	volič
<g/>
.	.	kIx.	.
</s>
<s>
Prezidentem	prezident	k1gMnSc7	prezident
republiky	republika	k1gFnSc2	republika
je	být	k5eAaImIp3nS	být
zvolen	zvolen	k2eAgMnSc1d1	zvolen
kandidát	kandidát	k1gMnSc1	kandidát
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
obdržel	obdržet	k5eAaPmAgMnS	obdržet
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
kole	kolo	k1gNnSc6	kolo
volby	volba	k1gFnSc2	volba
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
počet	počet	k1gInSc1	počet
platných	platný	k2eAgInPc2d1	platný
hlasů	hlas	k1gInPc2	hlas
oprávněných	oprávněný	k2eAgMnPc2d1	oprávněný
voličů	volič	k1gMnPc2	volič
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
takových	takový	k3xDgMnPc2	takový
kandidátů	kandidát	k1gMnPc2	kandidát
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
prezident	prezident	k1gMnSc1	prezident
republiky	republika	k1gFnSc2	republika
není	být	k5eNaImIp3nS	být
zvolen	zvolen	k2eAgMnSc1d1	zvolen
a	a	k8xC	a
do	do	k7c2	do
deseti	deset	k4xCc2	deset
dnů	den	k1gInPc2	den
se	se	k3xPyFc4	se
vyhlásí	vyhlásit	k5eAaPmIp3nS	vyhlásit
nová	nový	k2eAgFnSc1d1	nová
volba	volba	k1gFnSc1	volba
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Ústava	ústava	k1gFnSc1	ústava
pamatuje	pamatovat	k5eAaImIp3nS	pamatovat
i	i	k9	i
na	na	k7c6	na
situaci	situace	k1gFnSc6	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
by	by	kYmCp3nS	by
kandidát	kandidát	k1gMnSc1	kandidát
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
postoupil	postoupit	k5eAaPmAgMnS	postoupit
do	do	k7c2	do
druhého	druhý	k4xOgNnSc2	druhý
kola	kolo	k1gNnSc2	kolo
volby	volba	k1gFnSc2	volba
<g/>
,	,	kIx,	,
přestal	přestat	k5eAaPmAgMnS	přestat
být	být	k5eAaImF	být
volitelný	volitelný	k2eAgMnSc1d1	volitelný
za	za	k7c4	za
prezidenta	prezident	k1gMnSc4	prezident
republiky	republika	k1gFnSc2	republika
před	před	k7c7	před
druhým	druhý	k4xOgMnSc7	druhý
kolem	kolem	k7c2	kolem
volby	volba	k1gFnSc2	volba
anebo	anebo	k8xC	anebo
se	s	k7c7	s
práva	právo	k1gNnPc4	právo
kandidovat	kandidovat	k5eAaImF	kandidovat
vzdal	vzdát	k5eAaPmAgMnS	vzdát
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
takové	takový	k3xDgFnSc2	takový
situace	situace	k1gFnSc2	situace
<g/>
,	,	kIx,	,
postupuje	postupovat	k5eAaImIp3nS	postupovat
do	do	k7c2	do
druhého	druhý	k4xOgNnSc2	druhý
kola	kolo	k1gNnSc2	kolo
volby	volba	k1gFnSc2	volba
kandidát	kandidát	k1gMnSc1	kandidát
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
prvním	první	k4xOgNnSc6	první
kole	kolo	k1gNnSc6	kolo
volby	volba	k1gFnSc2	volba
získal	získat	k5eAaPmAgInS	získat
další	další	k2eAgInSc1d1	další
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
počet	počet	k1gInSc1	počet
platných	platný	k2eAgInPc2d1	platný
hlasů	hlas	k1gInPc2	hlas
oprávněných	oprávněný	k2eAgMnPc2d1	oprávněný
voličů	volič	k1gMnPc2	volič
<g/>
.	.	kIx.	.
</s>
<s>
Druhé	druhý	k4xOgNnSc1	druhý
kolo	kolo	k1gNnSc1	kolo
volby	volba	k1gFnSc2	volba
se	se	k3xPyFc4	se
však	však	k9	však
koná	konat	k5eAaImIp3nS	konat
i	i	k9	i
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
účastní	účastný	k2eAgMnPc1d1	účastný
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
ho	on	k3xPp3gInSc2	on
pouze	pouze	k6eAd1	pouze
jeden	jeden	k4xCgMnSc1	jeden
kandidát	kandidát	k1gMnSc1	kandidát
<g/>
.	.	kIx.	.
</s>
<s>
Navrhovat	navrhovat	k5eAaImF	navrhovat
kandidáty	kandidát	k1gMnPc4	kandidát
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
je	být	k5eAaImIp3nS	být
oprávněno	oprávnit	k5eAaPmNgNnS	oprávnit
nejméně	málo	k6eAd3	málo
dvacet	dvacet	k4xCc1	dvacet
poslanců	poslanec	k1gMnPc2	poslanec
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
nejméně	málo	k6eAd3	málo
deset	deset	k4xCc1	deset
senátorů	senátor	k1gMnPc2	senátor
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
kterýkoliv	kterýkoliv	k3yIgMnSc1	kterýkoliv
občan	občan	k1gMnSc1	občan
ČR	ČR	kA	ČR
starší	starší	k1gMnSc1	starší
18	[number]	k4	18
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
podpoří	podpořit	k5eAaPmIp3nS	podpořit
<g/>
-li	i	k?	-li
jeho	jeho	k3xOp3gInSc4	jeho
návrh	návrh	k1gInSc4	návrh
petice	petice	k1gFnSc1	petice
podepsaná	podepsaný	k2eAgFnSc1d1	podepsaná
nejméně	málo	k6eAd3	málo
50	[number]	k4	50
000	[number]	k4	000
občany	občan	k1gMnPc7	občan
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
oprávněnými	oprávněný	k2eAgInPc7d1	oprávněný
volit	volit	k5eAaImF	volit
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Volba	volba	k1gFnSc1	volba
prezidenta	prezident	k1gMnSc2	prezident
se	se	k3xPyFc4	se
koná	konat	k5eAaImIp3nS	konat
v	v	k7c6	v
posledních	poslední	k2eAgInPc6d1	poslední
šedesáti	šedesát	k4xCc6	šedesát
dnech	den	k1gInPc6	den
volebního	volební	k2eAgNnSc2d1	volební
období	období	k1gNnSc2	období
úřadujícího	úřadující	k2eAgMnSc2d1	úřadující
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
nejpozději	pozdě	k6eAd3	pozdě
však	však	k9	však
třicet	třicet	k4xCc1	třicet
dnů	den	k1gInPc2	den
před	před	k7c7	před
uplynutím	uplynutí	k1gNnSc7	uplynutí
volebního	volební	k2eAgNnSc2d1	volební
období	období	k1gNnSc2	období
úřadujícího	úřadující	k2eAgMnSc2d1	úřadující
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Uvolní	uvolnit	k5eAaPmIp3nS	uvolnit
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
úřad	úřad	k1gInSc1	úřad
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
koná	konat	k5eAaImIp3nS	konat
se	se	k3xPyFc4	se
volba	volba	k1gFnSc1	volba
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
do	do	k7c2	do
devadesáti	devadesát	k4xCc2	devadesát
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Volbu	volba	k1gFnSc4	volba
vyhlašuje	vyhlašovat	k5eAaImIp3nS	vyhlašovat
předseda	předseda	k1gMnSc1	předseda
Senátu	senát	k1gInSc2	senát
nejpozději	pozdě	k6eAd3	pozdě
devadesát	devadesát	k4xCc1	devadesát
dnů	den	k1gInPc2	den
před	před	k7c7	před
jejím	její	k3xOp3gNnSc7	její
konáním	konání	k1gNnSc7	konání
<g/>
.	.	kIx.	.
</s>
<s>
Uvolní	uvolnit	k5eAaPmIp3nS	uvolnit
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
úřad	úřad	k1gInSc1	úřad
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
vyhlásí	vyhlásit	k5eAaPmIp3nS	vyhlásit
předseda	předseda	k1gMnSc1	předseda
Senátu	senát	k1gInSc2	senát
volbu	volba	k1gFnSc4	volba
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
nejpozději	pozdě	k6eAd3	pozdě
do	do	k7c2	do
deseti	deset	k4xCc2	deset
dnů	den	k1gInPc2	den
poté	poté	k6eAd1	poté
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
nejpozději	pozdě	k6eAd3	pozdě
osmdesát	osmdesát	k4xCc4	osmdesát
dnů	den	k1gInPc2	den
před	před	k7c7	před
jejím	její	k3xOp3gNnSc7	její
konáním	konání	k1gNnSc7	konání
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
nebyla	být	k5eNaImAgFnS	být
funkce	funkce	k1gFnSc1	funkce
předsedy	předseda	k1gMnSc2	předseda
Senátu	senát	k1gInSc2	senát
obsazena	obsadit	k5eAaPmNgFnS	obsadit
<g/>
,	,	kIx,	,
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
by	by	kYmCp3nP	by
volbu	volba	k1gFnSc4	volba
předseda	předseda	k1gMnSc1	předseda
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
.	.	kIx.	.
</s>
<s>
Prezidentem	prezident	k1gMnSc7	prezident
republiky	republika	k1gFnSc2	republika
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
podle	podle	k7c2	podle
čl	čl	kA	čl
<g/>
.	.	kIx.	.
57	[number]	k4	57
zvolen	zvolit	k5eAaPmNgMnS	zvolit
občan	občan	k1gMnSc1	občan
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
volitelný	volitelný	k2eAgInSc1d1	volitelný
do	do	k7c2	do
Senátu	senát	k1gInSc2	senát
<g/>
,	,	kIx,	,
a	a	k8xC	a
současně	současně	k6eAd1	současně
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nikdo	nikdo	k3yNnSc1	nikdo
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvakrát	dvakrát	k6eAd1	dvakrát
za	za	k7c7	za
sebou	se	k3xPyFc7	se
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
republiky	republika	k1gFnSc2	republika
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
vzdát	vzdát	k5eAaPmF	vzdát
dle	dle	k7c2	dle
článku	článek	k1gInSc2	článek
61	[number]	k4	61
svého	svůj	k3xOyFgInSc2	svůj
úřadu	úřad	k1gInSc2	úřad
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
předsedy	předseda	k1gMnSc2	předseda
Senátu	senát	k1gInSc2	senát
<g/>
.	.	kIx.	.
</s>
<s>
Pravomoci	pravomoc	k1gFnPc1	pravomoc
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
jsou	být	k5eAaImIp3nP	být
upraveny	upravit	k5eAaPmNgFnP	upravit
zejména	zejména	k9	zejména
v	v	k7c6	v
ustanoveních	ustanovení	k1gNnPc6	ustanovení
článků	článek	k1gInPc2	článek
62	[number]	k4	62
a	a	k8xC	a
63	[number]	k4	63
<g/>
.	.	kIx.	.
</s>
<s>
Pravomoci	pravomoc	k1gFnPc1	pravomoc
vyjmenované	vyjmenovaný	k2eAgFnPc1d1	vyjmenovaná
v	v	k7c6	v
článku	článek	k1gInSc6	článek
62	[number]	k4	62
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
označit	označit	k5eAaPmF	označit
za	za	k7c4	za
osobní	osobní	k2eAgInPc4d1	osobní
či	či	k8xC	či
privilegované	privilegovaný	k2eAgInPc4d1	privilegovaný
<g/>
,	,	kIx,	,
když	když	k8xS	když
k	k	k7c3	k
jejich	jejich	k3xOp3gInSc3	jejich
výkonu	výkon	k1gInSc3	výkon
není	být	k5eNaImIp3nS	být
vyžadována	vyžadován	k2eAgFnSc1d1	vyžadována
kontrasignace	kontrasignace	k1gFnSc1	kontrasignace
členy	člen	k1gInPc4	člen
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Nemusí	muset	k5eNaImIp3nP	muset
se	se	k3xPyFc4	se
však	však	k9	však
jednat	jednat	k5eAaImF	jednat
o	o	k7c4	o
pravomoci	pravomoc	k1gFnPc4	pravomoc
samostatné	samostatný	k2eAgFnPc4d1	samostatná
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jejich	jejich	k3xOp3gInSc1	jejich
výkon	výkon	k1gInSc1	výkon
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vázán	vázat	k5eAaImNgMnS	vázat
na	na	k7c4	na
součinnost	součinnost	k1gFnSc4	součinnost
jiného	jiný	k2eAgInSc2d1	jiný
subjektu	subjekt	k1gInSc2	subjekt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
článku	článek	k1gInSc2	článek
63	[number]	k4	63
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c6	o
pravomoci	pravomoc	k1gFnSc6	pravomoc
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
jsou	být	k5eAaImIp3nP	být
vázané	vázané	k1gNnSc4	vázané
na	na	k7c4	na
součinnost	součinnost	k1gFnSc4	součinnost
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
a	a	k8xC	a
nejde	jít	k5eNaImIp3nS	jít
tak	tak	k6eAd1	tak
o	o	k7c4	o
jeho	jeho	k3xOp3gFnPc4	jeho
osobní	osobní	k2eAgFnPc4d1	osobní
pravomoci	pravomoc	k1gFnPc4	pravomoc
<g/>
.	.	kIx.	.
</s>
<s>
Právo	právo	k1gNnSc1	právo
prezidenta	prezident	k1gMnSc2	prezident
účastnit	účastnit	k5eAaImF	účastnit
se	se	k3xPyFc4	se
na	na	k7c6	na
schůzích	schůze	k1gFnPc6	schůze
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
obou	dva	k4xCgFnPc2	dva
komor	komora	k1gFnPc2	komora
parlamentu	parlament	k1gInSc2	parlament
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc2	jejich
výborů	výbor	k1gInPc2	výbor
a	a	k8xC	a
komisí	komise	k1gFnPc2	komise
je	být	k5eAaImIp3nS	být
zakotveno	zakotvit	k5eAaPmNgNnS	zakotvit
v	v	k7c6	v
článku	článek	k1gInSc6	článek
64	[number]	k4	64
Ústavy	ústava	k1gFnSc2	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Prezidenta	prezident	k1gMnSc4	prezident
republiky	republika	k1gFnSc2	republika
nelze	lze	k6eNd1	lze
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
článek	článek	k1gInSc4	článek
65	[number]	k4	65
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
výkonu	výkon	k1gInSc2	výkon
jeho	jeho	k3xOp3gFnSc2	jeho
funkce	funkce	k1gFnSc2	funkce
zadržet	zadržet	k5eAaPmF	zadržet
<g/>
,	,	kIx,	,
trestně	trestně	k6eAd1	trestně
stíhat	stíhat	k5eAaImF	stíhat
ani	ani	k8xC	ani
stíhat	stíhat	k5eAaImF	stíhat
pro	pro	k7c4	pro
přestupek	přestupek	k1gInSc4	přestupek
nebo	nebo	k8xC	nebo
jiný	jiný	k2eAgInSc4d1	jiný
správní	správní	k2eAgInSc4d1	správní
delikt	delikt	k1gInSc4	delikt
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
března	březen	k1gInSc2	březen
2013	[number]	k4	2013
může	moct	k5eAaImIp3nS	moct
Senát	senát	k1gInSc1	senát
se	s	k7c7	s
souhlasem	souhlas	k1gInSc7	souhlas
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
podat	podat	k5eAaPmF	podat
ústavní	ústavní	k2eAgFnSc4d1	ústavní
žalobu	žaloba	k1gFnSc4	žaloba
proti	proti	k7c3	proti
prezidentu	prezident	k1gMnSc3	prezident
republiky	republika	k1gFnSc2	republika
k	k	k7c3	k
Ústavnímu	ústavní	k2eAgInSc3d1	ústavní
soudu	soud	k1gInSc3	soud
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
pro	pro	k7c4	pro
velezradu	velezrada	k1gFnSc4	velezrada
nebo	nebo	k8xC	nebo
pro	pro	k7c4	pro
hrubé	hrubý	k2eAgNnSc4d1	hrubé
porušení	porušení	k1gNnSc4	porušení
Ústavy	ústava	k1gFnSc2	ústava
nebo	nebo	k8xC	nebo
jiné	jiný	k2eAgFnSc2d1	jiná
součásti	součást	k1gFnSc2	součást
ústavního	ústavní	k2eAgInSc2d1	ústavní
pořádku	pořádek	k1gInSc2	pořádek
<g/>
.	.	kIx.	.
</s>
<s>
Ústavní	ústavní	k2eAgInSc1d1	ústavní
soud	soud	k1gInSc1	soud
může	moct	k5eAaImIp3nS	moct
na	na	k7c6	na
základě	základ	k1gInSc6	základ
ústavní	ústavní	k2eAgFnSc2d1	ústavní
žaloby	žaloba	k1gFnSc2	žaloba
Senátu	senát	k1gInSc2	senát
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
prezident	prezident	k1gMnSc1	prezident
republiky	republika	k1gFnSc2	republika
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
prezidentský	prezidentský	k2eAgInSc1d1	prezidentský
úřad	úřad	k1gInSc1	úřad
a	a	k8xC	a
způsobilost	způsobilost	k1gFnSc1	způsobilost
jej	on	k3xPp3gMnSc4	on
znovu	znovu	k6eAd1	znovu
nabýt	nabýt	k5eAaPmF	nabýt
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
přijetí	přijetí	k1gNnSc3	přijetí
návrhu	návrh	k1gInSc2	návrh
ústavní	ústavní	k2eAgFnSc2d1	ústavní
žaloby	žaloba	k1gFnSc2	žaloba
Senátem	senát	k1gInSc7	senát
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
souhlasu	souhlas	k1gInSc2	souhlas
třípětinové	třípětinový	k2eAgFnSc2d1	třípětinová
většiny	většina	k1gFnSc2	většina
přítomných	přítomný	k2eAgMnPc2d1	přítomný
senátorů	senátor	k1gMnPc2	senátor
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
přijetí	přijetí	k1gNnSc3	přijetí
souhlasu	souhlas	k1gInSc2	souhlas
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
s	s	k7c7	s
podáním	podání	k1gNnSc7	podání
ústavní	ústavní	k2eAgFnSc2d1	ústavní
žaloby	žaloba	k1gFnSc2	žaloba
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
souhlasu	souhlas	k1gInSc2	souhlas
třípětinové	třípětinový	k2eAgFnSc2d1	třípětinová
většiny	většina	k1gFnSc2	většina
všech	všecek	k3xTgMnPc2	všecek
poslanců	poslanec	k1gMnPc2	poslanec
a	a	k8xC	a
současně	současně	k6eAd1	současně
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
nevysloví	vyslovit	k5eNaPmIp3nS	vyslovit
Poslanecká	poslanecký	k2eAgFnSc1d1	Poslanecká
sněmovna	sněmovna	k1gFnSc1	sněmovna
souhlas	souhlas	k1gInSc4	souhlas
do	do	k7c2	do
tří	tři	k4xCgInPc2	tři
měsíců	měsíc	k1gInPc2	měsíc
ode	ode	k7c2	ode
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
o	o	k7c4	o
něj	on	k3xPp3gInSc4	on
Senát	senát	k1gInSc4	senát
požádal	požádat	k5eAaPmAgMnS	požádat
<g/>
,	,	kIx,	,
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
souhlas	souhlas	k1gInSc1	souhlas
nebyl	být	k5eNaImAgInS	být
dán	dát	k5eAaPmNgInS	dát
<g/>
.	.	kIx.	.
</s>
<s>
Článek	článek	k1gInSc1	článek
66	[number]	k4	66
řeší	řešit	k5eAaImIp3nS	řešit
zastupování	zastupování	k1gNnSc4	zastupování
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
úřad	úřad	k1gInSc1	úřad
prezidenta	prezident	k1gMnSc2	prezident
uvolní	uvolnit	k5eAaPmIp3nS	uvolnit
a	a	k8xC	a
nový	nový	k2eAgMnSc1d1	nový
prezident	prezident	k1gMnSc1	prezident
ještě	ještě	k9	ještě
není	být	k5eNaImIp3nS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
či	či	k8xC	či
nesložil	složit	k5eNaPmAgMnS	složit
slib	slib	k1gInSc4	slib
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
prezident	prezident	k1gMnSc1	prezident
nemůže	moct	k5eNaImIp3nS	moct
ze	z	k7c2	z
závažných	závažný	k2eAgInPc2d1	závažný
důvodů	důvod	k1gInPc2	důvod
vykonávat	vykonávat	k5eAaImF	vykonávat
svůj	svůj	k3xOyFgInSc4	svůj
úřad	úřad	k1gInSc4	úřad
a	a	k8xC	a
usnese	usnést	k5eAaPmIp3nS	usnést
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
Poslanecká	poslanecký	k2eAgFnSc1d1	Poslanecká
sněmovna	sněmovna	k1gFnSc1	sněmovna
a	a	k8xC	a
Senát	senát	k1gInSc1	senát
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
některé	některý	k3yIgFnPc1	některý
pravomoci	pravomoc	k1gFnPc1	pravomoc
přecházejí	přecházet	k5eAaImIp3nP	přecházet
na	na	k7c4	na
předsedu	předseda	k1gMnSc4	předseda
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
další	další	k2eAgMnSc1d1	další
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
předseda	předseda	k1gMnSc1	předseda
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
Sněmovna	sněmovna	k1gFnSc1	sněmovna
rozpuštěna	rozpuštěn	k2eAgFnSc1d1	rozpuštěna
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
předseda	předseda	k1gMnSc1	předseda
Senátu	senát	k1gInSc2	senát
<g/>
.	.	kIx.	.
</s>
<s>
Tomu	ten	k3xDgNnSc3	ten
také	také	k9	také
přísluší	příslušet	k5eAaImIp3nS	příslušet
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
vymezené	vymezený	k2eAgFnSc2d1	vymezená
funkce	funkce	k1gFnSc2	funkce
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
výkon	výkon	k1gInSc4	výkon
funkce	funkce	k1gFnSc2	funkce
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
<g/>
-li	i	k?	-li
o	o	k7c4	o
vyhlášení	vyhlášení	k1gNnSc4	vyhlášení
voleb	volba	k1gFnPc2	volba
do	do	k7c2	do
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
.	.	kIx.	.
</s>
<s>
Charakteristika	charakteristika	k1gFnSc1	charakteristika
a	a	k8xC	a
složení	složení	k1gNnSc1	složení
vlády	vláda	k1gFnSc2	vláda
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
ustanovení	ustanovení	k1gNnSc6	ustanovení
článku	článek	k1gInSc2	článek
67	[number]	k4	67
Ústavy	ústava	k1gFnSc2	ústava
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
něhož	jenž	k3xRgMnSc2	jenž
je	být	k5eAaImIp3nS	být
vláda	vláda	k1gFnSc1	vláda
vrcholným	vrcholný	k2eAgMnSc7d1	vrcholný
orgánem	orgán	k1gMnSc7	orgán
výkonné	výkonný	k2eAgFnSc2d1	výkonná
moci	moc	k1gFnSc2	moc
a	a	k8xC	a
skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
předsedy	předseda	k1gMnSc2	předseda
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
místopředsedů	místopředseda	k1gMnPc2	místopředseda
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
ministrů	ministr	k1gMnPc2	ministr
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
je	být	k5eAaImIp3nS	být
sice	sice	k8xC	sice
v	v	k7c6	v
Ústavě	ústava	k1gFnSc6	ústava
zařazena	zařadit	k5eAaPmNgFnS	zařadit
až	až	k6eAd1	až
za	za	k7c4	za
prezidenta	prezident	k1gMnSc4	prezident
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
reálné	reálný	k2eAgFnSc2d1	reálná
moci	moc	k1gFnSc2	moc
vláda	vláda	k1gFnSc1	vláda
prezidenta	prezident	k1gMnSc2	prezident
převyšuje	převyšovat	k5eAaImIp3nS	převyšovat
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
je	být	k5eAaImIp3nS	být
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
jmenování	jmenování	k1gNnSc4	jmenování
<g/>
.	.	kIx.	.
</s>
<s>
Vládě	Vláďa	k1gFnSc3	Vláďa
je	být	k5eAaImIp3nS	být
totiž	totiž	k9	totiž
svěřena	svěřen	k2eAgFnSc1d1	svěřena
obecná	obecný	k2eAgFnSc1d1	obecná
působnost	působnost	k1gFnSc1	působnost
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
exekutivy	exekutiva	k1gFnSc2	exekutiva
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
prezident	prezident	k1gMnSc1	prezident
má	mít	k5eAaImIp3nS	mít
působnost	působnost	k1gFnSc4	působnost
jen	jen	k9	jen
v	v	k7c6	v
rozsahu	rozsah	k1gInSc6	rozsah
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
vymezuje	vymezovat	k5eAaImIp3nS	vymezovat
Ústava	ústava	k1gFnSc1	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Článek	článek	k1gInSc1	článek
68	[number]	k4	68
zakotvuje	zakotvovat	k5eAaImIp3nS	zakotvovat
vládu	vláda	k1gFnSc4	vláda
v	v	k7c6	v
systému	systém	k1gInSc6	systém
nejvyšších	vysoký	k2eAgInPc2d3	Nejvyšší
orgánů	orgán	k1gInPc2	orgán
státní	státní	k2eAgFnSc2d1	státní
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
upravuje	upravovat	k5eAaImIp3nS	upravovat
v	v	k7c6	v
otázce	otázka	k1gFnSc6	otázka
jmenování	jmenování	k1gNnSc2	jmenování
vztah	vztah	k1gInSc1	vztah
k	k	k7c3	k
prezidentu	prezident	k1gMnSc3	prezident
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
v	v	k7c6	v
otázce	otázka	k1gFnSc6	otázka
politické	politický	k2eAgFnSc2d1	politická
odpovědnosti	odpovědnost	k1gFnSc2	odpovědnost
k	k	k7c3	k
Poslanecké	poslanecký	k2eAgFnSc3d1	Poslanecká
sněmovně	sněmovna	k1gFnSc3	sněmovna
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
odstavec	odstavec	k1gInSc1	odstavec
upravuje	upravovat	k5eAaImIp3nS	upravovat
otázku	otázka	k1gFnSc4	otázka
odpovědnosti	odpovědnost	k1gFnSc2	odpovědnost
vlády	vláda	k1gFnSc2	vláda
Poslanecké	poslanecký	k2eAgFnSc3d1	Poslanecká
sněmovně	sněmovna	k1gFnSc3	sněmovna
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
Senátu	senát	k1gInSc2	senát
<g/>
,	,	kIx,	,
v	v	k7c6	v
odstavcích	odstavec	k1gInPc6	odstavec
dva	dva	k4xCgInPc4	dva
až	až	k9	až
pět	pět	k4xCc4	pět
je	být	k5eAaImIp3nS	být
upraven	upravit	k5eAaPmNgInS	upravit
postup	postup	k1gInSc1	postup
tvorby	tvorba	k1gFnSc2	tvorba
vlády	vláda	k1gFnSc2	vláda
po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
.	.	kIx.	.
</s>
<s>
Ústava	ústava	k1gFnSc1	ústava
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
71	[number]	k4	71
upravuje	upravovat	k5eAaImIp3nS	upravovat
dobrovolnou	dobrovolný	k2eAgFnSc4d1	dobrovolná
žádost	žádost	k1gFnSc4	žádost
vlády	vláda	k1gFnSc2	vláda
o	o	k7c6	o
vyslovení	vyslovení	k1gNnSc6	vyslovení
důvěry	důvěra	k1gFnSc2	důvěra
<g/>
.	.	kIx.	.
</s>
<s>
Ústava	ústava	k1gFnSc1	ústava
tak	tak	k9	tak
upravuje	upravovat	k5eAaImIp3nS	upravovat
tři	tři	k4xCgFnPc1	tři
situace	situace	k1gFnPc1	situace
spojené	spojený	k2eAgFnPc1d1	spojená
se	s	k7c7	s
žádostí	žádost	k1gFnSc7	žádost
o	o	k7c4	o
důvěru	důvěra	k1gFnSc4	důvěra
<g/>
:	:	kIx,	:
samostatně	samostatně	k6eAd1	samostatně
či	či	k8xC	či
souběžně	souběžně	k6eAd1	souběžně
s	s	k7c7	s
předloženým	předložený	k2eAgInSc7d1	předložený
návrhem	návrh	k1gInSc7	návrh
zákona	zákon	k1gInSc2	zákon
<g/>
,	,	kIx,	,
dobrovolně	dobrovolně	k6eAd1	dobrovolně
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
povinně	povinně	k6eAd1	povinně
do	do	k7c2	do
třiceti	třicet	k4xCc2	třicet
dní	den	k1gInPc2	den
od	od	k7c2	od
jmenování	jmenování	k1gNnSc2	jmenování
<g/>
.	.	kIx.	.
</s>
<s>
Iniciativa	iniciativa	k1gFnSc1	iniciativa
však	však	k9	však
může	moct	k5eAaImIp3nS	moct
vzejít	vzejít	k5eAaPmF	vzejít
i	i	k9	i
od	od	k7c2	od
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
může	moct	k5eAaImIp3nS	moct
vládě	vláda	k1gFnSc3	vláda
vyslovit	vyslovit	k5eAaPmF	vyslovit
nedůvěru	nedůvěra	k1gFnSc4	nedůvěra
(	(	kIx(	(
<g/>
článek	článek	k1gInSc1	článek
72	[number]	k4	72
Ústavy	ústava	k1gFnSc2	ústava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Návrh	návrh	k1gInSc1	návrh
na	na	k7c6	na
vyslovení	vyslovení	k1gNnSc6	vyslovení
nedůvěry	nedůvěra	k1gFnSc2	nedůvěra
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
podán	podat	k5eAaPmNgInS	podat
alespoň	alespoň	k9	alespoň
padesáti	padesát	k4xCc7	padesát
poslanci	poslanec	k1gMnPc7	poslanec
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
nedůvěra	nedůvěra	k1gFnSc1	nedůvěra
vládě	vláda	k1gFnSc3	vláda
vyslovena	vysloven	k2eAgFnSc1d1	vyslovena
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ta	ten	k3xDgFnSc1	ten
by	by	kYmCp3nS	by
demisi	demise	k1gFnSc4	demise
nepodala	podat	k5eNaPmAgFnS	podat
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
z	z	k7c2	z
dikce	dikce	k1gFnSc2	dikce
článku	článek	k1gInSc2	článek
75	[number]	k4	75
prezident	prezident	k1gMnSc1	prezident
povinnost	povinnost	k1gFnSc4	povinnost
takovou	takový	k3xDgFnSc4	takový
vládu	vláda	k1gFnSc4	vláda
odvolat	odvolat	k5eAaPmF	odvolat
<g/>
.	.	kIx.	.
</s>
<s>
Slib	slib	k1gInSc1	slib
člena	člen	k1gMnSc4	člen
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
člen	člen	k1gInSc1	člen
vlády	vláda	k1gFnSc2	vláda
skládá	skládat	k5eAaImIp3nS	skládat
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
upraven	upravit	k5eAaPmNgInS	upravit
v	v	k7c6	v
článku	článek	k1gInSc6	článek
69	[number]	k4	69
Ústavy	ústava	k1gFnSc2	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Člen	člen	k1gMnSc1	člen
vlády	vláda	k1gFnSc2	vláda
nesmí	smět	k5eNaImIp3nS	smět
vykonávat	vykonávat	k5eAaImF	vykonávat
činnosti	činnost	k1gFnSc2	činnost
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
povaha	povaha	k1gFnSc1	povaha
odporuje	odporovat	k5eAaImIp3nS	odporovat
výkonu	výkon	k1gInSc2	výkon
jeho	jeho	k3xOp3gFnSc2	jeho
funkce	funkce	k1gFnSc2	funkce
<g/>
,	,	kIx,	,
v	v	k7c6	v
podrobnostech	podrobnost	k1gFnPc6	podrobnost
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
článek	článek	k1gInSc1	článek
70	[number]	k4	70
na	na	k7c4	na
zákon	zákon	k1gInSc4	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
podává	podávat	k5eAaImIp3nS	podávat
dle	dle	k7c2	dle
článku	článek	k1gInSc2	článek
73	[number]	k4	73
odst	odsta	k1gFnPc2	odsta
<g/>
.	.	kIx.	.
1	[number]	k4	1
Ústavy	ústava	k1gFnSc2	ústava
demisi	demise	k1gFnSc4	demise
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
prezidenta	prezident	k1gMnSc2	prezident
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgMnPc1d1	ostatní
členové	člen	k1gMnPc1	člen
vlády	vláda	k1gFnSc2	vláda
předávají	předávat	k5eAaImIp3nP	předávat
demisi	demise	k1gFnSc4	demise
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
prezidenta	prezident	k1gMnSc2	prezident
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
premiéra	premiér	k1gMnSc2	premiér
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
teoretiky	teoretik	k1gMnPc7	teoretik
ústavního	ústavní	k2eAgNnSc2d1	ústavní
práva	právo	k1gNnSc2	právo
nepanuje	panovat	k5eNaImIp3nS	panovat
shoda	shoda	k1gFnSc1	shoda
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
následkem	následkem	k7c2	následkem
demise	demise	k1gFnSc2	demise
předsedy	předseda	k1gMnSc2	předseda
vlády	vláda	k1gFnSc2	vláda
je	být	k5eAaImIp3nS	být
i	i	k9	i
konec	konec	k1gInSc1	konec
vlády	vláda	k1gFnSc2	vláda
jako	jako	k8xC	jako
takové	takový	k3xDgFnSc2	takový
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
podal	podat	k5eAaPmAgMnS	podat
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
demisi	demise	k1gFnSc4	demise
s	s	k7c7	s
dovětkem	dovětek	k1gInSc7	dovětek
<g/>
,	,	kIx,	,
že	že	k8xS	že
demisi	demise	k1gFnSc4	demise
podává	podávat	k5eAaImIp3nS	podávat
celá	celý	k2eAgFnSc1d1	celá
vláda	vláda	k1gFnSc1	vláda
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
označeno	označit	k5eAaPmNgNnS	označit
za	za	k7c4	za
exces	exces	k1gInSc4	exces
vyplývající	vyplývající	k2eAgInSc4d1	vyplývající
z	z	k7c2	z
výjimečnosti	výjimečnost	k1gFnSc2	výjimečnost
situace	situace	k1gFnSc2	situace
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
by	by	kYmCp3nS	by
totiž	totiž	k9	totiž
svou	svůj	k3xOyFgFnSc4	svůj
demisi	demise	k1gFnSc4	demise
měla	mít	k5eAaImAgFnS	mít
podávat	podávat	k5eAaImF	podávat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
kolektivního	kolektivní	k2eAgNnSc2d1	kolektivní
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
–	–	k?	–
například	například	k6eAd1	například
usnesení	usnesení	k1gNnSc1	usnesení
vlády	vláda	k1gFnSc2	vláda
Mirka	Mirek	k1gMnSc2	Mirek
Topolánka	Topolánek	k1gMnSc2	Topolánek
ze	z	k7c2	z
dne	den	k1gInSc2	den
26	[number]	k4	26
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2009	[number]	k4	2009
č.	č.	k?	č.
371	[number]	k4	371
<g/>
,	,	kIx,	,
o	o	k7c6	o
podání	podání	k1gNnSc6	podání
demise	demise	k1gFnSc2	demise
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Filip	Filip	k1gMnSc1	Filip
se	se	k3xPyFc4	se
domnívá	domnívat	k5eAaImIp3nS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
premiér	premiér	k1gMnSc1	premiér
může	moct	k5eAaImIp3nS	moct
podat	podat	k5eAaPmF	podat
demisi	demise	k1gFnSc4	demise
<g/>
,	,	kIx,	,
vláda	vláda	k1gFnSc1	vláda
však	však	k9	však
demisi	demise	k1gFnSc4	demise
má	mít	k5eAaImIp3nS	mít
podat	podat	k5eAaPmF	podat
kolektivním	kolektivní	k2eAgNnSc7d1	kolektivní
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ovšem	ovšem	k9	ovšem
nebrání	bránit	k5eNaImIp3nP	bránit
premiérovi	premiérův	k2eAgMnPc1d1	premiérův
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
odvolání	odvolání	k1gNnSc4	odvolání
všech	všecek	k3xTgMnPc2	všecek
členů	člen	k1gMnPc2	člen
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Pavlíček	Pavlíček	k1gMnSc1	Pavlíček
spojuje	spojovat	k5eAaImIp3nS	spojovat
s	s	k7c7	s
demisí	demise	k1gFnSc7	demise
premiéra	premiér	k1gMnSc2	premiér
demisi	demise	k1gFnSc4	demise
celé	celá	k1gFnSc2	celá
vlády	vláda	k1gFnSc2	vláda
jako	jako	k8xS	jako
celku	celek	k1gInSc2	celek
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
vláda	vláda	k1gFnSc1	vláda
nemůže	moct	k5eNaImIp3nS	moct
podle	podle	k7c2	podle
něj	on	k3xPp3gNnSc2	on
být	být	k5eAaImF	být
bez	bez	k7c2	bez
předsedy	předseda	k1gMnSc2	předseda
<g/>
.	.	kIx.	.
</s>
<s>
Mikule	Mikule	k1gFnSc1	Mikule
řešení	řešení	k1gNnSc2	řešení
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
sporná	sporný	k2eAgNnPc4d1	sporné
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
toto	tento	k3xDgNnSc1	tento
Ústava	ústava	k1gFnSc1	ústava
výslovně	výslovně	k6eAd1	výslovně
neřeší	řešit	k5eNaImIp3nS	řešit
a	a	k8xC	a
Klíma	Klíma	k1gMnSc1	Klíma
k	k	k7c3	k
otázce	otázka	k1gFnSc3	otázka
nezaujal	zaujmout	k5eNaPmAgMnS	zaujmout
stanovisko	stanovisko	k1gNnSc4	stanovisko
<g/>
.	.	kIx.	.
</s>
<s>
Ústava	ústava	k1gFnSc1	ústava
neřeší	řešit	k5eNaImIp3nS	řešit
situaci	situace	k1gFnSc4	situace
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
postupovat	postupovat	k5eAaImF	postupovat
<g/>
,	,	kIx,	,
zemře	zemřít	k5eAaPmIp3nS	zemřít
<g/>
-li	i	k?	-li
premiér	premiér	k1gMnSc1	premiér
<g/>
.	.	kIx.	.
</s>
<s>
Článek	článek	k1gInSc1	článek
72	[number]	k4	72
upravuje	upravovat	k5eAaImIp3nS	upravovat
tři	tři	k4xCgFnPc4	tři
situace	situace	k1gFnPc4	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
vláda	vláda	k1gFnSc1	vláda
povinna	povinen	k2eAgFnSc1d1	povinna
podat	podat	k5eAaPmF	podat
demisi	demise	k1gFnSc4	demise
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
odstavce	odstavec	k1gInSc2	odstavec
třetího	třetí	k4xOgNnSc2	třetí
–	–	k?	–
podá	podat	k5eAaPmIp3nS	podat
<g/>
-li	i	k?	-li
vláda	vláda	k1gFnSc1	vláda
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
odstavce	odstavec	k1gInSc2	odstavec
2	[number]	k4	2
<g/>
)	)	kIx)	)
demisi	demise	k1gFnSc4	demise
–	–	k?	–
prezident	prezident	k1gMnSc1	prezident
republiky	republika	k1gFnSc2	republika
demisi	demise	k1gFnSc4	demise
přijme	přijmout	k5eAaPmIp3nS	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
republiky	republika	k1gFnSc2	republika
odvolá	odvolat	k5eAaPmIp3nS	odvolat
z	z	k7c2	z
dikce	dikce	k1gFnSc2	dikce
článku	článek	k1gInSc2	článek
74	[number]	k4	74
člena	člen	k1gMnSc2	člen
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
jestliže	jestliže	k8xS	jestliže
to	ten	k3xDgNnSc4	ten
navrhne	navrhnout	k5eAaPmIp3nS	navrhnout
předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Postup	postup	k1gInSc1	postup
podle	podle	k7c2	podle
tohoto	tento	k3xDgInSc2	tento
článku	článek	k1gInSc2	článek
nepřipadá	připadat	k5eNaImIp3nS	připadat
z	z	k7c2	z
povahy	povaha	k1gFnSc2	povaha
věci	věc	k1gFnSc2	věc
v	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
u	u	k7c2	u
premiéra	premiér	k1gMnSc2	premiér
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
může	moct	k5eAaImIp3nS	moct
svou	svůj	k3xOyFgFnSc4	svůj
funkci	funkce	k1gFnSc4	funkce
ztratit	ztratit	k5eAaPmF	ztratit
svou	svůj	k3xOyFgFnSc7	svůj
demisí	demise	k1gFnSc7	demise
<g/>
,	,	kIx,	,
demisí	demise	k1gFnSc7	demise
celé	celý	k2eAgFnSc2d1	celá
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
odvoláním	odvolání	k1gNnSc7	odvolání
celé	celá	k1gFnSc2	celá
vlády	vláda	k1gFnSc2	vláda
či	či	k8xC	či
smrtí	smrt	k1gFnSc7	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
je	být	k5eAaImIp3nS	být
kolegiálním	kolegiální	k2eAgInSc7d1	kolegiální
orgánem	orgán	k1gInSc7	orgán
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
ve	v	k7c6	v
sboru	sbor	k1gInSc6	sbor
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
mají	mít	k5eAaImIp3nP	mít
všichni	všechen	k3xTgMnPc1	všechen
členové	člen	k1gMnPc1	člen
rovné	rovný	k2eAgFnPc1d1	rovná
právo	právo	k1gNnSc4	právo
účasti	účast	k1gFnSc2	účast
a	a	k8xC	a
rovný	rovný	k2eAgInSc4d1	rovný
hlas	hlas	k1gInSc4	hlas
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
přijetí	přijetí	k1gNnSc3	přijetí
usnesení	usnesení	k1gNnSc2	usnesení
vlády	vláda	k1gFnSc2	vláda
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
dle	dle	k7c2	dle
článku	článek	k1gInSc2	článek
76	[number]	k4	76
souhlasu	souhlas	k1gInSc2	souhlas
nadpoloviční	nadpoloviční	k2eAgFnSc2d1	nadpoloviční
většiny	většina	k1gFnSc2	většina
jejích	její	k3xOp3gMnPc2	její
členů	člen	k1gMnPc2	člen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
vlády	vláda	k1gFnSc2	vláda
stojí	stát	k5eAaImIp3nS	stát
premiér	premiér	k1gMnSc1	premiér
–	–	k?	–
předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInPc1	jehož
úkoly	úkol	k1gInPc1	úkol
jsou	být	k5eAaImIp3nP	být
vymezeny	vymezit	k5eAaPmNgInP	vymezit
v	v	k7c6	v
článku	článek	k1gInSc6	článek
77	[number]	k4	77
odst	odstum	k1gNnPc2	odstum
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
:	:	kIx,	:
premiér	premiér	k1gMnSc1	premiér
organizuje	organizovat	k5eAaBmIp3nS	organizovat
činnost	činnost	k1gFnSc4	činnost
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
řídí	řídit	k5eAaImIp3nS	řídit
její	její	k3xOp3gFnSc1	její
schůze	schůze	k1gFnSc1	schůze
<g/>
,	,	kIx,	,
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
jejím	její	k3xOp3gNnSc7	její
jménem	jméno	k1gNnSc7	jméno
a	a	k8xC	a
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
další	další	k2eAgFnPc4d1	další
činnosti	činnost	k1gFnPc4	činnost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
mu	on	k3xPp3gMnSc3	on
svěřeny	svěřen	k2eAgFnPc1d1	svěřena
Ústavou	ústava	k1gFnSc7	ústava
či	či	k8xC	či
jinými	jiný	k2eAgInPc7d1	jiný
zákony	zákon	k1gInPc7	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
odstavec	odstavec	k1gInSc1	odstavec
článku	článek	k1gInSc2	článek
77	[number]	k4	77
řeší	řešit	k5eAaImIp3nS	řešit
situace	situace	k1gFnSc1	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
nemůže	moct	k5eNaImIp3nS	moct
dočasně	dočasně	k6eAd1	dočasně
některé	některý	k3yIgFnPc4	některý
své	svůj	k3xOyFgFnPc4	svůj
povinnosti	povinnost	k1gFnPc4	povinnost
vykonávat	vykonávat	k5eAaImF	vykonávat
<g/>
.	.	kIx.	.
</s>
<s>
Článek	článek	k1gInSc1	článek
78	[number]	k4	78
zakládá	zakládat	k5eAaImIp3nS	zakládat
generální	generální	k2eAgNnSc4d1	generální
zmocnění	zmocnění	k1gNnSc4	zmocnění
vlády	vláda	k1gFnSc2	vláda
k	k	k7c3	k
vydávání	vydávání	k1gNnSc3	vydávání
podzákonných	podzákonný	k2eAgInPc2d1	podzákonný
právních	právní	k2eAgInPc2d1	právní
předpisů	předpis	k1gInPc2	předpis
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
na	na	k7c6	na
základě	základ	k1gInSc6	základ
tohoto	tento	k3xDgNnSc2	tento
ustanovení	ustanovení	k1gNnSc2	ustanovení
Ústavy	ústava	k1gFnSc2	ústava
může	moct	k5eAaImIp3nS	moct
k	k	k7c3	k
provedení	provedení	k1gNnSc3	provedení
zákona	zákon	k1gInSc2	zákon
a	a	k8xC	a
v	v	k7c6	v
jeho	jeho	k3xOp3gFnPc6	jeho
mezích	mez	k1gFnPc6	mez
vydávat	vydávat	k5eAaPmF	vydávat
nařízení	nařízení	k1gNnSc4	nařízení
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
bez	bez	k7c2	bez
zvláštního	zvláštní	k2eAgNnSc2d1	zvláštní
zmocnění	zmocnění	k1gNnSc2	zmocnění
obsaženého	obsažený	k2eAgNnSc2d1	obsažené
v	v	k7c6	v
zákoně	zákon	k1gInSc6	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Správní	správní	k2eAgInPc1d1	správní
úřady	úřad	k1gInPc1	úřad
a	a	k8xC	a
orgány	orgán	k1gInPc1	orgán
územních	územní	k2eAgInPc2d1	územní
samosprávných	samosprávný	k2eAgInPc2d1	samosprávný
celků	celek	k1gInPc2	celek
mohou	moct	k5eAaImIp3nP	moct
vydávat	vydávat	k5eAaImF	vydávat
podzákonné	podzákonný	k2eAgInPc4d1	podzákonný
právní	právní	k2eAgInPc4d1	právní
předpisy	předpis	k1gInPc4	předpis
ve	v	k7c6	v
věcech	věc	k1gFnPc6	věc
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
na	na	k7c6	na
základě	základ	k1gInSc6	základ
a	a	k8xC	a
v	v	k7c6	v
mezích	mez	k1gFnPc6	mez
zákona	zákon	k1gInSc2	zákon
podle	podle	k7c2	podle
článku	článek	k1gInSc2	článek
79	[number]	k4	79
odst	odsta	k1gFnPc2	odsta
<g/>
.	.	kIx.	.
3	[number]	k4	3
jen	jen	k9	jen
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
jestliže	jestliže	k8xS	jestliže
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
zákon	zákon	k1gInSc1	zákon
výslovně	výslovně	k6eAd1	výslovně
zmocnil	zmocnit	k5eAaPmAgInS	zmocnit
–	–	k?	–
vláda	vláda	k1gFnSc1	vláda
právě	právě	k6eAd1	právě
takové	takový	k3xDgNnSc4	takový
zmocnění	zmocnění	k1gNnSc4	zmocnění
již	již	k6eAd1	již
nepotřebuje	potřebovat	k5eNaImIp3nS	potřebovat
<g/>
.	.	kIx.	.
</s>
<s>
Článek	článek	k1gInSc1	článek
79	[number]	k4	79
v	v	k7c6	v
odstavci	odstavec	k1gInSc6	odstavec
prvním	první	k4xOgMnSc6	první
určuje	určovat	k5eAaImIp3nS	určovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ministerstva	ministerstvo	k1gNnPc1	ministerstvo
a	a	k8xC	a
jiné	jiný	k2eAgInPc1d1	jiný
správní	správní	k2eAgInPc1d1	správní
úřady	úřad	k1gInPc1	úřad
lze	lze	k6eAd1	lze
zřídit	zřídit	k5eAaPmF	zřídit
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc4	jejich
působnost	působnost	k1gFnSc4	působnost
stanovit	stanovit	k5eAaPmF	stanovit
jen	jen	k9	jen
zákonem	zákon	k1gInSc7	zákon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
odstavci	odstavec	k1gInSc6	odstavec
druhém	druhý	k4xOgNnSc6	druhý
je	být	k5eAaImIp3nS	být
obsažen	obsažen	k2eAgInSc4d1	obsažen
ústavní	ústavní	k2eAgInSc4d1	ústavní
pokyn	pokyn	k1gInSc4	pokyn
k	k	k7c3	k
přijetí	přijetí	k1gNnSc3	přijetí
zvláštního	zvláštní	k2eAgInSc2d1	zvláštní
zákona	zákon	k1gInSc2	zákon
k	k	k7c3	k
úpravě	úprava	k1gFnSc3	úprava
právních	právní	k2eAgInPc2d1	právní
poměrů	poměr	k1gInPc2	poměr
státních	státní	k2eAgMnPc2d1	státní
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
zákonem	zákon	k1gInSc7	zákon
byl	být	k5eAaImAgInS	být
původně	původně	k6eAd1	původně
služební	služební	k2eAgInSc1d1	služební
zákon	zákon	k1gInSc1	zákon
<g/>
,	,	kIx,	,
přijatý	přijatý	k2eAgInSc1d1	přijatý
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
však	však	k9	však
nikdy	nikdy	k6eAd1	nikdy
nenabyl	nabýt	k5eNaPmAgMnS	nabýt
účinnosti	účinnost	k1gFnSc2	účinnost
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
byl	být	k5eAaImAgInS	být
nahrazen	nahradit	k5eAaPmNgInS	nahradit
zákonem	zákon	k1gInSc7	zákon
o	o	k7c6	o
státní	státní	k2eAgFnSc6d1	státní
službě	služba	k1gFnSc6	služba
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgNnSc1d1	státní
zastupitelství	zastupitelství	k1gNnSc1	zastupitelství
<g/>
,	,	kIx,	,
nová	nový	k2eAgFnSc1d1	nová
instituce	instituce	k1gFnSc1	instituce
nahrazující	nahrazující	k2eAgFnSc4d1	nahrazující
dosavadní	dosavadní	k2eAgFnSc4d1	dosavadní
prokuraturu	prokuratura	k1gFnSc4	prokuratura
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
upraveno	upravit	k5eAaPmNgNnS	upravit
v	v	k7c6	v
článku	článek	k1gInSc6	článek
80	[number]	k4	80
Ústavy	ústava	k1gFnSc2	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Vládní	vládní	k2eAgInSc1d1	vládní
návrh	návrh	k1gInSc1	návrh
Ústavy	ústava	k1gFnSc2	ústava
zahrnoval	zahrnovat	k5eAaImAgInS	zahrnovat
státní	státní	k2eAgNnSc4d1	státní
zastupitelství	zastupitelství	k1gNnSc4	zastupitelství
do	do	k7c2	do
hlavy	hlava	k1gFnSc2	hlava
čtvrté	čtvrtá	k1gFnSc2	čtvrtá
<g/>
,	,	kIx,	,
moci	moc	k1gFnSc2	moc
soudní	soudní	k2eAgFnSc2d1	soudní
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
hlavy	hlava	k1gFnSc2	hlava
třetí	třetí	k4xOgInSc1	třetí
byl	být	k5eAaImAgInS	být
institut	institut	k1gInSc4	institut
přesunut	přesunut	k2eAgInSc4d1	přesunut
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
jednání	jednání	k1gNnSc2	jednání
v	v	k7c6	v
ČNR	ČNR	kA	ČNR
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dikce	dikce	k1gFnSc2	dikce
Ústavy	ústava	k1gFnSc2	ústava
zastupuje	zastupovat	k5eAaImIp3nS	zastupovat
státní	státní	k2eAgNnSc4d1	státní
zastupitelství	zastupitelství	k1gNnSc4	zastupitelství
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
žalobu	žaloba	k1gFnSc4	žaloba
v	v	k7c6	v
trestním	trestní	k2eAgNnSc6d1	trestní
řízení	řízení	k1gNnSc6	řízení
a	a	k8xC	a
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
i	i	k9	i
další	další	k2eAgInPc4d1	další
úkoly	úkol	k1gInPc4	úkol
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
stanoví	stanovit	k5eAaPmIp3nS	stanovit
zákon	zákon	k1gInSc1	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Vymezení	vymezení	k1gNnSc1	vymezení
postavení	postavení	k1gNnSc2	postavení
a	a	k8xC	a
působnosti	působnost	k1gFnSc2	působnost
státního	státní	k2eAgNnSc2d1	státní
zastupitelství	zastupitelství	k1gNnSc2	zastupitelství
z	z	k7c2	z
dikce	dikce	k1gFnSc2	dikce
odst	odst	k1gInSc1	odst
<g/>
.	.	kIx.	.
2	[number]	k4	2
provádí	provádět	k5eAaImIp3nS	provádět
zákon	zákon	k1gInSc1	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
státní	státní	k2eAgNnSc1d1	státní
zastupitelství	zastupitelství	k1gNnSc1	zastupitelství
není	být	k5eNaImIp3nS	být
orgánem	orgán	k1gInSc7	orgán
moci	moct	k5eAaImF	moct
soudní	soudní	k2eAgInSc1d1	soudní
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
přiřazováno	přiřazovat	k5eAaImNgNnS	přiřazovat
k	k	k7c3	k
systému	systém	k1gInSc3	systém
justičních	justiční	k2eAgInPc2d1	justiční
orgánů	orgán	k1gInPc2	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Hlava	hlava	k1gFnSc1	hlava
čtvrtá	čtvrtá	k1gFnSc1	čtvrtá
<g/>
,	,	kIx,	,
čl	čl	kA	čl
<g/>
.	.	kIx.	.
81	[number]	k4	81
až	až	k9	až
96	[number]	k4	96
<g/>
,	,	kIx,	,
upravuje	upravovat	k5eAaImIp3nS	upravovat
moc	moc	k6eAd1	moc
soudní	soudní	k2eAgInSc1d1	soudní
<g/>
.	.	kIx.	.
</s>
<s>
Soudní	soudní	k2eAgMnPc1d1	soudní
moc	moc	k6eAd1	moc
vykonávají	vykonávat	k5eAaImIp3nP	vykonávat
dle	dle	k7c2	dle
článku	článek	k1gInSc2	článek
81	[number]	k4	81
Ústavy	ústava	k1gFnSc2	ústava
jménem	jméno	k1gNnSc7	jméno
republiky	republika	k1gFnSc2	republika
nezávislé	závislý	k2eNgInPc4d1	nezávislý
soudy	soud	k1gInPc4	soud
<g/>
.	.	kIx.	.
</s>
<s>
Nezávislost	nezávislost	k1gFnSc1	nezávislost
dle	dle	k7c2	dle
tohoto	tento	k3xDgNnSc2	tento
ustanovení	ustanovení	k1gNnSc2	ustanovení
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
soudy	soud	k1gInPc1	soud
jsou	být	k5eAaImIp3nP	být
institucionálně	institucionálně	k6eAd1	institucionálně
nezávislé	závislý	k2eNgFnPc1d1	nezávislá
na	na	k7c6	na
moci	moc	k1gFnSc6	moc
zákonodárné	zákonodárný	k2eAgFnSc6d1	zákonodárná
a	a	k8xC	a
výkonné	výkonný	k2eAgFnSc6d1	výkonná
<g/>
.	.	kIx.	.
</s>
<s>
Výkon	výkon	k1gInSc1	výkon
jménem	jméno	k1gNnSc7	jméno
republiky	republika	k1gFnSc2	republika
se	se	k3xPyFc4	se
promítá	promítat	k5eAaImIp3nS	promítat
i	i	k9	i
do	do	k7c2	do
úvodní	úvodní	k2eAgFnSc2d1	úvodní
formulace	formulace	k1gFnSc2	formulace
meritorních	meritorní	k2eAgNnPc2d1	meritorní
rozhodnutí	rozhodnutí	k1gNnPc2	rozhodnutí
všech	všecek	k3xTgInPc2	všecek
soudů	soud	k1gInPc2	soud
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
"	"	kIx"	"
<g/>
jménem	jméno	k1gNnSc7	jméno
republiky	republika	k1gFnSc2	republika
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Článek	článek	k1gInSc1	článek
82	[number]	k4	82
Ústavy	ústava	k1gFnSc2	ústava
na	na	k7c4	na
předchozí	předchozí	k2eAgNnSc4d1	předchozí
navazuje	navazovat	k5eAaImIp3nS	navazovat
<g/>
,	,	kIx,	,
když	když	k8xS	když
zaručuje	zaručovat	k5eAaImIp3nS	zaručovat
nezávislost	nezávislost	k1gFnSc4	nezávislost
a	a	k8xC	a
nestrannost	nestrannost	k1gFnSc4	nestrannost
soudců	soudce	k1gMnPc2	soudce
–	–	k?	–
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
nezávislost	nezávislost	k1gFnSc4	nezávislost
vlastní	vlastní	k2eAgFnSc2d1	vlastní
rozhodovací	rozhodovací	k2eAgFnSc2d1	rozhodovací
činnosti	činnost	k1gFnSc2	činnost
soudu	soud	k1gInSc2	soud
<g/>
.	.	kIx.	.
</s>
<s>
Články	článek	k1gInPc1	článek
83	[number]	k4	83
až	až	k8xS	až
89	[number]	k4	89
upravují	upravovat	k5eAaImIp3nP	upravovat
postavení	postavení	k1gNnSc4	postavení
Ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
ústavním	ústavní	k2eAgInSc7d1	ústavní
orgánem	orgán	k1gInSc7	orgán
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
jeho	on	k3xPp3gNnSc2	on
postavení	postavení	k1gNnSc2	postavení
a	a	k8xC	a
kompetence	kompetence	k1gFnPc1	kompetence
opírají	opírat	k5eAaImIp3nP	opírat
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
ústavní	ústavní	k2eAgFnSc4d1	ústavní
úpravu	úprava	k1gFnSc4	úprava
<g/>
.	.	kIx.	.
</s>
<s>
Nepřipadá	připadat	k5eNaImIp3nS	připadat
v	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
rozšíření	rozšíření	k1gNnSc1	rozšíření
či	či	k8xC	či
zúžení	zúžení	k1gNnSc1	zúžení
kompetencí	kompetence	k1gFnPc2	kompetence
obyčejným	obyčejný	k2eAgInSc7d1	obyčejný
zákonem	zákon	k1gInSc7	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Východiskem	východisko	k1gNnSc7	východisko
pro	pro	k7c4	pro
ústavní	ústavní	k2eAgFnSc4d1	ústavní
úpravu	úprava	k1gFnSc4	úprava
Ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
je	být	k5eAaImIp3nS	být
článek	článek	k1gInSc1	článek
83	[number]	k4	83
<g/>
,	,	kIx,	,
z	z	k7c2	z
nějž	jenž	k3xRgInSc2	jenž
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
obecná	obecný	k2eAgFnSc1d1	obecná
charakteristika	charakteristika	k1gFnSc1	charakteristika
soudu	soud	k1gInSc2	soud
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnPc4	jeho
kompetence	kompetence	k1gFnPc4	kompetence
a	a	k8xC	a
zásady	zásada	k1gFnPc4	zásada
fungování	fungování	k1gNnSc2	fungování
a	a	k8xC	a
právě	právě	k9	právě
tento	tento	k3xDgInSc1	tento
článek	článek	k1gInSc1	článek
je	být	k5eAaImIp3nS	být
základem	základ	k1gInSc7	základ
pro	pro	k7c4	pro
rozvinutí	rozvinutí	k1gNnSc4	rozvinutí
kompetence	kompetence	k1gFnSc2	kompetence
Ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
nelze	lze	k6eNd1	lze
dovozovat	dovozovat	k5eAaImF	dovozovat
jen	jen	k9	jen
ze	z	k7c2	z
článku	článek	k1gInSc2	článek
87	[number]	k4	87
<g/>
.	.	kIx.	.
</s>
<s>
Článek	článek	k1gInSc1	článek
84	[number]	k4	84
upravuje	upravovat	k5eAaImIp3nS	upravovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ústavní	ústavní	k2eAgInSc1d1	ústavní
soud	soud	k1gInSc1	soud
tvoří	tvořit	k5eAaImIp3nP	tvořit
soudci	soudce	k1gMnPc1	soudce
<g/>
,	,	kIx,	,
jichž	jenž	k3xRgMnPc2	jenž
je	být	k5eAaImIp3nS	být
patnáct	patnáct	k4xCc4	patnáct
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
prezident	prezident	k1gMnSc1	prezident
republiky	republika	k1gFnSc2	republika
se	se	k3xPyFc4	se
souhlasem	souhlas	k1gInSc7	souhlas
Senátu	senát	k1gInSc2	senát
na	na	k7c4	na
dobu	doba	k1gFnSc4	doba
deseti	deset	k4xCc2	deset
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Článek	článek	k1gInSc1	článek
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
čtvrtém	čtvrtý	k4xOgInSc6	čtvrtý
odstavci	odstavec	k1gInSc6	odstavec
upravuje	upravovat	k5eAaImIp3nS	upravovat
předpoklady	předpoklad	k1gInPc4	předpoklad
jmenování	jmenování	k1gNnSc2	jmenování
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
ústavního	ústavní	k2eAgMnSc2d1	ústavní
soudce	soudce	k1gMnSc2	soudce
<g/>
.	.	kIx.	.
</s>
<s>
Slib	slib	k1gInSc1	slib
soudce	soudce	k1gMnSc2	soudce
Ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
je	být	k5eAaImIp3nS	být
upraven	upravit	k5eAaPmNgInS	upravit
v	v	k7c6	v
článku	článek	k1gInSc6	článek
85	[number]	k4	85
Ústavy	ústava	k1gFnSc2	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Soudci	soudce	k1gMnPc1	soudce
Ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
mají	mít	k5eAaImIp3nP	mít
z	z	k7c2	z
dikce	dikce	k1gFnSc2	dikce
článku	článek	k1gInSc2	článek
86	[number]	k4	86
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
Ústavy	ústava	k1gFnSc2	ústava
vložen	vložit	k5eAaPmNgInS	vložit
až	až	k9	až
při	při	k7c6	při
schvalování	schvalování	k1gNnSc6	schvalování
16	[number]	k4	16
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
imunitu	imunita	k1gFnSc4	imunita
prakticky	prakticky	k6eAd1	prakticky
obdobnou	obdobný	k2eAgFnSc4d1	obdobná
jako	jako	k8xS	jako
členové	člen	k1gMnPc1	člen
Parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
kompetence	kompetence	k1gFnPc1	kompetence
Ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
jsou	být	k5eAaImIp3nP	být
uvedeny	uvést	k5eAaPmNgFnP	uvést
v	v	k7c6	v
článku	článek	k1gInSc6	článek
87	[number]	k4	87
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
nejedná	jednat	k5eNaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
výčet	výčet	k1gInSc4	výčet
veškeré	veškerý	k3xTgFnSc2	veškerý
působnosti	působnost	k1gFnSc2	působnost
Ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
<g/>
.	.	kIx.	.
</s>
<s>
Pravidla	pravidlo	k1gNnPc1	pravidlo
řízení	řízení	k1gNnSc2	řízení
před	před	k7c7	před
Ústavním	ústavní	k2eAgInSc7d1	ústavní
soudem	soud	k1gInSc7	soud
jsou	být	k5eAaImIp3nP	být
vymezena	vymezit	k5eAaPmNgFnS	vymezit
v	v	k7c6	v
článku	článek	k1gInSc6	článek
88	[number]	k4	88
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
odstavci	odstavec	k1gInSc6	odstavec
odkaz	odkaz	k1gInSc4	odkaz
na	na	k7c4	na
zákon	zákon	k1gInSc4	zákon
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
má	mít	k5eAaImIp3nS	mít
stanovit	stanovit	k5eAaPmF	stanovit
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
a	a	k8xC	a
za	za	k7c2	za
jakých	jaký	k3yRgFnPc2	jaký
podmínek	podmínka	k1gFnPc2	podmínka
je	být	k5eAaImIp3nS	být
oprávněn	oprávněn	k2eAgMnSc1d1	oprávněn
podat	podat	k5eAaPmF	podat
návrh	návrh	k1gInSc4	návrh
na	na	k7c4	na
zahájení	zahájení	k1gNnSc4	zahájení
řízení	řízení	k1gNnSc2	řízení
a	a	k8xC	a
další	další	k2eAgNnPc1d1	další
pravidla	pravidlo	k1gNnPc1	pravidlo
o	o	k7c4	o
řízení	řízení	k1gNnSc4	řízení
před	před	k7c7	před
Ústavním	ústavní	k2eAgInSc7d1	ústavní
soudem	soud	k1gInSc7	soud
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dikce	dikce	k1gFnSc2	dikce
článku	článek	k1gInSc2	článek
88	[number]	k4	88
odst	odsta	k1gFnPc2	odsta
<g/>
.	.	kIx.	.
2	[number]	k4	2
jsou	být	k5eAaImIp3nP	být
soudci	soudce	k1gMnPc1	soudce
Ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
při	při	k7c6	při
svém	svůj	k3xOyFgNnSc6	svůj
rozhodování	rozhodování	k1gNnSc6	rozhodování
vázáni	vázat	k5eAaImNgMnP	vázat
pouze	pouze	k6eAd1	pouze
ústavním	ústavní	k2eAgInSc7d1	ústavní
pořádkem	pořádek	k1gInSc7	pořádek
a	a	k8xC	a
zákonem	zákon	k1gInSc7	zákon
dle	dle	k7c2	dle
prvního	první	k4xOgInSc2	první
odstavce	odstavec	k1gInSc2	odstavec
tohoto	tento	k3xDgInSc2	tento
článku	článek	k1gInSc2	článek
–	–	k?	–
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
především	především	k9	především
o	o	k7c4	o
zákon	zákon	k1gInSc4	zákon
o	o	k7c6	o
Ústavním	ústavní	k2eAgInSc6d1	ústavní
soudu	soud	k1gInSc6	soud
č.	č.	k?	č.
182	[number]	k4	182
<g/>
/	/	kIx~	/
<g/>
1993	[number]	k4	1993
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
Sporná	sporný	k2eAgFnSc1d1	sporná
se	se	k3xPyFc4	se
jeví	jevit	k5eAaImIp3nS	jevit
otázka	otázka	k1gFnSc1	otázka
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
může	moct	k5eAaImIp3nS	moct
Ústavní	ústavní	k2eAgInSc1d1	ústavní
soud	soud	k1gInSc1	soud
přezkoumat	přezkoumat	k5eAaPmF	přezkoumat
ústavní	ústavní	k2eAgInSc4d1	ústavní
zákon	zákon	k1gInSc4	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
Filip	Filip	k1gMnSc1	Filip
a	a	k8xC	a
Klíma	Klíma	k1gMnSc1	Klíma
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
stanovisku	stanovisko	k1gNnSc6	stanovisko
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ústavní	ústavní	k2eAgInSc1d1	ústavní
soud	soud	k1gInSc1	soud
může	moct	k5eAaImIp3nS	moct
takový	takový	k3xDgInSc4	takový
ústavní	ústavní	k2eAgInSc4d1	ústavní
zákon	zákon	k1gInSc4	zákon
přezkoumat	přezkoumat	k5eAaPmF	přezkoumat
jako	jako	k8xS	jako
každý	každý	k3xTgMnSc1	každý
jiný	jiný	k2eAgMnSc1d1	jiný
<g/>
,	,	kIx,	,
jiní	jiný	k2eAgMnPc1d1	jiný
autoři	autor	k1gMnPc1	autor
–	–	k?	–
například	například	k6eAd1	například
Pavlíček	Pavlíček	k1gMnSc1	Pavlíček
–	–	k?	–
razí	razit	k5eAaImIp3nS	razit
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ústavní	ústavní	k2eAgInSc1d1	ústavní
soud	soud	k1gInSc1	soud
ústavní	ústavní	k2eAgInSc4d1	ústavní
zákon	zákon	k1gInSc4	zákon
přezkoumat	přezkoumat	k5eAaPmF	přezkoumat
či	či	k8xC	či
ani	ani	k8xC	ani
částečně	částečně	k6eAd1	částečně
zrušit	zrušit	k5eAaPmF	zrušit
nemůže	moct	k5eNaImIp3nS	moct
a	a	k8xC	a
nemůže	moct	k5eNaImIp3nS	moct
se	se	k3xPyFc4	se
od	od	k7c2	od
něj	on	k3xPp3gMnSc2	on
ani	ani	k8xC	ani
odchýlit	odchýlit	k5eAaPmF	odchýlit
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
Ústavní	ústavní	k2eAgInSc1d1	ústavní
soud	soud	k1gInSc1	soud
otázku	otázka	k1gFnSc4	otázka
vyřešil	vyřešit	k5eAaPmAgInS	vyřešit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zrušil	zrušit	k5eAaPmAgInS	zrušit
ústavní	ústavní	k2eAgInSc1d1	ústavní
zákon	zákon	k1gInSc1	zákon
o	o	k7c4	o
zkrácení	zkrácení	k1gNnSc4	zkrácení
pátého	pátý	k4xOgNnSc2	pátý
volebního	volební	k2eAgNnSc2d1	volební
období	období	k1gNnSc2	období
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
.	.	kIx.	.
</s>
<s>
Vykonatelnost	vykonatelnost	k1gFnSc1	vykonatelnost
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
Ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
je	být	k5eAaImIp3nS	být
upravena	upravit	k5eAaPmNgFnS	upravit
v	v	k7c6	v
článku	článek	k1gInSc6	článek
89	[number]	k4	89
odst	odstum	k1gNnPc2	odstum
<g/>
.	.	kIx.	.
1	[number]	k4	1
Ústavy	ústava	k1gFnSc2	ústava
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc1	druhý
odstavec	odstavec	k1gInSc1	odstavec
pak	pak	k6eAd1	pak
upravuje	upravovat	k5eAaImIp3nS	upravovat
závaznost	závaznost	k1gFnSc4	závaznost
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
Ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
závaznosti	závaznost	k1gFnSc3	závaznost
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
protichůdná	protichůdný	k2eAgNnPc1d1	protichůdné
stanoviska	stanovisko	k1gNnPc1	stanovisko
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
závazná	závazný	k2eAgNnPc1d1	závazné
všechna	všechen	k3xTgNnPc4	všechen
rozhodnutí	rozhodnutí	k1gNnSc3	rozhodnutí
Ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
včetně	včetně	k7c2	včetně
nosných	nosný	k2eAgInPc2d1	nosný
důvodů	důvod	k1gInPc2	důvod
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
že	že	k8xS	že
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
výroků	výrok	k1gInPc2	výrok
nálezů	nález	k1gInPc2	nález
<g/>
,	,	kIx,	,
kterými	který	k3yRgInPc7	který
se	se	k3xPyFc4	se
zrušují	zrušovat	k5eAaImIp3nP	zrušovat
právní	právní	k2eAgInPc1d1	právní
předpisy	předpis	k1gInPc1	předpis
či	či	k8xC	či
jejich	jejich	k3xOp3gNnPc1	jejich
ustanovení	ustanovení	k1gNnPc1	ustanovení
<g/>
,	,	kIx,	,
nejsou	být	k5eNaImIp3nP	být
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
Ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
obecně	obecně	k6eAd1	obecně
závazná	závazný	k2eAgFnSc1d1	závazná
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštní	zvláštní	k2eAgFnSc1d1	zvláštní
úprava	úprava	k1gFnSc1	úprava
pro	pro	k7c4	pro
případ	případ	k1gInSc4	případ
preventivní	preventivní	k2eAgFnSc2d1	preventivní
kontroly	kontrola	k1gFnSc2	kontrola
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
smluv	smlouva	k1gFnPc2	smlouva
je	být	k5eAaImIp3nS	být
upraven	upravit	k5eAaPmNgInS	upravit
ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
odstavci	odstavec	k1gInSc6	odstavec
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc1d1	základní
funkce	funkce	k1gFnSc1	funkce
soudů	soud	k1gInPc2	soud
je	být	k5eAaImIp3nS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
v	v	k7c6	v
článku	článek	k1gInSc6	článek
90	[number]	k4	90
Ústavy	ústava	k1gFnSc2	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Soudy	soud	k1gInPc1	soud
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
dikce	dikce	k1gFnSc2	dikce
Ústavy	ústava	k1gFnSc2	ústava
povolány	povolán	k2eAgFnPc1d1	povolána
především	především	k6eAd1	především
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zákonem	zákon	k1gInSc7	zákon
stanoveným	stanovený	k2eAgInSc7d1	stanovený
způsobem	způsob	k1gInSc7	způsob
poskytovaly	poskytovat	k5eAaImAgInP	poskytovat
ochranu	ochrana	k1gFnSc4	ochrana
právům	právo	k1gNnPc3	právo
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
soud	soud	k1gInSc1	soud
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
o	o	k7c6	o
vině	vina	k1gFnSc6	vina
a	a	k8xC	a
trestu	trest	k1gInSc6	trest
za	za	k7c4	za
trestné	trestný	k2eAgInPc4d1	trestný
činy	čin	k1gInPc4	čin
<g/>
.	.	kIx.	.
</s>
<s>
Soustava	soustava	k1gFnSc1	soustava
soudů	soud	k1gInPc2	soud
tvořená	tvořený	k2eAgNnPc4d1	tvořené
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
soudem	soud	k1gInSc7	soud
<g/>
,	,	kIx,	,
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
správním	správní	k2eAgInSc7d1	správní
soudem	soud	k1gInSc7	soud
<g/>
,	,	kIx,	,
vrchními	vrchní	k1gFnPc7	vrchní
<g/>
,	,	kIx,	,
krajskými	krajský	k2eAgInPc7d1	krajský
a	a	k8xC	a
okresními	okresní	k2eAgInPc7d1	okresní
soudy	soud	k1gInPc7	soud
je	být	k5eAaImIp3nS	být
upravena	upravit	k5eAaPmNgFnS	upravit
v	v	k7c6	v
článku	článek	k1gInSc6	článek
91	[number]	k4	91
odst	odstum	k1gNnPc2	odstum
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
odstavec	odstavec	k1gInSc1	odstavec
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
působnost	působnost	k1gFnSc4	působnost
a	a	k8xC	a
organizaci	organizace	k1gFnSc4	organizace
soudů	soud	k1gInPc2	soud
stanoví	stanovit	k5eAaPmIp3nS	stanovit
zákon	zákon	k1gInSc1	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Čtyřčlánková	čtyřčlánkový	k2eAgFnSc1d1	čtyřčlánková
soudní	soudní	k2eAgFnSc1d1	soudní
soustava	soustava	k1gFnSc1	soustava
s	s	k7c7	s
vrchními	vrchní	k2eAgInPc7d1	vrchní
soudy	soud	k1gInPc7	soud
je	být	k5eAaImIp3nS	být
kompromisem	kompromis	k1gInSc7	kompromis
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
řešil	řešit	k5eAaImAgInS	řešit
konkurenci	konkurence	k1gFnSc4	konkurence
dvou	dva	k4xCgFnPc2	dva
institucí	instituce	k1gFnPc2	instituce
v	v	k7c4	v
předvečer	předvečer	k1gInSc4	předvečer
zániku	zánik	k1gInSc2	zánik
federace	federace	k1gFnSc2	federace
–	–	k?	–
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
federálního	federální	k2eAgInSc2d1	federální
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
a	a	k8xC	a
republikového	republikový	k2eAgInSc2d1	republikový
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
situace	situace	k1gFnSc1	situace
neřešila	řešit	k5eNaImAgFnS	řešit
rušením	rušení	k1gNnSc7	rušení
instituce	instituce	k1gFnSc2	instituce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
právě	právě	k9	právě
vznikem	vznik	k1gInSc7	vznik
vrchních	vrchní	k2eAgInPc2d1	vrchní
soudů	soud	k1gInPc2	soud
–	–	k?	–
navíc	navíc	k6eAd1	navíc
se	se	k3xPyFc4	se
dosavadní	dosavadní	k2eAgMnSc1d1	dosavadní
předseda	předseda	k1gMnSc1	předseda
federálního	federální	k2eAgInSc2d1	federální
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
Otakar	Otakar	k1gMnSc1	Otakar
Motejl	Motejl	k1gMnSc1	Motejl
stal	stát	k5eAaPmAgMnS	stát
předsedou	předseda	k1gMnSc7	předseda
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
a	a	k8xC	a
dosavadní	dosavadní	k2eAgMnSc1d1	dosavadní
předseda	předseda	k1gMnSc1	předseda
republikového	republikový	k2eAgInSc2d1	republikový
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
Antonín	Antonín	k1gMnSc1	Antonín
Mokrý	Mokrý	k1gMnSc1	Mokrý
předsedou	předseda	k1gMnSc7	předseda
Vrchního	vrchní	k2eAgInSc2d1	vrchní
soudu	soud	k1gInSc2	soud
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
platilo	platit	k5eAaImAgNnS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
soudci	soudce	k1gMnPc1	soudce
přidělení	přidělení	k1gNnSc2	přidělení
k	k	k7c3	k
výkonu	výkon	k1gInSc3	výkon
funkce	funkce	k1gFnSc2	funkce
k	k	k7c3	k
"	"	kIx"	"
<g/>
původnímu	původní	k2eAgInSc3d1	původní
<g/>
"	"	kIx"	"
Nejvyššímu	vysoký	k2eAgInSc3d3	Nejvyšší
soudu	soud	k1gInSc3	soud
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
stali	stát	k5eAaPmAgMnP	stát
soudci	soudce	k1gMnPc1	soudce
přidělenými	přidělený	k2eAgFnPc7d1	přidělená
k	k	k7c3	k
výkonu	výkon	k1gInSc3	výkon
funkce	funkce	k1gFnSc2	funkce
k	k	k7c3	k
Vrchnímu	vrchní	k2eAgInSc3d1	vrchní
soudu	soud	k1gInSc3	soud
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jim	on	k3xPp3gMnPc3	on
u	u	k7c2	u
Vrchního	vrchní	k2eAgInSc2d1	vrchní
soudu	soud	k1gInSc2	soud
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
zachovány	zachovat	k5eAaPmNgFnP	zachovat
i	i	k9	i
jejich	jejich	k3xOp3gFnPc1	jejich
funkce	funkce	k1gFnPc1	funkce
<g/>
,	,	kIx,	,
do	do	k7c2	do
nichž	jenž	k3xRgMnPc2	jenž
byli	být	k5eAaImAgMnP	být
jmenováni	jmenovat	k5eAaBmNgMnP	jmenovat
u	u	k7c2	u
"	"	kIx"	"
<g/>
původního	původní	k2eAgInSc2d1	původní
<g/>
"	"	kIx"	"
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
přešli	přejít	k5eAaPmAgMnP	přejít
k	k	k7c3	k
Vrchnímu	vrchní	k2eAgInSc3d1	vrchní
soudu	soud	k1gInSc3	soud
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
bez	bez	k7c2	bez
změny	změna	k1gFnSc2	změna
ve	v	k7c6	v
funkčním	funkční	k2eAgNnSc6d1	funkční
zařazení	zařazení	k1gNnSc6	zařazení
pracovníci	pracovník	k1gMnPc1	pracovník
"	"	kIx"	"
<g/>
původního	původní	k2eAgInSc2d1	původní
<g/>
"	"	kIx"	"
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
ústavní	ústavní	k2eAgFnSc6d1	ústavní
úrovni	úroveň	k1gFnSc6	úroveň
zakotven	zakotvit	k5eAaPmNgInS	zakotvit
v	v	k7c6	v
článku	článek	k1gInSc6	článek
92	[number]	k4	92
Ústavy	ústava	k1gFnSc2	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Jmenování	jmenování	k1gNnSc1	jmenování
soudců	soudce	k1gMnPc2	soudce
prezidentem	prezident	k1gMnSc7	prezident
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
předpoklady	předpoklad	k1gInPc4	předpoklad
pro	pro	k7c4	pro
výkon	výkon	k1gInSc4	výkon
funkce	funkce	k1gFnSc2	funkce
soudce	soudce	k1gMnSc2	soudce
upravuje	upravovat	k5eAaImIp3nS	upravovat
článek	článek	k1gInSc1	článek
93	[number]	k4	93
Ústavy	ústava	k1gFnSc2	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Způsob	způsob	k1gInSc1	způsob
rozhodování	rozhodování	k1gNnSc2	rozhodování
soudců	soudce	k1gMnPc2	soudce
upravuje	upravovat	k5eAaImIp3nS	upravovat
článek	článek	k1gInSc1	článek
94	[number]	k4	94
<g/>
,	,	kIx,	,
vázanost	vázanost	k1gFnSc1	vázanost
soudců	soudce	k1gMnPc2	soudce
zákonem	zákon	k1gInSc7	zákon
a	a	k8xC	a
mezinárodní	mezinárodní	k2eAgFnSc7d1	mezinárodní
smlouvou	smlouva	k1gFnSc7	smlouva
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
právního	právní	k2eAgInSc2d1	právní
řádu	řád	k1gInSc2	řád
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
předložit	předložit	k5eAaPmF	předložit
věc	věc	k1gFnSc4	věc
Ústavnímu	ústavní	k2eAgInSc3d1	ústavní
soudu	soud	k1gInSc3	soud
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
soud	soud	k1gInSc1	soud
domnívá	domnívat	k5eAaImIp3nS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
zákon	zákon	k1gInSc1	zákon
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
použito	použít	k5eAaPmNgNnS	použít
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
ústavním	ústavní	k2eAgInSc7d1	ústavní
pořádkem	pořádek	k1gInSc7	pořádek
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zakotvena	zakotvit	k5eAaPmNgFnS	zakotvit
v	v	k7c6	v
článku	článek	k1gInSc6	článek
95	[number]	k4	95
Ústavy	ústava	k1gFnSc2	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgNnPc1d1	základní
pravidla	pravidlo	k1gNnPc1	pravidlo
soudního	soudní	k2eAgNnSc2d1	soudní
řízení	řízení	k1gNnSc2	řízení
–	–	k?	–
rovnost	rovnost	k1gFnSc1	rovnost
účastníků	účastník	k1gMnPc2	účastník
<g/>
,	,	kIx,	,
ústnost	ústnost	k1gFnSc1	ústnost
a	a	k8xC	a
veřejnost	veřejnost	k1gFnSc1	veřejnost
–	–	k?	–
jsou	být	k5eAaImIp3nP	být
upravena	upravit	k5eAaPmNgFnS	upravit
v	v	k7c6	v
článku	článek	k1gInSc6	článek
96	[number]	k4	96
<g/>
.	.	kIx.	.
</s>
<s>
Postavení	postavení	k1gNnSc1	postavení
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
kontrolního	kontrolní	k2eAgInSc2d1	kontrolní
úřadu	úřad	k1gInSc2	úřad
(	(	kIx(	(
<g/>
NKÚ	NKÚ	kA	NKÚ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc4	jeho
základní	základní	k2eAgFnSc4d1	základní
funkci	funkce	k1gFnSc4	funkce
<g/>
,	,	kIx,	,
proces	proces	k1gInSc1	proces
konstituování	konstituování	k1gNnSc2	konstituování
NKÚ	NKÚ	kA	NKÚ
a	a	k8xC	a
odkaz	odkaz	k1gInSc1	odkaz
na	na	k7c4	na
další	další	k2eAgFnSc4d1	další
úpravu	úprava	k1gFnSc4	úprava
postavení	postavení	k1gNnSc2	postavení
<g/>
,	,	kIx,	,
působnosti	působnost	k1gFnSc2	působnost
<g/>
,	,	kIx,	,
organizační	organizační	k2eAgFnSc2d1	organizační
struktury	struktura	k1gFnSc2	struktura
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
otázek	otázka	k1gFnPc2	otázka
je	být	k5eAaImIp3nS	být
obsažen	obsáhnout	k5eAaPmNgInS	obsáhnout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
97	[number]	k4	97
Ústavy	ústava	k1gFnSc2	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Zakotvení	zakotvení	k1gNnSc1	zakotvení
postavení	postavení	k1gNnSc2	postavení
a	a	k8xC	a
působnosti	působnost	k1gFnSc2	působnost
České	český	k2eAgFnSc2d1	Česká
národní	národní	k2eAgFnSc2d1	národní
banky	banka	k1gFnSc2	banka
(	(	kIx(	(
<g/>
ČNB	ČNB	kA	ČNB
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
článku	článek	k1gInSc6	článek
98	[number]	k4	98
Ústavy	ústava	k1gFnSc2	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
ustanovení	ustanovení	k1gNnSc2	ustanovení
je	být	k5eAaImIp3nS	být
ČNB	ČNB	kA	ČNB
ústřední	ústřední	k2eAgFnSc7d1	ústřední
bankou	banka	k1gFnSc7	banka
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
hlavním	hlavní	k2eAgInSc7d1	hlavní
cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
péče	péče	k1gFnSc1	péče
o	o	k7c4	o
cenovou	cenový	k2eAgFnSc4d1	cenová
stabilitu	stabilita	k1gFnSc4	stabilita
a	a	k8xC	a
do	do	k7c2	do
jejíž	jejíž	k3xOyRp3gFnSc2	jejíž
činnosti	činnost	k1gFnSc2	činnost
lze	lze	k6eAd1	lze
zasahovat	zasahovat	k5eAaImF	zasahovat
toliko	toliko	k6eAd1	toliko
na	na	k7c6	na
základě	základ	k1gInSc6	základ
zákona	zákon	k1gInSc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Zasazení	zasazení	k1gNnSc4	zasazení
ČNB	ČNB	kA	ČNB
do	do	k7c2	do
Ústavy	ústava	k1gFnSc2	ústava
žádal	žádat	k5eAaImAgMnS	žádat
po	po	k7c6	po
právnících	právník	k1gMnPc6	právník
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
Ústavu	ústav	k1gInSc3	ústav
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
připravovali	připravovat	k5eAaImAgMnP	připravovat
<g/>
,	,	kIx,	,
guvernér	guvernér	k1gMnSc1	guvernér
centrální	centrální	k2eAgFnSc2d1	centrální
banky	banka	k1gFnSc2	banka
Josef	Josef	k1gMnSc1	Josef
Tošovský	Tošovský	k1gMnSc1	Tošovský
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
však	však	k9	však
ústavní	ústavní	k2eAgNnSc1d1	ústavní
zakotvení	zakotvení	k1gNnSc1	zakotvení
banky	banka	k1gFnSc2	banka
nebylo	být	k5eNaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
vhodně	vhodně	k6eAd1	vhodně
zasadit	zasadit	k5eAaPmF	zasadit
do	do	k7c2	do
trojdělené	trojdělený	k2eAgFnSc2d1	trojdělený
státní	státní	k2eAgFnSc2d1	státní
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
získala	získat	k5eAaPmAgFnS	získat
ČNB	ČNB	kA	ČNB
vlastní	vlastní	k2eAgFnSc4d1	vlastní
hlavu	hlava	k1gFnSc4	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Územní	územní	k2eAgFnSc1d1	územní
samospráva	samospráva	k1gFnSc1	samospráva
je	být	k5eAaImIp3nS	být
upravena	upravit	k5eAaPmNgFnS	upravit
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
99	[number]	k4	99
až	až	k9	až
105	[number]	k4	105
<g/>
.	.	kIx.	.
</s>
<s>
Článek	článek	k1gInSc1	článek
99	[number]	k4	99
je	být	k5eAaImIp3nS	být
základním	základní	k2eAgNnSc7d1	základní
východiskem	východisko	k1gNnSc7	východisko
pro	pro	k7c4	pro
vnitřní	vnitřní	k2eAgNnSc4d1	vnitřní
územně-samosprávné	územněamosprávný	k2eAgNnSc4d1	územně-samosprávný
členění	členění	k1gNnSc4	členění
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
od	od	k7c2	od
územně-správního	územněprávní	k2eAgNnSc2d1	územně-správní
členění	členění	k1gNnSc2	členění
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
základem	základ	k1gInSc7	základ
je	být	k5eAaImIp3nS	být
zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
36	[number]	k4	36
<g/>
/	/	kIx~	/
<g/>
1960	[number]	k4	1960
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
republiku	republika	k1gFnSc4	republika
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
kraje	kraj	k1gInPc4	kraj
<g/>
,	,	kIx,	,
okresy	okres	k1gInPc1	okres
<g/>
,	,	kIx,	,
obce	obec	k1gFnPc1	obec
a	a	k8xC	a
vojenské	vojenský	k2eAgInPc1d1	vojenský
újezdy	újezd	k1gInPc1	újezd
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dikce	dikce	k1gFnSc2	dikce
článku	článek	k1gInSc2	článek
99	[number]	k4	99
se	se	k3xPyFc4	se
republika	republika	k1gFnSc1	republika
územně-samosprávně	územněamosprávně	k6eAd1	územně-samosprávně
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
obce	obec	k1gFnPc4	obec
(	(	kIx(	(
<g/>
základní	základní	k2eAgInPc4d1	základní
územní	územní	k2eAgInPc4d1	územní
samosprávné	samosprávný	k2eAgInPc4d1	samosprávný
celky	celek	k1gInPc4	celek
<g/>
)	)	kIx)	)
a	a	k8xC	a
kraje	kraj	k1gInSc2	kraj
(	(	kIx(	(
<g/>
vyšší	vysoký	k2eAgInPc1d2	vyšší
územní	územní	k2eAgInPc1d1	územní
samosprávné	samosprávný	k2eAgInPc1d1	samosprávný
celky	celek	k1gInPc1	celek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vyšší	vysoký	k2eAgInPc1d2	vyšší
územní	územní	k2eAgInPc1d1	územní
samosprávné	samosprávný	k2eAgInPc1d1	samosprávný
celky	celek	k1gInPc1	celek
začaly	začít	k5eAaPmAgInP	začít
fungovat	fungovat	k5eAaImF	fungovat
až	až	k9	až
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Dřívější	dřívější	k2eAgNnSc1d1	dřívější
znění	znění	k1gNnSc1	znění
tohoto	tento	k3xDgInSc2	tento
článku	článek	k1gInSc2	článek
před	před	k7c7	před
reformou	reforma	k1gFnSc7	reforma
veřejné	veřejný	k2eAgFnSc2d1	veřejná
správy	správa	k1gFnSc2	správa
bylo	být	k5eAaImAgNnS	být
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
se	se	k3xPyFc4	se
člení	členit	k5eAaImIp3nS	členit
na	na	k7c4	na
obce	obec	k1gFnPc4	obec
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
základními	základní	k2eAgInPc7d1	základní
územními	územní	k2eAgInPc7d1	územní
samosprávnými	samosprávný	k2eAgInPc7d1	samosprávný
celky	celek	k1gInPc7	celek
<g/>
.	.	kIx.	.
</s>
<s>
Vyššími	vysoký	k2eAgInPc7d2	vyšší
územními	územní	k2eAgInPc7d1	územní
samosprávnými	samosprávný	k2eAgInPc7d1	samosprávný
celky	celek	k1gInPc7	celek
jsou	být	k5eAaImIp3nP	být
země	zem	k1gFnSc2	zem
nebo	nebo	k8xC	nebo
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Územně	územně	k6eAd1	územně
samosprávné	samosprávný	k2eAgInPc4d1	samosprávný
celky	celek	k1gInPc4	celek
charakterizované	charakterizovaný	k2eAgInPc4d1	charakterizovaný
jako	jako	k8xS	jako
územní	územní	k2eAgNnSc4d1	územní
společenství	společenství	k1gNnSc4	společenství
občanů	občan	k1gMnPc2	občan
(	(	kIx(	(
<g/>
čl	čl	kA	čl
<g/>
.	.	kIx.	.
100	[number]	k4	100
odst	odsta	k1gFnPc2	odsta
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
)	)	kIx)	)
mají	mít	k5eAaImIp3nP	mít
také	také	k9	také
znak	znak	k1gInSc4	znak
ekonomický	ekonomický	k2eAgInSc4d1	ekonomický
a	a	k8xC	a
znak	znak	k1gInSc1	znak
samostatné	samostatný	k2eAgFnSc2d1	samostatná
správy	správa	k1gFnSc2	správa
svých	svůj	k3xOyFgFnPc2	svůj
záležitostí	záležitost	k1gFnPc2	záležitost
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
svých	svůj	k3xOyFgInPc2	svůj
orgánů	orgán	k1gInPc2	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc1	obec
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
součástí	součást	k1gFnSc7	součást
vyššího	vysoký	k2eAgInSc2d2	vyšší
územního	územní	k2eAgInSc2d1	územní
samosprávného	samosprávný	k2eAgInSc2d1	samosprávný
celku	celek	k1gInSc2	celek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
lze	lze	k6eAd1	lze
vytvořit	vytvořit	k5eAaPmF	vytvořit
či	či	k8xC	či
zrušit	zrušit	k5eAaPmF	zrušit
jen	jen	k9	jen
ústavním	ústavní	k2eAgInSc7d1	ústavní
zákonem	zákon	k1gInSc7	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Úprava	úprava	k1gFnSc1	úprava
správy	správa	k1gFnSc2	správa
<g/>
,	,	kIx,	,
činnosti	činnost	k1gFnSc2	činnost
a	a	k8xC	a
hospodaření	hospodaření	k1gNnSc2	hospodaření
územních	územní	k2eAgInPc2d1	územní
samosprávných	samosprávný	k2eAgInPc2d1	samosprávný
celků	celek	k1gInPc2	celek
upravuje	upravovat	k5eAaImIp3nS	upravovat
v	v	k7c6	v
ústavní	ústavní	k2eAgFnSc3d1	ústavní
rovině	rovina	k1gFnSc3	rovina
článek	článek	k1gInSc1	článek
101	[number]	k4	101
Ústavy	ústava	k1gFnSc2	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgNnSc1d1	základní
východisko	východisko	k1gNnSc1	východisko
pro	pro	k7c4	pro
volby	volba	k1gFnPc4	volba
do	do	k7c2	do
zastupitelstev	zastupitelstvo	k1gNnPc2	zastupitelstvo
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
je	být	k5eAaImIp3nS	být
jediný	jediný	k2eAgInSc1d1	jediný
Ústavou	ústava	k1gFnSc7	ústava
výslovně	výslovně	k6eAd1	výslovně
předpokládaný	předpokládaný	k2eAgInSc1d1	předpokládaný
orgán	orgán	k1gInSc1	orgán
územních	územní	k2eAgInPc2d1	územní
samosprávných	samosprávný	k2eAgInPc2d1	samosprávný
celků	celek	k1gInPc2	celek
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
obsaženo	obsáhnout	k5eAaPmNgNnS	obsáhnout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
102	[number]	k4	102
Ústavy	ústava	k1gFnSc2	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
zastupitelstev	zastupitelstvo	k1gNnPc2	zastupitelstvo
jsou	být	k5eAaImIp3nP	být
voleni	volit	k5eAaImNgMnP	volit
tajným	tajný	k2eAgInSc7d1	tajný
hlasování	hlasování	k1gNnSc4	hlasování
na	na	k7c6	na
základě	základ	k1gInSc6	základ
všeobecného	všeobecný	k2eAgNnSc2d1	všeobecné
<g/>
,	,	kIx,	,
rovného	rovný	k2eAgNnSc2d1	rovné
a	a	k8xC	a
přímého	přímý	k2eAgNnSc2d1	přímé
volebního	volební	k2eAgNnSc2d1	volební
práva	právo	k1gNnSc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Funkční	funkční	k2eAgNnSc1d1	funkční
období	období	k1gNnSc1	období
zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
čtyřleté	čtyřletý	k2eAgNnSc4d1	čtyřleté
<g/>
.	.	kIx.	.
</s>
<s>
Článek	článek	k1gInSc4	článek
103	[number]	k4	103
upravující	upravující	k2eAgInSc4d1	upravující
název	název	k1gInSc4	název
vyšších	vysoký	k2eAgInPc2d2	vyšší
územních	územní	k2eAgInPc2d1	územní
samosprávných	samosprávný	k2eAgInPc2d1	samosprávný
celků	celek	k1gInPc2	celek
byl	být	k5eAaImAgInS	být
vypuštěn	vypuštěn	k2eAgInSc1d1	vypuštěn
s	s	k7c7	s
účinností	účinnost	k1gFnSc7	účinnost
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
názvu	název	k1gInSc6	název
vyššího	vysoký	k2eAgInSc2d2	vyšší
územního	územní	k2eAgInSc2d1	územní
samosprávného	samosprávný	k2eAgInSc2d1	samosprávný
celku	celek	k1gInSc2	celek
mělo	mít	k5eAaImAgNnS	mít
rozhodovat	rozhodovat	k5eAaImF	rozhodovat
jeho	jeho	k3xOp3gNnSc1	jeho
zastupitelstvo	zastupitelstvo	k1gNnSc1	zastupitelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Ustanovení	ustanovení	k1gNnSc1	ustanovení
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
nemohlo	moct	k5eNaImAgNnS	moct
být	být	k5eAaImF	být
nikdy	nikdy	k6eAd1	nikdy
aplikováno	aplikován	k2eAgNnSc1d1	aplikováno
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
vyšší	vysoký	k2eAgInPc1d2	vyšší
územní	územní	k2eAgInPc1d1	územní
samosprávné	samosprávný	k2eAgInPc1d1	samosprávný
celky	celek	k1gInPc1	celek
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
právě	právě	k9	právě
až	až	k6eAd1	až
s	s	k7c7	s
účinností	účinnost	k1gFnSc7	účinnost
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Dřívější	dřívější	k2eAgNnSc1d1	dřívější
znění	znění	k1gNnSc1	znění
tohoto	tento	k3xDgInSc2	tento
článku	článek	k1gInSc2	článek
před	před	k7c7	před
reformou	reforma	k1gFnSc7	reforma
veřejné	veřejný	k2eAgFnSc2d1	veřejná
správy	správa	k1gFnSc2	správa
bylo	být	k5eAaImAgNnS	být
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
O	o	k7c6	o
názvu	název	k1gInSc6	název
vyššího	vysoký	k2eAgInSc2d2	vyšší
územního	územní	k2eAgInSc2d1	územní
samosprávného	samosprávný	k2eAgInSc2d1	samosprávný
celku	celek	k1gInSc2	celek
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
jeho	jeho	k3xOp3gNnSc1	jeho
zastupitelstvo	zastupitelstvo	k1gNnSc1	zastupitelstvo
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Působnost	působnost	k1gFnSc1	působnost
zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
upravuje	upravovat	k5eAaImIp3nS	upravovat
článek	článek	k1gInSc1	článek
104	[number]	k4	104
Ústavy	ústava	k1gFnSc2	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgInSc4	třetí
odstavec	odstavec	k1gInSc4	odstavec
tohoto	tento	k3xDgInSc2	tento
článku	článek	k1gInSc2	článek
zakládá	zakládat	k5eAaImIp3nS	zakládat
právo	právo	k1gNnSc4	právo
zastupitelstvem	zastupitelstvo	k1gNnSc7	zastupitelstvo
územních	územní	k2eAgInPc2d1	územní
samosprávných	samosprávný	k2eAgInPc2d1	samosprávný
celků	celek	k1gInPc2	celek
vydávat	vydávat	k5eAaPmF	vydávat
obecně	obecně	k6eAd1	obecně
závazné	závazný	k2eAgFnPc4d1	závazná
vyhlášky	vyhláška	k1gFnPc4	vyhláška
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
článku	článek	k1gInSc2	článek
100	[number]	k4	100
odst	odsta	k1gFnPc2	odsta
<g/>
.	.	kIx.	.
1	[number]	k4	1
lze	lze	k6eAd1	lze
zákonem	zákon	k1gInSc7	zákon
stanovit	stanovit	k5eAaPmF	stanovit
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jsou	být	k5eAaImIp3nP	být
územní	územní	k2eAgInPc4d1	územní
samosprávné	samosprávný	k2eAgInPc4d1	samosprávný
celky	celek	k1gInPc4	celek
též	též	k6eAd1	též
správními	správní	k2eAgInPc7d1	správní
obvody	obvod	k1gInPc7	obvod
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
jsou	být	k5eAaImIp3nP	být
územními	územní	k2eAgFnPc7d1	územní
jednotkami	jednotka	k1gFnPc7	jednotka
určenými	určený	k2eAgFnPc7d1	určená
pro	pro	k7c4	pro
výkon	výkon	k1gInSc4	výkon
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
(	(	kIx(	(
<g/>
čl	čl	kA	čl
<g/>
.	.	kIx.	.
105	[number]	k4	105
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
je	být	k5eAaImIp3nS	být
smíšený	smíšený	k2eAgInSc1d1	smíšený
model	model	k1gInSc1	model
výkonu	výkon	k1gInSc2	výkon
územní	územní	k2eAgFnSc2d1	územní
veřejné	veřejný	k2eAgFnSc2d1	veřejná
správy	správa	k1gFnSc2	správa
<g/>
,	,	kIx,	,
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
výkon	výkon	k1gInSc4	výkon
samosprávy	samospráva	k1gFnSc2	samospráva
i	i	k8xC	i
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
realizován	realizován	k2eAgInSc1d1	realizován
stejnými	stejný	k2eAgInPc7d1	stejný
orgány	orgán	k1gInPc7	orgán
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
územních	územní	k2eAgInPc2d1	územní
samosprávných	samosprávný	k2eAgInPc2d1	samosprávný
celků	celek	k1gInPc2	celek
<g/>
.	.	kIx.	.
</s>
<s>
Rozlišována	rozlišován	k2eAgFnSc1d1	rozlišována
je	být	k5eAaImIp3nS	být
samostatná	samostatný	k2eAgFnSc1d1	samostatná
a	a	k8xC	a
přenesená	přenesený	k2eAgFnSc1d1	přenesená
působnost	působnost	k1gFnSc1	působnost
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dikce	dikce	k1gFnSc2	dikce
článku	článek	k1gInSc2	článek
106	[number]	k4	106
se	s	k7c7	s
dnem	den	k1gInSc7	den
účinnost	účinnost	k1gFnSc1	účinnost
Ústavy	ústava	k1gFnPc4	ústava
stala	stát	k5eAaPmAgFnS	stát
Česká	český	k2eAgFnSc1d1	Česká
národní	národní	k2eAgFnSc1d1	národní
rada	rada	k1gFnSc1	rada
Poslaneckou	poslanecký	k2eAgFnSc4d1	Poslanecká
sněmovnou	sněmovna	k1gFnSc7	sněmovna
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
volební	volební	k2eAgNnSc1d1	volební
období	období	k1gNnSc1	období
skončilo	skončit	k5eAaPmAgNnS	skončit
6	[number]	k4	6
<g/>
.	.	kIx.	.
červnem	červen	k1gInSc7	červen
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
doby	doba	k1gFnSc2	doba
zvolení	zvolení	k1gNnSc4	zvolení
Senátu	senát	k1gInSc2	senát
měl	mít	k5eAaImAgMnS	mít
jeho	jeho	k3xOp3gFnSc2	jeho
funkce	funkce	k1gFnSc2	funkce
vykonávat	vykonávat	k5eAaImF	vykonávat
Prozatímní	prozatímní	k2eAgInSc4d1	prozatímní
Senát	senát	k1gInSc4	senát
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
způsob	způsob	k1gInSc1	způsob
jeho	jeho	k3xOp3gNnSc2	jeho
ustanovení	ustanovení	k1gNnSc2	ustanovení
měl	mít	k5eAaImAgInS	mít
řešit	řešit	k5eAaImF	řešit
samostatný	samostatný	k2eAgInSc1d1	samostatný
ústavní	ústavní	k2eAgInSc1d1	ústavní
zákon	zákon	k1gInSc1	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
nabytí	nabytí	k1gNnSc2	nabytí
účinnosti	účinnost	k1gFnSc2	účinnost
takového	takový	k3xDgInSc2	takový
ústavního	ústavní	k2eAgInSc2d1	ústavní
zákona	zákon	k1gInSc2	zákon
měla	mít	k5eAaImAgFnS	mít
vykonávat	vykonávat	k5eAaImF	vykonávat
funkce	funkce	k1gFnSc1	funkce
Senátu	senát	k1gInSc2	senát
Poslanecká	poslanecký	k2eAgFnSc1d1	Poslanecká
sněmovna	sněmovna	k1gFnSc1	sněmovna
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
nemohla	moct	k5eNaImAgNnP	moct
být	být	k5eAaImF	být
rozpuštěna	rozpuštěn	k2eAgNnPc1d1	rozpuštěno
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
článek	článek	k1gInSc1	článek
106	[number]	k4	106
v	v	k7c6	v
odstavci	odstavec	k1gInSc6	odstavec
4	[number]	k4	4
stanovoval	stanovovat	k5eAaImAgInS	stanovovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
přijetí	přijetí	k1gNnSc2	přijetí
zákonů	zákon	k1gInPc2	zákon
o	o	k7c6	o
jednacím	jednací	k2eAgInSc6d1	jednací
řádu	řád	k1gInSc6	řád
komor	komora	k1gFnPc2	komora
se	se	k3xPyFc4	se
mělo	mít	k5eAaImAgNnS	mít
postupovat	postupovat	k5eAaImF	postupovat
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
komorách	komora	k1gFnPc6	komora
podle	podle	k7c2	podle
jednacího	jednací	k2eAgInSc2d1	jednací
řádu	řád	k1gInSc2	řád
České	český	k2eAgFnSc2d1	Česká
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
bylo	být	k5eAaImAgNnS	být
připraveno	připravit	k5eAaPmNgNnS	připravit
několik	několik	k4yIc1	několik
návrhů	návrh	k1gInPc2	návrh
k	k	k7c3	k
otázce	otázka	k1gFnSc3	otázka
Prozatímního	prozatímní	k2eAgInSc2d1	prozatímní
Senátu	senát	k1gInSc2	senát
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc1	ten
nikdy	nikdy	k6eAd1	nikdy
nevznikl	vzniknout	k5eNaPmAgInS	vzniknout
a	a	k8xC	a
až	až	k6eAd1	až
do	do	k7c2	do
ustavení	ustavení	k1gNnSc2	ustavení
"	"	kIx"	"
<g/>
řádného	řádný	k2eAgInSc2d1	řádný
<g/>
"	"	kIx"	"
Senátu	senát	k1gInSc2	senát
vykonávala	vykonávat	k5eAaImAgFnS	vykonávat
Ústavou	ústava	k1gFnSc7	ústava
vymezené	vymezený	k2eAgFnSc2d1	vymezená
funkce	funkce	k1gFnSc2	funkce
horní	horní	k2eAgFnSc2d1	horní
komory	komora	k1gFnSc2	komora
Poslanecká	poslanecký	k2eAgFnSc1d1	Poslanecká
sněmovna	sněmovna	k1gFnSc1	sněmovna
<g/>
.	.	kIx.	.
</s>
<s>
Existovaly	existovat	k5eAaImAgInP	existovat
však	však	k9	však
i	i	k8xC	i
návrhy	návrh	k1gInPc1	návrh
obsadit	obsadit	k5eAaPmF	obsadit
horní	horní	k2eAgFnSc4d1	horní
komoru	komora	k1gFnSc4	komora
českého	český	k2eAgInSc2d1	český
Parlamentu	parlament	k1gInSc2	parlament
českými	český	k2eAgFnPc7d1	Česká
poslanci	poslanec	k1gMnPc1	poslanec
Federálního	federální	k2eAgNnSc2d1	federální
shromáždění	shromáždění	k1gNnSc2	shromáždění
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
volby	volba	k1gFnPc1	volba
do	do	k7c2	do
Senátu	senát	k1gInSc2	senát
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgFnP	konat
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
Článek	článek	k1gInSc1	článek
107	[number]	k4	107
upravuje	upravovat	k5eAaImIp3nS	upravovat
otázku	otázka	k1gFnSc4	otázka
prvních	první	k4xOgFnPc2	první
voleb	volba	k1gFnPc2	volba
do	do	k7c2	do
Senátu	senát	k1gInSc2	senát
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
obměnu	obměna	k1gFnSc4	obměna
jeho	jeho	k3xOp3gFnSc2	jeho
části	část	k1gFnSc2	část
každé	každý	k3xTgInPc4	každý
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
bylo	být	k5eAaImAgNnS	být
nutné	nutný	k2eAgNnSc1d1	nutné
vytvořit	vytvořit	k5eAaPmF	vytvořit
odkaz	odkaz	k1gInSc4	odkaz
na	na	k7c4	na
zákon	zákon	k1gInSc4	zákon
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
stanoví	stanovit	k5eAaPmIp3nS	stanovit
<g/>
,	,	kIx,	,
jakým	jaký	k3yIgInSc7	jaký
způsobem	způsob	k1gInSc7	způsob
se	se	k3xPyFc4	se
určí	určit	k5eAaPmIp3nS	určit
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
senátoři	senátor	k1gMnPc1	senátor
budou	být	k5eAaImBp3nP	být
po	po	k7c6	po
prvních	první	k4xOgFnPc6	první
volbách	volba	k1gFnPc6	volba
vykonávat	vykonávat	k5eAaImF	vykonávat
mandát	mandát	k1gInSc4	mandát
dvouleté	dvouletý	k2eAgFnPc1d1	dvouletá
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
čtyřleté	čtyřletý	k2eAgMnPc4d1	čtyřletý
a	a	k8xC	a
kteří	který	k3yIgMnPc1	který
šestileté	šestiletý	k2eAgNnSc4d1	šestileté
volební	volební	k2eAgNnSc4d1	volební
období	období	k1gNnSc4	období
<g/>
.	.	kIx.	.
</s>
<s>
Zasedání	zasedání	k1gNnSc1	zasedání
Senátu	senát	k1gInSc2	senát
svolal	svolat	k5eAaPmAgMnS	svolat
prezident	prezident	k1gMnSc1	prezident
na	na	k7c4	na
9	[number]	k4	9
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
ústavní	ústavní	k2eAgFnSc3d1	ústavní
konstrukci	konstrukce	k1gFnSc3	konstrukce
Senátu	senát	k1gInSc2	senát
jako	jako	k8xC	jako
nerozpustitelného	rozpustitelný	k2eNgInSc2d1	nerozpustitelný
a	a	k8xC	a
kontinuálního	kontinuální	k2eAgInSc2d1	kontinuální
orgánu	orgán	k1gInSc2	orgán
a	a	k8xC	a
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
zasedání	zasedání	k1gNnSc4	zasedání
obou	dva	k4xCgFnPc2	dva
komor	komora	k1gFnPc2	komora
stálá	stálý	k2eAgFnSc1d1	stálá
<g/>
,	,	kIx,	,
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
zároveň	zároveň	k6eAd1	zároveň
o	o	k7c6	o
zahájení	zahájení	k1gNnSc6	zahájení
jediné	jediný	k2eAgFnSc2d1	jediná
a	a	k8xC	a
poslední	poslední	k2eAgFnSc2d1	poslední
<g/>
.	.	kIx.	.
</s>
<s>
Zachování	zachování	k1gNnSc1	zachování
kontinuity	kontinuita	k1gFnSc2	kontinuita
vlády	vláda	k1gFnSc2	vláda
vzešlé	vzešlý	k2eAgFnSc2d1	vzešlá
z	z	k7c2	z
voleb	volba	k1gFnPc2	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
upravuje	upravovat	k5eAaImIp3nS	upravovat
článek	článek	k1gInSc4	článek
108	[number]	k4	108
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dikce	dikce	k1gFnSc2	dikce
článku	článek	k1gInSc2	článek
109	[number]	k4	109
vykonávala	vykonávat	k5eAaImAgFnS	vykonávat
funkci	funkce	k1gFnSc4	funkce
státního	státní	k2eAgNnSc2d1	státní
zastupitelství	zastupitelství	k1gNnSc2	zastupitelství
do	do	k7c2	do
jeho	jeho	k3xOp3gNnPc2	jeho
zřízení	zřízení	k1gNnPc2	zřízení
prokuratura	prokuratura	k1gFnSc1	prokuratura
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Prokuratura	prokuratura	k1gFnSc1	prokuratura
tuto	tento	k3xDgFnSc4	tento
funkci	funkce	k1gFnSc4	funkce
plnila	plnit	k5eAaImAgFnS	plnit
do	do	k7c2	do
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1993	[number]	k4	1993
tvořily	tvořit	k5eAaImAgFnP	tvořit
dle	dle	k7c2	dle
článku	článek	k1gInSc2	článek
110	[number]	k4	110
Ústavy	ústava	k1gFnSc2	ústava
soustavu	soustava	k1gFnSc4	soustava
soudů	soud	k1gInPc2	soud
také	také	k9	také
vojenské	vojenský	k2eAgInPc4d1	vojenský
soudy	soud	k1gInPc4	soud
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byla	být	k5eAaImAgFnS	být
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
ustanovení	ustanovení	k1gNnSc4	ustanovení
čl	čl	kA	čl
<g/>
.	.	kIx.	.
3	[number]	k4	3
odst	odsta	k1gFnPc2	odsta
<g/>
.	.	kIx.	.
5	[number]	k4	5
ústavního	ústavní	k2eAgInSc2d1	ústavní
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
4	[number]	k4	4
<g/>
/	/	kIx~	/
<g/>
1993	[number]	k4	1993
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
opatřeních	opatření	k1gNnPc6	opatření
souvisejících	související	k2eAgFnPc2d1	související
se	s	k7c7	s
zánikem	zánik	k1gInSc7	zánik
České	český	k2eAgFnSc2d1	Česká
a	a	k8xC	a
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
Federativní	federativní	k2eAgFnSc2d1	federativní
Republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
duplicita	duplicita	k1gFnSc1	duplicita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
žádný	žádný	k3yNgInSc1	žádný
právní	právní	k2eAgInSc1d1	právní
předpis	předpis	k1gInSc1	předpis
nepočítá	počítat	k5eNaImIp3nS	počítat
se	s	k7c7	s
zřízením	zřízení	k1gNnSc7	zřízení
vojenských	vojenský	k2eAgInPc2d1	vojenský
či	či	k8xC	či
polních	polní	k2eAgInPc2d1	polní
soudů	soud	k1gInPc2	soud
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ani	ani	k8xC	ani
v	v	k7c6	v
případě	případ	k1gInSc6	případ
stavu	stav	k1gInSc2	stav
ohrožení	ohrožení	k1gNnSc2	ohrožení
státu	stát	k1gInSc2	stát
či	či	k8xC	či
nouzového	nouzový	k2eAgInSc2d1	nouzový
stavu	stav	k1gInSc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Zachování	zachování	k1gNnSc1	zachování
kontinuity	kontinuita	k1gFnSc2	kontinuita
soudců	soudce	k1gMnPc2	soudce
i	i	k8xC	i
po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
samostatné	samostatný	k2eAgFnSc2d1	samostatná
republiky	republika	k1gFnSc2	republika
zakotvoval	zakotvovat	k5eAaImAgInS	zakotvovat
článek	článek	k1gInSc1	článek
111	[number]	k4	111
Ústavy	ústava	k1gFnSc2	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Článek	článek	k1gInSc1	článek
112	[number]	k4	112
vymezuje	vymezovat	k5eAaImIp3nS	vymezovat
množinu	množina	k1gFnSc4	množina
právních	právní	k2eAgInPc2d1	právní
předpisů	předpis	k1gInPc2	předpis
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
něž	jenž	k3xRgMnPc4	jenž
souhrnně	souhrnně	k6eAd1	souhrnně
zavádí	zavádět	k5eAaImIp3nS	zavádět
označení	označení	k1gNnSc4	označení
ústavní	ústavní	k2eAgInSc1d1	ústavní
pořádek	pořádek	k1gInSc1	pořádek
jako	jako	k8xS	jako
nový	nový	k2eAgInSc1d1	nový
pojem	pojem	k1gInSc1	pojem
ústavního	ústavní	k2eAgNnSc2d1	ústavní
práva	právo	k1gNnSc2	právo
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
zrušuje	zrušovat	k5eAaImIp3nS	zrušovat
část	část	k1gFnSc4	část
dosavadních	dosavadní	k2eAgInPc2d1	dosavadní
ústavních	ústavní	k2eAgInPc2d1	ústavní
zákonů	zákon	k1gInPc2	zákon
federace	federace	k1gFnSc2	federace
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
ústava	ústava	k1gFnSc1	ústava
polylegální	polylegální	k2eAgNnSc1d1	polylegální
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
odlišovat	odlišovat	k5eAaImF	odlišovat
ústavu	ústava	k1gFnSc4	ústava
(	(	kIx(	(
<g/>
s	s	k7c7	s
malým	malý	k2eAgNnSc7d1	malé
počátečním	počáteční	k2eAgNnSc7d1	počáteční
písmenem	písmeno	k1gNnSc7	písmeno
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ústavu	ústav	k1gInSc2	ústav
(	(	kIx(	(
<g/>
s	s	k7c7	s
velkým	velký	k2eAgNnSc7d1	velké
počátečním	počáteční	k2eAgNnSc7d1	počáteční
písmenem	písmeno	k1gNnSc7	písmeno
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
součásti	součást	k1gFnPc4	součást
ústavního	ústavní	k2eAgInSc2d1	ústavní
pořádku	pořádek	k1gInSc2	pořádek
<g/>
,	,	kIx,	,
ústavní	ústavní	k2eAgInSc4d1	ústavní
zákon	zákon	k1gInSc4	zákon
a	a	k8xC	a
Listinu	listina	k1gFnSc4	listina
základních	základní	k2eAgNnPc2d1	základní
práv	právo	k1gNnPc2	právo
a	a	k8xC	a
svobod	svoboda	k1gFnPc2	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
užit	užit	k2eAgInSc1d1	užit
termín	termín	k1gInSc1	termín
ústava	ústava	k1gFnSc1	ústava
(	(	kIx(	(
<g/>
s	s	k7c7	s
malým	malý	k2eAgNnSc7d1	malé
počátečním	počáteční	k2eAgNnSc7d1	počáteční
písmenem	písmeno	k1gNnSc7	písmeno
<g/>
)	)	kIx)	)
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
o	o	k7c4	o
ústavní	ústavní	k2eAgInSc4d1	ústavní
pořádek	pořádek	k1gInSc4	pořádek
<g/>
,	,	kIx,	,
výjimečně	výjimečně	k6eAd1	výjimečně
–	–	k?	–
v	v	k7c6	v
případě	případ	k1gInSc6	případ
§	§	k?	§
366	[number]	k4	366
odst	odsta	k1gFnPc2	odsta
<g/>
.	.	kIx.	.
2	[number]	k4	2
trestního	trestní	k2eAgInSc2d1	trestní
řádu	řád	k1gInSc2	řád
–	–	k?	–
o	o	k7c6	o
Ústavu	ústav	k1gInSc6	ústav
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Hovoří	hovořit	k5eAaImIp3nS	hovořit
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
o	o	k7c6	o
Ústavě	ústav	k1gInSc6	ústav
(	(	kIx(	(
<g/>
s	s	k7c7	s
velkým	velký	k2eAgNnSc7d1	velké
počátečním	počáteční	k2eAgNnSc7d1	počáteční
písmenem	písmeno	k1gNnSc7	písmeno
<g/>
)	)	kIx)	)
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
ústavní	ústavní	k2eAgInSc4d1	ústavní
zákon	zákon	k1gInSc4	zákon
ČNR	ČNR	kA	ČNR
č.	č.	k?	č.
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
1993	[number]	k4	1993
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
V	v	k7c6	v
případě	případ	k1gInSc6	případ
exekučního	exekuční	k2eAgInSc2d1	exekuční
řádu	řád	k1gInSc2	řád
však	však	k9	však
"	"	kIx"	"
<g/>
Ústava	ústava	k1gFnSc1	ústava
ČR	ČR	kA	ČR
<g/>
"	"	kIx"	"
má	mít	k5eAaImIp3nS	mít
zahrnovat	zahrnovat	k5eAaImF	zahrnovat
celý	celý	k2eAgInSc4d1	celý
ústavní	ústavní	k2eAgInSc4d1	ústavní
pořádek	pořádek	k1gInSc4	pořádek
<g/>
,	,	kIx,	,
především	především	k9	především
pak	pak	k6eAd1	pak
Listinu	listina	k1gFnSc4	listina
základních	základní	k2eAgNnPc2d1	základní
práv	právo	k1gNnPc2	právo
a	a	k8xC	a
svobod	svoboda	k1gFnPc2	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Ústavní	ústavní	k2eAgInSc1d1	ústavní
pořádek	pořádek	k1gInSc1	pořádek
je	být	k5eAaImIp3nS	být
nový	nový	k2eAgInSc1d1	nový
pojem	pojem	k1gInSc1	pojem
ústavního	ústavní	k2eAgNnSc2d1	ústavní
práva	právo	k1gNnSc2	právo
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
byl	být	k5eAaImAgMnS	být
pokusem	pokus	k1gInSc7	pokus
komplexně	komplexně	k6eAd1	komplexně
nahradit	nahradit	k5eAaPmF	nahradit
pojem	pojem	k1gInSc4	pojem
"	"	kIx"	"
<g/>
ústava	ústava	k1gFnSc1	ústava
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
s	s	k7c7	s
malým	malý	k2eAgNnSc7d1	malé
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ústavním	ústavní	k2eAgInSc7d1	ústavní
pořádkem	pořádek	k1gInSc7	pořádek
byl	být	k5eAaImAgInS	být
zaveden	zavést	k5eAaPmNgInS	zavést
pojem	pojem	k1gInSc4	pojem
součást	součást	k1gFnSc4	součást
ústavního	ústavní	k2eAgInSc2d1	ústavní
pořádku	pořádek	k1gInSc2	pořádek
vymezený	vymezený	k2eAgInSc4d1	vymezený
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
3	[number]	k4	3
a	a	k8xC	a
112	[number]	k4	112
odst	odsta	k1gFnPc2	odsta
<g/>
.	.	kIx.	.
1	[number]	k4	1
Ústavy	ústava	k1gFnSc2	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
součást	součást	k1gFnSc4	součást
ústavního	ústavní	k2eAgInSc2d1	ústavní
pořádku	pořádek	k1gInSc2	pořádek
označil	označit	k5eAaPmAgInS	označit
Ústavní	ústavní	k2eAgInSc1d1	ústavní
soud	soud	k1gInSc1	soud
také	také	k6eAd1	také
lidsko-právní	lidskorávní	k2eAgFnSc2d1	lidsko-právní
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
předmětem	předmět	k1gInSc7	předmět
kritiky	kritika	k1gFnSc2	kritika
některých	některý	k3yIgMnPc2	některý
teoretiků	teoretik	k1gMnPc2	teoretik
ústavního	ústavní	k2eAgNnSc2d1	ústavní
práva	právo	k1gNnSc2	právo
–	–	k?	–
Filipa	Filip	k1gMnSc2	Filip
<g/>
,	,	kIx,	,
Kysely	Kysela	k1gMnSc2	Kysela
<g/>
,	,	kIx,	,
Kühna	Kühn	k1gMnSc2	Kühn
<g/>
.	.	kIx.	.
</s>
<s>
Článek	článek	k1gInSc1	článek
112	[number]	k4	112
v	v	k7c6	v
odstavci	odstavec	k1gInSc6	odstavec
druhém	druhý	k4xOgNnSc6	druhý
zrušil	zrušit	k5eAaPmAgInS	zrušit
část	část	k1gFnSc4	část
ústavních	ústavní	k2eAgInPc2d1	ústavní
zákonů	zákon	k1gInPc2	zákon
federace	federace	k1gFnSc2	federace
<g/>
,	,	kIx,	,
v	v	k7c6	v
odstavci	odstavec	k1gInSc6	odstavec
třetím	třetí	k4xOgMnSc6	třetí
pak	pak	k6eAd1	pak
článek	článek	k1gInSc1	článek
dekonstitucionalizuje	dekonstitucionalizovat	k5eAaImIp3nS	dekonstitucionalizovat
(	(	kIx(	(
<g/>
zbavuje	zbavovat	k5eAaImIp3nS	zbavovat
síly	síla	k1gFnSc2	síla
ústavního	ústavní	k2eAgInSc2d1	ústavní
zákona	zákon	k1gInSc2	zákon
<g/>
)	)	kIx)	)
ostatní	ostatní	k2eAgInPc1d1	ostatní
ústavní	ústavní	k2eAgInPc1d1	ústavní
zákony	zákon	k1gInPc1	zákon
platné	platný	k2eAgInPc1d1	platný
na	na	k7c6	na
území	území	k1gNnSc6	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
ty	ten	k3xDgInPc4	ten
ústavní	ústavní	k2eAgInPc4d1	ústavní
zákony	zákon	k1gInPc4	zákon
či	či	k8xC	či
jejich	jejich	k3xOp3gFnPc4	jejich
části	část	k1gFnPc4	část
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
neměnily	měnit	k5eNaImAgFnP	měnit
či	či	k8xC	či
nedoplňovaly	doplňovat	k5eNaImAgFnP	doplňovat
Ústavu	ústav	k1gInSc2	ústav
Československé	československý	k2eAgFnSc2d1	Československá
socialistické	socialistický	k2eAgFnSc2d1	socialistická
republiky	republika	k1gFnSc2	republika
či	či	k8xC	či
ústavní	ústavní	k2eAgInSc1d1	ústavní
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
československé	československý	k2eAgFnSc6d1	Československá
federaci	federace	k1gFnSc6	federace
<g/>
.	.	kIx.	.
</s>
<s>
Ústava	ústava	k1gFnSc1	ústava
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
nabyla	nabýt	k5eAaPmAgFnS	nabýt
z	z	k7c2	z
dikce	dikce	k1gFnSc2	dikce
článku	článek	k1gInSc2	článek
113	[number]	k4	113
účinnost	účinnost	k1gFnSc1	účinnost
dnem	dnem	k7c2	dnem
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
