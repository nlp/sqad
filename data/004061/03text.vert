<s>
Pomeranč	pomeranč	k1gInSc1	pomeranč
je	být	k5eAaImIp3nS	být
plod	plod	k1gInSc4	plod
pomerančovníku	pomerančovník	k1gInSc2	pomerančovník
čínského	čínský	k2eAgMnSc2d1	čínský
(	(	kIx(	(
<g/>
Citrus	citrus	k1gInSc1	citrus
sinensis	sinensis	k1gFnSc2	sinensis
<g/>
)	)	kIx)	)
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
routovité	routovitý	k2eAgFnSc2d1	routovitý
(	(	kIx(	(
<g/>
Rutaceae	Rutacea	k1gFnSc2	Rutacea
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pomerančovníky	pomerančovník	k1gInPc1	pomerančovník
jsou	být	k5eAaImIp3nP	být
stromy	strom	k1gInPc1	strom
<g/>
,	,	kIx,	,
zřídka	zřídka	k6eAd1	zřídka
i	i	k9	i
keře	keř	k1gInPc1	keř
<g/>
.	.	kIx.	.
</s>
<s>
Pochází	pocházet	k5eAaImIp3nS	pocházet
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
ze	z	k7c2	z
subtropických	subtropický	k2eAgFnPc2d1	subtropická
hraničních	hraniční	k2eAgFnPc2d1	hraniční
oblastí	oblast	k1gFnPc2	oblast
Číny	Čína	k1gFnSc2	Čína
a	a	k8xC	a
Vietnamu	Vietnam	k1gInSc2	Vietnam
<g/>
,	,	kIx,	,
nikde	nikde	k6eAd1	nikde
neroste	růst	k5eNaImIp3nS	růst
jako	jako	k9	jako
planá	planý	k2eAgFnSc1d1	planá
rostlina	rostlina	k1gFnSc1	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
dovezen	dovézt	k5eAaPmNgInS	dovézt
portugalskými	portugalský	k2eAgMnPc7d1	portugalský
obchodníky	obchodník	k1gMnPc7	obchodník
a	a	k8xC	a
záhy	záhy	k6eAd1	záhy
po	po	k7c6	po
objevení	objevení	k1gNnSc6	objevení
Ameriky	Amerika	k1gFnSc2	Amerika
i	i	k9	i
tam	tam	k6eAd1	tam
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
je	být	k5eAaImIp3nS	být
rozšířen	rozšířen	k2eAgMnSc1d1	rozšířen
do	do	k7c2	do
všech	všecek	k3xTgFnPc2	všecek
subtropických	subtropický	k2eAgFnPc2d1	subtropická
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
v	v	k7c6	v
tropech	trop	k1gInPc6	trop
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
pro	pro	k7c4	pro
přílišnou	přílišný	k2eAgFnSc4d1	přílišná
vlhkost	vlhkost	k1gFnSc4	vlhkost
nedaří	dařit	k5eNaImIp3nS	dařit
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
nejvíce	hodně	k6eAd3	hodně
pěstované	pěstovaný	k2eAgNnSc4d1	pěstované
citrusové	citrusový	k2eAgNnSc4d1	citrusové
ovoce	ovoce	k1gNnSc4	ovoce
<g/>
.	.	kIx.	.
</s>
<s>
Pomerančovník	pomerančovník	k1gInSc1	pomerančovník
je	být	k5eAaImIp3nS	být
stálezelený	stálezelený	k2eAgInSc1d1	stálezelený
mělce	mělce	k6eAd1	mělce
kořenící	kořenící	k2eAgInSc1d1	kořenící
strom	strom	k1gInSc1	strom
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
kulovitou	kulovitý	k2eAgFnSc4d1	kulovitá
korunu	koruna	k1gFnSc4	koruna
a	a	k8xC	a
v	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
trnité	trnitý	k2eAgFnSc2d1	trnitá
větvičky	větvička	k1gFnSc2	větvička
<g/>
.	.	kIx.	.
</s>
<s>
Listy	list	k1gInPc1	list
jsou	být	k5eAaImIp3nP	být
podlouhle	podlouhle	k6eAd1	podlouhle
vejčité	vejčitý	k2eAgFnPc1d1	vejčitá
až	až	k8xS	až
eliptické	eliptický	k2eAgFnPc1d1	eliptická
<g/>
,	,	kIx,	,
středně	středně	k6eAd1	středně
veliké	veliký	k2eAgInPc1d1	veliký
<g/>
,	,	kIx,	,
tmavě	tmavě	k6eAd1	tmavě
zelené	zelený	k2eAgInPc1d1	zelený
s	s	k7c7	s
úzkými	úzký	k2eAgInPc7d1	úzký
řapíky	řapík	k1gInPc7	řapík
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
rostliny	rostlina	k1gFnPc1	rostlina
jednodomé	jednodomý	k2eAgFnPc1d1	jednodomá
a	a	k8xC	a
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
oboupohlavé	oboupohlavý	k2eAgInPc1d1	oboupohlavý
květy	květ	k1gInPc1	květ
<g/>
,	,	kIx,	,
opylovány	opylován	k2eAgInPc1d1	opylován
jsou	být	k5eAaImIp3nP	být
hmyzem	hmyz	k1gInSc7	hmyz
<g/>
.	.	kIx.	.
</s>
<s>
Květy	květ	k1gInPc1	květ
jsou	být	k5eAaImIp3nP	být
bílé	bílý	k2eAgInPc1d1	bílý
<g/>
,	,	kIx,	,
vonící	vonící	k2eAgInPc1d1	vonící
<g/>
,	,	kIx,	,
2	[number]	k4	2
až	až	k9	až
3	[number]	k4	3
cm	cm	kA	cm
veliké	veliký	k2eAgFnPc1d1	veliká
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
5	[number]	k4	5
<g/>
četné	četný	k2eAgInPc1d1	četný
<g/>
,	,	kIx,	,
petaly	petala	k1gFnPc1	petala
podlouhlé	podlouhlý	k2eAgFnPc1d1	podlouhlá
<g/>
,	,	kIx,	,
asi	asi	k9	asi
15	[number]	k4	15
x	x	k?	x
6	[number]	k4	6
mm	mm	kA	mm
velké	velká	k1gFnSc2	velká
<g/>
,	,	kIx,	,
tyčinky	tyčinka	k1gFnSc2	tyčinka
asi	asi	k9	asi
14	[number]	k4	14
mm	mm	kA	mm
<g/>
,	,	kIx,	,
semeníky	semeník	k1gInPc1	semeník
téměř	téměř	k6eAd1	téměř
kulovité	kulovitý	k2eAgInPc1d1	kulovitý
<g/>
.	.	kIx.	.
</s>
<s>
Plody	plod	k1gInPc1	plod
jsou	být	k5eAaImIp3nP	být
bobule	bobule	k1gFnPc4	bobule
(	(	kIx(	(
<g/>
hesperidium	hesperidium	k1gNnSc1	hesperidium
<g/>
)	)	kIx)	)
ve	v	k7c6	v
velikostech	velikost	k1gFnPc6	velikost
5	[number]	k4	5
až	až	k8xS	až
12	[number]	k4	12
cm	cm	kA	cm
<g/>
,	,	kIx,	,
kulovité	kulovitý	k2eAgFnPc1d1	kulovitá
až	až	k6eAd1	až
oválné	oválný	k2eAgFnPc1d1	oválná
<g/>
.	.	kIx.	.
</s>
<s>
Kůra	kůra	k1gFnSc1	kůra
plodu	plod	k1gInSc2	plod
(	(	kIx(	(
<g/>
perikarp	perikarp	k1gInSc1	perikarp
<g/>
)	)	kIx)	)
bývá	bývat	k5eAaImIp3nS	bývat
barvy	barva	k1gFnPc4	barva
žluté	žlutý	k2eAgFnPc4d1	žlutá
<g/>
,	,	kIx,	,
oranžové	oranžový	k2eAgFnPc4d1	oranžová
nebo	nebo	k8xC	nebo
šarlatově	šarlatově	k6eAd1	šarlatově
červené	červený	k2eAgNnSc1d1	červené
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
tenká	tenký	k2eAgFnSc1d1	tenká
<g/>
,	,	kIx,	,
přiléhající	přiléhající	k2eAgFnSc1d1	přiléhající
k	k	k7c3	k
dužnině	dužnina	k1gFnSc3	dužnina
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
obtížně	obtížně	k6eAd1	obtížně
loupatelná	loupatelný	k2eAgFnSc1d1	loupatelná
<g/>
.	.	kIx.	.
</s>
<s>
Dužina	dužina	k1gFnSc1	dužina
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
klínovitými	klínovitý	k2eAgInPc7d1	klínovitý
semeníkovými	semeníkový	k2eAgInPc7d1	semeníkový
pouzdry	pouzdro	k1gNnPc7	pouzdro
vyplněnými	vyplněný	k2eAgInPc7d1	vyplněný
tenkostěnnými	tenkostěnný	k2eAgInPc7d1	tenkostěnný
váčky	váček	k1gInPc7	váček
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
sladkou	sladký	k2eAgFnSc4d1	sladká
šťávu	šťáva	k1gFnSc4	šťáva
obklopující	obklopující	k2eAgNnSc1d1	obklopující
semena	semeno	k1gNnPc1	semeno
umístěná	umístěný	k2eAgNnPc1d1	umístěné
na	na	k7c6	na
středoúhlé	středoúhlý	k2eAgFnSc6d1	středoúhlý
semenici	semenice	k1gFnSc6	semenice
<g/>
.	.	kIx.	.
</s>
<s>
Dužina	dužina	k1gFnSc1	dužina
je	být	k5eAaImIp3nS	být
barvy	barva	k1gFnPc4	barva
žluté	žlutý	k2eAgFnPc4d1	žlutá
až	až	k6eAd1	až
oranžové	oranžový	k2eAgFnPc1d1	oranžová
<g/>
,	,	kIx,	,
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
kultivarů	kultivar	k1gInPc2	kultivar
i	i	k9	i
fialově	fialově	k6eAd1	fialově
červené	červený	k2eAgNnSc1d1	červené
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
šťavnatá	šťavnatý	k2eAgFnSc1d1	šťavnatá
<g/>
,	,	kIx,	,
osvěžující	osvěžující	k2eAgFnSc1d1	osvěžující
sladkokyselé	sladkokyselý	k2eAgFnPc4d1	sladkokyselá
chuti	chuť	k1gFnPc4	chuť
<g/>
.	.	kIx.	.
</s>
<s>
Semena	semeno	k1gNnPc1	semeno
poměrně	poměrně	k6eAd1	poměrně
velká	velký	k2eAgNnPc1d1	velké
<g/>
,	,	kIx,	,
mnohozárodečná	mnohozárodečný	k2eAgFnSc1d1	mnohozárodečný
(	(	kIx(	(
<g/>
polyembryonická	polyembryonický	k2eAgFnSc1d1	polyembryonický
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
bezsemenné	bezsemenný	k2eAgInPc4d1	bezsemenný
kultivary	kultivar	k1gInPc4	kultivar
<g/>
.	.	kIx.	.
</s>
<s>
Plody	plod	k1gInPc1	plod
jsou	být	k5eAaImIp3nP	být
kvalitním	kvalitní	k2eAgInSc7d1	kvalitní
zdrojem	zdroj	k1gInSc7	zdroj
vitamínu	vitamín	k1gInSc2	vitamín
C	C	kA	C
a	a	k8xC	a
konzumují	konzumovat	k5eAaBmIp3nP	konzumovat
se	se	k3xPyFc4	se
čerstvé	čerstvý	k2eAgInPc1d1	čerstvý
<g/>
,	,	kIx,	,
kompotují	kompotovat	k5eAaBmIp3nP	kompotovat
se	se	k3xPyFc4	se
nebo	nebo	k8xC	nebo
lisují	lisovat	k5eAaImIp3nP	lisovat
na	na	k7c4	na
osvěžující	osvěžující	k2eAgFnSc4d1	osvěžující
šťávu	šťáva	k1gFnSc4	šťáva
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
90	[number]	k4	90
<g/>
%	%	kIx~	%
produkce	produkce	k1gFnSc1	produkce
pomerančů	pomeranč	k1gInPc2	pomeranč
je	být	k5eAaImIp3nS	být
průmyslově	průmyslově	k6eAd1	průmyslově
zpracováno	zpracován	k2eAgNnSc1d1	zpracováno
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
slupek	slupka	k1gFnPc2	slupka
se	se	k3xPyFc4	se
získává	získávat	k5eAaImIp3nS	získávat
pektin	pektin	k1gInSc1	pektin
a	a	k8xC	a
hlavně	hlavně	k9	hlavně
esenciální	esenciální	k2eAgInPc1d1	esenciální
oleje	olej	k1gInPc1	olej
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
slouží	sloužit	k5eAaImIp3nP	sloužit
k	k	k7c3	k
aromatizaci	aromatizace	k1gFnSc3	aromatizace
potravin	potravina	k1gFnPc2	potravina
a	a	k8xC	a
v	v	k7c6	v
kosmetice	kosmetika	k1gFnSc6	kosmetika
<g/>
,	,	kIx,	,
v	v	k7c6	v
menší	malý	k2eAgFnSc6d2	menší
míře	míra	k1gFnSc6	míra
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
v	v	k7c6	v
lékařství	lékařství	k1gNnSc6	lékařství
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
tinktur	tinktura	k1gFnPc2	tinktura
<g/>
.	.	kIx.	.
</s>
<s>
Květu	Květa	k1gFnSc4	Květa
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gFnSc4	jeho
vůní	vůně	k1gFnPc2	vůně
a	a	k8xC	a
chuť	chuť	k1gFnSc4	chuť
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
upravující	upravující	k2eAgFnSc1d1	upravující
složka	složka	k1gFnSc1	složka
řady	řada	k1gFnSc2	řada
čajových	čajový	k2eAgFnPc2d1	čajová
směsí	směs	k1gFnPc2	směs
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lidovém	lidový	k2eAgNnSc6d1	lidové
lékařství	lékařství	k1gNnSc6	lékařství
se	se	k3xPyFc4	se
užívají	užívat	k5eAaImIp3nP	užívat
okvětní	okvětní	k2eAgInPc1d1	okvětní
plátky	plátek	k1gInPc1	plátek
svařené	svařený	k2eAgInPc1d1	svařený
se	s	k7c7	s
solí	sůl	k1gFnSc7	sůl
k	k	k7c3	k
uklidnění	uklidnění	k1gNnSc3	uklidnění
nervů	nerv	k1gInPc2	nerv
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
astmatu	astma	k1gNnSc3	astma
<g/>
,	,	kIx,	,
vysokému	vysoký	k2eAgInSc3d1	vysoký
tlaku	tlak	k1gInSc3	tlak
<g/>
,	,	kIx,	,
horečce	horečka	k1gFnSc3	horečka
a	a	k8xC	a
zvracení	zvracení	k1gNnSc1	zvracení
<g/>
,	,	kIx,	,
svařené	svařený	k2eAgNnSc1d1	svařené
s	s	k7c7	s
cukrem	cukr	k1gInSc7	cukr
proti	proti	k7c3	proti
nechutenství	nechutenství	k1gNnSc3	nechutenství
<g/>
.	.	kIx.	.
</s>
<s>
Pomerančovníky	pomerančovník	k1gInPc1	pomerančovník
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
podle	podle	k7c2	podle
vlastnosti	vlastnost	k1gFnSc2	vlastnost
plodů	plod	k1gInPc2	plod
do	do	k7c2	do
tří	tři	k4xCgFnPc2	tři
základních	základní	k2eAgFnPc2d1	základní
skupin	skupina	k1gFnPc2	skupina
<g/>
:	:	kIx,	:
obyčejné	obyčejný	k2eAgInPc1d1	obyčejný
–	–	k?	–
pomeranče	pomeranč	k1gInPc1	pomeranč
s	s	k7c7	s
poměrně	poměrně	k6eAd1	poměrně
tenkou	tenký	k2eAgFnSc7d1	tenká
slupkou	slupka	k1gFnSc7	slupka
<g/>
,	,	kIx,	,
žlutou	žlutý	k2eAgFnSc7d1	žlutá
až	až	k8xS	až
oranžovou	oranžový	k2eAgFnSc7d1	oranžová
dužinou	dužina	k1gFnSc7	dužina
<g/>
,	,	kIx,	,
pěstované	pěstovaný	k2eAgInPc4d1	pěstovaný
převážně	převážně	k6eAd1	převážně
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
a	a	k8xC	a
na	na	k7c6	na
Blízkém	blízký	k2eAgInSc6d1	blízký
Východě	východ	k1gInSc6	východ
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
přímý	přímý	k2eAgInSc4d1	přímý
konzum	konzum	k1gInSc4	konzum
i	i	k9	i
pro	pro	k7c4	pro
lisování	lisování	k1gNnSc4	lisování
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc4	ten
např.	např.	kA	např.
Citrus	citrus	k1gInSc1	citrus
sinensis	sinensis	k1gInSc1	sinensis
<g />
.	.	kIx.	.
</s>
<s>
Osbeck	Osbeck	k1gInSc1	Osbeck
cv	cv	k?	cv
'	'	kIx"	'
<g/>
Jaffa	Jaffa	k1gFnSc1	Jaffa
<g/>
'	'	kIx"	'
Citrus	citrus	k1gInSc1	citrus
sinensis	sinensis	k1gFnSc2	sinensis
Osbeck	Osbeck	k1gMnSc1	Osbeck
cv	cv	k?	cv
'	'	kIx"	'
<g/>
Hamlin	Hamlin	k1gInSc1	Hamlin
<g/>
'	'	kIx"	'
Citrus	citrus	k1gInSc1	citrus
sinensis	sinensis	k1gFnSc2	sinensis
Osbeck	Osbeck	k1gMnSc1	Osbeck
cv	cv	k?	cv
'	'	kIx"	'
<g/>
Murcia	Murcia	k1gFnSc1	Murcia
<g/>
'	'	kIx"	'
Citrus	citrus	k1gInSc1	citrus
sinensis	sinensis	k1gFnSc2	sinensis
Osbeck	Osbeck	k1gMnSc1	Osbeck
cv	cv	k?	cv
'	'	kIx"	'
<g/>
Valencia	Valencia	k1gFnSc1	Valencia
<g/>
'	'	kIx"	'
krvavé	krvavý	k2eAgNnSc1d1	krvavé
–	–	k?	–
mají	mít	k5eAaImIp3nP	mít
červené	červený	k2eAgFnPc4d1	červená
žilky	žilka	k1gFnPc4	žilka
v	v	k7c6	v
dužině	dužina	k1gFnSc6	dužina
a	a	k8xC	a
šťávu	šťáva	k1gFnSc4	šťáva
fialově	fialově	k6eAd1	fialově
rudé	rudý	k2eAgFnSc2d1	rudá
barvy	barva	k1gFnSc2	barva
Citrus	citrus	k1gInSc1	citrus
sinensis	sinensis	k1gFnSc4	sinensis
Osbeck	Osbecko	k1gNnPc2	Osbecko
cv	cv	k?	cv
<g />
.	.	kIx.	.
</s>
<s>
'	'	kIx"	'
<g/>
Moro	mora	k1gFnSc5	mora
<g/>
'	'	kIx"	'
Citrus	citrus	k1gInSc1	citrus
sinensis	sinensis	k1gFnSc2	sinensis
Osbeck	Osbeck	k1gMnSc1	Osbeck
cv	cv	k?	cv
'	'	kIx"	'
<g/>
Ruby	rub	k1gInPc1	rub
<g/>
'	'	kIx"	'
Citrus	citrus	k1gInSc1	citrus
sinensis	sinensis	k1gFnSc2	sinensis
Osbeck	Osbeck	k1gMnSc1	Osbeck
cv	cv	k?	cv
'	'	kIx"	'
<g/>
Sanguinello	Sanguinello	k1gNnSc1	Sanguinello
<g/>
'	'	kIx"	'
Citrus	citrus	k1gInSc1	citrus
sinensis	sinensis	k1gFnSc2	sinensis
Osbeck	Osbeck	k1gMnSc1	Osbeck
cv	cv	k?	cv
'	'	kIx"	'
<g/>
Tarocco	Tarocco	k1gNnSc1	Tarocco
<g/>
'	'	kIx"	'
pupečné	pupečný	k2eAgNnSc1d1	pupečný
–	–	k?	–
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
pomeranče	pomeranč	k1gInPc1	pomeranč
se	s	k7c7	s
základem	základ	k1gInSc7	základ
druhého	druhý	k4xOgInSc2	druhý
plodu	plod	k1gInSc2	plod
na	na	k7c6	na
bliznové	bliznový	k2eAgFnSc6d1	bliznový
straně	strana	k1gFnSc6	strana
plodu	plod	k1gInSc2	plod
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vypadá	vypadat	k5eAaImIp3nS	vypadat
jako	jako	k9	jako
pupek	pupek	k1gInSc1	pupek
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
Navel	Navel	k1gInSc1	Navel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
objeveny	objevit	k5eAaPmNgInP	objevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1820	[number]	k4	1820
na	na	k7c6	na
jediném	jediný	k2eAgInSc6d1	jediný
zmutovaném	zmutovaný	k2eAgInSc6d1	zmutovaný
stromu	strom	k1gInSc6	strom
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
a	a	k8xC	a
protože	protože	k8xS	protože
tyto	tento	k3xDgInPc1	tento
pomeranče	pomeranč	k1gInPc1	pomeranč
nemají	mít	k5eNaImIp3nP	mít
semena	semeno	k1gNnPc4	semeno
<g/>
,	,	kIx,	,
rozmnožil	rozmnožit	k5eAaPmAgMnS	rozmnožit
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc4	tento
strom	strom	k1gInSc4	strom
dále	daleko	k6eAd2	daleko
vegetativně	vegetativně	k6eAd1	vegetativně
pomoci	pomoct	k5eAaPmF	pomoct
roubů	roub	k1gInPc2	roub
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
všechny	všechen	k3xTgInPc1	všechen
dnešní	dnešní	k2eAgInPc1d1	dnešní
stromy	strom	k1gInPc1	strom
s	s	k7c7	s
těmito	tento	k3xDgInPc7	tento
pomeranči	pomeranč	k1gInPc7	pomeranč
jsou	být	k5eAaImIp3nP	být
přímými	přímý	k2eAgMnPc7d1	přímý
potomky	potomek	k1gMnPc7	potomek
jediného	jediný	k2eAgInSc2d1	jediný
stromu	strom	k1gInSc2	strom
<g/>
.	.	kIx.	.
</s>
<s>
Různých	různý	k2eAgInPc2d1	různý
kultivarů	kultivar	k1gInPc2	kultivar
se	se	k3xPyFc4	se
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
roubováním	roubování	k1gNnSc7	roubování
na	na	k7c4	na
rozdílné	rozdílný	k2eAgInPc4d1	rozdílný
podnože	podnož	k1gInPc4	podnož
<g/>
.	.	kIx.	.
</s>
<s>
Citrus	citrus	k1gInSc1	citrus
sinensis	sinensis	k1gFnSc2	sinensis
Osbeck	Osbeck	k1gMnSc1	Osbeck
cv	cv	k?	cv
'	'	kIx"	'
<g/>
Robertson	Robertson	k1gMnSc1	Robertson
Navel	Navel	k1gMnSc1	Navel
<g/>
'	'	kIx"	'
Citrus	citrus	k1gInSc1	citrus
sinensis	sinensis	k1gFnSc2	sinensis
Osbeck	Osbeck	k1gMnSc1	Osbeck
cv	cv	k?	cv
'	'	kIx"	'
<g/>
Thompson	Thompson	k1gMnSc1	Thompson
Navel	Navel	k1gMnSc1	Navel
<g/>
'	'	kIx"	'
Citrus	citrus	k1gInSc1	citrus
sinensis	sinensis	k1gFnSc2	sinensis
Osbeck	Osbeck	k1gMnSc1	Osbeck
cv	cv	k?	cv
'	'	kIx"	'
<g/>
Washington	Washington	k1gInSc1	Washington
Navel	Navel	k1gInSc1	Navel
<g/>
'	'	kIx"	'
</s>
<s>
Ročně	ročně	k6eAd1	ročně
se	se	k3xPyFc4	se
sklidí	sklidit	k5eAaPmIp3nS	sklidit
asi	asi	k9	asi
60	[number]	k4	60
milionů	milion	k4xCgInPc2	milion
tun	tuna	k1gFnPc2	tuna
citrusových	citrusový	k2eAgInPc2d1	citrusový
plodů	plod	k1gInPc2	plod
<g/>
.	.	kIx.	.
</s>
<s>
Skoro	skoro	k6eAd1	skoro
70	[number]	k4	70
<g/>
%	%	kIx~	%
připadá	připadat	k5eAaPmIp3nS	připadat
na	na	k7c4	na
pomeranče	pomeranč	k1gInPc4	pomeranč
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
stromu	strom	k1gInSc2	strom
pomerančovníku	pomerančovník	k1gInSc2	pomerančovník
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
průmětu	průmět	k1gInSc6	průmět
6	[number]	k4	6
–	–	k?	–
10	[number]	k4	10
m	m	kA	m
vysoký	vysoký	k2eAgMnSc1d1	vysoký
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ročně	ročně	k6eAd1	ročně
sklidí	sklidit	k5eAaPmIp3nS	sklidit
7000	[number]	k4	7000
–	–	k?	–
8000	[number]	k4	8000
plodů	plod	k1gInPc2	plod
<g/>
,	,	kIx,	,
známé	známý	k2eAgInPc1d1	známý
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
výnosy	výnos	k1gInPc1	výnos
20000	[number]	k4	20000
plodů	plod	k1gInPc2	plod
<g/>
.	.	kIx.	.
</s>
<s>
Pomerančovníky	pomerančovník	k1gInPc1	pomerančovník
mají	mít	k5eAaImIp3nP	mít
kvalitní	kvalitní	k2eAgInPc1d1	kvalitní
plody	plod	k1gInPc1	plod
jen	jen	k9	jen
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
projdou	projít	k5eAaPmIp3nP	projít
<g/>
-li	i	k?	-li
obdobím	období	k1gNnSc7	období
chladu	chlad	k1gInSc2	chlad
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
poskytnutí	poskytnutí	k1gNnSc4	poskytnutí
co	co	k9	co
největší	veliký	k2eAgFnSc2d3	veliký
a	a	k8xC	a
kvalitní	kvalitní	k2eAgFnSc2d1	kvalitní
úrody	úroda	k1gFnSc2	úroda
nutno	nutno	k6eAd1	nutno
stromy	strom	k1gInPc4	strom
prořezávat	prořezávat	k5eAaImF	prořezávat
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
pomerančovník	pomerančovník	k1gInSc1	pomerančovník
snese	snést	k5eAaPmIp3nS	snést
poměrně	poměrně	k6eAd1	poměrně
různorodé	různorodý	k2eAgNnSc1d1	různorodé
subtropické	subtropický	k2eAgNnSc1d1	subtropické
podnebí	podnebí	k1gNnSc1	podnebí
<g/>
,	,	kIx,	,
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
kultivary	kultivar	k1gInPc1	kultivar
jsou	být	k5eAaImIp3nP	být
k	k	k7c3	k
docílení	docílení	k1gNnSc3	docílení
maximálního	maximální	k2eAgInSc2d1	maximální
a	a	k8xC	a
kvalitního	kvalitní	k2eAgInSc2d1	kvalitní
výnosu	výnos	k1gInSc2	výnos
vázány	vázat	k5eAaImNgFnP	vázat
na	na	k7c6	na
určité	určitý	k2eAgFnSc6d1	určitá
zeměpisné	zeměpisný	k2eAgFnSc6d1	zeměpisná
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Všeobecně	všeobecně	k6eAd1	všeobecně
pomerančovník	pomerančovník	k1gInSc1	pomerančovník
nesnáší	snášet	k5eNaImIp3nS	snášet
přemočená	přemočený	k2eAgNnPc4d1	přemočený
místa	místo	k1gNnPc4	místo
ani	ani	k8xC	ani
vysokou	vysoký	k2eAgFnSc4d1	vysoká
vzdušnou	vzdušný	k2eAgFnSc4d1	vzdušná
vlhkost	vlhkost	k1gFnSc4	vlhkost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
velkého	velký	k2eAgNnSc2d1	velké
sucha	sucho	k1gNnSc2	sucho
je	být	k5eAaImIp3nS	být
však	však	k9	však
nutno	nutno	k6eAd1	nutno
stromy	strom	k1gInPc4	strom
zavlažovat	zavlažovat	k5eAaImF	zavlažovat
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
plné	plný	k2eAgFnSc2d1	plná
vegetace	vegetace	k1gFnSc2	vegetace
požaduje	požadovat	k5eAaImIp3nS	požadovat
vysokou	vysoký	k2eAgFnSc4d1	vysoká
teplotu	teplota	k1gFnSc4	teplota
a	a	k8xC	a
hodně	hodně	k6eAd1	hodně
slunečního	sluneční	k2eAgInSc2d1	sluneční
svitu	svit	k1gInSc2	svit
<g/>
,	,	kIx,	,
v	v	k7c6	v
období	období	k1gNnSc6	období
klidu	klid	k1gInSc2	klid
přežije	přežít	k5eAaPmIp3nS	přežít
krátkodobý	krátkodobý	k2eAgInSc4d1	krátkodobý
pokles	pokles	k1gInSc4	pokles
teplot	teplota	k1gFnPc2	teplota
pod	pod	k7c4	pod
bod	bod	k1gInSc4	bod
mrazu	mráz	k1gInSc2	mráz
<g/>
.	.	kIx.	.
</s>
<s>
Nesnáší	snášet	k5eNaImIp3nS	snášet
zasolenou	zasolený	k2eAgFnSc4d1	zasolená
půdu	půda	k1gFnSc4	půda
<g/>
,	,	kIx,	,
ideální	ideální	k2eAgInSc4d1	ideální
pH	ph	kA	ph
je	být	k5eAaImIp3nS	být
okolo	okolo	k7c2	okolo
5	[number]	k4	5
až	až	k9	až
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Stromy	strom	k1gInPc1	strom
se	se	k3xPyFc4	se
dožívají	dožívat	k5eAaImIp3nP	dožívat
až	až	k9	až
100	[number]	k4	100
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
z	z	k7c2	z
ekonomických	ekonomický	k2eAgInPc2d1	ekonomický
důvodů	důvod	k1gInPc2	důvod
se	se	k3xPyFc4	se
počítá	počítat	k5eAaImIp3nS	počítat
s	s	k7c7	s
životností	životnost	k1gFnSc7	životnost
30	[number]	k4	30
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgInPc1d1	Nové
stromky	stromek	k1gInPc1	stromek
se	se	k3xPyFc4	se
vypěstují	vypěstovat	k5eAaPmIp3nP	vypěstovat
roubováním	roubování	k1gNnSc7	roubování
na	na	k7c4	na
podnože	podnož	k1gFnPc4	podnož
vyrostlé	vyrostlý	k2eAgFnPc4d1	vyrostlá
ze	z	k7c2	z
semen	semeno	k1gNnPc2	semeno
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
plodí	plodit	k5eAaImIp3nS	plodit
za	za	k7c4	za
cca	cca	kA	cca
5	[number]	k4	5
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Pomerančovníky	pomerančovník	k1gInPc1	pomerančovník
se	se	k3xPyFc4	se
také	také	k9	také
vysazují	vysazovat	k5eAaImIp3nP	vysazovat
poblíž	poblíž	k6eAd1	poblíž
lidských	lidský	k2eAgNnPc2d1	lidské
obydlí	obydlí	k1gNnPc2	obydlí
i	i	k9	i
jako	jako	k9	jako
solitéry	solitér	k1gInPc1	solitér
pro	pro	k7c4	pro
poskytnutí	poskytnutí	k1gNnSc4	poskytnutí
stínu	stín	k1gInSc2	stín
<g/>
.	.	kIx.	.
</s>
<s>
Tabulka	tabulka	k1gFnSc1	tabulka
udává	udávat	k5eAaImIp3nS	udávat
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
průměrný	průměrný	k2eAgInSc1d1	průměrný
obsah	obsah	k1gInSc1	obsah
živin	živina	k1gFnPc2	živina
<g/>
,	,	kIx,	,
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
vitamínů	vitamín	k1gInPc2	vitamín
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
nutričních	nutriční	k2eAgInPc2d1	nutriční
parametrů	parametr	k1gInPc2	parametr
zjištěných	zjištěný	k2eAgInPc2d1	zjištěný
v	v	k7c6	v
pomerančích	pomeranč	k1gInPc6	pomeranč
<g/>
.	.	kIx.	.
</s>
