<p>
<s>
Anno	Anna	k1gFnSc5	Anna
Domini	Domin	k1gMnPc5	Domin
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
A.	A.	kA	A.
D.	D.	kA	D.
<g/>
)	)	kIx)	)
znamená	znamenat	k5eAaImIp3nS	znamenat
latinsky	latinsky	k6eAd1	latinsky
"	"	kIx"	"
<g/>
léta	léto	k1gNnSc2	léto
Páně	páně	k2eAgNnSc2d1	páně
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
L.	L.	kA	L.
P.	P.	kA	P.
<g/>
)	)	kIx)	)
a	a	k8xC	a
označuje	označovat	k5eAaImIp3nS	označovat
letopočet	letopočet	k1gInSc1	letopočet
od	od	k7c2	od
narození	narození	k1gNnSc2	narození
Krista	Kristus	k1gMnSc2	Kristus
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
historických	historický	k2eAgInPc6d1	historický
křesťanských	křesťanský	k2eAgInPc6d1	křesťanský
dokumentech	dokument	k1gInPc6	dokument
<g/>
,	,	kIx,	,
na	na	k7c6	na
nápisech	nápis	k1gInPc6	nápis
apod.	apod.	kA	apod.
V	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
používá	používat	k5eAaImIp3nS	používat
označení	označení	k1gNnSc1	označení
"	"	kIx"	"
<g/>
našeho	náš	k3xOp1gInSc2	náš
letopočtu	letopočet	k1gInSc2	letopočet
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
po	po	k7c4	po
Kristu	Krista	k1gFnSc4	Krista
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
po	po	k7c6	po
Kr.	Kr.	k1gFnSc6	Kr.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
"	"	kIx"	"
<g/>
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
éry	éra	k1gFnSc2	éra
<g/>
"	"	kIx"	"
ad	ad	k7c4	ad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Latinský	latinský	k2eAgInSc1d1	latinský
název	název	k1gInSc1	název
běžně	běžně	k6eAd1	běžně
používá	používat	k5eAaImIp3nS	používat
i	i	k9	i
angličtina	angličtina	k1gFnSc1	angličtina
(	(	kIx(	(
<g/>
zkracuje	zkracovat	k5eAaImIp3nS	zkracovat
na	na	k7c4	na
AD	ad	k7c4	ad
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vedle	vedle	k7c2	vedle
něho	on	k3xPp3gNnSc2	on
ale	ale	k9	ale
také	také	k9	také
používá	používat	k5eAaImIp3nS	používat
označení	označení	k1gNnSc1	označení
"	"	kIx"	"
<g/>
Common	Common	k1gMnSc1	Common
Era	Era	k1gMnSc1	Era
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
"	"	kIx"	"
<g/>
CE	CE	kA	CE
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
C.E.	C.E.	k1gFnSc1	C.E.
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
obdobu	obdoba	k1gFnSc4	obdoba
"	"	kIx"	"
<g/>
našeho	náš	k3xOp1gInSc2	náš
letopočtu	letopočet	k1gInSc2	letopočet
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Léta	léto	k1gNnPc1	léto
před	před	k7c7	před
narozením	narození	k1gNnSc7	narození
Krista	Krista	k1gFnSc1	Krista
označuje	označovat	k5eAaImIp3nS	označovat
latinské	latinský	k2eAgFnSc2d1	Latinská
ante	ant	k1gFnSc2	ant
Christum	Christum	k1gNnSc1	Christum
natum	natum	k1gInSc1	natum
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
AC	AC	kA	AC
<g/>
)	)	kIx)	)
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
před	před	k7c7	před
narozením	narození	k1gNnSc7	narození
(	(	kIx(	(
<g/>
Ježíše	Ježíš	k1gMnSc2	Ježíš
<g/>
)	)	kIx)	)
Krista	Kristus	k1gMnSc2	Kristus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
křesťanského	křesťanský	k2eAgInSc2d1	křesťanský
letopočtu	letopočet	k1gInSc2	letopočet
==	==	k?	==
</s>
</p>
<p>
<s>
Označení	označení	k1gNnSc1	označení
je	být	k5eAaImIp3nS	být
používáno	používat	k5eAaImNgNnS	používat
pro	pro	k7c4	pro
počítání	počítání	k1gNnSc4	počítání
let	léto	k1gNnPc2	léto
v	v	k7c6	v
křesťanské	křesťanský	k2eAgFnSc6d1	křesťanská
éře	éra	k1gFnSc6	éra
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
konvenčně	konvenčně	k6eAd1	konvenčně
používáno	používat	k5eAaImNgNnS	používat
juliánským	juliánský	k2eAgInSc7d1	juliánský
a	a	k8xC	a
gregoriánským	gregoriánský	k2eAgInSc7d1	gregoriánský
kalendářem	kalendář	k1gInSc7	kalendář
<g/>
.	.	kIx.	.
</s>
<s>
Přesněji	přesně	k6eAd2	přesně
<g/>
,	,	kIx,	,
roky	rok	k1gInPc1	rok
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
specifikovány	specifikovat	k5eAaBmNgInP	specifikovat
také	také	k9	také
jako	jako	k9	jako
Anno	Anna	k1gFnSc5	Anna
Domini	Domin	k1gMnPc1	Domin
Nostri	Nostr	k1gFnSc3	Nostr
Iesu	Iesus	k1gInSc2	Iesus
(	(	kIx(	(
<g/>
Jesu	Jesus	k1gInSc2	Jesus
<g/>
)	)	kIx)	)
Christi	Christ	k1gMnPc1	Christ
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
našeho	náš	k3xOp1gMnSc2	náš
pána	pán	k1gMnSc2	pán
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Datování	datování	k1gNnSc1	datování
'	'	kIx"	'
<g/>
anno	anno	k6eAd1	anno
Domini	Domin	k2eAgMnPc1d1	Domin
<g/>
'	'	kIx"	'
bylo	být	k5eAaImAgNnS	být
poprvé	poprvé	k6eAd1	poprvé
zavedeno	zavést	k5eAaPmNgNnS	zavést
roku	rok	k1gInSc2	rok
525	[number]	k4	525
a	a	k8xC	a
začalo	začít	k5eAaPmAgNnS	začít
být	být	k5eAaImF	být
používáno	používat	k5eAaImNgNnS	používat
v	v	k7c6	v
Západní	západní	k2eAgFnSc6d1	západní
Evropě	Evropa	k1gFnSc6	Evropa
během	během	k7c2	během
8	[number]	k4	8
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Číslování	číslování	k1gNnSc1	číslování
let	léto	k1gNnPc2	léto
podle	podle	k7c2	podle
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
éry	éra	k1gFnSc2	éra
je	být	k5eAaImIp3nS	být
běžně	běžně	k6eAd1	běžně
dominantní	dominantní	k2eAgFnSc1d1	dominantní
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
zemích	zem	k1gFnPc6	zem
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k6eAd1	jak
v	v	k7c6	v
komerční	komerční	k2eAgFnSc6d1	komerční
tak	tak	k6eAd1	tak
i	i	k9	i
ve	v	k7c6	v
vědecké	vědecký	k2eAgFnSc6d1	vědecká
praxi	praxe	k1gFnSc6	praxe
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
desetiletí	desetiletí	k1gNnSc6	desetiletí
je	být	k5eAaImIp3nS	být
globálním	globální	k2eAgInSc7d1	globální
standardem	standard	k1gInSc7	standard
uznávaným	uznávaný	k2eAgInSc7d1	uznávaný
mezinárodními	mezinárodní	k2eAgFnPc7d1	mezinárodní
institucemi	instituce	k1gFnPc7	instituce
jako	jako	k8xC	jako
např.	např.	kA	např.
Organizace	organizace	k1gFnSc1	organizace
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
nebo	nebo	k8xC	nebo
Světová	světový	k2eAgFnSc1d1	světová
poštovní	poštovní	k2eAgFnSc1d1	poštovní
unie	unie	k1gFnSc1	unie
<g/>
.	.	kIx.	.
</s>
<s>
Příčinou	příčina	k1gFnSc7	příčina
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
datování	datování	k1gNnSc2	datování
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
křesťanství	křesťanství	k1gNnSc3	křesťanství
<g/>
,	,	kIx,	,
převládá	převládat	k5eAaImIp3nS	převládat
v	v	k7c6	v
západním	západní	k2eAgInSc6d1	západní
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
velký	velký	k2eAgInSc1d1	velký
vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
vědu	věda	k1gFnSc4	věda
<g/>
,	,	kIx,	,
technologii	technologie	k1gFnSc4	technologie
a	a	k8xC	a
obchod	obchod	k1gInSc4	obchod
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
solární	solární	k2eAgInSc1d1	solární
Gregoriánský	gregoriánský	k2eAgInSc1d1	gregoriánský
kalendář	kalendář	k1gInSc1	kalendář
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
astronomicky	astronomicky	k6eAd1	astronomicky
poměrně	poměrně	k6eAd1	poměrně
přesný	přesný	k2eAgInSc1d1	přesný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Angličtina	angličtina	k1gFnSc1	angličtina
==	==	k?	==
</s>
</p>
<p>
<s>
Tradičně	tradičně	k6eAd1	tradičně
angličtina	angličtina	k1gFnSc1	angličtina
kopíruje	kopírovat	k5eAaImIp3nS	kopírovat
latinu	latina	k1gFnSc4	latina
při	při	k7c6	při
umísťování	umísťování	k1gNnSc6	umísťování
zkratky	zkratka	k1gFnSc2	zkratka
AD	ad	k7c4	ad
před	před	k7c4	před
číslici	číslice	k1gFnSc4	číslice
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
zkratku	zkratka	k1gFnSc4	zkratka
BC	BC	kA	BC
umísťuje	umísťovat	k5eAaImIp3nS	umísťovat
za	za	k7c4	za
číslici	číslice	k1gFnSc4	číslice
roku	rok	k1gInSc2	rok
<g/>
;	;	kIx,	;
např.	např.	kA	např.
64	[number]	k4	64
BC	BC	kA	BC
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
AD	ad	k7c4	ad
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
nyní	nyní	k6eAd1	nyní
běžně	běžně	k6eAd1	běžně
používá	používat	k5eAaImIp3nS	používat
i	i	k9	i
umístění	umístění	k1gNnSc1	umístění
zkratky	zkratka	k1gFnSc2	zkratka
AD	ad	k7c4	ad
za	za	k7c7	za
číslicí	číslice	k1gFnSc7	číslice
roku	rok	k1gInSc2	rok
(	(	kIx(	(
<g/>
např.	např.	kA	např.
2007	[number]	k4	2007
AD	ad	k7c4	ad
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
při	při	k7c6	při
použití	použití	k1gNnSc6	použití
zkratky	zkratka	k1gFnSc2	zkratka
BC	BC	kA	BC
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
také	také	k9	také
používána	používat	k5eAaImNgFnS	používat
při	při	k7c6	při
označení	označení	k1gNnSc6	označení
století	století	k1gNnSc2	století
nebo	nebo	k8xC	nebo
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
AD	ad	k7c4	ad
nebo	nebo	k8xC	nebo
2	[number]	k4	2
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnPc4	tisíciletí
AD	ad	k7c4	ad
<g/>
,	,	kIx,	,
navzdory	navzdory	k6eAd1	navzdory
nevhodné	vhodný	k2eNgFnSc6d1	nevhodná
kombinaci	kombinace	k1gFnSc6	kombinace
slov	slovo	k1gNnPc2	slovo
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
ve	v	k7c6	v
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
v	v	k7c6	v
roce	rok	k1gInSc6	rok
našeho	náš	k3xOp1gMnSc2	náš
Pána	pán	k1gMnSc2	pán
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Všimněte	všimnout	k5eAaPmRp2nP	všimnout
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
písmeno	písmeno	k1gNnSc1	písmeno
"	"	kIx"	"
<g/>
A	A	kA	A
<g/>
"	"	kIx"	"
ve	v	k7c6	v
výrazu	výraz	k1gInSc6	výraz
AD	ad	k7c4	ad
by	by	kYmCp3nP	by
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
množné	množný	k2eAgNnSc4d1	množné
číslo	číslo	k1gNnSc4	číslo
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
"	"	kIx"	"
<g/>
annis	annis	k1gFnSc2	annis
domini	domin	k2eAgMnPc1d1	domin
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
lze	lze	k6eAd1	lze
přeložit	přeložit	k5eAaPmF	přeložit
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
v	v	k7c6	v
rocích	rok	k1gInPc6	rok
našeho	náš	k3xOp1gMnSc2	náš
Pána	pán	k1gMnSc2	pán
<g/>
"	"	kIx"	"
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
daný	daný	k2eAgInSc4d1	daný
problém	problém	k1gInSc4	problém
odstraní	odstranit	k5eAaPmIp3nS	odstranit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Jiné	jiný	k2eAgInPc1d1	jiný
názvy	název	k1gInPc1	název
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Křesťanské	křesťanský	k2eAgInPc1d1	křesťanský
názvy	název	k1gInPc1	název
===	===	k?	===
</s>
</p>
<p>
<s>
Anno	Anna	k1gFnSc5	Anna
domini	domin	k2eAgMnPc1d1	domin
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
stejné	stejný	k2eAgNnSc4d1	stejné
označení	označení	k1gNnSc4	označení
<g/>
,	,	kIx,	,
jako	jako	k9	jako
archaické	archaický	k2eAgNnSc1d1	archaické
léta	léto	k1gNnPc1	léto
Páně	páně	k2eAgNnPc1d1	páně
nebo	nebo	k8xC	nebo
modernější	moderní	k2eAgNnPc1d2	modernější
po	po	k7c4	po
Kristu	Krista	k1gFnSc4	Krista
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
používal	používat	k5eAaImAgInS	používat
také	také	k9	také
výraz	výraz	k1gInSc1	výraz
Anno	Anna	k1gFnSc5	Anna
Salutis	Salutis	k1gFnSc6	Salutis
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
v	v	k7c6	v
roce	rok	k1gInSc6	rok
Spásy	spása	k1gFnSc2	spása
lidstva	lidstvo	k1gNnSc2	lidstvo
od	od	k7c2	od
věčného	věčný	k2eAgNnSc2d1	věčné
zatracení	zatracení	k1gNnSc2	zatracení
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
dle	dle	k7c2	dle
křesťanského	křesťanský	k2eAgNnSc2d1	křesťanské
pojetí	pojetí	k1gNnSc2	pojetí
přišla	přijít	k5eAaPmAgFnS	přijít
s	s	k7c7	s
narozením	narození	k1gNnSc7	narození
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
jsou	být	k5eAaImIp3nP	být
používány	používán	k2eAgFnPc4d1	používána
propracovanější	propracovaný	k2eAgFnPc4d2	propracovanější
formy	forma	k1gFnPc4	forma
tohoto	tento	k3xDgInSc2	tento
výrazu	výraz	k1gInSc2	výraz
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
Anno	Anna	k1gFnSc5	Anna
Nostrae	Nostrae	k1gInSc4	Nostrae
Salutis	Salutis	k1gFnPc4	Salutis
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
naší	náš	k3xOp1gFnSc2	náš
spásy	spása	k1gFnSc2	spása
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Anno	Anna	k1gFnSc5	Anna
Salutis	Salutis	k1gInSc4	Salutis
Humanae	Humanae	k1gNnPc7	Humanae
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
spásy	spása	k1gFnSc2	spása
lidstva	lidstvo	k1gNnSc2	lidstvo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Anno	Anna	k1gFnSc5	Anna
Reparatae	Reparatae	k1gInSc4	Reparatae
Salutis	Salutis	k1gFnPc4	Salutis
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
dovršené	dovršený	k2eAgFnSc2d1	dovršená
spásy	spása	k1gFnSc2	spása
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Civilní	civilní	k2eAgInPc1d1	civilní
názvy	název	k1gInPc1	název
===	===	k?	===
</s>
</p>
<p>
<s>
Stejný	stejný	k2eAgInSc1d1	stejný
význam	význam	k1gInSc1	význam
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
výraz	výraz	k1gInSc1	výraz
náš	náš	k3xOp1gInSc4	náš
letopočet	letopočet	k1gInSc4	letopočet
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
vyhýbá	vyhýbat	k5eAaImIp3nS	vyhýbat
odkazu	odkaz	k1gInSc2	odkaz
na	na	k7c6	na
křesťanský	křesťanský	k2eAgMnSc1d1	křesťanský
původu	původ	k1gInSc2	původ
tohoto	tento	k3xDgInSc2	tento
letopočtu	letopočet	k1gInSc2	letopočet
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
vhodnější	vhodný	k2eAgNnSc4d2	vhodnější
pro	pro	k7c4	pro
civilní	civilní	k2eAgInPc4d1	civilní
<g/>
,	,	kIx,	,
nenáboženské	náboženský	k2eNgInPc4d1	nenáboženský
účely	účel	k1gInPc4	účel
a	a	k8xC	a
také	také	k9	také
v	v	k7c6	v
dialogu	dialog	k1gInSc6	dialog
s	s	k7c7	s
ostatními	ostatní	k2eAgFnPc7d1	ostatní
církvemi	církev	k1gFnPc7	církev
<g/>
.	.	kIx.	.
</s>
<s>
Některými	některý	k3yIgMnPc7	některý
lidmi	člověk	k1gMnPc7	člověk
je	být	k5eAaImIp3nS	být
takové	takový	k3xDgNnSc1	takový
označení	označení	k1gNnSc1	označení
vnímáno	vnímat	k5eAaImNgNnS	vnímat
jako	jako	k8xS	jako
politicky	politicky	k6eAd1	politicky
korektní	korektní	k2eAgFnSc1d1	korektní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
židovském	židovský	k2eAgInSc6d1	židovský
kontextu	kontext	k1gInSc6	kontext
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
používán	používat	k5eAaImNgInS	používat
výraz	výraz	k1gInSc1	výraz
občanský	občanský	k2eAgInSc1d1	občanský
letopočet	letopočet	k1gInSc1	letopočet
(	(	kIx(	(
<g/>
o.	o.	k?	o.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Čínská	čínský	k2eAgFnSc1d1	čínská
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
založená	založený	k2eAgFnSc1d1	založená
roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
převzala	převzít	k5eAaPmAgFnS	převzít
západní	západní	k2eAgFnSc4d1	západní
datování	datování	k1gNnSc3	datování
<g/>
,	,	kIx,	,
když	když	k8xS	když
tuto	tento	k3xDgFnSc4	tento
éru	éra	k1gFnSc4	éra
nazvala	nazvat	k5eAaBmAgFnS	nazvat
gō	gō	k?	gō
(	(	kIx(	(
<g/>
公	公	k?	公
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
doslovně	doslovně	k6eAd1	doslovně
znamená	znamenat	k5eAaImIp3nS	znamenat
Běžný	běžný	k2eAgInSc1d1	běžný
letopočet	letopočet	k1gInSc1	letopočet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
speciálních	speciální	k2eAgInPc6d1	speciální
případech	případ	k1gInPc6	případ
(	(	kIx(	(
<g/>
Thelema	Thelema	k1gNnSc4	Thelema
<g/>
,	,	kIx,	,
Alfred	Alfred	k1gMnSc1	Alfred
Jarry	Jarra	k1gFnSc2	Jarra
<g/>
,	,	kIx,	,
zednářství	zednářství	k1gNnSc1	zednářství
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
latinský	latinský	k2eAgInSc1d1	latinský
výraz	výraz	k1gInSc1	výraz
Era	Era	k1gFnSc2	Era
vulgaris	vulgaris	k1gFnSc2	vulgaris
(	(	kIx(	(
<g/>
e.v.	e.v.	k?	e.v.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
S.	S.	kA	S.
I.	I.	kA	I.
Selešnikov	Selešnikov	k1gInSc1	Selešnikov
(	(	kIx(	(
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Člověk	člověk	k1gMnSc1	člověk
a	a	k8xC	a
čas	čas	k1gInSc1	čas
–	–	k?	–
nakl	nakl	k1gInSc1	nakl
<g/>
.	.	kIx.	.
</s>
<s>
Práce	práce	k1gFnSc1	práce
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
</s>
</p>
<p>
<s>
Eva	Eva	k1gFnSc1	Eva
Kotulová	Kotulový	k2eAgFnSc1d1	Kotulová
(	(	kIx(	(
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kalendář	kalendář	k1gInSc1	kalendář
aneb	aneb	k?	aneb
kniha	kniha	k1gFnSc1	kniha
o	o	k7c6	o
věčnosti	věčnost	k1gFnSc6	věčnost
času	čas	k1gInSc2	čas
–	–	k?	–
nakl	nakl	k1gMnSc1	nakl
<g/>
.	.	kIx.	.
</s>
<s>
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
</s>
</p>
<p>
<s>
David	David	k1gMnSc1	David
Ewing	Ewing	k1gMnSc1	Ewing
Duncan	Duncan	k1gMnSc1	Duncan
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kalendář	kalendář	k1gInSc1	kalendář
–	–	k?	–
vydav	vydávit	k5eAaPmRp2nS	vydávit
<g/>
.	.	kIx.	.
</s>
<s>
VOLVOX	VOLVOX	kA	VOLVOX
GLOBATOR	GLOBATOR	kA	GLOBATOR
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7207-326-5	[number]	k4	80-7207-326-5
</s>
</p>
<p>
<s>
Marie	Marie	k1gFnSc1	Marie
Bláhová	bláhový	k2eAgFnSc1d1	bláhová
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Historická	historický	k2eAgFnSc1d1	historická
chronologie	chronologie	k1gFnSc1	chronologie
–	–	k?	–
nakl	nakl	k1gInSc1	nakl
<g/>
.	.	kIx.	.
</s>
<s>
Libri	Libri	k6eAd1	Libri
<g/>
,	,	kIx,	,
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7277-024-1	[number]	k4	80-7277-024-1
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Ab	Ab	k?	Ab
urbe	urbat	k5eAaPmIp3nS	urbat
condita	condita	k1gFnSc1	condita
</s>
</p>
<p>
<s>
Chronologie	chronologie	k1gFnSc1	chronologie
</s>
</p>
<p>
<s>
ISO	ISO	kA	ISO
8601	[number]	k4	8601
</s>
</p>
<p>
<s>
Kalendář	kalendář	k1gInSc1	kalendář
</s>
</p>
<p>
<s>
Rok	rok	k1gInSc1	rok
nula	nula	k1gFnSc1	nula
</s>
</p>
<p>
<s>
Př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
</s>
</p>
<p>
<s>
AD	ad	k7c4	ad
(	(	kIx(	(
<g/>
časopis	časopis	k1gInSc4	časopis
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Astronomie	astronomie	k1gFnPc1	astronomie
a	a	k8xC	a
data	datum	k1gNnPc1	datum
biblických	biblický	k2eAgFnPc2d1	biblická
událostí	událost	k1gFnPc2	událost
–	–	k?	–
Jiří	Jiří	k1gMnSc1	Jiří
Grygar	Grygar	k1gMnSc1	Grygar
</s>
</p>
<p>
<s>
Kristem	Kristus	k1gMnSc7	Kristus
nebo	nebo	k8xC	nebo
před	před	k7c7	před
naším	náš	k3xOp1gInSc7	náš
letopočtem	letopočet	k1gInSc7	letopočet
<g/>
?	?	kIx.	?
</s>
<s>
–	–	k?	–
Naše	náš	k3xOp1gFnSc1	náš
řeč	řeč	k1gFnSc1	řeč
</s>
</p>
