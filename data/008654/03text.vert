<p>
<s>
Zlínská	zlínský	k2eAgFnSc1d1	zlínská
radnice	radnice	k1gFnSc1	radnice
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
Míru	mír	k1gInSc2	mír
ve	v	k7c6	v
Zlíně	Zlín	k1gInSc6	Zlín
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
postavena	postavit	k5eAaPmNgFnS	postavit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
starší	starý	k2eAgFnSc2d2	starší
vyhořelé	vyhořelý	k2eAgFnSc2d1	vyhořelá
renesanční	renesanční	k2eAgFnSc2d1	renesanční
radnice	radnice	k1gFnSc2	radnice
zlínským	zlínský	k2eAgMnSc7d1	zlínský
architektem	architekt	k1gMnSc7	architekt
Františkem	František	k1gMnSc7	František
Lydií	Lydie	k1gFnSc7	Lydie
Gahurou	Gahura	k1gFnSc7	Gahura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
radnici	radnice	k1gFnSc6	radnice
sídlí	sídlet	k5eAaImIp3nS	sídlet
Magistrát	magistrát	k1gInSc1	magistrát
města	město	k1gNnSc2	město
Zlína	Zlín	k1gInSc2	Zlín
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Nejstarší	starý	k2eAgFnSc1d3	nejstarší
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
zlínské	zlínský	k2eAgFnSc6d1	zlínská
radnici	radnice	k1gFnSc6	radnice
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1569	[number]	k4	1569
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jednopatrovou	jednopatrový	k2eAgFnSc4d1	jednopatrová
budovu	budova	k1gFnSc4	budova
na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
straně	strana	k1gFnSc6	strana
náměstí	náměstí	k1gNnSc2	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Znovu	znovu	k6eAd1	znovu
byla	být	k5eAaImAgNnP	být
postavena	postavit	k5eAaPmNgNnP	postavit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1586	[number]	k4	1586
v	v	k7c6	v
renesančním	renesanční	k2eAgInSc6d1	renesanční
slohu	sloh	k1gInSc6	sloh
<g/>
.	.	kIx.	.
</s>
<s>
Jedno	jeden	k4xCgNnSc1	jeden
její	její	k3xOp3gFnSc4	její
křídlo	křídlo	k1gNnSc1	křídlo
sloužilo	sloužit	k5eAaImAgNnS	sloužit
jako	jako	k8xC	jako
zájezdní	zájezdní	k2eAgInSc1d1	zájezdní
hostinec	hostinec	k1gInSc1	hostinec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
byla	být	k5eAaImAgFnS	být
vypsána	vypsán	k2eAgFnSc1d1	vypsána
soutěž	soutěž	k1gFnSc1	soutěž
na	na	k7c4	na
novostavbu	novostavba	k1gFnSc4	novostavba
zlínské	zlínský	k2eAgFnSc2d1	zlínská
radnice	radnice	k1gFnSc2	radnice
<g/>
.	.	kIx.	.
</s>
<s>
Vítězným	vítězný	k2eAgMnPc3d1	vítězný
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
návrh	návrh	k1gInSc1	návrh
zlínského	zlínský	k2eAgMnSc2d1	zlínský
rodáka	rodák	k1gMnSc2	rodák
architekta	architekt	k1gMnSc2	architekt
Františka	František	k1gMnSc2	František
Gahury	Gahura	k1gFnSc2	Gahura
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnSc1d1	původní
radnice	radnice	k1gFnSc1	radnice
vyhořela	vyhořet	k5eAaPmAgFnS	vyhořet
27	[number]	k4	27
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1921	[number]	k4	1921
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
položen	položit	k5eAaPmNgInS	položit
základní	základní	k2eAgInSc1d1	základní
kámen	kámen	k1gInSc1	kámen
nové	nový	k2eAgFnSc2d1	nová
radnice	radnice	k1gFnSc2	radnice
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
dokončena	dokončit	k5eAaPmNgFnS	dokončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
dnešní	dnešní	k2eAgFnSc2d1	dnešní
podoby	podoba	k1gFnSc2	podoba
byla	být	k5eAaImAgFnS	být
radnice	radnice	k1gFnSc1	radnice
dostavěna	dostavěn	k2eAgFnSc1d1	dostavěna
v	v	k7c6	v
letech	let	k1gInPc6	let
1936	[number]	k4	1936
<g/>
–	–	k?	–
<g/>
1937	[number]	k4	1937
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
její	její	k3xOp3gFnSc3	její
přístavbě	přístavba	k1gFnSc3	přístavba
v	v	k7c6	v
Bartošově	Bartošův	k2eAgFnSc6d1	Bartošova
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dílem	díl	k1gInSc7	díl
architekta	architekt	k1gMnSc2	architekt
a	a	k8xC	a
sochaře	sochař	k1gMnSc2	sochař
Františka	František	k1gMnSc2	František
Gahury	Gahura	k1gFnSc2	Gahura
je	být	k5eAaImIp3nS	být
i	i	k9	i
socha	socha	k1gFnSc1	socha
kováře	kovář	k1gMnSc2	kovář
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
straně	strana	k1gFnSc6	strana
budovy	budova	k1gFnSc2	budova
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Zlínská	zlínský	k2eAgFnSc1d1	zlínská
radnice	radnice	k1gFnSc1	radnice
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Webkamera	Webkamera	k1gFnSc1	Webkamera
-	-	kIx~	-
pohled	pohled	k1gInSc1	pohled
z	z	k7c2	z
radnice	radnice	k1gFnSc2	radnice
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
Míru	mír	k1gInSc2	mír
</s>
</p>
<p>
<s>
Historie	historie	k1gFnSc1	historie
zlínské	zlínský	k2eAgFnSc2d1	zlínská
radnice	radnice	k1gFnSc2	radnice
</s>
</p>
