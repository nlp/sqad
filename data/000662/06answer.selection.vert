<s>
Václav	Václav	k1gMnSc1	Václav
Lucemburský	lucemburský	k2eAgMnSc1d1	lucemburský
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
zvaný	zvaný	k2eAgMnSc1d1	zvaný
Český	český	k2eAgMnSc1d1	český
(	(	kIx(	(
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1337	[number]	k4	1337
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
8	[number]	k4	8
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1383	[number]	k4	1383
Lucemburk	Lucemburk	k1gMnSc1	Lucemburk
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
vévoda	vévoda	k1gMnSc1	vévoda
lucemburský	lucemburský	k2eAgMnSc1d1	lucemburský
(	(	kIx(	(
<g/>
od	od	k7c2	od
1353	[number]	k4	1353
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
brabantský	brabantský	k2eAgInSc1d1	brabantský
a	a	k8xC	a
limburský	limburský	k2eAgInSc1d1	limburský
(	(	kIx(	(
<g/>
od	od	k7c2	od
1355	[number]	k4	1355
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
českého	český	k2eAgMnSc2d1	český
krále	král	k1gMnSc2	král
Jana	Jan	k1gMnSc2	Jan
Lucemburského	lucemburský	k2eAgMnSc2d1	lucemburský
a	a	k8xC	a
Beatrix	Beatrix	k1gInSc1	Beatrix
Bourbonské	bourbonský	k2eAgFnSc2d1	Bourbonská
a	a	k8xC	a
nejmladší	mladý	k2eAgFnSc2d3	nejmladší
(	(	kIx(	(
<g/>
polorodý	polorodý	k2eAgMnSc1d1	polorodý
<g/>
)	)	kIx)	)
bratr	bratr	k1gMnSc1	bratr
Karla	Karel	k1gMnSc2	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
