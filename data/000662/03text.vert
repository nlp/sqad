<s>
Václav	Václav	k1gMnSc1	Václav
Lucemburský	lucemburský	k2eAgMnSc1d1	lucemburský
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
zvaný	zvaný	k2eAgMnSc1d1	zvaný
Český	český	k2eAgMnSc1d1	český
(	(	kIx(	(
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1337	[number]	k4	1337
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
8	[number]	k4	8
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1383	[number]	k4	1383
Lucemburk	Lucemburk	k1gMnSc1	Lucemburk
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
vévoda	vévoda	k1gMnSc1	vévoda
lucemburský	lucemburský	k2eAgMnSc1d1	lucemburský
(	(	kIx(	(
<g/>
od	od	k7c2	od
1353	[number]	k4	1353
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
brabantský	brabantský	k2eAgInSc1d1	brabantský
a	a	k8xC	a
limburský	limburský	k2eAgInSc1d1	limburský
(	(	kIx(	(
<g/>
od	od	k7c2	od
1355	[number]	k4	1355
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
českého	český	k2eAgMnSc2d1	český
krále	král	k1gMnSc2	král
Jana	Jan	k1gMnSc2	Jan
Lucemburského	lucemburský	k2eAgMnSc2d1	lucemburský
a	a	k8xC	a
Beatrix	Beatrix	k1gInSc1	Beatrix
Bourbonské	bourbonský	k2eAgFnSc2d1	Bourbonská
a	a	k8xC	a
nejmladší	mladý	k2eAgFnSc2d3	nejmladší
(	(	kIx(	(
<g/>
polorodý	polorodý	k2eAgMnSc1d1	polorodý
<g/>
)	)	kIx)	)
bratr	bratr	k1gMnSc1	bratr
Karla	Karel	k1gMnSc2	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Proslul	proslout	k5eAaPmAgMnS	proslout
jako	jako	k9	jako
dlouholetý	dlouholetý	k2eAgMnSc1d1	dlouholetý
mecenáš	mecenáš	k1gMnSc1	mecenáš
kronikáře	kronikář	k1gMnSc2	kronikář
a	a	k8xC	a
básníka	básník	k1gMnSc2	básník
Jeana	Jean	k1gMnSc2	Jean
Froissarta	Froissart	k1gMnSc2	Froissart
a	a	k8xC	a
také	také	k9	také
sám	sám	k3xTgMnSc1	sám
jako	jako	k9	jako
básník	básník	k1gMnSc1	básník
<g/>
.	.	kIx.	.
</s>
<s>
Skládal	skládat	k5eAaImAgInS	skládat
milostnou	milostný	k2eAgFnSc4d1	milostná
trubadúrskou	trubadúrský	k2eAgFnSc4d1	trubadúrská
poezii	poezie	k1gFnSc4	poezie
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
autorem	autor	k1gMnSc7	autor
79	[number]	k4	79
básní	báseň	k1gFnPc2	báseň
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
překlad	překlad	k1gInSc1	překlad
jeho	jeho	k3xOp3gFnPc2	jeho
lyrických	lyrický	k2eAgFnPc2d1	lyrická
básní	báseň	k1gFnPc2	báseň
od	od	k7c2	od
Gustava	Gustav	k1gMnSc2	Gustav
Francla	Francl	k1gMnSc2	Francl
nese	nést	k5eAaImIp3nS	nést
název	název	k1gInSc1	název
Netoužím	toužit	k5eNaImIp1nS	toužit
po	po	k7c6	po
ráji	ráj	k1gInSc6	ráj
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
jako	jako	k9	jako
třetí	třetí	k4xOgMnSc1	třetí
syn	syn	k1gMnSc1	syn
českého	český	k2eAgMnSc2d1	český
krále	král	k1gMnSc2	král
Jana	Jan	k1gMnSc2	Jan
Lucemburského	lucemburský	k2eAgMnSc2d1	lucemburský
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc6	jeho
druhé	druhý	k4xOgFnSc6	druhý
choti	choť	k1gFnSc6	choť
Beatrix	Beatrix	k1gInSc1	Beatrix
<g/>
,	,	kIx,	,
dcery	dcera	k1gFnPc1	dcera
Ludvíka	Ludvík	k1gMnSc2	Ludvík
Bourbonského	bourbonský	k2eAgMnSc2d1	bourbonský
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
císařským	císařský	k2eAgInSc7d1	císařský
řezem	řez	k1gInSc7	řez
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
přežila	přežít	k5eAaPmAgFnS	přežít
i	i	k8xC	i
jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
Beatrix	Beatrix	k1gInSc4	Beatrix
a	a	k8xC	a
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
něm	on	k3xPp3gInSc6	on
vlámská	vlámský	k2eAgFnSc1d1	vlámská
kronika	kronika	k1gFnSc1	kronika
Brabantsche	Brabantsche	k1gFnPc2	Brabantsche
Yeesten	Yeesten	k2eAgInSc1d1	Yeesten
<g/>
.	.	kIx.	.
</s>
<s>
Snad	snad	k9	snad
jako	jako	k9	jako
poděkování	poděkování	k1gNnSc4	poděkování
za	za	k7c4	za
tento	tento	k3xDgInSc4	tento
zázrak	zázrak	k1gInSc4	zázrak
dostal	dostat	k5eAaPmAgMnS	dostat
jméno	jméno	k1gNnSc4	jméno
Václav	Václav	k1gMnSc1	Václav
<g/>
,	,	kIx,	,
jméno	jméno	k1gNnSc1	jméno
zemského	zemský	k2eAgNnSc2d1	zemské
patrona	patrona	k1gFnSc1	patrona
obvyklé	obvyklý	k2eAgNnSc1d1	obvyklé
v	v	k7c6	v
přemyslovské	přemyslovský	k2eAgFnSc6d1	Přemyslovská
dynastii	dynastie	k1gFnSc6	dynastie
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
jej	on	k3xPp3gMnSc4	on
chtěl	chtít	k5eAaImAgMnS	chtít
oženit	oženit	k5eAaPmF	oženit
s	s	k7c7	s
dcerou	dcera	k1gFnSc7	dcera
římského	římský	k2eAgMnSc2d1	římský
císaře	císař	k1gMnSc2	císař
Ludvíka	Ludvík	k1gMnSc2	Ludvík
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Bavora	Bavor	k1gMnSc2	Bavor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
z	z	k7c2	z
plánovaného	plánovaný	k2eAgInSc2d1	plánovaný
sňatku	sňatek	k1gInSc2	sňatek
sešlo	sejít	k5eAaPmAgNnS	sejít
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
vzájemného	vzájemný	k2eAgInSc2d1	vzájemný
rozkolu	rozkol	k1gInSc2	rozkol
obou	dva	k4xCgInPc2	dva
panovníků	panovník	k1gMnPc2	panovník
<g/>
.	.	kIx.	.
</s>
<s>
Druhé	druhý	k4xOgInPc4	druhý
zásnuby	zásnub	k1gInPc4	zásnub
sjednala	sjednat	k5eAaPmAgFnS	sjednat
již	již	k9	již
ovdovělá	ovdovělý	k2eAgFnSc1d1	ovdovělá
Václavova	Václavův	k2eAgFnSc1d1	Václavova
matka	matka	k1gFnSc1	matka
Beatrix	Beatrix	k1gInSc1	Beatrix
s	s	k7c7	s
brabantským	brabantský	k2eAgMnSc7d1	brabantský
vévodou	vévoda	k1gMnSc7	vévoda
Janem	Jan	k1gMnSc7	Jan
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Zásnuby	zásnuba	k1gFnPc1	zásnuba
s	s	k7c7	s
poněkud	poněkud	k6eAd1	poněkud
starší	starý	k2eAgFnSc7d2	starší
ovdovělou	ovdovělý	k2eAgFnSc7d1	ovdovělá
Johanou	Johana	k1gFnSc7	Johana
Brabantskou	Brabantský	k2eAgFnSc7d1	Brabantská
<g/>
,	,	kIx,	,
dědičkou	dědička	k1gFnSc7	dědička
Brabantska	Brabantsko	k1gNnSc2	Brabantsko
a	a	k8xC	a
Limburska	Limbursko	k1gNnSc2	Limbursko
<g/>
,	,	kIx,	,
proběhly	proběhnout	k5eAaPmAgFnP	proběhnout
ve	v	k7c6	v
Václavových	Václavových	k2eAgNnPc6d1	Václavových
deseti	deset	k4xCc6	deset
letech	léto	k1gNnPc6	léto
(	(	kIx(	(
<g/>
snoubence	snoubenka	k1gFnSc6	snoubenka
bylo	být	k5eAaImAgNnS	být
pětadvacet	pětadvacet	k4xCc1	pětadvacet
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
17	[number]	k4	17
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1351	[number]	k4	1351
byla	být	k5eAaImAgFnS	být
podepsána	podepsán	k2eAgFnSc1d1	podepsána
sňatková	sňatkový	k2eAgFnSc1d1	sňatková
dohoda	dohoda	k1gFnSc1	dohoda
a	a	k8xC	a
samotný	samotný	k2eAgInSc1d1	samotný
obřad	obřad	k1gInSc1	obřad
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1352	[number]	k4	1352
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
se	se	k3xPyFc4	se
Václav	Václav	k1gMnSc1	Václav
stal	stát	k5eAaPmAgMnS	stát
otcem	otec	k1gMnSc7	otec
několika	několik	k4yIc2	několik
levobočků	levoboček	k1gMnPc2	levoboček
<g/>
,	,	kIx,	,
s	s	k7c7	s
Johanou	Johana	k1gFnSc7	Johana
děti	dítě	k1gFnPc4	dítě
neměli	mít	k5eNaImAgMnP	mít
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
jejich	jejich	k3xOp3gNnSc1	jejich
manželství	manželství	k1gNnSc1	manželství
zřejmě	zřejmě	k6eAd1	zřejmě
bylo	být	k5eAaImAgNnS	být
harmonické	harmonický	k2eAgNnSc1d1	harmonické
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
dobové	dobový	k2eAgFnSc2d1	dobová
klášterní	klášterní	k2eAgFnSc2d1	klášterní
kroniky	kronika	k1gFnSc2	kronika
platil	platit	k5eAaImAgInS	platit
za	za	k7c2	za
<g/>
:	:	kIx,	:
Měl	mít	k5eAaImAgInS	mít
dobrý	dobrý	k2eAgInSc1d1	dobrý
vztah	vztah	k1gInSc1	vztah
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
starším	starý	k2eAgMnSc7d2	starší
bratrem	bratr	k1gMnSc7	bratr
Karlem	Karel	k1gMnSc7	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
navzájem	navzájem	k6eAd1	navzájem
se	se	k3xPyFc4	se
podporovali	podporovat	k5eAaImAgMnP	podporovat
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
pomáhali	pomáhat	k5eAaImAgMnP	pomáhat
svým	svůj	k3xOyFgMnPc3	svůj
francouzským	francouzský	k2eAgMnPc3d1	francouzský
synovcům	synovec	k1gMnPc3	synovec
<g/>
,	,	kIx,	,
synům	syn	k1gMnPc3	syn
Jitky	Jitka	k1gFnSc2	Jitka
Lucemburské	lucemburský	k2eAgFnSc2d1	Lucemburská
v	v	k7c6	v
době	doba	k1gFnSc6	doba
po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Poitiers	Poitiersa	k1gFnPc2	Poitiersa
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
údajně	údajně	k6eAd1	údajně
na	na	k7c4	na
lepru	lepra	k1gFnSc4	lepra
a	a	k8xC	a
pochován	pochován	k2eAgMnSc1d1	pochován
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
klášterním	klášterní	k2eAgInSc6d1	klášterní
kostele	kostel	k1gInSc6	kostel
cisterciáckého	cisterciácký	k2eAgInSc2d1	cisterciácký
kláštera	klášter	k1gInSc2	klášter
Orval	orvat	k5eAaPmAgMnS	orvat
v	v	k7c6	v
nynější	nynější	k2eAgFnSc6d1	nynější
Belgii	Belgie	k1gFnSc6	Belgie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
zachován	zachován	k2eAgInSc1d1	zachován
jeho	jeho	k3xOp3gInSc1	jeho
náhrobek	náhrobek	k1gInSc1	náhrobek
<g/>
.	.	kIx.	.
</s>
<s>
Posledním	poslední	k2eAgNnSc7d1	poslední
přáním	přání	k1gNnSc7	přání
lucemburského	lucemburský	k2eAgMnSc2d1	lucemburský
vévody	vévoda	k1gMnSc2	vévoda
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jeho	jeho	k3xOp3gNnSc4	jeho
srdce	srdce	k1gNnSc4	srdce
poslali	poslat	k5eAaPmAgMnP	poslat
manželce	manželka	k1gFnSc3	manželka
<g/>
.	.	kIx.	.
</s>
<s>
Václavovým	Václavův	k2eAgMnSc7d1	Václavův
dědicem	dědic	k1gMnSc7	dědic
v	v	k7c6	v
Lucembursku	Lucembursko	k1gNnSc6	Lucembursko
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jeho	jeho	k3xOp3gMnSc1	jeho
synovec	synovec	k1gMnSc1	synovec
Václav	Václav	k1gMnSc1	Václav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
;	;	kIx,	;
v	v	k7c6	v
Brabantsku	Brabantsko	k1gNnSc6	Brabantsko
a	a	k8xC	a
Limbursku	Limbursko	k1gNnSc6	Limbursko
nadále	nadále	k6eAd1	nadále
vládla	vládnout	k5eAaImAgFnS	vládnout
Johana	Johana	k1gFnSc1	Johana
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
pod	pod	k7c7	pod
tlakem	tlak	k1gInSc7	tlak
burgundského	burgundský	k2eAgMnSc2d1	burgundský
vévody	vévoda	k1gMnSc2	vévoda
Filipa	Filip	k1gMnSc2	Filip
Smělého	Smělý	k1gMnSc2	Smělý
přijala	přijmout	k5eAaPmAgFnS	přijmout
za	za	k7c4	za
dědice	dědic	k1gMnSc4	dědic
svého	svůj	k3xOyFgMnSc4	svůj
prasynovce	prasynovec	k1gMnSc4	prasynovec
(	(	kIx(	(
<g/>
a	a	k8xC	a
Filipova	Filipův	k2eAgMnSc2d1	Filipův
syna	syn	k1gMnSc2	syn
<g/>
)	)	kIx)	)
Antonína	Antonín	k1gMnSc2	Antonín
<g/>
.	.	kIx.	.
</s>
