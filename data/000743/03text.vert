<s>
Spréva	Spréva	k1gFnSc1	Spréva
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Spree	Spree	k1gFnSc1	Spree
<g/>
,	,	kIx,	,
dolnolužickosrbsky	dolnolužickosrbsky	k6eAd1	dolnolužickosrbsky
Sprjewja	Sprjewj	k2eAgFnSc1d1	Sprjewj
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
řeka	řeka	k1gFnSc1	řeka
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
(	(	kIx(	(
<g/>
okres	okres	k1gInSc1	okres
Děčín	Děčín	k1gInSc1	Děčín
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
(	(	kIx(	(
<g/>
Sasko	Sasko	k1gNnSc1	Sasko
<g/>
,	,	kIx,	,
Braniborsko	Braniborsko	k1gNnSc1	Braniborsko
<g/>
,	,	kIx,	,
Berlín	Berlín	k1gInSc1	Berlín
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
375,3	[number]	k4	375,3
km	km	kA	km
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
<g/>
.	.	kIx.	.
</s>
<s>
Plocha	plocha	k1gFnSc1	plocha
povodí	povodí	k1gNnSc2	povodí
měří	měřit	k5eAaImIp3nS	měřit
9858	[number]	k4	9858
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Pramení	pramenit	k5eAaImIp3nS	pramenit
nedaleko	nedaleko	k7c2	nedaleko
českého	český	k2eAgNnSc2d1	české
území	území	k1gNnSc2	území
u	u	k7c2	u
severního	severní	k2eAgNnSc2d1	severní
úpatí	úpatí	k1gNnSc2	úpatí
Lužických	lužický	k2eAgFnPc2d1	Lužická
hor	hora	k1gFnPc2	hora
z	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
pramenů	pramen	k1gInPc2	pramen
<g/>
:	:	kIx,	:
v	v	k7c6	v
Neugersdorfu	Neugersdorf	k1gInSc6	Neugersdorf
<g/>
,	,	kIx,	,
na	na	k7c6	na
západním	západní	k2eAgNnSc6d1	západní
úbočí	úbočí	k1gNnSc6	úbočí
hory	hora	k1gFnSc2	hora
Kottmar	Kottmara	k1gFnPc2	Kottmara
a	a	k8xC	a
v	v	k7c6	v
Ebersbach-Spreedorfu	Ebersbach-Spreedorf	k1gInSc6	Ebersbach-Spreedorf
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
Neugersdorfem	Neugersdorf	k1gInSc7	Neugersdorf
a	a	k8xC	a
Ebersbachem	Ebersbach	k1gInSc7	Ebersbach
tvoří	tvořit	k5eAaImIp3nP	tvořit
česko-německou	českoěmecký	k2eAgFnSc4d1	česko-německá
hranici	hranice	k1gFnSc4	hranice
a	a	k8xC	a
v	v	k7c6	v
katastru	katastr	k1gInSc6	katastr
zaniklé	zaniklý	k2eAgFnSc2d1	zaniklá
obce	obec	k1gFnSc2	obec
Fukov	Fukovo	k1gNnPc2	Fukovo
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
asi	asi	k9	asi
500	[number]	k4	500
metrů	metr	k1gInPc2	metr
vniká	vnikat	k5eAaImIp3nS	vnikat
na	na	k7c4	na
české	český	k2eAgNnSc4d1	české
území	území	k1gNnSc4	území
<g/>
.	.	kIx.	.
</s>
<s>
Teče	téct	k5eAaImIp3nS	téct
přes	přes	k7c4	přes
Středoevropskou	středoevropský	k2eAgFnSc4d1	středoevropská
nížinu	nížina	k1gFnSc4	nížina
<g/>
,	,	kIx,	,
protéká	protékat	k5eAaImIp3nS	protékat
přes	přes	k7c4	přes
jezera	jezero	k1gNnPc4	jezero
Schwielochsee	Schwielochsee	k1gFnPc2	Schwielochsee
a	a	k8xC	a
Müggel	Müggela	k1gFnPc2	Müggela
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
se	se	k3xPyFc4	se
zleva	zleva	k6eAd1	zleva
vlévá	vlévat	k5eAaImIp3nS	vlévat
do	do	k7c2	do
Havoly	Havola	k1gFnSc2	Havola
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1977	[number]	k4	1977
Budyšínská	budyšínský	k2eAgFnSc1d1	Budyšínská
vodní	vodní	k2eAgFnSc1d1	vodní
nádrž	nádrž	k1gFnSc1	nádrž
<g/>
.	.	kIx.	.
levé	levý	k2eAgFnSc2d1	levá
-	-	kIx~	-
Dahme	Dahm	k1gInSc5	Dahm
pravé	pravý	k2eAgMnPc4d1	pravý
-	-	kIx~	-
Schwarzer	Schwarzer	k1gInSc1	Schwarzer
Schöps	Schöps	k1gInSc1	Schöps
<g/>
,	,	kIx,	,
Löbauer	Löbauer	k1gMnSc1	Löbauer
Wasser	Wasser	k1gMnSc1	Wasser
<g/>
,	,	kIx,	,
Wuhle	Wuhle	k1gFnSc1	Wuhle
<g/>
,	,	kIx,	,
Panke	Panke	k1gFnSc1	Panke
Nejvyšších	vysoký	k2eAgInPc2d3	Nejvyšší
vodních	vodní	k2eAgInPc2d1	vodní
stavů	stav	k1gInPc2	stav
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
<g/>
,	,	kIx,	,
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
hladina	hladina	k1gFnSc1	hladina
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
průtok	průtok	k1gInSc1	průtok
v	v	k7c6	v
ústí	ústí	k1gNnSc6	ústí
je	být	k5eAaImIp3nS	být
36	[number]	k4	36
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Ten	ten	k3xDgInSc1	ten
je	být	k5eAaImIp3nS	být
regulován	regulovat	k5eAaImNgInS	regulovat
četnými	četný	k2eAgFnPc7d1	četná
přehradními	přehradní	k2eAgFnPc7d1	přehradní
nádržemi	nádrž	k1gFnPc7	nádrž
a	a	k8xC	a
jezery	jezero	k1gNnPc7	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Řeka	řeka	k1gFnSc1	řeka
nepravidelně	pravidelně	k6eNd1	pravidelně
zamrzá	zamrzat	k5eAaImIp3nS	zamrzat
jen	jen	k9	jen
v	v	k7c6	v
některých	některý	k3yIgNnPc6	některý
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Řeka	řeka	k1gFnSc1	řeka
je	být	k5eAaImIp3nS	být
splavněna	splavněn	k2eAgFnSc1d1	splavněna
a	a	k8xC	a
vodní	vodní	k2eAgFnSc1d1	vodní
doprava	doprava	k1gFnSc1	doprava
je	být	k5eAaImIp3nS	být
možná	možná	k9	možná
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
182	[number]	k4	182
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
vodní	vodní	k2eAgFnSc2d1	vodní
cesty	cesta	k1gFnSc2	cesta
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
spojuje	spojovat	k5eAaImIp3nS	spojovat
Odru	Odra	k1gFnSc4	Odra
(	(	kIx(	(
<g/>
s	s	k7c7	s
kterou	který	k3yQgFnSc7	který
je	být	k5eAaImIp3nS	být
spojena	spojit	k5eAaPmNgFnS	spojit
kanálem	kanál	k1gInSc7	kanál
<g/>
)	)	kIx)	)
a	a	k8xC	a
Labe	Labe	k1gNnSc4	Labe
(	(	kIx(	(
<g/>
do	do	k7c2	do
kterého	který	k3yIgInSc2	který
se	se	k3xPyFc4	se
vlévá	vlévat	k5eAaImIp3nS	vlévat
Havola	Havola	k1gFnSc1	Havola
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
leží	ležet	k5eAaImIp3nS	ležet
města	město	k1gNnSc2	město
Budyšín	Budyšín	k1gInSc1	Budyšín
<g/>
,	,	kIx,	,
Chotěbuz	Chotěbuz	k1gFnSc1	Chotěbuz
<g/>
,	,	kIx,	,
Berlín	Berlín	k1gInSc1	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgFnP	být
použity	použit	k2eAgFnPc1d1	použita
informace	informace	k1gFnPc1	informace
z	z	k7c2	z
Velké	velký	k2eAgFnSc2d1	velká
sovětské	sovětský	k2eAgFnSc2d1	sovětská
encyklopedie	encyklopedie	k1gFnSc2	encyklopedie
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
"	"	kIx"	"
<g/>
Ш	Ш	k?	Ш
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Galerie	galerie	k1gFnSc1	galerie
Spréva	Spréva	k1gFnSc1	Spréva
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc4	Commons
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Spréva	Spréva	k1gFnSc1	Spréva
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
