<p>
<s>
Rubikova	Rubikův	k2eAgFnSc1d1	Rubikova
kostka	kostka	k1gFnSc1	kostka
je	být	k5eAaImIp3nS	být
mechanický	mechanický	k2eAgInSc1d1	mechanický
hlavolam	hlavolam	k1gInSc1	hlavolam
<g/>
,	,	kIx,	,
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
původní	původní	k2eAgFnSc6d1	původní
podobě	podoba	k1gFnSc6	podoba
tvořený	tvořený	k2eAgInSc4d1	tvořený
krychlí	krychle	k1gFnSc7	krychle
složenou	složený	k2eAgFnSc7d1	složená
z	z	k7c2	z
dílčích	dílčí	k2eAgFnPc2d1	dílčí
barevných	barevný	k2eAgFnPc2d1	barevná
krychliček	krychlička	k1gFnPc2	krychlička
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
úkolem	úkol	k1gInSc7	úkol
je	být	k5eAaImIp3nS	být
rotacemi	rotace	k1gFnPc7	rotace
přeuspořádat	přeuspořádat	k5eAaPmF	přeuspořádat
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
dílčí	dílčí	k2eAgFnPc4d1	dílčí
části	část	k1gFnPc4	část
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
každá	každý	k3xTgFnSc1	každý
strana	strana	k1gFnSc1	strana
celého	celý	k2eAgNnSc2d1	celé
tělesa	těleso	k1gNnSc2	těleso
byla	být	k5eAaImAgFnS	být
obarvena	obarvit	k5eAaPmNgFnS	obarvit
jen	jen	k6eAd1	jen
jednou	jeden	k4xCgFnSc7	jeden
barvou	barva	k1gFnSc7	barva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
atypických	atypický	k2eAgFnPc6d1	atypická
variantách	varianta	k1gFnPc6	varianta
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
hranoly	hranol	k1gInPc4	hranol
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
nejsou	být	k5eNaImIp3nP	být
krychlemi	krychle	k1gFnPc7	krychle
<g/>
,	,	kIx,	,
jehlany	jehlan	k1gInPc7	jehlan
<g/>
,	,	kIx,	,
mnohostěny	mnohostěn	k1gInPc1	mnohostěn
a	a	k8xC	a
další	další	k2eAgNnPc1d1	další
tělesa	těleso	k1gNnPc1	těleso
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kostka	kostka	k1gFnSc1	kostka
formátu	formát	k1gInSc2	formát
3	[number]	k4	3
<g/>
×	×	k?	×
<g/>
3	[number]	k4	3
<g/>
×	×	k?	×
<g/>
3	[number]	k4	3
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
hitem	hit	k1gInSc7	hit
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
70	[number]	k4	70
<g/>
.	.	kIx.	.
a	a	k8xC	a
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
vyráběna	vyrábět	k5eAaImNgFnS	vyrábět
v	v	k7c6	v
milionových	milionový	k2eAgFnPc6d1	milionová
sériích	série	k1gFnPc6	série
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
nejprodávanějším	prodávaný	k2eAgInSc7d3	nejprodávanější
produktem	produkt	k1gInSc7	produkt
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
ji	on	k3xPp3gFnSc4	on
maďarský	maďarský	k2eAgMnSc1d1	maďarský
sochař	sochař	k1gMnSc1	sochař
a	a	k8xC	a
architekt	architekt	k1gMnSc1	architekt
Ernő	Ernő	k1gMnSc1	Ernő
Rubik	Rubik	k1gMnSc1	Rubik
19	[number]	k4	19
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1974	[number]	k4	1974
<g/>
,	,	kIx,	,
patent	patent	k1gInSc1	patent
podal	podat	k5eAaPmAgInS	podat
30	[number]	k4	30
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1975	[number]	k4	1975
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Terminologie	terminologie	k1gFnSc2	terminologie
==	==	k?	==
</s>
</p>
<p>
<s>
Kvádrové	kvádrový	k2eAgInPc1d1	kvádrový
modely	model	k1gInPc1	model
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
primárně	primárně	k6eAd1	primárně
třemi	tři	k4xCgInPc7	tři
přirozenými	přirozený	k2eAgInPc7d1	přirozený
čísly	číslo	k1gNnPc7	číslo
oddělenými	oddělený	k2eAgFnPc7d1	oddělená
znakem	znak	k1gInSc7	znak
"	"	kIx"	"
<g/>
×	×	k?	×
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
každé	každý	k3xTgNnSc4	každý
z	z	k7c2	z
čísel	číslo	k1gNnPc2	číslo
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
počet	počet	k1gInSc1	počet
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
krychliček	krychlička	k1gFnPc2	krychlička
na	na	k7c6	na
jiné	jiný	k2eAgFnSc6d1	jiná
straně	strana	k1gFnSc6	strana
kostky	kostka	k1gFnSc2	kostka
<g/>
;	;	kIx,	;
nejsou	být	k5eNaImIp3nP	být
<g/>
-li	i	k?	-li
čísla	číslo	k1gNnPc1	číslo
stejná	stejný	k2eAgNnPc1d1	stejné
<g/>
,	,	kIx,	,
obvyklejší	obvyklý	k2eAgNnSc1d2	obvyklejší
je	být	k5eAaImIp3nS	být
vzestupné	vzestupný	k2eAgNnSc1d1	vzestupné
řazení	řazení	k1gNnSc1	řazení
(	(	kIx(	(
<g/>
kupř	kupř	kA	kupř
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
3	[number]	k4	3
<g/>
×	×	k?	×
<g/>
4	[number]	k4	4
<g/>
×	×	k?	×
<g/>
4	[number]	k4	4
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Neortodoxní	ortodoxní	k2eNgFnPc1d1	neortodoxní
varianty	varianta	k1gFnPc1	varianta
nemají	mít	k5eNaImIp3nP	mít
snadno	snadno	k6eAd1	snadno
předvídatelné	předvídatelný	k2eAgNnSc4d1	předvídatelné
značení	značení	k1gNnSc4	značení
<g/>
;	;	kIx,	;
zpravidla	zpravidla	k6eAd1	zpravidla
má	mít	k5eAaImIp3nS	mít
každá	každý	k3xTgFnSc1	každý
svůj	svůj	k3xOyFgInSc4	svůj
obchodní	obchodní	k2eAgInSc4d1	obchodní
název	název	k1gInSc4	název
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
vazba	vazba	k1gFnSc1	vazba
na	na	k7c4	na
tvar	tvar	k1gInSc4	tvar
kostky	kostka	k1gFnSc2	kostka
je	být	k5eAaImIp3nS	být
pochybná	pochybný	k2eAgFnSc1d1	pochybná
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
vůbec	vůbec	k9	vůbec
žádná	žádný	k3yNgFnSc1	žádný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
stav	stav	k1gInSc4	stav
kostky	kostka	k1gFnSc2	kostka
<g/>
,	,	kIx,	,
české	český	k2eAgNnSc1d1	české
názvosloví	názvosloví	k1gNnSc1	názvosloví
je	být	k5eAaImIp3nS	být
takové	takový	k3xDgNnSc1	takový
<g/>
,	,	kIx,	,
že	že	k8xS	že
kostka	kostka	k1gFnSc1	kostka
v	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
uspořádání	uspořádání	k1gNnSc6	uspořádání
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
každé	každý	k3xTgFnSc6	každý
stěně	stěna	k1gFnSc6	stěna
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
právě	právě	k9	právě
jedna	jeden	k4xCgFnSc1	jeden
barva	barva	k1gFnSc1	barva
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
složená	složený	k2eAgFnSc1d1	složená
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
kostku	kostka	k1gFnSc4	kostka
nevyhovující	vyhovující	k2eNgFnSc4d1	nevyhovující
této	tento	k3xDgFnSc3	tento
podmínce	podmínka	k1gFnSc3	podmínka
označujeme	označovat	k5eAaImIp1nP	označovat
přívlastkem	přívlastek	k1gInSc7	přívlastek
rozházená	rozházený	k2eAgNnPc5d1	rozházené
<g/>
.	.	kIx.	.
</s>
<s>
Proces	proces	k1gInSc4	proces
transformace	transformace	k1gFnSc2	transformace
tělesa	těleso	k1gNnSc2	těleso
z	z	k7c2	z
rozházeného	rozházený	k2eAgMnSc2d1	rozházený
do	do	k7c2	do
složeného	složený	k2eAgInSc2d1	složený
stavu	stav	k1gInSc2	stav
pak	pak	k6eAd1	pak
nazýváme	nazývat	k5eAaImIp1nP	nazývat
skládáním	skládání	k1gNnSc7	skládání
<g/>
,	,	kIx,	,
či	či	k8xC	či
řešením	řešení	k1gNnSc7	řešení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Charakteristika	charakteristikon	k1gNnPc1	charakteristikon
základní	základní	k2eAgFnSc2d1	základní
verze	verze	k1gFnSc2	verze
==	==	k?	==
</s>
</p>
<p>
<s>
Nejběžnějším	běžný	k2eAgInSc7d3	nejběžnější
typem	typ	k1gInSc7	typ
kostky	kostka	k1gFnSc2	kostka
je	být	k5eAaImIp3nS	být
model	model	k1gInSc1	model
3	[number]	k4	3
<g/>
×	×	k?	×
<g/>
3	[number]	k4	3
<g/>
×	×	k?	×
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgFnPc4	tři
vrstvy	vrstva	k1gFnPc4	vrstva
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
dohromady	dohromady	k6eAd1	dohromady
tvoří	tvořit	k5eAaImIp3nP	tvořit
26	[number]	k4	26
krychliček	krychlička	k1gFnPc2	krychlička
–	–	k?	–
8	[number]	k4	8
rohů	roh	k1gInPc2	roh
(	(	kIx(	(
<g/>
corners	cornersit	k5eAaPmRp2nS	cornersit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
12	[number]	k4	12
hran	hrana	k1gFnPc2	hrana
(	(	kIx(	(
<g/>
edges	edges	k1gInSc1	edges
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
6	[number]	k4	6
středů	střed	k1gInPc2	střed
(	(	kIx(	(
<g/>
centres	centresa	k1gFnPc2	centresa
<g/>
)	)	kIx)	)
–	–	k?	–
a	a	k8xC	a
jádro	jádro	k1gNnSc1	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
z	z	k7c2	z
krychliček	krychlička	k1gFnPc2	krychlička
je	být	k5eAaImIp3nS	být
nositelem	nositel	k1gMnSc7	nositel
jedné	jeden	k4xCgFnSc2	jeden
<g/>
,	,	kIx,	,
dvou	dva	k4xCgNnPc2	dva
nebo	nebo	k8xC	nebo
tří	tři	k4xCgFnPc2	tři
jednobarevných	jednobarevný	k2eAgFnPc2d1	jednobarevná
nálepek	nálepka	k1gFnPc2	nálepka
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
dohromady	dohromady	k6eAd1	dohromady
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
celém	celý	k2eAgNnSc6d1	celé
tělese	těleso	k1gNnSc6	těleso
zastoupeno	zastoupit	k5eAaPmNgNnS	zastoupit
tolik	tolik	k4yIc1	tolik
barev	barva	k1gFnPc2	barva
<g/>
,	,	kIx,	,
kolik	kolik	k4yIc4	kolik
stěn	stěna	k1gFnPc2	stěna
má	mít	k5eAaImIp3nS	mít
krychle	krychle	k1gFnSc1	krychle
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
šest	šest	k4xCc1	šest
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
barev	barva	k1gFnPc2	barva
na	na	k7c6	na
krychličce	krychlička	k1gFnSc6	krychlička
je	být	k5eAaImIp3nS	být
dán	dát	k5eAaPmNgMnS	dát
jejím	její	k3xOp3gNnSc7	její
umístěním	umístění	k1gNnSc7	umístění
na	na	k7c6	na
tělese	těleso	k1gNnSc6	těleso
–	–	k?	–
na	na	k7c6	na
rohové	rohový	k2eAgFnSc6d1	rohová
krychličce	krychlička	k1gFnSc6	krychlička
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
tři	tři	k4xCgFnPc4	tři
nálepky	nálepka	k1gFnPc4	nálepka
<g/>
,	,	kIx,	,
na	na	k7c6	na
hranové	hranový	k2eAgFnSc6d1	hranová
dvě	dva	k4xCgNnPc4	dva
a	a	k8xC	a
na	na	k7c6	na
středové	středový	k2eAgFnSc6d1	středová
jedna	jeden	k4xCgFnSc1	jeden
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Celá	celý	k2eAgFnSc1d1	celá
soustava	soustava	k1gFnSc1	soustava
je	být	k5eAaImIp3nS	být
propojena	propojit	k5eAaPmNgFnS	propojit
pohyblivým	pohyblivý	k2eAgInSc7d1	pohyblivý
mechanismem	mechanismus	k1gInSc7	mechanismus
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
kteroukoli	kterýkoli	k3yIgFnSc4	kterýkoli
vrstvu	vrstva	k1gFnSc4	vrstva
pootočit	pootočit	k5eAaPmF	pootočit
o	o	k7c4	o
libovolný	libovolný	k2eAgInSc4d1	libovolný
celočíselný	celočíselný	k2eAgInSc4d1	celočíselný
násobek	násobek	k1gInSc4	násobek
pravého	pravý	k2eAgInSc2d1	pravý
úhlu	úhel	k1gInSc2	úhel
<g/>
.	.	kIx.	.
</s>
<s>
Středy	střed	k1gInPc1	střed
jsou	být	k5eAaImIp3nP	být
jako	jako	k8xC	jako
jediné	jediný	k2eAgFnPc1d1	jediná
z	z	k7c2	z
částí	část	k1gFnPc2	část
nepohyblivé	pohyblivý	k2eNgNnSc1d1	nepohyblivé
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
zaujímají	zaujímat	k5eAaImIp3nP	zaujímat
vůči	vůči	k7c3	vůči
sobě	se	k3xPyFc3	se
stále	stále	k6eAd1	stále
stejnou	stejný	k2eAgFnSc4d1	stejná
polohu	poloha	k1gFnSc4	poloha
<g/>
.	.	kIx.	.
</s>
<s>
Barva	barva	k1gFnSc1	barva
středu	střed	k1gInSc2	střed
tím	ten	k3xDgInSc7	ten
pádem	pád	k1gInSc7	pád
určuje	určovat	k5eAaImIp3nS	určovat
<g/>
,	,	kIx,	,
jaká	jaký	k3yRgFnSc1	jaký
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
výsledná	výsledný	k2eAgFnSc1d1	výsledná
barva	barva	k1gFnSc1	barva
celé	celý	k2eAgFnSc2d1	celá
stěny	stěna	k1gFnSc2	stěna
tento	tento	k3xDgInSc1	tento
střed	střed	k1gInSc1	střed
obsahující	obsahující	k2eAgInSc1d1	obsahující
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
počet	počet	k1gInSc1	počet
kombinací	kombinace	k1gFnPc2	kombinace
(	(	kIx(	(
<g/>
permutací	permutace	k1gFnPc2	permutace
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
kostku	kostka	k1gFnSc4	kostka
3	[number]	k4	3
<g/>
×	×	k?	×
<g/>
3	[number]	k4	3
<g/>
×	×	k?	×
<g/>
3	[number]	k4	3
je	být	k5eAaImIp3nS	být
43	[number]	k4	43
252	[number]	k4	252
003	[number]	k4	003
274	[number]	k4	274
489	[number]	k4	489
856	[number]	k4	856
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Varianty	variant	k1gInPc1	variant
hlavolamu	hlavolam	k1gInSc2	hlavolam
==	==	k?	==
</s>
</p>
<p>
<s>
Princip	princip	k1gInSc1	princip
verze	verze	k1gFnSc1	verze
3	[number]	k4	3
<g/>
×	×	k?	×
<g/>
3	[number]	k4	3
<g/>
×	×	k?	×
<g/>
3	[number]	k4	3
byl	být	k5eAaImAgInS	být
aplikován	aplikovat	k5eAaBmNgInS	aplikovat
na	na	k7c4	na
větší	veliký	k2eAgInPc4d2	veliký
i	i	k8xC	i
menší	malý	k2eAgInPc4d2	menší
rozměry	rozměr	k1gInPc4	rozměr
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
se	se	k3xPyFc4	se
na	na	k7c4	na
trh	trh	k1gInSc4	trh
dostaly	dostat	k5eAaPmAgInP	dostat
typy	typ	k1gInPc1	typ
2	[number]	k4	2
<g/>
×	×	k?	×
<g/>
2	[number]	k4	2
<g/>
×	×	k?	×
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
×	×	k?	×
<g/>
4	[number]	k4	4
<g/>
×	×	k?	×
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
×	×	k?	×
<g/>
5	[number]	k4	5
<g/>
×	×	k?	×
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
×	×	k?	×
<g/>
6	[number]	k4	6
<g/>
×	×	k?	×
<g/>
6	[number]	k4	6
a	a	k8xC	a
7	[number]	k4	7
<g/>
×	×	k?	×
<g/>
7	[number]	k4	7
<g/>
×	×	k?	×
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
opuštění	opuštění	k1gNnSc3	opuštění
krychlového	krychlový	k2eAgNnSc2d1	krychlové
tělesa	těleso	k1gNnSc2	těleso
<g/>
,	,	kIx,	,
což	což	k3yQnSc4	což
demonstrují	demonstrovat	k5eAaBmIp3nP	demonstrovat
např.	např.	kA	např.
hlavolamy	hlavolam	k1gInPc1	hlavolam
Megaminx	Megaminx	k1gInSc1	Megaminx
<g/>
,	,	kIx,	,
Pyraminx	Pyraminx	k1gInSc1	Pyraminx
nebo	nebo	k8xC	nebo
Square-	Square-	k1gFnSc1	Square-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Konečně	konečně	k6eAd1	konečně
existují	existovat	k5eAaImIp3nP	existovat
také	také	k9	také
tzv.	tzv.	kA	tzv.
siamské	siamský	k2eAgFnPc4d1	siamská
kostky	kostka	k1gFnPc4	kostka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
spojením	spojení	k1gNnSc7	spojení
více	hodně	k6eAd2	hodně
jiných	jiný	k2eAgFnPc2d1	jiná
kostek	kostka	k1gFnPc2	kostka
–	–	k?	–
původních	původní	k2eAgMnPc2d1	původní
i	i	k8xC	i
odvozených	odvozený	k2eAgMnPc2d1	odvozený
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Českým	český	k2eAgInSc7d1	český
patentem	patent	k1gInSc7	patent
je	být	k5eAaImIp3nS	být
kostka	kostka	k1gFnSc1	kostka
Square	square	k1gInSc4	square
One	One	k1gFnSc2	One
<g/>
,	,	kIx,	,
alternativně	alternativně	k6eAd1	alternativně
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
označovaná	označovaný	k2eAgFnSc1d1	označovaná
Square	square	k1gInSc4	square
21	[number]	k4	21
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Řešení	řešení	k1gNnSc1	řešení
==	==	k?	==
</s>
</p>
<p>
<s>
Rubikovu	Rubikův	k2eAgFnSc4d1	Rubikova
kostku	kostka	k1gFnSc4	kostka
lze	lze	k6eAd1	lze
řešit	řešit	k5eAaImF	řešit
mnoha	mnoho	k4c2	mnoho
způsoby	způsob	k1gInPc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
mnoho	mnoho	k4c1	mnoho
systematických	systematický	k2eAgFnPc2d1	systematická
metod	metoda	k1gFnPc2	metoda
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
kterým	který	k3yRgMnPc3	který
kostku	kostka	k1gFnSc4	kostka
vyřeší	vyřešit	k5eAaPmIp3nS	vyřešit
i	i	k9	i
laik	laik	k1gMnSc1	laik
<g/>
.	.	kIx.	.
</s>
<s>
Postupy	postup	k1gInPc1	postup
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
především	především	k6eAd1	především
průměrným	průměrný	k2eAgInSc7d1	průměrný
počtem	počet	k1gInSc7	počet
tahů	tah	k1gInPc2	tah
potřebných	potřebný	k2eAgInPc2d1	potřebný
na	na	k7c6	na
složení	složení	k1gNnSc6	složení
<g/>
,	,	kIx,	,
principem	princip	k1gInSc7	princip
a	a	k8xC	a
množstvím	množství	k1gNnSc7	množství
různých	různý	k2eAgInPc2d1	různý
algoritmů	algoritmus	k1gInPc2	algoritmus
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
si	se	k3xPyFc3	se
zapamatovat	zapamatovat	k5eAaPmF	zapamatovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejznámější	známý	k2eAgFnSc1d3	nejznámější
metoda	metoda	k1gFnSc1	metoda
spočívá	spočívat	k5eAaImIp3nS	spočívat
ve	v	k7c4	v
skládání	skládání	k1gNnSc4	skládání
jedné	jeden	k4xCgFnSc2	jeden
vrstvy	vrstva	k1gFnSc2	vrstva
po	po	k7c4	po
druhé	druhý	k4xOgNnSc4	druhý
(	(	kIx(	(
<g/>
Beginner	Beginner	k1gInSc1	Beginner
layer	layra	k1gFnPc2	layra
by	by	kYmCp3nS	by
layer	layer	k1gInSc1	layer
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
známé	známý	k2eAgFnPc1d1	známá
a	a	k8xC	a
používané	používaný	k2eAgFnPc1d1	používaná
metody	metoda	k1gFnPc1	metoda
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
tzv.	tzv.	kA	tzv.
CFOP	CFOP	kA	CFOP
(	(	kIx(	(
<g/>
Jessica	Jessica	k1gMnSc1	Jessica
Fridrich	Fridrich	k1gMnSc1	Fridrich
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
method	methoda	k1gFnPc2	methoda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Lars	Lars	k1gInSc1	Lars
Petrus	Petrus	k1gInSc1	Petrus
method	methoda	k1gFnPc2	methoda
<g/>
,	,	kIx,	,
Zborovski-Bruchem	Zborovski-Bruch	k1gInSc7	Zborovski-Bruch
method	method	k1gInSc1	method
či	či	k8xC	či
corners	corners	k1gInSc1	corners
first	first	k1gFnSc1	first
<g/>
;	;	kIx,	;
jejich	jejich	k3xOp3gInSc7	jejich
společným	společný	k2eAgInSc7d1	společný
jmenovatelem	jmenovatel	k1gInSc7	jmenovatel
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
byly	být	k5eAaImAgFnP	být
vyvinuty	vyvinout	k5eAaPmNgFnP	vyvinout
pro	pro	k7c4	pro
rychlé	rychlý	k2eAgNnSc4d1	rychlé
skládání	skládání	k1gNnSc4	skládání
kostky	kostka	k1gFnSc2	kostka
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
speedcubing	speedcubing	k1gInSc1	speedcubing
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
skládání	skládání	k1gNnSc4	skládání
kostky	kostka	k1gFnSc2	kostka
na	na	k7c4	na
co	co	k3yRnSc4	co
nejmenší	malý	k2eAgInSc1d3	nejmenší
počet	počet	k1gInSc1	počet
tahů	tah	k1gInPc2	tah
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2010	[number]	k4	2010
bylo	být	k5eAaImAgNnS	být
hrubou	hrubý	k2eAgFnSc7d1	hrubá
výpočetní	výpočetní	k2eAgFnSc7d1	výpočetní
silou	síla	k1gFnSc7	síla
(	(	kIx(	(
<g/>
s	s	k7c7	s
různými	různý	k2eAgFnPc7d1	různá
optimalizacemi	optimalizace	k1gFnPc7	optimalizace
<g/>
)	)	kIx)	)
dokázáno	dokázán	k2eAgNnSc1d1	dokázáno
<g/>
,	,	kIx,	,
že	že	k8xS	že
jakoukoliv	jakýkoliv	k3yIgFnSc4	jakýkoliv
kombinaci	kombinace	k1gFnSc4	kombinace
lze	lze	k6eAd1	lze
vyřešit	vyřešit	k5eAaPmF	vyřešit
do	do	k7c2	do
20	[number]	k4	20
tahů	tah	k1gInPc2	tah
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Speedcubing	Speedcubing	k1gInSc4	Speedcubing
==	==	k?	==
</s>
</p>
<p>
<s>
Pojem	pojem	k1gInSc1	pojem
speedcubing	speedcubing	k1gInSc1	speedcubing
označuje	označovat	k5eAaImIp3nS	označovat
systém	systém	k1gInSc1	systém
soutěžení	soutěžení	k1gNnSc2	soutěžení
ve	v	k7c4	v
skládání	skládání	k1gNnSc4	skládání
Rubikovy	Rubikův	k2eAgFnSc2d1	Rubikova
kostky	kostka	k1gFnSc2	kostka
a	a	k8xC	a
příbuzných	příbuzný	k2eAgInPc2d1	příbuzný
hlavolamů	hlavolam	k1gInPc2	hlavolam
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k8xS	jak
název	název	k1gInSc1	název
napovídá	napovídat	k5eAaBmIp3nS	napovídat
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
především	především	k9	především
o	o	k7c4	o
soutěže	soutěž	k1gFnPc4	soutěž
v	v	k7c6	v
co	co	k9	co
nejrychlejším	rychlý	k2eAgNnSc6d3	nejrychlejší
složení	složení	k1gNnSc1	složení
<g/>
,	,	kIx,	,
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
řadě	řada	k1gFnSc6	řada
potom	potom	k6eAd1	potom
o	o	k7c6	o
složení	složení	k1gNnSc6	složení
v	v	k7c6	v
co	co	k9	co
nejmenším	malý	k2eAgInSc6d3	nejmenší
počtu	počet	k1gInSc6	počet
tahů	tah	k1gInPc2	tah
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
populární	populární	k2eAgFnSc1d1	populární
je	být	k5eAaImIp3nS	být
též	též	k9	též
zápolení	zápolení	k1gNnSc4	zápolení
ve	v	k7c4	v
skládání	skládání	k1gNnSc4	skládání
poslepu	poslepu	k6eAd1	poslepu
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
závodí	závodit	k5eAaImIp3nS	závodit
i	i	k9	i
ve	v	k7c6	v
složení	složení	k1gNnSc6	složení
co	co	k9	co
největšího	veliký	k2eAgInSc2d3	veliký
počtu	počet	k1gInSc2	počet
kostek	kostka	k1gFnPc2	kostka
<g/>
,	,	kIx,	,
a	a	k8xC	a
zřídka	zřídka	k6eAd1	zřídka
se	se	k3xPyFc4	se
na	na	k7c6	na
soutěžích	soutěž	k1gFnPc6	soutěž
objevuje	objevovat	k5eAaImIp3nS	objevovat
také	také	k9	také
skládání	skládání	k1gNnSc1	skládání
jen	jen	k6eAd1	jen
jednou	jeden	k4xCgFnSc7	jeden
rukou	ruka	k1gFnSc7	ruka
či	či	k8xC	či
nohama	noha	k1gFnPc7	noha
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
soutěžích	soutěž	k1gFnPc6	soutěž
instrumentů	instrument	k1gInPc2	instrument
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
soutěží	soutěžit	k5eAaImIp3nS	soutěžit
s	s	k7c7	s
kostkami	kostka	k1gFnPc7	kostka
2	[number]	k4	2
<g/>
×	×	k?	×
<g/>
2	[number]	k4	2
<g/>
×	×	k?	×
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
×	×	k?	×
<g/>
3	[number]	k4	3
<g/>
×	×	k?	×
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
×	×	k?	×
<g/>
4	[number]	k4	4
<g/>
×	×	k?	×
<g/>
4	[number]	k4	4
a	a	k8xC	a
5	[number]	k4	5
<g/>
×	×	k?	×
<g/>
5	[number]	k4	5
<g/>
×	×	k?	×
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hraje	hrát	k5eAaImIp3nS	hrát
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
na	na	k7c4	na
rychlost	rychlost	k1gFnSc4	rychlost
<g/>
,	,	kIx,	,
praxe	praxe	k1gFnSc1	praxe
bývá	bývat	k5eAaImIp3nS	bývat
taková	takový	k3xDgFnSc1	takový
<g/>
,	,	kIx,	,
že	že	k8xS	že
každý	každý	k3xTgInSc1	každý
soutěžící	soutěžící	k2eAgInSc1d1	soutěžící
úkol	úkol	k1gInSc1	úkol
splní	splnit	k5eAaPmIp3nS	splnit
pětkrát	pětkrát	k6eAd1	pětkrát
a	a	k8xC	a
jako	jako	k9	jako
jeho	jeho	k3xOp3gInSc1	jeho
výsledek	výsledek	k1gInSc1	výsledek
se	se	k3xPyFc4	se
započítá	započítat	k5eAaPmIp3nS	započítat
aritmetický	aritmetický	k2eAgInSc1d1	aritmetický
průměr	průměr	k1gInSc1	průměr
tří	tři	k4xCgMnPc2	tři
jeho	jeho	k3xOp3gInPc2	jeho
průměrných	průměrný	k2eAgInPc2d1	průměrný
časů	čas	k1gInPc2	čas
(	(	kIx(	(
<g/>
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
a	a	k8xC	a
nejhorší	zlý	k2eAgInSc4d3	Nejhorší
čas	čas	k1gInSc4	čas
se	se	k3xPyFc4	se
ignorují	ignorovat	k5eAaImIp3nP	ignorovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Běžné	běžný	k2eAgInPc1d1	běžný
časy	čas	k1gInPc1	čas
ve	v	k7c6	v
variantě	varianta	k1gFnSc6	varianta
3	[number]	k4	3
<g/>
×	×	k?	×
<g/>
3	[number]	k4	3
<g/>
×	×	k?	×
<g/>
3	[number]	k4	3
klesají	klesat	k5eAaImIp3nP	klesat
pod	pod	k7c7	pod
20	[number]	k4	20
s	s	k7c7	s
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
disciplínu	disciplína	k1gFnSc4	disciplína
činí	činit	k5eAaImIp3nS	činit
divácky	divácky	k6eAd1	divácky
atraktivní	atraktivní	k2eAgMnSc1d1	atraktivní
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
rozšířením	rozšíření	k1gNnSc7	rozšíření
kostky	kostka	k1gFnSc2	kostka
na	na	k7c4	na
4	[number]	k4	4
<g/>
×	×	k?	×
<g/>
4	[number]	k4	4
<g/>
×	×	k?	×
<g/>
4	[number]	k4	4
počet	počet	k1gInSc4	počet
nutných	nutný	k2eAgInPc2d1	nutný
tahů	tah	k1gInPc2	tah
dramaticky	dramaticky	k6eAd1	dramaticky
stoupá	stoupat	k5eAaImIp3nS	stoupat
<g/>
,	,	kIx,	,
a	a	k8xC	a
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
pochopitelně	pochopitelně	k6eAd1	pochopitelně
i	i	k9	i
časy	čas	k1gInPc1	čas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Disciplíny	disciplína	k1gFnPc1	disciplína
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc2	jenž
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
sledování	sledování	k1gNnSc2	sledování
účastník	účastník	k1gMnSc1	účastník
soutěže	soutěž	k1gFnSc2	soutěž
kostku	kostka	k1gFnSc4	kostka
nemůže	moct	k5eNaImIp3nS	moct
pozorovat	pozorovat	k5eAaImF	pozorovat
<g/>
,	,	kIx,	,
mívají	mívat	k5eAaImIp3nP	mívat
to	ten	k3xDgNnSc4	ten
pravidlo	pravidlo	k1gNnSc4	pravidlo
<g/>
,	,	kIx,	,
že	že	k8xS	že
čas	čas	k1gInSc1	čas
se	se	k3xPyFc4	se
spouští	spouštět	k5eAaImIp3nS	spouštět
v	v	k7c6	v
momentě	moment	k1gInSc6	moment
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
si	se	k3xPyFc3	se
soutěžící	soutěžící	k2eAgFnSc4d1	soutěžící
kostku	kostka	k1gFnSc4	kostka
začne	začít	k5eAaPmIp3nS	začít
prohlížet	prohlížet	k5eAaImF	prohlížet
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
normální	normální	k2eAgFnSc2d1	normální
soutěže	soutěž	k1gFnSc2	soutěž
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
čas	čas	k1gInSc4	čas
přidělený	přidělený	k2eAgInSc4d1	přidělený
na	na	k7c4	na
prohlédnutí	prohlédnutí	k1gNnSc4	prohlédnutí
kostky	kostka	k1gFnSc2	kostka
před	před	k7c7	před
zahájením	zahájení	k1gNnSc7	zahájení
skládání	skládání	k1gNnSc2	skládání
je	být	k5eAaImIp3nS	být
konstantní	konstantní	k2eAgFnSc1d1	konstantní
<g/>
,	,	kIx,	,
a	a	k8xC	a
sice	sice	k8xC	sice
15	[number]	k4	15
s.	s.	k?	s.
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgInPc4d1	oficiální
světové	světový	k2eAgInPc4d1	světový
rekordy	rekord	k1gInPc4	rekord
ve	v	k7c6	v
složení	složení	k1gNnSc6	složení
jedné	jeden	k4xCgFnSc2	jeden
kostky	kostka	k1gFnSc2	kostka
ke	k	k7c3	k
dni	den	k1gInSc3	den
28	[number]	k4	28
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2018	[number]	k4	2018
</s>
</p>
<p>
<s>
===	===	k?	===
Metody	metoda	k1gFnPc1	metoda
skládání	skládání	k1gNnSc2	skládání
ve	v	k7c6	v
speedcubingu	speedcubing	k1gInSc6	speedcubing
===	===	k?	===
</s>
</p>
<p>
<s>
Nejznámější	známý	k2eAgFnPc4d3	nejznámější
klasické	klasický	k2eAgFnPc4d1	klasická
metody	metoda	k1gFnPc4	metoda
skládání	skládání	k1gNnSc2	skládání
kostky	kostka	k1gFnSc2	kostka
nejsou	být	k5eNaImIp3nP	být
pro	pro	k7c4	pro
speedcubing	speedcubing	k1gInSc4	speedcubing
příliš	příliš	k6eAd1	příliš
vhodné	vhodný	k2eAgNnSc4d1	vhodné
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
na	na	k7c6	na
složení	složení	k1gNnSc6	složení
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
poměrně	poměrně	k6eAd1	poměrně
vysoký	vysoký	k2eAgInSc1d1	vysoký
počet	počet	k1gInSc1	počet
tahů	tah	k1gInPc2	tah
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
proto	proto	k8xC	proto
vynalezeny	vynalezen	k2eAgFnPc1d1	vynalezena
jiné	jiný	k2eAgFnPc1d1	jiná
metody	metoda	k1gFnPc1	metoda
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
počet	počet	k1gInSc1	počet
tahů	tah	k1gInPc2	tah
redukují	redukovat	k5eAaBmIp3nP	redukovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
složení	složení	k1gNnSc4	složení
zrychlují	zrychlovat	k5eAaImIp3nP	zrychlovat
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
důležitým	důležitý	k2eAgInSc7d1	důležitý
faktorem	faktor	k1gInSc7	faktor
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
potřebné	potřebný	k2eAgInPc1d1	potřebný
tahy	tah	k1gInPc1	tah
byly	být	k5eAaImAgInP	být
na	na	k7c6	na
kostce	kostka	k1gFnSc6	kostka
rychle	rychle	k6eAd1	rychle
vidět	vidět	k5eAaImF	vidět
<g/>
,	,	kIx,	,
a	a	k8xC	a
snížil	snížit	k5eAaPmAgInS	snížit
se	se	k3xPyFc4	se
tak	tak	k9	tak
čas	čas	k1gInSc1	čas
potřebný	potřebný	k2eAgInSc1d1	potřebný
na	na	k7c4	na
promyšlení	promyšlení	k1gNnSc4	promyšlení
dalšího	další	k2eAgInSc2d1	další
kroku	krok	k1gInSc2	krok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Naprostá	naprostý	k2eAgFnSc1d1	naprostá
většina	většina	k1gFnSc1	většina
metod	metoda	k1gFnPc2	metoda
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
určitým	určitý	k2eAgInSc7d1	určitý
způsobem	způsob	k1gInSc7	způsob
složit	složit	k5eAaPmF	složit
první	první	k4xOgFnPc4	první
dvě	dva	k4xCgFnPc4	dva
vrstvy	vrstva	k1gFnPc4	vrstva
kostky	kostka	k1gFnSc2	kostka
<g/>
.	.	kIx.	.
</s>
<s>
Složením	složení	k1gNnSc7	složení
třetí	třetí	k4xOgFnSc2	třetí
vrstvy	vrstva	k1gFnSc2	vrstva
se	se	k3xPyFc4	se
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
metody	metoda	k1gFnPc1	metoda
už	už	k6eAd1	už
poté	poté	k6eAd1	poté
neliší	lišit	k5eNaImIp3nP	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Výjimku	výjimka	k1gFnSc4	výjimka
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
tvoří	tvořit	k5eAaImIp3nS	tvořit
např.	např.	kA	např.
metoda	metoda	k1gFnSc1	metoda
corners	corners	k1gInSc1	corners
first	first	k1gFnSc1	first
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
nejdříve	dříve	k6eAd3	dříve
rohy	roh	k1gInPc1	roh
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
tzv.	tzv.	kA	tzv.
roux	roux	k1gInSc1	roux
method	method	k1gInSc1	method
<g/>
.	.	kIx.	.
<g/>
Nejpoužívanější	používaný	k2eAgFnPc1d3	nejpoužívanější
metody	metoda	k1gFnPc1	metoda
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
CFOP	CFOP	kA	CFOP
(	(	kIx(	(
<g/>
Jessica	Jessica	k1gMnSc1	Jessica
Fridrich	Fridrich	k1gMnSc1	Fridrich
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
method	methoda	k1gFnPc2	methoda
<g/>
)	)	kIx)	)
–	–	k?	–
spočívá	spočívat	k5eAaImIp3nS	spočívat
ve	v	k7c6	v
vytvoření	vytvoření	k1gNnSc6	vytvoření
tzv.	tzv.	kA	tzv.
kříže	kříž	k1gInPc1	kříž
(	(	kIx(	(
<g/>
čtyř	čtyři	k4xCgFnPc2	čtyři
správně	správně	k6eAd1	správně
umístěných	umístěný	k2eAgFnPc2d1	umístěná
a	a	k8xC	a
natočených	natočený	k2eAgFnPc2d1	natočená
hran	hrana	k1gFnPc2	hrana
jedné	jeden	k4xCgFnSc2	jeden
vrstvy	vrstva	k1gFnSc2	vrstva
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
po	po	k7c6	po
jehož	jehož	k3xOyRp3gInSc6	jehož
sestavení	sestavení	k1gNnSc6	sestavení
se	se	k3xPyFc4	se
společně	společně	k6eAd1	společně
umísťuje	umísťovat	k5eAaImIp3nS	umísťovat
jak	jak	k6eAd1	jak
roh	roh	k1gInSc4	roh
této	tento	k3xDgFnSc2	tento
vrstvy	vrstva	k1gFnSc2	vrstva
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
jemu	on	k3xPp3gInSc3	on
příslušné	příslušný	k2eAgFnPc4d1	příslušná
hrany	hrana	k1gFnPc4	hrana
ze	z	k7c2	z
střední	střední	k2eAgFnSc2d1	střední
vrstvy	vrstva	k1gFnSc2	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
se	se	k3xPyFc4	se
složí	složit	k5eAaPmIp3nP	složit
první	první	k4xOgFnPc1	první
dvě	dva	k4xCgFnPc1	dva
řady	řada	k1gFnPc1	řada
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgNnSc1d1	poslední
patro	patro	k1gNnSc1	patro
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
zpravidla	zpravidla	k6eAd1	zpravidla
dvěma	dva	k4xCgInPc7	dva
kroky	krok	k1gInPc7	krok
<g/>
,	,	kIx,	,
orientováním	orientování	k1gNnSc7	orientování
poslední	poslední	k2eAgFnSc2d1	poslední
vrstvy	vrstva	k1gFnSc2	vrstva
(	(	kIx(	(
<g/>
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
barva	barva	k1gFnSc1	barva
<g/>
,	,	kIx,	,
kostky	kostka	k1gFnPc1	kostka
jsou	být	k5eAaImIp3nP	být
proházeny	proházen	k2eAgFnPc1d1	proházen
<g/>
)	)	kIx)	)
a	a	k8xC	a
následným	následný	k2eAgNnSc7d1	následné
proházením	proházení	k1gNnSc7	proházení
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
složí	složit	k5eAaPmIp3nS	složit
zbytek	zbytek	k1gInSc1	zbytek
kostky	kostka	k1gFnSc2	kostka
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
metoda	metoda	k1gFnSc1	metoda
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
mezi	mezi	k7c7	mezi
speedcubery	speedcuber	k1gMnPc7	speedcuber
nejpoužívanější	používaný	k2eAgNnSc1d3	nejpoužívanější
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zborowski-Bruchem	Zborowski-Bruch	k1gInSc7	Zborowski-Bruch
method	methoda	k1gFnPc2	methoda
(	(	kIx(	(
<g/>
ZB	ZB	kA	ZB
method	method	k1gInSc1	method
<g/>
)	)	kIx)	)
–	–	k?	–
obdoba	obdoba	k1gFnSc1	obdoba
metody	metoda	k1gFnSc2	metoda
Fridrich	Fridrich	k1gMnSc1	Fridrich
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
při	při	k7c6	při
umístění	umístění	k1gNnSc6	umístění
posledního	poslední	k2eAgInSc2d1	poslední
rohu	roh	k1gInSc2	roh
a	a	k8xC	a
hrany	hrana	k1gFnSc2	hrana
prvních	první	k4xOgFnPc2	první
dvou	dva	k4xCgFnPc2	dva
vrstev	vrstva	k1gFnPc2	vrstva
správně	správně	k6eAd1	správně
natočí	natočit	k5eAaBmIp3nS	natočit
hrany	hrana	k1gFnPc4	hrana
poslední	poslední	k2eAgFnSc2d1	poslední
vrstvy	vrstva	k1gFnSc2	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Metoda	metoda	k1gFnSc1	metoda
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
těžká	těžký	k2eAgFnSc1d1	těžká
na	na	k7c6	na
učení	učení	k1gNnSc6	učení
<g/>
;	;	kIx,	;
na	na	k7c6	na
zvládnutí	zvládnutí	k1gNnSc6	zvládnutí
celé	celý	k2eAgFnSc2d1	celá
metody	metoda	k1gFnSc2	metoda
je	být	k5eAaImIp3nS	být
totiž	totiž	k9	totiž
potřeba	potřeba	k1gFnSc1	potřeba
zapamatovat	zapamatovat	k5eAaPmF	zapamatovat
si	se	k3xPyFc3	se
téměř	téměř	k6eAd1	téměř
800	[number]	k4	800
různých	různý	k2eAgInPc2d1	různý
algoritmů	algoritmus	k1gInPc2	algoritmus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zbigniew	Zbigniew	k?	Zbigniew
Zborowski	Zborowski	k1gNnPc2	Zborowski
method	method	k1gInSc1	method
(	(	kIx(	(
<g/>
ZZ	ZZ	kA	ZZ
method	method	k1gInSc1	method
<g/>
)	)	kIx)	)
–	–	k?	–
v	v	k7c6	v
první	první	k4xOgFnSc6	první
fázi	fáze	k1gFnSc6	fáze
se	se	k3xPyFc4	se
všechny	všechen	k3xTgFnPc1	všechen
hrany	hrana	k1gFnPc1	hrana
správně	správně	k6eAd1	správně
orientují	orientovat	k5eAaBmIp3nP	orientovat
a	a	k8xC	a
vytvoří	vytvořit	k5eAaPmIp3nP	vytvořit
se	se	k3xPyFc4	se
na	na	k7c6	na
spodní	spodní	k2eAgFnSc6d1	spodní
straně	strana	k1gFnSc6	strana
linie	linie	k1gFnSc2	linie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
zbytek	zbytek	k1gInSc4	zbytek
složení	složení	k1gNnSc2	složení
stačí	stačit	k5eAaBmIp3nS	stačit
pohyby	pohyb	k1gInPc4	pohyb
pravé	pravá	k1gFnSc2	pravá
<g/>
,	,	kIx,	,
levé	levý	k2eAgFnSc2d1	levá
a	a	k8xC	a
vrchní	vrchní	k2eAgFnSc2d1	vrchní
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
krok	krok	k1gInSc1	krok
je	být	k5eAaImIp3nS	být
stejný	stejný	k2eAgInSc1d1	stejný
jako	jako	k8xS	jako
u	u	k7c2	u
metody	metoda	k1gFnSc2	metoda
Fridrich	Fridrich	k1gMnSc1	Fridrich
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
na	na	k7c6	na
konci	konec	k1gInSc6	konec
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
různit	různit	k5eAaImF	různit
podle	podle	k7c2	podle
spousty	spousta	k1gFnSc2	spousta
alternativ	alternativa	k1gFnPc2	alternativa
pro	pro	k7c4	pro
poslední	poslední	k2eAgNnSc4d1	poslední
patro	patro	k1gNnSc4	patro
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lars	Lars	k6eAd1	Lars
Petrus	Petrus	k1gInSc1	Petrus
method	method	k1gInSc1	method
–	–	k?	–
spočívá	spočívat	k5eAaImIp3nS	spočívat
ve	v	k7c6	v
vytvoření	vytvoření	k1gNnSc6	vytvoření
tzv.	tzv.	kA	tzv.
pracovního	pracovní	k2eAgInSc2d1	pracovní
rohu	roh	k1gInSc2	roh
(	(	kIx(	(
<g/>
bloku	blok	k1gInSc2	blok
2	[number]	k4	2
<g/>
×	×	k?	×
<g/>
2	[number]	k4	2
<g/>
×	×	k?	×
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
dalším	další	k2eAgInSc6d1	další
kroku	krok	k1gInSc6	krok
rozšíří	rozšířit	k5eAaPmIp3nS	rozšířit
na	na	k7c4	na
2	[number]	k4	2
<g/>
×	×	k?	×
<g/>
3	[number]	k4	3
<g/>
×	×	k?	×
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
zorientují	zorientovat	k5eAaPmIp3nP	zorientovat
zbylé	zbylý	k2eAgInPc1d1	zbylý
hrany	hrana	k1gFnPc4	hrana
a	a	k8xC	a
následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
rozšíří	rozšířit	k5eAaPmIp3nS	rozšířit
na	na	k7c4	na
3	[number]	k4	3
<g/>
×	×	k?	×
<g/>
3	[number]	k4	3
<g/>
×	×	k?	×
<g/>
2	[number]	k4	2
(	(	kIx(	(
<g/>
tzn.	tzn.	kA	tzn.
dosložení	dosložení	k1gNnSc1	dosložení
do	do	k7c2	do
prvních	první	k4xOgFnPc2	první
dvou	dva	k4xCgFnPc2	dva
vrstev	vrstva	k1gFnPc2	vrstva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výhodou	výhoda	k1gFnSc7	výhoda
této	tento	k3xDgFnSc2	tento
metody	metoda	k1gFnSc2	metoda
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
ze	z	k7c2	z
všech	všecek	k3xTgMnPc2	všecek
zmíněných	zmíněný	k2eAgMnPc2d1	zmíněný
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
na	na	k7c4	na
složení	složení	k1gNnSc4	složení
nejméně	málo	k6eAd3	málo
tahů	tah	k1gInPc2	tah
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
u	u	k7c2	u
ní	on	k3xPp3gFnSc2	on
ovšem	ovšem	k9	ovšem
větší	veliký	k2eAgInSc1d2	veliký
problém	problém	k1gInSc1	problém
kroky	krok	k1gInPc1	krok
vidět	vidět	k5eAaImF	vidět
dopředu	dopředu	k6eAd1	dopředu
a	a	k8xC	a
neztrácet	ztrácet	k5eNaImF	ztrácet
tolik	tolik	k6eAd1	tolik
času	čas	k1gInSc2	čas
prohlížením	prohlížení	k1gNnSc7	prohlížení
kostky	kostka	k1gFnSc2	kostka
a	a	k8xC	a
plánováním	plánování	k1gNnSc7	plánování
dalšího	další	k2eAgInSc2d1	další
kroku	krok	k1gInSc2	krok
<g/>
.	.	kIx.	.
</s>
<s>
Metoda	metoda	k1gFnSc1	metoda
je	být	k5eAaImIp3nS	být
vhodná	vhodný	k2eAgFnSc1d1	vhodná
pro	pro	k7c4	pro
disciplíny	disciplína	k1gFnPc4	disciplína
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
je	být	k5eAaImIp3nS	být
cílem	cíl	k1gInSc7	cíl
složit	složit	k5eAaPmF	složit
kostku	kostka	k1gFnSc4	kostka
v	v	k7c6	v
co	co	k9	co
nejmenším	malý	k2eAgInSc6d3	nejmenší
počtu	počet	k1gInSc6	počet
tahů	tah	k1gInPc2	tah
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Corners	Corners	k1gInSc1	Corners
first	first	k1gInSc1	first
(	(	kIx(	(
<g/>
Ortega	Ortega	k1gFnSc1	Ortega
<g/>
)	)	kIx)	)
–	–	k?	–
nejdříve	dříve	k6eAd3	dříve
se	se	k3xPyFc4	se
složí	složit	k5eAaPmIp3nS	složit
všech	všecek	k3xTgInPc2	všecek
8	[number]	k4	8
rohů	roh	k1gInPc2	roh
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
hrany	hrana	k1gFnPc1	hrana
nejprve	nejprve	k6eAd1	nejprve
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
vrstvě	vrstva	k1gFnSc6	vrstva
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
v	v	k7c6	v
protější	protější	k2eAgFnSc6d1	protější
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
jedním	jeden	k4xCgInSc7	jeden
algoritmem	algoritmus	k1gInSc7	algoritmus
umístí	umístit	k5eAaPmIp3nS	umístit
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
správně	správně	k6eAd1	správně
otočí	otočit	k5eAaPmIp3nP	otočit
hrany	hrana	k1gFnPc4	hrana
ve	v	k7c6	v
vrstvě	vrstva	k1gFnSc6	vrstva
střední	střední	k2eAgFnSc6d1	střední
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
metoda	metoda	k1gFnSc1	metoda
tedy	tedy	k9	tedy
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
výše	vysoce	k6eAd2	vysoce
zmíněných	zmíněný	k2eAgInPc2d1	zmíněný
neprochází	procházet	k5eNaImIp3nS	procházet
stavem	stav	k1gInSc7	stav
složení	složení	k1gNnSc1	složení
dvou	dva	k4xCgFnPc2	dva
řad	řada	k1gFnPc2	řada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roux	Roux	k1gInSc1	Roux
–	–	k?	–
metoda	metoda	k1gFnSc1	metoda
založená	založený	k2eAgFnSc1d1	založená
na	na	k7c6	na
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
blockbuildingu	blockbuilding	k1gInSc2	blockbuilding
<g/>
"	"	kIx"	"
–	–	k?	–
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
kroku	krok	k1gInSc6	krok
se	se	k3xPyFc4	se
libovolným	libovolný	k2eAgInSc7d1	libovolný
způsobem	způsob	k1gInSc7	způsob
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
blok	blok	k1gInSc1	blok
1	[number]	k4	1
<g/>
×	×	k?	×
<g/>
2	[number]	k4	2
<g/>
×	×	k?	×
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
na	na	k7c6	na
levé	levý	k2eAgFnSc6d1	levá
straně	strana	k1gFnSc6	strana
kostky	kostka	k1gFnSc2	kostka
<g/>
,	,	kIx,	,
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
kroku	krok	k1gInSc6	krok
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
třeba	třeba	k6eAd1	třeba
vytvořit	vytvořit	k5eAaPmF	vytvořit
stejný	stejný	k2eAgInSc4d1	stejný
blok	blok	k1gInSc4	blok
na	na	k7c6	na
protější	protější	k2eAgFnSc6d1	protější
straně	strana	k1gFnSc6	strana
(	(	kIx(	(
<g/>
při	při	k7c6	při
zachování	zachování	k1gNnSc6	zachování
prvního	první	k4xOgMnSc2	první
bloku	blok	k1gInSc2	blok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Následuje	následovat	k5eAaImIp3nS	následovat
orientace	orientace	k1gFnSc1	orientace
a	a	k8xC	a
permutace	permutace	k1gFnPc1	permutace
rohů	roh	k1gInPc2	roh
horní	horní	k2eAgFnPc1d1	horní
vrstvy	vrstva	k1gFnPc1	vrstva
pomocí	pomocí	k7c2	pomocí
naučených	naučený	k2eAgInPc2d1	naučený
algoritmů	algoritmus	k1gInPc2	algoritmus
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc1d1	poslední
krok	krok	k1gInSc1	krok
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
zbylých	zbylý	k2eAgFnPc2d1	zbylá
6	[number]	k4	6
hran	hrana	k1gFnPc2	hrana
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
do	do	k7c2	do
tří	tři	k4xCgFnPc2	tři
částí	část	k1gFnPc2	část
(	(	kIx(	(
<g/>
orientace	orientace	k1gFnSc1	orientace
hran	hrana	k1gFnPc2	hrana
<g/>
,	,	kIx,	,
složení	složení	k1gNnSc2	složení
2	[number]	k4	2
bočních	boční	k2eAgFnPc2d1	boční
hran	hrana	k1gFnPc2	hrana
<g/>
,	,	kIx,	,
permutace	permutace	k1gFnSc2	permutace
posledních	poslední	k2eAgFnPc2d1	poslední
4	[number]	k4	4
hran	hrana	k1gFnPc2	hrana
<g/>
)	)	kIx)	)
–	–	k?	–
tento	tento	k3xDgInSc4	tento
krok	krok	k1gInSc4	krok
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
těžký	těžký	k2eAgInSc1d1	těžký
na	na	k7c4	na
pochopení	pochopení	k1gNnSc4	pochopení
<g/>
,	,	kIx,	,
zato	zato	k6eAd1	zato
však	však	k9	však
velmi	velmi	k6eAd1	velmi
efektivní	efektivní	k2eAgMnSc1d1	efektivní
(	(	kIx(	(
<g/>
používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
tahy	tah	k1gInPc7	tah
horní	horní	k2eAgFnSc2d1	horní
a	a	k8xC	a
prostřední	prostřední	k2eAgFnSc2d1	prostřední
vrstvou	vrstva	k1gFnSc7	vrstva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
metoda	metoda	k1gFnSc1	metoda
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
méně	málo	k6eAd2	málo
tahů	tah	k1gInPc2	tah
na	na	k7c4	na
jedno	jeden	k4xCgNnSc4	jeden
složení	složení	k1gNnSc4	složení
než	než	k8xS	než
ostatní	ostatní	k2eAgFnPc4d1	ostatní
metody	metoda	k1gFnPc4	metoda
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
hodně	hodně	k6eAd1	hodně
přemýšlení	přemýšlení	k1gNnSc4	přemýšlení
dopředu	dopředu	k6eAd1	dopředu
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
její	její	k3xOp3gFnSc4	její
celkovou	celkový	k2eAgFnSc4d1	celková
rychlost	rychlost	k1gFnSc4	rychlost
snižuje	snižovat	k5eAaImIp3nS	snižovat
<g/>
.	.	kIx.	.
<g/>
K	k	k7c3	k
tak	tak	k9	tak
rychlému	rychlý	k2eAgNnSc3d1	rychlé
složení	složení	k1gNnSc3	složení
<g/>
,	,	kIx,	,
od	od	k7c2	od
jakého	jaký	k3yRgNnSc2	jaký
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
slibovat	slibovat	k5eAaImF	slibovat
si	se	k3xPyFc3	se
úspěch	úspěch	k1gInSc4	úspěch
v	v	k7c6	v
soutěžích	soutěž	k1gFnPc6	soutěž
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vedle	vedle	k7c2	vedle
velké	velký	k2eAgFnSc2d1	velká
dávky	dávka	k1gFnSc2	dávka
motorického	motorický	k2eAgNnSc2d1	motorické
nadání	nadání	k1gNnSc2	nadání
pro	pro	k7c4	pro
provádění	provádění	k1gNnSc4	provádění
správných	správný	k2eAgInPc2d1	správný
kroků	krok	k1gInPc2	krok
ve	v	k7c6	v
vysoké	vysoký	k2eAgFnSc6d1	vysoká
rychlosti	rychlost	k1gFnSc6	rychlost
nezbytné	zbytný	k2eNgFnPc1d1	zbytný
se	se	k3xPyFc4	se
naučit	naučit	k5eAaPmF	naučit
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
různých	různý	k2eAgInPc2d1	různý
algoritmů	algoritmus	k1gInPc2	algoritmus
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
složení	složení	k1gNnSc4	složení
poslední	poslední	k2eAgFnSc2d1	poslední
vrstvy	vrstva	k1gFnSc2	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
metody	metoda	k1gFnSc2	metoda
Fridrich	Fridrich	k1gMnSc1	Fridrich
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
mezi	mezi	k7c4	mezi
speedcubery	speedcuber	k1gMnPc4	speedcuber
nejvíce	nejvíce	k6eAd1	nejvíce
oblíbená	oblíbený	k2eAgFnSc1d1	oblíbená
a	a	k8xC	a
uznávaná	uznávaný	k2eAgFnSc1d1	uznávaná
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
78	[number]	k4	78
<g/>
,	,	kIx,	,
metoda	metoda	k1gFnSc1	metoda
Roux	Roux	k1gInSc1	Roux
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
ohledu	ohled	k1gInSc6	ohled
znamená	znamenat	k5eAaImIp3nS	znamenat
počet	počet	k1gInSc1	počet
42	[number]	k4	42
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
počet	počet	k1gInSc1	počet
tahů	tah	k1gInPc2	tah
napříč	napříč	k7c7	napříč
těmito	tento	k3xDgInPc7	tento
algoritmy	algoritmus	k1gInPc7	algoritmus
je	být	k5eAaImIp3nS	být
cca	cca	kA	cca
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Všech	všecek	k3xTgInPc2	všecek
algoritmů	algoritmus	k1gInPc2	algoritmus
potřebných	potřebný	k2eAgInPc2d1	potřebný
pro	pro	k7c4	pro
složení	složení	k1gNnSc4	složení
poslední	poslední	k2eAgFnSc2d1	poslední
vrstvy	vrstva	k1gFnSc2	vrstva
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
kroku	krok	k1gInSc6	krok
je	být	k5eAaImIp3nS	být
1	[number]	k4	1
211	[number]	k4	211
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
možnostem	možnost	k1gFnPc3	možnost
lidské	lidský	k2eAgFnSc2d1	lidská
paměti	paměť	k1gFnSc2	paměť
příliš	příliš	k6eAd1	příliš
velké	velký	k2eAgNnSc4d1	velké
číslo	číslo	k1gNnSc4	číslo
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
na	na	k7c6	na
špičkové	špičkový	k2eAgFnSc6d1	špičková
úrovni	úroveň	k1gFnSc6	úroveň
v	v	k7c6	v
metodách	metoda	k1gFnPc6	metoda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
už	už	k6eAd1	už
jsou	být	k5eAaImIp3nP	být
hrany	hrana	k1gFnPc4	hrana
horní	horní	k2eAgFnPc4d1	horní
vrstvy	vrstva	k1gFnPc4	vrstva
správně	správně	k6eAd1	správně
natočeny	natočit	k5eAaBmNgFnP	natočit
z	z	k7c2	z
kroků	krok	k1gInPc2	krok
předtím	předtím	k6eAd1	předtím
jen	jen	k9	jen
jedním	jeden	k4xCgInSc7	jeden
krokem	krok	k1gInSc7	krok
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgFnSc1d1	poslední
vrstva	vrstva	k1gFnSc1	vrstva
řeší	řešit	k5eAaImIp3nS	řešit
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
krocích	krok	k1gInPc6	krok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
rychlé	rychlý	k2eAgNnSc4d1	rychlé
skládání	skládání	k1gNnSc4	skládání
poslepu	poslepu	k6eAd1	poslepu
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
úplně	úplně	k6eAd1	úplně
jiné	jiný	k2eAgFnPc1d1	jiná
metody	metoda	k1gFnPc1	metoda
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgInPc6	který
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
algoritmy	algoritmus	k1gInPc1	algoritmus
přemísťující	přemísťující	k2eAgInPc1d1	přemísťující
jen	jen	k9	jen
několik	několik	k4yIc1	několik
málo	málo	k4c1	málo
kostek	kostka	k1gFnPc2	kostka
na	na	k7c4	na
správná	správný	k2eAgNnPc4d1	správné
místa	místo	k1gNnPc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Důležitým	důležitý	k2eAgInSc7d1	důležitý
prvkem	prvek	k1gInSc7	prvek
pro	pro	k7c4	pro
rychlé	rychlý	k2eAgNnSc4d1	rychlé
skládání	skládání	k1gNnSc4	skládání
poslepu	poslepu	k6eAd1	poslepu
je	být	k5eAaImIp3nS	být
především	především	k9	především
schopnost	schopnost	k1gFnSc1	schopnost
rychle	rychle	k6eAd1	rychle
si	se	k3xPyFc3	se
zapamatovat	zapamatovat	k5eAaPmF	zapamatovat
počáteční	počáteční	k2eAgNnSc4d1	počáteční
rozložení	rozložení	k1gNnSc4	rozložení
kostky	kostka	k1gFnSc2	kostka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
V	v	k7c6	v
populární	populární	k2eAgFnSc6d1	populární
kultuře	kultura	k1gFnSc6	kultura
==	==	k?	==
</s>
</p>
<p>
<s>
Rubikova	Rubikův	k2eAgFnSc1d1	Rubikova
kostka	kostka	k1gFnSc1	kostka
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
filmech	film	k1gInPc6	film
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
3	[number]	k4	3
Idiots	Idiots	k1gInSc1	Idiots
</s>
</p>
<p>
<s>
Armageddon	Armageddon	k1gMnSc1	Armageddon
</s>
</p>
<p>
<s>
Brick	Brick	k6eAd1	Brick
</s>
</p>
<p>
<s>
Dude	Dude	k6eAd1	Dude
</s>
</p>
<p>
<s>
Holiday	Holidaa	k1gFnPc1	Holidaa
<g/>
:	:	kIx,	:
A	a	k9	a
Soldier	Soldier	k1gMnSc1	Soldier
Is	Is	k1gMnSc1	Is
Never	Never	k1gMnSc1	Never
Off	Off	k1gMnSc1	Off
Duty	Duty	k?	Duty
</s>
</p>
<p>
<s>
Chameleon	chameleon	k1gMnSc1	chameleon
Street	Street	k1gMnSc1	Street
</s>
</p>
<p>
<s>
Karthik	Karthik	k1gInSc1	Karthik
Calling	Calling	k1gInSc1	Calling
Karthik	Karthika	k1gFnPc2	Karthika
</s>
</p>
<p>
<s>
Let	let	k1gInSc1	let
Me	Me	k1gMnSc2	Me
In	In	k1gMnSc2	In
</s>
</p>
<p>
<s>
Let	léto	k1gNnPc2	léto
the	the	k?	the
Right	Right	k1gMnSc1	Right
One	One	k1gMnSc1	One
In	In	k1gMnSc1	In
</s>
</p>
<p>
<s>
My	my	k3xPp1nPc1	my
Name	Name	k1gNnPc7	Name
is	is	k?	is
Khan	Khan	k1gMnSc1	Khan
</s>
</p>
<p>
<s>
Nói	Nói	k?	Nói
the	the	k?	the
Albino	Albino	k1gNnSc4	Albino
</s>
</p>
<p>
<s>
Snowden	Snowdna	k1gFnPc2	Snowdna
</s>
</p>
<p>
<s>
The	The	k?	The
Pursuit	Pursuit	k1gInSc1	Pursuit
of	of	k?	of
Happyness	Happyness	k1gInSc1	Happyness
</s>
</p>
<p>
<s>
There	Ther	k1gMnSc5	Ther
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Something	Something	k1gInSc1	Something
About	About	k1gMnSc1	About
Mary	Mary	k1gFnSc1	Mary
</s>
</p>
<p>
<s>
WALL-E	WALL-E	k?	WALL-E
</s>
</p>
<p>
<s>
Where	Wher	k1gMnSc5	Wher
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
My	my	k3xPp1nPc1	my
Car	car	k1gMnSc1	car
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
Kostka	kostka	k1gFnSc1	kostka
sehrála	sehrát	k5eAaPmAgFnS	sehrát
roli	role	k1gFnSc4	role
rovněž	rovněž	k9	rovněž
v	v	k7c6	v
několika	několik	k4yIc6	několik
seriálech	seriál	k1gInPc6	seriál
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Doctor	Doctor	k1gMnSc1	Doctor
Who	Who	k1gMnSc1	Who
</s>
</p>
<p>
<s>
Everybody	Everyboda	k1gFnPc1	Everyboda
Hates	Hatesa	k1gFnPc2	Hatesa
Chris	Chris	k1gFnSc2	Chris
</s>
</p>
<p>
<s>
Seinfeld	Seinfeld	k6eAd1	Seinfeld
</s>
</p>
<p>
<s>
The	The	k?	The
Carrie	Carrie	k1gFnSc1	Carrie
Diaries	Diariesa	k1gFnPc2	Diariesa
</s>
</p>
<p>
<s>
The	The	k?	The
Fresh	Fresh	k1gInSc1	Fresh
Prince	princ	k1gMnSc2	princ
of	of	k?	of
Bel-Air	Bel-Air	k1gMnSc1	Bel-Air
</s>
</p>
<p>
<s>
The	The	k?	The
Simpsons	Simpsons	k1gInSc1	Simpsons
</s>
</p>
<p>
<s>
The	The	k?	The
Big	Big	k1gMnSc1	Big
Bang	Bang	k1gMnSc1	Bang
TheoryVe	TheoryVe	k1gInSc4	TheoryVe
filmech	film	k1gInPc6	film
a	a	k8xC	a
seriálech	seriál	k1gInPc6	seriál
se	se	k3xPyFc4	se
kostka	kostka	k1gFnSc1	kostka
typicky	typicky	k6eAd1	typicky
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
kontextu	kontext	k1gInSc6	kontext
spojujícím	spojující	k2eAgInSc6d1	spojující
inteligenci	inteligence	k1gFnSc4	inteligence
určité	určitý	k2eAgFnPc4d1	určitá
postavy	postava	k1gFnPc4	postava
s	s	k7c7	s
její	její	k3xOp3gFnSc7	její
schopností	schopnost	k1gFnPc2	schopnost
kostku	kostka	k1gFnSc4	kostka
rychle	rychle	k6eAd1	rychle
složit	složit	k5eAaPmF	složit
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
frustrací	frustrace	k1gFnPc2	frustrace
z	z	k7c2	z
neschopnosti	neschopnost	k1gFnSc2	neschopnost
ji	on	k3xPp3gFnSc4	on
složit	složit	k5eAaPmF	složit
(	(	kIx(	(
<g/>
And	Anda	k1gFnPc2	Anda
The	The	k1gFnSc2	The
Band	banda	k1gFnPc2	banda
Played	Played	k1gMnSc1	Played
On	on	k3xPp3gMnSc1	on
<g/>
,	,	kIx,	,
Being	Being	k1gMnSc1	Being
John	John	k1gMnSc1	John
Malkovich	Malkovich	k1gMnSc1	Malkovich
<g/>
,	,	kIx,	,
Hellboy	Hellboy	k1gInPc1	Hellboy
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Wedding	Wedding	k1gInSc1	Wedding
Singer	Singer	k1gInSc1	Singer
<g/>
,	,	kIx,	,
UHF	UHF	kA	UHF
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
nás	my	k3xPp1nPc2	my
na	na	k7c6	na
stejné	stejná	k1gFnSc6	stejná
téma	téma	k1gNnSc1	téma
-	-	kIx~	-
frustrace	frustrace	k1gFnSc1	frustrace
z	z	k7c2	z
nesložení	nesložení	k1gNnSc2	nesložení
kostky	kostka	k1gFnSc2	kostka
<g/>
,	,	kIx,	,
nazpíval	nazpívat	k5eAaBmAgMnS	nazpívat
písničku	písnička	k1gFnSc4	písnička
Rubikova	Rubikův	k2eAgFnSc1d1	Rubikova
kostka	kostka	k1gFnSc1	kostka
interpret	interpret	k1gMnSc1	interpret
Karel	Karel	k1gMnSc1	Karel
Zich	Zich	k1gMnSc1	Zich
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
i	i	k9	i
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
Rubikův	Rubikův	k2eAgInSc1d1	Rubikův
kubismus	kubismus	k1gInSc1	kubismus
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
spočívající	spočívající	k2eAgFnSc2d1	spočívající
ve	v	k7c4	v
skládání	skládání	k1gNnSc4	skládání
velkého	velký	k2eAgInSc2d1	velký
obrazu	obraz	k1gInSc2	obraz
z	z	k7c2	z
rastru	rastr	k1gInSc2	rastr
Rubikových	Rubikových	k2eAgFnPc2d1	Rubikových
kostek	kostka	k1gFnPc2	kostka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Rubikova	Rubikův	k2eAgFnSc1d1	Rubikova
pomsta	pomsta	k1gFnSc1	pomsta
</s>
</p>
<p>
<s>
Ernő	Ernő	k?	Ernő
Rubik	Rubik	k1gMnSc1	Rubik
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
První	první	k4xOgNnSc1	první
mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
skládání	skládání	k1gNnSc6	skládání
Rubikovy	Rubikův	k2eAgFnSc2d1	Rubikova
kostky	kostka	k1gFnSc2	kostka
</s>
</p>
<p>
<s>
Metody	metoda	k1gFnPc1	metoda
rychlého	rychlý	k2eAgNnSc2d1	rychlé
skládání	skládání	k1gNnSc2	skládání
Rubikovy	Rubikův	k2eAgFnSc2d1	Rubikova
kostky	kostka	k1gFnSc2	kostka
</s>
</p>
<p>
<s>
Božské	božský	k2eAgNnSc1d1	božské
číslo	číslo	k1gNnSc1	číslo
Rubikovy	Rubikův	k2eAgFnSc2d1	Rubikova
kostky	kostka	k1gFnSc2	kostka
</s>
</p>
<p>
<s>
Historické	historický	k2eAgInPc1d1	historický
rekordy	rekord	k1gInPc1	rekord
Rubikovy	Rubikův	k2eAgFnSc2d1	Rubikova
kostky	kostka	k1gFnSc2	kostka
</s>
</p>
<p>
<s>
Videa	video	k1gNnPc4	video
rekordů	rekord	k1gInPc2	rekord
ve	v	k7c4	v
skládání	skládání	k1gNnSc4	skládání
Rubikovy	Rubikův	k2eAgFnSc2d1	Rubikova
kostky	kostka	k1gFnSc2	kostka
</s>
</p>
<p>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
Rubikova	Rubikův	k2eAgFnSc1d1	Rubikova
kostka	kostka	k1gFnSc1	kostka
33	[number]	k4	33
<g/>
x	x	k?	x
<g/>
33	[number]	k4	33
<g/>
x	x	k?	x
<g/>
33	[number]	k4	33
</s>
</p>
<p>
<s>
Intuitivní	intuitivní	k2eAgInSc1d1	intuitivní
návod	návod	k1gInSc1	návod
na	na	k7c4	na
složení	složení	k1gNnSc4	složení
-	-	kIx~	-
využívá	využívat	k5eAaImIp3nS	využívat
interaktivní	interaktivní	k2eAgInPc4d1	interaktivní
animované	animovaný	k2eAgInPc4d1	animovaný
simulátory	simulátor	k1gInPc4	simulátor
Rubikovy	Rubikův	k2eAgFnSc2d1	Rubikova
kostky	kostka	k1gFnSc2	kostka
pro	pro	k7c4	pro
lepší	dobrý	k2eAgFnSc4d2	lepší
názornost	názornost	k1gFnSc4	názornost
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Rubikova	Rubikův	k2eAgFnSc1d1	Rubikova
kostka	kostka	k1gFnSc1	kostka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Téma	téma	k1gFnSc1	téma
Rubikova	Rubikův	k2eAgFnSc1d1	Rubikova
kostka	kostka	k1gFnSc1	kostka
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
stránka	stránka	k1gFnSc1	stránka
Rubikovy	Rubikův	k2eAgFnSc2d1	Rubikova
kostky	kostka	k1gFnSc2	kostka
</s>
</p>
