<s>
Jak	jak	k6eAd1
se	se	k3xPyFc4
označuje	označovat	k5eAaImIp3nS
typ	typ	k1gInSc1
intersexuálů	intersexuál	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1
mají	mít	k5eAaImIp3nP
v	v	k7c6
buňce	buňka	k1gFnSc6
soubor	soubor	k1gInSc1
chromozomů	chromozom	k1gInPc2
XX	XX	kA
a	a	k8xC
vyvinutou	vyvinutý	k2eAgFnSc4d1
dělohu	děloha	k1gFnSc4
i	i	k8xC
vaječníky	vaječník	k1gInPc4
<g/>
,	,	kIx,
ale	ale	k8xC
jejich	jejich	k3xOp3gFnPc1
genitálie	genitálie	k1gFnPc1
jsou	být	k5eAaImIp3nP
nejednoznačné	jednoznačný	k2eNgFnPc1d1
nebo	nebo	k8xC
převážně	převážně	k6eAd1
mužské	mužský	k2eAgNnSc1d1
<g/>
?	?	kIx.
</s>