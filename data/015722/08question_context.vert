<s desamb="1">
Jedinci	jedinec	k1gMnPc7
v	v	k7c6
této	tento	k3xDgFnSc6
skupině	skupina	k1gFnSc6
mají	mít	k5eAaImIp3nP
v	v	k7c6
buňce	buňka	k1gFnSc6
soubor	soubor	k1gInSc1
chromozomů	chromozom	k1gInPc2
XX	XX	kA
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
tedy	tedy	k9
genetickými	genetický	k2eAgFnPc7d1
ženami	žena	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spolu	spolu	k6eAd1
s	s	k7c7
tím	ten	k3xDgNnSc7
mají	mít	k5eAaImIp3nP
vyvinutou	vyvinutý	k2eAgFnSc4d1
dělohu	děloha	k1gFnSc4
i	i	k8xC
vaječníky	vaječník	k1gInPc4
<g/>
,	,	kIx,
avšak	avšak	k8xC
jejich	jejich	k3xOp3gFnPc1
genitálie	genitálie	k1gFnPc1
jsou	být	k5eAaImIp3nP
nejednoznačné	jednoznačný	k2eNgFnPc1d1
nebo	nebo	k8xC
převážně	převážně	k6eAd1
mužské	mužský	k2eAgNnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>