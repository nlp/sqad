<p>
<s>
Unknown	Unknown	k1gMnSc1	Unknown
Pleasures	Pleasures	k1gMnSc1	Pleasures
(	(	kIx(	(
<g/>
Neznámá	neznámá	k1gFnSc1	neznámá
potěšení	potěšení	k1gNnSc2	potěšení
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
první	první	k4xOgNnSc4	první
studiové	studiový	k2eAgNnSc4d1	studiové
album	album	k1gNnSc4	album
skupiny	skupina	k1gFnSc2	skupina
Joy	Joy	k1gFnPc2	Joy
Division	Division	k1gInSc1	Division
<g/>
,	,	kIx,	,
vydané	vydaný	k2eAgInPc1d1	vydaný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
<g/>
.	.	kIx.	.
</s>
<s>
Produkoval	produkovat	k5eAaImAgMnS	produkovat
ho	on	k3xPp3gNnSc4	on
Martin	Martin	k1gMnSc1	Martin
Hannett	Hannett	k1gMnSc1	Hannett
a	a	k8xC	a
nahrané	nahraný	k2eAgNnSc1d1	nahrané
bylo	být	k5eAaImAgNnS	být
Strawberry	Strawberro	k1gNnPc7	Strawberro
Studios	Studios	k?	Studios
<g/>
,	,	kIx,	,
Stockport	Stockport	k1gInSc1	Stockport
<g/>
,	,	kIx,	,
Anglie	Anglie	k1gFnSc1	Anglie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Návrh	návrh	k1gInSc1	návrh
obálky	obálka	k1gFnSc2	obálka
<g/>
:	:	kIx,	:
Joy	Joy	k1gFnSc1	Joy
Division	Division	k1gInSc1	Division
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
Saville	Saville	k1gFnSc2	Saville
a	a	k8xC	a
Chris	Chris	k1gFnSc2	Chris
Mathan	Mathana	k1gFnPc2	Mathana
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Skladby	skladba	k1gFnSc2	skladba
==	==	k?	==
</s>
</p>
<p>
<s>
Disorder	Disorder	k1gInSc1	Disorder
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
32	[number]	k4	32
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Day	Day	k?	Day
of	of	k?	of
the	the	k?	the
Lords	Lords	k1gInSc1	Lords
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
50	[number]	k4	50
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Candidate	Candidat	k1gMnSc5	Candidat
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Insight	Insight	k1gInSc1	Insight
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
29	[number]	k4	29
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
New	New	k?	New
Dawn	Dawn	k1gInSc1	Dawn
Fades	fadesa	k1gFnPc2	fadesa
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
48	[number]	k4	48
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
She	She	k?	She
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Lost	Lost	k1gInSc1	Lost
Control	Control	k1gInSc1	Control
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
57	[number]	k4	57
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Shadowplay	Shadowplaa	k1gFnPc1	Shadowplaa
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
56	[number]	k4	56
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Wilderness	Wilderness	k1gInSc1	Wilderness
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
38	[number]	k4	38
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Interzone	Interzon	k1gMnSc5	Interzon
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
16	[number]	k4	16
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
I	i	k9	i
Remember	Remember	k1gInSc1	Remember
Nothing	Nothing	k1gInSc1	Nothing
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
52	[number]	k4	52
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Obsazení	obsazení	k1gNnSc1	obsazení
==	==	k?	==
</s>
</p>
<p>
<s>
Ian	Ian	k?	Ian
Curtis	Curtis	k1gInSc1	Curtis
–	–	k?	–
zpěv	zpěv	k1gInSc1	zpěv
</s>
</p>
<p>
<s>
Bernard	Bernard	k1gMnSc1	Bernard
Sumner	Sumner	k1gMnSc1	Sumner
–	–	k?	–
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
klávesy	klávesa	k1gFnPc1	klávesa
</s>
</p>
<p>
<s>
Peter	Peter	k1gMnSc1	Peter
Hook	Hook	k1gMnSc1	Hook
–	–	k?	–
basová	basový	k2eAgFnSc1d1	basová
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc4	druhý
hlas	hlas	k1gInSc4	hlas
v	v	k7c6	v
písni	píseň	k1gFnSc6	píseň
"	"	kIx"	"
<g/>
Interzone	Interzon	k1gMnSc5	Interzon
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Stephen	Stephen	k2eAgInSc4d1	Stephen
Morris	Morris	k1gInSc4	Morris
–	–	k?	–
bubny	buben	k1gInPc4	buben
<g/>
,	,	kIx,	,
perkusy	perkus	k1gInPc4	perkus
</s>
</p>
