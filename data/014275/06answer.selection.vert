<s>
Severoatlantická	severoatlantický	k2eAgFnSc1d1
aliance	aliance	k1gFnSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
North	North	k1gInSc1
Atlantic	Atlantice	k1gFnPc2
Treaty	Treata	k1gFnSc2
Organization	Organization	k1gInSc1
–	–	k?
NATO	NATO	kA
<g/>
,	,	kIx,
francouzsky	francouzsky	k6eAd1
Organisation	Organisation	k1gInSc4
du	du	k?
Traité	Traitý	k2eAgFnSc2d1
de	de	k?
l	l	kA
<g/>
’	’	k?
<g/>
Atlantique	Atlantique	k1gInSc1
Nord	Nord	k1gInSc1
–	–	k?
OTAN	OTAN	kA
<g/>
;	;	kIx,
doslova	doslova	k6eAd1
Organizace	organizace	k1gFnSc1
Severoatlantické	severoatlantický	k2eAgFnSc2d1
smlouvy	smlouva	k1gFnSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
euroatlantický	euroatlantický	k2eAgInSc1d1
mezinárodní	mezinárodní	k2eAgInSc1d1
vojenský	vojenský	k2eAgInSc1d1
pakt	pakt	k1gInSc1
<g/>
.	.	kIx.
</s>