<s>
Severoatlantická	severoatlantický	k2eAgFnSc1d1
aliance	aliance	k1gFnSc1
</s>
<s>
Severoatlantická	severoatlantický	k2eAgFnSc1d1
aliance	aliance	k1gFnSc1
</s>
<s>
Organisation	Organisation	k1gInSc4
du	du	k?
Traité	Traitý	k2eAgFnSc2d1
de	de	k?
l	l	kA
<g/>
'	'	kIx"
<g/>
Atlantique	Atlantiqu	k1gMnSc2
Nord	Nord	k1gInSc1
logo	logo	k1gNnSc4
NATO	NATO	kA
</s>
<s>
Členské	členský	k2eAgFnPc1d1
země	zem	k1gFnPc1
NATO	NATO	kA
Zkratka	zkratka	k1gFnSc1
</s>
<s>
NATO	NATO	kA
<g/>
/	/	kIx~
<g/>
OTAN	OTAN	kA
Motto	motto	k1gNnSc1
</s>
<s>
„	„	k?
<g/>
Animus	Animus	k1gInSc1
in	in	k?
consulendo	consulendo	k6eAd1
liber	libra	k1gFnPc2
<g/>
“	“	k?
Vznik	vznik	k1gInSc1
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1949	#num#	k4
Typ	typ	k1gInSc1
</s>
<s>
vojenský	vojenský	k2eAgInSc4d1
pakt	pakt	k1gInSc4
Sídlo	sídlo	k1gNnSc4
</s>
<s>
Brusel	Brusel	k1gInSc1
<g/>
,	,	kIx,
Belgie	Belgie	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc1
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
52	#num#	k4
<g/>
′	′	k?
<g/>
33	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
4	#num#	k4
<g/>
°	°	k?
<g/>
25	#num#	k4
<g/>
′	′	k?
<g/>
30	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Úřední	úřední	k2eAgInSc4d1
jazyk	jazyk	k1gInSc4
</s>
<s>
angličtina	angličtina	k1gFnSc1
<g/>
,	,	kIx,
francouzština	francouzština	k1gFnSc1
Členové	člen	k1gMnPc1
</s>
<s>
30	#num#	k4
zemí	zem	k1gFnPc2
Generální	generální	k2eAgMnSc1d1
tajemník	tajemník	k1gMnSc1
</s>
<s>
Jens	Jens	k6eAd1
Stoltenberg	Stoltenberg	k1gInSc1
Hlavní	hlavní	k2eAgInSc1d1
orgán	orgán	k1gInSc1
</s>
<s>
Severoatlantická	severoatlantický	k2eAgFnSc1d1
rada	rada	k1gFnSc1
Oficiální	oficiální	k2eAgFnSc1d1
web	web	k1gInSc4
</s>
<s>
nato	nato	k6eAd1
<g/>
.	.	kIx.
<g/>
int	int	k?
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
můžou	můžou	k?
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Severoatlantická	severoatlantický	k2eAgFnSc1d1
aliance	aliance	k1gFnSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
North	North	k1gInSc1
Atlantic	Atlantice	k1gFnPc2
Treaty	Treata	k1gFnSc2
Organization	Organization	k1gInSc1
–	–	k?
NATO	NATO	kA
<g/>
,	,	kIx,
francouzsky	francouzsky	k6eAd1
Organisation	Organisation	k1gInSc4
du	du	k?
Traité	Traitý	k2eAgFnSc2d1
de	de	k?
l	l	kA
<g/>
’	’	k?
<g/>
Atlantique	Atlantique	k1gInSc1
Nord	Nord	k1gInSc1
–	–	k?
OTAN	OTAN	kA
<g/>
;	;	kIx,
doslova	doslova	k6eAd1
Organizace	organizace	k1gFnSc1
Severoatlantické	severoatlantický	k2eAgFnSc2d1
smlouvy	smlouva	k1gFnSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
euroatlantický	euroatlantický	k2eAgInSc1d1
mezinárodní	mezinárodní	k2eAgInSc1d1
vojenský	vojenský	k2eAgInSc1d1
pakt	pakt	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgInS
založen	založit	k5eAaPmNgInS
4	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1949	#num#	k4
podpisem	podpis	k1gInSc7
Severoatlantické	severoatlantický	k2eAgFnSc2d1
smlouvy	smlouva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aliance	aliance	k1gFnSc1
sídlí	sídlet	k5eAaImIp3nS
v	v	k7c6
Bruselu	Brusel	k1gInSc6
v	v	k7c6
Belgii	Belgie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reakcí	reakce	k1gFnPc2
na	na	k7c4
zřízení	zřízení	k1gNnSc4
Západoevropské	západoevropský	k2eAgFnSc2d1
unie	unie	k1gFnSc2
a	a	k8xC
Pařížské	pařížský	k2eAgFnSc2d1
dohody	dohoda	k1gFnSc2
umožňující	umožňující	k2eAgFnSc2d1
v	v	k7c6
roce	rok	k1gInSc6
1954	#num#	k4
vstup	vstup	k1gInSc4
NSR	NSR	kA
do	do	k7c2
NATO	NATO	kA
bylo	být	k5eAaImAgNnS
v	v	k7c6
roce	rok	k1gInSc6
1955	#num#	k4
založení	založení	k1gNnSc2
tzv.	tzv.	kA
východního	východní	k2eAgInSc2d1
bloku	blok	k1gInSc2
nazývaného	nazývaný	k2eAgInSc2d1
Varšavská	varšavský	k2eAgFnSc1d1
smlouva	smlouva	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgNnPc1
byla	být	k5eAaImAgNnP
po	po	k7c6
rozpadu	rozpad	k1gInSc6
sovětského	sovětský	k2eAgNnSc2d1
impéria	impérium	k1gNnSc2
a	a	k8xC
zániku	zánik	k1gInSc2
NDR	NDR	kA
v	v	k7c6
roce	rok	k1gInSc6
1991	#num#	k4
rozpuštěna	rozpustit	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
svém	svůj	k3xOyFgInSc6
počátku	počátek	k1gInSc6
byla	být	k5eAaImAgFnS
Severoatlantická	severoatlantický	k2eAgFnSc1d1
aliance	aliance	k1gFnSc1
jen	jen	k9
o	o	k7c4
trochu	trocha	k1gFnSc4
více	hodně	k6eAd2
než	než	k8xS
politické	politický	k2eAgNnSc1d1
sdružení	sdružení	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Korejská	korejský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
ale	ale	k8xC
podnítila	podnítit	k5eAaPmAgFnS
členské	členský	k2eAgInPc4d1
státy	stát	k1gInPc4
k	k	k7c3
vytvoření	vytvoření	k1gNnSc3
vojenské	vojenský	k2eAgFnSc2d1
struktury	struktura	k1gFnSc2
pod	pod	k7c7
dohledem	dohled	k1gInSc7
dvou	dva	k4xCgInPc2
amerických	americký	k2eAgMnPc2d1
velitelů	velitel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slovy	slovo	k1gNnPc7
prvního	první	k4xOgMnSc2
generálního	generální	k2eAgMnSc2d1
tajemníka	tajemník	k1gMnSc2
Hastingse	Hastingse	k1gFnSc2
Ismaye	Ismaye	k1gNnSc1
bylo	být	k5eAaImAgNnS
úkolem	úkol	k1gInSc7
NATO	NATO	kA
„	„	k?
<g/>
udržet	udržet	k5eAaPmF
Ameriku	Amerika	k1gFnSc4
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
,	,	kIx,
Rusko	Rusko	k1gNnSc1
mimo	mimo	k7c4
západní	západní	k2eAgFnSc4d1
Evropu	Evropa	k1gFnSc4
a	a	k8xC
Německo	Německo	k1gNnSc4
při	při	k7c6
zemi	zem	k1gFnSc6
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1966	#num#	k4
odešla	odejít	k5eAaPmAgFnS
Francie	Francie	k1gFnSc1
z	z	k7c2
vojenských	vojenský	k2eAgFnPc2d1
struktur	struktura	k1gFnPc2
NATO	NATO	kA
kvůli	kvůli	k7c3
snaze	snaha	k1gFnSc3
o	o	k7c4
udržení	udržení	k1gNnSc4
si	se	k3xPyFc3
vojenské	vojenský	k2eAgFnPc1d1
nezávislosti	nezávislost	k1gFnPc1
na	na	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvůli	kvůli	k7c3
tomu	ten	k3xDgNnSc3
se	se	k3xPyFc4
sídlo	sídlo	k1gNnSc1
přesunulo	přesunout	k5eAaPmAgNnS
z	z	k7c2
Paříže	Paříž	k1gFnSc2
do	do	k7c2
Bruselu	Brusel	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
pádu	pád	k1gInSc6
Berlínské	berlínský	k2eAgFnSc2d1
zdi	zeď	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1989	#num#	k4
se	se	k3xPyFc4
Aliance	aliance	k1gFnSc1
angažovala	angažovat	k5eAaBmAgFnS
ve	v	k7c6
válce	válka	k1gFnSc6
v	v	k7c6
Jugoslávii	Jugoslávie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc2
vojenské	vojenský	k2eAgFnSc2d1
operace	operace	k1gFnSc2
NATO	NATO	kA
v	v	k7c6
historii	historie	k1gFnSc6
proběhly	proběhnout	k5eAaPmAgFnP
mezi	mezi	k7c7
lety	léto	k1gNnPc7
1992	#num#	k4
a	a	k8xC
1995	#num#	k4
při	při	k7c6
válce	válka	k1gFnSc6
v	v	k7c6
Bosně	Bosna	k1gFnSc6
a	a	k8xC
Hercegovině	Hercegovina	k1gFnSc6
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
později	pozdě	k6eAd2
v	v	k7c6
roce	rok	k1gInSc6
1999	#num#	k4
v	v	k7c6
Jugoslávii	Jugoslávie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aliance	aliance	k1gFnSc1
se	se	k3xPyFc4
snažila	snažit	k5eAaImAgFnS
zlepšit	zlepšit	k5eAaPmF
vztahy	vztah	k1gInPc4
s	s	k7c7
východními	východní	k2eAgInPc7d1
státy	stát	k1gInPc7
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
vyústilo	vyústit	k5eAaPmAgNnS
v	v	k7c4
její	její	k3xOp3gNnSc4
rozšíření	rozšíření	k1gNnSc4
několika	několik	k4yIc7
státy	stát	k1gInPc7
bývalé	bývalý	k2eAgFnSc2d1
Varšavské	varšavský	k2eAgFnSc2d1
smlouvy	smlouva	k1gFnSc2
v	v	k7c6
letech	léto	k1gNnPc6
1999	#num#	k4
a	a	k8xC
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Článek	článek	k1gInSc1
5	#num#	k4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
tvoří	tvořit	k5eAaImIp3nS
základ	základ	k1gInSc4
Severoatlantické	severoatlantický	k2eAgFnSc2d1
smlouvy	smlouva	k1gFnSc2
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
poprvé	poprvé	k6eAd1
použit	použít	k5eAaPmNgInS
po	po	k7c6
teroristických	teroristický	k2eAgInPc6d1
útocích	útok	k1gInPc6
11	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2001	#num#	k4
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
útok	útok	k1gInSc1
byl	být	k5eAaImAgInS
považován	považován	k2eAgInSc1d1
za	za	k7c4
útok	útok	k1gInSc4
proti	proti	k7c3
všem	všecek	k3xTgMnPc3
19	#num#	k4
členským	členský	k2eAgInPc3d1
státům	stát	k1gInPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Od	od	k7c2
roku	rok	k1gInSc2
2003	#num#	k4
vede	vést	k5eAaImIp3nS
Aliance	aliance	k1gFnSc1
činnost	činnost	k1gFnSc1
Mezinárodních	mezinárodní	k2eAgFnPc2d1
bezpečnostních	bezpečnostní	k2eAgFnPc2d1
podpůrných	podpůrný	k2eAgFnPc2d1
sil	síla	k1gFnPc2
v	v	k7c6
Afghánistánu	Afghánistán	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
konce	konec	k1gInSc2
roku	rok	k1gInSc2
2011	#num#	k4
zajišťovala	zajišťovat	k5eAaImAgFnS
výcvik	výcvik	k1gInSc4
nové	nový	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
v	v	k7c6
Iráku	Irák	k1gInSc6
<g/>
,	,	kIx,
účastní	účastnit	k5eAaImIp3nS
se	se	k3xPyFc4
protipirátských	protipirátský	k2eAgFnPc2d1
operací	operace	k1gFnPc2
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
vyhlásila	vyhlásit	k5eAaPmAgFnS
bezletovou	bezletový	k2eAgFnSc4d1
zónu	zóna	k1gFnSc4
nad	nad	k7c7
Libyí	Libye	k1gFnSc7
v	v	k7c6
souladu	soulad	k1gInSc6
s	s	k7c7
rezolucí	rezoluce	k1gFnSc7
Rady	rada	k1gFnSc2
bezpečnosti	bezpečnost	k1gFnSc2
OSN	OSN	kA
č.	č.	k?
1973	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původní	původní	k2eAgInSc1d1
cíl	cíl	k1gInSc1
akce	akce	k1gFnSc2
<g/>
,	,	kIx,
na	na	k7c6
níž	jenž	k3xRgFnSc6
intervenční	intervenční	k2eAgFnSc6d1
síly	síla	k1gFnPc1
dostaly	dostat	k5eAaPmAgFnP
mandát	mandát	k1gInSc4
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
však	však	k9
rozšířen	rozšířit	k5eAaPmNgInS
(	(	kIx(
<g/>
už	už	k9
mimo	mimo	k7c4
mandát	mandát	k1gInSc4
OSN	OSN	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
vedlo	vést	k5eAaImAgNnS
ke	k	k7c3
svržení	svržení	k1gNnSc3
dosavadního	dosavadní	k2eAgInSc2d1
vládnoucího	vládnoucí	k2eAgInSc2d1
režimu	režim	k1gInSc2
a	a	k8xC
uvrhlo	uvrhnout	k5eAaPmAgNnS
zemi	zem	k1gFnSc4
do	do	k7c2
krvavého	krvavý	k2eAgInSc2d1
chaosu	chaos	k1gInSc2
trvajícího	trvající	k2eAgInSc2d1
dodnes	dodnes	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Dne	den	k1gInSc2
16	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2002	#num#	k4
byla	být	k5eAaImAgFnS
mezi	mezi	k7c4
NATO	NATO	kA
a	a	k8xC
Evropskou	evropský	k2eAgFnSc7d1
unií	unie	k1gFnSc7
podepsána	podepsán	k2eAgFnSc1d1
dohoda	dohoda	k1gFnSc1
o	o	k7c6
spolupráci	spolupráce	k1gFnSc6
Berlín	Berlín	k1gInSc4
plus	plus	k1gNnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
mimo	mimo	k7c4
jiné	jiný	k2eAgInPc4d1
umožňuje	umožňovat	k5eAaImIp3nS
Evropské	evropský	k2eAgFnSc3d1
unii	unie	k1gFnSc3
využívat	využívat	k5eAaImF,k5eAaPmF
prostředky	prostředek	k1gInPc4
i	i	k8xC
kapacity	kapacita	k1gFnPc4
NATO	NATO	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
současnosti	současnost	k1gFnSc6
je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7
Severoatlantické	severoatlantický	k2eAgFnSc2d1
aliance	aliance	k1gFnSc2
30	#num#	k4
členských	členský	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
,	,	kIx,
nejnovějšími	nový	k2eAgInPc7d3
členy	člen	k1gInPc7
jsou	být	k5eAaImIp3nP
od	od	k7c2
června	červen	k1gInSc2
2017	#num#	k4
Černá	černý	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
a	a	k8xC
od	od	k7c2
března	březen	k1gInSc2
2020	#num#	k4
Severní	severní	k2eAgFnSc1d1
Makedonie	Makedonie	k1gFnSc1
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
předtím	předtím	k6eAd1
od	od	k7c2
dubna	duben	k1gInSc2
2009	#num#	k4
Albánie	Albánie	k1gFnSc2
a	a	k8xC
Chorvatsko	Chorvatsko	k1gNnSc4
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkové	celkový	k2eAgInPc1d1
armádní	armádní	k2eAgInPc1d1
výdaje	výdaj	k1gInPc1
všech	všecek	k3xTgInPc2
členů	člen	k1gInPc2
NATO	NATO	kA
tvoří	tvořit	k5eAaImIp3nS
přes	přes	k7c4
70	#num#	k4
%	%	kIx~
celosvětových	celosvětový	k2eAgInPc2d1
armádních	armádní	k2eAgInPc2d1
výdajů	výdaj	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Samotné	samotný	k2eAgInPc1d1
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
zodpovídají	zodpovídat	k5eAaPmIp3nP,k5eAaImIp3nP
za	za	k7c4
43	#num#	k4
%	%	kIx~
celosvětových	celosvětový	k2eAgInPc2d1
výdajů	výdaj	k1gInPc2
na	na	k7c4
armádu	armáda	k1gFnSc4
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
<g/>
,	,	kIx,
Francie	Francie	k1gFnSc1
<g/>
,	,	kIx,
Německo	Německo	k1gNnSc1
a	a	k8xC
Itálie	Itálie	k1gFnSc1
za	za	k7c4
dalších	další	k2eAgNnPc2d1
15	#num#	k4
%	%	kIx~
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vlajka	vlajka	k1gFnSc1
NATO	NATO	kA
<g/>
/	/	kIx~
<g/>
OTAN	OTAN	kA
</s>
<s>
Členství	členství	k1gNnSc1
v	v	k7c6
organizacích	organizace	k1gFnPc6
<g/>
:	:	kIx,
</s>
<s>
státy	stát	k1gInPc1
v	v	k7c6
NATO	NATO	kA
a	a	k8xC
EU	EU	kA
</s>
<s>
státy	stát	k1gInPc1
pouze	pouze	k6eAd1
v	v	k7c6
EU	EU	kA
</s>
<s>
státy	stát	k1gInPc1
pouze	pouze	k6eAd1
v	v	k7c6
NATO	NATO	kA
</s>
<s>
Dějiny	dějiny	k1gFnPc1
</s>
<s>
Počátky	počátek	k1gInPc4
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Severoatlantická	severoatlantický	k2eAgFnSc1d1
smlouva	smlouva	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Severoatlantická	severoatlantický	k2eAgFnSc1d1
smlouva	smlouva	k1gFnSc1
byla	být	k5eAaImAgFnS
podepsána	podepsán	k2eAgFnSc1d1
4	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1949	#num#	k4
ve	v	k7c6
Washingtonu	Washington	k1gInSc6
<g/>
,	,	kIx,
D.C.	D.C.	k1gFnSc1
a	a	k8xC
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
ji	on	k3xPp3gFnSc4
ratifikovaly	ratifikovat	k5eAaBmAgInP
v	v	k7c6
srpnu	srpen	k1gInSc6
(	(	kIx(
<g/>
foto	foto	k1gNnSc1
<g/>
:	:	kIx,
Abbie	Abbie	k1gFnSc1
Rowe	Rowe	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Za	za	k7c4
předchůdce	předchůdce	k1gMnSc4
NATO	NATO	kA
je	být	k5eAaImIp3nS
považován	považován	k2eAgInSc1d1
Bruselský	bruselský	k2eAgInSc1d1
pakt	pakt	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
17	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1948	#num#	k4
podepsaly	podepsat	k5eAaPmAgFnP
Belgie	Belgie	k1gFnPc1
<g/>
,	,	kIx,
Nizozemsko	Nizozemsko	k1gNnSc1
<g/>
,	,	kIx,
Lucembursko	Lucembursko	k1gNnSc1
<g/>
,	,	kIx,
Francie	Francie	k1gFnSc1
a	a	k8xC
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
obraně	obrana	k1gFnSc3
proti	proti	k7c3
Sovětskému	sovětský	k2eAgInSc3d1
svazu	svaz	k1gInSc3
byla	být	k5eAaImAgFnS
ale	ale	k8xC
zapotřebí	zapotřebí	k6eAd1
i	i	k9
účast	účast	k1gFnSc4
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
se	se	k3xPyFc4
začalo	začít	k5eAaPmAgNnS
mluvit	mluvit	k5eAaImF
o	o	k7c6
nové	nový	k2eAgFnSc6d1
vojenské	vojenský	k2eAgFnSc6d1
alianci	aliance	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
To	ten	k3xDgNnSc1
vyústilo	vyústit	k5eAaPmAgNnS
v	v	k7c4
Severoatlantickou	severoatlantický	k2eAgFnSc4d1
smlouvu	smlouva	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
měla	mít	k5eAaImAgFnS
ten	ten	k3xDgInSc4
samý	samý	k3xTgInSc4
cíl	cíl	k1gInSc4
<g/>
:	:	kIx,
Ubránit	ubránit	k5eAaPmF
se	se	k3xPyFc4
Sovětskému	sovětský	k2eAgInSc3d1
svazu	svaz	k1gInSc3
a	a	k8xC
později	pozdě	k6eAd2
roku	rok	k1gInSc2
1955	#num#	k4
Varšavské	varšavský	k2eAgFnSc6d1
smlouvě	smlouva	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
podepsána	podepsán	k2eAgFnSc1d1
4	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1949	#num#	k4
ve	v	k7c6
Washingtonu	Washington	k1gInSc6
<g/>
,	,	kIx,
D.C.	D.C.	k1gMnSc1
pěti	pět	k4xCc2
státy	stát	k1gInPc1
Bruselského	bruselský	k2eAgInSc2d1
paktu	pakt	k1gInSc2
<g/>
,	,	kIx,
Spojenými	spojený	k2eAgInPc7d1
státy	stát	k1gInPc7
<g/>
,	,	kIx,
Kanadou	Kanada	k1gFnSc7
<g/>
,	,	kIx,
Portugalskem	Portugalsko	k1gNnSc7
<g/>
,	,	kIx,
Itálií	Itálie	k1gFnSc7
<g/>
,	,	kIx,
Norskem	Norsko	k1gNnSc7
<g/>
,	,	kIx,
Dánskem	Dánsko	k1gNnSc7
a	a	k8xC
Islandem	Island	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
místa	místo	k1gNnSc2
podpisu	podpis	k1gInSc2
se	se	k3xPyFc4
někdy	někdy	k6eAd1
nazývá	nazývat	k5eAaImIp3nS
Washingtonskou	washingtonský	k2eAgFnSc7d1
smlouvou	smlouva	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Smlouva	smlouva	k1gFnSc1
obsahuje	obsahovat	k5eAaImIp3nS
14	#num#	k4
článků	článek	k1gInPc2
<g/>
,	,	kIx,
mezi	mezi	k7c4
hlavní	hlavní	k2eAgFnPc4d1
patří	patřit	k5eAaImIp3nS
článek	článek	k1gInSc1
1	#num#	k4
<g/>
:	:	kIx,
</s>
<s>
„	„	k?
</s>
<s>
Smluvní	smluvní	k2eAgFnPc1d1
strany	strana	k1gFnPc1
se	se	k3xPyFc4
zavazují	zavazovat	k5eAaImIp3nP
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
je	být	k5eAaImIp3nS
uvedeno	uvést	k5eAaPmNgNnS
v	v	k7c6
Chartě	charta	k1gFnSc6
OSN	OSN	kA
<g/>
,	,	kIx,
urovnávat	urovnávat	k5eAaImF
veškeré	veškerý	k3xTgInPc4
mezinárodní	mezinárodní	k2eAgInPc4d1
spory	spor	k1gInPc4
<g/>
,	,	kIx,
v	v	k7c6
nichž	jenž	k3xRgFnPc6
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
účastny	účasten	k2eAgInPc1d1
<g/>
,	,	kIx,
mírovými	mírový	k2eAgInPc7d1
prostředky	prostředek	k1gInPc7
tak	tak	k8xC,k8xS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
nebyl	být	k5eNaImAgInS
ohrožen	ohrozit	k5eAaPmNgInS
mezinárodní	mezinárodní	k2eAgInSc1d1
mír	mír	k1gInSc1
<g/>
,	,	kIx,
bezpečnost	bezpečnost	k1gFnSc4
a	a	k8xC
spravedlnost	spravedlnost	k1gFnSc4
<g/>
,	,	kIx,
a	a	k8xC
zdržet	zdržet	k5eAaPmF
se	se	k3xPyFc4
ve	v	k7c6
svých	svůj	k3xOyFgInPc6
mezinárodních	mezinárodní	k2eAgInPc6d1
vztazích	vztah	k1gInPc6
hrozby	hrozba	k1gFnSc2
silou	síla	k1gFnSc7
nebo	nebo	k8xC
použití	použití	k1gNnSc1
síly	síla	k1gFnSc2
jakýmkoli	jakýkoli	k3yIgInSc7
způsobem	způsob	k1gInSc7
neslučitelným	slučitelný	k2eNgFnPc3d1
s	s	k7c7
cíli	cíl	k1gInPc7
OSN	OSN	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
“	“	k?
</s>
<s>
Ve	v	k7c6
článku	článek	k1gInSc6
IV	Iva	k1gFnPc2
Bruselského	bruselský	k2eAgInSc2d1
paktu	pakt	k1gInSc2
se	se	k3xPyFc4
výslovně	výslovně	k6eAd1
píše	psát	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
reakce	reakce	k1gFnSc1
států	stát	k1gInPc2
na	na	k7c4
ozbrojený	ozbrojený	k2eAgInSc4d1
útok	útok	k1gInSc4
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
vojenského	vojenský	k2eAgInSc2d1
charakteru	charakter	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
Tím	ten	k3xDgNnSc7
se	se	k3xPyFc4
liší	lišit	k5eAaImIp3nP
od	od	k7c2
článku	článek	k1gInSc2
5	#num#	k4
Severoatlantické	severoatlantický	k2eAgFnSc2d1
smlouvy	smlouva	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
vojenský	vojenský	k2eAgInSc1d1
útok	útok	k1gInSc1
pouze	pouze	k6eAd1
jednou	jeden	k4xCgFnSc7
z	z	k7c2
možných	možný	k2eAgFnPc2d1
reakcí	reakce	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navíc	navíc	k6eAd1
Severoatlantická	severoatlantický	k2eAgFnSc1d1
smlouva	smlouva	k1gFnSc1
(	(	kIx(
<g/>
ve	v	k7c6
článku	článek	k1gInSc6
6	#num#	k4
<g/>
)	)	kIx)
omezuje	omezovat	k5eAaImIp3nS
pole	pole	k1gNnSc4
působnosti	působnost	k1gFnSc2
nad	nad	k7c4
obratník	obratník	k1gInSc4
Raka	rak	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
To	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
důvod	důvod	k1gInSc1
<g/>
,	,	kIx,
proč	proč	k6eAd1
NATO	nato	k6eAd1
nezasáhlo	zasáhnout	k5eNaPmAgNnS
při	při	k7c6
válce	válka	k1gFnSc6
o	o	k7c4
Falklandy	Falkland	k1gInPc4
nebo	nebo	k8xC
při	při	k7c6
indické	indický	k2eAgFnSc6d1
vojenské	vojenský	k2eAgFnSc6d1
invazi	invaze	k1gFnSc6
do	do	k7c2
Portugalské	portugalský	k2eAgFnSc2d1
Indie	Indie	k1gFnSc2
roku	rok	k1gInSc2
1961	#num#	k4
a	a	k8xC
po	po	k7c6
anexi	anexe	k1gFnSc6
tohoto	tento	k3xDgNnSc2
území	území	k1gNnSc2
Indií	Indie	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Studená	studený	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Studená	studený	k2eAgFnSc1d1
válka	válka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Začátek	začátek	k1gInSc1
Korejské	korejský	k2eAgFnSc2d1
války	válka	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1950	#num#	k4
podnítil	podnítit	k5eAaPmAgInS
NATO	NATO	kA
k	k	k7c3
rozvoji	rozvoj	k1gInSc3
vojenských	vojenský	k2eAgFnPc2d1
struktur	struktura	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
konferenci	konference	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
1952	#num#	k4
v	v	k7c6
Lisabonu	Lisabon	k1gInSc6
se	se	k3xPyFc4
dohodla	dohodnout	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
zavede	zavést	k5eAaPmIp3nS
minimálně	minimálně	k6eAd1
50	#num#	k4
divizí	divize	k1gFnPc2
do	do	k7c2
konce	konec	k1gInSc2
toho	ten	k3xDgInSc2
roku	rok	k1gInSc2
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
do	do	k7c2
dvou	dva	k4xCgNnPc2
let	léto	k1gNnPc2
počet	počet	k1gInSc1
rozšíří	rozšířit	k5eAaPmIp3nS
na	na	k7c4
96	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
Příští	příští	k2eAgInSc1d1
rok	rok	k1gInSc1
byl	být	k5eAaImAgInS
ale	ale	k8xC
požadavek	požadavek	k1gInSc1
snížen	snížit	k5eAaPmNgInS
na	na	k7c4
zhruba	zhruba	k6eAd1
35	#num#	k4
divizí	divize	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Lisabonu	Lisabon	k1gInSc6
byla	být	k5eAaImAgFnS
také	také	k9
zavedena	zaveden	k2eAgFnSc1d1
pozice	pozice	k1gFnSc1
Generálního	generální	k2eAgMnSc2d1
tajemníka	tajemník	k1gMnSc2
NATO	NATO	kA
<g/>
,	,	kIx,
jímž	jenž	k3xRgNnSc7
se	se	k3xPyFc4
jako	jako	k9
první	první	k4xOgMnSc1
stal	stát	k5eAaPmAgMnS
baron	baron	k1gMnSc1
Hastings	Hastingsa	k1gFnPc2
Lionel	Lionel	k1gMnSc1
Ismay	Ismaa	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
září	září	k1gNnSc6
1952	#num#	k4
začalo	začít	k5eAaPmAgNnS
první	první	k4xOgNnSc4
velké	velký	k2eAgNnSc4d1
vojenské	vojenský	k2eAgNnSc4d1
cvičení	cvičení	k1gNnSc4
NATO	NATO	kA
na	na	k7c6
moři	moře	k1gNnSc6
<g/>
:	:	kIx,
při	při	k7c6
operaci	operace	k1gFnSc6
Mainbrace	Mainbrace	k1gFnSc2
nacvičovalo	nacvičovat	k5eAaImAgNnS
obranu	obrana	k1gFnSc4
Dánska	Dánsko	k1gNnSc2
a	a	k8xC
Norska	Norsko	k1gNnSc2
přes	přes	k7c4
160	#num#	k4
plavidel	plavidlo	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
Mezi	mezi	k7c4
další	další	k2eAgNnPc4d1
velká	velký	k2eAgNnPc4d1
cvičení	cvičení	k1gNnPc4
patří	patřit	k5eAaImIp3nS
např.	např.	kA
operace	operace	k1gFnSc1
Grand	grand	k1gMnSc1
Slam	slam	k1gInSc1
<g/>
,	,	kIx,
při	při	k7c6
níž	jenž	k3xRgFnSc6
NATO	nato	k6eAd1
poprvé	poprvé	k6eAd1
nacvičovalo	nacvičovat	k5eAaImAgNnS
ve	v	k7c6
Středozemním	středozemní	k2eAgNnSc6d1
moři	moře	k1gNnSc6
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
operace	operace	k1gFnPc1
Mariner	Marinero	k1gNnPc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
se	se	k3xPyFc4
účastnilo	účastnit	k5eAaImAgNnS
300	#num#	k4
lodí	loď	k1gFnPc2
a	a	k8xC
1000	#num#	k4
letadel	letadlo	k1gNnPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
operace	operace	k1gFnSc1
Italic	Italice	k1gFnPc2
Weld	Welda	k1gFnPc2
probíhající	probíhající	k2eAgFnSc1d1
v	v	k7c6
severní	severní	k2eAgFnSc6d1
Itálii	Itálie	k1gFnSc6
<g/>
,	,	kIx,
operace	operace	k1gFnSc1
Grand	grand	k1gMnSc1
Repulse	repulse	k1gFnSc1
v	v	k7c6
Německu	Německo	k1gNnSc6
nebo	nebo	k8xC
operace	operace	k1gFnSc1
Monte	Mont	k1gInSc5
Carlo	Carlo	k1gNnSc1
simulující	simulující	k2eAgFnPc1d1
podmínky	podmínka	k1gFnPc1
s	s	k7c7
atomovými	atomový	k2eAgFnPc7d1
zbraněmi	zbraň	k1gFnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1952	#num#	k4
se	se	k3xPyFc4
členy	člen	k1gInPc7
NATO	nato	k6eAd1
staly	stát	k5eAaPmAgInP
Řecko	Řecko	k1gNnSc4
a	a	k8xC
Turecko	Turecko	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někdy	někdy	k6eAd1
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
započalo	započnout	k5eAaPmAgNnS
budování	budování	k1gNnSc1
sítě	síť	k1gFnSc2
Gladio	Gladio	k1gNnSc1
<g/>
,	,	kIx,
evropského	evropský	k2eAgInSc2d1
protikomunistického	protikomunistický	k2eAgInSc2d1
odboje	odboj	k1gInSc2
organizovaného	organizovaný	k2eAgInSc2d1
Severoatlantickou	severoatlantický	k2eAgFnSc7d1
aliancí	aliance	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1953	#num#	k4
oznámil	oznámit	k5eAaPmAgInS
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
se	se	k3xPyFc4
měl	mít	k5eAaImAgInS
připojit	připojit	k5eAaPmF
k	k	k7c3
NATO	NATO	kA
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
zachoval	zachovat	k5eAaPmAgInS
mír	mír	k1gInSc1
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Členské	členský	k2eAgFnPc1d1
země	zem	k1gFnPc1
se	se	k3xPyFc4
ale	ale	k9
bály	bát	k5eAaImAgFnP
<g/>
,	,	kIx,
že	že	k8xS
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
chce	chtít	k5eAaImIp3nS
Alianci	aliance	k1gFnSc4
oslabit	oslabit	k5eAaPmF
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k8xC,k8xS
návrh	návrh	k1gInSc4
zamítly	zamítnout	k5eAaPmAgInP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
Přijetí	přijetí	k1gNnSc1
Západního	západní	k2eAgNnSc2d1
Německa	Německo	k1gNnSc2
do	do	k7c2
Aliance	aliance	k1gFnSc2
9	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1955	#num#	k4
popsal	popsat	k5eAaPmAgMnS
Halvard	Halvard	k1gMnSc1
Lange	Lang	k1gInSc2
<g/>
,	,	kIx,
tehdejší	tehdejší	k2eAgMnSc1d1
ministr	ministr	k1gMnSc1
zahraničí	zahraničí	k1gNnSc2
Norska	Norsko	k1gNnSc2
<g/>
,	,	kIx,
za	za	k7c4
„	„	k?
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
rozhodující	rozhodující	k2eAgInSc4d1
zlomový	zlomový	k2eAgInSc4d1
bod	bod	k1gInSc4
v	v	k7c6
dějinách	dějiny	k1gFnPc6
našeho	náš	k3xOp1gInSc2
kontinentu	kontinent	k1gInSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
Odpovědí	odpověď	k1gFnSc7
Sovětského	sovětský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
na	na	k7c4
tuto	tento	k3xDgFnSc4
událost	událost	k1gFnSc4
bylo	být	k5eAaImAgNnS
vytvoření	vytvoření	k1gNnSc4
Varšavské	varšavský	k2eAgFnSc2d1
smlouvy	smlouva	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
14	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1955	#num#	k4
podepsaly	podepsat	k5eAaPmAgFnP
Sovětský	sovětský	k2eAgInSc4d1
svaz	svaz	k1gInSc4
<g/>
,	,	kIx,
Maďarsko	Maďarsko	k1gNnSc1
<g/>
,	,	kIx,
Československo	Československo	k1gNnSc1
<g/>
,	,	kIx,
Polsko	Polsko	k1gNnSc1
<g/>
,	,	kIx,
Bulharsko	Bulharsko	k1gNnSc1
<g/>
,	,	kIx,
Rumunsko	Rumunsko	k1gNnSc1
<g/>
,	,	kIx,
Albánie	Albánie	k1gFnSc1
a	a	k8xC
Německá	německý	k2eAgFnSc1d1
demokratická	demokratický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
(	(	kIx(
<g/>
NDR	NDR	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
druhé	druhý	k4xOgFnSc6
polovině	polovina	k1gFnSc6
roku	rok	k1gInSc2
1957	#num#	k4
NATO	NATO	kA
uskutečnilo	uskutečnit	k5eAaPmAgNnS
několik	několik	k4yIc1
velkých	velký	k2eAgNnPc2d1
vojenských	vojenský	k2eAgNnPc2d1
cvičení	cvičení	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Operací	operace	k1gFnPc2
Counter	Counter	k1gMnSc1
Punch	Punch	k1gMnSc1
<g/>
,	,	kIx,
Strikeback	Strikeback	k1gMnSc1
a	a	k8xC
Deep	Deep	k1gMnSc1
Water	Watra	k1gFnPc2
se	se	k3xPyFc4
účastnilo	účastnit	k5eAaImAgNnS
více	hodně	k6eAd2
než	než	k8xS
250	#num#	k4
000	#num#	k4
vojáků	voják	k1gMnPc2
<g/>
,	,	kIx,
300	#num#	k4
lodí	loď	k1gFnPc2
a	a	k8xC
1500	#num#	k4
letadel	letadlo	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odstoupení	odstoupení	k1gNnSc1
Francie	Francie	k1gFnSc2
z	z	k7c2
vojenských	vojenský	k2eAgFnPc2d1
struktur	struktura	k1gFnPc2
</s>
<s>
Mapa	mapa	k1gFnSc1
zobrazující	zobrazující	k2eAgFnSc2d1
letecké	letecký	k2eAgFnSc2d1
základny	základna	k1gFnSc2
NATO	NATO	kA
ve	v	k7c6
Francii	Francie	k1gFnSc6
před	před	k7c7
jejím	její	k3xOp3gInSc7
odchodem	odchod	k1gInSc7
z	z	k7c2
Aliance	aliance	k1gFnSc2
roku	rok	k1gInSc2
1966	#num#	k4
</s>
<s>
Sjednocenost	sjednocenost	k1gFnSc1
NATO	NATO	kA
byla	být	k5eAaImAgFnS
narušena	narušit	k5eAaPmNgFnS
během	během	k7c2
vlády	vláda	k1gFnSc2
francouzského	francouzský	k2eAgMnSc2d1
prezidenta	prezident	k1gMnSc2
Charlese	Charles	k1gMnSc2
de	de	k?
Gaulla	Gaull	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
do	do	k7c2
úřadu	úřad	k1gInSc2
nastoupil	nastoupit	k5eAaPmAgMnS
roku	rok	k1gInSc2
1959	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
protestoval	protestovat	k5eAaBmAgMnS
proti	proti	k7c3
silnému	silný	k2eAgInSc3d1
vlivu	vliv	k1gInSc3
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
na	na	k7c4
Alianci	aliance	k1gFnSc4
a	a	k8xC
blízkým	blízký	k2eAgInPc3d1
vztahům	vztah	k1gInPc3
mezi	mezi	k7c7
Spojenými	spojený	k2eAgInPc7d1
státy	stát	k1gInPc7
a	a	k8xC
Spojeným	spojený	k2eAgNnSc7d1
královstvím	království	k1gNnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
17	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1958	#num#	k4
poslal	poslat	k5eAaPmAgMnS
prezidentovi	prezident	k1gMnSc3
Dwightu	Dwight	k1gMnSc3
D.	D.	kA
Eisenhowerovi	Eisenhower	k1gMnSc3
a	a	k8xC
britskému	britský	k2eAgMnSc3d1
premiérovi	premiér	k1gMnSc3
Haroldu	Harold	k1gMnSc3
Macmillanovi	Macmillan	k1gMnSc3
memorandum	memorandum	k1gNnSc4
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yRgInSc6,k3yIgInSc6,k3yQgInSc6
žádal	žádat	k5eAaImAgMnS
vyzdvižení	vyzdvižení	k1gNnSc1
Francie	Francie	k1gFnSc2
na	na	k7c4
stejnou	stejný	k2eAgFnSc4d1
pozici	pozice	k1gFnSc4
<g/>
,	,	kIx,
jakou	jaký	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
mají	mít	k5eAaImIp3nP
USA	USA	kA
a	a	k8xC
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
<g/>
,	,	kIx,
a	a	k8xC
rozšíření	rozšíření	k1gNnSc1
působnosti	působnost	k1gFnSc2
NATO	NATO	kA
na	na	k7c6
oblasti	oblast	k1gFnSc6
jako	jako	k8xS,k8xC
Francouzské	francouzský	k2eAgNnSc1d1
Alžírsko	Alžírsko	k1gNnSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
Francie	Francie	k1gFnSc1
potřebovala	potřebovat	k5eAaImAgFnS
pomoci	pomoct	k5eAaPmF
v	v	k7c6
boji	boj	k1gInSc6
proti	proti	k7c3
povstalcům	povstalec	k1gMnPc3
v	v	k7c6
alžírské	alžírský	k2eAgFnSc6d1
válce	válka	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odpověď	odpověď	k1gFnSc1
na	na	k7c4
memorandum	memorandum	k1gNnSc4
de	de	k?
Gaulla	Gaulla	k1gFnSc1
neuspokojila	uspokojit	k5eNaPmAgFnS
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k6eAd1
začal	začít	k5eAaPmAgInS
pro	pro	k7c4
Francii	Francie	k1gFnSc4
vytvářet	vytvářet	k5eAaImF
nezávislou	závislý	k2eNgFnSc4d1
obrannou	obranný	k2eAgFnSc4d1
sílu	síla	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chtěl	chtít	k5eAaImAgMnS
také	také	k9
Francii	Francie	k1gFnSc4
poskytnout	poskytnout	k5eAaPmF
možnost	možnost	k1gFnSc4
uzavřít	uzavřít	k5eAaPmF
mír	mír	k1gInSc4
s	s	k7c7
Východním	východní	k2eAgInSc7d1
blokem	blok	k1gInSc7
<g/>
,	,	kIx,
místo	místo	k1gNnSc4
aby	aby	kYmCp3nS
byla	být	k5eAaImAgFnS
zatažena	zatáhnout	k5eAaPmNgFnS
do	do	k7c2
globální	globální	k2eAgFnSc2d1
války	válka	k1gFnSc2
mezi	mezi	k7c7
NATO	NATO	kA
a	a	k8xC
státy	stát	k1gInPc1
Varšavské	varšavský	k2eAgFnSc2d1
smlouvy	smlouva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
březnu	březen	k1gInSc6
1959	#num#	k4
Francie	Francie	k1gFnSc1
stáhla	stáhnout	k5eAaPmAgFnS
své	svůj	k3xOyFgFnPc4
první	první	k4xOgFnPc4
jednotky	jednotka	k1gFnPc4
z	z	k7c2
velení	velení	k1gNnSc2
NATO	NATO	kA
<g/>
;	;	kIx,
v	v	k7c6
červnu	červen	k1gInSc6
de	de	k?
Gaulle	Gaull	k1gMnSc4
zakázal	zakázat	k5eAaPmAgInS
umisťovat	umisťovat	k5eAaImF
cizí	cizí	k2eAgFnPc4d1
jaderné	jaderný	k2eAgFnPc4d1
zbraně	zbraň	k1gFnPc4
na	na	k7c4
francouzské	francouzský	k2eAgNnSc4d1
území	území	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1966	#num#	k4
byly	být	k5eAaImAgInP
z	z	k7c2
vojenských	vojenský	k2eAgFnPc2d1
struktur	struktura	k1gFnPc2
NATO	nato	k6eAd1
staženy	stažen	k2eAgFnPc1d1
všechny	všechen	k3xTgFnPc1
francouzské	francouzský	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
a	a	k8xC
Francie	Francie	k1gFnSc1
vykázala	vykázat	k5eAaPmAgFnS
všechny	všechen	k3xTgFnPc4
cizí	cizí	k2eAgFnPc4d1
jednotky	jednotka	k1gFnPc4
ze	z	k7c2
svého	svůj	k3xOyFgNnSc2
území	území	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
Protože	protože	k8xS
předtím	předtím	k6eAd1
bylo	být	k5eAaImAgNnS
Vrchní	vrchní	k2eAgNnSc1d1
velitelství	velitelství	k1gNnSc1
spojeneckých	spojenecký	k2eAgFnPc2d1
sil	síla	k1gFnPc2
v	v	k7c6
Evropě	Evropa	k1gFnSc6
(	(	kIx(
<g/>
SHAPE	SHAPE	kA
<g/>
)	)	kIx)
umístěno	umístit	k5eAaPmNgNnS
poblíž	poblíž	k6eAd1
Paříže	Paříž	k1gFnSc2
<g/>
,	,	kIx,
muselo	muset	k5eAaImAgNnS
se	se	k3xPyFc4
přesunout	přesunout	k5eAaPmF
do	do	k7c2
Belgie	Belgie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Francie	Francie	k1gFnSc1
zůstala	zůstat	k5eAaPmAgFnS
členem	člen	k1gMnSc7
Aliance	aliance	k1gFnSc2
a	a	k8xC
zavázala	zavázat	k5eAaPmAgFnS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
případě	případ	k1gInSc6
útoku	útok	k1gInSc2
komunistů	komunista	k1gMnPc2
použije	použít	k5eAaPmIp3nS
na	na	k7c4
obranu	obrana	k1gFnSc4
Evropy	Evropa	k1gFnSc2
své	svůj	k3xOyFgFnSc2
vlastní	vlastní	k2eAgFnSc2d1
jednotky	jednotka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
francouzskými	francouzský	k2eAgMnPc7d1
a	a	k8xC
americkými	americký	k2eAgMnPc7d1
státními	státní	k2eAgMnPc7d1
úředníky	úředník	k1gMnPc7
byly	být	k5eAaImAgFnP
uzavřeny	uzavřen	k2eAgFnPc4d1
tajné	tajný	k2eAgFnPc4d1
dohody	dohoda	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
popisovaly	popisovat	k5eAaImAgFnP
návrat	návrat	k1gInSc4
francouzských	francouzský	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
do	do	k7c2
struktur	struktura	k1gFnPc2
NATO	NATO	kA
v	v	k7c6
případě	případ	k1gInSc6
<g/>
,	,	kIx,
že	že	k8xS
vypukne	vypuknout	k5eAaPmIp3nS
válka	válka	k1gFnSc1
mezí	mez	k1gFnPc2
Východem	východ	k1gInSc7
a	a	k8xC
Západem	západ	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Détente	détente	k2eAgFnSc1d1
a	a	k8xC
eskalace	eskalace	k1gFnSc1
konfliktů	konflikt	k1gInPc2
</s>
<s>
Politika	politika	k1gFnSc1
détente	détente	k2eAgFnSc1d1
vedla	vést	k5eAaImAgFnS
k	k	k7c3
několika	několik	k4yIc3
setkáním	setkání	k1gNnPc3
mezi	mezi	k7c7
vůdci	vůdce	k1gMnPc1
států	stát	k1gInPc2
NATO	NATO	kA
a	a	k8xC
Varšavské	varšavský	k2eAgFnSc2d1
smlouvy	smlouva	k1gFnSc2
</s>
<s>
Po	po	k7c6
druhé	druhý	k4xOgFnSc6
berlínské	berlínský	k2eAgFnSc6d1
krizi	krize	k1gFnSc6
a	a	k8xC
Karibské	karibský	k2eAgFnSc6d1
krizi	krize	k1gFnSc6
si	se	k3xPyFc3
obě	dva	k4xCgFnPc1
strany	strana	k1gFnPc1
studené	studený	k2eAgFnSc2d1
války	válka	k1gFnSc2
uvědomily	uvědomit	k5eAaPmAgFnP
nutnost	nutnost	k1gFnSc4
institucionalizovat	institucionalizovat	k5eAaImF
proces	proces	k1gInSc4
uvolňování	uvolňování	k1gNnSc2
napětí	napětí	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1967	#num#	k4
NATO	nato	k6eAd1
přijalo	přijmout	k5eAaPmAgNnS
strategii	strategie	k1gFnSc4
vytyčující	vytyčující	k2eAgFnPc4d1
dvě	dva	k4xCgFnPc4
funkce	funkce	k1gFnPc4
Aliance	aliance	k1gFnSc2
<g/>
:	:	kIx,
udržet	udržet	k5eAaPmF
vojenskou	vojenský	k2eAgFnSc4d1
bezpečnost	bezpečnost	k1gFnSc4
a	a	k8xC
zároveň	zároveň	k6eAd1
prosazovat	prosazovat	k5eAaImF
politiku	politika	k1gFnSc4
détente	détente	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Touto	tento	k3xDgFnSc7
strategií	strategie	k1gFnSc7
se	se	k3xPyFc4
NATO	nato	k6eAd1
řídilo	řídit	k5eAaImAgNnS
až	až	k9
do	do	k7c2
konce	konec	k1gInSc2
studené	studený	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1964	#num#	k4
nastoupil	nastoupit	k5eAaPmAgMnS
na	na	k7c4
místo	místo	k1gNnSc4
generálního	generální	k2eAgMnSc2d1
tajemníka	tajemník	k1gMnSc2
NATO	NATO	kA
italský	italský	k2eAgMnSc1d1
politik	politik	k1gMnSc1
Manlio	Manlio	k1gMnSc1
Brosio	Brosio	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednou	jednou	k6eAd1
z	z	k7c2
jeho	jeho	k3xOp3gFnPc2
prvních	první	k4xOgFnPc2
akcí	akce	k1gFnPc2
bylo	být	k5eAaImAgNnS
založení	založení	k1gNnSc1
Výboru	výbor	k1gInSc2
pro	pro	k7c4
obranné	obranný	k2eAgNnSc4d1
plánování	plánování	k1gNnSc4
<g/>
,	,	kIx,
jejímiž	jejíž	k3xOyRp3gMnPc7
podřízenými	podřízený	k1gMnPc7
orgány	orgán	k1gInPc7
byly	být	k5eAaImAgInP
v	v	k7c6
roce	rok	k1gInSc6
1966	#num#	k4
určeny	určen	k2eAgInPc1d1
již	již	k6eAd1
existující	existující	k2eAgInSc1d1
Vojenský	vojenský	k2eAgInSc1d1
výbor	výbor	k1gInSc1
a	a	k8xC
nově	nově	k6eAd1
vzniklá	vzniklý	k2eAgFnSc1d1
Skupina	skupina	k1gFnSc1
pro	pro	k7c4
jaderné	jaderný	k2eAgNnSc4d1
plánování	plánování	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
Všechny	všechen	k3xTgFnPc4
rozhodovací	rozhodovací	k2eAgFnPc4d1
pravomoci	pravomoc	k1gFnPc4
ve	v	k7c6
vojenských	vojenský	k2eAgFnPc6d1
záležitostech	záležitost	k1gFnPc6
byly	být	k5eAaImAgFnP
převedeny	převést	k5eAaPmNgFnP
na	na	k7c4
Výbor	výbor	k1gInSc4
pro	pro	k7c4
obranné	obranný	k2eAgNnSc4d1
plánování	plánování	k1gNnSc4
a	a	k8xC
tak	tak	k6eAd1
bylo	být	k5eAaImAgNnS
Francii	Francie	k1gFnSc4
umožněno	umožnit	k5eAaPmNgNnS
setrvat	setrvat	k5eAaPmF
v	v	k7c6
politické	politický	k2eAgFnSc6d1
struktuře	struktura	k1gFnSc6
Aliance	aliance	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1969	#num#	k4
učinily	učinit	k5eAaImAgInP,k5eAaPmAgInP
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
a	a	k8xC
Západní	západní	k2eAgNnSc1d1
Německo	Německo	k1gNnSc1
zásadní	zásadní	k2eAgFnSc2d1
změny	změna	k1gFnSc2
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
zahraniční	zahraniční	k2eAgFnSc6d1
politice	politika	k1gFnSc6
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
umožňovaly	umožňovat	k5eAaImAgFnP
lépe	dobře	k6eAd2
prosazovat	prosazovat	k5eAaImF
détente	détente	k2eAgMnPc4d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
Richard	Richard	k1gMnSc1
Nixon	Nixon	k1gMnSc1
zavedl	zavést	k5eAaPmAgMnS
Nixonovu	Nixonův	k2eAgFnSc4d1
doktrínu	doktrína	k1gFnSc4
a	a	k8xC
v	v	k7c6
Západním	západní	k2eAgNnSc6d1
Německu	Německo	k1gNnSc6
začala	začít	k5eAaPmAgFnS
platit	platit	k5eAaImF
Ostpolitik	Ostpolitik	k1gMnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
nahradila	nahradit	k5eAaPmAgFnS
Hallsteinovu	Hallsteinův	k2eAgFnSc4d1
doktrínu	doktrína	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
stanovovala	stanovovat	k5eAaImAgFnS
<g/>
,	,	kIx,
že	že	k8xS
Západní	západní	k2eAgNnSc1d1
Německo	Německo	k1gNnSc1
nebude	být	k5eNaImBp3nS
navazovat	navazovat	k5eAaImF
ani	ani	k8xC
udržovat	udržovat	k5eAaImF
diplomatické	diplomatický	k2eAgInPc4d1
vztahy	vztah	k1gInPc4
s	s	k7c7
žádným	žádný	k3yNgInSc7
státem	stát	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
naváže	navázat	k5eAaPmIp3nS
diplomatické	diplomatický	k2eAgInPc4d1
vztahy	vztah	k1gInPc4
s	s	k7c7
NDR	NDR	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Americký	americký	k2eAgMnSc1d1
viceprezident	viceprezident	k1gMnSc1
Bush	Bush	k1gMnSc1
na	na	k7c4
inspekci	inspekce	k1gFnSc4
vojsk	vojsko	k1gNnPc2
NATO	NATO	kA
v	v	k7c6
Západním	západní	k2eAgNnSc6d1
Německu	Německo	k1gNnSc6
v	v	k7c6
roce	rok	k1gInSc6
1983	#num#	k4
</s>
<s>
V	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
byly	být	k5eAaImAgFnP
učiněny	učiněn	k2eAgFnPc1d1
snahy	snaha	k1gFnPc1
o	o	k7c4
jaderné	jaderný	k2eAgNnSc4d1
odzbrojování	odzbrojování	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
březnu	březen	k1gInSc6
1970	#num#	k4
vešla	vejít	k5eAaPmAgFnS
v	v	k7c4
platnost	platnost	k1gFnSc4
Smlouva	smlouva	k1gFnSc1
o	o	k7c6
nešíření	nešíření	k1gNnSc6
jaderných	jaderný	k2eAgFnPc2d1
zbraní	zbraň	k1gFnPc2
a	a	k8xC
v	v	k7c6
květnu	květen	k1gInSc6
1972	#num#	k4
Richard	Richard	k1gMnSc1
Nixon	Nixon	k1gMnSc1
a	a	k8xC
Leonid	Leonid	k1gInSc1
Iljič	Iljič	k1gMnSc1
Brežněv	Brežněv	k1gMnSc1
podepsali	podepsat	k5eAaPmAgMnP
smlouvy	smlouva	k1gFnPc4
SALT	salto	k1gNnPc2
I	i	k8xC
a	a	k8xC
ABM	ABM	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1979	#num#	k4
pak	pak	k9
byla	být	k5eAaImAgFnS
Brežněvem	Brežněv	k1gInSc7
a	a	k8xC
novým	nový	k2eAgMnSc7d1
americkým	americký	k2eAgMnSc7d1
prezidentem	prezident	k1gMnSc7
Jimmym	Jimmymum	k1gNnPc2
Carterem	Carter	k1gInSc7
podepsána	podepsán	k2eAgFnSc1d1
smlouva	smlouva	k1gFnSc1
SALT	salto	k1gNnPc2
II	II	kA
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
přestala	přestat	k5eAaPmAgFnS
platit	platit	k5eAaImF
SALT	salto	k1gNnPc2
I.	I.	kA
SALT	salto	k1gNnPc2
II	II	kA
ale	ale	k8xC
nikdy	nikdy	k6eAd1
nevstoupila	vstoupit	k5eNaPmAgFnS
v	v	k7c4
platnost	platnost	k1gFnSc4
<g/>
,	,	kIx,
protože	protože	k8xS
její	její	k3xOp3gFnSc4
ratifikaci	ratifikace	k1gFnSc4
odmítl	odmítnout	k5eAaPmAgMnS
Senát	senát	k1gInSc4
USA	USA	kA
kvůli	kvůli	k7c3
sovětské	sovětský	k2eAgFnSc3d1
agresi	agrese	k1gFnSc3
v	v	k7c6
Afghánistánu	Afghánistán	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gInPc1
limity	limit	k1gInPc1
ale	ale	k9
oba	dva	k4xCgInPc1
státy	stát	k1gInPc1
vcelku	vcelku	k6eAd1
dodržovaly	dodržovat	k5eAaImAgInP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1976	#num#	k4
začal	začít	k5eAaPmAgInS
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
v	v	k7c6
Evropě	Evropa	k1gFnSc6
rozmisťovat	rozmisťovat	k5eAaImF
jaderné	jaderný	k2eAgInPc4d1
systémy	systém	k1gInPc4
typu	typ	k1gInSc2
SS-	SS-	k1gFnSc2
<g/>
20	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
Odpovědí	odpověď	k1gFnSc7
NATO	NATO	kA
bylo	být	k5eAaImAgNnS
přijetí	přijetí	k1gNnSc1
tzv.	tzv.	kA
dvoukolejného	dvoukolejný	k2eAgNnSc2d1
rozhodnutí	rozhodnutí	k1gNnSc2
z	z	k7c2
roku	rok	k1gInSc2
1979	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aliance	aliance	k1gFnSc1
se	se	k3xPyFc4
rozhodla	rozhodnout	k5eAaPmAgFnS
v	v	k7c6
Evropě	Evropa	k1gFnSc6
rozmístit	rozmístit	k5eAaPmF
téměř	téměř	k6eAd1
600	#num#	k4
odpalovacích	odpalovací	k2eAgNnPc2d1
zařízení	zařízení	k1gNnPc2
řízených	řízený	k2eAgFnPc2d1
střel	střela	k1gFnPc2
a	a	k8xC
zároveň	zároveň	k6eAd1
vyslovila	vyslovit	k5eAaPmAgFnS
podporu	podpora	k1gFnSc4
kontroly	kontrola	k1gFnSc2
zbrojení	zbrojení	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
vítězství	vítězství	k1gNnSc6
v	v	k7c6
prezidentských	prezidentský	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
roku	rok	k1gInSc2
1980	#num#	k4
vyhlásil	vyhlásit	k5eAaPmAgMnS
Ronald	Ronald	k1gMnSc1
Reagan	Reagan	k1gMnSc1
novou	nový	k2eAgFnSc4d1
Reaganovu	Reaganův	k2eAgFnSc4d1
doktrínu	doktrína	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
určila	určit	k5eAaPmAgFnS
nový	nový	k2eAgInSc4d1
směr	směr	k1gInSc4
politiky	politika	k1gFnSc2
Západu	západ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reagan	Reagan	k1gMnSc1
požadoval	požadovat	k5eAaImAgMnS
celosvětovou	celosvětový	k2eAgFnSc4d1
kampaň	kampaň	k1gFnSc4
za	za	k7c4
demokracii	demokracie	k1gFnSc4
<g/>
,	,	kIx,
pouhá	pouhý	k2eAgFnSc1d1
obrana	obrana	k1gFnSc1
proti	proti	k7c3
komunismu	komunismus	k1gInSc3
mu	on	k3xPp3gMnSc3
nepřipadala	připadat	k5eNaImAgFnS
dostatečná	dostatečný	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1982	#num#	k4
se	s	k7c7
členem	člen	k1gInSc7
Aliance	aliance	k1gFnSc2
stalo	stát	k5eAaPmAgNnS
Španělsko	Španělsko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
začalo	začít	k5eAaPmAgNnS
NATO	nato	k6eAd1
tlačit	tlačit	k5eAaImF
na	na	k7c4
Sovětský	sovětský	k2eAgInSc4d1
svaz	svaz	k1gInSc4
ohledně	ohledně	k7c2
dodržování	dodržování	k1gNnSc2
lidských	lidský	k2eAgNnPc2d1
práv	právo	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
tím	ten	k3xDgMnSc7
Reagan	Reagan	k1gMnSc1
na	na	k7c6
začátku	začátek	k1gInSc6
roku	rok	k1gInSc2
1983	#num#	k4
označil	označit	k5eAaPmAgInS
komunistický	komunistický	k2eAgInSc1d1
svět	svět	k1gInSc1
za	za	k7c4
„	„	k?
<g/>
říši	říš	k1gFnSc3
zla	zlo	k1gNnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
velká	velký	k2eAgFnSc1d1
část	část	k1gFnSc1
západních	západní	k2eAgNnPc2d1
médií	médium	k1gNnPc2
považovala	považovat	k5eAaImAgFnS
za	za	k7c2
přehnané	přehnaný	k2eAgFnSc2d1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
1	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1983	#num#	k4
sovětská	sovětský	k2eAgFnSc1d1
protivzdušná	protivzdušný	k2eAgFnSc1d1
obrana	obrana	k1gFnSc1
sestřelila	sestřelit	k5eAaPmAgFnS
bez	bez	k7c2
varování	varování	k1gNnSc2
jihokorejské	jihokorejský	k2eAgNnSc4d1
civilní	civilní	k2eAgNnSc4d1
letadlo	letadlo	k1gNnSc4
<g/>
;	;	kIx,
zemřelo	zemřít	k5eAaPmAgNnS
250	#num#	k4
lidí	člověk	k1gMnPc2
a	a	k8xC
tato	tento	k3xDgFnSc1
tragédie	tragédie	k1gFnSc1
podpořila	podpořit	k5eAaPmAgFnS
Reaganovu	Reaganův	k2eAgFnSc4d1
politiku	politika	k1gFnSc4
v	v	k7c6
očích	oko	k1gNnPc6
západní	západní	k2eAgFnSc2d1
veřejnosti	veřejnost	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
Konec	konec	k1gInSc1
studené	studený	k2eAgFnSc2d1
války	válka	k1gFnSc2
se	se	k3xPyFc4
pak	pak	k6eAd1
táhl	táhnout	k5eAaImAgMnS
ve	v	k7c6
znaku	znak	k1gInSc6
vzájemných	vzájemný	k2eAgFnPc2d1
dohod	dohoda	k1gFnPc2
o	o	k7c6
odzbrojování	odzbrojování	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
První	první	k4xOgFnSc2
schůze	schůze	k1gFnSc2
Reagana	Reagan	k1gMnSc2
a	a	k8xC
nového	nový	k2eAgMnSc2d1
sovětského	sovětský	k2eAgMnSc2d1
vůdce	vůdce	k1gMnSc2
Michaila	Michail	k1gMnSc2
Gorbačova	Gorbačův	k2eAgFnSc1d1
proběhla	proběhnout	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1985	#num#	k4
v	v	k7c6
Ženevě	Ženeva	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
druhé	druhý	k4xOgFnSc6
schůzi	schůze	k1gFnSc6
v	v	k7c6
Reykjavíku	Reykjavík	k1gInSc6
se	se	k3xPyFc4
Gorbačov	Gorbačov	k1gInSc1
snažil	snažit	k5eAaImAgInS
Reagana	Reagan	k1gMnSc4
přesvědčit	přesvědčit	k5eAaPmF
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
ustoupil	ustoupit	k5eAaPmAgInS
od	od	k7c2
jím	jíst	k5eAaImIp1nS
plánované	plánovaný	k2eAgFnSc2d1
Strategické	strategický	k2eAgFnSc2d1
obranné	obranný	k2eAgFnSc2d1
iniciativy	iniciativa	k1gFnSc2
<g/>
,	,	kIx,
což	což	k3yQnSc4,k3yRnSc4
on	on	k3xPp3gMnSc1
odmítl	odmítnout	k5eAaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
třetím	třetí	k4xOgInSc6
setkání	setkání	k1gNnSc6
ve	v	k7c6
Washingtonu	Washington	k1gInSc6
pak	pak	k6eAd1
Reagan	Reagan	k1gMnSc1
a	a	k8xC
Gorbačov	Gorbačov	k1gInSc4
podepsali	podepsat	k5eAaPmAgMnP
Smlouvu	smlouva	k1gFnSc4
o	o	k7c6
likvidaci	likvidace	k1gFnSc6
raket	raketa	k1gFnPc2
středního	střední	k2eAgInSc2d1
a	a	k8xC
krátkého	krátký	k2eAgInSc2d1
doletu	dolet	k1gInSc2
(	(	kIx(
<g/>
INF	INF	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
44	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
studené	studený	k2eAgFnSc6d1
válce	válka	k1gFnSc6
</s>
<s>
K	k	k7c3
NATO	NATO	kA
se	se	k3xPyFc4
od	od	k7c2
znovusjednocení	znovusjednocení	k1gNnSc2
Německa	Německo	k1gNnSc2
připojilo	připojit	k5eAaPmAgNnS
12	#num#	k4
nových	nový	k2eAgInPc2d1
členů	člen	k1gInPc2
</s>
<s>
Zánik	zánik	k1gInSc1
Varšavské	varšavský	k2eAgFnSc2d1
smlouvy	smlouva	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1991	#num#	k4
odstranil	odstranit	k5eAaPmAgMnS
de	de	k?
facto	facto	k1gNnSc4
hlavního	hlavní	k2eAgMnSc2d1
protivníka	protivník	k1gMnSc2
NATO	NATO	kA
a	a	k8xC
podnítil	podnítit	k5eAaPmAgMnS
nové	nový	k2eAgFnPc4d1
debaty	debata	k1gFnPc4
o	o	k7c6
účelu	účel	k1gInSc6
a	a	k8xC
povaze	povaha	k1gFnSc6
Aliance	aliance	k1gFnSc1
<g/>
.	.	kIx.
„	„	k?
<g/>
Oslovením	oslovení	k1gNnSc7
bývalých	bývalý	k2eAgMnPc2d1
nepřátel	nepřítel	k1gMnPc2
a	a	k8xC
návrhem	návrh	k1gInSc7
na	na	k7c6
spolupráci	spolupráce	k1gFnSc6
k	k	k7c3
udržování	udržování	k1gNnSc3
bezpečnosti	bezpečnost	k1gFnSc2
NATO	nato	k6eAd1
aktivně	aktivně	k6eAd1
přispělo	přispět	k5eAaPmAgNnS
k	k	k7c3
ukončení	ukončení	k1gNnSc3
rozdělení	rozdělení	k1gNnSc2
Evropy	Evropa	k1gFnSc2
na	na	k7c4
Východ	východ	k1gInSc4
a	a	k8xC
Západ	západ	k1gInSc4
<g/>
.	.	kIx.
<g/>
Tato	tento	k3xDgFnSc1
zásadní	zásadní	k2eAgFnSc1d1
<g />
.	.	kIx.
</s>
<s hack="1">
změna	změna	k1gFnSc1
v	v	k7c6
přístupu	přístup	k1gInSc6
byla	být	k5eAaImAgNnP
zakotvena	zakotvit	k5eAaPmNgNnP
v	v	k7c6
nové	nový	k2eAgFnSc6d1
strategické	strategický	k2eAgFnSc6d1
koncepci	koncepce	k1gFnSc6
NATO	NATO	kA
z	z	k7c2
listopadu	listopad	k1gInSc2
1991	#num#	k4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
přijala	přijmout	k5eAaPmAgFnS
širší	široký	k2eAgInSc4d2
bezpečnostní	bezpečnostní	k2eAgInSc4d1
rámec	rámec	k1gInSc4
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
období	období	k1gNnSc2
devadesátých	devadesátý	k4xOgNnPc2
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
si	se	k3xPyFc3
Spojené	spojený	k2eAgInPc4d1
státy	stát	k1gInPc4
politicky	politicky	k6eAd1
udržovaly	udržovat	k5eAaImAgFnP
a	a	k8xC
ještě	ještě	k6eAd1
upevňovaly	upevňovat	k5eAaImAgFnP
výjimečnou	výjimečný	k2eAgFnSc4d1
moc	moc	k1gFnSc4
<g/>
,	,	kIx,
díky	dík	k1gInPc1
které	který	k3yQgMnPc4,k3yIgMnPc4,k3yRgMnPc4
mohly	moct	k5eAaImAgInP
diktovat	diktovat	k5eAaImF
podmínky	podmínka	k1gFnPc4
<g/>
,	,	kIx,
ale	ale	k8xC
nemohly	moct	k5eNaImAgFnP
nebrat	brat	k5eNaImF,k5eNaPmF
do	do	k7c2
úvahy	úvaha	k1gFnSc2
Evropu	Evropa	k1gFnSc4
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
Japonsko	Japonsko	k1gNnSc4
<g/>
,	,	kIx,
Čínu	Čína	k1gFnSc4
a	a	k8xC
Rusko	Rusko	k1gNnSc4
<g/>
.	.	kIx.
<g/>
Vojensky	vojensky	k6eAd1
byl	být	k5eAaImAgInS
rozdíl	rozdíl	k1gInSc1
mezi	mezi	k7c7
USA	USA	kA
a	a	k8xC
zbytkem	zbytek	k1gInSc7
světa	svět	k1gInSc2
velmi	velmi	k6eAd1
výrazný	výrazný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Američané	Američan	k1gMnPc1
si	se	k3xPyFc3
však	však	k9
nemohli	moct	k5eNaImAgMnP
myslet	myslet	k5eAaImF
<g/>
,	,	kIx,
že	že	k8xS
jsou	být	k5eAaImIp3nP
schopni	schopen	k2eAgMnPc1d1
vyřešit	vyřešit	k5eAaPmF
všechny	všechen	k3xTgInPc4
problémy	problém	k1gInPc4
světa	svět	k1gInSc2
a	a	k8xC
s	s	k7c7
využitím	využití	k1gNnSc7
pouze	pouze	k6eAd1
svých	svůj	k3xOyFgMnPc2
vojáků	voják	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Již	již	k6eAd1
během	během	k7c2
desetiletí	desetiletí	k1gNnSc2
studené	studený	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
,	,	kIx,
především	především	k6eAd1
pak	pak	k6eAd1
v	v	k7c6
sedmdesátých	sedmdesátý	k4xOgNnPc6
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
se	se	k3xPyFc4
evropské	evropský	k2eAgFnSc2d1
<g/>
,	,	kIx,
latinskoamerické	latinskoamerický	k2eAgFnSc2d1
a	a	k8xC
blízkovýchodní	blízkovýchodní	k2eAgFnSc2d1
vojenské	vojenský	k2eAgFnSc2d1
politické	politický	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
kvůli	kvůli	k7c3
dosažení	dosažení	k1gNnSc3
svých	svůj	k3xOyFgInPc2
cílů	cíl	k1gInPc2
uchýlily	uchýlit	k5eAaPmAgFnP
k	k	k7c3
ozbrojenému	ozbrojený	k2eAgInSc3d1
boji	boj	k1gInSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
46	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
První	první	k4xOgNnSc1
rozšíření	rozšíření	k1gNnSc1
NATO	NATO	kA
po	po	k7c6
studené	studený	k2eAgFnSc6d1
válce	válka	k1gFnSc6
přišlo	přijít	k5eAaPmAgNnS
se	s	k7c7
znovusjednocením	znovusjednocení	k1gNnSc7
Německa	Německo	k1gNnSc2
3	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1990	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
bývalá	bývalý	k2eAgFnSc1d1
NDR	NDR	kA
připojila	připojit	k5eAaPmAgFnS
ke	k	k7c3
Spolkové	spolkový	k2eAgFnSc3d1
republice	republika	k1gFnSc3
Německo	Německo	k1gNnSc4
a	a	k8xC
Alianci	aliance	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
bylo	být	k5eAaImAgNnS
dohodnuto	dohodnut	k2eAgNnSc1d1
Smlouvou	smlouva	k1gFnSc7
dva	dva	k4xCgInPc4
plus	plus	k6eAd1
čtyři	čtyři	k4xCgInPc4
dříve	dříve	k6eAd2
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aby	aby	kYmCp3nS
s	s	k7c7
tímto	tento	k3xDgInSc7
bodem	bod	k1gInSc7
smlouvy	smlouva	k1gFnSc2
souhlasil	souhlasit	k5eAaImAgInS
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
<g/>
,	,	kIx,
strany	strana	k1gFnPc1
se	se	k3xPyFc4
dohodly	dohodnout	k5eAaPmAgFnP
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
bývalé	bývalý	k2eAgFnSc6d1
NDR	NDR	kA
nebudou	být	k5eNaImBp3nP
umístěny	umístit	k5eAaPmNgFnP
cizí	cizí	k2eAgFnPc1d1
vojenské	vojenský	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
a	a	k8xC
jaderné	jaderný	k2eAgFnPc1d1
zbraně	zbraň	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stephen	Stephen	k1gInSc1
F.	F.	kA
Cohen	Cohen	k1gInSc1
v	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
prohlásil	prohlásit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
NATO	NATO	kA
se	se	k3xPyFc4
tehdy	tehdy	k6eAd1
zavázalo	zavázat	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
nebude	být	k5eNaImBp3nS
rozšiřovat	rozšiřovat	k5eAaImF
dále	daleko	k6eAd2
na	na	k7c4
východ	východ	k1gInSc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
ale	ale	k8xC
podle	podle	k7c2
Roberta	Robert	k1gMnSc2
Zoellicka	Zoellicko	k1gNnSc2
<g/>
,	,	kIx,
tehdejšího	tehdejší	k2eAgMnSc2d1
zástupce	zástupce	k1gMnSc2
ministra	ministr	k1gMnSc2
zahraničních	zahraniční	k2eAgFnPc2d1
věcí	věc	k1gFnPc2
USA	USA	kA
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
se	se	k3xPyFc4
účastnil	účastnit	k5eAaImAgInS
smlouvání	smlouvání	k1gNnPc4
o	o	k7c4
<g />
.	.	kIx.
</s>
<s hack="1">
podmínkách	podmínka	k1gFnPc6
Smlouvy	smlouva	k1gFnPc1
dva	dva	k4xCgMnPc1
plus	plus	k6eAd1
čtyři	čtyři	k4xCgMnPc1
<g/>
,	,	kIx,
žádný	žádný	k3yNgInSc1
takový	takový	k3xDgInSc1
závazek	závazek	k1gInSc1
neexistoval	existovat	k5eNaImAgInS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
48	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
květnu	květen	k1gInSc6
2008	#num#	k4
Michail	Michail	k1gMnSc1
Sergejevič	Sergejevič	k1gMnSc1
Gorbačov	Gorbačov	k1gInSc4
prohlásil	prohlásit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
„	„	k?
<g/>
Američané	Američan	k1gMnPc1
slíbili	slíbit	k5eAaPmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
NATO	NATO	kA
se	se	k3xPyFc4
nebude	být	k5eNaImBp3nS
rozšiřovat	rozšiřovat	k5eAaImF
dál	daleko	k6eAd2
než	než	k8xS
za	za	k7c4
hranice	hranice	k1gFnPc4
Německa	Německo	k1gNnSc2
po	po	k7c6
studené	studený	k2eAgFnSc6d1
válce	válka	k1gFnSc6
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
49	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Součástí	součást	k1gFnSc7
rekonstrukce	rekonstrukce	k1gFnSc2
NATO	NATO	kA
po	po	k7c6
studené	studený	k2eAgFnSc6d1
válce	válka	k1gFnSc6
byla	být	k5eAaImAgFnS
reorganizace	reorganizace	k1gFnSc1
její	její	k3xOp3gFnSc2
vojenské	vojenský	k2eAgFnSc2d1
struktury	struktura	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vznikly	vzniknout	k5eAaPmAgInP
některé	některý	k3yIgFnPc4
nové	nový	k2eAgFnPc4d1
složky	složka	k1gFnPc4
<g/>
,	,	kIx,
např.	např.	kA
Velitelství	velitelství	k1gNnSc1
spojeneckého	spojenecký	k2eAgInSc2d1
sboru	sbor	k1gInSc2
rychlé	rychlý	k2eAgFnSc2d1
reakce	reakce	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
byly	být	k5eAaImAgFnP
uzavřeny	uzavřít	k5eAaPmNgFnP
dohody	dohoda	k1gFnPc1
o	o	k7c4
redukci	redukce	k1gFnSc4
vojenských	vojenský	k2eAgFnPc2d1
sil	síla	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Smlouva	smlouva	k1gFnSc1
o	o	k7c6
konvenčních	konvenční	k2eAgFnPc6d1
ozbrojených	ozbrojený	k2eAgFnPc6d1
silách	síla	k1gFnPc6
v	v	k7c6
Evropě	Evropa	k1gFnSc6
(	(	kIx(
<g/>
CFE	CFE	kA
<g/>
)	)	kIx)
mezi	mezi	k7c7
NATO	NATO	kA
a	a	k8xC
státy	stát	k1gInPc1
Varšavské	varšavský	k2eAgFnSc2d1
smlouvy	smlouva	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
byla	být	k5eAaImAgFnS
uzavřena	uzavřít	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1990	#num#	k4
<g/>
,	,	kIx,
vyžadovala	vyžadovat	k5eAaImAgFnS
snížení	snížení	k1gNnSc3
počtu	počet	k1gInSc2
jednotek	jednotka	k1gFnPc2
na	na	k7c4
hodnoty	hodnota	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
nepřekračují	překračovat	k5eNaImIp3nP
určité	určitý	k2eAgFnPc4d1
hranice	hranice	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
50	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
období	období	k1gNnSc6
<g />
.	.	kIx.
</s>
<s hack="1">
postbipolárního	postbipolární	k2eAgInSc2d1
světa	svět	k1gInSc2
<g/>
,	,	kIx,
tj.	tj.	kA
období	období	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
začalo	začít	k5eAaPmAgNnS
v	v	k7c6
roce	rok	k1gInSc6
1991	#num#	k4
zánikem	zánik	k1gInSc7
jedné	jeden	k4xCgFnSc2
ze	z	k7c2
dvou	dva	k4xCgFnPc2
světových	světový	k2eAgFnPc2d1
mocností	mocnost	k1gFnPc2
<g/>
,	,	kIx,
tj.	tj.	kA
Sovětského	sovětský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
ukončením	ukončení	k1gNnSc7
období	období	k1gNnSc2
nazývaného	nazývaný	k2eAgNnSc2d1
studená	studený	k2eAgFnSc1d1
válka	válka	k1gFnSc1
<g/>
,	,	kIx,
zůstala	zůstat	k5eAaPmAgFnS
ze	z	k7c2
dvou	dva	k4xCgFnPc2
supervelmocí	supervelmoc	k1gFnPc2
pouze	pouze	k6eAd1
jedna	jeden	k4xCgFnSc1
<g/>
,	,	kIx,
stále	stále	k6eAd1
více	hodně	k6eAd2
vyzbrojovaná	vyzbrojovaný	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
51	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1999	#num#	k4
byl	být	k5eAaImAgInS
pak	pak	k6eAd1
vytvořen	vytvořit	k5eAaPmNgInS
adaptovaný	adaptovaný	k2eAgInSc1d1
text	text	k1gInSc1
smlouvy	smlouva	k1gFnSc2
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yRgInSc6,k3yIgInSc6,k3yQgInSc6
byla	být	k5eAaImAgFnS
odstraněna	odstraněn	k2eAgFnSc1d1
bloková	blokový	k2eAgFnSc1d1
architektura	architektura	k1gFnSc1
(	(	kIx(
<g/>
NATO	NATO	kA
vs	vs	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Varšavská	varšavský	k2eAgFnSc1d1
smlouva	smlouva	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Státy	stát	k1gInPc1
NATO	NATO	kA
ale	ale	k8xC
zatím	zatím	k6eAd1
adaptovaný	adaptovaný	k2eAgInSc4d1
text	text	k1gInSc4
neratifikovaly	ratifikovat	k5eNaBmAgFnP
<g/>
,	,	kIx,
protože	protože	k8xS
čekají	čekat	k5eAaImIp3nP
<g/>
,	,	kIx,
až	až	k8xS
Rusko	Rusko	k1gNnSc1
stáhne	stáhnout	k5eAaPmIp3nS
své	svůj	k3xOyFgFnPc4
jednotky	jednotka	k1gFnPc4
z	z	k7c2
Gruzie	Gruzie	k1gFnSc2
a	a	k8xC
Moldavska	Moldavsko	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
52	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1995	#num#	k4
se	se	k3xPyFc4
Francie	Francie	k1gFnSc1
navrátila	navrátit	k5eAaPmAgFnS
do	do	k7c2
Vojenského	vojenský	k2eAgInSc2d1
výboru	výbor	k1gInSc2
NATO	NATO	kA
a	a	k8xC
od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
se	se	k3xPyFc4
zvýšila	zvýšit	k5eAaPmAgFnS
její	její	k3xOp3gFnSc1
spolupráce	spolupráce	k1gFnSc1
s	s	k7c7
vojenskými	vojenský	k2eAgFnPc7d1
strukturami	struktura	k1gFnPc7
Aliance	aliance	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Politika	politika	k1gFnSc1
Nicolase	Nicolasa	k1gFnSc6
Sarkozyho	Sarkozy	k1gMnSc2
nakonec	nakonec	k6eAd1
vyústila	vyústit	k5eAaPmAgFnS
v	v	k7c4
návrat	návrat	k1gInSc4
Francie	Francie	k1gFnSc2
do	do	k7c2
vojenského	vojenský	k2eAgNnSc2d1
velení	velení	k1gNnSc2
NATO	NATO	kA
4	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2009	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1999	#num#	k4
se	s	k7c7
členskými	členský	k2eAgFnPc7d1
zeměmi	zem	k1gFnPc7
NATO	nato	k6eAd1
staly	stát	k5eAaPmAgFnP
Česko	Česko	k1gNnSc4
<g/>
,	,	kIx,
Maďarsko	Maďarsko	k1gNnSc1
a	a	k8xC
Polsko	Polsko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Donald	Donald	k1gMnSc1
Rumsfeld	Rumsfeld	k1gMnSc1
a	a	k8xC
Victoria	Victorium	k1gNnPc1
Nulandová	Nulandový	k2eAgNnPc1d1
na	na	k7c6
summitu	summit	k1gInSc6
NATO-Ukrajina	NATO-Ukrajino	k1gNnSc2
ve	v	k7c6
Vilniusu	Vilnius	k1gInSc6
v	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
</s>
<s>
Na	na	k7c6
pražském	pražský	k2eAgInSc6d1
summitu	summit	k1gInSc6
v	v	k7c6
roce	rok	k1gInSc6
2002	#num#	k4
NATO	NATO	kA
zrušilo	zrušit	k5eAaPmAgNnS
některé	některý	k3yIgFnPc4
staré	starý	k2eAgFnPc4d1
struktury	struktura	k1gFnPc4
a	a	k8xC
založila	založit	k5eAaPmAgFnS
nové	nový	k2eAgInPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byly	být	k5eAaImAgInP
vytvořeny	vytvořen	k2eAgInPc1d1
Síly	síl	k1gInPc1
rychlé	rychlý	k2eAgFnSc2d1
reakce	reakce	k1gFnSc2
NATO	NATO	kA
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
zrušeno	zrušit	k5eAaPmNgNnS
Vrchní	vrchní	k2eAgNnSc1d1
velitelství	velitelství	k1gNnSc1
spojeneckých	spojenecký	k2eAgFnPc2d1
sil	síla	k1gFnPc2
v	v	k7c6
Atlantiku	Atlantik	k1gInSc6
(	(	kIx(
<g/>
SACLANT	SACLANT	kA
<g/>
)	)	kIx)
a	a	k8xC
vzniklo	vzniknout	k5eAaPmAgNnS
Velitelství	velitelství	k1gNnSc4
spojeneckých	spojenecký	k2eAgFnPc2d1
sil	síla	k1gFnPc2
pro	pro	k7c4
transformaci	transformace	k1gFnSc4
(	(	kIx(
<g/>
ACT	ACT	kA
<g/>
)	)	kIx)
a	a	k8xC
z	z	k7c2
Vrchního	vrchní	k2eAgNnSc2d1
velitelství	velitelství	k1gNnSc2
spojeneckých	spojenecký	k2eAgFnPc2d1
sil	síla	k1gFnPc2
v	v	k7c6
Evropě	Evropa	k1gFnSc6
(	(	kIx(
<g/>
SHAPE	SHAPE	kA
<g/>
)	)	kIx)
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
Velitelství	velitelství	k1gNnSc1
spojeneckých	spojenecký	k2eAgFnPc2d1
sil	síla	k1gFnPc2
pro	pro	k7c4
operace	operace	k1gFnPc4
(	(	kIx(
<g/>
ACO	ACO	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
53	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
pražském	pražský	k2eAgInSc6d1
summitu	summit	k1gInSc6
se	se	k3xPyFc4
také	také	k9
začalo	začít	k5eAaPmAgNnS
hovořit	hovořit	k5eAaImF
o	o	k7c6
vstupu	vstup	k1gInSc6
sedmi	sedm	k4xCc2
nových	nový	k2eAgInPc2d1
států	stát	k1gInPc2
do	do	k7c2
NATO	NATO	kA
<g/>
:	:	kIx,
jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
o	o	k7c4
Estonsko	Estonsko	k1gNnSc4
<g/>
,	,	kIx,
Lotyšsko	Lotyšsko	k1gNnSc4
<g/>
,	,	kIx,
Litvu	Litva	k1gFnSc4
<g/>
,	,	kIx,
Slovensko	Slovensko	k1gNnSc4
<g/>
,	,	kIx,
Slovinsko	Slovinsko	k1gNnSc1
<g/>
,	,	kIx,
Bulharsko	Bulharsko	k1gNnSc1
a	a	k8xC
Rumunsko	Rumunsko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
Aliance	aliance	k1gFnSc2
tyto	tento	k3xDgInPc1
státy	stát	k1gInPc1
vstoupily	vstoupit	k5eAaPmAgInP
29	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2004	#num#	k4
<g/>
,	,	kIx,
krátce	krátce	k6eAd1
před	před	k7c7
summitem	summit	k1gInSc7
v	v	k7c6
Istanbulu	Istanbul	k1gInSc6
<g/>
.	.	kIx.
30	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
začala	začít	k5eAaPmAgFnS
mise	mise	k1gFnSc1
Baltic	Baltice	k1gFnPc2
Air	Air	k1gFnPc2
Policing	Policing	k1gInSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
podporuje	podporovat	k5eAaImIp3nS
suverenitu	suverenita	k1gFnSc4
Baltských	baltský	k2eAgInPc2d1
států	stát	k1gInPc2
poskytováním	poskytování	k1gNnSc7
vojenských	vojenský	k2eAgInPc2d1
letounů	letoun	k1gInPc2
za	za	k7c7
účelem	účel	k1gInSc7
ochrany	ochrana	k1gFnSc2
před	před	k7c7
případným	případný	k2eAgInSc7d1
leteckým	letecký	k2eAgInSc7d1
útokem	útok	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
54	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Summit	summit	k1gInSc1
v	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
v	v	k7c6
Rize	Riga	k1gFnSc6
byl	být	k5eAaImAgMnS
prvním	první	k4xOgMnSc7
summitem	summit	k1gInSc7
NATO	NATO	kA
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
probíhal	probíhat	k5eAaImAgInS
na	na	k7c6
bývalém	bývalý	k2eAgNnSc6d1
území	území	k1gNnSc6
Sovětského	sovětský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavním	hlavní	k2eAgNnSc7d1
tématem	téma	k1gNnSc7
byla	být	k5eAaImAgFnS
otázka	otázka	k1gFnSc1
Afghánistánu	Afghánistán	k1gInSc2
a	a	k8xC
snahy	snaha	k1gFnSc2
o	o	k7c4
další	další	k2eAgNnSc4d1
rozšíření	rozšíření	k1gNnSc4
NATO	NATO	kA
a	a	k8xC
Partnerství	partnerství	k1gNnSc2
pro	pro	k7c4
mír	mír	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
55	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
summitu	summit	k1gInSc6
v	v	k7c6
Bukurešti	Bukurešť	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
byly	být	k5eAaImAgInP
ke	k	k7c3
členství	členství	k1gNnSc3
v	v	k7c6
Alianci	aliance	k1gFnSc6
přizvány	přizván	k2eAgInPc1d1
Chorvatsko	Chorvatsko	k1gNnSc1
a	a	k8xC
Albánie	Albánie	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
se	se	k3xPyFc4
oficiálně	oficiálně	k6eAd1
připojily	připojit	k5eAaPmAgInP
v	v	k7c6
dubnu	duben	k1gInSc6
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ukrajině	Ukrajina	k1gFnSc6
a	a	k8xC
Gruzii	Gruzie	k1gFnSc3
bylo	být	k5eAaImAgNnS
přislíbeno	přislíben	k2eAgNnSc1d1
přijetí	přijetí	k1gNnSc1
do	do	k7c2
Aliance	aliance	k1gFnSc2
v	v	k7c6
budoucnosti	budoucnost	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
56	#num#	k4
<g/>
]	]	kIx)
Černá	černý	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
je	být	k5eAaImIp3nS
členem	člen	k1gMnSc7
aliance	aliance	k1gFnSc2
od	od	k7c2
června	červen	k1gInSc2
2017	#num#	k4
<g/>
,	,	kIx,
Severní	severní	k2eAgFnSc1d1
Makedonie	Makedonie	k1gFnSc1
od	od	k7c2
roku	rok	k1gInSc2
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Protiraketová	protiraketový	k2eAgFnSc1d1
obrana	obrana	k1gFnSc1
</s>
<s>
Generální	generální	k2eAgMnSc1d1
tajemník	tajemník	k1gMnSc1
NATO	NATO	kA
<g/>
,	,	kIx,
americký	americký	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
a	a	k8xC
předsedové	předseda	k1gMnPc1
vlád	vláda	k1gFnPc2
Lotyšska	Lotyšsko	k1gNnSc2
<g/>
,	,	kIx,
Slovinska	Slovinsko	k1gNnSc2
<g/>
,	,	kIx,
Litvy	Litva	k1gFnSc2
<g/>
,	,	kIx,
Slovenska	Slovensko	k1gNnSc2
<g/>
,	,	kIx,
Rumunska	Rumunsko	k1gNnSc2
<g/>
,	,	kIx,
Bulharska	Bulharsko	k1gNnSc2
a	a	k8xC
Estonska	Estonsko	k1gNnSc2
po	po	k7c6
ceremonii	ceremonie	k1gFnSc6
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
jejich	jejich	k3xOp3gInSc7
vstupem	vstup	k1gInSc7
do	do	k7c2
NATO	NATO	kA
29	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2004	#num#	k4
na	na	k7c6
summitu	summit	k1gInSc6
v	v	k7c6
Istanbulu	Istanbul	k1gInSc6
</s>
<s>
Na	na	k7c6
pražském	pražský	k2eAgInSc6d1
summitu	summit	k1gInSc6
bylo	být	k5eAaImAgNnS
dohodnuto	dohodnout	k5eAaPmNgNnS
přezkoumání	přezkoumání	k1gNnSc1
možnosti	možnost	k1gFnSc2
ochrany	ochrana	k1gFnSc2
území	území	k1gNnSc2
Aliance	aliance	k1gFnSc2
před	před	k7c7
raketovými	raketový	k2eAgFnPc7d1
hrozbami	hrozba	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
později	pozdě	k6eAd2
začaly	začít	k5eAaPmAgInP
vyjednávat	vyjednávat	k5eAaImF
s	s	k7c7
Polskem	Polsko	k1gNnSc7
a	a	k8xC
Českem	Česko	k1gNnSc7
o	o	k7c4
vybudování	vybudování	k1gNnSc4
protiraketové	protiraketový	k2eAgFnSc2d1
obrany	obrana	k1gFnSc2
na	na	k7c6
jejich	jejich	k3xOp3gNnSc6
území	území	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
57	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
bylo	být	k5eAaImAgNnS
dohodnuto	dohodnout	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
základny	základna	k1gFnPc1
mají	mít	k5eAaImIp3nP
být	být	k5eAaImF
do	do	k7c2
provozu	provoz	k1gInSc2
uvedeny	uvést	k5eAaPmNgInP
do	do	k7c2
roku	rok	k1gInSc2
2015	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
58	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
<g />
.	.	kIx.
</s>
<s hack="1">
červenci	červenec	k1gInSc6
2008	#num#	k4
Česko	Česko	k1gNnSc1
a	a	k8xC
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
podepsaly	podepsat	k5eAaPmAgInP
předběžnou	předběžný	k2eAgFnSc4d1
dohodu	dohoda	k1gFnSc4
o	o	k7c4
umístění	umístění	k1gNnSc4
základny	základna	k1gFnSc2
protiraketové	protiraketový	k2eAgFnSc2d1
obrany	obrana	k1gFnSc2
na	na	k7c6
českém	český	k2eAgNnSc6d1
území	území	k1gNnSc6
<g/>
[	[	kIx(
<g/>
59	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
s	s	k7c7
Polskem	Polsko	k1gNnSc7
byla	být	k5eAaImAgFnS
podobná	podobný	k2eAgFnSc1d1
dohoda	dohoda	k1gFnSc1
uzavřena	uzavřít	k5eAaPmNgFnS
v	v	k7c6
srpnu	srpen	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
60	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
reakci	reakce	k1gFnSc6
na	na	k7c4
to	ten	k3xDgNnSc4
v	v	k7c6
Česku	Česko	k1gNnSc6
více	hodně	k6eAd2
než	než	k8xS
200	#num#	k4
000	#num#	k4
lidí	člověk	k1gMnPc2
podepsalo	podepsat	k5eAaPmAgNnS
petici	petice	k1gFnSc4
požadující	požadující	k2eAgNnSc1d1
referendum	referendum	k1gNnSc1
o	o	k7c6
zřízení	zřízení	k1gNnSc6
základny	základna	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
61	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
reakci	reakce	k1gFnSc6
na	na	k7c6
vyjednávání	vyjednávání	k1gNnSc6
ruský	ruský	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
Vladimir	Vladimir	k1gMnSc1
Putin	putin	k2eAgMnSc1d1
prohlásil	prohlásit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
uskutečnění	uskutečnění	k1gNnSc4
těchto	tento	k3xDgInPc2
plánů	plán	k1gInPc2
by	by	kYmCp3nS
mohlo	moct	k5eAaImAgNnS
vést	vést	k5eAaImF
k	k	k7c3
novým	nový	k2eAgInPc3d1
závodům	závod	k1gInPc3
ve	v	k7c6
zbrojení	zbrojení	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
naznačil	naznačit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
Rusko	Rusko	k1gNnSc1
odstoupí	odstoupit	k5eAaPmIp3nS
od	od	k7c2
Smlouvy	smlouva	k1gFnSc2
o	o	k7c6
konvenčních	konvenční	k2eAgFnPc6d1
ozbrojených	ozbrojený	k2eAgFnPc6d1
silách	síla	k1gFnPc6
v	v	k7c6
Evropě	Evropa	k1gFnSc6
(	(	kIx(
<g/>
CFE	CFE	kA
<g/>
)	)	kIx)
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
<g/>
,	,	kIx,
než	než	k8xS
všechny	všechen	k3xTgInPc1
státy	stát	k1gInPc1
NATO	nato	k6eAd1
podepíšou	podepsat	k5eAaPmIp3nP
adaptovaný	adaptovaný	k2eAgInSc4d1
text	text	k1gInSc4
smlouvy	smlouva	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
62	#num#	k4
<g/>
]	]	kIx)
Americký	americký	k2eAgMnSc1d1
ministr	ministr	k1gMnSc1
zahraničí	zahraničí	k1gNnSc2
na	na	k7c4
to	ten	k3xDgNnSc4
odpověděl	odpovědět	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
Rusko	Rusko	k1gNnSc1
nemá	mít	k5eNaImIp3nS
čeho	co	k3yRnSc2,k3yQnSc2,k3yInSc2
obávat	obávat	k5eAaImF
<g/>
,	,	kIx,
protože	protože	k8xS
několik	několik	k4yIc4
protiraketových	protiraketový	k2eAgFnPc2d1
střel	střela	k1gFnPc2
by	by	k9
ruský	ruský	k2eAgInSc4d1
jaderný	jaderný	k2eAgInSc4d1
arzenál	arzenál	k1gInSc4
zastavit	zastavit	k5eAaPmF
nedokázalo	dokázat	k5eNaPmAgNnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
62	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
listopadu	listopad	k1gInSc6
2007	#num#	k4
byl	být	k5eAaImAgInS
odstup	odstup	k1gInSc1
od	od	k7c2
smlouvy	smlouva	k1gFnSc2
CFE	CFE	kA
odhlasován	odhlasovat	k5eAaPmNgInS
ruským	ruský	k2eAgInSc7d1
parlamentem	parlament	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
52	#num#	k4
<g/>
]	]	kIx)
Dohoda	dohoda	k1gFnSc1
o	o	k7c6
umístění	umístění	k1gNnSc6
10	#num#	k4
protiraketových	protiraketový	k2eAgFnPc2d1
střel	střela	k1gFnPc2
a	a	k8xC
systému	systém	k1gInSc2
MIM-104	MIM-104	k1gMnSc1
Patriot	patriot	k1gMnSc1
do	do	k7c2
Polska	Polsko	k1gNnSc2
ze	z	k7c2
14	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2008	#num#	k4
vedla	vést	k5eAaImAgFnS
k	k	k7c3
jaderným	jaderný	k2eAgFnPc3d1
hrozbám	hrozba	k1gFnPc3
Polsku	Polska	k1gFnSc4
ze	z	k7c2
strany	strana	k1gFnSc2
Ruska	Rusko	k1gNnSc2
<g/>
.	.	kIx.
20	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2008	#num#	k4
navíc	navíc	k6eAd1
Rusko	Rusko	k1gNnSc1
oznámilo	oznámit	k5eAaPmAgNnS
Norsku	Norsko	k1gNnSc3
<g/>
,	,	kIx,
že	že	k8xS
zruší	zrušit	k5eAaPmIp3nS
všechnu	všechen	k3xTgFnSc4
vojenskou	vojenský	k2eAgFnSc4d1
spolupráci	spolupráce	k1gFnSc4
s	s	k7c7
NATO	NATO	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
63	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2009	#num#	k4
americký	americký	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
Barack	Barack	k1gMnSc1
Obama	Obama	k?
oznámil	oznámit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
opouští	opouštět	k5eAaImIp3nS
od	od	k7c2
plánu	plán	k1gInSc2
na	na	k7c4
protiraketové	protiraketový	k2eAgFnPc4d1
střely	střela	k1gFnPc4
dlouhého	dlouhý	k2eAgInSc2d1
doletu	dolet	k1gInSc2
a	a	k8xC
místo	místo	k7c2
toho	ten	k3xDgNnSc2
bude	být	k5eAaImBp3nS
Evropa	Evropa	k1gFnSc1
chráněna	chránit	k5eAaImNgFnS
proti	proti	k7c3
střelám	střela	k1gFnPc3
středního	střední	k2eAgInSc2d1
a	a	k8xC
krátkého	krátký	k2eAgInSc2d1
doletu	dolet	k1gInSc2
loďmi	loď	k1gFnPc7
využívající	využívající	k2eAgInSc4d1
systém	systém	k1gInSc4
Aegis	Aegis	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
64	#num#	k4
<g/>
]	]	kIx)
Oznámení	oznámení	k1gNnSc1
se	se	k3xPyFc4
setkalo	setkat	k5eAaPmAgNnS
s	s	k7c7
kritikou	kritika	k1gFnSc7
v	v	k7c6
americkém	americký	k2eAgInSc6d1
Kongresu	kongres	k1gInSc6
<g/>
[	[	kIx(
<g/>
65	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
hlavně	hlavně	k9
v	v	k7c6
Polsku	Polsko	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rusko	Rusko	k1gNnSc1
naopak	naopak	k6eAd1
rozhodnutí	rozhodnutí	k1gNnSc6
uvítalo	uvítat	k5eAaPmAgNnS
a	a	k8xC
oznámilo	oznámit	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
opatření	opatření	k1gNnPc1
přijatá	přijatý	k2eAgNnPc1d1
v	v	k7c6
reakci	reakce	k1gFnSc6
na	na	k7c4
americký	americký	k2eAgInSc4d1
projekt	projekt	k1gInSc4
<g/>
,	,	kIx,
mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
rozmístění	rozmístění	k1gNnSc4
raket	raketa	k1gFnPc2
typu	typ	k1gInSc2
9K720	9K720	k4
Iskander	Iskandra	k1gFnPc2
v	v	k7c6
Kaliningradské	kaliningradský	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
<g/>
,	,	kIx,
zruší	zrušit	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nově	nově	k6eAd1
zvolený	zvolený	k2eAgMnSc1d1
generální	generální	k2eAgMnSc1d1
tajemník	tajemník	k1gMnSc1
NATO	NATO	kA
Anders	Anders	k1gInSc1
Fogh	Fogh	k1gInSc1
Rasmussen	Rasmussen	k1gInSc1
navrhl	navrhnout	k5eAaPmAgInS
Rusku	Ruska	k1gFnSc4
spolupráci	spolupráce	k1gFnSc3
týkající	týkající	k2eAgFnSc2d1
se	se	k3xPyFc4
konkrétně	konkrétně	k6eAd1
protiraketové	protiraketový	k2eAgFnSc2d1
obrany	obrana	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
67	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
NATO	nato	k6eAd1
odsoudilo	odsoudit	k5eAaPmAgNnS
Smlouvu	smlouva	k1gFnSc4
OSN	OSN	kA
o	o	k7c6
zákazu	zákaz	k1gInSc6
jaderných	jaderný	k2eAgFnPc2d1
zbraní	zbraň	k1gFnPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
68	#num#	k4
<g/>
]	]	kIx)
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
v	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
na	na	k7c6
půdě	půda	k1gFnSc6
OSN	OSN	kA
podpořilo	podpořit	k5eAaPmAgNnS
122	#num#	k4
států	stát	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
69	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Členské	členský	k2eAgFnPc1d1
země	zem	k1gFnPc1
</s>
<s>
Mapa	mapa	k1gFnSc1
partnerských	partnerský	k2eAgFnPc2d1
smluv	smlouva	k1gFnPc2
NATO	NATO	kA
v	v	k7c6
Evropě	Evropa	k1gFnSc6
a	a	k8xC
okolí	okolí	k1gNnSc6
</s>
<s>
Členové	člen	k1gMnPc1
NATO	NATO	kA
</s>
<s>
Akční	akční	k2eAgInSc1d1
plán	plán	k1gInSc1
členství	členství	k1gNnSc2
(	(	kIx(
<g/>
MAP	mapa	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
Intenzifikovaný	Intenzifikovaný	k2eAgInSc4d1
dialog	dialog	k1gInSc4
</s>
<s>
Individuální	individuální	k2eAgInSc1d1
akční	akční	k2eAgInSc1d1
plán	plán	k1gInSc1
partnerství	partnerství	k1gNnSc2
</s>
<s>
Partnerství	partnerství	k1gNnSc1
pro	pro	k7c4
mír	mír	k1gInSc4
</s>
<s>
Severoatlantickou	severoatlantický	k2eAgFnSc4d1
smlouvu	smlouva	k1gFnSc4
podepsalo	podepsat	k5eAaPmAgNnS
v	v	k7c6
dubnu	duben	k1gInSc6
1949	#num#	k4
dvanáct	dvanáct	k4xCc4
států	stát	k1gInPc2
<g/>
:	:	kIx,
Spojené	spojený	k2eAgInPc4d1
státy	stát	k1gInPc4
americké	americký	k2eAgInPc4d1
<g/>
,	,	kIx,
Kanada	Kanada	k1gFnSc1
<g/>
,	,	kIx,
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
<g/>
,	,	kIx,
Francie	Francie	k1gFnSc1
<g/>
,	,	kIx,
Portugalsko	Portugalsko	k1gNnSc1
<g/>
,	,	kIx,
Belgie	Belgie	k1gFnSc1
<g/>
,	,	kIx,
Lucembursko	Lucembursko	k1gNnSc1
<g/>
,	,	kIx,
Nizozemsko	Nizozemsko	k1gNnSc1
<g/>
,	,	kIx,
Dánsko	Dánsko	k1gNnSc1
<g/>
,	,	kIx,
Norsko	Norsko	k1gNnSc1
<g/>
,	,	kIx,
Itálie	Itálie	k1gFnSc1
a	a	k8xC
Island	Island	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1952	#num#	k4
se	se	k3xPyFc4
připojilo	připojit	k5eAaPmAgNnS
Řecko	Řecko	k1gNnSc1
a	a	k8xC
Turecko	Turecko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1955	#num#	k4
po	po	k7c6
získání	získání	k1gNnSc6
plné	plný	k2eAgFnSc2d1
suverenity	suverenita	k1gFnSc2
vstoupilo	vstoupit	k5eAaPmAgNnS
Západní	západní	k2eAgNnSc1d1
Německo	Německo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1982	#num#	k4
se	se	k3xPyFc4
připojilo	připojit	k5eAaPmAgNnS
Španělsko	Španělsko	k1gNnSc1
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1999	#num#	k4
předali	předat	k5eAaPmAgMnP
ministři	ministr	k1gMnPc1
zahraničí	zahraničí	k1gNnSc2
Česka	Česko	k1gNnSc2
<g/>
,	,	kIx,
Maďarska	Maďarsko	k1gNnSc2
a	a	k8xC
Polska	Polsko	k1gNnSc2
své	svůj	k3xOyFgFnSc3
americké	americký	k2eAgFnSc3d1
kolegyni	kolegyně	k1gFnSc3
Madeleine	Madeleine	k1gFnSc3
Albrightové	Albrightová	k1gFnSc3
příslušné	příslušný	k2eAgFnSc2d1
ratifikační	ratifikační	k2eAgFnSc2d1
listiny	listina	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
jejich	jejich	k3xOp3gFnPc1
země	zem	k1gFnPc1
se	se	k3xPyFc4
tak	tak	k6eAd1
také	také	k9
staly	stát	k5eAaPmAgInP
členy	člen	k1gInPc1
NATO	NATO	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
se	se	k3xPyFc4
v	v	k7c6
největší	veliký	k2eAgFnSc6d3
vlně	vlna	k1gFnSc6
rozšiřování	rozšiřování	k1gNnSc1
NATO	NATO	kA
připojilo	připojit	k5eAaPmAgNnS
7	#num#	k4
států	stát	k1gInPc2
<g/>
:	:	kIx,
Litva	Litva	k1gFnSc1
<g/>
,	,	kIx,
Lotyšsko	Lotyšsko	k1gNnSc1
<g/>
,	,	kIx,
Estonsko	Estonsko	k1gNnSc1
<g/>
,	,	kIx,
Rumunsko	Rumunsko	k1gNnSc1
<g/>
,	,	kIx,
Bulharsko	Bulharsko	k1gNnSc1
<g/>
,	,	kIx,
Slovinsko	Slovinsko	k1gNnSc1
a	a	k8xC
Slovensko	Slovensko	k1gNnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2009	#num#	k4
přistoupily	přistoupit	k5eAaPmAgFnP
Chorvatsko	Chorvatsko	k1gNnSc4
a	a	k8xC
Albánie	Albánie	k1gFnSc1
<g/>
,	,	kIx,
5	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2017	#num#	k4
Černá	černý	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
a	a	k8xC
27	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2020	#num#	k4
Severní	severní	k2eAgFnSc2d1
Makedonie	Makedonie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Česko	Česko	k1gNnSc1
a	a	k8xC
NATO	NATO	kA
</s>
<s>
Česko	Česko	k1gNnSc1
spolu	spolu	k6eAd1
s	s	k7c7
Polskem	Polsko	k1gNnSc7
a	a	k8xC
Maďarskem	Maďarsko	k1gNnSc7
jako	jako	k8xC,k8xS
první	první	k4xOgFnSc2
země	zem	k1gFnSc2
bývalého	bývalý	k2eAgInSc2d1
Východního	východní	k2eAgInSc2d1
bloku	blok	k1gInSc2
vstoupilo	vstoupit	k5eAaPmAgNnS
12	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1999	#num#	k4
do	do	k7c2
NATO	NATO	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ministr	ministr	k1gMnSc1
zahraničí	zahraničí	k1gNnSc2
Jan	Jan	k1gMnSc1
Kavan	Kavan	k1gMnSc1
předal	předat	k5eAaPmAgMnS
v	v	k7c6
americkém	americký	k2eAgInSc6d1
Independence	Independence	k1gFnSc2
v	v	k7c6
Missouri	Missouri	k1gFnSc6
ratifikační	ratifikační	k2eAgFnSc4d1
listinu	listina	k1gFnSc4
americké	americký	k2eAgFnSc3d1
ministryni	ministryně	k1gFnSc3
Madeleine	Madeleine	k1gFnSc2
Albrightové	Albrightová	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
70	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Platí	platit	k5eAaImIp3nS
dohoda	dohoda	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
členské	členský	k2eAgFnSc2d1
země	zem	k1gFnSc2
NATO	NATO	kA
<g/>
,	,	kIx,
tedy	tedy	k9
i	i	k9
Česko	Česko	k1gNnSc1
<g/>
,	,	kIx,
mají	mít	k5eAaImIp3nP
vynakládat	vynakládat	k5eAaImF
2	#num#	k4
%	%	kIx~
HDP	HDP	kA
na	na	k7c4
svou	svůj	k3xOyFgFnSc4
obranu	obrana	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
71	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
72	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rozšiřování	rozšiřování	k1gNnSc1
</s>
<s>
Na	na	k7c6
summitu	summit	k1gInSc6
v	v	k7c6
Bukurešti	Bukurešť	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
bylo	být	k5eAaImAgNnS
budoucí	budoucí	k2eAgNnSc1d1
přizvání	přizvání	k1gNnSc1
do	do	k7c2
Aliance	aliance	k1gFnSc2
přislíbeno	přislíbit	k5eAaPmNgNnS
třem	tři	k4xCgFnPc3
státům	stát	k1gInPc3
<g/>
:	:	kIx,
Gruzii	Gruzie	k1gFnSc6
<g/>
,	,	kIx,
Ukrajině	Ukrajina	k1gFnSc6
a	a	k8xC
Severní	severní	k2eAgFnSc6d1
Makedonii	Makedonie	k1gFnSc6
(	(	kIx(
<g/>
tehdy	tehdy	k6eAd1
zvané	zvaný	k2eAgFnPc1d1
Republika	republika	k1gFnSc1
Makedonie	Makedonie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
56	#num#	k4
<g/>
]	]	kIx)
Severní	severní	k2eAgFnSc2d1
Makedonie	Makedonie	k1gFnSc2
v	v	k7c6
únoru	únor	k1gInSc6
2019	#num#	k4
podepsala	podepsat	k5eAaPmAgFnS
protokol	protokol	k1gInSc4
o	o	k7c4
přistoupení	přistoupení	k1gNnSc4
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
aby	aby	kYmCp3nS
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
členským	členský	k2eAgInSc7d1
státem	stát	k1gInSc7
NATO	NATO	kA
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
procházel	procházet	k5eAaImAgInS
ratifikací	ratifikace	k1gFnSc7
členskými	členský	k2eAgInPc7d1
státy	stát	k1gInPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
73	#num#	k4
<g/>
]	]	kIx)
Její	její	k3xOp3gNnSc1
přistoupení	přistoupení	k1gNnSc1
bylo	být	k5eAaImAgNnS
mnoho	mnoho	k4c4
let	léto	k1gNnPc2
blokováno	blokován	k2eAgNnSc4d1
Řeckem	Řecko	k1gNnSc7
kvůli	kvůli	k7c3
sporu	spor	k1gInSc3
o	o	k7c4
název	název	k1gInSc4
Makedonie	Makedonie	k1gFnSc2
<g/>
,	,	kIx,
ten	ten	k3xDgInSc1
byl	být	k5eAaImAgInS
vyřešen	vyřešit	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
Prespanskou	Prespanský	k2eAgFnSc7d1
dohodou	dohoda	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
74	#num#	k4
<g/>
]	]	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
Vstupu	vstup	k1gInSc2
Kypru	Kypr	k1gInSc2
do	do	k7c2
Aliance	aliance	k1gFnSc2
brání	bránit	k5eAaImIp3nS
Turecko	Turecko	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
75	#num#	k4
<g/>
]	]	kIx)
Ukrajinský	ukrajinský	k2eAgInSc1d1
parlament	parlament	k1gInSc1
v	v	k7c6
červnu	červen	k1gInSc6
2010	#num#	k4
odhlasoval	odhlasovat	k5eAaPmAgInS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
Ukrajina	Ukrajina	k1gFnSc1
o	o	k7c4
vstup	vstup	k1gInSc4
do	do	k7c2
NATO	NATO	kA
ucházet	ucházet	k5eAaImF
nebude	být	k5eNaImBp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
76	#num#	k4
<g/>
]	]	kIx)
Od	od	k7c2
roku	rok	k1gInSc2
2019	#num#	k4
má	mít	k5eAaImIp3nS
Ukrajina	Ukrajina	k1gFnSc1
ústavně	ústavně	k6eAd1
zakotvené	zakotvený	k2eAgNnSc4d1
směřování	směřování	k1gNnSc4
země	zem	k1gFnSc2
do	do	k7c2
NATO	NATO	kA
<g />
.	.	kIx.
</s>
<s hack="1">
a	a	k8xC
EU	EU	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
77	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
78	#num#	k4
<g/>
]	]	kIx)
Od	od	k7c2
června	červen	k1gInSc2
2020	#num#	k4
je	být	k5eAaImIp3nS
Ukrajina	Ukrajina	k1gFnSc1
součástí	součást	k1gFnPc2
iniciativy	iniciativa	k1gFnSc2
„	„	k?
<g/>
Enhanced	Enhanced	k1gMnSc1
Opportunity	Opportunita	k1gFnSc2
Partners	Partners	k1gInSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
společně	společně	k6eAd1
s	s	k7c7
Austrálií	Austrálie	k1gFnSc7
<g/>
,	,	kIx,
Finskem	Finsko	k1gNnSc7
<g/>
,	,	kIx,
Gruzií	Gruzie	k1gFnSc7
<g/>
,	,	kIx,
Jordánskem	Jordánsko	k1gNnSc7
a	a	k8xC
Švédskem	Švédsko	k1gNnSc7
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
79	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dalším	další	k2eAgMnSc7d1
potenciálním	potenciální	k2eAgMnSc7d1
kandidátem	kandidát	k1gMnSc7
je	být	k5eAaImIp3nS
Bosna	Bosna	k1gFnSc1
a	a	k8xC
Hercegovina	Hercegovina	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
Akčním	akční	k2eAgInSc6d1
plánu	plán	k1gInSc6
členství	členství	k1gNnSc2
(	(	kIx(
<g/>
MAP	mapa	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
předstupeň	předstupeň	k1gInSc1
ke	k	k7c3
vstupu	vstup	k1gInSc3
do	do	k7c2
NATO	NATO	kA
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc7
cílem	cíl	k1gInSc7
je	být	k5eAaImIp3nS
připravit	připravit	k5eAaPmF
budoucí	budoucí	k2eAgFnPc4d1
členské	členský	k2eAgFnPc4d1
země	zem	k1gFnPc4
na	na	k7c4
povinnosti	povinnost	k1gFnPc4
a	a	k8xC
závazky	závazek	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
členství	členství	k1gNnSc1
přináší	přinášet	k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
80	#num#	k4
<g/>
]	]	kIx)
Rusko	Rusko	k1gNnSc1
se	se	k3xPyFc4
snaží	snažit	k5eAaImIp3nS
rozšiřování	rozšiřování	k1gNnSc1
NATO	NATO	kA
zabránit	zabránit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
Vladimir	Vladimir	k1gInSc1
Putin	putin	k2eAgInSc1d1
navštívil	navštívit	k5eAaPmAgMnS
Srbsko	Srbsko	k1gNnSc4
a	a	k8xC
prohlásil	prohlásit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
nechce	chtít	k5eNaImIp3nS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
tento	tento	k3xDgInSc1
stát	stát	k1gInSc1
stal	stát	k5eAaPmAgInS
členem	člen	k1gInSc7
Aliance	aliance	k1gFnSc2
a	a	k8xC
její	její	k3xOp3gNnSc1
rozšiřování	rozšiřování	k1gNnSc1
je	být	k5eAaImIp3nS
proti	proti	k7c3
zájmům	zájem	k1gInPc3
Ruska	Rusko	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
81	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Působení	působení	k1gNnSc1
</s>
<s>
Intervence	intervence	k1gFnPc1
na	na	k7c6
Balkánu	Balkán	k1gInSc6
</s>
<s>
Válka	válka	k1gFnSc1
v	v	k7c6
Bosně	Bosna	k1gFnSc6
a	a	k8xC
Hercegovině	Hercegovina	k1gFnSc6
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článcích	článek	k1gInPc6
IFOR	IFOR	kA
<g/>
,	,	kIx,
SFOR	SFOR	kA
a	a	k8xC
Válka	válka	k1gFnSc1
v	v	k7c6
Bosně	Bosna	k1gFnSc6
a	a	k8xC
Hercegovině	Hercegovina	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Letadlo	letadlo	k1gNnSc1
NATO	NATO	kA
F-16	F-16	k1gFnSc2
účastnící	účastnící	k2eAgFnSc1d1
se	se	k3xPyFc4
bombardování	bombardování	k1gNnPc2
během	během	k7c2
operace	operace	k1gFnSc2
Rozhodná	rozhodný	k2eAgFnSc1d1
síla	síla	k1gFnSc1
konané	konaný	k2eAgNnSc1d1
po	po	k7c6
Srebrenickém	srebrenický	k2eAgInSc6d1
masakru	masakr	k1gInSc6
</s>
<s>
V	v	k7c6
rámci	rámec	k1gInSc6
operace	operace	k1gFnSc2
Deny	Dena	k1gFnSc2
Flight	Flight	k1gInSc1
(	(	kIx(
<g/>
Odepřený	odepřený	k2eAgInSc1d1
let	let	k1gInSc1
<g/>
)	)	kIx)
od	od	k7c2
12	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1993	#num#	k4
do	do	k7c2
20	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1995	#num#	k4
NATO	nato	k6eAd1
zajišťovalo	zajišťovat	k5eAaImAgNnS
bezletovou	bezletový	k2eAgFnSc4d1
zónu	zóna	k1gFnSc4
nad	nad	k7c7
Bosnou	Bosna	k1gFnSc7
a	a	k8xC
Hercegovinou	Hercegovina	k1gFnSc7
během	během	k7c2
války	válka	k1gFnSc2
v	v	k7c6
Bosně	Bosna	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
82	#num#	k4
<g/>
]	]	kIx)
Mezi	mezi	k7c7
červnem	červen	k1gInSc7
1993	#num#	k4
a	a	k8xC
červnem	červen	k1gInSc7
(	(	kIx(
<g/>
oficiálně	oficiálně	k6eAd1
říjnem	říjen	k1gInSc7
<g/>
)	)	kIx)
1996	#num#	k4
probíhala	probíhat	k5eAaImAgFnS
operace	operace	k1gFnSc1
Sharp	sharp	k1gInSc4
Guard	Guard	k1gInSc1
(	(	kIx(
<g/>
Bdělá	bdělý	k2eAgFnSc1d1
stráž	stráž	k1gFnSc1
<g/>
)	)	kIx)
neboli	neboli	k8xC
námořní	námořní	k2eAgFnSc1d1
blokáda	blokáda	k1gFnSc1
bývalé	bývalý	k2eAgFnSc2d1
Jugoslávie	Jugoslávie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
83	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
28	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1994	#num#	k4
sestřelily	sestřelit	k5eAaPmAgInP
americké	americký	k2eAgInPc1d1
letouny	letoun	k1gInPc1
F-16	F-16	k1gFnSc2
čtyři	čtyři	k4xCgNnPc1
vojenská	vojenský	k2eAgNnPc1d1
letadla	letadlo	k1gNnPc1
Republiky	republika	k1gFnSc2
srbské	srbský	k2eAgFnSc2d1
útočící	útočící	k2eAgFnSc7d1
proti	proti	k7c3
pozemnímu	pozemní	k2eAgInSc3d1
cíli	cíl	k1gInSc3
<g/>
;	;	kIx,
byl	být	k5eAaImAgInS
to	ten	k3xDgNnSc1
první	první	k4xOgInSc1
vojenský	vojenský	k2eAgInSc1d1
zásah	zásah	k1gInSc1
NATO	NATO	kA
v	v	k7c6
historii	historie	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
82	#num#	k4
<g/>
]	]	kIx)
Operace	operace	k1gFnSc1
Rozhodná	rozhodný	k2eAgFnSc1d1
síla	síla	k1gFnSc1
(	(	kIx(
<g/>
Deliberate	Deliberat	k1gInSc5
Force	force	k1gFnSc1
<g/>
)	)	kIx)
probíhala	probíhat	k5eAaImAgFnS
od	od	k7c2
30	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
do	do	k7c2
20	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1995	#num#	k4
a	a	k8xC
spočívala	spočívat	k5eAaImAgFnS
v	v	k7c6
bombardování	bombardování	k1gNnSc6
pozic	pozice	k1gFnPc2
Vojska	vojsko	k1gNnSc2
Republiky	republika	k1gFnSc2
srbské	srbský	k2eAgFnSc2d1
ohrožujících	ohrožující	k2eAgInPc2d1
tzv.	tzv.	kA
bezpečné	bezpečný	k2eAgFnPc4d1
zóny	zóna	k1gFnPc4
Sarajevo	Sarajevo	k1gNnSc1
a	a	k8xC
Goražde	Goražd	k1gInSc5
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
84	#num#	k4
<g/>
]	]	kIx)
Zásah	zásah	k1gInSc1
NATO	NATO	kA
dopomohl	dopomoct	k5eAaPmAgInS
k	k	k7c3
podepsání	podepsání	k1gNnSc3
Daytonské	daytonský	k2eAgFnSc2d1
dohody	dohoda	k1gFnSc2
v	v	k7c6
prosinci	prosinec	k1gInSc6
1995	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
součást	součást	k1gFnSc4
této	tento	k3xDgFnSc2
dohody	dohoda	k1gFnSc2
nasadilo	nasadit	k5eAaPmAgNnS
NATO	nato	k6eAd1
v	v	k7c6
rámci	rámec	k1gInSc6
operace	operace	k1gFnSc2
Joint	Jointa	k1gFnPc2
Endeavour	Endeavour	k1gInSc1
(	(	kIx(
<g/>
Společné	společný	k2eAgNnSc1d1
úsilí	úsilí	k1gNnSc1
<g/>
)	)	kIx)
mírové	mírový	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
IFOR	IFOR	kA
a	a	k8xC
později	pozdě	k6eAd2
SFOR	SFOR	kA
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
působily	působit	k5eAaImAgInP
od	od	k7c2
prosince	prosinec	k1gInSc2
1996	#num#	k4
do	do	k7c2
prosince	prosinec	k1gInSc2
2004	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
85	#num#	k4
<g/>
]	]	kIx)
NATO	NATO	kA
začalo	začít	k5eAaPmAgNnS
za	za	k7c4
účast	účast	k1gFnSc4
na	na	k7c6
těchto	tento	k3xDgFnPc6
operacích	operace	k1gFnPc6
udělovat	udělovat	k5eAaImF
medaile	medaile	k1gFnPc4
NATO	NATO	kA
<g/>
.	.	kIx.
</s>
<s>
Válka	válka	k1gFnSc1
v	v	k7c4
Kosovu	Kosův	k2eAgFnSc4d1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Operace	operace	k1gFnSc2
Spojenecká	spojenecký	k2eAgFnSc1d1
síla	síla	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Budova	budova	k1gFnSc1
poničená	poničený	k2eAgFnSc1d1
při	při	k7c6
bombardování	bombardování	k1gNnSc6
Jugoslávie	Jugoslávie	k1gFnSc2
silami	síla	k1gFnPc7
NATO	NATO	kA
v	v	k7c6
roce	rok	k1gInSc6
1999	#num#	k4
</s>
<s>
24	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1999	#num#	k4
NATO	nato	k6eAd1
zahájilo	zahájit	k5eAaPmAgNnS
jedenáctitýdenní	jedenáctitýdenní	k2eAgFnSc4d1
leteckou	letecký	k2eAgFnSc4d1
operaci	operace	k1gFnSc4
Spojenecká	spojenecký	k2eAgFnSc1d1
síla	síla	k1gFnSc1
(	(	kIx(
<g/>
Allied	Allied	k1gInSc1
Force	force	k1gFnSc2
<g/>
)	)	kIx)
proti	proti	k7c3
Svazové	svazový	k2eAgFnSc3d1
republice	republika	k1gFnSc3
Jugoslávie	Jugoslávie	k1gFnSc2
s	s	k7c7
cílem	cíl	k1gInSc7
zastavit	zastavit	k5eAaPmF
násilné	násilný	k2eAgNnSc4d1
jednání	jednání	k1gNnSc4
vůči	vůči	k7c3
albánskému	albánský	k2eAgNnSc3d1
obyvatelstvu	obyvatelstvo	k1gNnSc3
Kosova	Kosův	k2eAgNnSc2d1
ze	z	k7c2
strany	strana	k1gFnSc2
Srbů	Srb	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
se	se	k3xPyFc4
snažili	snažit	k5eAaImAgMnP
potlačit	potlačit	k5eAaPmF
vojenskou	vojenský	k2eAgFnSc7d1
silou	síla	k1gFnSc7
ozbrojené	ozbrojený	k2eAgNnSc4d1
povstání	povstání	k1gNnSc4
kosovskoalbánských	kosovskoalbánský	k2eAgMnPc2d1
separatistů	separatista	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
86	#num#	k4
<g/>
]	]	kIx)
NATO	NATO	kA
požadovalo	požadovat	k5eAaImAgNnS
souhlas	souhlas	k1gInSc4
Rady	rada	k1gFnSc2
bezpečnosti	bezpečnost	k1gFnSc2
OSN	OSN	kA
s	s	k7c7
vojenskou	vojenský	k2eAgFnSc7d1
intervencí	intervence	k1gFnSc7
<g/>
,	,	kIx,
ten	ten	k3xDgInSc1
však	však	k9
nebyl	být	k5eNaImAgInS
poskytnut	poskytnout	k5eAaPmNgInS
kvůli	kvůli	k7c3
vetu	veto	k1gNnSc3
Ruska	Rusko	k1gNnSc2
a	a	k8xC
Číny	Čína	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
dva	dva	k4xCgInPc4
státy	stát	k1gInPc4
později	pozdě	k6eAd2
podaly	podat	k5eAaPmAgInP
návrh	návrh	k1gInSc4
na	na	k7c4
ukončení	ukončení	k1gNnSc4
intervence	intervence	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
podpořila	podpořit	k5eAaPmAgFnS
ale	ale	k8xC
jen	jen	k9
Namibie	Namibie	k1gFnSc2
a	a	k8xC
nebyl	být	k5eNaImAgInS
tak	tak	k6eAd1
schválen	schválit	k5eAaPmNgInS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
87	#num#	k4
<g/>
]	]	kIx)
Konflikt	konflikt	k1gInSc1
skončil	skončit	k5eAaPmAgInS
11	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1999	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
jugoslávský	jugoslávský	k2eAgMnSc1d1
vůdce	vůdce	k1gMnSc1
Slobodan	Slobodan	k1gMnSc1
Milošević	Milošević	k1gMnSc1
vyhověl	vyhovět	k5eAaPmAgInS
požadavkům	požadavek	k1gInPc3
NATO	nato	k6eAd1
přijmutím	přijmutí	k1gNnSc7
rezoluce	rezoluce	k1gFnSc2
Rady	rada	k1gFnSc2
bezpečnosti	bezpečnost	k1gFnSc2
OSN	OSN	kA
<g />
.	.	kIx.
</s>
<s hack="1">
č.	č.	k?
1244	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1999	#num#	k4
vstoupili	vstoupit	k5eAaPmAgMnP
do	do	k7c2
Kosova	Kosův	k2eAgInSc2d1
na	na	k7c6
základě	základ	k1gInSc6
rezoluce	rezoluce	k1gFnSc2
č.	č.	k?
1244	#num#	k4
vojáci	voják	k1gMnPc1
KFOR	KFOR	kA
za	za	k7c7
účelem	účel	k1gInSc7
dosažení	dosažení	k1gNnSc2
trvalého	trvalý	k2eAgInSc2d1
míru	mír	k1gInSc2
a	a	k8xC
stability	stabilita	k1gFnSc2
v	v	k7c4
oblasti	oblast	k1gFnPc4
postižené	postižený	k2eAgFnPc4d1
válkou	válka	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
88	#num#	k4
<g/>
]	]	kIx)
Mise	mise	k1gFnSc1
se	se	k3xPyFc4
účastní	účastnit	k5eAaImIp3nS
také	také	k9
Armáda	armáda	k1gFnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
89	#num#	k4
<g/>
]	]	kIx)
Mezi	mezi	k7c7
srpnem	srpen	k1gInSc7
a	a	k8xC
zářím	září	k1gNnSc7
2001	#num#	k4
rovněž	rovněž	k9
probíhala	probíhat	k5eAaImAgFnS
Aliancí	aliance	k1gFnSc7
řízená	řízený	k2eAgFnSc1d1
operace	operace	k1gFnSc1
Essential	Essential	k1gInSc1
Harvest	Harvest	k1gFnSc1
(	(	kIx(
<g/>
Nezbytná	nezbytný	k2eAgFnSc1d1,k2eNgFnSc1d1
sklizeň	sklizeň	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
měla	mít	k5eAaImAgFnS
za	za	k7c4
cíl	cíl	k1gInSc4
shromáždit	shromáždit	k5eAaPmF
zbraně	zbraň	k1gFnPc4
a	a	k8xC
munici	munice	k1gFnSc4
odevzdanou	odevzdaný	k2eAgFnSc4d1
dobrovolně	dobrovolně	k6eAd1
albánskými	albánský	k2eAgMnPc7d1
povstalci	povstalec	k1gMnPc7
v	v	k7c6
Makedonii	Makedonie	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
90	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
<g/>
,	,	kIx,
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
a	a	k8xC
téměř	téměř	k6eAd1
všechny	všechen	k3xTgInPc1
další	další	k2eAgInPc1d1
členské	členský	k2eAgInPc1d1
státy	stát	k1gInPc1
NATO	NATO	kA
odporovaly	odporovat	k5eAaImAgInP
snahám	snaha	k1gFnPc3
o	o	k7c4
zavedení	zavedení	k1gNnSc4
pravidla	pravidlo	k1gNnSc2
souhlasu	souhlas	k1gInSc2
Rady	rada	k1gFnSc2
bezpečnosti	bezpečnost	k1gFnSc2
OSN	OSN	kA
s	s	k7c7
vojenskými	vojenský	k2eAgFnPc7d1
operacemi	operace	k1gFnPc7
Aliance	aliance	k1gFnSc2
<g/>
,	,	kIx,
mezi	mezi	k7c4
které	který	k3yRgFnPc4,k3yIgFnPc4,k3yQgFnPc4
patřila	patřit	k5eAaImAgFnS
např.	např.	kA
operace	operace	k1gFnSc1
v	v	k7c6
Srbsku	Srbsko	k1gNnSc6
v	v	k7c6
roce	rok	k1gInSc6
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nutnost	nutnost	k1gFnSc1
souhlasu	souhlas	k1gInSc2
OSN	OSN	kA
požadovala	požadovat	k5eAaImAgFnS
Francie	Francie	k1gFnSc1
<g/>
,	,	kIx,
Rusko	Rusko	k1gNnSc1
a	a	k8xC
Čína	Čína	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc1
strana	strana	k1gFnSc1
tvrdila	tvrdit	k5eAaImAgFnS
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
to	ten	k3xDgNnSc1
podlomilo	podlomit	k5eAaPmAgNnS
autoritu	autorita	k1gFnSc4
Aliance	aliance	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
poznamenala	poznamenat	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
Rusko	Rusko	k1gNnSc1
a	a	k8xC
Čína	Čína	k1gFnSc1
by	by	kYmCp3nS
útok	útok	k1gInSc1
na	na	k7c6
Jugoslávii	Jugoslávie	k1gFnSc6
vetovaly	vetovat	k5eAaBmAgFnP
a	a	k8xC
stejně	stejně	k6eAd1
tak	tak	k6eAd1
by	by	kYmCp3nP
mohly	moct	k5eAaImAgFnP
dělat	dělat	k5eAaImF
i	i	k9
v	v	k7c6
budoucnosti	budoucnost	k1gFnSc6
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
by	by	kYmCp3nP
zmařily	zmařit	k5eAaPmAgFnP
všechen	všechen	k3xTgInSc4
potenciál	potenciál	k1gInSc4
a	a	k8xC
účel	účel	k1gInSc4
Aliance	aliance	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
91	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Operace	operace	k1gFnSc1
v	v	k7c6
Afghánistánu	Afghánistán	k1gInSc6
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článcích	článek	k1gInPc6
Válka	válka	k1gFnSc1
v	v	k7c6
Afghánistánu	Afghánistán	k1gInSc6
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
–	–	k?
<g/>
současnost	současnost	k1gFnSc4
<g/>
)	)	kIx)
a	a	k8xC
Rozhodná	rozhodný	k2eAgFnSc1d1
podpora	podpora	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Teroristické	teroristický	k2eAgInPc1d1
útoky	útok	k1gInPc1
11	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2001	#num#	k4
přiměly	přimět	k5eAaPmAgInP
NATO	nato	k6eAd1
poprvé	poprvé	k6eAd1
použít	použít	k5eAaPmF
článek	článek	k1gInSc4
5	#num#	k4
Severoatlantické	severoatlantický	k2eAgFnSc2d1
smlouvy	smlouva	k1gFnSc2
</s>
<s>
Teroristické	teroristický	k2eAgInPc4d1
útoky	útok	k1gInPc4
z	z	k7c2
11	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2001	#num#	k4
přiměly	přimět	k5eAaPmAgInP
NATO	nato	k6eAd1
poprvé	poprvé	k6eAd1
v	v	k7c6
historii	historie	k1gFnSc6
uplatnit	uplatnit	k5eAaPmF
článek	článek	k1gInSc4
5	#num#	k4
Severoatlantické	severoatlantický	k2eAgFnSc2d1
smlouvy	smlouva	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
něj	on	k3xPp3gNnSc2
je	být	k5eAaImIp3nS
útok	útok	k1gInSc1
na	na	k7c4
jakéhokoli	jakýkoli	k3yIgMnSc4
člena	člen	k1gMnSc4
Aliance	aliance	k1gFnSc1
považován	považován	k2eAgInSc1d1
za	za	k7c4
útok	útok	k1gInSc4
proti	proti	k7c3
všem	všecek	k3xTgNnPc3
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2001	#num#	k4
NATO	NATO	kA
potvrdilo	potvrdit	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
<g />
.	.	kIx.
</s>
<s hack="1">
zmíněné	zmíněný	k2eAgInPc4d1
teroristické	teroristický	k2eAgInPc4d1
útoky	útok	k1gInPc4
pod	pod	k7c4
tento	tento	k3xDgInSc4
článek	článek	k1gInSc4
spadají	spadat	k5eAaImIp3nP,k5eAaPmIp3nP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
92	#num#	k4
<g/>
]	]	kIx)
Mezi	mezi	k7c4
osm	osm	k4xCc4
oficiálních	oficiální	k2eAgFnPc2d1
operací	operace	k1gFnPc2
provedených	provedený	k2eAgFnPc2d1
NATO	NATO	kA
patří	patřit	k5eAaImIp3nP
operace	operace	k1gFnSc1
Eagle	Eagle	k1gInSc4
Assist	Assist	k1gInSc1
(	(	kIx(
<g/>
Pomoc	pomoc	k1gFnSc1
orla	orel	k1gMnSc2
<g/>
;	;	kIx,
hlídání	hlídání	k1gNnSc2
amerického	americký	k2eAgInSc2d1
vzdušného	vzdušný	k2eAgInSc2d1
prostoru	prostor	k1gInSc2
<g/>
)	)	kIx)
a	a	k8xC
operace	operace	k1gFnSc1
Active	Actiev	k1gFnSc2
Endeavour	Endeavour	k1gInSc1
(	(	kIx(
<g/>
Aktivní	aktivní	k2eAgFnSc1d1
snaha	snaha	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
námořní	námořní	k2eAgFnSc2d1
operace	operace	k1gFnSc2
mající	mající	k2eAgMnPc1d1
za	za	k7c4
cíl	cíl	k1gInSc4
zabránit	zabránit	k5eAaPmF
pohybu	pohyb	k1gInSc3
teroristů	terorista	k1gMnPc2
a	a	k8xC
zbraní	zbraň	k1gFnPc2
hromadného	hromadný	k2eAgNnSc2d1
ničení	ničení	k1gNnSc2
a	a	k8xC
obecně	obecně	k6eAd1
zvýšit	zvýšit	k5eAaPmF
bezpečnost	bezpečnost	k1gFnSc4
námořní	námořní	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
probíhá	probíhat	k5eAaImIp3nS
od	od	k7c2
4	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2001	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
srpnu	srpen	k1gInSc6
2003	#num#	k4
NATO	nato	k6eAd1
započalo	započnout	k5eAaPmAgNnS
svou	svůj	k3xOyFgFnSc4
první	první	k4xOgFnSc4
operaci	operace	k1gFnSc4
mimo	mimo	k7c4
Evropu	Evropa	k1gFnSc4
<g/>
,	,	kIx,
když	když	k8xS
převzala	převzít	k5eAaPmAgFnS
kontrolu	kontrola	k1gFnSc4
nad	nad	k7c7
Mezinárodními	mezinárodní	k2eAgFnPc7d1
bezpečnostními	bezpečnostní	k2eAgFnPc7d1
podpůrnými	podpůrný	k2eAgFnPc7d1
sílami	síla	k1gFnPc7
(	(	kIx(
<g/>
ISAF	ISAF	kA
<g/>
)	)	kIx)
v	v	k7c6
Afghánistánu	Afghánistán	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
93	#num#	k4
<g/>
]	]	kIx)
ISAF	ISAF	kA
měly	mít	k5eAaImAgInP
původně	původně	k6eAd1
za	za	k7c4
úkol	úkol	k1gInSc4
zajistit	zajistit	k5eAaPmF
Kábul	Kábul	k1gInSc4
a	a	k8xC
okolí	okolí	k1gNnSc4
proti	proti	k7c3
Tálibánu	Tálibán	k2eAgFnSc4d1
a	a	k8xC
Al-Káidě	Al-Káida	k1gFnSc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
mohla	moct	k5eAaImAgFnS
fungovat	fungovat	k5eAaImF
vláda	vláda	k1gFnSc1
Hámida	Hámida	k1gFnSc1
Karzaje	Karzaje	k1gFnSc1
<g/>
.	.	kIx.
13	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2003	#num#	k4
Rada	rada	k1gFnSc1
bezpečnosti	bezpečnost	k1gFnSc2
OSN	OSN	kA
jednohlasně	jednohlasně	k6eAd1
schválila	schválit	k5eAaPmAgFnS
rozšíření	rozšíření	k1gNnSc4
působnosti	působnost	k1gFnSc2
ISAF	ISAF	kA
na	na	k7c6
zbytku	zbytek	k1gInSc6
území	území	k1gNnSc2
Afghánistánu	Afghánistán	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
94	#num#	k4
<g/>
]	]	kIx)
K	k	k7c3
březnu	březen	k1gInSc3
2012	#num#	k4
v	v	k7c6
Afghánistánu	Afghánistán	k1gInSc6
působilo	působit	k5eAaImAgNnS
téměř	téměř	k6eAd1
130	#num#	k4
000	#num#	k4
vojáků	voják	k1gMnPc2
pod	pod	k7c7
vedením	vedení	k1gNnSc7
NATO	NATO	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ukončení	ukončení	k1gNnSc1
bojových	bojový	k2eAgFnPc2d1
operací	operace	k1gFnPc2
v	v	k7c6
zemi	zem	k1gFnSc6
proběhlo	proběhnout	k5eAaPmAgNnS
na	na	k7c6
konci	konec	k1gInSc6
roku	rok	k1gInSc2
2014	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
95	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
2015	#num#	k4
byla	být	k5eAaImAgFnS
zahájena	zahájit	k5eAaPmNgFnS
nová	nový	k2eAgFnSc1d1
koaliční	koaliční	k2eAgFnSc1d1
operace	operace	k1gFnSc1
s	s	k7c7
názvem	název	k1gInSc7
Rozhodná	rozhodný	k2eAgFnSc1d1
podpora	podpora	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
má	mít	k5eAaImIp3nS
za	za	k7c4
úkol	úkol	k1gInSc4
s	s	k7c7
využitím	využití	k1gNnSc7
12	#num#	k4
500	#num#	k4
zahraničních	zahraniční	k2eAgMnPc2d1
vojáků	voják	k1gMnPc2
provádět	provádět	k5eAaImF
v	v	k7c6
této	tento	k3xDgFnSc6
zemi	zem	k1gFnSc6
výcvik	výcvik	k1gInSc4
<g/>
,	,	kIx,
poradenství	poradenství	k1gNnSc4
a	a	k8xC
podporu	podpora	k1gFnSc4
afghánských	afghánský	k2eAgFnPc2d1
ozbrojených	ozbrojený	k2eAgFnPc2d1
sil	síla	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
96	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Výcvik	výcvik	k1gInSc1
jednotek	jednotka	k1gFnPc2
v	v	k7c6
Iráku	Irák	k1gInSc6
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Válka	válka	k1gFnSc1
v	v	k7c6
Iráku	Irák	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
NATO	NATO	kA
započala	započnout	k5eAaPmAgFnS
na	na	k7c4
žádost	žádost	k1gFnSc4
irácké	irácký	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
výcvikovou	výcvikový	k2eAgFnSc4d1
misi	mise	k1gFnSc4
v	v	k7c6
Iráku	Irák	k1gInSc6
(	(	kIx(
<g/>
NATO	NATO	kA
Training	Training	k1gInSc1
Mission	Mission	k1gInSc1
Iraq	Iraq	k1gFnSc1
<g/>
,	,	kIx,
NTM-I	NTM-I	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
starala	starat	k5eAaImAgFnS
o	o	k7c4
výcvik	výcvik	k1gInSc4
nové	nový	k2eAgFnSc2d1
irácké	irácký	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
97	#num#	k4
<g/>
]	]	kIx)
Cílem	cíl	k1gInSc7
bylo	být	k5eAaImAgNnS
poskytnout	poskytnout	k5eAaPmF
demokraticky	demokraticky	k6eAd1
vedený	vedený	k2eAgInSc1d1
a	a	k8xC
schopný	schopný	k2eAgInSc1d1
obranný	obranný	k2eAgInSc1d1
sektor	sektor	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
98	#num#	k4
<g/>
]	]	kIx)
Spolupracovala	spolupracovat	k5eAaImAgFnS
nejdříve	dříve	k6eAd3
s	s	k7c7
Koalicí	koalice	k1gFnSc7
mnohonárodních	mnohonárodní	k2eAgFnPc2d1
sil	síla	k1gFnPc2
(	(	kIx(
<g/>
Multi-National	Multi-National	k1gFnSc1
Force	force	k1gFnSc1
Iraq	Iraq	k1gFnSc1
<g/>
,	,	kIx,
MNF-I	MNF-I	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
99	#num#	k4
<g/>
]	]	kIx)
po	po	k7c6
jejím	její	k3xOp3gInSc6
zániku	zánik	k1gInSc6
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
pak	pak	k6eAd1
s	s	k7c7
nově	nově	k6eAd1
vzniklou	vzniklý	k2eAgFnSc7d1
složkou	složka	k1gFnSc7
United	United	k1gMnSc1
States	States	k1gMnSc1
Forces	Forces	k1gMnSc1
–	–	k?
Iraq	Iraq	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původně	původně	k6eAd1
se	se	k3xPyFc4
mise	mise	k1gFnSc1
soustředila	soustředit	k5eAaPmAgFnS
hlavně	hlavně	k9
na	na	k7c4
výcvik	výcvik	k1gInSc4
vysoce	vysoce	k6eAd1
postavených	postavený	k2eAgMnPc2d1
důstojníků	důstojník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
září	září	k1gNnSc6
2005	#num#	k4
NATO	NATO	kA
otevřelo	otevřít	k5eAaPmAgNnS
na	na	k7c6
kraji	kraj	k1gInSc6
Bagdádu	Bagdád	k1gInSc2
vojenskou	vojenský	k2eAgFnSc4d1
akademii	akademie	k1gFnSc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
100	#num#	k4
<g/>
]	]	kIx)
ve	v	k7c6
které	který	k3yIgFnSc6,k3yQgFnSc6,k3yRgFnSc6
do	do	k7c2
konce	konec	k1gInSc2
února	únor	k1gInSc2
2009	#num#	k4
působili	působit	k5eAaImAgMnP
čtyři	čtyři	k4xCgMnPc4
čeští	český	k2eAgMnPc1d1
vojáci	voják	k1gMnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
101	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
říjnu	říjen	k1gInSc6
2007	#num#	k4
se	se	k3xPyFc4
operace	operace	k1gFnSc1
rozšířila	rozšířit	k5eAaPmAgFnS
na	na	k7c4
výcvik	výcvik	k1gInSc4
federální	federální	k2eAgFnSc2d1
policie	policie	k1gFnSc2
a	a	k8xC
v	v	k7c6
prosinci	prosinec	k1gInSc6
2008	#num#	k4
na	na	k7c4
další	další	k2eAgFnSc4d1
složky	složka	k1gFnPc4
jako	jako	k8xS,k8xC
např.	např.	kA
loďstvo	loďstvo	k1gNnSc1
a	a	k8xC
letectvo	letectvo	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
97	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
K	k	k7c3
31	#num#	k4
<g/>
.	.	kIx.
prosinci	prosinec	k1gInSc6
2011	#num#	k4
mise	mise	k1gFnSc1
skončila	skončit	k5eAaPmAgFnS
<g/>
,	,	kIx,
když	když	k8xS
vypršela	vypršet	k5eAaPmAgFnS
platnost	platnost	k1gFnSc4
dohody	dohoda	k1gFnSc2
o	o	k7c4
její	její	k3xOp3gFnSc4
existenci	existence	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Operace	operace	k1gFnSc1
se	se	k3xPyFc4
zúčastnilo	zúčastnit	k5eAaPmAgNnS
23	#num#	k4
členských	členský	k2eAgInPc2d1
států	stát	k1gInPc2
NATO	NATO	kA
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
přispěly	přispět	k5eAaPmAgInP
celkově	celkově	k6eAd1
částkou	částka	k1gFnSc7
téměř	téměř	k6eAd1
250	#num#	k4
milionů	milion	k4xCgInPc2
dolarů	dolar	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
98	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Operace	operace	k1gFnSc1
Ocean	Ocean	k1gMnSc1
Shield	Shield	k1gMnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Pirátství	pirátství	k1gNnSc2
v	v	k7c6
Somálsku	Somálsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2009	#num#	k4
zahájilo	zahájit	k5eAaPmAgNnS
NATO	nato	k6eAd1
operaci	operace	k1gFnSc4
Ocean	Oceana	k1gFnPc2
Shield	Shielda	k1gFnPc2
(	(	kIx(
<g/>
Oceánský	oceánský	k2eAgInSc1d1
štít	štít	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
spočívá	spočívat	k5eAaImIp3nS
v	v	k7c6
ochraně	ochrana	k1gFnSc6
námořní	námořní	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
v	v	k7c6
Adenském	adenský	k2eAgInSc6d1
zálivu	záliv	k1gInSc6
a	a	k8xC
Indickém	indický	k2eAgInSc6d1
oceánu	oceán	k1gInSc6
(	(	kIx(
<g/>
okolí	okolí	k1gNnSc6
Afrického	africký	k2eAgInSc2d1
rohu	roh	k1gInSc2
<g/>
)	)	kIx)
proti	proti	k7c3
somálským	somálský	k2eAgMnPc3d1
pirátům	pirát	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navazuje	navazovat	k5eAaImIp3nS
tak	tak	k9
na	na	k7c4
předchozí	předchozí	k2eAgFnPc4d1
operace	operace	k1gFnPc4
Allied	Allied	k1gMnSc1
Provider	Provider	k1gMnSc1
(	(	kIx(
<g/>
říjen	říjen	k1gInSc1
<g/>
–	–	k?
<g/>
prosinec	prosinec	k1gInSc1
2008	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Allied	Allied	k1gMnSc1
Protector	Protector	k1gMnSc1
(	(	kIx(
<g/>
březen-srpen	březen-srpen	k2eAgMnSc1d1
2009	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
probíhaly	probíhat	k5eAaImAgFnP
také	také	k9
v	v	k7c6
Adenském	adenský	k2eAgInSc6d1
zálivu	záliv	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavním	hlavní	k2eAgInSc7d1
cílem	cíl	k1gInSc7
operace	operace	k1gFnSc1
Allied	Allied	k1gMnSc1
Provider	Provider	k1gMnSc1
bylo	být	k5eAaImAgNnS
zabezpečení	zabezpečení	k1gNnSc4
civilních	civilní	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
Světového	světový	k2eAgInSc2d1
potravinového	potravinový	k2eAgInSc2d1
programu	program	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
do	do	k7c2
Afriky	Afrika	k1gFnSc2
dovážely	dovážet	k5eAaImAgFnP
potraviny	potravina	k1gFnPc1
<g/>
,	,	kIx,
a	a	k8xC
operace	operace	k1gFnSc1
Allied	Allied	k1gInSc4
Protector	Protector	k1gInSc4
rozšířila	rozšířit	k5eAaPmAgFnS
působnost	působnost	k1gFnSc1
i	i	k9
na	na	k7c4
komerční	komerční	k2eAgNnPc4d1
plavidla	plavidlo	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Operace	operace	k1gFnSc1
Ocean	Oceana	k1gFnPc2
Shield	Shield	k1gInSc4
kromě	kromě	k7c2
samotné	samotný	k2eAgFnSc2d1
kontroly	kontrola	k1gFnSc2
vod	voda	k1gFnPc2
poskytuje	poskytovat	k5eAaImIp3nS
okolním	okolní	k2eAgFnPc3d1
zemím	zem	k1gFnPc3
výcvik	výcvik	k1gInSc1
vlastních	vlastní	k2eAgFnPc2d1
protipirátských	protipirátský	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
102	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Intervence	intervence	k1gFnPc1
v	v	k7c6
Libyi	Libye	k1gFnSc6
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článcích	článek	k1gInPc6
Vojenská	vojenský	k2eAgFnSc1d1
intervence	intervence	k1gFnSc1
v	v	k7c6
Libyi	Libye	k1gFnSc6
a	a	k8xC
Občanská	občanský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
v	v	k7c6
Libyi	Libye	k1gFnSc6
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Start	start	k1gInSc1
řízené	řízený	k2eAgFnSc2d1
střely	střela	k1gFnSc2
Tomahawk	tomahawk	k1gInSc1
z	z	k7c2
amerického	americký	k2eAgMnSc2d1
torpédoborce	torpédoborec	k1gMnSc2
USS	USS	kA
Barry	Barra	k1gFnSc2
vyslané	vyslaný	k2eAgFnSc2d1
na	na	k7c4
podporu	podpora	k1gFnSc4
operace	operace	k1gFnSc2
proti	proti	k7c3
Libyi	Libye	k1gFnSc3
<g/>
,	,	kIx,
29	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2011	#num#	k4
</s>
<s>
Během	během	k7c2
občanské	občanský	k2eAgFnSc2d1
války	válka	k1gFnSc2
v	v	k7c6
Libyi	Libye	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
vyeskalovalo	vyeskalovat	k5eAaImAgNnS,k5eAaPmAgNnS,k5eAaBmAgNnS
násilí	násilí	k1gNnSc1
mezi	mezi	k7c7
protestanty	protestant	k1gMnPc7
a	a	k8xC
libyjskou	libyjský	k2eAgFnSc7d1
vládou	vláda	k1gFnSc7
v	v	k7c6
čele	čelo	k1gNnSc6
s	s	k7c7
Muammarem	Muammar	k1gMnSc7
Kaddáfím	Kaddáfí	k1gMnSc7
natolik	natolik	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
Rada	rada	k1gFnSc1
bezpečnosti	bezpečnost	k1gFnSc2
OSN	OSN	kA
17	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2011	#num#	k4
schválila	schválit	k5eAaPmAgFnS
rezoluci	rezoluce	k1gFnSc4
č.	č.	k?
1973	#num#	k4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
požadovala	požadovat	k5eAaImAgFnS
příměří	příměří	k1gNnSc4
a	a	k8xC
schválila	schválit	k5eAaPmAgFnS
vojenský	vojenský	k2eAgInSc4d1
zásah	zásah	k1gInSc4
za	za	k7c7
účelem	účel	k1gInSc7
obrany	obrana	k1gFnSc2
civilního	civilní	k2eAgNnSc2d1
obyvatelstva	obyvatelstvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Koalice	koalice	k1gFnSc1
<g/>
,	,	kIx,
ve	v	k7c6
které	který	k3yIgFnSc6,k3yQgFnSc6,k3yRgFnSc6
bylo	být	k5eAaImAgNnS
i	i	k9
několik	několik	k4yIc1
členů	člen	k1gInPc2
NATO	NATO	kA
<g/>
,	,	kIx,
zřídila	zřídit	k5eAaPmAgFnS
nedlouho	dlouho	k6eNd1
poté	poté	k6eAd1
nad	nad	k7c7
Libyí	Libye	k1gFnSc7
bezletovou	bezletový	k2eAgFnSc7d1
zónu	zóna	k1gFnSc4
<g/>
.	.	kIx.
20	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2011	#num#	k4
schválilo	schválit	k5eAaPmAgNnS
NATO	nato	k6eAd1
embargo	embargo	k1gNnSc1
na	na	k7c4
dovoz	dovoz	k1gInSc4
zbraní	zbraň	k1gFnSc7
libyjskému	libyjský	k2eAgInSc3d1
režimu	režim	k1gInSc3
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yIgFnPc4,k3yRgFnPc4
prosazovala	prosazovat	k5eAaImAgFnS
skrze	skrze	k?
operaci	operace	k1gFnSc3
Unified	Unified	k1gMnSc1
Protector	Protector	k1gMnSc1
(	(	kIx(
<g/>
Sjednocený	sjednocený	k2eAgMnSc1d1
ochránce	ochránce	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
měla	mít	k5eAaImAgFnS
za	za	k7c4
úkol	úkol	k1gInSc4
„	„	k?
<g/>
kontrolovat	kontrolovat	k5eAaImF
<g/>
,	,	kIx,
nahlásit	nahlásit	k5eAaPmF
a	a	k8xC
v	v	k7c6
případě	případ	k1gInSc6
potřeby	potřeba	k1gFnSc2
zastavit	zastavit	k5eAaPmF
lodě	loď	k1gFnPc4
podezřívané	podezřívaný	k2eAgFnPc4d1
z	z	k7c2
nelegálního	legální	k2eNgInSc2d1
převozu	převoz	k1gInSc2
zbraní	zbraň	k1gFnPc2
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
103	#num#	k4
<g/>
]	]	kIx)
24	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
NATO	nato	k6eAd1
souhlasilo	souhlasit	k5eAaImAgNnS
s	s	k7c7
převzetím	převzetí	k1gNnSc7
moci	moc	k1gFnSc2
nad	nad	k7c7
bezletovou	bezletový	k2eAgFnSc7d1
zónou	zóna	k1gFnSc7
od	od	k7c2
původní	původní	k2eAgFnSc2d1
koalice	koalice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moc	moc	k6eAd1
nad	nad	k7c7
pozemními	pozemní	k2eAgFnPc7d1
jednotkami	jednotka	k1gFnPc7
zůstala	zůstat	k5eAaPmAgFnS
v	v	k7c6
rukou	ruka	k1gFnPc6
koalice	koalice	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
104	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ne	ne	k9
všechny	všechen	k3xTgFnPc1
členské	členský	k2eAgFnPc1d1
země	zem	k1gFnPc1
se	se	k3xPyFc4
však	však	k9
bojových	bojový	k2eAgFnPc2d1
operací	operace	k1gFnPc2
zúčastnily	zúčastnit	k5eAaPmAgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
června	červen	k1gInSc2
2011	#num#	k4
jich	on	k3xPp3gFnPc2
bylo	být	k5eAaImAgNnS
jen	jen	k9
8	#num#	k4
z	z	k7c2
tehdy	tehdy	k6eAd1
celkových	celkový	k2eAgFnPc2d1
28	#num#	k4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
105	#num#	k4
<g/>
]	]	kIx)
což	což	k3yQnSc1,k3yRnSc1
vyústilo	vyústit	k5eAaPmAgNnS
v	v	k7c4
konflikt	konflikt	k1gInSc4
mezi	mezi	k7c7
americkým	americký	k2eAgMnSc7d1
ministrem	ministr	k1gMnSc7
obrany	obrana	k1gFnSc2
Robertem	Robert	k1gMnSc7
Gatesem	Gates	k1gMnSc7
a	a	k8xC
státy	stát	k1gInPc1
jako	jako	k8xC,k8xS
Polsko	Polsko	k1gNnSc1
<g/>
,	,	kIx,
Španělsko	Španělsko	k1gNnSc1
<g/>
,	,	kIx,
Nizozemsko	Nizozemsko	k1gNnSc1
<g/>
,	,	kIx,
Turecko	Turecko	k1gNnSc1
a	a	k8xC
Německo	Německo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gates	Gates	k1gInSc1
po	po	k7c6
těchto	tento	k3xDgFnPc6
zemích	zem	k1gFnPc6
chtěl	chtít	k5eAaImAgMnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
na	na	k7c6
operaci	operace	k1gFnSc6
podílely	podílet	k5eAaImAgInP
více	hodně	k6eAd2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
106	#num#	k4
<g/>
]	]	kIx)
Ve	v	k7c6
svém	svůj	k3xOyFgInSc6
proslovu	proslov	k1gInSc6
v	v	k7c6
Bruselu	Brusel	k1gInSc6
10	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
Gates	Gates	k1gMnSc1
dále	daleko	k6eAd2
kritizoval	kritizovat	k5eAaImAgMnS
nečinné	činný	k2eNgFnPc4d1
země	zem	k1gFnPc4
s	s	k7c7
poznámkou	poznámka	k1gFnSc7
<g/>
,	,	kIx,
že	že	k8xS
jejich	jejich	k3xOp3gNnSc1
jednání	jednání	k1gNnSc1
by	by	kYmCp3nS
mohlo	moct	k5eAaImAgNnS
způsobit	způsobit	k5eAaPmF
rozpad	rozpad	k1gInSc4
NATO	NATO	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
107	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Operace	operace	k1gFnSc1
v	v	k7c6
Libyi	Libye	k1gFnSc6
byla	být	k5eAaImAgFnS
prodloužena	prodloužit	k5eAaPmNgFnS
do	do	k7c2
září	září	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
108	#num#	k4
<g/>
]	]	kIx)
Týden	týden	k1gInSc1
na	na	k7c4
to	ten	k3xDgNnSc4
Norsko	Norsko	k1gNnSc1
oznámilo	oznámit	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
postupně	postupně	k6eAd1
utlumí	utlumit	k5eAaPmIp3nS
svou	svůj	k3xOyFgFnSc4
účast	účast	k1gFnSc4
na	na	k7c4
operaci	operace	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
kompletně	kompletně	k6eAd1
ukončí	ukončit	k5eAaPmIp3nS
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
srpnu	srpen	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
109	#num#	k4
<g/>
]	]	kIx)
Stažení	stažení	k1gNnSc4
norských	norský	k2eAgInPc2d1
stíhacích	stíhací	k2eAgInPc2d1
letounů	letoun	k1gInPc2
<g />
.	.	kIx.
</s>
<s hack="1">
nakonec	nakonec	k6eAd1
proběhlo	proběhnout	k5eAaPmAgNnS
4	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
110	#num#	k4
<g/>
]	]	kIx)
Velitel	velitel	k1gMnSc1
Royal	Royal	k1gMnSc1
Navy	Navy	k?
prohlásil	prohlásit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
britská	britský	k2eAgFnSc1d1
účast	účast	k1gFnSc1
je	být	k5eAaImIp3nS
kvůli	kvůli	k7c3
škrtům	škrt	k1gInPc3
v	v	k7c6
rozpočtu	rozpočet	k1gInSc6
neudržitelná	udržitelný	k2eNgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
111	#num#	k4
<g/>
]	]	kIx)
Operace	operace	k1gFnPc1
nakonec	nakonec	k6eAd1
probíhaly	probíhat	k5eAaImAgFnP
až	až	k9
do	do	k7c2
října	říjen	k1gInSc2
<g/>
;	;	kIx,
20	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
byl	být	k5eAaImAgMnS
<g />
.	.	kIx.
</s>
<s hack="1">
zabit	zabit	k2eAgInSc4d1
Muammar	Muammar	k1gInSc4
Kaddáfí	Kaddáf	k1gFnPc2
a	a	k8xC
31	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
byla	být	k5eAaImAgFnS
oficiálně	oficiálně	k6eAd1
ukončena	ukončen	k2eAgFnSc1d1
intervence	intervence	k1gFnSc1
NATO	NATO	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
112	#num#	k4
<g/>
]	]	kIx)
Intervence	intervence	k1gFnSc2
NATO	NATO	kA
pomohla	pomoct	k5eAaPmAgFnS
svrhnout	svrhnout	k5eAaPmF
a	a	k8xC
zavraždit	zavraždit	k5eAaPmF
vůdce	vůdce	k1gMnSc4
Libye	Libye	k1gFnSc1
<g/>
,	,	kIx,
diktátora	diktátor	k1gMnSc2
Muammara	Muammar	k1gMnSc2
Kaddáfího	Kaddáfí	k1gMnSc2
<g/>
,	,	kIx,
na	na	k7c4
druhou	druhý	k4xOgFnSc4
stranu	strana	k1gFnSc4
uvrhla	uvrhnout	k5eAaPmAgFnS
celou	celý	k2eAgFnSc4d1
zemi	zem	k1gFnSc4
do	do	k7c2
krvavého	krvavý	k2eAgInSc2d1
chaosu	chaos	k1gInSc2
<g/>
,	,	kIx,
trvajícího	trvající	k2eAgNnSc2d1
dodnes	dodnes	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Spolupráce	spolupráce	k1gFnSc1
s	s	k7c7
nečlenskými	členský	k2eNgInPc7d1
státy	stát	k1gInPc7
</s>
<s>
Euroatlantické	euroatlantický	k2eAgNnSc1d1
partnerství	partnerství	k1gNnSc1
</s>
<s>
NATO	nato	k6eAd1
zřídilo	zřídit	k5eAaPmAgNnS
Euroatlantickou	euroatlantický	k2eAgFnSc4d1
radu	rada	k1gFnSc4
partnerství	partnerství	k1gNnSc2
a	a	k8xC
Partnerství	partnerství	k1gNnSc2
pro	pro	k7c4
mír	mír	k1gInSc4
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
slouží	sloužit	k5eAaImIp3nP
ke	k	k7c3
spolupráci	spolupráce	k1gFnSc3
30	#num#	k4
států	stát	k1gInPc2
NATO	NATO	kA
a	a	k8xC
21	#num#	k4
„	„	k?
<g/>
partnerských	partnerský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
.	.	kIx.
<g/>
“	“	k?
</s>
<s>
Partnerství	partnerství	k1gNnSc1
pro	pro	k7c4
mír	mír	k1gInSc4
(	(	kIx(
<g/>
PfP	PfP	k1gMnPc2
<g/>
)	)	kIx)
bylo	být	k5eAaImAgNnS
zřízeno	zřídit	k5eAaPmNgNnS
v	v	k7c6
roce	rok	k1gInSc6
1994	#num#	k4
a	a	k8xC
je	být	k5eAaImIp3nS
založeno	založit	k5eAaPmNgNnS
na	na	k7c6
individuální	individuální	k2eAgFnSc6d1
vojenské	vojenský	k2eAgFnSc6d1
spolupráci	spolupráce	k1gFnSc6
každého	každý	k3xTgInSc2
partnerského	partnerský	k2eAgInSc2d1
státu	stát	k1gInSc2
s	s	k7c7
Aliancí	aliance	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
113	#num#	k4
<g/>
]	]	kIx)
Mezi	mezi	k7c7
členy	člen	k1gMnPc7
patří	patřit	k5eAaImIp3nS
všechny	všechen	k3xTgInPc1
současné	současný	k2eAgInPc1d1
a	a	k8xC
bývalé	bývalý	k2eAgInPc1d1
členské	členský	k2eAgInPc1d1
státy	stát	k1gInPc1
Společenství	společenství	k1gNnSc2
nezávislých	závislý	k2eNgInPc2d1
států	stát	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bělorusko	Bělorusko	k1gNnSc1
se	se	k3xPyFc4
k	k	k7c3
Partnerství	partnerství	k1gNnSc3
pro	pro	k7c4
mír	mír	k1gInSc4
připojilo	připojit	k5eAaPmAgNnS
v	v	k7c6
roce	rok	k1gInSc6
1995	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
114	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Euroatlantická	euroatlantický	k2eAgFnSc1d1
rada	rada	k1gFnSc1
partnerství	partnerství	k1gNnSc2
(	(	kIx(
<g/>
EAPC	EAPC	kA
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
ustanovena	ustanovit	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1997	#num#	k4
a	a	k8xC
slouží	sloužit	k5eAaImIp3nS
jako	jako	k9
fórum	fórum	k1gNnSc1
politické	politický	k2eAgFnSc2d1
výměny	výměna	k1gFnSc2
názorů	názor	k1gInPc2
mezi	mezi	k7c7
všemi	všecek	k3xTgFnPc7
padesáti	padesát	k4xCc7
státy	stát	k1gInPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
115	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Státy	stát	k1gInPc1
účastnící	účastnící	k2eAgInPc1d1
se	se	k3xPyFc4
partnerství	partnerství	k1gNnSc2
s	s	k7c7
NATO	NATO	kA
<g/>
[	[	kIx(
<g/>
116	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Partnerství	partnerství	k1gNnSc1
pro	pro	k7c4
mír	mír	k1gInSc4
</s>
<s>
Středomořskýdialog	Středomořskýdialog	k1gMnSc1
</s>
<s>
Kontaktní	kontaktní	k2eAgFnPc1d1
země	zem	k1gFnPc1
</s>
<s>
Istanbulská	istanbulský	k2eAgFnSc1d1
iniciativa	iniciativa	k1gFnSc1
spolupráce	spolupráce	k1gFnSc2
</s>
<s>
Společenstvínezávislých	Společenstvínezávislý	k2eAgInPc2d1
států	stát	k1gInPc2
</s>
<s>
Ostatní	ostatní	k2eAgFnSc3d1
socialistickézemě	socialistickézema	k1gFnSc3
studené	studený	k2eAgFnSc2d1
války	válka	k1gFnSc2
</s>
<s>
Neutrální	neutrální	k2eAgFnSc3d1
kapitalistickézemě	kapitalistickézema	k1gFnSc3
studené	studený	k2eAgFnSc2d1
války	válka	k1gFnSc2
</s>
<s>
Arménie	Arménie	k1gFnSc1
Arménie	Arménie	k1gFnSc2
</s>
<s>
Jugoslávie	Jugoslávie	k1gFnSc1
</s>
<s>
Rakousko	Rakousko	k1gNnSc1
Rakousko	Rakousko	k1gNnSc1
</s>
<s>
Alžírsko	Alžírsko	k1gNnSc1
Alžírsko	Alžírsko	k1gNnSc1
</s>
<s>
Austrálie	Austrálie	k1gFnSc1
Austrálie	Austrálie	k1gFnSc2
</s>
<s>
Bahrajn	Bahrajn	k1gMnSc1
Bahrajn	Bahrajn	k1gMnSc1
</s>
<s>
Ázerbájdžán	Ázerbájdžán	k1gInSc1
Ázerbájdžán	Ázerbájdžán	k1gInSc1
</s>
<s>
Bosna	Bosna	k1gFnSc1
a	a	k8xC
Hercegovina	Hercegovina	k1gFnSc1
Bosna	Bosna	k1gFnSc1
a	a	k8xC
Hercegovina	Hercegovina	k1gFnSc1
</s>
<s>
Finsko	Finsko	k1gNnSc1
Finsko	Finsko	k1gNnSc1
</s>
<s>
Egypt	Egypt	k1gInSc1
Egypt	Egypt	k1gInSc1
</s>
<s>
Japonsko	Japonsko	k1gNnSc1
Japonsko	Japonsko	k1gNnSc1
</s>
<s>
Katar	katar	k1gMnSc1
Katar	katar	k1gMnSc1
</s>
<s>
Bělorusko	Bělorusko	k1gNnSc1
Bělorusko	Bělorusko	k1gNnSc1
</s>
<s>
Srbsko	Srbsko	k1gNnSc1
Srbsko	Srbsko	k1gNnSc1
</s>
<s>
Irsko	Irsko	k1gNnSc1
Irsko	Irsko	k1gNnSc1
</s>
<s>
Izrael	Izrael	k1gInSc1
Izrael	Izrael	k1gInSc1
</s>
<s>
Nový	nový	k2eAgInSc1d1
Zéland	Zéland	k1gInSc1
Nový	nový	k2eAgInSc1d1
Zéland	Zéland	k1gInSc1
</s>
<s>
Kuvajt	Kuvajt	k1gInSc1
Kuvajt	Kuvajt	k1gInSc1
</s>
<s>
Kazachstán	Kazachstán	k1gInSc1
Kazachstán	Kazachstán	k1gInSc1
</s>
<s>
Malta	Malta	k1gFnSc1
Malta	Malta	k1gFnSc1
</s>
<s>
Jordánsko	Jordánsko	k1gNnSc1
Jordánsko	Jordánsko	k1gNnSc1
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
arabské	arabský	k2eAgInPc1d1
emiráty	emirát	k1gInPc1
Spojené	spojený	k2eAgInPc1d1
arabské	arabský	k2eAgInPc1d1
emiráty	emirát	k1gInPc1
</s>
<s>
Kyrgyzstán	Kyrgyzstán	k2eAgInSc1d1
Kyrgyzstán	Kyrgyzstán	k1gInSc1
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
Mauritánie	Mauritánie	k1gFnSc1
Mauritánie	Mauritánie	k1gFnSc2
</s>
<s>
Irák	Irák	k1gInSc1
Irák	Irák	k1gInSc1
</s>
<s>
Moldavsko	Moldavsko	k1gNnSc1
Moldavsko	Moldavsko	k1gNnSc1
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1
Švýcarsko	Švýcarsko	k1gNnSc1
</s>
<s>
Maroko	Maroko	k1gNnSc1
Maroko	Maroko	k1gNnSc1
</s>
<s>
Afghánistán	Afghánistán	k1gInSc1
Afghánistán	Afghánistán	k1gInSc1
</s>
<s>
Rusko	Rusko	k1gNnSc1
Rusko	Rusko	k1gNnSc1
</s>
<s>
Gruzie	Gruzie	k1gFnSc1
Gruzie	Gruzie	k1gFnSc2
</s>
<s>
Tunisko	Tunisko	k1gNnSc1
Tunisko	Tunisko	k1gNnSc1
</s>
<s>
Pákistán	Pákistán	k1gInSc1
Pákistán	Pákistán	k1gInSc1
</s>
<s>
Tádžikistán	Tádžikistán	k1gInSc1
Tádžikistán	Tádžikistán	k1gInSc1
</s>
<s>
Turkmenistán	Turkmenistán	k1gInSc1
Turkmenistán	Turkmenistán	k1gInSc1
</s>
<s>
Uzbekistán	Uzbekistán	k1gInSc1
Uzbekistán	Uzbekistán	k1gInSc1
</s>
<s>
Ukrajina	Ukrajina	k1gFnSc1
Ukrajina	Ukrajina	k1gFnSc1
</s>
<s>
Středomořský	středomořský	k2eAgInSc4d1
dialog	dialog	k1gInSc4
a	a	k8xC
ICI	ICI	kA
</s>
<s>
Středomořský	středomořský	k2eAgInSc1d1
dialog	dialog	k1gInSc1
byl	být	k5eAaImAgInS
zahájen	zahájit	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
1994	#num#	k4
a	a	k8xC
je	být	k5eAaImIp3nS
podobný	podobný	k2eAgInSc1d1
Partnerství	partnerství	k1gNnSc2
pro	pro	k7c4
mír	mír	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
soustředí	soustředit	k5eAaPmIp3nS
se	se	k3xPyFc4
na	na	k7c4
severoafrické	severoafrický	k2eAgInPc4d1
státy	stát	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Istanbulská	istanbulský	k2eAgFnSc1d1
iniciativa	iniciativa	k1gFnSc1
spolupráce	spolupráce	k1gFnSc2
(	(	kIx(
<g/>
ICI	ICI	kA
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
vyhlášena	vyhlásit	k5eAaPmNgFnS
na	na	k7c6
summitu	summit	k1gInSc6
v	v	k7c6
Istanbulu	Istanbul	k1gInSc6
v	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
a	a	k8xC
soustředí	soustředit	k5eAaPmIp3nS
se	se	k3xPyFc4
na	na	k7c4
státy	stát	k1gInPc4
širšího	široký	k2eAgInSc2d2
Blízkého	blízký	k2eAgInSc2d1
východu	východ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Individuální	individuální	k2eAgInSc1d1
akční	akční	k2eAgInSc1d1
plán	plán	k1gInSc1
partnerství	partnerství	k1gNnSc2
</s>
<s>
Individuální	individuální	k2eAgInPc1d1
akční	akční	k2eAgInPc1d1
plány	plán	k1gInPc1
partnerství	partnerství	k1gNnSc2
byly	být	k5eAaImAgFnP
zahájeny	zahájit	k5eAaPmNgFnP
v	v	k7c6
roce	rok	k1gInSc6
2002	#num#	k4
na	na	k7c6
summitu	summit	k1gInSc6
v	v	k7c6
Praze	Praha	k1gFnSc6
</s>
<s>
V	v	k7c6
listopadu	listopad	k1gInSc6
2002	#num#	k4
byly	být	k5eAaImAgInP
na	na	k7c6
pražském	pražský	k2eAgInSc6d1
summitu	summit	k1gInSc6
zahájeny	zahájen	k2eAgInPc4d1
Individuální	individuální	k2eAgInPc4d1
akční	akční	k2eAgInPc4d1
plány	plán	k1gInPc4
partnerství	partnerství	k1gNnSc2
(	(	kIx(
<g/>
IPAP	IPAP	kA
<g/>
)	)	kIx)
a	a	k8xC
je	být	k5eAaImIp3nS
otevřen	otevřít	k5eAaPmNgInS
zemím	zem	k1gFnPc3
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
mají	mít	k5eAaImIp3nP
politickou	politický	k2eAgFnSc4d1
vůli	vůle	k1gFnSc4
a	a	k8xC
schopnosti	schopnost	k1gFnPc4
prohloubit	prohloubit	k5eAaPmF
své	svůj	k3xOyFgInPc4
vztahy	vztah	k1gInPc4
s	s	k7c7
NATO	NATO	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
117	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
současnosti	současnost	k1gFnSc6
se	se	k3xPyFc4
IPAP	IPAP	kA
účastní	účastnit	k5eAaImIp3nS
následující	následující	k2eAgInPc4d1
státy	stát	k1gInPc4
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
118	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Gruzie	Gruzie	k1gFnSc1
Gruzie	Gruzie	k1gFnSc1
(	(	kIx(
<g/>
od	od	k7c2
29	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2004	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ázerbájdžán	Ázerbájdžán	k1gInSc1
Ázerbájdžán	Ázerbájdžán	k1gInSc1
(	(	kIx(
<g/>
od	od	k7c2
27	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2005	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Arménie	Arménie	k1gFnSc1
Arménie	Arménie	k1gFnSc2
(	(	kIx(
<g/>
od	od	k7c2
16	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2005	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Kazachstán	Kazachstán	k1gInSc1
Kazachstán	Kazachstán	k1gInSc1
(	(	kIx(
<g/>
od	od	k7c2
31	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2006	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Moldavsko	Moldavsko	k1gNnSc1
Moldavsko	Moldavsko	k1gNnSc1
(	(	kIx(
<g/>
od	od	k7c2
19	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2006	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Srbsko	Srbsko	k1gNnSc1
Srbsko	Srbsko	k1gNnSc1
(	(	kIx(
<g/>
od	od	k7c2
15	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2015	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Kontaktní	kontaktní	k2eAgFnPc1d1
země	zem	k1gFnPc1
</s>
<s>
Od	od	k7c2
devadesátých	devadesátý	k4xOgNnPc2
let	léto	k1gNnPc2
začala	začít	k5eAaPmAgFnS
Aliance	aliance	k1gFnSc1
spolupracovat	spolupracovat	k5eAaImF
se	s	k7c7
státy	stát	k1gInPc7
<g/>
,	,	kIx,
se	s	k7c7
kterými	který	k3yIgInPc7,k3yQgInPc7,k3yRgInPc7
nemá	mít	k5eNaImIp3nS
uzavřené	uzavřený	k2eAgNnSc1d1
žádné	žádný	k3yNgFnPc4
z	z	k7c2
výše	výše	k1gFnSc2,k1gFnSc2wB
uvedených	uvedený	k2eAgNnPc2d1
oficiálních	oficiální	k2eAgNnPc2d1
partnerství	partnerství	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Např.	např.	kA
Argentina	Argentina	k1gFnSc1
a	a	k8xC
Chile	Chile	k1gNnSc1
spolupracovaly	spolupracovat	k5eAaImAgInP
s	s	k7c7
NATO	NATO	kA
ve	v	k7c6
válce	válka	k1gFnSc6
v	v	k7c6
Bosně	Bosna	k1gFnSc6
a	a	k8xC
Hercegovině	Hercegovina	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1998	#num#	k4
Aliance	aliance	k1gFnSc1
vytvořila	vytvořit	k5eAaPmAgFnS
obecné	obecný	k2eAgFnPc4d1
směrnice	směrnice	k1gFnPc4
týkající	týkající	k2eAgFnPc4d1
se	se	k3xPyFc4
vztahů	vztah	k1gInPc2
s	s	k7c7
ostatními	ostatní	k2eAgFnPc7d1
zeměmi	zem	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Státy	stát	k1gInPc1
ochotné	ochotný	k2eAgInPc1d1
s	s	k7c7
NATO	NATO	kA
spolupracovat	spolupracovat	k5eAaImF
se	se	k3xPyFc4
nazývají	nazývat	k5eAaImIp3nP
„	„	k?
<g/>
kontaktní	kontaktní	k2eAgFnPc1d1
země	zem	k1gFnPc1
<g/>
“	“	k?
nebo	nebo	k8xC
„	„	k?
<g/>
další	další	k2eAgMnPc1d1
globální	globální	k2eAgMnPc1d1
partneři	partner	k1gMnPc1
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
119	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Struktura	struktura	k1gFnSc1
</s>
<s>
Anders	Anders	k1gInSc1
Fogh	Fogh	k1gInSc1
Rasmussen	Rasmussen	k1gInSc4
(	(	kIx(
<g/>
vlevo	vlevo	k6eAd1
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
od	od	k7c2
srpna	srpen	k1gInSc2
2009	#num#	k4
do	do	k7c2
října	říjen	k1gInSc2
2014	#num#	k4
generálním	generální	k2eAgMnSc7d1
tajemníkem	tajemník	k1gMnSc7
NATO	NATO	kA
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1
velitelství	velitelství	k1gNnSc1
NATO	NATO	kA
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
Harenu	Haren	k1gInSc6
v	v	k7c6
Bruselu	Brusel	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
120	#num#	k4
<g/>
]	]	kIx)
Pracují	pracovat	k5eAaImIp3nP
v	v	k7c6
něm	on	k3xPp3gNnSc6
delegace	delegace	k1gFnSc1
členských	členský	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
,	,	kIx,
styční	styčný	k2eAgMnPc1d1
důstojníci	důstojník	k1gMnPc1
nebo	nebo	k8xC
diplomaté	diplomat	k1gMnPc1
partnerských	partnerský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
,	,	kIx,
mezinárodní	mezinárodní	k2eAgMnPc1d1
civilní	civilní	k2eAgMnPc1d1
zaměstnanci	zaměstnanec	k1gMnPc1
a	a	k8xC
mezinárodní	mezinárodní	k2eAgMnPc1d1
vojenští	vojenský	k2eAgMnPc1d1
zaměstnanci	zaměstnanec	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkový	celkový	k2eAgInSc1d1
počet	počet	k1gInSc1
stálých	stálý	k2eAgMnPc2d1
zaměstnanců	zaměstnanec	k1gMnPc2
je	být	k5eAaImIp3nS
zhruba	zhruba	k6eAd1
4000	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
120	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hlavní	hlavní	k2eAgFnPc1d1
politické	politický	k2eAgFnPc1d1
instituce	instituce	k1gFnPc1
</s>
<s>
Hlavní	hlavní	k2eAgFnSc4d1
politickou	politický	k2eAgFnSc4d1
strukturu	struktura	k1gFnSc4
NATO	NATO	kA
tvoří	tvořit	k5eAaImIp3nS
Severoatlantická	severoatlantický	k2eAgFnSc1d1
rada	rada	k1gFnSc1
<g/>
,	,	kIx,
Výbor	výbor	k1gInSc1
pro	pro	k7c4
obranné	obranný	k2eAgNnSc4d1
plánování	plánování	k1gNnSc4
a	a	k8xC
Skupina	skupina	k1gFnSc1
pro	pro	k7c4
jaderné	jaderný	k2eAgNnSc4d1
plánování	plánování	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
121	#num#	k4
<g/>
]	]	kIx)
Těmto	tento	k3xDgMnPc3
orgánům	orgán	k1gMnPc3
jsou	být	k5eAaImIp3nP
podřízeny	podřízen	k2eAgInPc1d1
Hlavní	hlavní	k2eAgInPc1d1
výbory	výbor	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
řeší	řešit	k5eAaImIp3nP
specifické	specifický	k2eAgInPc4d1
úkoly	úkol	k1gInPc4
v	v	k7c6
politické	politický	k2eAgFnSc6d1
<g/>
,	,	kIx,
vojenské	vojenský	k2eAgFnSc6d1
a	a	k8xC
ekonomické	ekonomický	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
122	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Severoatlantická	severoatlantický	k2eAgFnSc1d1
rada	rada	k1gFnSc1
</s>
<s>
Severoatlantická	severoatlantický	k2eAgFnSc1d1
rada	rada	k1gFnSc1
je	být	k5eAaImIp3nS
nejvyšší	vysoký	k2eAgInSc4d3
rozhodovací	rozhodovací	k2eAgInSc4d1
a	a	k8xC
konzultační	konzultační	k2eAgInSc4d1
orgán	orgán	k1gInSc4
Aliance	aliance	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Delegace	delegace	k1gFnSc1
každé	každý	k3xTgFnSc3
z	z	k7c2
30	#num#	k4
členských	členský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
je	být	k5eAaImIp3nS
v	v	k7c6
ní	on	k3xPp3gFnSc6
zastoupena	zastoupit	k5eAaPmNgFnS
jedním	jeden	k4xCgMnSc7
delegátem	delegát	k1gMnSc7
(	(	kIx(
<g/>
stálým	stálý	k2eAgMnSc7d1
zástupcem	zástupce	k1gMnSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
123	#num#	k4
<g/>
]	]	kIx)
Rada	rada	k1gFnSc1
se	se	k3xPyFc4
schází	scházet	k5eAaImIp3nS
alespoň	alespoň	k9
jednou	jeden	k4xCgFnSc7
týdně	týdně	k6eAd1
<g/>
,	,	kIx,
zasedání	zasedání	k1gNnSc1
na	na	k7c6
úrovni	úroveň	k1gFnSc6
ministrů	ministr	k1gMnPc2
zahraničí	zahraničí	k1gNnSc2
<g/>
,	,	kIx,
předsedů	předseda	k1gMnPc2
vlád	vláda	k1gFnPc2
nebo	nebo	k8xC
ministrů	ministr	k1gMnPc2
obrany	obrana	k1gFnSc2
probíhají	probíhat	k5eAaImIp3nP
jednou	jednou	k6eAd1
za	za	k7c4
půl	půl	k1xP
roku	rok	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
121	#num#	k4
<g/>
]	]	kIx)
Radě	rada	k1gFnSc6
předsedá	předsedat	k5eAaImIp3nS
generální	generální	k2eAgMnSc1d1
tajemník	tajemník	k1gMnSc1
NATO	NATO	kA
a	a	k8xC
rozhodnutí	rozhodnutí	k1gNnSc1
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
přijata	přijmout	k5eAaPmNgFnS
jednomyslně	jednomyslně	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Seznam	seznam	k1gInSc1
generálních	generální	k2eAgInPc2d1
tajemníků	tajemník	k1gInPc2
<g/>
[	[	kIx(
<g/>
124	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
#	#	kIx~
</s>
<s>
Jméno	jméno	k1gNnSc1
</s>
<s>
Stát	stát	k1gInSc1
</s>
<s>
Období	období	k1gNnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
Generál	generál	k1gMnSc1
Hastings	Hastingsa	k1gFnPc2
Lionel	Lionel	k1gMnSc1
Ismay	Ismaa	k1gFnSc2
</s>
<s>
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1952	#num#	k4
–	–	k?
16	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1957	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
Paul-Henri	Paul-Henri	k6eAd1
Spaak	Spaak	k1gInSc1
</s>
<s>
Belgie	Belgie	k1gFnSc1
Belgie	Belgie	k1gFnSc1
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1957	#num#	k4
–	–	k?
21	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1961	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
Dirk	Dirk	k1gMnSc1
Stikker	Stikker	k1gMnSc1
</s>
<s>
Nizozemsko	Nizozemsko	k1gNnSc1
Nizozemsko	Nizozemsko	k1gNnSc1
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1961	#num#	k4
–	–	k?
1	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1964	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
Manlio	Manlio	k1gMnSc1
Brosio	Brosio	k1gMnSc1
</s>
<s>
Itálie	Itálie	k1gFnSc1
Itálie	Itálie	k1gFnSc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1964	#num#	k4
–	–	k?
1	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1971	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
Joseph	Joseph	k1gInSc1
Luns	Lunsa	k1gFnPc2
</s>
<s>
Nizozemsko	Nizozemsko	k1gNnSc1
Nizozemsko	Nizozemsko	k1gNnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1971	#num#	k4
–	–	k?
25	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1984	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
Peter	Peter	k1gMnSc1
Carrington	Carrington	k1gInSc4
</s>
<s>
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1984	#num#	k4
–	–	k?
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1988	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
Manfred	Manfred	k1gMnSc1
Wörner	Wörner	k1gMnSc1
</s>
<s>
Německo	Německo	k1gNnSc1
Německo	Německo	k1gNnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1988	#num#	k4
–	–	k?
13	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1994	#num#	k4
</s>
<s>
–	–	k?
</s>
<s>
Sergio	Sergio	k6eAd1
Balanzino	Balanzin	k2eAgNnSc1d1
(	(	kIx(
<g/>
zastupující	zastupující	k2eAgMnSc1d1
<g/>
)	)	kIx)
</s>
<s>
Itálie	Itálie	k1gFnSc1
Itálie	Itálie	k1gFnSc2
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1994	#num#	k4
–	–	k?
17	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1994	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
Willy	Willa	k1gFnPc1
Claes	Claesa	k1gFnPc2
</s>
<s>
Belgie	Belgie	k1gFnSc1
Belgie	Belgie	k1gFnSc1
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1994	#num#	k4
–	–	k?
20	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1995	#num#	k4
</s>
<s>
–	–	k?
</s>
<s>
Sergio	Sergio	k6eAd1
Balanzino	Balanzin	k2eAgNnSc1d1
(	(	kIx(
<g/>
zastupující	zastupující	k2eAgMnSc1d1
<g/>
)	)	kIx)
</s>
<s>
Itálie	Itálie	k1gFnSc1
Itálie	Itálie	k1gFnSc2
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1995	#num#	k4
–	–	k?
5	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1995	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
Javier	Javier	k1gInSc1
Solana	Solan	k1gMnSc2
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1995	#num#	k4
–	–	k?
6	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1999	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
George	Georg	k1gMnSc2
Robertson	Robertson	k1gMnSc1
</s>
<s>
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1999	#num#	k4
–	–	k?
17	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2003	#num#	k4
</s>
<s>
–	–	k?
</s>
<s>
Alessandro	Alessandra	k1gFnSc5
Minuto	minuta	k1gFnSc5
Rizzo	Rizza	k1gFnSc5
(	(	kIx(
<g/>
zastupující	zastupující	k2eAgMnPc1d1
<g/>
)	)	kIx)
</s>
<s>
Itálie	Itálie	k1gFnSc1
Itálie	Itálie	k1gFnSc2
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2003	#num#	k4
–	–	k?
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2004	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
Jaap	Jaap	k1gMnSc1
de	de	k?
Hoop	Hoop	k1gMnSc1
Scheffer	Scheffer	k1gMnSc1
</s>
<s>
Nizozemsko	Nizozemsko	k1gNnSc1
Nizozemsko	Nizozemsko	k1gNnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2004	#num#	k4
–	–	k?
1	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2009	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
Anders	Anders	k6eAd1
Fogh	Fogh	k1gInSc1
Rasmussen	Rasmussen	k2eAgInSc1d1
</s>
<s>
Dánsko	Dánsko	k1gNnSc1
Dánsko	Dánsko	k1gNnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2009	#num#	k4
–	–	k?
1	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2014	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
Jens	Jens	k6eAd1
Stoltenberg	Stoltenberg	k1gMnSc1
</s>
<s>
Norsko	Norsko	k1gNnSc1
Norsko	Norsko	k1gNnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2014	#num#	k4
</s>
<s>
Seznam	seznam	k1gInSc1
náměstků	náměstek	k1gMnPc2
generálního	generální	k2eAgMnSc2d1
tajemníka	tajemník	k1gMnSc2
<g/>
[	[	kIx(
<g/>
125	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
#	#	kIx~
</s>
<s>
Jméno	jméno	k1gNnSc1
</s>
<s>
Stát	stát	k1gInSc1
</s>
<s>
Období	období	k1gNnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
Jonkheer	Jonkheer	k1gMnSc1
van	vana	k1gFnPc2
Vredenburch	Vredenburch	k1gMnSc1
</s>
<s>
Nizozemsko	Nizozemsko	k1gNnSc1
Nizozemsko	Nizozemsko	k1gNnSc1
</s>
<s>
1952	#num#	k4
<g/>
–	–	k?
<g/>
1956	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
Adolph	Adolph	k1gMnSc1
Bentinck	Bentinck	k1gMnSc1
</s>
<s>
Nizozemsko	Nizozemsko	k1gNnSc1
Nizozemsko	Nizozemsko	k1gNnSc1
</s>
<s>
1956	#num#	k4
<g/>
–	–	k?
<g/>
1958	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
Alberico	Alberico	k6eAd1
Casardi	Casard	k1gMnPc1
</s>
<s>
Itálie	Itálie	k1gFnSc1
Itálie	Itálie	k1gFnSc2
</s>
<s>
1958	#num#	k4
<g/>
–	–	k?
<g/>
1962	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
Guido	Guido	k1gNnSc1
Colonna	Colonn	k1gInSc2
di	di	k?
Paliano	Paliana	k1gFnSc5
</s>
<s>
Itálie	Itálie	k1gFnSc1
Itálie	Itálie	k1gFnSc2
</s>
<s>
1962	#num#	k4
<g/>
–	–	k?
<g/>
1964	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
James	James	k1gMnSc1
A.	A.	kA
Roberts	Roberts	k1gInSc1
</s>
<s>
Kanada	Kanada	k1gFnSc1
Kanada	Kanada	k1gFnSc1
</s>
<s>
1964	#num#	k4
<g/>
–	–	k?
<g/>
1968	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
Osman	Osman	k1gMnSc1
Olcay	Olcaa	k1gFnSc2
</s>
<s>
Turecko	Turecko	k1gNnSc1
Turecko	Turecko	k1gNnSc1
</s>
<s>
1969	#num#	k4
<g/>
–	–	k?
<g/>
1971	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
Paolo	Paolo	k1gNnSc1
Pansa	Pansa	k1gFnSc1
Cedronio	Cedronio	k1gNnSc1
</s>
<s>
Itálie	Itálie	k1gFnSc1
Itálie	Itálie	k1gFnSc2
</s>
<s>
1971	#num#	k4
<g/>
–	–	k?
<g/>
1978	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
Rinaldo	Rinalda	k1gFnSc5
Petrignani	Petrignaň	k1gFnSc5
</s>
<s>
Itálie	Itálie	k1gFnSc1
Itálie	Itálie	k1gFnSc2
</s>
<s>
1978	#num#	k4
<g/>
–	–	k?
<g/>
1981	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
Eric	Eric	k1gFnSc1
da	da	k?
Rin	Rin	k1gFnSc2
</s>
<s>
Itálie	Itálie	k1gFnSc1
Itálie	Itálie	k1gFnSc2
</s>
<s>
1981	#num#	k4
<g/>
–	–	k?
<g/>
1985	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
Marcello	Marcello	k1gNnSc1
Guidi	Guid	k1gMnPc1
</s>
<s>
Itálie	Itálie	k1gFnSc1
Itálie	Itálie	k1gFnSc2
</s>
<s>
1985	#num#	k4
<g/>
–	–	k?
<g/>
1989	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
Amedeo	Amedeo	k6eAd1
de	de	k?
Franchis	Franchis	k1gInSc1
</s>
<s>
Itálie	Itálie	k1gFnSc1
Itálie	Itálie	k1gFnSc2
</s>
<s>
1989	#num#	k4
<g/>
–	–	k?
<g/>
1994	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
Sergio	Sergio	k6eAd1
Balanzino	Balanzin	k2eAgNnSc1d1
</s>
<s>
Itálie	Itálie	k1gFnSc1
Itálie	Itálie	k1gFnSc2
</s>
<s>
1994	#num#	k4
<g/>
–	–	k?
<g/>
2001	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
Alessandro	Alessandra	k1gFnSc5
Minuto	minuta	k1gFnSc5
Rizzo	Rizza	k1gFnSc5
</s>
<s>
Itálie	Itálie	k1gFnSc1
Itálie	Itálie	k1gFnSc2
</s>
<s>
2001	#num#	k4
<g/>
–	–	k?
<g/>
2007	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
Claudio	Claudio	k6eAd1
Bisogniero	Bisogniero	k1gNnSc1
</s>
<s>
Itálie	Itálie	k1gFnSc1
Itálie	Itálie	k1gFnSc2
</s>
<s>
2007	#num#	k4
<g/>
–	–	k?
<g/>
2012	#num#	k4
</s>
<s>
15	#num#	k4
</s>
<s>
Alexander	Alexandra	k1gFnPc2
Vershbow	Vershbow	k1gFnPc2
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
</s>
<s>
2012	#num#	k4
<g/>
–	–	k?
<g/>
2016	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
Rose	Rose	k1gMnSc1
Gottemoeller	Gottemoeller	k1gMnSc1
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
</s>
<s>
2016-2019	2016-2019	k4
</s>
<s>
17	#num#	k4
</s>
<s>
Mircea	Mirce	k2eAgFnSc1d1
Geoana	Geoana	k1gFnSc1
</s>
<s>
Rumunsko	Rumunsko	k1gNnSc1
Rumunsko	Rumunsko	k1gNnSc1
</s>
<s>
2019	#num#	k4
<g/>
-současnost	-současnost	k1gFnSc1
</s>
<s>
Vojenská	vojenský	k2eAgFnSc1d1
struktura	struktura	k1gFnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Předseda	předseda	k1gMnSc1
vojenského	vojenský	k2eAgInSc2d1
výboru	výbor	k1gInSc2
NATO	NATO	kA
<g/>
.	.	kIx.
</s>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3
vojenským	vojenský	k2eAgInSc7d1
orgánem	orgán	k1gInSc7
NATO	NATO	kA
je	být	k5eAaImIp3nS
Vojenský	vojenský	k2eAgInSc1d1
výbor	výbor	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
předkládá	předkládat	k5eAaImIp3nS
návrhy	návrh	k1gInPc4
a	a	k8xC
doporučení	doporučení	k1gNnPc4
Severoatlantické	severoatlantický	k2eAgFnSc2d1
radě	rada	k1gFnSc3
<g/>
,	,	kIx,
Výboru	výbor	k1gInSc3
pro	pro	k7c4
obranné	obranný	k2eAgNnSc4d1
plánování	plánování	k1gNnSc4
a	a	k8xC
Skupině	skupina	k1gFnSc3
pro	pro	k7c4
jaderné	jaderný	k2eAgNnSc4d1
plánování	plánování	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stejně	stejně	k6eAd1
jako	jako	k8xC,k8xS
v	v	k7c6
Severoatlantické	severoatlantický	k2eAgFnSc6d1
radě	rada	k1gFnSc6
je	být	k5eAaImIp3nS
každá	každý	k3xTgFnSc1
z	z	k7c2
členských	členský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
zastoupena	zastoupit	k5eAaPmNgFnS
jedním	jeden	k4xCgInSc7
(	(	kIx(
<g/>
vojenským	vojenský	k2eAgMnSc7d1
<g/>
)	)	kIx)
zástupcem	zástupce	k1gMnSc7
a	a	k8xC
výbor	výbor	k1gInSc4
se	se	k3xPyFc4
schází	scházet	k5eAaImIp3nS
alespoň	alespoň	k9
jednou	jeden	k4xCgFnSc7
týdně	týdně	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
121	#num#	k4
<g/>
]	]	kIx)
Předseda	předseda	k1gMnSc1
Vojenského	vojenský	k2eAgInSc2d1
výboru	výbor	k1gInSc2
je	být	k5eAaImIp3nS
volen	volit	k5eAaImNgInS
náčelníky	náčelník	k1gInPc7
generálních	generální	k2eAgInPc2d1
štábů	štáb	k1gInPc2
členských	členský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
na	na	k7c4
tříleté	tříletý	k2eAgNnSc4d1
období	období	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
2015	#num#	k4
do	do	k7c2
roku	rok	k1gInSc2
2018	#num#	k4
jím	jíst	k5eAaImIp1nS
byl	být	k5eAaImAgMnS
český	český	k2eAgMnSc1d1
armádní	armádní	k2eAgMnSc1d1
generál	generál	k1gMnSc1
Petr	Petr	k1gMnSc1
Pavel	Pavel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
2018	#num#	k4
zastává	zastávat	k5eAaImIp3nS
tuto	tento	k3xDgFnSc4
pozici	pozice	k1gFnSc4
sir	sir	k1gMnSc1
Stuart	Stuarta	k1gFnPc2
Peach	Peach	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Výboru	výbor	k1gInSc3
podléhají	podléhat	k5eAaImIp3nP
dvě	dva	k4xCgNnPc1
vojenská	vojenský	k2eAgNnPc1d1
velitelství	velitelství	k1gNnPc1
<g/>
:	:	kIx,
Velitelství	velitelství	k1gNnSc4
spojeneckých	spojenecký	k2eAgFnPc2d1
sil	síla	k1gFnPc2
pro	pro	k7c4
operace	operace	k1gFnPc4
(	(	kIx(
<g/>
ACO	ACO	kA
<g/>
)	)	kIx)
a	a	k8xC
Velitelství	velitelství	k1gNnSc1
spojeneckých	spojenecký	k2eAgFnPc2d1
sil	síla	k1gFnPc2
pro	pro	k7c4
transformaci	transformace	k1gFnSc4
(	(	kIx(
<g/>
ACT	ACT	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
121	#num#	k4
<g/>
]	]	kIx)
ACO	ACO	kA
sídlí	sídlet	k5eAaImIp3nS
ve	v	k7c6
Vrchním	vrchní	k2eAgNnSc6d1
velitelství	velitelství	k1gNnSc6
spojeneckých	spojenecký	k2eAgFnPc2d1
sil	síla	k1gFnPc2
v	v	k7c6
Evropě	Evropa	k1gFnSc6
(	(	kIx(
<g/>
SHAPE	SHAPE	kA
<g/>
)	)	kIx)
a	a	k8xC
velí	velet	k5eAaImIp3nS
mu	on	k3xPp3gMnSc3
vrchní	vrchní	k2eAgMnSc1d1
velitel	velitel	k1gMnSc1
sil	síla	k1gFnPc2
NATO	NATO	kA
v	v	k7c6
Evropě	Evropa	k1gFnSc6
(	(	kIx(
<g/>
SACEUR	SACEUR	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
byl	být	k5eAaImAgInS
od	od	k7c2
roku	rok	k1gInSc2
2009	#num#	k4
admirál	admirál	k1gMnSc1
James	James	k1gMnSc1
Stavridis	Stavridis	k1gFnSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
126	#num#	k4
<g/>
]	]	kIx)
v	v	k7c6
letech	léto	k1gNnPc6
2013	#num#	k4
až	až	k9
2016	#num#	k4
Philip	Philip	k1gInSc1
Breedlove	Breedlov	k1gInSc5
a	a	k8xC
následující	následující	k2eAgInPc4d1
tři	tři	k4xCgInPc4
roky	rok	k1gInPc4
Curtis	Curtis	k1gFnSc3
Scaparrotti	Scaparrotť	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
127	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
květnu	květen	k1gInSc6
2019	#num#	k4
se	se	k3xPyFc4
jím	on	k3xPp3gMnSc7
stal	stát	k5eAaPmAgMnS
admirál	admirál	k1gMnSc1
Tod	Tod	k1gFnSc2
Wolters	Wolters	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
128	#num#	k4
<g/>
]	]	kIx)
Velitelem	velitel	k1gMnSc7
ACT	ACT	kA
(	(	kIx(
<g/>
SACT	SACT	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
generál	generál	k1gMnSc1
Stéphane	Stéphan	k1gMnSc5
Abrial	Abrial	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
129	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Orgány	orgán	k1gInPc1
stojící	stojící	k2eAgInPc1d1
formálně	formálně	k6eAd1
mimo	mimo	k7c4
Alianci	aliance	k1gFnSc4
</s>
<s>
Parlamentní	parlamentní	k2eAgNnSc1d1
shromáždění	shromáždění	k1gNnSc1
NATO	NATO	kA
</s>
<s>
Parlamentní	parlamentní	k2eAgNnSc1d1
shromáždění	shromáždění	k1gNnSc1
sestává	sestávat	k5eAaImIp3nS
z	z	k7c2
poslanců	poslanec	k1gMnPc2
parlamentů	parlament	k1gInPc2
všech	všecek	k3xTgInPc2
členských	členský	k2eAgInPc2d1
států	stát	k1gInPc2
a	a	k8xC
14	#num#	k4
přidružených	přidružený	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
130	#num#	k4
<g/>
]	]	kIx)
Nemá	mít	k5eNaImIp3nS
výkonnou	výkonný	k2eAgFnSc4d1
moc	moc	k1gFnSc4
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
pouze	pouze	k6eAd1
konzultativní	konzultativní	k2eAgInSc4d1
charakter	charakter	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Delegáti	delegát	k1gMnPc1
pracují	pracovat	k5eAaImIp3nP
v	v	k7c6
pěti	pět	k4xCc6
výborech	výbor	k1gInPc6
<g/>
:	:	kIx,
ekonomickém	ekonomický	k2eAgNnSc6d1
<g/>
,	,	kIx,
politickém	politický	k2eAgNnSc6d1
<g/>
,	,	kIx,
bezpečnostním	bezpečnostní	k2eAgNnSc6d1
a	a	k8xC
vědecko-technickém	vědecko-technický	k2eAgMnSc6d1
a	a	k8xC
ve	v	k7c6
zvláštní	zvláštní	k2eAgFnSc6d1
skupině	skupina	k1gFnSc6
pro	pro	k7c4
středomořský	středomořský	k2eAgInSc4d1
dialog	dialog	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
131	#num#	k4
<g/>
]	]	kIx)
Počet	počet	k1gInSc1
poslanců	poslanec	k1gMnPc2
zemí	zem	k1gFnPc2
je	být	k5eAaImIp3nS
odvozen	odvodit	k5eAaPmNgInS
od	od	k7c2
počtu	počet	k1gInSc2
obyvatel	obyvatel	k1gMnPc2
<g/>
;	;	kIx,
v	v	k7c6
shromáždění	shromáždění	k1gNnSc6
jich	on	k3xPp3gInPc2
zasedá	zasedat	k5eAaImIp3nS
celkem	celkem	k6eAd1
přes	přes	k7c4
300	#num#	k4
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
z	z	k7c2
Česka	Česko	k1gNnSc2
7	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
132	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
133	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rada	rada	k1gFnSc1
NATO-Rusko	NATO-Rusko	k1gNnSc1
</s>
<s>
Ruský	ruský	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
Vladimir	Vladimir	k1gMnSc1
Putin	putin	k2eAgInSc4d1
na	na	k7c4
setkání	setkání	k1gNnSc4
rady	rada	k1gFnSc2
NATO-Rusko	NATO-Rusko	k1gNnSc1
v	v	k7c6
roce	rok	k1gInSc6
2002	#num#	k4
</s>
<s>
Rada	Rada	k1gMnSc1
NATO-Rusko	NATO-Rusko	k1gNnSc1
byla	být	k5eAaImAgFnS
založena	založit	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
2002	#num#	k4
<g/>
[	[	kIx(
<g/>
134	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
nahradila	nahradit	k5eAaPmAgFnS
Stálou	stálý	k2eAgFnSc4d1
společnou	společný	k2eAgFnSc4d1
radu	rada	k1gFnSc4
Rusko-NATO	rusko-nato	k6eAd1
(	(	kIx(
<g/>
Permanent	permanent	k1gInSc1
Join	Join	k1gNnSc1
Council	Council	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rusko	Rusko	k1gNnSc1
na	na	k7c6
jedné	jeden	k4xCgFnSc6
straně	strana	k1gFnSc6
a	a	k8xC
NATO	NATO	kA
na	na	k7c4
druhé	druhý	k4xOgNnSc4
v	v	k7c6
ní	on	k3xPp3gFnSc2
jednají	jednat	k5eAaImIp3nP
jako	jako	k9
rovnocenní	rovnocenný	k2eAgMnPc1d1
partneři	partner	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slouží	sloužit	k5eAaImIp3nS
ke	k	k7c3
konzultaci	konzultace	k1gFnSc3
o	o	k7c6
bezpečnostních	bezpečnostní	k2eAgFnPc6d1
a	a	k8xC
vojenských	vojenský	k2eAgFnPc6d1
otázkách	otázka	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
ruský	ruský	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
Dmitrij	Dmitrij	k1gMnSc1
Medveděv	Medveděv	k1gMnSc1
kritizoval	kritizovat	k5eAaImAgMnS
rozšiřování	rozšiřování	k1gNnSc4
NATO	NATO	kA
na	na	k7c4
východ	východ	k1gInSc4
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
podle	podle	k7c2
něho	on	k3xPp3gMnSc2
porušilo	porušit	k5eAaPmAgNnS
sliby	slib	k1gInPc4
dané	daný	k2eAgFnSc2d1
západními	západní	k2eAgMnPc7d1
politiky	politik	k1gMnPc7
po	po	k7c6
sjednocení	sjednocení	k1gNnSc6
Německa	Německo	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
135	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
2014	#num#	k4
NATO	nato	k6eAd1
přerušilo	přerušit	k5eAaPmAgNnS
s	s	k7c7
Ruskem	Rusko	k1gNnSc7
veškerou	veškerý	k3xTgFnSc4
spolupráci	spolupráce	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
březnu	březen	k1gInSc6
2015	#num#	k4
<g/>
,	,	kIx,
po	po	k7c6
uzavření	uzavření	k1gNnSc6
příměří	příměří	k1gNnSc2
v	v	k7c6
Minsku	Minsk	k1gInSc6
mezi	mezi	k7c7
proruskými	proruský	k2eAgMnPc7d1
separatisty	separatista	k1gMnPc7
v	v	k7c6
Donbasu	Donbas	k1gInSc6
a	a	k8xC
ukrajinskou	ukrajinský	k2eAgFnSc7d1
vládou	vláda	k1gFnSc7
<g/>
,	,	kIx,
němečtí	německý	k2eAgMnPc1d1
představitelé	představitel	k1gMnPc1
z	z	k7c2
okruhu	okruh	k1gInSc2
kancléřky	kancléřka	k1gFnSc2
Merkelové	Merkelová	k1gFnSc2
kritizovali	kritizovat	k5eAaImAgMnP
vrchního	vrchní	k2eAgMnSc4d1
velitele	velitel	k1gMnSc4
sil	síla	k1gFnPc2
NATO	NATO	kA
v	v	k7c6
Evropě	Evropa	k1gFnSc6
amerického	americký	k2eAgMnSc2d1
generála	generál	k1gMnSc2
Philipa	Philip	k1gMnSc2
Breedlova	Breedlův	k2eAgMnSc2d1
za	za	k7c4
„	„	k?
<g/>
nepravdivá	pravdivý	k2eNgNnPc1d1
tvrzení	tvrzení	k1gNnPc1
a	a	k8xC
přehnané	přehnaný	k2eAgInPc1d1
soudy	soud	k1gInPc1
<g/>
“	“	k?
o	o	k7c6
přítomnosti	přítomnost	k1gFnSc6
ruských	ruský	k2eAgNnPc2d1
vojsk	vojsko	k1gNnPc2
na	na	k7c6
Ukrajině	Ukrajina	k1gFnSc6
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
zbytečně	zbytečně	k6eAd1
vyhrocují	vyhrocovat	k5eAaImIp3nP
ukrajinskou	ukrajinský	k2eAgFnSc4d1
krizi	krize	k1gFnSc4
a	a	k8xC
ohrožují	ohrožovat	k5eAaImIp3nP
důvěryhodnost	důvěryhodnost	k1gFnSc4
NATO	NATO	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
německého	německý	k2eAgInSc2d1
magazínu	magazín	k1gInSc2
Der	drát	k5eAaImRp2nS
Spiegel	Spiegel	k1gMnSc1
generál	generál	k1gMnSc1
Breedlove	Breedlov	k1gInSc5
v	v	k7c6
posledních	poslední	k2eAgInPc6d1
měsících	měsíc	k1gInPc6
opakovaně	opakovaně	k6eAd1
veřejně	veřejně	k6eAd1
varoval	varovat	k5eAaImAgMnS
před	před	k7c7
hrozící	hrozící	k2eAgFnSc7d1
ruskou	ruský	k2eAgFnSc7d1
invazí	invaze	k1gFnSc7
nebo	nebo	k8xC
před	před	k7c7
přítomností	přítomnost	k1gFnSc7
vysokého	vysoký	k2eAgInSc2d1
počtu	počet	k1gInSc2
ruských	ruský	k2eAgMnPc2d1
vojáků	voják	k1gMnPc2
na	na	k7c6
Ukrajině	Ukrajina	k1gFnSc6
<g/>
,	,	kIx,
a	a	k8xC
pokaždé	pokaždé	k6eAd1
se	se	k3xPyFc4
podle	podle	k7c2
německé	německý	k2eAgFnSc2d1
rozvědky	rozvědka	k1gFnSc2
jeho	jeho	k3xOp3gFnSc2
informace	informace	k1gFnSc2
ukázaly	ukázat	k5eAaPmAgFnP
jako	jako	k9
přehnané	přehnaný	k2eAgFnPc1d1
nebo	nebo	k8xC
nepravdivé	pravdivý	k2eNgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Znepokojení	znepokojení	k1gNnSc1
nad	nad	k7c7
výroky	výrok	k1gInPc7
generála	generál	k1gMnSc2
Breedlova	Breedlův	k2eAgFnSc1d1
vyjádřili	vyjádřit	k5eAaPmAgMnP
i	i	k9
někteří	některý	k3yIgMnPc1
velvyslanci	velvyslanec	k1gMnPc1
členských	členský	k2eAgInPc2d1
států	stát	k1gInPc2
při	při	k7c6
NATO	NATO	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
136	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
v	v	k7c6
únoru	únor	k1gInSc6
2019	#num#	k4
odstoupily	odstoupit	k5eAaPmAgFnP
od	od	k7c2
Smlouvy	smlouva	k1gFnSc2
o	o	k7c6
likvidaci	likvidace	k1gFnSc6
raket	raketa	k1gFnPc2
středního	střední	k2eAgInSc2d1
a	a	k8xC
krátkého	krátký	k2eAgInSc2d1
doletu	dolet	k1gInSc2
(	(	kIx(
<g/>
INF	INF	kA
<g/>
)	)	kIx)
z	z	k7c2
roku	rok	k1gInSc2
1987	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
137	#num#	k4
<g/>
]	]	kIx)
Tento	tento	k3xDgInSc4
krok	krok	k1gInSc4
podpořila	podpořit	k5eAaPmAgFnS
i	i	k9
Severoatlantická	severoatlantický	k2eAgFnSc1d1
aliance	aliance	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
138	#num#	k4
<g/>
]	]	kIx)
Panují	panovat	k5eAaImIp3nP
obavy	obava	k1gFnPc1
<g/>
,	,	kIx,
že	že	k8xS
vypuknou	vypuknout	k5eAaPmIp3nP
<g />
.	.	kIx.
</s>
<s hack="1">
nové	nový	k2eAgInPc1d1
závody	závod	k1gInPc1
ve	v	k7c6
zbrojení	zbrojení	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
139	#num#	k4
<g/>
]	]	kIx)
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
a	a	k8xC
NATO	nato	k6eAd1
obvinily	obvinit	k5eAaPmAgFnP
z	z	k7c2
porušení	porušení	k1gNnPc2
smlouvy	smlouva	k1gFnSc2
Rusko	Rusko	k1gNnSc1
<g/>
,	,	kIx,
naopak	naopak	k6eAd1
Rusko	Rusko	k1gNnSc1
obvinilo	obvinit	k5eAaPmAgNnS
z	z	k7c2
porušení	porušení	k1gNnSc2
smlouvy	smlouva	k1gFnSc2
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
140	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
názoru	názor	k1gInSc2
některých	některý	k3yIgMnPc2
nezávislých	závislý	k2eNgMnPc2d1
expertů	expert	k1gMnPc2
a	a	k8xC
komentátorů	komentátor	k1gMnPc2
smlouvu	smlouva	k1gFnSc4
patrně	patrně	k6eAd1
porušovaly	porušovat	k5eAaImAgFnP
obě	dva	k4xCgFnPc1
<g />
.	.	kIx.
</s>
<s hack="1">
strany	strana	k1gFnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
141	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
142	#num#	k4
<g/>
]	]	kIx)
ale	ale	k8xC
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
od	od	k7c2
smlouvy	smlouva	k1gFnSc2
odstoupily	odstoupit	k5eAaPmAgInP
<g/>
,	,	kIx,
protože	protože	k8xS
signatářem	signatář	k1gMnSc7
smlouvy	smlouva	k1gFnSc2
není	být	k5eNaImIp3nS
Čína	Čína	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
může	moct	k5eAaImIp3nS
bez	bez	k7c2
omezení	omezení	k1gNnPc2
budovat	budovat	k5eAaImF
svůj	svůj	k3xOyFgInSc4
raketový	raketový	k2eAgInSc4d1
arzenál	arzenál	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
143	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
144	#num#	k4
<g/>
]	]	kIx)
Tento	tento	k3xDgInSc1
názor	názor	k1gInSc1
potvrdil	potvrdit	k5eAaPmAgInS
již	již	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
bezpečnostní	bezpečnostní	k2eAgInPc4d1
poradce	poradce	k1gMnSc1
amerického	americký	k2eAgMnSc2d1
prezidenta	prezident	k1gMnSc2
John	John	k1gMnSc1
Bolton	Bolton	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
145	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kritika	kritika	k1gFnSc1
a	a	k8xC
kontroverze	kontroverze	k1gFnSc1
</s>
<s>
V	v	k7c6
důsledku	důsledek	k1gInSc6
vojenské	vojenský	k2eAgFnSc2d1
intervence	intervence	k1gFnSc2
v	v	k7c6
Libyi	Libye	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
vedla	vést	k5eAaImAgFnS
ke	k	k7c3
svržení	svržení	k1gNnSc3
režimu	režim	k1gInSc2
Muammara	Muammar	k1gMnSc2
Kaddáfího	Kaddáfí	k1gMnSc2
v	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
<g/>
,	,	kIx,
se	se	k3xPyFc4
Libye	Libye	k1gFnSc1
propadla	propadnout	k5eAaPmAgFnS
do	do	k7c2
chaosu	chaos	k1gInSc2
a	a	k8xC
občanské	občanský	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
146	#num#	k4
<g/>
]	]	kIx)
Někteří	některý	k3yIgMnPc1
politici	politik	k1gMnPc1
<g/>
,	,	kIx,
například	například	k6eAd1
slovenský	slovenský	k2eAgMnSc1d1
premiér	premiér	k1gMnSc1
Robert	Robert	k1gMnSc1
Fico	Fico	k1gMnSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
147	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
]	]	kIx)
jihoafrický	jihoafrický	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
Jacob	Jacoba	k1gFnPc2
Zuma	Zuma	k1gMnSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
148	#num#	k4
<g/>
]	]	kIx)
americký	americký	k2eAgMnSc1d1
senátor	senátor	k1gMnSc1
Rand	rand	k1gInSc1
Paul	Paul	k1gMnSc1
<g/>
[	[	kIx(
<g/>
149	#num#	k4
<g/>
]	]	kIx)
nebo	nebo	k8xC
britský	britský	k2eAgMnSc1d1
politik	politik	k1gMnSc1
Nigel	Nigel	k1gMnSc1
Farage	Farage	k1gFnSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
150	#num#	k4
<g/>
]	]	kIx)
se	se	k3xPyFc4
domnívají	domnívat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
k	k	k7c3
destabilizaci	destabilizace	k1gFnSc3
Blízkého	blízký	k2eAgInSc2d1
východu	východ	k1gInSc2
a	a	k8xC
k	k	k7c3
následné	následný	k2eAgFnSc3d1
migrační	migrační	k2eAgFnSc3d1
vlně	vlna	k1gFnSc3
přispěly	přispět	k5eAaPmAgFnP
vojenské	vojenský	k2eAgFnPc1d1
intervence	intervence	k1gFnPc1
NATO	NATO	kA
a	a	k8xC
západních	západní	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
v	v	k7c6
Iráku	Irák	k1gInSc6
a	a	k8xC
Libyi	Libye	k1gFnSc6
i	i	k9
podpora	podpora	k1gFnSc1
povstalců	povstalec	k1gMnPc2
v	v	k7c6
Sýrii	Sýrie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
setkání	setkání	k1gNnSc6
s	s	k7c7
generálním	generální	k2eAgMnSc7d1
tajemníkem	tajemník	k1gMnSc7
NATO	NATO	kA
Jensem	Jens	k1gMnSc7
Stoltenbergem	Stoltenberg	k1gMnSc7
v	v	k7c6
září	září	k1gNnSc6
2015	#num#	k4
kritizoval	kritizovat	k5eAaImAgMnS
místopředseda	místopředseda	k1gMnSc1
vlády	vláda	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
Andrej	Andrej	k1gMnSc1
Babiš	Babiš	k1gMnSc1
NATO	NATO	kA
za	za	k7c4
nezájem	nezájem	k1gInSc4
o	o	k7c4
ochranu	ochrana	k1gFnSc4
evropských	evropský	k2eAgFnPc2d1
hranic	hranice	k1gFnPc2
a	a	k8xC
prohlásil	prohlásit	k5eAaPmAgMnS
<g/>
:	:	kIx,
„	„	k?
<g/>
NATO	NATO	kA
se	se	k3xPyFc4
chová	chovat	k5eAaImIp3nS
účelově	účelově	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proti	proti	k7c3
somálským	somálský	k2eAgMnPc3d1
pirátům	pirát	k1gMnPc3
zasáhlo	zasáhnout	k5eAaPmAgNnS
<g/>
,	,	kIx,
protože	protože	k8xS
tam	tam	k6eAd1
byl	být	k5eAaImAgInS
ekonomický	ekonomický	k2eAgInSc1d1
zájem	zájem	k1gInSc1
členských	členský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
NATO	NATO	kA
a	a	k8xC
dalších	další	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
Bezpečnostní	bezpečnostní	k2eAgFnSc2d1
rady	rada	k1gFnSc2
OSN	OSN	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
...	...	k?
<g/>
)	)	kIx)
NATO	NATO	kA
uprchlíci	uprchlík	k1gMnPc1
nezajímají	zajímat	k5eNaImIp3nP
<g/>
,	,	kIx,
přitom	přitom	k6eAd1
jejich	jejich	k3xOp3gFnSc7
vstupní	vstupní	k2eAgFnSc7d1
branou	brána	k1gFnSc7
do	do	k7c2
Evropy	Evropa	k1gFnSc2
je	být	k5eAaImIp3nS
Turecko	Turecko	k1gNnSc1
<g/>
,	,	kIx,
člen	člen	k1gInSc1
NATO	NATO	kA
<g/>
,	,	kIx,
a	a	k8xC
pašeráci	pašerák	k1gMnPc1
na	na	k7c6
území	území	k1gNnSc6
Turecka	Turecko	k1gNnSc2
<g/>
,	,	kIx,
člena	člen	k1gMnSc2
NATO	NATO	kA
<g/>
,	,	kIx,
operují	operovat	k5eAaImIp3nP
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
151	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Premiér	premiér	k1gMnSc1
Milo	milo	k6eAd1
Đukanović	Đukanović	k1gMnPc3
podepsal	podepsat	k5eAaPmAgMnS
protokol	protokol	k1gInSc4
o	o	k7c4
přistoupení	přistoupení	k1gNnSc4
Černé	Černé	k2eAgFnSc2d1
Hory	hora	k1gFnSc2
k	k	k7c3
NATO	NATO	kA
19	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2016	#num#	k4
</s>
<s>
Německý	německý	k2eAgMnSc1d1
ministr	ministr	k1gMnSc1
zahraničí	zahraničí	k1gNnSc2
ve	v	k7c6
vládě	vláda	k1gFnSc6
Angely	Angela	k1gFnSc2
Merkelové	Merkelová	k1gFnSc2
Frank-Walter	Frank-Walter	k1gMnSc1
Steinmeier	Steinmeier	k1gMnSc1
v	v	k7c6
červnu	červen	k1gInSc6
2016	#num#	k4
kritizoval	kritizovat	k5eAaImAgMnS
NATO	NATO	kA
za	za	k7c4
eskalaci	eskalace	k1gFnSc4
napětí	napětí	k1gNnSc2
ve	v	k7c6
vztazích	vztah	k1gInPc6
s	s	k7c7
Ruskem	Rusko	k1gNnSc7
a	a	k8xC
označil	označit	k5eAaPmAgInS
rozsáhlá	rozsáhlý	k2eAgNnPc4d1
vojenská	vojenský	k2eAgNnPc4d1
cvičení	cvičení	k1gNnPc4
NATO	nato	k6eAd1
u	u	k7c2
hranic	hranice	k1gFnPc2
Ruska	Rusko	k1gNnSc2
za	za	k7c2
„	„	k?
<g/>
kontraproduktivní	kontraproduktivní	k2eAgNnSc1d1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Steinmeier	Steinmeier	k1gMnSc1
uvedl	uvést	k5eAaPmAgMnS
<g/>
:	:	kIx,
„	„	k?
<g/>
To	ten	k3xDgNnSc1
<g/>
,	,	kIx,
co	co	k3yQnSc4,k3yInSc4,k3yRnSc4
bychom	by	kYmCp1nP
nyní	nyní	k6eAd1
neměli	mít	k5eNaImAgMnP
dělat	dělat	k5eAaImF
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
dál	daleko	k6eAd2
vyhrocovat	vyhrocovat	k5eAaImF
situaci	situace	k1gFnSc3
chrastěním	chrastění	k1gNnSc7
zbraní	zbraň	k1gFnSc7
a	a	k8xC
válečným	válečný	k2eAgNnSc7d1
štvaním	štvaní	k1gNnSc7
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
152	#num#	k4
<g/>
]	]	kIx)
Průzkum	průzkum	k1gInSc1
veřejného	veřejný	k2eAgNnSc2d1
mínění	mínění	k1gNnSc2
v	v	k7c6
Rusku	Rusko	k1gNnSc6
v	v	k7c6
červnu	červen	k1gInSc6
2016	#num#	k4
zjistil	zjistit	k5eAaPmAgInS
<g/>
,	,	kIx,
že	že	k8xS
68	#num#	k4
%	%	kIx~
dotázaných	dotázaný	k2eAgMnPc2d1
Rusů	Rus	k1gMnPc2
považuje	považovat	k5eAaImIp3nS
rozmístění	rozmístění	k1gNnSc1
vojsk	vojsko	k1gNnPc2
NATO	NATO	kA
u	u	k7c2
ruských	ruský	k2eAgFnPc2d1
hranic	hranice	k1gFnPc2
v	v	k7c6
Polsku	Polsko	k1gNnSc6
a	a	k8xC
Pobaltí	Pobaltí	k1gNnSc4
za	za	k7c4
hrozbu	hrozba	k1gFnSc4
pro	pro	k7c4
Rusko	Rusko	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
153	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
NATO	nato	k6eAd1
bylo	být	k5eAaImAgNnS
kritizováno	kritizovat	k5eAaImNgNnS
<g/>
,	,	kIx,
protože	protože	k8xS
mlčí	mlčet	k5eAaImIp3nS
k	k	k7c3
porušování	porušování	k1gNnSc3
lidských	lidský	k2eAgNnPc2d1
práv	právo	k1gNnPc2
v	v	k7c6
Turecku	Turecko	k1gNnSc6
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
154	#num#	k4
<g/>
]	]	kIx)
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
má	mít	k5eAaImIp3nS
druhé	druhý	k4xOgNnSc1
největší	veliký	k2eAgNnSc1d3
vojsko	vojsko	k1gNnSc1
v	v	k7c6
rámci	rámec	k1gInSc6
NATO	NATO	kA
a	a	k8xC
je	být	k5eAaImIp3nS
jedním	jeden	k4xCgMnSc7
z	z	k7c2
pěti	pět	k4xCc2
států	stát	k1gInPc2
NATO	NATO	kA
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
jsou	být	k5eAaImIp3nP
součástí	součást	k1gFnSc7
politiky	politika	k1gFnSc2
sdílení	sdílení	k1gNnSc2
jaderných	jaderný	k2eAgFnPc2d1
zbraní	zbraň	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
155	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
]	]	kIx)
V	v	k7c6
důsledku	důsledek	k1gInSc6
rozsáhlých	rozsáhlý	k2eAgFnPc2d1
politických	politický	k2eAgFnPc2d1
čistek	čistka	k1gFnPc2
v	v	k7c6
Turecku	Turecko	k1gNnSc6
proti	proti	k7c3
skutečným	skutečný	k2eAgInPc3d1
nebo	nebo	k8xC
domnělým	domnělý	k2eAgMnPc3d1
oponentům	oponent	k1gMnPc3
režimu	režim	k1gInSc2
po	po	k7c6
neúspěšném	úspěšný	k2eNgInSc6d1
pokusu	pokus	k1gInSc6
o	o	k7c4
vojenský	vojenský	k2eAgInSc4d1
převrat	převrat	k1gInSc4
ze	z	k7c2
dne	den	k1gInSc2
15	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2016	#num#	k4
požádalo	požádat	k5eAaPmAgNnS
několik	několik	k4yIc4
desítek	desítka	k1gFnPc2
vysoce	vysoce	k6eAd1
postavených	postavený	k2eAgMnPc2d1
tureckých	turecký	k2eAgMnPc2d1
důstojníků	důstojník	k1gMnPc2
působících	působící	k2eAgMnPc2d1
ve	v	k7c6
strukturách	struktura	k1gFnPc6
NATO	nato	k6eAd1
o	o	k7c4
azyl	azyl	k1gInSc4
v	v	k7c6
členských	členský	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
NATO	NATO	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
156	#num#	k4
<g/>
]	]	kIx)
Generálním	generální	k2eAgInSc7d1
tajemník	tajemník	k1gMnSc1
NATO	NATO	kA
Stoltenberg	Stoltenberg	k1gMnSc1
na	na	k7c4
to	ten	k3xDgNnSc4
reagoval	reagovat	k5eAaBmAgMnS
slovy	slovo	k1gNnPc7
<g/>
,	,	kIx,
že	že	k8xS
„	„	k?
<g/>
Turecko	Turecko	k1gNnSc1
má	mít	k5eAaImIp3nS
právo	práv	k2eAgNnSc1d1
stíhat	stíhat	k5eAaImF
organizátory	organizátor	k1gMnPc4
<g/>
“	“	k?
vojenského	vojenský	k2eAgInSc2d1
převratu	převrat	k1gInSc2
a	a	k8xC
zdůraznil	zdůraznit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
Turecko	Turecko	k1gNnSc1
je	být	k5eAaImIp3nS
klíčovým	klíčový	k2eAgMnSc7d1
spojencem	spojenec	k1gMnSc7
v	v	k7c6
rámci	rámec	k1gInSc6
NATO	NATO	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
157	#num#	k4
<g/>
]	]	kIx)
O	o	k7c4
azyl	azyl	k1gInSc4
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
<g />
.	.	kIx.
</s>
<s hack="1">
požádal	požádat	k5eAaPmAgMnS
i	i	k9
turecký	turecký	k2eAgMnSc1d1
admirál	admirál	k1gMnSc1
Mustafa	Mustaf	k1gMnSc4
Ugurlu	Ugurla	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
sloužil	sloužit	k5eAaImAgMnS
v	v	k7c6
misi	mise	k1gFnSc6
NATO	NATO	kA
v	v	k7c6
Norfolku	Norfolek	k1gInSc6
ve	v	k7c6
Virginii	Virginie	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
158	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
převratu	převrat	k1gInSc6
byl	být	k5eAaImAgMnS
zatčen	zatknout	k5eAaPmNgMnS
také	také	k9
generálmajor	generálmajor	k1gMnSc1
Cahit	Cahit	k1gMnSc1
Bakir	Bakir	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
velel	velet	k5eAaImAgInS
tureckým	turecký	k2eAgNnSc7d1
silám	síla	k1gFnPc3
v	v	k7c6
mezinárodních	mezinárodní	k2eAgFnPc6d1
jednotkách	jednotka	k1gFnPc6
NATO	NATO	kA
v	v	k7c6
Afghánistánu	Afghánistán	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
159	#num#	k4
<g/>
]	]	kIx)
Turecký	turecký	k2eAgInSc1d1
provládní	provládní	k2eAgInSc1d1
deník	deník	k1gInSc1
Yeni	Yen	k1gFnSc2
Şafak	Şafak	k1gMnSc1
obvinil	obvinit	k5eAaPmAgMnS
amerického	americký	k2eAgMnSc4d1
generála	generál	k1gMnSc4
Johna	John	k1gMnSc4
Campbella	Campbell	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
velel	velet	k5eAaImAgInS
jednotkám	jednotka	k1gFnPc3
NATO	nato	k6eAd1
v	v	k7c6
Afghánistánu	Afghánistán	k1gInSc6
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
podílel	podílet	k5eAaImAgMnS
na	na	k7c4
plánování	plánování	k1gNnSc4
vojenského	vojenský	k2eAgInSc2d1
převratu	převrat	k1gInSc2
v	v	k7c6
Turecku	Turecko	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
160	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Podle	podle	k7c2
některých	některý	k3yIgMnPc2
komentátorů	komentátor	k1gMnPc2
NATO	NATO	kA
při	při	k7c6
svém	svůj	k3xOyFgNnSc6
rozšiřování	rozšiřování	k1gNnSc6
do	do	k7c2
východní	východní	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
snížilo	snížit	k5eAaPmAgNnS
své	svůj	k3xOyFgInPc4
vlastní	vlastní	k2eAgInPc4d1
standardy	standard	k1gInPc4
a	a	k8xC
spolupracuje	spolupracovat	k5eAaImIp3nS
i	i	k9
se	s	k7c7
zkorumpovanými	zkorumpovaný	k2eAgInPc7d1
a	a	k8xC
autokratickými	autokratický	k2eAgInPc7d1
režimy	režim	k1gInPc7
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
Černá	černý	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
pod	pod	k7c7
vedením	vedení	k1gNnSc7
Mila	Milus	k1gMnSc2
Djukanoviče	Djukanovič	k1gMnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
161	#num#	k4
<g/>
]	]	kIx)
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
k	k	k7c3
NATO	NATO	kA
přistoupila	přistoupit	k5eAaPmAgFnS
19	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2016	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
162	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mezi	mezi	k7c7
členskými	členský	k2eAgFnPc7d1
zeměmi	zem	k1gFnPc7
NATO	NATO	kA
existuje	existovat	k5eAaImIp3nS
několik	několik	k4yIc4
interních	interní	k2eAgInPc2d1
konfliktů	konflikt	k1gInPc2
<g/>
,	,	kIx,
například	například	k6eAd1
spor	spor	k1gInSc1
o	o	k7c4
Gibraltar	Gibraltar	k1gInSc4
mezi	mezi	k7c7
Španělskem	Španělsko	k1gNnSc7
a	a	k8xC
Spojeným	spojený	k2eAgNnSc7d1
královstvím	království	k1gNnSc7
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
163	#num#	k4
<g/>
]	]	kIx)
nepřátelství	nepřátelství	k1gNnSc1
mezi	mezi	k7c7
Řeckem	Řecko	k1gNnSc7
a	a	k8xC
Tureckem	Turecko	k1gNnSc7
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
164	#num#	k4
<g/>
]	]	kIx)
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
se	se	k3xPyFc4
vystupňovalo	vystupňovat	k5eAaPmAgNnS
po	po	k7c6
turecké	turecký	k2eAgFnSc6d1
invazi	invaze	k1gFnSc6
na	na	k7c4
Kypr	Kypr	k1gInSc4
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
severní	severní	k2eAgFnSc1d1
část	část	k1gFnSc1
turecká	turecký	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
nezákonně	zákonně	k6eNd1
okupuje	okupovat	k5eAaBmIp3nS
<g/>
,	,	kIx,
nebo	nebo	k8xC
spor	spor	k1gInSc1
mezi	mezi	k7c7
některými	některý	k3yIgFnPc7
členskými	členský	k2eAgFnPc7d1
zeměmi	zem	k1gFnPc7
NATO	NATO	kA
a	a	k8xC
Tureckem	Turecko	k1gNnSc7
týkající	týkající	k2eAgFnSc2d1
se	se	k3xPyFc4
americké	americký	k2eAgFnSc2d1
podpory	podpora	k1gFnSc2
Kurdů	Kurd	k1gMnPc2
v	v	k7c6
Sýrii	Sýrie	k1gFnSc6
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
165	#num#	k4
<g/>
]	]	kIx)
ale	ale	k8xC
také	také	k9
turecké	turecký	k2eAgFnSc2d1
podpory	podpora	k1gFnSc2
syrské	syrský	k2eAgFnSc2d1
teroristické	teroristický	k2eAgFnPc4d1
organizace	organizace	k1gFnSc2
An-Nusrá	An-Nusrý	k2eAgFnSc1d1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
byla	být	k5eAaImAgFnS
napojená	napojený	k2eAgFnSc1d1
na	na	k7c6
Al-Káidu	Al-Káid	k1gInSc6
<g/>
.	.	kIx.
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
[	[	kIx(
<g/>
166	#num#	k4
<g/>
]	]	kIx)
Německá	německý	k2eAgFnSc1d1
kancléřka	kancléřka	k1gFnSc1
Merkelová	Merkelová	k1gFnSc1
kritizovala	kritizovat	k5eAaImAgFnS
tureckou	turecký	k2eAgFnSc4d1
invazi	invaze	k1gFnSc4
na	na	k7c4
sever	sever	k1gInSc4
Sýrie	Sýrie	k1gFnSc2
obydlený	obydlený	k2eAgInSc4d1
převážně	převážně	k6eAd1
Kurdy	Kurd	k1gMnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
167	#num#	k4
<g/>
]	]	kIx)
Naopak	naopak	k6eAd1
šéf	šéf	k1gMnSc1
NATO	NATO	kA
Jens	Jens	k1gInSc1
Stoltenberg	Stoltenberg	k1gInSc1
tureckou	turecký	k2eAgFnSc4d1
invazi	invaze	k1gFnSc4
podpořil	podpořit	k5eAaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
168	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
tresčích	tresčí	k2eAgFnPc6d1
válkách	válka	k1gFnPc6
o	o	k7c4
rybolovná	rybolovný	k2eAgNnPc4d1
práva	právo	k1gNnPc4
mezi	mezi	k7c7
Islandem	Island	k1gInSc7
a	a	k8xC
Spojeným	spojený	k2eAgNnSc7d1
královstvím	království	k1gNnSc7
zvítězil	zvítězit	k5eAaPmAgInS
Island	Island	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
hrozil	hrozit	k5eAaImAgInS
<g/>
,	,	kIx,
že	že	k8xS
vystoupí	vystoupit	k5eAaPmIp3nS
z	z	k7c2
aliance	aliance	k1gFnSc2
a	a	k8xC
uzavře	uzavřít	k5eAaPmIp3nS
leteckou	letecký	k2eAgFnSc4d1
základnu	základna	k1gFnSc4
NATO	NATO	kA
v	v	k7c6
Keflavíku	Keflavík	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
169	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
NATO	NATO	kA
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
NÁLEVKA	nálevka	k1gFnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Světová	světový	k2eAgFnSc1d1
politika	politika	k1gFnSc1
ve	v	k7c6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
I.	I.	kA
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Aleš	Aleš	k1gMnSc1
Skřivan	Skřivan	k1gMnSc1
ml.	ml.	kA
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
902261	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
256	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
RAUŠOVÁ	RAUŠOVÁ	kA
<g/>
,	,	kIx,
Zuzana	Zuzana	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Francie	Francie	k1gFnSc1
se	se	k3xPyFc4
po	po	k7c6
43	#num#	k4
letech	léto	k1gNnPc6
vrátí	vrátit	k5eAaPmIp3nS
do	do	k7c2
vojenského	vojenský	k2eAgNnSc2d1
velení	velení	k1gNnSc2
NATO	NATO	kA
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2009-03-11	2009-03-11	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
FIDLER	FIDLER	kA
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
;	;	kIx,
MAREŠ	Mareš	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc1
NATO	NATO	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Litomyšl	Litomyšl	k1gFnSc1
<g/>
:	:	kIx,
Paseka	Paseka	k1gMnSc1
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
.	.	kIx.
244	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7185	#num#	k4
<g/>
-	-	kIx~
<g/>
145	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
138	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
jen	jen	k9
<g/>
:	:	kIx,
Fidler	Fidler	k1gInSc1
1997	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
FIALA	Fiala	k1gMnSc1
<g/>
,	,	kIx,
Luděk	Luděk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Severoatlantická	severoatlantický	k2eAgFnSc1d1
aliance	aliance	k1gFnSc1
(	(	kIx(
<g/>
NATO	NATO	kA
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
NATO	NATO	kA
and	and	k?
the	the	k?
fight	fight	k1gMnSc1
against	against	k1gMnSc1
terrorism	terrorism	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
NATO	NATO	kA
<g/>
.	.	kIx.
<g/>
int	int	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Counter-piracy	Counter-piracy	k1gInPc1
operations	operations	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
NATO	NATO	kA
<g/>
.	.	kIx.
<g/>
int	int	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
EU-NATO	EU-NATO	k1gFnSc1
<g/>
:	:	kIx,
The	The	k1gFnSc1
framework	framework	k1gInSc1
for	forum	k1gNnPc2
permanent	permanent	k1gInSc1
relations	relations	k1gInSc1
and	and	k?
Berlin	berlina	k1gFnPc2
Plus	plus	k6eAd1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rada	rada	k1gFnSc1
Evropské	evropský	k2eAgFnSc2d1
unie	unie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
http://www.ceskenoviny.cz/zpravy/cerna-hora-se-oficialne-stane-clenem-nato/1493276	http://www.ceskenoviny.cz/zpravy/cerna-hora-se-oficialne-stane-clenem-nato/1493276	k4
<g/>
↑	↑	k?
HER	hra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třicátým	třicátý	k4xOgFnPc3
členem	člen	k1gMnSc7
NATO	NATO	kA
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
Severní	severní	k2eAgFnSc1d1
Makedonie	Makedonie	k1gFnSc1
<g/>
,	,	kIx,
její	její	k3xOp3gInSc1
vstup	vstup	k1gInSc1
do	do	k7c2
Aliance	aliance	k1gFnSc2
komplikoval	komplikovat	k5eAaBmAgInS
spor	spor	k1gInSc1
s	s	k7c7
Řeckem	Řecko	k1gNnSc7
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
<g/>
,	,	kIx,
2020-03-27	2020-03-27	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Albánie	Albánie	k1gFnSc1
a	a	k8xC
Chorvatsko	Chorvatsko	k1gNnSc1
jsou	být	k5eAaImIp3nP
v	v	k7c6
NATO	NATO	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidovky	Lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
The	The	k1gFnPc2
SIPRI	SIPRI	kA
Military	Militara	k1gFnSc2
Expenditure	Expenditur	k1gMnSc5
Database	Databas	k1gMnSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
SIPRI	SIPRI	kA
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
The	The	k1gFnSc2
15	#num#	k4
countries	countries	k1gMnSc1
with	with	k1gMnSc1
the	the	k?
highest	highest	k1gMnSc1
military	militara	k1gFnSc2
expenditure	expenditur	k1gMnSc5
in	in	k?
2009	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
SIPRI	SIPRI	kA
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Fidler	Fidler	k1gInSc1
1997	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
33	#num#	k4
<g/>
–	–	k?
<g/>
34	#num#	k4
<g/>
↑	↑	k?
Washingtonská	washingtonský	k2eAgFnSc1d1
smlouva	smlouva	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
NATOAktual	NATOAktual	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Severoatlantická	severoatlantický	k2eAgFnSc1d1
smlouva	smlouva	k1gFnSc1
(	(	kIx(
<g/>
Washington	Washington	k1gInSc1
D.C.	D.C.	k1gFnSc2
<g/>
,	,	kIx,
4	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1949	#num#	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ministerstvo	ministerstvo	k1gNnSc1
obrany	obrana	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Treaty	Treata	k1gFnSc2
of	of	k?
Economic	Economic	k1gMnSc1
<g/>
,	,	kIx,
Social	Social	k1gMnSc1
and	and	k?
Cultural	Cultural	k1gFnSc1
Collaboration	Collaboration	k1gInSc1
and	and	k?
Collective	Collectiv	k1gInSc5
Self-Defence	Self-Defence	k1gFnSc1
(	(	kIx(
<g/>
Brussels	Brussels	k1gInSc1
<g/>
,	,	kIx,
17	#num#	k4
March	March	k1gInSc1
1948	#num#	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
European	European	k1gMnSc1
NAvigator	NAvigator	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Haglund	Haglund	k1gInSc1
<g/>
,	,	kIx,
D.	D.	kA
G.	G.	kA
North	Northa	k1gFnPc2
Atlantic	Atlantice	k1gFnPc2
Treaty	Treata	k1gFnSc2
Organization	Organization	k1gInSc1
(	(	kIx(
<g/>
NATO	NATO	kA
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
History	Histor	k1gInPc4
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
NATO	NATO	kA
<g/>
:	:	kIx,
Disappointing	Disappointing	k1gInSc1
Performance	performance	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Time	Time	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1952	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
The	The	k1gMnSc1
Man	Man	k1gMnSc1
with	with	k1gMnSc1
the	the	k?
Oilcan	Oilcan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Time	Time	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1952	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Miller	Miller	k1gMnSc1
<g/>
,	,	kIx,
A.	A.	kA
P.	P.	kA
SACLANT	SACLANT	kA
<g/>
:	:	kIx,
Guardian	Guardian	k1gMnSc1
of	of	k?
the	the	k?
Atlantic	Atlantice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
28	#num#	k4
až	až	k9
34	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
All	All	k1gFnSc1
Hands	Handsa	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
20	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
2009	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
28	#num#	k4
až	až	k9
34	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
16	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
2006	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Porter	porter	k1gInSc1
<g/>
,	,	kIx,
R.	R.	kA
Vast	Vastum	k1gNnPc2
sea	sea	k?
excercise	excercise	k1gFnSc2
to	ten	k3xDgNnSc1
be	be	k?
atomic	atomic	k1gMnSc1
test	test	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
New	New	k1gFnSc1
York	York	k1gInSc1
Times	Times	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1953	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
The	The	k1gMnSc1
increase	increase	k6eAd1
in	in	k?
strength	strength	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
NATO	NATO	kA
<g/>
.	.	kIx.
<g/>
int	int	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Haberman	Haberman	k1gMnSc1
<g/>
,	,	kIx,
C.	C.	kA
Evolution	Evolution	k1gInSc1
in	in	k?
Europe	Europ	k1gInSc5
<g/>
;	;	kIx,
Italy	Ital	k1gMnPc4
discloses	disclosesa	k1gFnPc2
its	its	k?
web	web	k1gInSc1
of	of	k?
Cold	Cold	k1gInSc1
War	War	k1gMnSc1
guerrillas	guerrillas	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
New	New	k1gFnSc1
York	York	k1gInSc1
Times	Times	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1990	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Fast	Fast	k2eAgInSc4d1
facts	facts	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Canadian	Canadian	k1gInSc1
Broadcasting	Broadcasting	k1gInSc4
Corporation	Corporation	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rev	Rev	k1gFnSc1
<g/>
.	.	kIx.
2009-04-06	2009-04-06	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
1955	#num#	k4
<g/>
:	:	kIx,
West	West	k1gMnSc1
Germany	German	k1gMnPc7
accepted	accepted	k1gMnSc1
into	into	k6eAd1
Nato	nato	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
On	on	k3xPp3gMnSc1
This	This	k1gInSc4
Day	Day	k1gFnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Emergency	Emergenca	k1gFnSc2
Call	Calla	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Time	Time	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1957-09-30	1957-09-30	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Fidler	Fidler	k1gInSc1
1997	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
133	#num#	k4
<g/>
↑	↑	k?
To	to	k9
Charles	Charles	k1gMnSc1
André	André	k1gMnPc2
Joseph	Joseph	k1gMnSc1
Marie	Maria	k1gFnSc2
de	de	k?
Gaulle	Gaulle	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dwight	Dwight	k1gInSc1
D.	D.	kA
Eisenhower	Eisenhower	k1gInSc1
Memorial	Memorial	k1gInSc1
Commission	Commission	k1gInSc1
<g/>
,	,	kIx,
1958-10-20	1958-10-20	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Q	Q	kA
<g/>
&	&	k?
<g/>
A	A	kA
<g/>
:	:	kIx,
France	Franc	k1gMnSc2
and	and	k?
Nato	nato	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rev	Rev	k1gFnSc1
<g/>
.	.	kIx.
2009-03-11	2009-03-11	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Fidler	Fidler	k1gInSc1
1997	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
136	#num#	k4
<g/>
↑	↑	k?
Cody	coda	k1gFnSc2
<g/>
,	,	kIx,
E.	E.	kA
After	After	k1gInSc4
43	#num#	k4
Years	Yearsa	k1gFnPc2
<g/>
,	,	kIx,
France	Franc	k1gMnPc4
to	ten	k3xDgNnSc4
Rejoin	Rejoin	k1gMnSc1
NATO	NATO	kA
as	as	k1gNnSc1
Full	Full	k1gMnSc1
Member	Member	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Washington	Washington	k1gInSc1
Post	post	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Fidler	Fidler	k1gInSc1
1997	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
140	#num#	k4
<g/>
↑	↑	k?
Fidler	Fidler	k1gInSc1
1997	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
1411	#num#	k4
2	#num#	k4
Fidler	Fidler	k1gInSc1
1997	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
145	#num#	k4
<g/>
–	–	k?
<g/>
1481	#num#	k4
2	#num#	k4
Fidler	Fidler	k1gInSc1
1997	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
150	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
–	–	k?
<g/>
153	#num#	k4
<g/>
↑	↑	k?
Fidler	Fidler	k1gInSc1
1997	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
158	#num#	k4
<g/>
↑	↑	k?
Fidler	Fidler	k1gInSc1
1997	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
162	#num#	k4
<g/>
↑	↑	k?
Fidler	Fidler	k1gInSc1
1997	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
169	#num#	k4
<g/>
↑	↑	k?
Fidler	Fidler	k1gInSc1
1997	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
174	#num#	k4
<g/>
↑	↑	k?
Fidler	Fidler	k1gInSc1
1997	#num#	k4
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
str	str	kA
<g/>
.	.	kIx.
179	#num#	k4
<g/>
↑	↑	k?
Fidler	Fidler	k1gInSc1
1997	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
1871	#num#	k4
2	#num#	k4
Fidler	Fidler	k1gInSc1
1997	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
1901	#num#	k4
2	#num#	k4
Fidler	Fidler	k1gInSc1
1997	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
193	#num#	k4
<g/>
-	-	kIx~
<g/>
194	#num#	k4
<g/>
↑	↑	k?
Fidler	Fidler	k1gInSc1
1997	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
198	#num#	k4
<g/>
↑	↑	k?
http://www.nato.int/nato_static_fl2014/assets/pdf/pdf_publications/20120905_what_is_nato_cz.pdf	http://www.nato.int/nato_static_fl2014/assets/pdf/pdf_publications/20120905_what_is_nato_cz.pdf	k1gMnSc1
<g/>
↑	↑	k?
LABANCA	LABANCA	kA
<g/>
,	,	kIx,
Nicola	Nicola	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Válečné	válečný	k2eAgInPc1d1
konflikty	konflikt	k1gInPc1
dneška	dnešek	k1gInSc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Fortuna	Fortuna	k1gFnSc1
Libri	Libr	k1gFnSc2
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
287	#num#	k4
s.	s.	k?
S.	S.	kA
205	#num#	k4
<g/>
-	-	kIx~
<g/>
206	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Cohen	Cohen	k1gInSc1
<g/>
,	,	kIx,
S.	S.	kA
F.	F.	kA
Gorbachev	Gorbachva	k1gFnPc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Lost	Lost	k1gInSc1
Legacy	Legacy	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Nation	Nation	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2005	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Zoellick	Zoellick	k1gMnSc1
<g/>
,	,	kIx,
R.	R.	kA
B.	B.	kA
The	The	k1gFnPc2
Lessons	Lessonsa	k1gFnPc2
of	of	k?
German	German	k1gMnSc1
Unification	Unification	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
National	National	k1gMnSc1
Interest	Interest	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2000-09-22	2000-09-22	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
↑	↑	k?
Blomfield	Blomfield	k1gInSc1
<g/>
,	,	kIx,
A.	A.	kA
<g/>
,	,	kIx,
Smith	Smith	k1gMnSc1
<g/>
,	,	kIx,
M.	M.	kA
Gorbachev	Gorbachev	k1gFnSc1
<g/>
:	:	kIx,
US	US	kA
could	could	k1gInSc1
start	start	k1gInSc1
new	new	k?
Cold	Cold	k1gMnSc1
War	War	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Daily	Daila	k1gFnSc2
Telegraph	Telegrapha	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Deset	deset	k4xCc4
let	léto	k1gNnPc2
Smlouvy	smlouva	k1gFnSc2
o	o	k7c6
konvenčních	konvenční	k2eAgFnPc6d1
ozbrojených	ozbrojený	k2eAgFnPc6d1
silách	síla	k1gFnPc6
v	v	k7c6
Evropě	Evropa	k1gFnSc6
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ministerstvo	ministerstvo	k1gNnSc1
obrany	obrana	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
LABANCA	LABANCA	kA
<g/>
,	,	kIx,
Nicola	Nicola	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Válečné	válečný	k2eAgInPc1d1
konflikty	konflikt	k1gInPc1
dneška	dnešek	k1gInSc2
/	/	kIx~
<g/>
od	od	k7c2
roku	rok	k1gInSc2
1945	#num#	k4
do	do	k7c2
současnosti	současnost	k1gFnSc2
<g/>
/	/	kIx~
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Fortuna	Fortuna	k1gFnSc1
Libri	Libr	k1gFnSc2
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
287	#num#	k4
s.	s.	k?
S.	S.	kA
202	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Treaty	Treata	k1gFnSc2
on	on	k3xPp3gMnSc1
Conventional	Conventional	k1gMnSc1
Armed	Armed	k1gMnSc1
Forces	Forces	k1gMnSc1
in	in	k?
Europe	Europ	k1gInSc5
(	(	kIx(
<g/>
CFE	CFE	kA
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
GlobalSecurity	GlobalSecurita	k1gFnPc1
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Jones	Jones	k1gMnSc1
<g/>
,	,	kIx,
J.	J.	kA
L.	L.	kA
Vojenské	vojenský	k2eAgFnSc2d1
záležitosti	záležitost	k1gFnSc2
<g/>
:	:	kIx,
transformace	transformace	k1gFnSc1
vojenských	vojenský	k2eAgFnPc2d1
struktur	struktura	k1gFnPc2
NATO	NATO	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
NATO	NATO	kA
<g/>
.	.	kIx.
<g/>
int	int	k?
<g/>
,	,	kIx,
2004-01	2004-01	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Schoofs	Schoofs	k1gInSc1
<g/>
,	,	kIx,
J.	J.	kA
Three	Three	k1gInSc1
years	years	k1gInSc1
of	of	k?
NATO	NATO	kA
Baltic	Baltice	k1gFnPc2
Air	Air	k1gFnSc1
Policing	Policing	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
NATO	NATO	kA
po	po	k7c6
summitu	summit	k1gInSc6
v	v	k7c6
Rize	Riga	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Revue	revue	k1gFnSc1
Politika	politika	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2006	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Kulhánek	Kulhánek	k1gMnSc1
<g/>
,	,	kIx,
J.	J.	kA
Summit	summit	k1gInSc1
v	v	k7c6
Bukurešti	Bukurešť	k1gFnSc6
a	a	k8xC
budoucnost	budoucnost	k1gFnSc4
NATO	NATO	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
NATOAktual	NATOAktual	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Filip	Filip	k1gMnSc1
<g/>
,	,	kIx,
V.	V.	kA
Vznikne	vzniknout	k5eAaPmIp3nS
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
opravdu	opravdu	k6eAd1
raketová	raketový	k2eAgFnSc1d1
základna	základna	k1gFnSc1
USA	USA	kA
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
Britské	britský	k2eAgInPc1d1
listy	list	k1gInPc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2006	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Europe	Europ	k1gInSc5
wants	wantsa	k1gFnPc2
NATO	NATO	kA
missile	missile	k6eAd1
defense	defense	k1gFnSc1
system	systo	k1gNnSc7
complementary	complementara	k1gFnSc2
to	ten	k3xDgNnSc1
U.	U.	kA
<g/>
S.	S.	kA
system	syst	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
China	China	k1gFnSc1
View	View	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2007-04-19	2007-04-19	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Česko	Česko	k1gNnSc1
a	a	k8xC
USA	USA	kA
podepsaly	podepsat	k5eAaPmAgFnP
hlavní	hlavní	k2eAgFnSc4d1
smlouvu	smlouva	k1gFnSc4
o	o	k7c6
radaru	radar	k1gInSc6
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Dražanová	Dražanová	k1gFnSc1
<g/>
,	,	kIx,
A.	A.	kA
Riceová	Riceová	k1gFnSc1
podepsala	podepsat	k5eAaPmAgFnS
ve	v	k7c6
Varšavě	Varšava	k1gFnSc6
smlouvu	smlouva	k1gFnSc4
o	o	k7c6
protiraketové	protiraketový	k2eAgFnSc6d1
základně	základna	k1gFnSc6
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Zakázaní	zakázaný	k2eAgMnPc1d1
mladí	mladý	k2eAgMnPc1d1
komunisté	komunista	k1gMnPc1
získali	získat	k5eAaPmAgMnP
přes	přes	k7c4
180	#num#	k4
tisíc	tisíc	k4xCgInPc2
podpisů	podpis	k1gInPc2
proti	proti	k7c3
radaru	radar	k1gInSc3
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Russia	Russius	k1gMnSc4
in	in	k?
defence	defenec	k1gMnSc4
warning	warning	k1gInSc1
to	ten	k3xDgNnSc4
US	US	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2007-04-26	2007-04-26	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Norway	Norwaa	k1gMnSc2
<g/>
:	:	kIx,
Russia	Russius	k1gMnSc2
to	ten	k3xDgNnSc1
freeze	freézt	k5eAaPmIp3nS
NATO	nato	k6eAd1
military	militar	k1gMnPc4
ties	ties	k6eAd1
–	–	k?
World	World	k1gInSc1
news	news	k1gInSc1
–	–	k?
Europe	Europ	k1gInSc5
–	–	k?
Russia	Russia	k1gFnSc1
–	–	k?
msnbc	msnbc	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
MSNBC	MSNBC	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2008-08-20	2008-08-20	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Základny	základna	k1gFnSc2
v	v	k7c6
Česku	Česko	k1gNnSc6
a	a	k8xC
Polsku	Polsko	k1gNnSc6
nebudou	být	k5eNaImBp3nP
<g/>
,	,	kIx,
potvrdili	potvrdit	k5eAaPmAgMnP
Obama	Obama	k?
i	i	k8xC
Gates	Gates	k1gMnSc1
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Kongres	kongres	k1gInSc1
kritizoval	kritizovat	k5eAaImAgInS
Obamu	Obama	k1gFnSc4
za	za	k7c4
pozdní	pozdní	k2eAgInSc4d1
telefonát	telefonát	k1gInSc4
do	do	k7c2
Česka	Česko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Týden	týden	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Easton	Easton	k1gInSc1
<g/>
,	,	kIx,
A.	A.	kA
Polish	Polish	k1gInSc1
hopes	hopes	k1gInSc1
shot	shot	k1gInSc1
down	downa	k1gFnPc2
by	by	kYmCp3nS
US	US	kA
move	mov	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Nebude	být	k5eNaImBp3nS
radar	radar	k1gInSc4
<g/>
,	,	kIx,
nebudou	být	k5eNaImBp3nP
ani	ani	k8xC
rakety	raketa	k1gFnPc1
v	v	k7c6
Kaliningradu	Kaliningrad	k1gInSc3
<g/>
,	,	kIx,
vzkázalo	vzkázat	k5eAaPmAgNnS
Rusko	Rusko	k1gNnSc1
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
NATO	nato	k6eAd1
odsoudilo	odsoudit	k5eAaPmAgNnS
smlouvu	smlouva	k1gFnSc4
OSN	OSN	kA
o	o	k7c6
zákazu	zákaz	k1gInSc6
jaderných	jaderný	k2eAgFnPc2d1
zbraní	zbraň	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Natoaktual	Natoaktual	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
21	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Většina	většina	k1gFnSc1
států	stát	k1gInPc2
světa	svět	k1gInSc2
na	na	k7c6
půdě	půda	k1gFnSc6
OSN	OSN	kA
podpořila	podpořit	k5eAaPmAgFnS
zákaz	zákaz	k1gInSc4
jaderných	jaderný	k2eAgFnPc2d1
zbraní	zbraň	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deník	deník	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
BARTUŠKA	Bartuška	k1gMnSc1
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsme	být	k5eAaImIp1nP
členem	člen	k1gMnSc7
NATO	NATO	kA
<g/>
,	,	kIx,
aliance	aliance	k1gFnSc1
má	mít	k5eAaImIp3nS
nyní	nyní	k6eAd1
19	#num#	k4
členů	člen	k1gInPc2
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1999-03-14	1999-03-14	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
20	#num#	k4
let	léto	k1gNnPc2
Česka	Česko	k1gNnSc2
v	v	k7c6
NATO	NATO	kA
–	–	k?
Online	Onlin	k1gInSc5
reportáž	reportáž	k1gFnSc1
–	–	k?
Souhrn	souhrn	k1gInSc1
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Evropa	Evropa	k1gFnSc1
zvyšuje	zvyšovat	k5eAaImIp3nS
výdaje	výdaj	k1gInPc4
na	na	k7c4
obranu	obrana	k1gFnSc4
<g/>
,	,	kIx,
Česko	Česko	k1gNnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
nich	on	k3xPp3gFnPc6
šesté	šestý	k4xOgFnSc3
od	od	k7c2
konce	konec	k1gInSc2
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-07-11	2018-07-11	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Makedonie	Makedonie	k1gFnSc1
podepsala	podepsat	k5eAaPmAgFnS
protokol	protokol	k1gInSc4
o	o	k7c4
přistoupení	přistoupení	k1gNnSc4
k	k	k7c3
NATO	NATO	kA
-	-	kIx~
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
www.novinky.cz	www.novinky.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Po	po	k7c6
letech	léto	k1gNnPc6
sporů	spor	k1gInPc2
konečně	konečně	k6eAd1
dohoda	dohoda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Makedonie	Makedonie	k1gFnSc1
a	a	k8xC
Řecko	Řecko	k1gNnSc1
podepsaly	podepsat	k5eAaPmAgInP
dohodu	dohoda	k1gFnSc4
o	o	k7c6
názvu	název	k1gInSc6
makedonského	makedonský	k2eAgInSc2d1
státu	stát	k1gInSc2
<g/>
.	.	kIx.
iROZHLAS	iROZHLAS	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Simsek	Simsek	k1gInSc1
<g/>
,	,	kIx,
A.	A.	kA
Cyprus	Cyprus	k1gInSc1
a	a	k8xC
sticking	sticking	k1gInSc1
point	pointa	k1gFnPc2
in	in	k?
EU-NATO	EU-NATO	k1gMnSc4
co-operation	co-operation	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Southeast	Southeast	k1gMnSc1
European	European	k1gMnSc1
Times	Times	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2007-06-14	2007-06-14	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Ukraine	Ukrain	k1gInSc5
<g/>
’	’	k?
<g/>
s	s	k7c7
parliament	parliament	k1gMnSc1
votes	votes	k1gMnSc1
to	ten	k3xDgNnSc4
abandon	abandon	k1gNnSc4
Nato	nato	k6eAd1
ambitions	ambitions	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Ukrajinská	ukrajinský	k2eAgFnSc1d1
ústava	ústava	k1gFnSc1
zakotví	zakotvit	k5eAaPmIp3nS
směřování	směřování	k1gNnSc4
země	zem	k1gFnSc2
do	do	k7c2
EU	EU	kA
a	a	k8xC
NATO	NATO	kA
-	-	kIx~
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
www.novinky.cz	www.novinky.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Ukrajina	Ukrajina	k1gFnSc1
ústavně	ústavně	k6eAd1
zakotvila	zakotvit	k5eAaPmAgFnS
směrování	směrování	k1gNnSc4
země	zem	k1gFnSc2
do	do	k7c2
EU	EU	kA
a	a	k8xC
NATO	NATO	kA
|	|	kIx~
Svět	svět	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidovky	Lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-02-07	2019-02-07	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
NATO	NATO	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
NATO	NATO	kA
recognises	recognisesa	k1gFnPc2
Ukraine	Ukrain	k1gInSc5
as	as	k1gNnSc2
Enhanced	Enhanced	k1gMnSc1
Opportunities	Opportunities	k1gMnSc1
Partner	partner	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
NATO	NATO	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Kraus	Kraus	k1gMnSc1
<g/>
,	,	kIx,
T.	T.	kA
NATO	NATO	kA
souhlasí	souhlasit	k5eAaImIp3nS
s	s	k7c7
přijetím	přijetí	k1gNnSc7
Bosny	Bosna	k1gFnSc2
a	a	k8xC
Hercegoviny	Hercegovina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
EuroZprávy	EuroZpráva	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Russia	Russia	k1gFnSc1
Against	Against	k1gMnSc1
Serbian	Serbian	k1gMnSc1
Membership	Membership	k1gMnSc1
In	In	k1gMnSc1
NATO	NATO	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Propagandist	Propagandist	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Deny	Dena	k1gFnSc2
Flight	Flighta	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
NATOAktual	NATOAktual	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Bellamy	Bellam	k1gInPc1
<g/>
,	,	kIx,
Ch	Ch	kA
<g/>
.	.	kIx.
Naval	navalit	k5eAaPmRp2nS
blockade	blockást	k5eAaPmIp3nS
lifts	lifts	k6eAd1
in	in	k?
Adriatic	Adriatice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Independent	independent	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1996	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Operation	Operation	k1gInSc1
Deliberate	Deliberat	k1gInSc5
Force	force	k1gFnPc3
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
GlobalSecurity	GlobalSecurita	k1gFnPc1
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Peace	Peaec	k1gInSc2
support	support	k1gInSc1
operations	operations	k1gInSc1
in	in	k?
Bosnia	Bosnium	k1gNnSc2
and	and	k?
Herzegovina	Herzegovina	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
NATO	NATO	kA
<g/>
.	.	kIx.
<g/>
int	int	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Allied	Allied	k1gInSc1
Force	force	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
NATOAktual	NATOAktual	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Security	Securita	k1gFnSc2
Council	Council	k1gMnSc1
rejects	rejectsa	k1gFnPc2
demand	demanda	k1gFnPc2
for	forum	k1gNnPc2
cessation	cessation	k1gInSc1
of	of	k?
use	usus	k1gInSc5
of	of	k?
force	force	k1gFnSc3
against	against	k1gMnSc1
Federal	Federal	k1gMnSc1
Republic	Republice	k1gFnPc2
of	of	k?
Yugoslavia	Yugoslavius	k1gMnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
OSN	OSN	kA
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
NATO	NATO	kA
<g/>
'	'	kIx"
<g/>
s	s	k7c7
role	role	k1gFnSc2
in	in	k?
Kosovo	Kosův	k2eAgNnSc1d1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
NATO	NATO	kA
<g/>
.	.	kIx.
<g/>
int	int	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
KFOR	KFOR	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ministerstvo	ministerstvo	k1gNnSc1
obrany	obrana	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2010-01-14	2010-01-14	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Operation	Operation	k1gInSc1
Essential	Essential	k1gMnSc1
Harvest	Harvest	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
NATO	NATO	kA
<g/>
.	.	kIx.
<g/>
int	int	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
V	v	k7c6
Radě	rada	k1gFnSc6
bezpečnosti	bezpečnost	k1gFnSc2
zazněly	zaznět	k5eAaImAgInP,k5eAaPmAgInP
rozdílné	rozdílný	k2eAgInPc1d1
názory	názor	k1gInPc1
na	na	k7c4
zásah	zásah	k1gInSc4
NATO	nato	k6eAd1
proti	proti	k7c3
srbským	srbský	k2eAgInPc3d1
vojenským	vojenský	k2eAgInPc3d1
cílům	cíl	k1gInPc3
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
OSN	OSN	kA
Praha	Praha	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
NATO	NATO	kA
Update	update	k1gInSc1
<g/>
:	:	kIx,
Invocation	Invocation	k1gInSc1
of	of	k?
Article	Article	k1gInSc4
5	#num#	k4
confirmed	confirmed	k1gInSc4
–	–	k?
2	#num#	k4
October	October	k1gInSc1
2001	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
NATO	NATO	kA
<g/>
.	.	kIx.
<g/>
int	int	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Ghafour	Ghafour	k1gMnSc1
<g/>
,	,	kIx,
H.	H.	kA
Nato	nato	k6eAd1
<g/>
’	’	k?
<g/>
s	s	k7c7
Kabul	Kabul	k1gInSc1
role	role	k1gFnSc1
is	is	k?
its	its	k?
first	first	k1gInSc1
outside	outsid	k1gMnSc5
Europe	Europ	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Daily	Daila	k1gFnSc2
Telegraph	Telegrapha	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2003	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
UNSC	UNSC	kA
Resolution	Resolution	k1gInSc4
1510	#num#	k4
<g/>
,	,	kIx,
October	October	k1gInSc1
13	#num#	k4
<g/>
,	,	kIx,
2003	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
SVĚTNIČKA	světnička	k1gFnSc1
<g/>
,	,	kIx,
Lubomír	Lubomír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
NATO	nato	k6eAd1
neustoupí	ustoupit	k5eNaPmIp3nS
<g/>
,	,	kIx,
stažení	stažení	k1gNnSc2
z	z	k7c2
afghánského	afghánský	k2eAgInSc2d1
venkova	venkov	k1gInSc2
by	by	kYmCp3nS
paralyzovalo	paralyzovat	k5eAaBmAgNnS
celou	celý	k2eAgFnSc4d1
misi	mise	k1gFnSc4
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2012-03-20	2012-03-20	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
DAT	datum	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Afghánistán	Afghánistán	k1gInSc1
<g/>
:	:	kIx,
končí	končit	k5eAaImIp3nS
mise	mise	k1gFnSc1
ISAF	ISAF	kA
<g/>
,	,	kIx,
začíná	začínat	k5eAaImIp3nS
Rezolutní	rezolutní	k2eAgFnSc1d1
podpora	podpora	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
s	s	k7c7
českými	český	k2eAgMnPc7d1
vojáky	voják	k1gMnPc7
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
ČT	ČT	kA
<g/>
24	#num#	k4
<g/>
,	,	kIx,
31	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
NATO	NATO	kA
<g/>
’	’	k?
<g/>
s	s	k7c7
assistance	assistanka	k1gFnSc3
to	ten	k3xDgNnSc4
Iraq	Iraq	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
NATO	NATO	kA
<g/>
.	.	kIx.
<g/>
int	int	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Barbero	Barbero	k1gNnSc4
<g/>
,	,	kIx,
M.	M.	kA
D.	D.	kA
NATO	NATO	kA
Training	Training	k1gInSc1
Mission	Mission	k1gInSc1
–	–	k?
Iraq	Iraq	k1gMnSc2
<g/>
:	:	kIx,
Tactical	Tactical	k1gMnSc2
Size	Siz	k1gMnSc2
<g/>
...	...	k?
<g/>
Strategic	Strategic	k1gMnSc1
Impact	Impact	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velitelství	velitelství	k1gNnSc1
spojeneckých	spojenecký	k2eAgFnPc2d1
sil	síla	k1gFnPc2
pro	pro	k7c4
operace	operace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Lynch	Lynch	k1gInSc1
<g/>
,	,	kIx,
R.	R.	kA
<g/>
,	,	kIx,
Janzen	Janzen	k2eAgMnSc1d1
<g/>
,	,	kIx,
P.	P.	kA
D.	D.	kA
NATO	NATO	kA
Training	Training	k1gInSc1
Mission	Mission	k1gInSc1
<g/>
–	–	k?
<g/>
Iraq	Iraq	k1gFnSc1
<g/>
:	:	kIx,
Looking	Looking	k1gInSc1
to	ten	k3xDgNnSc1
the	the	k?
Future	Futur	k1gMnSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
JFQ	JFQ	kA
Forum	forum	k1gNnSc1
<g/>
,	,	kIx,
2006	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
NATO	NATO	kA
otevřelo	otevřít	k5eAaPmAgNnS
v	v	k7c6
Iráku	Irák	k1gInSc6
výcvikovou	výcvikový	k2eAgFnSc4d1
akademii	akademie	k1gFnSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
NATOAktual	NATOAktual	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Výcviková	výcvikový	k2eAgFnSc1d1
mise	mise	k1gFnSc1
NATO	NATO	kA
v	v	k7c6
Iráku	Irák	k1gInSc6
-	-	kIx~
NTM-I	NTM-I	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ministerstvo	ministerstvo	k1gNnSc1
obrany	obrana	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2009-02-27	2009-02-27	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Counter-piracy	Counter-piracy	k1gInPc1
operations	operations	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
NATO	NATO	kA
<g/>
.	.	kIx.
<g/>
int	int	k?
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2011-06-17	2011-06-17	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Statement	Statement	k1gInSc1
by	by	kYmCp3nS
the	the	k?
NATO	NATO	kA
Secretary	Secretara	k1gFnSc2
General	General	k1gFnSc2
on	on	k3xPp3gMnSc1
Libya	Libya	k1gMnSc1
arms	arms	k6eAd1
embargo	embargo	k1gNnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
NATO	NATO	kA
<g/>
.	.	kIx.
<g/>
int	int	k?
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
NATO	NATO	kA
to	ten	k3xDgNnSc1
police	police	k1gFnPc1
Libya	Liby	k1gInSc2
no-fly	no-fnout	k5eAaPmAgFnP
zone	zon	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Al-Džazíra	Al-Džazír	k1gMnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
al-Ramahi	al-Ramahi	k1gNnSc1
<g/>
,	,	kIx,
K.	K.	kA
NATO	NATO	kA
strikes	strikes	k1gMnSc1
Tripoli	Tripole	k1gFnSc4
<g/>
,	,	kIx,
Gaddafi	Gaddafe	k1gFnSc4
army	arma	k1gMnSc2
close	clos	k1gMnSc2
on	on	k3xPp3gMnSc1
Misrata	Misrat	k2eAgMnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Star	Star	kA
Online	Onlin	k1gMnSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011-06-09	2011-06-09	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Cloud	Cloud	k1gInSc1
<g/>
,	,	kIx,
D.	D.	kA
S.	S.	kA
Gates	Gates	k1gMnSc1
calls	calls	k6eAd1
for	forum	k1gNnPc2
more	mor	k1gInSc5
NATO	NATO	kA
allies	allies	k1gMnSc1
to	ten	k3xDgNnSc4
join	join	k1gNnSc4
Libya	Libya	k1gMnSc1
air	air	k?
campaign	campaign	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Los	los	k1gMnSc1
Angeles	Angeles	k1gMnSc1
Times	Times	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Burns	Burns	k1gInSc1
<g/>
,	,	kIx,
R.	R.	kA
Gates	Gates	k1gInSc1
blasts	blasts	k1gInSc1
NATO	NATO	kA
<g/>
,	,	kIx,
questions	questions	k6eAd1
future	futur	k1gMnSc5
of	of	k?
alliance	allianka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Washington	Washington	k1gInSc1
Times	Times	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
NATO	NATO	kA
extends	extends	k1gInSc4
Libya	Libyum	k1gNnSc2
operations	operations	k6eAd1
to	ten	k3xDgNnSc4
September	September	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Raw	Raw	k1gFnSc1
Story	story	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Norway	Norwaa	k1gFnSc2
to	ten	k3xDgNnSc1
quit	quit	k1gMnSc1
Libya	Libya	k1gMnSc1
operation	operation	k1gInSc4
by	by	kYmCp3nS
August	August	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
CNN-IBN	CNN-IBN	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Gazur	Gazur	k1gMnSc1
<g/>
,	,	kIx,
Z.	Z.	kA
Norsko	Norsko	k1gNnSc1
stáhlo	stáhnout	k5eAaPmAgNnS
stíhačky	stíhačka	k1gFnPc4
z	z	k7c2
operací	operace	k1gFnPc2
západních	západní	k2eAgMnPc2d1
spojenců	spojenec	k1gMnPc2
v	v	k7c6
Libyi	Libye	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mediafax	Mediafax	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Kirkup	Kirkup	k1gInSc1
<g/>
,	,	kIx,
J.	J.	kA
Navy	Navy	k?
chief	chief	k1gMnSc1
<g/>
:	:	kIx,
Britain	Britain	k1gMnSc1
cannot	cannota	k1gFnPc2
keep	keep	k1gMnSc1
up	up	k?
its	its	k?
role	role	k1gFnSc2
in	in	k?
Libya	Libya	k1gMnSc1
air	air	k?
war	war	k?
due	due	k?
to	ten	k3xDgNnSc4
cuts	cuts	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Daily	Daila	k1gFnSc2
Telegraph	Telegrapha	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Nato	nato	k6eAd1
chief	chief	k1gInSc1
Rasmussen	Rasmussen	k2eAgInSc1d1
'	'	kIx"
<g/>
proud	proud	k1gInSc1
<g/>
'	'	kIx"
as	as	k1gNnSc1
Libya	Liby	k1gInSc2
mission	mission	k1gInSc1
ends	ends	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Partnerství	partnerství	k1gNnSc2
pro	pro	k7c4
mír	mír	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
NATOAktual	NATOAktual	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Nato	nato	k6eAd1
and	and	k?
Belarus	Belarus	k1gMnSc1
–	–	k?
partnership	partnership	k1gMnSc1
<g/>
,	,	kIx,
past	past	k1gFnSc1
tensions	tensions	k1gInSc1
and	and	k?
future	futur	k1gMnSc5
possibilities	possibilities	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Foreign	Foreign	k1gInSc1
Policy	Polica	k1gFnSc2
and	and	k?
Security	Securita	k1gFnSc2
Research	Resear	k1gFnPc6
Center	centrum	k1gNnPc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
NATO	NATO	kA
Topics	Topics	k1gInSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Euro-Atlantic	Euro-Atlantice	k1gFnPc2
Partnership	Partnership	k1gMnSc1
Council	Council	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
NATO	NATO	kA
<g/>
.	.	kIx.
<g/>
int	int	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
NATO	NATO	kA
Partner	partner	k1gMnSc1
countries	countries	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
NATO	NATO	kA
<g/>
.	.	kIx.
<g/>
int	int	k?
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2009-03-06	2009-03-06	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Pondová	Pondová	k1gFnSc1
<g/>
,	,	kIx,
S.	S.	kA
Jak	jak	k8xC,k8xS
správně	správně	k6eAd1
chápat	chápat	k5eAaImF
program	program	k1gInSc4
Partnerství	partnerství	k1gNnSc2
pro	pro	k7c4
mír	mír	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
NATO	NATO	kA
<g/>
.	.	kIx.
<g/>
int	int	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Individual	Individual	k1gInSc1
Partnership	Partnership	k1gInSc1
Action	Action	k1gInSc1
Plans	Plans	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
NATO	NATO	kA
<g/>
.	.	kIx.
<g/>
int	int	k?
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2010-10-27	2010-10-27	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
NATO	NATO	kA
<g/>
’	’	k?
<g/>
s	s	k7c7
relations	relations	k6eAd1
with	with	k1gInSc1
partners	partners	k6eAd1
across	across	k1gInSc1
the	the	k?
globe	globus	k1gInSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
NATO	NATO	kA
<g/>
.	.	kIx.
<g/>
int	int	k?
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2011-04-12	2011-04-12	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
NATO	NATO	kA
Headquarters	Headquartersa	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
NATO	NATO	kA
<g/>
.	.	kIx.
<g/>
int	int	k?
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2011-04-11	2011-04-11	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Struktura	struktura	k1gFnSc1
NATO	NATO	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
NATOAktual	NATOAktual	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Orgány	orgány	k1gFnPc4
odpovědné	odpovědný	k2eAgFnSc3d1
Severoatlantické	severoatlantický	k2eAgFnSc3d1
radě	rada	k1gFnSc3
<g/>
,	,	kIx,
Výboru	výbor	k1gInSc3
pro	pro	k7c4
plánování	plánování	k1gNnSc4
obrany	obrana	k1gFnSc2
a	a	k8xC
Skupině	skupina	k1gFnSc3
pro	pro	k7c4
jaderné	jaderný	k2eAgNnSc4d1
plánování	plánování	k1gNnSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
NATOAktual	NATOAktual	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
National	National	k1gMnPc1
delegations	delegations	k6eAd1
to	ten	k3xDgNnSc4
NATO	nato	k6eAd1
What	What	k1gMnSc1
is	is	k?
their	their	k1gMnSc1
role	role	k1gFnSc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
NATO	NATO	kA
<g/>
.	.	kIx.
<g/>
int	int	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
NATO	NATO	kA
Who	Who	k1gFnSc2
<g/>
’	’	k?
<g/>
s	s	k7c7
who	who	k?
<g/>
?	?	kIx.
</s>
<s desamb="1">
–	–	k?
Secretaries	Secretaries	k1gMnSc1
General	General	k1gFnSc2
of	of	k?
NATO	NATO	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
NATO	NATO	kA
<g/>
.	.	kIx.
<g/>
int	int	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
NATO	NATO	kA
Who	Who	k1gFnSc2
<g/>
’	’	k?
<g/>
s	s	k7c7
who	who	k?
<g/>
?	?	kIx.
</s>
<s desamb="1">
–	–	k?
Deputy	Deput	k2eAgInPc1d1
Secretaries	Secretaries	k1gInSc1
General	General	k1gFnSc2
of	of	k?
NATO	NATO	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
NATO	NATO	kA
<g/>
.	.	kIx.
<g/>
int	int	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
SACEUR	SACEUR	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velitelství	velitelství	k1gNnSc1
spojeneckých	spojenecký	k2eAgFnPc2d1
sil	síla	k1gFnPc2
pro	pro	k7c4
operace	operace	k1gFnPc4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Vrchním	vrchní	k2eAgMnSc7d1
velitelem	velitel	k1gMnSc7
sil	síla	k1gFnPc2
NATO	nato	k6eAd1
bude	být	k5eAaImBp3nS
generál	generál	k1gMnSc1
Scaparrotti	Scaparrotť	k1gFnSc2
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2016-03-14	2016-03-14	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Vrchním	vrchní	k1gMnSc7
velitelem	velitel	k1gMnSc7
NATO	nato	k6eAd1
je	být	k5eAaImIp3nS
bojový	bojový	k2eAgMnSc1d1
pilot	pilot	k1gMnSc1
Wolters	Woltersa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pentagon	Pentagon	k1gInSc1
žádá	žádat	k5eAaImIp3nS
o	o	k7c4
posily	posila	k1gFnPc4
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-05-03	2019-05-03	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Supreme	Suprem	k1gInSc5
Allied	Allied	k1gInSc1
Commander	Commander	k1gMnSc1
Transformation	Transformation	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velitelství	velitelství	k1gNnSc1
spojeneckých	spojenecký	k2eAgFnPc2d1
sil	síla	k1gFnPc2
pro	pro	k7c4
operace	operace	k1gFnPc4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
About	About	k1gInSc1
the	the	k?
NATO	NATO	kA
Parliamentary	Parliamentara	k1gFnSc2
Assembly	Assembly	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Parlamentní	parlamentní	k2eAgInSc4d1
shromáždění	shromáždění	k1gNnSc1
NATO	NATO	kA
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Stálá	stálý	k2eAgFnSc1d1
delegace	delegace	k1gFnSc1
do	do	k7c2
Parlamentního	parlamentní	k2eAgNnSc2d1
shromáždění	shromáždění	k1gNnSc2
NATO	NATO	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslanecká	poslanecký	k2eAgFnSc1d1
sněmovna	sněmovna	k1gFnSc1
Parlamentu	parlament	k1gInSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Member	Member	k1gInSc1
Parliaments	Parliaments	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Parlamentní	parlamentní	k2eAgInSc4d1
shromáždění	shromáždění	k1gNnSc1
NATO	NATO	kA
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Associate	Associat	k1gInSc5
Member	Member	k1gMnSc1
Parliaments	Parliaments	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Parlamentní	parlamentní	k2eAgInSc4d1
shromáždění	shromáždění	k1gNnSc1
NATO	NATO	kA
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Státy	stát	k1gInPc1
založily	založit	k5eAaPmAgInP
novou	nový	k2eAgFnSc4d1
Radu	rada	k1gFnSc4
NATO-Rusko	NATO-Rusko	k1gNnSc1
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2002	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Der	drát	k5eAaImRp2nS
Spiegel	Spiegel	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
26	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Der	drát	k5eAaImRp2nS
Spiegel	Spiegel	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Rusko	Rusko	k1gNnSc1
a	a	k8xC
USA	USA	kA
potvrdily	potvrdit	k5eAaPmAgFnP
konec	konec	k1gInSc4
smlouvy	smlouva	k1gFnSc2
o	o	k7c6
likvidaci	likvidace	k1gFnSc6
raket	raketa	k1gFnPc2
krátkého	krátký	k2eAgInSc2d1
a	a	k8xC
středního	střední	k2eAgInSc2d1
doletu	dolet	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Statement	Statement	k1gInSc1
on	on	k3xPp3gMnSc1
Russia	Russius	k1gMnSc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
failure	failur	k1gMnSc5
to	ten	k3xDgNnSc1
comply	compnout	k5eAaPmAgFnP
with	with	k1gInSc4
the	the	k?
Intermediate-Range	Intermediate-Range	k1gInSc1
Nuclear	Nuclear	k1gMnSc1
Forces	Forces	k1gMnSc1
(	(	kIx(
<g/>
INF	INF	kA
<g/>
)	)	kIx)
Treaty	Treata	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
NATO	NATO	kA
<g/>
,	,	kIx,
2019-02-01	2019-02-01	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Čekají	čekat	k5eAaImIp3nP
svět	svět	k1gInSc4
závody	závod	k1gInPc1
ve	v	k7c6
zbrojení	zbrojení	k1gNnSc6
<g/>
?	?	kIx.
</s>
<s desamb="1">
Smlouva	smlouva	k1gFnSc1
o	o	k7c4
likvidaci	likvidace	k1gFnSc4
raket	raketa	k1gFnPc2
mezi	mezi	k7c7
Washingtonem	Washington	k1gInSc7
a	a	k8xC
Moskvou	Moskva	k1gFnSc7
zanikla	zaniknout	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
rozhlas	rozhlas	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Putin	putin	k2eAgInSc1d1
vrací	vracet	k5eAaImIp3nS
úder	úder	k1gInSc1
<g/>
,	,	kIx,
zastavuje	zastavovat	k5eAaImIp3nS
dohodu	dohoda	k1gFnSc4
s	s	k7c7
USA	USA	kA
o	o	k7c6
likvidaci	likvidace	k1gFnSc6
raket	raketa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Russia	Russia	k1gFnSc1
may	may	k?
have	havat	k5eAaPmIp3nS
violated	violated	k1gInSc4
the	the	k?
INF	INF	kA
Treaty	Treata	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Here	Here	k1gFnSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
how	how	k?
the	the	k?
United	United	k1gInSc1
States	Statesa	k1gFnPc2
appears	appears	k6eAd1
to	ten	k3xDgNnSc4
have	have	k1gNnSc4
done	don	k1gMnSc5
the	the	k?
same	same	k1gNnPc4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
14	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
NATO	NATO	kA
a	a	k8xC
Damoklův	Damoklův	k2eAgInSc1d1
meč	meč	k1gInSc1
nukleárního	nukleární	k2eAgInSc2d1
konfliktu	konflikt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
18	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Smlouva	smlouva	k1gFnSc1
o	o	k7c4
omezení	omezení	k1gNnSc4
raket	raketa	k1gFnPc2
mezi	mezi	k7c7
Ruskem	Rusko	k1gNnSc7
a	a	k8xC
USA	USA	kA
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
pomohla	pomoct	k5eAaPmAgFnS
uzavřít	uzavřít	k5eAaPmF
studenou	studený	k2eAgFnSc4d1
válku	válka	k1gFnSc4
<g/>
,	,	kIx,
končí	končit	k5eAaImIp3nS
kvůli	kvůli	k7c3
čínskému	čínský	k2eAgInSc3d1
arzenálu	arzenál	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hospodářské	hospodářský	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
President	president	k1gMnSc1
Trump	Trump	k1gMnSc1
to	ten	k3xDgNnSc4
pull	pull	k1gMnSc1
US	US	kA
from	from	k1gInSc1
Russia	Russium	k1gNnSc2
missile	missile	k6eAd1
treaty	treata	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
<g/>
,	,	kIx,
20	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2018	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Vypovězení	vypovězení	k1gNnSc2
Reaganovy	Reaganův	k2eAgFnSc2d1
a	a	k8xC
Gorbačovovy	Gorbačovův	k2eAgFnSc2d1
smlouvy	smlouva	k1gFnSc2
by	by	kYmCp3nS
byl	být	k5eAaImAgInS
úder	úder	k1gInSc4
mezinárodnímu	mezinárodní	k2eAgNnSc3d1
právu	právo	k1gNnSc3
<g/>
,	,	kIx,
varuje	varovat	k5eAaImIp3nS
Moskva	Moskva	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
22	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2018	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Chaos	chaos	k1gInSc1
v	v	k7c6
Libyi	Libye	k1gFnSc6
<g/>
:	:	kIx,
tři	tři	k4xCgFnPc1
vlády	vláda	k1gFnPc1
<g/>
,	,	kIx,
islamisté	islamista	k1gMnPc1
a	a	k8xC
bezradná	bezradný	k2eAgFnSc1d1
Evropa	Evropa	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Týden	týden	k1gInSc1
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2016	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Fico	Fico	k1gMnSc1
si	se	k3xPyFc3
umýva	umýva	k6eAd1
ruky	ruka	k1gFnSc2
<g/>
:	:	kIx,
Za	za	k7c4
situáciu	situácius	k1gMnSc6
v	v	k7c6
Líbyi	Líby	k1gInSc6
<g/>
,	,	kIx,
Iraku	Irak	k1gInSc6
a	a	k8xC
Sýrii	Sýrie	k1gFnSc3
vraj	vraj	k6eAd1
zodpovednosť	zodpovednostit	k5eAaPmRp2nS
nenesieme	nesiit	k5eNaPmRp1nP,k5eNaImRp1nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálne	Aktuáln	k1gInSc5
<g/>
.	.	kIx.
<g/>
sk	sk	k?
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2015	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
http://www.timeslive.co.za/politics/2015/09/15/Zuma-blames-European-bombing-for-Libyan-refugee-crisis	http://www.timeslive.co.za/politics/2015/09/15/Zuma-blames-European-bombing-for-Libyan-refugee-crisis	k1gInSc1
<g/>
↑	↑	k?
http://thehill.com/blogs/ballot-box/presidential-races/254321-rand-paul-blames-hillary-for-refugee-crisis	http://thehill.com/blogs/ballot-box/presidential-races/254321-rand-paul-blames-hillary-for-refugee-crisis	k1gInSc1
<g/>
↑	↑	k?
http://www.telegraph.co.uk/news/politics/nigel-farage/11548171/Nigel-Farage-David-Cameron-directly-caused-Libyan-migrant-crisis.html	http://www.telegraph.co.uk/news/politics/nigel-farage/11548171/Nigel-Farage-David-Cameron-directly-caused-Libyan-migrant-crisis.html	k1gInSc1
<g/>
↑	↑	k?
Pomůže	pomoct	k5eAaPmIp3nS
NATO	nato	k6eAd1
s	s	k7c7
migranty	migrant	k1gMnPc7
<g/>
?	?	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
věc	věc	k1gFnSc1
EU	EU	kA
<g/>
,	,	kIx,
tvrdí	tvrdit	k5eAaImIp3nS
Hamáček	Hamáček	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Babiš	Babiš	k1gInSc1
proti	proti	k7c3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Týden	týden	k1gInSc1
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2015	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Německý	německý	k2eAgMnSc1d1
ministr	ministr	k1gMnSc1
zahraničí	zahraničí	k1gNnSc2
kritizoval	kritizovat	k5eAaImAgMnS
NATO	nato	k6eAd1
za	za	k7c2
chrastění	chrastění	k1gNnSc2
zbraněmi	zbraň	k1gFnPc7
a	a	k8xC
válečné	válečný	k2eAgNnSc4d1
štvaní	štvaní	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
<g/>
.	.	kIx.
18	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2016	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Levada-Center	Levada-Center	k1gInSc1
and	and	k?
Chicago	Chicago	k1gNnSc1
Council	Council	k1gMnSc1
on	on	k3xPp3gMnSc1
Global	globat	k5eAaImAgMnS
Affairs	Affairs	k1gInSc4
about	about	k2eAgMnSc1d1
Russian-American	Russian-American	k1gMnSc1
relations	relationsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Levada-Center	Levada-Center	k1gInSc1
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2016	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Stand	Standa	k1gFnPc2
Up	Up	k1gMnSc1
to	ten	k3xDgNnSc1
Erdogan	Erdogan	k1gInSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
Assault	Assault	k1gMnSc1
on	on	k3xPp3gMnSc1
Democracy	Democracy	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
National	National	k1gMnSc1
Review	Review	k1gMnSc1
(	(	kIx(
<g/>
NR	NR	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
19	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2017	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Foreign	Foreign	k1gInSc1
Minister	Minister	k1gMnSc1
Wants	Wantsa	k1gFnPc2
US	US	kA
Nukes	Nukes	k1gMnSc1
out	out	k?
of	of	k?
Germany	German	k1gInPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Der	drát	k5eAaImRp2nS
Spiegel	Spiegel	k1gMnSc1
<g/>
,	,	kIx,
30	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
About	About	k2eAgInSc4d1
40	#num#	k4
Turkish	Turkish	k1gInSc4
NATO	NATO	kA
soldiers	soldiers	k6eAd1
request	request	k5eAaPmF
asylum	asylum	k1gNnSc4
in	in	k?
Germany-media	Germany-medium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reuters	Reuters	k1gInSc1
<g/>
.	.	kIx.
28	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2017	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Turečtí	turecký	k2eAgMnPc1d1
důstojníci	důstojník	k1gMnPc1
při	při	k7c6
NATO	NATO	kA
požádali	požádat	k5eAaPmAgMnP
kvůli	kvůli	k7c3
čistkám	čistka	k1gFnPc3
po	po	k7c6
puči	puč	k1gInSc6
o	o	k7c4
azyl	azyl	k1gInSc4
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2016	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Turecký	turecký	k2eAgMnSc1d1
důstojník	důstojník	k1gMnSc1
požádal	požádat	k5eAaPmAgMnS
v	v	k7c6
USA	USA	kA
o	o	k7c4
azyl	azyl	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2016	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Two	Two	k1gMnSc1
Afghan-based	Afghan-based	k1gMnSc1
Turkish	Turkish	k1gMnSc1
generals	generalsa	k1gFnPc2
detained	detained	k1gMnSc1
in	in	k?
Dubai	Duba	k1gFnSc2
after	after	k1gMnSc1
failed	failed	k1gMnSc1
coup	coup	k1gInSc1
<g/>
:	:	kIx,
CNN	CNN	kA
Turk	Turk	k1gInSc1
<g/>
.	.	kIx.
www.reuters.com	www.reuters.com	k1gInSc1
<g/>
.	.	kIx.
26	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Důstojníci	důstojník	k1gMnPc1
USA	USA	kA
podpořili	podpořit	k5eAaPmAgMnP
pokus	pokus	k1gInSc4
o	o	k7c4
převrat	převrat	k1gInSc4
v	v	k7c6
Turecku	Turecko	k1gNnSc6
<g/>
,	,	kIx,
tvrdí	tvrdit	k5eAaImIp3nS
právník	právník	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Týden	týden	k1gInSc1
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2016	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Protesty	protest	k1gInPc1
v	v	k7c6
Černé	Černé	k2eAgFnSc6d1
Hoře	hora	k1gFnSc6
mají	mít	k5eAaImIp3nP
dvě	dva	k4xCgFnPc4
příčiny	příčina	k1gFnPc4
<g/>
:	:	kIx,
chudobu	chudoba	k1gFnSc4
a	a	k8xC
nejdéle	dlouho	k6eAd3
vládnoucího	vládnoucí	k2eAgMnSc4d1
autokratického	autokratický	k2eAgMnSc4d1
vůdce	vůdce	k1gMnSc4
v	v	k7c6
Evropě	Evropa	k1gFnSc6
Archivováno	archivovat	k5eAaBmNgNnS
10	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
2016	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
<g/>
.	.	kIx.
31	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2015	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
The	The	k1gFnSc1
Balkans	Balkans	k1gInSc1
<g/>
’	’	k?
Corrupt	Corrupt	k2eAgInSc1d1
Leaders	Leaders	k1gInSc1
are	ar	k1gInSc5
Playing	Playing	k1gInSc4
NATO	nato	k6eAd1
for	forum	k1gNnPc2
a	a	k8xC
Fool	Foola	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Foreign	Foreign	k1gInSc1
Policy	Polica	k1gFnSc2
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2017	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Diplomaté	diplomat	k1gMnPc1
se	se	k3xPyFc4
přou	přít	k5eAaImIp3nP
o	o	k7c4
Gibraltar	Gibraltar	k1gInSc4
<g/>
,	,	kIx,
někdejší	někdejší	k2eAgMnSc1d1
lídr	lídr	k1gMnSc1
konzervativců	konzervativec	k1gMnPc2
hrozí	hrozit	k5eAaImIp3nS
vojskem	vojsko	k1gNnSc7
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2017	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
‚	‚	k?
<g/>
Hra	hra	k1gFnSc1
s	s	k7c7
ohněm	oheň	k1gInSc7
<g/>
.	.	kIx.
<g/>
‘	‘	k?
Mezi	mezi	k7c4
členy	člen	k1gMnPc4
NATO	NATO	kA
bobtná	bobtnat	k5eAaImIp3nS
spor	spor	k1gInSc1
o	o	k7c4
těžbu	těžba	k1gFnSc4
ropy	ropa	k1gFnSc2
a	a	k8xC
plynu	plyn	k1gInSc2
<g/>
,	,	kIx,
padají	padat	k5eAaImIp3nP
i	i	k8xC
slova	slovo	k1gNnPc1
o	o	k7c4
použití	použití	k1gNnSc4
vojenské	vojenský	k2eAgFnSc2d1
síly	síla	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
27	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Američané	Američan	k1gMnPc1
odstrašují	odstrašovat	k5eAaImIp3nP
Turky	Turek	k1gMnPc4
<g/>
,	,	kIx,
na	na	k7c4
ochranu	ochrana	k1gFnSc4
Kurdů	Kurd	k1gMnPc2
vyslali	vyslat	k5eAaPmAgMnP
transportéry	transportér	k1gInPc4
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
30	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2017	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Turci	Turek	k1gMnPc1
a	a	k8xC
Saúdové	Saúda	k1gMnPc1
šokovali	šokovat	k5eAaBmAgMnP
Západ	západ	k1gInSc4
<g/>
,	,	kIx,
v	v	k7c6
Sýrii	Sýrie	k1gFnSc6
vsadili	vsadit	k5eAaPmAgMnP
na	na	k7c4
džihádisty	džihádista	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
13	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2015	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Hořící	hořící	k2eAgFnSc2d1
mešity	mešita	k1gFnSc2
<g/>
,	,	kIx,
vypjaté	vypjatý	k2eAgFnPc1d1
emoce	emoce	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bitva	bitva	k1gFnSc1
o	o	k7c4
Afrín	Afrín	k1gInSc4
má	mít	k5eAaImIp3nS
dozvuky	dozvuk	k1gInPc4
i	i	k9
v	v	k7c6
Německu	Německo	k1gNnSc6
<g/>
.	.	kIx.
iDnes	iDnes	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
23	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2018	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Šéf	šéf	k1gMnSc1
NATO	NATO	kA
<g/>
:	:	kIx,
Turecko	Turecko	k1gNnSc1
má	mít	k5eAaImIp3nS
právo	právo	k1gNnSc4
na	na	k7c4
obranu	obrana	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
na	na	k7c4
přiměřenou	přiměřený	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Týden	týden	k1gInSc1
<g/>
.	.	kIx.
25	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2018	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Když	když	k8xS
David	David	k1gMnSc1
porazí	porazit	k5eAaPmIp3nS
Goliáše	goliáš	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Island	Island	k1gInSc1
vyhrál	vyhrát	k5eAaPmAgInS
tresčí	tresčí	k2eAgFnPc4d1
války	válka	k1gFnPc4
díky	díky	k7c3
úloze	úloha	k1gFnSc3
v	v	k7c6
NATO	NATO	kA
<g/>
,	,	kIx,
brexit	brexit	k1gInSc1
přináší	přinášet	k5eAaImIp3nS
novou	nový	k2eAgFnSc4d1
hrozbu	hrozba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
16	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
FIDLER	FIDLER	kA
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
;	;	kIx,
MAREŠ	Mareš	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc1
NATO	NATO	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Litomyšl	Litomyšl	k1gFnSc1
<g/>
:	:	kIx,
Paseka	Paseka	k1gMnSc1
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
.	.	kIx.
244	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7185	#num#	k4
<g/>
-	-	kIx~
<g/>
145	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
DUIGNAN	DUIGNAN	kA
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
NATO	NATO	kA
<g/>
:	:	kIx,
Its	Its	k1gFnSc1
Past	past	k1gFnSc1
<g/>
,	,	kIx,
Present	Present	k1gInSc1
<g/>
,	,	kIx,
and	and	k?
Future	Futur	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stanford	Stanford	k1gInSc1
<g/>
:	:	kIx,
Hoover	Hoover	k1gInSc1
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
8179	#num#	k4
<g/>
-	-	kIx~
<g/>
9782	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
KAPLAN	Kaplan	k1gMnSc1
<g/>
,	,	kIx,
Lawrence	Lawrenec	k1gMnSc2
S.	S.	kA
NATO	NATO	kA
Divided	Divided	k1gMnSc1
<g/>
,	,	kIx,
NATO	NATO	kA
United	United	k1gInSc1
<g/>
:	:	kIx,
The	The	k1gFnSc1
Evolution	Evolution	k1gInSc1
of	of	k?
an	an	k?
Alliance	Alliance	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Westport	Westport	k1gInSc1
<g/>
:	:	kIx,
Greenwood	Greenwood	k1gInSc1
Publishing	Publishing	k1gInSc1
Group	Group	k1gInSc1
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
275983772	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
SANDLER	SANDLER	kA
<g/>
,	,	kIx,
Todd	Todd	k1gMnSc1
<g/>
;	;	kIx,
HARTLEY	HARTLEY	kA
<g/>
,	,	kIx,
Keith	Keith	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Political	Political	k1gFnSc1
Economy	Econom	k1gInPc1
of	of	k?
NATO	NATO	kA
<g/>
:	:	kIx,
Past	past	k1gFnSc1
<g/>
,	,	kIx,
Present	Present	k1gInSc1
<g/>
,	,	kIx,
and	and	k?
into	into	k1gNnSc4
the	the	k?
21	#num#	k4
<g/>
st	st	kA
Century	Centura	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cambridge	Cambridge	k1gFnSc1
<g/>
:	:	kIx,
Cambridge	Cambridge	k1gFnSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
521630931	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Vlajka	vlajka	k1gFnSc1
Severoatlantické	severoatlantický	k2eAgFnSc2d1
aliance	aliance	k1gFnSc2
</s>
<s>
Hymna	hymna	k1gFnSc1
Severoatlantické	severoatlantický	k2eAgFnSc2d1
aliance	aliance	k1gFnSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Severoatlantická	severoatlantický	k2eAgFnSc1d1
aliance	aliance	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Dílo	dílo	k1gNnSc4
Severoatlantická	severoatlantický	k2eAgFnSc1d1
smlouva	smlouva	k1gFnSc1
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
NATO	NATO	kA
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Kategorie	kategorie	k1gFnSc1
NATO	NATO	kA
ve	v	k7c6
Wikizprávách	Wikizpráva	k1gFnPc6
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgFnPc1d1
internetové	internetový	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Informační	informační	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
o	o	k7c6
NATO	NATO	kA
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
BBC	BBC	kA
–	–	k?
Profile	profil	k1gInSc5
<g/>
:	:	kIx,
Nato	nato	k6eAd1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Encyclopæ	Encyclopæ	k1gNnSc2
Britannica	Britannic	k1gInSc2
–	–	k?
North	North	k1gInSc1
Atlantic	Atlantice	k1gFnPc2
Treaty	Treata	k1gFnSc2
Organization	Organization	k1gInSc1
(	(	kIx(
<g/>
NATO	NATO	kA
<g/>
)	)	kIx)
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
GlobalSecurity	GlobalSecurit	k1gInPc1
<g/>
.	.	kIx.
<g/>
org	org	k?
–	–	k?
North	North	k1gInSc1
Atlantic	Atlantice	k1gFnPc2
Treaty	Treata	k1gFnSc2
Organization	Organization	k1gInSc1
(	(	kIx(
<g/>
NATO	NATO	kA
<g/>
)	)	kIx)
</s>
<s>
Zpravodajství	zpravodajství	k1gNnSc1
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Zprávy	zpráva	k1gFnPc1
s	s	k7c7
tématem	téma	k1gNnSc7
NATO	NATO	kA
na	na	k7c6
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Zprávy	zpráva	k1gFnPc1
s	s	k7c7
tématem	téma	k1gNnSc7
NATO	NATO	kA
na	na	k7c6
The	The	k1gFnSc6
Guardian	Guardiana	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Zprávy	zpráva	k1gFnPc1
s	s	k7c7
tématem	téma	k1gNnSc7
NATO	NATO	kA
na	na	k7c6
The	The	k1gFnSc6
New	New	k1gFnPc2
York	York	k1gInSc1
Times	Times	k1gMnSc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Zprávy	zpráva	k1gFnPc1
s	s	k7c7
tématem	téma	k1gNnSc7
NATO	NATO	kA
na	na	k7c6
The	The	k1gFnSc6
Wall	Wall	k1gMnSc1
Street	Street	k1gMnSc1
Journal	Journal	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Severoatlantická	severoatlantický	k2eAgFnSc1d1
aliance	aliance	k1gFnSc1
(	(	kIx(
<g/>
NATO	NATO	kA
<g/>
)	)	kIx)
</s>
<s>
Albánie	Albánie	k1gFnSc1
•	•	k?
Belgie	Belgie	k1gFnSc1
•	•	k?
Bulharsko	Bulharsko	k1gNnSc1
•	•	k?
Černá	černat	k5eAaImIp3nS
Hora	hora	k1gFnSc1
•	•	k?
Česko	Česko	k1gNnSc1
•	•	k?
Dánsko	Dánsko	k1gNnSc1
•	•	k?
Estonsko	Estonsko	k1gNnSc1
•	•	k?
Francie	Francie	k1gFnSc2
•	•	k?
Chorvatsko	Chorvatsko	k1gNnSc4
•	•	k?
Island	Island	k1gInSc1
•	•	k?
Itálie	Itálie	k1gFnSc2
•	•	k?
Kanada	Kanada	k1gFnSc1
•	•	k?
Litva	Litva	k1gFnSc1
•	•	k?
Lotyšsko	Lotyšsko	k1gNnSc1
•	•	k?
Lucembursko	Lucembursko	k1gNnSc1
•	•	k?
Maďarsko	Maďarsko	k1gNnSc1
•	•	k?
Německo	Německo	k1gNnSc1
•	•	k?
Nizozemsko	Nizozemsko	k1gNnSc1
•	•	k?
Norsko	Norsko	k1gNnSc1
•	•	k?
Polsko	Polsko	k1gNnSc1
•	•	k?
Portugalsko	Portugalsko	k1gNnSc1
•	•	k?
Rumunsko	Rumunsko	k1gNnSc1
•	•	k?
Řecko	Řecko	k1gNnSc1
•	•	k?
Severní	severní	k2eAgFnSc2d1
Makedonie	Makedonie	k1gFnSc2
•	•	k?
Slovensko	Slovensko	k1gNnSc1
•	•	k?
Slovinsko	Slovinsko	k1gNnSc1
•	•	k?
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
•	•	k?
Španělsko	Španělsko	k1gNnSc1
•	•	k?
Turecko	Turecko	k1gNnSc1
•	•	k?
USA	USA	kA
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
NATO	NATO	kA
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
kn	kn	k?
<g/>
20010711289	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
377-3	377-3	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
13718	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
1537	#num#	k4
6279	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
79006743	#num#	k4
|	|	kIx~
ULAN	ULAN	kA
<g/>
:	:	kIx,
500225812	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
148423701	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
79006743	#num#	k4
</s>
