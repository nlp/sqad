<s>
Zasloužilý	zasloužilý	k2eAgMnSc1d1	zasloužilý
umělec	umělec	k1gMnSc1	umělec
Waldemar	Waldemar	k1gMnSc1	Waldemar
Matuška	Matuška	k1gMnSc1	Matuška
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1932	[number]	k4	1932
Košice	Košice	k1gInPc4	Košice
-	-	kIx~	-
30	[number]	k4	30
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2009	[number]	k4	2009
St.	st.	kA	st.
Petersburg	Petersburg	k1gMnSc1	Petersburg
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
sklář	sklář	k1gMnSc1	sklář
<g/>
,	,	kIx,	,
banjista	banjista	k1gMnSc1	banjista
<g/>
,	,	kIx,	,
zpěvák	zpěvák	k1gMnSc1	zpěvák
a	a	k8xC	a
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejpopulárnějších	populární	k2eAgMnPc2d3	nejpopulárnější
československých	československý	k2eAgMnPc2d1	československý
zpěváků	zpěvák	k1gMnPc2	zpěvák
60	[number]	k4	60
<g/>
.	.	kIx.	.
a	a	k8xC	a
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
oblíben	oblíbit	k5eAaPmNgMnS	oblíbit
je	být	k5eAaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
