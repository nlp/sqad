<s>
Zasloužilý	zasloužilý	k2eAgMnSc1d1	zasloužilý
umělec	umělec	k1gMnSc1	umělec
Waldemar	Waldemar	k1gMnSc1	Waldemar
Matuška	Matuška	k1gMnSc1	Matuška
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1932	[number]	k4	1932
Košice	Košice	k1gInPc4	Košice
-	-	kIx~	-
30	[number]	k4	30
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2009	[number]	k4	2009
St.	st.	kA	st.
Petersburg	Petersburg	k1gMnSc1	Petersburg
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
sklář	sklář	k1gMnSc1	sklář
<g/>
,	,	kIx,	,
banjista	banjista	k1gMnSc1	banjista
<g/>
,	,	kIx,	,
zpěvák	zpěvák	k1gMnSc1	zpěvák
a	a	k8xC	a
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejpopulárnějších	populární	k2eAgMnPc2d3	nejpopulárnější
československých	československý	k2eAgMnPc2d1	československý
zpěváků	zpěvák	k1gMnPc2	zpěvák
60	[number]	k4	60
<g/>
.	.	kIx.	.
a	a	k8xC	a
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
oblíben	oblíbit	k5eAaPmNgMnS	oblíbit
je	být	k5eAaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Konec	konec	k1gInSc1	konec
života	život	k1gInSc2	život
strávil	strávit	k5eAaPmAgInS	strávit
v	v	k7c6	v
St.	st.	kA	st.
Petersburgu	Petersburg	k1gInSc2	Petersburg
na	na	k7c6	na
Floridě	Florida	k1gFnSc6	Florida
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
dlouholetou	dlouholetý	k2eAgFnSc7d1	dlouholetá
manželkou	manželka	k1gFnSc7	manželka
byla	být	k5eAaImAgFnS	být
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Olga	Olga	k1gFnSc1	Olga
Matušková	Matušková	k1gFnSc1	Matušková
<g/>
.	.	kIx.	.
</s>
<s>
Waldemar	Waldemar	k1gMnSc1	Waldemar
Matuška	Matuška	k1gMnSc1	Matuška
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
Košicích	Košice	k1gInPc6	Košice
<g/>
,	,	kIx,	,
dětství	dětství	k1gNnSc1	dětství
prožíval	prožívat	k5eAaImAgMnS	prožívat
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
Mia	Mia	k1gFnSc1	Mia
Malinová	Malinová	k1gFnSc1	Malinová
byla	být	k5eAaImAgFnS	být
zpěvačkou	zpěvačka	k1gFnSc7	zpěvačka
Vídeňských	vídeňský	k2eAgNnPc2d1	Vídeňské
operetních	operetní	k2eAgNnPc2d1	operetní
divadel	divadlo	k1gNnPc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Vyučil	vyučit	k5eAaPmAgMnS	vyučit
se	s	k7c7	s
sklářem	sklář	k1gMnSc7	sklář
v	v	k7c6	v
Poděbradech	Poděbrady	k1gInPc6	Poděbrady
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
vystupovat	vystupovat	k5eAaImF	vystupovat
s	s	k7c7	s
různými	různý	k2eAgFnPc7d1	různá
kapelami	kapela	k1gFnPc7	kapela
-	-	kIx~	-
zpíval	zpívat	k5eAaImAgMnS	zpívat
a	a	k8xC	a
hrál	hrát	k5eAaImAgMnS	hrát
na	na	k7c4	na
rozličné	rozličný	k2eAgInPc4d1	rozličný
hudební	hudební	k2eAgInPc4d1	hudební
nástroje	nástroj	k1gInPc4	nástroj
(	(	kIx(	(
<g/>
např.	např.	kA	např.
kontrabas	kontrabas	k1gInSc1	kontrabas
<g/>
,	,	kIx,	,
banjo	banjo	k1gNnSc1	banjo
a	a	k8xC	a
kytara	kytara	k1gFnSc1	kytara
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vojně	vojna	k1gFnSc6	vojna
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
jako	jako	k9	jako
osvětový	osvětový	k2eAgMnSc1d1	osvětový
tajemník	tajemník	k1gMnSc1	tajemník
v	v	k7c6	v
Chebu	Cheb	k1gInSc6	Cheb
<g/>
.	.	kIx.	.
</s>
<s>
Začínal	začínat	k5eAaImAgInS	začínat
v	v	k7c6	v
divadélku	divadélko	k1gNnSc6	divadélko
Reduta	reduta	k1gFnSc1	reduta
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
se	s	k7c7	s
Suchým	Suchý	k1gMnSc7	Suchý
a	a	k8xC	a
Jiřím	Jiří	k1gMnSc7	Jiří
Šlitrem	Šlitr	k1gMnSc7	Šlitr
<g/>
,	,	kIx,	,
hrál	hrát	k5eAaImAgMnS	hrát
se	se	k3xPyFc4	se
skupinou	skupina	k1gFnSc7	skupina
Mambo	Mamba	k1gFnSc5	Mamba
kvintet	kvintet	k1gInSc4	kvintet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
nahrál	nahrát	k5eAaPmAgMnS	nahrát
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
píseň	píseň	k1gFnSc4	píseň
Suvenýr	suvenýr	k1gInSc4	suvenýr
a	a	k8xC	a
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
zpěvákem	zpěvák	k1gMnSc7	zpěvák
a	a	k8xC	a
hercem	herec	k1gMnSc7	herec
divadla	divadlo	k1gNnSc2	divadlo
Semafor	Semafor	k1gInSc1	Semafor
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
brzy	brzy	k6eAd1	brzy
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
prknech	prkno	k1gNnPc6	prkno
zpíval	zpívat	k5eAaImAgMnS	zpívat
sólově	sólově	k6eAd1	sólově
i	i	k9	i
v	v	k7c6	v
duetu	duet	k1gInSc6	duet
-	-	kIx~	-
s	s	k7c7	s
Karlem	Karel	k1gMnSc7	Karel
Štědrým	štědrý	k2eAgMnSc7d1	štědrý
<g/>
,	,	kIx,	,
Jiřím	Jiří	k1gMnSc7	Jiří
Suchým	Suchý	k1gMnSc7	Suchý
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1961	[number]	k4	1961
Evou	Eva	k1gFnSc7	Eva
Pilarovou	Pilarův	k2eAgFnSc7d1	Pilarova
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
sklízel	sklízet	k5eAaImAgMnS	sklízet
největší	veliký	k2eAgInPc4d3	veliký
úspěchy	úspěch	k1gInPc4	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Dvakrát	dvakrát	k6eAd1	dvakrát
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
Zlatého	zlatý	k2eAgMnSc4d1	zlatý
slavíka	slavík	k1gMnSc4	slavík
(	(	kIx(	(
<g/>
ve	v	k7c6	v
vůbec	vůbec	k9	vůbec
prvním	první	k4xOgInSc6	první
ročníku	ročník	k1gInSc6	ročník
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
a	a	k8xC	a
druhého	druhý	k4xOgMnSc4	druhý
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
několikrát	několikrát	k6eAd1	několikrát
byl	být	k5eAaImAgMnS	být
druhý	druhý	k4xOgMnSc1	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
popularita	popularita	k1gFnSc1	popularita
rostla	růst	k5eAaImAgFnS	růst
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgMnS	začít
se	se	k3xPyFc4	se
objevovat	objevovat	k5eAaImF	objevovat
ve	v	k7c6	v
filmech	film	k1gInPc6	film
jako	jako	k8xC	jako
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
nazpíval	nazpívat	k5eAaBmAgMnS	nazpívat
písně	píseň	k1gFnPc4	píseň
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
či	či	k8xC	či
konci	konec	k1gInSc6	konec
oblíbených	oblíbený	k2eAgInPc2d1	oblíbený
seriálů	seriál	k1gInPc2	seriál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
odešel	odejít	k5eAaPmAgMnS	odejít
i	i	k9	i
s	s	k7c7	s
Evou	Eva	k1gFnSc7	Eva
Pilarovou	Pilarová	k1gFnSc7	Pilarová
a	a	k8xC	a
Karlem	Karel	k1gMnSc7	Karel
Štědrým	štědrý	k2eAgMnSc7d1	štědrý
do	do	k7c2	do
divadla	divadlo	k1gNnSc2	divadlo
Rokoko	rokoko	k1gNnSc1	rokoko
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
Eva	Eva	k1gFnSc1	Eva
Pilarová	Pilarová	k1gFnSc1	Pilarová
vrátila	vrátit	k5eAaPmAgFnS	vrátit
do	do	k7c2	do
Semaforu	Semafor	k1gInSc2	Semafor
<g/>
,	,	kIx,	,
zpíval	zpívat	k5eAaImAgMnS	zpívat
v	v	k7c6	v
duetu	duet	k1gInSc6	duet
s	s	k7c7	s
Helenou	Helena	k1gFnSc7	Helena
Vondráčkovou	Vondráčková	k1gFnSc7	Vondráčková
<g/>
,	,	kIx,	,
Martou	Marta	k1gFnSc7	Marta
Kubišovou	Kubišová	k1gFnSc7	Kubišová
<g/>
,	,	kIx,	,
Jitkou	Jitka	k1gFnSc7	Jitka
Zelenkovou	Zelenková	k1gFnSc7	Zelenková
a	a	k8xC	a
dalšími	další	k2eAgFnPc7d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
práce	práce	k1gFnSc2	práce
v	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
i	i	k9	i
na	na	k7c6	na
jiných	jiný	k2eAgInPc6d1	jiný
projektech	projekt	k1gInPc6	projekt
<g/>
,	,	kIx,	,
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
kterých	který	k3yRgMnPc2	který
nazpíval	nazpívat	k5eAaBmAgInS	nazpívat
duety	duet	k1gInPc7	duet
i	i	k8xC	i
s	s	k7c7	s
Hanou	Hana	k1gFnSc7	Hana
Hegerovou	Hegerová	k1gFnSc7	Hegerová
a	a	k8xC	a
Karlem	Karel	k1gMnSc7	Karel
Gottem	Gott	k1gMnSc7	Gott
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
soukromém	soukromý	k2eAgInSc6d1	soukromý
životě	život	k1gInSc6	život
Waldemara	Waldemar	k1gMnSc2	Waldemar
Matušky	Matuška	k1gMnSc2	Matuška
se	se	k3xPyFc4	se
vystřídalo	vystřídat	k5eAaPmAgNnS	vystřídat
několik	několik	k4yIc1	několik
partnerek	partnerka	k1gFnPc2	partnerka
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
první	první	k4xOgFnSc7	první
manželkou	manželka	k1gFnSc7	manželka
byla	být	k5eAaImAgFnS	být
Jiřina	Jiřina	k1gFnSc1	Jiřina
Šťástková	Šťástková	k1gFnSc1	Šťástková
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yQgFnSc7	který
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
narodila	narodit	k5eAaPmAgFnS	narodit
dvojčata	dvojče	k1gNnPc4	dvojče
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
pochybení	pochybení	k1gNnSc3	pochybení
při	při	k7c6	při
očkování	očkování	k1gNnSc6	očkování
obě	dva	k4xCgFnPc1	dva
holčičky	holčička	k1gFnPc1	holčička
zemřely	zemřít	k5eAaPmAgFnP	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
manželství	manželství	k1gNnSc6	manželství
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
narodil	narodit	k5eAaPmAgMnS	narodit
syn	syn	k1gMnSc1	syn
Miroslav	Miroslav	k1gMnSc1	Miroslav
<g/>
,	,	kIx,	,
aktuálně	aktuálně	k6eAd1	aktuálně
žijící	žijící	k2eAgMnSc1d1	žijící
v	v	k7c6	v
Hamburgu	Hamburg	k1gInSc6	Hamburg
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
druhé	druhý	k4xOgNnSc1	druhý
manželství	manželství	k1gNnSc1	manželství
s	s	k7c7	s
Hanou	Hana	k1gFnSc7	Hana
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
přezdívalo	přezdívat	k5eAaImAgNnS	přezdívat
Dulina	Dulino	k1gNnPc4	Dulino
<g/>
,	,	kIx,	,
také	také	k9	také
nedopadlo	dopadnout	k5eNaPmAgNnS	dopadnout
dobře	dobře	k6eAd1	dobře
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
velkou	velký	k2eAgFnSc7d1	velká
láskou	láska	k1gFnSc7	láska
byla	být	k5eAaImAgFnS	být
herečka	herečka	k1gFnSc1	herečka
Jitka	Jitka	k1gFnSc1	Jitka
Zelenohorská	zelenohorský	k2eAgFnSc1d1	Zelenohorská
(	(	kIx(	(
<g/>
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xC	jako
Zdenička	Zdenička	k1gFnSc1	Zdenička
z	z	k7c2	z
filmu	film	k1gInSc2	film
Ostře	ostro	k6eAd1	ostro
sledované	sledovaný	k2eAgInPc1d1	sledovaný
vlaky	vlak	k1gInPc1	vlak
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pětiletém	pětiletý	k2eAgInSc6d1	pětiletý
vztahu	vztah	k1gInSc6	vztah
však	však	k9	však
za	za	k7c2	za
bouřlivých	bouřlivý	k2eAgFnPc2d1	bouřlivá
okolností	okolnost	k1gFnPc2	okolnost
utekl	utéct	k5eAaPmAgInS	utéct
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
životní	životní	k2eAgFnSc3d1	životní
partnerce	partnerka	k1gFnSc3	partnerka
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
byla	být	k5eAaImAgFnS	být
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Olga	Olga	k1gFnSc1	Olga
Blechová	Blechová	k1gFnSc1	Blechová
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Olgou	Olga	k1gFnSc7	Olga
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
oženil	oženit	k5eAaPmAgMnS	oženit
<g/>
,	,	kIx,	,
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jeho	jeho	k3xOp3gNnSc4	jeho
třetí	třetí	k4xOgNnSc4	třetí
manželství	manželství	k1gNnSc4	manželství
<g/>
,	,	kIx,	,
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
K.	K.	kA	K.
T.	T.	kA	T.
O.	O.	kA	O.
(	(	kIx(	(
<g/>
Kamarádi	kamarád	k1gMnPc1	kamarád
táborových	táborový	k2eAgInPc2d1	táborový
ohňů	oheň	k1gInPc2	oheň
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
manželství	manželství	k1gNnSc2	manželství
s	s	k7c7	s
Olgou	Olga	k1gFnSc7	Olga
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
druhý	druhý	k4xOgMnSc1	druhý
syn	syn	k1gMnSc1	syn
Waldemar	Waldemar	k1gMnSc1	Waldemar
<g/>
.	.	kIx.	.
</s>
<s>
Matuškova	Matuškův	k2eAgFnSc1d1	Matuškova
popularita	popularita	k1gFnSc1	popularita
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
velká	velká	k1gFnSc1	velká
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
zasloužilým	zasloužilý	k2eAgMnSc7d1	zasloužilý
umělcem	umělec	k1gMnSc7	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Koncertoval	koncertovat	k5eAaImAgMnS	koncertovat
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
toho	ten	k3xDgMnSc4	ten
využil	využít	k5eAaPmAgInS	využít
a	a	k8xC	a
zůstal	zůstat	k5eAaPmAgInS	zůstat
i	i	k9	i
se	s	k7c7	s
ženou	žena	k1gFnSc7	žena
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
zabydlel	zabydlet	k5eAaPmAgMnS	zabydlet
se	se	k3xPyFc4	se
na	na	k7c6	na
Floridě	Florida	k1gFnSc6	Florida
<g/>
.	.	kIx.	.
</s>
<s>
Socialistické	socialistický	k2eAgInPc1d1	socialistický
úřady	úřad	k1gInPc1	úřad
poté	poté	k6eAd1	poté
zakázaly	zakázat	k5eAaPmAgInP	zakázat
hrát	hrát	k5eAaImF	hrát
jeho	jeho	k3xOp3gFnPc1	jeho
skladby	skladba	k1gFnPc1	skladba
<g/>
,	,	kIx,	,
zlikvidovaly	zlikvidovat	k5eAaPmAgFnP	zlikvidovat
jeho	jeho	k3xOp3gFnSc4	jeho
desku	deska	k1gFnSc4	deska
Jsem	být	k5eAaImIp1nS	být
svým	svůj	k3xOyFgMnSc7	svůj
pánem	pán	k1gMnSc7	pán
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
teprve	teprve	k6eAd1	teprve
měla	mít	k5eAaImAgFnS	mít
vyjít	vyjít	k5eAaPmF	vyjít
<g/>
,	,	kIx,	,
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
Chalupáři	chalupář	k1gMnSc3	chalupář
byl	být	k5eAaImAgInS	být
smazán	smazán	k2eAgInSc1d1	smazán
jeho	jeho	k3xOp3gInSc1	jeho
hlas	hlas	k1gInSc1	hlas
<g/>
,	,	kIx,	,
zůstala	zůstat	k5eAaPmAgFnS	zůstat
jen	jen	k9	jen
hudba	hudba	k1gFnSc1	hudba
<g/>
,	,	kIx,	,
v	v	k7c6	v
Rozpacích	rozpak	k1gInPc6	rozpak
kuchaře	kuchař	k1gMnSc2	kuchař
Svatopluka	Svatopluk	k1gMnSc2	Svatopluk
bylo	být	k5eAaImAgNnS	být
odstraněno	odstranit	k5eAaPmNgNnS	odstranit
jeho	jeho	k3xOp3gNnSc1	jeho
jméno	jméno	k1gNnSc1	jméno
z	z	k7c2	z
titulků	titulek	k1gInPc2	titulek
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
mu	on	k3xPp3gMnSc3	on
odňat	odňat	k2eAgInSc4d1	odňat
titul	titul	k1gInSc4	titul
zasloužilý	zasloužilý	k2eAgMnSc1d1	zasloužilý
umělec	umělec	k1gMnSc1	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Waldemar	Waldemar	k1gMnSc1	Waldemar
Matuška	Matuška	k1gMnSc1	Matuška
zatím	zatím	k6eAd1	zatím
v	v	k7c6	v
USA	USA	kA	USA
pořádal	pořádat	k5eAaImAgInS	pořádat
koncerty	koncert	k1gInPc4	koncert
pro	pro	k7c4	pro
krajany	krajan	k1gMnPc4	krajan
a	a	k8xC	a
vydával	vydávat	k5eAaPmAgMnS	vydávat
zde	zde	k6eAd1	zde
další	další	k2eAgFnPc4d1	další
desky	deska	k1gFnPc4	deska
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
půdní	půdní	k2eAgInSc1d1	půdní
byt	byt	k1gInSc1	byt
na	na	k7c6	na
Václavském	václavský	k2eAgNnSc6d1	Václavské
náměstí	náměstí	k1gNnSc6	náměstí
získal	získat	k5eAaPmAgMnS	získat
František	František	k1gMnSc1	František
Janeček	Janeček	k1gMnSc1	Janeček
<g/>
,	,	kIx,	,
Matuška	Matuška	k1gMnSc1	Matuška
se	se	k3xPyFc4	se
po	po	k7c6	po
revoluci	revoluce	k1gFnSc6	revoluce
marně	marně	k6eAd1	marně
soudil	soudit	k5eAaImAgMnS	soudit
o	o	k7c4	o
jeho	jeho	k3xOp3gNnSc4	jeho
navrácení	navrácení	k1gNnSc4	navrácení
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Floridě	Florida	k1gFnSc6	Florida
zůstal	zůstat	k5eAaPmAgMnS	zůstat
i	i	k9	i
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
padla	padnout	k5eAaImAgFnS	padnout
komunistická	komunistický	k2eAgFnSc1d1	komunistická
vláda	vláda	k1gFnSc1	vláda
a	a	k8xC	a
nastala	nastat	k5eAaPmAgFnS	nastat
renesance	renesance	k1gFnSc1	renesance
jím	jíst	k5eAaImIp1nS	jíst
zpívaných	zpívaný	k2eAgFnPc2d1	zpívaná
skladeb	skladba	k1gFnPc2	skladba
<g/>
.	.	kIx.	.
</s>
<s>
Česko	Česko	k1gNnSc1	Česko
ale	ale	k8xC	ale
často	často	k6eAd1	často
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
a	a	k8xC	a
občas	občas	k6eAd1	občas
ještě	ještě	k9	ještě
i	i	k9	i
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
pokročilému	pokročilý	k2eAgInSc3d1	pokročilý
věku	věk	k1gInSc3	věk
musel	muset	k5eAaImAgInS	muset
počet	počet	k1gInSc1	počet
vystoupení	vystoupení	k1gNnSc4	vystoupení
omezit	omezit	k5eAaPmF	omezit
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
30	[number]	k4	30
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2009	[number]	k4	2009
na	na	k7c6	na
Floridě	Florida	k1gFnSc6	Florida
na	na	k7c4	na
zápal	zápal	k1gInSc4	zápal
plic	plíce	k1gFnPc2	plíce
a	a	k8xC	a
následné	následný	k2eAgNnSc4d1	následné
selhání	selhání	k1gNnSc4	selhání
srdce	srdce	k1gNnSc2	srdce
<g/>
.	.	kIx.	.
</s>
<s>
Pohřeb	pohřeb	k1gInSc1	pohřeb
měl	mít	k5eAaImAgInS	mít
18	[number]	k4	18
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2009	[number]	k4	2009
a	a	k8xC	a
přišlo	přijít	k5eAaPmAgNnS	přijít
na	na	k7c4	na
něj	on	k3xPp3gInSc4	on
kolem	kolem	k7c2	kolem
dvanácti	dvanáct	k4xCc2	dvanáct
tisíc	tisíc	k4xCgInPc2	tisíc
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Místem	místo	k1gNnSc7	místo
jeho	jeho	k3xOp3gInSc2	jeho
posledního	poslední	k2eAgInSc2d1	poslední
odpočinku	odpočinek	k1gInSc2	odpočinek
je	být	k5eAaImIp3nS	být
Vyšehradský	vyšehradský	k2eAgInSc4d1	vyšehradský
hřbitov	hřbitov	k1gInSc4	hřbitov
<g/>
.	.	kIx.	.
</s>
<s>
Zpívá	zpívat	k5eAaImIp3nS	zpívat
Waldemar	Waldemar	k1gMnSc1	Waldemar
Matuška	Matuška	k1gMnSc1	Matuška
-	-	kIx~	-
1965	[number]	k4	1965
-	-	kIx~	-
Supraphon	supraphon	k1gInSc1	supraphon
<g/>
,	,	kIx,	,
reedice	reedice	k1gFnSc1	reedice
1996	[number]	k4	1996
-	-	kIx~	-
Bonton	bonton	k1gInSc1	bonton
Music	Musice	k1gFnPc2	Musice
(	(	kIx(	(
<g/>
název	název	k1gInSc1	název
Nebeský	nebeský	k2eAgMnSc1d1	nebeský
kovboj	kovboj	k1gMnSc1	kovboj
<g/>
)	)	kIx)	)
Waldemar	Waldemar	k1gMnSc1	Waldemar
Matuška	Matuška	k1gMnSc1	Matuška
-	-	kIx~	-
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
reedice	reedice	k1gFnSc1	reedice
1996	[number]	k4	1996
-	-	kIx~	-
Bonton	bonton	k1gInSc1	bonton
Music	Musice	k1gFnPc2	Musice
(	(	kIx(	(
<g/>
název	název	k1gInSc1	název
Sbohem	sbohem	k0	sbohem
<g/>
,	,	kIx,	,
lásko	láska	k1gFnSc5	láska
<g/>
)	)	kIx)	)
Osm	osm	k4xCc4	osm
lásek	láska	k1gFnPc2	láska
Waldemara	Waldemar	k1gMnSc4	Waldemar
Matušky	Matuška	k1gMnSc2	Matuška
(	(	kIx(	(
<g/>
recitál	recitál	k1gInSc1	recitál
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
reedice	reedice	k1gFnSc1	reedice
1999	[number]	k4	1999
Johoho	Joho	k1gMnSc4	Joho
(	(	kIx(	(
<g/>
Waldemar	Waldemar	k1gMnSc1	Waldemar
Matuška	Matuška	k1gMnSc1	Matuška
zpívá	zpívat	k5eAaImIp3nS	zpívat
písně	píseň	k1gFnPc4	píseň
o	o	k7c6	o
moři	moře	k1gNnSc6	moře
<g/>
)	)	kIx)	)
-	-	kIx~	-
1971	[number]	k4	1971
-	-	kIx~	-
Supraphon	supraphon	k1gInSc1	supraphon
<g/>
,	,	kIx,	,
reedice	reedice	k1gFnSc1	reedice
1974	[number]	k4	1974
<g/>
,	,	kIx,	,
na	na	k7c6	na
CD	CD	kA	CD
1997	[number]	k4	1997
Waldemar	Waldemar	k1gMnSc1	Waldemar
Matuška	Matuška	k1gMnSc1	Matuška
(	(	kIx(	(
<g/>
1966	[number]	k4	1966
<g/>
-	-	kIx~	-
<g/>
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
-	-	kIx~	-
1971	[number]	k4	1971
<g/>
,	,	kIx,	,
určeno	určen	k2eAgNnSc1d1	určeno
pro	pro	k7c4	pro
export	export	k1gInSc4	export
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
výběr	výběr	k1gInSc4	výběr
Lidové	lidový	k2eAgFnSc2d1	lidová
písně	píseň	k1gFnSc2	píseň
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
-	-	kIx~	-
1972	[number]	k4	1972
-	-	kIx~	-
Supraphon	supraphon	k1gInSc4	supraphon
<g/>
,	,	kIx,	,
reedice	reedice	k1gFnSc2	reedice
1996	[number]	k4	1996
-	-	kIx~	-
Bonton	bonton	k1gInSc4	bonton
Music	Musice	k1gFnPc2	Musice
Láska	láska	k1gFnSc1	láska
nebeská	nebeský	k2eAgFnSc5d1	nebeská
(	(	kIx(	(
<g/>
s	s	k7c7	s
Evou	Eva	k1gFnSc7	Eva
Pilarovou	Pilarová	k1gFnSc7	Pilarová
<g/>
)	)	kIx)	)
-	-	kIx~	-
1973	[number]	k4	1973
-	-	kIx~	-
Supraphon	supraphon	k1gInSc4	supraphon
<g/>
,	,	kIx,	,
reedice	reedice	k1gFnSc2	reedice
1997	[number]	k4	1997
-	-	kIx~	-
Bonton	bonton	k1gInSc4	bonton
Music	Musice	k1gFnPc2	Musice
Kluci	kluk	k1gMnPc1	kluk
do	do	k7c2	do
nepohody	nepohoda	k1gFnSc2	nepohoda
-	-	kIx~	-
1976	[number]	k4	1976
<g/>
,	,	kIx,	,
reedice	reedice	k1gFnSc2	reedice
2000	[number]	k4	2000
Mám	mít	k5eAaImIp1nS	mít
vás	vy	k3xPp2nPc4	vy
všechny	všechen	k3xTgInPc4	všechen
stejně	stejně	k9	stejně
<g />
.	.	kIx.	.
</s>
<s>
rád	rád	k2eAgMnSc1d1	rád
-	-	kIx~	-
1976	[number]	k4	1976
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
MC	MC	kA	MC
<g/>
,	,	kIx,	,
výběr	výběr	k1gInSc1	výběr
Co	co	k3yInSc4	co
děláš	dělat	k5eAaImIp2nS	dělat
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
dělej	dělat	k5eAaImRp2nS	dělat
rád	rád	k6eAd1	rád
-	-	kIx~	-
1977	[number]	k4	1977
Suvenýr	suvenýr	k1gInSc1	suvenýr
(	(	kIx(	(
<g/>
Waldemar	Waldemar	k1gMnSc1	Waldemar
Matuška	Matuška	k1gMnSc1	Matuška
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
12	[number]	k4	12
nej	nej	k?	nej
<g/>
)	)	kIx)	)
-	-	kIx~	-
1980	[number]	k4	1980
<g/>
,	,	kIx,	,
reedice	reedice	k1gFnSc1	reedice
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
výběr	výběr	k1gInSc1	výběr
Waldemar	Waldemar	k1gMnSc1	Waldemar
-	-	kIx~	-
1983	[number]	k4	1983
Country	country	k2eAgFnPc2d1	country
Door	Doora	k1gFnPc2	Doora
Is	Is	k1gFnPc2	Is
Always	Always	k1gInSc1	Always
Open	Open	k1gMnSc1	Open
-	-	kIx~	-
1985	[number]	k4	1985
Waldemar	Waldemar	k1gMnSc1	Waldemar
<g />
.	.	kIx.	.
</s>
<s>
Matuška	matuška	k1gFnSc1	matuška
(	(	kIx(	(
<g/>
1966	[number]	k4	1966
-	-	kIx~	-
971	[number]	k4	971
<g/>
)	)	kIx)	)
-	-	kIx~	-
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
edice	edice	k1gFnSc1	edice
Trezor	trezor	k1gInSc1	trezor
<g/>
,	,	kIx,	,
reedice	reedice	k1gFnSc1	reedice
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
desky	deska	k1gFnSc2	deska
<g/>
,	,	kIx,	,
výběr	výběr	k1gInSc4	výběr
Co	co	k3yInSc4	co
neodnesl	odnést	k5eNaPmAgInS	odnést
čas	čas	k1gInSc1	čas
-	-	kIx~	-
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
výběr	výběr	k1gInSc1	výběr
Nej	Nej	k1gFnSc2	Nej
<g/>
,	,	kIx,	,
nej	nej	k?	nej
<g/>
,	,	kIx,	,
nej	nej	k?	nej
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
-	-	kIx~	-
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
živá	živý	k2eAgFnSc1d1	živá
nahrávka	nahrávka	k1gFnSc1	nahrávka
Nebeskej	Nebeskej	k?	Nebeskej
kovboj	kovboj	k1gMnSc1	kovboj
(	(	kIx(	(
<g/>
reedice	reedice	k1gFnSc1	reedice
<g />
.	.	kIx.	.
</s>
<s>
LP	LP	kA	LP
Zpívá	zpívat	k5eAaImIp3nS	zpívat
Waldemar	Waldemar	k1gMnSc1	Waldemar
Matuška	Matuška	k1gMnSc1	Matuška
<g/>
)	)	kIx)	)
-	-	kIx~	-
1996	[number]	k4	1996
Sbohem	sbohem	k0	sbohem
<g/>
,	,	kIx,	,
lásko	láska	k1gFnSc5	láska
(	(	kIx(	(
<g/>
reedice	reedice	k1gFnSc2	reedice
LP	LP	kA	LP
Waldemar	Waldemar	k1gMnSc1	Waldemar
Matuška	Matuška	k1gMnSc1	Matuška
<g/>
)	)	kIx)	)
-	-	kIx~	-
1996	[number]	k4	1996
Jsem	být	k5eAaImIp1nS	být
svým	svůj	k3xOyFgMnSc7	svůj
pánem	pán	k1gMnSc7	pán
-	-	kIx~	-
1997	[number]	k4	1997
Waldemar	Waldemar	k1gMnSc1	Waldemar
Matuška	Matuška	k1gMnSc1	Matuška
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc2	jeho
23	[number]	k4	23
suvenýrů	suvenýr	k1gInPc2	suvenýr
-	-	kIx~	-
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
výběr	výběr	k1gInSc4	výběr
To	to	k9	to
všechno	všechen	k3xTgNnSc4	všechen
odnes	odnést	k5eAaPmRp2nS	odnést
čas	čas	k1gInSc1	čas
-	-	kIx~	-
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
výběr	výběr	k1gInSc4	výběr
Niagára	Niagár	k1gMnSc2	Niagár
-	-	kIx~	-
1999	[number]	k4	1999
<g />
.	.	kIx.	.
</s>
<s>
Síň	síň	k1gFnSc1	síň
slávy	sláva	k1gFnSc2	sláva
-	-	kIx~	-
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
výběr	výběr	k1gInSc1	výběr
Portrét	portrét	k1gInSc1	portrét
hvězd	hvězda	k1gFnPc2	hvězda
-	-	kIx~	-
Waldemar	Waldemar	k1gMnSc1	Waldemar
Matuška	Matuška	k1gMnSc1	Matuška
-	-	kIx~	-
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
výběr	výběr	k1gInSc1	výběr
S	s	k7c7	s
čertem	čert	k1gMnSc7	čert
si	se	k3xPyFc3	se
hrát	hrát	k5eAaImF	hrát
(	(	kIx(	(
<g/>
s	s	k7c7	s
Evou	Eva	k1gFnSc7	Eva
Pilarovou	Pilarová	k1gFnSc7	Pilarová
<g/>
)	)	kIx)	)
-	-	kIx~	-
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
výběr	výběr	k1gInSc1	výběr
To	ten	k3xDgNnSc1	ten
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
-	-	kIx~	-
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
výběr	výběr	k1gInSc1	výběr
Co	co	k9	co
neodnesl	odnést	k5eNaPmAgInS	odnést
čas	čas	k1gInSc4	čas
(	(	kIx(	(
<g/>
Největší	veliký	k2eAgInPc4d3	veliký
TV	TV	kA	TV
hity	hit	k1gInPc4	hit
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
výběr	výběr	k1gInSc1	výběr
Dobrý	dobrý	k2eAgInSc1d1	dobrý
večer	večer	k6eAd1	večer
s	s	k7c7	s
Waldemarem	Waldemar	k1gMnSc7	Waldemar
(	(	kIx(	(
<g/>
TV	TV	kA	TV
písničky	písnička	k1gFnSc2	písnička
<g/>
)	)	kIx)	)
-	-	kIx~	-
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
výběr	výběr	k1gInSc4	výběr
Šťastné	Šťastné	k2eAgFnSc1d1	Šťastné
Vánoce	Vánoce	k1gFnPc1	Vánoce
-	-	kIx~	-
2004	[number]	k4	2004
Waldemar	Waldemar	k1gMnSc1	Waldemar
Matuška	Matuška	k1gMnSc1	Matuška
Gold	Gold	k1gMnSc1	Gold
-	-	kIx~	-
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
výběr	výběr	k1gInSc1	výběr
Slavík	slavík	k1gInSc1	slavík
z	z	k7c2	z
Madridu	Madrid	k1gInSc2	Madrid
(	(	kIx(	(
<g/>
Největší	veliký	k2eAgInPc4d3	veliký
hity	hit	k1gInPc4	hit
<g/>
)	)	kIx)	)
-	-	kIx~	-
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
výběr	výběr	k1gInSc1	výběr
Sbohem	sbohem	k0	sbohem
<g/>
,	,	kIx,	,
lásko	láska	k1gFnSc5	láska
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
kolekce	kolekce	k1gFnSc1	kolekce
-	-	kIx~	-
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
výběr	výběr	k1gInSc1	výběr
Vždyť	vždyť	k9	vždyť
jsou	být	k5eAaImIp3nP	být
Vánoce	Vánoce	k1gFnPc1	Vánoce
-	-	kIx~	-
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
výběr	výběr	k1gInSc1	výběr
(	(	kIx(	(
<g/>
příloha	příloha	k1gFnSc1	příloha
časopisu	časopis	k1gInSc2	časopis
Vlasta	Vlasta	k1gFnSc1	Vlasta
<g/>
,	,	kIx,	,
samostatně	samostatně	k6eAd1	samostatně
neprodejné	prodejný	k2eNgNnSc1d1	neprodejné
<g/>
)	)	kIx)	)
Nebeskej	Nebeskej	k?	Nebeskej
kovboj	kovboj	k1gMnSc1	kovboj
(	(	kIx(	(
<g/>
box	box	k1gInSc1	box
18	[number]	k4	18
CD	CD	kA	CD
americké	americký	k2eAgInPc4d1	americký
nosiče	nosič	k1gInPc4	nosič
<g/>
)	)	kIx)	)
-	-	kIx~	-
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
box	box	k1gInSc4	box
Teče	teč	k1gFnSc2	teč
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
teče	téct	k5eAaImIp3nS	téct
Niagara	Niagara	k1gFnSc1	Niagara
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
i	i	k8xC	i
v	v	k7c6	v
ČR	ČR	kA	ČR
Merry	Merra	k1gMnSc2	Merra
Christmas	Christmas	k1gMnSc1	Christmas
-	-	kIx~	-
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
distribuováno	distribuovat	k5eAaBmNgNnS	distribuovat
i	i	k9	i
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
LP	LP	kA	LP
Kdyby	kdyby	k9	kdyby
tisíc	tisíc	k4xCgInPc2	tisíc
klarinetů	klarinet	k1gInPc2	klarinet
-	-	kIx~	-
1964	[number]	k4	1964
Limonádový	limonádový	k2eAgMnSc1d1	limonádový
Joe	Joe	k1gFnSc7	Joe
aneb	aneb	k?	aneb
koňská	koňský	k2eAgFnSc1d1	koňská
opera	opera	k1gFnSc1	opera
-	-	kIx~	-
1964	[number]	k4	1964
Fantom	fantom	k1gInSc1	fantom
Morrisvillu	Morrisvill	k1gInSc2	Morrisvill
-	-	kIx~	-
1966	[number]	k4	1966
Ta	ten	k3xDgFnSc1	ten
naše	náš	k3xOp1gFnSc1	náš
písnička	písnička	k1gFnSc1	písnička
česká	český	k2eAgFnSc1d1	Česká
-	-	kIx~	-
filmový	filmový	k2eAgInSc1d1	filmový
muzikál	muzikál	k1gInSc1	muzikál
<g/>
,	,	kIx,	,
1967	[number]	k4	1967
Píseň	píseň	k1gFnSc1	píseň
pro	pro	k7c4	pro
Rudolfa	Rudolf	k1gMnSc4	Rudolf
<g />
.	.	kIx.	.
</s>
<s>
III	III	kA	III
<g/>
.	.	kIx.	.
-	-	kIx~	-
1967	[number]	k4	1967
<g/>
́	́	k?	́
<g/>
-	-	kIx~	-
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
seriálový	seriálový	k2eAgInSc4d1	seriálový
muzikál	muzikál	k1gInSc4	muzikál
Všichni	všechen	k3xTgMnPc1	všechen
dobří	dobrý	k2eAgMnPc1d1	dobrý
rodáci	rodák	k1gMnPc1	rodák
-	-	kIx~	-
1968	[number]	k4	1968
Kam	kam	k6eAd1	kam
slunce	slunce	k1gNnSc4	slunce
nechodí	chodit	k5eNaImIp3nP	chodit
-	-	kIx~	-
1971	[number]	k4	1971
Noc	noc	k1gFnSc1	noc
na	na	k7c6	na
Karlštejně	Karlštejn	k1gInSc6	Karlštejn
-	-	kIx~	-
filmový	filmový	k2eAgInSc4d1	filmový
muzikál	muzikál	k1gInSc4	muzikál
<g/>
,	,	kIx,	,
1973	[number]	k4	1973
Trhák	trhák	k1gInSc1	trhák
-	-	kIx~	-
1980	[number]	k4	1980
Rozpaky	rozpak	k1gInPc7	rozpak
kuchaře	kuchař	k1gMnSc2	kuchař
Svatopluka	Svatopluk	k1gMnSc2	Svatopluk
-	-	kIx~	-
1984	[number]	k4	1984
Chalupáři	chalupář	k1gMnPc1	chalupář
-	-	kIx~	-
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
píseň	píseň	k1gFnSc1	píseň
Když	když	k8xS	když
máš	mít	k5eAaImIp2nS	mít
v	v	k7c6	v
<g />
.	.	kIx.	.
</s>
<s>
chalupě	chalupa	k1gFnSc3	chalupa
orchestrion	orchestrion	k1gInSc4	orchestrion
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
hitem	hit	k1gInSc7	hit
Doktor	doktor	k1gMnSc1	doktor
z	z	k7c2	z
Vejminku	Vejmink	k1gInSc2	Vejmink
-	-	kIx~	-
1982	[number]	k4	1982
<g/>
,	,	kIx,	,
píseň	píseň	k1gFnSc4	píseň
Když	když	k8xS	když
jdou	jít	k5eAaImIp3nP	jít
na	na	k7c4	na
mužskýho	mužskýho	k?	mužskýho
léta	léto	k1gNnSc2	léto
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
hitem	hit	k1gInSc7	hit
Všichni	všechen	k3xTgMnPc1	všechen
dobří	dobrý	k2eAgMnPc1d1	dobrý
rodáci	rodák	k1gMnPc1	rodák
-	-	kIx~	-
1968	[number]	k4	1968
Píseň	píseň	k1gFnSc1	píseň
pro	pro	k7c4	pro
Rudolfa	Rudolf	k1gMnSc4	Rudolf
III	III	kA	III
<g/>
.	.	kIx.	.
-	-	kIx~	-
1967	[number]	k4	1967
-	-	kIx~	-
1968	[number]	k4	1968
Noc	noc	k1gFnSc1	noc
na	na	k7c6	na
Karlštejně	Karlštejn	k1gInSc6	Karlštejn
-	-	kIx~	-
1973	[number]	k4	1973
<g/>
,	,	kIx,	,
píseň	píseň	k1gFnSc1	píseň
Do	do	k7c2	do
věží	věž	k1gFnPc2	věž
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
hitem	hit	k1gInSc7	hit
Waldemar	Waldemar	k1gMnSc1	Waldemar
<g />
.	.	kIx.	.
</s>
<s>
Matuška	matuška	k1gFnSc1	matuška
<g/>
,	,	kIx,	,
Olga	Olga	k1gFnSc1	Olga
Matušková	Matušková	k1gFnSc1	Matušková
<g/>
,	,	kIx,	,
Waldemar	Waldemar	k1gMnSc1	Waldemar
G.	G.	kA	G.
Matuška	Matuška	k1gMnSc1	Matuška
<g/>
:	:	kIx,	:
Tisíc	tisíc	k4xCgInSc4	tisíc
mil	míle	k1gFnPc2	míle
<g/>
,	,	kIx,	,
těch	ten	k3xDgFnPc2	ten
tisíc	tisíc	k4xCgInSc4	tisíc
mil	míle	k1gFnPc2	míle
<g/>
...	...	k?	...
<g/>
,	,	kIx,	,
De	De	k?	De
-	-	kIx~	-
Mar	Mar	k1gFnSc1	Mar
Enterprise	Enterprise	k1gFnSc1	Enterprise
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc6	vydání
Computer	computer	k1gInSc1	computer
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
<g/>
251	[number]	k4	251
<g/>
-	-	kIx~	-
<g/>
2664	[number]	k4	2664
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
autobiografie	autobiografie	k1gFnSc1	autobiografie
Waldemar	Waldemar	k1gMnSc1	Waldemar
Matuška	Matuška	k1gMnSc1	Matuška
<g/>
,	,	kIx,	,
Ivan	Ivan	k1gMnSc1	Ivan
Foustka	Foustka	k1gFnSc1	Foustka
<g/>
:	:	kIx,	:
Svět	svět	k1gInSc1	svět
má	mít	k5eAaImIp3nS	mít
24	[number]	k4	24
hodiny	hodina	k1gFnSc2	hodina
<g/>
,	,	kIx,	,
Olympia	Olympia	k1gFnSc1	Olympia
<g/>
:	:	kIx,	:
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
Waldemar	Waldemar	k1gMnSc1	Waldemar
Matuška	Matuška	k1gMnSc1	Matuška
<g/>
,	,	kIx,	,
Ivan	Ivan	k1gMnSc1	Ivan
Foustka	Foustka	k1gFnSc1	Foustka
<g/>
:	:	kIx,	:
To	ten	k3xDgNnSc4	ten
všecko	všecek	k3xTgNnSc4	všecek
vodnes	vodnes	k1gInSc4	vodnes
čas	čas	k1gInSc4	čas
<g/>
...	...	k?	...
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Olympia	Olympia	k1gFnSc1	Olympia
<g/>
:	:	kIx,	:
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1970	[number]	k4	1970
Miroslav	Miroslav	k1gMnSc1	Miroslav
Černý	Černý	k1gMnSc1	Černý
<g/>
:	:	kIx,	:
Waldemar	Waldemar	k1gMnSc1	Waldemar
Matuška	Matuška	k1gMnSc1	Matuška
1	[number]	k4	1
<g/>
,	,	kIx,	,
G	G	kA	G
<g/>
+	+	kIx~	+
<g/>
W	W	kA	W
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
zpěvník	zpěvník	k1gInSc1	zpěvník
<g/>
,	,	kIx,	,
písně	píseň	k1gFnPc1	píseň
z	z	k7c2	z
1960	[number]	k4	1960
-	-	kIx~	-
1985	[number]	k4	1985
<g/>
,	,	kIx,	,
edice	edice	k1gFnSc1	edice
Legendy	legenda	k1gFnSc2	legenda
české	český	k2eAgFnSc2d1	Česká
country	country	k2eAgFnSc2d1	country
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
Černý	Černý	k1gMnSc1	Černý
<g/>
:	:	kIx,	:
Waldemar	Waldemar	k1gMnSc1	Waldemar
Matuška	Matuška	k1gMnSc1	Matuška
2	[number]	k4	2
<g/>
,	,	kIx,	,
G	G	kA	G
<g/>
+	+	kIx~	+
<g/>
W	W	kA	W
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
zpěvník	zpěvník	k1gInSc1	zpěvník
<g/>
,	,	kIx,	,
písně	píseň	k1gFnPc1	píseň
z	z	k7c2	z
1960	[number]	k4	1960
-	-	kIx~	-
1985	[number]	k4	1985
<g/>
,	,	kIx,	,
edice	edice	k1gFnSc1	edice
Legendy	legenda	k1gFnSc2	legenda
české	český	k2eAgFnSc2d1	Česká
country	country	k2eAgFnSc2d1	country
<g/>
,	,	kIx,	,
<g/>
DOLEŽAL	Doležal	k1gMnSc1	Doležal
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Morální	morální	k2eAgInSc1d1	morální
pád	pád	k1gInSc1	pád
jednoho	jeden	k4xCgMnSc2	jeden
zpěváka	zpěvák	k1gMnSc2	zpěvák
<g/>
.	.	kIx.	.
</s>
<s>
Rudé	rudý	k2eAgNnSc1d1	Rudé
právo	právo	k1gNnSc1	právo
<g/>
.	.	kIx.	.
1986	[number]	k4	1986
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
66	[number]	k4	66
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
208	[number]	k4	208
<g/>
,	,	kIx,	,
s.	s.	k?	s.
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
27	[number]	k4	27
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
dobová	dobový	k2eAgFnSc1d1	dobová
oficiální	oficiální	k2eAgFnSc1d1	oficiální
reakce	reakce	k1gFnSc1	reakce
na	na	k7c4	na
Matuškovu	Matuškův	k2eAgFnSc4d1	Matuškova
emigraci	emigrace	k1gFnSc4	emigrace
Michal	Michal	k1gMnSc1	Michal
Herzán	Herzán	k2eAgMnSc1d1	Herzán
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Budinský	Budinský	k2eAgMnSc1d1	Budinský
<g/>
:	:	kIx,	:
Divadlo	divadlo	k1gNnSc1	divadlo
Rokoko	rokoko	k1gNnSc1	rokoko
<g/>
,	,	kIx,	,
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Victory	Victor	k1gMnPc4	Victor
<g/>
,	,	kIx,	,
Most	most	k1gInSc1	most
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
<g/>
,	,	kIx,	,
22	[number]	k4	22
<g/>
,	,	kIx,	,
36	[number]	k4	36
<g/>
-	-	kIx~	-
<g/>
41	[number]	k4	41
<g/>
,	,	kIx,	,
43	[number]	k4	43
<g/>
,	,	kIx,	,
45	[number]	k4	45
<g/>
,	,	kIx,	,
50	[number]	k4	50
<g/>
,	,	kIx,	,
52	[number]	k4	52
<g/>
,	,	kIx,	,
67	[number]	k4	67
<g/>
,	,	kIx,	,
73	[number]	k4	73
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2015	[number]	k4	2015
vydala	vydat	k5eAaPmAgFnS	vydat
Česká	český	k2eAgFnSc1d1	Česká
mincovna	mincovna	k1gFnSc1	mincovna
pamětní	pamětní	k2eAgFnSc4d1	pamětní
medaili	medaile	k1gFnSc4	medaile
s	s	k7c7	s
vyobrazením	vyobrazení	k1gNnSc7	vyobrazení
Waldemara	Waldemar	k1gMnSc2	Waldemar
Matušky	Matuška	k1gMnSc2	Matuška
v	v	k7c6	v
sérii	série	k1gFnSc6	série
Slavíci	Slavík	k1gMnPc1	Slavík
ve	v	k7c6	v
zlatě	zlato	k1gNnSc6	zlato
<g/>
.	.	kIx.	.
</s>
<s>
Medaili	medaile	k1gFnSc4	medaile
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
MgA.	MgA.	k1gMnSc4	MgA.
Martina	Martin	k1gMnSc4	Martin
Daška	Dašek	k1gMnSc4	Dašek
<g/>
.	.	kIx.	.
</s>
<s>
Averz	Averz	k1gInSc1	Averz
medaile	medaile	k1gFnSc2	medaile
nese	nést	k5eAaImIp3nS	nést
vyobrazení	vyobrazení	k1gNnSc4	vyobrazení
obličeje	obličej	k1gInSc2	obličej
Waldemara	Waldemar	k1gMnSc2	Waldemar
Matušky	Matuška	k1gMnSc2	Matuška
<g/>
.	.	kIx.	.
</s>
<s>
Reverz	reverz	k1gInSc1	reverz
poté	poté	k6eAd1	poté
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
notový	notový	k2eAgInSc4d1	notový
zápis	zápis	k1gInSc4	zápis
písně	píseň	k1gFnSc2	píseň
To	to	k9	to
všechno	všechen	k3xTgNnSc4	všechen
odnes	odnést	k5eAaPmRp2nS	odnést
čas	čas	k1gInSc4	čas
<g/>
,	,	kIx,	,
banjo	banjo	k1gNnSc4	banjo
<g/>
,	,	kIx,	,
podpis	podpis	k1gInSc4	podpis
Waldemara	Waldemar	k1gMnSc2	Waldemar
Matušky	Matuška	k1gMnSc2	Matuška
a	a	k8xC	a
nápis	nápis	k1gInSc1	nápis
"	"	kIx"	"
<g/>
Zlatý	zlatý	k2eAgInSc1d1	zlatý
slavík	slavík	k1gInSc1	slavík
1962	[number]	k4	1962
&	&	k?	&
1967	[number]	k4	1967
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Medaile	medaile	k1gFnSc1	medaile
je	být	k5eAaImIp3nS	být
vyrobena	vyroben	k2eAgFnSc1d1	vyrobena
ve	v	k7c6	v
třech	tři	k4xCgNnPc6	tři
vyhotoveních	vyhotovení	k1gNnPc6	vyhotovení
-	-	kIx~	-
zlatá	zlatá	k1gFnSc1	zlatá
uncová	uncový	k2eAgFnSc1d1	uncová
a	a	k8xC	a
půluncová	půluncový	k2eAgFnSc1d1	půluncová
a	a	k8xC	a
stříbrná	stříbrná	k1gFnSc1	stříbrná
uncová	uncový	k2eAgFnSc1d1	uncová
<g/>
.	.	kIx.	.
</s>
