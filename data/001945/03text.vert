<s>
Manhattan	Manhattan	k1gInSc1	Manhattan
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
pěti	pět	k4xCc2	pět
newyorských	newyorský	k2eAgInPc2d1	newyorský
městských	městský	k2eAgInPc2d1	městský
obvodů	obvod	k1gInPc2	obvod
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
:	:	kIx,	:
boroughs	boroughs	k6eAd1	boroughs
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
s	s	k7c7	s
Brooklynem	Brooklyn	k1gInSc7	Brooklyn
<g/>
,	,	kIx,	,
Bronxem	Bronx	k1gInSc7	Bronx
<g/>
,	,	kIx,	,
Queensem	Queens	k1gInSc7	Queens
a	a	k8xC	a
Staten	Staten	k2eAgInSc1d1	Staten
Islandem	Island	k1gInSc7	Island
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
územně	územně	k6eAd1	územně
shodný	shodný	k2eAgInSc1d1	shodný
s	s	k7c7	s
okresem	okres	k1gInSc7	okres
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
County	Counta	k1gFnSc2	Counta
<g/>
)	)	kIx)	)
amerického	americký	k2eAgInSc2d1	americký
spolkového	spolkový	k2eAgInSc2d1	spolkový
státu	stát	k1gInSc2	stát
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Manhattan	Manhattan	k1gInSc1	Manhattan
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
dnešního	dnešní	k2eAgInSc2d1	dnešní
Manhattanu	Manhattan	k1gInSc2	Manhattan
navštívil	navštívit	k5eAaPmAgMnS	navštívit
anglický	anglický	k2eAgMnSc1d1	anglický
cestovatel	cestovatel	k1gMnSc1	cestovatel
Henry	Henry	k1gMnSc1	Henry
Hudson	Hudson	k1gMnSc1	Hudson
dne	den	k1gInSc2	den
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1609	[number]	k4	1609
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
své	svůj	k3xOyFgFnSc2	svůj
výpravy	výprava	k1gFnSc2	výprava
za	za	k7c4	za
Východoindickou	východoindický	k2eAgFnSc4d1	Východoindická
obchodní	obchodní	k2eAgFnSc4d1	obchodní
společnost	společnost	k1gFnSc4	společnost
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
nepřímo	přímo	k6eNd1	přímo
započal	započnout	k5eAaPmAgInS	započnout
migraci	migrace	k1gFnSc4	migrace
přistěhovalců	přistěhovalec	k1gMnPc2	přistěhovalec
z	z	k7c2	z
Dánska	Dánsko	k1gNnSc2	Dánsko
(	(	kIx(	(
<g/>
1620	[number]	k4	1620
<g/>
)	)	kIx)	)
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
dalších	další	k2eAgFnPc6d1	další
evropských	evropský	k2eAgFnPc6d1	Evropská
zemích	zem	k1gFnPc6	zem
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
začil	začit	k5eAaBmAgInS	začit
Manhattan	Manhattan	k1gInSc1	Manhattan
příliv	příliv	k1gInSc1	příliv
přistěhovalců	přistěhovalec	k1gMnPc2	přistěhovalec
z	z	k7c2	z
Irska	Irsko	k1gNnSc2	Irsko
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc4	jejichž
první	první	k4xOgNnSc4	první
setkání	setkání	k1gNnSc4	setkání
s	s	k7c7	s
Novým	nový	k2eAgInSc7d1	nový
světem	svět	k1gInSc7	svět
začínal	začínat	k5eAaImAgInS	začínat
právě	právě	k9	právě
zde	zde	k6eAd1	zde
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
představuje	představovat	k5eAaImIp3nS	představovat
Manhattan	Manhattan	k1gInSc1	Manhattan
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
i	i	k8xC	i
administrativní	administrativní	k2eAgNnSc4d1	administrativní
centrum	centrum	k1gNnSc4	centrum
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
a	a	k8xC	a
potažmo	potažmo	k6eAd1	potažmo
ekonomické	ekonomický	k2eAgNnSc4d1	ekonomické
a	a	k8xC	a
bankovní	bankovní	k2eAgNnSc4d1	bankovní
<g/>
/	/	kIx~	/
<g/>
finanční	finanční	k2eAgNnSc4d1	finanční
centrum	centrum	k1gNnSc4	centrum
celých	celý	k2eAgInPc2d1	celý
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
a	a	k8xC	a
v	v	k7c6	v
jistém	jistý	k2eAgInSc6d1	jistý
smyslu	smysl	k1gInSc6	smysl
i	i	k9	i
ikonický	ikonický	k2eAgInSc4d1	ikonický
symbol	symbol	k1gInSc4	symbol
západního	západní	k2eAgInSc2d1	západní
kapitalismu	kapitalismus	k1gInSc2	kapitalismus
<g/>
.	.	kIx.	.
</s>
<s>
Sídlí	sídlet	k5eAaImIp3nP	sídlet
zde	zde	k6eAd1	zde
Newyorské	newyorský	k2eAgFnPc1d1	newyorská
burzy	burza	k1gFnPc1	burza
cenných	cenný	k2eAgInPc2d1	cenný
papírů	papír	k1gInPc2	papír
<g/>
,	,	kIx,	,
zlata	zlato	k1gNnSc2	zlato
<g/>
,	,	kIx,	,
různých	různý	k2eAgFnPc2d1	různá
komodit	komodita	k1gFnPc2	komodita
<g/>
,	,	kIx,	,
pobočka	pobočka	k1gFnSc1	pobočka
Fedu	Feda	k1gFnSc4	Feda
a	a	k8xC	a
mnoho	mnoho	k4c4	mnoho
komerčních	komerční	k2eAgFnPc2d1	komerční
bank	banka	k1gFnPc2	banka
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
je	být	k5eAaImIp3nS	být
plná	plný	k2eAgFnSc1d1	plná
výškových	výškový	k2eAgFnPc2d1	výšková
budov	budova	k1gFnPc2	budova
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
stavět	stavět	k5eAaImF	stavět
již	již	k6eAd1	již
po	po	k7c6	po
roku	rok	k1gInSc6	rok
1920	[number]	k4	1920
a	a	k8xC	a
některé	některý	k3yIgInPc1	některý
mrakodrapy	mrakodrap	k1gInPc1	mrakodrap
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
době	doba	k1gFnSc6	doba
svého	svůj	k3xOyFgNnSc2	svůj
dokončení	dokončení	k1gNnSc6	dokončení
nejvyššími	vysoký	k2eAgFnPc7d3	nejvyšší
stavbami	stavba	k1gFnPc7	stavba
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Manhattan	Manhattan	k1gInSc1	Manhattan
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
59,5	[number]	k4	59,5
km2	km2	k4	km2
(	(	kIx(	(
<g/>
pevnina	pevnina	k1gFnSc1	pevnina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
87,5	[number]	k4	87,5
km2	km2	k4	km2
(	(	kIx(	(
<g/>
vč.	vč.	k?	vč.
vodstva	vodstvo	k1gNnSc2	vodstvo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nejhustěji	husto	k6eAd3	husto
obydlených	obydlený	k2eAgInPc2d1	obydlený
krajů	kraj	k1gInPc2	kraj
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
celé	celá	k1gFnSc6	celá
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
nejlidnatějším	lidnatý	k2eAgInSc7d3	nejlidnatější
městským	městský	k2eAgInSc7d1	městský
obvodem	obvod	k1gInSc7	obvod
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
ho	on	k3xPp3gMnSc4	on
co	co	k9	co
do	do	k7c2	do
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
předstihly	předstihnout	k5eAaPmAgFnP	předstihnout
čtvrtě	čtvrt	k1gFnPc1	čtvrt
Brooklyn	Brooklyn	k1gInSc1	Brooklyn
a	a	k8xC	a
Queens	Queens	k1gInSc1	Queens
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
mají	mít	k5eAaImIp3nP	mít
každá	každý	k3xTgFnSc1	každý
přes	přes	k7c4	přes
2,5	[number]	k4	2,5
milionu	milion	k4xCgInSc2	milion
<g/>
,	,	kIx,	,
Manhattan	Manhattan	k1gInSc1	Manhattan
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
třetí	třetí	k4xOgMnSc1	třetí
s	s	k7c7	s
1	[number]	k4	1
619	[number]	k4	619
090	[number]	k4	090
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
červenci	červenec	k1gInSc3	červenec
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
nejmenším	malý	k2eAgMnSc6d3	nejmenší
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
pěti	pět	k4xCc2	pět
obvodů	obvod	k1gInPc2	obvod
co	co	k8xS	co
do	do	k7c2	do
výměry	výměra	k1gFnSc2	výměra
<g/>
.	.	kIx.	.
</s>
<s>
Ročně	ročně	k6eAd1	ročně
ho	on	k3xPp3gMnSc4	on
navštíví	navštívit	k5eAaPmIp3nS	navštívit
přibližně	přibližně	k6eAd1	přibližně
50	[number]	k4	50
milionů	milion	k4xCgInPc2	milion
turistů	turist	k1gMnPc2	turist
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jej	on	k3xPp3gMnSc4	on
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c4	mezi
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejoblíbenějších	oblíbený	k2eAgFnPc2d3	nejoblíbenější
městských	městský	k2eAgFnPc2d1	městská
čtvrtí	čtvrt	k1gFnPc2	čtvrt
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejžádanější	žádaný	k2eAgFnPc4d3	nejžádanější
lokace	lokace	k1gFnPc4	lokace
pro	pro	k7c4	pro
kancelářské	kancelářský	k2eAgFnPc4d1	kancelářská
a	a	k8xC	a
obytné	obytný	k2eAgFnPc4d1	obytná
prostory	prostora	k1gFnPc4	prostora
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
s	s	k7c7	s
obrovskými	obrovský	k2eAgFnPc7d1	obrovská
cenami	cena	k1gFnPc7	cena
za	za	k7c4	za
pronájem	pronájem	k1gInSc4	pronájem
či	či	k8xC	či
odkoupení	odkoupení	k1gNnSc4	odkoupení
<g/>
.	.	kIx.	.
</s>
<s>
Mít	mít	k5eAaImF	mít
pobočku	pobočka	k1gFnSc4	pobočka
na	na	k7c6	na
některé	některý	k3yIgFnSc6	některý
z	z	k7c2	z
prominentních	prominentní	k2eAgFnPc2d1	prominentní
ulic	ulice	k1gFnPc2	ulice
Manhattanu	Manhattan	k1gInSc2	Manhattan
znamená	znamenat	k5eAaImIp3nS	znamenat
pro	pro	k7c4	pro
každou	každý	k3xTgFnSc4	každý
firmu	firma	k1gFnSc4	firma
punc	punc	k1gInSc4	punc
prestiže	prestiž	k1gFnSc2	prestiž
a	a	k8xC	a
světovosti	světovost	k1gFnSc2	světovost
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
bankovní	bankovní	k2eAgInSc4d1	bankovní
a	a	k8xC	a
finanční	finanční	k2eAgInSc4d1	finanční
sektor	sektor	k1gInSc4	sektor
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgNnPc2d1	další
odvětví	odvětví	k1gNnPc2	odvětví
je	být	k5eAaImIp3nS	být
též	též	k9	též
jedním	jeden	k4xCgNnSc7	jeden
ze	z	k7c2	z
světových	světový	k2eAgNnPc2d1	světové
center	centrum	k1gNnPc2	centrum
módy	móda	k1gFnSc2	móda
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgFnPc4	tři
části	část	k1gFnPc4	část
<g/>
:	:	kIx,	:
Uptown	Uptown	k1gMnSc1	Uptown
<g/>
,	,	kIx,	,
Midtown	Midtown	k1gMnSc1	Midtown
a	a	k8xC	a
Downtown	Downtown	k1gMnSc1	Downtown
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
do	do	k7c2	do
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2001	[number]	k4	2001
zde	zde	k6eAd1	zde
stála	stát	k5eAaImAgFnS	stát
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
dvojčata	dvojče	k1gNnPc1	dvojče
<g/>
"	"	kIx"	"
Světového	světový	k2eAgNnSc2d1	světové
obchodního	obchodní	k2eAgNnSc2d1	obchodní
centra	centrum	k1gNnSc2	centrum
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
však	však	k9	však
byla	být	k5eAaImAgFnS	být
zničena	zničit	k5eAaPmNgFnS	zničit
teroristickými	teroristický	k2eAgInPc7d1	teroristický
útoky	útok	k1gInPc7	útok
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
byl	být	k5eAaImAgInS	být
více	hodně	k6eAd2	hodně
než	než	k8xS	než
10	[number]	k4	10
let	léto	k1gNnPc2	léto
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
mrakodrapem	mrakodrap	k1gInSc7	mrakodrap
ve	v	k7c6	v
městě	město	k1gNnSc6	město
znovu	znovu	k6eAd1	znovu
Empire	empir	k1gInSc5	empir
State	status	k1gInSc5	status
Building	Building	k1gInSc1	Building
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
však	však	k9	však
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
překonalo	překonat	k5eAaPmAgNnS	překonat
dokončení	dokončení	k1gNnSc1	dokončení
stavby	stavba	k1gFnSc2	stavba
One	One	k1gFnPc2	One
World	World	k1gMnSc1	World
Trade	Trad	k1gInSc5	Trad
Center	centrum	k1gNnPc2	centrum
<g/>
,	,	kIx,	,
nového	nový	k2eAgNnSc2d1	nové
obchodního	obchodní	k2eAgNnSc2d1	obchodní
centra	centrum	k1gNnSc2	centrum
postaveného	postavený	k2eAgInSc2d1	postavený
místo	místo	k7c2	místo
původních	původní	k2eAgNnPc2d1	původní
"	"	kIx"	"
<g/>
dvojčat	dvojče	k1gNnPc2	dvojče
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
je	být	k5eAaImIp3nS	být
aktuálně	aktuálně	k6eAd1	aktuálně
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
budovou	budova	k1gFnSc7	budova
západní	západní	k2eAgFnSc2d1	západní
polokoule	polokoule	k1gFnSc2	polokoule
<g/>
.	.	kIx.	.
</s>
<s>
Drtivá	drtivý	k2eAgFnSc1d1	drtivá
většina	většina	k1gFnSc1	většina
nejoblíbenějších	oblíbený	k2eAgFnPc2d3	nejoblíbenější
památek	památka	k1gFnPc2	památka
a	a	k8xC	a
atrakcí	atrakce	k1gFnPc2	atrakce
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
je	být	k5eAaImIp3nS	být
situována	situovat	k5eAaBmNgFnS	situovat
právě	právě	k9	právě
na	na	k7c6	na
Manhattanu	Manhattan	k1gInSc6	Manhattan
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgNnPc4d3	nejvýznamnější
místa	místo	k1gNnPc4	místo
ostrova	ostrov	k1gInSc2	ostrov
patří	patřit	k5eAaImIp3nS	patřit
zmíněné	zmíněný	k2eAgFnPc4d1	zmíněná
One	One	k1gFnPc4	One
World	Worlda	k1gFnPc2	Worlda
Trade	Trad	k1gInSc5	Trad
Center	centrum	k1gNnPc2	centrum
<g/>
,	,	kIx,	,
památník	památník	k1gInSc1	památník
Ground	Ground	k1gMnSc1	Ground
Zero	Zero	k1gMnSc1	Zero
<g/>
,	,	kIx,	,
Empire	empir	k1gInSc5	empir
State	status	k1gInSc5	status
Building	Building	k1gInSc4	Building
<g/>
,	,	kIx,	,
Rockefeller	Rockefeller	k1gInSc4	Rockefeller
Center	centrum	k1gNnPc2	centrum
<g/>
,	,	kIx,	,
Broadway	Broadwaa	k1gFnPc1	Broadwaa
<g/>
,	,	kIx,	,
Central	Central	k1gFnPc1	Central
Park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
známá	známý	k2eAgFnSc1d1	známá
obchodní	obchodní	k2eAgFnSc1d1	obchodní
ulice	ulice	k1gFnSc1	ulice
Wall	Wall	k1gMnSc1	Wall
Street	Street	k1gMnSc1	Street
<g/>
,	,	kIx,	,
Chinatown	Chinatown	k1gMnSc1	Chinatown
<g/>
,	,	kIx,	,
Times	Times	k1gMnSc1	Times
Square	square	k1gInSc1	square
<g/>
,	,	kIx,	,
nákupní	nákupní	k2eAgFnPc1d1	nákupní
ulice	ulice	k1gFnPc1	ulice
5	[number]	k4	5
<g/>
th	th	k?	th
Avenue	avenue	k1gFnSc1	avenue
<g/>
,	,	kIx,	,
Chrysler	Chrysler	k1gInSc1	Chrysler
Building	Building	k1gInSc1	Building
<g/>
,	,	kIx,	,
Flatiron	Flatiron	k1gInSc1	Flatiron
Building	Building	k1gInSc1	Building
nebo	nebo	k8xC	nebo
Grand	grand	k1gMnSc1	grand
Central	Central	k1gMnSc1	Central
Terminal	Terminal	k1gMnSc1	Terminal
<g/>
.	.	kIx.	.
</s>
<s>
Nejnavštěvovanější	navštěvovaný	k2eAgFnSc7d3	nejnavštěvovanější
památkou	památka	k1gFnSc7	památka
je	být	k5eAaImIp3nS	být
Socha	socha	k1gFnSc1	socha
Svobody	svoboda	k1gFnSc2	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
73	[number]	k4	73
<g/>
.	.	kIx.	.
ulici	ulice	k1gFnSc6	ulice
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
konzulát	konzulát	k1gInSc1	konzulát
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Základem	základ	k1gInSc7	základ
Manhattanu	Manhattan	k1gInSc2	Manhattan
jsou	být	k5eAaImIp3nP	být
slída	slída	k1gFnSc1	slída
a	a	k8xC	a
břidlice	břidlice	k1gFnSc1	břidlice
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
silné	silný	k2eAgFnPc4d1	silná
a	a	k8xC	a
pevné	pevný	k2eAgFnPc4d1	pevná
horniny	hornina	k1gFnPc4	hornina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
vhodným	vhodný	k2eAgInSc7d1	vhodný
základem	základ	k1gInSc7	základ
pro	pro	k7c4	pro
výškové	výškový	k2eAgFnPc4d1	výšková
budovy	budova	k1gFnPc4	budova
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
mrakodrapů	mrakodrap	k1gInPc2	mrakodrap
je	být	k5eAaImIp3nS	být
situováno	situovat	k5eAaBmNgNnS	situovat
v	v	k7c6	v
Downtownu	Downtown	k1gInSc6	Downtown
a	a	k8xC	a
Midtownu	Midtown	k1gInSc6	Midtown
<g/>
.	.	kIx.	.
</s>
<s>
Manhattan	Manhattan	k1gInSc1	Manhattan
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
Downtown	Downtown	k1gInSc4	Downtown
(	(	kIx(	(
<g/>
Dolní	dolní	k2eAgFnSc1d1	dolní
Manhattan	Manhattan	k1gInSc1	Manhattan
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgInSc1d1	jižní
Manhattan	Manhattan	k1gInSc1	Manhattan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Midtown	Midtown	k1gNnSc1	Midtown
a	a	k8xC	a
Uptown	Uptown	k1gNnSc1	Uptown
(	(	kIx(	(
<g/>
Horní	horní	k2eAgInSc1d1	horní
Manhattan	Manhattan	k1gInSc1	Manhattan
<g/>
,	,	kIx,	,
severní	severní	k2eAgInSc1d1	severní
Manhattan	Manhattan	k1gInSc1	Manhattan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
th	th	k?	th
Avenue	avenue	k1gFnPc2	avenue
zase	zase	k9	zase
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
západní	západní	k2eAgFnSc4d1	západní
a	a	k8xC	a
východní	východní	k2eAgFnSc4d1	východní
stranu	strana	k1gFnSc4	strana
Manhattanu	Manhattan	k1gInSc2	Manhattan
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
západě	západ	k1gInSc6	západ
je	být	k5eAaImIp3nS	být
Manhattan	Manhattan	k1gInSc1	Manhattan
ohraničen	ohraničen	k2eAgInSc1d1	ohraničen
řekou	řeka	k1gFnSc7	řeka
Hudson	Hudsona	k1gFnPc2	Hudsona
<g/>
,	,	kIx,	,
na	na	k7c6	na
východě	východ	k1gInSc6	východ
řekou	řeka	k1gFnSc7	řeka
East	East	k2eAgInSc4d1	East
River	River	k1gInSc4	River
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
řeka	řeka	k1gFnSc1	řeka
Harlem	Harl	k1gInSc7	Harl
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
Manhattan	Manhattan	k1gInSc1	Manhattan
a	a	k8xC	a
Bronx	Bronx	k1gInSc1	Bronx
<g/>
,	,	kIx,	,
další	další	k2eAgFnSc1d1	další
část	část	k1gFnSc1	část
města	město	k1gNnSc2	město
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
Manhattanu	Manhattan	k1gInSc3	Manhattan
patří	patřit	k5eAaImIp3nS	patřit
také	také	k9	také
několik	několik	k4yIc4	několik
ostrovů	ostrov	k1gInPc2	ostrov
jako	jako	k8xC	jako
např.	např.	kA	např.
Roosevelt	Roosevelt	k1gMnSc1	Roosevelt
Island	Island	k1gInSc1	Island
<g/>
,	,	kIx,	,
Liberty	Libert	k1gInPc1	Libert
Island	Island	k1gInSc1	Island
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Manhattan	Manhattan	k1gInSc1	Manhattan
má	mít	k5eAaImIp3nS	mít
pevninskou	pevninský	k2eAgFnSc4d1	pevninská
rozlohu	rozloha	k1gFnSc4	rozloha
59	[number]	k4	59
km	km	kA	km
<g/>
2	[number]	k4	2
a	a	k8xC	a
87.5	[number]	k4	87.5
km	km	kA	km
<g/>
2	[number]	k4	2
včetně	včetně	k7c2	včetně
vodstva	vodstvo	k1gNnSc2	vodstvo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
délku	délka	k1gFnSc4	délka
měří	měřit	k5eAaImIp3nS	měřit
21,6	[number]	k4	21,6
km	km	kA	km
<g/>
,	,	kIx,	,
na	na	k7c4	na
šířku	šířka	k1gFnSc4	šířka
3,7	[number]	k4	3,7
km	km	kA	km
(	(	kIx(	(
<g/>
v	v	k7c6	v
nejširším	široký	k2eAgNnSc6d3	nejširší
místě	místo	k1gNnSc6	místo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
budování	budování	k1gNnSc6	budování
původního	původní	k2eAgInSc2d1	původní
World	World	k1gInSc1	World
Trade	Trad	k1gInSc5	Trad
Center	centrum	k1gNnPc2	centrum
bylo	být	k5eAaImAgNnS	být
vyhloubeno	vyhlouben	k2eAgNnSc1d1	vyhloubeno
obrovské	obrovský	k2eAgNnSc1d1	obrovské
množství	množství	k1gNnSc1	množství
hlíny	hlína	k1gFnSc2	hlína
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
později	pozdě	k6eAd2	pozdě
použito	použít	k5eAaPmNgNnS	použít
pro	pro	k7c4	pro
rozšíření	rozšíření	k1gNnSc4	rozšíření
manhattanského	manhattanský	k2eAgNnSc2d1	manhattanský
pobřeží	pobřeží	k1gNnSc2	pobřeží
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
Battery	Batter	k1gInPc1	Batter
Park	park	k1gInSc1	park
City	city	k1gNnSc1	city
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
bylo	být	k5eAaImAgNnS	být
více	hodně	k6eAd2	hodně
než	než	k8xS	než
92	[number]	k4	92
akrů	akr	k1gInPc2	akr
pobřeží	pobřeží	k1gNnSc2	pobřeží
a	a	k8xC	a
přes	přes	k7c4	přes
30	[number]	k4	30
akrů	akr	k1gInPc2	akr
parků	park	k1gInPc2	park
<g/>
.	.	kIx.	.
</s>
<s>
Manhattan	Manhattan	k1gInSc1	Manhattan
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
dálniční	dálniční	k2eAgNnSc1d1	dálniční
spojení	spojení	k1gNnSc1	spojení
se	s	k7c7	s
sousedním	sousední	k2eAgInSc7d1	sousední
státem	stát	k1gInSc7	stát
New	New	k1gMnSc2	New
Jersey	Jersea	k1gMnSc2	Jersea
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
mostu	most	k1gInSc2	most
George	George	k1gFnPc2	George
Washington	Washington	k1gInSc1	Washington
Bridge	Bridg	k1gInSc2	Bridg
a	a	k8xC	a
dvou	dva	k4xCgInPc2	dva
tunelů	tunel	k1gInPc2	tunel
-	-	kIx~	-
Lincoln	Lincoln	k1gMnSc1	Lincoln
Tunnel	Tunnel	k1gMnSc1	Tunnel
a	a	k8xC	a
Holland	Holland	k1gInSc1	Holland
Tunnel	Tunnela	k1gFnPc2	Tunnela
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgFnPc1	tři
hlavní	hlavní	k2eAgFnPc1d1	hlavní
části	část	k1gFnPc1	část
Manhattanu	Manhattan	k1gInSc2	Manhattan
-	-	kIx~	-
Downtown	Downtown	k1gMnSc1	Downtown
<g/>
,	,	kIx,	,
Midtown	Midtown	k1gMnSc1	Midtown
a	a	k8xC	a
Uptown	Uptown	k1gMnSc1	Uptown
-	-	kIx~	-
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
následujících	následující	k2eAgFnPc2d1	následující
22	[number]	k4	22
menších	malý	k2eAgFnPc2d2	menší
částí	část	k1gFnPc2	část
<g/>
:	:	kIx,	:
Financial	Financial	k1gMnSc1	Financial
District	District	k1gMnSc1	District
<g/>
,	,	kIx,	,
Tribeca	Tribeca	k1gMnSc1	Tribeca
<g/>
,	,	kIx,	,
Chinatown	Chinatown	k1gMnSc1	Chinatown
<g/>
,	,	kIx,	,
Lower	Lower	k1gMnSc1	Lower
East	Easta	k1gFnPc2	Easta
Side	Sid	k1gFnSc2	Sid
<g/>
,	,	kIx,	,
Little	Little	k1gFnSc2	Little
Italy	Ital	k1gMnPc4	Ital
<g/>
,	,	kIx,	,
Soho	Soho	k1gNnSc4	Soho
<g/>
,	,	kIx,	,
Greenwich	Greenwich	k1gInSc4	Greenwich
Village	Villag	k1gFnSc2	Villag
<g/>
,	,	kIx,	,
East	East	k1gMnSc1	East
Village	Villag	k1gFnSc2	Villag
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Stuyvesant	Stuyvesant	k1gMnSc1	Stuyvesant
Town	Town	k1gMnSc1	Town
<g/>
,	,	kIx,	,
Gramercy	Gramercy	k1gInPc1	Gramercy
<g/>
,	,	kIx,	,
Chelsea	Chelseum	k1gNnPc1	Chelseum
<g/>
,	,	kIx,	,
Murray	Murraa	k1gFnPc1	Murraa
Hill	Hill	k1gMnSc1	Hill
<g/>
,	,	kIx,	,
Garment	Garment	k1gMnSc1	Garment
District	District	k1gMnSc1	District
<g/>
,	,	kIx,	,
Times	Times	k1gMnSc1	Times
Square	square	k1gInSc1	square
<g/>
,	,	kIx,	,
Midtown	Midtown	k1gMnSc1	Midtown
West	West	k1gMnSc1	West
<g/>
,	,	kIx,	,
Midtown	Midtown	k1gMnSc1	Midtown
East	East	k1gMnSc1	East
<g/>
,	,	kIx,	,
Upper	Upper	k1gMnSc1	Upper
West	West	k2eAgInSc4d1	West
Side	Side	k1gInSc4	Side
<g/>
,	,	kIx,	,
Upper	Upper	k1gInSc4	Upper
East	Easta	k1gFnPc2	Easta
Side	Side	k1gNnSc1	Side
<g/>
,	,	kIx,	,
Central	Central	k1gFnSc1	Central
Park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
East	East	k1gInSc1	East
Harlem	Harlo	k1gNnSc7	Harlo
<g/>
,	,	kIx,	,
Harlem	Harlo	k1gNnSc7	Harlo
a	a	k8xC	a
Morningside	Morningsid	k1gInSc5	Morningsid
Heights	Heightsa	k1gFnPc2	Heightsa
<g/>
.	.	kIx.	.
</s>
<s>
Orientace	orientace	k1gFnSc1	orientace
na	na	k7c6	na
Manhattanu	Manhattan	k1gInSc6	Manhattan
je	být	k5eAaImIp3nS	být
relativně	relativně	k6eAd1	relativně
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
<g/>
.	.	kIx.	.
</s>
<s>
Podél	podél	k7c2	podél
ostrova	ostrov	k1gInSc2	ostrov
se	se	k3xPyFc4	se
svisle	svisle	k6eAd1	svisle
táhnou	táhnout	k5eAaImIp3nP	táhnout
"	"	kIx"	"
<g/>
Avenues	Avenues	k1gInSc1	Avenues
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
ulice	ulice	k1gFnPc1	ulice
<g/>
,	,	kIx,	,
kterých	který	k3yQgFnPc2	který
je	být	k5eAaImIp3nS	být
celkem	celkem	k6eAd1	celkem
12	[number]	k4	12
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
značeny	značen	k2eAgFnPc1d1	značena
čísly	číslo	k1gNnPc7	číslo
(	(	kIx(	(
<g/>
od	od	k7c2	od
východu	východ	k1gInSc2	východ
1	[number]	k4	1
<g/>
st	st	kA	st
-	-	kIx~	-
12	[number]	k4	12
<g/>
th	th	k?	th
Avenue	avenue	k1gFnPc2	avenue
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
Avenues	Avenuesa	k1gFnPc2	Avenuesa
mají	mít	k5eAaImIp3nP	mít
také	také	k9	také
svůj	svůj	k3xOyFgInSc4	svůj
název	název	k1gInSc4	název
(	(	kIx(	(
<g/>
Madison	Madison	k1gInSc4	Madison
Avenue	avenue	k1gFnSc4	avenue
ad	ad	k7c4	ad
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
obecně	obecně	k6eAd1	obecně
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
vždy	vždy	k6eAd1	vždy
jednoduše	jednoduše	k6eAd1	jednoduše
orientovat	orientovat	k5eAaBmF	orientovat
podle	podle	k7c2	podle
čísel	číslo	k1gNnPc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Vodorovně	vodorovně	k6eAd1	vodorovně
jsou	být	k5eAaImIp3nP	být
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
Manhattanu	Manhattan	k1gInSc6	Manhattan
rozmístěny	rozmístěn	k2eAgFnPc1d1	rozmístěna
"	"	kIx"	"
<g/>
Streets	Streets	k1gInSc1	Streets
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
(	(	kIx(	(
<g/>
až	až	k9	až
na	na	k7c4	na
pár	pár	k4xCyI	pár
výjimek	výjimka	k1gFnPc2	výjimka
především	především	k6eAd1	především
v	v	k7c6	v
dolním	dolní	k2eAgInSc6d1	dolní
Manhattanu	Manhattan	k1gInSc6	Manhattan
<g/>
)	)	kIx)	)
značeny	značen	k2eAgFnPc1d1	značena
také	také	k6eAd1	také
čísly	čísnout	k5eAaPmAgFnP	čísnout
a	a	k8xC	a
rozděleny	rozdělit	k5eAaPmNgInP	rozdělit
na	na	k7c4	na
West	West	k1gInSc4	West
(	(	kIx(	(
<g/>
západní	západní	k2eAgInPc4d1	západní
<g/>
)	)	kIx)	)
a	a	k8xC	a
East	East	k1gInSc1	East
(	(	kIx(	(
<g/>
východní	východní	k2eAgFnSc6d1	východní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obvyklá	obvyklý	k2eAgFnSc1d1	obvyklá
orientace	orientace	k1gFnSc1	orientace
na	na	k7c6	na
Manhattanu	Manhattan	k1gInSc6	Manhattan
tedy	tedy	k9	tedy
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
prakticky	prakticky	k6eAd1	prakticky
každé	každý	k3xTgNnSc4	každý
místo	místo	k1gNnSc4	místo
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
lze	lze	k6eAd1	lze
lokalizovat	lokalizovat	k5eAaBmF	lokalizovat
na	na	k7c4	na
roh	roh	k1gInSc4	roh
mezi	mezi	k7c7	mezi
některou	některý	k3yIgFnSc7	některý
Avenue	avenue	k1gFnSc7	avenue
a	a	k8xC	a
některou	některý	k3yIgFnSc4	některý
Street	Street	k1gMnSc1	Street
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
i	i	k8xC	i
přes	přes	k7c4	přes
zdánlivý	zdánlivý	k2eAgInSc4d1	zdánlivý
zmatek	zmatek	k1gInSc4	zmatek
mezi	mezi	k7c7	mezi
vysokými	vysoký	k2eAgFnPc7d1	vysoká
budovami	budova	k1gFnPc7	budova
a	a	k8xC	a
tisíci	tisíc	k4xCgInPc7	tisíc
lidmi	člověk	k1gMnPc7	člověk
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
snadné	snadný	k2eAgNnSc1d1	snadné
se	se	k3xPyFc4	se
na	na	k7c6	na
Manhattanu	Manhattan	k1gInSc6	Manhattan
zorientovat	zorientovat	k5eAaPmF	zorientovat
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc7d1	poslední
<g/>
,	,	kIx,	,
nejsevernější	severní	k2eAgFnSc7d3	nejsevernější
ulicí	ulice	k1gFnSc7	ulice
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
220	[number]	k4	220
<g/>
th	th	k?	th
Street	Street	k1gMnSc1	Street
<g/>
.	.	kIx.	.
</s>
<s>
Jedinou	jediný	k2eAgFnSc4d1	jediná
výjimku	výjimka	k1gFnSc4	výjimka
z	z	k7c2	z
rutinní	rutinní	k2eAgFnSc2d1	rutinní
mřížkové	mřížkový	k2eAgFnSc2d1	mřížková
struktury	struktura	k1gFnSc2	struktura
Manhattanu	Manhattan	k1gInSc2	Manhattan
tvoří	tvořit	k5eAaImIp3nS	tvořit
ulice	ulice	k1gFnSc1	ulice
Broadway	Broadwaa	k1gFnSc2	Broadwaa
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
táhne	táhnout	k5eAaImIp3nS	táhnout
úhlopříčně	úhlopříčně	k6eAd1	úhlopříčně
od	od	k7c2	od
Bowling	bowling	k1gInSc1	bowling
Green	Green	k2eAgMnSc1d1	Green
v	v	k7c6	v
Downtownu	Downtown	k1gInSc6	Downtown
přes	přes	k7c4	přes
celý	celý	k2eAgInSc4d1	celý
Manhattan	Manhattan	k1gInSc4	Manhattan
až	až	k9	až
do	do	k7c2	do
sousedního	sousední	k2eAgInSc2d1	sousední
Bronxu	Bronx	k1gInSc2	Bronx
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
názvy	název	k1gInPc1	název
čtvrtí	čtvrt	k1gFnPc2	čtvrt
jsou	být	k5eAaImIp3nP	být
odvozeny	odvodit	k5eAaPmNgFnP	odvodit
od	od	k7c2	od
zeměpisného	zeměpisný	k2eAgNnSc2d1	zeměpisné
postavení	postavení	k1gNnSc2	postavení
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Upper	Upper	k1gInSc4	Upper
East	East	k2eAgInSc1d1	East
Side	Side	k1gInSc1	Side
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgInPc1d1	jiný
podle	podle	k7c2	podle
etnických	etnický	k2eAgNnPc2d1	etnické
hledisek	hledisko	k1gNnPc2	hledisko
(	(	kIx(	(
<g/>
Little	Little	k1gFnSc1	Little
Italy	Ital	k1gMnPc4	Ital
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
čtvrtí	čtvrt	k1gFnPc2	čtvrt
jsou	být	k5eAaImIp3nP	být
zkratkami	zkratka	k1gFnPc7	zkratka
<g/>
,	,	kIx,	,
např.	např.	kA	např.
SoHo	SoHo	k1gNnSc1	SoHo
(	(	kIx(	(
<g/>
South	South	k1gInSc1	South
of	of	k?	of
Houston	Houston	k1gInSc1	Houston
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Nomad	Nomad	k1gInSc1	Nomad
(	(	kIx(	(
<g/>
North	North	k1gInSc1	North
Madison	Madison	k1gNnSc1	Madison
Park	park	k1gInSc1	park
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
Harlem	Harl	k1gInSc7	Harl
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
nizozemského	nizozemský	k2eAgNnSc2d1	Nizozemské
města	město	k1gNnSc2	město
z	z	k7c2	z
koloniální	koloniální	k2eAgFnSc2d1	koloniální
éry	éra	k1gFnSc2	éra
Haarlem	Haarl	k1gInSc7	Haarl
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
čtvrť	čtvrť	k1gFnSc1	čtvrť
má	mít	k5eAaImIp3nS	mít
svou	svůj	k3xOyFgFnSc4	svůj
specifickou	specifický	k2eAgFnSc4d1	specifická
historii	historie	k1gFnSc4	historie
<g/>
,	,	kIx,	,
SoHo	SoHo	k6eAd1	SoHo
je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
především	především	k9	především
jako	jako	k8xC	jako
obchodní	obchodní	k2eAgFnSc1d1	obchodní
čtvrť	čtvrť	k1gFnSc1	čtvrť
<g/>
,	,	kIx,	,
Greenwich	Greenwich	k1gInSc1	Greenwich
Village	Village	k1gInSc4	Village
zase	zase	k9	zase
jako	jako	k8xS	jako
rodiště	rodiště	k1gNnSc1	rodiště
beatnické	beatnický	k2eAgFnSc2d1	beatnická
generace	generace	k1gFnSc2	generace
<g/>
,	,	kIx,	,
Chelsea	Chelsea	k1gFnSc1	Chelsea
je	být	k5eAaImIp3nS	být
čtvrť	čtvrť	k1gFnSc4	čtvrť
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
nočního	noční	k2eAgInSc2d1	noční
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
Chinatown	Chinatown	k1gMnSc1	Chinatown
zase	zase	k9	zase
žije	žít	k5eAaImIp3nS	žít
čínskou	čínský	k2eAgFnSc7d1	čínská
kulturou	kultura	k1gFnSc7	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
Manhattan	Manhattan	k1gInSc1	Manhattan
sousedí	sousedit	k5eAaImIp3nS	sousedit
s	s	k7c7	s
Brooklynem	Brooklyn	k1gInSc7	Brooklyn
a	a	k8xC	a
Queensem	Queens	k1gInSc7	Queens
na	na	k7c6	na
západě	západ	k1gInSc6	západ
<g/>
,	,	kIx,	,
s	s	k7c7	s
Bronxem	Bronx	k1gInSc7	Bronx
na	na	k7c6	na
severu	sever	k1gInSc6	sever
a	a	k8xC	a
se	s	k7c7	s
státem	stát	k1gInSc7	stát
New	New	k1gMnSc2	New
Jersey	Jersea	k1gMnSc2	Jersea
na	na	k7c6	na
východě	východ	k1gInSc6	východ
<g/>
.	.	kIx.	.
</s>
<s>
Jižně	jižně	k6eAd1	jižně
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
nachází	nacházet	k5eAaImIp3nS	nacházet
poslední	poslední	k2eAgFnSc1d1	poslední
nejmenovaná	jmenovaný	k2eNgFnSc1d1	nejmenovaná
newyorská	newyorský	k2eAgFnSc1d1	newyorská
čtvrť	čtvrť	k1gFnSc1	čtvrť
<g/>
,	,	kIx,	,
Staten	Staten	k2eAgInSc1d1	Staten
Island	Island	k1gInSc1	Island
<g/>
.	.	kIx.	.
</s>
<s>
Manhattan	Manhattan	k1gInSc1	Manhattan
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
přechodné	přechodný	k2eAgFnSc6d1	přechodná
klimatické	klimatický	k2eAgFnSc6d1	klimatická
zóně	zóna	k1gFnSc6	zóna
mezi	mezi	k7c7	mezi
vlhkým	vlhký	k2eAgNnSc7d1	vlhké
subtropickým	subtropický	k2eAgNnSc7d1	subtropické
podnebím	podnebí	k1gNnSc7	podnebí
a	a	k8xC	a
vlhkým	vlhký	k2eAgNnSc7d1	vlhké
kontinentálním	kontinentální	k2eAgNnSc7d1	kontinentální
podnebím	podnebí	k1gNnSc7	podnebí
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
si	se	k3xPyFc3	se
Manhattan	Manhattan	k1gInSc1	Manhattan
i	i	k9	i
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
udržuje	udržovat	k5eAaImIp3nS	udržovat
relativně	relativně	k6eAd1	relativně
vyšší	vysoký	k2eAgFnPc4d2	vyšší
teploty	teplota	k1gFnPc4	teplota
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
ve	v	k7c6	v
vnitrozemských	vnitrozemský	k2eAgInPc6d1	vnitrozemský
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Jaro	jaro	k1gNnSc1	jaro
a	a	k8xC	a
podzim	podzim	k1gInSc4	podzim
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
Manhattanu	Manhattan	k1gInSc6	Manhattan
mírnější	mírný	k2eAgFnPc1d2	mírnější
<g/>
,	,	kIx,	,
léta	léto	k1gNnPc1	léto
jsou	být	k5eAaImIp3nP	být
teplejší	teplý	k2eAgInPc1d2	teplejší
s	s	k7c7	s
teplotami	teplota	k1gFnPc7	teplota
přes	přes	k7c4	přes
32	[number]	k4	32
°	°	k?	°
<g/>
C.	C.	kA	C.
Horní	horní	k2eAgFnSc1d1	horní
hranice	hranice	k1gFnSc1	hranice
teplotního	teplotní	k2eAgInSc2d1	teplotní
rekordu	rekord	k1gInSc2	rekord
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
41	[number]	k4	41
°	°	k?	°
<g/>
C	C	kA	C
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
červenec	červenec	k1gInSc1	červenec
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejnižší	nízký	k2eAgFnSc1d3	nejnižší
teplota	teplota	k1gFnSc1	teplota
-26	-26	k4	-26
°	°	k?	°
<g/>
C	C	kA	C
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
únor	únor	k1gInSc1	únor
1934	[number]	k4	1934
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
sněhu	sníh	k1gInSc2	sníh
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
průměru	průměr	k1gInSc2	průměr
63,5	[number]	k4	63,5
cm	cm	kA	cm
<g/>
,	,	kIx,	,
rekordním	rekordní	k2eAgInSc6d1	rekordní
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
ohledu	ohled	k1gInSc6	ohled
byl	být	k5eAaImAgInS	být
rok	rok	k1gInSc1	rok
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
napadlo	napadnout	k5eAaPmAgNnS	napadnout
až	až	k9	až
150	[number]	k4	150
cm	cm	kA	cm
sněhu	sníh	k1gInSc2	sníh
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Manhattanu	Manhattan	k1gInSc6	Manhattan
žije	žít	k5eAaImIp3nS	žít
přibližně	přibližně	k6eAd1	přibližně
1	[number]	k4	1
619	[number]	k4	619
090	[number]	k4	090
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
hustota	hustota	k1gFnSc1	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
cca	cca	kA	cca
27	[number]	k4	27
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
/	/	kIx~	/
<g/>
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
65	[number]	k4	65
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
bělochů	běloch	k1gMnPc2	běloch
<g/>
,	,	kIx,	,
18	[number]	k4	18
%	%	kIx~	%
černochů	černoch	k1gMnPc2	černoch
<g/>
,	,	kIx,	,
12	[number]	k4	12
%	%	kIx~	%
asiatů	asiat	k1gMnPc2	asiat
<g/>
,	,	kIx,	,
1	[number]	k4	1
%	%	kIx~	%
indiánů	indián	k1gMnPc2	indián
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
26	[number]	k4	26
%	%	kIx~	%
obyvatel	obyvatel	k1gMnSc1	obyvatel
Manhattanu	Manhattan	k1gInSc2	Manhattan
je	být	k5eAaImIp3nS	být
hispánského	hispánský	k2eAgInSc2d1	hispánský
nebo	nebo	k8xC	nebo
latinského	latinský	k2eAgInSc2d1	latinský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Manhattan	Manhattan	k1gInSc1	Manhattan
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejvýše	vysoce	k6eAd3	vysoce
příjmových	příjmový	k2eAgNnPc2d1	příjmové
měst	město	k1gNnPc2	město
v	v	k7c6	v
USA	USA	kA	USA
s	s	k7c7	s
počtem	počet	k1gInSc7	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
vyšším	vysoký	k2eAgInSc6d2	vyšší
než	než	k8xS	než
1	[number]	k4	1
milión	milión	k4xCgInSc4	milión
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
byla	být	k5eAaImAgFnS	být
průměrná	průměrný	k2eAgFnSc1d1	průměrná
daňová	daňový	k2eAgFnSc1d1	daňová
povinnost	povinnost	k1gFnSc1	povinnost
cca	cca	kA	cca
25	[number]	k4	25
000	[number]	k4	000
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Manhattan	Manhattan	k1gInSc1	Manhattan
má	mít	k5eAaImIp3nS	mít
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
příjem	příjem	k1gInSc1	příjem
na	na	k7c4	na
obyvatele	obyvatel	k1gMnPc4	obyvatel
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
na	na	k7c4	na
Upper	Upper	k1gInSc4	Upper
East	East	k2eAgInSc4d1	East
Side	Side	k1gInSc4	Side
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
koncentrace	koncentrace	k1gFnSc1	koncentrace
bohatých	bohatý	k2eAgMnPc2d1	bohatý
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
průměrný	průměrný	k2eAgInSc1d1	průměrný
příjem	příjem	k1gInSc1	příjem
90	[number]	k4	90
000	[number]	k4	000
dolarů	dolar	k1gInPc2	dolar
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Manhattan	Manhattan	k1gInSc1	Manhattan
také	také	k9	také
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
zažívá	zažívat	k5eAaImIp3nS	zažívat
tzv.	tzv.	kA	tzv.
baby	baby	k1gNnSc1	baby
boom	boom	k1gInSc1	boom
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
se	se	k3xPyFc4	se
počet	počet	k1gInSc1	počet
dětí	dítě	k1gFnPc2	dítě
do	do	k7c2	do
5	[number]	k4	5
let	léto	k1gNnPc2	léto
žijících	žijící	k2eAgMnPc2d1	žijící
na	na	k7c6	na
Manhattanu	Manhattan	k1gInSc6	Manhattan
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
o	o	k7c4	o
32	[number]	k4	32
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Manhattan	Manhattan	k1gInSc1	Manhattan
je	být	k5eAaImIp3nS	být
nábožensky	nábožensky	k6eAd1	nábožensky
velmi	velmi	k6eAd1	velmi
různorodý	různorodý	k2eAgInSc1d1	různorodý
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
dáno	dát	k5eAaPmNgNnS	dát
především	především	k6eAd1	především
vysokou	vysoký	k2eAgFnSc7d1	vysoká
koncentrací	koncentrace	k1gFnSc7	koncentrace
různých	různý	k2eAgFnPc2d1	různá
kultur	kultura	k1gFnPc2	kultura
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
části	část	k1gFnSc6	část
New	New	k1gFnSc4	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgNnSc7d3	veliký
náboženstvím	náboženství	k1gNnSc7	náboženství
je	být	k5eAaImIp3nS	být
římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
církev	církev	k1gFnSc1	církev
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
asi	asi	k9	asi
36	[number]	k4	36
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Následují	následovat	k5eAaImIp3nP	následovat
židé	žid	k1gMnPc1	žid
s	s	k7c7	s
20	[number]	k4	20
%	%	kIx~	%
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
protestanti	protestant	k1gMnPc1	protestant
(	(	kIx(	(
<g/>
9	[number]	k4	9
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
muslimové	muslim	k1gMnPc1	muslim
(	(	kIx(	(
<g/>
2,5	[number]	k4	2,5
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
60	[number]	k4	60
%	%	kIx~	%
obyvatel	obyvatel	k1gMnSc1	obyvatel
Manhattanu	Manhattan	k1gInSc2	Manhattan
mluví	mluvit	k5eAaImIp3nS	mluvit
anglicky	anglicky	k6eAd1	anglicky
jako	jako	k8xS	jako
rodným	rodný	k2eAgInSc7d1	rodný
jazykem	jazyk	k1gInSc7	jazyk
<g/>
,	,	kIx,	,
23	[number]	k4	23
%	%	kIx~	%
španělsky	španělsky	k6eAd1	španělsky
<g/>
,	,	kIx,	,
5	[number]	k4	5
%	%	kIx~	%
čínsky	čínsky	k6eAd1	čínsky
<g/>
,	,	kIx,	,
2	[number]	k4	2
%	%	kIx~	%
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
,	,	kIx,	,
1	[number]	k4	1
%	%	kIx~	%
japonsky	japonsky	k6eAd1	japonsky
<g/>
,	,	kIx,	,
1	[number]	k4	1
%	%	kIx~	%
korejsky	korejsky	k6eAd1	korejsky
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
jazyky	jazyk	k1gInPc1	jazyk
nedosahují	dosahovat	k5eNaImIp3nP	dosahovat
výše	vysoce	k6eAd2	vysoce
1	[number]	k4	1
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
jich	on	k3xPp3gFnPc2	on
několik	několik	k4yIc4	několik
desítek	desítka	k1gFnPc2	desítka
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
40	[number]	k4	40
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
Manhattanu	Manhattan	k1gInSc2	Manhattan
mluví	mluvit	k5eAaImIp3nP	mluvit
jiným	jiný	k2eAgInSc7d1	jiný
rodným	rodný	k2eAgInSc7d1	rodný
jazykem	jazyk	k1gInSc7	jazyk
<g/>
,	,	kIx,	,
než	než	k8xS	než
angličtinou	angličtina	k1gFnSc7	angličtina
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
z	z	k7c2	z
Manhattanu	Manhattan	k1gInSc2	Manhattan
dělá	dělat	k5eAaImIp3nS	dělat
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
jazykově	jazykově	k6eAd1	jazykově
a	a	k8xC	a
kulturně	kulturně	k6eAd1	kulturně
nejrozmanitějších	rozmanitý	k2eAgNnPc2d3	nejrozmanitější
měst	město	k1gNnPc2	město
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Manhattan	Manhattan	k1gInSc1	Manhattan
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
především	především	k6eAd1	především
velkou	velký	k2eAgFnSc7d1	velká
koncentrací	koncentrace	k1gFnSc7	koncentrace
vysokých	vysoký	k2eAgInPc2d1	vysoký
mrakodrapů	mrakodrap	k1gInPc2	mrakodrap
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
dalších	další	k2eAgFnPc2d1	další
památek	památka	k1gFnPc2	památka
a	a	k8xC	a
atrakcí	atrakce	k1gFnPc2	atrakce
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
různorodé	různorodý	k2eAgFnPc1d1	různorodá
v	v	k7c6	v
aplikované	aplikovaný	k2eAgFnSc6d1	aplikovaná
architektuře	architektura	k1gFnSc6	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
města	město	k1gNnSc2	město
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
architektonickém	architektonický	k2eAgInSc6d1	architektonický
stylu	styl	k1gInSc6	styl
art	art	k?	art
deco	deco	k1gMnSc1	deco
<g/>
,	,	kIx,	,
nejznámějšími	známý	k2eAgFnPc7d3	nejznámější
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
největšími	veliký	k2eAgFnPc7d3	veliký
budovami	budova	k1gFnPc7	budova
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
stylu	styl	k1gInSc6	styl
jsou	být	k5eAaImIp3nP	být
Empire	empir	k1gInSc5	empir
State	status	k1gInSc5	status
Building	Building	k1gInSc1	Building
a	a	k8xC	a
Chrysler	Chrysler	k1gInSc1	Chrysler
Building	Building	k1gInSc1	Building
<g/>
.	.	kIx.	.
</s>
<s>
Výškové	výškový	k2eAgInPc1d1	výškový
mrakodrapy	mrakodrap	k1gInPc1	mrakodrap
jsou	být	k5eAaImIp3nP	být
nejen	nejen	k6eAd1	nejen
poznávacím	poznávací	k2eAgNnSc7d1	poznávací
znamením	znamení	k1gNnSc7	znamení
Manhattanu	Manhattan	k1gInSc2	Manhattan
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k6eAd1	také
výsadou	výsada	k1gFnSc7	výsada
této	tento	k3xDgFnSc2	tento
čtvrti	čtvrt	k1gFnSc2	čtvrt
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1890	[number]	k4	1890
-	-	kIx~	-
1973	[number]	k4	1973
se	se	k3xPyFc4	se
na	na	k7c6	na
pozici	pozice	k1gFnSc6	pozice
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
budovy	budova	k1gFnSc2	budova
světa	svět	k1gInSc2	svět
vystřídalo	vystřídat	k5eAaPmAgNnS	vystřídat
celkem	celkem	k6eAd1	celkem
9	[number]	k4	9
budov	budova	k1gFnPc2	budova
stojících	stojící	k2eAgFnPc2d1	stojící
právě	právě	k6eAd1	právě
na	na	k7c6	na
Manhattanu	Manhattan	k1gInSc6	Manhattan
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
stavbou	stavba	k1gFnSc7	stavba
byla	být	k5eAaImAgFnS	být
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
World	World	k1gMnSc1	World
Building	Building	k1gInSc1	Building
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1890	[number]	k4	1890
s	s	k7c7	s
91	[number]	k4	91
m	m	kA	m
<g/>
,	,	kIx,	,
následovaly	následovat	k5eAaImAgFnP	následovat
Park	park	k1gInSc4	park
Row	Row	k1gFnSc2	Row
Building	Building	k1gInSc1	Building
(	(	kIx(	(
<g/>
119	[number]	k4	119
m	m	kA	m
<g/>
,	,	kIx,	,
1899	[number]	k4	1899
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Singer	Singer	k1gInSc1	Singer
Building	Building	k1gInSc1	Building
(	(	kIx(	(
<g/>
187	[number]	k4	187
m	m	kA	m
<g/>
,	,	kIx,	,
1908	[number]	k4	1908
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Woolworth	Woolworth	k1gInSc1	Woolworth
Building	Building	k1gInSc1	Building
(	(	kIx(	(
<g/>
241	[number]	k4	241
m	m	kA	m
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
1913	[number]	k4	1913
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
40	[number]	k4	40
Wall	Wallum	k1gNnPc2	Wallum
Street	Streeta	k1gFnPc2	Streeta
(	(	kIx(	(
<g/>
282	[number]	k4	282
m	m	kA	m
<g/>
,	,	kIx,	,
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Chrysler	Chrysler	k1gInSc1	Chrysler
Building	Building	k1gInSc1	Building
(	(	kIx(	(
<g/>
319	[number]	k4	319
m	m	kA	m
<g/>
,	,	kIx,	,
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Empire	empir	k1gInSc5	empir
State	status	k1gInSc5	status
Building	Building	k1gInSc1	Building
(	(	kIx(	(
<g/>
381	[number]	k4	381
m	m	kA	m
<g/>
,	,	kIx,	,
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
a	a	k8xC	a
World	World	k1gMnSc1	World
Trade	Trad	k1gInSc5	Trad
Center	centrum	k1gNnPc2	centrum
(	(	kIx(	(
<g/>
417	[number]	k4	417
m	m	kA	m
<g/>
,	,	kIx,	,
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Aktuálně	aktuálně	k6eAd1	aktuálně
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
budovou	budova	k1gFnSc7	budova
Manhattanu	Manhattan	k1gInSc2	Manhattan
One	One	k1gMnSc1	One
World	World	k1gMnSc1	World
Trade	Trad	k1gInSc5	Trad
Center	centrum	k1gNnPc2	centrum
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
3	[number]	k4	3
<g/>
.	.	kIx.	.
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
budovou	budova	k1gFnSc7	budova
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
541	[number]	k4	541
m	m	kA	m
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalším	další	k2eAgFnPc3d1	další
pozoruhodným	pozoruhodný	k2eAgFnPc3d1	pozoruhodná
stavbám	stavba	k1gFnPc3	stavba
náleží	náležet	k5eAaImIp3nS	náležet
<g/>
:	:	kIx,	:
Flatiron	Flatiron	k1gInSc1	Flatiron
Building	Building	k1gInSc1	Building
<g/>
,	,	kIx,	,
Seagram	Seagram	k1gInSc1	Seagram
Building	Building	k1gInSc1	Building
<g/>
,	,	kIx,	,
8	[number]	k4	8
Spruce	Spruce	k1gFnSc1	Spruce
Street	Streeta	k1gFnPc2	Streeta
(	(	kIx(	(
<g/>
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
by	by	kYmCp3nS	by
Gehry	Gehra	k1gFnPc1	Gehra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rockefeller	Rockefeller	k1gMnSc1	Rockefeller
Center	centrum	k1gNnPc2	centrum
<g/>
,	,	kIx,	,
Grand	grand	k1gMnSc1	grand
Central	Central	k1gMnSc1	Central
Terminal	Terminal	k1gMnSc1	Terminal
<g/>
,	,	kIx,	,
1	[number]	k4	1
Wall	Wall	k1gMnSc1	Wall
Street	Street	k1gMnSc1	Street
<g/>
,	,	kIx,	,
American	American	k1gMnSc1	American
International	International	k1gFnSc2	International
Building	Building	k1gInSc1	Building
(	(	kIx(	(
<g/>
70	[number]	k4	70
Pine	pin	k1gInSc5	pin
Street	Streeta	k1gFnPc2	Streeta
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
Times	Times	k1gMnSc1	Times
Building	Building	k1gInSc1	Building
<g/>
,	,	kIx,	,
Equitable	Equitable	k1gFnSc1	Equitable
Building	Building	k1gInSc1	Building
<g/>
,	,	kIx,	,
Old	Olda	k1gFnPc2	Olda
Merchant	Merchanta	k1gFnPc2	Merchanta
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
House	house	k1gNnSc1	house
<g/>
,	,	kIx,	,
Bayard	Bayard	k1gInSc1	Bayard
<g/>
-	-	kIx~	-
<g/>
Condict	Condict	k2eAgInSc1d1	Condict
Building	Building	k1gInSc1	Building
<g/>
,	,	kIx,	,
One	One	k1gFnSc1	One
Chase	chasa	k1gFnSc6	chasa
Manhattan	Manhattan	k1gInSc4	Manhattan
Plaza	plaz	k1gMnSc2	plaz
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
Stock	Stock	k1gMnSc1	Stock
Exchange	Exchange	k1gFnSc1	Exchange
<g/>
,	,	kIx,	,
General	General	k1gFnSc1	General
Electric	Electric	k1gMnSc1	Electric
Building	Building	k1gInSc1	Building
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
<g />
.	.	kIx.	.
</s>
<s>
York	York	k1gInSc1	York
City	city	k1gNnSc1	city
Hall	Hall	k1gInSc1	Hall
<g/>
,	,	kIx,	,
Bridge	Bridge	k1gFnSc1	Bridge
Cafe	Cafe	k1gFnSc1	Cafe
<g/>
,	,	kIx,	,
4	[number]	k4	4
World	Worlda	k1gFnPc2	Worlda
Trade	Trad	k1gInSc5	Trad
Center	centrum	k1gNnPc2	centrum
<g/>
,	,	kIx,	,
500	[number]	k4	500
Fifth	Fiftha	k1gFnPc2	Fiftha
Avenue	avenue	k1gFnPc2	avenue
<g/>
,	,	kIx,	,
270	[number]	k4	270
Park	park	k1gInSc1	park
Avenue	avenue	k1gFnSc2	avenue
<g/>
,	,	kIx,	,
United	United	k1gMnSc1	United
Nations	Nationsa	k1gFnPc2	Nationsa
Headquarters	Headquarters	k1gInSc1	Headquarters
<g/>
,	,	kIx,	,
Chanin	Chanin	k2eAgInSc1d1	Chanin
Building	Building	k1gInSc1	Building
<g/>
,	,	kIx,	,
20	[number]	k4	20
Exchange	Exchange	k1gNnSc2	Exchange
Place	plac	k1gInSc6	plac
<g/>
,	,	kIx,	,
Chamber	Chamber	k1gMnSc1	Chamber
of	of	k?	of
Commerce	Commerka	k1gFnSc3	Commerka
Building	Building	k1gInSc4	Building
<g/>
,	,	kIx,	,
MetLife	MetLif	k1gInSc5	MetLif
Building	Building	k1gInSc1	Building
<g/>
,	,	kIx,	,
731	[number]	k4	731
<g />
.	.	kIx.	.
</s>
<s>
Lexington	Lexington	k1gInSc1	Lexington
Avenue	avenue	k1gFnSc2	avenue
<g/>
,	,	kIx,	,
R.	R.	kA	R.
H.	H.	kA	H.
Macy	Maca	k1gMnSc2	Maca
and	and	k?	and
Company	Compana	k1gFnSc2	Compana
Store	Stor	k1gInSc5	Stor
<g/>
,	,	kIx,	,
Trump	Trump	k1gInSc4	Trump
World	Worlda	k1gFnPc2	Worlda
Tower	Towra	k1gFnPc2	Towra
<g/>
,	,	kIx,	,
Daily	Daila	k1gFnSc2	Daila
News	Newsa	k1gFnPc2	Newsa
Building	Building	k1gInSc1	Building
<g/>
,	,	kIx,	,
Mercantile	Mercantil	k1gMnSc5	Mercantil
Building	Building	k1gInSc1	Building
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Dakota	Dakota	k1gFnSc1	Dakota
<g/>
,	,	kIx,	,
One	One	k1gMnSc1	One
Dag	dag	kA	dag
Hammarskjold	Hammarskjold	k1gMnSc1	Hammarskjold
Plaza	plaz	k1gMnSc2	plaz
<g/>
,	,	kIx,	,
Hotel	hotel	k1gInSc4	hotel
Plaza	plaz	k1gMnSc2	plaz
<g/>
,	,	kIx,	,
Surrogate	Surrogat	k1gMnSc5	Surrogat
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Courthouse	Courthouse	k1gFnSc5	Courthouse
<g/>
,	,	kIx,	,
Citigroup	Citigroup	k1gMnSc1	Citigroup
Center	centrum	k1gNnPc2	centrum
<g/>
,	,	kIx,	,
Time	Time	k1gNnSc1	Time
Warner	Warnra	k1gFnPc2	Warnra
Center	centrum	k1gNnPc2	centrum
<g/>
,	,	kIx,	,
Condé	Condý	k2eAgNnSc4d1	Condé
Nast	Nast	k2eAgInSc4d1	Nast
Building	Building	k1gInSc4	Building
<g/>
.	.	kIx.	.
</s>
<s>
Trinity	Trinita	k1gFnPc1	Trinita
Church	Churcha	k1gFnPc2	Churcha
<g/>
,	,	kIx,	,
katedrála	katedrála	k1gFnSc1	katedrála
Saint	Saint	k1gMnSc1	Saint
John	John	k1gMnSc1	John
the	the	k?	the
Divine	Divin	k1gMnSc5	Divin
<g/>
,	,	kIx,	,
katedrála	katedrál	k1gMnSc4	katedrál
sv.	sv.	kA	sv.
Patrika	Patrik	k1gMnSc4	Patrik
<g/>
,	,	kIx,	,
Central	Central	k1gMnSc4	Central
Synagogue	Synagogu	k1gMnSc4	Synagogu
<g/>
,	,	kIx,	,
St.	st.	kA	st.
Paul	Paul	k1gMnSc1	Paul
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Chapel	Chapel	k1gInSc1	Chapel
<g/>
,	,	kIx,	,
Grace	Grace	k1gMnSc1	Grace
Church	Church	k1gMnSc1	Church
<g/>
,	,	kIx,	,
Church	Church	k1gMnSc1	Church
of	of	k?	of
the	the	k?	the
Ascension	Ascension	k1gInSc1	Ascension
<g/>
,	,	kIx,	,
Eldridge	Eldridge	k1gNnSc1	Eldridge
Street	Streeta	k1gFnPc2	Streeta
Synagogue	Synagogue	k1gNnSc2	Synagogue
<g/>
,	,	kIx,	,
St.	st.	kA	st.
George	Georg	k1gMnSc2	Georg
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Episcopal	Episcopal	k1gFnSc7	Episcopal
Church	Churcha	k1gFnPc2	Churcha
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejznámějším	známý	k2eAgNnPc3d3	nejznámější
a	a	k8xC	a
hlavním	hlavní	k2eAgNnPc3d1	hlavní
náměstím	náměstí	k1gNnPc3	náměstí
a	a	k8xC	a
ulicím	ulice	k1gFnPc3	ulice
na	na	k7c6	na
Manhattanu	Manhattan	k1gInSc6	Manhattan
náleží	náležet	k5eAaImIp3nS	náležet
<g/>
:	:	kIx,	:
Times	Times	k1gInSc1	Times
Square	square	k1gInSc1	square
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
th	th	k?	th
Avenue	avenue	k1gFnPc2	avenue
<g/>
,	,	kIx,	,
Broadway	Broadway	k1gInPc1	Broadway
<g/>
,	,	kIx,	,
Madison	Madison	k1gNnSc1	Madison
Square	square	k1gInSc1	square
<g/>
,	,	kIx,	,
Wall	Wall	k1gMnSc1	Wall
Street	Street	k1gMnSc1	Street
<g/>
,	,	kIx,	,
Union	union	k1gInSc1	union
Square	square	k1gInSc1	square
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
Metropolitan	metropolitan	k1gInSc1	metropolitan
Museum	museum	k1gNnSc4	museum
<g/>
,	,	kIx,	,
American	American	k1gMnSc1	American
Museum	museum	k1gNnSc1	museum
of	of	k?	of
Natural	Natural	k?	Natural
History	Histor	k1gInPc4	Histor
<g/>
,	,	kIx,	,
Guggenheim	Guggenheim	k1gInSc4	Guggenheim
Museum	museum	k1gNnSc4	museum
<g/>
,	,	kIx,	,
Carnegie	Carnegie	k1gFnSc1	Carnegie
Hall	Hall	k1gMnSc1	Hall
<g/>
,	,	kIx,	,
Lincoln	Lincoln	k1gMnSc1	Lincoln
Center	centrum	k1gNnPc2	centrum
<g/>
,	,	kIx,	,
Radio	radio	k1gNnSc1	radio
City	City	k1gFnPc1	City
Music	Music	k1gMnSc1	Music
Hall	Hall	k1gMnSc1	Hall
<g/>
,	,	kIx,	,
New	New	k1gMnSc1	New
York	York	k1gInSc1	York
Public	publicum	k1gNnPc2	publicum
Library	Librara	k1gFnSc2	Librara
<g/>
,	,	kIx,	,
Columbia	Columbia	k1gFnSc1	Columbia
University	universita	k1gFnPc1	universita
<g/>
,	,	kIx,	,
New	New	k1gFnPc1	New
York	York	k1gInSc1	York
University	universita	k1gFnSc2	universita
<g/>
.	.	kIx.	.
</s>
<s>
Brooklyn	Brooklyn	k1gInSc1	Brooklyn
Bridge	Bridge	k1gInSc1	Bridge
<g/>
,	,	kIx,	,
Manhattan	Manhattan	k1gInSc1	Manhattan
Bridge	Bridg	k1gFnSc2	Bridg
<g/>
,	,	kIx,	,
Queensboro	Queensbora	k1gFnSc5	Queensbora
Bridge	Bridge	k1gFnSc5	Bridge
<g/>
,	,	kIx,	,
Williamsburg	Williamsburg	k1gMnSc1	Williamsburg
Bridge	Bridg	k1gFnSc2	Bridg
<g/>
.	.	kIx.	.
</s>
<s>
Central	Centrat	k5eAaPmAgInS	Centrat
Park	park	k1gInSc1	park
<g/>
.	.	kIx.	.
</s>
<s>
Battery	Batter	k1gMnPc4	Batter
Park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
Washington	Washington	k1gInSc1	Washington
Square	square	k1gInSc1	square
Park	park	k1gInSc1	park
<g/>
.	.	kIx.	.
</s>
<s>
Upper	Upper	k1gInSc1	Upper
Manhattan	Manhattan	k1gInSc1	Manhattan
<g/>
,	,	kIx,	,
leží	ležet	k5eAaImIp3nS	ležet
severně	severně	k6eAd1	severně
od	od	k7c2	od
W	W	kA	W
110	[number]	k4	110
<g/>
th	th	k?	th
St	St	kA	St
na	na	k7c6	na
západě	západ	k1gInSc6	západ
a	a	k8xC	a
severně	severně	k6eAd1	severně
od	od	k7c2	od
E	E	kA	E
96	[number]	k4	96
<g/>
th	th	k?	th
St	St	kA	St
na	na	k7c6	na
východě	východ	k1gInSc6	východ
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
čtvrti	čtvrt	k1gFnPc4	čtvrt
<g/>
:	:	kIx,	:
Morningside	Morningsid	k1gInSc5	Morningsid
Heights	Heights	k1gInSc1	Heights
<g/>
,	,	kIx,	,
East	East	k1gInSc1	East
Harlem	Harlo	k1gNnSc7	Harlo
<g/>
,	,	kIx,	,
Harlem	Harlo	k1gNnSc7	Harlo
<g/>
,	,	kIx,	,
Manhattanville	Manhattanville	k1gInSc1	Manhattanville
<g/>
,	,	kIx,	,
Hamilton	Hamilton	k1gInSc1	Hamilton
Heights	Heights	k1gInSc1	Heights
<g/>
,	,	kIx,	,
Sugar	Sugar	k1gMnSc1	Sugar
Hill	Hill	k1gMnSc1	Hill
<g/>
,	,	kIx,	,
Washington	Washington	k1gInSc1	Washington
Heights	Heights	k1gInSc1	Heights
<g/>
,	,	kIx,	,
Hudson	Hudson	k1gNnSc1	Hudson
Heights	Heightsa	k1gFnPc2	Heightsa
<g/>
,	,	kIx,	,
Inwood	Inwooda	k1gFnPc2	Inwooda
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
Midtownem	Midtown	k1gInSc7	Midtown
a	a	k8xC	a
Upper	Upper	k1gInSc4	Upper
Manhattanem	Manhattan	k1gInSc7	Manhattan
leží	ležet	k5eAaImIp3nP	ležet
čtvrti	čtvrt	k1gFnPc4	čtvrt
<g/>
:	:	kIx,	:
Lincoln	Lincoln	k1gMnSc1	Lincoln
Square	square	k1gInSc4	square
<g/>
,	,	kIx,	,
Lenox	Lenox	k1gInSc4	Lenox
Hill	Hilla	k1gFnPc2	Hilla
<g/>
,	,	kIx,	,
Upper	Uppra	k1gFnPc2	Uppra
West	Westa	k1gFnPc2	Westa
Side	Side	k1gInSc1	Side
<g/>
,	,	kIx,	,
Upper	Upper	k1gInSc1	Upper
East	East	k1gInSc1	East
Side	Side	k1gFnSc1	Side
<g/>
,	,	kIx,	,
Yorkville	Yorkville	k1gFnSc1	Yorkville
<g/>
,	,	kIx,	,
Carnegie	Carnegie	k1gFnSc1	Carnegie
Hill	Hill	k1gMnSc1	Hill
<g/>
,	,	kIx,	,
Manhattan	Manhattan	k1gInSc1	Manhattan
Valley	Vallea	k1gFnSc2	Vallea
<g/>
.	.	kIx.	.
</s>
<s>
Midtown	Midtown	k1gInSc1	Midtown
Manhattan	Manhattan	k1gInSc1	Manhattan
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
oblast	oblast	k1gFnSc1	oblast
mezi	mezi	k7c7	mezi
14	[number]	k4	14
<g/>
th	th	k?	th
Street	Streeta	k1gFnPc2	Streeta
a	a	k8xC	a
59	[number]	k4	59
<g/>
th	th	k?	th
Street	Street	k1gMnSc1	Street
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
je	on	k3xPp3gFnPc4	on
čtvrti	čtvrt	k1gFnPc4	čtvrt
<g/>
:	:	kIx,	:
Chelsea	Chelsea	k1gMnSc1	Chelsea
<g/>
,	,	kIx,	,
Flatiron	Flatiron	k1gMnSc1	Flatiron
District	District	k1gMnSc1	District
<g/>
,	,	kIx,	,
Gramercy	Gramerca	k1gFnPc1	Gramerca
Park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
Rose	Rose	k1gMnSc1	Rose
Hill	Hill	k1gMnSc1	Hill
<g/>
,	,	kIx,	,
Hudson	Hudson	k1gMnSc1	Hudson
Yards	Yards	k1gInSc1	Yards
<g/>
,	,	kIx,	,
Garment	Garment	k1gMnSc1	Garment
District	District	k1gMnSc1	District
<g/>
,	,	kIx,	,
Koreatown	Koreatown	k1gMnSc1	Koreatown
<g/>
,	,	kIx,	,
Murray	Murraa	k1gFnPc1	Murraa
Hill	Hill	k1gMnSc1	Hill
<g/>
,	,	kIx,	,
Theater	Theater	k1gMnSc1	Theater
District	District	k1gMnSc1	District
<g/>
,	,	kIx,	,
Midtown	Midtown	k1gMnSc1	Midtown
<g/>
,	,	kIx,	,
Midtown	Midtown	k1gMnSc1	Midtown
East	East	k1gMnSc1	East
<g/>
,	,	kIx,	,
Hell	Hell	k1gMnSc1	Hell
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Kitchen	Kitchen	k1gInSc1	Kitchen
<g/>
,	,	kIx,	,
Sutton	Sutton	k1gInSc1	Sutton
Place	plac	k1gInSc6	plac
<g/>
.	.	kIx.	.
</s>
<s>
Lower	Lower	k1gInSc1	Lower
Manhattan	Manhattan	k1gInSc1	Manhattan
nebo	nebo	k8xC	nebo
také	také	k9	také
Downtown	Downtown	k1gInSc1	Downtown
Manhattan	Manhattan	k1gInSc1	Manhattan
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
oblast	oblast	k1gFnSc4	oblast
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
14	[number]	k4	14
<g/>
th	th	k?	th
Street	Street	k1gMnSc1	Street
<g/>
.	.	kIx.	.
</s>
<s>
Downtown	Downtown	k1gInSc1	Downtown
Manhattan	Manhattan	k1gInSc1	Manhattan
tvoří	tvořit	k5eAaImIp3nS	tvořit
čtvrti	čtvrt	k1gFnSc3	čtvrt
<g/>
:	:	kIx,	:
Battery	Batter	k1gInPc4	Batter
Park	park	k1gInSc1	park
City	City	k1gFnSc2	City
<g/>
,	,	kIx,	,
Civic	Civic	k1gMnSc1	Civic
Center	centrum	k1gNnPc2	centrum
<g/>
,	,	kIx,	,
Tribeca	Tribeca	k1gMnSc1	Tribeca
<g/>
,	,	kIx,	,
Financial	Financial	k1gMnSc1	Financial
District	Districta	k1gFnPc2	Districta
<g/>
,	,	kIx,	,
Chinatown	Chinatowna	k1gFnPc2	Chinatowna
<g/>
,	,	kIx,	,
Little	Little	k1gFnSc2	Little
Italy	Ital	k1gMnPc4	Ital
<g/>
,	,	kIx,	,
SoHo	SoHo	k1gMnSc1	SoHo
<g/>
,	,	kIx,	,
Lower	Lower	k1gMnSc1	Lower
East	East	k2eAgInSc4d1	East
Side	Side	k1gInSc4	Side
<g/>
,	,	kIx,	,
West	West	k2eAgInSc4d1	West
Village	Village	k1gInSc4	Village
<g/>
,	,	kIx,	,
East	East	k2eAgInSc4d1	East
Village	Village	k1gInSc4	Village
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Manhattanu	Manhattan	k1gInSc6	Manhattan
jasně	jasně	k6eAd1	jasně
vedou	vést	k5eAaImIp3nP	vést
už	už	k9	už
léta	léto	k1gNnPc4	léto
demokraté	demokrat	k1gMnPc1	demokrat
nad	nad	k7c7	nad
republikány	republikán	k1gMnPc7	republikán
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
začínali	začínat	k5eAaImAgMnP	začínat
s	s	k7c7	s
těsným	těsný	k2eAgInSc7d1	těsný
náskokem	náskok	k1gInSc7	náskok
52	[number]	k4	52
<g/>
%	%	kIx~	%
a	a	k8xC	a
postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
svou	svůj	k3xOyFgFnSc4	svůj
pozici	pozice	k1gFnSc4	pozice
upevňovali	upevňovat	k5eAaImAgMnP	upevňovat
až	až	k9	až
dodnes	dodnes	k6eAd1	dodnes
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jejich	jejich	k3xOp3gFnSc1	jejich
nadvláda	nadvláda	k1gFnSc1	nadvláda
činí	činit	k5eAaImIp3nS	činit
přibližně	přibližně	k6eAd1	přibližně
84	[number]	k4	84
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Politicky	politicky	k6eAd1	politicky
je	být	k5eAaImIp3nS	být
Manhattan	Manhattan	k1gInSc1	Manhattan
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
na	na	k7c4	na
čtyři	čtyři	k4xCgInPc4	čtyři
okrsky	okrsek	k1gInPc4	okrsek
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
všechny	všechen	k3xTgInPc4	všechen
vedou	vést	k5eAaImIp3nP	vést
demokraté	demokrat	k1gMnPc1	demokrat
(	(	kIx(	(
<g/>
Charles	Charles	k1gMnSc1	Charles
B.	B.	kA	B.
Rangel	Rangel	k1gMnSc1	Rangel
<g/>
,	,	kIx,	,
Jerrold	Jerrold	k1gMnSc1	Jerrold
Nadler	Nadler	k1gMnSc1	Nadler
<g/>
,	,	kIx,	,
Carolyn	Carolyn	k1gMnSc1	Carolyn
B.	B.	kA	B.
Maloney	Malonea	k1gFnPc1	Malonea
<g/>
,	,	kIx,	,
Nydia	Nydia	k1gFnSc1	Nydia
Velázquez	Velázquez	k1gMnSc1	Velázquez
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
žádný	žádný	k1gMnSc1	žádný
republikán	republikán	k1gMnSc1	republikán
nevyhrál	vyhrát	k5eNaPmAgMnS	vyhrát
prezidentské	prezidentský	k2eAgFnPc4d1	prezidentská
volby	volba	k1gFnPc4	volba
na	na	k7c6	na
Manhattanu	Manhattan	k1gInSc6	Manhattan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
oblast	oblast	k1gFnSc1	oblast
rájem	ráj	k1gInSc7	ráj
nelegálního	legální	k2eNgInSc2d1	nelegální
hazardu	hazard	k1gInSc2	hazard
a	a	k8xC	a
prostituce	prostituce	k1gFnSc2	prostituce
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
známa	znám	k2eAgFnSc1d1	známa
jako	jako	k8xC	jako
velmi	velmi	k6eAd1	velmi
nebezpečné	bezpečný	k2eNgNnSc4d1	nebezpečné
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
v	v	k7c6	v
New	New	k1gFnPc6	New
Yorku	York	k1gInSc2	York
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
organizovaný	organizovaný	k2eAgInSc4d1	organizovaný
zločin	zločin	k1gInSc4	zločin
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
gradoval	gradovat	k5eAaImAgInS	gradovat
přistěhovalectvím	přistěhovalectví	k1gNnSc7	přistěhovalectví
mnoha	mnoho	k4c3	mnoho
Italů	Ital	k1gMnPc2	Ital
včetně	včetně	k7c2	včetně
známého	známý	k2eAgMnSc2d1	známý
gangstera	gangster	k1gMnSc2	gangster
Al	ala	k1gFnPc2	ala
Capona	Capon	k1gMnSc2	Capon
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
pobřeží	pobřeží	k1gNnSc6	pobřeží
USA	USA	kA	USA
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
šířit	šířit	k5eAaImF	šířit
sicilská	sicilský	k2eAgFnSc1d1	sicilská
mafie	mafie	k1gFnSc1	mafie
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xS	jako
Cosa	Cos	k2eAgFnSc1d1	Cosa
Nostra	Nostra	k1gFnSc1	Nostra
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
využila	využít	k5eAaPmAgFnS	využít
prohibice	prohibice	k1gFnSc1	prohibice
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
jako	jako	k9	jako
příležitosti	příležitost	k1gFnPc1	příležitost
k	k	k7c3	k
nelegálnímu	legální	k2eNgInSc3d1	nelegální
obchodu	obchod	k1gInSc3	obchod
s	s	k7c7	s
alkoholem	alkohol	k1gInSc7	alkohol
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
tyto	tento	k3xDgFnPc1	tento
události	událost	k1gFnPc1	událost
znamenaly	znamenat	k5eAaImAgFnP	znamenat
neustálý	neustálý	k2eAgInSc4d1	neustálý
nárůst	nárůst	k1gInSc4	nárůst
kriminality	kriminalita	k1gFnSc2	kriminalita
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
až	až	k6eAd1	až
do	do	k7c2	do
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
bylo	být	k5eAaImAgNnS	být
v	v	k7c4	v
New	New	k1gFnSc4	New
Yorku	York	k1gInSc2	York
zaznamenáno	zaznamenat	k5eAaPmNgNnS	zaznamenat
390	[number]	k4	390
vražd	vražda	k1gFnPc2	vražda
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
o	o	k7c4	o
10	[number]	k4	10
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
už	už	k9	už
1117	[number]	k4	1117
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
rekordních	rekordní	k2eAgInPc2d1	rekordní
2262	[number]	k4	2262
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
začala	začít	k5eAaPmAgFnS	začít
kriminalita	kriminalita	k1gFnSc1	kriminalita
klesat	klesat	k5eAaImF	klesat
trendem	trend	k1gInSc7	trend
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
trvá	trvat	k5eAaImIp3nS	trvat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
údajů	údaj	k1gInPc2	údaj
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
je	být	k5eAaImIp3nS	být
New	New	k1gFnSc4	New
York	York	k1gInSc1	York
mezi	mezi	k7c7	mezi
deseti	deset	k4xCc7	deset
městy	město	k1gNnPc7	město
USA	USA	kA	USA
s	s	k7c7	s
nejnižší	nízký	k2eAgFnSc7d3	nejnižší
kriminalitou	kriminalita	k1gFnSc7	kriminalita
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Manhattanu	Manhattan	k1gInSc6	Manhattan
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
zaznamenáno	zaznamenat	k5eAaPmNgNnS	zaznamenat
503	[number]	k4	503
případů	případ	k1gInPc2	případ
vraždy	vražda	k1gFnSc2	vražda
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
bylo	být	k5eAaImAgNnS	být
vražd	vražda	k1gFnPc2	vražda
zaznamenáno	zaznamenat	k5eAaPmNgNnS	zaznamenat
pouze	pouze	k6eAd1	pouze
62	[number]	k4	62
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
značí	značit	k5eAaImIp3nS	značit
pokles	pokles	k1gInSc4	pokles
přibližně	přibližně	k6eAd1	přibližně
o	o	k7c4	o
88	[number]	k4	88
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
vloupání	vloupání	k1gNnSc2	vloupání
byl	být	k5eAaImAgInS	být
pokles	pokles	k1gInSc4	pokles
asi	asi	k9	asi
o	o	k7c4	o
80	[number]	k4	80
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
u	u	k7c2	u
krádeží	krádež	k1gFnPc2	krádež
aut	auto	k1gNnPc2	auto
až	až	k9	až
o	o	k7c4	o
93	[number]	k4	93
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Nemálo	málo	k6eNd1	málo
přispěli	přispět	k5eAaPmAgMnP	přispět
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
města	město	k1gNnSc2	město
a	a	k8xC	a
potírání	potírání	k1gNnSc2	potírání
kriminálních	kriminální	k2eAgInPc2d1	kriminální
případů	případ	k1gInPc2	případ
i	i	k8xC	i
starostové	starostová	k1gFnSc3	starostová
Rudy	ruda	k1gFnSc2	ruda
Giuliani	Giulian	k1gMnPc1	Giulian
a	a	k8xC	a
Michael	Michael	k1gMnSc1	Michael
Bloomberg	Bloomberg	k1gMnSc1	Bloomberg
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
zasadili	zasadit	k5eAaPmAgMnP	zasadit
o	o	k7c4	o
zvýšení	zvýšení	k1gNnSc4	zvýšení
vlivu	vliv	k1gInSc2	vliv
policie	policie	k1gFnSc2	policie
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
zpřísnění	zpřísnění	k1gNnSc1	zpřísnění
trestů	trest	k1gInPc2	trest
a	a	k8xC	a
přísnějšímu	přísný	k2eAgInSc3d2	přísnější
přístupu	přístup	k1gInSc3	přístup
ke	k	k7c3	k
zločinu	zločin	k1gInSc3	zločin
obecně	obecně	k6eAd1	obecně
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Manhattanu	Manhattan	k1gInSc6	Manhattan
je	být	k5eAaImIp3nS	být
situováno	situován	k2eAgNnSc1d1	situováno
obrovské	obrovský	k2eAgNnSc1d1	obrovské
množství	množství	k1gNnSc1	množství
nejcennějších	cenný	k2eAgFnPc2d3	nejcennější
nemovitostí	nemovitost	k1gFnPc2	nemovitost
světa	svět	k1gInSc2	svět
a	a	k8xC	a
oblast	oblast	k1gFnSc4	oblast
má	mít	k5eAaImIp3nS	mít
pověst	pověst	k1gFnSc1	pověst
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
nejdražších	drahý	k2eAgFnPc2d3	nejdražší
oblastí	oblast	k1gFnPc2	oblast
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
dostupných	dostupný	k2eAgInPc2d1	dostupný
údajů	údaj	k1gInPc2	údaj
přibližně	přibližně	k6eAd1	přibližně
dvě	dva	k4xCgFnPc1	dva
třetiny	třetina	k1gFnPc1	třetina
všech	všecek	k3xTgMnPc2	všecek
zaměstnaneckých	zaměstnanecký	k2eAgMnPc2d1	zaměstnanecký
poměrů	poměr	k1gInPc2	poměr
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
spočívají	spočívat	k5eAaImIp3nP	spočívat
právě	právě	k9	právě
na	na	k7c6	na
Manhattanu	Manhattan	k1gInSc6	Manhattan
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitějším	důležitý	k2eAgInSc7d3	nejdůležitější
hospodářským	hospodářský	k2eAgInSc7d1	hospodářský
sektorem	sektor	k1gInSc7	sektor
na	na	k7c6	na
Manhattanu	Manhattan	k1gInSc6	Manhattan
je	být	k5eAaImIp3nS	být
finanční	finanční	k2eAgInSc1d1	finanční
průmysl	průmysl	k1gInSc1	průmysl
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
pracuje	pracovat	k5eAaImIp3nS	pracovat
necelých	celý	k2eNgInPc2d1	necelý
280	[number]	k4	280
tisíc	tisíc	k4xCgInPc2	tisíc
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc4	jejich
příjmy	příjem	k1gInPc4	příjem
tvoří	tvořit	k5eAaImIp3nP	tvořit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
polovinu	polovina	k1gFnSc4	polovina
všech	všecek	k3xTgFnPc2	všecek
vyplacených	vyplacený	k2eAgFnPc2d1	vyplacená
mezd	mzda	k1gFnPc2	mzda
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
čtvrti	čtvrt	k1gFnSc6	čtvrt
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
měsíční	měsíční	k2eAgInSc1d1	měsíční
plat	plat	k1gInSc1	plat
ve	v	k7c6	v
finančním	finanční	k2eAgInSc6d1	finanční
sektoru	sektor	k1gInSc6	sektor
na	na	k7c6	na
Manhattanu	Manhattan	k1gInSc6	Manhattan
je	být	k5eAaImIp3nS	být
8.300	[number]	k4	8.300
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
v	v	k7c6	v
ostatních	ostatní	k2eAgInPc6d1	ostatní
oborech	obor	k1gInPc6	obor
zhruba	zhruba	k6eAd1	zhruba
2.500	[number]	k4	2.500
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
polovina	polovina	k1gFnSc1	polovina
finančního	finanční	k2eAgInSc2d1	finanční
průmyslu	průmysl	k1gInSc2	průmysl
je	být	k5eAaImIp3nS	být
situována	situovat	k5eAaBmNgFnS	situovat
na	na	k7c4	na
Wall	Wall	k1gInSc4	Wall
Street	Streeta	k1gFnPc2	Streeta
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
světovým	světový	k2eAgMnSc7d1	světový
centrem	centr	k1gMnSc7	centr
burzy	burza	k1gFnSc2	burza
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Manhattanu	Manhattan	k1gInSc6	Manhattan
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
48	[number]	k4	48
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
m	m	kA	m
<g/>
2	[number]	k4	2
kancelářských	kancelářský	k2eAgFnPc2d1	kancelářská
prostor	prostora	k1gFnPc2	prostora
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
dělá	dělat	k5eAaImIp3nS	dělat
největší	veliký	k2eAgFnSc4d3	veliký
kancelářskou	kancelářský	k2eAgFnSc4d1	kancelářská
čtvrť	čtvrť	k1gFnSc4	čtvrť
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Manhattanu	Manhattan	k1gInSc6	Manhattan
sídlí	sídlet	k5eAaImIp3nS	sídlet
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
významných	významný	k2eAgFnPc2d1	významná
firem	firma	k1gFnPc2	firma
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
těch	ten	k3xDgFnPc2	ten
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
akcie	akcie	k1gFnPc1	akcie
se	se	k3xPyFc4	se
obchodují	obchodovat	k5eAaImIp3nP	obchodovat
na	na	k7c6	na
burze	burza	k1gFnSc6	burza
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Downtownu	Downtown	k1gInSc6	Downtown
sídlí	sídlet	k5eAaImIp3nS	sídlet
např.	např.	kA	např.
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
Stock	Stock	k1gMnSc1	Stock
Exchange	Exchange	k1gFnSc1	Exchange
<g/>
,	,	kIx,	,
American	American	k1gMnSc1	American
Stock	Stock	k1gMnSc1	Stock
Exchange	Exchange	k1gFnSc1	Exchange
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
Board	Board	k1gMnSc1	Board
of	of	k?	of
Trade	Trad	k1gMnSc5	Trad
<g/>
,	,	kIx,	,
NASDAQ	NASDAQ	kA	NASDAQ
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Sedm	sedm	k4xCc1	sedm
z	z	k7c2	z
osmi	osm	k4xCc2	osm
největších	veliký	k2eAgFnPc2d3	veliký
reklamních	reklamní	k2eAgFnPc2d1	reklamní
agentur	agentura	k1gFnPc2	agentura
na	na	k7c6	na
světě	svět	k1gInSc6	svět
sídlí	sídlet	k5eAaImIp3nS	sídlet
na	na	k7c6	na
Manhattanu	Manhattan	k1gInSc6	Manhattan
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
centrum	centrum	k1gNnSc4	centrum
reklamy	reklama	k1gFnSc2	reklama
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
považována	považován	k2eAgFnSc1d1	považována
Madison	Madisona	k1gFnPc2	Madisona
Avenue	avenue	k1gFnPc4	avenue
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
a	a	k8xC	a
především	především	k6eAd1	především
pak	pak	k6eAd1	pak
Manhattan	Manhattan	k1gInSc4	Manhattan
jsou	být	k5eAaImIp3nP	být
světovým	světový	k2eAgInSc7d1	světový
centrem	centr	k1gInSc7	centr
televize	televize	k1gFnSc2	televize
<g/>
,	,	kIx,	,
filmu	film	k1gInSc2	film
<g/>
,	,	kIx,	,
reklamy	reklama	k1gFnSc2	reklama
a	a	k8xC	a
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgNnSc7d1	významné
odvětvím	odvětví	k1gNnSc7	odvětví
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
novinová	novinový	k2eAgNnPc1d1	novinové
nakladatelství	nakladatelství	k1gNnPc1	nakladatelství
<g/>
.	.	kIx.	.
</s>
<s>
Manhattan	Manhattan	k1gInSc1	Manhattan
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgInSc7d3	veliký
mediálním	mediální	k2eAgInSc7d1	mediální
trhem	trh	k1gInSc7	trh
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgFnPc4d3	nejvýznamnější
mediální	mediální	k2eAgFnPc4d1	mediální
skupiny	skupina	k1gFnPc4	skupina
patří	patřit	k5eAaImIp3nS	patřit
News	News	k1gInSc1	News
Corporation	Corporation	k1gInSc1	Corporation
<g/>
,	,	kIx,	,
Time	Time	k1gFnSc1	Time
Warner	Warner	k1gMnSc1	Warner
<g/>
,	,	kIx,	,
Hearst	Hearst	k1gMnSc1	Hearst
Corporation	Corporation	k1gInSc1	Corporation
a	a	k8xC	a
Viacom	Viacom	k1gInSc1	Viacom
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgMnPc1	tři
ze	z	k7c2	z
čtyř	čtyři	k4xCgInPc2	čtyři
největších	veliký	k2eAgInPc2d3	veliký
hudebních	hudební	k2eAgInPc2d1	hudební
labelů	label	k1gInPc2	label
sídlí	sídlet	k5eAaImIp3nS	sídlet
na	na	k7c6	na
Manhattanu	Manhattan	k1gInSc6	Manhattan
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
7	[number]	k4	7
z	z	k7c2	z
10	[number]	k4	10
největších	veliký	k2eAgFnPc2d3	veliký
reklamních	reklamní	k2eAgFnPc2d1	reklamní
agentur	agentura	k1gFnPc2	agentura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
se	se	k3xPyFc4	se
také	také	k9	také
točí	točit	k5eAaImIp3nS	točit
třetina	třetina	k1gFnSc1	třetina
všech	všecek	k3xTgInPc2	všecek
amerických	americký	k2eAgInPc2d1	americký
nezávislých	závislý	k2eNgInPc2d1	nezávislý
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
Manhattan	Manhattan	k1gInSc1	Manhattan
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
po	po	k7c6	po
Hollywoodu	Hollywood	k1gInSc2	Hollywood
nejčastější	častý	k2eAgFnSc7d3	nejčastější
lokalitou	lokalita	k1gFnSc7	lokalita
objevující	objevující	k2eAgFnSc7d1	objevující
se	se	k3xPyFc4	se
ve	v	k7c6	v
filmech	film	k1gInPc6	film
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc1	svůj
sídlo	sídlo	k1gNnSc1	sídlo
má	mít	k5eAaImIp3nS	mít
ve	v	k7c6	v
městě	město	k1gNnSc6	město
také	také	k9	také
200	[number]	k4	200
novin	novina	k1gFnPc2	novina
a	a	k8xC	a
350	[number]	k4	350
časopisů	časopis	k1gInPc2	časopis
<g/>
,	,	kIx,	,
knižní	knižní	k2eAgNnPc1d1	knižní
nakladatelství	nakladatelství	k1gNnPc1	nakladatelství
zaměstnávají	zaměstnávat	k5eAaImIp3nP	zaměstnávat
přes	přes	k7c4	přes
25	[number]	k4	25
tisíc	tisíc	k4xCgInPc2	tisíc
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgInPc4d3	nejvýznamnější
deníky	deník	k1gInPc4	deník
patří	patřit	k5eAaImIp3nS	patřit
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
Times	Times	k1gInSc1	Times
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
Daily	Daila	k1gFnSc2	Daila
News	Newsa	k1gFnPc2	Newsa
<g/>
,	,	kIx,	,
Wall	Walla	k1gFnPc2	Walla
Street	Streeta	k1gFnPc2	Streeta
Journal	Journal	k1gMnPc2	Journal
nebo	nebo	k8xC	nebo
New	New	k1gMnPc2	New
York	York	k1gInSc1	York
Post	posta	k1gFnPc2	posta
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
je	být	k5eAaImIp3nS	být
také	také	k9	také
centrem	centrum	k1gNnSc7	centrum
televizní	televizní	k2eAgFnSc2d1	televizní
tvorby	tvorba	k1gFnSc2	tvorba
-	-	kIx~	-
sídlí	sídlet	k5eAaImIp3nS	sídlet
zde	zde	k6eAd1	zde
HBO	HBO	kA	HBO
<g/>
,	,	kIx,	,
MTV	MTV	kA	MTV
<g/>
,	,	kIx,	,
ABC	ABC	kA	ABC
<g/>
,	,	kIx,	,
NBC	NBC	kA	NBC
<g/>
,	,	kIx,	,
CBS	CBS	kA	CBS
<g/>
,	,	kIx,	,
FOX	fox	k1gInSc4	fox
News	Newsa	k1gFnPc2	Newsa
nebo	nebo	k8xC	nebo
Comedy	Comeda	k1gMnSc2	Comeda
Central	Central	k1gMnSc2	Central
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
známé	známý	k2eAgInPc4d1	známý
filmy	film	k1gInPc4	film
a	a	k8xC	a
seriály	seriál	k1gInPc4	seriál
odehrávající	odehrávající	k2eAgInPc4d1	odehrávající
se	se	k3xPyFc4	se
na	na	k7c6	na
Manhattanu	Manhattan	k1gInSc6	Manhattan
lze	lze	k6eAd1	lze
zmínit	zmínit	k5eAaPmF	zmínit
Taxikář	taxikář	k1gMnSc1	taxikář
<g/>
,	,	kIx,	,
Já	já	k3xPp1nSc1	já
legenda	legenda	k1gFnSc1	legenda
<g/>
,	,	kIx,	,
Na	na	k7c4	na
sever	sever	k1gInSc4	sever
severozápadní	severozápadní	k2eAgFnSc7d1	severozápadní
linkou	linka	k1gFnSc7	linka
<g/>
,	,	kIx,	,
Wall	Wall	k1gMnSc1	Wall
Street	Street	k1gMnSc1	Street
<g/>
,	,	kIx,	,
Přátelé	přítel	k1gMnPc1	přítel
<g/>
,	,	kIx,	,
Kravaťáci	Kravaťák	k1gMnPc1	Kravaťák
<g/>
,	,	kIx,	,
Jak	jak	k8xS	jak
jsem	být	k5eAaImIp1nS	být
poznal	poznat	k5eAaPmAgInS	poznat
vaši	váš	k3xOp2gFnSc4	váš
matku	matka	k1gFnSc4	matka
<g/>
,	,	kIx,	,
Sex	sex	k1gInSc4	sex
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Manhattan	Manhattan	k1gInSc1	Manhattan
byl	být	k5eAaImAgInS	být
dějištěm	dějiště	k1gNnSc7	dějiště
mnoha	mnoho	k4c2	mnoho
významných	významný	k2eAgNnPc2d1	významné
kulturních	kulturní	k2eAgNnPc2d1	kulturní
hnutí	hnutí	k1gNnPc2	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
hnutí	hnutí	k1gNnSc1	hnutí
Harlem	Harlo	k1gNnSc7	Harlo
Renaissance	Renaissance	k1gFnSc2	Renaissance
<g/>
,	,	kIx,	,
od	od	k7c2	od
50	[number]	k4	50
<g/>
.	.	kIx.	.
do	do	k7c2	do
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
aktivní	aktivní	k2eAgNnSc1d1	aktivní
hnutí	hnutí	k1gNnSc1	hnutí
pop	pop	k1gMnSc1	pop
art	art	k?	art
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
je	být	k5eAaImIp3nS	být
spojeno	spojit	k5eAaPmNgNnS	spojit
především	především	k9	především
se	s	k7c7	s
slavným	slavný	k2eAgInSc7d1	slavný
Andym	Andym	k1gInSc1	Andym
Warholem	Warhol	k1gInSc7	Warhol
<g/>
.	.	kIx.	.
</s>
<s>
Kulturně	kulturně	k6eAd1	kulturně
velmi	velmi	k6eAd1	velmi
významnou	významný	k2eAgFnSc7d1	významná
čtvrtí	čtvrt	k1gFnSc7	čtvrt
je	být	k5eAaImIp3nS	být
Chelsea	Chelsea	k1gFnSc1	Chelsea
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
pro	pro	k7c4	pro
řadu	řada	k1gFnSc4	řada
muzeí	muzeum	k1gNnPc2	muzeum
<g/>
,	,	kIx,	,
galerií	galerie	k1gFnPc2	galerie
a	a	k8xC	a
kulturních	kulturní	k2eAgFnPc2d1	kulturní
akcí	akce	k1gFnPc2	akce
<g/>
.	.	kIx.	.
</s>
<s>
Patrně	patrně	k6eAd1	patrně
nejznámějším	známý	k2eAgNnSc7d3	nejznámější
kulturním	kulturní	k2eAgNnSc7d1	kulturní
místem	místo	k1gNnSc7	místo
na	na	k7c6	na
Manhattanu	Manhattan	k1gInSc6	Manhattan
a	a	k8xC	a
možná	možná	k9	možná
i	i	k9	i
na	na	k7c6	na
světě	svět	k1gInSc6	svět
je	být	k5eAaImIp3nS	být
ulice	ulice	k1gFnSc1	ulice
Broadway	Broadwaa	k1gFnSc2	Broadwaa
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xC	jako
dějiště	dějiště	k1gNnSc1	dějiště
mnoha	mnoho	k4c2	mnoho
nejvýznamnějších	významný	k2eAgNnPc2d3	nejvýznamnější
divadelních	divadelní	k2eAgNnPc2d1	divadelní
představení	představení	k1gNnPc2	představení
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
z	z	k7c2	z
39	[number]	k4	39
největších	veliký	k2eAgNnPc2d3	veliký
divadel	divadlo	k1gNnPc2	divadlo
je	být	k5eAaImIp3nS	být
situována	situovat	k5eAaBmNgFnS	situovat
okolo	okolo	k7c2	okolo
Times	Timesa	k1gFnPc2	Timesa
Square	square	k1gInSc1	square
<g/>
,	,	kIx,	,
kousek	kousek	k1gInSc1	kousek
od	od	k7c2	od
tohoto	tento	k3xDgNnSc2	tento
náměstí	náměstí	k1gNnSc2	náměstí
sídlí	sídlet	k5eAaImIp3nS	sídlet
také	také	k9	také
Lincoln	Lincoln	k1gMnSc1	Lincoln
Center	centrum	k1gNnPc2	centrum
s	s	k7c7	s
Metropolitan	metropolitan	k1gInSc1	metropolitan
Opera	opera	k1gFnSc1	opera
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
Philharmonic	Philharmonice	k1gFnPc2	Philharmonice
<g/>
,	,	kIx,	,
New	New	k1gFnPc1	New
York	York	k1gInSc1	York
City	City	k1gFnSc2	City
Ballet	Ballet	k1gInSc1	Ballet
a	a	k8xC	a
dalšími	další	k2eAgMnPc7d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Manhattan	Manhattan	k1gInSc1	Manhattan
je	být	k5eAaImIp3nS	být
také	také	k9	také
domovem	domov	k1gInSc7	domov
světově	světově	k6eAd1	světově
známých	známý	k2eAgNnPc2d1	známé
muzeí	muzeum	k1gNnPc2	muzeum
a	a	k8xC	a
výstav	výstava	k1gFnPc2	výstava
jako	jako	k8xS	jako
MoMA	MoMA	k1gFnPc2	MoMA
<g/>
,	,	kIx,	,
Whitney	Whitnea	k1gFnPc1	Whitnea
Museum	museum	k1gNnSc1	museum
of	of	k?	of
American	American	k1gMnSc1	American
Art	Art	k1gMnSc1	Art
<g/>
,	,	kIx,	,
Guggenheim	Guggenheim	k1gMnSc1	Guggenheim
Museum	museum	k1gNnSc1	museum
<g/>
,	,	kIx,	,
Metropolitan	metropolitan	k1gInSc1	metropolitan
Museum	museum	k1gNnSc1	museum
of	of	k?	of
Art	Art	k1gMnSc1	Art
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
významnými	významný	k2eAgFnPc7d1	významná
kulturními	kulturní	k2eAgFnPc7d1	kulturní
místy	místo	k1gNnPc7	místo
jsou	být	k5eAaImIp3nP	být
Carnegie	Carnegie	k1gFnSc2	Carnegie
Hall	Halla	k1gFnPc2	Halla
nebo	nebo	k8xC	nebo
Radio	radio	k1gNnSc4	radio
City	City	k1gFnSc2	City
Music	Music	k1gMnSc1	Music
Hall	Hall	k1gMnSc1	Hall
<g/>
.	.	kIx.	.
</s>
<s>
Turistika	turistika	k1gFnSc1	turistika
je	být	k5eAaImIp3nS	být
významným	významný	k2eAgInSc7d1	významný
zdrojem	zdroj	k1gInSc7	zdroj
příjmů	příjem	k1gInPc2	příjem
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
navštíví	navštívit	k5eAaPmIp3nS	navštívit
přibližně	přibližně	k6eAd1	přibližně
40	[number]	k4	40
milionů	milion	k4xCgInPc2	milion
turistů	turist	k1gMnPc2	turist
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Newyorská	newyorský	k2eAgFnSc1d1	newyorská
gastronomie	gastronomie	k1gFnSc1	gastronomie
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
imigranty	imigrant	k1gMnPc7	imigrant
<g/>
.	.	kIx.	.
</s>
<s>
Židé	Žid	k1gMnPc1	Žid
a	a	k8xC	a
Italové	Ital	k1gMnPc1	Ital
proslavili	proslavit	k5eAaPmAgMnP	proslavit
pečivo	pečivo	k1gNnSc4	pečivo
<g/>
,	,	kIx,	,
tvarohové	tvarohový	k2eAgInPc4d1	tvarohový
koláče	koláč	k1gInPc4	koláč
a	a	k8xC	a
newyorskou	newyorský	k2eAgFnSc4d1	newyorská
pizzu	pizza	k1gFnSc4	pizza
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
pouličních	pouliční	k2eAgMnPc2d1	pouliční
prodejců	prodejce	k1gMnPc2	prodejce
prodává	prodávat	k5eAaImIp3nS	prodávat
falafel	falafel	k1gInSc4	falafel
a	a	k8xC	a
kebab	kebab	k1gInSc4	kebab
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
původem	původ	k1gInSc7	původ
ze	z	k7c2	z
středního	střední	k2eAgInSc2d1	střední
východu	východ	k1gInSc2	východ
<g/>
,	,	kIx,	,
nejpopulárnějšími	populární	k2eAgMnPc7d3	nejpopulárnější
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
hot	hot	k0	hot
dogy	doga	k1gFnPc4	doga
a	a	k8xC	a
preclíky	preclík	k1gInPc4	preclík
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Manhattanu	Manhattan	k1gInSc6	Manhattan
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
také	také	k9	také
řada	řada	k1gFnSc1	řada
významných	významný	k2eAgFnPc2d1	významná
amerických	americký	k2eAgFnPc2d1	americká
restaurací	restaurace	k1gFnPc2	restaurace
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Manhattanu	Manhattan	k1gInSc6	Manhattan
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
parků	park	k1gInPc2	park
i	i	k8xC	i
pláží	pláž	k1gFnPc2	pláž
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
hojně	hojně	k6eAd1	hojně
navštěvované	navštěvovaný	k2eAgFnPc1d1	navštěvovaná
<g/>
.	.	kIx.	.
</s>
<s>
Nejoblíbenějším	oblíbený	k2eAgInSc7d3	nejoblíbenější
parkem	park	k1gInSc7	park
je	být	k5eAaImIp3nS	být
Central	Central	k1gFnSc1	Central
Park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
ročně	ročně	k6eAd1	ročně
navštíví	navštívit	k5eAaPmIp3nS	navštívit
desítky	desítka	k1gFnPc1	desítka
milionů	milion	k4xCgInPc2	milion
turistů	turist	k1gMnPc2	turist
<g/>
.	.	kIx.	.
</s>
<s>
Manhattan	Manhattan	k1gInSc1	Manhattan
je	být	k5eAaImIp3nS	být
domovem	domov	k1gInSc7	domov
týmů	tým	k1gInPc2	tým
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
Knicks	Knicks	k1gInSc1	Knicks
(	(	kIx(	(
<g/>
NBA	NBA	kA	NBA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
Rangers	Rangers	k1gInSc1	Rangers
(	(	kIx(	(
<g/>
NHL	NHL	kA	NHL
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
Liberty	Libert	k1gInPc1	Libert
(	(	kIx(	(
<g/>
WNBA	WNBA	kA	WNBA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
hrají	hrát	k5eAaImIp3nP	hrát
své	svůj	k3xOyFgMnPc4	svůj
domácí	domácí	k1gMnPc4	domácí
zápasy	zápas	k1gInPc4	zápas
v	v	k7c4	v
Madison	Madison	k1gInSc4	Madison
Square	square	k1gInSc4	square
Garden	Gardna	k1gFnPc2	Gardna
<g/>
.	.	kIx.	.
</s>
<s>
Manhattan	Manhattan	k1gInSc1	Manhattan
je	být	k5eAaImIp3nS	být
jedinou	jediný	k2eAgFnSc7d1	jediná
čtvrtí	čtvrt	k1gFnSc7	čtvrt
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
nemá	mít	k5eNaImIp3nS	mít
svůj	svůj	k3xOyFgInSc4	svůj
domácí	domácí	k2eAgInSc4d1	domácí
baseballový	baseballový	k2eAgInSc4d1	baseballový
tým	tým	k1gInSc4	tým
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Manhattanu	Manhattan	k1gInSc6	Manhattan
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
sportovních	sportovní	k2eAgFnPc2d1	sportovní
hal	hala	k1gFnPc2	hala
a	a	k8xC	a
hřišť	hřiště	k1gNnPc2	hřiště
různých	různý	k2eAgInPc2d1	různý
typů	typ	k1gInPc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
v	v	k7c6	v
Central	Central	k1gFnSc6	Central
Parku	park	k1gInSc2	park
je	být	k5eAaImIp3nS	být
desítky	desítka	k1gFnPc4	desítka
tenisových	tenisový	k2eAgInPc2d1	tenisový
kurtů	kurt	k1gInPc2	kurt
<g/>
,	,	kIx,	,
několik	několik	k4yIc4	několik
golfových	golfový	k2eAgNnPc2d1	golfové
hřišť	hřiště	k1gNnPc2	hřiště
<g/>
,	,	kIx,	,
fotbalová	fotbalový	k2eAgNnPc1d1	fotbalové
hřiště	hřiště	k1gNnPc1	hřiště
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Sportovní	sportovní	k2eAgNnSc1d1	sportovní
využití	využití	k1gNnSc1	využití
na	na	k7c6	na
Manhattanu	Manhattan	k1gInSc6	Manhattan
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
rozmanité	rozmanitý	k2eAgNnSc1d1	rozmanité
<g/>
.	.	kIx.	.
</s>
<s>
Manhattan	Manhattan	k1gInSc1	Manhattan
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
obrovské	obrovský	k2eAgNnSc4d1	obrovské
množství	množství	k1gNnSc4	množství
veřejných	veřejný	k2eAgFnPc2d1	veřejná
i	i	k8xC	i
soukromých	soukromý	k2eAgFnPc2d1	soukromá
institucí	instituce	k1gFnPc2	instituce
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgMnPc4d3	nejznámější
veřejné	veřejný	k2eAgMnPc4d1	veřejný
střední	střední	k1gMnPc4	střední
školy	škola	k1gFnSc2	škola
patří	patřit	k5eAaImIp3nS	patřit
Beacon	Beacon	k1gNnSc1	Beacon
High	Higha	k1gFnPc2	Higha
School	Schoola	k1gFnPc2	Schoola
<g/>
,	,	kIx,	,
Stuyvesant	Stuyvesanta	k1gFnPc2	Stuyvesanta
High	Higha	k1gFnPc2	Higha
School	School	k1gInSc1	School
<g/>
,	,	kIx,	,
High	High	k1gInSc1	High
School	School	k1gInSc1	School
of	of	k?	of
Fashion	Fashion	k1gInSc1	Fashion
Industries	Industries	k1gMnSc1	Industries
nebo	nebo	k8xC	nebo
Bard	bard	k1gMnSc1	bard
High	Higha	k1gFnPc2	Higha
School	Schoola	k1gFnPc2	Schoola
<g/>
.	.	kIx.	.
</s>
<s>
Manhattan	Manhattan	k1gInSc1	Manhattan
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
jedinou	jediný	k2eAgFnSc4d1	jediná
oficiální	oficiální	k2eAgFnSc4d1	oficiální
italskou	italský	k2eAgFnSc4d1	italská
školu	škola	k1gFnSc4	škola
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
La	la	k1gNnSc4	la
Scuola	Scuola	k1gFnSc1	Scuola
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Italia	Italius	k1gMnSc2	Italius
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
Manattanu	Manattan	k1gInSc6	Manattan
situovány	situován	k2eAgFnPc1d1	situována
i	i	k8xC	i
soukromé	soukromý	k2eAgFnPc1d1	soukromá
školy	škola	k1gFnPc1	škola
jako	jako	k8xS	jako
Brearley	Brearlea	k1gFnPc1	Brearlea
School	School	k1gInSc1	School
<g/>
,	,	kIx,	,
Dalton	Dalton	k1gInSc1	Dalton
School	School	k1gInSc1	School
<g/>
,	,	kIx,	,
St.	st.	kA	st.
David	David	k1gMnSc1	David
School	School	k1gInSc1	School
nebo	nebo	k8xC	nebo
Loyola	Loyola	k1gFnSc1	Loyola
School	Schoola	k1gFnPc2	Schoola
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgFnPc4d3	nejznámější
vysoké	vysoký	k2eAgFnPc4d1	vysoká
školy	škola	k1gFnPc4	škola
a	a	k8xC	a
univerzity	univerzita	k1gFnPc4	univerzita
umístěné	umístěný	k2eAgFnPc4d1	umístěná
na	na	k7c6	na
Manhattanu	Manhattan	k1gInSc6	Manhattan
patří	patřit	k5eAaImIp3nS	patřit
především	především	k9	především
Columbia	Columbia	k1gFnSc1	Columbia
University	universita	k1gFnSc2	universita
<g/>
,	,	kIx,	,
Fordham	Fordham	k1gInSc4	Fordham
College	Colleg	k1gFnSc2	Colleg
<g/>
,	,	kIx,	,
Cooper	Cooper	k1gMnSc1	Cooper
Union	union	k1gInSc1	union
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
University	universita	k1gFnSc2	universita
<g/>
,	,	kIx,	,
Berkeley	Berkele	k2eAgFnPc1d1	Berkele
College	Colleg	k1gFnPc1	Colleg
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
52	[number]	k4	52
<g/>
%	%	kIx~	%
obyvatel	obyvatel	k1gMnSc1	obyvatel
Manhattanu	Manhattan	k1gInSc2	Manhattan
nad	nad	k7c4	nad
25	[number]	k4	25
let	léto	k1gNnPc2	léto
má	mít	k5eAaImIp3nS	mít
minimálně	minimálně	k6eAd1	minimálně
bakalářský	bakalářský	k2eAgInSc1d1	bakalářský
titul	titul	k1gInSc1	titul
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
pátá	pátý	k4xOgFnSc1	pátý
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
statistika	statistika	k1gFnSc1	statistika
v	v	k7c6	v
celých	celá	k1gFnPc6	celá
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nejvzdělanějších	vzdělaný	k2eAgInPc2d3	nejvzdělanější
krajů	kraj	k1gInPc2	kraj
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Manhattan	Manhattan	k1gInSc1	Manhattan
je	být	k5eAaImIp3nS	být
významným	významný	k2eAgInSc7d1	významný
světovým	světový	k2eAgInSc7d1	světový
centrem	centr	k1gInSc7	centr
v	v	k7c6	v
lékařském	lékařský	k2eAgNnSc6d1	lékařské
vzdělávání	vzdělávání	k1gNnSc6	vzdělávání
a	a	k8xC	a
přírodních	přírodní	k2eAgFnPc6d1	přírodní
vědách	věda	k1gFnPc6	věda
<g/>
.	.	kIx.	.
</s>
<s>
Manhattan	Manhattan	k1gInSc1	Manhattan
přijímá	přijímat	k5eAaImIp3nS	přijímat
druhou	druhý	k4xOgFnSc4	druhý
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
částku	částka	k1gFnSc4	částka
z	z	k7c2	z
fondu	fond	k1gInSc2	fond
National	National	k1gMnSc1	National
Institutes	Institutes	k1gMnSc1	Institutes
of	of	k?	of
Health	Health	k1gInSc1	Health
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
výzkumu	výzkum	k1gInSc2	výzkum
a	a	k8xC	a
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
<g/>
.	.	kIx.	.
</s>
<s>
Manhattan	Manhattan	k1gInSc1	Manhattan
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
chlubit	chlubit	k5eAaImF	chlubit
také	také	k9	také
největším	veliký	k2eAgInSc7d3	veliký
knihovnickým	knihovnický	k2eAgInSc7d1	knihovnický
systémem	systém	k1gInSc7	systém
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dopravní	dopravní	k2eAgFnSc6d1	dopravní
stránce	stránka	k1gFnSc6	stránka
je	být	k5eAaImIp3nS	být
Manhattan	Manhattan	k1gInSc1	Manhattan
na	na	k7c6	na
velmi	velmi	k6eAd1	velmi
vysoké	vysoký	k2eAgFnSc6d1	vysoká
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
komplexní	komplexní	k2eAgInPc4d1	komplexní
propracované	propracovaný	k2eAgInPc4d1	propracovaný
systémy	systém	k1gInPc4	systém
<g/>
,	,	kIx,	,
od	od	k7c2	od
silniční	silniční	k2eAgFnSc2d1	silniční
infrastruktury	infrastruktura	k1gFnSc2	infrastruktura
až	až	k9	až
po	po	k7c4	po
metro	metro	k1gNnSc4	metro
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgMnSc7d3	veliký
podzemním	podzemní	k2eAgInSc7d1	podzemní
dopravním	dopravní	k2eAgInSc7d1	dopravní
komplexem	komplex	k1gInSc7	komplex
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Zdaleka	zdaleka	k6eAd1	zdaleka
nejoblíbenějším	oblíbený	k2eAgInSc7d3	nejoblíbenější
způsobem	způsob	k1gInSc7	způsob
přepravy	přeprava	k1gFnSc2	přeprava
po	po	k7c6	po
Manhattanu	Manhattan	k1gInSc6	Manhattan
je	být	k5eAaImIp3nS	být
právě	právě	k9	právě
metro	metro	k1gNnSc1	metro
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
silnice	silnice	k1gFnPc1	silnice
jsou	být	k5eAaImIp3nP	být
prakticky	prakticky	k6eAd1	prakticky
permanentně	permanentně	k6eAd1	permanentně
zacpané	zacpaný	k2eAgNnSc1d1	zacpané
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
podzemní	podzemní	k2eAgFnSc1d1	podzemní
dráha	dráha	k1gFnSc1	dráha
tou	ten	k3xDgFnSc7	ten
nejrychlejší	rychlý	k2eAgFnSc7d3	nejrychlejší
možností	možnost	k1gFnSc7	možnost
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
někam	někam	k6eAd1	někam
po	po	k7c6	po
Manhattanu	Manhattan	k1gInSc6	Manhattan
dopravit	dopravit	k5eAaPmF	dopravit
<g/>
.	.	kIx.	.
</s>
<s>
Dopravu	doprava	k1gFnSc4	doprava
metrem	metro	k1gNnSc7	metro
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
téměř	téměř	k6eAd1	téměř
500	[number]	k4	500
stanic	stanice	k1gFnPc2	stanice
rozdělených	rozdělená	k1gFnPc2	rozdělená
do	do	k7c2	do
12	[number]	k4	12
tras	trasa	k1gFnPc2	trasa
<g/>
.	.	kIx.	.
</s>
<s>
Možností	možnost	k1gFnSc7	možnost
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
dostat	dostat	k5eAaPmF	dostat
na	na	k7c4	na
Manhattan	Manhattan	k1gInSc4	Manhattan
či	či	k8xC	či
z	z	k7c2	z
Manhattanu	Manhattan	k1gInSc2	Manhattan
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
také	také	k9	také
vlak	vlak	k1gInSc1	vlak
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
slavný	slavný	k2eAgInSc1d1	slavný
vlakový	vlakový	k2eAgInSc1d1	vlakový
terminál	terminál	k1gInSc1	terminál
Grand	grand	k1gMnSc1	grand
Central	Central	k1gMnSc1	Central
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejvytíženějších	vytížený	k2eAgInPc2d3	nejvytíženější
vlakových	vlakový	k2eAgInPc2d1	vlakový
terminálů	terminál	k1gInPc2	terminál
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Letištní	letištní	k2eAgFnSc4d1	letištní
dopravu	doprava	k1gFnSc4	doprava
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
letiště	letiště	k1gNnSc4	letiště
v	v	k7c6	v
okolních	okolní	k2eAgFnPc6d1	okolní
čtvrtích	čtvrt	k1gFnPc6	čtvrt
<g/>
,	,	kIx,	,
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
Manhattanu	Manhattan	k1gInSc6	Manhattan
lze	lze	k6eAd1	lze
najít	najít	k5eAaPmF	najít
několik	několik	k4yIc1	několik
malých	malý	k2eAgNnPc2d1	malé
letišť	letiště	k1gNnPc2	letiště
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
však	však	k9	však
nejsou	být	k5eNaImIp3nP	být
mezinárodní	mezinárodní	k2eAgFnPc1d1	mezinárodní
<g/>
.	.	kIx.	.
</s>
<s>
Samozřejmě	samozřejmě	k6eAd1	samozřejmě
i	i	k9	i
na	na	k7c6	na
Manhattanu	Manhattan	k1gInSc6	Manhattan
je	být	k5eAaImIp3nS	být
možnost	možnost	k1gFnSc4	možnost
přepravovat	přepravovat	k5eAaImF	přepravovat
se	se	k3xPyFc4	se
autobusy	autobus	k1gInPc7	autobus
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
zmíněné	zmíněný	k2eAgFnSc3d1	zmíněná
vytíženosti	vytíženost	k1gFnSc3	vytíženost
místních	místní	k2eAgFnPc2d1	místní
silnic	silnice	k1gFnPc2	silnice
není	být	k5eNaImIp3nS	být
tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
příliš	příliš	k6eAd1	příliš
oblíbeným	oblíbený	k2eAgInSc7d1	oblíbený
a	a	k8xC	a
rozsáhlým	rozsáhlý	k2eAgInSc7d1	rozsáhlý
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
autobusovým	autobusový	k2eAgInSc7d1	autobusový
terminálem	terminál	k1gInSc7	terminál
je	být	k5eAaImIp3nS	být
Port	port	k1gInSc4	port
Authority	Authorita	k1gFnSc2	Authorita
Bus	bus	k1gInSc1	bus
Terminal	Terminal	k1gMnSc1	Terminal
a	a	k8xC	a
Penn	Penn	k1gMnSc1	Penn
Station	station	k1gInSc1	station
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
i	i	k9	i
nejvytíženější	vytížený	k2eAgFnSc7d3	nejvytíženější
stanicí	stanice	k1gFnSc7	stanice
metra	metro	k1gNnSc2	metro
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Manhattanu	Manhattan	k1gInSc2	Manhattan
vede	vést	k5eAaImIp3nS	vést
také	také	k9	také
několik	několik	k4yIc1	několik
trajektových	trajektový	k2eAgFnPc2d1	trajektová
tras	trasa	k1gFnPc2	trasa
<g/>
,	,	kIx,	,
tunelů	tunel	k1gInPc2	tunel
a	a	k8xC	a
mostů	most	k1gInPc2	most
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
spojují	spojovat	k5eAaImIp3nP	spojovat
Manhattan	Manhattan	k1gInSc4	Manhattan
s	s	k7c7	s
okolními	okolní	k2eAgFnPc7d1	okolní
čtvrtěmi	čtvrt	k1gFnPc7	čtvrt
a	a	k8xC	a
především	především	k9	především
státem	stát	k1gInSc7	stát
New	New	k1gFnSc2	New
Jersey	Jersea	k1gFnSc2	Jersea
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
oblíbeným	oblíbený	k2eAgInSc7d1	oblíbený
způsobem	způsob	k1gInSc7	způsob
dopravy	doprava	k1gFnSc2	doprava
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
Manhattanu	Manhattan	k1gInSc6	Manhattan
také	také	k9	také
taxi	taxe	k1gFnSc4	taxe
<g/>
,	,	kIx,	,
kterých	který	k3yQgMnPc2	který
zde	zde	k6eAd1	zde
fungují	fungovat	k5eAaImIp3nP	fungovat
tisíce	tisíc	k4xCgInPc1	tisíc
a	a	k8xC	a
v	v	k7c4	v
každý	každý	k3xTgInSc4	každý
okamžik	okamžik	k1gInSc4	okamžik
lze	lze	k6eAd1	lze
chytit	chytit	k5eAaPmF	chytit
volný	volný	k2eAgInSc1d1	volný
taxi	taxe	k1gFnSc3	taxe
vůz	vůz	k1gInSc4	vůz
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
přepravit	přepravit	k5eAaPmF	přepravit
zákazníky	zákazník	k1gMnPc4	zákazník
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
New	New	k1gFnSc6	New
Yorku	York	k1gInSc6	York
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Manhattan	Manhattan	k1gInSc1	Manhattan
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
ZemeSveta	ZemeSveta	k1gFnSc1	ZemeSveta
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
-	-	kIx~	-
Vydání	vydání	k1gNnSc1	vydání
časopisu	časopis	k1gInSc2	časopis
Země	zem	k1gFnSc2	zem
světa	svět	k1gInSc2	svět
věnované	věnovaný	k2eAgFnSc3d1	věnovaná
New	New	k1gFnSc3	New
Yorku	York	k1gInSc2	York
NYC	NYC	kA	NYC
<g/>
.	.	kIx.	.
<g/>
gov	gov	k?	gov
-	-	kIx~	-
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
města	město	k1gNnSc2	město
NYCityMap	NYCityMap	k1gInSc1	NYCityMap
-	-	kIx~	-
Interaktivní	interaktivní	k2eAgFnSc1d1	interaktivní
mapa	mapa	k1gFnSc1	mapa
města	město	k1gNnSc2	město
i	i	k8xC	i
se	s	k7c7	s
sítí	síť	k1gFnSc7	síť
metra	metro	k1gNnSc2	metro
NYCvisit	NYCvisit	k1gInSc4	NYCvisit
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
-	-	kIx~	-
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
turistická	turistický	k2eAgFnSc1d1	turistická
prezentace	prezentace	k1gFnSc1	prezentace
města	město	k1gNnSc2	město
NewYorkonline	NewYorkonlin	k1gInSc5	NewYorkonlin
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
-	-	kIx~	-
O	o	k7c6	o
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
</s>
