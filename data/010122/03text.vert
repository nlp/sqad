<p>
<s>
Zentiva	Zentiva	k1gFnSc1	Zentiva
je	být	k5eAaImIp3nS	být
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
farmaceutická	farmaceutický	k2eAgFnSc1d1	farmaceutická
společnost	společnost	k1gFnSc1	společnost
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
<g/>
,	,	kIx,	,
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
a	a	k8xC	a
prodává	prodávat	k5eAaImIp3nS	prodávat
generické	generický	k2eAgInPc4d1	generický
produkty	produkt	k1gInPc4	produkt
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
do	do	k7c2	do
30	[number]	k4	30
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2018	[number]	k4	2018
byla	být	k5eAaImAgFnS	být
Zentiva	Zentiva	k1gFnSc1	Zentiva
součástí	součást	k1gFnPc2	součást
skupiny	skupina	k1gFnSc2	skupina
Sanofi	Sanof	k1gFnSc2	Sanof
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2018	[number]	k4	2018
začala	začít	k5eAaPmAgFnS	začít
Zentiva	Zentiva	k1gFnSc1	Zentiva
opět	opět	k6eAd1	opět
působit	působit	k5eAaImF	působit
jako	jako	k8xC	jako
samostatná	samostatný	k2eAgFnSc1d1	samostatná
společnost	společnost	k1gFnSc1	společnost
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
nového	nový	k2eAgMnSc2d1	nový
vlastníka	vlastník	k1gMnSc2	vlastník
–	–	k?	–
společnosti	společnost	k1gFnSc2	společnost
Advent	advent	k1gInSc4	advent
International	International	k1gFnSc2	International
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
Zentivu	Zentiva	k1gFnSc4	Zentiva
od	od	k7c2	od
Sanofi	Sanof	k1gFnSc2	Sanof
koupila	koupit	k5eAaPmAgFnS	koupit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
působí	působit	k5eAaImIp3nP	působit
společnosti	společnost	k1gFnPc1	společnost
Zentiva	Zentivum	k1gNnSc2	Zentivum
<g/>
,	,	kIx,	,
k.s.	k.s.	k?	k.s.
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Zentiva	Zentiva	k1gFnSc1	Zentiva
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
a	a	k8xC	a
Léčiva	léčivo	k1gNnSc2	léčivo
a.s.	a.s.	k?	a.s.
<g/>
)	)	kIx)	)
a	a	k8xC	a
Zentiva	Zentiva	k1gFnSc1	Zentiva
Group	Group	k1gInSc1	Group
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
S	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
2500	[number]	k4	2500
zaměstnanci	zaměstnanec	k1gMnPc7	zaměstnanec
a	a	k8xC	a
dvěma	dva	k4xCgInPc7	dva
výrobními	výrobní	k2eAgInPc7d1	výrobní
závody	závod	k1gInPc7	závod
–	–	k?	–
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
Bukurešti	Bukurešť	k1gFnSc6	Bukurešť
–	–	k?	–
je	být	k5eAaImIp3nS	být
cílem	cíl	k1gInSc7	cíl
samostatné	samostatný	k2eAgFnSc2d1	samostatná
Zentivy	Zentiva	k1gFnSc2	Zentiva
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
vůdčí	vůdčí	k2eAgFnSc4d1	vůdčí
pozici	pozice	k1gFnSc4	pozice
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
generik	generika	k1gFnPc2	generika
a	a	k8xC	a
volně	volně	k6eAd1	volně
prodejných	prodejný	k2eAgInPc2d1	prodejný
léků	lék	k1gInPc2	lék
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Portfolio	portfolio	k1gNnSc1	portfolio
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
tvoří	tvořit	k5eAaImIp3nS	tvořit
500	[number]	k4	500
produktů	produkt	k1gInPc2	produkt
a	a	k8xC	a
800	[number]	k4	800
lékových	lékový	k2eAgFnPc2d1	léková
forem	forma	k1gFnPc2	forma
<g/>
.	.	kIx.	.
</s>
<s>
Pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
širokou	široký	k2eAgFnSc4d1	široká
škálu	škála	k1gFnSc4	škála
terapeutických	terapeutický	k2eAgFnPc2d1	terapeutická
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
hlavní	hlavní	k2eAgFnPc4d1	hlavní
produktové	produktový	k2eAgFnPc4d1	produktová
skupiny	skupina	k1gFnPc4	skupina
patří	patřit	k5eAaImIp3nS	patřit
léky	lék	k1gInPc4	lék
proti	proti	k7c3	proti
kardiovaskulárním	kardiovaskulární	k2eAgFnPc3d1	kardiovaskulární
chorobám	choroba	k1gFnPc3	choroba
<g/>
,	,	kIx,	,
analgetika	analgetikum	k1gNnPc1	analgetikum
<g/>
,	,	kIx,	,
přípravky	přípravka	k1gFnPc1	přípravka
proti	proti	k7c3	proti
chorobám	choroba	k1gFnPc3	choroba
centrálního	centrální	k2eAgInSc2d1	centrální
nervového	nervový	k2eAgInSc2d1	nervový
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
a	a	k8xC	a
trávící	trávící	k2eAgFnSc2d1	trávící
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
ženská	ženský	k2eAgFnSc1d1	ženská
medicína	medicína	k1gFnSc1	medicína
a	a	k8xC	a
léky	lék	k1gInPc1	lék
proti	proti	k7c3	proti
chorobám	choroba	k1gFnPc3	choroba
močového	močový	k2eAgInSc2d1	močový
traktu	trakt	k1gInSc2	trakt
a	a	k8xC	a
pohlavní	pohlavní	k2eAgFnSc2d1	pohlavní
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
doplňků	doplněk	k1gInPc2	doplněk
stravy	strava	k1gFnSc2	strava
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
zejména	zejména	k9	zejména
o	o	k7c4	o
vitaminové	vitaminový	k2eAgInPc4d1	vitaminový
přípravky	přípravek	k1gInPc4	přípravek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Kořeny	kořen	k1gInPc1	kořen
největšího	veliký	k2eAgInSc2d3	veliký
českého	český	k2eAgInSc2d1	český
farmaceutického	farmaceutický	k2eAgInSc2d1	farmaceutický
podniku	podnik	k1gInSc2	podnik
lze	lze	k6eAd1	lze
vystopovat	vystopovat	k5eAaPmF	vystopovat
až	až	k9	až
k	k	k7c3	k
lékárně	lékárna	k1gFnSc3	lékárna
"	"	kIx"	"
<g/>
U	u	k7c2	u
Černého	Černý	k1gMnSc2	Černý
orla	orel	k1gMnSc2	orel
<g/>
"	"	kIx"	"
umístěné	umístěný	k2eAgFnSc2d1	umístěná
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
na	na	k7c6	na
pražské	pražský	k2eAgFnSc6d1	Pražská
Malé	Malé	k2eAgFnSc6d1	Malé
Straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1857	[number]	k4	1857
tuto	tento	k3xDgFnSc4	tento
lékárnu	lékárna	k1gFnSc4	lékárna
zakoupil	zakoupit	k5eAaPmAgMnS	zakoupit
Benjamin	Benjamin	k1gMnSc1	Benjamin
Fragner	Fragner	k1gMnSc1	Fragner
(	(	kIx(	(
<g/>
1824	[number]	k4	1824
<g/>
–	–	k?	–
<g/>
1886	[number]	k4	1886
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Plány	plán	k1gInPc1	plán
na	na	k7c4	na
její	její	k3xOp3gNnSc4	její
rozšíření	rozšíření	k1gNnSc4	rozšíření
realizoval	realizovat	k5eAaBmAgMnS	realizovat
až	až	k9	až
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
<g/>
,	,	kIx,	,
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Karel	Karel	k1gMnSc1	Karel
Fragner	Fragner	k1gMnSc1	Fragner
(	(	kIx(	(
<g/>
1861	[number]	k4	1861
<g/>
–	–	k?	–
<g/>
1926	[number]	k4	1926
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
další	další	k2eAgNnSc4d1	další
rozšíření	rozšíření	k1gNnSc4	rozšíření
podniku	podnik	k1gInSc2	podnik
se	se	k3xPyFc4	se
zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Ing.	ing.	kA	ing.
Jiří	Jiří	k1gMnSc1	Jiří
Fragner	Fragner	k1gMnSc1	Fragner
(	(	kIx(	(
<g/>
1900	[number]	k4	1900
<g/>
–	–	k?	–
<g/>
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
vybudoval	vybudovat	k5eAaPmAgMnS	vybudovat
za	za	k7c2	za
účasti	účast	k1gFnSc2	účast
svého	svůj	k3xOyFgMnSc2	svůj
bratra	bratr	k1gMnSc2	bratr
<g/>
,	,	kIx,	,
architekta	architekt	k1gMnSc2	architekt
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Fragnera	Fragner	k1gMnSc2	Fragner
<g/>
,	,	kIx,	,
novou	nový	k2eAgFnSc4d1	nová
moderní	moderní	k2eAgFnSc4d1	moderní
farmaceutickou	farmaceutický	k2eAgFnSc4d1	farmaceutická
továrnu	továrna	k1gFnSc4	továrna
ve	v	k7c6	v
vesnici	vesnice	k1gFnSc6	vesnice
Dolní	dolní	k2eAgFnSc2d1	dolní
Měcholupy	Měcholupa	k1gFnSc2	Měcholupa
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
součástí	součást	k1gFnSc7	součást
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
místě	místo	k1gNnSc6	místo
původní	původní	k2eAgFnSc2d1	původní
továrny	továrna	k1gFnSc2	továrna
se	se	k3xPyFc4	se
nyní	nyní	k6eAd1	nyní
nachází	nacházet	k5eAaImIp3nS	nacházet
hlavní	hlavní	k2eAgNnSc4d1	hlavní
sídlo	sídlo	k1gNnSc4	sídlo
společnosti	společnost	k1gFnSc2	společnost
Zentiva	Zentivum	k1gNnSc2	Zentivum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Továrna	továrna	k1gFnSc1	továrna
"	"	kIx"	"
<g/>
Benjamin	Benjamin	k1gMnSc1	Benjamin
Fragner	Fragner	k1gMnSc1	Fragner
<g/>
"	"	kIx"	"
zahájila	zahájit	k5eAaPmAgFnS	zahájit
provoz	provoz	k1gInSc4	provoz
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1930	[number]	k4	1930
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
profitabilita	profitabilita	k1gFnSc1	profitabilita
byla	být	k5eAaImAgFnS	být
postavena	postavit	k5eAaPmNgFnS	postavit
na	na	k7c6	na
solidním	solidní	k2eAgInSc6d1	solidní
základě	základ	k1gInSc6	základ
výroby	výroba	k1gFnSc2	výroba
účinných	účinný	k2eAgFnPc2d1	účinná
látek	látka	k1gFnPc2	látka
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
byl	být	k5eAaImAgMnS	být
rozvinut	rozvinout	k5eAaPmNgMnS	rozvinout
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
široký	široký	k2eAgInSc4d1	široký
výzkum	výzkum	k1gInSc4	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
po	po	k7c6	po
uzavření	uzavření	k1gNnSc6	uzavření
českých	český	k2eAgFnPc2d1	Česká
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
nacistické	nacistický	k2eAgFnSc2d1	nacistická
okupace	okupace	k1gFnSc2	okupace
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
podnik	podnik	k1gInSc1	podnik
stal	stát	k5eAaPmAgInS	stát
díky	díky	k7c3	díky
pochopení	pochopení	k1gNnSc3	pochopení
a	a	k8xC	a
podpoře	podpora	k1gFnSc3	podpora
svého	svůj	k3xOyFgMnSc2	svůj
majitele	majitel	k1gMnSc2	majitel
útočištěm	útočiště	k1gNnSc7	útočiště
mnoha	mnoho	k4c2	mnoho
význačných	význačný	k2eAgMnPc2d1	význačný
odborníků	odborník	k1gMnPc2	odborník
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
jiného	jiné	k1gNnSc2	jiné
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
jako	jako	k9	jako
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
zemí	zem	k1gFnPc2	zem
Evropy	Evropa	k1gFnSc2	Evropa
roku	rok	k1gInSc2	rok
1944	[number]	k4	1944
úspěšně	úspěšně	k6eAd1	úspěšně
izolován	izolován	k2eAgInSc1d1	izolován
penicilin	penicilin	k1gInSc1	penicilin
(	(	kIx(	(
<g/>
pod	pod	k7c7	pod
tehdejším	tehdejší	k2eAgNnSc7d1	tehdejší
označením	označení	k1gNnSc7	označení
BF	BF	kA	BF
Mykoin	Mykoin	k1gInSc4	Mykoin
510	[number]	k4	510
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
znárodnění	znárodnění	k1gNnSc6	znárodnění
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
oddělení	oddělení	k1gNnSc3	oddělení
továrny	továrna	k1gFnSc2	továrna
od	od	k7c2	od
lékárny	lékárna	k1gFnSc2	lékárna
<g/>
.	.	kIx.	.
</s>
<s>
Měcholupský	Měcholupský	k2eAgInSc1d1	Měcholupský
podnik	podnik	k1gInSc1	podnik
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
součástí	součást	k1gFnSc7	součást
Spojených	spojený	k2eAgInPc2d1	spojený
farmaceutických	farmaceutický	k2eAgInPc2d1	farmaceutický
závodů	závod	k1gInPc2	závod
(	(	kIx(	(
<g/>
SPOFA	SPOFA	kA	SPOFA
<g/>
)	)	kIx)	)
a	a	k8xC	a
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
750	[number]	k4	750
zaměstnanci	zaměstnanec	k1gMnPc7	zaměstnanec
byl	být	k5eAaImAgMnS	být
bezkonkurenčně	bezkonkurenčně	k6eAd1	bezkonkurenčně
vedoucím	vedoucí	k2eAgInSc7d1	vedoucí
závodem	závod	k1gInSc7	závod
farmaceutické	farmaceutický	k2eAgFnSc2d1	farmaceutická
výroby	výroba	k1gFnSc2	výroba
v	v	k7c6	v
poválečném	poválečný	k2eAgNnSc6d1	poválečné
Československu	Československo	k1gNnSc6	Československo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
neustále	neustále	k6eAd1	neustále
rostoucí	rostoucí	k2eAgFnSc3d1	rostoucí
potřebě	potřeba	k1gFnSc3	potřeba
konečných	konečný	k2eAgFnPc2d1	konečná
lékových	lékový	k2eAgFnPc2d1	léková
forem	forma	k1gFnPc2	forma
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
zvažována	zvažován	k2eAgFnSc1d1	zvažována
otázka	otázka	k1gFnSc1	otázka
masivního	masivní	k2eAgNnSc2d1	masivní
rozšíření	rozšíření	k1gNnSc2	rozšíření
výroby	výroba	k1gFnSc2	výroba
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
program	program	k1gInSc1	program
rozšíření	rozšíření	k1gNnSc2	rozšíření
byl	být	k5eAaImAgInS	být
dokončen	dokončit	k5eAaPmNgInS	dokončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
a	a	k8xC	a
vedl	vést	k5eAaImAgInS	vést
k	k	k7c3	k
výstavbě	výstavba	k1gFnSc3	výstavba
moderních	moderní	k2eAgInPc2d1	moderní
výrobních	výrobní	k2eAgInPc2d1	výrobní
provozů	provoz	k1gInPc2	provoz
s	s	k7c7	s
odpovídajícím	odpovídající	k2eAgNnSc7d1	odpovídající
zařízením	zařízení	k1gNnSc7	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
zvětšený	zvětšený	k2eAgInSc1d1	zvětšený
závod	závod	k1gInSc1	závod
měl	mít	k5eAaImAgInS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
několikanásobné	několikanásobný	k2eAgNnSc4d1	několikanásobné
zvýšení	zvýšení	k1gNnSc4	zvýšení
produkce	produkce	k1gFnSc2	produkce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Současnost	současnost	k1gFnSc4	současnost
==	==	k?	==
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
prošla	projít	k5eAaPmAgFnS	projít
společnost	společnost	k1gFnSc1	společnost
několika	několik	k4yIc2	několik
významnými	významný	k2eAgFnPc7d1	významná
organizačními	organizační	k2eAgFnPc7d1	organizační
a	a	k8xC	a
vlastnickými	vlastnický	k2eAgFnPc7d1	vlastnická
změnami	změna	k1gFnPc7	změna
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
dokončen	dokončit	k5eAaPmNgInS	dokončit
program	program	k1gInSc1	program
modernizace	modernizace	k1gFnSc2	modernizace
a	a	k8xC	a
vytvořeny	vytvořen	k2eAgFnPc4d1	vytvořena
podmínky	podmínka	k1gFnPc4	podmínka
pro	pro	k7c4	pro
regionální	regionální	k2eAgInSc4d1	regionální
růst	růst	k1gInSc4	růst
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
zaniká	zanikat	k5eAaImIp3nS	zanikat
název	název	k1gInSc4	název
Léčiva	léčivo	k1gNnSc2	léčivo
a.s.	a.s.	k?	a.s.
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
založena	založen	k2eAgFnSc1d1	založena
Zentiva	Zentiva	k1gFnSc1	Zentiva
<g/>
,	,	kIx,	,
k.s.	k.s.	k?	k.s.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
byla	být	k5eAaImAgFnS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
nová	nový	k2eAgFnSc1d1	nová
firemní	firemní	k2eAgFnSc1d1	firemní
značka	značka	k1gFnSc1	značka
Zentiva	Zentiva	k1gFnSc1	Zentiva
(	(	kIx(	(
<g/>
Zentiva	Zentiva	k1gFnSc1	Zentiva
CZ	CZ	kA	CZ
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
Zentiva	Zentiva	k1gFnSc1	Zentiva
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
začleněna	začleněn	k2eAgFnSc1d1	začleněna
společnost	společnost	k1gFnSc1	společnost
Slovakofarma	Slovakofarma	k1gFnSc1	Slovakofarma
(	(	kIx(	(
<g/>
vedoucí	vedoucí	k2eAgFnSc1d1	vedoucí
farmaceutická	farmaceutický	k2eAgFnSc1d1	farmaceutická
firma	firma	k1gFnSc1	firma
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
)	)	kIx)	)
a	a	k8xC	a
Zentiva	Zentiva	k1gFnSc1	Zentiva
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
svoje	svůj	k3xOyFgNnSc4	svůj
zastoupení	zastoupení	k1gNnSc4	zastoupení
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
a	a	k8xC	a
Polsku	Polsko	k1gNnSc6	Polsko
a	a	k8xC	a
na	na	k7c6	na
některých	některý	k3yIgInPc6	některý
dalších	další	k2eAgInPc6d1	další
trzích	trh	k1gInPc6	trh
regionu	region	k1gInSc2	region
střední	střední	k2eAgFnSc2d1	střední
a	a	k8xC	a
východní	východní	k2eAgFnSc2d1	východní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
se	se	k3xPyFc4	se
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
akvizice	akvizice	k1gFnSc1	akvizice
společnosti	společnost	k1gFnSc2	společnost
Sicomed	Sicomed	k1gInSc1	Sicomed
(	(	kIx(	(
<g/>
vedoucí	vedoucí	k2eAgFnSc1d1	vedoucí
rumunská	rumunský	k2eAgFnSc1d1	rumunská
generická	generický	k2eAgFnSc1d1	generická
firma	firma	k1gFnSc1	firma
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
Zentiva	Zentiva	k1gFnSc1	Zentiva
zahájila	zahájit	k5eAaPmAgFnS	zahájit
své	svůj	k3xOyFgFnPc4	svůj
aktivity	aktivita	k1gFnPc4	aktivita
v	v	k7c6	v
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
a	a	k8xC	a
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
byla	být	k5eAaImAgFnS	být
začleněna	začleněn	k2eAgFnSc1d1	začleněna
společnost	společnost	k1gFnSc1	společnost
Eczacibaşi	Eczacibaş	k1gFnSc2	Eczacibaş
Generic	Generice	k1gFnPc2	Generice
Pharmaceuticals	Pharmaceuticals	k1gInSc1	Pharmaceuticals
(	(	kIx(	(
<g/>
významný	významný	k2eAgMnSc1d1	významný
turecký	turecký	k2eAgMnSc1d1	turecký
výrobce	výrobce	k1gMnSc1	výrobce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
firmu	firma	k1gFnSc4	firma
koupila	koupit	k5eAaPmAgFnS	koupit
francouzská	francouzský	k2eAgFnSc1d1	francouzská
skupina	skupina	k1gFnSc1	skupina
Sanofi	Sanof	k1gFnSc2	Sanof
a	a	k8xC	a
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
se	se	k3xPyFc4	se
vytvořit	vytvořit	k5eAaPmF	vytvořit
ze	z	k7c2	z
Zentivy	Zentiva	k1gFnSc2	Zentiva
svou	svůj	k3xOyFgFnSc4	svůj
evropskou	evropský	k2eAgFnSc4d1	Evropská
generickou	generický	k2eAgFnSc4d1	generická
platformu	platforma	k1gFnSc4	platforma
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
v	v	k7c6	v
sobě	sebe	k3xPyFc6	sebe
spojila	spojit	k5eAaPmAgFnS	spojit
tři	tři	k4xCgFnPc4	tři
obchodní	obchodní	k2eAgFnPc4d1	obchodní
značky	značka	k1gFnPc4	značka
<g/>
:	:	kIx,	:
Zentivu	Zentiva	k1gFnSc4	Zentiva
<g/>
,	,	kIx,	,
Winthrop	Winthrop	k1gInSc4	Winthrop
a	a	k8xC	a
Helvepharm	Helvepharm	k1gInSc4	Helvepharm
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
17	[number]	k4	17
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2018	[number]	k4	2018
společnost	společnost	k1gFnSc1	společnost
Sanofi	Sanof	k1gFnSc2	Sanof
oznámila	oznámit	k5eAaPmAgFnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
zahájila	zahájit	k5eAaPmAgNnP	zahájit
exkluzivní	exkluzivní	k2eAgNnPc1d1	exkluzivní
jednání	jednání	k1gNnPc1	jednání
se	se	k3xPyFc4	se
společností	společnost	k1gFnPc2	společnost
Advent	advent	k1gInSc1	advent
International	International	k1gMnSc1	International
o	o	k7c6	o
odprodeji	odprodej	k1gInSc6	odprodej
své	svůj	k3xOyFgFnSc2	svůj
evropské	evropský	k2eAgFnSc2d1	Evropská
generické	generický	k2eAgFnSc2d1	generická
divize	divize	k1gFnSc2	divize
-	-	kIx~	-
společnosti	společnost	k1gFnPc1	společnost
Zentiva	Zentivum	k1gNnSc2	Zentivum
<g/>
.	.	kIx.	.
</s>
<s>
Transakce	transakce	k1gFnSc1	transakce
o	o	k7c6	o
celkové	celkový	k2eAgFnSc6d1	celková
hodnotě	hodnota	k1gFnSc6	hodnota
1,919	[number]	k4	1,919
mld.	mld.	k?	mld.
EUR	euro	k1gNnPc2	euro
byla	být	k5eAaImAgFnS	být
úspěšně	úspěšně	k6eAd1	úspěšně
dokončena	dokončit	k5eAaPmNgFnS	dokončit
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
říjnu	říjen	k1gInSc3	říjen
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
</s>
<s>
Samostatná	samostatný	k2eAgFnSc1d1	samostatná
Zentiva	Zentiva	k1gFnSc1	Zentiva
tak	tak	k6eAd1	tak
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
Advent	advent	k1gInSc1	advent
International	International	k1gMnSc4	International
<g/>
,	,	kIx,	,
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
největších	veliký	k2eAgMnPc2d3	veliký
světových	světový	k2eAgMnPc2d1	světový
kapitálových	kapitálový	k2eAgMnPc2d1	kapitálový
fondů	fond	k1gInPc2	fond
<g/>
,	,	kIx,	,
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
do	do	k7c2	do
nové	nový	k2eAgFnSc2d1	nová
éry	éra	k1gFnSc2	éra
nezávislosti	nezávislost	k1gFnSc2	nezávislost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
společnosti	společnost	k1gFnSc2	společnost
Zentiva	Zentiva	k1gFnSc1	Zentiva
Group	Group	k1gMnSc1	Group
<g/>
,	,	kIx,	,
a.	a.	k?	a.
s.	s.	k?	s.
(	(	kIx(	(
<g/>
centrála	centrála	k1gFnSc1	centrála
<g/>
)	)	kIx)	)
a	a	k8xC	a
Zentiva	Zentiva	k1gFnSc1	Zentiva
<g/>
,	,	kIx,	,
k.	k.	k?	k.
s.	s.	k?	s.
(	(	kIx(	(
<g/>
obchodní	obchodní	k2eAgFnSc1d1	obchodní
jednotka	jednotka	k1gFnSc1	jednotka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
společnosti	společnost	k1gFnSc2	společnost
Advent	advent	k1gInSc4	advent
International	International	k1gFnSc2	International
</s>
</p>
