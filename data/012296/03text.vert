<p>
<s>
Ján	Ján	k1gMnSc1	Ján
Strapec	Strapec	k1gMnSc1	Strapec
(	(	kIx(	(
<g/>
*	*	kIx~	*
10	[number]	k4	10
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1989	[number]	k4	1989
Trnava	Trnava	k1gFnSc1	Trnava
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
zejména	zejména	k9	zejména
pod	pod	k7c7	pod
uměleckým	umělecký	k2eAgNnSc7d1	umělecké
jménem	jméno	k1gNnSc7	jméno
Strapo	Strapa	k1gFnSc5	Strapa
<g/>
,	,	kIx,	,
je	on	k3xPp3gInPc4	on
slovenský	slovenský	k2eAgMnSc1d1	slovenský
raper	raper	k1gMnSc1	raper
a	a	k8xC	a
freestyler	freestyler	k1gMnSc1	freestyler
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
dvojnásobným	dvojnásobný	k2eAgMnSc7d1	dvojnásobný
vítězem	vítěz	k1gMnSc7	vítěz
celoslovenské	celoslovenský	k2eAgFnSc2d1	celoslovenská
soutěže	soutěž	k1gFnSc2	soutěž
ve	v	k7c6	v
freestyle	freestyl	k1gInSc5	freestyl
battlu	battl	k1gMnSc3	battl
−	−	k?	−
Artattack	Artattack	k1gMnSc1	Artattack
Freestyle	Freestyl	k1gInSc5	Freestyl
Battle	Battle	k1gFnSc2	Battle
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
poprvé	poprvé	k6eAd1	poprvé
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
vydal	vydat	k5eAaPmAgMnS	vydat
EP	EP	kA	EP
MC	MC	kA	MC
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
věděl	vědět	k5eAaImAgMnS	vědět
příliš	příliš	k6eAd1	příliš
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
mixtape	mixtapat	k5eAaPmIp3nS	mixtapat
50	[number]	k4	50
<g/>
:	:	kIx,	:
<g/>
50	[number]	k4	50
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yIgInSc6	který
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
DJ	DJ	kA	DJ
Spinhandz	Spinhandz	k1gMnSc1	Spinhandz
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
vydal	vydat	k5eAaPmAgInS	vydat
s	s	k7c7	s
producentem	producent	k1gMnSc7	producent
Emeresem	Emeres	k1gMnSc7	Emeres
album	album	k1gNnSc1	album
23	[number]	k4	23
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Radio_Head	Radio_Head	k1gInSc4	Radio_Head
awards	awards	k1gInSc1	awards
2012	[number]	k4	2012
byl	být	k5eAaImAgInS	být
nominovaný	nominovaný	k2eAgInSc1d1	nominovaný
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
kategoriich	kategorii	k1gFnPc6	kategorii
<g/>
:	:	kIx,	:
Nováček	nováček	k1gInSc4	nováček
roku	rok	k1gInSc2	rok
a	a	k8xC	a
Nejlepší	dobrý	k2eAgNnSc1d3	nejlepší
album	album	k1gNnSc1	album
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Hip-Hop	Hip-Hop	k1gInSc1	Hip-Hop
<g/>
,	,	kIx,	,
Rap	rap	k1gMnSc1	rap
a	a	k8xC	a
R	R	kA	R
<g/>
&	&	k?	&
<g/>
B.	B.	kA	B.
</s>
</p>
<p>
<s>
==	==	k?	==
Hudební	hudební	k2eAgFnSc1d1	hudební
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
Strapo	Strapa	k1gFnSc5	Strapa
se	se	k3xPyFc4	se
k	k	k7c3	k
rapu	rap	k1gMnSc3	rap
dostal	dostat	k5eAaPmAgMnS	dostat
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
v	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
přestěhoval	přestěhovat	k5eAaPmAgMnS	přestěhovat
z	z	k7c2	z
Bratislavy	Bratislava	k1gFnSc2	Bratislava
na	na	k7c6	na
Záhoře	záhora	k1gFnSc6	záhora
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
první	první	k4xOgFnPc4	první
rapové	rapový	k2eAgFnPc4d1	rapová
interprety	interpret	k1gMnPc7	interpret
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
poslouchal	poslouchat	k5eAaImAgMnS	poslouchat
<g/>
,	,	kIx,	,
patřili	patřit	k5eAaImAgMnP	patřit
i	i	k9	i
Eminem	Emin	k1gMnSc7	Emin
a	a	k8xC	a
Názov	Názov	k1gInSc4	Názov
Stavby	stavba	k1gFnSc2	stavba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čase	čas	k1gInSc6	čas
popularity	popularita	k1gFnSc2	popularita
filmu	film	k1gInSc2	film
8	[number]	k4	8
Mile	mile	k6eAd1	mile
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
zajímat	zajímat	k5eAaImF	zajímat
o	o	k7c6	o
freestyle	freestyl	k1gInSc5	freestyl
battle	battle	k1gFnPc1	battle
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yRgInPc4	který
nejprve	nejprve	k6eAd1	nejprve
začal	začít	k5eAaPmAgInS	začít
chodit	chodit	k5eAaImF	chodit
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
jich	on	k3xPp3gFnPc2	on
začal	začít	k5eAaPmAgMnS	začít
účastnit	účastnit	k5eAaImF	účastnit
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
jako	jako	k9	jako
16	[number]	k4	16
<g/>
letý	letý	k2eAgInSc4d1	letý
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
freestyle	freestyl	k1gInSc5	freestyl
battle	battle	k1gFnPc4	battle
Bombaklad	Bombaklad	k1gInSc1	Bombaklad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
celoslovenský	celoslovenský	k2eAgInSc4d1	celoslovenský
Artattack	Artattack	k1gInSc4	Artattack
Freestyle	Freestyl	k1gInSc5	Freestyl
Battle	Battle	k1gFnSc5	Battle
<g/>
.	.	kIx.	.
</s>
<s>
Nejbližší	blízký	k2eAgInSc1d3	Nejbližší
rok	rok	k1gInSc1	rok
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgInS	zúčastnit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
30	[number]	k4	30
dalších	další	k2eAgFnPc2d1	další
soutěží	soutěž	k1gFnPc2	soutěž
ve	v	k7c6	v
freestyle	freestyl	k1gInSc5	freestyl
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podruhé	podruhé	k6eAd1	podruhé
podařilo	podařit	k5eAaPmAgNnS	podařit
vyhrát	vyhrát	k5eAaPmF	vyhrát
Artattack	Artattack	k1gInSc4	Artattack
freestyle	freestyl	k1gInSc5	freestyl
battle	battle	k1gFnSc1	battle
<g/>
.28	.28	k4	.28
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2007	[number]	k4	2007
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
jeho	jeho	k3xOp3gInSc4	jeho
první	první	k4xOgInSc4	první
EP	EP	kA	EP
MC	MC	kA	MC
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
věděl	vědět	k5eAaImAgMnS	vědět
příliš	příliš	k6eAd1	příliš
<g/>
.	.	kIx.	.
</s>
<s>
Celé	celá	k1gFnPc1	celá
EP	EP	kA	EP
produkoval	produkovat	k5eAaImAgMnS	produkovat
producent	producent	k1gMnSc1	producent
Karaoke	Karaok	k1gFnSc2	Karaok
Tundra	tundra	k1gFnSc1	tundra
<g/>
,	,	kIx,	,
jediní	jediný	k2eAgMnPc1d1	jediný
hosté	host	k1gMnPc1	host
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
jsou	být	k5eAaImIp3nP	být
Chutkass	Chutkass	k1gInSc4	Chutkass
a	a	k8xC	a
Ektor	Ektor	k1gInSc4	Ektor
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
Moja	Moja	k?	Moja
Reč	Reč	k1gMnSc1	Reč
objevil	objevit	k5eAaPmAgMnS	objevit
na	na	k7c6	na
remixu	remix	k1gInSc6	remix
skladby	skladba	k1gFnSc2	skladba
"	"	kIx"	"
<g/>
Máme	mít	k5eAaImIp1nP	mít
vás	vy	k3xPp2nPc4	vy
<g/>
"	"	kIx"	"
od	od	k7c2	od
české	český	k2eAgFnSc2d1	Česká
skupiny	skupina	k1gFnSc2	skupina
IdeaFatte	IdeaFatt	k1gInSc5	IdeaFatt
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yIgMnSc3	který
byl	být	k5eAaImAgInS	být
i	i	k9	i
udělaný	udělaný	k2eAgInSc1d1	udělaný
videoklip	videoklip	k1gInSc1	videoklip
<g/>
.	.	kIx.	.
</s>
<s>
Klip	klip	k1gInSc1	klip
má	mít	k5eAaImIp3nS	mít
k	k	k7c3	k
březnu	březen	k1gInSc3	březen
2013	[number]	k4	2013
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1	[number]	k4	1
313	[number]	k4	313
000	[number]	k4	000
zhlédnutí	zhlédnutí	k1gNnPc2	zhlédnutí
na	na	k7c6	na
oficiálním	oficiální	k2eAgNnSc6d1	oficiální
youtube	youtubat	k5eAaPmIp3nS	youtubat
kanálu	kanál	k1gInSc3	kanál
skupiny	skupina	k1gFnSc2	skupina
IdeaFatte	IdeaFatt	k1gInSc5	IdeaFatt
<g/>
.	.	kIx.	.
<g/>
Během	během	k7c2	během
let	léto	k1gNnPc2	léto
2008	[number]	k4	2008
a	a	k8xC	a
2009	[number]	k4	2009
se	se	k3xPyFc4	se
také	také	k9	také
podílel	podílet	k5eAaImAgMnS	podílet
spolupracemi	spolupráce	k1gFnPc7	spolupráce
na	na	k7c6	na
albech	album	k1gNnPc6	album
Utečenec	utečenec	k1gMnSc1	utečenec
(	(	kIx(	(
<g/>
Decko	Decko	k1gNnSc1	Decko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
P.	P.	kA	P.
<g/>
O.B	O.B	k1gFnSc7	O.B
a	a	k8xC	a
nebo	nebo	k8xC	nebo
Mount	Mount	k1gMnSc1	Mount
Emerest	Emerest	k1gMnSc1	Emerest
(	(	kIx(	(
<g/>
Emeres	Emeres	k1gMnSc1	Emeres
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
zveřejňuje	zveřejňovat	k5eAaImIp3nS	zveřejňovat
skladbu	skladba	k1gFnSc4	skladba
"	"	kIx"	"
<g/>
Musím	muset	k5eAaImIp1nS	muset
ísť	ísť	k?	ísť
<g/>
"	"	kIx"	"
produkovanou	produkovaný	k2eAgFnSc4d1	produkovaná
Emeresem	Emeres	k1gInSc7	Emeres
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc7	který
ukončil	ukončit	k5eAaPmAgInS	ukončit
svoji	svůj	k3xOyFgFnSc4	svůj
kariéru	kariéra	k1gFnSc4	kariéra
freestylera	freestyler	k1gMnSc2	freestyler
<g/>
,	,	kIx,	,
během	během	k7c2	během
které	který	k3yRgFnSc2	který
50	[number]	k4	50
<g/>
krát	krát	k6eAd1	krát
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
krát	krát	k6eAd1	krát
prohrál	prohrát	k5eAaPmAgMnS	prohrát
a	a	k8xC	a
jednou	jeden	k4xCgFnSc7	jeden
remízoval	remízovat	k5eAaPmAgMnS	remízovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
na	na	k7c6	na
skladbě	skladba	k1gFnSc6	skladba
"	"	kIx"	"
<g/>
Si	se	k3xPyFc3	se
zvonil	zvonit	k5eAaImAgMnS	zvonit
<g/>
?!	?!	k?	?!
(	(	kIx(	(
<g/>
Radikal	Radikal	k1gFnSc1	Radikal
diss	dissa	k1gFnPc2	dissa
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
Separem	Separ	k1gMnSc7	Separ
a	a	k8xC	a
Deckem	Decek	k1gMnSc7	Decek
dissoval	dissovat	k5eAaImAgMnS	dissovat
Radikála	radikál	k1gMnSc4	radikál
<g/>
,	,	kIx,	,
člena	člen	k1gMnSc4	člen
skupiny	skupina	k1gFnSc2	skupina
Dramatikz	Dramatikz	k1gMnSc1	Dramatikz
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yRgInSc7	který
měl	mít	k5eAaImAgMnS	mít
předtím	předtím	k6eAd1	předtím
nějaké	nějaký	k3yIgFnSc2	nějaký
spolupráce	spolupráce	k1gFnSc2	spolupráce
<g/>
.	.	kIx.	.
</s>
<s>
Videoklip	videoklip	k1gInSc1	videoklip
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2013	[number]	k4	2013
na	na	k7c6	na
oficiálním	oficiální	k2eAgInSc6d1	oficiální
kanále	kanál	k1gInSc6	kanál
Gramo	Grama	k1gFnSc5	Grama
Rokkaz	Rokkaz	k1gInSc4	Rokkaz
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
2	[number]	k4	2
600	[number]	k4	600
000	[number]	k4	000
zhlédnutí	zhlédnutí	k1gNnPc2	zhlédnutí
a	a	k8xC	a
stal	stát	k5eAaPmAgInS	stát
se	se	k3xPyFc4	se
nejsledovanější	sledovaný	k2eAgFnSc7d3	nejsledovanější
skladbou	skladba	k1gFnSc7	skladba
<g/>
,	,	kIx,	,
jakou	jaký	k3yQgFnSc4	jaký
kdy	kdy	k6eAd1	kdy
Strapo	Strapa	k1gFnSc5	Strapa
nahrál	nahrát	k5eAaPmAgInS	nahrát
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
roku	rok	k1gInSc2	rok
ještě	ještě	k6eAd1	ještě
vypustil	vypustit	k5eAaPmAgInS	vypustit
skladbu	skladba	k1gFnSc4	skladba
"	"	kIx"	"
<g/>
Dzime	Dzim	k1gMnSc5	Dzim
dzeci	dzec	k1gMnSc5	dzec
<g/>
"	"	kIx"	"
a	a	k8xC	a
videoklip	videoklip	k1gInSc4	videoklip
ke	k	k7c3	k
skladbě	skladba	k1gFnSc3	skladba
"	"	kIx"	"
<g/>
Daj	Daj	k1gFnSc1	Daj
peňáz	peňáza	k1gFnPc2	peňáza
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ktorý	ktorý	k2eAgInSc1d1	ktorý
bol	bol	k1gInSc1	bol
veľmi	veľ	k1gFnPc7	veľ
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
v	v	k7c6	v
získal	získat	k5eAaPmAgMnS	získat
piate	piate	k5eAaPmIp2nP	piate
miesto	miesto	k6eAd1	miesto
v	v	k7c6	v
hlasovaní	hlasovaný	k2eAgMnPc1d1	hlasovaný
o	o	k7c4	o
videoklip	videoklip	k1gInSc4	videoklip
roka	rok	k1gInSc2	rok
v	v	k7c6	v
ankete	anke	k1gNnSc2	anke
Slávik	Slávik	k1gInSc4	Slávik
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Videoklip	videoklip	k1gInSc1	videoklip
byl	být	k5eAaImAgInS	být
předzvěstí	předzvěst	k1gFnSc7	předzvěst
projektu	projekt	k1gInSc2	projekt
s	s	k7c7	s
Emeresem	Emeres	k1gInSc7	Emeres
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterým	který	k3yQgNnSc7	který
začal	začít	k5eAaPmAgInS	začít
pracovat	pracovat	k5eAaImF	pracovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2013	[number]	k4	2013
měl	mít	k5eAaImAgMnS	mít
na	na	k7c6	na
youtube	youtubat	k5eAaPmIp3nS	youtubat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1	[number]	k4	1
587	[number]	k4	587
000	[number]	k4	000
zhlédnutí	zhlédnutí	k1gNnPc2	zhlédnutí
<g/>
.1	.1	k4	.1
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2012	[number]	k4	2012
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
Strapovo	Strapův	k2eAgNnSc1d1	Strapův
album	album	k1gNnSc1	album
23	[number]	k4	23
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
českým	český	k2eAgMnSc7d1	český
producentem	producent	k1gMnSc7	producent
Emeresem	Emeres	k1gMnSc7	Emeres
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
albu	album	k1gNnSc3	album
byli	být	k5eAaImAgMnP	být
vytvořené	vytvořený	k2eAgInPc4d1	vytvořený
další	další	k2eAgInPc4d1	další
videoklipy	videoklip	k1gInPc4	videoklip
ke	k	k7c3	k
skladbám	skladba	k1gFnPc3	skladba
"	"	kIx"	"
<g/>
Sám	sám	k3xTgInSc1	sám
doma	doma	k6eAd1	doma
a	a	k8xC	a
bohatý	bohatý	k2eAgMnSc1d1	bohatý
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Starý	Starý	k1gMnSc1	Starý
pes	pes	k1gMnSc1	pes
<g/>
"	"	kIx"	"
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
i	i	k8xC	i
ke	k	k7c3	k
skladbe	skladbat	k5eAaPmIp3nS	skladbat
"	"	kIx"	"
<g/>
Loading	Loading	k1gInSc1	Loading
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
nasazená	nasazený	k2eAgFnSc1d1	nasazená
do	do	k7c2	do
slovenských	slovenský	k2eAgFnPc2d1	slovenská
rádií	rádio	k1gNnPc2	rádio
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
umístila	umístit	k5eAaPmAgFnS	umístit
nejvýše	nejvýše	k6eAd1	nejvýše
na	na	k7c4	na
57	[number]	k4	57
<g/>
.	.	kIx.	.
příčce	příčka	k1gFnSc6	příčka
a	a	k8xC	a
na	na	k7c6	na
6	[number]	k4	6
<g/>
.	.	kIx.	.
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
domácí	domácí	k2eAgFnSc2d1	domácí
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
9	[number]	k4	9
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
byla	být	k5eAaImAgFnS	být
premiéra	premiéra	k1gFnSc1	premiéra
první	první	k4xOgFnSc2	první
epizody	epizoda	k1gFnSc2	epizoda
internetového	internetový	k2eAgInSc2d1	internetový
dokumentárního	dokumentární	k2eAgInSc2d1	dokumentární
seriálu	seriál	k1gInSc2	seriál
-	-	kIx~	-
Ja	Ja	k?	Ja
som	soma	k1gFnPc2	soma
hip	hip	k0	hip
hop	hop	k0	hop
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
o	o	k7c6	o
Strapovi	Strap	k1gMnSc6	Strap
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Soukromý	soukromý	k2eAgInSc4d1	soukromý
život	život	k1gInSc4	život
==	==	k?	==
</s>
</p>
<p>
<s>
Strapo	Strapa	k1gFnSc5	Strapa
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
rozvedené	rozvedený	k2eAgFnSc2d1	rozvedená
rodiny	rodina	k1gFnSc2	rodina
a	a	k8xC	a
dost	dost	k6eAd1	dost
často	často	k6eAd1	často
se	se	k3xPyFc4	se
s	s	k7c7	s
matkou	matka	k1gFnSc7	matka
v	v	k7c6	v
dětství	dětství	k1gNnSc6	dětství
stěhoval	stěhovat	k5eAaImAgMnS	stěhovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
školy	škola	k1gFnSc2	škola
chodil	chodit	k5eAaImAgMnS	chodit
na	na	k7c4	na
dramatický	dramatický	k2eAgInSc4d1	dramatický
kroužek	kroužek	k1gInSc4	kroužek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pořadu	pořad	k1gInSc6	pořad
Ja	Ja	k?	Ja
som	soma	k1gFnPc2	soma
hip-hop	hipop	k1gInSc4	hip-hop
se	se	k3xPyFc4	se
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc1	ten
pomohlo	pomoct	k5eAaPmAgNnS	pomoct
překonat	překonat	k5eAaPmF	překonat
trému	tréma	k1gFnSc4	tréma
a	a	k8xC	a
natrénovat	natrénovat	k5eAaPmF	natrénovat
projev	projev	k1gInSc4	projev
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mu	on	k3xPp3gMnSc3	on
později	pozdě	k6eAd2	pozdě
pomohlo	pomoct	k5eAaPmAgNnS	pomoct
i	i	k8xC	i
při	pře	k1gFnSc3	pře
rapovaní	rapovaný	k2eAgMnPc1d1	rapovaný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
7	[number]	k4	7
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2010	[number]	k4	2010
měl	mít	k5eAaImAgInS	mít
Strapo	Strapa	k1gFnSc5	Strapa
vážnou	vážný	k2eAgFnSc7d1	vážná
autonehodu	autonehoda	k1gFnSc4	autonehoda
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yIgFnSc6	který
zemřel	zemřít	k5eAaPmAgMnS	zemřít
řidič	řidič	k1gMnSc1	řidič
auta	auto	k1gNnSc2	auto
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
seděl	sedět	k5eAaImAgMnS	sedět
<g/>
.	.	kIx.	.
</s>
<s>
Strapo	Strapa	k1gFnSc5	Strapa
popsal	popsat	k5eAaPmAgMnS	popsat
nehodu	nehoda	k1gFnSc4	nehoda
jako	jako	k8xS	jako
silný	silný	k2eAgInSc4d1	silný
zážitek	zážitek	k1gInSc4	zážitek
<g/>
,	,	kIx,	,
po	po	k7c6	po
kterém	který	k3yQgInSc6	který
přehodnotil	přehodnotit	k5eAaPmAgMnS	přehodnotit
mnohé	mnohý	k2eAgFnPc4d1	mnohá
věci	věc	k1gFnPc4	věc
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
živote	život	k1gInSc5	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Spory	spor	k1gInPc1	spor
a	a	k8xC	a
beefy	beef	k1gInPc1	beef
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
H16	H16	k1gMnPc5	H16
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
Strapo	Strapa	k1gFnSc5	Strapa
udělal	udělat	k5eAaPmAgInS	udělat
diss	diss	k1gInSc1	diss
na	na	k7c4	na
Otecka	otecko	k1gMnSc4	otecko
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
H	H	kA	H
<g/>
16	[number]	k4	16
<g/>
,	,	kIx,	,
skladbu	skladba	k1gFnSc4	skladba
"	"	kIx"	"
<g/>
Killa	Killa	k1gMnSc1	Killa
Khonec	Khonec	k1gMnSc1	Khonec
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zmínil	zmínit	k5eAaPmAgMnS	zmínit
v	v	k7c6	v
pár	pár	k4xCyI	pár
verších	verš	k1gInPc6	verš
i	i	k8xC	i
Majka	Majka	k1gFnSc1	Majka
Spirita	Spirita	k?	Spirita
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
na	na	k7c6	na
skladbě	skladba	k1gFnSc6	skladba
"	"	kIx"	"
<g/>
Aspirin	aspirin	k1gInSc1	aspirin
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nepřímoo	přímoo	k6eNd1	přímoo
zmínil	zmínit	k5eAaPmAgMnS	zmínit
názvy	název	k1gInPc7	název
jeho	jeho	k3xOp3gFnPc2	jeho
skladeb	skladba	k1gFnPc2	skladba
"	"	kIx"	"
<g/>
Nejsom	Nejsom	k1gInSc1	Nejsom
falošný	falošný	k2eAgInSc1d1	falošný
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Nech	nechat	k5eAaPmRp2nS	nechat
ti	ty	k3xPp2nSc3	ty
nejebe	jebat	k5eNaImIp3nS	jebat
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Tie	Tie	k1gFnSc1	Tie
isté	istý	k2eAgFnSc2d1	istý
pičoviny	pičovina	k1gFnSc2	pičovina
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Neverím	Neverím	k2eAgNnSc7d2	Neverím
ti	ten	k3xDgMnPc1	ten
<g/>
"	"	kIx"	"
a	a	k8xC	a
nasměroval	nasměrovat	k5eAaPmAgMnS	nasměrovat
je	být	k5eAaImIp3nS	být
zřejmě	zřejmě	k6eAd1	zřejmě
proti	proti	k7c3	proti
jejich	jejich	k3xOp3gMnPc3	jejich
podvodným	podvodný	k2eAgMnPc3d1	podvodný
autorům	autor	k1gMnPc3	autor
<g/>
.	.	kIx.	.
</s>
<s>
Otecko	otecko	k1gMnSc1	otecko
<g/>
,	,	kIx,	,
Spirit	Spirit	k1gInSc1	Spirit
(	(	kIx(	(
<g/>
ani	ani	k8xC	ani
jiný	jiný	k2eAgMnSc1d1	jiný
člen	člen	k1gMnSc1	člen
H	H	kA	H
<g/>
16	[number]	k4	16
<g/>
)	)	kIx)	)
na	na	k7c4	na
tyto	tento	k3xDgFnPc4	tento
dissy	dissa	k1gFnPc4	dissa
nikdy	nikdy	k6eAd1	nikdy
nereagovali	reagovat	k5eNaBmAgMnP	reagovat
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
najít	najít	k5eAaPmF	najít
video	video	k1gNnSc4	video
ze	z	k7c2	z
zákulisí	zákulisí	k1gNnSc2	zákulisí
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
konfrontace	konfrontace	k1gFnSc1	konfrontace
mezi	mezi	k7c7	mezi
členy	člen	k1gMnPc7	člen
H16	H16	k1gMnSc7	H16
a	a	k8xC	a
Strapem	Strap	k1gMnSc7	Strap
<g/>
.	.	kIx.	.
</s>
<s>
Spirit	Spirit	k1gInSc1	Spirit
ale	ale	k9	ale
necítí	cítit	k5eNaImIp3nS	cítit
ke	k	k7c3	k
Strapovi	Strap	k1gMnSc3	Strap
žádnou	žádný	k3yNgFnSc7	žádný
negativní	negativní	k2eAgFnSc7d1	negativní
energií	energie	k1gFnSc7	energie
<g/>
,	,	kIx,	,
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
pro	pro	k7c4	pro
mafiarecords	mafiarecords	k1gInSc4	mafiarecords
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
Strapo	Strapa	k1gFnSc5	Strapa
je	být	k5eAaImIp3nS	být
dobrý	dobrý	k2eAgInSc4d1	dobrý
rapper	rapper	k1gInSc4	rapper
a	a	k8xC	a
neměl	mít	k5eNaImAgInS	mít
by	by	kYmCp3nS	by
problém	problém	k1gInSc1	problém
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
vytvořit	vytvořit	k5eAaPmF	vytvořit
skladbu	skladba	k1gFnSc4	skladba
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
tomu	ten	k3xDgNnSc3	ten
ve	v	k7c6	v
skladbě	skladba	k1gFnSc6	skladba
"	"	kIx"	"
<g/>
Starý	starý	k2eAgMnSc1d1	starý
pes	pes	k1gMnSc1	pes
<g/>
"	"	kIx"	"
má	mít	k5eAaImIp3nS	mít
verš	verš	k1gInSc1	verš
"	"	kIx"	"
<g/>
Keď	Keď	k1gMnSc1	Keď
ty	ten	k3xDgInPc4	ten
si	se	k3xPyFc3	se
nový	nový	k2eAgInSc4d1	nový
človek	človek	k1gInSc4	človek
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
ja	ja	k?	ja
som	soma	k1gFnPc2	soma
starý	starý	k1gMnSc1	starý
pes	pes	k1gMnSc1	pes
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
může	moct	k5eAaImIp3nS	moct
odkazovat	odkazovat	k5eAaImF	odkazovat
na	na	k7c4	na
sólo	sólo	k2eAgNnSc4d1	sólo
album	album	k1gNnSc4	album
Nový	nový	k2eAgInSc1d1	nový
človek	človek	k1gInSc1	človek
od	od	k7c2	od
Majka	Majka	k1gFnSc1	Majka
Spirita	Spirita	k?	Spirita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Miky	Mik	k1gMnPc4	Mik
Mora	mora	k1gFnSc1	mora
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
nahrál	nahrát	k5eAaPmAgMnS	nahrát
Miky	Mik	k1gMnPc4	Mik
Mora	mora	k1gFnSc1	mora
skladbu	skladba	k1gFnSc4	skladba
"	"	kIx"	"
<g/>
1.6	[number]	k4	1.6
TDi	TDi	k1gFnPc2	TDi
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
diss	diss	k6eAd1	diss
na	na	k7c4	na
H	H	kA	H
<g/>
16	[number]	k4	16
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zmínil	zmínit	k5eAaPmAgMnS	zmínit
i	i	k9	i
Strapa	Strap	k1gMnSc4	Strap
a	a	k8xC	a
snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
zesměšnit	zesměšnit	k5eAaPmF	zesměšnit
Otecka	otecko	k1gMnSc4	otecko
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
dissuje	dissovat	k5eAaBmIp3nS	dissovat
i	i	k9	i
"	"	kIx"	"
<g/>
amatér	amatér	k1gMnSc1	amatér
Strapo	Strapa	k1gFnSc5	Strapa
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Radikal	Radikal	k1gMnPc5	Radikal
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
pěti	pět	k4xCc6	pět
spolupracích	spolupráce	k1gFnPc6	spolupráce
s	s	k7c7	s
Radikalem	Radikal	k1gMnSc7	Radikal
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
Dramatikz	Dramatikz	k1gInSc1	Dramatikz
<g/>
,	,	kIx,	,
Strapo	Strapa	k1gFnSc5	Strapa
hostoval	hostovat	k5eAaImAgInS	hostovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
na	na	k7c6	na
disse	dissa	k1gFnSc6	dissa
"	"	kIx"	"
<g/>
Si	se	k3xPyFc3	se
zvonil	zvonit	k5eAaImAgMnS	zvonit
<g/>
?!	?!	k?	?!
(	(	kIx(	(
<g/>
Radikal	Radikal	k1gFnSc1	Radikal
diss	dissa	k1gFnPc2	dissa
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
od	od	k7c2	od
Separa	Separ	k1gMnSc2	Separ
a	a	k8xC	a
Decka	Decek	k1gMnSc2	Decek
<g/>
,	,	kIx,	,
členů	člen	k1gMnPc2	člen
labelu	label	k1gInSc2	label
Gramo	Grama	k1gFnSc5	Grama
Rokkaz	Rokkaz	k1gInSc4	Rokkaz
<g/>
.	.	kIx.	.
</s>
<s>
Strapo	Strapa	k1gFnSc5	Strapa
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
hovoří	hovořit	k5eAaImIp3nP	hovořit
ve	v	k7c6	v
skladbe	skladbat	k5eAaPmIp3nS	skladbat
i	i	k9	i
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
lituje	litovat	k5eAaImIp3nS	litovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
s	s	k7c7	s
Radikalem	Radikal	k1gMnSc7	Radikal
skladby	skladba	k1gFnSc2	skladba
<g/>
.	.	kIx.	.
</s>
<s>
Radikal	Radikat	k5eAaPmAgMnS	Radikat
reagoval	reagovat	k5eAaBmAgMnS	reagovat
i	i	k9	i
na	na	k7c4	na
Strapa	Strap	k1gMnSc4	Strap
v	v	k7c4	v
jeho	jeho	k3xOp3gFnPc4	jeho
odpovědi	odpověď	k1gFnPc4	odpověď
"	"	kIx"	"
<g/>
Vypol	Vypol	k1gInSc4	Vypol
si	se	k3xPyFc3	se
zvonček	zvonček	k1gMnSc1	zvonček
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
Smetiar	Smetiar	k1gMnSc1	Smetiar
&	&	k?	&
Prenastaviteľné	Prenastaviteľný	k2eAgNnSc1d1	Prenastaviteľný
Wécko	Wécko	k1gNnSc4	Wécko
diss	dissa	k1gFnPc2	dissa
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Diskografie	diskografie	k1gFnSc2	diskografie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Studiová	studiový	k2eAgNnPc1d1	studiové
alba	album	k1gNnPc1	album
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Emeres	Emeresa	k1gFnPc2	Emeresa
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Ostatní	ostatní	k2eAgNnPc4d1	ostatní
===	===	k?	===
</s>
</p>
<p>
<s>
2007	[number]	k4	2007
<g/>
:	:	kIx,	:
MC	MC	kA	MC
<g/>
,	,	kIx,	,
ktorý	ktorý	k2eAgMnSc1d1	ktorý
vedel	vedel	k1gMnSc1	vedel
priveľa	priveľa	k1gMnSc1	priveľa
(	(	kIx(	(
<g/>
EP	EP	kA	EP
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2008	[number]	k4	2008
<g/>
:	:	kIx,	:
50	[number]	k4	50
<g/>
:	:	kIx,	:
<g/>
50	[number]	k4	50
(	(	kIx(	(
<g/>
mixtape	mixtapat	k5eAaPmIp3nS	mixtapat
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
DJ	DJ	kA	DJ
Spinhandz	Spinhandz	k1gInSc1	Spinhandz
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Skladby	skladba	k1gFnPc4	skladba
umístěné	umístěný	k2eAgFnPc4d1	umístěná
v	v	k7c6	v
žebříčcích	žebříček	k1gInPc6	žebříček
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Videoklipy	videoklip	k1gInPc4	videoklip
===	===	k?	===
</s>
</p>
<p>
<s>
Sólo	sólo	k1gNnSc1	sólo
<g/>
2011	[number]	k4	2011
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Daj	Daj	k1gFnSc1	Daj
peňáz	peňáza	k1gFnPc2	peňáza
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
2012	[number]	k4	2012
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Sám	sám	k3xTgInSc1	sám
doma	doma	k6eAd1	doma
a	a	k8xC	a
bohatý	bohatý	k2eAgInSc1d1	bohatý
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
2012	[number]	k4	2012
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Starý	Starý	k1gMnSc1	Starý
pes	pes	k1gMnSc1	pes
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
2013	[number]	k4	2013
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Loading	Loading	k1gInSc4	Loading
<g/>
"	"	kIx"	"
<g/>
Ostatní	ostatní	k1gNnSc4	ostatní
<g/>
2007	[number]	k4	2007
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Máme	mít	k5eAaImIp1nP	mít
vás	vy	k3xPp2nPc4	vy
(	(	kIx(	(
<g/>
MCs	MCs	k1gFnPc3	MCs
remix	remix	k1gInSc4	remix
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
IdeaFatte	IdeaFatt	k1gMnSc5	IdeaFatt
ft	ft	k?	ft
<g/>
.	.	kIx.	.
</s>
<s>
Moja	Moja	k?	Moja
Reč	Reč	k1gFnSc1	Reč
&	&	k?	&
Strapo	Strapa	k1gFnSc5	Strapa
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2011	[number]	k4	2011
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Si	se	k3xPyFc3	se
zvonil	zvonit	k5eAaImAgMnS	zvonit
<g/>
?!	?!	k?	?!
(	(	kIx(	(
<g/>
Radikal	Radikal	k1gFnSc1	Radikal
diss	dissa	k1gFnPc2	dissa
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Separ	Separ	k1gMnSc1	Separ
&	&	k?	&
Decko	Decko	k1gNnSc4	Decko
ft	ft	k?	ft
<g/>
.	.	kIx.	.
</s>
<s>
Strapo	Strapa	k1gFnSc5	Strapa
&	&	k?	&
DJ	DJ	kA	DJ
Spinhandz	Spinhandz	k1gInSc1	Spinhandz
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Ocenění	ocenění	k1gNnSc1	ocenění
a	a	k8xC	a
nominace	nominace	k1gFnSc1	nominace
==	==	k?	==
</s>
</p>
<p>
<s>
AurelRadio_Head	AurelRadio_Head	k6eAd1	AurelRadio_Head
awards	awards	k6eAd1	awards
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Strapo	Strapa	k1gFnSc5	Strapa
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc3d1	slovenská
Wikipedii	Wikipedie	k1gFnSc3	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
