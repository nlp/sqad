<s>
John	John	k1gMnSc1
J.	J.	kA
Collins	Collins	k1gInSc1
</s>
<s>
John	John	k1gMnSc1
J.	J.	kA
Collins	Collins	k1gInSc1
Narození	narození	k1gNnSc2
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1946	#num#	k4
(	(	kIx(
<g/>
75	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Irsko	Irsko	k1gNnSc4
nebo	nebo	k8xC
Hrabství	hrabství	k1gNnSc4
Tipperary	Tipperara	k1gFnSc2
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
</s>
<s>
University	universita	k1gFnPc4
College	College	k1gFnSc1
DublinHarvardova	DublinHarvardův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
editor	editor	k1gInSc1
a	a	k8xC
vysokoškolský	vysokoškolský	k2eAgMnSc1d1
učitel	učitel	k1gMnSc1
Zaměstnavatelé	zaměstnavatel	k1gMnPc1
</s>
<s>
Yale	Yale	k1gFnSc1
Divinity	Divinita	k1gFnSc2
SchoolChicagská	SchoolChicagský	k2eAgFnSc1d1
univerzitaHarvardova	univerzitaHarvardův	k2eAgFnSc1d1
univerzitaUniversity	univerzitaUniversit	k1gInPc1
of	of	k?
Notre	Notr	k1gMnSc5
Dame	Damus	k1gMnSc5
Ocenění	ocenění	k1gNnSc1
</s>
<s>
Burkitt	Burkitt	k2eAgInSc1d1
Medal	Medal	k1gInSc1
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
Nábož	Nábož	k1gFnSc1
<g/>
.	.	kIx.
vyznání	vyznání	k1gNnSc1
</s>
<s>
katolická	katolický	k2eAgFnSc1d1
církev	církev	k1gFnSc1
Manžel	manžel	k1gMnSc1
<g/>
(	(	kIx(
<g/>
ka	ka	k?
<g/>
)	)	kIx)
</s>
<s>
Adela	Adela	k6eAd1
Yarbro	Yarbro	k1gNnSc1
Collins	Collinsa	k1gFnPc2
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybit	k5eAaPmIp3nS,k5eAaImIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
John	John	k1gMnSc1
J.	J.	kA
Collins	Collins	k1gInSc1
(	(	kIx(
<g/>
*	*	kIx~
1946	#num#	k4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
starozákonní	starozákonní	k2eAgMnSc1d1
vědec	vědec	k1gMnSc1
a	a	k8xC
biblista	biblista	k1gMnSc1
původem	původ	k1gInSc7
z	z	k7c2
Irska	Irsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Působí	působit	k5eAaImIp3nS
na	na	k7c6
Yaleově	Yaleův	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
jako	jako	k9
profesor	profesor	k1gMnSc1
starozákonní	starozákonní	k2eAgFnSc2d1
kritiky	kritika	k1gFnSc2
a	a	k8xC
interpretace	interpretace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
své	svůj	k3xOyFgFnSc6
vědecké	vědecký	k2eAgFnSc6d1
práci	práce	k1gFnSc6
se	se	k3xPyFc4
věnuje	věnovat	k5eAaImIp3nS,k5eAaPmIp3nS
zejména	zejména	k9
apokryfní	apokryfní	k2eAgFnSc3d1
literatuře	literatura	k1gFnSc3
období	období	k1gNnSc3
druhého	druhý	k4xOgInSc2
chrámu	chrám	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Je	být	k5eAaImIp3nS
ženat	ženat	k2eAgMnSc1d1
s	s	k7c7
teoložkou	teoložka	k1gFnSc7
Adelou	Adelý	k2eAgFnSc7d1
Yarbro	Yarbro	k1gNnSc4
Collins	Collins	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
John	John	k1gMnSc1
J.	J.	kA
Collins	Collins	k1gInSc1
na	na	k7c6
stránkách	stránka	k1gFnPc6
Yale	Yal	k1gInSc2
Divinity	Divinita	k1gFnSc2
School	Schoola	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jo	jo	k9
<g/>
2007376774	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
132518732	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2103	#num#	k4
7206	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
80045215	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
108264912	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
80045215	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Křesťanství	křesťanství	k1gNnSc1
|	|	kIx~
Bible	bible	k1gFnSc1
|	|	kIx~
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
|	|	kIx~
Irsko	Irsko	k1gNnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
</s>
