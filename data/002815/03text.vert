<s>
Marketa	Market	k2eAgFnSc1d1	Marketa
Lazarová	Lazarová	k1gFnSc1	Lazarová
je	být	k5eAaImIp3nS	být
český	český	k2eAgInSc1d1	český
film	film	k1gInSc1	film
natočený	natočený	k2eAgInSc1d1	natočený
režisérem	režisér	k1gMnSc7	režisér
Františkem	František	k1gMnSc7	František
Vláčilem	Vláčil	k1gMnSc7	Vláčil
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
podle	podle	k7c2	podle
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
knihy	kniha	k1gFnSc2	kniha
Vladislava	Vladislav	k1gMnSc2	Vladislav
Vančury	Vančura	k1gMnSc2	Vančura
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
období	období	k1gNnSc6	období
středověku	středověk	k1gInSc2	středověk
na	na	k7c6	na
statku	statek	k1gInSc6	statek
loupeživého	loupeživý	k2eAgMnSc2d1	loupeživý
zemana	zeman	k1gMnSc2	zeman
<g/>
.	.	kIx.	.
</s>
<s>
Život	život	k1gInSc1	život
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byl	být	k5eAaImAgInS	být
krutý	krutý	k2eAgInSc1d1	krutý
<g/>
,	,	kIx,	,
lidé	člověk	k1gMnPc1	člověk
se	se	k3xPyFc4	se
museli	muset	k5eAaImAgMnP	muset
hodně	hodně	k6eAd1	hodně
snažit	snažit	k5eAaImF	snažit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
uživili	uživit	k5eAaPmAgMnP	uživit
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
ubránili	ubránit	k5eAaPmAgMnP	ubránit
vojsku	vojsko	k1gNnSc3	vojsko
českého	český	k2eAgMnSc2d1	český
krále	král	k1gMnSc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Vláčil	vláčit	k5eAaImAgMnS	vláčit
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
scenárista	scenárista	k1gMnSc1	scenárista
Pavlíček	Pavlíček	k1gMnSc1	Pavlíček
připravovali	připravovat	k5eAaImAgMnP	připravovat
zhruba	zhruba	k6eAd1	zhruba
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Režisér	režisér	k1gMnSc1	režisér
detailně	detailně	k6eAd1	detailně
rozkresloval	rozkreslovat	k5eAaImAgMnS	rozkreslovat
scény	scéna	k1gFnSc2	scéna
a	a	k8xC	a
dopodrobna	dopodrobna	k6eAd1	dopodrobna
studoval	studovat	k5eAaImAgMnS	studovat
středověkou	středověký	k2eAgFnSc4d1	středověká
dobu	doba	k1gFnSc4	doba
a	a	k8xC	a
její	její	k3xOp3gFnPc4	její
reálie	reálie	k1gFnPc4	reálie
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgInS	moct
nabídnout	nabídnout	k5eAaPmF	nabídnout
její	její	k3xOp3gInSc4	její
co	co	k9	co
nejvěrnější	věrný	k2eAgInSc4d3	nejvěrnější
průřez	průřez	k1gInSc4	průřez
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
plánoval	plánovat	k5eAaImAgMnS	plánovat
do	do	k7c2	do
filmu	film	k1gInSc2	film
zahrnout	zahrnout	k5eAaPmF	zahrnout
i	i	k9	i
scény	scéna	k1gFnPc4	scéna
z	z	k7c2	z
královského	královský	k2eAgInSc2d1	královský
dvora	dvůr	k1gInSc2	dvůr
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc2	jeho
okolí	okolí	k1gNnSc2	okolí
<g/>
,	,	kIx,	,
ty	ten	k3xDgInPc1	ten
však	však	k9	však
nakonec	nakonec	k6eAd1	nakonec
nebyly	být	k5eNaImAgInP	být
realizovány	realizovat	k5eAaBmNgInP	realizovat
-	-	kIx~	-
jak	jak	k8xS	jak
z	z	k7c2	z
finančních	finanční	k2eAgInPc2d1	finanční
důvodů	důvod	k1gInPc2	důvod
(	(	kIx(	(
<g/>
Marketa	Market	k2eAgFnSc1d1	Marketa
Lazarová	Lazarová	k1gFnSc1	Lazarová
byla	být	k5eAaImAgFnS	být
nejdražším	drahý	k2eAgInSc7d3	nejdražší
československým	československý	k2eAgInSc7d1	československý
filmem	film	k1gInSc7	film
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
kvůli	kvůli	k7c3	kvůli
beztak	beztak	k9	beztak
značné	značný	k2eAgFnSc3d1	značná
časové	časový	k2eAgFnSc3d1	časová
délce	délka	k1gFnSc3	délka
filmu	film	k1gInSc2	film
(	(	kIx(	(
<g/>
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
blíží	blížit	k5eAaImIp3nS	blížit
třem	tři	k4xCgFnPc3	tři
hodinám	hodina	k1gFnPc3	hodina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
dokázal	dokázat	k5eAaPmAgMnS	dokázat
Vláčil	vláčit	k5eAaImAgInS	vláčit
ve	v	k7c6	v
filmu	film	k1gInSc6	film
brilantně	brilantně	k6eAd1	brilantně
prolnout	prolnout	k5eAaPmF	prolnout
lyrickou	lyrický	k2eAgFnSc4d1	lyrická
a	a	k8xC	a
epickou	epický	k2eAgFnSc4d1	epická
složku	složka	k1gFnSc4	složka
<g/>
,	,	kIx,	,
nevinnost	nevinnost	k1gFnSc4	nevinnost
a	a	k8xC	a
lásku	láska	k1gFnSc4	láska
i	i	k8xC	i
vypočítavost	vypočítavost	k1gFnSc4	vypočítavost
a	a	k8xC	a
utrpení	utrpení	k1gNnSc4	utrpení
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
postihl	postihnout	k5eAaPmAgInS	postihnout
zemitost	zemitost	k1gFnSc4	zemitost
pohanství	pohanství	k1gNnSc2	pohanství
oproti	oproti	k7c3	oproti
odpoutanosti	odpoutanost	k1gFnSc3	odpoutanost
dematerializovaného	dematerializovaný	k2eAgNnSc2d1	dematerializovaný
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
kontrast	kontrast	k1gInSc1	kontrast
mezi	mezi	k7c7	mezi
vrchností	vrchnost	k1gFnSc7	vrchnost
a	a	k8xC	a
poddanými	poddaný	k1gMnPc7	poddaný
tehdejších	tehdejší	k2eAgInPc2d1	tehdejší
časů	čas	k1gInPc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
byl	být	k5eAaImAgInS	být
film	film	k1gInSc1	film
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
filmových	filmový	k2eAgMnPc2d1	filmový
kritiků	kritik	k1gMnPc2	kritik
a	a	k8xC	a
publicistů	publicista	k1gMnPc2	publicista
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
nejvýznamnějším	významný	k2eAgInSc7d3	nejvýznamnější
filmem	film	k1gInSc7	film
stoleté	stoletý	k2eAgFnSc2d1	stoletá
historie	historie	k1gFnSc2	historie
české	český	k2eAgFnSc2d1	Česká
kinematografie	kinematografie	k1gFnSc2	kinematografie
<g/>
.	.	kIx.	.
</s>
<s>
Kritikou	kritika	k1gFnSc7	kritika
je	být	k5eAaImIp3nS	být
vysoce	vysoce	k6eAd1	vysoce
hodnocen	hodnotit	k5eAaImNgInS	hodnotit
hned	hned	k6eAd1	hned
po	po	k7c6	po
několika	několik	k4yIc6	několik
stránkách	stránka	k1gFnPc6	stránka
<g/>
:	:	kIx,	:
coby	coby	k?	coby
vrchol	vrchol	k1gInSc4	vrchol
československého	československý	k2eAgInSc2d1	československý
historického	historický	k2eAgInSc2d1	historický
filmu	film	k1gInSc2	film
<g/>
,	,	kIx,	,
poetického	poetický	k2eAgInSc2d1	poetický
filmu	film	k1gInSc2	film
a	a	k8xC	a
samotného	samotný	k2eAgNnSc2d1	samotné
umění	umění	k1gNnSc2	umění
adaptace	adaptace	k1gFnSc2	adaptace
z	z	k7c2	z
literární	literární	k2eAgFnSc2d1	literární
předlohy	předloha	k1gFnSc2	předloha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
Casablanca	Casablanca	k1gFnSc1	Casablanca
kniha	kniha	k1gFnSc1	kniha
Markéta	Markéta	k1gFnSc1	Markéta
Lazarova	Lazarův	k2eAgFnSc1d1	Lazarova
<g/>
.	.	kIx.	.
</s>
<s>
Studie	studie	k1gFnPc1	studie
a	a	k8xC	a
dokumenty	dokument	k1gInPc1	dokument
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
texty	text	k1gInPc4	text
rozebírající	rozebírající	k2eAgInSc4d1	rozebírající
Vláčilův	Vláčilův	k2eAgInSc4d1	Vláčilův
film	film	k1gInSc4	film
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
oborů	obor	k1gInPc2	obor
a	a	k8xC	a
úhlů	úhel	k1gInPc2	úhel
pohledu	pohled	k1gInSc2	pohled
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
i	i	k9	i
rozhovory	rozhovor	k1gInPc4	rozhovor
s	s	k7c7	s
tvůrci	tvůrce	k1gMnPc7	tvůrce
filmu	film	k1gInSc2	film
a	a	k8xC	a
reprodukce	reprodukce	k1gFnSc2	reprodukce
kostýmních	kostýmní	k2eAgInPc2d1	kostýmní
návrhů	návrh	k1gInPc2	návrh
Theodora	Theodor	k1gMnSc4	Theodor
Pištěka	Pištěk	k1gMnSc4	Pištěk
<g/>
.	.	kIx.	.
</s>
<s>
Editorem	editor	k1gMnSc7	editor
publikace	publikace	k1gFnSc2	publikace
je	být	k5eAaImIp3nS	být
Petr	Petr	k1gMnSc1	Petr
Gajdošík	gajdošík	k1gMnSc1	gajdošík
<g/>
.	.	kIx.	.
</s>
<s>
Role	role	k1gFnSc1	role
potulného	potulný	k2eAgMnSc2d1	potulný
mnicha	mnich	k1gMnSc2	mnich
Bernarda	Bernard	k1gMnSc2	Bernard
byla	být	k5eAaImAgFnS	být
původně	původně	k6eAd1	původně
svěřena	svěřit	k5eAaPmNgFnS	svěřit
slovenskému	slovenský	k2eAgMnSc3d1	slovenský
herci	herec	k1gMnSc3	herec
Gustávu	Gustáv	k1gMnSc3	Gustáv
Valachovi	Valach	k1gMnSc3	Valach
<g/>
.	.	kIx.	.
</s>
<s>
Vedení	vedení	k1gNnSc1	vedení
Slovenského	slovenský	k2eAgNnSc2d1	slovenské
národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
ho	on	k3xPp3gMnSc4	on
pro	pro	k7c4	pro
natáčení	natáčení	k1gNnSc4	natáčení
ale	ale	k8xC	ale
neuvolnilo	uvolnit	k5eNaPmAgNnS	uvolnit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1965	[number]	k4	1965
byl	být	k5eAaImAgMnS	být
osloven	osloven	k2eAgMnSc1d1	osloven
herec	herec	k1gMnSc1	herec
Vladimír	Vladimír	k1gMnSc1	Vladimír
Menšík	Menšík	k1gMnSc1	Menšík
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
úlohu	úloha	k1gFnSc4	úloha
přijal	přijmout	k5eAaPmAgMnS	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
znovuuvedení	znovuuvedení	k1gNnPc4	znovuuvedení
do	do	k7c2	do
německých	německý	k2eAgNnPc2d1	německé
kin	kino	k1gNnPc2	kino
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
byl	být	k5eAaImAgInS	být
film	film	k1gInSc1	film
Marketa	Market	k2eAgFnSc1d1	Marketa
Lazarová	Lazarová	k1gFnSc1	Lazarová
zdigitalizován	zdigitalizován	k2eAgInSc1d1	zdigitalizován
a	a	k8xC	a
upraven	upraven	k2eAgInSc1d1	upraven
pro	pro	k7c4	pro
široká	široký	k2eAgNnPc4d1	široké
plátna	plátno	k1gNnPc4	plátno
<g/>
.	.	kIx.	.
</s>
<s>
Deník	deník	k1gInSc1	deník
Frankfurter	Frankfurtra	k1gFnPc2	Frankfurtra
Allgemeine	Allgemein	k1gInSc5	Allgemein
Zeitung	Zeitung	k1gMnSc1	Zeitung
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
poznamenal	poznamenat	k5eAaPmAgMnS	poznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
film	film	k1gInSc1	film
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
zvolen	zvolit	k5eAaPmNgMnS	zvolit
jako	jako	k8xC	jako
nejlepší	dobrý	k2eAgInSc1d3	nejlepší
český	český	k2eAgInSc1d1	český
film	film	k1gInSc1	film
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
článku	článek	k1gInSc2	článek
dodal	dodat	k5eAaPmAgMnS	dodat
tuto	tento	k3xDgFnSc4	tento
úvahu	úvaha	k1gFnSc4	úvaha
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Pro	pro	k7c4	pro
národně	národně	k6eAd1	národně
nezaujatého	zaujatý	k2eNgMnSc2d1	nezaujatý
pozorovatele	pozorovatel	k1gMnSc2	pozorovatel
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
otevírají	otevírat	k5eAaImIp3nP	otevírat
dveře	dveře	k1gFnPc4	dveře
ku	k	k7c3	k
dvojité	dvojitý	k2eAgFnSc3d1	dvojitá
cestě	cesta	k1gFnSc3	cesta
časem	časem	k6eAd1	časem
<g/>
:	:	kIx,	:
do	do	k7c2	do
(	(	kIx(	(
<g/>
ve	v	k7c6	v
filmu	film	k1gInSc6	film
<g/>
)	)	kIx)	)
představeného	představený	k2eAgInSc2d1	představený
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgNnSc6	který
by	by	kYmCp3nS	by
Thomas	Thomas	k1gMnSc1	Thomas
Hobbes	Hobbes	k1gMnSc1	Hobbes
našel	najít	k5eAaPmAgMnS	najít
motiv	motiv	k1gInSc4	motiv
a	a	k8xC	a
inspiraci	inspirace	k1gFnSc4	inspirace
<g/>
,	,	kIx,	,
a	a	k8xC	a
do	do	k7c2	do
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
period	perioda	k1gFnPc2	perioda
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
během	během	k7c2	během
níž	jenž	k3xRgFnSc2	jenž
studená	studený	k2eAgFnSc1d1	studená
válka	válka	k1gFnSc1	válka
systémů	systém	k1gInPc2	systém
na	na	k7c6	na
chvíli	chvíle	k1gFnSc6	chvíle
poskytla	poskytnout	k5eAaPmAgFnS	poskytnout
mnoho	mnoho	k4c4	mnoho
možností	možnost	k1gFnPc2	možnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
(	(	kIx(	(
<g/>
však	však	k9	však
<g/>
)	)	kIx)	)
přišly	přijít	k5eAaPmAgInP	přijít
tanky	tank	k1gInPc1	tank
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
GAJDOŠÍK	gajdošík	k1gMnSc1	gajdošík
<g/>
,	,	kIx,	,
P.	P.	kA	P.
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
ed	ed	k?	ed
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Marketa	Market	k2eAgFnSc1d1	Marketa
Lazarová	Lazarová	k1gFnSc1	Lazarová
<g/>
.	.	kIx.	.
</s>
<s>
Studie	studie	k1gFnPc1	studie
a	a	k8xC	a
dokumenty	dokument	k1gInPc1	dokument
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Casablanca	Casablanca	k1gFnSc1	Casablanca
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
87292	[number]	k4	87292
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
PAVLÍČEK	Pavlíček	k1gMnSc1	Pavlíček
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
.	.	kIx.	.
</s>
<s>
Marketa	Marketa	k1gFnSc1	Marketa
Lazarova	Lazarův	k2eAgFnSc1d1	Lazarova
<g/>
.	.	kIx.	.
</s>
<s>
Filmový	filmový	k2eAgInSc1d1	filmový
epos	epos	k1gInSc1	epos
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
volně	volně	k6eAd1	volně
zpracoval	zpracovat	k5eAaPmAgInS	zpracovat
podle	podle	k7c2	podle
motivů	motiv	k1gInPc2	motiv
Obrazů	obraz	k1gInPc2	obraz
z	z	k7c2	z
dějin	dějiny	k1gFnPc2	dějiny
národa	národ	k1gInSc2	národ
českého	český	k2eAgInSc2d1	český
V.	V.	kA	V.
Vančury	Vančura	k1gMnPc7	Vančura
a	a	k8xC	a
stejnojmenného	stejnojmenný	k2eAgInSc2d1	stejnojmenný
románu	román	k1gInSc2	román
Marketa	Market	k2eAgFnSc1d1	Marketa
Lazarová	Lazarová	k1gFnSc1	Lazarová
F.	F.	kA	F.
Pavlíček	Pavlíček	k1gMnSc1	Pavlíček
a	a	k8xC	a
F.	F.	kA	F.
Vláčil	vláčit	k5eAaImAgMnS	vláčit
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Sdružen	sdružen	k2eAgInSc1d1	sdružen
přátel	přítel	k1gMnPc2	přítel
odborného	odborný	k2eAgInSc2d1	odborný
filmového	filmový	k2eAgInSc2d1	filmový
tisku	tisk	k1gInSc2	tisk
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7004	[number]	k4	7004
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
93	[number]	k4	93
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
KŘIVÁNKOVÁ	Křivánková	k1gFnSc1	Křivánková
<g/>
,	,	kIx,	,
Darina	Darina	k1gFnSc1	Darina
<g/>
.	.	kIx.	.
</s>
<s>
Markéta	Markéta	k1gFnSc1	Markéta
Lazarova	Lazarův	k2eAgFnSc1d1	Lazarova
<g/>
.	.	kIx.	.
</s>
<s>
Reflex	reflex	k1gInSc1	reflex
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
25	[number]	k4	25
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
34	[number]	k4	34
<g/>
,	,	kIx,	,
s.	s.	k?	s.
36	[number]	k4	36
<g/>
–	–	k?	–
<g/>
43	[number]	k4	43
<g/>
.	.	kIx.	.
</s>
<s>
Marketa	Market	k2eAgFnSc1d1	Marketa
Lazarová	Lazarová	k1gFnSc1	Lazarová
na	na	k7c6	na
Kinoboxu	Kinobox	k1gInSc6	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Marketa	Market	k2eAgFnSc1d1	Marketa
Lazarová	Lazarová	k1gFnSc1	Lazarová
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc3d1	filmová
databázi	databáze	k1gFnSc3	databáze
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Marketa	Market	k2eAgFnSc1d1	Marketa
Lazarová	Lazarová	k1gFnSc1	Lazarová
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
stránky	stránka	k1gFnSc2	stránka
věnované	věnovaný	k2eAgFnSc2d1	věnovaná
filmu	film	k1gInSc6	film
Marketa	Market	k2eAgFnSc1d1	Marketa
Lazarová	Lazarová	k1gFnSc1	Lazarová
-	-	kIx~	-
http://www.nostalghia.cz/marketa	[url]	k1gFnSc1	http://www.nostalghia.cz/marketa
</s>
