kilobázích	kilobáh	k1gInPc6	kilobáh
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
kbp	kbp	k?	kbp
-	-	kIx~	-
kilobase	kilobase	k6eAd1	kilobase
pair	pair	k1gMnSc1	pair
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
1000	[number]	k4	1000
bp	bp	k?	bp
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
megabázích	megabáze	k1gFnPc6	megabáze
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
Mbp	Mbp	k1gFnSc6	Mbp
-	-	kIx~	-
megabase	megabase	k6eAd1	megabase
pair	pair	k1gMnSc1	pair
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
milion	milion	k4xCgInSc1	milion
bp	bp	k?	bp
<g/>
)	)	kIx)	)
či	či	k8xC	či
gigabázích	gigabáze	k1gFnPc6	gigabáze
