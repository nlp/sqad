<p>
<s>
Vznik	vznik	k1gInSc1	vznik
společnosti	společnost	k1gFnSc2	společnost
Gardner	Gardnra	k1gFnPc2	Gardnra
Denver	Denver	k1gInSc1	Denver
<g/>
,	,	kIx,	,
Inc	Inc	k1gFnSc1	Inc
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
datuje	datovat	k5eAaImIp3nS	datovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1859	[number]	k4	1859
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
společnost	společnost	k1gFnSc1	společnost
Gardner	Gardnra	k1gFnPc2	Gardnra
Governor	Governora	k1gFnPc2	Governora
Company	Compana	k1gFnSc2	Compana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1927	[number]	k4	1927
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
spojení	spojení	k1gNnSc3	spojení
ze	z	k7c2	z
společností	společnost	k1gFnPc2	společnost
Denver	Denver	k1gInSc1	Denver
Rock	rock	k1gInSc1	rock
Drill	Drill	k1gInSc4	Drill
Company	Compana	k1gFnSc2	Compana
<g/>
.	.	kIx.	.
</s>
<s>
Stanovil	stanovit	k5eAaPmAgInS	stanovit
se	se	k3xPyFc4	se
nový	nový	k2eAgInSc1d1	nový
název	název	k1gInSc1	název
společnosti	společnost	k1gFnSc2	společnost
GARDNER	GARDNER	kA	GARDNER
DENVER	Denver	k1gInSc1	Denver
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
podnikla	podniknout	k5eAaPmAgFnS	podniknout
společnost	společnost	k1gFnSc1	společnost
mnoho	mnoho	k6eAd1	mnoho
významných	významný	k2eAgFnPc2d1	významná
akvizicí	akvizice	k1gFnPc2	akvizice
a	a	k8xC	a
podnikatelských	podnikatelský	k2eAgInPc2d1	podnikatelský
kroků	krok	k1gInPc2	krok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
trendu	trend	k1gInSc6	trend
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
pokračovat	pokračovat	k5eAaImF	pokračovat
i	i	k9	i
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
je	být	k5eAaImIp3nS	být
společnost	společnost	k1gFnSc1	společnost
zaměřena	zaměřit	k5eAaPmNgFnS	zaměřit
více	hodně	k6eAd2	hodně
na	na	k7c4	na
evropský	evropský	k2eAgInSc4d1	evropský
trh	trh	k1gInSc4	trh
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
posílila	posílit	k5eAaPmAgFnS	posílit
své	svůj	k3xOyFgNnSc4	svůj
postavení	postavení	k1gNnSc4	postavení
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Gardner	Gardner	k1gInSc1	Gardner
Denver	Denver	k1gInSc1	Denver
výrazně	výrazně	k6eAd1	výrazně
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
portfolio	portfolio	k1gNnSc4	portfolio
produktů	produkt	k1gInPc2	produkt
spojením	spojení	k1gNnSc7	spojení
25	[number]	k4	25
společností	společnost	k1gFnPc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
společnosti	společnost	k1gFnPc4	společnost
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
CompAir	CompAir	k1gInSc1	CompAir
<g/>
,	,	kIx,	,
Tamrotor	Tamrotor	k1gInSc1	Tamrotor
<g/>
,	,	kIx,	,
Champion	Champion	k1gInSc1	Champion
<g/>
,	,	kIx,	,
Bottarini	Bottarin	k1gMnPc1	Bottarin
a	a	k8xC	a
Belliss	Belliss	k1gInSc1	Belliss
<g/>
,	,	kIx,	,
Elmo	Elmo	k1gMnSc1	Elmo
Rietschle	Rietschle	k1gMnSc1	Rietschle
<g/>
,	,	kIx,	,
Wittig	Wittig	k1gMnSc1	Wittig
<g/>
&	&	k?	&
<g/>
Drum	Drum	k1gMnSc1	Drum
<g/>
,	,	kIx,	,
Butterworth	Butterworth	k1gMnSc1	Butterworth
<g/>
,	,	kIx,	,
systémy	systém	k1gInPc7	systém
Emco	Emco	k6eAd1	Emco
Wheaton	Wheaton	k1gInSc4	Wheaton
<g/>
,	,	kIx,	,
nakládací	nakládací	k2eAgNnPc1d1	nakládací
ramena	rameno	k1gNnPc1	rameno
a	a	k8xC	a
palivové	palivový	k2eAgInPc1d1	palivový
systémy	systém	k1gInPc1	systém
Todo	Todo	k6eAd1	Todo
<g/>
,	,	kIx,	,
Thomas	Thomas	k1gMnSc1	Thomas
&	&	k?	&
<g/>
ILMVAC	ILMVAC	kA	ILMVAC
OEM	OEM	kA	OEM
<g/>
,	,	kIx,	,
Robushi	Robushi	k1gNnSc2	Robushi
a	a	k8xC	a
Conroe	Conroe	k1gNnSc2	Conroe
Plastics	Plasticsa	k1gFnPc2	Plasticsa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
30.7	[number]	k4	30.7
<g/>
.2013	.2013	k4	.2013
společnost	společnost	k1gFnSc1	společnost
Gardner	Gardnra	k1gFnPc2	Gardnra
Denver	Denver	k1gInSc4	Denver
zakoupila	zakoupit	k5eAaPmAgFnS	zakoupit
akcie	akcie	k1gFnSc1	akcie
společnosti	společnost	k1gFnSc2	společnost
Kohlberg	Kohlberg	k1gInSc1	Kohlberg
Kravis	Kravis	k1gFnSc2	Kravis
Roberts	Robertsa	k1gFnPc2	Robertsa
&	&	k?	&
CO	co	k8xS	co
LP	LP	kA	LP
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
KKR	KKR	kA	KKR
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
KKR	KKR	kA	KKR
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
a	a	k8xC	a
postupem	postupem	k7c2	postupem
času	čas	k1gInSc2	čas
se	se	k3xPyFc4	se
rozrostla	rozrůst	k5eAaPmAgFnS	rozrůst
v	v	k7c4	v
globální	globální	k2eAgFnSc4d1	globální
společnost	společnost	k1gFnSc4	společnost
investující	investující	k2eAgNnSc1d1	investující
do	do	k7c2	do
různých	různý	k2eAgNnPc2d1	různé
průmyslových	průmyslový	k2eAgNnPc2d1	průmyslové
odvětví	odvětví	k1gNnPc2	odvětví
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
společnost	společnost	k1gFnSc1	společnost
Gardner	Gardner	k1gInSc4	Gardner
Denver	Denver	k1gInSc1	Denver
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
lídrů	lídr	k1gMnPc2	lídr
v	v	k7c6	v
průmyslových	průmyslový	k2eAgFnPc6d1	průmyslová
aplikací	aplikace	k1gFnPc2	aplikace
<g/>
.	.	kIx.	.
<g/>
Výhradní	výhradní	k2eAgNnSc1d1	výhradní
zastupitelství	zastupitelství	k1gNnSc1	zastupitelství
značky	značka	k1gFnSc2	značka
Gardner	Gardnra	k1gFnPc2	Gardnra
Denver	Denver	k1gInSc1	Denver
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Tamrotor	Tamrotor	k1gInSc1	Tamrotor
<g/>
)	)	kIx)	)
na	na	k7c6	na
českém	český	k2eAgInSc6d1	český
a	a	k8xC	a
slovenském	slovenský	k2eAgInSc6d1	slovenský
trhu	trh	k1gInSc6	trh
drží	držet	k5eAaImIp3nS	držet
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
společnost	společnost	k1gFnSc1	společnost
Veskom	Veskom	k1gInSc1	Veskom
<g/>
,	,	kIx,	,
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
1859	[number]	k4	1859
založil	založit	k5eAaPmAgMnS	založit
Roberet	Roberet	k1gMnSc1	Roberet
Gardner	Gardner	k1gMnSc1	Gardner
společnost	společnost	k1gFnSc4	společnost
nesoucí	nesoucí	k2eAgInSc4d1	nesoucí
název	název	k1gInSc4	název
Gardner	Gardnra	k1gFnPc2	Gardnra
Governor	Governora	k1gFnPc2	Governora
Company	Compana	k1gFnSc2	Compana
a	a	k8xC	a
představil	představit	k5eAaPmAgInS	představit
inovační	inovační	k2eAgNnSc4d1	inovační
řešení	řešení	k1gNnSc4	řešení
pro	pro	k7c4	pro
účinnou	účinný	k2eAgFnSc4d1	účinná
kontrolu	kontrola	k1gFnSc4	kontrola
rychlosti	rychlost	k1gFnSc2	rychlost
pro	pro	k7c4	pro
parní	parní	k2eAgFnPc4d1	parní
lokomotivy	lokomotiva	k1gFnPc4	lokomotiva
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
krokem	krok	k1gInSc7	krok
připravil	připravit	k5eAaPmAgInS	připravit
cestu	cesta	k1gFnSc4	cesta
pro	pro	k7c4	pro
průmyslovou	průmyslový	k2eAgFnSc4d1	průmyslová
výrobu	výroba	k1gFnSc4	výroba
kompresorů	kompresor	k1gInPc2	kompresor
</s>
</p>
<p>
<s>
1927	[number]	k4	1927
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
spojení	spojení	k1gNnSc3	spojení
ze	z	k7c2	z
společností	společnost	k1gFnPc2	společnost
Denver	Denver	k1gInSc4	Denver
Rock	rock	k1gInSc1	rock
Drill	Drilla	k1gFnPc2	Drilla
Company	Compana	k1gFnSc2	Compana
a	a	k8xC	a
vzniká	vznikat	k5eAaImIp3nS	vznikat
nový	nový	k2eAgInSc4d1	nový
název	název	k1gInSc4	název
Gardner	Gardner	k1gInSc1	Gardner
Denver	Denver	k1gInSc1	Denver
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1959	[number]	k4	1959
v	v	k7c6	v
poválečném	poválečný	k2eAgNnSc6d1	poválečné
období	období	k1gNnSc6	období
Gardner	Gardnra	k1gFnPc2	Gardnra
Denver	Denver	k1gInSc4	Denver
dělá	dělat	k5eAaImIp3nS	dělat
řadu	řada	k1gFnSc4	řada
významných	významný	k2eAgInPc2d1	významný
kroků	krok	k1gInPc2	krok
a	a	k8xC	a
začíná	začínat	k5eAaImIp3nS	začínat
vyrábět	vyrábět	k5eAaImF	vyrábět
spirálové	spirálový	k2eAgInPc4d1	spirálový
šroubové	šroubový	k2eAgInPc4d1	šroubový
kompresory	kompresor	k1gInPc4	kompresor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1979	[number]	k4	1979
společnost	společnost	k1gFnSc1	společnost
Gardner	Gardnra	k1gFnPc2	Gardnra
Denver	Denver	k1gInSc4	Denver
získává	získávat	k5eAaImIp3nS	získávat
společnost	společnost	k1gFnSc1	společnost
COOPER	COOPER	kA	COOPER
Industries	Industries	k1gMnSc1	Industries
<g/>
,	,	kIx,	,
Inc	Inc	k1gMnSc1	Inc
</s>
</p>
<p>
<s>
1985	[number]	k4	1985
Spojení	spojení	k1gNnSc1	spojení
dvou	dva	k4xCgFnPc2	dva
divizí	divize	k1gFnPc2	divize
Gardner	Gardner	k1gInSc4	Gardner
Denver	Denver	k1gInSc4	Denver
Kompresory	kompresor	k1gInPc4	kompresor
a	a	k8xC	a
Petroleum	Petroleum	k1gInSc4	Petroleum
Equipment	Equipment	k1gInSc1	Equipment
Division	Division	k1gInSc1	Division
</s>
</p>
<p>
<s>
31.12	[number]	k4	31.12
<g/>
.1993	.1993	k4	.1993
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
Gardner	Gardner	k1gInSc1	Gardner
Denver	Denver	k1gInSc1	Denver
100	[number]	k4	100
<g/>
%	%	kIx~	%
dceřinou	dceřin	k2eAgFnSc7d1	dceřina
společností	společnost	k1gFnSc7	společnost
Cooper	Cooper	k1gMnSc1	Cooper
</s>
</p>
<p>
<s>
15.4	[number]	k4	15.4
<g/>
.1994	.1994	k4	.1994
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
rozděleni	rozdělen	k2eAgMnPc1d1	rozdělen
a	a	k8xC	a
Gardner	Gardner	k1gMnSc1	Gardner
Denver	Denver	k1gInSc4	Denver
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
nezávislou	závislý	k2eNgFnSc7d1	nezávislá
společností	společnost	k1gFnSc7	společnost
</s>
</p>
<p>
<s>
2001	[number]	k4	2001
získává	získávat	k5eAaImIp3nS	získávat
britského	britský	k2eAgMnSc4d1	britský
výrobce	výrobce	k1gMnSc4	výrobce
kompresorů	kompresor	k1gInPc2	kompresor
HAMWORTHY	HAMWORTHY	kA	HAMWORTHY
</s>
</p>
<p>
<s>
2004	[number]	k4	2004
Gardner	Gardner	k1gInSc1	Gardner
Denver	Denver	k1gInSc4	Denver
kupuje	kupovat	k5eAaImIp3nS	kupovat
Siltone	Silton	k1gInSc5	Silton
a	a	k8xC	a
podniky	podnik	k1gInPc7	podnik
EMCO	EMCO	kA	EMCO
Wheaton	Wheaton	k1gInSc4	Wheaton
divize	divize	k1gFnSc2	divize
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
působí	působit	k5eAaImIp3nS	působit
jako	jako	k9	jako
člen	člen	k1gMnSc1	člen
skupiny	skupina	k1gFnSc2	skupina
Gardner	Gardner	k1gMnSc1	Gardner
Denver	Denver	k1gInSc1	Denver
Energy	Energ	k1gMnPc4	Energ
</s>
</p>
<p>
<s>
2009	[number]	k4	2009
Gardner	Gardner	k1gInSc1	Gardner
Denver	Denver	k1gInSc4	Denver
<g/>
,	,	kIx,	,
Inc	Inc	k1gFnSc4	Inc
oslavil	oslavit	k5eAaPmAgInS	oslavit
výročí	výročí	k1gNnSc4	výročí
150	[number]	k4	150
let	léto	k1gNnPc2	léto
od	od	k7c2	od
svého	svůj	k3xOyFgNnSc2	svůj
založení	založení	k1gNnSc2	založení
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Gardner	Gardner	k1gInSc1	Gardner
Denver	Denver	k1gInSc1	Denver
CZ	CZ	kA	CZ
</s>
</p>
<p>
<s>
Gardner	Gardner	k1gInSc1	Gardner
Denver	Denver	k1gInSc1	Denver
<g/>
,	,	kIx,	,
Inc	Inc	k1gFnSc1	Inc
<g/>
.	.	kIx.	.
</s>
<s>
Corporate	Corporat	k1gMnSc5	Corporat
</s>
</p>
