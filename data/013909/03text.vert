<s>
2	#num#	k4
<g/>
.	.	kIx.
pobaltský	pobaltský	k2eAgInSc1d1
front	front	k1gInSc1
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
pobaltský	pobaltský	k2eAgInSc1d1
front	front	k1gInSc1
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
2	#num#	k4
<g/>
-й	-й	k?
П	П	k?
ф	ф	k?
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
název	název	k1gInSc1
vojenské	vojenský	k2eAgFnSc2d1
formace	formace	k1gFnSc2
Rudé	rudý	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
za	za	k7c2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
pobaltský	pobaltský	k2eAgInSc1d1
front	front	k1gInSc1
vznikl	vzniknout	k5eAaPmAgInS
20	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1943	#num#	k4
na	na	k7c6
základě	základ	k1gInSc6
rozkazu	rozkaz	k1gInSc2
Stavky	Stavka	k1gFnSc2
z	z	k7c2
16	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
přejmenováním	přejmenování	k1gNnSc7
Pobaltského	pobaltský	k2eAgInSc2d1
frontu	front	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
až	až	k9
21	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1943	#num#	k4
levé	levý	k2eAgNnSc4d1
křídlo	křídlo	k1gNnSc4
společně	společně	k6eAd1
s	s	k7c7
1	#num#	k4
<g/>
.	.	kIx.
pobaltským	pobaltský	k2eAgInSc7d1
frontem	front	k1gInSc7
útočilo	útočit	k5eAaImAgNnS
na	na	k7c6
vitebsko	vitebsko	k6eAd1
<g/>
–	–	k?
<g/>
polockém	polocký	k2eAgInSc6d1
směru	směr	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Začátkem	začátkem	k7c2
roku	rok	k1gInSc2
1944	#num#	k4
se	se	k3xPyFc4
front	front	k1gInSc1
účastnil	účastnit	k5eAaImAgInS
Leningradsko	Leningradsko	k1gNnSc4
<g/>
–	–	k?
<g/>
novgorodské	novgorodský	k2eAgFnPc4d1
operace	operace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vojska	vojsko	k1gNnPc1
frontu	fronta	k1gFnSc4
postoupila	postoupit	k5eAaPmAgFnS
o	o	k7c4
110	#num#	k4
160	#num#	k4
km	km	kA
až	až	k8xS
k	k	k7c3
Ostrovu	ostrov	k1gInSc3
a	a	k8xC
Idrici	Idrice	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
červenci	červenec	k1gInSc6
a	a	k8xC
srpnu	srpen	k1gInSc6
front	fronta	k1gFnPc2
postoupil	postoupit	k5eAaPmAgMnS
ještě	ještě	k9
o	o	k7c4
250	#num#	k4
km	km	kA
na	na	k7c4
západ	západ	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
září	září	k1gNnSc6
a	a	k8xC
říjnu	říjen	k1gInSc6
1944	#num#	k4
front	fronta	k1gFnPc2
v	v	k7c6
Rižské	rižský	k2eAgFnSc6d1
operaci	operace	k1gFnSc6
společně	společně	k6eAd1
s	s	k7c7
1	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
3	#num#	k4
<g/>
.	.	kIx.
pobaltským	pobaltský	k2eAgInSc7d1
frontem	front	k1gInSc7
obsadil	obsadit	k5eAaPmAgInS
Rigu	Riga	k1gFnSc4
a	a	k8xC
zablokoval	zablokovat	k5eAaPmAgInS
německou	německý	k2eAgFnSc4d1
skupinu	skupina	k1gFnSc4
armád	armáda	k1gFnPc2
Sever	sever	k1gInSc4
v	v	k7c6
Kuronsku	Kuronsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Němce	Němec	k1gMnSc2
v	v	k7c6
Kuronsku	Kuronsko	k1gNnSc6
front	fronta	k1gFnPc2
blokoval	blokovat	k5eAaImAgInS
až	až	k6eAd1
do	do	k7c2
dubna	duben	k1gInSc2
1945	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1945	#num#	k4
byl	být	k5eAaImAgInS
front	front	k1gInSc1
zrušen	zrušit	k5eAaPmNgInS
podle	podle	k7c2
rozhodnutí	rozhodnutí	k1gNnSc2
Stavky	Stavka	k1gFnSc2
z	z	k7c2
29	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1945	#num#	k4
<g/>
,	,	kIx,
vojska	vojsko	k1gNnSc2
byla	být	k5eAaImAgFnS
včleněna	včlenit	k5eAaPmNgFnS
do	do	k7c2
Leningradského	leningradský	k2eAgInSc2d1
frontu	front	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Podřízené	podřízený	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
armáda	armáda	k1gFnSc1
(	(	kIx(
<g/>
20	#num#	k4
<g/>
.	.	kIx.
–	–	k?
23	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1943	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
armáda	armáda	k1gFnSc1
(	(	kIx(
<g/>
20	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
–	–	k?
5	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1943	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
armáda	armáda	k1gFnSc1
(	(	kIx(
<g/>
20	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1943	#num#	k4
–	–	k?
1	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1945	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
úderná	úderný	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
(	(	kIx(
<g/>
20	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1943	#num#	k4
–	–	k?
15	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1944	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
gardová	gardový	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
(	(	kIx(
<g/>
20	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1943	#num#	k4
–	–	k?
jaro	jaro	k1gNnSc1
1944	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
gardová	gardový	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
(	(	kIx(
<g/>
20	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
–	–	k?
18	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1943	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
letecká	letecký	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
(	(	kIx(
<g/>
20	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1943	#num#	k4
–	–	k?
1	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1945	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úderná	úderný	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
(	(	kIx(
<g/>
20	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1943	#num#	k4
–	–	k?
2	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1944	#num#	k4
a	a	k8xC
16	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
–	–	k?
7	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1944	#num#	k4
a	a	k8xC
16	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1944	#num#	k4
–	–	k?
1	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1945	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
gardová	gardový	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
(	(	kIx(
<g/>
9	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1943	#num#	k4
–	–	k?
1	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1945	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
úderná	úderný	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
(	(	kIx(
<g/>
4	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
–	–	k?
8	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1944	#num#	k4
a	a	k8xC
9	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
–	–	k?
1	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1945	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
42	#num#	k4
<g/>
.	.	kIx.
armáda	armáda	k1gFnSc1
(	(	kIx(
<g/>
10	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1944	#num#	k4
–	–	k?
1	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1945	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
letecká	letecký	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
(	(	kIx(
<g/>
17	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
–	–	k?
27	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1944	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
51	#num#	k4
<g/>
.	.	kIx.
armáda	armáda	k1gFnSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
–	–	k?
1	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1945	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Velení	velení	k1gNnSc1
</s>
<s>
Velitel	velitel	k1gMnSc1
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1943	#num#	k4
–	–	k?
23	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1944	#num#	k4
armádní	armádní	k2eAgNnSc4d1
generál	generál	k1gMnSc1
(	(	kIx(
<g/>
od	od	k7c2
20	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1944	#num#	k4
generálplukovník	generálplukovník	k1gMnSc1
<g/>
)	)	kIx)
Markian	Markian	k1gMnSc1
Michajlovič	Michajlovič	k1gMnSc1
Popov	Popov	k1gInSc4
</s>
<s>
23	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1944	#num#	k4
–	–	k?
4	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1945	#num#	k4
armádní	armádní	k2eAgMnPc1d1
generál	generál	k1gMnSc1
Andrej	Andrej	k1gMnSc1
Ivanovič	Ivanovič	k1gMnSc1
Jerjomenko	Jerjomenka	k1gFnSc5
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
–	–	k?
9	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1945	#num#	k4
generálplukovník	generálplukovník	k1gMnSc1
Markian	Markian	k1gMnSc1
Michajlovič	Michajlovič	k1gMnSc1
Popov	Popov	k1gInSc4
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
–	–	k?
1	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1945	#num#	k4
maršál	maršál	k1gMnSc1
Sovětského	sovětský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
Leonid	Leonid	k1gInSc1
Alexandrovič	Alexandrovič	k1gMnSc1
Govorov	Govorov	k1gInSc1
</s>
<s>
Člen	člen	k1gMnSc1
vojenské	vojenský	k2eAgFnSc2d1
rady	rada	k1gFnSc2
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
–	–	k?
prosinec	prosinec	k1gInSc1
1943	#num#	k4
generálporučík	generálporučík	k1gMnSc1
Lev	Lev	k1gMnSc1
Zacharovič	Zacharovič	k1gMnSc1
Mechlis	Mechlis	k1gFnSc4
</s>
<s>
prosinec	prosinec	k1gInSc1
1943	#num#	k4
–	–	k?
duben	duben	k1gInSc1
1944	#num#	k4
generálporučík	generálporučík	k1gMnSc1
Nikolaj	Nikolaj	k1gMnSc1
Alexandrovič	Alexandrovič	k1gMnSc1
Bulganin	Bulganin	k2eAgMnSc1d1
</s>
<s>
duben	duben	k1gInSc1
1944	#num#	k4
–	–	k?
1	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1945	#num#	k4
generálporučík	generálporučík	k1gMnSc1
Vladimir	Vladimir	k1gMnSc1
Nikolajevič	Nikolajevič	k1gMnSc1
Bogatkin	Bogatkin	k1gMnSc1
</s>
<s>
Náčelník	náčelník	k1gInSc1
štábu	štáb	k1gInSc2
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1943	#num#	k4
–	–	k?
březen	březen	k1gInSc4
1945	#num#	k4
generálporučík	generálporučík	k1gMnSc1
(	(	kIx(
<g/>
od	od	k7c2
23	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1944	#num#	k4
generálplukovník	generálplukovník	k1gMnSc1
<g/>
)	)	kIx)
Leonid	Leonid	k1gInSc1
Michajlovič	Michajlovič	k1gInSc1
Sandalov	Sandalov	k1gInSc4
</s>
<s>
březen	březen	k1gInSc1
–	–	k?
1	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1945	#num#	k4
generálplukovník	generálplukovník	k1gMnSc1
Markian	Markian	k1gMnSc1
Michajlovič	Michajlovič	k1gMnSc1
Popov	Popov	k1gInSc4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
60	#num#	k4
л	л	k?
в	в	k?
п	п	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moskva	Moskva	k1gFnSc1
<g/>
:	:	kIx,
М	М	k?
о	о	k?
Р	Р	k?
Ф	Ф	k?
<g/>
,	,	kIx,
2004-2005	2004-2005	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Ф	Ф	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
"	"	kIx"
<g/>
П	П	k?
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Voroněž	Voroněž	k1gFnSc1
<g/>
:	:	kIx,
В	В	k?
к	к	k?
"	"	kIx"
<g/>
П	П	k?
<g/>
"	"	kIx"
п	п	k?
В	В	k?
г	г	k?
у	у	k?
<g/>
,	,	kIx,
2000-10	2000-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
2	#num#	k4
<g/>
-й	-й	k?
П	П	k?
ф	ф	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
</s>
<s>
'	'	kIx"
</s>
<s>
Nástupce	nástupce	k1gMnSc1
</s>
<s>
Pobaltský	pobaltský	k2eAgInSc1d1
front	front	k1gInSc1
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
pobaltský	pobaltský	k2eAgInSc1d1
front	front	k1gInSc1
20	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1943	#num#	k4
1	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1945	#num#	k4
</s>
<s>
včleněn	včleněn	k2eAgMnSc1d1
do	do	k7c2
Leningradského	leningradský	k2eAgInSc2d1
frontu	front	k1gInSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc4
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc4
<g/>
:	:	kIx,
Druhá	druhý	k4xOgFnSc1
světová	světový	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
