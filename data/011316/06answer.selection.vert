<s>
Nefrologie	Nefrologie	k1gFnSc1	Nefrologie
je	být	k5eAaImIp3nS	být
odvětví	odvětví	k1gNnSc4	odvětví
medicíny	medicína	k1gFnSc2	medicína
zabývající	zabývající	k2eAgFnSc2d1	zabývající
se	se	k3xPyFc4	se
diagnózou	diagnóza	k1gFnSc7	diagnóza
a	a	k8xC	a
léčbou	léčba	k1gFnSc7	léčba
ledvin	ledvina	k1gFnPc2	ledvina
<g/>
,	,	kIx,	,
transplantací	transplantace	k1gFnPc2	transplantace
ledvin	ledvina	k1gFnPc2	ledvina
a	a	k8xC	a
dialýzou	dialýza	k1gFnSc7	dialýza
<g/>
.	.	kIx.	.
</s>
