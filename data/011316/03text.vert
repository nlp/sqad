<p>
<s>
Nefrologie	Nefrologie	k1gFnSc1	Nefrologie
je	být	k5eAaImIp3nS	být
odvětví	odvětví	k1gNnSc4	odvětví
medicíny	medicína	k1gFnSc2	medicína
zabývající	zabývající	k2eAgFnSc2d1	zabývající
se	se	k3xPyFc4	se
diagnózou	diagnóza	k1gFnSc7	diagnóza
a	a	k8xC	a
léčbou	léčba	k1gFnSc7	léčba
ledvin	ledvina	k1gFnPc2	ledvina
<g/>
,	,	kIx,	,
transplantací	transplantace	k1gFnPc2	transplantace
ledvin	ledvina	k1gFnPc2	ledvina
a	a	k8xC	a
dialýzou	dialýza	k1gFnSc7	dialýza
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Termín	termín	k1gInSc1	termín
"	"	kIx"	"
<g/>
nefrologie	nefrologie	k1gFnPc1	nefrologie
<g/>
"	"	kIx"	"
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
užit	užít	k5eAaPmNgInS	užít
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
<g/>
.	.	kIx.	.
</s>
<s>
Předtím	předtím	k6eAd1	předtím
byl	být	k5eAaImAgInS	být
obor	obor	k1gInSc1	obor
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
ledvinové	ledvinový	k2eAgNnSc1d1	ledvinové
lékařství	lékařství	k1gNnSc1	lékařství
<g/>
"	"	kIx"	"
<g/>
K	k	k7c3	k
lékařům	lékař	k1gMnPc3	lékař
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
oborem	obor	k1gInSc7	obor
zabývali	zabývat	k5eAaImAgMnP	zabývat
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
a	a	k8xC	a
za	za	k7c4	za
zakladatele	zakladatel	k1gMnSc4	zakladatel
jsou	být	k5eAaImIp3nP	být
považováni	považován	k2eAgMnPc1d1	považován
Richard	Richard	k1gMnSc1	Richard
Bright	Bright	k1gMnSc1	Bright
(	(	kIx(	(
<g/>
1789	[number]	k4	1789
<g/>
-	-	kIx~	-
<g/>
1858	[number]	k4	1858
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Franz	Franz	k1gMnSc1	Franz
Volhard	Volhard	k1gMnSc1	Volhard
(	(	kIx(	(
<g/>
1872	[number]	k4	1872
<g/>
–	–	k?	–
<g/>
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
a	a	k8xC	a
Georg	Georg	k1gInSc1	Georg
Haas	Haasa	k1gFnPc2	Haasa
(	(	kIx(	(
<g/>
1886	[number]	k4	1886
<g/>
–	–	k?	–
<g/>
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Urologie	urologie	k1gFnSc1	urologie
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
nefrologie	nefrologie	k1gFnSc2	nefrologie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
