<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Finska	Finsko	k1gNnSc2	Finsko
<g/>
,	,	kIx,	,
zvaná	zvaný	k2eAgFnSc1d1	zvaná
také	také	k6eAd1	také
Siniristilippu	Siniristilipp	k1gInSc3	Siniristilipp
(	(	kIx(	(
<g/>
vlajka	vlajka	k1gFnSc1	vlajka
s	s	k7c7	s
modrým	modrý	k2eAgInSc7d1	modrý
křížem	kříž	k1gInSc7	kříž
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
podobu	podoba	k1gFnSc4	podoba
bílého	bílý	k2eAgInSc2d1	bílý
obdélníkového	obdélníkový	k2eAgInSc2d1	obdélníkový
listu	list	k1gInSc2	list
s	s	k7c7	s
modrým	modrý	k2eAgInSc7d1	modrý
skandinávským	skandinávský	k2eAgInSc7d1	skandinávský
křížem	kříž	k1gInSc7	kříž
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhled	k1gInSc7	vzhled
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
dánské	dánský	k2eAgFnSc2d1	dánská
vlajky	vlajka	k1gFnSc2	vlajka
a	a	k8xC	a
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
začátku	začátek	k1gInSc2	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Modrá	modrý	k2eAgFnSc1d1	modrá
barva	barva	k1gFnSc1	barva
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
finská	finský	k2eAgNnPc4d1	finské
jezera	jezero	k1gNnPc4	jezero
<g/>
,	,	kIx,	,
bílá	bílý	k2eAgFnSc1d1	bílá
barva	barva	k1gFnSc1	barva
sníh	sníh	k1gInSc1	sníh
<g/>
.	.	kIx.	.
<g/>
Státní	státní	k2eAgFnSc1d1	státní
vlajka	vlajka	k1gFnSc1	vlajka
má	mít	k5eAaImIp3nS	mít
navíc	navíc	k6eAd1	navíc
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
kříže	kříž	k1gInSc2	kříž
umístěn	umístit	k5eAaPmNgInS	umístit
státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
<g/>
.	.	kIx.	.
</s>
<s>
Námořní	námořní	k2eAgFnSc1d1	námořní
vlajka	vlajka	k1gFnSc1	vlajka
má	mít	k5eAaImIp3nS	mít
vzhled	vzhled	k1gInSc4	vzhled
odvozený	odvozený	k2eAgInSc4d1	odvozený
od	od	k7c2	od
státní	státní	k2eAgFnSc2d1	státní
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
,	,	kIx,	,
obdélníkový	obdélníkový	k2eAgInSc1d1	obdélníkový
list	list	k1gInSc1	list
je	být	k5eAaImIp3nS	být
však	však	k9	však
zakončen	zakončit	k5eAaPmNgInS	zakončit
třemi	tři	k4xCgInPc7	tři
plameny	plamen	k1gInPc7	plamen
<g/>
,	,	kIx,	,
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
je	být	k5eAaImIp3nS	být
odvozena	odvozen	k2eAgFnSc1d1	odvozena
prezidentská	prezidentský	k2eAgFnSc1d1	prezidentská
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
navíc	navíc	k6eAd1	navíc
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
v	v	k7c6	v
kantonu	kanton	k1gInSc6	kanton
kříž	kříž	k1gInSc1	kříž
Svobody	svoboda	k1gFnSc2	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Námořní	námořní	k2eAgFnSc1d1	námořní
vlajka	vlajka	k1gFnSc1	vlajka
má	mít	k5eAaImIp3nS	mít
poměr	poměr	k1gInSc4	poměr
stran	strana	k1gFnPc2	strana
11	[number]	k4	11
<g/>
:	:	kIx,	:
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
tvar	tvar	k1gInSc1	tvar
znaku	znak	k1gInSc2	znak
na	na	k7c6	na
těchto	tento	k3xDgFnPc6	tento
služebních	služební	k2eAgFnPc6d1	služební
vlajkách	vlajka	k1gFnPc6	vlajka
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
byl	být	k5eAaImAgInS	být
změněn	změnit	k5eAaPmNgInS	změnit
na	na	k7c4	na
čtvercový	čtvercový	k2eAgInSc4d1	čtvercový
a	a	k8xC	a
současně	současně	k6eAd1	současně
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
byl	být	k5eAaImAgInS	být
zvolen	zvolen	k2eAgInSc4d1	zvolen
tmavší	tmavý	k2eAgInSc4d2	tmavší
odstín	odstín	k1gInSc4	odstín
modré	modrý	k2eAgNnSc1d1	modré
<g/>
.	.	kIx.	.
<g/>
Pro	pro	k7c4	pro
modrou	modrý	k2eAgFnSc4d1	modrá
barvu	barva	k1gFnSc4	barva
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
předepsán	předepsat	k5eAaPmNgInS	předepsat
odstín	odstín	k1gInSc1	odstín
Pantone	Panton	k1gInSc5	Panton
294	[number]	k4	294
<g/>
c	c	k0	c
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
přibližně	přibližně	k6eAd1	přibližně
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
CMYK	CMYK	kA	CMYK
100,56	[number]	k4	100,56
<g/>
,0	,0	k4	,0
<g/>
,18	,18	k4	,18
<g/>
.5	.5	k4	.5
či	či	k8xC	či
RGB	RGB	kA	RGB
#	#	kIx~	#
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
3882	[number]	k4	3882
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
červenou	červená	k1gFnSc4	červená
186	[number]	k4	186
<g/>
c	c	k0	c
a	a	k8xC	a
pro	pro	k7c4	pro
žlutou	žlutý	k2eAgFnSc4d1	žlutá
123	[number]	k4	123
<g/>
c.	c.	k?	c.
<g/>
Úpravy	úprava	k1gFnSc2	úprava
paragrafů	paragraf	k1gInPc2	paragraf
o	o	k7c4	o
hanobení	hanobení	k1gNnSc4	hanobení
vlajky	vlajka	k1gFnSc2	vlajka
platí	platit	k5eAaImIp3nS	platit
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Od	od	k7c2	od
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
začalo	začít	k5eAaPmAgNnS	začít
postupně	postupně	k6eAd1	postupně
Švédsko	Švédsko	k1gNnSc1	Švédsko
připojovat	připojovat	k5eAaImF	připojovat
dnešní	dnešní	k2eAgNnSc4d1	dnešní
finské	finský	k2eAgNnSc4d1	finské
území	území	k1gNnSc4	území
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1284	[number]	k4	1284
bylo	být	k5eAaImAgNnS	být
celé	celý	k2eAgNnSc1d1	celé
toto	tento	k3xDgNnSc1	tento
území	území	k1gNnSc1	území
připojeno	připojit	k5eAaPmNgNnS	připojit
jako	jako	k8xS	jako
vévodství	vévodství	k1gNnSc1	vévodství
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1577	[number]	k4	1577
bylo	být	k5eAaImAgNnS	být
vyhlášeno	vyhlásit	k5eAaPmNgNnS	vyhlásit
velkovévodstvím	velkovévodství	k1gNnSc7	velkovévodství
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
vlajky	vlajka	k1gFnPc1	vlajka
vyvěšované	vyvěšovaný	k2eAgFnPc1d1	vyvěšovaná
na	na	k7c6	na
finském	finský	k2eAgNnSc6d1	finské
území	území	k1gNnSc6	území
byly	být	k5eAaImAgFnP	být
tedy	tedy	k9	tedy
modro-žluté	modro-žlutý	k2eAgFnPc1d1	modro-žlutá
vlajky	vlajka	k1gFnPc1	vlajka
Švédského	švédský	k2eAgNnSc2d1	švédské
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
finské	finský	k2eAgFnSc6d1	finská
válce	válka	k1gFnSc6	válka
(	(	kIx(	(
<g/>
nazývané	nazývaný	k2eAgNnSc1d1	nazývané
též	též	k9	též
rusko-švédská	rusko-švédský	k2eAgNnPc1d1	rusko-švédský
<g/>
)	)	kIx)	)
v	v	k7c6	v
letech	let	k1gInPc6	let
1808	[number]	k4	1808
<g/>
–	–	k?	–
<g/>
1809	[number]	k4	1809
bylo	být	k5eAaImAgNnS	být
Finsko	Finsko	k1gNnSc1	Finsko
postoupeno	postoupen	k2eAgNnSc1d1	postoupeno
Ruskému	ruský	k2eAgInSc3d1	ruský
impériu	impérium	k1gNnSc6	impérium
a	a	k8xC	a
29	[number]	k4	29
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1809	[number]	k4	1809
bylo	být	k5eAaImAgNnS	být
švédské	švédský	k2eAgNnSc1d1	švédské
Finské	finský	k2eAgNnSc1d1	finské
velkovévodství	velkovévodství	k1gNnSc1	velkovévodství
přeměněno	přeměnit	k5eAaPmNgNnS	přeměnit
na	na	k7c4	na
ruské	ruský	k2eAgFnPc4d1	ruská
autonomní	autonomní	k2eAgFnPc4d1	autonomní
(	(	kIx(	(
<g/>
s	s	k7c7	s
vlastním	vlastní	k2eAgInSc7d1	vlastní
parlamentem	parlament	k1gInSc7	parlament
<g/>
)	)	kIx)	)
Finské	finský	k2eAgNnSc1d1	finské
velkoknížectví	velkoknížectví	k1gNnSc1	velkoknížectví
<g/>
.	.	kIx.	.
</s>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
se	se	k3xPyFc4	se
změnila	změnit	k5eAaPmAgFnS	změnit
na	na	k7c4	na
vlajku	vlajka	k1gFnSc4	vlajka
ruskou	ruský	k2eAgFnSc4d1	ruská
<g/>
.	.	kIx.	.
<g/>
Ruská	ruský	k2eAgFnSc1d1	ruská
carská	carský	k2eAgFnSc1d1	carská
vláda	vláda	k1gFnSc1	vláda
byla	být	k5eAaImAgFnS	být
přinucena	přinutit	k5eAaPmNgFnS	přinutit
po	po	k7c6	po
sílícím	sílící	k2eAgInSc6d1	sílící
tlaku	tlak	k1gInSc6	tlak
na	na	k7c4	na
samostatnost	samostatnost	k1gFnSc4	samostatnost
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
rozšířit	rozšířit	k5eAaPmF	rozšířit
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
směrech	směr	k1gInPc6	směr
autonomii	autonomie	k1gFnSc4	autonomie
<g/>
.	.	kIx.	.
</s>
<s>
Objevily	objevit	k5eAaPmAgInP	objevit
se	se	k3xPyFc4	se
první	první	k4xOgInPc1	první
návrhy	návrh	k1gInPc1	návrh
finské	finský	k2eAgFnSc2d1	finská
vlajky	vlajka	k1gFnSc2	vlajka
ovlivněné	ovlivněný	k2eAgNnSc1d1	ovlivněné
státním	státní	k2eAgInSc7d1	státní
znakem	znak	k1gInSc7	znak
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
užíván	užívat	k5eAaImNgInS	užívat
i	i	k9	i
za	za	k7c2	za
ruské	ruský	k2eAgFnSc2d1	ruská
nadvlády	nadvláda	k1gFnSc2	nadvláda
<g/>
.	.	kIx.	.
</s>
<s>
Navrženy	navržen	k2eAgFnPc1d1	navržena
byly	být	k5eAaImAgFnP	být
např.	např.	kA	např.
vlajky	vlajka	k1gFnPc4	vlajka
s	s	k7c7	s
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
9	[number]	k4	9
žluto-červenými	žluto-červený	k2eAgInPc7d1	žluto-červený
nebo	nebo	k8xC	nebo
bílo-modrými	bíloodrý	k2eAgInPc7d1	bílo-modrý
vodorovnými	vodorovný	k2eAgInPc7d1	vodorovný
pruhy	pruh	k1gInPc7	pruh
se	s	k7c7	s
státním	státní	k2eAgInSc7d1	státní
znakem	znak	k1gInSc7	znak
v	v	k7c6	v
kantonu	kanton	k1gInSc6	kanton
<g/>
.	.	kIx.	.
</s>
<s>
Finský	finský	k2eAgMnSc1d1	finský
dramatik	dramatik	k1gMnSc1	dramatik
a	a	k8xC	a
historik	historik	k1gMnSc1	historik
Zacharias	Zacharias	k1gMnSc1	Zacharias
Topelius	Topelius	k1gMnSc1	Topelius
vyvěšoval	vyvěšovat	k5eAaImAgMnS	vyvěšovat
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
lodi	loď	k1gFnSc6	loď
variantu	varianta	k1gFnSc4	varianta
této	tento	k3xDgFnSc2	tento
vlajky	vlajka	k1gFnSc2	vlajka
s	s	k7c7	s
dvěma	dva	k4xCgInPc7	dva
modrobílými	modrobílý	k2eAgInPc7d1	modrobílý
pruhy	pruh	k1gInPc7	pruh
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
významu	význam	k1gInSc6	význam
barev	barva	k1gFnPc2	barva
řekl	říct	k5eAaPmAgInS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Bílá	bílý	k2eAgFnSc1d1	bílá
je	být	k5eAaImIp3nS	být
barva	barva	k1gFnSc1	barva
našich	náš	k3xOp1gFnPc2	náš
zim	zima	k1gFnPc2	zima
<g/>
,	,	kIx,	,
modrá	modrý	k2eAgFnSc1d1	modrá
je	být	k5eAaImIp3nS	být
barvou	barva	k1gFnSc7	barva
našich	náš	k3xOp1gNnPc2	náš
jezer	jezero	k1gNnPc2	jezero
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1863	[number]	k4	1863
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
finskou	finský	k2eAgFnSc4d1	finská
vlajku	vlajka	k1gFnSc4	vlajka
s	s	k7c7	s
pěti	pět	k4xCc7	pět
modrobílými	modrobílý	k2eAgInPc7d1	modrobílý
kosými	kosý	k2eAgInPc7d1	kosý
pruhy	pruh	k1gInPc7	pruh
s	s	k7c7	s
bílou	bílý	k2eAgFnSc7d1	bílá
pěticípou	pěticípý	k2eAgFnSc7d1	pěticípá
hvězdou	hvězda	k1gFnSc7	hvězda
umístěnou	umístěný	k2eAgFnSc7d1	umístěná
uprostřed	uprostřed	k7c2	uprostřed
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
.	.	kIx.	.
<g/>
Největší	veliký	k2eAgFnSc4d3	veliký
podporu	podpora	k1gFnSc4	podpora
ale	ale	k8xC	ale
získala	získat	k5eAaPmAgFnS	získat
vlajka	vlajka	k1gFnSc1	vlajka
tvořená	tvořený	k2eAgFnSc1d1	tvořená
bílým	bílý	k2eAgInSc7d1	bílý
listem	list	k1gInSc7	list
s	s	k7c7	s
modrým	modrý	k2eAgInSc7d1	modrý
křížem	kříž	k1gInSc7	kříž
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
symbolem	symbol	k1gInSc7	symbol
finského	finský	k2eAgNnSc2d1	finské
vlastenectví	vlastenectví	k1gNnSc2	vlastenectví
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
tuto	tento	k3xDgFnSc4	tento
vlajku	vlajka	k1gFnSc4	vlajka
se	se	k3xPyFc4	se
zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
paradoxně	paradoxně	k6eAd1	paradoxně
ruský	ruský	k2eAgMnSc1d1	ruský
car	car	k1gMnSc1	car
Alexandr	Alexandr	k1gMnSc1	Alexandr
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Nikolajevič	Nikolajevič	k1gMnSc1	Nikolajevič
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
4	[number]	k4	4
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1861	[number]	k4	1861
udělil	udělit	k5eAaPmAgInS	udělit
Nylandskému	Nylandský	k2eAgInSc3d1	Nylandský
jachtařskému	jachtařský	k2eAgInSc3d1	jachtařský
klubu	klub	k1gInSc3	klub
v	v	k7c6	v
Helsinkách	Helsinky	k1gFnPc6	Helsinky
bílou	bílý	k2eAgFnSc4d1	bílá
vlajku	vlajka	k1gFnSc4	vlajka
s	s	k7c7	s
modrým	modrý	k2eAgInSc7d1	modrý
křížem	kříž	k1gInSc7	kříž
a	a	k8xC	a
znakem	znak	k1gInSc7	znak
provincie	provincie	k1gFnSc2	provincie
Nyland	Nyland	k1gInSc1	Nyland
v	v	k7c6	v
kantonu	kanton	k1gInSc6	kanton
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vlajka	vlajka	k1gFnSc1	vlajka
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
legálně	legálně	k6eAd1	legálně
vyvěšována	vyvěšován	k2eAgFnSc1d1	vyvěšována
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
<g/>
.	.	kIx.	.
<g/>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1917	[number]	k4	1917
získalo	získat	k5eAaPmAgNnS	získat
Finsko	Finsko	k1gNnSc1	Finsko
nezávislost	nezávislost	k1gFnSc1	nezávislost
(	(	kIx(	(
<g/>
vyhlášena	vyhlášen	k2eAgFnSc1d1	vyhlášena
byla	být	k5eAaImAgFnS	být
senátem	senát	k1gInSc7	senát
17	[number]	k4	17
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
<g/>
,	,	kIx,	,
schválena	schválen	k2eAgFnSc1d1	schválena
parlamentem	parlament	k1gInSc7	parlament
6	[number]	k4	6
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
a	a	k8xC	a
uznána	uznat	k5eAaPmNgFnS	uznat
Ruskem	Rusko	k1gNnSc7	Rusko
4	[number]	k4	4
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1918	[number]	k4	1918
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
přijata	přijmout	k5eAaPmNgFnS	přijmout
provizorní	provizorní	k2eAgFnSc1d1	provizorní
vlajka	vlajka	k1gFnSc1	vlajka
tvořená	tvořený	k2eAgFnSc1d1	tvořená
červeným	červený	k2eAgInSc7d1	červený
listem	list	k1gInSc7	list
se	s	k7c7	s
zlatobílým	zlatobílý	k2eAgMnSc7d1	zlatobílý
finským	finský	k2eAgMnSc7d1	finský
lvem	lev	k1gMnSc7	lev
s	s	k7c7	s
mečem	meč	k1gInSc7	meč
<g/>
,	,	kIx,	,
bílými	bílý	k2eAgFnPc7d1	bílá
růžemi	růž	k1gFnPc7	růž
a	a	k8xC	a
šavlí	šavle	k1gFnSc7	šavle
pod	pod	k7c7	pod
lvem	lev	k1gMnSc7	lev
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
vlajka	vlajka	k1gFnSc1	vlajka
vyvěšena	vyvěsit	k5eAaPmNgFnS	vyvěsit
právě	právě	k6eAd1	právě
6	[number]	k4	6
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
před	před	k7c7	před
budovou	budova	k1gFnSc7	budova
Senátu	senát	k1gInSc2	senát
<g/>
.	.	kIx.	.
<g/>
Mezi	mezi	k7c4	mezi
28	[number]	k4	28
<g/>
.	.	kIx.	.
lednem	leden	k1gInSc7	leden
a	a	k8xC	a
25	[number]	k4	25
<g/>
.	.	kIx.	.
dubnem	duben	k1gInSc7	duben
1918	[number]	k4	1918
existovala	existovat	k5eAaImAgFnS	existovat
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
země	zem	k1gFnSc2	zem
krátce	krátce	k6eAd1	krátce
Finská	finský	k2eAgFnSc1d1	finská
socialistická	socialistický	k2eAgFnSc1d1	socialistická
dělnická	dělnický	k2eAgFnSc1d1	Dělnická
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
státní	státní	k2eAgFnSc7d1	státní
vlajkou	vlajka	k1gFnSc7	vlajka
byl	být	k5eAaImAgInS	být
červený	červený	k2eAgInSc1d1	červený
list	list	k1gInSc1	list
o	o	k7c6	o
poměru	poměr	k1gInSc6	poměr
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
německých	německý	k2eAgNnPc2d1	německé
vojsk	vojsko	k1gNnPc2	vojsko
byla	být	k5eAaImAgFnS	být
bolševická	bolševický	k2eAgFnSc1d1	bolševická
revoluce	revoluce	k1gFnSc1	revoluce
potlačena	potlačit	k5eAaPmNgFnS	potlačit
a	a	k8xC	a
stát	stát	k1gInSc1	stát
rozpuštěn	rozpuštěn	k2eAgInSc1d1	rozpuštěn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
finské	finský	k2eAgFnSc2d1	finská
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
byla	být	k5eAaImAgFnS	být
27	[number]	k4	27
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1918	[number]	k4	1918
schválena	schválen	k2eAgFnSc1d1	schválena
národní	národní	k2eAgFnSc1d1	národní
a	a	k8xC	a
obchodní	obchodní	k2eAgFnSc1d1	obchodní
vlajka	vlajka	k1gFnSc1	vlajka
(	(	kIx(	(
<g/>
poměr	poměr	k1gInSc1	poměr
stran	stran	k7c2	stran
vlajky	vlajka	k1gFnSc2	vlajka
17	[number]	k4	17
<g/>
:	:	kIx,	:
<g/>
26	[number]	k4	26
byl	být	k5eAaImAgInS	být
shodný	shodný	k2eAgInSc1d1	shodný
se	s	k7c7	s
státní	státní	k2eAgFnSc7d1	státní
vlajkou	vlajka	k1gFnSc7	vlajka
<g/>
)	)	kIx)	)
tvořená	tvořený	k2eAgNnPc4d1	tvořené
červeným	červený	k2eAgInSc7d1	červený
listem	list	k1gInSc7	list
se	s	k7c7	s
žlutým	žlutý	k2eAgInSc7d1	žlutý
křížem	kříž	k1gInSc7	kříž
<g/>
,	,	kIx,	,
modro-bíle	modroíle	k6eAd1	modro-bíle
lemovaným	lemovaný	k2eAgMnPc3d1	lemovaný
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
revoluci	revoluce	k1gFnSc6	revoluce
a	a	k8xC	a
občanské	občanský	k2eAgFnSc6d1	občanská
válce	válka	k1gFnSc6	válka
byla	být	k5eAaImAgFnS	být
rudá	rudý	k2eAgFnSc1d1	rudá
<g/>
/	/	kIx~	/
<g/>
červená	červený	k2eAgFnSc1d1	červená
barva	barva	k1gFnSc1	barva
velmi	velmi	k6eAd1	velmi
neoblíbená	oblíbený	k2eNgFnSc1d1	neoblíbená
<g/>
.	.	kIx.	.
29	[number]	k4	29
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1918	[number]	k4	1918
byla	být	k5eAaImAgFnS	být
parlamentem	parlament	k1gInSc7	parlament
schválena	schválen	k2eAgFnSc1d1	schválena
nová	nový	k2eAgFnSc1d1	nová
vlajka	vlajka	k1gFnSc1	vlajka
s	s	k7c7	s
bílým	bílý	k2eAgInSc7d1	bílý
listem	list	k1gInSc7	list
o	o	k7c6	o
poměru	poměr	k1gInSc6	poměr
11	[number]	k4	11
<g/>
:	:	kIx,	:
<g/>
18	[number]	k4	18
se	se	k3xPyFc4	se
světle	světle	k6eAd1	světle
modrým	modrý	k2eAgInSc7d1	modrý
skandinávským	skandinávský	k2eAgInSc7d1	skandinávský
křížem	kříž	k1gInSc7	kříž
<g/>
.	.	kIx.	.
</s>
<s>
Vlajku	vlajka	k1gFnSc4	vlajka
navrhli	navrhnout	k5eAaPmAgMnP	navrhnout
finští	finský	k2eAgMnPc1d1	finský
umělci	umělec	k1gMnPc1	umělec
Eero	Eero	k1gMnSc1	Eero
Snellman	Snellman	k1gMnSc1	Snellman
a	a	k8xC	a
Bruno	Bruno	k1gMnSc1	Bruno
Tuukkanen	Tuukkanen	k1gInSc4	Tuukkanen
<g/>
.17	.17	k4	.17
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1919	[number]	k4	1919
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
Finská	finský	k2eAgFnSc1d1	finská
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
souvislosti	souvislost	k1gFnSc6	souvislost
byly	být	k5eAaImAgFnP	být
12	[number]	k4	12
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1920	[number]	k4	1920
upraveny	upraven	k2eAgInPc4d1	upraven
státní	státní	k2eAgInPc4d1	státní
symboly	symbol	k1gInPc4	symbol
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
finského	finský	k2eAgInSc2d1	finský
znaku	znak	k1gInSc2	znak
byla	být	k5eAaImAgFnS	být
odstraněna	odstraněn	k2eAgFnSc1d1	odstraněna
koruna	koruna	k1gFnSc1	koruna
a	a	k8xC	a
změnil	změnit	k5eAaPmAgInS	změnit
se	se	k3xPyFc4	se
tvar	tvar	k1gInSc1	tvar
štítu	štít	k1gInSc2	štít
<g/>
.	.	kIx.	.
</s>
<s>
Barva	barva	k1gFnSc1	barva
kříže	kříž	k1gInSc2	kříž
byla	být	k5eAaImAgFnS	být
změněna	změnit	k5eAaPmNgFnS	změnit
na	na	k7c4	na
tmavší	tmavý	k2eAgInSc4d2	tmavší
odstín	odstín	k1gInSc4	odstín
modré	modrý	k2eAgNnSc1d1	modré
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
upřesněna	upřesněn	k2eAgFnSc1d1	upřesněna
byla	být	k5eAaImAgFnS	být
až	až	k9	až
25	[number]	k4	25
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1925	[number]	k4	1925
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
Státní	státní	k2eAgFnSc2d1	státní
rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
<g/>
S	s	k7c7	s
účinností	účinnost	k1gFnSc7	účinnost
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1978	[number]	k4	1978
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
poslední	poslední	k2eAgFnSc3d1	poslední
změně	změna	k1gFnSc3	změna
finské	finský	k2eAgFnSc2d1	finská
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vlajce	vlajka	k1gFnSc6	vlajka
již	již	k6eAd1	již
není	být	k5eNaImIp3nS	být
státní	státní	k2eAgInSc4d1	státní
znak	znak	k1gInSc4	znak
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
znakové	znakový	k2eAgNnSc1d1	znakové
znamení	znamení	k1gNnSc1	znamení
státního	státní	k2eAgInSc2d1	státní
znaku	znak	k1gInSc2	znak
na	na	k7c6	na
průmětu	průmět	k1gInSc6	průmět
obou	dva	k4xCgNnPc2	dva
ramen	rameno	k1gNnPc2	rameno
kříže	kříž	k1gInSc2	kříž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Prezidentská	prezidentský	k2eAgFnSc1d1	prezidentská
vlajka	vlajka	k1gFnSc1	vlajka
==	==	k?	==
</s>
</p>
<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
finského	finský	k2eAgMnSc2d1	finský
prezidenta	prezident	k1gMnSc2	prezident
se	se	k3xPyFc4	se
od	od	k7c2	od
finské	finský	k2eAgFnSc2d1	finská
vlajky	vlajka	k1gFnSc2	vlajka
liší	lišit	k5eAaImIp3nP	lišit
křížem	kříž	k1gInSc7	kříž
Svobody	svoboda	k1gFnSc2	svoboda
umístěným	umístěný	k2eAgInPc3d1	umístěný
v	v	k7c6	v
horním	horní	k2eAgInSc6d1	horní
rohu	roh	k1gInSc6	roh
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
protokolu	protokol	k1gInSc2	protokol
nosí	nosit	k5eAaImIp3nS	nosit
finský	finský	k2eAgMnSc1d1	finský
prezident	prezident	k1gMnSc1	prezident
při	při	k7c6	při
slavnostních	slavnostní	k2eAgFnPc6d1	slavnostní
příležitostech	příležitost	k1gFnPc6	příležitost
kříž	kříž	k1gInSc4	kříž
Svobody	svoboda	k1gFnSc2	svoboda
s	s	k7c7	s
řetězem	řetěz	k1gInSc7	řetěz
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
kříž	kříž	k1gInSc1	kříž
je	být	k5eAaImIp3nS	být
vlastně	vlastně	k9	vlastně
stylizovanou	stylizovaný	k2eAgFnSc7d1	stylizovaná
svastikou	svastika	k1gFnSc7	svastika
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
však	však	k9	však
nemá	mít	k5eNaImIp3nS	mít
nic	nic	k3yNnSc1	nic
společného	společný	k2eAgNnSc2d1	společné
s	s	k7c7	s
nacionálním	nacionální	k2eAgInSc7d1	nacionální
socialismem	socialismus	k1gInSc7	socialismus
<g/>
:	:	kIx,	:
6	[number]	k4	6
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1918	[number]	k4	1918
věnoval	věnovat	k5eAaPmAgInS	věnovat
nově	nově	k6eAd1	nově
vznikajícím	vznikající	k2eAgFnPc3d1	vznikající
finským	finský	k2eAgFnPc3d1	finská
leteckým	letecký	k2eAgFnPc3d1	letecká
silám	síla	k1gFnPc3	síla
Švéd	švéda	k1gFnPc2	švéda
Eric	Eric	k1gFnSc1	Eric
von	von	k1gInSc4	von
Rosen	rosen	k2eAgInSc1d1	rosen
své	svůj	k3xOyFgFnSc3	svůj
jediné	jediný	k2eAgNnSc1d1	jediné
letadlo	letadlo	k1gNnSc1	letadlo
(	(	kIx(	(
<g/>
francouzský	francouzský	k2eAgMnSc1d1	francouzský
Morane-Saulnier	Morane-Saulnier	k1gMnSc1	Morane-Saulnier
Type	typ	k1gInSc5	typ
L	L	kA	L
<g/>
)	)	kIx)	)
proti	proti	k7c3	proti
rudým	rudý	k2eAgMnPc3d1	rudý
Finům	Fin	k1gMnPc3	Fin
podporovaných	podporovaný	k2eAgInPc2d1	podporovaný
ze	z	k7c2	z
sousedního	sousední	k2eAgInSc2d1	sousední
bolševického	bolševický	k2eAgInSc2d1	bolševický
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
křídla	křídlo	k1gNnPc4	křídlo
nechal	nechat	k5eAaPmAgInS	nechat
dárce	dárce	k1gMnSc1	dárce
namalovat	namalovat	k5eAaPmF	namalovat
část	část	k1gFnSc4	část
z	z	k7c2	z
rodového	rodový	k2eAgInSc2d1	rodový
znaku	znak	k1gInSc2	znak
a	a	k8xC	a
osobní	osobní	k2eAgInSc4d1	osobní
symbol	symbol	k1gInSc4	symbol
štěstí	štěstí	k1gNnSc2	štěstí
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
ho	on	k3xPp3gMnSc4	on
nazýval	nazývat	k5eAaImAgMnS	nazývat
<g/>
,	,	kIx,	,
modrou	modrý	k2eAgFnSc4d1	modrá
svastiku	svastika	k1gFnSc4	svastika
(	(	kIx(	(
<g/>
modrá	modrý	k2eAgFnSc1d1	modrá
je	být	k5eAaImIp3nS	být
finskou	finský	k2eAgFnSc7d1	finská
národní	národní	k2eAgFnSc7d1	národní
barvou	barva	k1gFnSc7	barva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Vlajky	vlajka	k1gFnPc4	vlajka
autonomií	autonomie	k1gFnPc2	autonomie
a	a	k8xC	a
etnických	etnický	k2eAgFnPc2d1	etnická
skupin	skupina	k1gFnPc2	skupina
==	==	k?	==
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1954	[number]	k4	1954
se	se	k3xPyFc4	se
na	na	k7c6	na
Å	Å	k?	Å
souostroví	souostroví	k1gNnSc6	souostroví
<g/>
,	,	kIx,	,
autonomní	autonomní	k2eAgFnSc2d1	autonomní
a	a	k8xC	a
demilitarizované	demilitarizovaný	k2eAgFnSc2d1	demilitarizovaná
součástí	součást	k1gFnSc7	součást
Finska	Finsko	k1gNnSc2	Finsko
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
vlastní	vlastní	k2eAgFnSc1d1	vlastní
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Sámové	Sámo	k1gMnPc1	Sámo
(	(	kIx(	(
<g/>
Laponci	Laponec	k1gMnPc1	Laponec
<g/>
)	)	kIx)	)
používají	používat	k5eAaImIp3nP	používat
současnou	současný	k2eAgFnSc4d1	současná
sámskou	sámský	k2eAgFnSc4d1	sámská
vlajku	vlajka	k1gFnSc4	vlajka
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1986	[number]	k4	1986
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
vlajku	vlajka	k1gFnSc4	vlajka
mají	mít	k5eAaImIp3nP	mít
i	i	k9	i
některé	některý	k3yIgFnPc1	některý
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
etnických	etnický	k2eAgFnPc2d1	etnická
skupin	skupina	k1gFnPc2	skupina
žijících	žijící	k2eAgFnPc2d1	žijící
na	na	k7c6	na
území	území	k1gNnSc6	území
Finska	Finsko	k1gNnSc2	Finsko
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Den	den	k1gInSc4	den
finské	finský	k2eAgFnSc2d1	finská
vlajky	vlajka	k1gFnSc2	vlajka
==	==	k?	==
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1934	[number]	k4	1934
se	se	k3xPyFc4	se
každoročně	každoročně	k6eAd1	každoročně
v	v	k7c4	v
den	den	k1gInSc4	den
letního	letní	k2eAgInSc2d1	letní
slunovratu	slunovrat	k1gInSc2	slunovrat
(	(	kIx(	(
<g/>
okolo	okolo	k7c2	okolo
21	[number]	k4	21
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
)	)	kIx)	)
slaví	slavit	k5eAaImIp3nS	slavit
Den	den	k1gInSc4	den
finské	finský	k2eAgFnSc2d1	finská
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yQgInSc6	který
jsou	být	k5eAaImIp3nP	být
vztyčovány	vztyčován	k2eAgFnPc4d1	vztyčována
národní	národní	k2eAgFnPc4d1	národní
vlajky	vlajka	k1gFnPc4	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
tento	tento	k3xDgInSc4	tento
den	den	k1gInSc4	den
jsou	být	k5eAaImIp3nP	být
vlajky	vlajka	k1gFnPc1	vlajka
vyvěšovány	vyvěšován	k2eAgFnPc1d1	vyvěšována
i	i	k9	i
během	během	k7c2	během
státních	státní	k2eAgInPc2d1	státní
svátků	svátek	k1gInPc2	svátek
a	a	k8xC	a
významných	významný	k2eAgInPc2d1	významný
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
(	(	kIx(	(
<g/>
i	i	k9	i
při	při	k7c6	při
dni	den	k1gInSc6	den
finské	finský	k2eAgFnSc2d1	finská
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vyvěšení	vyvěšení	k1gNnSc4	vyvěšení
na	na	k7c6	na
úředních	úřední	k2eAgFnPc6d1	úřední
budovách	budova	k1gFnPc6	budova
nařízeno	nařídit	k5eAaPmNgNnS	nařídit
zákonem	zákon	k1gInSc7	zákon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
Finska	Finsko	k1gNnSc2	Finsko
</s>
</p>
<p>
<s>
Finská	finský	k2eAgFnSc1d1	finská
hymna	hymna	k1gFnSc1	hymna
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Finska	Finsko	k1gNnSc2	Finsko
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Finská	finský	k2eAgFnSc1d1	finská
vlajka	vlajka	k1gFnSc1	vlajka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
