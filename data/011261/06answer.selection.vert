<s>
Systém	systém	k1gInSc1	systém
tvoří	tvořit	k5eAaImIp3nS	tvořit
především	především	k9	především
8	[number]	k4	8
planet	planeta	k1gFnPc2	planeta
<g/>
,	,	kIx,	,
5	[number]	k4	5
trpasličích	trpasličí	k2eAgFnPc2d1	trpasličí
planet	planeta	k1gFnPc2	planeta
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
150	[number]	k4	150
měsíců	měsíc	k1gInPc2	měsíc
planet	planeta	k1gFnPc2	planeta
(	(	kIx(	(
<g/>
především	především	k9	především
u	u	k7c2	u
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
,	,	kIx,	,
Saturnu	Saturn	k1gInSc2	Saturn
<g/>
,	,	kIx,	,
Uranu	Uran	k1gInSc2	Uran
a	a	k8xC	a
Neptuna	Neptun	k1gMnSc2	Neptun
<g/>
)	)	kIx)	)
a	a	k8xC	a
další	další	k2eAgNnPc1d1	další
menší	malý	k2eAgNnPc1d2	menší
tělesa	těleso	k1gNnPc1	těleso
jako	jako	k8xS	jako
planetky	planetka	k1gFnPc1	planetka
<g/>
,	,	kIx,	,
komety	kometa	k1gFnPc1	kometa
<g/>
,	,	kIx,	,
meteoroidy	meteoroida	k1gFnPc1	meteoroida
apod.	apod.	kA	apod.
</s>
