<s>
Hyrynsalmi	Hyrynsal	k1gFnPc7
</s>
<s>
Hyrynsalmi	Hyrynsal	k1gFnPc7
</s>
<s>
znak	znak	k1gInSc1
</s>
<s>
Poloha	poloha	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
64	#num#	k4
<g/>
°	°	k?
<g/>
40	#num#	k4
<g/>
′	′	k?
<g/>
30	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
28	#num#	k4
<g/>
°	°	k?
<g/>
29	#num#	k4
<g/>
′	′	k?
<g/>
30	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Časové	časový	k2eAgNnSc1d1
pásmo	pásmo	k1gNnSc1
</s>
<s>
EET	EET	kA
<g/>
/	/	kIx~
<g/>
EEST	EEST	kA
Stát	stát	k1gInSc1
</s>
<s>
Finsko	Finsko	k1gNnSc1
Finsko	Finsko	k1gNnSc1
provincie	provincie	k1gFnSc2
</s>
<s>
Kainuu	Kainuu	k6eAd1
</s>
<s>
Rozloha	rozloha	k1gFnSc1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
1	#num#	k4
521,51	521,51	k4
km²	km²	k?
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
2	#num#	k4
492	#num#	k4
(	(	kIx(
<g/>
30.09	30.09	k4
<g/>
.2014	.2014	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
1.8	1.8	k4
obyv	obyv	k1gInSc1
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
Správa	správa	k1gFnSc1
Status	status	k1gInSc1
</s>
<s>
město	město	k1gNnSc1
Starosta	Starosta	k1gMnSc1
</s>
<s>
Heimo	Heimo	k6eAd1
Keränen	Keränen	k2eAgInSc1d1
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
www.hyrynsalmi.fi	www.hyrynsalmi.fi	k6eAd1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Hyrynsalmi	Hyrynsalmi	k1gFnSc1
je	být	k5eAaImIp3nS
obec	obec	k1gFnSc1
ve	v	k7c6
Finsku	Finsko	k1gNnSc6
v	v	k7c6
provincii	provincie	k1gFnSc6
Kainuu	Kainuu	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
měla	mít	k5eAaImAgFnS
2874	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Sousedí	sousedit	k5eAaImIp3nS
s	s	k7c7
obcemi	obec	k1gFnPc7
Kuhmo	Kuhma	k1gFnSc5
(	(	kIx(
<g/>
JV	JV	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Suomussalmi	Suomussal	k1gFnPc7
(	(	kIx(
<g/>
SV	sv	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Puolanka	Puolanka	k1gFnSc1
(	(	kIx(
<g/>
Z	Z	kA
<g/>
)	)	kIx)
a	a	k8xC
Ristijärvi	Ristijärev	k1gFnSc6
(	(	kIx(
<g/>
JZ	JZ	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Hyrynsalmi	Hyrynsal	k1gFnPc7
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
1	#num#	k4
2	#num#	k4
Výsledky	výsledek	k1gInPc1
sčítání	sčítání	k1gNnSc2
lidu	lid	k1gInSc2
(	(	kIx(
<g/>
finsky	finsky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Finsko	Finsko	k1gNnSc1
</s>
