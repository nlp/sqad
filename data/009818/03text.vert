<p>
<s>
Sociologie	sociologie	k1gFnSc1	sociologie
je	být	k5eAaImIp3nS	být
společenská	společenský	k2eAgFnSc1d1	společenská
věda	věda	k1gFnSc1	věda
zkoumající	zkoumající	k2eAgFnSc2d1	zkoumající
sociální	sociální	k2eAgInSc4d1	sociální
život	život	k1gInSc4	život
jednotlivců	jednotlivec	k1gMnPc2	jednotlivec
<g/>
,	,	kIx,	,
skupin	skupina	k1gFnPc2	skupina
a	a	k8xC	a
společností	společnost	k1gFnPc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
obsahu	obsah	k1gInSc6	obsah
sociologie	sociologie	k1gFnSc2	sociologie
a	a	k8xC	a
vymezení	vymezení	k1gNnSc4	vymezení
předmětu	předmět	k1gInSc2	předmět
jejího	její	k3xOp3gNnSc2	její
zkoumání	zkoumání	k1gNnSc2	zkoumání
neexistuje	existovat	k5eNaImIp3nS	existovat
shoda	shoda	k1gFnSc1	shoda
napříč	napříč	k7c7	napříč
různými	různý	k2eAgFnPc7d1	různá
sociologickými	sociologický	k2eAgFnPc7d1	sociologická
školami	škola	k1gFnPc7	škola
a	a	k8xC	a
paradigmaty	paradigma	k1gNnPc7	paradigma
<g/>
.	.	kIx.	.
<g/>
Sociologie	sociologie	k1gFnSc1	sociologie
je	být	k5eAaImIp3nS	být
chápána	chápán	k2eAgFnSc1d1	chápána
např.	např.	kA	např.
jako	jako	k8xS	jako
věda	věda	k1gFnSc1	věda
o	o	k7c4	o
jednání	jednání	k1gNnPc4	jednání
lidí	člověk	k1gMnPc2	člověk
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
<g/>
,	,	kIx,	,
o	o	k7c6	o
struktuře	struktura	k1gFnSc6	struktura
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
o	o	k7c6	o
sociální	sociální	k2eAgFnSc6d1	sociální
interakci	interakce	k1gFnSc6	interakce
<g/>
,	,	kIx,	,
o	o	k7c6	o
sociálních	sociální	k2eAgFnPc6d1	sociální
skupinách	skupina	k1gFnPc6	skupina
<g/>
,	,	kIx,	,
o	o	k7c6	o
sociálních	sociální	k2eAgInPc6d1	sociální
faktech	fakt	k1gInPc6	fakt
<g/>
,	,	kIx,	,
o	o	k7c6	o
společenském	společenský	k2eAgInSc6d1	společenský
systému	systém	k1gInSc6	systém
a	a	k8xC	a
nebo	nebo	k8xC	nebo
o	o	k7c6	o
sociální	sociální	k2eAgFnSc6d1	sociální
změně	změna	k1gFnSc6	změna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dějiny	dějiny	k1gFnPc1	dějiny
sociologie	sociologie	k1gFnPc1	sociologie
==	==	k?	==
</s>
</p>
<p>
<s>
Sociologie	sociologie	k1gFnSc1	sociologie
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
formovat	formovat	k5eAaImF	formovat
až	až	k9	až
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
otce	otec	k1gMnSc2	otec
sociologie	sociologie	k1gFnSc2	sociologie
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
francouzský	francouzský	k2eAgMnSc1d1	francouzský
filosof	filosof	k1gMnSc1	filosof
Auguste	August	k1gMnSc5	August
Comte	Comt	k1gMnSc5	Comt
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
také	také	k9	také
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1838	[number]	k4	1838
poprvé	poprvé	k6eAd1	poprvé
užívá	užívat	k5eAaImIp3nS	užívat
slovo	slovo	k1gNnSc1	slovo
sociologie	sociologie	k1gFnSc2	sociologie
(	(	kIx(	(
<g/>
z	z	k7c2	z
latinského	latinský	k2eAgInSc2d1	latinský
socius	socius	k1gMnSc1	socius
=	=	kIx~	=
společník	společník	k1gMnSc1	společník
nebo	nebo	k8xC	nebo
societas	societas	k1gMnSc1	societas
=	=	kIx~	=
společnost	společnost	k1gFnSc1	společnost
a	a	k8xC	a
řeckého	řecký	k2eAgNnSc2d1	řecké
logos	logos	k1gInSc1	logos
=	=	kIx~	=
slovo	slovo	k1gNnSc1	slovo
<g/>
,	,	kIx,	,
výklad	výklad	k1gInSc1	výklad
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Comtova	Comtův	k2eAgFnSc1d1	Comtova
sociologie	sociologie	k1gFnSc1	sociologie
je	být	k5eAaImIp3nS	být
pojímána	pojímán	k2eAgFnSc1d1	pojímána
jako	jako	k8xC	jako
pozitivní	pozitivní	k2eAgFnSc1d1	pozitivní
věda	věda	k1gFnSc1	věda
o	o	k7c6	o
společenském	společenský	k2eAgInSc6d1	společenský
pokroku	pokrok	k1gInSc6	pokrok
stojící	stojící	k2eAgFnSc1d1	stojící
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
hierarchie	hierarchie	k1gFnSc2	hierarchie
věd	věda	k1gFnPc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
jako	jako	k8xS	jako
Comte	Comt	k1gInSc5	Comt
zveřejňují	zveřejňovat	k5eAaImIp3nP	zveřejňovat
své	svůj	k3xOyFgFnPc4	svůj
úvahy	úvaha	k1gFnPc4	úvaha
o	o	k7c6	o
vztazích	vztah	k1gInPc6	vztah
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
a	a	k8xC	a
společenském	společenský	k2eAgInSc6d1	společenský
vývoji	vývoj	k1gInSc6	vývoj
Herbert	Herbert	k1gMnSc1	Herbert
Spencer	Spencer	k1gMnSc1	Spencer
a	a	k8xC	a
Karel	Karel	k1gMnSc1	Karel
Marx	Marx	k1gMnSc1	Marx
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
těchto	tento	k3xDgFnPc2	tento
tří	tři	k4xCgFnPc2	tři
osobností	osobnost	k1gFnPc2	osobnost
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
šestice	šestice	k1gFnSc2	šestice
sociologických	sociologický	k2eAgMnPc2d1	sociologický
klasiků	klasik	k1gMnPc2	klasik
ještě	ještě	k6eAd1	ještě
o	o	k7c4	o
generaci	generace	k1gFnSc4	generace
mladší	mladý	k2eAgNnSc1d2	mladší
Vilfredo	Vilfredo	k1gNnSc1	Vilfredo
Pareto	Paret	k2eAgNnSc1d1	Paret
<g/>
,	,	kIx,	,
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Tönnies	Tönnies	k1gMnSc1	Tönnies
<g/>
,	,	kIx,	,
Émile	Émile	k1gFnSc1	Émile
Durkheim	Durkheim	k1gMnSc1	Durkheim
<g/>
,	,	kIx,	,
Max	Max	k1gMnSc1	Max
Weber	Weber	k1gMnSc1	Weber
a	a	k8xC	a
Georg	Georg	k1gMnSc1	Georg
Simmel	Simmel	k1gMnSc1	Simmel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
sociologie	sociologie	k1gFnSc1	sociologie
institucionalizovala	institucionalizovat	k5eAaImAgFnS	institucionalizovat
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc4	první
nezávislou	závislý	k2eNgFnSc4d1	nezávislá
katedru	katedra	k1gFnSc4	katedra
sociologie	sociologie	k1gFnSc2	sociologie
založil	založit	k5eAaPmAgInS	založit
Albion	Albion	k1gInSc1	Albion
Woodbury	Woodbura	k1gFnSc2	Woodbura
Small	Smalla	k1gFnPc2	Smalla
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1892	[number]	k4	1892
na	na	k7c6	na
Chicagské	chicagský	k2eAgFnSc6d1	Chicagská
univerzitě	univerzita	k1gFnSc6	univerzita
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
též	též	k9	též
Chicagská	chicagský	k2eAgFnSc1d1	Chicagská
škola	škola	k1gFnSc1	škola
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sociologická	sociologický	k2eAgFnSc1d1	sociologická
teorie	teorie	k1gFnSc1	teorie
==	==	k?	==
</s>
</p>
<p>
<s>
Sociologie	sociologie	k1gFnSc1	sociologie
je	být	k5eAaImIp3nS	být
vědou	věda	k1gFnSc7	věda
multiparadigmatickou	multiparadigmatický	k2eAgFnSc7d1	multiparadigmatická
<g/>
.	.	kIx.	.
</s>
<s>
Sociologické	sociologický	k2eAgNnSc1d1	sociologické
poznání	poznání	k1gNnSc1	poznání
není	být	k5eNaImIp3nS	být
kumulativní	kumulativní	k2eAgNnSc1d1	kumulativní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Základní	základní	k2eAgNnSc1d1	základní
dělení	dělení	k1gNnSc1	dělení
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
sociologická	sociologický	k2eAgNnPc4d1	sociologické
paradigmata	paradigma	k1gNnPc4	paradigma
objektivistická	objektivistický	k2eAgNnPc4d1	objektivistické
a	a	k8xC	a
interpretativní	interpretativní	k2eAgNnPc4d1	interpretativní
<g/>
.	.	kIx.	.
</s>
<s>
Objektivisté	objektivista	k1gMnPc1	objektivista
považují	považovat	k5eAaImIp3nP	považovat
sociální	sociální	k2eAgFnSc4d1	sociální
realitu	realita	k1gFnSc4	realita
za	za	k7c4	za
objektivní	objektivní	k2eAgFnSc4d1	objektivní
skutečnost	skutečnost	k1gFnSc4	skutečnost
existující	existující	k2eAgMnSc1d1	existující
vně	vně	k7c2	vně
individuí	individuum	k1gNnPc2	individuum
<g/>
.	.	kIx.	.
</s>
<s>
Zabývá	zabývat	k5eAaImIp3nS	zabývat
se	s	k7c7	s
zkoumáním	zkoumání	k1gNnSc7	zkoumání
sociálních	sociální	k2eAgInPc2d1	sociální
faktů	fakt	k1gInPc2	fakt
<g/>
,	,	kIx,	,
sociálních	sociální	k2eAgFnPc2d1	sociální
změn	změna	k1gFnPc2	změna
<g/>
,	,	kIx,	,
sociální	sociální	k2eAgFnSc2d1	sociální
struktury	struktura	k1gFnSc2	struktura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Interpretativní	Interpretativní	k2eAgFnSc1d1	Interpretativní
sociologie	sociologie	k1gFnSc1	sociologie
považuje	považovat	k5eAaImIp3nS	považovat
sociální	sociální	k2eAgFnSc4d1	sociální
realitu	realita	k1gFnSc4	realita
za	za	k7c4	za
konstrukt	konstrukt	k1gInSc4	konstrukt
v	v	k7c6	v
myslích	mysl	k1gFnPc6	mysl
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
zajímá	zajímat	k5eAaImIp3nS	zajímat
ji	on	k3xPp3gFnSc4	on
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc4	tento
konstrukt	konstrukt	k1gInSc4	konstrukt
</s>
</p>
<p>
<s>
lidmi	člověk	k1gMnPc7	člověk
vytvářen	vytvářit	k5eAaPmNgInS	vytvářit
<g/>
,	,	kIx,	,
chápán	chápat	k5eAaImNgInS	chápat
a	a	k8xC	a
interpretován	interpretovat	k5eAaBmNgInS	interpretovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Dělení	dělení	k1gNnSc1	dělení
sociologických	sociologický	k2eAgNnPc2d1	sociologické
paradigmat	paradigma	k1gNnPc2	paradigma
dle	dle	k7c2	dle
Ritzera	Ritzero	k1gNnSc2	Ritzero
===	===	k?	===
</s>
</p>
<p>
<s>
paradigma	paradigma	k1gNnSc1	paradigma
sociálního	sociální	k2eAgInSc2d1	sociální
faktu	fakt	k1gInSc2	fakt
(	(	kIx(	(
<g/>
např.	např.	kA	např.
pozitivismus	pozitivismus	k1gInSc1	pozitivismus
<g/>
,	,	kIx,	,
marxismus	marxismus	k1gInSc1	marxismus
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
konfliktualistické	konfliktualistický	k2eAgFnPc1d1	konfliktualistický
teorie	teorie	k1gFnPc1	teorie
<g/>
,	,	kIx,	,
strukturální	strukturální	k2eAgInSc1d1	strukturální
funkcionalismus	funkcionalismus	k1gInSc1	funkcionalismus
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
paradigma	paradigma	k1gNnSc1	paradigma
sociálního	sociální	k2eAgNnSc2d1	sociální
chování	chování	k1gNnSc2	chování
(	(	kIx(	(
<g/>
např.	např.	kA	např.
behaviorismus	behaviorismus	k1gInSc1	behaviorismus
<g/>
,	,	kIx,	,
teorie	teorie	k1gFnSc1	teorie
sociální	sociální	k2eAgFnSc2d1	sociální
směny	směna	k1gFnSc2	směna
<g/>
,	,	kIx,	,
sociobiologie	sociobiologie	k1gFnSc2	sociobiologie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
paradigma	paradigma	k1gNnSc1	paradigma
sociální	sociální	k2eAgFnSc2d1	sociální
definice	definice	k1gFnSc2	definice
(	(	kIx(	(
<g/>
interpretativní	interpretativní	k2eAgFnSc1d1	interpretativní
sociologie	sociologie	k1gFnSc1	sociologie
–	–	k?	–
např.	např.	kA	např.
fenomenologická	fenomenologický	k2eAgFnSc1d1	fenomenologická
sociologie	sociologie	k1gFnSc1	sociologie
<g/>
,	,	kIx,	,
symbolický	symbolický	k2eAgInSc1d1	symbolický
interakcionismus	interakcionismus	k1gInSc1	interakcionismus
<g/>
,	,	kIx,	,
etnometodologie	etnometodologie	k1gFnSc1	etnometodologie
<g/>
,	,	kIx,	,
sociální	sociální	k2eAgInSc1d1	sociální
konstruktivismus	konstruktivismus	k1gInSc1	konstruktivismus
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Nominalismus	nominalismus	k1gInSc1	nominalismus
a	a	k8xC	a
realismus	realismus	k1gInSc1	realismus
==	==	k?	==
</s>
</p>
<p>
<s>
Dalším	další	k2eAgInSc7d1	další
přístupem	přístup	k1gInSc7	přístup
k	k	k7c3	k
vnímání	vnímání	k1gNnSc3	vnímání
paradigmat	paradigma	k1gNnPc2	paradigma
je	být	k5eAaImIp3nS	být
rozdělení	rozdělení	k1gNnSc1	rozdělení
na	na	k7c4	na
nominalismus	nominalismus	k1gInSc4	nominalismus
a	a	k8xC	a
realismus	realismus	k1gInSc4	realismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sociologický	sociologický	k2eAgInSc1d1	sociologický
realismus	realismus	k1gInSc1	realismus
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
předpokladu	předpoklad	k1gInSc2	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
společnost	společnost	k1gFnSc1	společnost
existuje	existovat	k5eAaImIp3nS	existovat
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
jednotlivci	jednotlivec	k1gMnSc6	jednotlivec
<g/>
.	.	kIx.	.
</s>
<s>
Sociální	sociální	k2eAgFnSc1d1	sociální
realita	realita	k1gFnSc1	realita
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
chápána	chápat	k5eAaImNgFnS	chápat
jako	jako	k8xC	jako
objektivizována	objektivizovat	k5eAaBmNgFnS	objektivizovat
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
je	být	k5eAaImIp3nS	být
také	také	k9	také
více	hodně	k6eAd2	hodně
jak	jak	k8xC	jak
souhrn	souhrn	k1gInSc4	souhrn
jednotlivých	jednotlivý	k2eAgNnPc2d1	jednotlivé
individuí	individuum	k1gNnPc2	individuum
<g/>
.	.	kIx.	.
</s>
<s>
Sociální	sociální	k2eAgFnPc1d1	sociální
normy	norma	k1gFnPc1	norma
jsou	být	k5eAaImIp3nP	být
nadindividuální	nadindividuální	k2eAgFnPc1d1	nadindividuální
<g/>
.	.	kIx.	.
</s>
<s>
Jedinec	jedinec	k1gMnSc1	jedinec
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
zdárné	zdárný	k2eAgFnSc2d1	zdárná
socializace	socializace	k1gFnSc2	socializace
přijímá	přijímat	k5eAaImIp3nS	přijímat
tyto	tento	k3xDgFnPc4	tento
normy	norma	k1gFnPc4	norma
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
svět	svět	k1gInSc1	svět
má	mít	k5eAaImIp3nS	mít
pevný	pevný	k2eAgInSc4d1	pevný
řád	řád	k1gInSc4	řád
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
přístup	přístup	k1gInSc1	přístup
je	být	k5eAaImIp3nS	být
uplatňován	uplatňovat	k5eAaImNgInS	uplatňovat
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
teoriích	teorie	k1gFnPc6	teorie
zabývajících	zabývající	k2eAgInPc2d1	zabývající
se	se	k3xPyFc4	se
sociální	sociální	k2eAgFnSc7d1	sociální
statikou	statika	k1gFnSc7	statika
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
sociologického	sociologický	k2eAgInSc2d1	sociologický
realismu	realismus	k1gInSc2	realismus
řadíme	řadit	k5eAaImIp1nP	řadit
fenomenologickou	fenomenologický	k2eAgFnSc4d1	fenomenologická
<g/>
,	,	kIx,	,
konfliktologickou	konfliktologický	k2eAgFnSc4d1	konfliktologický
a	a	k8xC	a
konsensuální	konsensuální	k2eAgFnSc4d1	konsensuální
sociologii	sociologie	k1gFnSc4	sociologie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sociologický	sociologický	k2eAgInSc1d1	sociologický
nominalismus	nominalismus	k1gInSc1	nominalismus
naopak	naopak	k6eAd1	naopak
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
společnost	společnost	k1gFnSc1	společnost
existuje	existovat	k5eAaImIp3nS	existovat
mezi	mezi	k7c7	mezi
individui	individuum	k1gNnPc7	individuum
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
intersubjektivní	intersubjektivní	k2eAgNnSc1d1	intersubjektivní
<g/>
.	.	kIx.	.
</s>
<s>
Sociální	sociální	k2eAgFnPc1d1	sociální
normy	norma	k1gFnPc1	norma
neexistují	existovat	k5eNaImIp3nP	existovat
mimo	mimo	k7c4	mimo
vědomí	vědomí	k1gNnSc4	vědomí
<g/>
.	.	kIx.	.
</s>
<s>
Normy	Norma	k1gFnPc1	Norma
se	se	k3xPyFc4	se
vytváří	vytvářet	k5eAaImIp3nP	vytvářet
skrze	skrze	k?	skrze
interakci	interakce	k1gFnSc4	interakce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
tomuto	tento	k3xDgNnSc3	tento
paradigmatu	paradigma	k1gNnSc3	paradigma
patří	patřit	k5eAaImIp3nS	patřit
škola	škola	k1gFnSc1	škola
interakcionistů	interakcionista	k1gMnPc2	interakcionista
<g/>
.	.	kIx.	.
</s>
<s>
Těžiště	těžiště	k1gNnSc1	těžiště
tohoto	tento	k3xDgNnSc2	tento
paradigmatu	paradigma	k1gNnSc2	paradigma
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
sociální	sociální	k2eAgFnSc2d1	sociální
dynamiky	dynamika	k1gFnSc2	dynamika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Významné	významný	k2eAgInPc4d1	významný
sociologické	sociologický	k2eAgInPc4d1	sociologický
směry	směr	k1gInPc4	směr
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc1	jejich
představitelé	představitel	k1gMnPc1	představitel
===	===	k?	===
</s>
</p>
<p>
<s>
pozitivismus	pozitivismus	k1gInSc1	pozitivismus
(	(	kIx(	(
<g/>
Auguste	August	k1gMnSc5	August
Comte	Comt	k1gMnSc5	Comt
<g/>
,	,	kIx,	,
Émile	Émile	k1gNnSc1	Émile
Durkheim	Durkheima	k1gFnPc2	Durkheima
<g/>
)	)	kIx)	)
a	a	k8xC	a
novopozitivismus	novopozitivismus	k1gInSc1	novopozitivismus
(	(	kIx(	(
<g/>
Paul	Paul	k1gMnSc1	Paul
Felix	Felix	k1gMnSc1	Felix
Lazarsfeld	Lazarsfeld	k1gMnSc1	Lazarsfeld
<g/>
,	,	kIx,	,
William	William	k1gInSc1	William
Ogburn	Ogburn	k1gInSc1	Ogburn
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
sociologismus	sociologismus	k1gInSc1	sociologismus
(	(	kIx(	(
<g/>
Émile	Émile	k1gFnSc1	Émile
Durkheim	Durkheim	k1gMnSc1	Durkheim
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
evolucionismus	evolucionismus	k1gInSc1	evolucionismus
(	(	kIx(	(
<g/>
Herbert	Herbert	k1gMnSc1	Herbert
Spencer	Spencer	k1gMnSc1	Spencer
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
formalismus	formalismus	k1gInSc1	formalismus
(	(	kIx(	(
<g/>
Georg	Georg	k1gMnSc1	Georg
Simmel	Simmel	k1gMnSc1	Simmel
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
marxismus	marxismus	k1gInSc1	marxismus
(	(	kIx(	(
<g/>
Karel	Karel	k1gMnSc1	Karel
Marx	Marx	k1gMnSc1	Marx
<g/>
)	)	kIx)	)
a	a	k8xC	a
neomarxismus	neomarxismus	k1gInSc1	neomarxismus
(	(	kIx(	(
<g/>
Max	Max	k1gMnSc1	Max
Horkheimer	Horkheimer	k1gMnSc1	Horkheimer
<g/>
,	,	kIx,	,
Theodor	Theodor	k1gMnSc1	Theodor
W.	W.	kA	W.
Adorno	Adorno	k1gNnSc1	Adorno
<g/>
,	,	kIx,	,
Herbert	Herbert	k1gInSc1	Herbert
Marcuse	Marcuse	k1gFnSc1	Marcuse
<g/>
,	,	kIx,	,
Jürgen	Jürgen	k1gInSc1	Jürgen
Habermas	Habermas	k1gInSc1	Habermas
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
psychologismus	psychologismus	k1gInSc1	psychologismus
(	(	kIx(	(
<g/>
Gabriel	Gabriel	k1gMnSc1	Gabriel
Tarde	Tard	k1gMnSc5	Tard
<g/>
,	,	kIx,	,
Gustave	Gustav	k1gMnSc5	Gustav
Le	Le	k1gMnSc5	Le
Bon	bon	k1gInSc4	bon
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
behaviorismus	behaviorismus	k1gInSc1	behaviorismus
(	(	kIx(	(
<g/>
Burrhus	Burrhus	k1gMnSc1	Burrhus
Frederic	Frederic	k1gMnSc1	Frederic
Skinner	Skinner	k1gMnSc1	Skinner
<g/>
,	,	kIx,	,
George	George	k1gFnSc1	George
Caspar	Caspar	k1gMnSc1	Caspar
Homans	Homans	k1gInSc1	Homans
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
italská	italský	k2eAgFnSc1d1	italská
politická	politický	k2eAgFnSc1d1	politická
sociologie	sociologie	k1gFnSc1	sociologie
(	(	kIx(	(
<g/>
Vilfredo	Vilfredo	k1gNnSc1	Vilfredo
Pareto	Paret	k2eAgNnSc1d1	Paret
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
Michels	Michelsa	k1gFnPc2	Michelsa
<g/>
,	,	kIx,	,
Gaetano	Gaetana	k1gFnSc5	Gaetana
Mosca	Moscus	k1gMnSc4	Moscus
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
fenomenologická	fenomenologický	k2eAgFnSc1d1	fenomenologická
sociologie	sociologie	k1gFnSc1	sociologie
(	(	kIx(	(
<g/>
Alfred	Alfred	k1gMnSc1	Alfred
Schütz	Schütz	k1gMnSc1	Schütz
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
symbolický	symbolický	k2eAgInSc4d1	symbolický
interakcionismus	interakcionismus	k1gInSc4	interakcionismus
(	(	kIx(	(
<g/>
George	Georg	k1gMnSc2	Georg
Herbert	Herbert	k1gMnSc1	Herbert
Mead	Mead	k1gMnSc1	Mead
<g/>
,	,	kIx,	,
Charles	Charles	k1gMnSc1	Charles
H.	H.	kA	H.
Cooley	Coolea	k1gFnPc1	Coolea
<g/>
,	,	kIx,	,
Herbert	Herbert	k1gMnSc1	Herbert
Blumer	Blumer	k1gMnSc1	Blumer
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
strukturalismus	strukturalismus	k1gInSc4	strukturalismus
(	(	kIx(	(
<g/>
Claude	Claud	k1gMnSc5	Claud
Lévi-Strauss	Lévi-Strauss	k1gInSc4	Lévi-Strauss
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
strukturální	strukturální	k2eAgInSc1d1	strukturální
funkcionalismus	funkcionalismus	k1gInSc1	funkcionalismus
(	(	kIx(	(
<g/>
Talcott	Talcott	k2eAgInSc1d1	Talcott
Parsons	Parsons	k1gInSc1	Parsons
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
K.	K.	kA	K.
Merton	Merton	k1gInSc1	Merton
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
sociální	sociální	k2eAgInSc1d1	sociální
konstruktivismus	konstruktivismus	k1gInSc1	konstruktivismus
(	(	kIx(	(
<g/>
Peter	Peter	k1gMnSc1	Peter
Berger	Berger	k1gMnSc1	Berger
<g/>
,	,	kIx,	,
Thomas	Thomas	k1gMnSc1	Thomas
Luckmann	Luckmann	k1gMnSc1	Luckmann
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
teorie	teorie	k1gFnSc1	teorie
sociální	sociální	k2eAgFnSc2d1	sociální
směny	směna	k1gFnSc2	směna
(	(	kIx(	(
<g/>
George	George	k1gFnSc1	George
Caspar	Caspar	k1gMnSc1	Caspar
Homans	Homans	k1gInSc1	Homans
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
Blau	Blaus	k1gInSc2	Blaus
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
sociobiologie	sociobiologie	k1gFnSc1	sociobiologie
(	(	kIx(	(
<g/>
Edward	Edward	k1gMnSc1	Edward
Osborne	Osborn	k1gInSc5	Osborn
Wilson	Wilson	k1gMnSc1	Wilson
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
etnometodologie	etnometodologie	k1gFnSc1	etnometodologie
(	(	kIx(	(
<g/>
Harold	Harold	k1gMnSc1	Harold
Garfinkel	Garfinkel	k1gMnSc1	Garfinkel
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
poststrukturalismus	poststrukturalismus	k1gInSc1	poststrukturalismus
(	(	kIx(	(
<g/>
Jacques	Jacques	k1gMnSc1	Jacques
Derrida	Derrida	k1gFnSc1	Derrida
<g/>
,	,	kIx,	,
Jean-François	Jean-François	k1gFnSc1	Jean-François
Lyotard	Lyotard	k1gMnSc1	Lyotard
<g/>
,	,	kIx,	,
Michel	Michel	k1gMnSc1	Michel
Foucault	Foucault	k1gMnSc1	Foucault
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
organicismus	organicismus	k1gInSc1	organicismus
(	(	kIx(	(
<g/>
Herbert	Herbert	k1gMnSc1	Herbert
Spencer	Spencer	k1gMnSc1	Spencer
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Metody	metoda	k1gFnPc1	metoda
sociologie	sociologie	k1gFnPc1	sociologie
==	==	k?	==
</s>
</p>
<p>
<s>
Sociologický	sociologický	k2eAgInSc1d1	sociologický
výzkum	výzkum	k1gInSc1	výzkum
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
popsat	popsat	k5eAaPmF	popsat
<g/>
,	,	kIx,	,
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
či	či	k8xC	či
předpovědět	předpovědět	k5eAaPmF	předpovědět
sociální	sociální	k2eAgInSc4d1	sociální
život	život	k1gInSc4	život
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
,	,	kIx,	,
skupin	skupina	k1gFnPc2	skupina
a	a	k8xC	a
společností	společnost	k1gFnPc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
je	být	k5eAaImIp3nS	být
užívána	užíván	k2eAgFnSc1d1	užívána
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
metod	metoda	k1gFnPc2	metoda
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
můžeme	moct	k5eAaImIp1nP	moct
zjednodušeně	zjednodušeně	k6eAd1	zjednodušeně
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
kvantitativní	kvantitativní	k2eAgNnSc4d1	kvantitativní
a	a	k8xC	a
kvalitativní	kvalitativní	k2eAgNnSc4d1	kvalitativní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kvantitativní	kvantitativní	k2eAgInSc1d1	kvantitativní
výzkum	výzkum	k1gInSc1	výzkum
se	se	k3xPyFc4	se
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
na	na	k7c6	na
populaci	populace	k1gFnSc6	populace
<g/>
.	.	kIx.	.
</s>
<s>
Snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
o	o	k7c4	o
reprezentativnost	reprezentativnost	k1gFnSc4	reprezentativnost
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
vypovídat	vypovídat	k5eAaPmF	vypovídat
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
nějaký	nějaký	k3yIgInSc4	nějaký
znak	znak	k1gInSc4	znak
zastoupen	zastoupit	k5eAaPmNgMnS	zastoupit
v	v	k7c4	v
populaci	populace	k1gFnSc4	populace
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jaký	jaký	k3yRgInSc4	jaký
postoj	postoj	k1gInSc4	postoj
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
u	u	k7c2	u
populace	populace	k1gFnSc2	populace
najít	najít	k5eAaPmF	najít
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
<g/>
,	,	kIx,	,
ne	ne	k9	ne
však	však	k9	však
nutně	nutně	k6eAd1	nutně
<g/>
,	,	kIx,	,
užívá	užívat	k5eAaImIp3nS	užívat
metody	metoda	k1gFnPc4	metoda
dotazníkového	dotazníkový	k2eAgNnSc2d1	dotazníkové
šetření	šetření	k1gNnSc2	šetření
či	či	k8xC	či
rozhovorů	rozhovor	k1gInPc2	rozhovor
a	a	k8xC	a
kvantitativní	kvantitativní	k2eAgFnSc2d1	kvantitativní
analýzy	analýza	k1gFnSc2	analýza
textů	text	k1gInPc2	text
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kvalitativní	kvalitativní	k2eAgInSc1d1	kvalitativní
výzkum	výzkum	k1gInSc1	výzkum
se	se	k3xPyFc4	se
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
na	na	k7c4	na
objekt	objekt	k1gInSc4	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Nepopisuje	popisovat	k5eNaImIp3nS	popisovat
zákonitosti	zákonitost	k1gFnPc4	zákonitost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
nacházet	nacházet	k5eAaImF	nacházet
vztahy	vztah	k1gInPc4	vztah
mezi	mezi	k7c7	mezi
kategoriemi	kategorie	k1gFnPc7	kategorie
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ne	ne	k9	ne
nutně	nutně	k6eAd1	nutně
<g/>
,	,	kIx,	,
užívá	užívat	k5eAaImIp3nS	užívat
metod	metoda	k1gFnPc2	metoda
focus	focus	k1gMnSc1	focus
groups	groupsa	k1gFnPc2	groupsa
<g/>
,	,	kIx,	,
hloubkových	hloubkový	k2eAgInPc2d1	hloubkový
rozhovorů	rozhovor	k1gInPc2	rozhovor
a	a	k8xC	a
analýzy	analýza	k1gFnSc2	analýza
textů	text	k1gInPc2	text
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vybrané	vybraný	k2eAgFnPc1d1	vybraná
konkrétní	konkrétní	k2eAgFnPc1d1	konkrétní
sociologické	sociologický	k2eAgFnPc1d1	sociologická
disciplíny	disciplína	k1gFnPc1	disciplína
==	==	k?	==
</s>
</p>
<p>
<s>
Zkoumání	zkoumání	k1gNnSc1	zkoumání
biosociálních	biosociální	k2eAgFnPc2d1	biosociální
a	a	k8xC	a
geografických	geografický	k2eAgFnPc2d1	geografická
podmínek	podmínka	k1gFnPc2	podmínka
společenského	společenský	k2eAgInSc2d1	společenský
života	život	k1gInSc2	život
</s>
</p>
<p>
<s>
Sociologie	sociologie	k1gFnSc1	sociologie
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
</s>
</p>
<p>
<s>
Sociální	sociální	k2eAgFnSc1d1	sociální
ekologie	ekologie	k1gFnSc1	ekologie
(	(	kIx(	(
<g/>
sociologie	sociologie	k1gFnSc1	sociologie
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
sociologie	sociologie	k1gFnSc1	sociologie
venkova	venkov	k1gInSc2	venkov
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sociální	sociální	k2eAgFnSc1d1	sociální
geografie	geografie	k1gFnSc1	geografie
</s>
</p>
<p>
<s>
Sociologie	sociologie	k1gFnSc1	sociologie
etnických	etnický	k2eAgFnPc2d1	etnická
a	a	k8xC	a
rasových	rasový	k2eAgFnPc2d1	rasová
skupin	skupina	k1gFnPc2	skupina
</s>
</p>
<p>
<s>
Sociologie	sociologie	k1gFnSc1	sociologie
sexuálních	sexuální	k2eAgInPc2d1	sexuální
vztahů	vztah	k1gInPc2	vztah
</s>
</p>
<p>
<s>
Sociologie	sociologie	k1gFnSc1	sociologie
věkových	věkový	k2eAgFnPc2d1	věková
skupin	skupina	k1gFnPc2	skupina
(	(	kIx(	(
<g/>
sociologie	sociologie	k1gFnSc1	sociologie
mládeže	mládež	k1gFnSc2	mládež
<g/>
,	,	kIx,	,
sociální	sociální	k2eAgFnSc2d1	sociální
gerontologie	gerontologie	k1gFnSc2	gerontologie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zkoumání	zkoumání	k1gNnSc1	zkoumání
politické	politický	k2eAgFnSc2d1	politická
a	a	k8xC	a
sociálněekonomické	sociálněekonomický	k2eAgFnSc2d1	sociálněekonomický
struktury	struktura	k1gFnSc2	struktura
společnosti	společnost	k1gFnSc2	společnost
</s>
</p>
<p>
<s>
Sociologie	sociologie	k1gFnSc1	sociologie
společenských	společenský	k2eAgFnPc2d1	společenská
tříd	třída	k1gFnPc2	třída
(	(	kIx(	(
<g/>
sociologie	sociologie	k1gFnSc1	sociologie
politiky	politika	k1gFnSc2	politika
vč.	vč.	k?	vč.
teorie	teorie	k1gFnSc2	teorie
elit	elita	k1gFnPc2	elita
<g/>
,	,	kIx,	,
sociologie	sociologie	k1gFnSc1	sociologie
dělnické	dělnický	k2eAgFnSc2d1	Dělnická
třídy	třída	k1gFnSc2	třída
<g/>
,	,	kIx,	,
sociologie	sociologie	k1gFnSc1	sociologie
rolnictva	rolnictvo	k1gNnSc2	rolnictvo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sociologie	sociologie	k1gFnSc1	sociologie
společenských	společenský	k2eAgFnPc2d1	společenská
vrstev	vrstva	k1gFnPc2	vrstva
(	(	kIx(	(
<g/>
sociologie	sociologie	k1gFnSc1	sociologie
inteligence	inteligence	k1gFnSc1	inteligence
<g/>
,	,	kIx,	,
sociologie	sociologie	k1gFnSc1	sociologie
tzv.	tzv.	kA	tzv.
středních	střední	k2eAgFnPc2d1	střední
"	"	kIx"	"
<g/>
tříd	třída	k1gFnPc2	třída
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sociologie	sociologie	k1gFnSc1	sociologie
povolání	povolání	k1gNnSc2	povolání
</s>
</p>
<p>
<s>
Sociologie	sociologie	k1gFnSc1	sociologie
společenských	společenský	k2eAgFnPc2d1	společenská
skupin	skupina	k1gFnPc2	skupina
(	(	kIx(	(
<g/>
sociologie	sociologie	k1gFnSc1	sociologie
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
sociologie	sociologie	k1gFnSc1	sociologie
malých	malý	k2eAgFnPc2d1	malá
skupin	skupina	k1gFnPc2	skupina
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sociologie	sociologie	k1gFnSc1	sociologie
spotřeby	spotřeba	k1gFnSc2	spotřeba
a	a	k8xC	a
životní	životní	k2eAgFnSc2d1	životní
úrovně	úroveň	k1gFnSc2	úroveň
(	(	kIx(	(
<g/>
sociologie	sociologie	k1gFnSc1	sociologie
volného	volný	k2eAgInSc2d1	volný
času	čas	k1gInSc2	čas
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zkoumání	zkoumání	k1gNnSc1	zkoumání
společenských	společenský	k2eAgFnPc2d1	společenská
<g/>
,	,	kIx,	,
pracovních	pracovní	k2eAgFnPc2d1	pracovní
a	a	k8xC	a
technických	technický	k2eAgFnPc2d1	technická
podmínek	podmínka	k1gFnPc2	podmínka
výroby	výroba	k1gFnSc2	výroba
</s>
</p>
<p>
<s>
Sociologie	sociologie	k1gFnSc1	sociologie
práce	práce	k1gFnSc2	práce
</s>
</p>
<p>
<s>
Sociologie	sociologie	k1gFnSc1	sociologie
průmyslu	průmysl	k1gInSc2	průmysl
</s>
</p>
<p>
<s>
Sociologie	sociologie	k1gFnSc1	sociologie
zemědělství	zemědělství	k1gNnSc2	zemědělství
</s>
</p>
<p>
<s>
Sociologie	sociologie	k1gFnSc1	sociologie
techniky	technika	k1gFnSc2	technika
</s>
</p>
<p>
<s>
Sociologie	sociologie	k1gFnSc1	sociologie
řízení	řízení	k1gNnSc2	řízení
</s>
</p>
<p>
<s>
Zkoumání	zkoumání	k1gNnSc4	zkoumání
forem	forma	k1gFnPc2	forma
společenského	společenský	k2eAgNnSc2d1	společenské
vědomí	vědomí	k1gNnSc2	vědomí
</s>
</p>
<p>
<s>
Sociologie	sociologie	k1gFnSc1	sociologie
kultury	kultura	k1gFnSc2	kultura
</s>
</p>
<p>
<s>
Sociologie	sociologie	k1gFnSc1	sociologie
práva	právo	k1gNnSc2	právo
(	(	kIx(	(
<g/>
sociologie	sociologie	k1gFnSc1	sociologie
deviantního	deviantní	k2eAgInSc2d1	deviantní
chováni	chovat	k5eAaImNgMnP	chovat
a	a	k8xC	a
kriminality	kriminalita	k1gFnSc2	kriminalita
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sociologie	sociologie	k1gFnSc1	sociologie
vědění	vědění	k1gNnSc2	vědění
a	a	k8xC	a
ideologie	ideologie	k1gFnSc2	ideologie
(	(	kIx(	(
<g/>
sociologie	sociologie	k1gFnSc1	sociologie
sociologie	sociologie	k1gFnSc1	sociologie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sociologie	sociologie	k1gFnSc1	sociologie
veřejného	veřejný	k2eAgNnSc2d1	veřejné
mínění	mínění	k1gNnSc2	mínění
</s>
</p>
<p>
<s>
Sociologie	sociologie	k1gFnSc1	sociologie
morálky	morálka	k1gFnSc2	morálka
</s>
</p>
<p>
<s>
Sociologie	sociologie	k1gFnSc1	sociologie
výchovy	výchova	k1gFnSc2	výchova
a	a	k8xC	a
vzdělání	vzdělání	k1gNnSc2	vzdělání
</s>
</p>
<p>
<s>
Sociologie	sociologie	k1gFnSc1	sociologie
umění	umění	k1gNnSc2	umění
</s>
</p>
<p>
<s>
Sociologie	sociologie	k1gFnSc1	sociologie
vědy	věda	k1gFnSc2	věda
</s>
</p>
<p>
<s>
Sociologie	sociologie	k1gFnSc1	sociologie
náboženství	náboženství	k1gNnSc2	náboženství
</s>
</p>
<p>
<s>
Sociologie	sociologie	k1gFnSc1	sociologie
jazyka	jazyk	k1gInSc2	jazyk
</s>
</p>
<p>
<s>
Zkoumání	zkoumání	k1gNnSc1	zkoumání
společenských	společenský	k2eAgFnPc2d1	společenská
institucí	instituce	k1gFnPc2	instituce
</s>
</p>
<p>
<s>
Sociologie	sociologie	k1gFnSc1	sociologie
státu	stát	k1gInSc2	stát
a	a	k8xC	a
práva	právo	k1gNnSc2	právo
</s>
</p>
<p>
<s>
Sociologie	sociologie	k1gFnSc1	sociologie
politiky	politika	k1gFnSc2	politika
(	(	kIx(	(
<g/>
sociologie	sociologie	k1gFnSc1	sociologie
politických	politický	k2eAgFnPc2d1	politická
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
sociologie	sociologie	k1gFnSc1	sociologie
propagandy	propaganda	k1gFnSc2	propaganda
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sociologie	sociologie	k1gFnSc1	sociologie
armády	armáda	k1gFnSc2	armáda
</s>
</p>
<p>
<s>
Sociologie	sociologie	k1gFnSc1	sociologie
zdravotnictví	zdravotnictví	k1gNnSc2	zdravotnictví
</s>
</p>
<p>
<s>
==	==	k?	==
Přehled	přehled	k1gInSc4	přehled
sociologických	sociologický	k2eAgInPc2d1	sociologický
směrů	směr	k1gInPc2	směr
a	a	k8xC	a
základních	základní	k2eAgMnPc2d1	základní
představitelů	představitel	k1gMnPc2	představitel
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Vztahy	vztah	k1gInPc1	vztah
k	k	k7c3	k
jiným	jiný	k2eAgFnPc3d1	jiná
vědám	věda	k1gFnPc3	věda
==	==	k?	==
</s>
</p>
<p>
<s>
Sociologie	sociologie	k1gFnSc1	sociologie
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
společenské	společenský	k2eAgFnPc4d1	společenská
vědy	věda	k1gFnPc4	věda
<g/>
.	.	kIx.	.
</s>
<s>
Souvisí	souviset	k5eAaImIp3nS	souviset
a	a	k8xC	a
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
především	především	k9	především
s	s	k7c7	s
těmito	tento	k3xDgFnPc7	tento
jinými	jiný	k2eAgFnPc7d1	jiná
vědami	věda	k1gFnPc7	věda
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
GIDDENS	GIDDENS	kA	GIDDENS
<g/>
,	,	kIx,	,
Anthony	Anthon	k1gInPc7	Anthon
<g/>
.	.	kIx.	.
</s>
<s>
Sociologie	sociologie	k1gFnSc1	sociologie
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Argo	Argo	k1gNnSc1	Argo
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
595	[number]	k4	595
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7203	[number]	k4	7203
<g/>
-	-	kIx~	-
<g/>
124	[number]	k4	124
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
GILLERNOVÁ	GILLERNOVÁ	kA	GILLERNOVÁ
<g/>
,	,	kIx,	,
Ilona	Ilona	k1gFnSc1	Ilona
<g/>
.	.	kIx.	.
</s>
<s>
Základy	základ	k1gInPc1	základ
Psychologie	psychologie	k1gFnSc2	psychologie
<g/>
,	,	kIx,	,
sociologie	sociologie	k1gFnSc1	sociologie
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Fortuna	Fortuna	k1gFnSc1	Fortuna
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
160	[number]	k4	160
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7168	[number]	k4	7168
<g/>
-	-	kIx~	-
<g/>
749	[number]	k4	749
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
sociologie	sociologie	k1gFnSc2	sociologie
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
sociologů	sociolog	k1gMnPc2	sociolog
</s>
</p>
<p>
<s>
Sociální	sociální	k2eAgFnSc1d1	sociální
inteligence	inteligence	k1gFnSc1	inteligence
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
sociologie	sociologie	k1gFnSc2	sociologie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Téma	téma	k1gNnSc1	téma
Sociologie	sociologie	k1gFnSc2	sociologie
ve	v	k7c4	v
WikicitátechSociologický	WikicitátechSociologický	k2eAgInSc4d1	WikicitátechSociologický
ústav	ústav	k1gInSc4	ústav
AV	AV	kA	AV
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
v.v.i.	v.v.i.	k?	v.v.i.
</s>
</p>
<p>
<s>
Katedra	katedra	k1gFnSc1	katedra
sociologie	sociologie	k1gFnSc1	sociologie
<g/>
,	,	kIx,	,
Fakulta	fakulta	k1gFnSc1	fakulta
sociálních	sociální	k2eAgFnPc2d1	sociální
studií	studie	k1gFnPc2	studie
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
</s>
</p>
<p>
<s>
Katedra	katedra	k1gFnSc1	katedra
sociologie	sociologie	k1gFnSc1	sociologie
<g/>
,	,	kIx,	,
Fakulta	fakulta	k1gFnSc1	fakulta
filozofická	filozofický	k2eAgFnSc1d1	filozofická
Západočeské	západočeský	k2eAgFnPc4d1	Západočeská
univerzity	univerzita	k1gFnPc4	univerzita
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
</s>
</p>
<p>
<s>
Portál	portál	k1gInSc1	portál
sociologie	sociologie	k1gFnSc2	sociologie
</s>
</p>
