<s>
Podle	podle	k7c2	podle
norem	norma	k1gFnPc2	norma
UIAA	UIAA	kA	UIAA
převzatých	převzatý	k2eAgFnPc2d1	převzatá
i	i	k8xC	i
do	do	k7c2	do
našeho	náš	k3xOp1gInSc2	náš
normového	normový	k2eAgInSc2d1	normový
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
lano	lano	k1gNnSc1	lano
testuje	testovat	k5eAaImIp3nS	testovat
pádem	pád	k1gInSc7	pád
80	[number]	k4	80
Kg	kg	kA	kg
závaží	závaží	k1gNnSc1	závaží
do	do	k7c2	do
jednoho	jeden	k4xCgInSc2	jeden
pramene	pramen	k1gInSc2	pramen
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
vydržet	vydržet	k5eAaPmF	vydržet
minimálně	minimálně	k6eAd1	minimálně
5	[number]	k4	5
normovaných	normovaný	k2eAgInPc2d1	normovaný
pádů	pád	k1gInPc2	pád
<g/>
,	,	kIx,	,
síla	síla	k1gFnSc1	síla
při	při	k7c6	při
zachycení	zachycení	k1gNnSc6	zachycení
pádu	pád	k1gInSc2	pád
je	být	k5eAaImIp3nS	být
maximálně	maximálně	k6eAd1	maximálně
12	[number]	k4	12
kN	kN	k?	kN
<g/>
.	.	kIx.	.
</s>
