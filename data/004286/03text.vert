<s>
Romance	romance	k1gFnSc1	romance
pro	pro	k7c4	pro
křídlovku	křídlovka	k1gFnSc4	křídlovka
je	být	k5eAaImIp3nS	být
český	český	k2eAgInSc1d1	český
hořký	hořký	k2eAgInSc1d1	hořký
poeticko-romantický	poetickoomantický	k2eAgInSc1d1	poeticko-romantický
film	film	k1gInSc1	film
režiséra	režisér	k1gMnSc2	režisér
Otakara	Otakar	k1gMnSc2	Otakar
Vávry	Vávra	k1gMnSc2	Vávra
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
natočený	natočený	k2eAgInSc4d1	natočený
na	na	k7c4	na
námět	námět	k1gInSc4	námět
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
básně	báseň	k1gFnSc2	báseň
Františka	František	k1gMnSc2	František
Hrubína	Hrubín	k1gMnSc2	Hrubín
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgInS	být
u	u	k7c2	u
tohoto	tento	k3xDgInSc2	tento
snímku	snímek	k1gInSc2	snímek
i	i	k8xC	i
spoluautorem	spoluautor	k1gMnSc7	spoluautor
scénáře	scénář	k1gInSc2	scénář
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hlavních	hlavní	k2eAgFnPc6d1	hlavní
rolích	role	k1gFnPc6	role
zde	zde	k6eAd1	zde
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
tehdy	tehdy	k6eAd1	tehdy
mladičký	mladičký	k2eAgMnSc1d1	mladičký
herec	herec	k1gMnSc1	herec
Jaromír	Jaromír	k1gMnSc1	Jaromír
Hanzlík	Hanzlík	k1gMnSc1	Hanzlík
a	a	k8xC	a
slovenská	slovenský	k2eAgFnSc1d1	slovenská
herečka	herečka	k1gFnSc1	herečka
Zuzana	Zuzana	k1gFnSc1	Zuzana
Cigánová	Cigánová	k1gFnSc1	Cigánová
<g/>
,	,	kIx,	,
nezapomenutelný	zapomenutelný	k2eNgInSc1d1	nezapomenutelný
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
zde	zde	k6eAd1	zde
předvedla	předvést	k5eAaPmAgFnS	předvést
i	i	k9	i
Miriam	Miriam	k1gFnSc1	Miriam
Kantorková	Kantorková	k1gFnSc1	Kantorková
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
širším	široký	k2eAgInSc6d2	širší
kontextu	kontext	k1gInSc6	kontext
československé	československý	k2eAgFnSc2d1	Československá
kinematografie	kinematografie	k1gFnSc2	kinematografie
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
poměrně	poměrně	k6eAd1	poměrně
vzácnou	vzácný	k2eAgFnSc4d1	vzácná
ukázku	ukázka	k1gFnSc4	ukázka
unikátního	unikátní	k2eAgNnSc2d1	unikátní
filmového	filmový	k2eAgNnSc2d1	filmové
díla	dílo	k1gNnSc2	dílo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
pokusem	pokus	k1gInSc7	pokus
o	o	k7c4	o
vizuální	vizuální	k2eAgInSc4d1	vizuální
přepis	přepis	k1gInSc4	přepis
většího	veliký	k2eAgNnSc2d2	veliký
básnického	básnický	k2eAgNnSc2d1	básnické
díla	dílo	k1gNnSc2	dílo
(	(	kIx(	(
<g/>
filmová	filmový	k2eAgFnSc1d1	filmová
báseň	báseň	k1gFnSc1	báseň
<g/>
)	)	kIx)	)
spadajícího	spadající	k2eAgInSc2d1	spadající
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
klasické	klasický	k2eAgFnSc2d1	klasická
české	český	k2eAgFnSc2d1	Česká
literatury	literatura	k1gFnSc2	literatura
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
celý	celý	k2eAgInSc1d1	celý
děj	děj	k1gInSc1	děj
filmu	film	k1gInSc2	film
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Lešan	Lešany	k1gInPc2	Lešany
na	na	k7c6	na
dolní	dolní	k2eAgFnSc6d1	dolní
Sázavě	Sázava	k1gFnSc6	Sázava
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
František	František	k1gMnSc1	František
Hrubín	Hrubín	k1gMnSc1	Hrubín
prožil	prožít	k5eAaPmAgMnS	prožít
své	svůj	k3xOyFgNnSc4	svůj
mládí	mládí	k1gNnSc4	mládí
<g/>
.	.	kIx.	.
</s>
<s>
Smutný	smutný	k2eAgInSc1d1	smutný
poetický	poetický	k2eAgInSc1d1	poetický
příběh	příběh	k1gInSc1	příběh
o	o	k7c6	o
setkání	setkání	k1gNnSc6	setkání
mladého	mladý	k2eAgMnSc2d1	mladý
člověka	člověk	k1gMnSc2	člověk
studenta	student	k1gMnSc2	student
Vojty	Vojta	k1gMnSc2	Vojta
(	(	kIx(	(
<g/>
Jaromír	Jaromír	k1gMnSc1	Jaromír
Hanzlík	Hanzlík	k1gMnSc1	Hanzlík
<g/>
)	)	kIx)	)
jak	jak	k6eAd1	jak
s	s	k7c7	s
první	první	k4xOgFnSc7	první
velkou	velký	k2eAgFnSc7d1	velká
láskou	láska	k1gFnSc7	láska
Terinou	Terina	k1gFnSc7	Terina
(	(	kIx(	(
<g/>
Zuzana	Zuzana	k1gFnSc1	Zuzana
Cigánová	Cigánová	k1gFnSc1	Cigánová
<g/>
)	)	kIx)	)
i	i	k9	i
se	s	k7c7	s
smrtí	smrt	k1gFnSc7	smrt
(	(	kIx(	(
<g/>
Vojtův	Vojtův	k2eAgMnSc1d1	Vojtův
dědeček	dědeček	k1gMnSc1	dědeček
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jaromír	Jaromír	k1gMnSc1	Jaromír
Hanzlík	Hanzlík	k1gMnSc1	Hanzlík
(	(	kIx(	(
<g/>
student	student	k1gMnSc1	student
Vojta	Vojta	k1gMnSc1	Vojta
<g/>
)	)	kIx)	)
Zuzana	Zuzana	k1gFnSc1	Zuzana
Cigánová	Cigánová	k1gFnSc1	Cigánová
(	(	kIx(	(
<g/>
Terina	Terina	k1gFnSc1	Terina
<g/>
)	)	kIx)	)
Štefan	Štefan	k1gMnSc1	Štefan
Kvietik	Kvietik	k1gMnSc1	Kvietik
(	(	kIx(	(
<g/>
Viktor	Viktor	k1gMnSc1	Viktor
<g/>
)	)	kIx)	)
Miriam	Miriam	k1gFnSc1	Miriam
Kantorková	Kantorková	k1gFnSc1	Kantorková
(	(	kIx(	(
<g/>
Tonka	Tonka	k1gFnSc1	Tonka
<g/>
)	)	kIx)	)
Július	Július	k1gMnSc1	Július
Vašek	Vašek	k1gMnSc1	Vašek
(	(	kIx(	(
<g/>
Vojta	Vojta	k1gMnSc1	Vojta
-	-	kIx~	-
padesátiletý	padesátiletý	k2eAgMnSc1d1	padesátiletý
<g/>
)	)	kIx)	)
Janusz	Janusz	k1gInSc1	Janusz
Strachocki	Strachocki	k1gNnSc1	Strachocki
(	(	kIx(	(
<g/>
Vojtův	Vojtův	k2eAgMnSc1d1	Vojtův
dědeček	dědeček	k1gMnSc1	dědeček
<g/>
)	)	kIx)	)
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Rozsíval	Rozsíval	k1gMnSc1	Rozsíval
(	(	kIx(	(
<g/>
Vojtův	Vojtův	k2eAgMnSc1d1	Vojtův
otec	otec	k1gMnSc1	otec
<g/>
)	)	kIx)	)
Jiří	Jiří	k1gMnSc1	Jiří
Štancl	Štancl	k1gMnSc1	Štancl
(	(	kIx(	(
<g/>
Terinin	Terinin	k2eAgMnSc1d1	Terinin
otec	otec	k1gMnSc1	otec
<g/>
)	)	kIx)	)
Věra	Věra	k1gFnSc1	Věra
Crháková	Crháková	k1gFnSc1	Crháková
(	(	kIx(	(
<g/>
Terinina	Terinin	k2eAgFnSc1d1	Terinin
matka	matka	k1gFnSc1	matka
<g/>
)	)	kIx)	)
Romance	romance	k1gFnSc1	romance
pro	pro	k7c4	pro
křídlovku	křídlovka	k1gFnSc4	křídlovka
(	(	kIx(	(
<g/>
film	film	k1gInSc4	film
<g/>
)	)	kIx)	)
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc3d1	filmová
databázi	databáze	k1gFnSc3	databáze
Filmová	filmový	k2eAgFnSc1d1	filmová
databáze	databáze	k1gFnSc1	databáze
</s>
