<s>
Immanuel	Immanuel	k1gMnSc1	Immanuel
Kant	Kant	k1gMnSc1	Kant
(	(	kIx(	(
<g/>
22	[number]	k4	22
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1724	[number]	k4	1724
Královec	Královec	k1gInSc1	Královec
–	–	k?	–
12	[number]	k4	12
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1804	[number]	k4	1804
Královec	Královec	k1gInSc1	Královec
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
německý	německý	k2eAgMnSc1d1	německý
filosof	filosof	k1gMnSc1	filosof
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
evropských	evropský	k2eAgMnPc2d1	evropský
myslitelů	myslitel	k1gMnPc2	myslitel
a	a	k8xC	a
poslední	poslední	k2eAgMnSc1d1	poslední
z	z	k7c2	z
představitelů	představitel	k1gMnPc2	představitel
osvícenství	osvícenství	k1gNnSc2	osvícenství
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
Kritikou	kritika	k1gFnSc7	kritika
čistého	čistý	k2eAgInSc2d1	čistý
rozumu	rozum	k1gInSc2	rozum
začíná	začínat	k5eAaImIp3nS	začínat
nové	nový	k2eAgNnSc4d1	nové
pojetí	pojetí	k1gNnSc4	pojetí
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
epistemologii	epistemologie	k1gFnSc6	epistemologie
(	(	kIx(	(
<g/>
teorii	teorie	k1gFnSc6	teorie
poznání	poznání	k1gNnSc4	poznání
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
etice	etika	k1gFnSc6	etika
<g/>
.	.	kIx.	.
</s>
<s>
Kant	Kant	k1gMnSc1	Kant
významně	významně	k6eAd1	významně
ovlivnil	ovlivnit	k5eAaPmAgMnS	ovlivnit
pozdější	pozdní	k2eAgInPc4d2	pozdější
romantické	romantický	k2eAgInPc4d1	romantický
a	a	k8xC	a
idealistické	idealistický	k2eAgMnPc4d1	idealistický
filosofy	filosof	k1gMnPc4	filosof
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
a	a	k8xC	a
novější	nový	k2eAgFnSc6d2	novější
filosofii	filosofie	k1gFnSc6	filosofie
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Kant	Kant	k1gMnSc1	Kant
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
jako	jako	k9	jako
čtvrté	čtvrtý	k4xOgNnSc4	čtvrtý
dítě	dítě	k1gNnSc4	dítě
ve	v	k7c6	v
zbožné	zbožný	k2eAgFnSc6d1	zbožná
pietistické	pietistický	k2eAgFnSc6d1	pietistická
rodině	rodina	k1gFnSc6	rodina
drobného	drobný	k2eAgMnSc4d1	drobný
řemeslníka	řemeslník	k1gMnSc4	řemeslník
a	a	k8xC	a
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
prožil	prožít	k5eAaPmAgMnS	prožít
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Královce	Královec	k1gInSc2	Královec
ve	v	k7c6	v
východním	východní	k2eAgNnSc6d1	východní
Prusku	Prusko	k1gNnSc6	Prusko
(	(	kIx(	(
<g/>
Königsberg	Königsberg	k1gMnSc1	Königsberg
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
Kaliningrad	Kaliningrad	k1gInSc1	Kaliningrad
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
z	z	k7c2	z
jeho	jeho	k3xOp3gNnPc2	jeho
osmi	osm	k4xCc2	osm
sourozenců	sourozenec	k1gMnPc2	sourozenec
se	se	k3xPyFc4	se
však	však	k9	však
jen	jen	k9	jen
čtyři	čtyři	k4xCgMnPc1	čtyři
dožili	dožít	k5eAaPmAgMnP	dožít
dospělého	dospělý	k1gMnSc4	dospělý
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
drobné	drobná	k1gFnPc4	drobná
až	až	k6eAd1	až
neduživé	duživý	k2eNgFnPc4d1	neduživá
postavy	postava	k1gFnPc4	postava
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
však	však	k9	však
velmi	velmi	k6eAd1	velmi
společenský	společenský	k2eAgInSc1d1	společenský
a	a	k8xC	a
sám	sám	k3xTgMnSc1	sám
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
poměrně	poměrně	k6eAd1	poměrně
přísný	přísný	k2eAgInSc1d1	přísný
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1740	[number]	k4	1740
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c6	na
zdejší	zdejší	k2eAgFnSc6d1	zdejší
univerzitě	univerzita	k1gFnSc6	univerzita
filosofii	filosofie	k1gFnSc4	filosofie
<g/>
,	,	kIx,	,
přírodní	přírodní	k2eAgFnPc4d1	přírodní
vědy	věda	k1gFnPc4	věda
a	a	k8xC	a
matematiku	matematika	k1gFnSc4	matematika
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1746	[number]	k4	1746
mu	on	k3xPp3gMnSc3	on
zemřel	zemřít	k5eAaPmAgMnS	zemřít
otec	otec	k1gMnSc1	otec
a	a	k8xC	a
univerzita	univerzita	k1gFnSc1	univerzita
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
jeho	jeho	k3xOp3gFnSc4	jeho
disertační	disertační	k2eAgFnSc4d1	disertační
práci	práce	k1gFnSc4	práce
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
řadu	řad	k1gInSc2	řad
let	léto	k1gNnPc2	léto
živil	živit	k5eAaImAgMnS	živit
jako	jako	k9	jako
vychovatel	vychovatel	k1gMnSc1	vychovatel
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Královce	Královec	k1gInSc2	Královec
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
na	na	k7c4	na
univerzitu	univerzita	k1gFnSc4	univerzita
vydal	vydat	k5eAaPmAgInS	vydat
práci	práce	k1gFnSc4	práce
o	o	k7c6	o
"	"	kIx"	"
<g/>
Obecných	obecný	k2eAgFnPc6d1	obecná
dějinách	dějiny	k1gFnPc6	dějiny
přírody	příroda	k1gFnSc2	příroda
a	a	k8xC	a
teorii	teorie	k1gFnSc4	teorie
oblohy	obloha	k1gFnSc2	obloha
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1755	[number]	k4	1755
<g/>
)	)	kIx)	)
a	a	k8xC	a
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
habilitoval	habilitovat	k5eAaBmAgMnS	habilitovat
spisem	spis	k1gInSc7	spis
"	"	kIx"	"
<g/>
O	o	k7c6	o
prvních	první	k4xOgFnPc6	první
zásadách	zásada	k1gFnPc6	zásada
metafyzického	metafyzický	k2eAgInSc2d1	metafyzický
poznání	poznání	k1gNnSc3	poznání
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Přednášel	přednášet	k5eAaImAgMnS	přednášet
logiku	logika	k1gFnSc4	logika
<g/>
,	,	kIx,	,
morální	morální	k2eAgFnSc4d1	morální
filosofii	filosofie	k1gFnSc4	filosofie
a	a	k8xC	a
metafyziku	metafyzika	k1gFnSc4	metafyzika
a	a	k8xC	a
jako	jako	k9	jako
úvod	úvod	k1gInSc4	úvod
ke	k	k7c3	k
studiu	studio	k1gNnSc3	studio
filosofie	filosofie	k1gFnSc2	filosofie
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
,	,	kIx,	,
geografii	geografie	k1gFnSc4	geografie
a	a	k8xC	a
antropologii	antropologie	k1gFnSc4	antropologie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
několik	několik	k4yIc4	několik
pozvání	pozvání	k1gNnPc2	pozvání
na	na	k7c4	na
různé	různý	k2eAgFnPc4d1	různá
univerzity	univerzita	k1gFnPc4	univerzita
<g/>
,	,	kIx,	,
až	až	k8xS	až
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1770	[number]	k4	1770
stal	stát	k5eAaPmAgMnS	stát
profesorem	profesor	k1gMnSc7	profesor
logiky	logika	k1gFnSc2	logika
a	a	k8xC	a
metafyziky	metafyzika	k1gFnSc2	metafyzika
v	v	k7c6	v
Královci	Královec	k1gInSc6	Královec
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
pak	pak	k6eAd1	pak
dvakrát	dvakrát	k6eAd1	dvakrát
rektorem	rektor	k1gMnSc7	rektor
<g/>
.	.	kIx.	.
1787	[number]	k4	1787
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
do	do	k7c2	do
Pruské	pruský	k2eAgFnSc2d1	pruská
akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
<g/>
,	,	kIx,	,
v	v	k7c6	v
posledním	poslední	k2eAgNnSc6d1	poslední
období	období	k1gNnSc6	období
života	život	k1gInSc2	život
měl	mít	k5eAaImAgInS	mít
však	však	k9	však
značné	značný	k2eAgFnPc4d1	značná
nesnáze	nesnáz	k1gFnPc4	nesnáz
s	s	k7c7	s
pruskou	pruský	k2eAgFnSc7d1	pruská
cenzurou	cenzura	k1gFnSc7	cenzura
<g/>
.	.	kIx.	.
</s>
<s>
Kant	Kant	k1gMnSc1	Kant
se	se	k3xPyFc4	se
řídil	řídit	k5eAaImAgMnS	řídit
přesným	přesný	k2eAgInSc7d1	přesný
denním	denní	k2eAgInSc7d1	denní
rozvrhem	rozvrh	k1gInSc7	rozvrh
<g/>
:	:	kIx,	:
vstával	vstávat	k5eAaImAgInS	vstávat
v	v	k7c4	v
pět	pět	k4xCc4	pět
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
dopoledne	dopoledne	k6eAd1	dopoledne
přednášel	přednášet	k5eAaImAgMnS	přednášet
<g/>
,	,	kIx,	,
k	k	k7c3	k
obědu	oběd	k1gInSc3	oběd
často	často	k6eAd1	často
zval	zvát	k5eAaImAgMnS	zvát
hosty	host	k1gMnPc4	host
a	a	k8xC	a
odpoledne	odpoledne	k6eAd1	odpoledne
chodil	chodit	k5eAaImAgMnS	chodit
na	na	k7c4	na
procházku	procházka	k1gFnSc4	procházka
po	po	k7c6	po
známé	známý	k2eAgFnSc6d1	známá
"	"	kIx"	"
<g/>
Cestě	cesta	k1gFnSc6	cesta
filosofů	filosof	k1gMnPc2	filosof
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
ale	ale	k9	ale
líčí	líčit	k5eAaImIp3nS	líčit
jako	jako	k9	jako
profesorský	profesorský	k2eAgMnSc1d1	profesorský
pedant	pedant	k1gMnSc1	pedant
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
přehnané	přehnaný	k2eAgNnSc1d1	přehnané
<g/>
.	.	kIx.	.
</s>
<s>
Hrával	hrávat	k5eAaImAgMnS	hrávat
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
karty	karta	k1gFnPc4	karta
i	i	k8xC	i
kulečník	kulečník	k1gInSc4	kulečník
a	a	k8xC	a
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
byl	být	k5eAaImAgInS	být
velice	velice	k6eAd1	velice
oblíbený	oblíbený	k2eAgInSc1d1	oblíbený
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
jako	jako	k9	jako
skvělý	skvělý	k2eAgMnSc1d1	skvělý
vypravěč	vypravěč	k1gMnSc1	vypravěč
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
na	na	k7c4	na
jeho	jeho	k3xOp3gFnPc4	jeho
přednášky	přednáška	k1gFnPc4	přednáška
vzpomínali	vzpomínat	k5eAaImAgMnP	vzpomínat
jeho	jeho	k3xOp3gMnPc1	jeho
žáci	žák	k1gMnPc1	žák
s	s	k7c7	s
nadšením	nadšení	k1gNnSc7	nadšení
<g/>
:	:	kIx,	:
Kant	Kant	k1gMnSc1	Kant
byl	být	k5eAaImAgMnS	být
vtipný	vtipný	k2eAgInSc4d1	vtipný
a	a	k8xC	a
hlavně	hlavně	k6eAd1	hlavně
vedl	vést	k5eAaImAgMnS	vést
své	svůj	k3xOyFgMnPc4	svůj
posluchače	posluchač	k1gMnPc4	posluchač
k	k	k7c3	k
samostatnému	samostatný	k2eAgNnSc3d1	samostatné
myšlení	myšlení	k1gNnSc3	myšlení
<g/>
.	.	kIx.	.
</s>
<s>
Dožil	dožít	k5eAaPmAgMnS	dožít
se	se	k3xPyFc4	se
téměř	téměř	k6eAd1	téměř
80	[number]	k4	80
let	léto	k1gNnPc2	léto
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
v	v	k7c6	v
Královeckém	Královecký	k2eAgInSc6d1	Královecký
dómu	dóm	k1gInSc6	dóm
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
zničeném	zničený	k2eAgInSc6d1	zničený
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
obnoveném	obnovený	k2eAgInSc6d1	obnovený
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ruské	ruský	k2eAgFnSc6d1	ruská
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Královci	Královec	k1gInSc6	Královec
je	být	k5eAaImIp3nS	být
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
se	s	k7c7	s
slavným	slavný	k2eAgInSc7d1	slavný
citátem	citát	k1gInSc7	citát
<g/>
:	:	kIx,	:
Dvě	dva	k4xCgFnPc1	dva
věci	věc	k1gFnPc1	věc
naplňují	naplňovat	k5eAaImIp3nP	naplňovat
mysl	mysl	k1gFnSc4	mysl
vždy	vždy	k6eAd1	vždy
novým	nový	k2eAgInSc7d1	nový
a	a	k8xC	a
rostoucím	rostoucí	k2eAgInSc7d1	rostoucí
obdivem	obdiv	k1gInSc7	obdiv
a	a	k8xC	a
úctou	úcta	k1gFnSc7	úcta
<g/>
,	,	kIx,	,
čím	co	k3yInSc7	co
častěji	často	k6eAd2	často
se	se	k3xPyFc4	se
jimi	on	k3xPp3gMnPc7	on
zabývá	zabývat	k5eAaImIp3nS	zabývat
<g/>
:	:	kIx,	:
hvězdné	hvězdný	k2eAgNnSc4d1	Hvězdné
nebe	nebe	k1gNnSc4	nebe
nade	nad	k7c7	nad
mnou	já	k3xPp1nSc7	já
a	a	k8xC	a
mravní	mravní	k2eAgInSc1d1	mravní
zákon	zákon	k1gInSc1	zákon
ve	v	k7c6	v
mně	já	k3xPp1nSc6	já
<g/>
.	.	kIx.	.
</s>
<s>
Kantovo	Kantův	k2eAgNnSc1d1	Kantovo
myšlení	myšlení	k1gNnSc1	myšlení
začíná	začínat	k5eAaImIp3nS	začínat
ve	v	k7c6	v
stopách	stopa	k1gFnPc6	stopa
Leibnizova	Leibnizův	k2eAgInSc2d1	Leibnizův
racionalismu	racionalismus	k1gInSc2	racionalismus
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
dobře	dobře	k6eAd1	dobře
vystihoval	vystihovat	k5eAaImAgInS	vystihovat
zkušenost	zkušenost	k1gFnSc4	zkušenost
matematika	matematik	k1gMnSc2	matematik
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInPc1	jehož
předměty	předmět	k1gInPc1	předmět
se	se	k3xPyFc4	se
nepoznávají	poznávat	k5eNaImIp3nP	poznávat
smysly	smysl	k1gInPc1	smysl
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
rozumem	rozum	k1gInSc7	rozum
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jako	jako	k9	jako
"	"	kIx"	"
<g/>
věci	věc	k1gFnPc4	věc
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
"	"	kIx"	"
<g/>
předkritickém	předkritický	k2eAgMnSc6d1	předkritický
<g/>
"	"	kIx"	"
období	období	k1gNnSc6	období
vydal	vydat	k5eAaPmAgMnS	vydat
důležitou	důležitý	k2eAgFnSc4d1	důležitá
práci	práce	k1gFnSc4	práce
o	o	k7c6	o
vzniku	vznik	k1gInSc6	vznik
planetárního	planetární	k2eAgInSc2d1	planetární
systému	systém	k1gInSc2	systém
"	"	kIx"	"
<g/>
podle	podle	k7c2	podle
Newtonových	Newtonových	k2eAgInPc2d1	Newtonových
zákonů	zákon	k1gInPc2	zákon
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
představil	představit	k5eAaPmAgMnS	představit
teorii	teorie	k1gFnSc3	teorie
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc3	jenž
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
říká	říkat	k5eAaImIp3nS	říkat
Kantova-Laplaceova	Kantova-Laplaceův	k2eAgFnSc1d1	Kantova-Laplaceova
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
práce	práce	k1gFnPc1	práce
o	o	k7c6	o
ohni	oheň	k1gInSc6	oheň
a	a	k8xC	a
o	o	k7c6	o
základech	základ	k1gInPc6	základ
metafysického	metafysický	k2eAgNnSc2d1	metafysické
poznávání	poznávání	k1gNnSc2	poznávání
(	(	kIx(	(
<g/>
Nova	nova	k1gFnSc1	nova
dilucidatio	dilucidatio	k1gMnSc1	dilucidatio
<g/>
)	)	kIx)	)
psal	psát	k5eAaImAgMnS	psát
latinsky	latinsky	k6eAd1	latinsky
<g/>
,	,	kIx,	,
práci	práce	k1gFnSc4	práce
"	"	kIx"	"
<g/>
O	o	k7c6	o
jediném	jediný	k2eAgInSc6d1	jediný
možném	možný	k2eAgInSc6d1	možný
důkazu	důkaz	k1gInSc6	důkaz
k	k	k7c3	k
prokazování	prokazování	k1gNnSc3	prokazování
existence	existence	k1gFnSc2	existence
Boží	boží	k2eAgFnSc2d1	boží
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
podal	podat	k5eAaPmAgMnS	podat
vlastní	vlastní	k2eAgFnSc4d1	vlastní
verzi	verze	k1gFnSc4	verze
tzv.	tzv.	kA	tzv.
ontologického	ontologický	k2eAgInSc2d1	ontologický
důkazu	důkaz	k1gInSc2	důkaz
<g/>
,	,	kIx,	,
opět	opět	k6eAd1	opět
německy	německy	k6eAd1	německy
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1766	[number]	k4	1766
vydal	vydat	k5eAaPmAgInS	vydat
spis	spis	k1gInSc1	spis
"	"	kIx"	"
<g/>
Sny	sen	k1gInPc1	sen
duchařovy	duchařův	k2eAgFnSc2d1	duchařův
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Träume	Träum	k1gInSc5	Träum
eines	eines	k1gMnSc1	eines
Geistersehers	Geistersehers	k1gInSc1	Geistersehers
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kritiku	kritika	k1gFnSc4	kritika
švédského	švédský	k2eAgMnSc2d1	švédský
mystika	mystik	k1gMnSc2	mystik
Swedenborga	Swedenborg	k1gMnSc2	Swedenborg
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
latinském	latinský	k2eAgInSc6d1	latinský
spise	spis	k1gInSc6	spis
"	"	kIx"	"
<g/>
O	o	k7c6	o
formě	forma	k1gFnSc6	forma
a	a	k8xC	a
principech	princip	k1gInPc6	princip
světa	svět	k1gInSc2	svět
smyslového	smyslový	k2eAgNnSc2d1	smyslové
a	a	k8xC	a
poznatelného	poznatelný	k2eAgNnSc2d1	poznatelné
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1770	[number]	k4	1770
<g/>
)	)	kIx)	)
poprvé	poprvé	k6eAd1	poprvé
ostře	ostro	k6eAd1	ostro
rozlišil	rozlišit	k5eAaPmAgMnS	rozlišit
mezi	mezi	k7c7	mezi
smyslovým	smyslový	k2eAgNnSc7d1	smyslové
poznáním	poznání	k1gNnSc7	poznání
jevů	jev	k1gInPc2	jev
(	(	kIx(	(
<g/>
fenoménů	fenomén	k1gInPc2	fenomén
<g/>
)	)	kIx)	)
a	a	k8xC	a
rozumovým	rozumový	k2eAgNnSc7d1	rozumové
poznáním	poznání	k1gNnSc7	poznání
věcí	věc	k1gFnPc2	věc
samých	samý	k3xTgMnPc6	samý
(	(	kIx(	(
<g/>
noumena	noumenon	k1gNnSc2	noumenon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
matematice	matematika	k1gFnSc6	matematika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
Kant	Kant	k1gMnSc1	Kant
četl	číst	k5eAaImAgMnS	číst
Davida	David	k1gMnSc4	David
Huma	Humus	k1gMnSc4	Humus
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ho	on	k3xPp3gNnSc4	on
podle	podle	k7c2	podle
jeho	jeho	k3xOp3gNnPc2	jeho
vlastních	vlastní	k2eAgNnPc2d1	vlastní
slov	slovo	k1gNnPc2	slovo
"	"	kIx"	"
<g/>
probudil	probudit	k5eAaPmAgInS	probudit
z	z	k7c2	z
dogmatického	dogmatický	k2eAgInSc2d1	dogmatický
spánku	spánek	k1gInSc2	spánek
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Uvědomil	uvědomit	k5eAaPmAgInS	uvědomit
si	se	k3xPyFc3	se
totiž	totiž	k9	totiž
platnost	platnost	k1gFnSc4	platnost
Humova	Humův	k2eAgInSc2d1	Humův
soudu	soud	k1gInSc2	soud
<g/>
,	,	kIx,	,
že	že	k8xS	že
rozumové	rozumový	k2eAgNnSc1d1	rozumové
poznání	poznání	k1gNnSc1	poznání
bez	bez	k7c2	bez
smyslového	smyslový	k2eAgInSc2d1	smyslový
názoru	názor	k1gInSc2	názor
není	být	k5eNaImIp3nS	být
obecně	obecně	k6eAd1	obecně
možné	možný	k2eAgNnSc1d1	možné
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
Humeův	Humeův	k2eAgInSc4d1	Humeův
empirismus	empirismus	k1gInSc4	empirismus
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
jisté	jistý	k2eAgNnSc1d1	jisté
poznání	poznání	k1gNnSc1	poznání
není	být	k5eNaImIp3nS	být
vůbec	vůbec	k9	vůbec
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
a	a	k8xC	a
tedy	tedy	k9	tedy
ke	k	k7c3	k
skepticismu	skepticismus	k1gInSc3	skepticismus
<g/>
.	.	kIx.	.
</s>
<s>
Jenže	jenže	k8xC	jenže
některé	některý	k3yIgInPc1	některý
apriorní	apriorní	k2eAgInPc1d1	apriorní
soudy	soud	k1gInPc1	soud
–	–	k?	–
například	například	k6eAd1	například
že	že	k8xS	že
2	[number]	k4	2
+	+	kIx~	+
2	[number]	k4	2
=	=	kIx~	=
4	[number]	k4	4
–	–	k?	–
jsou	být	k5eAaImIp3nP	být
nejen	nejen	k6eAd1	nejen
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dokonce	dokonce	k9	dokonce
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
.	.	kIx.	.
</s>
<s>
Něco	něco	k3yInSc1	něco
podobného	podobný	k2eAgNnSc2d1	podobné
platí	platit	k5eAaImIp3nS	platit
podle	podle	k7c2	podle
Kanta	Kant	k1gMnSc2	Kant
i	i	k9	i
o	o	k7c6	o
soudech	soud	k1gInPc6	soud
teoretické	teoretický	k2eAgFnSc2d1	teoretická
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
,	,	kIx,	,
a	a	k8xC	a
vzniká	vznikat	k5eAaImIp3nS	vznikat
tedy	tedy	k9	tedy
otázka	otázka	k1gFnSc1	otázka
<g/>
,	,	kIx,	,
jaké	jaký	k3yQgFnPc1	jaký
jsou	být	k5eAaImIp3nP	být
podmínky	podmínka	k1gFnPc4	podmínka
možnosti	možnost	k1gFnSc2	možnost
takového	takový	k3xDgNnSc2	takový
poznání	poznání	k1gNnSc2	poznání
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
odmlce	odmlka	k1gFnSc6	odmlka
vydává	vydávat	k5eAaImIp3nS	vydávat
roku	rok	k1gInSc2	rok
1781	[number]	k4	1781
Kant	Kant	k1gMnSc1	Kant
v	v	k7c6	v
Rize	Riga	k1gFnSc6	Riga
své	svůj	k3xOyFgNnSc4	svůj
stěžejní	stěžejní	k2eAgNnSc4d1	stěžejní
dílo	dílo	k1gNnSc4	dílo
<g/>
,	,	kIx,	,
Kritiku	kritika	k1gFnSc4	kritika
čistého	čistý	k2eAgInSc2d1	čistý
rozumu	rozum	k1gInSc2	rozum
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
úkol	úkol	k1gInSc1	úkol
filosofie	filosofie	k1gFnSc2	filosofie
vymezuje	vymezovat	k5eAaImIp3nS	vymezovat
třemi	tři	k4xCgFnPc7	tři
otázkami	otázka	k1gFnPc7	otázka
<g/>
:	:	kIx,	:
Co	co	k9	co
mohu	moct	k5eAaImIp1nS	moct
vědět	vědět	k5eAaImF	vědět
<g/>
?	?	kIx.	?
</s>
<s>
na	na	k7c4	na
niž	jenž	k3xRgFnSc4	jenž
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
jeho	jeho	k3xOp3gFnSc1	jeho
teorie	teorie	k1gFnSc1	teorie
poznání	poznání	k1gNnSc2	poznání
(	(	kIx(	(
<g/>
epistemologie	epistemologie	k1gFnSc1	epistemologie
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Co	co	k3yQnSc1	co
mám	mít	k5eAaImIp1nS	mít
činit	činit	k5eAaImF	činit
<g/>
?	?	kIx.	?
</s>
<s>
na	na	k7c4	na
niž	jenž	k3xRgFnSc4	jenž
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
jeho	jeho	k3xOp3gFnSc1	jeho
etika	etika	k1gFnSc1	etika
a	a	k8xC	a
V	v	k7c4	v
co	co	k3yRnSc4	co
mohu	moct	k5eAaImIp1nS	moct
doufat	doufat	k5eAaImF	doufat
<g/>
?	?	kIx.	?
</s>
<s>
na	na	k7c4	na
niž	jenž	k3xRgFnSc4	jenž
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
odpovědět	odpovědět	k5eAaPmF	odpovědět
filosofie	filosofie	k1gFnSc1	filosofie
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Kantův	Kantův	k2eAgInSc1d1	Kantův
"	"	kIx"	"
<g/>
kritický	kritický	k2eAgInSc1d1	kritický
obrat	obrat	k1gInSc1	obrat
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
objevem	objev	k1gInSc7	objev
nové	nový	k2eAgFnSc2d1	nová
oblasti	oblast	k1gFnSc2	oblast
filosofického	filosofický	k2eAgNnSc2d1	filosofické
poznání	poznání	k1gNnSc2	poznání
<g/>
,	,	kIx,	,
epistemologie	epistemologie	k1gFnSc2	epistemologie
či	či	k8xC	či
gnoseologie	gnoseologie	k1gFnSc2	gnoseologie
<g/>
.	.	kIx.	.
</s>
<s>
Transcendentální	transcendentální	k2eAgFnSc1d1	transcendentální
filosofie	filosofie	k1gFnSc1	filosofie
se	se	k3xPyFc4	se
nemá	mít	k5eNaImIp3nS	mít
zabývat	zabývat	k5eAaImF	zabývat
obsahem	obsah	k1gInSc7	obsah
našeho	náš	k3xOp1gNnSc2	náš
poznání	poznání	k1gNnSc2	poznání
<g/>
,	,	kIx,	,
povahou	povaha	k1gFnSc7	povaha
poznávaných	poznávaný	k2eAgFnPc2d1	poznávaná
věcí	věc	k1gFnPc2	věc
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
"	"	kIx"	"
<g/>
podmínkami	podmínka	k1gFnPc7	podmínka
možnosti	možnost	k1gFnSc2	možnost
<g/>
"	"	kIx"	"
poznání	poznání	k1gNnSc4	poznání
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
praktický	praktický	k2eAgInSc4d1	praktický
čili	čili	k8xC	čili
naivní	naivní	k2eAgInSc4d1	naivní
postoj	postoj	k1gInSc4	postoj
běžného	běžný	k2eAgInSc2d1	běžný
života	život	k1gInSc2	život
pokládá	pokládat	k5eAaImIp3nS	pokládat
věci	věc	k1gFnPc4	věc
za	za	k7c2	za
bezprostředně	bezprostředně	k6eAd1	bezprostředně
dané	daný	k2eAgFnSc2d1	daná
<g/>
,	,	kIx,	,
transcendentální	transcendentální	k2eAgFnSc2d1	transcendentální
filosofie	filosofie	k1gFnSc2	filosofie
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nám	my	k3xPp1nPc3	my
věci	věc	k1gFnSc3	věc
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
děje	dít	k5eAaImIp3nS	dít
<g/>
.	.	kIx.	.
</s>
<s>
Kant	Kant	k1gMnSc1	Kant
zde	zde	k6eAd1	zde
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
Descarta	Descart	k1gMnSc4	Descart
a	a	k8xC	a
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
jeho	jeho	k3xOp3gInSc4	jeho
objev	objev	k1gInSc4	objev
naopak	naopak	k6eAd1	naopak
naváže	navázat	k5eAaPmIp3nS	navázat
později	pozdě	k6eAd2	pozdě
fenomenologie	fenomenologie	k1gFnSc1	fenomenologie
<g/>
.	.	kIx.	.
</s>
<s>
Kritika	kritika	k1gFnSc1	kritika
čistého	čistý	k2eAgInSc2d1	čistý
rozumu	rozum	k1gInSc2	rozum
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
odpověď	odpověď	k1gFnSc1	odpověď
tradiční	tradiční	k2eAgFnSc2d1	tradiční
metafysice	metafysika	k1gFnSc3	metafysika
a	a	k8xC	a
racionalismu	racionalismus	k1gInSc3	racionalismus
(	(	kIx(	(
<g/>
Christian	Christian	k1gMnSc1	Christian
Wolff	Wolff	k1gMnSc1	Wolff
<g/>
,	,	kIx,	,
A.	A.	kA	A.
G.	G.	kA	G.
Baumgarten	Baumgarten	k2eAgMnSc1d1	Baumgarten
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
že	že	k8xS	že
poznání	poznání	k1gNnSc1	poznání
bez	bez	k7c2	bez
vnímání	vnímání	k1gNnSc2	vnímání
čili	čili	k8xC	čili
smyslového	smyslový	k2eAgInSc2d1	smyslový
názoru	názor	k1gInSc2	názor
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
proti	proti	k7c3	proti
Humeovu	Humeův	k2eAgInSc3d1	Humeův
empirismu	empirismus	k1gInSc3	empirismus
namítá	namítat	k5eAaImIp3nS	namítat
<g/>
,	,	kIx,	,
že	že	k8xS	že
smyslové	smyslový	k2eAgNnSc1d1	smyslové
vnímání	vnímání	k1gNnSc1	vnímání
není	být	k5eNaImIp3nS	být
nijak	nijak	k6eAd1	nijak
rozčleněno	rozčleněn	k2eAgNnSc1d1	rozčleněno
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
rozvažovací	rozvažovací	k2eAgFnSc1d1	rozvažovací
schopnost	schopnost	k1gFnSc1	schopnost
(	(	kIx(	(
<g/>
Verstand	Verstand	k1gInSc1	Verstand
<g/>
)	)	kIx)	)
nedodá	dodat	k5eNaPmIp3nS	dodat
pojmy	pojem	k1gInPc1	pojem
a	a	k8xC	a
nespojí	spojit	k5eNaPmIp3nP	spojit
je	on	k3xPp3gNnSc4	on
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
pomocí	pomocí	k7c2	pomocí
soudů	soud	k1gInPc2	soud
a	a	k8xC	a
závěrů	závěr	k1gInPc2	závěr
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
pak	pak	k6eAd1	pak
lze	lze	k6eAd1	lze
hovořit	hovořit	k5eAaImF	hovořit
o	o	k7c4	o
poznání	poznání	k1gNnSc4	poznání
<g/>
.	.	kIx.	.
</s>
<s>
Myšlenky	myšlenka	k1gFnPc1	myšlenka
bez	bez	k7c2	bez
obsahu	obsah	k1gInSc2	obsah
jsou	být	k5eAaImIp3nP	být
prázdné	prázdná	k1gFnPc1	prázdná
<g/>
,	,	kIx,	,
smyslový	smyslový	k2eAgInSc1d1	smyslový
názor	názor	k1gInSc1	názor
bez	bez	k7c2	bez
pojmů	pojem	k1gInPc2	pojem
je	být	k5eAaImIp3nS	být
slepý	slepý	k2eAgMnSc1d1	slepý
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
ani	ani	k9	ani
smyslové	smyslový	k2eAgInPc4d1	smyslový
vjemy	vjem	k1gInPc4	vjem
nejsou	být	k5eNaImIp3nP	být
nepodmíněné	podmíněný	k2eNgInPc1d1	nepodmíněný
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
předpokládají	předpokládat	k5eAaImIp3nP	předpokládat
podle	podle	k7c2	podle
Kanta	Kant	k1gMnSc2	Kant
"	"	kIx"	"
<g/>
vnější	vnější	k2eAgInSc1d1	vnější
smysl	smysl	k1gInSc1	smysl
<g/>
"	"	kIx"	"
prostoru	prostor	k1gInSc2	prostor
a	a	k8xC	a
"	"	kIx"	"
<g/>
vnitřní	vnitřní	k2eAgInSc4d1	vnitřní
smysl	smysl	k1gInSc4	smysl
<g/>
"	"	kIx"	"
času	čas	k1gInSc2	čas
<g/>
:	:	kIx,	:
vnější	vnější	k2eAgFnSc1d1	vnější
(	(	kIx(	(
<g/>
smyslová	smyslový	k2eAgFnSc1d1	smyslová
<g/>
)	)	kIx)	)
zkušenost	zkušenost	k1gFnSc1	zkušenost
je	být	k5eAaImIp3nS	být
vůbec	vůbec	k9	vůbec
možná	možná	k9	možná
jen	jen	k9	jen
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
a	a	k8xC	a
každá	každý	k3xTgFnSc1	každý
zkušenost	zkušenost	k1gFnSc1	zkušenost
se	se	k3xPyFc4	se
nutně	nutně	k6eAd1	nutně
děje	dít	k5eAaImIp3nS	dít
v	v	k7c6	v
čase	čas	k1gInSc6	čas
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
i	i	k9	i
logická	logický	k2eAgFnSc1d1	logická
věta	věta	k1gFnSc1	věta
o	o	k7c6	o
vyloučeném	vyloučený	k2eAgMnSc6d1	vyloučený
třetím	třetí	k4xOgNnSc6	třetí
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nějaké	nějaký	k3yIgNnSc1	nějaký
tvrzení	tvrzení	k1gNnSc1	tvrzení
A	a	k9	a
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
opak	opak	k1gInSc1	opak
non-A	non-A	k?	non-A
nemohou	moct	k5eNaImIp3nP	moct
platit	platit	k5eAaImF	platit
současně	současně	k6eAd1	současně
<g/>
;	;	kIx,	;
závisí	záviset	k5eAaImIp3nS	záviset
tedy	tedy	k9	tedy
také	také	k9	také
na	na	k7c6	na
čase	čas	k1gInSc6	čas
<g/>
.	.	kIx.	.
</s>
<s>
Naše	náš	k3xOp1gInPc1	náš
smysly	smysl	k1gInPc1	smysl
jsou	být	k5eAaImIp3nP	být
receptivní	receptivní	k2eAgInPc1d1	receptivní
a	a	k8xC	a
vnímají	vnímat	k5eAaImIp3nP	vnímat
působení	působení	k1gNnSc4	působení
věcí	věc	k1gFnPc2	věc
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInPc1	jejich
jevy	jev	k1gInPc1	jev
čili	čili	k8xC	čili
fenomény	fenomén	k1gInPc1	fenomén
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
věci	věc	k1gFnPc4	věc
samé	samý	k3xTgFnPc4	samý
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
jak	jak	k6eAd1	jak
jsou	být	k5eAaImIp3nP	být
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Věc	věc	k1gFnSc1	věc
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
(	(	kIx(	(
<g/>
Ding	Ding	k1gInSc1	Ding
an	an	k?	an
sich	sich	k1gInSc1	sich
<g/>
)	)	kIx)	)
patrně	patrně	k6eAd1	patrně
existuje	existovat	k5eAaImIp3nS	existovat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
"	"	kIx"	"
<g/>
afikuje	afikovat	k5eAaBmIp3nS	afikovat
<g/>
"	"	kIx"	"
čili	čili	k8xC	čili
dráždí	dráždit	k5eAaImIp3nP	dráždit
naše	náš	k3xOp1gInPc4	náš
smysly	smysl	k1gInPc4	smysl
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nám	my	k3xPp1nPc3	my
však	však	k9	však
přístupná	přístupný	k2eAgFnSc1d1	přístupná
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
působení	působení	k1gNnSc6	působení
a	a	k8xC	a
nijak	nijak	k6eAd1	nijak
jinak	jinak	k6eAd1	jinak
<g/>
.	.	kIx.	.
</s>
<s>
Samy	sám	k3xTgInPc1	sám
smyslové	smyslový	k2eAgInPc1d1	smyslový
vjemy	vjem	k1gInPc1	vjem
však	však	k9	však
nejsou	být	k5eNaImIp3nP	být
ještě	ještě	k6eAd1	ještě
ani	ani	k8xC	ani
jevy	jev	k1gInPc1	jev
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
je	být	k5eAaImIp3nS	být
nejprve	nejprve	k6eAd1	nejprve
třeba	třeba	k6eAd1	třeba
spojit	spojit	k5eAaPmF	spojit
vjemy	vjem	k1gInPc4	vjem
různých	různý	k2eAgInPc2d1	různý
smyslů	smysl	k1gInPc2	smysl
(	(	kIx(	(
<g/>
např.	např.	kA	např.
obou	dva	k4xCgNnPc2	dva
očí	oko	k1gNnPc2	oko
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
i	i	k8xC	i
sluchu	sluch	k1gInSc2	sluch
nebo	nebo	k8xC	nebo
hmatu	hmat	k1gInSc2	hmat
–	–	k?	–
Kant	Kant	k1gMnSc1	Kant
zde	zde	k6eAd1	zde
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
se	s	k7c7	s
scholastikou	scholastika	k1gFnSc7	scholastika
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
"	"	kIx"	"
<g/>
společném	společný	k2eAgInSc6d1	společný
smyslu	smysl	k1gInSc6	smysl
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
sensus	sensus	k1gMnSc1	sensus
communis	communis	k1gFnSc2	communis
<g/>
)	)	kIx)	)
a	a	k8xC	a
zasadit	zasadit	k5eAaPmF	zasadit
je	on	k3xPp3gNnPc4	on
do	do	k7c2	do
rámce	rámec	k1gInSc2	rámec
apriorních	apriorní	k2eAgInPc2d1	apriorní
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
před-zkušenostních	předkušenostní	k2eAgFnPc2d1	před-zkušenostní
forem	forma	k1gFnPc2	forma
názoru	názor	k1gInSc2	názor
<g/>
,	,	kIx,	,
totiž	totiž	k8xC	totiž
prostoru	prostor	k1gInSc2	prostor
a	a	k8xC	a
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
tedy	tedy	k9	tedy
teprve	teprve	k6eAd1	teprve
myslící	myslící	k2eAgMnPc4d1	myslící
já	já	k3xPp1nSc1	já
<g/>
,	,	kIx,	,
Descartovo	Descartův	k2eAgNnSc4d1	Descartovo
cogito	cogit	k2eAgNnSc4d1	cogito
čili	čili	k8xC	čili
vědomí	vědomí	k1gNnSc4	vědomí
sebe	sebe	k3xPyFc4	sebe
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc4	jenž
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
činí	činit	k5eAaImIp3nP	činit
počitky	počitek	k1gInPc1	počitek
(	(	kIx(	(
<g/>
Empfindung	Empfindung	k1gInSc1	Empfindung
<g/>
)	)	kIx)	)
a	a	k8xC	a
jevy	jev	k1gInPc1	jev
(	(	kIx(	(
<g/>
Erscheinung	Erscheinung	k1gInSc1	Erscheinung
<g/>
)	)	kIx)	)
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
a	a	k8xC	a
čase	čas	k1gInSc6	čas
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
zhruba	zhruba	k6eAd1	zhruba
obsah	obsah	k1gInSc4	obsah
Kantovy	Kantův	k2eAgFnSc2d1	Kantova
transcendentální	transcendentální	k2eAgFnSc2d1	transcendentální
estetiky	estetika	k1gFnSc2	estetika
čili	čili	k8xC	čili
nauky	nauka	k1gFnSc2	nauka
o	o	k7c6	o
smyslovém	smyslový	k2eAgNnSc6d1	smyslové
vnímání	vnímání	k1gNnSc6	vnímání
<g/>
.	.	kIx.	.
</s>
<s>
Pojmy	pojem	k1gInPc1	pojem
ovšem	ovšem	k9	ovšem
nemohou	moct	k5eNaImIp3nP	moct
pocházet	pocházet	k5eAaImF	pocházet
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
počitků	počitek	k1gInPc2	počitek
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
jsou	být	k5eAaImIp3nP	být
výkonem	výkon	k1gInSc7	výkon
rozvažovací	rozvažovací	k2eAgFnSc2d1	rozvažovací
schopnosti	schopnost	k1gFnSc2	schopnost
(	(	kIx(	(
<g/>
Verstand	Verstand	k1gInSc1	Verstand
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
svojí	svůj	k3xOyFgFnSc7	svůj
představivostí	představivost	k1gFnSc7	představivost
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
podle	podle	k7c2	podle
jistých	jistý	k2eAgNnPc2d1	jisté
pravidel	pravidlo	k1gNnPc2	pravidlo
<g/>
.	.	kIx.	.
</s>
<s>
Výrazem	výraz	k1gInSc7	výraz
těchto	tento	k3xDgNnPc2	tento
pravidel	pravidlo	k1gNnPc2	pravidlo
jsou	být	k5eAaImIp3nP	být
kategorie	kategorie	k1gFnPc1	kategorie
<g/>
,	,	kIx,	,
základní	základní	k2eAgFnSc1d1	základní
třídicí	třídicí	k2eAgFnSc1d1	třídicí
funkce	funkce	k1gFnSc1	funkce
rozvažovací	rozvažovací	k2eAgFnSc2d1	rozvažovací
schopnosti	schopnost	k1gFnSc2	schopnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
teprve	teprve	k6eAd1	teprve
na	na	k7c6	na
základě	základ	k1gInSc6	základ
kategorií	kategorie	k1gFnPc2	kategorie
může	moct	k5eAaImIp3nS	moct
naše	náš	k3xOp1gFnSc1	náš
soudnost	soudnost	k1gFnSc1	soudnost
(	(	kIx(	(
<g/>
Urteilskraft	Urteilskraft	k1gInSc1	Urteilskraft
<g/>
)	)	kIx)	)
přisuzovat	přisuzovat	k5eAaImF	přisuzovat
či	či	k8xC	či
podřizovat	podřizovat	k5eAaImF	podřizovat
počitky	počitek	k1gInPc4	počitek
schématům	schéma	k1gNnPc3	schéma
<g/>
.	.	kIx.	.
</s>
<s>
Schéma	schéma	k1gNnSc1	schéma
je	být	k5eAaImIp3nS	být
obecný	obecný	k2eAgInSc4d1	obecný
postup	postup	k1gInSc4	postup
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
představivost	představivost	k1gFnSc1	představivost
spojuje	spojovat	k5eAaImIp3nS	spojovat
pojmy	pojem	k1gInPc4	pojem
s	s	k7c7	s
počitky	počitek	k1gInPc7	počitek
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
jezevčíkovi	jezevčík	k1gMnSc6	jezevčík
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc2	který
jsem	být	k5eAaImIp1nS	být
poznal	poznat	k5eAaPmAgMnS	poznat
na	na	k7c6	na
ulici	ulice	k1gFnSc6	ulice
<g/>
,	,	kIx,	,
vím	vědět	k5eAaImIp1nS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
pes	pes	k1gMnSc1	pes
<g/>
,	,	kIx,	,
savec	savec	k1gMnSc1	savec
<g/>
,	,	kIx,	,
zvíře	zvíře	k1gNnSc1	zvíře
<g/>
,	,	kIx,	,
živočich	živočich	k1gMnSc1	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Taková	takový	k3xDgFnSc1	takový
i	i	k9	i
vícestupňová	vícestupňový	k2eAgNnPc1d1	vícestupňové
schémata	schéma	k1gNnPc1	schéma
jsou	být	k5eAaImIp3nP	být
tedy	tedy	k9	tedy
strukturující	strukturující	k2eAgInPc1d1	strukturující
obecné	obecný	k2eAgInPc1d1	obecný
pojmy	pojem	k1gInPc1	pojem
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
vnímání	vnímání	k1gNnSc3	vnímání
týkají	týkat	k5eAaImIp3nP	týkat
<g/>
,	,	kIx,	,
pocházejí	pocházet	k5eAaImIp3nP	pocházet
však	však	k9	však
z	z	k7c2	z
rozvažovací	rozvažovací	k2eAgFnSc2d1	rozvažovací
schopnosti	schopnost	k1gFnSc2	schopnost
<g/>
.	.	kIx.	.
</s>
<s>
Každé	každý	k3xTgNnSc1	každý
poznání	poznání	k1gNnSc1	poznání
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
z	z	k7c2	z
povahy	povaha	k1gFnSc2	povaha
věci	věc	k1gFnPc4	věc
závislé	závislý	k2eAgFnPc4d1	závislá
na	na	k7c6	na
výkonech	výkon	k1gInPc6	výkon
mysli	mysl	k1gFnSc2	mysl
a	a	k8xC	a
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
smyslu	smysl	k1gInSc6	smysl
subjektivní	subjektivní	k2eAgInPc1d1	subjektivní
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
základě	základ	k1gInSc6	základ
si	se	k3xPyFc3	se
klade	klást	k5eAaImIp3nS	klást
Kant	Kant	k1gMnSc1	Kant
otázku	otázka	k1gFnSc4	otázka
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
možná	možná	k9	možná
vědecká	vědecký	k2eAgFnSc1d1	vědecká
metafyzika	metafyzika	k1gFnSc1	metafyzika
<g/>
,	,	kIx,	,
a	a	k8xC	a
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ano	ano	k9	ano
<g/>
.	.	kIx.	.
</s>
<s>
Čistým	čistý	k2eAgNnSc7d1	čisté
uvažováním	uvažování	k1gNnSc7	uvažování
můžeme	moct	k5eAaImIp1nP	moct
totiž	totiž	k9	totiž
dospět	dospět	k5eAaPmF	dospět
k	k	k7c3	k
soudům	soud	k1gInPc3	soud
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
naše	náš	k3xOp1gNnSc4	náš
poznání	poznání	k1gNnSc4	poznání
rozmnožují	rozmnožovat	k5eAaImIp3nP	rozmnožovat
<g/>
;	;	kIx,	;
Kant	Kant	k1gMnSc1	Kant
je	on	k3xPp3gNnSc4	on
nazývá	nazývat	k5eAaImIp3nS	nazývat
syntetické	syntetický	k2eAgInPc4d1	syntetický
soudy	soud	k1gInPc4	soud
a	a	k8xC	a
priori	priori	k6eAd1	priori
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
kategorie	kategorie	k1gFnSc2	kategorie
příčinnosti	příčinnost	k1gFnSc2	příčinnost
(	(	kIx(	(
<g/>
kauzality	kauzalita	k1gFnSc2	kauzalita
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
jako	jako	k9	jako
nemůžeme	moct	k5eNaImIp1nP	moct
pozorovat	pozorovat	k5eAaImF	pozorovat
prostor	prostor	k1gInSc4	prostor
a	a	k8xC	a
čas	čas	k1gInSc4	čas
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
nám	my	k3xPp1nPc3	my
ani	ani	k9	ani
představa	představa	k1gFnSc1	představa
příčinnosti	příčinnost	k1gFnSc2	příčinnost
dána	dát	k5eAaPmNgFnS	dát
ve	v	k7c6	v
smyslovém	smyslový	k2eAgNnSc6d1	smyslové
vnímání	vnímání	k1gNnSc6	vnímání
<g/>
.	.	kIx.	.
</s>
<s>
Pozorujeme	pozorovat	k5eAaImIp1nP	pozorovat
dvě	dva	k4xCgFnPc1	dva
události	událost	k1gFnPc1	událost
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
pravidelně	pravidelně	k6eAd1	pravidelně
následují	následovat	k5eAaImIp3nP	následovat
po	po	k7c6	po
sobě	sebe	k3xPyFc6	sebe
<g/>
,	,	kIx,	,
a	a	k8xC	a
protože	protože	k8xS	protože
jsme	být	k5eAaImIp1nP	být
přesvědčeni	přesvědčit	k5eAaPmNgMnP	přesvědčit
o	o	k7c6	o
příčinných	příčinný	k2eAgFnPc6d1	příčinná
souvislostech	souvislost	k1gFnPc6	souvislost
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
chápeme	chápat	k5eAaImIp1nP	chápat
je	on	k3xPp3gInPc4	on
nutně	nutně	k6eAd1	nutně
jako	jako	k9	jako
příčinu	příčina	k1gFnSc4	příčina
a	a	k8xC	a
následek	následek	k1gInSc4	následek
<g/>
.	.	kIx.	.
</s>
<s>
Příčinnost	příčinnost	k1gFnSc1	příčinnost
<g/>
,	,	kIx,	,
příčina	příčina	k1gFnSc1	příčina
a	a	k8xC	a
účinek	účinek	k1gInSc1	účinek
tedy	tedy	k8xC	tedy
naši	náš	k3xOp1gFnSc4	náš
smyslovou	smyslový	k2eAgFnSc4d1	smyslová
zkušenost	zkušenost	k1gFnSc4	zkušenost
přesahují	přesahovat	k5eAaImIp3nP	přesahovat
a	a	k8xC	a
patří	patřit	k5eAaImIp3nP	patřit
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
metafysické	metafysický	k2eAgFnSc2d1	metafysická
<g/>
.	.	kIx.	.
</s>
<s>
Omezenost	omezenost	k1gFnSc1	omezenost
lidského	lidský	k2eAgInSc2d1	lidský
rozumu	rozum	k1gInSc2	rozum
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
také	také	k9	také
v	v	k7c6	v
antinomiích	antinomie	k1gFnPc6	antinomie
<g/>
,	,	kIx,	,
ve	v	k7c6	v
dvojicích	dvojice	k1gFnPc6	dvojice
protikladných	protikladný	k2eAgFnPc2d1	protikladná
tvrzení	tvrzení	k1gNnPc2	tvrzení
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
přesto	přesto	k8xC	přesto
dají	dát	k5eAaPmIp3nP	dát
rozumově	rozumově	k6eAd1	rozumově
dokázat	dokázat	k5eAaPmF	dokázat
<g/>
.	.	kIx.	.
</s>
<s>
To	to	k9	to
Kant	Kant	k1gMnSc1	Kant
chápe	chápat	k5eAaImIp3nS	chápat
jako	jako	k9	jako
doklad	doklad	k1gInSc1	doklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
zde	zde	k6eAd1	zde
lidský	lidský	k2eAgInSc1d1	lidský
rozum	rozum	k1gInSc1	rozum
překračuje	překračovat	k5eAaImIp3nS	překračovat
svou	svůj	k3xOyFgFnSc4	svůj
kompetenci	kompetence	k1gFnSc4	kompetence
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
uznává	uznávat	k5eAaImIp3nS	uznávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tomu	ten	k3xDgMnSc3	ten
nemůže	moct	k5eNaImIp3nS	moct
vyhnout	vyhnout	k5eAaPmF	vyhnout
<g/>
.	.	kIx.	.
</s>
<s>
Rozum	rozum	k1gInSc1	rozum
je	být	k5eAaImIp3nS	být
ta	ten	k3xDgFnSc1	ten
část	část	k1gFnSc1	část
naší	náš	k3xOp1gFnSc2	náš
mysli	mysl	k1gFnSc2	mysl
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
z	z	k7c2	z
pojmů	pojem	k1gInPc2	pojem
a	a	k8xC	a
soudů	soud	k1gInPc2	soud
vyvozuje	vyvozovat	k5eAaImIp3nS	vyvozovat
závěry	závěr	k1gInPc7	závěr
<g/>
.	.	kIx.	.
</s>
<s>
Rozum	rozum	k1gInSc1	rozum
přirozeně	přirozeně	k6eAd1	přirozeně
tíhne	tíhnout	k5eAaImIp3nS	tíhnout
k	k	k7c3	k
dalším	další	k2eAgInPc3d1	další
a	a	k8xC	a
dalším	další	k2eAgNnSc7d1	další
poznáním	poznání	k1gNnSc7	poznání
a	a	k8xC	a
touží	toužit	k5eAaImIp3nP	toužit
po	po	k7c6	po
poznání	poznání	k1gNnSc6	poznání
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
je	být	k5eAaImIp3nS	být
nepodmíněné	podmíněný	k2eNgNnSc1d1	nepodmíněné
a	a	k8xC	a
absolutní	absolutní	k2eAgNnSc1d1	absolutní
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
vydává	vydávat	k5eAaImIp3nS	vydávat
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
spekulace	spekulace	k1gFnSc2	spekulace
a	a	k8xC	a
nutně	nutně	k6eAd1	nutně
vytváří	vytvářit	k5eAaPmIp3nP	vytvářit
tři	tři	k4xCgFnPc1	tři
regulativní	regulativní	k2eAgFnPc1d1	regulativní
ideje	idea	k1gFnPc1	idea
<g/>
:	:	kIx,	:
nesmrtelná	smrtelný	k2eNgFnSc1d1	nesmrtelná
duše	duše	k1gFnSc1	duše
<g/>
;	;	kIx,	;
svoboda	svoboda	k1gFnSc1	svoboda
nekonečno	nekonečno	k1gNnSc1	nekonečno
či	či	k8xC	či
Bůh	bůh	k1gMnSc1	bůh
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
těch	ten	k3xDgInPc6	ten
ovšem	ovšem	k9	ovšem
nelze	lze	k6eNd1	lze
dokázat	dokázat	k5eAaPmF	dokázat
ani	ani	k8xC	ani
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
že	že	k8xS	že
nejsou	být	k5eNaImIp3nP	být
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
regulativní	regulativní	k2eAgFnPc1d1	regulativní
ideje	idea	k1gFnPc1	idea
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
přesto	přesto	k6eAd1	přesto
nezbytné	nezbytný	k2eAgNnSc1d1	nezbytný
<g/>
.	.	kIx.	.
</s>
<s>
Myšlenky	myšlenka	k1gFnPc1	myšlenka
Kritiky	kritika	k1gFnSc2	kritika
čistého	čistý	k2eAgInSc2d1	čistý
rozumu	rozum	k1gInSc2	rozum
byly	být	k5eAaImAgFnP	být
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
tak	tak	k6eAd1	tak
nové	nový	k2eAgNnSc1d1	nové
<g/>
,	,	kIx,	,
že	že	k8xS	že
vyvolaly	vyvolat	k5eAaPmAgFnP	vyvolat
řadu	řada	k1gFnSc4	řada
nedorozumění	nedorozumění	k1gNnSc2	nedorozumění
i	i	k8xC	i
falešných	falešný	k2eAgFnPc2d1	falešná
podezření	podezřeň	k1gFnPc2	podezřeň
<g/>
.	.	kIx.	.
</s>
<s>
Kant	Kant	k1gMnSc1	Kant
proto	proto	k8xC	proto
vydal	vydat	k5eAaPmAgMnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1783	[number]	k4	1783
svá	svůj	k3xOyFgNnPc4	svůj
Prolegomena	prolegomena	k1gNnPc4	prolegomena
ke	k	k7c3	k
každé	každý	k3xTgFnSc6	každý
příští	příští	k2eAgFnSc6d1	příští
metafysice	metafysika	k1gFnSc6	metafysika
jako	jako	k9	jako
stručný	stručný	k2eAgInSc4d1	stručný
úvod	úvod	k1gInSc4	úvod
do	do	k7c2	do
transcendentální	transcendentální	k2eAgFnSc2d1	transcendentální
filosofie	filosofie	k1gFnSc2	filosofie
a	a	k8xC	a
Kritiku	kritika	k1gFnSc4	kritika
pro	pro	k7c4	pro
druhé	druhý	k4xOgNnSc4	druhý
vydání	vydání	k1gNnSc4	vydání
(	(	kIx(	(
<g/>
1787	[number]	k4	1787
<g/>
)	)	kIx)	)
podstatně	podstatně	k6eAd1	podstatně
přepracoval	přepracovat	k5eAaPmAgInS	přepracovat
a	a	k8xC	a
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
<g/>
.	.	kIx.	.
</s>
<s>
Svoji	svůj	k3xOyFgFnSc4	svůj
etiku	etika	k1gFnSc4	etika
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
v	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
Kritiky	kritika	k1gFnSc2	kritika
čistého	čistý	k2eAgInSc2d1	čistý
rozumu	rozum	k1gInSc2	rozum
pouze	pouze	k6eAd1	pouze
naznačil	naznačit	k5eAaPmAgMnS	naznačit
<g/>
,	,	kIx,	,
rozvedl	rozvést	k5eAaPmAgMnS	rozvést
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
dalších	další	k2eAgInPc6d1	další
spisech	spis	k1gInPc6	spis
<g/>
.	.	kIx.	.
</s>
<s>
Kantův	Kantův	k2eAgInSc1d1	Kantův
význam	význam	k1gInSc1	význam
pro	pro	k7c4	pro
filosofickou	filosofický	k2eAgFnSc4d1	filosofická
etiku	etika	k1gFnSc4	etika
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
ještě	ještě	k6eAd1	ještě
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
pro	pro	k7c4	pro
teorii	teorie	k1gFnSc4	teorie
poznání	poznání	k1gNnSc2	poznání
<g/>
.	.	kIx.	.
</s>
<s>
Pokusil	pokusit	k5eAaPmAgMnS	pokusit
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
vybudovat	vybudovat	k5eAaPmF	vybudovat
racionální	racionální	k2eAgInSc4d1	racionální
etický	etický	k2eAgInSc4d1	etický
systém	systém	k1gInSc4	systém
<g/>
,	,	kIx,	,
založený	založený	k2eAgInSc4d1	založený
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
předpokladech	předpoklad	k1gInPc6	předpoklad
lidské	lidský	k2eAgFnSc2d1	lidská
svobody	svoboda	k1gFnSc2	svoboda
a	a	k8xC	a
rovnosti	rovnost	k1gFnSc2	rovnost
a	a	k8xC	a
nezávislý	závislý	k2eNgMnSc1d1	nezávislý
na	na	k7c4	na
jakékoli	jakýkoli	k3yIgFnPc4	jakýkoli
nahodilé	nahodilý	k2eAgFnPc4d1	nahodilá
zkušenosti	zkušenost	k1gFnPc4	zkušenost
<g/>
.	.	kIx.	.
</s>
<s>
Morálka	morálka	k1gFnSc1	morálka
podle	podle	k7c2	podle
Kanta	Kant	k1gMnSc2	Kant
je	být	k5eAaImIp3nS	být
buď	buď	k8xC	buď
heteronomní	heteronomní	k2eAgMnSc1d1	heteronomní
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
předepsaná	předepsaný	k2eAgFnSc1d1	předepsaná
zvenčí	zvenčí	k6eAd1	zvenčí
někým	někdo	k3yInSc7	někdo
jiným	jiný	k2eAgNnSc7d1	jiné
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
autonomní	autonomní	k2eAgFnSc4d1	autonomní
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
si	se	k3xPyFc3	se
svobodný	svobodný	k2eAgInSc4d1	svobodný
subjekt	subjekt	k1gInSc4	subjekt
dává	dávat	k5eAaImIp3nS	dávat
sám	sám	k3xTgMnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
ovšem	ovšem	k9	ovšem
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
obecný	obecný	k2eAgInSc1d1	obecný
rozumový	rozumový	k2eAgInSc1d1	rozumový
princip	princip	k1gInSc1	princip
čili	čili	k8xC	čili
"	"	kIx"	"
<g/>
maximu	maxima	k1gFnSc4	maxima
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
"	"	kIx"	"
<g/>
nutným	nutný	k2eAgInSc7d1	nutný
postulátem	postulát	k1gInSc7	postulát
praktického	praktický	k2eAgInSc2d1	praktický
rozumu	rozum	k1gInSc2	rozum
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
Kanta	Kant	k1gMnSc4	Kant
kategorický	kategorický	k2eAgInSc1d1	kategorický
imperativ	imperativ	k1gInSc1	imperativ
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1785	[number]	k4	1785
vydal	vydat	k5eAaPmAgInS	vydat
stručné	stručný	k2eAgInPc4d1	stručný
Základy	základ	k1gInPc4	základ
metafysiky	metafysika	k1gFnSc2	metafysika
mravů	mrav	k1gInPc2	mrav
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nejprve	nejprve	k6eAd1	nejprve
odmítá	odmítat	k5eAaImIp3nS	odmítat
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
posláním	poslání	k1gNnSc7	poslání
člověka	člověk	k1gMnSc2	člověk
bylo	být	k5eAaImAgNnS	být
dosahovat	dosahovat	k5eAaImF	dosahovat
blaženosti	blaženost	k1gFnPc4	blaženost
(	(	kIx(	(
<g/>
hedonismus	hedonismus	k1gInSc4	hedonismus
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
kdyby	kdyby	kYmCp3nS	kdyby
příroda	příroda	k1gFnSc1	příroda
určila	určit	k5eAaPmAgFnS	určit
člověka	člověk	k1gMnSc4	člověk
k	k	k7c3	k
blaženosti	blaženost	k1gFnSc3	blaženost
<g/>
,	,	kIx,	,
nebyla	být	k5eNaImAgFnS	být
by	by	kYmCp3nS	by
jej	on	k3xPp3gMnSc4	on
vybavila	vybavit	k5eAaPmAgFnS	vybavit
rozumem	rozum	k1gInSc7	rozum
<g/>
.	.	kIx.	.
</s>
<s>
Jednání	jednání	k1gNnSc1	jednání
člověka	člověk	k1gMnSc2	člověk
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
řídí	řídit	k5eAaImIp3nS	řídit
jeho	jeho	k3xOp3gFnPc7	jeho
náklonnostmi	náklonnost	k1gFnPc7	náklonnost
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
ostatně	ostatně	k6eAd1	ostatně
neliší	lišit	k5eNaImIp3nP	lišit
od	od	k7c2	od
jiných	jiný	k2eAgMnPc2d1	jiný
živočichů	živočich	k1gMnPc2	živočich
a	a	k8xC	a
nezaslouží	zasloužit	k5eNaPmIp3nS	zasloužit
si	se	k3xPyFc3	se
za	za	k7c4	za
to	ten	k3xDgNnSc1	ten
žádnou	žádný	k3yNgFnSc4	žádný
chválu	chvála	k1gFnSc4	chvála
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
dobré	dobrý	k2eAgNnSc1d1	dobré
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
rozhodování	rozhodování	k1gNnSc1	rozhodování
a	a	k8xC	a
jednání	jednání	k1gNnSc1	jednání
z	z	k7c2	z
povinnosti	povinnost	k1gFnSc2	povinnost
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
nutnost	nutnost	k1gFnSc1	nutnost
jednání	jednání	k1gNnSc2	jednání
z	z	k7c2	z
úcty	úcta	k1gFnSc2	úcta
k	k	k7c3	k
zákonu	zákon	k1gInSc3	zákon
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Mravní	mravní	k2eAgNnSc1d1	mravní
jednání	jednání	k1gNnSc1	jednání
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
jednání	jednání	k1gNnSc4	jednání
z	z	k7c2	z
povinnosti	povinnost	k1gFnSc2	povinnost
čili	čili	k8xC	čili
na	na	k7c6	na
základě	základ	k1gInSc6	základ
imperativů	imperativ	k1gInPc2	imperativ
<g/>
.	.	kIx.	.
</s>
<s>
Kant	Kant	k1gMnSc1	Kant
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
imperativy	imperativ	k1gInPc4	imperativ
hypotetické	hypotetický	k2eAgInPc4d1	hypotetický
<g/>
,	,	kIx,	,
zdůvodněné	zdůvodněný	k2eAgInPc4d1	zdůvodněný
něčím	něco	k3yInSc7	něco
jiným	jiný	k2eAgNnSc7d1	jiné
<g/>
,	,	kIx,	,
nějakou	nějaký	k3yIgFnSc7	nějaký
potřebou	potřeba	k1gFnSc7	potřeba
nebo	nebo	k8xC	nebo
cílem	cíl	k1gInSc7	cíl
<g/>
,	,	kIx,	,
a	a	k8xC	a
imperativ	imperativ	k1gInSc4	imperativ
kategorický	kategorický	k2eAgInSc4d1	kategorický
či	či	k8xC	či
nepodmíněný	podmíněný	k2eNgInSc4d1	nepodmíněný
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
jeden	jeden	k4xCgInSc4	jeden
a	a	k8xC	a
platí	platit	k5eAaImIp3nS	platit
nutně	nutně	k6eAd1	nutně
pro	pro	k7c4	pro
každou	každý	k3xTgFnSc4	každý
rozumovou	rozumový	k2eAgFnSc4d1	rozumová
a	a	k8xC	a
svobodnou	svobodný	k2eAgFnSc4d1	svobodná
bytost	bytost	k1gFnSc4	bytost
<g/>
.	.	kIx.	.
</s>
<s>
Opírá	opírat	k5eAaImIp3nS	opírat
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
postuláty	postulát	k1gInPc4	postulát
svobody	svoboda	k1gFnSc2	svoboda
a	a	k8xC	a
rovnosti	rovnost	k1gFnSc2	rovnost
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
rozumová	rozumový	k2eAgFnSc1d1	rozumová
bytost	bytost	k1gFnSc1	bytost
uznat	uznat	k5eAaPmF	uznat
musí	muset	k5eAaImIp3nS	muset
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jinak	jinak	k6eAd1	jinak
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
octla	octnout	k5eAaPmAgFnS	octnout
sama	sám	k3xTgFnSc1	sám
se	s	k7c7	s
sebou	se	k3xPyFc7	se
ve	v	k7c6	v
sporu	spor	k1gInSc6	spor
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
Kant	Kant	k1gMnSc1	Kant
formuluje	formulovat	k5eAaImIp3nS	formulovat
kategorický	kategorický	k2eAgInSc4d1	kategorický
imperativ	imperativ	k1gInSc4	imperativ
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jednej	jednat	k5eAaImRp2nS	jednat
tak	tak	k9	tak
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
maxima	maximum	k1gNnSc2	maximum
tvého	tvůj	k3xOp2gNnSc2	tvůj
jednání	jednání	k1gNnSc2	jednání
měla	mít	k5eAaImAgNnP	mít
na	na	k7c6	na
základě	základ	k1gInSc6	základ
tvé	tvůj	k3xOp2gFnSc2	tvůj
vůle	vůle	k1gFnSc2	vůle
stát	stát	k5eAaPmF	stát
obecným	obecný	k2eAgInSc7d1	obecný
přírodním	přírodní	k2eAgInSc7d1	přírodní
zákonem	zákon	k1gInSc7	zákon
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
pak	pak	k6eAd1	pak
odvodí	odvodit	k5eAaPmIp3nS	odvodit
i	i	k9	i
druhou	druhý	k4xOgFnSc4	druhý
formulaci	formulace	k1gFnSc4	formulace
kategorického	kategorický	k2eAgInSc2d1	kategorický
imperativu	imperativ	k1gInSc2	imperativ
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jednej	jednat	k5eAaImRp2nS	jednat
tak	tak	k9	tak
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
abys	aby	kYmCp2nS	aby
používal	používat	k5eAaImAgMnS	používat
lidství	lidství	k1gNnSc4	lidství
jak	jak	k8xC	jak
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
osobě	osoba	k1gFnSc6	osoba
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
v	v	k7c6	v
osobě	osoba	k1gFnSc6	osoba
každého	každý	k3xTgMnSc2	každý
druhého	druhý	k4xOgMnSc2	druhý
vždy	vždy	k6eAd1	vždy
zároveň	zároveň	k6eAd1	zároveň
jako	jako	k8xC	jako
účel	účel	k1gInSc4	účel
a	a	k8xC	a
nikdy	nikdy	k6eAd1	nikdy
pouze	pouze	k6eAd1	pouze
jako	jako	k8xS	jako
prostředek	prostředek	k1gInSc4	prostředek
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Roku	rok	k1gInSc2	rok
1788	[number]	k4	1788
vyšla	vyjít	k5eAaPmAgFnS	vyjít
Kritika	kritika	k1gFnSc1	kritika
praktického	praktický	k2eAgInSc2d1	praktický
rozumu	rozum	k1gInSc2	rozum
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
myšlenky	myšlenka	k1gFnPc4	myšlenka
Základů	základ	k1gInPc2	základ
rozvádí	rozvádět	k5eAaImIp3nS	rozvádět
šířeji	šířej	k1gMnSc3	šířej
<g/>
,	,	kIx,	,
a	a	k8xC	a
ještě	ještě	k9	ještě
roku	rok	k1gInSc2	rok
1797	[number]	k4	1797
Metafysika	metafysika	k1gFnSc1	metafysika
mravů	mrav	k1gInPc2	mrav
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
hlavně	hlavně	k9	hlavně
Kantovu	Kantův	k2eAgFnSc4d1	Kantova
filosofii	filosofie	k1gFnSc4	filosofie
práva	právo	k1gNnSc2	právo
<g/>
;	;	kIx,	;
i	i	k8xC	i
právo	právo	k1gNnSc1	právo
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
pro	pro	k7c4	pro
Kanta	Kant	k1gMnSc4	Kant
vlastně	vlastně	k9	vlastně
součástí	součást	k1gFnSc7	součást
mravnosti	mravnost	k1gFnSc2	mravnost
<g/>
.	.	kIx.	.
</s>
<s>
Kantova	Kantův	k2eAgFnSc1d1	Kantova
etika	etika	k1gFnSc1	etika
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
lidské	lidský	k2eAgFnSc2d1	lidská
svobody	svoboda	k1gFnSc2	svoboda
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc7	který
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
obhájit	obhájit	k5eAaPmF	obhájit
proti	proti	k7c3	proti
Humovu	Humův	k2eAgInSc3d1	Humův
názoru	názor	k1gInSc3	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
lidské	lidský	k2eAgNnSc1d1	lidské
jednání	jednání	k1gNnSc1	jednání
je	být	k5eAaImIp3nS	být
determinováno	determinovat	k5eAaBmNgNnS	determinovat
příčinami	příčina	k1gFnPc7	příčina
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
pohyb	pohyb	k1gInSc1	pohyb
těles	těleso	k1gNnPc2	těleso
určen	určit	k5eAaPmNgInS	určit
přírodními	přírodní	k2eAgInPc7d1	přírodní
zákony	zákon	k1gInPc7	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Kant	Kant	k1gMnSc1	Kant
proto	proto	k8xC	proto
dělí	dělit	k5eAaImIp3nS	dělit
skutečnost	skutečnost	k1gFnSc1	skutečnost
na	na	k7c4	na
"	"	kIx"	"
<g/>
říši	říše	k1gFnSc4	říše
přírody	příroda	k1gFnSc2	příroda
<g/>
"	"	kIx"	"
čili	čili	k8xC	čili
nutnosti	nutnost	k1gFnSc2	nutnost
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
platí	platit	k5eAaImIp3nS	platit
přírodní	přírodní	k2eAgInPc4d1	přírodní
zákony	zákon	k1gInPc4	zákon
<g/>
,	,	kIx,	,
a	a	k8xC	a
"	"	kIx"	"
<g/>
říši	říše	k1gFnSc4	říše
svobody	svoboda	k1gFnSc2	svoboda
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
platí	platit	k5eAaImIp3nS	platit
rozumově	rozumově	k6eAd1	rozumově
zdůvodněné	zdůvodněný	k2eAgInPc4d1	zdůvodněný
zákony	zákon	k1gInPc4	zákon
mravnosti	mravnost	k1gFnSc2	mravnost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Aristotelovy	Aristotelův	k2eAgFnSc2d1	Aristotelova
etiky	etika	k1gFnSc2	etika
ctností	ctnost	k1gFnPc2	ctnost
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
Kantova	Kantův	k2eAgFnSc1d1	Kantova
etika	etika	k1gFnSc1	etika
etikou	etika	k1gFnSc7	etika
povinností	povinnost	k1gFnSc7	povinnost
a	a	k8xC	a
racionalizovanou	racionalizovaný	k2eAgFnSc7d1	racionalizovaná
verzí	verze	k1gFnSc7	verze
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
morálky	morálka	k1gFnSc2	morálka
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
ztratila	ztratit	k5eAaPmAgFnS	ztratit
pozitivní	pozitivní	k2eAgFnSc1d1	pozitivní
motivace	motivace	k1gFnSc1	motivace
jednání	jednání	k1gNnSc2	jednání
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
Kanta	Kant	k1gMnSc2	Kant
pouze	pouze	k6eAd1	pouze
povinnost	povinnost	k1gFnSc4	povinnost
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
oprostit	oprostit	k5eAaPmF	oprostit
mravní	mravní	k2eAgNnSc4d1	mravní
rozhodování	rozhodování	k1gNnSc4	rozhodování
ode	ode	k7c2	ode
všeho	všecek	k3xTgNnSc2	všecek
zkušenostního	zkušenostní	k2eAgNnSc2d1	zkušenostní
a	a	k8xC	a
nahodilého	nahodilý	k2eAgNnSc2d1	nahodilé
a	a	k8xC	a
učinit	učinit	k5eAaPmF	učinit
mravnost	mravnost	k1gFnSc4	mravnost
součástí	součást	k1gFnPc2	součást
rozumu	rozum	k1gInSc2	rozum
ovšem	ovšem	k9	ovšem
Kant	Kant	k1gMnSc1	Kant
omezuje	omezovat	k5eAaImIp3nS	omezovat
mravnost	mravnost	k1gFnSc4	mravnost
na	na	k7c4	na
rozhodování	rozhodování	k1gNnSc4	rozhodování
samo	sám	k3xTgNnSc1	sám
a	a	k8xC	a
opakovaně	opakovaně	k6eAd1	opakovaně
zdůrazňuje	zdůrazňovat	k5eAaImIp3nS	zdůrazňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
dobrém	dobrý	k2eAgNnSc6d1	dobré
rozhodnutí	rozhodnutí	k1gNnSc6	rozhodnutí
nic	nic	k3yNnSc1	nic
nemění	měnit	k5eNaImIp3nS	měnit
<g/>
,	,	kIx,	,
i	i	k8xC	i
kdyby	kdyby	kYmCp3nS	kdyby
mělo	mít	k5eAaImAgNnS	mít
velmi	velmi	k6eAd1	velmi
špatné	špatný	k2eAgInPc1d1	špatný
následky	následek	k1gInPc1	následek
<g/>
:	:	kIx,	:
ty	ty	k3xPp2nSc1	ty
se	se	k3xPyFc4	se
hodnocení	hodnocení	k1gNnSc1	hodnocení
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
už	už	k6eAd1	už
netýkají	týkat	k5eNaImIp3nP	týkat
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
slabinu	slabina	k1gFnSc4	slabina
Kantovy	Kantův	k2eAgFnSc2d1	Kantova
"	"	kIx"	"
<g/>
etiky	etika	k1gFnSc2	etika
smýšlení	smýšlení	k1gNnSc2	smýšlení
<g/>
"	"	kIx"	"
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
Max	Max	k1gMnSc1	Max
Weber	Weber	k1gMnSc1	Weber
ve	v	k7c6	v
slavné	slavný	k2eAgFnSc6d1	slavná
přednášce	přednáška	k1gFnSc6	přednáška
"	"	kIx"	"
<g/>
Politika	politika	k1gFnSc1	politika
jako	jako	k8xS	jako
povolání	povolání	k1gNnSc1	povolání
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
je	být	k5eAaImIp3nS	být
vybaven	vybavit	k5eAaPmNgInS	vybavit
rozumem	rozum	k1gInSc7	rozum
a	a	k8xC	a
určen	určit	k5eAaPmNgInS	určit
ke	k	k7c3	k
svobodě	svoboda	k1gFnSc3	svoboda
<g/>
;	;	kIx,	;
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
jeho	jeho	k3xOp3gFnSc1	jeho
chyba	chyba	k1gFnSc1	chyba
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
nežije	žít	k5eNaImIp3nS	žít
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
Kant	Kant	k1gMnSc1	Kant
rozumí	rozumět	k5eAaImIp3nS	rozumět
osvícenství	osvícenství	k1gNnSc4	osvícenství
(	(	kIx(	(
<g/>
Aufklärung	Aufklärung	k1gInSc1	Aufklärung
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
vyjití	vyjití	k1gNnSc1	vyjití
člověka	člověk	k1gMnSc2	člověk
ze	z	k7c2	z
zaviněné	zaviněný	k2eAgFnSc2d1	zaviněná
nedospělosti	nedospělost	k1gFnSc2	nedospělost
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
své	svůj	k3xOyFgFnSc2	svůj
svobody	svoboda	k1gFnSc2	svoboda
bál	bát	k5eAaImAgMnS	bát
a	a	k8xC	a
nechával	nechávat	k5eAaImAgMnS	nechávat
se	se	k3xPyFc4	se
vést	vést	k5eAaImF	vést
jinými	jiný	k2eAgNnPc7d1	jiné
<g/>
.	.	kIx.	.
</s>
<s>
Charakterizuje	charakterizovat	k5eAaBmIp3nS	charakterizovat
je	on	k3xPp3gMnPc4	on
latinským	latinský	k2eAgFnPc3d1	Latinská
Sapere	Saper	k1gInSc5	Saper
aude	audus	k1gInSc5	audus
–	–	k?	–
odvaž	odvázat	k5eAaPmRp2nS	odvázat
se	se	k3xPyFc4	se
jednat	jednat	k5eAaImF	jednat
podle	podle	k7c2	podle
svého	svůj	k3xOyFgInSc2	svůj
rozumu	rozum	k1gInSc2	rozum
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgInPc4	tři
nezbytné	nezbytný	k2eAgInPc4d1	nezbytný
pilíře	pilíř	k1gInPc4	pilíř
svobodného	svobodný	k2eAgNnSc2d1	svobodné
–	–	k?	–
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
i	i	k9	i
odpovědného	odpovědný	k2eAgMnSc2d1	odpovědný
–	–	k?	–
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
totiž	totiž	k9	totiž
svobodu	svoboda	k1gFnSc4	svoboda
<g/>
,	,	kIx,	,
nesmrtelnou	smrtelný	k2eNgFnSc4d1	nesmrtelná
duši	duše	k1gFnSc4	duše
a	a	k8xC	a
Boha	bůh	k1gMnSc4	bůh
<g/>
,	,	kIx,	,
nemůže	moct	k5eNaImIp3nS	moct
sice	sice	k8xC	sice
čistý	čistý	k2eAgInSc4d1	čistý
rozum	rozum	k1gInSc4	rozum
dokázat	dokázat	k5eAaPmF	dokázat
<g/>
,	,	kIx,	,
nemůže	moct	k5eNaImIp3nS	moct
je	být	k5eAaImIp3nS	být
však	však	k9	však
ani	ani	k9	ani
vyvrátit	vyvrátit	k5eAaPmF	vyvrátit
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
vzniká	vznikat	k5eAaImIp3nS	vznikat
prostor	prostor	k1gInSc4	prostor
pro	pro	k7c4	pro
víru	víra	k1gFnSc4	víra
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	on	k3xPp3gInPc4	on
přijímá	přijímat	k5eAaImIp3nS	přijímat
jako	jako	k9	jako
prakticky	prakticky	k6eAd1	prakticky
nezbytné	nezbytný	k2eAgNnSc1d1	nezbytný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
smyslu	smysl	k1gInSc6	smysl
říká	říkat	k5eAaImIp3nS	říkat
Kant	Kant	k1gMnSc1	Kant
<g/>
,	,	kIx,	,
že	že	k8xS	že
musel	muset	k5eAaImAgMnS	muset
"	"	kIx"	"
<g/>
zrušit	zrušit	k5eAaPmF	zrušit
vědění	vědění	k1gNnSc4	vědění
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
totiž	totiž	k9	totiž
spekulativní	spekulativní	k2eAgFnSc4d1	spekulativní
teologickou	teologický	k2eAgFnSc4d1	teologická
metafysiku	metafysika	k1gFnSc4	metafysika
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
"	"	kIx"	"
<g/>
udělal	udělat	k5eAaPmAgInS	udělat
místo	místo	k6eAd1	místo
pro	pro	k7c4	pro
víru	víra	k1gFnSc4	víra
<g/>
"	"	kIx"	"
–	–	k?	–
ostatně	ostatně	k6eAd1	ostatně
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
Martin	Martin	k1gMnSc1	Martin
Luther	Luthra	k1gFnPc2	Luthra
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
třetí	třetí	k4xOgFnSc6	třetí
z	z	k7c2	z
Kantových	Kantových	k2eAgFnPc2d1	Kantových
otázek	otázka	k1gFnPc2	otázka
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
v	v	k7c4	v
co	co	k3yQnSc4	co
mohu	moct	k5eAaImIp1nS	moct
doufat	doufat	k5eAaImF	doufat
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
náboženství	náboženství	k1gNnSc1	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Náboženství	náboženství	k1gNnSc1	náboženství
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
Kanta	Kant	k1gMnSc2	Kant
založeno	založit	k5eAaPmNgNnS	založit
na	na	k7c6	na
morálce	morálka	k1gFnSc6	morálka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
ně	on	k3xPp3gFnPc4	on
nutná	nutný	k2eAgFnSc1d1	nutná
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
všechna	všechen	k3xTgFnSc1	všechen
organizovaná	organizovaný	k2eAgFnSc1d1	organizovaná
a	a	k8xC	a
rituální	rituální	k2eAgNnSc1d1	rituální
jednání	jednání	k1gNnSc1	jednání
Kant	Kant	k1gMnSc1	Kant
ve	v	k7c6	v
spise	spis	k1gInSc6	spis
Náboženství	náboženství	k1gNnSc2	náboženství
v	v	k7c6	v
mezích	mez	k1gFnPc6	mez
čistého	čistý	k2eAgInSc2d1	čistý
rozumu	rozum	k1gInSc2	rozum
(	(	kIx(	(
<g/>
1793	[number]	k4	1793
<g/>
)	)	kIx)	)
odmítá	odmítat	k5eAaImIp3nS	odmítat
jako	jako	k9	jako
zbytečná	zbytečný	k2eAgFnSc1d1	zbytečná
<g/>
.	.	kIx.	.
</s>
<s>
Lidské	lidský	k2eAgFnPc1d1	lidská
dějiny	dějiny	k1gFnPc1	dějiny
nejsou	být	k5eNaImIp3nP	být
řízeny	řídit	k5eAaImNgFnP	řídit
nějakým	nějaký	k3yIgInSc7	nějaký
zákonem	zákon	k1gInSc7	zákon
a	a	k8xC	a
nelze	lze	k6eNd1	lze
v	v	k7c6	v
nich	on	k3xPp3gInPc6	on
najít	najít	k5eAaPmF	najít
přímé	přímý	k2eAgNnSc4d1	přímé
řízení	řízení	k1gNnSc4	řízení
Boží	božit	k5eAaImIp3nS	božit
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
však	však	k9	však
mají	mít	k5eAaImIp3nP	mít
svůj	svůj	k3xOyFgInSc4	svůj
cíl	cíl	k1gInSc4	cíl
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
je	být	k5eAaImIp3nS	být
prosazování	prosazování	k1gNnSc4	prosazování
rozumu	rozum	k1gInSc2	rozum
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
nakonec	nakonec	k6eAd1	nakonec
povede	vést	k5eAaImIp3nS	vést
i	i	k9	i
ke	k	k7c3	k
světovému	světový	k2eAgInSc3d1	světový
míru	mír	k1gInSc3	mír
<g/>
.	.	kIx.	.
</s>
<s>
Nedovedeme	dovést	k5eNaPmIp1nP	dovést
si	se	k3xPyFc3	se
sice	sice	k8xC	sice
představit	představit	k5eAaPmF	představit
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
mohlo	moct	k5eAaImAgNnS	moct
stát	stát	k5eAaPmF	stát
<g/>
,	,	kIx,	,
jsme	být	k5eAaImIp1nP	být
však	však	k9	však
zároveň	zároveň	k6eAd1	zároveň
povinni	povinen	k2eAgMnPc1d1	povinen
jednat	jednat	k5eAaImF	jednat
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
kdyby	kdyby	kYmCp3nS	kdyby
to	ten	k3xDgNnSc1	ten
možné	možný	k2eAgNnSc1d1	možné
bylo	být	k5eAaImAgNnS	být
<g/>
.	.	kIx.	.
</s>
<s>
Kritika	kritika	k1gFnSc1	kritika
soudnosti	soudnost	k1gFnSc2	soudnost
(	(	kIx(	(
<g/>
1790	[number]	k4	1790
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgFnSc1	třetí
z	z	k7c2	z
Kantových	Kantových	k2eAgFnPc2d1	Kantových
Kritik	kritika	k1gFnPc2	kritika
<g/>
,	,	kIx,	,
chce	chtít	k5eAaImIp3nS	chtít
jeho	jeho	k3xOp3gInSc4	jeho
filosofický	filosofický	k2eAgInSc4d1	filosofický
systém	systém	k1gInSc4	systém
doplnit	doplnit	k5eAaPmF	doplnit
a	a	k8xC	a
nějak	nějak	k6eAd1	nějak
překlenout	překlenout	k5eAaPmF	překlenout
rozdíl	rozdíl	k1gInSc4	rozdíl
mezi	mezi	k7c7	mezi
teoretickým	teoretický	k2eAgInSc7d1	teoretický
a	a	k8xC	a
praktickým	praktický	k2eAgInSc7d1	praktický
rozumem	rozum	k1gInSc7	rozum
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yInSc1	co
je	on	k3xPp3gMnPc4	on
spojuje	spojovat	k5eAaImIp3nS	spojovat
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
pocity	pocit	k1gInPc1	pocit
libosti	libost	k1gFnSc2	libost
a	a	k8xC	a
nelibosti	nelibost	k1gFnSc2	nelibost
<g/>
.	.	kIx.	.
</s>
<s>
Kant	Kant	k1gMnSc1	Kant
zde	zde	k6eAd1	zde
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c6	o
soudech	soud	k1gInPc6	soud
o	o	k7c6	o
krásném	krásný	k2eAgInSc6d1	krásný
a	a	k8xC	a
vznešeném	vznešený	k2eAgMnSc6d1	vznešený
a	a	k8xC	a
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
části	část	k1gFnSc6	část
o	o	k7c6	o
soudech	soud	k1gInPc6	soud
teleologických	teleologický	k2eAgInPc2d1	teleologický
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc4	jenž
určují	určovat	k5eAaImIp3nP	určovat
vztah	vztah	k1gInSc4	vztah
člověka	člověk	k1gMnSc2	člověk
k	k	k7c3	k
přírodě	příroda	k1gFnSc3	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Estetické	estetický	k2eAgNnSc1d1	estetické
vnímání	vnímání	k1gNnSc1	vnímání
je	být	k5eAaImIp3nS	být
subjektivní	subjektivní	k2eAgInSc4d1	subjektivní
poznávací	poznávací	k2eAgInSc4d1	poznávací
proces	proces	k1gInSc4	proces
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
soudnost	soudnost	k1gFnSc1	soudnost
nějakému	nějaký	k3yIgInSc3	nějaký
předmětu	předmět	k1gInSc3	předmět
přisuzuje	přisuzovat	k5eAaImIp3nS	přisuzovat
nebo	nebo	k8xC	nebo
upírá	upírat	k5eAaImIp3nS	upírat
predikát	predikát	k1gInSc1	predikát
"	"	kIx"	"
<g/>
krásný	krásný	k2eAgInSc1d1	krásný
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Úsudky	úsudek	k1gInPc1	úsudek
vkusu	vkus	k1gInSc2	vkus
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
není	být	k5eNaImIp3nS	být
přítomen	přítomen	k2eAgInSc1d1	přítomen
žádný	žádný	k3yNgInSc1	žádný
zájem	zájem	k1gInSc1	zájem
soudícího	soudící	k2eAgInSc2d1	soudící
<g/>
,	,	kIx,	,
nejsou	být	k5eNaImIp3nP	být
podřízeny	podřízen	k2eAgFnPc1d1	podřízena
žádnému	žádný	k3yNgInSc3	žádný
pojmu	pojem	k1gInSc3	pojem
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nárokují	nárokovat	k5eAaImIp3nP	nárokovat
si	se	k3xPyFc3	se
obecnou	obecný	k2eAgFnSc4d1	obecná
platnost	platnost	k1gFnSc4	platnost
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
nutné	nutný	k2eAgFnPc1d1	nutná
<g/>
.	.	kIx.	.
</s>
<s>
Krásné	krásný	k2eAgNnSc1d1	krásné
i	i	k8xC	i
vznešené	vznešený	k2eAgNnSc1d1	vznešené
se	se	k3xPyFc4	se
líbí	líbit	k5eAaImIp3nS	líbit
bezprostředně	bezprostředně	k6eAd1	bezprostředně
a	a	k8xC	a
samo	sám	k3xTgNnSc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
<g/>
,	,	kIx,	,
krásné	krásný	k2eAgNnSc1d1	krásné
však	však	k9	však
vzbuzuje	vzbuzovat	k5eAaImIp3nS	vzbuzovat
pocit	pocit	k1gInSc4	pocit
libosti	libost	k1gFnSc2	libost
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
vznešené	vznešený	k2eAgInPc1d1	vznešený
pocity	pocit	k1gInPc1	pocit
obdivu	obdiv	k1gInSc2	obdiv
a	a	k8xC	a
úcty	úcta	k1gFnSc2	úcta
<g/>
.	.	kIx.	.
</s>
<s>
Krásné	krásný	k2eAgNnSc1d1	krásné
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
líbí	líbit	k5eAaImIp3nS	líbit
v	v	k7c6	v
prostém	prostý	k2eAgNnSc6d1	prosté
posouzení	posouzení	k1gNnSc6	posouzení
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
plyne	plynout	k5eAaImIp3nS	plynout
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
líbit	líbit	k5eAaImF	líbit
bez	bez	k7c2	bez
jakéhokoli	jakýkoli	k3yIgInSc2	jakýkoli
zájmu	zájem	k1gInSc2	zájem
<g/>
.	.	kIx.	.
</s>
<s>
Vznešené	vznešený	k2eAgNnSc1d1	vznešené
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
bezprostředně	bezprostředně	k6eAd1	bezprostředně
líbí	líbit	k5eAaImIp3nS	líbit
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
klade	klást	k5eAaImIp3nS	klást
odpor	odpor	k1gInSc4	odpor
proti	proti	k7c3	proti
zájmu	zájem	k1gInSc3	zájem
smyslů	smysl	k1gInPc2	smysl
<g/>
.	.	kIx.	.
</s>
<s>
Teleologická	teleologický	k2eAgFnSc1d1	teleologická
soudnost	soudnost	k1gFnSc1	soudnost
si	se	k3xPyFc3	se
všímá	všímat	k5eAaImIp3nS	všímat
účelnosti	účelnost	k1gFnSc2	účelnost
přírody	příroda	k1gFnSc2	příroda
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ovšem	ovšem	k9	ovšem
není	být	k5eNaImIp3nS	být
vlastnost	vlastnost	k1gFnSc4	vlastnost
věcí	věc	k1gFnPc2	věc
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
regulativní	regulativní	k2eAgFnSc1d1	regulativní
idea	idea	k1gFnSc1	idea
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
do	do	k7c2	do
nich	on	k3xPp3gFnPc2	on
vkládáme	vkládat	k5eAaImIp1nP	vkládat
my	my	k3xPp1nPc1	my
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
stroje	stroj	k1gInSc2	stroj
se	se	k3xPyFc4	se
rostlina	rostlina	k1gFnSc1	rostlina
sama	sám	k3xTgMnSc4	sám
reprodukuje	reprodukovat	k5eAaBmIp3nS	reprodukovat
a	a	k8xC	a
člověk	člověk	k1gMnSc1	člověk
zde	zde	k6eAd1	zde
vidí	vidět	k5eAaImIp3nS	vidět
podivuhodnou	podivuhodný	k2eAgFnSc4d1	podivuhodná
souvislost	souvislost	k1gFnSc4	souvislost
<g/>
,	,	kIx,	,
neměl	mít	k5eNaImAgMnS	mít
by	by	kYmCp3nS	by
ji	on	k3xPp3gFnSc4	on
však	však	k9	však
přisuzovat	přisuzovat	k5eAaImF	přisuzovat
Božímu	boží	k2eAgNnSc3d1	boží
působení	působení	k1gNnSc3	působení
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
formulace	formulace	k1gFnSc1	formulace
Kantových	Kantových	k2eAgFnPc2d1	Kantových
tří	tři	k4xCgFnPc2	tři
otázek	otázka	k1gFnPc2	otázka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
pak	pak	k6eAd1	pak
v	v	k7c6	v
"	"	kIx"	"
<g/>
Logice	logika	k1gFnSc6	logika
<g/>
"	"	kIx"	"
shrnul	shrnout	k5eAaPmAgMnS	shrnout
v	v	k7c4	v
jedinou	jediný	k2eAgFnSc4d1	jediná
<g/>
:	:	kIx,	:
Co	co	k3yInSc1	co
je	být	k5eAaImIp3nS	být
člověk	člověk	k1gMnSc1	člověk
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
,	,	kIx,	,
jasně	jasně	k6eAd1	jasně
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
východisko	východisko	k1gNnSc1	východisko
Kantova	Kantův	k2eAgNnSc2d1	Kantovo
myšlení	myšlení	k1gNnSc2	myšlení
je	být	k5eAaImIp3nS	být
antropologické	antropologický	k2eAgNnSc1d1	antropologické
<g/>
,	,	kIx,	,
že	že	k8xS	že
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
člověka	člověk	k1gMnSc2	člověk
a	a	k8xC	a
míří	mířit	k5eAaImIp3nS	mířit
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rozvrhu	rozvrh	k1gInSc6	rozvrh
svých	svůj	k3xOyFgFnPc2	svůj
přednášek	přednáška	k1gFnPc2	přednáška
Kant	Kant	k1gMnSc1	Kant
začínajícím	začínající	k2eAgMnPc3d1	začínající
studentům	student	k1gMnPc3	student
přednášel	přednášet	k5eAaImAgInS	přednášet
nejprve	nejprve	k6eAd1	nejprve
semestr	semestr	k1gInSc1	semestr
přírodní	přírodní	k2eAgFnSc2d1	přírodní
vědy	věda	k1gFnSc2	věda
a	a	k8xC	a
pak	pak	k6eAd1	pak
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1773	[number]	k4	1773
ještě	ještě	k9	ještě
semestr	semestr	k1gInSc1	semestr
antropologie	antropologie	k1gFnSc2	antropologie
jako	jako	k8xC	jako
nauky	nauka	k1gFnSc2	nauka
o	o	k7c6	o
člověku	člověk	k1gMnSc6	člověk
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
pokládal	pokládat	k5eAaImAgMnS	pokládat
za	za	k7c4	za
nutný	nutný	k2eAgInSc4d1	nutný
předpoklad	předpoklad	k1gInSc4	předpoklad
pro	pro	k7c4	pro
studium	studium	k1gNnSc4	studium
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
.	.	kIx.	.
</s>
<s>
Obsah	obsah	k1gInSc1	obsah
této	tento	k3xDgFnSc2	tento
antropologie	antropologie	k1gFnSc2	antropologie
–	–	k?	–
či	či	k8xC	či
podle	podle	k7c2	podle
dnešní	dnešní	k2eAgFnSc2d1	dnešní
terminologie	terminologie	k1gFnSc2	terminologie
spíše	spíše	k9	spíše
psychologie	psychologie	k1gFnSc2	psychologie
a	a	k8xC	a
etnografie	etnografie	k1gFnSc2	etnografie
-	-	kIx~	-
však	však	k9	však
sepsal	sepsat	k5eAaPmAgMnS	sepsat
a	a	k8xC	a
vydal	vydat	k5eAaPmAgMnS	vydat
až	až	k6eAd1	až
na	na	k7c6	na
konci	konec	k1gInSc6	konec
života	život	k1gInSc2	život
jako	jako	k8xS	jako
své	svůj	k3xOyFgNnSc4	svůj
poslední	poslední	k2eAgNnSc4d1	poslední
dílo	dílo	k1gNnSc4	dílo
(	(	kIx(	(
<g/>
Antropologie	antropologie	k1gFnSc2	antropologie
v	v	k7c6	v
pragmatickém	pragmatický	k2eAgInSc6d1	pragmatický
ohledu	ohled	k1gInSc6	ohled
<g/>
,	,	kIx,	,
1798	[number]	k4	1798
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dělí	dělit	k5eAaImIp3nS	dělit
ji	on	k3xPp3gFnSc4	on
na	na	k7c4	na
fysiologickou	fysiologický	k2eAgFnSc4d1	fysiologická
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
podle	podle	k7c2	podle
něho	on	k3xPp3gMnSc2	on
mnoho	mnoho	k6eAd1	mnoho
nepřináší	přinášet	k5eNaImIp3nS	přinášet
<g/>
,	,	kIx,	,
a	a	k8xC	a
pragmatickou	pragmatický	k2eAgFnSc7d1	pragmatická
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
systematicky	systematicky	k6eAd1	systematicky
uspořádat	uspořádat	k5eAaPmF	uspořádat
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
o	o	k7c6	o
člověku	člověk	k1gMnSc6	člověk
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc6	jeho
jednání	jednání	k1gNnSc6	jednání
ze	z	k7c2	z
zkušenosti	zkušenost	k1gFnSc2	zkušenost
víme	vědět	k5eAaImIp1nP	vědět
<g/>
.	.	kIx.	.
</s>
<s>
Začíná	začínat	k5eAaImIp3nS	začínat
výkladem	výklad	k1gInSc7	výklad
o	o	k7c4	o
vědomí	vědomí	k1gNnSc4	vědomí
sebe	sebe	k3xPyFc4	sebe
<g/>
,	,	kIx,	,
o	o	k7c6	o
smyslech	smysl	k1gInPc6	smysl
a	a	k8xC	a
představivosti	představivost	k1gFnSc6	představivost
<g/>
,	,	kIx,	,
o	o	k7c6	o
různých	různý	k2eAgFnPc6d1	různá
formách	forma	k1gFnPc6	forma
libosti	libost	k1gFnSc2	libost
a	a	k8xC	a
nelibosti	nelibost	k1gFnSc2	nelibost
<g/>
,	,	kIx,	,
o	o	k7c6	o
afektivitě	afektivita	k1gFnSc6	afektivita
a	a	k8xC	a
vášních	vášeň	k1gFnPc6	vášeň
a	a	k8xC	a
dospívá	dospívat	k5eAaImIp3nS	dospívat
až	až	k9	až
k	k	k7c3	k
výkladu	výklad	k1gInSc3	výklad
povahy	povaha	k1gFnSc2	povaha
a	a	k8xC	a
charakteru	charakter	k1gInSc2	charakter
<g/>
,	,	kIx,	,
národů	národ	k1gInPc2	národ
a	a	k8xC	a
ras	rasa	k1gFnPc2	rasa
<g/>
.	.	kIx.	.
</s>
<s>
Rasy	rasa	k1gFnPc1	rasa
jsou	být	k5eAaImIp3nP	být
čtyři	čtyři	k4xCgNnPc4	čtyři
<g/>
,	,	kIx,	,
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
z	z	k7c2	z
rozdílných	rozdílný	k2eAgNnPc2d1	rozdílné
podnebí	podnebí	k1gNnPc2	podnebí
a	a	k8xC	a
od	od	k7c2	od
bílé	bílý	k2eAgFnSc2d1	bílá
přes	přes	k7c4	přes
žlutou	žlutý	k2eAgFnSc4d1	žlutá
<g/>
,	,	kIx,	,
černou	černý	k2eAgFnSc4d1	černá
až	až	k8xS	až
po	po	k7c4	po
"	"	kIx"	"
<g/>
měděnou	měděný	k2eAgFnSc4d1	měděná
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
stále	stále	k6eAd1	stále
slabšími	slabý	k2eAgFnPc7d2	slabší
rozumovými	rozumový	k2eAgFnPc7d1	rozumová
schopnostmi	schopnost	k1gFnPc7	schopnost
<g/>
.	.	kIx.	.
</s>
<s>
Spisy	spis	k1gInPc1	spis
o	o	k7c6	o
logice	logika	k1gFnSc6	logika
<g/>
,	,	kIx,	,
o	o	k7c6	o
fyzické	fyzický	k2eAgFnSc6d1	fyzická
geografii	geografie	k1gFnSc6	geografie
a	a	k8xC	a
o	o	k7c6	o
pedagogice	pedagogika	k1gFnSc6	pedagogika
vydali	vydat	k5eAaPmAgMnP	vydat
podle	podle	k7c2	podle
Kantových	Kantových	k2eAgFnPc2d1	Kantových
přednášek	přednáška	k1gFnPc2	přednáška
až	až	k8xS	až
jeho	jeho	k3xOp3gMnPc1	jeho
žáci	žák	k1gMnPc1	žák
<g/>
.	.	kIx.	.
</s>
<s>
Kantovo	Kantův	k2eAgNnSc1d1	Kantovo
obtížné	obtížný	k2eAgNnSc1d1	obtížné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hluboké	hluboký	k2eAgFnPc1d1	hluboká
a	a	k8xC	a
důkladně	důkladně	k6eAd1	důkladně
zpracované	zpracovaný	k2eAgNnSc4d1	zpracované
dílo	dílo	k1gNnSc4	dílo
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
kritická	kritický	k2eAgFnSc1d1	kritická
poctivost	poctivost	k1gFnSc1	poctivost
i	i	k8xC	i
odvaha	odvaha	k1gFnSc1	odvaha
mu	on	k3xPp3gMnSc3	on
získaly	získat	k5eAaPmAgFnP	získat
už	už	k9	už
za	za	k7c2	za
života	život	k1gInSc2	život
obecné	obecný	k2eAgNnSc4d1	obecné
uznání	uznání	k1gNnSc4	uznání
a	a	k8xC	a
úctu	úcta	k1gFnSc4	úcta
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
jeho	jeho	k3xOp3gMnPc2	jeho
přímých	přímý	k2eAgMnPc2d1	přímý
žáků	žák	k1gMnPc2	žák
ho	on	k3xPp3gMnSc4	on
obdivoval	obdivovat	k5eAaImAgMnS	obdivovat
třeba	třeba	k6eAd1	třeba
Friedrich	Friedrich	k1gMnSc1	Friedrich
Schiller	Schiller	k1gMnSc1	Schiller
a	a	k8xC	a
mnoho	mnoho	k4c4	mnoho
dalších	další	k2eAgFnPc2d1	další
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
ale	ale	k9	ale
ozvali	ozvat	k5eAaPmAgMnP	ozvat
i	i	k9	i
kritikové	kritik	k1gMnPc1	kritik
<g/>
.	.	kIx.	.
</s>
<s>
Moses	Moses	k1gMnSc1	Moses
Mendelssohn	Mendelssohn	k1gMnSc1	Mendelssohn
mu	on	k3xPp3gMnSc3	on
vytýkal	vytýkat	k5eAaImAgMnS	vytýkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
všechno	všechen	k3xTgNnSc1	všechen
boří	bořit	k5eAaImIp3nS	bořit
<g/>
,	,	kIx,	,
Johann	Johann	k1gMnSc1	Johann
Georg	Georg	k1gMnSc1	Georg
Hamann	Hamann	k1gMnSc1	Hamann
a	a	k8xC	a
Johann	Johann	k1gMnSc1	Johann
Gottfried	Gottfried	k1gMnSc1	Gottfried
Herder	Herder	k1gMnSc1	Herder
<g/>
,	,	kIx,	,
že	že	k8xS	že
přehlíží	přehlížet	k5eAaImIp3nS	přehlížet
význam	význam	k1gInSc4	význam
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
s	s	k7c7	s
úctou	úcta	k1gFnSc7	úcta
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kriticky	kriticky	k6eAd1	kriticky
se	se	k3xPyFc4	se
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
vymezuje	vymezovat	k5eAaImIp3nS	vymezovat
jeho	jeho	k3xOp3gMnSc1	jeho
žák	žák	k1gMnSc1	žák
Fichte	Ficht	k1gInSc5	Ficht
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
tak	tak	k6eAd1	tak
dospěl	dochvít	k5eAaPmAgMnS	dochvít
ke	k	k7c3	k
svému	svůj	k3xOyFgInSc3	svůj
idealismu	idealismus	k1gInSc3	idealismus
<g/>
.	.	kIx.	.
</s>
<s>
Schelling	Schelling	k1gInSc1	Schelling
i	i	k8xC	i
Hegel	Hegel	k1gInSc1	Hegel
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
Kantův	Kantův	k2eAgInSc4d1	Kantův
systém	systém	k1gInSc4	systém
překonat	překonat	k5eAaPmF	překonat
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
ovšem	ovšem	k9	ovšem
také	také	k9	také
poměřovali	poměřovat	k5eAaImAgMnP	poměřovat
<g/>
.	.	kIx.	.
</s>
<s>
Arthur	Arthur	k1gMnSc1	Arthur
Schopenhauer	Schopenhauer	k1gMnSc1	Schopenhauer
sice	sice	k8xC	sice
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
Kantovo	Kantův	k2eAgNnSc4d1	Kantovo
rozlišení	rozlišení	k1gNnSc4	rozlišení
věci	věc	k1gFnSc2	věc
o	o	k7c4	o
sobě	se	k3xPyFc3	se
a	a	k8xC	a
věci	věc	k1gFnSc3	věc
jakožto	jakožto	k8xS	jakožto
jevu	jev	k1gInSc2	jev
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
filosofie	filosofie	k1gFnSc1	filosofie
se	se	k3xPyFc4	se
však	však	k9	však
dále	daleko	k6eAd2	daleko
ubírá	ubírat	k5eAaImIp3nS	ubírat
odlišným	odlišný	k2eAgInSc7d1	odlišný
směrem	směr	k1gInSc7	směr
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
kritici	kritik	k1gMnPc1	kritik
jako	jako	k9	jako
Friedrich	Friedrich	k1gMnSc1	Friedrich
Nietzsche	Nietzsche	k1gFnPc2	Nietzsche
jej	on	k3xPp3gMnSc4	on
odmítali	odmítat	k5eAaImAgMnP	odmítat
důkladně	důkladně	k6eAd1	důkladně
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
se	se	k3xPyFc4	se
nevyhnuli	vyhnout	k5eNaPmAgMnP	vyhnout
konfrontaci	konfrontace	k1gFnSc4	konfrontace
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Odmítavý	odmítavý	k2eAgInSc4d1	odmítavý
postoj	postoj	k1gInSc4	postoj
ke	k	k7c3	k
Kantově	Kantův	k2eAgFnSc3d1	Kantova
filozofii	filozofie	k1gFnSc3	filozofie
zaujímali	zaujímat	k5eAaImAgMnP	zaujímat
katoličtí	katolický	k2eAgMnPc1d1	katolický
filozofové	filozof	k1gMnPc1	filozof
tomistické	tomistický	k2eAgFnSc2d1	tomistická
orientace	orientace	k1gFnSc2	orientace
<g/>
.	.	kIx.	.
</s>
<s>
Katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
dekretem	dekret	k1gInSc7	dekret
ze	z	k7c2	z
dne	den	k1gInSc2	den
11	[number]	k4	11
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1827	[number]	k4	1827
zařadila	zařadit	k5eAaPmAgFnS	zařadit
Kantovu	Kantův	k2eAgFnSc4d1	Kantova
Kritiku	kritika	k1gFnSc4	kritika
čistého	čistý	k2eAgInSc2d1	čistý
rozumu	rozum	k1gInSc2	rozum
na	na	k7c4	na
Index	index	k1gInSc4	index
zakázaných	zakázaný	k2eAgFnPc2d1	zakázaná
knih	kniha	k1gFnPc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
zejména	zejména	k9	zejména
němečtí	německý	k2eAgMnPc1d1	německý
filosofové	filosof	k1gMnPc1	filosof
Kanta	Kant	k1gMnSc4	Kant
znovu	znovu	k6eAd1	znovu
objevují	objevovat	k5eAaImIp3nP	objevovat
a	a	k8xC	a
navazují	navazovat	k5eAaImIp3nP	navazovat
na	na	k7c4	na
něho	on	k3xPp3gMnSc4	on
dvě	dva	k4xCgFnPc4	dva
novokantovské	novokantovský	k2eAgFnPc4d1	novokantovský
školy	škola	k1gFnPc4	škola
<g/>
:	:	kIx,	:
spíše	spíše	k9	spíše
vědecky	vědecky	k6eAd1	vědecky
orientovaná	orientovaný	k2eAgFnSc1d1	orientovaná
"	"	kIx"	"
<g/>
marburská	marburský	k2eAgFnSc1d1	marburská
škola	škola	k1gFnSc1	škola
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jejímiž	jejíž	k3xOyRp3gMnPc7	jejíž
hlavními	hlavní	k2eAgMnPc7d1	hlavní
představiteli	představitel	k1gMnPc7	představitel
byli	být	k5eAaImAgMnP	být
Hermann	Hermann	k1gMnSc1	Hermann
Cohen	Cohen	k1gInSc4	Cohen
<g/>
,	,	kIx,	,
Paul	Paul	k1gMnSc1	Paul
Natorp	Natorp	k1gMnSc1	Natorp
nebo	nebo	k8xC	nebo
Ernst	Ernst	k1gMnSc1	Ernst
Cassirer	Cassirer	k1gMnSc1	Cassirer
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
škola	škola	k1gFnSc1	škola
bádenská	bádenský	k2eAgFnSc1d1	bádenská
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
orientovaná	orientovaný	k2eAgFnSc1d1	orientovaná
hlavně	hlavně	k9	hlavně
na	na	k7c4	na
filosofii	filosofie	k1gFnSc4	filosofie
hodnot	hodnota	k1gFnPc2	hodnota
a	a	k8xC	a
dějiny	dějiny	k1gFnPc4	dějiny
<g/>
,	,	kIx,	,
jejímiž	jejíž	k3xOyRp3gMnPc7	jejíž
hlavními	hlavní	k2eAgMnPc7d1	hlavní
představiteli	představitel	k1gMnPc7	představitel
byli	být	k5eAaImAgMnP	být
Wilhelm	Wilhelm	k1gInSc4	Wilhelm
Windelband	Windelbanda	k1gFnPc2	Windelbanda
a	a	k8xC	a
Heinrich	Heinrich	k1gMnSc1	Heinrich
Rickert	Rickert	k1gMnSc1	Rickert
<g/>
.	.	kIx.	.
</s>
<s>
Novokantovství	Novokantovství	k1gNnSc1	Novokantovství
bylo	být	k5eAaImAgNnS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgInPc2d1	hlavní
proudů	proud	k1gInPc2	proud
německé	německý	k2eAgFnSc2d1	německá
filosofie	filosofie	k1gFnSc2	filosofie
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
vyšli	vyjít	k5eAaPmAgMnP	vyjít
z	z	k7c2	z
něho	on	k3xPp3gMnSc2	on
i	i	k9	i
lidé	člověk	k1gMnPc1	člověk
jako	jako	k8xS	jako
Max	Max	k1gMnSc1	Max
Weber	Weber	k1gMnSc1	Weber
<g/>
,	,	kIx,	,
Nicolai	Nicola	k1gMnPc1	Nicola
Hartmann	Hartmann	k1gMnSc1	Hartmann
a	a	k8xC	a
Franz	Franz	k1gMnSc1	Franz
Rosenzweig	Rosenzweig	k1gMnSc1	Rosenzweig
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
s	s	k7c7	s
Kantem	Kant	k1gMnSc7	Kant
kriticky	kriticky	k6eAd1	kriticky
vyrovnávali	vyrovnávat	k5eAaImAgMnP	vyrovnávat
četní	četný	k2eAgMnPc1d1	četný
významní	významný	k2eAgMnPc1d1	významný
filosofové	filosof	k1gMnPc1	filosof
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Georg	Georg	k1gMnSc1	Georg
Simmel	Simmel	k1gMnSc1	Simmel
<g/>
,	,	kIx,	,
C.	C.	kA	C.
S.	S.	kA	S.
Peirce	Peirce	k1gMnSc1	Peirce
<g/>
,	,	kIx,	,
Edmund	Edmund	k1gMnSc1	Edmund
Husserl	Husserl	k1gMnSc1	Husserl
<g/>
,	,	kIx,	,
Max	Max	k1gMnSc1	Max
Weber	Weber	k1gMnSc1	Weber
<g/>
,	,	kIx,	,
Max	Max	k1gMnSc1	Max
Scheler	Scheler	k1gMnSc1	Scheler
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
Heidegger	Heidegger	k1gMnSc1	Heidegger
<g/>
,	,	kIx,	,
Karl	Karl	k1gMnSc1	Karl
Jaspers	Jaspers	k1gInSc1	Jaspers
<g/>
,	,	kIx,	,
Hannah	Hannah	k1gInSc1	Hannah
Arendtová	Arendtová	k1gFnSc1	Arendtová
<g/>
,	,	kIx,	,
Theodor	Theodor	k1gMnSc1	Theodor
W.	W.	kA	W.
Adorno	Adorno	k1gNnSc1	Adorno
<g/>
,	,	kIx,	,
Michel	Michel	k1gMnSc1	Michel
Foucault	Foucault	k1gMnSc1	Foucault
nebo	nebo	k8xC	nebo
Karl	Karl	k1gMnSc1	Karl
Popper	Popper	k1gMnSc1	Popper
i	i	k8xC	i
logici	logik	k1gMnPc1	logik
jako	jako	k8xS	jako
W.	W.	kA	W.
V.	V.	kA	V.
Quine	Quin	k1gInSc5	Quin
nebo	nebo	k8xC	nebo
P.	P.	kA	P.
F.	F.	kA	F.
Strawson	Strawson	k1gMnSc1	Strawson
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
trvalý	trvalý	k2eAgInSc1d1	trvalý
vliv	vliv	k1gInSc1	vliv
má	mít	k5eAaImIp3nS	mít
však	však	k9	však
Kantovo	Kantův	k2eAgNnSc1d1	Kantovo
myšlení	myšlení	k1gNnSc1	myšlení
v	v	k7c6	v
etice	etika	k1gFnSc6	etika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
myšlenkových	myšlenkový	k2eAgFnPc2d1	myšlenková
linií	linie	k1gFnPc2	linie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Kantově	Kantův	k2eAgFnSc6d1	Kantova
etice	etika	k1gFnSc6	etika
budují	budovat	k5eAaImIp3nP	budovat
autoři	autor	k1gMnPc1	autor
jako	jako	k8xS	jako
O.	O.	kA	O.
Höffe	Höff	k1gInSc5	Höff
<g/>
,	,	kIx,	,
K.	K.	kA	K.
<g/>
-	-	kIx~	-
<g/>
O.	O.	kA	O.
Apel	apel	k1gInSc1	apel
<g/>
,	,	kIx,	,
E.	E.	kA	E.
Tugendhat	Tugendhat	k1gFnSc2	Tugendhat
a	a	k8xC	a
zejména	zejména	k9	zejména
John	John	k1gMnSc1	John
Rawls	Rawlsa	k1gFnPc2	Rawlsa
<g/>
.	.	kIx.	.
</s>
<s>
Kantovy	Kantův	k2eAgInPc1d1	Kantův
spisy	spis	k1gInPc1	spis
vyšly	vyjít	k5eAaPmAgInP	vyjít
a	a	k8xC	a
vycházejí	vycházet	k5eAaImIp3nP	vycházet
ve	v	k7c6	v
stovkách	stovka	k1gFnPc6	stovka
různých	různý	k2eAgNnPc2d1	různé
vydání	vydání	k1gNnPc2	vydání
a	a	k8xC	a
překladů	překlad	k1gInPc2	překlad
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
v	v	k7c6	v
odborné	odborný	k2eAgFnSc6d1	odborná
literatuře	literatura	k1gFnSc6	literatura
obvykle	obvykle	k6eAd1	obvykle
citují	citovat	k5eAaBmIp3nP	citovat
podle	podle	k7c2	podle
Akademického	akademický	k2eAgNnSc2d1	akademické
vydání	vydání	k1gNnSc2	vydání
sebraných	sebraný	k2eAgInPc2d1	sebraný
spisů	spis	k1gInPc2	spis
<g/>
,	,	kIx,	,
dopisů	dopis	k1gInPc2	dopis
i	i	k8xC	i
rukopisné	rukopisný	k2eAgFnSc2d1	rukopisná
pozůstalosti	pozůstalost	k1gFnSc2	pozůstalost
(	(	kIx(	(
<g/>
AA	AA	kA	AA
<g/>
,	,	kIx,	,
Akademie-Ausgabe	Akademie-Ausgab	k1gMnSc5	Akademie-Ausgab
<g/>
,	,	kIx,	,
Berlin	berlina	k1gFnPc2	berlina
1905	[number]	k4	1905
<g/>
–	–	k?	–
<g/>
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
ve	v	k7c6	v
23	[number]	k4	23
svazcích	svazek	k1gInPc6	svazek
<g/>
,	,	kIx,	,
U	u	k7c2	u
nových	nový	k2eAgNnPc2d1	nové
vydání	vydání	k1gNnPc2	vydání
Kritiky	kritika	k1gFnSc2	kritika
čistého	čistý	k2eAgInSc2d1	čistý
rozumu	rozum	k1gInSc2	rozum
bývá	bývat	k5eAaImIp3nS	bývat
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
uvedena	uvést	k5eAaPmNgFnS	uvést
i	i	k8xC	i
<g />
.	.	kIx.	.
</s>
<s>
stránka	stránka	k1gFnSc1	stránka
prvního	první	k4xOgInSc2	první
(	(	kIx(	(
<g/>
A	A	kA	A
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
druhého	druhý	k4xOgMnSc2	druhý
(	(	kIx(	(
<g/>
B	B	kA	B
<g/>
)	)	kIx)	)
vydání	vydání	k1gNnSc1	vydání
<g/>
.	.	kIx.	.
1755	[number]	k4	1755
<g/>
:	:	kIx,	:
Všeobecné	všeobecný	k2eAgFnPc1d1	všeobecná
dějiny	dějiny	k1gFnPc1	dějiny
přírody	příroda	k1gFnSc2	příroda
a	a	k8xC	a
teorie	teorie	k1gFnSc1	teorie
nebes	nebesa	k1gNnPc2	nebesa
–	–	k?	–
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
spise	spis	k1gInSc6	spis
Kant	Kant	k1gMnSc1	Kant
předkládá	předkládat	k5eAaImIp3nS	předkládat
hypotézu	hypotéza	k1gFnSc4	hypotéza
vzniku	vznik	k1gInSc2	vznik
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
z	z	k7c2	z
mlhoviny	mlhovina	k1gFnSc2	mlhovina
(	(	kIx(	(
<g/>
Kantova-Laplaceova	Kantova-Laplaceův	k2eAgFnSc1d1	Kantova-Laplaceova
hypotéza	hypotéza	k1gFnSc1	hypotéza
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
1755	[number]	k4	1755
<g/>
<g />
.	.	kIx.	.
</s>
<s>
:	:	kIx,	:
De	De	k?	De
igne	igne	k1gInSc1	igne
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
O	o	k7c6	o
ohni	oheň	k1gInSc6	oheň
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
1781	[number]	k4	1781
<g/>
:	:	kIx,	:
Kritika	kritika	k1gFnSc1	kritika
čistého	čistý	k2eAgInSc2d1	čistý
rozumu	rozum	k1gInSc2	rozum
(	(	kIx(	(
<g/>
Kritik	kritik	k1gMnSc1	kritik
der	drát	k5eAaImRp2nS	drát
reinen	reinen	k1gInSc4	reinen
Vernunft	Vernunft	k1gInSc4	Vernunft
<g/>
)	)	kIx)	)
–	–	k?	–
vymezení	vymezení	k1gNnSc4	vymezení
apriorních	apriorní	k2eAgFnPc2d1	apriorní
(	(	kIx(	(
<g/>
předempirických	předempirický	k2eAgFnPc2d1	předempirický
<g/>
)	)	kIx)	)
podmínek	podmínka	k1gFnPc2	podmínka
možnosti	možnost	k1gFnSc2	možnost
jakéhokoli	jakýkoli	k3yIgNnSc2	jakýkoli
poznání	poznání	k1gNnSc2	poznání
i	i	k9	i
jeho	jeho	k3xOp3gNnPc2	jeho
přirozených	přirozený	k2eAgNnPc2d1	přirozené
omezení	omezení	k1gNnPc2	omezení
<g/>
.	.	kIx.	.
1783	[number]	k4	1783
<g/>
:	:	kIx,	:
Prolegomena	prolegomena	k1gNnPc4	prolegomena
ke	k	k7c3	k
<g />
.	.	kIx.	.
</s>
<s>
každé	každý	k3xTgFnSc3	každý
metafyzice	metafyzika	k1gFnSc3	metafyzika
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
chce	chtít	k5eAaImIp3nS	chtít
stát	stát	k5eAaPmF	stát
vědou	věda	k1gFnSc7	věda
–	–	k?	–
shrnutí	shrnutí	k1gNnSc4	shrnutí
základních	základní	k2eAgFnPc2d1	základní
myšlenek	myšlenka	k1gFnPc2	myšlenka
Kritiky	kritika	k1gFnSc2	kritika
čistého	čistý	k2eAgInSc2d1	čistý
rozumu	rozum	k1gInSc2	rozum
<g/>
,	,	kIx,	,
návrh	návrh	k1gInSc1	návrh
kritérií	kritérion	k1gNnPc2	kritérion
vědeckosti	vědeckost	k1gFnSc2	vědeckost
myšlenkových	myšlenkový	k2eAgInPc2d1	myšlenkový
systémů	systém	k1gInPc2	systém
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
1916	[number]	k4	1916
1785	[number]	k4	1785
<g/>
:	:	kIx,	:
Základy	základ	k1gInPc1	základ
metafyziky	metafyzika	k1gFnSc2	metafyzika
mravů	mrav	k1gInPc2	mrav
-	-	kIx~	-
stručné	stručný	k2eAgNnSc1d1	stručné
představení	představení	k1gNnSc1	představení
etiky	etika	k1gFnSc2	etika
kategorického	kategorický	k2eAgInSc2d1	kategorický
imperativu	imperativ	k1gInSc2	imperativ
<g/>
.	.	kIx.	.
1788	[number]	k4	1788
<g/>
:	:	kIx,	:
Kritika	kritika	k1gFnSc1	kritika
praktického	praktický	k2eAgInSc2d1	praktický
rozumu	rozum	k1gInSc2	rozum
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Kritik	kritik	k1gMnSc1	kritik
der	drát	k5eAaImRp2nS	drát
praktischen	praktischen	k1gInSc4	praktischen
Vernunft	Vernunft	k1gInSc4	Vernunft
<g/>
)	)	kIx)	)
–	–	k?	–
navrhuje	navrhovat	k5eAaImIp3nS	navrhovat
univerzální	univerzální	k2eAgFnSc4d1	univerzální
racionální	racionální	k2eAgFnSc4d1	racionální
etiku	etika	k1gFnSc4	etika
<g/>
,	,	kIx,	,
založenou	založený	k2eAgFnSc4d1	založená
na	na	k7c6	na
"	"	kIx"	"
<g/>
kategorickém	kategorický	k2eAgInSc6d1	kategorický
imperativu	imperativ	k1gInSc6	imperativ
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
1790	[number]	k4	1790
<g/>
:	:	kIx,	:
Kritika	kritika	k1gFnSc1	kritika
soudnosti	soudnost	k1gFnSc2	soudnost
spojuje	spojovat	k5eAaImIp3nS	spojovat
oblast	oblast	k1gFnSc1	oblast
teoretického	teoretický	k2eAgInSc2d1	teoretický
a	a	k8xC	a
praktického	praktický	k2eAgInSc2d1	praktický
rozumu	rozum	k1gInSc2	rozum
<g/>
,	,	kIx,	,
všímá	všímat	k5eAaImIp3nS	všímat
si	se	k3xPyFc3	se
citů	cit	k1gInPc2	cit
<g/>
,	,	kIx,	,
vkusu	vkus	k1gInSc2	vkus
a	a	k8xC	a
účelnosti	účelnost	k1gFnSc2	účelnost
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
<g/>
.	.	kIx.	.
1793	[number]	k4	1793
<g/>
<g />
.	.	kIx.	.
</s>
<s>
:	:	kIx,	:
Náboženství	náboženství	k1gNnPc4	náboženství
v	v	k7c6	v
mezích	mez	k1gFnPc6	mez
čistého	čistý	k2eAgInSc2d1	čistý
rozumu	rozum	k1gInSc2	rozum
–	–	k?	–
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
nutnost	nutnost	k1gFnSc4	nutnost
přijetí	přijetí	k1gNnSc2	přijetí
svobody	svoboda	k1gFnSc2	svoboda
<g/>
,	,	kIx,	,
nesmrtelné	smrtelný	k2eNgFnSc2d1	nesmrtelná
duše	duše	k1gFnSc2	duše
a	a	k8xC	a
Boha	bůh	k1gMnSc2	bůh
jako	jako	k8xC	jako
postulátů	postulát	k1gInPc2	postulát
praktického	praktický	k2eAgInSc2d1	praktický
rozumu	rozum	k1gInSc2	rozum
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
odmítá	odmítat	k5eAaImIp3nS	odmítat
náboženské	náboženský	k2eAgInPc4d1	náboženský
obřady	obřad	k1gInPc4	obřad
<g/>
.	.	kIx.	.
1795	[number]	k4	1795
<g/>
:	:	kIx,	:
O	o	k7c6	o
věčném	věčný	k2eAgInSc6d1	věčný
míru	mír	k1gInSc6	mír
racionální	racionální	k2eAgNnSc4d1	racionální
světové	světový	k2eAgNnSc4d1	světové
občanství	občanství	k1gNnSc4	občanství
by	by	kYmCp3nP	by
mělo	mít	k5eAaImAgNnS	mít
vést	vést	k5eAaImF	vést
ke	k	k7c3	k
světovému	světový	k2eAgInSc3d1	světový
míru	mír	k1gInSc3	mír
<g/>
.	.	kIx.	.
1797	[number]	k4	1797
<g/>
:	:	kIx,	:
Metafysika	metafysika	k1gFnSc1	metafysika
mravů	mrav	k1gInPc2	mrav
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
teorii	teorie	k1gFnSc4	teorie
práva	právo	k1gNnSc2	právo
a	a	k8xC	a
nauku	nauka	k1gFnSc4	nauka
o	o	k7c6	o
ctnostech	ctnost	k1gFnPc6	ctnost
1798	[number]	k4	1798
<g/>
:	:	kIx,	:
Antropologie	antropologie	k1gFnSc2	antropologie
shrnuje	shrnovat	k5eAaImIp3nS	shrnovat
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
by	by	kYmCp3nS	by
posluchač	posluchač	k1gMnSc1	posluchač
filosofie	filosofie	k1gFnSc2	filosofie
měl	mít	k5eAaImAgMnS	mít
vědět	vědět	k5eAaImF	vědět
o	o	k7c6	o
člověku	člověk	k1gMnSc6	člověk
<g/>
.	.	kIx.	.
</s>
