<p>
<s>
Román	román	k1gInSc1	román
pro	pro	k7c4	pro
ženy	žena	k1gFnPc4	žena
je	být	k5eAaImIp3nS	být
román	román	k1gInSc1	román
od	od	k7c2	od
českého	český	k2eAgMnSc2d1	český
spisovatele	spisovatel	k1gMnSc2	spisovatel
Michala	Michal	k1gMnSc2	Michal
Viewegha	Viewegh	k1gMnSc2	Viewegh
vydaný	vydaný	k2eAgInSc4d1	vydaný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Knihu	kniha	k1gFnSc4	kniha
vydalo	vydat	k5eAaPmAgNnS	vydat
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Petrov	Petrov	k1gInSc1	Petrov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kniha	kniha	k1gFnSc1	kniha
je	být	k5eAaImIp3nS	být
námětem	námět	k1gInSc7	námět
stejnojmenného	stejnojmenný	k2eAgInSc2d1	stejnojmenný
filmu	film	k1gInSc2	film
od	od	k7c2	od
Filipa	Filip	k1gMnSc2	Filip
Renče	Renče	k?	Renče
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Děj	děj	k1gInSc1	děj
==	==	k?	==
</s>
</p>
<p>
<s>
Příběh	příběh	k1gInSc1	příběh
je	být	k5eAaImIp3nS	být
o	o	k7c6	o
dvaadvacetileté	dvaadvacetiletý	k2eAgFnSc6d1	dvaadvacetiletá
redaktorce	redaktorka	k1gFnSc6	redaktorka
ženského	ženský	k2eAgInSc2d1	ženský
magazínu	magazín	k1gInSc2	magazín
Vyrovnaná	vyrovnaný	k2eAgFnSc1d1	vyrovnaná
žena	žena	k1gFnSc1	žena
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zažívá	zažívat	k5eAaImIp3nS	zažívat
velké	velký	k2eAgFnPc4d1	velká
změny	změna	k1gFnPc4	změna
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
milostném	milostný	k2eAgInSc6d1	milostný
životě	život	k1gInSc6	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
