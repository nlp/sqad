<s>
Couperin	Couperin	k1gInSc1	Couperin
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgInS	oženit
26	[number]	k4	26
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
roku	rok	k1gInSc2	rok
1689	[number]	k4	1689
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc7	jeho
manželkou	manželka	k1gFnSc7	manželka
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
Marie-Anne	Marie-Ann	k1gInSc5	Marie-Ann
Ansaultová	Ansaultová	k1gFnSc1	Ansaultová
<g/>
,	,	kIx,	,
osoba	osoba	k1gFnSc1	osoba
s	s	k7c7	s
rozsáhlými	rozsáhlý	k2eAgInPc7d1	rozsáhlý
styky	styk	k1gInPc7	styk
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gMnPc1	jejíž
přátelé	přítel	k1gMnPc1	přítel
se	se	k3xPyFc4	se
ukázali	ukázat	k5eAaPmAgMnP	ukázat
být	být	k5eAaImF	být
velice	velice	k6eAd1	velice
prospěšnými	prospěšný	k2eAgInPc7d1	prospěšný
pro	pro	k7c4	pro
další	další	k2eAgFnSc4d1	další
Françoisovou	Françoisový	k2eAgFnSc4d1	Françoisový
hudební	hudební	k2eAgFnSc4d1	hudební
dráhu	dráha	k1gFnSc4	dráha
<g/>
.	.	kIx.	.
</s>
