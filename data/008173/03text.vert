<p>
<s>
François	François	k1gInSc1	François
Couperin	Couperin	k1gInSc1	Couperin
(	(	kIx(	(
<g/>
IPA	IPA	kA	IPA
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
fʀ	fʀ	k?	fʀ
<g/>
̃	̃	k?	̃
<g/>
'	'	kIx"	'
<g/>
swa	swa	k?	swa
ku	k	k7c3	k
<g/>
'	'	kIx"	'
<g/>
pʀ	pʀ	k?	pʀ
<g/>
̃	̃	k?	̃
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
10	[number]	k4	10
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1668	[number]	k4	1668
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
–	–	k?	–
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1733	[number]	k4	1733
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
varhaník	varhaník	k1gMnSc1	varhaník
období	období	k1gNnSc2	období
baroka	baroko	k1gNnSc2	baroko
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
byl	být	k5eAaImAgInS	být
přezdíván	přezdívat	k5eAaImNgInS	přezdívat
jako	jako	k8xC	jako
Couperin	Couperin	k1gInSc1	Couperin
le	le	k?	le
Grand	grand	k1gMnSc1	grand
(	(	kIx(	(
<g/>
Velký	velký	k2eAgInSc1d1	velký
Couperin	Couperin	k1gInSc1	Couperin
<g/>
)	)	kIx)	)
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
odlišil	odlišit	k5eAaPmAgInS	odlišit
od	od	k7c2	od
jiných	jiný	k2eAgMnPc2d1	jiný
stejnojmenných	stejnojmenný	k2eAgMnPc2d1	stejnojmenný
muzikantů	muzikant	k1gMnPc2	muzikant
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
rodině	rodina	k1gFnSc6	rodina
před	před	k7c7	před
ním	on	k3xPp3gMnSc7	on
i	i	k9	i
po	po	k7c6	po
něm	on	k3xPp3gMnSc6	on
a	a	k8xC	a
zdůraznil	zdůraznit	k5eAaPmAgMnS	zdůraznit
jeho	jeho	k3xOp3gInSc4	jeho
význam	význam	k1gInSc4	význam
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
François	François	k1gInSc1	François
Couperin	Couperin	k1gInSc1	Couperin
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgInS	narodit
jako	jako	k9	jako
jediné	jediný	k2eAgNnSc4d1	jediné
dítě	dítě	k1gNnSc4	dítě
v	v	k7c6	v
muzikantské	muzikantský	k2eAgFnSc6d1	muzikantská
rodině	rodina	k1gFnSc6	rodina
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
Charles	Charles	k1gMnSc1	Charles
Couperin	Couperin	k1gInSc1	Couperin
(	(	kIx(	(
<g/>
1638	[number]	k4	1638
<g/>
-	-	kIx~	-
<g/>
79	[number]	k4	79
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
varhaníkem	varhaník	k1gMnSc7	varhaník
v	v	k7c6	v
chrámu	chrám	k1gInSc6	chrám
St.	st.	kA	st.
Gervais	Gervais	k1gFnSc4	Gervais
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nahradil	nahradit	k5eAaPmAgMnS	nahradit
svého	svůj	k3xOyFgMnSc4	svůj
bratra	bratr	k1gMnSc4	bratr
Louise	Louis	k1gMnSc4	Louis
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zde	zde	k6eAd1	zde
působil	působit	k5eAaImAgMnS	působit
a	a	k8xC	a
skládal	skládat	k5eAaImAgMnS	skládat
na	na	k7c4	na
varhany	varhany	k1gFnPc4	varhany
celých	celý	k2eAgNnPc2d1	celé
osm	osm	k4xCc1	osm
let	léto	k1gNnPc2	léto
před	před	k7c7	před
svou	svůj	k3xOyFgFnSc7	svůj
smrtí	smrt	k1gFnSc7	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Zřejmě	zřejmě	k6eAd1	zřejmě
právě	právě	k9	právě
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
mu	on	k3xPp3gMnSc3	on
poskytl	poskytnout	k5eAaPmAgMnS	poskytnout
první	první	k4xOgFnSc4	první
hudební	hudební	k2eAgFnSc4d1	hudební
hodiny	hodina	k1gFnPc4	hodina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ten	ten	k3xDgMnSc1	ten
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c4	v
den	den	k1gInSc4	den
svých	svůj	k3xOyFgFnPc2	svůj
čtyřicátých	čtyřicátý	k4xOgFnPc2	čtyřicátý
narozenin	narozeniny	k1gFnPc2	narozeniny
a	a	k8xC	a
desetiletého	desetiletý	k2eAgNnSc2d1	desetileté
Françoise	Françoise	k1gFnPc1	Françoise
ponechal	ponechat	k5eAaPmAgInS	ponechat
v	v	k7c6	v
péči	péče	k1gFnSc6	péče
matky	matka	k1gFnSc2	matka
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
nejbližším	blízký	k2eAgMnSc7d3	nejbližší
mužským	mužský	k2eAgMnSc7d1	mužský
příbuzným	příbuzný	k1gMnSc7	příbuzný
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
tedy	tedy	k9	tedy
jeho	jeho	k3xOp3gMnSc1	jeho
strýc	strýc	k1gMnSc1	strýc
a	a	k8xC	a
kmotr	kmotr	k1gMnSc1	kmotr
François	François	k1gFnSc2	François
Couperin	Couperin	k1gInSc1	Couperin
(	(	kIx(	(
<g/>
asi	asi	k9	asi
1631	[number]	k4	1631
<g/>
-	-	kIx~	-
<g/>
1710	[number]	k4	1710
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
svého	svůj	k3xOyFgMnSc4	svůj
synovce	synovec	k1gMnSc4	synovec
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
"	"	kIx"	"
<g/>
velkým	velký	k2eAgMnSc7d1	velký
hudebníkem	hudebník	k1gMnSc7	hudebník
a	a	k8xC	a
velkým	velký	k2eAgMnSc7d1	velký
pijanem	pijan	k1gMnSc7	pijan
<g/>
"	"	kIx"	"
a	a	k8xC	a
právě	právě	k9	právě
to	ten	k3xDgNnSc1	ten
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
důvod	důvod	k1gInSc4	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
svěřila	svěřit	k5eAaPmAgFnS	svěřit
hudební	hudební	k2eAgFnSc4d1	hudební
výchovu	výchova	k1gFnSc4	výchova
Françoise	Françoise	k1gFnSc2	Françoise
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
rodinného	rodinný	k2eAgMnSc2d1	rodinný
přítele	přítel	k1gMnSc2	přítel
<g/>
,	,	kIx,	,
varhaníka	varhaník	k1gMnSc2	varhaník
Jacquese	Jacques	k1gMnSc2	Jacques
Thomelina	Thomelina	k1gFnSc1	Thomelina
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zaujímal	zaujímat	k5eAaImAgMnS	zaujímat
místa	místo	k1gNnPc1	místo
u	u	k7c2	u
dvora	dvůr	k1gInSc2	dvůr
i	i	k9	i
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
kroky	krok	k1gInPc1	krok
byly	být	k5eAaImAgInP	být
tak	tak	k6eAd1	tak
význačné	význačný	k2eAgInPc1d1	význačný
a	a	k8xC	a
rodina	rodina	k1gFnSc1	rodina
byla	být	k5eAaImAgFnS	být
chována	chován	k2eAgFnSc1d1	chována
v	v	k7c6	v
takové	takový	k3xDgFnSc6	takový
úctě	úcta	k1gFnSc6	úcta
<g/>
,	,	kIx,	,
že	že	k8xS	že
církevní	církevní	k2eAgFnPc1d1	církevní
rada	rada	k1gFnSc1	rada
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
St.	st.	kA	st.
Gervais	Gervais	k1gFnSc1	Gervais
souhlasila	souhlasit	k5eAaImAgFnS	souhlasit
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
mohla	moct	k5eAaImAgFnS	moct
bydlet	bydlet	k5eAaImF	bydlet
v	v	k7c6	v
domě	dům	k1gInSc6	dům
chrámového	chrámový	k2eAgMnSc2d1	chrámový
varhaníka	varhaník	k1gMnSc2	varhaník
a	a	k8xC	a
aby	aby	kYmCp3nS	aby
on	on	k3xPp3gMnSc1	on
mohl	moct	k5eAaImAgMnS	moct
nastoupit	nastoupit	k5eAaPmF	nastoupit
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
až	až	k9	až
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
osmnácti	osmnáct	k4xCc2	osmnáct
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Couperin	Couperin	k1gInSc1	Couperin
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgInS	oženit
26	[number]	k4	26
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
roku	rok	k1gInSc2	rok
1689	[number]	k4	1689
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc7	jeho
manželkou	manželka	k1gFnSc7	manželka
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
Marie-Anne	Marie-Ann	k1gInSc5	Marie-Ann
Ansaultová	Ansaultová	k1gFnSc1	Ansaultová
<g/>
,	,	kIx,	,
osoba	osoba	k1gFnSc1	osoba
s	s	k7c7	s
rozsáhlými	rozsáhlý	k2eAgInPc7d1	rozsáhlý
styky	styk	k1gInPc7	styk
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gMnPc1	jejíž
přátelé	přítel	k1gMnPc1	přítel
se	se	k3xPyFc4	se
ukázali	ukázat	k5eAaPmAgMnP	ukázat
být	být	k5eAaImF	být
velice	velice	k6eAd1	velice
prospěšnými	prospěšný	k2eAgInPc7d1	prospěšný
pro	pro	k7c4	pro
další	další	k2eAgFnSc4d1	další
Françoisovou	Françoisový	k2eAgFnSc4d1	Françoisový
hudební	hudební	k2eAgFnSc4d1	hudební
dráhu	dráha	k1gFnSc4	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Povolení	povolení	k1gNnSc4	povolení
zveřejňovat	zveřejňovat	k5eAaImF	zveřejňovat
své	svůj	k3xOyFgFnPc4	svůj
skladby	skladba	k1gFnPc4	skladba
získal	získat	k5eAaPmAgInS	získat
Couperin	Couperin	k1gInSc1	Couperin
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1690	[number]	k4	1690
<g/>
.	.	kIx.	.
</s>
<s>
Manželům	manžel	k1gMnPc3	manžel
se	se	k3xPyFc4	se
v	v	k7c6	v
období	období	k1gNnSc6	období
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1690	[number]	k4	1690
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1707	[number]	k4	1707
narodily	narodit	k5eAaPmAgFnP	narodit
celkem	celkem	k6eAd1	celkem
čtyři	čtyři	k4xCgFnPc4	čtyři
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
dva	dva	k4xCgMnPc1	dva
chlapci	chlapec	k1gMnPc1	chlapec
a	a	k8xC	a
dvě	dva	k4xCgFnPc4	dva
dívky	dívka	k1gFnPc4	dívka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1690	[number]	k4	1690
se	se	k3xPyFc4	se
narodilo	narodit	k5eAaPmAgNnS	narodit
první	první	k4xOgNnSc1	první
dítě	dítě	k1gNnSc1	dítě
<g/>
,	,	kIx,	,
Marie-Madeleine	Marie-Madelein	k1gMnSc5	Marie-Madelein
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
roku	rok	k1gInSc2	rok
1705	[number]	k4	1705
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
Marguerite-Antoinette	Marguerite-Antoinett	k1gInSc5	Marguerite-Antoinett
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
roku	rok	k1gInSc2	rok
1706	[number]	k4	1706
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
první	první	k4xOgMnSc1	první
syn	syn	k1gMnSc1	syn
François-Laurent	François-Laurent	k1gMnSc1	François-Laurent
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1707	[number]	k4	1707
poslední	poslední	k2eAgFnSc2d1	poslední
dítě	dítě	k1gNnSc4	dítě
v	v	k7c4	v
manželství	manželství	k1gNnSc4	manželství
-	-	kIx~	-
Nicolas-Luise	Nicolas-Luise	k1gFnSc2	Nicolas-Luise
<g/>
.	.	kIx.	.
<g/>
Když	když	k8xS	když
Thomelin	Thomelina	k1gFnPc2	Thomelina
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1693	[number]	k4	1693
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
,	,	kIx,	,
převzal	převzít	k5eAaPmAgMnS	převzít
za	za	k7c4	za
něj	on	k3xPp3gInSc4	on
Couperin	Couperin	k1gInSc4	Couperin
místo	místo	k7c2	místo
varhaníka	varhaník	k1gMnSc2	varhaník
v	v	k7c6	v
chrámu	chrám	k1gInSc6	chrám
a	a	k8xC	a
jmenování	jmenování	k1gNnSc4	jmenování
dvorním	dvorní	k2eAgMnSc7d1	dvorní
varhaníkem	varhaník	k1gMnSc7	varhaník
<g/>
,	,	kIx,	,
provedené	provedený	k2eAgInPc1d1	provedený
samotným	samotný	k2eAgMnSc7d1	samotný
Ludvíkem	Ludvík	k1gMnSc7	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
zdvojnásobilo	zdvojnásobit	k5eAaPmAgNnS	zdvojnásobit
plat	plat	k1gInSc4	plat
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
pobíral	pobírat	k5eAaImAgInS	pobírat
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
své	svůj	k3xOyFgFnPc4	svůj
povinnosti	povinnost	k1gFnPc4	povinnost
v	v	k7c6	v
královské	královský	k2eAgFnSc6d1	královská
kapli	kaple	k1gFnSc6	kaple
a	a	k8xC	a
ve	v	k7c6	v
Versailles	Versailles	k1gFnPc6	Versailles
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
omezovaly	omezovat	k5eAaImAgInP	omezovat
jen	jen	k9	jen
na	na	k7c4	na
první	první	k4xOgInPc4	první
tři	tři	k4xCgInPc4	tři
měsíce	měsíc	k1gInPc4	měsíc
v	v	k7c6	v
roce	rok	k1gInSc6	rok
<g/>
,	,	kIx,	,
sdílel	sdílet	k5eAaImAgMnS	sdílet
se	s	k7c7	s
třemi	tři	k4xCgMnPc7	tři
dalšími	další	k2eAgMnPc7d1	další
varhaníky	varhaník	k1gMnPc7	varhaník
<g/>
,	,	kIx,	,
udržel	udržet	k5eAaPmAgMnS	udržet
si	se	k3xPyFc3	se
své	svůj	k3xOyFgNnSc4	svůj
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
chrámu	chrám	k1gInSc6	chrám
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pobýval	pobývat	k5eAaImAgMnS	pobývat
ještě	ještě	k9	ještě
další	další	k2eAgNnPc4d1	další
tři	tři	k4xCgNnPc4	tři
desetiletí	desetiletí	k1gNnPc4	desetiletí
<g/>
,	,	kIx,	,
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1723	[number]	k4	1723
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k1gNnSc1	místo
u	u	k7c2	u
dvora	dvůr	k1gInSc2	dvůr
obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
štědrý	štědrý	k2eAgInSc1d1	štědrý
plat	plat	k1gInSc1	plat
a	a	k8xC	a
nesrovnatelné	srovnatelný	k2eNgFnPc1d1	nesrovnatelná
pracovní	pracovní	k2eAgFnPc1d1	pracovní
podmínky	podmínka	k1gFnPc1	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
jeho	jeho	k3xOp3gNnPc7	jeho
povinnosti	povinnost	k1gFnSc2	povinnost
patřily	patřit	k5eAaImAgInP	patřit
rovněž	rovněž	k9	rovněž
i	i	k9	i
hodiny	hodina	k1gFnPc1	hodina
výuky	výuka	k1gFnSc2	výuka
hry	hra	k1gFnSc2	hra
na	na	k7c4	na
cembalo	cembalo	k1gNnSc4	cembalo
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yRgFnPc4	který
učil	učit	k5eAaImAgMnS	učit
hrát	hrát	k5eAaImF	hrát
královské	královský	k2eAgFnPc4d1	královská
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
mu	on	k3xPp3gMnSc3	on
udělen	udělen	k2eAgInSc4d1	udělen
titul	titul	k1gInSc4	titul
"	"	kIx"	"
<g/>
Maître	Maîtr	k1gMnSc5	Maîtr
de	de	k?	de
clavecin	clavecin	k1gInSc1	clavecin
des	des	k1gNnSc1	des
enfants	enfants	k1gInSc1	enfants
de	de	k?	de
France	Franc	k1gMnSc2	Franc
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
třech	tři	k4xCgNnPc6	tři
letech	léto	k1gNnPc6	léto
působení	působení	k1gNnSc2	působení
u	u	k7c2	u
dvora	dvůr	k1gInSc2	dvůr
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgMnS	být
udělen	udělen	k2eAgInSc4d1	udělen
šlechtický	šlechtický	k2eAgInSc4d1	šlechtický
titul	titul	k1gInSc4	titul
a	a	k8xC	a
erb	erb	k1gInSc4	erb
a	a	k8xC	a
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
se	se	k3xPyFc4	se
směl	smět	k5eAaImAgMnS	smět
podepisovat	podepisovat	k5eAaImF	podepisovat
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
le	le	k?	le
Chevalier	chevalier	k1gMnSc1	chevalier
Couperin	Couperin	k1gInSc1	Couperin
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Couperin	Couperin	k1gInSc1	Couperin
držel	držet	k5eAaImAgInS	držet
přibližně	přibližně	k6eAd1	přibližně
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1700	[number]	k4	1700
titul	titul	k1gInSc1	titul
hlavního	hlavní	k2eAgMnSc2d1	hlavní
dvorního	dvorní	k2eAgMnSc2d1	dvorní
hudebníka	hudebník	k1gMnSc2	hudebník
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
neoficiálně	neoficiálně	k6eAd1	neoficiálně
<g/>
,	,	kIx,	,
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
za	za	k7c4	za
učitele	učitel	k1gMnSc4	učitel
hry	hra	k1gFnSc2	hra
na	na	k7c4	na
cembalo	cembalo	k1gNnSc4	cembalo
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
neměl	mít	k5eNaImAgMnS	mít
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
konkurenci	konkurence	k1gFnSc4	konkurence
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1713	[number]	k4	1713
získal	získat	k5eAaPmAgMnS	získat
další	další	k2eAgNnSc4d1	další
povolení	povolení	k1gNnSc4	povolení
umožňující	umožňující	k2eAgNnSc4d1	umožňující
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
uveřejňovat	uveřejňovat	k5eAaImF	uveřejňovat
své	své	k1gNnSc4	své
díla	dílo	k1gNnSc2	dílo
počínaje	počínaje	k7c7	počínaje
cyklem	cyklus	k1gInSc7	cyklus
cembalových	cembalový	k2eAgFnPc2d1	cembalová
skladeb	skladba	k1gFnPc2	skladba
a	a	k8xC	a
včetně	včetně	k7c2	včetně
tří	tři	k4xCgInPc2	tři
z	z	k7c2	z
devíti	devět	k4xCc2	devět
Leçons	Leçonsa	k1gFnPc2	Leçonsa
des	des	k1gNnPc2	des
ténebres	ténebres	k1gMnSc1	ténebres
<g/>
.	.	kIx.	.
</s>
<s>
Králi	Král	k1gMnSc3	Král
slunce	slunce	k1gNnSc4	slunce
<g/>
,	,	kIx,	,
Ludvíkovi	Ludvíkův	k2eAgMnPc1d1	Ludvíkův
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
sloužil	sloužit	k5eAaImAgInS	sloužit
až	až	k9	až
do	do	k7c2	do
jeho	jeho	k3xOp3gFnSc2	jeho
smrti	smrt	k1gFnSc2	smrt
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1715	[number]	k4	1715
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1717	[number]	k4	1717
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
"	"	kIx"	"
<g/>
Ordinaire	Ordinair	k1gInSc5	Ordinair
de	de	k?	de
la	la	k0	la
chambre	chambr	k1gInSc5	chambr
pour	pour	k1gInSc4	pour
le	le	k?	le
clavecin	clavecin	k1gInSc1	clavecin
<g/>
"	"	kIx"	"
na	na	k7c6	na
dvoře	dvůr	k1gInSc6	dvůr
Ludvíka	Ludvík	k1gMnSc2	Ludvík
XV	XV	kA	XV
<g/>
.	.	kIx.	.
během	během	k7c2	během
regentství	regentství	k1gNnSc2	regentství
Filipa	Filip	k1gMnSc2	Filip
Orleánského	orleánský	k2eAgMnSc2d1	orleánský
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1716	[number]	k4	1716
uveřejnil	uveřejnit	k5eAaPmAgInS	uveřejnit
pojednání	pojednání	k1gNnSc4	pojednání
o	o	k7c6	o
své	svůj	k3xOyFgFnSc6	svůj
hře	hra	k1gFnSc6	hra
na	na	k7c4	na
cembalo	cembalo	k1gNnSc4	cembalo
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc7	který
nazval	nazvat	k5eAaPmAgMnS	nazvat
"	"	kIx"	"
<g/>
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
art	art	k?	art
de	de	k?	de
toucher	touchra	k1gFnPc2	touchra
le	le	k?	le
clavecin	clavecin	k1gInSc1	clavecin
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
pro	pro	k7c4	pro
druhé	druhý	k4xOgNnSc4	druhý
vydání	vydání	k1gNnSc4	vydání
v	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
roce	rok	k1gInSc6	rok
upravil	upravit	k5eAaPmAgMnS	upravit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Asi	asi	k9	asi
roku	rok	k1gInSc2	rok
1730	[number]	k4	1730
začal	začít	k5eAaPmAgInS	začít
Couperin	Couperin	k1gInSc1	Couperin
trpět	trpět	k5eAaImF	trpět
čím	co	k3yRnSc7	co
dál	daleko	k6eAd2	daleko
více	hodně	k6eAd2	hodně
zhoršujícím	zhoršující	k2eAgMnSc7d1	zhoršující
se	se	k3xPyFc4	se
zdravím	zdraví	k1gNnSc7	zdraví
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
však	však	k9	však
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
o	o	k7c4	o
jakou	jaký	k3yIgFnSc4	jaký
chorobu	choroba	k1gFnSc4	choroba
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
<g/>
.	.	kIx.	.
</s>
<s>
Vzdal	vzdát	k5eAaPmAgInS	vzdát
se	se	k3xPyFc4	se
svého	svůj	k3xOyFgNnSc2	svůj
místa	místo	k1gNnSc2	místo
u	u	k7c2	u
dvora	dvůr	k1gInSc2	dvůr
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ho	on	k3xPp3gInSc4	on
později	pozdě	k6eAd2	pozdě
zastoupila	zastoupit	k5eAaPmAgFnS	zastoupit
jeho	jeho	k3xOp3gFnSc1	jeho
dcera	dcera	k1gFnSc1	dcera
Marguerite-Antoinette	Marguerite-Antoinett	k1gMnSc5	Marguerite-Antoinett
<g/>
,	,	kIx,	,
a	a	k8xC	a
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
domě	dům	k1gInSc6	dům
v	v	k7c6	v
Rue	Rue	k1gFnSc6	Rue
Newe	New	k1gFnSc2	New
des	des	k1gNnSc2	des
Bons	Bonsa	k1gFnPc2	Bonsa
Enfants	Enfantsa	k1gFnPc2	Enfantsa
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
François	François	k1gInSc1	François
Couperin	Couperin	k1gInSc1	Couperin
patřil	patřit	k5eAaImAgInS	patřit
nepochybně	pochybně	k6eNd1	pochybně
k	k	k7c3	k
významnějším	významný	k2eAgFnPc3d2	významnější
postavám	postava	k1gFnPc3	postava
barokní	barokní	k2eAgFnSc2d1	barokní
éry	éra	k1gFnSc2	éra
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
také	také	k6eAd1	také
nejbližším	blízký	k2eAgInSc7d3	Nejbližší
francouzským	francouzský	k2eAgInSc7d1	francouzský
ekvivalentem	ekvivalent	k1gInSc7	ekvivalent
Johanna	Johann	k1gMnSc2	Johann
Sebastiana	Sebastian	k1gMnSc2	Sebastian
Bacha	Bacha	k?	Bacha
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
jeho	jeho	k3xOp3gMnSc7	jeho
současníkem	současník	k1gMnSc7	současník
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
znal	znát	k5eAaImAgInS	znát
a	a	k8xC	a
uznával	uznávat	k5eAaImAgInS	uznávat
jeho	jeho	k3xOp3gFnSc4	jeho
hudbu	hudba	k1gFnSc4	hudba
pro	pro	k7c4	pro
cembalo	cembalo	k1gNnSc4	cembalo
a	a	k8xC	a
některými	některý	k3yIgFnPc7	některý
pasážemi	pasáž	k1gFnPc7	pasáž
se	se	k3xPyFc4	se
nechal	nechat	k5eAaPmAgMnS	nechat
i	i	k9	i
dosti	dosti	k6eAd1	dosti
silně	silně	k6eAd1	silně
inspirovat	inspirovat	k5eAaBmF	inspirovat
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
s	s	k7c7	s
Bachem	Bach	k1gInSc7	Bach
dopisoval	dopisovat	k5eAaImAgMnS	dopisovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
žádné	žádný	k3yNgInPc1	žádný
dopisy	dopis	k1gInPc1	dopis
ani	ani	k8xC	ani
zmínky	zmínka	k1gFnPc1	zmínka
se	se	k3xPyFc4	se
nedochovaly	dochovat	k5eNaPmAgFnP	dochovat
<g/>
.	.	kIx.	.
</s>
<s>
Nepřežil	přežít	k5eNaPmAgMnS	přežít
skutečně	skutečně	k6eAd1	skutečně
ani	ani	k8xC	ani
jediný	jediný	k2eAgMnSc1d1	jediný
a	a	k8xC	a
záznamy	záznam	k1gInPc1	záznam
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
osobě	osoba	k1gFnSc6	osoba
z	z	k7c2	z
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
doby	doba	k1gFnSc2	doba
jsou	být	k5eAaImIp3nP	být
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
také	také	k9	také
velice	velice	k6eAd1	velice
vzácné	vzácný	k2eAgInPc1d1	vzácný
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
dozvídáme	dozvídat	k5eAaImIp1nP	dozvídat
něco	něco	k6eAd1	něco
o	o	k7c6	o
něm	on	k3xPp3gInSc6	on
jsou	být	k5eAaImIp3nP	být
jeho	jeho	k3xOp3gFnPc1	jeho
předmluvy	předmluva	k1gFnPc1	předmluva
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
ke	k	k7c3	k
svým	svůj	k3xOyFgMnPc3	svůj
uveřejněným	uveřejněný	k2eAgMnPc3d1	uveřejněný
pracím	prací	k2eAgMnPc3d1	prací
psal	psát	k5eAaImAgMnS	psát
<g/>
,	,	kIx,	,
získáváme	získávat	k5eAaImIp1nP	získávat
dojem	dojem	k1gInSc4	dojem
jakéhosi	jakýsi	k3yIgMnSc2	jakýsi
praktického	praktický	k2eAgMnSc2d1	praktický
muže	muž	k1gMnSc2	muž
a	a	k8xC	a
také	také	k9	také
zde	zde	k6eAd1	zde
nacházíme	nacházet	k5eAaImIp1nP	nacházet
popis	popis	k1gInSc4	popis
jeho	on	k3xPp3gMnSc2	on
strýce	strýc	k1gMnSc2	strýc
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
popisoval	popisovat	k5eAaImAgMnS	popisovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
velkého	velký	k2eAgMnSc2d1	velký
pijana	pijan	k1gMnSc2	pijan
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
jsme	být	k5eAaImIp1nP	být
zde	zde	k6eAd1	zde
již	již	k6eAd1	již
zmiňovaly	zmiňovat	k5eAaImAgInP	zmiňovat
<g/>
,	,	kIx,	,
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
měl	mít	k5eAaImAgMnS	mít
i	i	k9	i
vytříbený	vytříbený	k2eAgInSc4d1	vytříbený
smysl	smysl	k1gInSc4	smysl
pro	pro	k7c4	pro
humor	humor	k1gInSc4	humor
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
svědčí	svědčit	k5eAaImIp3nS	svědčit
i	i	k9	i
několik	několik	k4yIc4	několik
názvů	název	k1gInPc2	název
jeho	jeho	k3xOp3gNnPc2	jeho
děl	dělo	k1gNnPc2	dělo
pro	pro	k7c4	pro
cimbalo	cimbat	k5eAaImAgNnS	cimbat
jako	jako	k8xS	jako
např.	např.	kA	např.
Včely	včela	k1gFnSc2	včela
<g/>
,	,	kIx,	,
Úhoř	úhoř	k1gMnSc1	úhoř
<g/>
,	,	kIx,	,
Požitky	požitek	k1gInPc1	požitek
<g/>
,	,	kIx,	,
Zamilovaný	zamilovaný	k2eAgInSc1d1	zamilovaný
slavík	slavík	k1gInSc1	slavík
a	a	k8xC	a
nepřeložené	přeložený	k2eNgFnPc1d1	nepřeložená
Le	Le	k1gFnPc1	Le
tictic-choc	tictichoc	k6eAd1	tictic-choc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
začínal	začínat	k5eAaImAgInS	začínat
Couperin	Couperin	k1gInSc1	Couperin
jako	jako	k8xC	jako
varhaník	varhaník	k1gMnSc1	varhaník
<g/>
,	,	kIx,	,
komponoval	komponovat	k5eAaImAgMnS	komponovat
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
nástroj	nástroj	k1gInSc4	nástroj
jen	jen	k9	jen
minimální	minimální	k2eAgInSc4d1	minimální
počet	počet	k1gInSc4	počet
děl	dělo	k1gNnPc2	dělo
a	a	k8xC	a
namísto	namísto	k7c2	namísto
toho	ten	k3xDgInSc2	ten
psal	psát	k5eAaImAgInS	psát
nejvíce	hodně	k6eAd3	hodně
pro	pro	k7c4	pro
cembalo	cembalo	k1gNnSc4	cembalo
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgInPc4d1	mnohý
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
skladeb	skladba	k1gFnPc2	skladba
jsou	být	k5eAaImIp3nP	být
krátké	krátký	k2eAgFnPc1d1	krátká
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
je	být	k5eAaImIp3nS	být
shromáždil	shromáždit	k5eAaPmAgMnS	shromáždit
do	do	k7c2	do
sedmadvaceti	sedmadvacet	k4xCc2	sedmadvacet
cyklů	cyklus	k1gInPc2	cyklus
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
nazýval	nazývat	k5eAaImAgInS	nazývat
ordres	ordres	k1gInSc1	ordres
a	a	k8xC	a
které	který	k3yRgInPc4	který
zhruba	zhruba	k6eAd1	zhruba
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
suitám	suita	k1gFnPc3	suita
<g/>
.	.	kIx.	.
</s>
<s>
Couperinův	Couperinův	k2eAgInSc1d1	Couperinův
styl	styl	k1gInSc1	styl
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
spíše	spíše	k9	spíše
rokoku	rokoko	k1gNnSc3	rokoko
<g/>
,	,	kIx,	,
než	než	k8xS	než
např.	např.	kA	např.
Bachův	Bachův	k2eAgMnSc1d1	Bachův
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
typický	typický	k2eAgMnSc1d1	typický
barokní	barokní	k2eAgMnSc1d1	barokní
<g/>
:	:	kIx,	:
účelem	účel	k1gInSc7	účel
je	být	k5eAaImIp3nS	být
spíše	spíše	k9	spíše
líbivost	líbivost	k1gFnSc1	líbivost
<g/>
,	,	kIx,	,
než	než	k8xS	než
prostá	prostý	k2eAgFnSc1d1	prostá
krása	krása	k1gFnSc1	krása
a	a	k8xC	a
melodické	melodický	k2eAgFnPc1d1	melodická
linie	linie	k1gFnPc1	linie
jsou	být	k5eAaImIp3nP	být
ozdobeny	ozdoben	k2eAgInPc4d1	ozdoben
složitými	složitý	k2eAgInPc7d1	složitý
ornamenty	ornament	k1gInPc7	ornament
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
smyslu	smysl	k1gInSc6	smysl
je	být	k5eAaImIp3nS	být
naprostým	naprostý	k2eAgInSc7d1	naprostý
protějškem	protějšek	k1gInSc7	protějšek
francouzského	francouzský	k2eAgMnSc2d1	francouzský
malíře	malíř	k1gMnSc2	malíř
Watteaua	Watteauus	k1gMnSc2	Watteauus
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc1	jeho
mladšího	mladý	k2eAgMnSc4d2	mladší
současníka	současník	k1gMnSc4	současník
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
s	s	k7c7	s
láskou	láska	k1gFnSc7	láska
zobrazoval	zobrazovat	k5eAaImAgMnS	zobrazovat
idealizované	idealizovaný	k2eAgFnSc2d1	idealizovaná
postorální	postorální	k2eAgFnSc2d1	postorální
scény	scéna	k1gFnSc2	scéna
do	do	k7c2	do
líbivých	líbivý	k2eAgFnPc2d1	líbivá
kratochvílí	kratochvíle	k1gFnSc7	kratochvíle
<g/>
.	.	kIx.	.
</s>
<s>
Ostřejší	ostrý	k2eAgInSc1d2	ostřejší
vtip	vtip	k1gInSc1	vtip
dává	dávat	k5eAaImIp3nS	dávat
nejvíce	hodně	k6eAd3	hodně
najevo	najevo	k6eAd1	najevo
v	v	k7c4	v
Les	les	k1gInSc4	les
folies	folies	k1gMnSc1	folies
françoises	françoises	k1gMnSc1	françoises
ve	v	k7c6	v
třinácté	třináctý	k4xOgFnSc6	třináctý
ordre	ordre	k1gInSc1	ordre
svým	svůj	k3xOyFgInSc7	svůj
popisem	popis	k1gInSc7	popis
touhy	touha	k1gFnSc2	touha
<g/>
,	,	kIx,	,
stálosti	stálost	k1gFnSc3	stálost
atd.	atd.	kA	atd.
<g/>
,	,	kIx,	,
a	a	k8xC	a
skladbě	skladba	k1gFnSc3	skladba
z	z	k7c2	z
jedenácté	jedenáctý	k4xOgFnSc2	jedenáctý
ordre	ordre	k1gInSc1	ordre
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
evokuje	evokovat	k5eAaBmIp3nS	evokovat
skupinu	skupina	k1gFnSc4	skupina
minstrelů	minstrel	k1gMnPc2	minstrel
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
jsou	být	k5eAaImIp3nP	být
chycení	chycený	k2eAgMnPc1d1	chycený
spíše	spíše	k9	spíše
podle	podle	k7c2	podle
pravidla	pravidlo	k1gNnSc2	pravidlo
"	"	kIx"	"
<g/>
kdo	kdo	k3yInSc1	kdo
čím	co	k3yQnSc7	co
zachází	zacházet	k5eAaImIp3nS	zacházet
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
také	také	k9	také
schází	scházet	k5eAaImIp3nS	scházet
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Většina	většina	k1gFnSc1	většina
rozsáhlé	rozsáhlý	k2eAgFnSc2d1	rozsáhlá
Couperinové	Couperinový	k2eAgFnSc2d1	Couperinový
tvorby	tvorba	k1gFnSc2	tvorba
vokální	vokální	k2eAgFnSc2d1	vokální
<g/>
,	,	kIx,	,
komorní	komorní	k2eAgFnSc2d1	komorní
a	a	k8xC	a
cembalové	cembalový	k2eAgFnSc2d1	cembalová
byla	být	k5eAaImAgFnS	být
napsaná	napsaný	k2eAgFnSc1d1	napsaná
pro	pro	k7c4	pro
versaillské	versaillský	k2eAgInPc4d1	versaillský
Concerts	Concerts	k1gInSc4	Concerts
du	du	k?	du
Dimanche	Dimanche	k1gInSc1	Dimanche
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgFnSc1	některý
jeho	jeho	k3xOp3gFnSc1	jeho
komorní	komorní	k2eAgFnSc1d1	komorní
hudba	hudba	k1gFnSc1	hudba
svědčí	svědčit	k5eAaImIp3nS	svědčit
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
oblibě	obliba	k1gFnSc6	obliba
italského	italský	k2eAgMnSc2d1	italský
skladatele	skladatel	k1gMnSc2	skladatel
Corelliho	Corelli	k1gMnSc2	Corelli
a	a	k8xC	a
jednu	jeden	k4xCgFnSc4	jeden
svou	svůj	k3xOyFgFnSc4	svůj
ranou	raný	k2eAgFnSc4d1	raná
triovou	triový	k2eAgFnSc4d1	triová
sonátu	sonáta	k1gFnSc4	sonáta
žertovně	žertovně	k6eAd1	žertovně
označil	označit	k5eAaPmAgMnS	označit
jako	jako	k9	jako
italskou	italský	k2eAgFnSc4d1	italská
<g/>
,	,	kIx,	,
dokud	dokud	k6eAd1	dokud
nebyla	být	k5eNaImAgFnS	být
francouzským	francouzský	k2eAgNnSc7d1	francouzské
publikem	publikum	k1gNnSc7	publikum
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
sám	sám	k3xTgMnSc1	sám
napsal	napsat	k5eAaPmAgMnS	napsat
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
přijata	přijmout	k5eAaPmNgFnS	přijmout
s	s	k7c7	s
dychtivostí	dychtivost	k1gFnSc7	dychtivost
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
však	však	k9	však
objevovat	objevovat	k5eAaImF	objevovat
jeho	jeho	k3xOp3gInSc4	jeho
vlastní	vlastní	k2eAgInSc4d1	vlastní
styl	styl	k1gInSc4	styl
skládání	skládání	k1gNnSc2	skládání
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
ně	on	k3xPp3gInPc4	on
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
Concerts	Concerts	k1gInSc1	Concerts
royaux	royaux	k1gInSc1	royaux
a	a	k8xC	a
suita	suita	k1gFnSc1	suita
nazvaná	nazvaný	k2eAgFnSc1d1	nazvaná
Les	les	k1gInSc4	les
nations	nationsa	k1gFnPc2	nationsa
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
církevní	církevní	k2eAgFnSc1d1	církevní
hudba	hudba	k1gFnSc1	hudba
je	být	k5eAaImIp3nS	být
svým	svůj	k3xOyFgInSc7	svůj
charakterem	charakter	k1gInSc7	charakter
vážnější	vážní	k2eAgInPc4d2	vážnější
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
zde	zde	k6eAd1	zde
nalezneme	nalézt	k5eAaBmIp1nP	nalézt
určitou	určitý	k2eAgFnSc4d1	určitá
přitažlivost	přitažlivost	k1gFnSc4	přitažlivost
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
jeho	jeho	k3xOp3gInSc1	jeho
Leçons	Leçons	k1gInSc1	Leçons
des	des	k1gNnSc2	des
Ténè	Ténè	k1gFnSc2	Ténè
napsaná	napsaný	k2eAgFnSc1d1	napsaná
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1713	[number]	k4	1713
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
poslední	poslední	k2eAgInSc4d1	poslední
roky	rok	k1gInPc4	rok
panování	panování	k1gNnSc2	panování
Krále	Král	k1gMnSc2	Král
slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgMnSc3	jenž
sloužil	sloužit	k5eAaImAgInS	sloužit
a	a	k8xC	a
v	v	k7c6	v
které	který	k3yRgFnSc6	který
panuje	panovat	k5eAaImIp3nS	panovat
jakási	jakýsi	k3yIgFnSc1	jakýsi
pochmurná	pochmurný	k2eAgFnSc1d1	pochmurná
melodie	melodie	k1gFnSc1	melodie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Couperin	Couperin	k1gInSc1	Couperin
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Lullym	Lullym	k1gInSc1	Lullym
a	a	k8xC	a
Rameauem	Rameauem	k1gInSc1	Rameauem
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgFnSc7d1	hlavní
postavou	postava	k1gFnSc7	postava
francouzské	francouzský	k2eAgFnSc2d1	francouzská
barokní	barokní	k2eAgFnSc2d1	barokní
hudby	hudba	k1gFnSc2	hudba
a	a	k8xC	a
jako	jako	k9	jako
takový	takový	k3xDgInSc1	takový
je	být	k5eAaImIp3nS	být
hodnocen	hodnotit	k5eAaImNgInS	hodnotit
skladateli	skladatel	k1gMnSc3	skladatel
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
přišli	přijít	k5eAaPmAgMnP	přijít
po	po	k7c6	po
něm.	něm.	k?	něm.
Například	například	k6eAd1	například
Ravelovou	Ravelův	k2eAgFnSc7d1	Ravelova
poslední	poslední	k2eAgFnSc7d1	poslední
větší	veliký	k2eAgFnSc7d2	veliký
skladbou	skladba	k1gFnSc7	skladba
pro	pro	k7c4	pro
klavír	klavír	k1gInSc4	klavír
(	(	kIx(	(
<g/>
1917	[number]	k4	1917
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
suita	suita	k1gFnSc1	suita
skládající	skládající	k2eAgFnSc1d1	skládající
se	se	k3xPyFc4	se
ze	z	k7c2	z
šesti	šest	k4xCc2	šest
vět	věta	k1gFnPc2	věta
nazvaná	nazvaný	k2eAgFnSc1d1	nazvaná
Le	Le	k1gFnSc1	Le
tombeau	tombeau	k5eAaPmIp1nS	tombeau
de	de	k?	de
Couperin	Couperin	k1gInSc1	Couperin
<g/>
,	,	kIx,	,
napsaná	napsaný	k2eAgFnSc1d1	napsaná
k	k	k7c3	k
uctění	uctění	k1gNnSc3	uctění
velké	velký	k2eAgFnSc2d1	velká
památky	památka	k1gFnSc2	památka
jeho	jeho	k3xOp3gMnSc2	jeho
předchůdce	předchůdce	k1gMnSc2	předchůdce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Média	médium	k1gNnPc1	médium
===	===	k?	===
</s>
</p>
<p>
<s>
Cinquieme	Cinquiat	k5eAaImIp1nP	Cinquiat
Prelude	Prelud	k1gInSc5	Prelud
z	z	k7c2	z
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
art	art	k?	art
de	de	k?	de
toucher	touchra	k1gFnPc2	touchra
le	le	k?	le
clavecin	clavecin	k1gInSc1	clavecin
<g/>
.	.	kIx.	.
<g/>
Cromorne	Cromorn	k1gInSc5	Cromorn
en	en	k?	en
taille	taille	k1gFnSc6	taille
<g/>
,	,	kIx,	,
messe	messe	k6eAd1	messe
des	des	k1gNnSc1	des
Paroisses	Paroisses	k1gInSc1	Paroisses
<g/>
,	,	kIx,	,
Jean-Luc	Jean-Luc	k1gInSc1	Jean-Luc
Perrot	Perrot	k1gInSc1	Perrot
<g/>
,	,	kIx,	,
orgue	orgue	k1gNnSc1	orgue
François-Henri	François-Henr	k1gFnPc1	François-Henr
Clicquot	Clicquot	k1gMnSc1	Clicquot
de	de	k?	de
Souvigny	Souvigna	k1gFnSc2	Souvigna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Fugue	Fugue	k1gFnSc1	Fugue
sur	sur	k?	sur
les	les	k1gInSc1	les
jeux	jeux	k1gInSc1	jeux
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
anches	anches	k1gInSc1	anches
<g/>
,	,	kIx,	,
Kyrie	Kyrie	k1gNnSc1	Kyrie
<g/>
,	,	kIx,	,
messe	messe	k1gFnSc1	messe
des	des	k1gNnSc2	des
Paroisses	Paroissesa	k1gFnPc2	Paroissesa
<g/>
,	,	kIx,	,
Jean-Luc	Jean-Luc	k1gFnSc1	Jean-Luc
Perrot	Perrot	k1gInSc1	Perrot
<g/>
,	,	kIx,	,
orgue	orgue	k1gNnSc1	orgue
François-Henri	François-Henr	k1gFnPc1	François-Henr
Clicquot	Clicquot	k1gMnSc1	Clicquot
de	de	k?	de
Souvigny	Souvigna	k1gFnSc2	Souvigna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Citáty	citát	k1gInPc1	citát
==	==	k?	==
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Přiznávám	přiznávat	k5eAaImIp1nS	přiznávat
v	v	k7c6	v
dobré	dobrý	k2eAgFnSc6d1	dobrá
víře	víra	k1gFnSc6	víra
<g/>
,	,	kIx,	,
že	že	k8xS	že
miluji	milovat	k5eAaImIp1nS	milovat
více	hodně	k6eAd2	hodně
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
mne	já	k3xPp1nSc4	já
dojímá	dojímat	k5eAaImIp3nS	dojímat
než	než	k8xS	než
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
mne	já	k3xPp1nSc4	já
překvapuje	překvapovat	k5eAaImIp3nS	překvapovat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
François	François	k1gInSc1	François
Couperin	Couperin	k1gInSc1	Couperin
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Životopis	životopis	k1gInSc1	životopis
</s>
</p>
<p>
<s>
Životopis	životopis	k1gInSc1	životopis
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
https://web.archive.org/web/20031025144901/http://100musique.free.fr/histoire/compostr/couper_f.htm	[url]	k6eAd1	https://web.archive.org/web/20031025144901/http://100musique.free.fr/histoire/compostr/couper_f.htm
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
skladatelů	skladatel	k1gMnPc2	skladatel
vážné	vážný	k2eAgFnSc2d1	vážná
hudby	hudba	k1gFnSc2	hudba
</s>
</p>
<p>
<s>
Barokní	barokní	k2eAgFnSc1d1	barokní
hudba	hudba	k1gFnSc1	hudba
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
François	François	k1gFnSc2	François
Couperin	Couperin	k1gInSc1	Couperin
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
François	François	k1gFnSc1	François
Couperin	Couperin	k1gInSc4	Couperin
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Volně	volně	k6eAd1	volně
přístupné	přístupný	k2eAgFnPc4d1	přístupná
partitury	partitura	k1gFnPc4	partitura
děl	dít	k5eAaBmAgMnS	dít
od	od	k7c2	od
F.	F.	kA	F.
Couperina	Couperin	k2eAgFnSc1d1	Couperin
v	v	k7c6	v
projektu	projekt	k1gInSc6	projekt
IMSLP	IMSLP	kA	IMSLP
</s>
</p>
<p>
<s>
François	François	k1gInSc1	François
Couperin	Couperin	k1gInSc1	Couperin
na	na	k7c6	na
projektu	projekt	k1gInSc6	projekt
Musopen	Musopen	k2eAgMnSc1d1	Musopen
</s>
</p>
