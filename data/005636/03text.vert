<s>
Beverly	Beverl	k1gInPc4	Beverl
Hills	Hillsa	k1gFnPc2	Hillsa
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
v	v	k7c6	v
okresu	okres	k1gInSc6	okres
Los	los	k1gInSc1	los
Angeles	Angeles	k1gInSc1	Angeles
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
světových	světový	k2eAgFnPc2d1	světová
stran	strana	k1gFnPc2	strana
obklopeno	obklopit	k5eAaPmNgNnS	obklopit
stejnojmennou	stejnojmenný	k2eAgFnSc7d1	stejnojmenná
metropolí	metropol	k1gFnSc7	metropol
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
východě	východ	k1gInSc6	východ
sousedí	sousedit	k5eAaImIp3nS	sousedit
s	s	k7c7	s
městem	město	k1gNnSc7	město
West	West	k2eAgInSc1d1	West
Hollywood	Hollywood	k1gInSc1	Hollywood
a	a	k8xC	a
losangeleskou	losangeleský	k2eAgFnSc7d1	losangeleská
čtvrtí	čtvrt	k1gFnSc7	čtvrt
Beverly	Beverla	k1gFnSc2	Beverla
Grove	Groev	k1gFnSc2	Groev
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
se	se	k3xPyFc4	se
čtvrtí	čtvrtit	k5eAaImIp3nS	čtvrtit
Pico-Robertson	Pico-Robertson	k1gInSc1	Pico-Robertson
<g/>
,	,	kIx,	,
na	na	k7c6	na
západě	západ	k1gInSc6	západ
s	s	k7c7	s
Westwood	Westwooda	k1gFnPc2	Westwooda
a	a	k8xC	a
na	na	k7c6	na
severu	sever	k1gInSc6	sever
s	s	k7c7	s
Beverly	Beverlo	k1gNnPc7	Beverlo
Crest	Crest	k1gMnSc1	Crest
a	a	k8xC	a
Hollywood	Hollywood	k1gInSc1	Hollywood
Hills	Hillsa	k1gFnPc2	Hillsa
West	Westa	k1gFnPc2	Westa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
tehdy	tehdy	k6eAd1	tehdy
stála	stát	k5eAaImAgFnS	stát
fazolová	fazolový	k2eAgFnSc1d1	fazolová
farma	farma	k1gFnSc1	farma
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1914	[number]	k4	1914
skupina	skupina	k1gFnSc1	skupina
investorů	investor	k1gMnPc2	investor
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
nalézt	nalézt	k5eAaBmF	nalézt
ropu	ropa	k1gFnSc4	ropa
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
zde	zde	k6eAd1	zde
žádnou	žádný	k3yNgFnSc4	žádný
ropu	ropa	k1gFnSc4	ropa
nenašli	najít	k5eNaPmAgMnP	najít
<g/>
,	,	kIx,	,
nalezli	naleznout	k5eAaPmAgMnP	naleznout
zde	zde	k6eAd1	zde
vodní	vodní	k2eAgInPc4d1	vodní
prameny	pramen	k1gInPc4	pramen
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c6	na
nich	on	k3xPp3gInPc6	on
se	se	k3xPyFc4	se
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
vybudovat	vybudovat	k5eAaPmF	vybudovat
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
ve	v	k7c6	v
městě	město	k1gNnSc6	město
žilo	žít	k5eAaImAgNnS	žít
přes	přes	k7c4	přes
34	[number]	k4	34
500	[number]	k4	500
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
rozloha	rozloha	k1gFnSc1	rozloha
Beverly	Beverla	k1gFnSc2	Beverla
Hills	Hillsa	k1gFnPc2	Hillsa
je	být	k5eAaImIp3nS	být
14,8	[number]	k4	14,8
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
má	mít	k5eAaImIp3nS	mít
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nejznámější	známý	k2eAgFnSc4d3	nejznámější
poštovní	poštovní	k2eAgFnSc4d1	poštovní
adresu	adresa	k1gFnSc4	adresa
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
;	;	kIx,	;
90210	[number]	k4	90210
<g/>
.	.	kIx.	.
</s>
<s>
Beverly	Beverla	k1gMnSc2	Beverla
Hills	Hills	k1gInSc1	Hills
je	být	k5eAaImIp3nS	být
také	také	k9	také
domovem	domov	k1gInSc7	domov
mnoha	mnoho	k4c2	mnoho
celebrit	celebrita	k1gFnPc2	celebrita
<g/>
.	.	kIx.	.
</s>
<s>
Beverly	Beverla	k1gFnPc1	Beverla
Hills	Hillsa	k1gFnPc2	Hillsa
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
městem	město	k1gNnSc7	město
West	West	k2eAgInSc1d1	West
Hollywood	Hollywood	k1gInSc1	Hollywood
a	a	k8xC	a
dvěma	dva	k4xCgFnPc7	dva
čtvrtěmi	čtvrt	k1gFnPc7	čtvrt
Los	los	k1gMnSc1	los
Angeles	Angeles	k1gMnSc1	Angeles
(	(	kIx(	(
<g/>
Bel-Air	Bel-Air	k1gMnSc1	Bel-Air
a	a	k8xC	a
Holmby	Holmba	k1gFnPc1	Holmba
Hills	Hills	k1gInSc1	Hills
<g/>
)	)	kIx)	)
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
"	"	kIx"	"
<g/>
Platinum	Platinum	k1gInSc1	Platinum
Triangle	triangl	k1gInSc5	triangl
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
jedné	jeden	k4xCgFnSc3	jeden
z	z	k7c2	z
nejbohatších	bohatý	k2eAgFnPc2d3	nejbohatší
oblastí	oblast	k1gFnPc2	oblast
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
celých	celá	k1gFnPc2	celá
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
Evropan	Evropan	k1gMnSc1	Evropan
zavítal	zavítat	k5eAaPmAgMnS	zavítat
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
oblasti	oblast	k1gFnSc2	oblast
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
roku	rok	k1gInSc2	rok
1769	[number]	k4	1769
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
téměř	téměř	k6eAd1	téměř
50	[number]	k4	50
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
usadila	usadit	k5eAaPmAgFnS	usadit
Maria	Maria	k1gFnSc1	Maria
Rita	rito	k1gNnSc2	rito
Quinteros	Quinterosa	k1gFnPc2	Quinterosa
de	de	k?	de
Valdez	Valdez	k1gMnSc1	Valdez
s	s	k7c7	s
manželem	manžel	k1gMnSc7	manžel
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
pozemek	pozemek	k1gInSc4	pozemek
o	o	k7c6	o
výměře	výměra	k1gFnSc6	výměra
18	[number]	k4	18
km	km	kA	km
<g/>
2	[number]	k4	2
nazvali	nazvat	k5eAaBmAgMnP	nazvat
Rancho	Rancha	k1gFnSc5	Rancha
Rodeo	rodeo	k1gNnSc1	rodeo
de	de	k?	de
las	laso	k1gNnPc2	laso
Augas	Augasa	k1gFnPc2	Augasa
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
Ranč	ranč	k1gFnSc1	ranč
rodea	rodeo	k1gNnSc2	rodeo
na	na	k7c6	na
vodě	voda	k1gFnSc6	voda
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1854	[number]	k4	1854
byl	být	k5eAaImAgInS	být
ranč	ranč	k1gInSc1	ranč
prodán	prodat	k5eAaPmNgInS	prodat
druhému	druhý	k4xOgInSc3	druhý
starostovi	starosta	k1gMnSc3	starosta
Los	los	k1gMnSc1	los
Angeles	Angeles	k1gMnSc1	Angeles
<g/>
,	,	kIx,	,
Benjaminu	Benjamin	k1gMnSc3	Benjamin
Davisu	Davis	k1gInSc2	Davis
Wilsonovi	Wilson	k1gMnSc3	Wilson
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
prodeji	prodej	k1gInSc6	prodej
byl	být	k5eAaImAgMnS	být
celý	celý	k2eAgInSc4d1	celý
pozemek	pozemek	k1gInSc4	pozemek
rozporcován	rozporcován	k2eAgInSc4d1	rozporcován
na	na	k7c4	na
menší	malý	k2eAgFnPc4d2	menší
parcely	parcela	k1gFnPc4	parcela
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
byla	být	k5eAaImAgFnS	být
o	o	k7c6	o
výměře	výměra	k1gFnSc6	výměra
0,30	[number]	k4	0,30
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
skoupili	skoupit	k5eAaImAgMnP	skoupit
obchodní	obchodní	k2eAgMnPc1d1	obchodní
partneři	partner	k1gMnPc1	partner
Henry	Henry	k1gMnSc1	Henry
Hammel	Hammel	k1gMnSc1	Hammel
<g/>
,	,	kIx,	,
a	a	k8xC	a
Andrew	Andrew	k1gMnSc1	Andrew
H.	H.	kA	H.
Denker	Denker	k1gMnSc1	Denker
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
těch	ten	k3xDgInPc6	ten
pak	pak	k6eAd1	pak
ve	v	k7c6	v
velkém	velký	k2eAgInSc6d1	velký
pěstovalifazole	pěstovalifazol	k1gInSc6	pěstovalifazol
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
farma	farma	k1gFnSc1	farma
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
nazývala	nazývat	k5eAaImAgFnS	nazývat
Hammel	Hammel	k1gInSc4	Hammel
and	and	k?	and
Denker	Denker	k1gInSc1	Denker
Ranch	Ranch	k1gInSc1	Ranch
<g/>
,	,	kIx,	,
a	a	k8xC	a
zajímavostí	zajímavost	k1gFnSc7	zajímavost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
již	již	k9	již
roku	rok	k1gInSc2	rok
1888	[number]	k4	1888
zde	zde	k6eAd1	zde
plánovali	plánovat	k5eAaImAgMnP	plánovat
vybudovat	vybudovat	k5eAaPmF	vybudovat
město	město	k1gNnSc4	město
které	který	k3yRgFnPc4	který
se	se	k3xPyFc4	se
mělo	mít	k5eAaImAgNnS	mít
jmenovat	jmenovat	k5eAaBmF	jmenovat
"	"	kIx"	"
<g/>
Morocco	Morocco	k6eAd1	Morocco
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1900	[number]	k4	1900
skupina	skupina	k1gFnSc1	skupina
devíti	devět	k4xCc2	devět
obchodníků	obchodník	k1gMnPc2	obchodník
založila	založit	k5eAaPmAgFnS	založit
těžební	těžební	k2eAgFnSc1d1	těžební
společnost	společnost	k1gFnSc1	společnost
Amalgamet	Amalgameta	k1gFnPc2	Amalgameta
Oil	Oil	k1gFnSc2	Oil
Company	Compana	k1gFnSc2	Compana
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
skupina	skupina	k1gFnSc1	skupina
koupila	koupit	k5eAaPmAgFnS	koupit
Hammel	Hammel	k1gInSc4	Hammel
and	and	k?	and
Danker	Danker	k1gInSc1	Danker
Ranch	Ranch	k1gInSc1	Ranch
<g/>
,	,	kIx,	,
a	a	k8xC	a
začala	začít	k5eAaPmAgFnS	začít
zde	zde	k6eAd1	zde
hledat	hledat	k5eAaImF	hledat
ropu	ropa	k1gFnSc4	ropa
<g/>
.	.	kIx.	.
</s>
<s>
Bohužel	bohužel	k9	bohužel
ji	on	k3xPp3gFnSc4	on
zde	zde	k6eAd1	zde
nenašli	najít	k5eNaPmAgMnP	najít
tolik	tolik	k6eAd1	tolik
<g/>
,	,	kIx,	,
kolik	kolik	k9	kolik
by	by	kYmCp3nP	by
potřebovali	potřebovat	k5eAaImAgMnP	potřebovat
na	na	k7c4	na
úspěšné	úspěšný	k2eAgNnSc4d1	úspěšné
komerční	komerční	k2eAgNnSc4d1	komerční
využití	využití	k1gNnSc4	využití
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
roku	rok	k1gInSc2	rok
1906	[number]	k4	1906
zoragnizovali	zoragnizovat	k5eAaBmAgMnP	zoragnizovat
společnost	společnost	k1gFnSc4	společnost
Rodeo	rodeo	k1gNnSc1	rodeo
Land	Land	k1gMnSc1	Land
and	and	k?	and
Water	Water	k1gMnSc1	Water
Company	Compana	k1gFnSc2	Compana
<g/>
.	.	kIx.	.
</s>
<s>
Celou	celý	k2eAgFnSc4d1	celá
oblast	oblast	k1gFnSc4	oblast
přejmenovali	přejmenovat	k5eAaPmAgMnP	přejmenovat
na	na	k7c4	na
Beverly	Beverla	k1gFnPc4	Beverla
Hills	Hillsa	k1gFnPc2	Hillsa
<g/>
,	,	kIx,	,
rozkouskovali	rozkouskovat	k5eAaPmAgMnP	rozkouskovat
na	na	k7c4	na
malé	malý	k2eAgFnPc4d1	malá
parcely	parcela	k1gFnPc4	parcela
<g/>
,	,	kIx,	,
a	a	k8xC	a
následně	následně	k6eAd1	následně
je	on	k3xPp3gNnPc4	on
začali	začít	k5eAaPmAgMnP	začít
prodávat	prodávat	k5eAaImF	prodávat
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
část	část	k1gFnSc1	část
z	z	k7c2	z
jména	jméno	k1gNnSc2	jméno
byla	být	k5eAaImAgFnS	být
zvolena	zvolit	k5eAaPmNgFnS	zvolit
po	po	k7c6	po
stejnojmenném	stejnojmenný	k2eAgNnSc6d1	stejnojmenné
městě	město	k1gNnSc6	město
v	v	k7c6	v
Massachusetts	Massachusetts	k1gNnSc6	Massachusetts
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
druhá	druhý	k4xOgFnSc1	druhý
pak	pak	k6eAd1	pak
po	po	k7c6	po
blízkých	blízký	k2eAgInPc6d1	blízký
kopcích	kopec	k1gInPc6	kopec
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
budova	budova	k1gFnSc1	budova
tu	tu	k6eAd1	tu
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
parcel	parcela	k1gFnPc2	parcela
stála	stát	k5eAaImAgFnS	stát
roku	rok	k1gInSc2	rok
1907	[number]	k4	1907
<g/>
.	.	kIx.	.
</s>
<s>
Beverly	Beverla	k1gFnSc2	Beverla
Hills	Hillsa	k1gFnPc2	Hillsa
byla	být	k5eAaImAgFnS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
tehdy	tehdy	k6eAd1	tehdy
budovaných	budovaný	k2eAgFnPc2d1	budovaná
osad	osada	k1gFnPc2	osada
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
směli	smět	k5eAaImAgMnP	smět
žít	žít	k5eAaImF	žít
pouze	pouze	k6eAd1	pouze
lidé	člověk	k1gMnPc1	člověk
bílé	bílý	k2eAgFnSc2d1	bílá
pleti	pleť	k1gFnSc2	pleť
<g/>
.	.	kIx.	.
</s>
<s>
Omezující	omezující	k2eAgFnSc1d1	omezující
smlouva	smlouva	k1gFnSc1	smlouva
zakazovala	zakazovat	k5eAaImAgFnS	zakazovat
vlastnictví	vlastnictví	k1gNnSc4	vlastnictví
jakékoliv	jakýkoliv	k3yIgFnSc2	jakýkoliv
nemovitosti	nemovitost	k1gFnSc2	nemovitost
na	na	k7c6	na
místních	místní	k2eAgInPc6d1	místní
pozemcích	pozemek	k1gInPc6	pozemek
lidem	člověk	k1gMnPc3	člověk
jiné	jiný	k2eAgFnSc2d1	jiná
než	než	k8xS	než
bílé	bílý	k2eAgFnSc2d1	bílá
pleti	pleť	k1gFnSc2	pleť
<g/>
.	.	kIx.	.
</s>
<s>
Taktéž	Taktéž	k?	Taktéž
bylo	být	k5eAaImAgNnS	být
zakázáno	zakázán	k2eAgNnSc1d1	zakázáno
prodávat	prodávat	k5eAaImF	prodávat
dům	dům	k1gInSc4	dům
či	či	k8xC	či
parcelu	parcela	k1gFnSc4	parcela
židům	žid	k1gMnPc3	žid
<g/>
.	.	kIx.	.
</s>
<s>
Podnikatel	podnikatel	k1gMnSc1	podnikatel
Burton	Burton	k1gInSc4	Burton
Green	Green	k1gInSc1	Green
začal	začít	k5eAaPmAgInS	začít
s	s	k7c7	s
konstrukcí	konstrukce	k1gFnSc7	konstrukce
The	The	k1gFnSc2	The
Beverly	Beverla	k1gFnSc2	Beverla
Hills	Hillsa	k1gFnPc2	Hillsa
Hotel	hotel	k1gInSc1	hotel
roku	rok	k1gInSc2	rok
1911	[number]	k4	1911
<g/>
.	.	kIx.	.
</s>
<s>
Dokončen	dokončen	k2eAgInSc1d1	dokončen
byl	být	k5eAaImAgInS	být
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Dokončením	dokončení	k1gNnSc7	dokončení
hotelu	hotel	k1gInSc2	hotel
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
tou	ten	k3xDgFnSc7	ten
dobou	doba	k1gFnSc7	doba
již	již	k6eAd1	již
mírně	mírně	k6eAd1	mírně
skomírající	skomírající	k2eAgInPc1d1	skomírající
prodeje	prodej	k1gInPc1	prodej
parcel	parcela	k1gFnPc2	parcela
a	a	k8xC	a
nemovitostí	nemovitost	k1gFnPc2	nemovitost
<g/>
,	,	kIx,	,
opět	opět	k6eAd1	opět
nastartovaly	nastartovat	k5eAaPmAgFnP	nastartovat
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
roku	rok	k1gInSc2	rok
1914	[number]	k4	1914
mělo	mít	k5eAaImAgNnS	mít
Beverly	Beverla	k1gFnSc2	Beverla
Hills	Hills	k1gInSc4	Hills
dost	dost	k6eAd1	dost
obyvatel	obyvatel	k1gMnSc1	obyvatel
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
z	z	k7c2	z
osady	osada	k1gFnSc2	osada
stálo	stát	k5eAaImAgNnS	stát
samosprávné	samosprávný	k2eAgNnSc1d1	samosprávné
město	město	k1gNnSc1	město
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
vlastníci	vlastník	k1gMnPc1	vlastník
společnosti	společnost	k1gFnSc2	společnost
Rodeo	rodeo	k1gNnSc1	rodeo
Water	Water	k1gInSc1	Water
and	and	k?	and
Land	Land	k1gInSc4	Land
Company	Compana	k1gFnSc2	Compana
oddělit	oddělit	k5eAaPmF	oddělit
obchod	obchod	k1gInSc4	obchod
s	s	k7c7	s
pozemky	pozemek	k1gInPc7	pozemek
od	od	k7c2	od
obchodování	obchodování	k1gNnSc2	obchodování
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
se	se	k3xPyFc4	se
na	na	k7c4	na
Summit	summit	k1gInSc4	summit
Drive	drive	k1gInSc4	drive
začala	začít	k5eAaPmAgFnS	začít
stavět	stavět	k5eAaImF	stavět
luxusní	luxusní	k2eAgFnSc1d1	luxusní
vila	vila	k1gFnSc1	vila
Douglase	Douglasa	k1gFnSc3	Douglasa
Fairbankse	Fairbankse	k1gFnSc2	Fairbankse
a	a	k8xC	a
Mary	Mary	k1gFnSc2	Mary
Pickford	Pickforda	k1gFnPc2	Pickforda
<g/>
.	.	kIx.	.
</s>
<s>
Dokončena	dokončen	k2eAgFnSc1d1	dokončena
byla	být	k5eAaImAgFnS	být
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
a	a	k8xC	a
od	od	k7c2	od
novinářů	novinář	k1gMnPc2	novinář
dostala	dostat	k5eAaPmAgFnS	dostat
přezdívku	přezdívka	k1gFnSc4	přezdívka
"	"	kIx"	"
<g/>
Pickfair	Pickfair	k1gInSc4	Pickfair
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
vzrostla	vzrůst	k5eAaPmAgFnS	vzrůst
populace	populace	k1gFnSc1	populace
Beverly	Beverla	k1gFnSc2	Beverla
Hills	Hillsa	k1gFnPc2	Hillsa
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
zásoby	zásoba	k1gFnSc2	zásoba
vody	voda	k1gFnSc2	voda
stali	stát	k5eAaPmAgMnP	stát
politickou	politický	k2eAgFnSc7d1	politická
záležitostí	záležitost	k1gFnSc7	záležitost
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1923	[number]	k4	1923
se	se	k3xPyFc4	se
naskytlo	naskytnout	k5eAaPmAgNnS	naskytnout
řešení	řešení	k1gNnSc1	řešení
<g/>
,	,	kIx,	,
a	a	k8xC	a
sice	sice	k8xC	sice
připojení	připojení	k1gNnSc4	připojení
k	k	k7c3	k
městu	město	k1gNnSc3	město
Los	los	k1gInSc1	los
Angeles	Angeles	k1gInSc1	Angeles
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
připojení	připojení	k1gNnSc3	připojení
byly	být	k5eAaImAgFnP	být
jak	jak	k8xC	jak
slavní	slavný	k2eAgMnPc1d1	slavný
herci	herec	k1gMnPc1	herec
<g/>
,	,	kIx,	,
jež	jenž	k3xRgMnPc4	jenž
tu	tu	k6eAd1	tu
měli	mít	k5eAaImAgMnP	mít
své	svůj	k3xOyFgFnPc4	svůj
vily	vila	k1gFnPc4	vila
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
městská	městský	k2eAgFnSc1d1	městská
rada	rada	k1gFnSc1	rada
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1928	[number]	k4	1928
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
městě	město	k1gNnSc6	město
otevřen	otevřen	k2eAgInSc1d1	otevřen
hotel	hotel	k1gInSc1	hotel
Beverly	Beverla	k1gFnSc2	Beverla
Wilshire	Wilshir	k1gInSc5	Wilshir
Apartment	Apartment	k1gMnSc1	Apartment
Hotel	hotel	k1gInSc1	hotel
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
Beverly	Beverla	k1gFnSc2	Beverla
Wilshire	Wilshir	k1gInSc5	Wilshir
Hotel	hotel	k1gInSc1	hotel
<g/>
)	)	kIx)	)
na	na	k7c4	na
Wilshire	Wilshir	k1gInSc5	Wilshir
Boulevard	Boulevarda	k1gFnPc2	Boulevarda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1931	[number]	k4	1931
byla	být	k5eAaImAgFnS	být
otevřena	otevřít	k5eAaPmNgFnS	otevřít
současná	současný	k2eAgFnSc1d1	současná
městská	městský	k2eAgFnSc1d1	městská
radnice	radnice	k1gFnSc1	radnice
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
40	[number]	k4	40
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
mnozí	mnohý	k2eAgMnPc1d1	mnohý
černí	černý	k2eAgMnPc1d1	černý
herci	herec	k1gMnPc1	herec
a	a	k8xC	a
podnikatelé	podnikatel	k1gMnPc1	podnikatel
navzdory	navzdory	k7c3	navzdory
zákazu	zákaz	k1gInSc3	zákaz
začali	začít	k5eAaPmAgMnP	začít
stěhovat	stěhovat	k5eAaImF	stěhovat
do	do	k7c2	do
Beverly	Beverla	k1gFnSc2	Beverla
Hills	Hillsa	k1gFnPc2	Hillsa
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
místní	místní	k2eAgFnSc4d1	místní
bílé	bílý	k2eAgNnSc1d1	bílé
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
dožadovalo	dožadovat	k5eAaImAgNnS	dožadovat
dodržování	dodržování	k1gNnSc4	dodržování
smlouvy	smlouva	k1gFnSc2	smlouva
u	u	k7c2	u
soudu	soud	k1gInSc2	soud
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
obžalovanými	obžalovaný	k2eAgMnPc7d1	obžalovaný
byly	být	k5eAaImAgFnP	být
mimo	mimo	k6eAd1	mimo
jiných	jiný	k2eAgNnPc2d1	jiné
také	také	k9	také
černí	černý	k2eAgMnPc1d1	černý
herci	herec	k1gMnPc1	herec
Hattie	Hattie	k1gFnSc2	Hattie
McDaniel	McDaniel	k1gInSc1	McDaniel
<g/>
,	,	kIx,	,
Louise	Louis	k1gMnPc4	Louis
Beavers	Beaversa	k1gFnPc2	Beaversa
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Ethel	Ethel	k1gInSc1	Ethel
Waters	Watersa	k1gFnPc2	Watersa
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgFnSc1d1	národní
asociace	asociace	k1gFnSc1	asociace
pro	pro	k7c4	pro
rovnoprávnost	rovnoprávnost	k1gFnSc4	rovnoprávnost
barevných	barevný	k2eAgMnPc2d1	barevný
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
National	National	k1gFnSc1	National
Association	Association	k1gInSc1	Association
for	forum	k1gNnPc2	forum
the	the	k?	the
Advancement	Advancement	k1gMnSc1	Advancement
of	of	k?	of
Colored	Colored	k1gMnSc1	Colored
People	People	k1gMnSc1	People
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
postavila	postavit	k5eAaPmAgFnS	postavit
na	na	k7c4	na
obranu	obrana	k1gFnSc4	obrana
obžalovaných	obžalovaný	k1gMnPc2	obžalovaný
<g/>
,	,	kIx,	,
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
o	o	k7c6	o
nevymahatelnosti	nevymahatelnost	k1gFnSc6	nevymahatelnost
a	a	k8xC	a
neplatnosti	neplatnost	k1gFnSc6	neplatnost
městských	městský	k2eAgFnPc2d1	městská
smluv	smlouva	k1gFnPc2	smlouva
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
zvýhodňují	zvýhodňovat	k5eAaImIp3nP	zvýhodňovat
bílí	bílý	k2eAgMnPc1d1	bílý
obyvatelé	obyvatel	k1gMnPc1	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
proces	proces	k1gInSc1	proces
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
Shelley	Shellea	k1gFnSc2	Shellea
vs	vs	k?	vs
<g/>
.	.	kIx.	.
</s>
<s>
Kraemer	Kraemer	k1gInSc1	Kraemer
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
krátkosti	krátkost	k1gFnSc6	krátkost
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
afroamerická	afroamerický	k2eAgFnSc1d1	afroamerická
rodina	rodina	k1gFnSc1	rodina
jménem	jméno	k1gNnSc7	jméno
Shelley	Shellea	k1gFnSc2	Shellea
se	se	k3xPyFc4	se
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
koupit	koupit	k5eAaPmF	koupit
dům	dům	k1gInSc4	dům
v	v	k7c6	v
St.	st.	kA	st.
Louis	Louis	k1gMnSc1	Louis
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
městě	město	k1gNnSc6	město
tehdy	tehdy	k6eAd1	tehdy
také	také	k9	také
platila	platit	k5eAaImAgFnS	platit
smlouva	smlouva	k1gFnSc1	smlouva
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
znemožňovala	znemožňovat	k5eAaImAgFnS	znemožňovat
vlastnictví	vlastnictví	k1gNnSc4	vlastnictví
půdy	půda	k1gFnSc2	půda
osobám	osoba	k1gFnPc3	osoba
černé	černý	k2eAgFnSc2d1	černá
pleti	pleť	k1gFnSc2	pleť
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
afroamerická	afroamerický	k2eAgFnSc1d1	afroamerická
rodina	rodina	k1gFnSc1	rodina
se	se	k3xPyFc4	se
soudila	soudit	k5eAaImAgFnS	soudit
s	s	k7c7	s
mužem	muž	k1gMnSc7	muž
jménem	jméno	k1gNnSc7	jméno
Kraemer	Kraemer	k1gMnSc1	Kraemer
<g/>
.	.	kIx.	.
</s>
<s>
Soud	soud	k1gInSc1	soud
zde	zde	k6eAd1	zde
dospěl	dochvít	k5eAaPmAgInS	dochvít
k	k	k7c3	k
názoru	názor	k1gInSc3	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
smlouva	smlouva	k1gFnSc1	smlouva
je	být	k5eAaImIp3nS	být
čistě	čistě	k6eAd1	čistě
soukromé	soukromý	k2eAgNnSc4d1	soukromé
ujednání	ujednání	k1gNnSc4	ujednání
původních	původní	k2eAgMnPc2d1	původní
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1956	[number]	k4	1956
koupil	koupit	k5eAaPmAgMnS	koupit
podnikatel	podnikatel	k1gMnSc1	podnikatel
Paul	Paul	k1gMnSc1	Paul
Trousdale	Trousdala	k1gFnSc6	Trousdala
půdu	půda	k1gFnSc4	půda
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Doheny	Dohena	k1gFnSc2	Dohena
Ranch	Rancha	k1gFnPc2	Rancha
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
pár	pár	k4xCyI	pár
let	léto	k1gNnPc2	léto
tu	tu	k6eAd1	tu
vybudoval	vybudovat	k5eAaPmAgInS	vybudovat
komplex	komplex	k1gInSc4	komplex
Trousdale	Trousdala	k1gFnSc3	Trousdala
Estates	Estates	k1gInSc1	Estates
<g/>
,	,	kIx,	,
a	a	k8xC	a
přesvědčil	přesvědčit	k5eAaPmAgInS	přesvědčit
městskou	městský	k2eAgFnSc4d1	městská
radu	rada	k1gFnSc4	rada
Beverly	Beverla	k1gFnSc2	Beverla
Hills	Hills	k1gInSc4	Hills
o	o	k7c4	o
připojení	připojení	k1gNnSc4	připojení
svého	svůj	k3xOyFgInSc2	svůj
komplexu	komplex	k1gInSc2	komplex
k	k	k7c3	k
městu	město	k1gNnSc3	město
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
residenty	resident	k1gMnPc4	resident
tohoto	tento	k3xDgInSc2	tento
kompelxu	kompelx	k1gInSc2	kompelx
svého	svůj	k3xOyFgInSc2	svůj
času	čas	k1gInSc2	čas
patřili	patřit	k5eAaImAgMnP	patřit
taková	takový	k3xDgNnPc4	takový
jména	jméno	k1gNnPc4	jméno
jako	jako	k8xC	jako
Elvis	Elvis	k1gMnSc1	Elvis
Presley	Preslea	k1gFnSc2	Preslea
<g/>
,	,	kIx,	,
Frank	Frank	k1gMnSc1	Frank
Sinatra	Sinatrum	k1gNnSc2	Sinatrum
<g/>
,	,	kIx,	,
Dean	Dean	k1gMnSc1	Dean
Martin	Martin	k1gMnSc1	Martin
<g/>
,	,	kIx,	,
Ray	Ray	k1gMnSc1	Ray
Charles	Charles	k1gMnSc1	Charles
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Richard	Richard	k1gMnSc1	Richard
M.	M.	kA	M.
Nixon	Nixon	k1gMnSc1	Nixon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
návrh	návrh	k1gInSc1	návrh
rozšířit	rozšířit	k5eAaPmF	rozšířit
červenou	červený	k2eAgFnSc4d1	červená
linku	linka	k1gFnSc4	linka
metra	metro	k1gNnSc2	metro
podél	podél	k7c2	podél
Wilshire	Wilshir	k1gInSc5	Wilshir
Boulevard	Boulevard	k1gInSc4	Boulevard
hlouběji	hluboko	k6eAd2	hluboko
do	do	k7c2	do
centra	centrum	k1gNnSc2	centrum
Beverly	Beverla	k1gFnSc2	Beverla
Hills	Hillsa	k1gFnPc2	Hillsa
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
město	město	k1gNnSc1	město
tento	tento	k3xDgInSc4	tento
návrh	návrh	k1gInSc4	návrh
nepodpořilo	podpořit	k5eNaPmAgNnS	podpořit
<g/>
.	.	kIx.	.
</s>
<s>
Beverly	Beverla	k1gFnPc1	Beverla
Hills	Hillsa	k1gFnPc2	Hillsa
<g/>
,	,	kIx,	,
a	a	k8xC	a
přilehlé	přilehlý	k2eAgNnSc4d1	přilehlé
město	město	k1gNnSc4	město
West	West	k2eAgInSc1d1	West
Hollywood	Hollywood	k1gInSc1	Hollywood
jsou	být	k5eAaImIp3nP	být
společně	společně	k6eAd1	společně
obklopeny	obklopit	k5eAaPmNgFnP	obklopit
městem	město	k1gNnSc7	město
Los	los	k1gInSc1	los
Angeles	Angeles	k1gInSc1	Angeles
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
nížině	nížina	k1gFnSc6	nížina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
umístěná	umístěný	k2eAgFnSc1d1	umístěná
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Domy	dům	k1gInPc1	dům
postavené	postavený	k2eAgInPc1d1	postavený
v	v	k7c6	v
kopcích	kopec	k1gInPc6	kopec
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
města	město	k1gNnSc2	město
jsou	být	k5eAaImIp3nP	být
podstatně	podstatně	k6eAd1	podstatně
dražší	drahý	k2eAgInPc1d2	dražší
<g/>
,	,	kIx,	,
než	než	k8xS	než
domy	dům	k1gInPc1	dům
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
nížině	nížina	k1gFnSc6	nížina
<g/>
.	.	kIx.	.
</s>
<s>
Santa	Santa	k1gMnSc1	Santa
Monica	Monica	k1gMnSc1	Monica
Boulevard	Boulevard	k1gMnSc1	Boulevard
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
nížinu	nížina	k1gFnSc4	nížina
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
<g/>
;	;	kIx,	;
jižní	jižní	k2eAgFnSc2d1	jižní
a	a	k8xC	a
severní	severní	k2eAgFnSc2d1	severní
koleje	kolej	k1gFnSc2	kolej
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
název	název	k1gInSc1	název
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
staré	starý	k2eAgFnPc4d1	stará
tramvajové	tramvajový	k2eAgFnPc4d1	tramvajová
koleje	kolej	k1gFnPc4	kolej
společnosti	společnost	k1gFnSc2	společnost
Pacific	Pacifice	k1gFnPc2	Pacifice
Electric	Electrice	k1gFnPc2	Electrice
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
kdysi	kdysi	k6eAd1	kdysi
velmi	velmi	k6eAd1	velmi
vytížené	vytížený	k2eAgNnSc1d1	vytížené
<g/>
,	,	kIx,	,
a	a	k8xC	a
dopravovali	dopravovat	k5eAaImAgMnP	dopravovat
osoby	osoba	k1gFnPc4	osoba
z	z	k7c2	z
centra	centrum	k1gNnSc2	centrum
Los	los	k1gMnSc1	los
Angeles	Angeles	k1gMnSc1	Angeles
až	až	k9	až
k	k	k7c3	k
plážím	pláž	k1gFnPc3	pláž
na	na	k7c6	na
Santa	Sant	k1gMnSc2	Sant
Monice	Monika	k1gFnSc6	Monika
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
všechny	všechen	k3xTgInPc1	všechen
obchody	obchod	k1gInPc1	obchod
a	a	k8xC	a
úřady	úřad	k1gInPc1	úřad
jsou	být	k5eAaImIp3nP	být
v	v	k7c4	v
Beverly	Beverl	k1gInPc4	Beverl
Hills	Hillsa	k1gFnPc2	Hillsa
soustředěny	soustředit	k5eAaPmNgInP	soustředit
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Santa	Sant	k1gInSc2	Sant
Monica	Monica	k1gMnSc1	Monica
Boulevard	Boulevard	k1gMnSc1	Boulevard
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
dvě	dva	k4xCgFnPc4	dva
známé	známý	k2eAgFnPc4d1	známá
stavby	stavba	k1gFnPc4	stavba
města	město	k1gNnSc2	město
<g/>
;	;	kIx,	;
Beverly	Beverla	k1gFnSc2	Beverla
Hills	Hills	k1gInSc1	Hills
Hilton	Hilton	k1gInSc1	Hilton
a	a	k8xC	a
Beverly	Beverla	k1gFnSc2	Beverla
Hills	Hillsa	k1gFnPc2	Hillsa
Hotel	hotel	k1gInSc1	hotel
<g/>
.	.	kIx.	.
</s>
<s>
Centrum	centrum	k1gNnSc1	centrum
Beverly	Beverla	k1gFnSc2	Beverla
Hills	Hillsa	k1gFnPc2	Hillsa
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
ulice	ulice	k1gFnSc2	ulice
Rodeo	rodeo	k1gNnSc1	rodeo
Drive	drive	k1gInSc4	drive
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
vymezené	vymezený	k2eAgFnPc4d1	vymezená
Santa	Santo	k1gNnPc4	Santo
Monica	Monicus	k1gMnSc2	Monicus
Boulevard	Boulevard	k1gInSc1	Boulevard
a	a	k8xC	a
Wilshire	Wilshir	k1gInSc5	Wilshir
Boulevard	Boulevard	k1gMnSc1	Boulevard
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
části	část	k1gFnSc6	část
Beverly	Beverla	k1gFnSc2	Beverla
Hills	Hillsa	k1gFnPc2	Hillsa
<g/>
,	,	kIx,	,
na	na	k7c4	na
Santa	Sant	k1gMnSc4	Sant
Monica	Monic	k1gInSc2	Monic
Boulevard	Boulevard	k1gInSc1	Boulevard
<g/>
,	,	kIx,	,
stojí	stát	k5eAaImIp3nS	stát
budova	budova	k1gFnSc1	budova
radnice	radnice	k1gFnSc2	radnice
Beverly	Beverla	k1gFnSc2	Beverla
Hills	Hillsa	k1gFnPc2	Hillsa
<g/>
.	.	kIx.	.
</s>
<s>
Domy	dům	k1gInPc4	dům
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Wilshire	Wilshir	k1gInSc5	Wilshir
bulváru	bulvár	k1gInSc3	bulvár
mají	mít	k5eAaImIp3nP	mít
typickou	typický	k2eAgFnSc4d1	typická
městskou	městský	k2eAgFnSc4d1	městská
architekturu	architektura	k1gFnSc4	architektura
a	a	k8xC	a
bývají	bývat	k5eAaImIp3nP	bývat
většinou	většinou	k6eAd1	většinou
menší	malý	k2eAgNnPc1d2	menší
než	než	k8xS	než
ty	ten	k3xDgFnPc4	ten
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
apartmánů	apartmán	k1gInPc2	apartmán
nebo	nebo	k8xC	nebo
bytů	byt	k1gInPc2	byt
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
také	také	k9	také
oblast	oblast	k1gFnSc1	oblast
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc2	jenž
jsou	být	k5eAaImIp3nP	být
ceny	cena	k1gFnPc1	cena
domů	dům	k1gInPc2	dům
a	a	k8xC	a
bytů	byt	k1gInPc2	byt
nejlevnější	levný	k2eAgFnSc1d3	nejlevnější
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
sídlo	sídlo	k1gNnSc4	sídlo
v	v	k7c4	v
Beverly	Beverla	k1gFnPc4	Beverla
Hills	Hillsa	k1gFnPc2	Hillsa
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
mají	mít	k5eAaImIp3nP	mít
filmová	filmový	k2eAgNnPc4d1	filmové
studia	studio	k1gNnPc4	studio
Metro-Goldwyn-Mayer	Metro-Goldwyn-Mayra	k1gFnPc2	Metro-Goldwyn-Mayra
<g/>
.	.	kIx.	.
</s>
<s>
Beverly	Beverl	k1gInPc4	Beverl
Hills	Hillsa	k1gFnPc2	Hillsa
má	mít	k5eAaImIp3nS	mít
horké	horký	k2eAgNnSc1d1	horké
léta	léto	k1gNnPc4	léto
a	a	k8xC	a
příjemně	příjemně	k6eAd1	příjemně
teplé	teplý	k2eAgFnSc2d1	teplá
zimy	zima	k1gFnSc2	zima
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
teplota	teplota	k1gFnSc1	teplota
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
je	být	k5eAaImIp3nS	být
29,4	[number]	k4	29,4
OC	OC	kA	OC
<g/>
,	,	kIx,	,
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
pak	pak	k6eAd1	pak
19,5	[number]	k4	19,5
OC	OC	kA	OC
<g/>
.	.	kIx.	.
</s>
<s>
Ročně	ročně	k6eAd1	ročně
zde	zde	k6eAd1	zde
naprší	napršet	k5eAaPmIp3nS	napršet
480	[number]	k4	480
mm	mm	kA	mm
dešťových	dešťový	k2eAgFnPc2d1	dešťová
srážek	srážka	k1gFnPc2	srážka
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
léto	léto	k1gNnSc4	léto
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
charakteristický	charakteristický	k2eAgInSc4d1	charakteristický
mírný	mírný	k2eAgInSc4d1	mírný
vánek	vánek	k1gInSc4	vánek
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
některé	některý	k3yIgInPc1	některý
zimní	zimní	k2eAgInPc1d1	zimní
dny	den	k1gInPc1	den
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
chladné	chladný	k2eAgInPc1d1	chladný
<g/>
,	,	kIx,	,
příležitostně	příležitostně	k6eAd1	příležitostně
doplňovány	doplňován	k2eAgInPc1d1	doplňován
deštěm	dešť	k1gInSc7	dešť
<g/>
.	.	kIx.	.
</s>
<s>
Sníh	sníh	k1gInSc1	sníh
zde	zde	k6eAd1	zde
naposledy	naposledy	k6eAd1	naposledy
napadl	napadnout	k5eAaPmAgMnS	napadnout
během	během	k7c2	během
zimy	zima	k1gFnSc2	zima
1958	[number]	k4	1958
<g/>
.	.	kIx.	.
</s>
<s>
Rodeo	rodeo	k1gNnSc1	rodeo
Drive	drive	k1gInSc1	drive
je	být	k5eAaImIp3nS	být
3	[number]	k4	3
km	km	kA	km
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
ulice	ulice	k1gFnSc1	ulice
plná	plný	k2eAgFnSc1d1	plná
luxusních	luxusní	k2eAgMnPc2d1	luxusní
obchodů	obchod	k1gInPc2	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
severní	severní	k2eAgInSc1d1	severní
konec	konec	k1gInSc1	konec
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
křižovatce	křižovatka	k1gFnSc6	křižovatka
se	s	k7c7	s
slavným	slavný	k2eAgInSc7d1	slavný
bulvárem	bulvár	k1gInSc7	bulvár
Sunset	Sunseta	k1gFnPc2	Sunseta
<g/>
.	.	kIx.	.
</s>
<s>
Obchodní	obchodní	k2eAgFnSc1d1	obchodní
oblast	oblast	k1gFnSc1	oblast
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
obklopuje	obklopovat	k5eAaImIp3nS	obklopovat
Rodeo	rodeo	k1gNnSc4	rodeo
Drive	drive	k1gInSc1	drive
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
Golden	Goldno	k1gNnPc2	Goldno
Triangle	triangl	k1gInSc5	triangl
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
otce	otec	k1gMnSc4	otec
Rodeo	rodeo	k1gNnSc1	rodeo
Dive	div	k1gInSc5	div
je	on	k3xPp3gInPc4	on
považován	považován	k2eAgMnSc1d1	považován
Fred	Fred	k1gMnSc1	Fred
Hayman	Hayman	k1gMnSc1	Hayman
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zde	zde	k6eAd1	zde
roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
otevřel	otevřít	k5eAaPmAgMnS	otevřít
první	první	k4xOgMnSc1	první
high-end	highnd	k1gMnSc1	high-end
butik	butik	k1gInSc4	butik
<g/>
.	.	kIx.	.
</s>
<s>
Socha	socha	k1gFnSc1	socha
na	na	k7c6	na
jižním	jižní	k2eAgInSc6d1	jižní
konci	konec	k1gInSc6	konec
Rodeo	rodeo	k1gNnSc1	rodeo
Drive	drive	k1gInSc1	drive
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
Rodeo	rodeo	k1gNnSc1	rodeo
Drive	drive	k1gInSc1	drive
Walk	Walk	k1gMnSc1	Walk
of	of	k?	of
Style	styl	k1gInSc5	styl
<g/>
.	.	kIx.	.
</s>
<s>
Socha	socha	k1gFnSc1	socha
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
umístěna	umístit	k5eAaPmNgFnS	umístit
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Nápad	nápad	k1gInSc1	nápad
vztyčit	vztyčit	k5eAaPmF	vztyčit
zde	zde	k6eAd1	zde
sochu	socha	k1gFnSc4	socha
přišel	přijít	k5eAaPmAgMnS	přijít
z	z	k7c2	z
hlavy	hlava	k1gFnSc2	hlava
samotného	samotný	k2eAgMnSc2d1	samotný
otce	otec	k1gMnSc2	otec
Rodeo	rodeo	k1gNnSc1	rodeo
Drive	drive	k1gInSc1	drive
Freda	Fred	k1gMnSc2	Fred
Haymana	Hayman	k1gMnSc2	Hayman
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Beverly	Beverl	k1gInPc1	Beverl
Hills	Hills	k1gInSc1	Hills
Hotel	hotel	k1gInSc1	hotel
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
bulváru	bulvár	k1gInSc6	bulvár
Sunset	Sunseta	k1gFnPc2	Sunseta
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nejikoničtějších	ikonický	k2eAgInPc2d3	ikonický
hotelů	hotel	k1gInPc2	hotel
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Hotel	hotel	k1gInSc1	hotel
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
asociován	asociovat	k5eAaBmNgInS	asociovat
s	s	k7c7	s
hollywoodskými	hollywoodský	k2eAgInPc7d1	hollywoodský
herci	herc	k1gInPc7	herc
<g/>
,	,	kIx,	,
hudebnímy	hudebním	k1gInPc7	hudebním
hvězdami	hvězda	k1gFnPc7	hvězda
<g/>
,	,	kIx,	,
a	a	k8xC	a
jinými	jiný	k2eAgFnPc7d1	jiná
celebritami	celebrita	k1gFnPc7	celebrita
<g/>
.	.	kIx.	.
</s>
<s>
Hotel	hotel	k1gInSc1	hotel
má	mít	k5eAaImIp3nS	mít
208	[number]	k4	208
pokojů	pokoj	k1gInPc2	pokoj
a	a	k8xC	a
28	[number]	k4	28
domků	domek	k1gInPc2	domek
<g/>
;	;	kIx,	;
všechny	všechen	k3xTgInPc1	všechen
mají	mít	k5eAaImIp3nP	mít
fasádu	fasáda	k1gFnSc4	fasáda
natřeno	natřen	k2eAgNnSc1d1	natřeno
na	na	k7c4	na
broskvově	broskvově	k6eAd1	broskvově
růžovou	růžový	k2eAgFnSc4d1	růžová
s	s	k7c7	s
prvky	prvek	k1gInPc7	prvek
zelené	zelená	k1gFnSc2	zelená
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
tradiční	tradiční	k2eAgFnSc1d1	tradiční
barva	barva	k1gFnSc1	barva
hotelu	hotel	k1gInSc2	hotel
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Beverly	Beverl	k1gInPc1	Beverl
Hills	Hills	k1gInSc1	Hills
Hotel	hotel	k1gInSc1	hotel
byl	být	k5eAaImAgInS	být
postaven	postaven	k2eAgInSc1d1	postaven
roku	rok	k1gInSc2	rok
1912	[number]	k4	1912
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
ještě	ještě	k9	ještě
před	před	k7c7	před
existencí	existence	k1gFnSc7	existence
samotného	samotný	k2eAgNnSc2d1	samotné
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Původnímy	Původníma	k1gFnPc1	Původníma
majiteli	majitel	k1gMnSc3	majitel
byly	být	k5eAaImAgInP	být
bohatá	bohatý	k2eAgFnSc1d1	bohatá
vdova	vdova	k1gFnSc1	vdova
Margaret	Margareta	k1gFnPc2	Margareta
J.	J.	kA	J.
Anderson	Anderson	k1gMnSc1	Anderson
a	a	k8xC	a
její	její	k3xOp3gMnSc1	její
syn	syn	k1gMnSc1	syn
Stanley	Stanlea	k1gFnSc2	Stanlea
S.	S.	kA	S.
Anderson	Anderson	k1gMnSc1	Anderson
<g/>
.	.	kIx.	.
</s>
<s>
Beverly	Beverla	k1gFnPc1	Beverla
Hills	Hills	k1gInSc1	Hills
City	City	k1gFnSc2	City
Hall	Halla	k1gFnPc2	Halla
je	být	k5eAaImIp3nS	být
radnice	radnice	k1gFnSc1	radnice
a	a	k8xC	a
historická	historický	k2eAgFnSc1d1	historická
památka	památka	k1gFnSc1	památka
v	v	k7c4	v
Beverly	Beverla	k1gFnPc4	Beverla
Hills	Hillsa	k1gFnPc2	Hillsa
<g/>
.	.	kIx.	.
</s>
<s>
Radnice	radnice	k1gFnSc1	radnice
je	být	k5eAaImIp3nS	být
obklopena	obklopit	k5eAaPmNgFnS	obklopit
bulváry	bulvár	k1gInPc7	bulvár
North	North	k1gMnSc1	North
a	a	k8xC	a
South	South	k1gMnSc1	South
Santa	Santa	k1gMnSc1	Santa
Monica	Monica	k1gMnSc1	Monica
<g/>
,	,	kIx,	,
North	North	k1gMnSc1	North
Roxford	Roxford	k1gMnSc1	Roxford
Drive	drive	k1gInSc4	drive
<g/>
,	,	kIx,	,
a	a	k8xC	a
North	North	k1gMnSc1	North
Crescent	Crescent	k1gMnSc1	Crescent
Drive	drive	k1gInSc4	drive
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
hlavní	hlavní	k2eAgInSc1d1	hlavní
vchod	vchod	k1gInSc1	vchod
do	do	k7c2	do
radnice	radnice	k1gFnSc2	radnice
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c4	na
North	North	k1gInSc4	North
Roxford	Roxford	k1gInSc1	Roxford
Drive	drive	k1gInSc1	drive
<g/>
.	.	kIx.	.
</s>
<s>
Předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
než	než	k8xS	než
byla	být	k5eAaImAgFnS	být
vybudována	vybudovat	k5eAaPmNgFnS	vybudovat
tato	tento	k3xDgFnSc1	tento
budova	budova	k1gFnSc1	budova
se	se	k3xPyFc4	se
coby	coby	k?	coby
radnice	radnice	k1gFnPc1	radnice
používaly	používat	k5eAaImAgFnP	používat
prostory	prostora	k1gFnPc4	prostora
hotelu	hotel	k1gInSc2	hotel
Beverly	Beverla	k1gFnSc2	Beverla
Hills	Hillsa	k1gFnPc2	Hillsa
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
dvacátých	dvacátý	k4xOgInPc2	dvacátý
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
radnicí	radnice	k1gFnSc7	radnice
stala	stát	k5eAaPmAgFnS	stát
dvoupodlažní	dvoupodlažní	k2eAgFnSc1d1	dvoupodlažní
budova	budova	k1gFnSc1	budova
na	na	k7c4	na
Burton	Burton	k1gInSc4	Burton
Way	Way	k1gFnSc2	Way
<g/>
.	.	kIx.	.
</s>
<s>
Současná	současný	k2eAgFnSc1d1	současná
radnice	radnice	k1gFnSc1	radnice
byla	být	k5eAaImAgFnS	být
dokončena	dokončen	k2eAgFnSc1d1	dokončena
roku	rok	k1gInSc2	rok
1932	[number]	k4	1932
<g/>
.	.	kIx.	.
</s>
<s>
Budovu	budova	k1gFnSc4	budova
navrhovali	navrhovat	k5eAaImAgMnP	navrhovat
designéři	designér	k1gMnPc1	designér
a	a	k8xC	a
architekti	architekt	k1gMnPc1	architekt
William	William	k1gInSc1	William
J.	J.	kA	J.
Gage	Gag	k1gFnSc2	Gag
a	a	k8xC	a
Harry	Harra	k1gFnSc2	Harra
O.	O.	kA	O.
Koerner	Koerner	k1gInSc1	Koerner
<g/>
.	.	kIx.	.
</s>
<s>
Radnice	radnice	k1gFnSc1	radnice
je	být	k5eAaImIp3nS	být
postavená	postavený	k2eAgFnSc1d1	postavená
ve	v	k7c6	v
španělském	španělský	k2eAgInSc6d1	španělský
kolonizačním	kolonizační	k2eAgInSc6d1	kolonizační
stylu	styl	k1gInSc6	styl
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
ji	on	k3xPp3gFnSc4	on
noviny	novina	k1gFnPc1	novina
Los	los	k1gMnSc1	los
Angeles	Angeles	k1gMnSc1	Angeles
Times	Times	k1gMnSc1	Times
nazvali	nazvat	k5eAaBmAgMnP	nazvat
"	"	kIx"	"
<g/>
Nejdražší	drahý	k2eAgFnSc7d3	nejdražší
a	a	k8xC	a
nejokázalejší	okázalý	k2eAgFnSc7d3	nejokázalejší
radnicí	radnice	k1gFnSc7	radnice
široko	široko	k6eAd1	široko
daleko	daleko	k6eAd1	daleko
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
budova	budova	k1gFnSc1	budova
radnice	radnice	k1gFnSc1	radnice
prošla	projít	k5eAaPmAgFnS	projít
rozsáhlou	rozsáhlý	k2eAgFnSc7d1	rozsáhlá
rekonstrukcí	rekonstrukce	k1gFnSc7	rekonstrukce
<g/>
.	.	kIx.	.
</s>
<s>
Beverly	Beverl	k1gInPc1	Beverl
Hills	Hillsa	k1gFnPc2	Hillsa
Civic	Civice	k1gFnPc2	Civice
Center	centrum	k1gNnPc2	centrum
je	být	k5eAaImIp3nS	být
pamětihodnost	pamětihodnost	k1gFnSc1	pamětihodnost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
hlavní	hlavní	k2eAgNnSc1d1	hlavní
náměstí	náměstí	k1gNnSc1	náměstí
Beverly	Beverla	k1gFnSc2	Beverla
Hills	Hillsa	k1gFnPc2	Hillsa
<g/>
.	.	kIx.	.
</s>
<s>
Civic	Civic	k1gMnSc1	Civic
Center	centrum	k1gNnPc2	centrum
bylo	být	k5eAaImAgNnS	být
vybudováno	vybudovat	k5eAaPmNgNnS	vybudovat
roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
při	při	k7c6	při
rekonstrukci	rekonstrukce	k1gFnSc6	rekonstrukce
radnice	radnice	k1gFnSc2	radnice
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejíž	jejíž	k3xOyRp3gFnSc6	jejíž
bezprostřední	bezprostřední	k2eAgFnSc6d1	bezprostřední
blízkosti	blízkost	k1gFnSc6	blízkost
se	se	k3xPyFc4	se
Civic	Civice	k1gFnPc2	Civice
Center	centrum	k1gNnPc2	centrum
nachází	nacházet	k5eAaImIp3nS	nacházet
<g/>
.	.	kIx.	.
</s>
<s>
Beverly	Beverla	k1gFnSc2	Beverla
Hills	Hillsa	k1gFnPc2	Hillsa
Financial	Financial	k1gInSc1	Financial
Center	centrum	k1gNnPc2	centrum
je	být	k5eAaImIp3nS	být
budova	budova	k1gFnSc1	budova
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
bulváru	bulvár	k1gInSc6	bulvár
Wilshire	Wilshir	k1gInSc5	Wilshir
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
je	být	k5eAaImIp3nS	být
vysoká	vysoký	k2eAgFnSc1d1	vysoká
42	[number]	k4	42
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1952	[number]	k4	1952
dokončil	dokončit	k5eAaPmAgMnS	dokončit
americký	americký	k2eAgMnSc1d1	americký
architekt	architekt	k1gMnSc1	architekt
Frank	Frank	k1gMnSc1	Frank
Lloyd	Lloyd	k1gMnSc1	Lloyd
Wright	Wright	k1gMnSc1	Wright
práce	práce	k1gFnSc2	práce
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
poslední	poslední	k2eAgFnSc6d1	poslední
stavbě	stavba	k1gFnSc6	stavba
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angeles	k1gInSc4	Angeles
<g/>
;	;	kIx,	;
touto	tento	k3xDgFnSc7	tento
budovou	budova	k1gFnSc7	budova
je	být	k5eAaImIp3nS	být
Anderson	Anderson	k1gMnSc1	Anderson
Court	Courta	k1gFnPc2	Courta
Shops	Shopsa	k1gFnPc2	Shopsa
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
stavba	stavba	k1gFnSc1	stavba
je	být	k5eAaImIp3nS	být
malé	malý	k2eAgNnSc4d1	malé
třípatrové	třípatrový	k2eAgNnSc4d1	třípatrové
obchodní	obchodní	k2eAgNnSc4d1	obchodní
centrum	centrum	k1gNnSc4	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
je	být	k5eAaImIp3nS	být
dům	dům	k1gInSc4	dům
třípatrový	třípatrový	k2eAgInSc4d1	třípatrový
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
zde	zde	k6eAd1	zde
nabízí	nabízet	k5eAaImIp3nS	nabízet
své	svůj	k3xOyFgInPc4	svůj
produkty	produkt	k1gInPc4	produkt
pouze	pouze	k6eAd1	pouze
šest	šest	k4xCc4	šest
společností	společnost	k1gFnPc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Elektrická	elektrický	k2eAgFnSc1d1	elektrická
fontána	fontána	k1gFnSc1	fontána
je	být	k5eAaImIp3nS	být
vodní	vodní	k2eAgFnSc1d1	vodní
fontána	fontána	k1gFnSc1	fontána
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
na	na	k7c6	na
křižovatce	křižovatka	k1gFnSc6	křižovatka
bulvárů	bulvár	k1gInPc2	bulvár
Santa	Santa	k1gMnSc1	Santa
Monica	Monica	k1gMnSc1	Monica
a	a	k8xC	a
Wilshire	Wilshir	k1gInSc5	Wilshir
<g/>
.	.	kIx.	.
</s>
<s>
Fontána	fontána	k1gFnSc1	fontána
zde	zde	k6eAd1	zde
vyrostla	vyrůst	k5eAaPmAgFnS	vyrůst
začátkem	začátkem	k7c2	začátkem
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Wilshire	Wilshir	k1gMnSc5	Wilshir
Beverly	Beverl	k1gMnPc4	Beverl
Center	centrum	k1gNnPc2	centrum
je	být	k5eAaImIp3nS	být
pamětihodnost	pamětihodnost	k1gFnSc1	pamětihodnost
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaPmF	nalézt
na	na	k7c6	na
rohu	roh	k1gInSc6	roh
bulváru	bulvár	k1gInSc2	bulvár
Wilshire	Wilshir	k1gInSc5	Wilshir
a	a	k8xC	a
Beverly	Beverl	k1gMnPc4	Beverl
Drive	drive	k1gInSc4	drive
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
devítipatrová	devítipatrový	k2eAgFnSc1d1	devítipatrová
budova	budova	k1gFnSc1	budova
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
roku	rok	k1gInSc2	rok
1962	[number]	k4	1962
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
základech	základ	k1gInPc6	základ
budovy	budova	k1gFnSc2	budova
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaPmF	nalézt
pohrobky	pohrobek	k1gMnPc4	pohrobek
panické	panický	k2eAgFnSc2d1	panická
hrůzy	hrůza	k1gFnSc2	hrůza
z	z	k7c2	z
nukleárního	nukleární	k2eAgInSc2d1	nukleární
útoku	útok	k1gInSc2	útok
během	během	k7c2	během
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
obrovská	obrovský	k2eAgFnSc1d1	obrovská
místnost	místnost	k1gFnSc1	místnost
<g/>
,	,	kIx,	,
v	v	k7c6	v
které	který	k3yIgFnSc6	který
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
ukrývat	ukrývat	k5eAaImF	ukrývat
až	až	k9	až
4000	[number]	k4	4000
lidí	člověk	k1gMnPc2	člověk
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
až	až	k9	až
14	[number]	k4	14
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
2010	[number]	k4	2010
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
34,109	[number]	k4	34,109
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Hustota	hustota	k1gFnSc1	hustota
obyvatel	obyvatel	k1gMnPc2	obyvatel
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
2,306	[number]	k4	2,306
osoby	osoba	k1gFnPc1	osoba
<g/>
/	/	kIx~	/
<g/>
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Rasová	rasový	k2eAgFnSc1d1	rasová
pestrost	pestrost	k1gFnSc1	pestrost
je	být	k5eAaImIp3nS	být
následující	následující	k2eAgFnSc1d1	následující
<g/>
:	:	kIx,	:
28,112	[number]	k4	28,112
(	(	kIx(	(
<g/>
82,4	[number]	k4	82,4
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
bílých	bílý	k2eAgInPc2d1	bílý
746	[number]	k4	746
(	(	kIx(	(
<g/>
2,2	[number]	k4	2,2
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
černých	černý	k2eAgInPc2d1	černý
48	[number]	k4	48
(	(	kIx(	(
<g/>
0,1	[number]	k4	0,1
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
původních	původní	k2eAgMnPc2d1	původní
američanů	američan	k1gMnPc2	američan
3,032	[number]	k4	3,032
(	(	kIx(	(
<g/>
8,9	[number]	k4	8,9
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
asiatů	asiat	k1gMnPc2	asiat
12	[number]	k4	12
(	(	kIx(	(
<g/>
0,01	[number]	k4	0,01
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
tichomořských	tichomořský	k2eAgMnPc2d1	tichomořský
ostrovanů	ostrovan	k1gMnPc2	ostrovan
485	[number]	k4	485
(	(	kIx(	(
<g/>
1,4	[number]	k4	1,4
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
ostatních	ostatní	k2eAgFnPc2d1	ostatní
ras	rasa	k1gFnPc2	rasa
Město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
filmech	film	k1gInPc6	film
a	a	k8xC	a
televizních	televizní	k2eAgInPc6d1	televizní
seriálech	seriál	k1gInPc6	seriál
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
ně	on	k3xPp3gInPc4	on
patří	patřit	k5eAaImIp3nP	patřit
snímky	snímka	k1gFnPc4	snímka
Policajt	Policajt	k?	Policajt
v	v	k7c4	v
Beverly	Beverl	k1gMnPc4	Beverl
Hills	Hillsa	k1gFnPc2	Hillsa
(	(	kIx(	(
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Policajt	Policajt	k?	Policajt
v	v	k7c4	v
Beverly	Beverl	k1gInPc4	Beverl
Hills	Hillsa	k1gFnPc2	Hillsa
II	II	kA	II
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Policajt	Policajt	k?	Policajt
v	v	k7c4	v
Beverly	Beverl	k1gInPc4	Beverl
Hills	Hillsa	k1gFnPc2	Hillsa
III	III	kA	III
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
a	a	k8xC	a
Čivava	Čivava	k1gFnSc1	Čivava
z	z	k7c2	z
Beverly	Beverla	k1gFnSc2	Beverla
Hills	Hills	k1gInSc1	Hills
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
či	či	k8xC	či
seriál	seriál	k1gInSc4	seriál
Beverly	Beverla	k1gFnSc2	Beverla
Hills	Hills	k1gInSc1	Hills
90210	[number]	k4	90210
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
–	–	k?	–
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Beverly	Beverla	k1gFnPc4	Beverla
Hills	Hillsa	k1gFnPc2	Hillsa
bylo	být	k5eAaImAgNnS	být
také	také	k9	také
součástí	součást	k1gFnSc7	součást
videohry	videohra	k1gFnSc2	videohra
Grand	grand	k1gMnSc1	grand
Theft	Theft	k1gMnSc1	Theft
Auto	auto	k1gNnSc4	auto
V	v	k7c4	v
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
Cannes	Cannes	k1gNnSc2	Cannes
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc2	Francie
</s>
