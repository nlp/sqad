<s>
Nekrotickou	nekrotický	k2eAgFnSc4d1	nekrotická
enteritidu	enteritida	k1gFnSc4	enteritida
u	u	k7c2	u
kuřat	kuře	k1gNnPc2	kuře
poprvé	poprvé	k6eAd1	poprvé
popsal	popsat	k5eAaPmAgInS	popsat
Parish	Parish	k1gInSc1	Parish
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1961	[number]	k4	1961
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
ji	on	k3xPp3gFnSc4	on
také	také	k6eAd1	také
experimentálně	experimentálně	k6eAd1	experimentálně
reprodukoval	reprodukovat	k5eAaBmAgMnS	reprodukovat
<g/>
.	.	kIx.	.
</s>
