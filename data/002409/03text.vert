<s>
Nekrotická	nekrotický	k2eAgFnSc1d1	nekrotická
enteritida	enteritida	k1gFnSc1	enteritida
drůbeže	drůbež	k1gFnSc2	drůbež
(	(	kIx(	(
<g/>
NE	Ne	kA	Ne
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
akutně	akutně	k6eAd1	akutně
nebo	nebo	k8xC	nebo
chronicky	chronicky	k6eAd1	chronicky
probíhající	probíhající	k2eAgNnPc1d1	probíhající
onemocnění	onemocnění	k1gNnPc1	onemocnění
(	(	kIx(	(
<g/>
toxikóza	toxikóza	k1gFnSc1	toxikóza
<g/>
)	)	kIx)	)
střevního	střevní	k2eAgInSc2d1	střevní
traktu	trakt	k1gInSc2	trakt
drůbeže	drůbež	k1gFnSc2	drůbež
i	i	k9	i
mnoha	mnoho	k4c2	mnoho
jiných	jiný	k2eAgMnPc2d1	jiný
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
způsobované	způsobovaný	k2eAgFnPc1d1	způsobovaná
bakterií	bakterie	k1gFnSc7	bakterie
Clostridium	Clostridium	k1gNnSc1	Clostridium
perfringens	perfringens	k6eAd1	perfringens
a	a	k8xC	a
jejími	její	k3xOp3gInPc7	její
toxiny	toxin	k1gInPc7	toxin
<g/>
.	.	kIx.	.
</s>
<s>
Nezbytným	zbytný	k2eNgInSc7d1	zbytný
předpokladem	předpoklad	k1gInSc7	předpoklad
vzniku	vznik	k1gInSc2	vznik
NE	ne	k9	ne
jsou	být	k5eAaImIp3nP	být
predispoziční	predispoziční	k2eAgInPc1d1	predispoziční
faktory	faktor	k1gInPc1	faktor
poškozující	poškozující	k2eAgFnSc4d1	poškozující
střevní	střevní	k2eAgFnSc4d1	střevní
sliznici	sliznice	k1gFnSc4	sliznice
(	(	kIx(	(
<g/>
např.	např.	kA	např.
kokcidióza	kokcidióza	k1gFnSc1	kokcidióza
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
střevní	střevní	k2eAgFnSc4d1	střevní
mikroflóru	mikroflóra	k1gFnSc4	mikroflóra
(	(	kIx(	(
<g/>
dietetické	dietetický	k2eAgFnPc4d1	dietetická
závady	závada	k1gFnPc4	závada
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Onemocnění	onemocnění	k1gNnSc1	onemocnění
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
ekonomicky	ekonomicky	k6eAd1	ekonomicky
závažné	závažný	k2eAgInPc4d1	závažný
problémy	problém	k1gInPc4	problém
zejména	zejména	k9	zejména
ve	v	k7c6	v
výkrmu	výkrm	k1gInSc6	výkrm
kuřat	kuře	k1gNnPc2	kuře
a	a	k8xC	a
krůťat	krůtě	k1gNnPc2	krůtě
<g/>
.	.	kIx.	.
</s>
<s>
Nekrotickou	nekrotický	k2eAgFnSc4d1	nekrotická
enteritidu	enteritida	k1gFnSc4	enteritida
u	u	k7c2	u
kuřat	kuře	k1gNnPc2	kuře
poprvé	poprvé	k6eAd1	poprvé
popsal	popsat	k5eAaPmAgInS	popsat
Parish	Parish	k1gInSc1	Parish
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1961	[number]	k4	1961
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ji	on	k3xPp3gFnSc4	on
také	také	k6eAd1	také
experimentálně	experimentálně	k6eAd1	experimentálně
reprodukoval	reprodukovat	k5eAaBmAgMnS	reprodukovat
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
byla	být	k5eAaImAgFnS	být
NE	ne	k9	ne
diagnostikována	diagnostikovat	k5eAaBmNgFnS	diagnostikovat
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
zemích	zem	k1gFnPc6	zem
světa	svět	k1gInSc2	svět
s	s	k7c7	s
komerční	komerční	k2eAgFnSc7d1	komerční
produkcí	produkce	k1gFnSc7	produkce
hrabavé	hrabavý	k2eAgFnSc2d1	hrabavá
drůbeže	drůbež	k1gFnSc2	drůbež
<g/>
.	.	kIx.	.
</s>
<s>
Nejzávažnější	závažný	k2eAgInPc4d3	nejzávažnější
problémy	problém	k1gInPc4	problém
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
u	u	k7c2	u
výkrmových	výkrmový	k2eAgNnPc2d1	výkrmový
kuřat	kuře	k1gNnPc2	kuře
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ztráty	ztráta	k1gFnPc1	ztráta
v	v	k7c6	v
neléčených	léčený	k2eNgInPc6d1	neléčený
chovech	chov	k1gInPc6	chov
mohou	moct	k5eAaImIp3nP	moct
dosahovat	dosahovat	k5eAaImF	dosahovat
10	[number]	k4	10
i	i	k9	i
více	hodně	k6eAd2	hodně
procent	procento	k1gNnPc2	procento
<g/>
;	;	kIx,	;
u	u	k7c2	u
starších	starý	k2eAgMnPc2d2	starší
ptáků	pták	k1gMnPc2	pták
je	být	k5eAaImIp3nS	být
výskyt	výskyt	k1gInSc1	výskyt
sporadický	sporadický	k2eAgInSc1d1	sporadický
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
vysoce	vysoce	k6eAd1	vysoce
vnímavé	vnímavý	k2eAgMnPc4d1	vnímavý
také	také	k6eAd1	také
patří	patřit	k5eAaImIp3nP	patřit
ptáci	pták	k1gMnPc1	pták
z	z	k7c2	z
čeledí	čeleď	k1gFnPc2	čeleď
Tetraonidae	Tetraonidae	k1gFnPc2	Tetraonidae
(	(	kIx(	(
<g/>
tetřevovití	tetřevovitý	k2eAgMnPc1d1	tetřevovitý
<g/>
)	)	kIx)	)
a	a	k8xC	a
Odontophoridae	Odontophoridae	k1gFnSc1	Odontophoridae
(	(	kIx(	(
<g/>
křepelovití	křepelovitý	k2eAgMnPc1d1	křepelovitý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Původcem	původce	k1gMnSc7	původce
onemocnění	onemocnění	k1gNnSc2	onemocnění
je	být	k5eAaImIp3nS	být
podmíněně	podmíněně	k6eAd1	podmíněně
patogenní	patogenní	k2eAgFnSc2d1	patogenní
bakterie	bakterie	k1gFnSc2	bakterie
Clostridium	Clostridium	k1gNnSc1	Clostridium
perfringens	perfringens	k1gInSc1	perfringens
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
normální	normální	k2eAgFnSc2d1	normální
střevní	střevní	k2eAgFnSc2d1	střevní
mikroflóry	mikroflóra	k1gFnSc2	mikroflóra
zvířat	zvíře	k1gNnPc2	zvíře
a	a	k8xC	a
člověka	člověk	k1gMnSc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
značném	značný	k2eAgNnSc6d1	značné
množství	množství	k1gNnSc6	množství
se	se	k3xPyFc4	se
také	také	k9	také
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
půdě	půda	k1gFnSc6	půda
<g/>
,	,	kIx,	,
v	v	k7c6	v
prachu	prach	k1gInSc6	prach
<g/>
,	,	kIx,	,
v	v	k7c6	v
bahně	bahno	k1gNnSc6	bahno
<g/>
,	,	kIx,	,
ve	v	k7c6	v
fekáliích	fekálie	k1gFnPc6	fekálie
a	a	k8xC	a
v	v	k7c6	v
odpadních	odpadní	k2eAgFnPc6d1	odpadní
vodách	voda	k1gFnPc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Nekrózu	nekróza	k1gFnSc4	nekróza
střevní	střevní	k2eAgFnSc2d1	střevní
sliznice	sliznice	k1gFnSc2	sliznice
nejčastěji	často	k6eAd3	často
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
toxiny	toxin	k1gInPc1	toxin
produkované	produkovaný	k2eAgInPc1d1	produkovaný
C.	C.	kA	C.
perfringens	perfringens	k6eAd1	perfringens
typů	typ	k1gInPc2	typ
A	A	kA	A
nebo	nebo	k8xC	nebo
C.	C.	kA	C.
Z	z	k7c2	z
případů	případ	k1gInPc2	případ
nekrotické	nekrotický	k2eAgFnSc2d1	nekrotická
enteritidy	enteritida	k1gFnSc2	enteritida
u	u	k7c2	u
mladých	mladý	k2eAgMnPc2d1	mladý
pštrosů	pštros	k1gMnPc2	pštros
byla	být	k5eAaImAgFnS	být
také	také	k9	také
izolována	izolován	k2eAgFnSc1d1	izolována
bakterie	bakterie	k1gFnSc1	bakterie
C.	C.	kA	C.
difficile	difficile	k6eAd1	difficile
<g/>
.	.	kIx.	.
</s>
<s>
C.	C.	kA	C.
perfringens	perfringens	k6eAd1	perfringens
je	být	k5eAaImIp3nS	být
grampozitivní	grampozitivní	k2eAgFnSc1d1	grampozitivní
<g/>
,	,	kIx,	,
sporogenní	sporogenní	k2eAgFnSc1d1	sporogenní
<g/>
,	,	kIx,	,
nepohyblivá	pohyblivý	k2eNgFnSc1d1	nepohyblivá
tyčinka	tyčinka	k1gFnSc1	tyčinka
velikosti	velikost	k1gFnSc2	velikost
1	[number]	k4	1
x	x	k?	x
8	[number]	k4	8
μ	μ	k?	μ
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
opouzdřená	opouzdřený	k2eAgFnSc1d1	opouzdřená
polysacharidovým	polysacharidový	k2eAgNnSc7d1	polysacharidový
pouzdrem	pouzdro	k1gNnSc7	pouzdro
<g/>
.	.	kIx.	.
</s>
<s>
Produkuje	produkovat	k5eAaImIp3nS	produkovat
řadu	řada	k1gFnSc4	řada
biologicky	biologicky	k6eAd1	biologicky
aktivních	aktivní	k2eAgFnPc2d1	aktivní
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
nejdůležitější	důležitý	k2eAgFnSc1d3	nejdůležitější
jsou	být	k5eAaImIp3nP	být
letální	letální	k2eAgInPc1d1	letální
a	a	k8xC	a
nekrotizující	krotizující	k2eNgInPc1d1	nekrotizující
toxiny	toxin	k1gInPc1	toxin
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jejich	jejich	k3xOp3gNnSc2	jejich
zastoupení	zastoupení	k1gNnSc2	zastoupení
se	s	k7c7	s
kmeny	kmen	k1gInPc7	kmen
C.	C.	kA	C.
perfringens	perfringens	k6eAd1	perfringens
dělí	dělit	k5eAaImIp3nS	dělit
do	do	k7c2	do
skupin	skupina	k1gFnPc2	skupina
(	(	kIx(	(
<g/>
typů	typ	k1gInPc2	typ
<g/>
)	)	kIx)	)
A	A	kA	A
-	-	kIx~	-
E.	E.	kA	E.
Roste	růst	k5eAaImIp3nS	růst
poměrně	poměrně	k6eAd1	poměrně
rychle	rychle	k6eAd1	rychle
na	na	k7c6	na
krevním	krevní	k2eAgInSc6d1	krevní
agaru	agar	k1gInSc6	agar
inkubovaném	inkubovaný	k2eAgInSc6d1	inkubovaný
anaerogenně	anaerogenně	k6eAd1	anaerogenně
při	při	k7c6	při
37	[number]	k4	37
°	°	k?	°
<g/>
C	C	kA	C
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
hemolýzy	hemolýza	k1gFnSc2	hemolýza
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
krevním	krevní	k2eAgInSc6d1	krevní
agaru	agar	k1gInSc6	agar
s	s	k7c7	s
ovčími	ovčí	k2eAgFnPc7d1	ovčí
(	(	kIx(	(
<g/>
králičími	králičí	k2eAgFnPc7d1	králičí
<g/>
,	,	kIx,	,
lidskými	lidský	k2eAgInPc7d1	lidský
<g/>
)	)	kIx)	)
erytrocyty	erytrocyt	k1gInPc7	erytrocyt
vzniká	vznikat	k5eAaImIp3nS	vznikat
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
zóna	zóna	k1gFnSc1	zóna
úplné	úplný	k2eAgFnSc2d1	úplná
hemolýzy	hemolýza	k1gFnSc2	hemolýza
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
obklopena	obklopit	k5eAaPmNgFnS	obklopit
vnější	vnější	k2eAgFnSc7d1	vnější
zónou	zóna	k1gFnSc7	zóna
neúplné	úplný	k2eNgFnSc2d1	neúplná
hemolýzy	hemolýza	k1gFnSc2	hemolýza
<g/>
.	.	kIx.	.
</s>
<s>
Kolonie	kolonie	k1gFnPc1	kolonie
jsou	být	k5eAaImIp3nP	být
kulaté	kulatý	k2eAgInPc1d1	kulatý
<g/>
,	,	kIx,	,
šedobílé	šedobílý	k2eAgInPc1d1	šedobílý
barvy	barva	k1gFnPc4	barva
a	a	k8xC	a
průměrné	průměrný	k2eAgFnPc4d1	průměrná
velikosti	velikost	k1gFnPc4	velikost
2-5	[number]	k4	2-5
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
kmenů	kmen	k1gInPc2	kmen
C.	C.	kA	C.
perfringens	perfringens	k6eAd1	perfringens
fermentuje	fermentovat	k5eAaBmIp3nS	fermentovat
glukózu	glukóza	k1gFnSc4	glukóza
<g/>
,	,	kIx,	,
maltózu	maltóza	k1gFnSc4	maltóza
<g/>
,	,	kIx,	,
laktózu	laktóza	k1gFnSc4	laktóza
a	a	k8xC	a
sacharózu	sacharóza	k1gFnSc4	sacharóza
<g/>
,	,	kIx,	,
nefermentuje	fermentovat	k5eNaBmIp3nS	fermentovat
manitol	manitol	k1gInSc1	manitol
<g/>
.	.	kIx.	.
</s>
<s>
Variabilně	variabilně	k6eAd1	variabilně
fermentuje	fermentovat	k5eAaBmIp3nS	fermentovat
salicin	salicin	k1gInSc1	salicin
<g/>
.	.	kIx.	.
</s>
<s>
Základními	základní	k2eAgInPc7d1	základní
produkty	produkt	k1gInPc7	produkt
fermentace	fermentace	k1gFnSc2	fermentace
jsou	být	k5eAaImIp3nP	být
kyseliny	kyselina	k1gFnPc1	kyselina
octová	octový	k2eAgFnSc1d1	octová
a	a	k8xC	a
máselná	máselný	k2eAgFnSc1d1	máselná
<g/>
.	.	kIx.	.
</s>
<s>
Želatina	želatina	k1gFnSc1	želatina
je	být	k5eAaImIp3nS	být
hydrolyzována	hydrolyzován	k2eAgFnSc1d1	hydrolyzován
<g/>
,	,	kIx,	,
indol	indol	k1gInSc1	indol
není	být	k5eNaImIp3nS	být
produkován	produkovat	k5eAaImNgInS	produkovat
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
určení	určení	k1gNnSc4	určení
druhu	druh	k1gInSc2	druh
je	být	k5eAaImIp3nS	být
významná	významný	k2eAgFnSc1d1	významná
tvorba	tvorba	k1gFnSc1	tvorba
lecitinázy	lecitináza	k1gFnSc2	lecitináza
a	a	k8xC	a
nepřítomnost	nepřítomnost	k1gFnSc1	nepřítomnost
lipázy	lipáza	k1gFnSc2	lipáza
<g/>
.	.	kIx.	.
</s>
<s>
C.	C.	kA	C.
perfringens	perfringens	k1gInSc1	perfringens
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
vysokou	vysoký	k2eAgFnSc7d1	vysoká
odolností	odolnost	k1gFnSc7	odolnost
ve	v	k7c6	v
vnějším	vnější	k2eAgNnSc6d1	vnější
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Přirozené	přirozený	k2eAgInPc4d1	přirozený
výskyty	výskyt	k1gInPc4	výskyt
nekrotické	nekrotický	k2eAgFnSc2d1	nekrotická
enteritidy	enteritida	k1gFnSc2	enteritida
byly	být	k5eAaImAgInP	být
popsány	popsat	k5eAaPmNgInP	popsat
u	u	k7c2	u
kura	kur	k1gMnSc2	kur
domácího	domácí	k2eAgMnSc2d1	domácí
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
od	od	k7c2	od
2	[number]	k4	2
týdnů	týden	k1gInPc2	týden
do	do	k7c2	do
6	[number]	k4	6
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
ale	ale	k8xC	ale
u	u	k7c2	u
brojlerů	brojler	k1gMnPc2	brojler
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
2-5	[number]	k4	2-5
týdnů	týden	k1gInPc2	týden
chovaných	chovaný	k2eAgInPc2d1	chovaný
na	na	k7c6	na
hluboké	hluboký	k2eAgFnSc6d1	hluboká
podestýlce	podestýlka	k1gFnSc6	podestýlka
<g/>
.	.	kIx.	.
</s>
<s>
NE	ne	k9	ne
se	se	k3xPyFc4	se
také	také	k9	také
může	moct	k5eAaImIp3nS	moct
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
u	u	k7c2	u
mladých	mladý	k2eAgNnPc2d1	mladé
krůťat	krůtě	k1gNnPc2	krůtě
a	a	k8xC	a
krůt	krůta	k1gFnPc2	krůta
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
kokcidiózou	kokcidióza	k1gFnSc7	kokcidióza
nebo	nebo	k8xC	nebo
askaridiózou	askaridióza	k1gFnSc7	askaridióza
<g/>
.	.	kIx.	.
</s>
<s>
Subklinická	Subklinický	k2eAgFnSc1d1	Subklinická
nekrotická	nekrotický	k2eAgFnSc1d1	nekrotická
enteritida	enteritida	k1gFnSc1	enteritida
u	u	k7c2	u
brojlerů	brojler	k1gMnPc2	brojler
zhoršuje	zhoršovat	k5eAaImIp3nS	zhoršovat
využití	využití	k1gNnSc1	využití
krmiva	krmivo	k1gNnSc2	krmivo
a	a	k8xC	a
zpomaluje	zpomalovat	k5eAaImIp3nS	zpomalovat
jejich	jejich	k3xOp3gInSc4	jejich
růst	růst	k1gInSc4	růst
<g/>
.	.	kIx.	.
</s>
<s>
Bakterie	bakterie	k1gFnSc1	bakterie
C.	C.	kA	C.
perfringens	perfringens	k6eAd1	perfringens
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
i	i	k8xC	i
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
s	s	k7c7	s
chovanou	chovaný	k2eAgFnSc7d1	chovaná
drůbeží	drůbež	k1gFnSc7	drůbež
<g/>
;	;	kIx,	;
spory	spor	k1gInPc1	spor
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
přetrvávají	přetrvávat	k5eAaImIp3nP	přetrvávat
v	v	k7c6	v
půdě	půda	k1gFnSc6	půda
<g/>
,	,	kIx,	,
krmivu	krmivo	k1gNnSc6	krmivo
i	i	k8xC	i
v	v	k7c6	v
podestýlce	podestýlka	k1gFnSc6	podestýlka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
obecně	obecně	k6eAd1	obecně
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
obligátní	obligátní	k2eAgFnSc4d1	obligátní
anaerobní	anaerobní	k2eAgFnSc4d1	anaerobní
bakterii	bakterie	k1gFnSc4	bakterie
střevního	střevní	k2eAgInSc2d1	střevní
traktu	trakt	k1gInSc2	trakt
drůbeže	drůbež	k1gFnSc2	drůbež
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gNnSc1	jejíž
množství	množství	k1gNnSc1	množství
ve	v	k7c6	v
střevě	střevo	k1gNnSc6	střevo
lze	lze	k6eAd1	lze
ovlivňovat	ovlivňovat	k5eAaImF	ovlivňovat
dietou	dieta	k1gFnSc7	dieta
<g/>
.	.	kIx.	.
</s>
<s>
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
hladina	hladina	k1gFnSc1	hladina
rybí	rybí	k2eAgFnSc2d1	rybí
moučky	moučka	k1gFnSc2	moučka
<g/>
,	,	kIx,	,
pšenice	pšenice	k1gFnSc2	pšenice
nebo	nebo	k8xC	nebo
ječmene	ječmen	k1gInSc2	ječmen
v	v	k7c6	v
krmivu	krmivo	k1gNnSc6	krmivo
může	moct	k5eAaImIp3nS	moct
predisponovat	predisponovat	k5eAaBmF	predisponovat
anebo	anebo	k8xC	anebo
exacerbovat	exacerbovat	k5eAaImF	exacerbovat
výskyt	výskyt	k1gInSc4	výskyt
nekrotické	nekrotický	k2eAgFnSc2d1	nekrotická
enteritidy	enteritida	k1gFnSc2	enteritida
<g/>
.	.	kIx.	.
</s>
<s>
Poškození	poškození	k1gNnSc1	poškození
střevní	střevní	k2eAgFnSc2d1	střevní
sliznice	sliznice	k1gFnSc2	sliznice
(	(	kIx(	(
<g/>
hrubá	hrubý	k2eAgFnSc1d1	hrubá
podestýlka	podestýlka	k1gFnSc1	podestýlka
<g/>
,	,	kIx,	,
kokcidie	kokcidie	k1gFnSc1	kokcidie
<g/>
,	,	kIx,	,
dysfunkce	dysfunkce	k1gFnSc1	dysfunkce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
silně	silně	k6eAd1	silně
zamořené	zamořený	k2eAgNnSc1d1	zamořené
prostředí	prostředí	k1gNnSc1	prostředí
<g/>
,	,	kIx,	,
snížená	snížený	k2eAgFnSc1d1	snížená
odolnost	odolnost	k1gFnSc1	odolnost
(	(	kIx(	(
<g/>
stresy	stres	k1gInPc1	stres
<g/>
,	,	kIx,	,
interkurentní	interkurentní	k2eAgFnPc1d1	interkurentní
infekce	infekce	k1gFnPc1	infekce
<g/>
,	,	kIx,	,
infekční	infekční	k2eAgFnPc1d1	infekční
anémie	anémie	k1gFnPc1	anémie
kuřat	kuře	k1gNnPc2	kuře
<g/>
,	,	kIx,	,
infekční	infekční	k2eAgFnSc1d1	infekční
burzitida	burzitida	k1gFnSc1	burzitida
drůbeže	drůbež	k1gFnSc2	drůbež
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
dalšími	další	k2eAgInPc7d1	další
podpůrnými	podpůrný	k2eAgInPc7d1	podpůrný
faktory	faktor	k1gInPc7	faktor
vzniku	vznik	k1gInSc2	vznik
NE	Ne	kA	Ne
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
starší	starý	k2eAgFnSc2d2	starší
drůbeže	drůbež	k1gFnSc2	drůbež
vzniká	vznikat	k5eAaImIp3nS	vznikat
věková	věkový	k2eAgFnSc1d1	věková
rezistence	rezistence	k1gFnSc1	rezistence
k	k	k7c3	k
onemocnění	onemocnění	k1gNnSc3	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
onemocnění	onemocnění	k1gNnSc3	onemocnění
dochází	docházet	k5eAaImIp3nS	docházet
exogenně	exogenně	k6eAd1	exogenně
po	po	k7c6	po
orálním	orální	k2eAgInSc6d1	orální
příjmu	příjem	k1gInSc6	příjem
velkého	velký	k2eAgNnSc2d1	velké
množství	množství	k1gNnSc2	množství
bakterií	bakterie	k1gFnPc2	bakterie
anebo	anebo	k8xC	anebo
jejich	jejich	k3xOp3gInPc2	jejich
toxinů	toxin	k1gInPc2	toxin
(	(	kIx(	(
<g/>
krmivo	krmivo	k1gNnSc1	krmivo
<g/>
,	,	kIx,	,
trus	trus	k1gInSc1	trus
<g/>
,	,	kIx,	,
podestýlka	podestýlka	k1gFnSc1	podestýlka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
endogenně	endogenně	k6eAd1	endogenně
vlivem	vlivem	k7c2	vlivem
predispozičních	predispoziční	k2eAgInPc2d1	predispoziční
faktorů	faktor	k1gInPc2	faktor
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
pomnožení	pomnožení	k1gNnSc3	pomnožení
C.	C.	kA	C.
perfringens	perfringens	k1gInSc4	perfringens
v	v	k7c6	v
konečníku	konečník	k1gInSc6	konečník
a	a	k8xC	a
slepých	slepý	k2eAgNnPc6d1	slepé
střevech	střevo	k1gNnPc6	střevo
a	a	k8xC	a
následné	následný	k2eAgFnSc6d1	následná
migraci	migrace	k1gFnSc6	migrace
do	do	k7c2	do
tenkého	tenký	k2eAgNnSc2d1	tenké
střeva	střevo	k1gNnSc2	střevo
a	a	k8xC	a
produkci	produkce	k1gFnSc4	produkce
toxinů	toxin	k1gInPc2	toxin
<g/>
.	.	kIx.	.
</s>
<s>
Enterotoxin	Enterotoxin	k1gInSc1	Enterotoxin
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
permeabilitu	permeabilita	k1gFnSc4	permeabilita
(	(	kIx(	(
<g/>
propustnost	propustnost	k1gFnSc1	propustnost
<g/>
)	)	kIx)	)
kapilár	kapilára	k1gFnPc2	kapilára
<g/>
,	,	kIx,	,
porušuje	porušovat	k5eAaImIp3nS	porušovat
transport	transport	k1gInSc4	transport
tekutin	tekutina	k1gFnPc2	tekutina
a	a	k8xC	a
iontů	ion	k1gInPc2	ion
přes	přes	k7c4	přes
stěnu	stěna	k1gFnSc4	stěna
střevní	střevní	k2eAgFnSc4d1	střevní
a	a	k8xC	a
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
průjem	průjem	k1gInSc4	průjem
a	a	k8xC	a
nekrózu	nekróza	k1gFnSc4	nekróza
enterocytů	enterocyt	k1gInPc2	enterocyt
<g/>
.	.	kIx.	.
</s>
<s>
Inkubační	inkubační	k2eAgFnSc1d1	inkubační
doba	doba	k1gFnSc1	doba
po	po	k7c4	po
experimentální	experimentální	k2eAgFnSc4d1	experimentální
infekci	infekce	k1gFnSc4	infekce
je	být	k5eAaImIp3nS	být
1-2	[number]	k4	1-2
dny	den	k1gInPc7	den
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
postiženém	postižený	k2eAgNnSc6d1	postižené
hejnu	hejno	k1gNnSc6	hejno
výkrmových	výkrmový	k2eAgNnPc2d1	výkrmový
kuřat	kuře	k1gNnPc2	kuře
se	se	k3xPyFc4	se
nemoc	nemoc	k1gFnSc1	nemoc
projevuje	projevovat	k5eAaImIp3nS	projevovat
náhlými	náhlý	k2eAgInPc7d1	náhlý
úhyny	úhyn	k1gInPc7	úhyn
bez	bez	k7c2	bez
klinických	klinický	k2eAgInPc2d1	klinický
příznaků	příznak	k1gInPc2	příznak
během	během	k7c2	během
několika	několik	k4yIc2	několik
hodin	hodina	k1gFnPc2	hodina
(	(	kIx(	(
<g/>
enterotoxémie	enterotoxémie	k1gFnSc1	enterotoxémie
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
protrahovaným	protrahovaný	k2eAgInSc7d1	protrahovaný
průběhem	průběh	k1gInSc7	průběh
s	s	k7c7	s
všeobecnými	všeobecný	k2eAgInPc7d1	všeobecný
klinickými	klinický	k2eAgInPc7d1	klinický
příznaky	příznak	k1gInPc7	příznak
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
deprese	deprese	k1gFnPc4	deprese
<g/>
,	,	kIx,	,
nezájem	nezájem	k1gInSc4	nezájem
o	o	k7c4	o
krmivo	krmivo	k1gNnSc4	krmivo
<g/>
,	,	kIx,	,
polydipsie	polydipsie	k1gFnSc1	polydipsie
(	(	kIx(	(
<g/>
zvýšená	zvýšený	k2eAgFnSc1d1	zvýšená
žíznivost	žíznivost	k1gFnSc1	žíznivost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nechuť	nechuť	k1gFnSc1	nechuť
k	k	k7c3	k
pohybu	pohyb	k1gInSc3	pohyb
<g/>
,	,	kIx,	,
průjem	průjem	k1gInSc4	průjem
<g/>
,	,	kIx,	,
načepýřené	načepýřený	k2eAgNnSc1d1	načepýřené
peří	peří	k1gNnSc1	peří
<g/>
,	,	kIx,	,
zpomalení	zpomalení	k1gNnSc1	zpomalení
růstu	růst	k1gInSc2	růst
<g/>
,	,	kIx,	,
vyhublost	vyhublost	k1gFnSc4	vyhublost
a	a	k8xC	a
úhyn	úhyn	k1gInSc4	úhyn
<g/>
.	.	kIx.	.
</s>
<s>
Mortalita	mortalita	k1gFnSc1	mortalita
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
kolem	kolem	k7c2	kolem
5-20	[number]	k4	5-20
%	%	kIx~	%
a	a	k8xC	a
kulminuje	kulminovat	k5eAaImIp3nS	kulminovat
v	v	k7c6	v
prvních	první	k4xOgFnPc6	první
10-14	[number]	k4	10-14
dnech	den	k1gInPc6	den
po	po	k7c6	po
výskytu	výskyt	k1gInSc6	výskyt
<g/>
.	.	kIx.	.
</s>
<s>
Změny	změna	k1gFnPc1	změna
jsou	být	k5eAaImIp3nP	být
zpravidla	zpravidla	k6eAd1	zpravidla
lokalizovány	lokalizovat	k5eAaBmNgFnP	lokalizovat
v	v	k7c6	v
tenkém	tenký	k2eAgNnSc6d1	tenké
střevu	střevo	k1gNnSc6	střevo
<g/>
.	.	kIx.	.
</s>
<s>
Střeva	střevo	k1gNnPc1	střevo
jsou	být	k5eAaImIp3nP	být
dilatovaná	dilatovaný	k2eAgNnPc1d1	dilatovaný
plynem	plyn	k1gInSc7	plyn
a	a	k8xC	a
řídkým	řídký	k2eAgInSc7d1	řídký
zelenavým	zelenavý	k2eAgInSc7d1	zelenavý
nebo	nebo	k8xC	nebo
červenohnědým	červenohnědý	k2eAgInSc7d1	červenohnědý
obsahem	obsah	k1gInSc7	obsah
<g/>
.	.	kIx.	.
</s>
<s>
Stěna	stěna	k1gFnSc1	stěna
střev	střevo	k1gNnPc2	střevo
je	být	k5eAaImIp3nS	být
vrásčitá	vrásčitý	k2eAgFnSc1d1	vrásčitá
a	a	k8xC	a
zesílená	zesílený	k2eAgFnSc1d1	zesílená
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
fibrinózně	fibrinózně	k6eAd1	fibrinózně
nekrotického	nekrotický	k2eAgInSc2d1	nekrotický
zánětu	zánět	k1gInSc2	zánět
a	a	k8xC	a
potažená	potažený	k2eAgFnSc1d1	potažená
volně	volně	k6eAd1	volně
nebo	nebo	k8xC	nebo
pevně	pevně	k6eAd1	pevně
lpícími	lpící	k2eAgFnPc7d1	lpící
žlutavými	žlutavý	k2eAgFnPc7d1	žlutavá
nebo	nebo	k8xC	nebo
zelenavými	zelenavý	k2eAgFnPc7d1	zelenavá
difteroidními	difteroidní	k2eAgFnPc7d1	difteroidní
membránami	membrána	k1gFnPc7	membrána
<g/>
.	.	kIx.	.
</s>
<s>
Hemoragie	hemoragie	k1gFnPc1	hemoragie
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
přítomny	přítomen	k2eAgInPc1d1	přítomen
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nejsou	být	k5eNaImIp3nP	být
dominujícím	dominující	k2eAgInSc7d1	dominující
rysem	rys	k1gInSc7	rys
<g/>
.	.	kIx.	.
</s>
<s>
Infekci	infekce	k1gFnSc4	infekce
C.	C.	kA	C.
perfringens	perfringens	k6eAd1	perfringens
mohou	moct	k5eAaImIp3nP	moct
doprovázet	doprovázet	k5eAaImF	doprovázet
zvětšená	zvětšený	k2eAgNnPc1d1	zvětšené
játra	játra	k1gNnPc1	játra
<g/>
,	,	kIx,	,
slezina	slezina	k1gFnSc1	slezina
a	a	k8xC	a
ledviny	ledvina	k1gFnPc1	ledvina
s	s	k7c7	s
nekrotickými	nekrotický	k2eAgInPc7d1	nekrotický
okrsky	okrsek	k1gInPc7	okrsek
různé	různý	k2eAgFnSc2d1	různá
velikosti	velikost	k1gFnSc2	velikost
<g/>
.	.	kIx.	.
</s>
<s>
Diagnóza	diagnóza	k1gFnSc1	diagnóza
nekrotické	nekrotický	k2eAgFnSc2d1	nekrotická
enteritidy	enteritida	k1gFnSc2	enteritida
je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
patologických	patologický	k2eAgFnPc6d1	patologická
změnách	změna	k1gFnPc6	změna
<g/>
,	,	kIx,	,
izolaci	izolace	k1gFnSc3	izolace
a	a	k8xC	a
identifikaci	identifikace	k1gFnSc3	identifikace
příčinného	příčinný	k2eAgNnSc2d1	příčinné
agens	agens	k1gNnSc2	agens
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
také	také	k9	také
prokazovat	prokazovat	k5eAaImF	prokazovat
toxin	toxin	k1gInSc4	toxin
v	v	k7c6	v
séru	sérum	k1gNnSc6	sérum
<g/>
,	,	kIx,	,
střevním	střevní	k2eAgInSc6d1	střevní
obsahu	obsah	k1gInSc6	obsah
a	a	k8xC	a
jaterním	jaterní	k2eAgInSc6d1	jaterní
homogenátu	homogenát	k1gInSc6	homogenát
<g/>
.	.	kIx.	.
</s>
<s>
Diferenciální	diferenciální	k2eAgFnSc1d1	diferenciální
diagnostika	diagnostika	k1gFnSc1	diagnostika
<g/>
.	.	kIx.	.
</s>
<s>
Nekrotickou	nekrotický	k2eAgFnSc4d1	nekrotická
enteritidu	enteritida	k1gFnSc4	enteritida
nutno	nutno	k6eAd1	nutno
odlišit	odlišit	k5eAaPmF	odlišit
zejména	zejména	k9	zejména
od	od	k7c2	od
ulcerativní	ulcerativní	k2eAgFnSc2d1	ulcerativní
enteritidy	enteritida	k1gFnSc2	enteritida
ptáků	pták	k1gMnPc2	pták
a	a	k8xC	a
infekce	infekce	k1gFnSc2	infekce
kokcidií	kokcidie	k1gFnPc2	kokcidie
Eimeria	Eimerium	k1gNnSc2	Eimerium
brunetti	brunetti	k1gNnSc2	brunetti
<g/>
.	.	kIx.	.
</s>
<s>
Biochemické	biochemický	k2eAgFnPc1d1	biochemická
charakteristiky	charakteristika	k1gFnPc1	charakteristika
C.	C.	kA	C.
colinum	colinum	k1gNnSc1	colinum
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
od	od	k7c2	od
vlastností	vlastnost	k1gFnPc2	vlastnost
C.	C.	kA	C.
perfringens	perfringens	k6eAd1	perfringens
<g/>
.	.	kIx.	.
</s>
<s>
Infekce	infekce	k1gFnSc1	infekce
E.	E.	kA	E.
brunetti	brunetti	k1gNnSc1	brunetti
se	se	k3xPyFc4	se
diagnostikují	diagnostikovat	k5eAaBmIp3nP	diagnostikovat
mikroskopicky	mikroskopicky	k6eAd1	mikroskopicky
<g/>
;	;	kIx,	;
protože	protože	k8xS	protože
ale	ale	k8xC	ale
může	moct	k5eAaImIp3nS	moct
probíhat	probíhat	k5eAaImF	probíhat
současně	současně	k6eAd1	současně
s	s	k7c7	s
nekrotickou	nekrotický	k2eAgFnSc7d1	nekrotická
enteritidou	enteritida	k1gFnSc7	enteritida
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
prokázána	prokázat	k5eAaPmNgFnS	prokázat
i	i	k9	i
obě	dva	k4xCgNnPc4	dva
agens	agens	k1gNnPc4	agens
najednou	najednou	k6eAd1	najednou
<g/>
.	.	kIx.	.
</s>
<s>
Změny	změna	k1gFnPc1	změna
při	při	k7c6	při
nekrotické	nekrotický	k2eAgFnSc6d1	nekrotická
enteritidě	enteritida	k1gFnSc6	enteritida
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
lokalizovány	lokalizovat	k5eAaBmNgFnP	lokalizovat
v	v	k7c6	v
tenkém	tenký	k2eAgNnSc6d1	tenké
střevu	střevo	k1gNnSc6	střevo
<g/>
,	,	kIx,	,
s	s	k7c7	s
minimálním	minimální	k2eAgNnSc7d1	minimální
nebo	nebo	k8xC	nebo
žádným	žádný	k3yNgNnSc7	žádný
postižením	postižení	k1gNnSc7	postižení
slepých	slepý	k2eAgNnPc2d1	slepé
střev	střevo	k1gNnPc2	střevo
a	a	k8xC	a
jater	játra	k1gNnPc2	játra
<g/>
.	.	kIx.	.
</s>
<s>
Izolace	izolace	k1gFnSc1	izolace
a	a	k8xC	a
identifikace	identifikace	k1gFnSc1	identifikace
potvrzuje	potvrzovat	k5eAaImIp3nS	potvrzovat
diagnózu	diagnóza	k1gFnSc4	diagnóza
<g/>
.	.	kIx.	.
</s>
<s>
Léčebně	léčebně	k6eAd1	léčebně
i	i	k9	i
preventivně	preventivně	k6eAd1	preventivně
lze	lze	k6eAd1	lze
aplikovat	aplikovat	k5eAaBmF	aplikovat
řadu	řada	k1gFnSc4	řada
antimikrobiálních	antimikrobiální	k2eAgFnPc2d1	antimikrobiální
látek	látka	k1gFnPc2	látka
<g/>
;	;	kIx,	;
nejhodnější	hodný	k2eAgNnSc1d3	nejhodnější
je	být	k5eAaImIp3nS	být
vycházet	vycházet	k5eAaImF	vycházet
z	z	k7c2	z
výsledků	výsledek	k1gInPc2	výsledek
testů	test	k1gMnPc2	test
citlivosti	citlivost	k1gFnSc2	citlivost
<g/>
.	.	kIx.	.
</s>
<s>
Preventivní	preventivní	k2eAgNnPc1d1	preventivní
opatření	opatření	k1gNnPc1	opatření
jsou	být	k5eAaImIp3nP	být
obdobná	obdobný	k2eAgNnPc1d1	obdobné
jako	jako	k9	jako
při	při	k7c6	při
ulcerativní	ulcerativní	k2eAgFnSc6d1	ulcerativní
enteritidě	enteritida	k1gFnSc6	enteritida
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dalších	další	k2eAgNnPc2d1	další
opatření	opatření	k1gNnPc2	opatření
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
ale	ale	k9	ale
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
variabilní	variabilní	k2eAgFnSc4d1	variabilní
účinnost	účinnost	k1gFnSc4	účinnost
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
používány	používat	k5eAaImNgInP	používat
-	-	kIx~	-
odstranění	odstranění	k1gNnSc3	odstranění
rybí	rybí	k2eAgFnSc2d1	rybí
moučky	moučka	k1gFnSc2	moučka
z	z	k7c2	z
KS	ks	kA	ks
<g/>
,	,	kIx,	,
aplikace	aplikace	k1gFnSc1	aplikace
probiotik	probiotik	k1gMnSc1	probiotik
(	(	kIx(	(
<g/>
Lactobacillus	Lactobacillus	k1gMnSc1	Lactobacillus
acidophilus	acidophilus	k1gMnSc1	acidophilus
a	a	k8xC	a
Streptococcus	Streptococcus	k1gInSc1	Streptococcus
faecium	faecium	k1gNnSc1	faecium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
medikace	medikace	k1gFnPc4	medikace
krmiv	krmivo	k1gNnPc2	krmivo
(	(	kIx(	(
<g/>
Zn-bacitracin	Znacitracin	k1gMnSc1	Zn-bacitracin
<g/>
,	,	kIx,	,
avoparcin	avoparcin	k1gMnSc1	avoparcin
<g/>
,	,	kIx,	,
linkomycin	linkomycin	k1gMnSc1	linkomycin
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
okyselení	okyselení	k1gNnSc1	okyselení
střevního	střevní	k2eAgInSc2d1	střevní
traktu	trakt	k1gInSc2	trakt
(	(	kIx(	(
<g/>
ocet	ocet	k1gInSc1	ocet
<g/>
,	,	kIx,	,
kyselina	kyselina	k1gFnSc1	kyselina
mravenčí	mravenčí	k2eAgFnSc1d1	mravenčí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ohrožených	ohrožený	k2eAgFnPc6d1	ohrožená
oblastech	oblast	k1gFnPc6	oblast
se	se	k3xPyFc4	se
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
bakteriologická	bakteriologický	k2eAgFnSc1d1	bakteriologická
kontrola	kontrola	k1gFnSc1	kontrola
krmiva	krmivo	k1gNnSc2	krmivo
a	a	k8xC	a
podestýlky	podestýlka	k1gFnSc2	podestýlka
<g/>
.	.	kIx.	.
</s>
