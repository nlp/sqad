<s>
Jean	Jean	k1gMnSc1	Jean
Baptiste	Baptist	k1gMnSc5	Baptist
Perrin	Perrin	k1gInSc4	Perrin
[	[	kIx(	[
<g/>
žán	žán	k?	žán
batist	batist	k1gInSc1	batist
perén	perén	k1gInSc1	perén
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1870	[number]	k4	1870
<g/>
,	,	kIx,	,
Lille	Lille	k1gFnSc1	Lille
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
-	-	kIx~	-
17	[number]	k4	17
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1942	[number]	k4	1942
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1926	[number]	k4	1926
obdržel	obdržet	k5eAaPmAgMnS	obdržet
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
výzkum	výzkum	k1gInSc4	výzkum
nespojitých	spojitý	k2eNgInPc2d1	nespojitý
stavů	stav	k1gInPc2	stav
hmoty	hmota	k1gFnSc2	hmota
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
za	za	k7c4	za
objev	objev	k1gInSc4	objev
sedimentační	sedimentační	k2eAgFnSc2d1	sedimentační
rovnováhy	rovnováha	k1gFnSc2	rovnováha
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
dokázal	dokázat	k5eAaPmAgMnS	dokázat
teorii	teorie	k1gFnSc4	teorie
o	o	k7c6	o
atomové	atomový	k2eAgFnSc6d1	atomová
struktuře	struktura	k1gFnSc6	struktura
hmoty	hmota	k1gFnSc2	hmota
<g/>
.	.	kIx.	.
</s>
<s>
Les	les	k1gInSc1	les
principes	principes	k1gInSc1	principes
<g/>
.	.	kIx.	.
</s>
<s>
Exposé	exposé	k1gNnSc1	exposé
de	de	k?	de
thermodynamique	thermodynamiqu	k1gInSc2	thermodynamiqu
(	(	kIx(	(
<g/>
1901	[number]	k4	1901
<g/>
)	)	kIx)	)
Traité	Traitý	k2eAgFnSc2d1	Traitý
de	de	k?	de
chimie	chimie	k1gFnSc2	chimie
physique	physiqu	k1gFnSc2	physiqu
<g/>
.	.	kIx.	.
</s>
<s>
Les	les	k1gInSc1	les
principes	principes	k1gInSc1	principes
(	(	kIx(	(
<g/>
1903	[number]	k4	1903
<g/>
)	)	kIx)	)
Les	les	k1gInSc4	les
preuves	preuvesa	k1gFnPc2	preuvesa
de	de	k?	de
la	la	k1gNnSc4	la
réalité	réalitý	k2eAgFnSc2d1	réalitý
moléculaire	moléculair	k1gInSc5	moléculair
(	(	kIx(	(
<g/>
1911	[number]	k4	1911
<g/>
)	)	kIx)	)
Les	les	k1gInSc1	les
atomes	atomes	k1gInSc1	atomes
(	(	kIx(	(
<g/>
1913	[number]	k4	1913
<g/>
,	,	kIx,	,
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
Matiè	Matiè	k1gMnSc1	Matiè
et	et	k?	et
Lumiè	Lumiè	k1gMnSc1	Lumiè
(	(	kIx(	(
<g/>
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
Les	les	k1gInSc4	les
éléments	élémentsa	k1gFnPc2	élémentsa
de	de	k?	de
la	la	k1gNnSc4	la
physique	physiqu	k1gFnSc2	physiqu
(	(	kIx(	(
<g/>
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
<g />
.	.	kIx.	.
</s>
<s>
orientation	orientation	k1gInSc4	orientation
actuelle	actuelle	k1gInSc1	actuelle
des	des	k1gNnSc1	des
sciences	sciences	k1gInSc1	sciences
(	(	kIx(	(
<g/>
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
Les	les	k1gInSc1	les
formes	formes	k1gMnSc1	formes
chimiques	chimiques	k1gMnSc1	chimiques
de	de	k?	de
transition	transition	k1gInSc1	transition
(	(	kIx(	(
<g/>
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
La	la	k1gNnSc2	la
recherche	recherch	k1gInSc2	recherch
scientifique	scientifiqu	k1gFnSc2	scientifiqu
(	(	kIx(	(
<g/>
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
Grains	Grains	k1gInSc1	Grains
de	de	k?	de
matiè	matiè	k?	matiè
et	et	k?	et
grains	grains	k1gInSc1	grains
de	de	k?	de
lumiè	lumiè	k?	lumiè
(	(	kIx(	(
<g/>
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
organisation	organisation	k1gInSc1	organisation
de	de	k?	de
la	la	k1gNnSc2	la
recherche	recherch	k1gMnSc2	recherch
scientifique	scientifiqu	k1gMnSc2	scientifiqu
en	en	k?	en
France	Franc	k1gMnSc2	Franc
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
À	À	k?	À
la	la	k1gNnSc4	la
surface	surface	k1gFnSc2	surface
des	des	k1gNnSc7	des
choses	chosesa	k1gFnPc2	chosesa
(	(	kIx(	(
<g/>
1940	[number]	k4	1940
<g/>
-	-	kIx~	-
<g/>
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
La	la	k1gNnSc6	la
science	scienec	k1gInSc2	scienec
et	et	k?	et
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
espérance	espéranec	k1gMnSc2	espéranec
(	(	kIx(	(
<g/>
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
</s>
