<s>
Colours	Colours	k6eAd1	Colours
of	of	k?	of
Ostrava	Ostrava	k1gFnSc1	Ostrava
je	být	k5eAaImIp3nS	být
multižánrový	multižánrový	k2eAgInSc1d1	multižánrový
hudební	hudební	k2eAgInSc1d1	hudební
festival	festival	k1gInSc1	festival
každoročně	každoročně	k6eAd1	každoročně
pořádaný	pořádaný	k2eAgInSc1d1	pořádaný
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
ročník	ročník	k1gInSc1	ročník
festivalu	festival	k1gInSc2	festival
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
Ostravy	Ostrava	k1gFnSc2	Ostrava
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Stodolní	stodolní	k2eAgFnSc2d1	stodolní
ulice	ulice	k1gFnSc2	ulice
<g/>
,	,	kIx,	,
na	na	k7c6	na
výstavišti	výstaviště	k1gNnSc6	výstaviště
Černá	černý	k2eAgFnSc1d1	černá
louka	louka	k1gFnSc1	louka
a	a	k8xC	a
v	v	k7c6	v
klubu	klub	k1gInSc6	klub
Boomerang	Boomerang	k1gInSc1	Boomerang
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
festival	festival	k1gInSc1	festival
rostl	růst	k5eAaImAgInS	růst
a	a	k8xC	a
ze	z	k7c2	z
Stodolní	stodolní	k2eAgFnSc2d1	stodolní
ulice	ulice	k1gFnSc2	ulice
se	se	k3xPyFc4	se
přenesl	přenést	k5eAaPmAgInS	přenést
do	do	k7c2	do
prostoru	prostor	k1gInSc2	prostor
Slezkoostravského	Slezkoostravský	k2eAgInSc2d1	Slezkoostravský
hradu	hrad	k1gInSc2	hrad
a	a	k8xC	a
na	na	k7c4	na
další	další	k2eAgNnPc4d1	další
místa	místo	k1gNnPc4	místo
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
Ostravy	Ostrava	k1gFnSc2	Ostrava
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
se	se	k3xPyFc4	se
festival	festival	k1gInSc1	festival
koná	konat	k5eAaImIp3nS	konat
v	v	k7c6	v
průmyslovém	průmyslový	k2eAgInSc6d1	průmyslový
areálu	areál	k1gInSc6	areál
národní	národní	k2eAgFnSc2d1	národní
kulturní	kulturní	k2eAgFnSc2d1	kulturní
památky	památka	k1gFnSc2	památka
Dolní	dolní	k2eAgFnSc1d1	dolní
oblast	oblast	k1gFnSc1	oblast
Vítkovice	Vítkovice	k1gInPc1	Vítkovice
nedaleko	nedaleko	k7c2	nedaleko
centra	centrum	k1gNnSc2	centrum
Ostravy	Ostrava	k1gFnSc2	Ostrava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
historii	historie	k1gFnSc6	historie
festival	festival	k1gInSc1	festival
přivezl	přivézt	k5eAaPmAgInS	přivézt
do	do	k7c2	do
Ostravy	Ostrava	k1gFnSc2	Ostrava
řadu	řad	k1gInSc2	řad
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Mika	Mik	k1gMnSc2	Mik
<g/>
,	,	kIx,	,
Björk	Björk	k1gInSc1	Björk
<g/>
,	,	kIx,	,
Kasabian	Kasabian	k1gMnSc1	Kasabian
<g/>
,	,	kIx,	,
Rudimental	Rudimental	k1gMnSc1	Rudimental
<g/>
,	,	kIx,	,
Grinderman	Grinderman	k1gMnSc1	Grinderman
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
Plant	planta	k1gFnPc2	planta
<g/>
,	,	kIx,	,
ZAZ	ZAZ	kA	ZAZ
<g/>
,	,	kIx,	,
Alanis	Alanis	k1gFnSc1	Alanis
Morissette	Morissett	k1gInSc5	Morissett
<g/>
,	,	kIx,	,
Cranberries	Cranberries	k1gInSc1	Cranberries
<g/>
,	,	kIx,	,
Sinéad	Sinéad	k1gInSc1	Sinéad
O	O	kA	O
<g/>
'	'	kIx"	'
<g/>
Connor	Connor	k1gMnSc1	Connor
<g/>
,	,	kIx,	,
Bobby	Bobba	k1gFnPc1	Bobba
McFerrin	McFerrin	k1gInSc1	McFerrin
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Mariza	Mariz	k1gMnSc4	Mariz
<g/>
,	,	kIx,	,
Salif	Salif	k1gMnSc1	Salif
Keita	Keita	k1gMnSc1	Keita
<g/>
,	,	kIx,	,
Jamie	Jamie	k1gFnSc1	Jamie
Cullum	Cullum	k1gInSc1	Cullum
<g/>
,	,	kIx,	,
Janelle	Janelle	k1gFnSc1	Janelle
Monáe	Monáe	k1gFnSc1	Monáe
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Flaming	Flaming	k1gInSc1	Flaming
Lips	Lips	k1gInSc1	Lips
<g/>
,	,	kIx,	,
Antony	anton	k1gInPc1	anton
and	and	k?	and
the	the	k?	the
Johnsons	Johnsons	k1gInSc1	Johnsons
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Garbarek	Garbarka	k1gFnPc2	Garbarka
<g/>
,	,	kIx,	,
Gipsy	Gipsa	k1gFnPc1	Gipsa
Kings	Kings	k1gInSc1	Kings
<g/>
,	,	kIx,	,
Kronos	Kronos	k1gMnSc1	Kronos
Quartet	Quartet	k1gMnSc1	Quartet
<g/>
,	,	kIx,	,
Michael	Michael	k1gMnSc1	Michael
Nyman	Nyman	k1gMnSc1	Nyman
nebo	nebo	k8xC	nebo
Animal	animal	k1gMnSc1	animal
Collective	Collectiv	k1gInSc5	Collectiv
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
kvalitní	kvalitní	k2eAgNnPc1d1	kvalitní
jména	jméno	k1gNnPc1	jméno
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
jazzu	jazz	k1gInSc2	jazz
<g/>
,	,	kIx,	,
world	world	k1gMnSc1	world
music	music	k1gMnSc1	music
<g/>
,	,	kIx,	,
rocku	rock	k1gInSc2	rock
<g/>
,	,	kIx,	,
popu	pop	k1gInSc2	pop
i	i	k8xC	i
alternativy	alternativa	k1gFnSc2	alternativa
<g/>
.	.	kIx.	.
</s>
<s>
Festival	festival	k1gInSc1	festival
nabízí	nabízet	k5eAaImIp3nS	nabízet
také	také	k9	také
doprovodný	doprovodný	k2eAgInSc1d1	doprovodný
program	program	k1gInSc1	program
-	-	kIx~	-
divadla	divadlo	k1gNnSc2	divadlo
<g/>
,	,	kIx,	,
workshopy	workshop	k1gInPc1	workshop
<g/>
,	,	kIx,	,
diskuse	diskuse	k1gFnPc1	diskuse
<g/>
,	,	kIx,	,
filmy	film	k1gInPc1	film
apod.	apod.	kA	apod.
Colours	Colours	k1gInSc1	Colours
of	of	k?	of
Ostrava	Ostrava	k1gFnSc1	Ostrava
je	být	k5eAaImIp3nS	být
držitelem	držitel	k1gMnSc7	držitel
ceny	cena	k1gFnSc2	cena
Anděl	Anděla	k1gFnPc2	Anděla
za	za	k7c4	za
Hudební	hudební	k2eAgFnSc4d1	hudební
událost	událost	k1gFnSc4	událost
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
a	a	k8xC	a
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
získal	získat	k5eAaPmAgMnS	získat
ocenění	ocenění	k1gNnSc4	ocenění
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
hudební	hudební	k2eAgFnSc4d1	hudební
akci	akce	k1gFnSc4	akce
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
a	a	k8xC	a
2006	[number]	k4	2006
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
ALMA	alma	k1gFnSc1	alma
(	(	kIx(	(
<g/>
Akropolis	Akropolis	k1gFnSc1	Akropolis
Live	Live	k1gNnSc2	Live
Music	Musice	k1gInPc2	Musice
Awards	Awardsa	k1gFnPc2	Awardsa
<g/>
)	)	kIx)	)
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
koncert	koncert	k1gInSc1	koncert
<g/>
/	/	kIx~	/
<g/>
festival	festival	k1gInSc1	festival
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
2002	[number]	k4	2002
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
ročník	ročník	k1gInSc1	ročník
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
300	[number]	k4	300
muzikantů	muzikant	k1gMnPc2	muzikant
<g />
.	.	kIx.	.
</s>
<s>
z	z	k7c2	z
50	[number]	k4	50
hudebních	hudební	k2eAgFnPc2d1	hudební
skupin	skupina	k1gFnPc2	skupina
a	a	k8xC	a
24	[number]	k4	24
DJ	DJ	kA	DJ
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
<g/>
,	,	kIx,	,
z	z	k7c2	z
počtu	počet	k1gInSc2	počet
účinkujících	účinkující	k1gMnPc2	účinkující
bylo	být	k5eAaImAgNnS	být
14	[number]	k4	14
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
<g/>
:	:	kIx,	:
Senses	Sensesa	k1gFnPc2	Sensesa
(	(	kIx(	(
<g/>
FR	fr	k0	fr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Transsylvanians	Transsylvanians	k1gInSc1	Transsylvanians
(	(	kIx(	(
<g/>
HU	hu	k0	hu
+	+	kIx~	+
DE	DE	k?	DE
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Uado	Uado	k1gMnSc1	Uado
Taraban	Taraban	k1gMnSc1	Taraban
(	(	kIx(	(
<g/>
PL	PL	kA	PL
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Carantuohill	Carantuohill	k1gMnSc1	Carantuohill
(	(	kIx(	(
<g/>
PL	PL	kA	PL
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Africa	Afric	k2eAgFnSc1d1	Africa
Mma	Mma	k1gFnSc1	Mma
-	-	kIx~	-
festival	festival	k1gInSc1	festival
se	se	k3xPyFc4	se
odehrál	odehrát	k5eAaPmAgInS	odehrát
na	na	k7c6	na
šesti	šest	k4xCc6	šest
scénách	scéna	k1gFnPc6	scéna
(	(	kIx(	(
<g/>
3	[number]	k4	3
vnější	vnější	k2eAgMnSc1d1	vnější
a	a	k8xC	a
3	[number]	k4	3
vnitřní	vnitřní	k2eAgFnSc7d1	vnitřní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
2003	[number]	k4	2003
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
ročník	ročník	k1gInSc1	ročník
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
500	[number]	k4	500
muzikantů	muzikant	k1gMnPc2	muzikant
z	z	k7c2	z
60	[number]	k4	60
hudebních	hudební	k2eAgFnPc2d1	hudební
skupin	skupina	k1gFnPc2	skupina
a	a	k8xC	a
27	[number]	k4	27
<g />
.	.	kIx.	.
</s>
<s>
DJ	DJ	kA	DJ
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
<g/>
,	,	kIx,	,
z	z	k7c2	z
počtu	počet	k1gInSc2	počet
účinkujících	účinkující	k1gMnPc2	účinkující
bylo	být	k5eAaImAgNnS	být
16	[number]	k4	16
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
<g/>
:	:	kIx,	:
Goran	Goran	k1gInSc1	Goran
Bregović	Bregović	k1gFnSc2	Bregović
&	&	k?	&
Wedding	Wedding	k1gInSc1	Wedding
and	and	k?	and
Funeral	Funeral	k1gFnSc2	Funeral
Band	banda	k1gFnPc2	banda
(	(	kIx(	(
<g/>
východní	východní	k2eAgFnSc1d1	východní
Evropa	Evropa	k1gFnSc1	Evropa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Geoffrey	Geoffrea	k1gMnSc2	Geoffrea
Oryema	Oryem	k1gMnSc2	Oryem
(	(	kIx(	(
<g/>
Uganda	Uganda	k1gFnSc1	Uganda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kosheen	Kosheen	k1gInSc1	Kosheen
(	(	kIx(	(
<g/>
GB	GB	kA	GB
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Oi-Va-Voi	Oi-Va-Voi	k1gNnSc1	Oi-Va-Voi
(	(	kIx(	(
<g/>
GB	GB	kA	GB
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Sheva	Sheva	k1gFnSc1	Sheva
(	(	kIx(	(
<g/>
Israel	Israel	k1gInSc1	Israel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Te	Te	k1gFnSc1	Te
Vaka	Vaka	k1gFnSc1	Vaka
(	(	kIx(	(
<g/>
Polynesia	Polynesia	k1gFnSc1	Polynesia
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ZikFa	ZikFa	k1gMnSc1	ZikFa
(	(	kIx(	(
<g/>
FR	fr	k0	fr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Drum	Drum	k1gMnSc1	Drum
Machina	Machina	k1gMnSc1	Machina
(	(	kIx(	(
<g/>
PL	PL	kA	PL
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Hey	Hey	k1gMnSc1	Hey
(	(	kIx(	(
<g/>
PL	PL	kA	PL
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Korai	Korai	k1gNnSc1	Korai
Öröm	Öröma	k1gFnPc2	Öröma
(	(	kIx(	(
<g/>
HU	hu	k0	hu
<g/>
)	)	kIx)	)
-	-	kIx~	-
festival	festival	k1gInSc1	festival
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
na	na	k7c6	na
deseti	deset	k4xCc6	deset
scénách	scéna	k1gFnPc6	scéna
<g/>
(	(	kIx(	(
<g/>
6	[number]	k4	6
vnějších	vnější	k2eAgFnPc2d1	vnější
a	a	k8xC	a
4	[number]	k4	4
vnitřní	vnitřní	k2eAgFnSc7d1	vnitřní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
2004	[number]	k4	2004
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
ročník	ročník	k1gInSc1	ročník
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
384	[number]	k4	384
hudebníků	hudebník	k1gMnPc2	hudebník
z	z	k7c2	z
64	[number]	k4	64
hudebních	hudební	k2eAgFnPc2d1	hudební
skupin	skupina	k1gFnPc2	skupina
and	and	k?	and
22	[number]	k4	22
DJ	DJ	kA	DJ
<g/>
'	'	kIx"	'
<g />
.	.	kIx.	.
</s>
<s>
<g/>
s	s	k7c7	s
<g/>
,	,	kIx,	,
z	z	k7c2	z
počtu	počet	k1gInSc2	počet
účinkujících	účinkující	k1gMnPc2	účinkující
bylo	být	k5eAaImAgNnS	být
26	[number]	k4	26
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
<g/>
:	:	kIx,	:
Natacha	Natacha	k1gFnSc1	Natacha
Atlas	Atlas	k1gInSc1	Atlas
(	(	kIx(	(
<g/>
GB	GB	kA	GB
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bob	Bob	k1gMnSc1	Bob
Geldof	Geldof	k1gMnSc1	Geldof
(	(	kIx(	(
<g/>
GB	GB	kA	GB
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rachid	Rachid	k1gInSc1	Rachid
Taha	Tahum	k1gNnSc2	Tahum
(	(	kIx(	(
<g/>
Algeria	Algerium	k1gNnSc2	Algerium
<g/>
/	/	kIx~	/
<g/>
FR	fr	k0	fr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Zion	Zion	k1gMnSc1	Zion
Train	Train	k1gMnSc1	Train
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
GB	GB	kA	GB
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
So	So	kA	So
Kalmery	Kalmera	k1gFnSc2	Kalmera
(	(	kIx(	(
<g/>
DR	dr	kA	dr
Congo	Congo	k1gNnSc4	Congo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Duoud	Duoud	k1gInSc1	Duoud
(	(	kIx(	(
<g/>
Algeria	Algerium	k1gNnSc2	Algerium
<g/>
/	/	kIx~	/
<g/>
FR	fr	k0	fr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kanjar	Kanjar	k1gMnSc1	Kanjar
<g/>
'	'	kIx"	'
<g/>
Oc	Oc	k1gMnSc1	Oc
(	(	kIx(	(
<g/>
FR	fr	k0	fr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Urban	Urban	k1gMnSc1	Urban
Trad	Trad	k1gMnSc1	Trad
(	(	kIx(	(
<g/>
B	B	kA	B
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Oyster	Oyster	k1gMnSc1	Oyster
band	banda	k1gFnPc2	banda
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
GB	GB	kA	GB
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
B.	B.	kA	B.
Traore	Traor	k1gInSc5	Traor
(	(	kIx(	(
<g/>
Mali	Mali	k1gNnPc3	Mali
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Elliot	Elliot	k1gMnSc1	Elliot
Murphy	Murpha	k1gFnSc2	Murpha
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
/	/	kIx~	/
<g/>
FR	fr	k0	fr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
UR	Ur	k1gInSc1	Ur
<g/>
'	'	kIx"	'
<g/>
IA	ia	k0	ia
(	(	kIx(	(
<g/>
BY	by	k9	by
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Haydamaky	Haydamak	k1gInPc1	Haydamak
(	(	kIx(	(
<g/>
UA	UA	kA	UA
<g/>
)	)	kIx)	)
-	-	kIx~	-
festival	festival	k1gInSc1	festival
se	se	k3xPyFc4	se
<g />
.	.	kIx.	.
</s>
<s>
konal	konat	k5eAaImAgInS	konat
na	na	k7c6	na
sedmi	sedm	k4xCc6	sedm
scénách	scéna	k1gFnPc6	scéna
(	(	kIx(	(
<g/>
4	[number]	k4	4
vnější	vnější	k2eAgMnSc1d1	vnější
a	a	k8xC	a
3	[number]	k4	3
vnitřní	vnitřní	k2eAgFnSc7d1	vnitřní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
2005	[number]	k4	2005
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
<g/>
ročník	ročník	k1gInSc1	ročník
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Přes	přes	k7c4	přes
700	[number]	k4	700
hudebníků	hudebník	k1gMnPc2	hudebník
z	z	k7c2	z
více	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
hudebních	hudební	k2eAgFnPc2d1	hudební
skupin	skupina	k1gFnPc2	skupina
a	a	k8xC	a
25	[number]	k4	25
DJ	DJ	kA	DJ
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
<g/>
,	,	kIx,	,
z	z	k7c2	z
počtu	počet	k1gInSc2	počet
účinkujících	účinkující	k1gMnPc2	účinkující
bylo	být	k5eAaImAgNnS	být
30	[number]	k4	30
<g />
.	.	kIx.	.
</s>
<s>
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
<g/>
:	:	kIx,	:
George	Georg	k1gMnSc2	Georg
Clinton	Clinton	k1gMnSc1	Clinton
Parliament	Parliament	k1gMnSc1	Parliament
<g/>
/	/	kIx~	/
<g/>
Funkadelic	Funkadelice	k1gFnPc2	Funkadelice
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Asian	Asian	k1gInSc1	Asian
Dub	dub	k1gInSc1	dub
Foundation	Foundation	k1gInSc1	Foundation
(	(	kIx(	(
<g/>
GB	GB	kA	GB
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Fun-Da-Mental	Fun-Da-Mental	k1gMnSc1	Fun-Da-Mental
(	(	kIx(	(
<g/>
GB	GB	kA	GB
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Transglobal	Transglobal	k1gMnSc1	Transglobal
Underground	underground	k1gInSc1	underground
(	(	kIx(	(
<g/>
GB	GB	kA	GB
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Alabama	Alabama	k1gFnSc1	Alabama
<g/>
3	[number]	k4	3
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
GB	GB	kA	GB
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mariza	Mariza	k1gFnSc1	Mariza
(	(	kIx(	(
<g/>
PT	PT	kA	PT
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Klezmatics	Klezmatics	k1gInSc1	Klezmatics
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Farlanders	Farlanders	k1gInSc1	Farlanders
(	(	kIx(	(
<g/>
RU	RU	kA	RU
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Septeto	septeto	k1gNnSc1	septeto
Nacional	Nacional	k1gFnSc2	Nacional
(	(	kIx(	(
<g/>
CU	CU	kA	CU
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Enzo	Enzo	k6eAd1	Enzo
Avitabile	Avitabila	k1gFnSc3	Avitabila
(	(	kIx(	(
<g/>
IT	IT	kA	IT
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Daara	Daara	k1gFnSc1	Daara
J	J	kA	J
<g/>
,	,	kIx,	,
Alif	Alif	k1gInSc1	Alif
(	(	kIx(	(
<g/>
SEN	sen	k1gInSc1	sen
<g/>
)	)	kIx)	)
-	-	kIx~	-
festival	festival	k1gInSc1	festival
se	se	k3xPyFc4	se
odehrál	odehrát	k5eAaPmAgInS	odehrát
na	na	k7c6	na
osmi	osm	k4xCc6	osm
scénách	scéna	k1gFnPc6	scéna
(	(	kIx(	(
<g/>
6	[number]	k4	6
vnějších	vnější	k2eAgFnPc2d1	vnější
2	[number]	k4	2
vnitřní	vnitřní	k2eAgFnSc7d1	vnitřní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
2006	[number]	k4	2006
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
ročník	ročník	k1gInSc1	ročník
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Přes	přes	k7c4	přes
750	[number]	k4	750
ze	z	k7c2	z
123	[number]	k4	123
hudebních	hudební	k2eAgFnPc2d1	hudební
skupin	skupina	k1gFnPc2	skupina
<g/>
,	,	kIx,	,
z	z	k7c2	z
počtu	počet	k1gInSc2	počet
účinkujících	účinkující	k1gMnPc2	účinkující
bylo	být	k5eAaImAgNnS	být
33	[number]	k4	33
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
<g/>
:	:	kIx,	:
Salif	Salif	k1gMnSc1	Salif
Keita	Keita	k1gMnSc1	Keita
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
Plant	planta	k1gFnPc2	planta
<g/>
,	,	kIx,	,
Gogol	Gogol	k1gMnSc1	Gogol
Bordello	Bordello	k1gNnSc1	Bordello
<g/>
,	,	kIx,	,
Resin	Resin	k2eAgInSc1d1	Resin
Dogs	Dogs	k1gInSc1	Dogs
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Frames	Frames	k1gMnSc1	Frames
<g/>
,	,	kIx,	,
Delirious	Delirious	k1gMnSc1	Delirious
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
,	,	kIx,	,
Woven	Woven	k2eAgMnSc1d1	Woven
Hand	Hand	k1gMnSc1	Hand
<g/>
,	,	kIx,	,
Cheikh	Cheikh	k1gMnSc1	Cheikh
Lo	Lo	k1gMnSc1	Lo
<g/>
,	,	kIx,	,
Senses	Senses	k1gMnSc1	Senses
<g/>
,	,	kIx,	,
Rabasa	Rabasa	k1gFnSc1	Rabasa
<g/>
,	,	kIx,	,
Oojami	Ooja	k1gFnPc7	Ooja
<g/>
,	,	kIx,	,
Mariem	Mario	k1gMnSc7	Mario
Hasan	Hasana	k1gFnPc2	Hasana
<g/>
,	,	kIx,	,
Titi	Titi	k1gNnSc1	Titi
Robin	Robina	k1gFnPc2	Robina
<g/>
,	,	kIx,	,
Mojmir	Mojmira	k1gFnPc2	Mojmira
Novakovic	Novakovice	k1gFnPc2	Novakovice
<g/>
,	,	kIx,	,
Dubioza	Dubioza	k1gFnSc1	Dubioza
<g/>
,	,	kIx,	,
Zakopower	Zakopower	k1gInSc1	Zakopower
<g/>
,	,	kIx,	,
Bingui	Bingui	k1gNnSc1	Bingui
Jaa	Jaa	k1gFnSc2	Jaa
Jammy	Jamma	k1gFnSc2	Jamma
<g/>
,	,	kIx,	,
Zagar	Zagar	k1gMnSc1	Zagar
<g/>
,	,	kIx,	,
Sergnet	Sergnet	k1gMnSc1	Sergnet
Peper	Peper	k1gMnSc1	Peper
<g/>
,	,	kIx,	,
Funset	Funset	k1gMnSc1	Funset
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Canaman	Canaman	k1gMnSc1	Canaman
<g/>
,	,	kIx,	,
Stephan	Stephan	k1gMnSc1	Stephan
Micus	Micus	k1gMnSc1	Micus
-	-	kIx~	-
festival	festival	k1gInSc1	festival
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
na	na	k7c6	na
12	[number]	k4	12
scénách	scéna	k1gFnPc6	scéna
(	(	kIx(	(
<g/>
6	[number]	k4	6
vnějších	vnější	k2eAgFnPc2d1	vnější
a	a	k8xC	a
6	[number]	k4	6
vnitřních	vnitřní	k2eAgFnPc2d1	vnitřní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
2007	[number]	k4	2007
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
ročník	ročník	k1gInSc1	ročník
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Na	na	k7c6	na
festivalu	festival	k1gInSc6	festival
se	se	k3xPyFc4	se
představili	představit	k5eAaPmAgMnP	představit
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgMnPc4d1	jiný
<g/>
:	:	kIx,	:
Marianne	Mariann	k1gInSc5	Mariann
Faithfull	Faithfull	k1gInSc1	Faithfull
<g/>
,	,	kIx,	,
Mando	Manda	k1gFnSc5	Manda
Diao	Diao	k1gMnSc1	Diao
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Bajofondo	Bajofondo	k6eAd1	Bajofondo
Tango	tango	k1gNnSc1	tango
Club	club	k1gInSc1	club
<g/>
,	,	kIx,	,
Gipsy	Gipsa	k1gFnPc1	Gipsa
Kings	Kings	k1gInSc1	Kings
<g/>
,	,	kIx,	,
Coldcut	Coldcut	k1gInSc1	Coldcut
<g/>
,	,	kIx,	,
Yungchen	Yungchen	k1gInSc1	Yungchen
Lhamo	Lhama	k1gFnSc5	Lhama
<g/>
,	,	kIx,	,
Vinicio	Vinicio	k6eAd1	Vinicio
Capossela	Capossela	k1gFnSc1	Capossela
<g/>
,	,	kIx,	,
Orange	Orange	k1gFnSc1	Orange
Blossom	Blossom	k1gInSc1	Blossom
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Idan	Idan	k1gMnSc1	Idan
Raichel	Raichel	k1gMnSc1	Raichel
Project	Project	k1gMnSc1	Project
<g/>
,	,	kIx,	,
Ba	ba	k9	ba
Cissoko	Cissoko	k1gNnSc1	Cissoko
<g/>
,	,	kIx,	,
Goran	Goran	k1gMnSc1	Goran
Bregović	Bregović	k1gMnSc1	Bregović
<g/>
,	,	kIx,	,
Richard	Richard	k1gMnSc1	Richard
Bona	bona	k1gFnSc1	bona
<g/>
,	,	kIx,	,
Balkan	Balkan	k1gInSc1	Balkan
Beat	beat	k1gInSc1	beat
Box	box	k1gInSc1	box
<g/>
,	,	kIx,	,
Watcha	Watcha	k1gFnSc1	Watcha
<g />
.	.	kIx.	.
</s>
<s>
clan	clan	k1gMnSc1	clan
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Gasparyan	Gasparyan	k1gInSc1	Gasparyan
<g/>
,	,	kIx,	,
Salsa	salsa	k1gFnSc1	salsa
Celtica	Celtica	k1gMnSc1	Celtica
<g/>
,	,	kIx,	,
Alfonso	Alfonso	k1gMnSc1	Alfonso
X	X	kA	X
<g/>
,	,	kIx,	,
OSB	OSB	kA	OSB
Crew	Crew	k1gFnPc1	Crew
<g/>
,	,	kIx,	,
CocoRosie	CocoRosie	k1gFnPc1	CocoRosie
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
Hykes	Hykes	k1gMnSc1	Hykes
<g/>
,	,	kIx,	,
Martyna	Martyna	k1gFnSc1	Martyna
Jakubowicz	Jakubowicz	k1gInSc1	Jakubowicz
<g/>
,	,	kIx,	,
Saucy	Sauc	k2eAgInPc1d1	Sauc
Monky	Monk	k1gInPc1	Monk
<g/>
.	.	kIx.	.
2008	[number]	k4	2008
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
ročník	ročník	k1gInSc1	ročník
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Na	na	k7c6	na
festivalu	festival	k1gInSc6	festival
se	se	k3xPyFc4	se
představili	představit	k5eAaPmAgMnP	představit
mimo	mimo	k7c4	mimo
<g />
.	.	kIx.	.
</s>
<s>
jiné	jiný	k2eAgFnPc1d1	jiná
<g/>
:	:	kIx,	:
Sinéad	Sinéad	k1gInSc1	Sinéad
O	O	kA	O
<g/>
'	'	kIx"	'
<g/>
Connor	Connor	k1gMnSc1	Connor
<g/>
,	,	kIx,	,
Goldfrapp	Goldfrapp	k1gMnSc1	Goldfrapp
<g/>
,	,	kIx,	,
Happy	Happa	k1gFnPc1	Happa
Mondays	Mondays	k1gInSc1	Mondays
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Garbarek	Garbarka	k1gFnPc2	Garbarka
Group	Group	k1gInSc1	Group
a	a	k8xC	a
Trilok	Trilok	k1gInSc1	Trilok
Gurtu	Gurtu	k?	Gurtu
<g/>
,	,	kIx,	,
Gogol	Gogol	k1gMnSc1	Gogol
Bordello	Bordello	k1gNnSc1	Bordello
<g/>
,	,	kIx,	,
Habib	Habib	k1gMnSc1	Habib
Koité	Koitý	k2eAgFnSc2d1	Koitý
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Dandy	dandy	k1gMnSc1	dandy
Warhols	Warhols	k1gInSc1	Warhols
<g/>
,	,	kIx,	,
Koop	Koop	k1gMnSc1	Koop
<g/>
,	,	kIx,	,
Lou	Lou	k1gMnSc1	Lou
Rhodes	Rhodes	k1gMnSc1	Rhodes
<g/>
,	,	kIx,	,
Shantel	Shantel	k1gMnSc1	Shantel
a	a	k8xC	a
Bucovina	Bucovina	k1gFnSc1	Bucovina
Club	club	k1gInSc1	club
<g />
.	.	kIx.	.
</s>
<s>
Orkestar	Orkestar	k1gInSc1	Orkestar
<g/>
,	,	kIx,	,
Daby	Daby	k1gInPc1	Daby
Toure	Tour	k1gInSc5	Tour
<g/>
,	,	kIx,	,
Hawkwind	Hawkwind	k1gMnSc1	Hawkwind
<g/>
,	,	kIx,	,
Noa	Noa	k1gMnSc1	Noa
<g/>
,	,	kIx,	,
Sergant	Sergant	k1gMnSc1	Sergant
Garcia	Garcia	k1gFnSc1	Garcia
<g/>
,	,	kIx,	,
Inga	Inga	k1gFnSc1	Inga
Liljestrom	Liljestrom	k1gInSc1	Liljestrom
<g/>
,	,	kIx,	,
Craig	Craig	k1gInSc1	Craig
Adams	Adamsa	k1gFnPc2	Adamsa
a	a	k8xC	a
The	The	k1gFnPc2	The
Higher	Highra	k1gFnPc2	Highra
Dimension	Dimension	k1gInSc1	Dimension
Praise	Praise	k1gFnPc1	Praise
<g/>
,	,	kIx,	,
Recycler	Recycler	k1gInSc1	Recycler
<g/>
,	,	kIx,	,
Tim	Tim	k?	Tim
Eriksem	Eriks	k1gInSc7	Eriks
<g/>
,	,	kIx,	,
Tanya	Tany	k2eAgFnSc1d1	Tanya
Tagaq	Tagaq	k1gFnSc1	Tagaq
<g/>
,	,	kIx,	,
Les	les	k1gInSc1	les
Boukakes	Boukakes	k1gInSc1	Boukakes
<g/>
,	,	kIx,	,
Irfan	Irfan	k1gMnSc1	Irfan
<g/>
,	,	kIx,	,
Deva	Deva	k1gMnSc1	Deva
Premal	Premal	k1gMnSc1	Premal
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
Miten	Miten	k1gInSc1	Miten
<g/>
,	,	kIx,	,
Tosh	Tosh	k1gInSc1	Tosh
Meets	Meetsa	k1gFnPc2	Meetsa
Marley	Marlea	k1gFnSc2	Marlea
<g/>
,	,	kIx,	,
A	a	k8xC	a
Filleta	Filleta	k1gFnSc1	Filleta
<g/>
,	,	kIx,	,
a	a	k8xC	a
mnoho	mnoho	k6eAd1	mnoho
dalších	další	k2eAgInPc2d1	další
<g/>
.	.	kIx.	.
2009	[number]	k4	2009
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
ročník	ročník	k1gInSc1	ročník
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
99	[number]	k4	99
hudebních	hudební	k2eAgFnPc2d1	hudební
skupin	skupina	k1gFnPc2	skupina
(	(	kIx(	(
<g/>
35	[number]	k4	35
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
<g/>
:	:	kIx,	:
Ahn	Ahn	k1gFnSc1	Ahn
Trio	trio	k1gNnSc1	trio
<g/>
,	,	kIx,	,
Asian	Asian	k1gInSc1	Asian
Dub	dub	k1gInSc1	dub
Foundation	Foundation	k1gInSc4	Foundation
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
Byrne	Byrn	k1gInSc5	Byrn
<g/>
,	,	kIx,	,
Jamie	Jamie	k1gFnPc1	Jamie
Cullum	Cullum	k1gNnSc1	Cullum
<g/>
,	,	kIx,	,
Jape	Jape	k1gNnSc1	Jape
<g/>
,	,	kIx,	,
Johnny	Johnna	k1gFnPc1	Johnna
Clegg	Clegg	k1gMnSc1	Clegg
<g/>
,	,	kIx,	,
Jon	Jon	k1gMnSc1	Jon
Anderson	Anderson	k1gMnSc1	Anderson
<g/>
,	,	kIx,	,
KTU	KTU	kA	KTU
<g/>
,	,	kIx,	,
LA-	LA-	k1gFnSc1	LA-
<g/>
33	[number]	k4	33
<g/>
,	,	kIx,	,
Michael	Michael	k1gMnSc1	Michael
Nyman	Nymana	k1gFnPc2	Nymana
Band	banda	k1gFnPc2	banda
<g/>
,	,	kIx,	,
Morcheeba	Morcheeba	k1gMnSc1	Morcheeba
<g/>
,	,	kIx,	,
Maceo	Maceo	k1gMnSc1	Maceo
Parker	Parker	k1gMnSc1	Parker
<g/>
,	,	kIx,	,
Mamady	Mamada	k1gFnPc1	Mamada
Keita	Keito	k1gNnSc2	Keito
<g/>
,	,	kIx,	,
Mercury	Mercura	k1gFnSc2	Mercura
Rev	Rev	k1gFnSc2	Rev
<g/>
,	,	kIx,	,
N.	N.	kA	N.
<g />
.	.	kIx.	.
</s>
<s>
<g/>
O.H.A.	O.H.A.	k1gMnSc1	O.H.A.
<g/>
,	,	kIx,	,
Seun	Seun	k1gMnSc1	Seun
Kuti	kout	k5eAaImNgMnP	kout
&	&	k?	&
Egypt	Egypt	k1gInSc1	Egypt
80	[number]	k4	80
<g/>
,	,	kIx,	,
Speed	Speed	k1gMnSc1	Speed
Caravan	Caravan	k1gMnSc1	Caravan
<g/>
,	,	kIx,	,
Stereo	stereo	k2eAgMnSc1d1	stereo
MCs	MCs	k1gMnSc1	MCs
<g/>
,	,	kIx,	,
Diwan	Diwan	k1gMnSc1	Diwan
Project	Project	k1gMnSc1	Project
<g/>
,	,	kIx,	,
Nina	Nina	k1gFnSc1	Nina
Stiller	Stiller	k1gInSc1	Stiller
<g/>
,	,	kIx,	,
Dikanda	Dikanda	k1gFnSc1	Dikanda
<g/>
,	,	kIx,	,
Glenn	Glenn	k1gNnSc1	Glenn
Kaiser	Kaisra	k1gFnPc2	Kaisra
Band	banda	k1gFnPc2	banda
<g/>
,	,	kIx,	,
The	The	k1gFnPc2	The
Violet	Violeta	k1gFnPc2	Violeta
Burning	Burning	k1gInSc1	Burning
<g/>
,	,	kIx,	,
Terl	Terl	k1gMnSc1	Terl
Bryant	Bryant	k1gMnSc1	Bryant
&	&	k?	&
Red	Red	k1gMnSc1	Red
Drum	Drum	k1gMnSc1	Drum
<g/>
,	,	kIx,	,
Svjata	Svjat	k2eAgFnSc1d1	Svjat
Vatra	vatra	k1gFnSc1	vatra
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
a	a	k8xC	a
mnoho	mnoho	k6eAd1	mnoho
dalších	další	k2eAgInPc2d1	další
<g/>
.	.	kIx.	.
2010	[number]	k4	2010
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
ročník	ročník	k1gInSc1	ročník
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
154	[number]	k4	154
hudebních	hudební	k2eAgFnPc2d1	hudební
skupin	skupina	k1gFnPc2	skupina
mezi	mezi	k7c7	mezi
kterými	který	k3yIgInPc7	který
byli	být	k5eAaImAgMnP	být
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
Iggy	Igga	k1gFnPc1	Igga
&	&	k?	&
The	The	k1gMnSc1	The
Stooges	Stooges	k1gMnSc1	Stooges
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Cranberries	Cranberries	k1gMnSc1	Cranberries
<g/>
,	,	kIx,	,
Regina	Regina	k1gFnSc1	Regina
Spektor	Spektor	k1gMnSc1	Spektor
<g/>
,	,	kIx,	,
Afro	Afro	k1gMnSc1	Afro
Celt	celta	k1gFnPc2	celta
Sound	Sound	k1gMnSc1	Sound
System	Syst	k1gMnSc7	Syst
<g/>
,	,	kIx,	,
The	The	k1gMnSc7	The
Gypsy	gyps	k1gInPc1	gyps
Queens	Queens	k1gInSc1	Queens
and	and	k?	and
<g />
.	.	kIx.	.
</s>
<s>
Kings	Kings	k1gInSc1	Kings
<g/>
,	,	kIx,	,
Jaga	Jaga	k1gFnSc1	Jaga
Jazzist	Jazzist	k1gInSc1	Jazzist
<g/>
,	,	kIx,	,
El	Ela	k1gFnPc2	Ela
Gran	Gran	k1gMnSc1	Gran
Silencio	Silencio	k1gMnSc1	Silencio
<g/>
,	,	kIx,	,
Erik	Erik	k1gMnSc1	Erik
Truffaz	Truffaz	k1gInSc1	Truffaz
Paris	Paris	k1gMnSc1	Paris
Project	Project	k1gMnSc1	Project
<g/>
,	,	kIx,	,
Porcupine	Porcupin	k1gMnSc5	Porcupin
Tree	Treus	k1gMnSc5	Treus
<g/>
,	,	kIx,	,
Dulsori	Dulsor	k1gMnPc1	Dulsor
<g/>
,	,	kIx,	,
Peyoti	Peyot	k1gMnPc1	Peyot
for	forum	k1gNnPc2	forum
President	president	k1gMnSc1	president
<g/>
,	,	kIx,	,
Alamaailman	Alamaailman	k1gMnSc1	Alamaailman
Vasarat	Vasarat	k1gMnSc1	Vasarat
<g/>
,	,	kIx,	,
Huong	Huong	k1gMnSc1	Huong
Thanh	Thanh	k1gMnSc1	Thanh
<g/>
,	,	kIx,	,
A	a	k8xC	a
Hawk	Hawk	k1gInSc1	Hawk
and	and	k?	and
a	a	k8xC	a
Hacksaw	Hacksaw	k1gMnSc1	Hacksaw
<g/>
,	,	kIx,	,
Valravn	Valravn	k1gMnSc1	Valravn
<g/>
,	,	kIx,	,
<g/>
Fernando	Fernanda	k1gFnSc5	Fernanda
<g />
.	.	kIx.	.
</s>
<s>
Saunders	Saunders	k1gInSc1	Saunders
(	(	kIx(	(
<g/>
US	US	kA	US
<g/>
)	)	kIx)	)
a	a	k8xC	a
mnoho	mnoho	k6eAd1	mnoho
dalších	další	k2eAgInPc2d1	další
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
ročník	ročník	k1gInSc1	ročník
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Grinderman	Grinderman	k1gMnSc1	Grinderman
(	(	kIx(	(
<g/>
UK	UK	kA	UK
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Salif	Salif	k1gMnSc1	Salif
Keita	Keita	k1gMnSc1	Keita
(	(	kIx(	(
<g/>
Mali	Mali	k1gNnSc1	Mali
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Public	publicum	k1gNnPc2	publicum
Image	image	k1gFnPc4	image
Ltd	ltd	kA	ltd
(	(	kIx(	(
<g/>
UK	UK	kA	UK
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Yann	Yann	k1gMnSc1	Yann
<g />
.	.	kIx.	.
</s>
<s>
Tiersen	Tiersen	k1gInSc1	Tiersen
(	(	kIx(	(
<g/>
Francie	Francie	k1gFnSc1	Francie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Clannad	Clannad	k1gInSc1	Clannad
(	(	kIx(	(
<g/>
Irsko	Irsko	k1gNnSc1	Irsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Balkan	Balkan	k1gInSc1	Balkan
Brass	Brass	k1gInSc1	Brass
Battle	Battle	k1gFnSc1	Battle
(	(	kIx(	(
<g/>
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
/	/	kIx~	/
Srbsko	Srbsko	k1gNnSc1	Srbsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Swans	Swans	k1gInSc1	Swans
(	(	kIx(	(
<g/>
US	US	kA	US
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Apollo	Apollo	k1gMnSc1	Apollo
440	[number]	k4	440
(	(	kIx(	(
<g/>
UK	UK	kA	UK
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Brendan	Brendan	k1gMnSc1	Brendan
Perry	Perra	k1gFnSc2	Perra
(	(	kIx(	(
<g/>
UK	UK	kA	UK
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Santigold	Santigold	k1gMnSc1	Santigold
(	(	kIx(	(
<g/>
US	US	kA	US
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Horrors	Horrors	k1gInSc1	Horrors
(	(	kIx(	(
<g/>
UK	UK	kA	UK
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Semi	Semi	k1gNnSc7	Semi
Precious	Precious	k1gInSc1	Precious
Weapons	Weapons	k1gInSc1	Weapons
(	(	kIx(	(
<g/>
US	US	kA	US
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Herbaliser	Herbaliser	k1gMnSc1	Herbaliser
(	(	kIx(	(
<g/>
UK	UK	kA	UK
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Blackfield	Blackfield	k1gMnSc1	Blackfield
(	(	kIx(	(
<g/>
UK	UK	kA	UK
<g/>
/	/	kIx~	/
<g/>
ISR	ISR	kA	ISR
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Lisa	Lisa	k1gFnSc1	Lisa
Hannigan	Hannigan	k1gInSc1	Hannigan
(	(	kIx(	(
<g/>
Irsko	Irsko	k1gNnSc1	Irsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Joan	Joan	k1gMnSc1	Joan
As	as	k1gNnSc2	as
Police	police	k1gFnSc2	police
Woman	Woman	k1gMnSc1	Woman
(	(	kIx(	(
<g/>
US	US	kA	US
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Andreya	Andrey	k2eAgFnSc1d1	Andreya
Triana	Triana	k1gFnSc1	Triana
(	(	kIx(	(
<g/>
UK	UK	kA	UK
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
N.	N.	kA	N.
<g/>
O.H.A.	O.H.A.	k1gFnSc1	O.H.A.
-	-	kIx~	-
Circus	Circus	k1gInSc1	Circus
Underground	underground	k1gInSc1	underground
(	(	kIx(	(
<g/>
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
ČR	ČR	kA	ČR
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mono	mono	k2eAgMnSc1d1	mono
(	(	kIx(	(
<g/>
Japonsko	Japonsko	k1gNnSc1	Japonsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Dubioza	Dubioza	k1gFnSc1	Dubioza
kolektiv	kolektiv	k1gInSc1	kolektiv
(	(	kIx(	(
<g/>
Bosna	Bosna	k1gFnSc1	Bosna
a	a	k8xC	a
Hercegovina	Hercegovina	k1gFnSc1	Hercegovina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Luísa	Luísa	k1gFnSc1	Luísa
Maita	Maita	k1gFnSc1	Maita
(	(	kIx(	(
<g/>
Brazílie	Brazílie	k1gFnSc1	Brazílie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bomba	bomba	k1gFnSc1	bomba
Estéreo	Estéreo	k1gMnSc1	Estéreo
(	(	kIx(	(
<g/>
Kolumbie	Kolumbie	k1gFnSc1	Kolumbie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
La	la	k1gNnSc7	la
Shica	Shicum	k1gNnSc2	Shicum
(	(	kIx(	(
<g/>
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
SMOD	SMOD	kA	SMOD
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
Mali	Mali	k1gNnPc3	Mali
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Sam	Sam	k1gMnSc1	Sam
Karpienia	Karpienium	k1gNnSc2	Karpienium
(	(	kIx(	(
<g/>
Francie	Francie	k1gFnSc1	Francie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Stephan	Stephan	k1gMnSc1	Stephan
Micus	Micus	k1gMnSc1	Micus
(	(	kIx(	(
<g/>
Německo	Německo	k1gNnSc1	Německo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
Hykes	Hykes	k1gMnSc1	Hykes
(	(	kIx(	(
<g/>
US	US	kA	US
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Moana	Moana	k1gFnSc1	Moana
and	and	k?	and
the	the	k?	the
Tribe	Trib	k1gInSc5	Trib
(	(	kIx(	(
<g/>
Nový	nový	k2eAgInSc1d1	nový
Zéland	Zéland	k1gInSc1	Zéland
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Orchestre	orchestr	k1gInSc5	orchestr
International	International	k1gFnPc6	International
du	du	k?	du
Vetex	Vetex	k1gInSc1	Vetex
(	(	kIx(	(
<g/>
Belgie	Belgie	k1gFnSc1	Belgie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Aranis	Aranis	k1gFnSc1	Aranis
(	(	kIx(	(
<g/>
Belgie	Belgie	k1gFnSc1	Belgie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Frank	Frank	k1gMnSc1	Frank
Yamma	Yammum	k1gNnSc2	Yammum
(	(	kIx(	(
<g/>
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Oudaden	Oudaden	k2eAgMnSc1d1	Oudaden
(	(	kIx(	(
<g/>
Maroko	Maroko	k1gNnSc1	Maroko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Anomie	Anomie	k1gFnSc1	Anomie
Belle	bell	k1gInSc5	bell
(	(	kIx(	(
<g/>
US	US	kA	US
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Cedric	Cedric	k1gMnSc1	Cedric
Watson	Watson	k1gMnSc1	Watson
(	(	kIx(	(
<g/>
US	US	kA	US
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Electric	Electric	k1gMnSc1	Electric
Wire	Wir	k1gFnSc2	Wir
Hustle	Hustle	k1gFnSc2	Hustle
(	(	kIx(	(
<g/>
Nový	nový	k2eAgInSc1d1	nový
Zéland	Zéland	k1gInSc1	Zéland
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Fernando	Fernanda	k1gFnSc5	Fernanda
Saunders	Saunders	k1gInSc1	Saunders
(	(	kIx(	(
<g/>
US	US	kA	US
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Hazmat	Hazmat	k1gInSc1	Hazmat
Modine	Modin	k1gInSc5	Modin
(	(	kIx(	(
<g/>
US	US	kA	US
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Nils	Nils	k1gInSc1	Nils
Petter	Petter	k1gMnSc1	Petter
Molvæ	Molvæ	k1gMnSc1	Molvæ
/	/	kIx~	/
Jan	Jan	k1gMnSc1	Jan
Bang	Bang	k1gMnSc1	Bang
(	(	kIx(	(
<g/>
US	US	kA	US
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Miles	Miles	k1gMnSc1	Miles
Benjamin	Benjamin	k1gMnSc1	Benjamin
Anthony	Anthona	k1gFnSc2	Anthona
Robinson	Robinson	k1gMnSc1	Robinson
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
US	US	kA	US
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Paramount	Paramount	k1gMnSc1	Paramount
Styles	Styles	k1gMnSc1	Styles
(	(	kIx(	(
<g/>
US	US	kA	US
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rain	Rain	k1gMnSc1	Rain
Machine	Machin	k1gInSc5	Machin
(	(	kIx(	(
<g/>
US	US	kA	US
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Roy	Roy	k1gMnSc1	Roy
Ayers	Ayers	k1gInSc1	Ayers
(	(	kIx(	(
<g/>
US	US	kA	US
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tortured	Tortured	k1gInSc1	Tortured
Soul	Soul	k1gInSc1	Soul
(	(	kIx(	(
<g/>
US	US	kA	US
<g/>
)	)	kIx)	)
a	a	k8xC	a
mnoho	mnoho	k6eAd1	mnoho
dalších	další	k2eAgInPc2d1	další
<g/>
.	.	kIx.	.
2012	[number]	k4	2012
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g />
.	.	kIx.	.
</s>
<s>
<g/>
ročník	ročník	k1gInSc1	ročník
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Alanis	Alanis	k1gFnSc1	Alanis
Morissette	Morissett	k1gInSc5	Morissett
(	(	kIx(	(
<g/>
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bobby	Bobb	k1gInPc1	Bobb
McFerrin	McFerrin	k1gInSc1	McFerrin
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Flaming	Flaming	k1gInSc1	Flaming
Lips	Lips	k1gInSc1	Lips
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Zaz	Zaz	k1gFnSc1	Zaz
(	(	kIx(	(
<g/>
Francie	Francie	k1gFnSc1	Francie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Janelle	Janelle	k1gFnSc1	Janelle
Monáe	Monáe	k1gFnSc1	Monáe
(	(	kIx(	(
<g/>
US	US	kA	US
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Antony	anton	k1gInPc7	anton
and	and	k?	and
the	the	k?	the
Johnsons	Johnsons	k1gInSc1	Johnsons
(	(	kIx(	(
<g/>
UK	UK	kA	UK
<g/>
/	/	kIx~	/
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rufus	Rufus	k1gMnSc1	Rufus
Wainwright	Wainwright	k1gMnSc1	Wainwright
and	and	k?	and
his	his	k1gNnPc2	his
Band	band	k1gInSc1	band
(	(	kIx(	(
<g/>
US	US	kA	US
/	/	kIx~	/
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Animal	animal	k1gMnSc1	animal
Collective	Collectiv	k1gInSc5	Collectiv
<g/>
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Infected	Infected	k1gInSc1	Infected
Mushroom	Mushroom	k1gInSc1	Mushroom
(	(	kIx(	(
<g/>
Izrael	Izrael	k1gInSc1	Izrael
/	/	kIx~	/
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Mogwai	Mogwa	k1gMnPc7	Mogwa
(	(	kIx(	(
<g/>
UK	UK	kA	UK
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Parov	Parov	k1gInSc1	Parov
Stelar	Stelar	k1gInSc1	Stelar
Band	band	k1gInSc1	band
(	(	kIx(	(
<g/>
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kronos	Kronos	k1gMnSc1	Kronos
Quartet	Quartet	k1gMnSc1	Quartet
/	/	kIx~	/
Kimmo	Kimma	k1gFnSc5	Kimma
Pohjonen	Pohjonen	k2eAgInSc4d1	Pohjonen
/	/	kIx~	/
Samuli	Samule	k1gFnSc4	Samule
Kosminen	Kosminna	k1gFnPc2	Kosminna
-	-	kIx~	-
Uniko	Uniko	k1gNnSc1	Uniko
(	(	kIx(	(
<g/>
USA	USA	kA	USA
/	/	kIx~	/
Finsko	Finsko	k1gNnSc1	Finsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Fink	Fink	k1gMnSc1	Fink
(	(	kIx(	(
<g/>
UK	UK	kA	UK
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Hugh	Hugh	k1gInSc1	Hugh
Masekela	Masekela	k1gFnSc1	Masekela
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
JAR	jar	k1gFnSc1	jar
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ibrahim	Ibrahim	k1gMnSc1	Ibrahim
Maalouf	Maalouf	k1gMnSc1	Maalouf
(	(	kIx(	(
<g/>
Libanon	Libanon	k1gInSc1	Libanon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Staff	Staff	k1gMnSc1	Staff
Benda	Benda	k1gMnSc1	Benda
Bilili	Bilili	k1gMnSc1	Bilili
(	(	kIx(	(
<g/>
Kongo	Kongo	k1gNnSc1	Kongo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Orquesta	Orquesta	k1gMnSc1	Orquesta
Típica	Típica	k1gMnSc1	Típica
Fernández	Fernández	k1gInSc4	Fernández
Fierro	Fierro	k1gNnSc1	Fierro
(	(	kIx(	(
<g/>
Argentina	Argentina	k1gFnSc1	Argentina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Banco	Banco	k1gMnSc1	Banco
de	de	k?	de
Gaia	Gaia	k1gMnSc1	Gaia
(	(	kIx(	(
<g/>
UK	UK	kA	UK
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Celso	Celsa	k1gFnSc5	Celsa
Piñ	Piñ	k1gFnSc6	Piñ
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Mexiko	Mexiko	k1gNnSc1	Mexiko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Hjaltalín	Hjaltalín	k1gInSc1	Hjaltalín
(	(	kIx(	(
<g/>
Island	Island	k1gInSc1	Island
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Portico	Portico	k1gMnSc1	Portico
Quartet	Quartet	k1gMnSc1	Quartet
(	(	kIx(	(
<g/>
UK	UK	kA	UK
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tamikrest	Tamikrest	k1gFnSc1	Tamikrest
(	(	kIx(	(
<g/>
Mali	Mali	k1gNnSc1	Mali
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Katzenjammer	Katzenjammer	k1gInSc1	Katzenjammer
(	(	kIx(	(
<g/>
Norsko	Norsko	k1gNnSc1	Norsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Quique	Quique	k1gFnSc1	Quique
Neira	Neira	k1gFnSc1	Neira
&	&	k?	&
Najavibes	Najavibes	k1gInSc1	Najavibes
(	(	kIx(	(
<g/>
Chile	Chile	k1gNnSc2	Chile
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Gangpol	Gangpol	k1gInSc1	Gangpol
<g />
.	.	kIx.	.
</s>
<s>
&	&	k?	&
Mit	Mit	k1gFnSc1	Mit
(	(	kIx(	(
<g/>
Francie	Francie	k1gFnSc1	Francie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ewert	Ewert	k1gInSc1	Ewert
and	and	k?	and
The	The	k1gFnSc2	The
Two	Two	k1gMnSc1	Two
Dragons	Dragons	k1gInSc1	Dragons
(	(	kIx(	(
<g/>
Estonsko	Estonsko	k1gNnSc1	Estonsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
GaBlé	GaBl	k1gMnPc5	GaBl
(	(	kIx(	(
<g/>
Francie	Francie	k1gFnSc1	Francie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
R.	R.	kA	R.
<g/>
U.T.A.	U.T.A.	k1gFnSc1	U.T.A.
<g/>
(	(	kIx(	(
<g/>
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Acollective	Acollectiv	k1gInSc5	Acollectiv
(	(	kIx(	(
<g/>
Izrael	Izrael	k1gInSc1	Izrael
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Shtetl	Shtetl	k1gMnSc1	Shtetl
Superstars	Superstarsa	k1gFnPc2	Superstarsa
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
UK	UK	kA	UK
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
,	,	kIx,	,
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
<g/>
,	,	kIx,	,
Izrael	Izrael	k1gInSc1	Izrael
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Barons	Barons	k1gInSc1	Barons
Of	Of	k1gFnSc4	Of
Tang	tango	k1gNnPc2	tango
(	(	kIx(	(
<g/>
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bassekou	Bassekou	k1gMnSc5	Bassekou
Kouyate	Kouyat	k1gMnSc5	Kouyat
&	&	k?	&
Ngoni	Ngoeň	k1gFnSc6	Ngoeň
Ba	ba	k9	ba
(	(	kIx(	(
<g/>
Mali	Mali	k1gNnSc1	Mali
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Euzen	Euzen	k1gInSc1	Euzen
(	(	kIx(	(
<g/>
Dánsko	Dánsko	k1gNnSc1	Dánsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Dánjal	Dánjal	k1gMnSc1	Dánjal
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
Faerské	Faerský	k2eAgInPc1d1	Faerský
ostrovy	ostrov	k1gInPc1	ostrov
/	/	kIx~	/
Dánsko	Dánsko	k1gNnSc1	Dánsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Sauti	Sauti	k1gNnSc7	Sauti
Sol	sol	k1gNnSc2	sol
(	(	kIx(	(
<g/>
Keňa	Keňa	k1gFnSc1	Keňa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Árstíð	Árstíð	k1gFnSc1	Árstíð
(	(	kIx(	(
<g/>
Island	Island	k1gInSc1	Island
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rubik	Rubik	k1gMnSc1	Rubik
(	(	kIx(	(
<g/>
Finsko	Finsko	k1gNnSc1	Finsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Lindigo	Lindigo	k1gMnSc1	Lindigo
(	(	kIx(	(
<g/>
Réunion	Réunion	k1gInSc1	Réunion
/	/	kIx~	/
Francie	Francie	k1gFnSc1	Francie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ensemble	ensemble	k1gInSc1	ensemble
Yaman	Yaman	k1gInSc1	Yaman
(	(	kIx(	(
<g/>
Izrael	Izrael	k1gInSc1	Izrael
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Sea	Sea	k1gMnSc1	Sea
and	and	k?	and
Air	Air	k1gFnSc1	Air
(	(	kIx(	(
<g/>
Německo	Německo	k1gNnSc1	Německo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Creole	Creole	k1gFnSc2	Creole
Choir	Choir	k1gMnSc1	Choir
of	of	k?	of
Cuba	Cuba	k1gMnSc1	Cuba
(	(	kIx(	(
<g/>
Kuba	Kuba	k1gFnSc1	Kuba
/	/	kIx~	/
Haiti	Haiti	k1gNnSc1	Haiti
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Svjata	Svjat	k2eAgFnSc1d1	Svjat
Vatra	vatra	k1gFnSc1	vatra
<g/>
(	(	kIx(	(
<g/>
Estonsko	Estonsko	k1gNnSc1	Estonsko
<g/>
/	/	kIx~	/
<g/>
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
<g/>
)	)	kIx)	)
a	a	k8xC	a
mnoho	mnoho	k6eAd1	mnoho
dalších	další	k2eAgInPc2d1	další
<g/>
...	...	k?	...
2013	[number]	k4	2013
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
<g />
.	.	kIx.	.
</s>
<s>
ročník	ročník	k1gInSc1	ročník
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Sigur	Sigur	k1gMnSc1	Sigur
Rós	Rós	k1gMnSc1	Rós
(	(	kIx(	(
<g/>
Island	Island	k1gInSc1	Island
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jamie	Jamie	k1gFnSc1	Jamie
Cullum	Cullum	k1gInSc1	Cullum
(	(	kIx(	(
<g/>
UK	UK	kA	UK
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
xx	xx	k?	xx
(	(	kIx(	(
<g/>
UK	UK	kA	UK
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Knife	Knife	k?	Knife
(	(	kIx(	(
<g/>
Švédsko	Švédsko	k1gNnSc1	Švédsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tomahawk	tomahawk	k1gInSc1	tomahawk
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Damien	Damien	k2eAgInSc1d1	Damien
Rice	Rice	k1gInSc1	Rice
(	(	kIx(	(
<g/>
Irsko	Irsko	k1gNnSc1	Irsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bonobo	Bonoba	k1gFnSc5	Bonoba
(	(	kIx(	(
<g/>
UK	UK	kA	UK
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Asaf	Asaf	k1gMnSc1	Asaf
Avidan	Avidan	k1gMnSc1	Avidan
(	(	kIx(	(
<g/>
Izrael	Izrael	k1gInSc1	Izrael
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Woodkid	Woodkid	k1gInSc1	Woodkid
(	(	kIx(	(
<g/>
Francie	Francie	k1gFnSc1	Francie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Dub	Dub	k1gMnSc1	Dub
FX	FX	kA	FX
(	(	kIx(	(
<g/>
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Devendra	Devendra	k1gFnSc1	Devendra
Banhart	Banhart	k1gInSc1	Banhart
(	(	kIx(	(
<g/>
US	US	kA	US
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tiken	Tiken	k2eAgMnSc1d1	Tiken
Jah	Jah	k1gMnSc1	Jah
Fakoly	Fakola	k1gFnSc2	Fakola
(	(	kIx(	(
<g/>
Pobřeží	pobřeží	k1gNnSc1	pobřeží
slonoviny	slonovina	k1gFnSc2	slonovina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rokia	Rokius	k1gMnSc2	Rokius
Traoré	Traorý	k2eAgFnSc2d1	Traorý
(	(	kIx(	(
<g/>
Mali	Mali	k1gNnSc7	Mali
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Inspiral	Inspiral	k1gFnSc1	Inspiral
Carpets	Carpetsa	k1gFnPc2	Carpetsa
(	(	kIx(	(
<g/>
UK	UK	kA	UK
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
například	například	k6eAd1	například
Jon	Jon	k1gMnSc1	Jon
Hassell	Hassell	k1gMnSc1	Hassell
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Sara	Sara	k1gMnSc1	Sara
Tavares	Tavares	k1gMnSc1	Tavares
(	(	kIx(	(
<g/>
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Submotion	Submotion	k1gInSc1	Submotion
Orchestra	orchestra	k1gFnSc1	orchestra
<g/>
(	(	kIx(	(
<g/>
UK	UK	kA	UK
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Acoustic	Acoustice	k1gFnPc2	Acoustice
Africa	Afric	k1gInSc2	Afric
(	(	kIx(	(
<g/>
Pobřeží	pobřeží	k1gNnSc2	pobřeží
slonoviny	slonovina	k1gFnSc2	slonovina
<g/>
,	,	kIx,	,
Kamerun	Kamerun	k1gInSc1	Kamerun
<g/>
,	,	kIx,	,
Mali	Mali	k1gNnSc1	Mali
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Fanfara	Fanfara	k1gFnSc1	Fanfara
Tirana	Tirana	k1gFnSc1	Tirana
meets	meets	k6eAd1	meets
Transglobal	Transglobal	k1gMnSc2	Transglobal
<g />
.	.	kIx.	.
</s>
<s>
Underground	underground	k1gInSc1	underground
(	(	kIx(	(
<g/>
UK	UK	kA	UK
/	/	kIx~	/
Albánie	Albánie	k1gFnSc1	Albánie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Balkan	Balkan	k1gInSc1	Balkan
Beat	beat	k1gInSc1	beat
Box	box	k1gInSc1	box
(	(	kIx(	(
<g/>
Izrael	Izrael	k1gInSc1	Izrael
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Botanica	Botanica	k1gMnSc1	Botanica
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Girls	girl	k1gFnPc4	girl
Against	Against	k1gFnSc1	Against
Boys	boy	k1gMnPc2	boy
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Bots	Bots	k1gInSc1	Bots
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Dr	dr	kA	dr
Meaker	Meaker	k1gInSc1	Meaker
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
UK	UK	kA	UK
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Maria	Maria	k1gFnSc1	Maria
Peszek	Peszek	k1gInSc1	Peszek
(	(	kIx(	(
<g/>
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
Sam	Sam	k1gMnSc1	Sam
Lee	Lea	k1gFnSc6	Lea
(	(	kIx(	(
<g/>
UK	UK	kA	UK
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Aziz	Aziz	k1gMnSc1	Aziz
Sahmaoui	Sahmaou	k1gFnSc2	Sahmaou
&	&	k?	&
University	universita	k1gFnSc2	universita
of	of	k?	of
Gnawa	Gnawa	k1gFnSc1	Gnawa
(	(	kIx(	(
<g/>
Maroko	Maroko	k1gNnSc1	Maroko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Savina	Savina	k1gFnSc1	Savina
Yannatou	Yannatý	k2eAgFnSc4d1	Yannatý
&	&	k?	&
Primavera	Primavero	k1gNnSc2	Primavero
en	en	k?	en
Salonic	Salonice	k1gFnPc2	Salonice
(	(	kIx(	(
<g/>
Řecko	Řecko	k1gNnSc1	Řecko
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Dhafer	Dhafer	k1gMnSc1	Dhafer
Youssef	Youssef	k1gMnSc1	Youssef
(	(	kIx(	(
<g/>
Tunisko	Tunisko	k1gNnSc1	Tunisko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Amparo	Ampara	k1gFnSc5	Ampara
Sánchez	Sánchez	k1gInSc1	Sánchez
<g/>
(	(	kIx(	(
<g/>
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kumbia	Kumbia	k1gFnSc1	Kumbia
Queers	Queers	k1gInSc1	Queers
(	(	kIx(	(
<g/>
Argentina	Argentina	k1gFnSc1	Argentina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Anna	Anna	k1gFnSc1	Anna
Maria	Maria	k1gFnSc1	Maria
Jopek	Jopek	k1gInSc1	Jopek
(	(	kIx(	(
<g/>
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mama	mama	k1gFnSc1	mama
Marjas	Marjas	k1gMnSc1	Marjas
(	(	kIx(	(
<g/>
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Irie	Irie	k1gNnSc1	Irie
Révoltés	Révoltésa	k1gFnPc2	Révoltésa
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
Německo	Německo	k1gNnSc1	Německo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Duke	Duke	k1gInSc1	Duke
Special	Special	k1gInSc1	Special
(	(	kIx(	(
<g/>
Severní	severní	k2eAgNnSc1d1	severní
Irsko	Irsko	k1gNnSc1	Irsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mich	Mich	k1gInSc1	Mich
Gerber	gerbera	k1gFnPc2	gerbera
(	(	kIx(	(
<g/>
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Gin	gin	k1gInSc1	gin
Ga	Ga	k1gFnSc1	Ga
(	(	kIx(	(
<g/>
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Russkaja	Russkaja	k1gFnSc1	Russkaja
(	(	kIx(	(
<g/>
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
celá	celý	k2eAgFnSc1d1	celá
Drive	drive	k1gInSc4	drive
stage	stag	k1gInSc2	stag
se	s	k7c7	s
sedmnácti	sedmnáct	k4xCc7	sedmnáct
kapelami	kapela	k1gFnPc7	kapela
z	z	k7c2	z
<g />
.	.	kIx.	.
</s>
<s>
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
2014	[number]	k4	2014
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
ročník	ročník	k1gInSc1	ročník
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Robert	Robert	k1gMnSc1	Robert
Plant	planta	k1gFnPc2	planta
(	(	kIx(	(
<g/>
UK	UK	kA	UK
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
National	National	k1gMnSc1	National
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ZAZ	ZAZ	kA	ZAZ
(	(	kIx(	(
<g/>
Francie	Francie	k1gFnSc1	Francie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
MGMT	MGMT	kA	MGMT
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bastille	Bastille	k1gFnSc1	Bastille
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
UK	UK	kA	UK
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Newman	Newman	k1gMnSc1	Newman
(	(	kIx(	(
<g/>
UK	UK	kA	UK
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Butler	Butler	k1gInSc1	Butler
Trio	trio	k1gNnSc1	trio
(	(	kIx(	(
<g/>
Austrálie	Austrálie	k1gFnSc1	Austrálie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Angélique	Angélique	k1gFnSc1	Angélique
Kidjo	Kidjo	k1gMnSc1	Kidjo
(	(	kIx(	(
<g/>
Benin	Benin	k1gMnSc1	Benin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Chet	Chet	k2eAgInSc1d1	Chet
Faker	Faker	k1gInSc1	Faker
(	(	kIx(	(
<g/>
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Emilíana	Emilíana	k1gFnSc1	Emilíana
Torrini	Torrin	k1gMnPc1	Torrin
(	(	kIx(	(
<g/>
Island	Island	k1gInSc1	Island
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Trentemø	Trentemø	k1gMnPc5	Trentemø
(	(	kIx(	(
<g/>
Dánsko	Dánsko	k1gNnSc4	Dánsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Seasick	Seasick	k1gMnSc1	Seasick
Steve	Steve	k1gMnSc1	Steve
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jamie	Jamie	k1gFnSc1	Jamie
Woon	Woon	k1gMnSc1	Woon
(	(	kIx(	(
<g/>
UK	UK	kA	UK
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
The	The	k1gMnPc1	The
Asteroids	Asteroidsa	k1gFnPc2	Asteroidsa
Galaxy	Galax	k1gInPc4	Galax
Tour	Tour	k1gInSc1	Tour
(	(	kIx(	(
<g/>
Dánsko	Dánsko	k1gNnSc1	Dánsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ólafur	Ólafur	k1gMnSc1	Ólafur
Arnalds	Arnalds	k1gInSc1	Arnalds
(	(	kIx(	(
<g/>
Island	Island	k1gInSc1	Island
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
MØ	MØ	k1gFnSc1	MØ
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Dánsko	Dánsko	k1gNnSc1	Dánsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Grant	grant	k1gInSc1	grant
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Shaka	Shaka	k1gMnSc1	Shaka
Ponk	ponk	k1gInSc1	ponk
(	(	kIx(	(
<g/>
Francie	Francie	k1gFnSc1	Francie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Charles	Charles	k1gMnSc1	Charles
Bradley	Bradlea	k1gFnSc2	Bradlea
And	Anda	k1gFnPc2	Anda
His	his	k1gNnSc1	his
Extraordinaires	Extraordinaires	k1gMnSc1	Extraordinaires
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Goat	Goat	k1gInSc1	Goat
(	(	kIx(	(
<g/>
Švédsko	Švédsko	k1gNnSc1	Švédsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Les	les	k1gInSc1	les
Tambours	Tambours	k1gInSc1	Tambours
du	du	k?	du
Bronx	Bronx	k1gInSc1	Bronx
(	(	kIx(	(
<g/>
Francie	Francie	k1gFnSc1	Francie
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Hidden	Hiddno	k1gNnPc2	Hiddno
Orchestra	orchestra	k1gFnSc1	orchestra
(	(	kIx(	(
<g/>
UK	UK	kA	UK
<g/>
)	)	kIx)	)
2015	[number]	k4	2015
(	(	kIx(	(
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
ročník	ročník	k1gInSc1	ročník
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Björk	Björk	k1gInSc1	Björk
(	(	kIx(	(
<g/>
Island	Island	k1gInSc1	Island
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kasabian	Kasabian	k1gMnSc1	Kasabian
(	(	kIx(	(
<g/>
UK	UK	kA	UK
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rudimental	Rudimental	k1gMnSc1	Rudimental
(	(	kIx(	(
<g/>
UK	UK	kA	UK
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
St.	st.	kA	st.
Vincent	Vincent	k1gMnSc1	Vincent
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mika	Mik	k1gMnSc2	Mik
(	(	kIx(	(
<g/>
UK	UK	kA	UK
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Caribou	Cariba	k1gMnSc7	Cariba
(	(	kIx(	(
<g/>
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Clean	Clean	k1gMnSc1	Clean
Bandit	Bandit	k1gMnSc1	Bandit
(	(	kIx(	(
<g/>
UK	UK	kA	UK
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
José	José	k1gNnSc1	José
González	González	k1gInSc1	González
(	(	kIx(	(
<g/>
Švédsko	Švédsko	k1gNnSc1	Švédsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Cinematic	Cinematice	k1gFnPc2	Cinematice
Orchestra	orchestra	k1gFnSc1	orchestra
(	(	kIx(	(
<g/>
UK	UK	kA	UK
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rodrigo	Rodrigo	k1gMnSc1	Rodrigo
<g />
.	.	kIx.	.
</s>
<s>
y	y	k?	y
Gabriela	Gabriela	k1gFnSc1	Gabriela
(	(	kIx(	(
<g/>
Mexiko	Mexiko	k1gNnSc1	Mexiko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Klangkarussell	Klangkarussell	k1gInSc1	Klangkarussell
(	(	kIx(	(
<g/>
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
HVOB	HVOB	kA	HVOB
(	(	kIx(	(
<g/>
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Swans	Swans	k1gInSc1	Swans
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
Návštěvnost	návštěvnost	k1gFnSc1	návštěvnost
na	na	k7c6	na
festivalu	festival	k1gInSc6	festival
se	se	k3xPyFc4	se
neustále	neustále	k6eAd1	neustále
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
umožněno	umožnit	k5eAaPmNgNnS	umožnit
především	především	k9	především
přesunem	přesun	k1gInSc7	přesun
do	do	k7c2	do
Dolní	dolní	k2eAgFnSc2d1	dolní
oblasti	oblast	k1gFnSc2	oblast
Vítkovice	Vítkovice	k1gInPc1	Vítkovice
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nabízí	nabízet	k5eAaImIp3nS	nabízet
zásadně	zásadně	k6eAd1	zásadně
větší	veliký	k2eAgInSc4d2	veliký
areál	areál	k1gInSc4	areál
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
Slezskoostravském	slezskoostravský	k2eAgInSc6d1	slezskoostravský
hradě	hrad	k1gInSc6	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Kemp	kemp	k1gInSc1	kemp
u	u	k7c2	u
Areálu	areál	k1gInSc2	areál
-	-	kIx~	-
kapacita	kapacita	k1gFnSc1	kapacita
je	být	k5eAaImIp3nS	být
4.000	[number]	k4	4.000
stanů	stan	k1gInPc2	stan
Kemp	kemp	k1gInSc1	kemp
u	u	k7c2	u
Slezskoostravského	slezskoostravský	k2eAgInSc2d1	slezskoostravský
hradu	hrad	k1gInSc2	hrad
-	-	kIx~	-
kapacita	kapacita	k1gFnSc1	kapacita
je	být	k5eAaImIp3nS	být
2.000	[number]	k4	2.000
stanů	stan	k1gInPc2	stan
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
festival	festival	k1gInSc4	festival
organizovaný	organizovaný	k2eAgInSc4d1	organizovaný
pořadateli	pořadatel	k1gMnSc6	pořadatel
Colours	Colours	k1gInSc1	Colours
of	of	k?	of
Ostrava	Ostrava	k1gFnSc1	Ostrava
zdarma	zdarma	k6eAd1	zdarma
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
Ostravy	Ostrava	k1gFnSc2	Ostrava
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
festival	festival	k1gInSc1	festival
je	být	k5eAaImIp3nS	být
doprovodnou	doprovodný	k2eAgFnSc4d1	doprovodná
akci	akce	k1gFnSc4	akce
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
dříve	dříve	k6eAd2	dříve
probíhala	probíhat	k5eAaImAgFnS	probíhat
zároveň	zároveň	k6eAd1	zároveň
s	s	k7c7	s
hlavním	hlavní	k2eAgInSc7d1	hlavní
festivalem	festival	k1gInSc7	festival
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
však	však	k9	však
probíhá	probíhat	k5eAaImIp3nS	probíhat
před	před	k7c7	před
jeho	jeho	k3xOp3gInSc7	jeho
začátkem	začátek	k1gInSc7	začátek
<g/>
.	.	kIx.	.
</s>
<s>
Festivalu	festival	k1gInSc3	festival
v	v	k7c6	v
ulicích	ulice	k1gFnPc6	ulice
nabízí	nabízet	k5eAaImIp3nP	nabízet
koncerty	koncert	k1gInPc1	koncert
českých	český	k2eAgMnPc2d1	český
i	i	k8xC	i
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
muzikantů	muzikant	k1gMnPc2	muzikant
<g/>
,	,	kIx,	,
pouliční	pouliční	k2eAgMnPc1d1	pouliční
divadla	divadlo	k1gNnSc2	divadlo
<g/>
,	,	kIx,	,
tanečníky	tanečník	k1gMnPc4	tanečník
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
workshopy	workshop	k1gInPc1	workshop
a	a	k8xC	a
program	program	k1gInSc1	program
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Czech	Czech	k1gInSc1	Czech
Music	Musice	k1gInPc2	Musice
Crossroads	Crossroads	k1gInSc1	Crossroads
je	být	k5eAaImIp3nS	být
showcasový	showcasový	k2eAgInSc1d1	showcasový
festival	festival	k1gInSc1	festival
pořádaný	pořádaný	k2eAgInSc1d1	pořádaný
krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
Colours	Colours	k1gInSc1	Colours
of	of	k?	of
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
,	,	kIx,	,
spojený	spojený	k2eAgInSc1d1	spojený
s	s	k7c7	s
panelovými	panelový	k2eAgFnPc7d1	panelová
diskusemi	diskuse	k1gFnPc7	diskuse
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
projektu	projekt	k1gInSc2	projekt
je	být	k5eAaImIp3nS	být
prezentace	prezentace	k1gFnSc1	prezentace
české	český	k2eAgFnSc2d1	Česká
hudební	hudební	k2eAgFnSc2d1	hudební
tvorby	tvorba	k1gFnSc2	tvorba
před	před	k7c7	před
zahraničními	zahraniční	k2eAgMnPc7d1	zahraniční
odborníky	odborník	k1gMnPc7	odborník
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
ročník	ročník	k1gInSc1	ročník
festivalu	festival	k1gInSc2	festival
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Galerie	galerie	k1gFnSc1	galerie
Colours	Colours	k1gInSc1	Colours
of	of	k?	of
Ostrava	Ostrava	k1gFnSc1	Ostrava
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc4	Commons
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Colours	Colours	k1gInSc1	Colours
of	of	k?	of
Ostrava	Ostrava	k1gFnSc1	Ostrava
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
stránka	stránka	k1gFnSc1	stránka
</s>
