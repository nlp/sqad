<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Český	český	k2eAgInSc1d1
kras	kras	k1gInSc1
byla	být	k5eAaImAgFnS
vyhlášena	vyhlásit	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1972	#num#	k4
na	na	k7c6
rozloze	rozloha	k1gFnSc6
128	#num#	k4
km²	km²	k?
k	k	k7c3
ochraně	ochrana	k1gFnSc3
nejcennější	cenný	k2eAgFnSc2d3
části	část	k1gFnSc2
barrandienské	barrandienský	k2eAgFnSc2d1
pánve	pánev	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1972	#num#	k4
Rozloha	rozloha	k1gFnSc1
</s>