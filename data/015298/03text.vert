<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Český	český	k2eAgInSc1d1
kras	kras	k1gInSc1
</s>
<s>
Zdroje	zdroj	k1gInPc1
k	k	k7c3
infoboxuChráněná	infoboxuChráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgNnPc1d1
oblast	oblast	k1gFnSc4
Český	český	k2eAgInSc1d1
krasIUCN	krasIUCN	k?
kategorie	kategorie	k1gFnSc1
V	V	kA
(	(	kIx(
<g/>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
<g/>
)	)	kIx)
Pohled	pohled	k1gInSc1
na	na	k7c4
Svatý	svatý	k2eAgMnSc1d1
Jan	Jan	k1gMnSc1
pod	pod	k7c7
SkalouZákladní	SkalouZákladný	k2eAgMnPc1d1
informace	informace	k1gFnPc4
Vyhlášení	vyhlášení	k1gNnSc1
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1972	#num#	k4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
128	#num#	k4
km²	km²	k?
Správa	správa	k1gFnSc1
</s>
<s>
Správa	správa	k1gFnSc1
CHKO	CHKO	kA
Český	český	k2eAgInSc4d1
kras	kras	k1gInSc4
Poloha	poloha	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Kraje	kraj	k1gInSc2
</s>
<s>
Praha	Praha	k1gFnSc1
a	a	k8xC
Středočeský	středočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
56	#num#	k4
<g/>
′	′	k?
<g/>
27	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
14	#num#	k4
<g/>
°	°	k?
<g/>
9	#num#	k4
<g/>
′	′	k?
<g/>
44	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Geodata	Geodat	k1gMnSc2
(	(	kIx(
<g/>
OSM	osm	k4xCc1
<g/>
)	)	kIx)
</s>
<s>
OSM	osm	k4xCc1
<g/>
,	,	kIx,
WMF	WMF	kA
</s>
<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Český	český	k2eAgInSc1d1
kras	kras	k1gInSc1
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Kód	kód	k1gInSc4
</s>
<s>
22	#num#	k4
Web	web	k1gInSc1
</s>
<s>
www.ceskykras.ochranaprirody.cz	www.ceskykras.ochranaprirody.cz	k1gMnSc1
</s>
<s>
Obrázky	obrázek	k1gInPc4
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc4
či	či	k8xC
videa	video	k1gNnPc4
v	v	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Tento	tento	k3xDgInSc1
box	box	k1gInSc1
<g/>
:	:	kIx,
zobrazit	zobrazit	k5eAaPmF
•	•	k?
diskuse	diskuse	k1gFnSc2
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
není	být	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
ozdrojován	ozdrojován	k2eAgMnSc1d1
a	a	k8xC
může	moct	k5eAaImIp3nS
tedy	tedy	k9
obsahovat	obsahovat	k5eAaImF
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yQgFnPc4,k3yRgFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
ověřit	ověřit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
Jste	být	k5eAaImIp2nP
<g/>
-li	-li	k?
s	s	k7c7
popisovaným	popisovaný	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
seznámeni	seznámen	k2eAgMnPc1d1
<g/>
,	,	kIx,
pomozte	pomoct	k5eAaPmRp2nPwC
doložit	doložit	k5eAaPmF
uvedená	uvedený	k2eAgNnPc4d1
tvrzení	tvrzení	k1gNnPc4
doplněním	doplnění	k1gNnSc7
referencí	reference	k1gFnPc2
na	na	k7c4
věrohodné	věrohodný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Mapa	mapa	k1gFnSc1
Českého	český	k2eAgInSc2d1
krasu	kras	k1gInSc2
</s>
<s>
Císařská	císařský	k2eAgFnSc1d1
rokle	rokle	k1gFnSc1
–	–	k?
(	(	kIx(
<g/>
NPR	NPR	kA
Koda	Koda	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Skály	skála	k1gFnPc1
nedaleko	nedaleko	k7c2
Svatého	svatý	k2eAgMnSc2d1
Jána	Ján	k1gMnSc2
pod	pod	k7c7
Skalou	Skala	k1gMnSc7
</s>
<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Český	český	k2eAgInSc1d1
kras	kras	k1gInSc1
byla	být	k5eAaImAgFnS
vyhlášena	vyhlásit	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1972	#num#	k4
na	na	k7c6
rozloze	rozloha	k1gFnSc6
128	#num#	k4
km²	km²	k?
k	k	k7c3
ochraně	ochrana	k1gFnSc3
nejcennější	cenný	k2eAgFnSc2d3
části	část	k1gFnSc2
barrandienské	barrandienský	k2eAgFnSc2d1
pánve	pánev	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nachází	nacházet	k5eAaImIp3nS
se	se	k3xPyFc4
ve	v	k7c6
Středočeském	středočeský	k2eAgInSc6d1
kraji	kraj	k1gInSc6
mezi	mezi	k7c7
Prahou	Praha	k1gFnSc7
(	(	kIx(
<g/>
Radotínem	Radotín	k1gInSc7
<g/>
)	)	kIx)
a	a	k8xC
Berounem	Beroun	k1gInSc7
<g/>
,	,	kIx,
turisticky	turisticky	k6eAd1
nejznámější	známý	k2eAgFnSc7d3
částí	část	k1gFnSc7
je	být	k5eAaImIp3nS
oblast	oblast	k1gFnSc1
Karlštejna	Karlštejn	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
severu	sever	k1gInSc6
sahá	sahat	k5eAaImIp3nS
až	až	k9
po	po	k7c6
Mořinu	Mořin	k1gInSc6
a	a	k8xC
Loděnici	loděnice	k1gFnSc6
<g/>
,	,	kIx,
na	na	k7c6
západě	západ	k1gInSc6
po	po	k7c4
Koněprusy	Koněprus	k1gInPc4
<g/>
,	,	kIx,
na	na	k7c6
jihu	jih	k1gInSc6
po	po	k7c4
Všeradice	Všeradice	k1gFnPc4
a	a	k8xC
Zadní	zadní	k2eAgFnPc4d1
Třebaň	Třebaň	k1gFnPc4
</s>
<s>
Charakteristika	charakteristika	k1gFnSc1
území	území	k1gNnSc2
</s>
<s>
Jde	jít	k5eAaImIp3nS
o	o	k7c6
území	území	k1gNnSc6
tvořené	tvořený	k2eAgFnSc2d1
převážně	převážně	k6eAd1
prvohorními	prvohorní	k2eAgFnPc7d1
usazeninami	usazenina	k1gFnPc7
(	(	kIx(
<g/>
vápenci	vápenec	k1gInPc7
<g/>
,	,	kIx,
břidlicemi	břidlice	k1gFnPc7
<g/>
)	)	kIx)
silurského	silurský	k2eAgMnSc2d1
a	a	k8xC
devonského	devonský	k2eAgNnSc2d1
stáří	stáří	k1gNnSc2
s	s	k7c7
četnými	četný	k2eAgInPc7d1
krasovými	krasový	k2eAgInPc7d1
jevy	jev	k1gInPc7
včetně	včetně	k7c2
jeskyní	jeskyně	k1gFnPc2
patřících	patřící	k2eAgFnPc2d1
k	k	k7c3
největším	veliký	k2eAgFnPc3d3
v	v	k7c6
Čechách	Čechy	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přes	přes	k7c4
svou	svůj	k3xOyFgFnSc4
malou	malý	k2eAgFnSc4d1
nadmořskou	nadmořský	k2eAgFnSc4d1
výšku	výška	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
pohybuje	pohybovat	k5eAaImIp3nS
od	od	k7c2
208	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
(	(	kIx(
<g/>
hladina	hladina	k1gFnSc1
Berounky	Berounka	k1gFnSc2
<g/>
)	)	kIx)
do	do	k7c2
499	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
(	(	kIx(
<g/>
vrch	vrch	k1gInSc1
Bacín	Bacín	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
se	se	k3xPyFc4
zde	zde	k6eAd1
vytvořil	vytvořit	k5eAaPmAgInS
velmi	velmi	k6eAd1
pestrý	pestrý	k2eAgInSc1d1
členitý	členitý	k2eAgInSc1d1
reliéf	reliéf	k1gInSc1
<g/>
,	,	kIx,
zejména	zejména	k9
díky	díky	k7c3
erozní	erozní	k2eAgFnSc3d1
činnosti	činnost	k1gFnSc3
Berounky	Berounka	k1gFnSc2
a	a	k8xC
jejích	její	k3xOp3gInPc2
přítoků	přítok	k1gInPc2
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gNnPc1
údolí	údolí	k1gNnPc1
mají	mít	k5eAaImIp3nP
mnohdy	mnohdy	k6eAd1
kaňonovitý	kaňonovitý	k2eAgInSc4d1
ráz	ráz	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
oblasti	oblast	k1gFnSc6
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
cenná	cenný	k2eAgFnSc1d1
teplomilná	teplomilný	k2eAgFnSc1d1
květena	květena	k1gFnSc1
i	i	k8xC
zvířena	zvířena	k1gFnSc1
<g/>
,	,	kIx,
rovněž	rovněž	k9
se	se	k3xPyFc4
zde	zde	k6eAd1
nalézá	nalézat	k5eAaImIp3nS
velké	velký	k2eAgNnSc4d1
množství	množství	k1gNnSc4
cenných	cenný	k2eAgInPc2d1
geologických	geologický	k2eAgInPc2d1
profilů	profil	k1gInPc2
a	a	k8xC
světově	světově	k6eAd1
významných	významný	k2eAgNnPc2d1
nalezišť	naleziště	k1gNnPc2
zkamenělin	zkamenělina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Lesní	lesní	k2eAgNnPc4d1
společenstva	společenstvo	k1gNnPc4
dubových	dubový	k2eAgInPc2d1
hájů	háj	k1gInPc2
s	s	k7c7
velmi	velmi	k6eAd1
bohatě	bohatě	k6eAd1
rozvinutým	rozvinutý	k2eAgNnSc7d1
bylinným	bylinný	k2eAgNnSc7d1
patrem	patro	k1gNnSc7
si	se	k3xPyFc3
mnohde	mnohde	k6eAd1
zachovala	zachovat	k5eAaPmAgFnS
svůj	svůj	k3xOyFgInSc4
přirozený	přirozený	k2eAgInSc4d1
ráz	ráz	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
nejhodnotnějších	hodnotný	k2eAgFnPc6d3
oblastech	oblast	k1gFnPc6
byla	být	k5eAaImAgFnS
vyhlášena	vyhlášen	k2eAgFnSc1d1
maloplošná	maloplošný	k2eAgFnSc1d1
zvláště	zvláště	k6eAd1
chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
<g/>
,	,	kIx,
ze	z	k7c2
kterých	který	k3yIgFnPc2,k3yRgFnPc2,k3yQgFnPc2
rozlohou	rozloha	k1gFnSc7
největší	veliký	k2eAgFnSc7d3
je	být	k5eAaImIp3nS
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
Karlštejn	Karlštejn	k1gInSc1
(	(	kIx(
<g/>
1546	#num#	k4
ha	ha	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Adresa	adresa	k1gFnSc1
správy	správa	k1gFnSc2
CHKO	CHKO	kA
</s>
<s>
Správa	správa	k1gFnSc1
CHKO	CHKO	kA
Český	český	k2eAgInSc1d1
kras	kras	k1gInSc1
</s>
<s>
267	#num#	k4
18	#num#	k4
Karlštejn	Karlštejn	k1gInSc1
I.	I.	kA
<g/>
/	/	kIx~
<g/>
85	#num#	k4
</s>
<s>
Maloplošná	Maloplošný	k2eAgNnPc1d1
zvláště	zvláště	k6eAd1
chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
na	na	k7c6
území	území	k1gNnSc6
CHKO	CHKO	kA
Český	český	k2eAgInSc1d1
kras	kras	k1gInSc1
</s>
<s>
NPR	NPR	kA
Karlštejn	Karlštejn	k1gInSc1
</s>
<s>
NPR	NPR	kA
Koda	Koda	k1gFnSc1
</s>
<s>
NPP	NPP	kA
Černé	Černé	k2eAgFnSc1d1
rokle	rokle	k1gFnSc1
</s>
<s>
NPP	NPP	kA
Klonk	Klonk	k1gMnSc1
</s>
<s>
NPP	NPP	kA
Kotýz	Kotýz	k1gMnSc1
</s>
<s>
NPP	NPP	kA
Zlatý	zlatý	k2eAgMnSc1d1
kůň	kůň	k1gMnSc1
</s>
<s>
PR	pr	k0
Karlické	Karlický	k2eAgInPc5d1
údolí	údolí	k1gNnPc1
</s>
<s>
PR	pr	k0
Klapice	Klapice	k1gFnSc2
</s>
<s>
PR	pr	k0
Kobyla	kobyla	k1gFnSc1
</s>
<s>
PR	pr	k0
Kulivá	Kulivý	k2eAgFnSc1d1
hora	hora	k1gFnSc1
</s>
<s>
PR	pr	k0
Na	na	k7c4
Voskopě	Voskopa	k1gFnSc3
</s>
<s>
PR	pr	k0
Radotínské	radotínský	k2eAgFnSc6d1
údolí	údolí	k1gNnPc1
</s>
<s>
PR	pr	k0
Staňkovka	Staňkovka	k1gFnSc1
</s>
<s>
PR	pr	k0
Tetínské	Tetínský	k2eAgFnPc5d1
skály	skála	k1gFnPc1
</s>
<s>
PR	pr	k0
Voškov	Voškov	k1gInSc1
</s>
<s>
PP	PP	kA
Hvížďalka	Hvížďalka	k1gFnSc1
</s>
<s>
PP	PP	kA
Krásná	krásný	k2eAgFnSc1d1
stráň	stráň	k1gFnSc1
</s>
<s>
PP	PP	kA
Lom	lom	k1gInSc1
Kozolupy	Kozolupa	k1gFnPc4
</s>
<s>
PP	PP	kA
Syslí	syslí	k2eAgFnPc4d1
louky	louka	k1gFnPc4
u	u	k7c2
Loděnice	loděnice	k1gFnSc2
</s>
<s>
PP	PP	kA
Špičatý	špičatý	k2eAgInSc4d1
vrch	vrch	k1gInSc4
–	–	k?
Barrandovy	Barrandov	k1gInPc4
jámy	jáma	k1gFnPc4
</s>
<s>
PP	PP	kA
Zmrzlík	zmrzlík	k1gMnSc1
</s>
<s>
Maloplošná	Maloplošný	k2eAgNnPc1d1
zvláště	zvláště	k6eAd1
chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
pod	pod	k7c4
CHKO	CHKO	kA
Český	český	k2eAgInSc4d1
kras	kras	k1gInSc4
</s>
<s>
V	v	k7c6
kompetenci	kompetence	k1gFnSc6
Správy	správa	k1gFnSc2
CHKO	CHKO	kA
Český	český	k2eAgInSc1d1
kras	kras	k1gInSc1
jsou	být	k5eAaImIp3nP
tato	tento	k3xDgNnPc1
další	další	k2eAgNnPc1d1
zvláště	zvláště	k6eAd1
chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
nacházející	nacházející	k2eAgNnPc1d1
se	se	k3xPyFc4
mimo	mimo	k7c4
CHKO	CHKO	kA
<g/>
:	:	kIx,
</s>
<s>
NPR	NPR	kA
Větrušické	Větrušický	k2eAgFnSc2d1
rokle	rokle	k1gFnSc2
</s>
<s>
NPP	NPP	kA
Medník	medník	k1gMnSc1
</s>
<s>
NPP	NPP	kA
Požáry	požár	k1gInPc1
</s>
<s>
NPP	NPP	kA
Dalejský	Dalejský	k2eAgInSc4d1
profil	profil	k1gInSc4
</s>
<s>
NPP	NPP	kA
U	u	k7c2
Nového	Nového	k2eAgInSc2d1
mlýna	mlýn	k1gInSc2
</s>
<s>
NPP	NPP	kA
Barrandovské	barrandovský	k2eAgFnSc2d1
skály	skála	k1gFnSc2
</s>
<s>
NPP	NPP	kA
Lochkovský	Lochkovský	k2eAgInSc4d1
profil	profil	k1gInSc4
</s>
<s>
NPP	NPP	kA
Cikánka	cikánka	k1gFnSc1
I	i	k9
</s>
<s>
NPP	NPP	kA
Letiště	letiště	k1gNnSc1
Letňany	Letňan	k1gMnPc4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
CHKO	CHKO	kA
Český	český	k2eAgInSc1d1
kras	kras	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Český	český	k2eAgInSc1d1
kras	kras	k1gInSc1
na	na	k7c4
OpenStreetMap	OpenStreetMap	k1gInSc4
</s>
<s>
stránky	stránka	k1gFnPc1
CHKO	CHKO	kA
Český	český	k2eAgInSc1d1
kras	kras	k1gInSc1
</s>
<s>
Rozbory	rozbor	k1gInPc1
Chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
Český	český	k2eAgInSc1d1
kras	kras	k1gInSc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
v	v	k7c6
okrese	okres	k1gInSc6
Beroun	Beroun	k1gInSc1
Přírodní	přírodní	k2eAgInPc1d1
parky	park	k1gInPc1
</s>
<s>
Hřebeny	hřeben	k1gInPc1
•	•	k?
Povodí	povodí	k1gNnSc1
Kačáku	Kačák	k1gInSc2
Chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
</s>
<s>
Brdy	Brdy	k1gInPc1
•	•	k?
Český	český	k2eAgInSc1d1
kras	kras	k1gInSc1
•	•	k?
Křivoklátsko	Křivoklátsko	k1gNnSc4
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Karlštejn	Karlštejn	k1gInSc1
•	•	k?
Koda	Kod	k1gInSc2
•	•	k?
Týřov	Týřov	k1gInSc1
•	•	k?
Vůznice	Vůznice	k1gFnSc2
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Klonk	Klonk	k1gMnSc1
•	•	k?
Kotýz	Kotýz	k1gMnSc1
•	•	k?
Zlatý	zlatý	k2eAgMnSc1d1
kůň	kůň	k1gMnSc1
Přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Jouglovka	Jouglovka	k1gFnSc1
•	•	k?
Karlické	Karlický	k2eAgNnSc4d1
údolí	údolí	k1gNnSc4
•	•	k?
Kobyla	kobyla	k1gFnSc1
•	•	k?
Na	na	k7c6
Voskopě	Voskopa	k1gFnSc6
•	•	k?
Tetínské	Tetínský	k2eAgFnSc2d1
skály	skála	k1gFnSc2
•	•	k?
Voškov	Voškov	k1gInSc1
Přírodní	přírodní	k2eAgInSc1d1
památky	památka	k1gFnSc2
</s>
<s>
Branžovy	Branžův	k2eAgInPc1d1
•	•	k?
Housina	Housin	k2eAgFnSc1d1
•	•	k?
Jindřichova	Jindřichův	k2eAgFnSc1d1
skála	skála	k1gFnSc1
•	•	k?
Lom	lom	k1gInSc1
Kozolupy	Kozolupa	k1gFnSc2
•	•	k?
Lounín	Lounín	k1gMnSc1
•	•	k?
Otmíčská	Otmíčský	k2eAgFnSc1d1
hora	hora	k1gFnSc1
•	•	k?
Stará	starý	k2eAgFnSc1d1
Ves	ves	k1gFnSc1
•	•	k?
Stroupínský	Stroupínský	k2eAgInSc1d1
potok	potok	k1gInSc1
•	•	k?
Studánky	studánka	k1gFnSc2
u	u	k7c2
Cerhovic	Cerhovice	k1gFnPc2
•	•	k?
Syslí	syslí	k2eAgFnSc2d1
louky	louka	k1gFnSc2
u	u	k7c2
Loděnice	loděnice	k1gFnSc2
•	•	k?
Špičatý	špičatý	k2eAgInSc4d1
vrch	vrch	k1gInSc4
–	–	k?
Barrandovy	Barrandov	k1gInPc4
jámy	jáma	k1gFnSc2
•	•	k?
Trubínský	Trubínský	k2eAgInSc1d1
vrch	vrch	k1gInSc1
•	•	k?
Vraní	vraní	k2eAgFnSc1d1
skála	skála	k1gFnSc1
•	•	k?
Zahořanský	Zahořanský	k2eAgMnSc1d1
stratotyp	stratotyp	k1gMnSc1
•	•	k?
Zdická	zdický	k2eAgFnSc1d1
skalka	skalka	k1gFnSc1
u	u	k7c2
Kublova	Kublův	k2eAgInSc2d1
</s>
<s>
Chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
v	v	k7c6
okrese	okres	k1gInSc6
Praha-západ	Praha-západ	k1gInSc1
Přírodní	přírodní	k2eAgInPc1d1
parky	park	k1gInPc1
</s>
<s>
Hřebeny	hřeben	k1gInPc1
•	•	k?
Povodí	povodí	k1gNnSc1
Kačáku	Kačák	k1gInSc2
•	•	k?
Střed	střed	k1gInSc1
Čech	Čechy	k1gFnPc2
Chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
</s>
<s>
Český	český	k2eAgInSc1d1
kras	kras	k1gInSc1
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Černé	Černé	k2eAgFnSc1d1
rokle	rokle	k1gFnSc1
•	•	k?
Medník	Medník	k1gInSc4
Přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Karlické	Karlický	k2eAgNnSc1d1
údolí	údolí	k1gNnSc1
•	•	k?
Kobylí	kobylí	k2eAgFnSc1d1
draha	draha	k1gFnSc1
•	•	k?
Kulivá	Kulivý	k2eAgFnSc1d1
hora	hora	k1gFnSc1
•	•	k?
Radotínské	radotínský	k2eAgNnSc4d1
údolí	údolí	k1gNnSc4
•	•	k?
Roztocký	roztocký	k2eAgInSc1d1
háj	háj	k1gInSc1
–	–	k?
Tiché	Tichá	k1gFnSc2
údolí	údolí	k1gNnSc2
•	•	k?
Zvolská	Zvolský	k2eAgFnSc1d1
homole	homole	k1gFnSc1
Přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Andělské	andělský	k2eAgInPc1d1
schody	schod	k1gInPc1
•	•	k?
Břežanské	Břežanský	k2eAgNnSc1d1
údolí	údolí	k1gNnSc1
•	•	k?
Černolické	Černolický	k2eAgFnSc2d1
skály	skála	k1gFnSc2
•	•	k?
Čičovický	Čičovický	k2eAgInSc4d1
kamýk	kamýk	k1gInSc4
•	•	k?
Hostivické	Hostivický	k2eAgInPc4d1
rybníky	rybník	k1gInPc4
•	•	k?
Kněživka	Kněživka	k1gFnSc1
•	•	k?
Krásná	krásný	k2eAgFnSc1d1
stráň	stráň	k1gFnSc1
•	•	k?
Pazderna	pazderna	k1gFnSc1
•	•	k?
Skalsko	Skalsko	k1gNnSc4
•	•	k?
Třeštibok	Třeštibok	k1gInSc1
•	•	k?
Zákolanský	Zákolanský	k2eAgInSc1d1
potok	potok	k1gInSc1
</s>
<s>
Chráněné	chráněný	k2eAgFnPc1d1
krajinné	krajinný	k2eAgFnPc1d1
oblasti	oblast	k1gFnPc1
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
Beskydy	Beskyd	k1gInPc1
•	•	k?
Bílé	bílý	k2eAgInPc1d1
Karpaty	Karpaty	k1gInPc1
•	•	k?
Blaník	Blaník	k1gInSc1
•	•	k?
Blanský	blanský	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Brdy	Brdy	k1gInPc1
•	•	k?
Broumovsko	Broumovsko	k1gNnSc4
•	•	k?
České	český	k2eAgNnSc1d1
středohoří	středohoří	k1gNnSc1
•	•	k?
Český	český	k2eAgInSc1d1
kras	kras	k1gInSc1
•	•	k?
Český	český	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Český	český	k2eAgInSc1d1
ráj	ráj	k1gInSc1
•	•	k?
Jeseníky	Jeseník	k1gInPc1
•	•	k?
Jizerské	jizerský	k2eAgFnSc2d1
hory	hora	k1gFnSc2
•	•	k?
Kokořínsko	Kokořínsko	k1gNnSc1
–	–	k?
Máchův	Máchův	k2eAgInSc1d1
kraj	kraj	k1gInSc1
•	•	k?
Křivoklátsko	Křivoklátsko	k1gNnSc1
•	•	k?
Labské	labský	k2eAgInPc1d1
pískovce	pískovec	k1gInPc1
•	•	k?
Litovelské	litovelský	k2eAgNnSc1d1
Pomoraví	Pomoraví	k1gNnSc1
•	•	k?
Lužické	lužický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
•	•	k?
Moravský	moravský	k2eAgInSc4d1
kras	kras	k1gInSc4
•	•	k?
Orlické	orlický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
•	•	k?
Pálava	Pálava	k1gFnSc1
•	•	k?
Poodří	Poodří	k1gNnPc2
•	•	k?
Slavkovský	slavkovský	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Šumava	Šumava	k1gFnSc1
•	•	k?
Třeboňsko	Třeboňsko	k1gNnSc1
•	•	k?
Žďárské	Žďárské	k2eAgInPc1d1
vrchy	vrch	k1gInPc1
•	•	k?
Železné	železný	k2eAgFnSc2d1
hory	hora	k1gFnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Česko	Česko	k1gNnSc1
|	|	kIx~
Příroda	příroda	k1gFnSc1
</s>
