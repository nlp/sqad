<s>
Wreckx-N-Effect	Wreckx-N-Effect	k1gMnSc1
</s>
<s>
Wreckx-N-EffectZákladní	Wreckx-N-EffectZákladný	k2eAgMnPc1d1
informace	informace	k1gFnSc2
Žánry	žánr	k1gInPc1
</s>
<s>
new	new	k?
jack	jack	k1gMnSc1
swinghip	swinghip	k1gMnSc1
hop	hop	k0
Aktivní	aktivní	k2eAgMnPc1d1
roky	rok	k1gInPc4
</s>
<s>
1988	#num#	k4
-	-	kIx~
1996	#num#	k4
Vydavatelé	vydavatel	k1gMnPc1
</s>
<s>
Atlantic	Atlantice	k1gFnPc2
<g/>
,	,	kIx,
Motown	Motowna	k1gFnPc2
<g/>
,	,	kIx,
MCA	MCA	kA
Dřívější	dřívější	k2eAgMnPc1d1
členové	člen	k1gMnPc1
</s>
<s>
Markell	Markell	k1gMnSc1
RileyAqil	RileyAqil	k1gMnSc1
"	"	kIx"
<g/>
A-Plus	A-Plus	k1gMnSc1
<g/>
"	"	kIx"
DavidsonBrandon	DavidsonBrandon	k1gMnSc1
Mitchell	Mitchell	k1gMnSc1
(	(	kIx(
<g/>
zesnulý	zesnulý	k1gMnSc1
<g/>
)	)	kIx)
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Wreckx-N-Effect	Wreckx-N-Effect	k1gInSc4
byla	být	k5eAaImAgFnS
americká	americký	k2eAgFnSc1d1
new	new	k?
jack	jack	k6eAd1
swingová	swingový	k2eAgFnSc1d1
a	a	k8xC
hip-hopová	hip-hopový	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
proslavila	proslavit	k5eAaPmAgFnS
dvojnásobným	dvojnásobný	k2eAgInSc7d1
platinovým	platinový	k2eAgInSc7d1
singlem	singl	k1gInSc7
„	„	k?
<g/>
Rump	Rump	k1gInSc1
Shaker	Shaker	k1gInSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
RIAA	RIAA	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Kapela	kapela	k1gFnSc1
debutovala	debutovat	k5eAaBmAgFnS
stejnojmenným	stejnojmenný	k2eAgNnSc7d1
albem	album	k1gNnSc7
Wrecks-N-Effect	Wrecks-N-Effecta	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc4,k3yIgInPc4,k3yQgInPc4
nahráli	nahrát	k5eAaPmAgMnP,k5eAaBmAgMnP
v	v	k7c6
roce	rok	k1gInSc6
1988	#num#	k4
pod	pod	k7c7
vydavatelstvím	vydavatelství	k1gNnSc7
Atlantic	Atlantice	k1gFnPc2
Records	Recordsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gFnSc4
tvorbu	tvorba	k1gFnSc4
produkoval	produkovat	k5eAaImAgMnS
Teddy	Teddy	k1gMnSc1
Riley	Riley	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejúspěšnějším	úspěšný	k2eAgNnSc7d3
albem	album	k1gNnSc7
bylo	být	k5eAaImAgNnS
Hard	Hard	k1gInSc4
or	or	k?
Smooth	Smooth	k1gInSc1
z	z	k7c2
roku	rok	k1gInSc2
1992	#num#	k4
<g/>
,	,	kIx,
na	na	k7c6
kterém	který	k3yQgInSc6,k3yIgInSc6,k3yRgInSc6
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
dvojnásobný	dvojnásobný	k2eAgInSc1d1
platinový	platinový	k2eAgInSc1d1
singl	singl	k1gInSc1
„	„	k?
<g/>
Rump	Rump	k1gInSc1
Shaker	Shaker	k1gMnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1
píseň	píseň	k1gFnSc1
„	„	k?
<g/>
New	New	k1gFnSc1
Jack	Jack	k1gMnSc1
Swing	swing	k1gInSc1
<g/>
“	“	k?
se	se	k3xPyFc4
objevila	objevit	k5eAaPmAgFnS
na	na	k7c6
fiktivním	fiktivní	k2eAgInSc6d1
rádiu	rádius	k1gInSc6
CSR	CSR	kA
103.9	103.9	k4
v	v	k7c6
počítačové	počítačový	k2eAgFnSc6d1
hře	hra	k1gFnSc6
Grand	grand	k1gMnSc1
Theft	Theft	k1gMnSc1
Auto	auto	k1gNnSc1
<g/>
:	:	kIx,
San	San	k1gMnSc1
Andreas	Andreas	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Dřívější	dřívější	k2eAgMnPc1d1
členové	člen	k1gMnPc1
</s>
<s>
Markell	Markell	k1gInSc1
Riley	Rilea	k1gFnSc2
</s>
<s>
Aqil	Aqil	k1gMnSc1
"	"	kIx"
<g/>
A-Plus	A-Plus	k1gMnSc1
<g/>
"	"	kIx"
Davidson	Davidson	k1gMnSc1
</s>
<s>
Brandon	Brandon	k1gMnSc1
Mitchell	Mitchell	k1gMnSc1
(	(	kIx(
<g/>
zesnulý	zesnulý	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Diskografie	diskografie	k1gFnSc1
</s>
<s>
Studiová	studiový	k2eAgNnPc1d1
alba	album	k1gNnPc1
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
Název	název	k1gInSc1
</s>
<s>
Vrcholová	vrcholový	k2eAgFnSc1d1
pozice	pozice	k1gFnSc1
</s>
<s>
US	US	kA
</s>
<s>
US	US	kA
Hip-Hop	Hip-Hop	k1gInSc1
</s>
<s>
1989	#num#	k4
</s>
<s>
Wrecks-N-Effect	Wrecks-N-Effect	k1gMnSc1
</s>
<s>
103	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
1992	#num#	k4
</s>
<s>
Hard	Hard	k1gMnSc1
or	or	k?
Smooth	Smooth	k1gMnSc1
</s>
<s>
9	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
1996	#num#	k4
</s>
<s>
Raps	Raps	k6eAd1
New	New	k1gFnSc1
Generation	Generation	k1gInSc1
</s>
<s>
—	—	k?
</s>
<s>
—	—	k?
</s>
<s>
"	"	kIx"
<g/>
—	—	k?
<g/>
"	"	kIx"
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
album	album	k1gNnSc1
nedostalo	dostat	k5eNaPmAgNnS
do	do	k7c2
žádného	žádný	k3yNgInSc2
žebříčku	žebříček	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
RIAA	RIAA	kA
Gold	Gold	k1gInSc1
&	&	k?
Platinum	Platinum	k1gNnSc1
Searchable	Searchable	k1gFnSc3
Database	Databasa	k1gFnSc3
-	-	kIx~
"	"	kIx"
<g/>
Rump	Rump	k1gMnSc1
Shaker	Shaker	k1gMnSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
RIAA	RIAA	kA
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Retrieved	Retrieved	k1gInSc1
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Hudba	hudba	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2164	#num#	k4
6829	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
98038240	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
136210468	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
98038240	#num#	k4
</s>
