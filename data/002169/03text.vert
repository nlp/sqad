<s>
Des	des	k1gNnSc1	des
Moines	Moinesa	k1gFnPc2	Moinesa
(	(	kIx(	(
dɨ	dɨ	k?	dɨ
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
amerického	americký	k2eAgInSc2d1	americký
státu	stát	k1gInSc2	stát
Iowa	Iow	k1gInSc2	Iow
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
název	název	k1gInSc1	název
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
francouzštiny	francouzština	k1gFnSc2	francouzština
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
znamená	znamenat	k5eAaImIp3nS	znamenat
dovětek	dovětek	k1gInSc1	dovětek
(	(	kIx(	(
<g/>
koho	kdo	k3yInSc4	kdo
<g/>
,	,	kIx,	,
čeho	co	k3yRnSc2	co
<g/>
)	)	kIx)	)
mnichů	mnich	k1gMnPc2	mnich
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1851	[number]	k4	1851
je	být	k5eAaImIp3nS	být
Des	des	k1gNnSc2	des
Moines	Moinesa	k1gFnPc2	Moinesa
zmiňováno	zmiňovat	k5eAaImNgNnS	zmiňovat
jako	jako	k9	jako
Fort	Fort	k?	Fort
Des	des	k1gNnSc1	des
Moines	Moines	k1gMnSc1	Moines
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
Pevnost	pevnost	k1gFnSc1	pevnost
mnichů	mnich	k1gMnPc2	mnich
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1857	[number]	k4	1857
byl	být	k5eAaImAgInS	být
název	název	k1gInSc1	název
zkrácen	zkrátit	k5eAaPmNgInS	zkrátit
na	na	k7c4	na
Des	des	k1gNnSc4	des
Moines	Moinesa	k1gFnPc2	Moinesa
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
také	také	k9	také
správní	správní	k2eAgNnSc1d1	správní
město	město	k1gNnSc1	město
regionu	region	k1gInSc2	region
Polk	Polka	k1gFnPc2	Polka
County	Counta	k1gFnSc2	Counta
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
necelých	celý	k2eNgInPc2d1	necelý
200	[number]	k4	200
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
soutoku	soutok	k1gInSc6	soutok
řek	řeka	k1gFnPc2	řeka
Des	des	k1gNnSc1	des
Moines	Moines	k1gMnSc1	Moines
a	a	k8xC	a
Raccoon	Raccoon	k1gMnSc1	Raccoon
v	v	k7c6	v
centrální	centrální	k2eAgFnSc6d1	centrální
Iowě	Iowa	k1gFnSc6	Iowa
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
nemá	mít	k5eNaImIp3nS	mít
nic	nic	k3yNnSc1	nic
společného	společný	k2eAgNnSc2d1	společné
s	s	k7c7	s
regionem	region	k1gInSc7	region
Des	des	k1gNnSc2	des
Moines	Moinesa	k1gFnPc2	Moinesa
County	Counta	k1gFnSc2	Counta
<g/>
,	,	kIx,	,
ležícím	ležící	k2eAgNnSc7d1	ležící
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
203	[number]	k4	203
433	[number]	k4	433
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
76,4	[number]	k4	76,4
<g/>
%	%	kIx~	%
Bílí	bílý	k2eAgMnPc1d1	bílý
Američané	Američan	k1gMnPc1	Američan
10,2	[number]	k4	10,2
<g/>
%	%	kIx~	%
Afroameričané	Afroameričan	k1gMnPc1	Afroameričan
0,5	[number]	k4	0,5
<g/>
%	%	kIx~	%
Američtí	americký	k2eAgMnPc1d1	americký
indiáni	indián	k1gMnPc1	indián
4,4	[number]	k4	4,4
<g/>
%	%	kIx~	%
Asijští	asijský	k2eAgMnPc1d1	asijský
Američané	Američan	k1gMnPc1	Američan
0,1	[number]	k4	0,1
<g/>
%	%	kIx~	%
Pacifičtí	pacifický	k2eAgMnPc1d1	pacifický
ostrované	ostrovan	k1gMnPc1	ostrovan
5,0	[number]	k4	5,0
<g/>
%	%	kIx~	%
Jiná	jiný	k2eAgFnSc1d1	jiná
rasa	rasa	k1gFnSc1	rasa
3,4	[number]	k4	3,4
<g/>
%	%	kIx~	%
Dvě	dva	k4xCgNnPc1	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
ras	ras	k1gMnSc1	ras
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
hispánského	hispánský	k2eAgMnSc2d1	hispánský
nebo	nebo	k8xC	nebo
latinskoamerického	latinskoamerický	k2eAgInSc2d1	latinskoamerický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
rasu	rasa	k1gFnSc4	rasa
<g/>
,	,	kIx,	,
tvořili	tvořit	k5eAaImAgMnP	tvořit
12,0	[number]	k4	12,0
<g/>
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Gertrude	Gertrude	k6eAd1	Gertrude
Käsebierová	Käsebierový	k2eAgFnSc1d1	Käsebierový
(	(	kIx(	(
<g/>
1852	[number]	k4	1852
-	-	kIx~	-
1934	[number]	k4	1934
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fotografka	fotografka	k1gFnSc1	fotografka
Stanley	Stanlea	k1gFnSc2	Stanlea
B.	B.	kA	B.
Prusiner	Prusiner	k1gMnSc1	Prusiner
(	(	kIx(	(
<g/>
*	*	kIx~	*
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lékař	lékař	k1gMnSc1	lékař
-	-	kIx~	-
neurolog	neurolog	k1gMnSc1	neurolog
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
Stephen	Stephen	k2eAgInSc1d1	Stephen
Collins	Collins	k1gInSc1	Collins
(	(	kIx(	(
<g/>
*	*	kIx~	*
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
Scott	Scott	k1gMnSc1	Scott
Clemmensen	Clemmensen	k2eAgMnSc1d1	Clemmensen
(	(	kIx(	(
<g/>
*	*	kIx~	*
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
hokejový	hokejový	k2eAgMnSc1d1	hokejový
brankář	brankář	k1gMnSc1	brankář
Lolo	Lolo	k6eAd1	Lolo
Jonesová	Jonesový	k2eAgFnSc1d1	Jonesová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sprinterka-překážkárka	sprinterkařekážkárka	k1gFnSc1	sprinterka-překážkárka
Shawn	Shawn	k1gInSc1	Shawn
Johnsonová	Johnsonová	k1gFnSc1	Johnsonová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sportovní	sportovní	k2eAgFnSc1d1	sportovní
gymnastka	gymnastka	k1gFnSc1	gymnastka
<g/>
,	,	kIx,	,
absolutní	absolutní	k2eAgFnSc1d1	absolutní
mistryně	mistryně	k1gFnSc1	mistryně
světa	svět	k1gInSc2	svět
a	a	k8xC	a
olympijská	olympijský	k2eAgFnSc1d1	olympijská
vítězka	vítězka	k1gFnSc1	vítězka
na	na	k7c6	na
kladině	kladina	k1gFnSc6	kladina
Corey	Corea	k1gFnSc2	Corea
Todd	Todd	k1gInSc1	Todd
Taylor	Taylor	k1gMnSc1	Taylor
(	(	kIx(	(
<g/>
*	*	kIx~	*
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hudebník	hudebník	k1gMnSc1	hudebník
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
skupiny	skupina	k1gFnSc2	skupina
Slipknot	Slipknota	k1gFnPc2	Slipknota
Sid	Sid	k1gMnSc1	Sid
Wilson	Wilson	k1gMnSc1	Wilson
(	(	kIx(	(
<g/>
*	*	kIx~	*
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
,	,	kIx,	,
Dj	Dj	k1gMnSc1	Dj
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
skupiny	skupina	k1gFnSc2	skupina
Slipknot	Slipknota	k1gFnPc2	Slipknota
Catanzaro	Catanzara	k1gFnSc5	Catanzara
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc2	Itálie
Čerkasy	Čerkasa	k1gFnSc2	Čerkasa
<g/>
,	,	kIx,	,
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
Gent	Gent	k1gInSc1	Gent
<g/>
,	,	kIx,	,
Belgie	Belgie	k1gFnSc1	Belgie
Kófu	Kófus	k1gInSc2	Kófus
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc1	Japonsko
Naucalpan	Naucalpana	k1gFnPc2	Naucalpana
<g/>
,	,	kIx,	,
Mexiko	Mexiko	k1gNnSc1	Mexiko
Saint-Étienne	Saint-Étienn	k1gInSc5	Saint-Étienn
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
Stavropol	Stavropol	k1gInSc1	Stavropol
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
Š	Š	kA	Š
<g/>
'	'	kIx"	'
<g/>
-ťia-čuang	-ťia-čuang	k1gMnSc1	-ťia-čuang
<g/>
,	,	kIx,	,
Čínská	čínský	k2eAgFnSc1d1	čínská
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Des	des	k1gNnSc2	des
Moines	Moinesa	k1gFnPc2	Moinesa
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
