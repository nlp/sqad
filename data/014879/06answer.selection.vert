<s>
Říše	říše	k1gFnSc1
vznikla	vzniknout	k5eAaPmAgFnS
spojením	spojení	k1gNnSc7
slovanských	slovanský	k2eAgInPc2d1
kmenů	kmen	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
byly	být	k5eAaImAgInP
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
podřízeny	podřízen	k2eAgInPc1d1
<g/>
,	,	kIx,
ne	ne	k9
však	však	k9
poddány	poddán	k2eAgFnPc1d1
<g/>
,	,	kIx,
avarskému	avarský	k2eAgInSc3d1
kaganátu	kaganát	k1gInSc3
<g/>
,	,	kIx,
tj.	tj.	kA
kmenovému	kmenový	k2eAgInSc3d1
svazu	svaz	k1gInSc3
<g/>
,	,	kIx,
v	v	k7c6
Panonii	Panonie	k1gFnSc6
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
byl	být	k5eAaImAgInS
však	však	k9
na	na	k7c6
začátku	začátek	k1gInSc6
7	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
ve	v	k7c6
vnitropolitickém	vnitropolitický	k2eAgInSc6d1
rozvratu	rozvrat	k1gInSc6
<g/>
,	,	kIx,
v	v	k7c6
důsledku	důsledek	k1gInSc6
bojů	boj	k1gInPc2
mezi	mezi	k7c7
několika	několik	k4yIc7
nástupci	nástupce	k1gMnPc7
chána	chán	k1gMnSc2
Bajana	bajan	k1gMnSc2
<g/>
.	.	kIx.
</s>