<s>
Sámova	Sámův	k2eAgFnSc1d1
říše	říše	k1gFnSc1
</s>
<s>
Sámova	Sámův	k2eAgFnSc1d1
říše	říše	k1gFnSc1
</s>
<s>
←	←	k?
←	←	k?
</s>
<s>
624	#num#	k4
<g/>
/	/	kIx~
<g/>
626	#num#	k4
<g/>
–	–	k?
<g/>
659	#num#	k4
<g/>
/	/	kIx~
<g/>
661	#num#	k4
</s>
<s>
→	→	k?
→	→	k?
→	→	k?
→	→	k?
→	→	k?
</s>
<s>
geografie	geografie	k1gFnSc1
</s>
<s>
hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
neznámé	známý	k2eNgNnSc1d1
<g/>
,	,	kIx,
dle	dle	k7c2
některých	některý	k3yIgFnPc2
teorií	teorie	k1gFnPc2
slovenský	slovenský	k2eAgInSc4d1
Děvín	Děvín	k1gInSc4
</s>
<s>
obyvatelstvo	obyvatelstvo	k1gNnSc1
</s>
<s>
národnostní	národnostní	k2eAgNnSc1d1
složení	složení	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
Slované	Slovan	k1gMnPc1
</s>
<s>
jazyky	jazyk	k1gInPc1
<g/>
:	:	kIx,
</s>
<s>
praslovanština	praslovanština	k1gFnSc1
</s>
<s>
náboženství	náboženství	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
staroslovanské	staroslovanský	k2eAgInPc4d1
(	(	kIx(
<g/>
oficiální	oficiální	k2eAgInPc4d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
křesťanské	křesťanský	k2eAgFnPc1d1
(	(	kIx(
<g/>
okrajově	okrajově	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
státní	státní	k2eAgInSc1d1
útvar	útvar	k1gInSc1
</s>
<s>
státní	státní	k2eAgNnSc1d1
zřízení	zřízení	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
monarchie	monarchie	k1gFnSc1
a	a	k8xC
kmenová	kmenový	k2eAgFnSc1d1
konfederace	konfederace	k1gFnSc1
</s>
<s>
vznik	vznik	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
624	#num#	k4
<g/>
/	/	kIx~
<g/>
626	#num#	k4
</s>
<s>
zánik	zánik	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
658	#num#	k4
–	–	k?
smrt	smrt	k1gFnSc4
vládce	vládce	k1gMnSc4
Sáma	Sámo	k1gMnSc4
<g/>
659	#num#	k4
až	až	k9
661	#num#	k4
</s>
<s>
státní	státní	k2eAgInPc4d1
útvary	útvar	k1gInPc4
a	a	k8xC
území	území	k1gNnSc4
</s>
<s>
předcházející	předcházející	k2eAgFnSc1d1
<g/>
:	:	kIx,
</s>
<s>
Franská	franský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
</s>
<s>
Avarský	avarský	k2eAgInSc1d1
kaganát	kaganát	k1gInSc1
</s>
<s>
následující	následující	k2eAgInPc4d1
<g/>
:	:	kIx,
</s>
<s>
Franská	franský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
</s>
<s>
Korutanské	korutanský	k2eAgNnSc1d1
knížectví	knížectví	k1gNnSc1
</s>
<s>
Avarský	avarský	k2eAgInSc1d1
kaganát	kaganát	k1gInSc1
</s>
<s>
Moravské	moravský	k2eAgNnSc1d1
knížectví	knížectví	k1gNnSc1
</s>
<s>
Nitranské	nitranský	k2eAgNnSc1d1
knížectví	knížectví	k1gNnSc1
</s>
<s>
Sámova	Sámův	k2eAgFnSc1d1
říše	říše	k1gFnSc1
byl	být	k5eAaImAgInS
kmenový	kmenový	k2eAgInSc1d1
svaz	svaz	k1gInSc1
Slovanů	Slovan	k1gMnPc2
existující	existující	k2eAgMnSc1d1
ve	v	k7c6
střední	střední	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
v	v	k7c6
letech	let	k1gInPc6
624	#num#	k4
<g/>
–	–	k?
<g/>
659	#num#	k4
(	(	kIx(
<g/>
podle	podle	k7c2
Fredegarovy	Fredegarův	k2eAgFnSc2d1
kroniky	kronika	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Pojil	pojit	k5eAaImAgMnS
se	se	k3xPyFc4
s	s	k7c7
osobou	osoba	k1gFnSc7
franského	franský	k2eAgMnSc4d1
kupce	kupec	k1gMnSc4
Sáma	Sámo	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
si	se	k3xPyFc3
získal	získat	k5eAaPmAgMnS
velkou	velký	k2eAgFnSc4d1
prestiž	prestiž	k1gFnSc4
jako	jako	k8xS,k8xC
velitel	velitel	k1gMnSc1
během	během	k7c2
bojů	boj	k1gInPc2
Slovanů	Slovan	k1gInPc2
s	s	k7c7
Avary	Avar	k1gMnPc7
<g/>
,	,	kIx,
ovládajícími	ovládající	k2eAgMnPc7d1
dlouhá	dlouhý	k2eAgNnPc4d1
léta	léto	k1gNnPc4
střední	střední	k2eAgFnSc2d1
a	a	k8xC
jihovýchodní	jihovýchodní	k2eAgFnSc4d1
Evropu	Evropa	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Charakteristika	charakteristika	k1gFnSc1
</s>
<s>
Říše	říše	k1gFnSc1
vznikla	vzniknout	k5eAaPmAgFnS
spojením	spojení	k1gNnSc7
slovanských	slovanský	k2eAgInPc2d1
kmenů	kmen	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
byly	být	k5eAaImAgInP
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
podřízeny	podřízen	k2eAgInPc1d1
<g/>
,	,	kIx,
ne	ne	k9
však	však	k9
poddány	poddán	k2eAgFnPc1d1
<g/>
,	,	kIx,
avarskému	avarský	k2eAgInSc3d1
kaganátu	kaganát	k1gInSc3
<g/>
,	,	kIx,
tj.	tj.	kA
kmenovému	kmenový	k2eAgInSc3d1
svazu	svaz	k1gInSc3
<g/>
,	,	kIx,
v	v	k7c6
Panonii	Panonie	k1gFnSc6
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
byl	být	k5eAaImAgInS
však	však	k9
na	na	k7c6
začátku	začátek	k1gInSc6
7	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
ve	v	k7c6
vnitropolitickém	vnitropolitický	k2eAgInSc6d1
rozvratu	rozvrat	k1gInSc6
<g/>
,	,	kIx,
v	v	k7c6
důsledku	důsledek	k1gInSc6
bojů	boj	k1gInPc2
mezi	mezi	k7c7
několika	několik	k4yIc7
nástupci	nástupce	k1gMnPc7
chána	chán	k1gMnSc2
Bajana	bajan	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Franský	franský	k2eAgMnSc1d1
kupec	kupec	k1gMnSc1
Sámo	Sámo	k1gMnSc1
toho	ten	k3xDgMnSc4
využil	využít	k5eAaPmAgMnS
a	a	k8xC
spojil	spojit	k5eAaPmAgMnS
slovanské	slovanský	k2eAgInPc4d1
kmeny	kmen	k1gInPc4
proti	proti	k7c3
nim	on	k3xPp3gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
byl	být	k5eAaImAgMnS
zvolen	zvolit	k5eAaPmNgMnS
jejich	jejich	k3xOp3gMnSc7
vůdcem	vůdce	k1gMnSc7
(	(	kIx(
<g/>
Historia	Historium	k1gNnSc2
Francorum	Francorum	k1gNnSc4
<g/>
:	:	kIx,
latinsky	latinsky	k6eAd1
...	...	k?
<g/>
eum	eum	k?
super	super	k1gInSc2
se	se	k3xPyFc4
eligunt	eligunt	k1gInSc1
regem	regem	k6eAd1
<g/>
...	...	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nově	nově	k6eAd1
vzniklá	vzniklý	k2eAgFnSc1d1
Sámova	Sámův	k2eAgFnSc1d1
říše	říše	k1gFnSc1
měla	mít	k5eAaImAgFnS
charakter	charakter	k1gInSc4
blízký	blízký	k2eAgInSc4d1
avarskému	avarský	k2eAgInSc3d1
(	(	kIx(
<g/>
turko-tatarskému	turko-tatarský	k2eAgMnSc3d1
<g/>
)	)	kIx)
kaganátu	kaganát	k1gInSc3
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
byl	být	k5eAaImAgInS
posílen	posílit	k5eAaPmNgInS
prozíravou	prozíravý	k2eAgFnSc7d1
sňatkovou	sňatkový	k2eAgFnSc7d1
politikou	politika	k1gFnSc7
(	(	kIx(
<g/>
Fredegar	Fredegar	k1gInSc1
píše	psát	k5eAaImIp3nS
o	o	k7c6
12	#num#	k4
manželkách	manželka	k1gFnPc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Centrum	centrum	k1gNnSc1
Sámovy	Sámův	k2eAgFnSc2d1
říše	říš	k1gFnSc2
se	se	k3xPyFc4
nacházelo	nacházet	k5eAaImAgNnS
pravděpodobně	pravděpodobně	k6eAd1
na	na	k7c4
rozhraní	rozhraní	k1gNnSc4
Moravy	Morava	k1gFnSc2
a	a	k8xC
Dolních	dolní	k2eAgInPc2d1
Rakous	Rakousy	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Říše	říše	k1gFnSc1
dosáhla	dosáhnout	k5eAaPmAgFnS
nemalého	malý	k2eNgInSc2d1
významu	význam	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Již	již	k6eAd1
v	v	k7c6
době	doba	k1gFnSc6
před	před	k7c7
Sámovou	Sámův	k2eAgFnSc7d1
říší	říš	k1gFnSc7
byly	být	k5eAaImAgFnP
slovanské	slovanský	k2eAgInPc4d1
kmeny	kmen	k1gInPc4
velmi	velmi	k6eAd1
schopnou	schopný	k2eAgFnSc7d1
pěší	pěší	k2eAgFnSc7d1
armádou	armáda	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svou	svůj	k3xOyFgFnSc4
bojeschopnost	bojeschopnost	k1gFnSc4
(	(	kIx(
<g/>
hlavně	hlavně	k9
však	však	k9
díky	díky	k7c3
dobrému	dobrý	k2eAgNnSc3d1
vedení	vedení	k1gNnSc3
<g/>
)	)	kIx)
dokázaly	dokázat	k5eAaPmAgFnP
také	také	k9
<g/>
,	,	kIx,
asi	asi	k9
v	v	k7c6
jediném	jediný	k2eAgNnSc6d1
písemně	písemně	k6eAd1
dochovaném	dochovaný	k2eAgNnSc6d1
utkání	utkání	k1gNnSc6
<g/>
,	,	kIx,
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Wogastisburgu	Wogastisburg	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
631	#num#	k4
<g/>
,	,	kIx,
kde	kde	k6eAd1
porazily	porazit	k5eAaPmAgFnP
franská	franský	k2eAgNnPc4d1
vojska	vojsko	k1gNnPc4
krále	král	k1gMnSc2
Dagoberta	Dagobert	k1gMnSc2
I.	I.	kA
V	v	k7c6
následujících	následující	k2eAgNnPc6d1
pěti	pět	k4xCc6
letech	léto	k1gNnPc6
byly	být	k5eAaImAgFnP
podnikány	podnikán	k2eAgInPc4d1
nájezdy	nájezd	k1gInPc4
na	na	k7c4
franské	franský	k2eAgNnSc4d1
území	území	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
</s>
<s>
Poloha	poloha	k1gFnSc1
nebo	nebo	k8xC
jméno	jméno	k1gNnSc1
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
je	být	k5eAaImIp3nS
neznámá	známý	k2eNgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zachoval	Zachoval	k1gMnSc1
se	se	k3xPyFc4
jen	jen	k6eAd1
název	název	k1gInSc1
jakéhosi	jakýsi	k3yIgInSc2
Wogastisburgu	Wogastisburg	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
připomínán	připomínat	k5eAaImNgInS
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
podrobnou	podrobný	k2eAgFnSc7d1
zmínkou	zmínka	k1gFnSc7
ve	v	k7c6
Fredagarovi	Fredagar	k1gMnSc6
týkající	týkající	k2eAgInSc4d1
se	se	k3xPyFc4
následného	následný	k2eAgInSc2d1
konfliktu	konflikt	k1gInSc2
s	s	k7c7
franským	franský	k2eAgMnSc7d1
králem	král	k1gMnSc7
Dagobertem	Dagobert	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Konec	konec	k1gInSc1
a	a	k8xC
pokračování	pokračování	k1gNnSc1
říše	říš	k1gFnSc2
</s>
<s>
Má	mít	k5eAaImIp3nS
se	se	k3xPyFc4
za	za	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
po	po	k7c6
Sámově	Sámův	k2eAgFnSc6d1
smrti	smrt	k1gFnSc6
(	(	kIx(
<g/>
658	#num#	k4
nebo	nebo	k8xC
661	#num#	k4
<g/>
)	)	kIx)
se	se	k3xPyFc4
jeho	jeho	k3xOp3gFnSc1
říše	říše	k1gFnSc1
rozpadla	rozpadnout	k5eAaPmAgFnS
<g/>
,	,	kIx,
čemuž	což	k3yQnSc3,k3yRnSc3
pravděpodobně	pravděpodobně	k6eAd1
velmi	velmi	k6eAd1
napomohl	napomoct	k5eAaPmAgMnS
fakt	fakt	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
Sámo	Sámo	k1gMnSc1
měl	mít	k5eAaImAgMnS
početné	početný	k2eAgNnSc4d1
potomstvo	potomstvo	k1gNnSc4
(	(	kIx(
<g/>
snad	snad	k9
22	#num#	k4
synů	syn	k1gMnPc2
a	a	k8xC
15	#num#	k4
dcer	dcera	k1gFnPc2
podle	podle	k7c2
Fredegara	Fredegar	k1gMnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odstředivé	odstředivý	k2eAgFnPc1d1
kmenové	kmenový	k2eAgFnPc1d1
tendence	tendence	k1gFnPc1
byly	být	k5eAaImAgFnP
posíleny	posílit	k5eAaPmNgFnP
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
téměř	téměř	k6eAd1
každý	každý	k3xTgInSc1
mocnější	mocný	k2eAgInSc1d2
kmen	kmen	k1gInSc1
svazu	svaz	k1gInSc2
měl	mít	k5eAaImAgInS
mezi	mezi	k7c7
Sámovými	Sámův	k2eAgFnPc7d1
manželkami	manželka	k1gFnPc7
svou	svůj	k3xOyFgFnSc4
zástupkyni	zástupkyně	k1gFnSc4
a	a	k8xC
jejího	její	k3xOp3gMnSc4
syna	syn	k1gMnSc4
–	–	k?
pretendenta	pretendent	k1gMnSc4
trůnu	trůn	k1gInSc2
<g/>
,	,	kIx,
se	s	k7c7
kterým	který	k3yRgMnSc7,k3yIgMnSc7,k3yQgMnSc7
se	se	k3xPyFc4
mohl	moct	k5eAaImAgMnS
identifikovat	identifikovat	k5eAaBmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yRnSc1,k3yQnSc1
bylo	být	k5eAaImAgNnS
za	za	k7c2
Sámova	Sámův	k2eAgInSc2d1
života	život	k1gInSc2
při	při	k7c6
udržování	udržování	k1gNnSc6
říše	říš	k1gFnSc2
výhodou	výhoda	k1gFnSc7
<g/>
,	,	kIx,
se	se	k3xPyFc4
po	po	k7c6
jeho	jeho	k3xOp3gFnSc6
smrti	smrt	k1gFnSc6
stalo	stát	k5eAaPmAgNnS
naopak	naopak	k6eAd1
překážkou	překážka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
O	o	k7c6
období	období	k1gNnSc6
po	po	k7c6
rozpadu	rozpad	k1gInSc6
Sámovy	Sámův	k2eAgFnSc2d1
říše	říš	k1gFnSc2
nejsou	být	k5eNaImIp3nP
žádné	žádný	k3yNgInPc4
písemné	písemný	k2eAgInPc4d1
prameny	pramen	k1gInPc4
<g/>
,	,	kIx,
přesto	přesto	k8xC
podle	podle	k7c2
archeologických	archeologický	k2eAgInPc2d1
nálezů	nález	k1gInPc2
lze	lze	k6eAd1
usuzovat	usuzovat	k5eAaImF
<g/>
,	,	kIx,
že	že	k8xS
říše	říše	k1gFnSc1
fungovala	fungovat	k5eAaImAgFnS
nadále	nadále	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
samozřejmě	samozřejmě	k6eAd1
ne	ne	k9
v	v	k7c6
takovém	takový	k3xDgNnSc6
měřítku	měřítko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
pravděpodobně	pravděpodobně	k6eAd1
roztříštěna	roztříštit	k5eAaPmNgFnS
mezi	mezi	k7c4
Sámovy	Sámův	k2eAgMnPc4d1
následovníky	následovník	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
vládli	vládnout	k5eAaImAgMnP
na	na	k7c6
svých	svůj	k3xOyFgNnPc6
územích	území	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
určitých	určitý	k2eAgInPc6d1
pozdějších	pozdní	k2eAgInPc6d2
pramenech	pramen	k1gInPc6
se	se	k3xPyFc4
postupně	postupně	k6eAd1
objevují	objevovat	k5eAaImIp3nP
jména	jméno	k1gNnPc4
Moravod	Moravoda	k1gFnPc2
<g/>
,	,	kIx,
Vladuc	Vladuc	k1gFnSc1
<g/>
,	,	kIx,
Suanthos	Suanthos	k1gMnSc1
<g/>
,	,	kIx,
Samoslav	Samoslav	k1gMnSc1
<g/>
,	,	kIx,
Hormidor	Hormidor	k1gMnSc1
a	a	k8xC
Mojmír	Mojmír	k1gMnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
jejich	jejich	k3xOp3gFnSc1
skutečnost	skutečnost	k1gFnSc1
je	být	k5eAaImIp3nS
velice	velice	k6eAd1
neurčitá	určitý	k2eNgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velice	velice	k6eAd1
pravděpodobně	pravděpodobně	k6eAd1
jde	jít	k5eAaImIp3nS
o	o	k7c4
pouhou	pouhý	k2eAgFnSc4d1
literární	literární	k2eAgFnSc4d1
fikci	fikce	k1gFnSc4
kronikářů	kronikář	k1gMnPc2
Tomáše	Tomáš	k1gMnSc4
Pěšiny	pěšina	k1gFnSc2
a	a	k8xC
Gelasia	Gelasius	k1gMnSc2
Dobnera	Dobner	k1gMnSc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
se	se	k3xPyFc4
pokusili	pokusit	k5eAaPmAgMnP
po	po	k7c6
vzoru	vzor	k1gInSc6
českých	český	k2eAgInPc2d1
mýtů	mýtus	k1gInPc2
vytvořit	vytvořit	k5eAaPmF
přímou	přímý	k2eAgFnSc4d1
návaznost	návaznost	k1gFnSc4
Sámova	Sámův	k2eAgInSc2d1
rodu	rod	k1gInSc2
na	na	k7c4
mojmírovská	mojmírovský	k2eAgNnPc4d1
knížata	kníže	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kažopádně	Kažopádně	k1gFnSc1
hradiště	hradiště	k1gNnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
byla	být	k5eAaImAgFnS
na	na	k7c6
vzestupu	vzestup	k1gInSc6
za	za	k7c2
Sámovy	Sámův	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
<g/>
,	,	kIx,
nezanikla	zaniknout	k5eNaPmAgFnS
<g/>
,	,	kIx,
ba	ba	k9
dokonce	dokonce	k9
se	se	k3xPyFc4
dále	daleko	k6eAd2
rozvíjela	rozvíjet	k5eAaImAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
tohoto	tento	k3xDgInSc2
lze	lze	k6eAd1
usuzovat	usuzovat	k5eAaImF
<g/>
,	,	kIx,
že	že	k8xS
byla	být	k5eAaImAgFnS
jádrem	jádro	k1gNnSc7
pro	pro	k7c4
založení	založení	k1gNnSc4
budoucí	budoucí	k2eAgFnSc2d1
Velkomoravské	velkomoravský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
moci	moc	k1gFnSc3
chopili	chopit	k5eAaPmAgMnP
Mojmírovci	Mojmírovec	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
Prameny	pramen	k1gInPc1
</s>
<s>
Všechny	všechen	k3xTgFnPc1
teorie	teorie	k1gFnPc1
o	o	k7c6
Sámově	Sámův	k2eAgFnSc6d1
říši	říš	k1gFnSc6
vycházejí	vycházet	k5eAaImIp3nP
z	z	k7c2
jediné	jediný	k2eAgFnSc2d1
zprávy	zpráva	k1gFnSc2
v	v	k7c6
tzv.	tzv.	kA
Fredegarově	Fredegarův	k2eAgFnSc6d1
kronice	kronika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Nejstarší	starý	k2eAgFnSc1d3
(	(	kIx(
<g/>
tendenční	tendenční	k2eAgFnSc1d1
a	a	k8xC
v	v	k7c6
tomto	tento	k3xDgInSc6
bodě	bod	k1gInSc6
nespolehlivé	spolehlivý	k2eNgFnPc1d1
<g/>
)	)	kIx)
zprávy	zpráva	k1gFnPc1
pocházejí	pocházet	k5eAaImIp3nP
z	z	k7c2
textu	text	k1gInSc2
spisku	spisek	k1gInSc2
O	o	k7c6
obrácení	obrácení	k1gNnSc6
Bavorů	Bavor	k1gMnPc2
a	a	k8xC
Korutanců	Korutanec	k1gMnPc2
na	na	k7c4
víru	víra	k1gFnSc4
(	(	kIx(
<g/>
Libellus	Libellus	k1gInSc1
de	de	k?
conversione	conversion	k1gInSc5
Bagoariorum	Bagoariorum	k1gNnSc4
et	et	k?
Carantonorum	Carantonorum	k1gInSc1
<g/>
,	,	kIx,
rok	rok	k1gInSc1
870	#num#	k4
<g/>
)	)	kIx)
ze	z	k7c2
Solnohradu	Solnohrad	k1gInSc2
(	(	kIx(
<g/>
Salzburg	Salzburg	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
je	být	k5eAaImIp3nS
ve	v	k7c6
více	hodně	k6eAd2
částech	část	k1gFnPc6
přímo	přímo	k6eAd1
odvozen	odvodit	k5eAaPmNgInS
z	z	k7c2
Fredegara	Fredegar	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třetí	třetí	k4xOgInSc1
text	text	k1gInSc1
–	–	k?
také	také	k6eAd1
odvozený	odvozený	k2eAgMnSc1d1
z	z	k7c2
Fredegara	Fredegar	k1gMnSc2
–	–	k?
jsou	být	k5eAaImIp3nP
Gesta	gesto	k1gNnPc1
Dagoberti	Dagobert	k1gMnPc1
I.	I.	kA
regnis	regnis	k1gFnSc1
Francorum	Francorum	k1gInSc1
z	z	k7c2
první	první	k4xOgFnSc2
třetiny	třetina	k1gFnSc2
9	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
z	z	k7c2
opatství	opatství	k1gNnSc2
Saint-Denis	Saint-Denis	k1gFnSc2
u	u	k7c2
Paříže	Paříž	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Moravia	Moravia	k1gFnSc1
Magna	Magna	k1gFnSc1
<g/>
:	:	kIx,
Kronika	kronika	k1gFnSc1
tak	tak	k6eAd1
řečeného	řečený	k2eAgMnSc2d1
Fredegara	Fredegar	k1gMnSc2
(	(	kIx(
<g/>
výňatek	výňatek	k1gInSc1
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
The	The	k?
fourth	fourth	k1gMnSc1
book	book	k1gMnSc1
of	of	k?
the	the	k?
Chronicle	Chronicle	k1gFnSc2
of	of	k?
Fredegar	Fredegar	k1gMnSc1
with	with	k1gMnSc1
its	its	k?
continuations	continuations	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Překlad	překlad	k1gInSc1
J.	J.	kA
M.	M.	kA
Wallace-Hadrill	Wallace-Hadrill	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
London	London	k1gMnSc1
<g/>
:	:	kIx,
T.	T.	kA
Nelson	Nelson	k1gMnSc1
<g/>
,	,	kIx,
1960	#num#	k4
<g/>
.	.	kIx.
lxvii	lxvie	k1gFnSc6
<g/>
,	,	kIx,
137	#num#	k4
s.	s.	k?
</s>
<s>
BERANOVÁ	Beranová	k1gFnSc1
<g/>
,	,	kIx,
Magdalena	Magdalena	k1gFnSc1
<g/>
;	;	kIx,
LUTOVSKÝ	LUTOVSKÝ	kA
<g/>
,	,	kIx,
Michal	Michal	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slované	Slovan	k1gMnPc1
v	v	k7c6
Čechách	Čechy	k1gFnPc6
:	:	kIx,
archeologie	archeologie	k1gFnSc1
6	#num#	k4
<g/>
.	.	kIx.
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Libri	Libri	k1gNnSc1
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
475	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7277	#num#	k4
<g/>
-	-	kIx~
<g/>
413	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
BLÁHOVÁ	Bláhová	k1gFnSc1
<g/>
,	,	kIx,
Marie	Marie	k1gFnSc1
<g/>
;	;	kIx,
FROLÍK	FROLÍK	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
;	;	kIx,
PROFANTOVÁ	PROFANTOVÁ	kA
<g/>
,	,	kIx,
Naďa	Naďa	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velké	velký	k2eAgFnPc1d1
dějiny	dějiny	k1gFnPc1
zemí	zem	k1gFnPc2
Koruny	koruna	k1gFnSc2
české	český	k2eAgFnSc2d1
I.	I.	kA
Do	do	k7c2
roku	rok	k1gInSc2
1197	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
;	;	kIx,
Litomyšl	Litomyšl	k1gFnSc1
<g/>
:	:	kIx,
Paseka	Paseka	k1gMnSc1
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
800	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7185	#num#	k4
<g/>
-	-	kIx~
<g/>
265	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
COLLINS	COLLINS	kA
<g/>
,	,	kIx,
Roger	Roger	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Die	Die	k1gFnSc1
Fredegar-Chroniken	Fredegar-Chronikna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hannover	Hannover	k1gInSc1
<g/>
:	:	kIx,
Hahnsche	Hahnsche	k1gInSc1
Buchhandlung	Buchhandlunga	k1gFnPc2
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
152	#num#	k4
s.	s.	k?
(	(	kIx(
<g/>
Monumenta	Monument	k1gMnSc2
Germaniae	Germania	k1gMnSc2
historica	historicus	k1gMnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
7752	#num#	k4
<g/>
-	-	kIx~
<g/>
5704	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
CHARVÁT	Charvát	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zrod	zrod	k1gInSc1
českého	český	k2eAgInSc2d1
státu	stát	k1gInSc2
<g/>
:	:	kIx,
568	#num#	k4
<g/>
-	-	kIx~
<g/>
1055	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Vyšehrad	Vyšehrad	k1gInSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
263	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7021	#num#	k4
<g/>
-	-	kIx~
<g/>
845	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
LUTOVSKÝ	LUTOVSKÝ	kA
<g/>
,	,	kIx,
Michal	Michal	k1gMnSc1
<g/>
;	;	kIx,
PROFANTOVÁ	PROFANTOVÁ	kA
<g/>
,	,	kIx,
Naďa	Naďa	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sámova	Sámův	k2eAgFnSc1d1
říše	říše	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
.	.	kIx.
89	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
200	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
420	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
LUTOVSKÝ	LUTOVSKÝ	kA
<g/>
,	,	kIx,
Michal	Michal	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Encyklopedie	encyklopedie	k1gFnSc1
slovanské	slovanský	k2eAgFnSc2d1
archeologie	archeologie	k1gFnSc2
v	v	k7c6
Čechách	Čechy	k1gFnPc6
<g/>
,	,	kIx,
na	na	k7c6
Moravě	Morava	k1gFnSc6
a	a	k8xC
ve	v	k7c6
Slezsku	Slezsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Libri	Libri	k1gNnSc1
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
431	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7277	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
54	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Heslo	heslo	k1gNnSc1
Sámo	Sámo	k1gMnSc1
<g/>
,	,	kIx,
Sámova	Sámův	k2eAgFnSc1d1
říše	říše	k1gFnSc1
<g/>
,	,	kIx,
Wogastisburg	Wogastisburg	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
MĚŘÍNSKÝ	MĚŘÍNSKÝ	kA
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
České	český	k2eAgFnPc4d1
země	zem	k1gFnPc4
od	od	k7c2
příchodu	příchod	k1gInSc2
Slovanů	Slovan	k1gInPc2
po	po	k7c4
Velkou	velký	k2eAgFnSc4d1
Moravu	Morava	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Libri	Libri	k1gNnSc1
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
564	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7277	#num#	k4
<g/>
-	-	kIx~
<g/>
407	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Sámova	Sámův	k2eAgFnSc1d1
říše	říše	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Bohové	bůh	k1gMnPc1
a	a	k8xC
mýty	mýtus	k1gInPc4
staré	starý	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
-	-	kIx~
Sámo	Sámo	k1gMnSc1
a	a	k8xC
Wogastisburg	Wogastisburg	k1gMnSc1
na	na	k7c4
YouTube	YouTub	k1gInSc5
</s>
<s>
KUČERA	Kučera	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
,	,	kIx,
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praotec	praotec	k1gMnSc1
Sámo	Sámo	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
<g/>
,	,	kIx,
2012-07-10	2012-07-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
ŠIMÍK	ŠIMÍK	kA
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kronika	kronika	k1gFnSc1
tak	tak	k6eAd1
řečeného	řečený	k2eAgMnSc2d1
Fredegara	Fredegar	k1gMnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moravia	Moravia	k1gFnSc1
Magna	Magna	k1gFnSc1
<g/>
,	,	kIx,
2007	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Historie	historie	k1gFnSc1
|	|	kIx~
Středověk	středověk	k1gInSc1
</s>
