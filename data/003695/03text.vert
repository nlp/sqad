<s>
Macbeth	Macbeth	k1gInSc1	Macbeth
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
The	The	k1gMnSc2	The
Tragedy	Trageda	k1gMnSc2	Trageda
of	of	k?	of
Macbeth	Macbeth	k1gInSc1	Macbeth
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
tragédie	tragédie	k1gFnSc1	tragédie
Williama	William	k1gMnSc2	William
Shakespeara	Shakespeare	k1gMnSc2	Shakespeare
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1606	[number]	k4	1606
<g/>
.	.	kIx.	.
</s>
<s>
Hra	hra	k1gFnSc1	hra
je	být	k5eAaImIp3nS	být
inspirována	inspirovat	k5eAaBmNgFnS	inspirovat
historickou	historický	k2eAgFnSc7d1	historická
postavou	postava	k1gFnSc7	postava
skotského	skotský	k2eAgMnSc4d1	skotský
krále	král	k1gMnSc4	král
Macbetha	Macbeth	k1gMnSc2	Macbeth
I.	I.	kA	I.
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mezi	mezi	k7c7	mezi
touto	tento	k3xDgFnSc7	tento
historickou	historický	k2eAgFnSc7d1	historická
postavou	postava	k1gFnSc7	postava
a	a	k8xC	a
jejím	její	k3xOp3gNnSc7	její
Shakespearovým	Shakespearův	k2eAgNnSc7d1	Shakespearovo
pojetím	pojetí	k1gNnSc7	pojetí
je	být	k5eAaImIp3nS	být
značný	značný	k2eAgInSc1d1	značný
rozdíl	rozdíl	k1gInSc1	rozdíl
<g/>
.	.	kIx.	.
</s>
<s>
Macbeth	Macbeth	k1gMnSc1	Macbeth
se	se	k3xPyFc4	se
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
z	z	k7c2	z
nejoblíbenějších	oblíbený	k2eAgMnPc2d3	nejoblíbenější
skotských	skotský	k2eAgMnPc2d1	skotský
panovníků	panovník	k1gMnPc2	panovník
proměnil	proměnit	k5eAaPmAgInS	proměnit
v	v	k7c4	v
tyrana	tyran	k1gMnSc4	tyran
a	a	k8xC	a
uzurpátora	uzurpátor	k1gMnSc4	uzurpátor
až	až	k9	až
nějakých	nějaký	k3yIgInPc2	nějaký
300	[number]	k4	300
let	léto	k1gNnPc2	léto
po	po	k7c6	po
své	svůj	k3xOyFgFnSc6	svůj
smrti	smrt	k1gFnSc6	smrt
a	a	k8xC	a
od	od	k7c2	od
konce	konec	k1gInSc2	konec
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
takto	takto	k6eAd1	takto
mylně	mylně	k6eAd1	mylně
vnímán	vnímat	k5eAaImNgInS	vnímat
všemi	všecek	k3xTgMnPc7	všecek
kronikáři	kronikář	k1gMnPc7	kronikář
a	a	k8xC	a
historiky	historik	k1gMnPc7	historik
<g/>
.	.	kIx.	.
</s>
<s>
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
čerpal	čerpat	k5eAaImAgMnS	čerpat
z	z	k7c2	z
dostupných	dostupný	k2eAgInPc2d1	dostupný
pramenů	pramen	k1gInPc2	pramen
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
a	a	k8xC	a
nelze	lze	k6eNd1	lze
mu	on	k3xPp3gMnSc3	on
vyčítat	vyčítat	k5eAaImF	vyčítat
historickou	historický	k2eAgFnSc4d1	historická
nepřesnost	nepřesnost	k1gFnSc4	nepřesnost
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
divadelní	divadelní	k2eAgFnSc6d1	divadelní
hře	hra	k1gFnSc6	hra
<g/>
.	.	kIx.	.
</s>
<s>
Hodnocení	hodnocení	k1gNnSc1	hodnocení
doby	doba	k1gFnSc2	doba
a	a	k8xC	a
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
zachycených	zachycený	k2eAgInPc2d1	zachycený
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
hodnocením	hodnocení	k1gNnSc7	hodnocení
dávnověkého	dávnověký	k2eAgNnSc2d1	dávnověké
Skotska	Skotsko	k1gNnSc2	Skotsko
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
alžbětinské	alžbětinský	k2eAgFnSc2d1	Alžbětinská
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
jako	jako	k9	jako
každá	každý	k3xTgFnSc1	každý
doba	doba	k1gFnSc1	doba
své	svůj	k3xOyFgInPc4	svůj
předsudky	předsudek	k1gInPc4	předsudek
<g/>
.	.	kIx.	.
</s>
<s>
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
hru	hra	k1gFnSc4	hra
napsal	napsat	k5eAaPmAgMnS	napsat
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
nového	nový	k2eAgMnSc2d1	nový
krále	král	k1gMnSc2	král
Jakuba	Jakub	k1gMnSc2	Jakub
I.	I.	kA	I.
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gFnSc6	jehož
osobě	osoba	k1gFnSc6	osoba
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
sjednocení	sjednocení	k1gNnSc3	sjednocení
skotského	skotský	k2eAgInSc2d1	skotský
a	a	k8xC	a
anglického	anglický	k2eAgInSc2d1	anglický
trůnu	trůn	k1gInSc2	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Hra	hra	k1gFnSc1	hra
tedy	tedy	k9	tedy
měla	mít	k5eAaImAgFnS	mít
hlavně	hlavně	k6eAd1	hlavně
odkazovat	odkazovat	k5eAaImF	odkazovat
na	na	k7c4	na
králův	králův	k2eAgInSc4d1	králův
původ	původ	k1gInSc4	původ
a	a	k8xC	a
na	na	k7c4	na
pozitivní	pozitivní	k2eAgFnPc4d1	pozitivní
stránky	stránka	k1gFnPc4	stránka
spojení	spojení	k1gNnSc1	spojení
obou	dva	k4xCgFnPc2	dva
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Duncan	Duncan	k1gInSc1	Duncan
–	–	k?	–
skotský	skotský	k1gInSc1	skotský
král	král	k1gMnSc1	král
Malcolm	Malcolm	k1gMnSc1	Malcolm
–	–	k?	–
jeho	jeho	k3xOp3gMnSc1	jeho
nejstarší	starý	k2eAgMnSc1d3	nejstarší
syn	syn	k1gMnSc1	syn
Donalbain	Donalbain	k1gMnSc1	Donalbain
–	–	k?	–
jeho	jeho	k3xOp3gMnSc1	jeho
nejmladší	mladý	k2eAgMnSc1d3	nejmladší
syn	syn	k1gMnSc1	syn
Macbeth	Macbeth	k1gMnSc1	Macbeth
–	–	k?	–
generál	generál	k1gMnSc1	generál
v	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
krále	král	k1gMnSc2	král
Duncana	Duncan	k1gMnSc2	Duncan
<g/>
;	;	kIx,	;
později	pozdě	k6eAd2	pozdě
skotský	skotský	k2eAgMnSc1d1	skotský
král	král	k1gMnSc1	král
Lady	Lada	k1gFnSc2	Lada
Macbeth	Macbeth	k1gMnSc1	Macbeth
–	–	k?	–
Macbethova	Macbethova	k1gFnSc1	Macbethova
manželka	manželka	k1gFnSc1	manželka
<g/>
,	,	kIx,	,
skotská	skotský	k2eAgFnSc1d1	skotská
královna	královna	k1gFnSc1	královna
Banquo	Banquo	k1gNnSc1	Banquo
–	–	k?	–
Macbethův	Macbethův	k2eAgMnSc1d1	Macbethův
přítel	přítel	k1gMnSc1	přítel
a	a	k8xC	a
generál	generál	k1gMnSc1	generál
v	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
krále	král	k1gMnSc2	král
Duncana	Duncan	k1gMnSc2	Duncan
Fleance	Fleanec	k1gMnSc2	Fleanec
–	–	k?	–
jeho	on	k3xPp3gInSc2	on
syn	syn	k1gMnSc1	syn
Macduffe	Macduff	k1gInSc5	Macduff
–	–	k?	–
vládce	vládce	k1gMnPc4	vládce
Fife	Fif	k1gFnSc2	Fif
Lady	Lada	k1gFnSc2	Lada
Macduff	Macduff	k1gMnSc1	Macduff
–	–	k?	–
jeho	jeho	k3xOp3gFnSc6	jeho
žena	žena	k1gFnSc1	žena
Macduffův	Macduffův	k2eAgMnSc1d1	Macduffův
syn	syn	k1gMnSc1	syn
Seyton	Seyton	k1gInSc1	Seyton
–	–	k?	–
Macbethův	Macbethův	k2eAgMnSc1d1	Macbethův
sluha	sluha	k1gMnSc1	sluha
Hekaté	Hekatá	k1gFnSc2	Hekatá
–	–	k?	–
čarodějnická	čarodějnický	k2eAgFnSc1d1	čarodějnická
bohyně	bohyně	k1gFnSc1	bohyně
Tři	tři	k4xCgMnPc4	tři
čarodějnice	čarodějnice	k1gFnPc4	čarodějnice
Tři	tři	k4xCgMnPc1	tři
vrazi	vrah	k1gMnPc1	vrah
Setkání	setkání	k1gNnSc2	setkání
s	s	k7c7	s
čarodějnicemi	čarodějnice	k1gFnPc7	čarodějnice
Hned	hned	k6eAd1	hned
v	v	k7c6	v
úvodu	úvod	k1gInSc6	úvod
je	být	k5eAaImIp3nS	být
navozena	navozen	k2eAgFnSc1d1	navozena
nezvyklá	zvyklý	k2eNgFnSc1d1	nezvyklá
atmosféra	atmosféra	k1gFnSc1	atmosféra
příběhu	příběh	k1gInSc2	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Ozývá	ozývat	k5eAaImIp3nS	ozývat
se	se	k3xPyFc4	se
hrom	hrom	k1gInSc1	hrom
<g/>
,	,	kIx,	,
blýská	blýskat	k5eAaImIp3nS	blýskat
se	se	k3xPyFc4	se
a	a	k8xC	a
objeví	objevit	k5eAaPmIp3nP	objevit
se	se	k3xPyFc4	se
tři	tři	k4xCgFnPc1	tři
čarodějnice	čarodějnice	k1gFnPc1	čarodějnice
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
oznámí	oznámit	k5eAaPmIp3nP	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
sejdou	sejít	k5eAaPmIp3nP	sejít
na	na	k7c6	na
vřesovišti	vřesoviště	k1gNnSc6	vřesoviště
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
pozdravily	pozdravit	k5eAaPmAgFnP	pozdravit
Macbetha	Macbetha	k1gFnSc1	Macbetha
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
stejně	stejně	k6eAd1	stejně
tajemně	tajemně	k6eAd1	tajemně
zmizí	zmizet	k5eAaPmIp3nP	zmizet
<g/>
.	.	kIx.	.
</s>
<s>
Bitva	bitva	k1gFnSc1	bitva
Mezitím	mezitím	k6eAd1	mezitím
se	se	k3xPyFc4	se
odehrála	odehrát	k5eAaPmAgFnS	odehrát
bitva	bitva	k1gFnSc1	bitva
mezi	mezi	k7c7	mezi
vojskem	vojsko	k1gNnSc7	vojsko
skotského	skotský	k2eAgMnSc2d1	skotský
krále	král	k1gMnSc2	král
Duncana	Duncan	k1gMnSc2	Duncan
a	a	k8xC	a
vojáky	voják	k1gMnPc7	voják
nespokojených	spokojený	k2eNgFnPc2d1	nespokojená
skotských	skotská	k1gFnPc2	skotská
pánů	pan	k1gMnPc2	pan
<g/>
.	.	kIx.	.
</s>
<s>
Povstalce	povstalec	k1gMnSc4	povstalec
podporuje	podporovat	k5eAaImIp3nS	podporovat
norský	norský	k2eAgMnSc1d1	norský
král	král	k1gMnSc1	král
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
muži	muž	k1gMnPc7	muž
<g/>
.	.	kIx.	.
</s>
<s>
Duncanův	Duncanův	k2eAgMnSc1d1	Duncanův
nejstatečnější	statečný	k2eAgMnSc1d3	nejstatečnější
generál	generál	k1gMnSc1	generál
<g/>
,	,	kIx,	,
Macbeth	Macbeth	k1gMnSc1	Macbeth
<g/>
,	,	kIx,	,
však	však	k9	však
nakonec	nakonec	k6eAd1	nakonec
nepřítele	nepřítel	k1gMnSc4	nepřítel
poráží	porážet	k5eAaImIp3nP	porážet
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
se	se	k3xPyFc4	se
dozví	dozvědět	k5eAaPmIp3nS	dozvědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
povstaleckých	povstalecký	k2eAgMnPc2d1	povstalecký
pánů	pan	k1gMnPc2	pan
<g/>
,	,	kIx,	,
thén	thén	k1gInSc4	thén
z	z	k7c2	z
Cawdoru	Cawdor	k1gInSc2	Cawdor
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
zajat	zajat	k2eAgInSc1d1	zajat
<g/>
.	.	kIx.	.
</s>
<s>
Přikazuje	přikazovat	k5eAaImIp3nS	přikazovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
popraven	popraven	k2eAgMnSc1d1	popraven
jako	jako	k8xS	jako
zrádce	zrádce	k1gMnSc1	zrádce
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
zároveň	zároveň	k6eAd1	zároveň
odmění	odměnit	k5eAaPmIp3nS	odměnit
Macbetha	Macbetha	k1gFnSc1	Macbetha
tím	ten	k3xDgInSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
uděluje	udělovat	k5eAaImIp3nS	udělovat
titul	titul	k1gInSc4	titul
théna	théna	k6eAd1	théna
z	z	k7c2	z
Cawdoru	Cawdor	k1gInSc2	Cawdor
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
posílá	posílat	k5eAaImIp3nS	posílat
své	svůj	k3xOyFgMnPc4	svůj
dva	dva	k4xCgMnPc4	dva
šlechtice	šlechtic	k1gMnPc4	šlechtic
<g/>
,	,	kIx,	,
Rosse	Ross	k1gMnPc4	Ross
a	a	k8xC	a
Anguse	Anguse	k1gFnPc4	Anguse
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Macbetha	Macbetha	k1gMnSc1	Macbetha
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
právě	právě	k6eAd1	právě
vrací	vracet	k5eAaImIp3nS	vracet
z	z	k7c2	z
boje	boj	k1gInSc2	boj
<g/>
,	,	kIx,	,
touto	tento	k3xDgFnSc7	tento
zprávou	zpráva	k1gFnSc7	zpráva
přivítali	přivítat	k5eAaPmAgMnP	přivítat
<g/>
.	.	kIx.	.
</s>
<s>
Macbeth	Macbeth	k1gMnSc1	Macbeth
<g/>
,	,	kIx,	,
Banquo	Banquo	k1gMnSc1	Banquo
a	a	k8xC	a
čarodějnice	čarodějnice	k1gFnSc1	čarodějnice
Macbeth	Macbetha	k1gFnPc2	Macbetha
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
přítelem	přítel	k1gMnSc7	přítel
Banquem	Banqu	k1gMnSc7	Banqu
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vřesovišti	vřesoviště	k1gNnSc6	vřesoviště
je	on	k3xPp3gInPc4	on
potkají	potkat	k5eAaPmIp3nP	potkat
čarodějnice	čarodějnice	k1gFnPc4	čarodějnice
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
si	se	k3xPyFc3	se
zatím	zatím	k6eAd1	zatím
povídají	povídat	k5eAaImIp3nP	povídat
o	o	k7c6	o
svých	svůj	k3xOyFgInPc6	svůj
špatných	špatný	k2eAgInPc6d1	špatný
skutcích	skutek	k1gInPc6	skutek
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
je	být	k5eAaImIp3nS	být
Macbeth	Macbeth	k1gInSc4	Macbeth
a	a	k8xC	a
Banquo	Banquo	k6eAd1	Banquo
náhle	náhle	k6eAd1	náhle
v	v	k7c6	v
mlze	mlha	k1gFnSc6	mlha
spatří	spatřit	k5eAaPmIp3nS	spatřit
<g/>
,	,	kIx,	,
osloví	oslovit	k5eAaPmIp3nS	oslovit
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
Macbetha	Macbetha	k1gFnSc1	Macbetha
jako	jako	k8xS	jako
théna	théna	k1gFnSc1	théna
z	z	k7c2	z
Cawdoru	Cawdor	k1gInSc2	Cawdor
a	a	k8xC	a
předpoví	předpovědět	k5eAaPmIp3nS	předpovědět
mu	on	k3xPp3gMnSc3	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
králem	král	k1gMnSc7	král
<g/>
.	.	kIx.	.
</s>
<s>
Banquovi	Banquův	k2eAgMnPc1d1	Banquův
řeknou	říct	k5eAaPmIp3nP	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Krále	Král	k1gMnSc2	Král
zplodíš	zplodit	k5eAaPmIp2nS	zplodit
<g/>
,	,	kIx,	,
třebas	třebas	k9	třebas
nebudeš	být	k5eNaImBp2nS	být
králem	král	k1gMnSc7	král
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Poté	poté	k6eAd1	poté
zmizí	zmizet	k5eAaPmIp3nS	zmizet
<g/>
.	.	kIx.	.
</s>
<s>
Macbeth	Macbeth	k1gInSc1	Macbeth
i	i	k8xC	i
Banquo	Banquo	k1gNnSc1	Banquo
jsou	být	k5eAaImIp3nP	být
zmatení	zmatení	k1gNnSc1	zmatení
<g/>
.	.	kIx.	.
</s>
<s>
Macbeth	Macbeth	k1gInSc1	Macbeth
sní	sníst	k5eAaPmIp3nS	sníst
o	o	k7c6	o
královském	královský	k2eAgInSc6d1	královský
trůnu	trůn	k1gInSc6	trůn
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
chvíli	chvíle	k1gFnSc6	chvíle
přicházejí	přicházet	k5eAaImIp3nP	přicházet
Ross	Ross	k1gInSc4	Ross
s	s	k7c7	s
Angusem	Angus	k1gInSc7	Angus
<g/>
.	.	kIx.	.
</s>
<s>
Oznamují	oznamovat	k5eAaImIp3nP	oznamovat
mu	on	k3xPp3gNnSc3	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
thénem	thén	k1gMnSc7	thén
z	z	k7c2	z
Cawdoru	Cawdor	k1gInSc2	Cawdor
-	-	kIx~	-
první	první	k4xOgFnSc1	první
věštba	věštba	k1gFnSc1	věštba
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
vyplnila	vyplnit	k5eAaPmAgFnS	vyplnit
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
Macbeth	Macbeth	k1gMnSc1	Macbeth
začíná	začínat	k5eAaImIp3nS	začínat
uvažovat	uvažovat	k5eAaImF	uvažovat
i	i	k9	i
o	o	k7c6	o
předpovědi	předpověď	k1gFnSc6	předpověď
druhé	druhý	k4xOgFnSc6	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
okamžik	okamžik	k1gInSc4	okamžik
si	se	k3xPyFc3	se
dokonce	dokonce	k9	dokonce
představí	představit	k5eAaPmIp3nS	představit
<g/>
,	,	kIx,	,
že	že	k8xS	že
zabije	zabít	k5eAaPmIp3nS	zabít
Duncana	Duncana	k1gFnSc1	Duncana
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hned	hned	k6eAd1	hned
tuto	tento	k3xDgFnSc4	tento
myšlenku	myšlenka	k1gFnSc4	myšlenka
zažene	zahnat	k5eAaPmIp3nS	zahnat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Duncanově	Duncanův	k2eAgInSc6d1	Duncanův
hradě	hrad	k1gInSc6	hrad
král	král	k1gMnSc1	král
Macbetha	Macbetha	k1gMnSc1	Macbetha
i	i	k8xC	i
Banqua	Banqua	k1gMnSc1	Banqua
vítá	vítat	k5eAaImIp3nS	vítat
a	a	k8xC	a
oznamuje	oznamovat	k5eAaImIp3nS	oznamovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jednoho	jeden	k4xCgMnSc4	jeden
ze	z	k7c2	z
svých	svůj	k3xOyFgMnPc2	svůj
synů	syn	k1gMnPc2	syn
<g/>
,	,	kIx,	,
Malcolma	Malcolma	k1gNnSc4	Malcolma
<g/>
,	,	kIx,	,
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
dědicem	dědic	k1gMnSc7	dědic
trůnu	trůn	k1gInSc2	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
prohlásí	prohlásit	k5eAaPmIp3nS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
hodlá	hodlat	k5eAaImIp3nS	hodlat
navštívit	navštívit	k5eAaPmF	navštívit
Macbethovo	Macbethův	k2eAgNnSc4d1	Macbethův
sídlo	sídlo	k1gNnSc4	sídlo
v	v	k7c6	v
Invernessu	Inverness	k1gInSc6	Inverness
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
velká	velký	k2eAgFnSc1d1	velká
pocta	pocta	k1gFnSc1	pocta
<g/>
.	.	kIx.	.
</s>
<s>
Macbeth	Macbeth	k1gInSc1	Macbeth
spěchá	spěchat	k5eAaImIp3nS	spěchat
pryč	pryč	k6eAd1	pryč
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
připravil	připravit	k5eAaPmAgInS	připravit
na	na	k7c4	na
králův	králův	k2eAgInSc4d1	králův
příjezd	příjezd	k1gInSc4	příjezd
<g/>
.	.	kIx.	.
</s>
<s>
Lady	lady	k1gFnSc1	lady
Macbethová	Macbethová	k1gFnSc1	Macbethová
připravuje	připravovat	k5eAaImIp3nS	připravovat
vraždu	vražda	k1gFnSc4	vražda
Na	na	k7c6	na
Macbethově	Macbethův	k2eAgInSc6d1	Macbethův
hradě	hrad	k1gInSc6	hrad
dostala	dostat	k5eAaPmAgFnS	dostat
lady	lady	k1gFnSc1	lady
Macbethová	Macbethová	k1gFnSc1	Macbethová
dopis	dopis	k1gInSc4	dopis
od	od	k7c2	od
svého	svůj	k3xOyFgMnSc2	svůj
manžela	manžel	k1gMnSc2	manžel
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
píše	psát	k5eAaImIp3nS	psát
o	o	k7c6	o
věštbách	věštba	k1gFnPc6	věštba
čarodějnic	čarodějnice	k1gFnPc2	čarodějnice
<g/>
.	.	kIx.	.
</s>
<s>
Vzápětí	vzápětí	k6eAd1	vzápětí
přijíždí	přijíždět	k5eAaImIp3nS	přijíždět
posel	posít	k5eAaPmAgMnS	posít
se	s	k7c7	s
zprávou	zpráva	k1gFnSc7	zpráva
o	o	k7c6	o
králově	králův	k2eAgFnSc6d1	králova
návštěvě	návštěva	k1gFnSc6	návštěva
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
si	se	k3xPyFc3	se
uvědomuje	uvědomovat	k5eAaImIp3nS	uvědomovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
skvělá	skvělý	k2eAgFnSc1d1	skvělá
příležitost	příležitost	k1gFnSc1	příležitost
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
krále	král	k1gMnSc2	král
zbavit	zbavit	k5eAaPmF	zbavit
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgFnPc1	svůj
nekalé	kalý	k2eNgFnPc1d1	nekalá
myšlenky	myšlenka	k1gFnPc1	myšlenka
se	se	k3xPyFc4	se
ale	ale	k9	ale
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
manžela	manžel	k1gMnSc2	manžel
nesnaží	snažit	k5eNaImIp3nP	snažit
potlačit	potlačit	k5eAaPmF	potlačit
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
její	její	k3xOp3gMnSc1	její
muž	muž	k1gMnSc1	muž
přijíždí	přijíždět	k5eAaImIp3nS	přijíždět
chvíli	chvíle	k1gFnSc4	chvíle
před	před	k7c7	před
králem	král	k1gMnSc7	král
<g/>
,	,	kIx,	,
říká	říkat	k5eAaImIp3nS	říkat
mu	on	k3xPp3gMnSc3	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
již	již	k6eAd1	již
vše	všechen	k3xTgNnSc4	všechen
připravila	připravit	k5eAaPmAgFnS	připravit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhý	druhý	k4xOgInSc4	druhý
den	den	k1gInSc4	den
vítá	vítat	k5eAaImIp3nS	vítat
lady	lady	k1gFnSc1	lady
Macbethová	Macbethová	k1gFnSc1	Macbethová
přátelsky	přátelsky	k6eAd1	přátelsky
krále	král	k1gMnSc2	král
Duncana	Duncan	k1gMnSc2	Duncan
<g/>
.	.	kIx.	.
</s>
<s>
Macbeth	Macbeth	k1gInSc1	Macbeth
má	mít	k5eAaImIp3nS	mít
sice	sice	k8xC	sice
velké	velký	k2eAgFnPc4d1	velká
pochybnosti	pochybnost	k1gFnPc4	pochybnost
a	a	k8xC	a
obavy	obava	k1gFnPc4	obava
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
podvolí	podvolit	k5eAaPmIp3nS	podvolit
<g/>
.	.	kIx.	.
</s>
<s>
Vražda	vražda	k1gFnSc1	vražda
Lady	lady	k1gFnSc1	lady
Macbethová	Macbethová	k1gFnSc1	Macbethová
nasype	nasypat	k5eAaPmIp3nS	nasypat
Duncanovým	Duncanův	k2eAgFnPc3d1	Duncanova
strážím	stráž	k1gFnPc3	stráž
do	do	k7c2	do
nápoje	nápoj	k1gInSc2	nápoj
drogu	droga	k1gFnSc4	droga
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
usnuli	usnout	k5eAaPmAgMnP	usnout
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
připraví	připravit	k5eAaPmIp3nS	připravit
pro	pro	k7c4	pro
Macbetha	Macbeth	k1gMnSc4	Macbeth
dvě	dva	k4xCgFnPc4	dva
dýky	dýka	k1gFnPc4	dýka
a	a	k8xC	a
hlídá	hlídat	k5eAaImIp3nS	hlídat
před	před	k7c7	před
královými	králův	k2eAgFnPc7d1	králova
komnatami	komnata	k1gFnPc7	komnata
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
vejde	vejít	k5eAaPmIp3nS	vejít
Macbeth	Macbeth	k1gInSc1	Macbeth
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Macbeth	Macbeth	k1gInSc1	Macbeth
vysílen	vysílen	k2eAgInSc1d1	vysílen
vyjde	vyjít	k5eAaPmIp3nS	vyjít
ven	ven	k6eAd1	ven
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
ruce	ruka	k1gFnPc4	ruka
i	i	k8xC	i
dýky	dýka	k1gFnPc4	dýka
od	od	k7c2	od
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zděšen	zděsit	k5eAaPmNgMnS	zděsit
ze	z	k7c2	z
svého	svůj	k3xOyFgInSc2	svůj
činu	čin	k1gInSc2	čin
<g/>
.	.	kIx.	.
</s>
<s>
Vtom	vtom	k6eAd1	vtom
někdo	někdo	k3yInSc1	někdo
buší	bušit	k5eAaImIp3nS	bušit
na	na	k7c4	na
bránu	brána	k1gFnSc4	brána
hradu	hrad	k1gInSc2	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Lady	lady	k1gFnSc1	lady
Macbethová	Macbethová	k1gFnSc1	Macbethová
sama	sám	k3xTgMnSc4	sám
rychle	rychle	k6eAd1	rychle
odnese	odnést	k5eAaPmIp3nS	odnést
dýky	dýka	k1gFnPc4	dýka
a	a	k8xC	a
pošle	poslat	k5eAaPmIp3nS	poslat
Macbetha	Macbetha	k1gFnSc1	Macbetha
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
převlékl	převléknout	k5eAaPmAgMnS	převléknout
a	a	k8xC	a
předstíral	předstírat	k5eAaImAgMnS	předstírat
<g/>
,	,	kIx,	,
že	že	k8xS	že
už	už	k6eAd1	už
spal	spát	k5eAaImAgMnS	spát
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
hrad	hrad	k1gInSc4	hrad
přijeli	přijet	k5eAaPmAgMnP	přijet
dva	dva	k4xCgMnPc1	dva
šlechtici	šlechtic	k1gMnPc1	šlechtic
-	-	kIx~	-
Lennox	Lennox	k1gInSc1	Lennox
a	a	k8xC	a
Macduff	Macduff	k1gInSc1	Macduff
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
král	král	k1gMnSc1	král
poslal	poslat	k5eAaPmAgMnS	poslat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ho	on	k3xPp3gMnSc4	on
brzy	brzy	k6eAd1	brzy
ráno	ráno	k6eAd1	ráno
navštívili	navštívit	k5eAaPmAgMnP	navštívit
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
uvidí	uvidět	k5eAaPmIp3nS	uvidět
zavražděného	zavražděný	k2eAgMnSc4d1	zavražděný
Duncana	Duncan	k1gMnSc4	Duncan
<g/>
,	,	kIx,	,
probudí	probudit	k5eAaPmIp3nS	probudit
celý	celý	k2eAgInSc4d1	celý
dům	dům	k1gInSc4	dům
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
přiběhne	přiběhnout	k5eAaPmIp3nS	přiběhnout
Macbeth	Macbeth	k1gInSc4	Macbeth
<g/>
,	,	kIx,	,
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
návalu	nával	k1gInSc6	nával
hněvu	hněv	k1gInSc2	hněv
právě	právě	k9	právě
zabil	zabít	k5eAaPmAgMnS	zabít
předpokládané	předpokládaný	k2eAgMnPc4d1	předpokládaný
vrahy	vrah	k1gMnPc4	vrah
-	-	kIx~	-
stráže	stráž	k1gFnSc2	stráž
<g/>
.	.	kIx.	.
</s>
<s>
Macbeth	Macbeth	k1gMnSc1	Macbeth
králem	král	k1gMnSc7	král
Skotska	Skotsko	k1gNnSc2	Skotsko
Duncanovi	Duncanův	k2eAgMnPc1d1	Duncanův
synové	syn	k1gMnPc1	syn
<g/>
,	,	kIx,	,
Malcolm	Malcolm	k1gMnSc1	Malcolm
a	a	k8xC	a
Donalbain	Donalbain	k1gMnSc1	Donalbain
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
bojí	bát	k5eAaImIp3nS	bát
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
otcově	otcův	k2eAgFnSc6d1	otcova
smrti	smrt	k1gFnSc6	smrt
jsou	být	k5eAaImIp3nP	být
v	v	k7c4	v
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
i	i	k8xC	i
oni	onen	k3xDgMnPc1	onen
<g/>
,	,	kIx,	,
a	a	k8xC	a
rozhodnou	rozhodnout	k5eAaPmIp3nP	rozhodnout
se	se	k3xPyFc4	se
uprchnout	uprchnout	k5eAaPmF	uprchnout
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
ale	ale	k9	ale
vyvolá	vyvolat	k5eAaPmIp3nS	vyvolat
podezření	podezření	k1gNnSc4	podezření
<g/>
,	,	kIx,	,
že	že	k8xS	že
byli	být	k5eAaImAgMnP	být
zapleteni	zaplést	k5eAaPmNgMnP	zaplést
do	do	k7c2	do
vraždy	vražda	k1gFnSc2	vražda
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
Macbeth	Macbeth	k1gMnSc1	Macbeth
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
králem	král	k1gMnSc7	král
<g/>
.	.	kIx.	.
</s>
<s>
Nedlouho	dlouho	k6eNd1	dlouho
po	po	k7c6	po
korunovaci	korunovace	k1gFnSc6	korunovace
se	se	k3xPyFc4	se
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
uspořádat	uspořádat	k5eAaPmF	uspořádat
hostinu	hostina	k1gFnSc4	hostina
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
pozve	pozvat	k5eAaPmIp3nS	pozvat
i	i	k9	i
Banqua	Banqua	k1gMnSc1	Banqua
-	-	kIx~	-
ten	ten	k3xDgMnSc1	ten
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
přesvědčen	přesvědčit	k5eAaPmNgMnS	přesvědčit
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Duncana	Duncan	k1gMnSc4	Duncan
zabil	zabít	k5eAaPmAgMnS	zabít
právě	právě	k9	právě
Macbeth	Macbeth	k1gMnSc1	Macbeth
<g/>
.	.	kIx.	.
</s>
<s>
Macbeth	Macbeth	k1gInSc1	Macbeth
má	mít	k5eAaImIp3nS	mít
o	o	k7c4	o
Banqua	Banquum	k1gNnPc4	Banquum
velký	velký	k2eAgInSc4d1	velký
zájem	zájem	k1gInSc4	zájem
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
čarodějnice	čarodějnice	k1gFnPc1	čarodějnice
předpověděly	předpovědět	k5eAaPmAgFnP	předpovědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gMnPc1	jeho
synové	syn	k1gMnPc1	syn
stanou	stanout	k5eAaPmIp3nP	stanout
králi	král	k1gMnSc3	král
<g/>
.	.	kIx.	.
</s>
<s>
Macbeth	Macbeth	k1gMnSc1	Macbeth
si	se	k3xPyFc3	se
tedy	tedy	k9	tedy
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
najme	najmout	k5eAaPmIp3nS	najmout
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
odpoledne	odpoledne	k6eAd1	odpoledne
vrahy	vrah	k1gMnPc4	vrah
<g/>
,	,	kIx,	,
o	o	k7c6	o
čemž	což	k3yQnSc6	což
však	však	k9	však
neřekne	říct	k5eNaPmIp3nS	říct
své	svůj	k3xOyFgFnSc3	svůj
ženě	žena	k1gFnSc3	žena
<g/>
.	.	kIx.	.
</s>
<s>
Banquo	Banquo	k6eAd1	Banquo
je	být	k5eAaImIp3nS	být
při	při	k7c6	při
přepadení	přepadení	k1gNnSc6	přepadení
opravdu	opravdu	k6eAd1	opravdu
zabit	zabít	k5eAaPmNgMnS	zabít
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInPc4	jeho
syn	syn	k1gMnSc1	syn
Fleance	Fleance	k1gFnSc2	Fleance
ale	ale	k8xC	ale
unikne	uniknout	k5eAaPmIp3nS	uniknout
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
hostiny	hostina	k1gFnSc2	hostina
se	se	k3xPyFc4	se
král	král	k1gMnSc1	král
dozvídá	dozvídat	k5eAaImIp3nS	dozvídat
o	o	k7c4	o
úmrtí	úmrtí	k1gNnSc4	úmrtí
Banqua	Banqu	k1gInSc2	Banqu
a	a	k8xC	a
Fleancově	Fleancův	k2eAgInSc6d1	Fleancův
útěku	útěk	k1gInSc6	útěk
<g/>
.	.	kIx.	.
</s>
<s>
Vzápětí	vzápětí	k6eAd1	vzápětí
uvidí	uvidět	k5eAaPmIp3nS	uvidět
Banquova	Banquův	k2eAgMnSc4d1	Banquův
ducha	duch	k1gMnSc4	duch
<g/>
.	.	kIx.	.
</s>
<s>
Vyděsí	vyděsit	k5eAaPmIp3nS	vyděsit
se	se	k3xPyFc4	se
a	a	k8xC	a
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
hostiny	hostina	k1gFnSc2	hostina
se	se	k3xPyFc4	se
chová	chovat	k5eAaImIp3nS	chovat
velmi	velmi	k6eAd1	velmi
divně	divně	k6eAd1	divně
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
co	co	k9	co
nejdříve	dříve	k6eAd3	dříve
znovu	znovu	k6eAd1	znovu
setkat	setkat	k5eAaPmF	setkat
s	s	k7c7	s
čarodějnicemi	čarodějnice	k1gFnPc7	čarodějnice
z	z	k7c2	z
vřesoviště	vřesoviště	k1gNnSc2	vřesoviště
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
věštba	věštba	k1gFnSc1	věštba
Když	když	k8xS	když
najde	najít	k5eAaPmIp3nS	najít
Macbeth	Macbeth	k1gInSc1	Macbeth
čarodějnice	čarodějnice	k1gFnSc2	čarodějnice
v	v	k7c6	v
jeskyni	jeskyně	k1gFnSc6	jeskyně
<g/>
,	,	kIx,	,
chce	chtít	k5eAaImIp3nS	chtít
aby	aby	kYmCp3nP	aby
mu	on	k3xPp3gMnSc3	on
předpověděly	předpovědět	k5eAaPmAgFnP	předpovědět
budoucnost	budoucnost	k1gFnSc4	budoucnost
<g/>
.	.	kIx.	.
</s>
<s>
Ony	onen	k3xDgFnPc1	onen
ho	on	k3xPp3gInSc4	on
varují	varovat	k5eAaImIp3nP	varovat
před	před	k7c7	před
Macduffem	Macduff	k1gInSc7	Macduff
a	a	k8xC	a
řeknou	říct	k5eAaPmIp3nP	říct
mu	on	k3xPp3gNnSc3	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
nikdo	nikdo	k3yNnSc1	nikdo
"	"	kIx"	"
<g/>
z	z	k7c2	z
ženy	žena	k1gFnSc2	žena
zrozený	zrozený	k2eAgMnSc1d1	zrozený
<g/>
"	"	kIx"	"
mu	on	k3xPp3gMnSc3	on
nemůže	moct	k5eNaImIp3nS	moct
ublížit	ublížit	k5eAaPmF	ublížit
<g/>
.	.	kIx.	.
</s>
<s>
Dodají	dodat	k5eAaPmIp3nP	dodat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Macbeth	Macbeth	k1gMnSc1	Macbeth
nebude	být	k5eNaImBp3nS	být
poražen	porazit	k5eAaPmNgMnS	porazit
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
"	"	kIx"	"
<g/>
birnamský	birnamský	k2eAgInSc1d1	birnamský
les	les	k1gInSc1	les
nezteče	ztéct	k5eNaPmIp3nS	ztéct
dunsinanský	dunsinanský	k2eAgInSc1d1	dunsinanský
hrad	hrad	k1gInSc1	hrad
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Macbetha	Macbetha	k1gMnSc1	Macbetha
to	ten	k3xDgNnSc4	ten
nevystraší	vystrašit	k5eNaPmIp3nS	vystrašit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
les	les	k1gInSc1	les
se	se	k3xPyFc4	se
přece	přece	k9	přece
nemůže	moct	k5eNaImIp3nS	moct
hýbat	hýbat	k5eAaImF	hýbat
<g/>
.	.	kIx.	.
</s>
<s>
Ptá	ptat	k5eAaImIp3nS	ptat
se	se	k3xPyFc4	se
ale	ale	k9	ale
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
Banquovi	Banquův	k2eAgMnPc1d1	Banquův
dědicové	dědic	k1gMnPc1	dědic
stanou	stanout	k5eAaPmIp3nP	stanout
králi	král	k1gMnSc3	král
<g/>
.	.	kIx.	.
</s>
<s>
Zjeví	zjevit	k5eAaPmIp3nS	zjevit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
přízrak	přízrak	k1gInSc4	přízrak
osmi	osm	k4xCc2	osm
panovníků	panovník	k1gMnPc2	panovník
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ano	ano	k9	ano
<g/>
.	.	kIx.	.
</s>
<s>
Macbeth	Macbeth	k1gInSc1	Macbeth
se	se	k3xPyFc4	se
mstí	mstít	k5eAaImIp3nS	mstít
Macduffovi	Macduffův	k2eAgMnPc1d1	Macduffův
Macduff	Macduff	k1gMnSc1	Macduff
se	se	k3xPyFc4	se
obrátil	obrátit	k5eAaPmAgMnS	obrátit
proti	proti	k7c3	proti
králi	král	k1gMnSc3	král
a	a	k8xC	a
uprchl	uprchnout	k5eAaPmAgInS	uprchnout
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
připojil	připojit	k5eAaPmAgMnS	připojit
ke	k	k7c3	k
svému	svůj	k3xOyFgMnSc3	svůj
bratru	bratr	k1gMnSc3	bratr
<g/>
.	.	kIx.	.
</s>
<s>
Macbeth	Macbetha	k1gFnPc2	Macbetha
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
dozví	dozvědět	k5eAaPmIp3nS	dozvědět
a	a	k8xC	a
mstí	mstít	k5eAaImIp3nS	mstít
se	se	k3xPyFc4	se
na	na	k7c6	na
Macduffově	Macduffův	k2eAgFnSc6d1	Macduffův
rodině	rodina	k1gFnSc6	rodina
-	-	kIx~	-
nechá	nechat	k5eAaPmIp3nS	nechat
zavraždit	zavraždit	k5eAaPmF	zavraždit
jeho	jeho	k3xOp3gFnSc4	jeho
manželku	manželka	k1gFnSc4	manželka
i	i	k9	i
s	s	k7c7	s
jeho	jeho	k3xOp3gFnPc7	jeho
dětmi	dítě	k1gFnPc7	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Macduff	Macduff	k1gMnSc1	Macduff
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc4	ten
dozví	dozvědět	k5eAaPmIp3nS	dozvědět
a	a	k8xC	a
chce	chtít	k5eAaImIp3nS	chtít
Macbetha	Macbetha	k1gFnSc1	Macbetha
osobně	osobně	k6eAd1	osobně
zabít	zabít	k5eAaPmF	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Lady	lady	k1gFnSc1	lady
Macbethová	Macbethová	k1gFnSc1	Macbethová
mezitím	mezitím	k6eAd1	mezitím
zešílí	zešílet	k5eAaPmIp3nS	zešílet
při	při	k7c6	při
neustálém	neustálý	k2eAgNnSc6d1	neustálé
vzpomínání	vzpomínání	k1gNnSc6	vzpomínání
na	na	k7c4	na
vraždu	vražda	k1gFnSc4	vražda
Duncana	Duncan	k1gMnSc2	Duncan
<g/>
.	.	kIx.	.
</s>
<s>
Bitva	bitva	k1gFnSc1	bitva
a	a	k8xC	a
vyplnění	vyplnění	k1gNnSc4	vyplnění
věštby	věštba	k1gFnSc2	věštba
Král	Král	k1gMnSc1	Král
se	se	k3xPyFc4	se
stará	starat	k5eAaImIp3nS	starat
o	o	k7c4	o
posílení	posílení	k1gNnSc4	posílení
obrany	obrana	k1gFnSc2	obrana
kolem	kolem	k7c2	kolem
dunsinanského	dunsinanský	k2eAgInSc2d1	dunsinanský
hradu	hrad	k1gInSc2	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
vojáků	voják	k1gMnPc2	voják
se	se	k3xPyFc4	se
připojila	připojit	k5eAaPmAgFnS	připojit
ke	k	k7c3	k
druhé	druhý	k4xOgFnSc3	druhý
straně	strana	k1gFnSc3	strana
<g/>
,	,	kIx,	,
on	on	k3xPp3gMnSc1	on
si	se	k3xPyFc3	se
však	však	k9	však
je	být	k5eAaImIp3nS	být
jist	jist	k2eAgMnSc1d1	jist
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
poražen	poražen	k2eAgMnSc1d1	poražen
-	-	kIx~	-
ne	ne	k9	ne
dokud	dokud	k6eAd1	dokud
birnamský	birnamský	k2eAgInSc1d1	birnamský
les	les	k1gInSc1	les
nedojde	dojít	k5eNaPmIp3nS	dojít
k	k	k7c3	k
Dunsinanu	Dunsinan	k1gInSc3	Dunsinan
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gMnPc1	jeho
nepřátelé	nepřítel	k1gMnPc1	nepřítel
sešli	sejít	k5eAaPmAgMnP	sejít
právě	právě	k6eAd1	právě
v	v	k7c6	v
birnamském	birnamský	k2eAgInSc6d1	birnamský
lese	les	k1gInSc6	les
<g/>
,	,	kIx,	,
každý	každý	k3xTgMnSc1	každý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
se	se	k3xPyFc4	se
zamaskoval	zamaskovat	k5eAaPmAgMnS	zamaskovat
větvemi	větev	k1gFnPc7	větev
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
Macbeth	Macbeth	k1gMnSc1	Macbeth
připravuje	připravovat	k5eAaImIp3nS	připravovat
k	k	k7c3	k
bitvě	bitva	k1gFnSc3	bitva
<g/>
,	,	kIx,	,
uslyší	uslyšet	k5eAaPmIp3nP	uslyšet
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc1	jeho
žena	žena	k1gFnSc1	žena
spáchala	spáchat	k5eAaPmAgFnS	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tom	ten	k3xDgInSc6	ten
okamžiku	okamžik	k1gInSc6	okamžik
posel	posel	k1gMnSc1	posel
hlásí	hlásit	k5eAaImIp3nS	hlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
birnamský	birnamský	k2eAgInSc1d1	birnamský
les	les	k1gInSc1	les
se	se	k3xPyFc4	se
pohnul	pohnout	k5eAaPmAgInS	pohnout
<g/>
.	.	kIx.	.
</s>
<s>
Malcolm	Malcolm	k6eAd1	Malcolm
a	a	k8xC	a
Macduff	Macduff	k1gInSc4	Macduff
zahájili	zahájit	k5eAaPmAgMnP	zahájit
útok	útok	k1gInSc4	útok
<g/>
.	.	kIx.	.
</s>
<s>
Macbeth	Macbeth	k1gMnSc1	Macbeth
svůj	svůj	k3xOyFgInSc4	svůj
hrad	hrad	k1gInSc4	hrad
ztratí	ztratit	k5eAaPmIp3nS	ztratit
a	a	k8xC	a
v	v	k7c6	v
boji	boj	k1gInSc6	boj
s	s	k7c7	s
Macduffem	Macduff	k1gInSc7	Macduff
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgInS	narodit
předčasně	předčasně	k6eAd1	předčasně
císařským	císařský	k2eAgInSc7d1	císařský
řezem	řez	k1gInSc7	řez
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zabit	zabít	k5eAaPmNgMnS	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Skotsku	Skotsko	k1gNnSc6	Skotsko
je	být	k5eAaImIp3nS	být
konečně	konečně	k6eAd1	konečně
mír	mír	k1gInSc4	mír
a	a	k8xC	a
Malcolm	Malcolm	k1gInSc4	Malcolm
je	být	k5eAaImIp3nS	být
korunován	korunovat	k5eAaBmNgMnS	korunovat
králem	král	k1gMnSc7	král
<g/>
.	.	kIx.	.
1948	[number]	k4	1948
Macbeth	Macbeth	k1gInSc1	Macbeth
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
americký	americký	k2eAgInSc1d1	americký
film	film	k1gInSc1	film
<g/>
.	.	kIx.	.
</s>
<s>
Režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Orson	Orson	k1gMnSc1	Orson
Welles	Welles	k1gMnSc1	Welles
<g/>
;	;	kIx,	;
hrají	hrát	k5eAaImIp3nP	hrát
<g/>
:	:	kIx,	:
Orson	Orson	k1gMnSc1	Orson
Welles	Welles	k1gMnSc1	Welles
<g/>
,	,	kIx,	,
Dan	Dan	k1gMnSc1	Dan
O	O	kA	O
<g/>
'	'	kIx"	'
<g/>
Herlihy	Herlih	k1gInPc1	Herlih
<g/>
,	,	kIx,	,
Roddy	Rodd	k1gInPc1	Rodd
McDowall	McDowalla	k1gFnPc2	McDowalla
<g/>
.	.	kIx.	.
1957	[number]	k4	1957
Krvavý	krvavý	k2eAgInSc4d1	krvavý
trůn	trůn	k1gInSc4	trůn
<g/>
,	,	kIx,	,
japonský	japonský	k2eAgInSc4d1	japonský
film	film	k1gInSc4	film
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
je	být	k5eAaImIp3nS	být
přenesen	přenést	k5eAaPmNgInS	přenést
do	do	k7c2	do
feudálního	feudální	k2eAgNnSc2d1	feudální
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Akira	Akira	k1gFnSc1	Akira
Kurosawa	Kurosawa	k1gFnSc1	Kurosawa
<g/>
;	;	kIx,	;
hrají	hrát	k5eAaImIp3nP	hrát
<g/>
:	:	kIx,	:
Toshirô	Toshirô	k1gMnSc5	Toshirô
Mifune	Mifun	k1gMnSc5	Mifun
<g/>
,	,	kIx,	,
Isuzu	Isuz	k1gInSc6	Isuz
Yamada	Yamada	k1gFnSc1	Yamada
<g/>
.	.	kIx.	.
1971	[number]	k4	1971
Macbeth	Macbeth	k1gInSc1	Macbeth
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Tragedy	Trageda	k1gMnSc2	Trageda
of	of	k?	of
Macbeth	Macbeth	k1gInSc1	Macbeth
<g/>
)	)	kIx)	)
anglický	anglický	k2eAgInSc1d1	anglický
film	film	k1gInSc1	film
<g/>
.	.	kIx.	.
</s>
<s>
Režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Roman	Roman	k1gMnSc1	Roman
Polanski	Polansk	k1gFnSc2	Polansk
<g/>
;	;	kIx,	;
hrají	hrát	k5eAaImIp3nP	hrát
<g/>
:	:	kIx,	:
Jon	Jon	k1gMnPc1	Jon
Finch	Finch	k1gMnSc1	Finch
<g/>
,	,	kIx,	,
<g/>
Francesca	Francesca	k1gMnSc1	Francesca
Annis	Annis	k1gFnSc2	Annis
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
Shaw	Shaw	k1gMnSc1	Shaw
<g/>
,	,	kIx,	,
Terence	Terence	k1gFnSc1	Terence
Bayler	Bayler	k1gMnSc1	Bayler
<g/>
.	.	kIx.	.
1979	[number]	k4	1979
Macbeth	Macbeth	k1gInSc1	Macbeth
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
<g/>
'	'	kIx"	'
(	(	kIx(	(
<g/>
A	a	k9	a
Performance	performance	k1gFnSc1	performance
of	of	k?	of
Macbeth	Macbeth	k1gInSc1	Macbeth
<g/>
)	)	kIx)	)
anglický	anglický	k2eAgInSc1d1	anglický
televizní	televizní	k2eAgInSc1d1	televizní
film	film	k1gInSc1	film
<g/>
.	.	kIx.	.
</s>
<s>
Režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Philip	Philip	k1gMnSc1	Philip
Casson	Casson	k1gMnSc1	Casson
<g/>
;	;	kIx,	;
hrají	hrát	k5eAaImIp3nP	hrát
<g/>
:	:	kIx,	:
Ian	Ian	k1gFnSc1	Ian
McKellen	McKellen	k1gInSc1	McKellen
<g/>
,	,	kIx,	,
Judi	Judi	k1gNnSc1	Judi
Dench	Dench	k1gInSc1	Dench
<g/>
,	,	kIx,	,
Roger	Roger	k1gInSc1	Roger
Rees	Rees	k1gInSc1	Rees
<g/>
,	,	kIx,	,
Ian	Ian	k1gMnSc1	Ian
McDiarmid	McDiarmid	k1gInSc1	McDiarmid
<g/>
.	.	kIx.	.
1981	[number]	k4	1981
Macbeth	Macbeth	k1gInSc1	Macbeth
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
československý	československý	k2eAgInSc1d1	československý
televizní	televizní	k2eAgInSc1d1	televizní
film	film	k1gInSc1	film
<g/>
.	.	kIx.	.
</s>
<s>
Režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Antonín	Antonín	k1gMnSc1	Antonín
Dvořák	Dvořák	k1gMnSc1	Dvořák
<g/>
;	;	kIx,	;
hrají	hrát	k5eAaImIp3nP	hrát
<g/>
:	:	kIx,	:
Dušan	Dušan	k1gMnSc1	Dušan
Jamrich	Jamrich	k1gMnSc1	Jamrich
<g/>
,	,	kIx,	,
Zdena	Zdena	k1gFnSc1	Zdena
Studenková	Studenkový	k2eAgFnSc1d1	Studenková
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
Huba	huba	k1gFnSc1	huba
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Durdík	Durdík	k1gMnSc1	Durdík
ml.	ml.	kA	ml.
<g/>
,	,	kIx,	,
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
Huba	huba	k1gFnSc1	huba
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
Kňažko	Kňažka	k1gFnSc5	Kňažka
<g/>
,	,	kIx,	,
Eva	Eva	k1gFnSc1	Eva
Matejková	Matejková	k1gFnSc1	Matejková
<g/>
.	.	kIx.	.
1982	[number]	k4	1982
Macbeth	Macbeth	k1gInSc1	Macbeth
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
maďarský	maďarský	k2eAgInSc1d1	maďarský
televizní	televizní	k2eAgInSc1d1	televizní
film	film	k1gInSc1	film
<g/>
.	.	kIx.	.
</s>
<s>
Režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
<g/>
Béla	Béla	k1gMnSc1	Béla
Tarr	Tarr	k1gMnSc1	Tarr
<g/>
;	;	kIx,	;
hrají	hrát	k5eAaImIp3nP	hrát
<g/>
:	:	kIx,	:
György	Györg	k1gInPc1	Györg
Cserhalmi	Cserhal	k1gFnPc7	Cserhal
<g/>
,	,	kIx,	,
Džoko	Džoko	k1gNnSc1	Džoko
Rosič	Rosič	k1gInSc1	Rosič
<g/>
.	.	kIx.	.
2001	[number]	k4	2001
Americký	americký	k2eAgInSc1d1	americký
Macbeth	Macbeth	k1gInSc1	Macbeth
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Scotland	Scotland	k1gInSc1	Scotland
<g/>
,	,	kIx,	,
PA	Pa	kA	Pa
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
americká	americký	k2eAgFnSc1d1	americká
komedie	komedie	k1gFnSc1	komedie
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
částečná	částečný	k2eAgFnSc1d1	částečná
parodie	parodie	k1gFnSc1	parodie
původní	původní	k2eAgFnSc2d1	původní
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Billy	Bill	k1gMnPc4	Bill
Morrissette	Morrissett	k1gMnSc5	Morrissett
<g/>
,	,	kIx,	,
hrají	hrát	k5eAaImIp3nP	hrát
<g/>
:	:	kIx,	:
James	James	k1gInSc1	James
LeGros	LeGrosa	k1gFnPc2	LeGrosa
<g/>
,	,	kIx,	,
Maura	Maur	k1gMnSc2	Maur
Tierney	Tiernea	k1gFnSc2	Tiernea
<g/>
,	,	kIx,	,
Christopher	Christophra	k1gFnPc2	Christophra
Walken	Walkna	k1gFnPc2	Walkna
<g/>
.	.	kIx.	.
2005	[number]	k4	2005
Macbeth	Macbeth	k1gInSc1	Macbeth
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
anglický	anglický	k2eAgInSc1d1	anglický
televizní	televizní	k2eAgInSc1d1	televizní
film	film	k1gInSc1	film
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
je	být	k5eAaImIp3nS	být
přenesen	přenést	k5eAaPmNgInS	přenést
do	do	k7c2	do
moderní	moderní	k2eAgFnSc2d1	moderní
doby	doba	k1gFnSc2	doba
z	z	k7c2	z
prostředí	prostředí	k1gNnSc2	prostředí
restaurací	restaurace	k1gFnPc2	restaurace
<g/>
.	.	kIx.	.
</s>
<s>
Režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Mark	Mark	k1gMnSc1	Mark
Brozel	Brozel	k1gMnSc1	Brozel
<g/>
;	;	kIx,	;
hrají	hrát	k5eAaImIp3nP	hrát
<g/>
:	:	kIx,	:
James	James	k1gInSc1	James
McAvoy	McAvoa	k1gFnSc2	McAvoa
<g/>
,	,	kIx,	,
Keeley	Keelea	k1gFnSc2	Keelea
Hawes	Hawesa	k1gFnPc2	Hawesa
<g/>
.	.	kIx.	.
2006	[number]	k4	2006
Macbeth	Macbeth	k1gInSc1	Macbeth
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
australský	australský	k2eAgInSc1d1	australský
thriler	thriler	k1gInSc1	thriler
<g/>
.	.	kIx.	.
</s>
<s>
Přeneseno	přenést	k5eAaPmNgNnS	přenést
do	do	k7c2	do
moderní	moderní	k2eAgFnSc2d1	moderní
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Geoffrey	Geoffrey	k1gInPc1	Geoffrey
Wright	Wrighta	k1gFnPc2	Wrighta
<g/>
;	;	kIx,	;
hrají	hrát	k5eAaImIp3nP	hrát
<g/>
:	:	kIx,	:
Sam	Sam	k1gMnSc1	Sam
Worthington	Worthington	k1gInSc4	Worthington
<g/>
,	,	kIx,	,
Victoria	Victorium	k1gNnSc2	Victorium
Hill	Hilla	k1gFnPc2	Hilla
<g/>
.	.	kIx.	.
2010	[number]	k4	2010
Macbeth	Macbeth	k1gInSc1	Macbeth
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
anglický	anglický	k2eAgInSc1d1	anglický
televizní	televizní	k2eAgInSc1d1	televizní
film	film	k1gInSc1	film
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
je	být	k5eAaImIp3nS	být
přenesen	přenést	k5eAaPmNgInS	přenést
do	do	k7c2	do
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
připodobňuje	připodobňovat	k5eAaImIp3nS	připodobňovat
Stalinovo	Stalinův	k2eAgNnSc1d1	Stalinovo
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Rupert	Rupert	k1gMnSc1	Rupert
Goold	Goold	k1gMnSc1	Goold
<g/>
;	;	kIx,	;
hrají	hrát	k5eAaImIp3nP	hrát
<g/>
:	:	kIx,	:
Patrick	Patrick	k1gMnSc1	Patrick
Stewart	Stewart	k1gMnSc1	Stewart
<g/>
,	,	kIx,	,
Oliver	Oliver	k1gMnSc1	Oliver
Burch	Burch	k1gMnSc1	Burch
<g/>
.	.	kIx.	.
2015	[number]	k4	2015
Macbeth	Macbeth	k1gInSc1	Macbeth
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
anglické	anglický	k2eAgNnSc1d1	anglické
drama	drama	k1gNnSc1	drama
<g/>
.	.	kIx.	.
</s>
<s>
Režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Justin	Justin	k1gMnSc1	Justin
Kurzel	Kurzel	k1gMnSc1	Kurzel
<g/>
;	;	kIx,	;
hrají	hrát	k5eAaImIp3nP	hrát
<g/>
:	:	kIx,	:
Michael	Michael	k1gMnSc1	Michael
Fassbender	Fassbender	k1gMnSc1	Fassbender
<g/>
,	,	kIx,	,
Marion	Marion	k1gInSc1	Marion
Cotillard	Cotillard	k1gInSc1	Cotillard
Terry	Terra	k1gFnSc2	Terra
Pratchett	Pratchetta	k1gFnPc2	Pratchetta
se	se	k3xPyFc4	se
nechal	nechat	k5eAaPmAgInS	nechat
inspirovat	inspirovat	k5eAaBmF	inspirovat
Shakespearovým	Shakespearův	k2eAgInSc7d1	Shakespearův
Macbethem	Macbeth	k1gInSc7	Macbeth
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Soudné	soudný	k2eAgFnSc2d1	soudná
sestry	sestra	k1gFnSc2	sestra
(	(	kIx(	(
<g/>
Wyrd	Wyrd	k1gInSc1	Wyrd
Sisters	Sistersa	k1gFnPc2	Sistersa
<g/>
)	)	kIx)	)
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1988	[number]	k4	1988
(	(	kIx(	(
<g/>
originál	originál	k1gInSc1	originál
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
vyšel	vyjít	k5eAaPmAgInS	vyjít
překlad	překlad	k1gInSc1	překlad
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
Opera	opera	k1gFnSc1	opera
Macbeth	Macbetha	k1gFnPc2	Macbetha
od	od	k7c2	od
Giuseppe	Giusepp	k1gInSc5	Giusepp
Verdiho	Verdi	k1gMnSc2	Verdi
<g/>
.	.	kIx.	.
kapela	kapela	k1gFnSc1	kapela
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
má	mít	k5eAaImIp3nS	mít
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
repertoáru	repertoár	k1gInSc6	repertoár
píseň	píseň	k1gFnSc1	píseň
Macbeth	Macbeth	k1gInSc1	Macbeth
<g/>
.	.	kIx.	.
</s>
<s>
MACURA	Macura	k1gMnSc1	Macura
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
a	a	k8xC	a
kolektiv	kolektiv	k1gInSc1	kolektiv
<g/>
.	.	kIx.	.
</s>
<s>
Slovník	slovník	k1gInSc4	slovník
světových	světový	k2eAgNnPc2d1	světové
literárních	literární	k2eAgNnPc2d1	literární
děl	dělo	k1gNnPc2	dělo
2	[number]	k4	2
<g/>
/	/	kIx~	/
M-	M-	k1gMnPc2	M-
<g/>
Ž.	Ž.	kA	Ž.
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
207	[number]	k4	207
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
960	[number]	k4	960
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
459	[number]	k4	459
<g/>
.	.	kIx.	.
</s>
<s>
JIŘÍK	Jiřík	k1gMnSc1	Jiřík
<g/>
,	,	kIx,	,
Richard	Richard	k1gMnSc1	Richard
<g/>
.	.	kIx.	.
</s>
<s>
Stručné	stručný	k2eAgInPc1d1	stručný
výklady	výklad	k1gInPc1	výklad
nejlepších	dobrý	k2eAgNnPc2d3	nejlepší
dramat	drama	k1gNnPc2	drama
Shakespearových	Shakespearových	k2eAgNnPc2d1	Shakespearových
<g/>
.	.	kIx.	.
</s>
<s>
Vyškov	Vyškov	k1gInSc1	Vyškov
:	:	kIx,	:
Hanácké	hanácký	k2eAgNnSc1d1	Hanácké
knihkupectví	knihkupectví	k1gNnSc1	knihkupectví
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Pátka	Pátek	k1gMnSc2	Pátek
<g/>
,	,	kIx,	,
1907	[number]	k4	1907
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
Macbeth	Macbetha	k1gFnPc2	Macbetha
<g/>
,	,	kIx,	,
s.	s.	k?	s.
59	[number]	k4	59
<g/>
-	-	kIx~	-
<g/>
67	[number]	k4	67
<g/>
.	.	kIx.	.
</s>
<s>
SHAKESPEARE	Shakespeare	k1gMnSc1	Shakespeare
<g/>
,	,	kIx,	,
William	William	k1gInSc1	William
<g/>
.	.	kIx.	.
</s>
<s>
Makbeth	Makbeth	k1gMnSc1	Makbeth
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Josef	Josef	k1gMnSc1	Josef
Jiří	Jiří	k1gMnSc1	Jiří
Kolár	Kolár	k1gMnSc1	Kolár
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Museum	museum	k1gNnSc1	museum
Království	království	k1gNnSc2	království
českého	český	k2eAgNnSc2d1	české
<g/>
,	,	kIx,	,
1868	[number]	k4	1868
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
české	český	k2eAgNnSc1d1	české
vydání	vydání	k1gNnSc1	vydání
Shakespearova	Shakespearův	k2eAgMnSc2d1	Shakespearův
Makbetha	Makbeth	k1gMnSc2	Makbeth
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
Josefa	Josef	k1gMnSc2	Josef
J.	J.	kA	J.
Kolára	Kolár	k1gMnSc2	Kolár
<g/>
..	..	k?	..
SHAKESPEARE	Shakespeare	k1gMnSc1	Shakespeare
<g/>
,	,	kIx,	,
William	William	k1gInSc1	William
<g/>
.	.	kIx.	.
</s>
<s>
Makbeth	Makbeth	k1gMnSc1	Makbeth
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Josef	Josef	k1gMnSc1	Josef
Václav	Václav	k1gMnSc1	Václav
Sládek	Sládek	k1gMnSc1	Sládek
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
J.	J.	kA	J.
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
1896	[number]	k4	1896
nebo	nebo	k8xC	nebo
1897	[number]	k4	1897
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Macbeth	Macbetha	k1gFnPc2	Macbetha
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Dílo	dílo	k1gNnSc1	dílo
Macbeth	Macbeth	k1gMnSc1	Macbeth
(	(	kIx(	(
<g/>
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
<g/>
)	)	kIx)	)
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Macbeth	Macbeth	k1gMnSc1	Macbeth
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
</s>
