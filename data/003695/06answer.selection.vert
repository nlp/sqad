<s>
Hra	hra	k1gFnSc1	hra
je	být	k5eAaImIp3nS	být
inspirována	inspirovat	k5eAaBmNgFnS	inspirovat
historickou	historický	k2eAgFnSc7d1	historická
postavou	postava	k1gFnSc7	postava
skotského	skotský	k2eAgMnSc4d1	skotský
krále	král	k1gMnSc4	král
Macbetha	Macbeth	k1gMnSc2	Macbeth
I.	I.	kA	I.
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mezi	mezi	k7c7	mezi
touto	tento	k3xDgFnSc7	tento
historickou	historický	k2eAgFnSc7d1	historická
postavou	postava	k1gFnSc7	postava
a	a	k8xC	a
jejím	její	k3xOp3gNnSc7	její
Shakespearovým	Shakespearův	k2eAgNnSc7d1	Shakespearovo
pojetím	pojetí	k1gNnSc7	pojetí
je	být	k5eAaImIp3nS	být
značný	značný	k2eAgInSc1d1	značný
rozdíl	rozdíl	k1gInSc1	rozdíl
<g/>
.	.	kIx.	.
</s>
